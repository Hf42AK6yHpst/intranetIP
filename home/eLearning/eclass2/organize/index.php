<?php
# editing by
/**
 * Log:
 * 		2011-10-19 (Thomas)
 * 			Table is ordered by Course Code by default 
 * 		2011-08-23 (Yuen)
 * 			added import member to course function
 * 
 * 
 * 		Date: 2010-11-11 (Thomas)
 * 		Details: modified the sql, join user_course table to get no. of users of corresponding classroom
 * 		Date: 2010-07-23 (Jason Lam)
 * 		Details: change the dbtable to new display using displayFormat_IP25_table(), thus set $li->IsColOff = "IP25_table";
 * 
 * $eclass_mck - Do not display eclass license msg when this variable is not set (unlimited)
 */   
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
eclass_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";


//$roomType = 12;	# for testing
$specialClassroom = true;

# eClass
$lo = new libeclass();
$leclass = new libeclass2007('', $roomType, $specialClassroom);
$eclass_quota = $lo->status();

$lu = new libuser($UserID);

# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if ($lu->RecordType!=1 || !$la->isAccessEClassMgt())
  {
          header("Location: /");
          exit;
  }
}else $isAdmin = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# init
$lo->setRoomType($roomType);


# select courses the teacher teaches
$course_list = "0";
$sql = "SELECT course_id FROM user_course WHERE memberType='T' AND user_email='".$lu->UserEmail."'";
$row = $lo->returnArray($sql, 1);
for ($i=0; $i<sizeof($row); $i++)
{
        $course_list .= ",".$row[$i][0];
}

# TABLE SQL
$keyword = trim($keyword);
if($order == "") $order = 1; // Order in ascending order by default
if($field == "") $field = 1; // Ordered by Course Code by default
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     case 5: $field = 5; break;
     case 6: $field = 6; break;
     case 7: $field = 7; break;
     default: $field = 7; break;
}

$sql_title = ($lu->teaching&&$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& $la->isAccessEClassNTMgtCourse() || $isAdmin) ? "CONCAT('<a class=\"tablelink\" href=\"edit.php?keyword=$keyword&course_id[]=', a.course_id, '&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage\">', a.course_name, '</a>')" : "a.course_name";
$name_field = getNameFieldByLang("b.");

/* show only the course the teacher teaches or creates */
$sql = "SELECT
               a.course_id,
               a.course_code,
               {$sql_title},			   
               CONCAT('<a class=\"tablelink\" href=\"user/index.php?course_id=', a.course_id, '\">', IF(c.cnt IS NULL OR c.cnt = '', 0, c.cnt), '</a>'),
               ifnull(a.max_user,'-'),
               ifnull(a.max_storage,'-'),
               if(b.UserID is null,'-',".$name_field.") as creator_name,
               a.inputdate,
               CONCAT('<input type=\"checkbox\" id=\"course_id',  a.course_id,'\" name=\"course_id[]\" value=\"', a.course_id ,'\">
			   <input type=\"hidden\" id=\"Vcourse_id',a.course_id,'\" value=\"',a.IsFromSubjectGroup,'\">
			   <input type=\"hidden\" id=\"Ncourse_id',a.course_id,'\" value=\"',a.course_name,'\">
			   '), 
			   if(SubjectGroupID != '' && SubjectGroupID is not null && SubjectGroupID != '0', 'row_avaliable', 'row_approved') as trCustClass  
          FROM
               course as a 
          LEFT JOIN
               {$intranet_db}.INTRANET_USER as b ON a.creator_id = b.UserID
		  LEFT JOIN
			   (SELECT
						course_id,
						COUNT(*) AS cnt
				FROM
						user_course
				WHERE
						status IS NULL
				GROUP BY
						course_id) AS c ON a.course_id = c.course_id
          WHERE (a.course_name like '%".str_replace(Array("%","_"),Array("\\%","\\_"),htmlspecialchars(addslashes($keyword),ENT_QUOTES))."%' OR a.course_code like '%".str_replace(Array("%","_"),Array("\\%","\\_"),$keyword)."%')                               
                           AND a.RoomType='".$roomType."' 
          ";

$sql .= ($isAdmin)? "" : " AND (a.course_id IN ($course_list) OR a.creator_id='$UserID')";
// echo htmlspecialchars($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("course_id", "course_code", "course_name", "no_users", "max_user", "max_storage", "creator_name", "inputdate");
$li->db = $eclass_db;
$li->sql = $sql;
$li->no_col = ($lu->teaching&&$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& $la->isAccessEClassNTMgtCourse() || $isAdmin) ? sizeof($li->field_array)+2 : sizeof($li->field_array)+1;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = "IP25_table";

// TABLE COLUMN
$li->column_list .= "<th width='1' class='tabletop tabletopnolink' style='vertical-align:middle'>&nbsp;#&nbsp;</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(0, $i_eClassCourseID)."</th>\n";
$li->column_list .= "<th width='15%' class='tabletop' style='vertical-align:middle'>".$li->column(1, $i_eClassCourseCode)."</th>\n";
$li->column_list .= "<th width='25%' class='tabletop' style='vertical-align:middle'>".$li->column(2, $i_eClassCourseName)."</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(3, $i_eClassNumUsers)."</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(4, $i_eClassMaxUser)."</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(5, $i_eClassMaxStorage)."</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(6, $i_eClassCreatorBy)."</th>\n";
$li->column_list .= "<th width='20%' class='tabletop' style='vertical-align:middle'>".$li->column(7, $i_eClassInputdate)."</th>\n";
$li->column_list .=  ($lu->teaching&&$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& $la->isAccessEClassNTMgtCourse() || $isAdmin) ? "<th width='1' class='tabletop' style='vertical-align:middle'>".$li->check("course_id[]")."</th>\n" : "";

// TABLE FUNCTION BAR
if ($lu->teaching&&$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& $la->isAccessEClassNTMgtCourse()|| $isAdmin)
{
	$AddBtn = "<a href=\"javascript:checkNew('new.php?roomType=".$roomType."')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
	$AddBtn .= " &nbsp; <a href=\"javascript:checkNew('import_member2course.php?roomType=".$roomType."')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $Lang['eclass']['import_member2course'] . "</a>";
	$editBtn = "<a href=\"javascript:checkEdit(document.form1,'course_id[]','edit.php?roomType=".$roomType."')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

	if (!$sys_custom['disable_course_removal'])
	{
		$delBtn = "<a href=\"javascript:CheckRemoveItemState();\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
	}

	$copyBtn = "<a href=\"javascript:checkEdit(document.form1,'course_id[]','copy.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_copy.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_copy . "</a>";
	
	/*
	### license left message ###
	# added by Kelvin 2010-03-19
	# edited by Sandy 2010-05-25 : add checking to ignore license msg when eclass license is set to unlimited.
	if(isset($eclass_mck) && $eclass_mck > 0){
		$l_left = $lo->ticket();
		$l_used = $lo->license - $l_left;
		$license_html = $i_eClassLicenseDisplayN1 . $lo->license . $i_eClassLicenseDisplay2 . $l_left . $i_eClassLicenseDisplay3 . $i_eClassLicenseDisplay4 . $l_used . $i_eClassLicenseDisplay5;
	}
	*/
}

$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag  	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3")."</td>";
$searchTag 	.= "</tr></table>";





### Title ###
$TAGS_OBJ[] = array("<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_management.gif' align='absmiddle' /> " . $i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script language="javascript">
function CheckRemoveItemState(){
	/*removeItem = $('input[name="course_id[]"]:checked');
	//alert(removeItem.length);
	//return false;
	warningItem = '';
	cnt = 0;
	removeItem.each(function(){
		isFromSubjectGroup = document.getElementById('V'+this.id).value;
		if (isFromSubjectGroup == 1){
			warningItem = (cnt>0?', ':'')+document.getElementById('N'+this.id).value
			cnt++;
		}
	});
	extraStr ='?isRemoveSubjectGroup=0';
	if (cnt>0){
		if (confirm("<?=$Lang['eclass']['warning']['checkRemoveConfrim']?>"+warningItem)){
		
			extraStr ='?isRemoveSubjectGroup=1';
		}
	}*/
	checkRemove(document.form1,'course_id[]','remove.php');
}
</script>
<form name="form1" method="post" action="index.php">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td align="center">
      <? if($leclass->checkeSpecialRoomtAccessPermission()){ ?>
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" class="tabletext">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
                </tr>
                <tr><td><p><?=$AddBtn?></p></td><td align="right"><?=$searchTag?></td></tr>
                <tr>
                  <td align="left" valign="bottom"><?=$license_html?></td>
                  <td align="right" valign="bottom" height="28">
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
                        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
                          <table border="0" cellspacing="0" cellpadding="2">
                            <tr>
                              <td nowrap><?=$delBtn?></td>
                              <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                              <td nowrap><?=$editBtn?></td>
                              <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                              <td nowrap><?=$copyBtn?></td>
                            </tr>
                          </table>
                        </td>
                        <td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <?=$li->display();?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      <? } else { ?>
      	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
        </table>
      <? } ?>
      </td>
    </tr>
  </table>
  <br />

  <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
  <input type="hidden" name="order" value="<?php echo $li->order; ?>">
  <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
  <input type="hidden" id="roomType" name="roomType" value="<?=$roomType?>" />
</form>



<?php
eclass_closedb();
$linterface->LAYOUT_STOP();
?>