<?php
// editing by Paul

/******************************************* Changes log **********************************************
 * 2019-04-30 (Paul): Replace all $_REQUEST as $_REQUEST is insecure.
 * 2012-09-12 (CharlesMa): add check if force to one page mode  
 ******************************************************************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT."includes/libebookreader_ui.php");

intranet_auth();
intranet_opendb();

//$task = $_REQUEST['task'];

$libebookreader_ui = new libebookreader_ui();
switch($task)
{	
	case "Get_Table_of_Content_Layer":
		$BookID = IntegerSafe($BookID);
		$PageMode = IntegerSafe($PageMode);
		$ImageBook = ($ImageBook == '1')?true:false;
		$ImageWidth = IntegerSafe($ImageWidth);
		
		if($PageMode=='1' || ($userBrowser->browsertype == 'MSIE' && $ImageBook != '1')){
			echo $libebookreader_ui->Get_Table_of_Content_Layer_IE($BookID, $PageMode, $ImageBook, $ImageWidth);
		}else{
			echo $libebookreader_ui->Get_Table_of_Content_Layer($BookID, $PageMode, $ImageBook, $ImageWidth);
		}
	break;
	
	case "Get_Audio_Control":
		$src = urldecode($src);
		$width = IntegerSafe($width);
		$height = IntegerSafe($height);
		
		//2013-10-22 Charles Ma
		$version = explode('.',$userBrowser->version);
		if(($userBrowser->browsertype == 'Firefox' && $version[0] <= 8) || $userBrowser->browsertype == 'MSIE'){
			echo $libebookreader_ui->Get_Audio_Object_Control($src, $width, $height);
		}else if ($userBrowser->platform == 'Andriod' || $userBrowser->platform == 'Linux'){
			echo "<audio name=\"".$divPlayer."audio_player\" id=\"".$divPlayer."audio_player\" autoplay=\"autoplay\" controls=\"controls\">\n
				<source src=\"".$src."\" type=\"audio/mpeg\" />\n
				</audio>";
		}else{
			echo $libebookreader_ui->Get_Audio_Control($src, $width, $height);
		}
	break;
	
	case "Get_Page_Highlights":
		$BookID = IntegerSafe($BookID);
		$_UserID = $_SESSION['UserID'];
		$RefID = urldecode($RefID);
		$libebookreader = $libebookreader_ui->Get_libebookreader();
		$highlights = $libebookreader->Get_Page_Highlights($BookID,$_UserID,$RefID);
		for($i=0;$i<count($highlights);$i++){
			$highlights[$i] = stripslashes($highlights[$i]);
		}
		
		echo implode(",", $highlights);
	break;
	
	case "Get_Tool_Bookmark_List_Log_List":
		$BookID = IntegerSafe($BookID);
		$_UserID = $_SESSION['UserID'];
		$PageMode = IntegerSafe($PageMode);
		$ImageBook = ($ImageBook == '1')?true:false;
		
		echo $libebookreader_ui->Get_Tool_Bookmark_List_Log_List($BookID, $_UserID, $PageMode, $ImageBook);
	break;
	
	case "Get_Book_User_Highlight_Notes":
		$BookID = IntegerSafe($BookID);
		$_UserID = $_SESSION['UserID'];
		$libebookreader = $libebookreader_ui->Get_libebookreader();
		$notes_list = $libebookreader->Get_Book_User_Highlight_Notes($BookID,$_UserID);
		
		$json_array = "[";
		$delimiter = "";
		for($i=0;$i<count($notes_list);$i++){
			$json_array .= $delimiter.stripslashes($notes_list[$i]['Highlight']);
			$delimiter = ",";
		}
		$json_array .= "]";
		
		echo $json_array;
	break;
	
	case "Get_Tool_Notes_Log_Entry":
		echo $libebookreader_ui->Get_Tool_Notes_Log_Entry();
	break;
	
	case "Get_Tool_Book_Review_Log_List":
		$BookID = IntegerSafe($BookID);
		echo $libebookreader_ui->Get_Tool_Book_Review_Log_List($BookID);
	break;
	
	case "Get_Tool_Book_Review_Helpful_Vote": 
		$ReviewID = IntegerSafe($ReviewID);
		echo $libebookreader_ui->Get_Tool_Book_Review_Helpful_Vote($ReviewID);
	break;
	
	case "Get_HTML_Image_Srcs":	//2013-10-17 Charles Ma
		$BookID = IntegerSafe($BookID);
		//$PageMode = $_REQUEST['PageMode'];
		$RequestPaths = cleanCrossSiteScriptingCode($RequestPaths);
		$extra = $extra;
		$libebookreader = $libebookreader_ui->Get_libebookreader();
		$img_src_arr = $libebookreader->Get_HTML_Image_Srcs($BookID,$RequestPaths,$extra);
		
		$json_arr = "[";
		$delimiter = "";
		for($i=0;$i<count($img_src_arr);$i++){
			$json_arr.= $delimiter."'".rawurlencode($img_src_arr[$i])."'";
			$delimiter = ",";
		}
		$json_arr.="]";
		
		echo $json_arr;
	break;
	
	case "Get_Tool_Page_Notes_Log_Entries":
		$BookID = IntegerSafe($BookID);
		$_UserID = $_SESSION['UserID'];
		$PageMode = IntegerSafe($PageMode);
		$libebookreader = $libebookreader_ui->Get_libebookreader();
		$data_list = $libebookreader->Get_Book_User_Page_Notes_List($BookID,$_UserID,$PageMode);
		$x = $libebookreader_ui->Get_Tool_Page_Notes_Log_Entries($data_list);
		
		echo $x;
	break;
	
	case "Search_Keyword":
		$BookID = IntegerSafe($BookID);
		$KeyWord = cleanCrossSiteScriptingCode($KeyWord);
		$libebookreader = $libebookreader_ui->Get_libebookreader();
		$result = $libebookreader->Search_KeyWord($BookID,$KeyWord);
		echo $result;
	break;
	
	case "Get_MP3_Srcs":
//		$libebookreader = $libebookreader_ui->Get_libebookreader();
//		echo $libebookreader->retrieveMP3Path($_REQUEST['BookID']);
	break;
}


intranet_closedb();
?>
