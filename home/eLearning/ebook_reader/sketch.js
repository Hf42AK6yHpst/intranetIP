navigator.sayswho= (function(){
    var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\bOPR\/(\d+)/)
        if(tem!= null) return 'Opera '+tem[1];
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();

var c_right;

function initate_drawing_page(type){
	$("#browser_info").html(navigator.sayswho);
	c_right = document.getElementById("book_comic_image_"+type+"_canvas");	
	
	
	var current_color = "#ff0000"; var selected_color = "#ff0000";
	var currentWidth = 10; var lineWidth = 1; var eraserWidth = 5; 
 	var globalCompositeOperation = "xor";
 	var globalAlpha = 0.1;
	var history_index = 0;

	var mouseDown = false;	
	
	$("#book_comic_image_"+type+"_canvas").unbind("mousedown touchstart").bind("mousedown touchstart", function(){			
		$(".main_content").css({"position":"initial","z-index": 3}); 
		
		$(".book_comic_image_canvas").css("z-index","4");
		mouseDown = true;
		current_point_arr =[];
		var ctx = $("#book_comic_image_"+type+"_canvas").get(0).getContext("2d");
		ctx.globalAlpha=globalAlpha;
		ctx.closePath();
		ctx.beginPath();
		point_group = point_group.slice(0,point_group.length - history_index);
		history_index = 0;
		return false;
	});

	$("#book_comic_image_"+type+"_canvas").unbind("mouseup mouseout touchmove").bind("mouseup mouseout touchmove", function(){
		$(".main_content").css({"position":"relative","z-index": 5}); 		
			
		var ctx = $("#book_comic_image_"+type+"_canvas").get(0).getContext("2d");
		ctx.globalAlpha=globalAlpha;
		ctx.closePath();
		mouseDown = false;	
		
		if(current_point_arr.length > 0){
			current_point_arr =[];		
			
			width = $("#book_comic_image_"+type+"_canvas").css("width");
	   		height = $("#book_comic_image_"+type+"_canvas").css("height");

	   		var dataURL = c_right.toDataURL();	
			var current_image_data = ctx.getImageData(0, 0, parseInt(width), parseInt(height));

			point_group.push(current_image_data);
		}	
		return false;
	});

	$("#book_comic_image_"+type+"_canvas").unbind("mousemove touchend").bind("mousemove touchend", function(evt){				
		
		$(".book_comic_image_canvas").css("z-index","4");
		var ctx = $("#book_comic_image_"+type+"_canvas").get(0).getContext("2d");
		if(mouseDown){

			ctx.globalAlpha=0.2;
			
			if(current_point_arr.length == 0){
				current_point_arr.push([evt.offsetX,evt.offsetY]);
			}else if (jQuery.inArray( [evt.offsetX,current_point_arr[0][1]], current_point_arr ) == -1){
				current_point_arr.push([evt.offsetX,current_point_arr[0][1]]);
			}else{
				return true;
			}			
			
			
			ctx.strokeStyle = current_color;
			ctx.globalCompositeOperation = globalCompositeOperation;
			if(current_point_arr.length > 1){
      			ctx.lineWidth = currentWidth;
				var i = current_point_arr.length-1;
				
				ctx.lineTo(current_point_arr[i][0]/parseFloat($('body').css('zoom')),current_point_arr[0][1]/parseFloat($('body').css('zoom')));
				ctx.stroke();
			}else{
				var i = current_point_arr.length-1;
				ctx.moveTo(current_point_arr[i][0] / parseFloat($('body').css('zoom')),current_point_arr[i][1]/parseFloat($('body').css('zoom')));
			}
		}
		return false;
	});
}

$(document).ready(function(){
	
	$("#btn_color_more").bind("click tap", function(){
		$("#tool_layer_color_fill").css("visibility", $("#tool_layer_color_fill").css("visibility") == "hidden" ? "visible" : "hidden" );		
		current_color = selected_color; currentWidth = lineWidth;
		globalCompositeOperation = "source-over";
	});

	$("#btn_eraser_more").bind("click tap", function(){
		$("#tool_layer_pen_size").css("visibility", $("#tool_layer_pen_size").css("visibility") == "hidden" ? "visible" : "hidden" );
		current_color = "rgba(0,0,0,1)"; currentWidth = eraserWidth;
		globalCompositeOperation = "destination-out";
	});

	$(".btn_erase_all").bind("click tap", function(){
		ctx.clearRect(0, 0,  parseInt($("#book_comic_image_"+type+"_canvas").css("width")),  parseInt($("#book_comic_image_"+type+"_canvas").css("height")));
	});
	

	$( "#unit_control_btn_line" ).draggable({ axis: "x",containment: "#containment-wrapper-line", scroll: false ,	start: function(){ 		
	},	drag: function(){ 
		lineWidth= ($(this).css("left").split("px")[0]) * 50 /185;
		currentWidth = lineWidth;
	},	stop: function( event, ui ) {			
	}	
	});

	$( "#unit_control_btn_eraser" ).draggable({ axis: "x",containment: "#containment-wrapper-eraser", scroll: false ,	start: function(){ 		
	},	drag: function(){ 
		eraserWidth= ($(this).css("left").split("px")[0]) * 50 /185;
		currentWidth = eraserWidth;
	},	stop: function( event, ui ) {			
	}	
	});
	
	$("#btn_undo").bind("click tap", function(){
		if(point_group.length > history_index + 1){
			history_index++;
			ctx.putImageData(point_group[point_group.length - history_index-1], 0, 0);
		}
	});

	$("#btn_redo").bind("click tap", function(){
		if(history_index > 0){
			history_index--;
			ctx.putImageData(point_group[point_group.length - history_index-1], 0, 0);
		}
	});

	///////////////////////

	$(".btn_hand_draw").bind("click tap", function(){
		current_color = selected_color; currentWidth = lineWidth;
		globalCompositeOperation = "source-over";
	});

	$(".btn_eraser").bind("click tap", function(){
		current_color = "rgba(0,0,0,1)";  currentWidth = eraserWidth;		
		globalCompositeOperation = "destination-out";
	});

	///////////////////////

	$(".group_btn").bind("click tap", function(){
		$(".group_btn").removeClass("selected");
		$(this).toggleClass("selected");
	});

	$(".btn_color_select").bind("click tap", function(){
		$(".btn_color_select").parent().removeClass("selected_color");
		$(this).parent().toggleClass("selected_color");
		current_color = $(this).attr("color");
		selected_color =current_color;
	});

	$("a").bind("click tap" , function(){
		if($(this).attr("id") == "unit_control_btn_line"){

		}
		else if($(this).attr("id") != "btn_color_more"){
			$("#tool_layer_color_fill").css("visibility", "hidden" );
		}else if($(this).attr("id") != "btn_eraser_more"){
			$("#tool_layer_pen_size").css("visibility", "hidden" );			
		}
	})

});


var point_group =[]; var current_point_arr =[];