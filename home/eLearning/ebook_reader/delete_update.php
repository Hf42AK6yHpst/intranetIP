<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");

intranet_auth();
intranet_opendb();

$BookID = $_REQUEST['BookID'];
if(empty($BookID) || !isset($_REQUEST['BookID'])){
	$msg = "0|=|Delete book fail";
	header("Location: index.php?msg=".urlencode($msg));
	exit;
}

$libebookreader = new libebookreader();

$success = $libebookreader->Delete_Book($BookID);

if($success){
	$msg = "1|=|Delete book success";
}else{
	$msg = "0|=|Delete book fail";
}

intranet_closedb();
header("Location: index.php?msg=".urlencode($msg));
?>