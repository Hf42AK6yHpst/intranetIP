Node.prototype.countPrevSiblings = function()
{
	var count = 0;
	var obj = this;
	var counter_text = false;
	while(obj && obj.previousSibling)
	{	//console.log(count);
		//console.log(obj);
		//console.log(obj.nodeType);
		obj = obj.previousSibling;	
		if (obj.toString() != "[object Text]" || !counter_text)++count;
		if (obj.toString() == "[object Text]")counter_text=true;
		else counter_text=false;
	}

	return count;
}

Node.prototype.traverseTop = function(treeNode)
{
	var obj = this;
	var superParentNode = document.getElementById('book_content_main');
	if (obj && obj.parentNode && obj.parentNode != superParentNode) //should set a correct anchor for document.body inside the ebook reader
	{
		var newTreeNode = {};
		//treeNode.node = (treeNode.childNode) ? obj.countPrevSiblings() : obj.countPrevSiblings()-3; //the lowest level should -1, because the element is not created at that moment		
		var numPrevSiblings = obj.countPrevSiblings();
		
		treeNode.node = (treeNode.childNode) ? numPrevSiblings : numPrevSiblings-1; //the lowest level should -1, because the element is not created at that moment
		newTreeNode.childNode = treeNode;
		
		return obj.parentNode.traverseTop(newTreeNode);
	}
	else
	{
		treeNode.node = obj.countPrevSiblings();
		return treeNode;
	}
}

Node.prototype.getNode = function (tree)
{
	var obj = this;
//	console.log(obj.childNodes);	
//	console.log(tree);	
	
	if (tree.childNode)
		return obj.childNodes[tree.node].getNode(tree.childNode);
	else
		return obj.childNodes[tree.node];
}	