<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
intranet_auth();
intranet_opendb();

$bid = $_REQUEST['bid'];

$ebook_root_path = $intranet_root."/file/eBook/".$bid."/";
//$ebook_http_root_path = "/file/eBook/".$bid."/";

//$request_url = substr($_SERVER['REQUEST_URI'],0,strrpos($_SERVER['REQUEST_URI'], '/',0));
$request_url = $_SERVER['REQUEST_URI'];
if($request_url[strlen($request_url)-1] != '/'){
	$request_url .= '/';
}

$libebookreader = new libebookreader();

$parse_success = $libebookreader->Parse_EPUB_XML($ebook_root_path);

if(!$parse_success){
	echo "Fail to parse ebook.";
	intranet_closedb();
	exit;
}

$ebook_doc_path = $libebookreader->Encrypt($libebookreader->ebook_doc_path,$libebookreader->ENCRYPT_KEY);
$ebook_subfolder_path = $libebookreader->Encrypt($libebookreader->ebook_subfolder_path, $libebookreader->ENCRYPT_KEY);
$ebook_http_root_path = $libebookreader->Encrypt("/file/eBook/".$bid."/", $libebookreader->ENCRYPT_KEY);

$js = 'var bookdata = {';
$js .= "'id' : '".$bid."',\n";
$js .= '\'item\' : {},'."\n";
$js .= '\'spine\' : [],'."\n";
$js .= '\'doc_root\' : \''.$ebook_doc_path.'\','."\n";
$js .= '\'http_root\' : \''.$ebook_http_root_path.'\','."\n";
$js .= '\'subfolder\' : \''.$ebook_subfolder_path.'\''."\n";
$js .= '};'."\n";

$pages = '';
$buttons = '&nbsp;[<a href="javascript:jsChangeView(2);">Two Column View</a>] | [<a href="javascript:jsChangeView(1);">Scroll View</a>]';

$items = $libebookreader->opf_items;
$spines = $libebookreader->opf_spine;

if(count($items)>0){
	foreach($items as $key => $val){
		$js .= "bookdata.item['".$key."'] = {'id':'".$val['id']."', 'href':'".$val['href']."', 'media-type':'".$val['media-type']."'};\n";
	}
}
$pages = '';
$delimiter = '';
if(count($spines)>0){
	foreach($spines as $key => $val){
		$js .= "bookdata.spine.push({'idref':'".$val['idref']."'});\n";
		
		$pages .= $delimiter.'<a href="javascript:jsLoadDocument('.$key.');" title="Go to page '.$key.'">'.$key.'</a>';
		$delimiter = ' | ';
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<header>
	<meta charset="UTF-8" content="text/html" http-equiv="Content-Type">
	<script src="/home/eLearning/ebook_reader/jquery-1.5.1.min.js" language="JavaScript" ></script>
	<script src="/home/eLearning/ebook_reader/jquery-ui-1.8.11.custom.min.js" language="JavaScript" ></script>
	<script src="/home/eLearning/ebook_reader/jquery.highlight-3.js" language="JavaScript" ></script>
	<style type=text/css>
		#divBook {
			position: absolute;
			width: 1024px;
			height: 768px;
			left: 10px;
			overflow: hidden;
			border: #000000 1px solid;
			padding: 20px;
		} 
		
		.multicolumnElement {
		    -moz-column-count: 2;
			-moz-column-width: 200px;
			-moz-column-gap: 30px;
			-webkit-column-count: 2;
			-webkit-column-width: 200px;
			-webkit-column-gap: 30px;
			height: 768px;
			position: relative; 
		}
		
		.highlight {
			background: #FFFF00;
		} 
	</style>
</header>
<body>
<script language="JavaScript">
<?=$js?>

function jsLoadDocument(i)
{
	var docpath = '';
	if(typeof(bookdata.spine[i]) == 'undefined'){
		return;
	}else if(typeof(bookdata.item[bookdata.spine[i].idref])!='undefined'){
		docpath = bookdata.item[bookdata.spine[i].idref].href;
		$('div#Content').load(
			'<?=$request_url?>' + docpath,
			{
				
			},
			function(data){
				init();
			}
		);
	}
}

function jsChangeView(mode)
{
	var $dc = $('div#Content');
	if(mode==2){
		if(!$dc.hasClass('multicolumnElement'))
			$dc.addClass('multicolumnElement');
	}else{
		$dc.removeClass('multicolumnElement');
	}
}

var ebook = {
	currentPage: 1,
	totalPage: 3,
	pageWidth: 1024,
	movementTolerance: 30,
	tableOfContent: null,

	initialize: function()
	{
		$.ajax({
			type: "GET",
			url: "column_count.htm",
			dataType: "xml",
			success: function(xml) {
				this.tableOfContent = xml; //...
			}
		});
	},

	moveWithinTolerance: function(start, stop)
	{
		return Math.abs(stop - start) <= this.movementTolerance;
	},

	movePage: function(start, stop)
	{
		if ((stop < start) && (this.currentPage + 1 > this.totalPage)) return false;
		if ((stop > start) && (this.currentPage - 1 < 1)) return false;

		if (this.moveWithinTolerance(start, stop))
		{
			return false;
		}
		else
		{
			this.currentPage += (stop < start ? 1 : -1);
			return true;
		}
	}
};

var init = function(){
 
 $("div#Content").draggable({
 	axis: "x",
 	start: function(event, ui) {
 		start = ui.position.left;
 	},
 	stop: function(event, ui) {
 		stop = ui.position.left;

 		if (ebook.movePage(start, stop))
 			$(this).animate({left: "+="+((ebook.pageWidth-Math.abs(stop-start))*(stop>start?1:-1))}, "slow");
 		else
 			$(this).animate({left: "-="+(stop-start)}, "fast");
	}
 });
 var contentObj = document.getElementById('Content'); 
}; 

$(document).ready(function(){
	jsLoadDocument(0);
});
</script>
<div id="divBook">
	<div id="Content" class="multicolumnElement ui-draggable"></div>
</div>
<div id="divNavigator" style="position: fixed; left : 0px; top: 816px; background: none repeat scroll 0% 0% rgb(204, 204, 204);"><?=$pages.$buttons?></div>
</body>
</html>
<?
intranet_closedb();
?>