<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT.'lang/ebook_reader_lang.'.$intranet_session_language.'.php');

intranet_auth();
intranet_opendb();

$task = $_REQUEST['task'];

$libebookreader = new libebookreader();
switch($task)
{	
	case "AddHighlight":
		$DataArray = array();
		$DataArray[0]['BookID'] = $_REQUEST['BookID'];
		$DataArray[0]['RefID'] = urldecode($_REQUEST['RefID']);
		$DataArray[0]['UniqueID'] = $_REQUEST['UniqueID'];
		$DataArray[0]['Highlight'] = $_REQUEST['Highlight'];
		//$DataArray[0]['Notes'] = urldecode($_REQUEST['Notes']);
		
		$success = $libebookreader->Update_Book_User_Highlight_Notes($DataArray);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['AddHighlightSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['AddHighlightFail'];
		}
	break;
	
	case "DeleteHighlight":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		//$RefID = $_REQUEST['RefID'];
		$UniqueID = $_REQUEST['UniqueID'];
		$success = $libebookreader->Delete_Book_User_Highlight_Notes($BookID,$_UserID,'',$UniqueID);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['DeleteHighlightSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['DeleteHighlightFail'];
		}
	break;
	
	case "UpdateAllHighlights":
		$DataArray = array();
		$BookID = $_REQUEST['BookID'];
		$RefID = urldecode($_REQUEST['RefID']);
		$UniqueID = $_REQUEST['UniqueID'];
		$Highlight = $_REQUEST['Highlight'];
		
		for($i=0;$i<count($UniqueID);$i++){
			$DataArray[$i]['BookID'] = $BookID;
			$DataArray[$i]['RefID'] = $RefID;
			$DataArray[$i]['UniqueID'] = $UniqueID[$i];
			$DataArray[$i]['Highlight'] = $Highlight[$i];
		}
		
		$success = $libebookreader->Update_Book_User_Highlight_Notes($DataArray);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['AddHighlightSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['AddHighlightFail'];
		}
	break;
	
	case "UpdateHighlightNotesByID":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$UniqueID = $_REQUEST['UniqueID'];
		$HighlightNotes = $_REQUEST['HighlightNotes'];
		$success = $libebookreader->Update_Book_User_Highlight_Notes_By_ID($BookID, $_UserID, $UniqueID, $HighlightNotes);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['UpdateHighlightNotesSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['UpdateHighlightNotesFail'];
		}
	break;
	
	case "AddBookmark":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$PageMode = $_REQUEST['PageMode'];
		$PageNum = $_REQUEST['PageNum'];
		
		$success = $libebookreader->Add_Book_User_Bookmark($BookID, $_UserID, $PageMode, $PageNum);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['AddBookmarkSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['AddBookmarkFail'];
		}
	break;
	
	case "DeleteBookmark":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$PageMode = $_REQUEST['PageMode'];
		$PageNum = $_REQUEST['PageNum'];
		
		$success = $libebookreader->Delete_Book_User_Bookmark($BookID, $_UserID, $PageMode, $PageNum);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['DeleteBookmarkSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['DeleteBookmarkFail'];
		}
	break;
	
	case "GetBookNotes":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$PageMode = $_REQUEST['PageMode'];
		$PageNum = $_REQUEST['PageNum'];
		
		$data = $libebookreader->Get_Book_User_Notes($BookID, $_UserID, $PageMode, $PageNum);
		
		$datetime = trim($data[0]['DateModified'])==''?'':$libebookreader->FormatDatetime($data[0]['DateModified']);
		
		$x = "{";
		$x.= "'Notes': '".rawurlencode($data[0]['Notes'])."', ";
		$x.= "'DateModified' : '".$datetime."' ";
		$x.= "}";
		echo $x;
	break;
	
	case "UpdateBookNotes":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$PageMode = $_REQUEST['PageMode'];
		$PageNum = $_REQUEST['PageNum'];
		$Notes = stripslashes(urldecode($_REQUEST['Notes']));
		
		$success = $libebookreader->Update_Book_User_Notes($BookID, $_UserID, $PageMode, $PageNum, $Notes);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['AddNotesSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['AddNotesFail'];
		}
	break;
	
	case "DeleteBookNotes":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$PageMode = $_REQUEST['PageMode'];
		$PageNum = $_REQUEST['PageNum'];
		
		$success = $libebookreader->Delete_Book_User_Notes($BookID, $_UserID, $PageMode, $PageNum);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['DeleteNotesSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['DeleteNotesFail'];
		}
	break;
	
	case "UpdateBookNotesByRecordID":
		$RecordID = $_REQUEST['RecordID'];
		$Notes =  trim(urldecode($_REQUEST['Notes']));
		
		$success = $libebookreader->Update_Book_User_Notes_By_RecordID($RecordID, $Notes);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['AddNotesSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['AddNotesFail'];
		}
	break;
	
	case "DeleteBookNotesByRecordID":
		$RecordID = $_REQUEST['RecordID'];
		
		$success = $libebookreader->Delete_Book_User_Notes_By_RecordID($RecordID);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['DeleteNotesSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['DeleteNotesFail'];
		}
	break;
	
	case "Update_User_Progress":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$ProgressStatus = $_REQUEST['ProgressStatus'];
		$Percentage = $_REQUEST['Percentage'];
		if($ProgressStatus == 1){
			$Percentage = '';
		}
		$success = $libebookreader->Update_User_Process($BookID, $_UserID, $ProgressStatus, $Percentage);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['UpdateReadingProgressSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['UpdateReadingProgressFail'];
		}
	break;
	
	case "Get_User_Progress":
		$pBookID = $_REQUEST['BookID'];
		$pUserID = $_SESSION['UserID'];
		$result = $libebookreader->Get_User_Progress($pBookID, $pUserID);
		$json = "{";
		$json.= "'ProgressStatus':'".$result[0]['ProgressStatus']."',";
		$json.= "'Percentage':'".$result[0]['Percentage']."',";
		$json.= "'DateModified':'".$result[0]['DateModified']."'";
		$json.= "}";
		echo $json;
	break;
	
	case "Send_Recommend_Email":
		$To = rawurldecode($_REQUEST['To']);
		$Content = OsCommandSafe(rawurldecode($_REQUEST['Content']));
		$BookID = $_REQUEST['BookID'];
		
		$success = $libebookreader->Send_Recommend_Email($To,$Content,$BookID);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['SendRecommendEmailSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['SendRecommendEmailFail'];
		}
	break;
	
	case "Add_Book_Review":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$Rating = $_REQUEST['Rating'];
		$Content = OsCommandSafe(trim(rawurldecode($_REQUEST['Content'])));
		
		$success = $libebookreader->Add_Book_Review($BookID,$_UserID,$Rating, $Content);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['AddBookReviewSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['AddBookReviewFail'];
		}
	break;
	
	case "Add_Book_Review_Helpful":
		$BookID = $_REQUEST['BookID'];
		$_UserID = $_SESSION['UserID'];
		$ReviewID = $_REQUEST['ReviewID'];
		$Choose = $_REQUEST['Choose'];
		
		$success = $libebookreader->Add_Book_Review_Helpful($BookID,$_UserID,$ReviewID,$Choose);
		if($success){
			echo $Lang['eBookReader']['ReturnMsg']['VoteReviewHelpfulSuccess'];
		}else{
			echo $Lang['eBookReader']['ReturnMsg']['VoteReviewHelpfulFail'];
		}
	break;
	
	case "Switch_Page_Mode":
		$_SESSION['eBookReaderPageMode'] = $_REQUEST['PageMode'];
		if($_REQUEST['BookID'] && $_REQUEST['LastViewPageNumber'] != ''){
			$_SESSION['eBookReaderLastViewPageNumber'] = $libebookreader->Get_PageNumber_Mapping_By_PageMode($_REQUEST['BookID'],$_REQUEST['PageMode'],$_REQUEST['LastViewPageNumber'],$_REQUEST['IsImageBook']==1);
			$_SESSION['eBookReaderLastViewShowTableOfContent'] = $_REQUEST['ShowTableOfContent'];
		}
	break;
	
	case "Switch_Paper":
		$_SESSION['eBookReaderPaperStyle'] = $_REQUEST['PaperStyle']; // white or yellow
		if($_REQUEST['LastViewPageNumber'] != ''){
			$_SESSION['eBookReaderLastViewPageNumber'] = $_REQUEST['LastViewPageNumber'];
			$_SESSION['eBookReaderLastViewShowTableOfContent'] = $_REQUEST['ShowTableOfContent'];
		}
	break;
	
	case "Get_iFrame_Tag":
		$src = trim(rawurldecode($_REQUEST['src']));
		$id = trim(rawurldecode($_REQUEST['id']));
		$width = $_REQUEST['width'];
		$height = $_REQUEST['height'];
		echo $libebookreader->Get_iFrame_Tag($src,$id,$width,$height);
	break;
}


intranet_closedb();
?>
