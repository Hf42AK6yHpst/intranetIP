<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];
$BookID			  = isset($BookID) && is_array($BookID)? $BookID : array();

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);
$objiTextbook->admin_allow_create_book($Current_Code);

if(count($BookID)){
	# Check if any student using the selected book
	$sql = "SELECT
				COUNT(*)
			FROM
				ITEXTBOOK_DISTRIBUTION AS id INNER JOIN
				ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID
			WHERE
				id.BookID IN (".(implode(',', $BookID)).")";
	$std_no_with_books = current($objiTextbook->returnVector($sql));
	
	if($std_no_with_books==0){
		$sql = "UPDATE
					ITEXTBOOK_BOOK
				SET
					Status = ".$iTextbook_cfg['db_setting']['book']['status']['deleted'].",
					DateModified = NOW(),
					ModifiedBy = $UserID
				WHERE
					BookID IN (".(implode(',', $BookID)).")";
		$result_msg = $objiTextbook->db_db_query($sql)? "delete" : "delete_failed";
	}
	else{
		$result_msg = "delete_failed";
	}
}
else
	$result_msg = "delete_failed";

header("location: book_list.php?Current_ModuleID=$Current_ModuleID&msg=$result_msg");
?>