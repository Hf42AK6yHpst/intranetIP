<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];
$BookID			  = isset($BookID) && $BookID!=''? $BookID : 0; 
$IsSelected		  = isset($IsSelected) && is_array($IsSelected)? $IsSelected : array();
$Order		  	  = isset($Order) && is_array($Order)? $Order : array();
$special_unit	  = isset($special_unit) && is_array($special_unit)? $special_unit : array();

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);
$objiTextbook->admin_allow_create_book($Current_Code);

# Process Special Chapters
for($i=0;$i<count($special_unit);$i++){
	$Chapters .= ($Chapters? $iTextbook_cfg['db_setting']['book']['chapter']['dilimeter'] : "").$special_unit[$i];
}

# Process Selected Chapters
for($i=0;$i<count($Order);$i++){
	$chpater_no = $Order[$i];
	if($IsSelected[$chpater_no]==1){
		$Chapters .= ($Chapters? $iTextbook_cfg['db_setting']['book']['chapter']['dilimeter'] : "").$chpater_no;
	}
}

# Checking of the selected chapter is valid or not
# [If students are assigned to book, only order of the unit can be changed]
# Get Selected Book Info
if($BookID){
	$BookInfo = $objiTextbook->get_book_info($BookID, true);
	$IsError  = false;
	
	if($BookInfo['UserCount'] > 0){
		$chk_chapter_ary = explode($iTextbook_cfg['db_setting']['book']['chapter']['dilimeter'], $Chapters);
		
		if(count($BookInfo['Chapters'])!=count($chk_chapter_ary))
			die('Selected Chapters are invalid! [Err code: 001]');
		
		for($i=0;$i<count($BookInfo['Chapters']);$i++){
			if(!in_array($BookInfo['Chapters'][$i], $chk_chapter_ary))
				die('Selected Chapters are invalid! [Err code : 002]');
		}
	}
}

if($BookID){
	$sql = "UPDATE
				ITEXTBOOK_BOOK
			SET
				BookName = '$BookName',
				BookLang = '$BookLang',
				Chapters = '$Chapters',
				DateModified = NOW(),
				ModifiedBy = '$UserID'
			WHERE
				BookID = $BookID";
	$result_msg = $objiTextbook->db_db_query($sql)? "update" : "update_failed";
}
else{
	$sql = "INSERT INTO
				ITEXTBOOK_BOOK
				(BookType, BookName, BookLang, Chapters, Status, DateInput, DateModified, ModifiedBy)
			VALUES
				('".$iTextbook_cfg[$Current_Code]['module_code']."', '$BookName', '$BookLang', '$Chapters', '".$iTextbook_cfg['db_setting']['book']['status']['not_deleted']."', NOW(), NOW(), '$UserID')";
	$result_msg = $objiTextbook->db_db_query($sql)? "add" : "add_failed";
	$BookID 	= $objiTextbook->db_insert_id();
}

header("location: book_list_add.php?Current_ModuleID=$Current_ModuleID&BookID=$BookID&msg=$result_msg");
?>