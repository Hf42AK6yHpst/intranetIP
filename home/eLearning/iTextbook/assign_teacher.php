<?php
//modifying By Thomas
if ($page_size_change == 1){
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('ies_admin_default.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# Get iTextbook Special Room course_id
$sql = "SELECT course_id FROM $eclass_db.course WHERE course_code = '$Current_Code' AND RoomType = 7";
$course_id = current($objiTextbook->returnVector($sql));

$sql = "SELECT
			".getNameFieldWithClassNumberByLang("iu.")." as UserName,
			CONCAT('<input type=\'checkbox\' id=\'teacher_user_id_', uc.user_id, '\' name=\'teacher_user_id[]\' value=\'', uc.user_id, '\'>')
		FROM
	    	$intranet_db.INTRANET_USER AS iu INNER JOIN
			$eclass_db.user_course AS uc ON iu.UserEmail = uc.user_email  
		WHERE
			iu.RecordType = ".USERTYPE_STAFF." AND
			iu.RecordStatus IN (0, 1, 2) AND
			uc.course_id = $course_id";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = array("UserName");
$li->no_col = count($li->field_array) + 2;
$li->column_array = array_fill(0, $li->no_col, 0);

# TABLE COLUMN
$li->column_list .= "<th width='3%' class='tabletop tabletopnolink' style='vertical-align:middle'>&nbsp;#&nbsp;</th>\n";
$li->column_list .= "<th width='*' class='tabletop' style='vertical-align:middle'>".$li->column(0, $Lang['itextbook']['TeachersName'])."</th>";
$li->column_list .= "<th width='3%' class='tabletop' style='vertical-align:middle'>".$li->check("teacher_user_id[]")."</th>\n";

# Generate Page navigation array
$page_ary[] = array("display"=>$Lang['itextbook']['AssignTeacherInCharge'], "link"=>"javascript:go_assign_teacher()");

# Generate System Message after operation
$html_message = isset($msg)? $linterface->GET_SYS_MSG($msg) : "";

# Generate UI
$title = $iTextbook_cfg[$Current_Code]['book_name'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objiTextbook->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script>
function go_assign_teacher(){
	var obj = document.header_form;
	
	obj.action = 'assign_teacher.php';
	obj.submit();
}

function add_teacher(){
	newWindow('import_teacher.php?Current_ModuleID=<?=$Current_ModuleID?>', 2);
}

function remove_teacher(){
	if(countChecked(document.form1, 'teacher_user_id[]')==0){
		alert(globalAlertMsg2);
		return;
	}
	
	if(confirm("<?=$Lang['itextbook']['RemoveAssignTeacherConfirm']?>")){
		var AssignedTeacher_ary = new Array();
		$('[name=teacher_user_id[]]:checked').each(function(id){
			AssignedTeacher_ary.push($(this).val());
		});
		
		$.post(
			"ajax.php",
			{
				type			 		 : 'Delete_Teacher',
				Current_ModuleID 		 : '<?=$Current_ModuleID?>',
				'AssignedTeacher_ary[]'  : AssignedTeacher_ary
			},
			function(data){
				document.form1.submit();
			}
		);
	}
}
</script>
<table width="100%">
	<tr><td><?=$objiTextbook->gen_tab_menu(3)?></td></tr>
	<tr><td><?=$objiTextbook->gen_admin_nav($page_ary)?></td></tr>
	<tr>
		<td>
			<form name="form1" method="POST">
				<div class="content_top_tool">
  					<div style="float:right">
  						<?=$html_message?>
  					</div>
				</div>
				<div class="content_top_tool">
  					<div class="Conntent_tool">
  						<a title="Import" href="javascript:add_teacher()"><?=$Lang['itextbook']['AddTeacher']?></a>
  					</div>
				</div>
				<div class="table_board">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td align="right">
								<table border="0" cellpadding="0" cellspacing="0">
          							<tr>
            							<td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
            							<td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
              								<table border="0" cellpadding="0" cellspacing="2">
												<tr>
                    								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
                    								<td nowrap="nowrap"><a class="tabletool" href="javascript:void(0)" onclick="remove_teacher()"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" ><?=$ec_iPortfolio['delete']?></a></td>
                  								</tr>
              								</table>
            							</td>
            							<td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
          							</tr>
        						</table>
							</td>
						</tr>
						<tr>
							<td><?=$li->display();?></td>
						</tr>
					</table>
					<br />
			  		<p class="spacer"></p>
				</div>
			  	<input type="hidden" name="pageNo" value="<?=$li->pageNo?>"/>
			  	<input type="hidden" name="order" value="<?=$li->order?>"/>
			  	<input type="hidden" name="field" value="<?=$li->field?>"/>
			  	<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
			  	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
				<input type="hidden" name="Current_ModuleID" value="<?=$Current_ModuleID?>"/>
			</form>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>