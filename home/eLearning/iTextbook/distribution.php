<?php
//modifying By Siuwan
if ($page_size_change == 1){
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('ies_admin_default.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);

# Calculating License left for this purchase record 
$quota_left  = $objiTextbook->get_distributions_quota_left($ModuleLicenseID, $Current_Code);
$total_quota  = $objiTextbook->get_distributions_total_quota($ModuleLicenseID);
# Get No of Books Created
$no_of_book	 = $objiTextbook->get_books_info($iTextbook_cfg[$Current_Code]['module_code']);

# Generate System Message
$html_message   = $msg? $linterface->GET_SYS_MSG($msg) : "&nbsp;";

# Generate Distribution tables
$Distribution_user_info   = $objiTextbook->get_distributions_user_info($ModuleLicenseID);
$distribution_table_board = count($Distribution_user_info)? $objiTextbook->gen_distribution_info_table($Current_Code, 1, $Distribution_user_info) : ""; 

# Generate Page navigation array
$page_ary[] = array("display"=>$Lang['itextbook']['PurchaseRecord'], "link"=>"javascript:go_purchase_record()");
$page_ary[] = array("display"=>$Lang['itextbook']['DistributionStatus'], "link"=>"");

# Generate UI
$title = $iTextbook_cfg[$Current_Code]['book_name'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objiTextbook->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script>
var current_distribution_no = <?=count($Distribution_user_info)?>;
var no_of_book = <?=$no_of_book?>;

function go_purchase_record(){
	var obj = document.header_form;
	
	obj.action = 'index.php';
	obj.submit();
}

function add_student(DistributionID, DistributionNo){
	if(document.form1.LicenseLeft.value > 0)
		newWindow('import.php?DistributionID=' + DistributionID + '&DistributionNo=' + DistributionNo, 2);
	else
		alert('<?=$Lang['itextbook']['NoMoreLicenseLeft']?>');
}

function add_distribution(){
	if(no_of_book==0){
		$.post(
				"ajax.php",
				{
					type			 : 'Get_No_of_Books',
					Current_ModuleID : '<?=$Current_ModuleID?>'
				},
				function(data){
					no_of_book = parseInt(data);
				}
		);
	}
	
	if(no_of_book){
		if(document.form1.LicenseLeft.value > 0){
			if(confirm("<?=$Lang['itextbook']['CreateDistributionConfirm']?>")){
				$.post(
					"ajax.php",
					{
						type			 : 'Get_New_Distribution_Table',
						ModuleLicenseID  : '<?=$ModuleLicenseID?>',
						Current_ModuleID : '<?=$Current_ModuleID?>',
						DistributionNo	 : (current_distribution_no + 1)
					},
					function(data){
						var result 		= parseInt(data.split('|=|')[0]);
						var result_data = data.split('|=|')[1];
						
						if(result){
							if(current_distribution_no==0){
								$('#distribution_table_board').html(result_data);
							}
							else{
								$('#Distribution_' + current_distribution_no).after(result_data);
							}
							current_distribution_no++;
						}
						else{
							alert(result_data);
						}
					}
				);
			}
		}
		else
			alert('<?=$Lang['itextbook']['NoMoreLicenseLeft']?>');
	}
	else{
		alert('<?=$Lang['itextbook']['NoBookCreated']?>');
	}
}

function delete_distribution(DistributionID, DistributionNo){
	if(confirm("<?=$Lang['itextbook']['DeleteDistributionConfirm']?>")){
		$.post(
			"ajax.php",
			{
				type			 : 'Delete_Distribution',
				ModuleLicenseID  : '<?=$ModuleLicenseID?>',
				Current_ModuleID : '<?=$Current_ModuleID?>',
				DistributionID	 : DistributionID
			},
			function(data){
				if(data){
					$('#sys_msg_' + DistributionNo).html(data);
				}
				else{
					document.form1.action = 'distribution.php?msg=delete';
					document.form1.submit();
				}
			}
		);
	}
}

function update_book(DistributionID, DistributionNo, BookID){
	if($('#sys_msg_' + DistributionNo).html())
		eval('SysMsgTimerObj_' + DistributionNo + '.ClearTimer()');
	
	$.post(
		"ajax.php",
		{
			type			 : 'Update_Distribution_Table_Book',
			ModuleLicenseID  : '<?=$ModuleLicenseID?>',
			Current_ModuleID : '<?=$Current_ModuleID?>',
			DistributionID	 : DistributionID, 
			DistributionNo	 : DistributionNo,
			BookID			 : BookID
		},
		function(data){
			$('#sys_msg_' + DistributionNo).html(data);
		}
	);
}

function reload_distribution(DistributionID, DistributionNo, sys_msg){
	$.post(
		"ajax.php",
		{
			type			 : 'Reload_Distribution',
			ModuleLicenseID  : '<?=$ModuleLicenseID?>',
			Current_ModuleID : '<?=$Current_ModuleID?>',
			DistributionID	 : DistributionID, 
			DistributionNo	 : DistributionNo,
			sys_msg			 : sys_msg
		},
		function(data){
			document.form1.LicenseLeft.value = data.split('|=|')[0];
			$('#LicenseLeft').html(data.split('|=|')[0]);
			$('#Distribution_' + DistributionNo).html(data.split('|=|')[1]);
		}
	);
}

function SysMsgTimerObj(ObjName, DistributionNo, WaitingTime){
	this.DistributionNo = DistributionNo;
	this.WaitingTime    = WaitingTime;
	this.TimerID		= '';
	
	this.StartTimer		= function(){
							if(this.WaitingTime>0){
								this.WaitingTime--;
								this.TimerID = setTimeout(ObjName + ".StartTimer()",1000);
							}
							else{
								this.ClearTimer();
							}
						  }
	this.ClearTimer		= function(){
							clearTimeout(this.TimerID);
							$('#sys_msg_' + this.DistributionNo).html('');
						  }
}

function delete_distribution_user(DistributionID, DistributionNo){
	if(countChecked(document.form1, 'row_' + DistributionNo + '_[]')==0){
		alert(globalAlertMsg2);
		return;
	}
	
	if(confirm("<?=$Lang['itextbook']['DeleteDistributionUserConfirm']?>")){
		var DistributionUser_ary = new Array();
		$('[name=row_' + DistributionNo + '_[]]:checked').each(function(id){
			DistributionUser_ary.push($(this).val());
		});
		
		$.post(
			"ajax.php",
			{
				type			 		 : 'Delete_Distribution_User',
				ModuleLicenseID  		 : '<?=$ModuleLicenseID?>',
				Current_ModuleID 		 : '<?=$Current_ModuleID?>',
				DistributionID	 		 : DistributionID, 
				DistributionNo	 		 : DistributionNo,
				'DistributionUser_ary[]' : DistributionUser_ary
			},
			function(data){
				document.form1.LicenseLeft.value = data.split('|=|')[0];
				$('#LicenseLeft').html(data.split('|=|')[0]);
				$('#Distribution_' + DistributionNo).html(data.split('|=|')[1]);
			}
		);
	}
}

function check_all_row(LR, DistributionNo){
	var all_check = $('#' + LR + '_check_all_' + DistributionNo).attr('checked')? 'checked' : '';
	$('.' + LR + '_row_' + DistributionNo).attr('checked', all_check);
}
</script>
<table width="100%">
	<tr><td><?=$objiTextbook->gen_tab_menu(1)?></td></tr>
	<tr><td><?=$objiTextbook->gen_admin_nav($page_ary)?></td></tr>
	<tr>
		<td>
			<form id="form1" name="form1" method="POST">
				<div class="content_top_tool">
				  	<div style="float:right">
				  		<?=$html_message?>
				  	</div>
				</div>
				<div class="table_board">
					<table width="100%">
					<?if($total_quota!=$iTextbook_cfg['LicenseSettingAry']['UnlimitQuota']){?>
						<tr>
							<td>
								<?=$Lang['itextbook']['LicenseLeft']?> : <span id="LicenseLeft"><?=$quota_left?></span>
							</td>
						</tr>
					<?}?>	
						<?php if($iTextbook_cfg[$Current_Code]['LicenseType']['HaveExpiryDate']){
								$expiryDate = $objiTextbook->get_license_expiry_date($ModuleLicenseID);	
								if(!empty($expiryDate)){
						?>
						<tr>
							<td>
								<?=$Lang['itextbook']['ExpiryDate']?> : <?=date('Y-m-d', strtotime($expiryDate))?>
							</td>
						</tr>
							<?php } ?>
						<?php } ?>
						<tr>
							<td>
								<fieldset class="instruction_box">
									<legend class="instruction_title"><?=$Lang['itextbook']['Instruction']?></legend>
									<span>
										<ul>
											<li><?=$Lang['itextbook']['DistributionInstruction'][0]?></li>
											<li><?=$Lang['itextbook']['DistributionInstruction'][1][0]?><?=$iTextbook_cfg[$Current_Code]['cooldown']?><?=$Lang['itextbook']['DistributionInstruction'][1][1]?></li>
											<li><?=$Lang['itextbook']['DistributionInstruction'][2]?></li>
										</ul>
									</span>
								</fieldset>
					  		</td>
						</tr>
						<tr>
							<td align="center">
								<div id="distribution_table_board" class="table_board">
									<?=$distribution_table_board?>
								</div>
								<br /><p class=\"spacer\"></p>
								<div class="content_top_tool">
				  					<div class="Conntent_tool">
				  						<a title="<?=$Lang['itextbook']['AddDistribution']?>" href="javascript:add_distribution()"><?=$Lang['itextbook']['AddDistribution']?></a>
				  					</div>
								</div>
							</td>
						</tr>
					</table>
			  		<p class="spacer"></p>
				</div>
				<input type="hidden" name="ModuleLicenseID" value="<?=$ModuleLicenseID?>"/>
				<input type="hidden" name="Current_ModuleID" value="<?=$Current_ModuleID?>"/>
				<input type="hidden" name="LicenseLeft" value="<?=$quota_left?>"/>
			</form>
		</td>
	</tr>
	<tr>
		<td>
			<div class="edit_bottom">
				<p class="spacer"></p>
   	 				<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="go_purchase_record()"/>
  				<p class="spacer"></p>
  			</div>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>