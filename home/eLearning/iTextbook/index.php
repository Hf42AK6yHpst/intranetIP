<?php
//modifying By Siuwan
if ($page_size_change == 1){
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('ies_admin_default.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];

$field	= isset($field) && $field!=''? $field : 0;
$order	= isset($order) && $order!=''? $order : 0;
$pageNo	= isset($pageNo) && $pageNo!=''? $pageNo : 1;

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);
//$total_quota  = $objiTextbook->get_iTextbook_total_quota($Current_ModuleID);
//$quota_used	  = $objiTextbook->get_iTextbook_total_used($Current_ModuleID, false);
//$quota_left	  = $total_quota - $quota_used;

$objiTextbook->admin_auth($Current_ModuleID);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$sql = "SELECT
			iml.InputDate,
			".($iTextbook_cfg[$Current_Code]['LicenseType']['ChapterStudent']? "iml.NumberOfUnit,":"")."
			".($iTextbook_cfg[$Current_Code]['LicenseType']['ChapterStudent']? "iml.NumberOfStudents,":"")."
			".($iTextbook_cfg[$Current_Code]['LicenseType']['HaveExpiryDate']? "DATE_FORMAT(iml.ExpiryDate, '%Y-%m-%d'),":"")."
			IF(iml.NumberOfLicense=".$iTextbook_cfg['LicenseSettingAry']['UnlimitQuota'].", '".$Lang['itextbook']['Unlimited']."',CONCAT((iml.NumberOfLicense - IFNULL(tmp_d.UserNo, '0')), ' / ', iml.NumberOfLicense)) AS user_quota_left,
			CONCAT('<a href=\'javascript:go_distribution(', iml.ModuleLicenseID, ')\'>".$Lang['itextbook']['Edit']."</a>') AS distribution_link,
			(iml.NumberOfLicense - IFNULL(tmp_d.UserNo, '0')) AS UserNo_ordering
		FROM
			INTRANET_MODULE_LICENSE AS iml LEFT JOIN
			(
				SELECT
					id.ModuleLicenseID AS ModuleLicenseID,
					IFNULL(COUNT(*)*".$iTextbook_cfg[$Current_Code]['MaxChapters'].", '0') AS UserNo
				FROM
					ITEXTBOOK_DISTRIBUTION AS id INNER JOIN
					ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID
				GROUP BY
					id.ModuleLicenseID
			) AS tmp_d ON iml.ModuleLicenseID = tmp_d.ModuleLicenseID
		WHERE
			iml.ModuleID = $Current_ModuleID";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array[] = "InputDate";
if($iTextbook_cfg[$Current_Code]['LicenseType']['ChapterStudent']){
	$li->field_array[] = "NumberOfUnit";
	$li->field_array[] = "NumberOfStudents";
}
if($iTextbook_cfg[$Current_Code]['LicenseType']['HaveExpiryDate']){
	$li->field_array[] = "ExpiryDate";
}
$li->field_array[] = "user_quota_left";
$li->field_array[] = "distribution_link";
$li->field_array[] = "UserNo_ordering";

$li->no_col = count($li->field_array);
$li->column_array = array_fill(0, $li->no_col, 0);

# TABLE COLUMN
$col_cnt=0;
$li->column_list .= "<th width='5%' class='tabletop tabletopnolink' style='vertical-align:middle'>&nbsp;#&nbsp;</th>\n";
$li->column_list .= "<th width='*' class='tabletop' style='vertical-align:middle'>".$li->column($col_cnt++, $Lang['itextbook']['PurchaseDate'])."</th>";
$li->column_list .= $iTextbook_cfg[$Current_Code]['LicenseType']['ChapterStudent']? "<th width='16%' class='tabletop' style='vertical-align:middle'>".$li->column($col_cnt++, $Lang['itextbook']['NoOfUnits'])."</th>":"";
$li->column_list .= $iTextbook_cfg[$Current_Code]['LicenseType']['ChapterStudent']? "<th width='16%' class='tabletop' style='vertical-align:middle'>".$li->column($col_cnt++, $Lang['itextbook']['NoOfStudents'])."</th>":"";
$li->column_list .= $iTextbook_cfg[$Current_Code]['LicenseType']['HaveExpiryDate']? "<th width='16%' claas='tabletop' style='vertical-align:middle'>".$li->column($col_cnt++, $Lang['itextbook']['ExpiryDate'])."</th>":"";
$li->column_list .= "<th width='16%' claas='tabletop' style='vertical-align:middle'>".$li->column($col_cnt+=2, $Lang['itextbook']['LicenseLeft'])."</th>";
$li->column_list .= "<th width='10%' claas='tabletop' style='vertical-align:middle'>".$Lang['itextbook']['DistributionStatus']."</th>";

# Generate Page navigation array
$page_ary[] = array("display"=>$Lang['itextbook']['PurchaseRecord'], "link"=>"javascript:go_purchase_record()");

# Generate UI
$title = $iTextbook_cfg[$Current_Code]['book_name'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objiTextbook->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script>
function go_purchase_record(){
	var obj = document.header_form;
	
	obj.action = 'index.php';
	obj.submit();
}

function go_distribution(ModuleLicenseID){
	var obj = document.form1;
	
	obj.ModuleLicenseID.value = ModuleLicenseID;
	obj.action = 'distribution.php';
	obj.submit();
}
</script>
<table width="100%">
	<tr><td><?=$objiTextbook->gen_tab_menu(1)?></td></tr>
	<tr><td><?=$objiTextbook->gen_admin_nav($page_ary)?></td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<form name="form1" method="POST" action="index.php">
				<div class="table_board">
					<?=$li->display();?>
					<br />
			  		<p class="spacer"></p>
				</div>
			  	<input type="hidden" name="pageNo" value="<?=$li->pageNo?>"/>
			  	<input type="hidden" name="order" value="<?=$li->order?>"/>
			  	<input type="hidden" name="field" value="<?=$li->field?>"/>
			  	<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
			  	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
				<input type="hidden" name="Current_ModuleID" value="<?=$Current_ModuleID?>"/>
				<input type="hidden" name="ModuleLicenseID" value=""/>
			</form>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>