<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");

intranet_auth();
intranet_opendb();

$objiTextbook = new libitextbook();

# Get ModuleLicenseID of selected distribution
$sql = "SELECT
    		id.ModuleLicenseID, im.code
		FROM
    		ITEXTBOOK_DISTRIBUTION AS id INNER JOIN
    		INTRANET_MODULE_LICENSE AS iml ON id.ModuleLicenseID = iml.ModuleLicenseID INNER JOIN
    		INTRANET_MODULE AS im ON iml.ModuleID = im.ModuleID
		WHERE
			DistributionID = $DistributionID";
list($ModuleLicenseID, $Current_Code) = current($objiTextbook->returnArray($sql));

# Group Category
$li = new libgrouping();
$lgroupcat = new libgroupcategory();

$cats = $lgroupcat->returnAllCat();

# First Drop Down List
if($CatID < 0){
  unset($ChooseGroupID);
  $ChooseGroupID[0] = 0-$CatID;
}

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name='CatID' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()'>\n" : "<select name='CatID' onChange='this.form.submit()'>\n";
$x1 .= "<option value='0'></option>\n";

for ($i=0; $i<sizeof($cats); $i++){
	list($id,$name) = $cats[$i];
  	$x1 .= $id!=0? "<option value='$id' ".(($CatID==$id)?"SELECTED":"").">$name</option>\n" : "";
}

$x1 .= "<option value='Subj' ".(($CatID=='Subj')?"SELECTED":"").">".$Lang['SysMgr']['Homework']['SubjectGroup']."</option>\n";
$x1 .= "<option value='0'>".str_repeat("_",20)."</option>\n";
$x1 .= "<option value='0'></option>\n";
$x1 .= "<option value='-2' ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
$x1 .= "<option value='0'></option>\n";
$x1 .= "</select>";

# Second Drop Down List
if($CatID!=0 && $CatID > 0 || $CatID=='Subj') {
	$x2  = "<select name='ChooseGroupID[]' size='5' multiple>\n";
  	
  	if ($CatID=='Subj'){
		$classTitle = "c.ClassTitle".strtoupper($intranet_session_language);
		$sql = "SELECT
					c.SubjectGroupID,
					$classTitle AS ClassTitle
				FROM
					{$intranet_db}.SUBJECT_TERM AS t INNER JOIN 
					{$intranet_db}.SUBJECT_TERM_CLASS AS c ON t.SubjectGroupID = c.SubjectGroupID 
				WHERE
				 	t.YearTermID IN
					(
						SELECT
							ayt.YearTermID
						FROM
							{$intranet_db}.ACADEMIC_YEAR AS ay INNER JOIN
							{$intranet_db}.ACADEMIC_YEAR_TERM AS ayt ON ay.AcademicYearID = ayt.AcademicYearID
						WHERE
							NOW() BETWEEN ayt.TermStart AND ayt.TermEnd
					)
				ORDER BY ClassTitle";	
		$row = $li->returnArray($sql);
	}
  	else{
		$row = $li->returnCategoryGroups($CatID);
  	}
	
	for($i=0; $i<sizeof($row); $i++){
		$GroupCatID   = $row[$i][0];
		$GroupCatName = $row[$i][1];
		
		$x2 .= "<option value='$GroupCatID'";
		for($j=0; $j<sizeof($ChooseGroupID); $j++){
		  $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
		}
		$x2 .= ">$GroupCatName</option>\n";
	}
	
  	$x2 .= "<option>".str_repeat("&nbsp;",40)."</option>\n";
  	$x2 .= "</select>\n";
}

# Third Drop Down List
if($CatID < 0){
	# Return users with identity chosen
	$selectedUserType = 0 - $CatID;

	$sql = "SELECT
				UserID
			FROM
				{$intranet_db}.ITEXTBOOK_DISTRIBUTION_USER
			WHERE
				DistributionID = $DistributionID";
	$allUser = $li->returnVector($sql);
	
	$sql = "SELECT
				u.UserID, ".getNameFieldWithClassNumberByLang("u.")."
			FROM
	            INTRANET_USER as u
			WHERE
				u.RecordType = $selectedUserType AND
				u.RecordStatus IN (0, 1, 2) AND
				u.UserID NOT IN ('".implode("','",$allUser)."')
			ORDER BY
				u.ClassName, u.ClassNumber, u.EnglishName";
	$row = $li->returnArray($sql);
	
	$x3  = "<select name='ChooseUserID[]' size='10' multiple>\n";
	for($i=0; $i<sizeof($row); $i++)
    	$x3 .= "<option value='".$row[$i][0]."'>".$row[$i][1]."</option>\n";
	$x3 .= "	<option>".str_repeat("&nbsp;",40)."</option>\n";
	$x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
	$sql = "SELECT
				UserID
			FROM
				{$intranet_db}.ITEXTBOOK_DISTRIBUTION_USER
			WHERE
				DistributionID = $DistributionID";
	$allUser = $li->returnVector($sql);
	
	if ($CatID=='Subj'){
		$sql = "SELECT
					i.UserID, ".getNameFieldWithClassNumberByLang("i.")."
				FROM
					{$intranet_db}.INTRANET_USER AS i INNER JOIN
					{$intranet_db}.SUBJECT_TERM_CLASS_USER AS u ON i.UserID = u.UserID AND SubjectGroupID IN ('".implode("','",$ChooseGroupID)."')
				WHERE
					i.UserID NOT IN ('".implode("','",$allUser)."') AND
					i.RecordStatus IN (0, 1, 2) AND
					i.RecordType = ".USERTYPE_STUDENT."
				GROUP BY
					i.UserID";
	}
	else{
		$sql = "SELECT
	                a.UserID, ".getNameFieldWithClassNumberByLang("a.")."
	            FROM
	                INTRANET_USER AS a, INTRANET_USERGROUP AS b
	            WHERE
	                a.UserID = b.UserID AND
					a.UserID NOT IN ('".implode("','",$allUser)."') AND
					a.RecordStatus IN (0, 1, 2) AND
					a.RecordType = ".USERTYPE_STUDENT." AND
					b.GroupID IN (".implode(",", $ChooseGroupID).")
				GROUP BY
					a.UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	}
	$row = $li->returnArray($sql);
	
	$x3  = "<select name='ChooseUserID[]' size='5' multiple>\n";
	for($i=0; $i<sizeof($row); $i++)
    	$x3 .= "<option value='".$row[$i][0]."'>".$row[$i][1]."</option>\n";
  	$x3 .= "	<option>".str_repeat("&nbsp;",40)."</option>\n";
  	$x3 .= "</select>\n";
}

$MODULE_OBJ['title'] = "<div style='float:left;overflow:hidden;height:20px;' title=''>".$Lang['itextbook']['AddStudentToDistribution']."</div>";
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_import, "");
?>

<script language="JavaScript1.2">
function import_update(obj){
 	var total_selected = countOption(obj.elements["ChooseUserID[]"]);
  	if (total_selected <= 0){
    	alert("<?=$i_eClass_no_user_selected?>");
    	return;
  	}

	var quota_left  = '<?=$objiTextbook->get_distributions_quota_left($ModuleLicenseID, $Current_Code)?>';
	var license_req = total_selected*<?=$iTextbook_cfg[$Current_Code]['MaxChapters']?>;
	if (license_req > parseInt(quota_left)){
		alert("<?=$Lang['itextbook']['AddStudentLicenseWarning'][0]?>" + total_selected + "<?=$Lang['itextbook']['AddStudentLicenseWarning'][1]?>\n\r\n\r<?=$Lang['itextbook']['RequireLicense']?>: " + license_req + "\n\r<?=$Lang['itextbook']['LicenseLeft']?>: " + parseInt(quota_left));
		return;
	}

	obj.action = "import_update.php";
	obj.submit();
}

function SelectAll(obj)
{
  for (i=0; i<obj.length; i++)
  {
    obj.options[i].selected = true;
  }
}
</script>

<br />
<form name="form1" action="import.php" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    	<tr>
      		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
      		<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
    	</tr>
    	<tr>
      		<td align="center">
        		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
					<tr valign="top">
            			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_frontpage_campusmail_select_category?> </span></td>
            			<td><?=$x1;?></td>
          			</tr>
          			
<?php if($CatID!=0 && $CatID > 0 || $CatID=='Subj') { ?>
          			<tr valign="top">
            			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_frontpage_campusmail_select_group?> </span></td>
            			<td>
              				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                				<tr>
                  					<td><?=$x2;?></td>
                  					<td><?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]'])", "expand_btn") ?></td>
                				</tr>
              				</table>
            			</td>
          			</tr>
<?php } ?>

<?php if(isset($ChooseGroupID)) { ?>
          			<tr valign="top">
            			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_frontpage_campusmail_select_user?> </span></td>
            			<td>
              				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                				<tr>
                  					<td><?=$x3;?></td>
                  					<td>
                    					<?=$linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['ChooseUserID[]']);import_update(this.form);", "add_btn") ?><br>
                    					<?=$linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['ChooseUserID[]']);", "select_all_btn") ?>
                  					</td>
                				</tr>
              				</table>
            			</td>
          			</tr>
<? } ?>

        		</table>
      		</td>
    	</tr>
	</table>
	<table width="95%" border="0" cellpadding="5" cellspacing="0">
	    <tr>
			<td colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td><span class="tabletextremark"><?=$Lang['itextbook']['ImportRemark'][0]?><?=$iTextbook_cfg[$Current_Code]['MaxChapters']?><?=$Lang['itextbook']['ImportRemark'][1]?></span></td>
					</tr>
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          			</tr>
          			<tr>
            			<td align="center">
              				<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","close_btn") ?>
            			</td>
          			</tr>
        		</table>
      		</td>
    	</tr>
	</table>
	<br />
	<input type="hidden" name="Current_Code" value="<?=$Current_Code?>"/>
	<input type="hidden" name="DistributionID" value="<?=$DistributionID?>"/>
	<input type="hidden" name="DistributionNo" value="<?=$DistributionNo?>"/>
	<input type="hidden" name="ModuleLicenseID" value="<?=$ModuleLicenseID?>"/>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>