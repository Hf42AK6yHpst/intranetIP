<?php

// Modifing by  
/**
 * Change Log:
 * 2016-09-05 Pun [ip.2.5.7.10.1]
 *  - update SessionKey after timeout instead of always generate it.
 */

include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libuser.php");
intranet_auth();
intranet_opendb();

# session_register("eClassMode");
# $eClassMode = "I";
# setcookie("eClassMode", "I", 0, "", $eclass_httppath, 0);

if(!isset($BookID) || $BookID=='')
	header('Location:/');

$li = new libuser($UserID);

// $session_key = $li->generateSessionKey();
// $li->updateSession($session_key);
$results = $li->getSessionKeyLastUpdatedFromUserId($UserID);
$session_key = trim($results["SessionKey"]);
if ($session_key=="")
{
    $session_key = $li->generateSessionKey();
    $li->updateSessionByUserId($session_key, $UserID);
}
$li->updateSessionLastUpdatedByUserId($UserID);

$email = $li->UserEmail;
$passwd = $li->UserPassword;

/************************* update by fai ****************************/
/* checking for reading / special room , path should be redirect to eclass30 file path
/* if normal class room, path should be redirect to eclass40 file path
/*******************/
$cfg_IsNormalClassRoom	= 0;  
$cfg_IsReadyRoom		= 2;  //may not use this variable or value, for reference only
$roomType = array();
$sql = "select 
			c.RoomType as 'RoomType',
			c.course_code
		from ".$eclass_db.".course as c 
			inner join ".$eclass_db.".user_course as uc on c.course_id = uc.course_id 
		where 
	uc.user_course_id = ".$uc_id;
$roomTypeArray = $li->returnArray($sql);

if(sizeof($roomTypeArray) != 1 )
{	//suppose roomtype should be one value only

	echo "Error in Course ID. Cannot not enter classroom<Br/>";
	exit();
}

$roomType = $roomTypeArray[0]["RoomType"];
$course_code = $roomTypeArray[0]["course_code"];


//DEFAULT THE URL IS eclass40
$redirect_url_root = $eclass_url_root;
$redirect_httppath_first = $eclass_httppath_first;

//if($roomType != $cfg_IsNormalClassRoom)
if($roomType != $cfg_IsNormalClassRoom && $roomType !=7) # not normal classroom and not itextbook
{
	//IF THE ROOM TYPE IS NOT A NORMAL CLASSROOM
	//OVERRIDE $redirect_url_root AND   $redirect_httppath_first , SET IT TO ECLASS30 

	$redirect_url_root = $eclass30_url_root;
//	$redirect_httppath_first = "";  have not find the suppose value, find it later
}
## Parent go to ambook
$isParent = (isset($isParent) && $isParent != '') ? $isParent : '';
$childID = (isset($childID) && $childID != '') ? $childID : '';

$kisViewPDFOnly = ($sys_custom['kis_worksheet_pdf_only'])?"&kis_worksheet_pdf_only=1":"";

if(strpos($course_code,'grammartouch') !== false){
	header("Location: ../../asp/".$course_code.".php");
}else if ($eclass_httppath_first == "")
{
    $url = "$redirect_url_root/check2.php?user_course_id=$uc_id&eclasskey=".$session_key."&jumpback=$jumpback&BookID=$BookID"."&isParent=".$isParent."&childID=".$childID.$kisViewPDFOnly;
    #$url = "$eclass_url_root/check.php?user_course_id=$uc_id&email=$email&password=$passwd";
    header("Location: $eclass_url_root/mode.php?url=".urlencode($url));
}
else
{
    #$url = "$eclass_httppath_first/check.php?user_course_id=$uc_id&email=$email&password=$passwd";
    $url = "$redirect_httppath_first/check2.php?user_course_id=$uc_id&eclasskey=".$session_key."&jumpback=$jumpback&BookID=$BookID"."&isParent=".$isParent."&childID=".$childID.$kisViewPDFOnly;
    header("Location: $eclass_httppath_first/mode.php?url=".urlencode($url));
}

intranet_closedb();
?>
