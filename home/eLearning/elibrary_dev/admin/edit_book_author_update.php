<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$AuthorArr = $LibeLib->GET_AUTHOR_DETAILS($AuthorID);

$Type = $AuthorArr[0]["InputBy"];
$TmpAuthorID = $LibeLib->UPDATE_BOOK_AUTHOR($author,$description,$Type);
if ($TmpAuthorID != $AuthorID)
{
	$LibeLib->UPDATE_BOOK_AUTHORID($BookID,$TmpAuthorID);
}

intranet_closedb();

header("Location: edit_book_author.php?BookID=$BookID&AuthorID=$TmpAuthorID");


?>