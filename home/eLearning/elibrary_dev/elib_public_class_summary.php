<?php
//using by:

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once("elib_script.php");



intranet_auth();

intranet_opendb();

########################################################

$field = (isset($_REQUEST['field']))? $_REQUEST['field'] : "";
$order = (isset($_REQUEST['order']))? $_REQUEST['order'] : 0;
$pageNo = (isset($_REQUEST['pageNo']))? $_REQUEST['pageNo'] : 1;
$YearClassID = (isset($_REQUEST['YearClassID']))? $_REQUEST['YearClassID']: "";
$FromMonth = (isset($_REQUEST['FromMonth']))? $_REQUEST['FromMonth'] : "";
$ToMonth = (isset($_REQUEST['ToMonth']))? $_REQUEST['ToMonth']: "";
$ToYear = (isset($_REQUEST['ToYear']))? $_REQUEST['ToYear']: Date("Y");
$FromYear = (isset($_REQUEST['FromYear']))? $_REQUEST['FromYear']: "";
$search = (isset($_REQUEST['search']))? $_REQUEST['search']: "";
 
$linterface 	= new interface_html("default3.html");
$li 			= new libdbtable($field, $order, $pageNo);
$lo 			= new libeclass();
$lelib 			= new elibrary();
$ly				= new year_class($YearClassID, false,false,false);

$ClassName = $ly->ClassTitleEN;


########################################################



$CurrentPage	= "PageMyeClass";

### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$ParArr["UserID"] = $_SESSION["UserID"];

$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$DisplayMode = 4; // list mode (no image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$lang = $_SESSION["intranet_session_language"];

//$tmpArr[0]["value"] = "UserName";
$tmpArr[0]["value"] = "ClassName";
$tmpArr[1]["value"] = "num_student";
$tmpArr[2]["value"] = "NumBookRead";
//$tmpArr[3]["value"] = "read_average";
$tmpArr[3]["value"] = "NumBookReview";
//$tmpArr[5]["value"] = "review_average";

$tmpArr[0]["text"] = $eLib["html"]["class"];
$tmpArr[1]["text"] = $eLib["html"]["num_of_students"];
$tmpArr[2]["text"] = $eLib["html"]["num_of_book_read"];
$tmpArr[3]["text"] = $eLib["html"]["num_of_review"];

$tmpArr[0]["width"] = "40%";
$tmpArr[1]["width"] = "15%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "10%";
//$tmpArr[4]["width"] = "10%";
//$tmpArr[5]["width"] = "10%*";

$tmpArr2[0]["value"] = "ClassName";
$tmpArr2[1]["value"] = "num_student";
$tmpArr2[2]["value"] = "NumBookRead";
$tmpArr2[3]["value"] = "read_average";
$tmpArr2[4]["value"] = "NumBookReview";
$tmpArr2[5]["value"] = "review_average";

$tmpArr2[0]["text"] = "&nbsp;";
$tmpArr2[1]["text"] = "&nbsp;";
$tmpArr2[2]["text"] = $eLib["html"]['Total'];
$tmpArr2[3]["text"] = $eLib["html"]['Average'];
$tmpArr2[4]["text"] = $eLib["html"]['Total'];
$tmpArr2[5]["text"] = $eLib["html"]['Average'];

#############################################################################
#############################################################################
$dsql1 = "drop table IF EXISTS INTRANET_ELIB_TEMP_NUM_REVIEW_{$currUserID}";
$lelib->db_db_query($dsql1);

$dsql2 = "drop table IF EXISTS INTRANET_ELIB_TEMP_NUM_READ_{$currUserID}";
$lelib->db_db_query($dsql2);

$tsql1 = "
		CREATE TABLE INTRANET_ELIB_TEMP_NUM_REVIEW_{$currUserID}
		SELECT c.ClassName, Count(d.ReviewID) as NumBookReview 
		FROM INTRANET_ELIB_BOOK a, INTRANET_USER c, INTRANET_ELIB_BOOK_REVIEW d 
		WHERE a.BookID = d.BookID AND c.UserID = d.UserID AND a.Publish = 1 AND a.ContentTLF IS NOT NULL
		GROUP BY c.ClassName
		";

$tsql2 = "
		CREATE TABLE INTRANET_ELIB_TEMP_NUM_READ_{$currUserID}
		SELECT c.ClassName, Count(DISTINCT b.BookID) as NumBookRead, b.DateModified
		FROM INTRANET_ELIB_BOOK a, INTRANET_ELIB_BOOK_HISTORY b, INTRANET_USER c
		WHERE a.BookID = b.BookID AND b.UserID = c.UserID AND a.Publish = 1 AND a.ContentTLF IS NOT NULL			
		GROUP BY c.ClassName
		";	
		

		
$lelib->db_db_query($tsql1);
$lelib->db_db_query($tsql2);

## SQL Query
$con = "";
if(!empty($FromMonth)){
	$con = '
			AND Date_Format(b.DateModified, "%Y-%m")  >= "'.$FromYear.'-'.$FromMonth.'"
			AND Date_Format(b.DateModified, "%Y-%m") <= "'.$ToYear.'-'.$ToMonth.'" ';
}

$sql = "
		SELECT
			count(*) as num_student,
			if (a.ClassName != '', a.ClassName, '--') as ClassName,				
			 if((b.NumBookRead IS NOT NULL),b.NumBookRead, 0) as NumBookRead,
			if((c.NumBookReview IS NOT NULL),c.NumBookReview, 0) as NumBookReview,
			if((b.NumBookRead IS NOT NULL),ROUND(b.NumBookRead/count(*)), 0)as read_average,
			if((c.NumBookReview IS NOT NULL),ROUND(c.NumBookReview/count(*)), 0)as review_average,
			y.YearClassID			
		FROM
			INTRANET_USER as a			
		LEFT JOIN
			INTRANET_ELIB_TEMP_NUM_REVIEW_{$currUserID} as c
		ON
			a.ClassName = c.ClassName";
$sql .= !empty($con) && !empty($search)? " INNER " : " LEFT ";
$sql .=	" JOIN
			INTRANET_ELIB_TEMP_NUM_READ_{$currUserID} as b
		ON
			a.ClassName = b.ClassName
		LEFT JOIN 
			YEAR_CLASS as y
		ON 
			a.ClassName = y.ClassTitleEN
	 	WHERE 1	
	 		AND
	 		y.AcademicYearID = ".Get_Current_Academic_Year_ID();
	 				
$sql .=  !empty($search)? $con : "";	
$sql .=	" GROUP BY
	 		a.ClassName		
		";
		
		
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("ClassName", "num_student", "NumBookRead", "read_average","NumBookReview", "review_average");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = "";

$sortField = $li->field_array[$field];

$inputArr["sortField"] 		= $sortField;
$inputArr["sortFieldOrder"] = $order;
$inputArr["sortFieldArray"] = $tmpArr;
$inputArr["sortFieldArray2"] = $tmpArr2;
$inputArr["selectNumField"] = 5;
$currUserID = $_SESSION["UserID"];

$aryDisplay = $lelib->displayClassSummaryDetail($inputArr, $eLib, $li->built_sql($sql));
$display_content = $aryDisplay['contentHtml'];
$totalRecord = $aryDisplay["totalRecord"];
$DisplayNumPage = $aryDisplay["DisplayNumPage"];

// drop the temp table create before
$lelib->db_db_query($dsql1);
$lelib->db_db_query($dsql2);


#####################################################
#####################################################

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

///////////////////// Selection Box ////////////////////////////////////////////////

$returnClassNameArr = $lelib->GET_CLASSNAME();

	$ClassNameArr = array(
				array("-1", $eLib["html"]["all_class"])
				);
			
	//debug_r($returnClassNameArr);
	for($i = 0; $i < count($returnClassNameArr); $i++)
	{
		
		if(trim($returnClassNameArr[$i]["ClassTitleEN"]) != "")
		$tmpClassArr = "";
		$tmpClassArr = array($returnClassNameArr[$i]["ClassTitleEN"], $returnClassNameArr[$i]["ClassTitleEN"]);
		
		$ClassNameArr[$i+1] = $tmpClassArr;
	}
	//debug_r($ClassNameArr);
	

	if($ClassName == "")
	$ClassName = $ClassNameArr[0][0];
	
	//$le->getSelectClass( "name='ClassName' id='ClassName' onChange='changeClassName(this);'", $ClassName, );
	$ClassSelection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($ClassNameArr, "name='ClassName' id='ClassName' onChange='changeClassName(this);'", "", $ClassName);
	
	$ThisYear = Date("Y")*1;
	$aryMonth = array(array(1,1),array(2,2),array(3,3),array(4,4),array(5,5),array(6,6),array(7,7),array(8,8),array(9,9),array(10,10),array(11,11),array(12,12));
	$aryYear = array(array($ThisYear,$ThisYear), array($ThisYear-1,$ThisYear-1),array($ThisYear-2,$ThisYear-2),array($ThisYear-3,$ThisYear-3),array($ThisYear-4,$ThisYear-4),array($ThisYear-5,$ThisYear-5)); 
		
	$FromMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='FromMonth' id='FromMonth'", "", $FromMonth);
	$FromYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='FromYear' id='FromYear'", "", $FromYear);
	$ToMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='ToMonth' id='ToMonth'", "", $ToMonth);
	$ToYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='ToYear' id='ToYear'", "", $ToYear);
	
	$classStr = $eLib["html"]["class"]." : ";
	
	$SelectionTable = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	//$SelectionTable .= "<tr><td><span class=\"tabletext\">".$classStr." ".$ClassSelection."</span></td></tr>";
	$SelectionTable .= '<tr><td><span class="tabletext">'.$Lang['Header']['Menu']['Period'].':'.
						$FromMonthSelection.$FromYearSelection.$Lang['SysMgr']['Homework']['To'].$ToMonthSelection.$ToYearSelection.
						'<input type="button" name="search" onClick="applySearch();" value="'.$Lang['Btn']['Apply'].'" /></span></td></tr>';
	
	$SelectionTable .= "</table>";
//////////////////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();


?>


<script language="JavaScript" type="text/JavaScript">
<!--

function changeClassName(obj)
{
	document.form1.CurrentPageNum.value = 1;
	document.form1.submit();
} // end function change class name

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

function applySearch(){
	
	var strFromDate = document.form1.FromYear.value+"-"+document.form1.FromMonth.value+"-0";
	var strToDate = document.form1.ToYear.value+"-"+document.form1.ToMonth.value+"-0";
	if(compareDate(strToDate,strFromDate)){
		$("#search").val(1);
		document.form1.submit();
	}
}


//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">

<form name="form1" action="" method="post">
	

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="top" width="172" style="margin:0px padding:0px;" >
		<!-- Template , Left Navigation panel -->
		<?=include_once('elib_left_navigation.php')?>		
	</td>
	<td align="center" valign="top">
		<!-- Start of Content -->
		
		<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>


			<!-- head menu -->
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
			
			<!-- NAVIGATION CRUMBTRAIL --> 
			<td class="eLibrary_navigation">
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
				<a href="index.php"><?=$eLib["html"]["home"]?></a> 
			
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle">
				<?=$eLib["html"]["class_summary"]?>
			</td>
			</tr>
			</table>
			
			<!-- end head menu -->


			<table width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
				<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
				<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
			</tr>
			
			<tr>
				<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>
				
				<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif">
				<!-- show content -->
				<?=$SelectionTable; ?>
				<!--<?=$contentHtml; ?>-->
					
				<?=$display_content?>
				
				<!-- end show content -->
				</td>
				
				
				<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
			</tr>
																
			<tr>
				<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
				<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
				<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
			</tr>
			</table>
		<!-- End of Content -->							
	</td></tr>
</table>	


<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$order?>" >


<input type="hidden" name="order" value="<?=$order?>" >
<input type="hidden" name="field" value="<?=$field?>" >
<input type="hidden" name="pageNo" value="<?=pageNo?>" >

<input type="hidden" name="search" id="search" value="" >
</form>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
