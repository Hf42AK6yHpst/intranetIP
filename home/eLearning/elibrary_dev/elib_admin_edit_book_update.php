<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$InputArray["BookID"] = $BookID;
$InputArray["Title"] = $Title;
$InputArray["Author"] = $Author;
$InputArray["SeriesEditor"] = $SeriesEditor;
$InputArray["Category"] = $Category;
$InputArray["SubCategory"] = $Subcategory;
$InputArray["Level"] = $Level;
$InputArray["AdultContent"] = $AdultContent;
$InputArray["Publisher"] = $Publisher;
$InputArray["Category"] = $Category;
$InputArray["Source"] = $SourceFrom;
$InputArray["Language"] = $Language;
$InputArray["Preface"] = $Preface;
$InputArray["Type"] = $InputBy;
$InputArray["Publish"] = $Publish;
$InputArray["Copyright"] = $Copyright;

$LibeLib->UPDATE_BOOK_INFO2($InputArray);


intranet_closedb();

header("Location: elib_admin.php");


?>