<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");

intranet_auth();
intranet_opendb();

// additional javascript for eLibrary only
include_once("elib_script.php");

$LibeLib = new elibrary();

$settingArr = $LibeLib->GET_USER_BOOK_SETTING();

//debug_r($settingArr);

$selected1 = "";
$selected2 = "";
$selected3 = "";
$selected4 = "";
$selected5 = "";
$selected6 = "";

if($settingArr[0]["DisplayReviewer"] == 5)
$selected1 = "selected";
else if($settingArr[0]["DisplayReviewer"] == 10)
$selected2 = "selected";
else if($settingArr[0]["DisplayReviewer"] == 15)
$selected3 = "selected";
else if($settingArr[0]["DisplayReviewer"] == 20)
$selected4 = "selected";
else if($settingArr[0]["DisplayReviewer"] == 30)
$selected5 = "selected";
else if($settingArr[0]["DisplayReviewer"] == 40)
$selected6 = "selected";
else
$selected2 = "selected";

$selectDisplayReviewer = "<select name=\"DisplayReviewer\">";
$selectDisplayReviewer .= "<option value=5 ".$selected1.">5</option>";
$selectDisplayReviewer .= "<option value=10 ".$selected2.">10</option>";
$selectDisplayReviewer .= "<option value=15 ".$selected3.">15</option>";
$selectDisplayReviewer .= "<option value=20 ".$selected4.">20</option>";
$selectDisplayReviewer .= "<option value=30 ".$selected5.">30</option>";
$selectDisplayReviewer .= "<option value=40 ".$selected6.">40</option>";
$selectDisplayReviewer .= "</select>";

//////////////////////////////////////////////////////////////////////////////

$selected1 = "";
$selected2 = "";
$selected3 = "";
$selected4 = "";
$selected5 = "";
$selected6 = "";

if($settingArr[0]["DisplayReview"] == 5)
$selected1 = "selected";
else if($settingArr[0]["DisplayReview"] == 10)
$selected2 = "selected";
else if($settingArr[0]["DisplayReview"] == 15)
$selected3 = "selected";
else if($settingArr[0]["DisplayReview"] == 20)
$selected4 = "selected";
else if($settingArr[0]["DisplayReview"] == 30)
$selected5 = "selected";
else if($settingArr[0]["DisplayReview"] == 40)
$selected6 = "selected";
else
$selected2 = "selected";

$selectDisplayReview = "<select name=\"DisplayReview\">";
$selectDisplayReview .= "<option value=5 ".$selected1.">5</option>";
$selectDisplayReview .= "<option value=10 ".$selected2.">10</option>";
$selectDisplayReview .= "<option value=15 ".$selected3.">15</option>";
$selectDisplayReview .= "<option value=20 ".$selected4.">20</option>";
$selectDisplayReview .= "<option value=30 ".$selected5.">30</option>";
$selectDisplayReview .= "<option value=40 ".$selected6.">40</option>";
$selectDisplayReview .= "</select>";

/////////////////////////////////////////////////////////////////////////////////
$selected1 = "";
$selected2 = "";
$selected3 = "";

if($settingArr[0]["DisplayRecommendBook"] == 1)
$selected1 = "selected";
else if($settingArr[0]["DisplayRecommendBook"] == 4)
$selected2 = "selected";
else if($settingArr[0]["DisplayRecommendBook"] == 9)
$selected3 = "selected";
else
$selected2 = "selected";

$selectRecommendBook = "<select name=\"RecommendBook\" onChange=\"changeDisplay(this);\">";
$selectRecommendBook .= "<option value=1 ".$selected1.">1</option>";
$selectRecommendBook .= "<option value=4 ".$selected2.">4</option>";
$selectRecommendBook .= "<option value=9 ".$selected3.">9</option>";
$selectRecommendBook .= "</select>";

////////////////////////////////////////////////////////////////////////////////
$selected1 = "";
$selected2 = "";
$selected3 = "";

if($settingArr[0]["DisplayWeeklyHitBook"] == 1)
$selected1 = "selected";
else if($settingArr[0]["DisplayWeeklyHitBook"] == 4)
$selected2 = "selected";
else if($settingArr[0]["DisplayWeeklyHitBook"] == 9)
$selected3 = "selected";
else
$selected1 = "selected";

$selectWeeklyHitBook = "<select name=\"WeeklyHitBook\" onChange=\"changeDisplay(this);\">";
$selectWeeklyHitBook .= "<option value=1 ".$selected1.">1</option>";
$selectWeeklyHitBook .= "<option value=4 ".$selected2.">4</option>";
$selectWeeklyHitBook .= "<option value=9 ".$selected3.">9</option>";
$selectWeeklyHitBook .= "</select>";

//////////////////////////////////////////////////////////////////////////////////////

$selected1 = "";
$selected2 = "";
$selected3 = "";

if($settingArr[0]["DisplayHitBook"] == 1)
$selected1 = "selected";
else if($settingArr[0]["DisplayHitBook"] == 4)
$selected2 = "selected";
else if($settingArr[0]["DisplayHitBook"] == 9)
$selected3 = "selected";
else
$selected3 = "selected";

$selectHitBook = "<select name=\"HitBook\" onChange=\"changeDisplay(this);\">";
$selectHitBook .= "<option value=1 ".$selected1.">1</option>";
$selectHitBook .= "<option value=4 ".$selected2.">4</option>";
$selectHitBook .= "<option value=9 ".$selected3.">9</option>";
$selectHitBook .= "</select>";

?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function changeDisplay(obj)
{
	var objName = obj.name;
	var refTableStr = objName + "Table";
	var refTable = document.getElementById(refTableStr);
	
	deleteTable(refTable);
	
	addRow(refTable, obj.value);
	
} // end function changeDisplay

function deleteTable(obj)
{
	for(var i = obj.rows.length - 1 ; i >= 0 ; i--)
	obj.deleteRow(i);
} // end function delete row

function addRow(obj, num)
{
	var str = "";
	var x,y,z,a;
	
	var BookName = "<?=$eLib["html"]["book_name"]?>";
	var BookCover = "<?=$eLib["html"]["book_cover"]?>";
	
	switch(num){
		case "1":
			var width= 200;
			var height = 285;
			break;
		case "4":
			var width= 100;
			var height = 135;
			break;
		default:
			var width= 45;
			var height = 65;
			break;
		
	}
	
	str += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\"><tr>";
	str += "<td width=\""+width+"\" height=\""+height+"\" align=\"center\" bgcolor=\"#C5E0FC\" class=\"tabletextremark\">"+BookCover+"</td>";
	str += "</tr></table>";
	//str += "<span class=\"eLibrary_booktitle_normal\">"+BookName+"</span>";
	
	if(num == 1)
	{		
	
		var x = obj.insertRow(0);
		var y = x.insertCell(0);
		y.innerHTML = str;
		y.align = "center";
	}
	else if(num == 4)
	{	
		for(i = 0; i < 2; i++)
		{
			x = obj.insertRow(i);
			y = x.insertCell(0);
			z = x.insertCell(1);
			y.innerHTML = str;
			z.innerHTML = str;
			y.align = "center";
			z.align = "center";
		}		
	}
	else if(num == 9)
	{		
		for(i = 0; i < 3; i++)
		{
			x = obj.insertRow(i);
			y = x.insertCell(0);
			z = x.insertCell(1);
			a = x.insertCell(2);
			y.innerHTML = str;
			z.innerHTML = str;
			a.innerHTML = str;
			y.align = "center";
			z.align = "center";
			a.align = "center";
		}
		
	}
} // end function add row

var NumReviewer = <?=$settingArr[0]["DisplayReviewer"]?>;
var NumReview = <?=$settingArr[0]["DisplayReview"]?>;
var NumRecommend = <?=$settingArr[0]["DisplayRecommendBook"]?>;
var NumWeeklyHit = <?=$settingArr[0]["DisplayWeeklyHitBook"]?>;
var NumHit = <?=$settingArr[0]["DisplayHitBook"]?>;

function init()
{
	var obj = window.opener;
	obj.resetPage();
	resetDisplay();
}

function save()
{
	var formObj = document.all["form1"];
	formObj.submit();
	init();
} // end function save

function resetDisplay()
{
	var selectReviewer = document.all["DisplayReviewer"];
	var selectReview = document.all["DisplayReview"];
	var selectRecommendBook = document.all["RecommendBook"];
	var selectWeeklyHitBook = document.all["WeeklyHitBook"];
	var selectHitBook = document.all["HitBook"];
	
	//alert(NumReviewer + " " + NumReview + " " + NumRecommend + " " + NumWeeklyHit + " " + NumHit);
	
	selectReviewer.selectedIndex = getOptionIndexList(NumReviewer);
	selectReview.selectedIndex = getOptionIndexList(NumReview);
	selectRecommendBook.selectedIndex = getOptionIndexImage(NumRecommend);
	selectWeeklyHitBook.selectedIndex = getOptionIndexImage(NumWeeklyHit);
	selectHitBook.selectedIndex = getOptionIndexImage(NumHit);
	
	changeDisplay(selectRecommendBook);
	changeDisplay(selectWeeklyHitBook);
	changeDisplay(selectHitBook);
	
} // end function reset

function getOptionIndexList(n)
{
	if(n == 5)
	return 0;
	else if(n == 10)
	return 1;
	else if(n == 15)
	return 2;
	else if(n == 20)
	return 3;
	else if(n == 30)
	return 4;
	else if(n == 40)
	return 5;
	else
	return 1;
} // end function return option index list

function getOptionIndexImage(n)
{
	if(n == 1)
	return 0;
	else if(n == 4)
	return 1;
	else if(n == 9)
	return 2;
	else
	return 2;
} // end function return option index image

function cancel()
{
	window.close();
} // end function cancel

//init();
//-->
</script>
<link href="css/text" rel="stylesheet" type="text/css">
<link href="css/content.css" rel="stylesheet" type="text/css">
<link href="css/topbar.css" rel="stylesheet" type="text/css">
<link href="css/leftmenu.css" rel="stylesheet" type="text/css">
<link href="css/footer.css" rel="stylesheet" type="text/css">
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
#Layer1 {	
	position:absolute;
	width:150px;
	height:14px;
	z-index:2;
}
-->
</style>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv='content-type' content='text/html; charset=big5' />

<body background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_prev_month_on.gif','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_next_month_on.gif')">

<form name="form1" action="elib_setting_update.php">
<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="50" valign="top"> 
				<table width="101%" border="0" cellspacing="0" cellpadding="0">
              	<tr> 
	                <td width="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" width="10" height="41"></td>
	                <td width="200" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" class="title">eLibrary  Settings</td>
	                <td width="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_02.gif" ></td>
	                <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_03.gif">&nbsp;</td>
	                <td width="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_04.gif" width="22" height="41"></td>
              	</tr>
            	</table>
            </td>
        </tr>
        <tr> 
          <td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
                <td width="10" valign="top">&nbsp;</td>
                <td>
                	<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="13" height="33"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_01_t.gif" width="13" height="33"></td>
						<td height="33" valign="bottom" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_02_t.gif" class="imailpagetitle">
							<table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="left">&nbsp;<span class="contenttitle">Portal Display Settings</span></td>
								<td align="right"><label></label></td>
							</tr>
							</table>
						</td>
						<td width="11" height="33"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_03_t.gif" width="11" height="33"></td>
					</tr>
					
					
					<tr>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_04.gif" width="13" height="13"></td>
						<td align="center" bgcolor="#FFFFFF">

							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="left">
							
							<!-- ********************************************* -->
							<!-- ------------------ROW 1------------------ -->
							<tr>
    							<td valign="top" style="padding:10px 10px 10px 0">
    								<table width="175" border="0" cellspacing="0" cellpadding="5">
										<tr><td height="260" align="center" bgcolor="#CCCCCC"><span class="eLibrary_title_heading2"><?=$eLib["html"]["records"]?></span></td></tr>
									</table>
       							</td>
        
        						<td width="100%" valign="top" style="padding:10px 0 0 0">        
        							<table width="100%" border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td align="center" valign="middle" bgcolor="#CCCCCC" width="60%">
                        					<span class="eLibrary_title_heading2"><?=$eLib["html"]["book_cover"]?></span>
                        				</td>
                        
                        				<td bgcolor="#EBEBEB" width="40%">
                        
                        					<table width="100%" border="0" cellspacing="0">
											<tr>
												<td height="20" align="center" bgcolor="#CCCCCC"><span class="eLibrary_title_heading"><?=$eLib["html"]["recommended_books_2"]?></span></td>
											</tr>
											<tr>
												<td height="30" align="center">
													<span class="tabletext"><?=$eLib["html"]["display"]?></span>
													<?=$selectRecommendBook?>
													<span class="tabletext"><?=$eLib["html"]["books"]?><br><br></span>
												</td>
											</tr>
                                        	<tr>
                                        		<td>
			                                        <!-- 9 book covers display -->
			                                        <?=$LibeLib->printSettingBookTable("RecommendBookTable", $image_path, $LAYOUT_SKIN, $settingArr[0]["DisplayRecommendBook"]);?>
			                                        <!-- book cover display end-->
                                				</td>
                                			</tr>
											</table>
											<span class="eLibrary_title_heading"></span>
										</td>
									</tr>
									</table>        
        						</td>
    						</tr>
	    					<!-- -----------------  END of ROW 1------------------ -->
	    					<!-- ********************************************* -->
	    					
	    					<!-- ********************************************* -->
	    					<!-- ------------------ROW 2------------------ -->
	    					<tr>
	    					
	    						<!-- ----------------- ROW 2 : LEFT PANEL ------ -->
	    						<td valign="top" style="padding:5px 10px 10px 0">
	        						<table width="175" border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td height="400" align="center" bgcolor="#CCCCCC"><span class="eLibrary_title_heading2"><?=$eLib["html"]["catalogue"]?></span></td>
									</tr>
									</table>
	       						</td>
	       						<!-- ----------------- END LEFT PANEL ------ -->
	        
	        
	        					<!-- ----------------- ROW 2 : CENTER PANEL ------ -->
						        <td valign="top">
						        	<table width="59%" border="0" cellpadding="0" cellspacing="0" style="float:left">
						        	
						        	
						        	<!-- ------ HIGHEST SECTION --------------  -->
						            <tr>
						            	<td valign="top" width="100%" style="padding:5px 0 0 0">
						                	<table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="#EBEBEB">
											<tr>
												<td height="20" align="center" bgcolor="#CCCCCC"><span class="eLibrary_title_heading"><?=$eLib["html"]["bookS_with_highest_hit_rate"]?></span></td>
											</tr>
	                                        <tr>
												<td height="30" align="center">
													<span class="tabletext"><strong><?=$eLib["html"]["last_week"]?></strong> -- <?=$eLib["html"]["display"]?></span>
														<?=$selectWeeklyHitBook?>
													<span class="tabletext"> <?=$eLib["html"]["books"]?></span>
												</td>
											</tr>
	                                        <tr>
	                                        <td>
		                                        <!-- book cover display -->
			                                     <?=$LibeLib->printSettingBookTable("WeeklyHitBookTable", $image_path, $LAYOUT_SKIN, $settingArr[0]["DisplayWeeklyHitBook"]);?>
											</td>
										</tr>
										</table>
	                				</td>
	                			</tr>
	        					<!-- ------ END of HIGHEST SECTION --------------  -->
	        					
	        					<!-- ------ ACCUMULATE SECTION --------------  -->
	        					<tr>
	        						<td width="100%" valign="top">
	        							<table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="#EBEBEB">
										<tr>
											<td align="center" height="1" bgcolor="#CCCCCC"></td>
										</tr>
	                                
										<tr>
											<td height="30" align="center">
												<span class="tabletext"><strong><?=$eLib["html"]["accumulated"]?></strong> -- <?=$eLib["html"]["display"]?></span>
												<?=$selectHitBook?>
												<span class="tabletext"> <?=$eLib["html"]["books"]?></span>
											</td>
										</tr>
	                                     <tr>
	                                     	<td>
		                                        <!-- book cover display -->
		                                       	<?=$LibeLib->printSettingBookTable("HitBookTable", $image_path, $LAYOUT_SKIN, $settingArr[0]["DisplayHitBook"]);?>
	                                        </td>
										</tr>
										</table>
	        						</td>
	        					</tr>
	        					<!-- ------ END of ACCUMULATE SECTION --------------  -->
	            					
	            				</table>
	                
	                			<!-- ------ ROW 2 : RIGHT PANEL -------- -->
	                			<table border="0" cellpadding="0" cellspacing="0" width="40%" style="float:right">
	                			
	                			<!-- ***************** MOST ACTIVE ***************** -->               
	                			<tr>
	                				<td valign="top" style="padding:5px 0 0 0">
	                					<table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#EBEBEB">
										<tr>
											<td align="center" bgcolor="#CCCCCC"><span class="eLibrary_title_heading"><?=$eLib["html"]["most_active_reviewers"]?></span></td>
										</tr>
										<tr>
											<td height="110" align="center">
												<span class="tabletext"><?=$eLib["html"]["display_top"]?></span>
												<?=$selectDisplayReviewer?>
											</td>
										</tr>
										</table>
	                				</td>
	                			</tr>
	                			<!-- ***************** END of MOST ACTIVE ***************** -->
	                			
	                			
	                			<!-- *****************  MOST HELPFUL  ***************** -->
	                			<tr>
	                				<td valign="top" style="padding:20px 0 0 0">
	                					<table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#EBEBEB">
										<tr>
											<td align="center" bgcolor="#CCCCCC"><span class="eLibrary_title_heading"><?=$eLib["html"]["most_useful_reviews"]?></span></td>
										</tr>
										<tr>
											<td height="110" align="center">
												<span class="tabletext"><?=$eLib["html"]["display_top"]?></span>
												<?=$selectDisplayReview?>
											</td>
										</tr>
										</table>
	                				</td>
	                			</tr>        
	        					</table>
	        					<!-- *****************  END of MOST HELPFUL  ***************** -->
	        					
	        					<!-- ------ END of RIGHT PANEL -------- -->
	        
	        				</td>
	    				</tr>
	    				<!-- ------ END of ROW 2 -------- -->
	    				<!-- ********************************************* -->
	    				
						</table>
					
						<br style="clear:both">
						<br>
	
						<!-- -------------------- BUTTON PANEL ------------------------------>
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td>&nbsp;</td>
							
							<td align="center" valign="bottom">
								<input name="Submit" type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="save();" value="<?=$eLib["html"]["save"]?>">
			<input name="Reset" type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="resetDisplay();" value="<?=$eLib["html"]["reset"]?>">
			<input name="Canel" type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="cancel();" value="<?=$eLib["html"]["cancel"]?>">
							</td>
						</tr>
						</table>
						<!-- -------------------- END of BUTTON PANEL ------------------------------>
											
						<br>
	    				</td>
	    				<td width="11" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_06.gif" width="11" height="13"></td>
					</tr>
					<tr>
						<td width="13" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_07.gif" width="13" height="10"></td>
						<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_08.gif" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_08.gif" width="13" height="10"></td>
						<td width="11" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_09.gif" width="11" height="10"></td>
					</tr>
					</table>
					<br>
				</td>
			</tr>
	        </table>
		</td>
	</tr>
	</table>
</td>
</tr>
<tr> 
    <td height="20" bgcolor="#999999"> 
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="right"><span class="footertext">Powered by</span> <a href="#"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/logo_eclass_footer.gif" border="0" align="absmiddle"></a></td>
          <td width="20"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="20" height="20"></td>
        </tr>
    	</table>
   </td>
  </tr>
</table>

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/board_scheme_03.gif" width="4" height="4">
</form>

<?php
intranet_closedb();
?>
