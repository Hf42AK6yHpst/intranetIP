<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pagamo/libpagamo.php");
intranet_opendb();

$li = new pagamo();
/*
 * 	Status Code representation:
 *	10: account created and assigned class
 *	11: account created but failed to assign class
 *	12: account creation failed
 *	20: account login success
 *	21: account login failed
 */
$PaGamOLogin = (isset($PaGamOLogin))?$PaGamOLogin:"111111-1";
$info = explode("-",$PaGamOLogin);
switch($responses){
	case 10:
		$msg = "both success. PaGamO Login:".$PaGamOLogin;
		$li->updateStatus('AccCreation', $info[1]);
		$li->addToLog($responses, $msg, $info[1]);
		break;
	case 11:
		$msg = "account created. class assignment failed. PaGamO Login:".$PaGamOLogin;
		$li->updateStatus('AccCreation', $info[1]);
		$li->addToLog($responses, $msg, $info[1]);
		break;
	case 12:
		$msg = "account creation failed";
		$li->addToLog($responses, $msg, $info[1]);
		break;
	case 20:
		$msg = "logged in";
		$li->updateStatus('LogIn', $info[1]);
		break;
	case 21:
		$msg = "failed to login";
		$li->addToLog($responses, $msg, $info[1]);
		break;
}
intranet_closedb();
?>