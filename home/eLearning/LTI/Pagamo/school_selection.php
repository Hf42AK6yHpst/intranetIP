<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pagamo/libpagamo.php");
intranet_opendb();

$li = new pagamo();
if($li->isGeneralAdmin()){
	if($sys_custom['non_eclass_PaGamO']){
			
	}else{
		header('Location: index.php?schoolCode='.$config_school_code.'');
	}
}else{
	header('Location: index.php');
}
$schooList = $li->getSchoolList();

intranet_closedb();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>eClass x PaGamO</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="/templates/pagamo/css/jquery-ui.css" rel="stylesheet" type="text/css">
	<link href="/templates/pagamo/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="/templates/pagamo/css/for-bootstrap4.css" rel="stylesheet" type="text/css">
	<link href="/templates/pagamo/css/main.css" rel="stylesheet" type="text/css">
	<link href="/templates/pagamo/css/order.css" rel="stylesheet" type="text/css">
	<link href="/templates/pagamo/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" media="all" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/earlyaccess/notosanstc.css" media="all" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="/templates/pagamo/js/bootstrap-select.min.js"></script>
	<script src="/templates/pagamo/js/effect.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link rel="shortcut icon" href="/templates/pagamo/images/favicon.gif" type="image/x-icon">
	<link rel="icon" href="/templates/pagamo/images/favicon.gif" type="image/x-icon">
	<meta name="viewport" content="width=device-width">
	<script>
	$( document ).ready(function() {
		$('#blkForm div.student').each(function(index){
			$(this).on('click', function(){
				if($(this).hasClass('unchecked')){ //from unchecked to checked
					$('.student.checked').click();
					if(parseInt($('right', this).css('height')) > parseInt($(this).css('height'))){
						$(this).css('height', $('right', this).css('height'));
					}
				}else{ // from checked to unchecked
					$('.student').css('height','');
				}				
			});
		});
	});
	function chooseSchool(){
		var schoolCode = $('input[name="schoolCode"]:checked').val();
		this.location.href = "index.php?schoolCode="+schoolCode;
	}
	</script>
</head>

<!-- Select / Deselect effect in effect.js, programmer can re-write if necessary-->

<body>
	<div id="blkForm" class="unchecked">
		<div class="table-header">
			<span></span><span class="transition" id="blkForm-Class" style="width: 46.5% !important;"><?=$Lang['PaGamO']['choose_school']?></span><!-- Show this if selected any of the years-->
		</div>
		<?php 
		foreach($schooList as $schoolIdx=>$school){
		?>
		<div class="student"><!-- Add .checked if selected-->
			<right>			
			</right>
			<div>
				<left style="width:100%">
					<span><input type="radio" id="cbx<?=$schoolIdx?>" name="schoolCode" value="<?=$schoolIdx?>"><label for="cbx<?=$schoolIdx?>"></label></span><span style="display:unset;"><?=$school['SchoolName']?></span>
				</left>				
			</div>
		</div>
		<?php 
		}
		?>		
	</div>

	<div class="center marginB20" style="left:350px;">
		<input name="next" type="button" value="<?=$Lang['General']['Start']?>" class="next_button button pointer showWhenChecked" onclick='javascript:chooseSchool()'/><!-- Show this if selected any of the years-->
	</div>

</body>
</html>
