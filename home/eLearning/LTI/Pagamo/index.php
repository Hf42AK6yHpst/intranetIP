<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pagamo/libpagamo.php");
intranet_opendb();

$li = new pagamo();

if($li->isGeneralAdmin() && !isset($schoolCode)){
	header('Location: school_selection.php');
	exit();
}

if(!$li->hasQuotation()){
	header('Location: /logout.php');
	exit();
}

if($sys_custom['non_eclass_PaGamO']){
	$schoolCode = (isset($schoolCode))?$schoolCode:"";
	$schoolCode = $li->setUpKeySecretUrl($schoolCode);
}else{
	$schoolCode = $li->setUpKeySecretUrl($config_school_code);
}

$launch_data = $li->generateLaunchData($schoolCode);
$signature = $li->generateSignature($launch_data);

?>
<html>
<head>
</head>
<body onload="document.ltiLaunchForm.submit();">
<!--<body>-->
<form id="ltiLaunchForm" name="ltiLaunchForm" method="POST" action="<?php printf($li->getLaunchUrl()); ?>">
<?php foreach ($launch_data as $k => $v ) { ?>
	<input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>">
<?php } ?>
	<input type="hidden" name="oauth_signature" value="<?php echo $signature ?>">
</form>
<body>
</html>
<?php	

intranet_closedb();
?>
