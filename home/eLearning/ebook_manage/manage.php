<?php
// editing by Paul 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT."includes/libebookreader_ui.php");
if($_SESSION['isSchemaUpdate']!=1){
	intranet_auth();
}
intranet_opendb();

$bid = $_REQUEST['bid'];
$PageMode = !isset($_REQUEST['PageMode']) ? 2 : $_REQUEST['PageMode'];

$ebook_root_path = $intranet_root."/file/eBook/".$bid."/";
$request_url = $_SERVER['REQUEST_URI'];
if($request_url[strlen($request_url)-1] != '/'){
	$request_url .= '/';
}

$libebookreader_ui = new libebookreader_ui();
$libebookreader = $libebookreader_ui->Get_libebookreader();

$parse_success = $libebookreader->Parse_EPUB_XML($ebook_root_path);

if(!$parse_success){
	echo "Fail to parse ebook.";
	intranet_closedb();
	exit;
}

$book_info = $libebookreader->Get_Book_List($bid);

$book_elibinfo =  $libebookreader->Get_eLib_Book_Info($bid);  
$Publisher = $book_elibinfo[0]['Publisher'];

$engclassicMode = $Publisher=="English Classic"||$SubCategory=="English Classic";

$isImageBook = $book_info[0]['ImageBook'] == '1';
//2013-3-14 Charles Ma
$ForceToOnePageMode = trim($book_info[0]['ForceToOnePageMode']) == '1';		
$OnePageModeWidth = trim($book_info[0]['OnePageModeWidth']) == ''?  '1024' : trim($book_info[0]['OnePageModeWidth']);


$js = 'var bookdata = {';
$js .= "'id' : '".$bid."',\n";
$js .= '\'item\' : {},'."\n";
$js .= '\'spine\' : []'."\n";
$js .= '};'."\n";

$items = $libebookreader->opf_items;
$spines = $libebookreader->opf_spine;

if(count($items)>0){
	foreach($items as $key => $val){
		$js .= "bookdata.item['".$key."'] = {'id':'".$val['id']."', 'href':'".$val['href']."', 'media-type':'".$val['media-type']."'};\n";
	}
}

if(count($spines)>0){
	//foreach($spines as $key => $val){
	//	$js .= "bookdata.spine.push({'idref':'".$val['idref']."'});\n";
	//}
	for($i=0;$i<count($spines);$i++){
		$js .= "bookdata.spine.push({'idref':'".$spines[$i]['idref']."'});\n";
	}
}

echo $libebookreader_ui->Get_Layout_Start();
?>
<script language="JavaScript">
var pageModes = [2,1];
var pageMode = pageModes.shift();
//var pageModeWidth = [99999,650,980];
//var pageModeWidth = [99999,650,1024];

<?php if($ForceToOnePageMode){?>	
	var pageModeWidth = [99999,1000,980];
	var pageModeContainerWidth = [99999,4118,1024];
	//var pageModeWidth = [99999, 650,<?=$OnePageModeWidth ?> - 44];
	//var pageModeContainerWidth = [99999, 768,<?=$OnePageModeWidth ?>];	
<?php } else {?>
	var pageModeWidth = [99999,650,980];
	var pageModeContainerWidth = [99999,768,1024];
<?php }?>


var pageIndex = -1;
<?=$js?>

function jsLoadDocument(i)
{
	var docpath = '';
	if(typeof(bookdata.spine[i]) == 'undefined' || bookdata.spine[i]==null){
		return;
	}else if(typeof(bookdata.item[bookdata.spine[i].idref])!='undefined'){
		docpath = bookdata.item[bookdata.spine[i].idref].href;
		document.getElementById('InnerPostProcessFrame').src = '<?=$request_url?>' + docpath + '?PageMode=' + pageMode;
	}
}

function jsCountPage()
{
	if(typeof(bookdata.spine[pageIndex])=='undefined' || bookdata.spine[pageIndex] == null){
		pageIndex+=1;
		setTimeout("jsLoadDocument("+pageIndex+");",100);
		return;
	}
	//var docObj = $(document.getElementById('InnerPostProcessFrame').contentDocument);
	//var page_count = Math.ceil(docObj.width() / pageModeWidth[pageMode]);

	var docObj = $(document.getElementById('InnerPostProcessFrame').contentDocument);
	//console.log([bookdata,docObj.width(), pageModeContainerWidth[pageMode], pageModeWidth[pageMode]]);
	
	if(docObj.width() <= pageModeContainerWidth[pageMode]){ // less than or equal to container width, count as 1 
		var page_count = 1;
	}else{
		var page_count = Math.ceil(docObj.width() / pageModeWidth[pageMode]);
	}
	var NoOfPages = page_count;
	if(docObj[0].getElementsByClassName('page').length > 0){
		if(pageMode==1){
			NoOfPages = docObj[0].getElementsByClassName('page').length;
		}else if(pageMode==2){
			var LastPageNodeId = parseInt(docObj[0].getElementsByClassName('page').length) - 1;
			var FirstPageNo = parseInt(docObj[0].getElementsByClassName('page')[0].getAttribute("pageno"));
			var LastPageNo = parseInt(docObj[0].getElementsByClassName('page')[LastPageNodeId].getAttribute("pageno"));
			var backNo = 0;
			while(!LastPageNo){
				backNo = backNo + 1;
				if(LastPageNodeId < backNo){
					break;
				}
				LastPageNo = parseInt(docObj[0].getElementsByClassName('page')[LastPageNodeId-backNo].getAttribute("pageno"))+backNo;
				if(LastPageNodeId == backNo){
					break;
				}
			}
			NoOfPages = Math.ceil((LastPageNo - FirstPageNo + 1)/2);
		}
		if(isNaN(NoOfPages)){
			NoOfPages = page_count;
		}
	}
	console.log([pageMode,docObj,bookdata,docObj.width(), pageModeContainerWidth[pageMode], pageModeWidth[pageMode],page_count, NoOfPages]);

/*	
	var dc = document.getElementById('InnerPostProcessFrame').contentDocument.getElementById('book_content_main');
	var cw = pageModeWidth[pageMode];
	var scrolledAmount = dc.scrollLeft;
	while(scrolledAmount <= dc.scrollLeft){
		dc.scrollLeft += cw;
		scrolledAmount += cw;
	}
	var width = dc.scrollLeft + dc.clientWidth;
	var page_count = Math.ceil(width / pageModeWidth[pageMode]);
*/	
	jsUpdateProgress();
	if(NoOfPages==page_count || !isNaN(NoOfPages)){
		$.post(
			'/home/eLearning/ebook_manage/ajax_task.php',
			{
				"Task":"update_page_count",
				"BookID":bookdata.id,
				"RefID": encodeURIComponent(bookdata.spine[pageIndex].idref),
				"PageMode":pageMode,
				"PageCount":page_count,
				"NoOfPages":NoOfPages 
			},
			function(data){
				if(data=="1"){ // no error, continue process
					pageIndex += 1;
					if(pageIndex >= bookdata.spine.length)
					{
						if(pageModes.length > 0){
							pageMode = pageModes.shift();
							pageIndex = 0;
							//$('div#book_content').removeClass().addClass('book_' + pageMode + 'page_content');
							setTimeout("jsLoadDocument("+pageIndex+");",100);
						}else{
							var dvStatus= window.parent.document.getElementById('divStatus');
							if(typeof(dvStatus)!='undefined'){
								dvStatus.innerHTML = 'Done';
							}
						}
					}else{
						setTimeout("jsLoadDocument("+pageIndex+");",100);
					}
				}else{
					var dvStatus= window.parent.document.getElementById('divStatus');
					if(typeof(dvStatus)!='undefined'){
						dvStatus.innerHTML = 'Error occur and stopped';
					}
				}
			}
		);
	}else{
		var dvStatus= window.parent.document.getElementById('divStatus');
		if(typeof(dvStatus)!='undefined'){
			dvStatus.innerHTML = 'Error occur and stopped';
		}
	}
}

function jsUpdateProgress()
{
	var dvPageMode = window.parent.document.getElementById('divPageMode');
	var dvTotal = window.parent.document.getElementById('divTotal');
	var dvNum = window.parent.document.getElementById('divNum');
	if(typeof(dvPageMode)!='undefined'){
		dvPageMode.innerHTML = pageMode;
	}
	if(typeof(dvTotal)!='undefined'){
		dvTotal.innerHTML = bookdata.spine.length;
	}
	if(typeof(dvNum)!='undefined'){
		dvNum.innerHTML = pageIndex + 1;
	}
}

function jsUpdateProcessImageBook()
{
	var dvPageMode = window.parent.document.getElementById('divPageMode');
	var dvTotal = window.parent.document.getElementById('divTotal');
	var dvNum = window.parent.document.getElementById('divNum');
	var dvStatus= window.parent.document.getElementById('divStatus');					
	if(typeof(dvPageMode)!='undefined'){
		dvPageMode.innerHTML = 'Image book';
	}
	if(typeof(dvTotal)!='undefined'){
		dvTotal.innerHTML = bookdata.spine.length;
	}
	if(typeof(dvNum)!='undefined'){
		dvNum.innerHTML = bookdata.spine.length;
	}
	if(typeof(dvStatus)!='undefined'){
		dvStatus.innerHTML = 'Done';
	}
}

$(document).ready(function(){

	//console.log(pageModeContainerWidth);
<?php if($isImageBook){ ?>
	jsUpdateProcessImageBook();
<?php }else{ ?>
	//jsLoadDocument(pageIndex);
	jsCountPage();
<?php } ?>
});
</script>
<iframe id="InnerPostProcessFrame" width="1px" height="1px" src=""></iframe>
<?php
echo $libebookreader_ui->Get_Layout_Stop();
if($_SESSION['isSchemaUpdate']!=1){
	intranet_closedb();
}
?>