<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();
session_register_intranet('isSchemaUpdate',1);
header("Content-type: text/html; charset=utf-8");

$libebookreader = new libebookreader();
if(isset($_REQUEST['bid'])){
	$ebook_root_path = $intranet_root."/file/eBook/".$bid."/";
	$success = $libebookreader->Parse_EPUB_XML($ebook_root_path);
	if($success){
		//$book_id = $libebookreader->BookID;
		$book_id = $bid;
		echo 'BookID '.$book_id.'<br>';

		$x = '<div>Page Mode: <span id="divPageMode"></span> page mode<br>';
		$x .= 'Total number of files: <span id="divTotal"></span><br>';
		$x .= 'Current processing file: <span id="divNum"></span><br>';
		$x .= 'Process status: <span id="divStatus">processing...</span><br>';
		$x .= '</div><br>';
		$x .= '<iframe id="PostProcessFrame" width="1px" height="1px" src="/home/eLearning/ebook_manage/'.$book_id.'/reader/"></iframe>';
	}
}

echo $x;

echo '<br>';
echo '<a href="index.php" title="Go back to index page">Back</a>';

?>