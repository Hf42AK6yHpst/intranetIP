<?php

//using: 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//var_dump($intranet_session_language);
include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
//include_once($PATH_WRT_ROOT."lang/lang.b5.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();

// additional javascript for eLibrary only
include_once("elib_script.php");

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

### Title ###
//$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];

$lelib = new elibrary();

$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$ParArr["UserID"] = $_SESSION["UserID"];

$settingArr = $lelib->GET_USER_BOOK_SETTING($ParArr);

$NumReviewer = $settingArr[0]["DisplayReviewer"];
$NumReview = $settingArr[0]["DisplayReview"];
$NumRecommend = $settingArr[0]["DisplayRecommendBook"];
$NumWeeklyHit = $settingArr[0]["DisplayWeeklyHitBook"];
$NumHit = $settingArr[0]["DisplayHitBook"];

if($NumReviewer == 0 || $NumReviewer == "")
$NumReviewer = 10;

if($NumReview == 0 || $NumReview == "")
$NumReview = 10;

if($NumRecommend == 0 || $NumRecommend == "")
$NumRecommend = 4;

if($NumWeeklyHit  == 0 || $NumWeeklyHit  == "")
$NumWeeklyHit  = 1;

if($NumHit  == 0 || $NumHit  == "")
$NumHit = 4;

$totalPage = 0;
$DisplayNumPage = 0;

// additional javascript for eLibrary only
include_once("elib_script_function.php");

//debug_r($lelib->GET_RECOMMEND_BOOK($NumRecommend));

if($plugin['koha'])
{
	$koha_btn = "<a href='admin/login_koha.php' target='_blank'>Go to Koha</a><br />";
}

$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/JavaScript">
<!--

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">

<!-- Setting logo -->
<?
if($lelib->IS_ADMIN_USER($_SESSION["UserID"]))
{
?>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_01.gif" width="4" height="4"></td>
<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_03.gif" width="4" height="4"></td>
</tr>
<tr>
<td width="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td bgcolor="#d7e9f4"><a href="#" class="menuon" onClick="MM_openBrWindow('elib_setting.php','','scrollbars=yes')"> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_manage.gif" width="20" height="20" border="0" align="absmiddle"><?=$eLib["html"]["settings"]?></a></td>
<td width="4" bgcolor="#d7e9f4">&nbsp;</td>
</tr>
<tr>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_07.gif" width="4" height="4"></td>
<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td width="4" height="4" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_09.gif" width="4" height="4"></td>
</tr>
</table>
<?
}
?>
<!-- End Setting logo -->
	<!-- Template , Left Navigation panel --> 
	<?php include_once('elib_left_navigation.php'); ?>
	
</td>
<td align="center" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td width="45%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="7" height="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_01a.gif" width="7" height="22"></td>
			<td height="22" align="center" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_02a.gif" class="eLibrary_title_heading2"><?=$eLib["html"]["most_active_reviewers"]?></td>
			<td width="7" height="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_03a.gif" alt="" width="7" height="22"></td>
		</tr>
		<tr>
			<td width="7" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_01.gif" width="7" height="12"></td>
			<td align="left" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_02.gif" height="170" valign="top">
			<div id="div" style="height:165px; width:100%; position:absolute; overflow: auto;">
			<?=$lelib->printMostActiveReviewers($lelib->GET_MOST_ACTIVE_REVIEWERS($NumReviewer), $image_path, $LAYOUT_SKIN);?>
			</div>																	
			</td>
			<td width="7" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_03.gif" alt="" width="7" height="12"></td>
		</tr>
		<tr>
			<td width="7" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_04.gif" width="7" height="6"></td>
			<td height="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_05.gif" width="7" height="6"></td>
			<td width="7" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_06.gif" width="7" height="6"></td>
		</tr>
	</table></td>
<td width="55%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" height="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_01b.gif" width="7" height="22"></td>
		<td height="22" align="center" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_02b.gif" class="eLibrary_title_heading2"><?=$eLib["html"]["most_useful_reviews"]?></td>
		<td width="7" height="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_03b.gif" alt="" width="7" height="22"></td>
	</tr>
	<tr>
		<td width="7" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_01.gif" width="7" height="12"></td>
		<td align="left" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_02.gif" height="170" valign="top">
		<div id="div2" style="height:165px; width:100%; position:absolute; overflow: auto;">
		<?=$lelib->printMostUsefulReviews($lelib->GET_MOST_USEFUL_REVIEWS($NumReview), $image_path, $LAYOUT_SKIN);?>
		</div></td>
		<td width="7" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_03.gif" alt="" width="7" height="12"></td>
	</tr>
	<tr>
		<td width="7" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_04.gif" width="7" height="6"></td>
		<td height="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_05.gif" width="7" height="6"></td>
		<td width="7" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_06.gif" width="7" height="6"></td>
	</tr>
</table></td>
</tr>
</table>
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- recommend table -->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="33%" height="30" align="center" bgcolor="#eeeded" class="eLibrary_title_heading"> <?=$eLib["html"]["recommended_books"]?></td>
	<td width="1" height="30" align="center" bgcolor="#eeeded"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="1" height="10"></td>
	<td width="66%" height="30" align="center" bgcolor="#eeeded"><span class="eLibrary_title_heading"><?=$eLib["html"]["bookS_with_highest_hit_rate"]?></span></td>
</tr>
<tr>
	<td align="center" valign="top">
	
	<table width="95%" border="0" cellspacing="0" cellpadding="2">
		<tr>
			<td align="right"><a href="#" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=recommend','listbook','scrollbars=yes')" class="eLibrary_list_all"><?=$eLib["html"]["list_all"]?></a></td>
		</tr>
	</table>
	<?=$lelib->printBookTable($lelib->GET_RECOMMEND_BOOK($NumRecommend), $image_path, $LAYOUT_SKIN, $NumRecommend, "recommend", $eLib);?>
	</td>
	<!-- end recommend table -->
	
	<td width="1" align="center">
	<table width="1%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="1" bgcolor="#d7d7d7"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="1" height="350"></td>
			</tr>
	</table>
	</td>
	
	<td align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td width="40%" align="center" class="eLibrary_title_sub_heading">&nbsp;<?=$eLib["html"]["book_with_hit_rate_last_week"]?>&nbsp;</td>
			<td align="center"><span class="eLibrary_title_sub_heading">&nbsp;<?=$eLib["html"]["book_with_hit_rate_accumulated"]?>&nbsp;</span></td>
		</tr>
		<tr>
			<td align="center" valign="top">
			
			<!-- weekly hit -->
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td align="right"><a href="#" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=weeklyhit','listbook','scrollbars=yes')" class="eLibrary_list_all"><?=$eLib["html"]["list_all"]?></a></td>
					</tr>
				</table>
			
			<?=$lelib->printBookTable($lelib->GET_WEEKLY_HIT_BOOK($NumWeeklyHit), $image_path, $LAYOUT_SKIN, $NumWeeklyHit, "weeklyHit", $eLib);?>
			<!-- end weekly hit -->
			</td>
			
			<td>
			<!-- hit book -->
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td align="right"><a href="#" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=hit','listbook','scrollbars=yes')" class="eLibrary_list_all"><?=$eLib["html"]["list_all"]?></a></td>
					</tr>
				</table>
			<?=$lelib->printBookTable($lelib->GET_HIT_BOOK($NumHit), $image_path, $LAYOUT_SKIN, $NumHit, "hit", $eLib);?>
			<!-- end hit book -->
			</td>
		</tr>
	</table>															
	</td>
	</tr>
</table>
<br>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
