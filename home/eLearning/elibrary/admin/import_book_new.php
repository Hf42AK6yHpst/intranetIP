<?php
// editing by CharlesMa
/******************************************* Changes **********************************************
  *  2012-09-12 (CharlesMa): Force to one page mode & one page width
 * * 2012-04-03 (Carlos): Add input field Relevant Subject
 **************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if ($BookArr["Title"] != "")
{
	//$BookTitle = " (".iconv("UTF-8", "big5"."//IGNORE", $BookArr["Title"]).")";
	$BookTitle = " (".$BookArr["Title"].")";
}


$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$BookTitle,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

### step box
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=1, $CustStepArr='');

### instruction box
$htmlAry['instructionBox'] = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $eLib['Book']["BeforeImportMsg"]);

$sample_file = "ebook_import.xls";
$csvFile = "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$linterface->LAYOUT_START();


$SourceArr = array();
if (is_array($eLib['Source']) && count($eLib['Source'])>0)
{
	foreach($eLib['Source'] as $Key => $Value)	
	{
		$SourceArr[] = array($Key,$Value);
	}
}

$tagNameAry = $LibeLib->returnAvailableTag($assocAry=1);
$avaliableTagName = "";
$delim = "";
foreach($tagNameAry as $tag)
{
	$avaliableTagName .= $delim."\"$tag\"";
	$delim = ", ";
}

$csv_format = "";
$delim = "<br>";
for($i=0; $i<sizeof($Lang['libms']['import_book_new']['ImportCSVDataCol']); $i++){
	if($i!=0) $csv_format .= $delim;
	$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$Lang['libms']['import_book_new']['ImportCSVDataCol'][$i];
}

?>


<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function checkInputFrom(obj)
{
	if(document.form1.upload_file.value == "")
	{
		alert("<?=$eLib["html"]["please_upload_csvfile"]?>");
		return false;
	}
		
	return true;
}

//-->
</script>


<form  method="post" action="import_book_new_validate.php" name="form1" enctype="multipart/form-data" onSubmit="return checkInputFrom(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
	</tr>
	<?=$htmlAry['generalImportStepTbl']?>
	<div style="width:90%;margin-left: 5%;"><?=$htmlAry['instructionBox']?></div>
	<tr>
		<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td><br />
			
			<table align="center" width="80%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td class="formfieldtitle" align="left" width="30%"><?=$iDiscipline['Import_Source_File']?>: 
					<span class="tabletextremark">
					<?=$Lang['General']['XLSFileFormat']?></span>
				</td>
				<td class="tabletext" width="70%">
					<input class="file" type="file" name="upload_file">
				</td>
			</tr>			
			<tr>
				<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
				<td class="tabletext"><?=$csvFile?></td>
			</tr>
			<tr>
				<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
				<td class="tabletext"><?=$csv_format?></td>
			</tr>
			
			</table>
			
			</td>
		</tr>
		</table>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	    <tr>
	    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	    </tr>
	    <tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button","parent.location='index.php'") ?>
			</td>
		</tr>		
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		</table>                                
		</td>
	</tr>
</table>

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>