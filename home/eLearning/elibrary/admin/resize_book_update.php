<?php
// editing by CharlesMa
/******************************************* Changes **********************************************
  *  2012-09-12 (CharlesMa): Force to one page mode & one page width
 * * 2012-04-03 (Carlos): Add input field Relevant Subject
 **************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/libebookreader.php");
intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
$LibeBookreader = new libebookreader();

if (!$LibeLib->IS_ADMIN_USER($UserID))
{     
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     intranet_closedb();
     exit();
}


if($bookID){
	$result = $LibeBookreader->compressBookImage($bookID);
}else{
	$result = array($bookID,-1);
}

echo implode("|==@==|",$result);

intranet_closedb();

?>