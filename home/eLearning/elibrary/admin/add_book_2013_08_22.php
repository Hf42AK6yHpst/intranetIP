<?php
// editing by CharlesMa
/******************************************* Changes **********************************************
  *  2012-09-12 (CharlesMa): Force to one page mode & one page width
 * * 2012-04-03 (Carlos): Add input field Relevant Subject
 **************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
### Title ###
//$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];


if ($BookArr["Title"] != "")
{
	//$BookTitle = " (".iconv("UTF-8", "big5"."//IGNORE", $BookArr["Title"]).")";
	$BookTitle = " (".$BookArr["Title"].")";
}


$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$BookTitle,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();


$SourceArr = array();
if (is_array($eLib['Source']) && count($eLib['Source'])>0)
{
	foreach($eLib['Source'] as $Key => $Value)	
	{
		$SourceArr[] = array($Key,$Value);
	}
}


$Language = "chi";   
$LangArr = array();
$LangArr[] = array("chi",$i_QB_LangSelectChinese);
$LangArr[] = array("eng",$i_QB_LangSelectEnglish);
$LangSelect = $linterface->GET_SELECTION_BOX($LangArr," name='Language' ","", $Language);

$AdultContent = "No"; 
$AdultContentArr = array();
$AdultContentArr[] = array("Yes","Yes");
$AdultContentArr[] = array("No","No");
$AdultContentSelect = $linterface->GET_SELECTION_BOX($AdultContentArr," name='AdultContent' ","", $AdultContent);

$tagNameAry = $LibeLib->returnAvailableTag($assocAry=1);
$avaliableTagName = "";
$delim = "";
foreach($tagNameAry as $tag)
{
	$avaliableTagName .= $delim."\"$tag\"";
	$delim = ", ";
}
?>




<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function checkInputFrom(obj)
{
	if(document.form1.Title.value == "")
	{
		alert("<?=$eLib["html"]["please_enter_title"]?>");
		return false;
	}
	
	if(document.form1.Author.value == "")
	{
		alert("<?=$eLib["html"]["please_enter_author"]?>");
		return false;
	}
	
	return true;
}

function onSelectSource(){
	var theCopy = document.getElementById("copyrightTR");
	var theSelect = document.getElementById("SourceFrom");
	if ($("#Publisher").val()=="")
	{
		$("#Publisher").val($("#SourceFrom option:selected").text());
	}

	if(theSelect.value == "cup")
	{
		theCopy.style.display = "block";
	} else
	{
		theCopy.style.display = "none";	
	}

}

function onSelectImageBook()
{
	var sel = document.getElementById("ImageBook");
	var tr = document.getElementById('imageWidthTR');
	if(sel && tr){
		if(sel.value == '1'){
			tr.style.display = '';
		}else{
			tr.style.display = 'none';
		}
	}
}

function onSelectForceToOnePageMode() //2013-3-13 Charles Ma
{
	var sel = document.getElementById("ForceToOnePageMode");
	var tr = document.getElementById('onePageModeWidthTR');
	var tr2 = document.getElementById('onePageModeHeightTR');
	var tr3 = document.getElementById('onePageModeCoverTR');
	if(sel && tr){
		if(sel.value == '1'){
//			tr.style.display = '';
//			tr2.style.display = '';
			tr3.style.display = '';
		}else{
//			tr.style.display = 'none';
//			tr2.style.display = 'none';
			tr3.style.display = 'none';
		}
	}
}
//-->
</script>


<form name="form1" method="post" action="add_book_info_update.php" enctype="multipart/form-data" onSubmit="return checkInputFrom(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
		<td><br />
		
		<table align="center" width="80%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><span class="tabletextrequire">*</span> <?=$eLib['Book']["Title"]?></span></td>
			<td>
			<input type="text" name="Title"  size="100%" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><span class="tabletextrequire">*</span> <?=$eLib['Book']["Author"]?></span></td>
			<td>
			<input type="text" name="Author"  size="100%" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["SeriesEditor"]?></span></td>
			<td>
			<input type="text" name="SeriesEditor"  size="100%" />
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Category"]?></span></td>
			<td>
			<input type="text" name="Category"  size="100%" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Subcategory"]?></span></td>
			<td>
			<input type="text" name="SubCategory"  size="100%" />	
			</td>
		</tr>	
		
		<!--
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['html']["RelevantSubject"]?></span></td>
			<td>
			<input type="text" name="RelevantSubject"  size="100%" />	
			</td>
		</tr>
		-->
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$Lang['StudentRegistry']['Tag']?></span></td>
			<td>
			<input type="text" name="BookTags" id="BookTags"  size="100%" value="<?=$BookTags?>" /> <span class="tabletextremark"><?=$Lang['StudentRegistry']['CommaAsSeparator']?></span>
			</td>
		</tr>
			
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Level"]?></span></td>
			<td>
			<input type="text" name="Level"  />	
			</td>
		</tr>	
				
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["AdultContent"]?></span></td>
			<td>
			<?=$AdultContentSelect?>
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib["SourceFrom"]?></span></td>
			<td>
				<select id="SourceFrom" name="SourceFrom" onchange="onSelectSource()">
				<?
					for ($i=0; $i<sizeof($SourceArr); $i++) {
						list($ID, $Name) = $SourceArr[$i];
						echo "<option value=\"{$ID}\">{$Name}</option>\n";
					}
				?>
				</select>
			</td>
		</tr>	

		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Publisher"]?></span></td>
			<td>
			<input type="text" name="Publisher" id="Publisher"  size="100%" />	
			</td>
		</tr>	
		
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Language"]?></span></td>
			<td>
			<?=$LangSelect?>
			</td>
		</tr>	
		
		<!----------------  2013-05-30 (CharlesMa) ------------------------>	
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["FirstPublished"]?></span></td>
			<td>
			<input type="text" name="FirstPublished" id="FirstPublished"  size="100%" value="<?=date('Y')?>" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["ePublisher"]?></span></td>
			<td>
			<input type="text" name="ePublisher" id="ePublisher"  size="100%" value="BroadLearning Education (Asia) Ltd."/>	
			</td>
		</tr>
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["CopyrightYear"]?></span></td>
			<td>
			<input type="text" name="CopyrightYear" id="CopyrightYear"  size="100%" value="Copyright&#169;BroadLearning Education (Asia) Limited <?=date('Y')?>"/>	
			</td>
		</tr>
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["CopyrightStatement"]?></span></td>
			<td>
			<?=$linterface->GET_TEXTAREA("CopyrightStatement", "This publication is in copyright. All rights reserved. No part of this publication may be reproduced, stored in a retrieval system, or transmitted in any form or by any means, electronic, mechanical, recording or otherwise without the prior written permission of the publishers.", 100, 5)?>	
			</td>
		</tr>
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["IndexPageImage"]?></span></td>
			<td><input type="file" name="IndexPageImage" id="IndexPageImage" size="40" accept="image/*"></td>
		</tr>
		<!----------------  2013-05-30 (CharlesMa) END ------------------------>	
		<!----------------  2013-06-14 (CharlesMa) ------------------------>	
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["WorkSheet"]?></span></td>
			<td><input type="file" name="WorkSheet" id="WorkSheet" size="40"></td>
		</tr>
		
		<!----------------  2013-06-14 (CharlesMa) END ------------------------>	
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Description"]?></span></td>
			<td>	
			<?=$linterface->GET_TEXTAREA("Preface", "", 100, 5)?>
			</td>
		</tr>
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["ePUB"] ?></span></td>
			<td><input type="file" name="upload_file" size="40" /></td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["ePUBIsImageBook"]?></span></td>
			<td><select name="ImageBook" id="ImageBook" onchange="onSelectImageBook();">
					<option value='1'><?=$Lang['General']['Yes']?></option>
					<option value='' selected='selected'><?=$Lang['General']['No']?></option>
				</select></td>
		</tr>
		
		<tr id="imageWidthTR" style="display:none;">
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["ePUBImageWidth"]?></span></td>
			<td><input type="text" name="ImageWidth" id="ImageWidth" size="10"> <?=$eLib['Book']["ePUBImageWidthUnit"]?></td>
		</tr>
		
<!----------------  2012-09-12 (CharlesMa) ------------------------>	
			
		<tr <? if(!$sys_custom['eBook']['addBook']['forceToOnePageMode']) echo 'style="display:none;"'; ?>>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["forceToOnePageMode"]?></span></td>
			<td><select name="ForceToOnePageMode" id="ForceToOnePageMode" onchange="onSelectForceToOnePageMode();">
					<option value='1'><?=$Lang['General']['Yes']?></option>
					<option value='0' selected='selected'><?=$Lang['General']['No']?></option>
				</select></td>
		</tr>
				
		<tr id="onePageModeCoverTR" style="display:none;">
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["onePageModeCover"]?></span></td>
			<td><input type="file" name="onePageModeCover" id="onePageModeCover" size="40" accept="image/*"></td>
		</tr>
		
		<tr id="onePageModeWidthTR" style="display:none;">
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["onePageModeWidth"]?></span></td>
			<td><input type="text" name="OnePageModeWidth" id="OnePageModeWidth" size="10"> <?=$eLib['Book']["ePUBImageWidthUnit"]?></td>
		</tr>
		
		<tr id="onePageModeHeightTR" style="display:none;">
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["onePageModeHeight"]?></span></td>
			<td><input type="text" name="OnePageModeHeight" id="OnePageModeHeight" size="10"> <?=$eLib['Book']["ePUBImageWidthUnit"]?></td>
		</tr>
		
<!----------------  2012-09-12 (CharlesMa) END ------------------------>		

		<? 
			$TmpCpy = $LibeLib->GET_COPYRIGHT_TEMPLATE("cup");
		?>
		<tr id="copyrightTR" style="display:none;">
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Copyright"]?></span></td>
			<td>
			<!--<?=$linterface->GET_TEXTAREA("Copyright", $BookArr['Copyright'], 100, 8)?>-->
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr><td><?=$TmpCpy?></td></tr>
			</table>
			
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr><td>[JS1]: </td><td><input type="text" value="" name="JS1"/></td></tr>
			<tr><td>[JS2]: </td><td><input type="text" value="" name="JS2"/></td></tr>
			<tr><td>[JS3]: </td><td><input type="text" value="" name="JS3"/></td></tr>
			<tr><td>[JS4]: </td><td><input type="text" value="" name="JS4"/></td></tr>
			<tr><td>[JS5]: </td><td><input type="text" value="" name="JS5"/></td></tr>
			</table>
			</td>
		</tr>	

		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib["html"]["publish"]?></span></td>
			<td>		
			<input type="radio" id="Publish" name="Publish" value="1" checked /> <?=$eLib["html"]["publish"]?> 
			<input type="radio" id="Publish" name="Publish" value="0"  /> <?=$eLib["html"]["unpublish"]?>	
			</td>
		</tr>			
		
		</table>
		
		</td>
	</tr>
	</table>
	</td>
</tr>


<tr>
	<td colspan="2">        
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
    <tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button","parent.location='index.php'") ?>
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	</table>                                
	</td>
</tr>
</table>

<script language="javascript">

$('#BookTags').multicomplete({
	source: [<?=$avaliableTagName?>]
});

</script>

<input type="hidden" name="BookID" id="BookID" value="<?=$BookID?>" />
<input type="hidden" name="InputBy" id="InputBy" value="<?=$InputBy?>" />

</form>

<?php
intranet_closedb();

//echo $linterface->FOCUS_ON_LOAD("form1.InputFile");
$linterface->LAYOUT_STOP();

?>