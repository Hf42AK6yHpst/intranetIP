<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


## Prompt login
include_once("../../../check_admin.php");


intranet_opendb();
###########################################
$linterface 	= new interface_html();
$lu 			= new libuser($UserID);
$LibeLib 		= new elibrary();

$search = isset($_REQUEST['search'])? trim($_REQUEST['search']) : "";

$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = "Install License";
$TAGS_OBJ[] = array($plugin['eLib_license'] == 1? "Install Site License" : "Install Student Book License");


//IF (b.Publish='1','"."<b>Y</b>"."', '"."<font color=gray>N</font>"."'),

if(!empty($search)){
	$condition = " AND (b.BookID like '%".$search."%' OR SUBSTRING((b.BookID+100000000), 2) like '%".$search."%' OR Title like '%".$search."%' OR Author like '%".$search."%') ";
	
}

if($plugin['eLib_license'] == 1){
	## Site License
$sql = "
			SELECT
				CONCAT('<b>', lpad(b.BookID,8,0)  , '</a>')  as code, 
				CONCAT('<b>', b.Title , '</a>') as Title,
				b.Author,
				b.Publisher,
				IF (b.Source='green','".$eLib['Source']["green"]."', '".$eLib['Source']["cup"]."') ,
				IF (b.Language='eng','".$i_QB_LangSelectEnglish."', '".$i_QB_LangSelectChinese."'),
				b.DateModified,
				IF (bl.BookID !='','"."<b>Y</b>"."', '"."<font color=gray>N</font>"."') as enabled,
				CONCAT('<input type=\"checkbox\" name=\"BookID[]\" value=\"',b.BookID,'\" />')					
			FROM
				INTRANET_ELIB_BOOK as b
			LEFT JOIN 
				INTRANET_ELIB_BOOK_ENABLED as bl
			ON
				b.BookID = bl.BookID 
			WHERE 1 and b.Publish=1 ".$condition;
			
		  
}else{
	## Student License
	//and SUM( bl.NumberOfCopy)>=0
	## Get list of books with invalid quota
	$strInvalid = "";
	$extra = "";
	$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK_LICENSE WHERE md5(CONCAT(BookID,'_',NumberOfCopy,'_',InputDate)) != CheckKey";
	$aryCheck = $LibeLib->returnVector($sql);
	if($aryCheck != array() && $aryCheck[0] != ""){		
		$strInvalid = implode(",", $aryCheck);
		$extra = ", IF (bl.BookID IN ($strInvalid), '*', '')"; 
		
		//$condition .= " AND bl.BookID NOT IN ($strInvalid)";
	}
	

	$sql = "
			SELECT
				CONCAT('<b>', lpad(b.BookID,8,0) , '</a>')  as code, 
				CONCAT('<b>', b.Title , '</a>') as Title,				
				b.Author,
				b.Publisher,
				IF (b.Source='green','".$eLib['Source']["green"]."', '".$eLib['Source']["cup"]."') ,
				IF (b.Language='eng','".$i_QB_LangSelectEnglish."', '".$i_QB_LangSelectChinese."'),
				b.DateModified,
				IF (bl.BookID !='' , CONCAT('<a href=\"install_quota_history.php?BookID=', bl.BookID,'&TB_iframe=true&amp;height=500&amp;width=600\" class=\"thickbox\">',SUM( bl.NumberOfCopy) $extra, '</a>') , '"."<font color=gray>Disabled</font>"."') as enabled,						
				CONCAT('<input type=\"checkbox\" name=\"BookID[]\" id=\"BookID_',b.BookID,'\" value=\"',b.BookID,'\" />')					
			FROM
				INTRANET_ELIB_BOOK as b
			LEFT JOIN 
				INTRANET_ELIB_BOOK_LICENSE as bl
			ON
				b.BookID = bl.BookID 
			WHERE 1 
				".$condition."
			GROUP BY 
			    b.BookID
		  ";
//echo $sql;
}



if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 1;
$order = ($order == '' || $order != 0) ? 1 : 0;

$li = new libdbtable2007($field, $order, $pageNo);
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
$li->page_size = ($page_size_change!="") ?$numPerPage : $li->page_size;

if($plugin['eLib_license'] == 1){
	$li->field_array = array("b.BookID", "Title", "Author", "Publisher", "Source", "Language", "DateModified",  "bl.BookID");
}else{
	$li->field_array = array("b.BookID", "Title", "Author", "Publisher", "Source", "Language", "DateModified","SUM( bl.NumberOfCopy)");
}


$li->sql = $sql;//$LibeLib->GET_BOOK_OVERVIEW_SQL($TmpArr);
//echo htmlspecialchars($li->sql);

$li->IsColOff = "2";
$li->no_col = 10;
$li->column_array = array(12,12,12,12,12,0,0,0);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletop tabletopnolink'>"."#"."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Code"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Title"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Author"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Publisher"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib["SourceFrom"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_QB_LangSelect)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["DateModified"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $plugin['eLib_license']==1? $eLib["admin"]["Enable"] : $eLib['admin']['Enabled_Quota'])."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("BookID[]")."</td>\n
";
####################################################

$linterface->LAYOUT_START();

?>
<link href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>

<BR />
<BR />
<form name="form1" method="POST">
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr><td>
	<input type="text" name="search" id="search" value="<?=$search?>" /><input type="button" name="search" id="search" value="search"  onClick="document.form1.submit();"  />
</td></tr>
<tr>
	<td align="left">
		<?php if($plugin['eLib_license'] == 1){ ?>
			<a href="install_import.php?type=status&TB_iframe=true&amp;height=500&amp;width=600" class="thickbox" title="Import Status">Import Status</a>
		<?php }else{?>
			<a href="install_import.php?type=quota&TB_iframe=true&amp;height=500&amp;width=600" class="thickbox" title="Import Quota">Import Quota</a>
		<?php }?>
	</td>
	<td align="right">
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
			<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td nowrap>
					<?php if($plugin['eLib_license'] == 1){ ?>
					<!-- SITE LICENSE -->
						<a href="install_status_change.php?enable=1&TB_iframe=true&amp;height=500&amp;width=600" class="thickbox tabletool" title="Enable License">
						Enable</a>&nbsp;&nbsp;
						<a href="install_status_change.php?enable=0&TB_iframe=true&amp;height=500&amp;width=600" class="thickbox tabletool" title="Disable License">
						Disable</a>
					<?php }else{?>
					<!-- STUDENT LICENSE -->
						<a href="install_quota.php?TB_iframe=true&amp;height=500&amp;width=600" class="thickbox tabletool" title="Modify Quota">
						Modify Quota</a>	
					<?php }?>
					
				</td>
			</tr>
			</table>
			</td>
			<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">
		<?=$li->display();?>
	</td>
</tr>
</table>

<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />
	

</form>
<?php

$linterface->LAYOUT_STOP();
###########################################
intranet_closedb();

?>