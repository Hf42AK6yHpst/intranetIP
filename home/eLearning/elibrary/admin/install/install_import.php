<?php


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_opendb();
###########################################
$msg = ""; 

## Init Library
$LibeLib = new elibrary();
$linterface 	= new interface_html("popup5.html");

## Get Post Variables
$type = isset($_REQUEST['type'])? strtolower(trim($_REQUEST['type'])) : "";
$xmsg = isset($_REQUEST['xmsg'])? trim($_REQUEST['xmsg']) : "";

if($type == ""){
	echo("Import method not selected. Please close popup and try again.");
	die();
}

if($plugin['eLib_license'] == 1){
	$heading = "Status";
	$form_action = "install_status_import_update.php";
	$header_fail_msg  = "BookID, Enable";
}else{
	## quota
	$heading = "Quota";
	$form_action = "install_quota_import_update.php";
	$header_fail_msg  = "BookID, Quota";	
}

$sample_import = GET_CSV("Sample_".$heading."_Import.csv");

if(!empty($xmsg)){
	switch($xmsg){
		case "import_failed":
			$msg = "Wrong import file type. File must be of type csv or txt.";
			break;
		case "import_header_failed";
			$msg = "Header in import file mismatch. Please re-check header matches the following: ".$header_fail_msg."";
			break;
		case "import_no_record":
			$msg = "No data for importing.";
			break;
	}
}


$linterface->LAYOUT_START();
?>
<script>

	function submit_import(){
		if(confirm("Are you sure you want to import?")){
			document.form1.submit();
		}		
	}
</script>
<form name="form1" id="form1" Method="post" action="<?=$form_action?>" enctype="multipart/form-data" style="margin:0px;padding="0px" >

<div style="height:490px;width:627px;background:white;">
	<div id="display_content" style="height:470px;overflow-y:auto;">
	
	<BR />	
	<table width="100%">	
	<tr><td align="center">	
		<table width="90%" cellpadding="0" cellspacing="0">
			<?php
			if(!empty($msg)){
				?>
				<tr><td colspan="3" align="right">
				<BR />
				<span class="systemmsg" style="color:red;font-weight:bold;"><?=$msg?></span>
				<br />
				<br /> 
				</td></tr>
				<?php
			}
			?>
			<tr style="font-weight:bold;" height="30px">
				<td width="100">Import File </td>
				<td width="10">:</td>
				<td width="*"><input type="file" name="csvfile" id="csvfile" size="50" /></td>			
			</tr>
			<tr style="font-weight:bold;" height="30px">
				<td>Sample</td>
				<td>:</td>
				<td>
					<a href="<?=$sample_import?>" target="_blank">Sample_<?=$heading?>_Import_unicode.csv</a>
				</td>			
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Header format : <?=$header_fail_msg?></td>
			</tr>				
		</table>
	</td></tr>
	</table>
	</div>
	
	<div id="btn_panel" style="height:40px;background:#EEE;text-align:center;">	
		<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
			<tr><td align="center" valign="center">
				<input type="button" name="close_booklist" id="close_booklist" value="Confirm Import" onClick="submit_import();"/>
				<input type="button" name="close_booklist" id="close_booklist" value="Close" onClick="window.parent.tb_remove();"/>
			</td></tr>
		</table>
			
	</div>		
</div>
</form>

<?php
$linterface->LAYOUT_STOP();

###########################################
intranet_closedb();

?>