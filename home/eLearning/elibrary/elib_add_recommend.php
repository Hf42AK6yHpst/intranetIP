<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once("elib_script.php");

// additional javascript for eLibrary only

intranet_opendb();
#################################

$lelib = new elibrary();

$linterface 	= new interface_html("popup5.html");

## Redirect if user is not ADMIN
$isAdmin = $lelib->IS_ADMIN_USER($_SESSION["UserID"]);

if(!$isAdmin)
	header("Location: book_detail.php?BookID=$BookID");	

$CurrentPage	= "PageMyeClass";

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];

$tab_title = $lelib->getPageTabTitle();

$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $eLib["html"]["recommend_this_book"].$tab_title;
$MODULE_OBJ["logo"] = "";
$customLeftMenu = ' ';

$ParArr["BookID"] = $BookID;
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$RecArr = $lelib->getBookRecommend($ParArr);

if(count($RecArr) <= 0){
	$Mode 				= "add";
	$WholeSchoolChecked = "Checked";
}
else
{
	$Mode 				= "edit";
	$WholeSchoolChecked = "";
	$DefaultDescription = $RecArr[0]["Description"];
	$DefaultGroup 		= $RecArr[0]["RecommendGroup"];
	$GroupArr 			= explode("|", $DefaultGroup);
}

$returnArr = $lelib->getBookInformation($ParArr);

if($returnArr != "")
{	
	$Title = $returnArr[0]["Title"];
	$Author = $returnArr[0]["Author"];
	$Source = $returnArr[0]["Source"];
	$Category = $returnArr[0]["Category"];
	$SubCategory = $returnArr[0]["SubCategory"];
	$Publisher = $returnArr[0]["Publisher"];
	$Level = $returnArr[0]["Level"];
	$DateModified = $returnArr[0]["DateModified"];
	$Description = $returnArr[0]["Preface"];

	if($Description == "")
	$Description = "--";
	
	if($Level == "")
	$Level = "--";
	
	$Source = $lelib->getSourceFullName($Source);
	
	$reviewArr = $lelib->getReview($ParArr);
	
	$reviewNum = count($reviewArr);
	
	$sumRating = 0;
	for($c = 0; $c < $reviewNum ; $c++)
	{
		$sumRating += $reviewArr[$c]["Rating"];
	}
	
	if($reviewNum > 0)
		$rating = $sumRating / $reviewNum;
	else
		$rating = 0;

	//// draw star ////////////////////////////////////////////////////////////////////////////////////////
	
	$x = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
	$x .= "<td class=\"tabletextremark\"><a href=\"book_detail.php?BookID=$BookID\" class=\"contenttool\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_comment.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">".$eLib["html"]["rating"]."</a></td>";
	$x .= "<td>";
	for ($star = 1; $star <= 5; $star++)
	{
		if($star <= $rating)
		{
		$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_on.gif\" width=\"15\" height=\"20\">";
		}
		else
		{
			if($star <= $rating + 0.5)
			$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_half.gif\" width=\"15\" height=\"20\">";
			else
			$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_off.gif\" width=\"15\" height=\"20\">";
		}
	}
	$x .= "</td></tr></table>";
	$starImg = $x;

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
} //end if get book detail

////////////////////////////////////////////////////////////
	$sql = "select YearName, YearID from YEAR order by Sequence";

    $classLevelArr = $lelib->returnArray($sql,2);
    
    $classSize = count($classLevelArr);
    
    if($classSize>0){
	    $checkFormList .= '<table border="0"><tr valign="top">';
	    $index = 0;
		for($i = 0; $i < $classSize; $i++)
		{
			if($index == 5){
				$checkFormList .= '</tr><tr valign="top">';
				$index = 0;
			}
			$index++;
			
			// check the class group is selected
			if($Mode == "edit")
			{
				$checked = "";
				for($k = 0; $k < count($GroupArr); $k++)
				{
					if(($classLevelArr[$i]["YearID"] == $GroupArr[$k] && $GroupArr[$k] != "") || $GroupArr[$k] == -1)
					{
						$checked = "checked";
						break;
					}
				}
			}
			else
				$checked = "checked";
			
			$ClassCheckBox = "<input type='checkbox' name='YearID[]' id='YearID_".$classLevelArr[$i]["YearID"]."' value='".$classLevelArr[$i]["YearID"]."' ".$checked." >";
			$checkFormList .= "<td>".$ClassCheckBox."<label for='YearID_".$classLevelArr[$i]["YearID"]."'>".$classLevelArr[$i]["YearName"]."</label>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		}
		$checkFormList .= '</tr></table>';
    }

$linterface->LAYOUT_START();

?>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function submitForm()
{
	document.form1.submit();
} // end confirm form

var AddRecommend = {
	'_BookID' : <?=$BookID?>,
	'_serverScript' : 'elibrary_ajax.php',
	'_Mode'	: '<?=$Mode?>',
	init:function(){},
	
	SetAllSchool: function(obj){		
		var setAll = $(obj).is(':checked');
		$("input[name=YearID[]]").attr("checked", setAll);
	},
	
	onSubmit: function(){
		var wholeSchool = $("#wholeSchool:checked")? true: false;
		var description = $("[name=Description]").val();
		var recommendSchool = "";
		$("input[name=YearID[]]:checked").each(function(){
			recommendSchool += (recommendSchool!="")? "|"+$(this).val() : $(this).val();
		});
		
		$.post(this._serverScript,
		{method: 'add_recommend', BookID: this._BookID, Mode: this._Mode,
		recommendSchool: recommendSchool, description: description},
		function(data, status){
			if(data){
				AddRecommend.closeThickbox();
			}
		});		
	},
	onCancel: function(){
		this.closeThickbox();
	},
	closeThickbox: function(){
		window.parent.tb_remove();
	}
};


//-->
</script>

<form name="form1" action="" method="post" style="margin:0px;padding:0px;">

<!-------------- Content Form ------------------>
<div style="text-align:left;height:347px; padding: 0 0 0 20px;background:#FFFFFF;">

<BR />
<span class="" style="font-weight:bold;"><?=$Title?></span>
<BR />
<BR />

<table width="100%" >
<tr valign="top">
<td>
	<span class="tabletext"><?=$eLib["html"]["recommend_to"]?></span>
	<BR />
	<input type="checkbox" name="wholeSchool" id="wholeSchool"  onClick="AddRecommend.SetAllSchool(this);" <?=$WholeSchoolChecked?> />
	<label for="wholeSchool"><span class="tabletext"><?=$eLib["html"]["whole_school"]?></span></label>
	<BR /><BR />
	<span class="tabletext">
		<?=$checkFormList?>
	</span>
</tr>
<tr valign="top">
<td >
	<BR />
	<span class="tabletext"><?=$eLib["html"]["reason_to_recommend"]?></span>
	<BR />
	<?=$linterface->GET_TEXTAREA("Description", $DefaultDescription, 100, 5)?>
</td>
</tr>
</table>
</div>

<!--  BUTTON PANEL -->
<div style="text-align:center;height:50px; padding: 15px 0 0 20px;">
<input name="Submit" type="button" class="formbutton" onClick="AddRecommend.onSubmit();"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$eLib["html"]["submit"]?>">
&nbsp;
<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$eLib["html"]["reset"]?>">
&nbsp;
<input name="Cancel" type="button" class="formbutton" onClick="AddRecommend.onCancel();" value="<?=$eLib["html"]["cancel"]?>">
</div>


<!-------------- End Content Form -------------->

<input type="hidden" name="BookID" value="<?=$BookID?>" >
<input type="hidden" name="Mode" value="<?=$Mode?>" >
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>