<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID) || $ReviewID<=0 || $BookID<=0)
{
     header ("Location: /");
     intranet_closedb();
     exit();
}


if ($LibeLib->REMOVE_BOOK_REVIEW($BookID, $ReviewID))
{
	$xmsg = "DeleteSuccess";
} else
{
	$xmsg = "DeleteUnsuccess";
}


intranet_closedb();

header("Location: book_detail.php?BookID=".$BookID."&xmsg=DeleteSuccess");


?>