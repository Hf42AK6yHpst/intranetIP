<?php
/*
 * 	Log
 * 	
 * 	Date:	2015-08-31 [Cameron] create this file
 * 
 */

$PATH_WRT_ROOT = "../../../";


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();
$LibeLib = new elibrary();

if (!$LibeLib->IS_ADMIN_USER($_SESSION["UserID"]))
{
	header("location: /home/");
	die();
}
 
$QuotationID = $_POST['QuotationID'];
$task = $_POST['task'];
//error_log("\n\n _POST-->".print_r($_POST,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");

switch($task) {
	case 'select':
		$bookID = $_POST['AvailableBookID'];
		break;
	case 'deselect':
		$bookID = $_POST['SelectedBookID'];
		break;
	default:
		break;
}


$libelibinstall = new elibrary_install(); 
$ret = $libelibinstall->update_ebook_selection($QuotationID, $bookID, $task);
echo $ret;
 
intranet_closedb(); 
 
?>