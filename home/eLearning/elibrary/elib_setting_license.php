<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
$Libelibinstall = new elibrary_install();

if (!$LibeLib->IS_ADMIN_USER($_SESSION["UserID"]))
{
	header("location: /home/");
	die();
}


include_once("elib_script.php");


$linterface = new interface_html();
//$toolbar = $linterface->GET_LNK_ADD("elib_setting_tags_new.php",$button_new,"","","",0);

//2013-08-19 Charles Ma
if(!$Libelibinstall->retrive_license_enable())
	$license_style = 'style="display:none;"';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

<script language="JavaScript" src="/templates/script.js"></script>
<script language="JavaScript" src="/templates/2007script.js"></script>
<link href="css/text" rel="stylesheet" type="text/css">
<link href="css/content.css" rel="stylesheet" type="text/css">
<link href="css/topbar.css" rel="stylesheet" type="text/css">
<link href="css/leftmenu.css" rel="stylesheet" type="text/css">
<link href="css/footer.css" rel="stylesheet" type="text/css">
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<style type="text/css">
</style>
<script>
function JSDeleteTag(TagID)
{
	if (confirm("<?=$eLib['admin']['Confirm_Del_Tag']?>"))
	{
		self.location = "elib_setting_tags_delete.php?TagID=" + TagID;
	}
}
</script>

</head>

<body background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_prev_month_on.gif','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_next_month_on.gif')">

<form name="form1" action="">
<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="50" valign="top"> 
				<table width="101%" border="0" cellspacing="0" cellpadding="0">
              	<tr> 
	                <td width="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" width="10" height="41"></td>
	                <td width="200" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" class="title"><?=$eLib['html']['elibrary_settings']?></td>
	                <td width="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_02.gif" ></td>
	                <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_03.gif">&nbsp;</td>
	                <td width="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_04.gif" width="22" height="41"></td>
              	</tr>
            	</table>
            </td>
        </tr>
        <tr> 
          <td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
                <td width="10" valign="top">&nbsp;</td>
                <td>
                	<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="13" height="33"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_01_t.gif" width="13" height="33"></td>
						<td height="33" valign="bottom" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_02_t.gif" class="imailpagetitle">
							<!--
							<table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="left">&nbsp;<span class="contenttitle"><?=$eLib['html']['portal_display_settings']?></span></td>
								<td align="right"><label></label></td>
							</tr>
							</table>
							-->
							<div id="Content_tab">
											<ul>
											<li ><a href="elib_setting.php"><span><font size="-1"><?=$eLib['html']['portal_display_settings']?></font></span></a></li>
											<li ><a href="elib_setting_tags.php"><span><font size="-1"><?=$Lang['StudentRegistry']['Tag']?></font></span></a></li>
											<li <?=$license_style?> id="current"><a href="elib_setting_license.php"><span><font size="-1"><?=$Lang['StudentRegistry']['License']?></font></span></a></li>
											<!--<li><a href="eLibrary_admin_book_list.htm"><span>Book List</span></a></li>-->
										</ul>
									</div>
						</td>
						<td width="11" height="33"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_03_t.gif" width="11" height="33"></td>
					</tr>
					
					
					<tr>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_04.gif" width="13" height="13"></td>
						<td align="center" bgcolor="#FFFFFF">

						<table width="98%" border="0" cellpadding="10" align="center"><tr><td>
						
						<div class="content_top_tool">
	<?=$toolbar?>
	<!--<div class="Conntent_search"><input name="keyword" id="keyword" type="text" value="<?=stripslashes($keyword)?>" /></div>-->     
	<br style="clear:both" />
</div>

                          <div class="table_board">

							<?= $Libelibinstall->retrieve_elib_license_setting_html() ?>
                            
                          </div>
						</td></tr></table>
										
						<br />
	    				</td>
	    				<td width="11" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_06.gif" width="11" height="13"></td>
					</tr>
					<tr>
						<td width="13" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_07.gif" width="13" height="10"></td>
						<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_08.gif" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_08.gif" width="13" height="10"></td>
						<td width="11" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_09.gif" width="11" height="10"></td>
					</tr>
					</table>
					<br>
				</td>
			</tr>
	        </table>
		</td>
	</tr>
	</table>
</td>
</tr>
</table>

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/board_scheme_03.gif" width="4" height="4">
</form>

</body>
</html>
<?php

intranet_closedb();
?>