<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();
#################################################################
$strBookStudentID = isset($_REQUEST['strBookStudentID'])? $_REQUEST['strBookStudentID'] : "";

if($strBookStudentID==""){
	intranet_closedb();
	echo $eLib['SystemMsg']['Err_NoStudentsSelected'];
	die();
}  

$objInstall	= new elibrary_install();

if($objInstall->delete_book_student($strBookStudentID)){
	echo 1;
}else{
	echo 0;
}




#################################################################
intranet_closedb();
?>