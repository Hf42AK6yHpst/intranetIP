<?php
//using by 
/**
 * Change Log:
 * 2018-12-11 Pun
 *  - Added ISBN
 */
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");


// additional javascript for eLibrary only
//include_once("elib_script.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
######################################################

$linterface 	= new interface_html("popup.html");
$lelib = new elibrary();
$CurrentPage	= "PageMyeClass";
$objInstall 	= new elibrary_install();

## Redirect if user is not ADMIN
$isAdmin = $lelib->IS_ADMIN_USER($_SESSION["UserID"]);

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];
$TAGS_OBJ[] = array($title,"");

$tab_title = $lelib->getPageTabTitle();

$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] = "";
$customLeftMenu = ' ';
//$CurrentPageArr['eLib'] = 1;


$ParArr["BookID"] = $BookID;

if($CurrentPageNum == "")
$CurrentPageNum = 1;

if($DisplayNumPage == "")
$DisplayNumPage = 5;

$ParArr["CurrentPageNum"] = $CurrentPageNum;
$ParArr["DisplayNumPage"] = $DisplayNumPage;
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$returnArr = $lelib->getBookInformation($ParArr);

if($returnArr != "")
{	
	$sumRating = 0;
	## Get Book Details
	$Title 			= $returnArr[0]["Title"];
	$Author 		= $returnArr[0]["Author"];
	$Source 		= $returnArr[0]["Source"];
	$Category 		= trim($returnArr[0]["Category"]);
	$SubCategory 	= trim($returnArr[0]["SubCategory"]);
	$RelevantSubject = trim($returnArr[0]["RelevantSubject"]);
	$RelevantSubject = $RelevantSubject == ''? '--': $RelevantSubject;
	$Publisher 		= trim($returnArr[0]["Publisher"]);
	$Level 			= ($returnArr[0]["Level"] !="")? $returnArr[0]["Level"] : "--";
	$DateModified 	= ($returnArr[0]["DateModified"]!="")? $returnArr[0]["DateModified"] : "--";
	$Description 	= trim($returnArr[0]["Preface"]);
	$AdultContent 	= $returnArr[0]["AdultContent"];
	$Publish 		= $returnArr[0]["Publish"];
	$IsTLF			= $returnArr[0]["IsTLF"];
	$BookFormat		= $returnArr[0]["BookFormat"];
	$ISBN		= $returnArr[0]["ISBN"];
	if ($Publisher=="")
	{
		$Publisher = "--";
	}
	
	if ($Description=="")
	{
		$Description = "--";
	}
	
	if ($Category!="" && $SubCategory!="")
	{
		$Categorys = $Category." -> ". $SubCategory;
	} elseif ($Category!="")
	{
		$Categorys = $Category;
	} elseif ($SubCategory!="")
	{
		$Categorys = $SubCategory;
	} else
	{
		$Categorys = "--";
	}
	
	$Source = $lelib->getSourceFullName($Source);	
	$reviewArr = $lelib->getReview($ParArr);
	$hits = $lelib->Get_Book_Hits($ParArr["BookID"]);

	$reviewNum = count($reviewArr);
	
	for($c = 0; $c < $reviewNum ; $c++)	
		$sumRating += $reviewArr[$c]["Rating"];
	
	$rating = ($reviewNum > 0)? $sumRating / $reviewNum : 0;
	
	## Generate Rating star images
	
	$starImg = "";
	for ($star = 1; $star <= 5; $star++)
	{
		if($star <= $rating)
			$img = "on";
		
		else		
			if($star <= $rating + 0.5)
				$img = "half";		
			else
				$img = "off";
		
		$starImg .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_".$img.".gif\" width=\"15\" height=\"20\" align='absmiddle' />";
	}
	
	$tagName = $lelib->returnTagNameByBook($BookID);
	$BookTags = $lelib->displayTags($tagName, false);
} 

## Generate HTML information block
$ParArr["currUserID"] = $_SESSION["UserID"];
$resultArr = $lelib->displayReviewTable($ParArr, $eLib);
$contentHtml = $resultArr["content"];
$totalRecord = $resultArr["totalRecord"];

## Calculate Page info
if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
		$CurrentPageNum = 1;
}
$totalPage = ceil($totalRecord / $DisplayNumPage);

//
############################################################
## Get Favourite Link
$isFavourtie = $lelib->IS_MY_FAVOURITE_BOOK($ParArr);

if($isFavourtie){
	$f_word = $eLib["html"]["remove_my_favourite"];
	$js_fave = 'BookDetail.remove_favourite();';
	$div_class = "fav_remove";
}else{	
	$f_word = $eLib["html"]["add_to_my_favourite"];
	$js_fave = '$(\'#elib_tools_fav\').show()';
	$div_class = "fav_add";
}

if(mb_strlen($Description,"UTF-8") > 250 ){
	$Description = mb_substr($Description, 0,250,"UTF-8")."...\"";
}

$my_favourite_link = '<a href="javascript:void(0)"   onClick="'.$js_fave.'">'.$f_word.'</a>';
############################################################

## Check book is readable or not

$has_book_access = $objInstall->check_book_is_readable($BookID, $_SESSION['UserID']);
$read_link = $objInstall->get_book_reader($BookID, $BookFormat, $IsTLF, $_SESSION['UserID']);

// for testing only
/*
if ($BookFormat==$lelib->physical_book_type_code)
{
	$physicalBookObj = $lelib->GET_PHYSICAL_BOOK_INFO($BookID);
	//debug_r($physicalBookObj);
}
*/

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

if (file_exists($intranet_root."/file/elibrary/content/{$BookID}/image/cover.jpg"))
{
	# $cover_dimension = " width='163' height='220' ";
	
	$imgsize = getimagesize($intranet_root."/file/elibrary/content/{$BookID}/image/cover.jpg");
	if ($imgsize[0]>163)
	{
		$height_new = $imgsize[1] * (163/$imgsize[0]);
		if ($height_new>220)
		{
			$cover_dimension = " height='220' ";
		} else
		{
			$cover_dimension = " width='163' ";
		}
	} elseif ($imgsize[1]>220)
	{
		$cover_dimension = " height='220' ";
	}
	
	$book_cover_image = "<img src=\"/file/elibrary/content/$BookID/image/cover.jpg\" $cover_dimension />";
} else
{
	$cover_dimension = " width='0' height='0' ";
	$book_cover_image = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/eLibrary/cover_no_image.jpg\" width='163' height='220' />";
}


####################################################################################################
//if($Publish == "1"){ //this book is published
?>

<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--

function voteReview(answer, ReviewID)
{	
	$.post('elibrary_ajax.php', 
		{method:'vote_review', BookID: '<?=$BookID?>', answer:answer, ReviewID: ReviewID},
    	function(data, textStatus){
    	
    		if(data != "" && data != 0){
    			// Update UI display + remove vote panel    			
    			$("#helpful_display_"+ReviewID).html(data);
    			$("#helpful_vote_panel_"+ReviewID).html("");	
    		}    		  
    	}
   	 );
} // end function

function addReview()
{
	var formObj = document.getElementById("reviewFormDiv");
	
	formObj.innerHTML = "";

	var x = "";
	
	x += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td>";
	x += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr>";
	x += "<td width=\"20%\" align=\"left\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">";
	x += "<?=$eLib["html"]["review_content"]?>";
	x += "</span></td>";
	x += "<td width=\"80%\" align=\"left\" valign=\"top\"><textarea name=\"review_content\" id=\"review_content\" rows=\"5\" wrap=\"virtual\" class=\"textboxtext\"></textarea>";
	x += "</td></tr>";
	x += "<tr>";
	x += "<td align=\"left\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">";
	x += "<?=$eLib["html"]["rating"]?>";
	x += "</span></td>";
	x += "<td align=\"left\" valign=\"top\">";
	x += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td>";
	
	for(var i = 1; i <=5 ; i++)
	{
		x += getStar(i);	
	} 
	x += "</td></tr></table>";
	x += "</td></tr></table>";
	x += "</td></tr>";
	
	x += "<tr>";
	x += "<td align=\"center\">";
	x += "<input name=\"comfirm\" type=\"button\" class=\"formbutton\" onClick=\"confirmForm();\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"<?=$eLib["html"]["confirm"]?>\">";
	//x += "&nbsp;<input name=\"reset\" type=\"reset\" class=\"formbutton\" onClick=\"resetForm();\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"<?=$eLib["html"]["reset"]?>\">";
	x += "&nbsp;<input name=\"cancel\" type=\"button\" class=\"formbutton\" onClick=\"cancelForm();\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"<?=$eLib["html"]["cancel"]?>\">";
	x += "</td></tr>";
	//x += "<tr><td height=\"5\" class=\"dotline\"><img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif\" width=\"10\" height=\"5\"></td></tr>";
	
	x += "</table>";
	
	formObj.innerHTML = x;
}

function updateRating(n)
{
	document.form1.MyRating.value = n;
}

function getStar(n)
{
	var x = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif\" name=\"star"+n+"\" width=\"30\" height=\"35\" border=\"0\" id=\"star"+n+"\" onMouseOver=\"updateRating("+n+");";
	for(var i = 5; i >= 1; i--)
	{
		if(n >= i)
		x += "MM_swapImage('star"+i+"','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_on.gif',1);";
		else
		x += "MM_swapImage('star"+i+"','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);";
	}
	x += "\">";
	
	return x;
} // end function get star

function cancelForm()
{
	var formObj = document.getElementById("reviewFormDiv");
	
	formObj.innerHTML = "";
	
	x = "";
	x += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";
	x += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	x += "<tr><td>";
	x += "<a href=\"javascript:void();\" onClick=\"addReview();\" class=\"contenttool\"><img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> <?=$eLib['html']['add_new_review']?></a></td>";
	x += "<td><img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif\"></td>";
	x += "</tr></table>";
	x += "</td></tr></table>";
	
	formObj.innerHTML = x;
} // end cancel form

function confirmForm()
{
	if (Trim($("#review_content").val())=="")
	{
		alert("<?=$Lang['SysMgr']['WarnEmptyReview']?>");
		document.form1.review_content.focus();
		return false;
	}
	document.form1.action = "elib_add_review_update.php";
	document.form1.submit();
	document.form1.action = "book_detail.php";
	
} // end confirm form

function resetForm()
{
	MM_swapImage('star1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
	MM_swapImage('star2','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
	MM_swapImage('star3','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
	MM_swapImage('star4','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
	MM_swapImage('star5','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
} // end function reset form


var BookDetail = {
	'_BookID' : <?=$BookID?>,
	'_serverScript' : 'elibrary_ajax.php',
	init: function(){
				
	},
	add_favourite : function(){
		
		$.post(this._serverScript, 
		{
			method : 'add_favourite',
			BookID : this._BookID
		},
		function(data,status){		
			$('#elib_tools_fav').hide();
			if(data != "" && data != 0){
				$('span#addFaveLink').parent().attr("class", "fav_remove");			
				$('span#addFaveLink').html('<a href="javascript:void(0)" onClick="BookDetail.remove_favourite();"><?=$eLib["html"]['remove_my_favourite']?></a>');		
			}
		});
	},
	remove_favourite: function(){
		$.post(this._serverScript, 
		{
			method : 'remove_favourite',
			BookID : this._BookID
		},
		function(data,status){
			
			if(data != "" && data != 0){	
				$('span#addFaveLink').parent().attr("class", "fav_add");		
				$('span#addFaveLink').html('<a href="javascript:void(0)" onClick="$(\'#elib_tools_fav\').show()"><?=$eLib["html"]['add_to_my_favourite']?></a>');		
			}
		});
	}
};

$(document).ready(function(){
	BookDetail.init();
});

//-->
</script>

<form name="form1" action="book_detail.php" method="post">

<table width="96%" border="0" cellspacing="0" cellpadding="5"><tr><td>	


<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr>
	<td align="left" valign="top">
		<table width="670" align="center" border="0" cellspacing="0" cellpadding="3">
		<tr><td><img src="/images/space.gif" border='0' width='2' height='1' /></td></tr>
		</table>
		<table width="100%" align="center" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td align="center">
			
		
			<div class="elib_detail_left" style="float:left;"></div>
            <div class="elib_detail_bg" style="float:left;">            
	            <div class="elib_detail_book">
	            	<!-- OPEN BOOK TO READ -->
					<a href="Javascript:void(0);" title="<?=$eLib["action"]['open_book']?>" onClick="<?=$read_link?>" ><?=$book_cover_image?><br /><?=$eLib["action"]['open_book']?></a>
	            </div>
	            <!-- BOOK DETAIL -->
	            <div class="elib_detail_info">            	
	            	<table >
	                	<tr><td class="subject" valign="top" nowrap="nowrap"><?=$eLib["html"]["title"]?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top" width="280"><font size="3"><?=$Title?></font></td></tr>
	                    <tr><td class="subject" valign="top" nowrap="nowrap"><?=$eLib["html"]["author"]?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top"><?=$Author?></td></tr>
	                    <tr><td class="subject" valign="top" nowrap="nowrap"><?=$eLib["html"]["publisher"]?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top"><?=$Publisher?></td></tr>
	                    <tr><td class="subject" valign="top" nowrap="nowrap"><?=$eLib["html"]["category"]?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top"><?=$Categorys?></td></tr>
	                    
	                    
	                   <!--   <tr><td class="subject" valign="top"><?=$eLib["html"]["source"]?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top"><?=$Source?></td></tr> -->
	                   <tr><td class="subject" valign="top" ><?=$eLib["html"]["level"]?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top"><?=$Level?></td></tr>
	                   <!-- <tr><td class="subject" valign="top"><?=$eLib["html"]["book_input_date"]?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top"><?=$DateModified?></td></tr> -->
	                   <?php if($ISBN): ?>
	                   		<tr><td class="subject" valign="top" ><?=$eLib["html"]["ISBN"]?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top"><?=$ISBN?></td></tr>
	                   <?php endif; ?>
	                   <tr><td class="subject" valign="top" nowrap="nowrap"><?=$Lang['StudentRegistry']['Tag']?></td><td valign="top" nowrap="nowrap">: &nbsp;</td><td valign="top"><?=$BookTags?></td></tr>
	                </table>
				</div>
            </div>
            <div class="elib_detail_right" style="float:left;"></div>
            
            
                        
           	<div class="clear"></div>
           	
           	<!-- BOOK DESCRIPTION -->
            <div class="elib_bookdes_wrap">
            	<div class="elib_bookdes_top"><?=$eLib["html"]["description"]?></div>
            	<div class="elib_bookdes_body"><?=$Description?></div>            
            	<div class="elib_bookdes_bottom"></div>
            </div>            
            
            
            <!-- FAVOURITE LAYER -->
            <div class="elib_tools_wrap">           
            
            	<div id="elib_tools_fav" style="display:none;visibility:visible;">
                		<div class="close"><a href="Javascript:void(0);" onClick="$('#elib_tools_fav').hide();"><?=$Lang['Btn']['Close']?></a></div>
                  <div class="body" style="position:relative;">
                   	<div class="fav">
                        	<?=$eLib["html"]['add_to_my_favourite']?>
                            <!-- This book already in your favourite list -->
                            <!-- Remove this book from My Favourite -->
                    </div>
						<div class="bottom"><input name="submitBtn" type="button" class="formbutton_00" onClick = "BookDetail.add_favourite();"
 							onmouseover="this.className='formbuttonon_00'" onMouseOut="this.className='formbutton_00'" value="<?=$Lang["Btn"]['Save']?>"/>
					</div>
                </div>			
			  </div>
			  
            	<!-- Number of hits -->
            	<?php if ($hits>0)
            	{?>
	        	<div class="hits" style="text-align:left;">
		        		<?= str_replace("<HITS>", number_format($hits), $eLib["html"]['hits_accumulated'])?>
	            </div>
	            <?php } ?>
            	<!--   AVERAGE   -->
	        	<div class="reviews" style="text-align:left;">
		        		<?=$eLib["html"]['rating']?>
		        	<?=$starImg?> (<?=$reviewNum?> <?=$eLib["html"]["reviews"]?>)
	            </div>
	            
	            <!--    FAVOURITE    -->
	        	<div class="<?=$div_class?>" style="text-align:left;">
	        		<span class="text01" id="addFaveLink">	        			
	        			<?=$my_favourite_link?>
	        		</span>
	        	</div>
	            <!-- <div class="fav_remove"><span class="text01"><a href="#">Remove from My Favourite</a></span></div> -->
	            
	            <!--  RECOMMEND  -->
	            <?php 
	            if ($isAdmin){
	            ?>
	            <div class="add_review" style="text-align:left;">
	            	<span class="text01">
	            	<a href="elib_add_recommend.php?BookID=<?=$BookID?>&TB_iframe=true&amp;height=400&amp;width=550" class="thickbox" title="<?=$eLib["html"]["recommend_this_book"]?>">
	            		<?=$eLib["html"]["recommend_this_book"]?>
	            	</a>
	            	</span>
	            </div>    
	            <?php } ?>
	                                                    
            </div>
            
            <!-- eLibrary Book Detail End -->
        </td>
        </tr>
        </table>
	</td>
	</tr>
		
	<tr><td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td></tr>
			
											
	<!-- show the display top heading -->
	<?=$lelib->displayReviewTableHeading($ParArr, $eLib);?>
	<!-- end show the display top heading -->

	<tr><td align="left">

	<!-- add review -->
	<?php if($has_book_access){ ?>
	<div name="reviewFormDiv" id="reviewFormDiv">
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td>
					<a href="javascript:void();" onClick="addReview();" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$eLib["html"]["add_new_review"]?></a>
				</td>
				<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
	</div>
	<?php } ?>
	<!-- end add review -->
	
	<!--      CONTENT    -->
	<?=$contentHtml?>

	<tr><td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td></tr>
</table>
	
</td></tr></table>


<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="BookID" value="<?=$BookID?>" >
<input type="hidden" name="MyRating" value="<?=$MyRating?>" >
<input type="hidden" name="Helpful" value="0" >
<input type="hidden" name="ReviewID" value="0" >
</form>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>