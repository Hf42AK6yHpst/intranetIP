<?php
/**
 * Change Log:
 * 2018-12-12 Pun [ip.2.5.10.1.1]
 *  - Added ISBN
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$keywords = trim($keywords);
$_SESSION["elib"]["is_search"] = 1;
$_SESSION["elib"]["is_search_select"] = $isSearchSelect;
$_SESSION["elib"]["search_word"] = $keywords;

$title = $lelib->printSearchInput($ParArr, $eLib);

$tab_title = $lelib->getPageTabTitle();
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

if($selectSearchType == "0")
	$BookTitle = $keywords;

if($selectSearchType == "1")
	$Author = $keywords;

if($BookTitle == "" || $BookTitle == null)
	$BookTitleStr = "---";

if($DisplayMode == "" || $DisplayMode == null)
	$DisplayMode = 1;

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 20;

if($sortField == "")
$sortField = "Title";


if($sortFieldOrder == "")
$sortFieldOrder = "ASC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["BookTitle"] = $BookTitle;
$inputArr["Author"] = $Author;
$inputArr["Source"] = $Source;
$inputArr["Category"] = $selCategory;
$inputArr["Level"] = $selLevel;
$inputArr["startdate"] = $startdate;
$inputArr["enddate"] = $enddate;
$inputArr["Tag"] = $BookTag;
$inputArr["ISBN"] = $ISBN;
//the "With Worksheets" field is disabled (by Adam 2008.11.06)
$checkWorksheets = NULL;
//////////////////////////////////////////////////////////////
$inputArr["checkWorksheets"] = $checkWorksheets;
$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

// for normal search
$inputArr["selectSearchType"] = $selectSearchType;
$inputArr["keywords"] = $keywords;

$inputArr["all_category"] = $eLib["html"]["all_category"];
$inputArr["all_level"] = $eLib["html"]["all_level"];

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

//debug_r($inputArr);
//debug_r($inputArr);
$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Author";
$tmpArr[2]["value"] = "Publisher";
$tmpArr[3]["value"] = "Category";
$tmpArr[4]["value"] = "SubCategory";

$tmpArr[0]["text"] = $eLib["html"]["title"];
$tmpArr[1]["text"] = $eLib["html"]["author"];
$tmpArr[2]["text"] = $eLib["html"]["publisher"];
$tmpArr[3]["text"] = $eLib["html"]["category"];
$tmpArr[4]["text"] = $eLib["html"]["subcategory"];

$tmpArr[0]["width"] = "25%";
$tmpArr[1]["width"] = "15%";
$tmpArr[2]["width"] = "25%";
$tmpArr[3]["width"] = "15%";
$tmpArr[4]["width"] = "15%";

$inputArr["sortFieldArray"] = $tmpArr;

$resultArr = $lelib->displayBookDetail($inputArr, $eLib);

$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

$select_s1 = "";
$select_s2 = "";
$select_s3 = "";
$select_s4 = "";
$select_s5 = "";

if($sortField == "Title")
$select_s1 = "selected";
else if($sortField == "Author")
$select_s2 = "selected";
else if($sortField == "Publisher")
$select_s3 = "selected";
else if($sortField == "Category")
$select_s4 = "selected";
else if($sortField == "SubCategory")
$select_s5 = "selected";

if($DisplayMode == 2)
$sortStr = "";
else if ($DisplayMode == 1)
{
	$sortArr = Array();
	$sortArr[] = array("Title", $eLib["html"]["book_title"]);
	$sortArr[] = array("Author", $eLib["html"]["author"]);
	$sortArr[] = array("Publisher", $eLib["html"]["publisher"]);
	$sortArr[] = array("Category", $eLib["html"]["category"]);
	$sortArr[] = array("SubCategory", $eLib["html"]["subcategory"]);
	
	$tmpSortSelection = $linterface->GET_SELECTION_BOX($sortArr, "name='sortField' id='sortField' onChange='sortFunction(this);'", "", $sortField);
	
$sortStr = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
$sortStr .= "<tr>";
$sortStr .= "<td align=\"left\" valign=\"middle\" class=\"tabletext\">".$eLib["html"]["sort_by"]." :</td>";
$sortStr .= "<td align=\"left\" valign=\"middle\">";
$sortStr .= $tmpSortSelection;
$sortStr .= "</td></tr></table>";
}
$sortSelection = $sortStr;
////////////////////////////////////////////////////////////////////////////

// set view icon
$x = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$x .= "<td class=\"tabletextremark\">".$eLib["html"]["view"]."</td>";
$x .= "<td><a href=\"#\">";

if($DisplayMode == 1)
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif\" alt=\"".$eLib["html"]["with_books_cover"]."\" name=\"view_list21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list21\" onMouseOver=\"MM_swapImage('view_list21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(1);\">";
else
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list.gif\" alt=\"".$eLib["html"]["with_books_cover"]."\" name=\"view_list21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list21\" onMouseOver=\"MM_swapImage('view_list21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(1);\">";

$x .= "</a></td>";
$x .= "<td><a href=\"#\">";

if($DisplayMode == 2)
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif\" alt=\"".$eLib["html"]["table_list"]."\" name=\"view_21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_21\" onMouseOver=\"MM_swapImage('view_21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(2);\">";
else
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table.gif\" alt=\"".$eLib["html"]["table_list"]."\" name=\"view_21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_21\" onMouseOver=\"MM_swapImage('view_21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(2);\">";

$x .= "</a></td></tr></table>";

$viewChangeSelection = $x;

/////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">
<link href="css/elib_josephine.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" >
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">


<!-- ------------------------------------------------------------ -->
<!-- ---------------      LEFT NAVIGATION BAR ----------------------- --> 
<?php include_once('elib_left_navigation.php'); ?>

</td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="elib_advanced_search_result.php" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="elib_advanced_search_form.php"><?=$eLib["html"]["advance_search"]?></a>
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["search_result"]?></td>
</tr>
</table>
<!-- end head menu -->

<!-- result / select -->
<table width="98%" border="0" cellspacing="0" cellpadding="0" >
<?php  if (trim($selectSearchType)=="") { ?>
<tr>
<td height="30" align="left" valign="bottom">
<span class="tabletext"><?=$eLib["html"]["category"]?></span> :<span> &quot;<strong><?=$selCategory?></strong>&quot;</span>,  
&nbsp; <span class="tabletext"><?=$eLib["html"]["level"]?></span> :<span> &quot;<strong><?=$selLevel?></strong>&quot;</span>  
<?php if ($BookTitle!="") { ?> , &nbsp; <span class="tabletext"><?=$eLib["html"]["book_title"]?></span> :<span> &quot;<strong><?=stripslashes($BookTitle)?></strong>&quot;</span> <?php } ?>
<?php if ($Author!="") { ?> , &nbsp; <span class="tabletext"><?=$eLib["html"]["author"]?></span> :<span> &quot;<strong><?=stripslashes($Author)?></strong>&quot;</span> <?php } ?>
<?php if ($ISBN!="") { ?> , &nbsp; <span class="tabletext"><?=$eLib["html"]["ISBN"]?></span> :<span> &quot;<strong><?=stripslashes($ISBN)?></strong>&quot;</span> <?php } ?>
<?php if ($BookTag!="") { ?> , &nbsp; <span class="tabletext"><?=$Lang['StudentRegistry']['Tag']?></span> :<span> &quot;<strong><?=stripslashes($BookTag)?></strong>&quot;</span> <?php } ?>
</td>
</tr>
<?php } ?>
<tr>
<td align="left">
<table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr>
<td><?=$sortSelection?></td>
<td align="right">

<!-- view icon -->
<?=$viewChangeSelection?>
<!-- end view icon -->

</td></tr></table>
</td></tr></table>
<!-- end result / select -->

<!-- show content -->
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr><td>
<?=$contentHtml; ?>
</td></tr></table>
<!-- end show content -->

<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >

<?
if($DisplayMode == 2){
?>
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<?
}
?>

<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

<input type="hidden" name="BookTitle" value="<?=stripslashes($BookTitle)?>" >
<input type="hidden" name="Author" value="<?=stripslashes($Author)?>" >
<input type="hidden" name="Source" value="<?=$Source?>" >
<input type="hidden" name="selCategory" value="<?=$selCategory?>" >
<input type="hidden" name="selLevel" value="<?=$selLevel?>" >
<input type="hidden" name="startdate" value="<?=$startdate?>" >
<input type="hidden" name="enddate" value="<?=$enddate?>" >
<input type="hidden" name="checkWorksheets" value="<?=$checkWorksheets?>" >

<input type="hidden" name="selectSearchType" value="<?=$selectSearchType?>" >
<input type="hidden" name="keywords" value="<?=$keywords?>" >
<input type="hidden" name="BookTag" value="<?=stripslashes($BookTag)?>" >

</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
//2012-11-15 (CharlesMa)

<?php if((strlen($Category) != strlen(utf8_decode($Category))) || (strlen($keywords) != strlen(utf8_decode($keywords)))){ ?>
	document.getElementById('elib_cata_wrap_01').style.display='none';
	document.getElementById('elib_cata_wrap_02').style.display='block';
<?php }else{												?>
	document.getElementById('elib_cata_wrap_01').style.display='block';
	document.getElementById('elib_cata_wrap_02').style.display='none';
<?php }												?>

if('<?=$intranet_session_language?>' == "en"){
	//showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	//showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
