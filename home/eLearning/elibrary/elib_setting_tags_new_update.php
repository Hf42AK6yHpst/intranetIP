<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();


$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$Libelibinstall = new elibrary_install();	// 2013-08-19 Charles Ma
//2013-08-19 Charles Ma
if(!$Libelibinstall->retrive_license_enable())
	$license_style = 'style="display:none;"';

$TagName = trim($TagName);
$IsEdit = (isset($TagID) && $TagID>0);

if (!$IsEdit)
{
	# search if same tag name exists (if so, edit)
	$sql = "SELECT TagID FROM INTRANET_ELIB_TAG WHERE TagName='".$TagName."'";
	$result = $LibeLib->returnVector($sql);
	if ($result[0]!="")
	{
		$IsEdit = true;
		$TagID = $result[0];
	} else
	{
		# add to DB
		$TagIDArray = $LibeLib->returnTagIDByTagName($TagName);
		$TagID = $TagIDArray[0];
	}
	
	# add related books
	$LibeLib->AssignTagToBooks($TagID, $BookIDSelected);
} else
{
	# update TagName
	$sql = "UPDATE INTRANET_ELIB_TAG SET TagName='{$TagName}' WHERE TagID='{$TagID}' ";
	$LibeLib->db_db_query($sql);
	
	# remove not in current selection
	$sql = "DELETE FROM INTRANET_ELIB_BOOK_TAG WHERE TagID='{$TagID}' ";
	if (is_array($BookIDSelected) && sizeof($BookIDSelected)>0)
	{
		$booksKept = implode(",", $BookIDSelected);
		$sql .= " AND BookID NOT IN ({$booksKept}) ";		
	}
	$LibeLib->db_db_query($sql);
	
	# add new selection
	$LibeLib->AssignTagToBooks($TagID, $BookIDSelected);
}


intranet_closedb();

header("location: elib_setting_tags.php?xmsg=AddSuccess")
?>