<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();
#######################################################################
## Init library
$linterface 	= new interface_html("default3.html");
$lelib		= new elibrary();
$objInstall		= new elibrary_install();

#########################################
### Set page Tite info 
$ParArr["image_path"] 		= $image_path;
$ParArr["LAYOUT_SKIN"] 		= $LAYOUT_SKIN;
$title 						= $lelib->printSearchInput($ParArr, $eLib);

$tab_title = $lelib->getPageTabTitle();
	
$TAGS_OBJ[] 				= array($title,"");
$MODULE_OBJ["title"] 		= $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] 		= "";
$MODULE_OBJ['title_css'] 	= "menu_opened";
$customLeftMenu 			= ' ';
$CurrentPageArr['eLib'] 	= 1;


#########################################
# Get list of readable books SQL
$aryReadBook = $objInstall->get_readable_book_info($_SESSION['UserID']);

if($aryReadBook != array()){
	$display_readable_books = $lelib->newPrintBookTable($aryReadBook, $image_path, $LAYOUT_SKIN, "na", "Readable", $eLib);
}else{
	$display_readable_books = "<BR /><BR />".$eLib['SystemMsg']['NoMyBook'];
}

$linterface->LAYOUT_START();
//additional javascript for eLibrary only
include_once("elib_script_function.php");
#######################################################################
?>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">
<link href="css/elib_josephine.css" rel="stylesheet" type="text/css">




<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="center">
  				<table width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
        			<td>
        				<table width="99%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center" bgcolor="#FFFFFF" width="200px" valign="top">
							
							
                    			<table cellpadding="0" cellspacing="0" border="0" width="100%" align="left">
                    			<tr>
                    				<td valign="top" width="175">
									                    			
										<!-- ------------------------------------------------------------ -->
										<!-- ---------------------    SETTING      ---------------------- -->
										<!-- Setting logo -->
										<?
										if($lelib->IS_ADMIN_USER($_SESSION["UserID"]))
										{
										?>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">
										<table width="95%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_01.gif" width="4" height="4"></td>
											<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
											<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_03.gif" width="4" height="4"></td>
										</tr>
										<tr>
											<td width="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
											<td bgcolor="#d7e9f4"><a href="#" class="menuon" onClick="MM_openBrWindow('elib_setting.php','','scrollbars=yes')"> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_manage.gif" width="20" height="20" border="0" align="absmiddle"><?=$eLib["html"]["settings"]?></a></td>
											<td width="4" bgcolor="#d7e9f4">&nbsp;</td>
										</tr>
										<?php
										if($plugin['eLib_license'] == 2){
										?>
										<tr>
											<td width="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
											<td bgcolor="#d7e9f4"><a href="#" class="menuon" onClick="MM_openBrWindow('admin_book_license.php','','scrollbars=yes')"> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_manage.gif" width="20" height="20" border="0" align="absmiddle"><?=$eLib['license']['ManageBookLicense']?></a></td>
											<td width="4" bgcolor="#d7e9f4">&nbsp;</td>
										</tr>
										<?php
										}
										?>
										<tr>
											<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_07.gif" width="4" height="4"></td>
											<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
											<td width="4" height="4" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_09.gif" width="4" height="4"></td>
										</tr>
										</table>
										
										<?
										
										}
										?>
                    				
		                    			<!-- ------------------------------------------------------------ -->
										<!-- ---------------      LEFT NAVIGATION BAR ----------------------- -> 
										<?php include_once('elib_left_navigation.php'); ?>								
									</td>

									<td valign="top" align="left">
										<div class="800width"></div>
										<!-- eLibrary Content Begin -->


									</td>
								</tr>
							</table>
												
							
                      
                      </td> 
                      <td valign="top" align="left">
                      	<BR />
                      	<?=$display_readable_books?>
                      </td>                     
                    </tr>                   
                  </table>
                  <br>
                </td>
              </tr>
            </table></td>
        </tr>
    </table></td>
  </tr>			
</table>


<?php
#######################################################################
$linterface->LAYOUT_STOP();
intranet_closedb();
?>