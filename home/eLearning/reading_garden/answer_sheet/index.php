<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_answersheet_template_page_size", "numPerPage");
$arrCookies[] = array("ck_answersheet_template_page_number", "pageNo");
$arrCookies[] = array("ck_answersheet_template_page_order", "order");
$arrCookies[] = array("ck_answersheet_template_page_field", "field");	
$arrCookies[] = array("ck_answersheet_template_page_keyword", "Keyword");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_AnswerSheetSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AnswerSheetTemplate'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$linterface->LAYOUT_START($ReturnMessage);

//$linterface = new interface_html("default3.html");
//$ReadingGardenUI = new libreadinggarden_ui();
//$ReadingGardenLib = new libreadinggarden();
//
//$CurrentPageArr['eLearningReadingGarden'] = 1;
//$TAGS_OBJ[] = array($Lang['ReadingGarden']['AnswerSheetTemplate'], "", 0);
//$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();
//
//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
//$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
//$linterface->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Answer_Sheet_Template_Index($field,$order,$pageNo,$numPerPage);

?>
<script language="javascript" src="/templates/forms/reading_garden_form_edit.js"></script>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script>
function Get_Answersheet_Template_Table()
{
	var formData = $('#form1').serialize();
	formData += "&Action=GetTemplateTable";
	
	Block_Element("DivTemplateTable");
	$.ajax({
		type:"POST",
		url:"ajax_load.php",
		data: formData,  
		success: function(ReturnData) {
			$("#DivTemplateTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("DivTemplateTable");
			Scroll_To_Top();
		}
	});
}

function Get_Answersheet_Template_Form(is_new, template_id)
{
	if(!template_id){
		template_id = $('input[name="TemplateID\\[\\]"]:checked').val();
	}
	tb_show("<?=$Lang['Btn']['Edit']?>","#TB_inline?height=600&width=750&inlineId=FakeLayer");
	
	$.post(
		"ajax_load.php",
		{
			"Action":"GetTemplateForm",
			"IsNew":is_new,
			"TemplateID":template_id 
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
		});
}

function Get_Answersheet_Template_Preview_Form(template_id)
{
	$.post(
		"ajax_load.php",
		{
			"Action":"GetTemplatePreviewForm",
			"TemplateID":template_id 
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
		});
}

function Add_Answersheet_Template()
{
	Block_Thickbox();
	var CategoryID = $('select#CategoryID').val() || '';
	$.post(
		"ajax_update.php",
		{
			"Action":"AddTemplate",
			"TemplateName":encodeURIComponent($('input#TemplateName').val()),
			"AnswerSheet":encodeURIComponent($('input#qStr').val()),
			"CategoryID":encodeURIComponent(CategoryID)
		},
		function(ReturnData){
			if(ReturnData=="1"){
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['AddAnswerSheetTemplateSuccess']?>');
			}else{
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['AddAnswerSheetTemplateFail']?>');
			}
			Get_Answersheet_Template_Table();
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Update_Answersheet_Template()
{
	Block_Thickbox();
	var CategoryID = $('select#CategoryID').val() || '';
	$.post(
		"ajax_update.php",
		{
			"Action":"UpdateTemplate",
			"TemplateID":$('input#TemplateID').val(),
			"TemplateName":encodeURIComponent($('input#TemplateName').val()),
			"AnswerSheet":encodeURIComponent($('input#qStr').val()),
			"CategoryID":encodeURIComponent(CategoryID)
		},
		function(ReturnData){
			if(ReturnData=="1"){
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateAnswerSheetTemplateSuccess']?>');
			}else{
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateAnswerSheetTemplateFail']?>');
			}
			Get_Answersheet_Template_Table();
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Check_Template_Name(issubmit)
{
	var error_cnt = 0;
	if($.trim($('input#TemplateName').val())==''){
		//$('input#submit_btn').attr('disabled',true);
		$('div#TemplateNameWarningDiv').html('<?=$Lang['ReadingGarden']['WarningMsg']['BlankAnswerSheetTemplateName']?>');
		$('div#TemplateNameWarningDiv').css('display','');
		error_cnt+=1;
		return false;
	}else{
		//$('input#submit_btn').attr('disabled',false);
		$('div#TemplateNameWarningDiv').html('');
		$('div#TemplateNameWarningDiv').css('display','none');
	}
	
	$.post(
		"ajax_update.php",
		{
			"Action":"CheckTemplateName",
			"TemplateName":encodeURIComponent($('input#TemplateName').val()),
			"OldTemplateName":encodeURIComponent($('input#OldTemplateName').val())
		},
		function(ReturnData){
			if(ReturnData=="1"){
				//$('input#submit_btn').attr('disabled',false);
				$('div#TemplateNameWarningDiv').html('');
				$('div#TemplateNameWarningDiv').css('display','none');
				var ans_str = $.trim($('input#qStr').val());
				if(ans_str=='')
				{
					error_cnt+=1;
					alert('<?=$Lang['ReadingGarden']['WarningMsg']['PleaseComposeAsnwerSheet']?>');
				}
				if(issubmit==1 && error_cnt==0){
					if($('input#IsNew').val()==1)
						Add_Answersheet_Template();
					else
						Update_Answersheet_Template();
				}
			}else{
				//$('input#submit_btn').attr('disabled',true);
				$('div#TemplateNameWarningDiv').html('<?=$Lang['ReadingGarden']['WarningMsg']['AnswerSheetTemplateNameExist']?>');
				$('div#TemplateNameWarningDiv').css('display','');
			}
		}
	);
}

$().ready(function(){
	Get_Answersheet_Template_Table();
})
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>