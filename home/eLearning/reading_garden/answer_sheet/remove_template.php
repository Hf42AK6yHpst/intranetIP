<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$TemplateID = $_REQUEST['TemplateID'];
$Result = $ReadingGardenLib->Delete_Answer_Sheet_Template($TemplateID);

if($Result)
	$Msg = $Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetTemplateSuccess'];
else
	$Msg = $Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetTemplateFail'];

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>