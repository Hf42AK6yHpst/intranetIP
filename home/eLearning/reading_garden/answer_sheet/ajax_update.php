<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "AddTemplate":
		$DataArr['TemplateName'] = trim(urldecode(stripslashes($_REQUEST['TemplateName'])));
		$DataArr['AnswerSheet'] = trim(urldecode(stripslashes($_REQUEST['AnswerSheet'])));
		$DataArr['CategoryID'] = explode(",",trim(urldecode(stripslashes($_REQUEST['CategoryID']))));
		
		$ReadingGardenLib->Start_Trans();
		$result["InsertTemplate"] = $ReadingGardenLib->New_Answer_Sheet_Template($DataArr);
		$DataArr['TemplateID'] = mysql_insert_id();
		$result["UpdateMapping"] = $ReadingGardenLib->Update_Answer_Sheet_Template_Category_Mapping($DataArr);
		
		if(!in_array(false,$result))
		{
			$ReadingGardenLib->Commit_Trans();
			echo "1";
		}
		else
		{
			$ReadingGardenLib->RollBack_Trans();
			echo "0";
		}
	break;
	case "UpdateTemplate":
		$DataArr['TemplateID'] = $_REQUEST['TemplateID'];
		$DataArr['TemplateName'] = trim(urldecode(stripslashes($_REQUEST['TemplateName'])));
		$DataArr['AnswerSheet'] = trim(urldecode(stripslashes($_REQUEST['AnswerSheet'])));
		$DataArr['CategoryID'] = explode(",",trim(urldecode(stripslashes($_REQUEST['CategoryID']))));
		
		$ReadingGardenLib->Start_Trans();
		
		$result["UpdateTemplate"] = $ReadingGardenLib->Update_Answer_Sheet_Template($DataArr);
		$result["UpdateMapping"] = $ReadingGardenLib->Update_Answer_Sheet_Template_Category_Mapping($DataArr);
		
		if(!in_array(false,$result))
		{
			$ReadingGardenLib->Commit_Trans();
			echo "1";
		}
		else
		{
			$ReadingGardenLib->RollBack_Trans();
			echo "0";
		}
	break;
	case "CheckTemplateName":
		$TemplateName = trim(urldecode(stripslashes($_REQUEST['TemplateName'])));
		$OldTemplateName = trim(urldecode(stripslashes($_REQUEST['OldTemplateName'])));
		if($TemplateName==""){
			echo "0";
		}else if($TemplateName == $OldTemplateName){
			echo "1";
		}else{
			$result = $ReadingGardenLib->Get_Answer_Sheet_Template("",$TemplateName);
			if(sizeof($result)>0)
				echo "0";
			else
				echo "1";//ok
		}
	break;
}

intranet_closedb();
?>