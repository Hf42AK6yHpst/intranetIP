<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "GetForumTopicContent":
		$ForumID = $_REQUEST['ForumID'];
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		$TopicInfo = $ReadingGardenLib->Get_Forum_Topic($ForumID,$ForumTopicID);
		echo $TopicInfo[0]['Message'];
	break;
	case "GetForumPostContent":
		//$ForumID = $_REQUEST['ForumID'];
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		$ForumPostID = $_REQUEST['ForumPostID'];
		$TopicInfo = $ReadingGardenLib->Get_Forum_Topic_Post($ForumTopicID, $ForumPostID);
		echo $TopicInfo[0]['Message'];
	break;
	
	case "ApproveTopic":
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		$Action="approve";
		$DataArr['ForumTopicID'] = $ForumTopicID;
		$Success = $ReadingGardenLib->Manage_Forum_Topic_Record($Action,$DataArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;
	case "RejectTopic":
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		$Action="reject";
		$DataArr['ForumTopicID'] = $ForumTopicID;
		$Success = $ReadingGardenLib->Manage_Forum_Topic_Record($Action,$DataArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;
	case "MarkTopicOnTop":
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		$Action="markontop";
		$DataArr['ForumTopicID'] = $ForumTopicID;
		$Success = $ReadingGardenLib->Manage_Forum_Topic_Record($Action,$DataArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;
	case "RemoveTopicOnTop":
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		$Action="removeontop";
		$DataArr['ForumTopicID'] = $ForumTopicID;
		$Success = $ReadingGardenLib->Manage_Forum_Topic_Record($Action,$DataArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;
	
	case "DeleteTopic":
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		$Action="delete";
		$DataArr['ForumTopicID'] = $ForumTopicID;
		$Success = $ReadingGardenLib->Manage_Forum_Topic_Record($Action,$DataArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;
	
	case "DeletePost":
		$ForumPostID = $_REQUEST['ForumPostID'];
		$Action = "delete";
		$DataArr['ForumPostID'] = $ForumPostID;
		$Success = $ReadingGardenLib->Manage_Forum_Post_Record($Action,$DataArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("DeleteSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("DeleteUnSuccess");
	break;
	
	case "ApprovePost":
		$ForumPostID = $_REQUEST['ForumPostID'];
		$Action="approve";
		$DataArr['ForumPostID'] = $ForumPostID;
		$Success = $ReadingGardenLib->Manage_Forum_Post_Record($Action,$DataArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;
	case "RejectPost":
		$ForumPostID = $_REQUEST['ForumPostID'];
		$Action="reject";
		$DataArr['ForumPostID'] = $ForumPostID;
		$Success = $ReadingGardenLib->Manage_Forum_Post_Record($Action,$DataArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;
}

intranet_closedb();
?>
