<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "ReloadForumTopicTable":
		$ForumID = $_REQUEST['ForumID'];
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$RecordStatus = $_REQUEST['RecordStatus'];

		$arrCookies[] = array("ck_forum_topic_page_size", "numPerPage");
		$arrCookies[] = array("ck_forum_topic_page_number", "pageNo");
		$arrCookies[] = array("ck_forum_topic_page_order", "order");
		$arrCookies[] = array("ck_forum_topic_page_field", "field");	
		$arrCookies[] = array("ck_forum_topic_page_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $ReadingGardenUI->Get_Forum_Topic_DBTable($ForumID, $Keyword,$field, $order, $pageNo, $numPerPage, $RecordStatus);
	break;
	case "ReloadForumTopicTableTool":
		$RecordStatus = $_REQUEST['RecordStatus'];
		$FieldName = $_REQUEST['FieldName'];
		echo $ReadingGardenUI->Get_Forum_Topic_Table_Tool_Layer($RecordStatus,$FieldName);
	break;
	
	case "ReloadForumPostTable":
		$ForumID = $_REQUEST['ForumID'];
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$RecordStatus = $_REQUEST['RecordStatus'];
		
		$arrCookies[] = array("ck_forum_post_page_size", "numPerPage");
		$arrCookies[] = array("ck_forum_post_page_number", "pageNo");
		$arrCookies[] = array("ck_forum_post_page_order", "order");
		$arrCookies[] = array("ck_forum_post_page_field", "field");	
		$arrCookies[] = array("ck_forum_post_page_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $ReadingGardenUI->Get_Forum_Post_DBTable($ForumID,$ForumTopicID,$field, $order, $pageNo, $numPerPage,$Keyword,$RecordStatus);
	break;
	
	case "ReloadForumPostTableTool":
		$RecordStatus = $_REQUEST['RecordStatus'];
		$FieldName = $_REQUEST['FieldName'];
		echo $ReadingGardenUI->Get_Forum_Topic_Table_Tool_Layer($RecordStatus,$FieldName,0);
	break;
	
	case "ReloadForumTopicEntry":
		$ForumID = $_REQUEST['ForumID'];
		$ForumTopicID = $_REQUEST['ForumTopicID'];
		echo $ReadingGardenUI->Get_Forum_Topic_Entry($ForumID,$ForumTopicID);
	break;
}

intranet_closedb();
?>