<?php
// editing by 


/*
 * ******************** Change Log ********************
 * Date		:	2011-09-08 Yuen - support iPad using plain text editor
*/


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$ReadingGardenLib = new libreadinggarden();

$ForumID = $_REQUEST['ForumID'];
$ForumTopicID = $_REQUEST['ForumTopicID'];
$DataArr['ForumID'] = $ForumID;
$DataArr['ForumTopicID'] = $ForumTopicID;
$DataArr['TopicSubject'] = stripslashes(trim($_REQUEST['TopicSubject']));
$DataArr['Attachment'] = GetCommonAttachmentFolderPath($AttachmentArr, $Attachment);

if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($_REQUEST['Message']==strip_tags($_REQUEST['Message']))
	{
		$_REQUEST['Message'] = nl2br($_REQUEST['Message']);
	}
}
$DataArr['Message'] = stripslashes(trim($_REQUEST['Message']));
$FromPost = $_REQUEST['FromPost'];

if(trim($ForumTopicID)=='')
	$Action = "add";
else
	$Action = "update";

$Success = $ReadingGardenLib->Manage_Forum_Topic_Record($Action,$DataArr);

if($Action=="add")
{
	if($Success)
		$Msg = "AddSuccess";
	else
		$Msg = "AddUnsuccess";
}
else if($Action=="update")
{
	if($Success)
		$Msg = "UpdateSuccess";
	else
		$Msg = "UpdateUnsuccess";
}

intranet_closedb();
if($FromPost==1 && $ForumTopicID!='')
	header("Location: ./post.php?ForumID=$ForumID&ForumTopicID=$ForumTopicID&Msg=$Msg");
else
	header("Location: ./topic.php?ForumID=$ForumID&Msg=$Msg");
?>