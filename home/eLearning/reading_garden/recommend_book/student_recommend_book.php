<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

if($Keyword)
	$Keyword = stripslashes($Keyword);

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_recommend_book_list_page_size", "numPerPage");
$arrCookies[] = array("ck_recommend_book_list_page_number", "pageNo");
$arrCookies[] = array("ck_recommend_book_list_page_order", "order");
$arrCookies[] = array("ck_recommend_book_list_page_field", "field");	
$arrCookies[] = array("ck_recommend_book_list_page_keyword", "Keyword");
$arrCookies[] = array("ck_recommend_book_list_page_CategoryID", "CategoryID");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);
	
$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$ReadingGardenLib->Update_Portal_Class_Session();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_DatePicker_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Student_Recommend_Book_List_Index($field,$order,$pageNo,$numPerPage,$CategoryID, $BookIDArr, $Keyword);

?>
<script language="javascript" src="/templates/forms/reading_garden_form_edit.js"></script>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script>
function Get_Recommend_Book_CoverView_Table()
{
	var formData = $('#form1').serialize();
	var ClassID =  $('#ClassID').val()?$('#ClassID').val():'';
	formData += "&Action=GetRecommendBookCoverViewTable";
	
	Block_Element("DivBookTable");
	$.ajax({  
		type: "POST",  
		url: "ajax_load.php?ClassID="+ClassID,
		data: formData,
		cache:false,
		success: function(ReturnData) {
			$("div#DivBookTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("DivBookTable");
			Scroll_To_Top();
		} 
	});
}

function Sort_Table(elementID, fieldName)
{
	$('input#'+fieldName).val($('#'+elementID).val());
	Get_Recommend_Book_CoverView_Table();
}

function Check_Positive_Number(n)
{
	var value = parseFloat(n);
	if (isNaN(value) || value <= 0)
		return false;
	else
		return true;
}

$().ready(
	function(){
		Get_Recommend_Book_CoverView_Table();
	}
);
</script>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();
?>