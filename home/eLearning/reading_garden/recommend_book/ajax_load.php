<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "GetRecommendBookCoverViewTable":
		$CategoryID = $_REQUEST['CategoryID'];
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		$BookIDArr = $_REQUEST['BookIDArr'];
		$ClassID = $_REQUEST['ClassID'];

		$arrCookies[] = array("ck_recommend_book_list_page_size", "numPerPage");
		$arrCookies[] = array("ck_recommend_book_list_page_number", "pageNo");
		$arrCookies[] = array("ck_recommend_book_list_page_order", "order");
		$arrCookies[] = array("ck_recommend_book_list_page_field", "field");	
		$arrCookies[] = array("ck_recommend_book_list_page_keyword", "Keyword");
		$arrCookies[] = array("ck_recommend_book_list_page_CategoryID", "CategoryID");
		updateGetCookies($arrCookies);
		
		echo $ReadingGardenUI->Get_Student_Recommend_Book_List_DBTable($field, $order, $pageNo, $numPerPage, $CategoryID, $Keyword, $BookIDArr, $ClassID);
	break;
	case "GetReadingRecordEditForm":
		$BookID = $_REQUEST['BookID'];
		$IsNew = $_REQUEST['IsNew'];
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		echo $ReadingGardenUI->Get_Recommend_Book_Reading_Record_Edit_Form($BookID, $IsNew, $ReadingRecordID);
	break;
	case "GetBookReportEditForm":
		$Type = $_REQUEST['Type'];
		$IsNew = $_REQUEST['IsNew'];
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		$BookReportID = $_REQUEST['BookReportID'];
		$AssignedReadingID = $_REQUEST['AssignedReadingID'];
		echo $ReadingGardenUI->Get_Student_Book_Report_Edit_Form($Type, $IsNew, $ReadingRecordID, $BookReportID, $AssignedReadingID);
	break;
	case "GetReadingRecordLayer":
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		echo $ReadingGardenUI->Get_Student_Reading_Record_Layer($ReadingRecordID);
	break;
	/*
	case "GetRecommendBookReportTable":
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		
		$arrCookies[] = array("ck_recommend_book_report_list_page_size", "numPerPage");
		$arrCookies[] = array("ck_recommend_book_report_list_page_number", "pageNo");
		$arrCookies[] = array("ck_recommend_book_report_list_page_order", "order");
		$arrCookies[] = array("ck_recommend_book_report_list_page_field", "field");	
		$arrCookies[] = array("ck_recommend_book_report_list_page_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $ReadingGardenUI->Get_Student_Recommend_Book_Report_List_DBTable($field, $order, $pageNo, $numPerPage, $Keyword);
	break;*/
}	

intranet_closedb();
?>