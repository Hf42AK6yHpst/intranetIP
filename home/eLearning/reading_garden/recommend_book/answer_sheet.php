<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
include_once($PATH_WRT_ROOT."includes/libform.php");


intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$lform = new libform();

$ReadingRecordID = $_REQUEST['reading_record_id'];
$AssignedReadingID = $_REQUEST['assigned_reading_id'];
$BookReportID = $_REQUEST['book_report_id'];

$AnswerSheetQue = $ReadingGardenLib->Get_Answer_Sheet_From_Assigned_Reading_Or_ReadingRecord($AssignedReadingID,$ReadingRecordID);
$BookReportInfo = $ReadingGardenLib->Get_Student_Book_Report($BookReportID);
$AnswerSheetAns = $BookReportInfo[0]['AnswerSheet'];

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/reading_garden_form_edit.js"></script>
<script language="Javascript">
var replyslip = '<?=$Lang['ReadingGarden']['AnswerSheet']?>';
</script>

<br />
<p>
<table width="80%" align="center" border="0">
<form name="ansForm" method="post" action="">
        <input type="hidden" id="qStr" name="qStr" value="<?=htmlspecialchars($AnswerSheetQue,ENT_QUOTES)?>">
        <input type="hidden" id="aStr" name="aStr" value="<?=htmlspecialchars($AnswerSheetAns,ENT_QUOTES)?>">
</form>

<tr>
	<td align="left">
		<div id="AnswerSheetDiv"></div>
	    <script language="Javascript">
	    <?=$lform->getWordsInJS()?>
	    var MarksTxt = '<?=$Lang['ReadingGarden']['Mark(s)']?>';
		var AnswerTxt = '<?=$Lang['ReadingGarden']['Answer']?>';
	    var s = $('input#qStr').val();
		s = s.replace(/"/g, '&quot;');
		$('input#qStr').val(s);
	    var sheet= new Answersheet();
	    // attention: MUST replace '"' to '&quot;'
	    sheet.qString=s;
	    s = $('input#aStr').val();
		s = s.replace(/"/g, '&quot;');
		$('input#aStr').val(s);
	    sheet.aString=s;
	    //edit submitted application
	    sheet.mode=1;
	    sheet.answer=sheet.sheetArr();
	    
	    Part3 = '';
	   	//document.write(editPanel());
	   	$(document).ready(function(){
			$('div#AnswerSheetDiv').html(editPanel());
			$('div#blockInput').html(sheet.writeSheet());
		});
	    </script>
	</td>
</tr>

<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>

<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
	</td>
</tr>
</table>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>