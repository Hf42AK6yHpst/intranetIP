<?php
// editing by
/*
 * 	Log
 * 
 * 	2016-05-16 [Cameron] 
 * 		fix bug: get ForumID by 1st record in the array if it's from POST and get by string value if it's from GET
 */ 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
	
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$ForumID = is_array($ForumID)?$ForumID[0]:$ForumID;
//$ForumID = sizeof($ForumID)>0?$ForumID[0]:$ForumID;

$CurrentPage = "Management_Forum";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['ForumMgmt']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);
echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Management_Forum_Edit_UI($ForumID);

?>
<script>
function js_Check_Form()
{
	var form_valid = $('input#FormValid');
	var classes = $("select#ClassID option:selected");
	if(classes.length==0){
		$('#WarnBlankClassID').show();
		form_valid.val('0');
	}else{
		$('#WarnBlankClassID').hide();
	}
	
	if(form_valid.val()=='1')
		document.form1.submit();
}

function js_Check_ForumName()
{
	var form_valid = $('input#FormValid');
	form_valid.val('1');
	var forum_name = $.trim($('input#ForumName').val());
	
	if(forum_name == ''){
		$('#WarnMsgForumName').html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestInputForumName']?>');
		$('#WarnForumName').show();
		form_valid.val('0');
		js_Check_Form();
	}else{
		$.post(
			"ajax_task.php",
			{
				"Action":"CheckForumName",
				"ForumName":encodeURIComponent(forum_name),
				"ForumID":$('input#ForumID').val()
			},
			function(ResponseData){
				if(ResponseData=='1'){
					$('#WarnMsgForumName').html('');
					$('#WarnForumName').hide();
				}else{
					$('#WarnMsgForumName').html('<?=$Lang['ReadingGarden']['WarningMsg']['ForumNameExist']?>');
					$('#WarnForumName').show();
					form_valid.val('0');
				}
				js_Check_Form();
			}
		);
	}
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

