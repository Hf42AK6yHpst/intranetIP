<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$ReadingGardenLib = new libreadinggarden();

$ForumID = $_REQUEST['ForumID'];
$DataArr['ForumID'] = $ForumID;
$Action = "delete";

$Success = $ReadingGardenLib->Manage_Forum_Record($Action,$DataArr);

if($Success)
	$Msg = "DeleteSuccess";
else
	$Msg = "DeleteUnsuccess";

header("Location: ./forum.php?Msg=$Msg");
intranet_closedb();
?>