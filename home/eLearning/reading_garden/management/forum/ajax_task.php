<?php
// using : 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "ReloadForumTable":
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		echo $ReadingGardenUI->Get_Management_Forum_Table($Keyword);
	break;
	case "UpdateForumDisplayOrder":
		$ForumIDArr = $RowForumID;
		$Success = $ReadingGardenLib->Management_Update_Forum_Display_Order($ForumIDArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;
	case "CheckForumName":
		$ForumName = trim(urldecode(stripslashes($_REQUEST['ForumName'])));
		$ForumID = trim($_REQUEST['ForumID']);
		$Success = $ReadingGardenLib->Management_Check_Forum_Name($ForumName,$ForumID);
		if($Success)
			echo "1";
		else
			echo "0";
	break;
}

intranet_closedb();
?>