<?php
// using :

/*
 * ******************** Change Log ********************
 * Date		:	2011-09-08 Yuen - support iPad using plain text editor
*/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$ReadingGardenLib = new libreadinggarden();

$AnnouncementID = $_REQUEST['AnnouncementID'];
$DataArr['Title'] = trim(stripslashes($_REQUEST['Title']));
$Attachment = ($userBrowser->platform=="iPad") ? array() : Array_Trim((array)$Attachment);

if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")

{

	if ($_REQUEST['Message']==strip_tags($_REQUEST['Message']))

	{

		$_REQUEST['Message'] = nl2br($_REQUEST['Message']);

	}

}

$DataArr['Message'] = trim(stripslashes($_REQUEST['Message']));
$DataArr['ClassID'] = $_REQUEST['ClassID'];
$DataArr['StartDate'] = trim(stripslashes($_REQUEST['StartDate']));
$DataArr['EndDate'] = trim(stripslashes($_REQUEST['EndDate']));

if(trim($AnnouncementID)=='')
	$Action = "add";
else
	$Action = "edit";

if(!empty($Attachment) || count($old_Attachment))
{
	$Path = $ReadingGardenLib->Settings_Announcement_Upload_Attachment($AssignedReadingID, $Attachment, $Attachment_name, $TempPath, $old_Attachment);
	$DataArr['Attachment'] = $Path;
}
if($Path || empty($Attachment))
{
	$Success = $ReadingGardenLib->Manage_Announcement_Record($Action,$DataArr,$AnnouncementID);
	
	if($Action=="add")
	{
		if($Success)
			$Msg = "AddSuccess";
		else
			$Msg = "AddUnsuccess";
	}
	else if($Action=="edit")
	{
		if($Success)
			$Msg = "UpdateSuccess";
		else
			$Msg = "UpdateUnsuccess";
	}
} else
{
	# special case;
	//debug("spe");
}

header("location: ./announcement.php?Msg=$Msg");

intranet_closedb();
?>
