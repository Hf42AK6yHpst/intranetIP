<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

if($Keyword)
	$Keyword = stripslashes($Keyword);

$arrCookies[] = array("ck_settings_announcement_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_announcement_page_number", "pageNo");
$arrCookies[] = array("ck_settings_announcement_page_order", "order");
$arrCookies[] = array("ck_settings_announcement_page_field", "field");	
$arrCookies[] = array("ck_settings_announcement_page_keyword", "Keyword");

if($clearCoo == 1)
	clearCookies($arrCookies);
else
	updateGetCookies($arrCookies);

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Management_Announcement";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AnnouncementMgmt']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

//echo $ReadingGardenUI->Include_JS_CSS();

echo $ReadingGardenUI->Get_Management_Announcement_UI($field, $order, $pageNo, $numPerPage, $Keyword);

?>
<script>

</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>