<?
// Modifying by: 
############# Change Log [Start] ################
#
#
############# Change Log [End] ##################

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();
$limport = new libimporttext();
$lo = new libfilesystem();

### Check File Format
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_award_record_step1.php?ReturnMsg=WrongFileFormat");
	exit();
}

# Get Data from Import File
$data = $limport->GET_IMPORT_TXT($csvfile);

# Check No of Record
$numOfBook = count($data) - 1;
if($numOfBook == 0){
	header("location: import_award_record_step1.php?ReturnMsg=ImportUnsuccess_NoRecord");
	exit();
}

# Check File Column Header
if(is_array($data))
{
	$col_name = array_shift($data);
}
$file_format = array("Class Name","Class Number","User Login","Item Code","Times");
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import_award_record_step1.php?ReturnMsg=WrongCSVHeader");
	exit();
}


### Title / Menu
$CurrentPage = "Management_PopularAwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;
$TAGS_OBJ[] = array($Lang['ReadingGarden']['PopularAwardSchemeArr']['ImportAwardRecord'], "", "");

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

# Return Message
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['ReturnMsg']));
$linterface->LAYOUT_START($ReturnMessage);


### Move the import File to Temp Folder
$folder_prefix = $intranet_root."/file/import_temp/elibrary/reading_scheme/award";

if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);
$SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($csvfile, $TargetFilePath);

# Navaigtion Btn
$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['PopularAwardScheme'], "javascript:js_Cancel();");
$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['PopularAwardSchemeArr']['ImportAwardRecord'], "");

### Top Info Table
$BookImportResultInfoTable = '';
$BookImportResultInfoTable .= '<table class="form_table_v30">'."\n";
	$BookImportResultInfoTable .= '<tr>'."\n";
		$BookImportResultInfoTable .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$BookImportResultInfoTable .= '<td><div id="SuccessCountDiv">'.$numOfValidRecords.'</div></td>'."\n";
	$BookImportResultInfoTable .= '</tr>'."\n";
	$BookImportResultInfoTable .= '<tr>'."\n";
		$BookImportResultInfoTable .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$BookImportResultInfoTable .= '<td><div id="FailCountDiv"></div></td>'."\n";
	$BookImportResultInfoTable .= '</tr>'."\n";
$BookImportResultInfoTable .= '</table>'."\n";

### iFrame for validation
$thisSrc = "ajax_validation.php?RecordType=import_award&TargetFilePath=".$TargetFilePath;
$ImportIFrame .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";
//$ImportIFrame .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;"></iframe>'."\n";

### Block UI Msg
$ProcessingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfBook, $Lang['SysMgr']['Timetable']['RecordsValidated']);

### Buttons
$import_buttons = '';
$import_buttons .= '<div class="edit_bottom_v30">'."\n";
	$import_buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "js_Go_Import();", 'ImportBtn', '', $Disabled=1);
	$import_buttons .= " &nbsp;";
	$import_buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
	$import_buttons .= " &nbsp;";
	$import_buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");
$import_buttons .= '</div>'."\n";

?>

<script language="javascript">
$(document).ready(function () {
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Go_Back()
{
	window.location = 'import_award_record_step1.php';
}

function js_Cancel()
{
	window.location = 'index.php';
}

function js_Go_Import()
{
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = "import_award_record_step3.php";
	jsObjForm.submit();
}
</script>

<br />
<form id="form1" name="form1" method="post">
	<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
	<br />
	<?= $linterface->GET_IMPORT_STEPS($CurrStep=2) ?>
	<div class="table_board">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td align="center">
				<?=$BookImportResultInfoTable?>
				<br style="clear:both;" />
				<div id="ErrorTableDiv"></div>
				</td>
			</tr>
		</table>
	</div>
	<?=$import_buttons?>
	<?=$ImportIFrame?>
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>