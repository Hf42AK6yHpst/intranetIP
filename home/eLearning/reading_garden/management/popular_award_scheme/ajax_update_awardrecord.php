<?php
// using Rita

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data from JS
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Import_Award")
{
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### Get the Temp Data To be Insert
	$sql = "Select * from TEMP_READING_SCHEME_AWARD_IMPORT where UserID = '".$_SESSION['UserID']."'  order by RowNumber";
	$TempAwardRecordArr = $ReadingGardenLib->returnArray($sql);
	$numOfAwardRecord = count($TempAwardRecordArr);
	
	$SuccessArr = array();
	for ($i=0; $i<$numOfAwardRecord; $i++)
	{		
		$thisItemID = $TempAwardRecordArr[$i]['ItemID'];
		$thisTimes = $TempAwardRecordArr[$i]['Times'];
		$thisStudentID = $TempAwardRecordArr[$i]['StudentID'];
				
		### Insert into DB ####
		for ($j=0; $j<$thisTimes; $j++){
			$SuccessArr['TempID'] = $ReadingGardenLib->Submit_Popular_Award_Scheme_Record($thisItemID, $thisStudentID);
		}
		
		### Update Imported Number of Record
 		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUISpan").innerHTML = "'.($i + 1).'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("NumOfProcessedPageSpan").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	
	### Remove Temp Records
	$ReadingGardenLib->Delete_Book_Import_Temp_Data();
}
echo $success;


intranet_closedb();
?>