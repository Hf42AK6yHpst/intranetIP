<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "GetPopularAwardSchemeStudentRecordTable":
		$CategoryID = $_REQUEST['CategoryID'];
		$StudentID = $_REQUEST['StudentID'];
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

		$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_size", "numPerPage");
		$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_number", "pageNo");
		$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_order", "order");
		$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_field", "field");	
		$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_CategoryID", "CategoryID");
		updateGetCookies($arrCookies);
		
		echo $ReadingGardenUI->Get_Popular_Award_Scheme_Form_Student_Score_Record_DBTable($StudentID, $field, $order, $pageNo, $numPerPage, $CategoryID);	
	break;
}	

intranet_closedb();
?>