<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_size", "numPerPage");
$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_number", "pageNo");
$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_order", "order");
$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_field", "field");	
$arrCookies[] = array("ck_setting_popular_award_scheme_student_record_page_CategoryID", "CategoryID");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Management_PopularAwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['PopularAwardScheme'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Popular_Award_Scheme_Form_Student_Score_Record_UI($_REQUEST['StudentID'], $field, $order ,$pageNo ,$numPerPage ,$_REQUEST['CategoryID']);

?>
<script>
function js_Reload_Popular_Award_Scheme_Record_Table()
{
	var formData = $('#form1').serialize();
//	var CategoryID =  $('#CategoryID').val()?$('#CategoryID').val():'';
//	var StudentID =  $('#StudentID').val()?$('#StudentID').val():'';
	formData += "&Action=GetPopularAwardSchemeStudentRecordTable";
	
	Block_Element("StudentRecordDBTable");
	$.ajax({  
		type: "POST",  
//		url: "ajax_reload.php?CategoryID="+CategoryID+"&StudentID="+StudentID,
		url: "ajax_reload.php",
		data: formData,
		cache:false,
		success: function(ReturnData) {
			$("div#StudentRecordDBTable").html(ReturnData);
			UnBlock_Element("StudentRecordDBTable");
			Scroll_To_Top();
		} 
	});
}

function js_Delete_Popular_Award_Scheme_Record()
{
	var StudentItemIDArr = new Array();
	$(".StudentItemID:checked").each(function(){
		StudentItemIDArr.push($(this).val());
	})
	var StudentItemID = StudentItemIDArr.join(",");
	
	$.post(
		"ajax_update.php",
		{
			Action:"DeletePopularAwardSchemeRecord",
			StudentItemID:StudentItemID
		},
		function(ReturnData){
			Get_Return_Message(ReturnData);
			js_Reload_Popular_Award_Scheme_Record_Table()
		}
	)	
}

function sortPage(order,field,formObj)
{
	formObj.order.value = order;
	formObj.field.value = field;
	formObj.pageNo.value = 1;
	js_Reload_Popular_Award_Scheme_Record_Table();
}

$(function(){
	js_Reload_Popular_Award_Scheme_Record_Table();
});
</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>