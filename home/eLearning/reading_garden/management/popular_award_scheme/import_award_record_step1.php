<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Management_PopularAwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;

# Page Title
$TAGS_OBJ[] = array($Lang['ReadingGarden']['PopularAwardSchemeArr']['ImportAwardRecord'], "", "");
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

# Return Message
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['ReturnMsg']));
$linterface->LAYOUT_START($ReturnMessage);

# Navaigtion
$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['PopularAwardScheme'], "javascript:js_Cancel();");
$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['PopularAwardSchemeArr']['ImportAwardRecord'], "");

# CSV Sample File
$csvFile = "import_award_record_sample.csv";
$SampleCSVFile = "<a class='tablelink' href='". GET_CSV($csvFile) ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

# Buttons
$ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();");
$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();");

# Column Property Display
$ColumnTitleArr = $Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'] ;
$ColumnPropertyArr = array(3,3,3,1,1);
$RemarksArr = array();
$RemarksArr[0] =  '<a id="remarkBtn_class" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'class\');">['.$Lang['General']['Remark'].']</a>';
$RemarksArr[3] =  '<a id="remarkBtn_itemCode" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'itemCode\');">['.$Lang['General']['Remark'].']</a>';

$ColumnDisplay = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
	
# Get Exisiting class
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
$YearObj = new Year($YearClassID);
$classList = $YearObj->Get_All_Classes();
$numOfClass = count($classList);

# Get Exisiting item
$itemListArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Item_Info();
$numOfItemListArr = count($itemListArr);


# Form Display 
$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="post" action="import_award_record_step2.php" enctype="multipart/form-data">'."\n";
	$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
	$x .= '<br />'."\n";
	$x .= $linterface->GET_IMPORT_STEPS($CurrStep=1);
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<table class="form_table_v30">'."\n";
				
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['General']['SourceFile'].' <span class="tabletextremark">'.$Lang['General']['CSVFileFormat'].'</span></td>'."\n";
							$x .= '<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['General']['CSVSample'].'</td>'."\n";
							$x .= '<td>'.$SampleCSVFile.'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'."\n";
								$x .= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']."\n";
							$x .= '</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $ColumnDisplay."\n";
								$x .= "<br /><br />\n";
								$x .= '<span class="tabletextremark">('.$Lang['SysMgr']['ReadingGarden']['ImportAwardRecord']['UserInfoRemarks'].')</span>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="tabletextremark" colspan="2">'."\n";
								$x .= $linterface->MandatoryField();
								$x .= $linterface->ReferenceField();
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	
	# Buttons
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $ContinueBtn;
		$x .= "&nbsp;";
		$x .= $CancelBtn;
	$x .= '</div>'."\n";

	
	# Class Remarks Layer
	$thisRemarksType = 'class';
	$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:200px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									//$x .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
									$x .= '<th>'.$ColumnTitleArr[0].'</th>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
							$x .= '<tbody>'."\n";
											
							for($i=0; $i<$numOfClass; $i++){
								//$classID = $classList[$i]['YearClassID'];
								$thisClassNameENG = $classList[$i]['ClassTitleEN'];
								$thisClassNameCHI = $classList[$i]['ClassTitleB5'];								
								$thisClassNameByLang = Get_Lang_Selection($thisClassNameCHI, $thisClassNameENG);
												
								$x .= '<tr>'."\n";
								//$x .= '<td>'.$classID.'</td>'."\n";
								$x .= '<td>'.$thisClassNameByLang.'</td>'."\n";
								$x .= '</tr>'."\n";							
							}			
																																																							
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
	
	
	# Item Code Remarks Layer	
	$thisRemarksType = 'itemCode';
	$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";									
									$x .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
									$x .= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Item'].'</th>'."\n";																											
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
							$x .= '<tbody>'."\n";
							
							for($i=0;$i<$numOfItemListArr;$i++){
								$thisItemCode = $itemListArr[$i]['ItemCode'];
								$thisItemDescENG = $itemListArr[$i]['ItemDescEn'];
								$thisItemDescCHI = $itemListArr[$i]['ItemDescB5'];
								
								$thisItemName = Get_Lang_Selection($thisItemDescENG,$thisItemDescCHI);
								$x .= '<tr>'."\n";
									$x .= '<td width="50%">'.$thisItemCode.'</td>'."\n";	
									$x .= '<td width="50%">'.$thisItemName.'</td>'."\n";				
								$x .= '</tr>'."\n";	
							}
																										
							$x .= '<tbody>'."\n";
							for ($i=0; $i<$numOfCategoryListArrEng; $i++){	
								$thisCategoryEngName = $categoryListArrEng[$i]['CategoryName'];			
								$x .= '<tr>'."\n";
									$x .= '<td>'.$thisCategoryEngName.'</td>'."\n";				
								$x .= '</tr>'."\n";				
							}																																
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";

$x .= '</form>'."\n";
$x .= '<br />'."\n";

echo $x;

?>

<script language="javascript">

function js_Continue() {
	if (Trim(document.getElementById('csvfile').value) == "") {
		 alert('<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>');
		 document.getElementById('csvfile').focus();
		 return false;
	}
	
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = "import_award_record_step2.php";
	jsObjForm.submit();
}

function js_Go_Back() {
	window.location = 'index.php';
}

function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function js_Cancel()
{
	window.location = 'index.php';
}


function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}

</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>