<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();


if(!$ReadingGardenLib->isStudent && $clearCoo==1) // use default ClassID instead of ClassID saved in cookie. 
	$ClassID = $ReadingGardenLib->Get_Teacher_View_Default_Class_ID();

$ReadingGardenLib->Update_Portal_Class_Session();

$AssignedReadingList = $ReadingGardenLib->Get_Index_Assigned_Reading_List();
$LoadPageOnReady = count($AssignedReadingList)>0?'AssignedReading':'ReadingRecord';

$linterface = new interface_html();

$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_DatePicker_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_Answer_Sheet_CSS();
echo $ReadingGardenUI->Get_Reading_Garden_Index($ClassID);


?>
<script>
function js_Load_MyRecord_Area(Area)
{
	Block_Element("MyRecord_Area");
	SwtichTab(Area);
	$.post(
		"ajax_index_reload.php",
		{
			Action:"ReloadMyRecordArea",
			TagSelected:Area
		},
		function(ReturnData){
			$("div#MyRecord_Area").html(ReturnData);
			initThickBox();
			CurrRow = null;

			UnBlock_Element("MyRecord_Area");
			$("#CurrentTab").val(Area);
			
		}
	);
}

function SwtichTab(Selected)
{
	var TabID = Selected+"Tab";
//	if(Selected == 'AssignedReading')
//	{
//		var On = 'AssignedReadingTab';
//		var Off = 'ReadingRecordTab';
//	}
//	else
//	{
//		var Off = 'AssignedReadingTab';
//		var On = 'ReadingRecordTab';
//	}
	$("li").removeClass("mybook_record_tab_current")
	$("li#"+TabID).addClass("mybook_record_tab_current")
}

var CurrRow;
function js_Show_Book(row)
{
	if(CurrRow == row) return true;
	
	//hide Current
//	$("div.reading_recommend_book_list").find("div.recommend_book_cover").hide();
//	$("div.reading_recommend_book_list").find("div.like_comment").hide();
//	$("div.book_list_item:eq("+CurrRow+")").removeClass("reading_recommend_book_list");
	
	//show Clicked
	$("div.book_list_item:eq("+row+")").addClass("reading_recommend_book_list");
	$("div.reading_recommend_book_list").find("div.recommend_book_cover").show();
	$("div.reading_recommend_book_list").find("div.like_comment").show();
	
	CurrRow = row
}

function js_Go_To_Student_Record()
{
	var CurrTab = $("#CurrentTab").val();
	window.location = "student_record/index.php?CurrentTab="+CurrTab;
}

function js_Select_Announcement_Type(jsType)
{
	if($("#AnnouncementType").val()!=jsType)
	{
		$(".thumb_list_tab_on").removeClass("thumb_list_tab_on");
		$("#AnnouncementType").val(jsType);
		$("#AnnouncementTab"+jsType).addClass("thumb_list_tab_on");
		js_Reload_Announcement_List()
	}
}

function js_Reload_Announcement_List()
{
	Block_Element("reading_annoucement_list");
	var AnnouncementType = $("#AnnouncementType").val();
	var ClassID = $("#ClassID").val()?$("#ClassID").val():'';
	$.post(
		"ajax_index_reload.php",
		{
			Action:"ReloadAnnouncementArea",
			AnnouncementType:AnnouncementType,
			ClassID:ClassID
		},
		function(ReturnData){
			$("#reading_annoucement_list").html(ReturnData);
			UnBlock_Element("reading_annoucement_list");
		}
	)
}

function js_Reload_Recommend_Report_List()
{
	Block_Element("reading_recommend_report");
	$.post(
		"ajax_index_reload.php",
		{
			Action:"ReloadRecommendReportArea"
		},
		function(ReturnData){
			$("#reading_recommend_report").html(ReturnData);
			UnBlock_Element("reading_recommend_report");
		}
	)
}

function js_View_Award_Level_Layer(LevelID)
{
	$("#Layer"+LevelID).siblings(".LevelLayer").hide();
	$("#Layer"+LevelID).toggle();
}

function js_View_Level()
{
	newWindow("view_level.php",10);
}

$().ready(function(){
	if(<?=$ReadingGardenLib->isStudent==1?"true":"false"?>)
		js_Load_MyRecord_Area('<?=$LoadPageOnReady?>');
	else
		js_Load_Teacher_View_Area('ReadingTarget');
	js_Reload_Announcement_List();
	
})

/* teacher view */
function js_Load_Teacher_View_Area(Area)
{
	var ClassID = $("#ClassID").val()?$("#ClassID").val():'';
	
	Block_Element("MyRecord_Area");
	SwtichTab(Area);
	if(Area == "StudentState"){
		$("span#moreBtn").hide();		
		$("span#schExportBtn").hide();
		$("span#classExportBtn").hide();		
	}		
	else{
		$("span#moreBtn").show();	
		$("span#schExportBtn").show();	
		$("span#classExportBtn").show();	
	}

	$.post(
		"ajax_index_reload.php",
		{
			Action:"ReloadTeacherViewArea",
			TagSelected:Area,
			ClassID:ClassID
		},
		function(ReturnData){
			$("div#MyRecord_Area").html(ReturnData);
			initThickBox();
			UnBlock_Element("MyRecord_Area");
			$("#CurrentTab").val(Area);
		}
	);
}

// Call from js_Save_Marking in home/eLearning/reading_garden/book_report_marking.php
function js_Mark_Report_CallBack()
{
	js_Reload_Recommend_Report_List();
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>