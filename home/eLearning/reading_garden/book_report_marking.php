<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");


//if(!isset($_SESSION['UserID']) && isset($_REQUEST['key']))
if(isset($_REQUEST['key']))
{
	if(!libreadinggarden::Grant_View_Right_For_Report_Page($_REQUEST['key'])) exit;
	$PublicPage = true;
	$key = $_REQUEST['key'];
}else{
	intranet_auth();
}
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$ReadingRecordID = $_REQUEST['ReadingRecordID'];
$BookReportInfo= $ReadingGardenLib->Get_Student_Book_Report('',$ReadingRecordID);
$BookReportID = $BookReportInfo[0]['BookReportID'];
$isMarked = $BookReportInfo[0]['Grade']?1:0;

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?=$intranet_session_language?>.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/script.js" language="JavaScript"></script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content_25.css">
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content_30.css">
<?
echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
?>
<style type="text/css">
.msg_board_left { height:30px; margin:0 auto; float:none; display:block; padding-left:8px; width:200px; background:url(../../../images/2009a/system_message_left.gif) no-repeat top left;}


</style>
<script language="JavaScript">
var jsKey = '<?=$key?>';
function js_Save_Marking(jsClose)
{
	Block_Element("DivMarkingArea");
	var Recommend = ($('#Recommend').attr('checked')?1:0);
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"MarkBookReport",
			"BookReportID":$('input#BookReportID').val(),
			"Grade":$('input#Grade').val(),
			"TeacherComment":encodeURIComponent($('#TeacherComment').val()),
			"Recommend":Recommend,
			"OldRecommend":$('input#OldRecommend').val(),
			"StudentID":$('input#StudentID').val(),
			"StickerID[]":Get_Element_Values('StickerID[]') 
		},
		function(ResponseData){
			if(ResponseData=="1"){
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				$('input#OldRecommend').val(Recommend);
				
				if(parent.opener.js_Mark_Report_CallBack)
					parent.opener.js_Mark_Report_CallBack();
			}else{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
			}
			
			UnBlock_Element("DivMarkingArea");
			if(jsClose==1){
				window.parent.close();
			}
		}
	);
}

function Get_Element_Values(name)
{
	var elems = $('input[name='+name+']');
	var returnArray = new Array();
	elems.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function js_Load_Attachment()
{
	$('#AnswerSheetBtn').attr('class','');
	$('#AttachmentBtn').attr('class','thumb_list_tab_on');
	$('#OnlineWritingBtn').attr('class','');
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportAttachment",
			"key":window.parent.jsKey,
			"ReadingRecordID":$('#ReadingRecordList').val()
		},
		function(ReturnData){
			if(ReturnData!=''){
				var indexLastSlash = ReturnData.lastIndexOf('/',ReturnData);
				var folder = ReturnData.substring(0,indexLastSlash+1);
				var file = encodeURIComponent(ReturnData.substr(indexLastSlash+1));
				top.ViewFrame.location = "<?=$ReadingGardenLib->BookReportPath?>"+folder+file;
			}
		}
	);
}

function js_Load_Answersheet()
{
	$('#AnswerSheetBtn').attr('class','thumb_list_tab_on');
	$('#AttachmentBtn').attr('class','');
	$('#OnlineWritingBtn').attr('class','');
	var reading_record_id = $('#ReadingRecordList').val();
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportID",
			"key":window.parent.jsKey,
			"ReadingRecordID":reading_record_id
		},
		function(ReturnData){
			if(ReturnData!=''){
				top.ViewFrame.location = 'answer_sheet.php?inframe=1&reading_record_id='+reading_record_id+'&book_report_id='+ReturnData+'&key='+window.parent.jsKey;
			}
		}
	);
}

function js_Load_Online_Writing()
{
	$('#OnlineWritingBtn').attr('class','thumb_list_tab_on');
	$('#AnswerSheetBtn').attr('class','');
	$('#AttachmentBtn').attr('class','');
	var reading_record_id = $('#ReadingRecordList').val();
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportID",
			"key":window.parent.jsKey,
			"ReadingRecordID":reading_record_id
		},
		function(ReturnBookReportID){
			if(ReturnBookReportID!=''){
				top.ViewFrame.location = 'online_writing.php?book_report_id='+ReturnBookReportID+'&key='+window.parent.jsKey;
			}
		}
	);
}

function js_Load_Marking_Area()
{
<?php if ($PublicPage) { ?>
	return;
<?php } ?>
	var edit_frame_dom = $(window.parent.frames["EditFrame"].document)
	var edit_frame_div = edit_frame_dom.find('div#DivMarkingArea');
	edit_frame_div.parent().load(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportMarkingEditArea",
			"ReadingRecordID":$('#ReadingRecordList').val(),
			"key":window.parent.jsKey,
			"RecommendOnly": '<?=$_REQUEST['RecommendOnly']?>'
		},
		function(ReturnData){
			
		}
	);
}

function js_Refresh_View_Buttons()
{
	$('#ViewButtonsLayer').load(
		"ajax_marking_task.php",
		{
			"Action":"GetViewButtonsLayer",
			"key":window.parent.jsKey,
			"ReadingRecordID":$('#ReadingRecordList').val()
		},
		function(ReturnData){
			
		}
	);
}

function js_Set_Comment(jObj,jTarget)
{
	var targetObj = document.getElementById(jTarget);
	var targetVal = $.trim(targetObj.value);
	targetObj.value += targetVal==''?$(jObj).html():"\n"+$(jObj).html();
}

function js_Get_Comment(jCaller, jArrData, jMenu, jTarget)
{
	var listStr = "";
	var count = 0;
	
	var container = $('#'+jMenu);
	var ref = $(jCaller).closest('tr');
	var pos = ref.position();
	var btn_hide = (typeof(js_btn_hide)!="undefined") ? js_btn_hide : "";
	var box_height = (jArrData.length>10) ? "200" : "105";
	
	listStr  = "<table width='220' height='100' border='0' cellpadding='0' cellspacing='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='19'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_01.gif"?>' width='5' height='19'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='19' valign='middle' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_02.gif"?>'>&nbsp;</td>";
	listStr += "<td style='border:0px; padding:0px;' width='19' height='19'><a href='javascript:void(0)' onClick=\"$('#" + jMenu + "').hide();\"><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_close_off.gif"?>' name='pre_close' width='19' height='19' border='0' id='pre_close' onMouseOver=\"MM_swapImage('pre_close','','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_close_on.gif"?>',1)\" onMouseOut='MM_swapImgRestore()' /></a></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' ><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr style='border:0px; padding:0px;' >";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='50' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_04.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_04.gif"?>' width='5' height='19'></td>";
	
	listStr += "<td style='border:0px; padding:0px;' bgcolor='#FFFFF7'><div style=\"overflow-x: auto; overflow-y: auto; height: " + box_height + "px; width: 210px; \">";
	
	listStr += "<table width='220' border='0' cellspacing='0' cellpadding='2' class='tbclip'>";
	if (typeof(jArrData)=="undefined" || jArrData.length==0)
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<tr><td style='border:0px; padding:0px;' class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
		}
	} else
	{
		for (var i = 0; i < jArrData.length; i++)
		{
			tmpShowValue = jArrData[i];
			
			listStr += "<tr><td style=\"border-bottom:1px #EEEEEE solid; background-color: #FFFFFF\">";
			listStr += "<a class='presetlist' style=\"color:#4288E8\" href=\"javascript:void(0);\"  onclick=\"js_Set_Comment(this,'"+jTarget+"');$('#" + jMenu + "').hide();\" title=\"" + tmpShowValue + "\">" + tmpShowValue + "</a>";
			listStr += "</td></tr>";
			count ++;
		}
	}
	listStr += "</table></div></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_06.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_06.gif"?>' width='6' height='6' border='0' /></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='6'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_07.gif"?>' width='5' height='6'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='6' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_08.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_08.gif"?>' width='100%' height='6' border='0' /></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' height='6'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_09.gif"?>' width='6' height='6'></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "</table>";
	
	container.html(listStr);
	container.css({position:'absolute',
					left:pos.left+'px',
					top:pos.top+ref.height()+'px',
					display:''});
}

function js_Set_Sticker(jObj,jTarget,jIndex)
{
	var container = $('#'+jTarget);
	var btn = $(jObj);
	var image = btn.html();
	btn.remove();
	sticker_id = jsStickerList[jIndex].StickerID;
	jsStickerList[jIndex].IsAdded = '1';
	var data = '<a href="javascript:void(0)" onclick="js_Update_Sticker('+sticker_id+',\'0\',\''+jTarget+'\');$(this).remove();" title="<?=$Lang['ReadingGarden']['ClickToRemoveSticker']?>">'+ image + '<input type="hidden" name="StickerID[]" value="'+sticker_id+'" /></a>';
	container.append(data);
	container.css('height',Math.min(360,container.height()+120)+'px');
}

function js_Update_Sticker(jID, jIsAdded, jTarget)
{
	var total_added = 0;
	for(i=0;i<jsStickerList.length;i++){
		if(jsStickerList[i].StickerID == jID)
			jsStickerList[i].IsAdded = jIsAdded;
		if(jsStickerList[i].IsAdded=='1') total_added++;
	}
	var container = $('#'+jTarget);
	container.css('height',Math.min(360,total_added*120)+'px');
}

function js_Get_Sticker(jCaller, jArrData, jMenu, jTarget)
{
	var listStr = "";
	var count = 0;
	
	var container = $('#'+jMenu);
	var ref = $(jCaller).closest('td');
	var pos = ref.position();
	var btn_hide = (typeof(js_btn_hide)!="undefined") ? js_btn_hide : "";
	var box_height = (jArrData.length>10) ? "600" : "360";
	
	listStr  = "<table width='150' height='100' border='0' cellpadding='0' cellspacing='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='19'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_01.gif"?>' width='5' height='19'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='19' valign='middle' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_02.gif"?>'>&nbsp;</td>";
	listStr += "<td style='border:0px; padding:0px;' width='19' height='19'><a href='javascript:void(0)' onClick=\"$('#" + jMenu + "').hide();\"><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_close_off.gif"?>' name='pre_close' width='19' height='19' border='0' id='pre_close' onMouseOver=\"MM_swapImage('pre_close','','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_close_on.gif"?>',1)\" onMouseOut='MM_swapImgRestore()' /></a></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' ><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr style='border:0px; padding:0px;' >";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='50' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_04.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_04.gif"?>' width='5' height='19'></td>";
	
	listStr += "<td style='border:0px; padding:0px;' bgcolor='#FFFFF7'><div style=\"overflow-x: hidden; overflow-y: auto; height: " + box_height + "px; width: 150px; \">";
	
	listStr += "<table width='150' border='0' cellspacing='0' cellpadding='2' class='tbclip'>";
	if (typeof(jArrData)=="undefined" || jArrData.length==0)
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<tr><td style='border:0px; padding:0px;' class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
		}
	} else
	{
		for (var i = 0; i < jArrData.length; i++)
		{
			if(jArrData[i].IsAdded=='1') continue;
			image_path = jArrData[i].ImagePath;
			
			listStr += "<tr><td style=\"border-bottom:1px #EEEEEE solid; background-color: #FFFFFF\">";
			listStr += "<a class='presetlist' style=\"color:#4288E8\" href=\"javascript:void(0);\"  onclick=\"js_Set_Sticker(this,'"+jTarget+"','"+i+"');$('#" + jMenu + "').hide();\"><img src=\""+image_path+"\" width=\"120px\" height=\"120px\" /></a>";
			listStr += "</td></tr>";
		}
	}
	listStr += "</table></div></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_06.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_06.gif"?>' width='6' height='6' border='0' /></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='6'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_07.gif"?>' width='5' height='6'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='6' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_08.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_08.gif"?>' width='100%' height='6' border='0' /></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' height='6'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_09.gif"?>' width='6' height='6'></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "</table>";
	
	container.html(listStr);
	container.css({position:'absolute',
					left:pos.left+'px',
					top:pos.top+'px',
					display:''});
}

<?php
if($PublicPage){
?>
/*
$(window).unload(function(evt){
	$.get(
		"ajax_marking_task.php",
		{
			"Action":"UnregisterSession"
		},
		function(ReturnData){
			
		}
	);
});
*/
<?php	
}
?>
</script>
<?

$ReadingRecordID = $_REQUEST['ReadingRecordID'];
$RecommendOnly = $_REQUEST['RecommendOnly'];

$Part = $_REQUEST['Part'];

if($Part=='top'){// top part - student info area
	echo $ReadingGardenUI->Get_Book_Report_Marking_Top_Info_Area($ReadingRecordID,$RecommendOnly, $PublicPage);
		echo '<script>'."\n";
		echo '$(document).ready(function(){
				js_Refresh_View_Buttons();
			  });';
		echo '</script>'."\n";
}elseif($Part=='right'){ // right part - mark & comment area
	echo $ReadingGardenUI->Get_Book_Report_Marking_Edit_Area($ReadingRecordID,$RecommendOnly, $PublicPage);
}else{ // whole frameset
	echo $ReadingGardenUI->Get_Book_Report_Marking_Page($ReadingRecordID,$RecommendOnly, $PublicPage);
}

//if($PublicPage){
//	unset($_SESSION['UserID']);
//}
intranet_closedb();
?>