<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Statistic";
$CurrentPageArr['eLearningReadingGarden'] = 1;
$TAGS_OBJ[] = array($Lang['ReadingGarden']['StatisticArr']['MenuTitle'], "", 1);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Statistic_Index();

?>
<script language="javascript">
function js_Reload_Date_Range()
{
	var AcademicYearID = $("select#AcademicYearID","#form1").val();
	$.post(
		"ajax_reload.php",
		{
			"Action":"GetPeriodOfAcademicYear",
			"AcademicYearID":AcademicYearID
		},
		function(ReturnData){
			DateArr = ReturnData.split("|==|");
			$("input#FromDate","#form1").val(DateArr[0]);
			$("input#ToDate","#form1").val(DateArr[1]);
		}
	)
}

function js_Reload_Row_Item_Selection()
{
	var RowItem = $("input.RowItem:checked","#form1").val();
	if(RowItem=="Form")
	{
		$("tr#ClassSelectionTR").hide();
	}
	else
	{
		$("tr#ClassSelectionTR").show();
	}
}

function js_Reload_Class_Selection()
{
	var YearID = Get_Selection_Value('YearID[]', 'String');
	
	$.post(
		"../ajax_reload.php",
		{
			"Action" : "ReloadClassSelection",
			"SelectionID" : "YearClassID[]",
			"noFirst" : 1,
			"isMultiple" : 1,
			"YearID" : YearID			
		},
		function(ReturnData){
			$("span#ClassSelectionSpan").html(ReturnData);
			js_Select_All('YearClassID[]',true);
		}
	)
}

function js_Reload_Col_Item_Selection()
{
	var ColItem = $("input.ColItem:checked","#form1").val();
	 if(ColItem=="Category")
	{
		$("tr#CategorySelectionTR").show();
		$("tr#MonthSelectionTR").hide();
	}
	else
	{
		$("tr#CategorySelectionTR").hide();
		$("tr#MonthSelectionTR").show();
	}
}

function js_Submit_Form()
{
	var FormValid = true; 
	var RowItem = $("input.RowItem:checked","#form1").val();
	var ColItem = $("input.ColItem:checked","#form1").val();
	
	$("div.WarnMsgDiv").hide();
	
	if(Get_Selection_Value('YearID[]','String')=='')
	{
		$("div#WarnBlankForm").show();	
		FormValid = false;
	}
	
	if(RowItem != 'Form' && Get_Selection_Value('YearClassID[]','String')=='')
	{
		$("div#WarnBlankClass").show();
		FormValid = false;
	}	
	
	if(ColItem == "Category" && Get_Selection_Value('CategoryID','String')=='')
	{
		$("div#WarnBlankCategory").show();
		FormValid = false;
	}
	
	if(FormValid)	
		document.form1.submit();		
}

$().ready(function(){
	js_Reload_Date_Range();
	js_Reload_Row_Item_Selection();
	js_Select_All('YearID[]',true,'js_Reload_Class_Selection();');
	js_Reload_Col_Item_Selection();
	js_Select_All('CategoryID',true);
})
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>