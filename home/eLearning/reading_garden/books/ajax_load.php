<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "GetBookTable":
		$CategoryID = $_REQUEST['CategoryID'];
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		$BookSource = implode(",",(array)$_REQUEST['BookSource']);

		$arrCookies[] = array("ck_book_list_page_size", "numPerPage");
		$arrCookies[] = array("ck_book_list_page_number", "pageNo");
		$arrCookies[] = array("ck_book_list_page_order", "order");
		$arrCookies[] = array("ck_book_list_page_field", "field");	
		$arrCookies[] = array("ck_book_list_page_keyword", "Keyword");
		//$arrCookies[] = array("ck_book_list_page_YearID", "YearID");
		$arrCookies[] = array("ck_book_list_page_CategoryID", "CategoryID");
		$arrCookies[] = array("ck_book_list_page_BookSource", "BookSource");
		updateGetCookies($arrCookies);
		
		$BookSourceArr = explode(",",$BookSource);
				
		echo $ReadingGardenUI->Get_Book_DBTable($field, $order, $pageNo, $numPerPage, $Keyword, $CategoryID, $BookSourceArr);
	break;
	case "GetBookEditForm":
		$IsNew = $_REQUEST['IsNew'];
		$BookID = $_REQUEST['BookID'];
		echo $ReadingGardenUI->Get_Book_Edit_Form($IsNew,$BookID);
	break;
	case "GetELIBBookLayer":
		echo $ReadingGardenUI->Get_ELIB_Book_Layer();
	break;
	
	// moved to reading_garden/ajax_reload.php
//	case "GetCategorySelection":
//		$Language = $_REQUEST['Language'];
//		$SelCategoryID = $_REQUEST['SelCategoryID'];
//		echo $ReadingGardenUI->Get_Category_Selection("SelCategoryID", "SelCategoryID", $SelCategoryID, '', '', '', $Language);
//	break;
}

intranet_closedb();
?>