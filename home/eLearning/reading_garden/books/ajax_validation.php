<?php
// using Rita
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$lexport = new libexporttext();
$limport = new libimporttext();
$ReadingGardenLib = new libreadinggarden();
$YearObj = new form_class_manage();
$libdb = new libdb();

### Get Variables
$TargetFilePath = stripslashes($_REQUEST['TargetFilePath']);
	
### Include js libraries
$jsInclude = '';
$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
echo $jsInclude;

### Get Data from the csv file
$data = $limport->GET_IMPORT_TXT($TargetFilePath, $incluedEmptyRow=0, $lineBreakReplacement='<!--linebreakhere-->');
$col_name = array_shift($data);
//debug_pr($data);
$SuccessArr = array();
$SuccessArr['Delete_Old_Temp_Records'] = $ReadingGardenLib->Delete_Book_Import_Temp_Data();

//debug_pr($data);
# Get exisiting form
$yearFormArr = $YearObj -> Get_Form_List('',1,'','','','');
$numOfForm = count($yearFormArr);
for($i=0; $i<$numOfForm; $i++){
	$yearFormName = $yearFormArr[$i]['YearName'];
	$yearFormNameArr[] = $yearFormArr[$i]['YearName'];
	$yearFormIDArr[$yearFormName][]= $yearFormArr[$i]['YearID'];
}

# Get Answer Sheet 
$answerSheetArr =  $ReadingGardenLib ->Get_Answer_Sheet_Template();
$numOfAnswerSheetArr = count($answerSheetArr);
for($i=0; $i<$numOfAnswerSheetArr; $i++){
	$answerSheetNameArr[] = $answerSheetArr[$i]['TemplateName'];
}

##### Check Data and Insert to DB #####
$numOfData = count($data);
$errorCount = 0;
$successCount = 0;
$numForEachRecord=0;
//$formError = 0;
$thisRowNumber ='';

for($i=0;$i<$numOfData;$i++)
{
	$thisErrorMsgArr = array();
	$thisRowNumber = $i + 2;
	$thisBookTitle = trim($data[$i][0]);
	$thisCallNumber = trim($data[$i][1]);
	$thisISBN = trim($data[$i][2]);
	$thisAuthor = trim($data[$i][3]);
	$thisPublisher = trim($data[$i][4]);
	$thisLanguage = trim($data[$i][5]);
	$thisCategory = trim($data[$i][6]);
	$thisDescription = trim($data[$i][7]);
	$thisAssignToForm = trim($data[$i][8]);
	$thisAnswerSheet = trim($data[$i][9]);
		
	### Check Book Title
	if($thisBookTitle==''){
		$thisErrorMsgArr[] = $Lang['ReadingGarden']['Import_Error']['BookTitleIsMissing'];
	}
	
	### Check Call Number ###
	if (strlen($thisCallNumber) > 20){		
		$thisErrorMsgArr[] = $Lang['ReadingGarden']['Import_Error']['InvalidCallNumber'];
	}
	
	
	### Check ISBN ###
	if (strlen($thisISBN) > 20){		
		$thisErrorMsgArr[] = $Lang['ReadingGarden']['Import_Error']['InvalidISBN'];
	}
	
	
	### Check Language
	if($thisLanguage!=''){
		if(STRTOUPPER($thisLanguage)!='CHINESE' && STRTOUPPER($thisLanguage)!='ENGLISH' && STRTOUPPER($thisLanguage)!='OTHERS'){
			$thisErrorMsgArr[] = $Lang['ReadingGarden']['Import_Error']['InvalidLanguage'];
		}
		else{
			# Convert to DB value
			if(STRTOUPPER($thisLanguage)=='CHINESE'){
				$thisLanguageDBValue = CATEGORY_LANGUAGE_CHINESE;
			}
			elseif(STRTOUPPER($thisLanguage)=='ENGLISH'){
				$thisLanguageDBValue = CATEGORY_LANGUAGE_ENGLISH;			
			}
			elseif(STRTOUPPER($thisLanguage)!='OTHERS'){
				$thisLanguageDBValue = CATEGORY_LANGUAGE_OTHERS;	
			}
		}
	}
	else{		
		$thisErrorMsgArr[] = $Lang['ReadingGarden']['Import_Error']['LanguageIsMissing'];
	}
	
	
	### Check Category
	// Get exisiting categories 
	$thisCategoryID = NULL;
	$thisCategoryName == NULL;
	if($thisCategory!=''){
		
		if(strtoupper($thisCategory) == "UNCATEGORIZED"){
//			/$thisCategoryID = NULL;
		}
		else{
			
			$thisCategoryListArr = $ReadingGardenLib->Get_Category_List('',$thisLanguageDBValue);
			$numOfCategoryListArr = count($thisCategoryListArr);	
			
			$thisCategoryCodeArr ='';
			$thisCategoryCode ='';
			for ($j=0; $j<$numOfCategoryListArr; $j++){	
				$thisCategoryCode = $thisCategoryListArr[$j]['CategoryCode'];
				$thisCategoryCodeArr[]=  $thisCategoryCode;
			}	
			
			if(!in_array($thisCategory,$thisCategoryCodeArr, true)){
				$thisErrorMsgArr[] = $Lang['ReadingGarden']['Import_Error']['InvalidCategory'];
			
			}
			else{				
				$thisCategoryListArrByName = $ReadingGardenLib->Get_Category_List('',$thisLanguageDBValue,$libdb->Get_Safe_Sql_Query($thisCategory));
				$thisCategoryID = $thisCategoryListArrByName[0]['CategoryID'];
			}
		}
	}
	else{
		$thisErrorMsgArr[] = $Lang['ReadingGarden']['Import_Error']['CategoryIsMissing'];	
	}
	
	# Check Form
	$tempFormIDArrToString = NULL;
	$tempFormNameArrToString = NULL;
	
	if($thisAssignToForm!=''){
		$tempFormIDArrToString='';
		
		$tempFormNameArr='';
		$eachTempFormIdArr ='';
		$importFormArrlist = explode(',',$thisAssignToForm);
			
		$numOfTempFormArr = count($importFormArrlist);
		$formError=0;
		for($j=0; $j<$numOfTempFormArr; $j++){
			$eachTempFormArr = trim($importFormArrlist[$j]);		
			$tempFormNameArr[] = $eachTempFormArr;	
					
			if(!in_array($eachTempFormArr, $yearFormNameArr, true)){ 
				$formError ++;			
			}
			else{
				$eachTempFormIdArr [] = $yearFormIDArr[$eachTempFormArr][0];	
			}
						
		}
		
		$tempFormNameArrToString = implode(',', $tempFormNameArr);
		$tempFormIDArrToString = implode(',', $eachTempFormIdArr);
		
		if($formError > 0){
			$thisErrorMsgArr['InvalidForm'] = $Lang['ReadingGarden']['Import_Error']['InvalidForm'];
		}

	}
		
	### Check Answer Sheet 
	$thisAnswerSheetID = NULL;
	if($thisAnswerSheet!=''){
		if(!in_array($thisAnswerSheet,$answerSheetNameArr, true)){ 
				$thisErrorMsgArr[] = $Lang['ReadingGarden']['Import_Error']['InvalidAnswerSheet'];
		}
		else{	
					
			$thisAnswerSheetIDArry = $ReadingGardenLib ->Get_Answer_Sheet_Template('',$libdb->Get_Safe_Sql_Query($thisAnswerSheet),'');	
			$thisAnswerSheetID = $thisAnswerSheetIDArry[0]['TemplateID'];
		}
	}
	
	
	
	### Update Processing Display
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= '$("span#BlockUISpan").html("'.($i + 1).'");';
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	### Prepare sql to insert to the temp csv
	$InsertTempDataArr[] = " ( '".$_SESSION['UserID']."', '$thisRowNumber', '".$libdb->Get_Safe_Sql_Query($thisBookTitle)."', '".$libdb->Get_Safe_Sql_Query($thisCallNumber)."', '".$libdb->Get_Safe_Sql_Query($thisISBN)."', '".$libdb->Get_Safe_Sql_Query($thisAuthor)."', '".$libdb->Get_Safe_Sql_Query($thisPublisher)."',
								'$thisLanguage', '".$libdb->Get_Safe_Sql_Query($thisCategory)."', '$thisCategoryID', '".$libdb->Get_Safe_Sql_Query($thisDescription)."', '".$libdb->Get_Safe_Sql_Query($tempFormNameArrToString)."','".$libdb->Get_Safe_Sql_Query($tempFormIDArrToString)."', '".$libdb->Get_Safe_Sql_Query($thisAnswerSheet)."', '$thisAnswerSheetID', now()
							) ";
				
	### Record the error messages		
	if(count($thisErrorMsgArr) == 0){
		$successCount++;	
	}else{
		$RowErrorRemarksArr[$thisRowNumber] = $thisErrorMsgArr;

	}
		
	### Update Processing Display
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
	$thisJSUpdate .= '$("span#BlockUISpan").html("'.($i + 1).'");';
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;	
}

$errorCount = count($RowErrorRemarksArr);

### Insert Data in Temp Table
//$InsertTempDataValue = implode(',', $InsertTempDataArr);
//$sql = "Insert Into TEMP_READING_SCHEME_BOOK_IMPORT
//			(UserID, RowNumber, BookName, CallNumber, ISBN, Author, Publisher, Language, CategoryCode, CategoryID, Description, YearName, YearIDList, DefaultAnswerSheet, DefaultAnswerSheetID, DateInput)
//		Values
//			$InsertTempDataValue
//		";
//$SuccessArr['Insert_Temp_CSV_Records'] = $ReadingGardenLib->db_db_query($sql);


$numOfInsertData = count($InsertTempDataArr);
if ($numOfInsertData > 0) {
	$numOfDataPerChunck = 1000;
	//$numOfChunk = ceil($numOfInsertData / $numOfDataPerChunck);
	$insertDataSplitedArr = array_chunk($InsertTempDataArr, $numOfDataPerChunck);

	foreach((array)$insertDataSplitedArr as $_insertDateArr) {

		$sql = "Insert Into TEMP_READING_SCHEME_BOOK_IMPORT
				(
				UserID, RowNumber, BookName, CallNumber, ISBN, Author, Publisher, Language, CategoryCode, CategoryID, Description, YearName, YearIDList, DefaultAnswerSheet, DefaultAnswerSheetID, DateInput
				)
				Values
				".implode(',', (array)$_insertDateArr);
		
		//debug_pr($sql);	
		$SuccessArr['Insert_Temp_CSV_Records'] = $ReadingGardenLib->db_db_query($sql);

	}
}
	
$ErrorTable = '';
if($errorCount > 0){ 
	
	$ErrorRowNumArr = array_keys($RowErrorRemarksArr);
	$ErrorRowNumList = implode(',', $ErrorRowNumArr);
	

	$sql = "SELECT *
			FROM 
			TEMP_READING_SCHEME_BOOK_IMPORT 
			WHERE 
			UserID = '".$_SESSION['UserID']."' And RowNumber In ($ErrorRowNumList) ";
	$ErrorRowInfoArr = $ReadingGardenLib->returnArray($sql);
	
		
		
	# Error Table Display 
	$ErrorTable .= '<table class="common_table_list_v30 view_table_list_v30">';
	$ErrorTable .= '<thead>';
	$ErrorTable .=  '<tr>';
	$ErrorTable .=  '<th width="10">'.$Lang['General']['ImportArr']['Row'].'</td>';
	$ErrorTable .=  '<th width="10%">'.$Lang['ReadingGarden']['BookName'].'</th>';
	$ErrorTable .=  '<th width="7%">'.$Lang['ReadingGarden']['CallNumber'].'</th>';
	$ErrorTable .=  '<th width="7%">'.$Lang['ReadingGarden']['ISBN'].'</th>';
	$ErrorTable .=  '<th width="10%">'.$Lang['ReadingGarden']['Author'].'</th>';
	$ErrorTable .=  '<th width="10%">'.$Lang['ReadingGarden']['Publisher'].'</th>';
	$ErrorTable .=  '<th width="7%">'.$Lang['ReadingGarden']['Language'].'</th>';
	$ErrorTable .=  '<th width="7%">'.$Lang['ReadingGarden']['Category'].'</th>';
	$ErrorTable .=  '<th width="10%">'.$Lang['ReadingGarden']['Description'].'</th>';
	$ErrorTable .=  '<th width="10%">'.$Lang['ReadingGarden']['AssignToForm'].'</th>';
	$ErrorTable .=  '<th width="10%">'.$Lang['ReadingGarden']['AnswerSheet'].'</th>';
	$ErrorTable .=  '<th width="10%">'.$iDiscipline['AccumulativePunishment_Import_Failed_Reason'].'</th>';
	$ErrorTable .=  '</tr>';	
	$ErrorTable .= '</thead>';	
	
	$ErrorTable .= '<tbody>';
	for ($i=0; $i<$errorCount; $i++)
	{	
		//$numForEachRecord ++;				
	
		### Display Emtpy Value
		$thisRowNumber 	= $ErrorRowInfoArr[$i]['RowNumber'];	
		$thisBookName = $ErrorRowInfoArr[$i]['BookName']!=null? $ErrorRowInfoArr[$i]['BookName'] : '<font color="red">***</font>';
		$thisCallNumber = $ErrorRowInfoArr[$i]['CallNumber']!=null?  $ErrorRowInfoArr[$i]['CallNumber']: '---';
		$thisISBN = $ErrorRowInfoArr[$i]['ISBN']!=null? $ErrorRowInfoArr[$i]['ISBN']: '---';
		$thisAuthor = $ErrorRowInfoArr[$i]['Author']!=null? $ErrorRowInfoArr[$i]['Author']: '---';
		$thisPublisher = $ErrorRowInfoArr[$i]['Publisher']!= null? $ErrorRowInfoArr[$i]['Publisher']: '---';
		$thisLanguage = $ErrorRowInfoArr[$i]['Language']!=null? $ErrorRowInfoArr[$i]['Language']:'<font color="red">***</font>';
		$thisCategory = $ErrorRowInfoArr[$i]['CategoryCode']!=null? $ErrorRowInfoArr[$i]['CategoryCode']:'<font color="red">***</font>';
		$thisDescription = $ErrorRowInfoArr[$i]['Description']!=null? str_replace('<!--linebreakhere-->', '<br />', $ErrorRowInfoArr[$i]['Description']): '---';
		$thisYearName = $ErrorRowInfoArr[$i]['YearName']? $ErrorRowInfoArr[$i]['YearName'] : '---' ;
		$thisDefaultAnswerSheet = $ErrorRowInfoArr[$i]['DefaultAnswerSheet']? $ErrorRowInfoArr[$i]['DefaultAnswerSheet']: '---';
		
		$thisErrorRemarksArr =  $RowErrorRemarksArr[$thisRowNumber];
		
		### Add Bullet '-' 
		$thisErrorRemarksArrWithBullet= array();
		for($j=0;$j<count($thisErrorRemarksArr);$j++){
			
			$thisErrorRemarksArrWithBullet[] = '- ' . $thisErrorRemarksArr[$j];
		}
		
	
		if($thisErrorRemarksArr!=''){	
			$thisErrorDisplay = implode('<br />', $thisErrorRemarksArrWithBullet);
		}
		
		
		### Check if the field need to display in red color
		
		if (in_array($Lang['ReadingGarden']['Import_Error']['BookTitleIsMissing'], $thisErrorRemarksArr)){
			$thisBookName = '<font color="red">'.$thisBookName.'</font>';
		}	
		
		
		if (in_array($Lang['ReadingGarden']['Import_Error']['InvalidCallNumber'], $thisErrorRemarksArr)){
			$thisCallNumber = '<font color="red">'.$thisCallNumber.'</font>';
		}	
		
		if (in_array($Lang['ReadingGarden']['Import_Error']['InvalidISBN'], $thisErrorRemarksArr)){
			$thisISBN = '<font color="red">'.$thisISBN.'</font>';
		}	
		
		
		if (in_array($Lang['ReadingGarden']['Import_Error']['InvalidLanguage'], $thisErrorRemarksArr) || in_array($Lang['ReadingGarden']['Import_Error']['LanguageIsMissing'], $thisErrorRemarksArr)){
			$thisLanguage = '<font color="red">'.$thisLanguage.'</font>';
		}

		if (in_array($Lang['ReadingGarden']['Import_Error']['InvalidCategory'], $thisErrorRemarksArr) || in_array($Lang['ReadingGarden']['Import_Error']['CategoryIsMissing'], $thisErrorRemarksArr)){
			$thisCategory = '<font color="red">'.$thisCategory.'</font>';
		}
		
		if (in_array($Lang['ReadingGarden']['Import_Error']['InvalidForm'], $thisErrorRemarksArr)){
			$thisYearName = '<font color="red">'.$thisYearName.'</font>';
		}
		
		
	
		
		
		$thisErrorDisplay = '<font color="red">'.$thisErrorDisplay.'</font>';
		
		$css_i = ($i % 2) ? "2" : "";
		
		$ErrorTable .=  '<tr>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'" width="10">'.$thisRowNumber.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'"> '.$thisBookName.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisCallNumber.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisISBN.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisAuthor.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisPublisher.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisLanguage.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisCategory.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisDescription.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisYearName.'</td>';
		$ErrorTable .=  '<td class="tablebluerow'.$css_i.'">'.$thisDefaultAnswerSheet.'</td>';		
		$ErrorTable .= '<td class="tablebluerow'.$css_i.'">'. $thisErrorDisplay .'</td>';
		$ErrorTable .=  '</tr>';
	}
	
	$ErrorTable .= '</tbody>';
}
	
$ErrorTable .=  '</table>';
$ErrorTable = str_replace("\\", "\\\\", $ErrorTable);
$ErrorTable = str_replace("'", "\'", $ErrorTable);
	
$thisJSUpdate = '';
$thisJSUpdate .= '<script language="javascript">'."\n";
$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$ErrorTable.'\';';
$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCount.'\';';

if ($errorCount == 0) 
{
	$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
	$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
}
		
$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
$thisJSUpdate .= '</script>'."\n";

echo $thisJSUpdate;
	
intranet_closedb();
?>