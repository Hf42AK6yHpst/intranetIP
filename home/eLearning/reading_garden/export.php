<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();
$lexport = new libexporttext();

if(isset($ClassID)){
	$class_arr = $ReadingGardenLib->Get_Class_List($ClassID);	
}else{
	$class_arr = $ReadingGardenLib->Get_Class_List('');	
}

$StudentTargetArr = array();
$StudentTargetWithoutGradeArr = array();

$have_grade = true;

foreach($class_arr as $value){	
	if($have_grade){
		$current_target = $ReadingGardenUI->Get_Reading_Garden_Index_Teacher_View_Reading_Target_Import($value,$have_grade);
	}else{
		$current_target = $ReadingGardenUI->Get_Reading_Garden_Index_Teacher_View_Reading_Target_Import($value);
	}
	foreach($current_target as $target){		
		array_push($StudentTargetArr, $target);		
		unset($target[6]);
		array_push($StudentTargetWithoutGradeArr, $target);		
	}
}

if($have_grade){
	$exportColumn = array($Lang['General']['Class'],$Lang['General']['ClassNumber'],$Lang['Identity']['Student'],$Lang['ReadingGarden']['Progress'],$Lang['ReadingGarden']['ReadingRecord'],$Lang['ReadingGarden']['BookReport'],$Lang['ReadingGarden']['Score']);	
	$export_content = $lexport->GET_EXPORT_TXT($StudentTargetArr, $exportColumn);
}else{
	$exportColumn = array($Lang['General']['Class'],$Lang['General']['ClassNumber'],$Lang['Identity']['Student'],$Lang['ReadingGarden']['Progress'],$Lang['ReadingGarden']['ReadingRecord'],$Lang['ReadingGarden']['BookReport']);	
$export_content = $lexport->GET_EXPORT_TXT($StudentTargetWithoutGradeArr, $exportColumn);
}

$filename = 'export.csv';
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>