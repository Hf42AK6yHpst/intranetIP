<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$ReadingGardenLib->Update_Portal_Class_Session();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Announcement_List_DBTable_UI($ClassID, $AnnouncementType, $pageNo,$order,$field,$numPerPage, $Keyword);

?>
<script>
$().ready(function(){
	js_Select_Announcement_Type('<?=$AnnouncementType?>');
})

function js_Select_Announcement_Type(jsType)
{
	if($("#AnnouncementType").val()!=jsType)
	{
		$(".thumb_list_tab_on").removeClass("thumb_list_tab_on");
		$("#AnnouncementType").val(jsType);
		$("#AnnouncementTab"+jsType).addClass("thumb_list_tab_on");
	}
	js_Reload_Announcement_List()
}

function js_Reload_Announcement_List()
{
	
//	var AnnouncementType = $("#AnnouncementType").val();
//	var ClassID = $("#ClassID").val()?$("#ClassID").val():'';
//	$.post(
//		"ajax_reload.php",
//		{
//			Action:"ReloadAnnouncementArea",
//			AnnouncementType:AnnouncementType,
//			ClassID:ClassID
//		},
//		function(ReturnData){
//			$("div#AnnoucementListDiv").html(ReturnData);
//			UnBlock_Element("AnnoucementListDiv");
//		}
//	)
	
	var ClassID = $("#ClassID").val()?$("#ClassID").val():'';
	var AnnouncementType = $("#AnnouncementType").val();
	var Keyword = $("#Keyword").val();
	
	Block_Element("AnnoucementListDiv");
	var jsSubmitString = $("#form1").serialize();
	jsSubmitString += '&Action=ReloadAnnouncementArea&ClassID='+ClassID+'&AnnouncementType='+AnnouncementType+'&Keyword='+Keyword;
	
	Block_Element("BookListDBTable");
	$.ajax({  
		type: "POST",  
		url: "ajax_reload.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			$("div#AnnoucementListDiv").html(ReturnData);
			UnBlock_Element("AnnoucementListDiv");
		} 
	});
}

function sortPage(order,field,formObj)
{
	formObj.order.value = order;
	formObj.field.value = field;
	formObj.pageNo.value = 1;
	js_Reload_Announcement_List();
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>