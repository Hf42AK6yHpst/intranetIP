<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_recommend_book_list_page_size", "numPerPage");
$arrCookies[] = array("ck_recommend_book_list_page_number", "pageNo");
$arrCookies[] = array("ck_recommend_book_list_page_order", "order");
$arrCookies[] = array("ck_recommend_book_list_page_field", "field");	
$arrCookies[] = array("ck_recommend_book_list_page_keyword", "Keyword");
$arrCookies[] = array("ck_recommend_book_list_page_CategoryID", "CategoryID");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);
	
$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Student_Recommend_Report_List_UI($field,$order,$pageNo,$numPerPage);
?>
<script>

</script>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();
?>