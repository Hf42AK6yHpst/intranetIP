<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();

$arrCookies[] = array("ck_settings_assign_reading_page_ClassID", "YearClassID");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);
	
$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_AssignReadingSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));

$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_Edit_Assign_Reading($AssignedReadingID[0]);


?>
<script>
function js_Open_Select_Book_Layer()
{
	var YearID = $("#YearID").val();
//	$("div#AssignReadingLayerDiv").hide();
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadAssignReadingSelectBookLayer",
			YearID:YearID
		},
		function(ReturnData)
		{
			$("div#TB_ajaxContent").html(ReturnData);
			js_Reload_Book_Showcase();
		}
	);
}

function js_Reload_Book_Showcase()
{
	var YearID = $("#BookYearID").val();
	var CategoryID = $("#CategoryID").val();
	var Keyword = $("#BookKeyword").val();
	var BookID = $("#BookID").val();
	Block_Element("ItemShowcase");
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadBookShowCase",
			YearID:YearID,
			CategoryID:CategoryID,
			Keyword:Keyword,
			SelectedBookID:BookID
		},
		function(ReturnData)
		{
			$("div#ItemShowcase").html(ReturnData);
			$("div#SelectBookLayer").show();
			UnBlock_Element("ItemShowcase");
		}
	);
}

function js_Close_Select_Book_Layer()
{
	$("div#SelectBookLayer").hide().html("");
	$("div#AssignReadingLayerDiv").show();
}

function js_Select_Book(BookID)
{
	
	if($("#BookID").val() == BookID)
		return false;
		
	$("#BookID").val(BookID);
//	js_Close_Select_Book_Layer();
	tb_remove();
	js_Preview_Book_Info();
	js_Reload_Assigned_Class_Selection();
}

function js_Preview_Book_Info()
{
	var BookID = $("#BookID").val();
	js_Load_Book_Default_AnswerSheet(BookID);
	if(!BookID)
		return false;
	
	$("div#BookInfoDiv").html('<?=$ReadingGardenUI->Get_Ajax_Loading_Image()?>');	
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadBookInfo",
			BookID:BookID
		},
		function(ReturnData)
		{
			$("div#BookInfoDiv").html(ReturnData);
		}
	);
}

function js_Load_Book_Default_AnswerSheet(BookID)
{
		
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadDefaultAnswerSheet",
			BookID:BookID
		},
		function(ReturnData)
		{
			$("input#AnswerSheet").val("");
			$("#AnswerSheetID").val(ReturnData);
			if(ReturnData)
				$("input#UseAnswerSheet").attr("checked","checked");
			else
				$("input#UseAnswerSheet").attr("checked","");
			
			js_Enable_AnswerSheet()
		}
	);
	
}

function js_Reload_Assigned_Class_Selection()
{
	var BookID = $("#BookID").val();
	if($("#YearClassIDArr\\[\\]").val())
		var YearClassID = $("#YearClassIDArr\\[\\]").val().toString();
	var AssignedReadingID = $("#AssignedReadingID").val();
	
	$("span#AssignedClassSelectionSpan").html('<?=$ReadingGardenUI->Get_Ajax_Loading_Image()?>');
	$.post(
		"../../ajax_reload.php",
		{
			Action:"ReloadAssignedReadingClassSelection",
			YearClassIDArr:YearClassID,
			BookID:BookID,
			SelectionID:"YearClassIDArr[]",
			isAll:0,
			noFirst:1,
			isMultiple:1,
			AssignedReadingID:AssignedReadingID
		},
		function(ReturnData)
		{
			$("span#AssignedClassSelectionSpan").html(ReturnData);
//			js_Reload_Assign_Reading_Table()
		}
	);
}
//
//function js_Add_Attachment()
//{
//	for(AttachmentNum=0; $(".AttRow"+AttachmentNum).length>=1;AttachmentNum++);
//	
//	var x ='';
//	x += '<tr class="AttRow'+AttachmentNum+'">'+"\n";
//		x += '<td class="AttachmentFileTD">'+"\n";
//				x += '<input type="file" name="Attachment[]" class="Attachment" >'+"\n";
//			x += '[<a href="javascript:void(0);" onclick="js_Remove_Attachment_Row('+AttachmentNum+')"><?=$Lang['Btn']['Delete']?></a>]'+"\n";
//		x += '</td>'+"\n";
//	x += '</tr>'+"\n";
//	
//	$("#AddAttachmentRow").before(x);
//	$("#AttachmentTitleTD").attr("rowspan",$(".Attachment").length+1);
//}

function js_Remove_Attachment()
{
//	if($(".Attachment").length==1)
//	{
//		js_Add_Attachment();
//		js_Remove_Attachment_Row(RowID);
//	}
//	else if($(".AttRow"+RowID).find("#AttachmentTitleTD"))
//	{
//		$(".AttachmentFileTD:eq(1)").before($(".AttRow"+RowID).find("#AttachmentTitleTD"))
//		$(".AttRow"+RowID).remove();
//		$("#AttachmentTitleTD").attr("rowspan",$(".Attachment").length+1);
//	}
	$("#AttachmentSpan").html('<input type="file" name="Attachment[]" class="Attachment" >'+"\n");
}
//
//var isAddMore = '';
//function aj_Save_Form(AddMore)
//{
//	var isEdit = $("#isEdit").val();
//	if(isEdit==1 && !confirm("<?=$Lang['ReadingGarden']['ConfirmArr']['AssignedReadingOfAllClassWillBeUpdate']?>"))
//		return false;
//	
//	Block_Thickbox();
//	isAddMore = AddMore;
//	var obj = document.TBForm;
//	obj.action = "ajax_save_assign_reading.php";
//	obj.target = "FileUploadFrame";
//	obj.encoding = "multipart/form-data";
//	obj.method = 'post';
//	obj.submit();	
//	Restore_Form(obj)
//}

function Restore_Form(formObj)
{
	formObj.target = '_self';
	formObj.action = 'assign_reading_update.php';
	formObj.method = 'post';
}

function CheckTBForm(AddMore)
{
	var FormValid = true;
	$("div.WarnMsgDiv").hide();
	$("input.Mandatory").each(function(){
		
		if($(this).val().Trim()=='')
		{
			FormValid = false;
			var WarnDivClass = "WarnBlank"+$(this).attr("id");
			
			$("#"+WarnDivClass).show();
		}
	})
	
	if($("#UseAnswerSheet").attr("checked") && ($("#form1").find("#AnswerSheet").val().Trim()=='' && $("#form1").find("#AnswerSheetID").val().Trim()==''))
	{
		FormValid = false;
		$("#WarnBlankAnswerSheet").show();
	}
	
	if($("#OnlineWriting").attr("checked") )
	{
		var UpperLimit = $("form#form1").find("input#WritingLimit").val().Trim();
		var LowerLimit = $("form#form1").find("input#LowerLimit").val().Trim();

		if(UpperLimit!='') UpperLimit = parseInt(UpperLimit);
		if(LowerLimit!='') LowerLimit = parseInt(LowerLimit);

		if(UpperLimit !='' && !is_positive_int(UpperLimit))
		{
			FormValid = false;
			$("div#WarnNonIntegerWritingLimit").show();
		}

		if(LowerLimit !='' && !is_positive_int(LowerLimit))
		{
			FormValid = false;
			$("div#WarnNonIntegerWritingLimit").show();
		}
		
		if(UpperLimit !='' && LowerLimit !='' && UpperLimit<LowerLimit)
		{
			FormValid = false;
			$("div#WarnInvalidRange").show();
		}
	} 
	
	return FormValid; 	 
} 

// AnswerSheet
function js_Edit_AnswerSheet(Preview)
{
	newWindow('',10)
	var obj = document.form1;
	obj.isPreview.value = Preview;
	obj.action = "answersheet.php";
	obj.target = "intranet_popup10";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function js_View_AnswerSheet()
{
	js_Edit_AnswerSheet(1)
}

function js_Delete_AnswerSheet()
{
	if(!confirm("<?=$Lang['ReadingGarden']['ConfirmArr']['TheModificationOfTheAnswerSheetWillBeDeleted']?>")) return false;

	$("input#AnswerSheet").val("");
	js_Toggle_AnswerSheet_Selection();
}

function js_Enable_AnswerSheet()
{
	if($("input#UseAnswerSheet").attr("checked"))
	{
		$("div#AnswerSheetBtn").show()
	}
	else
	{
		$("div#AnswerSheetBtn").hide()
	}
		
	js_Toggle_AnswerSheet_Selection();
}

function js_Toggle_AnswerSheet_Selection()
{
	if($("input#AnswerSheet").val().Trim()!='')
	{
		$("div#SelectAnswerSheet").hide();
		$("span#DeleteAnswerSheetBtn").show();
	}
	else
	{
		$("div#SelectAnswerSheet").show();
		$("span#DeleteAnswerSheetBtn").hide();
	}
}

function js_Toggle_WordLimit()
{
	if($("input#OnlineWriting").attr("checked"))
		$("div#WordLimitDiv").show()
	else
		$("div#WordLimitDiv").hide()
}

function js_Toggle_Attachment()
{
	if($("input#BookReportRequired").attr("checked"))
		$("div#AttachmentDiv").show()
	else
		$("div#AttachmentDiv").hide()
}

function js_Preview_AnswerSheet(AnswerSheet, AssignedReadingID)
{
	newWindow('',10)
	var obj = document.form1;
	obj.AnswerSheet.value=AnswerSheet;
	obj.action = "answersheet.php?ViewOnly=1&AssignedReadingID="+AssignedReadingID;
	obj.target = "intranet_popup10";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function js_Select_All_Year_Class()
{
	js_Select_All_With_OptGroup("YearClassIDArr[]");
}

$().ready(function(){
	if("<?=$AssignedReadingID?>"=="")
		js_Select_All_Year_Class();
	
	js_Toggle_WordLimit();
	js_Enable_AnswerSheet();
	js_Toggle_Attachment();
		
})
</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>