<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

//if(!empty($Attachment) || count($old_Attachment))
//{
//	$Path = $ReadingGardenLib->Settings_Assign_Reading_Upload_Attachment($AssignedReadingID, $Attachment, $Attachment_name, $TempPath, $old_Attachment);
//}

//if($Path || !empty($Attachment))
//{
	$AssignReadingInfo['BookID'] = $BookID;
	$AssignReadingInfo['AssignedReadingName'] = trim(stripslashes($AssignedReadingName));
	$AssignReadingInfo['Description'] = trim(stripslashes($Description));
	if($UseAnswerSheet==1)
		$AssignReadingInfo['AnswerSheet'] = stripslashes($AnswerSheet);
	else
		$AssignReadingInfo['AnswerSheet'] = '';
//	$AssignReadingInfo['Attachment'] = $Path;
	$AssignReadingInfo['BookReportRequired'] = $BookReportRequired;
	
	$ReadingGardenLib->Start_Trans();
	if(empty($AssignedReadingID))
	{
		
		$Success["Create"] = $ReadingGardenLib->Settings_Create_Assign_Reading($AssignReadingInfo);
		$AssignedReadingID = mysql_insert_id();
//		$ParArr['AssignedReadingID'] = $AssignedReadingID;
//		$ParArr['ClassID'] = $YearClassIDArr;
		$Success["Assign"] = $ReadingGardenLib->Settings_Update_Class_Assign_Reading($AssignedReadingID,$YearClassIDArr);
		
		if(!in_array(false,$Success))
		{
			$ReadingGardenLib->Commit_Trans();
			$Msg = "AddSuccess";
		}
		else
		{
			$ReadingGardenLib->RollBack_Trans();
			$Msg = "AddUnsuccess";
		}
	}
	else
	{
		$Success["Update"] = $ReadingGardenLib->Settings_Update_Assign_Reading($AssignedReadingID, $AssignReadingInfo);
		$Success["Assign"] = $ReadingGardenLib->Settings_Update_Class_Assign_Reading($AssignedReadingID,$YearClassIDArr);

		if(!in_array(false,$Success))
		{
			$ReadingGardenLib->Commit_Trans();
			$Msg = "UpdateSuccess";
		}
		else
		{
			$ReadingGardenLib->RollBack_Trans();
			$Msg = "UpdateUnsuccess";
		}
	}
//}

intranet_closedb();
?>
<script>
window.top.js_Save_Complete("<?=$ReadingGardenUI->Get_Return_Message($Msg)?>");
</script>
