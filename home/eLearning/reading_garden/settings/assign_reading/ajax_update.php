<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "AddAssignReadingMapping":
		$ParArr['ClassID'] = $_REQUEST['ClassID'];
		$ParArr['AssignedReadingID'] = $_REQUEST['AssignedReadingID'];
		
		if($ReadingGardenLib->Settings_Assign_Reading_To_Class($ParArr))
			echo $ReadingGardenUI->Get_Return_Message("AddSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("AddUnsuccess");
	break; 
	
	case "DeleteAssignReadingMapping":
//		$AssignedReadingTargetID = $_REQUEST['AssignedReadingTargetID'];
		$AssignedReadingID = $_REQUEST['AssignedReadingID'];
		$YearClassID = $_REQUEST['YearClassID'];
		$YearClassID = $ReadingGardenLib->Get_Class_List_From_Form_Class_Option($YearClassID);

		if ($ReadingGardenLib->Settings_Remove_Assign_Reading_From_Class('',$AssignedReadingID,$YearClassID))
			echo $ReadingGardenUI->Get_Return_Message("UnassignClassSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UnassignClassFail");
	break;

	case "UpdateAssignReadingOrder":
		$AssignedReadingIDArr = $ClassAssignedReadingID;
		$YearClassID = $_REQUEST['YearClassID'];
		$YearClassID = $ReadingGardenLib->Get_Class_List_From_Form_Class_Option($YearClassID);
		
		$Success = $ReadingGardenLib->Settings_Update_Assign_Reading_Display_Order($AssignedReadingIDArr,$YearClassID);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnsuccess");
	break; 
	case "DeleteAssignReading":
		$AssignedReadingID = $_REQUEST['AssignedReadingID'];
		$AssignedReadingIDArr = explode(",",$AssignedReadingID);
		
		$Success = $ReadingGardenLib->Settings_Delete_Assign_Reading($AssignedReadingIDArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("DeleteSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("DeleteUnsuccess");	
	break;
}	

intranet_closedb();
?>