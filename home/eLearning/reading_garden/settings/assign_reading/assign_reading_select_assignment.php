<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_AssignReadingSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));

$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_Assign_Reading_To_Class_Step2_Layer($ClassID);

?>
<script>
function js_Back_To_Main()
{
	window.location = "assign_reading.php";
}

function js_Check_All_Assign_Reading(obj)
{
	$(obj.form).find(".AssignedReadingID").attr("checked",obj.checked);
}

function Check_Book(obj,BookID)
{
	if(obj.checked)
	{
		$(".Book_"+BookID).not(obj).attr("disabled","disabled")
	}
	else
	{
		$(".Book_"+BookID).attr("disabled","")
	}
}

function js_Preview_AnswerSheet(AnswerSheet, AssignedReadingID)
{
	newWindow('',10)
	var obj = document.form1;
	obj.AnswerSheet.value=AnswerSheet;
	obj.action = "answersheet.php?ViewOnly=1&AssignedReadingID="+AssignedReadingID;
	obj.target = "intranet_popup10";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function Restore_Form(formObj)
{
	formObj.target = '_self';
	formObj.action = '';
	formObj.method = 'post';
}

function CheckForm()
{
	$(".WarnMsg").hide();
	if( $(".AssignedReadingID:checked").length <= 0)
	{
		$("#WarnBlankAssignedReadingID").show();
		return false;
	}
	
	return true;

}

</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>