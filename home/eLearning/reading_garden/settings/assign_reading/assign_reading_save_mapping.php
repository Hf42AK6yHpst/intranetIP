<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
//include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
//$ReadingGardenUI = new libreadinggarden_ui();

$ClassAssignReading = unserialize(urldecode($AssignReadingData));

$ReadingGardenLib->Start_Trans();

foreach($ClassAssignReading as $ClassID => $AssignedReadingIDArr)
{
	$ParArr['ClassID'] = $ClassID;
	$ParArr['AssignedReadingID'] = $AssignedReadingIDArr; 
	
	$Succuess[] = $ReadingGardenLib->Settings_Assign_Reading_To_Class($ParArr);	
}

if(in_array(false,$Succuess))
{
	$ReadingGardenLib->RollBack_Trans();
	$msg = "AssignToClassFail";
}
else
{
	$ReadingGardenLib->Commit_Trans();
	$msg = "AssignToClassSuccess";
}	
header("location: assign_reading.php?Msg=$msg");

intranet_closedb();
?>