<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_AssignReadingSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));

$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_Assign_Reading_To_Class_Step1_Layer($YearClassID);

?>
<script>

function CheckForm()
{
	$(".WarnMsg").hide();
	if( $("#ClassID\\[\\]").val() == null)
	{
		$("#WarnBlankClassID").show();
		return false;
	}
	
	return true;

}

function js_Back_To_Main()
{
	window.location = "assign_reading.php";
}


</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>