<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

if($qStr)
	$AnswerSheet = $qStr;
else if($AnswerSheet)
	$AnswerSheet = $AnswerSheet;
else
{
	$TemplateInfo = $ReadingGardenLib->Get_Answer_Sheet_Template($AnswerSheetID);
	$AnswerSheet = addslashes($TemplateInfo[0]['AnswerSheet']);
}

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

if($AssignedReadingID)
{
	echo $ReadingGardenUI->Get_Answer_Sheet_Preview_Assigned_Reading_Info_Table($AssignedReadingID);
	if($ViewOnly == 1)
	{
		$AssignReadingInfo = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingID);
		$AnswerSheet = addslashes($AssignReadingInfo[0]['AnswerSheet']);
	}
}

if($isPreview == 1 || $ViewOnly == 1)
	echo $ReadingGardenUI->Get_Answer_Sheet_Preview_UI($AnswerSheet,$ViewOnly);
else
	echo $ReadingGardenUI->Get_Answer_Sheet_Edit_UI($AnswerSheet);

?>
<script>
function js_Edit()
{
	var obj = document.ansForm;
	obj.isPreview.value = 0;
	obj.submit();
}

function js_Save()
{
	var qStr = $("#qStr").val();
	opener.window.document.form1.AnswerSheet.value = qStr;
	opener.window.js_Toggle_AnswerSheet_Selection();
	var obj = document.ansForm;
	obj.isPreview.value = 1;
	
	obj.submit();
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>