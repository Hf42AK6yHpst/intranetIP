<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

foreach($WinnerID as $thisRecord)
{
	list($thisWinnerID, $thisAwardLevelID) = explode(",",$thisRecord);
	$WinnerAwardArr[$thisWinnerID][] = $thisAwardLevelID;
}

//$AwardList = unserialize($AwardList);
$Success = $ReadingGardenLib->Settings_Manage_Award_Scheme_Winner($action, $AwardSchemeID, $WinnerAwardArr);

if($action=="assign")
{
	if($Success)
		$msg = "AssignSuccess";
	else
		$msg = "AssignFail";
}
else
{
	if($Success)
		$msg = "UnassignSuccess";
	else
		$msg = "UnassignFail";
}

header("location: ./generate_winner.php?AwardSchemeID=".$AwardSchemeID."&Msg=".$msg);

intranet_closedb();
?>