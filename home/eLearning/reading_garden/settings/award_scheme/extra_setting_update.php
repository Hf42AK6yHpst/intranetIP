<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$Setting['RequireSelectAwardScheme'] = $RequireSelectAwardScheme;
$Success = $ReadingGardenLib->Settings_Update_Settings($Setting);

if($Success)
{
	$msg = "SettingSaveSuccess"; 
}
else
{
	$msg = "SettingSaveFail";
}
header("location: ./extra_setting.php?Msg=$msg");

intranet_closedb();
?>