<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_generate_winner_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_generate_winner_page_number", "pageNo");
$arrCookies[] = array("ck_settings_generate_winner_page_order", "order");
$arrCookies[] = array("ck_settings_generate_winner_page_field", "field");
$arrCookies[] = array("ck_settings_generate_winner_winner_status", "WinnerStatus");	

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

$linterface = new interface_html();
$ReadingGardenUI = new libreadinggarden_ui();
$ReadingGardenLib = new libreadinggarden();

$CurrentPage = "Settings_AwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AwardScheme'], "award_scheme.php", 1);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['ExtraSetting'], "extra_setting.php", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$linterface->LAYOUT_START($ReturnMessage);

if(is_array($AwardSchemeID)) $AwardSchemeID = $AwardSchemeID[0];

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Settings_Award_Scheme_Generate_Winner_UI($AwardSchemeID,$WinnerStatus, $field, $order, $pageNo, $numPerPage, $AwardLevelID);

?>
<script language="javascript">

</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>