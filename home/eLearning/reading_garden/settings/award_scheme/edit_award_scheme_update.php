<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
//include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.carlos.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$AwardSchemeID = trim($_REQUEST['AwardSchemeID']);
if($AwardSchemeID=='')
{
	// Add Award
		$DataArr['AwardName'] = trim(urldecode(stripslashes($_REQUEST['AwardName'])));
		$DataArr['Target'] = trim(urldecode(stripslashes($_REQUEST['AwardTarget'])));
		$DataArr['AwardType'] = $_REQUEST['AwardType'];
		$DataArr['Language'] = $_REQUEST['AwardLanguage'];
		$DataArr['StartDate'] = trim($_REQUEST['StartDate']);
		$DataArr['EndDate'] = trim($_REQUEST['EndDate']);
		$DataArr['ResultPublishDate'] = trim($_REQUEST['ResultPublishDate']);
		//$DataArr['MinBookRead'] = trim($_REQUEST['MinBookRead']);
		//$DataArr['MinReportSubmit'] = trim($_REQUEST['MinReportSubmit']);
		$DataArr['TargetClassItem'] = $_REQUEST['ClassID'];
		$DataArr['Description'] = trim(urldecode(stripslashes($_REQUEST['Description'])));
		$DataArr['Attachment'] = GetCommonAttachmentFolderPath($AttachmentArr, $FolderPath);
		//$DataArr['RequirementItemCategory'] = $_REQUEST['RequirementItemCategory'];
		//$DataArr['RequirementItemQuantity'] = $_REQUEST['RequirementItemQuantity'];
		
		$entry_num_list = $_REQUEST['AwardLevelEntryNumber'];
		$LevelEntries = array();
		
		$level=1;
		for($i=0;$i<sizeof($entry_num_list);$i++){
			$entry_num = $entry_num_list[$i];
			
//			$LevelEntries[$i]['Level'] = ${"Level$entry_num"};
			$LevelEntries[$i]['Level'] = $level;
			$LevelEntries[$i]['LevelName'] = ${"LevelName$entry_num"};
			$LevelEntries[$i]['MinBookRead'] = ${"MinBookRead$entry_num"};
			$LevelEntries[$i]['MinReportSubmit'] = ${"MinReportSubmit$entry_num"};
			//$LevelEntries[$i]['RequirementID'] = ${"RequirementID$entry_num"};
			$LevelEntries[$i]['RequirementItemCategory'] = ${"RequirementItemCategory$entry_num"};
			$LevelEntries[$i]['RequirementItemQuantity'] = ${"RequirementItemQuantity$entry_num"};
			$LevelEntries[$i]['AwardType'] = ${"AwardType$entry_num"};
			
			$level++;
		}
		
		$DataArr['Level'] = $LevelEntries;
		$ReadingGardenLib->Start_Trans();
		$Result = $ReadingGardenLib->Add_Award_Scheme($DataArr);
		if($Result){
			$ReadingGardenLib->Commit_Trans();
			$Msg = "AddSuccess";
		}else{
			$ReadingGardenLib->Rollback_Trans();
			$Msg = "AddUnsuccess";
		}
}else{
	//Update Award 
		$DataArr['AwardSchemeID'] = $_REQUEST['AwardSchemeID'];
		$DataArr['AwardName'] = trim(urldecode(stripslashes($_REQUEST['AwardName'])));
		$DataArr['Target'] = trim(urldecode(stripslashes($_REQUEST['AwardTarget'])));
		$DataArr['AwardType'] = $_REQUEST['AwardType'];
		$DataArr['Language'] = $_REQUEST['AwardLanguage'];
		$DataArr['StartDate'] = trim($_REQUEST['StartDate']);
		$DataArr['EndDate'] = trim($_REQUEST['EndDate']);
		$DataArr['ResultPublishDate'] = trim($_REQUEST['ResultPublishDate']);
		//$DataArr['MinBookRead'] = trim($_REQUEST['MinBookRead']);
		//$DataArr['MinReportSubmit'] = trim($_REQUEST['MinReportSubmit']);
		$DataArr['TargetClassItem'] = $_REQUEST['ClassID'];
		//$DataArr['RequirementItemCategory'] = $_REQUEST['RequirementItemCategory'];
		//$DataArr['RequirementItemQuantity'] = $_REQUEST['RequirementItemQuantity'];
		$DataArr['WinnerGenerated'] = $_REQUEST['WinnerGenerated'];
		$DataArr['Description'] = trim(urldecode(stripslashes($_REQUEST['Description'])));
		$DataArr['Attachment'] = GetCommonAttachmentFolderPath($AttachmentArr, $FolderPath);
		
		$entry_num_list = $_REQUEST['AwardLevelEntryNumber'];
		$LevelEntries = array();
		
		$level=1;
		for($i=0;$i<sizeof($entry_num_list);$i++){
			$entry_num = $entry_num_list[$i];
			
			$LevelEntries[$i]['AwardLevelID'] = ${"AwardLevelID$entry_num"};
//			$LevelEntries[$i]['Level'] = ${"Level$entry_num"};
			$LevelEntries[$i]['Level'] = $level;
			$LevelEntries[$i]['LevelName'] = ${"LevelName$entry_num"};
			$LevelEntries[$i]['MinBookRead'] = ${"MinBookRead$entry_num"};
			$LevelEntries[$i]['MinReportSubmit'] = ${"MinReportSubmit$entry_num"};
			//$LevelEntries[$i]['RequirementID'] = ${"RequirementID$entry_num"};
			$LevelEntries[$i]['RequirementItemCategory'] = ${"RequirementItemCategory$entry_num"};
			$LevelEntries[$i]['RequirementItemQuantity'] = ${"RequirementItemQuantity$entry_num"};
			$LevelEntries[$i]['AwardType'] = ${"AwardType$entry_num"};
			
			$level++;
		}
		
		$DataArr['Level'] = $LevelEntries;
		$ReadingGardenLib->Start_Trans();
		$Result = $ReadingGardenLib->Update_Award_Scheme($DataArr);
		if($Result){
			$ReadingGardenLib->Commit_Trans();
			$Msg = "UpdateSuccess";
		}else{
			$ReadingGardenLib->Rollback_Trans();
			$Msg = "UpdateUnsuccess";
		}
}

intranet_closedb();
header("Location: ./award_scheme.php?Msg=$Msg");
?>