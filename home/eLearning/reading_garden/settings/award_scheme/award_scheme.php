<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenUI = new libreadinggarden_ui();
$ReadingGardenLib = new libreadinggarden();

$CurrentPage = "Settings_AwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AwardScheme'], "", 1);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['ExtraSetting'], "extra_setting.php", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$linterface->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Settings_Award_Scheme_Index();

?>
<script language="javascript">
function Get_Multiple_Selection_Values(name)
{
	var opts = $('select[name='+name+'] option:selected');
	var returnArray = new Array();
	opts.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Get_Element_Values(name)
{
	var elems = $('input[name='+name+']');
	var returnArray = new Array();
	elems.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Get_Checked_Checkbox(name)
{
	var elems = $('input[name='+name+']:checked');
	var returnArray = new Array();
	elems.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Check_Positive_Int(n)
{
	var value = parseInt(n);
	var result = /^\d+$/.test(n);
	if (isNaN(value) || value < 0 || result==false)
		return false;
	else
		return true;
}

function Check_All_Award(checked)
{
	$('input[name="AwardSchemeID\\[\\]"]').attr('checked',checked);
}

function Get_Award_Scheme_Table()
{
	Block_Element('DivAwardSchemeTable');
	$.post(
		"ajax_load.php",
		{
			"Action":"GetAwardSchemeTable",
			"Keyword":encodeURIComponent($('input#Keyword').val()),
			"AwardLanguage":$('#Language').val()
		},
		function(ReturnData){
			$("#DivAwardSchemeTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("DivAwardSchemeTable");
			Scroll_To_Top();
		}
	);
}

function Get_Award_Edit_Form(is_new, award_scheme_id)
{
	if(!award_scheme_id){
		award_scheme_id = $('input[name="AwardSchemeID\\[\\]"]:checked').val();
	}
	/*
	tb_show("<?=$Lang['Btn']['Edit']?>","#TB_inline?height=600&width=750&inlineId=FakeLayer");
	$.post(
		"ajax_load.php",
		{
			"Action":"GetAwardEditForm",
			"IsNew":is_new,
			"AwardSchemeID":award_scheme_id 
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
		}
	);
	*/
	window.location = "./edit_award_scheme.php?AwardSchemeID=" + award_scheme_id;
}

function Add_Requirement_Item()
{
	var container = $('div#DivRequirement');
	var category = $('#CategoryID option:selected');
	var cat_val = category.val();
	var cat_text = category.text();
	var exist_cat_items = $('input[name=RequirementItemCategory\\[\\]]');
	var is_cat_exist = false;
	exist_cat_items.each(
		function(index){
			if($(this).val()==cat_val)
				is_cat_exist = true;
		}
	);
	if(is_cat_exist){
		$('div#CategoryWarningLayer').html('<?=$Lang['ReadingGarden']['WarningMsg']['CategoryAlreadyAdded']?>');
		$('div#CategoryWarningLayer').css('display','');
		return;
	}else{
		$('div#CategoryWarningLayer').html('');
		$('div#CategoryWarningLayer').css('display','none');
	}
	var item_cat = htmlspecialchars(cat_text)+'<input type="hidden" name="RequirementItemCategory[]" value="'+cat_val+'"/>';
	var item_quantity = '<input name="RequirementItemQuantity[]" type="text" maxlength="10" size="10" value="1"/>';
	//var item_delete_btn = '<span class="table_row_tool row_content_tool"><a title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" href="#" onclick="$(this).parent().parent().remove();return false;"></a><span>';
	var item_delete_btn = '<a href="javascript:void(0);" onclick="$(this).parent().remove();"><img border="0" src="<?=$image_path."/".$LAYOUT_SKIN."/icon_delete.gif"?>" title="<?=$Lang['Btn']['Delete']?>" /></a>';
	container.append('<div>'+item_cat + '&nbsp;' + item_quantity + '&nbsp;' + item_delete_btn + '&nbsp;</div>');
}
/*
function Add_TargetClass_Item()
{
	var container = $('div#DivTargetClass');
	var classOpt = $('#ClassID option:selected');
	var class_val = classOpt.val();
	var class_text = classOpt.text();
	var exist_class_items = $('input[name=TargetClassItem\\[\\]]');
	var is_class_exist = false;
	exist_class_items.each(
		function(index){
			if($(this).val()==class_val)
				is_class_exist = true;
		}
	);
	if(is_class_exist){
		$('div#TargetClassWarningLayer').html('<?=$Lang['ReadingGarden']['WarningMsg']['ClassAlreadyAdded']?>');
		$('div#TargetClassWarningLayer').css('display','');
		return;
	}else{
		$('div#TargetClassWarningLayer').html('');
		$('div#TargetClassWarningLayer').css('display','none');
	}
	var item_class = htmlspecialchars(class_text)+'<input type="hidden" name="TargetClassItem[]" value="'+class_val+'"/>';
	//var item_delete_btn = '<span class="table_row_tool row_content_tool"><a title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" href="#" onclick="$(this).parent().parent().remove();return false;"></a><span>';
	var item_delete_btn = '<a href="javascript:void(0);" onclick="$(this).parent().remove();"><img border="0" src="<?=$image_path."/".$LAYOUT_SKIN."/icon_delete.gif"?>" title="<?=$Lang['Btn']['Delete']?>" /></a>';
	container.append('<div>'+item_class + '&nbsp;' + item_delete_btn + '&nbsp;</div>');
}
*/
function Check_Award(issubmit)
{
	// data validation
	var submit_btn = $('input#submit_btn');
	var error_cnt = 0;
	var error_focus = null;
	var check_name = Check_Award_Name(0);
	if(check_name==false){
		error_cnt+=1;
		error_focus = $('input#AwardName');
	}
	var award_type = $('#AwardType').val();
	var target_class = Get_Multiple_Selection_Values('ClassID[]');
	var TargetClassWarningLayer = $('div#TargetClassWarningDiv');
	if(award_type==2 && target_class.length<=1){// inter-class
		TargetClassWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastTwoClasses']?>');
		TargetClassWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('select[name="ClassID\\[\\]"]');
	}else if(award_type==1 && target_class.length==0){// individual
		TargetClassWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneClasses']?>');
		TargetClassWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('select[name="ClassID\\[\\]"]');
	}else{
		TargetClassWarningLayer.html('');
		TargetClassWarningLayer.css('display','none');
	}
	
	var start_date = $('input#StartDate').val();
	var end_date = $('input#EndDate').val();
	var start_date_obj = document.getElementById('StartDate');
	var end_date_obj = document.getElementById('EndDate');
	var DatePeriodWarningLayer = $('div#DatePeriodWarningDiv');
	if(!check_date_without_return_msg(start_date_obj) || !check_date_without_return_msg(end_date_obj)){
		DatePeriodWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['InvalidDateFormat']?>');
		DatePeriodWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#StartDate');
	}else{
		DatePeriodWarningLayer.html('');
		DatePeriodWarningLayer.css('display','none');
	}
	
	if(start_date > end_date){
		DatePeriodWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['InvalidDateRange']?>');
		DatePeriodWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#StartDate');
	}else{
		DatePeriodWarningLayer.html('');
		DatePeriodWarningLayer.css('display','none');
	}
	
	var publish_date = $('input#ResultPublishDate').val();
	var publish_date_obj = document.getElementById('ResultPublishDate');
	var ResultPublishDateWarningLayer = $('div#ResultPublishDateWarningDiv');
	if(publish_date<end_date)
	{
		ResultPublishDateWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['InvalidPublishDate']?>');
		ResultPublishDateWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#ResultPublishDate');
	}else{
		ResultPublishDateWarningLayer.html('');
		ResultPublishDateWarningLayer.css('display','none');
	}
	
	var min_book = $.trim($('input#MinBookRead').val());
	var min_report = $.trim($('input#MinReportSubmit').val());
	var MinBookWarningLayer = $('div#MinBookWarningDiv');
	var MinReportWarningLayer = $('div#MinReportWarningDiv');
	if(min_book != '' && !Check_Positive_Int(min_book)){
		MinBookWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#MinBookRead');
	}else{
		MinBookWarningLayer.css('display','none');
	}
	if(min_report != '' && !Check_Positive_Int(min_report)){
		MinReportWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#MinReportSubmit');
	}else{
		MinReportWarningLayer.css('display','none');
	}
	
	var CategoryWarningLayer = $('div#CategoryWarningLayer');
	var RequireItem = $('input[name=RequirementItemQuantity\\[\\]]');
	var quantity_valid = true;
	RequireItem.each(
		function(index){
			var amount = $(this).val();
			if(!Check_Positive_Int(amount)){
				quantity_valid = false;
			}
		}
	);
	if(!quantity_valid){
		CategoryWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger']?>');
		CategoryWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('#CategoryID');
	}else{
		CategoryWarningLayer.html('');
		CategoryWarningLayer.css('display','none');
	}
	
	if(error_cnt>0){
		if(error_focus != null) error_focus.focus();
		return false;
	}
	var isnew = $('input#IsNew').val();
	if(issubmit==1){
		if(isnew==1)
			Add_Award();
		else
		{
			if($("input#WinnerGenerated").val()!=1 || confirm("<?=$Lang['ReadingGarden']['ConfirmArr']['WinnerListWasGenerated']?>"))
			{
				Update_Award();
			}
		}
	}
}

function Check_Award_Name(ajaxmode)
{
	var award_name = $.trim($('input#AwardName').val());
	var award_name_warning_layer = $('div#AwardNameWarningDiv');
	var old_award_name = $('input#OldAwardName').val();
	var submit_btn = $('input#submit_btn');
	if(award_name==''){
		award_name_warning_layer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestInputAwardName']?>');
		award_name_warning_layer.css('display','');
		submit_btn.attr('disabled',true);
		return false;
	}else{
		award_name_warning_layer.html('');
		award_name_warning_layer.css('display','none');
		submit_btn.attr('disabled',false);
	}
	if(award_name==old_award_name) return true;
	
	if(ajaxmode==1){
		$.post(
			"ajax_update.php",
			{
				"Action":"CheckAwardName",
				"AwardName":encodeURIComponent(award_name),
				"OldAwardName":encodeURIComponent(old_award_name) 
			},
			function(ReturnData){
				if(ReturnData=="1"){
					award_name_warning_layer.html('');
					award_name_warning_layer.css('display','none');
					submit_btn.attr('disabled',false);
				}else{
					award_name_warning_layer.html('<?=$Lang['ReadingGarden']['WarningMsg']['AwardNameExist']?>');
					award_name_warning_layer.css('display','');
					submit_btn.attr('disabled',true);
				}
			}
		);
	}
}

function Add_Award()
{
	Block_Thickbox();
	var award_lang = '';
	if($('#AwardLanguageChi').attr('checked')==true)
		award_lang = $('#AwardLanguageChi').val();
	else if($('#AwardLanguageEng').attr('checked')==true)
		award_lang = $('#AwardLanguageEng').val();
	else
		award_lang = $('#AwardLanguageOthers').val();
	$.post(
		"ajax_update.php",
		{
			"Action":"AddAward",
			"AwardName":encodeURIComponent($('input#AwardName').val()),
			"AwardTarget":encodeURIComponent($('input#AwardTarget').val()),
			"AwardType":$('#AwardType').val(),
			"AwardLanguage":award_lang,
			"StartDate":$('input#StartDate').val(),
			"EndDate":$('input#EndDate').val(),
			"ResultPublishDate":$('input#ResultPublishDate').val(),
			"MinBookRead":$('input#MinBookRead').val(),
			"MinReportSubmit":$('input#MinReportSubmit').val(),
			"TargetClassItem[]":Get_Multiple_Selection_Values('ClassID[]'),
			"RequirementItemCategory[]":Get_Element_Values('RequirementItemCategory[]'),
			"RequirementItemQuantity[]":Get_Element_Values('RequirementItemQuantity[]')
		},
		function(ReturnData){
			if(ReturnData=="1")
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['AddAwardSchemeSuccess']?>');
			else
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['AddAwardSchemeFail']?>');
			Get_Award_Scheme_Table();
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Update_Award()
{
	Block_Thickbox();
	var award_lang = '';
	if($('#AwardLanguageChi').attr('checked')==true)
		award_lang = $('#AwardLanguageChi').val();
	else if($('#AwardLanguageEng').attr('checked')==true)
		award_lang = $('#AwardLanguageEng').val();
	else
		award_lang = $('#AwardLanguageOthers').val();
	$.post(
		"ajax_update.php",
		{
			"Action":"UpdateAward",
			"AwardSchemeID":$('input#AwardSchemeID').val(),
			"AwardName":encodeURIComponent($('input#AwardName').val()),
			"AwardTarget":encodeURIComponent($('input#AwardTarget').val()),
			"AwardType":$('#AwardType').val(),
			"AwardLanguage":award_lang,
			"StartDate":$('input#StartDate').val(),
			"EndDate":$('input#EndDate').val(),
			"ResultPublishDate":$('input#ResultPublishDate').val(),
			"MinBookRead":$('input#MinBookRead').val(),
			"MinReportSubmit":$('input#MinReportSubmit').val(),
			"TargetClassItem[]":Get_Multiple_Selection_Values('ClassID[]'),
			"RequirementItemCategory[]":Get_Element_Values('RequirementItemCategory[]'),
			"RequirementItemQuantity[]":Get_Element_Values('RequirementItemQuantity[]'),
			"WinnerGenerated":$('input#WinnerGenerated').val()
		},
		function(ReturnData){
			if(ReturnData=="1")
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateAwardSchemeSuccess']?>');
			else
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateAwardSchemeFail']?>');
			Get_Award_Scheme_Table();
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Delete_Award()
{
	//if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteAwardScheme']?>')){
		Block_Element('DivAwardSchemeTable');
		$.post(
			"ajax_update.php",
			{
				"Action":"DeleteAward",
				"AwardSchemeID[]":Get_Checked_Checkbox('AwardSchemeID[]') 
			},
			function(ReturnData){
				if(ReturnData=="1")
					Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['DeleteAwardSchemeSuccess']?>');
				else
					Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['DeleteAwardSchemeFail']?>');
				Get_Award_Scheme_Table();
				UnBlock_Element("DivAwardSchemeTable");
				Scroll_To_Top();
			}
		);
	//}
}

$().ready(function(){
	Get_Award_Scheme_Table();
})
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>