<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
//include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.carlos.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
//$ReadingGardenUI = new libreadinggarden_ui();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "AddAward":
		$DataArr['AwardName'] = trim(urldecode(stripslashes($_REQUEST['AwardName'])));
		$DataArr['Target'] = trim(urldecode(stripslashes($_REQUEST['AwardTarget'])));
		$DataArr['AwardType'] = $_REQUEST['AwardType'];
		$DataArr['Language'] = $_REQUEST['AwardLanguage'];
		$DataArr['StartDate'] = trim($_REQUEST['StartDate']);
		$DataArr['EndDate'] = trim($_REQUEST['EndDate']);
		$DataArr['ResultPublishDate'] = trim($_REQUEST['ResultPublishDate']);
		$DataArr['MinBookRead'] = trim($_REQUEST['MinBookRead']);
		$DataArr['MinReportSubmit'] = trim($_REQUEST['MinReportSubmit']);
		$DataArr['TargetClassItem'] = $_REQUEST['TargetClassItem'];
		$DataArr['RequirementItemCategory'] = $_REQUEST['RequirementItemCategory'];
		$DataArr['RequirementItemQuantity'] = $_REQUEST['RequirementItemQuantity'];
		$Result = $ReadingGardenLib->Add_Award_Scheme($DataArr);
		if($Result)
			echo "1";
		else
			echo "0";
	break;
	case "UpdateAward":
		$DataArr['AwardSchemeID'] = $_REQUEST['AwardSchemeID'];
		$DataArr['AwardName'] = trim(urldecode(stripslashes($_REQUEST['AwardName'])));
		$DataArr['Target'] = trim(urldecode(stripslashes($_REQUEST['AwardTarget'])));
		$DataArr['AwardType'] = $_REQUEST['AwardType'];
		$DataArr['Language'] = $_REQUEST['AwardLanguage'];
		$DataArr['StartDate'] = trim($_REQUEST['StartDate']);
		$DataArr['EndDate'] = trim($_REQUEST['EndDate']);
		$DataArr['ResultPublishDate'] = trim($_REQUEST['ResultPublishDate']);
		$DataArr['MinBookRead'] = trim($_REQUEST['MinBookRead']);
		$DataArr['MinReportSubmit'] = trim($_REQUEST['MinReportSubmit']);
		$DataArr['TargetClassItem'] = $_REQUEST['TargetClassItem'];
		$DataArr['RequirementItemCategory'] = $_REQUEST['RequirementItemCategory'];
		$DataArr['RequirementItemQuantity'] = $_REQUEST['RequirementItemQuantity'];
		$DataArr['WinnerGenerated'] = $_REQUEST['WinnerGenerated'];
		$Result = $ReadingGardenLib->Update_Award_Scheme($DataArr);
		if($Result)
			echo "1";
		else
			echo "0";
	break;
	case "DeleteAward":
		$AwardSchemeID = $_REQUEST['AwardSchemeID'];
		$Result = $ReadingGardenLib->Delete_Award_Scheme($AwardSchemeID);
		if($Result)
			echo "1";
		else
			echo "0";
	break;
	case "CheckAwardName":
		$AwardName = trim(urldecode(stripslashes($_REQUEST['AwardName'])));
		$OldAwardName = trim(urldecode(stripslashes($_REQUEST['OldAwardName'])));
		if($AwardName == $OldAwardName)
		{
			echo "1";
		}else{
			$Result = $ReadingGardenLib->Check_Award_Scheme_Name($AwardName);
			if($Result)
				echo "1";//ok
			else
				echo "0";
		}
	break;
}

intranet_closedb();
?>