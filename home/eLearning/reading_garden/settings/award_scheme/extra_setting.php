<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenUI = new libreadinggarden_ui();
$ReadingGardenLib = new libreadinggarden();

$CurrentPage = "Settings_AwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AwardScheme'], "award_scheme.php", 0);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['ExtraSetting'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$linterface->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Settings_Award_Scheme_Extra_Setting();

?>
<script language="javascript">
$().ready(function(){
	js_Display_View();
	js_Disable_Date_Field();
});

function js_Display_View()
{
	$(".DisplayView").show();
	$(".EditView").hide();
}

function js_Edit_View()
{
	$(".DisplayView").hide();
	$(".EditView").show();
}

</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>