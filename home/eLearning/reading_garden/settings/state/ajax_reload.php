<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{

	case "ReloadStateTable":
		echo $ReadingGardenUI->Get_Settings_State_Setting_Table();
	break; 
	case "LoadStateEditLayer":
		$StateID = $_REQUEST['StateID'];
	
		echo $ReadingGardenUI->Get_Settings_State_Setting_Edit_Layer($StateID);
	break;
	case "CheckScoreExist":
		$StateID = $_REQUEST['StateID'];
		$Score = $_REQUEST['Score'];
	
		echo $ReadingGardenLib->Is_State_Score_Exist($Score, $StateID);
	break;
}	

intranet_closedb();
?>