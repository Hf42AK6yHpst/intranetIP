<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "ReloadReccomendBookListDBTable":
	
		$YearID = $_REQUEST['YearID'];
		$CategoryID = $_REQUEST['CategoryID'];
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

		$arrCookies[] = array("ck_settings_recommend_book_list_page_size", "numPerPage");
		$arrCookies[] = array("ck_settings_recommend_book_list_page_number", "pageNo");
		$arrCookies[] = array("ck_settings_recommend_book_list_page_order", "order");
		$arrCookies[] = array("ck_settings_recommend_book_list_page_field", "field");	
		$arrCookies[] = array("ck_settings_recommend_book_list_page_keyword", "Keyword");
		$arrCookies[] = array("ck_settings_recommend_book_list_page_YearID", "YearID");
		$arrCookies[] = array("ck_settings_recommend_book_list_page_CategoryID", "CategoryID");
		updateGetCookies($arrCookies);

		echo $ReadingGardenUI->Get_Settings_Recommend_Book_List_DBTable($YearID, $field, $order, $pageNo, $numPerPage, $Keyword, $CategoryID);
	break; 
	
	case "LoadAddRecommendBookLayer":
		$YearID = $_REQUEST['YearID'];
		
		echo $ReadingGardenUI->Get_Settings_Recommend_Book_List_New_Layer($YearID);
	break; 
	
//	case "AutoCompleteSearchBook":
//		
//		echo $YearID."|".$YearID."\n";
//		echo $YearID."|".$YearID."\n";
//		echo $YearID."|".$YearID."\n";
//	break;
	case "LoadBookShowCase":
		$YearID = $_REQUEST['YearID'];
		$CategoryID = $_REQUEST['CategoryID'];
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		
		echo $ReadingGardenUI->Get_Settings_Recommend_Book_List_Book_Showcase($CategoryID,$Keyword,$YearID);
	break;
	
	
}	

intranet_closedb();
?>