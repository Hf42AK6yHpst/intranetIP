<?php
// editing by 

/************************************************
 * Date: 18-03-2013 (Rita)
 * Details: fix different js function name issue, change to js_Category_Setting()
 ************************************************/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_BookCategorySettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;
$TAGS_OBJ[] = array($Lang['ReadingGarden']['BookCategorySettings']['MenuTitle'], "", 1);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_Book_Category_Setting_UI();

?>
<script>
//function js_Edit_Category(Lang, CategoryID, msg)
//{
//	var CategoryID = CategoryID?CategoryID:'';
//	$.post(
//		"ajax_reload.php",
//		{
//			Action:"LoadCategorySettingTBLayer",
//			Language:Lang,
//			CategoryID:CategoryID
//		},
//		function(ReturnData)
//		{
//			$('div#TB_ajaxContent').html(ReturnData);
//			if(msg)
//				Get_Return_Message(msg);
//		}
//	);
//}


function js_Category_Setting(Lang, CategoryID, msg)
{
	var CategoryID = CategoryID?CategoryID:'';
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadCategorySettingTBLayer",
			Language:Lang,
			CategoryID:CategoryID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			if(msg)
				Get_Return_Message(msg);
		}
	);
}



function js_Add_Category(Lang,msg)
{
	js_Category_Setting(Lang,'',msg);
}

function js_Delete_Category(CategoryID,ContainBook)
{
	var Msg = ContainBook?'<?=$Lang['ReadingGarden']['ConfirmArr']['DeleteCategoryWithBook']?>':'<?=$Lang['ReadingGarden']['ConfirmArr']['DeleteCategory']?>'
	if(!confirm(Msg))
		return false;
	
	$.post(
		"ajax_update.php",
		{
			Action:"DeleteCategory",
			CategoryID:CategoryID
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			js_Reload_Category_Table();
		}
	);
}

function js_Update_Order()
{
	var jsSubmitString = $("input.RowCategoryID").serialize();
	jsSubmitString += '&Action=UpdateCategoryDisplayOrder';

	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
			Scroll_To_Top()
			if(ReturnData.substring(0,1) != 1)
				js_Reload_Category_Table();
			
		} 
	});
	
}

function js_Reload_Category_Table()
{
	Block_Element("CategoryTableDiv");
	$.post(
		"ajax_reload.php",
		{
			Action:"ReloadCategoryTable"
		},
		function(ReturnData)
		{
			$("div#CategoryTableDiv").html(ReturnData);
			initThickBox();
			js_Init_DND_Table();
			UnBlock_Element("CategoryTableDiv");
		}
	);
}

var AjaxCount = 0;
//var AddMore = false;
var CodeExist = false;
function CheckCode(Obj)
{
	var CategoryID = $("#CategoryID").val();
	var Language = $("#Language").val();
	var code = Obj.value;
	if(code.Trim()!='')
	{
		$("div#WarnBlankCategoryCode").hide();
		AjaxCount++;

		$.post(
			"ajax_reload.php",
			{
				Action:"CheckCategoryCodeExist",
				CategoryID:CategoryID,
				CategoryCode:code,
				Language:Language
			},
			function(ReturnData)
			{
				AjaxCount--;
				if(ReturnData==1)
				{
					$("div#WarnExistCategoryCode").show();
					CodeExist = true;
				}
				else
				{
					$("div#WarnExistCategoryCode").hide();
					CodeExist = false;
				}	
				
			}
		);
	}
	else
		$("div#WarnBlankCategoryCode").show();
	
}

function CheckTBForm(AddMore)
{
	
	var FormValid = true;
	$("div.WarnMsgDiv").hide();
	$("input.Mandatory").each(function(){
		if($(this).val().Trim()=='')
		{
			$("div#WarnBlank"+$(this).attr("id")).show();
			FormValid = false;
		}
	})
	
	// stop submitting if there is still any ajax checking in progress
	if(AjaxCount>0 || CodeExist)
	{
		return false;
	} 
	
	if(FormValid)
		AjaxSaveTBForm(AddMore);
}

function AjaxSaveTBForm(AddMore)
{
	var CategoryID = $("#CategoryID").val();
	var CategoryCode = $("#CategoryCode").val();
	var CategoryName = $("#CategoryName").val();
	var Language = $("#Language").val();
	
	$.post(
		"ajax_update.php",
		{
			Action:"UpdateCategoryInfo",
			CategoryID:CategoryID,
			CategoryCode:CategoryCode,
			CategoryName:CategoryName,
			Language:Language
		},
		function(ReturnData)
		{
			
			Get_Return_Message(ReturnData);
			if(ReturnData.substring(0,1)==1)
			{
				js_Reload_Category_Table()
				
				if(AddMore)
					js_Add_Category(Language,ReturnData);
				else
					js_Hide_ThickBox();
			}
		}
	);
	
}

function js_Init_DND_Table() {
		
	var JQueryObj = $("table#CategoryTable");
	var DraggingRowIdx = 0; 
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			if($(table).find("tr").index(DroppedRow)!= DraggingRowIdx) // update db only if order changed
				js_Update_Order();
		},
		onDragStart: function(table, DraggedTD) {
			DraggingRowIdx = $(table).find("tr").index($(DraggedTD).parent());
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	
}

$().ready(function(){
	js_Reload_Category_Table();
})
</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>