<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "UpdateCategoryInfo";
		$CategoryInfo['CategoryName'] = stripslashes($_REQUEST['CategoryName']);
		$CategoryInfo['CategoryCode'] = stripslashes($_REQUEST['CategoryCode']);
		$CategoryInfo['Language'] = stripslashes($_REQUEST['Language']);
		$CategoryID = $_REQUEST['CategoryID'];
		
		if(trim($CategoryID) != '' )
		{
			$Success = $ReadingGardenLib->Settings_Update_Category($CategoryInfo, $CategoryID);
			if($Success)
				echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
			else
				echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
		}
		else
		{
			$Success = $ReadingGardenLib->Settings_Insert_Category($CategoryInfo);
			if($Success)
				echo $ReadingGardenUI->Get_Return_Message("AddSuccess");
			else
				echo $ReadingGardenUI->Get_Return_Message("AddUnSuccess");
		}
		
	break;
	case "DeleteCategory";
		$CategoryID = $_REQUEST['CategoryID'];
		
		$Success = $ReadingGardenLib->Settings_Delete_Category( $CategoryID);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("DeleteSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("DeleteUnSuccess");
	break;	
	
	case "UpdateCategoryDisplayOrder";
		$CategoryIDArr = $RowCategoryID;
		
		$Success = $ReadingGardenLib->Settings_Update_Category_Display_Order($CategoryIDArr);
		if($Success)
			echo $ReadingGardenUI->Get_Return_Message("UpdateSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateUnSuccess");
	break;	
}	

intranet_closedb();
?>