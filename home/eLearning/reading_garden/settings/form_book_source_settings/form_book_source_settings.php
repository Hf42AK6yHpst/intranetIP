<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_BookSourceSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['BookSourceSettings']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Get_Settings_Form_Book_Source_Setting_UI();

?>
<script>
function SetAllSelection()
{
	var SetVal = $("#SetAllSourceSelection").val()
	if(SetVal.Trim()!='')
		$("select.SourceSelect").val(SetVal);
}

function CheckForm()
{
	FormValid = true;
	
	$("div.WarnMsgDiv").hide();
	$("select.SourceSelect").each(function(){
		if($(this).val().Trim()==false)
		{
			$("div#"+$(this).attr("id")+"WarnMsg").show();
			FormValid = false;
		}
	})
	
	return FormValid;
}

</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>