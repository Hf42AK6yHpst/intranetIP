<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

//debug_pr($_POST);
//die;
$ReadingGardenLib = new libreadinggarden();

$Success = $ReadingGardenLib->Settings_Save_Class_Book_Source_Settings($ClassBookSourceArr);

if($Success)
{
	$Msg = "SettingsSaveSuccess";
}
else
{
	$Msg = "SettingsSaveFail";
}
header("location: form_book_source_settings.php?Msg=".urlencode($Msg));


intranet_closedb();
?>