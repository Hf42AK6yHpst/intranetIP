<?php
// using : 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "GetCommentBankTable":
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		
		$arrCookies[] = array("ck_comment_bank_page_size", "numPerPage");
		$arrCookies[] = array("ck_comment_bank_page_number", "pageNo");
		$arrCookies[] = array("ck_comment_bank_page_order", "order");
		$arrCookies[] = array("ck_comment_bank_page_field", "field");	
		$arrCookies[] = array("ck_comment_bank_page_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $ReadingGardenUI->Get_Settings_Report_Comment_Bank_DBTable($field, $order, $pageNo, $numPerPage, $Keyword);
	break; 
	case "GetCommentBankEditForm":
		$CommentID = stripslashes(trim($_REQUEST['CommentID']));
		echo $ReadingGardenUI->Get_Settings_Report_Comment_Bank_Edit_Form($CommentID);
	break;
	case "SaveCommentBank":
		$CommentID = trim($_REQUEST['CommentID']);
		$Comment = trim(urldecode(stripslashes($_REQUEST['Comment'])));
		if($CommentID==''){
			// New Comment Bank
			$success = $ReadingGardenLib->Settings_Manage_Comment_Bank("add",'',$Comment);
			if($success)
				$Msg = "AddSuccess";
			else
				$Msg = "AddUnsuccess";
			echo $ReadingGardenUI->Get_Return_Message($Msg);
		}else{
			// Edit Comment Bank
			$success = $ReadingGardenLib->Settings_Manage_Comment_Bank("update",$CommentID,$Comment);
			if($success)
				$Msg = "UpdateSuccess";
			else
				$Msg = "UpdateUnsuccess";
			echo $ReadingGardenUI->Get_Return_Message($Msg);
		}
	break;
}	

intranet_closedb();
?>