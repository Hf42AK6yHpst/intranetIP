<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_comment_bank_page_size", "numPerPage");
$arrCookies[] = array("ck_comment_bank_page_number", "pageNo");
$arrCookies[] = array("ck_comment_bank_page_order", "order");
$arrCookies[] = array("ck_comment_bank_page_field", "field");	
$arrCookies[] = array("ck_comment_bank_page_keyword", "Keyword");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_CommentBankSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['CommentBank'], "", 1);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_Report_Comment_Bank_UI($field,$order,$pageNo,$numPerPage);

?>
<script>
var isAddMore = 0;
function Get_Comment_Bank_Table()
{
	var formData = $('#form1').serialize();
	formData += "&Action=GetCommentBankTable";
	
	Block_Element("DivCommentBankTable");
	$.ajax({
		type:"POST",
		url:"ajax_reload.php",
		data: formData,  
		success: function(ReturnData) {
			$("#DivCommentBankTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("DivCommentBankTable");
			Scroll_To_Top();
		}
	});
}

function Get_Comment_Bank_Form(CommentID,Msg)
{
	if(CommentID!='' && $("input[name=CommentID\\[\\]]:checked").val())
		CommentID = $("input[name=CommentID\\[\\]]:checked").val();
	tb_show("<?=$Lang['Btn']['Edit']?>","#TB_inline?height=350&width=550&inlineId=FakeLayer");
	
	$.post(
		"ajax_reload.php",
		{
			Action:"GetCommentBankEditForm",
			CommentID:CommentID
		},
		function(ReturnData){
			$("div#TB_ajaxContent").html(ReturnData);
			
			if(Msg)
				Get_Return_Message(Msg);
		}
	)
}

function CheckTBForm()
{
	$("div.WarnMsgDiv").hide();
	var comment_id = $.trim($("#TBForm").find("input#CommentID").val());
	var comment = $.trim($('input#Comment').val());
	if(comment==''){
		$("div.WarnMsgDiv").show();
	}else {
		Block_Thickbox();
		$.post(
			"ajax_reload.php",
			{
				Action:"SaveCommentBank",
				Comment:encodeURIComponent($.trim($("input#Comment").val())),
				CommentID:comment_id 
			},
			function(ReturnMsg){
				if(isAddMore == 1)
					Get_Comment_Bank_Form('',ReturnMsg);
				else{
					tb_remove();
					Get_Return_Message(ReturnMsg);
				}
				isAddMore = 0;
				UnBlock_Thickbox();
				Get_Comment_Bank_Table();
			}
		)
	}
}

$().ready(function(){
	Get_Comment_Bank_Table();
})
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>