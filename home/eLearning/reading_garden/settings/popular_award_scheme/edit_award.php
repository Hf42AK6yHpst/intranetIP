<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_PopularAwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardSetting'], "", 1);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoringItem'], "scoring_item_settings.php", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_Popular_Award_Scheme_Edit_Award_UI($AwardID[0]);

?>
<script>
function Add_Requirement()
{
//	BlockElement("RequirementEntryTD");
	if($("div.EntryDiv").length == 0)
		var NextIndex = 0;
	else
		var NextIndex = parseInt($(".EntryIndex:last").val()) + 1;
	
	$.post(
		"ajax_reload.php",
		{
			"Action": "AddAwardRequirementEntry",
			EntryIndex: NextIndex
		},
		function(ReturnData){
			$("td#RequirementEntryTD").append(ReturnData);
//			UnBlockElement("RequirementEntryTD");
		}
	);
}

function Delete_Requirement(EntryIndex)
{
	$("div#EntryDiv_"+EntryIndex).remove();
}

function js_Check_Form()
{
	var FormValid = true;
	$("div.WarnMsg").hide();

	// name
	if($.trim($("input#AwardNameEn").val()) == '')
	{
		$("div#WarnEmptyAwardNameEn").show();
		FormValid = false;
	}

	if($.trim($("input#AwardNameCh").val()) == '')
	{
		$("div#WarnEmptyAwardNameCh").show();
		FormValid = false;
	}

	if($.trim($("input#TotalScore").val()) == '')
	{
		$("div#WarnEmptyScoreRequired_Total").show();
		FormValid = false;
	}
	else if(!is_positive_int($("input#TotalScore").val()))
	{
		$("div#WarnNumberScoreRequired_Total").show();
		FormValid = false;
	}
	
	if(!$("#YearID\\\[\\\]").val())
	{
		$("div#WarnEmptyForm").show();
		FormValid = false;
	}
	

	// Requirement	
	var CategoryGroupArr = new Object();
	$(".EntryIndex").each(function(){
		var Idx = $(this).val();
		
		// check score
		if($.trim($("input#Score_"+Idx).val()) == '')
		{
			$("div#WarnEmptyScoreRequired_"+Idx).show();
			FormValid = false;
		}
		else if(!is_positive_int($("input#Score_"+Idx).val()))
		{
			$("div#WarnNumberScoreRequired_"+Idx).show();
			FormValid = false;
		}
		
		// check duplicated category group
		var $CheckedCategoryObj = $("input.CategorySelect_"+Idx+":checked");
		if($CheckedCategoryObj.length==0)
		{
			$("div#WarnEmptyCategory_"+Idx).show();
			FormValid = false;
		}
		else
		{
			var checked_Cat = new Array();
			$CheckedCategoryObj.each(function(){
				checked_Cat.push($(this).val());
			});
			
			CatGroupStr = checked_Cat.join(",");
			if(!CategoryGroupArr[CatGroupStr])
				CategoryGroupArr[CatGroupStr] = Idx;
			else
			{
				var DuplicatedIdx = CategoryGroupArr[CatGroupStr];
				$("div#WarnDuplicatedRequirement_"+DuplicatedIdx).show();
				$("div#WarnDuplicatedRequirement_"+Idx).show();
				FormValid = false;
			}
		}
	});
	
	return FormValid;
}

$(function(){
	if(<?=$AwardID[0]?0:1?>)
		Select_All_Options('YearID[]',true);
}); 

</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>