<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();

$ReadingGardenLib->Start_Trans();

$AwardNameEn = trim(stripslashes($AwardNameEn));
$AwardNameCh = trim(stripslashes($AwardNameCh));

if(!$AwardID)
{
	$Success['Insert'] = $ReadingGardenLib->Insert_Popular_Award_Scheme_Award($AwardNameEn, $AwardNameCh, $TotalScore);
	$AwardID = mysql_insert_id();
	$AddEdit = "Add";
}
else
{
	$Success['Edit'] = $ReadingGardenLib->Edit_Popular_Award_Scheme_Award($AwardID, $AwardNameEn, $AwardNameCh, $TotalScore);
	$AddEdit = "Edit";
}

$Success['UpdateFormMapping'] = $ReadingGardenLib->Update_Popular_Award_Scheme_Form_Mapping($AwardID, $YearID);

$Success['UpdateRequirement'] = $ReadingGardenLib->Update_Popular_Award_Scheme_Award_Requirement($AwardID, array_values((array)$Score), array_values((array)$CategoryID));
 
if(!in_array(false, (array)$Success))
{
	$ReadingGardenLib->Commit_Trans();
	$msg = $AddEdit."AwardSuccess";	
} 
else
{
	$ReadingGardenLib->RollBack_Trans();
	$msg = $AddEdit."AwardFail";
}

header("location: index.php?Msg=$msg");

 
intranet_closedb();
?>