<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_PopularAwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardSetting'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoringItem'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_Popular_Award_Scheme_Item_Setting_UI();

?>
<script>
var CodeValid = true;
var EditingCat = '';
function js_Category_Setting(Msg)
{
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadCategorySettingTBLayer"
		},
		function(ReturnData)
		{
			EditingCat = '';
			$('div#TB_ajaxContent').html(ReturnData);
			js_Init_DND_Category_Table();
			if(Msg)
				Get_Return_Message(Msg);
		}
	);
}

function js_Add_Category()
{
	if(EditingCat != '') return false;
	
	EditingCat = "new";

	$("#row_new").show();
	$("#add_row").hide();
}

function js_Cancel_Add_Category()
{
	$("#row_new").hide();
	$(".WarnMsg").hide();
	$("#add_row").show();
	CodeValid = true;
	
	$("#row_new").find(".WarnUnavailableCategoryCode").hide();
	$("#row_new").find(".CategoryCode").val("");
	$("#row_new").find(".CategoryNameEn").val("");
	$("#row_new").find(".CategoryNameCh").val("");
	EditingCat = '';
}

function js_Validate_Category()
{
	var FormValid = true;
	$(".WarnMsg").hide();
	
	$("#row_"+EditingCat).find(".required").each(function(){
		$this = $(this);
		if($.trim($this.val())=='')
		{
			$("#row_"+EditingCat).find(".WarnEmpty"+$this.attr("id")).show();
			FormValid = false;
		}
	});
	
	FormValid = CodeValid && FormValid;
	
	if(FormValid)
		Ajax_Save_Category();
}

function js_Check_Code()
{
	var Code = $("#row_"+EditingCat).find(".CategoryCode").val();
	
	if(Code != '' )
	{
		$("#row_"+EditingCat).find(".WarnEmptyCategoryCode").hide();
		$.post(
			"ajax_reload.php",
			{
				Action:"CheckCodeExist",
				CategoryCode:Code,
				CategoryID:EditingCat
			},
			function(ReturnData)
			{
				CodeValid = ReturnData == 1
				if(!CodeValid)
					$("#row_"+EditingCat).find(".WarnUnavailableCategoryCode").show();
				else		
					$("#row_"+EditingCat).find(".WarnUnavailableCategoryCode").hide();
			}
		);
	}
	else
	{
		$("#row_"+EditingCat).find(".WarnUnavailableCategoryCode").hide();
		$("#row_"+EditingCat).find(".WarnEmptyCategoryCode").show();
	}
}

function Ajax_Save_Category()
{
	var CategoryCode = $("#row_"+EditingCat).find(".CategoryCode").val();
	var CategoryNameEn = $("#row_"+EditingCat).find(".CategoryNameEn").val();
	var CategoryNameCh = $("#row_"+EditingCat).find(".CategoryNameCh").val();
	
	$.post(
		"ajax_update.php",
		{
			Action:"AddEditCategory",
			CategoryID:EditingCat,
			CategoryCode:CategoryCode,
			CategoryNameEn:CategoryNameEn,
			CategoryNameCh:CategoryNameCh
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			js_Category_Setting(ReturnData);
			js_Reload_Item_Table();
		}
	);
}

function js_Delete_Category(CategoryID)
{
	if(!confirm("<?=$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsConfirmArr']['DeleteCategory']?>"))
		return false;
	
	$.post(
		"ajax_update.php",
		{
			Action:"DeleteCategory",
			CategoryID:CategoryID
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			js_Category_Setting(ReturnData);
			js_Reload_Item_Table();
		}
	);
}

function js_Update_Category_Order()
{
	var jsSubmitString = $("input.CategoryID").serialize();
	jsSubmitString += '&Action=UpdateCategoryDisplayOrder';

	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
			js_Reload_Item_Table();
			if(ReturnData.substring(0,1) != 1)
				js_Category_Setting(ReturnData);
				
		} 
	});
	
}


function js_Edit_Category(CategoryID)
{
	if(EditingCat != '') return false; 
	
	EditingCat = CategoryID;
	
	$("#row_"+CategoryID).find(".InputDiv").show();
	$("#row_"+CategoryID).find(".DisplayDiv").hide();
}

function js_Cancel_Edit_Category(CategoryID)
{
	EditingCat = '';
	$("#row_"+CategoryID).find(".InputDiv").hide();
	$("#row_"+CategoryID).find(".DisplayDiv").show();
	$("#row_"+CategoryID).find(".WarnUnavailableCategoryCode").hide();
}

function js_Init_DND_Category_Table() {
		
	var JQueryObj = $("table#CategoryTable");
	var DraggingRowIdx = 0; 
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			if($(table).find("tr").index(DroppedRow)!= DraggingRowIdx) // update db only if order changed
				js_Update_Category_Order();
		},
		onDragStart: function(table, DraggedTD) {
			DraggingRowIdx = $(table).find("tr").index($(DraggedTD).parent());
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	
}

// Item Management
var ItemCodeValid = true;
function js_Reload_Item_Table()
{
	Block_Element("ItemTableDiv");
	
	$.post(
		"ajax_reload.php",
		{
			Action:"ReloadItemTable"
		},
		function(ReturnData)
		{
			$("div#ItemTableDiv").html(ReturnData);
			initThickBox();
			js_Init_DND_Item_Table();
			UnBlock_Element("ItemTableDiv");
		}
	);
}

function js_Edit_Item(CategoryID, ItemID, ReturnMsg)
{
	var CategoryID = CategoryID || '';
	var ItemID = ItemID || '';
	var ReturnMsg = ReturnMsg || '';
	
	$.post(
		"ajax_reload.php",
		{
			Action:"EditItemLayer",
			CategoryID: CategoryID,
			ItemID: ItemID
		},
		function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			
			if(ReturnMsg)
				Get_Return_Message(ReturnMsg);
		}
	);
}

function js_Validate_Item(SubmitAndAdd)
{
	var SubmitAndAdd = SubmitAndAdd || '';
	var FormValid = true;
	$(".WarnMsgDiv").hide();
	
	$("#TBForm").find(".Mandatory").each(function(){
		$this = $(this);
		
		if($.trim($this.val()) == '')
		{
			FormValid = false;
			$("#WarnEmpty"+$this.attr("id")).show();
		}
	});
	
	// check numeric score
	$score = $("#Score");
	if($.trim($score.val())!='' && !(IsNumeric($score.val()) && $score.val()>0))
	{
		$("#WarnNonNumbericScore").show();
		FormValid = false;
	}
	
	FormValid = FormValid && ItemCodeValid;
	
	if(FormValid)
		Ajax_Save_Item(SubmitAndAdd);
}

function Ajax_Save_Item(SubmitAndAdd)
{
	var SubmitAndAdd = SubmitAndAdd || '';
	var CategoryID = $("#CategoryID").val();
	var ItemID = $("#ItemID").val();
	var ItemCode = $("#ItemCode").val();
	var ItemDescCh = $("#ItemDescCh").val();
	var ItemDescEn = $("#ItemDescEn").val();
	var Score = $("#Score").val();
	var CanSubmitByStudent = $(".CanSubmitByStudent:checked").val();
	
	$.post(
		"ajax_update.php",
		{
			Action:"AddEditItem",
			CategoryID:CategoryID,
			ItemID:ItemID,
			ItemCode:ItemCode,
			ItemDescCh:ItemDescCh,
			ItemDescEn:ItemDescEn,
			Score:Score,
			CanSubmitByStudent:CanSubmitByStudent
		},
		function(ReturnData){
			Get_Return_Message(ReturnData);
			js_Reload_Item_Table();
			if(SubmitAndAdd)
				js_Edit_Item(CategoryID, '', ReturnData);
			else
				js_Hide_ThickBox();
		}
	);
	
}

function js_Delete_Item(ItemID)
{
	if(!confirm("<?=$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsConfirmArr']['DeleteItem']?>"))
		return false;
	
	$.post(
		"ajax_update.php",
		{
			Action:"DeleteItem",
			ItemID:ItemID
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			js_Reload_Item_Table();
		}
	);
}

function js_Check_ItemCode()
{
	var ItemID = $("input#ItemID").val();
	var Code = $("#ItemCode").val();
	
	if(Code != '' )
	{
		$("#WarnEmptyItemCode").hide();
		$.post(
			"ajax_reload.php",
			{
				Action:"CheckItemCodeExist",
				ItemCode:Code,
				ItemID:ItemID
			},
			function(ReturnData)
			{
				ItemCodeValid = ReturnData == 1
				if(!ItemCodeValid)
					$("#WarnItemCodeExist").show();
				else		
					$("#WarnItemCodeExist").hide();
			}
		);
	}
	else
	{
		$("#WarnItemCodeExist").hide();
		$("#WarnEmptyItemCode").show();
	}
}

function js_Update_Item_Order()
{
	var jsSubmitString = $("input.RowItemID").serialize();
	jsSubmitString += '&Action=UpdateItemDisplayOrder';

	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
			if(ReturnData.substring(0,1) != 1)
				js_Reload_Item_Table();
				
		} 
	});
	
}

function js_Init_DND_Item_Table() {
		
	var JQueryObj = $("table#ItemTable");
	var DraggingRowIdx = 0; 
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			if($(table).find("tr").index(DroppedRow)!= DraggingRowIdx) // update db only if order changed
				js_Update_Item_Order();
		},
		onDragStart: function(table, DraggedTD) {
			DraggingRowIdx = $(table).find("tr").index($(DraggedTD).parent());
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	
}

$().ready(function(){
	js_Reload_Item_Table();
})
</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>