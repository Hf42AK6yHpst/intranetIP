<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "AddEditCategory":
		$CategoryCode = stripslashes($_REQUEST['CategoryCode']);
		$CategoryNameEn = stripslashes($_REQUEST['CategoryNameEn']);
		$CategoryNameCh = stripslashes($_REQUEST['CategoryNameCh']);
		if($CategoryID == 'new')
		{
			$Success = $ReadingGardenLib->Add_Popular_Award_Scheme_Category($CategoryCode, $CategoryNameEn, $CategoryNameCh);
			$Msg = $Success?"AddCategorySuccess":"AddCategoryFail";
		}
		else
		{
			$UpdateData['CategoryCode'] = $CategoryCode;
			$UpdateData['CategoryNameEn'] = $CategoryNameEn;
			$UpdateData['CategoryNameCh'] = $CategoryNameCh;
			$Success = $ReadingGardenLib->Update_Popular_Award_Scheme_Category($UpdateData, $CategoryID);
			$Msg = $Success?"UpdateCategorySuccess":"UpdateCategoryFail";
		}
		
		
		echo $ReadingGardenUI->Get_Return_Message($Msg);		
	break;
	case "DeleteCategory":
		$Success = $ReadingGardenLib->Delete_Popular_Award_Scheme_Category($CategoryID);
		$Msg = $Success?"DeleteCategorySuccess":"DeleteCategoryFail";
		echo $ReadingGardenUI->Get_Return_Message($Msg);		
	break;
	case "UpdateCategoryDisplayOrder":
		$Success = $ReadingGardenLib->Update_Popular_Award_Scheme_Category_Display_Order($CategoryID);
		$Msg = $Success?"UpdateCategoryOrderSuccess":"UpdateCategoryOrderFail";
		echo $ReadingGardenUI->Get_Return_Message($Msg);		
	break;
	case "AddEditItem":
		$ItemCode = trim(stripslashes($_REQUEST['ItemCode']));
		$ItemDescEn = trim(stripslashes($_REQUEST['ItemDescEn']));
		$ItemDescCh = trim(stripslashes($_REQUEST['ItemDescCh']));
		
		if(trim($ItemID) == '')
		{
			$Success = $ReadingGardenLib->Add_Popular_Award_Scheme_Item($ItemCode, $ItemDescEn, $ItemDescCh, $Score, $CanSubmitByStudent, $CategoryID);
			$Msg = $Success?"AddItemSuccess":"AddItemFail";
		}
		else
		{
			$UpdateData['ItemCode'] = $ItemCode;
			$UpdateData['ItemDescEn'] = $ItemDescEn;
			$UpdateData['ItemDescCh'] = $ItemDescCh;
			$UpdateData['Score'] = $Score;
			$UpdateData['CanSubmitByStudent'] = $CanSubmitByStudent;
			$Success = $ReadingGardenLib->Update_Popular_Award_Scheme_Item($UpdateData, $ItemID);
			$Msg = $Success?"UpdateItemSuccess":"UpdateItemFail";
		}
		
		echo $ReadingGardenUI->Get_Return_Message($Msg);		
	break;
	case "DeleteItem":
		$Success = $ReadingGardenLib->Delete_Popular_Award_Scheme_Item($ItemID);
		$Msg = $Success?"DeleteItemSuccess":"DeleteItemFail";
		echo $ReadingGardenUI->Get_Return_Message($Msg);		
	break;
	case "UpdateItemDisplayOrder":
		$Success = $ReadingGardenLib->Update_Popular_Award_Scheme_Item_Display_Order($RowItemID);
		$Msg = $Success?"UpdateItemOrderSuccess":"UpdateItemOrderFail";
		echo $ReadingGardenUI->Get_Return_Message($Msg);		
	break;
	
}	

intranet_closedb();
?>