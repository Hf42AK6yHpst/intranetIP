<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "ReloadItemTable":
		echo $ReadingGardenUI->Get_Settings_Popular_Award_Scheme_Item_Setting_Table();
	break;
	case "LoadCategorySettingTBLayer":
		echo $ReadingGardenUI->Get_Setting_Popular_Award_Scheme_Category_Edit_Layer();
	break; 	 
	case "CheckCodeExist":
		echo !$ReadingGardenLib->Is_Popular_Award_Scheme_Category_Code_Exist($CategoryCode, $CategoryID);
	break;
	case "EditItemLayer":
		echo $ReadingGardenUI->Get_Popular_Award_Scheme_Item_Edit_Layer($CategoryID, $ItemID);
	break;
	case "CheckItemCodeExist":
		echo !$ReadingGardenLib->Is_Popular_Award_Scheme_Item_Code_Exist($ItemCode, $ItemID);
	break;
	case "AddAwardRequirementEntry":
		echo $ReadingGardenUI->Get_Popular_Award_Scheme_Requirement_Entry($EntryIndex);
	break;

}	

intranet_closedb();
?>