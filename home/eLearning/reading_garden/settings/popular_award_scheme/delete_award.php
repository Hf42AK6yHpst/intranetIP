<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();

$Success['DeleteAward'] = $ReadingGardenLib->Remove_Popular_Award_Scheme_Award($AwardID);
 
if(!in_array(false, (array)$Success))
{
	$msg = "DeleteAwardSuccess";	
} 
else
{
	$msg = "DeleteAwardFail";
}

header("location: index.php?Msg=$msg");
 
intranet_closedb();
?>