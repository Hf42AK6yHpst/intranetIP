<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
//modifying By
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libportal.php");


intranet_auth();
intranet_opendb();

/**
 * GET INPUT PARAMETER
 */
$stage_id = intval(trim($stage_id));
$scheme_id = intval(trim($scheme_id));


$html_stage_id = $stage_id;
$html_scheme_id = $scheme_id;

$note_id = retrieveNoteIdByStageIdAndUserId($UserID, $stage_id);
// DEBUG_R($note_id);
$li = new libuser($UserID);
$objIES = new libies();



// debug_r($step_id);
 if(empty($stage_id) || !is_int($stage_id))
 {
	 echo "Invalide access , Empty Stage ID<br/>";
	 exit();
 }


$linterface = new interface_html($ies_cfg["DefaultFrontEndInterface"]);
$CurrentPage = "StudentProgress";



### Title ###
$title = $ies_lang['IES_TITLE'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$scheme_arr = $objIES->GET_USER_SCHEME_ARR($UserID);
$html_scheme_selection = libies_ui::GET_SCHEME_SELECTION($scheme_arr, $scheme_id);

$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);
$html_stage_selection = libies_ui::GET_STAGE_SELECTION($stage_arr,$stage_id,$scheme_id);

//get the stage deadline
$html_StageDeadLine = "";
for($i = 0, $j= sizeof($stage_arr); $i < $j; $i++)
{
	$_tmpStageID = $stage_arr[$i]["StageID"];
	$_tmpStageDeadLine = $stage_arr[$i]["Deadline"];

	$html_StageDeadLine = ($_tmpStageID == $stage_id) ? $_tmpStageDeadLine: $html_StageDeadLine;
}

$stage_arr = $objIES->GET_STAGE_DETAIL($stage_id);
list($stage_title, $stage_sequence, $stage_deadline) = $stage_arr;

$task_arr = getStageTaskArr($stage_id);

$html_content = "";

$html_js_load_task_map = "var task_step_map = [";


//LOOP FOR EACH TASK FOR THIS STAGE
for($i=0; $i<count($task_arr); $i++)
{

  $t_task_id = $task_arr[$i]['TaskID'];
  $t_step_id = $task_arr[$i]['StepID'];
	$html_js_load_task_map .= "[".$t_task_id.", ".$t_step_id."],";



}


$html_js_load_task_map = substr($html_js_load_task_map, 0, -1)."]\n";



##########################################
##	HTML - stage_title
$html["stage_title"] = $stage_arr["Title"];
##########################################
// $html_templateFile this variable exist in each model.php
$templateFile = "templates/handInTask.tmpl.php";

if(file_exists($templateFile)){
  ob_start();
  include_once($templateFile);
  $html_content = ob_get_contents();
  ob_end_clean();
}


$linterface->LAYOUT_START();
include_once("templates/generalframe.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();




/**
 * Retrieve Note Id by the stageid and the userid
 * @param	int	$ParUserId - the user id
 * @param	int	$ParStageId - the stage id
 * @return	A note id that belong to a student in a scheme
 */
function retrieveNoteIdByStageIdAndUserId($ParUserId,$ParStageId) {
	global $intranet_db;
	$ldb = new libdb();
	$sql = "SELECT IRN.NOTEID
			FROM $intranet_db.IES_REFLECT_NOTE IRN
			INNER JOIN IES_SCHEME_STUDENT ISCHS
				ON IRN.SCHEMESTUDENTID = ISCHS.SCHEMESTUDENTID
			INNER JOIN IES_STAGE ISTA
				ON ISCHS.SCHEMEID = ISTA.SCHEMEID
			WHERE ISTA.STAGEID = '$ParStageId'
				AND ISCHS.USERID = '$ParUserId'
			";
//DEBUG_R($sql);
	$returnArray = $ldb->returnArray($sql);
	$noteid = $returnArray[0]["NOTEID"];
	return $noteid;
}

/**
* GET DETAILS FOR A STUDENT RELATED TO A TASK
*
* @param : INT $taskId TASK ID
* @param : INT $studentID Student ID
* @return : ARRAY TASK DETAILS FOR THE STUDENT RELATED TO THE TASK
*
*/
/*
function getStudentTaskDetails($taskId , $userId)
{
	$ldb = new libdb();
	$sql = "select
		        RecordID,
				TaskID,
				UserID,
				Status,
				DateInput,
				InputBy,
				DateModified,
				ModifyBy
			from
				IES_TASK_STUDENT_DETAILS
			where
				TaskID = {$taskId} and
				UserID = {$userId}
			";

	$returnArray = $ldb->returnArray($sql);

	return $returnArray;
}
function testScript($taskId , $userId)
{
	$ldb = new libdb();
	$sql = "";
	$ldb->db_db_query($sql);
}

function checkTaskStart($parTaskID){
  global $intranet_db, $UserID;

  $ldb = new libdb();

  // Check if task or step in task has been done
  $sql = "SELECT DISTINCT TaskID FROM {$intranet_db}.IES_TASK_HANDIN WHERE TaskID = {$parTaskID} AND UserID = {$UserID} ";
  $sql .= "UNION ";
  $sql .= "SELECT DISTINCT TaskID FROM {$intranet_db}.IES_QUESTION_HANDIN question_handin INNER JOIN {$intranet_db}.IES_STEP step ON question_handin.StepID = step.StepID WHERE step.TaskID = {$parTaskID} AND question_handin.UserID = {$UserID}";
  $task_answer = count($ldb->returnVector($sql));

	return ($task_answer > 0);
}
*/
function initAllStudentTaskDetails()
{
	/*
	insert into IES_TASK_STUDENT_DETAILS
(TaskID,UserID,Status,DateInput)
select
	task.taskID,
	student.UserID,
	1,now()

 from IES_SCHEME_STUDENT as student
inner join IES_SCHEME as scheme
	 on student.schemeID = scheme.schemeID
inner join IES_STAGE as stage
	on stage.schemeID = scheme.schemeID
inner join IES_TASK as task
	on task.stageid = stage.stageid


	*/
}
function getStageTaskArr($parStageID)
{
  global $intranet_db;

  $ldb = new libdb();

  $sql = "SELECT task.TaskID, step.StepID FROM {$intranet_db}.IES_TASK task LEFT JOIN {$intranet_db}.IES_STEP step ON task.TaskID = step.TaskID WHERE task.StageID = '{$parStageID}' GROUP BY task.TaskID HAVING MIN(step.Sequence) ORDER BY task.Sequence, step.Sequence";
  $stageTaskArr = $ldb->returnArray($sql);

  return $stageTaskArr;
}

?>
