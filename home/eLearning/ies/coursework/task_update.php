<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

//GET USER INPUT VARIABLE
$task_content = trim($task_content);

$lies = new libies();

$lies->Start_Trans();
$res = $lies->UPDATE_TASK_CONTENT($task_id, $task_content, $task_type, $UserID);
if($res)
{
  $lies->Commit_Trans();
//  echo "Saved";
}
else
{
  $lies->RollBack_Trans();
//  echo "Not Saved";
}

?>