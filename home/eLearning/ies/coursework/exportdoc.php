<?php
//Editing by: thomas
	$PATH_WRT_ROOT = "../../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	//include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
	include_once($PATH_WRT_ROOT."includes/ies/libies.php");
	include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/ies/libies_export.php");
	intranet_auth();
	intranet_opendb();
	
	$_folder = "/tmp/ies_export/";
	$folder_target = "{$_folder}/{$ParUserId}/{$ParStageId}";
	$libies_export = new libies_export($folder_target);
//	$objIES = new libies();



	$libies_export->setLang(libies::getThisSchemeLang());
	$fs = new libfilesystem();
	$save2tmp=true;
	
	
	$stage_details = $libies_export ->GET_STAGE_DETAIL($ParStageId);
//	DEBUG_r($stage_details);
	$Sequence = $stage_details["Sequence"];
	$Title = $stage_details["Title"];
	
	if($Sequence != 3){
		$libies_export ->export_stage_report($ParStageId, $ParUserId);
	}

	if($Sequence == 2){
		$SurveyIDs = $libies_export->getStudentStageSurvey($ParStageId, $ParUserId);

		$SurveyID = $SurveyIDs[$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]];
		$InterviewID =  $SurveyIDs[$ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]];
		$ObservationID =  $SurveyIDs[$ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]];

		$answerSurvey[$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]] = $SurveyIDs[$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]];
		$answerSurvey[$ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]] = $SurveyIDs[$ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]];
		$answerSurvey[$ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]] = $SurveyIDs[$ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]];

		foreach($answerSurvey as $_surveyCodeNo => $_surveyIDArray){
			if(!is_array($_surveyIDArray) ){
				continue;  // NO DETAILS FOR THIS CODE NO , SKIP TO NEXT CODE NO
			}
			$_details = $libies_export->getSurveyDetails($_surveyIDArray);
			for($s = 0,$s_max = sizeof($_details);$s < $s_max; $s++){
				$_surveyID = $_details[$s]['SurveyID'];
				if(is_numeric($_surveyID)){
					$libies_export ->QuestionPrintForWord($_surveyID, $save2tmp);

					if($_surveyCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
						$libies_export ->exportAnaylsisreport($_surveyID, $ParUserId);
					}
					if($_surveyCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0] || 
						$_surveyCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){
						$libies_export ->export_SurveyResult($_surveyID);
					}
				}
			}
		}
	}
	
	if($Sequence == 3){

		$libies_export->export_stage_3_report($ParStageId, $ParUserId);

		if (!empty($ParStageId)) {
			## DocSectionPrintForWord
//			$libies_export->DocSectionPrintForWord($ParUserId, $ParStageId, $save2tmp);
//			## export_DocSection
//			$libies_export->export_DocSection();
		}
		
	}
	
	##remove the zip file first
	$zip_name = $ParUserId."_Stage_".$ParStageId.".zip";
	$zip_path = $_folder."/".$zip_name;

	$fs->file_remove($zip_path);
	$fs->file_zip($ParStageId, $zip_path, "{$_folder}/$ParUserId/");
	
	##remove the folder
	$fs->folder_remove_recursive($folder_target);
	
	##out the zip file to browser and let the user download it.
	output2browser(get_file_content($zip_path), $zip_name);
	
	
	intranet_closedb();

?>