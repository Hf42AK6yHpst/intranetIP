<?php
//modifying By fai
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html($ies_cfg["DefaultFrontEndInterface"]);
$CurrentPage = "StudentProgress";

$li = new libuser($UserID);
$objIES = new libies();

### Title ###
$title = $ies_lang['IES_TITLE'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$task_arr = getStageTaskArr($stage_id);

$content = "";
$js_load_task_map = "var task_step_map = [";
for($i=0; $i<count($task_arr); $i++)
{
  $t_task_id = $task_arr[$i]['TaskID'];
  $t_step_id = $task_arr[$i]['StepID'];
  $t_title = $task_arr[$i]['Title'];

  $content .= "<div class=\"IES_task\"><div class=\"task_title task_open\"><span>".$t_title."</span></div></div>";
  $content .= "<div class=\"clear\"></div>";

  $js_load_task_map .= "[".$t_task_id.", ".$t_step_id."],";

  $content .= '
<div id="task_block_'.$t_task_id.'">

</div>
';

  $content .= "<div class=\"IES_task\"><div class=\"task_title task_close\"><span>".$t_title."</span></div> <span class=\"content_grey_text\">(未開放)</span><div class=\"clear\"></div></div>";
}
$js_load_task_map = substr($js_load_task_map, 0, -1)."]\n";

$linterface->LAYOUT_START();
include_once("templates/stage.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();


function getStageTaskArr($parStageID)
{
  global $intranet_db;

  $ldb = new libdb();

  $sql = "SELECT task.TaskID, step.StepID, task.Title FROM {$intranet_db}.IES_TASK task LEFT JOIN {$intranet_db}.IES_STEP step ON task.TaskID = step.TaskID WHERE task.StageID = '{$parStageID}' GROUP BY task.TaskID HAVING MIN(step.Sequence) ORDER BY task.Sequence, step.Sequence";
  $stageTaskArr = $ldb->returnArray($sql);

  return $stageTaskArr;
}

?>
