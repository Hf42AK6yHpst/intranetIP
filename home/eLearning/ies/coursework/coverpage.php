<?php
//modifying By Max
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

//GET USER VARIABLE 
$stage_id = intval(trim($stage_id));

if(empty($stage_id) || !is_int($stage_id))
{
	echo "Invalide access , Empty STAGE ID<br/>";
    exit();
}

intranet_auth();
intranet_opendb();

$objIES = new libies();

$layout_tmpl = ($objIES->isIES_Teacher($UserID) || $objIES->IS_ADMIN_USER($UserID)) ? $ies_cfg["PreviewFrontEndInterface"] : $ies_cfg["DefaultFrontEndInterface"];
$linterface = new interface_html($layout_tmpl);
$CurrentPage = "StudentProgress";

$stageDetails = $objIES->GET_STAGE_DETAIL($stage_id);

if(is_array($stageDetails))
{
	$h_stageTitle = $stageDetails["Title"];
	$h_stageDeadline = $stageDetails["Deadline"];
	$h_stageSeq = $stageDetails["Sequence"];

	//Temporary pass $h_stageSeq to this function , may be change to $stage_id for next pharse
	$h_stageDesc = $objIES->getStageDescription($h_stageSeq);
	
	$h_nextStepAction = "<input type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onclick=\"this.form.submit();\" value=\" {$Lang['IES']['Enter']} \" />";
	
	switch($h_stageSeq){
    case 1:
    default:
      $h_frameClass = "student_stage1";
      $h_content = <<<HTML
  <div style="padding-top:60px; width:700px; margin-left:auto; margin-right:auto">
    <div class="main_paper_top"><div><div></div></div></div>
    <div class="main_paper_left"><div class="main_paper_right">
      <div class="main_paper_body_left"><div class="main_paper_body_right"><div class="main_paper_body ">
        <div class="stage1_title"><span class="title">{$h_stageTitle}</span><span class="deadline">{$Lang['IES']['SubmissionDeadline']} : {$h_stageDeadline}</span><div class="clear"></div></div>
        <div class="stage1_paper"> <!-- class "stage1_paper" making a single line paper-->
          {$h_stageDesc}
          <div style="text-align:center">
            {$h_nextStepAction}
          </div>
        </div> <!--class "stage1_paper" end -->
      </div></div></div>
    </div></div>
    <div class="main_paper_bottom"><div><div></div></div></div>
  </div>
HTML;
      break;
    case 2:
      $h_frameClass = "student_stage2";
      $h_content = <<<HTML
  <div class="stage2_cover">
    <div class="stage2_title"><span class="title">{$h_stageTitle}</span><div class="clear"></div></div>
    <div class="cover_wrap"> <!-- added 25-06-2010 by Eli -->
      {$h_stageDesc}
    </div> <!-- added 25-06-2010 by Eli -->
    
    <div style="text-align:center">
      {$h_nextStepAction}
    </div>
  </div>
HTML;
      break;
    case 3:
    	$h_frameClass = "student_stage3";
      $h_content = <<<HTML
	<div class="stage3_cover">
		<div class="stage3_title"><span class="title">{$h_stageTitle}</span><div class="clear"></div></div>
		<div class="cover_wrap"> <!-- added 25-06-2010 by Eli -->
      {$h_stageDesc}
    </div> <!-- added 25-06-2010 by Eli -->
    
    <div style="text-align:center">
      {$h_nextStepAction}
    </div>
  </div>
HTML;
    	break;
  }
}

//$scheme_arr = $objIES->GET_SCHEME_ARR();
$scheme_arr = $objIES->GET_USER_SCHEME_ARR($UserID);
$html_scheme_selection = libies_ui::GET_SCHEME_SELECTION($scheme_arr, $scheme_id);

$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);
$html_stage_selection = libies_ui::GET_STAGE_SELECTION($stage_arr,$stage_id,$scheme_id);

### Title ###
$title = $ies_lang['IES_TITLE'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/coverpage.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>