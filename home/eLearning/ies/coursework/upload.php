<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
//intranet_auth();
intranet_opendb();

$libfilesystem = new libfilesystem();
$finalUploadResult = false;
$resultCreatedFolder = false;
if (!empty($_FILES)) {
	$targetPath = "/file/ies/scheme/scheme_s$scheme_id/u".$student_id."/";
	# create folder
	$targetFullPath = str_replace('//', '/', $intranet_root.$targetPath);
	if ( file_exists($targetFullPath) ) {
		$resultCreatedFolder = true;
	} else {
		$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
	}
	
	chmod($targetFullPath, 0755);
	
	# get file name
	$tempFile = $_FILES['submitFile']['tmp_name'];
	$origFileName =  $_FILES['submitFile']['name'];
	
	# encode file name
	$targetFileHashName = sha1($student_id.microtime());
	$targetFile = $targetFullPath.$targetFileHashName;
	
	# upload file
	$uploadResult = move_uploaded_file($tempFile, $targetFile);
	
	# add record to DB
	$libies = new libies();
	$IS_CREATE_IF_NO_CURRENT_BATCH = true;
	$batchId = $libies->getCurrentBatchId($stage_id,$student_id,$IS_CREATE_IF_NO_CURRENT_BATCH);
	$stage_student_id = $libies->updateStudentStageBatchHandinStatus($stage_id,$student_id,$ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["submitting"]);
	$file_id = $libies->handInFileToStage($origFileName,$targetPath,$targetFileHashName,$student_id,$batchId);
	
	$file_info = current($libies->getStageHandinFileInfo_byFileId($file_id));
	
	$finalUploadResult = $resultCreatedFolder && $uploadResult && $file_id;
}
intranet_closedb();   
sleep(1);
?>
<script language="javascript" type="text/javascript">window.top.window.stopUpload(<?=($finalUploadResult?1:0)?>, <?=$file_info["FILEID"]?>, "<?=$file_info["FILENAME"]?>", "<?=$file_info["FILEHASHNAME"]?>", "<?=$file_info["DATEMODIFIED"]?>");</script>   
