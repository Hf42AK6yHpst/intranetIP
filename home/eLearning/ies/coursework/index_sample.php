<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "StudentProgress";

$li = new libuser($UserID);
$objIES = new libies();

### Title ###
$title = $ies_lang['IES_TITLE'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
