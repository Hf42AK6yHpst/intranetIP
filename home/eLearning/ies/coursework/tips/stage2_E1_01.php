<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();


$ies_lang["b5"]["string1"] = "可以是受害人，也可以是受益人。";
$ies_lang["en"]["string1"] = "They can be victims or beneficiaries.";
$_string1 = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "對議題非常熟悉的專家，能分析事態發展、提出解決方法、發表有啟發性的個人意見。";
$ies_lang["en"]["string2"] = "They can be experts who are very familiar with the issue and can provide analysis of the development of the incident, solutions and inspirational advice.";
$_string2 = $ies_lang[$_lang]["string2"];


$ies_lang["b5"]["string3"] = "事件由個別人士或某個群體而起的，某些後果可能是他(們)造成的。";
$ies_lang["en"]["string3"] = "An incident can be initiated by an individual or a group.  Some of the consequences may be brought about by him / her / them.";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = " 有能力改變現狀的人，例如：傳媒，或有決策權的人。";
$ies_lang["en"]["string4"] = "They are people who can change the present situation, e.g. the media, decision-maker.";
$_string4 = $ies_lang[$_lang]["string4"];

$ies_lang["b5"]["string5"] = "通常是市民大眾，事件對他們未必會有直接影響，而他們也未必深入了解該事件，但出於不同原因而對議題表示關注，也願意發表個人見解。";
$ies_lang["en"]["string5"] = "They are usually the general public who may not be directly influenced by the incident and they may not know the incident too well. But because of certain reasons, they are concerned about it and are willing to share their views.";
$_string5 = $ies_lang[$_lang]["string5"];


$ies_lang["b5"]["string6"] = "直接受事件影響的人";
$ies_lang["en"]["string6"] = "Persons who are directly affected by the incident";
$_string6 = $ies_lang[$_lang]["string6"];

$ies_lang["b5"]["string7"] = "認識事件的人";
$ies_lang["en"]["string7"] = "Persons who know the incident well";
$_string7 = $ies_lang[$_lang]["string7"];


$ies_lang["b5"]["string8"] = "引發事件的人";
$ies_lang["en"]["string8"] = "Persons who initiate the incident";
$_string8 = $ies_lang[$_lang]["string8"];


$ies_lang["b5"]["string9"] = "對事件具影響力的人";
$ies_lang["en"]["string9"] = "Persons who can exert influence on the incident";
$_string9 = $ies_lang[$_lang]["string9"];

$ies_lang["b5"]["string10"] = "關心事件的人";
$ies_lang["en"]["string10"] = "Persons who care about the incident";
$_string10 = $ies_lang[$_lang]["string10"];


$ies_lang["b5"]["string11"] = "如何選擇訪問對象？";
$ies_lang["en"]["string11"] = "How to select interviewees?";
$_string11 = $ies_lang[$_lang]["string11"];



$six_w_config = array("w_who" => $_string1,
					  "w_where" => $_string2,
					  "w_why" => $_string3,
					  "w_what" => $_string4,
					  "w_when" => $_string5					  
				);
				
function get_six_w(){
	global $six_w_config;
	return $six_w_config;
}






$six_w_array = get_six_w();
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
<script>
var js_six_array = new Array();
var last = "w_who";

<?
foreach($six_w_array as $key => $value)
{
?>
	js_six_array['<?=$key?>'] = "<?=$value?>";
<?
}
unset($value);
?>

var six_w_title = {'w_who':'<?=$_string6?>', 
	'w_where':'<?=$_string7?>', 
	'w_why':'<?=$_string8?>', 
	'w_what':'<?=$_string9?>',
	'w_when':'<?=$_string10?>'
};

function sw_click(obj){
	id = $(obj).attr('id');
	if(last != id){
		$('#'+id).addClass(id+'_current');
		$('#'+last).removeClass(last+'_current');
		$(six_w_box).addClass(id+'_current');
		$(six_w_box).removeClass(last+'_current');
		last = id;
		$('.six_w_detail').html(js_six_array[id]);
		$('#six_w_title').html(six_w_title[id]);
	}

}

</script>


<div class="six_w_box five_box">
<h2><?=$_string11?></h2>
<a href="javascript:self.parent.tb_remove();" class="IES_tool_close" title="關閉">&nbsp;</a>
<div class="side_col six_w_list" style="margin-left:15px">
<a href="#" id="w_who" onclick="javascript:sw_click(this)" class="w_who_current" style="float:right; margin-top:27px"><span><?=$_string6?></span></a>
<a href="javascript:void(0);" id="w_where" onclick="javascript:sw_click(this)" style="float:right; margin-top:65px"><span><?=$_string7?></span></a><!-- add class="w_where_current" when clicked --> 
</div>

<div class="side_col six_w_list">
<a href="javascript:void(0);" id="w_why" onclick="javascript:sw_click(this)" style="float:right; margin-top:75px"><span><?=$_string8?></span></a>
</div>

<div class="side_col six_w_list" style="padding-left:45px; padding-bottom:30px">
<a href="javascript:void(0);" id="w_what" onclick="javascript:sw_click(this)" style="float:left; margin-top:27px"><span><?=$_string9?></span></a><!-- add class="w_what_current" when clicked --> 
<a href="javascript:void(0);" id="w_when" onclick="javascript:sw_click(this)" style="float:left; margin-top:65px"><span><?=$_string10?></span></a><!-- add class="w_when_current" when clicked --> 
</div>
<br class="clear" />

<div class="six_w_bottom">
<div class="six_w_list" style="float:left">
<a href="javascript:void(0);" id="six_w_box" class="w_who_current" style="float:left"><span id="six_w_title" style="font-size:15px;"><?=$_string6?></span></a>
</div>
<div class="six_w_talk">&nbsp;</div>
<div class="six_w_detail" style="font-size:15px;">
<?=$six_w_array["w_who"]?>
</div>
<div class="clear"></div>
</div>

</div> <!-- end of .six_w_box -->

<?
intranet_closedb();
?>