<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$ies_lang["b5"]["string1"] = "甚麼人？";
$ies_lang["en"]["string1"] = "Who?";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "甚麼地點？";
$ies_lang["en"]["string2"] = "Where?";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "甚麼原因？";
$ies_lang["en"]["string3"] = "Why?";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "甚麼事？";
$ies_lang["en"]["string4"] = "What?";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "甚麼時間？";
$ies_lang["en"]["string5"] = "When?";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "如何進行探究？";
$ies_lang["en"]["string6"] = "How?";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "六何法檢測你對你的研究題目認識有多深";
$ies_lang["en"]["string7"] = "Examine how well you understand your topic for enquiry with 6 Wh-questions";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/




if ($_lang == "en") {
	$six_w_config = array("w_who" => "What are the characteristics of the person(s) you would like to interview?<br/>How can you obtain valuable information from them? <br />What stakeholders are involved in the study? Are they difficult to locate?<br />Who are affected by the issue? Are they difficult to locate?<br />Which individuals or groups can affect the going of the issue? Is it hard to obtain information from them?<br/>The information needed can be obtained from these people, but are they easy to reach?",
						  "w_where" => "Where can observations on the enquiry phenomenon be done?<br \>What location(s)/region(s) is/are to be targeted at?<br \>If the study area is too large (e.g. China/Guangdong province), do you have the ability to obtain relevant and representative data?<br \>If the study area is too small (e.g. your school/form), is the value of the study high enough?<br \>If the same study is to be conducted in different areas, will the result be different?<br \>To bring the most benefits to the overall study, what location should your study target at?",
						  "w_why" => "What are the possible factors that affect the theme of enquiry?<br \>What factors are more explicit?<br \>What factors do you think are overlooked by most people?",
						  "w_what" => "What related incidents or issues does the study involve? Is it hard to obtain such background information?<br \>Are there any concrete incidents or examples in relation to the enquiry topic?  Is it hard to obtain such information?<br \>Is the incident involved a universal phenomenon or only an individual case?<br \>Does the incident involve any social meaning? Is it worth spending time on the enquiry?<br \>If the topic for enquiry is too uncommon, will it be hard to collect relevant information?<br \>Does the enquiry topic involve sensitive information which can hardly be obtained?",
						  "w_when" => "Will there be a fixed period of time (e.g. from the year 2008 to 2010) for the study to be conducted? Will it be too long?<br \>Will the enquiry topic change with time? Is there anything you have to pay attention to in the process of enquiry?<br \>Is the time element important to the study?<br \>Will the results of the two studies on the same topic be different, if one was conducted ten years ago and the other now? <br \>Will the factors affecting an incident change with time?",	
						  "w_how" => "What method(s) should be used to collect information?<br \>For research method, should we use qualitative method (e.g. in-depth interview) or quantitative method (e.g. survey)?<br \>What are the advantages and drawbacks of qualitative and quantitative methods of research?<br \>Do you have sufficient ability, time and resources to complete the research according to the selected method(s) and plan?"
					);
} else {
	$six_w_config = array("w_who" => "你想訪問的人有甚麼特點？如何在他們身上取得有價值的資料？<br />研究涉及甚麼持分者， 這些人難不難找? <br />問題影響哪些人， 這些關鍵人物難不難找?<br />有哪些人或群體可以影響事態發展，要從這些人身上找資料，難不難?<br />從哪些人身上可以拿到我們需要的資料，接觸這些受訪者容易嗎？",
						  "w_where" => "在甚麼地點能觀察到所研究的現象？<br \>研究範圍限制在哪些地點/地區？<br \>如果研究範圍太大（例如：中國、廣東省），你有能力去取得相關而有代表性的數據嗎？<br \>如果研究範圍太小（例如：你的學校、班級），研究價值高嗎？<br \>同一個研究題目，放在不同的地方去研究，結果會否不同？<br \>把焦點限制在哪個地點，對你整體研究最有利？ （例如︰沙田區、觀塘區）",
						  "w_why" => "有哪些因素影響這個研究主題？<br \>有哪些因素是比較明顯的？<br \>有哪些因素是你認為被大多數人忽略的？",
						  "w_what" => "探究涉及哪些相關事件或議題，這些議題的背景資料難不難找？<br \>有哪些具體事件或事例與研究題目相關，這些資料又難不難找？<br \>涉及的事件是社會普遍的現象，還是偶爾出現的個別事件？<br \>事件有沒有社會意義，是否值得花時間研究？<br \>如果研究的題目太冷門，尋找資料時會不會出現困難？<br \>探究話題是否涉及難以尋找的敏感資料？",
						  "w_when" => "研究有沒有一個固定的時期（例如︰2008年至2010年），時間是否太長？<br \>所探究的話題會否隨著時間而改變，在探究的過程有沒有需要注意的地方？<br \>研究的時間性重要嗎？<br \>同一個研究話題，十年前的結果與現在的會否有分別？<br \>影響某件事件的因素又會否隨著時間而改變？",	
						  "w_how" => "應採用甚麼方法來搜集資料？<br \>應採用質化（Qualitative）（例如︰深入訪談）或量化（Quantitative）（例如︰問卷調查）研究方法？<br \>採用質化／量化研究方法有何好處？有何不足之處？<br \>你有足夠的能力、時間和資源去完成你所採用的研究方法和計劃嗎？"
					);	
}
				
function get_six_w(){
	global $six_w_config;
	return $six_w_config;
}



$six_w_array = get_six_w();
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">



<script>
var js_six_array = new Array();
var last = "w_who";

<?
foreach($six_w_array as $key => $value)
{
?>
	js_six_array['<?=$key?>'] = "<?=$value?>";
<?
}
unset($value);
?>

var six_w_title = {'w_who':'<?=$ies_lang[$_lang]["string1"]?>', 
	'w_where':'<?=$ies_lang[$_lang]["string2"]?>', 
	'w_why':'<?=$ies_lang[$_lang]["string3"]?>', 
	'w_what':'<?=$ies_lang[$_lang]["string4"]?>',
	'w_when':'<?=$ies_lang[$_lang]["string5"]?>',
	'w_how':'<?=$ies_lang[$_lang]["string6"]?>'};

function sw_click(id){
	if(id != last){
		$('#'+id).addClass(id+'_current');
		$('#'+last).removeClass(last+'_current');
		$(six_w_box).addClass(id+'_current');
		$(six_w_box).removeClass(last+'_current');
		last = id;
		$('.six_w_detail').html(js_six_array[id]);
		$('#six_w_title').html(six_w_title[id]);
	}

}

</script>

<div class="six_w_box">
<h2><?=$ies_lang[$_lang]["string7"]?></h2>
<a href="javascript:self.parent.tb_remove();" class="IES_tool_close" title="<?=$Lang['IES']['Close']?>">&nbsp;</a>
<div class="side_col six_w_list" style="margin-left:15px">
<a href="#" id="w_who" onclick="javascript:sw_click(this.id)" class="w_who_current" style="float:right; margin-top:27px"><span style="font-size:13px;"><?=$ies_lang[$_lang]["string1"]?></span></a>
<a href="#" id="w_where" onclick="javascript:sw_click(this.id)" style="float:left; margin-top:65px"><span style="font-size:13px;"><?=$ies_lang[$_lang]["string2"]?></span></a><!-- add class="w_where_current" when clicked --> 
<a href="#" id="w_why" onclick="javascript:sw_click(this.id)" style="float:right; margin-top:65px"><span style="font-size:13px;"><?=$ies_lang[$_lang]["string3"]?></span></a><!-- add class="w_why_current" when clicked --> 
</div>

<div class="center_col">
&nbsp;</div>

<div class="side_col six_w_list">
<a href="#" id="w_what" onclick="javascript:sw_click(this.id)" style="float:left; margin-top:27px"><span style="font-size:13px;"><?=$ies_lang[$_lang]["string4"]?></span></a><!-- add class="w_what_current" when clicked --> 
<a href="#" id="w_when" onclick="javascript:sw_click(this.id)" style="float:right; margin-top:65px"><span style="font-size:13px;"><?=$ies_lang[$_lang]["string5"]?></span></a><!-- add class="w_when_current" when clicked --> 
<a href="#" id="w_how" onclick="javascript:sw_click(this.id)" style="float:left; margin-top:65px"><span style="font-size:13px;"><?=$ies_lang[$_lang]["string6"]?></span></a><!-- add class="w_how_current" when clicked --> 
</div>
<br class="clear" />

<div class="six_w_bottom">
<div class="six_w_list" style="float:left">
<a href="#" id="six_w_box" class="w_who_current" style="float:left"><span id="six_w_title" style="font-size:13px;"><?=$ies_lang[$_lang]["string1"]?></span></a>
</div>
<div class="six_w_talk">&nbsp;</div>
<div class="six_w_detail" style="font-size:13px;">
<?=$six_w_array["w_who"]?>
</div>
<div class="clear"></div>
</div>

</div> <!-- end of .six_w_box -->




<?
intranet_closedb();
?>