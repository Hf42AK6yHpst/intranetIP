<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "如何辨識模棱兩可的關鍵詞？";
$ies_lang["en"]["string1"] = "How to identify ambiguous key words?";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "模棱兩可的關鍵詞有以下特徵：";
$ies_lang["en"]["string2"] = "Ambiguous key words have the following features:";
$subtitle = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "特徵1";
$ies_lang["en"]["string3"] = "Feature 1";
$_string3 = $ies_lang[$_lang]["string3"];


$ies_lang["b5"]["string4"] = "特徵2";
$ies_lang["en"]["string4"] = "Feature 2";
$_string4 = $ies_lang[$_lang]["string4"];

$ies_lang["b5"]["string5"] = "特徵3";
$ies_lang["en"]["string5"] = "Feature 3";
$_string5 = $ies_lang[$_lang]["string5"];


if($_lang == "b5"){
$S0001_Answer = <<<hhtml
不同人對某些字詞可有不同理解。<br><br>
例如：<br>
某政策會否使「生活素質」提高？<br><br>
說明：<br>
問題提及的「生活素質」，不同人有不同理解，要清楚定義是哪方面的「生活素質」，討論才有意義。
hhtml;

$S0002_Answer = <<<hhtml
字詞包含的範圍太廣，有多重意義。<br><br>
例如：<br>
某政策能否提高我校的「教育質素」？<br><br>
說明：<br>
「教育質素」一詞範圍太廣，究竟是指「老師的專業水平」？ 「公開試成績和入大學率」？ 「OLE的多樣性」，還是「校園設施」？這些都需要在開始討論前界定清楚。
hhtml;

$S0003_Answer = <<<hhtml
沒有設定客觀標準，使結果難以驗證。<br><br>
例如：<br>
某政策是否「有效」？<br><br>
說明：<br>
除了要界定清楚甚麽為之「有效」，還要為「有效」定下一些可以量度與分析的客觀標準。<br>
hhtml;

}else{
$S0001_Answer = <<<hhtml
Different people have different interpretations of these words.<br><br>
Example:<br>
Will a certain policy improve ‘quality of life’?<br><br>
Explanation:<br>
Different people have different interpretations of ‘quality of life’. The aspect referred to should be well-defined for a meaningful discussion.
hhtml;

$S0002_Answer = <<<hhtml
These words have a wide scope and multiple meanings.<br><br>
Example:<br>
Will a certain policy improve the ‘quality of education’ of my school?<br><br>
Explanation:<br>
The scope of ‘quality of education’ is too wide. Does it refer to ‘teacher professionalism’? ‘Public examination results and university entrance rate’? ‘Diversity of OLE’ or ‘School facilities’? All these have to be well-defined at the start.
hhtml;

$S0003_Answer = <<<hhtml
No objective standards are available to verify the connotation of these words.<br><br>
Example:<br>
Is a certain policy ‘effective’?<br><br>
Explanation:<br>
Besides clearly defining ‘effective’, measurable and analytical objective standards have to be set.<br>
hhtml;

}
$S0001 = array($_string3, $S0001_Answer);
$S0002 = array($_string4, $S0002_Answer);
$S0003 = array($_string5, $S0003_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>