<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "量化研究方法簡介";
$ies_lang["en"]["string1"] = "What is quantitative research?";
$title = $ies_lang[$_lang]["string1"];



if($_lang == "b5"){
	$S0001 = array("甚麽是量化研究方法？", "<b>量化研究方法（例如：問卷調查）</b>是為了把所收集的數據量化處理，以獲得具代表性的結果。統一從問卷收集的大量資料，經過統計分析，用圖表看出結果，然後作出分析，最後能對某些事情下結論。而量化方法所得的是客觀的數據，方便統計。<br /><br />例如： 本問卷調查訪問了30人，其中有12人認同，18人不認同。或問卷結果顯示，70%被訪者認為……");

	$S0002 = array("如何做好量化研究？", "研究的質量，取決於訪問的對象是否夠代表性，回答人數是否足夠，以及題目本身的質量（例如：選項是否包含所有可能的答案、問題所得資料是否有助回答研究問題）");
			
	$S0003 = array("樣本數目要多少才足夠？", "如樣本數量太少，便難以得出具代表性的結果。樣本的多少也要視乎個別情況而定，很難一概而論，一般是多於30人，有些大型的研究更超過1000人，建議你請教你的指導老師。");

	$S0004 = array("量化研究的例子" , "<b>問卷調查 </b><br />透過邀請一定數量的被訪者回答一系列問題來獲得數據作為研究。問卷調查的型式可以是街頭訪問，邀請被訪者面對面回答，也可以是電話抽樣訪問，或是網上發送。<br /><br />很多同學會選擇街頭訪問，可是大多數人都可能會拒絕接受訪問，大家要有被拒絕的心理準備。街頭訪問的最大挑戰就是如何能使被訪者願意接受訪問。<br /><br />至於網上問卷，由於我們無法監管回答的質量，所以大家最好收集足夠數量的樣本，以增加資料的可靠性。");
}else{
	$S0001 = array("What is a quantitative research method?", "A quantitative research method helps quantify the data collected and obtain representative results. Survey is one example. The research process goes like this: First, standardise a huge amount of data collected from questionnaires. After statistical analyses, present the results with graphs and charts. Lastly, make analysis and conclusion for the issue. The data collected from quantitative research is objective and easy for statistical follow-up.<br /><br />Here are some examples: 30 people have conducted the survey, in which 12 agree and 18 disagree. The results of the survey show that 70% of the respondents think that…");

	$S0002 = array("How to conduct quality quantitative research?", "The factors determining the research quality are the representativeness of the research objects, the size of respondents, and the quality of the questions, like whether the items have included all possible answers, and whether the data corresponding to the questions set in the questionnaires can help answer the research question. ");
			
	$S0003 = array("How big should the sample size be?", "If the sample size is too small, it will be difficult to obtain representative results. The sample size varies in different cases. In general, it will be more than 30. But in some large-scale research, it could be more than 1000. Please consult your supervising teachers.");

	$S0004 = array("Examples of quantitative research" , "<b>Survey</b><br />Survey is a quantitative research method by which data is collected by inviting a considerable number of respondents to answer a series of questions. Survey could be done online, or in the form of a face-to-face interview on the street and a telephone interview by random sampling.<br /><br />A lot of students prefer doing the survey on the street but are very likely to be rejected by most people. Get yourself prepared psychologically. The greatest challenge for this type of survey is how to make the interviewee feel like doing the survey.<br /><br />As we cannot monitor the quality of the answers for online survey, it would be best if we could gather enough samples to enhance data reliability.");
}
##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,
				   "S0004" => $S0004,	
			);
$html_class = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>