<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "各種常用研究方法的利弊";
$ies_lang["en"]["string1"] = "The Pros and Cons of Common Research Methods";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "實地考察";
$ies_lang["en"]["string2"] = "Field observation";
$_string2 = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "深入訪問";
$ies_lang["en"]["string3"] = "In-depth interview";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "網上問卷";
$ies_lang["en"]["string4"] = "Online questionnaire";
$_string4 = $ies_lang[$_lang]["string4"];



//$subtitle = "XXXXXXXXXXXXXXX";
if($_lang == "b5"){
	$S0001_Answer = <<<hhtml
<strong>好處：</strong>
<ul style="margin-top:0">
<li>沒有人為干擾或引導，現象自然發生，有客觀現象可供觀察。</li>
<li>親歷其境，具真實感的體驗，可能得出新觀點。</li>
<li>可以觀察一些非言語能表達的潛意識行為 (例如：觀察排隊的現象、觀察遊行人士的舉動）。</li>
</ul>
<strong>壞處/局限：</strong>
<ul style="margin-top:0">
<li>人為觀察，難免涉及主觀判斷 / 容易出現過度主觀的分析。</li>
<li>容易出現選擇性觀察和選擇性分析 （例如：只觀察和記錄你認為對解答研究問題有用的東西，故意忽略或不記錄某些與立場不乎的現象）。</li>
<li>安全性問題 （例如：觀察示威活動，因安全理由，不宜參與其中）。</li>
<li>觀察的時間可能會很長，而且不保證有預期的結果。</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
<strong>好處：</strong>
<ul style="margin-top:0">
<li>可以深入探究某個行為和觀點背後的動機和理據。</li>
<li>可以發問跟進問題，不像問卷般有固定的答題模式，可以更深入了解問題。</li>
<li>研究對象的發揮空間大，可以探究更廣闊的話題。</li>
<li>相比焦點小組，深入訪問比較適合問一些較私人或敏感的問題 （例如：被訪者對性的看法或對吸毒的看法）。</li>
<li>適合一些複雜的議題，有助了解各持分者對議題的不同觀點。</li>
</ul>
<strong>壞處/局限：</strong>
<ul style="margin-top:0">
<li>相對網上問卷或街頭訪問，比較花時間。訪問員需要做好事前準備和事後功夫，例如：預約、分析研究對象的背景、訪問、記錄、分析整合，步驟比較繁複。</li>
<li>關鍵人物可能會拒絕訪問，臨時爽約，或不願意提供某些關鍵資料，所以風險比較高。</li>
<li>因為沒有客觀數字輔助，所得的資料難以分析比較。</li>
<li>對訪問者的訪問技巧要求非常高（例如：引導對方思考、引導對方提供更深入的意見）。</li>
</ul>
hhtml;

$S0003_Answer = <<<hhtml
<strong>好處：</strong>
<ul style="margin-top:0">
<li>方便而低成本，可以大規模發放。</li>
<li>發放問卷的速度快，資料比較容易管理。</li>
<li>容易分析數據，省去了輸入所得數據的時間。</li>
<li>相對其他研究方法，可以獲得大量數據。</li>
<li>問卷通常使用客觀性題目（例如：選擇題、是非題），可以大規模統一收集某種數據。不像深入訪問般，得不到統一類型的數據。</li>
</ul>
<strong>壞處/局限：</strong>
<ul style="margin-top:0">
<li>難以控制樣本和回答質量（難以知道回答的是甚麼人）。</li>
<li>被訪者通常缺乏誘因去回答，回應比率相對較低。</li>
<li>網上問卷沒有面對面回答的壓力，難以確定被訪者是否認真回答，數據可靠性可能偏低，只能靠增加樣本數目減低其影響。</li>
<li>問及敏感或私人話題時，被訪者通常不會誠實回答問卷，難以像深入訪問般問及深入的問題或探討問題的本源。</li>
</ul>
hhtml;
}else{
$S0001_Answer = <<<hhtml
<strong>Pros:</strong>
<ul style="margin-top:0">
<li>Things go naturally without man-made interference or guidance. Objective phenomena can be observed.</li>
<li>New insights emerge when observers truly experience what is going on at the scene.</li>
<li>Non-verbal subconscious behaviour, e.g. the phenomenon of queuing or the behaviour of demonstrators/protestors, can be observed.</li>
</ul>
<strong>Cons:</strong>
<ul style="margin-top:0">
<li>There is an element of subjectivity in man-made observation which may lead to too subjective analysis.</li>
<li>There may be selective observation and analysis. For instance, you may intentionally ignore or not record the phenomenon against your stance, but you only observe and record what you think is crucial to answer the research question.</li>
<li>Safety reason: For example, you want to observe demonstrations. But for safety reasons, you should not participate in them.</li>
<li>It may take quite a long time to conduct observations but an expected outcome is not guaranteed.</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
<strong>Pros:</strong>
<ul style="margin-top:0">
<li>You can further enquire into the motives and justifications behind a particular form of behaviour and viewpoint. </li>
<li>Unlike questionnaires where the questions are already set, you can ask follow-up questions in the interview to better understand the issue.</li>
<li>Interviewees are free to elaborate their ideas which may open up more topics for discussion. </li>
<li>Compared to focus group, in-depth interview is more suitable for asking more private or sensitive questions. For example, interviewees will be asked about what they think towards sex or drug abuse.</li>
<li>It is suitable for discussing complicated issues where viewpoints of different stakeholders are shared and learnt.</li>
</ul>
<strong>Cons:</strong>
<ul style="margin-top:0">
<li>The process is more time-consuming, compared to that of online questionnaires or face-to-face interviews on the street. Interviewers should get a lot of things done before and after the interview. They should make an appointment with the interviewees, analyse interviewees’ backgrounds, do the interviews, record the details, analyse and organise the data collected. These steps are a bit complicated.</li>
<li>Interviewees may refuse to be interviewed, not show up, or not be willing to provide crucial information. So, there is a higher risk for such a method.</li>
<li>As statistical data is not available for such a method, it will be harder to do comparison and analysis. </li>
<li>It requires brilliant interview skills. For example, interviewers have to guide the interviewees to think and provide more in-depth ideas.</li>
</ul>
hhtml;

$S0003_Answer = <<<hhtml
<strong>Pros:</strong>
<ul style="margin-top:0">
<li>Convenient and cost effective. Questionnaires can be sent on a large-scale.</li>
<li>Speedy distribution of questionnaires enables easy management of data.</li>
<li>Saves time for data entry. Data can be analysed at ease.</li>
<li>Compared to other research methods, online questionnaire collects a relatively large amount of data.</li>
<li>The questions set in a questionnaire are usually objective, e.g. multiple choice and true or false questions, and hence a particular type of data can be collected on a large scale. This is totally different from in-depth interview, in which data collected cannot be standardised.</li>
</ul>
<strong>Cons:</strong>
<ul style="margin-top:0">
<li>Sample size and quality of responses can hardly be monitored. You will not know who the respondent is. </li>
<li>Respondents usually lack incentive in answering the questions. Therefore, the response rate is relatively low.</li>
<li>As online questionnaires pose no face-to-face pressure for respondents, it is hard to tell whether respondents are serious enough about answering the questions. Thus, the data authenticity is rather low. Expanding the sample size would help minimize these drawbacks.</li>
<li>Respondents will not provide a real answer for sensitive or private questions. Therefore, it will be hard to ask in-depth questions or enquire into the origins of the issues, as what is done in in-depth interviews.</li>
</ul>
hhtml;
}
$S0001 = array($_string2, $S0001_Answer);

$S0002 = array($_string3, $S0002_Answer);
		
$S0003 = array($_string4 , $S0003_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,	
			);
$html_class = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>
