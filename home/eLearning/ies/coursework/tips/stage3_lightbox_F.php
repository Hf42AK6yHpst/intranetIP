<?php
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();

$ies_lang["b5"]["string1"] = "如何在報告中加入圖表？";
$ies_lang["en"]["string1"] = "How to include graphs in the report?";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "如果你選用eClass通識獨立專題探究平台發放問卷並收集數據，可以結合MS Excel產生統計圖表。方法如下：";
$ies_lang["en"]["string2"] = "If you use the LSIES platform in eClass to deliver questionnaires and collect data, you can use MS Excel to create graphs as follows:";
$subtitle = $ies_lang[$_lang]["string2"];

if($_lang == "b5"){
$S0001_Answer = <<<hhtml
<div align="center">
	<img align="absmiddle" src="/images/2009a/ies/student/excel_sample_{$_lang}.jpg"><br>
</div>
<ul> 
	<li>在eClass 通識獨立專題探究平台上收集問卷數據。</li>
	<li>選取所需要的變項(Variables)。</li>
 	<li>產生數據列表。</li>
 	<li>把整個數據列表複製到MS Excel並貼上。</li>
 	<li>選取整個數據列表(包括標題)。</li>
 	<li>從工具列選取「插入」。</li>
 	<li>選擇適當的圖表。</li>
 	<li>調整圖表的外觀，例如：標題(Title)、標示(Key)。</li>
 	<li>複製並貼在MS Word檔案中。</li>
</ul>
<br>
hhtml;
}else{
$S0001_Answer = <<<hhtml
<div align="center">
	<img align="absmiddle" src="/images/2009a/ies/student/excel_sample_{$_lang}.jpg"><br>
</div>
<ul> 
	<li>Collect the survey data from the LSIES platform in eClass.</li>
	<li>Select all the variables required.</li>
 	<li>Create a data table.</li>
 	<li>Copy and paste the data table on MS Excel.</li>
 	<li>Select the whole data table (including the heading).</li>
 	<li>Select ‘insert’ from the tool bar.</li>
 	<li>Select the suitable graph template.</li>
 	<li>Format the graph, e.g. title, key.</li>
 	<li>Copy and paste it on the MS Word file.</li>
</ul>
<br>
hhtml;
}

intranet_closedb();
?>
<?
//================================ Content ==================================
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/IES.js"></script>

<script>
var last = "";

function reheight(){
	var IE = /*@cc_on!@*/false;
	var WholeHeight = $(document).height();
	var topHeight = $("#top_content").outerHeight();
	var top = (!IE) ? parseInt($("#show_nr").css("margin-top")) : parseInt($("#show_nr").css("margin-top")) * -1;
	var padding = (!IE) ? parseInt($("#show_nr").css("padding-top")) +  parseInt($("#show_nr").css("padding-bottom")): 0;
	//alert('WholeHeight = '+WholeHeight+'topHeight='+topHeight);
	var theHeight = WholeHeight - topHeight - top - padding;
	return theHeight;
}


$(document).ready(function() {
	
	
    $('.sw_click').click(function(){
    	if(last == ""){
    		var bottomHeight = reheight();
			$('#show_nr').css('height', bottomHeight);
    	}
    	if(last != ""){
    		$('#' + last).removeClass(last + '_current');
    	}
    	last = $(this).attr('id');
    	$(this).addClass($(this).attr('id') + '_current');
    	var content= '';
    	<?
    	if(is_array($nr_array)){
    		foreach($nr_array as $key=>$value){
    			//addslashes(nl2br($value[1]))
    			$w_string_array = explode("\n", $value[1]);
    			$w_string = "";
    			if(is_array($w_string_array)){
    				foreach($w_string_array as $a_string){
    										
    					$w_string .= trim($a_string);
    				}
    				
    			}
    			echo "if(last == '{$key}'){";
    			echo "content = \"".addslashes($w_string)."\"; }";
    		}
    	}
    	?>
    	
    	$('#show_nr').html(content);
    	
    });
});
</script>
<style>
.five_example .narrow_sample  {width: 17%}
</style>


<body style="overflow:hidden;">

<div class="narrow_range_box two_example"> <!-- add class three_example -->
	<div id='top_content'>
		<h2><?=$title?></h2>	
		<a href="javascript:self.parent.tb_remove();" class="IES_tool_close" title="關閉">&nbsp;</a>
		<h3><?=$subtitle?></h3>
		<div class="clear"></div>
	</div>
	<div class="narrow_range_bottom" id="show_nr" style="text-align:left; padding-left:35px; font-size:15px; height:400px">
		<?=$S0001_Answer?>
	</div>
	<div class="clear"></div>
	
</div> <!-- end of .narrow_range_box -->   
</body>
