<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "引用文獻注意事項";
$ies_lang["en"]["string1"] = "Citation guide";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "尋找網上資料";
$ies_lang["en"]["string2"] = "Finding information on the Internet";
$_string2 = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "引用書籍資料";
$ies_lang["en"]["string3"] = "Citing book(s)";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "引用報章資料";
$ies_lang["en"]["string4"] = "Citing newspaper(s)";
$_string4 = $ies_lang[$_lang]["string4"];


if($_lang == "b5"){
$S0001_Answer = <<<hhtml
<ul>
	<li>作者或發表機構有沒有權威性？</li>
	<li>與探究題目有甚麼關係？</li>
	<li>資料談及的是事實，還是意見？</li>
	<li>文章有沒有商業目的或其他宣傳目的，例如是廣告或作者受到某些商業利益影響？</li>
	<li>文章有沒有可靠的理據支持？</li>
	<li>文章是甚麽時候寫的，資料會否過時？</li>
	<li>作者有沒有嘗試誇大某些觀點，或故意忽略某些反方論點？</li>
	<li>網站是否權威，例如官方網站或個人博客？</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
<ul>
	<li>看看簡介和大綱，確定文章的重點。</li>
	<li>到網上書店尋找該書的網上書評，看看其他讀者對此書的評論。</li>
	<li>檢查該書籍或學術文章背後的參考資料，看看作者是否有足夠的資料搜集。</li>
	<li>檢視書籍提供的資料，是否有助你回應焦點問題。</li>
	<li>避免浪費時間大量影印，應只摘錄有用的文章或重點。</li>
	<li>要記錄好書名、作者資料、出版社、出版年份等資料，方便製作參考書目。</li>
</ul>
hhtml;

$S0003_Answer = <<<hhtml
<ul>
	<li>引用時要辨識清楚事實與意見。</li>
	<li>不同報章可能會有不同立場，對同一事件的理解也不一樣，所以要比較多份報章，否則論據便不夠客觀。</li>
	<li>留意資料的可信性，有否偏頗，或選擇性披露某些資料。</li>
</ul>
hhtml;

}else{
$S0001_Answer = <<<hhtml
<ul>
	<li>Is the Internet source authoritative?</li>
	<li>How is it related to the enquiry topic?</li>
	<li>Is the information fact or opinion?</li>
	<li>Does the source have any business or publicity purposes, e.g. is the article an advertisement or related to commercial benefits?</li>
	<li>Is there any reliable justification for the article?</li>
	<li>When was the article written? Could the information be out-dated?</li>
	<li>Does the writer tend to exaggerate certain viewpoints, or ignore some counter-arguments deliberately?</li>
	<li>Is the website authoritative, e.g. an official website or a blog?</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
<ul>
	<li>Preview the blurb and outline to confirm the main points of the articles.</li>
	<li>Look up Internet reviews on the book(s) to check readers’ evaluations.</li>
	<li>Examine the references of the book(s) or literature to ensure sufficient data collection has been done by the author.</li>
	<li>Examine whether the information provided in the book(s) can address the focus questions of your enquiry.</li>
	<li>Make extracts from useful articles only to avoid excessive photocopying.</li>
	<li>Record the book title, author, publisher and year of publication accurately for the bibliography of your enquiry report.</li>
</ul>
hhtml;

$S0003_Answer = <<<hhtml
<ul>
	<li>Distinguish facts from opinions clearly when citations are made.</li>
	<li>Newspapers’ stances and interpretations on the same event may vary. Therefore, information from different newspapers should be compared to ensure objectivity of the arguments.</li>
	<li>Take notice of the reliability of information to ensure it is not selective disclosure with bias.</li>
</ul>
hhtml;

}
$S0001 = array($_string2, $S0001_Answer);
$S0002 = array($_string3, $S0002_Answer);
$S0003 = array($_string4, $S0003_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>