<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "如何撰寫高質量的建議？";
$ies_lang["en"]["string1"] = "How to write up high-quality recommendations?";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "從不同持分者的角度出發";
$ies_lang["en"]["string2"] = "With respect to the angles of various stakeholders";
$_string2 = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "從建議的本質出發";
$ies_lang["en"]["string3"] = "With respect to the nature of the recommendations";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "把建議寫得具體和細緻";
$ies_lang["en"]["string4"] = "Write up the recommendations concretely and precisely";
$_string4 = $ies_lang[$_lang]["string4"];

if($_lang == "b5"){
$S0001_Answer = <<<hhtml
一個問題發生，背後可能牽連很多持分者，不同持分者在事件中也可能有不同的角色、權利與責任。 當我們寫建議的時候，可以從持分者角度出發，建議不同的持分者如何共同協助解決問題，而不是把責任歸咎在其中一兩個持分者身上。 
hhtml;

$S0002_Answer = <<<hhtml
我們可以把建議分為短期措施與長期措施。短期措施是解決當務之急，可以減低問題加劇的可能性或暫時舒緩問題，但未必能直達問題根源。長期措施旨在解決問題根源，但通常需要一段長時間或不斷投入資源才能見效。同學如能結合短期與長期措施，可使建議更全面和有意義。
hhtml;

$S0003_Answer = <<<hhtml
同學要盡量把建議寫得具體仔細。在構思過程中不斷問自己一些「六何問題」（例如：怎樣……？甚麼……？哪裡……？誰人……？為何重要……？）。不斷刺激自己思維，把建議寫得更具體和更深入，加強說服力。
hhtml;

}else{
$S0001_Answer = <<<hhtml
Any issue may involve many stakeholders who may have different roles, rights and responsibilities.
We can write up recommendations with respect to the angles of various stakeholders, and suggest how they can solve problems through collaboration, instead of imputing blame on one or two stakeholders. 
hhtml;

$S0002_Answer = <<<hhtml
We can classify the recommendations into short-term and long-term policies.
Short-term policies can solve a burning issue, minimise the aggravation of the problem or ease the problem temporarily, but it cannot reach the roots.
Long-term policies can treat a problem at its roots, but effect can only be seen after a long period of time, or resources have to be allocated continuously.
If you can combine both short-term and long-term policies, your recommendations will be more comprehensive and meaningful.
hhtml;

$S0003_Answer = <<<hhtml
You should write up the recommendations as specifically as possible.
Use the ‘6Ws’ in the course of writing (e.g. How…? What…? Where…? Who…? When…? Why so important…?) to stimulate your thinking, and write up the convincing recommendations concretely and thoroughly.
hhtml;

}
$S0001 = array($_string2, $S0001_Answer);
$S0002 = array($_string3, $S0002_Answer);
$S0003 = array($_string4, $S0003_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>