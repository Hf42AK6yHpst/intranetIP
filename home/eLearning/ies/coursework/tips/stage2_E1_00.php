<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "訪問流程";
$ies_lang["en"]["string1"] = "The course of an interview";

$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "題目：調查青少年使用網上社交友網站 Facebook 的原因和情況";
$ies_lang["en"]["string2"] = "Examine how and why teenagers use Facebook, the social networking site.";

$subtitle = $ies_lang[$_lang]["string2"];

if($_lang == "b5"){
$S0001_Answer = <<<hhtml
感謝與歡迎 <br />
<ol>
<li>簡述是次訪問的目的</li>
<li>簡述是次訪問流程</li>
<li>提醒受訪者該訪問將會錄音或錄影</li>
<li>澄清受訪者的問題</li>
</ol>
例子：<br />
<ul>
<li>今天的訪問是為了了解年輕人使用 Facebook 的原因和情況。</li>
<li>訪問的流程如下：首先，我會問關於……的問題，然後再談……，最後……</li>
<li>答案沒有對與錯之分，只是想聽聽你的意見，所以不用太拘緊。</li>
<li>提提你，訪問的過程將會錄音或錄影。 </li>
<li>在訪問之前，你有沒有甚麽疑問或不清楚的地方？</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
重點： <br />
<ol>
<li>了解受訪者使用Facebook的情況</li>
<li>了解受訪者使用Facebook的原因</li>
</ol>
<b>重點1的例子：</b><br />

首先，我會問有關你個人使用Facebook的情況。 <br /><br />
<b><u>一般問題</u></b>
<ul>
<li>你平日一星期上多少次Facebook？</li>
<li>每次花多少時間？</li>
<li>通常是甚麼時段上Facebook？</li>
<li>上Facebook是不是一種習慣？這種習慣形成了多久？ </li>
<li>你通常上Facebook做些甚麽？（例如：交朋友、聯絡朋友、玩遊戲、分享資訊）</li>
<li>與其他交友網站比較，Facebook如何吸引你？</li><br />
</ul>
<b><u>深入探討</u></b><br />

那麼…… 
<ul>
<li>如果有人禁止你上Facebook一個星期，你覺得你的世界會有甚麼不同？ </li>
<li>如果你的手機有上網功能，你會否用手機上Facebook？ </li><br />
</ul>

<br />
<b>重點2的例子：</b><br />

接下來，我會問有關你個人使用Facebook的原因： <br /><br />
<b><u>一般問題</u></b>
<ul>
<li>你認為有甚麼原因吸引你上Facebook？（朋友？Facebook的功能和特性？遊戲？） </li>
<li>如果你的朋友不再使用Facebook，你會否依然會上Facebook？ </li>
<li>你覺得為甚麼你身邊的人會喜歡上Facebook？ </li>
<li>Facebook能否有效地幫助你增進同儕之間的友誼？ </li>
</ul>
<b><u>深入探討</u></b><br />
哪個原因的吸引力最大？為什麼？ <br /> <br />

提示：訪問時，多問一些跟進問題，獲得更深入資料。
hhtml;

$S0003_Answer = <<<hhtml
多謝你寶貴的意見！
hhtml;
}else{
$S0001_Answer = <<<hhtml
Thank you and welcome<br />
<ol>
<li>State the objective(s) of the interview</li>
<li>State rundown of the interview</li>
<li>Remind the interviewee that the interview will be recorded.</li>
<li>Clarify the interviewee’s questions.</li>
</ol>
Example:<br />
<ul>
<li>The objectives of today’s interview are to learn about how and why teenagers use Facebook. </li>
<li>The rundown will be as follows: first, I’ll ask you something about…, then…, and lastly….</li>
<li>There are no model answers. I just want to know what you think. So, feel free to speak.</li>
<li>Please be reminded that the process will be recorded.</li>
<li>Before we start, is there anything you are not sure of?</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
Focus:<br />
<ol>
<li>To learn about how the interviewee uses Facebook</li>
<li>To learn about why the interviewee uses Facebook</li>
</ol>
<b>Focus 1 example:</b><br />

First, I’ll ask about how you use Facebook.<br /><br />
<b><u>General questions</u></b>
<ul>
<li>How many times do you usually use Facebook each week?</li>
<li>How long do you use it each time?</li>
<li>When do you usually use it?</li>
<li>Is using Facebook your habit? How long did it take you to form this habit?</li>
<li>What do you usually do on Facebook? (Let’s say, make friends? Connect friends? Play games? Share information?)</li>
<li>Compared to the other social networking sites, in what way is Facebook fascinating to you?</li><br />
</ul>
<b><u>Further questions</u></b><br />

So…
<ul>
<li>If somebody doesn’t allow you to use Facebook for a week, in what way do you think your life will be different?</li>
<li>If your mobile phone can connect to the Internet, will you use it to access Facebook?</li><br />
</ul>

<br />
<b>Focus 2 example:</b><br />

In the following part, I’ll ask about why you use Facebook.<br /><br />
<b><u>General questions</u></b>
<ul>
<li>What are the reasons that attract you to use Facebook? (Friends? The functions and characteristics of Facebook itself? Games?)</li>
<li>If your friends no longer use Facebook, will you still use it?</li>
<li>In your opinion, why do people with connections to you like using Facebook?</li>
<li>Can Facebook help enhance friendship effectively?</li>
</ul>
<b><u>Further questions</u></b><br />
What is the greatest reason that pushes you to use Facebook? Why?<br /> <br />

Hint: Raise more follow-up questions in interviews in order to collect further information.

hhtml;

$S0003_Answer = <<<hhtml
Thanks for sharing with me.
hhtml;
}


$ies_lang["b5"]["string3"] = "第一部分：正式訪問之前";
$ies_lang["en"]["string3"] = "Part 1: Before the interview";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "第二部分：訪問內容";
$ies_lang["en"]["string4"] = "Part 2: Interview";
$_string4 = $ies_lang[$_lang]["string4"];

$ies_lang["b5"]["string5"] = "第三部分：完結";
$ies_lang["en"]["string5"] = "Part 3: End of interview";
$_string5 = $ies_lang[$_lang]["string5"];



$S0001 = array($_string3, $S0001_Answer);
$S0002 = array($_string4, $S0002_Answer);		
$S0003 = array($_string5, $S0003_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,	
			);
$html_class = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>