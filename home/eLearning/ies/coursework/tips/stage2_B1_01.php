<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "質化研究方法簡介";
$ies_lang["en"]["string1"] = "Introduction to Qualitative Method";
$title = $ies_lang[$_lang]["string1"];


if($_lang == "b5"){
	$S0001 = array("甚麽是質化研究方法？" , "質化研究方法（例如：深入訪問和實地考察）常用以探索問題背後的深層原因，或一個觀點背後的理據，甚至透過與受訪者直接交流或觀察，而推斷某種行為和想法背後的動機。 ");

	$S0002 = array("相比量化方法，質化方法有何好處？", "質化方法所得到的資料性質與量化研究(例如：問卷訪問)不同。質化研究方法在收集資料的形式上比較自由。以深入訪問為例，訪問員不必被問卷的問題限制發問的次序和內容。同時，他們也能因應被訪者的回應，問一些較深入的問題，直至得到有用的資料。至於實地考察，研究員可以透過觀察一些客觀現象（例如：遊行集會），理解和推斷研究對象的行為和動機，這些資料都是不能靠問卷獲得的。");
			
	$S0003 = array("如何做好質化研究？" , "要從質化研究收集到高質量的數據，過程中一定要抓穩研究目的，例如：不要讓訪問脫離探究焦點，或漫無目的進行觀察。而且，由於質化研究過程中容易出現頗多變數，同學們要注意臨場應變，例如：在訪問中要善於問跟進問題，適當引導對方思考，才能獲得所需的資料。");

	$S0004 = array("質化研究的例子" ,"<b>深入訪問 </b><br />透過一對一訪問，我們可以深入理解研究對象就某些觀點所持的理由和價值觀，有時甚至聽到他們的個人經歷，這些資料都有助我們深入分析議題，例如：我們可以訪問大澳居民對政府活化大澳計劃的看法，當中可能涉及一些多角度的觀點。一般問卷調查有固定題目，局限了回應的深度。透過深入訪問，獲得的答案一定比問卷調查所得的更深入。<br /><br /><b>實地考察</b><br \>研究員到現場觀察，了解在實際環境下人們的行為，並記錄在觀察過程中的所見所聞，再分析其背後原因。記錄的方法有很多，包括筆錄、拍照、錄影，但記錄一定要完整，方便老師查閱。<br \><br \>例如：同學如有興趣研究香港學生的補習文化，可以在情況許可下，到補習社內外實地觀察其他同學報讀的行為和觀察他們決策的過程。但同學要注意所觀察的地點是否需要先獲得負責人批准，尤其是私人地方。<br /><br />記錄時要保持客觀，建議直接描述所見所聞，不應加插個人意見，影響資料的中立性。");
}else{
	$S0001 = array("What is a qualitative research method?" , "A qualitative research method is adopted to explore the underlying reasons of a problem or the rationale behind a viewpoint. Motives influencing certain behaviour or thinking can be deduced from observation or direct interaction with interviewees. Examples of qualitative research are in-depth interview and field observation.");

	$S0002 = array("In comparison with the quantitative method, what are the strengths of the qualitative method?", "The nature of the data collection method for qualitative and quantitative research (e.g. survey) is different. Qualitative research adopts a more open form of data collection method. Take in-depth interview as an example. Interviewers are not restricted to the set order of the questions or the question itself, but they can dig deeper into the issue based on the instant response of the interviewees until they obtain some useful information. As for field observation, researchers can understand and deduce the behaviour and motives of the research objects through observing some objective phenomena like march and assembly. Such information cannot be obtained by survey.");
			
	$S0003 = array("How to conduct quality qualitative research?" , "To collect quality data from qualitative research, you have to grasp the research objectives throughout the process. For example, do not let the interview deviate from the enquiry focuses or conduct the observation aimlessly. Besides, with so many variables in the qualitative research process, you have to be very responsive in dealing with contingencies. Skilfully posing follow-up questions during the interview will provide the interviewee with the appropriate guidance for thought, so that useful data could be collected.");

	$S0004 = array("Examples of qualitative research" ,"<b>In-depth interview</b><br />Through a one-to-one interview, we not only know the viewpoint the research object holds but also better understand his/her rationale and values. We could even hear about their personal experience sometimes. These data will enable us to conduct an in-depth analysis of the issue. For example, after interviewing Tai O residents about their views on the government’s revitalisation project, we might have collected some multi-perspective viewpoints. As there are set questions for survey in general, the profundity of the responses is limited. Therefore, the responses gathered from in-depth interviews should be more thorough than those from surveys.<br /><br /><b>Field observation</b><br \>The researcher conducts the observation onsite to learn more about the human behaviour in reality. During the process, he/she has to record all the observations for the rationale analysis later. There are many ways to record the details, including note taking, photo and video shooting. Make sure the record is comprehensive for teacher’s review.<br \><br \>For example, you are interested in studying the culture of Hong Kong students attending tutorial classes. If the situation allows, you may visit a tutorial centre to observe the behaviour of the students who are enrolling in courses and their decision-making process, from both inside and outside the centre. Make sure to get approval from the person-in-charge before the visit, especially for private premises.<br /><br />Be objective when keeping the record. In order not to affect the neutrality of the data collected, you are advised to describe all observations directly instead of putting forward your own opinions.<br/><br/>");
}

$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,
				   "S0004" => $S0004,	
			);
			
$html_class = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";			

intranet_closedb();
?>