<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$ies_lang["b5"]["string1"] = "收窄探究範圍的例子";
$ies_lang["en"]["string1"] = "How to narrow down the scope of enquiry?";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "請看看以下例子，理解如何從一個廣闊的範圍收窄成一個有意義的焦點︰";
$ies_lang["en"]["string2"] = "Please refer to the 'Exemplars' below to find out how a wide scope can be narrowed down to a meaningful focus for enquiry.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "教育";
$ies_lang["en"]["string3"] = "Education";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "日本文化";
$ies_lang["en"]["string4"] = "Japanese Culture";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "Facebook";
$ies_lang["en"]["string5"] = "Facebook";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "世博";
$ies_lang["en"]["string6"] = "Expo 2010 Shanghai China";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

if ($_lang == "en") {
	$S0001 = array(
			array("Education","(There are many educational issues. Which facet of education?)"),
			array("Education system","(Which sector of the education system?)"),
			array("Implementation of the New Senior Secondary (NSS) curriculum","(Which part of the NSS curriculum?)"),
			array("Implementation of NSS Liberal Studies","(Who is the target of enquiry?)"),
			array("Implementation of NSS Liberal Studies in my school","(Do you need to narrow down the scope of the enquiry target?)"),
			array("Implementation of NSS Liberal Studies in SS1 in my school","(Can you set a time frame?)"),
			array("Implementation of NSS Liberal Studies in SS1 in my school in the first year",""),
			);
	
	$S0002 = array(
			array("Japanese Culture","(Under the broad issue of Japanese culture, which area would you like to examine?)"),
			array("Japanese popular culture","(Which aspect of Japanese popular culture would you like to examine, its changes, development or impact?)"),
			array("Impact of Japanese popular culture","(Who are influenced by Japanese popular culture?)"),
			array("Impact of Japanese popular culture on teenagers","(Do you need to narrow down the scope of the enquiry target?)"),
			array("Impact of Japanese popular culture on teenagers in Hong Kong","(What kind of impact does Japanese popular culture have on our teenagers?)"),
			array("Impact of Japanese popular culture on teenage consumer behaviour in Hong Kong","(Do you need to refine the wording of the title?)"),
			array("A study on the impact of Japanese popular culture on teenage consumer behaviour in Hong Kong ","")
			);
			
	$S0003 = array(
			array("Facebook","(Which aspect of Facebook would you like to examine?)"),
			array("Facebook is popular.","(What would you like to examine, the causes of the popularity of Facebook or its impact?)"),
			array("The causes of the popularity of Facebook and its impact ","(Who is the target of enquiry?)"),
			array("The causes and impact of the popularity of Facebook among teenagers","(Is the wording of the title clearly defined?)"),
			array("The causes and impact of the popularity of social networking sites among teenagers","(Do you need to refine the wording of the title?)"),
			array("A study on the causes and impact of the popularity of social networking sites among teenagers in Hong Kong","")
			);	
	
	$S0004 = array(
			array("Expo 2010 Shanghai China","(Which aspect would you like to examine?)"),
			array("Expo 2010 Shanghai China and national identity","(How are the two things related?)"),
			array("Expo 2010 Shanghai China enhances national identity ","(Who is the target of enquiry?)"),
			array("Expo 2010 Shanghai China enhances the national identity of Hong Kong people","(The title for enquiry should be a question.)"),
			array("Can Expo 2010 Shanghai China enhance the national identity of Hong Kong people?","(Is the wording of the title clearly defined? Is a multi-dimensional study possible?)"),
			array("To what extent will Hong Kong's participation in Expo 2010 Shanghai China enhance the national identity of Hong Kong people?","")
			);
}
else {
	$S0001 = array(
			array("教育","(問︰與教育相關的議題很多，教育的哪個面向？)"),
			array("教育制度","(問︰教育制度的哪個範疇？)"),
			array("新高中的推行","(問︰教育制度的哪個範疇？)"),
			array("新高中通識科的推行","(問︰探究對象是誰？)"),
			array("新高中通識科在我校的推行","(問︰是否需要再收窄探究對象？)"),
			array("新高中通識科在我校中四級的推行","(問︰能否界定時間？)"),
			array("新高中通識科第一年在我校中四級的推行",""),
			);
	
	$S0002 = array(
			array("日本文化","(問︰日本文化範圍很廣，你想研究的範疇是甚麼？)"),
			array("日本流行文化","(問︰你想研究日本流行文化的變遷？發展？影響？)"),
			array("日本流行文化的影響","(問︰日本文化影響對甚麼人的影響？)"),
			array("日本流行文化對青少年的影響","(問︰是否需要再收窄探究對象？)"),
			array("日本流行文化對香港青少年的影響","(問︰對青少年的哪方面影響？)"),
			array("日本流行文化對香港青少年消費行為的影響","(問︰是否需要再修飾題目的用詞？)"),
			array("探討日本流行文化對香港青少年消費行為的影響 ","")
			);
			
	$S0003 = array(
			array("Facebook","(問︰你想研究哪個面向？)"),
			array("Facebook受歡迎","(問︰你想研究其成因？影響？)"),
			array("Facebook受歡迎的原因及其影響","(問︰探究對象是誰？)"),
			array("Facebook受青少年歡迎的原因及其影響","(問︰題目用詞是否有清楚定義？)"),
			array("社交網站受青少年歡迎的原因及其影響","(問︰是否需要再修飾題目的用詞？)"),
			array("探討社交網站受香港青少年歡迎的原因及其影響","")
			);	
	
	$S0004 = array(
			array("世博","(問︰你想研究哪個面向？)"),
			array("世博與國民身份認同","(問︰兩者關係如何？)"),
			array("世博提升國民身份認同","(問︰探究對象是誰？)"),
			array("世博提升香港人國民身份認同","(問︰探究題目應是一條問題。)"),
			array("世博是否能提升香港人國民身份認同","(問︰題目用詞是否有清楚定義？是否容許多角度探究？)"),
			array("香港參與世博，在多大程度上能提升香港市民的國民身份認同？  ","")
			);
}
	

		
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,
				   "S0004" => $S0004,	
			);
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">

<script>
var js_nr_array = new Array();	
<?
foreach($nr_array as $key => $value){
?>
	js_nr_array['<?=$key?>'] = new Array();
<?
	for($i = 0; $i < sizeof($value); $i++){
		
?>
	
	js_nr_array['<?=$key?>'][<?=$i?>] = new Array();
	js_nr_array['<?=$key?>'][<?=$i?>][0] = "<?=$value[$i][0]?>";
	js_nr_array['<?=$key?>'][<?=$i?>][1] = "<?=$value[$i][1]?>";
<?	
	}
}
unset($value);
?>
var last = "S0000";
function sw_click(id){
	
	
	if(id != last){
		$('#'+id).addClass(id+'_current');
		$('#'+last).removeClass(last+'_current');		
		last = id;
		var str = "";
		str += "<a href=\"javascript:addline(0)\"><span class=\"narrow_r\" style=\"font-size:13px;\">" + js_nr_array[last][0][0] + "</span>";
		str += "<span class=\"narrow_q\" style=\"font-size:13px;\">" + js_nr_array[last][0][1] + "</span></a><br />";
		$('#show_nr').html(str);	
	}
	

}

function addline(num){
	var loop_num = num;
	var str = "";
	if((num + 1) < js_nr_array[last].length){
		loop_num = num + 1;
	}
	
	for(i = 0; i <= loop_num; i++){
		if(i != 0){
			str += "<img src=\"/images/2009a/ies/student/orange_arrow.gif\" /><br />";
		} 
		str += "<a href=\"javascript:addline(" + i + ")\"><span class=\"narrow_r\" style=\"font-size:13px;\">" + 
				js_nr_array[last][i][0] + "</span>";
		str += "<span class=\"narrow_q\" style=\"font-size:13px;\">" + js_nr_array[last][i][1] + "</span></a><br />";
		
	}
	$('#show_nr').html(str);
	
}

</script>

<div class="narrow_range_box">
<h2><?=$ies_lang[$_lang]["string1"]?></h2>
<a href="javascript:self.parent.tb_remove();" class="IES_tool_close" title="<?=$Lang['IES']['Close']?>">&nbsp;</a>


<h3><?=$ies_lang[$_lang]["string2"]?></h3>
<div class="narrow_sample">
<a href="#" onclick="javascript:sw_click(this.id)" id="S0001"><span><?=$ies_lang[$_lang]["string3"]?></span></a>
</div>
<div class="narrow_sample">
<a href="#" onclick="javascript:sw_click(this.id)" id="S0002"><span><?=$ies_lang[$_lang]["string4"]?></span></a>
</div>
<div class="narrow_sample">
<a href="#" onclick="javascript:sw_click(this.id)" id="S0003"><span><?=$ies_lang[$_lang]["string5"]?></span></a>
</div>
<div class="narrow_sample">
<a href="#" onclick="javascript:sw_click(this.id)" id="S0004"><span><?=$ies_lang[$_lang]["string6"]?></span></a>
</div>
<div class="clear"></div>


<div id="show_nr" class="narrow_range_bottom" style="text-align:left; padding-left:35px">

</div>


</div> <!-- end of .narrow_range_box -->

<?
intranet_closedb();
?>