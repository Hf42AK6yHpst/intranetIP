<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "數據描述與分析範例";
$ies_lang["en"]["string1"] = "An exemplar for data exposition and analysis";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "《青少年上網行為研究問卷》<br/><b>根據以下的問卷問題：</b><br/>在需要上課的日子（公眾假期除外），你平均每天約花多少時間上網？";
$ies_lang["en"]["string2"] = "‘The survey questionnaire for adolescent Internet behaviour.’<br/><b>Survey question:</b><br/>On average how many hours per day during school days (except public holidays) do you spend on the Internet?";
$subtitle = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "我們取得下列數據";
$ies_lang["en"]["string3"] = "The following data is collected:";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "描述與分析";
$ies_lang["en"]["string4"] = "Exposition and analysis:";
$_string4 = $ies_lang[$_lang]["string4"];


if($_lang == "b5"){
$S0001_Answer = <<<hhtml
<table class=common_table_list>
  <tbody><tr>
    <th>時數	</th>
    <th>人數（比例）</th>
  </tr>
  <tr>
    <td valign=top><p>少於1小時 </p></td>
    <td valign=top><p>2人（4%） </p></td>
  </tr>
  <tr>
    <td valign=top><p>1-2小時 </p></td>
    <td valign=top><p>6人 （12%） </p></td>
  </tr>
  <tr>
    <td valign=top><p>2-3小時 </p></td>
    <td valign=top><p>9人 （18%） </p></td>
  </tr>
  <tr>
    <td valign=top><p>3-4小時 </p></td>
    <td valign=top><p>17人 （34%） </p></td>
  </tr>
  <tr>
    <td valign=top><p>4-5小時 </p></td>
    <td valign=top><p>13人 （26%） </p></td>
  </tr>
  <tr>
    <td valign=top><p>5-6小時 </p></td>
    <td valign=top><p>2人 （4%） </p></td>
  </tr>
  <tr>
    <td valign=top><p>6小時以上 </p></td>
    <td valign=top><p>1人 （2%） </p></td>
  </tr>
  <tr>
    <td valign=top style="border-top: 1px solid rgb(204, 204, 204);"><p>&nbsp;</p></td>
    <td valign=top style="border-top: 1px solid rgb(204, 204, 204);"><p>共50    （100%） </p></td>
  </tr>
</tbody></table>
hhtml;

$S0002_Answer = <<<hhtml
<table class="sample_table">
  <tbody><tr>
    <td width="40%"><span class=" step_title">描述步驟</span></td>
    <td width="10%">&nbsp;</td>
    <td width="40%"><span class="sample_title">例子</span></td>
  </tr>
  <tr>
    <td><div id="arrow_1" class="step_arrow_v"><a href="javascript:show_box(1)">&nbsp;</a></div></td>
    <td>&nbsp;</td>
    <td><div id="arrow_2" class="step_arrow_v"><a href="javascript:show_box(2)">&nbsp;</a></div></td>
  </tr>
  <tr>
    <td><span id="box_1" style="visibility: hidden;" class="step_box">先描述上表顯示甚麼資料。</span></td>
    <td><div id="arrow_3" style="visibility: hidden;"  class="step_arrow"><a href="javascript:show_box(2)">&nbsp;</a></div></td>
    <td><span id="box_2" style="visibility: hidden;" class="sample_box">上表顯示受訪者每天平均花在上網的時間。</span></td>
  </tr>
  <tr>
    <td><div id="arrow_4" style="visibility: hidden;" class="step_arrow_v"><a href="javascript:show_box(3)">&nbsp;</a></div></td>
    <td>&nbsp;</td>
    <td><div id="arrow_5" style="visibility: hidden;" class="step_arrow_v"><a href="javascript:show_box(4)">&nbsp;</a></div></td>
  </tr>
  <tr>
    <td><span id="box_3" style="visibility: hidden;" class="step_box">選取重點信息加以歸納，不要羅列數據。</span></td>
    <td><div id="arrow_6" style="visibility: hidden;" class="step_arrow"><a href="javascript:show_box(4)">&nbsp;</a></div></td>
    <td><span id="box_4" style="visibility: hidden;" class="sample_box">研究顯示有超過一半受訪者平均每天花3-5小時上網，當中超過三成（34%）每天花3-4小時上網，而約兩成半（26%）每天花4-5小時上網。</span></td>
  </tr>
  <tr>
    <td><div id="arrow_7" style="visibility: hidden;" class="step_arrow_v"><a href="javascript:show_box(5)">&nbsp;</a></div></td>
    <td>&nbsp;</td>
    <td><div id="arrow_8" style="visibility: hidden;" class="step_arrow_v"><a href="javascript:show_box(6)">&nbsp;</a></div></td>
  </tr>
  <tr>
    <td><span id="box_5" style="visibility: hidden;" class="step_box">1. 告訴讀者數據背後意味著甚麽。<br>2. 歸納重點，根據研究所得作合理推斷。</span></td>
    <td><div id="arrow_9" style="visibility: hidden;" class="step_arrow"><a href="javascript:show_box(6)">&nbsp;</a></div></td>
    <td><span id="box_6" style="visibility: hidden;" class="sample_box">扣除上學（約8小時）與睡眠時間（約8小時），學生在上學日子的課餘時間實際只有餘下的8小時。從是次調查得知，過半數受訪者把超過一半的課餘時間投放在網上世界，意味著放在課外活動、日常社交、學業時間、和家庭溝通的時間相應較少。</span></td>
  </tr>
</tbody></table>
hhtml;

}else{
$S0001_Answer = <<<hhtml
<table class=common_table_list>
  <tbody><tr>
    <th>No. of hours</th>
    <th>No. of people (ratio)</th>
  </tr>
  <tr>
    <td valign=top><p>Less than an hour</p></td>
    <td valign=top><p>2（4%）</p></td>
  </tr>
  <tr>
    <td valign=top><p>1-2 hours</p></td>
    <td valign=top><p>6（12%）</p></td>
  </tr>
  <tr>
    <td valign=top><p>2-3 hours</p></td>
    <td valign=top><p>9（18%）</p></td>
  </tr>
  <tr>
    <td valign=top><p>3-4 hours</p></td>
    <td valign=top><p>17（34%）</p></td>
  </tr>
  <tr>
    <td valign=top><p>4-5 hours</p></td>
    <td valign=top><p>13（26%）</p></td>
  </tr>
  <tr>
    <td valign=top><p>5-6 hours</p></td>
    <td valign=top><p>2（4%）</p></td>
  </tr>
  <tr>
    <td valign=top><p>6 hours or above</p></td>
    <td valign=top><p>1（2%）</p></td>
  </tr>
  <tr>
    <td valign=top style="border-top: 1px solid rgb(204, 204, 204);"><p>&nbsp;</p></td>
    <td valign=top style="border-top: 1px solid rgb(204, 204, 204);"><p>Total 50    （100%）</p></td>
  </tr>
</tbody></table>
hhtml;

$S0002_Answer = <<<hhtml
<table class="sample_table">
  <tbody><tr>
    <td width="40%"><span class=" step_title">Procedure</span></td>
    <td width="10%">&nbsp;</td>
    <td width="40%"><span class="sample_title">Example:</span></td>
  </tr>
  <tr>
    <td><div id="arrow_1" class="step_arrow_v"><a href="javascript:show_box(1)">&nbsp;</a></div></td>
    <td>&nbsp;</td>
    <td><div id="arrow_2" class="step_arrow_v"><a href="javascript:show_box(2)">&nbsp;</a></div></td>
  </tr>
  <tr>
    <td><span id="box_1" style="visibility: hidden;" class="step_box">Firstly, describe the data in the table above.</span></td>
    <td><div id="arrow_3" style="visibility: hidden;"  class="step_arrow"><a href="javascript:show_box(2)">&nbsp;</a></div></td>
    <td><span id="box_2" style="visibility: hidden;" class="sample_box">The table above shows the average time per day the interviewees spend on the Internet.</span></td>
  </tr>
  <tr>
    <td><div id="arrow_4" style="visibility: hidden;" class="step_arrow_v"><a href="javascript:show_box(3)">&nbsp;</a></div></td>
    <td>&nbsp;</td>
    <td><div id="arrow_5" style="visibility: hidden;" class="step_arrow_v"><a href="javascript:show_box(4)">&nbsp;</a></div></td>
  </tr>
  <tr>
    <td><span id="box_3" style="visibility: hidden;" class="step_box">Highlight and integrate the important information without displaying all the raw data.</span></td>
    <td><div id="arrow_6" style="visibility: hidden;" class="step_arrow"><a href="javascript:show_box(4)">&nbsp;</a></div></td>
    <td><span id="box_4" style="visibility: hidden;" class="sample_box">The findings show that more than half of the interviewees spend 3 to 5 hours a day on the Internet, while 34% of them spend 3 to 4 hours, and the rest (26%) spend 4 to 5 hours.</span></td>
  </tr>
  <tr>
    <td><div id="arrow_7" style="visibility: hidden;" class="step_arrow_v"><a href="javascript:show_box(5)">&nbsp;</a></div></td>
    <td>&nbsp;</td>
    <td><div id="arrow_8" style="visibility: hidden;" class="step_arrow_v"><a href="javascript:show_box(6)">&nbsp;</a></div></td>
  </tr>
  <tr>
    <td><span id="box_5" style="visibility: hidden;" class="step_box">1. Explain to readers the implications of the data.<br>2.	Summarise the key points and draw inferences from the research findings.</span></td>
    <td><div id="arrow_9" style="visibility: hidden;" class="step_arrow"><a href="javascript:show_box(6)">&nbsp;</a></div></td>
    <td><span id="box_6" style="visibility: hidden;" class="sample_box">After deducting the school time and sleeping time (about 8 hours each), there are only 8 hours left for students on school days. According to the findings, more than 50% of the interviewees spend half or more of their time after school on the Internet, which implies that the time spent on co-curricular and social activities, studying and family communication will be relatively reduced.</span></td>
  </tr>
</tbody></table>
hhtml;

}
$S0001 = array($_string3, $S0001_Answer);
$S0002 = array($_string4, $S0002_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>
<script>
	function show_box(step){
		if(step >= 1){
			document.getElementById("arrow_1").className = "step_arrow_v_clicked";
			document.getElementById("arrow_3").style.visibility = "visible";
			document.getElementById("arrow_4").style.visibility = "visible";
			document.getElementById("box_1").style.visibility = "visible";
		}
		if(step == 2 || step == 4 || step == 6){
			document.getElementById("arrow_2").className = "step_arrow_v_clicked";
			document.getElementById("arrow_3").className = "step_arrow_clicked";
			document.getElementById("arrow_5").style.visibility = "visible";
			document.getElementById("box_2").style.visibility = "visible";
		}
		if(step >= 3){
			document.getElementById("arrow_4").className = "step_arrow_v_clicked";
			document.getElementById("arrow_6").style.visibility = "visible";
			document.getElementById("arrow_7").style.visibility = "visible";
			document.getElementById("box_3").style.visibility = "visible";
		}
		if(step == 4 || step == 6){
			document.getElementById("arrow_5").className = "step_arrow_v_clicked";
			document.getElementById("arrow_6").className = "step_arrow_clicked";
			document.getElementById("arrow_8").style.visibility = "visible";
			document.getElementById("box_4").style.visibility = "visible";
		}
		if(step >= 5){
			document.getElementById("arrow_7").className = "step_arrow_v_clicked";
			document.getElementById("arrow_9").style.visibility = "visible";
			document.getElementById("box_5").style.visibility = "visible";
		}
		if(step == 6){
			document.getElementById("arrow_8").className = "step_arrow_v_clicked";
			document.getElementById("arrow_9").className = "step_arrow_clicked";
			document.getElementById("box_6").style.visibility = "visible";
		}
	}
</script>