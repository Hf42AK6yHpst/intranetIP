<?php
//modifying By connie
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_bibi.php"); 
intranet_auth();
intranet_opendb();


$libies_bibi = new libies_bibi();
$libies= new libies();

$bibiId = $_REQUEST['bibiId'];
$schemeID = $_REQUEST['scheme_id'];
//debug_r($schemeID);
if(!isset($bibi_lang) )
{ 
	$SchemeInfoArr = current($libies->getTableInfo('IES_SCHEME', 'SchemeID',$schemeID));

	$bibi_lang = $SchemeInfoArr['Language'];
	
	$bibi_LANG =($bibi_lang=='en')? 'en':'b5';
	$bibi_lang=($bibi_lang=='en')? 'eng':'chi';
	
	include_once($PATH_WRT_ROOT."lang/ies_lang.$bibi_LANG.php");
}


$skipArray = array();
$skipArray[]="chibook";
$skipArray[]="chinew";
$skipArray[]="chinet";
$skipArray[]="manual";
$skipArray[]="engbook";
$skipArray[]="engnew";
$skipArray[]="engnet";


if(!empty($bibiId)){
	$bibidetails = $libies_bibi->getbibiArrayById($bibiId);

	$type = $bibidetails['Type'];
	
	if(in_array($type,$skipArray))
	{
		if($bibidetails['Todisplay']){
			$isdisplaytobibi = "CHECKED";
		}
		else{
			$notdisplaytobibi = "CHECKED";
		}
	}
	else
	{
		if($bibidetails['Todisplay']){
			$notdisplaytobibi = "CHECKED";
		}
		else{
			$isdisplaytobibi = "CHECKED";
		}
	}
	
	$content = $bibidetails['Content'];

	
	$contentArray = $libies_bibi->parseContentString($content);

}
else{

}



//echo scheme_id;
$html_title = $libies_bibi->getbibiTitle();
$html_lang_table = $libies_bibi->getbibiLangChoicTable($bibi_lang); 
$html_table = $libies_bibi->getFillTableDisplay($type, $contentArray);
$html_list_chi = $libies_bibi->getTypeListDisplay('chi', 'lang_list', $type,0); 
$html_list_en = $libies_bibi->getTypeListDisplay('eng', 'lang_list', $type,0); 

$html_list_chi = str_replace("\n", "", $html_list_chi);
$html_list_en = str_replace("\n", "", $html_list_en);

if($bibi_lang == 'chi'){
	$html_list_current = $html_list_chi;
	
}
else if($bibi_lang == 'eng'){
	$html_list_current = $html_list_en;
}
else{
	$isdisplay ='display:none';
	
}

if(!empty($bibidetails)){

	if(stristr($type, "eng")){
		$html_lang_table = "English"; 
	}
	else{
		$html_lang_table = "中文";
	}
	$isdisplay ='';
	$html_list_current = $Lang['IES'][$type]['fieldname'];
	
}



$html_old_data_Warning='';

if(!empty($html_table)){
	if(in_array($type,$skipArray))
	{
		$html_old_data_Warning =$Lang['IES']['oldData'];
	}
	else
	{
	 	$html_submit_button = "<input name=\"submit2\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onclick=\"formcheck()\" value=\" {$Lang['IES']['Save']} \" />";		
	}
}

if(in_array($type,$skipArray))
{
	$html_displaytobibi = $Lang['IES']['displaytobibi'];
	$html_isdisplaytobibi =$Lang['IES']['true'];
	$html_notdisplaytobibi =$Lang['IES']['false'];
	$html_Yes = "1";
	$html_No = "0";
}
else
{	
	$html_displaytobibi = $Lang['IES']['displaytobibi_new'];
	$html_isdisplaytobibi =$Lang['IES']['ForSelfRef'];
	$html_notdisplaytobibi =$Lang['IES']['ForDispInBibi'];
	$html_Yes = "0";
	$html_No = "1";
}

//it is for adding the old data
//$html_submit_button = "<input name=\"submit2\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onclick=\"formcheck()\" value=\" {$Lang['IES']['Save']} \" />";		

if(!empty($msg)){
//	$html_msg = $libies_bibi->Get_WARNING_MSG($msg); 
	
	if($msg=='add')
	{
		$Msg= $Lang['IES']['AlertMsg']["add"];
	}
	else if($msg=='update')
	{
		$Msg= $Lang['IES']['AlertMsg']["update"];
	}
	$html_msg = "<div class=\"alert_message\"><span class=\"fail\"><font color=\"green\">{$Msg}</font></span></div> ";
}

$linterface = new interface_html($ies_cfg["ThickboxFrontEndInterface"]);
$linterface->LAYOUT_START($msg);
include "templates/addbibi.tmpl.php";
$linterface->LAYOUT_STOP();


intranet_closedb();

?>