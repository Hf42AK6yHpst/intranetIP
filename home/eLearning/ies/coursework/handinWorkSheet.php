<?php

$PATH_WRT_ROOT = "../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/libinterface.php");
include_once("{$PATH_WRT_ROOT}includes/libfilesystem.php");
include_once("{$PATH_WRT_ROOT}includes/libipfilesmanager.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
//$uploadField = libiportal_fm::getUploadFileFieldHtml("processUpload.php");

intranet_auth();
intranet_opendb();
$libdb = new libdb();

$linterface = new interface_html();
$loadingImg = $linterface->Get_Ajax_Loading_Image();
$objIES = new libies();
$studentID = $UserID;

$html_upload = $objIES->returnFileUploadResources($scheme_id,$ies_cfg['HandinFileByStudent'],$ies_cfg['WorksheetFlag_Edit'], $worksheetID,$studentID);
include_once("templates/handinWorkSheet_tmpl.php");
intranet_closedb();
?>