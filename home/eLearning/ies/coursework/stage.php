<?php
//modifying By :
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/ies_lang.".$_SESSION["IES_CURRENT_LANG"].".php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libportal.php");


intranet_auth();
intranet_opendb();
//debug_pr($_POST);
/**
 * GET INPUT PARAMETER
 */
$stage_id = intval(trim($stage_id));
$scheme_id = intval(trim($scheme_id));


 //debug_r($scheme_id);
 if(empty($stage_id) || !is_int($stage_id) || empty ($scheme_id))
 {
	 echo "Invalide access , Empty Stage ID<br/>";
	 exit();
 }


$html_stage_id = $stage_id;
$html_scheme_id = $scheme_id;
$objsystem = new libfilesystem();
$objIES = new libies();


$note_id = $objIES->retrieveNoteIdByStageIdAndUserId($UserID, $stage_id);






$layout_tmpl = ($objIES->isIES_Teacher($UserID) || $objIES->IS_ADMIN_USER($UserID)) ? $ies_cfg["PreviewFrontEndInterface"] : $ies_cfg["DefaultFrontEndInterface"];
$linterface = new interface_html($layout_tmpl);
$CurrentPage = "StudentProgress";



### Title ###
$title = $ies_lang['IES_TITLE'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

//$scheme_arr = $objIES->GET_SCHEME_ARR();
$scheme_arr = $objIES->GET_USER_SCHEME_ARR($UserID);
$html_scheme_selection = libies_ui::GET_SCHEME_SELECTION($scheme_arr, $scheme_id);

$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);
$html_stage_selection = libies_ui::GET_STAGE_SELECTION($stage_arr,$stage_id,$scheme_id);

$stage_arr = $objIES->GET_STAGE_DETAIL($stage_id);
list($stage_title, $stage_sequence, $stage_deadline) = $stage_arr;

$html_stage_deadline = $stage_deadline;
$task_arr = getStageTaskArr($stage_id);


$html_content = "";

$html_js_load_task_map = "var task_step_map = [";

//LOOP FOR EACH TASK FOR THIS STAGE
$withTaskMap = false;
for($i=0,$i_max = sizeof($task_arr); $i < $i_max; $i++){

	$t_task_id = $task_arr[$i]['TaskID'];
	$t_step_id = $task_arr[$i]['StepID'];

	// handle stage 3 display if necessary
//	if ($stage_sequence==3) {
//		$currentTaskParentsInfo = current($objIES->getParentsIds("task",$t_task_id));
//		if ($currentTaskParentsInfo["TASK_SEQ"] == 1) {
//
//		}
//	}

	$task_detail_arr = $objIES->GET_TASK_BLOCK_INFO($t_task_id, $UserID);

	// Skip displaying task if disabled
	$t_is_task_enable = $task_detail_arr["is_task_enable"];
	if($t_is_task_enable)
	{
		$task_answer = $task_detail_arr["task_answer"];
//debug_r($task_answer);
		$task_question_type = $task_detail_arr["task_question_type"];
		if($task_answer !== false) {
			switch(strtoupper($task_question_type)) {
				case "TABLE":
					$div_class = 'ies_diary';
					break;
				default:
					$div_class = '';
			}
		}

		$withTaskMap = true; //mark it has task map
		$html_js_load_task_map .= "[".$t_task_id.", ".$t_step_id."],";
		$html_content .= "<div id=\"task_block_".$t_task_id."\" class=\"IES_task {$div_class}\">";
		$html_content .= libies_ui::GET_TASK_BLOCK_CONTENT($task_detail_arr);
		$html_content .= "</div>";
	}
}

if($withTaskMap > 0){
	//remove the last occurance ',' of $html_js_load_task_map
	$html_js_load_task_map = substr($html_js_load_task_map, 0, -1)."]\n";
}else{
	//should have this checking, otherwise with remove the last occurance of $html_js_load_task_map "[" , this will have JS error
	$html_js_load_task_map .= "]\n";
}

$html_frameInfo = $objIES->GET_PAGEFRAME_INFO($stage_sequence);

$linterface->LAYOUT_START();
include_once("templates/stage.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();


function initAllStudentTaskDetails()
{
	/*
	insert into IES_TASK_STUDENT_DETAILS
(TaskID,UserID,Status,DateInput)
select
	task.taskID,
	student.UserID,
	1,now()

 from IES_SCHEME_STUDENT as student
inner join IES_SCHEME as scheme
	 on student.schemeID = scheme.schemeID
inner join IES_STAGE as stage
	on stage.schemeID = scheme.schemeID
inner join IES_TASK as task
	on task.stageid = stage.stageid


	*/
}
function getStageTaskArr($parStageID)
{
  global $intranet_db;

  $ldb = new libdb();

  $sql = "SELECT task.TaskID, step.StepID FROM {$intranet_db}.IES_TASK task LEFT JOIN {$intranet_db}.IES_STEP step ON task.TaskID = step.TaskID WHERE task.StageID = '{$parStageID}' GROUP BY task.TaskID HAVING MIN(step.Sequence) ORDER BY task.Sequence, step.Sequence";

  $stageTaskArr = $ldb->returnArray($sql);

  return $stageTaskArr;
}

?>