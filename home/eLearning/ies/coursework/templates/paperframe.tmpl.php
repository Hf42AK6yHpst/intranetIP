<?php 
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
// PLEASE DON'T INSERT ANY PHP FUNCTIONS OR DATA MANIPULATION CODE HERE
// ONLY PHP VARIABLES CAN BE USED FOR DISPLAY
if($html_scheme_id == "")
{
	echo "schemeID not exist. Program exist<br/>";	
	exit();
}
if($html_stage_id == "")
{
	echo "stageID not exist. Program exist<br/>";	
	exit();
} 
//Get the detail description
$folder_path = "{$intranet_root}/file/ies/lang/scheme/{$scheme_id}/";
$file_path = $folder_path."scheme_lang.php";
$html_folderDesc ="";
$html_textDesc ="";
$html_noteDesc ="";
$html_worksheetDesc ="";
$html_FAQDesc ="";
//the title of the student tool
$objIESS = new libies();
$All_DESC = $objIESS->GET_DESC_DETAIL($file_path);
if($All_DESC != NULL)
{
	$html_folderDesc = $All_DESC["FolderDesc"];
	$html_textDesc = $All_DESC["TextDesc"];
	$html_noteDesc = $All_DESC["NoteDesc"];
	$html_worksheetDesc = $All_DESC["WorksheetDesc"];
	$html_FAQDesc = $All_DESC["FAQDesc"];
}

// Check if the user is a teacher / admin
$html_is_staff = ($objIESS->IS_ADMIN_USER($UserID) || $objIESS->isIES_Teacher($UserID));

?>

<script language="JavaScript" src="/templates/jquery/ui.core.js"></script>
<script language="JavaScript" src="/templates/jquery/ui.draggable.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery.clipboard.min.js"></script>
<script language="JavaScript">
var noteID = 0;
/*
// common function for going to specific scheme
function doScheme(jParSchemeID){
  document.form1.scheme_id.value = jParSchemeID;
  document.form1.action = "index.php";
  document.form1.submit();
}
*/
/*
* functions for toolbar
*/

function doStep(jParStepID){
  window.location = "coursework.php?step_id="+jParStepID+"&scheme_id=<?=$scheme_id?>";
}

function loadContent(evt,contentType)
{
	
		//link = document.getElementById('campusLinkico');
		//alert(document.defaultView.getComputedStyle(link,null).getPropertyValue('left'));

		//$("#stu_files").css("left", (evt.clientX+20)+"px");
		//$("#stu_files").css("top", '-75px');  // control the display top location , relative to the mouse pointer

		serverURL = "../ajax/ajax_coursework_toolBar_handler.php";
		$.ajax({
      	url: serverURL,
      	type: "POST",
      	data: "type="+contentType+"&scheme_id="+<?=$scheme_id?>,
      	success:  function(data){
                  $('#'+contentType).html(data);
                  $('#'+contentType).show();
                }
    	});
    
    //('#stu_files').load(serverURL,{type: "stu_files"});
	

}

function openRightTool(){
  $('.IES_sheet_wrap').css('padding-right', '320px');
  $('.IES_right_tool').css('display', 'block');
  $('li.reflect_note').addClass('current');
}

function closeRightTool(){
  $('.IES_sheet_wrap').css('padding-right', '1px');
  $('.IES_right_tool').css('display', 'none');
  $('li.reflect_note').removeClass('current');
}

function doEditNote() {
	type = "edit_note";
	noteid = $("#note_id").val();
	
	var data_for_note = "";
	if (noteid != "") {
		data_for_note = "&noteid="+noteid;
	}
//	alert(data_for_note);
//	alert("/ajax/ajax_coursework_toolBar_handler.php"+"?"+"type="+type+data_for_note);
  $.ajax({
    url:      "../ajax/ajax_coursework_toolBar_handler.php",
    type:     "POST",
    async:    false,
    data:     "type="+type+data_for_note,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
				//$("#task_block_"+parTaskID).parent().find("a.show_intro").hide();
                $("#IES_right_tool").html(data);
              }
  });
}

function doCancelEditNote() {
	type = "reflect_note";
	noteid = $("#noteId").val();
	
	var data_for_note = "";
	if (noteid != "") {
		data_for_note = "noteid="+noteid;
	}
	
  $.ajax({
    url:      "../ajax/ajax_coursework_toolBar_handler.php",
    type:     "POST",
    async:    false,
    data:     "type="+type+data_for_note,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
				//$("#task_block_"+parTaskID).parent().find("a.show_intro").hide();
                $("#IES_right_tool").html(data);
              }
  });
}


function doUpdateNote() {
	type = "update_note";
	noteid = $("#note_id").val();
	noteContent = $("#note_content").val();
	var data_for_note = '';
	if (noteid != "") {
		data_for_note = "&noteid="+noteid;
	}
	data_for_note = data_for_note + "&noteContent=" + noteContent + "&stageId=" + <?=$stage_id?>;
  $.ajax({
    url:      "../ajax/ajax_coursework_toolBar_handler.php",
    type:     "POST",
    async:    false,
    data:     "type="+type+data_for_note,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
    			$("#IES_right_tool").html(data);    			
              }
  });
}

function doDisplayFAQ(scheme_id) {

	$('#IES_right_tool').load(
	"../ajax/ajax_coursework_toolBar_handler.php",
	{
	type: 'faq',
	scheme_id: scheme_id
	},
	function(ReturnData)
	{
	}
	);
}

function doDisplayTaskAnswer(scheme_id) {

	$('#IES_right_tool').load(
	"../ajax/ajax_coursework_toolBar_handler.php",
	{
	type: 'taskAns',
	scheme_id: scheme_id
	},
	function(ReturnData)
	{
	}
	);
}

$(document).ready(function(){

	//show hide scheme option					   
	$("ul.scheme_option").hide(); 
	$("a.scheme_select").click(function() {
    $("ul.scheme_option").slideToggle("normal");
    return false;
  });

	//selection box action			   
  $('.selectAnsList ul li').toggle(
    function(event) {
      $(this).addClass('active');  
    },
    function(event) {
      $(this).removeClass('active');
    }
  );
  
  $(".draggable").draggable();
  
	//student tool
  $('a.toolBar').live('click', function(){
    if($('.IES_right_tool').is(':hidden')) {
  	  //USE THE ATTR "NAME" TO CHECK THE CLICK TYPE (FAQ, REFLECT...)
  	  var type = $(this).attr("name");
      openRightTool();
  	  	
      var otherData = "";
      if (type=="reflect_note") {
      		otherData = "&noteid="+$("input[name=note_id]").val();
      	
      }

  		$.ajax({
        url:      "../ajax/ajax_coursework_toolBar_handler.php",
        type:     "POST",
        async:    false,
        data:     "type="+type+otherData,  // PASS THE TYPE TO THE AJAX FILE TO DETERMINE THE DISPLAY
        error:    function(xhr,ajaxOptions,thrownError){
                    alert(xhr,responseText);
                  },
        success:  function(data){
                    $("#IES_right_tool").html(data);
                  }
      });
    }
   else {
      closeRightTool();
    }
  });
  
  // Bind "live" action to X button in right tool box
  $('.IES_tool_close').live('click', function() {
    // Live handler called.
    closeRightTool();
  });

<?php if($html_is_staff) { ?>  
  // Disable all buttons if teacher login
  $("input[type=button]").each(function(){
    var btnFuncStr = (typeof $(this).attr('onclick') == "function") ? $(this).attr('onclick').toString().replace(/\n/g,'') : "";
    var btnAble =  (btnFuncStr.indexOf("doSaveStep") == -1) && (btnFuncStr.indexOf("doStep") != -1 || btnFuncStr.indexOf("backStage") != -1);

    $(this).attr("disabled", true);
    if(btnAble)
    {
      $(this).attr("disabled", false);
    }
  });
  
  // Remove action for student tools, and alert user
  $(".student_tools").find("a").each(function(){
    $(this).attr('onclick', '');
    $(this).die('click');
    $(this).click(function(){
      alert("<?=$Lang['IES']['NAinPreview']?>");
    });
  });
  
  // Remove action for submission link, and alert user
  $("a.submission").attr('href', 'javascript:;').click(function(){
    alert("<?=$Lang['IES']['NAinPreview']?>");
  });
<?php } ?>

	// Prepare for clipboard copy
	// * Not supported in Flash 10 or later
	$.clipboardReady(function(){
	}, { swfpath: "/templates/jquery/jquery.clipboard.swf", debug: true });

});



</script>

<div class="<?=$html_frameInfo["background_class"]?>"><div class="theme"> <!-- stage 1 background -->

  <div class="top_left_btn"> <!-- logo , scheme, stage btn -->
    <div class="IES_logo"><div><a href="javascript:doScheme(<?=$scheme_id?>)">&nbsp;</a></div></div> <!-- IES logo -->
    <?=$html_scheme_selection?>
    <a href="#" class="scheme_select" title="<?=$Lang['IES']['ChooseOtherScheme']?>">&nbsp;</a>
    <?=$html_stage_selection?>
  </div> <!--class "top_left_btn" end -->
  
  <div class="clear"></div>
  <div class="student_tools"><div> <!-- student tool -->
    <ul>
      <li><a href="javascript:;" onclick="loadContent(event,'stu_files')" name ="stu_files" class="stu_files" title="<?=$html_folderDesc?>"><span><?=$Lang['IES']['Folder']?></span></a></li>
      <li><a href="javascript:;" onclick="loadContent(event,'bibi')" name ="bibi" class="bibi" title="<?=$html_textDesc?>"><span><?=$Lang['IES']['BibliographyRecords']?></span></a></li>
      <li><a href="javascript:;" name ="reflect_note" class="reflect_note toolBar" title="<?=$html_noteDesc?>"><span><?=$Lang['IES']['ReflectNote']?></span></a></li>
      <li><a href="javascript:;" onclick="loadContent(event,'worksheet')" name ="worksheet" class="worksheet" title="<?=$html_worksheetDesc?>"><span><?=$Lang['IES']['Worksheet'] ?></span></a></li>
      <li><a href="javascript:;" name ="faq" onClick="doDisplayFAQ(<?=$scheme_id?>)" class="faq toolBar" title="<?=$html_FAQDesc?>"><span><?=$Lang['IES']['FAQ']?></span></a></li>
      <li><a href="javascript:;" name ="taskAnsMgr" onClick="doDisplayTaskAnswer(<?=$scheme_id?>)" class="mywork toolBar"><span><?=$Lang['IES']['TaskAnswer']?></span></a></li>
    </ul>
  </div></div> <!-- student tool end-->
  
  <div class="IES_file floating_tool_box draggable resizable" id="stu_files" style="width:700px; top:230px; right:15px; display:none;">
  
  <!-- student file list (ajax display)-->
  </div>
  <div class="IES_bibi floating_tool_box draggable resizable" id="bibi" style="width:700px; top:230px; right:15px; display:none;">
   <!-- student file list (ajax display)-->
  </div>
    <div class="IES_worksheet floating_tool_box draggable resizable" id="worksheet" style="width:700px; top:230px; right:15px; display:none;">
   <!-- student file list (ajax display)-->
  </div>
  
  <div class="clear"></div>
  
  <div class="IES_sheet_wrap" style="padding-right:1px"> <!-- when expand 反思手記 and FAQ; padding-right is 320px ; collapse is 1px-->
    <div class="IES_sheet"> <!-- no need change anything for the action of 反思手記 and FAQ -->
      <div class="main_paper_top"><div><div></div></div></div>
      <div class="main_paper_left"><div class="main_paper_right">
        <div class="main_paper_body_left"><div class="main_paper_body_right"><div class="main_paper_body ">
          <div class="stage1_title"><span class="title"><?=$stage_title?></span><span class="deadline"><?=$Lang['IES']['SubmissionDeadline']?> <?=$Lang['_symbol']["colon"]?> <?=$html_stage_deadline?></span>
		  
		  
		   <!-- top right btn start #### added 07-06-2010 #### -->
                <div class="IES_top_tool">
               <!-- <a href="#" class="font_S" title="">&nbsp;</a>
                <a href="#" class="font_L_current" title="">&nbsp;</a>
                <div class="v_line"></div>-->
                <!--<a href="#" class="print" title="">列印</a>-->
       <!--         <a href="./exportdoc.php?ParUserId=<?=$UserID?>&ParStageId=<?=$html_stage_id?>" class="export" title="">匯出</a>-->
         <!--       <div class="v_line"></div> -->
                <a href="handInTask.php?stage_id=<?=$html_stage_id?>&scheme_id=<?=$html_scheme_id?>&is_current_record=1" class="submission" title=""><span><?=$Lang['IES']['Submit']?></span></a>
                </div>
                <!-- top right btn end --><div class="clear"></div></div>
            <div class="stage1_paper"> <!-- class "stage1_paper" making a single line paper-->                 

<?=$html_content?>  
			
				<!-- bottom right btn start #### added 26-01-2011 #### -->
                <br/>
                <div class="IES_top_tool">
	               	<!-- <a href="#" class="font_S" title="">&nbsp;</a>
	                <a href="#" class="font_L_current" title="">&nbsp;</a>
	                <div class="v_line"></div>-->
	                <!--<a href="#" class="print" title="">列印</a>-->
	       			<!--         <a href="./exportdoc.php?ParUserId=<?=$UserID?>&ParStageId=<?=$html_stage_id?>" class="export" title="">匯出</a>-->
	         		<!--       <div class="v_line"></div> -->
         	       	<a href="handInTask.php?stage_id=<?=$html_stage_id?>&scheme_id=<?=$html_scheme_id?>&is_current_record=1" class="submission" title=""><span><?=$Lang['IES']['Submit']?></span></a>
                </div>
                <!-- bottom right btn end -->
            
            </div> <!--class "stage1_paper" end -->
          </div>
        </div></div>
      </div></div>
      <div class="main_paper_bottom"><div><div></div></div></div>        
    </div> <!-- IES_sheet end-->
    
    <div class="IES_right_tool" id="IES_right_tool" style="display:none">&nbsp;</div> <!-- when collapse 反思手記 and FAQ : display: none-->
    <div class="clear"></div>
  </div> <!-- IES_sheet_wrap end -->
  
</div></div>

<input type="hidden" name="note_id" id="note_id" value="<?=$note_id?>" />

<!--<input type="hidden" name="html_scheme_id" id="html_scheme_id" value="<?=$scheme_id?>" />-->