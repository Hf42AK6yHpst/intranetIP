<?php

$ies_lang["b5"]["string1"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string1"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. If any revision is required, return to the previous step.";

$ies_lang["b5"]["string2"] = "結論回應了研究目的與焦點問題。";
$ies_lang["en"]["string2"] = "The conclusion addresses the enquiry objectives and focus questions.";

$ies_lang["b5"]["string3"] = "結論是根據剛才的分析而寫的。";
$ies_lang["en"]["string3"] = "The conclusion is written up with reference to the previous analysis.";

$ies_lang["b5"]["string4"] = "結論與緒論邏輯一致，前後呼應。";
$ies_lang["en"]["string4"] = "The logic of the conclusion and preface echoes and is consistent.";

$ies_lang["b5"]["string5"] = "結論部分如有新觀點，已作闡釋。";
$ies_lang["en"]["string5"] = "If there is a new standpoint in the conclusion, it is explained clearly.";

$ies_lang["b5"]["string6"] = "結論綜合了報告的各大重點及重大的研究發現。";
$ies_lang["en"]["string6"] = "The conclusion summarises all the main points in the report and significant research findings.";

$ies_lang["b5"]["string7"] = "修訂結論，務求達到自我檢測所有指標。";
$ies_lang["en"]["string7"] = "Revise the conclusion to meet all requirements of the self-check.";

?>