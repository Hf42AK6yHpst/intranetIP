<?php
$html_textarea = '<textarea style="width:100%; height:50px" name="question[textarea][1]">'.$answer[1].'</textarea> ';
$html_textareaWithTitle = libies_ui::getTextareaDisplayWithTitle($ies_lang[$_lang]["string7"], $html_textarea);
?>

<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
    	<p>
    		<?=$ies_lang[$_lang]["string1"]?>
    		<br/><br/>
    		<?=$ies_lang[$_lang]["string2"]?>
    		<span class="prev_answer"><?=$html_prev_task_answer_1?></span>
    		<br/><br/>
    		<?=$ies_lang[$_lang]["string3"]?>
    		<span class="prev_answer"><?=$html_prev_task_answer_2?></span>
    		<br/><br/>
    		<?=$ies_lang[$_lang]["string4"]?>
    		<span class="prev_answer"><?=$html_prev_task_answer_3?></span>
    		<br/><br/>
    		<?=$ies_lang[$_lang]["string5"]?>
    		<span class="prev_answer"><?=$html_prev_task_answer_4?></span>
    		<br/><br/>
    		<?=$ies_lang[$_lang]["string6"]?>
    		<span class="prev_answer"><?=$html_prev_task_answer_5?></span>
    		<br/><br/>
    	</p>
    	
	    
		<?=$html_textareaWithTitle?>

	    <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
	      <div style="float:right; padding-top:5px">
	        <input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
	      </div>
	    </div>
	</div>     
		
	<!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$prev_step_id?>);" value=" <?=$Lang['IES']['LastStep']?> " />
    <input id="save_submit" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Submit']?> " />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />                                      
  </div></div>
  
</div> <!-- task_wrap end -->