<?

$ies_lang["b5"]["string1"] = "細心檢視自己的題目，看看有沒有相關概念和關鍵詞需要下定義。";
$ies_lang["en"]["string1"] = "Examine your topic carefully to see whether any definitions of related concepts and key words are required.";

$ies_lang["b5"]["string2"] = "階段一撰寫的探究題目：<br/>";
$ies_lang["en"]["string2"] = "The topic for enquiry written up at Stage 1:<br/>";

$ies_lang["b5"]["string3"] = "階段二撰寫的探究題目：<br/>";
$ies_lang["en"]["string3"] = "The topic for enquiry written up at Stage 2:<br/>";

$ies_lang["b5"]["string4"] = "階段三撰寫的研究背景：<br/>";
$ies_lang["en"]["string4"] = "The background information written up at Stage 3:<br/>";

$ies_lang["b5"]["string5"] = "階段三撰寫的研究目的：<br/>";
$ies_lang["en"]["string5"] = "The enquiry objectives written up at Stage 3:<br/>";

$ies_lang["b5"]["string6"] = "階段一撰寫的相關知識與概念：<br/>";
$ies_lang["en"]["string6"] = "The related concepts and relevant knowledge written up at Stage 1:<br/>";

$ies_lang["b5"]["string7"] = "如有需要，請在下列空格給相關知識與概念下定義：";
$ies_lang["en"]["string7"] = "Define the related concepts and key words in the box below if necessary.";

?>