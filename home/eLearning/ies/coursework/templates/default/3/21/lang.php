<?php

$ies_lang["b5"]["string1"] = "參考你在階段一和二的反思手記，回顧你的學習歷程，整理階段三的學習反思，記下你在此階段學到的知識和技能，或在過程中的經歷和轉變。";
$ies_lang["en"]["string1"] = "With reference to your reflection logs at Stages 1 and 2, recall your learning path, generalise the learning reflection at Stage 3 and mark down the knowledge and skills learnt, or the experience and changes encountered in the course.";

$ies_lang["b5"]["string2"] = "反思不是純粹記下個人感想，而是總結第一至三階段的學習成果，例如：你哪方面的研究技巧提高了？";
$ies_lang["en"]["string2"] = "Not only personal feelings are recorded in the reflection, the learning outcomes of the three stages are also summarised, e.g. which aspects of your enquiry skills have improved?";

$ies_lang["b5"]["string3"] = "反思寫作方法";
$ies_lang["en"]["string3"] = "Reflection writing method";

$ies_lang["b5"]["string4"] = "階段一撰寫的反思：<br/>";
$ies_lang["en"]["string4"] = "The reflection written up at Stage 1:<br/>";

$ies_lang["b5"]["string5"] = "階段二撰寫的反思：<br/>";
$ies_lang["en"]["string5"] = "The reflection written up at Stage 2:<br/>";

$ies_lang["b5"]["string6"] = "參考反思手記及階段一、二的反思，撰寫整個專題研習報告的反思：";
$ies_lang["en"]["string6"] = "With reference to the 'Reflection log' and the reflection at Stages 1 and 2, write up the reflection of the whole enquiry report.";

?>