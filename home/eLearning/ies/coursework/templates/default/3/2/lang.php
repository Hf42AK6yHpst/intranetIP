<?
$ies_lang["b5"]["string1"] = "撰寫研究背景時，緊記要以描述客觀現象入題。<br />";
$ies_lang["en"]["string1"] = "Remember to start with describing objective phenomena when writing up the background information.<br />";

$ies_lang["b5"]["string2"] = "你可以參考以下角度，延伸內容：<br />";
$ies_lang["en"]["string2"] = "You can elaborate on the subject matter by considering the following perspectives:<br />";

$ies_lang["b5"]["string3"] = "有甚麼客觀現象促使問題或衝突的發生？";
$ies_lang["en"]["string3"] = "What kinds of objective phenomena trigger problems or conflicts?";

$ies_lang["b5"]["string4"] = "有哪些大趨勢促成這問題或衝突的形成？";
$ies_lang["en"]["string4"] = "Which trends trigger these problems or conflicts?";

$ies_lang["b5"]["string5"] = "為甚麼這個問題值得研究？";
$ies_lang["en"]["string5"] = "Why is this topic worth studying?";

$ies_lang["b5"]["string6"] = "研究這個問題有何社會意義？";
$ies_lang["en"]["string6"] = "What inherent social implications does this enquiry have?";

$ies_lang["b5"]["string7"] = "為甚麼這個問題值得我們關心？";
$ies_lang["en"]["string7"] = "Why is this topic worth paying attention to?";

$ies_lang["b5"]["string8"] = "要突出研究話題的重要性，可參考以下角度：";
$ies_lang["en"]["string8"] = "You can highlight the significance of the enquiry topic by considering the following perspectives:";

$ies_lang["b5"]["string9"] = "此現象影響很多人";
$ies_lang["en"]["string9"] = "This phenomenon affects many people.";

$ies_lang["b5"]["string10"] = "此現象對社會有長遠影響";
$ies_lang["en"]["string10"] = "This phenomenon has long-term influence on society.";

$ies_lang["b5"]["string11"] = "所研究的話題為熱門議題 (加以解釋)，所以值得研究";
$ies_lang["en"]["string11"] = "The enquiry topic is a hot issue (with explanations) and therefore worth studying.";

$ies_lang["b5"]["string12"] = "目前沒有類似的研究";
$ies_lang["en"]["string12"] = "No similar enquiry has been conducted so far.";

$ies_lang["b5"]["string13"] = "研究有社會意義 (加以解釋)";
$ies_lang["en"]["string13"] = "The enquiry has inherent social implications (with explanations).";

$ies_lang["b5"]["string14"] = "檢查在階段一撰寫的研究背景是否客觀：<br />";
$ies_lang["en"]["string14"] = "Examine the objectivity of the background information written up at Stage 1:<br />";

$ies_lang["b5"]["string15"] = "參考上述角度，整理研究背景：<br />";
$ies_lang["en"]["string15"] = "Organise the background information with reference to the above perspectives:<br />";
?>