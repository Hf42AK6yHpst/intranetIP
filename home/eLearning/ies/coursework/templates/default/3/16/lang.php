<?php

$ies_lang["b5"]["string1"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string1"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. If any revision is required, return to the previous step.";

$ies_lang["b5"]["string2"] = "具備多角度分析。";
$ies_lang["en"]["string2"] = "Multi-perspective analysis is demonstrated.";

$ies_lang["b5"]["string3"] = "以上分析具有一定的深度。";
$ies_lang["en"]["string3"] = "The above analysis has a reasonable degree of profundity.";

$ies_lang["b5"]["string4"] = "以上分析能解答焦點問題。";
$ies_lang["en"]["string4"] = "The above analysis can address the focus questions.";

$ies_lang["b5"]["string5"] = "以上分析能回應研究重點。";
$ies_lang["en"]["string5"] = "The above analysis can address the enquiry objectives.";

$ies_lang["b5"]["string6"] = "以上分析能看到持分者的不同觀點。";
$ies_lang["en"]["string6"] = "The above analysis can demonstrate different viewpoints of stakeholders.";

$ies_lang["b5"]["string7"] = "修訂研究分析，務求達到自我檢測所有指標。";
$ies_lang["en"]["string7"] = "Revise the research analysis to meet all requirements of the self-check.";
?>