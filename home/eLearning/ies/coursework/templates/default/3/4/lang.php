<?

$ies_lang["b5"]["string1"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string1"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. Return to the previous step if any revision is required.";

$ies_lang["b5"]["string2"] = "研究目的清晰具體，一目了然。";
$ies_lang["en"]["string2"] = "The enquiry objectives are concrete, specific and clear.";

$ies_lang["b5"]["string3"] = "研究目的充分回應了研究題目。";
$ies_lang["en"]["string3"] = "The enquiry objectives address the enquiry topic completely.";

$ies_lang["b5"]["string4"] = "研究目的能夠解答某些問題、分析某個方法的利弊、提出某些建議等。";
$ies_lang["en"]["string4"] = "The enquiry objectives can answer certain questions, analyse the pros and cons of a certain method, provide certain suggestions, etc.";

$ies_lang["b5"]["string5"] = "修訂研究目的，務求達到自我檢測所有指標。";
$ies_lang["en"]["string5"] = "Revise the enquiry objectives to meet all requirements of the self-check.";

?>