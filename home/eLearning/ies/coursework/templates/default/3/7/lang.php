<?php

$ies_lang["b5"]["string1"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string1"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. If any revision is required, return to the previous step.";

$ies_lang["b5"]["string2"] = "緒論能引起讀者興趣。";
$ies_lang["en"]["string2"] = "The preface can arouse the interest of readers.";

$ies_lang["b5"]["string3"] = "緒論能突出問題嚴重性/衝突重點。";
$ies_lang["en"]["string3"] = "The preface can highlight the significance of the enquiry topic/ the major conflict.";

$ies_lang["b5"]["string4"] = "緒論能交代議題背景。";
$ies_lang["en"]["string4"] = "The preface can explain the background information of the issue.";

$ies_lang["b5"]["string5"] = "緒論客觀持平，不會引起讀者反感。";
$ies_lang["en"]["string5"] = "The preface is objective and will not induce readers’ antipathy.";

$ies_lang["b5"]["string6"] = "緒論能帶出研究焦點或目的。";
$ies_lang["en"]["string6"] = "The preface can bring out the enquiry focuses or objectives.";

$ies_lang["b5"]["string7"] = "修訂緒論，務求達到自我檢測所有指標。";
$ies_lang["en"]["string7"] = "Revise the preface to meet all requirements of the self-check.";

?>