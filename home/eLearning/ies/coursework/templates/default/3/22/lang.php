<?php

$ies_lang["b5"]["string1"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string1"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. If any revision is required, return to the previous step.";

$ies_lang["b5"]["string2"] = "反思深刻，能歸納三個階段的所學所長。";
$ies_lang["en"]["string2"] = "The reflection is profound and can sum up all the knowledge and skills learnt at the three stages.";

$ies_lang["b5"]["string3"] = "反思內容包含研究技巧上的反思。";
$ies_lang["en"]["string3"] = "The reflection includes that on research skills.";

$ies_lang["b5"]["string4"] = "反思內容包含研究態度上的反思。";
$ies_lang["en"]["string4"] = "The reflection includes that on research attitudes.";

$ies_lang["b5"]["string5"] = "反思內容包含個人價值觀上的反思。";
$ies_lang["en"]["string5"] = "The reflection includes that on personal values.";

$ies_lang["b5"]["string6"] = "修訂反思，務求達到自我檢測所有指標。";
$ies_lang["en"]["string6"] = "Revise the reflection to meet all requirements of the self-check.";

?>