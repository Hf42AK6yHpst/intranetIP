<?

$ies_lang["b5"]["string1"] = "你需要在這裡為題目中某些重要字詞下定義。";
$ies_lang["en"]["string1"] = "You have to define certain key words of the topic here.";

$ies_lang["b5"]["string2"] = "很多時候，題目本身會包含一些模棱兩可的字詞。如果這些字詞有多於一個可能的解釋，而不及早下定義，便會嚴重影響報告的邏輯性。 一般而言，我們會引用比較有權威的定義，例如學者或官方對某個概念的定義，加強權威性。";
$ies_lang["en"]["string2"] = "Sometimes there are words in the topic that are ambiguous. If such words with more than one meaning are not well-defined at the very beginning , the logic of the report will be affected. Generally speaking, we usually cite official definitions or those from scholars to add authority to our report.";
$ies_lang["b5"]["string3"] = "如何辨識模棱兩可的關鍵詞？";
$ies_lang["en"]["string3"] = "How to identify ambiguous key words?";

$ies_lang["b5"]["string4"] = "從題目中找出需要給予定義的關鍵詞，複製並在下方貼上：";
$ies_lang["en"]["string4"] = "Indicate which key words in the questions have to be defined, copy and paste them in the box below.";

$ies_lang["b5"]["string5"] = "研究報告簡介";
$ies_lang["en"]["string5"] = "A brief introduction of the enquiry report";

$ies_lang["b5"]["string6"] = "2. 文獻探討";
$ies_lang["en"]["string6"] = "2. Literature review";

$ies_lang["b5"]["string7"] = "「文獻探討」通常具備以下元素：";
$ies_lang["en"]["string7"] = "‘Literature review’ usually consists of the following elements:";

$ies_lang["b5"]["string8"] = "引用權威，給予題目的各個關鍵詞清楚的定義";
$ies_lang["en"]["string8"] = "Clear definitions of all key words of the topic with citations.";

$ies_lang["b5"]["string9"] = "綜合前人研究，加以評論其不足與可取之處，解釋是次研究的獨特性。";
$ies_lang["en"]["string9"] = "A synthesis of previous studies with comments on their strengths and shortcomings, and explanations on uniqueness of this study.";

$ies_lang["b5"]["string10"] = "支持與反駁前人觀點，建構自己立場。";
$ies_lang["en"]["string10"] = "Support and refutations of predecessors’ arguments to substantiate own stance.";

$ies_lang["b5"]["string11"] = "「文獻探討」不但讓你為關鍵詞下定義和展示你博覽群書的機會。能力較強的同學，甚至會在這部分反駁前人的見解，或運用他們的觀點加以發輝，增強自己的論點、理論的深度和報告的可讀性。";
$ies_lang["en"]["string11"] = "You can define the key words of the topic and demonstrate your good reads in ‘Literature review’. Students looking for more challenge can also refute or elaborate on predecessors’ arguments for their own assertions, to enhance the profundity of the arguments and readability of the report.";

$ies_lang["b5"]["string12"] = "需要給予定義的關鍵詞";
$ies_lang["en"]["string12"] = "The words that have to be defined:";

$ies_lang["b5"]["string13"] = "答案：";
$ies_lang["en"]["string13"] = "Answer:";

$ies_lang["b5"]["string14"] = "解釋：";
$ies_lang["en"]["string14"] = "Explanation:";

$ies_lang["b5"]["string15"] = "檢視練習結果";
$ies_lang["en"]["string15"] = "Check your answers";

$ies_lang["b5"]["string16"] = "重做練習";
$ies_lang["en"]["string16"] = "Redo the exercise";

$ies_lang["b5"]["string17"] = "如需要給予定義的關鍵詞多於一個，請用「,」分隔關鍵詞。";
$ies_lang["en"]["string17"] = "If there are more than one word that have to be defined, use ',' to separate them.";

/* Question */
$ies_lang["b5"]["question1"] = "政府應否在公眾地方實施全面禁煙？";
$ies_lang["en"]["question1"] = "Should smoking be completely prohibited in public areas?";

$ies_lang["b5"]["question2"] = "沉迷上網會否令青少年失去社交能力？";
$ies_lang["en"]["question2"] = "Will internet addiction of adolescents jeopardise their social ability?";

$ies_lang["b5"]["question3"] = "港人的飲食習慣如何受文化全球化影響？";
$ies_lang["en"]["question3"] = "How does cultural globalisation affect the eating habits of Hongkongers?";

$ies_lang["b5"]["question4"] = "大角嘴居民對發展高鐵持甚麼態度？";
$ies_lang["en"]["question4"] = "What are the attitudes of the residents in Tai Kok Tsui towards the construction of the Express Rail Link?";

$ies_lang["b5"]["question5"] = "其他學習經歷會否局限新高中學生的全人發展？";
$ies_lang["en"]["question5"] = "Will the whole-person development of New Senior Secondary students be restricted by Other Learning Experiences?";

$ies_lang["b5"]["question6"] = "電子學習能否有效提高我校的教育質素？";
$ies_lang["en"]["question6"] = "Can eLearning improve the quality of education of my school effectively?";

/* Answer */
$ies_lang["b5"]["answer1"] = "公眾地方";
$ies_lang["en"]["answer1"] = "public areas";

$ies_lang["b5"]["answer2"] = "全面禁煙";
$ies_lang["en"]["answer2"] = "smoking completely prohibited";

$ies_lang["b5"]["answer3"] = "沉迷上網";
$ies_lang["en"]["answer3"] = "internet addiction";

$ies_lang["b5"]["answer4"] = "青少年";
$ies_lang["en"]["answer4"] = "adolescents";

$ies_lang["b5"]["answer5"] = "社交能力";
$ies_lang["en"]["answer5"] = "social ability";

$ies_lang["b5"]["answer6"] = "港人";
$ies_lang["en"]["answer6"] = "Hongkongers";

$ies_lang["b5"]["answer7"] = "飲食習慣";
$ies_lang["en"]["answer7"] = "eating habits";

$ies_lang["b5"]["answer8"] = "文化全球化";
$ies_lang["en"]["answer8"] = "cultural globalisation";

$ies_lang["b5"]["answer9"] = "影響";
$ies_lang["en"]["answer9"] = "affect";

$ies_lang["b5"]["answer10"] = "發展高鐵";
$ies_lang["en"]["answer10"] = "construction of the Express Rail Link";

$ies_lang["b5"]["answer11"] = "態度";
$ies_lang["en"]["answer11"] = "attitudes";

$ies_lang["b5"]["answer12"] = "其他學習經歷";
$ies_lang["en"]["answer12"] = "Other Learning Experiences";

$ies_lang["b5"]["answer13"] = "新高中";
$ies_lang["en"]["answer13"] = "New Senior Secondary";

$ies_lang["b5"]["answer14"] = "全人發展";
$ies_lang["en"]["answer14"] = "whole-person development";

$ies_lang["b5"]["answer15"] = "電子學習";
$ies_lang["en"]["answer15"] = "eLearning";

$ies_lang["b5"]["answer16"] = "有效";
$ies_lang["en"]["answer16"] = "effectively";

$ies_lang["b5"]["answer17"] = "教育質素";
$ies_lang["en"]["answer17"] = "quality of education";

/* Explanation */
$ies_lang["b5"]["explain1"] = "哪裡是公眾地方的範圍？怎樣才算全面禁煙？";
$ies_lang["en"]["explain1"] = "What are public areas? How to define ‘smoking completely prohibited’?";

$ies_lang["b5"]["explain2"] = "怎樣才算沉迷上網？何謂青少年？是否按年齡界定？何謂社交能力？";
$ies_lang["en"]["explain2"] = "How to define ‘internet addiction’? Who are adolescents? Are they defined by age? What is social ability?";

$ies_lang["b5"]["explain3"] = "何謂港人？何謂飲食習慣？何謂文化全球化？受到哪方面的影響？";
$ies_lang["en"]["explain3"] = "Who are Hongkongers? What are eating habits? What is cultural globalisation? In what aspects are eating habits affected?";

$ies_lang["b5"]["explain4"] = "高鐵發展計劃的主要內容是甚麼？居民在哪方面的態度？";
$ies_lang["en"]["explain4"] = "What are the major project details of the Express Rail Link? What aspects of the residents’ attitudes deserve focus?";

$ies_lang["b5"]["explain5"] = "其他學習經歷是甚麼？新高中的理念是甚麼？全人發展是甚麼？";
$ies_lang["en"]["explain5"] = "What are Other Learning Experiences? What is the rationale of New Senior Secondary? What is whole-person development?";

$ies_lang["b5"]["explain6"] = "電子學習是指甚麼？如何衡量是否「有效」？何謂教育質素？";
$ies_lang["en"]["explain6"] = "What is eLearning? How to measure ‘effectively’? What is quality of education?";
?>