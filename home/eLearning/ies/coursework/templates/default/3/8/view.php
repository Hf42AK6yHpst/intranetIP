﻿<?php
$parStudentAnswer = $current_step_answer_arr;
$submit1_display_html = "";
$submit2_display_html = "style=\"display:none\"";

if(sizeof($current_step_answer_arr) > 0 && is_array($current_step_answer_arr))
{
	//student have answer with this question


	if($redo == 1){
		//since this is a redo , empty student answer
		$parStudentAnswer = null;
	}
	else
	{
		$submit2_display_html = '';
		$submit1_display_html = "style=\"display:none\"";
	}
}

$html_question = generateQuestion($_question[1],$parStudentAnswer);
?>
<script>
var question_answer_sperator = "<?=$ies_cfg["question_answer_sperator"]?>";
var new_set_data_sperator = "<?=$ies_cfg["new_set_question_answer_sperator"]?>"; 
var comment_answer_sperator = "<?=$ies_cfg["comment_answer_sperator"]?>";
var question_code_ary = new Array();
<?php
for($i=0,$j=sizeof($question_code);$i<$j;$i++){
	echo "question_code_ary[".$i."] = \"".$question_code[$i]."\";\n";
}
?>
function handleAnswer(){
	var studentAllAns = "";
	var TeacherViewStdAns = "";
	var dummy = " "; 
	var cnt = 0;
	$("input[name='std_ans[]']").each(function(key, val){
		var _studentAns = $(this).val();
		
		if(studentAllAns == ""){
			studentAllAns = question_code_ary[cnt] + question_answer_sperator + _studentAns;
		}
		else{
			studentAllAns += new_set_data_sperator + question_code_ary[cnt] + question_answer_sperator + _studentAns;
		}
		
		if(TeacherViewStdAns == ""){
			TeacherViewStdAns = (cnt+1) + ". " + _studentAns;
		}
		else{
			TeacherViewStdAns += " " + (cnt+1) + ". " + _studentAns;
		}
		cnt++;
	});
	try{
		studentFinalAns = studentAllAns  + comment_answer_sperator + dummy + question_answer_sperator + TeacherViewStdAns;
		$("input[name^=question[radio][1]]").val(studentFinalAns);
	}
	catch(err){
		alert(err.toString());
	}
}
function ReDoStep(jParStepID,jParSchemID){
 	 window.location = "coursework.php?step_id="+jParStepID+"&scheme_id="+jParSchemID+"&redo=1";
}
</script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
    	<div class="show_intro" style="width:100%">
    		<script>
    			function toggle_hint_block(hint_id){
    				obj = document.getElementById(hint_id);
					if(obj.style.display != 'none'){
						obj.style.display = 'none';
					}
					else{
						obj.style.display = 'block';
					}
				}
    		</script>
    		<a class="task_intro" href="javascript:toggle_hint_block('hint_content_01')"><?=$ies_lang[$_lang]["string5"]?></a>
			<p class="spacer"></p> 
			<div id="hint_content_01" class="intro_content" style="display:none;width:98%">
				<b><?=$ies_lang[$_lang]["string6"]?></b><br/>
				<?=$ies_lang[$_lang]["string7"]?>
				<ol>
					<li><?=$ies_lang[$_lang]["string8"]?></li>
					<li><?=$ies_lang[$_lang]["string9"]?></li>
					<li><?=$ies_lang[$_lang]["string10"]?></li>
				</ol>
				<?=$ies_lang[$_lang]["string11"]?>
			</div>
		</div>
		<p class="spacer"></p><br/>
		<?=$ies_lang[$_lang]["string1"]?><br/><br/>
		<?=$ies_lang[$_lang]["string2"]?><br/><br/>
		<a href="tips/stage3_lightbox_03.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo"><?=$ies_lang[$_lang]["string3"]?></a>
		<br/><br/>
		<span class="task_instruction_text"><?=$ies_lang[$_lang]["string4"]?></span><br/>
		<span class="task_instruction_text" <?=$submit1_display_html?>>(<?=$ies_lang[$_lang]["string17"]?>)</span>
		<br/>
		<div class="IES_task_form">
        	<div class="wrapper" id="question_area">
				<?=$html_question?>
				
			</div>
        	<div class="view_result" id="submit1" <?=$submit1_display_html?>>
        		<input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="handleAnswer();doSaveStep(<?=$step_id?>,true);" value=" <?=$ies_lang[$_lang]["string15"]?> " />
        	</div>  
            <div class="view_result" id="submit2" <?=$submit2_display_html?>>
            	<input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="ReDoStep(<?=$step_id?>,<?=$scheme_id?>)" value=" <?=$ies_lang[$_lang]["string16"]?> " />
            </div>  
		</div>
		<div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
	      <div style="float:right; padding-top:5px">
	        <!--input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" 儲存 " /-->
	      </div>
	    </div>
	<input type = "hidden" name = "question[radio][1]" id="question1" value="">
	</div>     
	<!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['NextStep']?> " />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->