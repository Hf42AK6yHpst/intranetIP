<?php

$ies_lang["b5"]["string1"] = "你可以根據研究分析，綜合各個觀點，提出一些簡單的建議回應題目。";
$ies_lang["en"]["string1"] = "You can refer to the research findings, integrate all viewpoints, and propose some simple recommendations to address the enquiry topic.";

$ies_lang["b5"]["string2"] = "高分小貼士";
$ies_lang["en"]["string2"] = "Tips for good grades";

$ies_lang["b5"]["string3"] = "任何建議、論點或解決方法，都必須根據探究得來的數據和資料而產生，不可以無中生有。";
$ies_lang["en"]["string3"] = "Any recommendation, assertion or solution must be based on the data collected in the enquiry. Fabrication is not allowed.";

$ies_lang["b5"]["string4"] = "所有你提出的建議、論點或解決方法，都應該緊密聯繫焦點問題。事實上，提出建議的目的就是為了解答焦點問題。";
$ies_lang["en"]["string4"] = "All recommendations, assertions or solutions should be closely related to the focus questions. In fact, the recommendation is supposed to be made to address the focus questions.";

$ies_lang["b5"]["string5"] = "如果情況許可，可以根據探究所得的數據/資料，推論出將來的一些轉變或趨勢。";
$ies_lang["en"]["string5"] = "Predict future changes or trends according to the data collected in the enquiry if possible.";

$ies_lang["b5"]["string6"] = "如何撰寫高質量的建議？";
$ies_lang["en"]["string6"] = "How to write up high-quality recommendations?";

$ies_lang["b5"]["string7"] = "階段一撰寫的探究題目：<br/>";
$ies_lang["en"]["string7"] = "The topic for enquiry written up at Stage 1:<br/>";

$ies_lang["b5"]["string8"] = "階段二撰寫的探究題目：<br/>";
$ies_lang["en"]["string8"] = "The topic for enquiry written up at Stage 2:<br/>";

$ies_lang["b5"]["string9"] = "階段三撰寫的研究目的：<br/>";
$ies_lang["en"]["string9"] = "The enquiry objectives written up at Stage 3:<br/>";

$ies_lang["b5"]["string10"] = "階段三撰寫的焦點問題：<br/>";
$ies_lang["en"]["string10"] = "The focus questions written up at Stage 3:<br/>";

$ies_lang["b5"]["string11"] = "階段三撰寫的分析結果：<br/>";
$ies_lang["en"]["string11"] = "The analysis of findings written up at Stage 3:<br/>";

$ies_lang["b5"]["string12"] = "撰寫研究建議或個人啟示：";
$ies_lang["en"]["string12"] = "Write up the research recommendations or personal inspiration.";

?>