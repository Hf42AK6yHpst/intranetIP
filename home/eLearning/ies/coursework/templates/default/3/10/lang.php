<?php

$ies_lang["b5"]["string1"] = "為了補充一手資料的不足，我們要在報告中歸納一些與議題相關的研究報告，或引用一些具代表性的新聞和官方數字。引用文獻資料時，要注意以下事項：";
$ies_lang["en"]["string1"] = "To supplement what is lacking in the first-hand information, we have to quote some issue-related literature in our report, or cite some representative news items and official figures. The following should be considered when citations are made:";

$ies_lang["b5"]["string2"] = "引用文獻注意事項";
$ies_lang["en"]["string2"] = "Citation guide";

$ies_lang["b5"]["string3"] = "階段一撰寫的探究題目：<br/>";
$ies_lang["en"]["string3"] = "The topic for enquiry written up at Stage 1:<br/>";

$ies_lang["b5"]["string4"] = "階段二撰寫的探究題目：<br/>";
$ies_lang["en"]["string4"] = "The topic for enquiry written up at Stage 2:<br/>";

$ies_lang["b5"]["string5"] = "階段三撰寫的研究背景：<br/>";
$ies_lang["en"]["string5"] = "The background information written up at Stage 3:<br/>";

$ies_lang["b5"]["string6"] = "階段三撰寫的研究目的：<br/>";
$ies_lang["en"]["string6"] = "The enquiry objectives written up at Stage 3:<br/>";

$ies_lang["b5"]["string7"] = "階段一撰寫的相關知識與概念：<br/>";
$ies_lang["en"]["string7"] = "The related concepts and relevant knowledge written up at Stage 1:<br/>";

$ies_lang["b5"]["string8"] = "撰寫文獻探討部分時，可翻查搜集得來的文獻資料。緊記突出參考資料的重點，並提供相關的資料來源。";
$ies_lang["en"]["string8"] = "You can look up the references collected when writing up the ‘Literature review’. Remember to highlight the main points of the references and state the sources.";

$ies_lang["b5"]["string9"] = "撰寫文獻探討，並引述資料來源：";
$ies_lang["en"]["string9"] = "Writing up ‘Literature review’ and citing sources:";

?>