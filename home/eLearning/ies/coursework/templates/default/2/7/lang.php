<?php
$ies_lang["b5"]["string1"] = "現在，你可根據所選定的研究方法，設計精確的研究工具來蒐集資料。設定研究工具時，緊記要回應探究問題。";
$ies_lang["en"]["string1"] = "Based on your chosen research method(s), design your research tool(s) concisely for data collection. Bear the focus questions in mind when designing your research tool(s).";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "要開始設計有關研究工具，請點擊「製作」。你可以選擇以下任何一種或以上的研究方式，並同時製作多份問卷或記錄表。";
$ies_lang["en"]["string2"] = "Get started to design your research tool(s). Click on ‘Create’. You may use any one or more of the research method(s) below, and create more than one questionnaire or record form.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

?>