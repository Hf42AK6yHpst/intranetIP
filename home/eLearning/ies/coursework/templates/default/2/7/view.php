<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script>
$(document).ready(function() {
	
	var defaultSurveyType = '';
	defaultSurveyType = $("#defaultSurveyType").val();

	defaultSurveyType = (defaultSurveyType == '') ? 1: defaultSurveyType;

	//this page may load from "form_update.php", then active the TAB with the return created survey type
	$("#sType_" + defaultSurveyType).addClass("current");
	$("#area_sType_" + defaultSurveyType).show();

	$('.surveyType').click(function(){
		
		$("#area_sType_1").hide();
		$("#area_sType_2").hide();
		$("#area_sType_3").hide();				

		$("#sType_1").removeClass("current");
		$("#sType_2").removeClass("current");
		$("#sType_3").removeClass("current");
		
		var _id = $(this).attr("id");
		var _displayElement = 'area_'+_id;

		$("#"+_displayElement).show();
		$(this).addClass("current");

		
	});
});
</script>
<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

    <?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>
		
  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
    	<?=$ies_lang[$_lang]["string1"]?><br /><br />
		<?=$ies_lang[$_lang]["string2"]?><br /><br />
      		
    </div>

    <?=$html_editsurveyTable?>

    <!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Back']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->

<?php
//FOR RETURN TO THIS PAGE from /home/eLearning/ies/survey/form_update.php, 
//CONTROL THE VALUE TO SET INTERVIEW OR OBSERVATION AS DEFAULT TAB
?>
<input type = "hidden" name="defaultSurveyType" id = "defaultSurveyType" value="<?=$defaultSurveyType?>"></input>