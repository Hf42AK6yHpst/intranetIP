<?php
$html_textarea = '<textarea style="width:100%; height:50px" name="question[textarea][1]" id="textbox_">'.$answer[1].'</textarea> ';
$html_textareaWithTitle = libies_ui::getTextareaDisplayWithTitle($ies_lang[$_lang]["string19"], $html_textarea);
?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

    <?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
      <?=$ies_lang[$_lang]["string1"]?><br /><br />

   <?=$ies_lang[$_lang]["string2"]?><br /><br />

	<b><?=$ies_lang[$_lang]["string3"]?></b>
		<ol>
		  <li><?=$ies_lang[$_lang]["string4"]?></li>
		  <li><?=$ies_lang[$_lang]["string5"]?></li>
		  <li><?=$ies_lang[$_lang]["string6"]?></li>
		</ol> 


 
	<?=$ies_lang[$_lang]["string7"]?>
		<ul>
		  <li><?=$ies_lang[$_lang]["string8"]?></li>
		  <li><?=$ies_lang[$_lang]["string9"]?></li>
		  <li><?=$ies_lang[$_lang]["string10"]?></li> 
		  <li><?=$ies_lang[$_lang]["string11"]?></li>
		  <li><?=$ies_lang[$_lang]["string12"]?></li>
		</ul>


	<?=$ies_lang[$_lang]["string13"]?><br />
	
	<a href="tips/stage2_C1_01.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo"><?=$ies_lang[$_lang]["string20"]?></a>
   	
     <br /><br />
<span style="text-decoration: underline"><?=$ies_lang[$_lang]["string14"]?></span><br /><br />

<?=$ies_lang[$_lang]["string15"]?><br /><br />

<?=$ies_lang[$_lang]["string16"]?><br /><br />

<span style="text-decoration: underline"><?=$ies_lang[$_lang]["string17"]?></span><br /><br />

<?=$ies_lang[$_lang]["string18"]?><br /><br />

 </div>
       
	<?=$html_textareaWithTitle?>

    <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
      <div style="float:right; padding-top:5px">
        <input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
      </div>
    </div> 
    <!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input id="save_submit" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Submit']?> " />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Back']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->