<?php
$ies_lang["b5"]["string1"] = "最後，說明你所收集的數據和資料，是屬於一手或二手資料，還是兩者皆有。";
$ies_lang["en"]["string1"] = "Lastly, state if the data collected belongs to first-hand or second-hand information, or both.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "參考你所選擇的研究方法和工具，說明你收集的數據和資料屬於哪類：";
$ies_lang["en"]["string2"] = "Refer to the research method(s) and tool(s) you have opted for and state the source of information your data belongs to.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "總括來說，是次收集的數據（屬於一手資料/屬於二手資料/包括一手和二手資料）。";
$ies_lang["en"]["string3"] = "To sum up, the data collected this time (belongs to first-hand information / belongs to second-hand information / consists of first-hand and second-hand information).";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/
?>