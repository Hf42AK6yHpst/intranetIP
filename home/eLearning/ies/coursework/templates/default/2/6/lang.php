<?php
$ies_lang["b5"]["string1"] = "解釋了研究方法的設計理念後，這部分要求你把研究的進度清晰地記錄下來。請定期填寫研究的最新進度，輸入你已完成的工作項目、日期和工作目的、遇到的困難及解決方法。";
$ies_lang["en"]["string1"] = "After justifying the rationale behind the design of your research method(s), record the progress of the data collection process clearly. Enter regularly the dates, details of the task done, task objectives, difficulties encountered and solutions in your research diary.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "定期填寫研究日誌：";
$ies_lang["en"]["string2"] = "Fill in the research diary regularly.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "日期";
$ies_lang["en"]["string3"] = "Dates";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "主要工作";
$ies_lang["en"]["string4"] = "Major tasks done";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "工作目的";
$ies_lang["en"]["string5"] = "Task objectives";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "遇到的困難";
$ies_lang["en"]["string6"] = "Difficulties encountered";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "解決方法";
$ies_lang["en"]["string7"] = "Solutions";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

?>