<?php

$target_taskID = $objIES->getTaskIDByTaskInfo($scheme_id, 1, "topic");
$SnapshotID = $objIES->getTaskSnapshotAnswer($target_taskID, $UserID);
$comments = $objIES->getCommentArr($SnapshotID[0]['SnapshotAnswerID']);
$html_comment = "";
$html_prev_task_answer = nl2br($objIES->gettaskAnswer($target_taskID, $UserID));

if(!empty($html_prev_task_answer)){
	$html_prev_task_answer = "{$Lang['IES']['HandinAnswer']}：<br />".$html_prev_task_answer;
}
else{
	$html_prev_task_answer = "{$Lang['IES']['HandinAnswer']}：<br /> --";
}

if(!empty($comments)){
	$html_comment .= $Lang['IES']['FirstStageTeacherComment']."： <br />";
	foreach($comments as $acomment){
		$html_comment .= $acomment['Comment']."<br />";
	}
}

$html_templateFile = "view.php";
?>