<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<script>
$(document).ready(function() {

	$("#sType_1").addClass("current");
	$("#area_sType_1").show();

	$('.surveyType').click(function(){
		
		$("#area_sType_1").hide();
		$("#area_sType_2").hide();
		$("#area_sType_3").hide();				

		$("#sType_1").removeClass("current");
		$("#sType_2").removeClass("current");
		$("#sType_3").removeClass("current");
		
		var _id = $(this).attr("id");
		var _displayElement = 'area_'+_id;
		$("#"+_displayElement).show();
		$(this).addClass("current");

		
	});
});
</script>

<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

    <?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
     	<?=$ies_lang[$_lang]["string1"]?><br /><br />

		<?=$ies_lang[$_lang]["string2"]?><br /><br />

		<?=$ies_lang[$_lang]["string3"]?>
			<ol>
				<li><?=$ies_lang[$_lang]["string4"]?></li>
				<li><?=$ies_lang[$_lang]["string5"]?></li>
			</ol> 

		<?=$ies_lang[$_lang]["string6"]?><br /><br />
      

    </div>
    
	<?=$html_table?>

    <!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Back']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->