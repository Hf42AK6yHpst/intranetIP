<?php
$ies_lang["b5"]["string1"] = "開始進行研究前，要先假設某些重要因果關係，讓之後的討論與分析得以成立。";
$ies_lang["en"]["string1"] = "Having some important causal relationships hypothesised before the study starts will help establish subsequent discussions and analyses.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "例如：「網上遊戲如何影響青少年建立人際關係」這題目，其中的一個假設可能是：網上遊戲會對青少年建立人際關係構成正面或負面影響。有了這個假設，我們就能把「網上遊戲」和「青少年建立人際關係」兩個概念建立關係。";
$ies_lang["en"]["string2"] = "For example, one of the possible hypotheses for the research question 'How do online games influence teenagers in terms of interpersonal relationship building?' could be 'Online games will have positive or negative influences on teenagers' interpersonal relationship building'. With such hypothesis, the relationship between the two concepts – 'online games' and 'teenage interpersonal relationship building' - is established.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "但如果題目要求開放式答案而非驗證因果關係，例如：「探討低收入人士對最低工資立法的觀點」。在這情況下，假設對這類題目的作用未必太大。是否撰寫，則要視乎個別題目需要和老師的要求了。";
$ies_lang["en"]["string3"] = "However, if the research question is an open-ended one and if proving the casual relationships of a certain concepts is not the objective, a hypothesis is not quite necessary in this case. You may still write one if appropriate or if required by your teacher. A research question of such kind will be 'To study how low income people view the legislation of minimum wages'. ";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "你可因應題目需要或老師要求，在下列位置撰寫「研究假設」。";
$ies_lang["en"]["string4"] = "Write a research hypothesis if appropriate or if it is your teacher's requirement. Just leave the section blank if you do not have a hypothesis for your topic.";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

?>