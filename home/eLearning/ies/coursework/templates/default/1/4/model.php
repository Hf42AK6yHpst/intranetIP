﻿<?php

function getQuestionJS($_question)
{
  $js_option_arr = "var optgroup_arr = {\n";
  foreach($_question AS $_q_no => $_optgroup_arr)
  {
    foreach($_optgroup_arr AS $_optgroup_title => $_optgroup_opt_arr)
    {
      $js_option_arr .= "\"".$_optgroup_title."\" : [";
      for($i=0; $i<count($_optgroup_opt_arr); $i++)
      {
        $_opt = $_optgroup_opt_arr[$i];
      
        $js_option_arr .= "\"".$_opt."\",\n";
      }
      $js_option_arr = substr($js_option_arr, 0, -2)."],\n";
    }
  }
  $js_option_arr = substr($js_option_arr, 0, -2)."}\n";
  
  return $js_option_arr;
}


$_question[1] = array(
  $ies_lang[$_lang]["choice1"] =>
    array(
      $ies_lang[$_lang]["choice2"],
      $ies_lang[$_lang]["choice3"],
      $ies_lang[$_lang]["choice4"],
      $ies_lang[$_lang]["choice5"],
      $ies_lang[$_lang]["choice6"]
    ),
  $ies_lang[$_lang]["choice7"] =>
    array(
      $ies_lang[$_lang]["choice8"],
      $ies_lang[$_lang]["choice9"],
      $ies_lang[$_lang]["choice10"],
      $ies_lang[$_lang]["choice11"],
      $ies_lang[$_lang]["choice12"]
    ),
  $ies_lang[$_lang]["choice13"] =>
    array(
      $ies_lang[$_lang]["choice14"],
      $ies_lang[$_lang]["choice15"]
    ),
  $ies_lang[$_lang]["choice16"] =>
    array(
      $ies_lang[$_lang]["choice17"],
      $ies_lang[$_lang]["choice18"],
      $ies_lang[$_lang]["choice19"],
      $ies_lang[$_lang]["choice20"],
      $ies_lang[$_lang]["choice21"],
      $ies_lang[$_lang]["choice22"]
    ),
  $ies_lang[$_lang]["choice23"] => array($ies_lang[$_lang]["choice24"])
);

$js_option_arr = getQuestionJS($_question);

$html_templateFile = "view.php";
?>
