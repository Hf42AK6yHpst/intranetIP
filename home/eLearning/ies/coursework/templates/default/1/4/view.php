﻿<script language="JavaScript">

<?=$js_option_arr?>

var pattern_str = "([XYZ])";
var hidden_delim = "##";

function CreateInputObj(input_name, input_val)
{
  var inputObj =  $("<input>").attr("name", "question["+input_name+"][1][]").attr("type", "hidden").val(input_val);

  return inputObj;
}

/**
 * ADD STUDENT SELECT RECORD TO THE RIGHT PANEL
 **/
function AddInputEntry(to_select_id, display_text, input_obj)
{
  $("#"+to_select_id)
    .append($("<div></div>").attr("class", "selected")
      .append($("<span></span>").html(display_text+" ").append(input_obj))
      .append($("<a></a>").attr("class", "deselect").click(function(){
        // bind action to remove button
        $(this).parent().remove();

		//update user select number
        doCountOptionSelected();
      }))
      .append($("<div></div>").attr("class", "clear"))
    );
}

// Move selected options, and bind action to remove button
function MoveSelectedOptions(from_select_id, to_select_id)
{
  var pattern = new RegExp(pattern_str, "g");

	$("#"+from_select_id+" option:selected").each(function(i){
    var val = $(this).val();
    if(val == "<?=$ies_lang[$_lang]["choice24"]?>")
    {
		//since it is 自訂, insert the replace variable "X" into hidden
      var text = val + ": <input type=\"text\" style=\"width:70%\" value=\"\" />";
      val = "X";
    }
    else
    {
		//replace caption "X, Y" with a input box for user input
      var text = $(this).val().replace(pattern, "<input type=\"text\" class=\"input_short\" value=\"$1\" />");
    }
    // Replace: X ==> ##X##
    val = val.replace(pattern, hidden_delim+"$1"+hidden_delim);
    var inputObj =  CreateInputObj("select_mod_custom", val);

    // Add selected options , 
    AddInputEntry(to_select_id, text, inputObj);
  });
  
  doCountOptionSelected();
  $("#"+from_select_id).width($("#"+from_select_id).parent().width());
}

// Update selected option count 
function doCountOptionSelected(){
  var count = $("#target").find(".selected").size();
  
  $('#optionCount').html(count);
}

// Replace hidden form input value with input text value
// ##X## ==> X
function doReplaceHiddenValue(){
  $('#target').find('.selected').each(function(){
    var hiddenInput = $(this).find('input[type=hidden]');
    var pattern = new RegExp(hidden_delim+pattern_str+hidden_delim);

    $(this).find('input[type=text]').each(function(){
      hiddenInput.val(hiddenInput.val().replace(pattern, $(this).val()));
    });
  });
}

$(document).ready(function(){
  // Initialize options and checked saved options
  $.each(optgroup_arr, function(optgroup_title, opt_arr) {
    $('#source').append($('<optgroup></optgroup>').attr("label", optgroup_title));
    $.each(opt_arr, function(key, val) {
      var optionObj = $('<option></option>').val(val).html(val);
      $('#source optgroup[label='+optgroup_title+']').append(optionObj);
    });
  });
  
  // Bind action to transfer button
  $('#transfer').click(function(){
    MoveSelectedOptions("source", "target");
  });
  
  // Bind action to remove all entry link
  $('.del_all').click(function(){
    $('#target').find('.selected').each(function(){
      $(this).remove();
    });
    doCountOptionSelected();
  });
  
  // Bind action to save button
  $('#replace_save').click(function(){
    doReplaceHiddenValue();
    
    // Submit for save
    $.ajax({
      url:      "step_update.php",
      type:     "POST",
      data:     $("#form1").serialize(),
      error:    function(xhr, ajaxOptions, thrownError){
                  alert(xhr.responseText);
                },
      success:  function(xml){
                  $('#target').find('.selected').each(function(){
                    $(this).find('input[type=text]').each(function(){
                      var myVal = $(this).val();
                      $(this).replaceWith(myVal);
                    });
                  });
                  
                  date = $(xml).find("date").text();
                  $("#lastModifiedDate").html(date);
                }
    });
  });
  
<?php for($i=0; $i<count($answer[1]["custom"]); $i++) { ?>
  inputObj = CreateInputObj("select_mod_custom", "<?=$answer[1]["custom"][$i]?>");
  AddInputEntry("target", "<?=$answer[1]["custom"][$i]?>", inputObj);
<?php } ?>
  
  doCountOptionSelected();
});

</script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
    <?=$html_answer_display?>

    <div class="task_content">
      <?=$ies_lang[$_lang]["string1"]?><br /><br />

	<?=$ies_lang[$_lang]["string2"]?><br /><br />

<?=$ies_lang[$_lang]["string3"]?><br />
	   <a href="tips/narrow_range.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="task_demo thickbox"><?=$ies_lang[$_lang]["string4"]?></a>
    </div>
	<br /><br />  
	<?=$ies_lang[$_lang]["string5"]?><br /><br />

    
    <table class="form_table" style="width:98%;font-size:15px">
      <col class="field_title" />
      <col  class="field_c" />
      <col />
      <tr>
        <td> </td>
        <td colspan="2"> </td>
      </tr>
      <tr>
        <td colspan="2" style="width:44%">
          <div style="text-align:left;padding-right:5px;padding-bottom:5px">

<!--<select id="groupSelection" onchange="member_selection_block.select_members_in_group(this)">
<option value="allStudent">All Students</option>

</select>-->
          </div>
          <select id="source" class="selectAnsList" multiple>

          </select>
        </td>
        <td style="text-align:center;padding-top:100px;width:6%">
          <input id="transfer" class="formbutton" type="button" value=">>" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" />
        </td>
        <td style="width:44%">
          <div class="task_instruction_box"><?=$ies_lang[$_lang]["string6"]?></div>
          <div id="target" class="task_selected_box">
            <h5><span style="float:left;"><span class="stu_edit"></span><?=$ies_lang[$_lang]["string7"]?><span id="optionCount"></span> </span> <span style="float:right;"> <a href="javascript:;" class="del_all"><?=$ies_lang[$_lang]["string8"]?></a></span>
              <p class="spacer"></p>
            </h5>
          </div>
          <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span> <!-- added 18-05-2010 by ELI -->
            <div style="float:right; padding-top:5px">
              <input id="replace_save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
            </div>
          </div><!-- added 18-05-2010 by ELI -->
        </td>
      </tr>
    </table>
  </div>                         
  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$prev_step_id?>);" value=" <?=$Lang['IES']['LastStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['NextStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doReplaceHiddenValue(); doSaveStep(<?=$next_step_id?>,true);" value=" <?=$Lang['IES']['SaveAndNextStep']?> " />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />
  </div></div>
</div> <!-- task_wrap end -->