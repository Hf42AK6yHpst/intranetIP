<?

$ies_lang["b5"]["string1"] = "確定了題目後，你需要解釋在探究過程中將會應用哪些相關概念，並解釋為什麼與題目相關。此外，你需要清楚為題目內的一些關鍵詞下定義，因為不同的定義，可能會產生不同的結論。要讓整份報告具有嚴謹邏輯，清楚給關鍵詞下定義是必須的。";
$ies_lang["en"]["string1"] = "After the topic is decided, you have to explain what related concepts will be applied in the enquiry process and in what sense they are connected with the topic. Besides, you have to define keywords clearly because different definitions will lead to different conclusions. To produce a report that is logically reasoned, defining keywords clearly is essential. ";

$ies_lang["b5"]["string2"] = "同學可以參考以下角度，協助撰寫相關概念部分： ";
$ies_lang["en"]["string2"] = "The following perspectives will help you write up the part about related concepts:";

$ies_lang["b5"]["string3"] = "「探究過程將涉及一些相關概念和知識，包括……」 ";
$ies_lang["en"]["string3"] = "'The process of enquiry will involve some related concepts and relevant knowledge, including…'";

$ies_lang["b5"]["string4"] = "「因為概念與題目有密切關係，例如……」";
$ies_lang["en"]["string4"] = "'Owing to the close relationships between the concept and the topic, e.g., …'";

$ies_lang["b5"]["string5"] = "「這些相關概念能引發新的思考角度，例如……」";
$ies_lang["en"]["string5"] = "'These related concepts will bring new perspectives, e.g….'";

$ies_lang["b5"]["string6"] = "「這些相關概念能引發多角度討論，例如……」";
$ies_lang["en"]["string6"] = "'These related concepts will enhance multi-perspective discussions, e.g….'";

$ies_lang["b5"]["string7"] = "「就我所知，到目前為止，仍未有研究採用這個切入點去分析，所以……」";
$ies_lang["en"]["string7"] = "'To the best of my knowledge, there has not been a study adopting such a perspective for analysis at present. Therefore, …'";

$ies_lang["b5"]["string8"] = "你也可以用下列句型給關鍵詞下定義：";
$ies_lang["en"]["string8"] = "You may like to define keywords using the following sentence structures:";

$ies_lang["b5"]["string9"] = "(某概念) 是指……";
$ies_lang["en"]["string9"] = "The concept of…refers to….";

$ies_lang["b5"]["string10"] = "學者曾為（某概念）下過定義……";
$ies_lang["en"]["string10"] = "Scholars have once defined the concept of…";

$ies_lang["b5"]["string11"] = "請解釋相關概念與題目的關係︰";
$ies_lang["en"]["string11"] = "Explain the relationship between related concepts and your topic.";



?>