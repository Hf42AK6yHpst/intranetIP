<?
$ies_lang["b5"]["string1"] = "議訂題目通常從感興趣的議題出發，然後逐步收窄焦點。<br /><br /> 初步構思的範圍，可以較為廣泛，但焦點會比較模糊，例如有同學想做有關「日本流行文化」的探究，但至於如何定義「日本流行文化」、怎樣研究、目的和焦點是什麼？都未必一開始就能想得很清楚。請放心，我們會逐步引導你收窄範圍。";
$ies_lang["en"]["string1"] = "A topic normally emerges from an issue of your interest, with the scope narrowed down step by step.<br /><br />The scope of enquiry could be wider at the initial brainstorming stage, and the focus less specific. For example, when a student wants to enquire about ‘Japanese popular culture’, he may not have thought of issues like the definition of Japanese pop culture, the way to conduct the study, the objectives and focuses of the enquiry study right from the start. Take it easy!";

$ies_lang["b5"]["string2"] = "如果你現在毫無頭緒，不妨參考以下角度，寫出一個你認為最好的研究話題。<br /><br />讓你感興趣的研究話題可以是：";
$ies_lang["en"]["string2"] = "You will be guided step by step to narrow down the scope.<br /><br />If you still have no idea how to set a research topic, you are advised to take into account the following perspectives before coming up with the best one.<br /><br />Research topic that interests you could be:";

$ies_lang["b5"]["string3"] = "自己最熟悉的話題";
$ies_lang["en"]["string3"] = "any topic you are most familiar with";

$ies_lang["b5"]["string4"] = "最貼近自己生活的話題";
$ies_lang["en"]["string4"] = "any topic with a close connection to your daily life";

$ies_lang["b5"]["string5"] = "讓你感到好奇的社會議題";
$ies_lang["en"]["string5"] = "any social issue that arouses your curiosity";

$ies_lang["b5"]["string6"] = "富爭議性的社會議題";
$ies_lang["en"]["string6"] = "any social issue that is controversial";

$ies_lang["b5"]["string7"] = "最近才出現的社會現象";
$ies_lang["en"]["string7"] = "any recent social phenomenon";


$ies_lang["b5"]["string8"] = "請嘗試列出你最感興趣的一個議題︰（如：補習/ 補習的效用/ 補習風氣）";
$ies_lang["en"]["string8"] = "Write down an issue you are most interested in (e.g. tutorial classes/ the effectiveness of tutorial classes/ the trend of attending tutorial classes).";

?>