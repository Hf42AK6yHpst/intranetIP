<?php
$ies_lang["b5"]["string1"] = "在這部分，我們需要說明將會採用甚麼方法去探究，及如何尋找所需資料。";
$ies_lang["en"]["string1"] = "In this section, you have to tell what research method(s) you are going to use and how you are going to collect the information needed.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "探究角度";
$ies_lang["en"]["string2"] = "Enquiry perspective(s)";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "通識科著重多角度思考，要使內容達致客觀可信，我們需要考慮不同的探究角度，並帶出不同人士或持分者的看法，加以分析後，再引導讀者從不同角度和觀點來認識議題。";
$ies_lang["en"]["string3"] = "Liberal Studies emphasise multi-perspective thinking. To ensure the objectivity and credibility of the content, we have to consider different perspectives for enquiry, bring up the viewpoints of individuals or stakeholders, and guide readers to look at the issue from different angles based on your analyses. ";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "例如：探討有關新高中教育議題，我們的探究角度可能是校長、家長、老師、學生、社區、僱主、教育局等持分者的觀點。";
$ies_lang["en"]["string4"] = "For example, our enquiry perspectives for the study on the New Senior Secondary (NSS) education issues could be the viewpoints of different stakeholders, e.g. school principals, parents, teachers, students, the community, employers, the Education Bureau, etc. ";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "研究對象";
$ies_lang["en"]["string5"] = "Research objects";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "研究對象是指你在第二階段收集數據時，所要接觸的人物。在填寫探究計劃書時思考研究對象是非常重要的。若同學能及時發現你的研究對象很難接觸得到，就需要當機立斷，及時調整題目了！";
$ies_lang["en"]["string6"] = "Research objects are the people you have to get in touch for data collection at the second stage. To have careful consideration of your research objects at the project proposal stage is very important. If you find your research objects difficult to reach at the moment, take decisive action to revise the research question immediately. ";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "例如：探討有關新高中教育議題，我們的研究對象就可能是校長、副校長、高中老師、高中生家長、本校高中學生、教育局官員等。";
$ies_lang["en"]["string7"] = "For example, the target objects for the study on the New Senior Secondary (NSS) education issues might be school principals, vice principals, teachers, senior secondary students, their parents, officials of the Education Bureau, etc.";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

$ies_lang["b5"]["string8"] = "這時候，你可能發現自己的題目涉及很多研究對象，而且有些研究對象似乎不太可能接受我們訪問（例如：教育局局長），但我們卻可以從其他二手資料理解他們的觀點。但碰到某些比較極端的例子，例如：當研究對象是未婚媽媽、罪犯，或吸毒者時，你就要重新思考一下探究角度了。";
$ies_lang["en"]["string8"] = "Now, you may have noticed that your topic has involved many research objects and some of them such as the Secretary for Education may not be available for an interview, but we will know their viewpoints from other secondary sources. Yet, in some extreme cases when your research objects happen to be unmarried pregnant ladies, offenders, or drug addicts, you are advised to reconsider your enquiry perspective.";
/*
<?=$ies_lang[$_lang]["string8"]?>
*/

$ies_lang["b5"]["string9"] = "請根據你的題目和焦點問題，清楚列明你的探究角度和研究對象︰";
$ies_lang["en"]["string9"] = "Specify your enquiry perspective(s) and research object(s) based on your topic for enquiry and focus questions.";
/*
<?=$ies_lang[$_lang]["string9"]?>
*/
?>