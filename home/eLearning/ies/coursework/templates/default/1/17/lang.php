<?php
$ies_lang["b5"]["string1"] = "由構思探究題目到搜集相關資料，以至訂立初步探究計劃的工作項目，你從中學習到甚麼呢？在此部分，你要反思並總結這一階段的學習歷程。你可以一邊參考你的反思手記，一邊回答以下的問題：";
$ies_lang["en"]["string1"] = "What have you learnt through designing the topic for enquiry, collecting related information and arranging the task items of the preliminary work plan? In this section, you have to write up your reflection and summarise the course of learning for this stage. You may refer to the Reflection Log as you answer the following questions. ";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "你的探究目的是甚麼呢？";
$ies_lang["en"]["string2"] = "What are your enquiry objectives?";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "你所選的探究題目是否與你有切身關係呢？";
$ies_lang["en"]["string3"] = "Does the chosen topic for enquiry have a close connection with you?";
/*
<?=$ies_lang[$_lang]["strin3"]?>
*/

$ies_lang["b5"]["string4"] = "在撰寫探究計劃書時，你有甚麼得著和啟發呢（例如：學術知識、概念、人際技巧、解難能力）？";
$ies_lang["en"]["string4"] = "What have you learnt and how are you inspired when writing up the project proposal? (in terms of academic knowledge, concepts, interpersonal skills, problem solving skills, etc.)";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "在這階段的工作後，你經歷了哪些重要事件？觀察到哪些重要事物呢？";
$ies_lang["en"]["string5"] = "What are the major events you have experienced after this stage? What are the major events you have observed? ";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "這些重要事件或事物對你的個人價值觀或人際關係有甚麼影響？";
$ies_lang["en"]["string6"] = "What are the influences of these major matters on your personal values or interpersonal relationships?";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "回顧你的學習歷程，參考右上方的反思手記，並整理階段一的「反思」︰";
$ies_lang["en"]["string7"] = "Please refer to the Reflection Log on the top right hand corner as you review your course of learning and tidy up your reflection for Stage 1.";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/
?>