<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/IES.js"></script>
</head>

<body>
<div class="IES_bibi IES_lightbox">
	<h4>新增文獻</h4>
  <div class="edit_task_pop_board_write standard_height">
  <table class="form_table IES_table">
      <tr>
        <td>語言</td>
        <td>:</td>
        <td>
        <table class="innertable">
                    <col class="field_title" />
					<col class="field_title" />
                    	<tr><td><input type="radio" onClick="self.location='?bi_lang=ch'"  name="2" id='bi_ch'/><label for='bi_ch'>中文</label></td>
                        	<td><input type="radio" checked="checked"  name="2" id='bi_en'/><label for='bi_en'>English</label></td></tr>
                    </table>
        </td>
      </tr>
      
      <tr>
        <td>Type</td>
        <td>:</td>
        <td>
        <select>
        <optgroup label="Books, Reports">
        <option>One or More Author</option>
        <option>Published by the Corporate Author</option>
        <option>Anonymous Author</option>
        <option>Chapter in a Book</option>
        </optgroup>
        
        <optgroup label="Journals, Magazines, newspapers in print format">
        <option>One to Seven Authors</option>
        <option>Eight or More Authors (list 1st six and last authors)</option>
        </optgroup>
        
        <optgroup label="Online resources">
        <option>Online report with author (If there is no author or date, leave the fields blank)</option>
        </optgroup>
        </select>
        </td>
      </tr>
      
      <tr>
        <td>Author name</td>
        <td>:</td>
        <td>
        Surname <input type="text"/>&nbsp;&nbsp;Initial <input type="text"/><br />
        Surname <input type="text"/>&nbsp;&nbsp;Initial <input type="text"/>&nbsp;&nbsp;<a href="IES_student_task1_step4(bibi)add03M.html">+more</a></td>
      </tr>
      
      <tr>
        <td>Year of Publication</td>
        <td>:</td>
        <td><input type="text" /></td>
      </tr>
      
      <tr>
        <td>Title of Work</td>
        <td>:</td>
        <td><input type="text" class="input_med" /></td>
      </tr>
      
      <tr>
        <td>Location</td>
        <td>:</td>
        <td><input type="text" /></td>
      </tr>
      
      <tr>
        <td>Publisher</td>
        <td>:</td>
        <td><input type="text" class="input_med" /></td>
      </tr>
      
      <tr>
        <td>Tag</td>
        <td>:</td>
        <td><input type="text" class="input_med" /></td>
      </tr>
      
      <tr>
        <td>Content</td>
        <td>:</td>
        <td>Page no.: <input type="text" class="input_num" /><br />
        <textarea rows="4"></textarea>
        <br />
        <br /><a href="#">+more</a></td>
      </tr>

      <col class="field_title" />
      <col  class="field_c" />
    </table>
  </div>
    <div class="IES_lightbox_bottom">
      <input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" 儲存 " />
				<input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" 取消 " />
	</div>
</div>

</body>