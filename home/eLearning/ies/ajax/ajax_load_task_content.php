<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");



intranet_auth();
intranet_opendb();


$objIES = new libies();
//DEBUG_R(IsTeacherComment($task_id, $UserID));
//$TaskSnapshotAnswer = $objIES->getTaskSnapshotAnswer($task_id, $UserID);

?>

<!-- ############ task_ans_box start ##############  -->
<div class="IES_ans_box" style="display:block"> <!-- IES_ans_box, control show hide off ans box -->
  <div class="writing_area_min task_wrap">
    <?=$html_task_content?>
    <div style="width:100%">
      <?=$html_edit_button?>
    
      <div class="task_index_edit" style="display:none"> <!-- ###27-05-2010 -->
        <a href="javascript:doStep(<?=$step_id?>)" class="gotosteps"><span>以預設步驟幫助</span></a> <!-- ###27-05-2010 -->
        <div class="h-spacer">&nbsp;</div> <!-- ###27-05-2010 -->
        <a href="javascript:doEditTask(<?=$task_id?>)" class="instant_write" style="float:right"><span>即時修改</span></a> <!-- ###27-05-2010 -->
      </div> <!-- ### 27-05-2010 -->
    
    </div>
    <div class="clear"></div>
  </div>        
  <?//Check if the teacher comment exist or not!!!############?>
  <?if($objIES->IsTeacherComment($task_id, $UserID)== "TRUE"){?>
  <div class="teacher_comment"><a href="javascript:doGetComment(<?=$task_id?>)">老師回饋</a></div><!-- if have new comment, <a> add class "new"-->
 	<?}
 	else{?>
  	<div class="teacher_comment" style="display:none;"><a href="javascript:doGetComment(<?=$task_id?>)">老師回饋</a></div><!-- if have new comment, <a> add class "new"-->
 	<?} #########endIF############?>

  <div class="edit_date">呈交日期 : <?=$task_input_date?> | 最後修改日期 : <?=$task_modified_date?></div>
  <div class="clear"></div>
   <div style="display:none;" id="Comment_block_<?=$task_id?>" class="IES_commentbox"></div> 
</div> <!-- IES_ans_box, control show hide off ans box ### END ###-->
<?intranet_closedb();?>