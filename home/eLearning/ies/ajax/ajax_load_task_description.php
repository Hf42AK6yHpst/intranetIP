<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
$task_arr = $objIES->GET_TASK_BLOCK_INFO($task_id, $UserID);
echo libies_ui::GET_TASK_DESCRIPTION($task_arr, $close_btn);

intranet_closedb();
?>
