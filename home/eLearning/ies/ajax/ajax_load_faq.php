<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

	
intranet_auth();
intranet_opendb();

$ldb = new libdb();	

$sql = "SELECT QuestionID, Question, Answer FROM IES_FAQ WHERE RecordStatus=".$ies_cfg["faq_recordStatus_approved"]." AND (SchemeID=$scheme_id OR SchemeID=0) AND (Answer IS NOT NULL OR Answer!='') ORDER BY DateInput";
$result = $ldb->returnArray($sql,3);

for($i=0; $i<sizeof($result); $i++) {
	$faqContent .= "<li><a href='javascript:;' onClick=\"spanDisplay('spanQuestion{$result[$i][0]}','field{$result[$i][0]}')\">{$result[$i][1]}</a><div id='spanQuestion{$result[$i][0]}' style='display:none;'>{$result[$i][2]} <a href='javascript:;' onClick=\"spanDisplay('spanQuestion{$result[$i][0]}','field{$result[$i][0]}')\">(xxx)</a></div><input type='hidden' name='field{$result[$i][0]}' id='field{$result[$i][0]}' value='0'></li>";	
}
?>
<div class = "IES_faq">
  <div class="board_top"><div></div></div>
  <div class="board_left"><div class="board_right">
    <div class="board_content">
      <div class="inner">
        <a href="javascript:;" class="IES_tool_close" title="關閉<?=$scheme_id?>">&nbsp;</a>
        <h4><a href="javascript:;" class="faq_tab current"><span>常見問題</span></a><!--<a href="IES_student_task1_step4(FAQ_2).html" class="faq_tab"><span>我要發問</span></a>--></h4>
        <div class="Conntent_search faq_search">
          <input type="text" value="搜尋"/>
        </div>
        <br class="clear" />
        <ul class="IES_faq_list">
          <?=$faqContent?>
        </ul>
      </div> <!-- inner end -->
    </div> <!-- board_content end -->
  </div></div>
  <div class="board_bottom"><div></div></div>
</div>
<script language='javascript'>
function spanDisplay(spanName, field) {
	if(document.getElementById(field).value==0) {
		document.getElementById(spanName).style.display = 'block';	
		document.getElementById(field).value = 1;
	} else {
		document.getElementById(spanName).style.display = 'none';	
		document.getElementById(field).value = 0;
	}
}
</script>
<?
intranet_closedb();
?>
