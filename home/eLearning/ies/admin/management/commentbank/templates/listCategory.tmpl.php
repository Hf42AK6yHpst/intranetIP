<script language="JavaScript">
var editing = 0;
var org = '';
var org2 = '';
function AddCommentCategory(){
	if(!editing){
		$(".emptyRecord").remove();
		$('#add_row').fadeIn(0);
		$('.edit_dim').hide();
		$('.delete_dim').hide();
		editing = 1;
	}
}

function CancelAddCommentCategory(){
	if(editing){
		$('.edit_dim').show();
		$('.delete_dim').show();
		$('#add_row').fadeOut(0);
		$('#addcommentcategory').val('');
		editing = 0;
	}
}

function RemoveCommentCategory(id) {
	if(!editing){
		if(confirm('<?=$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete?>'))	{
			document.form1.action = "commentCategoryHandler.php?Action="+type+strtoaction;
			document.form1.submit();
		}
	}
}

function EditCommentCategory(id){
	if(!editing){
		org = $('#commentcategory_'+id).html();
		org2 = $('#commentcategoryen_'+id).html();		
		$('#commentcategory_'+id).html("<input id=\"editcommentcategory\" name=\"editcommentcategory[B5]\" value=\""+$('#h_commentcategory_'+id).val()+"\" />");
		$('#commentcategoryen_'+id).html("<input id=\"editcommentcategoryen\" name=\"editcommentcategory[EN]\" value=\""+$('#h_commentcategoryen_'+id).val()+"\" />");
		$('.edit_dim').hide();
		$('.delete_dim').hide();
		$('#button_add_'+id).show();
		$('#button_cancel_'+id).show();
		editing = 1;
	}
	

}

function CancelEdit(id){
	if(editing){
		$('#button_add_'+id).hide();
		$('#button_cancel_'+id).hide();
		$('.edit_dim').show();
		$('.delete_dim').show();
		$('#commentcategory_'+id).html(org);
		$('#commentcategoryen_'+id).html(org2);
		editing = 0;
	}
}

function SubmitForm(type, id){
	var strtoaction = "";
	if (id != undefined && id != "") {
		strtoaction = "&id="+id;
	}

	if(type=="remove" && !editing){
		if(confirm('<?=$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete?>\n*<?=$Lang['IES']['Warning']['DeleteCategory']?>'))	{
		} else {
			return false;
		}
	}
	document.form1.action = "commentCategoryHandler.php?Action="+type+strtoaction;
	document.form1.submit();
}



function CheckForm(type, id){
	if(type == "new"){
		if($('#addcommentcategory').val().length != 0 || $('#addcommentcategoryen').val().length != 0){
			SubmitForm(type);
		}
		else{
			alert ("<?=$Lang['IES']['CommentCategoryWarning']?>");
		}
	}
	else if(type == "edit"){
		if($('#editcommentcategory').val().length != 0 || $('#editcommentcategoryen').val().length != 0){
			SubmitForm(type, id);
		}
		else{
			alert ("<?=$Lang['IES']['CommentCategoryWarning']?>");
		}
	}
}

</script>
<form name="form1" method="POST" >
<?=$html_navigation?>
<table width="100%">
  <tr>
    <td align="right"><?=$html_message?></td>
  </tr>
</table>
<table width="100%">
  <tr>
    <td align="right"></td>
  </tr>
</table>

   <div class="content_top_tool">
   <div class="table_board">
   <table class="common_table_list">
	<thead>
	  <tr>
	  <th class="num_check">#</th>
	  <th width=35%><?=$Lang['IES']['CommentCategory']?>(<?=$Lang['IES']['Chinese']?>)</th>
	  <th width=35%><?=$Lang['IES']['CommentCategory']?>(<?=$Lang['IES']['English']?>)</th>
	  <th width=10%><?=$Lang['IES']['NumberOfCategories']?></th>
	  <th>&nbsp;</th>
	  </tr>
	</thead>
	<tbody>
	  <?=$html_category_list?>
	  <tr>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td><div class="table_row_tool row_content_tool"><a href="javascript:void(0);" onclick="AddCommentCategory()" class="add" title="<?=$Lang['IES']['AddCommentCategory']?>"></a></div></td>
	  </tr>	
	</tbody>
	</table>
   </div>
</div>
             
</form>