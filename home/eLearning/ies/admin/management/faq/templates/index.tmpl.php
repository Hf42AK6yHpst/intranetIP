<form name="form1" method="POST">
<?=$html_js_script?>
<table width="100%">
  <tr>
    <td width="60%"><?=$html_toolbar?></td>
    <td align="right"><?=$html_message?></td>
  </tr>
</table>


<table width="100%" cellpadding="2" cellspacing="0" border="0">
	<tr>
		<td><?=$html_filters?></td>
		<td class="content_top_tool" align="right">  
			<div class="Conntent_search">
    			<input name="searchText" type="text" value="<?=$searchText?>" />
  			</div>
		</td>
	</tr>
</table>
<div class="table_board">
  <table class="common_table_list">
    <thead>
      <tr>
        <th class="num_check">#</th>
        <!--<th><?=$i_ClassNumber?></th>-->
        <th><?=$Lang['IES']['CreatedBy'] ?></th>
        <th><?=$Lang['IES']['Scheme']?></th>
        <th><?=$Lang['IES']['QuestionDate']?></th>
        <th><?=$Lang['IES']['Question']?></th>
        <th><?=$Lang['IES']['Answer']?></th>
        <th><?=$Lang['IES']['AssignTo']?></th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <?=$html_scheme_list?>
    </tbody>
  </table>
  <br />
  <p class="spacer"></p>
</div>
</form>
