<?php
//modifying By : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();

$objIES = new libies();
$ldb = new libdb();

if(!$objIES->CHECK_ACCESS("IES-MGMT-FAQ-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "FAQ";



# Result message after operation
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

if(!isset($sch_id) && isset($s_id) && $s_id!="")
	$sch_id = $s_id;

# toolbar menu
$html_toolbar = $linterface->GET_LNK_NEW("javascript:goNew()",$button_new,"","","",0);


# Scheme Menu
$sql = "SELECT sch.SchemeID, sch.Title FROM IES_SCHEME sch LEFT OUTER JOIN IES_FAQ faq ON (faq.FromSchemeID=sch.SchemeID) WHERE faq.RecordStatus=".$ies_cfg["faq_recordStatus_approved"]." GROUP BY faq.FromSchemeID ORDER BY sch.Title";
$schemeData = $ldb->returnArray($sql,2);

$blank_selected = ($sch_id=="") ? " selected" : "";
$schemeMenu = "<SELECT NAME='sch_id' id='sch_id' onChange='formSubmit()'>";
$schemeMenu .= "<OPTION VALUE='' $blank_selected>".$Lang['IES']['AllScheme']."</OPTION>";

//debug_pr($schemeMenu);
for($i=0; $i<sizeof($schemeData); $i++) {
	$optionSelected = ($sch_id==$schemeData[$i][0]) ? " selected" : "";
	$schemeMenu .= "<OPTION value='".$schemeData[$i][0]."' $optionSelected>".$schemeData[$i][1]."</OPTION>";
}
$schemeMenu .= "</SELECT>";

/*
# User Type Menu
$questionTypeMenu = "<SELECT name='questionType' id='questionType' onChange='formSubmit()'>";
$questionTypeMenu .= "<OPTION value=''".(($questionType=='')?" selected":"").">".$Lang['IES']['AllQuestionType']."</option>";
$questionTypeMenu .= "<OPTION value='".$ies_cfg["faq_userType_student"]."'".(($questionType==$ies_cfg["faq_userType_student"])?" selected":"").">".$Lang['IES']['AskedByStudent']."</option>";
$questionTypeMenu .= "<OPTION value='".$ies_cfg["faq_userType_teacher"]."'".(($questionType==$ies_cfg["faq_userType_teacher"])?" selected":"").">".$Lang['IES']['AskedByTeacher']."</option>";
$questionTypeMenu .= "</SELECT>";

if(!isset($recordstatus)) $recordstatus=$ies_cfg["faq_not_yet_replied"];
# Reply status
$recordStatusMenu = "<SELECT name='recordstatus' id='recordstatus' onChange='formSubmit()'>";
$recordStatusMenu .= "<OPTION value=''".(($recordstatus=="")?" selected":"").">".$Lang['SysMgr']['SchoolNews']['AllStatus']."</OPTION>";
$recordStatusMenu .= "<OPTION value='".$ies_cfg["faq_not_yet_replied"]."'".(($recordstatus==$ies_cfg["faq_not_yet_replied"])?" selected":"").">".$Lang['IES']['NotYetReplied']."</OPTION>";
$recordStatusMenu .= "<OPTION value='".$ies_cfg["faq_replied"]."'".(($recordstatus==$ies_cfg["faq_replied"])?" selected":"").">".$Lang['IES']['Replied']."</OPTION>";
$recordStatusMenu .= "</SELECT>";
*/
$html_filters .= $schemeMenu."&nbsp;".$questionTypeMenu."&nbsp;".$recordStatusMenu;

# Temporary query retrieving scheme here
if($sch_id!="")	
	$conds .= " AND faq.FromSchemeID=$sch_id";

if($questionType!="")
	$conds .= " AND faq.UserType=$questionType";
	
/*	
if($recordstatus==$ies_cfg["faq_not_yet_replied"])
	$conds .= " AND (faq.Answer IS NULL OR faq.Answer='')";
else if($recordstatus==$ies_cfg["faq_replied"])
	$conds .= " AND (faq.Answer IS NOT NULL && faq.Answer!='')";
*/	
if($searchText!="") {
	$searchText = intranet_htmlspecialchars(stripslashes(stripslashes($searchText)));
	$conds .= " AND (sch.Title LIKE '%$searchText%' OR faq.Question LIKE '%$searchText%' OR Answer LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%')";
}

$name_field = getNameFieldByLang("USR.");
$sql = "SELECT 
			IFNULL(CONCAT(USR.ClassName,'-',USR.ClassNumber),'---') as ClassNameNum, 
			$name_field as name, 
			IF(faq.FromSchemeID='0','---',sch.Title) as Title,
			LEFT(faq.DateInput,10),
			CONCAT('<a href=javascript:goEdit(',faq.QuestionID,')>',faq.Question,'</a>'),
			IF(faq.Answer IS NULL, '".$Lang['IES']['NotYetReplied']."','".$Lang['IES']['Replied']."'),
			faq.AssignToStudent,
			CONCAT('<div class=table_row_tool><a href=javascript:removeQ(',faq.QuestionID,') class=delete_dim title=Delete></a></div>') as del,
			faq.FromSchemeID,
			faq.QuestionID,
			faq.Answer,
			faq.AssignType
		FROM
			IES_FAQ faq LEFT OUTER JOIN
			IES_SCHEME sch ON (sch.SchemeID=faq.FromSchemeID) LEFT OUTER JOIN
			INTRANET_USER USR ON (USR.UserID=faq.AskedBy) 
		WHERE
			faq.RecordStatus=".$ies_cfg["faq_recordStatus_approved"]." 
			$conds
		ORDER BY faq.DateInput DESC
		";

$result = $ldb->returnArray($sql);

for($i=0; $i<count($result); $i++)
{
	if($result[$i][2]=="")
		$result[$i][2] = "---";
	if($result[$i][10]=="") {													# not yet answered
		$assignTo = "---";
	} else if($result[$i][11]==$ies_cfg["faq_assignType_to_all"]) {				# to all scheme
		$assignTo = $Lang['IES']['AssignToAllScheme'];
	} else {
		if($result[$i][11]==$ies_cfg["faq_assignType_to_student"]) {												# to student
			$assignTo = $Lang['IES']['AssignToParticularStudent'];	
		} else {
			$sql = "SELECT COUNT(*) FROM IES_FAQ_RELATIONSHIP WHERE QuestionID=".$result[$i][9];
			$temp = $ldb->returnVector($sql);
			$assignTo = $Lang['IES']['AssignTo']." ".$temp[0]." ".$Lang['IES']['Schemes'];
		}
	}
	
	
    $html_scheme_list .= "<tr>";
    $html_scheme_list .= "<td valign='top'>".($i+1)."</td>";
    //$html_scheme_list .= "<td valign='top'>".$result[$i][0]."</td>";
    $html_scheme_list .= "<td valign='top'>".$result[$i][1]."</td>";
    $html_scheme_list .= "<td valign='top'>".$result[$i][2]."</td>";
    $html_scheme_list .= "<td valign='top'>".$result[$i][3]."</td>";
    $html_scheme_list .= "<td valign='top'>".$result[$i][4]."</td>";
    $html_scheme_list .= "<td valign='top'>".$result[$i][5]."</td>";
    $html_scheme_list .= "<td valign='top'>".$assignTo."</td>";
    $html_scheme_list .= "<td valign='top'>".$result[$i][7]."</td>";
    $html_scheme_list .= "</tr>";
}
if(sizeof($result)==0) {
	$html_scheme_list = "<tr><td colspan='8' height='40' align='center' valign='middle'>".$no_record_msg."</td></tr>";
}

$html_scheme_list .= "<input type='hidden' name='id' id='id' value=''>";
$html_scheme_list .= "<input type='hidden' name='s_id' id='s_id' value='{$sch_id}'>";


$html_js_script .= "
<script language='javascript'>
function removeQ(id) {
	if(confirm('$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete'))	{
		document.getElementById('id').value = id;
		document.form1.action = 'remove.php';
		document.form1.submit();
	}
}

function goEdit(id) {
	document.getElementById('id').value = id;
	document.form1.action = 'edit.php';
	document.form1.submit();
}

function goNew() {
	document.form1.action = 'new.php';
	document.form1.submit();
}

function formSubmit() {
	document.form1.submit();	
}
</script>
";

### Title ###
$title = $Lang['IES']['FAQ'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
