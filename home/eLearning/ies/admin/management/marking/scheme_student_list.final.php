<?php
/* Bookmark1: Temporary Hide Scheme Mark Column */
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
// Get array of stages
$stage_arr = $objIES->GET_STAGE_ARR($SchemeID);
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
  $t_stage_id = $stage_arr[$i]["StageID"];
  $t_stage_title = $stage_arr[$i]["Title"];

  $create_field_sql .= "Stage{$t_stage_id}_Status text, ";
  $create_field_sql .= "Stage{$t_stage_id}_Mark varchar(16), ";
  
  $insert_temptable_sql = "INSERT INTO tempStageHandinStatus (StudentID, Stage{$t_stage_id}_Status, Stage{$t_stage_id}_Mark) ";
  $insert_temptable_sql .= "SELECT UserID, ";
  
  


  
  $insert_temptable_sql .= "CASE ";
  //$insert_temptable_sql .= "WHEN Approval=".STATUS_APPROVED." THEN CONCAT('commented{$ies_cfg["handin_status_identifier"]}<a href=\"scheme_stage_final.php?SchemeID={$SchemeID}&StageID={$t_stage_id}#uid', UserID, '\" class=\"view_works\">&nbsp;</a>') ";
  $insert_temptable_sql .= "WHEN BatchHandinStatus=".STATUS_SUBMITTING." THEN CONCAT('justsave{$ies_cfg["handin_status_identifier"]}<a href=\"scheme_stage_final.php?SchemeID={$SchemeID}&StageID={$t_stage_id}#uid', UserID, '\" class=\"view_works\">&nbsp;</a>') ";
  $insert_temptable_sql .= "WHEN BatchHandinStatus=".STATUS_SUBMITTED." THEN CONCAT('submited{$ies_cfg["handin_status_identifier"]}<a href=\"scheme_stage_final.php?SchemeID={$SchemeID}&StageID={$t_stage_id}#uid', UserID, '\" class=\"new_submit\">&nbsp;</a>') ";
  $insert_temptable_sql .= "WHEN BatchHandinStatus=".STATUS_TEACHER_READ." THEN CONCAT('submited{$ies_cfg["handin_status_identifier"]}<a href=\"scheme_stage_final.php?SchemeID={$SchemeID}&StageID={$t_stage_id}#uid', UserID, '\" class=\"view_works\">&nbsp;</a>') ";
  $insert_temptable_sql .= "WHEN BatchHandinStatus=".STATUS_REDO." THEN CONCAT('needredo{$ies_cfg["handin_status_identifier"]}<a href=\"scheme_stage_final.php?SchemeID={$SchemeID}&StageID={$t_stage_id}#uid', UserID, '\" class=\"view_works\">&nbsp;</a>') ";
  $insert_temptable_sql .= "END, ";
  //$insert_temptable_sql .= "CONCAT('{$ies_cfg["handin_status_identifier"]}', IF(Approval=".STATUS_APPROVED.", ".STATUS_APPROVED.", BatchHandinStatus)), ";
  
  
  
  
  $insert_temptable_sql .= "IF(Score IS NULL, '--', Score) FROM {$intranet_db}.IES_STAGE_STUDENT WHERE StageID = {$t_stage_id} ";
  $insert_temptable_sql .= "ON DUPLICATE KEY UPDATE Stage{$t_stage_id}_Status = VALUES(Stage{$t_stage_id}_Status), Stage{$t_stage_id}_Mark = VALUES(Stage{$t_stage_id}_Mark)";
  $insert_temptable_sql_arr[] = $insert_temptable_sql;
  $select_field_sql .= "IF(temp_shs.Stage{$t_stage_id}_Status IS NULL, '{$ies_cfg["handin_status_identifier"]}0', temp_shs.Stage{$t_stage_id}_Status), ";
  $select_field_sql .= "IF(temp_shs.Stage{$t_stage_id}_Mark IS NULL, '--', temp_shs.Stage{$t_stage_id}_Mark), ";
  
  $total_mark_field_sql .= "";
  
  $html_table_column .= "<th class=\"paper th_task\" title=\"{$t_stage_title}\"><a href=\"scheme_stage_final.php?SchemeID={$SchemeID}&StageID={$t_stage_id}\" class=\"paper\">".$Lang['IES']['Stage'].($i+1)."</a></th>";
  $html_table_column .= "<th class=\"IES_stage_mark\">{$Lang['IES']['Mark']}</th>";
}
$stage_col_count = count($stage_arr) * 2;

$sql = "CREATE TEMPORARY TABLE tempStageHandinStatus( ";
$sql .= "StudentID int(8), ";
$sql .= $create_field_sql;
$sql .= "PRIMARY KEY (StudentID))";
$ldb->db_db_query($sql);

for($i=0, $i_max=count($insert_temptable_sql_arr); $i<$i_max; $i++)
{
  $sql = $insert_temptable_sql_arr[$i];
  $ldb->db_db_query($sql);
}

# Main query
if ($order=="") $order=1;
if ($field=="") $field=1;
$LibTable = new libies_dbtable($field, $order, $pageNo);
$sql =  "
          SELECT
            CONCAT(yc.ClassTitleEN, ' - ', ycu.ClassNumber),
            /* CONCAT('<a href=\"scheme_student_final.php?SchemeID={$SchemeID}&StudentID=', iu.UserID, '\">', ".getNameFieldByLang2("iu.").", '</a>') AS DisplayName, */
            ".getNameFieldByLang2("iu.")." AS DisplayName,
						".substr($select_field_sql, 0, -2)."/*Bookmark1[Start] ,
						IFNULL(schemestudent.Score, '--') Bookmark1[End]*/
          FROM
            {$intranet_db}.IES_SCHEME_STUDENT AS schemestudent
          INNER JOIN {$intranet_db}.INTRANET_USER AS iu
            ON schemestudent.UserID = iu.UserID
          INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
            ON iu.UserID = ycu.UserID
          INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
            ON ycu.YearClassID = yc.YearClassID
          LEFT JOIN tempStageHandinStatus AS temp_shs
            ON temp_shs.StudentID = iu.UserID
          WHERE
            yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." AND
            schemestudent.SchemeID = {$SchemeID}
            $cond
        ";

$sql =  "
          SELECT
            CONCAT(yc.ClassTitleEN, ' - ', ycu.ClassNumber),
            /* CONCAT('<a href=\"scheme_student_final.php?SchemeID={$SchemeID}&StudentID=', iu.UserID, '\">', ".getNameFieldByLang2("iu.").", '</a>') AS DisplayName, */
            concat('<span style=\"float:left\">',".getNameFieldByLang2("iu.").",'</span> <a href=\"#TB_inline?height=500&width=750&inlineId=FakeLayer\" onclick=\"Show_ipFileList(',iu.UserID,'); return false;\" class=\"thickbox setting_row\" title =\"".$Lang['IES']['ViewStudentFile']."\"><span style=\"float:right\"><img src =\"".$image_path."/".$LAYOUT_SKIN."/eOffice/icon_resultview.gif\" border =\"0\" ></span></a>') AS DisplayName,
						".substr($select_field_sql, 0, -2)." ,
						IFNULL(schemestudent.Score, '--') 
          FROM
            {$intranet_db}.IES_SCHEME_STUDENT AS schemestudent
          INNER JOIN {$intranet_db}.INTRANET_USER AS iu
            ON schemestudent.UserID = iu.UserID
          INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
            ON iu.UserID = ycu.UserID
          INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
            ON ycu.YearClassID = yc.YearClassID
          LEFT JOIN tempStageHandinStatus AS temp_shs
            ON temp_shs.StudentID = iu.UserID
          WHERE
            yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." AND
            schemestudent.SchemeID = {$SchemeID}
            $cond
        ";
// TABLE INFO
$LibTable->field_array = array("DisplayName", "CONCAT(iu.ClassName, LPAD(iu.ClassNumber, 2 ,'0'))");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = 9999;
$LibTable->no_col = 4 + $stage_col_count;
$LibTable->no_col = $LibTable->no_col;		//Bookmark1
$LibTable->table_tag = "<table class=\"common_table_list stage1\">";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list = "<thead>";
$LibTable->column_list .= "<tr>";
$LibTable->column_list .= "<th class=\"num_check sub_row_top\">#</th>";
$LibTable->column_list .= "<th class=\"sub_row_top\">{$i_UserClassNumber}</th>";
$LibTable->column_list .= "<th class=\"sub_row_top\">{$i_general_name}</th>";
$LibTable->column_list .= "<th class=\"sub_row_top\" colspan=\"{$stage_col_count}\" >{$Lang['IES']['Stage']}</th>";
$LibTable->column_list .= "<th class=\"sub_row_top\">{$Lang['IES']['TotalMark']}</th>";
$LibTable->column_list .= "</tr>";
$LibTable->column_list .= "<tr>";
$LibTable->column_list .= "<th colspan=\"3\" >&nbsp;</th>";
$LibTable->column_list .= $html_table_column;
$LibTable->column_list .= "<th><input type=\"button\" id=\"calSchemeMark\" value=\"{$Lang['IES']['CalSchemeMark']}\" class=\"formsmallbutton\" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" /></th>";
$LibTable->column_list .= "</tr>";
$LibTable->column_list .= "</thead>";

$html_legend = <<<HTML
  <img src="/images/2009a/ies/white_dot.gif" /> : {$Lang['IES']['AnswerNotSumbitted']}&nbsp;&nbsp;&nbsp;
  <img src="/images/2009a/ies/yellow_dot.gif" /> : {$Lang['IES']['HandInCommentedMarked']}&nbsp;&nbsp; 
  <img src="/images/2009a/ies/red_dot.gif" /> : {$Lang['IES']['RedoSent']}&nbsp;&nbsp;&nbsp; 
  <!--<img src="/images/2009a/ies/green_dot.gif" /> : {$Lang['IES']['Approved']}&nbsp;&nbsp;&nbsp;--> 
  <img src="/images/2009a/ies/alert_new2.gif" /> : {$Lang['IES']['HandInIn']}
HTML;

$html_export_score_lnk = "<a href=\"scheme_score_export.php?schemeID={$SchemeID}\" class=\"export\">{$Lang['Btn']['Export']}</a>";

?>
