<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();
$_snapshotID;
$_taskID;
$_studentID;

if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
$li->Start_Trans();

foreach($mark AS $key => $markArr)
{
  $old_num = $key;	
  list($_snapshotID, $_taskID, $_studentID) = explode("_", base64_decode($key));
  
  if(empty($_snapshotID))
  {
    // snapshot current answer
    $sql = "INSERT INTO {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT ";
    $sql .= "(TaskID, UserID, Answer, QuestionType, AnswerTime, DateInput, InputBy) ";
    $sql .= "SELECT TaskID, UserID, Answer, QuestionType, DateModified, NOW(), {$UserID} ";
    $sql .= "FROM IES_TASK_HANDIN WHERE TaskID = {$_taskID} AND UserID = {$_studentID}";
    $res[] = $li->db_db_query($sql);
    
    $_snapshotID = $li->db_insert_id();
  }
  
  foreach($markArr AS $markCriteria => $score)
  {
    $sql = "INSERT INTO {$intranet_db}.IES_MARKING ";
    $sql .= "(AssignmentID, AssignmentType, MarkCriteria, MarkerID, Score, DateInput, InputBy, ModifyBy) VALUES ";
    $sql .= "({$_snapshotID}, {$AssignmentType}, {$markCriteria}, {$UserID}, {$score}, NOW(), {$UserID}, {$UserID}) ";
    $sql .= "ON DUPLICATE KEY UPDATE Score = VALUES(Score), MarkerID = VALUES(MarkerID), ModifyBy = VALUES(ModifyBy)";
    $res[] = $li->db_db_query($sql);
    
    if($markCriteria == MARKING_CRITERIA_OVERALL)
    {
      $sql = "INSERT INTO {$intranet_db}.IES_TASK_STUDENT ";
      $sql .= "(TaskID, UserID, Score, DateInput, InputBy, ModifyBy) VALUES ";
      $sql .= "({$_taskID}, {$_studentID}, {$score}, NOW(), {$UserID}, {$UserID}) ";
      $sql .= "ON DUPLICATE KEY UPDATE Score = VALUES(Score), ModifyBy = VALUES(ModifyBy)";
      $res[] = $li->db_db_query($sql);
    }
  } 
}

$final_res = (count($res) == count(array_filter($res)));
if($final_res)
{
  $li->Commit_Trans();
  $answerEncodeNo = base64_encode($_snapshotID."_".$_taskID."_".$_studentID);
  $lastMarkerArr = libies_static::getLastMarker($_snapshotID, $ies_cfg["DB_IES_MARKING_AssignmentType"]["task"]);
  $markerName = $lastMarkerArr["MarkerName"];
  $lastModifiedDate = $lastMarkerArr["LastModified"];
  $html_latest_modified = empty($lastMarkerArr) ? "" : "({$lastModifiedDate} {$markerName})";
  	

  $XML = $objIES->generateXML(
			array(
				array("date", $html_latest_modified), 
				array("new_code", $answerEncodeNo),
				array("old_code", $old_num)
			), TRUE, TRUE
		);

  header("Content-Type:   text/xml");
  echo $XML;	
}
else
{
  $li->RollBack_Trans();
  //echo "Fail to mark";
}

intranet_closedb();

?>