<?php
// modifying By connie

/**********************************************************************
* Checked by Eric (20100619)
* - matching template: templates/scheme_student_list.tmpl.php
* - supplementary script: scheme_student_list.final.php, scheme_student_list.stage.php 
* - comment added
* - reorder lines of codes to match display order 
**********************************************************************/ 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_dbtable.php");

// Temporary use
include_once("common.php");

intranet_auth();
intranet_opendb();

$objIES = new libies();
$ldb = new libdb();

$SchemeID = $_GET['SchemeID'];


if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

// Check if user has access right
$schemeDenied = isSchemeDenied($SchemeID, $UserID);
if($schemeDenied)
{
	echo "Invalide access , Empty SCHEME ID<br/>";
  exit();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "StudentProgress";

// Get the scheme detail
$schemeDetails = $objIES->GET_SCHEME_DETAIL($SchemeID);
$html_schemeTitle = $schemeDetails["Title"];

// Pull-down for scheme switching
$scheme_arr = $objIES->GET_SCHEME_ARR();
$schemeid_arry = "var sch_arr=new Array();";
$html_scheme_option = "<span class=\"table_filter\">";
$html_scheme_option .= "<select name=\"SchemeID\" class=\"formtextbox\">";
$is_selected = "selected=\"selected\"";
for($i=0, $i_max=count($scheme_arr); $i<$i_max; $i++)
{
  $t_scheme_id = $scheme_arr[$i]["SchemeID"];
  $t_scheme_title = $scheme_arr[$i]["Title"];

	$html_scheme_option .= ($t_scheme_id == $SchemeID) ? "<option value=\"{$t_scheme_id}\" {$is_selected}>" : "<option value=\"{$t_scheme_id}\">";
	$html_scheme_option .= $t_scheme_title;
	$html_scheme_option .= "</option>";
}
$html_scheme_option .= "</select>";
$html_scheme_option .= "&nbsp;[&nbsp;<a href = \"javascript:newWindow('/home/eLearning/ies/coursework/index.php?scheme_id=".$SchemeID."',95)\">".$button_preview."</a>&nbsp;]";

// Get TeachingTeacher and AssessTeacher
$html_TeachingTeacher = "";
$html_AssessTeacher = "";
$result_r = $objIES->get_teacher($SchemeID);
foreach($result_r as $value){
	if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
		$html_TeachingTeacher .= "{$value['TeacherName']} <br \>";
	else if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"])
		$html_AssessTeacher .= "{$value['TeacherName']} <br \>";	
}
if(empty($html_TeachingTeacher)){ 
	$html_TeachingTeacher = "--";
}
if(empty($html_AssessTeacher)){ 
	$html_AssessTeacher = "--";
}

// Get array of stages
$stage_arr = $objIES->GET_STAGE_ARR($SchemeID);

// Redirect to designated stage if stage ID is not set
// empty Stage ID = final
if(!isset($StageID))
{
  $StageID = $stage_arr[0]["StageID"];

  header("Location: scheme_student_list.php?SchemeID={$SchemeID}&StageID={$StageID}");
  DIE();
}

// Create tab navigation by array of stages
$tab_arr = array();
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
  $t_stage_id = $stage_arr[$i]["StageID"];
  $t_stage_title = $stage_arr[$i]["Title"];

  $tab_arr["tab{$t_stage_id}"] = array("scheme_student_list.php?SchemeID={$SchemeID}&StageID={$t_stage_id}", $t_stage_title);
}
$tab_arr["tab"] = array("scheme_student_list.php?SchemeID={$SchemeID}&StageID=", $Lang['IES']['StageSubmitStatus']);
$html_tab_nav = libies_ui::GET_TAB_MENU($tab_arr,"tab{$StageID}");

// Include different pages to use different query and display for stage and final submission
if(empty($StageID))
{
  // Final submission
  include_once("scheme_student_list.final.php");
}
else
{
  // Stage submission
  include_once("scheme_student_list.stage.php");
}
$pageSizeChangeEnabled = false;

$html_student_list = $LibTable->displayIES_StudentList();
//$html_student_list .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
//$html_student_list .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td></tr>" : "";
//$html_student_list .= "</table>";

### Title ###
$title = $ies_lang['StudentProgress'];

$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

// navigation
$nav_arr[] = array($ies_lang['StudentProgress'], "./");
$nav_arr[] = array($html_schemeTitle, "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);

$linterface->LAYOUT_START();
include_once("templates/scheme_student_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>