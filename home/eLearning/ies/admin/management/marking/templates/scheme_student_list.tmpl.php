<?php //editing by : connie?>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/2009a/js/script.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/ui.core.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/ui.draggable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/ui.droppable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/ui.selectable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.jeditable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script>
				

<SCRIPT LANGUAGE="javascript">

$(document).ready(function(){

  // Bind action to scheme selection
  $("select[name=SchemeID]").change(function(){
    var scheme_id = $(this).val();
    // jump to final submission if $StageID is empty
    var stage_id_str = "<?= (empty($StageID) ? "&StageID=" : "") ?>";
    
    window.location = "scheme_student_list.php?SchemeID="+scheme_id+stage_id_str;
  });
  
  // Calculate scheme mark
  $("#calSchemeMark").click(function(){
		$.ajax({
	    url:      "../../../ajax/ajax_cal_scheme_mark.php",
	    type:     "POST",
	    data: 	  "SchemeID=<?=$SchemeID?>",
	    error:    function(xhr, ajaxOptions, thrownError){
	                alert(xhr.responseText);
	              },
	    success:  function(){
									window.location.reload();
	              }
	  });
	});
	

});

function Show_ipFileList(studentID){
	$.post(
		"/ipfiles/viewStudentFile.php", 
		{ 
			studentID: studentID,
			moduleCode:"ies"
//			ole_title: ole_title,
//			PATH_WRT_ROOT: "../../../../"
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);		
		}
	);	
}
</script >

<!-- navigation start -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<br style="clear:both" />

<table class="form_table" style="width:100%">
  <col class="field_title" />
  <col  class="field_c" />
  <tr>
    <td><?=$Lang['IES']['Scheme']?></td>
    <td>:</td>
    <td >
    <?=$html_scheme_option?>
      
    </td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['TeachingTeacher']?></td>
    <td>:</td>
    <td><?=$html_TeachingTeacher?></td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['AssessTeacher']?></td>
    <td>:</td>
    <td><?=$html_AssessTeacher?></td>
  </tr>
</table>
<br style="clear:both" />
<!--a href = "testRubric.php" target="_blank">Rubric</a-->
<?=$html_tab_nav?>
                       
<div class="table_board">
  <!--<div class="common_table_tool">
  <a href="#" class="marks_release">{{分數發布}}</a>
  
  </div>-->
  <div class="content_top_tool">
    <div class="Conntent_tool">
		<?=$html_export_score_lnk?>
    <!--<a href="#" class="print"> Print</a>		-->				</div>
    
    <?=$html_student_list?>

    <br />
    <span class="symbol_remark">
      <?=$Lang['IES']['Remark']?>&nbsp;&nbsp;&nbsp;
      <?=$html_legend?>
      <p class="spacer"></p>
    </span>
  </div>
</div>
