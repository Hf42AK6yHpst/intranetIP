<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();

if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
$li->Start_Trans();

foreach($_POST AS $input_name => $input_val){
  $batchID = base64_decode($input_name);
  
  $sql = "INSERT INTO {$intranet_db}.IES_STAGE_HANDIN_COMMENT ";
  $sql .= "(BatchID, Comment, InputBy) VALUES ";
  $sql .= "({$batchID}, '".mysql_real_escape_string(stripslashes($input_val))."', {$UserID})";
  $res = $li->db_db_query($sql);

  if($res)
  {
    $li->Commit_Trans();
    $objIES->updateStudentStageBatchHandinStatus($StageID, $StudentID, STATUS_TEACHER_READ, STATUS_SUBMITTED);
    $commentArr = libies_static::getBatchCommentArr($batchID);
    for($j=0, $j_max=count($commentArr); $j<$j_max; $j++)
    {
      $t_comment = nl2br($commentArr[$j]["Comment"]);
      $t_inputdate = $commentArr[$j]["DateInput"];
      $t_teacher = $commentArr[$j]["CommentTeacher"];
  
      $commentStr .= "<div style=\"float:left; width:85%;\">".$t_comment."</div>";
      $commentStr .= "<div class=\"table_row_tool\" style=\"float:right\"><a href=\"javascript:;\" onclick=\"DeleteBatchComment('".$commentArr[$j]["BatchCommentID"]."', $batchID, this)\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete2']}\"></a></div>";
	  	$commentStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";
     // $commentStr .= "<input type=\"button\" value=\"{$Lang['IES']['Delete2']}\" onmouseout=\"this.className='formsmallbutton'\" onmouseover=\"this.className='formsmallbuttonon'\" class=\"formsmallbutton\" onclick=\"DeleteComment('".$commentArr[$j]["BatchCommentID"]."', $batchID, this)\" style=\"float: right;\">";
      $commentStr .= "<div class=\"edit_top\"></div>";
    }
    echo $commentStr;
  }
  else
  {
    $li->RollBack_Trans();
    
  }
}

?>