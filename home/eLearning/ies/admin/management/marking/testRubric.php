<?
$PATH_WRT_ROOT = "../../../../../../";
//include_once($PATH_WRT_ROOT."includes/global.php");
//$BLOCK_LIB_LOADING = true;
$eclass40_filepath = "/home/web/eclass40/eclass40/";
$cfg_eclass_lib_path = $eclass40_filepath."/src/includes/php";
$eclass_http_root = "/eclass40";
$domain = "http://192.168.0.146:31001";
include_once($eclass40_filepath.'/system/settings/global.php');
include_once($cfg_eclass_lib_path.'/lib-assessment.php');
include_once($cfg_eclass_lib_path.'/lib-groups.php');
include_once($cfg_eclass_lib_path.'/lib-quiz.php');
//auth("AT", $fs->HelperSet['Rubrics']);
$course_id = 62;
$ck_course_id = 62;
$std_rubric_set_id =2;


$cfg["path_images"] = $domain.$eclass_http_root."/images";
$cfg["path_images_root"] = $eclass_http_full_root."/images";
$cfg["path_css"] = $domain.$eclass_http_root."/css";
$cfg["path_js"] = $domain.$eclass_http_root."/js";


opendb();
################################################################################


## Library
$lass = new assessment();


## Get Data
$std_rubric_set_id = (isset($std_rubric_set_id) && $std_rubric_set_id != '') ? $std_rubric_set_id : '';

## Initialization
$stdRubricArr = array();
$stdRubricDetailArr = array();
$stdRubricSetArr = array();
$set_title   = ($set_title=='')?'':$set_title;
$description = ($description=='')?'':$description;

$cur_tabs    = 4;
$cur_caption = $Lang['Rubric']['Rubrics'];
$contentTpl 	 = 'resource/a.tpl';
//$resourceContent = 'resource/rubrics/std_rubric_set_new.tpl';


## Preparation
# Check whether the passing standdard rubric set exists or not
$is_edit = 0;

# default settype
$set_type = ($set_type=='')?$cfg['rubric_set_type_level']:$set_type;
if($std_rubric_set_id != ''){
	# Get Standard Rubric Set Info
	$stdRubricSetArr = $lass->Get_Standard_Rubric_Set($std_rubric_set_id);
//debug_r($stdRubricSetArr);
	if(count($stdRubricSetArr) > 0){
		$set_title   = trim($stdRubricSetArr[0]['set_title']);
		$description = trim($stdRubricSetArr[0]['description']);
		$set_type    = trim($stdRubricSetArr[0]['set_type']);
		# Get Standard Rubric Basic Info
		$stdRubricArr = $lass->Get_Standard_Rubric($std_rubric_set_id);
//debug_r($stdRubricArr);
		if(count($stdRubricArr) > 0){
			for($i=0 ; $i<count($stdRubricArr) ; $i++){
				$std_rubric_id = $stdRubricArr[$i]['std_rubric_id'];
				
				# Get Standard Rubric Detail Info
				$stdRubricDetailArr[$std_rubric_id] = $lass->Get_Standard_Rubric_Detail($std_rubric_id);
//debug_r($stdRubricDetailArr[$std_rubric_id]);
			}
		}
		$is_edit = 1;
	}
	$set_type_hidden = '<input type="hidden" name="set_type" value="'.$set_type.'"/>';
}

if($set_type==$cfg['rubric_set_type_series'])
{
	 
	for($a=0;$a<sizeof($stdRubricArr);$a++)
	{
		$std_rubric_group_id = $stdRubricArr[$a]['std_rubric_group_id'];
		$tmpRubricArr[$std_rubric_group_id][] = $stdRubricArr[$a]; 
	}
	unset($stdRubricArr);
	$stdRubricArr = $tmpRubricArr;
	
	$score_box_count = array();
	foreach($stdRubricDetailArr as $std_rubric_id=>$detail_value)
	{
		for($a=0;$a<sizeof($detail_value);$a++)
		{
			$std_rubric_group_id 	= $detail_value[$a]['std_rubric_group_id'];
			$std_rubric_score 	 	= $detail_value[$a]['score'];
			$std_rubric_desc		= $detail_value[$a]['description'];
			$tmpRubricDetailArr[$std_rubric_group_id][] = $detail_value[$a];
			$std_rubric_desc_and_score[$std_rubric_group_id][$std_rubric_score] = $std_rubric_desc;
			if(trim($std_rubric_desc!=''))
			{
				 $score_box_count[$std_rubric_group_id]++;
			} 
		} 
	}
	unset($stdRubricDetailArr);
	$stdRubricDetailArr = $tmpRubricDetailArr;

}


# to handle series table ini function in assessment_setting.js
$criteria_table_ct = sizeof($stdRubricArr)-1;
switch($set_type)
{
	case $cfg['rubric_set_type_level']:
	case "":
		$resourceContent = 'resource/rubrics/std_rubric_set_new.tpl';
		$add_row_type_js   = 'add_new_std_rubric_level';
		$add_detail_row_js = 'add_new_std_rubric_detail_level';
	break;
	case $cfg['rubric_set_type_range']:

		$resourceContent = 'resource/rubrics/std_rubric_set_new_range.tpl';
		$add_row_type_js   = 'add_new_std_rubric_range';
		$add_detail_row_js = 'add_new_std_rubric_detail_range';
	break;
	case $cfg['rubric_set_type_series']:
		$resourceContent = 'resource/rubrics/std_rubric_set_new_series.tpl';
		$add_row_type_js   = 'add_new_std_rubric_series';
		$add_detail_row_js = 'add_new_std_rubric_detail_series';
	break;
	default:
		$resourceContent = 'resource/rubrics/std_rubric_set_new.tpl';
		$add_row_type_js   = 'add_new_std_rubric_level;';
		$add_detail_row_js = 'add_new_std_rubric_detail_level';
	break;
}
//$resourceContent = 'resource/rubrics/std_rubric_set_new.tpl';
$settype_rb = ($std_rubric_set_id)?$lass->get_rubric_settype_name($set_type):$lass->getSetTypeRadioBtn($set_type);

## Smarty Retreive Data
$objSmarty->assign(array(
	'cur_caption' 		 => $cur_caption, 
	'cur_tabs'	  		 => $cur_tabs, 
	'set_title'			 => $set_title, 
	'description'		 => $description,
	'std_rubric_set_id'  => $std_rubric_set_id, 
	'stdRubricArr' 		 => $stdRubricArr, 
	'stdRubricDetailArr' => $stdRubricDetailArr,
	'std_rubric_desc_and_score'  => $std_rubric_desc_and_score,
	
	'item_box_html'	 =>	$item_box_html, 
	'is_edit'			 => $is_edit,
	'settype_rb'		 => $settype_rb,
	'add_row_type_js'	 =>	$add_row_type_js,
	'add_detail_row_js'	 => $add_detail_row_js,
	'set_type'			 => $set_type,
	'set_type_hidden'	 => $set_type_hidden,
	'score_box_count'	 =>$score_box_count
));

## Assign main variables 
$objSmarty->assign(array(	
	'LANG'				=> $lang,
	'cfg'				=> $cfg,				
	'PATH_URL'			=> $cfg["path_econtent"],		
	'head_logo'			=> "",		
	'Jscript'			=> array('ui/ui.core_dd','ui/ui.sortable_dd','script', 'assessment_settings', 
							 'plugin/jquery.tablednd_0_5', 'jquery.form'),
	'CSS'				=> array('common', 'master'),
	'showCrumbTrail' 	=> true,
	'CRUMBTRAIL'		=> array(array('url'=>'/src/index.php', 'caption'=>$Lang['General']['Home']),
								 array('url'=>'', 'caption'=>$Lang['Header']['Resource']),
								 array('url'=>'', 'caption'=>$cur_caption)
							),
	'hasMsg'			=> $hasMsg,
	'message'			=> array($message, $style),
	'CONTENTTPL' 		=> $contentTpl, 
	'RESOURCECONTENT' 	=> $resourceContent ,
	'MainContent' 		=> $mainContent ,				
	'bodyExtra'			=> "", 
	'plain_layout'		=> 0,
	'HEADER_CURRENT' 	=> ''
));

############################################
$objSmarty->assign(array(
	'PATH_IMAGE'		=> $cfg["path_images"]."/", 
	'PATH_ROOT' 		=> $eclass_http_root,
	'PATH_CSS'  		=> $cfg["path_css"],
	'PATH_IMAGE_ROOT' => $cfg["path_images_root"],  
	'PATH_JS'   		=> $cfg["path_js"]
)); 

$objSmarty->display('test_master.tpl');

############################################################################
?>
	
<script>
var JsLang = new Array();
JsLang['jsDeleteRubric'] = '<?=$Lang['Rubric']['DeleteRubric']?>';
JsLang['jsRemoveRubricDetail'] = '<?=$Lang['Rubric']['RemoveRubricDetail']?>';
JsLang['jsRubricLevel'] = '<?=$Lang['Rubric']['Level']?>';
JsLang['jsRubricDescription1'] = '<?=$Lang['Rubric']['Description1']?>';
JsLang['jsNewRubricDetail'] = '<?=$Lang['Rubric']['NewRubricDetail']?>';
JsLang['jsNewCriteria'] = '<?=$Lang['Rubric']['NewCriteria']?>';
JsLang['jsRange'] = '<?=$Lang['Rubric']['SetType']['Range']?>';
JsLang['jsLevel'] = '<?=$Lang['Rubric']['SetType']['Level']?>';
JSCriteriaTableCt = '<?=$criteria_table_ct?>';
JsLang['jsStartFromCheckBox'] = '<?=$Lang['Rubric']['series_point_start_item_check_box']?>';
JsLang['jsSeriesLowest'] = '<?=$Lang['Rubric']['SeriesLowestScore']?>';
JsLang['jsSeriesHighest'] = '<?=$Lang['Rubric']['SeriesHighestScore']?>';
JsLang['jsAlertIncorrectScoreRange'] = '<?=$Lang['Rubric']['IncorrectScoreRange']?>';
JsLang['jsCriteria'] = '<?=$Lang['Rubric']['Criteria']?>';
JsLang['jsAlertScoreIntegerFormat'] = '<?=$Lang['SysMsg']['ScoreFormat']?>';
JsLang['ConfirmChangeSeriesScoreBox'] = '<?=$Lang['Assessment']['jsAlertRubricSeriesChangeBoxConfirm']?>';
JsLang['jsAlertEnterSeriesDescription'] = '<?=$Lang['Assessment']['jsAlertEnterSeriesDescription']?>';
JsLang['jsAlertLevelEnterScore'] = '<?=$Lang['Assessment']['jsAlertEnterRubricLevelScore']?>';
var g_score_problem = new Array();

<?
# default add one block of std rubric form when creating new standard rubric 
if($is_edit == 0){
echo $add_row_type_js.'()';

}
?>
</script>