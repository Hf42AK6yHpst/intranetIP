<?php
//modifying By : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
	header("Location: /");
	exit;
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPageArr['IES'] = 1;
$CurrentPage = "Worksheet";

$objIES = new libies();
$ldb = new libdb();

# Result message after operation
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

# toolbar menu
//$html_toolbar = $linterface->GET_LNK_NEW("new.php?scheme_id={$schemeID}",$button_new,"","","",0);

$thisTeacherID = $UserID;

if($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] ){
	//check not a IES Admin login
	//do nothing
}else{
	$teacherSQLQuery = " inner join IES_SCHEME_TEACHER as ies_t  on ies_t.schemeID = scheme.schemeid ";
	$teacherCriteria = " and ies_t.UserID = {$thisTeacherID} ";  // the teacher id is equal to this teacher login
}


# Scheme 
//$sql = "SELECT sch.SchemeID, sch.Title, COUNT(ws.WorksheetID) FROM IES_SCHEME sch LEFT OUTER JOIN IES_WORKSHEET ws ON (ws.SchemeID=sch.SchemeID) GROUP BY sch.SchemeID ORDER BY sch.Title";


//group by ==> there is more than one worksheet for a scheme
$sql = 'select scheme.SchemeID as SchemeID , scheme.Title as Title,  count(ws.WorksheetID) as count
			from IES_SCHEME as scheme 
			'.$teacherSQLQuery.'
			left join IES_WORKSHEET as ws on scheme.SchemeID = ws.SchemeID 
		where 1
		    '.$teacherCriteria.'
		group by scheme.SchemeID, scheme.Title
		order by scheme.title
		';
$schemeData = $ldb->returnArray($sql,3);

if(count($schemeData)>0){
	# table content
	for($i=0,$i_max = count($schemeData); $i<$i_max  ; $i++)
	{	
		$html_worksheet_list .= "<tr>";
		$html_worksheet_list .= "<td valign='top'>".($i+1)."</td>";
		$html_worksheet_list .= "<td valign='top'><a href='worksheet.php?scheme_id=".$schemeData[$i][0]."'>".$schemeData[$i][1]."</a></td>";
		$html_worksheet_list .= "<td valign='top'>".$schemeData[$i][2]."</td>";
		$html_worksheet_list .= "</tr>";
	}
}else{
	$html_worksheet_list .= '<tr><td colspan="3" align="center">'.$Lang['IES']['NoRecordAtThisMoment'].'</td></tr>';
}

$html_js_script .= "
<script language='javascript'>
function removeQ(id) {
	if(confirm('$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete'))	{
		document.getElementById('id').value = id;
		document.form1.action = 'remove.php';
		document.form1.submit();
	}
}

function formSubmit() {
	document.form1.submit();	
}
</script>
";

### Title ###
$title = $Lang['IES']['Worksheet'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
