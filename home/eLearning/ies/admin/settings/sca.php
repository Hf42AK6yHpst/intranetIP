<?php
/** [Modification Log] Modifying By: Connie
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_move_student.php");


//GET HTTP VARIABLE 
$schemeID = intval(trim($schemeID));
$scheme_name = trim($scheme_name);


intranet_auth();
intranet_opendb();


	$li = new libdb();
	$objIES = new libies();
	if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
		$objIES->NO_ACCESS_RIGHT_REDIRECT();
	}
	$res = array();
	
	$SuccessArr = array();
	$li->Start_Trans();
	//$li = new libies_move_student();
	
	$srcSchemeID = 391;
	$trgSchemeID = 617;
	$studentID = 1698;

// check student exist in the target schemeID, if exist, return error

##########################
### copy STUDENT
##########################
//copy student to the scheme first, update the schemeid with new schemeid
//$copyResult = $li->updateStudentSchemeID($srcSchemeID, $trgSchemeID, $studentID,$UserID); //debug fai must be reopen

//$result = $objIES->GET_SCHEME_DETAIL($srcSchemeID); // may useful later

##########################
### copy stage handin 
##########################

/** move data start here (by Connie)**/
// IES_STAGE_STUDENT_20110909  (backup)


$SchemeStudentResult = $objIES->UpdateTableID("IES_SCHEME_STUDENT","SchemeID",$srcSchemeID, $trgSchemeID, $studentID);

//There is a assum that the number of stage in src and trg are equal
$srcStageInfo = $objIES->getTableInfoOrderBy('IES_STAGE', 'SchemeID',$srcSchemeID,'Sequence');
$trgStageInfo = $objIES->getTableInfoOrderBy('IES_STAGE', 'SchemeID',$trgSchemeID,'Sequence');

debug_r($srcStageInfo);
debug_r($trgStageInfo);

for($i = 0,$i_max = count($srcStageInfo); $i < $i_max; $i++){
	$srcStageID = $srcStageInfo[$i]['StageID'];	
	$trgStageID = $trgStageInfo[$i]['StageID'];
	
	$StageStudentResult = $objIES->UpdateTableID("IES_STAGE_STUDENT","StageID",$srcStageID, $trgStageID, $studentID);
	
	debug_r($i);	
	debug_r('$srcStageID');	
	debug_r($srcStageID);	
	
	debug_r('$trgStageID');	
	debug_r($trgStageID);	
	
	
	$ForDebug=$objIES->UpdateTableID("IES_STAGE_HANDIN_BATCH","StageID",$srcStageID,$trgStageID,$studentID);
	
	debug_r('$ForDebug = IES_STAGE_HANDIN_BATCH');	
	debug_r($ForDebug);	
	
	$srcTaskInfo = $objIES->getTableInfoOrderBy('IES_TASK', 'StageID',$srcStageID,'Sequence');
	$trgTaskInfo = $objIES->getTableInfoOrderBy('IES_TASK', 'StageID',$trgStageID,'Sequence');
	for($j = 0,$j_max = count($srcTaskInfo); $j < $j_max; $j++){
		
		$srcTaskID = $srcTaskInfo[$j]['TaskID'];	
		$trgTaskID = $trgTaskInfo[$j]['TaskID'];
		
		$TaskStudentResult = $objIES->UpdateTableID("IES_TASK_STUDENT","TaskID",$srcTaskID, $trgTaskID, $studentID);
		debug_r('$TaskStudentResult');	
		debug_r($TaskStudentResult);
				
		$ForDebug=$objIES->UpdateTableID("IES_TASK_HANDIN","TaskID",$srcTaskID,$trgTaskID,$studentID);
		
		$TaskSnapResult = $objIES->UpdateTableID("IES_TASK_HANDIN_SNAPSHOT","TaskID",$srcTaskID, $trgTaskID, $studentID);
		debug_r('$TaskSnapResult');	
		debug_r($TaskSnapResult);	
		
		debug_r('$ForDebug = IES_TASK_HANDIN');	
		debug_r($ForDebug);	
		
		$srcStepInfo = $objIES->getTableInfoOrderBy('IES_STEP', 'TaskID',$srcTaskID,'Sequence');
		$trgStepInfo = $objIES->getTableInfoOrderBy('IES_STEP', 'TaskID',$trgTaskID,'Sequence');
		
		for($a = 0,$a_max = count($srcStepInfo); $a < $a_max; $a++){
		
			$srcStepID = $srcStepInfo[$a]['StepID'];	
			$trgStepID = $trgStepInfo[$a]['StepID'];
				
			$ForDebug=$objIES->UpdateTableID("IES_QUESTION_HANDIN","StepID",$srcStepID,$trgStepID,$studentID);
			
			debug_r('$ForDebug = IES_QUESTION_HANDIN');	
			debug_r($ForDebug);	
		}
		
	}
	
	
	
	/** end move**/



	##########################
	### copy stage IES_STAGE_HANDIN_BATCH
	##########################
//	if($srcStageSeq == $trgStageSeq){
//		$copyResult[] = $li->updateStageHandInBatch($srcStageID,$trgStageID,$studentID, $UserID);
//	}
//	
	##########################
	### copy stage IES_STAGE_HANDIN_FILE
	##########################
	/*
	$sql = 'select * from IES_STAGE_HANDIN_BATCH  where StageID = '.$srcStageID.' and UserID = '.$studentID;
	$li->returnArray();
	for($batch_i = 0,$batch_i_max  = count($stageHandin); $batch_i < $batch_i_max; $batch_i++){
		$batchID = $stageHandin[$batch_i]['BatchID'];
		$sql = "select * from IES_STAGE_HANDIN_FILE where BatchID = {$batchID}";
	}
	*/
}







	  if (in_array(false, $SuccessArr))
	  {
	  	$li->RollBack_Trans();
	    $msg = "add_failed";
	  }
	  else
	  {
	    $li->Commit_Trans();
	    $msg = "add";
	  }
  	  
  	  intranet_closedb();

	  exit();
	  if($errorMsg=='noerror')
	  {
	  	header("Location: scheme_edit.php?schemeID=".$newSchemeID);
	  }
	  else
	  {//failed
	  	header("Location: scheme_copy.php?msg=".$errorMsg);
	  }
	  
	  
	  	
	
?>
