<?php
//modifying By thomas
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
//GET USER VARIABLE 
$schemeID = intval(trim($schemeID));

if(empty($schemeID) || !is_int($schemeID))
{
	echo "Invalide access , Empty SCHEME ID<br/>";
    exit();
}

$html_schemeID = $schemeID;  // FOR DISPLAY HIDDEN FORM FIELD VALUE

intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$objDB = new libdb();
$li = new libuser($UserID);

$objDB_UI = new libies_ui();


$schemeDetails = $objIES->GET_SCHEME_DETAIL($schemeID);
# Please dont remove this, need to release for later display
//$h_RadioLang = $Lang['IES']['SchemeLang'][$schemeDetails["Language"]];
$h_schemeTitle = $schemeDetails["SchemeType"];
//if the result set (schemeDetails) not equal to one and or is not a array, should have error. Exit the program
/*if(sizeof($schemeDetails) != 1 || !is_array($schemeDetails))
{
	echo "SYSTEM ERROR, SCHEME ID NOT FIND<br/>";
    exit();
}*/
$schemeTitle =$schemeDetails["Title"];
$html_schemeTitle = $schemeTitle;

$html_reset_btn = $linterface->GET_ACTION_BTN($Lang['IES']['Reset'], "button", "ResetSection()");
$html_cancel_reset_btn = $linterface->GET_ACTION_BTN($button_cancel, "button", "tb_remove()");


$stage_arr = $objIES->GET_STAGE_ARR($schemeID);
$html_stage_doc_section = "";
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
	// Section set for stage 3 only
	$_stage_seq = $stage_arr[$i]["Sequence"];
	if($_stage_seq != 3) continue;

	$_stage_id = $stage_arr[$i]["StageID"];
	$_stage_title = $stage_arr[$i]["Title"];

	$html_stage_doc_section .= <<<HTML
<table class="common_table_list ClassDragAndDrop" id="{$ies_cfg['DocSectionType']['section']}" stageID="{$_stage_id}">
	<thead>
		<col width="25%" />
		<col width="70%" />
		<col width="5%" />
	</thead>
	<tbody>
HTML;
	/*
	$sql = "SELECT SectionID, Title ";
	$sql .= "FROM {$intranet_db}.IES_STAGE_DOC_SECTION ";
	$sql .= "WHERE StageID = {$_stage_id} ";
	$sql .= "ORDER BY Sequence";
	$section_arr = $objDB->returnArray($sql);
	*/
	$section_arr = $objIES->GET_SECTION_ARR($_stage_id);
	$h_stage_row_cnt = 2 + count($section_arr);

	$html_stage_doc_section .= <<<HTML
		<tr class="nodrag nodrop">
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th><a href="javascript:;" name="resetSecLnk" schemeID="{$schemeID}" stageSeq="{$_stage_seq}">{$Lang['IES']['Reset']}</a></th>
		</tr>
		<tr class="nodrag nodrop">
			<td rowspan="{$h_stage_row_cnt}">{$_stage_title}</td>
			<td style="display: none;">&nbsp;</td>
			<td style="display: none;">
				<div class="table_row_tool row_content_tool">
					<a href="#TB_inline?height=450&amp;width=750&amp;inlineId=FakeLayer" class="thickbox add_dim" title="Add Class" onclick="Show_Class_Form('','44','3'); return false;"></a>
				</div>
			</td>
		</tr>
HTML;

	for($j=0, $j_max=count($section_arr); $j<$j_max; $j++)
	{
		$_sectionID = $section_arr[$j]["SectionID"];
		$_sectionTitle = $section_arr[$j]["Title"];
		
		/*
		$sql = "SELECT dst.TaskID, t.Title ";
		$sql .= "FROM {$intranet_db}.IES_STAGE_DOC_SECTION_TASK dst ";
		$sql .= "INNER JOIN {$intranet_db}.IES_TASK t ";
		$sql .= "ON dst.TaskID = t.TaskID ";
		$sql .= "WHERE SectionID = {$_sectionID} ";
		$sql .= "ORDER BY dst.Sequence";
		$section_task_arr = $objDB->returnArray($sql);
		*/
		$section_task_arr = $objIES->GET_SECTION_TASK_ARR($_sectionID);
		$section_rowspan_cnt = count($section_task_arr) + 2;
	
		$html_stage_doc_section .= <<<HTML
		<tr class="sub_row" id="{$_sectionID}">
			<td>
				<table class="common_table_list ClassDragAndDrop" id="{$ies_cfg['DocSectionType']['task']}" sectionID="{$_sectionID}">
					<col width="20%" />
					<col width="72%" />
					<col width="8%" />
					<tr class="nodrag nodrop">
						<td rowspan="{$section_rowspan_cnt}">{$_sectionTitle}&nbsp;</td>
					</tr>
HTML;
			
			for($k=0, $k_max=count($section_task_arr); $k<$k_max; $k++){
				$_taskID = $section_task_arr[$k]["TaskID"];
				$_taskTitle = $section_task_arr[$k]["Title"];
			
				$html_stage_doc_section .= <<<HTML
					<tr id="{$_taskID}">
						<td>{$_taskTitle}</td>
						<td class="Dragable">
							<div class="table_row_tool"><a href="#" class="move_order_dim" title="{$Lang['Btn']['Move']}"></a></div>
							<div class="table_row_tool"><a href="#" class="delete_dim" title="{$Lang['Btn']['Delete']}" sectionID="{$_sectionID}" taskID="{$_taskID}"></a></div>
						</td>
					</tr>
HTML;
			}

			$html_stage_doc_section .= <<<HTML
					<tr class="nodrag nodrop">
						<td>&nbsp;</td>
						<td>
							<div class="table_row_tool row_content_tool">
								<a href="scheme_add_doc_section_task.php?schemeID={$schemeID}&stageID={$_stage_id}&sectionID={$_sectionID}&KeepThis=true&TB_iframe=true&height=350&width=500" class="thickbox add_dim" title="{$Lang['IES']['NewDocSection']}"></a>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td class="Dragable">
				<div class="table_row_tool"><a href="#" class="move_order_dim" title="{$Lang['Btn']['Move']}"></a></div>
				<div class="table_row_tool"><a href="#" class="delete_dim" title="{$Lang['Btn']['Delete']}" sectionID="{$_sectionID}"></a></div>
			</td>
		</tr>
HTML;

	}

	$html_stage_doc_section .= <<<HTML
		<tr class="nodrag nodrop">
			<td>&nbsp;</td>
			<td>
				<div class="table_row_tool row_content_tool">
					<a href="scheme_add_doc_section.php?schemeID={$schemeID}&stageID={$_stage_id}&KeepThis=true&TB_iframe=true&height=350&width=500" class="thickbox add_dim" title="{$Lang['IES']['NewDocSection']}"></a>
				</div>
			</td>
		</tr>
HTML;


}

$html_stage_doc_section .= <<<HTML
	</tbody>
</table>
HTML;


//FUNCTION TAB HANDLING
$currentTagFunction = "docExportSetUp";
$tabArray = $ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"];

$tabAppendParameter = "?schemeID={$schemeID}";
$newTab = $objIES->appendParameterToTab($tabAppendParameter,$tabArray);
$html_functionTab = $objIES->GET_TAB_MENU($currentTagFunction,$newTab);	

// navigation
$nav_arr[] = array($Lang['IES']['Scheme'], "scheme_index.php");
$nav_arr[] = array($html_schemeTitle, "");




$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";
### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_export_section.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
