<?php
/** [Modification Log] Modifying By: Connie
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

//GET HTTP VARIABLE 
$schemeID1 = intval(trim($schemeID1));
$schemeID2 = intval(trim($schemeID2));

$schemeIDsrc = 564;
$schemeIDtrg = 600;



intranet_auth();
intranet_opendb();


	$li = new libdb();
	$objIES = new libies();
	if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
		$objIES->NO_ACCESS_RIGHT_REDIRECT();
	}
	$res = array();


	/** it is for IES_SCHEME**/
	$srcSchemeArray = current($objIES->getTableInfo('IES_SCHEME','SchemeID',$schemeIDsrc));
	$trgSchemeArray = current($objIES->getTableInfo('IES_SCHEME','SchemeID',$schemeIDtrg));

	

	
		
	$passNo = 0;
	$failNo = 0;
	$html = '<table width="100%" cellpadding="0" cellspacing="0" border="1">';
	$html .= '<tr><td>&nbsp;</td><td>schemeID Src :'.$schemeIDsrc.'</td><td>schemeID Trg :'.$schemeIDtrg.'</td><td>&nbsp;</td>';
	
	$skipArray = array('SchemeID','Title','DateInput','InputBy','DateModified','ModifyBy');
	foreach ($srcSchemeArray as $key => $srcData){

		if(is_numeric($key)){
			//do nothing
			continue; 
		}
		

		$html .= '<tr>';
		
		$trgData = $trgSchemeArray[$key];
		if($key == 'SchemeID' ){

		}
	
		if(in_array($key , $skipArray)){
			$htmlResult = '<font color="blue">Pass</font>';		
			
		}else{
			if($trgData == $srcData){
				$passNo = $passNo +1;
				$htmlResult = '<font color="green">Pass</font>';
			}else{
				$failNo  = $failNo +1;
				$htmlResult = '<font color="red">Failed</font>';
			}
		}
		
	
		
		$html .= '<td>'.$key.'</td><td>'.$srcData.'</td><td>'.$trgData.'</td><td>'.$htmlResult.'&nbsp;</td>';		



	}
	$html .= '</table>';
	echo '<br/><br/>--------------HERE is IES_SCHEME------------------------<br/><br/>';
	echo '<br/>IES_SCHEME';
	$summary = 'Pass ['.$passNo.']<br/>';
	$summary .= 'Fail ['.$failNo.']<br/>';
	echo '<br/>'.$summary.'<br/>';
	echo $html;
	
	/** it is for IES_STAGE**/
	$srcStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDsrc);
	$trgStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDtrg);


	echo '<br/><br/>--------------HERE is IES_STAGE------------------------<br/><br/>';
	
    for($i=0;$i<count($srcStageArray);$i++)
    { 
    	$skipArray = array('SchemeID','StageID','DateInput','InputBy','DateModified','ModifyBy');
    	
		$passNo = 0;
		$failNo = 0;
		$html = '<table width="100%" cellpadding="0" cellspacing="0" border="1">';
		$html .= '<tr><td>&nbsp;</td><td>schemeID Src :'.$schemeIDsrc.'</td><td>schemeID Trg :'.$schemeIDtrg.'</td><td>&nbsp;</td>';
		foreach ($srcStageArray[$i] as $key => $srcData){
	
			if(is_numeric($key)){
				//do nothing
				continue; 
			}
	
			$html .= '<tr>';
			
			$trgData = $trgStageArray[$i][$key];
			if($key == 'StageID' ){
	
			}
			
			if(in_array($key , $skipArray)){
				$htmlResult = '<font color="blue">Pass</font>';		
			
			}else{
				if($trgData == $srcData){
					$passNo = $passNo +1;
					$htmlResult = '<font color="green">Pass</font>';
				}else{
					$failNo  = $failNo +1;
					$htmlResult = '<font color="red">Failed</font>';
				}
			}
			$html .= '<td>'.$key.'</td><td>'.$srcData.'</td><td>'.$trgData.'</td><td>'.$htmlResult.'&nbsp;</td>';		
	
		}
		$html .= '</table>';
		
		$summary = 'Pass ['.$passNo.']<br/>';
		$summary .= 'Fail ['.$failNo.']<br/>';
		echo '<br/>IES_STAGE';
		echo '<br/>'.$summary.'<br/>';
		echo $html;
	}
	
	
	
	
	/** it is for IES_TASK**/
	echo '<br/><br/>--------------HERE is IES_TASK------------------------<br/><br/>';
	$srcStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDsrc);
	$trgStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDtrg);
	
	for($i=0;$i<count($srcStageArray);$i++)
    { 
    	$stageIDsrc = $srcStageArray[$i]['StageID'];
    	$stageIDtrg = $trgStageArray[$i]['StageID'];
    	
	
		$srcTaskArray = $objIES->getTableInfo('IES_TASK','StageID',$stageIDsrc);
		$trgTaskArray = $objIES->getTableInfo('IES_TASK','StageID',$stageIDtrg);
		for ($j=0;$j<count($srcTaskArray);$j++){
	
			$skipArray = array('TaskID','StageID','DateInput','InputBy','DateModified','ModifyBy');
	
			$passNo = 0;
			$failNo = 0;
			$html = '<table width="100%" cellpadding="0" cellspacing="0" border="1">';
			$html .= '<tr><td>&nbsp;</td><td>schemeID Src :'.$schemeIDsrc.'</td><td>schemeID Trg :'.$schemeIDtrg.'</td><td>&nbsp;</td>';
			$html .= '<tr><td>&nbsp;</td><td>stageID Src :'.$stageIDsrc.'</td><td>stageID Trg :'.$stageIDtrg.'</td><td>&nbsp;</td>';
		
			foreach ($srcTaskArray[$j] as $key => $srcData){
		
				if(is_numeric($key)){
					//do nothing
					continue; 
				}
		
				$html .= '<tr>';
				
				$trgData = $trgTaskArray[$j][$key];
				if($key == 'SchemeID' ){
		
				}
				if(in_array($key , $skipArray)){
					$htmlResult = '<font color="blue">Pass</font>';		
			
				}else{
					if($trgData == $srcData){
						$passNo = $passNo +1;
						$htmlResult = '<font color="green">Pass</font>';
					}else{
						$failNo  = $failNo +1;
						$htmlResult = '<font color="red">Failed</font>';
					}
				}
				$html .= '<td>'.$key.'</td><td>'.$srcData.'</td><td>'.$trgData.'</td><td>'.$htmlResult.'&nbsp;</td>';				
		
			}
			$html .= '</table>';		
			$summary = 'Pass ['.$passNo.']<br/>';
			$summary .= 'Fail ['.$failNo.']<br/>';
			echo '<br/>IES_TASK';
			echo '<br/>'.$summary.'<br/>';
			echo $html;
		}
		
    }

	/** it is for IES_STEP**/
	echo '<br/><br/>--------------HERE is IES_STEP------------------------<br/><br/>';
	$srcStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDsrc);
	$trgStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDtrg);
	
	for($a=0;$a<count($srcStageArray);$a++)
    { 
    	$stageIDsrc = $srcStageArray[$a]['StageID'];
    	$stageIDtrg = $trgStageArray[$a]['StageID'];
    		
		$srcTaskArray = $objIES->getTableInfo('IES_TASK','StageID',$stageIDsrc);
		$trgTaskArray = $objIES->getTableInfo('IES_TASK','StageID',$stageIDtrg);
		
		for($i=0;$i<count($srcTaskArray);$i++)
    	{ 
	    	$taskIDsrc = $srcTaskArray[$i]['TaskID'];
	    	$taskIDtrg = $trgTaskArray[$i]['TaskID'];
	    		
			$srcStepArray = $objIES->getTableInfo('IES_STEP','TaskID',$taskIDsrc);
			$trgStepArray = $objIES->getTableInfo('IES_STEP','TaskID',$taskIDtrg);
		
			for ($j=0;$j<count($srcStepArray);$j++){
		
				$skipArray = array('StepID','TaskID','DateInput','InputBy','DateModified','ModifyBy');
				
				$passNo = 0;
				$failNo = 0;
				$html = '<table width="100%" cellpadding="0" cellspacing="0" border="1">';
				$html .= '<tr><td>&nbsp;</td><td>schemeID Src :'.$schemeIDsrc.'</td><td>schemeID Trg :'.$schemeIDtrg.'</td><td>&nbsp;</td>';
				$html .= '<tr><td>&nbsp;</td><td>stageID Src :'.$stageIDsrc.'</td><td>stageID Trg :'.$stageIDtrg.'</td><td>&nbsp;</td>';
				$html .= '<tr><td>&nbsp;</td><td>taskID Src :'.$taskIDsrc.'</td><td>taskID Trg :'.$taskIDtrg.'</td><td>&nbsp;</td>';
			
				foreach ($srcStepArray[$j] as $key => $srcData){
			
					if(is_numeric($key)){
						//do nothing
						continue; 
					}
			
					$html .= '<tr>';
					
					$trgData = $trgStepArray[$j][$key];
					if($key == 'SchemeID' ){
			
					}
					
					if(in_array($key , $skipArray)){
						$htmlResult = '<font color="blue">Pass</font>';		
			
					}else{
						if($trgData == $srcData){
							$passNo = $passNo +1;
							$htmlResult = '<font color="green">Pass</font>';
						}else{
							$failNo  = $failNo +1;
							$htmlResult = '<font color="red">Failed</font>';
						}
					}
					$html .= '<td>'.$key.'</td><td>'.$srcData.'</td><td>'.$trgData.'</td><td>'.$htmlResult.'&nbsp;</td>';				
			
				}
				$html .= '</table>';
				echo '<br/>IES_STEP';		
				$summary = 'Pass ['.$passNo.']<br/>';
				$summary .= 'Fail ['.$failNo.']<br/>';
				echo '<br/>'.$summary.'<br/>';
				echo $html;
			}
    	}
		
    }
    
    /** it is for IES_STAGE_DOC_SECTION**/
	echo '<br/><br/>--------------HERE is IES_STAGE_DOC_SECTION------------------------<br/><br/>';
	$srcStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDsrc);
	$trgStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDtrg);
	
	for($a=0;$a<count($srcStageArray);$a++)
    { 
    		$stageIDsrc = $srcStageArray[$a]['StageID'];
	    	$stageIDtrg = $trgStageArray[$a]['StageID'];
	    		
			$srcStageDocSectionArray = $objIES->getTableInfo('IES_STAGE_DOC_SECTION','StageID',$stageIDsrc);
			$trgStageDocSectionArray = $objIES->getTableInfo('IES_STAGE_DOC_SECTION','StageID',$stageIDtrg);
			    	
			for ($j=0;$j<count($srcStageDocSectionArray);$j++){
		
				$skipArray = array('SectionID','StageID','DateInput','InputBy','DateModified','ModifyBy');
				$passNo = 0;
				$failNo = 0;
				$html = '<table width="100%" cellpadding="0" cellspacing="0" border="1">';
				$html .= '<tr><td>&nbsp;</td><td>schemeID Src :'.$schemeIDsrc.'</td><td>schemeID Trg :'.$schemeIDtrg.'</td><td>&nbsp;</td>';
				$html .= '<tr><td>&nbsp;</td><td>stageID Src :'.$stageIDsrc.'</td><td>stageID Trg :'.$stageIDtrg.'</td><td>&nbsp;</td>';
			
				foreach ($srcStageDocSectionArray[$j] as $key => $srcData){
			
					if(is_numeric($key)){
						//do nothing
						continue; 
					}
			
					$html .= '<tr>';
					
					$trgData = $trgStageDocSectionArray[$j][$key];
					if($key == 'SchemeID' ){
			
					}
					
					if(in_array($key , $skipArray)){
						$htmlResult = '<font color="blue">Pass</font>';		
			
					}else{
						if($trgData == $srcData){
							$passNo = $passNo +1;
							$htmlResult = '<font color="green">Pass</font>';
						}else{
							$failNo  = $failNo +1;
							$htmlResult = '<font color="red">Failed</font>';
						}
					}
					$html .= '<td>'.$key.'</td><td>'.$srcData.'</td><td>'.$trgData.'</td><td>'.$htmlResult.'&nbsp;</td>';				
			
				}
				$html .= '</table>';
				echo '<br/>IES_STAGE_DOC_SECTION';	
				$summary = 'Pass ['.$passNo.']<br/>';
				$summary .= 'Fail ['.$failNo.']<br/>';
				echo '<br/>'.$summary.'<br/>';
				echo $html;
			}
    }
    
    /** it is for IES_STAGE_DOC_SECTION_TASK**/
	echo '<br/><br/>--------------HERE is IES_STAGE_DOC_SECTION_TASK------------------------<br/><br/>';
	$srcStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDsrc);
	$trgStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDtrg);
	
	for($i=0;$i<count($srcStageArray);$i++)
    { 
    	$stageIDsrc = $srcStageArray[$i]['StageID'];
    	$stageIDtrg = $trgStageArray[$i]['StageID'];
    
    	
		$srcStageDocSectionArray = $objIES->getTableInfo('IES_STAGE_DOC_SECTION','StageID',$stageIDsrc);
		$trgStageDocSectionArray = $objIES->getTableInfo('IES_STAGE_DOC_SECTION','StageID',$stageIDtrg);
			 
	    for($a=0;$a<count($srcStageDocSectionArray);$a++)
    	{ 
	    	$sectionIDsrc = $srcStageDocSectionArray[$a]['SectionID'];
	    	$sectionIDtrg = $trgStageDocSectionArray[$a]['SectionID'];
	    
	    	$sql = 'select * from IES_STAGE_DOC_SECTION_TASK where sectionid = '.$sectionIDsrc.' order by sequence';
	    	$srcStageDocSectionTaskArray = $li->returnArray($sql);
			//$srcStageDocSectionTaskArray = $objIES->getTableInfo('IES_STAGE_DOC_SECTION_TASK','SectionID',$sectionIDsrc);
			$sql = 'select * from IES_STAGE_DOC_SECTION_TASK where sectionid = '.$sectionIDtrg.' order by sequence';
			debug_r($sql);
			//$trgStageDocSectionTaskArray = $objIES->getTableInfo('IES_STAGE_DOC_SECTION_TASK','SectionID',$sectionIDtrg);
			$trgStageDocSectionTaskArray = $li->returnArray($sql);    	   	
			for ($j=0;$j<count($srcStageDocSectionTaskArray);$j++){
		
				$skipArray = array('SectionID','TaskID','DateInput','InputBy','DateModified','ModifyBy');
				$passNo = 0;
				$failNo = 0;
				$html = '<table width="100%" cellpadding="0" cellspacing="0" border="1">';
				$html .= '<tr><td>&nbsp;</td><td>schemeID Src :'.$schemeIDsrc.'</td><td>schemeID Trg :'.$schemeIDtrg.'</td><td>&nbsp;</td>';
				$html .= '<tr><td>&nbsp;</td><td>stageID Src :'.$stageIDsrc.'</td><td>stageID Trg :'.$stageIDtrg.'</td><td>&nbsp;</td>';
				$html .= '<tr><td>&nbsp;</td><td>sectionID Src :'.$sectionIDsrc.'</td><td>sectionID Trg :'.$sectionIDtrg.'</td><td>&nbsp;</td>';
			
				foreach ($srcStageDocSectionTaskArray[$j] as $key => $srcData){
			
					if(is_numeric($key)){
						//do nothing
						continue; 
					}
			
					$html .= '<tr>';
					
					$trgData = $trgStageDocSectionTaskArray[$j][$key];
					if($key == 'SchemeID' ){
			
					}
					
					if(in_array($key , $skipArray)){
						$htmlResult = '<font color="blue">Pass</font>';		
			
					}else{
						if($trgData == $srcData){
							$passNo = $passNo +1;
							$htmlResult = '<font color="green">Pass</font>';
						}else{
							$failNo  = $failNo +1;
							$htmlResult = '<font color="red">Failed</font>';
						}
					}
					$html .= '<td>'.$key.'</td><td>'.$srcData.'</td><td>'.$trgData.'</td><td>'.$htmlResult.'&nbsp;</td>';				
			
				}
				$html .= '</table>';		
				$summary = 'Pass ['.$passNo.']<br/>';
				$summary .= 'Fail ['.$failNo.']<br/>';
				echo '<br/>IES_STAGE_DOC_SECTION_TASK';
				echo '<br/>'.$summary.'<br/>';
				echo $html;
			}
    	}
    }
    
        /** it is for IES_STAGE_MARKING_CRITERIA**/
	echo '<br/><br/>--------------HERE is IES_STAGE_MARKING_CRITERIA------------------------<br/><br/>';
	$srcStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDsrc);
	$trgStageArray = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeIDtrg);
	
	for($a=0;$a<count($srcStageArray);$a++)
    { 
    		$stageIDsrc = $srcStageArray[$a]['StageID'];
	    	$stageIDtrg = $trgStageArray[$a]['StageID'];
	    		
			$srcStageMarkingCriteriaArray = $objIES->getTableInfo('IES_STAGE_MARKING_CRITERIA','StageID',$stageIDsrc);
			$trgStageMarkingCriteriaArray = $objIES->getTableInfo('IES_STAGE_MARKING_CRITERIA','StageID',$stageIDtrg);
			    	
			for ($j=0;$j<count($srcStageMarkingCriteriaArray);$j++){
		
				$skipArray = array('StageMarkCriteriaID','StageID','DateInput','InputBy','DateModified','ModifyBy');
				$passNo = 0;
				$failNo = 0;
				$html = '<table width="100%" cellpadding="0" cellspacing="0" border="1">';
				$html .= '<tr><td>&nbsp;</td><td>schemeID Src :'.$schemeIDsrc.'</td><td>schemeID Trg :'.$schemeIDtrg.'</td><td>&nbsp;</td>';
				$html .= '<tr><td>&nbsp;</td><td>stageID Src :'.$stageIDsrc.'</td><td>stageID Trg :'.$stageIDtrg.'</td><td>&nbsp;</td>';
			
				foreach ($srcStageMarkingCriteriaArray[$j] as $key => $srcData){
			
					if(is_numeric($key)){
						//do nothing
						continue; 
					}
			
					$html .= '<tr>';
					
					$trgData = $trgStageMarkingCriteriaArray[$j][$key];
					if($key == 'SchemeID' ){
			
					}
					if(in_array($key , $skipArray)){
						$htmlResult = '<font color="blue">Pass</font>';		
			
					}else{
						if($trgData == $srcData){
							$passNo = $passNo +1;
							$htmlResult = '<font color="green">Pass</font>';
						}else{
							$failNo  = $failNo +1;
							$htmlResult = '<font color="red">Failed</font>';
						}
					}
					$html .= '<td>'.$key.'</td><td>'.$srcData.'</td><td>'.$trgData.'</td><td>'.$htmlResult.'&nbsp;</td>';				
			
				}
				$html .= '</table>';
				echo '<br/>IES_STAGE_MARKING_CRITERIA';	
				$summary = 'Pass ['.$passNo.']<br/>';
				$summary .= 'Fail ['.$failNo.']<br/>';
				echo '<br/>'.$summary.'<br/>';
				echo $html;
			}
    }
    
    
echo '<br/><br/>--------------------------------------------END-----------------------------------------------------<br/><br/>';
intranet_closedb();	
exit();

	$SuccessArr = array();
	  $li->Start_Trans();


	
?>
