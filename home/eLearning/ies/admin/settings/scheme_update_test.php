<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

//GET HTTP VARIABLE 
$schemeID = intval(trim($schemeID));
$scheme_name = trim($scheme_name);


intranet_auth();
intranet_opendb();


$li = new libdb();
$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
$isNewScheme = ($schemeID == "")?	true	:	false;

  $li->Start_Trans();

	if($isNewScheme)
	{
		$schemeSQL = "INSERT INTO {$intranet_db}.IES_SCHEME (Title, DateInput, InputBy, ModifyBy) VALUES ('".$scheme_name."', NOW(), ".$UserID.", ".$UserID.")";
	}
	else
	{
		$schemeSQL = "update {$intranet_db}.IES_SCHEME set Title = '{$scheme_name}' , ModifyBy = {$UserID} where SchemeID = {$schemeID}";
	}
	$res[] = $li->db_db_query($schemeSQL);

	if($isNewScheme)
	{
		//THIS IS A NEW SCHEME , NEED TO GET THE NEW CREATED SCHEME ID
		$schemeID = $li->db_insert_id();
	}


	//HANDLE STUDENT LIST
	if(!$isNewScheme){ 
		//NOT A NEW SCHEME 

		//REMOVE STUDENT FROM IES_SCHEME_STUDENT WHO DOES NOT IN THE HTML SELECT LIST
		$_studentIDList = "";
		$_studentIDList = (sizeof($StudentSelected) > 0) ? implode(',',$StudentSelected) : "";
			
		if(trim($_studentIDList) != ""){
			$keepStudentList = " AND UserID not in (".$_studentIDList.")";
		}
		
		//case 1) HTML LIST IS EMPTY. USER REMOVE ALL EXISTING STUDENT FROM THE HTML LIST , THEN $keepStudentList WILL BECOME ""
		//		  IT WILL REMOVE ALL THE EXISTING STUDENT IN IES_SCHEME_STUDENT
		//case 2) HTML LIST NOT EMPTY. USER MAY KEEP SOME EXISTING STUDENT , THEN $keepStudentList WILL HAVE VALUE
		
		// Eric Yip (20100730): retrieve student records to be deleted first,
		// to prepare for status update in intranet module
		$getDeleteStudentSql = "SELECT UserID FROM {$intranet_db}.IES_SCHEME_STUDENT ";
		$getDeleteStudentSql .= "WHERE SchemeID = {$schemeID} {$keepStudentList}";
		$deleteStudentArr = $li->returnVector($getDeleteStudentSql);

		$removeStudentSql = "DELETE FROM {$intranet_db}.IES_SCHEME_STUDENT "; 
		$removeStudentSql .= "WHERE SchemeID = {$schemeID} {$keepStudentList}";	
		$res[] = $li->db_db_query($removeStudentSql);
		
		// Eric Yip (20100730): update intranet module status if deleted students do not in any scheme
		$_deletedStudentIDList = (sizeof($StudentSelected) > 0) ? implode(',',$deleteStudentArr) : "";
		$getRemainStudentSql = "SELECT UserID FROM {$intranet_db}.IES_SCHEME_STUDENT ";
		$getRemainStudentSql .= "WHERE UserID IN (".$_deletedStudentIDList.")";
		$remainStudentArr = $li->returnVector($getRemainStudentSql);
		
		// Students who are not in any scheme
		$updateStatusStudentArr = array_diff($deleteStudentArr, $remainStudentArr);
		$libIntranetModule = new libIntranetModule();
		$q_result = $libIntranetModule->updateModuleUserUsageStatus($ies_cfg["moduleCode"],$updateStatusStudentArr,$im_cfg["DB_INTRANET_MODULE_USER_UsageStatus"]["NotYetUsed"]);
	}

	if(!empty($StudentSelected)){
		//GET EXISTING STUDENT IN THE SCHEME {$schemeID}
		$sql = "select SchemeID,UserID,SchemeStudentID from {$intranet_db}.IES_SCHEME_STUDENT where SchemeID = {$schemeID} ";
		$existingStudentList  = $li->returnArray($sql);

		//INSERT ALL THE STUDENT IN HTML LIST TO IES_SCHEME_STUDENT
		for($i=0; $i<count($StudentSelected); $i++){
			$_studentIsExistBefore = false;

			//CHECK WHETHER STUDENT EXIST IN THE SCHEME BEFORE
			for($j = 0;$j < sizeof($existingStudentList);$j++)
			{
				$_existingStudentID = $existingStudentList[$j]["UserID"];
				if($StudentSelected[$i] == $_existingStudentID)
				{
					//studented student exist in the scheme before
					$_studentIsExistBefore = true;
					break;
				}
			}

			if($_studentIsExistBefore == false)
			{
				//ADD THE STUDNET TO THE SCHEME
				$values[] = "(".$schemeID.", ".$StudentSelected[$i].", ".$UserID.")";
				$newlyInsertStudent[] = $StudentSelected[$i];
			}
		}

		if(sizeof($values) > 0)	// this means that $newlyInsertStudent also > 0
		{
			//insert into IES_SCHEME_STUDENT
			$sql = "INSERT INTO {$intranet_db}.IES_SCHEME_STUDENT (SchemeID, UserID, InputBy) VALUES ";
			$sql .= implode(", ", $values);
			$q_result = $li->db_db_query($sql);
			if ($q_result) {
				$libIntranetModule = new libIntranetModule();
				$q_result = $libIntranetModule->updateModuleUserUsageStatus($ies_cfg["moduleCode"],$newlyInsertStudent,$im_cfg["DB_INTRANET_MODULE_USER_UsageStatus"]["Used"]);
			}
			
			$res[] = $q_result;
		}

		unset($values);
  }

  //GET EXISTING TEACHER IN THE SCHEME {$schemeID}
	$sql = "select SchemeTeacherID,SchemeID,UserID,TeacherType from {$intranet_db}.IES_SCHEME_TEACHER where SchemeID = {$schemeID} ";
	$existingTeacherList  = $li->returnArray($sql);

  $res[] = $objIES->handleTeacherToScheme($schemeID,$SelectedTeachingTeacher,$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"],$existingTeacherList,$isNewScheme,$UserID);
	$res[] = $objIES->handleTeacherToScheme($schemeID,$SelectedAssessTeacher,$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"],$existingTeacherList,$isNewScheme,$UserID);

  //HANDLE THE SCHEME ONLY FOR NEW SCHEME
	if($isNewScheme){
	  $mode = ini_get('zend.ze1_compatibility_mode');
	  ini_set('zend.ze1_compatibility_mode', '0');
	  $xmlstr = get_file_content("scheme.default_test.xml");
	  $xml = new SimpleXMLElement($xmlstr);
	  
	  # Update the scheme version
	  $t_scheme_version = $xml[version];
	  if($t_scheme_version == "")
		{
$t_scheme_version = 0;
		}
//	  debug_r($t_scheme_version);
	  $sql = "UPDATE {$intranet_db}.IES_SCHEME SET VERSION = $t_scheme_version WHERE SCHEMEID = $schemeID";
//	  debug_r($sql);
	  $res[] = $li->db_db_query($sql);
	  
	  $env = $plugin['IES_ENV'];
	  for($i=0; $i<count($xml->stage); $i++)
	  {
	  	if (strtoupper($env)=="DEV" || strtoupper($env)=="UAT") $xml->stage[$i][availability] = "ALL";	//	Control the update power of development
	  	if (strtoupper($xml->stage[$i][availability])=="DEVELOPMENT") continue;
	  	
  		$t_stage_seq = (int) $xml->stage[$i]->sequence;
  		$t_stage_title = (string) $xml->stage[$i]->title;
  		$tasks = $xml->stage[$i]->tasks[0];	// tasks is a bundle of task, will only had 1 tag in a stage
  		
  		
  		
  		$sql = "INSERT INTO {$intranet_db}.IES_STAGE (SchemeID, Title, Sequence, DateInput, InputBy, ModifyBy) VALUES (".$schemeID.", '".$t_stage_title."', ".$t_stage_seq.", NOW(), ".$UserID.", ".$UserID.")";
  		$res[] = $li->db_db_query($sql);
  		
  		$t_stage_id = $li->db_insert_id();
  
  		for($j=0; $j<count($tasks->task); $j++)
  		{
  		  $t_task_code = (string) $tasks->task[$j]->code;
  		  $t_task_seq = (int) $tasks->task[$j]->sequence;
  		  $t_task_title = (string) $tasks->task[$j]->title;
  		  $t_task_description = (string) $tasks->task[$j]->description;
  		  $t_task_instantEdit = (int) $tasks->task[$j]->instantedit;
  		  $steps = $tasks->task[$j]->steps;	// steps is a bundle of step, will only had 1 tag in a task
  		  
		  if($t_task_instantEdit == "")
			{
				$t_task_instantEdit = 1;
			}
  		  $sql = "INSERT INTO {$intranet_db}.IES_TASK (StageID, Title, Code, Sequence, Approval, Description, InstantEdit, DateInput, InputBy, ModifyBy) VALUES (".$t_stage_id.", '".$t_task_title."', '".$t_task_code."', ".$t_task_seq.", 1, '".$t_task_description."', ".$t_task_instantEdit.", NOW(), ".$UserID.", ".$UserID.")";
  
  		  $res[] = $li->db_db_query($sql);
  		  
  		  $t_task_id = $li->db_insert_id();
  		  
  		  for($k=0; $k<count($steps->step); $k++)
  		  {
    			$t_step_no = (int) $steps->step[$k]->no;
    			$t_step_seq = (int) $steps->step[$k]->sequence;
    			$t_step_title = (string) $steps->step[$k]->title;
    			$t_save_to_task = (string) $steps->step[$k]->savetotask;
    
    			$sql = "INSERT INTO {$intranet_db}.IES_STEP (TaskID, Title, StepNo, Sequence, SaveToTask, DateInput, InputBy, ModifyBy) VALUES (".$t_task_id.", '".$t_step_title."', ".$t_step_no.", ".$t_step_seq.", ".$t_save_to_task.", NOW(), ".$UserID.", ".$UserID.")";
    			$res[] = $li->db_db_query($sql);
  		  }    
  		}
	  }
	  ini_set('zend.ze1_compatibility_mode', $mode);
	  

	} //if($isNewScheme)

	if (!in_array(false,$res))
  {
    $li->Commit_Trans();
    $msg = "add";
  }
  else
  {
    $li->RollBack_Trans();
    $msg = "add_failed";
  }
//exit();
if($stepSet)
{
  header("Location: scheme_step_edit.php?msg=".$msg."&SchemeID=".$schemeID);
}
else
{
  header("Location: scheme_index.php?msg=".$msg);
}


	
?>
