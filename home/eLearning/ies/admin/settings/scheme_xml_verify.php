<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();


$mode = ini_get('zend.ze1_compatibility_mode');
ini_set('zend.ze1_compatibility_mode', '0');
#	Read XML
$XMLString = get_file_content("scheme.default.xml");
$SimpleXMLElement = new SimpleXMLElement($XMLString);

$duplicationCheck = array();
foreach($SimpleXMLElement->stage as $scheme_key => $stage) {
	$stage_sequence = (int) $stage->sequence;
	
	if (isset($duplicationCheck["stage"][$stage_sequence])) {
		$duplicationCheck["stage"][$stage_sequence]["duplicateSeq"] = 1;
	} else {
		$duplicationCheck["stage"][$stage_sequence]["sequence"] = $stage_sequence;
		$duplicationCheck["stage"][$stage_sequence]["duplicateSeq"] = 0;
	}
	$stage_tasks = $stage->tasks[0];
	$taskCodeCheck = array();
	$stepNoCheck = array();
	foreach($stage_tasks as $stage_key => $task) {
		$task_code = (string) $task->code;
		$task_sequence = (int) $task->sequence;
		if (isset($duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence])) {
			$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["duplicateSeq"] = 1;
		} else {
			$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["sequence"] = $task_sequence;
			$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["duplicateSeq"] = 0;
		}
		
		if (in_array($task_code,$taskCodeCheck)) {
			$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["duplicateCode"] = 1;
		} else {
			$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["duplicateCode"] = 0;
		}
		$taskCodeCheck[] = $task_code;
		
		
		
		
		$task_steps = $task->steps[0];
		foreach($task_steps as $task_key => $step) {
			$step_no = (int) $step->no;
			$step_sequence = (int) $step->sequence;
			if (isset($duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["step"][$step_sequence])) {
				$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["step"][$step_sequence]["duplicateSeq"] = 1;
			} else {
				$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["step"][$step_sequence]["sequence"] = $step_sequence;
				$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["step"][$step_sequence]["duplicateSeq"] = 0;
			}
			
			if (in_array($step_no,$stepNoCheck)) {
				$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["step"][$step_sequence]["duplicateNo"] = 1;
			} else {
				$duplicationCheck["stage"][$stage_sequence]["task"][$task_sequence]["step"][$step_sequence]["duplicateNo"] = 0;
			}
			$stepNoCheck[] = $step_no;
		}
	}
}




$HTML = <<<HTMLEND
<table border=1>
<tr>
<th>
/
</th>
<th>
Stage
</th>
<th>
Task
</th>
<th>
Step
</th>
</tr>
{{{ BODY }}}
</table>
HTMLEND;
//debug_r($duplicationCheck);
if (is_array($duplicationCheck)) {
	foreach($duplicationCheck["stage"] as $stageSequence => $stageElement) {
		if (empty($stageSeq)) {
			$stageSeq = empty($stageElement["duplicateSeq"])?$stageElement["duplicateSeq"]:$stageSequence;
		}
		foreach($stageElement["task"] as $taskSequence => $taskElement) {
			$taskLevelSeq = "$stageSequence - $taskSequence";
			if (empty($taskSeq)) {
				$taskSeq = empty($taskElement["duplicateSeq"])?$taskElement["duplicateSeq"]:$taskLevelSeq;
			}
			if (empty($taskCode)) {
				$taskCode = empty($taskElement["duplicateCode"])?$taskElement["duplicateCode"]:$taskLevelSeq;
			}
			foreach($taskElement["step"] as $stepSequence => $stepElement) {
				$stepLevelSeq = "$stageSequence - $taskSequence - $stepSequence";
				if (empty($stepSeq)) {
					$stepSeq = empty($stepElement["duplicateSeq"])?$stepElement["duplicateSeq"]:$stepLevelSeq;
				}
				if (empty($stepNo)) {
					$stepNo = empty($stepElement["duplicateNo"])?$stepElement["duplicateNo"]:$stepLevelSeq;
				}
			}
		}
		
	}
	
	$sequenceTrArray = array($stageSeq,$taskSeq,$stepSeq);
	$codeTrArray = array("/",$taskCode,"/");
	$noTrArray = array("/","/",$stepNo);
	
	
	
	
	$duplicated = "<span style='color:red'>DUPLICATED</span>";
	$normal = "<span style='color:green'>NORMAL</span>";
	$tbody .= "<tr><td>Sequence</td>";

	foreach($sequenceTrArray as $seqKey => $seqElement) {
		$tbody .= "<td>".($seqElement>0?"$duplicated - $seqElement": ($seqElement=="0"?$normal:$seqElement) )."</td>";
	}
	$tbody .= "</tr>";
	$tbody .= "<tr><td>Code</td>";
	foreach($codeTrArray as $codeKey => $codeElement) {
		$tbody .= "<td>".($codeElement>0?"$duplicated - $codeElement": ($codeElement=="0"?$normal:$codeElement) )."</td>";
	}
	$tbody .= "</tr>";
	$tbody .= "<tr><td>No</td>";
	foreach($noTrArray as $noKey => $noElement) {
		$tbody .= "<td>".($noElement>0?"$duplicated - $noElement": ($noElement=="0"?$normal:$noElement) )."</td>";
	}
	$tbody .= "</tr>";
}
$HTML = str_replace("{{{ BODY }}}", $tbody, $HTML);
echo $HTML;

ini_set('zend.ze1_compatibility_mode', $mode);
?>