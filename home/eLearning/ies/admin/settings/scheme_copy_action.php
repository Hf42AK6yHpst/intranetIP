<?php
/** [Modification Log] Modifying By:
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

//GET HTTP VARIABLE
$schemeID = intval(trim($schemeID));

$scheme_name = trim($scheme_name);


intranet_auth();
intranet_opendb();


	$li = new libdb();
	$objIES = new libies();
	if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
		$objIES->NO_ACCESS_RIGHT_REDIRECT();
	}
	$res = array();

	$SuccessArr = array();
	  $li->Start_Trans();


			/** copy start here connie part **/

			$errorMsg='noerror'; /* it is for checking whether there is any error.*/
			$IsSame='same';

			/**1. Workflow---this part is for copying the record from IES_SCHEME,IES_STAGE,IES_TASK, IES_STEP**/

			$schemeArray = $objIES->getTableInfo('IES_SCHEME','SchemeID',$schemeID);

			$newSchemeInfoArr = array();
			$newSchemeInfoArr['Title'] = $li->Get_Safe_Sql_Query($scheme_name);
			$newSchemeInfoArr['MaxScore'] = $schemeArray[0]['MaxScore'];
			$newSchemeInfoArr['DateInput'] = 'NOW()';
			$newSchemeInfoArr['InputBy'] = $_SESSION['UserID'];
			$newSchemeInfoArr['ModifyBy'] =$_SESSION['UserID'];
			$newSchemeInfoArr['Version'] = $schemeArray[0]['Version'];
			$newSchemeInfoArr['Language'] = $li->Get_Safe_Sql_Query($schemeArray[0]['Language']);
			$newSchemeInfoArr['SchemeType'] = $li->Get_Safe_Sql_Query($schemeArray[0]['SchemeType']);

			$newSchemeID = $objIES->insertScheme($newSchemeInfoArr);

			$oldSchemeID = $schemeArray[0]['SchemeID'];


			$IsSame = compareCopyRecord('SchemeRecord',$oldSchemeID,$newSchemeID);
			if($IsSame=='notsame')
			{
				$errorMsg =1;
				$SuccessArr['compareRecord_SchemeRecord'] = false;

			}
			else{

				/**2.ToopTip--if the original SchemeID contains scheme_lang.php, create new folder for new SchemeID and copy the .php to the folder**/

				$Old_folder_path = "{$intranet_root}/file/ies/lang/scheme/{$oldSchemeID}/scheme_lang.php";

				if (file_exists($Old_folder_path)) {
				    //create scheme_lang.php which saves the detail of the scheme
					$New_folder_path = "{$intranet_root}/file/ies/lang/scheme/{$newSchemeID}/";
					$objsystem = new libfilesystem();
					$objsystem->folder_new($New_folder_path);

					$New_folder_path = $New_folder_path."scheme_lang.php";

					if (!copy($Old_folder_path, $New_folder_path)) {
						$errorMsg =1;
						$SuccessArr['cantcopyfile'] = false; //if cannot copy the file.
					}

				} else {
				}

				/**end part 2**/

				$stageDetailArr = $objIES->getTableInfo('IES_STAGE','SchemeID',$schemeID);

				for($i = 0,$i_max=sizeof($stageDetailArr);$i<$i_max;$i++){

					$newStageInfoArr = array();
					$newStageInfoArr['SchemeID'] = $newSchemeID;
					$newStageInfoArr['Title'] = $li->Get_Safe_Sql_Query($stageDetailArr[$i]['Title']);
					$newStageInfoArr['Description'] = $li->Get_Safe_Sql_Query($stageDetailArr[$i]['Description']);
					$newStageInfoArr['Sequence'] = $stageDetailArr[$i]['Sequence'];
					$newStageInfoArr['Deadline'] = $stageDetailArr[$i]['Deadline'];
					$newStageInfoArr['MaxScore'] = $stageDetailArr[$i]['MaxScore'];
					$newStageInfoArr['Weight'] = $stageDetailArr[$i]['Weight'];
					$newStageInfoArr['DateInput'] = 'NOW()';
					$newStageInfoArr['InputBy'] = $_SESSION['UserID'];
					$newStageInfoArr['ModifyBy'] =$_SESSION['UserID'];


					$newStageID = $objIES->insertStage($newStageInfoArr);
					$oldStageID = $stageDetailArr[$i]['StageID'];

					$IsSame = compareCopyRecord('StageRecord',$oldStageID,$newStageID);
					if($IsSame=='notsame')
					{
						$errorMsg =1;
						$SuccessArr['compareRecord_StageRecord'] = false;
					}
					else
					{
						/**3. Marking settings---this part is for copying the record from IES_STAGE_MARKING_CRITERIA**/
						$MarkingCriteriaDetailArr = $objIES->getTableInfo('IES_STAGE_MARKING_CRITERIA','StageID',$oldStageID);

						for($y = 0,$y_max=sizeof($MarkingCriteriaDetailArr);$y<$y_max;$y++){

							$newStageMarkingCriteriaArr = array();
							$newStageMarkingCriteriaArr['StageID'] = $newStageID;
							$newStageMarkingCriteriaArr['MarkCriteriaID'] = $MarkingCriteriaDetailArr[$y]['MarkCriteriaID'];
							$newStageMarkingCriteriaArr['task_rubric_id'] = $MarkingCriteriaDetailArr[$y]['task_rubric_id'];
							$newStageMarkingCriteriaArr['MaxScore'] = $MarkingCriteriaDetailArr[$y]['MaxScore'];
							$newStageMarkingCriteriaArr['Weight'] = $MarkingCriteriaDetailArr[$y]['Weight'];
							$newStageMarkingCriteriaArr['DateInput'] = 'NOW()';
							$newStageMarkingCriteriaArr['InputBy'] = $_SESSION['UserID'];
							$newStageMarkingCriteriaArr['ModifyBy'] =$_SESSION['UserID'];

							$newStageMarkingCriteriaID = $objIES->insertStageMarkingCriteria($newStageMarkingCriteriaArr);

							$oldStageMarkingCriteriaID = $MarkingCriteriaDetailArr[$y]['StageMarkCriteriaID'];

							$IsSame = compareCopyRecord('StageMarkingCriteriaRecord',$oldStageMarkingCriteriaID,$newStageMarkingCriteriaID);
							if($IsSame=='notsame')
							{
								$errorMsg =1;
								$SuccessArr['compareRecord_StageMarkingCriteriaRecord'] = false;
							}
						}
						/**end part 3**/


						/**4 Word Document Export Settings ---this part is for copying the record from IES_STAGE_DOC_SECTION and IES_STAGE_DOC_SECTION_TASK**/

						$oldStageDocSectionArr = $objIES->getTableInfo('IES_STAGE_DOC_SECTION','StageID',$oldStageID);
						for($t = 0,$t_max=sizeof($oldStageDocSectionArr);$t<$t_max;$t++){

							$newStageDocSectionArr = array();

							$newStageDocSectionArr['StageID'] = $newStageID;
							$newStageDocSectionArr['Sequence'] = $oldStageDocSectionArr[$t]['Sequence'];
							$newStageDocSectionArr['Title'] = $li->Get_Safe_Sql_Query($oldStageDocSectionArr[$t]['Title']);
							$newStageDocSectionArr['DateInput'] = 'NOW()';
							$newStageDocSectionArr['InputBy'] = $_SESSION['UserID'];
							$newStageDocSectionArr['ModifyBy'] =$_SESSION['UserID'];



							$newStageDocSectionID = $objIES->insertStageDocSection($newStageDocSectionArr);
							$oldStageDocSectionID =$oldStageDocSectionArr[$t]['SectionID'];

							$IsSame = compareCopyRecord('StageDocSectionRecord',$oldStageDocSectionID,$newStageDocSectionID);
							if($IsSame=='notsame')
							{
								$errorMsg =1;
								$SuccessArr['compareRecord'] = false;
							}


							$oldStageDocSectionTaskArr = $objIES->getTableInfo('IES_STAGE_DOC_SECTION_TASK','SectionID',$oldStageDocSectionID);
							for($r = 0,$r_max=sizeof($oldStageDocSectionTaskArr);$r<$r_max;$r++){

								$newStageDocSectionTaskArr = array();

								$newStageDocSectionTaskArr['SectionID'] = $newStageDocSectionID;
								$newStageDocSectionTaskArr['TaskID'] = $oldStageDocSectionTaskArr[$r]['TaskID'];
								$newStageDocSectionTaskArr['Sequence'] = $oldStageDocSectionTaskArr[$r]['Sequence'];
								$newStageDocSectionTaskArr['DateInput'] = 'NOW()';
								$newStageDocSectionTaskArr['InputBy'] = $_SESSION['UserID'];
								$newStageDocSectionTaskArr['ModifyBy'] =$_SESSION['UserID'];

								$Fordebug = $objIES->insertStageDocSectionTask($newStageDocSectionTaskArr);


								$StageDocSectionTaskID = $oldStageDocSectionTaskArr[$r]['TaskID'];

								$IsSame = CompareStageDocSectionTaskRecord($oldStageDocSectionID,$newStageDocSectionID,$StageDocSectionTaskID);

								if($IsSame=='notsame')
								{
									$errorMsg =1;
									$SuccessArr['compareRecord_StageDocSectionTaskRecord'] = false;
								}
							}

						}

						/**end part 4**/

						$taskDetailArr = $objIES->getTableInfo('IES_TASK','StageID',$oldStageID);

						for($j = 0,$j_max=sizeof($taskDetailArr);$j<$j_max;$j++){

							$newTaskInfoArr = array();
							$newTaskInfoArr['StageID'] = $newStageID;
							$newTaskInfoArr['Title'] = $li->Get_Safe_Sql_Query($taskDetailArr[$j]['Title']);
							$newTaskInfoArr['Code'] = $li->Get_Safe_Sql_Query($taskDetailArr[$j]['Code']);
							$newTaskInfoArr['Sequence'] = $taskDetailArr[$j]['Sequence'];
							$newTaskInfoArr['Approval'] = $taskDetailArr[$j]['Approval'];
							$newTaskInfoArr['Enable'] = $taskDetailArr[$j]['Enable'];
							$newTaskInfoArr['Description'] = $li->Get_Safe_Sql_Query($taskDetailArr[$j]['Description']);
							$newTaskInfoArr['InstantEdit'] = $taskDetailArr[$j]['InstantEdit'];
							$newTaskInfoArr['DateInput'] = 'NOW()';
							$newTaskInfoArr['InputBy'] = $_SESSION['UserID'];
							$newTaskInfoArr['ModifyBy'] =$_SESSION['UserID'];

							$newTaskID = $objIES->insertTask($newTaskInfoArr);

							$oldTaskID = $taskDetailArr[$j]['TaskID'];

							$IsSame = compareCopyRecord('TaskRecord',$oldTaskID,$newTaskID);
							if($IsSame=='notsame')
							{
								$errorMsg =1;
								$SuccessArr['compareRecord_TaskRecord'] = false;
							}



							$stepDetailArr = $objIES->getTableInfo('IES_STEP','TaskID',$oldTaskID);

							for($n = 0,$n_max=sizeof($stepDetailArr);$n<$n_max;$n++){

								$newStepInfoArr = array();
								$newStepInfoArr['TaskID'] = $newTaskID;
								$newStepInfoArr['Title'] = $li->Get_Safe_Sql_Query($stepDetailArr[$n]['Title']);
								$newStepInfoArr['StepNo'] = $stepDetailArr[$n]['StepNo'];
								$newStepInfoArr['Status'] = $stepDetailArr[$n]['Status'];
								$newStepInfoArr['Sequence'] = $stepDetailArr[$n]['Sequence'];
								$newStepInfoArr['SaveToTask'] = $stepDetailArr[$n]['SaveToTask'];
								$newStepInfoArr['DateInput'] = 'NOW()';
								$newStepInfoArr['InputBy'] = $_SESSION['UserID'];
								$newStepInfoArr['ModifyBy'] =$_SESSION['UserID'];

								$newStepID = $objIES->insertStep($newStepInfoArr);

								$oldStepID = $stepDetailArr[$n]['StepID'];

								$IsSame = compareCopyRecord('StepRecord',$oldStepID,$newStepID);
								if($IsSame=='notsame')
								{
									$errorMsg =1;
									$SuccessArr['compareRecord_StepRecord'] = false;
								}

							}
					  }


					}
					/**Since the TaskID in IES_STAGE_DOC_SECTION_TASK is old, we should update the new TaskID. For Stage 3 only**/
					if($stageDetailArr[$i]['Sequence']==3)
					{
						UpdateNewTaskID($newStageID);
					}

				}
			 }


    /** end part 1**/


	/** copy end here **/

	//	debug_pr($SuccessArr);
		//die();

	  if (in_array(false, $SuccessArr))
	  {
	  	$li->RollBack_Trans();
	    $msg = "add_failed";
	  }
	  else
	  {
	    $li->Commit_Trans();
	    $msg = "add";
	  }

  	  intranet_closedb();
	  if($errorMsg=='noerror')
	  {
	  	$msg='add';
	  	header("Location: scheme_edit.php?schemeID=".$newSchemeID."&msg=".$msg);
	  }
	  else
	  {//failed
	  	header("Location: scheme_copy.php?msg=".$errorMsg);
	  }

	exit();


		/**

        * Compare the Original Record and  Copyed Record by using primary key

        * @owner : Connie (20110906)

        * @param : String $RecordIndicator (to indicate the tableName) eg.SchemeRecord for IES_SCHEME

        * @param : String $OriginalID, eg.SchemeID (original)

        * @param : String $NewID eg.SchemeID (new)

        * @return : $returnResult1 (if 2 records are the same, it would return 'same'. Otherwise, it would return 'notsame')

        *

        */

	function compareCopyRecord($RecordIndicator,$OriginalID,$NewID)
	{
	  global $intranet_db;

	  $ldb = new libdb();

	  $returnResult1= 'same';

	  if($RecordIndicator=='SchemeRecord')
	  {
		$sqlOri = "SELECT * FROM {$intranet_db}.IES_SCHEME WHERE SchemeID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_SCHEME WHERE SchemeID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['MaxScore'],$OriginalArray[0]['Version'],$OriginalArray[0]['Language'], $OriginalArray[0]['SchemeType']);
		$arrayNew = array($NewArray[0]['MaxScore'],$NewArray[0]['Version'], $NewArray[0]['Language'],$NewArray[0]['SchemeType']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);
      }
	  else if($RecordIndicator=='StageRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_STAGE WHERE StageID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_STAGE WHERE StageID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['Title'],$OriginalArray[0]['Description'],$OriginalArray[0]['Sequence'], $OriginalArray[0]['Deadline'],$OriginalArray[0]['MaxScore'],$OriginalArray[0]['Weight']);
		$arrayNew = array($NewArray[0]['Title'],$NewArray[0]['Description'], $NewArray[0]['Sequence'],$NewArray[0]['Deadline'],$NewArray[0]['MaxScore'],$NewArray[0]['Weight']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);
	  }
	  else if($RecordIndicator=='TaskRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_TASK WHERE TaskID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);
		$sqlNew = "SELECT * FROM {$intranet_db}.IES_TASK WHERE TaskID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['Title'],$OriginalArray[0]['Code'],$OriginalArray[0]['Sequence'], $OriginalArray[0]['Approval'],$OriginalArray[0]['Enable'],$OriginalArray[0]['Description'],$OriginalArray[0]['InstantEdit']);
		$arrayNew = array($NewArray[0]['Title'],$NewArray[0]['Code'], $NewArray[0]['Sequence'],$NewArray[0]['Approval'],$NewArray[0]['Enable'],$NewArray[0]['Description'],$NewArray[0]['InstantEdit']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);

	  }
	  else if($RecordIndicator=='StepRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_STEP WHERE StepID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_STEP WHERE StepID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['Title'],$OriginalArray[0]['StepNo'],$OriginalArray[0]['Status'],$OriginalArray[0]['Sequence'],$OriginalArray[0]['SaveToTask']);
		$arrayNew = array($NewArray[0]['Title'],$NewArray[0]['StepNo'], $NewArray[0]['Status'],$NewArray[0]['Sequence'],$NewArray[0]['SaveToTask']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);
	  }
	  else if($RecordIndicator=='StageDocSectionRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_STAGE_DOC_SECTION WHERE SectionID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);
		$sqlNew = "SELECT * FROM {$intranet_db}.IES_STAGE_DOC_SECTION WHERE SectionID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['Sequence'],$OriginalArray[0]['Title']);
		$arrayNew = array($NewArray[0]['Sequence'], $NewArray[0]['Title']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);

	  }
	  else if($RecordIndicator=='StageMarkingCriteriaRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_STAGE_MARKING_CRITERIA WHERE StageMarkCriteriaID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_STAGE_MARKING_CRITERIA WHERE StageMarkCriteriaID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['MarkCriteriaID'],$OriginalArray[0]['task_rubric_id'],$OriginalArray[0]['MaxScore'],$OriginalArray[0]['Weight']);
		$arrayNew = array($NewArray[0]['MarkCriteriaID'], $NewArray[0]['task_rubric_id'],$NewArray[0]['MaxScore'],$NewArray[0]['Weight']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);

	  }
		return $returnResult1;
	}

	/**

        * Compare the Original Record and  Copyed Record by using sectionid and taskid (IES_STAGE_DOC_SECTION_TASK)

        * @owner : Connie (20110908)

        * @param : String $OldSectionID,$NewSectionID,$TaskID

        * @return : $returnResult1 (if 2 records are the same, it would return 'same'. Otherwise, it would return 'notsame')

        *

        */

	function CompareStageDocSectionTaskRecord($OldSectionID,$NewSectionID,$TaskID)
	{
		$ldb = new libdb();
		$sqlOri = "SELECT * FROM {$intranet_db}.IES_STAGE_DOC_SECTION_TASK WHERE SectionID = '{$OldSectionID}' and TaskID = '{$TaskID}' order by sequence";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_STAGE_DOC_SECTION_TASK WHERE SectionID = '{$NewSectionID}' and TaskID = '{$TaskID}' order by sequence";
		$NewArray = $ldb->returnArray($sqlNew);


		$arrayOld = array($OriginalArray[0]['Sequence']);
		$arrayNew = array($NewArray[0]['Sequence']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);

	}

		/**

        * Compare 2 arrays

        * @owner : Connie (20110906)

        * @param : $array1

        * @param : $array2

        * @return : $returnResult1 (if 2 arrays are the same, it would return 'same'. Otherwise, it would return 'notsame')

        *

        */
	function compareTwoArray($array1,$array2)
	{
		$diff = array_diff($array1, $array2);

		if(count($diff)==0) # is the same
		{
			  $returnResult='same';
		}
		else $returnResult='notsame';

		return $returnResult;
	}

/**

        * For update the TaskID in IES_STAGE_DOC_SECTION_TASK

        * @owner : Connie (20110907)

        * @param : $ParNewStageid (Based on the new stageID)

        */
	function UpdateNewTaskID($ParNewStageid)
	{
		$ldb = new libdb();
		$sql = "update IES_STAGE_DOC_SECTION_TASK as doc inner join IES_STAGE_DOC_SECTION as b on doc.sectionid = b.sectionid
				inner join IES_TASK as t on t.taskid = doc.taskid
				inner join IES_TASK as t2 on t.code = t2.code
				set doc.taskid = t2.taskid
				where b.StageID= '".$ParNewStageid."' and t2.stageid ='".$ParNewStageid."'";

		$ForDebug=$ldb->db_db_query($sql);
	}


?>
