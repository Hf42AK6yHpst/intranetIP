<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$li->Start_Trans();

$sql =  "
          DELETE
            scheme, scheme_t, scheme_s, stage, task, step 
          FROM
            {$intranet_db}.IES_SCHEME scheme
            LEFT JOIN {$intranet_db}.IES_SCHEME_TEACHER scheme_t ON scheme.SchemeID = scheme_t.SchemeID
            LEFT JOIN {$intranet_db}.IES_SCHEME_STUDENT scheme_s ON scheme.SchemeID = scheme_s.SchemeID
            INNER JOIN {$intranet_db}.IES_STAGE stage ON scheme.SchemeID = stage.SchemeID
            LEFT JOIN {$intranet_db}.IES_STAGE_DOC_SECTION section ON stage.StageID = section.StageID
            LEFT JOIN {$intranet_db}.IES_STAGE_DOC_SECTION_TASK section_t ON section.SectionID = section_t.SectionID
            LEFT JOIN {$intranet_db}.IES_STAGE_MARKING_CRITERIA ismc ON stage.StageID = ismc.StageID
            INNER JOIN {$intranet_db}.IES_TASK task ON stage.StageID = task.StageID
            INNER JOIN {$intranet_db}.IES_STEP step ON task.TaskID = step.TaskID
          WHERE
            scheme.SchemeID = ".$SchemeID."
        ";
$res = $li->db_db_query($sql);

if($res)
{
	/*
  //delete the scheme detail's folder	
  $folder_path = "{$intranet_root}/file/ies/lang/scheme/{$SchemeID}/";
  $file_path = $folder_path."scheme_lang.php";
  $objsystem = new libfilesystem();
  $objsystem -> file_remove($file_path);
  $objsystem -> folder_remove($folder_path);
  //======
   
   */
  $li->Commit_Trans();
  $msg = "delete";
}
else
{
  $li->RollBack_Trans();
  $msg = "delete_failed";
}

header("Location: scheme_index.php?msg=".$msg);

?>