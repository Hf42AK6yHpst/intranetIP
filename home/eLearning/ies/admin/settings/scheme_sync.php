<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

#	Access Checking
intranet_auth();
intranet_opendb();
$env = $plugin['IES_ENV'];
########################## Core ############################
if ($Sync) {
	$now = date("YmdGis");
	writeLog("Start Synchronize [Time: {$now}]....");
	$mode = ini_get('zend.ze1_compatibility_mode');
	ini_set('zend.ze1_compatibility_mode', '0');
	$libdb = new libdb();
	$libdb->Start_Trans();
	$syncFinalResult = SyncScheme();
	if ($syncFinalResult) {
		writeLog("");
		writeLog("Updated IDs:");
		if (is_array($syncFinalResult)) {
			foreach($syncFinalResult as $finalKey => $finalElement) {
				writeLog($finalKey."[ID]: ");
				writeLog(implode(",",$finalElement));
			}
		}
//		writeLog(serialize($syncFinalResult));
		writeLog("");
		$libdb->Commit_Trans();
	} else {
    	$libdb->RollBack_Trans();
	}
	
	ini_set('zend.ze1_compatibility_mode', $mode);
	$now = date("YmdGis");
	writeLog("End Synchronize [Time: {$now}]....");
} else {
	LoadPage();
}

function SyncScheme() {
	global $env;
	#	Read XML
	$XMLString = get_file_content("scheme.default.xml");
	$SimpleXMLElement = new SimpleXMLElement($XMLString);
	if (strtoupper($SimpleXMLElement[type]) == "IES") {
		#	Load Schemes Info type=ies
		$schemesDetail = libies::GetAllSchemesVersion();
		writeLog("Total existing schemes: ".count($schemesDetail));
		
		writeLog("XML Version: [{$SimpleXMLElement[version]}]");
		writeLog("-------------------------------------------");
		$res = array();
		#	foreach schemes info						|__~~~--------- UPDATE SCHEME ---------~~~__|
		foreach($schemesDetail as $key => $schemeDetail) {
//			if ($schemeDetail["SCHEMEID"] <= 171) {
//				continue;
//			}
		#		Check the version the same with current schemes
		#		if the version are the same
			writeLog("##########################################################################");
			writeLog("Start Process -> [SchemeID: {$schemeDetail["SCHEMEID"]}; Current Version: {$schemeDetail["VERSION"]}] - {$schemeDetail["TITLE"]}");
			writeLog("##########################################################################");
			if ($schemeDetail["VERSION"] == $SimpleXMLElement[version]) {
		#			abort update this scheme
				writeLog("The version is already the newest!");
				writeLog("");
		#		else
			} else {
		#			update this scheme
		#			foreach xml stages					|__~~~--------- UPDATE STAGE ---------~~~__|
				foreach($SimpleXMLElement->stage as $scheme_key => $stage) {
		#				Check the stage availability
					writeLog("----> Processing [Stage Sequence: {$stage->sequence}; Title: {$stage->title}]");
					if (strtoupper($env)=="DEV" || strtoupper($env)=="UAT") $stage[availability] = "ALL";	//	Control the update power of development
					switch (strtoupper($stage[availability])) {
		#				default OR if availiablity == "development"
						default:
						case "DEVELOPMENT":
							writeLog("----> This is a developing stage and no update!");
							break;
		#				if availiablity == "all"
						case "ALL":
		#					update the stage
							$stage_title = $stage->title;
							$stage_sequence = $stage->sequence;
							$stageResult = updateStage($schemeDetail["SCHEMEID"], $stage_sequence, $stage_title);
							$res["stage_update"][] = $stageResult;
							if (!$stageResult) {
								break 3;	#~ break the switch, next stage update and break from current scheme update
							}
							
							$stage_tasks = $stage->tasks[0];		
		#					foreach xml tasks			|__~~~--------- UPDATE TASK ---------~~~__|
							foreach($stage_tasks as $stage_key => $task) {
		#						update the task
								$task_code = $task->code;
								$task_title = $task->title;
								$task_sequence = $task->sequence;
								$task_description = $task->description;
								writeLog("--------> Processing [Task Sequence: {$task->sequence}; Title: {$task->title}]");
								$taskResult = updateTask($schemeDetail["SCHEMEID"],$stage_sequence,$task_sequence,$task_code,$task_title,$task_description);
								$res["task_update"][] = $taskResult;
								if (!$taskResult) {
									break 4;	#~ break next task update, the switch, next stage update and break from current scheme update
								}
								
								$task_steps = $task->steps[0];
		#						foreach xml steps		|__~~~--------- UPDATE STEP ---------~~~__|
								foreach($task_steps as $task_key => $step) {
		#							update the step (with handling extra fields like savetotask)
									$step_no = $step->no;
									$step_sequence = $step->sequence;
									$step_title = $step->title;
									$step_savetotask = $step->savetotask;
									
									writeLog("----------------> Processing [Step Sequence: {$step->sequence}; Title: {$step->title}]");
									$stepResult = updateStep($schemeDetail["SCHEMEID"],$stage_sequence,$task_sequence,$step_sequence,$step_no,$step_title,$step_savetotask,$task_code);
									$res["step_update"][] = $stepResult;
									if (!$stepResult) {
										break 5;	#~ break next step update, next task update, the switch, next stage update and break from current scheme update
									}
								}
							}
							break;	
					}
	
				}
		#			update scheme version
				$schemeResult = updateScheme($schemeDetail["SCHEMEID"],$SimpleXMLElement[version]);
				$res["scheme_update"][] = $schemeResult;
				if (!$schemeResult) {
					break;	#~ break from next scheme update
				}
				writeLog("--------------------------------------------------------------\n");
			}
		}
		$finalUpdateResult = true;
		if (count($res)>0) {
			foreach($res as $resKey => $resValueArray) {
				if (in_array(false,$resValueArray)) {
					$finalUpdateResult = false;
					break;
				}
			}
		} else {
			$finalUpdateResult = false;
		}
		
		
		##########################################################
		##	Final result return
		$returnResult = $res;	//	Return Updated IDs if success
		if ($finalUpdateResult) {
			writeLog("[[[[[[[[[[Start verifying data]]]]]]]]]]");
		#		Verify Data
			$verifyResult = verifyData($SimpleXMLElement, $res);
		#		if verified
			if ($verifyResult) {
		#			show success
				writeLog("Verification Success.....");
		#		else
			} else {
		#			dump error
				writeLog("Verification Failed.....Data mismatch during data verification....");
				$returnResult = false;
			}
			
			writeLog("[[[[[[[[[[End verifying data]]]]]]]]]]");
		} else {
			$returnResult = false;
			writeLog("Error occurred during the process of updating....Or all scheme are already the newest!");
		}
	} else {
		writeLog("The type of the XML should be ies....");
		$returnResult = false;
	}
	//	Return Updated IDs if success
	return $returnResult;
	##########################################################
}
function LoadPage() {
	$HTML = <<<HTMLEND
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title></title>
  </head>
  <body>
    <form name="form1" >
      <input type="submit" value="Sync..." />
      <input type="hidden" name="Sync" value="1"/>
    </form>
  </body>
</html>
HTMLEND;
	echo $HTML;
}

########################## All below are misc functions ############################
function verifyData($ParSimpleXMLElement, $resultIds) {
	global $env,$libdb;
	$result = false;
	
	################################################
	##	Verify
	foreach($resultIds["scheme_update"] as $schemeKey => $schemeId) {
		foreach($ParSimpleXMLElement->stage as $stageKey => $stage) {
			if (strtoupper($env)=="DEV") $stage[availability] = "ALL";	//	Control the update power of development
			switch (strtoupper($stage[availability])) {
				default:
				case "DEVELOPMENT":
					break;
				case "ALL":
					$stage_sequence = $stage->sequence;
					$stage_title = $stage->title;
					
					$stage_tasks = $stage->tasks[0];
					foreach($stage_tasks as $taskKey => $task) {
						$task_sequence = $task->sequence;
						$task_code = $task->code;
						$task_title = $task->title;
						$task_description = $task->description;
						
						$task_steps = $task->steps[0];
						foreach($task_steps as $stepKey => $step) {
							$step_sequence = $step->sequence;
							$step_no = $step->no;
							$step_title = $step->title;
							$step_savetotask = $step->savetotask;
//							$sql = "
//								SELECT
//									STEP.STEPID
//								FROM
//									IES_STEP STEP
//									INNER JOIN IES_TASK TASK
//									ON STEP.TASKID = TASK.TASKID
//									INNER JOIN IES_STAGE STAGE
//									ON TASK.STAGEID = STAGE.STAGEID
//									INNER JOIN IES_SCHEME SCHEME
//									ON STAGE.SCHEMEID = SCHEME.SCHEMEID
//								WHERE
//									SCHEME.SCHEMEID = $schemeId
//									AND STAGE.SEQUENCE = $stage_sequence
//									AND TASK.SEQUENCE = $task_sequence
//									AND STEP.SEQUENCE = $step_sequence
//									AND STAGE.TITLE = '$stage_title'
//									AND TASK.CODE = '$task_code' AND TASK.TITLE = '$task_title' AND TASK.DESCRIPTION = '$task_description'
//									AND STEP.STEPNO = $step_no AND STEP.TITLE = '$step_title' AND STEP.SAVETOTASK = $step_savetotask
//								";
							$sql = "
								SELECT
									STEP.STEPID
								FROM
									IES_STEP STEP
									INNER JOIN IES_TASK TASK
									ON STEP.TASKID = TASK.TASKID
									INNER JOIN IES_STAGE STAGE
									ON TASK.STAGEID = STAGE.STAGEID
									INNER JOIN IES_SCHEME SCHEME
									ON STAGE.SCHEMEID = SCHEME.SCHEMEID
								WHERE
									SCHEME.SCHEMEID = $schemeId
									AND STAGE.SEQUENCE = $stage_sequence
									AND TASK.SEQUENCE = $task_sequence
									AND STEP.SEQUENCE = $step_sequence
									AND STAGE.TITLE = '$stage_title'
									AND TASK.CODE = '$task_code' AND TASK.TITLE = '$task_title' AND TASK.DESCRIPTION = '$task_description'
									AND STEP.STEPNO = $step_no AND STEP.TITLE = '$step_title' AND STEP.SAVETOTASK = $step_savetotask
								";
							$foundResult = current($libdb->returnVector($sql));
							if ($foundResult) {
								writeLog("Matched[StepID: $foundResult; StepTitle: $step_title] - Scheme[ID: $schemeId] > Stage[Sequence: $stage_sequence] > Task[Sequence: $task_sequence] > Step[Sequence: $step_sequence]");
							} else {
								writeLog("Missed[StepTitle: $step_title] - Scheme[ID: $schemeId] > Stage[Sequence: $stage_sequence] > Task[Sequence: $task_sequence] > Step[Sequence: $step_sequence]");
								SQLLog($sql);;
							}
							$res[] = $foundResult;
						}			
					}
					break;
			}
		}
//		$res [] = false;
		if (in_array(false,$res)) {
			$result = false;
		} else {
			$result = true;
		}
	}
	################################################
	return $result;
}
function SQLLog($ParSQLDisplay) {
	
	writeLog("*****************************************\n");
	writeLog($ParSQLDisplay);
	if (mysql_error()) {
		writeLog("");
		writeLog("Update error:\n");
		writeLog(mysql_error());
		writeLog("");
	}
	writeLog("*****************************************\n");
	writeLog("");
	
}
function updateScheme($ParSchemeId, $ParSchemeVersion) {
	global $libdb, $intranet_db;
	$result = false;
	$sql = "UPDATE {$intranet_db}.IES_SCHEME SET VERSION = $ParSchemeVersion WHERE SCHEMEID = $ParSchemeId";
	$updateResult = $libdb->db_db_query($sql);
	if ($updateResult) {
		$result = $ParSchemeId;
	}
	SQLLog($sql);
	return $result;
}
function getStageIdBySchemeIdAndStageSeq($ParSchemeId, $ParStageSequence) {
	global $libdb, $intranet_db;
	$sql = "SELECT STAGE.STAGEID FROM {$intranet_db}.IES_STAGE STAGE
			INNER JOIN IES_SCHEME SCHEME
				ON STAGE.SCHEMEID = SCHEME.SCHEMEID
			WHERE SCHEME.SCHEMEID = $ParSchemeId AND STAGE.SEQUENCE = $ParStageSequence";
	$targetStageId = current($libdb->returnVector($sql));
	return $targetStageId;
}
function getTaskIdBySchemeIdAndStageSeqAndTaskSeq($ParSchemeId, $ParStageSequence, $ParTaskSequence) {
	global $libdb, $intranet_db;
	$sql = "SELECT TASK.TASKID FROM {$intranet_db}.IES_TASK TASK
			INNER JOIN IES_STAGE STAGE
				ON TASK.STAGEID = STAGE.STAGEID
			INNER JOIN IES_SCHEME SCHEME
				ON STAGE.SCHEMEID = SCHEME.SCHEMEID
			WHERE SCHEME.SCHEMEID = $ParSchemeId 
				AND STAGE.SEQUENCE = $ParStageSequence 
				AND TASK.SEQUENCE = $ParTaskSequence";
	$targetTaskId = current($libdb->returnVector($sql));
//	debug_r($sql);
	return $targetTaskId;
}
function getTaskIdBySchemeIdAndStageSeqAndTaskCode($ParSchemeId, $ParStageSequence, $ParTaskCode) {
	global $libdb, $intranet_db;
	$sql = "SELECT TASK.TASKID FROM {$intranet_db}.IES_TASK TASK
			INNER JOIN IES_STAGE STAGE
				ON TASK.STAGEID = STAGE.STAGEID
			INNER JOIN IES_SCHEME SCHEME
				ON STAGE.SCHEMEID = SCHEME.SCHEMEID
			WHERE SCHEME.SCHEMEID = $ParSchemeId 
				AND STAGE.SEQUENCE = $ParStageSequence 
				AND TASK.CODE = '$ParTaskCode'";
	$targetTaskId = current($libdb->returnVector($sql));
	return $targetTaskId;
}
function updateStage($ParSchemeId, $ParStageSequence, $ParStageTitle) {
	global $libdb, $intranet_db, $UserID;
	$result = false;

	$targetStageId = getStageIdBySchemeIdAndStageSeq($ParSchemeId, $ParStageSequence);

	#####################################################################
	##	Just put the fields that you need to update over here
	$updateFieldsArray[] = "TITLE = '{$ParStageTitle}'";
	$updateFieldsString = implode(",",$updateFieldsArray);
	#####################################################################
//	if ($targetStageId==32) {
//		$updateFieldsString .= "....p";
//	}
	if ($targetStageId) {
		$sql = "UPDATE {$intranet_db}.IES_STAGE
				SET $updateFieldsString
				WHERE STAGEID = $targetStageId";
		$updateResultId = $targetStageId;
	} else {
		writeLog("NEW STAGE");
		$sql = "INSERT INTO {$intranet_db}.IES_STAGE
				SET $updateFieldsString,
				SCHEMEID = $ParSchemeId,
				SEQUENCE = $ParStageSequence,
				DATEINPUT = NOW(),
				MODIFYBY = $UserID";
	}
	$result = $libdb->db_db_query($sql);
	SQLLog($sql);
	###################################################
	##	Success Update/Insert return update id / insert id
	$result = chooseIdToShow($result,$updateResultId);
	###################################################
	return $result;
}
function updateTask($ParSchemeId, $ParStageSequence, $ParTaskSequence, $ParTaskCode, $ParTaskTitle, $ParTaskDescription) {
	global $libdb, $intranet_db, $UserID;
	$result = false;
//echo "----->>111";
	$targetTaskId = getTaskIdBySchemeIdAndStageSeqAndTaskCode($ParSchemeId, $ParStageSequence, $ParTaskCode);
//	debug_r($sql);
	#####################################################################
	##	Just put the fields that you need to update over here
//	$updateFieldsArray[] = "CODE = '{$ParTaskCode}'";
	$updateFieldsArray[] = "SEQUENCE = $ParTaskSequence";
	$updateFieldsArray[] = "TITLE = '{$ParTaskTitle}'";
	$updateFieldsArray[] = "DESCRIPTION = '{$ParTaskDescription}'";
	$updateFieldsString = implode(",",$updateFieldsArray);
	#####################################################################
	
	if ($targetTaskId) {
		$sql = "UPDATE {$intranet_db}.IES_TASK
				SET $updateFieldsString
				WHERE TASKID = $targetTaskId";	
		$updateResultId = $targetTaskId;
	} else {
		$stageId = getStageIdBySchemeIdAndStageSeq($ParSchemeId, $ParStageSequence);
		writeLog("NEW TASK");
		$sql = "INSERT INTO {$intranet_db}.IES_TASK
				SET CODE = '{$ParTaskCode}',$updateFieldsString,
				STAGEID = $stageId,
				DATEINPUT = NOW(),
				INPUTBY = $UserID,
				MODIFYBY = $UserID
				APPROVAL = 1";	
	}
	$result = $libdb->db_db_query($sql);
	SQLLog($sql);
	###################################################
	##	Success Update/Insert return update id / insert id
	$result = chooseIdToShow($result,$updateResultId);
	###################################################
	return $result;
}
function updateStep($ParSchemeId, $ParStageSequence, $ParTaskSequence, $ParStepSequence, $ParStepNo, $ParStepTitle, $ParStepSaveToTask,$ParTaskCode) {
	global $libdb, $intranet_db, $UserID;
	$result = false;
	
	$sql = "SELECT STEP.STEPID FROM {$intranet_db}.IES_STEP STEP
			INNER JOIN IES_TASK TASK
				ON STEP.TASKID = TASK.TASKID
			INNER JOIN IES_STAGE STAGE
				ON TASK.STAGEID = STAGE.STAGEID
			INNER JOIN IES_SCHEME SCHEME
				ON STAGE.SCHEMEID = SCHEME.SCHEMEID
			WHERE SCHEME.SCHEMEID = $ParSchemeId
				AND STAGE.SEQUENCE = $ParStageSequence
				AND TASK.CODE = '$ParTaskCode'
				AND STEP.SEQUENCE = $ParStepSequence";
	
	$targetStepId = current($libdb->returnVector($sql));
	
	#####################################################################
	##	Just put the fields that you need to update over here
	$updateFieldsArray[] = "STEPNO = {$ParStepNo}";
	$updateFieldsArray[] = "TITLE = '{$ParStepTitle}'";
	$updateFieldsArray[] = "SAVETOTASK = {$ParStepSaveToTask}";
	$updateFieldsString = implode(",",$updateFieldsArray);
	#####################################################################
	
	if ($targetStepId) {
		$sql = "UPDATE {$intranet_db}.IES_STEP
				SET $updateFieldsString
				WHERE STEPID = $targetStepId";	
		$updateResultId = $targetStepId;
	} else {
//		echo "---->$ParSchemeId,$ParStageSequence,$ParTaskSequence";
		$taskId = getTaskIdBySchemeIdAndStageSeqAndTaskCode($ParSchemeId, $ParStageSequence, $ParTaskCode);
		writeLog("NEW STEP");
		$sql = "INSERT INTO {$intranet_db}.IES_STEP
				SET $updateFieldsString,
				TASKID = $taskId,
				SEQUENCE = $ParStepSequence,
				DATEINPUT = NOW(),
				INPUTBY = $UserID,
				MODIFYBY = $UserID";	
	}
	$result = $libdb->db_db_query($sql);
	SQLLog($sql);
	###################################################
	##	Success Update/Insert return update id / insert id
	$result = chooseIdToShow($result,$updateResultId);
	###################################################
	return $result;
}
function chooseIdToShow($ParResult, $ParUpdateResultId) {
	if ($ParResult){
		if(!isset($ParUpdateResultId)) {	//	success insert
			$ParResult = mysql_insert_id();
		} else {	//
			$ParResult = $ParUpdateResultId;
		}
	}
	return $ParResult;
}
function writeLog($ParContent, $ParShowToScreen=true) {
	global $intranet_root;
	$ParContent .= "\n";
	$today = date("Ymd");
	if ($ParShowToScreen) {
		echo $ParContent."<br/>";
	}
	
	$logPath = $intranet_root."/file/ies/log";
	if(!file_exists($logPath))
	{
	  mkdir($logPath, 0777, true);
	}
	$logFile = "$logPath/scheme_sync_log_$today.log";
	error_log($ParContent, 3, $logFile);
}
		
intranet_closedb();
?>


