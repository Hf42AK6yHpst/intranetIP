<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_dbtable.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

$objIES = new libies();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "DefaultSurveyQuestion";

$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

$sql_que_type = "SUBSTRING_INDEX(REPLACE(SUBSTRING_INDEX(QuestionStr, '||', 1), '#QUE#', ''), ',', 1)";
$sql_que = "SUBSTRING_INDEX(SUBSTRING_INDEX(QuestionStr, '||', 2), '||', -1)";
$sql_opt = "SUBSTRING_INDEX(SUBSTRING_INDEX(QuestionStr, '||', -2), '||', -1)";

$cond = empty($searchText) ? "" : " AND ({$sql_que} LIKE '%".$searchText."%' OR {$sql_opt} LIKE '%".$searchText."%')";

# Main query
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libies_dbtable($field, $order, $pageNo);
$sql = "SELECT ";
$sql .= "CONCAT('<a href=\"/home/eLearning/ies/survey/editDefaultQue.php?question_id=', QuestionID, '&KeepThis=true&TB_iframe=true&height=400&width=700\" class=\"thickbox\">', {$sql_que}, '</a>'), ";
$sql .= "CASE {$sql_que_type} ";
foreach($ies_cfg["Questionnaire"]["Question"] AS $q_type => $q_config_arr)
{        
  $sql .= "WHEN {$q_type} THEN '{$q_config_arr[1]}' ";
}
$sql .= "END, DateModified, ";
$sql .= "CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=checkbox name=\"question_id[]\" value=', QuestionID ,'>') ";
$sql .= "FROM {$intranet_db}.IES_DEFAULT_SURVEY_QUESTION ";
$sql .= "WHERE 1 and (DefaultQuestion <> {$ies_cfg['Questionnaire']['isDefaultQuestion']} or DefaultQuestion  is null) {$cond}";


// TABLE INFO
$LibTable->field_array = array($sql_que_type, $sql_que, "DateModified");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$LibTable->no_col = 5;
$LibTable->table_tag = "<table class=\"common_table_list\">";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list = "<thead>";
$LibTable->column_list .= "<tr class=\"tabletop\">";
$LibTable->column_list .= "<th width=\"5%\">#</th>";
$LibTable->column_list .= "<th width=\"50%\">".$LibTable->column(0,$Lang['IES']['Question'])."</th>";
$LibTable->column_list .= "<th width=\"20%\">".$LibTable->column(1,$Lang['IES']['QuestionFormat'])."</th>";
$LibTable->column_list .= "<th width=\"20%\">".$LibTable->column(2,$Lang['IES']['LastModifiedDate'])."</th>";
$LibTable->column_list .= "<th width=\"5%\">".$LibTable->check("question_id[]")."</th>";
$LibTable->column_list .= "</tr>";
$LibTable->column_list .= "</thead>";

$html_table_content = $LibTable->displayPlain();
$html_table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$html_table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td></tr>" : "";
$html_table_content .= "</table>";

### Title ###
$title = $Lang['IES']['DefaultSurveyQuestion'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>