<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$task_arr = $objIES->GET_TASK_ARR($stageID);

$html_task_selection = "<select name=\"TaskID[]\" multiple size=\"".count($task_arr)."\" style=\"width:100%\">";
for($i=0, $i_max=count($task_arr); $i<$i_max; $i++)
{
	$_taskID = $task_arr[$i]["TaskID"];
	$_taskTitle = $task_arr[$i]["Title"];

	$html_task_selection .= "<option value=\"{$_taskID}\">{$_taskTitle}</option>";
}
$html_task_selection .= "</select>";

$linterface = new interface_html("popup5.html");
$linterface->LAYOUT_START();
?>

<script language="JavaScript">

function checkform(){
	if($("input[name=title]").val() == "")
	{
		alert("<?=$Lang['IES']['Warning']['FillInSectionTitle']?>");
		$("input[name=title]").focus();
		return false;
	}
	
	return true;
}

</script>

<form name="form1" method="POST" action="scheme_add_doc_section_update.php" onSubmit="return checkform();">

	<p class="spacer"></p>
	<table align="center" style="width:95%">
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><?=$Lang['IES']['SectionTitle']?></td>
			<td width="65%"><input type="text" name="title" style="width:100%" /></td>
		</tr>
	</table>
  
  <br />
  <p class="spacer"></p>
  <div class="edit_bottom">
    <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['Btn']['Submit']?>" />
    <input onclick="parent.window.tb_remove();" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['Btn']['Cancel']?>" />
  </div>

<input type="hidden" name="SchemeID" value="<?=$schemeID?>" />
<input type="hidden" name="StageID" value="<?=$stageID?>" />
</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>