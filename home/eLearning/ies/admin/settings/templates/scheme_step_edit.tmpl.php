<script language="JavaScript">
function disableTask(jChkBoxObj){
  var stage_id = $(jChkBoxObj).attr("stage_id");
  var task_id = $(jChkBoxObj).attr("task_id");
  var checked = $(jChkBoxObj).attr("checked");

  $("input[name^=TaskApproval]").each(function(){
    if($(this).attr("stage_id") == stage_id && $(this).attr("task_id") == task_id)
    {
      // Enabled = Approval disabled false
      // Disable = Approval disabled true
      $(this).attr("disabled", !checked);
    }
  });
}

$(document).ready(function(){
  $("input[name^=TaskEnable]").click(function(){
    disableTask($(this));
  });

  $('input[name=taskEnableMaster]').click(function() {
    var stage_id = $(this).attr("stage_id");
    var checked = $(this).attr("checked");
    
    $("input[name^=TaskEnable]").each(function(){
      if($(this).attr("stage_id") == stage_id)
      {
        $(this).attr("checked", checked);
        disableTask($(this));
      }
    });  
	});

	$('input[name=taskApprovalMaster]').click(function() {
    var stage_id = $(this).attr("stage_id");
    var checked = $(this).attr("checked");
    
    $("input[name^=TaskApproval]").each(function(){
      if($(this).attr("stage_id") == stage_id)
      {
        $(this).attr("checked", checked);
      }
    });  
	});
});
</script>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <!-- navigation star -->
      <div class="navigation">
        <?=$html_navigation?>
      </div>
      <!-- navigation end -->
    </td>
    <td align="right">
      <?=$html_message?>
    </td>
  </tr>
</table>

<br style="clear:both" />

<form name="scheme_step_edit" id="scheme_step_edit" action="scheme_step_edit_update.php" method="POST">
<?=$html_functionTab?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="right"><input type="button" onClick="newWindow('../../coursework/index.php?scheme_id=<?=$SchemeID?>', 95)" value="<?=$button_preview?>" /><br/><br/></td>
  </tr>
  <tr>
    <td align="center">
      <table class="common_table_list rights_table_list table_board form_table">
        <tr class="tabletop">
          <th colspan="2">&nbsp;</th >
          <th style="display:none"><?=$Lang['IES']['UseThisFunction']?></th>
          <th><?=$Lang['IES']['Enable']?></th>
          <th><?=$Lang['IES']['NeedTeacherApproval']?></th>
        </tr>
        <?=$html_stage_list?>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
        <p class="spacer"></p>
        <input type="button" class="formbutton"
        value="<?=$button_save?>" onclick="this.form.submit()" />
        <input type="button" class="formbutton"
        value="<?=$button_cancel?>" onclick="window.location='scheme_index.php'" />
        <p class="spacer"></p>
      </div>
    </td>
  </tr>
</table>

<input type="hidden" name="SchemeID" value="<?=$SchemeID?>" />
</form>