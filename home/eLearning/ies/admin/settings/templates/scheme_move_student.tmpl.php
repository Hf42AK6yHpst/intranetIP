<?php
/** [Modification Log] Modifying By: connie
 * *******************************************
 * *******************************************
 */
?>
<script language="JavaScript">
$(document).ready(function(){
	$('select#schemeID option:nth-child(2)').attr('selected', true);
	js_Reload_FromScheme_Student_Selection(1);
	
	$("select#schemeID").change(function(){
    	js_Changed_FromScheme_Selection(this.value);
	});

	$("input#AddAll").click(function(){
		$("select#SchemeStudentSel option").each(function(){$(this).attr("selected","selected");}); 
		MoveSelectedOptions("SchemeStudentSel", "SelectedStudentSel");
	});
	
	$("input#Add").click(function(){
    	MoveSelectedOptions("SchemeStudentSel", "SelectedStudentSel");
	});
	
});

function js_Changed_FromScheme_Selection(jsFromSchemeID) {
	js_Reload_FromScheme_Student_Selection(1);
}

function js_Reload_FromScheme_Student_Selection(jsSelectAll) {
	jsSelectAll = jsSelectAll || 0;
	
	// Change to reload image for the student selection
	$('div#schemeStudentSelDiv', 'form#scheme_new').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	
	// Get the selected scheme
	var jsCurFromSchemeID = $('select#schemeID', 'form#scheme_new').val();
	
	// Get the selected students
	var ExcludeStudentIDPara = '';
	$("select#SelectedStudentSel option").each(function(i){
		ExcludeStudentIDPara += "&ExcludeStudentIDArr[]=" + $(this).val();
	});
	
	// Prepare ajax data
	var url = "ajax/ajax_handler.php";
	var postContent = "Action=reload_scheme_student_selection&SchemeID=" + jsCurFromSchemeID;
	postContent += ExcludeStudentIDPara;
	
	
	
	// Call ajax to reload scheme student selection
	$.ajax({
		type: "POST",
    	url: url,
    	data: postContent,
    	success: function(data) {
			$('div#schemeStudentSelDiv', 'form#scheme_new').html(data);

			if (jsSelectAll == 1) {
				
				$('select#SchemeStudentSel > option').attr('selected', true);
				
			}
			$("select#SchemeStudentSel").width($("#SchemeStudentSel").parent().width());
			$("select#SelectedStudentSel").width($("#SelectedStudentSel").parent().width());
      	}
	});
	
	
}




function MoveSelectedOptions(from_select_id, to_select_id)
{
	$("#"+from_select_id+" option:selected").each(function(i){
    	var student_id = $(this).val();
    	var student_name = $(this).text();
	
    	$("#"+to_select_id).append($("<option></option>").attr("value",student_id).text(student_name));
    	$(this).remove();
	});
  
	$("#"+from_select_id).width($("#"+from_select_id).parent().width());
	$("#"+to_select_id).width($("#"+to_select_id).parent().width());
	
	Reorder_Selection_List(from_select_id);
	Reorder_Selection_List(to_select_id);
}

function Reorder_Selection_List(selectId) {

	var selectList = document.getElementById(selectId);

	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
  }
}

function js_Clicked_Remove_Selected_Student_Button() {
	$('select#SelectedStudentSel > option:selected').remove();
	js_Reload_FromScheme_Student_Selection();
}

function Check_Form() {
	// Connie TBD
	// Get the selected students
	
	if($("select#SelectedStudentSel option").length==0)
	{
		alert("<?=$Lang['IES']['PlsSelectStudent'] ?>");
		return false;
	}
	else
	{
		$("select#SelectedStudentSel option").attr('selected', true);
	}
	
	if (confirm("<?=$Lang['IES']['TransferStudent']?>")) 
	{
		$('form#scheme_new').submit();
	} 
	else {
		return false;
	} 
	
}
</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<div align="right">
<?=$html_message?>
</div>

<br style="clear:both" />
<form id="scheme_new" method="POST" action="student_copy_action.php">
<?=$html_functionTab?>
<table class="form_table">
  <!--col class="field_title" />
  <col class="field_c" /-->

<tr>
<td colspan=3>
<?=$h_notes?>
</td>
</tr>
<?
//[Start] please don't delete support with two lang in future FAI 20101013
?>


 <!-- <tr>/>
    <td><?=$Lang['IES']['SchemeLangCaption']?></td>
    <td>:</td>
    <td><?=$h_RadioLang?></td>
  </tr>/> !-->
<?
//echo $h_RadioLang; //hard code to use BIG5 first , should be remove while support with select lang
//[END] please don't delete support with two lang in future FAI 20101013

?>

<!--  <tr>
    <td>
    	<?=$Lang['IES']['SchemeTitle']?>
    </td>
    <td>:</td>
    <td>
    	<?=$html_schemeTitle?>
    </td>
  </tr>  !-->
  
  <tr>
    <td><?=$Lang['IES']['OldScheme']?></td>
    <td>:</td>
    <td>
  		<table width="100%" border="0" cellspacing="0" cellpadding="5">
  			<tr>
  				<td width="50%" bgcolor="#EEEEEE"><?=$html_fromSchemeSel?></td>
				<td width="40">&nbsp;</td>
				<td width="50%" bgcolor="#EFFEE2" class="steptitletext"><?=$Lang['SysMgr']['FormClassMapping']['StudentSelected']?> </td>
			</tr>

			<tr>
				<td bgcolor="#EEEEEE" align="center">

  					<div id="schemeStudentSelDiv"></div>
  					<span class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?> </span>
          		</td>
          		<td>
		            <input id="AddAll" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width:40px;" title="<?=$Lang['Btn']['AddAll']?>"/><br />
		            <input id="Add" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width:40px;" title="<?=$Lang['Btn']['AddSelected']?>"/><br /><br />
		            <!--
		            <input id="Remove" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveSelected']?>"/><br />
		            <input id="RemoveAll" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveAll']?>"/>
		            -->
		        </td>
          		<td bgcolor="#EFFEE2">
          		 
					<select id="SelectedStudentSel" name="SelectedStudentArr[]" size="10"  multiple="true">
					</select>
					<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], 'button', 'js_Clicked_Remove_Selected_Student_Button();')?>
          		</td>
        	</tr>
      </table>
      <p class="spacer"></p>
    </td>
  </tr>
</table>




<div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
  <p class="spacer"></p>
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['IES']['Submit']?>" onclick="Check_Form();" />
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="window.location='scheme_index.php'" />
  <p class="spacer"></p>
</div>

<input type="hidden" name="stepSet" id="stepSet" />
<input type="hidden" name="trgSchemeID" id="trgSchemeID" value="<?=$html_schemeID?>"/>
</form>
