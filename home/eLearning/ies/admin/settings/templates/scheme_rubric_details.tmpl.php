<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="JavaScript">
function Check_Form() {
	
	var isNumWeight = true;
	var isNumMaxScore = true;
	var isNumCriteriaWeight = true;

	// Check if weight is a number
	$("input[name^=stageWeight]").each(function(key)
	{
		if(!IsNumeric($(this).val()))
		{
			isNumWeight = false;
			return false;
		}
	});
	
	// Check if max score is a number
	$("input[name^=stageMaxScore]").each(function(key)
	{
		var thisMaxScore = $(this).val();

		if(!IsNumeric(thisMaxScore))
		{
			isNumMaxScore = false;
			return false;
		}
	});
	
	// Check if weight is a number
	$("input[name^=stageCriteriaWeight]").each(function(key)
	{
		if(!IsNumeric($(this).val()))
		{
			isNumCriteriaWeight = false;
			return false;
		}
	});
	
	if(!isNumWeight || !isNumCriteriaWeight)
	{
		alert("<?=$Lang['IES']['Warning']['WeightNumeric']?>");
		return false;
	}
	if(!isNumMaxScore)
	{
		alert("<?=$Lang['IES']['Warning']['MaxScoreNumeric']?>");
		return false;
	}

  return true;
}

function Remove_Task_Rubric(task_rubric_id) {
	if(confirm(globalAlertMsg3))
	{
		$("#taskRubricID").val(task_rubric_id);
		$("#scheme_new").attr("action", "scheme_stage_rubric_delete.php");
		$("#scheme_new").submit();
	}
}

$(document).ready(function(){

});

</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<div align="right">
<?=$html_message?>
</div>


<table width = "100%">
  <tr>
  	<td><?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $Lang['IES']['MarkingSettingsInstruction'], "")?></td>
  </tr>
</table>
<br style="clear:both" />
<form id="scheme_new" method="POST" action="scheme_rubric_details_update.php" onSubmit="return Check_Form()">
<?=$html_functionTab?>
<?=$h_content?>
<span class="tabletextrequire">#</span> <?=$Lang['IES']['MaxScoreRestrictedByRubric']?>
<div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
  <p class="spacer"></p>
    <input type="submit" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_save?>" onclick="$('#stepSet').val(0)" />
   
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="window.location='scheme_index.php'" />
  <p class="spacer"></p>
</div>

<input type="hidden" name="stepSet" id="stepSet" />
<input type="hidden" name="schemeID" id="schemeID" value="<?=$html_schemeID?>"/>
<input type="hidden" name="taskRubricID" id="taskRubricID" />
</form>
