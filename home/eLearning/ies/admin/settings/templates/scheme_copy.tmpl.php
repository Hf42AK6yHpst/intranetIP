<?php
/** [Modification Log] Modifying By: Connie
 * *******************************************
 * *******************************************
 */
?>
<script language="JavaScript">
function Check_Form() {
	
}

function checkSchemeNull() {
	var title = Trim($('#scheme_name').val());
	var schemeid = Trim($('#schemeID').val()); // connie part 
	
	if(schemeid=='')
	{
		alert("<?=$Lang['IES']['SelectExistScheme']?>");
		return false;
	}
	if(title=='')
	{
		alert("<?=$Lang['IES']['InputSchemeTitle']?>");
		return false;
	}
	
	$('form#scheme_new').submit();
}


function checkTitleDuplicate(){
	var title = Trim($('#scheme_name').val()); // connie part
	
	$('div.WarningDiv' , 'form#scheme_new').hide();
	
	$.post(
		"ajax/ajax_handler.php", 
		{
			Action:'check_scheme_title',
			schemeTitle: title
		},
		function(ReturnData) {
			if(ReturnData == 1){ //isDuplicate
				$('div#CodeMustNotDuplicateWarningDiv2' , 'form#scheme_new').show();
				$('#scheme_name').focus();
			}
		}
	);
}
</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<div align="right" style="color:red">
<?=$html_message?>
</div>


<br style="clear:both" />
<form id="scheme_new" method="post" action="scheme_copy_action.php">
<?=$html_functionTab?>
<table class="form_table" > <!-- debug-->
  <!--col class="field_title" />
  <col class="field_c" /-->
	<tr>
		<td width="15%"><?=$Lang['IES']['ExistingScheme']?></td>
		<td width="10">:</td>
		<td><?=$h_schemeSelection?></td>
	</tr>
		<tr>
		<td><?=$Lang['IES']['SchemeTitle']?></td>
		<td>:</td>
		<td><input name="scheme_name" type="text" id="scheme_name" class="textbox" onchange="checkTitleDuplicate()" value="<?=intranet_htmlspecialchars($html_schemeTitle)?>"/>
			<?=$linterface->Get_Form_Warning_Msg('CodeMustNotDuplicateWarningDiv2', $Lang['IES']['SchemeTitleDuplicate'], $Class='WarningDiv')?>
		</td>
		
	</tr>
</table>

<div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
  <p class="spacer"></p>
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_save?>" onclick="checkSchemeNull();" />
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="window.location='scheme_index.php'" />
  <p class="spacer"></p>
</div>
</form>
