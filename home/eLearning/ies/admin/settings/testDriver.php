<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/importDefaultCommentBank.php");

intranet_opendb();

$ImportDefaultCommentBank = new ImportDefaultCommentBank();
$ImportDefaultCommentBank->import();

intranet_closedb();
?>