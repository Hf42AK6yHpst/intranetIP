<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$li = new libdb();
$sql = "INSERT INTO {$intranet_db}.IES_STAGE_DOC_SECTION ";
$sql .= "(StageID, Sequence, Title, DateInput, InputBy, ModifyBy) ";
$sql .= "SELECT {$StageID}, IFNULL(MAX(Sequence), 0)+1, '".mysql_real_escape_string(stripslashes($title))."', NOW(), {$UserID}, {$UserID} ";
$sql .= "FROM {$intranet_db}.IES_STAGE_DOC_SECTION ";
$sql .= "WHERE StageID = {$StageID}";
$li->db_db_query($sql);

intranet_closedb();
?>

<script language="JavaScript">
	parent.window.location = "scheme_export_section.php?schemeID=<?=$SchemeID?>&msg=add";
	parent.window.tb_remove();
</script>