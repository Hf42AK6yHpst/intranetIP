<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_rubric.php");
//GET USER VARIABLE 
$schemeID = intval(trim($schemeID));


if(empty($schemeID) || !is_int($schemeID))
{
	echo "Invalide access , Empty SCHEME ID<br/>";
    exit();
    //$schemeID = 0;
    
}
intranet_auth();
intranet_opendb();

$ldb = new libdb();
$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

// Update scheme max score
$sql = "UPDATE {$intranet_db}.IES_SCHEME SET ";
$sql .= "MaxScore = {$schemeMaxScore} ";
$sql .= "WHERE SchemeID = {$schemeID}";
$ldb->db_db_query($sql);

// Update weight and max score of stage
for($i=0, $i_max=count($stageID); $i<$i_max; $i++)
{
	$_stageID = $stageID[$i];
	$_stage_weight = $stageWeight[$_stageID];
	$_stage_maxScore = $stageMaxScore[$_stageID];
	
	$sql = "UPDATE {$intranet_db}.IES_STAGE SET ";
	$sql .= "Weight = {$_stage_weight}, ";
	$sql .= "MaxScore = {$_stage_maxScore} ";
	$sql .= "WHERE StageID = {$_stageID}";
	$ldb->db_db_query($sql);
}

// Update stage criteria weight and max score
for($i=0, $i_max=count($stageCriteriaID); $i<$i_max; $i++)
{
	$_stageMarkingCriteriaID = $stageCriteriaID[$i];
	$_stage_criteria_weight = $stageCriteriaWeight[$_stageMarkingCriteriaID];
	$_stage_criteria_maxScore = $stageCriteriaMaxScore[$_stageMarkingCriteriaID];

	$sql = "UPDATE {$intranet_db}.IES_STAGE_MARKING_CRITERIA SET ";
	$sql .= "Weight = {$_stage_criteria_weight}, ";
	$sql .= "MaxScore = {$_stage_criteria_maxScore} ";
	$sql .= "WHERE StageMarkCriteriaID = {$_stageMarkingCriteriaID}";
	$ldb->db_db_query($sql);
}

$classRoomID = libies_static::getIESRurbicClassRoomID();
$objRubric = new libiesrubric($classRoomID);

//F: save to eclassRubric
if(is_array($rubricCriteria))
{
	foreach($rubricCriteria AS $_stageID => $criteriaArr)
	{
	  foreach($criteriaArr AS $criteriaID => $std_rubric_set_id)
	  {
	    // skip if no rubric selected
	    if(empty($std_rubric_set_id)) continue;
	    
	    $std_rubric_arr = $objRubric->getStandardRubric($std_rubric_set_id);
	    
	    // assume rubric set has only one rubric
	    $std_rubric_id = $std_rubric_arr[0]["STD_RUBRIC_ID"];
	    
	    // Copy standard rubric to task rubric
	    $task_rubric_id = $objRubric->stdRubric2taskRubric($schemeID, $_stageID, $std_rubric_id);
	    
	    // Get max score of rubrics
	    $rubric_max_score = $objRubric->getRubricMaxScore($task_rubric_id);
	    
	    // Map task rubric to stage marking criteria
	    // Update max score by rubrics
	    $sql = "UPDATE {$intranet_db}.IES_STAGE_MARKING_CRITERIA ";
	    $sql .= "SET task_rubric_id = {$task_rubric_id}, MaxScore = {$rubric_max_score} ";
	    $sql .= "WHERE StageID = {$_stageID} AND MarkCriteriaID = {$criteriaID}";
	    $ldb->db_db_query($sql);	    
	  }
	}
}

// re-calculate stage total mark after weight and max mark update
for($i=0, $i_max=count($stageID); $i<$i_max; $i++)
{
	$_stageID = $stageID[$i];
	libies_static::calculateStageMark($_stageID);
}

// re-calculate scheme total mark after weight and max mark update
libies_static::calculateSchemeMark($schemeID);

$msg = "update";

intranet_closedb();
header("Location: scheme_rubric_details.php?msg=".$msg."&schemeID=".$schemeID);

?>