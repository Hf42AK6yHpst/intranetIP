<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

/*
// Save and set steps button
$html_button_display = "<input type=\"submit\" class=\"formbutton\"";
$html_button_display .= "onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" " .
		"value=\"{$Lang['IES']['Button']['SaveAndSetStep']}\" onclick=\"$('#stepSet').val(1)\" />";
*/


$li = new libuser($UserID);
$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$h_schemeSelection = $objIES->getSchemeSelection();


## Navigation Bar ##
$nav_arr[] = array($Lang['IES']['Scheme'], "scheme_index.php");
$nav_arr[] = array($Lang['IES']['Copy'], "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);

##Returned error message###
$errorMsg = $_GET["msg"];
if ($errorMsg==1)
{
	$html_message=$Lang['IES']['CopyFailed'];
}


### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_copy.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
