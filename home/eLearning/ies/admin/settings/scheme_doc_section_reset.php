<?php
//modifying By
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

/*
* Parameter:
* $SchemeID - ID of the scheme
* $StageSeq - Sequence of stage in the scheme
*/

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$li = new libdb();

// Delete all section for the selected scheme and stage (by sequence)
$sql = "DELETE isds, isdst ";
$sql .= "FROM {$intranet_db}.IES_STAGE_DOC_SECTION isds ";
$sql .= "LEFT JOIN {$intranet_db}.IES_STAGE_DOC_SECTION_TASK isdst ON isds.SectionID = isdst.SectionID ";
$sql .= "INNER JOIN {$intranet_db}.IES_STAGE stage ON isds.StageID = stage.StageID ";
$sql .= "WHERE stage.SchemeID = '{$schemeID}' AND stage.Sequence = '{$stageSeq}'";
$li->db_db_query($sql);

if($toDefault)
{
	// Add back section loaded from default
	$sql = "SELECT Language FROM {$intranet_db}.IES_SCHEME WHERE SchemeID = '{$schemeID}'";
	$selectedLang = current($li->returnVector($sql));
	$objIES->addDocSectionFromDefault($schemeID, $selectedLang, $UserID, $stageSeq);
}

intranet_closedb();
header("Location: scheme_export_section.php?schemeID={$schemeID}&msg=update");
?>