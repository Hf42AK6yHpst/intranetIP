<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

$ldb = new libdb();

// Clear stage
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_STAGE_STUDENT");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_STAGE_HANDIN_COMMENT");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_STAGE_HANDIN_FILE");

// Clear task
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_TASK_HANDIN_COMMENT");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_TASK_HANDIN");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_TASK_STUDENT");

// Clear step
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_QUESTION_HANDIN");

// Clear marking
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_MARKING");

?>