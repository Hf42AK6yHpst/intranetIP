<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

$ldb = new libdb();

$cid = 352;

$sql = "TRUNCATE TABLE {$eclass_prefix}c{$cid}.standard_rubric_set";
$ldb->db_db_query($sql);

$sql = "TRUNCATE TABLE {$eclass_prefix}c{$cid}.standard_rubric";
$ldb->db_db_query($sql);

$sql = "TRUNCATE TABLE {$eclass_prefix}c{$cid}.standard_rubric_detail";
$ldb->db_db_query($sql);

$sql = "TRUNCATE TABLE {$eclass_prefix}c{$cid}.task_rubric";
$ldb->db_db_query($sql);

$sql = "TRUNCATE TABLE {$eclass_prefix}c{$cid}.task_rubric_detail";
$ldb->db_db_query($sql);

$sql = "INSERT IGNORE INTO {$intranet_db}.IES_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
$sql .= "VALUES (1, '總分', 'Score', 1, NOW(), 1, 1)";
$ldb->db_db_query($sql);

$sql = "INSERT IGNORE INTO {$intranet_db}.IES_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
$sql .= "VALUES (21, '過程', 'Process', 1, NOW(), 1, 1)";
$ldb->db_db_query($sql);

$sql = "INSERT IGNORE INTO {$intranet_db}.IES_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
$sql .= "VALUES (22, '課業', 'Task', 1, NOW(), 1, 1)";
$ldb->db_db_query($sql);

$sql = "SELECT StageID FROM {$intranet_db}.IES_STAGE";
$stage_id_arr = $ldb->returnVector($sql);

$sql = "SELECT MarkCriteriaID FROM {$intranet_db}.IES_MARKING_CRITERIA";
$mark_criteria_id_arr = $ldb->returnVector($sql);

for($i=0, $i_max=count($stage_id_arr); $i<$i_max; $i++)
{
  for($j=0, $j_max=count($mark_criteria_id_arr); $j<$j_max; $j++)
  {
    $sql = "INSERT IGNORE INTO {$intranet_db}.IES_STAGE_MARKING_CRITERIA (StageID, MarkCriteriaID, DateInput, InputBy, ModifyBy) ";
    $sql .= "VALUES ({$stage_id_arr[$i]}, {$mark_criteria_id_arr[$j]}, NOW(), 1, 1)";
    $ldb->db_db_query($sql);
  }
}

$sql = "UPDATE {$intranet_db}.IES_MARKING im ";
$sql .= "INNER JOIN {$intranet_db}.IES_STAGE_HANDIN_BATCH ishb ON im.AssignmentID = ishb.BatchID AND im.AssignmentType = 2 ";
$sql .= "INNER JOIN {$intranet_db}.IES_STAGE_MARKING_CRITERIA ismc ON ishb.StageID = ismc.StageID AND im.MarkCriteria = ismc.MarkCriteriaID ";
$sql .= "SET im.StageMarkCriteriaID = ismc.StageMarkCriteriaID";
$ldb->db_db_query($sql);

intranet_closedb();
?>