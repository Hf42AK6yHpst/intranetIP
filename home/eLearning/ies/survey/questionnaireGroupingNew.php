<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();
$key = $_REQUEST["key"];
//$key = "U3VydmV5SUQ9MjM3JlN0dWRlbnRJRD0xNjk4JlRhc2tJRD05NzM0JklzU3RhZ2UzPTAmU3RhZ2UzVGFza0lEPSZGcm9tUGFnZT1Db2xsZWN0OlN1cnZleQ==";
$libies_survey = new libies_survey();
$decodedURIs = $libies_survey->breakEncodedURI($key);
$SurveyID = $decodedURIs["SurveyID"];
$TaskID = $decodedURIs["TaskID"];
$StudentID = $decodedURIs["StudentID"];
$FromPage = $decodedURIs["FromPage"];
//debug_r($decodedURIs);
$survey_info_select = $_REQUEST["survey_info_select"];
$updateResult = empty($updateResult)?0:1;

$IS_IES_STUDENT = $libies_survey->isIES_Student($UserID);
##################################################
##	HTML - select_question
$selectedQuestion = $survey_info_select;
$showNoCombine = false;
$js_needConfirm = 1;


############################
## first question display ##
############################
$name = "survey_info_select";
$id = "survey_info_select";
$surveyResultDivID = 'surveyInfoBox';
$onChangeAction="changeSurvey(\"{$id}\",\"{$surveyResultDivID}\");";
$html["select_question"] = $libies_survey->getSelectQuestionBox($SurveyID,$name,$id,$selectedQuestion,$showNoCombine,$onChangeAction);

#############################
## second question display ##
#############################
$name = "survey_info_select2";
$id = "survey_info_select2";
$surveyResultDivID = 'surveyInfoBox2';
$onChangeAction="changeSurvey(\"{$id}\",\"{$surveyResultDivID}\");";
$html["select_question2"] = $libies_survey->getSelectQuestionBox($SurveyID,$name,$id,$selectedQuestion,$showNoCombine,$onChangeAction);

##################################################

$surveyDetails = $libies_survey->getSurveyDetails($SurveyID);
if(is_array($surveyDetails) && count($surveyDetails) == 1) {
	$surveyDetails = current($surveyDetails);
}
$html["survey_title"] = $libies_survey->getFormattedSurveyTitle($surveyDetails["SurveyType"],$surveyDetails["SurveyTypeName"],$surveyDetails["Title"],$key,$IS_IES_STUDENT);

##################################################
##	HTML - tag
$currentTag = "GroupingList";
$html["tag"] = libies_ui::GET_MANAGEMENT_BROAD_TAB_MENU($currentTag,$IS_IES_STUDENT,$key,$FromPage);
##################################################

############################################
##	HTML - close_button
$html["close_button"] = libies_ui::getCloseButton();
############################################
$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);	
$linterface = new interface_html("ies_survey.html");
$html["createCombineBoxReturnMsgDiv"] = $linterface->Get_Thickbox_Return_Message_Layer('createCombineBoxReturnMsgDiv');


// navigation
$nav_arr[] = array($Lang['IES']['GroupingList'], "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$linterface->LAYOUT_START();
include_once("templates/questionnaireGroupingNew.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>