<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

if(empty($QuestionID))
{
  $sql = "INSERT INTO {$intranet_db}.IES_DEFAULT_SURVEY_QUESTION ";
  $sql .= "(QuestionStr, DateInput, InputBy, ModifyBy) ";
  $sql .= "VALUES ";
  $sql .= "('{$qStr}', NOW(), {$UserID}, {$UserID})";

  $msg = "add";
}
else
{
  $sql = "UPDATE {$intranet_db}.IES_DEFAULT_SURVEY_QUESTION ";
  $sql .= "SET ";
  $sql .= "QuestionStr = '{$qStr}', ";
  $sql .= "ModifyBy = {$UserID} ";
  $sql .= "WHERE QuestionID = {$QuestionID}";
  
  $msg = "update";
}
$li->db_db_query($sql);

intranet_closedb();
?>

<script language="JavaScript">
var url = parent.window.location.pathname+"?msg=<?=$msg?>";
parent.window.location = url;
parent.tb_remove();

</script>