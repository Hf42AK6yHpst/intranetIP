<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
?>
<!--<script type="text/javascript" src="<?=$intranet_root?>/templates/ies.js"></script>-->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript">
<!--
var combineTableNumber = 0;
var previousSelectedIndex = 0;
var isEdited = false;
function setSurveyQuestion(qStr)
{
  document.form1.qStr.value = qStr;
}
function changeSurvey(s_divID,r_DivID) {

	var srcDivID = "#"+s_divID;
	var resultDivID = "#"+r_DivID;
	var x_axis = $(srcDivID).val();

	if(isEdited && !confirm("<?=$Lang['IES']['ConfirmChangeMapping']?>")){
		$(srcDivID).attr("selectedIndex",previousSelectedIndex);
	}
	else{
		previousSelectedIndex = $(srcDivID).attr("selectedIndex");
		loadSurveyInfo(s_divID,r_DivID);
		isEdited = false;
		$(resultDivID+" :input").click(function(){isEdited = true;});
		$("#createCombineBox :input").click(function(){isEdited = true;});
		resetCombineButton();
	}
}
function loadSurveyInfo(s_divID,r_DivID) {

	var srcDivID = '';
	var resultDivID = '';
	if(typeof(s_divID) == "undefined"){
		srcDivID = "#survey_info_select";
	}else{
		srcDivID = "#" + s_divID;
	}

	if(typeof(r_DivID) == "undefined"){
		resultDivID = "#surveyInfoBox";
	}else{
		resultDivID = "#"+ r_DivID;
	}

//	var x_axis = $("#survey_info_select").val();
	var x_axis = $(srcDivID).val();
//	$("#surveyInfoBox").html("<center><?=addslashes($linterface->Get_Ajax_Loading_Image())?></center>");
	$(resultDivID).html("<center><?=addslashes($linterface->Get_Ajax_Loading_Image())?></center>");
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=loadSurveyInfo&key=<?=$key?>&XAxis="+x_axis+"&DisplayCreateNewQuestionUI=0",
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
//	  $("#surveyInfoBox").html(result);
	  $(resultDivID).html(result);
			}
	});
}
function addCombineBox() {
	var x_axis = $("#survey_info_select").val();
	combineTableNumber++;
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=loadCombineBox&key=<?=$key?>&CombineTableNumber="+combineTableNumber+"&XAxis="+x_axis,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  $("#createCombineBox").prepend(result);
			  loadCombineBoxContent("combinationInfo[new]["+combineTableNumber+"][QuestionNum]");
			  
			  Get_Return_Message('<?=$Lang['IES']['YouCanInputOtherPairUpGroupHere']?>', 'div#createCombineBoxReturnMsgDiv');
			  Scroll_To_Element('createCombineBoxReturnMsgDiv');
			  $("input.combinationInfoTitle").each( function () {
			  		$(this).focus();
			  		return false;
			  });
			}
	});
}
/* object debug */
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}
function loadCombineBoxContent(id) {
	var x_axis = $("#survey_info_select").val();
	var y_axis = $("#"+getSlashedNameId(id)).val();
	if (y_axis == undefined) {
		y_axis = "";
	}
	
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=loadCombineBoxContent&key=<?=$key?>&XAxis="+x_axis+"&YAxis="+y_axis,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  $target = $("#"+getSlashedNameId((id.replace("QuestionNum","Content"))));
			  $target.html(result);
			  
			  $axisTitle = $("#"+getSlashedNameId((id.replace("QuestionNum","AxisTitle"))));
			  if (y_axis == "") {
			  	$axisTitle.hide();
			  } else {
			  	$axisTitle.show();
			  }
			}
	});
}
function loadMappingTables() {
	var x_axis = $("#survey_info_select").val();
	$("#createCombineBox").html("<center><?=addslashes($linterface->Get_Ajax_Loading_Image())?></center>");
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=loadMappingTables&key=<?=$key?>&XAxis="+x_axis,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  $("#createCombineBox").html("");
			  $("#createCombineBox").prepend(result);
			}
	});
}
function getSlashedNameId(name_or_id) {
	var result = (name_or_id.replace(/\[/g,"\\[")).replace(/\]/g,"\\]");
	return result;
}
function removeBox(id) {
	if (confirm("<?=$Lang['IES']['DeleteWarning']?>")) {
		$("#"+getSlashedNameId(id)).remove();
	}
}

function saveMappings() {
	$checkingTarget =$('.combinationInfoTitle');
	try {
		$checkingTarget.each(function() {
			if ($(this).val()=="") {
				throw false;				
			}
		});
		if ($("#checkCurrent").attr("checked") && $("#currentTitle").val()=="") {
			throw false;
		}
	} catch (e) {
		alert("<?=$Lang['IES']['PleaseFillInCombinationName']?>");
		return;
	}
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=saveMappings&key=<?=$key?>&"+$("form[name=form1]").serialize(),
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  if (Number(result) == 1) {
			  	$("input[name=updateResult]").val(1);
			  	$("form[name=form1]").attr("action","");
				$("form[name=form1]").submit();
			  } else {
			  	$("input[name=updateResult]").val(0);
			  	$("#showFailed").show().fadeOut(5000);
			  }
//			  $("#createCombineBox").prepend(result);
			}
	});
}

//copy from saveMappings , saveMappings may be don't use later
function saveMappings2(){
		try{
			if($("#combinationInfo_Title").val() == ""){
				throw false;
			}
		}catch(e){
			alert("<?=$Lang['IES']['PleaseFillInCombinationName']?>");
			$("#combinationInfo_Title").focus();
			return;
		}
		$("#saveMappingBtn").attr("disabled", true);

		$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=saveMappings2&key=<?=$key?>&"+$("form[name=form1]").serialize(),
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  if(Number(result) ==1){
					goGroupingList();

			  }else{
				  	goGroupingList();
			  }
			  return;
			  if (Number(result) == 1) {
			  	$("input[name=updateResult]").val(1);
			  	$("form[name=form1]").attr("action","");
				$("form[name=form1]").submit();
			  } else {
			  	$("input[name=updateResult]").val(0);
			  	$("#showFailed").show().fadeOut(5000);
			  }

			}
	});

}
function displayCombineResult(){

	var question1 = $("#survey_info_select").val();
	var question2 = $("#survey_info_select2").val();

	if(question1 == question2){
		alert('<?=$Lang['IES']['Warning']['CompareEqualQuestion']?>');
		return;
	}
	
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=showCombineQuestionAnalysis&key=<?=$key?>&question1="+question1+"&question2="+question2,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  $("#createCombineBox").prepend(result);
			  	$("#combineResult").hide();
				$("#saveMappingBtn").attr("disabled", false);
			}
	});
}
function resetCombineButton(){
	$("#combineResult").show();
	$("#createCombineBox").html('');
	$("#saveMappingBtn").attr("disabled", true);
}
function showHideCurrentTitle() {
	if ($("#checkCurrent").attr("checked")) {
		$("#currentTitleDiv").show();
	} else {
		$("#currentTitleDiv").hide();
	}
}

$(document).ready(function() {
	loadSurveyInfo();
	loadSurveyInfo("survey_info_select2","surveyInfoBox2");
//	loadMappingTables();
	if (<?=$updateResult?>==1) {
		$("#showSuccess").show().fadeOut(5000);
		$("input[name=updateResult]").val(0);
	}
	previousSelectedIndex = $("#survey_info_select").attr("selectedIndex");
	$("#surveyInfoBox :input").click(function(){isEdited = true;});
	$("#createCombineBox :input").click(function(){isEdited = true;});

	$("#saveMappingBtn").attr("disabled", true);
});
-->

</script>

<form name="form1" method="POST" action="">
<div class="q_header">
<h3><?=$html["survey_title"]?></h3>
<?=$html["tag"]?> 
</div><!-- q_header end -->
<div class="q_content">

    <!-- navigation star -->
		<div class="navigation"><?=$html_navigation?>
		</div>		
	<!-- navigation end -->
	<?=$Lang['IES']['GroupingInstruction2']?>

<div ><br/><?=$Lang['IES']['ChooseQuestion']?><?=$Lang['_symbol']["colon2"]?> <?=$html["select_question"]?>

<!--span id="showSuccess" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordUpdated())?></span-->
<!--span id="showFailed" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordFail())?></span-->
<br/>
<br/>
</div>



<div class="ies_q_box" id="surveyInfoBox">
</div>

<div ><br/><?=$Lang['IES']['ChooseQuestion']?><?=$Lang['_symbol']["colon2"]?> <?=$html["select_question2"]?>

<!--span id="showSuccess" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordUpdated())?></span-->
<!--span id="showFailed" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordFail())?></span-->
<br/>
<br/>
</div>

<div class="ies_q_box" id="surveyInfoBox2">
</div>

<div class="ies_q_box" id="combineResult">
				<div class="btn">
				<input type="button" value="<?=$Lang['IES']['newgrouping']?>" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" class="formbutton" name="submit2" onClick="displayCombineResult()">
				</div>
</div>
<?=$html["createCombineBoxReturnMsgDiv"]?>
<div id="createCombineBox"></div>
<div><span style="color:red">*</span><?=$Lang['IES']['GreyCantMap']?></div>
  <!-- submit btn start -->
  <div class="edit_bottom"> 
   <!-- <span> 更新日期 :兩天前</span>-->
    <p class="spacer"></p>    
    <input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"  value=" <?=$Lang['IES']['Save']?> " onClick="saveMappings2()" id="saveMappingBtn"/>
    <?=$html["close_button"]?>
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->
</div> <!-- q_content end -->


<!--input type="hidden" name="currentQuestionNumber" /-->
<input type="hidden" name="qStr" />
<input type="hidden" name="updateResult" />
<input type="hidden" name="key" value="<?=$key?>" />
</form>

