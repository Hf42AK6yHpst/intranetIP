<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
?>
<!--<script type="text/javascript" src="<?=$intranet_root?>/templates/ies.js"></script>-->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript">
<!--
var combineTableNumber = 0;
var previousSelectedIndex = 0;
var isEdited = false;
function setSurveyQuestion(qStr)
{
  document.form1.qStr.value = qStr;
}
function changeSurvey() {
	var x_axis = $("#survey_info_select").val();

	if(isEdited && !confirm("<?=$Lang['IES']['ConfirmChangeMapping']?>")){
		$("#survey_info_select").attr("selectedIndex",previousSelectedIndex);
	}
	else{
		previousSelectedIndex = $("#survey_info_select").attr("selectedIndex");
		loadSurveyInfo();
		loadMappingTables();
		isEdited = false;
		$("#surveyInfoBox :input").click(function(){isEdited = true;});
		$("#createCombineBox :input").click(function(){isEdited = true;});
	}
}
function loadSurveyInfo() {
	var x_axis = $("#survey_info_select").val();
	$("#surveyInfoBox").html("<center><?=addslashes($linterface->Get_Ajax_Loading_Image())?></center>");
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=loadSurveyInfo&key=<?=$key?>&XAxis="+x_axis,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  $("#surveyInfoBox").html(result);
			}
	});
}
function addCombineBox() {
	var x_axis = $("#survey_info_select").val();
	combineTableNumber++;
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=loadCombineBox&key=<?=$key?>&CombineTableNumber="+combineTableNumber+"&XAxis="+x_axis,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  $("#createCombineBox").prepend(result);
			  loadCombineBoxContent("combinationInfo[new]["+combineTableNumber+"][QuestionNum]");
			  
			  Get_Return_Message('<?=$Lang['IES']['YouCanInputOtherPairUpGroupHere']?>', 'div#createCombineBoxReturnMsgDiv');
			  Scroll_To_Element('createCombineBoxReturnMsgDiv');
			  $("input.combinationInfoTitle").each( function () {
			  		$(this).focus();
			  		return false;
			  });
			}
	});
}
/* object debug */
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}
function loadCombineBoxContent(id) {
	var x_axis = $("#survey_info_select").val();
	var y_axis = $("#"+getSlashedNameId(id)).val();
	if (y_axis == undefined) {
		y_axis = "";
	}
	
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=loadCombineBoxContent&key=<?=$key?>&XAxis="+x_axis+"&YAxis="+y_axis,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  $target = $("#"+getSlashedNameId((id.replace("QuestionNum","Content"))));
			  $target.html(result);
			  
			  $axisTitle = $("#"+getSlashedNameId((id.replace("QuestionNum","AxisTitle"))));
			  if (y_axis == "") {
			  	$axisTitle.hide();
			  } else {
			  	$axisTitle.show();
			  }
			}
	});
}
function loadMappingTables() {
	var x_axis = $("#survey_info_select").val();
	$("#createCombineBox").html("<center><?=addslashes($linterface->Get_Ajax_Loading_Image())?></center>");
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=loadMappingTables&key=<?=$key?>&XAxis="+x_axis,
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  $("#createCombineBox").html("");
			  $("#createCombineBox").prepend(result);
			}
	});
}
function getSlashedNameId(name_or_id) {
	var result = (name_or_id.replace(/\[/g,"\\[")).replace(/\]/g,"\\]");
	return result;
}
function removeBox(id) {
	if (confirm("<?=$Lang['IES']['DeleteWarning']?>")) {
		$("#"+getSlashedNameId(id)).remove();
	}
}

function saveMappings() {
	$checkingTarget =$('.combinationInfoTitle');
	try {
		$checkingTarget.each(function() {
			if ($(this).val()=="") {
				throw false;				
			}
		});
		if ($("#checkCurrent").attr("checked") && $("#currentTitle").val()=="") {
			throw false;
		}
	} catch (e) {
		alert("<?=$Lang['IES']['PleaseFillInCombinationName']?>");
		return;
	}
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=saveMappings&key=<?=$key?>&"+$("form[name=form1]").serialize(),
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();
			  if (Number(result) == 1) {
			  	$("input[name=updateResult]").val(1);
			  	$("form[name=form1]").attr("action","");
				$("form[name=form1]").submit();
			  } else {
			  	$("input[name=updateResult]").val(0);
			  	$("#showFailed").show().fadeOut(5000);
			  }
//			  $("#createCombineBox").prepend(result);
			}
	});
}

function showHideCurrentTitle() {
	if ($("#checkCurrent").attr("checked")) {
		$("#currentTitleDiv").show();
	} else {
		$("#currentTitleDiv").hide();
	}
}

$(document).ready(function() {
	loadSurveyInfo();
	loadMappingTables();
	if (<?=$updateResult?>==1) {
		$("#showSuccess").show().fadeOut(5000);
		$("input[name=updateResult]").val(0);
	}
	previousSelectedIndex = $("#survey_info_select").attr("selectedIndex");
	$("#surveyInfoBox :input").click(function(){isEdited = true;});
	$("#createCombineBox :input").click(function(){isEdited = true;});
});
-->
</script>

<form name="form1" method="POST" action="">
<div class="q_header">
<h3><?=$html["survey_title"]?></h3>
<?=$html["tag"]?> 
</div><!-- q_header end -->
<div class="q_content">

    <!-- navigation star -->
		<div class="navigation"><?=$html_navigation?>
		</div>		
	<!-- navigation end -->
	<?=$Lang['IES']['GroupingInstruction']?>
<div class="content_top_tool"><br/><?=$Lang['IES']['ChooseQuestion']?><?=$Lang['_symbol']["colon"]?> <?=$html["select_question"]?>
<span id="showSuccess" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordUpdated())?></span>
<span id="showFailed" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordFail())?></span>
<br/>
<br/>
</div>
<div class="ies_q_box" id="surveyInfoBox">
</div>
<?=$html["createCombineBoxReturnMsgDiv"]?>
<div id="createCombineBox"></div>
<div><span style="color:red">*</span><?=$Lang['IES']['GreyCantMap']?></div>
  <!-- submit btn start -->
  <div class="edit_bottom"> 
   <!-- <span> 更新日期 :兩天前</span>-->
    <p class="spacer"></p>    
    <input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"  value=" <?=$Lang['IES']['Save']?> " onClick="saveMappings()"/>
    <?=$html["close_button"]?>
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->
</div> <!-- q_content end -->


<!--input type="hidden" name="currentQuestionNumber" /-->
<input type="hidden" name="qStr" />
<input type="hidden" name="updateResult" />
<input type="hidden" name="key" value="<?=$key?>" />
</form>

