<?php
/** [Modification Log] Modifying By: Stanley
 * *******************************************
 * *******************************************
 */
?>
<script type="text/javascript" src="<?=$intranet_root?>/templates/ies.js"></script>
<script>

function delete_email(obj, id){
	if (confirm("<?=$Lang['IES']['DeleteWarning']?>")) {
		$.ajax({
			url:      "ajax/ajax_delete_email.php",
			type:     "POST",
			data:     "SurveyEmailID="+id,
			success:  function(xml){				 
				 	if (xml) {
				 		$(obj).parents("tr:first").remove();
				 	} else {
				 		alert("Remove Failed");
				 	}
				}
		});
	}
}

</script>
<div class="q_wrapper">
<form name="form1" method="POST" action="">
	<div class="q_header">
    	
    	<h3>
        <span>&nbsp;<?=$html_TypeName?>&nbsp;</span><?=$html_studentSelectionBox?><br />
        <?=$Page_title?>&nbsp;
        </h3>
        
       <!-- <?=(base64_encode("SurveyID=$SurveyID&euserid=1740"))?>-->
        <?=$html_tag?>
        
        <!--<ul class="q_tab">
            <li class=""><a href="management.php?key=<?=$originalKey?>"><?=$Lang['IES']['manage_answered_questionaire']?></a></li>
            <li class="current"><a href="#"><?=$Lang['IES']['EmailList']?></a></li>
        </ul>-->
    
    </div><!-- q_header end -->
	<div class="q_content">
    
    <!-- navigation star -->
                  <span class="navigation" style="float:left;"><?=$html_navigation?>
                  </span>
                  <?=$h_statusCount?>
                  <!-- navigation end -->
     <div class="content_top_tool">
     	<?=$html_tool?>
     </div>
    <div class="table_board">
                   <?=$html_table?>
                     <br />
                     <p class="spacer"></p>
                  </div>
    
    
    
    
    <!-- submit btn start -->
    <div class="edit_bottom"> 
    <p class="spacer"></p>

			<?=$html_close_button?>
             <p class="spacer"></p>
    </div>
    <!-- submit btn end -->
                           
	</div> <!-- q_content end -->
	<input type="hidden" name="key" value="<?=$originalKey?>" />
	</form>
</div> <!-- q_wrapper end -->