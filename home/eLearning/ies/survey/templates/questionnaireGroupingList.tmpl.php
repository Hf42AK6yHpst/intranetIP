<?php
/** [Modification Log] Modifying By: thomas
 * *******************************************
 * *******************************************
 */
 
?>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="<?=$intranet_root?>/templates/ies.js"></script>
<script language="JavaScript">
<!--
$(document).ready(function(){
	// Initialise the table

	if (<?=$hiddenUpdateResult?>==1) {
		$("#showSuccess").show().fadeOut(5000);
		$("input[name=updateResult]").val(0);
	}

	$(".ClassDragAndDrop").tableDnD({
		dragHandle: "Dragable",
		onDragClass: "move_selected",
		onDrop:	function(table, row){
								$.ajax({
									type: "POST",
									url: "ajax/ajax_groupingListHandler.php",
									data: "Action=Sequence&key=<?=$key?>&"+$.tableDnD.serialize(),
									success: function(msg){
									}
								});
						}
	});
});



function setSurveyQuestion(qStr)
{
  document.form1.qStr.value = qStr;
}
function removeMapping(obj,ParEncodedSurveyMappingID) {
	if (confirm("<?=$Lang['IES']['DeleteWarning']?>")) {
		$.ajax({
			url:      "ajax/ajax_groupingListHandler.php",
			type:     "POST",
			data:     "Action=removeMapping&SurveyMappingID="+ParEncodedSurveyMappingID,
			async:	  false,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
				 
				 	result = $(xml).find("result").text();
				 	if (Number(result)==1) {
				 		//$(obj).parents("tr:first").remove();
				 		//window.parent.opener.location.reload(true);		// reload the parent page to update the discovery in the index page
						//self.location.reload(true);		// reload the parent page to update the discovery in the index page
						$("input[name=hiddenUpdateResult]").val(1);
						$("form[name=form1]").attr("action","");
						$("form[name=form1]").submit();
				 	} else {
					  	$("input[name=hiddenUpdateResult]").val(0);
					  	$("#showFailed").show().fadeOut(5000);
				 	}
				}
		});
	}
}

function reloadPage()
{
	document.form1.action = 'questionnaireGroupingList.php';
	document.form1.submit();
}
-->
</script>

<form name="form1" method="POST" action="">
<div class="q_header">
<h3><?=$html["survey_title"]?></h3>
<?=$html["tag"]?> 
</div><!-- q_header end -->
<div class="q_content">

    <!-- navigation star -->
		<div class="navigation">
			<table width="100%" style="font-size:15px">
				<tr>
					<td>
						<?=$html_navigation?>
					</td>
					<td>
						<div class="IES_top_tool" style="margin:0">
							<?=$html["print_and_export"]?>
						</div>
					</td>
				</tr>
			</table>
		</div>
	<!-- navigation end -->
	<?=$html_instr?>
	<br/>
	<span id="showSuccess" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordUpdated())?></span>
	<span id="showFailed" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordFail())?></span>
	<div class="content_top_tool">
		<?=$html_tool?>
	</div>
	<br/>
	<!--a href = "">New</a--> <!-- temp-->
	<br/>
	<br/>
<div class="table_board">
<?=$html["listing_table"]?>
                       
                           
	<br>
	<p class="spacer"></p>
</div>
  <!-- submit btn start -->
  <div class="edit_bottom"> 
    <?=$html["close_button"]?>
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->
</div> <!-- q_content end -->

<input type="hidden" name="qStr" />
<input type="hidden" name="key" value="<?=$key?>" />
<input type="hidden" name="hiddenUpdateResult" />
</form>

