<?php
/** [Modification Log] Modifying By: thomas
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();
$key = $_REQUEST["key"];
$libies_survey = new libies_survey();
$decodedURIs = $libies_survey->breakEncodedURI($key);
$TaskID = $decodedURIs["TaskID"];
$IsStage3 = $decodedURIs["IsStage3"];
$SurveyID = $decodedURIs["SurveyID"];
$StudentID = $decodedURIs["StudentID"];

$FromPage = $decodedURIs["FromPage"];
list($FromTask,$FromModule) = explode(":",$FromPage);


$IS_IES_STUDENT = $libies_survey->isIES_Student($UserID);
if($IS_IES_STUDENT){
	$StudentID = $_SESSION['UserID'];	
}

if ($IsStage3) {
	$IsCommentEdit = 1;
} else {
	$IsCommentEdit = 0;
}
//$html["listing_table"] = $libies_survey->getSurveyListingTable($SurveyID, $StudentID, $IS_IES_STUDENT, $IsCommentEdit, $ForSurveyDiscovery);


$surveyDetails = $libies_survey->getSurveyDetails($SurveyID);
$surveyDetails = current($surveyDetails);
$survey_type = $surveyDetails['SurveyType'];
$html["survey_title"] = $libies_survey->getFormattedSurveyTitle($surveyDetails["SurveyType"],$surveyDetails["SurveyTypeName"],$surveyDetails["Title"],$key,$IS_IES_STUDENT);

#############################################
##	Construct Tag
$currentTag = "GroupingList";
$html["tag"] = libies_ui::GET_MANAGEMENT_BROAD_TAB_MENU($currentTag, $IS_IES_STUDENT, $key, $FromPage, $ForDiscoveryList);
#############################################

//$html_instr = "<hr />".$Lang['IES']['Questionnaire_questionGroupingList']['string1']."<hr />";

### Discovery Info
$SurveyMapping = $libies_survey->loadSurveyMapping($SurveyID, $StudentID);
$SurveyMappingID = $SurveyMapping[0]["SURVEYMAPPINGID"];

if ($SurveyMappingID != '')
{
	$title = htmlspecialchars($SurveyMapping[0]["MAPPINGTITLE"]);
	$comment = htmlspecialchars($SurveyMapping[0]["COMMENT"]);
}
else
{
	$title = '';
	$comment = '';
}

$successMessage = str_replace("\n"," ",libies_ui::getMsgRecordUpdated());
$failMessage = str_replace("\n"," ",libies_ui::getMsgRecordFail());

$html["status_display"] = '';
$html["status_display"] .= '<tr>';
	$html["status_display"] .= '<td colspan="3">';
		$html["status_display"] .= '&nbsp;';
		$html["status_display"] .= '<span id="showSuccess" style="display:none; float:right;">'.$successMessage.'</span>';
		$html["status_display"] .= '<span id="showFailed" style="display:none; float:right;">'.$failMessage.'</span>';
	$html["status_display"] .= '</td>';
$html["status_display"] .= '</tr>';

$html["title"] = '';
$html["title"] .= '<tr>';
	$html["title"] .= '<td>'.$Lang['IES']['DiscoveryName'].'</td>';
	$html["title"] .= '<td>:</td>';
	$html["title"] .= '<td><input type="text" class="textbox" id="MappingTitle" name="MappingTitle" value="'.$title.'"></td>';
$html["title"] .= '</tr>';

$html["commentBox"] = <<<HTMLEND
<tr>
	<td>{$Lang['IES']['DiscoveryDetails']}</td>
	<td>:</td>
	<td>
		<textarea name="commentBox" rows="5">{$comment}</textarea>
		<p class="spacer"></p>
		<br/>  
    </td>
</tr>
HTMLEND;
############################################
##	HTML - export button
if ($IS_IES_STUDENT) {
	$html["print_and_export"] = "<a href=\"exportCombinationAnalysis.php?SurveyID=$SurveyID\" class=\"export\" title=\"\">{$Lang['IES']['Export_Discovery']}</a>";
}
############################################
##	HTML - close_button
$html["save_button"] = '<input name="submit2" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'"  value="'.$Lang['IES']['Save'].'" onClick="saveComment()"/>';
$html["close_button"] = libies_ui::getCloseButton();
############################################
$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);	// ivan
$linterface = new interface_html("ies_survey.html");
// navigation
$nav_arr[] = array($Lang['IES']['answered_questionaire_discovery'], "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$linterface->LAYOUT_START();
include_once("templates/questionnaire_discovery_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>