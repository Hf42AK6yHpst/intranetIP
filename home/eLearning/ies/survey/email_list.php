<?
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_opendb();

##Get KEY
####################
$libies_survey = new libies_survey();
$originalKey = $_REQUEST["key"];
$parameters = $libies_survey->breakEncodedURI($originalKey);
$SurveyID = $parameters["SurveyID"];
$StudentID = $parameters["StudentID"];
$TaskID = $parameters["TaskID"];
$FromPage = $parameters["FromPage"];


if(empty($SurveyID)){
	echo "NO SURVEYID";
	exit();
}
################

//######################GET the SURVEY ###########################
$SurveyDetails = $libies_survey->getSurveyDetails($SurveyID);
//DEBUG_R($SurveyDetails);
if(is_array($SurveyDetails) && count($SurveyDetails) == 1){
	$SurveyDetails = current($SurveyDetails);
}
$Survey_title = $SurveyDetails["Title"];
$Survey_desc = $SurveyDetails["Description"]; 
$Survey_question = $SurveyDetails["Question"];
$Survey_type = $SurveyDetails["SurveyType"];
$Survey_InputBy = $SurveyDetails["InputBy"];

$Survey_startdate = date("Y-m-d", strtotime($SurveyDetails["DateStart"]));
$Survey_enddate = date("Y-m-d", strtotime($SurveyDetails["DateEnd"]));
$html_TypeName = $SurveyDetails["SurveyTypeName"];
#########################################


$surveyEmailDetatils = $libies_survey->getSurveyEmailDetails($SurveyID);
//debug_r($surveyEmailDetatils);

$IS_IES_STUDENT = $libies_survey->isIES_Student($UserID);


if(!$IS_IES_STUDENT){
	$html_studentSelectionBox = $libies_survey->getStudentSurveySelectionBox($originalKey,$Survey_type);
}
//DEBUG_R($surveyEmailDetatils);
$html_table = "<table class=\"common_table_list\">";
	$html_table .= "<tr><th class=\"num_check\">#</th><th>{$Lang['IES']['Respondent']}</th><th>{$Lang['IES']['Email']['ReceivedDate']}</th><th width='50px'>{$Lang['IES']['Status']}</th><th width='5px'></th>";
	

$StatusDetails = array(
					$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['NOT_SENT']=>array(
																		"COLORED_TEXT"=>"<font color='red'>".$ies_cfg['DB_IES_SURVEY_EMAIL_Status__Lang'][$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['NOT_SENT']]."</font>",
																		"COUNT"=>0
																		),
					$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['SENT']=>array(
																		"COLORED_TEXT"=>"<font>".$ies_cfg['DB_IES_SURVEY_EMAIL_Status__Lang'][$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['SENT']]."</font>",
																		"COUNT"=>0
																		),
					$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['DONE']=>array(
																		"COLORED_TEXT"=>"<font color='green'>".$ies_cfg['DB_IES_SURVEY_EMAIL_Status__Lang'][$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['DONE']]."</font>",
																		"COUNT"=>0
																		)
					);


if(is_array($surveyEmailDetatils)){
	$count = 1;
	
	foreach($surveyEmailDetatils as $email){
		$html_table .= "<tr>";
		$html_table .= "<td>{$count}</td>";
		$count++;
		if(!empty($email["EMAIL"])){
			$e_string = "&lt;{$email["EMAIL"]}&gt;";
		}
		else{
			$e_string = "";
		}
		$html_table .= "<td>{$email["EmailDisplayName"]}{$e_string}</td>";
		$html_table .= "<td>{$email["DATEINPUT"]}</td>";
		if($email['STATUS'] != $ies_cfg['DB_IES_SURVEY_EMAIL_Status']['DONE']){
			if($email['STATUS'] == $ies_cfg['DB_IES_SURVEY_EMAIL_Status']['NOT_SENT']){
				$html_table .= "<td><font color='red'>".$StatusDetails[$email['STATUS']]["COLORED_TEXT"]."</font></td>";
			}
			else if($email['STATUS'] == $ies_cfg['DB_IES_SURVEY_EMAIL_Status']['SENT']){
				$html_table .= "<td>".$StatusDetails[$email['STATUS']]["COLORED_TEXT"]."</td>";
			}
			$en_id = base64_encode($email["SURVEYEMAILID"]);
			$html_table .= "<td class=\"sub_row\"><div class=\"table_row_tool\">";
			$html_table .= "<a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"Delete\" onclick=\"delete_email(this, '{$en_id}')\"/></div></td>";
		}
		else	// this case is only done i.e.$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['DONE']
		{
			$html_table .= "<td><font color='green'>".$StatusDetails[$email['STATUS']]["COLORED_TEXT"]."</font></td>";
			$html_table .= "<td></td>";
		}
		$html_table .= "</tr>";
		$StatusDetails[$email['STATUS']]["COUNT"]++;
	}
	$html_table .= "</table>";
}

# Construct the count of each status
foreach($StatusDetails as $key => $element) {
	$h_statusCount .= $element["COLORED_TEXT"].$Lang['_symbol']["colon"]." ".$element["COUNT"]."&nbsp;&nbsp;&nbsp;&nbsp;";
}
$h_statusCount = "<span style='float:right;'>".$h_statusCount."</span>";


if(sizeof($surveyEmailDetatils) == 0){
	
	$html_table .= "<tr><td colspan='4'><center>".$Lang['IES']['NoRecordAtThisMoment']."</center></td></tr>";
	
}
$html_table .= "</table>";
############################################
##	Construct Tag
$currentTag = "EmailList";
$html_tag = libies_ui::GET_MANAGEMENT_BROAD_TAB_MENU($currentTag,$IS_IES_STUDENT,$originalKey,$FromPage);
############################################


############################################
##	close button
$html_close_button = libies_ui::getCloseButton();
############################################
$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);	
$linterface = new interface_html("ies_survey.html");
// navigation
$nav_arr[] = array($Lang['IES']['EmailList'], "");

$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$loading_image = $linterface->Get_Ajax_Loading_Image();

$linterface->LAYOUT_START();
$Title = "Testing";
$Page_title = $Survey_title;
include_once("templates/email_list.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>