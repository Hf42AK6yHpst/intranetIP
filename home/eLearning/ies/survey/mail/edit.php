<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

class libies_temp extends libies {

	/**
	 * The function is used by /home/web/eclass40/intranetIP25/home/eLearning/ies/survey/mail/edit.php
	 * to get the html of the gamma mail internal selection box
	 * @param imap_gamma $imap_gamma
	 * @return String - HTML
	 */
	function getGammaMailInternalHtml($imap_gamma) {
		$INTERNAL = "INTERNAL";
		$emailHtmlShell = $this->getEmailHtmlShell();
		$temp_internet_mail_access = $imap_gamma->internet_mail_access;	// for restore
		$imap_gamma->internet_mail_access = false;
		$gammaMailSelectionBoxInternal = $imap_gamma->Get_AddressBook_Select_Toolbar("ToAddress_$INTERNAL","",$INTERNAL);
		$imap_gamma->internet_mail_access = $temp_internet_mail_access;
		$returnString = str_replace(array("{{{ HTML_EMAIL_ADD }}}","{{{ int/ext }}}"), array($gammaMailSelectionBoxInternal,$INTERNAL), $emailHtmlShell);
		return $returnString;
	}
	/**
	 * The function is used by /home/web/eclass40/intranetIP25/home/eLearning/ies/survey/mail/edit.php
	 * to get the html of the iMail internal selection box
	 * @return String - HTML
	 */
	function getIMailInternalHtml() {
		global $i_frontpage_campusmail_choose,$image_path,$LAYOUT_SKIN,$PATH_WRT_ROOT,$i_frontpage_campusmail_remove;
		$INTERNAL = "INTERNAL";
		$emailHtmlShell = $this->getEmailHtmlShell();
		$htmlInternalEmailAdd = <<<HTMLEND
			<br/>
			<textarea id="ToAddress_$INTERNAL" name="ToAddress_$INTERNAL" style="display:none"></textarea>
			<div id="internalToTextDiv">
				<select name="Recipient[]" size="5" style="width: 50%" multiple>
				</select>
			</div>
			<br/>
			<div id="internalToRemoveBtnDiv">
				<a href="javascript:newWindow('{$PATH_WRT_ROOT}/home/imail/choose/index.php?fieldname=Recipient[]',9)" class='iMailsubject' >
				<img src="{$image_path}/{$LAYOUT_SKIN}/iMail/btn_address_book.gif" alt="{$i_frontpage_campusmail_choose}" width="20" height="20" border="0" align="absmiddle" />
				{$i_frontpage_campusmail_choose}
				</a>
				<a href="javascript:removeInHiddenToAddress(document.form1.elements['Recipient[]']);checkOptionRemove(document.form1.elements['Recipient[]']);" class='iMailsubject' >
				<img src="{$image_path}/{$LAYOUT_SKIN}/iMail/btn_delete_selected.gif" alt="{$i_frontpage_campusmail_remove}" width="20" height="20" border="0" align="absmiddle" />
				$i_frontpage_campusmail_remove
				</a>
			</div>
HTMLEND;
		$returnString = str_replace(array("{{{ HTML_EMAIL_ADD }}}","{{{ int/ext }}}"), array($htmlInternalEmailAdd,$INTERNAL), $emailHtmlShell);
		return $returnString;
	}
	/**
	 * The function is used by /home/web/eclass40/intranetIP25/home/eLearning/ies/survey/mail/edit.php
	 * to get the html of the external selection box
	 * @return String - HTML
	 */
	function getExternalHtml() {
		$EXTERNAL = "EXTERNAL";
		$emailHtmlShell = $this->getEmailHtmlShell();
		$htmlExternalEmailAdd = <<<HTMLEND
			<br/>
			<textarea id="ToAddress_$EXTERNAL" name="ToAddress_$EXTERNAL"></textarea>
HTMLEND;
		$returnString = str_replace(array("{{{ HTML_EMAIL_ADD }}}","{{{ int/ext }}}"), array($htmlExternalEmailAdd,$EXTERNAL), $emailHtmlShell);
		return $returnString;
	}
	/**
	 * The function is used by /home/web/eclass40/intranetIP25/home/eLearning/ies/survey/mail/edit.php
	 * to get the html of the email selection box
	 * @return String - HTML
	 */
	function getEmailHtmlShell() {
		$emailHtmlShell = <<<HTMLEND
		<tr>
			<td colspan="2" style=" width:300px;">
				<a href="#" id="ChooseSent_{{{ int/ext }}}" ch="1">選擇未發送</a> | <a href="#" id="ChooseAll_{{{ int/ext }}}" ch="1">選擇全部</a>
		
				<div id="EmailCheckBox_{{{ int/ext }}}" class="selectAnsList" style="height:160px ; width:300px; overflow:scroll; overflow-x:scroll; background-color:#C0C0C0;">
				</div>
				<span style="font-size:0.85em; margin-top:5px" id="MailCountBox_{{{ int/ext }}}"></span>
			</td>
			<td  style="text-align:center;padding-top:100px;width:10%">
				<input class="formbutton" type="button" value="<<" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" id="AddEmail_{{{ int/ext }}}"/>	
			</td>
			<td style="width:44%">
				在此加入電郵
				{{{ HTML_EMAIL_ADD }}}		
			</td>
		</tr>
HTMLEND;
		return $emailHtmlShell;
	}
}






$INTERNAL = "INTERNAL";
$EXTERNAL = "EXTERNAL";


$originalKey = $_REQUEST["key"];
$decodedKey = base64_decode($_REQUEST["key"]);


$parameters = array();
foreach(explode("&",$decodedKey) as $pKey => $parameter) {
	$paraNameValuePair = explode("=",$parameter);
	$paraName = $paraNameValuePair[0];
	$parVar = $paraNameValuePair[1];
	$parameters[$paraName] = $parVar;
}
// Get the SurveyID From key
$SurveyID = $parameters["SurveyID"];
$task_id = $parameters["task_id"];

////DEBUG($task_id);
//echo base64_encode(base64_decode("U3VydmV5SUQ9MTAwJlJlc3BvbmRlbnQ9MTkzNA==")."&respondentNature=2");
if(empty($SurveyID)){
	exit();
}

$msg = $_REQUEST["msg"];
$e_msg = $_REQUEST["e_msg"];


$IsGammaMail = isGammaMail();
$IsIMail = isIMail();

$libies = new libies_temp();
$surveyEmailDetatils = $libies->getSurveyEmailDetails($SurveyID);
##Get title and content of the email
$suveryEmailContentArray = $libies->getSurveyEmailContent($SurveyID);
//DEBUG_R($suveryEmailContentArray);

if (!empty($surveyEmailDetatils)) {	# edit record
	
	####
	##Gen a JS array for email list
	#####
	$EmailList_jsArray = "";
	foreach($surveyEmailDetatils as $value){
		switch($value['RESPONDENTNATURE']) {
			default:
			case $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']:
				$listType = $INTERNAL;
				if ($IsGammaMail) {
					$emailAddress = $value['IMAPUSEREMAIL'];
				} else {
					$emailAddress = "U".$value['USERID'];
				}
				break;
			case $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']:
				$listType = $EXTERNAL;
				$emailAddress = $value['EMAIL'];
				break;
		}
		
		if (!empty($value['USERID'])) {
			/* This is the approach to get Class title and class number from Year Class 
			$DisplayName = "&quot;".$value['ENGLISHNAME']." (".$value['CLASSTITLE'.strtoupper($intranet_session_language)]."-{$value['CLASSNUMBER']})&quot;";
			*/
//			$DisplayName = "&quot;".$value['ENGLISHNAME']." (".$value['CLASSNAME']."-{$value['CLASSNUMBER']})&quot;";
			$DisplayName = "&quot;".$value['USERNAME']."&quot;";
		} else {
			$DisplayName = $value['EmailDisplayName'];
		}
		$EmailList_jsArray .= "EmailList_{$listType}['{$emailAddress}'] = [];";
		$EmailList_jsArray .= "EmailList_{$listType}['{$emailAddress}']['content'] = '{$DisplayName} &lt;{$emailAddress}&gt;' ;";
		$EmailList_jsArray .= "EmailList_{$listType}['{$emailAddress}']['answer'] = ".(isset($value['ANSWERID'])?1:0).";";
		if($value['STATUS']){
			$EmailList_jsArray .= "EmailList_{$listType}['{$emailAddress}']['status'] = 1 ;";
		}
		else
			$EmailList_jsArray .= "EmailList_{$listType}['{$emailAddress}']['status'] = 0 ;";
		
	}
	$EmailList_jsArray .= "UpdateList('$INTERNAL');UpdateList('$EXTERNAL');";
	###############################################
	##	HTML - title
	$html["Title"] = htmlspecialchars($suveryEmailContentArray["Title"]);
	###############################################
		
	$content = $suveryEmailContentArray["Content"];	
	
	###############################################
	##	HTML - content
	$html["DateModified"] = $suveryEmailContentArray["DateModified"];
	###############################################
	
	//$dateModified = $surveyEmailDetatils["DATEMODIFIED"];
	
	
	###############################################
	##	HTML - export
	//$html["export"] = "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"javascript: goExport();\">$button_export</a>";
	###############################################
} else if (!empty($SurveyID)) {	# a new record
	// do nothing
}



###############################################
##	HTML - content
# Components size
$msg_box_width = "100%";
$msg_box_height = 200;
$obj_name = "content";
$editor_width = $msg_box_width;
$editor_height = $msg_box_height;

$questionnaireStr = "<a target=\"_blank\" href=\"http://".$_SERVER["HTTP_HOST"]."/home/eLearning/ies/survey/questionnaire.php?key={{{ Q_KEY }}}\">The Questionnaire</a>";

if (empty($content)) {
	$content .= $questionnaireStr;
}
$init_html_content = $content;

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');

$objHtmlEditor = new FCKeditor($obj_name, $msg_box_width, $editor_height);
$objHtmlEditor->Value = $init_html_content;
ob_start();
$objHtmlEditor->Create();
$html["Content"] = ob_get_contents();
ob_end_clean();
###############################################




###############################################
##	HTML - content
$html["DateModified"] = empty($html["DateModified"])?"--":$html["DateModified"];
###############################################



###############################################
##	HTML - select_emails
$linterface = new interface_html();	// this is necessary for the use of Get_AddressBook_Select_Toolbar()
$imap_gamma = new imap_gamma();
$libwebmail = new libwebmail();
$libcampusmail = new libcampusmail();
if ($libwebmail->has_webmail && $libwebmail->hasWebmailAccess($UserID) && $libwebmail->type == 3)
{
    $noWebmail = false;
}
else
{
    $noWebmail = true;
}
$IsCampusMailInternal = !$libcampusmail->usage_internal_disabled;
$IsCampusMailExternal = !($noWebmail || $libcampusmail->usage_external_disabled);
$IsGammaMailExternal = $imap_gamma->AccessInternetMail();

# if gamma mail || iMail internal 
if ( $IsGammaMail || $IsCampusMailInternal) {
	# if gamma mail => gamma mail layout
	if ( $IsGammaMail ) {
		$html["select_emails_internal"] = $libies->getGammaMailInternalHtml($imap_gamma);
	# else if iMail internal => iMail layout
	} else if ( $IsCampusMailInternal ) {
		$html["select_emails_internal"] = $libies->getIMailInternalHtml();
	}
# if gamma mail external || iMail external
} 
if ( ($IsGammaMail && $IsGammaMailExternal) || (!$IsGammaMail && $IsCampusMailExternal)) {
	# original layout
	$html["select_emails_external"] = $libies->getExternalHtml();
# else no rights
} 
if ( !$IsGammaMail  && ( !$IsCampusMailInternal && $IsCampusMailExternal ) ) {
	# prompt no rights
	$html["cant_access_mail"] = "你沒有寄電郵的權限";
}
# set form type
if ($IsGammaMail) {
	$html["mail_type"] = "GammaMail";
	$html["form_name_id"] = "NewMailForm";	
} else if ($IsCampusMailInternal||$IsCampusMailExternal) {
	$html["mail_type"] = "iMail";
	$html["form_name_id"] = "form1";
} else {
	// do nothing
}


# Please ask fai to see how to do this checking
function isGammaMail() {
	global $plugin,$special_feature;
	return $plugin['imail_gamma'] 
			&& !empty($_SESSION['SSV_EMAIL_LOGIN']) 
			&& !empty($_SESSION['SSV_LOGIN_EMAIL']) 
			&& (
				$special_feature['forCharles'] 
				|| $special_feature['imail']!==true 
				|| $plugin['imail_gamma']
				);
}

function isIMail() {
	global $plugin,$access2webmail,$access2campusmail;
	return ( 
				($_SESSION["SSV_PRIVILEGE"]["special_feature"]['imail'])  
				|| ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"]) 
				|| ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"]) 
				|| ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"]) 
			)
			&& $plugin['imail_gamma']!==true;
}
###############################################







if (!empty($msg)) {	# updated Message
	$updateMessage = $libies->Get_WARNING_MSG($msg);
	
}
if (!empty($e_msg)) {
	$updateMessage .= $libies->Get_WARNING_MSG($e_msg);
	
}

include_once("templates/edit.tmpl.php");

intranet_closedb();
?>