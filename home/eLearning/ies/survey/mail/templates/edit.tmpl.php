<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
?>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="/lang/script.en.js"></script>
<script language="JavaScript" src="/templates/2007script.js"></script>
<script language="JavaScript" src="/templates/script.js"></script>
<script type="text/javascript">
<!--
var EmailList_INTERNAL = [];
var EmailList_EXTERNAL = [];


$(document).ready(function(){
    $("input[name=IS_SEND_EMAIL]").val(0);
    <?=$EmailList_jsArray?>
    $("#AddEmail_INTERNAL").click(function(){
    	var Type = "INTERNAL";
		bindAddEmail(Type);
    });
    $("#AddEmail_EXTERNAL").click(function(){
    	var Type = "EXTERNAL";
		bindAddEmail(Type);
    });
    
    bindChooseAll("INTERNAL");
    bindChooseAll("EXTERNAL");
    bindChooseSent("INTERNAL");
    bindChooseSent("EXTERNAL");
});

/* object debug */
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}

function goImport() {
    var $targetForm = $("form[name=<?=$html["form_name_id"]?>]");
    $targetForm.attr("action","import.php");
    $targetForm.submit();   
}
function goExport() {
    var $targetForm = $("form[name=<?=$html["form_name_id"]?>]");
    $targetForm.attr("action","export.php");
    $targetForm.submit();   
}
function goExport() {
    var $targetForm = $("form[name=<?=$html["form_name_id"]?>]");
    $targetForm.attr("action","export.php");
    $targetForm.submit();
}
function goSave() {
	SelectAllInternalEmails();
    var $targetForm = $("form[name=<?=$html["form_name_id"]?>]");
    $targetForm.attr("action","edit_update.php");
    $targetForm.submit();
}
function goSendEmail() {
	SelectAllInternalEmails();
    var $targetForm = $("form[name=<?=$html["form_name_id"]?>]");
    $("input[name=IS_SEND_EMAIL]").val(1);
    $targetForm.attr("action","edit_update.php");
    $targetForm.submit();
}
function SelectAllInternalEmails() {
	$("select[name=Recipient\\[\\]] option").attr("selected","selected");
}
function CheckEmailFormat(Parmail,Type){
	EmailListArray = eval('EmailList_'+Type);
	if(!Parmail.match(/@/)){
		alert(Parmail+" is not a Valid Email Address");
		return false;
	}
	else if(EmailListArray[Parmail] != undefined){
		if(EmailListArray[Parmail]['status'] != 2){
			alert(Parmail +" is already added");
			return false;
		}
		else{ 
			return true;
		}
	}
	else
		return true;
}
function CheckUserIDFormat() {
	
}
function htmlspecialchars(str) {
	if (typeof(str) == "string") {
		str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
		str = str.replace(/"/g, "&quot;");
		str = str.replace(/'/g, "&#039;");
		str = str.replace(/</g, "&lt;");
		str = str.replace(/>/g, "&gt;");
	}
	return str;
}
function RemoveFromList(Parmail,Type){
	EmailListArray = eval('EmailList_'+Type);
	EmailListArray[Parmail]['status'] = 2;
	UpdateList(Type);
}

function UpdateList(Type){
	EmailListArray = eval('EmailList_'+Type);
	var appendtext = "<ol>";
	var total = 0;
	var sent = 0;
	for(var key in EmailListArray){
		if(typeof(EmailListArray[key]) == 'function'){
			
		}
		else{
			if(EmailListArray[key]['status'] < 2){
				total ++;
			    appendtext += "<li><nobr>";
			    
			    if(EmailListArray[key]['status'] == 1){
			    	sent++;
					appendtext += "<input class=\"mailcheck\" name='Email["+Type+"][]' value='"+ EmailListArray[key]['content'] + "' type=\"checkbox\" id='"+ key + "'/>";
					appendtext += "<span class=\"sent\">(已發送)</span>";
				}
				else{
					appendtext += "<input class=\"mailcheck mail_sent\" name='Email["+Type+"][]' value='"+ EmailListArray[key]['content'] + "' type=\"checkbox\" id='"+ key + "' CHECKED/>";
					
				}
				if ($("#mailType").val().toUpperCase() == "IMAIL" && Type == "INTERNAL") {
					var contentVal = EmailListArray[key]['content'];
					var label = contentVal.substr(0,contentVal.indexOf(" &lt;"));
				} else {
					var label = EmailListArray[key]['content'];
				}
				appendtext += "<label for='"+ key +"' >" + label +"</label>";
				if (EmailListArray[key]['answer'] == null || EmailListArray[key]['answer'] == 0) {
					appendtext += "<a href=\"#\" onclick='RemoveFromList(\""+ key +"\",\"" + Type + "\")' class=\"remove_list\" title=\"刪除\"> X </a>";
				}
				appendtext += "</nobr></li>";
				appendtext += "<INPUT type='hidden' name='MailList["+Type+"][]' Value='"+EmailListArray[key]['content']+"' />";
			}
			else{
				appendtext += "<INPUT type='hidden' name='DeleteMail["+Type+"][]' Value='"+EmailListArray[key]['content']+"' />";
			}
			
		}
	}
	appendtext += "</ol>";
	
	$('#MailCountBox_'+Type).html("已發送的數目 ( "+sent+"/"+total+" )");
	$('#EmailCheckBox_'+Type).html(appendtext);
}

function Linkfill(){
	var field = FCKeditorAPI.GetInstance('content');				
	field.InsertHtml('<?=$questionnaireStr?>');	
}

function bindAddEmail(Type) {

	var mailType = $("#mailType").val().toUpperCase();
	$source = $("#ToAddress_"+Type);
	
	if ( mailType == "IMAIL" && Type == "INTERNAL" ) {
		var strToAddress = "";
		$("select[name=Recipient\\[\\]]").find("option").each(function() {
			var text = $(this).text();
			var val = $(this).val();
			if (jQuery.trim(text+val) != "") {
				strToAddress = strToAddress + '"'+text+'" \<'+val+'\>\;';
			}
		});
		$source.val(strToAddress);
	}
    	var addlist = $source.val();

    	EmailListArray = eval('EmailList_'+Type);
   		if(addlist.length){
   			addlist = addlist.replace(/;/g, ",");
	    	addlist = addlist.split(",");
	    	for(var i = 0; i < addlist.length; i++){
	    		var aEmail = jQuery.trim(addlist[i]);
	    		if (aEmail=="") {continue;}	// skip when the email is empty
	    		
	    		var start = aEmail.indexOf("<");
	    		var aEmailKey;
	    		//email within  < >
	    		if(start != -1){
	    			var sub = aEmail.substr(start+1);
	    			aEmailKey = sub.replace(">", "");
	    			if(aEmail.indexOf(">") == -1){
	    				aEmail += ">";
	    			}	
	    		}
	    		else{
	    			aEmailKey = aEmail;
	    			aEmail = "<" + aEmail + ">";
	    		}
	    		aEmailKey = aEmailKey.replace(" ", "");
	    		
	    		if(mailType == "IMAIL" || CheckEmailFormat(aEmailKey,Type)){
	    		
	    			if(EmailListArray[aEmailKey] == undefined){
	    				EmailListArray[aEmailKey] = [];
	    			}
	    			EmailListArray[aEmailKey]['content'] = htmlspecialchars(aEmail);
	    			EmailListArray[aEmailKey]['status'] = 0;
	    		}
	    		
	    	}
	    	UpdateList(Type);
	    	
	    	
	    	
	    	if ( mailType == "IMAIL" && Type == "INTERNAL" ) {
	    		$("select[name=Recipient\\[\\]]").find("option").remove();
	    	} else if ( mailType == "GAMMAMAIL" && Type == "INTERNAL" ) {
	    		$("#ToAddress_"+Type+"Select").find("option").remove();
	    	}
	    	
	    	
	    	$source.val("");
    	}
    
    
}

function bindChooseAll(Type) {
    $('#ChooseAll_'+Type).click(function(){ 
		$targetAll = $('#ChooseAll_'+Type);  
		$targetSent = $('#ChooseSent_'+Type);
    	if($targetAll.attr("ch") == "1"){
	    	$("input[name=Email\\["+Type+"\\]\\[\\]].mailcheck").each(function(){
				$(this).attr('checked',true);
			});
    		$targetAll.html("取消全部");
    		$targetSent.html("取消未發送");
    		$targetAll.attr("ch", "0");
	    	$targetSent.attr("ch", "0");
			
		}
		else{
			$("input[name=Email\\["+Type+"\\]\\[\\]].mailcheck").each(function(){
				$(this).attr('checked', false);
			});
			$targetAll.html("選擇全部");
			$targetSent.html("選擇未發送");
			$targetAll.attr("ch", "1");
			$targetSent.attr("ch", "1");
		}
    });
}
function bindChooseSent(Type) {

    $('#ChooseSent_'+Type).click(function(){
		$targetSent = $('#ChooseSent_'+Type);
    	if($targetSent.attr("ch") == "1"){
	    	$("input[name=Email\\["+Type+"\\]\\[\\]].mail_sent").each(function(){
				$(this).attr('checked',true);
			});
    		$targetSent.html("取消未發送");
    		$targetSent.attr("ch", "0");
	    	
			
		}
		else{
			$("input[name=Email\\["+Type+"\\]\\[\\]].mail_sent").each(function(){
				$(this).attr('checked', false);
			});
			$targetSent.html("選擇未發送");
			$targetSent.attr("ch", "1");
			
		}
    });
}
function RemoveEmailInSelection(Textfield)
{	
	var SelName = Textfield+"Select";	
	var EmailStr = $("#"+Textfield).val();	
	$("#"+SelName).children(":selected").each(function(){		
		EmailStr = EmailStr.replace($(this).val()+";","");		
		$(this).remove();		
	});	
	$("#"+Textfield).val(EmailStr);	
}
function removeInHiddenToAddress(obj) {
	var $selectedValues = $(obj).find("option:selected");
	$selectedValues.each(function() {
		$("#ToAddress_INTERNAL").text(
			$("#ToAddress_INTERNAL").text().replace($(this).val(),"")
			);
	});
}
-->
</script>
<style>
<!--override content25.css-->

.form_table tr td.field_title{	border-bottom:1px solid #E6E6E6 ; width:30%; }
.form_table col.field_title {width:100px;}
.form_table tr td{	vertical-align:top;	text-align:left; line-height:18px; padding-top:5px; padding-bottom:5px;}
.form_table tr td.field_title{	border-bottom:1px solid #E6E6E6 ; width:30%; }

</style>
<body>
<form name="<?=$html["form_name_id"]?>" id="<?=$html["form_name_id"]?>" action="" method="post">
請把研究對象的電郵地址貼在右方的空白地方，核對後，點撃「<<」加入。有關電郵發送的情況，可參考左方的格子。
要邀請研究對象填寫問卷，讓他們明白問卷調查的目的，請你在「內容」扼要說明，然後點擊「插入問卷連結」，問卷連結便會在「內容」中出現。最後，發送電郵。
若要分批發送，可重覆以上步驟。
<hr />
   <table class="form_table" style="width:100%">
            <col class="field_title" />
			<col class="field_c" />      
			<?=$updateMessage?>
            <tr><td>電郵管理</td><td>:</td><td>
   
             <table border=0 class="form_table" style="width:100%;font-size:15px; padding-top:0; margin-top:-24px; *margin-top:-14px">
        <col class="field_title" />
        <col class="field_c" />
        <col />
        

      <tr>
      	                  <td> </td>
                  
		  
		          <td colspan="2">
					          </td>
        </tr>
	<?=$html["cant_access_mail"]?>
	<?=$html["select_emails_internal"]?>
	<?=$html["select_emails_external"]?>
</table>
            
            
            
            
            </td></tr>
            <tr><td>主題</td><td>:</td><td><input name="title" type="text" id="textfield" value="<?=$html["Title"]?>" style="width:90%"/></td></tr>
            <tr><td>問卷連結</td><td>:</td><td><a href='javascript:Linkfill()' >擊點插入問卷連結</a></td></tr>
            <tr><td>內容</td><td>:</td><td><?=$html["Content"]?></td></tr>
           
            </table>


                <div style="margin:0 auto; text-align:center">
                <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="goSave()" value="儲存" />        
      			<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="goSendEmail()" value="寄出" />
				<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="javascript:self.parent.tb_remove();" value="取消" />
	</div>
	
  <input type="hidden" id="SurveyID" name="SurveyID" value="<?=$SurveyID?>" />
  <input type="hidden" id="SurveyEmailID" name="SurveyEmailID" value="<?=$SurveyEmailID?>" />
  <input type="hidden" name="task_id" value="<?=$task_id?>" />
  <input type="hidden" id="IS_SEND_EMAIL" name="IS_SEND_EMAIL" value="0" />
  <input type="hidden" id="mailType" name="mailType" value="<?=$html["mail_type"]?>"/>
  
</form>
</body>
