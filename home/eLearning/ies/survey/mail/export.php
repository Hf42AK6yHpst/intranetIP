<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();
##############################################
##	Get parameters
$SurveyEmailID = $_POST["SurveyEmailID"];
##############################################


##############################################
##	Retrieve data
if (!empty($SurveyEmailID)) {
	$libies = new libies();
	$surveyEmailDetails = $libies->getSurveyEmailDetails($SurveyEmailID);
	$emailList = $surveyEmailDetails["EMAILLIST"];
	$pureEmailList = $libies->getSurveyEmailListPureEmail($emailList);
}
##############################################


##############################################
##	Construct content
$libexporttext = new libexporttext();
$rows[] = implode($ies_cfg["CSV_Delimiter"],$ies_cfg["SurveyEmailCSVHeader"]);
if (is_array($pureEmailList)) {
	foreach($pureEmailList as $key => $email) {
		$rows[] = $email;
	}
}
$content = implode("\n",$rows);
##############################################


##############################################
##	Export file to browswer
$libexporttext->EXPORT_FILE("SurveyEmailList_".$surveyEmailDetails["TITLE"]."_".date("YmdHis").".csv",$content,false,true);
##############################################
intranet_closedb();
?>