<?
/** [Modification Log] Modifying By: Stanley
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey_email.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$originalKey = $_REQUEST["key"];
$decodedKey = base64_decode($_REQUEST["key"]);

$parameters = array();
foreach(explode("&",$decodedKey) as $pKey => $parameter) {
	$paraNameValuePair = explode("=",$parameter);
	$paraName = $paraNameValuePair[0];
	$parVar = $paraNameValuePair[1];
	$parameters[$paraName] = $parVar;
}

// Get the SurveyID From key
$SurveyID = $parameters["SurveyID"];
$task_id = $parameters["task_id"];

if(empty($SurveyID)){
	exit();
}
$libies = new libies_survey_email();
$GAMMA_MAIL_SETTINGS = $libies->GAMMA_MAIL_SETTINGS;
$IMAIL_SETTINGS = $libies->IMAIL_SETTINGS;

$msg = $_REQUEST["msg"];
$e_msg = $_REQUEST["e_msg"];

$IsGammaMail = $libies->isGammaMail();


$surveyEmailDetatils = $libies->getSurveyEmailDetails($SurveyID);
##Get title and content of the email
$suveryEmailContentArray = $libies->getSurveyEmailContent($SurveyID);

###############################################
##	HTML - title
$html["Title"] = htmlspecialchars($suveryEmailContentArray["Title"]);
###############################################
	
$content = $suveryEmailContentArray["Content"];	

###############################################
##	HTML - content
$html["DateModified"] = (!empty($suveryEmailContentArray["DateModified"]))? $suveryEmailContentArray["DateModified"]:"--";
###############################################
	

###############################################
##	HTML - content
# Components size
$msg_box_width = "100%";
$msg_box_height = 200;
$obj_name = "content";
$editor_width = $msg_box_width;
$editor_height = $msg_box_height;

$questionnaireStr = "&nbsp;&nbsp; <a target=\"_blank\" href=\"http://".$_SERVER["HTTP_HOST"]."/home/eLearning/ies/survey/questionnaire.php?key={{{ Q_KEY }}}\">{$Lang['IES']['ClickToAnswerQuestionnaire']}</a>&nbsp;&nbsp; ";

if (empty($content)) {
//	$content .= " ".$questionnaireStr." ";
	$content .= $questionnaireStr;
}
$init_html_content = $content;
#
##
###############################################

###############################################
##	HTML - select_emails
$linterface = new interface_html();	// this is necessary for the use of Get_AddressBook_Select_Toolbar()
$imap_gamma = new imap_gamma();
//debug_r(auth_sendmail());
# if gamma mail || iMail internal 

if ( !$libies->canSendMail ) {
	# prompt no rights
	$html["cant_access_mail"] = $Lang['IES']['NoRightToSendEmail'];
	
} else {
	if ( $GAMMA_MAIL_SETTINGS["ACCESS_RIGHT"] || $IMAIL_SETTINGS["INTERNAL"]) {
		# if gamma mail => gamma mail layout
		if ( $GAMMA_MAIL_SETTINGS["ACCESS_RIGHT"] ) {
			$html["select_emails_internal"] = $libies->getGammaMailInternalHtml($imap_gamma);
		# else if iMail internal => iMail layout
		} else if ( $IMAIL_SETTINGS["ACCESS_RIGHT"] && $IMAIL_SETTINGS["INTERNAL"] ) {
			$html["select_emails_internal"] = $libies->getIMailInternalHtml();
		}
	} 
	# if gamma mail external || iMail external
	
	if (!$GAMMA_MAIL_SETTINGS["ACCESS_RIGHT"] && $IMAIL_SETTINGS["EXTERNAL"]) {
		# original layout
		$html["select_emails_external"] = $libies->getExternalHtml();
	
	} 
	$h_sendEmailButton = '<input type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" onClick="goSendEmail()" value="'.$Lang['IES']['Send'].'" />';
}
# set form type
if ($GAMMA_MAIL_SETTINGS["ACCESS_RIGHT"]) {
	$html["mail_type"] = "GammaMail";
	$html["form_name_id"] = "NewMailForm";	
} else if ($IMAIL_SETTINGS["INTERNAL"]||$IMAIL_SETTINGS["EXTERNAL"]) {
	$html["mail_type"] = "iMail";
	$html["form_name_id"] = "form1";
} else {
	// do nothing
}



include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');

$objHtmlEditor = new FCKeditor($obj_name, $msg_box_width, $editor_height);
$objHtmlEditor->Value = $init_html_content;
ob_start();
$objHtmlEditor->Create();
$html["Content"] = ob_get_contents();
ob_end_clean();
###############################################






# updated Message
if (!empty($msg)) {	
	$updateMessage = $libies->Get_WARNING_MSG($msg);
	
}
if (!empty($e_msg)) {
	$updateMessage .= $libies->Get_WARNING_MSG($e_msg);	
}
##
include_once("templates/compose_email.tmpl.php");

intranet_closedb();
?>