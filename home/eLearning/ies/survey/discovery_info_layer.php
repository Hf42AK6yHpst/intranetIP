<?php
/** [Modification Log] Modifying By: ivan
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

$libies_survey = new libies_survey();
$SurveyMappingID = $_REQUEST["SurveyMappingID"];

if ($SurveyMappingID != '')
{
	$SurveyMapping = $libies_survey->loadSurveyMapping(null,null,$SurveyMappingID);
	$currentSurveyMapping = current($SurveyMapping);
	$surveyID = $currentSurveyMapping["SURVEYID"];
	$title = htmlspecialchars($currentSurveyMapping["MAPPINGTITLE"]);
	$comment = htmlspecialchars($currentSurveyMapping["COMMENT"]);
}
else
{
	$surveyID = $_REQUEST["SurveyID"];
	$title = '';
	$comment = '';
}



$successMessage = str_replace("\n"," ",libies_ui::getMsgRecordUpdated());
$failMessage = str_replace("\n"," ",libies_ui::getMsgRecordFail());

$html["title"] = '';
$html["title"] .= '<tr>';
	$html["title"] .= '<td>'.$Lang['IES']['DiscoveryName'].'</td>';
	$html["title"] .= '<td>:</td>';
	$html["title"] .= '<td><input type="text" class="textbox" id="MappingTitle" name="MappingTitle" value="'.$title.'"></td>';
$html["title"] .= '</tr>';

$html["commentBox"] = <<<HTMLEND
<tr>
	<td>{$Lang['IES']['DiscoveryDetails']}</td>
	<td colspan="2">&nbsp;
	    <span id="showSuccess" style="display:none; float:right;">{$successMessage}</span>
		<span id="showFailed" style="display:none; float:right;">{$failMessage}</span>
	</td>
</tr>
<tr>
	<td colspan="3" style="text-align:center">
	<textarea name="commentBox" rows="5">{$comment}</textarea>
	<p class="spacer"></p> <br/>  
    <input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"  value=" {$Lang['IES']['Save']} " onClick="saveComment()"/>
    <input name="SurveyMappingID" value="{$SurveyMappingID}" type="hidden" />
	    </td>
</tr>
HTMLEND;


include_once("templates/discovery_info_layer.tmpl.php");
intranet_closedb();
?>