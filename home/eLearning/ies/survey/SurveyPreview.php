<?
//EDITING BY: STANLEY
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
intranet_opendb();



$SurveyID = base64_decode($_REQUEST['SurveyID']);
if(empty($SurveyID)){
	echo "NO SurveyID";
	exit();
}
$libies = new libies();
$key = "SurveyID={$SurveyID}";
$encode_key = base64_encode($key);

$HTML_Respondent = "<INPUT type='text' class='textbox' name='Respondent'/>";
############################################
##	HTML - close_button
$html["close_button"] = libies_ui::getCloseButton();
############################################

##############################################
# get the Survey Details
$SurveyDetails = $libies->getSurveyDetails($SurveyID);

if(is_array($SurveyDetails) && count($SurveyDetails) == 1){
	$SurveyDetails = current($SurveyDetails);

	$SurveyStartDate = $SurveyDetails["DateStart"];
	$SurveyEndDate = $SurveyDetails["DateEnd"];
	$Survey_title = $SurveyDetails["Title"];
	$Survey_desc = $SurveyDetails["Description"]; 
	$Survey_question = $SurveyDetails["Question"];
	$Survey_Type = $SurveyDetails["SurveyType"];	
	$html_TypeName = $SurveyDetails["SurveyTypeName"];
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
$li = new libdb();
$Title = $Lang['IES']['FillInTheQuestionnaire'];

$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($Survey_Type);

$linterface = new interface_html("ies_survey.html");
$linterface->LAYOUT_START();
include_once("templates/SurveyPreview.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>