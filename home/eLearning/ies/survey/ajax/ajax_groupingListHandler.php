<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$Action = $_POST["Action"];
$SurveyMappingID = base64_decode($_POST["SurveyMappingID"]);
$SurveyMappingIDInSeq = $_POST["SurveyMappingIDInSeq"];


$libies_survey = new libies_survey();
$decodedURIs = $libies_survey->breakEncodedURI($key);
$StudentID = $decodedURIs["StudentID"];
$SurveyID = $decodedURIs["SurveyID"];
switch(strtoupper($Action)) {
	case "REMOVEMAPPING":
		$result = $libies_survey->removeSurveyMapping(array($SurveyMappingID));
		break;
	case "SEQUENCE":
		$result = $libies_survey->updateMappingSequence($SurveyID,$StudentID,$SurveyMappingIDInSeq);
		break;
	default:
		break;
}

# Output the modified date to the page
header("Content-Type:   text/xml");
$ISCDATA = true;
$XML = $libies_survey->generateXML(
					array(
						array("result", ($result?1:0))
					),$ISCDATA
				);
echo $XML;

intranet_closedb();
?>