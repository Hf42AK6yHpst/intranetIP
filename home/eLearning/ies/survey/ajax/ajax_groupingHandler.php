<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$libies_survey = new libies_survey();
$key = $_REQUEST["key"];
$decodedURIs = $libies_survey->breakEncodedURI($key);
$SurveyID = $decodedURIs["SurveyID"];
$TaskID = $decodedURIs["TaskID"];
$StudentID = $decodedURIs["StudentID"];
$Action = $_POST["Action"];
$CombineTableNumber = $_POST["CombineTableNumber"];
$XAxis = $_POST["XAxis"];
//error_log(" first --> XAxis -->".$XAxis."\n", 3, "/tmp/aaa.txt");

$YAxis = $_POST["YAxis"];
$questionX = $_POST['question1'];
$questionY = $_POST['question2'];
$DisplayCreateNewQuestionUI = $_POST['DisplayCreateNewQuestionUI'];
$DisplayCreateNewQuestionUI = ($DisplayCreateNewQuestionUI == '')? 1 : 0;
//error_log("Action ==> ".$Action."\n", 3, "/tmp/aaa.txt");


//error_log(" question1 ==> ".$question1." question2 ==> ".$question2."\n", 3, "/tmp/aaa.txt");

switch(strtoupper($Action)) {
	case "LOADSURVEYINFO":
		$result = $libies_survey->GetIndividualAnswerDisplay($SurveyID,$XAxis,true, $StudentID,$DisplayCreateNewQuestionUI);
		break;
	case "LOADCOMBINEBOX":
		$isNewTable=true;
		$combineNameUse = $libies_survey->getCombineNameUse($isNewTable,$CombineTableNumber);
		$combineBoxContent = $libies_survey->getCombineBoxContent($ParSurveyID,null,null,$combineNameUse);
		$result = $libies_survey->generateCombineBox($SurveyID,$combineNameUse,$combineBoxContent,$XAxis);
		break;
	case "LOADCOMBINEBOXCONTENT":
		$isNewTable=true;
		$combineNameUse = $libies_survey->getCombineNameUse($isNewTable,$CombineTableNumber);
		$result = $libies_survey->getCombineBoxContent($SurveyID,$XAxis,$YAxis,$combineNameUse);
		break;
	case "LOADMAPPINGTABLES":
		$result = $libies_survey->generateMappingTables($SurveyID,$StudentID,$XAxis);
		break;
	case "SHOWCOMBINEQUESTIONANALYSIS";
		$combineBoxContent = $libies_survey->getCombineBoxContent($SurveyID,$questionX,$questionY,'');

		//the dummy is hardcode to display the x , y axis title , may be temporary
		$displayMode = 2;
		$analysisHTMLTitleName = "combinationInfo";
		$result = $libies_survey->generateCombineBox($SurveyID,$analysisHTMLTitleName,$combineBoxContent,$XAxis,'dummy',"","","","",$displayMode);
		break;
	case "SAVEMAPPINGS":
		$XAxis = $_POST["survey_info_select"];
		$combinationInfo = $_POST["combinationInfo"];
		$checkCurrent = $_POST["checkCurrent"];
		if (!empty($checkCurrent)) {
			$_tmpData["Title"] = $_POST["currentTitle"];
			$_tmpData["QuestionNum"] = "";
			$_tmpData["XTitle"] = "";
			$_tmpData["YTitle"] = "";


			if ($checkCurrent == -1) {
				$currentStatus = "new";
				if (is_array($combinationInfo[$currentStatus])) {
					// do nothing
				} else {
					$combinationInfo[$currentStatus] = array();
				}
				array_push($combinationInfo[$currentStatus],$_tmpData);
			} else {
				$currentStatus = "old";
				$combinationInfo[$currentStatus][$checkCurrent] = $_tmpData;
			}
		}


		$resultArray = $libies_survey->saveMappings($XAxis,$combinationInfo,$SurveyID,$StudentID);
		if (in_array(false,$resultArray)) {
			$result = 0;
		} else {
			$result = 1;
//			$libies_survey->UPDATE_TASK_CONTENT($TaskID,"","survey:analysis",$StudentID);
		}
		break;
	case "SAVEMAPPINGS2":
		$XAxis = $_POST["survey_info_select"];
		$combinationInfo['QuestionNum'] = $_POST["survey_info_select2"];

		$saveArray = array();
		$saveArray['new'] = array();
		array_push($saveArray['new'],$combinationInfo);

		$deleteMode = 0; // switch off the delete mode in f:saveMappings
		//since saveMappings accept ARRAY as data type , so create $saveArray that map the data type 
		$resultArray = $libies_survey->saveMappings($XAxis,$saveArray,$SurveyID,$StudentID,$deleteMode);
		if (in_array(false,$resultArray)) {
			$result = 0;
		} else {
			$result = 1;
		}
	break;
	case "SAVECOMMENT":
		$SurveyMappingID = $_POST["SurveyMappingID"];
		$comment = $_POST["commentBox"];
		$result = $libies_survey->updateSurveyComment($SurveyMappingID,$comment);
		break;
	case "SAVESURVEY":
		$SurveyMappingID = $_POST["SurveyMappingID"];
		$data = array();
		$data['yTitle'] = $_POST['yTitle'];
		$data['xTitle'] = $_POST['xTitle'];
		$data['title']  = $_POST['title'];
		$result = $libies_survey->updateSurvey($SurveyMappingID,$data);
		break;
	case "SAVESURVEYDISCOVERY":
		$SurveyID = $_POST["SurveyID"];
		$SurveyMappingID = $_POST["SurveyMappingID"];
		$title = $libies_survey->Get_Safe_Sql_Query(trim(urldecode(stripslashes($_POST["MappingTitle"]))));
		$comment = $libies_survey->Get_Safe_Sql_Query(trim(urldecode(stripslashes($_POST["commentBox"]))));
		
		if ($SurveyMappingID == '')
			$result = $libies_survey->newSurveyMapping($SurveyID, $_SESSION['UserID'], $ies_cfg['DB_IES_SURVEY_MAPPING_XMAPPING']['Survey_Comment'], $ParYAxis='', $ParXTitle='', $ParYTitle='', $title, $comment);
		else
			$result = $libies_survey->updateSurveyMapping($SurveyMappingID, $ies_cfg['DB_IES_SURVEY_MAPPING_XMAPPING']['Survey_Comment'], $ParYAxis='', $ParXTitle='', $ParYTitle='', $title, $comment);
			
		break;

	case "SAVEINDIVIDUALQUESTION":

		$deleteMappingID = array(); // store the surveyMapping ID that need to delete
		$resultArray = array(); // store the update / delete result
		$resultUpdateArray  = array();
		$resultDeleteArray = array();
		foreach($FocusQuestionSurveyMappingID as $questionNo => $surveyMappingID){
			$currentStatus = '';			
			$combinationInfo = array();

			$XAxis = $questionNo;

			//HANDLE NEW / UPDATE MAPPING
			if(empty($FocusQuestionSelected[$questionNo])){
				//do nothing , user has not check this question
			}else{
				//this question is checked in the UI

				$_tmpData["Title"] = $FocusQuestionTitle[$questionNo];
				$_tmpData["QuestionNum"] = "";
				$_tmpData["XTitle"] = "";
				$_tmpData["YTitle"] = "";
				
				if(is_numeric($surveyMappingID)) {
					$currentStatus = 'old';
					$combinationInfo[$currentStatus][$surveyMappingID] = $_tmpData;
				}else{
					$currentStatus = 'new';
					$combinationInfo[$currentStatus] = array();
					array_push($combinationInfo[$currentStatus],$_tmpData);			
				}
				$deleteMode = 0;  // switch off the delete mode in f:saveMappings
				$resultUpdateArray = $libies_survey->saveMappings($XAxis,$combinationInfo,$SurveyID,$StudentID,$deleteMode);

			}		

			//HANDLE DELETE MAPPING
			if(!empty($surveyMappingID) && empty($FocusQuestionSelected[$questionNo])){
				//if the DB surveyMapping id has value [!empty($surveyMappingID)], 
				//and user has not check this question [empty($FocusQuestionSelected[$questionNo])]
				//--> user want to remove / delete this mapping
				$deleteMappingID[] = $surveyMappingID;
			}
		}

		if(count($deleteMappingID) > 0){			
			$resultDeleteArray[] = $libies_survey->removeSurveyMapping($deleteMappingID);
			
		}

		if (in_array(false,$resultArray)) {
				$result = 0;
		} else {
				$result = 1;
		}

		break;
	default:
		break;
}

# Output the modified date to the page
header("Content-Type:   text/xml");
$ISCDATA = true;
$XML = $libies_survey->generateXML(
					array(
						array("result", $result)
					),$ISCDATA
				);
echo $XML;

intranet_closedb();
?>