<?php
/** [Modification Log] Modifying By:
 * *******************************************
 * 2011-01-11	Thomas	(201101111357)
 * -	Add $libies_survey->set_display_survey_mapping_option(false)
 *		before calling $libies_survey->GetIndividualAnswerDisplay($SurveyID, $i);
 *		to hide survey mapping options
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
$key = $_REQUEST["key"];

$hiddenUpdateResult = empty($hiddenUpdateResult)?0:1;

$libies_survey = new libies_survey();
$decodedURIs = $libies_survey->breakEncodedURI($key);
$SurveyID = $decodedURIs["SurveyID"];
$FromPage = $decodedURIs["FromPage"];
$StudentID = $decodedURIs["StudentID"];
$IS_IES_STUDENT = $libies_survey->isIES_Student($UserID);	// Check the current user is student or not
//$html["tag"] = $libies_survey->getQuestionnaireTags(1,$IS_IES_STUDENT);

##################################################
##	HTML - tag
$currentTag = "Overall";
$html["tag"] = libies_ui::GET_MANAGEMENT_BROAD_TAB_MENU($currentTag,$IS_IES_STUDENT,$key,$FromPage);
##################################################

############################################
##	HTML - close_button
$html["close_button"] = libies_ui::getCloseButton();
############################################

if(empty($SurveyID)){
	echo "NO SurveyID";
	exit();
}
$question = $libies_survey->getSurveyQuestion($SurveyID);
$questionsArray = $libies_survey->breakQuestionsString($question);
//DEBUG_R($question);
$libies_survey->set_display_survey_mapping_option(false); // Do not display the survey mapping option
for($i = 1; $i <= sizeof($questionsArray) ; $i++){
	$displayCreateNewQuestionOption = 2;
	$HTML_TABLE .= $libies_survey->GetIndividualAnswerDisplay($SurveyID, $i,'',$StudentID,$displayCreateNewQuestionOption);
}
if (empty($HTML_TABLE)) {
	$HTML_TABLE = $Lang['IES']['NoRecordAtThisMoment'];
}
if ($IS_IES_STUDENT) {
	//<a href=\"#\" class=\"print\" title=\"\">列印</a>
	$html["print_and_export"] = "<a href=\"export2excel.php?SurveyID=$SurveyID\" class=\"export\" title=\"\">{$Lang['IES']['Export_Result']}</a>";
}
                
$surveyDetails = $libies_survey->getSurveyDetails($SurveyID);
if(is_array($surveyDetails) && count($surveyDetails) == 1){
	$surveyDetails = current($surveyDetails);
}
$html["survey_title"] = $libies_survey->getFormattedSurveyTitle($surveyDetails["SurveyType"],$surveyDetails["SurveyTypeName"],$surveyDetails["Title"],$key,$IS_IES_STUDENT);
$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);	
$linterface = new interface_html("ies_survey.html");
$nav_arr[] = array($Lang['IES']['Overall'], "");

$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$linterface->LAYOUT_START();
include_once("templates/questionnaireDisplay.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>