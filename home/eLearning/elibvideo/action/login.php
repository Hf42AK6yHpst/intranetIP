<?php
/*
 * 	Modifying: 
 * 
 * 	Log
 * 
 * 	2016-01-06 [Cameron]
 * 		- replace session_unregister to session_unregister_intranet for supporting php5.4+ 
 * 
 */
header('Content-Type: text/html; charset=utf-8');
intranet_auth();
intranet_opendb();

function encrypt($str){
	$Encrypt1 = base64_encode($str);
	$MidPos = ceil(strlen($Encrypt1)/2);
	$Encrypta = substr($Encrypt1, 0, $MidPos);
	$Encryptb = substr($Encrypt1, $MidPos);

	return base64_encode($Encryptb.$Encrypta."=".(strlen($Encrypt1)-$MidPos));	
}

function decrypt($str){
	$Encrypt1 = base64_decode($str);
	$SplitSizePos = strrpos($Encrypt1, "=");
	$MidPos = (int) substr($Encrypt1, $SplitSizePos+1);
	$Encrypt1 = substr($Encrypt1, 0, $SplitSizePos);
	$Encrypta = substr($Encrypt1, 0, $MidPos);
	$Decryptb = substr($Encrypt1, $MidPos);

	return base64_decode($Decryptb.$Encrypta);
}

$loginArr["SysLang"] = isset($_REQUEST['SysLang']) && $_REQUEST['SysLang']!=''? decrypt($_REQUEST['SysLang']) : "en";
$loginArr["SchoolCode"] = isset($_REQUEST['SchoolCode']) && $_REQUEST['SchoolCode']!=''? decrypt($_REQUEST['SchoolCode']) : 0;
$loginArr["eClassUserType"] = isset($_REQUEST['eClassUserType']) && $_REQUEST['eClassUserType']!=''? decrypt($_REQUEST['eClassUserType']) : 0;
$loginArr["eClassUserID"] = isset($_REQUEST['eClassUserID']) && $_REQUEST['eClassUserID']!=''? decrypt($_REQUEST['eClassUserID']) : 0;
$loginArr["UserNameChi"] = isset($_REQUEST['UserNameChi']) && $_REQUEST['UserNameChi']!=''? decrypt($_REQUEST['UserNameChi']) : 0;
$loginArr["UserNameEng"] = isset($_REQUEST['UserNameEng']) && $_REQUEST['UserNameEng']!=''? decrypt($_REQUEST['UserNameEng']) : 0;
$loginArr["UserNickname"] = isset($_REQUEST['UserNickname']) && $_REQUEST['UserNickname']!=''? decrypt($_REQUEST['UserNickname']) : 0;
$loginArr["ClassLevel"] = isset($_REQUEST['ClassLevel']) && $_REQUEST['ClassLevel']!=''? decrypt($_REQUEST['ClassLevel']) : 0;
$loginArr["ClassName"] = isset($_REQUEST['ClassName']) && $_REQUEST['ClassName']!=''? decrypt($_REQUEST['ClassName']) : 0;
$loginArr["ClassNumber"] = isset($_REQUEST['ClassNumber']) && $_REQUEST['ClassNumber']!=''? decrypt($_REQUEST['ClassNumber']) : 0;
$loginArr["TimeStamp"] = isset($_REQUEST['TimeStamp']) && $_REQUEST['TimeStamp']!=''? decrypt($_REQUEST['TimeStamp']) : 0;
$loginArr["SchoolAddress"] = isset($_REQUEST['SchoolAddress']) && $_REQUEST['SchoolAddress']!=''? decrypt($_REQUEST['SchoolAddress']) : 0;
$loginArr["UserEmail"] = isset($_REQUEST['UserEmail']) && $_REQUEST['UserEmail']!=''? decrypt($_REQUEST['UserEmail']) : 0;

///////////////////////////////////////////////
//debug_r(array($SchoolUserID,$SchoolCode,$UserID)); die();
//$SchoolUserID = $loginArr["eClassUserID"];
//$SchoolCode = $loginArr["SchoolCode"];
//$UserID = '';

session_unregister_intranet("ELIB_VIDEO_Lang");
$_SESSION["ELIB_VIDEO_Lang"] = $loginArr["SysLang"];	

$libelibvideo = new libelibvideo($UserID,$loginArr["SchoolCode"], $UserID, true, $loginArr);

?>

<?php
	intranet_closedb();
?>