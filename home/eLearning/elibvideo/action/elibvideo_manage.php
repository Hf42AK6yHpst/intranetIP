<?php
// editing by Siuwan

//$PATH_PAS = $PATH_WRT_ROOT.'src/itextbook/poemsandsongs/';
//
//include_once($PATH_WRT_ROOT.'system/settings/global.php');
//include_once($cfg_eclass_lib_path.'/lib-poemsandsongs.php');
//include_once($PATH_WRT_ROOT.'src/itextbook/poemsandsongs/config/poemsandsongs-config.php');

intranet_opendb();
session_start();

$task = $_REQUEST['task'];

$orderByPublishedTime = $_REQUEST['orderByPublishedTime'];
$orderByViewCount 	  = $_REQUEST['orderByViewCount'];
$itemCount 			  = $_REQUEST['itemCount'];
$pageNumber    		  = $_REQUEST['pageNumber'];
$videoID    		  = $_REQUEST['videoID'];
$viewMode    		  = $_REQUEST['viewMode'];

$category    		  = $_REQUEST['category'];
$locality    		  = $_REQUEST['locality'];
$keyword    		  = $_REQUEST['keyword'];

$filter    		  	  = $_REQUEST['filter'];

$file    		  	  = $_REQUEST['file'];
//$isAddViewCount		  = $_REQUEST['isAddViewCount'];

$Language = isset($_SESSION["ELIB_VIDEO_Lang"]) && $_SESSION["ELIB_VIDEO_Lang"]!=''? $_SESSION["ELIB_VIDEO_Lang"] : "en";	
include_once($intranet_root.'/lang/elibvideo.lang.'.$Language.'.php');	

//$UserID = $_SESSION["ELIB_VIDEO_UserID"];
//$SchoolCode = $_SESSION["ELIB_VIDEO_SchoolCode"];
//$SchoolUserID = $_SESSION["ELIB_VIDEO_SchoolUserID"];

$itemTotalNumber = 0;

$libelibvideo = new libelibvideo($UserID,$_SESSION["ELIB_VIDEO_SchoolCode"], $UserID,"","");


switch($task)
{			
	case "retrieve_video_category_frontpage_html":
		echo $libelibvideo->retrieve_video_category_frontpage_html($category,$orderByPublishedTime,$orderByViewCount,$itemCount, $pageNumber,$viewMode,$Lang );
	break;
	
	case "retrieve_video_category_list_html":
		echo $libelibvideo->retrieve_video_category_list_html($category,$orderByPublishedTime,$orderByViewCount,$itemCount, $pageNumber,$viewMode,$itemTotalNumber,$Lang);
	break;
	
	case "retrieve_category_list_html":
		echo $libelibvideo->retrieve_category_list_html($Lang);
	break;
	
	case "retrieve_video_locality_frontpage_html":
		echo $libelibvideo->retrieve_video_locality_frontpage_html($locality,$orderByPublishedTime,$orderByViewCount,$itemCount, $pageNumber,$viewMode,$Lang);
	break;
	
	case "retrieve_video_locality_list_html":
		echo $libelibvideo->retrieve_video_locality_list_html($locality,$orderByPublishedTime,$orderByViewCount,$itemCount, $pageNumber,$viewMode,$itemTotalNumber,$Lang);
	break;
	
	case "retrieve_locality_list_html":
		echo $libelibvideo->retrieve_locality_list_html($Lang);
	break;
	
	/////////////////////////////////////////////////////////////////////////////////////
	///////////////////////// Front Page END
	/////////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////////////////
	///////////////////////// Video Page
	/////////////////////////////////////////////////////////////////////////////////////
	
	case "retrieve_videopage_html":		
		echo $libelibvideo->retrieve_videopage_html($category, $locality, $keyword, $orderByPublishedTime, $orderByViewCount, $videoID, $filter,$Lang);
	break;
	
	case "add_favourite":
		echo $libelibvideo->insert_favourite($videoID, $UserID);
	break;
	
	case "delete_favourite":
		echo $libelibvideo->delete_favourite($videoID, $UserID);
	break;
	
	case "retrieve_videopage_link":		
		echo $libelibvideo->retrieve_videopage_link($videoID);
	break;
	
	/////////////////////////////////////////////////////////////////////////////////////
	///////////////////////// Video Page END
	/////////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////////////////
	///////////////////////// Favourite Page
	/////////////////////////////////////////////////////////////////////////////////////	
	
	case "retrieve_favouritepage_html":
		echo $libelibvideo->retrieve_favouritepage_html($itemCount,$pageNumber,$Lang);
	break;
	
	case "retrieve_favourite_list_html":
		echo $libelibvideo->retrieve_favourite_list_html($itemCount,$pageNumber, $itemTotalNumber,$Lang);
	break;
	
	case "delete_particular_favourite_video":
		$libelibvideo->delete_particular_favourite_video($videoID);
		echo $libelibvideo->retrieve_favouritepage_html($itemCount,$pageNumber,$Lang);
	break;
	
	case "delete_all_favourite_video":
		$libelibvideo->delete_all_favourite_video();
		echo $libelibvideo->retrieve_favouritepage_html($itemCount,$pageNumber,$Lang);
	break;
	
	/////////////////////////////////////////////////////////////////////////////////////
	///////////////////////// Favourite Page END
	/////////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////////////////
	///////////////////////// Search Page
	/////////////////////////////////////////////////////////////////////////////////////	
	
	case "retrieve_searchpage_html":
		echo $libelibvideo->retrieve_searchpage_html($Lang);
	break;
	
	case "retrieve_searchresultpage_html":
		echo $libelibvideo->retrieve_searchresultpage_html($category,$locality,$keyword,$orderByPublishedTime,$orderByViewCount,$itemCount, $pageNumber, $Lang);		
	break;
	
	case "retrieve_searchresultlist_html":
		echo $libelibvideo->retrieve_searchresultlist_html($category,$locality,$keyword,$orderByPublishedTime,$orderByViewCount,$itemCount, $pageNumber, $itemTotalNumber, $Lang);
	break;
}

intranet_closedb();


?>

