<?php
intranet_opendb();

$Filter = isset($_REQUEST['Filter']) && $_REQUEST['Filter']!=''? $_REQUEST['Filter'] : "category";

//$UserID = $_SESSION["ELIB_VIDEO_UserID"];
//$SchoolCode = $_SESSION["ELIB_VIDEO_SchoolCode"];
//$SchoolUserID = $_SESSION["ELIB_VIDEO_SchoolUserID"];

$VideoID = isset($_REQUEST['videoID']) && $_REQUEST['videoID']!=''? $_REQUEST['videoID'] : "";
$Filter = isset($_REQUEST['filter']) && $_REQUEST['filter']!=''? $_REQUEST['filter'] : "category";
$Category = isset($_REQUEST['category']) && $_REQUEST['category']!=''? $_REQUEST['category'] : "";
$Locality = isset($_REQUEST['locality']) && $_REQUEST['locality']!=''? $_REQUEST['locality'] : "";
$OrderByPublishedTime = isset($_REQUEST['orderByPublishedTime']) && $_REQUEST['orderByPublishedTime']!=''? $_REQUEST['orderByPublishedTime'] : 0;
$OrderByViewCount = isset($_REQUEST['orderByViewCount']) && $_REQUEST['orderByViewCount']!=''? $_REQUEST['orderByViewCount'] : 0;
$KeyWord = isset($_REQUEST['keyword']) && $_REQUEST['keyword']!=''? $_REQUEST['keyword'] : "";

$Tag = isset($_REQUEST['tag']) && $_REQUEST['tag']!=''? $_REQUEST['tag'] : "";

$Message = isset($_REQUEST['message']) && $_REQUEST['message']!=''? $_REQUEST['message'] : "";

$libelibvideo = new libelibvideo($UserID,$_SESSION["ELIB_VIDEO_SchoolCode"], $UserID);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/content.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="includes/js/jquery-ui.js"></script>
<script type="text/javascript" src="includes/js/elibvideo_manage.js"></script>
<title>:: eClass :: Videos :: APF Channel</title>
<script type="text/javascript">
<!--
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
<script type="text/javascript">
$(document).ready(function() {	
	elibvideo_manage.on_init_uploadpage('retrieve_searchpage_html', 
	'<?php echo "index.php?Mode=action&Task=elibvideo_manage" ?>');				
});
</script>
</head>

<body>

        <div id="container">
            <!---->        
            <div class="title_header">
                <div class="title_header_logo">
                    <a  href="index.php"  class="apf_logo"></a>
                </div>
                 <div class="search_bar">
                      <input id="keyword_field_s" type="text"  placeholder="" onKeyDown="javascript:(function(){if (event.keyCode ==13) {$('#searchpage_keyword').attr('value', $('#keyword_field_s').val());document.searchpage_form.submit();} })()"  />
                      <a id="video_searchpage" href="javascript:void(0);"  >Advanced Search</a>
                  </div>
            </div>
            
            <!---->
            <div class="main_board">
            	<div class="main_board_top"><div class="main_board_top_right"><div class="main_board_top_bg">
                    <div class="board_tab">
                        <ul>
                        	<li><a id="video_category_videopage" href="javascript:void(0);" class="tab_cat"><span><em></em>Categories</span></a></li>
                            <li><a id="video_locality_videopage" href="javascript:void(0);" class="tab_local"><span><em></em>Locality</span></a></li>
                           	<li><a id="video_favouritepage" href="javascript: void(0)" class="tab_bookmark"><span><em></em>My Favourite</span></a></li>
                        </ul>
                    </div>
           		 </div></div></div>
                 <div class="main_board_content"><div class="main_board_content_right"><div class="main_board_content_bg">
                 	 <!---->
                      <p class="spacer"></p>
                 	
                    	
                    	<!---->
                        <div class="main_content">
	                    	<div class="navigator">
	                        	<span class="nav_search">Upload CSV - <?php echo $Message;?></span>
	                        </div><br /><br />
							<div class="table_board">
	                             <p class="spacer"></p>
	                             <table id="search_table" class="form_table">
	                             	<form name="update_form" action="index.php"  enctype="multipart/form-data" method="post">
		                             	<input type="hidden" name="Mode" id="Mode" value="action"/>
		                             	<input type="hidden" name="Task" id="Task" value="elibvideo_update"/>
		                                <input  type="file" name="file" id="file"/>
	                                 </form>
	                            </table>	                            
	         				</div> <p class="spacer"></p>
                             <div class="edit_bottom">				            
					              <input name="submit3" type="button" class="formbutton" id="btn_upload" value="Upload" />
					              <input name="submit2" type="button" class="formsubbutton" id="btn_search_reset" value="Reset" />				             
				              </div>
                        </div>
                        
                        
                        <form method="POST" action="" name="favouritepage_form" id="favouritepage_form">
		                 	<input name="Mode"  value="content" type="hidden"/>
		                 	<input name="Task"  value="favouritepage" type="hidden"/>
		                 </form>
		                 <form method="POST" action="" name="frontpage_form" id="frontpage_form">
							<input name="Mode"  value="content" type="hidden"/>
							<input name="Task"  value="frontpage" type="hidden"/>
							<input name="Filter"  value="" type="hidden" id="frontpage_filter" />
						</form>
						<form method="POST" action="" name="searchpage_form" id="searchpage_form">
		                 	<input name="Mode"  value="content" type="hidden"/>
		                 	<input name="Task"  value="searchpage" type="hidden"/>
		                 	<input name="tag" 		id="searchpage_tag" value="" type="hidden"/>
	                 		<input name="keyword" 	id="searchpage_keyword" value="" type="hidden"/>
		                 </form>
                        <p class="spacer"></p>
                        <!---->
                    
                 
                 </div></div></div>
            </div>
            <!---->
            <p class="spacer"></p>
            <div class="footer">Powered by <a href="#" title="eClass"></a></div>
            <!---->
         </div>
         
</body>
</html>

<?php
	intranet_closedb();
?>
