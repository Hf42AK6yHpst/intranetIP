<?php
intranet_opendb();

$Filter = isset($_REQUEST['Filter']) && $_REQUEST['Filter']!=''? $_REQUEST['Filter'] : "category";

//$UserID = $_SESSION["ELIB_VIDEO_UserID"];
//$SchoolCode = $_SESSION["ELIB_VIDEO_SchoolCode"];
//$SchoolUserID = $_SESSION["ELIB_VIDEO_SchoolUserID"];

$Language = isset($_SESSION["ELIB_VIDEO_Lang"]) && $_SESSION["ELIB_VIDEO_Lang"]!=''? $_SESSION["ELIB_VIDEO_Lang"] : "en";

$VideoID = isset($_REQUEST['videoID']) && $_REQUEST['videoID']!=''? $_REQUEST['videoID'] : "";
$Filter = isset($_REQUEST['filter']) && $_REQUEST['filter']!=''? $_REQUEST['filter'] : "category";
$Category = isset($_REQUEST['category']) && $_REQUEST['category']!=''? $_REQUEST['category'] : "";
$Locality = isset($_REQUEST['locality']) && $_REQUEST['locality']!=''? $_REQUEST['locality'] : "";
$OrderByPublishedTime = isset($_REQUEST['orderByPublishedTime']) && $_REQUEST['orderByPublishedTime']!=''? $_REQUEST['orderByPublishedTime'] : 0;
$OrderByViewCount = isset($_REQUEST['orderByViewCount']) && $_REQUEST['orderByViewCount']!=''? $_REQUEST['orderByViewCount'] : 0;
$KeyWord = isset($_REQUEST['keyword']) && $_REQUEST['keyword']!=''? $_REQUEST['keyword'] : "";

if(is_array($Category))$Category = implode (",", $Category);
if(is_array($Locality))$Locality = implode (",", $Locality);
if(is_array($KeyWord))$KeyWord = implode (",", $KeyWord);

$libelibvideo = new libelibvideo($UserID,$_SESSION["ELIB_VIDEO_SchoolCode"], $UserID);

// Expires in the past
header("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
// Always modified
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
// HTTP/1.1
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
// HTTP/1.0
header("Pragma: no-cache");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='cache-control' content='no-store'>
<meta http-equiv='expires' content='-1'>
<meta http-equiv='pragma' content='no-cache'>
<script type="text/javascript" src="includes/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="includes/js/jquery-ui.js"></script>
<script type="text/javascript" src="includes/js/elibvideo_manage.js"></script>
<script type="text/javascript" src="includes/js/source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="includes/js/source/jquery.fancybox.css?v=2.1.0" media="screen">
<link href="css/content.css" rel="stylesheet" type="text/css" />
<title>:: eClass :: Videos :: APF Channel</title>

<script type="text/javascript">
$(document).ready(function() {	
	elibvideo_manage.on_init_videopage('retrieve_videopage_html', 
	'<?php echo "index.php?Mode=action&Task=elibvideo_manage" ?>', '<?=$VideoID?>', '<?=$Filter?>',
	'<?=$Category?>', '<?=$Locality?>','<?=$OrderByPublishedTime?>','<?=$OrderByViewCount?>', '<?=addslashes($KeyWord)?>' );
	
	$('.fancybox').fancybox( {
	 'fitToView'    	 : false,	
	 'autoDimensions'    : false,
	 'autoScale'         : false,
	 'autoSize'          : false,
	 'width'             : 800,
	 'height'            : 600,
	 'padding'			 : 0,
	
	 'transitionIn'      : 'elastic',
	 'transitionOut'     : 'elastic',
	 'type'              : 'iframe'    }
	
	);						
});
</script>

</head>
<body>		
        <div id="container">
            <!---->        
            <div class="title_header <?php if($Language == "b5") echo "title_header_logo_chn"; ?>">
            	<div class="terms_conditon"> <a target="_blank" href="terms.htm" class="fancybox fancybox.iframe"><?=$Lang['Common']['TermAndConditions']?></a>                    </div>
                <div class="title_header_logo">
                    <a  href="index.php"  class="apf_logo"></a>                </div>
                 
            </div>            
            <!---->
            <div class="main_board">
            	<div class="main_board_top"><div class="main_board_top_right"><div class="main_board_top_bg">
                    <div class="board_tab">
                        <ul>
                        	<li class="current_tab"><a id="video_category_videopage" href="javascript:void(0);" class="tab_cat"><span><em></em><?=$Lang['Header']['Categories']?></span></a></li>
                            <li><a id="video_locality_videopage" href="javascript:void(0);" class="tab_local"><span><em></em><?=$Lang['Header']['Location']?></span></a></li>
                           	<li><a id="video_favouritepage" href="javascript: void(0)" class="tab_bookmark"><span><em></em><?=$Lang['Header']['MyFavourite']?></span></a></li>
                        </ul>
                        <div class="search_bar"><a id="video_searchpage" href="javascript:void(0);"  ><?=$Lang['Header']['AdvancedSearch']?></a>
		                      <input id="keyword_field_s" type="text"  placeholder="" onKeyDown="javascript:(function(){if (event.keyCode ==13) {$('#searchpage_keyword').attr('value', $('#keyword_field_s').val());document.searchpage_form.submit();} })()"  />
		                      
		                  </div>
                    </div>
           		 </div></div></div>
                 <div class="main_board_content">
                 <div id="main_board_content_right" class="main_board_content_right">
                 <div id="main_board_content_bg" class="main_board_content_bg">
                 	 <!---->
                      <p class="spacer"></p>
                 	<div  id="main_content">
                    	<!---->
                    	
                    	<!---->
                        <div class="main_content">
                        	<!--<div class="navigator">
                            	<span>Cat1</span>
                            </div>-->
                           	<div class="tool_box">
                           		<a id="next_video_btn" href="javascript:void(0);"class="btn_next" ><span><?=$Lang['VideoPage']['Next']?></span></a>
                           		<a id="prev_video_btn" href="javascript:void(0);" class="btn_prev" ><span><?=$Lang['VideoPage']['Previous']?></span></a>
                            </div>
                            <p class="spacer"></p>
                            <div id="video_page_main_content"></div>
                          <!--main content end-->
                        </div>
                        <p class="spacer"></p>
                        <!---->
                    </div>
                 	 <!---->
                  <form method="GET" action="" name="favouritepage_form" id="favouritepage_form">
	                 	<input name="Mode"  value="content" type="hidden"/>
	                 	<input name="Task"  value="favouritepage" type="hidden"/>
	                 </form>
	                 <form method="GET" action="" name="frontpage_form" id="frontpage_form">
						<input name="Mode"  value="content" type="hidden"/>
						<input name="Task"  value="frontpage" type="hidden"/>
						<input name="Filter"  value="" type="hidden" id="frontpage_filter" />
					</form>
					<form method="GET" action="" name="searchpage_form" id="searchpage_form">
	                 	<input name="Mode"  	value="content" type="hidden"/>
	                 	<input name="Task"  	value="searchpage" type="hidden"/>
	                 	<input name="tag" 		id="searchpage_tag" value="" type="hidden"/>
	                 	<input name="keyword" 	id="searchpage_keyword" value="" type="hidden"/>
	                 </form>
                 </div></div></div>
            </div>
            <!---->
            <p class="spacer"></p>
            <div class="footer">Powered by <a href="#" title="eClass"></a></div>
            <!---->
         </div>     
</body>
</html>

<?php
	intranet_closedb();
?>
