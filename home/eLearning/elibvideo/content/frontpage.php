<?php
intranet_opendb();

$Filter = isset($_REQUEST['Filter']) && $_REQUEST['Filter']!=''? $_REQUEST['Filter'] : "category";

//$UserID = $_SESSION["ELIB_VIDEO_UserID"];
//$SchoolCode = $_SESSION["ELIB_VIDEO_SchoolCode"];
//$SchoolUserID = $_SESSION["ELIB_VIDEO_SchoolUserID"];

$Language = isset($_SESSION["ELIB_VIDEO_Lang"]) && $_SESSION["ELIB_VIDEO_Lang"]!=''? $_SESSION["ELIB_VIDEO_Lang"] : "en";

$libelibvideo = new libelibvideo($UserID,$_SESSION["ELIB_VIDEO_SchoolCode"], $UserID);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/content.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="includes/js/jquery-1.8.2.min.js"></script>-->
<script type="text/javascript" src="includes/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="includes/js/jquery-ui.js"></script>
<script type="text/javascript" src="includes/js/elibvideo_manage.js"></script>
<script type="text/javascript" src="includes/js/source/jquery.fancybox.js?v=2.1.0"></script>
<script type="text/javascript" src="includes/js/height_control.js"></script>
<link rel="stylesheet" type="text/css" href="includes/js/source/jquery.fancybox.css?v=2.1.0" media="screen">
<title>:: eClass :: Videos :: APF Channel</title>

<script type="text/javascript">
$(document).ready(function() {	
	elibvideo_manage.on_init_frontpage('retrieve_video_<?echo $Filter;?>_frontpage_html', '<?php echo "index.php?Mode=action&Task=elibvideo_manage" ?>');	
						
	$('.fancybox').fancybox( {
	 'fitToView'    	 : false,	
	 'autoDimensions'    : false,
	 'autoScale'         : false,
	 'autoSize'          : false,
	 'width'             : 800,
	 'height'            : 600,
	 'padding'			 : 0,
	
	 'transitionIn'      : 'elastic',
	 'transitionOut'     : 'elastic',
	 'type'              : 'iframe'    }
	
	);			
	
	navigator.sayswho= (function(){
	    var ua= navigator.userAgent, 
	    N= navigator.appName, tem, 
	    M= ua.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*([\d\.]+)/i) || [];
	    M= M[2]? [M[1], M[2]]:[N, navigator.appVersion, '-?'];
	    if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
	    return M;
	})();
	
	var browser = navigator.sayswho[0].toLowerCase();
	var version = navigator.sayswho[1].split(".");
		
	if(browser == "chrome" || (browser == "MSIE" && version >= 10 )){
		console.log("OK");
	}else{
		alert("<?=$Lang['Message']['BrowserChecking']?>");		
	}
});
</script>
</head>
<body>
        <div id="container">
            <!---->        
            <div class="title_header">
                <div class="title_header_logo <?php if($Language == "b5") echo "title_header_logo_chn"; ?> ">
                    <a href="index.php" class="apf_logo"></a>
                </div>
                <div class="terms_conditon"> 
	                <a target="_blank"  href="terms.htm" class="fancybox fancybox.iframe">
	                <?=$Lang['Common']['TermAndConditions']?></a>  
                </div>                
            </div>
            
            <!---->
            <div class="main_board">
            	<div class="main_board_top"><div class="main_board_top_right"><div class="main_board_top_bg">
                    <div class="board_tab">
                        <ul>
                        	<li class="current_tab"><a id="video_category_frontpage" href="javascript: void(0)" class="tab_cat"><span><em></em><?=$Lang['Header']['Categories']?></span></a></li>
                            <li><a id="video_locality_frontpage" href="javascript: void(0)" class="tab_local"><span><em></em><?=$Lang['Header']['Location']?></span></a></li>
                            <li><a id="video_favouritepage" href="javascript: void(0)" class="tab_bookmark"><span><em></em><?=$Lang['Header']['MyFavourite']?></span></a></li>
                        </ul>
                         <div class="search_bar"><a id="video_searchpage" href="javascript:void(0);"  ><?=$Lang['Header']['AdvancedSearch']?></a>
		                      <input id="keyword_field_s" type="text"  placeholder="" onKeyDown="javascript:(function(){if (event.keyCode ==13) {$('#searchpage_keyword').attr('value', $('#keyword_field_s').val());document.searchpage_form.submit();} })()"  />
		                      
		                  </div>
                    </div>
           		 </div></div></div>
                 <div class="main_board_content"><div class="main_board_content_right"><div class="main_board_content_bg">
                 	 <!---->
                      <p class="spacer"></p>
                 	<div class="cat_board">
                    	<!---->
                    	<div id="cat_list" class="cat_list" style="height: 296px;">
                        	<!-- Fill from libelibvideo.php -->
                        </div>
                    	<!---->
                        <div class="main_content">
                        	<div class="toggle">
                            	<a id="orderByViewCount" href="javascript: void(0)"><?=$Lang['FrontPage']['MostViewed']?></a>
                                <a id="orderByPublishedTime" href="javascript: void(0)" class="selected_toggle"><?=$Lang['FrontPage']['LatestUploaded']?></a>
                            	<span><?=$Lang['FrontPage']['SortBy']?> : </span>
                            </div> 
                            <div class="toggle_view">
				            	<ul>
					                <li><a id="viewList" href="javascript: void(0)" class="view_list" title=""></a></li>
				                    <li class="selected"><a id="viewCover" href="javascript: void(0)" class="view_cover" title=""></a></li>
				                    <li> <span><?=$Lang['FrontPage']['ViewBy']?></span> </li>
				                </ul>
				            </div>
                            <p class="spacer"></p>                       
                            <div id="video_list" class="video_list">                            	
                            
                                <!-- Fill from libelibvideo.php -->                                
                                	
                            </div>
                        </div>
                        <p class="spacer"></p>
                        <!---->
                    </div>
                 	 <!---->
                 <form method="GET" action="" name="favouritepage_form" id="favouritepage_form">
                 	<input name="Mode"  value="content" type="hidden"/>
                 	<input name="Task"  value="favouritepage" type="hidden"/>
                 </form>
                 <form method="GET" action="" name="searchpage_form" id="searchpage_form">
                 	<input name="Mode"  value="content" type="hidden"/>
                 	<input name="Task"  value="searchpage" type="hidden"/>
                 	<input name="tag" 		id="searchpage_tag" value="" type="hidden"/>
	                <input name="keyword" 	id="searchpage_keyword" value="" type="hidden"/>
                 </form>
                 </div></div></div>
            </div>
            <!---->
            <p class="spacer"></p>
            <div class="footer">Powered by <a href="#" title="eClass"></a></div>
            <!---->
         </div>
</body>
</html>

<?php
	intranet_closedb();
?>
