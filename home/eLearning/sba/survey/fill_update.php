<?php
//editing by: Paul
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");


$libies_survey = new libies_survey();
$originalKey = $_REQUEST["key"];
$parameters = $libies_survey->breakEncodedURI($originalKey);
$SurveyID = $parameters["SurveyID"];
$StudentID = $parameters["StudentID"];
$TaskID = $parameters["TaskID"];
$FromPage = $parameters["FromPage"];
$email = strtolower($parameters["email"]);
$RESPONDENTNATURE = (!empty($parameters["respondentNature"]))?$parameters["respondentNature"]:$_REQUEST["respondentNature"];
//DEBUG_R($RESPONDENTNATURE);

if(empty($SurveyID)){
	echo "ERROR";
	exit();
}

if(empty($Respondent)){
	echo "Please input the Respondent!";
	exit();
}

$libies = new libies();

//####Only update the db when the users come from email link. Otherwise, it is input by the student manually.
if($RESPONDENTNATURE == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL'] || $RESPONDENTNATURE == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']){
	################################################
	##	Get input parameters

	$Respondent = $email;
	$realEmail = $libies->getEmailFromEmailDisplay($email);
	
//	$SurveryDetailArry = $libies->getSurveyEmailDetails($SurveyID,$email);
//	$SurveryEmailID = $SurveryDetailArry['SURVEYEMAILID'];
	##	Handling the suvery done by user
	
	# handle survey done
	$libies->updateEmailStatus($SurveyID, $realEmail, $ies_cfg['DB_IES_SURVEY_EMAIL_Status']['DONE'], $RESPONDENTNATURE);

}


##############################################
##Save the answer to database
##!! MUST DO
##
$dm = $ies_cfg["Questionnaire"]["Delimiter"]["Answer"];
$ans_dm = $ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"];
$Answer = "";
$arr_count = 0;    

$q_array = explode("#QUE#", $qstr);
$arr_count = -1;

$qtype = array();
foreach($q_array as $v){	
	if($v != ""){
		$str = explode("||", $v);
		$str = explode(",", $str[0]);
		foreach($str as $value){
			$qtype[$arr_count][] = $value;
		}
		
	}
	$arr_count++;
}
/**
 * $qtype[x][0] = type
 * $qtype[x][1] = option num 
 * $qtype[x][2] = question num
 */ 

//DEBUG_R($qtype);
//********qtype refer to iesConfig.ini.php
/*
 * 
 * Format: [{Question code (to DB)}] = array({Question Type JS Library Name}, {Display Name})
$ies_cfg["Questionnaire"]["QuestionType"]["2"] = array("question_mc_single", "MC (Single)");
$ies_cfg["Questionnaire"]["QuestionType"]["3"] = array("question_mc_multi", "MC (Multiple)");
$ies_cfg["Questionnaire"]["QuestionType"]["4"] = array("question_fillin_short", "Fill-in (Short)");
$ies_cfg["Questionnaire"]["QuestionType"]["5"] = array("question_fillin_long", "Fill-in (Long)");
$ies_cfg["Questionnaire"]["QuestionType"]["6"] = array("question_table_likert", "Likert Scale");
$ies_cfg["Questionnaire"]["QuestionType"]["7"] = array("question_table_input", "Table-like Input");
## End: Questionnaire ##
 */
if(!empty($SurveyID) && !empty($Respondent)){
	for($c = 0; $c < $arr_count; $c++){
		$Answer .= $dm;
		$is_first = true;
		
		if($qtype[$c][0] == $ies_cfg["Questionnaire"]["QuestionType"]["MC"] ){
			if(isset($ans[$c][0][0])){
//DEBUG_R($ans[$c][0][0]);
//				DEBUG("IN");
				$Answer .= $ans[$c][0][0];
			}
			else {
//				DEBUG("INON");
				$Answer .= "";
			}
		}
		else if($qtype[$c][0] == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]){
			for($j = 0; $j < $qtype[$c][1]; $j++){
				if(!$is_first){
					$Answer .= $ans_dm;
				}
				if(!empty($ans[$c][0][$j])){
					$Answer .= $ans[$c][0][$j];
				}
				else{
					$Answer .= "0";
				}
				$is_first = false;
			}
		}
		else if($qtype[$c][0] == $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"] || $qtype[$c][0] == $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]){
			$Answer .= $ans[$c][0][0];
		}
		else if($qtype[$c][0] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]){
			for($k = 0; $k < $qtype[$c][1]; $k++){
				if(!$is_first){
					$Answer .= $ans_dm;
				}
				$Answer .= $ans[$c][$k][0];
				$is_first = false;
				
			}
		}
		
		else if($qtype[$c][0] == $ies_cfg["Questionnaire"]["QuestionType"]["Table"]){
			
			for($k = 0; $k < $qtype[$c][1]; $k++){
				for($j = 0; $j < $qtype[$c][2]; $j++){
					if(!$is_first){
						$Answer .= $ans_dm;
					}
					$Answer .= $ans[$c][$k][$j];
					$is_first = false;
				}
			}
		}	
	}
	
	
	$li = new libdb();

	$Answer = htmlspecialchars($Answer);
	if(empty($InterviewDate)){
		$InterViewDate = 'NOW()';
	}
	else{
		$InterViewDate = "'{$InterviewDate} 00:00:00'";
	}
	## 0 = null
	## 
	if($RESPONDENTNATURE != $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
		$sql = "INSERT INTO IES_SURVEY_ANSWER (SurveyID, Respondent, Answer, DateInput, DateModified, Type, Status, RespondentNature, UserID) VALUES ($SurveyID,'$Respondent','$Answer',$InterViewDate,$InterViewDate, 1, 1, $RESPONDENTNATURE, 0) ON DUPLICATE KEY UPDATE Answer ='$Answer' , DateModified = NOW()" ;
	}
	else{
		$sql = "INSERT INTO IES_SURVEY_ANSWER (SurveyID, UserID, Answer, DateInput, DateModified, Type, Status, RespondentNature, Respondent) VALUES ($SurveyID,'$Respondent','$Answer',$InterViewDate,$InterViewDate, 1, 1, $RESPONDENTNATURE, '0') ON DUPLICATE KEY UPDATE Answer ='$Answer' , DateModified = NOW()" ;
	}

	$result_sql = $li->db_db_query($sql);
}

if($result_sql){
	$msg = "add";
}
else{
	$msg = "add_failed";	
}
//exit(); //reopen
if($AddMore){
	$encode_SurveyID = base64_encode($SurveyID);
	//header("Location: /home/eLearning/ies/survey/questionnaire.php?key=$originalKey&msg=$msg");
	header("Location: /home/eLearning/sba/?mod=survey&task=questionnaire&key=$originalKey&msg=$msg");
	exit();
}
else{
	//header("Location: result.php?key=$originalKey&Respondent=$Respondent&isedit=$isedit&msg=$msg&respondentNature=$RESPONDENTNATURE");
	header("Location: /home/eLearning/sba/?mod=survey&task=result&key=$originalKey&Respondent=$Respondent&isedit=$isedit&msg=$msg&respondentNature=$RESPONDENTNATURE");
	exit();
}
?>