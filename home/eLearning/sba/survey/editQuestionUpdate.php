<?php

//r_sType ==> scheme Type
$r_sType = trim($r_sType);

$li = new libdb();

if(empty($QuestionID))
{
  $sql = "INSERT INTO {$intranet_db}.IES_DEFAULT_SURVEY_QUESTION ";
  $sql .= "(QuestionStr, DateInput, InputBy, ModifyBy,SchemeType) ";
  $sql .= "VALUES ";
  $sql .= "('{$qStr}', NOW(), {$sba_thisUserID}, {$sba_thisUserID},'{$r_sType}')";

  $msg = "add";
}
else
{
  $sql = "UPDATE {$intranet_db}.IES_DEFAULT_SURVEY_QUESTION ";
  $sql .= "SET ";
  $sql .= "QuestionStr = '{$qStr}', ";
  $sql .= "ModifyBy = {$sba_thisUserID} ";
  $sql .= "WHERE QuestionID = {$QuestionID}";
  
  $msg = "update";
}
$li->db_db_query($sql);

//intranet_closedb();
?>

<script language="JavaScript">
//var url = parent.window.location.pathname+"?msg=<?=$msg?>";
var url = '?mod=survey&task=setQuestion'+'&msg=<?=$msg?>';
parent.window.location = url;
parent.tb_remove();
</script>