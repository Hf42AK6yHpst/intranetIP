<?php


/** [Modification Log] Modifying By: fai
 * *******************************************
 * *******************************************
 */
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
//intranet_opendb();

//org ies <input type="button" value="�s�@" onclick="newWindow('/home/eLearning/ies/survey/createForm.php?TaskID=16256&amp;SurveyType=1',10)" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" class="formbutton" name="submit2">
//page --> http://192.168.0.146:31002/home/eLearning/sba/?mod=survey&task=createForm&TaskID=17238&SurveyType=1
////		$thisJsAction = "newWindow('/home/eLearning/sba/?mod=survey&task=createForm&StepID=".$this->getStepID()."&TaskID=".$this->getTaskID()."&scheme_id=".$this->getSchemeID()."&SurveyType=".$SurveyType."',10)";

$objIES = new libies();

// Survey Detail
//$dec_SurveyID = intval(base64_decode($SurveyID));
$dec_SurveyID = base64_decode($SurveyID);

$loadPreviewForTeacher = false;
if($StepID == '' || $TaskID == '' || $scheme_id == ''){
	if(intval($dec_SurveyID) > 0){
		//if with surveyid , preview is false
		$loadPreviewForTeacher = false;
	}else{
		$loadPreviewForTeacher = true;
	}
}


//if(is_numeric($dec_SurveyID) && $dec_SurveyID > 0)
if(is_numeric($dec_SurveyID))
{
  $survey_detail_arr = $objIES->getSurveyDetails($dec_SurveyID);
  
  if(count($survey_detail_arr) == 1 && is_array($survey_detail_arr)){
		$survey_detail_arr = current($survey_detail_arr);
  }

  $survey_title = $survey_detail_arr["Title"];
  $survey_disptitle = $survey_detail_arr["Title"];
  $survey_desc = $survey_detail_arr["Description"]; 
  $survey_question = $survey_detail_arr["Question"];
  $survey_startdate = date("Y-m-d", strtotime($survey_detail_arr["DateStart"]));
  $survey_enddate = date("Y-m-d", strtotime($survey_detail_arr["DateEnd"]));
  $survey_type = $survey_detail_arr["SurveyType"];

  $survey_typename = $survey_detail_arr["SurveyTypeName"];
  $survey_remark1 = $survey_detail_arr["Remark1"];
  $survey_remark2 = $survey_detail_arr["Remark2"];
  $survey_remark3 = $survey_detail_arr["Remark3"];
  $survey_remark4 = $survey_detail_arr["Remark4"];
}
else
{
  $survey_title = "";
  $survey_disptitle = $Lang['IES']['CreateNewQuestionnaire'];

  $survey_type = $SurveyType;
	if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){
		$survey_disptitle = $Lang['IES']['CreateNewObserve'];
	}


  
  foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $surveyTypeArr)
  {
    $_surveyTypeCode = $surveyTypeArr[0];
    $_surveyTypeName = $surveyTypeArr[1];
    
    if($_surveyTypeCode == $SurveyType)
    {
      $survey_typename = $_surveyTypeName;
      break;
    }
  }
}
//debug_r('type = '.$survey_type);
// End date for survey only (not for interview and field observation)
$show_enddate_input = ($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]);

// Post-action message
if(isset($msg))
{
  $html_alertMsg = libies_ui::getMsgRecordUpdated();
}

$questionTabCaption = ($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]) ? $Lang['IES']['QuestionnaireQuestion']: $Lang['IES']['QuestionnaireQuestionType2'];


// Tab Config
$TabMenuArr = array();

//if($objIES->isIES_Teacher($UserID))
if($objIES->isIES_Teacher($sba_thisUserID)){
  $TabMenuArr[] = array("#", $Lang['IES']['BasicSettings']);
}
else
{
//  $TabMenuArr[] = array("createForm.php?SurveyID={$SurveyID}", $Lang['IES']['BasicSettings']);
  $TabMenuArr[] = array("?mod=survey&task=createForm&SurveyID={$SurveyID}", $Lang['IES']['BasicSettings']);
}


if(is_numeric($dec_SurveyID))
{
//  $TabMenuArr[] = array("viewQue.php?key=".base64_encode("SurveyID={$dec_SurveyID}"), $questionTabCaption);

  $TabMenuArr[] = array("?mod=survey&task=viewQue&key=".base64_encode("SurveyID={$dec_SurveyID}"), $questionTabCaption);
}



//if(!$sba_allowStudentSubmitHandin){
	//sba_allowStudentSubmitHandin  == false, mean it is a teacher login , display a preview page for teacher to see "the look" of the survey
	// so the SurveyID is empty
//	$TabMenuArr[] = array("?mod=survey&task=viewQue&key=".base64_encode("SurveyID="), $questionTabCaption);
//}


$html_tab_menu = libies_ui::GET_SURVEY_TAB_MENU($TabMenuArr, 0);

$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);

$linterface = new interface_html("sba_survey.html");
$linterface->LAYOUT_START();

$displayOtherRemarkInputFlag= true;
if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
	$displayOtherRemarkInputFlag = false;
}

$h_questionnaireTitle = $Lang['IES']['QuestionnaireTitle'];
if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]){
	$h_remarkCaption1 = $Lang['IES']['Questionnaire_InterviewRemark1'];
	$h_remarkCaption2 = $Lang['IES']['Questionnaire_InterviewRemark2'];
	$h_remarkCaption3 = $Lang['IES']['Questionnaire_InterviewRemark3'];
	$h_remarkCaption4 = $Lang['IES']['Questionnaire_InterviewRemark4'];
	$h_questionnaireTitle = $Lang['IES']['Questionnaire_InterviewTitle'];
}else if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){
	$h_remarkCaption1 = $Lang['IES']['Questionnaire_ObserveRemark1'];
	$h_remarkCaption2 = $Lang['IES']['Questionnaire_ObserveRemark2'];
	$h_remarkCaption3 = $Lang['IES']['Questionnaire_ObserveRemark3'];
	$h_remarkCaption4 = $Lang['IES']['Questionnaire_ObserveRemark4'];
	$h_questionnaireTitle = $Lang['IES']['Questionnaire_ObserveTitle'];


}
?>
<script>
function submitForm(form){

	checkedIsPassed = 1;

	$(".WarningDiv").each(function(){
		$(this).hide();
	});


	var title = $("#id_surveyTitle").val();
	title = $.trim(title);

	if(title == ''){
		checkedIsPassed = 0;
		$("#id_surveyTitle").focus();
		$('#SurveyNameInputErrorDiv').show();
	}


	if(checkedIsPassed){
		form.submit();
	}
}
</script>
<form name="form1" method="POST" action="index.php">

  <div class="q_header">
    <h3>
      <span>&nbsp;<?=$survey_typename?>&nbsp;</span><br />
      <?=$survey_disptitle?>
    </h3>
    <?=$html_tab_menu?>
  
  </div><!-- q_header end -->
  
  <div class="q_content">
    <?=$html_alertMsg?>
  
    <table class="form_table">
      <col class="field_title" />
      <col  class="field_c" />
      <tr>
        <td><?=$h_questionnaireTitle?><span class="tabletextrequire">*</span></td>
        <td>:</td>
        <td ><input type="text" name="title" id="id_surveyTitle" value="<?=$survey_title?>" class="textbox" />
		<?=$linterface->Get_Form_Warning_Msg('SurveyNameInputErrorDiv', "Please input name", $Class='WarningDiv')?>
		</td>
      </tr>
  
      <tr>
        <td><?=$Lang['IES']['QuestionnaireDescription']?></td>
        <td>:</td>
        <td>
          <textarea name="description" style="width:100%; height:50px"><?=$survey_desc?></textarea>  
        </td>
      </tr>

<?
	if($displayOtherRemarkInputFlag){
?>

    <tr>
        <td><?=$h_remarkCaption1?></td>
        <td>:</td>
        <td>
			<input type="text" name="remark1" value="<?=$survey_remark1?>" class="textbox" />
        </td>
      </tr>

    <tr>
        <td><?=$h_remarkCaption2?></td>
        <td>:</td>
        <td>
			<input type="text" name="remark2" value="<?=$survey_remark2?>" class="textbox" />
        </td>
      </tr>


    <tr>
        <td><?=$h_remarkCaption3?></td>
        <td>:</td>
        <td>
			<input type="text" name="remark3" value="<?=$survey_remark3?>" class="textbox" />
        </td>
      </tr>

    <tr>
        <td><?=$h_remarkCaption4?></td>
        <td>:</td>
        <td>
			<input type="text" name="remark4" value="<?=$survey_remark4?>" class="textbox" />
        </td>
      </tr>
<?
}	//close if($displayOtherRemarkInputFlag)
?>

<?php if($show_enddate_input) { ?>  
      <tr>
        <td><?=$Lang['IES']['QuestionnaireEnddate']?></td>
        <td>:</td>
        <td>
          <span class="row_content">
            <?=$linterface->GET_DATE_PICKER("enddate", $survey_enddate)?>
          </span>
        </td>
      </tr>
<?php } ?>
  
    </table>

    <!-- submit btn start -->
    <div class="edit_bottom"> 
      <p class="spacer"></p>
<?
if($loadPreviewForTeacher){
   //  do nothing
}else{
?>
	<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"  value="<?=$Lang['IES']['Save']?>" onclick="submitForm(this.form)" />
	<input type="reset" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$Lang['IES']['Reset']?> " />
<?}?>
      <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="window.close();" value=" <?=$Lang['IES']['Close']?> " />
      <p class="spacer"></p>
    </div>
    <!-- submit btn end -->
  </div> <!-- q_content end -->
  <input type="hidden" name="mod" value="survey" />
  <input type="hidden" name="task" value="form_update" />
  <input type="hidden" name="SurveyID" value="<?=$SurveyID?>" />
  <input type="hidden" name="SurveyType" value="<?=$SurveyType?>" />
  <input type="hidden" name="TaskID" value="<?=$TaskID?>">
  <input type="hidden" name="StepID" value="<?=$StepID?>">  
  <input type="hidden" name="scheme_id" value="<?=$scheme_id?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
?>