<?php
/** [Modification Log] Modifying By: thomas
 * *******************************************
 * *******************************************
 */

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");

//intranet_auth();
//intranet_opendb();
$key = $_REQUEST["key"];
$libies_survey = new libies_survey();
$decodedURIs = $libies_survey->breakEncodedURI($key);
$TaskID = $decodedURIs["TaskID"];
$IsStage3 = $decodedURIs["IsStage3"];
$SurveyID = $decodedURIs["SurveyID"];
$StudentID = $decodedURIs["StudentID"];
$hiddenUpdateResult = empty($hiddenUpdateResult)?0:1;
$FromPage = $decodedURIs["FromPage"];
list($FromTask,$FromModule) = explode(":",$FromPage);
$FromModuleLower = strtolower($FromModule);
$ForSurveyDiscovery = ($IsStage3 == 1 && ($FromModuleLower == "interview" || $FromModuleLower == "observe"))? 1 : 0;


$IS_IES_STUDENT = $libies_survey->isIES_Student($sba_thisUserID);
/* strange code , don't know the condition, comment on 20110823, wait for delete
if($IS_IES_STUDENT && $ForSurveyDiscovery){
	### New Discovery Button
	$StudentID = $_SESSION['UserID'];
	$html_tool = "<div class=\"Conntent_tool\"><a class=\"IES_file_upload thickbox\" href=\"discovery_info_layer.php?SurveyID=$SurveyID&KeepThis=false&TB_iframe=true&height=480&width=550\" title=\"{$Lang['Btn']['New']}\">{$Lang['IES']['New']} </a></div><br style=\"clear:both\" />";	
}
*/
if($IS_IES_STUDENT){
//	$html_tool = "<div class=\"Conntent_tool\"><a href=\"/home/eLearning/ies/survey/questionnaire.php?key={$originalKey}\" title=\"{$Lang['Btn']['New']}\">{$Lang['IES']['New']} </a></div>";	
	$html_tool = '<div class=\"Conntent_tool\"><a onclick="goNewAnalysis()" href="#">'.$Lang['IES']['New'].'</a></div>';
}

if ($IsStage3) {
	$IsCommentEdit = 1;
} else {
	$IsCommentEdit = 0;
}

$html["listing_table"] = $libies_survey->getSurveyListingTable($SurveyID, $StudentID, $IS_IES_STUDENT, $IsCommentEdit, $ForSurveyDiscovery);

if($SurveyID > 0){
	$surveyDetails = $libies_survey->getSurveyDetails($SurveyID);
	if(is_array($surveyDetails) && count($surveyDetails) == 1){
		$surveyDetails = current($surveyDetails);
	}
}else{
	list($FromTask,$surveyCode) = explode(":",$FromPage);

	$Survey_type = $ies_cfg["Questionnaire"]["SurveyType"][$surveyCode][0];
	$html_TypeName = $ies_cfg["Questionnaire"]["SurveyType"][$surveyCode][1];

	$surveyDetails["Title"] = $Lang['SBA']['Survey']['StudentInputSurveyName'];
	$surveyDetails["SurveyTypeName"] = $html_TypeName;
}
$html["survey_title"] = $libies_survey->getFormattedSurveyTitle($surveyDetails["SurveyType"],$surveyDetails["SurveyTypeName"],$surveyDetails["Title"],$key,$IS_IES_STUDENT);

#############################################
##	Construct Tag
$currentTag = "GroupingList";
$html["tag"] = libies_ui::GET_MANAGEMENT_BROAD_TAB_MENU($currentTag, $IS_IES_STUDENT, $key, $FromPage, $ForDiscoveryList);
#############################################
if (!$ForSurveyDiscovery){
	if($IsCommentEdit){
		$html_instr = "<hr />".$Lang['IES']['Questionnaire_questionGroupingList']['string1_w_CommentEdit']."<hr />";
	}
	else{
		$html_instr = "<hr />".$Lang['IES']['Questionnaire_questionGroupingList']['string1']."<hr />";
	}
}
############################################
##	HTML - export button
if ($IS_IES_STUDENT) {
	$_SESSION['thisExportSurveyID']=$SurveyID;
	$html["print_and_export"] = "<a href=\"index.php?mod=survey&task=exportCombinationAnalysis\" target='_blank' class=\"export\" title=\"\">{$Lang['IES']['Export_Analysis']}</a>";
}
############################################
############################################
##	HTML - close_button
$html["close_button"] = libies_ui::getCloseButton();
############################################
$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);	
$linterface = new interface_html("sba_survey.html");

// navigation
if ($ForSurveyDiscovery){
	$nav_arr[] = array($Lang['IES']['answered_questionaire_discovery_list'], "");
}
else{
	$nav_arr[] = array($Lang['IES']['GroupingList'], "");
}

$html_navigation = $linterface->GET_NAVIGATION($nav_arr);


$linterface->LAYOUT_START();
include_once("templates/survey/questionnaireGroupingList.tmpl.php");
$linterface->LAYOUT_STOP();

?>