<?php
//modifying By : 
/*
if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
	header("Location: /");
	exit;
}
*/
if(!isset($scheme_id) || $scheme_id=="") {
	header("Location: index.php?mod=worksheet&task=index");
	exit;
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPageArr['IES'] = 1;
$CurrentPage = "Worksheet";

$objIES = new libies();
$ldb = new libdb();

# Result message after operation
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

# toolbar menu
$html_toolbar = $linterface->GET_LNK_NEW("index.php?mod=worksheet&task=new&scheme_id={$scheme_id}",$button_new,"","","",0);

# Scheme 
$sql = "SELECT ws.WorksheetID as WorksheetID, ws.Title, LEFT(ws.StartDate,10), LEFT(ws.EndDate,10), COUNT(f.WorksheetFileID), ws.WorksheetID, CONCAT('<a href=\'javascript: void(0);\' id=\'remove_',ws.WorksheetID,'\' class=\'deselect\' title=\'".$Lang['IES']['Delete']."\'onClick=\'javascript: removeWorksheet(this.form,',ws.WorksheetID,');\'>Del&nbsp;<\/a>','| Edit') FROM IES_WORKSHEET ws LEFT OUTER JOIN IES_WORKSHEET_TEACHER_FILE f ON (f.WorksheetID=ws.WorksheetID) LEFT OUTER JOIN IES_WORKSHEET_HANDIN_COMMENT c ON (c.WorksheetID=ws.WorksheetID) WHERE ws.RecordStatus=".$ies_cfg["recordStatus_approved"]." AND ws.SchemeID=$scheme_id GROUP BY ws.WorksheetID ORDER BY ws.Title";

$result = $ldb->returnArray($sql,5);


//HANDLE THE NUMBER OF STUDENT HANDIN PER EACH WORKSHEET, MAY NEED TO TURN THE PERFORMANCE
$noOfHandinResult = array();
$sql = "select count(*) as noOfHandin, a.worksheetid as 'worksheetid' from 
			(select count(*) , studentid , WorksheetID from IES_WORKSHEET_HANDIN_FILE group by StudentID, WorksheetID) as a group by worksheetid
		";
$noOfHandinResult = $ldb->returnArray($sql);


# table content
if(count($result) > 0){
	for($i=0,$i_max = count($result); $i<$i_max; $i++)
	{
		$_worksheetid = $result[$i]['WorksheetID'];

		//FIND the number of student handin of the worksheetid $_worksheetid
		$noOfhandIn = 0;
		for($j = 0,$j_max = count($noOfHandinResult);$j<$j_max;$j++){
			$_compareWorkSheetid = $noOfHandinResult[$j]['worksheetid'];
			if($_worksheetid == $_compareWorkSheetid){
				$noOfhandIn = $noOfHandinResult[$j]['noOfHandin'];
				break;
			}
		}
		$html_worksheet_list .= "<form name='form{$i}' method=\"post\"><tr>";
		$html_worksheet_list .= "<td valign='top'>".($i+1)."</td>";
		$html_worksheet_list .= "<td valign='top'><a href='index.php?mod=worksheet&task=detail&scheme_id={$scheme_id}&sheetID=".$result[$i][0]."'>".$result[$i][1]."</a></td>";
		$html_worksheet_list .= "<td valign='top'>".$result[$i][2]."</td>";
		$html_worksheet_list .= "<td valign='top'>".$result[$i][3]."</td>";
		$html_worksheet_list .= "<td valign='top'>".$noOfhandIn." </td>";
		$html_worksheet_list .= "<td valign='top'>";
		$sql = "SELECT COUNT(distinct f.StudentID), COUNT(distinct c.StudentID) FROM IES_WORKSHEET_HANDIN_FILE f LEFT OUTER JOIN IES_WORKSHEET_HANDIN_COMMENT c ON (f.WorksheetID=c.WorksheetID) WHERE f.WorkSheetID=".$_worksheetid;
		$temp = $objIES->returnArray($sql, 2);

		$html_worksheet_list .= (($temp[0][0]-$temp[0][1])>0) ? ($temp[0][0]-$temp[0][1]) : 0;

		$html_worksheet_list .= "</td>";
		$html_worksheet_list .= "<td valign='top'><div class='table_row_tool'><a href='javascript:goEdit(document.form{$i},".$result[$i][0].")'>&nbsp;</a></div><div class='table_row_tool'><a href='javascript: void(0);' id='remove_".$result[$i][0]."' class='delete' title='".$Lang['IES']['Delete']."'onClick='javascript: removeWorksheet(document.form{$i},".$result[$i][0].");'>&nbsp;</a></div><input type='hidden' name='sheetID' id='sheetID' value='".$result[$i][0]."'></td>";
		$html_worksheet_list .= "</tr></form>";
	}
}else{
	$html_worksheet_list .= '<tr><td colspan="7" align="center">'.$Lang['IES']['NoRecordAtThisMoment'].'</td></tr>';
}


$html_js_script .= "
<script language='javascript'>
function removeWorksheet(obj,id) {
	if(confirm('$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete')) {
		obj.action = 'index.php?mod=worksheet&task=remove_update';
		obj.submit();
	}
}

function goEdit(obj,id) {
	obj.action = 'index.php?mod=worksheet&task=edit';
	obj.submit();
}

</script>
";

$schemeData = $objIES->GET_SCHEME_DETAIL($scheme_id);

### Title ###
$title = $Lang['IES']['Worksheet'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($Lang['IES']['Scheme'], "index.php?mod=worksheet&task=index");
$PAGE_NAVIGATION[] = array($schemeData['Title'], "");

$linterface->LAYOUT_START();
include_once("templates/worksheet/worksheet.tmpl.php");
$linterface->LAYOUT_STOP();

?>
