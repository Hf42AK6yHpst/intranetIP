<?php
//modifying By : 
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
/*
if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
	header("Location: /");
	exit;
}
*/
$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "Worksheet";

$objIES = new libies();
$ldb = new libdb();

$worksheetDetail = $objIES->getWorksheetDetail($sheetID);
$startDate = substr($worksheetDetail['StartDate'],0,10);
$endDate = substr($worksheetDetail['EndDate'],0,10);

# scheme data
$schemeData = $objIES->GET_SCHEME_DETAIL($worksheetDetail['SchemeID']);

$anyStudentSubmission = $objIES->checkStudentSubmitWorksheetHandin($sheetID);

# Scheme Menu
if($anyStudentSubmission) {
	$schemeMenu = $schemeData['Title']."<input type='hidden' name='schemeID' id='schemeID' value='".$worksheetDetail['SchemeID']."'>";
} else {
	$schemeMenu = $objIES->getSchemeSelection($worksheetDetail['SchemeID']);
}

# button
//$html_button .= $linterface->GET_ACTION_BTN($button_reset, "reset")."&nbsp;";
$html_button .= $linterface->GET_ACTION_BTN($button_back, "button", "goCancel()");
$html_button .= "<input type='hidden' name='schemeID' id='schemeID' value='{$scheme_id}'>";
$html_button .= "<input type='hidden' name='sheetID' id='sheetID' value='{$sheetID}'>";
$html_button .= "<input type='hidden' name='ClickID' id='ClickID' value=''>";
//$html_button .= "<input type='hidden' name='task' id='task' value=''>";

# construct the attachment list
$fileList = "";
$fileRecord = $objIES->getWorksheetFileData("IES_WORKSHEET_TEACHER_FILE",$sheetID);
for($i=0; $i<sizeof($fileRecord); $i++) {
	list($file_id, $file_name, $folder_path, $file_hashname, $date_modified) = $fileRecord[$i];
	$fileList .= "<li><a href='/home/eLearning/sba/getfile.php?type={$ies_cfg['HandinFileByTeacher']}&fhn={$file_hashname}'>{$file_name}</a><br /><span class='update_record'>({$date_modified})</span></li>";
}
if($fileList!="") {
	$fileList = "<ul class='attachment_list'>".$fileList."</ul>";
} else {
	$fileList = "---";
}


# table content
$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['WorksheetTitle']."</td><td width='1'> : </td><td width='80%'>".$worksheetDetail['Title']."</td></tr>";
$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['Scheme']."</td><td width='1'> : </td><td width='80%'>".$schemeData['Title']."</td></tr>";
$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['StartEndDate']."</td><td width='1'> : </td><td width='80%'>{$i_From} ".$startDate." {$i_To} ".$endDate."</td></tr>";
$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$eDiscipline["File"]."</td><td width='1'> : </td><td width='80%'>{$fileList}</td></tr>";
$html_worksheet_list .= "<tr><td valign='top' colspan='3' align='right'><div align='right'>".$linterface->GET_BTN($button_edit, "button", "goEdit()")."</div></td></tr>";
$html_worksheet_list .= "<input type='hidden' name='sheetID' id='sheetID' value='{$sheetID}'>";

# construct the student list
$studentRecord = $objIES->getWorksheetStudent($sheetID);

for($i=0; $i<sizeof($studentRecord); $i++) {
	list($user_id, $class, $name, $file_id, $comment,$TeacherName, $commentDateModified) = $studentRecord[$i];
	$fileList = "";
	
	$fileRecord = $objIES->getWorksheetFileData(" IES_WORKSHEET_HANDIN_FILE ",$sheetID, " AND StudentID=$user_id");
	for($j=0; $j<sizeof($fileRecord); $j++) {
		list($file_id, $file_name, $folder_path, $file_hashname, $date_modified) = $fileRecord[$j];
		$fileList .= "<li><a href='/home/eLearning/sba/getfile.php?type={$ies_cfg['HandinFileByStudent']}&fhn={$file_hashname}'>{$file_name}</a><br /><span class='update_record'>({$date_modified})</span></li>";
	}
	if($fileList!="") {
		$fileList = "<ul class='attachment_list'>".$fileList."</ul>";
	} else {
		$fileList = "---";
	}
	
	$html_student_list .= "<form name='form".($i+1)."' id='form".($i+1)."'><tr><td>".($i+1)."</td><td>$class</td><td>$name</td><td>$fileList</td>";
	if(sizeof($fileRecord)>0){
		$h_updateStatus = '';
		if($commentDateModified == ''){
			$h_updateStatus = '';
		}else{
			$h_updateStatus = "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$commentDateModified} {$Lang['IES']['From']} {$TeacherName})</span></div>";
		}
		$html_student_list .= "<td>";
			$html_student_list .= $linterface->GET_TEXTAREA("comment_$user_id", $comment);
			$html_student_list .= "<div id='updateStatue".($i+1)."'>".$h_updateStatus."</div>";
		$html_student_list .= "</td>";
		$html_student_list .= "<td>".$linterface->GET_BTN($button_submit, "button", "show_result('form".($i+1)."','updateStatue".($i+1)."')");
			$html_student_list .= "<input type='hidden' name='studentID' id='studentID' value='".$user_id."'>";
			$html_student_list .= "<input type='hidden' name='sheetID' id='sheetID' value='".$sheetID."'>";
		$html_student_list .= "</td>";
	}else{
		$html_student_list .= "<td>&nbsp;</td><td>&nbsp;</td>";
	}

	$html_student_list .= "</tr></form>";
}



$html_form_tag = " name='form0' method='POST'";


### Title ###
$title = $Lang['IES']['Worksheet'];
$TAGS_OBJ[] = array($title,"");

$PAGE_NAVIGATION[] = array($Lang['IES']['Scheme'], "index.php?mod=worksheet&task=index");
$PAGE_NAVIGATION[] = array($schemeData['Title'], "index.php?mod=worksheet&task=worksheet&scheme_id=".$worksheetDetail['SchemeID']);
$PAGE_NAVIGATION[] = array($worksheetDetail['Title'], "");



$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/worksheet/detail.tmpl.php");
$linterface->LAYOUT_STOP();

?>
