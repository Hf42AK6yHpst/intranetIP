<?php
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$libfilesystem = new libfilesystem();
$finalUploadResult = false;
$resultCreatedFolder = false;



if (!empty($_FILES)) {
	# create folder
	$dbStorePath = "/file/sba/scheme/";
	$targetPath = $intranet_root.$dbStorePath;
	if(!is_dir($targetPath)) {
		$libfilesystem->folder_new($targetPath);
		chmod($targetPath, 0755);
	}

	# temp folder
	$dbStorePath .= "tmp/";
	$targetPath .= "tmp/";

	if(!is_dir($targetPath)) {
		$libfilesystem->folder_new($targetPath);
		chmod($targetPath, 0755);
	}
	# get file name
	$tempFile = $_FILES['submitFile']['tmp_name'];
	$origFileName =  $_FILES['submitFile']['name'];

	# encode file name
	$targetFileHashName = sha1($student_id.microtime());
	$targetFile = $targetPath.$targetFileHashName;
	
	# upload file
	if($type==$ies_cfg['HandinFileByStudent']) {	# student file update->move to student directory
		$dbStorePath = "/file/sba/scheme/scheme_s".$scheme_id."/u".$UserID."/";
		$pathStudent = $intranet_root.$dbStorePath;
		if(!is_dir($pathStudent)){
			$libfilesystem->folder_new($pathStudent);
		}
		$targetPath = $pathStudent;
		$targetFile = $pathStudent.$targetFileHashName;

		$uploadResult = move_uploaded_file($tempFile, $targetFile);

	} else {		# teacher file update, more to "tmp" folder
		$uploadResult = move_uploaded_file($tempFile, $targetFile);
	}



	# add record to DB
	$libies = new libies();
	$IS_CREATE_IF_NO_CURRENT_BATCH = true;
	$jsFunctionWindowLocation = '';
	switch($type) {
		case $ies_cfg['HandinFileByTeacher']	:	$dbtable = "IES_WORKSHEET_TEACHER_FILE";
													$jsFunctionWindowLocation = 'window.top.window';
													break;
		case $ies_cfg['HandinFileByStudent']	:	$dbtable = "IES_WORKSHEET_HANDIN_FILE";
													$dataAry['StudentID'] = $UserID;
													$dataAry['WorksheetID'] = $sheetID;
													$jsFunctionWindowLocation = 'window.parent';
													break;
		default	:	break;
	}
	
	$inputby = $UserID;
	
	//$dataAry['dbtable'] = $dbtable;
	$dataAry['FileName'] = $origFileName;
	$dataAry['FolderPath'] = $dbStorePath;
	$dataAry['FileHashName'] = $targetFileHashName;
	$dataAry['FileName'] = $origFileName;

	if(is_numeric($sheetID)){
		//submit with worksheet id
		$dataAry['WorksheetID'] = $sheetID;
		$sqlCondition = '  or WorksheetID = '.$sheetID.' ';
	}

	//$dataAry['DateInput'] = "NOW()";
	//$dataAry['InputBy'] = $UserID;
	
	$file_id = $libies->storeHandInFile($dbtable, $dataAry);
	
	$file_info = array();
//	$file_info = current($libies->getHandinFileInfo($dbtable, " WHERE (WorksheetID =0 or WorksheetID = {$sheetID}) AND ModifyBy=$UserID AND WorksheetFileID=$file_id"));

	
	$tmpResult = $libies->getHandinFileInfo($dbtable, " WHERE (WorksheetID =0 ".$sqlCondition." ) AND ModifyBy=$UserID AND WorksheetFileID=$file_id");



	if(is_array($tmpResult) && count($tmpResult) == 1){
		$file_info = current($tmpResult);
	}
	
	$finalUploadResult = $resultCreatedFolder && $uploadResult && $file_id;
}
//intranet_closedb();   
sleep(1);


?>

<script language="javascript" type="text/javascript">
	<?=$jsFunctionWindowLocation?>.stopUpload('<?=($finalUploadResult?1:0)?>', '<?=$file_info["WorksheetFileID"]?>', "<?=$file_info["FileName"]?>", "<?=$file_info["FileHashName"]?>", "<?=$file_info["DateModified"]?>");
</script>   
?>