<?php
/*
 * Editing by Paul
 * 
 * Modification Log:
 * 2016-08-15 (Paul)
 * 		- hide Mr Yeung schema (IES scheme 2017) by Sep 1 
 * 2016-07-18 (Paul)
 * 		- add checking of IES_SCHEME.RecordStatus, hide scheme that with RecordStatus = 0 for default scheme
 */ 
/*
/*
if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
	header("Location: /");
	exit;
}
*/
$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPageArr['IES'] = 1;
$CurrentPage = "Worksheet";

$objIES = new libies();
$ldb = new libdb();

# Result message after operation
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

# toolbar menu
//$html_toolbar = $linterface->GET_LNK_NEW("new.php?scheme_id={$schemeID}",$button_new,"","","",0);

$thisTeacherID = $UserID;

if($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] ){
	//check not a IES Admin login
	//do nothing
}else{
	$teacherSQLQuery = " inner join IES_SCHEME_TEACHER as ies_t  on ies_t.schemeID = scheme.schemeid ";
	$teacherCriteria = " and ies_t.UserID = {$thisTeacherID} ";  // the teacher id is equal to this teacher login
}
if(time() >= mktime(0, 0, 0, 9, 1, 2016)){
	$cond = ' and scheme.DefaultSchemeCode NOT IN (5)';
}else{
	$cond = '';
}

# Scheme 
//$sql = "SELECT sch.SchemeID, sch.Title, COUNT(ws.WorksheetID) FROM IES_SCHEME sch LEFT OUTER JOIN IES_WORKSHEET ws ON (ws.SchemeID=sch.SchemeID) GROUP BY sch.SchemeID ORDER BY sch.Title";


//group by ==> there is more than one worksheet for a scheme
$sql = 'select scheme.SchemeID as SchemeID , scheme.Title as Title,  count(ws.WorksheetID) as count
			from IES_SCHEME as scheme 
			'.$teacherSQLQuery.'
			left join IES_WORKSHEET as ws on scheme.SchemeID = ws.SchemeID 
		where (scheme.RecordStatus > 0 OR scheme.DefaultSchemeCode=0) AND scheme.SchemeType=\''.$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO'].'\' 
		    '.$teacherCriteria.'
		    '.$cond.'
		group by scheme.SchemeID, scheme.Title
		order by scheme.title
		';
$schemeData = $ldb->returnArray($sql,3);

if(count($schemeData)>0){
	# table content
	for($i=0,$i_max = count($schemeData); $i<$i_max  ; $i++)
	{	
		$html_worksheet_list .= "<tr>";
		$html_worksheet_list .= "<td valign='top'>".($i+1)."</td>";
		$html_worksheet_list .= "<td valign='top'><a href='index.php?mod=worksheet&task=worksheet&scheme_id=".$schemeData[$i][0]."'>".$schemeData[$i][1]."</a></td>";
		$html_worksheet_list .= "<td valign='top'>".$schemeData[$i][2]."</td>";
		$html_worksheet_list .= "</tr>";
	}
}else{
	$html_worksheet_list .= '<tr><td colspan="3" align="center">'.$Lang['IES']['NoRecordAtThisMoment'].'</td></tr>';
}

$html_js_script .= "
<script language='javascript'>
function removeQ(id) {
	if(confirm('$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete'))	{
		document.getElementById('id').value = id;
		document.form1.action = 'index.php?mod=worksheet&task=remove';
		document.form1.submit();
	}
}

function formSubmit() {
	document.form1.submit();	
}
</script>
";

### Title ###
$title = $Lang['IES']['Worksheet'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/worksheet/index.tmpl.php");
$linterface->LAYOUT_STOP();
?>
