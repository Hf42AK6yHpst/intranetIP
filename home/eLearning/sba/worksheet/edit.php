<?php
//modifying By : 
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
/*
if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
	header("Location: /");
	exit;
}
*/
$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "Worksheet";

$objIES = new libies();
$ldb = new libdb();

$worksheetDetail = $objIES->getWorksheetDetail($sheetID);
$startDate = substr($worksheetDetail['StartDate'],0,10);
$endDate = substr($worksheetDetail['EndDate'],0,10);
$scheme_id = $worksheetDetail['SchemeID'];
# scheme data
$schemeData = $objIES->GET_SCHEME_DETAIL($worksheetDetail['SchemeID']);

$anyStudentSubmission = $objIES->checkStudentSubmitWorksheetHandin($sheetID);

# Scheme Menu
if($anyStudentSubmission) {

	$schemeMenu = $schemeData['Title']."<input type='hidden' name='schemeID' id='schemeID' value='".$worksheetDetail['SchemeID']."'>";
} else {
	$schemeMenu = $objIES->getSchemeSelection($worksheetDetail['SchemeID']);
}

# button
$html_button .= $linterface->GET_ACTION_BTN($button_submit, "submit")."&nbsp;";
$html_button .= $linterface->GET_ACTION_BTN($button_reset, "reset")."&nbsp;";
$html_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "goCancel()");
//$html_button .= "<input type='hidden' name='schemeID' id='schemeID' value='{$scheme_id}'>";
//$html_button .= "<input type='hidden' name='sheetID' id='sheetID' value='{$sheetID}'>";
/*
$load_file_list = "";
$fileRecord = $objIES->getWorksheetFileData("IES_WORKSHEET_TEACHER_FILE", $sheetID);
for($i=0; $i<sizeof($fileRecord); $i++) {
	list($file_id, $file_name, $folder_path, $file_hashname, $modifiedDate) = $fileRecord[$i];
	$load_file_list .= "addFileDisplay('$file_id', '$file_name', '$file_hashname', '$modifiedDate', true);";
}
*/

# table content
$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['WorksheetTitle']."</td><td width='1'> : </td><td width='80%'><input type='text' name='title' id='title' size='50' value='".$worksheetDetail['Title']."'></td></tr>";
////$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['Scheme']."</td><td width='1'> : </td><td width='80%'>$schemeMenu</td></tr>";
$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['StartEndDate']."</td><td width='1'> : </td><td width='80%'>{$i_From} ".$linterface->GET_DATE_PICKER("startDate",$startDate)." {$i_To} ".$linterface->GET_DATE_PICKER("endDate",$endDate)."</td></tr>";
$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['UploadFile']."</td><td width='1'> : </td><td width='80%'>".$objIES->returnFileUploadResources($scheme_id,$ies_cfg['HandinFileByTeacher'],$ies_cfg['WorksheetFlag_Edit'], $sheetID)."<!--<div id=\"handin_block\"></div>--></td></tr>";

$html_form_action = 'index.php?mod=worksheet&task=edit_update';

### Title ###
$title = $Lang['IES']['Worksheet'];
$TAGS_OBJ[] = array($title,"");

$PAGE_NAVIGATION[] = array($Lang['IES']['Scheme'], "index.php?mod=worksheet&task=index");
$PAGE_NAVIGATION[] = array($schemeData['Title'], "index.php?mod=worksheet&task=worksheet&scheme_id=".$worksheetDetail['SchemeID']);
$PAGE_NAVIGATION[] = array($worksheetDetail['Title'], "index.php?mod=worksheet&task=detail&sheetID=$sheetID");
$PAGE_NAVIGATION[] = array($button_edit,"");



$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/worksheet/edit.tmpl.php");

echo $linterface->FOCUS_ON_LOAD("form1.title"); 

$linterface->LAYOUT_STOP();

?>
