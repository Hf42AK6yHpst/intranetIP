<?
include_once($intranet_root."/includes/sba/libSbaTask.php");
include_once($intranet_root."/includes/sba/libSbaStep.php");
include_once($intranet_root."/includes/sba/libSbaStepHandinFactory.php");

# Initialize Objects
$objTaskMapper = new SbaTaskMapper();
$objStepMapper = new SbaStepMapper();
$linterface    = new interface_html("sba_default.html");

# Get Task Object
$task = $objTaskMapper->getByTaskId($task_id);

# Get All Steps of this Task
//$steps = $task->getObjStepsAry();

if($step_id){
	# Get Step Object
	$step = $objStepMapper->getByStepId($step_id);
	
	# Get the AnswerID of the Step Handin
	$answer_id = $sba_thisMemberType==USERTYPE_STUDENT? $sba_libSba->getAnswerIDByStepIDUserID($step_id, $sba_thisStudentID) : null;
	
	# Get the Step Handin Object
	$StepHandin = libSbaStepHandinFactory::createStepHandin($task_id,$step->getQuestionType(), $step->getStepID(), $step->getQuestion(), $answer_id);
	
	# Define $h_step_sequence for Step Sequence No.
	$h_step_sequence = $step->getSequence(); 
	
	# Define $h_step_handin for Step Tools Display (Not for editing the tools)
	$h_step_handin = $StepHandin->getQuestionDisplay();
	
	# Define $h_step_handin_edit for Step Tools Display (Only for editing the tools [Teachers Only])
	$h_step_handin_edit = $StepHandin->getEditQuestionDisplay();
	
	# Define $h_step_handin_edit_caption for the Caption of the Step Tools
	$h_step_handin_edit_caption = $StepHandin->getCaption();
	
	# Define $h_steps_list
	$h_steps_list = $sba_libSba_ui->getTaskStepsCircleList($task, $step_id, $sba_allow_edit, false);
}
else{
	if($sba_allow_edit){
		$step = new step();
		$step->setTaskID($task_id);
		
		# Define $h_step_sequence
		$h_step_sequence = count($task->getObjStepsAry()) + 1;
		
		# Define $h_steps_list
		$h_steps_list = $sba_libSba_ui->getTaskStepsCircleList($task, $step_id, $sba_allow_edit, true);
	}
	else{
		echo "Access Denied";
		die();
	}
}

# Define $h_headling
$h_headling = $sba_libStage->getTitle();

# Define $h_deadline
$h_deadline = "呈交限期 : ".$sba_libStage->getDeadline();

# Define $h_navigation
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$sba_libScheme->getTitle(), 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=scheme_list&scheme_id=$scheme_id");
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$sba_libStage->getTitle(), 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=stageInfo&scheme_id=$scheme_id&stage_id=$stage_id");
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$task->getTitle(),
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=task&scheme_id=$scheme_id&stage_id=$stage_id&task_id=$task_id");
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>($step_id? $step->getTitle():"New Step"));
$h_navigation = $sba_libSba_ui->getNavigation($nav_ary);

# Define $h_task_title
$h_task_title = $task->getTitle();

# Define $h_step_title
$h_step_title = htmlspecialchars($step->getTitle());

# Define $h_step_description_display
$h_step_description_display = $linterface->Get_Display_From_TextArea($step->getDescription());

# Define $h_step_description_textarea
$h_step_description_textarea = $linterface->GET_TEXTAREA('r_step_Description', $step->getDescription());

# Define $h_step_question_type_option
if(is_array($sba_cfg['DB_IES_STEP_QuestionType']) && count($sba_cfg['DB_IES_STEP_QuestionType'])>0){
	foreach($sba_cfg['DB_IES_STEP_QuestionType'] as $QuestionTypeAry){
		$QuestionType = $QuestionTypeAry[0];
		$QuestionTypeCaption = $QuestionTypeAry[1];
		
		$h_step_question_type_option .= "<option value='$QuestionType'>$QuestionTypeCaption</option>";
	}
}

# Define $h_edit_bottom_btn
$h_edit_bottom_btn = "<input type=\"button\" value=\" &lt; 返回工作設定 \" onmouseout=\"this.className='formbutton'\" onmouseover=\"this.className='formbuttonon'\" class=\"formbutton\" name=\"submit2\" onclick=\"go_tasks()\">
					  <input type=\"button\" value=\" 離開 \" onmouseout=\"this.className='formsubbutton'\" onmouseover=\"this.className='formsubbuttonon'\" class=\"formsubbutton\" name=\"submit2\">";

if($msg!="") {
	$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];	
}

$linterface->LAYOUT_START($ReturnMsg);

$template_script = "templates/content/step.tmpl.php";
include_once("templates/paper_template1.php");

$linterface->LAYOUT_STOP();
?>