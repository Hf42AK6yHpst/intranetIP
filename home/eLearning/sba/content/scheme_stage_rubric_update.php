<?php
//modifying By
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");

include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_rubric.php");


$classRoomID = libies_static::getIESRurbicClassRoomID();
$objRubric = new libiesrubric($classRoomID);

$objRubric->Start_Trans();
for($i=0, $i_max=count($task_rubric_id); $i<$i_max; $i++)
{
	$_rubric_id = $task_rubric_id[$i];
	$_rubric_title = $objRubric->Get_Safe_Sql_Query($task_rubric_title[$_rubric_id]);

	$sql = "UPDATE task_rubric SET title = '{$_rubric_title}' WHERE rubric_id = {$_rubric_id}";
	$res[] = $objRubric->db_db_query($sql);

	$sql = "DELETE FROM task_rubric_detail WHERE rubric_id = {$_rubric_id}";
	$res[] = $objRubric->db_db_query($sql);

	for($j=0, $j_max=count($rubric_detail[$_rubric_id]); $j<$j_max; $j++)
	{
		$_score_from = $score_from[$_rubric_id][$j];
		$_score_to = $score_to[$_rubric_id][$j];
		$_score = $score[$_rubric_id][$j];
		$_desc = $objRubric->Get_Safe_Sql_Query($desc[$_rubric_id][$j]);
		
		$sql = "INSERT INTO task_rubric_detail ";
		$sql .= "(rubric_id, task_id, phase_id, assessment_id, score, score_from, score_to, description, createdby, inputdate) ";
		$sql .= "SELECT rubric_id, task_id, phase_id, assessment_id, '{$_score}', '{$_score_from}', '{$_score_to}', '{$_desc}', createdby, NOW() ";
		$sql .= "FROM task_rubric WHERE rubric_id = {$_rubric_id}";
		$res[] = $objRubric->db_db_query($sql);
	}
	
  // Get max score of rubrics
  $rubric_max_score = $objRubric->getRubricMaxScore($_rubric_id);
  
  // Map task rubric to stage marking criteria
  // Update max score by rubrics
  $sql = "UPDATE {$intranet_db}.IES_STAGE_MARKING_CRITERIA ";
  $sql .= "SET MaxScore = {$rubric_max_score} ";
  $sql .= "WHERE task_rubric_id = {$_rubric_id}";
  $res[] = $sba_libSba->db_db_query($sql);
}
$final_res = (count($res) == count(array_filter($res)));

if($final_res)
{
	$objRubric->Commit_Trans();
}
else
{
	$objRubric->RollBack_Trans();
}

?>

<script language="JavaScript">
// retrieve query string from parent window
var query = parent.window.location.search.substring(1);

// parse query string
var qsParm = new Array();
var parms = query.split('&');
for (var i=0; i<parms.length; i++) {
	var pos = parms[i].indexOf('=');
	if (pos > 0) {
		var key = parms[i].substring(0,pos);
		var val = parms[i].substring(pos+1);
		qsParm[key] = val;
	}
}
qsParm["msg"] = "update";

// rebuild query string
query = "";
for(i in qsParm)
{
	query += i+"="+qsParm[i]+"&";
}
query = query.substring(0, query.length-1);

parent.window.tb_remove();
parent.window.location.href = parent.window.location.pathname+"?"+query;
</script>