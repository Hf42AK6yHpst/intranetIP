<?php
/**
 * Editing by Paul
 * 
 * Modification Log: 
 * 2019-05-17 (Paul) [ip.2.5.10.6.1]
 * 		- edit the folder path to ensure the file is under eclass folder
 * 
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");

$fs=new libfilesystem();

$_folder = $intranet_root.'/file'.$sba_cfg['SBA_EXPORT_STUDENTHANDIN_ROOT_PATH']."u_".$_SESSION['UserID'];
$fs->folder_new($_folder);

$_folder.="/imgtemp";
$fs->folder_new($_folder);

if (isset($chart_id)){
	echo $chart_id;
	$SurveyIDAry=$_SESSION['SurveyIDAry'];
	$SurveyID=$SurveyIDAry[$chart_id]['SurveyID'];
        $StepID=$SurveyIDAry[$chart_id]['StepID'];

	$fs = new libfilesystem();

	$file_new_path = $_folder.'/'.$SurveyID.'-'.$chart_id.'.png';
	$_SESSION['ExportPathAry'][$StepID][$SurveyID][$chart_id]=$file_new_path;//save the graph paths
	
	$f = fopen($file_new_path, 'w');
	fwrite($f, base64_decode($image_data));
	fclose($f);

	chmod($file_new_path, 0777); 
	echo $file_new_path;
}

?>