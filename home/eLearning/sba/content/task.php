<?
# using : Thomas
include_once($intranet_root."/includes/sba/libSbaTask.php");
include_once($intranet_root."/includes/sba/libSbaStep.php");
include_once($intranet_root."/includes/sba/libSbaTaskHandinFactory.php");

# Initialize Objects
$objTaskMapper = new SbaTaskMapper();
$linterface    = new interface_html("sba_default.html");

# Get All Tasks of this Stage
$tasks = $objTaskMapper->findByStageId($stage_id);

# Add an empty task for Creating New Task
if($sba_allow_edit){
	$emptyTasks = new task();
	$emptyTasks->setStageID($stage_id);
	
	$tasks[] = $emptyTasks;
}

# Define $h_headling
$h_headling = $sba_libStage->getTitle();

# Define $h_deadline
$h_deadline = $Lang['IES']['SubmissionDeadline']." : ".$sba_libStage->getDeadline();

# Define $h_navigation
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$sba_libScheme->getTitle(), 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=scheme_list&scheme_id=$scheme_id");
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$sba_libStage->getTitle(), 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=stageInfo&scheme_id=$scheme_id&stage_id=$stage_id");
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$Lang['SBA']['TaskList']);
$h_navigation = $sba_libSba_ui->getNavigation($nav_ary);

# Define $h_stage_top_tool
if($sba_allow_edit){
	$h_stage_top_tool = '<div id="sba_task_reorderBtn" style="margin-top:-25px;'.(count($tasks)<=2? 'display:none;':'').'" class="stage_top_tool">
							<a title="'.$Lang['SBA']['ReOrderTask'].'" class="move" href="javascript:void(0)" onclick="reorderTask()"><span>'.$Lang['SBA']['ReOrderTask'].'</span></a>
						 </div>';
}

# Define $h_main_content
for($i=0, $i_max=count($tasks); $i<$i_max; $i++){
	$_objTask = $tasks[$i];
	
	# Set the Sequence
	$_objTask->setSequence($i+1); // Sequence Should Start With 1
	
	# Get the AnswerID of the Task Handin
	$_answerID = $_objTask->getTaskID() && $sba_thisMemberType==USERTYPE_STUDENT? $sba_libSba->getAnswerIDByTaskIDUserID($_objTask->getTaskID(), $sba_thisStudentID) : null;
	
	# Get the Task Handin Object
	# Remark : Temporary set the Task Handin Type as $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']
	$_objTaskHandin = libSbaTaskHandinFactory::createTaskHandin($_objTask->getStageID(), $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea'], $_objTask->getTaskID(), $_answerID);
	
	$h_main_content .= $sba_libSba_ui->getTaskUI($_objTask, $_objTaskHandin, $sba_allow_edit);
}

if(!$sba_allow_edit){
	$h_right_button = '<div class="IES_top_tool">
							<a class="submission" href="index.php?mod=content&task=handinTask&scheme_id='.$scheme_id.'&stage_id='.$stage_id.'" title="">
								<span>'.$Lang['IES']['Submit'].'</span>
							</a>
				    	</div>
				    	<br class="clear">';
	$h_main_content .= $h_right_button;
}

# Define $h_button
$h_button = '<input name="back_btn" type="button" class="formbutton" value=" &lt; '.$Lang['IES']['Back'].' " onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'">
			 <input name="cancel_btn" type="button" class="formsubbutton" value=" '.$Lang['SBA']['Btn_Quit'].' " onmouseout="this.className=\'formsubbutton\'" onmouseover="this.className=\'formsubbuttonon\'">';

$linterface->LAYOUT_START();

$template_script = "templates/content/task.tmpl.php";
include_once("templates/paper_template1.php");

$linterface->LAYOUT_STOP();
?>