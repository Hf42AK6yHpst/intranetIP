<?
# using : Thomas
include_once($intranet_root."/includes/sba/libSbaTask.php");
include_once($intranet_root."/includes/sba/libSbaStep.php");

# Initialize Objects
$objTaskMapper = new SbaTaskMapper();
$objStepMapper = new SbaStepMapper();
$linterface    = new interface_html("sba_thickbox.html");

# Get All Tasks of this Stage
$tasks = $objTaskMapper->findByStageId($stage_id);

# Define $h_thickbox_header
$h_thickbox_header = $sba_libStage->getTitle()." - ".$Lang['SBA']['TasksAndSteps'];

# Define $h_class
$h_class = "task_shortcut";

# Define $h_task_step_details
for($i=0;$i<count($tasks);$i++){
	$_objTask = $tasks[$i];
	
	# Get All Steps of this Task
	$steps = $objStepMapper->findByTaskId($_objTask->getTaskID());	
	
	$h_task_step_details .= "<tr class=\"task_top\"><th><a href=\"javascript:void(0)\" onclick=\"goTask('".$_objTask->getTaskID()."')\">".$_objTask->getTitle()."</a></th><td>".(count($steps)>0? $steps[0]->getTitle():$Lang['SBA']['NoSteps'])."</td></tr>";
	
	for($j=1;$j<count($steps);$j++){ // first step is displayed above, i.e. start with index 1
		$_objStep = $steps[$j];
		$h_task_step_details .= "<tr><th>&nbsp;</th><td>".$_objStep->getTitle()."</td></tr>";
	}
}

$h_task_step_details = $h_task_step_details? $h_task_step_details:"<tr><td colspan='2' align='center'>".$Lang['SBA']['NoTasks']."</td></tr>";

$linterface->LAYOUT_START();

$template_script = "templates/content/stageDetails.tmpl.php";
include_once("templates/thickbox_template.php");

$linterface->LAYOUT_STOP();
?>