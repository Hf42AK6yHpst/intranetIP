<?php

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

# Initialize Variable
$finalUploadResult   = false;
$resultCreatedFolder = false;

# Initialize Objects
$libfilesystem = new libfilesystem();

if (!empty($_FILES)) {
	$targetPath = $sba_cfg['SBA_STUDENTHANDIN_ROOT_PATH']."scheme_s$scheme_id/u".$student_id."/";
	# create folder
	$targetFullPath = str_replace('//', '/', $intranet_root.$targetPath);
	if ( file_exists($targetFullPath) ) {
		$resultCreatedFolder = true;
	} else {
		$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
	}
	
	chmod($targetFullPath, 0755);
	
	# get file name
	$tempFile = $_FILES['submitFile']['tmp_name'];
	$origFileName =  $_FILES['submitFile']['name'];
	
	# encode file name
	$targetFileHashName = sha1($student_id.microtime());
	$targetFile = $targetFullPath.$targetFileHashName;
	
	# upload file
	$uploadResult = move_uploaded_file($tempFile, $targetFile);
	
	# add record to DB
	$libies = new libies();
	$IS_CREATE_IF_NO_CURRENT_BATCH = true;
	$batchId = $libies->getCurrentBatchId($stage_id,$student_id,$IS_CREATE_IF_NO_CURRENT_BATCH);
	$stage_student_id = $libies->updateStudentStageBatchHandinStatus($stage_id,$student_id,$ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["submitting"]);
	$file_id = $libies->handInFileToStage($origFileName,$targetPath,$targetFileHashName,$student_id,$batchId);
	
	$file_info = current($libies->getStageHandinFileInfo_byFileId($file_id));
	
	$finalUploadResult = $resultCreatedFolder && $uploadResult && $file_id;
}
   
sleep(1);
?>
<script language="javascript" type="text/javascript">window.top.window.stopUpload(<?=($finalUploadResult?1:0)?>, <?=$file_info["FILEID"]?>, "<?=$file_info["FILENAME"]?>", "<?=$file_info["FILEHASHNAME"]?>", "<?=$file_info["DATEMODIFIED"]?>");</script>   
