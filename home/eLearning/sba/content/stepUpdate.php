<?php
# using : fai
include_once($intranet_root."/includes/sba/libSbaStep.php");
include_once($intranet_root."/includes/sba/libSbaStepHandinFactory.php");

# Initialize variable
$r_step_Action = isset($r_step_Action) && $r_step_Action!=''? $r_step_Action:'';

switch($r_step_Action){
	case 'StepEditUpdate' : 
		# Initialize variable
		$StepID 	  = isset($step_id) && $step_id!=''? $step_id:0;
		$TaskID 	  = isset($task_id) && $task_id!=''? $task_id:0;
		$Title		  = isset($r_step_Title) && $r_step_Title!=''? stripslashes($r_step_Title):'';
		$Description  = isset($r_step_Description) && $r_step_Description!=''? stripslashes($r_step_Description):'';
		$QuestionType = isset($r_step_QuestionType) && $r_step_QuestionType!=''? $r_step_QuestionType:'';
		$Question	  = isset($r_step_Question) && $r_step_Question!=''? $r_step_Question:array();
		$Sequence	  = isset($r_step_Sequence) && $r_step_Sequence!=''? $r_step_Sequence:0;
		
		# Initialize Objects
		$objStepMapper = new SbaStepMapper();
		$objStepHandin = libSbaStepHandinFactory::createStepHandin($TaskID,$QuestionType, $StepID, $Question);
		
		# Get the Question String from $objStepHandin object by passing an $Question array to it
		$QuestionStr = $objStepHandin->getQuestionStr();
		
		# Get Step Object
		if($StepID){
			# Get the existing Step
			$objStep = $objStepMapper->getByStepId($StepID);
			
			# Return Message
			$msg = "UpdateSuccess";
		}
		else{
			$objStep = new step();
			
			# Set Step Instance for newly created Task
			$objStep->setStepNo(null); // Insert a NULL to StepNo field in table IES_STEP AS StepNo is Useless in SBA
			$objStep->setDateInput('now()');
			$objStep->setInputBy($sba_thisUserID);
			
			# Return Message
			$msg = "AddSuccess";
		}
		
		# Set Step Instance
		$objStep->setTaskID($TaskID);
		$objStep->setTitle($Title);
		$objStep->setDescription($Description);
		$objStep->setQuestionType($QuestionType);
		$objStep->setQuestion($QuestionStr);
		$objStep->setSequence($Sequence);
		$objStep->setSaveToTask(0); // Do not allow step answer to directly save to task
		$objStep->setModifyBy($sba_thisUserID);
		
		/* Unknown field */
		$objStep->setStatus($Status);
		
		$objStepMapper->save($objStep);
		
		header('location:index.php?mod=content&task=step&scheme_id='.$scheme_id.'&stage_id='.$stage_id.'&task_id='.$task_id.'&step_id='.$objStep->getStepID().'&msg='.$msg);
		
		break;
	default :
		break;
}