<?
# Initialize Objects
$linterface = new interface_html("sba_default.html");

# Define $h_headling
$h_headling = $sba_libStage->getTitle();

# Define $h_deadline
$h_deadline = $Lang['IES']['StudentSubmitData']." : ".$sba_libStage->getDeadline();

# Define $h_right_button
$h_right_button = "<div class='IES_top_tool'>
						<a title='' class='submission' href='index.php?mod=content&task=task&scheme_id=$scheme_id&stage_id=$stage_id'><span>".$Lang['IES']['BackToHomepage']."</span></a>
				   </div>";

$linterface->LAYOUT_START();

$template_script = "templates/content/handinTask.tmpl.php";
include_once("templates/paper_template1.php");

echo $h_stage_content;

$linterface->LAYOUT_STOP();
?>