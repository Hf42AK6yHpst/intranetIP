<?php
include_once($PATH_WRT_ROOT."includes/sba/libies_bibi.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaScheme.php");

$libies_bibi = new libies_bibi();
$objSbaScheme = new SbaScheme($scheme_id);
$SchemeTitle = $objSbaScheme->getTitle();
$SchemeTitle = $sba_libSba->filename_safe($SchemeTitle);
$SchemeTitle = str_replace('.','',$SchemeTitle);
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=".$SchemeTitle."_".$Lang['IES']['bibi'].".doc");
?>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<body>
<?

$bibiArray = $libies_bibi->getAllbibiBySchemeId($scheme_id, $UserID, $type, $cond);


if(is_array($bibiArray)){
	echo "<table style='border-collapse:collapse'>";
	echo "<tr><td class='tool_title_col'>{$Lang['IES']['bibi']}</td>
			<td class='tool_type_col'>{$Lang['IES']['type']}</td>
			<td class='tool_tags_col'>{$Lang['IES']['remark']}</td></tr>";
	foreach($bibiArray as $key=>$value){
		echo "<tr>";
		$stringArray = $libies_bibi->parseContentString($value['Content']);
		$PresentString = $libies_bibi->getPresentString($stringArray, $value['Type']);
		
		echo "<td>";
		echo nl2br($libies_bibi->stringfilter($PresentString));
		echo "</td>";
		echo "<td>";
		echo $Lang['IES'][$value['Type']]['fieldname'];
		echo "</td>";
		echo "<td>";
		echo nl2br($stringArray['remark']);
		echo "</td>";
		echo "</tr>";

	}
	echo "</table>";
}


?>
</body>
</html>
