<?
# using : Thomas

# Initialize Objects
$objIES 	= new libies();
$linterface = new interface_html("sba_thickbox.html");

# Define $h_thickbox_header
$h_thickbox_header = $Lang['SBA']['ReOrder'];

# Define $h_class
$h_class = "stage_reorder";

# Get All Stage of this Scheme
$stages_ary = $objIES->GET_STAGE_ARR($scheme_id);

# Define $h_thickbox_content
for($i=0, $i_max=count($stages_ary); $i<$i_max; $i++){
	$h_thickbox_content .= "<div class=\"stage".($i+1)." sba_reorderStage_item\" style=\"margin-bottom:15px\"> <!-- define the stage number, the background will vary-->
								<div class=\"star\"></div>
								<div class=\"cover_wrap\">".$stages_ary[$i]['Title']."</div>
								<input type=\"hidden\" id=\"sba_reorderStage_".$stages_ary[$i]['StageID']."\" name=\"r_sba_reorderStage[]\" value=\"".$stages_ary[$i]['StageID']."\">
							</div>";
}

# Define $h_thickbox_bottom_btn
$h_thickbox_bottom_btn = "<input type=\"button\" value=\"".$Lang['SBA']['Btn_Apply']."\" onclick=\"formSubmit()\" onmouseout=\"this.className='formbutton'\" onmouseover=\"this.className='formbuttonon'\" class=\"formbutton\" name=\"submit2\">
						  <input type=\"button\" value=\"".$Lang['IES']['Cancel']."\" onclick=\"window.parent.tb_remove()\" onmouseout=\"this.className='formsubbutton'\" onmouseover=\"this.className='formsubbuttonon'\" class=\"formsubbutton\" name=\"submit2\">";

$linterface->LAYOUT_START();

$template_script = "templates/content/reorderStage.tmpl.php";
include_once("templates/thickbox_template.php");

$linterface->LAYOUT_STOP();
?>