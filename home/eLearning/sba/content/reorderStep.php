<?
# using : Thomas

include_once($intranet_root."/includes/sba/libSbaStep.php");

# Initialize Objects
$objStepMapper = new SbaStepMapper();
$linterface    = new interface_html("sba_thickbox.html");

# Define $h_thickbox_header
$h_thickbox_header = $Lang['SBA']['ReOrder'];

# Define $h_class
$h_class = "stage_reorder";

# Get All Steps of this Stage
$steps_ary = $objStepMapper->findByTaskId($task_id);

# Define $h_thickbox_content
for($i=0, $i_max=count($steps_ary); $i<$i_max; $i++){
	$_objStep = $steps_ary[$i];
	
	$h_thickbox_content .= "<div class=\"task_steps sba_reorderStep_item\" style=\"margin-bottom:15px\">
								<div>".$_objStep->getTitle()."</div>
								<input type=\"hidden\" id=\"sba_reorderStep_".$_objStep->getStepID()."\" name=\"r_sba_reorderStep[]\" value=\"".$_objStep->getStepID()."\">
							</div>";
}

# Define $h_thickbox_bottom_btn
$h_thickbox_bottom_btn = "<input type=\"button\" value=\"".$Lang['SBA']['Btn_Apply']."\" onclick=\"formSubmit()\" onmouseout=\"this.className='formbutton'\" onmouseover=\"this.className='formbuttonon'\" class=\"formbutton\" name=\"submit2\">
						  <input type=\"button\" value=\"".$Lang['IES']['Cancel']."\" onclick=\"window.parent.tb_remove()\" onmouseout=\"this.className='formsubbutton'\" onmouseover=\"this.className='formsubbuttonon'\" class=\"formsubbutton\" name=\"submit2\">";

$linterface->LAYOUT_START();

$template_script = "templates/content/reorderStep.tmpl.php";
include_once("templates/thickbox_template.php");

$linterface->LAYOUT_STOP();
?>