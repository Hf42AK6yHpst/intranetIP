<?php
$linterface = new interface_html("sba_survey.html");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$linterface->LAYOUT_START();
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/sba/libies.php");
include_once($PATH_WRT_ROOT."includes/sba/libSba.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaStepHandinFactory.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
include_once("survey/generateChartScript.php");

$objTaskMapper = new SbaTaskMapper();
$libies_survey = new libies_survey();
$sba_libSba = new libSba();
$objStepMapper = new SbaStepMapper();

$count=0;
$script = "\nvar QuestionTitleAry = new Array();\n";
$script .= "var dataArr = new Array();\n";
$chartArr=array();
$html="";
$SurveyIDAry=array();
$ExportPathAry=array();

foreach ($objTaskMapper->findByStageId($stage_id) as $tasks){
	/*
	if(!is_array($task)) continue;
	if(!is_array($task)){
		$task = array($task);
	}
	if(!$task[0]) continue;
	*/
	if(!is_array($tasks)){
		$tasks = array($tasks);
	}
	if(!$tasks[0]) continue;
	
	foreach ($tasks as $mtask){
		$steps = $mtask->getObjStepsAry();
		$question_type=$sba_cfg['DB_IES_STEP_QuestionType']['surveyAnalysis'][0];
		
		if(!is_array($steps)){
			$steps = array($steps);
		}
		if(!$steps[0]) continue;
		
		foreach($steps as $step){
                    
                        $thisStepID = $step->getStepID();
			if ($step->getQuestionType()!=$question_type) continue;
			
			$stepHandin=libSbaStepHandinFactory::createStepHandin($step->getTaskID(),$step->getQuestionType(),$step->getStepID(),$step->getQuestion());
			
			$objSteps = $objStepMapper->findByStepIds($stepHandin->getSelfQuestionDataStructure());
			$surveyIDs=array();
			if(!is_array($objSteps)){
				$objSteps = array($objSteps);
			}
			if(!$objSteps[0]) continue;
			
                        
			foreach ($objSteps as $objStep){
                            
                                
				$surveyIDs=$stepHandin->getSurveyIDs($objStep->getStepID());
                                $surveys = $libies_survey->getStudentSurveyBySurveyId($surveyIDs);
                                
                                if(!is_array($surveys)){
                                    $surveys = array($surveys);
                                }
                                if(!$surveys[0]) continue; 
                                foreach ($surveys as $survey){
                                        
                                        $SurveyID=$survey['SurveyID'];
                                        if ($SurveyID==0) continue;
                                        
                                        $questionsArray = $libies_survey->breakQuestionsString($survey['Question']);
                                        
                                        $folder_directory = $libies_survey->createTempFolder($sba_thisUserID,$SurveyID);
                                        
                                        $ExportPathAry[$stepID][$SurveyID]=array();
                                
                                        if(!is_array($questionsArray)){
                                                $questionsArray = array($questionsArray);
                                        }
                                        if(!$questionsArray) continue;
                                        foreach ($questionsArray as $num=>$question){//questions in the survey
                                                                                
                                                if (!$libies_survey->isQuestionTypehasGraph($question['TYPE'])) continue;//no grpah, skip
                                                                                                
                                                $script .= "QuestionTitleAry[$count] = \"".$sba_libSba->filename_safe($question['TITLE'])."\";\n";	
                                        
                                                $displayCreateNewQuestionOption = 2;
                                                $chartArr[$count.'_bar'] = $libies_survey->GetChartArr($SurveyID, $num,'',$sba_thisStudentID,$displayCreateNewQuestionOption,'bar');
						$SurveyIDAry[$count.'_bar']=array('StepID'=>$thisStepID, 'SurveyID'=>$SurveyID);
						
						if ($libies_survey->isQuestionTypehasPieChart($question['TYPE'])){//not pie of likert
						    $chartArr[$count.'_pie'] = $libies_survey->GetChartArr($SurveyID, $num,'',$sba_thisStudentID,$displayCreateNewQuestionOption,'pie');
						    $SurveyIDAry[$count.'_pie']=array('StepID'=>$thisStepID, 'SurveyID'=>$SurveyID);
						}
						
                                                $html .= $libies_survey->ReturnSurveyChartDiv($question['TYPE'], $count+1);
                                                
                                               
                                                $count++;
                                        }
                                        
                                        $SurveyMappings = $libies_survey->loadSurveyMapping($SurveyID,$sba_thisStudentID);//mapping graphs
                                        if (!$SurveyMappings) continue;//no mapping, skip
                                        
                                        foreach ($SurveyMappings as $SurveyMapping){
                                                
                                                $SurveyMappingID=$SurveyMapping['SURVEYMAPPINGID'];
                                                $x_axis = $SurveyMapping["XMAPPING"];
                                                $y_axis = $SurveyMapping["YMAPPING"];
                                                
                                                $chartArr[$count.'_bar']=$libies_survey->GetCombineChart($SurveyID,$x_axis,$y_axis,$SurveyMappingID);
                                                
                                                $html .= $libies_survey->ReturnSurveyChartDiv('surveyMapping', $count+1);
                                                
                                                $SurveyIDAry[$count.'_bar']=array('StepID'=>$thisStepID, 'SurveyID'=>$SurveyID);
                                                $count++;
                                        }
                                }
			}
			
			
			
		}				
	}
	
}
foreach($chartArr as $chartID => $chartJSON ) {
			
	$script.="dataArr['".($chartID)."'] = ".$chartJSON->toPrettyString().";\n";//print the chart data for flash to get

}

$script.="chartNum =".sizeof($chartArr).";\n";	
$_SESSION['SurveyIDAry']=$SurveyIDAry;//for storing the order of exports vs survey id
$_SESSION['ExportPathAry']=$ExportPathAry;//for storing the survey id vs image path

echo "<script>$script</script><div style='left:-9999px; position:absolute;'>$html</div>";
//echo "<script>$script</script><div style=''>$html</div>";
?>
<script type="text/javascript">
	var graphLoaded=0;

	function ofc_ready(i){

	    var id = i[0];
	    setTimeout(function(){
		var image_binary = swfobject.getObjectById("my_chart_"+id+"_Final").get_img_binary();
						
		$.post("content/uploadFlashImage.php", {image_data: image_binary, chart_id: id}, function(data){
		   
		    if (++graphLoaded==chartNum){
			opener.location.href="index.php?mod=content&task=exportdoc&exportFormat=<?=$exportFormat?>&stage_id=<?=$stage_id?>&scheme_id=<?=$scheme_id?>";
			window.close();
		    }
		});
	    }, 2000);
	    
	}
			
	for (var id in dataArr){
	    
	    swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart_"+id+"_Final", <?=$sba_cfg['SBA']['SurveyChart']['ChartWidth']?>, <?=$sba_cfg['SBA']['SurveyChart']['ChartHeight']?>, "9.0.0", "", {id: id});
	    
	}

	if(chartNum==0){
		opener.location.href="index.php?mod=content&task=exportdoc&exportFormat=<?=$exportFormat?>&stage_id=<?=$stage_id?>&scheme_id=<?=$scheme_id?>";
		window.close();
	}
	//end for
</script>
<?php echo $linterface->Get_Ajax_Loading_Image(); ?>
<!--h2>Loading...</h2/-->
<?$linterface->LAYOUT_STOP();?>