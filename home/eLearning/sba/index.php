<?php
//editing by : 
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");


if(count($plugin['SBA']) == 0) {
	//redirect to home if have not enable $plugin['IES_COMBO']
	header("Location: /");
	exit();
}


//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/sba_lang.$intranet_session_language.php");


// skip authenticate user login if do public survey
$skipAuth = false;
if($mod=='survey' && ($task=='questionnaire' || $task=='fill_update' || $task=='result') && $key!=''){
	$surveyParameters = breakEncodedURI($key);
	if($surveyParameters['skipCheck']==1){
		$skipAuth = true;
	}
}

if(!$skipAuth){
	intranet_auth();
}

intranet_opendb();


//default lang is same as IP25
$sba_thisSchemeLang = $intranet_session_language;
$_SESSION['sba_SchemeLang']['currentSessionLang'] = $sba_thisSchemeLang;

$libdb = new libdb();

if(strtolower($mod)==strtolower('admin') || 
		strtolower($mod)==strtolower('commentbank') || 
		((strtolower($mod)==strtolower('worksheet')) && strtolower($task)==strtolower('worksheet')) || 
		((strtolower($mod)==strtolower('worksheet')) && strtolower($task)==strtolower('new')) ||
		((strtolower($mod)==strtolower('worksheet')) && strtolower($task)==strtolower('detail'))
	) {
		//do nothing because those come from admin  , skip the lang checking
	// do nothing
}else{
	//only consider the scheme lang for these mod
	if(isset($scheme_id) && intval($scheme_id)>0) {
		if(isset($_SESSION['sba_SchemeLang'][$scheme_id]) && $_SESSION['sba_SchemeLang'][$scheme_id] != ""){
			$sba_thisSchemeLang = $_SESSION['sba_SchemeLang'][$scheme_id];
		}else{
			$sql = "select Language as 'Language' from IES_SCHEME where schemeid={$scheme_id}";
			$tempResult = $libdb->returnVector($sql);
			$sba_thisSchemeLang = ( $tempResult[0] == "" ) ? $intranet_session_language :  $tempResult[0];
			$_SESSION['sba_SchemeLang'][$scheme_id] = $sba_thisSchemeLang;
		}

		//for some page , eg survey , there is no schemeID , but suppose use the current session lang
		//save the lang to a current session
		$_SESSION['sba_SchemeLang']['currentSessionLang'] = $sba_thisSchemeLang; 
	}else{
		if(isset($_SESSION['sba_SchemeLang']['currentSessionLang']) && $_SESSION['sba_SchemeLang']['currentSessionLang']!=''){
			$sba_thisSchemeLang = $_SESSION['sba_SchemeLang']['currentSessionLang'];
		}
	}
}

include_once($PATH_WRT_ROOT."lang/lang.$sba_thisSchemeLang.php");
include_once($PATH_WRT_ROOT."lang/sba_lang.$sba_thisSchemeLang.php");

include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/sba/libies.php");

include_once($PATH_WRT_ROOT."includes/sba/libSba.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaScheme.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaStage.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaTask.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaStep.php");
# Lang will be override by scheme lang (refer to index_content.php)

include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");


$sba_libSba = new libSba();

# 
if(isset($isPreview) && $isPreview) {
	$_SESSION['sba_IsPreview'] = 1;
}
if(isset($isEdit) && $isEdit) {
	$_SESSION['sba_IsPreview'] = 0  ;	
}



$sba_thisAction = (isset($_POST['r_action']))? $_POST['r_action'] : $_GET['r_action'];
$sba_thisActionIsMarking = false;

switch ($sba_thisAction) {
	case $sba_cfg["actionArr"]["marking"]:
		$sba_thisActionIsMarking = true;
		if ($p_studentId == '') {
			// do nth
		}
		else {
			$_SESSION['sba_pretentStudentID'] = $p_studentId;
		}
		break;
	case $sba_cfg["actionArr"]["peerMarking"]:
		$sba_thisActionIsMarking = true;
		$sba_thisPeerMarkingMarkerUserId = $sba_thisUserID;
		
		if ($p_studentId == '') {
			// do nth
		}
		else {
			$_SESSION['sba_pretentStudentID'] = $p_studentId;
		}
		break;
	case $sba_cfg["actionArr"]["preview"]:
	default:
		$_SESSION['sba_pretentStudentID'] = '';
}

//IP intranet user id 
$sba_thisStudentID = ($_SESSION['sba_pretentStudentID'])? $_SESSION['sba_pretentStudentID'] : $_SESSION['UserID'];
$sba_thisUserID = $_SESSION['UserID'];

$sba_thisMemberType = ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['sba_pretentStudentID']) ? USERTYPE_STUDENT : USERTYPE_STAFF;



#################################################################
# Return whether current user can edit SBA/Scheme (for Teacher) #
#################################################################
$sba_allow_edit = $sba_libSba->IsEditable($sba_thisUserID);



################################################################
# Return whether current user is in preview mode (for Teacher) #
################################################################
$sba_is_preview = $_SESSION['sba_IsPreview'];
if($sba_thisMemberType==USERTYPE_STAFF) {
	if($sba_is_preview){
		$sba_allow_edit =''; // if teacher acts as student (i.e. $sba_is_preview==1, then teacher does not allow to edit in preview mode)
	}
}


//DON'T CHANGE THIS VALUE UNLESS YOU CONFIRM THE LOGIC
$sba_allowStudentSubmitHandin = !($sba_allow_edit  || $sba_is_preview);



$sba_logFile = '/tmp/sba.txt';


//GET USER REQUEST ACTION
$mod		= trim($mod);
$task		= trim($task);

if(trim($mod) == ''){
		$mod = $sba_thisMemberType==USERTYPE_STUDENT ? 'content' : 'admin';
		$task = 'index';

}

if(!isset($task) || $task=="") $task = 'index';


$task = str_replace("../","",$task);
$task_script = $mod.'/'.$task.'.php';

if(file_exists($task_script)){
	switch($mod){
		case 'content':
			$layout = 'index_content.php';
			break;
		case 'admin':
			$layout = 'index_admin.php';
			break;
		default:
			break;
	}
	/*
	ob_start();
		include_once($task_script);
		$html_content .= ob_get_contents();
	ob_end_clean();
	*/
	
	if($layout != "") {
		include_once($layout);
	}

	include_once($task_script);


}else{
	include_once('accessDeny.php');
}



//include_once('index_admin.php');
intranet_closedb();
?>