<?php
//modifying By : fai

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/sba/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/sba/libies.php");


include_once("common.php");

$objIES = new libies();
# Get parameters from previous page

$task_id = $task_id;
$StudentID = $StudentID;

$CurrentPage = "StudentProgress";

################################################
##	Start: Constructing the html for display
################################################

$libuser = new libuser($StudentID);

$UserDisplayName = (strtoupper($intranet_session_language) == "EN" ? $libuser->EnglishName:$libuser->ChineseName);
$answerAreaTemplate = $objIES->template_AnswerArea02($UserDisplayName);
$IS_DISPLAY_ALL_STEPS = true;
$IS_SHOW_DATE = true;
$html_display=$objIES->getAnswerArea($task_id,$StudentID,"");

################################################
##	End: Constructing the html for display
################################################

//$linterface = new interface_html();

### Title ###
$title = $ies_lang['StudentProgress'];
$TAGS_OBJ[] = array($title,"");

//$linterface->LAYOUT_START();

include_once("templates/admin/step_number_thick_box.tmpl.php");

//$linterface->LAYOUT_STOP();

intranet_closedb();

?>