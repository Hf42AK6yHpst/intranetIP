<?php

//modifying By mAX
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");


$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$li = new libdb();
$lexport = new libexporttext();

// CSV Header (stage title)
$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);
$criteria_arr = libies_static::getMarkingCriteria();
$header[0][] = "";		// Place holder
$header[1][] = "";		// Place holder
$currentStageTitle = "";
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
	for($j=0, $j_max=count($criteria_arr); $j<$j_max; $j++)
	{
		$_stageTitle = "";
		$_criteriaTitle = $criteria_arr[$j]["CRITERIATITLE"];
	
		// Not to repeat stage title
		if($currentStageTitle != $stage_arr[$i]["Title"])
		{
			$_stageTitle = $stage_arr[$i]["Title"];
			$currentStageTitle = $stage_arr[$i]["Title"];
		}
		$header[0][] = $_stageTitle;
		$header[1][] = $_criteriaTitle;
	}
	
	// Explicit add comment column
	$header[0][] = "";
	$header[1][] = $Lang['IES']['TeacherComment'];
}
$header[0][] = $Lang['IES']['TotalMark'];
$header[1][] = "";

// CSV Content (score)
$createField = "UserID int(8), ";
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
	$_stageID = $stage_arr[$i]["StageID"];
	$_stageCommentField = "StageComment_".$_stageID;

	for($j=0, $j_max=count($criteria_arr); $j<$j_max; $j++)
	{
		$_mcID = $criteria_arr[$j]["MarkCriteriaID"];
		$_stageScoreField = "StageScore_".$_stageID."_".$_mcID;

		$createField .= "{$_stageScoreField} varchar(16), ";
		$selectField .= "t.{$_stageScoreField}, ";
	}
	
	$createField .= "{$_stageCommentField} longtext, ";
	$selectField .= "t.{$_stageCommentField}, ";
}
$sql = "CREATE TEMPORARY TABLE tempScore({$createField} OverallScore varchar(16), PRIMARY KEY (UserID)) DEFAULT CHARSET=utf8";
$li->db_db_query($sql);

for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
	$_stageID = $stage_arr[$i]["StageID"];
	$_stageCommentField = "StageComment_".$_stageID;
	
	for($j=0, $j_max=count($criteria_arr); $j<$j_max; $j++)
	{
		$_mcID = $criteria_arr[$j]["MarkCriteriaID"];
		$_stageScoreField = "StageScore_".$_stageID."_".$_mcID;

		$sql = "INSERT INTO tempScore(UserID, {$_stageScoreField}) ";
		$sql .= "SELECT shb.UserID, m.Score ";
		$sql .= "FROM {$intranet_db}.IES_MARKING m ";
		$sql .= "INNER JOIN {$intranet_db}.IES_STAGE_MARKING_CRITERIA sc ";
		$sql .= "ON m.StageMarkCriteriaID = sc.StageMarkCriteriaID ";
		$sql .= "INNER JOIN {$intranet_db}.IES_STAGE_HANDIN_BATCH shb ";
		$sql .= "ON m.AssignmentID = shb.BatchID AND m.AssignmentType = {$ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]} ";
		$sql .= "WHERE sc.StageID = {$_stageID} AND sc.MarkCriteriaID = {$_mcID} ";
		$sql .= "ORDER BY m.DateModified ";
		$sql .= "ON DUPLICATE KEY UPDATE {$_stageScoreField} = VALUES({$_stageScoreField})";
		$li->db_db_query($sql);
	}
	
	$sql = "INSERT INTO tempScore(UserID, {$_stageCommentField}) ";
	$sql .= "SELECT shb.UserID, shc.Comment ";
	$sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH shb ";
	$sql .= "INNER JOIN {$intranet_db}.IES_STAGE_HANDIN_COMMENT shc ";
	$sql .= "ON shb.BatchID = shc.BatchID ";
	$sql .= "WHERE shb.StageID = {$_stageID} ";
	$sql .= "ON DUPLICATE KEY UPDATE {$_stageCommentField} = VALUES({$_stageCommentField})";
	$li->db_db_query($sql);
}

$sql = "INSERT INTO tempScore(UserID, OverallScore) ";
$sql .= "SELECT UserID, Score ";
$sql .= "FROM {$intranet_db}.IES_SCHEME_STUDENT ";
$sql .= "WHERE SchemeID = {$scheme_id} ";
$sql .= "ON DUPLICATE KEY UPDATE OverallScore = VALUES(OverallScore)";
$li->db_db_query($sql);

$sql = "SELECT DISTINCT ".getNameFieldByLang2("iu.")." AS StudentName, {$selectField} t.OverallScore ";
//$sql = "SELECT DISTINCT ".getNameFieldByLang2("iu.")." AS StudentName, {$selectField} '' ";
$sql .= "FROM {$intranet_db}.IES_SCHEME_STUDENT scheme_s ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON scheme_s.UserID = iu.UserID ";
$sql .= "LEFT JOIN tempScore t ON scheme_s.UserID = t.UserID ";
$sql .= "WHERE scheme_s.SchemeID = {$scheme_id} ";
$sql .= "ORDER BY iu.UserID";
$stage_mark_arr = $li->returnArray($sql);

$exportcontent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($stage_mark_arr, $header);
$lexport->EXPORT_FILE("scheme_score_{$scheme_id}_".time().".csv", $exportcontent, true);

?>