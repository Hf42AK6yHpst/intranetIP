<?php

$task_arr = $objIES->GET_TASK_ARR($stage_id);

for($i=0, $i_max=count($task_arr); $i<$i_max; $i++)
{
  $t_task_id = $task_arr[$i]["TaskID"];
  $t_task_title = $task_arr[$i]["Title"];
  $t_task_seq = $task_arr[$i]["Sequence"];

  $create_disp_field_sql .= "Task{$t_task_id} varchar(255), ";
  
  // Insert query to temporary table (for task status - cell background)
  $insert_tempdisptable_sql = "INSERT INTO tempTaskDisplay (StudentID, ClassNameNum, DisplayName, Task{$t_task_id}) ";
  $insert_tempdisptable_sql .= "SELECT schemestudent.UserID, CONCAT(yc.ClassTitleEN, ' - ', ycu.ClassNumber), CONCAT('<a href=\"index.php?mod=admin&task=scheme_student_stage&scheme_id={$scheme_id}&stage_id={$stage_id}&StudentID=', iu.UserID, '\">', ".getNameFieldByLang2("iu.").", '</a>'), ";
  

  $insert_tempdisptable_sql .= "CONCAT(";
  $insert_tempdisptable_sql .= "IF(IFNULL(handinstatus.haveHandin, 0) = 0, '', 'submited'), ";
  $insert_tempdisptable_sql .= "' ', ";
  $insert_tempdisptable_sql .= "IF(task.Enable = 0, 'disabled', ";      // Task Disabled
  $insert_tempdisptable_sql .= "IF(task.Approval = 0, 'noneed_approve', ";      // Task no need approval
  $insert_tempdisptable_sql .= "IF(taskstudent.Approved = {$ies_cfg["DB_IES_TASK_STUDENT_Approved"]["approved"]}, 'noneed_approve', 'need_approve'))), ";     // Task approved
  $insert_tempdisptable_sql .= "'{$ies_cfg["handin_status_identifier"]}' ";
  $insert_tempdisptable_sql .= ") ";
  
  $insert_tempdisptable_sql .= "FROM {$intranet_db}.IES_TASK AS task, ";
  $insert_tempdisptable_sql .= "{$intranet_db}.IES_SCHEME_STUDENT AS schemestudent ";
  $insert_tempdisptable_sql .= "INNER JOIN {$intranet_db}.INTRANET_USER AS iu ON schemestudent.UserID = iu.UserID ";
  $insert_tempdisptable_sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON iu.UserID = ycu.UserID ";
  $insert_tempdisptable_sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID ";
  $insert_tempdisptable_sql .= "LEFT JOIN tempTaskHandinStatus AS handinstatus ON handinstatus.UserID = schemestudent.UserID AND handinstatus.TaskID = {$t_task_id} ";
  $insert_tempdisptable_sql .= "LEFT JOIN {$intranet_db}.IES_TASK_STUDENT AS taskstudent ON taskstudent.UserID = schemestudent.UserID AND taskstudent.TaskID = {$t_task_id} ";
  
  $insert_tempdisptable_sql .= "WHERE task.TaskID = {$t_task_id} AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." AND schemestudent.SchemeID = {$scheme_id} ";

  $insert_tempdisptable_sql .= "ORDER BY yc.Sequence, ycu.ClassNumber ";
  $insert_tempdisptable_sql .= "ON DUPLICATE KEY UPDATE Task{$t_task_id} = VALUES(Task{$t_task_id})";
  $insert_tempdisptable_sql_arr[] = $insert_tempdisptable_sql;
  
  // Insert query to temporary table (for handin link - dot in cell)
  $insert_tempdisptable_sql = "INSERT INTO tempTaskDisplay (StudentID, ClassNameNum, DisplayName, Task{$t_task_id}) ";
  $insert_tempdisptable_sql .= "SELECT schemestudent.UserID, CONCAT(yc.ClassTitleEN, ' - ', ycu.ClassNumber), CONCAT('<a href=\"index.php?mod=admin&task=scheme_student_stage&scheme_id={$scheme_id}&stage_id={$stage_id}&StudentID=', iu.UserID, '\">', ".getNameFieldByLang2("iu.").", '</a>'), ";
  
  $insert_tempdisptable_sql .= "CONCAT(";
  $insert_tempdisptable_sql .= "'<a href=\"index.php?mod=admin&task=scheme_task&scheme_id={$scheme_id}&stage_id={$stage_id}&TaskID={$t_task_id}#uid', schemestudent.UserID, '\" class=\"', ";
  $insert_tempdisptable_sql .= " IF(sum(handinstatus.newHandin) > 1, 'new_step', IF(sum(handinstatus.newHandin) = 1, 'new_submit', 'view_works')), ";//3: both, 2: step, 1: task, 0: none
  $insert_tempdisptable_sql .= "'\">&nbsp;</a>'";
  $insert_tempdisptable_sql .= ")"; 
  
  $insert_tempdisptable_sql .= "FROM tempTaskHandinStatus AS handinstatus ";
  $insert_tempdisptable_sql .= "INNER JOIN {$intranet_db}.IES_SCHEME_STUDENT AS schemestudent ON handinstatus.UserID = schemestudent.UserID ";
  $insert_tempdisptable_sql .= "INNER JOIN {$intranet_db}.INTRANET_USER AS iu ON schemestudent.UserID = iu.UserID ";
  $insert_tempdisptable_sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON iu.UserID = ycu.UserID ";
  $insert_tempdisptable_sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID ";

  $insert_tempdisptable_sql .= "WHERE handinstatus.TaskID = {$t_task_id} AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." AND schemestudent.SchemeID = {$scheme_id} ";
  $insert_tempdisptable_sql .= "GROUP BY handinstatus.UserID ";
  $insert_tempdisptable_sql .= "ORDER BY yc.Sequence, ycu.ClassNumber ";
  $insert_tempdisptable_sql .= "ON DUPLICATE KEY UPDATE Task{$t_task_id} = CONCAT(Task{$t_task_id}, VALUES(Task{$t_task_id}))";
  $insert_tempdisptable_sql_arr[] = $insert_tempdisptable_sql;
  
  $select_field_sql .= ", IFNULL(temp_td.Task{$t_task_id}, '{$ies_cfg["handin_status_identifier"]}')";
  $html_table_column .= "<th class=\"paper th_task\" title=\"{$t_task_title}\"><a href=\"index.php?mod=admin&task=scheme_task&scheme_id={$scheme_id}&stage_id={$stage_id}&TaskID={$t_task_id}\" class=\"paper\">{$t_task_seq}</a></th>";
}

// Temporary table for storing handin status
// Using Memory engine to increase performance
$sql = "CREATE TEMPORARY TABLE tempTaskHandinStatus ENGINE=MEMORY ";
$sql .= "SELECT taskhandin.UserID, taskhandin.TaskID, ";
$sql .= "IF(count(taskhandin.AnswerID) > 0, 1, 0) AS haveHandin, ";
$sql .= "IF(COUNT(taskhandin.AnswerID) > 0 AND (COUNT(snapshot.SnapshotAnswerID) = 0 OR taskhandin.DateModified > MAX(snapshot.AnswerTime)), 1, 0) AS newHandin ";
$sql .= "FROM {$intranet_db}.IES_TASK_HANDIN taskhandin ";
$sql .= "LEFT JOIN {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT snapshot ON taskhandin.TaskID = snapshot.TaskID AND taskhandin.UserID = snapshot.UserID ";
$sql .= "INNER JOIN {$intranet_db}.IES_TASK task ON taskhandin.TaskID = task.TaskID ";
$sql .= "WHERE task.StageID = {$stage_id} ";
$sql .= "GROUP BY taskhandin.UserID, taskhandin.TaskID ";
$sql .= "ORDER BY taskhandin.UserID, taskhandin.TaskID";
$ldb->db_db_query($sql);

// Insert question handin status
$sql = "INSERT INTO tempTaskHandinStatus ";
$sql .= "SELECT questionhandin.UserID, task.TaskID, ";
$sql .= "IF(count(questionhandin.AnswerID) > 0, 1, 0) AS haveHandin, ";
$sql .= "IF(COUNT(questionhandin.AnswerID) > 0 AND (COUNT(snapshot.SnapshotAnswerID) = 0), 2, 0) AS newHandin ";//no need to reappear when user update task after submitted comments and marks
$sql .= "FROM {$intranet_db}.IES_QUESTION_HANDIN questionhandin ";
$sql .= "RIGHT JOIN {$intranet_db}.IES_STEP step on questionhandin.StepID = step.StepID " ;
$sql .= "INNER JOIN {$intranet_db}.IES_TASK task on step.TaskID = task.TaskID ";
$sql .= "LEFT JOIN {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT snapshot ON task.TaskID = snapshot.TaskID AND questionhandin.UserID = snapshot.UserID ";
$sql .= "WHERE task.StageID = {$stage_id} ";
$sql .= "GROUP BY questionhandin.UserID, step.TaskID ";
$sql .= "ORDER BY questionhandin.UserID, step.TaskID ";
$ldb->db_db_query($sql);

// Calculated display result from above temporary table and store in another temporary table
$sql = "CREATE TEMPORARY TABLE tempTaskDisplay ( ";
$sql .= "RecordOrder int(8) auto_increment, StudentID int(8), ClassNameNum varchar(255), DisplayName text, ";
$sql .= $create_disp_field_sql;
$sql .= "PRIMARY KEY (RecordOrder), UNIQUE KEY StudentID (StudentID)) DEFAULT CHARSET=utf8";
$ldb->db_db_query($sql);

for($i=0, $i_max=count($insert_tempdisptable_sql_arr); $i<$i_max; $i++)
{
  $sql = $insert_tempdisptable_sql_arr[$i];
  $ldb->db_db_query($sql);
}

# Main query
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libies_dbtable($field, $order, $pageNo);
$sql =  "
          SELECT
            temp_td.ClassNameNum,
            temp_td.DisplayName
            $select_field_sql
          FROM
            tempTaskDisplay AS temp_td
        ";
//$rs = $objIES->returnArray($sql);
//debug_pr($rs);
// TABLE INFO
$LibTable->field_array = array("temp_td.RecordOrder");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = 9999;
$LibTable->no_col = 3 + count($task_arr);
$LibTable->table_tag = "<table class=\"common_table_list stage1\">";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list = "<thead>";
$LibTable->column_list .= "<tr>";
$LibTable->column_list .= "<th class=\"num_check sub_row_top\">#</th>";
$LibTable->column_list .= "<th class=\"sub_row_top\">{$i_UserClassNumber}</th>";
$LibTable->column_list .= "<th class=\"sub_row_top\">{$i_general_name}</th>";
$LibTable->column_list .= "<th class=\"sub_row_top\" colspan=\"".count($task_arr)."\" >{$Lang['IES']['Task']}</th>";
$LibTable->column_list .= "</tr>";

$LibTable->column_list .= "<tr>";
$LibTable->column_list .= "<th colspan=\"3\" >&nbsp;</th>";
$LibTable->column_list .= $html_table_column;
$LibTable->column_list .= "</tr>";
$LibTable->column_list .= "</thead>";

$html_legend = <<<HTML
  <img src="/images/2009a/ies/yellow_dot.gif" /> : {$Lang['IES']['HandInCommented']}&nbsp;&nbsp; 
  <img src="/images/2009a/ies/alert_new2.gif" /> : {$Lang['IES']['HandInIn']}
HTML;

$html_export_score_lnk = "";

?>