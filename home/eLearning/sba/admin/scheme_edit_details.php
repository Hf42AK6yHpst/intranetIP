<?php
//modifying By 
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

//GET USER VARIABLE 
$schemeID = intval(trim($schemeID));


if(empty($schemeID) || !is_int($schemeID))
{
	//echo "Invalide access , Empty SCHEME ID<br/>";
    //exit();
    $schemeID = 0;
}
# Create a new interface instance
$linterface = new interface_html();

//GET SCHEME EDIT DETAIL
$folder_path = "{$intranet_root}/file/sba/lang/scheme/{$schemeID}/";
$file_path = $folder_path."scheme_lang.php";

//Get the scheme setup by user if it is exist
//description of the scheme
if(is_file($file_path)){
	include($file_path);
	$html_FolderDesc = stripslashes($Lang_cus['IES']['FolderDesc']);
	$html_TextDesc = stripslashes($Lang_cus['IES']['TextDesc']);
	$html_NoteDesc = stripslashes($Lang_cus['IES']['NoteDesc']);
	$html_WorksheetDesc = stripslashes($Lang_cus['IES']['WorksheetDesc']);
	$html_FAQDesc = stripslashes($Lang_cus['IES']['FAQDesc']);
}
else{

	$html_FolderDesc = $Lang['IES']['FolderDesc_org'];
	$html_TextDesc = $Lang['IES']['TextDesc_org'];
	$html_NoteDesc = $Lang['IES']['NoteDesc_org'];
	$html_WorksheetDesc = $Lang['IES']['WorksheetDesc_org'];
	$html_FAQDesc = $Lang['IES']['FAQDesc_org'];
}

$html_schemeID = $schemeID;  // FOR DISPLAY HIDDEN FORM FIELD VALUE

//intranet_auth();
//intranet_opendb();

$objIES = new libies();
$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$objDB = new libdb();
$li = new libuser($UserID);

$objDB_UI = new libies_ui();


$schemeDetails = $objIES->GET_SCHEME_DETAIL($schemeID); // keep



$schemeTitle =$schemeDetails["Title"];
$html_schemeTitle = $schemeTitle;


//FUNCTION TAB HANDLING
$currentTagFunction = "editSetUp";
$tabArray = $ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"];

$tabAppendParameter = "&schemeID={$schemeID}";

$newTab = $objIES->appendParameterToTab($tabAppendParameter,$tabArray);
$html_functionTab = $objIES->GET_TAB_MENU($currentTagFunction,$newTab);	


// navigation
$nav_arr[] = array($Lang['IES']['Scheme'], "index.php?mod=admin&task=settings_scheme_index");
$nav_arr[] = array($html_schemeTitle, "");




$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";
### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/admin/scheme_edit_details.tmpl.php");
$linterface->LAYOUT_STOP();
//intranet_closedb();


?>
