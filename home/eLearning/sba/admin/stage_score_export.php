<?php
//modifying By 
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
include_once("common.php");

$objIES = new libies();

$objTaskMapper = new SbaTaskMapper();
$tasks = $objTaskMapper->findByStageId($stage_id);
// working students
$students = libies_static::getSchemeStudentArr($scheme_id);

$content = $Lang['IES']['ClassNumber']."\t".$Lang['IES']['StudentName'];
foreach ((array)$tasks as $task){
    //get task attributes
    $content .= "\t".preg_replace("`\s`", " ", $task->getTitle());//prevent csv from crashing, replace \n, \t to space
    $task_ids[]	   = $task->getTaskID();    
}
$content .= "\n";
$task_count = sizeof($tasks);
foreach ($students as $student){
   
    $student_marks = array();
    $libuser = new libuser($student['UserID']);
    
    $class_name = $libuser->ClassName? preg_replace("`\s`", " ", $libuser->ClassName.' - '.$libuser->ClassNumber): '';
    
    $content .= $class_name."\t".preg_replace("`\s`", " ", $student[1]);
    for ($i = 0; $i < $task_count; $i++)
    {

	$displayAnswerArr = getTaskSnapshotAnswer($task_ids[$i], $student['UserID']);//get snapshot id
	$task_score = libies_static::getTaskScoreArr($displayAnswerArr[0]['SnapshotAnswerID']);// get score record
	$content .= "\t".$task_score[0]['score'];
	
    }
    $content .= "\n";
}

header('Content-Disposition: attachment; filename="stage_'.$stage_id.'_score.csv"');
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Description: File Transfer");

echo chr(255).chr(254).mb_convert_encoding($content,'UTF-16LE','UTF-8');

?>
