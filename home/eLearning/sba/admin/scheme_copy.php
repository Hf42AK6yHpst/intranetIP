<?php
//modifying By Bill
include_once($PATH_WRT_ROOT."includes/libuser.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");


//intranet_auth();
//intranet_opendb();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$objIES = new libies();
/*
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
*/
$h_schemeSelection = $objIES->getSchemeSelection($selectedSchemeID="",$skipSchemeID = '',$SchemeType = $sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']);


## Navigation Bar ##
$nav_arr[] = array($Lang['IES']['Scheme'], "index.php?mod=admin&task=settings_scheme_index");
$nav_arr[] = array($Lang['IES']['Copy'], "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);

##Returned error message###
$errorMsg = $_GET["msg"];
if ($errorMsg==1)
{
	$html_message=$Lang['IES']['CopyFailed'];
}


### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/admin/scheme_copy.tmpl.php");
$linterface->LAYOUT_STOP();
//intranet_closedb();
?>
