<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2016-04-08 Pun [ip.2.5.7.4.1] [94670]
 *  - modified commentShow(), hide popup after select items
 * 2011-01-11 Ivan
 * 	- modified setOverallMark()
 * 		1) use the weighted Total Mark instead of the Total Mark for calculation
 * 		2) treat empty string mark as zero mark
 * *******************************************
 */
?>
<link href='<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css' rel='stylesheet' type='text/css'>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script language="JavaScript">
var span_array=new Array();
<?=$html_js_array?>
function addBatchComment(jBtnObj){
  var formObj = $(jBtnObj).parent().parent();
  var formAction = $(formObj).attr("action");
  
  $.ajax({
    url:      "index.php",
    type:     "POST",
    data:     $(formObj).serialize(),
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
 	
                $(jBtnObj).parent().parent().parent().children('.IES_comment_string').html(data);
                $(jBtnObj).parent().hide();
                $(jBtnObj).parent().children(".IES_comment_textarea").val('');
                $(jBtnObj).parent().parent().parent().parent().find("a[name=addComment]").removeAttr("class");
                
              }
  });
}

function stageAllow(jBtnObj)
{
  var student_id = $(jBtnObj).attr("student_id");
  var stage_id = $(jBtnObj).attr("stage_id");

  $.ajax({
    url:      "stage_approval_update.php",
    type:     "POST",
    data:     "stage_id="+stage_id+"&student_id="+student_id,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                if(data == 1) {
                  //alert("Stage approved");
                  $(jBtnObj).replaceWith("<span class=\"IES_allow\"><?=$Lang['IES']['AllowedToNextStep']?></span>");
                }
                else {
                  alert("Fail to approve stage");
                }
              }
  });
}

function sendRedoStage(jBtnObj)
{
  var batch_id = jBtnObj.attr("batch_id");

  $.ajax({
    //url:      "stage_status_update.php",
    url:	  "index.php",
    type:     "POST",
    data:     "mod=ajax&task=ajax_admin&ajaxAction=RedoStage&batch_id="+batch_id+"&batch_status=<?=$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["redo"]?>",
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                if(data == 1) {
                  $(jBtnObj).replaceWith("<span class=\"IES_redo\"><?=$Lang['IES']['RedoSent']?></span>");
                }
                else {
                  alert("FAIL TO SEND REQUEST");
                }
              }
  });
}

function giveMark(jBtnObj)
{
  var formObj = $(jBtnObj).parent().parent();
  var ValInput = true;
  $(jBtnObj).hide();
  
  // Check if radio button has checked in each radio group
  var radioGroup = {};
  // Put names of radio groups in array 
  $(formObj).find("input[type=radio]").each(function(){
    radioGroup[$(this).attr("name")] = true;
  });
  // count number of radio groups
  var radioGroupCount = 0;
  $.each(radioGroup, function() { // then count them
    radioGroupCount++;
  });
  // count checked radio button
  var radioGroupCheckedCount = $(formObj).find("input[type=radio]:checked").length;
  // compare
  if(radioGroupCount != radioGroupCheckedCount) {
    // not all questions answered
    alert(globalAlertMsg18);
    ValInput = false;
    $(jBtnObj).show();
    return false;
  }

  // check overall score
  $(formObj).find("input[type=text]").each(function(){
    var mark = $(this).val();
  
    if(mark == "")
    {
      alert(globalAlertMsg9);
      $(this).focus();
      ValInput = false;
      $(jBtnObj).show();
      return false;
    }
    else if(!IsNumeric(mark))
    {
      alert(globalAlertMsg9);
      $(this).focus();
      ValInput = false;
      $(jBtnObj).show();
      return false;
    }
  });
  
  if(ValInput)
  {
    $.ajax({
      //url:        "stage_mark_update.php",
      url:		  "index.php",
      type:       "POST",
      data:       $(formObj).serialize(),
      beforeSend: function(){
                    $(jBtnObj).parent().children('.update_record').html("");
                  },
      error:      function(xhr, ajaxOptions, thrownError){
                    alert(xhr.responseText);
                  },
      success:    function(data){
                    if(data.length > 0){
                      $(jBtnObj).parent().children('.update_record').html(data);
                    }
                    $(jBtnObj).show();
                  }
    });
  }
}

function isOverallResultField(jObj){
	// Overall result is read only
  return (jObj.attr("readonly"));
}

function setOverallMark(jTextObj){
  var objName = $(jTextObj).attr("name");
  var pattern = /mark\[[^\[\]]*\]/;
  var groupName = pattern.exec(objName);

  var totalMark = 0;
  var totalWeight = 0;
  var finalResultObj;
  var stageMaxScore = 0;

  $("input[name^='"+groupName+"']").each(function(){
  	// If input field is overall result
    if(isOverallResultField($(this)))
    {
      finalResultObj = $(this);
      stageMaxScore = $(this).attr("maxScore");
    }
    // If input field has not been filled => treat as zero mark
    else if($(this).val() == "")
    {
    	var weight = $(this).attr("weight");
    	totalWeight += parseFloat(weight);
    }
    else if(IsNumeric($(this).val()))
    {
    	var subMark = parseFloat($(this).val());
    	var weight = $(this).attr("weight");
    	var maxScore = $(this).attr("maxScore");
    
    	// Retrieve range
    	var range_arr = $(this).siblings("span[name^=range]");
    	var in_range = false;
    	
    	// Check if input score is in range
    	if(range_arr.length > 0)
    	{
	    	$.each(range_arr, function(){
	    		var from = $(this).attr("from");
	    		var to = $(this).attr("to");
	    		
	    		if(from <= subMark && subMark <= to)
	    		{
						in_range = true;
						return false;
					}
				});
			}
			else
			{
				in_range = true;
			}
			
			// Add up weighted marks
			if(in_range)
			{
				// commented on 2011-01-11 by Ivan - Changed to weighted total mark for calculation
				//totalMark += parseFloat($(this).val()) / maxScore;
				totalMark += (parseFloat($(this).val()) / maxScore) * weight;
				totalWeight += parseFloat(weight);
			}
			else
			{
				alert("<?=$Lang['IES']['ScoreOutRange']?>");
				$(jTextObj).val("");
			}
    }
  });
  
  if(IsNumeric(totalMark) && (IsNumeric(totalWeight) && totalWeight > 0))
  {
  	// Round to 2 d.p.
    $(finalResultObj).val(Math.round((totalMark / totalWeight * stageMaxScore)*100)/100);
    return true;
  }
  else
  {
		$(finalResultObj).val("");
		return false;
	}
}


function ShowTableAll(obj){
	
	if($('#'+obj +" option:selected'").val() == "hide")
	{
		$('a[name=showtable]').each(function(){
				$(this).html('(<?=$Lang['Btn']['Show']?>)');
		});
		for(var i = 0; i < span_array.length; i++){
			
			$(".display_"+i+":visible").each(function(){
				$(this).css('display','none');
			});
			$(".column_"+i).each(function(){
			$(this).attr("rowspan",1);	
		})
		}
		
	}
	else{
		$('a[name=showtable]').each(function(){
				$(this).html('(<?=$Lang['Btn']['Hide']?>)');
		});
		for(var i = 0; i < span_array.length; i++){
			
			$(".display_"+i+":hidden").each(function(){
				$(this).css('display','');
			});
			$(".column_"+i).each(function(){
				$(this).attr("rowspan",span_array[i]);	
			})
		}
	}
}


function add_from_commentbank(obj, id, tid){
	var textarea = document.getElementById(tid);
	$(textarea).val($(textarea).val()+"\r\n"+$(obj).text());
	
}

function DeleteBatchComment(BatchCommentID, batchID ,obj){
	if(confirm('<?=$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete?>'))	{
		$.ajax({
		    //url:      "stage_comment_delete.php",
		    url:	  "index.php",
		    type:     "POST",
		    data: 	  "mod=ajax&task=ajax_admin&ajaxAction=DeleteStageComment&BatchCommentID="+BatchCommentID+"&batchID="+batchID,
		    error:    function(xhr, ajaxOptions, thrownError){
		                alert(xhr.responseText);
		              },
		    success:  function(data){
		    			//alert(data);
		                $(obj).parent().parent().html(data);
		              }
	  });
  }
}

function doFilesDownload() {
	$("form[name=form1]").attr("action","index.php");
	$("form[name=form1]").submit();
}

$(document).ready(function(){
  $("a[name=addComment]").click(function(){
    $(this).parent().parent().find("div[name=comment_box]").show();
    $(this).attr("class", "current");
  });
  
  $("a[name=closeComment]").click(function(){
    $(this).parent().parent().hide();
    $(this).parent().parent().parent().parent().find("a[name=addComment]").removeAttr("class");
  });
  
  $("a[name=addMarking]").click(function(){
    $(this).parent().parent().find("div[name=marking_box]").show();
    $(this).attr("class", "current");
  });
  
  $("a[name=closeMarking]").click(function(){
    $(this).parent().parent().hide();
    $(this).parent().parent().parent().parent().find("a[name=addMarking]").removeAttr("class");
  });
  
  $("input[name=giveMark]").click(function(){
    giveMark($(this));
  });

  $("input[name=redo]").click(function(){
    sendRedoStage($(this));
  });

  $("input[name=allow]").click(function(){
    stageAllow($(this));
  });
  
  $("input[name^=mark]").change(function(){
    setOverallMark($(this));
  });
  
  $("select[name=stage_id]").change(function(){
    window.location = "index.php?mod=admin&task=scheme_stage_final&scheme_id=<?=$scheme_id?>&stage_id="+$(this).val();
  });
  
  $(".commentbank_btn1").toggle(function() {
  	  commentShow($(this).parent().children('.IES_select_commentbank'),$(this).attr('answerEncodeNo'));
	}, function() {
	  commentHide($(this).parent().children('.IES_select_commentbank'));
	});
	
	$(".commentbank_btn2").toggle(function() {
  	  $(this).parent().children(".categoryList").slideDown();
	}, function() {
	  $(this).parent().children(".categoryList").slideUp();
	});
	
  $('a[name=showtable]').click(function(){
  	var id = $(this).attr('val');
  	if($(".column_"+id).attr('rowspan') > 1)
	{
		$(this).html('(<?=$Lang['Btn']['Show']?>)');
		$(".display_"+id+":visible").each(function(){
				$(this).css('display','none');
		});
		
		$(".column_"+id).each(function(){
			$(this).attr("rowspan",1);	
		})
		
	}
	else{
		
		$(this).html('(<?=$Lang['Btn']['Hide']?>)');			
		$(".display_"+id+":hidden").each(function(){
			$(this).css('display','');
		});
		$(".column_"+id).each(function(){
			$(this).attr("rowspan",span_array[id]);	
		})
		
	}
  });
});
function commentShow($ParTarget,ParAnswerEncodeNo,ParCategoryID) {
	
	if (ParCategoryID == undefined) {
		ParCategoryID = "";
	}
	
	$.ajax({
		//url:      "ajax/ajax_commentLoadingHandler.php",
		url:	  "index.php",
		type:     "POST",
		data:     "mod=ajax&task=ajax_admin&ajaxAction=GetComment&categoryID="+ParCategoryID+"&answerEncodeNo="+ParAnswerEncodeNo+"&language=<?=$scheme_detail_arr["Language"]?>",
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  
			  h_comment = $(xml).find("h_comment").text();
			  
			  $ParTarget.html(h_comment);
			  $ParTarget.show();
			  
			}
	});
	$ParTarget.parent().children(".categoryList").slideUp();
}
function commentHide($ParTarget) {
	$ParTarget.hide();
}
</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
  <br />
</div>
<!-- navigation end -->

<?=$html_tab_nav?>
<form name="form1" method="POST">
	<input type="hidden" name="stage_id" value="<?=$stage_id?>" />
	<input type="hidden" name="mod" value="admin" />
	<input type="hidden" name="task" value="downloadAllFiles" />
</form>
<div class="content_top_tool">
  <div class="Conntent_tool"><!--a href="#" class="export"><?=$Lang['Btn']['Export']?></a--></div>
  <br style="clear:both" />
</div>
<br style="clear:both" />

<div class="table_board"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
  		<?=$h_showDownloadFiles?>
  	</tr>
    <tr>
      <td valign="bottom">      
        <div class="table_filter">     

          <select id="DisplayController" name="DisplayController" onChange="javascript:ShowTableAll(this.id);">
            <option value="hide"><?=$Lang['IES']['ShowLatestResponse']?></option>
            <option value="show"><?=$Lang['IES']['ShowAllResponse']?></option>
          </select>
        </div>
        <br  style="clear:both"/>
      </td>
      <td valign="bottom">&nbsp;</td>
    </tr>
  </table>
  
  <table class="common_table_list">
    <col style="width:10%" />
    <col style="width:10%" />                     
    <col style="width:25%" />
    <col style="width:25%" />
    <col style="width:25%" />
    <thead>
      <tr>
        <th><?=$Lang['IES']['Class']?> - <?=$Lang['IES']['ClassNumber']?></th>
        <th><?=$Lang['IES']['Student2']?></th>
        <th><?=$Lang['IES']['StudentInputData2']?></th>
        <th class="sub_row_top"><?=$Lang['IES']['Reviews']?></th>
        <th class="sub_row_top"><?=$Lang['IES']['GiveMark']?></th>
      </tr>
    </thead>
    <tbody>
      <?=$html_content?>
    </tbody>
  </table>

  <p class="spacer"></p>
  <br />
  <p class="spacer"></p>
</div>
