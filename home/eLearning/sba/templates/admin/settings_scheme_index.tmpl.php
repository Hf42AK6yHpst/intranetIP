<?
//editing by: Bill
?>
<script language="JavaScript">
$(document).ready(function(){
	$('.delete_dim').live('click', function() {
	        return confirm("<?=$Lang['IES']['DeleteWarning']?>")
	});
});

function js_Goto_New()
{
	location.href = "index.php?mod=admin&task=settings_scheme_new";
}
function js_Goto_Copy()
{
	location.href = "index.php?mod=admin&task=scheme_copy";
}
</script>

<table align="right">
  <tr>
    <td><?=$html_message?></td>
  </tr>
</table>

<form name="searchForm" method="POST">
<br style="clear:both" />
<div class="content_top_tool">

	<div class="Conntent_tool"><?=$h_toolBar?></div>
 
  <div class="Conntent_search">
    <input name="searchText" type="text" value="<?=$html_searchText?>" />
  </div>
</div>
</form>
<br style="clear:both" />

<div class="table_board">

<?php if(!empty($html_default_scheme_list)){ ?>
 <table class="common_table_list">
    <thead>
      <tr>
        <th class="num_check">#</th>
        <th id="setting_default_scheme_list_field_scheme"><?=$Lang['IES']['Default'].' '.$Lang['IES']['Scheme']?></th>
        <th><?=$Lang['IES']['Description']?></th>
        <th id="setting_default_scheme_list_field_steps"><?=$Lang['IES']['Steps']?></th>
        <!--th>&nbsp;</th-->
      </tr>
    </thead>
    <tbody>
      <?=$html_default_scheme_list?>
    </tbody>
  </table>
  <br />
<?php } ?>
  <table class="common_table_list">
    <thead>
      <tr>
        <th class="num_check">#</th>
        <th id="setting_scheme_list_field_scheme"><?=$Lang['IES']['Scheme']?></th>
        <th><?=$Lang['IES']['StudentNumber']?></th>
        <th><?=$Lang['IES']['TeachingTeacher']?></th>
        <th><?=$Lang['IES']['AssessTeacher']?></th>
        <th id="setting_scheme_list_field_steps"><?=$Lang['IES']['Steps']?></th>
        <th id="setting_scheme_list_field_tools">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <?=$html_scheme_list?>
    </tbody>
  </table>
  <br />
  <p class="spacer"></p>
</div>
<script>
$('#setting_default_scheme_list_field_scheme').width($('#setting_scheme_list_field_scheme').width());
$('#setting_default_scheme_list_field_steps').width(8+$('#setting_scheme_list_field_steps').width()+$('#setting_scheme_list_field_tools').width());
//$('#setting_default_scheme_list_field_steps').width(8+$('#setting_scheme_list_field_steps').width());
</script>			
