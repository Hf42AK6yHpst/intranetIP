<?php
/** [Modification Log] Modifying By: Bill
 * *******************************************
 * *******************************************
 */
?>
<script language="JavaScript">
function Check_Form() {
	
}

function checkSchemeNull() {
	var title = Trim($('#scheme_name').val());
	var schemeid = Trim($('#schemeID').val()); // connie part 
	
	if(schemeid=='')
	{
		alert("<?=$Lang['IES']['SelectExistScheme']?>");
		return false;
	}
	if(title=='')
	{
		alert("<?=$Lang['IES']['InputSchemeTitle']?>");
		return false;
	}
	
	$('form#scheme_new').submit();
}


function checkTitleDuplicate(){
	var title = Trim($('#scheme_name').val()); // connie part
	
	$('div.WarningDiv' , 'form#scheme_new').hide();
	
	$.post(
		"ajax/ajax_handler.php", 
		{
			Action:'check_scheme_title',
			schemeTitle: title
		},
		function(ReturnData) {
			if(ReturnData == 1){ //isDuplicate
				$('div#CodeMustNotDuplicateWarningDiv2' , 'form#scheme_new').show();
				$('#scheme_name').focus();
			}
		}
	);
}

function toCancel(){
	$('input[name="task"]').val('settings_scheme_index');
	$('form#scheme_new').submit();
	
	
}

</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<div align="right" style="color:red">
<?=$html_message?>
</div>


<br style="clear:both" />
<form id="scheme_new" method="post" action="index.php">
<?=$html_functionTab?>
<table class="form_table" > <!-- debug-->
  <!--col class="field_title" />
  <col class="field_c" /-->
	<tr>
		<td width="15%"><?=$Lang['IES']['ExistingScheme']?></td>
		<td width="10">:</td>
		<td><?=$h_schemeSelection?></td>
	</tr>
		<tr>
		<td><?=$Lang['IES']['SchemeTitle']?></td>
		<td>:</td>
		<td><input name="r_schemeName" type="text" id="scheme_name" class="textbox" onchange="checkTitleDuplicate()" value="<?=intranet_htmlspecialchars($html_schemeTitle)?>"/>
			<?=$linterface->Get_Form_Warning_Msg('CodeMustNotDuplicateWarningDiv2', $Lang['IES']['SchemeTitleDuplicate'], $Class='WarningDiv')?>
		</td>
		
	</tr>
</table>

<div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
  <p class="spacer"></p>
  <input type="hidden" name="mod" value="admin">
  <input type="hidden" name="task" value="scheme_copy_action">
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_save?>" onclick="checkSchemeNull();" />
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="toCancel()" />
  <p class="spacer"></p>
</div>
</form>
