<?
/* modifying : Isaac
 * 2019-04-18 Isaac:
 * 	added highchart
 *
 */
// include_once($PATH_WRT_ROOT."includes/json.php");
// $json = new JSON_obj();
// $html = $json->decode($html);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title></title>
	<link href="css/text" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/IES_q_pop.css" rel="stylesheet" type="text/css">
	
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/IES_q_pop_thick.css" rel="stylesheet" type="text/css">
	
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="/lang/script.en.js"></script>
<script language="JavaScript" src="/templates/2007script.js"></script>
<script language="JavaScript" src="/templates/script.js"></script>
	<script>
	<!--
	function saveComment() {
		
		$.ajax({
			url:      "index.php",
			type:     "POST",
			data:     "mod=ajax&task=ajax_groupingHandler&Action=saveComment&"+$("form[name=form2]").serialize(),
			async:	  false,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
					  	  result = $(xml).find("result").text();
			 			  if (Number(result) == 1) {
						  	$("#showSuccess").show().fadeOut(5000);
						  } else {
						  	$("#showFailed").show().fadeOut(5000);
						  }
				}
		});
	}

	function saveSurvey() {

		$.ajax({
			url:      "index.php",
			type:     "POST",
			data:     "mod=ajax&task=ajax_groupingHandler&Action=saveSurvey&"+$("form[name=form2]").serialize(),
			async:	  false,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
					  	  result = $(xml).find("result").text();
			 			  if (Number(result) == 1) {
						  	$("#showSuccess").show().fadeOut(5000);
						  } else {
						  	$("#showFailed").show().fadeOut(5000);
						  }
				}
		});
	}

	-->
	</script>
  </head>
  <body>
  <form name="form2" method="POST">
  <div class="form_board thick_box" style='overflow-x:hidden'>
  <div class="ies_q_box q_group">

        	
            <table class="form_table">
            <col class="field_title" />
			<col  class="field_c" />
            <?=$html["title"]?>
            <?=$html["x_question"]?>
            <?=$html["y_question"]?>
            </table>

            <div class="q_option">
            <?=$html["display_table"]?>
            </div>
            
            <table class="form_table">
            <col class="field_title" />
			<col  class="field_c" />
            <?=$html["x_title"]?>
            <?=$html["y_title"]?>
            <?=$html["editUI"]?>
            </table>
	    <?=$html["print_and_export"]?>
 </div>
 

 </div>

</form>

	<? include_once("survey/generateChartScript.php");?>																																																																																																																																																																																																																																																																																																																						
<!--
	<script type="text/javascript">
		var chartNum = <?=count($chartArr)?>;
		
		function ofc_ready(i){
			    
	
		    var id = i[0];
		    setTimeout(function(){
			var image_binary = swfobject.getObjectById("my_chart_Final").get_img_binary();
							
			$.post("survey/uploadFlashImage.php", {image_data: image_binary, chart_id: id}, function(data){
			    $("#my_chart_Final").replaceWith(data);
			    
			    <? if ($callback): ?>
			    if(--chartNum == 0 && typeof(<?=$callback?>)=='function'){
				<?=$callback?>();
			    }
			    <? endif; ?>
			});
		    }, 2000);
		    
		}
		
		
		var dataArr = new Array();
		<?php
			
			foreach( (array)$chartArr as $chartID => $chartJSON ) {
			
				if(count($chartJSON)==0) continue; 
				echo "dataArr[".($chartID)."] = ".$chartJSON->toPrettyString()."\n";
			
			}
		?>
		 

		
		
	</script>
	
	<script type="text/javascript">
		var chartNum = <?=count($chartArr)?>;
		
		for(var k=0; k<chartNum; k++){
			
			
			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart_Final", "<?=$chartWidth?>", "<?=$chartHeight?>", "9.0.0", "", {id: k});
		
		}
		
		//end for
		
		
	</script>
	-->
	<?php  
	echo $highChartJs;
	?>

  </body>
</html>
