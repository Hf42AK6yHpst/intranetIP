<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * 20190409 Isaac
 * Modified changeGraph() to for highchart 
 * 
 * *******************************************
 */
?>
<script type="text/javascript" src="<?=$intranet_httppath?>/templates/2009a/js/sba/SBAIES.js"></script>
<script language="JavaScript">
<!--
function setSurveyQuestion(qStr)
{
  document.form1.qStr.value = qStr;
}

function showHideCurrentTitle(divID) {
	var srcDivElement = '#chkFocusQuestion_'+divID;
	var trgDivElement = '#currentTitleDiv_' + divID;
	
	var trgDivElement2 = '#setQuestionControl_' + divID;


	if ($(srcDivElement).attr("checked")) {
		$(trgDivElement).show();
		$(trgDivElement2).removeClass('check_group').addClass('check_group_checked');
	} else {
		$(trgDivElement).hide();
		$(trgDivElement2).removeClass('check_group_checked').addClass('check_group');
	}
}
// function changeGraph(QuestionNo, i){

// 	$('#my_chart_'+QuestionNo+'parent .my_chart_container').css({'display': 'none'+parseInt($('#my_chart_'+QuestionNo+'parent').width())*i+'px'}).find('img').eq(i).addClass('current_chart');

// }

function changeGraph(QuestionNo, i){
	if(i == 0){
		document.getElementById('my_chart_'+QuestionNo+'_bar_Final').style.display = "block";
		if(document.getElementById('my_chart_'+QuestionNo+'_pie_Final')){
		document.getElementById('my_chart_'+QuestionNo+'_pie_Final').style.display = "none";
		}
	}else{
		document.getElementById('my_chart_'+QuestionNo+'_bar_Final').style.display = "none";
		if(document.getElementById('my_chart_'+QuestionNo+'_pie_Final')){
			document.getElementById('my_chart_'+QuestionNo+'_pie_Final').style.display = "block";
			}
	}
}

function displayStatistics(divID){
	var trgElementId = '#q_result_'+divID;
	var titleElement = '#title_'+divID
	var hidShowCaption = '#displayStatistics_'+divID;

	if($(trgElementId).is(':visible')){
		$(trgElementId).hide();
		$(hidShowCaption).html('<?=$Lang['IES']['ShowRecord']?>');
		
	}else{
		$(trgElementId).show();
		$(hidShowCaption).html('<?=$Lang['IES']['HideRecord']?>');
		
	}
	//displayChartDiv(divID-1);

}
function unCheckSubQuestion(subQuestionStr){

	var subQuestionNo = subQuestionStr.split("##");
	for(var i = 0;i < subQuestionNo.length;i++){
		var _no = subQuestionNo[i];

		var _questionChkBox = '#chkFocusQuestion_'+ _no;
		var _questionTitle = '#currentTitle_'+ _no;

		_questionChkBox  = _questionChkBox.replace('.','\\.');
		_questionTitle  = _questionTitle.replace('.','\\.');

		$(_questionChkBox).attr('checked',false); //uncheck the checkbox
		$(_questionTitle).val(''); //reset the title
	}

}
function selectQuestionChkBox(eleID){
	var chkBoxID = '#chkFocusQuestion_' + eleID;
	chkBoxID = chkBoxID.replace('.','\\.');
	$(chkBoxID).attr('checked',true); //check the checkbox
}
function saveMappings() {
	var firstEmptyElement= '';

	try{
		$("input[id^='chkFocusQuestion_']").each(function(index, domEle){
			if($(this).attr("checked")){
				var _thisEleID = $(this).attr('id');

				//$_thisEleID => chkFocusQuestion_1 , split by "_" and get 1
				var _pointer = _thisEleID.split("_")[1];

				var _currentTitleEleID = '#currentTitle_'+_pointer;
				_currentTitleEleID  = _currentTitleEleID.replace('.','\\.');


				if($(_currentTitleEleID).val() == ''){
					firstEmptyElement = _currentTitleEleID;
					throw false;
				}
				
			}
		});
	}catch(e){
		alert("<?=$Lang['IES']['PleaseFillInCombinationName']?>");
		$(firstEmptyElement).focus();
		return;
	}


	$('input#mod').val('ajax');
	$('input#task').val('ajax_groupingHandler');

	$.ajax({
		url:      "index.php",
		type:     "POST",
		data:     "Action=SAVEINDIVIDUALQUESTION&key=<?=$key?>&"+$("form[name=form1]").serialize(),
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  result = $(xml).find("result").text();

			  if (Number(result) == 1) {
			  	$("input[name=hiddenUpdateResult]").val(1);
//			  	$("form[name=form1]").attr("action","");
				$('input#mod').val('survey');
				$('input#task').val('questionnaireDisplay');
				$("form[name=form1]").submit();
			  } else {
			  	$("input[name=hiddenUpdateResult]").val(0);
			  	$("#showFailed").show().fadeOut(5000);
			  }
		}

	});
}

/*
function saveQuestionTitle(){

	$.post(
		"ajax/ajax_groupingHandler.php", 
		{ 
			Action: 'SAVEINDIVIDUALQUESTION',
			DivID: jsLinkID,
			DBFieldName: 'RecordStatus',
			UpdateValue: TargetRecordStatus
	
		},
		function(ReturnData)
		{
	//		js_Reload_Class_Group_Add_Edit_Table();
			saveQuestionTitleCallBack();				
		//$('#debugArea').html(ReturnData);
		}
	);
}
*/
/*
function saveQuestionTitleCallBack(){
		console.log("saveQuestionTitleCallBack");
}*/
$(document).ready(function(){
	if (<?=$hiddenUpdateResult?>==1) {
		$("#showSuccess").show().fadeOut(5000);
		$("input[name=hiddenUpdateResult]").val(0);
	}
	$('.my_chart_types_radio:checked').click();
	$('#saveChartImage').click();
});
-->
</script>

<form name="form1" method="POST" action="index.php">

<div class="q_header">
<h3><?=$html["survey_title"]?></h3>
<?=$html["tag"]?> 

</div><!-- q_header end -->


<div class="q_content">
    <!-- navigation star -->
		<div class="navigation">
			<table width="100%" style="font-size:15px">
				<tr>
					<td>
						<?=$html_navigation?>
					</td>
					<td>
						<div class="IES_top_tool" style="margin:0">
							<?=$html["print_and_export"]?> <?=$html["export_with_chart"]?>
						</div>
					</td>
				</tr>
			</table>
		</div>
	<!-- navigation end -->
	<!--div style="text-align:right;" ><a href="export2excel.php?SurveyID=<?=$SurveyID?>">Export to Excel</a>&nbsp;&nbsp;&nbsp;<a href="export2word.php?SurveyID=<?=$SurveyID?>">Export to Word</a></div>-->
	<hr/><?=$Lang['IES']['Questionnaire_Display']['string1']?><hr/><br/>
	<span id="showSuccess" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordUpdated())?></span>
	<span id="showFailed" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordFail())?></span>
	<br/><br/>

	<?=$HTML_TABLE?>

  <!-- submit btn start -->
    <!-- submit btn start -->
    <div class="edit_bottom"> 
    <p class="spacer"></p>

	<input type="hidden" name="mod" id="mod" value=""/>
	<input type="hidden" name="task" id="task" value=""/>

	<input type="hidden" name="qStr" />
	<input type="hidden" name="hiddenUpdateResult" />
	<input type="hidden" name="key" value="<?=$key?>" />
	<div id="chartImg"></div>
<?
	if($sba_allowStudentSubmitHandin){
?>
    <input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"  value=" <?=$Lang['IES']['Save']?> " onClick="saveMappings()"/>
<?
	}
?>
			<?=$html["close_button"]?>
             <p class="spacer"></p>
    </div>
  <!-- submit btn end -->
</div> <!-- q_content end -->

</form>