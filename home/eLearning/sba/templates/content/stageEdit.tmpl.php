<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script language="javascript">
function Remove_Task_Rubric(task_rubric_id) {
	
	if(confirm(globalAlertMsg3))
	{
		/*
		$("#taskRubricID").val(taskRubricID);
		$("#task").val("scheme_stage_rubric_delete");
		$("#formStageEdit").submit();
		*/		
		document.location.href = "index.php?mod=content&task=scheme_stage_rubric_delete&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>&taskRubricID="+task_rubric_id;
	}
}
function doSubmitNew() { //2015-06-24 Siuwan check weight and maxscore
	resetDivInnerHtml();
	
	var IsValid = true;
	var Weight,MaxScore,Idx;
	var hasInvalidValue = false;
	var StageName = $('#r_stage_name').val();
	var CriteriaLength = $("input[name='stageCriteriaID[]']").length;
	
	if(StageName=="") {
		$('#stageNameDiv').html('<?=$Lang['SBA']['AlertMsg']['PleaseFillInStageName_InRed']?>').show();
		IsValid = false;
	}
	$('input[name^="r_stageStatus"]:checked').each(function(){
		Idx = $(this).attr("id").replace("r_stageStatus","");
		Weight = $('#stageCriteriaWeight'+Idx).val();
		MaxScore = $('#stageCriteriaMaxScore'+Idx).val();
		if(Weight=="" || MaxScore=="" || (Weight!="" && isNaN(Weight)) || (MaxScore!="" && isNaN(MaxScore))){
			hasInvalidValue = true;
		}
	});
	if(hasInvalidValue) {
		$('#stageRubricDiv').html('<?=$Lang['SBA']['AlertMsg']['PleaseSelectWeightAndMaxScore_InRed']?>').show();
		IsValid = false;
	}
	
	if(IsValid==false) {
		return false;	
	}
}


function doSubmit() {
	/*
	var FormSuccess = true;
	
	var CriteriaLength = $("#stageCriteriaID").length;
	alert(CriteriaLength);
	
	
	if($("#stageCriteriaWeight[]").val() == "") {
		alert("xx");
		FormSuccess = false;	
	}
	
	
	if(FormSuccess) {
		$("#task").val("stageUpdate");
		$("#formStageEdit").submit();
	}
	*/	
	///////////////////////////////////////////////////////
	
	
	resetDivInnerHtml();
	
	var IsValid = true;
	
	var StageName = $('#r_stage_name').val();
	var Weight1 = $('#stageCriteriaWeight1').val();
	var MaxScore1 = $('#stageCriteriaMaxScore1').val();
	var Weight2 = $('#stageCriteriaWeight2').val();
	var MaxScore2 = $('#stageCriteriaMaxScore2').val();
	
	
	if(StageName=="") {
		$('#stageNameDiv').html('<?=$Lang['SBA']['AlertMsg']['PleaseFillInStageName_InRed']?>').show();
		IsValid = false;
	}
	/*
	if(RecordStatus=="undefined") {
		$('#stageRecordStatusDiv').html('<?=$Lang['SBA']['AlertMsg']['PleaseSelectStatus_InRed']?>').show();
		IsValid = false;
	}
	*/
	
	if ($('#r_stageStatus1:checked').val() !== undefined && (Weight1=="" || MaxScore1=="") || ($('#r_stageStatus2:checked').val() !== undefined && (Weight2=="" || MaxScore2==""))) {
		$('#stageRubricDiv').html('<?=$Lang['SBA']['AlertMsg']['PleaseSelectWeightAndMaxScore_InRed']?>').show();
		IsValid = false;
	}
	
	//if(Weight1=="" || MaxScore1=="" || Weight2=="" || MaxScore2=="" || isNaN(Weight1) || isNaN(MaxScore1) || isNaN(Weight2) || isNaN(MaxScore2)) 
	if((Weight1!="" && isNaN(Weight1)) || (Weight2!="" && isNaN(Weight2)) || (MaxScore1!="" && isNaN(MaxScore1)) || (MaxScore2!="" && isNaN(MaxScore2)))
	{
	
		$('#stageRubricDiv').html('<?=$Lang['SBA']['AlertMsg']['PleaseSelectWeightAndMaxScore_InRed']?>').show();
		IsValid = false;
	}
	
	if(IsValid==false) {
		return false;	
	}
}


$(document).ready( function() {
	
});

function resetDivInnerHtml()
{
	$("#stageNameDiv").html("");
	$("#stageRecordstatusDiv").html("");
	$("#stageRubricDiv").html("");	
}

function Delete_Stage(stage_id)
{
	if(confirm("<?=$Lang['SBA']['AlertMsg']['DeleteStageConfirmMsg']?>")) {
		/*
		$("#mod").val("content");
		$("#task").val("stageDeleteUpdate");
		$("#formStageEdit").submit();
		*/
		self.location.href = "index.php?mod=content&task=stageDeleteUpdate&scheme_id=<?=$scheme_id?>&stage_id="+stage_id;
	}
}

</script>


<form name="formStageEdit" id="formStageEdit" method="POST" action="index.php" onSubmit="return doSubmitNew();">

	<div class="stage<?=$h_stage_sequence?>"> <!-- define the stage number, the background will vary-->
    <div class="star"></div>
    
    <? if($sba_allow_edit) { ?>
    <!-- stage right btn start -->
    <!--
        <div class="stage_top_tool">
            <a href="#SBAIES_tea_stage1_coverpage_edit_lightbox.html" class="move" title="<?=$Lang['SBA']['Rearrange']?>"><span><?=$Lang['SBA']['Rearrange']?></span></a>
            <a href="#" class="delete" title="'.$Lang['IES']['Delete'].'"><span><?=$Lang['IES']['Delete']?></span></a>
        </div>
    -->
    <!-- stage right btn end -->
    <? } ?>                
    <div class="cover_wrap">
                   
    	<?=$h_main_content;?>
       	
       	 <br class="clear" />
       	 <!--
       <span class="tabletextrequire">#</span> <?=$Lang['IES']['MaxScoreRestrictedByRubric']?>  
		-->
		
       	<!--
       	<div class="task_writing_bottom"><div>
                                      
                </div></div>
       	-->
       	
       	<br style="clear:both;"/>
       	<br style="clear:both;"/>
       	
	</div>
	
		
    <br class="clear" />

    		<div class="edit_bottom_main">
			<br class="clear" />
			<p class="spacer"></p>
			
			
			<input name="submit" type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'");return document.MM_returnValue" value="<?=$Lang['SBA']['Submit']?>" />
			
			<?=$extraButton?>
			
			<input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onclick="MM_goToURL('parent','index.php?mod=content&task=stageInfo&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>');return document.MM_returnValue" value="<?=$Lang['IES']['Cancel']?> " />
			
			
			<input type="hidden" name="mod" id="mod" value="content">
			<input type="hidden" name="task" id="task" value="stageUpdate">
				
			
			<p class="spacer"></p>
		</div>
		
    <input type="hidden" name="scheme_id" id="scheme_id" value="<?=$scheme_id?>"/>
    <input type="hidden" name="stage_id" id="stage_id" value="<?=$stage_id?>" />
    <input type="hidden" name="taskRubricID" id="taskRubricID" value="" />
    
    
</form>    