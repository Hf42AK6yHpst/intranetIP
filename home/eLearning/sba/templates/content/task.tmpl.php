<?
/** [Modification Log] Modifying By: Paul
 * 2017-07-05 Paul [ip.2.5.8.7.1]: 
 * Modified function get_task_stdAnswer_handin_form(), renew the error handling method to prevent keeping Block_element running forever upon error in ajax
 * 2016-04-11 (Siuwan) [ip.2.5.7.4.1] (Case:#Q94669)
 * modified js function step_edit_form_submit(),get_task_intro(),task_intro_edit_form_submit(),get_step(), added target=_BLANK for fckeditor hyperlink content
 * 2012-08-17 Mick:
 * New functions for tablePlanner questions
 * 2012-08-14 Mick:
 * New function step_stdAnswer_handin_form_reset()
 */
$_SESSION['sba_task_editor']="";
echo $h_main_content;
?>
<script src="/templates/jquery/jquery.blockUI.js" type="text/javascript"></script>
<script src="/templates/jquery/ui/ui.sortable_dd.js" type="text/javascript"></script>
<script src="/templates/jquery/ui/ui.sortable_dd.js" type="text/javascript"></script>
<script src="/templates/jquery/plupload/plupload.full.js" type="text/javascript"></script>
<script>
lang_insertSbaElement = '<?=$Lang['IES']['InsertSbaElement']?>';
<?php if($sba_allow_edit && count($tasks)==1){ /* Automatically show create new task form if no task is created */ ?>
	$(document).ready(function(){
		get_task_edit_form();
	});
<?php } ?>

/* Global Object for indicating the page is editing or not */
var const_editCheck_type_task 		= 'task';
var const_editCheck_type_taskHandin = 'taskHandin';
var const_editCheck_type_introStep  = 'introStep';
var const_editCheck_type_thickbox   = 'thickbox';

var editCheck = (function(){
	var type 	= '';
	var task_id = 0;
	
	return{
		IsEditing   : function(parType, parTaskID){
						parType   = parType || '';
						parTaskID = parTaskID || 0;
						
						if(parType=='' && parTaskID==0){
							return !(type=='' && task_id==0);
						}
						else{
							return (parType==type && parTaskID==task_id);
						}
					  },
	
		RequestEdit : function(parType, parTaskID, msg){
						 msg = msg || '<?=$Lang['SBA']['StillInEditModeAlertMsg']?>';
						 
						 if(type=='' && task_id==0){
						 	type 	= parType;
						 	task_id = parTaskID;
						 	return true;
						 }
						 else{
						 	alert(msg);
						 	return false;
						 }
					  },
	
		FinishEdit  : function(){
						 type 	 = '';
						 task_id = 0;
					  }
	}
})();





/****************************\
* Edit Task Related Function *
\****************************/

var sba_task_content_html = new Array();

function get_task(task_id){
	task_id = task_id || 0;
	
	var task_suffix = task_id? task_id : 'new';
	
	$.ajax({
			url:      "index.php?mod=ajax&task=ajax_task",
			type:     "POST",
			async:    false,
			data:     "ajaxAction=GetTask&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success	: function(xml){
						var resultTaskTitleUI = $(xml).find("TaskTitleUI").text();
						var resultUI 		  = $(xml).find("UI").text();
						
						$('#sba_task_header_'+task_suffix).html(resultTaskTitleUI);
						
						if(task_id){
							/* Save the corresponding Task Content UI in sba_task_content_html[task_id]*/
							sba_task_content_html[task_id] = '';
						}
						
						/* Update the corresponding task UI */
						$('#sba_task_'+task_id).replaceWith(resultUI);
					  }
  	});
}

function get_task_edit_form(task_id){
	task_id = task_id || 0;
	
	var task_suffix = task_id? task_id : 'new';
	
	if(editCheck.RequestEdit(const_editCheck_type_task, task_id)){
		$.ajax({
				url		: "index.php?mod=ajax&task=ajax_task",
				type	: "POST",
				async	: false,
				data	: "ajaxAction=TaskEditForm&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id,
				error	: function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success	: function(xml){
							var resultTaskTitleUI = $(xml).find("TaskTitleUI").text();
							var resultUI 		  = $(xml).find("UI").text();
							
							if(task_suffix=='new'){
								/* Change to New Task Title Button back to a Div */
								$('#sba_task_header_'+task_suffix).html(resultTaskTitleUI);
							}
							else{
								/* Save the corresponding Task Content UI in sba_task_content_html[task_id]*/
								sba_task_content_html[task_id] = $('#sba_task_content_'+task_suffix).html();
							}
							
							/* Display the form at Task Content Box */
							$('#sba_task_content_'+task_suffix).html(resultUI);
						  }
	  	});
	}
}

function task_edit_form_submit(task_id){
	task_id = task_id || 0;
	
	var task_suffix = task_id? task_id : 'new';
	
	/* Hide all warning box */
	$('.WarningDiv').hide();
	
	var formObj = document.getElementById('sba_task_editform_'+task_suffix);
	
	if(formObj.r_task_Title.value==''){
		$('#sba_task_editform_'+task_suffix+'_Title_ErrorDiv').show();
		return false;
	}
	else if(!formObj.r_task_Enable[0].checked && !formObj.r_task_Enable[1].checked ){
		$('#sba_task_editform_'+task_suffix+'_Enable_ErrorDiv').show();
		return false;
	}
	
	$.ajax({
			url		: "index.php?mod=ajax&task=ajax_task",
			type	: "POST",
			data	: 'ajaxAction=TaskEditUpdate&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>&' + $("#sba_task_editform_"+task_suffix).serialize(),
			async	: false,
			error	: function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
				   	  },
			success : function(xml){
						var resultTaskID = $(xml).find("TaskID").text();
						var resultUI 	 = $(xml).find("UI").text();
						var responseMsg  = $(xml).find("ResponseMsg").text();
						
						/* Clear the Content box first */
						$('#sba_task_content_'+task_suffix).html('');
						
						if(task_id){
							/* Update the corresponding task UI */
							$('#sba_task_'+task_id).replaceWith(resultUI);
							
							/* Clear the saved html */
							sba_task_content_html[task_id] = '';
						}
						else{
							/* Insert the newly created task UI before the new task form */
							$('#sba_task_'+task_suffix).before(resultUI);
						}
						
						/* Call the cancel to do the clean up */
						task_edit_form_cancel(task_id);
						
						/* Edit Task Introduction */
						if(task_suffix=='new'){
							get_task_intro_edit_form(resultTaskID);
						}
						
						/* Check no of task and show task reorder btn */
						check_tasks_reorder_btn();
						
						/* Display the response message */
						show_response_msg(resultTaskID, responseMsg);
					   }
	});
}

function task_edit_form_cancel(task_id){
	task_id = task_id || 0;
	
	var task_suffix = task_id? task_id : 'new';
	
	editCheck.FinishEdit();
	
	/* If sba_task_content_html[task_id] store some html (i.e. not callback from task update success), load the saved html back */
	if(typeof(sba_task_content_html[task_id])!='undefined' && sba_task_content_html[task_id]!=''){
		$('#sba_task_content_'+task_suffix).html(sba_task_content_html[task_id]);
		sba_task_content_html[task_id] = '';
	}
	
	if(task_suffix=='new'){
		/* Clear the New Task Form */
		$('#sba_task_content_'+task_suffix).html('');
		
		/* Change to New Task Title back to a Button */
		$('#sba_task_header_'+task_suffix).html("<?=$sba_libSba_ui->getTaskHeaderTitleUI(null, true)?>");
	}
}

function task_delete(task_id){
	if(confirm('<?=$Lang['SBA']['AreYouSureDeleteTask']?>')){
		$.ajax({
				url		: "index.php?mod=ajax&task=ajax_task",
				type	: "POST",
				async	: false,
				data	: "ajaxAction=DeleteTask&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id,
				error	: function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success	: function(xml){
							editCheck.FinishEdit();
							$('#sba_task_'+task_id).attr('isDel','1').hide();
							check_tasks_reorder_btn();
						  }
	  	});
	}
}





/*****************************************\
* Edit Task Introduction Related Function *
\*****************************************/

var sba_task_content_introStep_content_html = new Array();

function get_task_intro(task_id){
	
	Block_Element('sba_task_content_introStep_'+task_id);
	
	
	    
	$.ajax({
			url:      "index.php?mod=ajax&task=ajax_task",
			type:     "POST",
			data:     "ajaxAction=TaskIntroduction&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
						var resultUI = $(xml).find("UI").text();
						
						/* Display the Task Introduction */
						$('#sba_task_content_introStep_content_'+task_id).html(resultUI);
						/* Add target=_BLANK for fckeditor hyperlink content */
						$("div.task_box_content,div.task_content").find("a:not([class])").attr("target","_BLANK");
						UnBlock_Element('sba_task_content_introStep_'+task_id);
					   }
	});
	    
	
	
}

function get_task_intro_edit_form(task_id){
	if(editCheck.RequestEdit(const_editCheck_type_introStep, task_id)){
		Block_Element('sba_task_content_introStep_'+task_id);
		
		if($('#sba_task_content_introStep_editIntro_'+task_id).html()){
			$.ajax({
					url  	: "index.php?mod=ajax&task=ajax_task",
					type  	: "POST",
					data	: "ajaxAction=GetTaskIntroductionRaw&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id,
					error 	: function(xhr, ajaxOptions, thrownError){
								alert(xhr.responseText);
						  	  },
					success	: function(xml){
								var resultTaskIntro = $(xml).find("TaskIntro").text();
								var resultTaskIntroAttachment = $(xml).find("TaskIntroAttachment").text();
							
								/* Hide Other Content */
								$('#sba_task_content_introStep_contentBox_'+task_id+' > div:not(#sba_task_content_introStep_editIntro_'+task_id+')').hide();
								
								/* Display the Task Introduction Form */
								$('#sba_task_content_introStep_editIntro_'+task_id).show();
								
							
								editor_setHTML('r_task_Introduction_'+task_id, resultTaskIntro);
								
								UnBlock_Element('sba_task_content_introStep_'+task_id);
						   	  }
	  		});
		}
		else{
			$.ajax({
					url  	: "index.php?mod=ajax&task=ajax_task",
					type  	: "POST",
					data	: "ajaxAction=TaskIntroductionForm&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id,
					error 	: function(xhr, ajaxOptions, thrownError){
								alert(xhr.responseText);
						  	  },
					success	: function(xml){
								var resultUI = $(xml).find("UI").text();
							
								/* Hide Other Content */
								$('#sba_task_content_introStep_contentBox_'+task_id+' > div:not(#sba_task_content_introStep_editIntro_'+task_id+')').hide();
								
								/* Display the Task Introduction Form */
								$('#sba_task_content_introStep_editIntro_'+task_id).html(resultUI).show();
								UnBlock_Element('sba_task_content_introStep_'+task_id);
						   	  }
	  		});
		}
	}
}



function removeFile(obj,ParFileHashName) {
	if (confirm('<?=$Lang['IES']['AreYouSureYouWouldLikeToDeleteThisFile']?>')) {
		$.ajax({
			url:      'index.php?mod=ajax&task=ajax_TaskFileHandler',
			type:     'POST',
			data:     'Action=RemoveFile&fileHashName='+ParFileHashName,
			async:	  false,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
				  if (Number(xml) == 1) {
				  	$(obj).parents('tr:first').remove();
				  } else {
				  	alert('<?=$Lang['IES']['UploadFailed']?>');
				  }
				}
		});
	}
}

function task_attachment_upload(task_id){
	myFrame = top.frames['sba_task_iframe_'+task_id];
	with(myFrame.document){
		open();
		write('<form enctype="multipart/form-data" id="hiddenForm1" method="POST" action="index.php?mod=admin&task=uploadTaskFiles"><input type="hidden" id="task_id" name="task_id" value="'+task_id+'"><input type="hidden" id="scheme_id" name="scheme_id" value="<?=$sba_libScheme->getSchemeID()?>"></form>');
		close();
	}
	var iframeContent = '';
	var cloned = ''; 
	$('#sba_task_intro_editform_'+task_id+' :file').each(function(){
		cloned = $(this).clone(true);
		cloned.insertAfter($(this)); 
		$('#sba_task_iframe_'+task_id).contents().find('form').append($(this));
	});
}


function task_intro_edit_form_submit(task_id){
    
	var oEditor = FCKeditorAPI.GetInstance('r_task_Introduction_'+task_id);
	oEditor.UpdateLinkedField(); // Update the content in FCK Editor to corresponding textarea field
	
	var formObj = document.getElementById('sba_task_intro_editform_'+task_id);
	formObj.r_task_Introduction.value = $('#r_task_Introduction_'+task_id).val();
    
	/* Confirm the changes in attach files */
	confirm_attach_files();
	$.ajax({
			url:      "index.php?mod=ajax&task=ajax_task",
			type:     "POST",
			data:     "ajaxAction=TaskIntroductionUpdate&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id+"&"+$("#sba_task_intro_editform_"+task_id).serialize(),
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
						var resultUI 	 = $(xml).find("UI").text();
						var responseMsg  = $(xml).find("ResponseMsg").text();
						var resultAttachmentUI = $(xml).find("AttachmentUI").text();
						
						/* Update the corresponding Task Introuction UI*/						
						$('#sba_task_content_introStep_content_'+task_id).html(resultUI);
						
						
						/* Call the cancel to do the clean up */
						
						task_intro_edit_form_cancel(task_id, true);
						
						/* Display the response message */
						show_response_msg(task_id, responseMsg);
						/* Add target=_BLANK for fckeditor hyperlink content */
						$("div.task_box_content,div.task_content").find("a:not([class])").attr("target","_BLANK");
						UnBlock_Element('sba_task_content_introStep_'+task_id);
					   }
  	});
}

function task_intro_edit_form_cancel(task_id, keep_file){
	editCheck.FinishEdit();
	
	cancel_attach_files();
	
	/* Hide Other Content */
	$('#sba_task_content_introStep_contentBox_'+task_id+' > div:not(#sba_task_content_introStep_content_'+task_id+')').hide();
								
	/* Display the Task Introduction Form */
	$('#sba_task_content_introStep_content_'+task_id).show();
	
	editor_setHTML('r_task_Introduction_'+task_id, '');
}





/****************************\
* Edit Step Related Function *
\****************************/

function get_step(task_id, step_id){
	Block_Element('sba_task_content_introStep_'+task_id);
	
	$.ajax({
			url:      "index.php?mod=ajax&task=ajax_step",
			type:     "POST",
			data:     "ajaxAction=GetStep&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id+"&step_id="+step_id,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
						var resultUI = $(xml).find("UI").text();
						
						/* Update the corresponding Step UI*/
						$('#sba_task_content_introStep_content_'+task_id).html(resultUI);
						/* Add target=_BLANK for fckeditor hyperlink content */
						$("div.task_box_content,div.task_content").find("a:not([class])").attr("target","_BLANK");
						UnBlock_Element('sba_task_content_introStep_'+task_id);
					   }
  	});
}

function get_step_edit_form(task_id, step_id){
	step_id = step_id || 0;
	
	if(editCheck.RequestEdit(const_editCheck_type_introStep, task_id)){
		Block_Element('sba_task_content_introStep_'+task_id);
		
		if($('#sba_task_content_introStep_editStep_'+task_id).html()){
			$.ajax({
					url  	: "index.php?mod=ajax&task=ajax_step",
					type  	: "POST",
					data	: "ajaxAction=GetStepEditRaw&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id+"&step_id="+step_id,
					error 	: function(xhr, ajaxOptions, thrownError){
								alert(xhr.responseText);
						  	  },
					success	: function(xml){
								var resultStepTitle       = $(xml).find("StepTitle").text();
								var resultStepDescription = $(xml).find("StepDescription").text();
								var resultStepSequence	  = $(xml).find("StepSequence").text();
								var resultStepAttachmentDisplay  = $(xml).find("StepAttachmentDisplay").text();
								var resultStepToolCaption = $(xml).find("StepToolCaption").text();
								var resultStepToolDisplay = $(xml).find("StepToolDisplay").text();
								
							
								/* Hide Other Content */
								$('#sba_task_content_introStep_contentBox_'+task_id+' > div:not(#sba_task_content_introStep_editStep_'+task_id+')').hide();
								
								/* Display the Task Introduction Form */
								$('#sba_task_content_introStep_editStep_'+task_id).show();
								
								var formObj = document.getElementById('sba_step_editform_'+task_id);
								
								/* Show Delete Button or not */
								if(step_id){
									formObj.deleteBtn.style.display = '';
								}
								else{
									formObj.deleteBtn.style.display = 'none';
									
								}
								
								/* Set the Step Tool Caption */
								$('#sba_step_editform_'+task_id+'_toolCaption').html(resultStepToolCaption);
								
								/* Set the Step Tool Display */
								$('#sba_step_editform_'+task_id+'_toolDisplay').html(resultStepToolDisplay);
								
								if(resultStepToolCaption && resultStepToolDisplay){
									$('#sba_step_editform_'+task_id+'_tool').show();
									$('#sba_step_editform_'+task_id+'_selectTools').hide();
								}
								
								$('#sba_step_editform_'+task_id+' .attachments').html(resultStepAttachmentDisplay);
								
								/* Set the Step ID */
								formObj.step_id.value = step_id;
								
								/* Set the Step Title */
								formObj.r_step_Title.value = resultStepTitle;
								
								/* Set the Step Sequence */
								formObj.r_step_Sequence.value = resultStepSequence;

								/* Set the Step Description */
								editor_setHTML('r_step_Description_'+task_id, resultStepDescription);
								
								UnBlock_Element('sba_task_content_introStep_'+task_id);
						   	  }
	  		});
		}
		else{
			$.ajax({
					url		: "index.php?mod=ajax&task=ajax_step",
					type	: "POST",
					data	: "ajaxAction=GetStepEditForm&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id+"&step_id="+step_id,
					error	: function(xhr, ajaxOptions, thrownError){
								alert(xhr.responseText);
							  },
					success	: function(xml){
								var resultUI = $(xml).find("UI").text();
								
								/* Hide Other Content */
								$('#sba_task_content_introStep_contentBox_'+task_id+' > div:not(#sba_task_content_introStep_editStep_'+task_id+')').hide();
								
								/* Display the form at Introduction Step Content Box */
								$('#sba_task_content_introStep_editStep_'+task_id).html(resultUI).show();
								
								UnBlock_Element('sba_task_content_introStep_'+task_id);
							  }
		  	});
		}
	}
}

function step_edit_form_submit(task_id){
	/* Hide all warning box */
	$('.WarningDiv').hide();
	var oEditor = FCKeditorAPI.GetInstance('r_step_Description_'+task_id);
	oEditor.UpdateLinkedField(); // Update the content in FCK Editor to corresponding textarea field
	
	var formObj = document.getElementById('sba_step_editform_'+task_id);
	formObj.r_step_Description.value = $('#r_step_Description_'+task_id).val();
	
	if(formObj.r_step_Title.value==''){
		$('#sba_step_editform_'+task_id+'_Title_ErrorDiv').show();
		return false;
	}
	else if(typeof(formObj.r_step_QuestionType)=='undefined'){
		$('#sba_step_editform_'+task_id+'_TmpQuestionType_ErrorDiv').show();
		return false;
	}
	else if(typeof(step_tool_validate_fail)=='function' && step_tool_validate_fail()){
		return false;
	}
	
	Block_Element('sba_task_content_introStep_'+task_id);
	/* Confirm the changes in attach files */
	confirm_attach_files();
	
	    	    
	$.ajax({
		url		: "index.php?mod=ajax&task=ajax_step",
		type	: "POST",
		data	: 'ajaxAction=StepEditUpdate&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>&task_id='+task_id+'&'+$("#sba_step_editform_"+task_id).serialize(),
		error	: function(xhr, ajaxOptions, thrownError){
				alert(xhr.responseText);
			  },
		success : function(xml){
				var resultStepID	= $(xml).find("StepID").text();
				var resultStepTitle = $(xml).find("StepTitle").text();
				var resultUI 		= $(xml).find("UI").text();
				var responseMsg     = $(xml).find("ResponseMsg").text();
				
				/* Update the corresponding step UI */
				$('#sba_task_content_introStep_content_'+task_id).html(resultUI);

				/* Reset all the tabMenu Button to not current stage */
				$('#sba_task_content_introStep_tabMenu_'+task_id+' a').removeClass('current');
				
				if(formObj.step_id.value=='' || formObj.step_id.value==0){
					$('#sba_task_content_introStep_tabMenu_addStepBtn_'+task_id).before(resultStepTitle);
				}
				else{
					$('#sba_task_content_introStep_tabMenu_stepBtn_'+resultStepID).replaceWith(resultStepTitle);
				}
				
				
				/* Call the cancel to do the clean up */
				step_edit_form_cancel(task_id, resultStepID);
				
				/* Check no of step and show step reorder btn */
				check_steps_reorder_btn(task_id);
				
				/* Check no of step and modify add step btn */
				check_add_step_btn(task_id);
				
				/* Display the response message */
				show_response_msg(task_id, responseMsg);
				
				/* Add target=_BLANK for fckeditor hyperlink content */
				$("div.task_box_content,div.task_content").find("a:not([class])").attr("target","_BLANK");
				
				UnBlock_Element('sba_task_content_introStep_'+task_id);
			  }
	});
		
	
	
	
	
}


function step_edit_form_cancel(task_id, step_id){
	editCheck.FinishEdit();
	
	cancel_attach_files();
	step_id = step_id || 0;
	
	var formObj = document.getElementById('sba_step_editform_'+task_id);

	if(!step_id && !parseInt(formObj.step_id.value)){
		$(formObj).find('.attachments_edit .delete_row').click();
		task_content_introStep_tabMenu_onclick(task_id, 1, 0, 0);
		
	}
	
	/* Hide Other Content */
	$('#sba_task_content_introStep_contentBox_'+task_id+' > div:not(#sba_task_content_introStep_content_'+task_id+')').hide();
								
	/* Display the Task Introduction Form */
	$('#sba_task_content_introStep_content_'+task_id).show();
	
	step_edit_form_reset(task_id);
}

function step_edit_form_reset(task_id){
	var formObj = document.getElementById('sba_step_editform_'+task_id);
	
	/* Reset The Form */
	$('.WarningDiv').hide();
								
	/* Set the Step Tool Caption */
	$('#sba_step_editform_'+task_id+'_toolCaption').html('');
								
	/* Set the Step Tool Display */
	$('#sba_step_editform_'+task_id+'_toolDisplay').html('');
	
	/* Set the Step ID */
	formObj.step_id.value = '';
	
	/* Set the Step Title */
	formObj.r_step_Title.value = '';
		
	/* Set the Step Sequence */
	formObj.r_step_Sequence.value = '';
	
	/* Set the Step Description */
	editor_setHTML('r_step_Description_'+task_id, '');
	
	delete_step_edit_form_tool(task_id);
}

function get_step_edit_form_tool(task_id){
	var form_id	= 'sba_step_editform_'+task_id;
	
	var QuestionType = $('#'+form_id+'_TmpQuestionType').val();
		
	if(QuestionType){
		$('#'+form_id+'_tool_loadingImg').show();
		
		$.ajax({
				url		: "index.php?mod=ajax&task=ajax_step",
				type	: "POST",
				data	: 'ajaxAction=GetNewStepHandin&task_id='+task_id+'&r_step_QuestionType=' + QuestionType,
				async	: false,
				error	: function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success : function(xml){
							var resultUI = $(xml).find("UI").text();
							
							$('#'+form_id+'_tool').show();
							$('#'+form_id+'_selectTools').hide();
								
							$('#'+form_id+'_toolCaption').html($('#'+form_id+'_TmpQuestionType option:selected').html());
							$('#'+form_id+'_toolDisplay').html(resultUI);
								
							/* Reset the Question Type */
							$('#'+form_id+'_TmpQuestionType').val('');
							$('#'+form_id+'_tool_loadingImg').hide();
						  }
		});
	}
	else{
		alert("<?=$Lang['SBA']['PleaseSelectAnswerTool']?>");
	}
}

function delete_step_edit_form_tool(task_id){
	
	
	var form_id	= 'sba_step_editform_'+task_id;
	
	$('#'+form_id+'_tool').hide();
	$('#'+form_id+'_selectTools').show();
								
	$('#'+form_id+'_toolCaption').html('');
	$('#'+form_id+'_toolDisplay').html('');
								
	/* Reset the Question Type */
	$('#'+form_id+'_TmpQuestionType').val('');
}

function step_delete(task_id){
	if(confirm('<?=$Lang['SBA']['AreYouSureDeleteStep']?>')){
		var step_id = document.getElementById('sba_step_editform_'+task_id).step_id.value;
		
		$.ajax({
				url		: "index.php?mod=ajax&task=ajax_step",
				type	: "POST",
				data	: "ajaxAction=DeleteStep&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id+"&step_id="+step_id,
				error	: function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success	: function(xml){
							var responseMsg = $(xml).find("ResponseMsg").text();
							
							editCheck.FinishEdit();
							
							task_content_introStep_tabMenu_onclick(task_id, 1, 0, 0);
							
							/* Hide Other Content */
							$('#sba_task_content_introStep_contentBox_'+task_id+' > div:not(#sba_task_content_introStep_content_'+task_id+')').hide();
								
							/* Display the Task Introduction Form */
							$('#sba_task_content_introStep_content_'+task_id).show();
							
							$('#sba_task_content_introStep_tabMenu_stepBtn_'+step_id).replaceWith('');
							
							step_edit_form_reset(task_id);
							
							check_steps_reorder_btn(task_id);
							
							/* Check no of step and modify add step btn */
							check_add_step_btn(task_id);
							
							/* Display the response message */
							show_response_msg(task_id, responseMsg);
						  }
	  	});
	}
}


/***********************************************\
* Introduction tablePlanner Step edit Related Function *
\***********************************************/

function tablePlanner_uiHelper(e, ui){//for jqueryui sortable
	ui.children().each(function() {
		$(this).width($(this).width());
	});
	return ui;
};
			
function tablePlanner_addRow(){
	
	var row='<tr><td>'+(tablePlanner_rowNum+1)+'</td>';
	for (i=0; i<tablePlanner_columnNum; i++){
		row+='<td><input type="text" style="width:98%;" name="r_stdAnswer[]"/></td>';
	}
	row+='<td><a class="delete_row" onclick="tablePlanner_deleteRow(this)" title="Delete" href="javascript: void(0);"></a></td></tr>';
	tablePlanner_rowNum++;
	
	$('#tablePlanner_display tbody').append(row);
}

function tablePlanner_resetRow(){
	tablePlanner_rowNum=1;
	$('#tablePlanner_display tbody tr').not(':first').remove();
}

function tablePlanner_deleteRow(element){
	

	if ($('#tablePlanner_display tr').length <=3){
		$('#tablePlanner_error').html('*You must answer at least one row');
	}else{

		$(element).parent().parent().remove();
		tablePlanner_reNumberRows();
		
	}
}

function tablePlanner_reNumberRows(){
	$('#tablePlanner_display tbody tr').each(
		function(index){
			tablePlanner_rowNum=index+1;
			$(this).find('td:first').html(tablePlanner_rowNum);
		}
	);
}

function tablePlanner_addColumn(){
	$('#addRowTh').before('<th><input type="text" style="width:80%;" name="r_step_Question[]"/><a class="delete_row" onclick="tablePlanner_deleteColumn(this)" title="Delete" href="javascript: void(0);"></a></th>');

}

function tablePlanner_deleteColumn(element){
	if ($('#tablePlanner_edit th').length <= 3){

		$('#tablePlanner_error').html('*The table must have least one column');
	}else{
		$(element).parent().remove();
	}
}


/***********************************************\
* Introduction / Step Tab Menu Related Function *
\***********************************************/

function task_content_introStep_tabMenu_onclick(task_id, isIntro, isNewStep, step_id){
	task_id = task_id || 0;
	
	var task_suffix = task_id? task_id : 'new';
	
	if(isNewStep){
		if(!editCheck.IsEditing()){
			sba_task_content_introStep_content_html[task_id] = '';
			$('#sba_task_content_introStep_tabMenu_'+task_suffix+' a').removeClass('current');
			$('#sba_task_content_introStep_tabMenu_addStepBtn_'+task_suffix).addClass('current');
			get_step_edit_form(task_id, 0);
			
			/* If Introduction / Step Content Box is collapsed, expand it */
			if($('#sba_task_content_introStep_content_collapse_'+task_id).css('display')!='none'){
				task_content_introStep_content_showHide(task_id);
			}
		}
		else{
			alert('<?=$Lang['SBA']['StillInEditModeAlertMsg']?>');
		}
	}
	else{
		if(!editCheck.IsEditing(const_editCheck_type_introStep, task_id)){
			sba_task_content_introStep_content_html[task_id] = '';
			$('#sba_task_content_introStep_tabMenu_'+task_suffix+' a').removeClass('current');
		
			if(isIntro){
				$('#sba_task_content_introStep_tabMenu_introBtn_'+task_suffix).addClass('current');
				get_task_intro(task_id);
			}
			else if(step_id){
				$('#sba_task_content_introStep_tabMenu_stepBtn_'+step_id).addClass('current');
				get_step(task_id, step_id);
			}
		
			/* If Introduction / Step Content Box is collapsed, expand it */
			if($('#sba_task_content_introStep_content_collapse_'+task_id).css('display')!='none'){
				task_content_introStep_content_showHide(task_id);
			}
		}
		else{
			alert('<?=$Lang['SBA']['StillInEditModeAlertMsg']?>');
		}
	}
}





/******************************************************\
* Introduction / Step Content Box ShowHide function *
\******************************************************/

function task_content_introStep_content_showHide(task_id){
	if($('#sba_task_content_introStep_content_collapse_'+task_id).css('display')=='none'){
		$('#sba_task_content_introStep_contentBox_'+task_id).hide();
		$('#sba_task_content_introStep_content_collapse_'+task_id).show();
		$('#sba_task_content_introStep_tabMenu_collapseBtn_'+task_id).removeClass().addClass('expand').attr('title', '<?=$Lang['SBA']['Expand']?>');
		$('#sba_task_content_introStep_tabMenu_collapseBtn_'+task_id+' span').text('<?=$Lang['SBA']['Expand']?>');
	}
	else{
		$('#sba_task_content_introStep_content_collapse_'+task_id).hide();
		$('#sba_task_content_introStep_contentBox_'+task_id).show();
		$('#sba_task_content_introStep_tabMenu_collapseBtn_'+task_id).removeClass().addClass('collapse').attr('title', '<?=$Lang['SBA']['Collapse']?>');
		$('#sba_task_content_introStep_tabMenu_collapseBtn_'+task_id+' span').text('<?=$Lang['SBA']['Collapse']?>');
	}
}





/**************************************\
* Student Task Handin Related Function *
\**************************************/

function get_task_stdAnswer_handin_form(task_id){
        
	if(editCheck.RequestEdit(const_editCheck_type_taskHandin, task_id)){
		task_id = task_id || 0;
		
		var task_suffix = task_id? task_id : 'new';
		
		if($('#sba_task_content_stdAnswer_edit_'+task_suffix).html()){
			var formObj = document.getElementById('sba_task_stdAnswer_handinform_'+task_id);
			
			switch(formObj.r_QuestionType.value){
				case '<?=$sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']?>':
					get_task_stdAnswer_handin_TEXTAREA(task_id);
					break;
			}
		}
		else{
			Block_Element('sba_task_content_stdAnswer_'+task_id);
			
			$.ajax({
				url		: "index.php?mod=ajax&task=ajax_task",
				type 	: "POST",
				data 	: 'ajaxAction=TaskHandinForm&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id='+task_id,
				success : function(xml){
							var resultUI = $(xml).find("UI").text();
							
							$('#sba_task_content_stdAnswer_content_'+task_id).hide();
							
							$('#sba_task_content_stdAnswer_edit_'+task_id).html(resultUI).show();
							
							UnBlock_Element('sba_task_content_stdAnswer_'+task_id);
						  },
				error 	: function(xhr, ajaxOptions, thrownError){
							alert(ajaxOptions);
							editCheck.FinishEdit();
							UnBlock_Element('sba_task_content_stdAnswer_'+task_id);
					   	  }
				
			});
		}
	}
}

function task_stdAnswer_handin_form_submit(task_id){
	var formObj = document.getElementById('sba_task_stdAnswer_handinform_'+task_id);
	
	switch(formObj.r_QuestionType.value){
		case '<?=$sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']?>':
			var oEditor = FCKeditorAPI.GetInstance('r_Answer_'+task_id);
			oEditor.UpdateLinkedField(); // Update the content in FCK Editor to corresponding textarea field
			
			formObj.r_Answer.value = $('#r_Answer_'+task_id).val();
			break;
	}
	
	Block_Element('sba_task_content_stdAnswer_'+task_id);
	
	$.ajax({
			url		: "index.php?mod=ajax&task=ajax_task",
			type	: "POST",
			data	: 'ajaxAction=TaskHandinUpdate&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id='+task_id+'&'+$("#sba_task_stdAnswer_handinform_"+task_id).serialize(),
			error	: function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
				   	  },
			success : function(xml){
						var resultUI 	 = $(xml).find("UI").text();
						var resultInfoUI = $(xml).find("InfoUI").text();
						var responseMsg  = $(xml).find("ResponseMsg").text();
						
						$('#sba_task_content_stdAnswer_content_'+task_id).html(resultUI);
						
						$('#sba_task_content_stdAnswer_info_'+task_id).html(resultInfoUI);
						
						task_stdAnswer_handin_form_cancel(task_id);
						
						/* Display the response message */
						show_response_msg(task_id, responseMsg);
						
						UnBlock_Element('sba_task_content_stdAnswer_'+task_id);
					  }
	});
}

function task_stdAnswer_handin_form_cancel(task_id){
        
	editCheck.FinishEdit();
	
	$('#sba_task_content_stdAnswer_content_'+task_id).show();
							
	$('#sba_task_content_stdAnswer_edit_'+task_id).hide();
	
	var formObj = document.getElementById('sba_task_stdAnswer_handinform_'+task_id);
	
	switch(formObj.r_QuestionType.value){
		case '<?=$sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']?>':
			editor_setHTML('r_Answer_'+task_id, '');
			break;
		default :
			$('#sba_task_content_stdAnswer_edit_'+task_id).html('');
			break;
	}
}

function get_task_stdAnswer_handin_TEXTAREA(task_id){
	$.ajax({
			url		: "index.php?mod=ajax&task=ajax_task",
			type 	: "POST",
			data 	: 'ajaxAction=GetTaskHandinFormInfo&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id='+task_id,
			async 	: false,
			error 	: function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
				   	  },
			success : function(xml){
						var resultStdAns   = $(xml).find("StdAns").text();
						var resultAnswerID = $(xml).find("AnswerID").text();
						
						$('#sba_task_content_stdAnswer_content_'+task_id).hide();
						
						$('#sba_task_content_stdAnswer_edit_'+task_id).show();
						
						var formObj = document.getElementById('sba_task_stdAnswer_handinform_'+task_id);
						
						formObj.r_AnswerID.value = resultAnswerID;
						
						editor_setHTML('r_Answer_'+task_id, resultStdAns);
					  }
	});
}





/**************************************\
* Student Step Handin Related Function *
\**************************************/
function step_stdAnswer_handin_form_reset(step_id){//reset inputs inside step handin

        $("#sba_step_handinForm_"+step_id+' input').each(function() {
            switch(this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        });


}
function step_stdAnswer_handin_form_submit(task_id, step_id, answer_id){
	Block_Element('sba_task_content_stdAnswer_'+task_id);
	
	$.ajax({
			url		: "index.php?mod=ajax&task=ajax_step",
			type	: "POST",
			data	: 'ajaxAction=StepHandinUpdate&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id='+task_id+'&step_id='+step_id+'&r_answer_id='+answer_id+'&r_isDelete=0&'+$("#sba_step_handinForm_"+step_id).serialize(),
			async	: false,
			error	: function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
				   	  },
			success : function(xml){
						var resultUI 	= $(xml).find("UI").text();
						var responseMsg = $(xml).find("ResponseMsg").text();
						
						/* Save the corresponding Task Introduction UI in sba_task_content_introStep_content_html[task_id]*/
						sba_task_content_introStep_content_html[task_id] = '';
						
						/* Update the corresponding Step UI*/
						$('#sba_task_content_introStep_content_'+task_id).html(resultUI);
						
						/* Display the response message */
						show_response_msg(task_id, responseMsg);
						
						UnBlock_Element('sba_task_content_stdAnswer_'+task_id);
					  }
	});
}


function analysysSubmitAsHandin(task_id, step_id, submitedSurveyId, questionType){

	$.ajax({
			url		: "index.php",
			type	: "POST",
			data	: "mod=ajax&task=ajax_HandInSurveyAnalysis&r_surveyId="+submitedSurveyId+"&r_stepId="+step_id+'&r_questionType='+questionType,
			success : function(data){
						task_content_introStep_tabMenu_onclick(task_id, 0, 0, step_id);
				      }
	});

}

function deletePowerConcept(task_id, step_id, answer_id, PowerConceptID){
	if(confirm('Are you sure to delete Concept Map?')){
		$.ajax({
			url		: "index.php?mod=ajax&task=ajax_step",
			type	: "POST",
			async	: false,
			data	: "ajaxAction=DeletePowerConcept&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id+"&step_id="+step_id+"&r_answer_id="+answer_id+"&r_powerconcept_id="+PowerConceptID,
			error	: function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success	: function(xml){
						var resultUI    = $(xml).find("UI").text();
						var responseMsg = $(xml).find("ResponseMsg").text();
						
						/* Clear the corresponding Task Introduction UI in sba_task_content_introStep_content_html[task_id]*/
						sba_task_content_introStep_content_html[task_id] = '';
						
						/* Display the form at Introduction Step Content Box */
						$('#sba_task_content_introStep_content_'+task_id).html(resultUI);
						
						/* Display the response message */
						show_response_msg(task_id, responseMsg);
					  }
		});
	}		
}

function deleteSurveyAnalysis(task_id, step_id, answer_id, survey_id){
	if(confirm('Are you sure to delete survey?')){
		$.ajax({
			url		: "index.php?mod=ajax&task=ajax_step",
			type	: "POST",
			async	: false,
			data	: "ajaxAction=DeleteSurveyAnalysis&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id+"&step_id="+step_id+"&r_answer_id="+answer_id+"&r_survey_id="+survey_id,
			error	: function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success	: function(xml){
						var resultUI    = $(xml).find("UI").text();
						var responseMsg = $(xml).find("ResponseMsg").text();
						
						/* Clear the corresponding Task Introduction UI in sba_task_content_introStep_content_html[task_id]*/
						sba_task_content_introStep_content_html[task_id] = '';
						
						/* Display the form at Introduction Step Content Box */
						$('#sba_task_content_introStep_content_'+task_id).html(resultUI);
						
						/* Display the response message */
						show_response_msg(task_id, responseMsg);
					  }
		});
	}
}





/******************************\
* Reorder Task / Step Function *
\******************************/

function reorderTask(){
	if(editCheck.RequestEdit(const_editCheck_type_thickbox)){
		show_dyn_thickbox_ip('','index.php?mod=content&task=reorderTask&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>', 'TB_iframe=true&modal=true', 1, 480, 780, 1);
	}
}

function reorderStep(task_id){
	if(editCheck.RequestEdit(const_editCheck_type_thickbox, task_id)){
		show_dyn_thickbox_ip('','index.php?mod=content&task=reorderStep&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id='+task_id, 'TB_iframe=true&modal=true', 1, 480, 780, 1);
	}
}





/******************************\
* Redefine tb_remove Function *
\******************************/

var tb_remove = (function(){
	var tb_remove_ori = tb_remove;
	return function(){
		editCheck.FinishEdit();
		tb_remove_ori();
	}
})();




/***********************************\
* Task Reorder Show / Hide function *
\***********************************/

function check_tasks_reorder_btn(){
	if(($('.sba_task').length - $('.sba_task[isDel=1]').length) > 2){
		$('#sba_task_reorderBtn').show();
	}
	else{
		$('#sba_task_reorderBtn').hide();
	}
}





/***********************************\
* Step Reorder Show / Hide function *
\***********************************/

function check_steps_reorder_btn(task_id){
	var no_Of_Step = $('.sba_step_'+task_id).length;
	
	if(no_Of_Step >= 2){
		$('#sba_step_reorderBtn_'+task_id).show();
	}
	else{
		$('#sba_step_reorderBtn_'+task_id).hide();
	}
}

function check_add_step_btn(task_id){
	var no_Of_Step = $('.sba_step_'+task_id).length;
	
	if(no_Of_Step==0){
		$('#sba_task_content_introStep_tabMenu_addStepBtn_'+task_id).removeClass('add');
		$('#sba_task_content_introStep_tabMenu_addStepBtn_'+task_id+' span font').html('<?=$Lang['SBA']['AddStep']?>');
	}
	else{
		$('#sba_task_content_introStep_tabMenu_addStepBtn_'+task_id).addClass('add');
		$('#sba_task_content_introStep_tabMenu_addStepBtn_'+task_id+' span font').html('&nbsp;');
	}
}





/**********************************\
* Response Message Related Function*
\**********************************/

var TimeoutObjAry = new Array();
function show_response_msg(task_id, msg) {
	if(Trim(msg) != ''){
		clearTimeout(TimeoutObjAry[task_id]);
		
		/* Display the Response Message */
		$('#sba_task_msgBox_'+task_id+' div span').html(msg);
		$('#sba_task_msgBox_'+task_id).css('visibility', 'visible');
		
		TimeoutObjAry[task_id] = setTimeout("hide_response_msg("+task_id+");",1000*5);
	}
}

function hide_response_msg(task_id) {
	/* Delete the Response Message */
	$('#sba_task_msgBox_'+task_id+' div span').html('');
	$('#sba_task_msgBox_'+task_id).css('visibility', 'hidden');
	
	clearTimeout(TimeoutObjAry[task_id]);
}





/*****************************\
* FCK Editor Related Function *
\*****************************/
function FCKeditor_OnComplete( editorInstance ){
	var formObj = editorInstance.GetParentForm();
	var fckName = editorInstance.Name.replace(/_\d*\b/, '');
	
	switch(fckName){
		case 'r_Answer': // Student Handin Task Answer
			formObj.submitBtn.disabled = '';
			formObj.cancelBtn.disabled = '';
			break;
			
		case 'r_task_Introduction': // Task Introduction
			formObj.submitBtn.disabled = '';
			formObj.cancelBtn.disabled = '';
			break;
			
		case 'r_step_Description': // Step Description
			formObj.submitBtn.disabled = '';
			formObj.cancelBtn.disabled = '';
			
			if(typeof(formObj.deleteBtn)!='undefined'){
				formObj.deleteBtn.disabled = '';
			}
			break;
			
		default:
			break;
	}
}

function editor_setHTML(instanceName, text){
	var oEditor = FCKeditorAPI.GetInstance(instanceName) ;
	oEditor.SetHTML(text);
}

function editor_getHTML(instanceName){
	var oEditor = FCKeditorAPI.GetInstance(instanceName) ;
	return oEditor.GetXHTML(true);
}
function editor_insertHTML(instanceName, text){
	var oEditor = FCKeditorAPI.GetInstance(instanceName) ;
	oEditor.InsertHtml(text);
}
function loadInsertMenu(instanceName){

    tb_show('', './?mod=ajax&task=ajax_task_insertMenu&ajaxAction=getMenu&scheme_id=<?=$scheme_id?>&note_id=<?=$noteid?>&editorName='+instanceName+'&height=400&width=650');
}

/*********************\
* Get Teacher Comment *
\*********************/

function doGetComment(task_id){
	if ($("#sba_task_content_stdAnswer_commentBlock_"+task_id).is(':visible')){
		$("#sba_task_content_stdAnswer_commentBlock_"+task_id).hide();
    }
  	else{
  		$.ajax({
			    url:      "index.php?mod=ajax&task=ajax_load_teacher_comment",
			    type:     "POST",
			    async:    false,
			    data:     "scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id="+task_id,
			    error:    function(xhr, ajaxOptions, thrownError){
			                alert(xhr.responseText);
			              },
			    success:  function(data){
			    			if(data != "0"){
			    				$("#sba_task_content_stdAnswer_commentBlock_"+task_id).html(data);
			    				$("#sba_task_content_stdAnswer_commentBlock_"+task_id).show("fast");	
			    			}
			              }
  		});
	}
}

function remove_attachfile(file_id){
    
    $('.attachments .file_'+file_id).after('<input type="hidden" name="r_remove_Attachment[]" value="'+file_id+'"/>').hide();
}

function cancel_attach_files(){
   
    var new_attach_file_ids =  $('.new_attach_file_id').map(function(){ return $(this).val()}).get();
    
    if (new_attach_file_ids.length > 0){
	$('.new_attach_file_id').parent().remove();
	$.post("index.php?mod=ajax&task=ajax_fileupload&action=removeFiles", {'new_attach_file_ids[]':new_attach_file_ids});
    }
    
    $('.attachments .attach_file').show();
    $('input[name="r_remove_Attachment[]"]').remove();
}

function confirm_attach_files(){
    
    $('.new_attach_file_id').removeClass('new_attach_file_id');
    $('input[name="r_remove_Attachment[]"]').each(function(){ $('.attachments .file_'+$(this).val()).remove()}).remove();
}
function init_uploader(button_id, params) {
   
    $(function() {
	    
	    var uploader = new plupload.Uploader({
		    runtimes : 'gears,html5,flash,silverlight,browserplus',
		    browse_button : button_id,
		    multipart_params: params,
		    multi_selection: true,
		    drop_element: button_id,
		    url : './?mod=ajax&task=ajax_fileupload&ajaxAction=addFile&scheme_id=<?=$sba_libScheme->getSchemeID()?>',
		    flash_swf_url : '/templates/jquery/plupload/plupload.flash.swf',
		    silverlight_xap_url : '/templates/jquery/plupload/plupload.silverlight.xap'
	    });
	    uploader.init();
	    
    
	    uploader.bind('FilesAdded', function(up, files) {
		
		    $.map(files, function(file){

			$('.attachment_'+params.task_id+'_'+params.step_id).prepend('<div class="attach_file plupload_'+file.id+'"><a class="file_name">'+file.name+'</a> <span class="progress"></span></div>');
			
		    });
		
		
		    up.refresh(); // Reposition Flash/Silverlight
		    uploader.start();
	    });
    
	    uploader.bind('UploadProgress', function(up, file) {

		    $('.attachments .plupload_'+file.id+' .progress').html(file.percent + "%");
		   
	    });
    
    
	    uploader.bind('FileUploaded', function(up, file, info) {

		    $('.attachments .plupload_'+file.id).replaceWith(info.response);
		   
	    });
    });
		
}
</script>