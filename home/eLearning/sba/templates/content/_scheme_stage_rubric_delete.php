<?php
//modifying By 
/*
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/sba.php");
include_once($PATH_WRT_ROOT."includes/sba.php");
include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");

include_once($PATH_WRT_ROOT."includes/sba/libies.php");

include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
*/
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_rubric.php");



$classRoomID = libies_static::getIESRurbicClassRoomID();
$objRubric = new libiesrubric($classRoomID);

$objRubric->Start_Trans();

$sql = "DELETE FROM task_rubric_detail WHERE rubric_id = {$taskRubricID}";
$res[] = $objRubric->db_db_query($sql);

$sql = "DELETE FROM task_rubric WHERE rubric_id = {$taskRubricID}";
$res[] = $objRubric->db_db_query($sql);

$sql = "UPDATE {$intranet_db}.IES_STAGE_MARKING_CRITERIA SET task_rubric_id = NULL WHERE task_rubric_id = {$taskRubricID}";
$res[] = $objRubric->db_db_query($sql);

$final_res = (count($res) == count(array_filter($res)));

if($final_res)
{
	$objRubric->Commit_Trans();
	$msg = "delete";
}
else
{
	$objRubric->RollBack_Trans();
	$msg = "delete_failed";
}

intranet_closedb();
header("Location: index.php?mod=content&task=scheme_rubric_details&scheme_id={$scheme_id}&msg={$msg}");

?>