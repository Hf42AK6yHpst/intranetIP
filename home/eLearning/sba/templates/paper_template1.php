<?
/** [Modification Log] Modifying By: Siuwan
 *
 * 2016-04-11 (Siuwan) [ip.2.5.7.4.1] (Case:#Q94669)
 * added target=_BLANK for fckeditor hyperlink content when on load
 */
?>
<script>
	$(document).ready(function(){
		$(".draggable").draggable();
		$("div.task_box_content,div.task_content").find("a:not([class])").attr("target","_BLANK");
	});
</script>
<div class="main_paper_top"><div><div></div></div></div>

<div class="main_paper_left"><div class="main_paper_right">
	<div class="main_paper_body_left"><div class="main_paper_body_right"><div class="main_paper_body">
	
	
	<div class="stage1_title">
		<span class="title"><?=$h_headling?></span><span class="deadline"><?=$h_deadline?></span>
		
	
		<!-- top right btn start -->
		<?=$h_right_button?>
		<!-- top right btn end -->
	
		<div class="clear"></div>
		
	</div>
	
	<div class="SBA_IES_logo"></div>
	
	<? if($sba_allow_edit) {?>
	<!-- pretend student page preview -->
	<!--
                <div class="pretend_stu_tool">
                <div class="below"><span>此為學生版顯示情形</span></div>
                </div>
	-->
   <!-- pretend student page preview -->
 	<? } ?>
	
	
	<div class="navigation"><?=$h_navigation?></div>

	<?=$h_stage_top_tool?>
	
	<div class="stage_paper"> <!-- class "stage_paper" making a gradient paper-->
	
	<?
	/*
	if($with_sub_template) {
		include_once("templates/paper_sub_template.php");
			
	} else {
	*/
		//echo $h_main_content;
		include_once($template_script);	
	//}
	?>
	
		<!-- submit btn start -->
		<!--
		<div class="edit_bottom_main">
			<br class="clear" />
			<p class="spacer"></p>
			
			<?/*=$h_button*/?>
			
			<p class="spacer"></p>
		</div>
		-->
		<!-- submit btn end -->	
	</div> 
	<!--class "stage_paper" end -->                     
	   
	</div></div></div>
	
</div></div>

<div class="main_paper_bottom"><div><div></div></div></div>  