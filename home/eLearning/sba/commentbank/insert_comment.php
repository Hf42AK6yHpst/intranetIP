<?php
$objIES = new libies();
$ldb = new libdb();

$categoryID = $_POST["categoryID"];

$msg = "add";

$addcomment = array_map("htmlspecialchars",$addcomment);
if(!$objIES->CHECK_ACCESS("IES-MGMT-COMMENTBANK-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
$redirectTaskInfo = "task=listComment&categoryID=$categoryID";
if(!empty($addcomment)){
	$sql = "INSERT INTO {$intranet_db}.IES_COMMENT ";
	$sql .= "(Comment, DateInput, InputBy, ModifyBy, CommentEN, CategoryID, SchemeType) ";
	$sql .= "VALUES ";
	$sql .= "('$addcomment[B5]', NOW(), $UserID, $UserID, '$addcomment[EN]', $categoryID, '".$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']."')";
	$result = $ldb->db_db_query($sql);
	
	intranet_closedb();
	
	header("Location: index.php?mod=".$mod."&msg=".$msg."&$redirectTaskInfo");
}
else{
	echo "<script language='javascript'>";
	echo "alert('Comment blanked');";
	echo "window.location='index.php?mod=".$mod."&$redirectTaskInfo'";
	echo "</script>";
}
?>
