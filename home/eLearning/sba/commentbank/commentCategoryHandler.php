<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
 
$libies = new libies();

$Action = $_REQUEST["Action"];

switch(strtolower($Action)) {
	default:
	case "new":
	$title = $_REQUEST["addcommentcategory"];
	$result = $libies->addCommentCategory($title,'',$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']);
	if ($result) {
		$msg = "add";
	} else {
		$msg = "update_failed";
	}
	break;
	case "edit":
	$title = $_REQUEST["editcommentcategory"];
	$categoryID = $_REQUEST["id"];
	$result = $libies->editCommentCategory($categoryID,$title);
	if ($result) {
		$msg = "update";
	} else {
		$msg = "update_failed";
	}
	break;
	case "remove":
	$categoryID = $_REQUEST["id"];
	$result = $libies->removeCommentCategory($categoryID);
	if ($result) {
		$msg = "delete";
	} else {
		$msg = "delete_failed";
	}
	break;
}
header("Location: index.php?mod=".$mod."&task=listCategory&msg=".$msg);
?>
