<?
// editing by : 
$returnHTML = '';
$sba_libStage = new SbaStage();

switch($ajaxAction) {
	case "displaySchemeInfoForEdit": 
	include_once($PATH_WRT_ROOT."includes/libinterface.php");

	if(intval($scheme_id) > 0 ) {
			$objScheme = new SbaScheme($scheme_id);

			$_Title = $objScheme->getTitle();
			$_Introduction = $objScheme->getIntroduction();
			$_lang = $objScheme->getLanguage();
			$_schemeRecordStatus = $objScheme->getRecordStatus();
			$_recordStatusCheck[$_schemeRecordStatus] = " checked";

			$h_lang_b5_checked = ' checked ';
			$h_lang_eng_checked = '';
			if(strtolower($_lang) == $ies_cfg["IESSchemeSupportLang"][1]){
				$h_lang_b5_checked = '';
				$h_lang_eng_checked = ' checked ';				
			}

			//if lang is empty , set b5 as default
			if($_lang == ''){
				$h_lang_b5_checked = ' checked ';
				$h_lang_eng_checked = '';
			}

			$linterface = new interface_html();
			$h_introduction  = $linterface->GET_TEXTAREA("r_introduction", $_Introduction);

			$h_missingTitleMsg = $linterface->Get_Form_Warning_Msg('schemeNameInputErrorDiv', $Lang['IES']['InputSchemeTitle'], $Class='WarningDiv');

$var = <<<HTML
	<form name="form1" id="form1" method="post" action="index.php">

<table class="form_table">
			  <tr>
                <td>{$Lang['IES']['SchemeLangCaption']}<span class="tabletextrequire">*</span></td>
			    <td>:</td>
			    <td>
					<input id="r_schemeLang_b5" type="radio" name="r_schemeLang" value="{$ies_cfg["IESSchemeSupportLang"][0]}" {$h_lang_b5_checked}/><label for="r_schemeLang_b5">中文</label> 
					<input id="r_schemeLang_eng" type="radio" name="r_schemeLang" value="{$ies_cfg["IESSchemeSupportLang"][1]}" {$h_lang_eng_checked}/><label for="r_schemeLang_eng">ENG</label>
				</td>
			    </tr>
                    <col width="120">
                    <col class="field_c" />
                    <tr><td>{$Lang['IES']['SchemeTitle']}<span class="tabletextrequire">*</span></td>
                      <td>:</td>
                      <td><input name="r_title" type="text" class="textbox" id="r_title" value="{$_Title}">
                      <br /><span style="display:block; font-size:0.75em; color:#999999">({$Lang['SBA']['WordCountSuggest']}){$h_missingTitleMsg}
							</td>
                    </tr>
                    <tr>
                      <td>{$Lang['IES']['Introduction']}</td>
                      <td>:</td>
                      <td>
						{$h_introduction}
							<!--textarea name ="r_introduction" rows ="20" cols ="100" style="width:100%; height:50px">{$_Introduction}</textarea-->	
					  </td>
					</tr>
                    <tr>
                      <td>{$Lang['IES']['Status']}<span class="tabletextrequire">*</span></td>
                      <td>:</td>
                      <td><input type="radio" name="r_SchemeRecordStatus" id="SchemeRecordStatus[1]" value="1" {$_recordStatusCheck[1]}/><label for="SchemeRecordStatus[1]">{$Lang['IES']['RecordStatusPublish']}</label> <input type="radio" name="r_SchemeRecordStatus" id="SchemeRecordStatus[0]" value="0" {$_recordStatusCheck[0]}/><label for="SchemeRecordStatus[0]">{$Lang['IES']['RecordStatusNotPublish']}</label></td>
                    </tr>
                    </table>
            <div class="task_writing_bottom"><div>
	<input type="hidden" name="mod" value="content">
	<input type="hidden" name="task" value="schemeInfoUpdate">
		<input type="hidden" name="scheme_id" value="{$scheme_id}">
                 <input name="cmdSave" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="checkInput(this.form)" value="{$Lang['IES']['Save2']}" />
				<input name="cmdReset" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onclick="resetEditToViewMode()" value="{$Lang['IES']['Cancel2']} " />
                                        
                </div></div>

	</form>
HTML;

		$returnHTML = $var;
			
	}
	break;
	
	case "getSchemeStageListEditView":
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		include_once($PATH_WRT_ROOT."includes/sba/libSbaScheme.php");
		if(intval($scheme_id) > 0){
			$linterface = new interface_html();
			$objIES = new libies();
			$sba_libScheme = new SbaScheme($scheme_id);
			
			$result = $objIES->GET_STAGE_ARR($scheme_id);
			$schemeMaxScore = $sba_libScheme->getMaxScore();
			
			$x = '<div class="theme_edit">';
		if($sba_allow_edit){
				$x.= '<div class="stage_top_tool">
						<a title="'.$Lang['SBA']['ReOrder'].'" class="move" href="javascript:void(0)" onclick="show_dyn_thickbox_ip(\'\',\'index.php?mod=content&task=reorderStage&scheme_id='.$scheme_id.'\', \'TB_iframe=true&modal=true\', 1, 480, 780, 1)"><span>'.$Lang['SBA']['ReOrder'].'</span></a>
					  </div>';
		}
			$x .= '</div>
					<br class="clear" /><br />';
			$u_existingStage  = '<table class="about_stage">';
			$u_existingStage .= '<tr class="title"><td>'.$Lang['IES']['Criteria'].'</td><td>'.$Lang['IES']['DateSubmitted'].'</td><td>'.$Lang['IES']['Status'].'</td>';
			$u_existingStage .= '<td>'.$Lang['IES']['Weight'].'</td><td>'.$Lang['SBA']['FullMark'].'</td>';
			$u_existingStage .= '</tr>';
			
			$buttonCaption1  = ($sba_allow_edit)? $Lang['SBA']['GoToStageSetting']: $Lang['SBA']['Enter'];
			
			//GENERATE THE STAGE LIST
			for($i = 0; $i < count($result); $i++){
				
				$_ColorIndex = ($i % 5) + 1;
				
				$_Title = $result[$i]['Title'];
				$_StageID = $result[$i]['StageID'];
				$_Weight = $result[$i]['Weight'];
				$_MaxScore = $result[$i]['MaxScore'];
				$_RecordStatus = $result[$i]['RecordStatus'];
				
				//$sba_libStage = new SbaStage($_StageID, $sba_allow_edit);
				$_Deadline = $sba_libStage->ReturnValidSubmissionDate($result[$i]['Deadline'], " ");
				
				
				$_TitleField = '<span class="tabletextrequire">*</span><input type="text" name="Title[]" class="textbox" value="'.htmlspecialchars($_Title,ENT_QUOTES).'" />';
				$_TitleField.= '<input type="hidden" name="StageID[]" value="'.$_StageID.'" />';
				$_TitleField.= '<div id="TitleWarningDiv'.$_StageID.'" style="display:none;color:red;"></div>';
				
				$_DeadlineField = $linterface->GET_DATE_PICKER("Deadline[]",$_Deadline,"","yy-mm-dd","","","","","Deadline$i");
				$_DeadlineField.= '<div id="DeadlineWarningDiv'.$_StageID.'" style="display:none;color:red;"></div>';
				
				$_WeightField = '<input type="text" name="Weight[]" class="input_num" value="'.$_Weight.'" />';
				$_WeightField.= '<div id="WeightWarningDiv'.$_StageID.'" style="display:none;color:red;"></div>';
				
				$_MaxScoreField = '<input type="text" name="MaxScore[]" class="input_num" value="'.$_MaxScore.'" />';
				$_MaxScoreField.= '<div id="MaxScoreWarningDiv'.$_StageID.'" style="display:none;color:red;"></div>';
				
				$_RecordStatusField = '<input type="radio" id="RecordStatusY'.$_StageID.'" name="RecordStatus['.$_StageID.']" value="1" '.($_RecordStatus==1?'checked':'').' /><label for="RecordStatusY'.$_StageID.'">'.$Lang['IES']['RecordStatusPublish'].'</label>';
				$_RecordStatusField.= '<input type="radio" id="RecordStatusN'.$_StageID.'" name="RecordStatus['.$_StageID.']" value="0" '.($_RecordStatus!=1?'checked':'').' /><label for="RecordStatusN'.$_StageID.'">'.$Lang['IES']['RecordStatusNotPublish'].'</label>';
				
				$u_existingStage .='<tr class="s'.$_ColorIndex.'"><td>'.$_TitleField.'</td><td><span class="tabletextrequire">*</span>'.$_DeadlineField.'</td><td>'.$_RecordStatusField.'</td>';
				$u_existingStage .= '<td>'.$_WeightField.'</td><td>'.$_MaxScoreField.'</td>';
				$u_existingStage .= '</tr>';
				
			}
			
			$u_existingStage .= '<tr>
								  <td style="padding: 0pt;">&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td style="text-align: right;">'.$Lang['SBA']['MaxScore'].':</td>
								  <td>
									<input type="text" class="input_num" id="TotalMaxScore" name="TotalMaxScore" value="'.$schemeMaxScore.'" />
								  </td>
								</tr>';
			
			$u_existingStage  .= '</table>';
			
			$x .= $u_existingStage;
			$x .= '<br />';
			
			if($sba_allow_edit) {
				$x .= '<div class="stage_left_tool" id="sba_newButton">
							<a href="index.php?mod=content&task=stageNew&scheme_id='.$scheme_id.'" class="add" title="'.$Lang['SBA']['AddStage'].'"><span>'.$Lang['SBA']['AddStage'].'</span></a>
						</div>
						<div class="clear">
						</div>
						<div class="task_writing_bottom">
							<div>
								<input type="button" value="'.$Lang['IES']['Save2'].'" onclick="updateSchemeStageList('.$scheme_id.');" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" class="formbutton" name="submitUpdateStage">
								<input type="button" value="'.$Lang['IES']['Cancel2'].' " onclick="getSchemeStageListView('.$scheme_id.');" onmouseout="this.className=\'formsubbutton\'" onmouseover="this.className=\'formsubbuttonon\'" class="formsubbutton" name="submitCancel">		
							</div>
						</div>';
			}
		}
		$returnHTML = $x;
	break;
	
	case "getSchemeStageListView":
		//include_once($PATH_WRT_ROOT."includes/libinterface.php");
		include_once($PATH_WRT_ROOT."includes/sba/libSbaScheme.php");
		if(intval($scheme_id) > 0){
			//$linterface = new interface_html();
			$sba_libScheme = new SbaScheme($scheme_id);
			$objIES = new libies();
			$result = $objIES->GET_STAGE_ARR($scheme_id);
			$schemeMaxScore = $sba_libScheme->getMaxScore();
			
			$x = '<div class="theme_edit">';
			if($sba_allow_edit){
				$x.= '<div class="stage_top_tool">
						<a title="'.$Lang['IES']['Edit'].'" class="edit" href="javascript:void(0);" onclick="getSchemeStageListEditView('.$scheme_id.');"><span>'.$Lang['IES']['Edit'].'</span></a>
						<a title="'.$Lang['SBA']['ReOrder'].'" class="move" href="javascript:void(0)" onclick="show_dyn_thickbox_ip(\'\',\'index.php?mod=content&task=reorderStage&scheme_id='.$scheme_id.'\', \'TB_iframe=true&modal=true\', 1, 480, 780, 1)"><span>'.$Lang['SBA']['ReOrder'].'</span></a>
					  </div>';
			}
			$x .= '</div>
					<br class="clear" /><br />';
			$u_existingStage  = '<table class="about_stage">';
			$u_existingStage .= '<tr class="title"><td>'.$Lang['IES']['Criteria'].'</td><td>'.$Lang['IES']['DateSubmitted'].'</td><td>'.$Lang['IES']['Status'].'</td>';
			$u_existingStage .= '<td>'.$Lang['IES']['Weight'].'</td><td>'.$Lang['SBA']['FullMark'].'</td><td>'.$Lang['SBA']['TasksAndSteps'].'</td><td>&nbsp;</td>';
			$u_existingStage .= '</tr>';
			
			$buttonCaption1  = ($sba_allow_edit)? $Lang['SBA']['GoToStageSetting']:$Lang['SBA']['Enter'];
			
			//GENERATE THE STAGE LIST
			for($i = 0; $i < count($result); $i++){
				
				$_ColorIndex = ($i % 5) + 1;
				
				$_Title = $result[$i]['Title'];
				$_Deadline = $sba_libStage->ReturnValidSubmissionDate($result[$i]['Deadline']);
				$_StageID = $result[$i]['StageID'];
				$_Weight = $result[$i]['Weight'] ? $result[$i]['Weight'] : "-";
				$_MaxScore = $result[$i]['MaxScore'] ? $result[$i]['MaxScore'] : "-";
				$_RecordStatus = $result[$i]['RecordStatus']==1 ? $Lang['IES']['RecordStatusPublish'] : $Lang['IES']['RecordStatusNotPublish'];
				
				$u_existingStage .='<tr class="s'.$_ColorIndex.'"><td>'.$_Title.'</td><td>'.$_Deadline.'</td><td>'.$_RecordStatus.'</td>';
				$u_existingStage .= '<td>'.$_Weight.'</td><td>'.$_MaxScore.'</td><td><a href=\'javascript:void(0)\' onclick=\'show_dyn_thickbox_ip("","index.php?mod=content&task=stageDetails&scheme_id='.$scheme_id.'&stage_id='.$_StageID.'", "TB_iframe=true&modal=true", 1, 480, 780, 1)\'>'.$Lang['SBA']['StageDetails'].'</a></td>';
				$u_existingStage .= '<td><input name="submitStageSetting[]" type="button" class="formbutton" value="'.$buttonCaption1.'" onclick="location.href=\'?mod=content&task=stageInfo&stage_id='.$_StageID.'&scheme_id='.$scheme_id.'\'"/></td>';
				$u_existingStage .= '</tr>';
				
			}
			
			$u_existingStage .= '<tr>
								  <td style="padding: 0pt;">&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td style="text-align: right;">'.$Lang['SBA']['MaxScore'].':</td>
								  <td>
									<strong>'.$schemeMaxScore.'</strong>
								  </td>
								</tr>';
			
			$u_existingStage  .= '</table>';
			$x .= $u_existingStage;
			$x .= '<br />';
			
			if($sba_allow_edit) { 
				$x .= '<div class="stage_left_tool" id="sba_newButton">
							<a href="index.php?mod=content&task=stageNew&scheme_id='.$scheme_id.'" class="add" title="'.$Lang['SBA']['AddStage'].'"><span>'.$Lang['SBA']['AddStage'].'</span></a>
						</div>
						<div class="clear"></div>';
			}
			
		}
		$returnHTML = $x;
	break;
	
	case "updateSchemeStageList";
	
		include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
		
		$libdb = new libdb();
		
		$StageIDArr = $_REQUEST['StageID'];
		$TitleArr = $_REQUEST['Title'];
		$DeadlineArr = $_REQUEST['Deadline'];
		$WeightArr = $_REQUEST['Weight'];
		$MaxScoreArr = $_REQUEST['MaxScore'];
		$RecordStatusArr = $_REQUEST['RecordStatus'];
		$TotalMaxScore = $_REQUEST['TotalMaxScore'];
		
		$success = array();
		for($i=0;$i<count($StageIDArr);$i++){
			$safeTitle = $libdb->Get_Safe_Sql_Query(trim(urldecode(stripslashes($TitleArr[$i]))));
			$deadline = trim($DeadlineArr[$i]);
			$weight = trim($WeightArr[$i]) ? "'".trim($WeightArr[$i])."'" : "NULL";
			$maxscore = trim($MaxScoreArr[$i]) ? "'".trim($MaxScoreArr[$i])."'" : "NULL";
			$recordStatus = $RecordStatusArr[$i];
			$sql = "UPDATE ".$intranet_db.".IES_STAGE SET 
						Title='".$safeTitle."',Deadline='".$deadline."',Weight=".$weight.",MaxScore=".$maxscore.",RecordStatus='".$recordStatus."',DateModified=NOW(),ModifyBy='".$sba_thisUserID."' 
					WHERE StageID='".$StageIDArr[$i]."' ";
					
			$success[] = $libdb->db_db_query($sql);
			
			// re-calculate stage total mark after weight and max mark update
			libies_static::calculateStageMark($StageIDArr[$i]);
		}
		// re-calculate scheme total mark after weight and max mark update
		libies_static::calculateSchemeMark($thisSchemeID);
		
		$sql = "UPDATE ".$intranet_db.".IES_SCHEME SET MaxScore='".$TotalMaxScore."',DateModified=NOW(),ModifyBy='".$sba_thisUserID."' WHERE SchemeID='".$thisSchemeID."'";
		$success[] = $libdb->db_db_query($sql);
		
		if(!in_array(false,$success)){
			echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
		}else{
			echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		}
	break;
	
	default		: 
			break;
}

if(trim($returnHTML) == ''){
	//do nothing
}else{
	echo $returnHTML;
}
?>