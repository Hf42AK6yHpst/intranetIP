<?php
// editing by 

//$file_id = $file_id;

$libies = new libies();


$q_result = false;

$actionTable = '';
switch($userType) {
	case $ies_cfg['HandinFileByTeacher']	:	
		$actionTable = "IES_WORKSHEET_TEACHER_FILE";		
		break;
	case $ies_cfg['HandinFileByStudent']	:	
		$actionTable = "IES_WORKSHEET_HANDIN_FILE";
		break;
	default 								:	
		echo 'userType not find.';
		exit();
		break;
}

//for safe
if($actionTable == ''){
	echo 'Operation Error!';
	exit();
}

$physicalFileRemoved = $libies->removeWorksheetFilePhysical($actionTable, $file_id);

if($physicalFileRemoved) {
	$q_result = $libies->removeHandInFile($actionTable,$file_id);
}
	
//intranet_closedb();

# Output the modified date to the page
header("Content-Type:   text/xml");
$libies = new libies();
$XML = $libies->generateXML(
					array(
						array("result", ($q_result?1:0)),
						array("file_exist", ($physicalFileRemoved?1:0)),
						array("file_id", $file_id)
					)
				);
echo $XML;
?>