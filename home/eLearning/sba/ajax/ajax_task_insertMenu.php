<?
include_once($intranet_root."/includes/sba/libSbaTask.php");
include_once($intranet_root."/includes/sba/libSbaStepHandinFactory.php");
include_once($intranet_root."/includes/sba/libies_survey.php");
include_once($intranet_root."/includes/sba/libies_bibi.php");
include_once($intranet_root."/includes/libipfilesmanager.php");
        
$libSbaStepMapper = new SbaStepMapper();
$libSba = new libSba();
$libSurvey = new libies_survey();

$task_id=str_replace('r_Answer_', '', $editorName);
switch ($ajaxAction){
    
    case 'getBibi':
        $libies_bibi = new libies_bibi();
        $bibiArray = $libies_bibi->getAllbibiBySchemeId($scheme_id, $UserID, $type, $cond);
        

        if(is_array($bibiArray)){
            foreach($bibiArray as $key=>$value){
                $stringArray = $libies_bibi->parseContentString($value['Content']);
                $bibi=nl2br($libies_bibi->stringfilter($libies_bibi->getPresentString($stringArray, $value['Type'])));
?>
                <table style="width:100%;">
                        <col style="width:7%"/><col style="width:48%"/><col style="width:15%"/><col style="width:30%"/>
                        <tr>
                            <td><input type="checkbox" name="Bibis[]" value="<?=$bibi?>"/></td>
                            <td class="tool_title_col">
                                <?=$bibi?>
                            </td>
                            <td class="tool_type_col">
                                <?=$Lang['IES'][$value['Type']]['fieldname']?>
                            </td>
                            <td class="tool_tags_col" >
                                <?=nl2br($stringArray['remark'])?>
                            </td>
                        </tr>
                </table>
<?
            }
        }else{ echo $Lang['IES']['TemporarilyNoContent']; }
        break;
    
    case 'getFiles':
        $libies_bibi = new libies_bibi();
        $bibiArray = $libies_bibi->getAllbibiBySchemeId($scheme_id, $UserID, $type, $cond);
        $encodedModuleCode = base64_encode($sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']);
        $libipfilemanager = new libipfilesmanager($UserID, $moduleCode);
		$fileList = $libipfilemanager->getFileList();
		if(!empty($fileList)){
			foreach($fileList as $fileObj){
				$file = (array)$fileObj;
				$PortalFilefileName = "";
				$PortalFilefolderPath = "";
				$PortalFilefileHashName = "";
				foreach($file as $k=>$v){
					$strKey = urldecode(str_replace("%00","",urlencode($k)));
					
					switch($strKey){
						case 'PortalFilefileName':
							$PortalFilefileName = $v;
							break;
						case 'PortalFilefolderPath':
							$PortalFilefolderPath = $v;
							break;
						case 'PortalFilefileHashName':
							$PortalFilefileHashName = $v;
							break;
						default:
							break;			
					}
				}
				$fileExt = $libipfilemanager->getFileExtention($PortalFilefileName);
				$imgArr = array("jpg","jpeg","gif","bmp","png");
				$fileLink = $PortalFilefolderPath.$PortalFilefileHashName;
				
				if(in_array(strtolower($fileExt), $imgArr)){					
					$fileDisplayText = "<a href='".$fileLink."' target='_blank'>".$PortalFilefileName."</a><br><img src='".$fileLink."'>";
				}else{
					$fileDisplayText = "<a href='".$fileLink."' target='_blank'>".$PortalFilefileName."</a>";					
				}
				$outputFile = nl2br($libies_bibi->stringfilter($fileDisplayText));
				?>
				<table style="width:100%;">
                        <col style="width:7%"/><col style="width:48%"/><col style="width:15%"/><col style="width:30%"/>
                        <tr>
                            <td><input type="checkbox" name="Bibis[]" value="<?=$outputFile?>"/></td>
                            <td class="tool_title_col">
                                <?=$PortalFilefileName?>
                            </td>
                        </tr>
                </table>
				<?
			}
		}else{
			echo $Lang['IES']['TemporarilyNoContent'];
		}
        break;
    
    case 'getMenu':
        $steps=$libSbaStepMapper->findByTaskId($task_id);
        
?>
        <script>
        
        
        $(document).ready(function(){
                                              
                $('#choose_type').submit(function(){
                    $('#insert_step1').slideUp();
                    $.post('./?mod=ajax&task=ajax_task_insertMenu&ajaxAction=step1&scheme_id=<?=$scheme_id?>&editorName=<?=$editorName?>',  
                            $(this).serialize(),
                            function(data){
                                if (data=='fail')
                                    alert('Please select a valid element');
                                else                            
                                    $('#insert_step1').html(data).slideDown();
                    });
                    return false;
                });
                
                $('input[name="type"]').click(function(){
                    if($(this).val()=='step' && $(this).is(':checked')){
                        $('.option_step').removeAttr('disabled');
                    }else{
                        $('.option_step').attr('disabled','disabled');
                    }
                })
                
                $('#insert').click(function(){
           
                    var html=$("#insert_result").html() || $('#form_step1 input:checked').map(function(){return this.value}).get().join('<br/>');
                    if (!html){
                        alert('Please select a valid element');
                    }else{
                        editor_insertHTML('<?=$editorName?>',html);
                        tb_remove();
                    }
                });
                
        });

        </script>
        <h3><?=$Lang['IES']['InsertSbaElement']?></h3>
        <div>
        
            <div style="position:relative;overflow:hidden;height: 320px;width:650px;">
                <div id='insert_step0' style="width:650px;">
                    
                    <h4><?=$Lang['IES']['ChooseInsertContent']?></h4>
                    <form id="choose_type">
                    <input type='hidden' name='note_id' value='<?=$note_id?>'/>
                    <input type='radio' name='type' id="file_radio" value="file"><label for="file_radio"><?=$Lang['IES']['Folder']?></label>
                    <input type='radio' name='type' id="bibi_radio" value="bibi"><label for="bibi_radio"><?=$Lang['IES']['TextDesc']?></label>
                    <input type='radio' name='type' id="reflect_radio" value="reflect"><label for="reflect_radio"><?=$Lang['IES']['NoteDesc']?></label>
                    <input type='radio' name='type' id="step_radio" value="step"><label for="step_radio"><?=$Lang['IES']['TheContentOfStep']?>:</label>
			<select class="option_step" name="StepID" style="width:200px; margin:5px 20px 0px;" disabled='disabled'>
                        <? foreach ($steps as $step) :?>
                            <option value="<?=$step->getStepID()?>"><?=$step->getTitle()?></option>
                        <? endforeach; ?>
                    </select>
                    <input  type="submit" value="<?=$Lang['IES']['Go']?>" />
                    </form>
                  
                </div>
                <div id='insert_step1' style="width:650px;display:none;">
                    
                  
                </div>
            
            </div>
        <div style='text-align:center'><input type="button" value="<?=$Lang['IES']['Insert']?>" id="insert"/></div>
        </div>
<?
    break;

    case 'step1':
        if ($type=='reflect'){
           
            $sql = "SELECT CONTENT FROM IES_REFLECT_NOTE
                    WHERE NOTEID = '$note_id' Limit 1";
           
            $returnArray = array();
            $content = current($libdb->returnVector($sql));
            echo getFinalHtml(htmlspecialchars($content));
            
        }else if ($type=='bibi'){
            
?>
            <script>

                var getBibiList = function(){
                    $('#insert_step2').slideUp();
                    $.post('./?mod=ajax&task=ajax_task_insertMenu&ajaxAction=getBibi&scheme_id=<?=$scheme_id?>&editorName=<?=$editorName?>',
                        $('#bibi_filter').serialize(), function(data){
                        $('#insert_step2').html(data).slideDown();
                    })
                };
                    
                $('#bibi_filter select').change(getBibiList);
                $('#bibi_filter input').keyup(getBibiList);
                
                getBibiList();
                            
            </script>
            <h4><?=$Lang['IES']['ChooseBibi']?>:</h4>
            <form id="bibi_filter">
                <select name="type">
                        <option value="">--<?=$Lang['IES']['Select']?>--</option>
                        <optgroup label="中文">
                            <? foreach ($ies_cfg['bibi']['chi'] as $type): ?>
                                <option value="<?=$type?>"><?=$Lang['IES'][$type]['fieldname']?></option>
                            <? endforeach; ?>
                        </optgroup>
                        <optgroup label="English">
                            <? foreach ($ies_cfg['bibi']['eng'] as $type): ?>
                                <option value="<?=$type?>"><?=$Lang['IES'][$type]['fieldname']?></option>
                            <? endforeach; ?>
                        </optgroup>
                        
                </select>
                <input name="cond" type='text' title="<?=$Lang['IES']['Search']?>" placeholder="<?=$Lang['IES']['Search']?>" />
            </form>
            
            <form id="form_step1">
                    
                <table style="width:100%; border-width: 1px; border-bottom: solid">
                    <col style="width:7%"/><col style="width:48%"/><col style="width:15%"/><col style="width:30%"/>
                    <tr>
                        <th>&nbsp;</th>
                        <th><?=$Lang['IES']['bibi']?></th>
                        <th><?=$Lang['IES']['type']?></th>
                        <th><?=$Lang['IES']['remark']?></th>
                    </tr>
                </table>
            <div id='insert_step2' style="width:650px; height:180px; display:none;overflow:scroll;"></div>
            </form>
            
<?
        }else if ($type="step" && $StepID){
            
            include_once($intranet_root."/includes/sba/libSbaStepHandin.php");
            $libSbaStepMapper = new SbaStepMapper();
            $step = $libSbaStepMapper->getByStepID($StepID);
            $AnserID = $libSba->getAnswerIDByStepIDUserID($StepID, $sba_thisStudentID);
            
            $questionType=$step->getQuestionType();
            //get handin object
            $libStepHandin = libSbaStepHandinFactory::createStepHandin($task_id,$step->getQuestionType(),$StepID,$step->getQuestion(),$AnserID);
            
            switch ($questionType){
                case $sba_cfg['DB_IES_STEP_QuestionType']['longQuestion'][0]:
                case $sba_cfg['DB_IES_STEP_QuestionType']['selfEvaluation'][0]:
                case $sba_cfg['DB_IES_STEP_QuestionType']['tablePlanner'][0]:
                    $content=current($libStepHandin->getExportDisplay());
                    echo getFinalHtml($content);
                    break;
                case $sba_cfg['DB_IES_STEP_QuestionType']['observeEdit'][0]:
                case $sba_cfg['DB_IES_STEP_QuestionType']['interviewEdit'][0]:
                case $sba_cfg['DB_IES_STEP_QuestionType']['surveyEdit'][0]:
                    $SurveyIDs=$libStepHandin->getSurveyIDs();
?>
                    <script>

                    $('#insert_menu1').change(function(){
                        if ($(this).val()=='') return false;
                        $('#insert_step2').slideUp();
                        $.post('./?mod=ajax&task=ajax_task_insertMenu&ajaxAction=step2&scheme_id=<?=$scheme_id?>&editorName=<?=$editorName?>',
                            {Questionnaire: $(this).val()}, function(data){
                            $('#insert_step2').html(data).slideDown();
                        })
                    });
                                        
                    </script>
                    
                    <h4><?=$Lang['IES']['ChooseAnItem']?>:</h4>
                    <select id="insert_menu1" name="Questionnaire" style="float:left; width:200px">
                        <option value="">--<?=$Lang['IES']['Select']?>--</option>
                        <? foreach ($SurveyIDs as $SurveyID) : ?>
                            <? $surveyDetails = current($libSurvey->getSurveyDetails($SurveyID)); ?>
                            <option value="<?=$SurveyID?>"><?=$surveyDetails["Title"]?></option>
                        <? endforeach; ?>
                    </select>
                    <div id='insert_step2' style="width:650px; display:none;"></div>
<?
                    break;
                
                case $sba_cfg['DB_IES_STEP_QuestionType']['observeAnalysis'][0]:
                case $sba_cfg['DB_IES_STEP_QuestionType']['interviewAnalysis'][0]:
                case $sba_cfg['DB_IES_STEP_QuestionType']['surveyAnalysis'][0]:
                  
                    $objStepMapper = new SbaStepMapper();
                    $objSteps = $objStepMapper->findByStepIds($libStepHandin->getSelfQuestionDataStructure());
                    
                    $SurveyIDs=array();
                    foreach ($objSteps as $objStep){
			
			
                            $SurveyIDs=array_merge($SurveyIDs, $libStepHandin->getSurveyIDs($objStep->getStepID()));
                    }
                    array_unique($SurveyIDs);
		    
                    $SurveyMappings=array();
                    foreach ($SurveyIDs as $SurveyID){
                            $surveyDetails[$SurveyID] = current($libSurvey->getSurveyDetails($SurveyID));
                            $_SESSION['sba_surveyKeys'][$SurveyID]=$libStepHandin->getQuestionnaireKey($SurveyID);
                            
                            $SurveyMappings=array_merge($SurveyMappings, $libSurvey->loadSurveyMapping($SurveyID, $sba_thisStudentID));
                    }
                    
?>
            
                    <script>
                    $(function(){
                        
                        $('#insert_menu1').change(function(){
                            if ($(this).val()=='') return false;
                            $('#insert_step2').slideUp();
                            $.post('./?mod=ajax&task=ajax_task_insertMenu&ajaxAction=step2&scheme_id=<?=$scheme_id?>&editorName=<?=$editorName?>',
                                $(this).find('option:selected').parent('optgroup').attr('title')+'='+$(this).val(), function(data){
                                $('#insert_step2').html(data).slideDown();
                            })
                        });
                                                  
                    });
                    </script>
                    <h4><?=$Lang['IES']['ChooseAnItem']?>:</h4>

                    <select id="insert_menu1" name="" style="float:left; width:200px">
                        <option value="">--<?=$Lang['IES']['Select']?>--</option>
                        <? if (!empty($SurveyIDs)) :  ?>
                        <optgroup label="<?=$Lang['IES']['ViewResultsByQuestion']?>" title="Analysis">
                            <? foreach ($SurveyIDs as $SurveyID) : ?>
                                <option value="<?=$SurveyID?>"><?=$surveyDetails[$SurveyID]["Title"]?></option>
                            <? endforeach; ?>
                        </optgroup>
                        <? endif; ?>
                        <? if (!empty($SurveyMappings)) :  ?>
                        <optgroup label="<?=$Lang['IES']['GroupingList']?>" title="Comparison">
                            <? foreach ($SurveyMappings as $SurveyMapping) : ?>
                                <option value="<?=$SurveyMapping['SURVEYMAPPINGID']?>"><?=$SurveyMapping["MAPPINGTITLE"]?></option>
                            <? endforeach; ?>
                        </optgroup>
                        <? endif; ?>
                    </select>

                    <div id='insert_step2' style="width:650px; display:none;"></div>
<?                  break;
            }
        }else if('file'){
            
?>
            <script>

                var getFolderList = function(){
                    $('#insert_step2').slideUp();
                    $.post('./?mod=ajax&task=ajax_task_insertMenu&ajaxAction=getFiles&scheme_id=<?=$scheme_id?>&editorName=<?=$editorName?>',
                        {}, function(data){
                        $('#insert_step2').html(data).slideDown();
                    })
                };
                                   
                getFolderList();
                            
            </script>
            <h4><?=$Lang['IES']['Folder']?>:</h4>
                       
            <form id="form_step1">
                    
                <table style="width:100%; border-width: 1px; border-bottom: solid">
                    <col style="width:13%"/><col style="width:87%"/>
                    <tr>
                        <th>&nbsp;</th>
                        <th><?=$Lang['General']['FileNames']?></th>
                    </tr>
                </table>
            <div id='insert_step2' style="width:650px; height:180px; display:none;overflow:scroll;"></div>
            </form>
            
<?
        }else{ echo 'fail'; }
        
    break;

    case 'step2':
        if ($Analysis){
            
            $key = $_SESSION['sba_surveyKeys'][$Analysis];
            
?>
            <script>
                function copyContents(){
                    $('#insert_result').html($('#survey_iframe').contents().find('.ies_q_box').find('.q_option, .my_chart_container .current_chart')).slideDown().find('input').remove();
                    
		    $('#insert_result').find('*').removeAttr('class').removeAttr('id').removeAttr('style').removeAttr('onclick').removeAttr('href');
                }
            </script>       
<?
            echo "<iframe  style='width: 1px;height: 1px;'  id='survey_iframe' src='./?key=$key&mod=survey&task=questionnaireDisplay&callback=window.parent.copyContents'></iframe>";
            echo getFinalHtml("<h5>Loading...</h5>");
            
        }else if ($Comparison || $Comparison=== '0'){
?>
            <script>      
                function copyContents(){
                    $('#insert_result').html($('#survey_iframe').contents().find('.ies_q_box').find('.q_option, #my_chart_parent img')).slideDown().find('input').remove();;
                    $('#insert_result').find('*').removeAttr('class').removeAttr('id').removeAttr('style').removeAttr('onclick').removeAttr('href');
                }
            </script>
<?
            echo "<iframe  style='width: 1px;height: 1px;' id='survey_iframe' src='./?mod=survey&task=questionnaireShowTable&SurveyMappingID=$Comparison&callback=window.parent.copyContents'></iframe>";
            echo getFinalHtml("<h5>Loading...</h5>");
            
        }else if ($Questionnaire){

            echo getFinalHtml($libSurvey->genQuestionsDOC($Questionnaire));
                           
        }else{ echo 'fail'; }
        
    break;
}
function getFinalHtml($content){
    
    $content = empty($content)? $Lang['IES']['TemporarilyNoContent'] : $content;
                
    return "<div contenteditable='true' id='insert_result' style='width:650px; height: 180px; overflow-y: auto;'>
                $content
            </div>";
}
?>