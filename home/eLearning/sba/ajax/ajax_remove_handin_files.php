<?php
# Using : Thomas

# Initialize Object
$libies = new libies();

$q_result = false;
$physicalFileRemoved = $libies->removeHandInFilePhysical($file_id);
if ($physicalFileRemoved) {
	$q_result = $libies->removeHandInFileInStage($file_id);
}



intranet_closedb();
# Output the modified date to the page
header("Content-Type:   text/xml");
$libies = new libies();
$XML = $libies->generateXML(
					array(
						array("result", ($q_result?1:0)),
						array("file_id", $file_id)
					)
				);
echo $XML;
?>