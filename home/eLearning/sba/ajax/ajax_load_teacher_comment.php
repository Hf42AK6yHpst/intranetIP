<?php
# Using : Thomas

$objIES = new libies();
$AllTaskSnapshotAnswerIDArray = array();
$TaskSnapshotAnswer = $objIES->getTaskSnapshotAnswer($task_id, $sba_thisStudentID);


list($tasktype) = $objIES->get_task_type($task_id);

if(sizeof($TaskSnapshotAnswer) > 0){
	
	$TaskSnapshotAnswer_cnt = 0;

	foreach($TaskSnapshotAnswer as $value){
		$AllTaskSnapshotAnswerIDArray[] = $value["SnapshotAnswerID"];
		$teachercomment_ARR = $objIES->getCommentArr($value["SnapshotAnswerID"]);
		$teacherScore_ARR = $objIES->getTaskHandInScore($value["SnapshotAnswerID"]);				

		$snapShotScore = '';
		//requirement on 20110916 , assume there is only one score for each snapshot
		if(is_array($teacherScore_ARR) && count($teacherScore_ARR) > 0){
			$teacherScore_ARR = current($teacherScore_ARR);
			$snapShotScore = "<div class=\"IES_mark icon_tea\">
                					<div class=\"comment_mark\">
										".$Lang['IES']['Mark']." :
										<span>".$teacherScore_ARR['Score']."</span>
									</div>
									<br>
                					<span class=\"update_record\">(".$teacherScore_ARR["DateInput"].$Lang['IES']['CommentBy'].$teacherScore_ARR["ScoreTeacher"].")</span>
                					<div class=\"clear\"></div> <!-- added 20120724 -->
                			  </div>";
			//$snapShotScore = $Lang['IES']['Mark'].' : '.$teacherScore_ARR['Score'].'<span class="update_record"> ('.$teacherScore_ARR["DateInput"].$Lang['IES']['CommentBy'].$teacherScore_ARR["ScoreTeacher"].')</span>';
		}

?>
	<? if($TaskSnapshotAnswer_cnt>0){ ?>
		<br class="clear"/>
	<? } ?>
	
	<div class="IES_comment_entry">
		<? if(!stristr($tasktype, "survey")){ //DON"T display the answer if it is an survey ?>
			<div class="stu_ans">
				<?=nl2br($value["Answer"])?>
				<div style="clear:both"><span class="update_record"><?=$value["AnswerDate"]?></span></div>
			</div>
		<? }
		   
		   echo $snapShotScore; 
			
		   for($i=0;$i<sizeof($teachercomment_ARR);$i++){
				if(!$teachercomment_ARR[$i]["Status"]){
					$new_class = "new";
				}
				else {
					$new_class = "";
				}
		?>
			<div class="IES_comment icon_tea">
				<?=nl2br($teachercomment_ARR[$i]["Comment"])?>
				<br />
				<span class="update_record <?=$new_class?>">
					<?="(".$teachercomment_ARR[$i]["DateInput"]." ".$Lang['IES']['CommentBy']." ".$teachercomment_ARR[$i]["CommentTeacher"].")"?>
				</span>
				<div class="clear"></div>
			</div>
		<? } ?>
	</div> 
	<?   
		$TaskSnapshotAnswer_cnt++;
	} // close foreach($TaskSnapshotAnswer as $value)
	$objIES->updateTeacherCommentStatus($AllTaskSnapshotAnswerIDArray, 1);	
}
else{
	echo "0";
}
?>

                
                
            
                
              
                
        