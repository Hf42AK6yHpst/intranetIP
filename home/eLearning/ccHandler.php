<?php
/*
 * Modify by : Siuwan
 *
 * login classroom and go to eContent / Assessment
 *
 */
include ("../../includes/global.php");
include ("../../includes/libdb.php");

 // $ccToken = encrypt_string("cid=609&t=NOTE&fid=422");
 // var_dump($ccToken);
 // VkVVbVptbGtQVFF5TWc9PVkybGtQVFl3T1NaMFBVNVA9MTY=
if (! isset($ccToken) || empty($ccToken)) {
    echo "Invaid token.";
    exit();
}

intranet_auth();
intranet_opendb();
parse_str(decrypt_string($ccToken), $ccParams);

$courseId = isset($ccParams['cid']) ? IntegerSafe($ccParams['cid']) : null;

if (is_null($courseId)) {
    echo "Invaid course id.";
    exit();
}
$libdb = new libdb();
$sql = "
    SELECT
        user_course_id
    FROM
        " . $eclass_db . ".user_course 
    WHERE
        course_id = '" . $courseId . "'
    AND
        intranet_user_id = '" . $_SESSION['UserID'] . "'
";

$userCourseId = current($libdb->returnVector($sql));

if ($userCourseId > 0) {
    header("Location: login.php?uc_id=" . $userCourseId . "&jumpback=cc&r_var=" . $ccToken);
}else{
    echo "Record not found.";
}

intranet_closedb();
exit();
?>