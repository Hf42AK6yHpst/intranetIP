<?php
/**
 * Change Log:
 * 2019-01-14 Pun [155611] [ip.2.5.10.1.1]
 *  - Fixed cannot upload files
 * 2018-02-21 Pun [135283] [ip.2.5.9.3.1]
 *  - Added multiple file upload
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

if ($courseID=="" || $categoryID=="")
{
	header("Location: index.php");
	die();
}

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] && (!(isset($sys_custom['project']['NCS'])&&$sys_custom['project']['NCS']==true && $_SESSION['UserType']==1 && $_SESSION['isTeaching']==1)))
{
  if (!$lu->teaching || !$la->isAccessEClassMgtCourse())
  {
    header("Location: /");
  }
}


$ori_memberType = $ck_memberType;
$ck_memberType = "Z";

$fm = new fileManager($courseID, $categoryID, $folderID);
$vPath = stripslashes($fm->getVirtualPath());

$js_big5file = generateFileUploadNameHandler("form1","userfile1","userfile1_hidden");

$linterface = new interface_html();
$CurrentPage = "PageShareArea";

$li = new libuser($UserID);
$lo = new libeclass();
$leclass = new libeclass2007();
$lp = new libportal();

#### File upload START ####
$pluploadButtonId = 'UploadButton';
$pluploadFileListDivId = 'FileListDiv';
$pluploadContainerId = 'pluploadDiv';
$pluploadBtn = $linterface->GET_SMALL_BTN($Lang['General']['PleaseSelectFiles'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
#### File upload END ####


### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_directory.gif' align='absmiddle' /> $i_eClass_Admin_Shared_Files";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($i_eClass_Admin_Shared_Files, "index.php");	
$PAGE_NAVIGATION[] = array($button_upload, "");	
?>
<script language="JavaScript" src="/templates/jquery/jquery-1.12.4.min.js"></script>
<?=$linterface->Include_Plupload_JS_CSS()?>
<?=$js_big5file?>


<form id="form1" name=form1 method="post" action="file_upload_update.php" enctype="multipart/form-data">
<br>
<table width=90% border=0 cellpadding=5 cellspacing=0 align="center">
  <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
  </tr>
  <tr>
  <td colspan="2">
        <table width="70%" border="0" cellspacing="0" cellpadding="5" align="center"> 
        <tr>
        <td width=20% class="formfieldtitle"><span class="tabletext"><?php echo $file_location; ?>:</span>&nbsp;</td>
        <td><?=$vPath?></td>
        </tr>
        
        <tr>
        <td width=20% class="formfieldtitle"><span class="tabletext"><?php echo $file_file; ?>:</span>&nbsp;</td>
        <td>
            <div id="<?=$pluploadFileListDivId?>">
            	 <!--div class="fileDetails">
            	 	<input type="hidden" name="uploaded_files[]" value="" />
            	 	<span>abc.jpg (123 KB)</span>
            	 	[
            	 		<a class="tabletool" href="javascript:void(0);"><?=$Lang['Btn']['Delete'] ?></a>
        	 		]
            	 </div-->
            </div>
            <div id="<?=$pluploadContainerId?>" style="margin-bottom: 5px;"></div>
			<?=$pluploadBtn?>
		</td>
        </tr>
        
        <tr>
        <td class="formfieldtitle" valign="top"><span class="tabletext"><?php echo $bulletin_des; ?>:</span>&nbsp;</td>
        <td><textarea class="inputfield" name=description cols=40 rows=5 wrap=virtual></textarea></td>
        </tr>
        
        <tr>
        <td class="formfieldtitle" nowrap><span class="tabletext"><?= $file_access_right ?>(<?=$file_alluser?>) :</span>&nbsp;</td>
        <td>
          <table width="300" border="0" cellspacing="1" cellpadding="0">
        	<tr>
        	  <td align="left" width="100" class="bodycolor5"><?=$file_read?></td>
        	  <td align="left" width="100" class="bodycolor5"><?=$file_readwrite?></td>
        	</tr>
        	<tr>
        	  <td align="left" class="bodycolor3">
        		<input type="checkbox" name="access_all[]" value="AR" checked onClick="switchCheck(this.form, 'access_all[]', 'AW')">
        	  </td>
        	  <td align="left" class="bodycolor3">
        		<input type="checkbox" name="access_all[]" value="AW" onClick="switchCheck(this.form, 'access_all[]', 'AR')">
        	  </td>
        	</tr>
          </table>
        </td>
        </tr>
      </table>
   </td>
   </tr>
</table>

<table width="70%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
  <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
  <td align="center">
    <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
    <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
    <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
  </td>
</tr>
</table>
<input name="courseID" value="<?=$courseID?>" type=hidden>
<input name="categoryID" value="<?=$categoryID?>" type=hidden>
<input name="folderID" value="<?=$folderID?>" type=hidden>
<input name="file_no" value="1" type="hidden">
<p></p>
</form>

<script>

$(document).ready( function() {
	var file_uploader = {};
	var tempFiles = [];
	var isUploadDone = true;
	
	initUploadAttachment('<?=$pluploadContainerId?>','<?=$pluploadButtonId?>');
	function initUploadAttachment(containerId,buttonId)
	{
		file_uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
	        browse_button : buttonId,
	        container : containerId,
	        file_data_name 		: 'Filedata',
	        multi_selection     : true,
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : '//<?=$eclass40_httppath ?>/src/tool/plupload/fileupload.php',
			multipart_params 	: {
				enableDocToHtml : 0,
				courseID: '<?php echo $courseID;?>',
				// categoryID: '<?php echo $categoryID;?>', // Upload to temp folder, same as direct upload 				
			}
	    });
	    
		file_uploader.init();
		
		file_uploader.bind('BeforeUpload', function(up, file){
		});
		
		file_uploader.bind('FilesAdded', function(up, files) {
	        $.each(files, function(i, file) {
	            $('#'+containerId).append(
	                '<div id="' + file.id + '">' +
	                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
	            '</div>');
	        });

	        up.refresh(); // Reposition Flash/Silverlight
	        up.start();
	        isUploadDone = false;
	    });
	    
	    file_uploader.bind('UploadProgress', function(up, file) {
	        $('#' + file.id + " b").html(file.percent + "%");
		});
	    
	    file_uploader.bind('FileUploaded', function(up, file, response) {
			var uploaderResult = $.parseJSON(response.response);
			tempFiles.push(uploaderResult);
		});
		
	    file_uploader.bind('UploadComplete', function(up, files) {
    		var $fileListDivId = $('#<?=$pluploadFileListDivId ?>');

			$.each(tempFiles, function(index, file){
    			if(file.result["result"]==false){
    				alert("<?=$Lang['System_Msg']['Survey_Fileupload'][0] ?>");
    				return false;
    			}

    			//// Add files display START ////
    			var $div = $('<div>');
    			var $input = $('<input>');
    			var $span = $('<span>');
    			var $a = $('<a>');

    			// Container START //
    			$div.addClass('fileDetails');
    			$fileListDivId.append($div);
    			// Container END //
    			
    			// Submit value START //
    			$input.attr({
        			'type': 'hidden',
        			'name': 'uploaded_files[]',
        			'value': file.path
    			});
    			$div.append($input);
    			// Submit value END //
    			
    			// Display file name START //
				var sizeHtml = '';
				var size = file.size;
				if(size > (1024 * 1024)){
					sizeHtml = parseFloat(size / 1024 / 1024).toFixed(2) + ' MB';
				}else{
					sizeHtml = parseFloat(size / 1024).toFixed(2) + ' KB';
				}
				$span.html(file.name + ' (' + sizeHtml + ')&nbsp;');
				$div.append($span);
    			// Display file name END //
    			
				// Delete btn START //
				$a.attr({
					'class': 'tabletool',
					'href': 'javascript:void(0);'
				})
    				.html('[<?=$Lang['Btn']['Delete'] ?>]')
    				.click(function(){
    					$(this).closest('.fileDetails').remove();
    				});
				$div.append($a);
				// Delete btn END //
    			//// Add files display END ////
			});
		    tempFiles.length = 0;
			$('#' + containerId + ' *').hide();
			isUploadDone = true;
		});
	}


	$('#form1').submit(function(e){
		if(!isUploadDone){
			alert('<?=$Lang['Gamma']['Warning']['AttachmentUploadInProgress'] ?>');
			return false;
		}
		return true;
	});
});
</script>

<?php
$ck_memberType = $ori_memberType;
$linterface->LAYOUT_STOP();
intranet_closedb();
?>