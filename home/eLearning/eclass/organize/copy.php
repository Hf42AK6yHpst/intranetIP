<?php
//Modifying By 
/**
 * Change Log:
 * 2020-05-06 (Tommy)
 *  - modified permission checking, added $plugin["Disable_eClassroom"]
 * 2016-04-07 (Pun) [84012] [ip.2.5.7.4.1]
 *  - Added $subjectID to eClass classroom for directory
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
eclass_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

#### subject and subject group select box #####
$libdb = new libdb();
$fieldName = $intranet_session_language=="en"?"EN_DES":"CH_DES";
$sql = "select
	ASS.RecordID,
	ASS.{$fieldName} as SubjectName
from
	ASSESSMENT_SUBJECT AS ASS
LEFT JOIN
	LEARNING_CATEGORY LC
ON
	ASS.LearningCategoryID = LC.LearningCategoryID
where
	ASS.RecordStatus = 1
AND
	(ASS.CMP_CODEID IS NULL OR ASS.CMP_CODEID = '')
ORDER BY
	(ASS.LearningCategoryID IS NULL OR ASS.LearningCategoryID = ''),
	LC.DisplayOrder,
	ASS.DisplayOrder
";
$result = $libdb->returnArray($sql); 

$sql = "SELECT SubjectID FROM {$eclass_db}.course where course_id='".$course_id[0]."' ";
$rs = $libdb->returnVector($sql);
$subjectLinkID = $rs[0];

######## Subject Link START ########
#### Generate HTML START ####
if($sys_custom['eClass']['EnableLinkToSubject']){
	$subjectSelectHTML = '<select id="subjectLinkID" name="subjectLinkID">';
	$subjectSelectHTML .= "<option value=''>--{$button_select}--</option>";
	foreach ($result as $r){
		$selected = ($subjectLinkID==trim($r["RecordID"])?"selected ":"");
		$subjectSelectHTML .= '<option value="'.$r["RecordID"].'" '.$selected.'>'.$r["SubjectName"].'</option>';
	}
	$subjectSelectHTML .= '</select>';
}
#### Generate HTML END ####
######## Subject Link END ########

######## Subject Group START ########
$subjectBox = '<select id="subjectID" name="subjectID" onchange="getSelectGroup()">';
$subjectBox .= "<option value='' selected>--{$button_select}--</option>";
foreach ($result as $r){
	$subjectBox .= '<option value="'.$r["RecordID"].'" >'.$r["SubjectName"].'</option>';
}
$subjectBox .= '</select>';
$subjectBox .= '&nbsp;&nbsp;<div id="subjectGroupSelectBox" style="display:inline"></div>';

$groupList = "<select name='subjectGroupID[]' id='subjectGroupList' multiple>";  
$groupList .= "</select>";

$subjectBox .= '<div id="subjectGroupListBox">'.$groupList.'<br />
'.$linterface->GET_ACTION_BTN($Lang['Btn']['RemoveSelected'], "button", "javascript:subjectGroupHandler.removeFromList();",$Lang['Btn']['RemoveSelected']).'
</div>';
######## Subject Group END ########
###############################################

#################### Subject Group Mapping ########################
$sql = "select t.SubjectID, t.SubjectGroupID 
		from {$intranet_db}.SUBJECT_TERM as t
		where
			t.YearTermID in (
				select
					ayt.YearTermID
				from
					{$intranet_db}.ACADEMIC_YEAR as ay
					inner join
					{$intranet_db}.ACADEMIC_YEAR_TERM as ayt
					on ay.AcademicYearID = ayt.AcademicYearID
				where NOW() between ayt.TermStart and ayt.TermEnd
			)
			order by t.SubjectID, t.SubjectGroupID 
		";
$result = $libdb->returnArray($sql);
$asso = array();
$jsMapping = '{}';
if (!empty($result)){
	foreach($result as $r){
		$asso[$r[0]][] = $r[1];
	}
	$jsMapping = '{';
	foreach($asso as $sid=>$gid){
		$jsMapping .= $sid.':[';
		if (!empty($gid)){
			$jsMapping .= implode(',',$gid);
		}
		$jsMapping .= '],';
	}
	$jsMapping= rtrim($jsMapping,',');
	$jsMapping .= '}';
}
###################################################################

# eClass
$lo = new libeclass($course_id[0]);
$eclass_quota = $lo->status();
$lelcass = new libeclass2007();

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] || $plugin["Disable_eClassroom"])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
  {
    header("Location: /");
  }
}
# verify user right
// get right of creator of the course / admin
$sql = "SELECT course_id FROM course where course_id='".$course_id[0]."' ";
$sql .= ($_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])? " LIMIT 1 " : " AND creator_id='$UserID' LIMIT 1 ";
$row = $lo->returnArray($sql, 1);
$isAccessGained = ($row[0][0]!="");
if (!$isAccessGained)
{
  // get right of teacher identity in the course
  $sql = "SELECT course_id FROM user_course where user_email='".$lu->UserEmail."' AND memberType='T' AND course_id='".$course_id[0]."' LIMIT 1";
  $row = $lo->returnArray($sql, 1);
  $isAccessGained = ($row[0][0]!="");
}




$tool_select = "";
# Check Equation Editor
if ($lo->license_equation !=0)
{
	$content = trim(get_file_content($lo->filepath."/files/equation.txt"));
	$equation_class = ($content=="")? array(): explode(",",$content);
	$left = $lo->license_equation - sizeof($equation_class);
	if ($left > 0)
	{
		$tool_select .= "<input type=checkbox name=hasEquation value=1> $i_eClass_Tool_EquationEditor ($i_eClass_Tool_LicenseLeft: $left)";
	}
}

### Title ###
$TAGS_OBJ[] = array($i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $lelcass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_copy.($intranet_session_language=="en"?" ":""). $i_adminmenu_eclass, "");
?>

<script language="javascript">
var input_value = "";
function checkform(obj){
	 $('#subjectGroupList option').attr('selected','selected');
  if(!check_text(obj.course_code, "<?php echo $i_alert_pleasefillin.$i_eClassCourseCode; ?>.")) return false;
  if(!check_text(obj.course_name, "<?php echo $i_alert_pleasefillin.$i_eClassCourseName; ?>.")) return false;
  if ($('#subjectGroupList option:selected').length == 0){
	alert('<?=$Lang['eclass']['warning']['noSubjectGroup']?>');
  }
}

function check_number(f){
	var patt = new RegExp(/^[\d]+$/);
	if((f.value!="")&&(!patt.test(f.value))){
		f.value=input_value;
		return false;
	}else{
		input_value = f.value;
		return true;
	}
}

function getSelectGroup(){
	selectedValue = document.getElementById('subjectID').options[document.getElementById('subjectID').selectedIndex].value;
	document.body.style.cursor='wait';
	var param = {};
	param['subjectID'] = selectedValue;
	
	var sbjList= [];
	$('#subjectGroupList option').each(function(){
		sbjList.push(this.value);
	});
	param['exclusList[]'] = sbjList;
	
	$('div#subjectGroupSelectBox').load(
		'getSelectSubjectGroup.php',
		param,
		function(){
			document.body.style.cursor='default';
		}
	);
}

var subjectGroupHandler=(function(){
	var groupMapping = <?=$jsMapping?>;
	return {
		addSubjectGroup:function (obj){
			if (obj.selectedIndex == 0){
				return;
			}
			
			var selectedOpt = $('#subjectGroupID option:selected');
			
			$('#subjectGroupList').append(selectedOpt);
		},
		removeFromList:function(){
			var groupList = groupMapping[$('#subjectID option:selected').val()];
			$('#subjectGroupList option:selected').each(function(){
				if (groupList instanceof Array && $.inArray(parseInt(this.value),groupList)!=-1){
					this.selected = false;
					 $('#subjectGroupID').append(this);
				}
				else{
					$(this).remove();
				}
			});
		}
	}	
})();

</script>


<br />   
<form name="form1" action="copy_update.php" method="post" onSubmit="return checkform(this);">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
      <td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
    </tr>

    <tr>
      <td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td><br />
              <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
<? if ($isAccessGained) {?>
<?php if($lo->license == "" || $lo->ticket()>0) { ?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseCode?> <span class='tabletextrequire'>*</span></span></td>
                  <td><input name="course_code" type="text" class="textboxnum" maxlength="10" value="<?php echo $lo->course_code; ?>" /></td>
                </tr> 
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseName?> <span class='tabletextrequire'>*</span></span></td>
                  <td><input name="course_name" type="text" class="textboxtext" maxlength="100" value="Copy Of <?php echo $lo->course_name; ?>" /></td>
                </tr> 
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseDesc?> </span></td>
                  <td><?=$linterface->GET_TEXTAREA("course_desc", $lo->course_desc);?></td>
                </tr> 
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassNumUsers?> </span></td>
                  <td><input name="max_user" type="text" class="textboxnum" maxlength="5" value="<?php echo $lo->max_user; ?>" /></td>
                </tr> 
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassMaxStorage?> </span></td>
                  <td class="tabletext"><input name="max_storage" type="text" class="textboxnum" maxlength="5" value="<?php echo $lo->max_storage; ?>" /> MB</td>
                </tr> 
				<?php 
                	$la->retrieveEClassSettings();
          			$default_max_size = $la->eclass_settings_loaded["default_max_size"]; 
          			if ($default_max_size=="" || $default_max_size < 0){
          				$size = ($lo->max_size == -1) ? "": $lo->max_size; ?>
                		<tr valign="top">
                  			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eclass']['default_max_size']?> </span></td>
                  			<td class="tabletext"><input name="max_size" type="text" class="textboxnum" maxlength="5" onpropertychange="check_number(this)" oninput="check_number(this)" onpaste="check_number(this)" value="<?php echo $size; ?>" /> MB</td>
                		</tr>
          		<?php
          			};
          		?>
          		
				<!-- subject link  -->
				<?php if($sys_custom['eClass']['EnableLinkToSubject']){ ?>
					<tr valign="top">
	                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['SysMgr']['Homework']['Subject']?></span></td>
	                  <td class="tabletext mayHidden"><?=$subjectSelectHTML?></td>
	                </tr>
                <?php } ?>
                
				<!-- subject group -->
				<tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></span></td>
                  <td class="tabletext"><?=$subjectBox?></td>
                </tr>
				
<? if ($tool_select != "") { ?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClass_Tools?> </span></td>
                  <td class="tabletext"><?=$tool_select?></td>
                </tr>
<? } ?>
<!--				
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
                  <td class="tabletext"><?php echo $lo->eClassCategory(); ?></td>
                </tr>
-->                
<?php if ($eclass_version >= 3.0) { ?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassAdminFileRight?> </span></td>
                  <td><input type="checkbox" value="1" name="is_release_file"></td>
                </tr>
<?php } ?>
         
<? } else {?>
                <tr valign="top">
                  <td class="tabletext" colspan="2" align="center"><?=$i_eClassLicenseFull?></td>
                </tr> 
<? } ?>
<? } else {?>
                <tr valign="top">
                  <td class="tabletext" colspan="2" align="center"><br><br><blockquote><blockquote><b>Sorry, you don't have the access right to add user to this course!</b></blockquote></blockquote></td>
                </tr>
<? } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<? if ($isAccessGained and ($lo->license == "" || $lo->ticket()>0)) {?>
          <tr>
            <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
          </tr>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
              <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
<? } else {?>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
<? } ?>
        </table>                                
      </td>
    </tr>
  </table>
  <br />

  <input type="hidden" name="course_id" value="<?php echo $lo->course_id; ?>" />
</form>

<?php
print $linterface->FOCUS_ON_LOAD("form1.course_code");
$linterface->LAYOUT_STOP();
eclass_closedb();
?>