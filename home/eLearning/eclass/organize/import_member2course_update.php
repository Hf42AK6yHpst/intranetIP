<?php

// Modifing by : 

/********************** Change Log ***********************/
#
#   Date    :   2020-05-06 (Tommy)
#   Details :   modified permission checking, added $plugin["Disable_eClassroom"]
#
#   Date	:	2018-09-14 [Rox]
#	Details	:	Modified coomants "no suchs user" to "no suchs course"
#
#	Date	:	2016-10-07 [Ronald]
#	Details	:	Added $_SESSION['LoginPrefix'] for import user account for import student to powerlesson
#
#	Date	:	2015-10-19 [Pun] ip2.5.6.12.1
#	Details	:	Added sync student to subject group
#
#	Date	:	2014-02-12 [Jason]
#	Details	:	improve to force the course_id which is retrieved from the unicode file to be integer.
#
# 	Date	:	2011-08-23 [Yuen]
#	Details	:	added this function
#
/******************* End Of Change Log *******************/

@SET_TIME_LIMIT(600);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
eclass_opendb();

# block illegal access
$la = new libaccess($UserID);
$la->retrieveEClassSettings();
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] || $plugin["Disable_eClassroom"])
{
	if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
	{
		header("Location: /");
	}
}
$limport = new libimporttext();
$iCal = new icalendar();


// YuEn: temporary
if ($MemberType=="")
{
	die ("No member type (S, T or A) selected!");
} else
{
	if ($MemberType=="S")
	{
		$RoleTitle = $i_eClass_group_student;
	} elseif ($MemberType=="T")
	{
		$RoleTitle = $i_eClass_group_teacher;
	} elseif ($MemberType=="A")
	{
		$RoleTitle = $i_eClass_group_assistant;
	}
}

$format_array = array("UserLogin","CourseID");
$li = new libdb();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
	header("Location: import_member2course.php?failed=2&MemberType=".$MemberType);
	exit();
} else {

	$data = $limport->GET_IMPORT_TXT($filepath);

	$toprow = array_shift($data);# drop the title bar
	for ($i=0; $i<sizeof($format_array); $i++)
	{
		if (strtoupper(trim($toprow[$i])) != strtoupper($format_array[$i]))
		{
			header("Location: import_member2course.php?xmsg=import_header_failed");
			exit();
		}
	}

	if($sys_custom['sahk_login_without_prefix']){
		foreach($data as $key=>$value){
			$data[$key][0] = $_SESSION['LoginPrefix'].$data[$key][0];
		}
	}
	$lo = new libeclass();

	# preload all students and teachers in associative array
	$sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, Title, EnglishName, ChineseName , NickName, TitleEnglish, TitleChinese, HomeTelNo, Address, FaxNo,DateOfBirth, UserLogin
				FROM INTRANET_USER WHERE recordstatus=1 AND recordtype IN (1, 2)";
	$row = $li->returnArray($sql,17);

	for ($i=0; $i<sizeof($row); $i++)
	{
		$UserLogin = $row[$i][17];
		$users[$UserLogin] = $row[$i];
	}


	# preload all courses
	$sql = "SELECT course_id, course_name FROM {$eclass_db}.course WHERE roomtype=0";
	$row = $li->returnArray($sql,2);
	for ($i=0; $i<sizeof($row); $i++)
	{
		$courses[] = $row[$i][0];
			
		$courses_title[$row[$i][0]] = $row[$i][1];
	}

	$Errors = array();

	# tidy up data
	for ($i=0; $i<sizeof($data); $i++)
	{
		$data[$i][0] = trim($data[$i][0]);
		$data[$i][1] = (int)trim($data[$i][1]);
		list($user_login,$course_id) = $data[$i];
		// echo "$i: $user_login,$course_id,$group_name<br>";


		if (!is_array($users[$user_login]))
		{
			# no such user
			if($sys_custom['sahk_login_without_prefix']){
				$user_login = str_replace($_SESSION['LoginPrefix'],'',$user_login);
			}
			$Errors[] = $Lang['General']['ImportArr']['Row'].($i+2) . " - ".$Lang['eclass']['import_member_error_no_user']." '{$user_login}'! ";
		}
		if (!in_array($course_id, $courses))
		{
		    # no such course
			$Errors[] = $Lang['General']['ImportArr']['Row'].($i+2) . " - ".$Lang['eclass']['import_member_error_no_course']." {$course_id}! ";
		}

	}

	$courses_involved = array();
	if (sizeof($Errors)<1)
	{
		# tidy up data
		for ($i=0; $i<sizeof($data); $i++)
		{
			list($user_login,$course_id) = $data[$i];
			$UserObj = $users[$user_login];

			$User_ID = $UserObj[0];
			$UserEmail = $UserObj[1];
			$UserPassword = $UserObj[2];
			$ClassNumber = $UserObj[3];
			$FirstName = $UserObj[4];
			$LastName = $UserObj[5];
			$ClassName = $UserObj[6];
			$Title = $UserObj[7];
			$EngName = $UserObj[8];
			$ChiName = $UserObj[9];
			$NickName = $UserObj[10];
			$titleEng = $UserObj[11];
			$titleChi = $UserObj[12];

			$HomeTelNo = $UserObj[13];
			$Address = $UserObj[14];
			$FaxNo = $UserObj[15];
			$DateOfBirth = $UserObj[16];

			$eclassClassNumber = (trim($ClassName) == "") ? $ClassNumber : trim($ClassName)." - ".trim($ClassNumber);
			/*
				if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
					$eclassClassNumber = $ClassNumber;
					else
						$eclassClassNumber = $ClassName ." - ".$ClassNumber;
						*/

					$lo = new libeclass($course_id);

					# check if exist (according to UserEmail)
					$sql_check = "SELECT user_course_id FROM {$eclass_db}.user_course WHERE course_id='$course_id' AND user_email='".addslashes($UserEmail)."' ";
					$row_check = $lo->returnVector($sql_check);
					if (sizeof($row_check)==1)
					{
						$Skips[] = $Lang['General']['ImportArr']['Row'].($i+2) . " - ".$Lang['eclass']['import_member_error_user']."! ";
					} else
					{
						$lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info, "", $User_ID,$titleEng,$titleChi);
						$course_user_added[$course_id] ++;
					}

					if (!in_array($course_id, $courses_involved))
					{
						$courses_involved[] = $course_id;
					}
					$course_users[$course_id][] = $User_ID;
		}
			
		# update course information and iCalendar access rights
		for ($i=0; $i<sizeof($courses_involved); $i++)
		{
			$course_id = $courses_involved[$i];
			$lo = new libeclass($course_id);
			$lo->eClassUserNumber($lo->course_id);

			# insert calendar viewer
			$sql = "select CalID from course where course_id = '$course_id'";
			$calID = $lo->returnVector($sql);
			$sql = "select UserID from CALENDAR_CALENDAR where CalID = '".$calID[0]."'";
			$existUser = $iCal->returnVector($sql);
			$newUser = array_diff($course_users[$course_id],$existUser);
			$iCal->insertCalendarViewer($calID[0],$newUser,"W",$course_id,'C');
		}
			
		# results
		# some problems, cannot import...
		if (is_array($Skips) && sizeof($Skips)>0)
		{
			$result_table .= "<tr><td colspan='2'><br /><fieldset class='instruction_box'><legend class='instruction_title''>".$Lang['eclass']['import_member_skipped']."</legend>\n";
			$result_table .= "<table border='0' cellpadding='5' cellspacing='2'>";
			for ($i=0; $i<sizeof($Skips); $i++)
			{
				$result_table .= "<tr><td>&nbsp;</td><td>".$Skips[$i]."</td></tr>\n";
			}
			$result_table .= "</table></fieldset></td></tr>\n";
		}
			
		if (is_array($course_user_added) && sizeof($course_user_added)>0)
		{

			$result_table .= "<tr><td colspan='2'><br /><fieldset class='instruction_box'><legend class='instruction_title''>".$Lang['eclass']['import_member_success']."</legend>\n";
			$result_table .= "<table border='0' cellpadding='5' cellspacing='2'>";
			$result_table .= "<tr><td nowrap='nowrap'>&nbsp; &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap' align='right' style='border-bottom:1px solid grey;'>".$Lang['SysMgr']['SubjectClassMapping']['eClass']."</td><td style='border-bottom:1px solid grey;'>".$Lang['eclass']['import_member_success_amount']."</td></tr>\n";
			foreach ($course_user_added as $course_id => $import_tatal)
			{
				$result_table .= "<tr><td>&nbsp;</td><td nowrap='nowrap' align='right'>".$courses_title[$course_id]."</td><td>".$course_user_added[$course_id]."</td></tr>\n";
			}
			$result_table .= "</table> </fieldset></td></tr>\n";

		}
		$button_finish_cancel = $Lang['Btn']['Done'];
	} else
	{
		# some problems, cannot import...
		$result_table .= "<tr><td colspan='2'><br /><font color='red'>".$Lang['eclass']['import_member_error_failed']."</font></td></tr>\n";
		for ($i=0; $i<sizeof($Errors); $i++)
		{
			$result_table .= "<tr><td>&nbsp;</td><td>".$Errors[$i]."</td></tr>\n";
		}
		$button_finish_cancel = $Lang['Btn']['Cancel'];
	}

}

foreach ((array)$course_user_added as $course_id => $import_tatal){
	$subjectGroupIdArr = $lo->returnSubjectGroupID($course_id);
	$lo->createSubjectGroupForClassroom($course_id, $subjectGroupIdArr);
	$lo->syncUserToSubjectGroup($course_id);
}

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

$lelcass	= new libeclass2007();



### Title ###
$TAGS_OBJ[] = array($i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $lelcass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($Lang['eclass']['import_member2course'], "");

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_imported_result . " (".$RoleTitle.")", 1);
?>



<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
      <td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
    </tr>
</table>

<form name="form1" action="import_member2course_update.php" method="post" enctype="multipart/form-data">
<?= $linterface->GET_STEPS($STEPS_OBJ) ?>

<table id="html_body_frame" width="88%" align="center" border="0" cellspacing="0" cellpadding="5">



<?=$result_table?>


<tr>
	<td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='import_member2course.php?MemberType=".$MemberType."'","retrybtn") ?>
              <?= $linterface->GET_ACTION_BTN($button_finish_cancel, "button", "self.location='index.php'","finishbtn") ?>
            </td>
          </tr>
		</table>
	</td>
</tr>
</table>


</form>

<?php

$linterface->LAYOUT_STOP();
eclass_closedb();
?>