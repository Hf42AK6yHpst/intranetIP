<?php

// Modifing by : 

/********************** Change Log ***********************/
#
#   Date    :   2020-05-06 (Tommy)
#   Details :   modified permission checking, added $plugin["Disable_eClassroom"]
#
# 	Date	:	2020-04-09 (Ray)
#	Details	:	created
# no csv uploaded
# wrong header
#
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

include_once($PATH_WRT_ROOT."includes/libimporttext.php");

$linterface = new interface_html();
$limport = new libimporttext();

intranet_auth();
eclass_opendb();

$lo 		= new libeclass();
$lelcass	= new libeclass2007();
$eclass_quota 	= $lo->status();
$lu 		= new libuser($UserID);

# block illegal access
$la = new libaccess($UserID);
$la->retrieveEClassSettings();
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] || $plugin["Disable_eClassroom"])
{
	if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
	{
		header("Location: /");
	}
}

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

### Title ###
$TAGS_OBJ[] = array($i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $lelcass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($Lang['eclass']['import_course_and_assign_teacher'], "");


$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


?>


	<br />
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
		</tr>
	</table>

	<form name="form1" action="import_course_and_assign_teacher_update.php" method="post" enctype="multipart/form-data">
		<?= $linterface->GET_STEPS($STEPS_OBJ) ?>

		<table id="html_body_frame" width="88%" align="center" border="0" cellspacing="0" cellpadding="5">



			<tr>
				<td align=left nowrap><span class="tabletext"><?=$Lang['General']['SourceFile'] . " <span class='tabletextremark'>" .$Lang['General']['CSVFileFormat']."</span>"?> <span class='tabletextrequire'>*</span></span></td>
				<td><input type=file size=50 name=userfile><br /><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
					<?php if ($failed==2) {echo "<font color='red'>".$Lang['eclass']['import_member_csv_alert']."</font>";} ?>
				</td>
			</tr>

			<tr>
				<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
				<td class="tabletext"><a class=functionlink_new href="<?= GET_CSV("import_course_and_assign_teacher.csv", '', false)?>" target=_blank><?=$i_general_clickheredownloadsample?></a></td>
			</tr>
			<tr>
				<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
				<td class="tabletext"><?=$Lang['eclass']['import_course_and_assign_teacher_csv_format']?></td>
			</tr>

			<tr>
				<td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
						<tr>
							<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
						</tr>
						<tr>
							<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
						</tr>
						<tr>
							<td align="center">
								<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
								<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'","cancelbtn") ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>


	</form>

<?php

$linterface->LAYOUT_STOP();
eclass_closedb();
?>