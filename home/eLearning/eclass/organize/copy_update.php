<?php
//Modifying By 
/**
 * Change Log:
 * 2020-05-06 (Tommy)
 * - modified permission checking, added $plugin["Disable_eClassroom"]
 * 2018-12-10 (Pun) [153810] [ip.2.5.10.1.1]
 *  - Added set_time_limit() to 30 minutes
 * 2016-04-07 (Pun) [84012] [ip.2.5.7.4.1]
 *  - Added $subjectID to eClass classroom for directory
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
intranet_auth();
eclass_opendb(); 

set_time_limit(30 * 60);
// debug_r($_REQUEST);
// exit;
$course_code = intranet_htmlspecialchars($course_code);
$course_name = intranet_htmlspecialchars($course_name);
$course_desc = intranet_htmlspecialchars($course_desc);
$max_user = intranet_htmlspecialchars(0 + $max_user);
$max_storage = intranet_htmlspecialchars(0 + $max_storage);
$max_size = intranet_htmlspecialchars(0 + $max_size);
if($max_size <= 0) $max_size = -1;
if($max_user == 0) $max_user = "NULL";
if($max_storage == 0) $max_storage = "NULL";

$lo = new libeclass();

$lu = new libuser($UserID);
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] || $plugin["Disable_eClassroom"])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
  {
    header("Location: /");
    die();
  }
}
if($lo->license != "" &&  $lo->ticket() == 0){
  die();
}else{
  $course_id = $lo->eClassCopy($course_id, $course_code, $course_name, $course_desc, $max_user, $max_storage, $UserID, $subjectGroupID, $max_size, $subjectLinkID);
  $lo->eClassSubjectUpdate($subj_id, $course_id);
  if ($eclass_version >=3.0 && $is_release_file) {
    $sql = "UPDATE eclass_file SET memberType='S', User_id=0, UserName=null, UserEmail=null WHERE memberType='T'";
    $lo->db = $eclass_prefix."c$course_id";
    $lo->db_db_query($sql);
  }
}

# update subject group
if (isset($subjectGroupID) && !empty($subjectGroupID)){
$libdb = new libdb();
$sql = "update SUBJECT_TERM_CLASS set course_id = $course_id 
		where
			SubjectGroupID in ('".implode("','",array_unique($subjectGroupID))."')
		";
 $libdb->db_db_query($sql);
} 
# insert course Calendar
$iCal = new icalendar();
$calID = $iCal->createSystemCalendar($course_name,3,"P",$course_desc);
$sql = "Update course set CalID = '$calID' where course_id = '$course_id'";
$lo->db_db_query($sql);
$dn = $eclass_prefix."c$course_id";
$sql = "select user_email from $dn.usermaster where status <> 'deleted' or status is null";
$userEmail = $lo->returnVector($sql);
$sqlEmail = empty($userEmail)?"":implode("','",$userEmail);
$sql = "select UserID from INTRANET_USER where UserEmail in ('$sqlEmail')";
$newUser = $iCal->returnVector($sql);
$iCal->insertCalendarViewer($calID,$newUser,"W",$course_id,'C');

# Set Equation Editor Rights
if ($hasEquation == 1)
{
    $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
    $equation_class = ($content=="")? array(): explode(",",$content);
    $equation_class[] = $course_id;
    write_file_content(implode(",",$equation_class),$lo->filepath."/files/equation.txt");
}

eclass_closedb();
header("Location: index.php?xmsg=add");

?>