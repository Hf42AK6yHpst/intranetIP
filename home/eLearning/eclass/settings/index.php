<?php
// Modifing by :  Pun

/********************** Change Log ***********************/
#	Date	:	2020-03-30 (Pun)
# 	Details	:	 Added eLearning timetable
#
#	Date	:	2018-02-14 (Pun) [135488] [ip.2.5.9.3.1]
# 	Details	:	 Added access right checking
#
#	Date	:	2015-10-07 (Paul) [ip.2.5.6.10.1.0]
# 	Details	:	 Add max file upload size setting
#
#	Date	:	2015-02-10 (Siuwan) [ip.2.5.6.3.1.0]
# 	Details	:	 Replace $i_eClass_teacher_open_course with $Lang['eclass']['setting']['TeacherEditEclass'] (Case#E75253)
#
#	Date	:	2014-09-03 (Charles Ma)
# 	Details	:	 eClass setting - PowerLesson and Enable children's eClass  for all class
# 					- [20140903-eclass-setting-pl-setting-parent2cchild-eclass]
#
# 	Date	:	2012-08-22 [Siuwan]
#	Details	:	add a checkbox to enable Children's eClass
#
# 	Date	:	2010-04-16 [Yuen]
#	Details	:	add default max course storage setting
#				will be adopted when create new courses
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-eClass"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$CurrentPage = "PageSettings";

$li = new libuser($UserID);
$lo = new libeclass();
$leclass = new libeclass2007();
$lgeneralsettings = new libgeneralsettings();
$lf = new libfilesystem();

# Left menu
$title = $i_eClass_Admin_Settings;
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$general_setting = $lgeneralsettings->Get_General_Setting("eClassSettings");//20140903-eclass-setting-pl-setting-parent2cchild-eclass

# block illegal access
$la = new libaccess();
$la->retrieveAccessEClass(true);
$la->retrieveEClassSettings();
$ec_mgt_checked = ($la->isAccessEClassMgt()) ? "checked" : "";
$ec_mgt_course_checked = ($la->isAccessEClassMgtCourse()) ? "checked" : "";
$nt_mgt_course_checked = ($la->isAccessEClassNTMgtCourse()) ? "checked" : "";
if (!$la->isAccessEClassMgt()){
	$ec_mgt_course_checked .= $ec_mgt_course_checked.' disabled';
	$nt_mgt_course_checked .= $nt_mgt_course_checked.' disabled';
}
#Children's eClass access
$ec_p_mgt_checked = ($la->isAccessChildrenEClassMgt()) ? "checked" : "";
$ec_pl_checked = ($general_setting["isEmbeddedPowerLesson"]) ? "checked" : ""; //20140903-eclass-setting-pl-setting-parent2cchild-eclass
# Access Email , true = Enable (checked) , false = Disable
$ec_email_teacher_0_checked = ($la->isAccessEClassEmailTeacher()) ? "checked" : "";
$ec_email_helper_0_checked = ($la->isAccessEClassEmailHelper()) ? "checked" : "";
$ec_email_student_0_checked = ($la->isAccessEClassEmailStudent()) ? "checked" : "";
# Max Quota for course
$default_max_quota = $la->eclass_settings_loaded["default_max_quota"];
if ($default_max_quota=="" || $default_max_quota<0)
{
	$default_max_quota = 30;
}
# Max Filie size for submission for all courses

$default_max_size = $la->eclass_settings_loaded["default_max_size"];
if ($default_max_size=="" || $default_max_size<0)
{
	$default_max_size = "";
}


$base_dir = "$intranet_root/file/templates/";
if (!is_dir($base_dir))
{
    $lf->folder_new($base_dir);
}
$target_file = "$base_dir"."bulletin_badwords.txt";
$data = get_file_content($target_file);


#### eLearning Timetable START ####
$sql = "select SettingValue from GENERAL_SETTING where module = 'eLearningTimetable' and SettingName='enableTimetable' ";
$val = $la->returnVector($sql);
$chkElearningTimetableEnabled = ($val[0] === '0')?'':' checked';
#### eLearning Timetable END ####

// For powervoice control
if ($plugin['power_voice'])
{
	$pvoice_target_file = "$intranet_root/file/powervoice.txt";
	$pvoice_data = get_file_content($pvoice_target_file);
	$pvoice_array = unserialize($pvoice_data);

	//$powervoice_content = "<p>";
	//$powervoice_content .= "<b><span class=\"extraInfo\">[<span class=subTitle>".$iPowerVoice['power_voice_setting']."</span>]</span></b><hr size=1 class='hr_sub_separator''>";

	$powervoice_content .= "<table width='350' border='1' bordercolor='#F7F7F9' cellspacing='1' cellpadding='1'>";
	$bitrate_select = "";
	$bitrate_select .= "<select name='bitrate' >";
	if ($pvoice_array['bitrate'] == "128")
	{
		$bitrate_select .= "<option value=\"128\" selected=\"selected\" >128</option>";
	} else {
		$bitrate_select .= "<option value=\"128\" >128</option>";
	}
	if (($pvoice_array['bitrate'] == "96") || ($pvoice_array['bitrate'] == ""))
	{
		$bitrate_select .= "<option value=\"96\" selected=\"selected\" >96</option>";
	} else {
		$bitrate_select .= "<option value=\"96\" >96</option>";
	}
	if ($pvoice_array['bitrate'] == "64")
	{
		$bitrate_select .= "<option value=\"64\" selected=\"selected\" >64</option>";
	} else {
		$bitrate_select .= "<option value=\"64\" >64</option>";
	}
	if ($pvoice_array['bitrate'] == "32")
	{
		$bitrate_select .= "<option value=\"32\" selected=\"selected\" >32</option>";
	} else {
		$bitrate_select .= "<option value=\"32\" >32</option>";
	}
	$bitrate_select .= "</select>";
	$powervoice_content .= "<tr><td style=\"vertical-align:bottom\" align='left'>".$iPowerVoice['bit_rate']."</td><td style=\"vertical-align:bottom\" align='left'>{$bitrate_select} {$iPowerVoice['bit_rate_unit']}</td></tr>";
	$sample_select = "";
	$sample_select .= "<select name='samplerate' >";
	if ($pvoice_array['sampling_frequency'] == "22050")
	{
		$sample_select .= "<option value=\"44100\" >44100</option>";
		$sample_select .= "<option value=\"22050\" selected=\"selected\" >22050</option>";
	} else {
		$sample_select .= "<option value=\"44100\" selected=\"selected\" >44100</option>";
		$sample_select .= "<option value=\"22050\" >22050</option>";
	}
	$sample_select .= "</select>";
	$powervoice_content .= "<tr><td style=\"vertical-align:bottom\" align='left'>".$iPowerVoice['sampling_rate']."</td><td style=\"vertical-align:bottom\" align='left'>{$sample_select} {$iPowerVoice['sampling_rate_unit']} </td></tr>";
	$length_select = "";
	$length_select .= "<select name='voice_length' >";
	if (($pvoice_array['length'] == "60") || ($pvoice_array['length'] == ""))
	{
		$length_select .= "<option value=\"60\" selected=\"selected\" >60 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"60\" >60 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "30")
	{
		$length_select .= "<option value=\"30\" selected=\"selected\" >30 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"30\" >30 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "25")
	{
		$length_select .= "<option value=\"25\" selected=\"selected\" >25 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"25\" >25 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "20")
	{
		$length_select .= "<option value=\"20\" selected=\"selected\" >20 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"20\" >20 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "15")
	{
		$length_select .= "<option value=\"15\" selected=\"selected\" >15 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"15\" >15 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "10")
	{
		$length_select .= "<option value=\"10\" selected=\"selected\" >10 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"10\" >10 {$iPowerVoice['sound_length_unit']}</option>";
	}
	if ($pvoice_array['length'] == "5")
	{
		$length_select .= "<option value=\"5\" selected=\"selected\" >5 {$iPowerVoice['sound_length_unit']}</option>";
	} else {
		$length_select .= "<option value=\"5\" >5 {$iPowerVoice['sound_length_unit']}</option>";
	}
	$length_select .= "</select>";
	$powervoice_content .= "<tr><td style=\"vertical-align:bottom\" align='left' >".$iPowerVoice['sound_length']."</td><td style=\"vertical-align:bottom\" align='left'>{$length_select} </td></tr>";
	$powervoice_content .= "</table>";
	$powervoice_content .= "</p>";
}

?>

<script language="javascript">
var input_value = "";
function triggerCourse(obj, isCheck){
	if (!isCheck)
	{
		obj.ec_mgt_course.checked = false;
		obj.ec_mgt_course.disabled = true;
		obj.nt_mgt_course.checked = false;
		obj.nt_mgt_course.disabled = true;
	} else
	{
		obj.ec_mgt_course.disabled = false;
		obj.nt_mgt_course.disabled = false;
	}

	return;
}

function check_number(f){
	var patt = new RegExp(/^[\d]+$/);
	if((f.value!="")&&(!patt.test(f.value))){
		f.value=input_value;
		return false;
	}else{
		input_value = f.value;
		return true;
	}
}
</script>

<form name="form1" action="settings_update.php" method="post">
<table width="98%" border=0 cellpadding=0 cellspacing=5 align="center">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr><td colspan='2'>

<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$i_frontpage_menu_eclass_mgt?> </span>-</em>
		<p class="spacer"></p>
</div>
<table class="form_table_v30">
	<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['Rights']?> </td>
			<td>
			<input type=checkbox onClick="triggerCourse(this.form, this.checked)" name="ec_mgt" id="ec_mgt1" value=1 <?=$ec_mgt_checked?> /> <label for="ec_mgt1"><?=$i_eClass_management_enable?></label>
			<div>
&nbsp; &nbsp; &nbsp; <input type=checkbox name="ec_mgt_course" value=1 id="ec_mgt_course1" <?=$ec_mgt_course_checked?>> <label for="ec_mgt_course1"><?=$Lang['eclass']['setting']['TeacherEditEclass']?></label><br />
&nbsp; &nbsp; &nbsp; <input type=checkbox name="nt_mgt_course" value=1 id="nt_mgt_course1" <?=$nt_mgt_course_checked?>> <label for="nt_mgt_course1"><?=$Lang['eclass']['setting']['NAEditEclass'] ?></label><br />
			</td>
		</tr>
	<tr valign='top'>
			<td class="field_title" nowrap><?=$i_frontpage_eclass_eclass_enable_children_setting?></td>
			<td nowrap>
			<input type=checkbox name="ec_p_mgt" value=1 <?=$ec_p_mgt_checked?> /> </td>
		</tr>
	<?if ($plugin['power_lesson'] ){?>
	<tr valign='top'>
			<td class="field_title" nowrap><?=$i_frontpage_eclass_eclass_embeded_powerLesson?></td>
			<td nowrap>
			<input type=checkbox name="ec_pl" value=1 <?=$ec_pl_checked?> /> </td>
		</tr>
	<?}?>
	<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['Btn']['Enable']?> <?= $Lang['eLearningTimetable']['eLearningTimetable'] ?> </td>
			<td>
				<input type="hidden" value="0" name="eLearningTimetableEnabled"/>
				<input
					type="checkbox"
					value="1"
					id="eLearningTimetableEnabled"
					name="eLearningTimetableEnabled"
					<?= $chkElearningTimetableEnabled ?>
				/>
			</td>
		</tr>
	<tr valign='top'>
			<td class="field_title" nowrap><?= $Lang['eclass']['default_max_quota'] ?> </td>
			<td>
			<input type="input" name="default_max_quota" maxlength="5" value="<?=$default_max_quota?>" size="6" /> MB</td>
		</tr>
	<tr valign='top'>
			<td class="field_title" nowrap><?= $Lang['eclass']['default_max_size'] ?> </td>
			<td>
			<input type="input" name="default_max_size" maxlength="5" onpropertychange="check_number(this)" oninput="check_number(this)" onpaste="check_number(this)" value="<?=$default_max_size?>" size="6" /> MB</td>
		</tr>
</table>


<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$i_frontpage_schoolinfo_groupinfo_group_bulletin?> </span>-</em>
		<p class="spacer"></p>
</div>
<table class="form_table_v30">
	<tr valign='top'>
			<td class="field_title" ><?=$i_eClass_Bulletin_BadWords_Instruction_top?> </td>
			<td>
			<span class="tabletextremark"><?=$i_CampusMail_New_BadWords_Instruction_bottom?></span><br />
				<textarea name=data COLS=60 ROWS=20><?=$data?></textarea>
			</td>
		</tr>
</table>


<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$iPowerVoice['power_voice_setting']?> </span>-</em>
		<p class="spacer"></p>
</div>
<table class="form_table_v30">
	<tr valign='top'>
			<td class="field_title" nowrap><?=$iPowerVoice['bit_rate']?> </td>
			<td>
			<?=$bitrate_select?> <?=$iPowerVoice['bit_rate_unit']?>
		</tr>
	<tr valign='top'>
			<td class="field_title" nowrap><?=$iPowerVoice['sampling_rate']?></td>
			<td nowrap>
			<?=$sample_select?> <?=$iPowerVoice['sampling_rate_unit']?>
			</td>
		</tr>
	<tr valign='top'>
			<td class="field_title" nowrap><?= $iPowerVoice['sound_length'] ?> </td>
			<td>
			<?=$length_select?>
			</td>
		</tr>
</table>


</td>
</tr>

<tr>
	<td colspan='2' height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
</tr>

<tr>
	<td colspan='2' align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<span class="dotline">
						<?= $linterface->GET_ACTION_BTN($button_update, "submit")?>
					</span>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
