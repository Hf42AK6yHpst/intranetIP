<?php
//modifying By 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageMyChildreneClass";

$li = new libuser($UserID);
$lo = new libeclass();
$leclass = new libeclass2007();
$lp = new libportal();

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> $i_frontpage_eclass_eclass_my";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$ChildList = $li->getChildrenList($UserID);

$childID = (!empty($childID) && $childID != "")?$childID:$ChildList[0]['StudentID'];
$ChildSelection = $linterface->GET_SELECTION_BOX($ChildList, "name='childID' onchange='this.form.childID.value=this.options[this.selectedIndex].value;this.form.submit();'", "", $childID);

$linterface->LAYOUT_START();
?>
<script>
function fe_eclass2(id){
        newWindow('/home/eLearning/login.php?uc_id='+id+'&isParent=1&childID=<?=$childID?>',8);
}
</script>
 
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center">
      <table width="96%" border="0" cellspacing="0" cellpadding="8">
        <tr>
          <td width="50%"><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='0' /></td>
          <td width="50%"><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='0' /></td>
        </tr>
        <tr>
          <td colspan="2">
			
            <table border='0' cellspacing='0' cellpadding='0' width="100%">
              <tr>
				<td width='55%'><form name="form1" id="form1" action="children.php" method="post"><?=$ChildSelection?></form></td>
                <td align='center' valign='middle'><span class='tabletext'><?=$i_general_icon_list?></span><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/eclass/icon_arrow.gif' width='11' height='11' align='absmiddle'> </td>
                <td align='right'>
                  <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tr>
                      <td width='4'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/eclass/iconlist_left.gif' width='4' height='18'></td>
                      <td bgcolor='#fafafa'>
                        <table border='0' cellpadding='0' cellspacing='0' class='iconlist'>
                          <tr>
                            <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_econtent.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_eClass_MyeClass_Content?>&nbsp;</td>
                            <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_assessment.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_assessment?>&nbsp;</td>
                            <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_forum.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_eClass_MyeClass_Bulletin?>&nbsp;</td>
                            <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_annou.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_eClass_MyeClass_Announcement?>&nbsp;</td>
                            <td align='center'>&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                      <td width='4'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/eclass/iconlist_right.gif' width='4' height='18'></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        
        <?=$lp->displayChildrenEClass($UserID,$childID)?>
      	
      </table>
    </td>
  </tr>
</table>        
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
