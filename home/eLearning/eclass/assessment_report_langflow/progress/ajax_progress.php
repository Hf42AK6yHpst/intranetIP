<?php
// using: 

####################################
         // Change Log //
/*
 * 	2016-01-13 (Jason) IP.2.5.7.1.1
 * 		- Fix the problem of not handle weekly report properly cross year, like 2015 Dec to 2016 Jan
 * 		- the counting of number of week becomes negative 
 * 	2015-10-23 (Paul) IP.2.5.7.1.1
 * 		- Create the file for LangFlow centre to for student progress checking
 * 
 */         
####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
eclass_opendb();

$ldb = new libdb();
$li = new libuser($UserID);

switch ($task)
{
	case "UpdateStudentList": //The AJAX response to give teacher the student list of the selected classroom
		$class_id = $_POST['ClassroomID'];
		$classroom_db = classNamingDB($class_id);
		
		$sql = "SELECT user_id, firstname, chinesename ";
		$sql .= "FROM ".$classroom_db.".usermaster WHERE TitleEnglish =''";
		$row = $ldb->returnArray($sql);
		$output = "<select id='student_list' onchange=jsUpdateTable()>";
		for ($i=0; $i < sizeof($row); $i++){
			$name = ($intranet_session_language == 'b5')?$row[$i]['chinesename']:$row[$i]['firstname'];
			$name = ($name=="")?$row[$i]['firstname']:$name;
			if ($i == 0){
				$output .= "<option value='".$row[$i]['user_id']."' selected='selected'>".$name."</option>";
			}else{
				$output .= "<option value='".$row[$i]['user_id']."'>".$name."</option>";
			}
		}
		$output .= "</select>";
		echo $output;
	break;
	
	case "UpdateTable": //The AJAX response to update the data presented
		// Collection of requried vaiables
		$mode = $_POST['Mode']; //Type of the table: By Week or By Month
		$class_id = $_POST['ClassroomID']; 
		$UserID = $_SESSION['UserID'];
		$role = $_SESSION['UserType'];
		if ($_SESSION['UserType'] == '2'){
			$role = "STUDENT";
		}else if ($_SESSION['UserType'] == '1'){
			$uid = $_POST['StudentID'];
			if ($_SESSION['isTeaching'])
				$role = "TEACHER";
		}
		if ($_SESSION['schoolsettings']['isAdmin']){
			$role = "ADMIN";
		}
		if ($mode == "Year"){   // In Mode "By Month", data of the whole year to be collected (12 months)
			$currentyear = date("Y");
			$startdate = date("Y-m-d", mktime(0, 0, 0, 1, 1, $currentyear));
			$enddate = date("Y-m-d", mktime(0, 0, 0, 12, 31, $currentyear));
		}else{ // In Mode "By Week", data of the last 12 weeks (including the current week) to be collected
			if (date("w", time()) == 1){ //Week starts from Monday 
				$starting_time = strtotime('last monday', strtotime('tomorrow')); //as on monday, last monday would be a week before, this is to return the time of the monday by based on Tuesday
			}else{
				$starting_time = strtotime('last monday');  
			}	
			$startdate = date("Y-m-d", $starting_time-7*24*60*60*11); 
			$enddate = date("Y-m-d", $starting_time+6*24*60*60);
		}
		
		$classroom_db = classNamingDB($class_id); //Go to the DB of the specific classroom

		// To setup the print value of the time considered (X-axis of the graph)
		if ($mode == "Year"){
			$timeRange = array();
			for($i=1; $i <= 12; $i++){
				array_push($timeRange, $Lang['General']['month'][$i]);
			}	
		}else{
			$timeRange = array();
			$start = array();
			$end = array();
			for ($i=0; $i < 12; $i++){
				array_push($start, strtotime($startdate)+($i*7*24*60*60));
				array_push($end, strtotime($startdate)+(($i+1)*7*24*60*60-24*60*60));
//			}
//			for($i=0; $i < 12; $i++){
				if ($intranet_session_language == 'en'){
					$date_format = date("j", $start[$i])." ".date("M", $start[$i]);
					$date_format .= " ~ ";
					$date_format .= date("j", $end[$i])." ".date("M", $start[$i]);
				}else{
					$date_format = date("n", $start[$i])." ".$Lang['General']['Month']." ".date("j", $start[$i])." ".$Lang['General']['DayType4'][0];
					$date_format .= " ~ ";
					$date_format .= date("n", $end[$i])." ".$Lang['General']['Month']." ".date("j", $end[$i])." ".$Lang['General']['DayType4'][0];
				}
				
				array_push($timeRange, $date_format);
			}	
		}

		// To collect the start date, end date and student score of all assessments ended within the specified time period
		$sql = "SELECT afm.mark as mark, a.startdate as Start, a.enddate as End  ";
		$sql .= "FROM ".$classroom_db.".assessment_final_mark afm ";
		$sql .= "INNER JOIN ".$classroom_db.".assessment a ON afm.assessment_id = a.assessment_id ";
		if ($_SESSION['UserType'] == '2'){
			$sql .= "INNER JOIN ".$classroom_db.".usermaster u ON afm.user_id = u.user_id AND u.intranet_user_id=".$UserID." ";
		}else{
			$sql .= "INNER JOIN ".$classroom_db.".usermaster u ON afm.user_id = u.user_id AND u.user_id=".$uid." ";
		}
		$sql .= "WHERE ((a.startdate >= '".$startdate."' and a.enddate <= '".$enddate."')or(a.startdate >= '".$startdate."' and a.startdate <= '".$enddate."'))";
		$sql .= "ORDER BY a.startdate ASC ";

		$row = $ldb->returnArray($sql);
		
		//Collect the score data		
		if ($row != null){
			$marks = array();

			$monthcheck = 1;
			$week = 0;	
			for ($i=0; $i < sizeof($row); $i++){
				if ($mode == "Year"){
					$month = date("n", strtotime($row[$i]['Start']));
					if ($monthcheck != $month){
						$monthcheck = $month;		//As return value is sorted by start date, update the current checking month value if not match		
					}
					$marks[$monthcheck-1] += $row[$i]['mark'];
				}else{
					$assessment_start = date("W", strtotime($row[$i]['Start']));
//					$assessment_end = date("W", strtotime($row[$i]['End']));
					$diff = $assessment_start - date("W", strtotime($startdate));
					if ($week != $diff){
						if($diff < 0){
							$week =  $diff + date("W", strtotime('31 December '.date('Y', strtotime($startdate))));	
						} else {
							$week =  $diff; //As return value is sorted by start date, update the current checking week value if not match
						}		
					}					
					$marks[$week] += $row[$i]['mark'];
				}	
			}

		}
	
		//Table of the values to be returned
		$returnValue = "<table id='progress_table' class='common_table_list_v30'>";
		if ($mode == "Year"){
			$returnValue .= "<tr><th width='50%' nowrap='nowrap'>";
		}else{
			$returnValue .= "<tr><th width='10%' nowrap='nowrap'>";
		}
		if ($mode == "Week"){
			$returnValue .= $Lang['eClass']['LfAssessmentReport']['WeekHeader']."</th><th width='10%'>".$Lang['eClass']['LfAssessmentReport']['header3']."</th></tr>";
		}else{
			$returnValue .= $Lang['General']['Month']."</th><th width='50%'>".$Lang['eClass']['LfAssessmentReport']['header3']."</th></tr>";
		}
		for ($i=0; $i < 12; $i++){
			$shown_mark = ($marks[$i] != null)? my_round($marks[$i],1) : "--";
			$returnValue .= "<tr><td>".$timeRange[$i]."</td><td>".$shown_mark."</td></tr>";
		}			
		$returnValue .= "</table>";
		
		$JSON = new JSON_obj();
		$returnArray = array($timeRange, $marks, $returnValue); //return with the managed data and the HTML table for graph generation
		$returnArray = $JSON->encode($returnArray); //to pass an array to javascript, json_encode is required (PHP > 5.2). This is a substitute function if json_encode does not exist
		echo $returnArray;
	break;
}

eclass_closedb();
die;
?>