<?php
// using: Paul

####################################
         // Change Log //
/*
 * 	
 * 	2015-10-23 (Paul) IP.2.5.7.1.1
 * 		- Create the file for LangFlow centre to for ranking student assessment result
 * 
 */         
####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
eclass_opendb();

$ldb = new libdb();
$li = new libuser($UserID);

switch ($task)
{
	case "ChangeDisplayWeek": //Update the display of week if either arrow being pressed
	global $Lang;
		//get the new week data		
		$tsNumOfDay = $ChangeValue*24*60*60;
		$newWeekStartTimeStamp = $WeekStartTimeStamp + $tsNumOfDay;
				
		$curr_date = date("Y-m-d", $newWeekStartTimeStamp);
		$curr_year = substr($curr_date,0,4);
		$curr_month = substr($curr_date,5,2);
		$curr_day = substr($curr_date,8,2);
				
		$StartOfTheWeek = date("Y-m-d",mktime(0,0,0,$curr_month , $curr_day-$curr_weekday, $curr_year));
		$EndOfTheWeek = date("Y-m-d",mktime(0,0,0,$curr_month , $curr_day-$curr_weekday+6, $curr_year));
				
		$StartYearOfTheWeek = substr($StartOfTheWeek,0,4);
		$StartMonthOfTheWeek = substr($StartOfTheWeek,5,2);
		$StartDayOfTheWeek = substr($StartOfTheWeek,8,2);
				
		$EndYearOfTheWeek = substr($EndOfTheWeek,0,4);
		$EndMonthOfTheWeek = substr($EndOfTheWeek,5,2);
		$EndDayOfTheWeek = substr($EndOfTheWeek,8,2);
				
		$DisplayStartOfTheWeek = $StartDayOfTheWeek." ".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$StartMonthOfTheWeek].",".$StartYearOfTheWeek;
		$DisplayEndOfTheWeek = $EndDayOfTheWeek." ".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$EndMonthOfTheWeek].",".$EndYearOfTheWeek;
				
		$returnString = $newWeekStartTimeStamp.":::".$DisplayStartOfTheWeek." - ".$DisplayEndOfTheWeek;
		echo $returnString;
	break;
	
	
	case "UpdateRank":
		$starting_time = $_POST['WeekStartTimeStamp'];
		$mode = $_POST['Mode'];
		$class_id = $_POST['ClassroomID'];
		$UserID = $_SESSION['UserID'];
		$role = $_SESSION['UserType'];
		if ($_SESSION['UserType'] == '2'){
			$role = "STUDENT";
		}else if ($_SESSION['UserType'] == '1'){
			if ($_SESSION['isTeaching'])
				$role = "TEACHER";
		}
		if ($_SESSION['schoolsettings']['isAdmin']){
			$role = "ADMIN";
		}
		
		$startdate = date("Y-m-d", $starting_time);
		$enddate = date("Y-m-d", $starting_time+7*24*60*60);
		
		$classroom_db = classNamingDB($class_id);

		//Hold the usermaster table to have all the students inside the classroom, and mapping with the assessment information of each student
#		$sql = "SELECT u.EnglishName as eng_name, u.ChineseName as chi_name, SUM(afm.mark) as mark, COUNT(afm.assessment_id) as done, uc.user_email as mail ";
		$sql = "SELECT u.intranet_user_id as id, u.firstname as eng_name, u.chinesename as chi_name, SUM(afm.mark) as mark, COUNT(afm.assessment_id) as done  ";
		$sql .= "FROM ".$classroom_db.".usermaster u ";
//		$sql .= "FROM ".$classroom_db.".assessment_final_mark afm ";		
//		$sql .= "INNER JOIN ".$classroom_db.".assessment a ON afm.assessment_id = a.assessment_id ";
//		$sql .= "AND ((a.startdate >= '".$startdate."' and a.enddate <= '".$enddate."')or(a.enddate >= '".$startdate."' and a.enddate <= '".$enddate."')) ";
//		$sql .= "INNER JOIN ".$classroom_db.".usermaster u ON afm.user_id = u.user_id ";
		$sql .= "LEFT JOIN ";
		$sql .= "(SELECT fm.assessment_id, fm.user_id, fm.mark ";
		$sql .= "FROM ".$classroom_db.".assessment_final_mark fm, ".$classroom_db.".assessment a ";
		$sql .= "WHERE fm.assessment_id = a.assessment_id ";

#		$sql .= "INNER JOIN ".$eclass_db.".user_course uc ON (uc.course_id = '".$class_id."' AND uc.user_id = afm.user_id) ";
#		$sql .= "INNER JOIN ".$intranet_db.".INTRANET_USER u ON (uc.user_email = u.UserEmail) ";
		if ($starting_time != 0){
			$sql .= "AND ((a.startdate >= '".$startdate."' and a.enddate <= '".$enddate."')or(a.startdate >= '".$startdate."' and a.startdate <= '".$enddate."')) ";
//			$sql .= "WHERE ((a.startdate >= '".$startdate."' and a.enddate <= '".$enddate."')or(a.startdate >= '".$startdate."' and a.startdate <= '".$enddate."')) ";
		}
		$sql .= ") afm  ON u.user_id = afm.user_id ";
		$sql .= "WHERE u.TitleEnglish ='' AND u.memberType='S' ";
		$sql .= "GROUP BY u.user_id ";
#		$sql .= "GROUP BY afm.user_id ";
		$sql .= "ORDER BY mark DESC, done ASC, id ASC ";
		if ($role != "STUDENT"){
			$sql .= "LIMIT 100";
		}
		$row = $ldb->returnArray($sql);
		$top100 = ($role != "STUDENT")? true : false; 
		if ($row != null){ //create the return Table
			$recorded = array();
			$returnValue = "<table id='ranking_table' class='common_table_list_v30'>";
			$returnValue .= "<tr><th width='5%' nowrap='nowrap'>";
			$returnValue .= $Lang['eClass']['LfAssessmentReport']['header1']."</th><th width='35%'>".$Lang['eClass']['LfAssessmentReport']['header2']."</th><th width='30%'>".$Lang['eClass']['LfAssessmentReport']['header3']."</th><th width='30%'>".$Lang['eClass']['LfAssessmentReport']['header4']."</th></tr>";
			for($i=0; $i < sizeof($row); $i++){
				$rank = $i+1;
				$name = ($intranet_session_language == 'b5')?$row[$i]['chi_name']:$row[$i]['eng_name'];
				$name = ($name=="")?$row[$i]['eng_name']:$name;
				if ($row[$i]['done'] == '0'){
					$mark = "--";
				}else{
					$mark = my_round($row[$i]['mark'],1);
				}
				if ($rank <= 100){
					if ($UserID == $row[$i]['id']){
						$returnValue .= "<tr class='row_avaliable'><td>".$rank."</td><td>".$name."</td><td>".$mark."</td><td>".$row[$i]['done']."</td></tr>";
						$top100 = true;
					}else{
						$returnValue .= "<tr><td>".$rank."</td><td>".$name."</td><td>".$mark."</td><td>".$row[$i]['done']."</td></tr>";
					}
				}else			
				//if the student is not within top 100
				if (($rank>100) && (!$top100)){
					if ($UserID == $row[$i]['id']){
						$returnValue .= "<tr><td colspan='4'>.....</td></tr>";
						$returnValue .= "<tr class='row_avaliable'><td>".$rank."</td><td>".$name."</td><td>".$mark."</td><td>".$row[$i]['done']."</td></tr>";
						$top100 = true;
						break;
					}	
				}
			};
			
			
			
			// Give dummy record if less than 100 students are in the classroom
			if ($rank < 100){
				for ($i=$rank+1; $i <= 100; $i++){
					$returnValue .= "<tr><td>".$i."</td><td>--</td><td>--</td><td>--</td></tr>";
				}
			}
			$returnValue .= "</table>";
		}else{
			$returnValue = "<p>".$Lang['eClass']['LfAssessmentReport']['EmptyRecord']."</p>";
		}
		echo $returnValue;
	break;
}

eclass_closedb();
die;
?>