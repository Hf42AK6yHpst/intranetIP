<?php
#Rank
# editing by Paul

####################################
         // Change Log //
/*
 * 	
 * 	2015-10-23 (Paul) IP.2.5.7.1.1
 * 		- Create the file for LangFlow centre to for ranking to be viewed by student
 * 
 */         
####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
eclass_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageAssessmentReportRank";

# eClass
$li = new libuser($UserID);
$lo = new libeclass();
$lp = new libportal();
$leclass = new libeclass2007();

$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_statistics.gif' align='absmiddle' /> ".$Lang['eClass']['LfAssessmentReport']['Ranking'];
//$TAGS_OBJ[] = array($title,"");
$TAGS_OBJ[] = array($Lang['eClass']['LfAssessmentReport']['Weekly'],"weekly.php",0);
$TAGS_OBJ[] = array($Lang['eClass']['LfAssessmentReport']['Accumulated'],"accumulated.php",1);


$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// Main content

// Generation of the filter "classroom"
if($_SESSION['schoolsettings']['isAdmin']){ //is Admin
	$courses_select = $lo->getSelectEclass(' class="course_list" ');
	$display = $courses_select;
	$role = "Admin";
}else if ($li->isStudent() || ($li->isTeacherStaff()&&($li->teaching))){ // is student or teacher
	$row = $lp->returnEClassUserIDCourseIDStandard($li->UserEmail);
	$display = "<div class='classroom'><select id='classroom_list' onchange=jsUpdateRank()>";
	if(sizeof($row)==0){
		$display .= "<option class='no_class_msg value=-9999 selected='selected'>".$Lang['eClass']['LfAssessmentReport']['NoClass']."</option>"; 	
	}else{
		for($i=0; $i < sizeof($row); $i++){
			$course_id = $row[$i][0];
			$course_code = $row[$i][1];
			$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
			if ($i == 0){
				$display .= "<option value='".$course_id."' selected='selected'>".$course_code." - ".$course_name."</option>";
			}else{
				$display .= "<option value='".$course_id."'>".$course_code." - ".$course_name."</option>";
			}
		}
	}
	$display .= "</select></div>";
	if ($li->isStudent()){
		$role = "Student";
	}else{
		$role = "Teacher";
	}
}
$display .= "<input type='hidden' name='role' id='role' value='".$role."'>";

// Get User Name
if ($role == "Student"){
	$std_email = $li->UserEmail;
	$display .= "<input type='hidden' name='std_email' id='std_email' value='".$std_email."'>";
}
$tab_content .= "<br>";
$display .= $tab_content;
?>

<script>
$(document).ready(function(){jsUpdateRank()});

function jsUpdateRank(){
	Block_Document();
	var e = document.getElementById("classroom_list");
		try{
			var classID = e.options[e.selectedIndex].value;
			$.post(
				"ajax_rank.php",
				{
					task : "UpdateRank",
					WeekStartTimeStamp : 0,
					Mode : "All",
					ClassroomID : classID
				},
				function(responseData)
				{
					var ReturnVal = "";
					ReturnVal += responseData;
					$("#ranking").html(ReturnVal);
					UnBlock_Document();
				}
			);
		}catch(err){
			var ReturnVal = "<p><?=$Lang['eClass']['LfAssessmentReport']['NoClass']?></p>";
			$("#ranking").html(ReturnVal);
		}
		
}
</script>
<?=$display?>
<div id="ranking" class="ranking_table_list">
</div>
<style>
.ranking_table_list{
	width: 100%;
}
</style>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>