<?php
# editing by
/**
 * Change Log:
 * 2018-02-14 (Pun) [135488] [ip.2.5.9.3.1]
 *      - Added access right checking
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
intranet_auth();
eclass_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-eClass"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# eClass
$lo = new libeclass();
$eclass_quota = $lo->status();
$libdb = new libdb();

$show= isset($show) ? $show : 'all';
$id = intval($id);

function genFreezeTable($head, $content, $freezeColumn){
    
    
    $script="<script>
            $(document).ready(function(){
                $('.freeze-table-right').scroll(function(){
                    $('.freeze-table-left').scrollTop($(this).scrollTop());
                    $('.freeze-table-top-right').scrollLeft($(this).scrollLeft());
                });
            });
            
            </script>";
            
    $html='<div class="freeze-table"><div class="freeze-table-top"><div class="freeze-table-top-left"><table>';
    
    foreach ($head as $row){
        $html.='<tr>';
        for ($j=0, $row=array_values($row) ;$j<$freezeColumn; $j++){
            $html.='<th>'.$row[$j].'</th>';
        }
        $html.='</tr>';
    }
    
    $html.='</table></div><div class="freeze-table-top-right"><table>';
    
    foreach ($head as $row){
        $html.='<tr>';
        for ($j=$freezeColumn, $head_column=sizeof($row), $row=array_values($row);$j<$head_column; $j++){
            $html.='<th><div>'.$row[$j].'</div></th>';
        }
        $html.='</tr>';
    }
        
    $html.='</table></div></div><div class="freeze-table-content"><div class="freeze-table-left"><table>';
    
    foreach ($content as $row){
        $html.='<tr>';
         for ($j=0, $row=array_values($row) ;$j<$freezeColumn; $j++){
            $html.='<td><div>'.$row[$j].'</div></td>';
        }
        $html.='</tr>';
    }
    
    $html.='</table></div><div class="freeze-table-right"><table>';
    
    foreach ($content as $row){
        $html.='<tr>';
        for ($j=$freezeColumn, $content_column=sizeof($row), $row=array_values($row);$j<$content_column; $j++){
            $html.='<td>'.$row[$j].'</td>';
        }
        $html.='</tr>';
    }
    
    $html.='</table></div></div></div>';
    
    return $script.$html;
}


if (isset($type) && $id!=0){
    
    //referenced from lib-grouping returnGroupUsers();
    $username_field = getNameFieldByLang("a.");
    
    $sql  = "SELECT a.UserID, $username_field as UserName, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '&nbsp;', CONCAT(a.ClassName,' - ',a.ClassNumber) ) as UserClass, c.course_id, c.user_id
    FROM INTRANET_USER AS a
    INNER JOIN INTRANET_USERGROUP AS b ON a.UserID = b.UserID and b.GroupID=$id 
    LEFT JOIN $eclass_db.user_course AS c ON c.user_email=a.UserEmail AND (c.status is NULL OR c.status NOT IN ('deleted')) AND c.memberType='S'
    ORDER BY a.UserID ";
    $courses_students = $libdb->returnArray($sql);
      
    $sql  = "SELECT distinct(d.course_id), d.course_name
    FROM INTRANET_USER AS a
    INNER JOIN INTRANET_USERGROUP AS b ON a.UserID = b.UserID and b.GroupID=$id 
    LEFT JOIN $eclass_db.user_course AS c ON c.user_email=a.UserEmail AND (c.status is NULL OR c.status NOT IN ('deleted')) AND c.memberType='S'
    INNER JOIN $eclass_db.course AS d ON c.course_id=d.course_id AND d.RoomType='0'
    ORDER BY d.course_name";
    $courses = $libdb->returnArray($sql);
    $content=array();
    $head=array($i_ClassName.' ('.$i_ClassNumber.')',$i_UserName);
    $assessments_total=array();
    
    $sql = "create temporary table tempRecordTable(
                course_id int(8), 
                user_id int(8), assessment_id int(8),
                handinby int(2), phasetype int(2), enddate datetime, function_id int(8),
                gf_group_id int(8), gf_user_id int(8), ug_group_id int(8), ug_user_id int(8),
                index idx_user_id(user_id),
                index idx_assessment_id(assessment_id),
                index idx_gf_group_id(gf_group_id),
                index idx_gf_user_id(gf_user_id),
                index idx_ug_group_id(ug_group_id),
                index idx_ug_user_id(ug_user_id),
                index idx_function_id(function_id),
                index idx_handinby(handinby)
            )";
    $libdb->db_db_query($sql);
            
    
    foreach ($courses as $course){
        
        $head[]=$course['course_name'];
        $course_id=$course['course_id'];
        $eClassDB=$lo->classNamingDB($course_id);

        $sql = "insert into tempRecordTable(course_id, user_id,assessment_id,handinby,phasetype,enddate)
                
                select
                    $course_id,
                    u.user_id,
                    a.assessment_id,
                    a.handinby,
                    a.phasetype,
                    a.enddate 
                from $eClassDB.usermaster as u 
                CROSS JOIN $eClassDB.assessment a 
                on a.assessment_id in (
                    select a.assessment_id from $eClassDB.assessment a 
                    Left JOIN $eClassDB.grouping_function gf 
                    ON gf.function_id = a.assessment_id AND gf.function_type = 'A' 
                    where (gf.function_id is null or gf.function_id = '' or gf.function_id = 0) and a.fromlesson is null
                ) 
                and a.status <> 1
                group by user_id, a.assessment_id,a.phasetype
                ";
                
        $libdb->db_db_query($sql);
        
        $sql = "insert into tempRecordTable(course_id, user_id,assessment_id,handinby,phasetype,enddate,function_id,gf_group_id,gf_user_id,ug_group_id,ug_user_id)
                            
                select
                    $course_id,
                    case when gf.user_id is null then gp.user_id else gf.user_id end as user_id, 
                    a.assessment_id,
                    a.handinby,
                    a.phasetype,
                    a.enddate,
                    gf.function_id,
                    gf.group_id,
                    gf.user_id,
                    gp.group_id,
                    gp.user_id
                from $eClassDB.assessment a 
                inner join $eClassDB.grouping_function gf 
                ON gf.function_id = a.assessment_id AND gf.function_type = 'A' and a.status <> 1
                Left join $eClassDB.user_group as gp
                on gp.group_id = gf.group_id
                group by user_id, a.assessment_id,phasetype,gf.function_id,gf.group_id
                ";
                
        $libdb->db_db_query($sql);
                       
        foreach($courses_students as $student){
             $user_id=$student['UserID'];
            if (!isset($content[$user_id])){
                $content[$user_id]=array('UserClass'=>$student['UserClass'],'UserName'=>$student['UserName']);
            }

            if ($student['course_id']==$course_id){
                  
                
                $eClassUserID=$student['user_id'];
                
                $sql="select count(distinct assessment_id)
                    from tempRecordTable 
                    where user_id='$eClassUserID'
                    and course_id='$course_id'
                    ";
                $student_assessments_total=current($libdb->returnVector($sql));
                
                $sql = ($show=='all') ?
                        "SELECT count(distinct u.assessment_id)
                        FROM tempRecordTable as u 				
                        inner join $eClassDB.assessment_final_mark afm
                        ON afm.assessment_id = u.assessment_id
                        and u.user_id='$eClassUserID'
                        and u.course_id='$course_id'
                        AND (
                                (u.handinby = 1 AND afm.group_id = u.gf_group_id)
                                OR
                                (u.handinby = 0 AND afm.user_id = u.user_id) 
                        )
                        where 
                        afm.assessment_mark_id is not null 
                        AND IFNULL(afm.mark, '') <> '' AND afm.is_final = 1	
                        AND (
                                (u.function_id IS NULL AND u.handinby = 0) 
                                OR
                                (u.gf_user_id = u.user_id AND u.handinby = 0) 
                                OR
                                (u.ug_user_id = u.user_id AND u.handinby = 1) 
                        )"
                    :
                        "SELECT count(distinct u.assessment_id)
                        FROM tempRecordTable as u 
                        inner join $eClassDB.`phase` p on p.assessment_id = u.assessment_id 	
                        and u.user_id='$eClassUserID'
                        and u.course_id='$course_id'
                        inner JOIN $eClassDB.task t ON t.phase_id = p.phase_id
                        LEFT JOIN $eClassDB.assessment_final_mark afm ON afm.assessment_id = u.assessment_id
                        AND (
                                (u.handinby = 1 AND afm.group_id = u.gf_group_id)
                                OR
                                (u.handinby = 0 AND afm.user_id = u.user_id) 
                        )
                        WHERE
                        (
                                SELECT MIN(h.inputdate)
                                FROM $eClassDB.handin h
                                WHERE h.assignment_id = t.task_id
                                AND (
                                        (u.handinby = 1 AND h.group_id = u.gf_group_id) 
                                        OR
                                        (u.handinby = 0 AND h.user_id = u.user_id) 
                                )
                                AND (h.status='L' OR h.status='LR' OR h.status='D' OR h.status='DR')
                        ) > IF(u.phasetype = 1, p.enddate, u.enddate)
                        and afm.assessment_mark_id IS NOT NULL
                        AND IFNULL(afm.mark, '') <> '' AND afm.is_final = 1";
                        
		$assessments_done=intval(current($libdb->returnVector($sql)));

                $content[$user_id][$course_id]=$assessments_done.' / '.$student_assessments_total;
                    
            }else if (!isset($content[$user_id][$course_id])){
                $content[$user_id][$course_id]='--';
            }
            
        }
        
    }

    $head=array($head);
    if (empty($content)){//no eclasses result, prevent crashing layout
        $head[0][]='--';
        if (empty($courses_students)){//no students
            $content[]=array('--','--',$ec_iPortfolio['no_student']);
        }else{
            foreach($courses_students as $student){//has students, but no eclasses, then show students list
                $content[$student['UserID']]=array('UserClass'=>$student['UserClass'],'UserName'=>$student['UserName'],'--');
            }
        }

    }
    echo genFreezeTable($head,$content,2);
}
intranet_closedb();
?>