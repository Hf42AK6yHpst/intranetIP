<?php
# editing by Paul
/**
 * Change Log:
 * 2018-02-14 (Pun) [135488] [ip.2.5.9.3.1]
 *      - Added access right checking
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

intranet_auth();
eclass_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-eClass"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface 	= new interface_html();
$CurrentPage	= "PageAssessmentReport";

# eClass
$lo = new libeclass();
$leclass = new libeclass2007();

$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$Lang['Markbook']['Markbook'];
$TAGS_OBJ[] = array($title,"");
//$TAGS_OBJ[] = array('Markbooks',"/home/eLearning/eclass/assessment_report/markbook.php",1);
//$TAGS_OBJ[] = array($i_eClass_Admin_AssessmentReport,"/home/eLearning/eclass/assessment_report/index.php",0);

$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
	
$courses_select = $lo->getSelectEclass(' class="course_list" ');
?>
<style>
.ass_progress_table_list tr td{height:25px}
.no_markbook_msg, .addRowHeader{margin:5px}
/**************************
mark book 3 columns
**************************/
/* *** deal with the Markbook without Column feature, use below css statement marked ".marksheet_container" ***/
.marksheet_container {padding-left: 300px;padding-right: 150px;}
.marksheet_container .marksheet_inner {width: 100%;}
.marksheet_container .marksheet_float_wrap {float: left; width: 100%; margin-left: -300px; /*** Same length as .outer padding-left but with negative value ***/}
.marksheet_container .marksheet_main_content {float: right;margin-right: -300px; /*** Same length as .outer padding-left but with negative value ***/width: 100%; overflow-x:scroll}
* html .marksheet_main_content {position: relative; /*** IE needs this  ***/}
.marksheet_container .marksheet_left_content {float:left;width:300px; overflow-x:scroll}
.marksheet_container .marksheet_right_content {float:left;width:150px;position:relative;margin-right:-150px; overflow-x:scroll}

/* *** deal with the Markbook with Column feature, use below css statement marked ".marksheet_container1" ***/
.marksheet_container1 {padding-left: 150px;padding-right: 150px;}
.marksheet_container1 .marksheet_inner {width: 100%;}
.marksheet_container1 .marksheet_float_wrap {float: left; width: 100%; margin-left: -150px; /*** Same length as .outer padding-left but with negative value ***/}
.marksheet_container1 .marksheet_main_content {float: right;/*** Same length as .outer padding-left but with negative value ***/width: 60%; overflow-x:scroll}
* html .marksheet_main_content {position: relative; /*** IE needs this  ***/}
.marksheet_container1 .marksheet_left_content {float:left;width:40%; overflow-x:scroll}
.marksheet_container1 .marksheet_right_content {float:left;width:150px;position:relative;margin-right:-150px; overflow-x:scroll}


/**************************
grade book 3 columns
**************************/
/* *** deal with the Markbook without Column feature, use below css statement marked ".marksheet_container_garde" ***/
.marksheet_container_garde {padding-left: 300px;padding-right: 35px;}
.marksheet_container_garde .marksheet_inner {width: 100%;}
.marksheet_container_garde .marksheet_float_wrap {float: left; width: 100%; margin-left: -300px; /*** Same length as .outer padding-left but with negative value ***/}
.marksheet_container_garde .marksheet_main_content {float: right;margin-right: -300px; /*** Same length as .outer padding-left but with negative value ***/width: 100%; overflow-x:scroll}

.marksheet_container_garde .marksheet_left_content {float:left; width:300px; overflow-x:scroll}
.marksheet_container_garde .marksheet_right_content {float:right; width:35px; position:relative;margin-right:-35px; overflow-x:scroll}

/* *** deal with the Markbook with Column feature, use below css statement marked ".marksheet_container_garde1" ***/
.marksheet_container_garde1 {padding-left: 150px;padding-right: 150px;}
.marksheet_container_garde1 .marksheet_inner {width: 100%;}
.marksheet_container_garde1 .marksheet_float_wrap {float: left; width: 100%; margin-left: -150px; /*** Same length as .outer padding-left but with negative value ***/}
.marksheet_container_garde1 .marksheet_main_content {float: right;/*** Same length as .outer padding-left but with negative value ***/width: 60%; overflow-x:scroll}

.marksheet_container_garde1 .marksheet_left_content {float:left;width:40%; overflow-x:scroll}
.marksheet_container_garde1 .marksheet_right_content {float:right;width:150px;position:relative;margin-right:-150px; overflow-x:scroll}


/**************************
rubrics book 3 columns
**************************/
/* *** deal with the Markbook without Column feature, use below css statement marked ".marksheet_container_rubrics" ***/
.marksheet_container_rubrics {padding-left: 300px;padding-right: 100px;}
.marksheet_container_rubrics .marksheet_inner {width: 100%;}
.marksheet_container_rubrics .marksheet_float_wrap {float: left; width: 100%; margin-left: -300px; /*** Same length as .outer padding-left but with negative value ***/}
.marksheet_container_rubrics .marksheet_main_content {float: right;margin-right: -300px; /*** Same length as .outer padding-left but with negative value ***/width: 100%; overflow-x:scroll}

.marksheet_container_rubrics .marksheet_left_content {float:left;width:300px; overflow-x:scroll}
.marksheet_container_rubrics .marksheet_right_content {float:right;width:100px;position:relative;margin-right:-100px; overflow-x:scroll}

/* *** deal with the Markbook with Column feature, use below css statement marked ".marksheet_container_rubrics1" ***/
.marksheet_container_rubrics1 {padding-left: 150px;padding-right: 150px;}
.marksheet_container_rubrics1 .marksheet_inner {width: 100%;}
.marksheet_container_rubrics1 .marksheet_float_wrap {float: left; width: 100%; margin-left: -150px; /*** Same length as .outer padding-left but with negative value ***/}
.marksheet_container_rubrics1 .marksheet_main_content {float: right;/*** Same length as .outer padding-left but with negative value ***/width: 60%; overflow-x:scroll}

.marksheet_container_rubrics1 .marksheet_left_content {float:left;width:40%; overflow-x:scroll}
.marksheet_container_rubrics1 .marksheet_right_content {float:right;width:150px;position:relative;margin-right:-150px; overflow-x:scroll}


</style>
<script>
    
    $(function(){
	
	
	$('.course_list').change(function(){
	    var course_id = $(this).val();
	    if (!course_id) return;

	    $('.no_markbook_msg').fadeOut();
	    $.get('ajax_markbook.php?action=getMarkbookList',{course_id: course_id}, function(data, status, xhr){
		if (data!=''){
		    $('.markbook_list').find('option[value!=""]').remove();
		    $('.markbook_list').append(data).show();
		}else{
		    $('.no_markbook_msg').fadeIn();
		    $('.markbook_list').hide()
		}
		$('.addRowHeader').hide();
		$('.marksheet_inner').empty();
		
	    });
	}).change();
	
	$('.markbook_list').change(function(){
	    var marksheet_id = $(this).val();
	    if (!marksheet_id) return;
	    $.get('ajax_markbook.php?action=getMarkbook',{course_id: $('.course_list').val(), marksheet_id: marksheet_id}, function(data){
		$('.addRowHeader').fadeIn();
		$('.marksheet_inner').html(data).hide().fadeIn().find('.ms_border_left tr').find('td:first-child, th:first-child').remove();
		$('.marksheet_inner').find(':text').attr('disabled','disabled');
		$('.marksheet_inner .phase_start').css('cursor','auto').removeAttr('onmouseover').removeAttr('onmouseout').removeAttr('onmouseup').removeAttr('onmousedown').unbind('mouseover mouseout mouseup mousedown').find('.del_column_area').remove()
		
	    });
	});
	
	$('#addRowHeader').click(function(){
	    if ($(this).is(':checked')){
	    	var tbody1 = $('#table0 tbody tr');
			var tbody2 = $('#table1 tbody tr');
			var tbody3 = $('#table2 tbody tr');
		    if($(".phase_category").length > 0){
				for (i = 4; i < tbody1.length; i+=5){
					$(tbody1[i]).after($('#table0 tr:eq(1)').clone(true)).next().addClass("dummyRow0");
				    $(tbody1[i]).after($('#table0 tr:first').clone(true)).next().addClass("dummyRow0");
				    $(tbody2[i]).after($('#table1 tr:eq(1)').clone(true)).next().addClass("dummyRow1").find('input:hidden').remove();
				    $(tbody2[i]).after($('#table1 tr:first').clone(true)).next().addClass("dummyRow1").find('input:hidden').remove();
				    $(tbody3[i]).after($('#table2 tr:eq(1)').clone(true)).next().addClass("dummyRow2");
				    $(tbody3[i]).after($('#table2 tr:first').clone(true)).next().addClass("dummyRow2");
				}
		    }else{
				for (i = 4; i < tbody1.length; i+=5){
				    $(tbody1[i]).after($('#table0 tr:first').clone(true)).next().addClass("dummyRow0");
				    $(tbody2[i]).after($('#table1 tr:first').clone(true)).next().addClass("dummyRow1").find('input:hidden').remove();
				    $(tbody3[i]).after($('#table2 tr:first').clone(true)).next().addClass("dummyRow2");
				}
		    }	
		
	    }else{
		$(".dummyRow0").remove();
		$(".dummyRow1").remove();
		$(".dummyRow2").remove();
	    }
	    
	}).removeAttr('checked');
	
    });
</script>
<?=$courses_select?>

<select class="markbook_list" style="display:none">
    <option value="">-- <?=$Lang['Btn']['Select']?> --</option>
</select>
<div class="no_markbook_msg" style="display:none"><?=$Lang['Markbook']['NoMarkbook']?></div>

<div class="addRowHeader" style="display:none">
    <input id="addRowHeader" type="checkbox" name="addRowHeader">
    <label for="addRowHeader"><?=$Lang['Markbook']['displayColHeader']?></label>
</div>

<div class='marksheet_container1'><div class='marksheet_inner'></div></div>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>