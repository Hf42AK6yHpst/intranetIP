<?php
# editing by
/**
 * Change Log:
 * 2018-02-14 (Pun) [135488] [ip.2.5.9.3.1]
 *      - Added access right checking
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/settings.php");
include_once("$eclass40_filepath/system/settings/global.php");
include_once("$eclass40_filepath/src/lang/lang.".($intranet_session_language=='b5'?'chib5':'eng').".php");
include_once("$eclass40_filepath/src/includes/php/lib-marksheet.php");
include_once("$eclass40_filepath/src/includes/php/lib-groups.php");

opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-eClass"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

switch($action){
    case 'getMarkbookList':
	
	$sql = "select marksheet_id, title from marksheet order by title";
	
	$libdb = new libdb();
	$libdb->db = classNamingDB($course_id);
	$marksheets = $libdb->returnArray($sql);
		    
	foreach ($marksheets as $marksheet){
	    echo '<option value="'.$marksheet['marksheet_id'].'">'.$marksheet['title'].'</option>';
	}
			
    break;
    case 'getMarkbook':
	
	$ck_course_id = $course_id;
	
	$lg = new libgroups(classNamingDB($ck_course_id));
	
	$group_user 	= $lg->returnUserListInFunction('MS', $marksheet_id);
	$userid_list 	= (count($group_user) > 0) ? implode(',', $group_user) : '';
	$user_involved 	= $lg->Get_Course_Student(false, $userid_list, true);
	
	$lms = new marksheet($marksheet_id, false, $user_involved);
	
	echo $lms->display_marksheet();
	
    break;


}

?>