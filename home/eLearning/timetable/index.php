<?php
/**
 * Change Log:
 * 2020-03-30 Pun
 *  - File created
 */
include_once( "../../../includes/global.php" );
include_once( "../../../includes/libdb.php" );
include_once( "../../../includes/libuser.php" );

intranet_auth();
intranet_opendb();
header( "Content-Type:text/html;charset=UTF-8" );

//// Check access right START ////
$lu = new libuser( $UserID );

if ( ! $lu->isStudent() && ! $lu->isParent()  && ! $lu->isTeacherStaff() ) {
	echo 'Access denied';
	exit();
}
//// Check access right END ////

//// Init START ////
$studentId = ( $lu->isStudent() || $lu->isTeacherStaff() ) ? $UserID : IntegerSafe( $studentId );

define( 'DAYS_PER_WEEK', 7 );
$assetPath = '/home/eLearning/timetable/assets/';
//// Init END ////

if ( $intranet_session_language == "b5" ) {
	$pl2Lang = 'zh-hk';
} elseif ( $intranet_session_language == "gb" ) {
	$pl2Lang = 'zh-cn';
} else {
	$pl2Lang = 'en';
}

#### Parent START ####
$studentArr = array();
if ( $isFromPortal && $lu->isParent() ) {
	$rs = $lu->getChildrenList();
	foreach ( $rs as $student ) {
		$studentArr[ $student[0] ] = $student[1];
	}

	if ( ! $studentId ) {
		$studentId = $rs[0][0];
	}
}

#### Parent END ####

?><!doctype html>
<html>
<head>
    <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $Lang['eLearningTimetable']['eLearningTimetable'] ?></title>
    <link rel="stylesheet" type="text/css" href="<?= $assetPath ?>css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= $assetPath ?>css/style.css">
</head>
<body>

<div class="elearn-overview <?= ( $lu->isParent() ) ? 'parent' : '' ?>">
	<?php if ( $isFromPortal && count( $studentArr ) > 1 ): ?>
        <div class="dropdown select-child">
            <button
                    class="btn btn-sm dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
            >
				<?= $studentArr[ $studentId ] ?>
            </button>
            <div class="dropdown-menu dropdown-menu-right text-right" aria-labelledby="dropdownMenuButton">
				<?php foreach ( $studentArr as $sid => $studentName ): ?>
                    <a class="dropdown-item" href="?isFromPortal=1&studentId=<?= $sid ?>"><?= $studentName ?></a>
				<?php endforeach; ?>
            </div>
        </div>
	<?php endif; ?>

    <!-- add .ej for eClass Junior style, add .with-status if status available-->
    <div class="date-range">
        <a id="seeLastWeek" href="javascript:void(0)" class="btn-nav prev"></a>
        <div id="currentWeekText" class="date"></div>
        <a id="seeNextWeek" href="javascript:void(0)" class="btn-nav next"></a>
    </div>

    <div id="loadingContainer"
         class="week-table week-table-collapse js-week-table-tabs seven-cols"
    >
        <div class="loading">
            <div class="spinner-border text-info" role="status">
                <span class="sr-only">Loading...</span>
            </div>

        </div>
    </div>
    <div id="resultContainer"
         class="week-table week-table-collapse js-week-table-tabs seven-cols"
         style="display: none;"
    >
        <!-- tabs for mobile -->
        <div class="week-tab-list" role="tablist">
			<?php for ( $i = 0; $i < DAYS_PER_WEEK; $i ++ ): ?>
                <button class="btn-tab weekday_<?= $i ?>" role="tab"
                        aria-selected="false">
                    <span class="mobile weekdayTxt_<?= $i ?>"></span>
                    <div class="weekday"><?= $Lang['General']['DayType4'][ $i ] ?></div>
                </button>
			<?php endfor; ?>
        </div>

		<?php for ( $i = 0; $i < DAYS_PER_WEEK; $i ++ ): ?>
            <div style="order:0;" class="week-table-cell date weekday_<?= $i ?>">
                <span class="desktop weekdayTxt_<?= $i ?>"></span>
                (<?= $Lang['General']['DayType4'][ $i ] ?>)
            </div>
            <?php if($i === DAYS_PER_WEEK-1):?>
                <div style="order:0;" class="break"></div>
            <?php endif;?>

            <div style="order:1;" class="week-table-cell weekday_<?= $i ?> contentNormal">
                <div class="no-data">--</div>
            </div>
			<?php if($i === DAYS_PER_WEEK-1):?>
                <div style="order:1;" class="break"></div>
			<?php endif;?>

            <div style="order:2;" class="week-table-cell real-time weekday_<?= $i ?> contentRealTime">
                <div class="no-data">--</div>
            </div>
			<?php if($i === DAYS_PER_WEEK-1):?>
                <div style="order:2;" class="break"></div>
			<?php endif;?>
		<?php endfor; ?>
    </div>
</div>

<script id="itemTmpl" type="text/html">
    <div class="item">
        <div class="item-icon {{iconClass}}"></div>
        <div class="item-title"
             onclick="preDoJob({{onclickParam}})"
        >
            <div class="period {{periodClass}}">{{period}}</div>
            {{itemTitle}}
            <div class="due {{startTimeClass}}"><?= $Lang['eLearningTimetable']['startTime'] ?>：{{startTime}}</div>
            <div class="due {{dueDateClass}}"><?= $Lang['eLearningTimetable']['dueDate'] ?>：{{dueDate}}</div>
        </div>
        <div class="status"></div>
    </div>
</script>


<script type="text/javascript" src="/templates/moment/moment_2.24.0.min.js"></script>
<script type="text/javascript" src="/templates/script.js"></script>
<script type="text/javascript" src="<?= $assetPath ?>js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?= $assetPath ?>js/popper.min.js"></script>
<script type="text/javascript" src="<?= $assetPath ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assetPath ?>js/cols-to-tabs.js"></script>
<script type="text/javascript" src="<?= $assetPath ?>js/elearning-timetable.js"></script>
<!--<script type="text/javascript" src="--><? //= $assetPath ?><!--js/elearning-timetable.js?_=-->
<? //=date('Y-m-d_H:i:s')?><!--"></script>-->

<script>
  window.isFromPortal = false;
  window.timetableLang = {
    "dateNotStartedItem": '<?=$Lang['eLearningTimetable']['msg']['dateNotStartedItem']?>',
    "timeNotStartedItem": '<?=$Lang['eLearningTimetable']['msg']['timeNotStartedItem']?>',
    "notStartedLessonHours": '<?=$Lang['eLearningTimetable']['msg']['notStartedLessonHours']?>',
    "notStartedLessonMinutes": '<?=$Lang['eLearningTimetable']['msg']['notStartedLessonMinutes']?>',
    "lessonWillStartSoon": '<?=$Lang['eLearningTimetable']['msg']['lessonWillStartSoon']?>'
  };
  window.studentId = '<?=$studentId?>';
</script>
<?php if ( $isFromPortal ): ?>
    <form
            id="powerLessonLogin"
            name="powerLessonLogin"
            style="display:none;"
            action="<?= ( checkHttpsWebProtocol() ) ? 'https' : 'http' ?>://<?= $eclass40_httppath ?>/src/powerlesson/portal/login.php?enableUrlRedirect=1"
            target="PowerLesson20"
            method="POST"
    >
        <input type="hidden" name="sessionKey" value="<?= $lu->sessionKey ?>"/>
        <input type="hidden" name="sessionKeepAliveTime" value="<?= $session_expiry_time * 60 ?>"/>
        <input type="hidden" name="defaultLang" value="<?= $pl2Lang ?>"/>
        <input type="hidden" name="platformType" value="intranet"/>
        <input type="hidden" name="platformVersion" value="<?= Get_IP_Version() ?>"/>
        <input type="hidden" name="courseId" value=""/>
        <input type="hidden" name="lessonId" value=""/>
        <input type="hidden" name="phaseId" value=""/>
    </form>

    <script>
      window.userType = '<?=$lu->isTeacherStaff()? 'T' : ($lu->isParent()? 'P' : 'S')?>';
      window.isFromPortal = true;
      $(function () {
        var $tb = $(window.top.document).find('#TB_window');
        var $tbContent = $tb.find('#TB_ajaxContent');
        $tbContent.css('width', '100%');
        $tbContent.css('padding', '0');
      });
    </script>
<?php endif; ?>
</body>
</html>
