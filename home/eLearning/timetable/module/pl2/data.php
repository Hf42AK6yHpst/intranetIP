<?php
/**
 * Change Log:
 * 2020-03-11 Pun
 *  - File created
 */
intranet_auth();
intranet_opendb();

$_results = array();
$powerlesson = new PowerLesson();
$courses = $powerlesson->run(
    'getLessonsOfCoursesForELearningTimetable',
    array(
        'startDate' => "{$date} 00:00:00", // DateTime Object of start date
        'endDate' => "{$date} 23:59:59", // DateTime Object of end date
        'intranetUserId' => $studentId,
    )
);

if($_GET['debug']==1){
    debug_r(array(
        'startDate' => "{$date} 00:00:00", // DateTime Object of start date
        'endDate' => "{$date} 23:59:59", // DateTime Object of end date
        'intranetUserId' => $studentId,
    ));
    debug_r($courses);
    exit;
}

foreach ($courses as $d1) {
    $courseId = $d1['id'];
    foreach ($d1['lessons'] as $d2) {
        $lessonId = $d2['id'];
        $subjectId = $d2['subjectId'];
        $title = ($d2['title'])?$d2['title']:$Lang['eLearningTimetable']['untitledLessonPlan'];

        foreach ($d2['phases'] as $d3) {
            $phaseId = $d3['id'];
            $phaseType = $d3['phaseType'];
            $startDate = new DateTime($d3['startDate']);
            $startDate = $startDate->format('Y-m-d H:i:s');
            $endDate = new DateTime($d3['endDate']);
            $endDate = $endDate->format('Y-m-d H:i:s');

            if ($phaseType === 'PRE') {
                $titleExtra = $Lang['eLearningTimetable']['preLesson'];
            } elseif ($phaseType === 'POST') {
                $titleExtra = $Lang['eLearningTimetable']['postLesson'];
            }

            $_results[] = array(
                'id' => "$lessonId;{$phaseId}",
                'type' => "lesson_{$phaseType}",
                'title' => $title,
                'titleExtra' => $titleExtra,
                'subject_id' => $subjectId,
                'course_id' => $courseId,
                'start_time' => $startDate,
                'end_date' => $endDate
            );
        }
    }
}
//debug_r($_results);


return $_results;
