<?php
/**
 * Change Log:
 * 2020-03-11 Pun
 *  - File created
 */
intranet_auth();
intranet_opendb();

$_results = array();
foreach ($courseArr as $courseInfo) {
    $lo            = new libgroups(classNamingDB($courseInfo['course_id']));
    $exceptionList = $courseInfo['memberType'] === 'S'? $lo->returnFunctionIDs($cfg['group_function_type']['assessment'], $courseInfo['user_id']) : '0';
    $subSql        = $courseInfo['memberType'] === 'S'? "
        LEFT JOIN
            handin AS b 
        ON
            b.assignment_id = a.assignment_id AND
            b.user_id = '{$courseInfo['user_id']}' AND
            (a.fromlesson = '' OR a.fromlesson IS NULL)" : "";

    //// Get homework START ////
    $sql = "SELECT 
        a.assignment_id AS id,
        'homework' AS type,
        a.title,
        a.SubjectID as subject_id,
        '{$courseInfo['course_id']}' as course_id,
        a.StartDate as start_time,
        a.deadline as end_date
    FROM 
        assignment as a
    {$subSql}
    WHERE 
        DATE_FORMAT(a.StartDate, '%Y-%m-%d') = '{$date}'
    AND 
        a.assignment_id NOT IN ({$exceptionList})
    AND
        a.worktype <> 18
    ORDER BY 
        a.SubjectID ASC, a.title";
    $rs = $lo->returnArray($sql);
    $_results = array_merge($_results, $rs);
    //// Get homework END ////
}

return $_results;
