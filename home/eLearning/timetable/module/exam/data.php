<?php
/**
 * Change Log:
 * 2020-03-11 Pun
 *  - File created
 */
intranet_auth();
intranet_opendb();

$_results = array();
foreach ($courseArr as $courseInfo) {
    $lo            = new libgroups(classNamingDB($courseInfo['course_id']));
    $exceptionList = $courseInfo['memberType'] === 'S'? $lo->returnFunctionIDs($cfg['group_function_type']['exam'], $courseInfo['user_id']) : '0';  // Get homework with group not include user

    //// Get exam/practice START ////
    $sql = "SELECT 
        q.quiz_id AS id,
        IF(ttype=1,'exam','exercise') AS type,
        q.title,
        q.SubjectID as subject_id,
        '{$courseInfo['course_id']}' as course_id,
        q.startdate as start_time,
        q.enddate as end_date
    FROM 
        quiz as q
    WHERE 
        DATE_FORMAT(q.startdate, '%Y-%m-%d') = '{$date}'
    AND 
        q.quiz_id NOT IN ({$exceptionList})
    AND
        q.status='1'
    ORDER BY 
        ttype, q.SubjectID ASC, q.title";
    $rs = $lo->returnArray($sql);


    $_results = array_merge($_results, $rs);
    //// Get exam/practice END ////
}

return $_results;
