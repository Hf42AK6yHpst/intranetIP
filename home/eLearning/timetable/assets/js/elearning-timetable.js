$(function () {

  //// Variables START ////
  var DAYS_PER_WEEK = 7;
  var todayDate = moment().startOf('day');
  var viewWeekFirstDate = todayDate.clone().startOf('week');
  //// Variables END ////

  //// Helper function START ////
  var debounce = (function (fn, delay, atBegin) {
    var timer = null, last = 0, during;
    return function () {
      var self = this, args = arguments;
      var exec = function () {
        fn.apply(self, args);
      }
      if (atBegin && !timer) {
        exec();
        atBegin = false;
      } else {
        during = Date.now() - last;
        if (during > delay) {
          exec();
        } else {
          if (timer) clearTimeout(timer);
          timer = setTimeout(function () {
            exec();
          }, delay);
        }
      }
      last = Date.now();
    }
  });

  var dateToStr = (function (date) {
    if (typeof (date) === 'string') {
      date = moment(date);
    }

    return date.format('YYYY-MM-DD');
  });

  var timeToStr = (function (date) {
    if (typeof (date) === 'string') {
      date = moment(date);
    }

    return date.format('HH:mm');
  });
  //// Helper function END ////

  //// Basic UI START ////
  var updateDateHeader = (function () {
    var loopDate = viewWeekFirstDate.clone();

    for (var i = 0; i < DAYS_PER_WEEK; i++) {
      $('.desktop.weekdayTxt_' + i).html(loopDate.format('DD/MM'));
      $('.mobile.weekdayTxt_' + i).html(loopDate.format('DD'));
      loopDate.add(1, 'days');
    }
    loopDate.subtract(1, 'days');

    if (viewWeekFirstDate.isSame(loopDate, 'month')) {
      $('#currentWeekText').html(
        viewWeekFirstDate.format('DD - ' + loopDate.format('DD') + '/MM/YYYY')
      );
    } else if (viewWeekFirstDate.isSame(loopDate, 'year')) {
      $('#currentWeekText').html(
        viewWeekFirstDate.format('DD/MM') +
        ' - ' +
        loopDate.format('DD/MM/YYYY')
      );
    } else {
      $('#currentWeekText').html(
        viewWeekFirstDate.format('DD/MM/YYYY - ') + loopDate.format('DD/MM/YYYY')
      );
    }
  });

  var updateDateTodayFuture = (function () {
    var loopDate = viewWeekFirstDate.clone();
    $('.today, .future').removeClass('today future');

    for (var i = 0; i < DAYS_PER_WEEK; i++) {
      if (todayDate.isSame(loopDate, 'day')) {
        $('.weekday_' + i).addClass('today');
      } else if (todayDate.isBefore(loopDate)) {
        $('.contentNormal .weekday_' + i).addClass('future');
      }
      loopDate.add(1, 'days');
    }
  });
  //// Basic UI END ////


  //// Generate data START ////
  var getData = (function (loopDate) {
    return $.get('ajax.php', {
      'date': loopDate.format('YYYY-MM-DD'),
      'studentId': window.studentId
    })
  });

  var allData = {};
  var _loadData = (function (date) {
    var loopDate = date.clone();
    var promiseArr = [];

    for (var i = 0; i < DAYS_PER_WEEK; i++) {
      var loopDateStr = loopDate.format('YYYY-MM-DD');
      if (typeof (allData[loopDateStr]) === 'undefined') {
        promiseArr.push(getData(loopDate));
      } else {
        promiseArr.push($.Deferred().resolve(allData[loopDateStr]));
      }
      loopDate.add(1, 'days');
    }

    $('.narrow').removeClass('narrow');
    $.when.apply($, promiseArr).done(function () {
      for (var i = 0; i < arguments.length; i++) {
        if (typeof (arguments[i][0]) === 'string') {
          var data = JSON.parse(arguments[i][0]);
          allData[data.date] = data;
        }
      }

      loopDate = viewWeekFirstDate.clone();
      for (var i = 0; i < DAYS_PER_WEEK; i++) {
        if (typeof (allData[loopDate.format('YYYY-MM-DD')]) !== 'undefined') {
          updateContent(allData[loopDate.format('YYYY-MM-DD')]);
        }
        loopDate.add(1, 'days');
      }

      $('#loadingContainer').hide();
      $('#resultContainer').show();
    });
  });
  var loadData = (debounce(function (time) {
    _loadData(time);
  }, 2000, true));
  //// Generate data END ////

  //// Update content START ////
  var createItemContainer = (function (title) {
    var $subjectDiv = $('<div class="subject">');
    var $subjectTitleDiv = $('<div class="subject-title">');
    $subjectDiv.append($subjectTitleDiv);
    $subjectTitleDiv.append(title);

    return $subjectDiv;
  });

  var itemTmpl = $('#itemTmpl').html();
  var createItem = (function (date, data) {
    if (typeof (data) === 'undefined' || data.length === 0) {
      return '';
    }

    var normalHtml = '';
    var realTimeHtml = '';
    for (var j = 0; j < data.length; j++) {
      var itemHtml = itemTmpl;
      var isFuture = moment().isBefore(data[j].startDateTime);

      var param = '';
      param += "'" + data[j].startDateTime + "'";
      param += ", '" + data[j].courseId + "'";
      param += ", '" + data[j].userCourseId + "'";
      param += ", '" + data[j].memberType + "'";
      param += ", '" + data[j].type + "'";
      param += ", '" + data[j].id + "'";
      itemHtml = itemHtml.replace('{{onclickParam}}', param);

      if (data[j].type === 'lesson_PRE' || data[j].type === 'lesson_POST') {
        itemHtml = itemHtml.replace('{{itemTitle}}', data[j].title + ' (' + data[j].titleExtra + ')');
      } else {
        itemHtml = itemHtml.replace('{{itemTitle}}', data[j].title);
      }

      if (data[j].type === 'lesson_IN') {
        var period = '';
        period += timeToStr(data[j].startDateTime) + ' - ';
        period += timeToStr(data[j].endDate);

        itemHtml = itemHtml.replace('{{period}}', period);
        itemHtml = itemHtml.replace('{{startTimeClass}}', 'd-none');
        itemHtml = itemHtml.replace('{{dueDateClass}}', 'd-none');
      } else if (isFuture) {
        itemHtml = itemHtml.replace('{{periodClass}}', 'd-none');
        itemHtml = itemHtml.replace('{{startTime}}', timeToStr(data[j].startDateTime));
        itemHtml = itemHtml.replace('{{dueDateClass}}', 'd-none');
      } else if (
        typeof (data[j].endDate) === 'undefined' ||
        data[j].endDate === '0000-00-00 00:00:00' ||
        data[j].endDate === '0000-00-00' ||
        data[j].endDate === null ||
        data[j].endDate === ''
      ) {
        itemHtml = itemHtml.replace('{{periodClass}}', 'd-none');
        itemHtml = itemHtml.replace('{{startTimeClass}}', 'd-none');
        itemHtml = itemHtml.replace('{{dueDateClass}}', 'd-none');
      } else {
        itemHtml = itemHtml.replace('{{periodClass}}', 'd-none');
        itemHtml = itemHtml.replace('{{startTimeClass}}', 'd-none');
        itemHtml = itemHtml.replace('{{dueDate}}', data[j].endDate.slice(0, -3));
      }

      if (data[j].type === 'econtent') {
        itemHtml = itemHtml.replace('{{iconClass}}', 'view');
      } else if (data[j].type === 'assessment') {
        itemHtml = itemHtml.replace('{{iconClass}}', 'work');
      } else if (data[j].type.indexOf('lesson_') > -1) {
        itemHtml = itemHtml.replace('{{iconClass}}', 'lesson');
      } else {
        itemHtml = itemHtml.replace('{{iconClass}}', '');
      }

      if (data[j].type === 'lesson_IN') {
        realTimeHtml += itemHtml;
      } else {
        normalHtml += itemHtml;
      }
    }

    return {
      normalHtml: normalHtml,
      realTimeHtml: realTimeHtml,
    };
  });

  var updateContent = (function (data) {
    var weekday = moment(data.date).weekday();
    var hasNormalContent = false;
    var hasRealTimeContent = false;

    $.each(data.courseInfo, function (courseId, courseInfo) {
      var $container = createItemContainer(courseInfo.title);

      var itemHtml = createItem(data.date, data.data[courseId]);
      if (itemHtml.normalHtml) {
        hasNormalContent = true;
        $('.contentNormal.weekday_' + weekday).append($container.clone().append(itemHtml.normalHtml));
      }
      if (itemHtml.realTimeHtml) {
        hasRealTimeContent = true;
        $('.contentRealTime.weekday_' + weekday).append($container.clone().append(itemHtml.realTimeHtml));
      }
    });

    if (!hasNormalContent) {
      $('.contentNormal.weekday_' + weekday).html('<div class="no-data">--</div>');
    }
    if (!hasRealTimeContent) {
      $('.contentRealTime.weekday_' + weekday).html('<div class="no-data">--</div>');
    }

    if (!hasNormalContent && !hasRealTimeContent && (weekday === 0 || weekday === 6)) {
      $('.weekday_' + weekday).addClass('narrow');
    }
  });
  //// Update content END ////

  //// Do job START ////
  var enterPl2 = (function (courseId, lessonId, phaseId) {
    var $pl2LoginForm = $('#powerLessonLogin');
    $pl2LoginForm.find('[name="courseId"]').val(courseId);
    $pl2LoginForm.find('[name="lessonId"]').val(lessonId);
    $pl2LoginForm.find('[name="phaseId"]').val(phaseId);
    $pl2LoginForm.submit();
  });
  window.preDoJob = (function (startDateTime, courseId, userCourseId, memberType, type, id) {
    $('#resultContainer').hide();
    $('#loadingContainer').show();
    if (type === 'lesson_IN') {
      var idArr = id.split(';');
      $.get('module/pl2/lessonStatus.php', {
        'courseId': courseId,
        'lessonId': idArr[0]
      }, function (res) {
        if (memberType === 'T' || res === 'IN_PROGRESS' || res === 'FINISHED') {
          enterPl2(courseId, idArr[0], idArr[1]);
        } else {
          var now = moment();
          var start = moment(startDateTime);
          var diffHours = start.diff(now, 'hours');
          var diffMinutes = start.diff(now, 'minutes');

          if (diffHours > 0) {
            var timeStr = '';
            if (start.isSame(now, 'day')) {
              timeStr = start.format('HH:mm');
            } else {
              timeStr = start.format('YYYY-MM-DD HH:mm');
            }
            alert(window.timetableLang.notStartedLessonHours.replace('{{time}}', timeStr));
          } else if (diffMinutes > 0) {
            alert(window.timetableLang.notStartedLessonMinutes.replace('{{time}}', diffMinutes));
          } else {
            alert(window.timetableLang.lessonWillStartSoon);
          }
        }
      }).always(function () {
        $('#loadingContainer').hide();
        $('#resultContainer').show();
      });
    } else {
      $('#loadingContainer').hide();
      $('#resultContainer').show();
      window.doJob(startDateTime, courseId, userCourseId, memberType, type, id);
    }
    return false;
  });
  window.doJob = (function (startDateTime, courseId, userCourseId, memberType, type, id) {
    if (!window.isFromPortal || !(window.userType === 'T' || window.userType === 'S')) {
      // console.error('From App')
      return;
    }

    var now = moment();
    var itemDateTime = moment(startDateTime);
    if (memberType === 'S' && now.isBefore(itemDateTime)) {
      if (now.isSame(itemDateTime, 'day')) {
        alert(window.timetableLang.timeNotStartedItem.replace('{{time}}', timeToStr(itemDateTime)));
      } else {
        alert(window.timetableLang.dateNotStartedItem.replace('{{time}}', dateToStr(itemDateTime)));
      }
      // console.error('Future date')
      return;
    }

    var url = 'ajax.php?action=login&user_course_id=' + userCourseId;
    if (type === 'econtent') {
      newWindow(url + '&direct=econtent&id=' + id, 8);
    } else if (type === 'assessment') {
      newWindow(url + '&direct=assignment&id=' + id, 8);
    } else if (type.indexOf('lesson_') > -1 && type !== 'lesson_IN') {
      var idArr = id.split(';');
      enterPl2(courseId, idArr[0], idArr[1]);
    }
  });
  //// Do job END ////

  //// Init START ////
  $('#seeLastWeek').click(function () {
    $('#loadingContainer').show();
    $('#resultContainer').hide();
    $('.contentNormal, .contentRealTime').html('');
    viewWeekFirstDate.subtract(7, 'days');

    updateDateHeader();
    updateDateTodayFuture();
    loadData(viewWeekFirstDate);
  });
  $('#seeNextWeek').click(function () {
    $('#loadingContainer').show();
    $('#resultContainer').hide();
    $('.contentNormal, .contentRealTime').html('');

    viewWeekFirstDate.add(7, 'days');
    updateDateHeader();
    updateDateTodayFuture();
    loadData(viewWeekFirstDate);
  });

  $('.weekday_' + todayDate.weekday()).click();
  $('.contentNormal, .contentRealTime').html('');
  updateDateHeader();
  updateDateTodayFuture();
  loadData(viewWeekFirstDate);
  //// Init END ////


  /* /// Thickbox START ////
  var $tb = $(window.top.document).find('#TB_closeAjaxWindow');
  var $a = $('<a>close</a>').click(function(){
    $(document.body).empty();
    window.top.tb_remove();
  });
  $tb.html($a);
  //// Thickbox END /// */
});

