(function ($) {
  "use strict";
  $.fn.responsiveTable = function() { 

    var toggleColumns = function($table) {
      var selectedControls = [];
      $table.find(".btn-tab").each( function() {
        selectedControls.push( $(this).attr("aria-selected") );
      });
      var cellCount = 0, colCount = 0;
      var setNum = $table.find(".week-table-cell").length / Math.max( $table.find(".btn-tab").length );
      $table.find(".week-table-cell").each( function() {
        $(this).addClass("hidden-mobile");
        if( selectedControls[colCount] === "true" ) $(this).removeClass("hidden-mobile");
        cellCount++;
        if( cellCount % setNum === 0 ) colCount++; 
      });
    };
    $(this).each(function(){ toggleColumns($(this)); });

    $(this).find(".btn-tab").click( function() {
      $(this).attr("aria-selected","true").siblings().attr("aria-selected","false");
      toggleColumns( $(this).parents(".week-table") );
    });
	  
  };
}(jQuery));


$(".js-week-table-tabs").responsiveTable();