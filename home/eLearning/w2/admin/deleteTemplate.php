<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2SchemeConfigGenerator.php');
include_once($PATH_WRT_ROOT.'includes/w2/w2ShareContentConfig.inc.php');

	GLOBAL $w2_cfg;
	$configFilePath = $PATH_WRT_ROOT."home/eLearning/w2/content/";
	$configFileName = "_w2_contentConfig.inc.php";
	$r_contentCode = isset($_GET['r_contentCode'])?$_GET['r_contentCode']:exit;
	$cid = isset($_GET['cid'])?$_GET['cid']:exit;
	$objDB = new libdb();
	$sql = 
	"
	Update
		W2_CONTENT_DATA
	Set 
		status = ".W2_DELETED.",
		dateDeleted = now(),
		deletedBy = ${_SESSION[UserID]}
	Where 
		cid = ${cid};
	";

	$result = $objDB->db_db_query($sql);
	if($result)
	{
//		if($r_contentCode=='eng'){
//			$schoolCode = strtolower($w2_cfg["schoolCodeCurrent"]);
//			$configFullPath = $configFilePath.$schoolCode.$configFileName;
//			$objW2SchemeConfigGenerator = new libW2SchemeConfigGenerator();
//			$objW2SchemeConfigGenerator->generateConfigFile($configFullPath,$schoolCode=$w2_cfg["schoolCodeCurrent"]);
//		}	
		$msg = 'DeleteSuccess';	
	}
	else
	{
		$msg = 'DeleteFail';
	}
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: /home/eLearning/w2/index.php?mod=admin&task=templateManagement&r_contentCode=${r_contentCode}&cid=${cid}&r_returnMsgKey=${msg}");		

?>