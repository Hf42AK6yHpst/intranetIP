<?
include_once($PATH_WRT_ROOT."includes/w2/w2ShareContentConfig.inc.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$eng_dirList= array(
	'c1/'=>array('model.php', 'view.php'),
	'c2/'=>array('model.php', 'view.php'),
	'c3/'=>array('model.php', 'view.php'),
	'c4/'=>array('model.php', 'view.php'),
	'c5/'=>array('model.php', 'view.php'),
	'reference/'=>array('referenceGrammer.php', 'referenceVocab.php'),
	''=>array('model.php')
);
		
GLOBAL $w2_cfg;
$objDB = new libdb();
//$r_publish_type = (trim($_GET['r_publish_type'])=='')?exit():$_GET['r_publish_type'];
$r_contentCode = (trim($_POST['r_contentCode'])=='')?exit():$_POST['r_contentCode'];
$schemeInfo = ($_POST['schemeInfo']=='')?exit():$_POST['schemeInfo'];
$schemeData=explode('-To-',$schemeInfo,2);
$cid = ($_POST['cid']=='')?exit():$_POST['cid'];

$arySchoolList = array();
$aryContent = array();

$schoolConfig = array();
$schoolConfig['betaSite'] = array(
							'server'=>'ip25-tc-beta.eclass.com.hk',
							'login'=>'writing20',
							'pwd'=>'ftpw20',
						);
//$schoolConfig['schoolA'] = array(
//							'server'=>'118.142.70.142',
//							'login'=>'fai',
//							'pwd'=>'djfkds3333',
//						);
//
//$schoolConfig['schoolB'] = array(
//							'server'=>'118.142.70.142x',
//							'login'=>'faix',
//							'pwd'=>'djfkds3333x',
//						);

$arySchoolList = 'betaSite';
//$arySchoolList =$schemeData[1];
$aryContent =strToLower($w2_cfg["schoolCodeCurrent"]).'_scheme'.$schemeData[0];		

$data = strToLower($w2_cfg["schoolCodeCurrent"]).",".$r_contentCode.",".$schemeData[0];
file_put_contents('/tmp/ftpControlFile.inc.ini',$data);

// source file
$srcPath = $intranet_root."/home/eLearning/w2/content/".$r_contentCode."/".$aryContent;
if( !engFileValidation($srcPath,$w2_cfg["schoolCodeCurrent"]) )
{
	return false;
}

//$start = microtime(true);
////$result = start_publish($aryContent,$arySchoolList,$r_contentCode);
$result = start_publish($aryContent,$arySchoolList,$r_contentCode);
//$total = microtime(true) - $start;
if($result != '')
{
	$status = W2_SHARED_SUCCEED;
	
}
else
{
	$status = W2_SHARED_FAIL;
}


//Add shared record
$sql =
"
Insert Into 
	W2_SHARED_RECORD
	(cid, r_contentCode, targetSchoolCode, status, sharedDate)
VALUES
	(${cid}, '${r_contentCode}', '${schemeData[1]}', ${status}, now())
";

$result = $objDB->db_db_query($sql);


if($result !='')
{
	//Change content status to 'published'
	$sql =
	"
	Update
		W2_CONTENT_DATA
	SET
		status = ".W2_PUBLISHED.", datePublished = now()
	Where
		cid = ${cid};
	";
	$result = $objDB->db_db_query($sql);
}
echo $result;
//echo "Time consumed: ${total},<br/> ${result} gets";

//exit();
#################################################
function start_publish($aryContent, $schoolList,$contentCode){
	global $intranet_root,$w2_cfg;
	
	$_schemeCode = $aryContent; 

//	$tarFileArray = tarFile($_schemeCode,$contentCode,$arySchoolList);
	
	// config file
	$srcConfigFileFullPath = $intranet_root.'/home/eLearning/w2/content/'.strToLower($w2_cfg["schoolCodeCurrent"]).'_w2_contentConfig.inc.php';

	// control file
	$srcControlFileFullPath = '/tmp/ftpControlFile.inc.ini';
	
	$fileArray = array ($srcControlFileFullPath,$srcConfigFileFullPath);

	if( $fileArray == true && $_schemeCode){
		
		$result = ftpFile($fileArray, $_schemeCode,$schoolList,$contentCode);
		if($result)
		{
			$result = getFtpResult($fileArray,$schoolList);
			return $result;
		}
	}
		
	
}
//function start_publish($aryContent, $arySchoolList,$contentCode){
//	if(!is_array($aryContent)){
//		$aryContent = array($aryContent);
//	}
//
//	if(!is_array($arySchoolList)){
//		$arySchoolList = array($arySchoolList);
//	}
//
//	for($i = 0,$iMax = count($aryContent);$i < $iMax; $i++){
//		$_schemeCode = $aryContent[$i]; 
//
//		$tarFile = tarFile($_schemeCode,$contentCode);
//		if($tarFile !== false){
//			for($s = 0,$sMax = count($arySchoolList);$s < $sMax; $s++)
//			{
//				$result = ftpFileToSchools($tarFile,$arySchoolList[$s]);
//
//				if($result)
//				{
//					$result = getFtpResult($tarFile,$arySchoolList[$s]);
//					debug_r($result);
//					echo $result;
//				}
//			}
//		}
//	}
//}

function engFileValidation($srcPath,$schoolCode)
{
	GLOBAL $w2_cfg;
	if(file_exists($srcPath))
	{
		global $eng_dirList;
		foreach((array)$eng_dirList as $key => $fileList) //forreach directory
		{
			foreach( (array)$fileList as $file) //foreach files in a directory
			{
				if(!file_exists($srcPath.'/'.$key.$file))
				{
					bl_error_log($w2_cfg['moduleCode'],'Error: Incomplete content file.<br />');
					return false;
				}
			}
		}
		return true;
	}
	else
	{
		bl_error_log($w2_cfg['moduleCode'],'Error: Path ('.$srcPath.') does not exist.<br />');
		return false;
	}
}
function getFtpResult($tarFile, $schoolCode){
	//f:curl_init copy from http://www.lornajane.net/posts/2011/posting-json-data-with-php-curl
	//	$data_string = json_encode($data);
	$data_string = '';
	global $schoolConfig;
	
	$ftp_server = $schoolConfig[$schoolCode]['server'];
	$url = $ftp_server.'/home/eLearning/w2/content/test_publish.php';
	$ch = curl_init($url);   

//	$ch = curl_init('http://192.168.0.149/test/test_publish.php');                                                                      
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);                                                                                                                   
	 
	$result = curl_exec($ch);
	return $result;
//	$objJson = new JSON_obj();
//	$jsonStr = $objJson->decode($result);
//	return $jsonStr;
}

//function ftpFileToSchools($tarFileArray , $arySchoolList){
//	ftpFile($tarFileArray ,$arySchoolList);
//	return true;
//}
//function ftpFileToSchools($tarFile , $arySchoolList){
//	if(!is_array($arySchoolList)){
//		$arySchoolList = array($arySchoolList);
//	}
//
//	for($i = 0,$iMax = count($arySchoolList);$i < $iMax; $i++){
//		ftpFile($tarFile ,$arySchoolList[$i]);
//	}
//	return true;
//}

function ftpFile($aryFile, $schemeName, $schoolCode,$contentCode){
	global $w2_cfg, $schoolConfig, $eng_dirList,$intranet_root;;
	if(!is_array($aryFile)){
		$aryFile = array($aryFile);
	}

	$ftp_server = $schoolConfig[$schoolCode]['server'];
	$conn_id = ftp_connect($ftp_server);

	if($conn_id === false){
		$err = 'FTP connect fail. Server Id '.$ftp_server;
		bl_error_log($w2_cfg['moduleCode'],$err);
		return false;
	}

	$ftp_user_name = 	$schoolConfig[$schoolCode]['login'];
	$ftp_user_pass = 	$schoolConfig[$schoolCode]['pwd'];;

	$login_result = ftp_login($conn_id,$ftp_user_name,$ftp_user_pass);
	if($login_result !== true){
		bl_error_log($w2_cfg['moduleCode'],'Cannot login to ftp server ['.$ftp_server.']!!');
		return false;
	}
	// control file, config file
	for($i = 0,$iMax = count($aryFile); $i < $iMax; $i++){
			$_file = $aryFile[$i];
			if(!file_exists($_file)){				
				bl_error_log($w2_cfg['moduleCode'],'Try to ftp a file '.$_file.' but the src file does not exist');
			}else{
				if(!is_readable($_file)){
					bl_error_log($w2_cfg['moduleCode'],'Try to ftp a file '.$_file.' but src file does not readable by server');
				}else{
					//$fp = fopen($_file, 'r');
					//try to upload $file
					//if (ftp_fput($conn_id, $_file, $fp, FTP_ASCII)) {

					$remote_file = get_file_basename($_file);

					if (ftp_put($conn_id, $remote_file, $_file, FTP_BINARY)) {
//						if (ftp_chmod($conn_id, 0644, $remote_file) !== false) {
//							bl_error_log($w2_cfg['moduleCode'],$result);
//						} 
//						else 
//						{
//							$result =  "could not chmod $remote_file.";
//							bl_error_log($w2_cfg['moduleCode'],$result);
//						}
					} else {
						bl_error_log($w2_cfg['moduleCode'],'Ftp a file '.$_file.' to ['.$ftp_server.'] but failed.');
					}
					//fclose($fp);
				}
			}
	}
	ftp_chdir($conn_id, $contentCode);
	$dirList = ftp_nlist($conn_id, '.');
//	echo debug_r(in_array($schemeName, $dirList))."<br>";
	if( !in_array($schemeName, $dirList)  )
	{
		ftp_mkdir($conn_id, $schemeName);
	}
	ftp_chdir($conn_id, $schemeName);
//	echo ftp_pwd($conn_id)."<br/>";
	
	$dirList = ftp_nlist($conn_id, '.');
	foreach((array)$eng_dirList as $key => $fileList) //forreach directory
	{

		if($key !='')
		{
			$compareKey = explode("/", $key);
			if( !in_array( $compareKey[0], $dirList)  )
			{
				ftp_mkdir($conn_id, $key);
			}
			ftp_chdir($conn_id, $key);
		}
//		echo ftp_pwd($conn_id)."<br/>";
		
		foreach( (array)$fileList as $file) //foreach files in a directory
		{
			$_file = $intranet_root."/home/eLearning/w2/content/".$contentCode.'/'.$schemeName.'/'.$key.$file;
			if(!file_exists($_file)){				
				bl_error_log($w2_cfg['moduleCode'],'Try to ftp a file '.$_file.' but the src file does not exist');
			}else{
				if(!is_readable($_file)){
					bl_error_log($w2_cfg['moduleCode'],'Try to ftp a file '.$_file.' but src file does not readable by server');
				}else{
					$remote_file = get_file_basename($_file);
					if (ftp_put($conn_id, $remote_file, $_file, FTP_BINARY)) {
					} else {
						bl_error_log($w2_cfg['moduleCode'],'Ftp a file '.$_file.' to ['.$ftp_server.'] but failed.');
					}

					//fclose($fp);
				}
			}
		}
		if($key !='')
		{
			ftp_chdir($conn_id, '../');
		}
//		echo ftp_pwd($conn_id)."<br/>";		
	}
	
	ftp_close($conn_id);
	return true;
}

// function tarFile($schemeCode,$contentCode,$arySchoolList){
//	global $intranet_root,$w2_cfg;
//
//	// source file
//	$srcDir = $intranet_root."/home/eLearning/w2/content/".$contentCode."/";
//	$srcFile = $schemeCode;	
//	$tarFile = '/tmp/'.$schemeCode.'.tar';
//	
////	// config file
////	$srcConfigDir = $intranet_root.'/home/eLearning/w2/content/';
////	$srcConfigFile = strToLower($w2_cfg["schoolCodeCurrent"]).'_w2_contentConfig.inc.php';				
////	$tarConfigFile = '/tmp/config.tar';
////
////	// control file
////	$srcControlDir = '/tmp/';
////	$srcControlFile = 'ftpControlFile.inc.ini';
////	$tarControlFile = '/tmp/control.tar';
//	
//	$srcDirList = array($srcDir); 
//	$srcFileList = array($srcFile); 
//	$tarFileList = array($tarFile); 
//	
//	foreach( (array)$tarFileList as $tarFileItem)
//	{
//		if(file_exists($tarFileItem))
//		{
//			$result = unlink ($tarFileItem);
//		}
//	}
//	$loopCounter = count($srcDirList);
//	for( $i=0; $i <$loopCounter; $i++ )
// 	{
// 		$srcFileFullPath = $srcDirList[$i].$srcFileList[$i];
//		if(!file_exists($srcFileFullPath)){
//
//			bl_error_log($w2_cfg['moduleCode'],'Request tar file but src dir / file does not eixst ['.$srcFileFullPath.']');
//			return false;
//		}
//	
//		//If it has already existed , check if it is writable
//		if(file_exists($tarFileList[$i])){
//			if(!is_writable($tarFileList[$i])){
//				$cmd = 'ls  -ltr '.$tarFileList[$i];
//				exec($cmd,$output);
//				echo 'File is not writable ['.$srcFileFullPath.']!'; 
//				bl_error_log($w2_cfg['moduleCode'],'Try to overwrite a file ['.$tarFileList[$i].'] but it is unwritable by server!!');
//				//write to log
//				return false;
//			}
//		}
//		
////		$cmd = "tar -cvf ".$tarFileList[$i]." ".$srcDirList[$i].$srcFileList[$i]." --strip-components 5";
//		$cmd = "tar -cvf ".$tarFileList[$i]." ".$srcDirList[$i].$srcFileList[$i];
//
//		exec ( $cmd ,$output ,$return_var);
//		if(!file_exists($tarFileList[$i])){
//			$cmd = 'df -k';
//			exec($cmd);
//			//write log
//			bl_error_log($w2_cfg['moduleCode'],'Suppose a tar file must exist ['.$tarFileList[$i].'],  but it doe not');
//			return false;
//		}
// 	}
////	return $tarFile;
//	return $tarFileList;
// }

function bl_error_log($projectName='',$errorMsg='',$errorType=''){
//	$mail = 'mfkhoe@g2.broadlearning.com';
	$mail = 'adamwan@g4.broadlearning.com';
	mail($mail,"Project {$projectName} - ".date("F j, Y, g:i a"),$errorMsg);
	return true;
}
?>