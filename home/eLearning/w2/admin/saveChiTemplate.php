<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentGenerator.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
$cid = trim($_POST['cid']);
$r_contentCode = trim($_POST['r_contentCode']);
$saveTab = isset($_POST['saveTab'])?trim($_POST['saveTab']):exit;
$parseData = $engContentParser->parseEngContent($cid);
### DB Control Info
$schemeNum= $objEngContentBuilder->getNextSchemeNum($r_contentCode);
if($r_contentCode!='chi'){exit;}
//debug_r($_POST);exit;
try 
{
	
	switch($saveTab)
	{
		case 0:
			### General column
			
			$topicIntro = isset($_POST['topicIntro'])?trim($_POST['topicIntro']):exit;
			$topicName = isset($_POST['topic'])?trim($_POST['topic']):exit;
			$category =  isset($_POST['category'])?$Lang['W2']['textTypeAry'][$_POST['category']]:exit;
			$grade =  isset($_POST['grade'])?trim($_POST['grade']):exit;			

			if( empty($cid) )
			{
				$buildData[]= $objEngContentBuilder->buildChiGeneralInfoInsert($topicIntro,$topicName,$category,$grade);
			}
			else
			{
				$buildData[]= $objEngContentBuilder->buildChiGeneralInfoUpdate($topicIntro,$topicName,$category,$grade);
			}
			break;
		case 1:
			### Step1 column
			$Step1Data = array();
			$Step1Data['step1Int'] =  isset($_POST['step1Int'])?trim($_POST['step1Int']):exit;
			$Step1Data['step1WordCount'] = isset($_POST['step1WordCount'])?trim($_POST['step1WordCount']):exit;
			$Step1Data['step1TextType'] = isset($_POST['step1TextType'])?implode(',',$_POST['step1TextType']):exit;
			$Step1Data['step1TopicKeyword'] = isset($_POST['step1TopicKeyword'])?trim($_POST['step1TopicKeyword']):exit;
			$Step1Data['step1KeywordMeaning'] = isset($_POST['step1KeywordMeaning'])?trim($_POST['step1KeywordMeaning']):exit;
			$buildData[]= $objEngContentBuilder->buildChiStep1($Step1Data);
			break;
		case 2:
			### Step2 column
			$referenceAry = !empty($parseData['step2Data']['step2ReferenceAry']['referenceTopic'])?$parseData['step2Data']['step2ReferenceAry']:'';
			$step2Int = isset($_POST['step2Int'])?trim($_POST['step2Int']):exit;
			$step2ConceptType = isset($_POST['conceptType'])?trim($_POST['conceptType']):exit;
			$buildData[] = $objEngContentBuilder->buildChiStep2($step2Int,$step2Concept,$referenceAry);
			if($parseData['status']==W2_GENERATED){
				$W2EngContentGenerator=new libW2GenerateEngContent($PATH_WRT_ROOT);
				$W2EngContentGenerator->getMindMapGeneration($parseData, $w2_classRoomDB,$eclass_db);
			}
			break;		
		case 3:
			### Step3 column
			$step3Data = $parseData['step3Data'];			
			$step3Int = isset($_POST['step3Int'])?intranet_htmlspecialchars(trim($_POST['step3Int'])):exit;
			$step3Writing = !empty($step3Data['step3Writing'])?$step3Data['step3Writing']:'';
			$step3RemarkAry = $step3Data['step3RemarkAry'];
			$buildData[]= $objEngContentBuilder->buildChiStep3($step3Int,$step3Outline,$step3Writing,$step3RemarkAry);

		
			break;
		case 4:
			### Step4 column
			$step4Int = isset($_POST['step4Int'])?intranet_htmlspecialchars(trim($_POST['step4Int'])):exit;
			$step4Data = $parseData['step4Data'];
			$step4Vocab = $step4Data['step4Vocab'];
			$step4Grammar = $step4Data['step4Grammar'];
			$buildData[]= $objEngContentBuilder->buildChiStep4($step4Int,$step4Approach,$step4ExampleSentence,$step4Vocab,$step4Grammar);
			
			break;
		case 5:
			### Step5 column
			$step5Int = isset($_POST['step5Int'])?trim($_POST['step5Int']):exit;
			$step5DefaultWriting = isset($_POST['step5DefaultWriting'])?trim($_POST['step5DefaultWriting']):exit;
			$buildData[]= $objEngContentBuilder->buildStep5($step5Int,$step5DefaultWriting);
			break;
		case 6:
			### Step5 column
			
			$resourceName = $_POST['resource'];
			$resourceWebsite = $_POST['website'];
			
			$resourceCount = count($resourceName);
			for($i=0; $i<$resourceCount; ++$i)
			{
				$tempObj=null;
				if( trim($resourceName[$i])=='')
				{
					continue;
				}
				$tempObj['resourceName'] = trim($resourceName[$i]);
				$tempObj['resourceWebsite'] = trim($resourceWebsite[$i]);
				
				for($c=0; $c<5; ++$c)
				{
					if($_POST["r".($i+1)."_step".($c+1)])
					{
						$tempObj['checked'][]='c'.($c+1);
					}
				}
				$refObj[] = $tempObj;

			}
			$buildData[]= $objEngContentBuilder->buildResource($refObj);
			break;
		default:
			break;
	}
	
	if( empty($cid) )
	{
			$buildData[]= $objEngContentBuilder->buildDBInfoInsert($r_contentCode,$schemeNum);	
			$sql = 'INSERT INTO W2_CONTENT_DATA
			('
			.$buildData[0]['field'].','
			.$buildData[1]['field'].
			
			') VALUES
			('
			.$buildData[0]['content'].','
			.$buildData[1]['content'].
			');';
			

			if($saveTab==2){
				$buildData[2]['field'] = 'conceptType';
				$buildData[2]['content'] = "'$step2ConceptType'";
			
				$sql = 'INSERT INTO W2_CONTENT_DATA
				('
				.$buildData[0]['field'].','
				.$buildData[1]['field'].','
				.$buildData[2]['field'].
				
				') VALUES
				('
				.$buildData[0]['content'].','
				.$buildData[1]['content'].','
				.$buildData[2]['content'].
				');';
			}
	}
	else
	{
			if($saveTab==0)
			{
				$data = $buildData[0];
			}
			else
			{
				$data = $buildData[0]['field'].'='.$buildData[0]['content'];
			}
			$buildData[]= $objEngContentBuilder->buildDBInfoUpdate($r_contentCode,$schemeNum);
			
			if($saveTab==2){
				$buildData[2]['field'] = 'conceptType';
				$buildData[2]['content'] = "'$step2ConceptType'";
				$data = $buildData[0]['field'].'='.$buildData[0]['content'].','.
						$buildData[2]['field'].'='.$buildData[2]['content'];	
			}		
			$sql = 'UPDATE W2_CONTENT_DATA SET '.			
			$data.','.
			implode(",", (array)$buildData[1]).
			' WHERE cid=\''.$cid.'\';';
			
	}
	$rs = $objDB->db_db_query($sql);
	
	
	if($rs)
	{
		$cid = empty($cid)?$objDB->db_insert_id():$cid;
		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /home/eLearning/w2/index.php?mod=admin&task=newChiTemplateWriting&r_contentCode=$r_contentCode&cid=$cid&r_returnMsgKey=AddSuccess&tabNum=$saveTab");
	}
} 
catch (Exception $e) 
{
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}


#####################Helper function #####################################################
function isInjectionCodePresent($stringList)
{
	foreach($stringList as $string)
	{
		if (strpos($string, '<?php') !== false)
	    {
	    	return true;
	    }
	}
}

?>