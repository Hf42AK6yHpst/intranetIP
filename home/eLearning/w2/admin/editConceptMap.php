<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");

$objDB = new libdb();
$r_conceptMapId  = trim($r_conceptMapId);
$r_cid = trim($r_cid);
if(!empty($r_cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($r_cid);
	$r_contentCode = $parseData['r_contentCode'];
	$tabNum = $w2_cfg['contentInput']['mindMapStep'][$r_contentCode]-1;
}
$withoutConceptMap = false; //flag to store if with concept Map saved before
if(!empty($r_conceptMapId)){//if fail to find record, create a new one
	$cnt = _getConceptMapRecord($r_conceptMapId,$type);
	$r_conceptMapId = $cnt>0?$r_conceptMapId:'';
}
if($r_conceptMapId==''){
	$withoutConceptMap = true;
	$r_conceptMapId = _insertEmptyPowerConceptRecord($w2_thisUserID,$type);
}

		//Make sure with concept Map Id
		if(intval($r_conceptMapId)>0){
		
			if($withoutConceptMap){
				if(intval($r_cid)>0){
					$fieldname = $type==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?'powerBoardID':'powerConceptID';
					$sql = "update W2_CONTENT_DATA set ".$fieldname." = ".$r_conceptMapId.", conceptType='".$type."' where cid = ".$r_cid;
					$objDB->db_db_query($sql);
				}
			}else{
				$sql = "update W2_CONTENT_DATA set conceptType='".$type."' where cid = ".$r_cid;
				$objDB->db_db_query($sql);
			}
			
			switch($type){
				case $w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]:
			//	window.opener.location.reload(false);		
					$js_function = "/*reload parent page*/
							window.opener.location.href=window.opener.location.href+'&tabNum=".$tabNum."';
							/* Go to PowerConcept Tool */
							window.location.href = '/templates/powerconcept/index.php?PowerConceptID=".$r_conceptMapId."&r_comeFrom=w2';"; 
				break;
				case $w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]:
				
					$js_function = "
						var url = window.opener.location.href;
						if(url.indexOf('&tabNum=".$tabNum."')==-1)
							url +='&tabNum=".$tabNum."';
						window.opener.location.href=url;
					";
					$iframe_src = $eclass40_httppath."src/tool/powerboard5/pb5_w2.php?powerBoardID=".$r_conceptMapId;
				
				break;
			}
		
		}else{
			$js_function = '';
			$errorMessage = 'System cannot find a valid concept map Setting! Load input for concept map aborted!';
		}
function _getConceptMapRecord($r_conceptMapId,$type){
	global $w2_cfg;
	if($type==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]){
		$table = "TOOLS_POWERBOARD";
		$field = "PowerBoardID";
	}else{
		$table = "TOOLS_POWERCONCEPT";
		$field = "PowerConceptID";
	}
	$objDB = new libdb();
	$sql = "SELECT COUNT(*) FROM 
				".$table." 
			WHERE 
				".$field." = '".$r_conceptMapId."'";
				
	return current($objDB->returnVector($sql));
}

function _insertEmptyPowerConceptRecord($parUserID,$type){
		//global $sba_cfg;
		global $w2_cfg;
		if($type==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]){
			$table = "TOOLS_POWERBOARD";
		}else{
			$table = "TOOLS_POWERCONCEPT";
		}
		$objDB = new libdb();
		$sql = "INSERT INTO
					".$table."
					(UserID, ModuleCode, InputDate, ModifiedDate)
				VALUES
					($parUserID, '".$w2_cfg['moduleCode']."', NOW(), NOW())";
		$objDB->db_db_query($sql);
		return $objDB->db_insert_id();
		
	}

?>

<script><?=$js_function?></script>
<?=$errorMessage?>

<?php if($type==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]):?>
<iframe src="http://<?=$iframe_src?>" width="100%" height="100%" frameborder="0" marginheight="0" marginwidth="0"></iframe>
<?php endif;?>
