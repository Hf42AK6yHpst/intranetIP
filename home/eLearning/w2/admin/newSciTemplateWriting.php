<?
// using:

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2RefManager.php");

$r_contentCode = trim($_GET['r_contentCode']);
$cid = trim($_GET['cid']);
$tabNum = isset($_GET['tabNum'])?$_GET['tabNum']:-1;
$r_returnMsgKey = $_GET['r_returnMsgKey'];
$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
### Page title
$h_title = strtoupper($cid ==''?$Lang['W2']['newTemplate']:$Lang['W2']['editTemplate']);	

### Get template information from database
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	$conceptType = $parseData['conceptType'];
	$powerConceptID = $parseData['powerConceptID'];
	$powerBoardID = $parseData['powerBoardID'];
	$attachment = $parseData['attachment'];
	$grade = $parseData['grade'];
	$teacherAttachmentData = $parseData['teacherAttachmentData'];
	$powerConceptCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]?'checked':'';
	$powerBoardCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?'checked':'';
	$attachmentCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]?'checked':'';		
}
$h_gradeSelection = $w2_libW2->getGradeSelection($grade);

$objDB = new libdb();

### Topic & Theme - TextType
$textTypeSelectionBoxItems = getSelectionItems("category","category","SciThemeAry",$parseData['category']);

### Step 1 Purpose
$h_selectStep1PurposeHTML =  getSelectionItems("step1Purpose","step1Purpose","PurposeAry",$parseData['step1Data']['step1Purpose']);

### Step 2 Reference Materials
$h_step2ArticleHTML = getArticleGenerated($parseData['step2Data']['step2ArticleAry'],2,$parseData['status']);

### Step 3 Highlight
$stepArticleAry = array();

$aCnt = count($parseData['step2Data']['step2ArticleAry']);
for($i=0;$i<$aCnt;$i++){
	$_infoboxCode = 'infobox_'.$cid.'_'.$i;
	$_paragraphAry = $parseData['step2Data']['step2ArticleAry']['articleItem'.$i];
	$_article = '';
	foreach((array)$_paragraphAry as $__item){
		$_article .= str_replace('&nbsp;',' ',$__item['paragraph']).'<br/>';
	}
	$stepArticleAry[] = array('infoboxCode'=>$_infoboxCode,'article'=>$_article);
}
$h_step3highlightArticleHTML = getHighlightArticleGenerated($stepArticleAry,$parseData['step3Data']['step3RefItemAry'],$parseData['status']);

### Step 4 Writing Sample
$h_step4ParagraphHTML = '';
$paragraphAry = $parseData['step4Data']['step4ParagraphAry'];
$paragraphCnt = count($paragraphAry);
$paragraphCnt = $paragraphCnt>0?$paragraphCnt:$w2_cfg['contentInput']['SampleWritingDefaultParagraphNum'];
for($i=1;$i<=$paragraphCnt;$i++){
	$_thisParagraphAry = $paragraphAry['paragraphItem'.$i];
	$action = count($_thisParagraphAry)>0?'EDIT':'NEW';
	$h_step4ParagraphHTML .= $w2_libW2->getParagraphBubbleHTML($i,$_thisParagraphAry,$action,4);
}
$h_step4ParagraphHTML .= '<input type ="hidden" name="r_step4ParagraphCounter" id="r_step4ParagraphCounter" value="'.$paragraphCnt.'">';
###Step 4 Learning Tips (Remarks)
$h_step4RemarkHTML = $w2_libW2->getStep3RemarkHTML($parseData['step4Data']['step4RemarkAry'],4);
### Category selection box items

### Step 5 Outline
$h_step5OutlineHTML = getStepQuestionArrayGenerated('step5Outline',$parseData['step5Data']['step5OutlineAry'],$parseData['status']);

### Resource case
$resourceCounter = 1;
$getResourceArrayHTML =  getResrouceTableItemsGenerated ($parseData['resourceData'], $resourceCounter);


### FCK editor objects
$FCKEditor = new FCKeditor ( 'step1Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step1Data']['step1Int'];
$step1InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step2Int' , "100%", "180", "", "", "");
$FCKEditor->Value = intranet_undo_htmlspecialchars($parseData['step2Data']['step2Int']);
$step2InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step3Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step3Data']['step3Int'];
$step3InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step4Int' , "100%", "180", "", "", "");
$FCKEditor->Value = intranet_undo_htmlspecialchars($parseData['step4Data']['step4Int']);
$step4InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step5Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step5Data']['step5Int'];
$step5InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step6Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step6Data']['step6Int'];
$step6InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step6DefaultWriting' , "100%", "300", "", "", "");
$FCKEditor->Value = !empty($parseData['step6Data']['step6DefaultWriting'])?$parseData['step6Data']['step6DefaultWriting']:"";
$step6DefaultWritingEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

### Set level
$isBasicSelected = $parseData['level']=='Basic'?'checked':'';
$isAdvancedSelected = $parseData['level']=='Advanced'? 'checked':'';

### button panel
$h_cancelButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'doCancel()', 'formsubbutton', ' onmouseover="this.className=\'formsubbuttonon\'" onmouseout="this.className=\'formsubbutton\'" ',0,'formsubbutton');
$h_saveButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', 'doFormCheck(\'-1\');', 'formbutton', 'onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'"',0,'formbutton');
$h_addItemButton = "<a href='#' onclick='addDraftField();return false;' id='addItemBtn' class='new'>Add more</a>";

$h_step2AddMoreButton = "<a href='javascript:void(0);' onclick='addStepArticle(2);return false;' id='step2ArticleAddMore' class='new'>".$Lang['W2']['addMoreQuestion']."</a>";
$h_step5AddMoreButton = "<a href='javascript:void(0);' onclick='addStepQuestionField(\"step5Outline\",false);return false;' id='step5OutlineAddMore' class='new'>".$Lang['W2']['addMoreQuestion']."</a>";


### Form checking warning messages
// divId = {inputId}_warningDiv
$h_inputTopicNameWarning = $linterface->Get_Form_Warning_Msg('r_TopicName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_inputTopicIntroWarning = $linterface->Get_Form_Warning_Msg('r_TopicIntro_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_selectCategoryWarning = $linterface->Get_Form_Warning_Msg('r_SelectedCategory_warningDiv', $Lang['W2']['warningMsgArr']['selectCat'], 'warningDiv_0');
$h_selectLevelWarning = $linterface->Get_Form_Warning_Msg('r_SelectedLevel_warningDiv', $Lang['W2']['warningMsgArr']['selectLevel'], 'warningDiv_0');

$h_inputStep1IntWarning = $linterface->Get_Form_Warning_Msg('r_Step1Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_selectStep1PurposeWarning = $linterface->Get_Form_Warning_Msg('r_Step1Purpose_warningDiv', $Lang['W2']['warningMsgArr']['selectPurpose'], 'warningDiv_1');
$h_inputStep1AudienceWarning = $linterface->Get_Form_Warning_Msg('r_Step1AudienceWarning_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_inputStep1TeacherVocabWarning = $linterface->Get_Form_Warning_Msg('r_Step1TeacherVocabWarning_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');

$h_inputStep2IntWarning = $linterface->Get_Form_Warning_Msg('r_Step2Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');
$h_inputStep2SourceWarning = $linterface->Get_Form_Warning_Msg('r_Step2Source_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');
$h_inputStep2SourceImageWarning = $linterface->Get_Form_Warning_Msg('r_Step2SourceImage_warningDiv', $Lang['W2']['jsWarningAry']['wrongImageFormat'], 'warningDiv_2');

$h_inputStep3IntWarning = $linterface->Get_Form_Warning_Msg('r_Step3Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');

$h_inputStep5IntWarning = $linterface->Get_Form_Warning_Msg('r_Step5Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');
$h_inputStep5OutlineWarning = $linterface->Get_Form_Warning_Msg('r_Step5Outline_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');

$h_inputStep6IntWarning = $linterface->Get_Form_Warning_Msg('r_Step6Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_6');
$h_inputStep6DefaultWritingWarning = $linterface->Get_Form_Warning_Msg('r_Step6DefaultWriting_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_6');

$h_inputStep7WebNameWarning = $linterface->Get_Form_Warning_Msg('r_Step7WebName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_7');
$h_inputStep7WebsiteWarning = $linterface->Get_Form_Warning_Msg('r_Step7Website_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_7');

$h_submissionInvalidWarning = $linterface->Get_Form_Warning_Msg('submissionInvalidWarnMsg', $Lang['General']['JS_warning']['InvalidDateRange'], 'warningDiv');
$h_mustUpdatePeerMarkingSettingsWarning = $linterface->Get_Form_Warning_Msg('mustUpdatePeerMarkingSettingsWarnDiv', $Lang['W2']['warningMsgArr']['mustUpdatePeerMarkingSettings'], 'warningDiv');


### content
ob_start();
include('templates/'.$mod.'/'.$task.'.tmpl.php');
$html_content = ob_get_contents();
ob_end_clean();

//Logic function helper layer
#####################################################################################################
function getResrouceTableItemsGenerated ($resourceList, &$resourceCounter)
{
	$resourceHTML ='';
	$resourceList = $resourceList['resource'];

	foreach( (array) $resourceList as $resourceItem)
	{
		$_website = !empty($resourceItem['resourceItemWebsite'])?$resourceItem['resourceItemWebsite']:'';
		$_itemName = !empty($resourceItem['resourceItemName'])?$resourceItem['resourceItemName']:'';
		$resourceHTML .= "<tr class='resourceArray'>"."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<input type='text' style='width:95%' name='resource[]' class='resource' id='resource_{$resourceCounter}' value='".$_itemName."' data-resourceID = '{$resourceCounter}'/>"."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<input type='text' style='width:100%' name='website[]' id='website_{$resourceCounter}' class='webiste' value='".$_website."' data-resourceID = '{$resourceCounter}' />"."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<span class='table_row_tool'><a href='#' class='deleteResourceBtn tool_delete_dim' title='Delete'>&nbsp;</a></span>"."\n\r";
		$resourceHTML .= '</td>'."\n\r";

		$resourceHTML .= '<td>'."\n\r";
		for ($i=0; $i<6; ++$i)
		{
			
			$isChecked = '';
			if ( $resourceItem["resourceItemChecked"]['c'.($i+1)] )
			{
				$isChecked = 'checked';
			}
			$resourceHTML .= ('<label for="step'.($i+1).'">'.($i+1).'</label><input type="checkbox" id="step'.($i+1).'" name="r'.$resourceCounter.'_step'.($i+1).'" class="step" '.$isChecked.' />&nbsp;'."\n\r" );
		}
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '</tr>'."\n\r";
		++$resourceCounter;
	}
	return $resourceHTML;
}

function getSelectionItems($name, $id, $ArrayName, $defaultValue='')
{
	global $Lang;
	$defaultKey = array_search(intranet_undo_htmlspecialchars($defaultValue),$Lang['W2'][$ArrayName]);
	$returnStr='';
	$returnStr .= '<select id="'.$id.'" name="'.$name.'" >';
	$returnStr .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
	foreach ( (array)$Lang['W2'][$ArrayName] as $_key => $_value){
		$isSelectedHTML = ("$defaultKey" == "$_key")?'selected':'';
		$returnStr .= '<option value="'.$_key.'" '.$isSelectedHTML.' >'.$_value.'</option>';
	}
	$returnStr .= '</select>';
	return $returnStr;
}

function getArticleGenerated($stepArticleAry,$step,$status){
	global $Lang,$w2_cfg,$cid,$intranet_root,$PATH_WRT_ROOT,$LAYOUT_SKIN,$w2_libW2;
	$html = '';

	$qCnt = count($stepArticleAry)>0?count($stepArticleAry):1;
	for($i=0;$i<$qCnt;$i++){
		$_step = $step.'_'.$i;
		$h_stepParagraphHTML = '';
		$paragraphAry = $stepArticleAry['articleItem'.$i];
		$paragraphCnt = count($paragraphAry);
		$paragraphCnt = $paragraphCnt>0?$paragraphCnt:$w2_cfg['contentInput']['SampleWritingDefaultParagraphNum'];
		for($j=1;$j<=$paragraphCnt;$j++){
			
			$_thisParagraphAry = $paragraphAry['paragraphItem'.$j];
			$action = count($_thisParagraphAry)>0?'EDIT':'NEW';
			$h_stepParagraphHTML .= $w2_libW2->getParagraphBubbleHTML($j,$_thisParagraphAry,$action,$_step,false);
		}
		$h_stepParagraphHTML .= '<input type ="hidden" name="r_step'.$_step.'ParagraphCounter" id="r_step'.$_step.'ParagraphCounter" value="'.$paragraphCnt.'">';
		$html .= '<div>';
		$html .='<div id="div_step'.$_step.'Article[]" class="infobox" style="width:80%;line-height: 20px;">';
			$html .= '<a class="top" href="javascript:void(0);">'.$Lang['W2']['article'].' '.($i+1).'</a>';
			$html .= '<div class="infobox_content">';
				$html .= '<div class="content_input_sample" style="padding-right:0px;">';
		           $html .= '<div class="content_input_sample_left">';
		           	$html .= '<div class="step'.$_step.'WritingSample content_input_main_grp" style="margin-bottom:0">';
			        	$html .= '<div class="content_input_main_tag">';
			            	$html .= '<h1>'.$Lang['W2']['structure'].'</h1>';
			                	$html .= '</div>';
			                    $html .= '<div class="content_input_main">';
			                    	$html .= '<h1>'.$Lang['W2']['paragraph'].'</h1>';
			                    $html .= '</div>';
		                      	$html .= '<br style="clear:both" />';
		                    $html .= '</div>';
		                    $html .= '<div id="Step'.$_step.'_Paragraph_Div">'.$h_stepParagraphHTML.'</div>';
		                    $html .= '<div class="content_input_main_grp">';
		                    	$html .= '<div class="content_input_main_tag"></div>';
		                        $html .= '<div class="content_input_main Content_tool_190"><a href="javascript:void(0);" class="new" onclick="addNewParagraph(\''.$_step.'\');">'.$Lang['W2']['newParagraph'].'</a></div>';
		                      	$html .= '<br style="clear:both" />';
		                    $html .= '</div>';
		                $html .= '</div>';
				
       			$html .='</div>';
			$html .='</div>';
		$html .='</div>';
		$html .='<span class="table_row_tool"><a href="javascript:void(0);" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="'.$Lang['Btn']['Delete'].'">&nbsp;</a></span>';
		$html .= '</div>';
	}
	$html .= '<input type="hidden" name="Cnt_step'.$step.'Article" id="Cnt_step'.$step.'Article" value="'.$qCnt.'">';
	return ( empty($html)==true)?'':$html;	
}
function getHighlightArticleGenerated($stepArticleAry,$step3RefItemAry,$status){
	global $Lang,$w2_cfg;
	$html = '';
	$aCnt = count($stepArticleAry);

	for($i=0;$i<$aCnt;$i++){
		$_infoboxCode = $stepArticleAry[$i]['infoboxCode'];
		
		$html .= '<div class="infobox" style="width:70%;line-height: 20px;">';
			$html .= '<a class="top" href="javascript:void(0);">'.$Lang['W2']['article'].' '.($i+1).'</a>';
			$html .= '<div class="infobox_content w2_infoxboxContentDiv">';
				$html .= '<div class="sample_sci" style="padding-left:0px;">';
					$html .= '<div id="w2_infoboxEssayDiv_'.$_infoboxCode.'" class="essay">';
					$html .= $stepArticleAry[$i]['article'];	
					$html .= '</div>';
					$html .= '<div class="key_sci">';
						$objW2RefManager = new libW2RefManager();
						$objW2RefManager->setInfoboxCode($_infoboxCode);
						$objW2RefManager->setIsEditable(true);
						$objW2RefManager->setDisplayMode($w2_cfg["refDisplayMode"]["input"]);
						$html .= $objW2RefManager->display();
		      		$html .= '</div>';
				$html .= '</div>';	
			$html .= '</div>';	
		$html .= '</div>';	
	}
	return $html;
}
function getStepQuestionArrayGenerated($name,$stepQuestion,$status,$withAnswer=false)
{
	global $Lang,$w2_cfg;
	$draftQHTML=''; 
	$qCnt = (count($stepQuestion)>0)?count($stepQuestion):$w2_cfg['contentInput']['defaultInputNum'];
	for($i=0;$i<$qCnt;$i++){
		$_question = $withAnswer?$stepQuestion['item'.$i]['question']:$stepQuestion['question'.$i]; 
		$_answer = $withAnswer?$stepQuestion['item'.$i]['answer']:'';
		$draftQHTML .='<div id="div_'.$name.'[]">';
			$draftQHTML .='<span style="float:left;width:80px;">'.$Lang['W2']['question'].': </span> <textarea name="'.$name.'Question[]" rows="3" class="'.$name.' textbox" style="width:70%; float:left">'.(!empty($_question)?stripslashes($_question):'').'</textarea>';
			$draftQHTML .='<span class="table_row_tool"><a href="javascript:void(0);" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="'.$Lang['Btn']['Delete'].'">&nbsp;</a></span>';
			$draftQHTML .='<br style="clear:both;">';
		if($withAnswer){
			$draftQHTML .='<span style="float:left;width:80px;">'.$Lang['W2']['answer'].': </span> <textarea name="'.$name.'Answer[]" rows="3" class="'.$name.' textbox" style="width:70%; float:left">'.(!empty($_answer)?stripslashes($_answer):'').'</textarea>';
			$draftQHTML .='<br style="clear:both;">';
			$draftQHTML .='<br style="clear:both;">';
		}
		$draftQHTML .='</div>';
	}
	$draftQHTML .= '<input type="hidden" name="Cnt_'.$name.'" id="Cnt_'.$name.'" value="'.$qCnt.'">';
	return ( empty($draftQHTML)==true)?'':$draftQHTML;
}
function _frameLayout($title , $content,$bottom){
	global $Lang;
	$html = '<form action ="/home/eLearning/w2/index.php" method = "post" id="w2_form" name="w2_form" enctype="multipart/form-data">';
	$html .= '<div class="main_top_blank"> <!-- for blank div 20140113 -->';
	$html .= '</div>';
	$html .= '<p class="spacer"></p>';   
	$html .= '<div class="themename_grp"> <!-- New themename group 20140113 -->';
		$html .= '<span class="themename_icon"></span>';
		$html .= '<span class="themename_main">'.$Lang['W2']['topic'].': <strong id="topicHeader" data-originalText="('.$Lang['W2']['topicName'].')">('.$Lang['W2']['topicName'].')</strong></span>';
	$html .= '</div>';
	$html .= '<br />';
	$html .= '<div class="write_board write_board_input">';
		//$html .= '<div class="content_input">';
			$html .= $content;
			$html .= '<div class="write_board_bottom_left">';
				$html .= '<div class="write_board_bottom_right"></div>';
			$html .= '</div>';
		//$html .= '</div>';
	$html .= '</div>';
	$html .= '</form>	';		


	return $html;
}

function escapeJavaScriptText($string)
{
    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
}

//Presentation layer


#####################################################################################################
$str =<<<HTML
HTML;
$linterface->LAYOUT_START($returnMsg);

echo _frameLayout($h_title,$html_content,'');
$linterface->LAYOUT_STOP();

exit();
?>