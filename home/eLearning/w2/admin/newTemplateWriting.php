<?
// using:

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");


$r_contentCode = trim($_GET['r_contentCode']);
$cid = trim($_GET['cid']);
$tabNum = isset($_GET['tabNum'])?$_GET['tabNum']:-1;
$r_returnMsgKey = $_GET['r_returnMsgKey'];
$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];

### Page title
$h_title = strtoupper($cid ==''?$Lang['W2']['newTemplate']:$Lang['W2']['editTemplate']);	

### Get template information from database
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	$conceptType = $parseData['conceptType'];
	$powerConceptID = $parseData['powerConceptID'];
	$powerBoardID = $parseData['powerBoardID'];
	$attachment = $parseData['attachment'];
	$grade = $parseData['grade'];
	$powerConceptCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]?'checked':'';
	$powerBoardCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?'checked':'';
	$attachmentCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]?'checked':'';		
}
$h_gradeSelection = $w2_libW2->getGradeSelection($grade);

$objDB = new libdb();

### Step 4 case
$getStep4QArrayHTML = getStep4QuestionArrayGenerated($parseData['step4Data']['step4DraftQList'],$parseData['status']);
$deleteButtonForStep4 = '<span class="table_row_tool" ><a href="#" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="Delete">&nbsp;</a></span>';

$h_draftQHTML .='<div class="step4DraftQColumns[]">';
$h_draftQHTML .='<input name="step4DraftQ[]" class="step4DraftQ" type="text" size="80" required />';
$h_draftQHTML .=($parseData['status']==W2_GENERATED)?$deleteButtonForStep4:'';
$h_draftQHTML .='</div>';

### Resource case
$resourceCounter = 1;
$getResourceArrayHTML =  getResrouceTableItemsGenerated ($parseData['resourceData'], $resourceCounter);


### FCK editor objects
$FCKEditor = new FCKeditor ( 'step1Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step1Data']['step1Int'];
$step1InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step2Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step2Data']['step2Int'];
$step2InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step3Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step3Data']['step3Int'];
$step3InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

//$FCKEditor = new FCKeditor ( 'step3Sample' , "100%", "300", "", "", "");
//$FCKEditor->Value = $parseData['step3Data']['step3SampleWriting'];
//$step3SampleWritingEditor =escapeJavaScriptText( trim( $FCKEditor->CreateHtml() ) );

//$FCKEditor = new FCKeditor ( 'step3Vocab' , "100%", "180", "", "", "");
//$FCKEditor->Value = $parseData['step3Data']['step3Vocab'];
//$step3VocabEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );
//
//$FCKEditor = new FCKeditor ( 'step3Grammar' , "100%", "180", "", "", "");
//$FCKEditor->Value = $parseData['step3Data']['step3Grammar'];
//$step3GrammarEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step4Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step4Data']['step4Int'];
$step4InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step5Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step5Data']['step5Int'];
$step5InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step5DefaultWriting' , "100%", "300", "", "", "");
$FCKEditor->Value = !empty($parseData['step5Data']['step5DefaultWriting'])?$parseData['step5Data']['step5DefaultWriting']:"";
$step5DefaultWritingEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

###Step 3 Paragraph Bubble
$h_step3ParagraphHTML = '';
if(!empty($parseData['step3Data']['step3SampleWriting'])){
	$paragraphAry['paragraphItem1']['paragraph'] = $parseData['step3Data']['step3SampleWriting'];
}else{
	$paragraphAry = $parseData['step3Data']['step3ParagraphAry'];
}

$paragraphCnt = count($paragraphAry);
$paragraphCnt = $paragraphCnt>0?$paragraphCnt:$w2_cfg['contentInput']['SampleWritingDefaultParagraphNum'];
for($i=1;$i<=$paragraphCnt;$i++){
	$_thisParagraphAry = $paragraphAry['paragraphItem'.$i];
	$action = count($_thisParagraphAry)>0?'EDIT':'NEW';
	$h_step3ParagraphHTML .= $w2_libW2->getParagraphBubbleHTML($i,$_thisParagraphAry,$action);
}
$h_step3ParagraphHTML .= '<input type ="hidden" name="paragraphCounter" id="paragraphCounter" value="'.$paragraphCnt.'">';
###Step 3 Learning Tips (Remarks)
$h_step3RemarkHTML = $w2_libW2->getStep3RemarkHTML($parseData['step3Data']['step3RemarkAry']);
### Category selection box items
$textTypeSelectionBoxItems = getCategoryItems("step1TextType","step1TextType",$parseData['step1Data']['step1TextType']);

### Set level
$isBasicSelected = $parseData['level']=='Basic'?'checked':'';
$isAdvancedSelected = $parseData['level']=='Advanced'? 'checked':'';

### button panel
$h_cancelButton = $linterface->GET_ACTION_BTN('Cancel', 'button', 'doCancel()', 'formsubbutton', ' onmouseover="this.className=\'formsubbuttonon\'" onmouseout="this.className=\'formsubbutton\'" ',0,'formsubbutton');
$h_saveButton = $linterface->GET_ACTION_BTN('Save', 'button', 'doFormCheck(\'-1\');', 'formbutton', 'onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'"',0,'formbutton');
$h_addItemButton = "<a href='#' onclick='addDraftField();return false;' id='addItemBtn' class='new'>Add more</a>";

### Form checking warning messages
// divId = {inputId}_warningDiv
$h_inputTopicNameWarning = $linterface->Get_Form_Warning_Msg('r_TopicName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_inputTopicIntroWarning = $linterface->Get_Form_Warning_Msg('r_TopicIntro_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_selectCategoryWarning = $linterface->Get_Form_Warning_Msg('r_SelectedCategory_warningDiv', $Lang['W2']['warningMsgArr']['selectCat'], 'warningDiv_0');
$h_selectLevelWarning = $linterface->Get_Form_Warning_Msg('r_SelectedLevel_warningDiv', $Lang['W2']['warningMsgArr']['selectLevel'], 'warningDiv_0');

$h_inputStep1IntWarning = $linterface->Get_Form_Warning_Msg('r_Step1Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_selectStep1TextTypeWarning = $linterface->Get_Form_Warning_Msg('r_Step1TextType_warningDiv', $Lang['W2']['warningMsgArr']['selectTextType'], 'warningDiv_1');
$h_inputStep1PurposeWarning = $linterface->Get_Form_Warning_Msg('r_Step1Purpose_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_inputStep1ContentWarning = $linterface->Get_Form_Warning_Msg('r_Step1Content_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');

$h_inputStep2IntWarning = $linterface->Get_Form_Warning_Msg('r_Step2Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');

$h_inputStep3IntWarning = $linterface->Get_Form_Warning_Msg('r_Step3Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
$h_inputStep3SampleWritingWarning = $linterface->Get_Form_Warning_Msg('r_Step3SampleWriting_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
$h_inputStep3VocabWarning = $linterface->Get_Form_Warning_Msg('r_Step3Vocab_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
$h_inputStep3GrammarWarning = $linterface->Get_Form_Warning_Msg('r_Step3Grammar_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');

$h_inputStep4IntWarning = $linterface->Get_Form_Warning_Msg('r_Step4Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');
$h_inputStep4DraftQsWarning = $linterface->Get_Form_Warning_Msg('r_Step4DraftQs_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');

$h_inputStep5IntWarning = $linterface->Get_Form_Warning_Msg('r_Step5Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');
$h_inputStep5DefaultWritingWarning = $linterface->Get_Form_Warning_Msg('r_Step5DefaultWriting_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');


$h_submissionInvalidWarning = $linterface->Get_Form_Warning_Msg('submissionInvalidWarnMsg', $Lang['General']['JS_warning']['InvalidDateRange'], 'warningDiv');
$h_mustUpdatePeerMarkingSettingsWarning = $linterface->Get_Form_Warning_Msg('mustUpdatePeerMarkingSettingsWarnDiv', $Lang['W2']['warningMsgArr']['mustUpdatePeerMarkingSettings'], 'warningDiv');


### content
ob_start();
include('templates/admin/newTemplateWriting.tmpl.php');
$html_content = ob_get_contents();
ob_end_clean();

//Logic function helper layer
#####################################################################################################
function getResrouceTableItemsGenerated ($resourceList, &$resourceCounter)
{
	$resourceHTML ='';
	$resourceList = $resourceList['resource'];
	

	foreach( (array) $resourceList as $resourceItem)
	{
		$_website = !empty($resourceItem['resourceItemWebsite'])?$resourceItem['resourceItemWebsite']:'';
		$_itemName = !empty($resourceItem['resourceItemName'])?$resourceItem['resourceItemName']:'';
		$resourceHTML .= "<tr class='resourceArray'>"."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<input type='text' style='width:95%' name='resource[]' class='resource' id='resource_{$resourceCounter}' value='".$_itemName."' data-resourceID = '{$resourceCounter}'/>"."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<input type='text' style='width:100%' name='website[]' id='website_{$resourceCounter}' class='webiste' value='".$_website."' data-resourceID = '{$resourceCounter}' />"."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<span class='table_row_tool'><a href='#' class='deletResourceBtn tool_delete_dim' title='Delete'>&nbsp;</a></span>"."\n\r";
		$resourceHTML .= '</td>'."\n\r";

		$resourceHTML .= '<td>'."\n\r";
		for ($i=0; $i<5; ++$i)
		{
			
			$isChecked = '';
			if ( $resourceItem["resourceItemChecked"]['c'.($i+1)] )
			{
				$isChecked = 'checked';
			}
			$resourceHTML .= ('<label for="step'.($i+1).'">'.($i+1).'</label><input type="checkbox" id="step'.($i+1).'" name="r'.$resourceCounter.'_step'.($i+1).'" class="step" '.$isChecked.' />&nbsp;'."\n\r" );
		}
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '</tr>'."\n\r";
		++$resourceCounter;
	}
	return $resourceHTML;
}

function getCategoryItems($name, $id,$defaultValue='')
{
	global $w2_cfg_contentSetting;
	
	$returnStr='';
	foreach ( (array)$w2_cfg_contentSetting['eng'] as $item)
	{
		$typeList[] = $item['type'];	
	}
	$uniqueTypeList = array_unique($typeList);
	asort($uniqueTypeList);
	foreach ( (array)$uniqueTypeList as $type)
	{
		$isSelectedHTML = ("$defaultValue" == "$type")?'selected':'';
		$returnStr .= '<option value="'.$type.'" '.$isSelectedHTML.' >'.$type.'</option>';
	}
	return $returnStr;
}

function getStep4QuestionArrayGenerated($parseData,$status)
{
	
	$draftQHTML='';
	foreach( (array)$parseData as $draftQ)
	{
		$draftQHTML .='<div class="step4DraftQColumns[]">';
		$draftQHTML .='<textarea name="step4DraftQ[]" rows="2" class="step4DraftQ textbox" style="width:70%; float:left">'.$draftQ.'</textarea>';
		$draftQHTML .=($status==W2_GENERATED)?'':'<span class="table_row_tool"><a href="#" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="Delete">&nbsp;</a></span>';
		$draftQHTML .='</div>';
	}
	return ( empty($draftQHTML)==true)?'':$draftQHTML;
}
function _frameLayout($title , $content,$bottom){
	global $Lang;
	$html = '<form action ="/home/eLearning/w2/index.php" method = "post" id="w2_form" name="w2_form" enctype="multipart/form-data">';
	$html .= '<div class="main_top_blank"> <!-- for blank div 20140113 -->';
	$html .= '</div>';
	$html .= '<p class="spacer"></p>';   
	$html .= '<div class="themename_grp"> <!-- New themename group 20140113 -->';
		$html .= '<span class="themename_icon"></span>';
		$html .= '<span class="themename_main">'.$Lang['W2']['topic'].': <strong id="topicHeader" data-originalText="('.$Lang['W2']['topicName'].')">('.$Lang['W2']['topicName'].')</strong></span>';
	$html .= '</div>';
	$html .= '<br />';
	$html .= '<div class="write_board write_board_input">';
		//$html .= '<div class="content_input">';
			$html .= $content;
			$html .= '<div class="write_board_bottom_left">';
				$html .= '<div class="write_board_bottom_right"></div>';
			$html .= '</div>';
		//$html .= '</div>';
	$html .= '</div>';
	$html .= '</form>	';		


	return $html;
}

function escapeJavaScriptText($string)
{
    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
}

//Presentation layer


#####################################################################################################
$str =<<<HTML
HTML;
$linterface->LAYOUT_START($returnMsg);

echo _frameLayout($h_title,$html_content,'');
$linterface->LAYOUT_STOP();

exit();
?>