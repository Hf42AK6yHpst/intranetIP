<?
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");

//$objWriting = new libWriting($r_writingId);
$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);

$success = $objWriting->delete($w2_thisUserID);
$returnMsgKey = ($success)? 'DeleteSuccess' : 'DeleteUnsuccess';

header("Location: /home/eLearning/w2/?task=writingMgmt&mod=writing&r_contentCode=".$r_contentCode."&r_returnMsgKey=".$returnMsgKey);
?>