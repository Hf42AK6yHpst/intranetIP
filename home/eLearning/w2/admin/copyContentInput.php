<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
$objDB = new libdb();

$W2EngContentParser = new libW2EngContentParser();
$parseData = $W2EngContentParser->parseEngContent($cid);

$sql = "SELECT MAX(schemeNum) FROM W2_CONTENT_DATA WHERE r_contentCode = '".$parseData['r_contentCode']."'";
$schemeNum = current($objDB->returnVector($sql))+1;
$cid = $_GET['cid'];
$sql = "SHOW COLUMNS FROM W2_CONTENT_DATA";
$fieldNameAry = $objDB->returnArray($sql);
$inputFieldName = "";
$inputFieldValue = "";
for($i=1;$i<count($fieldNameAry);$i++){ // 0 - cid
	$inputFieldName .= $i>1?",":"";
	$inputFieldName .= $fieldNameAry[$i]["Field"];	
	
	$inputFieldValue .= $i>1?",":"";
	switch($fieldNameAry[$i]["Field"]){
		case "topicName":
			$inputFieldValue .= "'".$parseData["topicName"]." (".$Lang['W2']['copy'].")'";				
		break;
		case "schemeNum":
			$inputFieldValue .= $schemeNum;	
		break;
		case "status":
			$inputFieldValue .= 0;	//draft
		break;		
		case "dateInput":
		case "dateModified":
			$inputFieldValue .= "NOW()";	
		break;	
		case "createdBy":
		case "modifiedBy":		
			$inputFieldValue .= $w2_thisUserID;	
		break;	
		case "dateGenerated":	
		case "datePublished":	
		case "deletedby":	
		case "dateDeleted":				
			$inputFieldValue .= "NULL";	
		break;	
		case "powerConceptID":				
			$inputFieldValue .= "NULL";	
		break;		
		case "powerBoardID":				
			$inputFieldValue .= "NULL";	
		break;	
		case "attachment":				
			$inputFieldValue .= "NULL";	
		break;					
		default:
			$inputFieldValue .= $fieldNameAry[$i]["Field"];				
	}
	
}
$sql = "INSERT INTO W2_CONTENT_DATA (".$inputFieldName.") SELECT ".$inputFieldValue." FROM W2_CONTENT_DATA WHERE cid = '".$cid."'";

$objDB->db_db_query($sql);
$cur_cid = $objDB->db_insert_id();
if($cur_cid>0){
	##Concept Map
	$cur_pid = "";
	$conceptMapIdField = $parseData['conceptType']=="powerconcept"?"PowerConceptID":"PowerBoardID";
	$conceptMapId = $parseData['conceptType']=="powerconcept"?$parseData['powerConceptID']:$parseData['powerBoardID'];
	if($parseData['conceptType']=="powerconcept"){
		$sql = "INSERT INTO TOOLS_POWERCONCEPT (DataContent,UserID,ModuleCode,InputDate,ModifiedDate) ";
		$sql .= "SELECT DataContent,'".$w2_thisUserID."',ModuleCode,NOW(),NOW() FROM TOOLS_POWERCONCEPT WHERE PowerConceptID = '".$conceptMapId."'";
		$objDB->db_db_query($sql);
		$cur_pid = $objDB->db_insert_id();		
	}else if($parseData['conceptType']=="powerboard"){
		$sql = "INSERT INTO TOOLS_POWERBOARD (DataContent,UserID,ModuleCode,ImageData,InputDate,ModifiedDate) ";
		$sql .= "SELECT DataContent,'".$w2_thisUserID."',ModuleCode,ImageData,NOW(),NOW() FROM TOOLS_POWERBOARD WHERE PowerBoardID = '".$conceptMapId."'";
		$objDB->db_db_query($sql);
		$cur_pid = $objDB->db_insert_id();		
	}
	
	$sql = "UPDATE W2_CONTENT_DATA SET ".$conceptMapIdField."='".$cur_pid."' WHERE cid = '".$cur_cid."'";
	$objDB->db_db_query($sql);
	
	##Reference Data
	$fromDir = $intranet_root.'/file/w2/reference/cid_'.$cid;
	$toDir = $intranet_root.'/file/w2/reference/cid_'.$cur_cid;
	if(is_dir($fromDir)){
		folder_copy($fromDir,$toDir);
	}
	$fromDir = $intranet_root.'/file/w2/teacher_attachment/cid_'.$cid;
	$toDir = $intranet_root.'/file/w2/teacher_attachment/cid_'.$cur_cid;
	if(is_dir($fromDir)){
		folder_copy($fromDir,$toDir);
	}	
	##Ref Item
	$sql = "INSERT INTO W2_CONTENT_REF_CATEGORY (cid,REF_CATEGORY_CODE,INFOBOX_CODE,TITLE,CSS_SET,DATE_INPUT,INPUT_BY,DATE_MODIFIED,MODIFIED_BY,DELETE_STATUS,DELETED_BY) ";
	$sql .= "SELECT '".$cur_cid."',REF_CATEGORY_CODE,REPLACE(INFOBOX_CODE,'".$cid."','".$cur_cid."'),TITLE,CSS_SET,NOW(),'".$w2_thisUserID."',NOW(),'".$w2_thisUserID."',DELETE_STATUS,NULL FROM W2_CONTENT_REF_CATEGORY ";
	$sql .= "WHERE cid = '".$cid."' AND DELETE_STATUS = '".$w2_cfg["DB_W2_REF_ITEM_DELETE_STATUS"]["active"]."'";
	$objDB->db_db_query($sql);
	
	$sql = "INSERT INTO W2_CONTENT_REF_ITEM (cid,REF_CATEGORY_CODE,INFOBOX_CODE,TITLE,DATE_INPUT,INPUT_BY,DATE_MODIFIED,MODIFIED_BY,DELETE_STATUS,DELETED_BY) ";
	$sql .= "SELECT '".$cur_cid."',REF_CATEGORY_CODE,REPLACE(INFOBOX_CODE,'".$cid."','".$cur_cid."'),TITLE,NOW(),'".$w2_thisUserID."',NOW(),'".$w2_thisUserID."',DELETE_STATUS,NULL FROM W2_CONTENT_REF_CATEGORY ";
	$sql .= "WHERE cid = '".$cid."' AND DELETE_STATUS = '".$w2_cfg["DB_W2_REF_ITEM_DELETE_STATUS"]["active"]."'";
	$objDB->db_db_query($sql);

	
	header("Location: /home/eLearning/w2/index.php?mod=admin&task=templateManagement&r_contentCode=".$parseData["r_contentCode"]."&r_returnMsgKey=Addsuccess");
}else{
	header("Location: /home/eLearning/w2/index.php?mod=admin&task=templateManagement&r_contentCode=".$parseData["r_contentCode"]."&r_returnMsgKey=AddUnsuccess");
}
function folder_copy($source, $dest){
	$lf = new libfilesystem();
    $row = $lf->return_folderlist($source);
    for($i=sizeof($row)-1; $i>=0; $i--){
	    $file = $row[$i];
	    if($file=="." || $file=="..") continue;
	    $ext = str_replace(substr($source."/",0,strrpos($source."/","/")),'',$file);
	    $lf->item_copy($file, $dest.$ext);
    }
}
exit();
?>
