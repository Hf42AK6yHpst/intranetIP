<?php
/*
 * Editing by 
 * 
 * Modification Log: 
 * 2015-09-23 (Jason)
 * 		- fix the problem of empty PowerConceptID and PowerBoardID which does not set properly in Step 2. Due to the old system design 
 */
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentGenerator.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$cid = $_GET['cid'];
$r_contentCode = $_GET['r_contentCode'];
try 
{
	$W2EngContentParser = new libW2EngContentParser();
	$parseData = $W2EngContentParser->parseEngContent($cid);
	
	# check and repaire PowerConcept record
	if($parseData['conceptType']==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]&&$parseData['powerConceptID']=='')
	{
//		bl_error_log($projectName='W2 ENGINE',$errorMsg="MISSING PowerconceptID While Generation",$errorType='');
//		header("Location: /home/eLearning/w2/index.php?mod=admin&task=templateManagement&r_contentCode=${r_contentCode}&r_returnMsgKey=AddUnsuccess");
//		exit();

		# insert empty powerconcept record 
		# refer to /home/eLearning/w2/admin/edtConceptMap.php > _insertEmptyPowerConceptRecord()
		$objDB = new libdb();
		$sql = "INSERT INTO TOOLS_POWERCONCEPT (UserID, ModuleCode, InputDate, ModifiedDate)
				VALUES ('".$w2_thisUserID."', '".$w2_cfg['moduleCode']."', NOW(), NOW())";
		$objDB->db_db_query($sql);
		$r_conceptMapId = $objDB->db_insert_id();
		$parseData['powerConceptID'] = $r_conceptMapId;
		
		# repaire / update the powerConceptID which does not set properly due to old system design
		$sql = "update W2_CONTENT_DATA set powerConceptID = ".$r_conceptMapId." where cid = '".$cid."' ";
		$objDB->db_db_query($sql);
	}	
	
	# check and repaire PowerBoard record
	if($parseData['conceptType']==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]&&$parseData['powerBoardID']=='')
	{
		# insert empty powerboard record 
		# refer to /home/eLearning/w2/admin/edtConceptMap.php > _insertEmptyPowerConceptRecord()
		$objDB = new libdb();
		$sql = "INSERT INTO TOOLS_POWERBOARD (UserID, ModuleCode, InputDate, ModifiedDate)
				VALUES ('".$w2_thisUserID."', '".$w2_cfg['moduleCode']."', NOW(), NOW())";
		$objDB->db_db_query($sql);
		$r_conceptMapId = $objDB->db_insert_id();
		$parseData['powerBoardID'] = $r_conceptMapId;
		
		# repaire / update the powerBoardID which does not set properly due to old system design
		$sql = "update W2_CONTENT_DATA set powerBoardID = ".$r_conceptMapId." where cid = '".$cid."' ";
		$objDB->db_db_query($sql);
	}	
	
	if($r_contentCode=='sci'){
		$stepsInvolved=6;
	}else{
		$stepsInvolved=5;
	}
	if(checkEngContentIsCompleted($parseData,$stepsInvolved))
	{
		//special branch for questions of array 
		$parseData['step4Data']['step4DraftQList'] = getStep4DraftQArrayParsed($parseData['step4Data']['step4DraftQList']);
		getResouceArrayReparsed($parseData);
		
		$W2EngContentGenerator=new libW2GenerateEngContent($PATH_WRT_ROOT);
		$statusUpdated = $W2EngContentGenerator->getTableUpdateToGenerated($cid);
//		if($r_contentCode=='eng'){
//			$W2EngContentGenerator->generateEngContent($parseData,$w2_classRoomDB,$eclass_db);
//		}
		if($parseData['conceptType']!=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){
			$W2EngContentGenerator->getMindMapGeneration($parseData, $w2_classRoomDB,$eclass_db);		
		}
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /home/eLearning/w2/index.php?mod=admin&task=templateManagement&r_contentCode=$r_contentCode&cid=$cid&r_returnMsgKey=AddSuccess");		
		exit;
	}
	else
	{
		//$msg = "Please check if the steps are all filled";
		//&Msg=${msg}
		bl_error_log($projectName='W2 ENGINE',$errorMsg="Incomplete Content",$errorType='');
		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /home/eLearning/w2/index.php?mod=admin&task=templateManagement&r_contentCode=${r_contentCode}&r_returnMsgKey=AddUnsuccess");
		exit;
	}
	
} 
catch (Exception $e) 
{
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
###################### Logic helper function ###############################################################
function checkEngContentIsCompleted($parseData,$stepsInvolved)
{
	$isCompleted= true;
	if($parseData['topicName']=='' || $parseData['topicIntro']==''
		|| $parseData['level']=='')
	{		
			$isCompleted = false;
	}
	for ($i=1;$i<=$stepsInvolved-1; $i++)
	{
		$isCompleted = !checkArray($parseData['step'.$i.'Data']); 
	}
	if($isCompleted)
	{
		$isCompleted = !checkArray($parseData['step'.$stepsInvolved.'Data']['step'.$stepsInvolved.'Int']);
	}
	return $isCompleted;
}
function checkArray( $array ) 
{
	if ( empty( $array ) ) return true;
    foreach ( (array)$array as $key => $value ) 
    {
        if ( is_array( $value ) ) 
        {
            if ( checkArray( $value ) ) return true;
        }
        else 
        {
            if ( empty( $value ) ) return true;
        }
    }
    return false;
}
function getStep4DraftQArrayParsed($parseData)
{
	$returnStr ='';
	$returnStr ='<table>';	

	$i=0;
	foreach( (array)$parseData as $draftQ)
	{
		$returnStr.= '<tr>';
		$returnStr.= '<td>';
		$returnStr.= $draftQ;
		$returnStr.= '<textarea rows="5" class="textbox" id="textfield" name="r_question[textarea][<?=$w2_m_ansCode['.$i.']?>]" <?=$w2_h_answerDefaultDisabledAttr?>><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode['.$i.']])?></textarea>';
		$returnStr.= '</td>';
		$returnStr.= '</tr>';
		++$i;
	}
	$returnStr .= '</table>';

	return $returnStr;
}

function getResouceArrayReparsed(&$parseData)
{
	global $Lang;
	$resourceList = $parseData['resourceData']['resource'];
	
	
	foreach( (array) $resourceList as $resourceItem)
	{
		if (!preg_match('/^http:\/\//', $resourceItem['resourceItemWebsite'])) 
		{
			$resourceItem['resourceItemWebsite'] = 'http://'.$resourceItem['resourceItemWebsite'];
		}
		$resourceItemHTML = "<a target='_blank' href='{$resourceItem[resourceItemWebsite]}'>".$resourceItem['resourceItemName'].'</a><br />';
		
		for ($i=0; $i<5; ++$i)
		{
			if ( $resourceItem["resourceItemChecked"]['c'.($i+1)] )
			{
				$parseData['step'.($i+1).'Data']['step'.($i+1).'Resource'] .= $resourceItemHTML;		
			}
		}
		
	}
	for($i = 0; $i < 5; ++$i)
	{
		if($parseData['step'.($i+1).'Data']['step'.($i+1).'Resource'])
		{
			$parseData['step'.($i+1).'Data']['step'.($i+1).'Resource'] = '<br />'.$Lang['W2']['refWebsite'].':<br />'.$parseData['step'.($i+1).'Data']['step'.($i+1).'Resource'];
		}
	}
}


?>