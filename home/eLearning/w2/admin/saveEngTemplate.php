<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentGenerator.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
$cid = trim($_POST['cid']);
$r_contentCode = trim($_POST['r_contentCode']);
$saveTab = isset($_POST['saveTab'])?trim($_POST['saveTab']):exit;
$parseData = $engContentParser->parseEngContent($cid);
### DB Control Info
$schemeNum= $objEngContentBuilder->getNextSchemeNum($r_contentCode);
//isInjectionCodePresent($_POST)?'OK':exit();

try 
{
	
	switch($saveTab)
	{
		case 0:
			### General column
			$topicIntro = isset($_POST['topicIntro'])?trim($_POST['topicIntro']):exit;
			$topicName = isset($_POST['topic'])?trim($_POST['topic']):exit;
			$level =  isset($_POST['level'])?trim($_POST['level']):exit;
			$grade =  isset($_POST['grade'])?trim($_POST['grade']):exit;			
//			$category =  isset($_POST['category'])?trim($_POST['category']):exit;
			if( empty($cid) )
			{
				$buildData[]= $objEngContentBuilder->buildGeneralInfoInsert($topicIntro,$topicName,$level,$grade);
			}
			else
			{
				$buildData[]= $objEngContentBuilder->buildGeneralInfoUpdate($topicIntro,$topicName,$level,$grade);
			}
			
			break;
		case 1:
			### Step1 column
			$step1Int =  isset($_POST['step1Int'])?trim($_POST['step1Int']):exit;
			$step1TextType = isset($_POST['step1TextType'])?trim($_POST['step1TextType']):exit;
			$step1Purpose = isset($_POST['step1Purpose'])?trim($_POST['step1Purpose']):exit;
			$step1Content = isset($_POST['step1Content'])?trim($_POST['step1Content']):exit;
			$buildData[]= $objEngContentBuilder->buildStep1($step1Int,$step1TextType,$step1Purpose,$step1Content);
			break;
		case 2:
			### Step2 column
			$step2Int = isset($_POST['step2Int'])?trim($_POST['step2Int']):exit;
			$step2ConceptType = isset($_POST['conceptType'])?trim($_POST['conceptType']):exit;
			$buildData[]= $objEngContentBuilder->buildStep2($step2Int);
			if($parseData['status']==W2_GENERATED){
				$W2EngContentGenerator=new libW2GenerateEngContent($PATH_WRT_ROOT);
				$W2EngContentGenerator->getMindMapGeneration($parseData, $w2_classRoomDB,$eclass_db);
			}
			break;
//		case 3:
//			$step3Int = isset($_POST['step3Int'])?trim($_POST['step3Int']):exit;
//			$step3Sample = isset($_POST['step3Sample'])?trim($_POST['step3Sample']):exit;
//			$step3Vocab = isset($_POST['step3Vocab'])?trim($_POST['step3Vocab']):exit;
//			$step3Grammar = isset($_POST['step3Grammar'])?trim($_POST['step3Grammar']):exit;
//			$buildData[]= $objEngContentBuilder->buildStep3($step3Int,$step3Sample,$step3Vocab, $step3Grammar);
//			break;			
		case 3:
			### Step3 column
			$step3Data = $parseData['step3Data'];
			$step3Vocab = !empty($step3Data["step3Vocab"])?$step3Data["step3Vocab"]:'';
			$step3Grammar = !empty($step3Data["step3Grammar"])?$step3Data["step3Grammar"]:'';
			
			$step3Int = isset($_POST['step3Int'])?trim($_POST['step3Int']):exit;
			$paragraphCounter = isset($_POST['paragraphCounter'])?trim($_POST['paragraphCounter']):exit;
			$step3ParagraphAry = array();
			for($i=1;$i<=$paragraphCounter;$i++){
				$_title = isset($_POST['leftBubbleTitle_'.$i])?addslashes(intranet_htmlspecialchars(trim($_POST['leftBubbleTitle_'.$i]))):'';
				$_content = isset($_POST['leftBubbleContent_'.$i])?addslashes(intranet_htmlspecialchars(trim($_POST['leftBubbleContent_'.$i]))):'';
				$_paragraph = trim($_POST['editorParagraph'.$i]);
				if(!empty($_paragraph)){
					$step3ParagraphAry[] = array('leftBubbleTitle'=>$_title,'leftBubbleContent'=>$_content,'paragraph'=>$_paragraph);
				}							
			}
			for($i=1;$i<=$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
				$_title = addslashes(intranet_htmlspecialchars(trim($_POST['remarkTitle_'.$i])));
				$_content = addslashes(intranet_htmlspecialchars(trim($_POST['remarkContent_'.$i])));
				if(!empty($_title)||!empty($_content))
					$step3RemarkAry[] = array('remarkTitle'=>$_title,'remarkContent'=>$_content);		
				else
					$step3RemarkAry[] = array();									
			}	
					
			$buildData[]= $objEngContentBuilder->buildStep3New($step3Int, $step3ParagraphAry, $step3RemarkAry, $step3Vocab, $step3Grammar);
			
			
//			$step3Sample = isset($_POST['step3Sample'])?trim($_POST['step3Sample']):exit;
//			$step3Vocab = isset($_POST['step3Vocab'])?trim($_POST['step3Vocab']):exit;
//			$step3Grammar = isset($_POST['step3Grammar'])?trim($_POST['step3Grammar']):exit;
//			$buildData[]= $objEngContentBuilder->buildStep3($step3Int,$step3Sample,$step3Vocab, $step3Grammar);
		
			break;
		case 4:
			### Step4 column
			$step4Int = isset($_POST['step4Int'])?trim($_POST['step4Int']):exit;
			$step4DraftQ = isset($_POST['step4DraftQ'])?$_POST['step4DraftQ']:'';
//			$step4DraftQ = ($_POST['step4DraftQ']);
			$buildData[]= $objEngContentBuilder->buildStep4($step4Int,$step4DraftQ);
			
			break;
		case 5:
			### Step5 column
			$step5Int = isset($_POST['step5Int'])?trim($_POST['step5Int']):exit;
			$step5DefaultWriting = isset($_POST['step5DefaultWriting'])?trim($_POST['step5DefaultWriting']):exit;
			$buildData[]= $objEngContentBuilder->buildStep5($step5Int,$step5DefaultWriting);
			break;
		case 6:
			### Step5 column
			
			$resourceName = $_POST['resource'];
			$resourceWebsite = $_POST['website'];
			
			$resourceCount = count($resourceName);
			for($i=0; $i<$resourceCount; ++$i)
			{
				$tempObj=null;
				if( trim($resourceName[$i])=='')
				{
					continue;
				}
				$tempObj['resourceName'] = trim($resourceName[$i]);
				$tempObj['resourceWebsite'] = trim($resourceWebsite[$i]);
				
				for($c=0; $c<5; ++$c)
				{
					if($_POST["r".($i+1)."_step".($c+1)])
					{
						$tempObj['checked'][]='c'.($c+1);
					}
				}
				$refObj[] = $tempObj;

			}
			$buildData[]= $objEngContentBuilder->buildResource($refObj);
			break;
		default:
			break;
	}
	
	if( empty($cid) )
	{
			$buildData[]= $objEngContentBuilder->buildDBInfoInsert($r_contentCode,$schemeNum);	
			$sql = 'INSERT INTO W2_CONTENT_DATA
			('
			.$buildData[0]['field'].','
			.$buildData[1]['field'].
			
			') VALUES
			('
			.$buildData[0]['content'].','
			.$buildData[1]['content'].
			');';
			
			if($saveTab==1||$saveTab==2)
			{
				if($saveTab==1){
					$buildData[2]['field'] = 'category';
					$buildData[2]['content'] = "'$step1TextType'";
				}elseif($saveTab==2){
					$buildData[2]['field'] = 'conceptType';
					$buildData[2]['content'] = "'$step2ConceptType'";
				}
				$sql = 'INSERT INTO W2_CONTENT_DATA
				('
				.$buildData[0]['field'].','
				.$buildData[1]['field'].','
				.$buildData[2]['field'].
				
				') VALUES
				('
				.$buildData[0]['content'].','
				.$buildData[1]['content'].','
				.$buildData[2]['content'].
				');';
			}
	}
	else
	{
			if($saveTab==0)
			{
				$data = $buildData[0];
			}
			else
			{
				$data = $buildData[0]['field'].'='.$buildData[0]['content'];
			}
			$buildData[]= $objEngContentBuilder->buildDBInfoUpdate($r_contentCode,$schemeNum);
			
			if($saveTab==1)
			{
				$buildData[2]['field'] = 'category';
				$buildData[2]['content'] = "'$step1TextType'";
				
				$data = $buildData[0]['field'].'='.$buildData[0]['content'].','.
						$buildData[2]['field'].'='.$buildData[2]['content'];				
			}elseif($saveTab==2){
				$buildData[2]['field'] = 'conceptType';
				$buildData[2]['content'] = "'$step2ConceptType'";
				$data = $buildData[0]['field'].'='.$buildData[0]['content'].','.
						$buildData[2]['field'].'='.$buildData[2]['content'];	
			}		
			$sql = 'UPDATE W2_CONTENT_DATA SET '.			
			$data.','.
			implode(",", (array)$buildData[1]).
			' WHERE cid=\''.$cid.'\';';
	}
	$rs = $objDB->db_db_query($sql);
	
	if($rs)
	{
		$cid = empty($cid)?$objDB->db_insert_id():$cid;
		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /home/eLearning/w2/index.php?mod=admin&task=newTemplateWriting&r_contentCode=$r_contentCode&cid=$cid&r_returnMsgKey=AddSuccess&tabNum=$saveTab");
	}
} 
catch (Exception $e) 
{
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}


#####################Helper function #####################################################
function isInjectionCodePresent($stringList)
{
	foreach($stringList as $string)
	{
		if (strpos($string, '<?php') !== false)
	    {
	    	return true;
	    }
	}
}

?>