<?php
$PATH_WRT_ROOT = "../../../../";
$PATH = $PATH_WRT_ROOT.'home/eLearning/w2/util/script/';
include_once($PATH_WRT_ROOT."includes/global.php");

intranet_opendb();
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/w2/w2_contentConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/w2/w2Config.inc.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH."engineData.inc.php");

global $w2_cfg_contentSetting, $eclass_db, 
		$eclass_prefix,$w2_cfg,
		$engineTopicList;
		

$roomType = $w2_cfg["w2ClassRoomType"];
$w2_classRoomID= getEClassRoomID($roomType);
$w2_classRoomDB = $eclass_prefix.'c'.$w2_classRoomID;
$objDb = new libdb();

	echo "###########################################################";
	echo "<br>";
	echo "Part 1 : Static Content";
	foreach($w2_cfg_contentSetting as $subject=>$topicList){
		echo '<table>';
		echo '<tr><th colspan="3">'.$subject.'</th></tr>';
		
		$i=0;
		$countSubjectSuccess =0;
		$countSubjectFailed =0;
		$countSkip = 0;
		foreach($topicList as $topicCode=>$topicData){
		$powerconceptName = 'W2_DEFAULT_'.strtoupper($subject).'_'.strtoupper($topicCode);
		$sql=<<<SQL
			Select
				1
			From
				{$w2_classRoomDB}.powerconcept
			Where
				Name = "{$powerconceptName}";
SQL;
			$_return=$objDb->returnArray($sql);
			echo '<tr>';
			echo '<td>'.++$i.'</td>';
			echo '<td>'.$topicData['name'].'</td>';
			echo '<td>';
			
			if( 
				($subject =='eng' && $topicCode=='scheme53' ) ||
				($subject =='eng' && $topicCode=='scheme54' ) ||
				($subject =='chi' && $topicCode=='scheme13' ) ||
				($subject =='chi' && $topicCode=='scheme14' )
			){
				echo '<font color="blue">Originally No Powerconcept</font>';
				++$countSkip;
			}
			else{
				echo ($_return[0]?'<font color="blue">Powerconcept Exists</font>':'<font color="red">Powerconcept Not Exists</font>');
				$_return[0]?++$countSubjectSuccess:++$countSubjectFailed;
				
			}

			echo '</td>';
	
			echo '</tr>';

		}
		$totalCount = $countSubjectFailed+$countSubjectSuccess+$countSkip;
		echo "<tr><td colspan='3'><strong>
									<font color='green'>Total: $totalCount </font>,
									<font color='blue'>Success: $countSubjectSuccess </font>,
									<font color='blue'>Skipped: $countSkip </font>,
									<font color='red'>Failed: $countSubjectFailed</font>
					</strong></td></tr>";
		echo '</table>';
	}
	echo "###########################################################";	
	
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo "<br>";
	
	echo "###########################################################";
	echo "<br>";
	echo "Part 2 : Engine Content";
	
	
	$i=0;
	echo '<table style="width:100%;border:black solid 1px">';
	echo '<tr><th colspan="4">Engine Data List</th></tr>';
	
	$countSubjectSuccess =0;
	$countSubjectFailed =0;
	$countSkip = 0;
	foreach($engineTopicList as $topicData){
			
		$sql=<<<SQL
			Select
				cd.topicName, tp.powerconceptID
			From
				W2_CONTENT_DATA cd
			Left Join
				TOOLS_POWERCONCEPT tp
			USING
				(POWERCONCEPTID)
			Where
				cd.level = "{$topicData[0]}"
			And
				cd.topicName = "{$topicData[1]}"
SQL;
			$_return=$objDb->returnArray($sql);
			echo '<tr>';
			
			echo '<td>'.++$i.'</td>';
			echo '<td>'.$topicData[1].'</td>';
			echo '<td>'.($_return[0]['topicName']?
					'<font color="blue">Content Exists</font>':
					'<font color="red">Content Not Exists</font>').'</td>';
			echo '<td>'.($_return[0]['powerconceptID']?
					'<font color="blue">Powerconcept Exists</font>':
					'<font color="red">Powerconcept Not Exists</font>').'</td>';
			
	
			echo '</tr>';
			($_return[0]['topicName']&&$_return[0]['powerconceptID'])?++$countSubjectSuccess:++$countSubjectFailed;

	}
		$totalCount = $countSubjectFailed+$countSubjectSuccess+$countSkip;
		echo "<tr><td colspan='4'><strong>
									<font color='green'>Total: $totalCount </font>,
									<font color='blue'>Success: $countSubjectSuccess </font>,
									<font color='blue'>Skipped: $countSkip </font>,
									<font color='red'>Failed: $countSubjectFailed</font>
					</strong></td></tr>";
	
	echo '</table>';
	echo "###########################################################";

intranet_closedb();

?>