<?php
/*
 * 	Log
 * 	Date:	2015-03-24 [Adam]: Fetch the classroom id again once created
 */

$PATH_WRT_ROOT = "../../../../";
$PATH = $PATH_WRT_ROOT.'home/eLearning/w2/util/script/';

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/w2_lang.$intranet_session_language.php");


global $intranet_db,$eclass_db, $intranet_db_user, $intranet_db_pass,
$eclass_db_user, $eclass_db_pass, $config_school_code ;

//intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/w2/w2Config.inc.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");


$roomType = $w2_cfg["w2ClassRoomType"];
//$roomType = 14; ### 20140829 Temp Change
$w2_classRoomID= getEClassRoomID($roomType);

$part1 = $_GET['part1']?$_GET['part1']:false;
$part2 = $_GET['part2']?$_GET['part2']:false;

//if(!$plugin['W2']){
//	echo "No W2 is installed at this school!";
//	exit();
//	
//}
if($w2_classRoomID){
	echo "### Skip Eclass Creation.";
	echo "<br>";
	echo "<br>";
}
else{
	include_once($PATH_WRT_ROOT."home/eLearning/w2/util/createEclassRoom.php");
	$w2_classRoomID= getEClassRoomID($roomType);
}



	### Part 1 Static Files With Concept Map
	
	echo '### Start W2 Static Content Input ###';
	echo '<br>';	
	echo '<br>';	
	$fileName = "{$PATH}w2_static.sql";
	$command = "mysql -hlocalhost -u".OsCommandSafe($eclass_db_user)." -p".OsCommandSafe($eclass_db_pass)." ".OsCommandSafe($eclass_db)." < ".OsCommandSafe($fileName)."";
	debug_r($fileName);
	if($part1){
		exec($command,$output=array(),$worked);
		switch($worked){
		    case 0:
		        echo 'Import file <b>w2_static</b> successfully imported to database <b>' .$mysqlDatabaseName .'</b><br>';
		        break;
		    case 1:
		        echo '<font color="red">There was an error during import. Please make sure the import file is saved in the same folder as this script and check your values:<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$mysqlDatabaseName .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$mysqlUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$mysqlHostName .'</b></td></tr><tr><td>MySQL Import Filename:</td><td><b>w2_static</b></td></tr></table><br></font>';
		        break;
		   	default:
		   		debug_r($output);
		   	break;
		}
	
		# Finally, link everything together
		$r_run=1;
		include_once($PATH_WRT_ROOT."/home/eLearning/w2/util/synConceptMapToPreview.php");
	}
	else{
		echo '<font color="green">### Skipped This Part ###</font>';
		echo '<br>';	
		echo '<br>';	
		echo '<br>';	
		
	}
	echo '#######################################################################################################';
	echo '<br>';
	echo '<br>';
	
	$schoolCode = $config_school_code;
	### Part 2 English Engine Data

	echo 'Start W2 Engine Content Input';
	echo '<br>';
	echo '<br>';
	
	if($part2){	
		foreach(range(1,4) as $i){
			include_once($PATH."/engine_{$i}.php");
		}
			
				
		### Update SchoolCode
		$objDb = new libdb();
		$sql=<<<SQL
			UPDATE
				{$intranet_db}.W2_CONTENT_DATA
			SET
				schoolCode = "{$schoolCode}",
				status = 0	
SQL;
		$_return=$objDb->db_db_query($sql);
		if($_return){
			echo '### School Code Update Success! ###';
			echo '<br>';	
		}
		else{
			echo '<font color="red">### School Code Update Failure! ###</font>';
			echo '<br>';	
		}		
		### Create Config File

	}
	else{
		echo '<font color="green">### Skipped This Part ###</font>';
		echo '<br>';	
		echo '<br>';	
		echo '<br>';	
		
	}
	$configFilePath = "{$PATH_WRT_ROOT}/home/eLearning/w2/content/{$schoolCode}_w2_contentConfig.inc.php";
	
	if(!file_exists($configFilePath) && $genConfig){
			echo '<font color="green">Config File Does Not Exist! Create Now. </font><br>';
			$myfile = fopen($configFilePath, "w");
			fclose($myfile);
			if(file_exists($configFilePath)){
				echo '<font color="blue">Create Successfully. </font><br>';
			}
			else{
				echo '<font color="red">Failed!!! Please Check if the access right is not configured properly. </font><br>';
			}
	}
	else{
			echo '<font color="green">Config File Exists! This step is skipped. </font><br>';
	}
	echo 'Operation End.';
	
?>