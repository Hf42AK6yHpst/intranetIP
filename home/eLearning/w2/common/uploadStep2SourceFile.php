<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($intranet_root."/includes/libfilesystem.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
$objFile = new libfilesystem();
#init
$step2Source = array();
$uploadFileListAry = array();
$cid = trim($_POST['cid']);
$step = trim($_POST['step']);
$type = trim($_POST['type']);

define("UPLOAD_SUCCESS", 1);
$tarPath = '/file/w2/reference/cid_'.$cid;
$destinationPath = $intranet_root.$tarPath;

$parseData = $engContentParser->parseEngContent($cid);
$step2Data = $parseData['step2Data'];
$step2Int = isset($_POST['step2Int'])?trim($_POST['step2Int']):exit;
$existingStep2SourceAry = $step2Data['step2SourceAry'];

$idx = 0;
for($i=0;$i<$Cnt_step2Source;$i++){
	$_file = $_FILES['step2SourceImage'.$i]; 
	$_sourceQ = $_POST['step2Source'.$i.'Question'];
	$_sourceA = $_POST['step2Source'.$i.'Answer'];
	$_existSource = $existingStep2SourceAry['sourceItem'.$i];
	$_sourceCnt = count($_sourceQ);
	$_error = $_file['error'];
	if($_sourceCnt>0){//have record
		if($_error==0&&!empty($_file)){ //new image file
			$_name = $_file['name'];
			$_type = $_file['type'];
			$_size = $_file['size'];
			$_tmp_name = $_file['tmp_name'];
			if(trim($_name) != ''){
				$_timeStamp = date('Y-m-d_H-i-s');
				$_filepath = $_timeStamp.'/'.$_name;
				$_descPath = $destinationPath.'/'.$_timeStamp;
				$_destinationFile = $destinationPath.'/'.$_filepath;
				
				//check related folder if not exist
				if(file_exists($_descPath)){
					//do nothing
					$folderIsReadyForCopy  = true;
				}else{
					$objFile->folder_new($_descPath);
					//make sure the folder exist
					if(file_exists($_descPath)){
						$folderIsReadyForCopy  = true;
					}else{
						//cannot create the folder stop the upload
						exit();
					}
				}
				if($folderIsReadyForCopy){
					$result = move_uploaded_file($_tmp_name,$_destinationFile);
					if($result == UPLOAD_SUCCESS && file_exists($_destinationFile)){
						$step2SourceAry['sourceItem'.$idx]['image']=$_filepath;
						//remove existing image if any
						if(!empty($_existSource['image'])){
							list($_timeStamp,$_fileName) = explode('/',$_existSource['image']); 
							$objFile->file_remove($destinationPath.'/'.$_existSource['image']);
							$row = $objFile->return_folderlist($destinationPath.'/'.$_timeStamp);
							if(sizeof($row)==0){
								$objFile->folder_remove($destinationPath.'/'.$_timeStamp);
							}
						}
					}
				}			
			}
			
		}elseif(!empty($_existSource['image'])){ //use existing image file
			$step2SourceAry['sourceItem'.$idx]['image']=$_existSource['image'];
		}
		$step2SourceAry['sourceItem'.$idx]['questionAry'] = array();
		for($j=0;$j<$_POST['Cnt_step2Source'.$i];$j++){
			if(!empty($_sourceQ[$j])&&!empty($_sourceA[$j])){
				$step2SourceAry['sourceItem'.$idx]['questionAry'][$j]['question'] = trim($_sourceQ[$j]);
				$step2SourceAry['sourceItem'.$idx]['questionAry'][$j]['answer'] = trim($_sourceA[$j]);
			}
		}
		$idx++;
	}else if(count($_existSource)>0){
		//remove existing file
		if(!empty($_existSource['image'])){
			list($_timeStamp,$_fileName) = explode('/',$_existSource['image']); 
			$objFile->file_remove($destinationPath.'/'.$_existSource['image']);
			$row = $objFile->return_folderlist($destinationPath.'/'.$_timeStamp);
			if(sizeof($row)==0){
				$objFile->folder_remove($destinationPath.'/'.$_timeStamp);
			}
		}
	}
}

$buildData = $objEngContentBuilder->buildLsStep2($step2Int,$step2SourceAry,$updateFile=true);
$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";

$rs = $objDB->db_db_query($sql);

?>
<script>
parent.goToNextStep();
</script>