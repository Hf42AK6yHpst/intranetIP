<?php

$sql = 'SELECT FILE_ORI_NAME,CONCAT(\''.$intranet_root.'\', FILE_PATH, \'/\',FILE_HASH_NAME)  as DOWNLOAD_PATH from W2_STEP_STUDENT_FILE where FILE_HASH_NAME = \''.$r_fName.'\'';

$objDb  = new libdb();
error_log($sql."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
$result = $objDb->returnResultSet($sql);

if(is_array($result) && count($result) == 1){
	$result  = current($result);
	error_log($result['DOWNLOAD_PATH']."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
	$file_abspath = $result['DOWNLOAD_PATH'];
	$raw_file_name = $result['FILE_ORI_NAME'];
}else{
	exit();
}

if($file_abspath == ''){
	exit();
}
// Check browser agent
$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
if (preg_match('/opera/', $userAgent)) { $agentName = 'opera'; } 
elseif (preg_match('/webkit/', $userAgent)) { $agentName = 'safari'; } 
elseif (preg_match('/msie/', $userAgent)) { $agentName = 'msie'; } 
elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { $agentName = 'mozilla'; } 
else { $agentName = 'unrecognized'; }

// Output file to browser
$mime_type = mime_content_type($file_abspath);
$file_content = file_get_contents($file_abspath);

switch ($agentName) {
	default:
	case "msie":
		$file_name = urlencode($raw_file_name);
		break;
	case "mozilla":
		$file_name = $raw_file_name;
		break;
}

# header to output
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-type: $mime_type");
header("Content-Length: ".strlen($file_content));
header("Content-Disposition: attachment; filename=\"".$file_name."\"");

echo $file_content;

?>