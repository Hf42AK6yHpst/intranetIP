<script>
$(document).ready( function() {	
	<?=$h_writingEditorOnDocumentReadyJs?>
});
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function loadStudentAttachmentByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_studentAttachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
<?php 
//duplicat upload file in callBack_saveStepAns, comment first
//function callBack_saveFinalStepAns
?>
function callBack_saveStepAns(){
	uploadFile();
}

function individualAnsHandling() {
	<?=$h_writingEditorIndividualAnsHandlingJs?>
}

function saveMarkReturnMsg(status) {
	var returnMsg = (status=='1')? '<?=$Lang['General']['ReturnMessage']['RecordSaveSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['RecordSaveUnSuccess']?>';
	
	Scroll_To_Top();
	Get_Return_Message(returnMsg);
}

</script>
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>6</span><?=$Lang['W2']['step6Title'][$contentCode]?></div></div></div>
						<div class="write_board_left">
							<div class="write_board_right">
                            	<div class="instruction"> <!-- New class 20140113 --><?=intranet_undo_htmlspecialchars($step6Data['step6Int'])?></div>
                            	<div class="subtitle_new"><span><?=$Lang['W2']['engMyWriting']?></span></div>
                            <div class="content">
                            	<div class="draft">
	                                <div class="write">
	                                <?=$h_writingTaskDisplay?>
	                                <br />
	                                </div>
                               		<div class="ref">
										<span class="title"><?=$Lang['W2']['vocabularies']?>:</span>
										<ul class="point">
										<?php
											$vocabAry = array();
											$meaningAry = array();
											
											foreach((array)$step4Data['HightlightWordsAry']['vocabularies'] as $_itemAry){
												$vocabAry[] = array(stripslashes($_itemAry['word']),stripslashes($_itemAry['meaning']));
											}
											foreach((array)$step4Data['HightlightWordsAry']['expressions'] as $_itemAry){
												$meaningAry[] = array(stripslashes($_itemAry['word']),stripslashes($_itemAry['meaning']));
											}
										?>
										<?=$w2_libW2->generateReferencePanelStyle2($vocabAry,'vocab');?>
                                  		</ul>
										<div class="line"></div><span class="title"><?=$Lang['W2']['usefulExpressions']?>:</span>
		                                  <ul class="point">
												<?=$w2_libW2->generateReferencePanelStyle2($meaningAry,'useful')?>   
		                                  </ul>
										<div class="line"></div>
										
										<?if($h_conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){?>
											<span class="title"><?=$Lang['W2']['brainstorming']?>:</span> <a class="thickbox" href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadStudentAttachmentByThickbox('','<h1 class=ref><?=addslashes($Lang['W2']['conceptMap'])?></h1>','');"><br/><?=$Lang['W2']['clickHere']?></a>
										<?}else{?>	
											<span class="title"><?=$Lang['W2']['brainstorming']?>:</span> <a href="javaScript:void(0)" OnClick="<?=$h_conceptMapLink?>"><br/><?=$Lang['W2']['clickHere']?></a>
										<?}?>	
									</div>						
                                </div>
                                <!-- start of student attachment -->
                                <br style="clear:both" /><br />
                                <?=$step6Data['step6Resource']?>
								<div id="w2_studentUploadFilePanel">
<?
echo $h_fileUpload;
?>
								</div>
                                <!-- end of student attachment -->
                          </div>
                           <p class="spacer"></p>
                           <div class="edit_bottom">
<?php 
	 echo $h_saveAsDraftButton;
?>
<?
	echo $h_submitHandInButton;
?>
<?
	echo $h_cancelButton;
?>    
                              <!--<input name="submit4" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="Cancel" />-->
                              </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
					<input type="hidden" id="r_editorSource" name="r_editorSource" value="<?=$w2_h_editorSource?>" />