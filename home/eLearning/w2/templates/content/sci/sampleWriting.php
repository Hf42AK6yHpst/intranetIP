<script>
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function loadHighlightedWordsByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_highlightedWords', filePath, headerHtml, extraParam, '' , 'handinForm');
}
</script>
<div class="write_board">
	<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>4</span><?=$Lang['W2']['step4Title'][$contentCode]?></div></div></div>
	<div class="write_board_left"><div class="write_board_right">
    	<div class="instruction"><?=intranet_undo_htmlspecialchars($step4Data['step4Int'])?></div>
        <div class="subtitle_new"><span><?=$Lang['W2']['writingSample']?></span></div> <!-- New class 20140113 -->
        <div class="btn_right_grp_new"">
        <?if($hasTeacherAttachment){?>
        	<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes($Lang['W2']['teacherAttachment'])?></h1>','');">
				<span><?=$Lang['W2']['teacherAttachment']?></span>
			</a>
		<?}?>
			<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadHighlightedWordsByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['highlightedWordsPhrases']))?></h1>','');" class="thickbox btn_ref_new">
				<span><?php echo $Lang['W2']['highlightedWordsPhrasesWithBreak']?></span>
			</a>
		</div>
        <div class="content">
        	<div class="sample_new">
            	<!---- paper start------------>
                <div id="paper_template" class="template_<?=$paddedTemplateID?> paper_template_fit"> <!-- 'paper_template_fit' is for the new structure 20140109 -->
                 	<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">
                    	<div class="pt_top_element"></div>
                    </div></div></div>
                 	<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">
                    	<div class="paper_main_content">
                    	<?=$stepParagraphHTML?>
                        <p class="spacer"></p>
                        </div>
                        
                    </div></div></div>
                   	<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">
                    </div></div></div>
                    <div id="template_clipart"><span class="clipart_<?=$paddedClipArtID?>"></span></div>
                </div>
                <div class="remarks remarks_fit"><!-- 'remarks_fit' is for the new structure 20140109 -->
                <?if(!empty($step4Data['step4Note'])){?>
	                <div class="keynote">
	                	<div id="keynote"><span><?=$Lang['W2']['note']?>:</span><br /><?=stripslashes($step4Data['step4Note'])?></div>
	                </div>
	            <?}?>    
					<?=$stepRemarkHTML?>
				</div>
            </div>
            <br style="clear:both;">
            <?=$step4Data['step4Resource']?>
      </div>
      <p class="spacer"></p>
       <div class="edit_bottom">
            <?php
					echo $h_nextStepButton;
					echo $h_cancelButton;
			?> 
		</div>
	</div></div>
	<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
</div>
                        
          <!-- ********* Main  Content end ************ -->