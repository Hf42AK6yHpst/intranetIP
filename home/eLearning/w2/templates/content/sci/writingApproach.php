<script>
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function loadStudentAttachmentByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_studentAttachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
</script>

          
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>5</span><?=$Lang['W2']['step5Title'][$contentCode]?></div></div></div>
						<div class="write_board_left"><div class="write_board_right">
                            <div class="instruction"> <!-- New class 20140113 --><?=intranet_undo_htmlspecialchars($step5Data['step5Int'])?></div>
                            <div class="subtitle_new"><span><?=$Lang['W2']['myDraft']?></span></div>
                            <div class="btn_right_grp_new">
						        <?if($hasTeacherAttachment){?>
						        	<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes($Lang['W2']['teacherAttachment'])?></h1>','');">
										<span><?=$Lang['W2']['teacherAttachment']?></span>
									</a>
								<?}?>
							</div>
							<div class="content">
                            	<div class="draft">
                            		<div class="write">
		                                <?$i=0;?>
					            		<table>
					            		<?foreach((array)$step5Data['step5OutlineAry'] as  $_value):?>
								        		<tr>
													<td>
														<?=($i+1)?>. <?=stripslashes($_value)?>
													</td>
												</tr>	
					                            <?$w2_m_ansCode=$ansPrefix.($i+1);?>	
												<tr>
													<td><textarea name="r_question[textarea][<?=$w2_m_ansCode?>]" rows="5" class="textbox" id="textfield" <?=$w2_h_answerDefaultDisabledAttr?>><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?></textarea></td>
												</tr>
											<?$i++;?>
					            		<?endforeach;?>
					            		</table>
					            		<br />
					            		<?=$step5Data['step5Resource']?>
                                	</div>
									<div class="ref">
										<span class="title"><?=$Lang['W2']['vocabularies']?>:</span>
										<ul class="point">
										<?php
											$vocabAry = array();
											$meaningAry = array();
											foreach((array)$step4Data['HightlightWordsAry']['vocabularies'] as $_itemAry){
												$vocabAry[] = array($_itemAry['word'],$_itemAry['meaning']);
											}
											foreach((array)$step4Data['HightlightWordsAry']['expressions'] as $_itemAry){
												$meaningAry[] = array($_itemAry['word'],$_itemAry['meaning']);
											}
										?>
										<?=$w2_libW2->generateReferencePanelStyle2($vocabAry,'vocab');?>
                                  		</ul>
										<div class="line"></div><span class="title"><?=$Lang['W2']['usefulExpressions']?>:</span>
		                                  <ul class="point">
												<?=$w2_libW2->generateReferencePanelStyle2($meaningAry,'useful')?>   
		                                  </ul>
										<div class="line"></div>
										<?if($h_conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){?>
											<span class="title"><?=$Lang['W2']['brainstorming']?>:</span> <a class="thickbox" href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadStudentAttachmentByThickbox('','<h1 class=ref><?=addslashes($Lang['W2']['conceptMap'])?></h1>','');"><br/><?=$Lang['W2']['clickHere']?></a>
										<?}else{?>	
											<span class="title"><?=$Lang['W2']['brainstorming']?>:</span> <a href="javaScript:void(0)" OnClick="<?=$h_conceptMapLink?>"><br/><?=$Lang['W2']['clickHere']?></a>
										<?}?>	
									</div>
                                </div>
                                
                            </div>

                           <p class="spacer"></p>
                           <div class="edit_bottom">                                
							<?
								echo $h_nextStepButton;
								echo '&nbsp;';
								echo $h_cancelButton;
							?>
                           </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>