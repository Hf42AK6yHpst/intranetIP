<script>
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}

</script>

		<!-- ************ Main Content Start ************  -->
         
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>1</span><?=$Lang['W2']['step1Title'][$contentCode]?></div></div></div>
						<div class="write_board_left"><div class="write_board_right">
                            <div class="instruction"><?=intranet_undo_htmlspecialchars($step1Data['step1Int'])?></div>                      
                            <div class="subtitle_new"><span><?=$Lang['W2']['topicAnalysis']?></span></div>
                        <?if($hasTeacherAttachment){?>
				            <div class="btn_right_grp_new">
								<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
									<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
								</a>
							</div>   
                        <?}?>                            
                            <div class="content">
                            	<table>
                            		<?$ansIdx=1;?>
                            		<?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
                                	<tr>
                                		<td width="100"><?=$Lang['W2']['wordCount']?></td>
                                		<td>:</td>
                                		<td>
	                                	  	<input name="r_question[text][<?=$w2_m_ansCode?>]" type="text" id="textfield" class="w2_ansInput" size="40" value="<?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?>" <?=$w2_h_answerDefaultDisabledAttr?> onkeyup="checkEnableSubmitBtn();" />
	                                		<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode?>', 'text');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" /> 
	                                		<span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>"><?=$step1Data['step1WordCount']?></span>
                                		</td>
                                	</tr>
                                	<?$ansIdx++;?>
                                	<?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
                                	<tr>
                                		<td><?=$Lang['W2']['textType']?></td>
                                		<td>:</td>
                                		<td>
                                		<?foreach((array)$Lang['W2']['textTypeChoiceAry'] as $_key => $_value):?>
                                			<?$step1Data['step1TextType'] = str_replace($_key,$_value,$step1Data['step1TextType']);?>
											<input type="checkbox" name="r_question[checkbox][<?=$w2_m_ansCode?>][]" id="checkbox1" value="<?=($_key+1)?>" <?=$w2_libW2->getAnsChecked($w2_s_thisStepAns[$w2_m_ansCode], ($_key+1))?> <?=$w2_h_answerDefaultDisabledAttr?> onclick="checkEnableSubmitBtn();" /> <?=$_value?> 
                                		<?endforeach;?>
                                			&nbsp;
                                			<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode?>', 'checkbox');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" />
                                          	<span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>"><?=$step1Data['step1TextType']?></span>
                                        </td>
                                	</tr>
                                	<?$ansIdx++;?>
                                	<?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
                                	<tr>
                                		<td><?=$Lang['W2']['topicKeyword']?></td>
                                		<td>:</td>
                                		<td>
	                                	  	<input name="r_question[text][<?=$w2_m_ansCode?>]" type="text" id="textfield" class="w2_ansInput" size="40" value="<?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?>" <?=$w2_h_answerDefaultDisabledAttr?> onkeyup="checkEnableSubmitBtn();" />
	                                		<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode?>', 'text');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" /> 
	                                		<span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>"><?=$step1Data['step1TopicKeyword']?></span>
                                		</td>
                                	</tr>
                                	<?$ansIdx++;?>
                                	<?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
                                	<tr>
                                		<td><?=$Lang['W2']['keywordMeaning']?></td>
                                		<td>:</td>
                                		<td>
	                                	  	<input style="display:inline-block; vertical-align:top" name="r_question[text][<?=$w2_m_ansCode?>]" type="text" id="textfield" class="w2_ansInput" size="40" value="<?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?>" <?=$w2_h_answerDefaultDisabledAttr?> onkeyup="checkEnableSubmitBtn();" />
	                                		<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode?>', 'text');" style="vertical-align:top; display:inline-block; <?=$w2_h_checkBtnDefaultDisplayAttr?>" /> 
	                                		<span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans" style="display:inline-block; <?=$w2_h_modelAnsDefaultDisplayAttr?>"><?=$step1Data['step1KeywordMeaning']?></span>
                                		</td>
                                	</tr>
                                </table>
                                <?=$step1Data['step1Resource']?>
                          </div>
                           <p class="spacer"></p>
                           <div class="edit_bottom">
                                <?php 
								echo $h_nextStepButton;
								?>
								<?php 
								echo $h_cancelButton;
								?>								
                              </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
                        
          <!-- ********* Main  Content end ************ -->