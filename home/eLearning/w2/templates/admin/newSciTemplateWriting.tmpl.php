<?php
// using: Adam
?>
<script>
var resourceCounter= <?php echo $resourceCounter; ?>;
var unsaved=new Array(); 

<!-- Check Unsaved situation START -->
function FCKeditor_OnComplete( editorInstance )
{
	editorInstance.Events.AttachEvent( 'OnSelectionChange', checkData ) ;
}
function checkData( editorInstance )
{
	if(editorInstance.IsDirty())
	{
		var currentContentNum = $('.current').attr('data-contentNum');
		unsaved[currentContentNum]= true;

	}
}
<!-- Check Unsaved situation END -->

$(document).ready(function(){
	$( ".menuTab" ).click(function() {
        var currentContentNum = $('.current').attr('data-contentNum');
		if(doFormCheck(currentContentNum,true) || $(this).attr('data-isSaved') != "0") //data-isSaved = stepInt is not null
		{
			if(unsaved[currentContentNum] && !window.confirm("<?=$Lang['W2']['jsWarningAry']['contentNotSaved']?>"))
			{
				return false;
			}
			else
			{
				$('#w2_form')[0].reset();
				$("input[name^='step'][type='hidden']").each(function(){
					var editor = FCKeditorAPI.GetInstance( $(this).attr('id') );
					editor.SetHTML( $(this).val() );
				});
				unsaved[currentContentNum]= false;
			}
			<!-- Tab effect-->
			$(".menuTab").removeClass("current");
	        $(this).addClass("current");

			<!-- Tab content effect-->
			var contentNum = $(this).attr('data-contentNum');   
	        $(".stepContent").hide();
	        $("#content_"+contentNum).show();

	        <!-- lazy loading -->
	        var contentLoaded = $(this).attr('data-contentLoaded');
	        if(contentLoaded=='false')
	        {
	        	loadFCKEditor(contentNum);
	        }

	        $(this).attr('data-contentLoaded',true);
		}
	});
	$("span#remark_highlight_icon_big a").click(function(){
		var order = $(this).attr('id').split('_')[1];
		var step = $('.current').attr('data-contentNum');
   		var html = '<input type="text" name="r_step'+step+'RemarkTitle_'+order+'" id="r_step'+step+'RemarkTitle_'+order+'" />';
   		html += '<textarea name="r_step'+step+'RemarkContent_'+order+'" id="r_step'+step+'RemarkContent_'+order+'"></textarea>';
   		$(this).closest('span').html(html);
   		return false;
	});
	$(".deleteBtn").click(function(){
   		$(this).parent().parent().remove();
   		return false;
	});	
	$(".deleteResourceBtn").live('click', function(){
    	$(this).parent().parent().parent().remove();
   		return false;
   	});
   	$("input[name='conceptType']").change( function(){
   		if($(this).val()=='<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]?>'){
			$('#conceptMapLinkPB').addClass('frame_off');
			$('#conceptMapLink').removeClass('frame_off');
			if(dataChecker())
				newWindow('?task=editConceptMap&mod=admin&type=<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]?>&r_conceptMapId=<?=$powerConceptID?>&r_cid=<?=$cid?>', 38);
   		}else if($(this).val()=='<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?>'){
			$('#conceptMapLink').addClass('frame_off');
			$('#conceptMapLinkPB').removeClass('frame_off');
				if(dataChecker())
					newWindow('?task=editConceptMap&mod=admin&type=<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?>&r_conceptMapId=<?=$powerBoardID?>&r_cid=<?=$cid?>', 36);
   		}else{
   			$('#conceptMapLink').addClass('frame_off');
   			$('#conceptMapLinkPB').addClass('frame_off');
   		}
    	
   	});
   	
	<!-- set default value START -->
	$('#topic').keyup(function() {
		$('#topicHeader').html($('#topic').val() );
		if( $('#topic').val()=="")
		{
			var text = $('#topicHeader').attr('data-originalText');
			$('#topicHeader').html(text);
		}
    });	

	if( $('#topic').val() !='')
    {
	    $('#topicHeader').html($('#topic').val());	
    }
    <!-- set default value END -->

	var tabNum = <?=$tabNum;?>;
	if(tabNum != -1 &&tabNum != 7 )
	{
		$('#step'+(tabNum+1)+'Name').trigger('click');
	}

	<!-- Check Unsaved situation START -->
	$(":input").change(function(){ //triggers change in all input fields including text type
	    var currentContentNum = $('.current').attr('data-contentNum');
	    unsaved[currentContentNum] = true;

	});
	$(":textarea").change(function(){ //triggers change in all input fields including text type
	    var currentContentNum = $('.current').attr('data-contentNum');
	    unsaved[currentContentNum] = true;
	});
	<!-- Check Unsaved situation END -->
	if(resourceCounter==1){
		addResource();
	}
	<?php for($i=0;$i<$aCnt;$i++){?>
		highlightInputContent("<?=$stepArticleAry[$i]['infoboxCode']?>");
	<?php }?>
});
function removeParagraph(pid){
	$("#div_paragraph_"+pid).remove();
}
function dataChecker()
{
	var currentContentNum = $('.current').attr('data-contentNum');

	if(unsaved[currentContentNum] && !window.confirm("<?=$Lang['W2']['jsWarningAry']['contentNotSaved']?>"))
	{
		return false;
	}
	else
	{
		return true;
	}
}
function loadFCKEditor(contentNum)
{
	switch(contentNum)
	{
		case '1':
			$('#step1Editor_1').append(<?="'".$step1InstructionEditor."'"?>);
		break;
		case '2':
			$('#step2Editor_1').append(<?="'".$step2InstructionEditor."'"?>);
		break;
		case '3':
			$('#step3Editor_1').append(<?="'".$step3InstructionEditor."'"?>);
		break;
		case '4':
			$('#step4Editor_1').append(<?="'".$step4InstructionEditor."'"?>);
		break;
		case '5':
			$('#step5Editor_1').append(<?="'".$step5InstructionEditor."'"?>);
		break;		
		case '6':
			$('#step6Editor_1').append(<?="'".$step6InstructionEditor."'"?>);
			$('#step6Editor_2').append(<?="'".$step6DefaultWritingEditor."'"?>);
		break;
		default:
		break;
		$(".textbox").live('change', function(){
	    	var parentDiv = $(this).parent().parent().parent();
	   		parentDiv.remove();
		});		
	}
}
function addStepQuestionField(name,withAns){
	var qCnt = parseInt($('#Cnt_'+name).val());
	var html = '';
	if(qCnt < <?=$w2_cfg['contentInput']['maxInputQNum']?>){
		html +='<div id="div_'+name+'[]">';
			html +='<span style="float:left;width:80px;"><?=$Lang['W2']['question']?>: </span> <textarea name="'+name+'Question[]" rows="3" class="'+name+' textbox" style="width:70%; float:left"></textarea>';
			html +='<span class="table_row_tool"><a href="javascript:void(0);" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="<?=$Lang['Btn']['Delete']?>">&nbsp;</a></span>';
			html +='<br style="clear:both;">';
		if(withAns){
			html +='<span style="float:left;width:80px;"><?=$Lang['W2']['answer']?>: </span> <textarea name="'+name+'Answer[]" rows="3" class="'+name+' textbox" style="width:70%; float:left"></textarea>';
			html +='<br style="clear:both;">';
			html +='<br style="clear:both;">';	
		}		
		html +='</div>';	
		$( "#"+name+"Array" ).append( html );
	
		$(".deleteBtn").live('click', function(){
	    	$(this).parent().parent().remove();
	    	var cnt = parseInt($('#Cnt_'+name).val());
	    	$('#Cnt_'+name).val(cnt-1);
	    	$('#'+name+'AddMore').show();
	    	return false;
		});
		$(":input").live('change', function(){ //trigers change in all input fields including text type
		    var currentContentNum = $('.current').attr('data-contentNum');
		    unsaved[currentContentNum] = true;
		});
		qCnt++;
		$('#Cnt_'+name).val(qCnt);
		if(qCnt==<?=$w2_cfg['contentInput']['maxInputQNum']?>){
			$('#'+name+'AddMore').hide();
		}
	}
	
}

function addResource()
{
	var resourceHTML= '';
	resourceHTML += "<tr class='resourceArray'>";
	resourceHTML += '<td>';
	resourceHTML += '</td>';
	resourceHTML += '<td>';
	resourceHTML += '</td>';
	resourceHTML += '<td>';
	resourceHTML += "<input type='text' name='resource[]' class='resource' style='width:95%' id='resource"+resourceCounter+"' data-resourceID ='"+resourceCounter+"' >";
	resourceHTML += '</td>';
	resourceHTML += '<td>';
	resourceHTML += "<input type='text' name='website[]' class='webiste' style='width:100%' id='website"+resourceCounter+"' data-resourceID ='"+resourceCounter+"' >";
	resourceHTML += '</td>';
	resourceHTML += '<td>';
	resourceHTML += "<span class='table_row_tool'><a href='#' class='deletResourceBtn tool_delete_dim' title='Delete'>&nbsp;</a></span>";
	resourceHTML += '</td>';
	
	resourceHTML += '<td>';
	for (var i=0; i<5; ++i)
	{
									
		resourceHTML += '<label for="step'+(i+1)+'">'+(i+1)+'</label><input type="checkbox" id="step'+(i+1)+'" name="r'+resourceCounter+'_step'+(i+1)+'" class="step"/>&nbsp;&nbsp;';
	}
	resourceHTML += '</td>';
	resourceHTML += '</tr>';
	
	$( ".resourceArray:last" ).after( resourceHTML );

	$(".deletResourceBtn").live('click', function(){
    	var parentDiv = $(this).parent().parent().parent();
   		parentDiv.remove();
	});
	++resourceCounter;
	
	$(":input").live('change', function(){ //trigers change in all input fields including text type
	    var currentContentNum = $('.current').attr('data-contentNum');
	    unsaved[currentContentNum] = true;
	});
}
function doFormCheck(tabNum,checkOnly) 
{	
	if(tabNum == -1)
	{
		tabNum = $('.current').attr('data-contentNum');
	}
	
	var checkOnly = checkOnly || false;
	var canSubmit = true;
	switch(tabNum)
	{
		case '0':
			canSubmit = validGeneralInfo();
			break;
		case '1':
			canSubmit = validStep1();
			break;
		case '2':
			canSubmit = validStep2();
			break;
		case '3':
			canSubmit = validStep3();
			break;
		case '4':
			canSubmit = validStep4();
			break;
		case '5':
			canSubmit = validStep5();
			break;
		case '6':
			canSubmit = validStep6();
			break;		
		case '7':
			canSubmit = validStep7();
			break;					
		default:
			break;
	}
	
	if( !checkOnly )
	{
		if (canSubmit ) {
			$('input#saveTab').val(tabNum);	
	    	$('input#mod').val('admin');
			$('input#task').val('saveSciTemplate');
			$('form#w2_form').submit();
		    
		}
	}
	else
	{
		return canSubmit;
	}
}

function goToNextStep(){
	var tabNum = $('.current').attr('data-contentNum');
	var _url = '/home/eLearning/w2/index.php?cid=<?=$cid?>&mod=admin&task=newLsEngTemplateWriting&r_contentCode=<?=$r_contentCode?>&r_returnMsgKey=AddSuccess&tabNum='+tabNum;
	window.location = _url;	
}
function doCancel()
{
	 var currentContentNum = $('.current').attr('data-contentNum');
	if(unsaved[currentContentNum] && !window.confirm("<?=$Lang['W2']['jsWarningAry']['contentNotSaved']?>"))
	{
		return false;
	}
	else
	{
		$('#w2_form')[0].reset();
		unsaved[currentContentNum]= false;
	}
	//if(savedReminder())
	location.href="/home/eLearning/w2/index.php?task=templateManagement&mod=admin&r_contentCode=<?php echo $r_contentCode ?>";
}
function validGeneralInfo()
{
	var isValid = 1;
	$(".warningDiv_0").hide();	
	if($('#topic').val() =='')
	{
		$("#r_TopicName_warningDiv").show();
		isValid = 0;
	}
	if($('#topicIntro').val() =='')
	{
		$("#r_TopicIntro_warningDiv").show();
		isValid = 0;
	}
	if($('#category').val()=='')
	{
		$("#r_SelectedCategory_warningDiv").show();				
		isValid = 0;
	}
	return isValid;
}

function validStep1()
{
	var isValid = 1;
	$(".warningDiv_1").hide();	
	
	var editor = FCKeditorAPI.GetInstance('step1Int');
	if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
	{	
		$("#r_Step1Int_warningDiv").show();
		isValid = 0;
	}
	if($('#step1Purpose').val() =='')
	{
		$("#r_Step1Purpose_warningDiv").show();
		isValid = 0;
	}	
	if($('#step1Audience').val() =='')
	{
		$("#r_Step1AudienceWarning_warningDiv").show();
		isValid = 0;
	}
	if($('#step1TeacherVocab').val() =='')
	{
		$("#r_Step1TeacherVocabWarning_warningDiv").show();
		isValid = 0;
	}
	return isValid;
}

function validStep2()
{
	var isValid = 1;
	$(".warningDiv_2").hide();	
	var editor = FCKeditorAPI.GetInstance('step2Int');
	if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
	{	
		$("#r_Step2Int_warningDiv").show();
		isValid = 0;
	}
	var articleCnt = $("#Cnt_step2Article").val();
	for(var i=0 ;i < articleCnt;i++){
		$("div.step2_"+i+"WritingSample").each(function(){
			var id = $(this).attr("id");
			if(id!=''){
				var idx = id.split('_')[4];
				editor = FCKeditorAPI.GetInstance("r_step2_"+i+"editorParagraph"+idx);
				if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
				{	
					$("#r_Step2_"+i+"Paragraph_warningDiv"+idx).show();
					isValid = 0;
				}
			}		
		});	
	}

	return isValid;
}

function validStep3()
{
	var isValid = 1;
	
	$(".warningDiv_3").hide();	
	var editor = FCKeditorAPI.GetInstance('step3Int');
	if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
	{	
		$("#r_Step3Int_warningDiv").show();
		isValid = 0;
	}
	return isValid;
}

function validStep4()
{
	var isValid = 1;
	
	$(".warningDiv_4").hide();
	var editor = FCKeditorAPI.GetInstance('step4Int');
	if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
	{	
		$("#r_Step4Int_warningDiv").show();
		isValid = 0;
	}
	$("div.step4WritingSample").each(function(){
		var id = $(this).attr("id");
		if(id!=''){
			var idx = id.split('_')[3];
			editor = FCKeditorAPI.GetInstance('r_step4editorParagraph'+idx);
			if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
			{	
				$("#r_Step4Paragraph_warningDiv"+idx).show();
				isValid = 0;
			}
		}		
	});
	
	return isValid;

}

function validStep5()
{
	var isValid = 1;
	
	$(".warningDiv_5").hide();
	var editor = FCKeditorAPI.GetInstance('step5Int');
	if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
	{	
		$("#r_Step5Int_warningDiv").show();
		isValid = 0;
	}
	$("textarea[name='step5Outline[]'").each(function(i){
	
		if($(this).val()==''||$("textarea[name='step5Outline[]']").get(i).value==''){
			$("#r_Step5Outline_warningDiv").show();
			isValid = 0;
		}
	});
	
	return isValid;

}
function validStep6()
{
	var isValid = 1;
	$(".warningDiv_6").hide();	
	var editor = FCKeditorAPI.GetInstance('step6Int');
	if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
	{
		$("#r_Step6Int_warningDiv").show();
		isValid = 0;
	}

	editor = FCKeditorAPI.GetInstance('step6DefaultWriting');
	if( editor.GetHTML() =='&nbsp;' || editor.GetHTML() =='<br />')
	{
		$("#r_Step6DefaultWriting_warningDiv").show();
		isValid = 0;
	}

	return isValid;
}
function validStep7(){
	var isValid = 1;
	$(".warningDiv_7").hide();
	$("input[name='resource[]'").each(function(i){
		if($(this).val()==''||$("input[name='resource[]']").get(i).value==''){
			$("#r_Step7WebName_warningDiv").show();
			isValid = 0;
		}
	});
	$("input[name='website[]'").each(function(i){
		if($(this).val()==''||$("input[name='website[]']").get(i).value==''){
			$("#r_Step7Website_warningDiv").show();
			isValid = 0;
		}
	});	
	return isValid;	
}

function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'w2_form');
}

function loadHighlightedWordsByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_highlightedWords', filePath, headerHtml, extraParam, '' , 'w2_form');
}
function loadInputRefCategoryByThickbox(headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_inputRefGroup', '', headerHtml, extraParam, 'initEditRefCategoryThickbox();' , 'w2_form');
}
function reloadInputRefDisplay(infoboxCode) {
	$("#w2_form input[id=mod]").val("ajax");
	$("#w2_form input[id=task]").val("reloadInputRefDisplay");
	
	var refDivId = 'refDiv_' + infoboxCode;
	
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#w2_form").serialize() + '&r_infoboxCode=' + infoboxCode,
		success:  function(xml){
						highlightInputContent(infoboxCode);
						$('div#' + refDivId).html(xml);
						initThickBox();
				  }
	});
}
function highlightInputContent(infoboxCode) {
	infoboxCode = infoboxCode || '';
	
	$("#w2_form input[id=mod]").val("ajax");
	$("#w2_form input[id=task]").val("getInputHighlightJson");
	
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#w2_form").serialize() + '&r_infoboxCode=' + infoboxCode,
		success:  function(json){
			//var highlightObj = JSON.parse(json); 		// IE does not have JSON by default
			if (typeof (JSON) == 'undefined') {
			    eval("var highlightObj = " + json);
			}
			else {
			    var highlightObj = JSON.parse(json);
			}
								
			$.each(highlightObj, function(_infoboxCode, _hightlightAssoAry) {
				if (infoboxCode != '' && _infoboxCode != infoboxCode) {
					return;
				}
				
				var essayDivId = 'w2_infoboxEssayDiv_' + _infoboxCode;
				
				// unhighlight all items first
				$.each(_hightlightAssoAry, function(__css, __itemTitleAry) {
					$("div#" + essayDivId).unhighlight({ className: __css, element: 'span' }); 
				});
				
				// highlight the items now
				$.each(_hightlightAssoAry, function(__css, __itemTitleAry) {
					$("div#" + essayDivId).highlight(__itemTitleAry, { wordsOnly: true, className: __css, element: 'span', caseSensitive: false }); 
				});
			});
		  }
	});
}


function deleteInputRefItem(confirmMsg, refItemId, infoboxCode) {
	$('#mod').val("ajax");
	$('#task').val("deleteInputRefItem");
	
	if (confirm(confirmMsg)) {
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#w2_form").serialize() + '&r_refItemId=' + refItemId,
			success:  function(xml){
							reloadInputRefDisplay(infoboxCode);
					  }
		});
	}
}
function saveInputRefItem(newRefDivId, textboxId, refCategoryType, refCategoryCode, infoboxCode, refItemId) {
	refItemId = refItemId || '';
	
	$('#mod').val("ajax");
	$('#task').val("saveInputRefItem");
	
	var refItemTitle = Trim($('input#' + textboxId).val());
	
	$('div#' + newRefDivId).html(getAjaxLoadingMsg());
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#w2_form").serialize() + '&r_refItemTitle=' + refItemTitle + '&r_refCategoryType=' + refCategoryType + '&r_refCategoryCode=' + refCategoryCode + '&r_infoboxCode=' + infoboxCode + '&r_refItemId=' + refItemId,
		success:  function(xml){
						reloadInputRefDisplay(infoboxCode);
				  }
	});
}
function removeStepItemBox(divID){
	if(document.getElementById(divID) != undefined){
		$('#'+divID).remove();
		var currentContentNum = $('.current').attr('data-contentNum');
	    unsaved[currentContentNum] = true;
	}
}
function removeTeacherAttachment(fid){
	var answer = confirm("<?=$Lang['W2']['jsWarningAry']['deleteAttachment']?>");
	var cid = $('#cid').val();
	var sid = $('#step').val();
	var r_contentCode = $('#r_contentCode').val();	
	if (answer){
		$.ajax({
				url:      "/home/eLearning/w2/index.php",
				type:     "POST",
				data:     "r_fid="+fid+"&r_cid="+cid+"&r_sid="+sid+"&r_contentCode="+r_contentCode+"&task=deleteTeacherAttachmentFile&mod=common",
				error:    function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success:  function(xml){
							$("#w2_teacher_attachment_table").html(xml);
						  }
		  });
	}

}
function addStepArticle(step){	
	var qCnt = parseInt($('#Cnt_step'+step+'Article').val()); 
	var html = '';
	var stepId = step+'_'+qCnt;
	if(qCnt < <?=$w2_cfg['contentInput']['maxInputQNum']?> ){
		html += '<div>';
			html += '<div id="div_step'+stepId+'Article[]" class="infobox" style="width:80%;line-height: 20px;">';	
				html += '<a class="top" href="javascript:void(0);"><?=$Lang['W2']['article']?> '+qCnt+'</a>';
				html += '<div class="infobox_content">';
					html += '<div class="content_input_sample" style="padding-right:0px;">';
		          	 html += '<div class="content_input_sample_left">';
						html += '<div class="step'+stepId+'WritingSample content_input_main_grp" style="margin-bottom:0">';
			        		html += '<div class="content_input_main_tag">';
			            		html += '<h1><?=$Lang['W2']['structure']?></h1>';
			                html += '</div>';
			                html += '<div class="content_input_main">';
			                	html += '<h1><?=$Lang['W2']['paragraph']?></h1>';
			                html += '</div>';
		                    html += '<br style="clear:both" />';
		                html += '</div>';	
 						html += '<div id="Step'+stepId+'_Paragraph_Div">';
 							html += '<input type="hidden" name="r_step'+stepId+'ParagraphCounter" id="r_step'+stepId+'ParagraphCounter" value="0">';
 						html += '</div>';
		                    html += '<div class="content_input_main_grp">';
		                    	html += '<div class="content_input_main_tag"></div>';
		                        html += '<div class="content_input_main Content_tool_190"><a href="javascript:void(0);" class="new" onclick="addNewParagraph(\''+stepId+'\');"><?=$Lang['W2']['newParagraph']?></a></div>';
		                      	html += '<br style="clear:both" />';
		                    html += '</div>';
		                html += '</div>';	
					html += '</div>';
				html += '</div>';
			html += '</div>';
			html += '<span class="table_row_tool"><a href="javascript:void(0);" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="<?=$Lang['Btn']['Delete']?>">&nbsp;</a></span>';
		html +=  '</div>';
		$( "#step"+step+"ArticleArray" ).append( html );	 
		$(".deleteBtn").live('click', function(){
	    	$(this).parent().parent().remove();
	    	var cnt = parseInt($('#Cnt_step'+step+'Article').val());
	    	$('#Cnt_step'+step+'Article').val(cnt-1);
	    	$('#step'+step+'ArticleAddMore').show();
	    	return false;
		});
		$(":input").live('change', function(){ //trigers change in all input fields including text type
		    var currentContentNum = $('.current').attr('data-contentNum');
		    unsaved[currentContentNum] = true;
		});
		qCnt++;
		$('#Cnt_step'+step+'Article').val(qCnt);
		if(qCnt==<?=$w2_cfg['contentInput']['maxInputQNum']?>){
			$('#step'+step+'ArticleAddMore').hide();
		}  
		addNewParagraph(stepId);            	          	 
	}
	
}
function removeAttachmentBox(divID){
	if(document.getElementById(divID) != undefined){
		$('#'+divID).remove();
	}
}
function addHighlightWordRow(type){

	var tableObj = $('#w2_highlightword_'+type+'_table');
	var cntObj = $('#w2_highlightword_'+type+'_counter');
	var cnt = cntObj.val();
			
	var html = '<tr class="vocab_group" id="w2_highlightword_'+type+'_'+cnt+'">';
		html += '<td class="title" width="20%"><input class="highlightword" type="text" name="'+type+'_word_'+cnt+'" id="'+type+'_word_'+cnt+'" style="width:150px;" value=""/></td>';
		html += '<td width="1%">:</td>';
		html += '<td width="74%"><textarea class="highlightword" name="'+type+'_meaning_'+cnt+'" id="'+type+'_meaning_'+cnt+'"></textarea></td>';
		html += '<td class="table_row_tool" width="5%"><a href="javaScript:void(0)" class="tool_delete_dim" title="<?=$Lang['Btn']['Delete']?>" onclick="removeAttachmentBox(\'w2_highlightword_'+type+'_'+cnt+'\')">&nbsp;</a></td>';
		html += '</tr>';
	tableObj.append(html);
	var new_cnt = parseInt(cnt) + parseInt(1);
	cntObj.val(new_cnt);

}
function addAttachmentBox(){

	var tableObj = $('#teacher_attachment_table');
	var cntObj = $('#attachmentBox_counter');
	var cnt = cntObj.val();
			
	var html = '<tr id="w2_uploadFileId_'+cnt+'">';
		html += '<td><img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/w2/icon_attachment2.gif"></td>';
		html += '<td><input type="text" name="r_tattname_'+cnt+'" id="r_tattname_'+cnt+'" style="width:250px"/></td>';
		html += '<td><input type="file" name="r_tatt_'+cnt+'" id="r_tatt_'+cnt+'" style="width:190px"/></td>';
		html += '<td class="table_row_tool"><a href="javaScript:void(0)" class="tool_delete_dim" title="<?=$Lang['Btn']['Delete']?>" onclick="removeAttachmentBox(\'w2_uploadFileId_'+cnt+'\')">&nbsp;</a></td>';
		html += '</tr>';
	tableObj.append(html);
	var new_cnt = parseInt(cnt) + parseInt(1);
	cntObj.val(new_cnt);

}
function validTeacherAttachment()
{
	var isValid = 1;
	var step = $("#thickboxForm input[id=step]").val();
	$(".warningDiv_"+step).hide();	
	$('.teacherAttachment').each(function(){
		if($(this).val()==''){
			$("#r_teacherAttachment_warningDiv").show();
			isValid = 0;
		}
	});	
	return isValid;
}
function uploadTeacherAttachmentFile(){
	if(validTeacherAttachment()){
		$("#thickboxForm input[id=mod]").val("common");
		$("#thickboxForm input[id=task]").val("uploadTeacherAttachmentFile");
	
		$('#uploadFileIFrame').attr('src', "/home/eLearning/w2/index.php");
		$('#thickboxForm').attr('method', 'post').attr('target', 'uploadFileIFrame').submit();
	}
}
function refreshTeacherAttachmentFilePanel(){
	$("#thickboxForm input[id=mod]").val("ajax");
	$("#thickboxForm input[id=task]").val("reloadTeacherAttachmentFilePanel");

		$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#thickboxForm").serialize(),
		error:	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					$("#w2_teacher_attachment_table").html(xml);
				  }
	});

}
function addNewParagraph(step){
	step = step || '';
	var cnt = $("#r_step"+step+"ParagraphCounter").val();
	var r_contentCode = $('#r_contentCode').val();
	var new_cnt = parseInt(cnt) + parseInt(1);
		$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     "r_pid="+new_cnt+"&r_contentCode="+r_contentCode+"&task=addSampleWritingParagraph&mod=ajax&step="+step,
		error:	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(html){
					$("#Step"+step+"_Paragraph_Div").append(html);
					$("#r_step"+step+"ParagraphCounter").val(new_cnt);
				  }
	});	
}

/* *** Start of highlight *** */
function GetSelectedText_Highlight(oFCKeditor, color){
	if(oFCKeditor.EditorWindow.getSelection){
		// work in Chrome or FF
		var selection = oFCKeditor.EditorWindow.getSelection();
	} else {
		// work in IE
		var selection = oFCKeditor.EditorDocument.selection;	
	}
	
	//var range = selection.createRange();
	var range = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
	if(selection=='') return;
	var st = 0;
	var en = selection.rangeCount-1;
	highlight_text(range, selection, st,en);
	
	// output
	var ra = selection.getRangeAt(0);

	var newNode = document.createElement("span");
	var timestamp = new Date().getTime();
	newNode.className = 'editor_highlight highlight_'+color;
	$(newNode).attr('name','span_'+timestamp);
	newNode.appendChild(ra.extractContents()); 
	ra.insertNode(newNode);
}
function GetSelectedText_UnHighlight(oFCKeditor, color){
	if(oFCKeditor.EditorWindow.getSelection){
		// work in Chrome or FF
		var selection = oFCKeditor.EditorWindow.getSelection();
	} else {
		// work in IE
		var selection = oFCKeditor.EditorDocument.selection;	
	}
	var range = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
	
	var this_nodes = getSelectedNodes(selection);
	var t,f,n;
	var span_names = [];
	var node_idx = [];
	 for(var i=0;i<this_nodes.length;i++){
	 	var node_parent_ary = []; 
	 	var have_match = 0;
		t = this_nodes[i];
		
		$(t).parents("span.editor_highlight").each(function(){			
			n = $(this).attr('name');
			f = jQuery.inArray(n, node_parent_ary);
			if (f<0) {
				node_parent_ary.push(n);
			}	
		});
		if(node_parent_ary.length>0){
			for(var j=0;j<node_parent_ary.length;j++){ 
				f = jQuery.inArray(node_parent_ary[j], span_names);
				if(f>=0){
					have_match = 1;
				}
			}
			if(have_match && node_parent_ary.length>span_names.length){
				node_idx[node_idx.length-1] = i;
			}else if(!have_match){
				node_idx.push(i);
			}
			span_names = node_parent_ary;
		}
	 }
	
	 for(var i=0;i<node_idx.length;i++){ 
		$(this_nodes[node_idx[i]]).parents("span.editor_highlight").each(function(){
			n = $(this).attr('name');
			 $(this).parents("body").find("span[name='"+n+"']").each(function(){
				$(this).replaceWith($(this).html());
	 		});	
		});
	}

}
var pos = 0;

function dig(el){
    $(el).contents().each(function(i,e){
        if (e.nodeType==1){
            // not a textnode
         dig(e);   
        }else{
            if (pos<start){
               if (pos+e.length>=start){
                range.setStart(e, start-pos);
               }
            }
            
            if (pos<end){
               if (pos+e.length>=end){
                range.setEnd(e, end-pos);
               }
            }            
            
            pos = pos+e.length;
        }
    });  
}
var start,end, range;

function highlight_text(range, selection, st,en){
//    range = document.createRange();
	var clonedSelection = range.cloneContents();;
        
    var div = document.createElement('div');
    div.appendChild(clonedSelection);
    start = st;
    end = en;
    dig(div);
    selection.addRange(range);
}
function nextNode(node) {
    if (node.hasChildNodes()) {
        return node.firstChild;
    } else {
        while (node && !node.nextSibling) {
            node = node.parentNode;
        }
        if (!node) {
            return null;
        }
        return node.nextSibling;
    }
}

function getRangeSelectedNodes(range) {
    var node = range.startContainer;
    var endNode = range.endContainer;

    // Special case for a range that is contained within a single node
    if (node == endNode) {
        return [node];
    }

    // Iterate nodes until we hit the end container
    var rangeNodes = [];
    while (node && node != endNode) {
        rangeNodes.push( node = nextNode(node) );
    }

    // Add partially selected nodes at the start of the range
    node = range.startContainer;
    while (node && node != range.commonAncestorContainer) {
        rangeNodes.unshift(node);
        node = node.parentNode;
    }

    return rangeNodes;
}

function getSelectedNodes(selection) {
    if (!selection.isCollapsed) {
        return getRangeSelectedNodes(selection.getRangeAt(0));
    }
    return [];
}
/* *** End of highlight *** */
</script>

<!--
<h3><span id="topicIntroHeader" data-originalText="(TOPIC INTRODUCTION)">(TOPIC INTRODUCTION)</span></h3>
-->	

<div class="subtab_top_left">
	<div class="subtab_top_right">
		<div class="subtab_menu">
			<a id="step0Name" class="menuTab current" data-contentNum="0" data-contentFilled="false" data-isSaved="<?=!empty($parseData['topicName'])?!empty($parseData['topicName']):0; ?>"><div><?=$Lang['W2']['topicAndTheme_eng']?></div></a>
			<a id="step1Name" class="menuTab" data-contentNum="1" data-contentLoaded="false" data-isSaved="<?=!empty($parseData['step1Data']['step1Int'])?!empty($parseData['step1Data']['step1Int']):0; ?>"><div><?=$Lang['W2']['step1_'.$r_contentCode]?></div></a>
			<a id="step2Name" class="menuTab" data-contentNum="2" data-contentLoaded="false" data-isSaved="<?=!empty($parseData['step2Data']['step2Int'])?!empty($parseData['step2Data']['step2Int']):0; ?>"><div><?=$Lang['W2']['step2_'.$r_contentCode]?></div></a>
			<a id="step3Name" class="menuTab" data-contentNum="3" data-contentLoaded="false" data-isSaved="<?=!empty($parseData['step3Data']['step3Int'])?!empty($parseData['step3Data']['step3Int']):0; ?>"><div><?=$Lang['W2']['step3_'.$r_contentCode]?></div></a>
			<a id="step4Name" class="menuTab" data-contentNum="4" data-contentLoaded="false" data-isSaved="<?=!empty($parseData['step4Data']['step4Int'])?!empty($parseData['step4Data']['step4Int']):0; ?>"><div><?=$Lang['W2']['step4_'.$r_contentCode]?></div></a>
			<a id="step5Name" class="menuTab" data-contentNum="5" data-contentLoaded="false" data-isSaved="<?=!empty($parseData['step5Data']['step5Int'])?!empty($parseData['step5Data']['step5Int']):0; ?>"><div><?=$Lang['W2']['step5_'.$r_contentCode]?></div></a>
			<a id="step6Name" class="menuTab" data-contentNum="6" data-contentLoaded="false" data-isSaved="<?=!empty($parseData['step5Data']['step6Int'])?!empty($parseData['step6Data']['step6Int']):0; ?>"><div><?=$Lang['W2']['step6_'.$r_contentCode]?></div></a>
			<a id="step7Name" class="menuTab" data-contentNum="7" data-contentLoaded="false" data-isSaved="<?=!empty($parseData['resource'])?!empty($parseData['resource']):0; ?>"><div><?=$Lang['W2']['resource']?></div></a>
		</div>
	</div>
</div>

<!-- content -->
<div class="write_board_left"><div class="write_board_right">
    <div class="content_input">
    		<!-- General Info -->
				<div id="content_0" class="stepContent"><table style="width:100%">
					<col width="150px" />
                    <col width="10px" />

					<!-- Topic -->
					<tbody>
						<tr>
							<th><?=$Lang['W2']['topic']?></th>
							<td>&nbsp;</td>
							<td>
								<span class="theme_icon"></span>
								<input type="text" name="topic" id="topic" size="80" maxlength='80' placeholder="<?=$Lang['W2']['topic']?>" value="<?=$parseData['topicName']?>" required/>
								<?=$h_inputTopicNameWarning?>
							</td>
						</tr>
							
						<tr class="instruction">
							<td class="instruction"><?=$Lang['W2']['introduction']?></td>
							<td class="instruction"></td>
							<td class="instruction">
								<textarea name="topicIntro" id="topicIntro" rows="4" style="resize:none" placeholder="<?=$Lang['W2']['introduction']?>" required ><?=$parseData['topicIntro']?></textarea>
								<?=$h_inputTopicIntroWarning?>
							</td>
						</tr>

						<tr>
							<th><?=$Lang['W2']['textType']?></th>
							<td>:</td>
							<td><?=$textTypeSelectionBoxItems?>
								<?=$h_selectCategoryWarning?>
							</td>
						</tr>
					<tr><th><?=$Lang['W2']['grade']?></th>
						<td>:</td>
                        <td><?=$h_gradeSelection?></td>
                     </tr>					
					</tbody>
				</table>				
				</div>
    	<!-- General Info End-->
				
				<!-- Step 1 -->
				<div id="content_1" class="stepContent" style="display:none;"><table style="width:100%">
					<col width="150px" />
                    <col width="10px" />
				<tbody>
					<tr class="instruction">
						<td class="instruction"><?=$Lang['W2']['instruction']?></td>
						<td class="instruction"></td>
						<td class="instruction">
							<div id="step1Editor_1" class="fckeditor"></div>
							<?=$h_inputStep1IntWarning?>
						</td>
					</tr>				
                    <tr>
						<td>
							<div class="subtitle_new">
								<span><?=$Lang['W2']['topicAnalysis']?></span>
							</div>
							<!-- New class 20140113 -->
						</td>
						<td>&nbsp;</td>
						<td class="input_row2">
							<div class="btn_right_grp_new" style="margin-bottom:-25px;">
								<!-- New class 20140113 -->
								<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
									<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
								</a>							
								<!-- New class 20140113 -->
							</div>
						</td>
					</tr>	
					<tr>
						<th><?=$Lang['W2']['purpose']?></th>
						<td>:</td>
						<td>
							<?=$h_selectStep1PurposeHTML?>
							<?=$h_selectStep1PurposeWarning?>
						</td>
					</tr>
					<tr>
						<th><?=$Lang['W2']['audience']?></th>
						<td>:</td>
						<td>
							<input type="text" name="step1Audience" id="step1Audience" size="80" value="<?=$parseData['step1Data']['step1Audience']?>" />
							<?=$h_inputStep1AudienceWarning?>
						</td>
					</tr>	
					<tr>
						<th valign="top"><?=$Lang['W2']['vocabSuggestedByTeacher']?></th>
						<td>:</td>
						<td><?=$Lang['W2']['pressEnterToSeparateVocab']?><br/>
							<textarea name="step1TeacherVocab" id="step1TeacherVocab" rows="5" style="width:60%"><?=(!empty($parseData['step1Data']['step1TeacherVocab'])?stripslashes($parseData['step1Data']['step1TeacherVocab']):'')?></textarea>
							<?=$h_inputStep1TeacherVocabWarning?>
						</td>
					</tr>						
				</tbody>			
			
				</table>
				</div>
				<!-- Step 1 END-->
				
				<!-- Step 2 -->
				<div id="content_2" class="stepContent" style="display:none;"><table style="width:100%">
						<col width="150px" />
                 	   <col width="10px" />
						<tbody>
							<tr class="instruction">
								<td class="instruction"><?=$Lang['W2']['instruction']?></td>
								<td class="instruction"></td>
								<td class="instruction">
									<div id="step2Editor_1"></div>
									<?=$h_inputStep2IntWarning?>
								</td>
							</tr>
							<tr>
								<td>
									<div class="subtitle_new">
										<span><?=$Lang['W2']['referenceMaterials']?></span>
									</div>	
								</td>
								<td></td>
								<td class="input_row2">
									<div class="btn_right_grp_new" style="margin-bottom:-25px;">
										<!-- New class 20140113 -->
										<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
											<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
										</a>
										<!-- New class 20140113 -->
									</div>
									<div id="step2ArticleArray">
										<?=$h_step2ArticleHTML?>
									</div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td class="Content_tool_190">
									<?=$h_step2AddMoreButton?>
								</td>
							</tr>	
						</tbody>
				</table>
				<iframe id="uploadFileIFrame" name="uploadFileIFrame" style="width:100%;height:300px;display:none;"></iframe>
				</div>
				<!-- Step 2 END-->
				
				<!-- Step 3 -->
				
				<div id="content_3" class="stepContent" style="display:none;">
					<table style="width:100%">
						<col width="150px" />
                        <col width="10px" />
						<tr class="instruction">
							<td class="instruction"><?=$Lang['W2']['instruction']?></td>
							<td class="instruction"></td>
							<td class="instruction">
								<div id="step3Editor_1"></div>
								<?=$h_inputStep3IntWarning?>
							</td>
						</tr>
						<tr>
							<td>
								<div class="subtitle_new">
									<span><?=$Lang['W2']['referenceMaterials']?></span>
								</div>	
							</td>
							<td></td>
							<td class="input_row2">
								<div class="btn_right_grp_new" style="margin-bottom:-25px;">
						       		<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
										<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
									</a>
						      	</div>
						      	<span style="color:#8438b0;font-weight:bold;"><?=$Lang['W2']['sciHighlightInstruction']?></span>
						      	<?=$h_step3highlightArticleHTML?>
                                </td>
                           </tr>
						<tr>
							<td>
								<div class="subtitle_new">
									<span><?=$Lang['W2']['conceptMap']?></span>
								</div>	
							</td>
							<td></td>
							<td class="input_row2">
									<!-- <div class="btn_right_grp"><a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper(($Lang['W2']['teacherAttachment'])))?></h1>','');"><span><?=$Lang['W2']['teacherAttachment']?></span></a></div> -->
									<div class="conceptmap_choice"><input type="radio" name="conceptType" id="conceptType_1" value="<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]?>" <?=$powerConceptCheck?>> <label for="conceptType_1"><?=$Lang['W2']['powerConcept']?></label><br /><a href="javaScript:void(0)" OnClick="javascript:if(dataChecker() )newWindow('?task=editConceptMap&mod=admin&type=<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]?>&r_conceptMapId=<?=$powerConceptID?>&r_cid=<?=$cid?>', 38)" id="conceptMapLink" class="conceptmap_frame_choice <?=($powerConceptCheck?'':'frame_off')?>"><div><span><?=$Lang['W2']['powerConcept']?></span></div></a></div>
									<div class="conceptmap_choice"><input type="radio" name="conceptType" id="conceptType_2" value="<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?>" <?=$powerBoardCheck?>> <label for="conceptType_2"><?=$Lang['W2']['powerBoard']?></label><br /><a href="javaScript:void(0)" OnClick="javascript:if(dataChecker() )newWindow('?task=editConceptMap&mod=admin&type=<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?>&r_conceptMapId=<?=$powerBoardID?>&r_cid=<?=$cid?>', 36)" id="conceptMapLinkPB" class="conceptmap_frame_choice <?=($powerBoardCheck?'':'frame_off')?>"><div><span><?=$Lang['W2']['powerBoard']?></span></div></a></div>
									<div class="conceptmap_choice"><input type="radio" name="conceptType" id="conceptType_3" value="<?=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]?>" <?=$attachmentCheck?>> <label for="conceptType_3"><?=$Lang['W2']['attachment']?></label></div>
                                </td>
                           </tr>									
                    </table>
				</div>
				<!-- Step 3 END-->
				
				<!-- Step 4 -->				
				<div id="content_4" class="stepContent" style="display:none;"><table style="width:100%">
					<col width="150px" />
                    <col width="10px" />
					<tr class="instruction">
						<th class="instruction">
							<?=$Lang['W2']['instruction']?>
						</th>
						<td class="instruction"></td>						
						<td class="instruction">
							<div id="step4Editor_1"></div>		
							<?=$h_inputStep4IntWarning?>
						</td>
					</tr>
					<tr>
						<td>
							<div class="subtitle_new">
								<span><?=$Lang['W2']['writingSample']?></span>
							</div>	
						</td>
						<td></td>
						<td class="input_row2">
							 <div class="btn_right_grp_new" style="margin-bottom:-25px;">
		                    	<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref_new" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');"><span><?=$Lang['W2']['teacherAttachmentWithBreak']?></span></a>
								<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadHighlightedWordsByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['highlightedWordsPhrases']))?></h1>','');" class="thickbox btn_ref_new">
									<span><?php echo $Lang['W2']['highlightedWordsPhrasesWithBreak']?></span>
								</a>	
		                  	</div>
		                </td>
		             <tr>
		             	<td colspan="3">
		                  	<br style="clear:both" />
                    		<div class="content_input_sample">
		                    	<div class="content_input_sample_left">
		                    	<!-- Group #0 -->
		                        	<div class="content_input_main_grp" style="margin-bottom:0">
			                            <div class="content_input_main_tag">
			                                <h1><?=$Lang['W2']['structure']?></h1>
			                            </div>
			                            <div class="content_input_main">
			                            	<h1><?=$Lang['W2']['paragraph']?></h1>
			                            </div>
		                      		 <br style="clear:both" />
		                      	 	</div>
		                        <!-- Paragraph -->
		                        	<div id="Step4_Paragraph_Div"><?=$h_step4ParagraphHTML?></div>
		                        <!-- End Paragraph -->
		                            <div class="content_input_main_grp">
		                             	<div class="content_input_main_tag"></div>
		                                <div class="content_input_main Content_tool_190"><a href="javascript:void(0);" class="new" onclick="addNewParagraph(4);"><?=$Lang['W2']['newParagraph']?></a></div>
		                                <br style="clear:both" />
		                                
		                            </div>
		                             <!-- Group Color Reference -->
		                            
		                       </div>
		                       <div class="content_input_sample_right">
		                       <div class="remarks keynote">
                            	<div id="keynote">
                            		<span><?=$Lang['W2']['note']?>:</span><br>
                            	 	<textarea name="step4Note" id="step4Note" rows="5"><?=(!empty($parseData['step4Data']['step4Note'])?stripslashes($parseData['step4Data']['step4Note']):'')?></textarea>
                            	</div>
		                       </div>
		                          <h1><?=$Lang['W2']['learningTips']?></h1>
		                       	  <div class="content_input_main_remark">
		                           	<?=$h_step4RemarkHTML?>
		                          </div>
		                       </div>
		                    </div>
						</td>
					</tr>			
				</table>
				</div>
				<!-- Step 4 END-->
				<!-- Step 5 -->				
				<div id="content_5" class="stepContent" style="display:none;"><table style="width:100%">
					<col width="150px" />
                    <col width="10px" />
					<tr class="instruction">
						<th class="instruction">
							<?=$Lang['W2']['instruction']?>
						</th>
						<td class="instruction"></td>						
						<td class="instruction">
							<div id="step5Editor_1"></div>		
							<?=$h_inputStep5IntWarning?>
						</td>
					</tr>
					<tr>
						<td>
							<div class="subtitle_new">
								<span><?=$Lang['W2']['myDraft']?></span>
							</div>	
						</td>
						<td></td>
						<td class="input_row2">
							 <div class="btn_right_grp_new" style="margin-bottom:-25px;">
		                    	<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref_new" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');"><span><?=$Lang['W2']['teacherAttachmentWithBreak']?></span></a>
		                  	</div>
							<div id="step5OutlineArray">
								<?=$h_step5OutlineHTML?>
							</div>
							<?=$h_inputStep5OutlineWarning?>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td class="Content_tool_190">
							<?=$h_step5AddMoreButton?>
						</td>
					</tr>				
				</table>
				</div>
				<!-- Step 5 END-->				
				<!-- Step 6 -->
				<div id="content_6" class="stepContent" style="display:none;"><table style="width:100%">
						<col width="150px" />
                    	<col width="10px" />
						<tr class="instruction">
							<th class="instruction">
								<?=$Lang['W2']['instruction']?>
							</th>
							<td class="instruction"></td>
							<td class="instruction">
								
								<div id="step6Editor_1"></div>					
								<!--
								<textarea class="ckeditor" name="step5Int" id="step5Int" required ></textarea>												
								-->
								<?=$h_inputStep6IntWarning?>
							</td>
						</tr>
						<!--
						<tr><td>&nbsp;</td><td>&nbsp;</td>
						<td><div class="btn_right_grp"><a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');"><span><?=$Lang['W2']['teacherAttachment']?></span></a></div>
                         </td>
                   		</tr>
                   		-->
						<tr>
							<th>
								<div class="subtitle_new"><span><?=$Lang['W2']['engMyWriting']?></span></div>
							</th>
							<td></td>
							<td class="input_row2">
								<div class="btn_right_grp_new" style="margin-bottom:-25px;">
									<!-- New class 20140113 -->
									<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
										<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
									</a>
									<!-- New class 20140113 -->
								</div>
								<div id="step6Editor_2" style="width:82%"></div>					
								<!--
								<textarea class="ckeditor" name="step5Int" id="step5Int" required ></textarea>												
								-->
								<?=$h_inputStep6DefaultWritingWarning?>
							</td>
						</tr>
				</table>
				</div>
				<!-- Step 6 END-->

				<!-- Step 7 -->
				<div id="content_7" class="stepContent" style="display:none;">
                	<table style="width:100%">
                        <colgroup>
                            <col width="130px">
                            <col width="10px">
                            <col width="200px">
                            <col>
                            <col width="40px">
                            <col width="200px">
                        </colgroup>
                        <tbody>
                       <tr class="resourceArray">
                    	  <th><?=$Lang['W2']['resource']?></th>
                    	  <td>:</td>
                    	  <td style="color:#666"><?=$Lang['W2']['name']?></td>
                  	      <td style="color:#666"><?=$Lang['W2']['website']?></td>
                  	      <td style="color:#666">&nbsp;</td>
                  	      <td style="color:#666"><?=$Lang['W2']['step']?></td>
                   	    </tr>
                   	  <?php echo $getResourceArrayHTML;?>
                   	  	<tr>
                    	  <td>&nbsp;</td>
                    	  <td>&nbsp;</td>
                    	  <td><?=$h_inputStep6WebNameWarning?></td>
                  	      <td><?=$h_inputStep6WebsiteWarning?></td>
                  	      <td class="Content_tool_190"></td>
                  	      <td class="Content_tool_190">&nbsp;</td>
                   	  </tr>
                    	<tr>
                    	  <td>&nbsp;</td>
                    	  <td>&nbsp;</td>
                    	  <td class="Content_tool_190"><a href="#" class="new" onclick="addResource(); return false;"><?=$Lang['W2']['addMore']?></a></td>
                  	      <td class="Content_tool_190">&nbsp;</td>
                  	      <td class="Content_tool_190">&nbsp;</td>
                  	      <td class="Content_tool_190">&nbsp;</td>
                   	  </tr>
                   	  
                    </tbody></table>
					<br />
				</div>
				<!-- Step 7 END-->					
    	
 	</div>

   <div class="edit_bottom">
        <?php echo $h_saveButton; ?>
        <?php echo $h_cancelButton; ?>
      </div>
</div></div>
	
<input type="hidden" id="r_recordStatus" name="r_recordStatus" value="<?=$w2_cfg["DB_W2_WRITING_RECORD_STATUS"]["public"]?>" />
<input type = "hidden" name="cid" id="cid" value="<?=$cid?>">
<input type = "hidden" name="saveTab" id="saveTab" value="">
<input type = "hidden" name="task" id="task" value="">
<input type = "hidden" name="mod" id="mod" value="">
<input type = "hidden" name="r_contentCode" id="r_contentCode" value="<?=$r_contentCode?>">