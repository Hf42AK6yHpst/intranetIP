<?
// using: Adam

echo $h_floatingLayer;
?>
<br/>
<div class="main_top_blank"> <!-- for blank div 20140113 -->
	<!--<div class="navigation"><a href="#">Home</a> > English share writing</div>--> <!-- div removed 20140113 -->
</div>
<p class="spacer"></p>

<!-- Main Content Start -->
<div class="shareboard">

	<!-- Top Skateboard Background Image Start -->
	<div class="sb_top">
		<div><div></div></div>
	</div>
	<!-- Top Skateboard Background Image End -->
	<div class="sb_content">
		<div class="sb_content_right">
			<div class="sb_content_bg">
    			<div class="shareboard_main_content">
        			<div class="shareboard_info">
        				<?php
							if($intranet_hardcode_lang=='en'): 
        						echo count($sharedWritingList).' '.$Lang['W2']['shareThisWritingHeader'];
        					else:
        						echo $Lang['W2']['shareThisWritingHeader']."(".count($sharedWritingList).")";
							endif;?>
        				
        			</div>
						<!-- Display Each Shared Writing Start -->	        			
	        			<?php $i=1; foreach( (array)$sharedWritingList as $sharedWriting): ?>
							<a href="<?php echo $sharedWriting['link']?>" class="shareboard_writing_small writing<?php echo $i; ?>">
								<h1><?php echo $sharedWriting['writingName']?></h1>
								<div class="stu_name"><?php echo $sharedWriting['studentName']?> (<?php echo $sharedWriting['className']?>-<?php echo $sharedWriting['classNumber']?>)</div>
								<div class="shareboard_writing_text"><?php echo $sharedWriting['abstract']?></div>                   
								<span class="more">(<?php echo $Lang['W2']['readMore']?>)</span>
								<div class="shareboard_icon">
									<span class="icon_sticker"><?php echo $Lang['W2']['stickers']?>(<?php echo ($sharedWriting['sticker']>99)?'99+':($sharedWriting['sticker']| (string)'0' )?>)</span>
									<span class="icon_comment"><?php echo $Lang['W2']['comments']?>(<?php echo ($sharedWriting['comment']>99)?'99+':($sharedWriting['comment']| (string)'0') ?>)</span>
								</div>
							</a>
	        			<?php $i = $i%2+1; endforeach; ?>
						<!-- Display Each Shared Writing End -->
						<p class="spacer"></p>
		        </div>
		    </div>
		</div>
	</div>
	
	<!-- Bottom Skateboard Background Image Start -->
	<div class="sb_bottom">
		<div><div></div></div>
	</div>
	<!-- Bottom Skateboard Background Image End -->
</div>
<!-- Main Content End -->