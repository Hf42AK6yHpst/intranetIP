<?
/* !!!!!!!!!!!!!!!!!IMPORTANT NOTE START!!!!!!!!!!!!!!!!!
 * 
 * 	Please update modification log after edit any line below [Date, Name, IP ver. and case number if any]!!
 * 
 * !!!!!!!!!!!!!!!!!!IMPORTANT NOTE END!!!!!!!!!!!!!!!!!!
 * Editing by Siuwan
 * 
 * Modification Log:
 * 2015-10-06 (Siuwan) [ip.2.5.6.10.1]
 * 		- display previous draft using ajax if $showCorrectionArea is true
 */

?>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/w2/content/viewHandinHandler.js"></script>
<script LANGUAGE="javascript">
var isCurrentMarkingRecordSaveAsScore = <?=$isCurrentMarkingRecordSaveAsScore == "" ? 0 : 1?>;//20140529-w2gsgame
$(document).ready(function(){
	
	// For Adding new Layout Start
	if($('.write_board_right').children().eq(0).hasClass('content')){
		$('.write_board_right').children().eq(0).addClass('instruction');
	}
	$('.subtitle').eq(0).addClass('subtitle_new');
	$('.subtitle_new').eq(0).removeClass('subtitle');
	
	
	$('.btn_vocab').addClass('btn_vocab_new');
	$('.btn_vocab_new').removeClass('btn_vocab');

	$('.btn_right_grp').addClass('btn_right_grp_new');
	$('.btn_right_grp_new').removeClass('btn_right_grp');

	$('.btn_ref').addClass('btn_ref_new');
	$('.btn_ref_new').removeClass('btn_ref');

	$('.btn_grammar').addClass('btn_grammar_new');
	$('.btn_grammar_new').removeClass('btn_grammar');
	
	<?php if((($stepCode =='c3' || $stepCode =='c4' && $r_contentCode=='chi' &&!$hasDynamicContent) 
			|| ($r_contentCode=='eng'&&!$hasDynamicContent))
			|| ($stepCode =='c5' && ($r_contentCode!='sci' ) &&!$hasDynamicContent )
			|| ($stepCode =='c6' && ($r_contentCode=='sci' ) &&!$hasDynamicContent )
			): ?>
	$('.subtitle_new').after($('.btn_right_grp_new').clone(true));
	$('.btn_right_grp_new').eq(1).remove();
	<?php endif; ?>
	<?php if($stepCode =='c5' && ($r_contentCode!='sci') &&!$hasDynamicContent ): ?>
		$('.btn_right_grp_new').addClass("move_left");
		$('.marking_board').addClass("lower");
	<?php endif;?>
	<?php if ($r_contentCode=='eng' &&!$hasDynamicContent): ?>
	$('.btn_ref_new span').html('<?php echo addslashes($Lang['W2']['teacherAttachmentWithBreak']) ?>')
	$('.btn_vocab_new span').html('<?php echo $Lang['W2']['moreVocabularyForEngine']?>')
	$('.btn_grammar_new span').html('<?php echo $Lang['W2']['grammarAnalysisForEngine']?>')
	<?php endif; ?>
	
	// For Adding new Layout End
	
	if(typeof window.individualDocumentReadyHandling == 'function') {
		individualDocumentReadyHandling();
	}
	
	//Move the reference passage under the subtitle
	$("div.btn_right_grp").insertAfter("div.subtitle");
	//Move marking board under title
	$("div.marking_board").insertAfter("div.title");
	//Marking - Comment
	$("div.tool_comment a#comment_link").click(function(){
		$("div#write_comment").toggle();
	});
	//Marking - Stickers
	$("div.tool_sticker a#sticker_link").click(function(){
		$("div#select_sticker").toggle();
	});	
	$("a.marking_sticker").click(function(){
		var sticker_id = $(this).closest('li').attr("id");
		if($("li#"+sticker_id).hasClass("selected")){
			$("li#"+sticker_id).removeClass("selected");
		}else{
			$("li#"+sticker_id).addClass("selected");
		}
		var r_sticker = '';
		$("ul.sticker_list li.selected").each(function(i){
			r_sticker += i>0?':':'';
			r_sticker += this.id;
		});
		$("#r_sticker").val(r_sticker);
	});
<?if($showCorrectionArea || $handinLatestVersion > 1):?>
	$("div.correction_board").insertBefore("div.draft");
	$("div.btn_right_grp").remove();
	$("div.ref_board a#ref_link").click(function(){
		$("div#ref_layer").toggle();
	});	
	
	$("select.select_draft").change(function(){
		$("#r_version").val($(this).val());
	<?if($showCorrectionArea){?>	
		$('#mod').val("ajax");
		$('#task').val("getStudentHandinDraft");	
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#handinForm").serialize(),
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(data){
						  if (data) {
							  $('div.draft_previous div.write').html(data);
						  }
					  }
		});
	<?}else{?>
		$('#mod').val("handin");
		$('#task').val("viewHandIn");
		$("form#handinForm").submit();
	<?}?>				
	});	
<? endif;?>
<?php if($hasDynamicContent):?>
	<?php if($isLastStep&&$showCorrectionArea):?>	
	var ref = $("div.draft div.ref").html();
	if(ref!=''){
		$("div#ref_layer").html(ref);
	}else{
		$("div.correction_board").addClass("draft_no_ref");
	}
	$("div.draft div.ref").hide();
	tb_init('a.thickbox');
	<?php else:?>
	$("div.draft div.ref").show();
	<?php endif;?>
<?php else:?>	
	<?php if($isLastStep&&$showCorrectionArea):?>	
	var ref = $("div.draft div.ref").html();
	if(ref!=''){
		$("div#ref_layer").html(ref);
	}else{
		$("div.correction_board").addClass("draft_no_ref");
	}
	$("div.draft div.ref").hide();
	tb_init('a.thickbox');
	<?php else:?>
	<?php if(!($r_contentCode=='sci'&& $stepCode =='c2')):?>
		$("div.ref").insertAfter("div.write");
	<?php endif;?>
	$("div.draft div.ref").show();
	<?php endif;?>	
<?php endif;?>
<?if($isSampleWriting):?>
	$('span#remark_highlight_icon_big').click(function(){
		var hideHighlight = false;
		if($(this).hasClass('current')){
			hideHighlight = true;
			$(this).removeClass('current');
		}
		var className = $(this).attr('class').replace('remark_highlight_icon_big_','');
		var updateClassName = '';
		switch(className){
		<?foreach($w2_cfg["SampleWriting"]["HighlightMapping"] as $_key => $_value):?>
			case '<?=$_key?>':updateClassName='highlight_<?=$_value?>';break;
		<?endforeach;?>
		}
		if(hideHighlight){
			$('.'+updateClassName).removeClass(updateClassName).addClass(updateClassName+'_off');
			$(this).find('u').hide();		
		}else{
			showEssayHighlight(updateClassName);
			$(this).addClass('current');
			$(this).find('u').show();
		}
		
	});
	hideEssayHighlight();
<?endif;?>
<?if($isLastStep):?>
	if($('#r_mark').length>0){
		$('#r_mark').keypress(function(e){
			if(e.which==13){
				event.preventDefault();
			}
		});
	}
<?endif;?>	
});
function hideEssayHighlight(){
	$('span#remark_highlight_icon_big').find('u').hide();
	$('span#remark_highlight_icon_big').removeClass('current');
	$('.highlight_red').removeClass('highlight_red').addClass('highlight_red_off');
	$('.highlight_orange').removeClass('highlight_orange').addClass('highlight_orange_off');
	$('.highlight_yellow').removeClass('highlight_yellow').addClass('highlight_yellow_off');
	$('.highlight_lightgreen').removeClass('highlight_lightgreen').addClass('highlight_lightgreen_off');
	$('.highlight_green').removeClass('highlight_green').addClass('highlight_green_off');
	$('.highlight_lightblue').removeClass('highlight_lightblue').addClass('highlight_lightblue_off');
	$('.highlight_blue').removeClass('highlight_blue').addClass('highlight_blue_off');
	$('.highlight_darkblue').removeClass('highlight_darkblue').addClass('highlight_darkblue_off');
	$('.highlight_purple').removeClass('highlight_purple').addClass('highlight_purple_off');
	$('.highlight_pink').removeClass('highlight_pink').addClass('highlight_pink_off');
}
function showEssayHighlight(c){
	hideEssayHighlight();
	$('.'+c+'_off').removeClass(c+'_off').addClass(c);
}
	
//function saveHandIn(){
//	$("#task").val('saveHandIn');
//	document.handinForm.submit();
//}

/*
function goToStep(schemeCode, stepCode, stepId,writingId,content){
	var _url = '/home/eLearning/w2/index.php?mod=handin&task=viewHandIn&r_contentCode='+content+'&r_schemeCode='+ schemeCode+'&r_stepCode='+stepCode+'&r_stepId='+stepId+'&r_writingId='+writingId;
	window.location = _url;
}
*/

function goToStep(schemeCode, stepCode, stepId, writingId, content, p_studentId){
	$('#mod').val("handin");
	$('#task').val("viewHandIn");
	
	if (stepId == '' || stepId == null) {
		// do nth
	}
	else {
		$("#r_stepId").val(stepId);
	}
	
	if (stepCode == '' || stepCode == null) {
		// do nth
	}
	else {
		$("#r_stepCode").val(stepCode);
	}
	
	if (p_studentId == '' || p_studentId == null) {
		// do nth
	}
	else {
		$("#p_studentId").val(p_studentId);
	}
	
	$("#handinForm").submit();
}
function addComment(){
	var chosen_comment = $('#r_commentBank option:selected').html();
	if(chosen_comment != '<?=$Lang['W2']['pleaseSelect']?>'){
		var comment = $('#r_comment').val();
		
		if(comment=='')
			$('#r_comment').val(chosen_comment);
		else
			$('#r_comment').val(comment + '\n' + chosen_comment);
	}
}
function uploadStudentAttachment(goNextStep, updateStepStatus){
	$("#handinForm input[id=mod]").val("common");
	$("#handinForm input[id=task]").val("uploadStudentAttachmentFile");
	$("#handinForm input[id=r_goNextStep]").val(goNextStep);
	$("#handinForm input[id=r_stepStatus]").val(updateStepStatus);

	$("#uploadFileIFrame").attr('src', "/home/eLearning/w2/index.php");
	$("#handinForm").attr('method', 'post').attr('target', 'uploadFileIFrame').submit().attr('target', '_self');
}
function saveStepAns(goNextStep, updateStepStatus, includeAnsCodeText){
	updateStepStatus = updateStepStatus || '';
	includeAnsCodeText = includeAnsCodeText || '';

	$('#mod').val("ajax");
	$('#task').val("saveStudentHandIn");
	var schemeCode = $('#r_schemeCode').val();
	var nextStepCode = $('#r_nextStepCode').val();
	var nextStepId = $('#r_nextStepId').val();
	var writingId = $('#r_writingId').val();
	var content= $('#r_content').val();
	var action = $('#r_action').val(); 
	if(typeof window.individualAnsHandling == 'function') {
		individualAnsHandling();
	}
	var stepCode = $('#r_stepCode').val();
	<?if($contentCode=='sci'){?>
	if(stepCode == 'c1'){
		var keywordAry = $('#txtAreaScienceTerm').val().split("\n");
		keywordAry = jQuery.grep(keywordAry, function( n ) {return ( Trim(n) !== '');});
		if(keywordAry.length<5){
			alert('<?=$Lang['W2']['jsWarningAry']['atLeast5RelatedScienceTerms']?>');
			 $('#txtAreaScienceTerm').focus();
			return false;
		}
	}
	<?}?>
	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     <?=$h_submitForm?>.serialize() + '&r_submitStatus=' + updateStepStatus + '&r_includeAnsCodeText=' + includeAnsCodeText,
		async:		false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					if(typeof callBack_saveStepAns == 'function') {
						callBack_saveStepAns();
					}
					if(goNextStep)
					{
					  //if goNextStep == true /1 , go to next step
					  goToStep(schemeCode, nextStepCode, nextStepId,writingId,content);
					}
					else
					{
						//refresh self once
						//this line to handle upload file problem 20121102
						//goToStep(schemeCode, $('#r_stepCode').val(), $('#r_stepId').val(),writingId,content);
					
//						alert('save step ok!!');
					  //have not handle
					  /*
					  date = $(xml).find("date").text();
					  $("#lastModifiedDate").html(date);
					  return 1;
					  */
					}
				  }
  });
}
function submitFinalStep(){

//alert('enter js f:submitFinalStep');
	saveStepAns(0);  // save ans and don't go to next step "0"
	saveFinalStepAns();

}
function saveFinalStepAns(){

//	alert('saveFinalStepAns');
	$('#mod').val("ajax");
	$('#task').val("saveStudentFinalHandIn");

	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize(),
		async: false,
		error:	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					  //have not handle
					  /*
					  date = $(xml).find("date").text();
					  $("#lastModifiedDate").html(date);
					  return 1;
						*/
//					alert(xhr.responseText);

					if(typeof callBack_saveFinalStepAns == 'function') {
						callBack_saveFinalStepAns();
					}
					//alert('toptop');
					Scroll_To_Top();
					//$Lang['General']['ReturnMessage']['AddSuccess'] = '1|=|Record Added.';
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['AddSuccess']?>');
				  }
	});

	
}

function changedStudentSel(p_studentId) {
	$('#p_studentId').val(p_studentId);
	
	var r_stepCode = $('#r_stepCode').val();
	var r_stepId = $('#r_stepId').val();
	
	goToStep('', r_stepCode, r_stepId, '', '');
}

function updateStepApprovalStatus(approvalStatus, p_studentId) {
	$('#mod').val("ajax");
	$('#task').val("updateStepApprovalStatus");

	$.ajax({
		url:      "/home/eLearning/w2/index.php",
		type:     "POST",
		data:     $("#handinForm").serialize() + '&r_approvalStatus=' + approvalStatus,
		error:	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
					result = $(xml).find("result").text();
					Scroll_To_Top();					
					if(result == 1){						
						Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
						updateApprovalStatusUI(approvalStatus);
					}else{						
						Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
					}

//					window.location.reload();
//						alert(xml);
					  //have not handle
					  /*
					  date = $(xml).find("date").text();
					  $("#lastModifiedDate").html(date);
					  return 1;
						*/
				  }
	});
}
function saveHandinMarking(successMsg, failedMsg) {
	$('#mod').val("ajax");
	$('#task').val("saveStudentHandinMarking");
	$('#markWarningSpan').hide();
	$("#passScoreToGSRecord").val(false);
	var mark = $('#r_mark').val();
	if(mark < <?=$lowestmark?$lowestmark:0?> || mark > <?=$fullmark?$fullmark:0?>){
		$('#markWarningSpan').show();
		return false;
	}else{
		//20140529-w2gsgame
		var isAllowSave = false;
		if(!isCurrentMarkingRecordSaveAsScore && mark != ''){
			var r=confirm("<?=$Lang['W2']['confirmGSGameScoreSave']?>");
			if (r==true) { 
				isAllowSave = true;
				$("#passScoreToGSRecord").val(true);
				isCurrentMarkingRecordSaveAsScore = true;
			}
		}else{
			isAllowSave = true; 
		}
		
		if(isAllowSave){			
			$.ajax({
				url:      "/home/eLearning/w2/index.php",
				type:     "POST",
				data:     $("#handinForm").serialize(),
				error:    function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success:  function(xml){
							  var returnMsg;
							  
							  if (xml) {
								  returnMsg = successMsg;
								  var comment_id = xml.split('|=|')[0];
								  var html = xml.split('|=|')[1];
								  $('div#display_mark_comment').html(html);
								  $('input#r_commentId').val(comment_id);
							  }
							  else {
								  returnMsg = failedMsg;
							  }
							  
							  Get_Return_Message(returnMsg);
						  }
			});
		}
	}
}
function updateApprovalStatusUI(approvalStatus){
	if(approvalStatus == <?=$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]?>){

		$("#w2_stepApproval_wait").removeClass('btn_wait').addClass('btn_wait_current');
		$("#w2_stepApproval_approve").removeClass('btn_approve_current').addClass('btn_approve');

	}else if(approvalStatus == <?=$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]?>){

		$("#w2_stepApproval_wait").removeClass('btn_wait_current').addClass('btn_wait');
		$("#w2_stepApproval_approve").removeClass('btn_approve').addClass('btn_approve_current');
	}else if(approvalStatus == <?=$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]?>){
		$(".btn_redo span").html("<?=$Lang['W2']['RedoSent']?>");
		$(".btn_redo").removeClass('btn_redo').addClass('btn_redo_current').removeAttr("onclick");
	}
}
function addStudentAttachmentBox(){
	var attName = 'attachmentBox';  //hard code first , variable name should be map with name used in uploadFile.php
	var divObj = '#w2_div_'+attName;
	var cntObj = $('#div_'+attName+'_counter');
	var cnt = cntObj.val();

	var divAttachContainer = 'div_'+attName+'_'+cnt;
	var html = '<tr id="'+divAttachContainer+'" >';
	    html += '<td><img src="/images/2009a/w2/icon_attachment2.gif"></td>';
		html += '<td><input name="r_'+attName+'_supplementName[]" type="text" id="div_'+attName+'_supplementName_'+cnt+'" size="40"/></td>';
		html += '<td><input name="r_'+attName+'[]" type="file" id="div_'+attName+'_'+cnt+'" size="40"/></td>';
		html += '<td class="table_row_tool"><a href="javaScript:void(0)" class="tool_delete_dim" title="Delete" OnClick="removeAttachmentBox(\''+divAttachContainer+'\')">&nbsp;</a></td>';
		html += '</tr>';
    $(divObj+" > tbody:last").append(html);  
//	divObj.append(html);
	var new_cnt = parseInt(cnt) + parseInt(1);
	cntObj.val(new_cnt);


}
function addAttachmentBox(){
	var attName = 'attachmentBox';  //hard code first , variable name should be map with name used in uploadFile.php
	var divObj = '#w2_div_'+attName;
	var cntObj = $('#div_'+attName+'_counter');
	var cnt = cntObj.val();

	var divAttachContainer = 'div_'+attName+'_'+cnt;
/*
	var html = '<div id="'+divAttachContainer+'" >';
	    html += '&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/2009a/w2/icon_attachment2.gif">';
		html += '<input name="r_'+attName+'_supplementName[]" type="text" id="div_'+attName+'_supplementName_'+cnt+'" size="40"/>';
		html += '&nbsp;&nbsp;';
		html += '<input name="r_'+attName+'[]" type="file" id="div_'+attName+'_'+cnt+'" size="40"/>';

		html += '&nbsp;&nbsp;';
		html += '<a href="javaScript:void(0)" class="tool_delete_dim" title="Delete" OnClick="removeAttachmentBox(\''+divAttachContainer+'\')">&nbsp;[Delete]<a>';
		html += '</div>';
*/
	var html = '<tr id="'+divAttachContainer+'" >';
	    html += '<td><img src="/images/2009a/w2/icon_attachment2.gif"></td>';

//		html += '<td><input name="r_'+attName+'_supplementName[]" type="text" id="div_'+attName+'_supplementName_'+cnt+'" size="40"/></td>';

		html += '<td>&nbsp;&nbsp;<input name="r_'+attName+'[]" type="file" id="div_'+attName+'_'+cnt+'" size="40"/></td>';
		html += '<td class="table_row_tool"><a href="javaScript:void(0)" class="tool_delete_dim" title="Delete" OnClick="removeAttachmentBox(\''+divAttachContainer+'\')">&nbsp;</a></td>';
		html += '</tr>';
    $(divObj+" > tbody:last").append(html);  
//	divObj.append(html);
	var new_cnt = parseInt(cnt) + parseInt(1);
	cntObj.val(new_cnt);


}
function removeAttachment(fileName,remove_divId){
	var removeDivObj = 'w2_uploadFileId_' + remove_divId;
	var answer = confirm("Delete file?")
	if (answer){
		$.ajax({
				url:      "/home/eLearning/w2/index.php",
				type:     "POST",
				data:     "r_fName="+fileName+"&task=deleteFile&mod=common",
				error:    function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success:  function(xml){
							  //remove the file div
							if(document.getElementById(removeDivObj) != undefined){
								$('#'+removeDivObj).remove();
							}
						  }
		  });
	}

}
function removeStudentAttachmentFile(fileName,remove_divId){
	var removeDivObj = 'w2_uploadFileId_' + remove_divId;
	var answer = confirm("Delete file?")
	if (answer){
		$.ajax({
				url:      "/home/eLearning/w2/index.php",
				type:     "POST",
				data:     "r_fName="+fileName+"&task=deleteStudentAttachmentFile&mod=common",
				error:    function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success:  function(xml){
							  //remove the file div
							if(document.getElementById(removeDivObj) != undefined){
								$('#'+removeDivObj).remove();
							}
						  }
		  });
	}

}
function toggleStickerList() {	
    var target = document.getElementById("item_sticker");
	
    if (target.className == 'sticker_collsape') {
        target.className = 'sticker_expand';
    } else {
        target.className = 'sticker_collsape';
    }
}
function removeAttachmentBox(divID){
	if(document.getElementById(divID) != undefined){
		//document.getElementById(divID).innerHTML = '';
		$('#'+divID).remove();
	}
}

function shareToStudent(contentCode,schemeCode,stepId,stepHandinCode,studentId){
	var setAction = $("#w2_shareToStudent").attr("share_action");
	$.ajax({
				url:      "/home/eLearning/w2/index.php",
				type:     "POST",
				data:     "task=shareToStudent&mod=ajax&r_contentCode="+contentCode+"&r_schemeCode="+schemeCode+"&r_stepId="+stepId+"&r_stepHandinCode="+stepHandinCode+"&r_studentId="+studentId+'&r_setAction='+setAction,
				async:    false,
				error:    function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success:  function(xml){
							  //update the button for this action
							if(setAction == 1){
								$("#w2_shareToStudent").html('<span><?=$Lang['W2']['shareWritingShared']?></span>');
//								$("#w2_shareToStudent").attr('class','btn_certificate_dark btn_share');
								$("#w2_shareToStudent").attr('class','shared btn_share_new');
								$("#w2_shareToStudent").mouseover(function(){changeShareText(1)});
								$("#w2_shareToStudent").mouseout(function(){changeShareText(0)});
								$('#w2_shareToStudent').attr('share_action', 0);
							}else{
								$("#w2_shareToStudent").html('<span><?=$Lang['W2']['shareThisWriting']?></span>');
// Comment By Adam For New Layout Start								
//								$("#w2_shareToStudent").attr('class','btn_certificate_light btn_share');
// Comment By Adam For New Layout End
								$("#w2_shareToStudent").attr('class','btn_share_new');
								$('#w2_shareToStudent').attr('share_action', 1);
							}
						  }
	});
}

function changeShareText(str){
	str = str==1?'<?=$Lang['W2']['shareWritingUnshare']?>':'<?=$Lang['W2']['shareWritingShared']?>';
	$("#w2_shareToStudent[share_action=0]").children('span').html(str);
}
function resetMarkingArea(){
	$('#r_mark').val('');
	$('textarea.write_comment_area').val('');	
	$("ul.sticker_list li.selected").each(function(i){
		$(this).removeClass('selected');
	});
	$('#r_sticker').val('');
}
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}

</script>

<form name = "handinForm" action="index.php" id="handinForm" method="POST" enctype="multipart/form-data">
<?=$objHandinLayout->display()?>



<!--hr/-->
<?php
//echo $h_preStepButton;
?>
<?php
//echo $h_nextStepButton;
?>
<!--hr/-->

<input type = "hidden" name="r_writingId"				id="r_writingId"				value="<?=$writingId?>">
<input type = "hidden" name="r_studentId"				id="r_studentId"				value="<?=$w2_thisStudentID?>">
<input type = "hidden" name="r_peerMarkingMarkerUserId"	id="r_peerMarkingMarkerUserId"	value="<?=$w2_thisPeerMarkingMarkerUserId?>">
<input type = "hidden" name="r_writingStudentId"		id="r_writingStudentId"			value="<?=$writingStudentId?>">
<input type = "hidden" name="r_contentCode"				id="r_contentCode"				value="<?=$contentCode?>">
<input type = "hidden" name="r_schemeCode"				id="r_schemeCode"				value="<?=$schemeCode?>">
<input type = "hidden" name="r_stepCode"				id="r_stepCode"					value="<?=$stepCode?>">
<input type = "hidden" name="r_stepId"					id="r_stepId"					value="<?=$stepId?>">
<input type = "hidden" name="r_preStepCode"				id="r_preStepCode"				value="<?=$preStepCode?>">
<input type = "hidden" name="r_preStepId"				id="r_preStepId"				value="<?=$preStepID?>">
<input type = "hidden" name="r_nextStepCode"			id="r_nextStepCode" 			value="<?=$nextStepCode?>">
<input type = "hidden" name="r_nextStepId"				id="r_nextStepId"				value="<?=$nextStepID?>">
<input type = "hidden" name="r_commentId"				id="r_commentId"				value="<?=$stepStudentCommentId?>">
<input type = "hidden" name="r_tabTask"					id="r_tabTask"					value="<?=$r_tabTask?>">
<input type = "hidden" name="r_action"					id="r_action"					value="<?=$w2_thisAction?>" >
<input type = "hidden" name="r_handinSubmitStatus"		id="r_handinSubmitStatus"		value="<?=$r_handinSubmitStatus?>">
<input type = "hidden" name="r_version"					id="r_version"					value="<?=$handinVersion?>">
<input type = "hidden" name="r_curStepHandinCode"		id="r_curStepHandinCode"		value="<?=$w2_m_ansCode[0]?>">
<input type = "hidden" name="r_sticker"					id="r_sticker"					value="<?=$marker_sticker?>">
<input type = "hidden" name="cid"						id="cid"						value="<?=$cid?>">
<input type = "hidden" name="r_goNextStep"				id="r_goNextStep"						value="">
<input type = "hidden" name="r_stepStatus"				id="r_stepStatus"				value="">
<input type = "hidden" name="p_studentId"				id="p_studentId"				value="">
<input type = "hidden" name="mod"						id="mod"						value="">
<input type = "hidden" name="task"						id="task"						value="">
<input type = "hidden" name="passScoreToGSRecord"	 	id="passScoreToGSRecord"		value="">

</form>