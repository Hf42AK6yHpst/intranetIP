<?
include_once($PATH_WRT_ROOT."includes/w2/libWritingTeacher.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2Export.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
if(!$w2_thisIsAdmin) die();
$objDb = new libdb();
$lexport = new libw2export();
$fs = new libfilesystem();

$contentCode = $_REQUEST['r_contentCode'];
$r_returnMsgKey = $_REQUEST['r_returnMsgKey'];
$r_writingId = (isset($_REQUEST['r_writingId']))? $_REQUEST['r_writingId'] : "";
$stepId = $w2_libW2->getWritingLastStepId($r_writingId);


### Get Writing Info
$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
$writingTitle = $objWriting->getTitle();

$writing_folder = $intranet_root."/file/w2/writing_report/".$r_writingId;
$writing_name = $Lang['W2']['subjectFileCode'][$contentCode]."_".$writingTitle."_".date("Ymd");
$writing_name = $fs->SafeGuardFileName($writing_name,true);
$writing_name_encoded = mb_convert_encoding($writing_name ,"big5","utf8");
$folder_target = $writing_folder."/".$writing_name_encoded;
createOutputFolder($folder_target);

$writingToBeMarkedStudentAssoAry = $w2_libW2->getStudentWritingRecordByWritingId($contentCode,$r_writingId);
foreach((array)$writingToBeMarkedStudentAssoAry as $_studentId => $_studentWritingAry){
	$_studentWritingAttachmentAry = $lexport->getStudentWritingFile($stepId,$_studentId);
	
	$_writingName = $Lang['W2']['subjectFileCode'][$contentCode]."_".$writingTitle."_".$_studentWritingAry['CLASS']."_".$_studentWritingAry['CLASS_NO']."_".date("Ymd");
	$_writingName = $fs->SafeGuardFileName($_writingName,true);
	$_writingName = mb_convert_encoding($_writingName ,"big5","utf8");
	$_folderTarget = $folder_target."/".$_writingName;
	createOutputFolder($_folderTarget);
	### Prepare folder and student attachment
	$_studentWritingAttachmentCnt = count($_studentWritingAttachmentAry);
	if($_studentWritingAttachmentCnt>0){
		$_studentAttachmentFolder = $_folderTarget."/attachment";
		createOutputFolder($_studentAttachmentFolder);
		for($i=0;$i<$_studentWritingAttachmentCnt;$i++){
			$_from = $_studentWritingAttachmentAry[$i]["DOWNLOAD_PATH"];
			$_filename = $_studentWritingAttachmentAry[$i]["FILE_ORI_NAME"];
			$_filename = $fs->SafeGuardFileName($_filename,true);
			$_filename = mb_convert_encoding($_filename ,"big5","utf8");
			if(file_exists($_from)){
				$fs->item_copy($_from, $_studentAttachmentFolder."/".$_filename);
			}
		}
	}
	### Get Last Step info of each students of each writings 
	$_content = "";
	$_content .= $Lang['W2']['className'].": ".$_studentWritingAry['CLASS']." ".$Lang['W2']['ClassNo'].": ".$_studentWritingAry['CLASS_NO']."<br/>";"<br/>";
	$_content .= $Lang['W2']['name'].": ".$_studentWritingAry['STUDENT_NAME']."<br/><br/>";
	for($i=1;$i<=$w2_cfg['HandinArr']['maxRedoAttemptCount'];$i++){
		if($_studentWritingAry['DRAFT'.$i]!='-'&&$_studentWritingAry['DRAFT'.$i]>=0){
			$_content .= $w2_libW2->getDraftName($i)." ".$Lang['W2']['mark'].": ".$_studentWritingAry['DRAFT'.$i]."<br/>";
			$_content .= $_studentWritingAry['ANSWER'.$i]."<br/><br/>";	
		}
	}
	$_docName = $_writingName.".doc";
	$file_content = $lexport->createDoc($_content, $_docName, false);
	
	$fs->file_remove("{$_folderTarget}/$_docName");
	$fs->file_write($file_content, "{$_folderTarget}/$_docName");
	
}


	
$zip_name = $writing_name.".zip";
$zip_name_encode = urlencode($writing_name).".zip";
$zip_path = $writing_folder."/".$zip_name;

chdir($folder_target);
exec("zip -r ../".OsCommandSafe($zip_name_encode)." *");
$fs->file_rename($writing_folder."/".$zip_name_encode, $writing_folder."/".$zip_name);
$fs->folder_remove_recursive($folder_target);

##out the zip file to browser and let the user download it.
output2browser(get_file_content($zip_path), $zip_name);

function createOutputFolder($folder_target){
	global $fs;
	if(!file_exists($folder_target)) //create folder
	{
		$fs->folder_new($folder_target);
	}else{ //remove everything inside
		$row = $fs->return_folderlist($folder_target);
	    for($i=0; $i<sizeof($row); $i++){
	      $fs->item_remove($row[$i]);
	    }
	}
}
?>