<?
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingStudent.php");
include_once($PATH_WRT_ROOT."includes/w2/libStepStudentComment.php");

$r_writingId = (isset($_REQUEST['r_writingId']))? $_REQUEST['r_writingId'] : "";
$r_studentId = (isset($_REQUEST['student_id']))? $_REQUEST['student_id'] : "";
$r_version = (isset($_REQUEST['r_version']))? $_REQUEST['r_version'] : "";
$r_contentCode = (isset($_REQUEST['r_contentCode']))? $_REQUEST['r_contentCode'] : "";

$stepInfoAry = $w2_libW2->getStepInfoByContent($r_contentCode);
$lastStepCode = $stepInfoAry[count($stepInfoAry)-1]['code'];

$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
$w2_objContent = libW2ContentFactory::createContent($r_contentCode);
$r_schemeCode = $objWriting->getSchemeCode();
$templateID =  $objWriting->getTemplateID()?$objWriting->getTemplateID():1;
$clipArtID = $objWriting->getClipArtID()?$objWriting->getClipArtID():1;
$templateID =	str_pad($templateID,  2, "0", STR_PAD_LEFT);
$clipArtID =	str_pad($clipArtID,  2, "0", STR_PAD_LEFT);
$curStep = str_replace('c','',$lastStepCode);

$contentDetailsAry = $w2_libW2->getModelFile($r_contentCode,$r_schemeCode,$lastStepCode);
if(file_exists($contentDetailsAry['model'])){
	include_once($contentDetailsAry['model']);
	$stepHandinCode = $w2_m_ansCode[0];
	$hardCodeContent = file_get_contents($contentDetailsAry['view']);
    $hardCodeContent = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $hardCodeContent);                   	
}else{
	if((strpos($r_schemeCode,'_')!==false)){
		$engContentParser = new libW2EngContentParser();
		list($schoolCode,$scheme) = explode('_',$r_schemeCode);
		$stepHandinCode = $schoolCode.'_'.$r_contentCode.'_'.$scheme.'_'.$lastStepCode.'_ans1';
		$schemeNum = str_replace('scheme','',$scheme);
		$contentData = array($schoolCode,$schemeNum,$r_contentCode);
		$parseData = $engContentParser->parseEngContent('',$contentData);
		$instruction = intranet_undo_htmlspecialchars($parseData['step'.$curStep.'Data']['step'.$curStep.'Int']);
	}
}
### Get the info of the writings of the student
$writingInfoAry = $w2_libW2->findStudentWriting($r_studentId, $r_contentCode, $r_writingId);
$stepId = $w2_libW2->getWritingLastStepId($r_writingId);


### Check student latest version
$latestVersion = $w2_libW2->getStudentStepHandinLatestVersion($stepId,$r_studentId);
if($r_version==$latestVersion){//current version
	$studentWritingRecord = $w2_libW2->getStudentStepHandinRecord($stepId,$r_studentId,$stepHandinCode);
}else{//history record
	$studentWritingRecord = current($w2_libW2->getStepHandinHisotryRecord($stepId,array($r_studentId),$r_version));
}
$objComment = new libStepStudentComment();
$commentBankSelection = $w2_thisAction==$w2_cfg["actionArr"]["peerMarking"]&&$r_contentCode=='eng'?$objComment->getCommentBankSelection():'';
$displayHandinCommentHTML = '';
$displayHandinCommentHTML .= $w2_libW2->getDisplayMarkingLayerHTML($stepId,$r_studentId,$r_writingId,$stepHandinCode,$r_version,true);
$displayHandinCommentHTML = '
	<div class="mark_comment">
       	<div id="display_mark_comment" class="show_sticker">'.$displayHandinCommentHTML.'</div>
    </div>
';




$linterface->LAYOUT_START();
include_once('templates/report/viewStudentWriting.tmpl.php');
$linterface->LAYOUT_STOP();
?>