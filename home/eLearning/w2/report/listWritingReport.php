<?
include_once($PATH_WRT_ROOT."includes/w2/libWritingTeacher.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");

$r_contentCode = $_REQUEST['r_contentCode'];
$r_returnMsgKey = $_REQUEST['r_returnMsgKey'];


### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_REQUEST['curPage']))? $_REQUEST['curPage'] : 1;
$numPerPage = (isset($_REQUEST['numPerPage']))? $_REQUEST['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_REQUEST['sortColumnIndex']))? $_REQUEST['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_REQUEST['sortOrder']))? $_REQUEST['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];
$r_schemeCode = (isset($_REQUEST['r_schemeCode']))? $_REQUEST['r_schemeCode'] : "";
$r_grade = (isset($_REQUEST['grade']))? $_REQUEST['grade'] : "";
$r_contentTypeSuper = (isset($_REQUEST['contentSuperTypeSlt']))? $_REQUEST['contentSuperTypeSlt'] : "";
$r_contentType = (isset($_REQUEST['contentTypeSlt']))? $_REQUEST['contentTypeSlt'] : "";
$keyword = (isset($_REQUEST['keyword']))? $_REQUEST['keyword'] : "";

$objDb = new libdb();
$objWritingTeacher = new libWritingTeacher($w2_thisUserID);

$accessiableOnly = $w2_thisIsAdmin?false:true;
$schemeCodeAry = array();
$sortUrl = "";
### Get the Teacher Writing Info of this Content
if(empty($r_schemeCode)&&empty($r_grade)&&empty($r_contentTypeSuper)&&empty($r_contentType)&&empty($keyword)){//no filter
	$teacherWritingInfoArr = $objWritingTeacher->findTeacherWritingByContent($r_contentCode, $accessiableOnly);
}else{
	if(!empty($r_schemeCode)){
		$contentAttributes = $w2_libW2->getContentAttributes($r_contentCode,$r_schemeCode);
		$schemeType = $contentAttributes['type'];
		$schemeCodeAry[] = $r_schemeCode;
	}else{
		$contentList = $w2_libW2->getContentList($r_contentCode);
		$sortUrl.="&grade=".$r_grade."&contentSuperTypeSlt=".$r_contentTypeSuper."&contentTypeSlt=".$r_contentType."&keyword=".$keyword;
		foreach($contentList as $_schemeCode => $_contentAry){
			if(!empty($r_grade)&&$_contentAry['grade']!=$r_grade) continue;  
			if(!empty($r_contentTypeSuper)&&$_contentAry['type_super']!=$r_contentTypeSuper) continue;  
			if(!empty($r_contentType)&&$_contentAry['type']!=$r_contentType) continue;  
			$schemeCodeAry[] = $_schemeCode;
		}
	}
	$searchValueAry["schemeCodeAry"] 	= empty($schemeCodeAry)&&empty($keyword)?array(""):$schemeCodeAry;
	$searchValueAry["keyword"]			= !empty($keyword)?$keyword:"";
	$teacherWritingInfoArr = $objWritingTeacher->findTeacherWritingByContent($r_contentCode, $accessiableOnly, $searchValueAry);
}

//debug_r($teacherWritingInfoArr);
$numOfTeacherWriting = count($teacherWritingInfoArr);


### Get the Student of each Writing
$writingAllStudentAry = $w2_libW2->getWritingStudentInfoAry($r_contentCode);
$writingAllStudentAssoAry = BuildMultiKeyAssoc($writingAllStudentAry, array('WRITING_ID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
$writingCompletedStudentAry = $w2_libW2->getWritingStudentInfoAry($r_contentCode, null, $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"]);
$writingCompletedStudentAssoAry = BuildMultiKeyAssoc($writingCompletedStudentAry, array('WRITING_ID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
$writingIdAry = array_keys($writingAllStudentAssoAry);

## Get Filter Selection
$contentAryGroupByType  = $w2_libW2->getContentGroupByType($r_contentCode);

$contentAryGroupBySuperType  = $w2_libW2->getContentGroupByTypeSuper($r_contentCode);
$h_gradeSelection = $w2_libW2->getGradeSelection($r_grade,'onchange="resetThemeSelection();"',$showAll=true);

if(strtolower($r_contentCode) == strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])){
	$h_select1 = '<select name="contentSuperTypeSlt" id="id_contentSuperTypeSlt" onchange="updateContentTypeSlt(this.options[this.options.selectedIndex].value,\''.$r_contentCode.'\'); $(\'#id_contentTypeSlt\').val(\'\'); updateSchemeCodeSlt($(\'#id_contentTypeSlt\').val(\'\'),\''.$r_contentCode.'\')">';
	$h_select1 .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
	foreach($contentAryGroupBySuperType as $_typeSuper => $_Details){
		$_selected = '';
		if($r_contentTypeSuper != '' && $r_contentTypeSuper == $_typeSuper){
			$_selected = ' SELECTED ';
		}
		$h_select1 .= '<option value="'.$_typeSuper.'" '.$_selected.'>'.$_typeSuper.'</option>';
	}
	$h_select1 .= '</select>';

}
$h_select2 = $w2_libW2->getTextTypeSelection($r_contentCode,$r_contentTypeSuper,$r_contentType,$r_grade);
$h_selectSchemeCode = $w2_libW2->getSchemeCodeSelection($r_contentCode,$r_contentTypeSuper,$r_contentType,$r_schemeCode,$r_grade);

### Get Last Step info of each students of each writings 
$writingToBeMarkedStudentAssoAry = $w2_libW2->getWritingToMarkStudent($writingIdAry);


### Prepare temp table for display
$SuccessArr = array();
$sql = '';
$sql .= "Create Temporary Table If Not Exists TMP_W2_PAGE (
			WRITING_NAME varchar(255) default null,
			WRITING_NAME_LINK text default null,
			SCHEME_NAME varchar(255) default null,
			LEVEL varchar(50) default NULL,
     		CATEGORY varchar(50) default NULL,
			START_DATE varchar(255) default null,			
			END_DATE varchar(255) default null,
			COMPLETED text default null
		) ENGINE=InnoDB Charset=utf8
";
//debug_r($sql);
$SuccessArr['CreateTempTable'] = $objDb->db_db_query($sql);



### Prepare Temp Table Data
$insertAry = array();
for ($i=0, $i_max=$numOfTeacherWriting; $i<$i_max; $i++) {
	
	$_writingId = $teacherWritingInfoArr[$i]['WRITING_ID'];

	$_writingName = $teacherWritingInfoArr[$i]['TITLE'];
	$_writingNameLink = '<a href="/home/eLearning/w2/index.php?mod=report&task=listWritingClassReport&r_contentCode='.$r_contentCode.'&r_writingId='.$_writingId.'">'.$_writingName.'</a>';
	
	$_schemeCode = $teacherWritingInfoArr[$i]['SCHEME_CODE'];
	$_contentAttributes = $w2_libW2->getContentAttributes($r_contentCode,$_schemeCode);

	$_schemeName = $_contentAttributes["name"];
	$_level = $r_contentCode==strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])?$_contentAttributes["type_super"]:'';
	$_category = $r_contentCode==strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])?$_contentAttributes["type"]:'';
	
	$_startDate = getDateByDateTime($teacherWritingInfoArr[$i]['START_DATE_TIME']);
	$_startDateDisplay = (is_date_empty($teacherWritingInfoArr[$i]['START_DATE_TIME']))? $Lang['General']['EmptySymbol'] : $_startDate;
	
	$_endDate = getDateByDateTime($teacherWritingInfoArr[$i]['END_DATE_TIME']);
	$_endDateDisplay = (is_date_empty($teacherWritingInfoArr[$i]['END_DATE_TIME']))? $Lang['General']['EmptySymbol'] : $_endDate;
	
	$_writingStatus = $w2_libW2->getStatusTextByCode($teacherWritingInfoArr[$i]['RECORD_STATUS']);

	$_completedStudentCount = count((array)$writingCompletedStudentAssoAry[$_writingId]);
	
	$_allStudentCount = count((array)$writingAllStudentAssoAry[$_writingId]);
	unset($_paraAssoAry['r_handinSubmitStatus']);
	
	$_completed = $_completedStudentCount.' / '.$_allStudentCount;
	
	//$insertAry[$i] = " ('".$objDb->Get_Safe_Sql_Query($_writingName)."', '".$objDb->Get_Safe_Sql_Query($_schemeName)."', '".$_startDate."', '".$_endDate."', '".$objDb->Get_Safe_Sql_Query($_writingStatus)."', '".$_completed."', '".$_toMark."') ";
	$insertAry[$i] = " ('".$objDb->Get_Safe_Sql_Query($_writingName)."', '".$objDb->Get_Safe_Sql_Query($_writingNameLink)."', '".$objDb->Get_Safe_Sql_Query($_schemeName)."', '".$_level."', '".$_category."', '".$_startDateDisplay."', '".$_endDateDisplay."', '".$_completed."') ";
	
}

### Insert data into Temp Table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$sql = "Insert Into TMP_W2_PAGE Values ".implode(",", (array)$insertAry);
//	debug_r($sql);
	$SuccessArr['InsertTempTable'] = $objDb->db_db_query($sql);
}


### Set Display Table Object's Attributes
$objTableMgr = new libtable_mgr();
$objTableMgr->setSql('Select * From TMP_W2_PAGE');
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);


$objTableMgr->addColumn('WRITING_NAME_LINK', 1, $Lang['W2']['exerciseName'], '27%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='WRITING_NAME');
if($r_contentCode==strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])){

### 20140925 Change Language
$objTableMgr->addColumn('SCHEME_NAME', 1, $Lang['W2']['topic'], '15%');
//$objTableMgr->addColumn('SCHEME_NAME', 1, $Lang['W2']['theme'], '15%'); ###
$objTableMgr->addColumn('LEVEL', 1, $Lang['W2']['level'], '10%');
$objTableMgr->addColumn('CATEGORY', 1, $Lang['W2']['category'], '15%');
}else{
	### 20140925 Change Language
	$objTableMgr->addColumn('SCHEME_NAME', 1, $Lang['W2']['topic'], '40%');
//	$objTableMgr->addColumn('SCHEME_NAME', 1, $Lang['W2']['theme'], '40%'); ###
}
$objTableMgr->addColumn('START_DATE', 1, $Lang['General']['StartDate'], '10%');
$objTableMgr->addColumn('END_DATE', 1, $Lang['General']['EndDate'], '10%');
$objTableMgr->addColumn('COMPLETED', 0, $Lang['W2']['submitted'].' / '.$Lang['W2']['totalStudents'], '13%', $headerClass='', $dataClass='', $extraAttribute='style="text-align:center;"');


$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$r_contentCode.$sortUrl);

$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());
$h_tableResult = $objTableMgr->display();
$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');

$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
include_once('templates/report/listWritingReport.tmpl.php');
$linterface->LAYOUT_STOP();
?>