<?
// using: Siuwan

//with a student login account
//student/listWriting.php
//http://192.168.0.146:31002/home/eLearning/w2/index.php?mod=student&task=listWriting
include_once($PATH_WRT_ROOT."includes/w2/libWritingStudent.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");

$r_contentCode = $_GET['r_contentCode'];

$objDb = new libdb();
//$objWritingStudent = new libWritingStudent($w2_thisStudentID);
//$result = $objWritingStudent->findStudentWritingByContent($r_contentCode);


### Legend display
$h_legend = $w2_libW2->htmlLegendInfo(array($w2_cfg["legendArr"]["incomplete"], $w2_cfg["legendArr"]["completed"], $w2_cfg["legendArr"]["studentWaitingTeacherComment"], $w2_cfg["legendArr"]["studentNeedCheckComment"], $w2_cfg["legendArr"]["peerCommented"]));


### Get the Student Writing Info of this Content
$studentWritingInfoArr = $w2_libW2->findStudentWritingByContent($w2_thisStudentID, $r_contentCode, $writingStatusArr=null, $withinPeriod=true, $allowPeerMarking=null, $withinPeerMarkingPeriod=null,$publicWritingOnly=true);
$numOfStudentWriting = count($studentWritingInfoArr);


### Get the Steps of this Content
$stepInfoArr = $w2_libW2->getStepInfoByContent($r_contentCode);
$numOfStep = count($stepInfoArr);


### Get the Steps Status of the Student
$studentStepStatusInfoArr = $w2_libW2->getStudentWritingStepStatus($w2_thisStudentID);
$studentStepStatusAssoArr = BuildMultiKeyAssoc($studentStepStatusInfoArr, array('USER_ID', 'WRITING_ID', 'STEP_CODE'));


### Get Peer Marking Data
$peerMarkingDataInfoAry = $w2_libW2->getStudentPeerMarkingTargetData($markerUserId=null, $writingIdAry=null, $markedOnly=true, $w2_thisStudentID);
$peerMarkingDataAssoAry = BuildMultiKeyAssoc($peerMarkingDataInfoAry, array('WRITING_ID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
unset($peerMarkingDataInfoAry);


### Prepare temp table for display
$SuccessArr = array();
$sql = '';
$sql .= "Create Temporary Table If Not Exists TMP_W2_PAGE (
			WRITING_NAME varchar(255) default null,
			SCHEME_NAME_LINK text default null,
			SCHEME_NAME text default null,
			COMPLETION_DATE varchar(255) default null,
		";
for ($i=0, $i_max=$numOfStep; $i<$i_max; $i++) {
	$_stepCode = $stepInfoArr[$i]['code'];
	$sql .= "STEP_STATUS_".$_stepCode." text default null";
	
	if ($i < $i_max - 1) {
		$sql .= ", ";
	}
}
$sql .= "
		) ENGINE=InnoDB Charset=utf8
";
$SuccessArr['CreateTempTable'] = $objDb->db_db_query($sql);


$insertAry = array();
for ($i=0, $i_max=$numOfStudentWriting; $i<$i_max; $i++) {
	$_writingId = $studentWritingInfoArr[$i]['WRITING_ID'];
	$_writingName = $studentWritingInfoArr[$i]['TITLE'];
	$_schemeCode = $studentWritingInfoArr[$i]['SCHEME_CODE'];
	$_writingStudentId = $studentWritingInfoArr[$i]['WRITING_STUDENT_ID'];
	
	$_linkId = 'schemeNameLink_'.$_writingId;
	$_onMouseOut = 'onmouseout="hideFloatLayer();"';
	$_onMouseOver = 'onmouseover="loadWritingIntroFloatLayer(\''.$_writingId.'\', \''.$_linkId.'\', \'layer_ex_brief\');"';
	$_schemeName = $w2_libW2->getSchemeNameBySchemeCode($_schemeCode,$r_contentCode); 
	$_schemeNameLink = '<a id="'.$_linkId.'" href="?mod=handin&task=viewHandIn&r_contentCode='.$r_contentCode.'&r_writingId='.$_writingId.'" '.$_onMouseOut.' '.$_onMouseOver.'>'.$_schemeName.'</a>';
	
	$_wirtingEndTime = $studentWritingInfoArr[$i]['END_DATE_TIME'];
	$_completionDate = (is_date_empty($_wirtingEndTime))? $Lang['General']['EmptySymbol'] : getDateByDateTime($_wirtingEndTime);
	
	$insertAry[$i] = "";
	$insertAry[$i] .= " ('".$objDb->Get_Safe_Sql_Query($_writingName)."', '".$objDb->Get_Safe_Sql_Query($_schemeNameLink)."', '".$objDb->Get_Safe_Sql_Query($_schemeName)."', '".$_completionDate."'";
	
	$_stepIconClickable = true;
	for ($j=0, $j_max=$numOfStep; $j<$j_max; $j++) {
		$_stepCode = $stepInfoArr[$j]['code'];
		
		$_stepId = $studentStepStatusAssoArr[$w2_thisStudentID][$_writingId][$_stepCode]['STEP_ID'];
		$_stepStatus = $studentStepStatusAssoArr[$w2_thisStudentID][$_writingId][$_stepCode]['STEP_STATUS'];
		$_commentId = $studentStepStatusAssoArr[$w2_thisStudentID][$_writingId][$_stepCode]['COMMENT_ID'];
		$_commentStatus = $studentStepStatusAssoArr[$w2_thisStudentID][$_writingId][$_stepCode]['COMMENT_STATUS'];
		$_version = $w2_libW2->getStudentStepHandinLatestVersion($_stepId,$w2_thisStudentID);
		if(!empty($_version)){
			$_commentAry = current($w2_libW2->getTeacherComment($_stepId,$w2_thisStudentID,'',$_version));
			$_commentId = $_commentAry['COMMENT_ID'];
			$_commentStatus = $_commentAry['COMMENT_STATUS'];
		}
		// Get Icon Class for different step status
		$_stepStatusIconClass = $w2_libW2->getCompleteStatusIconClass($_stepStatus);
		
		// Determine the icon onclick action
		$_stepDisplay = '';
		if ($_stepIconClickable) {
			$_paraAssoAry = array();
			$_paraAssoAry['mod'] = 'handin';
			$_paraAssoAry['task'] = 'viewHandIn';
			$_paraAssoAry['r_contentCode'] = $r_contentCode;
			$_paraAssoAry['r_writingId'] = $_writingId;
			$_paraAssoAry['r_schemeCode'] = $_schemeCode;
			$_paraAssoAry['r_stepCode'] = $_stepCode;
			$_paraAssoAry['r_stepId'] = $_stepId;
			$_paraAssoAry['r_writingStudentId'] = $_writingStudentId;
			$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
			//$_href = '?mod=handin&task=viewHandIn&r_contentCode='.$r_contentCode.'&r_writingId='.$_writingId.'&r_schemeCode='.$_schemeCode.'&r_stepCode='.$_stepCode.'&r_stepId='.$_stepId;
			$_stepDisplay .= '<a class="'.$_stepStatusIconClass.'" href="?'.$_para.'"></a>';
		}
		else {
			$_stepDisplay .= '<span class="'.$_stepStatusIconClass.'"></span>';
		}
		
		// Check if need to show any icons for the step
		$__iconAry = array();
		if ($_commentId) {
			// show comment icon if the teacher has entered comment for this step
			$__iconAry[] = $w2_libW2->getIconCommentHtml($_commentStatus);
		}
		
		if ($j == $numOfStep - 1) {
			// last step => check if show peer comment icon
			$__showPeerCommentIcon = false;
			if (isset($peerMarkingDataAssoAry[$_writingId])) {
				$__showPeerCommentIcon = true;
			}
			if(!empty($_version)&&$__showPeerCommentIcon==false){
				$_peerCommentAry = $w2_libW2->getPeerComment($_writingId,$w2_thisStudentID,$markerID='',$_version);
				if(count($_peerCommentAry)>0){
					$__showPeerCommentIcon = true;	
				}
			}
			
			
			if ($__showPeerCommentIcon) {
				$__iconAry[] = $w2_libW2->getIconPeerCommentHtml();
			}
		}
		
		$__numOfIcon = count($__iconAry);
		if ($__numOfIcon > 0) {
			$_stepDisplay .= '<div class="comment_grp">';
				$_stepDisplay .= implode('', $__iconAry);
			$_stepDisplay .= '</div>';
		}
		
		
		$insertAry[$i] .= ", '".$objDb->Get_Safe_Sql_Query($_stepDisplay)."'";
		
		
		// If the step is not reached yet, the next step becomes not clickable
		if ($_stepStatus == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]) {
			// do nth
		}
		else {
			$_stepIconClickable = false;
		}
	}
	
	$insertAry[$i] .= ")";
}


$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$sql = "Insert Into TMP_W2_PAGE Values ".implode(",", (array)$insertAry);
	$SuccessArr['InsertTempTable'] = $objDb->db_db_query($sql);
}



### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_GET['curPage']))? $_GET['curPage'] : 1;
$numPerPage = (isset($_GET['numPerPage']))? $_GET['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_GET['sortColumnIndex']))? $_GET['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_GET['sortOrder']))? $_GET['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];

$WritingNameWidth = 20;
$WritingThemeWidth = 20;
$CompletionDateWidth = 10;
$StepWidth = floor((100 - $WritingNameWidth - $WritingThemeWidth - $CompletionDateWidth) / $numOfStep);

$objTableMgr = new libtable_mgr();
$objTableMgr->setSql('Select * From TMP_W2_PAGE');
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);

$objTableMgr->addColumn('WRITING_NAME', 1, $Lang['W2']['exerciseName'], $WritingNameWidth.'%');
$objTableMgr->addColumn('SCHEME_NAME_LINK', 1, $Lang['W2']['theme'], $WritingThemeWidth.'%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='SCHEME_NAME');
$objTableMgr->addColumn('COMPLETION_DATE', 1, $Lang['W2']['expectedCompletionDate'], $CompletionDateWidth.'%');
for ($i=0; $i<$numOfStep; $i++) {
	$_stepCode = $stepInfoArr[$i]['code'];
	$_stepTitle = Get_Lang_Selection($stepInfoArr[$i]['titleChi'], $stepInfoArr[$i]['titleEng']);
	
	$objTableMgr->addColumn('STEP_STATUS_'.$_stepCode, 0, $_stepTitle, $StepWidth.'%', 'icon_status', 'icon_status');
}


$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$r_contentCode);

//$objTableMgr->setDisplayResultSet($arr);
//$objTableMgr->setResultSetTotalRecord($int);


$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());

$h_tableResult = $objTableMgr->display();
$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');

$linterface->LAYOUT_START();
include_once('templates/writing/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>