<?
// using: Adam
$r_contentCode = $_GET['r_contentCode'];
$r_writingId = $_GET['r_writingId'];
$r_handinSubmitStatus = ($_GET['r_handinSubmitStatus'])? $_GET['r_handinSubmitStatus'] : null;
$r_fromToBeMarked = $_GET['r_fromToBeMarked'];

$objDb = new libdb();


### Legend display
$h_legend = $w2_libW2->htmlLegendInfo(array($w2_cfg["legendArr"]["incomplete"], $w2_cfg["legendArr"]["completed"], $w2_cfg["legendArr"]["teacherNeedProvideComment"], $w2_cfg["legendArr"]["teacherAlreadyCommented"], $w2_cfg["legendArr"]["peerCommented"]));


### Get the Steps of this Content
$stepInfoArr = $w2_libW2->getStepInfoByContent($r_contentCode);
$numOfStep = count($stepInfoArr);

### Get Student Writing Info
$writingStudentInfoAry = $w2_libW2->getWritingStudentInfoAry($r_contentCode, $r_writingId, $r_handinSubmitStatus);
$numOfStudent = count($writingStudentInfoAry);

### Get Student Steps Info
$studentStepStatusInfoArr = $w2_libW2->getStudentWritingStepStatus($studentIdAry=null, $r_writingId, $stepIdAry=null);
$studentStepStatusAssoArr = BuildMultiKeyAssoc($studentStepStatusInfoArr, array('USER_ID', 'WRITING_ID', 'STEP_CODE'));

if ($r_fromToBeMarked) {
	$toBeMarkedStudentInfoAry = $w2_libW2->getWritingToMarkStudent($r_writingId);
	$toBeMarkedStudentIdAry = Get_Array_By_Key($toBeMarkedStudentInfoAry[$r_writingId], 'USER_ID');
}

### Get Student Peer Marking Data
$peerMarkingDataInfoAry = $w2_libW2->getStudentPeerMarkingTargetData($markerUserIdAry=null, $r_writingId, $markedOnly=true);
$peerMarkingDataAssoAry = BuildMultiKeyAssoc($peerMarkingDataInfoAry, 'TARGET_USER_ID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
unset($peerMarkingDataInfoAry);


### Prepare temp table for display
$SuccessArr = array();
$sql = '';
$sql .= "Create Temporary Table If Not Exists TMP_W2_PAGE (
			CLASS_NAME varchar(255) default null,
			CLASS_NUMBER int(8) default null,
			CLASS_NUMBER_DISPLAY varchar(255) default null,
			STUDENT_NAME varchar(255) default null,
			STUDENT_NAME_LINK varchar(255) default null,
			SUBMISSION_DATE date default null,
			WRITING_STATUS tinyint(2) default null,
";
for ($i=0, $i_max=$numOfStep; $i<$i_max; $i++) {
	$_stepCode = $stepInfoArr[$i]['code'];
	$sql .= "STEP_STATUS_".$_stepCode." text default null";
	
	if ($i < $i_max-1) {
		$sql .= ", ";
	}
}
$sql .= "	
		) ENGINE=InnoDB Charset=utf8
";
$SuccessArr['CreateTempTable'] = $objDb->db_db_query($sql);


### Prepare Temp Table Data
$insertAry = array();
for ($i=0, $i_max=$numOfStudent; $i<$i_max; $i++) {
	$_studentId = $writingStudentInfoAry[$i]['USER_ID'];


	if ($r_fromToBeMarked && !in_array($_studentId, (array)$toBeMarkedStudentIdAry)) {
		continue;
	}
//	if ($r_fromToBeMarked) {
//		$stepInfoAry = $w2_libW2->getWritingStep((array)$r_writingId);
//		$_stepID = $stepInfoAry[0]['STEP_ID'];
//		debug_r($writingStudentInfoAry[$i]);
//		$version = $w2_libW2->getStudentStepHandinLatestVersion($_stepID,$_studentId);
//		$teacherCommentList = $w2_libW2->getTeacherComment($_stepID,$_studentId,null,$version);
//		if($teacherCommentList !=''){
//			continue;
//		}
//	}
	
	$_writingId = $writingStudentInfoAry[$i]['WRITING_ID'];
	$_schemeCode = $writingStudentInfoAry[$i]['SCHEME_CODE'];
	$_className = $writingStudentInfoAry[$i]['CLASS_NAME'];
	$_classNumber = $writingStudentInfoAry[$i]['CLASS_NUMBER'];
	$_classNumberDisplay = ($_classNumber=='' || $_classNumber==0)? $Lang['General']['EmptySymbol'] : $_classNumber;
	$_studentName = Get_Lang_Selection($writingStudentInfoAry[$i]['USER_CHINESE_NAME'], $writingStudentInfoAry[$i]['USER_ENGLISH_NAME']);
	$_submissionDate = $writingStudentInfoAry[$i]['HANDIN_SUBMIT_STATUS_DATE'];
	$_submissionStatus = $writingStudentInfoAry[$i]['HANDIN_SUBMIT_STATUS'];
	$_writingStudentId = $writingStudentInfoAry[$i]['WRITING_STUDENT_ID'];
	
	$_insertAry = array();
	$_insertAry['CLASS_NAME'] = "'".$objDb->Get_Safe_Sql_Query($_className)."'";
	$_insertAry['CLASS_NUMBER'] = "'".$_classNumber."'";
	$_insertAry['CLASS_NUMBER_DISPLAY'] = "'".$_classNumberDisplay."'";
	$_insertAry['STUDENT_NAME'] = "'".$objDb->Get_Safe_Sql_Query($_studentName)."'";
	//$_insertAry['STUDENT_NAME_LINK'] = "'".$objDb->Get_Safe_Sql_Query($_studentNameLink)."'";
	$_insertAry['STUDENT_NAME_LINK'] = "'".$objDb->Get_Safe_Sql_Query($_studentName)."'";
	$_insertAry['SUBMISSION_DATE'] = "'".$_submissionDate."'";
	$_insertAry['WRITING_STATUS'] = "'".$_submissionStatus."'";
	
	### Student Step Status Display
	for ($j=0, $j_max=$numOfStep; $j<$j_max; $j++) {
		$_stepCode = $stepInfoArr[$j]['code'];
		$_stepDbField = 'STEP_STATUS_'.$_stepCode;
		
		$_stepId = $studentStepStatusAssoArr[$_studentId][$_writingId][$_stepCode]['STEP_ID'];
		$_stepStatus = $studentStepStatusAssoArr[$_studentId][$_writingId][$_stepCode]['STEP_STATUS'];
		$_commentId = $studentStepStatusAssoArr[$_studentId][$_writingId][$_stepCode]['COMMENT_ID'];
		$_commentStatus = $studentStepStatusAssoArr[$_studentId][$_writingId][$_stepCode]['COMMENT_STATUS'];
		$_version = $w2_libW2->getStudentStepHandinLatestVersion($_stepId,$_studentId); 
		if(!empty($_version)){
			$_commentAry = current($w2_libW2->getTeacherComment($_stepId,$_studentId,'',$_version));
			$_commentId = $_commentAry['COMMENT_ID'];
			$_commentStatus = $_commentAry['COMMENT_STATUS'];
		}
		// Get Icon Class for different step status
		$_stepStatusIconClass = $w2_libW2->getCompleteStatusIconClass($_stepStatus);
		
		// Determine the icon onclick action
		$_stepDisplay = '';
		//if ($_stepStatus==$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]) {
			// if student submitted => allow teacher to click into the step to view student's work
			$_paraAssoAry = array();
			$_paraAssoAry['mod'] = 'handin';
			$_paraAssoAry['task'] = 'viewHandIn';
			$_paraAssoAry['r_action'] = $w2_cfg["actionArr"]["marking"];
			$_paraAssoAry['r_contentCode'] = $r_contentCode;
			$_paraAssoAry['r_handinSubmitStatus'] = $r_handinSubmitStatus;
			$_paraAssoAry['r_fromToBeMarked'] = $r_fromToBeMarked;
			$_paraAssoAry['r_writingId'] = $_writingId;
			$_paraAssoAry['r_schemeCode'] = $_schemeCode;
			$_paraAssoAry['r_stepCode'] = $_stepCode;
			$_paraAssoAry['r_stepId'] = $_stepId;
			$_paraAssoAry['isPretentStudent'] = 1;
			$_paraAssoAry['p_studentId'] = $_studentId;
			$_paraAssoAry['r_writingStudentId'] = $_writingStudentId;
			$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
			
			$_stepDisplay .= '<a class="'.$_stepStatusIconClass.'" href="?'.$_para.'"></a>';
		//}
		//else {
		//	$_stepDisplay .= '<span class="'.$_stepStatusIconClass.'"></span>';
		//}
		
		$__iconAry = array();
		
		if ($_commentId) {
			// show comment icon if the teacher has entered comment for this step
			$__iconAry[] = $w2_libW2->getIconCommentHtml($_commentStatus);
		}
		
		if ($j == $numOfStep-1) {
			// last step => check show peer comment icon or not
			$_hasPeerCommented = false;
			if (isset($peerMarkingDataAssoAry[$_studentId])) {
				$_hasPeerCommented = true;
			}
			if(!empty($_version)&&$__showPeerCommentIcon==false){
				$_peerCommentAry = $w2_libW2->getPeerComment($_writingId,$_studentId,$markerID='',$_version);
				if(count($_peerCommentAry)>0){
					$__showPeerCommentIcon = true;	
				}
			}
			if ($_hasPeerCommented) {
				$__iconAry[] = $w2_libW2->getIconPeerCommentHtml();
			}
		}
		
		$__numOfIcon = count($__iconAry);
		if ($__numOfIcon > 0) {
			$_stepDisplay .= '<div class="comment_grp">';
				$_stepDisplay .= implode('', $__iconAry);
			$_stepDisplay .= '</div>';
		}
		
		$_insertAry[$_stepDbField] = "'".$_stepDisplay."'";
	}
	
	$insertAry[$i] = "(".implode(',', (array)$_insertAry).")";
}


### Insert data into Temp Table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$sql = "Insert Into TMP_W2_PAGE Values ".implode(",", (array)$insertAry);
	$SuccessArr['InsertTempTable'] = $objDb->db_db_query($sql);
}


### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_GET['curPage']))? $_GET['curPage'] : 1;
$numPerPage = (isset($_GET['numPerPage']))? $_GET['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_GET['sortColumnIndex']))? $_GET['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_GET['sortOrder']))? $_GET['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];

$sql = "";
$sql .= "Select
				If (CLASS_NAME='', '".$Lang['General']['EmptySymbol']."', CLASS_NAME) AS CLASS_NAME,
				CLASS_NUMBER_DISPLAY,
				STUDENT_NAME_LINK,
				If (SUBMISSION_DATE='0000-00-00', '".$Lang['General']['EmptySymbol']."', SUBMISSION_DATE) AS SUBMISSION_DATE,
				WRITING_STATUS,
				STUDENT_NAME,
				CLASS_NUMBER,
		";
for ($i=0, $i_max=$numOfStep; $i<$i_max; $i++) {
	$_stepCode = $stepInfoArr[$i]['code'];
	$sql .= " STEP_STATUS_".$_stepCode." ";
	
	if ($i < $i_max-1) {
		$sql .= ", ";
	}
}
$sql .= "From
				TMP_W2_PAGE
		";
		
### Set Display Table Object's Attributes
$ClassWidth = 12;
$ClassNoWidth = 10;
$StudentNameWidth = 20;
$SubmissionDateWidth = 15;
$StepWidth = floor((100 - $ClassWidth - $ClassNoWidth - $StudentNameWidth - $SubmissionDateWidth) / $numOfStep);

$objTableMgr = new libtable_mgr();
$objTableMgr->setSql($sql);
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);
$objTableMgr->addExtraSort('CLASS_NUMBER', 0);
$objTableMgr->setSubmitMode(2);

$objTableMgr->addColumn($dbField='', $isSortable=1, $uiTitle=$Lang['SysMgr']['FormClassMapping']['Class'], $ClassWidth.'%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='CLASS_NAME', $rowspan=1, $colspan=1, $rowLevel=1);
$objTableMgr->addColumn($dbField='', $isSortable=1, $uiTitle=$Lang['SysMgr']['FormClassMapping']['ClassNo'], $ClassNoWidth.'%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='CLASS_NUMBER', $rowspan=1, $colspan=1, $rowLevel=1);
$objTableMgr->addColumn($dbField='', $isSortable=1, $uiTitle=$Lang['General']['Name'], $StudentNameWidth.'%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='STUDENT_NAME', $rowspan=1, $colspan=1, $rowLevel=1);
$objTableMgr->addColumn($dbField='', $isSortable=1, $uiTitle=$Lang['W2']['submissionDate'], $SubmissionDateWidth.'%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='SUBMISSION_DATE', $rowspan=1, $colspan=1, $rowLevel=1);
$objTableMgr->addColumn($dbField='', $isSortable=0, $uiTitle=$Lang['W2']['steps'], $width='', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='', $rowspan=1, $colspan=$numOfStep, $rowLevel=1);

$objTableMgr->addColumn($dbField='CLASS_NAME', $isSortable=0, $uiTitle='', $width='', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='', $rowspan=1, $colspan=1, $rowLevel=2);
$objTableMgr->addColumn($dbField='CLASS_NUMBER_DISPLAY', $isSortable=0, $uiTitle='', $width='', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='CLASS_NUMBER', $rowspan=1, $colspan=1, $rowLevel=2);
$objTableMgr->addColumn($dbField='STUDENT_NAME_LINK', $isSortable=0, $uiTitle='', $width='', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='', $rowspan=1, $colspan=1, $rowLevel=2);
$objTableMgr->addColumn($dbField='SUBMISSION_DATE', $isSortable=0, $uiTitle='', $width='', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='', $rowspan=1, $colspan=1, $rowLevel=2);

for ($i=0, $i_max=$numOfStep; $i<$i_max; $i++) {
	$_stepCode = $stepInfoArr[$i]['code'];
	
	$_stepDbField = 'STEP_STATUS_'.$_stepCode;
	$_stepIndex = $i + 1;
	$_stepNeedApproval = true;
	$_stepClass = ($_stepNeedApproval)? 'icon_status need_approval' : 'icon_status';
	
	$objTableMgr->addColumn($_stepDbField, $isSortable=0, '<span>'.$_stepIndex.'</span>', $StepWidth.'%', $headerClass=$_stepClass, $dataClass='icon_status', $extraAttribute='', $sortDbField='', $rowspan=1, $colspan=1, $rowLevel=2);
}

$objTableMgr->setHeaderTrClassAry(array('', 'step record_bottom'));
$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$paraAssoAry = array();
$paraAssoAry['mod'] = $mod;
$paraAssoAry['task'] = $task;
$paraAssoAry['r_contentCode'] = $r_contentCode;
$paraAssoAry['r_writingId'] = $_writingId;
$paraAssoAry['r_handinSubmitStatus'] = $r_handinSubmitStatus;
$para = $w2_libW2->getUrlParaByAssoAry($paraAssoAry);
$objTableMgr->setSubmitExtraPara('?'.$para);
$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());
$h_tableResult = $objTableMgr->display();

$linterface->LAYOUT_START();
include_once('templates/writing/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>