<?
//using: Adam
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingXml.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingStudent.php");

$r_contentCode = $_GET['r_contentCode'];
$r_schemeCode = $_GET['r_schemeCode'];
$r_writingId = $_GET['r_writingId'];
$p_studentId = $_GET['p_studentId'];
$r_writingStudentId = $_GET['r_writingStudentId'];
$r_stepId = $_GET['r_stepId'];
$stepCode = ($r_contentCode=='sci')?'c6':'c5';

$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
# Template Change 
$templateID =  $objWriting->getTemplateID() | (string) '1';	// default 1
$clipArtID =  $objWriting->getClipArtID() | (string) '1';	// default 1

$templateID =  str_pad($templateID,  2, "0", STR_PAD_LEFT);
$clipArtID =  str_pad($clipArtID,  2, "0", STR_PAD_LEFT);

$sharedWriting = findShareWritingByStudent($p_studentId, $r_contentCode,$r_writingStudentId);
$sharedWriting = $sharedWriting[0];

$writingData =  getWritingAns($r_writingId,$p_studentId,$stepCode);
$writingData = $writingData[0]['WRITING'];

$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');

$version = $w2_libW2->getStudentStepHandinLatestVersion($r_stepId,$p_studentId);
$markCommentHTML = $w2_libW2->getDisplayMarkingLayerHTML($r_stepId,$p_studentId,$r_writingId,$r_contentCode.'_'.$r_schemeCode.'_'.$stepCode.'_ans1',$version);

####################################### Presentation Layer #################################################
$linterface->LAYOUT_START();
include_once('templates/'.$mod.'/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();


###################################### Helper Function Layer ##############################################
/**
* REMARKS: NOT THE SAME FUNCTION AS viewHandIn.php!
* Return STEP 5/6 answer for a give writing id 
* 
* @param : INT $writingid, Writing ID
* @param : INT $studentID, Ans for the Student ID 
* @return : String HTML for the walking step
*/
function getWritingAns($writingid,$studentID, $stepCode){
	global $intranet_db;
//SUBSTR(h.ANSWER, 1,100) as "WRITING"
	$sql= 'select	
					h.ANSWER as "WRITING"
			from 
					'.$intranet_db.'.W2_WRITING as w 
					inner join '.$intranet_db.'.W2_STEP as step on step.WRITING_ID = w.WRITING_ID
					inner join '.$intranet_db.'.W2_STEP_HANDIN as h on h.STEP_ID = step.STEP_ID
			where
					h.USER_ID = '.$studentID.' and w.WRITING_ID = '.$writingid.' and step.CODE = "'.$stepCode.'" ';
	$objDB  = new libDB();
	$result = $objDB->returnResultSet($sql);

	return $result;
}

/////////////////////////////////////////////


function findShareWritingByStudentSQL($studentID, $contentCode,$r_writingStudentId){
	global $intranet_db,$w2_cfg;
	
	$nameField = getNameFieldByLang2("u.");
	$archiveNameField = getNameFieldByLang2("ua.");
	
	$sql = "Select hw.WRITING_ID, hw.Title as WRITING_NAME, hw.SCHEME_CODE, hs.USER_ID,
			h.DATE_INPUT as SHARING_DATE, step.STEP_ID, hs.WRITING_STUDENT_ID,
			IF(ua.UserID is not null, $archiveNameField, $nameField) as STUDENT_NAME,
			u.ClassName, u.ClassNumber
			from ".$intranet_db.".W2_WRITING_STUDENT s 
			inner join ".$intranet_db.".W2_WRITING w on s.WRITING_ID = w.WRITING_ID
			inner join ".$intranet_db.".W2_SHARE_TO_STUDENT_HANDIN h on w.SCHEME_CODE = h.SCHEME_CODE
			inner join ".$intranet_db.".W2_STEP step on h.STEP_ID = step.STEP_ID 
			inner join ".$intranet_db.".W2_WRITING_STUDENT hs on step.WRITING_ID = hs.WRITING_ID and hs.USER_ID = h.STUDENT_ID
			inner join ".$intranet_db.".W2_WRITING hw on hs.WRITING_ID = hw.WRITING_ID
			left join ".$intranet_db.".INTRANET_USER u on h.STUDENT_ID = u.UserID
			left join ".$intranet_db.".INTRANET_ARCHIVE_USER ua on h.STUDENT_ID = ua.UserID
			where s.USER_ID = '$studentID'
			and w.CONTENT_CODE = '$contentCode'
			and hw.CONTENT_CODE = '$contentCode'
			and w.END_DATE_TIME < now()
			and hs.WRITING_STUDENT_ID = '{$r_writingStudentId}'
			group by hw.WRITING_ID, hw.Title, hs.USER_ID
			";		
	return $sql;		
}
	
function findShareWritingByStudent($studentID, $contentCode,$r_writingStudentId){
	$objDB  = new libDB();
	$sql = findShareWritingByStudentSQL($studentID, $contentCode,$r_writingStudentId);
	
	$result = $objDB->returnResultSet($sql);
	return $result;
}



?>