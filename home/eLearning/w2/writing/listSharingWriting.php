<?
//using: Adam
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingXml.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingStudent.php");
include_once($PATH_WRT_ROOT."includes/w2/libw2.php");

$r_contentCode = $_GET['r_contentCode'];

$objDB = new libdb();
define(MAX_NAME_SHOWN,40);
define(MAX_TEXT_SHOWN,300);
### Legend display
//$h_legend = $w2_libW2->htmlLegendInfo(array($w2_cfg["legendArr"]["studentNeedCheckComment"], $w2_cfg["legendArr"]["peerCommented"]));

### Navigator
//$h_navigator = $Lang['W2']['home'].' > '.$Lang['W2']['shareWriting'];

$stepCode = ($r_contentCode=='sci')?'c6':'c5';

$w2_libW2 = new libW2();
$w2_thisIsAdmin = $w2_libW2->isAdminUser();

$w2_thisStudentID = $w2_thisIsAdmin?null:$w2_thisStudentID;
$sharingWritingInfoArr = $w2_libW2->findShareWritingByStudent($w2_thisStudentID, $r_contentCode);
$sharedWritingList = array();

for($i=0, $countWriting = count($sharingWritingInfoArr); $i< $countWriting;++$i){
	$teacherCommentList = array();
	$peerCommentList = array();
	
	$_writingID = $sharingWritingInfoArr[$i]['WRITING_ID'];
	$_schemeCode = $sharingWritingInfoArr[$i]['SCHEME_CODE'];
//	$_schemeName = $w2_libW2->getSchemeNameBySchemeCode($_schemeCode,$r_contentCode);
	$_userID = $sharingWritingInfoArr[$i]['USER_ID'];
//	$_sharingDate = $sharingWritingInfoArr[$i]['SHARING_DATE'];
	$_stepID = $sharingWritingInfoArr[$i]['STEP_ID'];
	$_writingStudentID= $sharingWritingInfoArr[$i]['WRITING_STUDENT_ID'];
	
	$w2_objContent = libW2ContentFactory::createContent($r_contentCode);
	$finalStepCode = $w2_objContent->getWritingTaskStepCode();
	
	
	$_paraAssoAry = array();
//	$_paraAssoAry['mod'] = 'handin';
//	$_paraAssoAry['task'] = 'viewHandIn';
	$_paraAssoAry['mod'] = 'writing';
	$_paraAssoAry['task'] = 'sharedWritingContent';
	$_paraAssoAry['r_action'] = $w2_cfg["actionArr"]["sharing"];
	$_paraAssoAry['r_contentCode'] = $r_contentCode;
	$_paraAssoAry['r_writingId'] = $_writingID;
	$_paraAssoAry['r_schemeCode'] = $_schemeCode;
	$_paraAssoAry['r_stepId'] = $_stepID;
	$_paraAssoAry['r_stepCode'] = $finalStepCode;
	$_paraAssoAry['isPretentStudent'] = 1;
	$_paraAssoAry['p_studentId'] = $_userID;
	$_paraAssoAry['r_writingStudentId'] = $_writingStudentID;
	
	$sharedWritingList[$i]['studentName'] = $sharingWritingInfoArr[$i]['STUDENT_NAME'];
	$sharedWritingList[$i]['writingName'] = substr($sharingWritingInfoArr[$i]['WRITING_NAME'],0,MAX_NAME_SHOWN);
	
	if(strlen($sharingWritingInfoArr[$i]['WRITING_NAME'])>MAX_NAME_SHOWN){
		$sharedWritingList[$i]['writingName'] .= "...";
	}
	$sharedWritingList[$i]['className'] = $sharingWritingInfoArr[$i]['ClassName'];
	$sharedWritingList[$i]['classNumber'] = $sharingWritingInfoArr[$i]['ClassNumber'];
	$sharedWritingList[$i]['link'] = '?'.$w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
	
	$writingData =  getWritingAns($_writingID,$_userID,$stepCode);
	$abstract = strip_tags($writingData[0]['WRITING']);
	$sharedWritingList[$i]['abstract'] =substr($abstract,0,MAX_TEXT_SHOWN);
	if(strlen(strip_tags($writingData[0]['WRITING']))>MAX_TEXT_SHOWN){
		$sharedWritingList[$i]['abstract'] .= "...";
	}

	$version = $w2_libW2->getStudentStepHandinLatestVersion($_stepID,$_userID);
	$teacherCommentList = $w2_libW2->getTeacherComment($sharingWritingInfoArr[$i]['STEP_ID'],$_userID,null,$version);
	$peerCommentList = $w2_libW2->getPeerComment($_writingID,$_userID,$markerID='',$version,$includeNullComment=false);

	foreach($teacherCommentList as $teacherComment){
		if($teacherComment['CONTENT']!=null){
			++$sharedWritingList[$i]['comment'];
		}
		if($teacherComment['STICKER']!=null){
			$sharedWritingList[$i]['sticker']+= count( explode(':', $teacherComment['STICKER']));
		}
	}
	foreach($peerCommentList as $peerComment){
		if($peerComment['CONTENT']!=null){
			++$sharedWritingList[$i]['comment'];
		}
		if($peerComment['STICKER']!=null){
			$sharedWritingList[$i]['sticker']+= count( explode(':', $peerComment['STICKER']));
		}
	}

	
}

//$sharedWritingList = $w2_libW2->findShareWritingByStudent($w2_thisStudentID, $r_contentCode);

$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');

####################################### Presentation Layer #################################################

$linterface->LAYOUT_START();
include_once('templates/'.$mod.'/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();


###################################### Helper Function Layer ##############################################

/**
* REMARKS: NOT THE SAME FUNCTION AS viewHandIn.php!
* Return STEP 5/6 answer for a give writing id 
* 
* @param : INT $writingid, Writing ID
* @param : INT $studentID, Ans for the Student ID 
* @return : String HTML for the walking step
*/
function getWritingAns($writingid,$studentID, $stepCode){
	global $intranet_db;
//SUBSTR(h.ANSWER, 1,100) as "WRITING"
	$sql= 'select	
					h.ANSWER as "WRITING"
			from 
					'.$intranet_db.'.W2_WRITING as w 
					inner join '.$intranet_db.'.W2_STEP as step on step.WRITING_ID = w.WRITING_ID
					inner join '.$intranet_db.'.W2_STEP_HANDIN as h on h.STEP_ID = step.STEP_ID
			where
					h.USER_ID = '.$studentID.' and w.WRITING_ID = '.$writingid.' and step.CODE = "'.$stepCode.'" ';
	$objDB  = new libDB();
	$result = $objDB->returnResultSet($sql);

	return $result;
}

?>
