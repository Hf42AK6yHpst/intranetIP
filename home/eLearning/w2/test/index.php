<?
$content = array();
$school = array();

$content = array('A','B','C');
$school = array('SchoolA','SchoolB','SchoolC');

$h = '';

$noOfColumn = 1;
$js = 'var task = [];'."\n";
	$h .='<tr height="40"><td align="center">Content</td>';
	for($s = 0,$sMax = count($school);$s <$sMax; $s++){
		++$noOfColumn;
		$h .= '<td align="center">School No. '.($s + 1).'</td>';
	}
	$h .='</td>';

$columnWidth = 100 / $noOfColumn;
$taskCounter =  0;
for($i = 0,$iMax = count($content);$i < $iMax; $i++){
	$h.='<tr><td align="center" height="40" width="'.$columnWidth.'%">'.$content[$i].'</td>';

	for($s = 0,$sMax = count($school);$s <$sMax; $s++){
		$divId = $content[$i].'_'.$school[$s];
		
		$h .= '<td align="center" height="40" width = "'.$columnWidth.'%" id="'.$divId.'">'.$school[$s].' (waiting to start)</td>';
		$js .= 'task['.$taskCounter++.'] = \''.$divId.'\';'."\n";

	}
	$h.= '</tr>';
}


?>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
<?
	echo $js;
?>
</script>
<style>
.CSSTableGenerator {
	margin:0px;padding:0px;
	width:100%;
	box-shadow: 10px 10px 5px #888888;
	border:1px solid #000000;
	
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
	
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
	
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
	
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}.CSSTableGenerator table{
	width:100%;
	height:100%;
	margin:0px;padding:0px;
}.CSSTableGenerator tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
	
}
.CSSTableGenerator tr:nth-child(odd){ background-color:#ffaa56; }
.CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }.CSSTableGenerator td{
	vertical-align:middle;
	
	
	border:1px solid #000000;
	border-width:0px 1px 1px 0px;
	text-align:left;
	padding:7px;
	font-size:10px;
	font-family:Arial;
	font-weight:normal;
	color:#000000;
}.CSSTableGenerator tr:last-child td{
	border-width:0px 1px 0px 0px;
}.CSSTableGenerator tr td:last-child{
	border-width:0px 0px 1px 0px;
}.CSSTableGenerator tr:last-child td:last-child{
	border-width:0px 0px 0px 0px;
}
.CSSTableGenerator tr:first-child td{
		background:-o-linear-gradient(bottom, #ff7f00 5%, #bf5f00 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ff7f00), color-stop(1, #bf5f00) );
	background:-moz-linear-gradient( center top, #ff7f00 5%, #bf5f00 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#ff7f00", endColorstr="#bf5f00");	background: -o-linear-gradient(top,#ff7f00,bf5f00);

	background-color:#ff7f00;
	border:0px solid #000000;
	text-align:center;
	border-width:0px 0px 1px 1px;
	font-size:14px;
	font-family:Arial;
	font-weight:bold;
	color:#ffffff;
}
.CSSTableGenerator tr:first-child:hover td{
	background:-o-linear-gradient(bottom, #ff7f00 5%, #bf5f00 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ff7f00), color-stop(1, #bf5f00) );
	background:-moz-linear-gradient( center top, #ff7f00 5%, #bf5f00 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#ff7f00", endColorstr="#bf5f00");	background: -o-linear-gradient(top,#ff7f00,bf5f00);

	background-color:#ff7f00;
}
.CSSTableGenerator tr:first-child td:first-child{
	border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
	border-width:0px 0px 1px 1px;
}
</style>
</head>

<body>

<div class="CSSTableGenerator" >
<table border="1">
<?
echo $h;
?>
</table>
</div>
</body>
<script>

$( document ).ready(function() {
	if(task.length>0){
		task.reverse(); 
		transferToSchool(task);
	}
});

function transferToSchool(taskDetails){
	if(taskDetails.length > 0){
		data = taskDetails[taskDetails.length-1];
		

		$('#' + data).html('<img src="/home/eLearning/w2/test/processing.gif" border = "0" height="40" width="49"/>');
		$.ajax({
		type: "POST",
		url: "?mod=test&task=transfer",
		data: { school: data}
		}).done(function( msg ) {
			$('#' + data).html('<font color="green">DONE</font>');
			taskDetails.pop();
			transferToSchool(taskDetails);
		});
	}else{
		alert('action completed!!');
	}


}
</script>
</html>