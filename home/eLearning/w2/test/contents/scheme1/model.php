<?
//this array must be same as the number of step in this scheme and the element must be equal to the directory name;
//$c_model_cfg['step'] = array('c1','c2','c3','c4'); <-- seems useless 20120227 fai

////////////////////////////
//// CONCEPT MAP CONFIG ////
////////////////////////////
//handle which step c1, c2 .. c5 with concept map
$c_model_cfg['withConceptMapStep'] = array();
$c_model_cfg['withConceptMapStep'][]= 'c2';
$c_model_cfg['withConceptMapStep'][]= 'c4';
$c_model_cfg['withConceptMapStep'][]= 'c5';

//Compulsory config value for concept map , may be one value only  (not array)
//This value must be "Exactly" the ans code in a step, any ans code in any step is also ok. But this ans code should not used to answer and step (e.g text, radio..)
//It mainly use for record the concept map update time
$c_model_cfg['withConceptMapStep_ansCode'] = 'codeKKK'; 
?>