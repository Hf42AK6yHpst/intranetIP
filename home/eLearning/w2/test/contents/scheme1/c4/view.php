<?
/*
 $displayMode = ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) ? $w2_cfg['conceptMapDisplayMode']['teacherPreivewImage']: $w2_cfg['conceptMapDisplayMode']['studentAns'];

 $withDefaultConceptMap = $objWriting->getWithDefaultConceptMap();

 $html = $w2_libW2->findPowerConceptLinkForStudent($w2_thisStudentID,$objWriting,$w2_classRoomID,$studentHandInAllowAction,$withDefaultConceptMap,$displayMode);
 */
?>
<script>
    /*
     $('#handinForm').submit(function() {
     alert('Handler for .submit() called.');
     return false;
     });
     */
</script>

<div class="write_board">
 <div class="write_board_top_step_right">
  <div class="write_board_top_step_left">
   <div class="title">
    <span>
     4</span>
    Drafting
   </div>
  </div>
 </div>
 <div class="write_board_left">
  <div class="write_board_right">
   <div class="content">
    Organize the ideas you built up in the mind map to make them more structured like a diary.
    <br />
    <br />
    Content from this step will be brought to next step ONCE only. Once you have started step 5, any changes in this step will not be brought forward.
   </div>
   <div class="subtitle">
    <span>
     My draft</span>
   </div>
   <div class="content">
    <div class="draft">
     <div class="write">
      <table>
       <tr>
        <td>
         Where did you go? How was the weather? How did you feel?
         <br />
         <textarea rows="5" class="textbox" id="textfield" name="r_question[textarea][<?=$w2_m_ansCode[0] ?>]" <?=$w2_h_answerDefaultDisabledAttr ?>><?=$w2_libW2 -> getAnsValue($w2_s_thisStepAns[$w2_m_ansCode[0]]) ?></textarea>
        </td>
       </tr>
       <tr>
        <td>
         Did something unexpected happen? What did you do? How did you feel?
         <br />
         <textarea  rows="5" class="textbox" id="textfield" name="r_question[textarea][<?=$w2_m_ansCode[1] ?>]" <?=$w2_h_answerDefaultDisabledAttr ?>><?=$w2_libW2 -> getAnsValue($w2_s_thisStepAns[$w2_m_ansCode[1]]) ?></textarea>
        </td>
       </tr>
       <tr>
        <td>
         How did you spend the day at last? What did you feel at the end of the day?
         <br />
         <textarea rows="5" class="textbox" id="textfield" name="r_question[textarea][<?=$w2_m_ansCode[2] ?>]" <?=$w2_h_answerDefaultDisabledAttr ?>><?=$w2_libW2 -> getAnsValue($w2_s_thisStepAns[$w2_m_ansCode[2]]) ?></textarea>
        </td>
       </tr>
      </table>
     </div>
     <div class="ref">

      <span class="title">
       Vocabulary:</span>
      <br />
      <span class="subtitle">
       Adjective describing feelings</span>
      <br />
      <?=$h_vocabDispaly ?>
      <br />
      <a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox" onclick="loadVocabByThickbox('/eng/scheme1/reference/referenceVocab.php','<h1 class=ref>More Vocabulary</h1>','');">
       More...</a>
      <div class="line"></div>

      <span class="title">
       Grammar:</span>
      <br />
      <span class="subtitle">
       Simple past tense</span>
      <br />
      We use the simple past tense to talk about things that happened in the past.
      <br />
      <a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox" onclick="loadContentByThickbox('/eng/scheme1/reference/referenceGrammer.php','<h1 class=ref>Grammar Analysis</h1>','');">
       More...</a>
      <br/>

      <div class="line"></div>

      <span class="title">
       Brainstorming:</span>
      <a href="javaScript:void(0)" OnClick="<?=$h_conceptMapLink ?>">
       Click here</a>
     </div>
    </div>
   </div>
   <p class="spacer"></p>
   <div class="edit_bottom">
    <?php
	echo $h_saveAsDraftButton;
    ?>
    <?
	echo $h_saveAndNextButton;
    ?>
    <?
	echo $h_cancelButton;
    ?>
    <!--<input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="Cancel" />-->
   </div>
  </div>
 </div>
 <div class="write_board_bottom_left">
  <div class="write_board_bottom_right"></div>
 </div>
</div>