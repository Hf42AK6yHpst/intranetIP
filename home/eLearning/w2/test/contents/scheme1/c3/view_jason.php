
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>190 Project - Writing 2.0</title>
<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/w2/common.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/w2/writing20.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/w2/skin_watercolor.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/w2/thickbox.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery-ui-1.7.3.custom.min.js"></script>
<script language="JavaScript" src="/templates/script.js"></script>
<script language="JavaScript" src="/templates/2007script.js"></script>
<script language="JavaScript" src="/templates/brwsniff.js"></script>
<script language="JavaScript" src="/templates/jquery/thickbox.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="/templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="/lang/script.en.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery.highlight.js"></script>
<script language="JavaScript" src="/templates/2009a/js/w2/w2.js"></script>
<script language="JavaScript" src="/templates/json2.js"></script>
</head>

<body>
<div id="Page_container">

<script>
    /* 
     $('#handinForm').submit(function() {
     alert('Handler for .submit() called.');
     return false;
     });
     */
</script>

<div class="write_board">
 <div class="write_board_top_step_right">
  <div class="write_board_top_step_left">
   <div class="title">
    <span>
     3
    </span>
    Related Text-type and Vocabulary
   </div>
  </div>
 </div>
 <div class="write_board_left">
  <div class="write_board_right">
   <div class="content">
    Read the sample writing below to learn about the structure of a diary entry. You may click the buttons below to learn more about related vocabulary and grammar needed for this writing task.
   </div>
   <div class="subtitle">
    <span>
     Sample Writing
    </span>
   </div>
   <div class="btn_right_grp">
    <!--a href="190_Writing2.0_eng_incomplete_step3_pop_ref.htm?KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=600" class="thickbox btn_ref"><span>Teacher's attachment</span></a-->
    <!--<a href="190_Writing2.0_eng_incomplete_step3_pop_vocab.htm?KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=600" class="thickbox btn_vocab"><span>More Vocabulary</span></a>-->

    <a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_vocab" onclick="loadVocabByThickbox('/eng/scheme1/reference/referenceVocab.php','<h1 class=ref>More Vocabulary</h1>','');">
     <span>
      More Vocabulary
     </span>
    </a>

    <!--<a href="190_Writing2.0_eng_incomplete_step3_pop_grammar.htm?KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=600" class="thickbox btn_grammar"><span>Grammar Analysis</span></a>-->
    <!--a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_grammar"
    onclick="loadThickbox('getThickboxHtml_grammar');"><span>Grammar Analysis</span></a-->
    <a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_grammar" onclick="loadContentByThickbox('/eng/scheme1/reference/referenceGrammer.php','<h1 class=ref>Grammar Analysis</h1>','');">
     <span>
      Grammar Analysis
     </span>
    </a>
   </div>
   <div class="content">
    <div class="sample">
     <!-- start : for the upper tag only -->
     <div class="upper">
      <div id="tag_top" class="tag_top_01" style="padding-right:110px">
       <div class="top">
        <span>
         Weather:
        </span>
        the weather of the day when you wrote the entry
       </div><div class="bottom"></div>
      </div>
     </div>
     <br style="clear:both" />
     <!-- end : for the upper tag only -->
     <div class="tag">
      <div id="tag_left" class="tag_left_02 new_tag">
       <div class="top">
        <span>
         Date:
        </span>
        the date when you wrote the entry
       </div><div class="bottom"></div>
      </div>
      <!--div id="tag_left" class="tag_left_03"><div class="top"><span>Greeting:</span> some people start with greeting the diary</div><div class="bottom"></div> </div-->
      <div id="tag_left" class="tag_left_04 new_tag">
       <div class="top">
        <span>
         Introduction:
        </span>
        a short review of the day to bring up the event
       </div><div class="bottom"></div>
      </div>
      <!--div id="tag_left" class="tag_left_05"><div class="top"><span>Body:</span> details about what we did and how we felt</div><div class="bottom"></div> </div-->
      <div id="tag_left" class="tag_left_06 new_tag">
       <div class="top">
        <span>
         Body:
        </span>
        the events are usually presented chronologically
       </div><div class="bottom"></div>
      </div>
      <br/>
      <br/>
      <div id="tag_left" class="tag_left_05 new_tag">
       <div class="top">
        <span>
         Body:
        </span>
        the turning point of the event and the change of feelings
       </div><div class="bottom"></div>
      </div>
      <br/>
      <div id="tag_left" class="tag_left_07 new_tag">
       <div class="top">
        <span>
         Ending:
        </span>
        a concluding remark of the day
       </div><div class="bottom"></div>
      </div>

     </div>
     <div class="essay">
      <span style="float:right">
       Rainy
      </span>
      <div class="tag_label"></div>Sunday, 30 October
      <br />

      <br />
      <div class="tag_label"></div>Today really <em><strong>was</strong></em>
      <span class="keynum">
       (1)
      </span>
      really unforgettable. <em><strong>I never thought a picnic could be so much fun!</strong></em>
      <span class="keynum">
       (2)
      </span>
      !
      <br />
      <br />
      My family are outgoing, so we went to Sai Kung for picnic.
      It was sunny when we set off.
      My brother and I were very <em><strong>excited</strong></em>
      <span class="keynum">
       (3)
      </span>.
      However, when we were about to arrive, the sky turned very dark.
      When we finally reached our picnic sight, it started to rain!
      We had to sit in the car and wait<div class="tag_label"></div>. We were so <em><strong>bored</strong></em> and we couldn&rsquo;t believe that our picnic had been ruined..
      <br />
      <br/>
      <div class="tag_label"></div>It carried on raining so we decided to leave, but just as we were about to drive away, the rain stopped and the sky cleared.
      There was even a rainbow in the sky!
      My brother and I were <em><strong>ecstatic</strong></em>
      <span class="keynum">
       (4)
      </span>
      .
      For my grandmother it was really special because she had never seen a rainbow before!
      She was <em><strong>amazed.</strong></em>
      <br/>
      <br/>
      <div class="tag_label"></div>We had our picnic as we planned.
      It turned out to be a lot <em><strong>more fun</strong></em>
      <span class="keynum">
       (5)
      </span>
      Today really taught me to be prepared and not to loose hope because you never know what is going to happen next!
      By the end of the day we were all <em><strong>exhausted</strong></em>.
      What an unforgettable day!
      <br/>
      <br/>

     </div>
     <div class="key">
      <div id="key_right">
       <div class="top">
        (1) Grammar:
        <br />
        Use the
        <span>
         past tenses
        </span>
        to describe the event and your feelings at that time.
       </div><div class="bottom"></div>
      </div>
      <div id="key_right">
       <div class="top">
        (2) Vocabulary:
        <br />
        <span>
         Use different phrases
        </span>
        to help reocrd feelings.
       </div><div class="bottom"></div>
      </div>
      <!--div id="key_right"><div class="top">(3) Grammar:<br /> Use the <span>present tenses</span>  to describe things that are still ture now.</div><div class="bottom"></div> </div-->
      <div id="key_right">
       <div class="top">
        (3) Vocabulary:
        <br />
        Use
        <span>
         adjectives
        </span>
        to reocrd your feelings.
       </div><div class="bottom"></div>
      </div>
      <div id="key_right">
       <div class="top">
        (4) Grammar:
        <br />
        Use
        <span>
         adverbs
        </span>
        to describe the manner of the action.
       </div><div class="bottom"></div>
       <div id="key_right">
        <div class="top">
         (5) Grammar:
         <br />
         Use
         <span>
          comparative adjectives
         </span>
         to do comparison.
        </div><div class="bottom"></div>
       </div>

      </div>
      <!-- start : for the lower tag only -->
      <br style="clear:both" />
      <!--div class="lower">
      <div id="tag_bottom" class="tag_bottom_01" style="padding-right:100px"><div class="top"></div><div class="bottom"><span>Signature:</span> xxxxxxxxxxxx xxxxxxxxxxx</div> </div>
      </div-->
      <!-- end : for the lower tag only -->
     </div>
    </div>
    <p class="spacer"></p>
    <div class="edit_bottom">
     <?php
	echo $h_nextStepButton;
	echo $h_cancelButton;
     ?>
     <!--<input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="Cancel" />-->
    </div>
   </div>
  </div>
  <div class="write_board_bottom_left">
   <div class="write_board_bottom_right"></div>
  </div>
 </div>


<div id="debug_txt">
</div>

</body>
</html>