<script>
//$('#handinForm').submit(function() {
//  alert('Handler for .submit() called.');
//  return false;
//});

// 20111217 Ivan: moved to /templates/2009a/js/w2/content/type1.js
//function showModelAns(ansNum) {
//	$('#modelAnsSpan_' + ansNum).show();
//	
//	checkEnableSubmitBtn();
//}
//
//function checkEnableSubmitBtn() {
//	var allAnswered = true;
//	
//	// Check if all model answers are clicked
//	$('.modelans').each( function() {
//		if ($(this).is(':hidden')) {
//			allAnswered = false;
//		}
//	});
//	
////	// Check if all answer has input
////	$('.w2_ansInput').each( function() {
////		if ($(this).val().Trim() == '') {
////			allAnswered = false;
////		}
////	});
//	
//	if (allAnswered) {
//		$('#saveButton').attr('disabled', '');
//	}
//	else {
//		$('#saveButton').attr('disabled', 'disabled');
//	}
//}
</script>
		<!-- ************ Main Content Start ************  -->
          
          
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>1</span>Examining the Question</div></div></div>
						<div class="write_board_left"><div class="write_board_right">
                            <div class="content">
                            	Read the following instruction for a writing task. Find out the text type, purpose and the content it requires. <b>Choose or type the correct answer.</b>
                            	<br /><br />
                            	You spent an unforgettable day with your family members. Write in your diary what happened and how you felt. Use around 120 words.
                            </div>
                            <div class="subtitle"><span>Analysis</span></div>
                            <div class="content">
                            	<table>
                                	<tr>
	                                	<td>Text-type</td><td>:</td><td>
	                                	  <select name="r_question[select][<?=$w2_m_ansCode[0]?>]" style="width:80px;" <?=$w2_h_answerDefaultDisabledAttr?>>
	                                        <option value="2" <?=$w2_libW2->getAnsSelected($w2_s_thisStepAns[$w2_m_ansCode[0]], 2)?>>Article</option>
	                                        <option value="3" <?=$w2_libW2->getAnsSelected($w2_s_thisStepAns[$w2_m_ansCode[0]], 3)?>>Blog</option>
	                                        <option value="1" <?=$w2_libW2->getAnsSelected($w2_s_thisStepAns[$w2_m_ansCode[0]], 1)?>>Diary</option>
	                                      </select>
	                                     <!-- <?= $w2_objContent->getTextTypeSelection($w2_m_ansCode[0], $w2_s_thisStepAns[$w2_m_ansCode[0]], $w2_h_answerDefaultDisabledAttr) ?>  -->
	                                     <input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode[0]?>', 'select');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" />
	                                      <span id="modelAnsSpan_<?=$w2_m_ansCode[0]?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>">Diary</span>
	                                	</td>
                                	</tr>
                                    <tr>
                                    	<td>Purpose</td>
                                    	<td>:</td>
                                    	<td>
                                    		<input name="r_question[text][<?=$w2_m_ansCode[1]?>]" type="text" id="textfield" class="w2_ansInput" size="40" value="<?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode[1]])?>" <?=$w2_h_answerDefaultDisabledAttr?> onkeyup="checkEnableSubmitBtn();" />
                                    		<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode[1]?>', 'text');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" /> 
                                    		<span id="modelAnsSpan_<?=$w2_m_ansCode[1]?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>">To record / recount</span>
                                    	</td>
                                    </tr>
                                	<!--tr>
                                	  <td>Audience</td>
                                	  <td>:</td>
                                	  <td>
										  <input name="r_question[text][<?=$w2_m_ansCode[2]?>]" type="text" id="textfield" class="w2_ansInput" size="40" value="<?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode[2]])?>" <?=$w2_h_answerDefaultDisabledAttr?> onkeyup="checkEnableSubmitBtn();" />
                                	  	  <input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode[2]?>', 'text');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" />
                                          <span id="modelAnsSpan_<?=$w2_m_ansCode[2]?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>">Self</span>
                                      </td>
                                	</tr-->
                                    <tr>
                                    	<td>Content</td>
                                    	<td>:</td>
                                    	<td>
                                    		<input name="r_question[text][<?=$w2_m_ansCode[3]?>]" type="text" id="textfield" class="w2_ansInput" size="40" value="<?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode[3]])?>" <?=$w2_h_answerDefaultDisabledAttr?> onkeyup="checkEnableSubmitBtn();" /> 
                                    		<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode[3]?>', 'text');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" />
                                        	<span id="modelAnsSpan_<?=$w2_m_ansCode[3]?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>">What happened, feelings</span>
                                        </td>
                                    </tr>
                                    <!--tr>
                                    	<td>Other Information</td>
                                    	<td>:</td>
                                      	<td>
                                      		<input name="r_question[text][<?=$w2_m_ansCode[2]?>]" name="textfield" type="text" id="textfield" class="w2_ansInput" size="40" value="<?=$w2_s_thisStepAns[$w2_m_ansCode[2]]?>" <?=$w2_h_answerDefaultDisabledAttr?> onkeyup="checkEnableSubmitBtn();" />
                                        	<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'"  onclick="showModelAns(4);" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" />
                                        	<span id="modelAnsSpan_4" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>">One day, family members</span>
                                        </td>
                                    </tr-->
                                </table>
                          </div>
                           <p class="spacer"></p>
                           <div class="edit_bottom">
                                <?php 
								echo $h_nextStepButton;
								?>
								<?php 
								echo $h_cancelButton;
								?>								
                              </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
                        
          <!-- ********* Main  Content end ************ -->