<?
// using:
//include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/w2ShareContentConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2PublishMenuGenerator.php");
$r_contentCode = $_GET['r_contentCode'];
$r_returnMsgKey = $_GET['r_returnMsgKey'];
$cidList = is_Array($_POST['allSch'])?$_POST['allSch']:(array)$_POST['allSch'];

GLOBAL $w2_cfg;
$objDb = new libdb();
$objW2PublishMenuGenerator =new W2PublishMenuGenerator();
$publishList = $objW2PublishMenuGenerator->getPublishMenu($r_contentCode,false,$cidList);


$h_backButton = $linterface->GET_ACTION_BTN($Lang['W2']['backToPublish'], 'button', 'backToPublish()', 'backBtn');

### Toolbar
if ($w2_libW2->isAdminUser()) {
}

### Table
## Table Width
$h_publishTableWidthSetter='';
$h_publishTableWidthSetter.='<col  width="1%" />'."\n\r";
$h_publishTableWidthSetter.='<col nowrap="nowrap" width="39%" />'."\n\r";
$schCount = count($w2_cfg["schoolCode"]);
for ($i=0; $i<$schCount;$i++)
{
	$h_publishTableWidthSetter.='<col nowrap="nowrap" width="15%" />'."\n\r";
}

foreach( (array)$publishList as $publishItem)
{
	$schemeNumList[]=$publishItem;
}
## Table Header
$h_publishTableHeader='';
$h_publishTableHeader .= "<th>#</th>"."\n\r";
$h_publishTableHeader .= "<th>".$Lang['W2']['topic']."</th>"."\n\r";
foreach( (array)$w2_cfg["schoolCode"] as $schoolDetail)
{
	if($schoolDetail["shortDesc"] != $w2_cfg["schoolCodeCurrent"])
	{
		
		$h_publishTableHeader .= '<th>'.$schoolDetail["shortDesc"].'<br /></th>'."\n\r";
	}
}

## Table Content
$h_publishTableContent='';
$js = 'var task = [];'."\n";
$loadNum = 1;
$columnNum=1;
$taskCounter =  0;
foreach( (array)$publishList as $publishItem)
{
	$h_publishTableContent.="<tr class='normal record_bottom'>";
	$h_publishTableContent .= "<td>${columnNum}</td>"."\n\r";
	$h_publishTableContent .= "<td>".$publishItem["topicName"]."</td>"."\n\r";
	
	foreach( $w2_cfg["schoolCode"] as $schoolDetail)
	{
		if($w2_cfg["schoolCodeCurrent"] != $schoolDetail["shortDesc"])
		{
			$shortDesc = strtolower($schoolDetail["shortDesc"]);
			if ( $publishItem["${shortDesc}Sent"] == true)
			{
				$uploadStatus= "Done";
			}
			else
			{
				$cid = $publishItem['cid'];
				$divId = $publishItem['schemeNum'].'_'.$shortDesc;
				$js .= 'task['.$taskCounter++.'] = [\''.$divId.'\',\''.$cid.'\'];'."\n";		
				$uploadStatus= "<span  id='$divId' data-order='${loadNum}'>Not Yet</span>";
			}
			$h_publishTableContent .= "<td>".$uploadStatus."</td>"."\n\r";
			$loadNum++;
		}
	}
	$h_publishTableContent .="</tr>";
	$columnNum++;
}
###

$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
include_once('templates/admin/publishAll.tmpl.php');
$linterface->LAYOUT_STOP();



####################################################################################
function js_str($s)
{
    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
}

function js_array($array)
{
    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
}


?>