<?
// using: Adam
//include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");


include_once($PATH_WRT_ROOT."includes/w2/libWritingTeacher.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2MenuGenerator.php");

$objDB = new libdb();
$r_contentCode = $_GET['r_contentCode'];
$r_returnMsgKey = $_GET['r_returnMsgKey'];
switch ($r_contentCode) {
	case 'eng':
	case 'ls_eng':
	case 'sci':
		$intranet_hardcode_lang = 'en';
		break;
	case 'chi':
	case 'ls':
		$intranet_hardcode_lang = 'b5';
		break;
}

### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_GET['curPage']))? $_GET['curPage'] : 1;
$numPerPage = (isset($_GET['numPerPage']))? $_GET['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_GET['sortColumnIndex']))? $_GET['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_GET['sortOrder']))? $_GET['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];

### Toolbar
if ($w2_libW2->isAdminUser()) {
		$h_newTemplateBtn = '<a href="/home/eLearning/w2/index.php?mod=admin&task=newTemplateWriting&r_contentCode='.$r_contentCode.'">'.$Lang['Btn']['New'].'</a>';
}

### Prepare temp table for display
$SuccessArr = array();
$sql = "Create Temporary Table If Not Exists TMP_W2_TEMPLATE_PAGE (
			CONTENT_NAME varchar(1000) default null,
			THEME varchar(255) default null,
			CREATE_DATE varchar(255),			
			LEVEL varchar(20) default null,			
			SCHOOLNAME varchar(255) default null,			
			LAST_MODIFIED varchar(255),			
			STATUS int (3)  default 0,
			DELIMITER varchar (2)  default null,
			GENERATE_BTN varchar (1000)  default null,
			EDIT_BTN varchar (255)  default null,
			DELETE_BTN varchar (255)  default null
		) ENGINE=InnoDB Charset=utf8
";
$SuccessArr['CreateTempTable'] = $objDB->db_db_query($sql);


### Add data into a temporary table

## Template data by engine
$W2TemplateMenuGenerator = new W2TemplateMenuGenerator($intranet_hardcode_lang);
$menuList= $W2TemplateMenuGenerator->getTemplateMenu($r_contentCode);
foreach( (array)$menuList as $menuItem)
{
	$sql = "Insert Into TMP_W2_TEMPLATE_PAGE Values ( '".implode("','", (array)$menuItem)."');";
	$SuccessArr['InsertTempTable'] = $objDB->db_db_query($sql);
}

## Old Writing2.0 data
$menuList =  $W2TemplateMenuGenerator->getEngConfigDataList($r_contentCode);
foreach( (array)$menuList as $menuItem)
{
	$sql = "Insert Into TMP_W2_TEMPLATE_PAGE Values ( '".implode("','", (array)$menuItem)."');";
	$SuccessArr['InsertTempTable'] = $objDB->db_db_query($sql);
}

## New data shared by Other schools

### Set Display Table Object's Attributes
$objTableMgr = new libtable_mgr();
$objTableMgr->setSql('Select * From TMP_W2_TEMPLATE_PAGE');
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);

$objTableMgr->addColumn('CONTENT_NAME', 1, $Lang['W2']['topic'], '30%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='CONTENT_NAME');
if($r_contentCode =='eng')
{
	$objTableMgr->addColumn('LEVEL', 1, $Lang['W2']['level'], '8%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='LEVEL');
}
$objTableMgr->addColumn('THEME', 1, $Lang['W2']['textType'], '8%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='THEME');
$objTableMgr->addColumn('LAST_MODIFIED', 1, $Lang['W2']['lastUpdateDate'], '10%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='LAST_MODIFIED');
$objTableMgr->addColumn('SCHOOLNAME', 1, $Lang['W2']['createdBy'], '25%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='SCHOOLNAME');
if($r_contentCode !='eng')
{
	$objTableMgr->addColumn('BLANK', 1, '', '10%');
}

$objTableMgr->addColumn('GENERATE_BTN', 1, '', '7%');
$objTableMgr->addColumn('DELIMITER', 1, '', '1%');
$objTableMgr->addColumn('EDIT_BTN', 1, '', '3%');
$objTableMgr->addColumn('DELIMITER', 1, '', '1%');
$objTableMgr->addColumn('DELETE_BTN', 1, '', '7%');

$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$r_contentCode);

$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());
$h_tableResult = $objTableMgr->display();

$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
include_once('templates/admin/templateManagement.tmpl.php');
$linterface->LAYOUT_STOP();
?>