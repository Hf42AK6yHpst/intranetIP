<?
// using:

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");

$r_contentCode = $_GET['r_contentCode'];
$cid = $_GET['cid'];
$tabNum = isset($_GET['tabNum'])?$_GET['tabNum']:0;
$r_returnMsgKey = $_GET['r_returnMsgKey'];
$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];

### Page title
if( $cid =='')
{
	$h_title = strtoupper($Lang['W2']['newTemplate']);	
}
else
{
	$h_title = strtoupper($Lang['W2']['editTemplate']);
}

### Get template information from database
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
}

$objDB = new libdb();
$getStep4QArrayHTML = getStep4QuestionArrayGenerated($parseData['step4Data']['step4DraftQList']);
### FCK editor objects

$FCKEditor = new FCKeditor ( 'step1Int' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step1Data']['step1Int'];
$step1InstructionEditor = escapeJavaScriptText($FCKEditor->CreateHtml() );

$FCKEditor = new FCKeditor ( 'step2Int' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step2Data']['step2Int'];
$step2InstructionEditor = escapeJavaScriptText($FCKEditor->CreateHtml() );

$FCKEditor = new FCKeditor ( 'step3Int' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step3Data']['step3Int'];
$step3InstructionEditor = escapeJavaScriptText($FCKEditor->CreateHtml() );

$FCKEditor = new FCKeditor ( 'step3Sample' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step3Data']['step3SampleWriting'];
$step3SampleWritingEditor =escapeJavaScriptText( $FCKEditor->CreateHtml() );

$FCKEditor = new FCKeditor ( 'step3Vocab' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step3Data']['step3Vocab'];
$step3VocabEditor = escapeJavaScriptText($FCKEditor->CreateHtml() );

$FCKEditor = new FCKeditor ( 'step3Grammar' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step3Data']['step3Grammar'];
$step3GrammarEditor = escapeJavaScriptText($FCKEditor->CreateHtml() );

$FCKEditor = new FCKeditor ( 'step4Int' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step4Data']['step4Int'];
$step4InstructionEditor = escapeJavaScriptText($FCKEditor->CreateHtml() );

$FCKEditor = new FCKeditor ( 'step5Int' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step5Data']['step5Int'];
$step5InstructionEditor = escapeJavaScriptText($FCKEditor->CreateHtml() );

$FCKEditor = new FCKeditor ( 'step5DefaultWriting' , "100%", "320", "", "", "");
$FCKEditor->Value = $parseData['step5Data']['step5DefaultWriting'];
$step5DefaultWritingEditor = escapeJavaScriptText($FCKEditor->CreateHtml() );



### Category selection box items
$categorySelectionBoxItems = getCategoryItems("step1Cateogry","step1Cateogry",$parseData['category']);
$textTypeSelectionBoxItems = getCategoryItems("step1TextType","step1TextType",$parseData['step1Data']['step1TextType']);


### button panel
$h_save0Button = $linterface->GET_ACTION_BTN('Save', 'button', 'doFormCheck(0);', 'submitBtn');
$h_save1Button = $linterface->GET_ACTION_BTN('Save', 'button', 'doFormCheck(1);', 'submitBtn');
$h_save2Button = $linterface->GET_ACTION_BTN('Save', 'button', 'doFormCheck(2);', 'submitBtn');
$h_save3Button = $linterface->GET_ACTION_BTN('Save', 'button', 'doFormCheck(3);', 'submitBtn');
$h_save4Button = $linterface->GET_ACTION_BTN('Save', 'button', 'doFormCheck(4);', 'submitBtn');
$h_save5Button = $linterface->GET_ACTION_BTN('Save', 'button', 'doFormCheck(5);', 'submitBtn');
$h_submitButton = $linterface->GET_ACTION_BTN('Generate', 'button', 'doFormCheck();', 'submitBtn');
$h_addItemButton = $linterface->GET_ACTION_BTN('Add Item', 'button', 'addDraftField()', 'addItemBtn');

### Form checking warning messages
// divId = {inputId}_warningDiv
$h_inputTopicNameWarning = $linterface->Get_Form_Warning_Msg('r_TopicName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_inputTopicIntroWarning = $linterface->Get_Form_Warning_Msg('r_TopicIntro_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_selectCategoryWarning = $linterface->Get_Form_Warning_Msg('r_SelectedCategory_warningDiv', $Lang['W2']['warningMsgArr']['selectCat'], 'warningDiv_0');
$h_selectLevelWarning = $linterface->Get_Form_Warning_Msg('r_SelectedLevel_warningDiv', $Lang['W2']['warningMsgArr']['selectLevel'], 'warningDiv_0');

$h_inputStep1IntWarning = $linterface->Get_Form_Warning_Msg('r_Step1Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_selectStep1TextTypeWarning = $linterface->Get_Form_Warning_Msg('r_Step1TextType_warningDiv', $Lang['W2']['warningMsgArr']['selectTextType'], 'warningDiv_1');
$h_inputStep1PurposeWarning = $linterface->Get_Form_Warning_Msg('r_Step1Purpose_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_inputStep1ContentWarning = $linterface->Get_Form_Warning_Msg('r_Step1Content_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');

$h_inputStep2IntWarning = $linterface->Get_Form_Warning_Msg('r_Step2Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');

$h_inputStep3IntWarning = $linterface->Get_Form_Warning_Msg('r_Step3Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
$h_inputStep3SampleWritingWarning = $linterface->Get_Form_Warning_Msg('r_Step3SampleWriting_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
$h_inputStep3VocabWarning = $linterface->Get_Form_Warning_Msg('r_Step3Vocab_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
$h_inputStep3GrammarWarning = $linterface->Get_Form_Warning_Msg('r_Step3Grammar_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');

$h_inputStep4IntWarning = $linterface->Get_Form_Warning_Msg('r_Step4Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');
$h_inputStep4DraftQsWarning = $linterface->Get_Form_Warning_Msg('r_Step4DraftQs_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');

$h_inputStep5IntWarning = $linterface->Get_Form_Warning_Msg('r_Step5Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');
$h_inputStep5DefaultWritingWarning = $linterface->Get_Form_Warning_Msg('r_Step5DefaultWriting_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');


$h_submissionInvalidWarning = $linterface->Get_Form_Warning_Msg('submissionInvalidWarnMsg', $Lang['General']['JS_warning']['InvalidDateRange'], 'warningDiv');
$h_mustUpdatePeerMarkingSettingsWarning = $linterface->Get_Form_Warning_Msg('mustUpdatePeerMarkingSettingsWarnDiv', $Lang['W2']['warningMsgArr']['mustUpdatePeerMarkingSettings'], 'warningDiv');


$returnStr .='<div class="step4DraftQColumns[]">'.'\n\r';
$returnStr .='<input name="step4DraftQ[]" type="text" size="80" required />'.'\n\r';
$returnStr .='<input type="button" class="deleteBtn" name="deleteBtn[]" value="delete" />'.'\n\r';
$returnStr .='</div>'.'\n\r';
### content
ob_start();
include('templates/admin/newTemplateWriting.tmpl.php');
$html_content .= ob_get_contents();
ob_end_clean();

//Logic function helper layer
#####################################################################################################
function getCategoryItems($name, $id,$defaultValue='')
{
	global $w2_cfg_contentSetting;
	
	$returnStr='';
	foreach ( (array)$w2_cfg_contentSetting['eng'] as $item)
	{
		$typeList[] = $item['type'];	
	}
	$uniqueTypeList = array_unique($typeList);
	foreach ( (array)$uniqueTypeList as $type)
	{
		$isSelectedHTML = ("$defaultValue" == "$type")?'selected':'';
		$returnStr .= '<option value="'.$type.'" '.$isSelectedHTML.' >'.$type.'</option>';
	}
	return $returnStr;
}

function getStep4QuestionArrayGenerated($parseData)
{
	
	$draftQHTML='';
	foreach( (array)$parseData as $draftQ)
	{
		$draftQHTML .='<div class="step4DraftQColumns[]">';
		$draftQHTML .='<input name="step4DraftQ[]" type="text" class="step4DraftQ" size="80" required value="'.$draftQ.'" />';
		$draftQHTML .='&nbsp;';
		$draftQHTML .='<input type="button" class="deleteBtn" name="deleteBtn[]" value="delete"/>';
		$draftQHTML .='</div>';
	}
	return ( empty($draftQHTML)==true)?'':$draftQHTML;
}
function _frameLayout($title , $content,$bottom){
	$html = <<<HTML
				<form action ="/home/eLearning/w2/index.php" method = "post" id="w2_form" name="w2_form">
					<div class="write_board">
							<div class="write_board_top_step_right">
								<div class="write_board_top_step_left">
									<div class="title">{$title}</div>
								</div>
							</div>
							<div class="write_board_left">
								<div class="write_board_right">
									<div class="content">{$content}</div>
								    <p class="spacer"></p>
									<div class="edit_bottom">{$bottom}</div>
								</div>
							</div>
							<div class="write_board_bottom_left">
								<div class="write_board_bottom_right"></div>
							</div>
					</div>   	
				</form>
HTML;

	return $html;
}

function escapeJavaScriptText($string)
{
    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
}
//Presentation layer
#####################################################################################################

$linterface->LAYOUT_START($returnMsg);

echo _frameLayout($h_title,$html_content,'');
$linterface->LAYOUT_STOP();

exit();
?>