<?php


$r_action = $_POST['r_action'];
$r_stepId = $_POST['r_stepId'];
$r_sticker = $_POST['r_sticker'];
$r_version = $_POST['r_version']?$_POST['r_version']:1;
$r_mark = $_POST['r_mark'];
$r_writingId = trim(stripslashes($_POST['r_writingId']));
$r_studentId = trim(stripslashes($_POST['r_studentId']));
$r_peerMarkingMarkerUserId = trim(stripslashes($_POST['r_peerMarkingMarkerUserId']));
$r_curStepHandinCode = $_POST['r_curStepHandinCode']; 
$passScoreToGSRecord = $_POST['passScoreToGSRecord'];//20140529-w2gsgame
$r_commentId = $_POST['r_commentId'];
$r_comment[0] = trim(stripslashes($_POST['r_comment'][0]));


switch($r_action){
	case $w2_cfg["actionArr"]["marking"]: //teacher
		include_once($PATH_WRT_ROOT.'includes/w2/libStepStudentComment.php');
		$objComment = new libStepStudentComment($r_commentId);
		//comment
		$objComment->setContent($r_comment[0]);
		$objComment->setStepId($r_stepId);
		$objComment->setUserId($r_studentId);
		$objComment->setVersion($r_version);
		$objComment->setSticker($r_sticker);
		$objComment->setCommentStatus($w2_cfg["DB_W2_STEP_STUDENT_COMMENT"]["teacherCommented"]);
		$objComment->setDateModified('now()');
		$objComment->setModifiedBy($_SESSION['UserID']);
		if($objComment->save()){
			//marks
			if($passScoreToGSRecord)//20140529-w2gsgame
				$w2_libW2->insertMarkingScoreToGSGame($passScoreToGSRecord, $r_stepId,$r_studentId,$r_curStepHandinCode,$r_version, $r_mark);
			$w2_libW2->updateStudentStepHandinRecord($r_stepId,$r_studentId,$r_curStepHandinCode,array("MARK"=>$r_mark));
			$r_commentId = $objComment->getCommentId();
		}
		break;		
	case $w2_cfg["actionArr"]["peerMarking"]:
		include_once($PATH_WRT_ROOT.'includes/w2/libW2PeerMarkingData.php');
		$peerMarkingDataInfoAry = $w2_libW2->getStudentPeerMarkingTargetData($r_peerMarkingMarkerUserId, $r_writingId, $markedOnly=null, $r_studentId, $r_version);
		$recordId = $peerMarkingDataInfoAry[0]['RECORD_ID'];
		$objPeerMarkingData = new libW2PeerMarkingData($recordId);
		if(!$recordId){
			$objPeerMarkingData->setWritingId($r_writingId);
			$objPeerMarkingData->setMarkerUserId($r_peerMarkingMarkerUserId);
			$objPeerMarkingData->setTargetUserId($r_studentId);
		}
		$objPeerMarkingData->setComment($r_comment[0]);
		$objPeerMarkingData->setSticker($r_sticker);
		$objPeerMarkingData->setMark($r_mark);
		$objPeerMarkingData->setVersion($r_version);
		$objPeerMarkingData->setViewStatus($w2_cfg["DB_W2_PEER_MARKING_DATA_VIEW_STATUS"]["viewed"]);
		$objPeerMarkingData->setDateModified('now()');
		$objPeerMarkingData->setModifiedBy($w2_thisUserID);
		$objPeerMarkingData->save();
		break;	
}







echo $r_commentId."|=|".$w2_libW2->getDisplayMarkingLayerHTML($r_stepId,$w2_thisStudentID,$r_writingId,$r_curStepHandinCode,$r_version);
?>