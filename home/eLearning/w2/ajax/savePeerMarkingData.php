<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2PeerMarkingData.php');

$r_writingId = trim(stripslashes($_POST['r_writingId']));
$r_studentId = trim(stripslashes($_POST['r_studentId']));
$r_peerMarkingMarkerUserId = trim(stripslashes($_POST['r_peerMarkingMarkerUserId']));
$r_peerComment = trim(stripslashes($_POST['r_peerComment'][0]));
$r_sticker = $_POST['r_sticker'];
$r_version = $_POST['r_version']?$_POST['r_version']:1;
$r_mark = $_POST['r_mark'];

$peerMarkingDataInfoAry = $w2_libW2->getStudentPeerMarkingTargetData($r_peerMarkingMarkerUserId, $r_writingId, $markedOnly=null, $r_studentId);
$recordId = $peerMarkingDataInfoAry[0]['RECORD_ID'];

$objPeerMarkingData = new libW2PeerMarkingData($recordId);
if ($recordId > 0) {
	// do nth
}
else {
	$objPeerMarkingData->setWritingId($r_writingId);
	$objPeerMarkingData->setMarkerUserId($r_peerMarkingMarkerUserId);
	$objPeerMarkingData->setTargetUserId($r_studentId);
}
$objPeerMarkingData->setComment($r_peerComment);
$objPeerMarkingData->setSticker($r_sticker);
$objPeerMarkingData->setMark($r_mark);
$objPeerMarkingData->setViewStatus($w2_cfg["DB_W2_PEER_MARKING_DATA_VIEW_STATUS"]["viewed"]);
$objPeerMarkingData->setDateModified('now()');
$objPeerMarkingData->setModifiedBy($w2_thisUserID);

$success = $objPeerMarkingData->save();

$recordId = $objPeerMarkingData->getRecordId();
echo ($success && $recordId > 0)? $recordId : '0';
?>