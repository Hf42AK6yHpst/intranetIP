<?php

include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libStepStudentComment.php");
$r_contentCode = $_POST['r_contentCode'];
$r_writingId = $_POST['r_writingId'];
$r_stepId = $_POST['r_stepId'];
$r_commentId = $_POST['r_commentId'];
$r_filePath = trim(stripslashes($_POST['r_filePath']));
$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));
$step = !empty($r_stepCode)?str_replace('c','',$r_stepCode):trim(stripslashes($_POST['saveTab']));
$cid = $_POST['cid']; 
$h_button = '';
if($w2_thisUserType==USERTYPE_STAFF&&empty($r_writingId)&&empty($r_action)) $can_edit = true;
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	switch($r_contentCode){
		case 'chi':$stepVocab = $parseData['step4Data']['step4Vocab'];$saveStepContentTMPL='saveChiStepContent';break;
		case 'eng':$stepVocab = $parseData['step3Data']['step3Vocab'];$saveStepContentTMPL='saveStep3Content';break;
	}
}

$stepVocab = !empty($stepVocab)?intranet_undo_htmlspecialchars($stepVocab):'';
if($can_edit){
$content = '<div id="w2_moreVocab_table">';
$content .= $w2_libW2->displayMoreVocabTemplate($step,$cid,$stepVocab);
$content .= '</div>';
$content .= $linterface->Get_Form_Warning_Msg('r_Step'.$step.'Vocab_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_'.$step);

$content .= '<input type="hidden" name="type" id="type" value="vocab">';
$content .= '<input type="hidden" name="step" id="step" value="'.$step.'">';	
$content .= '<input type="hidden" name="mod" id="mod" value="">';	
$content .= '<input type="hidden" name="task" id="task" value="">';
$content .= '<input type="hidden" name="cid" id="cid" value="'.$cid.'">';

$h_button .= '<input type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" style="'.$submitBtnDisplayAttr.'" onclick="saveMoreVocab(\''.$Lang['W2']['returnMsgArr']['updateSuccess'].'\', \''.$Lang['W2']['returnMsgArr']['updateFailed'].'\');return false;" />';
}else{
	$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
	### Get Student Writing Info
	$writingStudentInfoAry = $w2_libW2->findStudentWriting($w2_thisStudentID, $r_contentCode, $r_writingId);
	$vocab = $writingStudentInfoAry[0]['OTHER_INFO'];
	$writingSumbitStatus = $writingStudentInfoAry[0]['HANDIN_SUBMIT_STATUS'];
	### Get Teacher Comment of this Step
	if ($r_commentId != '' && $r_commentId > 0) {
		// have comment => load and show the teacher comment link
		$objComment = new libStepStudentComment($r_commentId);
		$stepComment = nl2br($objComment->getContent());
		$stepCommentLastUpdate = $objComment->getDateModified();
		$stepCommentLastUpdateBy = $objComment->getModifiedBy();
		$lastModifiedDisplay = Get_Last_Modified_Remark($stepCommentLastUpdate, '', $stepCommentLastUpdateBy);
		
		$commentLinkStyle = ' style="display:block;" ';
	}
	else {
		// no comment => hide the teacher comment link
		$commentLinkStyle = ' style="display:none;" ';
	}
	if ($objWriting->isCompleted() || $w2_thisAction==$w2_cfg["actionArr"]["marking"] || $w2_thisAction==$w2_cfg["actionArr"]["peerMarking"]) {
		$textareaDisabled = 'disabled';
		$submitBtnDisplayAttr = 'display:none;';
	}else {
		$textareaDisabled = '';
		$submitBtnDisplayAttr = '';
	}
	$header = $r_headerHtml;
	$content = $stepVocab;
	$content .= '<br />';
    $content .= $Lang['W2']['vocabularyPool'].': <br />';
	$content .= '<textarea name="r_otherInfo[0]" rows="4" class="textbox" style="width:95%;" '.$textareaDisabled.'>'.$vocab.'</textarea>';
	$content .= '<br />';
	$content .= '<br />';
	$content .= '<a href="javascript:triggerCommentLayerInThickbox();" class="btn_comment_tea_simple" title="Show teacher\'s comment" '.$commentLinkStyle.'>'.$Lang['W2']['teachersComment'].'<span class="arrow">&nbsp;</span></a>';
	$content .= '<br style="clear:both;" />';
	$content .= '<div id="commentDivInThickbox" class="comment" style="display:none;">';
	$content .= $stepComment;
	$content .= '<br><br><span class="date">'.$lastModifiedDisplay.'</span>';
	$content .= '</div>';

$h_button .= '<input type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" style="'.$submitBtnDisplayAttr.'" onclick="saveOtherInfo(\''.$Lang['W2']['returnMsgArr']['vocabUpdateSuccess'].'\', \''.$Lang['W2']['returnMsgArr']['vocabUpdateFailed'].'\');" />';	
}
$h_button .= '<input type="button" class="formsubbutton" onmouseover="this.className=\'formsubbuttonon\'" onmouseout="this.className=\'formsubbutton\'" value="'.$Lang['Btn']['Close'].'" onclick="js_Hide_ThickBox();" />';

echo $w2_libW2->getThickboxHtml($r_headerHtml, $content, $h_button);
?>
<script>
<?if($can_edit){?>
function validVocab(){
	var editor = FCKeditorAPI.GetInstance('step<?=$step?>Vocab');
	var isValid = 1;
	var content = editor.GetHTML();
	if( content =='&nbsp;' || content =='<br />')
	{	
		$("#r_Step<?=$step?>Vocab_warningDiv").show();
		isValid = 0;
	}else{
		$("#thickboxForm input[id=step<?=$step?>Vocab]").val(content);
	}
	return isValid;
}
function saveMoreVocab(successMsg, failedMsg) {
	$("#r_Step<?=$step?>Vocab_warningDiv").hide();
	$("#thickboxForm input[id=mod]").val("ajax");
	$("#thickboxForm input[id=task]").val("<?=$saveStepContentTMPL?>");
	if(validVocab()){
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#thickboxForm").serialize(),
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
						  var returnMsg = (xml)? successMsg : failedMsg;
						  Get_Return_Message(returnMsg);
					  }
		});
	}
}
<?}?>
</script>