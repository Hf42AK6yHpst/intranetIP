<?
	//	update the student status 
	
	if($r_approvalStatus==$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]){
		//save step handin to hisory for redo writing
		$w2_libW2->createStepHandinHisoryRecord($r_stepId,$r_studentId,$r_curStepHandinCode);
		$w2_libW2->updateStudentWritingHandinStatus($r_writingId,$r_studentId,'');
	}
	$result = $w2_libW2->updateStepStudentApprovalStatus($r_stepId,$r_studentId,$r_approvalStatus);
	header("Content-Type:   text/xml");

$XML = generate_XML(
					array(
						array("result", ($result?1:0)),	
					)
				);
echo $XML;
?>