<?php
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");

if ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
	$writingObjSource = $w2_cfg["writingObjectSource"]["xml"];
}
else {
	$writingObjSource = $w2_cfg["writingObjectSource"]["db"];
}

$objWriting = libWritingFactory::createWriting($writingObjSource, $r_writingId, $r_contentCode, $r_schemeCode);
$stepHandinHistory = $w2_libW2->getStepHandinHisotryRecord($r_stepId,array($w2_thisStudentID),$r_version);

$answerMarked = $stepHandinHistory[0]['ANSWER_MARKED'];	
$answerOfPreviousVersion = $stepHandinHistory[0]['ANSWER'];	
$displayHandinCommentHTML = $w2_libW2->getDisplayMarkingLayerHTML($r_stepId,$w2_thisStudentID,$r_writingId,$r_curStepHandinCode,$r_version);

$templateID =  $objWriting->getTemplateID();
$clipArtID = $objWriting->getClipArtID();
$templateID = !empty($templateID)?$templateID:1;
$clipArtID = !empty($clipArtID)?$clipArtID:1;

$html = "";
$html .= '<div class="mark_comment">';
	$html .= '<div id="display_mark_comment" class="show_sticker">';	
    	$html .= $displayHandinCommentHTML;
    $html .= '</div>';
$html .= '</div>';   
$html .= '<div id="paper_template" class="template_'.str_pad($templateID,  2, "0", STR_PAD_LEFT).'">';                            
	$html .= '<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">';
    	$html .= '<div class="pt_top_element"></div>';
    $html .= '</div></div></div>';
    $html .= '<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">';
    	$html .= '<div class="paper_main_content">';
        	$html .= '<div class="essay">'.(($answerMarked != '') ? $answerMarked : $answerOfPreviousVersion).'</div>';
        	$html .= '<p class="spacer"></p>';
        $html .= '</div>';
   $html .= '</div></div></div>';
	$html .= '<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">';
    $html .= '</div></div></div>';
    $html .= '<div id="template_clipart"><span class="clipart_'.str_pad($clipArtID,  2, "0", STR_PAD_LEFT).'"></span></div>';
$html .= '</div>';	
echo $html;
?>