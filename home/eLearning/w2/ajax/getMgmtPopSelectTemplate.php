<?php
// using:
/*
 * 	Log
 * 	Date:	2015-02-10 [Cameron] use $Lang['W2']['template']['EssaySample'] and move the content to Lang file (w2_lang.{$Lang}.php)
 */
?>
<script type="text/javascript">
var templateID = $('#r_templateID').val();
var clipArtID = $('#r_clipArtID').val();

<!--

$(document).ready(function(){
	$('#TB_load').remove();
	// Get Current Setting Start
	change_paper('template_'+pad(templateID, 2),'theme_'+pad(templateID, 2),'paper_template','theme');
	change_paper('clipart_'+pad(clipArtID, 2),'clipart_'+pad(clipArtID, 2),'template_clipart','clipart')
	// Get Current Setting End
});

function saveTemplateSettings(){
	$('.paper_list').find('.selected').each(function(){
		var rs = $(this).attr('Id').split("_");
		templateID = rs[1];
	});
	$('.clipart_list').find('.selected').each(function(){
		var rs = $(this).attr('Id').split("_");
		clipArtID = rs[1];
	});
	$('#r_templateID').val(templateID);
	$('#r_clipArtID').val(clipArtID);
	
	var themeClass = 'theme_'+templateID;
	$('#template_using').removeClass();
	$('#template_using').addClass(themeClass);
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function change_paper(a,b,c,d) {
	var newclass = a;
    var paper_class = document.getElementById(c);
	paper_class.className = newclass;
	if(d=="clipart"){n=35} else { n=19}
	 for (i=1; i<n; i++)
	 { 
	  if (i < 10) {
	  	i = "0" + i;
	  }
	   var targetbtn = document.getElementById(d + "_" + i);
	   targetbtn.className = "";	   
	 } 
	var targetbtn = document.getElementById(b);
	targetbtn.className = "selected";
}

-->

</script>
<div class="content_pop_board_190 content_pop_template_setting">
  <h1 class="set_template"><?php echo $Lang['W2']['paperTemplate']?></h1>
  	<div class="template_board">
  		<div class="preview_template">
  		 <!--- template paper start----->
                            	 <div id="paper_template" class="template_01">                                
                                 	<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">
                                    	<div class="pt_top_element"></div>
                                    </div></div></div>
                                 	<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">
                                    	<div class="paper_main_content">
                                        <!---------------->
                                       
                                <!----------------------------- essay start ----------------------------------------->
<?=$Lang['W2']['template']['EssaySample']?>
                                 <!----------------------------- essay end ----------------------------------------->
                            	
                                        
                                        
                                        <!----------------->
                                        <p class="spacer"></p>
                                        </div>
                                    </div></div></div>

                                   	<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">
                                    	
                                    </div></div></div>
                                    <!--------clipart---------->
                                    <div id="template_clipart" class="clipart_01"><span></span></div>
                                </div>	
                                <!--- template paper end----->
  		</div>
   	  <div class="control_template">
   
      <div class="control_board">
		<div class="subtitle"><span><?=$Lang['W2']['template']['Wallpaper'] ?></span></div>
     
            		
    <ul class="paper_list">
            	<li id="theme_01" class="selected">
               	  <a href="#" class="template_thumb" title="" onclick="change_paper('template_01','theme_01','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Paper']?></em>                </li>
  				<li id="theme_02">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_02','theme_02','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Email']?></em>                </li>
                <li id="theme_03">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_03','theme_03','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Panda']?></em>                </li>
                 <li id="theme_04">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_04','theme_04','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Squirrel']?></em>                </li>
                <li id="theme_05">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_05','theme_05','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Sketchbook']?></em>                </li>
                <li id="theme_06">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_06','theme_06','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Robot']?></em>                </li>
                <li id="theme_07">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_07','theme_07','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Film']?></em>                </li>
                 <li id="theme_08">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_08','theme_08','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Leaflet']?></em>                </li>
                 <li id="theme_09">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_09','theme_09','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Postbox']?></em>                </li>
                 <li id="theme_10">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_10','theme_10','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['News']?></em>                </li>
                  <li id="theme_11">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_11','theme_11','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Letter']?></em>                </li>
                   <li id="theme_12">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_12','theme_12','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Friends']?></em>                </li>
                  <li id="theme_13">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_13','theme_13','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Cooking']?></em>                </li>
                  <li id="theme_14">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_14','theme_14','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['ChineseWriting']?></em>                </li>
                    <li id="theme_15">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_15','theme_15','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['ChineseTravel']?></em>                </li>
                     <li id="theme_16">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_16','theme_16','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Travel']?></em>                </li>
                     <li id="theme_17">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_17','theme_17','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['City']?></em>                </li>
                     <li id="theme_18">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('template_18','theme_18','paper_template','theme')"><span></span></a>
                    <em><?=$Lang['W2']['template']['Coast']?></em>                </li>
                   
               <p class="spacer"></p>
                <div class="setting_board_arrow"></div> 	
        </ul>  
       
      </div> 
      <div class="control_board">
      <div class="subtitle"><span><?=$Lang['W2']['template']['Clipart']?></span></div>
       
         
            		
    <ul class="clipart_list">
            	<li id="clipart_01" class="selected">
               	  <a href="#" class="template_thumb" title="" onclick="change_paper('clipart_01','clipart_01','template_clipart','clipart')"><span></span></a>
                   
                </li>
                <li  id="clipart_02">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_02','clipart_02','template_clipart','clipart')"><span></span></a>
                   
                </li>
                <li id="clipart_03">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_03','clipart_03','template_clipart','clipart')"><span></span></a>
                   
                </li>
                 <li  id="clipart_04">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_04','clipart_04','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li  id="clipart_05">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_05','clipart_05','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_06">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_06','clipart_06','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_07">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_07','clipart_07','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li  id="clipart_08">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_08','clipart_08','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_09">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_09','clipart_09','template_clipart','clipart')"><span></span></a>
                    
                </li>
				 <li id="clipart_10">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_10','clipart_10','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_11">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_11','clipart_11','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_12">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_12','clipart_12','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_13">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_13','clipart_13','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_14">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_14','clipart_14','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_15">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_15','clipart_15','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_16">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_16','clipart_16','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_17">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_17','clipart_17','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_18">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_18','clipart_18','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_19">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_19','clipart_19','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_20">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_20','clipart_20','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_21">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_21','clipart_21','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_22">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_22','clipart_22','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_23">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_23','clipart_23','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_24">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_24','clipart_24','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_25">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_25','clipart_25','template_clipart','clipart')"><span></span></a>
                    
                </li>
                <li id="clipart_26">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_26','clipart_26','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_27">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_27','clipart_27','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_28">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_28','clipart_28','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_29">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_29','clipart_29','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_30">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_30','clipart_30','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_31">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_31','clipart_31','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_32">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_32','clipart_32','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_33">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_33','clipart_33','template_clipart','clipart')"><span></span></a>
                    
                </li>
                 <li id="clipart_34">
                	<a href="#" class="template_thumb" title="" onclick="change_paper('clipart_34','clipart_34','template_clipart','clipart')"><span></span></a>
                    
                </li>
               <p class="spacer"></p>
                <div class="setting_board_arrow"></div>
        </ul>  
      </div>
    
   	  </div>
  	
    </div>
	<div class="edit_bottom" id="edit_content_pop">
		<input name="submit" type="button" class="formbutton" id="thickboxSubmit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?php echo $Lang['Btn']['Save']?>" onclick="saveTemplateSettings();js_Hide_ThickBox();"/>
		<input name="cancel" type="button" class="formsubbutton" id="thickboxCancel" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="<?php echo $Lang['Btn']['Cancel']?>" onclick="js_Hide_ThickBox();" />
	</div>
	<input name="paper" type="hidden" value="<?php echo $_GET['paper'] ?>"/>
	<input name="clipArt" type="hidden" value="<?php echo $_GET['clipArt'] ?>"/>
	 
</div>
