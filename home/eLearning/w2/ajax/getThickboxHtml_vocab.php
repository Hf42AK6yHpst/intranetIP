<?php


include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libStepStudentComment.php");

### Get Form Post Variables
$r_contentCode = $_POST['r_contentCode'];
$r_writingId = $_POST['r_writingId'];
$r_stepId = $_POST['r_stepId'];
$r_commentId = $_POST['r_commentId'];

$r_filePath = trim(stripslashes($_POST['r_filePath']));
$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));


$h_content = '';
ob_start();
include_once($intranet_root.$w2_cfg['modulePath'].'/content'.$r_filePath);
$h_content .= ob_get_contents();
ob_end_clean();


//$objWriting = new libWriting($r_writingId);
$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);


### Get Student Writing Info
$writingStudentInfoAry = $w2_libW2->findStudentWriting($w2_thisStudentID, $r_contentCode, $r_writingId);
$vocab = $writingStudentInfoAry[0]['OTHER_INFO'];
$writingSumbitStatus = $writingStudentInfoAry[0]['HANDIN_SUBMIT_STATUS'];


### Get Teacher Comment of this Step
if ($r_commentId != '' && $r_commentId > 0) {
	// have comment => load and show the teacher comment link
	$objComment = new libStepStudentComment($r_commentId);
	$stepComment = nl2br($objComment->getContent());
	$stepCommentLastUpdate = $objComment->getDateModified();
	$stepCommentLastUpdateBy = $objComment->getModifiedBy();
	$lastModifiedDisplay = Get_Last_Modified_Remark($stepCommentLastUpdate, '', $stepCommentLastUpdateBy);
	
	$commentLinkStyle = ' style="display:block;" ';
}
else {
	// no comment => hide the teacher comment link
	$commentLinkStyle = ' style="display:none;" ';
}



//if ($writingSumbitStatus == $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"]) {
if ($objWriting->isCompleted() || $w2_thisAction==$w2_cfg["actionArr"]["marking"]) {
	$textareaDisabled = 'disabled';
	$submitBtnDisplayAttr = 'display:none;';
}
else {
	$textareaDisabled = '';
	$submitBtnDisplayAttr = '';
}


/*
$header = <<<html
<h1 class="vocab">More Vocabulary</h1>
html;
*/

// For New Layout
//$header = '<h1 class="ref">'.strtoupper(strip_tags($r_headerHtml)).'</h1>';
$header = $r_headerHtml;
$content = $h_content;

/*
$content = <<<html
html;
*/

$content .= <<<html
      <br />
      {$Lang['W2']['vocabularyPool']}: <br />
<textarea name="r_otherInfo[0]" rows="4" class="textbox" style="width:95%;" {$textareaDisabled}>{$vocab}</textarea>
<br />
<br />
<a href="javascript:triggerCommentLayerInThickbox();" class="btn_comment_tea_simple" title="Show teacher's comment" {$commentLinkStyle}>{$Lang['W2']['teachersComment']}<span class="arrow">&nbsp;</span></a>
<br style="clear:both;" />
<div id="commentDivInThickbox" class="comment" style="display:none;">
	{$stepComment}
	<br><br><span class="date">{$lastModifiedDisplay}</span>
</div>
html;


$button = <<<html
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="{$Lang['Btn']['Save']}" style="{$submitBtnDisplayAttr}" onclick="saveOtherInfo('{$Lang['W2']['returnMsgArr']['vocabUpdateSuccess']}', '{$Lang['W2']['returnMsgArr']['vocabUpdateFailed']}');" />
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="{$Lang['Btn']['Close']}" onclick="js_Hide_ThickBox();" />
html;


echo $w2_libW2->getThickboxHtml($header, $content, $button);
?>