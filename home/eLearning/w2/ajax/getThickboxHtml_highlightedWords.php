<?php

include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libStepStudentComment.php");
$r_contentCode = $_POST['r_contentCode'];
$r_writingId = $_POST['r_writingId'];
$r_stepId = $_POST['r_stepId'];
$r_commentId = $_POST['r_commentId'];
$r_filePath = trim(stripslashes($_POST['r_filePath']));
$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));
$cid = $_POST['cid']; 
$h_button = '';
if($w2_thisUserType==USERTYPE_STAFF&&empty($r_writingId)&&empty($r_action)) $can_edit = true;
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	$stepData = $parseData['step4Data'];
}
$content = '<div id="w2_highlightWords_table">';
if($can_edit){
$content .= '<strong>'.$Lang['W2']['vocabularies'].'</strong><br/>';
$content .= $w2_libW2->displayHighlightWordsTemplate($stepData['HightlightWordsAry']['vocabularies'],'vocab',$can_edit);
$content .= '<br/>';
$content .= '<strong>'.$Lang['W2']['usefulExpressions'].'</strong><br/>';
$content .= $w2_libW2->displayHighlightWordsTemplate($stepData['HightlightWordsAry']['expressions'],'expression',$can_edit);
$content .= '<input type="hidden" name="mod" id="mod" value="">';	
$content .= '<input type="hidden" name="task" id="task" value="">';
$content .= '<input type="hidden" name="cid" id="cid" value="'.$cid.'">';

$h_button .= '<input type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" style="'.$submitBtnDisplayAttr.'" onclick="saveHighlightWords(\''.$Lang['W2']['returnMsgArr']['updateSuccess'].'\', \''.$Lang['W2']['returnMsgArr']['updateFailed'].'\');return false;" />';
}else{
$content .= '<table class="grp_highlight">';
	$content .= '<tr class="vocab_group">';
		$content .= '<td><strong>'.$Lang['W2']['wordsPhrases'].'</strong></td>';
		$content .= '<td>&nbsp;</td>';
		$content .= '<td><strong>'.$Lang['W2']['meaningUsage'].'</strong></td>';
	$content .= '</tr>';
	foreach((array)$stepData['HightlightWordsAry']['vocabularies'] as $_itemAry){
		$content .= '<tr class="vocab_group">';
	        $content .= '<td class="title">'.stripslashes($_itemAry['word']).'</td>';
	        $content .= '<td>:</td>';
	        $content .= '<td>'.stripslashes($_itemAry['meaning']).'</td>';
        $content .= '</tr>';
	}
	foreach((array)$stepData['HightlightWordsAry']['expressions'] as $_itemAry){
		$content .= '<tr class="vocab_group">';
	        $content .= '<td class="title">'.stripslashes($_itemAry['word']).'</td>';
	        $content .= '<td>:</td>';
	        $content .= '<td>'.stripslashes($_itemAry['meaning']).'</td>';
        $content .= '</tr>';
	}
$content .= '</table>';		
}
$content .= '</div>';
$h_button .= '<input type="button" class="formsubbutton" onmouseover="this.className=\'formsubbuttonon\'" onmouseout="this.className=\'formsubbutton\'" value="'.$Lang['Btn']['Close'].'" onclick="js_Hide_ThickBox();" />';

echo $w2_libW2->getThickboxHtml($r_headerHtml, $content, $h_button);
?>
<script>
<?if($can_edit){?>
function checkValid(){
	isValid = 1;
	$('.highlightword_vocab').each(function(){
		if(isValid&&$(this).val()==''){
			$("#r_vocabWord_warningDiv").show();
			$(this).focus();
			isValid = 0;
		}
	});	
	$('.highlightword_expression').each(function(){
		if(isValid&&$(this).val()==''){
			$("#r_expressionWord_warningDiv").show();
			$(this).focus();
			isValid = 0;
		}
	});		
	return isValid;
}
function saveHighlightWords(successMsg, failedMsg) {
	$(".warningDiv_4").hide();
	$("#thickboxForm input[id=mod]").val("ajax");
	$("#thickboxForm input[id=task]").val("saveHighlightWords");
	if(checkValid()){
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#thickboxForm").serialize(),
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
						  var returnMsg = (xml)? successMsg : failedMsg;
						  Get_Return_Message(returnMsg);
					  }
		});
	}
}
<?}?>
</script>