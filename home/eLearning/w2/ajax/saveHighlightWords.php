<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
#init
$cid = trim($_POST['cid']);
$parseData = $engContentParser->parseEngContent($cid);
$step4Data = $parseData['step4Data'];
$step4Data['HightlightWordsAry'] = array();
for($i=0;$i<$w2_highlightword_vocab_counter;$i++){
	$_word = isset($_POST['vocab_word_'.$i])?addslashes(intranet_htmlspecialchars(trim($_POST['vocab_word_'.$i]))):'';
	$_meaning = isset($_POST['vocab_meaning_'.$i])?addslashes(intranet_htmlspecialchars(trim($_POST['vocab_meaning_'.$i]))):'';
	if(!empty($_word)&&!empty($_meaning)){
		$step4Data['HightlightWordsAry']['vocabularies']['item'.$i] = array('word'=>$_word,'meaning'=>$_meaning);
	}
}
for($i=0;$i<$w2_highlightword_expression_counter;$i++){
	$_word = isset($_POST['expression_word_'.$i])?addslashes(intranet_htmlspecialchars(trim($_POST['expression_word_'.$i]))):'';
	$_meaning = isset($_POST['expression_meaning_'.$i])?addslashes(intranet_htmlspecialchars(trim($_POST['expression_meaning_'.$i]))):'';	
	if(!empty($_word)&&!empty($_meaning)){
		$step4Data['HightlightWordsAry']['expressions']['item'.$i] = array('word'=>$_word,'meaning'=>$_meaning);
	}
}

$buildData = $objEngContentBuilder->buildSciStep4($step4Data,true);
$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content'].", dateModified = NOW() WHERE cid='".$cid."'";

$result = $objDB->db_db_query($sql);


echo $result;

?>

