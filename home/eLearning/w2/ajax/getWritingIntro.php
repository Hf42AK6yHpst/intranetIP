<?php
// using:
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");

$r_writingId = $_POST['r_writingId'];

//$objWriting = new libWriting($r_writingId);
$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
$intro = $objWriting->getIntroduction();

echo ($intro=='')? $Lang['General']['EmptySymbol'] : $intro;
?>