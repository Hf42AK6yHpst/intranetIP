<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefCategoryFactory.php');

$r_infoboxCode = $_POST['r_infoboxCode'];
$r_writingStudentId = $_POST['r_writingStudentId'];
$r_refCategoryId = $_POST['r_refCategoryId'];
$r_refCategoryTitle = trim(stripslashes($_POST['r_refCategoryTitle']));
$r_refCategoryCssSet = $_POST['r_refCategoryCssSet'];
$r_cid = $_POST['cid'];

$objRefCategoryContentInput = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["contentInput"], $r_refCategoryId);
$objRefCategoryContentInput->setInfoboxCode($r_infoboxCode);
$objRefCategoryContentInput->setContentId($r_cid);
$objRefCategoryContentInput->setTitle($r_refCategoryTitle);
$objRefCategoryContentInput->setCssSet($r_refCategoryCssSet);
$objRefCategoryContentInput->setModifiedBy($_SESSION['UserID']);
if ($r_refCategoryId == '') {
	$objRefCategoryContentInput->setInputBy($_SESSION['UserID']);
}
$refCategoryId = $objRefCategoryContentInput->save();

echo ($refCategoryId > 0)? $refCategoryId : '0';
?>