<?php
include_once($PATH_WRT_ROOT."includes/w2/libWritingStudent.php");

### Get Post Variable
$r_writingStudentId = $_POST['r_writingStudentId'];
$r_otherInfo[0] = stripslashes(trim($_POST['r_otherInfo'][0]));

### Save Other Info
$objWritingStudent = new libWritingStudent($r_writingStudentId);
$objWritingStudent->setOtherInfo($r_otherInfo[0]);
$success = $objWritingStudent->save();

echo ($success)? '1' : '0';
?>