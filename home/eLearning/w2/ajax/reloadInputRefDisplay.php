<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2RefManager.php");

$r_infoboxCode = $_POST['r_infoboxCode'];
$r_cid = $_POST['cid'];

### Generate the display
$objW2RefManager = new libW2RefManager();
$objW2RefManager->setIsEditable(true);
$objW2RefManager->setInfoboxCode($r_infoboxCode);
$objW2RefManager->setDisplayMode($w2_cfg["refDisplayMode"]["input"]);
echo $objW2RefManager->display();
?>