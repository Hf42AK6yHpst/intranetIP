<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$r_filePath = trim(stripslashes($_POST['r_filePath']));
$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));
$step = !empty($r_stepCode)?str_replace('c','',$r_stepCode):trim(stripslashes($_POST['saveTab']));
$cid = $_POST['cid']; 
$h_button = '';
if($w2_thisUserType==USERTYPE_STAFF&&empty($r_writingId)&&empty($r_action)) $can_edit = true;
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	$referenceAry = $parseData['step2Data']['step2ReferenceAry'];
}
$content = '<div id="w2_step2Reference_table" style="height:100%">';
$content .= $w2_libW2->getStep2ReferenceHTML($cid,$referenceAry,$can_edit);
$content .= '</div>';

if($can_edit){
$content .= '<input type="hidden" name="step" id="step" value="'.$step.'">';	
$content .= '<input type="hidden" name="mod" id="mod" value="">';	
$content .= '<input type="hidden" name="task" id="task" value="">';
$content .= '<input type="hidden" name="cid" id="cid" value="'.$cid.'">';
$content .= '<input type="hidden" name="r_contentCode" id="r_contentCode" value="'.$r_contentCode.'">';

$h_button .= <<<html
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="{$Lang['Btn']['Save']}" style="{$submitBtnDisplayAttr}" onclick="uploadStep2ReferenceFile();return false;" />
html;
}
$h_button .= <<<html
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="{$Lang['Btn']['Close']}" onclick="js_Hide_ThickBox();" />
<iframe id="uploadFileIFrame" name="uploadFileIFrame" style="width:100%;height:300px;display:none;"></iframe>
html;

echo $w2_libW2->getThickboxHtml($r_headerHtml, $content, $h_button);


?>
<script>
function validStep2Reference(){
	var isValid = 1,path;
	var type = $("#thickboxForm input[name=type]:checked").val();
	var arr = [ "jpg", "jpeg", "gif", "bmp", "png" ];
	$(".warningDiv_2").hide();
	if($("#thickboxForm input[id=topic]").val()==''){	
		$("#r_ReferenceTopic_warningDiv").show();
		isValid = 0;
	}
	if($("#thickboxForm input[id=type]").val()=='')
	{
		$("#r_ReferenceType_warningDiv").show();				
		isValid = 0;
	}	
	if(type=='normal'){
		$(".normalImage").each(function(){
			path = $(this).val().toLowerCase();
			var ext = path.substr( (path.lastIndexOf('.') +1) );
			if(path.length>0 && $.inArray(ext,arr)==-1){
	     		$("#r_ReferenceNormalImage1_warningDiv").show();				
				isValid = 0;	
			}
		});		
	}else if(type=="compare"){
		$(".compare1Image").each(function(){
			path = $(this).val().toLowerCase();
			var ext = path.substr( (path.lastIndexOf('.') +1) );
			
			if(path.length>0 && $.inArray(ext,arr)==-1){
	     		$("#r_ReferenceCompareImage1_warningDiv").show();				
				isValid = 0;	
			}
		});	
		$(".compare2Image").each(function(){
			path = $(this).val().toLowerCase();
			var ext = path.substr( (path.lastIndexOf('.') +1) );
			
			if(path.length>0 && $.inArray(ext,arr)==-1){
	     		$("#r_ReferenceCompareImage2_warningDiv").show();				
				isValid = 0;	
			}
		});					
	}
	return isValid;
}

function uploadStep2ReferenceFile(){
	if(validStep2Reference()){
		$("#thickboxForm input[id=mod]").val("common");
		$("#thickboxForm input[id=task]").val("uploadStep2ReferenceFile");
		$('#uploadFileIFrame').attr('src', "/home/eLearning/w2/index.php");
		$('#thickboxForm').attr('method', 'post').attr('target', 'uploadFileIFrame').submit();
	}
}
</script>