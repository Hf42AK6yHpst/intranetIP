<?
//modify : fai
	include_once($PATH_WRT_ROOT.'includes/w2/libStepStudentComment.php');
	
	$r_saveFromEclass = $_POST['r_saveFromEclass'];
	$r_editorSource = $_POST['r_editorSource'];
	$r_includeAnsCodeText = trim(stripslashes($_POST['r_includeAnsCodeText']));
	$includeAnsCodeArr = explode($w2_cfg['delimiter1'], $r_includeAnsCodeText);
	$r_commentId = $_POST['r_commentId'];

	$objDb = new libdb();
	$successAry = array();

	//SOME STEP MAY NOT HAVE QUESTION THAT NEED TO ANSWER
	if(count($r_question) > 0){
		foreach ($r_question as $_ansType => $_ansAssoArr){
			foreach ($_ansAssoArr as $__stepCode => $__ans){
				// Check if save this answer
				if ($r_includeAnsCodeText == '') {
					// no text => save all answer
				}
				else {
					// save specific answer
					if (in_array($__stepCode, (array)$includeAnsCodeArr)) {
						// save answer
					}
					else {
						// skip answer
						continue;
					}
				}
				
				switch ($_ansType) {
					case 'checkbox':
						$__ans = implode($w2_cfg['delimiter1'], $__ans);
						break;
					case 'editor':
						if ($r_editorSource == $w2_cfg["editorSource"]["ip"]) {
							$__ans = $w2_libW2->getEditorSaveContent($__ans);
						}
						break;
					case 'select':
					case 'radio':
					case 'text':
					case 'textarea':
					case 'infoboxStatus':
					default:
						break;
				}
				$__ans = $objDb->Get_Safe_Sql_Query(trim(stripslashes($__ans)));
				if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
					$__ansSQL = 'ANSWER_MARKED=values(ANSWER)';
				}else{
					$__ansSQL = 'ANSWER=values(ANSWER)';
				}
				
				$sql = 'insert into 
						W2_STEP_HANDIN 
						(`STEP_ID`,`USER_ID`,`STEP_HANDIN_CODE`,`ANSWER`,`DATE_INPUT`,`DATE_MODIFIED`) values
						('.$r_stepId.','.$w2_thisStudentID.',\''.$__stepCode.'\',\''.$__ans.'\',now(),now()) 
						ON DUPLICATE KEY UPDATE 
						'.$__ansSQL.',DATE_MODIFIED=now()';
			
				$successAry['saveAns'][$__stepCode] = $objDb->db_db_query($sql);
				
				
				
//error_log($sql."    <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
			}
		}
	}	

	if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
		// do not update student handin status for teacher marking
	}
	else {

		//$saveToDBStatus default is submitted
		$saveToDBStatus = ($r_submitStatus == '') ? $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]: $r_submitStatus;
		//clear redo when submit
		if($saveToDBStatus == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"] && $r_action==$w2_cfg["actionArr"]["redo"]){
			$sql = 'update W2_STEP_STUDENT set APPROVAL_STATUS = 0 where STEP_ID = '.$r_stepId.' and USER_ID = '.$w2_thisStudentID.' AND APPROVAL_STATUS = '.$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"];
			$objDb->db_db_query($sql);
		}
		$sql = 'insert into 
				'.$intranet_db.'.W2_STEP_STUDENT (`STEP_ID`,`USER_ID`,`STEP_STATUS`,`DATE_INPUT`,`DATE_MODIFIED`) 
					values
				('.$r_stepId.','.$w2_thisStudentID.','.$saveToDBStatus.',now(),now())
				ON DUPLICATE KEY UPDATE
				DATE_MODIFIED = now(),STEP_STATUS = values(STEP_STATUS)
		  	   ';
//		error_log($sql." ".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
		$successAry['updateStepStudentStatus'] = $objDb->db_db_query($sql);
		
		
		### Update Comment Status to "studentHasRead"
		if ($r_commentId != '') {
			$objComment = new libStepStudentComment($r_commentId);
			$objComment->setCommentStatus($w2_cfg["DB_W2_STEP_STUDENT_COMMENT"]["studentHasRead"]);
			$successAry['updateCommentStatus'] = $objComment->save();
		}
	}
	$success = (in_array(false, (array)$successAry))? false : true;

	$returnMsg = '';
	if ($w2_thisAction==$w2_cfg["actionArr"]["marking"] && $r_saveFromEclass) {
		$returnMsg .= '<script language="javascript">'."\n";
			if ($success) {
				$returnMsg .= 'window.top.saveMarkReturnMsg(\'1\');';
			}
			else {
				$returnMsg .= 'window.top.saveMarkReturnMsg(\'0\');';
			}			
		$returnMsg .= '</script>'."\n";
	}
	else {
		$returnMsg .= ($success)? '1' : '0';
	}
	
	echo $returnMsg;
?>