<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$r_filePath = trim(stripslashes($_POST['r_filePath']));
$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));
$cid = $_POST['cid']; 
$h_button = '';
$tarPath = '/file/w2/reference/cid_'.$cid;
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	$step2SourceAry = $parseData['step2Data']['step2SourceAry'];
	$sourceCnt = count($step2SourceAry);
	for($i=0;$i<$sourceCnt;$i++){
		$_sourceAry = $step2SourceAry['sourceItem'.$i];
		$content .= getStep2SourceBox($_sourceAry,$i);
	}
	
	
}

function getStep2SourceBox($sourceAry,$i){
	global $tarPath,$Lang;
   	$image = $tarPath.'/'.$sourceAry['image'];
	$html = '';
	$html .= '<div class="infobox">';
		$html .= '<a class="top" title="'.$Lang['W2']['viewSource'].'" href="javascript:void(0);" onclick="triggerInfoBoxContent(this);">'.$Lang['W2']['source'].' '.($i+1).'<span class="arrow">&nbsp;</span></a>';
		$html .= '<div class="infobox_content w2_infoxboxContentDiv" style="display:none;">';
 			$html .= '<img src="'.$image.'" width="95%">';
			$html .= '<span class="ques">'.$Lang['W2']['checkPoint'].'</span>';
			$html .= '<div class="ques_content">';
				$html .= '<span class="type">'.$Lang['W2']['shortQuestions'].'</span>';
				$html .= '<table width="100%">';
					$html .= '<tbody>';
					$_qNo = 1;
					foreach((array)$sourceAry["questionAry"] as $_questionAry){
						$html .= '<tr>';
							$html .= '<td>'.$_qNo.'.	'.$_questionAry['question'].'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>';
								$html .= '<span class="modelans w2_modelAnsSpan">'.$_questionAry['answer'].'</span>';
							$html .= '</td>';
						$html .= '</tr>';
						$_qNo++;
					}
					$html .= '</tbody>';
				$html .= '</table>';
			$html .= '</div>';
		$html .= '</div>';
	$html .= '</div>';
	return $html;
}
$h_button .= <<<html
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="{$Lang['Btn']['Close']}" onclick="js_Hide_ThickBox();" />
html;



echo $w2_libW2->getThickboxHtml($r_headerHtml, $content, $h_button);
?>
