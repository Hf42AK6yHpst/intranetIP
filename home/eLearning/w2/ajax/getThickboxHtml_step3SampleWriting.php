<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$r_filePath = trim(stripslashes($_POST['r_filePath']));
$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));
$step = !empty($r_stepCode)?str_replace('c','',$r_stepCode):trim(stripslashes($_POST['saveTab']));
$cid = $_POST['cid']; 
$h_button = '';
if($w2_thisUserType==USERTYPE_STAFF&&empty($r_writingId)&&empty($r_action)) $can_edit = true;
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	$step3Data = $parseData['step3Data'];
}
$content = '<div id="w2_step3SampleWriting_table" style="height:100%">';
$content .= $w2_libW2->getStep3ThickboxSampleWritngHTML($step3Data,$can_edit);
$content .= '</div>';

if($can_edit){
$content .= '<input type="hidden" name="step" id="step" value="'.$step.'">';	
$content .= '<input type="hidden" name="mod" id="mod" value="">';	
$content .= '<input type="hidden" name="task" id="task" value="">';
$content .= '<input type="hidden" name="cid" id="cid" value="'.$cid.'">';
$content .= '<input type="hidden" name="r_contentCode" id="r_contentCode" value="'.$r_contentCode.'">';

$h_button .= <<<html
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="{$Lang['Btn']['Save']}" style="{$submitBtnDisplayAttr}" onclick="saveStep3SamplingWriting('{$Lang['W2']['returnMsgArr']['updateSuccess']}', '{$Lang['W2']['returnMsgArr']['updateFailed']}');return false;" />
html;
}
$h_button .= <<<html
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="{$Lang['Btn']['Close']}" onclick="js_Hide_ThickBox();" />
<iframe id="uploadFileIFrame" name="uploadFileIFrame" style="width:100%;height:300px;display:none;"></iframe>
html;

echo $w2_libW2->getThickboxHtml($r_headerHtml, $content, $h_button);


?>
<script>
$(document).ready(function(){
	<?if($can_edit):?>
	$("span#remark_highlight_icon_big a").click(function(){
		var order = $(this).attr('id').split('_')[1];
   		var html = '<input type="text" name="remarkTitle_'+order+'" id="remarkTitle_'+order+'" />';
   		html += '<textarea name="remarkContent_'+order+'" id="remarkContent_'+order+'"></textarea>';
   		$(this).closest('span').html(html);
   		return false;
	});
	<?else:?>
$('span#remark_highlight_icon_big').click(function(){
		var hideHighlight = false;
		if($(this).hasClass('current')){
			hideHighlight = true;
			$(this).removeClass('current');
		}
		var className = $(this).attr('class').replace('remark_highlight_icon_big_','');
		var updateClassName = '';
		switch(className){
		<?foreach($w2_cfg["SampleWriting"]["HighlightMapping"] as $_key => $_value):?>
			case '<?=$_key?>':updateClassName='highlight_<?=$_value?>';break;
		<?endforeach;?>
		}
		if(hideHighlight){
			$('.'+updateClassName).removeClass(updateClassName).addClass(updateClassName+'_off');
			$(this).find('u').hide();		
		}else{
			showEssayHighlight(updateClassName);
			$(this).addClass('current');
			$(this).find('u').show();
		}
		
	});
	hideEssayHighlight();	
	<?endif;?>
});
<?if($can_edit):?>
function validWriting(){
	var editor = FCKeditorAPI.GetInstance('step3Writing');
	var isValid = 1;
	var content = editor.GetHTML();
	if( content =='&nbsp;' || content =='<br />')
	{	
		$("#r_Step3Writing_warningDiv").show();
		isValid = 0;
	}else{
		$("#thickboxForm input[id=step3Writing]").val(content);
	}
	return isValid;
}
function saveStep3SamplingWriting(successMsg, failedMsg) {
	$("#thickboxForm input[id=mod]").val("ajax");
	$("#thickboxForm input[id=task]").val("saveChiStepContent");
	
	if(validWriting()){
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#thickboxForm").serialize(),
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
						  var returnMsg = (xml)? successMsg : failedMsg;
						  Get_Return_Message(returnMsg);
					  }
		});
	}
	
}
<?else:?>
function hideEssayHighlight(){
	$('span#remark_highlight_icon_big').find('u').hide();
	$('span#remark_highlight_icon_big').removeClass('current');
	$('.highlight_red').removeClass('highlight_red').addClass('highlight_red_off');
	$('.highlight_orange').removeClass('highlight_orange').addClass('highlight_orange_off');
	$('.highlight_yellow').removeClass('highlight_yellow').addClass('highlight_yellow_off');
	$('.highlight_lightgreen').removeClass('highlight_lightgreen').addClass('highlight_lightgreen_off');
	$('.highlight_green').removeClass('highlight_green').addClass('highlight_green_off');
	$('.highlight_lightblue').removeClass('highlight_lightblue').addClass('highlight_lightblue_off');
	$('.highlight_blue').removeClass('highlight_blue').addClass('highlight_blue_off');
	$('.highlight_darkblue').removeClass('highlight_darkblue').addClass('highlight_darkblue_off');
	$('.highlight_purple').removeClass('highlight_purple').addClass('highlight_purple_off');
	$('.highlight_pink').removeClass('highlight_pink').addClass('highlight_pink_off');
}
function showEssayHighlight(c){
	hideEssayHighlight();
	$('.'+c+'_off').removeClass(c+'_off').addClass(c);
}
<?endif;?>
</script>
