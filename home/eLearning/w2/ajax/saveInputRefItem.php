<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefItemFactory.php');

$r_infoboxCode = $_POST['r_infoboxCode'];
$r_cid = $_POST['cid'];
$r_refCategoryCode = $_POST['r_refCategoryCode'];
$r_refItemId = $_POST['r_refItemId'];
$r_refItemTitle = trim(stripslashes($_POST['r_refItemTitle']));


$objRefCategoryContentInput = libW2RefItemFactory::createObject($w2_cfg["refItemObjectSource"]["contentInput"], $r_refItemId);
$objRefCategoryContentInput->setInfoboxCode($r_infoboxCode);
$objRefCategoryContentInput->setContentId($r_cid);
$objRefCategoryContentInput->setRefCategoryCode($r_refCategoryCode);
$objRefCategoryContentInput->setTitle($r_refItemTitle);
$objRefCategoryContentInput->setModifiedBy($_SESSION['UserID']);
if ($r_refItemId == '') {
	$objRefCategoryContentInput->setInputBy($_SESSION['UserID']);
}
$refItemId = $objRefCategoryContentInput->save();

echo ($refItemId > 0)? $refItemId : '0';
?>