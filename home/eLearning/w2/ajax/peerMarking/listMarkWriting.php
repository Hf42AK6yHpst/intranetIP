<?
// using: 

$r_contentCode = $_GET['r_contentCode'];

$objDb = new libdb();


### Legend display
//$h_legend = $w2_libW2->htmlLegendInfo(array($w2_cfg["legendArr"]["incomplete"], $w2_cfg["legendArr"]["completed"], $w2_cfg["legendArr"]["studentWaitingTeacherComment"], $w2_cfg["legendArr"]["studentNeedCheckComment"]));

### New icon
$newIcon = $w2_libW2->getIconNew();


### Get the Student Writing Info of this Content
$studentWritingInfoArr = $w2_libW2->findStudentWritingByContent($w2_thisStudentID, $r_contentCode, $writingStatusArr=null, $withinPeriod=null, $allowPeerMarking=true, $withinPeerMarkingPeriod=true);
$numOfStudentWriting = count($studentWritingInfoArr);


### Get Student Peer Marking Data
$toBeEvaluatedWritingAry = BuildMultiKeyAssoc($w2_libW2->getStudentPeerMarkingTargetData($w2_thisUserID, null, $markedOnly=false), array('WRITING_ID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
$evaluatedWritingAry = BuildMultiKeyAssoc($w2_libW2->getStudentPeerMarkingTargetData($w2_thisUserID, null, $markedOnly=true), array('WRITING_ID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);


### Prepare temp table for display
$SuccessArr = array();
$sql = '';
$sql .= "Create Temporary Table If Not Exists TMP_W2_PAGE (
			WRITING_NAME varchar(255) default null,
			SCHEME_NAME_SPAN text default null,
			SCHEME_NAME varchar(255) default null,
			TO_BE_EVALUATED_LINK text default null,
			TO_BE_EVALUATED_NUM int(4) default 0,
			EVALUATED_LINK text default null,
			EVALUATED_NUM int(4) default 0
		) ENGINE=InnoDB Charset=utf8
";
$SuccessArr['CreateTempTable'] = $objDb->db_db_query($sql);


$insertAry = array();
for ($i=0, $i_max=$numOfStudentWriting; $i<$i_max; $i++) {
	$_writingId = $studentWritingInfoArr[$i]['WRITING_ID'];
	$_writingName = $studentWritingInfoArr[$i]['TITLE'];
	$_schemeCode = $studentWritingInfoArr[$i]['SCHEME_CODE'];
	
	### Theme
	$_schemeSpanId = 'schemeNameSpan_'.$_writingId;
	$_onMouseOut = 'onmouseout="hideFloatLayer();"';
	$_onMouseOver = 'onmouseover="loadWritingIntroFloatLayer(\''.$_writingId.'\', \''.$_schemeSpanId.'\', \'layer_ex_brief\');"';
	$_schemeName = $w2_libW2->getSchemeNameBySchemeCode($_schemeCode,$r_contentCode); 
	$_schemeNameSpan = '<span id="'.$_schemeSpanId.'" '.$_onMouseOut.' '.$_onMouseOver.'>'.$_schemeName.'</span>';
	
	### "To be Evaluated" and "Evaluated"
	$_paraAssoAry = array();
	$_paraAssoAry['mod'] = 'peerMarking';
	$_paraAssoAry['task'] = 'listMarkWritingStudent';
	$_paraAssoAry['r_contentCode'] = $r_contentCode;
	$_paraAssoAry['r_writingId'] = $_writingId;
	
	// To be Evaluated
	$_toBeEvaluatedLink = '';
	$_numOfToBeEvaluated = count((array)$toBeEvaluatedWritingAry[$_writingId]);
	if ($_numOfToBeEvaluated > 0) {
		$_paraAssoAry['r_showEvaluated'] = 0;
		$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
		
		$_showNewIcon = false;
		$_viewStatusAry = Get_Array_By_Key($toBeEvaluatedWritingAry[$_writingId], 'VIEW_STATUS');
		if (in_array($w2_cfg["DB_W2_PEER_MARKING_DATA_VIEW_STATUS"]["notViewed"], $_viewStatusAry)) {
			$_showNewIcon = true;
		}
		
		$_toBeEvaluatedLink .= '<a href="?'.$_para.'">'.$_numOfToBeEvaluated.'</a>';
		if ($_showNewIcon) {
			$_toBeEvaluatedLink .= ' '.$newIcon;
		}
	}
	else {
		$_toBeEvaluatedLink .= $_numOfToBeEvaluated;
	}
	
	// Evaluated
	$_evaluatedLink = '';
	$_numOfEvaluated = count((array)$evaluatedWritingAry[$_writingId]);
	if ($_numOfEvaluated > 0) {
		$_paraAssoAry['r_showEvaluated'] = 1;
		$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
		
		$_showNewIcon = false;
		$_viewStatusAry = Get_Array_By_Key($evaluatedWritingAry[$_writingId], 'VIEW_STATUS');
		if (in_array($w2_cfg["DB_W2_PEER_MARKING_DATA_VIEW_STATUS"]["notViewed"], $_viewStatusAry)) {
			$_showNewIcon = true;
		}
		
		$_evaluatedLink .= '<a href="?'.$_para.'">'.$_numOfEvaluated.'</a>';
		if ($_showNewIcon) {
			$_evaluatedLink .= ' '.$newIcon;
		}
	}
	else {
		$_evaluatedLink .= $_numOfEvaluated;
	}
	
	$insertAry[$i] = "";
	$insertAry[$i] .= " ('".$objDb->Get_Safe_Sql_Query($_writingName)."', '".$objDb->Get_Safe_Sql_Query($_schemeNameSpan)."', '".$objDb->Get_Safe_Sql_Query($_schemeName)."', '".$objDb->Get_Safe_Sql_Query($_toBeEvaluatedLink)."', '".$_numOfEvaluated."', '".$objDb->Get_Safe_Sql_Query($_evaluatedLink)."', '".$_numOfEvaluated."') ";
}

$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$sql = "Insert Into TMP_W2_PAGE Values ".implode(",", (array)$insertAry);
	$SuccessArr['InsertTempTable'] = $objDb->db_db_query($sql);
}


### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_GET['curPage']))? $_GET['curPage'] : 1;
$numPerPage = (isset($_GET['numPerPage']))? $_GET['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_GET['sortColumnIndex']))? $_GET['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_GET['sortOrder']))? $_GET['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];

$objTableMgr = new libtable_mgr();
$objTableMgr->setSql('Select * From TMP_W2_PAGE');
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);

$objTableMgr->addColumn('WRITING_NAME', 1, $Lang['W2']['exerciseName'], '35%');
$objTableMgr->addColumn('SCHEME_NAME_SPAN', 1, $Lang['W2']['theme'], '25%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='SCHEME_NAME');
$objTableMgr->addColumn('TO_BE_EVALUATED_LINK', 1, $Lang['W2']['toBeEvaluated'], '20%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='TO_BE_EVALUATED_NUM');
$objTableMgr->addColumn('EVALUATED_LINK', 1, $Lang['W2']['evaluated'], '20%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='EVALUATED_NUM');

$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$r_contentCode);

$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());

$h_tableResult = $objTableMgr->display();
$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');

$linterface->LAYOUT_START();
include_once('templates/'.$mod.'/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>