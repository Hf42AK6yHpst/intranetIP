<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
#init
$cid = trim($_POST['cid']);
$step = trim($_POST['step']);
$type = trim($_POST['type']);
$parseData = $engContentParser->parseEngContent($cid);
$step3Data = $parseData['step3Data'];
$step3Int = $step3Data['step3Int'];
$step3OutlineAry = $step3Data['step3OutlineAry'];
$step3Writing = intranet_htmlspecialchars(trim($_POST['step3Writing']));
##Prepare Step3 Rmark Data
$step3RemarkAry = array();
for($i=1;$i<=$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
	$_title = $_POST['remarkTitle_'.$i];
	$_content = $_POST['remarkContent_'.$i];
	if(!empty($_title)||!empty($_content))
		$step3RemarkAry['remarkItem'.$i] = array('remarkTitle'=>$_title,'remarkContent'=>$_content);		
	else
		$step3RemarkAry['remarkItem'.$i] = array();									
}	
$buildData = $objEngContentBuilder->buildChiStep3($step3Int,$step3OutlineAry,$step3Writing,$step3RemarkAry);
$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";

$result = $objDB->db_db_query($sql);
debug_r($sql);

echo $result;
?>

