<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
#init
$attachmentAry = array();
$cid = trim($_POST['cid']);
$step = trim($_POST['step']);
$type = trim($_POST['type']);
$parseData = $engContentParser->parseEngContent($cid);
$step3Data = $parseData['step3Data'];
$step3Int = $step3Data['step3Int'];
##Prepare Step3 Paragraph Data
$step3ParagraphAry = array();
if(!empty($parseData['step3Data']['step3SampleWriting'])){
	$paragraphAry['paragraphItem1']['paragraph'] = $parseData['step3Data']['step3SampleWriting'];
	$paragraphCounter = 1;
}else{
	$paragraphAry = $parseData['step3Data']['step3ParagraphAry'];
	$paragraphCounter = count($paragraphAry);
}
for($i=1;$i<=$paragraphCounter;$i++){
	$_paragraphAry = $paragraphAry['paragraphItem'.$i];
	$_title = !empty($_paragraphAry['leftBubbleTitle'])?$_paragraphAry['leftBubbleTitle']:'';
	$_content = !empty($_paragraphAry['leftBubbleContent'])?$_paragraphAry['leftBubbleContent']:'';
	$_paragraph = !empty($_paragraphAry['paragraph'])?$_paragraphAry['paragraph']:'';
	if(!empty($_paragraph)){
		$step3ParagraphAry[] = array('leftBubbleTitle'=>$_title,'leftBubbleContent'=>$_content,'paragraph'=>$_paragraph);
	}			
}
##Prepare Step3 Rmark Data
$step3RemarkAry = array();
for($i=1;$i<=$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
	if(count($parseData['step3Data']['step3RemarkAry']['remarkItem'.$i])>0){
		$step3RemarkAry[] = $parseData['step3Data']['step3RemarkAry']['remarkItem'.$i];	
	}else{
		$step3RemarkAry[] = array();
	}		
}
$step3Vocab = !empty($step3Data["step3Vocab"])?$step3Data["step3Vocab"]:'';
$step3Grammar = !empty($step3Data["step3Grammar"])?$step3Data["step3Grammar"]:'';

$match = false;
$result = 0;
switch($type){
	case 'vocab':$match=true;$step3Vocab=intranet_htmlspecialchars(trim($_POST['step3Vocab']));break;
	case 'grammar':$match=true;$step3Grammar=intranet_htmlspecialchars(trim($_POST['step3Grammar']));break;	
}

if($match){
	#Existing Files
	$buildData = $objEngContentBuilder->buildStep3New($step3Int, $step3ParagraphAry, $step3RemarkAry, $step3Vocab, $step3Grammar);
	$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";
	$result = $objDB->db_db_query($sql);
}

echo $result;
?>

