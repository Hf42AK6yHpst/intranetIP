<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefCategoryFactory.php');

$r_infoboxCode = $_POST['r_infoboxCode'];
$r_writingStudentId = $_POST['r_writingStudentId'];
$r_refCategoryId = $_POST['r_refCategoryId'];
$r_refCategoryTitle = trim(stripslashes($_POST['r_refCategoryTitle']));
$r_refCategoryCssSet = $_POST['r_refCategoryCssSet'];


$objW2RefCategorySelfAdd = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["selfAdd"], $r_refCategoryId);
$objW2RefCategorySelfAdd->setInfoboxCode($r_infoboxCode);
$objW2RefCategorySelfAdd->setWritingStudentId($r_writingStudentId);
$objW2RefCategorySelfAdd->setTitle($r_refCategoryTitle);
$objW2RefCategorySelfAdd->setCssSet($r_refCategoryCssSet);
$objW2RefCategorySelfAdd->setModifiedBy($_SESSION['UserID']);
if ($r_refCategoryId == '') {
	$objW2RefCategorySelfAdd->setInputBy($_SESSION['UserID']);
}
$refCategoryId = $objW2RefCategorySelfAdd->save();

echo ($refCategoryId > 0)? $refCategoryId : '0';
?>