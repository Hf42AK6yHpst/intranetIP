<?
include_once($PATH_WRT_ROOT."includes/json.php");

$YearClassID = $_POST['YearClassID'];
$SelectedStudentArr = (is_array($_POST['r_selectedStudentId']))? $_POST['r_selectedStudentId'] : array();

$student_arr = _GET_W2_STUDENT_ARR($YearClassID, $SelectedStudentArr);

$json = new JSON_obj();
$json_str = $json->encode($student_arr);
echo $json_str;

//this function copy for ies f:GET_IES_STUDENT_ARR
function _GET_W2_STUDENT_ARR($YearClassID="", $ExcludeStudentID=array()){
		global $intranet_db,$intranet_root;
		$objDb = new libdb();
		/*
		// get the module id of IES 
	  include_once($intranet_root."/includes/libIntranetModule.php");
	  $objModuleLicense = new libintranetmodule();
	  $iesModuleID = $objModuleLicense->getModuleID($ies_cfg["moduleCode"]);
		*/

	  $conds = ($YearClassID == "") ? "" : " AND ycu.YearClassID = ".$YearClassID;
	  $conds .= empty($ExcludeStudentID) ? "" : " AND iu.UserID NOT IN ('".implode("','", (array)$ExcludeStudentID)."')";
	  $conds .= " AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'";

	  // GET THE STUDENT LIST FROM TABLE : INTRANET_MODULE_USER (authorized student to use IES)
	  $sql =  "
				SELECT
				  iu.UserID,
				  ".getNameFieldWithClassNumberByLang ("iu.")." AS StudentName
				FROM
				  {$intranet_db}.INTRANET_USER iu
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
				  ON iu.UserID = ycu.UserID
				INNER JOIN {$intranet_db}.YEAR_CLASS yc
				  ON yc.YearClassID = ycu.YearClassID
				WHERE
				  1
				  $conds
				ORDER BY ".Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN')." , 	
				  ycu.ClassNumber
			  ";


	  $returnArr = $objDb->returnResultSet($sql);

	  return $returnArr;
	}

?>