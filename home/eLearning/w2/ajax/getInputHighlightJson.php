<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefManager.php');
include_once($PATH_WRT_ROOT.'includes/json.php');
$r_infoboxCode = $_POST['r_infoboxCode'];
$r_cid = $_POST['cid'];
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
}
$objJson = new JSON_obj();


### Generate the JSON
for ($i=0, $i_max=count($parseData['step2Data']['step2ArticleAry']); $i<$i_max; $i++) {
	$_infoxboxCode = 'infobox_'.$cid.'_'.$i;
	
	if ($r_infoboxCode != '' && $_infoxboxCode != $r_infoboxCode) {
		continue;
	}
 	
 	$objW2RefManager = new libW2RefManager();
 	$objW2RefManager->setInfoboxCode($r_infoboxCode);
 	$objW2RefManager->setDisplayMode($w2_cfg["refDisplayMode"]["input"]);
 	$w2_h_refHtml[$_infoxboxCode] = $objW2RefManager->returnHighlightInfoAry();
}

echo $objJson->encode($w2_h_refHtml);
?>