<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2PeerMarkingData.php');

$r_peerMarkingDataRecordId = trim(stripslashes($_POST['r_peerMarkingDataRecordId']));

$isCommentEditMode = ($w2_thisAction == $w2_cfg["actionArr"]["peerMarking"])? true : false; 

echo $w2_libW2->getPeerCommentLayerContent($isCommentEditMode, $r_peerMarkingDataRecordId);
?>