<?php 
// editing by Carlos
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
intranet_auth();
intranet_opendb();

$libebookreader = new libebookreader();

$PageMode = $_REQUEST['PageMode'];
$param = str_replace("/home/eLearning/ebook_search/", "", $_SERVER['REQUEST_URI']);

$qmPos = strpos($param, "?", 0);
if($qmPos !== FALSE){
	$param = substr($param,0,$qmPos);
}

$array_param = preg_split('[\\/]', $param, -1, PREG_SPLIT_NO_EMPTY);
$bookID = array_shift($array_param);
if(strtolower($array_param[0])=='search'){
	array_shift($array_param);
}
$file = implode("/", $array_param);

$ebook_root_path = $intranet_root."/file/eBook/".$bookID."/";

// subfolder should be retrived by querying DB for book ID
//$subfolder = "OEBPS/";
$book = $libebookreader->Get_Book_List($bookID);
$subfolder = trim($book[0]['BookSubFolder']);

$file_path = $ebook_root_path . $subfolder.$file;

echo $libebookreader->Get_Rewrite_File_Content($file_path, true, $PageMode);	

intranet_closedb();
?>