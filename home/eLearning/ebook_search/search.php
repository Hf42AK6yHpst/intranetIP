<?php
// editing by  
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT."includes/libebookreader_ui.php");
intranet_auth();
intranet_opendb();

$bid = $_REQUEST['bid'];
$PageMode = $_REQUEST['PageMode'];
$filename = urldecode($_REQUEST['filename']);
$keyword = $_REQUEST['keyword']; // encoded form

if(isset($userBrowser)){
	$platform = $userBrowser->platform;
	$browsertype = $userBrowser->browsertype;
}

$ebook_root_path = $intranet_root."/file/eBook/".$bid."/";
$request_url = $_SERVER['REQUEST_URI'];
if($request_url[strlen($request_url)-1] != '/'){
	$request_url .= '/';
}

$request_url = '/home/eLearning/ebook_search/'.$bid.'/search/';

$libebookreader_ui = new libebookreader_ui();
//$libebookreader = $libebookreader_ui->Get_libebookreader();

echo $libebookreader_ui->Get_Layout_Start();
?>
<script language="JavaScript">
var browsertype = '<?=$browsertype?>';
var pageMode = <?=$PageMode?>;
var pageModeWidth = [99999,650,980];
var pageModeContainerWidth = [99999,768,1024];
var filename = "<?=$filename?>";
var keyword = "<?=$keyword?>"; // encoded form

function jsGetTruePageNumber(jqueryObj){
	var posx = jqueryObj.offset().left;
	var orgx = $(document.getElementById('InnerPostProcessFrame').contentDocument.getElementById('book_content_main')).offset().left;
	 var pageno = jqueryObj.parent().parent().attr("pageno") || jqueryObj.parent().parent().parent().attr("pageno") || jqueryObj.parent().parent().parent().parent().attr("pageno") ;
	
	if(browsertype == 'MSIE'){
		var page = 0;
	}else if(pageno != "" && pageno != undefined){
		var page = [parseInt(pageno), "exact"];
	}else if(pageMode == 2){
		var page = Math.floor((posx - orgx ) / (pageModeWidth[pageMode]/2));
	}else{
		var page = Math.floor((posx - orgx ) / pageModeWidth[pageMode]);
	}
	//console.log('posx = ' + posx);
	//console.log('orgx = ' + orgx);
	return page;
}

function jsLoadDocument()
{
	document.getElementById('InnerPostProcessFrame').src = '<?=$request_url?>' + filename + '?PageMode=' + pageMode;
}

function jsCountPage()
{
	if(document.getElementById('InnerPostProcessFrame').src == ''){
		return;
	}
	var decodedKeyword = decodeURIComponent(keyword);
	var dc = $(document.getElementById('InnerPostProcessFrame').contentDocument.getElementById('book_content_main'));
	
	dc.highlight(decodedKeyword);
	
	var highlightNodes = dc.find('span.highlight');
	var pages = [];
	var prev_text = '';
	if(highlightNodes.length > 0){
		// find page number and paragraph, return to parent window
		for(var i=0;i<highlightNodes.length;i++){
			var jqNode = $(highlightNodes[i]);
			var page_num = jsGetTruePageNumber(jqNode);
			var parent_text = jqNode.parent().text();
			if(parent_text == prev_text){
				continue;
			}
			prev_text = parent_text;
			pages.push({'page':page_num,
						'text':encodeURIComponent(parent_text)});
			//console.log('page_num = ' + page_num);
		}
	}
	if(typeof(window.parent.jsSearchCallback) == 'function'){
		window.parent.jsSearchCallback(pages);
	}
	//console.log(pages);
}

$(document).ready(function(){
	jsLoadDocument();
});
</script>
<iframe id="InnerPostProcessFrame" width="1024px" height="768px" src=""></iframe>
<?php
echo $libebookreader_ui->Get_Layout_Stop();
intranet_closedb();
?>