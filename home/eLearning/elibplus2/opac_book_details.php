<?
/*
 * 	Log
 * 
 *  2018-07-16 [Cameron] cast book_id and cover_type_id to integer to avoid sql injection [case #G142774]
 *  
 * 	2016-09-27 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

//intranet_auth();
intranet_opendb();

$libelibplus = new libelibplus();
$is_opac_open = $libelibplus->get_portal_setting('open_opac');
if ((!$is_opac_open) || (isset($_SESSION['UserID']) && $_SESSION['UserID'] > 0)) {
	header("location: /home/eLearning/elibplus2/index.php");
	exit;
}

 
$hideTopMenuPart = ($_GET['target'] == '_blank') ? false : true;	// hide top menu or not

$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$_GET['msg']]);
$hideTopMenuPart = false;	// reset back to show


$libelibplus_ui = new libelibplus_ui();

$cover_type_id = (int) $_GET['cover_type_id'];	// generic physical book cover id
$cover_type_id = $cover_type_id ? $cover_type_id : rand(1,10);
$book_id = (int) $_GET['book_id'];
$book_id = $book_id ? $book_id : 0;
$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);

$detail_data=$lelibplus->getBookDetail();
$stat_data = $lelibplus->getBookStat();

if ($detail_data['image']) {
	$image_no = '';
}
else {
	$image_no = $cover_type_id;
}
$detail_data['image_no'] = $image_no;

if ($permission['admin']){
    $detail_data['recommend']=$lelibplus->getRecommendedClassLevels();
}


$opac_search_ebook = $libelibplus->get_portal_setting('opac_search_ebook');
if (!$opac_search_ebook) {
	$book_type 	= 'type_physical';
}
else {
	$book_type 	= 'type_all';
}


?>

<!--CONTENT starts-->
<div class="elib-content">

  <!--BOOK DETAILS starts-->
  <div class="container book-details">
    <div class="table-header"><?=$Lang["libms"]["portal"]["book_details"]?></div>
    <div class="table-body bg-brown">
    	<div class="details-left col-sm-3">
        	<div class="book-covers">
        		<?=($detail_data['book_type'] == 'ebook') ? $libelibplus_ui->geteBookCover($detail_data, false, "div", "", false) : $libelibplus_ui->getpBookCover($detail_data, false, "div", "", false)?>
			</div>
		</div>
		
		<!-- book basic info, show if the field exist -->
    	<div class="details-right col-sm-9">
        	<div class="details-info-box">
	            <table class="list-table">
	            <tr><td class="list-title" colspan="2"><?=$detail_data['title']?><?=(trim($detail_data['sub_title'])!=''?'<br> - '.trim($detail_data['sub_title']):'')?></td></tr>
	            <tr><td class="list-label"><?=$eLib["html"]["author"]?>:</td><td>
		<? foreach ((array)$detail_data['authors'] as $i=>$author): ?>
		    <?=$i>0?', '.$author:'<a href="advanced_search.php?author='.(libelibplus::special_raw_string($author)).'" target="_parent">'.$author.'</a>'?>
		<? endforeach; ?>
	        	</td></tr>
            	
		<? if (!empty($detail_data['publisher'])) : ?>            	
            <tr><td class="list-label"><?=$eLib["html"]["publisher"]?>:</td><td><a href="advanced_search.php?publisher=<?=libelibplus::special_raw_string($detail_data['publisher'])?>" target="_parent"><?=$detail_data['publisher']?></a></td></tr>
        <? endif; ?>
        
		<? if (!empty($detail_data['publish_year'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]["publish_year"]?>:</td><td><?=$detail_data['publish_year']?></td></tr>
		<? endif; ?>
						
		<? if (!empty($detail_data['publisher_place'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]["publish_place"]?>:</td><td><?=$detail_data['publisher_place']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['category'])) : 
			if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
				$catTitle = explode('-',$detail_data['category']);
				if (count($catTitle) > 2) {
					if ($intranet_session_language == 'en') {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[1]; 
					}	
					else {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[2];
					}
				}
				else {
					$dispTitle = $detail_data['category'];
				}
			}
			else {
				$dispTitle = $detail_data['category']; 
			}		
		?>
			<tr><td class="list-label"><?=$eLib["html"]["category"]?>:</td><td><a href="advanced_search.php?category=<?=libelibplus::special_raw_string($detail_data['category'])?>" target="_parent"><?=$dispTitle?></a><?=(empty($detail_data['subcategory']))? '' : ' > <a href="advanced_search.php?page=search&subcategory='.libelibplus::special_raw_string($detail_data['subcategory']).'" target="_parent">'.$detail_data['subcategory'].'</a>'?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['subject'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]['Subject']?>:</td><td><?=$detail_data['subject']?></td></tr>
		<? endif; ?>

		<? if (!empty($detail_data['level']) || $detail_data['level'] == "0") : ?>
			<tr><td class="list-label"><?=$eLib['Book']["Level"]?>:</td><td><a href="advanced_search.php?level=<?=libelibplus::special_raw_string($detail_data['level'])?>" target="_parent"><?=$detail_data['level']?></a></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['language'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]["language"]?>:</td><td><?=$detail_data['language']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['edition'])) : ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]["edition"]?>:</td><td><?=$detail_data['edition']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['callnumber'])) : ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]['call_number']?>:</td><td><?=$detail_data['callnumber']?></td></tr>
		<? endif; ?>

		<? if (!empty($detail_data['isbns'])) :
			foreach((array)$detail_data['isbns'] as $k=>$v) {
				if (!empty($v)) {
					$isbns[] = $v;	
				}
			}
			if (!empty($isbns)):
		 ?>
			<tr><td class="list-label"><?=$eLib["html"]['ISBN']?>:</td><td><?=implode(", ",$isbns)?></td></tr>
		 <? endif; ?>
		<? endif; ?>

		<? if (!empty($detail_data['no_of_page'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]['NoOfPage']?>:</td><td><?=$detail_data['no_of_page']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['location'])) : ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]["location"]?>:</td><td><?=$detail_data['location']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['tags'])) : ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]["tags"]?>:</td><td>
			<? $t_idx = 0; ?>
		    <? foreach((array)$detail_data['tags'] as $tag):?>
			<?=($t_idx>0)?',':''?> <a href="advanced_search.php?tag_id=<?=$tag['tag_id']?>" target="_parent"><?=$tag['tag_name']?></a><? $t_idx = 1; ?>  
		    <? endforeach;?>
		<? endif;?>
				</td></tr>

		<? if (!empty($detail_data['physical_book_url'])) : 
			if ((strpos($detail_data['physical_book_url'],'http://') === false) && (strpos($detail_data['physical_book_url'],'https://') === false)) {
				$link = 'http://'.$detail_data['physical_book_url']; 
			}
			else {
				$link = $detail_data['physical_book_url']; 
			}
		?>
				<tr valign="top"><td class="list-label"><?=$Lang["libms"]["book"]["URL"]?>:</td><td><a target="_blank" href="<?=$link?>"><span style="display: block; width: 140px; word-wrap: break-word;"><?=$detail_data['physical_book_url']?></span></a></td></tr>
		<? endif; ?>
            
			</table> <!-- end list-table -->
			
			<!-- book description -->
          	<div class="description"><p><?=$detail_data['description']?></div>            
            </div> <!-- end details-info-box -->
            
        </div>
    </div>
  </div>
  <!--BOOK DETAILS ends-->
  
  <!--ACTION BOX starts-->
  <div class="container action-box">
        
    <?=$libelibplus_ui->getRelatedBooksUI($detail_data['id'], $book_type)?>
    
  </div>
  <!--ACTION BOX starts-->
  
</div>
<!--CONTENT ends-->  

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>