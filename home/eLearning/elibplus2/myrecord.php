<?
/*
 * 	Log
 *
 *  2018-06-21 [Cameron]
 *      - load first top tab(current loan) on first load, don't preload other tabs (loan history, penalty, lost record)
 *      - add function change_loan_book_record_filter() [case #Y140487]
 *      - handle tab-loan in handle_change_filter()
 *      	
 * 	2017-01-17 [Cameron]
 * 		- show eBook Record instead of loan record if $plugin['eLib_Lite'] == true
 * 
 * 	2017-01-10 [Cameron]
 * 		- add container user-photo-container to embrace my photo
 * 
 * 	2016-12-06 [Cameron] 
 * 		- fix bug: cannot click subsequent renew button by student after renew first book [case #B109758]
 * 
 * 	2016-08-09 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

session_register_intranet('elibplus2_menu_tab','myrecord');

$lelibplus = new elibrary_plus();

## Layout start
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START();

$libelibplus_ui = new libelibplus_ui();

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);

$isPurchasedeBook = $lelibplus->isPurchasedeBook();

list($record_data['name'],$record_data['class'],$record_data['image'])=$lelibplus->getUserInfo();

if (!$plugin['eLib_Lite']) {
	$loan_data['loan_books']		= $lelibplus->getUserLoanBooksCurrent(0,1000); // max 1000 records
	$loan_data['reserved_books']	= $lelibplus->getUserReservedBooks();
	$loan_data['penalty_current']	= $lelibplus->getUserPenaltyCurrent();
	
	$main_content = $libelibplus_ui->getMyRecordSideTabLoanBookRecordUI($loan_data);
}
else {
	$page_no = 1;
	$record_per_page = 10;
	$offset = 0;
	list($total,$ebook_data['reading'])=$lelibplus->getReadingProgress($offset, $record_per_page, true);
	
	$nav_para['show_total'] = true;
	$nav_para['total'] = $total;
	$nav_para['current_page'] = $page_no;
	$nav_para['record_per_page'] = $record_per_page;
	$nav_para['unit'] = $Lang["libms"]["portal"]["book_total"];
	
	$main_content = $libelibplus_ui->getMyRecordSideTabeBookRecordUI($ebook_data, $nav_para);
}
?>
  
<!--CONTENT starts-->
<div class="elib-content">
  <div class="container myrecord">
    <ul class="side-tabs nav nav-pills nav-stacked col-md-2">
      <li class="user-photo-container">
      	<div class="user-photo">
      		<img src="<?=$record_data['image']?>">
      	</div><br><?=$record_data['name']?> <span><?=$record_data['class']?></span></li>
<? if (!$plugin['eLib_Lite']):?>      	
      <li class="active"><a href="#tab-loan" data-toggle="pill"><?=$eLib_plus["html"]["myloanbookrecord"]?></a></li>
	<? if ($isPurchasedeBook):?>      
	      <li><a href="#tab-ebook" data-toggle="pill"><?=$eLib_plus["html"]["ebookrecord"]?></a></li>
	<? endif;?>      
<? else:?>
	  <li class="active"><a href="#tab-ebook" data-toggle="pill"><?=$eLib_plus["html"]["ebookrecord"]?></a></li>
<? endif;?>      
      <li><a href="#tab-fav" data-toggle="pill"><?=$eLib_plus["html"]["myfavourite"]?></a></li>
      <li><a href="#tab-review" data-toggle="pill"><?=$eLib_plus["html"]["myreview"]?></a></li>
    </ul>
    <div class="tab-content col-md-10">
    	<!-- right hand side content -->
    	<?=$main_content?>
    </div>
  </div>
</div>
<!--CONTENT ends--> 


<script language="javascript">
$(document).ready(function(){

	// side tab click
	$('.side-tabs a[href^="#tab-"]').on('click', function(e) {
	  	e.preventDefault();
	  	var tab = $(this).attr('href');
	  	tab = tab.replace('#','');
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_my_record.php',
			data : 'action='+tab,		  
			success: function(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
				  	$('.myrecord .tab-content').html(ajaxReturn.html);
				  	if (tab=='tab-review') {
					  	if ($('.table-footer').length) {
					  		$('.table-footer').remove();
					  	}
					  	$('#tab-review .table-body').after(ajaxReturn.footer);
				  	}
				}
			},
			error: show_ajax_error
		});
	});

	// in side tab 1 (Loan Book Record), top tab click: Current Status / Loan Record / Penalty Record / Lost Record
	$(document).on('click', '.loan-tabs li:not(.active) a[href^="#loan-"]', function(e) {
     	e.preventDefault();
     	var tab = $(this).attr('href');
     	tab = tab.replace('#','');
    	$.ajax({
    		dataType: "json",
    		type: "POST",
    		url: 'ajax_get_my_record.php',
    		data : 'action='+tab,		  
    		success: function(ajaxReturn) {
    			if (ajaxReturn != null && ajaxReturn.success){
				  	if ($('.table-footer').length) {
				  		$('.table-footer').remove();
				  	}
    			  	$('.loan-tabs .tab-content').html(ajaxReturn.html);
    			}
    		},
    		error: show_ajax_error
    	});
    });
	
	// in side tab 2 (eBook Record), top tab click: My Reading Record / My Notes 
	$(document).on('click', '.loan-tabs li:not(.active) a[href^="#ebook-"]', function(e) {
	  	e.preventDefault();
	  	var tab = $(this).attr('href');
	  	tab = tab.replace('#','');
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_my_record.php',
			data : 'action='+tab,		  
			success: function(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
				  	if ($('.table-footer').length) {
				  		$('.table-footer').remove();
				  	}
				  	$('#tab-ebook .tab-content').html(ajaxReturn.html);
				}
			},
			error: show_ajax_error
		});
	});
	
	$(document).on('click', '#tab-fav .btn-views a span', function(e){
		var target_type = $(e.target).parent().attr('href').replace('#','');
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_my_record.php',
			data : {action: 'get_fav_' + target_type},			
			success: function(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
					if ($('#tab-fav .table-header .btn-views a').attr('href').replace('#','') == 'cover') {
						$('#tab-fav .table-header .btn-views').html('<span class="btn-table-view off glyphicon glyphicon-th-large"></span><a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>');
						$('#tab-fav .table-body').addClass('bookshelf');
					}
					else {
						$('#tab-fav .table-header .btn-views').html('<a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a><span class="btn-list-view off glyphicon glyphicon-th-list"></span>');
						$('#tab-fav .table-body').removeClass('bookshelf');
					}
					
				  	$('#tab-fav .table-body').html(ajaxReturn.html);
				}
			}, 
			error: show_ajax_error
		});
	});

	$(document).on('click', '#tab-fav .btn-remove', function(e){
		if(confirm('<?=$eLib_plus["html"]["removefavourite"]?>?')){
		   	$('#tab-fav .table-body').slideUp();
		   	$.get('ajax.php',{action: 'removeFavourite', book_id: $(this).attr('data-book-id')}, function(data){
		   		if (data != null && data.success) {
					$.ajax({
						dataType: "json",
						type: "POST",
						url: 'ajax_get_my_record.php',
						data : {action: 'get_fav_list'},			
						success: function(ajaxReturn) {
							if (ajaxReturn != null && ajaxReturn.success){
								$('#fav-total').html(ajaxReturn.total);
							  	$('#tab-fav .table-body').html(ajaxReturn.html).slideDown();
							}
						}, 
						error: show_ajax_error
					});
		   		}
		    }, 'json');
		}
		return false;
	});

	$(document).on('click', '#loan-current .reserve .btn-remove', function(e){
		if(confirm('<?=$eLib_plus["html"]["areusuretocancelthereservation"]?>')){
		   	$('.myrecord .tab-content').slideUp();
			$.get($(this).attr('href'), function(data){
				load_loan_book_record();
			});
		}
		return false;
	});
	
	$(document).on('click', '.renew-button', function(e) {
        if (confirm('<?=$eLib_plus["html"]["areusuretorenewthebook"]?>')){
//			$.fancybox.showLoading();
			$('.myrecord .tab-content').slideUp();
			$.get('ajax.php', {action: 'renewBook', borrow_id: $(this).attr('data-borrow-id')}, function(data){
//				$.fancybox.hideLoading();
				if (data<=0){
					alert('<?=$eLib_plus["html"]["failedtorenew"] ?>');
					$('.myrecord .tab-content').slideDown();
//					$.fancybox.hideLoading();
				}
				else {
					load_loan_book_record();
//					$.fancybox.hideLoading();
				}
			});
		}
		return false;
	});
	
	var para = {page_no: 1,
				record_per_page: 10};
	
	$(document).on('change', '#page_no, #record_per_page', function(e) {
		e.preventDefault();
		handle_change_filter(para);
	});
	
	$(document).on('click','#PreviousBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())-1);
		$('#PreviousBtn').removeAttr('href');
		handle_change_filter(para);
	});

	$(document).on('click','#NextBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())+1);
		$('#NextBtn').removeAttr('href');
		handle_change_filter(para);
	});
	
	<?=$libelibplus_ui->getBookDetailFancybox()?>	
});

// para is object
function change_loan_book_record_filter(para, active_loan_tab) {
	// history loan record
	if (active_loan_tab == 'loan-record') {
    	$.ajax({
    		dataType: "json",
    		type: "POST",
    		url: 'ajax_get_my_record.php',
    		data : {'action': 'get_loan_book_record_loan_record',
    				'page_no': para.page_no,
    				'record_per_page': para.record_per_page
    			   },
    		success: function(ajaxReturn){
    			if (ajaxReturn != null && ajaxReturn.success){
    			  	$('#'+active_loan_tab+' tbody').html(ajaxReturn.html).slideDown();
    			  	if ($('.table-footer').length) {
    			  		$('.table-footer').remove();
    			  	}
    			  	$('#'+active_loan_tab).after(ajaxReturn.footer);
    			}
    		},
    		error: show_ajax_error
    	});
	}
}

function change_ebook_record_filter(para, active_loan_tab) {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_my_record.php',
		data : {'action': (active_loan_tab == 'ebook-record') ? 'get_ebook_my_reading_record' : 'get_ebook_my_notes',
				'page_no': para.page_no,
				'record_per_page': para.record_per_page
			   },			
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
			  	$('#'+active_loan_tab+' tbody').html(ajaxReturn.html).slideDown();
			  	if ($('.table-footer').length) {
			  		$('.table-footer').remove();
			  	}
			  	$('#'+active_loan_tab).after(ajaxReturn.footer);
			}
		},
		error: show_ajax_error
	});
}

function change_review_filter(para) {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_my_record.php',
		data : {'action': 'tab-review',
				'page_no': para.page_no,
				'record_per_page': para.record_per_page,
				'not_show_header': 1
			   },			
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
			  	$('#tab-review .table-body').html(ajaxReturn.html).slideDown();
			  	if ($('.table-footer').length) {
			  		$('.table-footer').remove();
			  	}
			  	$('#tab-review .table-body').after(ajaxReturn.footer);			  	
			}
		},
		error: show_ajax_error
	});
}

function handle_change_filter(para) {
	var active_side_tab = $('.side-tabs li.active').find('a').attr('href').replace('#','');
	para.page_no = $('#page_no').val();
	para.record_per_page = $('#record_per_page').val();
	if (active_side_tab == 'tab-ebook') {
		var active_loan_tab = $('.loan-tabs li.active').find('a').attr('href').replace('#','');
		change_ebook_record_filter(para, active_loan_tab);	
	}
	else if (active_side_tab == 'tab-review') {
		change_review_filter(para);
	}
	else if (active_side_tab == 'tab-loan') {
		var active_loan_tab = $('.loan-tabs li.active').find('a').attr('href').replace('#','');
		change_loan_book_record_filter(para, active_loan_tab);
	}
}

function load_loan_book_record() {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_my_record.php',
		data : 'action=tab-loan',		  
		success: function(ajaxReturn) {
			if (ajaxReturn != null && ajaxReturn.success){
			  	$('.myrecord .tab-content').html(ajaxReturn.html).slideDown();
			}
		},
		error: show_ajax_error
	});
}


function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

function ViewNotesContent_htmlbook(id, title, noteId,PageNum1PageMode, PageNum2PageMode)
{
 	var url = '../ebook_reader/'+id+'/reader/';
     var newWindow = window.open(url, "title","status=1");

      if (!newWindow) return false;
	
	var formid = "newForm", ID = 'id';
	
	
     var html = "";	     
     html += "<html><head></head><body><form  id='formid' method='post' action='" + url + "'>";
     html += "<input type='hidden' name='mynote_p1' value='" + PageNum1PageMode+ "'/>";
     html += "<input type='hidden' name='mynote_p2' value='" + PageNum2PageMode+ "'/>";     
     html += "<input type='hidden' name='action' value='mynote'/>";
     html += "</form></body><script>document.getElementById('formid').submit();<\/script></html>";

     newWindow.document.write(html);
     	     
     return newWindow;	
};    


function MM_openBrWindowFull(theURL,winName,features, centerWindow) { //v2.0

  var is_swf = theURL.indexOf('tool/index.php?BookID=');
  var centerWindow = true;//(centerWindow)? centerWindow : false;
  
  if(is_swf != -1)
  {
	  params  = 'width='+screen.width;
	  params += ', height='+screen.height;
	  params += ', top=0, left=0'
	  params += ', fullscreen=yes';
	  params += ', ' + features;
	  window.open(theURL,winName,params);
  }
  else
  {
	  var w = 800;
	  var h = 650;
	  params  = 'width='+w;
	  params += ', height='+h;
	  params += ', top=0, left=0'
	  params += ', fullscreen=no';
	  params += ', ' + features;
	  
	  
	  if(centerWindow){
	  	var wleft 	= (screen.width - w) / 2;
	  	var wtop 	= (screen.height - h) / 2;
	  	params += ",left=" +wleft+ ", top=" + wtop
	  }
	  
	  var win = window.open(theURL,winName,params);
	  
	  if(centerWindow){
	  	//Just in case width and height are ignored
		win.resizeTo(w, h);
		
		//Just in case left and top are ignored
		win.moveTo(wleft, wtop);
		win.focus();
	  }
  }
}

function ViewNotesContent(id, notes, noteId)
{
	//alert(id + " " + notes);
	MM_openBrWindowFull('/home/eLearning/elibrary/tool/index.php?BookID='+id+'&NoteType='+notes+'&NoteID='+noteId,'booktool','scrollbars=yes');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>