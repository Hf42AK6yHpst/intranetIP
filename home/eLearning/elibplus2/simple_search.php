<?
/*
 * 	Log
 * 
 * 	2017-05-17 [Cameron] fix bug: subsequent simple search need to update hid_keyword field value for navigation
 * 
 * 	2017-04-06 [Cameron] handle apostrophe problem by applying stripslashes() for search field when get_magic_quotes_gpc() is not set (php5.4)
 * 
 * 	2016-10-05 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

//intranet_auth();
intranet_opendb();

if ($_SESSION['UserID']) {
	$permission = elibrary_plus::getUserPermission($UserID);
	if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
		$libelibplus_ui = new libelibplus_ui();
		echo $libelibplus_ui->geteLibCloseUI();
		die();
	}
}
else {
	$libelibplus = new libelibplus();
	$is_opac_open = $libelibplus->get_portal_setting('open_opac');
	if (!$is_opac_open) {
		intranet_closedb();
		header("location: /home/eLearning/elibplus2/index.php");
		exit;
	}
}

$elibplus2_menu_tab = '';

$lelibplus = new elibrary_plus();
$libelibplus = new libelibplus();

// Getting search result
$search_result = array();
$search_result = $libelibplus->get_search_result();

if (count($search_result)) {
	$books_data = $search_result['books_data'];
	$nav_para = $search_result['nav_para'];
	$show_result = true;
	
	if(count($books_data)>1) {
		$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
	}
	else {
		$navigation_id = '';
	}
}
else {
	$show_result = false;
}

## Layout start
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$_GET['msg']]);

$libelibplus_ui = new libelibplus_ui();

$keyword = trim($_POST['keyword']);
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}
$keyword = intranet_htmlspecialchars($keyword);
?>

  
<!--CONTENT starts-->
<div class="elib-content"> 

 <form class="form-horizontal" name="advanced_search_form" id="advanced_search_form" method="post" action="">

	<input type="hidden" name="form_method" value="post">
    <input type="hidden" name="search_type" value="simple_search">
    <input type="hidden" name="update_navigation" id="update_navigation" value="1">
	<input type="hidden" name="order" id="order" value="">
	<input type="hidden" name="sortby" id="sortby-select" value="">
	<input type="hidden" name="view_type" id="view_type" value="">	<!-- cover / list -->
	<input type="hidden" name="keyword" id="hid_keyword" value="<?=$keyword?>">
  
  <!-- Show search result starts -->  	
  <div class="container search-result" id="search-result" style="display:none">
    <div class="table-header">
    	<?=$eLib["html"]["search_result"]?> <span class="total-no"><span id="total"><?=($show_result?$nav_para['total']:'')?></span><?=' '. $Lang["libms"]["portal"]["books_found"]?></span>
    	<div class="btn-views">
        	<span class="btn-table-view off glyphicon glyphicon-th-large"></span>  
    		<a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>
        </div>  
    </div>
    <div class="table-body">
    	<? if ($show_result) include('book_cover_view.php');?>
    </div>  
  	<div class="table-footer">
  		<? if ($show_result) echo $libelibplus_ui->getSearchResultListFooter($nav_para);?>
  	</div>  
  </div>
  <!-- Show search result ends -->
 </form>  	
  
  	
</div>
<!--CONTENT ends--> 


<script language="javascript">
$(document).ready(function(){
	
    $('#advanced_search_form').submit(function(e){
    	e.preventDefault();    	
    	ajax_search_book($(this),1);
    });

    $('#search_form').submit(function(e){
    	e.preventDefault();
    	$('#hid_keyword').val($('#keyword').val());
    	ajax_search_book($(this),1);
    });
	
	$(document).on('change', '#page_no, #record_per_page', function(e) {
		e.preventDefault();
		ajax_search_book($('#advanced_search_form'),1);
	});
	
	$(document).on('click','#PreviousBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())-1);
		$('#PreviousBtn').removeAttr('href');
		ajax_search_book($('#advanced_search_form'),1);
	});

	$(document).on('click','#NextBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())+1);
		$('#NextBtn').removeAttr('href');
		ajax_search_book($('#advanced_search_form'),1);
	});
	
	$(document).on('click', '.btn-views a span', function(e){			// cover / list view icon click
		var target_type = $(e.target).parent().attr('href').replace('#','');
		$('#view_type').val(target_type);
		$('#update_navigation').val('0');
		ajax_search_book($('#advanced_search_form'),0);
		$('#update_navigation').val('1');	// set back to original value		
	});
	
	<? if ($show_result): ?>
		$('#search-result').show();
	<? endif;?>
	
	<?=$libelibplus_ui->getBookDetailFancybox()?>
			
});

function ajax_search_book(form,update_navigation) {
	$.fancybox.showLoading();
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_search_result.php',
		data : form.serialize(),		  
		success: update_navigation ? load_search_result_and_navigation : load_search_result,
		error: show_ajax_error
	});	
}

function load_search_result(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$.fancybox.hideLoading();
		if ($('.table-header .btn-views a').attr('href').replace('#','') == 'cover') {
			$('.table-header .btn-views').html('<span class="btn-table-view off glyphicon glyphicon-th-large"></span><a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>');
		}
		else {
			$('.table-header .btn-views').html('<a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a><span class="btn-list-view off glyphicon glyphicon-th-list"></span>');
		}

	  	$('#search-result div.table-body').html(ajaxReturn.html);
	  	$('#total').html(ajaxReturn.total);
		$('#search-result').show();
	}
}

function load_search_result_and_navigation(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$.fancybox.hideLoading();
	  	$('#search-result div.table-body').html(ajaxReturn.html);
	  	$('#total').html(ajaxReturn.total);
	  	$('.table-footer').replaceWith(ajaxReturn.footer_html);
	  	$('#search-result').show();
	}
}

function sortBookList(field, order) {
	$('#sortby-select').val(field);
	$('#order').val(order);
	ajax_search_book($('#advanced_search_form'),1);	
}
	
function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>