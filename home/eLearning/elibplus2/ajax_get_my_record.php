<?
/*
 * 	Log
 *
 *  2018-06-21 [Cameron]
 *      - retrieve total number of records and pass $nav_para in getMyRecordLoanRecordUI() [case #Y140487]
 *      - add case get_loan_book_record_loan_record
 *       
 * 	2016-08-09 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

$libelibplus_ui = new libelibplus_ui();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = true;	// whether to remove new line, carriage return, tab and back slash

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);

switch($action) {
## side tab 1: loan book record
	case "loan-current":	// top tab 1
        $loan_data['loan_books']		= $lelibplus->getUserLoanBooksCurrent(0,1000); // max 1000 records
		$loan_data['reserved_books']	= $lelibplus->getUserReservedBooks();
        $loan_data['penalty_current']	= $lelibplus->getUserPenaltyCurrent();
        $x = $libelibplus_ui->getMyRecordCurrentStatusUI($loan_data);
        $json['success'] = true;
		break;
		
	case "loan-record": 	// top tab 2
	    $page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
	    $record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
	    $offset = $record_per_page * ($page_no-1);
	    list($total,$loan_data['loan_history'])	= $lelibplus->getUserLoanBooksHistory($offset, $record_per_page, $withTotal = true);
	    
	    $nav_para['show_total'] = true;
	    $nav_para['total'] = $total;
	    $nav_para['current_page'] = $page_no;
	    $nav_para['record_per_page'] = $record_per_page;
	    $nav_para['unit'] = $Lang["libms"]["portal"]["totalRecords"];
		
	    $x = $libelibplus_ui->getMyRecordLoanRecordUI($loan_data, $nav_para);
		$json['success'] = true;
		break;
		
	case "loan-penalty":	// top tab 3
		$loan_data['penalty_history']	= $lelibplus->getUserPenaltyHistory();
		$x = $libelibplus_ui->getMyRecordPenaltyUI($loan_data);
		$json['success'] = true;
		break;
		
	case "loan-lost":		// top tab 4
		$loan_data['lost_books']	= $lelibplus->getUserLostBooksHistory();
		$x = $libelibplus_ui->getMyRecordLostBookUI($loan_data);
		$json['success'] = true;
		break;

	case "get_loan_book_record_loan_record":  // side tab 1 top tab 2: change navigation in loan history
	    $page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
	    $record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
	    $offset = $record_per_page * ($page_no-1);
	    list($total,$loan_data['loan_history'])	= $lelibplus->getUserLoanBooksHistory($offset, $record_per_page, $withTotal = true);
	    
	    if (count($loan_data['loan_history']) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
	        $page_no = 1;
	        $offset = 0;
	        list($total,$loan_data['loan_history'])	= $lelibplus->getUserLoanBooksHistory($offset, $record_per_page, $withTotal = true);
	    }
	    
	    $start_record_index = $record_per_page * ($page_no-1);
	    $x = $libelibplus_ui->getMyRecordLoanRecordContent($loan_data, $start_record_index);
	    $json['success'] = true;
	    
	    $nav_para['show_total'] = true;
	    $nav_para['total'] = $total;
	    $nav_para['current_page'] = $page_no;
	    $nav_para['record_per_page'] = $record_per_page;
	    $nav_para['unit'] = $Lang["libms"]["portal"]["totalRecords"];
	    
	    $y = $libelibplus_ui->getSearchResultListFooter($nav_para);
	    $y = remove_dummy_chars_for_json($y);
	    $json['footer'] = $y;
	    break;
	    
## side tab click: side tab 1
	case "tab-loan":
        $loan_data['loan_books']		= $lelibplus->getUserLoanBooksCurrent(0,1000); // max 1000 records
		$loan_data['reserved_books']	= $lelibplus->getUserReservedBooks();
        $loan_data['penalty_current']	= $lelibplus->getUserPenaltyCurrent();
        
		$x = $libelibplus_ui->getMyRecordSideTabLoanBookRecordUI($loan_data);
        $json['success'] = true;
		break;
	    
## side tab 2: ebook record
	case "tab-ebook":
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
		list($total,$ebook_data['reading'])=$lelibplus->getReadingProgress($offset, $record_per_page, true);
		
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["book_total"];
		
		$x = $libelibplus_ui->getMyRecordSideTabeBookRecordUI($ebook_data, $nav_para);
        $json['success'] = true;
		break;

	case "get_ebook_my_reading_record":		// change navigation in ebook > my reading record
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
		list($total,$ebook_data['reading'])=$lelibplus->getReadingProgress($offset, $record_per_page, true);
		
		if (count($ebook_data['reading']) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;
			list($total,$ebook_data['reading'])=$lelibplus->getReadingProgress($offset, $record_per_page, true);
		}
		$start_record_index = $record_per_page * ($page_no-1);
		$x = $libelibplus_ui->getMyRecordMyReadingRecordContent($ebook_data, $start_record_index);
		$json['success'] = true;
		
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["book_total"];

		$y = $libelibplus_ui->getSearchResultListFooter($nav_para);
		$y = remove_dummy_chars_for_json($y);
		$json['footer'] = $y;
		break;
 		
	case "get_ebook_my_notes":		// side tab 2 top tab 2: change navigation in ebook > my notes
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);		
		list($total,$ebook_data['notes'])=$lelibplus->getUserNotes($offset, $record_per_page, true);
		
		if (count($ebook_data['notes']) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;			
			list($total,$ebook_data['notes'])=$lelibplus->getUserNotes($offset, $record_per_page, true);
		}
		$start_record_index = $record_per_page * ($page_no-1);
		$x = $libelibplus_ui->getMyRecordMyNotesContent($ebook_data, $start_record_index);
		$json['success'] = true;
		
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["book_total"];

		$y = $libelibplus_ui->getSearchResultListFooter($nav_para);
		$y = remove_dummy_chars_for_json($y);
		$json['footer'] = $y;
		break;

	case "ebook-record":	// side tab 2 top tab 1
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
		list($total,$ebook_data['reading'])=$lelibplus->getReadingProgress($offset, $record_per_page, true);
		
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["book_total"];
		
		$x = $libelibplus_ui->getMyRecordMyReadingRecordUI($ebook_data, $nav_para);
        $json['success'] = true;
		break;
		
	case "ebook-notes":		// side tab 2 top tab 2
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
		list($total,$ebook_data['notes'])=$lelibplus->getUserNotes($offset, $record_per_page, true);
		
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["book_total"];
		
		$x = $libelibplus_ui->getMyRecordMyNotesUI($ebook_data, $nav_para);
        $json['success'] = true;
		break;

	case "get_fav_cover":		// side tab 3 favourite cover view
		$books_data = $lelibplus->getUserFavourites();
		$x = $libelibplus_ui->getMyRecordMyFavouriteCoverView($books_data);
        $json['success'] = true;
		break;

	case "get_fav_list":		// side tab 3 favourite list view
		$books_data = $lelibplus->getUserFavourites();
		$x = $libelibplus_ui->getMyRecordMyFavouriteListView($books_data);
		$json['total'] = count($books_data).' '.$eLib["html"]["books"];
        $json['success'] = true;
		break;

## side tab 3: my favourite books
	case "tab-fav":		// favourite
		$books_data = $lelibplus->getUserFavourites();
		$x = $libelibplus_ui->getMyRecordSideTabMyFavouriteUI($books_data);
        $json['success'] = true;
		break;

## side tab 4: my reviews
	case "tab-review":
		$not_show_header = $_POST['not_show_header'] ? $_POST['not_show_header'] : 0;
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
		$class_name = false;
						
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], 'type_all', '');

		list($total, $reviews_data)=$lelibplus->getUserReviews($_SESSION['UserID'], $class_name, $offset, $record_per_page);

		if (count($reviews_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;
			list($total, $reviews_data)=$lelibplus->getUserReviews($_SESSION['UserID'], $class_name, $offset, $record_per_page);
		}
						
		$start_record_index = $record_per_page * ($page_no-1);
		
		if ($not_show_header) {		// change navigation in my review
			$x = $libelibplus_ui->getReviewByUserUI($reviews_data, $total, $start_record_index, false);
		}
		else {
			$x = $libelibplus_ui->getMyRecordSideTabMyReviewUI($reviews_data, $total, $start_record_index);
		}
		$json['success'] = true;
		
		$nav_para['show_total'] = false;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;

		$y = $libelibplus_ui->getSearchResultListFooter($nav_para);
		$y = remove_dummy_chars_for_json($y);
		$json['footer'] = $y;
		break;
		
}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}
$json['html'] = $x;

echo json_encode($json);

intranet_closedb();
?>