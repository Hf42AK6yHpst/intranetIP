<?php
/*
 * 	Log
 * 
 * 	Note: assume data has been added to portal setting table for initial setup
 * 		here handle portal settings update only
 * 
 * 	2017-01-17 [Cameron]
 * 		- don't handle pBook related items if $plugin['eLib_Lite'] == true
 *
 * 	2017-01-16 [Cameron]
 * 		- add $isPurchasedeBook, don't handle eBook related items if client do not purchase eBook
 * 
 * 	2016-11-11 [Cameron]
 * 		- change access control to $permission['portal_setting'] [case #Z108442]
 * 
 * 	2016-07-07 [Cameron] create this file
 * 	
 */
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

//if (!$permission['portal_setting'] || ($permission['portal_setting'] && $plugin['eLib_Lite'])) {
if (!$permission['portal_setting']) {		
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	intranet_closedb();
	$xmsg = 'UpdateUnsuccess';
	header("Location: portal_settings_edit.php?xmsg=".$xmsg);	
	exit;
}

$lelibplus = new elibrary_plus();
$isPurchasedeBook = $lelibplus->isPurchasedeBook();

$libms = new liblms();

$page_type = $_POST['page_type'];
$page_type = $page_type == 'opac' ? $page_type : 'internal';

$ret = array();

if ($isPurchasedeBook) {
	$exception_array[] = 'ebook_carousel_interval_min';
	$exception_array[] = 'ebook_carousel_interval_sec';
	$ebook_carousel_interval = $_POST['setting']['ebook_carousel_interval_min'] * 60 + $_POST['setting']['ebook_carousel_interval_sec'];
	$sql = "UPDATE `LIBMS_PORTAL_SETTING` SET `value` = {$ebook_carousel_interval} WHERE `name` = 'ebook_carousel_interval' AND page_type='{$page_type}'";
	$ret[] = $libms->db_db_query($sql);
}

if (!$plugin['eLib_Lite']) {		
	$exception_array[] = 'pbook_carousel_interval_min';
	$exception_array[] = 'pbook_carousel_interval_sec';
	$pbook_carousel_interval = $_POST['setting']['pbook_carousel_interval_min'] * 60 + $_POST['setting']['pbook_carousel_interval_sec'];
	$sql = "UPDATE `LIBMS_PORTAL_SETTING` SET `value` = {$pbook_carousel_interval} WHERE `name` = 'pbook_carousel_interval' AND page_type='{$page_type}'";
	$ret[] = $libms->db_db_query($sql);
}

foreach($_POST['setting'] as $k=>$v) {
	if (!in_array($k,$exception_array)) { 
		$sanitized_value = PHPToSQL($v);
		$sanitized_name = PHPToSQL($k);
		$sql = "UPDATE `LIBMS_PORTAL_SETTING` SET `value` = {$sanitized_value} WHERE `name` = {$sanitized_name} AND page_type='{$page_type}'";
		$ret[] = $libms->db_db_query($sql);
	}
}
if (!in_array(false,$ret)) {
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$returnMsgKey = 'UpdateUnsuccess';
}

header("location: portal_settings_edit.php?page_type=".$page_type."&msg=".$returnMsgKey);


intranet_closedb(); 
?>