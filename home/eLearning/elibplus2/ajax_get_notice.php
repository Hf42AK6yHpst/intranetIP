<?php
/*
 * 	Log
 * 
 * 	2016-06-06 [Cameron] create this file
 * 	
 */
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");


//intranet_auth();
intranet_opendb();

if ($_SESSION['UserID']) {
	$permission = elibrary_plus::getUserPermission($UserID);
	if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
		$libelibplus_ui = new libelibplus_ui();
		echo $libelibplus_ui->geteLibCloseUI();
		die();
	}
}
else {
	$libelibplus = new libelibplus();
	$is_opac_open = $libelibplus->get_portal_setting('open_opac');
	if (!$is_opac_open) {
		intranet_closedb();
		header("location: /home/eLearning/elibplus2/index.php");
		exit;
	}
}

$libelibplus_ui = new libelibplus_ui();

$action = $_GET['action']? $_GET['action']: $_POST['action'];
$offset = $_POST['offset'];
$x = '';
$json['success'] = false;

switch($action){
	case "getMoreNoticeItem":
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$total_notice = $lelibplus->getTotalPortalNotices('news');	
		$limit = $elibplus_cfg['portal_news_more']?$elibplus_cfg['portal_news_more']:10;
		$newsItemList = $libelibplus_ui->getNewsItemUI($offset,$limit);

		$x = $newsItemList['html'];
		$json['offset'] = $newsItemList['offset'];
		$json['show_more'] = ($newsItemList['offset'] >= $total_notice ? false : true);
		$json['success'] = true;
		
	break;

	case "news":
		$x = $libelibplus_ui->getPortalNewsUI();
		$json['success'] = true;
	break;

	case "op-hr":
		$x = $libelibplus_ui->getPortalOpeningHoursUI();
		$json['success'] = true;
	break;
	
	case "rules":
		$x = $libelibplus_ui->getPortalRulesUI();
		$json['success'] = true;
	break;

	case "getPrevWeekCalendar":
		$date = $_POST['ref_date'];
		$date = strtotime($date); 
		$timestamp = strtotime("-7 day", $date);
		if (date('Y-m-d',$timestamp)==date('Y-m-d')) {		// navigated to today, use current timestamp
			$is_now = true;
			$timestamp = time();	// current timestamp
		}
		else {
			$is_now = false;
		}	
		$x = $libelibplus_ui->getPortalOpeningHoursItemUI($timestamp,$is_now);
		$json['success'] = true;
	break;

	case "getNextWeekCalendar":
		$date = $_POST['ref_date'];
		$date = strtotime($date); 
		$timestamp = strtotime("+7 day", $date);		
		if (date('Y-m-d',$timestamp)==date('Y-m-d')) {		// navigated to today, use current timestamp
			$is_now = true;
			$timestamp = time();	// current timestamp
		}
		else {
			$is_now = false;
		}	
		$x = $libelibplus_ui->getPortalOpeningHoursItemUI($timestamp,$is_now);
		$json['success'] = true;
	break;
	
}

$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;
echo json_encode($json);

intranet_closedb(); 
?>