<?php
/*
 * 	Log
 * 
 * 	2016-06-03 [Cameron] create this file
 * 	
 */
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
//include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
//include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);
if (!$permission['admin']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$action = $_GET['action']? $_GET['action']: $_POST['action'];

switch($action){
	case "uploadPortalNoticeAttachment":
		$fs = new libfilesystem();
				
		$file 		= $_FILES['attachment'];
		$file_name	= str_replace(array("#"," ","'",'"',"&","?"),"_",$file['name']);

		$temp_dir 	= $intranet_root.'/file/elibplus2/tmp/';
		$temp_path 	= $intranet_root.'/file/elibplus2/tmp/'.$file_name;
		$temp_url 	= '/file/elibplus2/tmp/'.urlencode($file_name);
		
		$fs->createFolder($temp_dir);
		
		move_uploaded_file($file["tmp_name"], $temp_path);
		
		if ($callback){
			$file_extention = getFileExtention($file_name, true);
			
			if (($file_extention == 'jpg') || ($file_extention == 'jpeg') || ($file_extention == 'png') || ($file_extention == 'gif')) {
				$show_image = 1;
			}
			else {
				$show_image = 0;
			}
			
		    echo "<script>$callback('".$temp_url."', '".$file_name."','".$show_image."');</script>";
		}
	break;
	
	case "removePortalNoticeAttachment":		// in adding process, have uploaded attachment, but finally canceled the page
		$temp_file_path = $_POST['temp_file_path'];
		$result = array();
		if ($temp_file_path){
		    $fs = new libfilesystem();
		    $result[] = $fs->file_remove($intranet_root.urldecode($temp_file_path));
		}
		
		$json['success'] = in_array(false,$result)?false:true;
	    echo json_encode($json);
	break;
	
	case "removePortalNotice":		// remove notice and attachment
		$id = $_POST['id'];		
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$notice = current($lelibplus->getPortalNotices(false, $id, 0, 1));
		$attachment = $notice['attachment'];
		
		$result = array();
		if ($attachment) {
			$fs = new libfilesystem();
			$result[] = $fs->file_remove($intranet_root.$attachment);
			$result[] = $fs->folder_remove($intranet_root.dirname($attachment));//will not remove it if still having sth inside
		}
		$result[] = $lelibplus->removePortalNotice($id);		

	    $json['success'] = in_array(false,$result)?false:true;
	    echo json_encode($json);
	break;
}

intranet_closedb(); 
?>