<?
/*
 * 	Log
 * 
 * 	2016-07-20 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

$elibplus2_menu_tab = 'statistics';

$lelibplus = new elibrary_plus();

// get default all reviews data
$order = 'desc';
$sort = 'last_submitted';
$page_no = 1;
$record_per_page = 10;
$order_sql = $sort.' '. $order;
$offset = $record_per_page * ($page_no-1);
$searchFromDate = '';
$searchToDate = '';
list($total,$data) = $lelibplus->getAllReviewsRecord($offset, $record_per_page, $order_sql, $searchFromDate, $searchToDate, true);

$start_record_index = $record_per_page * ($page_no-1);
$libelibplus_ui = new libelibplus_ui();		

$nav_para['show_total'] = true;
$nav_para['total'] = $total;
$nav_para['current_page'] = $page_no;
$nav_para['record_per_page'] = $record_per_page;
$nav_para['unit'] = $Lang["libms"]["portal"]["book_total"];
$nav_para['cust_nav_style'] = 'cust-pagination2';

## Layout start
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START();


?>
  
<!--CONTENT starts-->
<div class="elib-content">
  <div class="container statistics">
    <form name="statistics_form" id="statistics_form" method="post">
    <ul class="side-tabs nav nav-pills nav-stacked col-md-3">
      <li class="active"><a href="#tab-all-reviews" data-toggle="pill"><?=$eLib["html"]["all_reviews"]?></a></li>
  <? if ($_SESSION['UserType'] == 1):?>
      <li><a href="#tab-all-classes" data-toggle="pill"><?=$eLib["html"]["class_summary"]?></a></li>
  <? endif;?>
    </ul>
    
    <div class="tab-content col-md-9" id="statistics-content">
      
      <!-- start of AllReviews -->
      <div class="tab-pane active" id="tab-all-reviews">
        <div class="table-header">      
        	<?=$libelibplus_ui->getStatisticsAllReviewsHeader()?>
        </div>
        
        <div class="table-body">        
			<div class="table-responsive">
			    <table class="table-list" id="all-reviews-table">
			      	<thead>
			        	<tr valign="bottom">
			            	<th>#</th>
			            	<th>&nbsp;</th>
			            	<th><a href="#" name="sort_bt"><?=$eLib["html"]["book_title"]?></a></th>
			            	<th><a href="#" name="sort_a"><?=$eLib["html"]["author"]?></a></th>
			            	<th><a href="#" name="sort_nor"><?=$eLib["html"]["num_of_review"]?></a></th>
			            	<th><a href="#" name="sort_ls" class="sort-desc"><?=$eLib["html"]["last_submitted"]?><span class="glyphicon glyphicon-triangle-bottom"></span></a></th>
			            </tr>
			        </thead>
			        <tbody>
			        	<!-- Statistics AllReviews Content -->
			        	<?=$libelibplus_ui->getStatisticsAllReviewsContent($data, $start_record_index)?>
			        </tbody>			        
    			</table>
			</div>
    	</div>
    	
    	<!-- footer: navigation bar -->
    	<?=$libelibplus_ui->getSearchResultListFooter($nav_para)?>
    	
      </div>
      <!-- end of AllReviews -->

      
      <!-- AllClasses -->
      <!-- start of AllClasses -->
      <div class="tab-pane" id="tab-all-classes">
        <div class="table-header">
        	<?=$libelibplus_ui->getStatisticsAllClassesHeader()?>
        </div>
        <div class="table-body">
            <div class="table-responsive">
	        	<table class="table-list" id="all-classes-table">
	      			<!-- Statistics AllClasses -->
	            </table>
			</div>            
        </div>
        
        <!-- footer: navigation bar -->
        
      </div>    
      <!-- end of AllClasses -->
      
	</div>
	
	</form>
  </div>
</div>
<!--CONTENT ends--> 


<script language="javascript">
$(document).ready(function(){
	var active_side_tab = $('.side-tabs li.active').find('a').attr('href').replace('#','');
	  
	$(document).on('click', '.side-tabs li:not(.active)', function(e){
		$(e.target).addClass('active').siblings().removeClass('active');
		active_side_tab = $(this).find('a').attr('href').replace('#','');
		if ( active_side_tab == "tab-all-classes") {
			$( "#apply_all_classes" ).trigger( "click" );	
		}
		else {
			$( "#apply_all_reviews" ).trigger( "click" );
		}
	});
	
	
	/////////////////////////////////////////////
	// tab-all-reviews scripts	
	// default parameter object
	var para = {searchFromDate: '',
				searchToDate: '',
				page_no: 1,
				record_per_page: 10,
				sort: 'sort_ls',		// last submit
				order: 'desc'};
	
	$(document).on('click', '#apply_all_reviews', function(e){
		e.preventDefault();
		if ($("input:radio[name='searchDataPeriod']:checked").val() == 'Date' ) {
			if($('#searchFromDate').val() > $('#searchToDate').val()){
			    alert('<?=$eLib["html"]["Incorrect_Date"]?>');		
				return false;
			}
			else {
				para.searchFromDate = $('#searchFromDate').val();		
				para.searchToDate = $('#searchToDate').val();
			}			
		}
		else {
			para.searchFromDate = '';		
			para.searchToDate = '';
		}
		para.searchDataPeriod = $("input:radio[name='searchDataPeriod']:checked").val();
		change_all_reviews_filter(para);	
	});

	$(document).on('click','#all-reviews-table th a', function(e){
		e.preventDefault();
		var current_order = $('#all-reviews-table th span').hasClass('glyphicon-triangle-bottom') ? 'desc' : 'asc';
		para.sort = $(this).attr('name');
		if ($(this).hasClass('sort-desc')) {
			para.order = 'asc';		// toggle
			$(this).removeClass('sort-desc').addClass('sort-asc');
			$('#all-reviews-table th span').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-top');
		}
		else if ($(this).hasClass('sort-asc')) {
			para.order = 'desc';	// toggle
			$(this).removeClass('sort-asc').addClass('sort-desc');
			$('#all-reviews-table th span').removeClass('glyphicon-triangle-top').addClass('glyphicon-triangle-bottom');
		}
		else {
			para.order = current_order;
			var text = $(this).text();			
			$('#all-reviews-table th span.glyphicon').remove();
			if (current_order == 'asc') {
				$(this).addClass('sort-asc').parent().siblings().find('a').removeClass('sort-asc');
				$(this).html(text+'<span class="glyphicon glyphicon-triangle-top"></span>');	
			} 
			else {
				$(this).addClass('sort-desc').parent().siblings().find('a').removeClass('sort-desc');
				$(this).html(text+'<span class="glyphicon glyphicon-triangle-bottom"></span>');	
			}
		}
		
		change_all_reviews_filter(para);
	});
			
	$(document).on('focus','#searchFromDate, #searchToDate', function(){
		$("input:radio[name='searchDataPeriod']").prop('checked',true);
	});
		
	// end tab-all-reviews
	/////////////////////////////////////////////
	
	
	/////////////////////////////////////////////
	// start tab-all-classes
	// default parameter object
	var ac_para = {ClassName: '',
				student_id: '',
				FromMonth: '<?=date('m')?>',
				FromYear: '<?=date('Y')?>',
				ToMonth: '<?=date('m')?>',
				ToYear: '<?=date('Y')?>',
				show_column_title: 0,		// reload whole header row or not
				page_no: 1,
				record_per_page: 10,
				sort: 'sort_c',				// class name
				order: 'asc'};
	
	$(document).on('click', '#apply_all_classes', function(e){
		e.preventDefault();
		if($('#FromYear_Search').val() + $('#FromMonth_Search').val() > $('#ToYear_Search').val() + $('#ToMonth_Search').val()){
		    alert('<?=$eLib["html"]["Incorrect_Date"]?>');		
			return false;
		}
		else {
			ac_para.ClassName = $('#ClassName_Search').val();
			ac_para.student_id = '';		
			ac_para.FromMonth = $('#FromMonth_Search').val();
			ac_para.FromYear = $('#FromYear_Search').val();
			ac_para.ToMonth = $('#ToMonth_Search').val();		
			ac_para.ToYear = $('#ToYear_Search').val();
			ac_para.show_column_title = 1;
		}

		$('#student_id').val('');		// reset for export
		$('#statistics_type').val('');
		
		if (ac_para.ClassName == '') {
			ac_para.sort = 'sort_c';	// class name
			ac_para.order = 'asc';
		}
		else {
			ac_para.sort = 'sort_cn';	// class number
			ac_para.order = 'asc';
		}
		change_all_classes_filter(ac_para);	
	});
		
	$(document).on('click','#all-classes-table th a', function(e){
		e.preventDefault();
		var current_order = $('#all-classes-table th span').hasClass('glyphicon-triangle-bottom') ? 'desc' : 'asc';
		ac_para.sort = $(this).attr('name');
		if ($(this).hasClass('sort-desc')) {
			ac_para.order = 'asc';		// toggle
			$(this).removeClass('sort-desc').addClass('sort-asc');
			$('#all-classes-table th span').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-top');
		}
		else if ($(this).hasClass('sort-asc')) {
			ac_para.order = 'desc';	// toggle
			$(this).removeClass('sort-asc').addClass('sort-desc');
			$('#all-classes-table th span').removeClass('glyphicon-triangle-top').addClass('glyphicon-triangle-bottom');
		}
		else {
			ac_para.order = current_order;
			var text = $(this).text();			
			$('#all-classes-table th span.glyphicon').remove();
			if (current_order == 'asc') {
				$(this).addClass('sort-asc').parent().siblings().find('a').removeClass('sort-asc');
				$(this).html(text+'<span class="glyphicon glyphicon-triangle-top"></span>');	
			} 
			else {
				$(this).addClass('sort-desc').parent().siblings().find('a').removeClass('sort-desc');
				$(this).html(text+'<span class="glyphicon glyphicon-triangle-bottom"></span>');	
			}
		}

		if ($('#all-classes-table thead').attr('name') != 'table-header-student') {
			ac_para.student_id = '';
			ac_para.show_column_title = 0;
			change_all_classes_filter(ac_para);
		}
		else {
			ac_para.student_id = $('#student_id').val();
			ac_para.show_column_title = 0;
			change_student_filter(ac_para);
		}
			
	});
		
	$(document).on('click','#all-classes-table td.col-class-name a', function(e){
		e.preventDefault();
		$('#ClassName_Search').val($(this).attr('data-class-name'));
		$( "#apply_all_classes" ).trigger( "click" );		
	});

	$(document).on('click','#all-classes-table td.col-student-name a, #all-classes-table td.col-book-read a', function(e){
		e.preventDefault();
		ac_para.ClassName = $(this).attr('data-class-name');
		ac_para.student_id = $(this).attr('data-student-id');
		$('#student_id').val($(this).attr('data-student-id'));
		$('#statistics_type').val('hits');
		ac_para.show_column_title = 1;
		change_student_filter(ac_para)		
	});

	$(document).on('click','#all-classes-table td.col-book-review a', function(e){
		e.preventDefault();
		ac_para.ClassName = $(this).attr('data-class-name');
		ac_para.student_id = $(this).attr('data-student-id');
		$('#student_id').val($(this).attr('data-student-id'));
		$('#statistics_type').val('reviews');
		ac_para.show_column_title = 0;	// no header row
		change_review_filter(ac_para)		
	});
	
	// end tab-all-classes
	/////////////////////////////////////////////


	$(document).on('change', '#page_no, #record_per_page', function(e) {
		e.preventDefault();
		if (active_side_tab == 'tab-all-reviews') {
			para.page_no = $('#page_no').val();
			para.record_per_page = $('#record_per_page').val();
			change_all_reviews_filter(para);
		}
		else if (active_side_tab == 'tab-all-classes') {
			ac_para.show_column_title = 0;
			ac_para.page_no = $('#page_no').val();
			ac_para.record_per_page = $('#record_per_page').val();
	
			if ($('#all-classes-table thead').attr('name') == 'table-header-student') {
				ac_para.student_id = $('#student_id').val();
				change_student_filter(ac_para);
			}
			else if ($('#all-classes-table thead').attr('name') == 'table-header-reviews') {
				ac_para.student_id = $('#student_id').val();
				change_review_filter(ac_para);
			}
			else {
				ac_para.student_id = '';
				change_all_classes_filter(ac_para);
			}
		}
	});
	
	$(document).on('click','#PreviousBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())-1);
		$('#PreviousBtn').removeAttr('href');
		if (active_side_tab == 'tab-all-reviews') {
			para.page_no = $('#page_no').val();
			change_all_reviews_filter(para);
		}
		else {
			ac_para.show_column_title = 0;
			ac_para.page_no = $('#page_no').val();

			if ($('#all-classes-table thead').attr('name') == 'table-header-student') {
				ac_para.student_id = $('#student_id').val();
				change_student_filter(ac_para);
			}
			else if ($('#all-classes-table thead').attr('name') == 'table-header-reviews') {
				ac_para.student_id = $('#student_id').val();
				change_review_filter(ac_para);
			}
			else {
				ac_para.student_id = '';
				change_all_classes_filter(ac_para);
			}
		}
	});

	$(document).on('click','#NextBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())+1);
		$('#NextBtn').removeAttr('href');
		if (active_side_tab == 'tab-all-reviews') {
			para.page_no = $('#page_no').val();
			change_all_reviews_filter(para);
		}
		else {
			ac_para.show_column_title = 0;
			ac_para.page_no = $('#page_no').val();

			if ($('#all-classes-table thead').attr('name') == 'table-header-student') {
				ac_para.student_id = $('#student_id').val();
				change_student_filter(ac_para);
			}
			else if ($('#all-classes-table thead').attr('name') == 'table-header-reviews') {
				ac_para.student_id = $('#student_id').val();
				change_review_filter(ac_para);
			}
			else {
				ac_para.student_id = '';
				change_all_classes_filter(ac_para);
			}
		}
	});
	
	<?=$libelibplus_ui->getBookDetailFancybox()?>
	
});


// para is object
function change_all_reviews_filter(para) {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_statistics.php',
		data : {'action': 'get_statistics_all_reviews',
				'searchFromDate': para.searchFromDate,
				'searchToDate': para.searchToDate,
				'page_no': para.page_no,
				'record_per_page': para.record_per_page,
				'sort': para.sort,
				'order': para.order
			   },			
		success: function(ajaxReturn){
			show_result(ajaxReturn);
		},
		error: show_ajax_error
	});
}

function show_result(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#all-reviews-table tbody').html(ajaxReturn.html);
	  	if ($('.table-footer').length) {
	  		$('.table-footer').remove();
	  	}
	  	$('#tab-all-reviews .table-body').after(ajaxReturn.footer);
	}
}

function jsExportAllReviews(page) {
	var current_order = $('#all-reviews-table th span').hasClass('glyphicon-triangle-bottom') ? 'desc' : 'asc';
	var current_sort = $('#all-reviews-table th span.glyphicon').parent().attr('name');
	var filter = '&searchDataPeriod=' + $("input:radio[name='searchDataPeriod']:checked").val() + '&searchFromDate=' + $('#searchFromDate').val() + '&searchToDate=' + $('#searchToDate').val() + '&order=' + current_order + '&sort=' + current_sort; 
	self.location = "export_reviews.php?page=" + page + filter;	
}

function jsExportStatistics() {
	var para = "?ClassName=" + $('#ClassName_Search').val() + "&FromMonth=" + $('#FromMonth_Search').val() + "&FromYear=" + $('#FromYear_Search').val() + "&ToMonth=" + $('#ToMonth_Search').val() + "&ToYear=" + $('#ToYear_Search').val() + "&student_id=" + $('#student_id').val() + "&statistics_type=" + $('#statistics_type').val();
	self.location = "export_statistics.php" + para; 
}

function change_all_classes_filter(para) {
	var action = para.ClassName == '' ? 'get_statistics_all_classes' : 'get_statistics_by_class';
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_statistics.php',
		data : {'action': action,
				'ClassName': para.ClassName,
				'FromMonth': para.FromMonth,
				'FromYear': para.FromYear,
				'ToMonth': para.ToMonth,
				'ToYear': para.ToYear,
				'show_column_title': para.show_column_title,
				'page_no': para.page_no,
				'record_per_page': para.record_per_page,
				'sort': para.sort,
				'order': para.order
			   },			
		success: function(ajaxReturn){
			show_all_classes_summary_result(ajaxReturn, para.show_column_title);
		},
		error: show_ajax_error
	});
}

function show_all_classes_summary_result(ajaxReturn, show_column_title) {
	if (ajaxReturn != null && ajaxReturn.success){
		if (show_column_title == 1) {
	  		$('#all-classes-table').html(ajaxReturn.html);
		}
		else {
			if ($('#all-classes-table tbody').length) {
				$('#all-classes-table tbody').remove();
			}
			$('#all-classes-table thead').after(ajaxReturn.html);
		}
	  	if ($('.table-footer').length) {
	  		$('.table-footer').remove();
	  	}
	  	$('#tab-all-classes .table-body').after(ajaxReturn.footer);
	}
}

function change_student_filter(para) {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_statistics.php',
		data : {'action': 'get_statistics_by_student',
				'ClassName': para.ClassName,
				'student_id': para.student_id,
				'FromMonth': para.FromMonth,
				'FromYear': para.FromYear,
				'ToMonth': para.ToMonth,
				'ToYear': para.ToYear,
				'show_column_title': para.show_column_title,
				'page_no': para.page_no,
				'record_per_page': para.record_per_page,
				'sort': para.sort,
				'order': para.order
			   },			
		success: function(ajaxReturn){
			show_all_classes_summary_result(ajaxReturn, para.show_column_title);
		},
		error: show_ajax_error
	});
}

function change_review_filter(para) {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_statistics.php',
		data : {'action': 'get_statistics_review_by_student',
				'ClassName': para.ClassName,
				'student_id': para.student_id,
				'FromMonth': para.FromMonth,
				'FromYear': para.FromYear,
				'ToMonth': para.ToMonth,
				'ToYear': para.ToYear,				
				'page_no': para.page_no,
				'record_per_page': para.record_per_page
			   },			
		success: function(ajaxReturn){
			show_review_by_student(ajaxReturn);
		},
		error: show_ajax_error
	});
}

function show_review_by_student(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#all-classes-table').html(ajaxReturn.html);
	  	if ($('.table-footer').length) {
	  		$('.table-footer').remove();
	  	}
	  	$('#tab-all-classes .table-body:not(#review_body)').after(ajaxReturn.footer);
	}
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>