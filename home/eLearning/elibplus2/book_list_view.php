<?
/*
 * 	Log
 * 
 * 	2017-05-18 [Cameron]
 * 		- replace " with &quot; for hyperlink tilte
 * 
 * 	2016-06-20 [Cameron] create this file
 */

if (!isset($books_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}
?>
      <div class="table-responsive">
      <table class="table-list">
      	<thead>
        	<tr valign="bottom">
            	<th>#</th>
            	<th>&nbsp;</th>
				<th>
				<? if ($search_result['column_sorting']): ?>
				 <a <? if ($search_result['sortby']=='title') :?>
				  		href='#' onclick="sortBookList('title', '<?=($search_result['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$search_result['order']?>"
					<? else: ?>
				  		href='#' onclick="sortBookList('title', 'asc'); return false;"
					<? endif; ?>><?=$eLib["html"]["book_title"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["book_title"]?>
			  	<? endif;?>
				</th>

			  	<th>
			  	<? if ($search_result['column_sorting']): ?>
			  	 <a <? if ($search_result['sortby']=='author') :?>
			  			href='#' onclick="sortBookList('author', '<?=($search_result['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$search_result['order']?>"
			  		<? else: ?>
			  			href='#' onclick="sortBookList('author', 'asc'); return false;"
			  		<? endif; ?>><?=$eLib["html"]["author"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["author"]?>
			  	<? endif;?>
			  	</th>
			  	
			  	<th>
			  	<? if ($search_result['column_sorting']): ?>
			  	 <a <? if ($search_result['sortby']=='publisher') :?>
				  		href='#' onclick="sortBookList('publisher', '<?=($search_result['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$search_result['order']?>"
					<? else: ?>
						href='#' onclick="sortBookList('publisher', 'asc'); return false;"
					<? endif; ?>><?=$eLib["html"]["publisher"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["publisher"]?>
			  	<? endif;?>
			  	</th>
			  	
			  	<th>			  	
			  	<? if ($search_result['column_sorting'] && isset($search_result['language']) && (($search_result['language']=='chi') || ($search_result['language']=='eng'))):?>
			  	 <a <? if ($search_result['sortby']=='hitrate') :?>
				  		href='#' onclick="sortBookList('hitrate', '<?=($search_result['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$search_result['order']?>"
					<? else: ?>
				  		href='#' onclick="sortBookList('hitrate', 'asc'); return false;"
					<? endif; ?>><?=$Lang["libms"]["portal"]["result_list"]["hits_loans"]?></a>
			  	<? else:?>
			  		<?=$Lang["libms"]["portal"]["result_list"]["hits_loans"]?>
			  	<? endif;?>
			  	</th>
			  	
			  	<th>
			  	<? if ($search_result['column_sorting']): ?>
			  	 <a <? if ($search_result['sortby']=='rating') :?>
				  		href='#' onclick="sortBookList('rating', '<?=($search_result['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$search_result['order']?>"
					<? else: ?>
					  	href='#' onclick="sortBookList('rating', 'asc'); return false;"
					<? endif; ?>><?=$eLib["html"]["rating"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["rating"]?>
			  	<? endif;?>
				</th>
					
            </tr>
        </thead>
        
      	<tbody>
<? if ($nav_para['total']==0):?>
        	<tr>
            	<td colspan="7"><?=$eLib["html"]["no_record"]?></td>
            </tr>
<? else:
		foreach ((array)$books_data as $i=>$book):
		$j = $start_record_index + $i + 1;
?>			
        	<tr>
            	<td><?=$j?></td>
            	<td>
            <? if (isset($_SESSION['UserID']) && !empty($_SESSION['UserID'])):?>
				<? if (!empty($book['url']) && !($book['type'] == 'expired')) :?>
				  <a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon-ebooks" href="javascript:<?=$book['url']?>" ></a>
				<? elseif(($book['type'] == 'expired')): ?>
				  <a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon-ebooks"></a>
				<? endif; ?>
			<? endif; ?>
            		&nbsp;</td>
            		
			<? if (!($book['type'] == 'expired')) : ?>
				<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=str_replace('"','&quot;',$book['title'].' - '.$book['author'])?>"><?=$book['title']?></a></td>
				<td><?=($book['author']?$book['author']:'--')?></td>
			    <td><?=($book['publisher']?$book['publisher']:'--')?></td>
			    <td><?=isset($book['hit_rate'])? $book['hit_rate']: $book['loan_count']?></td>
			    <td><?=$book['rating']?></td>
			<? else: ?>
				<td class="off"><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=str_replace('"','&quot;',$book['title'].' - '.$book['author'])?>"><?=$book['title']?></a></td>
				<td class="off"><?=($book['author']?$book['author']:'--')?></td>
			    <td class="off"><?=($book['publisher']?$book['publisher']:'--')?></td>
			    <td class="off"><?=isset($book['hit_rate'])? $book['hit_rate']: $book['loan_count']?></td>
			    <td class="off"><?=$book['rating']?></td>
			<? endif; ?>
            </tr>
<?						
		endforeach;
   endif;
?>      
        </tbody>
      </table>
      </div>