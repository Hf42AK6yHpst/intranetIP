<?
/*
 * 	Log
 * 
 * 	2016-06-20 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

//intranet_auth();
intranet_opendb();

if ($_SESSION['UserID']) {
	$permission = elibrary_plus::getUserPermission($UserID);
	if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
		$libelibplus_ui = new libelibplus_ui();
		echo $libelibplus_ui->geteLibCloseUI();
		die();
	}
}
else {
	$libelibplus = new libelibplus();
	$is_opac_open = $libelibplus->get_portal_setting('open_opac');
	if (!$is_opac_open) {
		intranet_closedb();
		header("location: /home/eLearning/elibplus2/index.php");
		exit;
	}
}

$libelibplus = new libelibplus();
if (isset($_POST['more_type']) && ($_POST['more_type'] != '')) {
	$search_result = $libelibplus->get_more_records_by_group();
}
else {
	$search_result = $libelibplus->get_search_result();
}

$books_data = $search_result['books_data'];
$nav_para = $search_result['nav_para'];
if (count($nav_para)>0) {
	$json['total'] = $nav_para['total'];
}
else {
	$json['total'] = '';
}

if(count($books_data)>1) {
	$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
}
else {
	$navigation_id = '';
}

$libelibplus_ui = new libelibplus_ui();

$json['success'] = false;

## body
ob_start();
if ($search_result['view_type']=='cover') {
	include('book_cover_view.php');
}
else {	
	$start_record_index = $nav_para['record_per_page'] * ($nav_para['current_page']-1);
	include('book_list_view.php');
}
$x = ob_get_contents();
ob_end_clean();

$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;
$json['footer_html'] = '';

## footer
if ($search_result['update_navigation']) {
	$x = $libelibplus_ui->getSearchResultListFooter($nav_para);
	$x = remove_dummy_chars_for_json($x);
	$json['footer_html'] = $x;
}

$json['success'] = true;

echo json_encode($json);

intranet_closedb();
?>