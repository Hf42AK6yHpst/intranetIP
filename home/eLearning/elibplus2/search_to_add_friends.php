<?
/*
 * 	Log
 *
 * 	2017-10-16 [Cameron] update pending friend list after addd friend request
 *  
 * 	2017-09-27 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

if (($_SESSION['UserType'] != USERTYPE_STUDENT) && ($_SESSION['UserType'] != USERTYPE_STAFF)) {
	die();
}
 
$hideTopMenuPart = true;	// hide top menu
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START();
$hideTopMenuPart = false;	// reset back to show

$keyword = urldecode($_GET['AddFriendKeyword']);
$libelibplus = new libelibplus();

$libelibplus_ui = new libelibplus_ui();
$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
if ($_SESSION['UserType'] == '2') {		// student
	$scope = $libelibplus->get_portal_setting('my_friends_scope');
}
else {
	$scope = 2;		// all classes
}

$friend_to_add_ui = $libelibplus_ui->getMyFriendSearchToAddFriend($keyword,$scope);

?>

<!--CONTENT starts-->
<div class="elib-content">
	<div class="container myfriends">
    	<div class="table-header"><?=$Lang["libms"]["portal"]["myfriends"]["search_friends"]?></div>
  		<div class="table-body find-friend">
        	<div class="search-area">
            	<div class="form-group has-feedback">
					<input type="text" class="form-control input-sm" name="AddFriendKeyword" id="AddFriendKeyword" value="<?=$keyword?>" placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>">
              		<i class="glyphicon glyphicon-search form-control-feedback"></i>
                </div>
            </div>
            <ul class="friend-result" id="search_friend_result">
            <?=$friend_to_add_ui?>
            </ul>   
  		</div>  
  	</div>  
</div>
<!--CONTENT ends-->  

<script language="javascript">
$(document).ready(function(){
	$(document).on('keyup', '#AddFriendKeyword', function(e) {
	  	e.preventDefault();
	  	var keyword = $('#AddFriendKeyword').val();
		var key = e.which || e.charCode || e.keyCode;
		if ((key == 13) && (keyword.length > 0)){
			$.ajax({
				dataType: 'json',
				async: true,
				type: 'POST',
				url: 'ajax_my_friends.php',
				data: {	'action': 'search_friends_to_add',
						'AddFriendKeyword': keyword,
						'scope': '<?=$scope?>'
					},
				success: handle_search,
				error: show_ajax_error
			});
	  	}
	});
	
});	

function add_friend(user_id) {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_my_friends.php',
		data : {
				'action': 'addFriendBySearch',
				'StudentID': user_id
			},		  
		success: function(ajaxReturn) {
			if (ajaxReturn != null && ajaxReturn.success){
			  	$("a[data-user_id='"+user_id+"']").parent().remove();
			  	$(".friend-list .table-body ul", parent.document).prepend(ajaxReturn.html);
			  	$("#ClassName", parent.document).val('');
			  	$("#StudentID :not(:first-child)", parent.document).remove();
			}
		},
		error: show_ajax_error
	});
}

function handle_search(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#search_friend_result').html(ajaxReturn.html);
	}
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>