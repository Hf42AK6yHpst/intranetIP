<?
/*
 * Log
 * Note: $_GET value is from BookCategory or Book Details linking, it has higher priority
 * $_POST value is from opac advance search
 *
 * 2019-06-27 [Cameron]
 * - add search filter: introduction [case #N158541]
 * 
 * 2018-05-15 [Cameron]
 * - add sort by BookID in advanced search [case #Y137702]
 *
 * 2017-12-13 [Cameron]
 * - add hidden field book_category_type to search [case #F130026]
 *
 * 2017-05-15 [Cameron]
 * - fix bug: apply intranet_htmlspecialchars to tag so that it can display tag containing <
 *
 * 2017-01-18 [Cameron]
 * - modify the logic to show book_type: don't show it if there's no eBook, don't show physical book if $plugin['eLib_Lite']=true
 *
 * 2016-09-13 [Cameron] create this file
 *
 */
include_once ("config.php");
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/elib_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libelibrary_plus.php");
include_once ($PATH_WRT_ROOT . "includes/elibplus2/libelibplus.php");
include_once ($PATH_WRT_ROOT . "includes/elibplus2/libelibplus_ui.php");
include_once ($PATH_WRT_ROOT . "includes/elibplus2/elibplusConfig.inc.php");

// intranet_auth();
intranet_opendb();

if ($_SESSION['UserID']) {
    $permission = elibrary_plus::getUserPermission($UserID);
    if (! $permission['elibplus_opened'] && ! $plugin['eLib_Lite']) {
        $libelibplus_ui = new libelibplus_ui();
        echo $libelibplus_ui->geteLibCloseUI();
        die();
    }
} else {
    $libelibplus = new libelibplus();
    $is_opac_open = $libelibplus->get_portal_setting('open_opac');
    if (! $is_opac_open) {
        intranet_closedb();
        header("location: /home/eLearning/elibplus2/index.php");
        exit();
    }
}

?>

<!--SEARCH BOX starts-->
<div class="container adv-search">
	<div class="adv-search-title"><?=$Lang["libms"]["portal"]["advanced_search"]?></div>
	<div class="adv-search-box">

		<div class="form-group">
			<label for="input-title" class="col-sm-2 control-label"><?=$eLib["html"]["title"]?></label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="title"
					id="input-title"
					placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>"
					value="<?=$_POST['title']?$_POST['title']:''?>">
			</div>
			<label for="input-subtitle" class="col-sm-2 control-label"><?=$eLib["html"]["subtitle"]?></label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="subtitle"
					id="input-subtitle"
					placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>"
					value="<?=$_POST['subtitle']?$_POST['subtitle']:''?>">
			</div>
		</div>

		<div class="form-group">
			<label for="input-author" class="col-sm-2 control-label"><?=$eLib["html"]["author"]?></label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="author"
					id="input-author"
					placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>"
					value="<?=$_GET['author']?$_GET['author']:($_POST['author']?$_POST['author']:'')?>">
			</div>
			<label for="input-subject" class="col-sm-2 control-label"><?=$eLib["html"]["Subject"]?></label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="subject"
					id="input-subject"
					placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>"
					value="<?=$_POST['subject']?$_POST['subject']:''?>">
			</div>
		</div>

		<div class="form-group">
			<label for="input-publisher" class="col-sm-2 control-label"><?=$eLib["html"]["publisher"]?></label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="publisher"
					id="input-publisher"
					placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>"
					value="<?=$_GET['publisher']?$_GET['publisher']:($_POST['publisher']?$_POST['publisher']:'')?>">
			</div>
			<label for="input-isbn" class="col-sm-2 control-label"><?=$eLib["html"]["ISBN"]?></label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="isbn" id="input-isbn"
					placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>"
					value="<?=$_POST['isbn']?$_POST['isbn']:''?>">
			</div>
		</div>

		<div class="form-group">
			<label for="input-call_number" class="col-sm-2 control-label"><?=$eLib_plus["html"]['call_number']?></label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="call_number"
					id="input-call_number"
					placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>"
					value="<?=$_POST['call_number']?$_POST['call_number']:''?>">
			</div>
			<label for="input-category" class="col-sm-2 control-label"><?=$eLib["html"]["category"]?></label>
			<div class="col-sm-4">
				<select class="form-control" name="lang_and_category"
					id="category-select">
					<option value="">- <?=$eLib_plus["html"]["all"]?> -</option>
<?
$lang_and_cat = $_POST['lang_and_category'] ? $_POST['lang_and_category'] : '';

if (! empty($lang_and_cat)) {
    list ($selectedLang, $selectedCat) = explode(',', $lang_and_cat);
} else {
    if (! empty($_GET['language']) && ! empty($_GET['category'])) { // from browse category
        $selectedLang = $_GET['language'];
        $selectedCat = $_GET['category'];
    } else {
        $selectedLang = '';
        $selectedCat = '';
    }
}

foreach ((array) $book_catalog as $l => $book_language) {
    foreach ((array) $book_language['categories'] as $cat) {
        if ($cat['title']) {
            if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
                $catTitle = explode('-', $cat['title']);
                if (count($catTitle) > 2) {
                    $dispTitle = $catTitle[0] . '-' . $catTitle[1] . trim($catTitle[2]);
                } else {
                    $dispTitle = $cat['title'];
                }
            } else {
                $dispTitle = $cat['title'];
            }
            $selected = (($selectedLang == $l) && ($selectedCat == $cat['title'])) ? 'selected' : '';
            echo '<option value="' . $l . ',' . $cat['title'] . '"' . $selected . '>' . $dispTitle . '</option>';
        } else {
            $dispTitle = '';
        }
    }
}
?>    	
            </select>
			</div>
		</div>

		<div class="form-group">
			<label for="input-language" class="col-sm-2 control-label"><?=$eLib["html"]["language"]?></label>
			<div class="col-sm-4">
				<select class="form-control" name="language" id="language-select">
					<option value="">- <?=$eLib_plus["html"]["all"]?> -</option>
<? $selectLang = $_GET['language'] ? $_GET['language'] : ($_POST['language'] ? $_POST['language'] : ''); ?>
<? foreach ((array)$book_languages as $lang) : ?>
				<option value="<?=$lang['Language']?>"
						<?=($selectLang == $lang['Language'] ? 'selected' : '')?>><?=$lang['Language']?></option>
<? endforeach; ?>
            </select>
			</div>
			<label for="input-tag" class="col-sm-2 control-label"><?=$eLib_plus["html"]["tag"]?></label>
			<div class="col-sm-4">
				<select class="form-control" name="tag_id" id="tag-select">
					<option value="">- <?=$eLib_plus["html"]["all"]?> -</option>
<? $selected_tag_id = $_GET['tag_id']?$_GET['tag_id']:($_POST['tag_id']?$_POST['tag_id']:''); ?>             	
<?

foreach ((array) $book_catalog_tags as $tag) :
    $tag['title'] = intranet_htmlspecialchars($tag['title']);
    ?>
				<option value="<?=$tag['tag_id']?>"
						<?=($selected_tag_id == $tag['tag_id'] ? 'selected' : '')?>><?=$tag['title']?></option>
<? endforeach; ?>
            </select>
			</div>
		</div>

<?
if (! isset($_SESSION['UserID']) || $_SESSION['UserID'] <= 0) { // from opac
    $opac_search_ebook = $libelibplus->get_portal_setting('opac_search_ebook');
} else {
    $opac_search_ebook = true;
}
$isPurchasedeBook = $lelibplus->isPurchasedeBook();

if ($opac_search_ebook && $isPurchasedeBook) :
    ?>		
        <div class="form-group">
			<label for="input-format" class="col-sm-2 control-label"><?=$eLib["html"]["display2"]?></label>
			<div class="col-sm-10">
				<input type="radio" id='book-format-type_all' name="book_type"
					value="type_all"
					<?=($_POST['book_type'] == 'type_all' ? ' checked' : '')?>><label
					for="book-format-type_all" class="control-label"><?=$eLib["html"]["all_books"]?></label>&nbsp;&nbsp;&nbsp;          
<? if (!$plugin['eLib_Lite']):?>
          	<input type="radio" id='book-format-type_physical'
					name="book_type" value="type_physical"
					<?=($_POST['book_type'] == 'type_physical' ? ' checked' : '')?>><label
					for="book-format-type_physical" class="control-label"><?=$eLib_plus["html"]["book"]?></label>&nbsp;&nbsp;&nbsp;
<? endif;?>
          	<input type="radio" id='book-format-type_pc' name="book_type"
					value="type_pc"
					<?=($_POST['book_type'] == 'type_pc' ? ' checked' : '')?>><label
					for="book-format-type_pc" class="control-label"><?=$eLib_plus["html"]["ebook"]?>(<?=$eLib_plus["html"]["pconly"]?>)</label>&nbsp;&nbsp;&nbsp;
				<input type="radio" id='book-format-type_ipad' name="book_type"
					value="type_ipad"
					<?=($_POST['book_type'] == 'type_ipad' ? ' checked' : '')?>><label
					for="book-format-type_ipad" class="control-label"><?=$eLib_plus["html"]["ebook"]?>(<?=$eLib_plus["html"]["pcipad"]?>)</label>
			</div>
		</div>
<? endif;?>

        <div class="form-group">
			<label for="input-order" class="col-sm-2 control-label"><?=$eLib_plus["html"]["order"]?></label>
			<div class="col-sm-4">
				<select class="form-control" name="sortby" id="sortby-select">
					<option value="title"
						<?=($_POST['sortby'] == 'title' ? ' selected' : '')?>><?=$eLib["html"]["title"]?></option>
					<option value="subtitle"
						<?=($_POST['sortby'] == 'subtitle' ? ' selected' : '')?>><?=$eLib["html"]["subtitle"]?></option>
					<option value="author"
						<?=($_POST['sortby'] == 'author' ? ' selected' : '')?>><?=$eLib["html"]["author"]?></option>
					<option value="publisher"
						<?=($_POST['sortby'] == 'publisher' ? ' selected' : '')?>><?=$eLib["html"]["publisher"]?></option>
					<option value="hitrate"
						<?=($_POST['sortby'] == 'hitrate' ? ' selected' : '')?>><?=$eLib_plus["html"]["hitrate"]?></option>
					<option value="rating"
						<?=($_POST['sortby'] == 'rating' ? ' selected' : '')?>><?=$eLib["html"]["rating"]?></option>
					<option value="source"
						<?=($_POST['sortby'] == 'source' ? ' selected' : '')?>><?=$eLib["html"]["source"]?></option>
					<option value="level"
						<?=($_POST['sortby'] == 'level' ? ' selected' : '')?>><?=$eLib["html"]["level"]?></option>
					<option value="bookID"
						<?=($_POST['sortby'] == 'bookID' ? ' selected' : '')?>><?=$Lang["libms"]["portal"]["advancedSearch"]["sortBy"]["bookID"]?></option>
				</select>
			</div>
			
			<label for="input-subject" class="col-sm-2 control-label"><?=$Lang["libms"]["book"]["introduction"]?></label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="introduction"
					id="input-introduction"
					placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>"
					value="<?=$_POST['introduction']?$_POST['introduction']:''?>">
			</div>
			
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn-type1" id="advance-search-submit"><?=$eLib["html"]["search"]?> <span
						class="glyphicon glyphicon-search"></span>
				</button>
			</div>
		</div>

		<input type="hidden" name="form_method" value="post"> <input
			type="hidden" name="search_type" id="search_type"
			value="<?=(isset($_GET['search_type']) ? $_GET['search_type'] : '')?>">
		<input type="hidden" name="update_navigation" id="update_navigation"
			value="1"> <input type="hidden" name="order" id="order" value=""> <input
			type="hidden" name="view_type" id="view_type" value="">
		<!-- cover / list -->
		<input type="hidden" name="category" id="category"
			value="<?=$_GET['category']?$_GET['category']:''?>"> <input
			type="hidden" name="subcategory" id="subcategory"
			value="<?=$_GET['subcategory']?$_GET['subcategory']:''?>"> <input
			type="hidden" name="class_level_id"
			value="<?=$_GET['class_level_id']?>">
<?
$book_category_type_val = '';
if ($_GET['search_type'] == 'category') {
    if ($_GET['language'] == 'others_1') {
        $book_category_type_val = '1';
    } else if ($_GET['language'] == 'others_2') {
        $book_category_type_val = '2';
    }
} else if ($_POST['book_category_type']) {
    $book_category_type_val = $_POST['book_category_type'];
}
?>
		<input type="hidden" name="book_category_type"
			value="<?=$book_category_type_val?>">

	</div>
</div>
<!--SEARCH BOX ends-->
