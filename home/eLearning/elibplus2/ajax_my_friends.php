<?
/*
 * 	Log
 *
 * 	2017-10-16 [Cameron]
 * 		- add remove_friend_request
 *  
 * 	2017-09-29 [Cameron] 
 * 		- apply function getUserListToAddFriend() to case addFriend, changeClass and ignore_friend_request
 * 		- allow staff and student to access this page
 * 		- add case search_my_friends 
 * 	
 * 	2017-09-28 [Cameron] modify case addFriend and changeClass to support retrieving staff
 * 
 * 	2017-09-27 [Cameron] add search_friends_to_add
 * 		
 * 	2017-07-13 [Cameron] apply open_my_friends function checking
 * 
 * 	2017-04-11 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

$libelibplus = new libelibplus();
$open_my_friends = $libelibplus->get_portal_setting('open_my_friends');
if (!$open_my_friends) {
	intranet_closedb();
	die();
}

if (($_SESSION['UserType'] != USERTYPE_STUDENT) && ($_SESSION['UserType'] != USERTYPE_STAFF)) {
	die();
}


$libelibplus_ui = new libelibplus_ui();

$json['success'] = false;
$x = '';
$y = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = true;	// whether to remove new line, carriage return, tab and back slash

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);

switch($action) {

	case "addFriend":
		$ret = $lelibplus->addFriendRequest($_POST['StudentID']);
		if ($ret[0]) {
			$x = $libelibplus_ui->getUserListToAddFriend();
			$notificationID = $ret[1];
			$y = $libelibplus_ui->getMyFriendPendingFriend($notificationID);
	        $json['success'] = true;
		}
		break;
		
	case "changeClass":
		$x = $libelibplus_ui->getUserListToAddFriend();
        $json['success'] = true;
		break;
		
	case "accept_friend_request":
		$ret = $lelibplus->acceptFriendRequest($_POST['NotificationID']);
		if ($ret) {
        	$json['success'] = true;
		}
		break;

	case "ignore_friend_request":
		$ret = $lelibplus->ignoreFriendRequest($_POST['NotificationID']);
		if ($ret) {
			$x = $libelibplus_ui->getUserListToAddFriend();
        	$json['success'] = true;
		}
		break;

	case "remove_notification":
		$ret = $lelibplus->removeNotification($_POST['NotificationID']);
		if ($ret) {
        	$json['success'] = true;
		}
		break;

	case "act-viewers":		// friend's ranking: read ebooks
		if (isset($lelibplus)) {
			unset($lelibplus);
		}
		$ranking_data = array();
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$_POST['DateRange']);
		$ranking_data = $lelibplus->getMyFriendRankingReadingeBook();	// default
		$tab = 'viewers';
		$x = $libelibplus_ui->getMyFriendRankingViewersUI($ranking_data);
		$json['success'] = true;
		break;
		
	case "act-borrowers":	// friend's ranking: borrow books
		if (isset($lelibplus)) {
			unset($lelibplus);
		}
		$ranking_data = array();
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$_POST['DateRange']);
		$ranking_data = $lelibplus->getMostBorrowFriends($limit=5, $classNameWithBracket=true);
		$tab = 'borrowers';
		
		$x = $libelibplus_ui->getMyFriendRankingBorrowersUI($ranking_data);
		$json['success'] = true;
		
		break;		

	case "act-reviewers":	// friend's ranking: review books
		if (isset($lelibplus)) {
			unset($lelibplus);
		}
		$ranking_data = array();
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$_POST['DateRange']);
		$myFriendIDs = $lelibplus->getMyFriendIDs();
		if (count($myFriendIDs)) {
			$cond = " AND b.UserID in ('".implode("','",(array)$myFriendIDs)."')";
			$ranking_data = $lelibplus->getMostActiveUsers($limit=5, $bookType='type_all', $userType='', $classNameWithBracket=true, $cond);
		}
		$tab = 'reviewers';
		
		$x = $libelibplus_ui->getMyFriendRankingReviewersUI($ranking_data);
		$json['success'] = true;
		
		break;		

	case "share_book":
		$data = array();
		$data['book_id'] = $_POST['book_id'];
		$data['selected_friend_ids'] = $_POST['selected_friend_ids'];
		$data['Comments'] = $_POST['Comments'];
		$ret = $lelibplus->shareBook($data);
		if ($ret) {
			$sharedByMe = $lelibplus->getSharedByMeOfABook($_POST['book_id']);
			$x = $libelibplus_ui->getSharedByMeOfABookUI($sharedByMe);
			$disp_total_shared_by_me = sprintf($Lang["libms"]["portal"]["book"]["total_shared_by_me"],count($sharedByMe));
			$json['shared_by_me_tab'] = $disp_total_shared_by_me;
        	$json['success'] = true;
		}
		break;
		
	case "act-history-books-cover":		// friend's borrow books history
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all','accumulated_by_friend');
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
	
		$borrowPara['FriendID'] = $_REQUEST['FriendID'];
		$borrowPara['sortby'] = $_REQUEST['sortby'] ? $_REQUEST['sortby'] : 'a.BorrowTime';
		$borrowPara['order'] = $_REQUEST['order'] ? $_REQUEST['order'] : 'desc';
		$borrowPara['offset'] = $offset;
		$borrowPara['limit'] = $record_per_page;
	
		list($borrowed_total, $borrowed_data) = $lelibplus->getMyFriendBooksBorrowed($borrowPara);
		
		if (count($borrowed_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$borrowPara['offset'] = 0;
			list($borrowed_total, $borrowed_data) = $lelibplus->getMyFriendBooksBorrowed($borrowPara);
		}
		
		$nav_para['show_total'] = false;
		$nav_para['total'] = $borrowed_total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["myfriends"]["book_total"];
		$nav_para['tab_id'] = "act-history-books";
		
		$x = $libelibplus_ui->getMyFriendReadBookCoverUI($borrowed_data, $nav_para);
		$json['success'] = true;
		
		break;

	case "act-history-books-list":		// friend's borrow books history
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all','accumulated_by_friend');
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
	
		$borrowPara['FriendID'] = $_REQUEST['FriendID'];
		$borrowPara['sortby'] = $_REQUEST['sortby'] ? $_REQUEST['sortby'] : 'a.BorrowTime';
		$borrowPara['order'] = $_REQUEST['order'] ? $_REQUEST['order'] : 'desc';
		$borrowPara['offset'] = $offset;
		$borrowPara['limit'] = $record_per_page;
	
		list($borrowed_total, $borrowed_data) = $lelibplus->getMyFriendBooksBorrowed($borrowPara);
		if (count($borrowed_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$borrowPara['offset'] = 0;
			list($borrowed_total, $borrowed_data) = $lelibplus->getMyFriendBooksBorrowed($borrowPara);
		}
		
		$nav_para['show_total'] = false;
		$nav_para['total'] = $borrowed_total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["myfriends"]["book_total"];
		$nav_para['tab_id'] = "act-history-books";
		$nav_para['column_sorting'] = "1";
		$nav_para['sortby'] = $borrowPara['sortby'];
		$nav_para['order'] = $borrowPara['order'];
		
		$x = $libelibplus_ui->getMyFriendReadBookListUI($borrowed_data, $nav_para);
		$json['success'] = true;
		
		break;

	case "act-history-ebooks-cover":		// friend's reading e-books history
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all','accumulated_by_friend');
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
	
		$viewPara['FriendID'] = $_REQUEST['FriendID'];
		$viewPara['sortby'] = $_REQUEST['sortby'] ? $_REQUEST['sortby'] : 'h.DateModified';
		$viewPara['order'] = $_REQUEST['order'] ? $_REQUEST['order'] : 'desc';
		$viewPara['offset'] = $offset;
		$viewPara['limit'] = $record_per_page;
	
		list($viewed_total, $viewed_data) = $lelibplus->getMyFriendeBookViewed($viewPara);
		if (count($viewed_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$viewPara['offset'] = 0;
			list($viewed_total, $viewed_data) = $lelibplus->getMyFriendeBookViewed($viewPara);
		}
			
		$nav_para['show_total'] = false;
		$nav_para['total'] = $viewed_total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["myfriends"]["ebook_total"];
		$nav_para['tab_id'] = "act-history-ebooks"; 
		
		$x = $libelibplus_ui->getMyFriendReadBookCoverUI($viewed_data, $nav_para);
		$json['success'] = true;
		
		break;

	case "act-history-ebooks-list":		// friend's reading e-books history
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all','accumulated_by_friend');
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
	
		$viewPara['FriendID'] = $_REQUEST['FriendID'];
		$viewPara['sortby'] = $_REQUEST['sortby'] ? $_REQUEST['sortby'] : 'h.DateModified';
		$viewPara['order'] = $_REQUEST['order'] ? $_REQUEST['order'] : 'desc';
		$viewPara['offset'] = $offset;
		$viewPara['limit'] = $record_per_page;
	
		list($viewed_total, $viewed_data) = $lelibplus->getMyFriendeBookViewed($viewPara);
		if (count($viewed_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$viewPara['offset'] = 0;
			list($viewed_total, $viewed_data) = $lelibplus->getMyFriendeBookViewed($viewPara);
		}
			
		$nav_para['show_total'] = false;
		$nav_para['total'] = $viewed_total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["myfriends"]["ebook_total"];
		$nav_para['tab_id'] = "act-history-ebooks"; 
		$nav_para['column_sorting'] = "1";
		$nav_para['sortby'] = $viewPara['sortby'];
		$nav_para['order'] = $viewPara['order'];
		
		$x = $libelibplus_ui->getMyFriendReadBookListUI($viewed_data, $nav_para);
		$json['success'] = true;
		
		break;

	case "act-history-reviews-cover":		// friend's book review history
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all','accumulated_by_friend');
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
	
		list($viewed_total, $reviews_data)=$lelibplus->getUserReviews($_REQUEST['FriendID'], $class_name=false, $offset, $record_per_page);

		if (count($reviews_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;
			list($viewed_total, $reviews_data)=$lelibplus->getUserReviews($_REQUEST['FriendID'], $class_name=false, $offset, $record_per_page);
		}
						
		$nav_para['show_total'] = false;
		$nav_para['total'] = $viewed_total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['tab_id'] = "act-history-reviews"; 
		
		$x = $libelibplus_ui->getMyFriendReviewsUI($reviews_data, $nav_para);
		$json['success'] = true;
		
		break;
		
	case "act-history-sharing-cover":		// book sharing history with friend
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$offset = $record_per_page * ($page_no-1);
	
		$para['FriendID'] = $_REQUEST['FriendID'];
		$para['sortby'] = $_REQUEST['sortby'] ? $_REQUEST['sortby'] : 'a.DateModified';
		$para['order'] = $_REQUEST['order'] ? $_REQUEST['order'] : 'desc';
		$para['offset'] = $offset;
		$para['limit'] = $record_per_page;
	
		list($shared_total, $shared_data) = $lelibplus->getSharingBooks($para);
		if (count($shared_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$para['offset'] = 0;
			list($shared_total, $shared_data) = $lelibplus->getSharingBooks($para);			
		}
		
		$nav_para['show_total'] = false;
		$nav_para['total'] = $shared_total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['tab_id'] = "act-history-sharing";
		
		list($nav_para['friend_name'],$friend_class,$friend_image) = $lelibplus->getUserInfo($_REQUEST['FriendID'],$classNameWithBracket=false);
		
		$x = $libelibplus_ui->getMyFriendShareBookUI($shared_data, $nav_para);
		$json['success'] = true;
	
		break;
		
	case "unfriend":
		$ret = $lelibplus->removeFriend($_POST['FriendID']);
		if ($ret) {
        	$json['success'] = true;
		}
		break;

	case "view-review":
		$x = $libelibplus_ui->getReviewListUI($_POST['book_id']);
		$json['success'] = true;
		break;
		
	case "shared-to-me":
		$sharedToMe = $lelibplus->getSharedToMeOfABook($_POST['book_id']);
		$x = $libelibplus_ui->getSharedToMeOfABookUI($sharedToMe);
		$json['success'] = true;
		break;				

	case "shared-by-me":
		$sharedByMe = $lelibplus->getSharedByMeOfABook($_POST['book_id']);
		$x = $libelibplus_ui->getSharedByMeOfABookUI($sharedByMe);
		$json['success'] = true;
		break;				

	case "remove_shared_to_me":
	
		$ret = $lelibplus->removeSharedToMe($_POST['ShareID']);
		if ($ret) {
			$sharedToMe = $lelibplus->getSharedToMeOfABook($_POST['BookID']);
			$x = $libelibplus_ui->getSharedToMeOfABookUI($sharedToMe);
			if ($_POST['update_tab_total']) {
				$disp_total_shared_to_me = sprintf($Lang["libms"]["portal"]["book"]["total_shared_to_me"],count($sharedToMe));
				$json['shared_to_me_tab'] = $disp_total_shared_to_me;
			}
			$json['success'] = true;
		}		
		break;				

	case "search_friends_to_add":
		$x = $libelibplus_ui->getMyFriendSearchToAddFriend($_POST['AddFriendKeyword'],$_POST['scope']);
		$json['success'] = true;
		break;
		
	case "addFriendBySearch":
		$ret = $lelibplus->addFriendRequest($_POST['StudentID']);
		if ($ret[0]) {
			$notificationID = $ret[1];
			$x = $libelibplus_ui->getMyFriendPendingFriend($notificationID);
	        $json['success'] = true;
		}
		break;

	case "search_my_friends":
		$x = $libelibplus_ui->getMyFriendSearchFriendList($_POST['FriendKeyword'],$_POST['scope']);
		$json['success'] = true;
		break;

	case "remove_friend_request":
		$ret = $lelibplus->removeNotification($_POST['NotificationID']);
		if ($ret) {
        	$json['success'] = true;
		}
		break;
								
}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
	$y = remove_dummy_chars_for_json($y);
}
$json['html'] = $x;
$json['html2'] = $y;

echo json_encode($json);

intranet_closedb();
?>