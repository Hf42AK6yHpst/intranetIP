<?php
/*
 * 	using:
 * 
 * 	Log
 *
 *  Date:   2019-07-04 [Cameron]
 *          add column NumOfChiBookRead and NumOfEngBookRead for all classes [case #A163164]
 *           
 *	Date:	2016-11-21 [Cameron]
 *			fix bug: pass empty as date range when initialized elibrary_plus    
 *
 * 	Date:	2016-08-05 [Cameron]
 * 			copy from elibplus and modify it
 * 
 * 	Note: 	1. max number of records to export is 9999
 * 			2. only user who has eAdmin-LibraryMgmtSystem right can access this function
 * 
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
$permission = elibrary_plus::getUserPermission($UserID);

if(!$permission['elibplus_opened'] && !$plugin['eLib_Lite']){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], 'type_all', '');

# Define Column Title
$exportColumn = array();

# get filter condition and prepare sql [start]
	$offset = $offset? $offset: 0;
	$amount = 9999;
	$FromDate = null;
	$ToDate = null;
	
	$ClassName 		= $_REQUEST['ClassName'];
	$FromMonth 		= $_REQUEST['FromMonth'] ? intval($_REQUEST['FromMonth']) : intval(date('m'));
	$ToMonth 		= $_REQUEST['ToMonth'] ? intval($_REQUEST['ToMonth']) : intval(date('m'));
	$FromYear 		= $_REQUEST['FromYear'] ? intval($_REQUEST['FromYear']) : intval(date('Y'));
	$ToYear 		= $_REQUEST['ToYear'] ? intval($_REQUEST['ToYear']) : intval(date('Y'));
    $student_id 	= $_REQUEST['student_id'];
    $statistics_type= $_REQUEST['statistics_type'];

# get filter condition and prepare sql [end]

## case 1: all classes
if (empty($ClassName)) {
	$filename = "ebook_stats_by_class.csv";

	$order_sql = " class_name ";
	$ebook_data['reading']=$lelibplus->getPublicClassSummary($offset, $amount, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);					
	
	$exportColumn[0][] = $eLib["html"]["class"];
	$exportColumn[0][] = $eLib["html"]["num_of_students"];
	$exportColumn[0][] = $eLib["html"]['Total']."(".$eLib["html"]["num_of_book_hit"].")";
	$exportColumn[0][] = $Lang["libms"]["portal"]["statistics"]["totalHitsChi"];
	$exportColumn[0][] = $Lang["libms"]["portal"]["statistics"]["totalHitsEng"];
	$exportColumn[0][] = $eLib["html"]["Average"]."(".$eLib["html"]["num_of_book_hit"].")";
	$exportColumn[0][] = $eLib["html"]['Total']."(".$eLib["html"]["num_of_review"].")";
	$exportColumn[0][] = $eLib["html"]["Average"]."(".$eLib["html"]["num_of_review"].")";
	
	for ($i=0, $iMax=sizeof($ebook_data['reading']); $i<$iMax; $i++)
	{
		$dataObj = $ebook_data['reading'][$i];
		$ExportArr[] = array(strip_tags($dataObj["class_link"]), $dataObj["num_student"], $dataObj["NumBookRead"], $dataObj["NumChiBookRead"], $dataObj["NumEngBookRead"], $dataObj["read_average"], $dataObj["NumBookReview"], $dataObj["review_average"]);
	}
}
else {
	## case 2: specific class
	if (empty($student_id)) {
		$filename = "ebook_stats_by_student.csv";
		
		$order_sql = " class_number ";
		$ebook_data['reading']=$lelibplus->getPublicStudentSummary($offset, $amount,$ClassName, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);
		
		$exportColumn[0][] = $eLib["html"]["student_name"];
		$exportColumn[0][] = $eLib["html"]["class"];
		$exportColumn[0][] = $eLib["html"]["class_number"];
		$exportColumn[0][] = $eLib["html"]["num_of_book_read"];
		$exportColumn[0][] = $eLib["html"]["num_of_book_hit"];
		$exportColumn[0][] = $eLib["html"]["num_of_review"];

		for ($i=0, $iMax=sizeof($ebook_data['reading']); $i<$iMax; $i++)
		{
			$dataObj = $ebook_data['reading'][$i];
			$ExportArr[] = array(strip_tags($dataObj["user_link"]), $dataObj["class_name"], $dataObj["class_number"], strip_tags($dataObj["NumBooks"]), strip_tags($dataObj["NumBookRead"]), strip_tags($dataObj["NumBookReview"]));
		}
	}
	else {
		## case 3: hits / progress by student
		if ($statistics_type == 'hits') { 
			$filename = "ebook_stats_of_a_student.csv";
			
			$order_sql = " title ";
			$ebook_data['reading']=$lelibplus->getPublicStudentParticularSummary($offset, $amount,$student_id,$ClassName, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);	
			
			$exportColumn[0][] = $eLib["html"]["book_title"];
			$exportColumn[0][] = $eLib["html"]["author"];
			$exportColumn[0][] = $eLib["html"]["Progress"];
			$exportColumn[0][] = $eLib["html"]["ReadTimes"];
			$exportColumn[0][] = $eLib["html"]["last_read"];

			for ($i=0, $iMax=sizeof($ebook_data['reading']); $i<$iMax; $i++)
			{
				$dataObj = $ebook_data['reading'][$i];
				$ExportArr[] = array(strip_tags($dataObj["title"]), $dataObj["author"], $dataObj["percentage"], strip_tags($dataObj["read_times"]), $dataObj["last_accessed"]);
			}
		}
		## case 4: reviews by student
		else if ($statistics_type == 'reviews') {
			$filename = "ebook_stats_of_a_student_reviews.csv";
				
			$order_sql = "b.Title, u.EnglishName";
			
			list($total, $ebook_data['reading'])=$lelibplus->getUserReviews2($student_id, $ClassName, $offset, $amount,$FromMonth, $ToMonth,$FromYear,$ToYear);

			$exportColumn[0][] = $eLib["html"]["book_title"];
			$exportColumn[0][] = $eLib["html"]["author"];
			$exportColumn[0][] = $eLib_plus["html"]["export"]["Rating"];
			$exportColumn[0][] = $eLib_plus["html"]["export"]["Content"];
			$exportColumn[0][] = $eLib_plus["html"]["export"]["DateModified"];
			$exportColumn[0][] = $eLib_plus["html"]["likes"];
			
			for ($i=0,$iMax=count($ebook_data['reading']); $i<$iMax; $i++)
			{
				$dataObj = $ebook_data['reading'][$i];
				$ExportArr[] = array($dataObj["title"], $dataObj["author"], $dataObj["rating"], $dataObj["content"], $dataObj["date"], $dataObj["likes"]);
			}
			
		}
		else {
			// error
		}
	}
}

unset($ebook_data['reading']);


$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");

?>