<?
/*
 * 	Log
 * 
 * 	2017-10-16 [Cameron]
 * 		- add remove friend request function
 * 
 * 	2017-09-29 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

if (($_SESSION['UserType'] != USERTYPE_STUDENT) && ($_SESSION['UserType'] != USERTYPE_STAFF)) {
	die();
}

 
$hideTopMenuPart = true;	// hide top menu
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START();
$hideTopMenuPart = false;	// reset back to show

$keyword = urldecode($_GET['FriendKeyword']);

$libelibplus = new libelibplus();

$libelibplus_ui = new libelibplus_ui();
$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
if ($_SESSION['UserType'] == '2') {		// student
	$scope = $libelibplus->get_portal_setting('my_friends_scope');
}
else {
	$scope = 2;		// all classes
}

$friend_list_ui = $libelibplus_ui->getMyFriendSearchFriendList($keyword,$scope);

?>

<!--CONTENT starts-->
<div class="elib-content">
	<div class="container myfriends">
    	<div class="table-header"><?=$Lang["libms"]["portal"]["myfriends"]["search_friends"]?></div>
  		<div class="table-body find-friend">
        	<div class="search-area">
            	<div class="form-group has-feedback">
					<input type="text" class="form-control input-sm" name="FriendKeyword" id="FriendKeyword" value="<?=$keyword?>" placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>">
              		<i class="glyphicon glyphicon-search form-control-feedback"></i>
                </div>
            </div>
            <ul class="friend-result" id="search_friend_result">
            <?=$friend_list_ui?>
            </ul>   
  		</div>  
  	</div>  
</div>
<!--CONTENT ends-->  

<script language="javascript">
$(document).ready(function(){
	$(document).on('keyup', '#FriendKeyword', function(e) {
	  	e.preventDefault();
	  	var keyword = $('#FriendKeyword').val();
		var key = e.which || e.charCode || e.keyCode;
		if ((key == 13) && (keyword.length > 0)){
			$.ajax({
				dataType: 'json',
				async: true,
				type: 'POST',
				url: 'ajax_my_friends.php',
				data: {	'action': 'search_my_friends',
						'FriendKeyword': keyword,
						'scope': '<?=$scope?>'
					},
				success: handle_search,
				error: show_ajax_error
			});
	  	}
	});
	
	$('.btn-remove-request').on('click', function(e) {
	  	e.preventDefault();
    	if (confirm('<?=$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_remove_friend_request"] ?>')){
    		var NotificationID = $(this).attr('data-notification_id');
			$.ajax({
				dataType: 'json',
				async: true,
				type: 'POST',
				url: 'ajax_my_friends.php',
				data: {	
						'action': 'remove_friend_request',
						'NotificationID': NotificationID
					},
				success: function(ajaxReturn) {
					if (ajaxReturn != null && ajaxReturn.success){
						$("a[data-notification_id='"+NotificationID+"']").parent().remove();
						$("a[data-notification_id='"+NotificationID+"']", parent.document).parent().parent().remove();
					}
				},
				error: show_ajax_error
			});
    	}        
	});
	
});	


function handle_search(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#search_friend_result').html(ajaxReturn.html);
	}
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>