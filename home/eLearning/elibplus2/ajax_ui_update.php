<?
/*
 * 	Log
 * 
 *  2018-09-27 [Cameron] 
 *      - add case updateReview
 *      - apply IntegerSafe to book_id
 * 
 *  2018-07-16 [Cameron] cast $book_id and $review_id to integer to avoid sql injection [case #G142774]
 *   
 * 	2017-04-28 [Cameron]
 * 		- update total in book review tab when add / delete review
 * 
 * 	2017-02-09 [Cameron]
 * 		- fix bug to support php5.4: should call stripslashes() because lib.php already add slashes to variables, but it call pack_value()
 * 			when addReview and submitRecommend
 * 
 * 	2016-11-11 [Cameron]
 * 		- add case deleteRecommend
 * 
 * 	2016-09-27 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

$libelibplus_ui = new libelibplus_ui();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = true;	// whether to remove new line, carriage return, tab and back slash

switch($action) {
		
	case 'submitReview':
	    $book_id = IntegerSafe($_POST['book_id']);
		$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
		if (!get_magic_quotes_gpc()) {
			$content = stripslashes($content);	
		}
		$result = $lelibplus->addReview($content, IntegerSafe($_POST['rate']));
		if ($result) {
			$json['success'] = true;
			$x = $libelibplus_ui->getReviewItemUI($book_id,0,1,$result);
			$x = $x['html'];
			$y = $libelibplus_ui->getBookStatUI($book_id);
			$y = remove_dummy_chars_for_json($y);
			$json['book_stat_html'] = $y;
			
			$total_reviews = $lelibplus->getTotalBookReviews();
			$disp_total_reviews = sprintf($Lang["libms"]["portal"]["book"]["total_reviews"],$total_reviews);
			$json['view_review_tab'] = $disp_total_reviews;
		}
		break;

	case 'updateReview':
	    $book_id = IntegerSafe($_POST['book_id']);
	    $reviewID = IntegerSafe($_POST['ReviewID']);
	    $rating = IntegerSafe($_POST['Rating']);
	    $content = $_POST['Content'];
	    $lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
	    if (!get_magic_quotes_gpc()) {
	        $content = stripslashes($content);
	    }
	    
	    $para = array();
	    $para['ReviewID'] = $reviewID;
	    $para['Rating'] = $rating;
	    $para['Content'] = $content;
	    $result = $lelibplus->updateBookReview($para);
	    if ($result) {
	        $json['success'] = true;
	        $x = $libelibplus_ui->getBookReviewContentUI($reviewID);
	        $y = $libelibplus_ui->getBookStatUI($book_id);
	        $y = remove_dummy_chars_for_json($y);
	        $json['book_stat_html'] = $y;
	    }
	    break;
	    
	case 'removeAllReview':
		$book_id = (int) $_GET['book_id'];
		$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
		$result = $lelibplus->removeAllReviews($book_id);
		if ($result) {
			$json['success'] = true;
			$x = $libelibplus_ui->getReviewListUI($book_id);
			$y = $libelibplus_ui->getBookStatUI($book_id);
			$y = remove_dummy_chars_for_json($y);
			$json['book_stat_html'] = $y;

			$total_reviews = $lelibplus->getTotalBookReviews();
			$disp_total_reviews = sprintf($Lang["libms"]["portal"]["book"]["total_reviews"],$total_reviews);
			$json['view_review_tab'] = $disp_total_reviews;
		}
		break;

	case 'removeReview':
		$review_id = (int) $_GET['review_id'];
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$review = $lelibplus->getReviewByReviewID($review_id);
		
		if (count($review) > 0) {
			$book_id = $review[0]['BookID'];
			$result = $lelibplus->removeReviews($review_id);
			
			if ($result) {
				$json['success'] = true;
				$json['review_id'] = $review_id;
				$y = $libelibplus_ui->getBookStatUI($book_id);
				$y = remove_dummy_chars_for_json($y);
				$json['book_stat_html'] = $y;
				
				$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
				$total_reviews = $lelibplus->getTotalBookReviews();
				$disp_total_reviews = sprintf($Lang["libms"]["portal"]["book"]["total_reviews"],$total_reviews);
				$json['view_review_tab'] = $disp_total_reviews;
			}
		}
		break;
	    
	case 'submitRecommend':
	    $book_id = IntegerSafe($_POST['book_id']);
		if (!get_magic_quotes_gpc()) {
			$content = stripslashes($content);	
		}
		$class_level_ids = $_POST['class_level_ids'];
		$mode = $_POST['mode'];
		
		$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
        if ($permission['admin']){
        	$result = $lelibplus->addOrEditRecommend($class_level_ids, $content, $mode);
        }
        else {
        	$result = false;
        }
		if ($result) {
			$json['success'] = true;
			$x = $libelibplus_ui->getRecommendedInfo($book_id);
		}
		break;

	case 'deleteRecommend':
	    $book_id = IntegerSafe($_POST['book_id']);
		
		$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
        if ($permission['admin']){
        	$result = $lelibplus->removeRecommendByBookID($book_id);
        }
        else {
        	$result = false;
        }
		if ($result) {
			$json['success'] = true;
			$x = $libelibplus_ui->getRecommendedInfo($book_id);
		}
		break;
			
}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}
$json['html'] = $x;

echo json_encode($json);

intranet_closedb();
?>