<?php
/*
 * 	Log
 * 
 * 	2016-06-02 [Cameron] create this file
 * 	
 */
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['admin']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	intranet_closedb();
	$xmsg = ($IsNew) ? 'AddUnsuccess' : 'UpdateUnsuccess';
	header("Location: news_edit.php?xmsg=".$xmsg);	
	exit;
}

$fs = new libfilesystem();
$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);

$id = $_POST['id'];    
$type = $_POST['type'];
$file_temp_path = $_POST['file_temp_path'];
$delete_attachment = $_POST['delete_attachment'];

$attachment = null;
$attachment_url = null;

if ($id){
    $notice = current($lelibplus->getPortalNotices($type, $id, 0, 1));
    $attachment = $notice['attachment'];
    $action = 'update';
}
else {
	$action = 'add';
}

if ($file_temp_path){
    $file_temp_path 	= urldecode($file_temp_path);
    if (!$delete_attachment){
		$file_name 		= $fs->get_file_basename($file_temp_path);
		$current_time 	= time();
		$attachment_dir 	= $intranet_root.'/file/elibplus2/attachments/'.$current_time.'/';
		$attachment_url	= '/file/elibplus2/attachments/'.$current_time.'/'.$file_name;

		$fs->createFolder($attachment_dir);
		$fs->file_copy($intranet_root.$file_temp_path, $attachment_dir);
    }
    $fs->file_remove($file_temp_path);//force remove attachment
}

if ($attachment && ($file_temp_path || $delete_attachment)){
	$fs->file_remove($intranet_root.$attachment);
	$fs->folder_remove($intranet_root.dirname($attachment));
	$old_attachment_exists = true;
	
	if($delete_attachment){//force remove attachment
		$no_of_removed_attachments = $lelibplus->removePortalNoticeAttachments($id);
	}
}

if (!get_magic_quotes_gpc()) {
	$dataArr['title'] = $lelibplus->Get_Safe_Sql_Query(standardizeFormPostValue($_POST['title']));
	$dataArr['content'] = $lelibplus->Get_Safe_Sql_Query(standardizeFormPostValue($_POST['content']));
}
else {
	$dataArr['title'] = $lelibplus->Get_Safe_Sql_Query($_POST['title']);
	$dataArr['content'] = $lelibplus->Get_Safe_Sql_Query($_POST['content']);
}
$dataArr['attachment'] = $attachment_url;
$dataArr['priority'] = standardizeFormPostValue($_POST['priority']);
$dataArr['date_start'] = standardizeFormPostValue($_POST['date_start']);
$dataArr['date_end'] = standardizeFormPostValue($_POST['date_end']);
  
$id = $lelibplus->setPortalNotice($type, $id, $dataArr, $old_attachment_exists);

if($id){
	if($action == 'add'){
		$returnMsgKey = 'AddSuccess';
	}
	else{
		$returnMsgKey = 'UpdateSuccess';
	}
	$recordID = $dataArr['RecordID'];
//	header("location: notice_edit.php?notice_id=".$id."&type=".$type."&msg=".$returnMsgKey);
	header("location: index.php?notice_type=".$type."&msg=".$returnMsgKey."&#".$type);
}
else{
	if($action == 'add'){
		$returnMsgKey = 'AddUnsuccess';
	}
	else{
		$returnMsgKey = 'UpdateUnsuccess';
	}
//	header("location: notice_edit.php?type=".$type."&msg=".$returnMsgKey);
	header("location: index.php?notice_type=".$type."&msg=".$returnMsgKey."&#".$type);	
}

intranet_closedb(); 
?>