<?
/* using: 
 * 
 * 	Log
 * 
 * 	2016-05-18 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);


if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}
    

session_register_intranet('elibplus2_menu_tab','');

$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START();

$libelibplus = new libelibplus();
$settings = $libelibplus->get_bulk_portal_setting('all_book_shelf');
$settings = BuildMultiKeyAssoc($settings, 'name', array('value'),1);	// get settings in key=>value pair
$ebook_recommend_book = $settings['ebook_recommend_book'];
$ebook_most_hit = $settings['ebook_most_hit'];
$ebook_new = $settings['ebook_new'];
$ebook_best_rated = $settings['ebook_best_rated'];
$pbook_recommend_book = $settings['pbook_recommend_book'];
$pbook_most_loan = $settings['pbook_most_loan'];
$pbook_new = $settings['pbook_new'];
$pbook_best_rated = $settings['pbook_best_rated'];

$libelibplus_ui = new libelibplus_ui();

?>
<!--CONTENT starts-->
<div class="elib-index">
<? if ( $ebook_recommend_book || $ebook_most_hit || $ebook_new || $ebook_best_rated ||
		$pbook_recommend_book || $pbook_most_loan || $pbook_new || $pbook_best_rated ) :?>  
  <!--SHELVES starts-->
  <div class="elib-shelves"> 
<? if ( $ebook_recommend_book || $ebook_most_hit || $ebook_new || $ebook_best_rated ) :?>  
    <!--EBOOK SHELF starts-->
    <div class="elib-shelf container-fluid"> 
<?
	## show bBook shelf according to setting
	if ($ebook_recommend_book)
		echo $libelibplus_ui->geteBookCarousel('recommend');
	else if ($ebook_most_hit)
		echo $libelibplus_ui->geteBookCarousel('most_hit');
	else if ($ebook_new)
		echo $libelibplus_ui->geteBookCarousel('new');
	else if ($ebook_best_rated)
		echo $libelibplus_ui->geteBookCarousel('best_rated');
		
	echo $libelibplus_ui->geteBookRankingUI();	
?>
	
    </div>
    <!--EBOOK SHELF ends-->
<? endif;?>
    
<? if ($pbook_recommend_book || $pbook_most_loan || $pbook_new || $pbook_best_rated ) :?>    
    <!--PHYSICAL BOOK SHELF starts-->
    <div class="elib-shelf container-fluid">
<?
	## show physical book shelf according to setting
	if ($pbook_recommend_book)
		echo $libelibplus_ui->getpBookCarousel('recommend');
	else if ($pbook_most_loan)
		echo $libelibplus_ui->getpBookCarousel('most_loan');
	else if ($pbook_new)
		echo $libelibplus_ui->getpBookCarousel('new');
	else if ($pbook_best_rated)
		echo $libelibplus_ui->getpBookCarousel('best_rated');
		
	echo $libelibplus_ui->getpBookRankingUI();		
?>
    </div>
    <!--PHYSICAL BOOK SHELF ends--> 
<? endif;?>
     
  </div>
  <!--SHELVES ends-->
<? endif;?> 

<?=$libelibplus_ui->getPortalBottomPart()?>
 
</div>
<!--CONTENT ends-->


<script language="javascript">
$(document).ready(function(){

	// bookshelf filter button group
	$('.shelf-filter.btn-group .btn').on('click', function(e) {
	  	e.preventDefault();
	  	$(this)
	    	.addClass('active')
	    	.tab('show')
	    	.siblings('.btn')
	      		.removeClass('active');
	  	var type = $(this).attr('id');
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_book_shelf.php',
			data : 'type='+type,		  
			success: (type.indexOf('ebook_')>-1) ? load_ebook_shelf : load_pbook_shelf,
			error: show_ajax_error
		});
	});
	
	$('#ebook_ranking_type, #ebook_ranking_range, #pbook_ranking_type, #pbook_ranking_range').on('change', function(e) {
		e.preventDefault();
	  	var id = $(this).attr('id');
	  	if (id.indexOf('ebook_')>-1) {
	  		var type = 'ebook';
	  		var ranking_type = $('#ebook_ranking_type').val();	
	  		var ranking_range = $('#ebook_ranking_range').val();
	  	}
	  	else {
	  		var type = 'physical';
	  		var ranking_type = $('#pbook_ranking_type').val();	
	  		var ranking_range = $('#pbook_ranking_range').val();
	  	}
	  	
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_ranking.php',
			data : {
				'type':type,
				'ranking_type':ranking_type,
				'ranking_range':ranking_range	
			},		  
			success: (id.indexOf('ebook_')>-1) ? load_ebook_ranking : load_pbook_ranking,
			error: show_ajax_error
		});
		
	});

	$(document).on('click','.news-title, .btn-expand, .news-content span[id^="brief_news_content_"]:not(:has(a))', function(e){
		e.preventDefault();
		var i = $(this).attr('id').substr($(this).attr('id').lastIndexOf('_')+1);
		expand_news(i);	
	});
	
	$(document).on('click','#more_news', function(e){	
	  	e.preventDefault();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_notice.php',
			data : {
				'action':'getMoreNoticeItem',
				'offset':$('#more_news').attr('data-news-offset')
			},		  
			success: load_more_news,
			error: show_ajax_error
		});
	});
	
	$('#announcement_tab a').on('click', function(e) {
	  	e.preventDefault();
	  	var type = $(this).attr('href');
	  	type = type.replace('#','');
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_notice.php',
			data : 'action='+type,		  
			success: load_announcement,
			error: show_ajax_error
		});
	});

	$(document).on('click','#op-hr .btn-prev:not(.dim)', function(e){
		e.preventDefault();
	  	var ref_date = $('#date_0').attr('data-date');
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_notice.php',
			data : {
				'action': 'getPrevWeekCalendar',
				'ref_date': ref_date
			},		  
			success: load_calendar,
			error: show_ajax_error
		});
	});

	$(document).on('click','#op-hr .btn-next', function(e){
		e.preventDefault();
	  	var ref_date = $('#date_0').attr('data-date');
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_notice.php',
			data : {
				'action': 'getNextWeekCalendar',
				'ref_date': ref_date
			},		  
			success: load_calendar,
			error: show_ajax_error
		});
	});
	
	$(document).on('click', '#ebook_ranking', function(e){    	
    	e.preventDefault();    	
    	$('#ebook_ranking_form').submit();
        return false;
    });

	$(document).on('click', '#pbook_ranking', function(e){    	
    	e.preventDefault();    	
    	$('#pbook_ranking_form').submit();
        return false;
    });

	$(document).on('click', '#ebook_more', function(e){    	
    	e.preventDefault();
    	var btn_click = $('#ebook_btn_group .active').attr('id');
    	if (btn_click == 'ebook_most_hit') {
    		self.location = "ranking.php?more_type=" + btn_click;
    	}
    	else if (btn_click == 'ebook_best_rated') {
    		self.location = "ranking.php?more_type=" + btn_click;
    	}
    	else {
    		self.location = "more_books.php?more_type=" + btn_click;
    	}    	
        return false;
    });

	$(document).on('click', '#pbook_more', function(e){    	
    	e.preventDefault();
    	var btn_click = $('#pbook_btn_group .active').attr('id');
    	if (btn_click == 'pbook_most_loan') {
    		self.location = "ranking.php?more_type=" + btn_click;
    	}
    	else if (btn_click == 'pbook_best_rated') {
    		self.location = "ranking.php?more_type=" + btn_click;
    	}
    	else {
	    	self.location = "more_books.php?more_type=" + btn_click;
    	}    	
        return false;
    });
	
	$('.fancybox_magazine').fancybox( {
	 'fitToView'    	 : false,	
	 'autoDimensions'    : false,
	 'autoScale'         : false,
	 'autoSize'          : false,
     'width'             : 550,
     'height'            : 320,
	 'padding'			 : 0,
     'transitionIn'      : 'elastic',
     'transitionOut'     : 'elastic',
     'type'              : 'iframe'    }
	);

	$('.fancybox_magazine_learnthenews').fancybox( {
	'fitToView'    	 : false,	
	 'autoDimensions'    : false,
	 'autoScale'         : false,
	 'autoSize'          : false,
     'width'             : 820,
     'height'            : 500,
	 'padding'			 : 0,
     'transitionIn'      : 'elastic',
     'transitionOut'     : 'elastic',
     'type'              : 'iframe'    }
	);


	$('.others-epost .fancybox').on('click',function(){
		$.fancybox.showLoading();
	});


<? if($special_feature['emagUserAccountLimit'][$UserID]){ // 20141105-emag-provider?>
	setTimeout(function(){
		redirect_to_emag_site();
		$(".fancybox-overlay").append("<div id='fancybox-block' style='width: 100%;height: 100%;'></div>");
		$(".fancybox-close").toggleClass("fancybox-close");
		$(document).bind('DOMNodeInserted', function(e) {
		    var element = e.target;
		    if($(element).attr("class") == "fancybox-item fancybox-close"){
		    	$(element).css("display","none");
		    }
		});
	},1000);
<?}?>

<?=$libelibplus_ui->getBookDetailFancybox()?>

});	// end $(document).ready()

function load_ebook_shelf(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#carousel-ebooks').html(ajaxReturn.html);
	}
}	

function load_pbook_shelf(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#carousel-pbooks').html(ajaxReturn.html);
	}
}	

function load_ebook_ranking(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#ebook_ranking_table').html(ajaxReturn.html);
	}
}	

function load_pbook_ranking(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#pbook_ranking_table').html(ajaxReturn.html);
	}
}	

function load_more_news(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#news ul').append(ajaxReturn.html);
	  	$('#more_news').attr('data-news-offset',ajaxReturn.offset);	// update new offset
	  	if (!ajaxReturn.show_more) {
	  		$('#more_news').hide();
	  	}
	}
}	

function load_announcement(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#announcement_content').html(ajaxReturn.html);
	}
}	

function load_calendar(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#calendar_items').html(ajaxReturn.html);
	}
}	

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

function redirect_to_emag_site (){
	$("#redirect_to_emag_site").click();
}

function expand_news(news_no) {
	var obj = $('#news_content_'+news_no).parent();
	$('#brief_news_content_'+news_no).fadeOut('fast');
	$('#full_news_content_'+news_no).css('white-space','normal');		// disable nowrap
	$('#news_title_'+news_no+' a').removeAttr('href');
	$('#full_news_content_'+news_no).slideDown('slow');
	obj.children('.btn-expand').hide();
}

<? if ($_GET['msg']): ?>
Get_Return_Message("<?=$Lang['General']['ReturnMessage'][$_GET['msg']]?>");
<? endif;?>

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>