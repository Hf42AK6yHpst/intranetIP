<?php
// editing by CharlesMa

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");	
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();
global $intranet_session_language, $intranet_root, $UserID, $US_Intranet_IDType, $config_school_code, $eclass40_httppath;

function encrypt($str){
	$Encrypt1 = base64_encode($str);
	$MidPos = ceil(strlen($Encrypt1)/2);
	$Encrypta = substr($Encrypt1, 0, $MidPos);
	$Encryptb = substr($Encrypt1, $MidPos);

	return base64_encode($Encryptb.$Encrypta."=".(strlen($Encrypt1)-$MidPos));	
}

function decrypt($str){
	$Encrypt1 = base64_decode($str);
	$SplitSizePos = strrpos($Encrypt1, "=");
	$MidPos = (int) substr($Encrypt1, $SplitSizePos+1);
	$Encrypt1 = substr($Encrypt1, 0, $SplitSizePos);
	$Encrypta = substr($Encrypt1, 0, $MidPos);
	$Decryptb = substr($Encrypt1, $MidPos);

	return base64_decode($Decryptb.$Encrypta);
}

$lib_user = new libuser($UserID);

$TimeStamp = date("Y-m-d H:i:s");

$SysLang = $intranet_session_language;
$SchoolCode = $config_school_code;

$eClassUserType = $lib_user->RecordType;

$eClassUserID = $UserID;
$UserNameChi = $lib_user->ChineseName;
$UserNameEng = $lib_user->EnglishName;
$UserNickname = $lib_user->NickName;

$ClassLevel = $lib_user->ClassLevel;
$ClassName = $lib_user->ClassName;
$ClassNumber = $lib_user->ClassNumber;


$SchoolAddress = $eclass40_httppath;

$UserEmail = $lib_user->UserEmail;

$formdata .= "<input type='hidden' name=\"SysLang\" value=\"".encrypt($SysLang)."\" />";
$formdata .= "<input type='hidden' name=\"SchoolCode\" value=\"".encrypt($SchoolCode)."\" />";
$formdata .= "<input type='hidden' name=\"eClassUserType\" value=\"".encrypt($eClassUserType)."\" />";
$formdata .= "<input type='hidden' name=\"eClassUserID\" value=\"".encrypt($eClassUserID)."\" />";
$formdata .= "<input type='hidden' name=\"UserNameChi\" value=\"".encrypt($UserNameChi)."\" />";
$formdata .= "<input type='hidden' name=\"UserNameEng\" value=\"".encrypt($UserNameEng)."\" />";
$formdata .= "<input type='hidden' name=\"UserNickname\" value=\"".encrypt($UserNickname)."\" />";
$formdata .= "<input type='hidden' name=\"ClassLevel\" value=\"".encrypt($ClassLevel)."\" />";
$formdata .= "<input type='hidden' name=\"ClassName\" value=\"".encrypt($ClassName)."\" />";
$formdata .= "<input type='hidden' name=\"ClassNumber\" value=\"".encrypt($ClassNumber)."\" />";
$formdata .= "<input type='hidden' name=\"TimeStamp\" value=\"".encrypt($TimeStamp)."\" />";
$formdata .= "<input type='hidden' name=\"SchoolAddress\" value=\"".encrypt($SchoolAddress)."\" />";
$formdata .= "<input type='hidden' name=\"UserEmail\" value=\"".encrypt($UserEmail)."\" />";
//
$url = 'http://emag.eclass.com.hk/home/eLearning/elibrary/magazine_learnthenews_list.php';

intranet_closedb();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> eClass IP 2.5 > eLibrary plus</title>
 <script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.fancybox.js"></script>
	<script src="<?=$PATH_WRT_ROOT?>templates/script.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<?=$PATH_WRT_ROOT?>templates/2009a/css/jquery.fancybox.css?v=2.1.0" media="screen" />
	<link href="<?=$PATH_WRT_ROOT?>home/eLearning/elibrary/css/content_25.css" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/elibplus.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript">
	function MM_showHideLayers() { //v9.0
	
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) 
	  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
		if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v=='hide')?'none':v; }
		obj.display=v; }
	}	
	$(document).ready(function(){
		if("<?=$redirect_to_emag_site?>" == "1"){
			window.MainForm.submit();
		}
	});
	function redirectTo_emag (){			
		 window.parent.redirect_to_emag_site();
	}
</script>
<body class="magazine_list_bg">
	<h1 class="magazine_title">Online Magazine(s)</h1>
	<div class="magazine_list">
 		<ul>
          
          <div class="post_stand_row">
               <li class="magazine_learnthenews"><a  href="javascript: void(0)" onclick="redirectTo_emag();" title="Learn the News"></a></li>               
				<form name="MainForm" id="MainForm" action="<?=$url?>" method="post">
					<?=$formdata?>
				</form>
               <li class="magazine_cosmos"><a href="javascript:newWindow('/api/cosmos_access.php', 36)" title="Cosmos"></a></li>                                               
          </div>
          <div class="post_list_board_glass">&nbsp;</div>                                 
      </ul>                                         
    </div>
</body>
</html>

<?php


?>