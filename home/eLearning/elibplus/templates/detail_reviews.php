<?
/*
 * 	Using: 
 * 
 * 	Log
 * 	
 * 	2016-06-14 Cameron
 * 		- fix bug: when scroll down to get more review record, should show <ul> and </ul> for first loading
 * 			otherwise, it'll append duplicate record. [case #X81479]
 * 		- should unbind click event first, then bind it when click 'like/unlike'
 */
 //20150210 Ryan ej DM#535 Changed nl2br($review['content'])
 if (!isset($reviews_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>

$(document).ready(function(){

    $('.review_detail_list .like').unbind("click").bind("click",function(){
        $.post($(this).attr('href'), function(data){
                var like_data=data.split(',');
                $('body #like_'+like_data[0]+" .icon_like").html(like_data[1]);
                $('body #like_'+like_data[0]+" .like").html(like_data[2]);
        });
       
        return false;
    })

    $('.review_detail_list .btn_delete').unbind("click").bind("click",function(){	// 2013-11-14-deleteallbookcomment
        
        if (confirm('<?=$eLib_plus["html"]["areusuretoremovethereview"] ?>')){
            $.fancybox.showLoading();
            $.get($(this).attr('href'), function(data){
                    $('.review_detail_list #review_log_'+data).slideUp(function(){$(this).remove()});
                    $.fancybox.hideLoading();
                    loadStat();
            });
        }
       
        return false;
    });

    $('.remove_all_review').unbind("click").bind("click",function(){	// 		2013-11-14-deleteallbookcomment
        
        if (confirm('<?=$eLib_plus["html"]["areusuretoremoveallthereview"] ?>')){
            $.fancybox.showLoading();
            $.get($(this).attr('href'), function(data){
                    $('.review_detail_list').slideUp(function(){$(this).remove()});
                    $.fancybox.hideLoading();
                    loadStat();
            });
        }
       
        return false;
    });
    
    <? if (sizeof($reviews_data)>=$amount): ?>
    $('.fancybox-inner').off('scroll').on('scroll',function(){

	if ($('.fancybox-inner').prop('scrollHeight')-$('.fancybox-inner').scrollTop()-210<=$('.fancybox-inner').height()){
	    $('.fancybox-inner').off('scroll');
	    $.fancybox.showLoading();
	    $.get('ajax.php?action=getReviews',{book_id: '<?=$book_id?>', offset: '<?=$offset+$amount?>'}, function(data){

		$('.review_detail_list').append(data);
		$.fancybox.hideLoading();
		 
	    });
	}
	
    });
    <? endif; ?>
});
</script>
<? if (empty($reviews_data)&&$offset==0) :?>
    <?=$eLib_plus["html"]["noreviews"]?>
<? else : ?>
  <? if ($offset == 0):?>	
    <ul class="review_detail_list">
  <? endif;?>    
    <? foreach ($reviews_data as $review) : ?>
        <li class="review_log" id="review_log_<?=$review['id']?>">
                <div class="user_photo">
                <span><img src="<?=$review['image']?>" /></span>
            </div>
                <div class="review_detail">
                <h2><?=$review['name'] ? $review['name'] : $eLib_plus["html"]["anonymous"] ?><span> <?=$review['class']?></span></h2>
                
                <? if ($permission['admin']): ?>
                	<div class="book_tool">
                		<a href="ajax.php?action=removeReview&review_id=<?=$review['id']?>" title="<?=$eLib["html"]["remove"]?>" class="btn_delete"></a>
                	
                	</div>
                <? endif; ?>
                
                <div class="star_s"><ul>
                <? for ($i=0; $i<5; $i++) : ?>
                            <li <?=$i<$review['rating']? 'class="star_on"': ''?>>
                 <? endfor; ?>
                </ul></div>
                
                         <p class="spacer"></p>
                         <div class="review_content"><?=nl2br($review['content'])?></div>
                         <p class="spacer"></p>
                        <div class="review_footer">
                                <span class="date_time" title="<?=$review['date']?>"><?=elibrary_plus::getDaysWord($review['date'])?></span>
                                <div class="tool_like" id="like_<?=$review['id']?>">
                                    <a class="icon_like"><?=$review['likes']?></a>
				    <span></span>
				    <a href="ajax.php?action=handleLike&review_id=<?=$review['id']?>&book_id=<?=$book_id?>" class="like">
					
					<?=($lelibplus->getHelpfulCheck($review['id'])) ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"] ?>
				    </a>
                                    
                                    
                                </div>
                  </div>
            
            </div>
        </li><p class="spacer"></p>
    <? endforeach; ?>
  <? if ($offset == 0):?>    
    </ul>
  <? endif;?>
<!--    <p class="spacer"></p>-->
<? endif; ?>
