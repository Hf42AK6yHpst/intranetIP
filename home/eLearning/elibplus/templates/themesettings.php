<script>
$(function(){
    var old_theme = $('body').attr('class');
  
    $('.bg_list li a').click(function(){
	
	var background_theme = $(this).find('.classes').html();
	$('body').fadeOut(function(){
	    $(this).removeClass('elib_bg theme_wood theme_rainbow theme_xmas_2012').addClass(background_theme).fadeIn();
	});
	$(this).parent().addClass('selected').siblings('li').removeClass('selected');
	return false;
    });
    
    $('.bookshelf_list li a').click(function(){
	
	var bookshelf_theme = $(this).find('.classes').html();
	$('body').fadeOut(function(){
	    $(this).removeClass('bookshelf_type_01 bookshelf_type_01_sky bookshelf_type_02 bookshelf_type_03').addClass(bookshelf_theme).fadeIn();
	});
	$(this).parent().addClass('selected').siblings('li').removeClass('selected');
	return false;
    });
    
     $('.setting_bg .formsubbutton').click(function(){
	
	
	$('body').fadeOut(function(){
	    $(this).removeClass('bookshelf_type_01 bookshelf_type_01_sky bookshelf_type_02 bookshelf_type_03 elib_bg theme_wood theme_rainbow theme_xmas_2012').addClass(old_theme).fadeIn();
	});
	
	
	$.fancybox.close();
    });
    $('.setting_bg .formbutton').click(function(){
	
	$.fancybox.close();
	$.post('ajax.php?action=setTheme',{
	    background_theme: $('.bg_list li.selected .classes').html(),
	    bookshelf_theme:  $('.bookshelf_list li.selected .classes').html()
	});

    });
	
    
});
</script>
<div class="setting_bg">
	<div class="setting_board">
	<h1 class="setting_title"><?=$eLib_plus["html"]["themesetting"]?></h1>
     <p class="spacer"></p>
    <div class="control_board">
    	<h2><span><?=$eLib_plus["html"]["background"]?></span></h2>
             
            		
    <ul class="bg_list">
            	<li <?=$theme['background_theme']=='elib_bg'?'class="selected"':''?> id="theme_01">
                	<a target="_parent" title="" class="template_thumb" href="#"><span></span><span style="display:none" class="classes">elib_bg</span></a>
                    <em>Cool</em>
                </li>
                <li <?=$theme['background_theme']=='theme_wood'?'class="selected"':''?> id="theme_02">
                	<a target="_parent" title="" class="template_thumb" href="#"><span></span><span style="display:none" class="classes">theme_wood</span></a>
                    <em>Wooden</em>
                </li>
                <li <?=$theme['background_theme']=='theme_rainbow'?'class="selected"':''?> id="theme_03">
                	<a target="_parent" title="" class="template_thumb" href="#"><span></span><span style="display:none" class="classes">theme_rainbow</span></a>
                    <em>Rainbow</em>
                </li>
                 <li <?=$theme['background_theme']=='theme_xmas_2012'?'class="selected"':''?> id="theme_04">
                	<a target="_parent" title="" class="template_thumb" href="#"><span></span><span style="display:none" class="classes">theme_xmas_2012</span></a>
                    <em>Xmas</em>
                </li>
               <p class="spacer"></p>
                
            </ul>  
             </div>
     <p class="spacer"></p>
    <div class="control_board">
    	<h2><span><?=$eLib_plus["html"]["bookshelf"]?></span></h2>
             
            <ul class="bookshelf_list">
            	<li <?=$theme['bookshelf_theme']=='bookshelf_type_01'?'class="selected"':''?> id="theme_01">
                	<a target="_parent" title="" class="template_thumb" href="#"><span></span><span style="display:none" class="classes">bookshelf_type_01</span></a>
                    <em>Designer Bookshelf</em>
                </li>
                <li <?=$theme['bookshelf_theme']=='bookshelf_type_01 bookshelf_type_01_sky'?'class="selected"':''?> id="theme_02">
                	<a target="_parent" title="" class="template_thumb" href="#"><span></span><span style="display:none" class="classes">bookshelf_type_01 bookshelf_type_01_sky</span></a>
                    <em>Designer Bookshelf Sky</em>
                </li>
                <li <?=$theme['bookshelf_theme']=='bookshelf_type_02'?'class="selected"':''?> id="theme_03">
                	<a target="_parent" title="" class="template_thumb" href="#"><span></span><span style="display:none" class="classes">bookshelf_type_02</span></a>
                    <em>Classic Wooden</em>
                </li>
                <li <?=$theme['bookshelf_theme']=='bookshelf_type_03'?'class="selected"':''?> id="theme_04">
                	<a target="_parent" title="" class="template_thumb" href="#"><span></span><span style="display:none" class="classes">bookshelf_type_03</span></a>
                    <em>Color Wooden 03</em>
                </li>
             <p class="spacer"></p> 
                
            </ul>
             </div>
   
    <p class="spacer"></p>
      </div>
       
	<div class="edit_bottom">
	    <input type="button" value="<?=$eLib["html"]["Apply"]?>" class="formbutton">
	    <input type="button" class="formsubbutton" value="<?=$button_cancel?>">
	</div>
	
        <p class="spacer"></p>

</div>