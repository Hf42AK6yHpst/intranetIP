<?php
/* Change Log:
 * 
 *  Date: 2016-02-15 (Henry) [ip.2.5.7.3.1]
 * 			- fix: the scrolling of the table
 * 
 * 	Date: 2015-12-23 (Paul) [ip.2.5.7.1.1]
 * 			- modified the UI for expired book
 * 
 * 
*/
?>
<? if (!isset($books_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? if ($offset==0) :?>
<script>
$(function(){
   book_number = '<?=$total?>';	//2015-05-04 Charles Ma - P77849
   $('.book_result .total').html('<?=$total?>');
   $('.book_result').fadeIn();
});
</script>
<div class="table_board" style="height:550px">	   
    <table class="common_table_list">
		    
	  <col nowrap="nowrap" style="width:5%"/>
          <col nowrap="nowrap" style="width:45%"/>
          <col nowrap="nowrap" style="width:15%"/>
          <col nowrap="nowrap" style="width:20%"/>
          <col nowrap="nowrap" style="width:10%"/>
          <col nowrap="nowrap" style="width:5%"/>
	  <thead>
	      <tr>
		  <th class="num_check">#</th>
		  <th><a
			 
		  <? if ($sortby=='title') :?>
		  href='#' onclick="sortBookList('title', '<?=($order=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$order?>"
		  <? else: ?>
		  href='#' onclick="sortBookList('title', 'asc'); return false;"
		  <? endif; ?>
		  
		  ><?=$eLib["html"]["book_title"]?></a></th>
		  <th><a 
			 
		  <? if ($sortby=='author') :?>
		  href='#' onclick="sortBookList('author', '<?=($order=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$order?>"
		  <? else: ?>
		  href='#' onclick="sortBookList('author', 'asc'); return false;"
		  <? endif; ?>
		  
		  ><?=$eLib["html"]["author"]?></a></th>
		  <th><a 
		  
		  <? if ($sortby=='publisher') :?>
		  href='#' onclick="sortBookList('publisher', '<?=($order=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$order?>"
		  <? else: ?>
		  href='#' onclick="sortBookList('publisher', 'asc'); return false;"
		  <? endif; ?>
		  
		  ><?=$eLib["html"]["publisher"]?></a></th>
		  <th><a 
		  
		  <? if ($sortby=='hitrate') :?>
		  href='#' onclick="sortBookList('hitrate', '<?=($order=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$order?>"
		  <? else: ?>
		  href='#' onclick="sortBookList('hitrate', 'asc'); return false;"
		  <? endif; ?>
		  
		  ><?=$eLib_plus["html"]["hitrate"]?>/<?=$eLib_plus["html"]["borrow"]?></a></th>
		  <th><a 
		  
		  <? if ($sortby=='rating') :?>
		  href='#' onclick="sortBookList('rating', '<?=($order=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$order?>"
		  <? else: ?>
		  href='#' onclick="sortBookList('rating', 'asc'); return false;"
		  <? endif; ?>
		  
		  ><?=$eLib["html"]["rating"]?></a></th>
	      </tr>
		<? if (!$books_data): ?>
		    <tr><td colspan="6"><?=$eLib["html"]["no_record"]?></td></tr>
		<? endif; ?>
	  </thead>

	  <tbody>
<? endif; ?>
	    <? foreach($books_data as $num=>$book) :?>
	      <tr>
		    <td><?=$num+$offset+1?></td>
		    <td><div class="ebook_title">
			<? if (!empty($book['url']) && !($book['type'] == 'expired')) :?>
			  <a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon_read_now" href="javascript:<?=$book['url']?>" ></a>
			<? elseif(($book['type'] == 'expired')): ?>
			  <a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon_read_now" href="#" ></a>
			<? endif; ?>
			<? if (!($book['type'] == 'expired')) : ?>
				<a rel="category_list" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_title book_detail"><?=$book['title']?></a>
			<? else: ?>
				<a rel="category_list" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_title book_detail off_shelf_list"><?=$book['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a>
			<? endif; ?>
		      </div></td>
		    <? if (!($book['type'] == 'expired')) : ?>
			    <td><?=$book['author']?></td>
			    <td><?=$book['publisher']?></td>
			    <td><?=isset($book['hit_rate'])? $book['hit_rate']: $book['loan_count']?></td>
			    <td><?=$book['rating']?></td>
		    <? else: ?>
		    	<td class="off_shelf_list"><?=$book['author']?></td>
			    <td class="off_shelf_list"><?=$book['publisher']?></td>
			    <td class="off_shelf_list"><?=isset($book['hit_rate'])? $book['hit_rate']: $book['loan_count']?></td>
			    <td class="off_shelf_list"><?=$book['rating']?></td>
		    <? endif; ?>
	      </tr>
	    <? endforeach; ?>

<? if ($offset==0) :?>
	  <tbody>			
      </table>
  </div>		     

<p class="spacer"></p>
<? endif; ?>


