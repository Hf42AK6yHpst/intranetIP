<?php
/*
 * 	Log
 * 
 * 	Date:	2015-07-30 [Cameron]
 * 			- hide "To Top" check box for "rules" form
 */
?>
<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>
$(function(){
    $('.form_btn .close_news').click(function(){
	
	var temp_file_path = $('.news_form input[name="file_temp_path"]').val();
	
	if (temp_file_path!=''){
	    $.post('ajax.php?action=removePortalNoticeAttachment', {temp_file_path: temp_file_path}, function(){
		$.fancybox.close();
	    });
	   
	}else{
	     $.fancybox.close();
	}
	return false;
    });
    
    $('.form_btn .delete_news').click(function(){
	
	if (!confirm('<?=$eLib["html"]["confirm_remove_msg"]?>')) return false;
		
	$.post('ajax.php?action=removePortalNotice', {id: '<?=$portal_data['notice']['id']?>', type: '<?=$type?>'}, function(){
	    $('.p_tab_<?=$type?>').click();
	    $.fancybox.close();
	});
	return false;
    });
    
    $('.news_tool .delete_attachment').click(function(){
	
	$('.attach_status').hide();
	$('.news_tool .tool_new').show();
	$('.news_form input[name="delete_attachment"]').val(1);
	
	return false;
    });
    
    $('.news_tool .tool_new').click(function(){
		
	if (navigator.userAgent.match(/msie/i)){
	    $('.attach_form').show();
	}else{
	    $('.news_list_bg input[name="attachment"]').click();
	}
	
	
	return false;
    });

    $('.news_form').submit(function(){
	
	if($('#date_start').val() > $('#date_end').val()){
	    alert('<?=$eLib["html"]["Incorrect_Date"]?>');		
		return false;
	}
	
	if ($('.news_content').val()==''){
	    alert('<?=$eLib_plus["html"]["pleaseentercontent"]?>');
	    return false;
	}
	
	$.post('ajax.php?action=setPortalNotice', $(this).serialize(), function(){
	    $('.p_tab_<?=$type?>').click();
	    $.fancybox.close();
	});
	return false;
    });
    
    $('.news_list_bg input[name="attachment"]').change(function(){
	
	$.fancybox.showLoading();
	$(this).parent().submit();
	
    });
});

function portal_news_attachmentSet(temp_file_path, file_name){
    
    $.fancybox.hideLoading();
    $('.news_form input[name="file_temp_path"]').val(temp_file_path);
    $('.news_form .attach_status').show().find('.attached_file').attr('href',temp_file_path).html(file_name);
    $('.news_tool .delete_attachment').show();
    $('.news_tool .tool_new').hide();
    $('.news_form input[name="delete_attachment"]').val(0);
}


		$('input#date_from').ready(function(){
			$('input#date_start').datepick({
				
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0,
				onSelect: function(dateText, inst) {
Date_Picker_Check_Date_Format(document.getElementById('date_start'),'DPWL-date_start','');
					}
				});
			});
			
		$('input#date_to').ready(function(){
			$('input#date_end').datepick({
				
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0,
				onSelect: function(dateText, inst) {
Date_Picker_Check_Date_Format(document.getElementById('date_end'),'DPWL-date_end','');
					}
				});
			});		

</script>
<div class="news_list_bg" style="height:470px">
    <div class=" news_list ">
    	<div class="news_header"><?=$portal_data['header']?></div>

            <form class="news_form">
		<? if($type=='news'): ?>
		<span><?=$eLib_plus["html"]["title"]?>:</span>
		    <input type="text" name="title" class="news_title" value="<?=htmlspecialchars(stripslashes($portal_data['notice']['title']))?>">
		<p class="spacer"></p>
		<span><?=$eLib_plus["html"]["content"]?>:</span>
		<? endif; ?>
		<textarea class="news_content" wrap="virtual" name="content" rows="5"><?=$portal_data['notice']['content']?></textarea>
		
		<div <?=$portal_data['notice']['attachment']?'':'style="display:none"'?> class="attach_status">
		    <?=$eLib_plus["html"]["attachedfile"] ?>:
		    <a class="attached_file" href="<?=str_replace($portal_data['notice']['attachment_name'], urlencode($portal_data['notice']['attachment_name']), $portal_data['notice']['attachment'])?>" target="_blank"><?=$portal_data['notice']['attachment_name']?></a>
		    <div class="news_tool tool_normal" style="float:right;top:-3px;">
			 <a href="#" class="tool_delete delete_attachment">&nbsp;</a>

		    </div>
		</div>		
		
		<div class="news_tool tool_normal" >
		    <a  <?=$portal_data['notice']['attachment']?'style="display:none"':''?> href="#" class="tool_new"><?=$eLib_plus["html"]["attachfile"] ?></a>
		    <div <?=$portal_data['notice']['attachment']?'style="display:none"':''?> class="tool_new">
		    	<div style="padding-bottom:25px"></div>
		    </div>
		</div>
		
		<div class="" style="display:<?=($type=="rules"? "none":"block")?>">
			<?=$Lang['Btn']['ToTop'] ?> : <input type="checkbox" name="priority" <?=$portal_data['notice']['priority']?"checked":""?> value="1">
		</div>
		<br/>
		<div class="">
			<?=$Lang['General']['From']?> : <input type="text" name="date_start" id="date_start" value="<?=strtotime($portal_data['notice']["date_start"]) == "943891200"  || $portal_data['notice']["date_start"] == "" ? "" : date("Y-m-d",strtotime($portal_data['notice']['date_start']))?>" >
			<?=$Lang['General']['To']?> : <input  type="text" name="date_end" id="date_end" value="<?= strtotime($portal_data['notice']["date_end"]) == "943891200" || $portal_data['notice']["date_end"] == "" ? "" :  date("Y-m-d",strtotime($portal_data['notice']['date_end']))?>" >
		</div> 
		
		<div class="form_btn">
		    <input type="submit" value="<?=$eLib["html"]["submit"]?>" class="formbutton" name="submit2">
		    <? if ($id): ?>
		    <input type="button" value="<?=$eLib["html"]["Delete"]?>" class="formsubbutton delete_news" name="submit2">
		    <? endif; ?>
		    <input type="button" value="<?=$eLib["html"]["cancel"]?>" class="formsubbutton close_news" name="submit2">
		</div>
		 
		
		<input type="hidden" name="id" value="<?=$portal_data['notice']['id']?>"/>
		<input type="hidden" name="type" value="<?=$type?>"/>
		<input type="hidden" name="file_temp_path" value=""/>
		<input type="hidden" name="delete_attachment" value="0"/>

	    </form>
	    
	    <form style="display:none;float:right;margin-top:-65px;" class="attach_form" target="upload_frame" action="ajax.php?action=uploadPortalNoticeAttachment&callback=window.parent.portal_news_attachmentSet" method="post" enctype="multipart/form-data">
		<input class="upload_button" name="attachment" type="file"/>
	      
	    </form>
		    	   
            <iframe name="upload_frame" style="display:none;"></iframe>
    </div>
</div>