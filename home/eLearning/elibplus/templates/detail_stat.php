<?php
/*
 * 	Log:
 * 
 * 	Date: 2015-08-11 [Cameron] 
 * 			- fix bug on clicking "Remove Favourite" by changing the class to 'add_fav' for removefavourite icon 
 *
 * 	Date: 2015-08-03 [Cameron]
 * 			- change book_tool button position to under the wood background 
 * 
 */
?>
<? if (!isset($stat_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>
$(document).ready(function(){
		
		 $('.add_recommend').unbind("click").bind("click", function(e){
                    
                    $($(this).attr('href')).slideDown().siblings().hide();
                    
                    return false;
                    
        });		
		
         $('.book_tool .add_request').unbind("click").bind("click",function(e){
		if($('.book_tool .add_request').attr('class') == "cancelhold add_request"){
			if(!confirm('Are you sure you want to delete?'))
				return false;
		}
		$.fancybox.showLoading();
		$.get($(this).attr('href'),function(result){
		  
		    var target = $('.book_tool .add_request');
		    //Henry Added
		    var reservedCount = $('#div_reserved_count').text();
		    
		    if (result==1){
			target.html('<?=$eLib_plus["html"]["cancelhold"]?>');
			//Henry Added
			$('#div_reserved_count').html(parseInt(reservedCount) + 1);
			target.removeClass( "add_request" ).addClass( "cancelhold add_request" );
		    }else if (result==0){
			target.html('<?=$eLib_plus["html"]["requesthold"]?>');
			//Henry Added
			$('#div_reserved_count').html(parseInt(reservedCount) - 1);
			target.removeClass( "cancelhold add_request" ).addClass( "add_request" );
		    }else{
		    	if (result==-1)
					alert('<?=$eLib_plus["html"]["cannothold_borrowed"]?>');
				else if(result==-2)
					alert('<?=$eLib_plus["html"]["cannothold_over_quota"]?>');
				else if(result==-3)
					alert('<?=$eLib_plus["html"]["cannothold_book_disable"]?>');
				else if(result==-4)
					alert('<?=$eLib_plus["html"]["cannothold_book_available"]?>');
				else
					alert('<?=$eLib_plus["html"]["cannothold"]?>');
		    }
		    $.fancybox.hideLoading();
		});
		return false;                     
         });
	 $('.book_tool .add_fav').click(function(e){
		$.fancybox.showLoading();
		$.get($(this).attr('href'),function(result){
		  
		    var target = $('.book_tool .add_fav');
		    if (result){
			target.html('<?=$eLib_plus["html"]["removefavourite"]?>');
		    }else{
			target.html('<?=$eLib_plus["html"]["addtofavourite"]?>');
		    }
		    $.fancybox.hideLoading();
		});
		return false;   
	 });
	
         	    
});
</script>

<div class="star_b">
         <ul>
                  <? for ($i=0; $i<5; $i++) : ?>
                              <li <?=$i<$stat_data['rating']? 'class="star_on"': ''?>>
                   <? endfor; ?>
                  </ul>
         <span>(<?=$stat_data['review_count']?> <?=$eLib["html"]["reviews"]?>
         <?= isset($stat_data['hit_rate'])? ', '.$stat_data['hit_rate'].' '.$eLib_plus["html"]["hitsofreading"]: ''?>
         <?= isset($stat_data['loan_count'])? ', '.$eLib_plus["html"]["borrowed"].' '.$stat_data['loan_count'].' '.$eLib_plus["html"]["times"]: ''?>)
         </span>
</div>
<div class="book_tool" style="padding: 18px;clear: both;">
		<? if ($permission['admin']) : ?>
         <a href="#recommend_form" class="recommend_to add_recommend"><?=$eLib["html"]["recommend_this_book"]?></a>
         <em></em>
         <? endif; ?>
         <? if ($stat_data['is_favourite']) : ?>
                 <a href="ajax.php?action=handleFavourite&book_id=<?=$book_id?>" class="add_fav"><?=$eLib_plus["html"]["removefavourite"]?></a>
         <? else: ?>
                 <a href="ajax.php?action=handleFavourite&book_id=<?=$book_id?>" class="add_fav"><?=$eLib_plus["html"]["addtofavourite"]?></a>
         <? endif; ?>
         <? if ($stat_data['status'] && $permission['reserve']) : ?>
                 <? if ($stat_data['is_reserved']) : ?>
                  <!--<a href="ajax.php?action=handleReservation&book_id=<?=$book_id?>" class="add_request"><?=$eLib_plus["html"]["cancelhold"]?></a>
                 -->
                 <a href="ajax.php?action=handleReservation&book_id=<?=$book_id?>" class="cancelhold add_request"><?=$eLib_plus["html"]["cancelhold"]?></a>
                 <? else: ?>
                  <a href="ajax.php?action=handleReservation&book_id=<?=$book_id?>" class="add_request"><?=$eLib_plus["html"]["requesthold"]?></a>
                 <? endif; ?>
         <? endif; ?>
 
</div>