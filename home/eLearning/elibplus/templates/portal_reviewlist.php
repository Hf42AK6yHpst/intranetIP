<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<div class="ranking_list ranking_list_review">
    <ul>
	<? if(!$portal_data['reviews']) : ?><li><div><?=$eLib_plus["html"]["norecord"]?></div></li><? endif;?>
	<? foreach ($portal_data['reviews'] as $i=>$review): ?>
	
	    <li <?=$i<3?'class="rank_'.($i+1):''?>"><a class="book_detail" rel="portal_review" href="ajax.php?action=getBookDetail&book_id=<?=$review['id']?>"><span><?=$i+1?></span><h1><?=$review['title']?></h1><em><u>Reviewed by </u><?=$review['name']?><u><?=$review['class']?></u></em></a><p class="spacer"></p></li>
	<? endforeach; ?>

    </ul>
</div>