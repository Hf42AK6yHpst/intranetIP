<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<div class="ranking_list">
    <ul>
	
	<li class="rank_title"><div><h1><?=$eLib["html"]["student"]?></h1><em><?=$eLib["html"]['Total']?></em></div></li>
	<? if(!$portal_data['users']) : ?><li><div><?=$eLib_plus["html"]["norecord"]?></div></li><? endif;?>

	<? foreach ($portal_data['users'] as $i=>$user): ?>
	
	    <li <?=$i<3?'class="rank_'.($i+1).'"':''?>> <a href="<?='./?page=ranking&range='.$range.'#'.$type?>"><span><?=$i+1?></span><h1><?=$user['name']?><u><?=$user['class']?> </u></h1><em><?=$user['review_count']?$user['review_count']:$user['loan_count']?> </em></a></li>
	    
	<? endforeach; ?>

    </ul>
</div>