<?
/*
 * 	Log
 * 
 * 	2016-08-05 [Cameron]
 * 		- fix bug: reset student_id ="" when click "Apply" button to fit export function 
 */
?>
<? if (!isset($ebook_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? if (!$ebook_data['reading']&&$offset==0): ?>
    <tr><td colspan="6" align="center"><?=$eLib_plus["html"]["norecord"]?></td></tr>
<? else: ?>
<tr style="display:none;">
    <script>    
	$(".record_detail").attr({"class": "record_detail record_pcs"});
    
    var FromMonth = "";
	var FromYear = "";
	var ToMonth = "";
	var ToYear = "";
    var Class = "";
	<? if (sizeof($ebook_data['reading'])>=$amount): ?>
	$('.record_pcs').off('scroll').on('scroll',function(e){
	    if ($(this).prop('scrollHeight')-$(this).scrollTop()-150<=$(this).height()){		
	    	FromMonth = $("#FromMonth_Search").val();
			FromYear = $("#FromYear_Search").val();
			ToMonth = $("#ToMonth_Search").val();
			ToYear = $("#ToYear_Search").val();
			var sort =$("#Sort_Search").val();
			var order = $("#Order_Search").val();
			
			$.fancybox.showLoading();
			$('.record_detail').off('scroll');
			$.get('ajax.php?action=getPublicClassSummaryRecord&year_class_id=<?=$year_class_id?>&student_id=<?=$student_id?>&class_name=<?=$class_name?>&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear, {offset:'<?=$offset+$amount?>', order : order, sort : sort}, function(data){
			    $.fancybox.hideLoading();
			    $('.record_pcs table').append(data);		     
			});		
	    }
	});	
	<? endif; ?>
	var applySearch_Search = function(){
		if($('#FromMonth_Search').val()=='' || $('#FromYear_Search').val()=='' || $('#ToMonth_Search').val()=='' || $('#ToYear_Search').val()=='')
		{
			if($('#FromMonth_Search').val()!='' || $('#FromYear_Search').val()!='' || $('#ToMonth_Search').val()!='' || $('#ToYear_Search').val()!=''){
				alert('Invalid date range.');
				return false;
			}
		}
		var strFromDate = $('#FromYear_Search').val()+"-"+$('#FromMonth_Search').val()+"-0";
		var strToDate =$('#ToYear_Search').val()+"-"+$('#ToMonth_Search').val()+"-0";
		
		if(compareDate(strToDate,strFromDate)<0){
			alert('Invalid date range.');
				return false;
		}
		student_id ="";
		$("#Sort_Search").val('');
		 $("#Order_Search").val('');
		$("#FromMonth").val($("#FromMonth_Search").val());
		$("#FromYear").val($("#FromYear_Search").val());
		$("#ToMonth").val($("#ToMonth_Search").val());
		$("#ToYear").val($("#ToYear_Search").val());
		
		FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();
		ClassName = $("#ClassName_Search").val();
		
		$.fancybox.showLoading();
		$('.record_detail').off('scroll');
		
		if(ClassName == "" || ClassName == undefined || !ClassName){
			$.get('ajax.php?action=getPublicClassSummaryRecord&class_name=<?=$class_name?>&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear, {offset:'0'}, function(data){
			    $.fancybox.hideLoading();
			    $('.record_detail table').html($("#pulic_class_summary_temp").html());
			    $('.record_detail table').append(data);	     
			     $('.record_detail').scrollTop(0);
			});	
		}else{
			$.get('ajax.php?action=getPublicClassSummaryRecord&class_name='+ClassName+'&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear, {offset:'0'}, function(data){
			    $.fancybox.hideLoading();
			    $('.record_detail table').html($("#pulic_student_summary_temp").html());
			    $('.record_detail table').append(data);	     
			     $('.record_detail').scrollTop(0);
			});	
		}
		
	}	
	
	$('.class_tab').click(function(){
		var target=$(this).attr('href');				
		var class_name = target.slice(target.lastIndexOf("#ClassName")).replace('#ClassName','');	
		
		FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();		
		
		$("#Sort_Search").val('');
		 $("#Order_Search").val('');
		 
		$("#ClassName_Search").val(class_name);	
		$.fancybox.showLoading();
		$.get('ajax.php?action=getPublicClassSummaryRecord&class_name='+class_name+'&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear,{offset: 0}, function(data){					
		    $.fancybox.hideLoading();
		    $('.record_detail table').html($("#pulic_student_summary_temp").html());
		    $('.record_detail table').append(data);
		    $('.record_detail').scrollTop(0);
		});		
		
		return false;
	});
	
	$('.sort_column').click(function(){		
		$.fancybox.showLoading();
		
		var sort = $(this).attr('name');
		var order = $(this).attr('class').replace("sort_column", "").replace(" ", "");
		
		if(order == ""|| order == "sort_asc") order = "sort_dec";
		else order = "sort_asc";
				
		$("#Sort_Search").val(sort);
		$("#Order_Search").val(order);
		
		FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();	
		
		$.get('ajax.php?action=getPublicClassSummaryRecord&year_class_id=<?=$year_class_id?>&student_id=<?=$student_id?>&class_name=<?=$class_name?>&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear,{offset: 0, order : order, sort : sort}, function(data){	  
		    $.fancybox.hideLoading();
			$('.sort_column').attr("class","sort_column");
		    $('.record_detail table').html($("#pulic_class_summary_temp").html());
			 $('.record_detail table').append(data);	   
		    $('.record_detail').scrollTop(0);
		});
		
		return false;
	});
	
	$(function(){
		$('.sort_column').attr("class","sort_column");
		$('[name="<?=$sort?>"]').attr("class", "sort_column <?=$order?>");
	});		
	
	
    </script>
</tr>
	<? foreach ($ebook_data['reading'] as $num=>$progress) : ?>
	    <tr>
		<td><?=$offset+$num+1?></td>	
		<td><?=$progress['class_link']?></td>
		<td><?=$progress['num_student']?></td>
		<td><?=$progress['NumBookRead']?></td>
		<td><?=$progress['read_average']?></td>
		<td><?=$progress['NumBookReview']?></td>
		<td><?=$progress['review_average']?></td>
	    </tr>
    <? endforeach; ?>
<? endif; ?>