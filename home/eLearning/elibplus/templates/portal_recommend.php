<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
    <? if(!$portal_data['books']) : ?><li><div><?=$eLib_plus["html"]["norecord"]?></div></li><? endif;?>
    <? foreach ($portal_data['books'] as $book) : ?>
	<? if ($book['image']): ?>
	    <li>
	    <a class="book_cover" href="#">
		<img src="<?=$book['image']?>"><span class="icon_ebook_01"></span>
	    </a>
	<? else: ?>
	    <li class="no_cover">
	    <a class="book_cover" href="#">
		<span class="icon_ebook_01"></span>
	    </a>
	<? endif; ?>
	
	    <div class="book_over_btn">eBook (PC+iPad)<a href="#">Read Now</a><a href="#">Book Detail</a></div>
	    <? if ($book['reason']): ?>
		<div class="most_recommend_reason">
		    
		    <h1><?=$book['title']?></h1>
		    <h2><?=$book['author']?></h2>
		   
		    <p class="spacer"></p>
		    
			<div class="most_recommend_reason_detail"><?=$book['reason']?></div>
		    
		    
		</div>
	    <? endif; ?>
	</li>
    
    <? endforeach; ?>