<? 
/*
 * Editing by Paul
 * 
 * Modification Log:
 * 
 * 2015-12-16 (Paul)
 * 		- modified the UI for expired book
 * 2015-02-11 (Henry)
 * 		- added sub title in book details (hided temporary)
 * 
 */
if (!isset($rank_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>

$(document).ready(function(){
    $('.toggle_tool a').click(function(){
        var target=$($(this).attr('href'));
        
        target.fadeIn().siblings().hide();
        $(this).parent().addClass('selected').siblings().removeClass('selected');
        
        return false;
    });
});
</script>

      <div class="toggle_tool">
	  <ul>
	      <li><a href="#ranking_list" class="view_list" title="<?=$eLib_plus["html"]["booklist"]?>"></a></li>
	      <li class="selected"><a href="#ranking_cover" class="view_cover" title="<?=$eLib_plus["html"]["bookcover"]?>"></a></li>
	      <li><span><?=$eLib_plus["html"]["viewby"]?>:</span> </li>
	  </ul>
      </div>
   
<div class="ranking_book_list">
    
<ul id="ranking_cover">

    
<!-- rank 04-->
<? foreach (array_slice($rank_data,0,5) as $num=>$book): ?>
    <li class="ranking_book_list_log">
        <div class="ranking_book_list_rank">
            <? if ($num<3){
                echo '<span class="rank_0'.($num+1).'"></span>';
            }else{
                echo '<span>'.($num+1).'</span>';
            }
            
            ?>
        </div>
        <div class="ranking_book_list_book">
            <ul class="book_list">
            
                    <?=elibrary_plus::getBookListView(array($book))?>
                

            </ul>
        </div>
        <div class="ranking_book_list_detail">

            <h1><?=$book['title']?></h1>
            <table>
                <colgroup>
                    <col class="book_info_field">
                    <col class="book_info_field_sep">
                </colgroup>
                <tbody>
                	<!--<?=($book['type'] == 'physical'&&trim($book['sub_title']) != ''?'<tr><td colspan="2"> - '.$book['sub_title'].'</td></tr>':'')?>-->
                    <tr><td><?=$eLib["html"]["author"]?> </td><td>:</td><td>
                    <? foreach ($book['authors'] as $i=>$author): ?>
                                    <?=$i>0?", ":''?><a href="./?page=search&keyword=<?=$author?>"><?=$author?></a>
                                <? endforeach; ?>
                    </td></tr>
                    <tr><td><?=$eLib["html"]["publisher"]?> </td><td>:</td><td><?=$book['publisher']?></td></tr>
                </tbody>
            </table>
            <div class="star_b">
                <ul>
                <? for ($i=0; $i<5; $i++) : ?>
                              <li <?=$i<$book['rating']? 'class="star_on"': ''?>>
                <? endfor; ?>
                </ul>
                <span>(<?=$book['review_count']?> <?=$eLib["html"]["reviews"]?>
                <?=isset($book['hit_rate']) ? ', '.$book['hit_rate'].' '.$eLib_plus["html"]["hitsofreading"]: '' ?>
                <?= isset($book['loan_count'])? ', '.$eLib_plus["html"]["borrowed"].' '.$book['loan_count'].' '.$eLib_plus["html"]["times"]: ''?>
                )
                </span>
            </div>
        
        </div>
        <p class="spacer"></p>
    </li>
    <!-- rank 04 end -->
<? endforeach; ?>
    </ul>
            
<div class="table_board" id="ranking_list" style="display:none;">	   
	    <table class="common_table_list">
			    
		  <col nowrap="nowrap"/>
		  <thead>
		      <tr>
			  <th class="num_check">#</th>
			  <th><?=$eLib["html"]["book_title"]?></th>
			  <th><?=$eLib["html"]["author"]?></th>
			  <th><?=$eLib["html"]["publisher"]?></th>
			  <th><?=$eLib_plus["html"]["hitsofreading"]?>/<?=$eLib_plus["html"]["borrow"]?></th>
                          <th><?=$eLib["html"]["rating"]?></th>
		      </tr>
		  </thead>
	
		  <tbody>
		    <? foreach($rank_data as $num=>$book) :?>
		      <tr>
			    <td><?=$num+1?></td>
			    <td>
			      <div class="ebook_title">
				<? if (!empty($book['url']) && !($book['type'] == 'expired')) : ?>
					<a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon_read_now" href="javascript:<?=$book['url']?>" ></a>
				<? elseif($book['type'] == 'expired'): ?>
					<a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon_read_now" href="#" ></a>
				<? endif; ?>
				<? if (!($book['type'] == 'expired')) : ?>
					<a rel="record_favourite_list" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_detail"><?=$book['title']?></a>
			      <!--<?=($book['type'] == 'physical'&&trim($book['sub_title']) != ''?' - '.$book['sub_title']:'')?>-->
			   <? else : ?>
			   		<a rel="record_favourite_list" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_detail off_shelf_list"><?=$book['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a>
			   <? endif; ?>
			      </div>
			    </td>
			    <? if (!($book['type'] == 'expired')) : ?>
			    	<td><?=$book['author']?></td>
			    	<td><?=$book['publisher']?></td>
                	<td><?=isset($book['hit_rate'])? $book['hit_rate']: $book['loan_count']?></td>
			    	<td><?=$book['rating']?></td>
			    <? else: ?>
			    	<td class="off_shelf_list"><?=$book['author']?></td>
			    	<td class="off_shelf_list"><?=$book['publisher']?></td>
                	<td class="off_shelf_list"><?=isset($book['hit_rate'])? $book['hit_rate']: $book['loan_count']?></td>
			    	<td class="off_shelf_list"><?=$book['rating']?></td>
			    <? endif; ?>	
			    
		      </tr>
		    <? endforeach; ?>
		  <tbody>			
	      </table>
	  </div>	
</div>


  
      
      <p class="spacer"></p>


