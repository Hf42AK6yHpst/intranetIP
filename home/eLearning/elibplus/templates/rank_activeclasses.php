<? if (!isset($rank_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>

$(document).ready(function(){
    $('.ranking_user_list .like').click(function(e){
        
        $.post($(this).attr('href'), function(data){
                var like_data=data.split(',');
                $('#like_'+like_data[0]+" .icon_like").html(like_data[1]);
                $('#like_'+like_data[0]+" .like").html(like_data[2]);
        });
       
        return false;
    })
    
    $('.ranking_user_list_log_open_shadow a, .no_of_review a').click(function(e){
        
        var target=$($(this).attr('href'));
        target.fadeToggle('slow').parent().toggleClass('ranking_user_list_log_open');
        return false;
    });
    
});
</script>


                    <div class="ranking_user_list">
            
                            <ul>
               <!--rank title-->
                <li class="ranking_user_list_log_title">
                <div class="ranking_user_list_title">
                    <span><?=$eLib["html"]["class"]?></span><span><?=$eLib["html"]["num_of_review"]?></span><span><?=$eLib_plus["html"]["lastreview"]?></span>
                </div>
                </li>
             <!--rank title end-->
                    <!-- rank 01-->
                <? foreach ($rank_data as $num=>$class) :?>
                    <li class="ranking_user_list_log">
                        <div class="ranking_user_list_rank"><span <?=$num<3? 'class="rank_0'.($num+1).'"' : ''?>><?=($num+1)?></span></div>
                        <div class="user_photo user_class"></div>
                        <div class="ranking_user_list_detail">
                                <h2><?=$class['class']?></h2>
                                <div class="no_of_review"><a href="#rank_review_<?=$num+1?>"><?=$class['review_count']?> <?=$eLib["html"]["reviewsUnit"]?></a></div>
                                <div class="last_review"><a href="ajax.php?action=getBookDetail&book_id=<?=$class['reviews'][0]['id']?>" class="book_cover"><?=$class['reviews'][0]['title']?></a></div>
                        </div>
                        
            
                       <p class="spacer"></p>
                       <div class="ranking_user_review_container" id="rank_review_<?=$num+1?>" style="display:none"><ul class="ranking_user_review">
                        <? foreach ($class['reviews'] as $review) :?>
                            <li class="review_log" id="review_log_<?=$review['review_id']?>">
                                <h1 class="class_reviewer"><img src="<?=$review['user_image']?>" align="absmiddle"><?=$review['name']?> :</h1>
                                <div class="review_book">
                                        <ul class="book_list">
                                            
                                        <?=elibrary_plus::getBookListView(array($review))?>
                                        
                                        </ul>
                                </div>
                                <div class="review_detail">
                                    
                                    <div class="star_s">
                                        <ul>
                                        <? for ($i=0; $i<5; $i++) : ?>
                                                      <li <?=$i<$review['rating']? 'class="star_on"': ''?>>
                                        <? endfor; ?>
                                        </ul>
                                    </div>
                                    
                                     <p class="spacer"></p>
                                     <div class="review_content"><?=$review['content']?></div>
                                     <p class="spacer"></p>
                                    <div class="review_footer">
                                        <span class="date_time" title="<?=$review['date']?>"><?=$review['days']?> <?=$eLib_plus["html"]["daysago"]?></span>
                                        <div class="tool_like" id="like_<?=$review['review_id']?>">
                                                                        
                                            <a href="ajax.php?action=handleLike&review_id=<?=$review['review_id']?>&book_id=<?=$review['book_id']?>" class="like">
                                                    <?=($lelibplus->getHelpfulCheck($review['review_id'])) ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"] ?>
                                                
                                            </a>
                                            
                                            <a class="icon_like"><?=$review['likes']?></a>
                                        </div>
                                  </div>
                                
                                </div>
                                <p class="spacer"></p>
                            </li>
                        <? endforeach; ?>
                              <p class="spacer"></p>
                            
                        </ul>
                        <div class="ranking_user_list_log_open_shadow"><a href="#rank_review_<?=$num+1?>">Hide</a></div>
                                </div>
                        
                    </li>
                    <? endforeach; ?>
   
                    </ul>
                </div>

                <p class="spacer"></p>

