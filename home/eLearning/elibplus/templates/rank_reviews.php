<? if (!isset($rank_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>

$(document).ready(function(){
    $('.ranking_user_list .like').click(function(e){
        
        $.post($(this).attr('href'), function(data){
                var like_data=data.split(',');
                $('#like_'+like_data[0]+" .icon_like").html(like_data[1]);
                $('#like_'+like_data[0]+" .like").html(like_data[2]);
        });
       
        return false;
    })
        
});
</script>

                    <div class="ranking_user_list ranking_user_list_review">
            
                            <ul>
              
                    <!-- rank 01-->
            <? foreach ($rank_data as $num=>$review) :?>
                <li class="ranking_user_list_log" id="review_log_<?=$review['review_id']?>">
                    <div class="ranking_user_list_rank"><span <?=$num<3? 'class="rank_0'.($num+1).'"' : ''?>><?=($num+1)?></span></div>
                    <div class="user_photo">
                        <span><img src="<?=$review['user_image']?>"></span>
                    </div>
                    
                     <div class="ranking_user_list_detail">
                        <h2><?=$review['name'] ? $review['name'] : $eLib_plus["html"]["anonymous"] ?> <span><?=$review['class']?></span></h2>
                        <ul class="ranking_best_review">
                        <li class="review_log">
                            <div class="review_book">
                                    <ul class="book_list book_list_small">
                                    
                                         <?=elibrary_plus::getBookListView(array($review))?>
                                    
                              <!--<li class="no_cover"> <a href="#" class="book_cover"><h1>Bookname</h1><span>Author</span></a><a href="#" class="icon_ebook"><span>Read Now</span></a></li>-->
                                    </ul>
                            </div>
                            <div class="review_detail">
                            
                                
                                <div class="star_s">
                                    <ul>
                                        <? for ($i=0; $i<5; $i++) : ?>
                                                      <li <?=$i<$review['rating']? 'class="star_on"': ''?>>
                                        <? endfor; ?>
                                    </ul>
                                </div>
                                
                                 <p class="spacer"></p>
                                 <div class="review_content"><?=$review['content']?></div>
                                 <p class="spacer"></p>
                                <div class="review_footer">
                                        <span class="date_time" title="<?=$review['date']?>"><?=elibrary_plus::getDaysWord($review['date'])?></span>
                                        <div class="tool_like" id="like_<?=$review['review_id']?>">
                                                                        
                                            <a href="ajax.php?action=handleLike&review_id=<?=$review['review_id']?>&book_id=<?=$review['book_id']?>" class="like">

                                                    <?=($lelibplus->getHelpfulCheck($review['review_id'])) ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"] ?>
                          
                                                
                                            </a>
                                            
                                            <a class="icon_like"><?=$review['likes']?></a>
                                        </div>
                            </div>
                            <p class="spacer"></p>
                        </li>
                       <p class="spacer"></p>
                    </ul>
                   
                      </div>
                    
        
                   <p class="spacer"></p>
                   
                 
                  
                </li>
                <? endforeach; ?>
                <!-- rank 01 end -->
                
                </ul>
            </div>

            
              
                  
                  <p class="spacer"></p>
 
