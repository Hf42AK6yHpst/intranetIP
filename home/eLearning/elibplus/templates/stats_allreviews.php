<?php
/*
 * 	Log 
 * 	
 * 	Date:	2015-04-28 [Cameron]
 * 			- Add date range filter and export button
 * 
 * 	Note: export function is available to user who has eAdmin-LibraryMgmtSystem right
 */
?>
<? if (!isset($allreviews)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>
$(function(){
	
	$('.record_tab li a').click(function(){		
		var target=$(this).attr('href');
		$(target).siblings().fadeOut();
		$.fancybox.showLoading();
		$.get('ajax.php?action=get'+target.replace('#',''),{offset: 0}, function(data){
		    $(target).fadeIn().find('table').append(data);
		    $('.record_detail').scrollTop(0);
		    $.fancybox.hideLoading();
		});
		;
		$(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
		
		return false;
	});
	
	$('.record_tab li.current_tab a').click();
	
	$("#DataPeriod").val($("input:radio[name='searchDataPeriod']:checked").val());
	$("#FromDate").val($("#searchFromDate").val());
	$("#ToDate").val($("#searchToDate").val());
	
	$('#ApplySearch').click(function(){
		$("#DataPeriod").val($("input:radio[name='searchDataPeriod']:checked").val());
		$("#FromDate").val($("#searchFromDate").val());
		$("#ToDate").val($("#searchToDate").val());
		
		$.ajax({		
			type: "POST",	
			url: 'ajax.php',
			data: {	
				'action':'getAllReviewsRecord',
				'searchDataPeriod':$("input:radio[name='searchDataPeriod']:checked").val(),
				'searchFromDate':$('#searchFromDate').val(),
				'searchToDate':$('#searchToDate').val(),
				'sort':$('#searchSort').val(),
				'order':$('#searchOrder').val()
			},
			timeout:1000,
			success : function(return_data) {			
			    $.fancybox.hideLoading();
				$('.sort_column').attr("class","sort_column");
			    $('.record_detail table').html($("#all_reviews_temp").html());
			    $('.record_detail table').append(return_data);
			    $('.record_detail').scrollTop(0);
			}			
		});		
	});
});	

function jsExport(page)
{
	var filter = '&searchDataPeriod=' + $("input:radio[name='searchDataPeriod']:checked").val() + '&searchFromDate=' + $('#searchFromDate').val() + '&searchToDate=' + $('#searchToDate').val() + '&order=' + $('#searchOrder').val() + '&sort=' + $('#searchSort').val(); 
	self.location = "export_reviews.php?page=" + page + filter;	
}

</script>

<div class="record_tab" style='display:none'>
	<ul>
	<li class="current_tab"><a href="#AllReviewsRecord"><span><?=$eLib["html"]["my_reading_history"] ?></span></a></li>
    </ul>
</div>
<?php
	if($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ) {
?>
<div class="content_top_tool"  style="float: right;">
	<div class="Conntent_tool"><a href="javascript:jsExport('stats')" class="export"><?=$eLib_plus["html"]["export"]["review_statistics"]?></a></div>  
	<div class="Conntent_tool"><a href="javascript:jsExport('reviews')" class="export"><?=$eLib_plus["html"]["export"]["reviews"]?></a></div>
</div>
<?php
	}
?>
<!-- date range filter -->
<?php
	// default one week before up to today
	$searchFromDate = ($searchFromDate == '') ? date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 7, date("Y"))) : $searchFromDate;
	$searchToDate = ($searchToDate == '') ? date('Y-m-d') : $searchToDate;
	$Period = ($Period == 'Date') ? $Period : 'All';	// default All
?>	
<div class="content_top_tool">
	<input name="searchDataPeriod" type="radio" id="radioPeriod_All" value="All" <? echo ($Period=="All")?" checked":"" ?>><?=$eLib_plus["html"]["all"]?><br>
	<input name="searchDataPeriod" type="radio" id="radioPeriod_Date" value="Date" <? echo ($Period=="Date")?" checked":"" ?>>
	<span><?=$i_From?></span>
	<span><?=$linterface->GET_DATE_PICKER("searchFromDate",$searchFromDate,"")?>
	<span id='div_DateEnd_err_msg'></span></span>
	<span><?=$i_To?></span>
	<span><?=$linterface->GET_DATE_PICKER("searchToDate",$searchToDate,"")?></span>
	<span><input type="button" name="search" id="ApplySearch" value="<?=$Lang['Btn']['Apply']?>"/></span>
</div>

<div class="record_detail">
	   <div id="AllReviewsRecord" style='display:'>
	      <div class="table_board" >	
			<table class="common_table_list">
				<col nowrap="nowrap" width="5%"/>
				<col nowrap="nowrap" width="30%"/>
				<col nowrap="nowrap" width="30%"/>
				<col nowrap="nowrap" width="15%"/>
				<col nowrap="nowrap" width="15%"/>
				<tr>
				  <th class="num_check">#</th>
				  <th><a href="#" name="sort_bt" class="sort_column"><?=$eLib["html"]["book_title"]?></a></th>
				  <th><a href="#" name="sort_a" class="sort_column"><?=$eLib["html"]["author"]?></a></th>
				  <th><a href="#" name="sort_nor" class="sort_column"><?=$eLib["html"]["num_of_review"]?></a></th>
				  <th><a href="#" name="sort_ls" class="sort_column"><?=$eLib["html"]["last_submitted"]?></a></th>
				</tr>
			</table>
		</div>
	   </div>
</div>           

<input type="hidden" id="FromDate" value="">
<input type="hidden" id="ToDate" value="">
<input type="hidden" id="DataPeriod" value="">
<input type="hidden" id="searchOrder" value="">
<input type="hidden" id="searchSort" value="">
  
 <div id="record_col_template" style='display:none'> 	
     <div class="table_board" id="all_reviews_temp">	
		<table class="common_table_list">
			<col nowrap="nowrap" width="5%"/>
			<col nowrap="nowrap" width="30%"/>
			<col nowrap="nowrap" width="30%"/>
			<col nowrap="nowrap" width="15%"/>
			<col nowrap="nowrap" width="15%"/>
			<tr>
			  <th class="num_check">#</th>
			  <th><a href="#" name="sort_bt" class="sort_column"><?=$eLib["html"]["book_title"]?></a></th>
			  <th><a href="#" name="sort_a" class="sort_column"><?=$eLib["html"]["author"]?></a></th>
			  <th><a href="#" name="sort_nor" class="sort_column"><?=$eLib["html"]["num_of_review"]?></a></th>
			  <th><a href="#" name="sort_ls" class="sort_column"><?=$eLib["html"]["last_submitted"]?></a></th>
			</tr>
		</table>
	</div>
 </div>                    
<p class="spacer"></p>
                
              