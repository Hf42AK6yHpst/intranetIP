<?
/*
 * 	Log
 * 
 * 	2016-09-27 [Cameron]
 * 		- display news in html format
 */
?>

<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>
$(function(){
    <? if (sizeof($portal_data['news'])>=$amount): ?>
    $('#portal_news .portal_news_content_detail').off('scroll').on('scroll',function(e){
	
	if ($(this).prop('scrollHeight')-$(this).scrollTop()-90<=$(this).height()){
	    
	    $.fancybox.showLoading();
	    $('#portal_news .portal_news_content_detail').off('scroll');
	    $.get('ajax.php?action=getPortalNotices&type=news', {offset:'<?=$offset+$amount?>'}, function(data){
		$.fancybox.hideLoading();
		
		$('#portal_news .portal_news_content_detail ul').append(data);
		 
	    });
	    
	}
    });
    <? endif; ?>
	
	$('.portal_news_content_detail').css("resize","none"); // 20131209-elibplus-announcement
			
    $('#portal_news .tool_edit').off('click').click(function(){
	    $.fancybox.open('ajax.php?action=getPortalNoticeForm&type=news&id='+$(this).find('.id').html(),{
		type: 'ajax',
		margin: 0,
		padding: 0
	    });
	    return false;
    });
    
    
});
   
</script>
<? foreach ($portal_data['news'] as $news_item): ?>
    <li id="portal_news_<?=$news_item['id']?>">                	
	<h1>
		<span class='title'><?=$news_item['title']?></span>
		<span class="date_time" title="<?=$news_item['modified']?>">
		<?=$news_item['not_started'] ? " [".$Lang['General']['Hide']."] " : ""?>
		<?=$news_item['expired'] ? " [".$Lang["ebook_license"]["expired"]."] " : ""?> 
		<?=$news_item['priority'] ? " [".$Lang['Btn']['ToTop']."] " : ""?>
		<?=elibrary_plus::getDaysWord($news_item['modified'])?></span>
	</h1> 
   
	<p class="spacer"></p>
	<div><?=nl2br(intranet_undo_htmlspecialchars($news_item['content']))?></div>
       
	<p class="spacer"></p>
	<div class="news_tool">
	<? if ($permission['admin']): ?>
	 <a href="#" class="tool_edit"><?=$button_edit?><span class="id" style="display:none"><?=$news_item['id']?></span></a>
	<? endif; ?>
	</div>
	<? if ($news_item['attachment']): ?>
	<span class="attachment"><?=$eLib_plus["html"]["attachment"]?>: <a href="<?=str_replace($news_item['attachment_name'], urlencode($news_item['attachment_name']), $news_item['attachment'])?>" target="_blank"><?=$news_item['attachment_name']?></a></span>
	<? endif; ?>
    </li>
    <p class="spacer"></p>
<? endforeach; ?>