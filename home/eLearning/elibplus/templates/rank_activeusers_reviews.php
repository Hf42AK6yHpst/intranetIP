<? if (!isset($rank_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>
$(function(){
    $('.ranking_user_review .like').off('click').click(function(e){
        
        $.post($(this).attr('href'), function(data){
                var like_data=data.split(',');
                $('#like_'+like_data[0]+" .icon_like").html(like_data[1]);
                $('#like_'+like_data[0]+" .like").html(like_data[2]);
        });
       
        return false;
    });
    <? if (sizeof($rank_data)>=$amount): ?>
    $('.ranking_user_review').off('scroll').on('scroll',function(){
	
	if ($('.ranking_user_review').prop('scrollHeight')-$('.ranking_user_review').scrollTop()-210<=$('.ranking_user_review').height()){
	    $('.ranking_user_review').off('scroll');
	    $.fancybox.showLoading();
	    $.get('ajax.php?action=getMostActiveUserReviews',{user_id: '<?=$user_id?>', offset: '<?=$offset+$amount?>'}, function(data){

		$('.ranking_user_review').append(data);
		$.fancybox.hideLoading();
		 
	    });
	}
	
    });
    <? endif; ?>
});
    
    
</script>
<? foreach ($rank_data as $review) :?>
    <li class="review_log" id="review_log_<?=$review['review_id']?>">
	<div class="review_book">
		<ul class="book_list book_list_small">
		    
		<?=elibrary_plus::getBookListView(array($review))?>
		
		</ul>
	</div>
	<div class="review_detail">
	    
	    <div class="star_s">
		<ul>
		<? for ($i=0; $i<5; $i++) : ?>
			      <li <?=$i<$review['rating']? 'class="star_on"': ''?>>
		<? endfor; ?>
		</ul>
	    </div>

	     <p class="spacer"></p>
	     <div class="review_content"><?=$review['content']?></div>
	     <p class="spacer"></p>
	    <div class="review_footer">
		<span class="date_time" title="<?=$review['date']?>"><?=elibrary_plus::getDaysWord($review['date'])?></span>
		<div class="tool_like" id="like_<?=$review['review_id']?>">
						
		    <a href="ajax.php?action=handleLike&review_id=<?=$review['review_id']?>&book_id=<?=$review['book_id']?>" class="like">
			    <?=($lelibplus->getHelpfulCheck($review['review_id'])) ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"] ?>
			
		    </a>
		    
		    <a class="icon_like"><?=$review['likes']?></a>
		</div>
	  </div>
	
	</div>
	<p class="spacer"></p>
    </li>
<? endforeach; ?>