<?php
/*
 * 	Log
 * 
 * 	Date:	2015-11-30 [Cameron]
 * 		- add afterShow() event to $('.book_detail').fancybox to adjust the height of book description
 * 
 */
?>
<? if (!isset($record_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script type="text/javascript">
$(document).ready(function() {
      
      $('.book_detail').fancybox({
		  autoDimensions : false,
		  autoScale : false,
		  autoSize : false,
		  width : 800,
		  height : 600,
		  type : 'ajax',
		  padding: 0,
		  nextEffect: 'fade',
		  prevEffect: 'fade' ,
          beforeClose: function(){
          	//loadRecord(location.hash, false);
          	},
			afterShow: function() {
				var isScroll = false;
				var baseline = 140;
		        if ($('#book_description').height() > baseline) {
		        	if ($('#book_info_summary').height() <= baseline) {
		        		$('#book_description').css('height',baseline);
		        		isScroll = true;
		        	}
		        	else {
		        		if ($('#book_info_summary').height() < $('#book_description').height()) {
		        			$('#book_description').css('height',$('#book_info_summary').height()-10);
		        			isScroll = true;
		        		}
		        	}
		        }
		        
		        if (isScroll == true) {
					$('#book_description').css('width','260px');
					$('#book_description').css('overflow-y','scroll');        	
					$('#book_description').css('overflow-x','hidden');
					var h = $('#book_description').height() - 15;
					$('.book_desc span').css('background-position','215px '+ h + 'px');
					
					$('#book_description').scroll(function (event) {
					    var scroll = $('#book_description').scrollTop();
					    var height = $('#book_description').height() - 15 + scroll;  
					    $('.book_desc span').css('background-position','215px '+ height + 'px');
					});
		        }
		        else {
		        	$('#book_description').css('width','240px');
		        	$('#book_description').css('overflow-y','hidden');
		        	$('#book_description').css('overflow-x','hidden');
		        }
	    	}    	
      });
      
      $('.record_menu li a').click(function(){
	  loadRecord($(this).attr('href'), true, 0);
      });
           
     
      <?if($plugin['eLib_Lite']){?>
      	if(location.hash == ""){
      		loadRecord("#eBookRecord", true, 0);      		
      	}else{      		
      		loadRecord(location.hash, true, 0);
      	}
      <?}else{?>
      		loadRecord(location.hash, true, 0);
      <?}?>
});

function loadRecord(href, animate, offset){
    
    if ($('.record_menu li a[href="'+href+'"]').length==0) href="#LoanRecord";
    var target=$(href);
    var tab_action=href.replace('#','get');
    
    $.fancybox.showLoading();
    if (animate) $('.record_content_detail').fadeOut();
    $.get('ajax.php',{action: tab_action, offset: offset}, function(data){

	$('.record_content_detail').html(data).fadeIn('slow');
	$('.record_content_detail  .record_detail').scrollTop(0);

	$.fancybox.hideLoading();
	 
    });

    $('a[href="'+href+'"]').parent().addClass('current_tab').siblings().removeClass('current_tab');
    
}
</script>

<div class="elib_content_board" id="record_board">
  
  <div class="elib_content_board_top"><div class="elib_content_board_top_right"><div class="elib_content_board_top_bg">
  </div></div></div>
	
    <div class="elib_content_board_content"><div class="elib_content_board_content_right"><div class="elib_content_board_content_bg">
    	<!---->
        
        	<div class="record_content">
            	<div class="record_content_menu">
                	<div class="record_user_detail">
                    	<div class="user_photo">
    	                	<span><img src="<?=$record_data['image']?>" /></span>
                            
	                    </div>
                        <h2><?=$record_data['name']?> <span><?=$record_data['class']?></span></h2>
                        <div class="record_menu">
                        	<ul>
                        		<? if (!$plugin['eLib_Lite']){?>
	                            	<li class="current_tab"><a href="#LoanRecord"><?=$eLib_plus["html"]["myloanbookrecord"]?></a></li>
	                                <li><a href="#eBookRecord"><?=$eLib_plus["html"]["ebookrecord"]?></a></li>
                            	<?}else{?>
	                                <li class="current_tab"><a href="#eBookRecord"><?=$eLib_plus["html"]["ebookrecord"]?></a></li>
                            	<?}?>
                                <li><a href="#FavouriteRecord"><?=$eLib_plus["html"]["myfavourite"]?></a></li>
                                <li><a href="#ReviewRecord"><?=$eLib_plus["html"]["myreview"]?></a></li>
                            </ul>
                        </div>
                    </div>
                
                
                </div>
              <div class="record_content_detail">
				
           	
              </div>
            
              <p class="spacer"></p>
            </div>
	  <p class="spacer"></p>
   		 <!---->
    </div></div></div>
    <div class="elib_content_board_bottom"><div class="elib_content_board_bottom_right"><div class="elib_content_board_bottom_bg"></div></div></div>
</div>
</div>
