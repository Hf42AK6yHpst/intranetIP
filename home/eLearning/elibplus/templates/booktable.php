<? if (!isset($books_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>

	<div class="table_board">	   
    <table class="common_table_list">
		    
	  <col nowrap="nowrap"/>
	  <thead>
	      <tr>
		  <th class="num_check">#</th>
		  <th><a href="#" class="sort_asc">Book Title</a></th>
		  <th>Author</th>
		  <th>Publisher</th>
		  <th>Hit Rate</th>
	      </tr>
	  </thead>

	  <tbody>
	    <? foreach($books_data as $num=>$book) :?>
	      <tr>
		    <td><?=$num+1?></td>
		    <td>
			<div class="ebook_title">
			      <? if (!empty($book['url'])) : ?>
			      <a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon_read_now" href="#" onclick="<?=$book['url']?>" ></a>
			      <? endif; ?>
			      <a rel="category_booklist" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_cover"><?=$book['title']?></a>
			</div>
		    </td>
		    <td><?=$book['author']?></td>
		    <td><?=$book['publisher']?></td>
		    <td><?=$book['hit_rate']?></td>
	      </tr>
	    <? endforeach; ?>
	  <tbody>			
      </table>
		     
</div>
<p class="spacer"></p>


