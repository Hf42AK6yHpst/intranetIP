<?
/*
 * 	Using:	Paul
 * 
 * 	Log
 * 	
 * Date: 	2015-12-09 [Paul]
 * 			- refine the UI for book to show a clear message that the book is expired before parent/student/teacher click on the book
 * 
 * 	Date:	2015-11-20 [Cameron]	
 * 			- apply htmlspecialchars() to title inside hyper-link for book title and author
 * 
 * 	Date:	2015-09-23 [Cameron]
 * 			- apply intranet_undo_htmlspecialchars() to parameter in elibrary_plus::getEllipsisString()
 */
?>
<? if (!isset($books)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
    <? if(!$books) : ?>
	<li class="no_cover<?=$showrank&&$i==0?' rank_01':''?>">
		<a class="book_cover">
		    <h1><?=$eLib_plus["html"]["norecord"]?></h1>
		    <em></em>
		</a>
	</li>
    <? endif;?>
    <? if(sizeof($books)>1) $rel = mt_rand(0, 1000000); ?>
    <script>
	if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {//handling hiding hover button function on ios device
	    $(".book_list li").off('touchstart').on('touchstart',function(){return true;}).parent().parent().off('touchstart').on('touchstart',function(){return true;});
	}
	
    </script>
    <? foreach ($books as $i=>$book) :?>
	<? if ($book['image']): ?>
	    <li <?=$showrank&&$i==0?'class="rank_01"':''?>>
		<a class="book_cover" title="<?=htmlspecialchars($book['title']).' - '.htmlspecialchars($book['author'])?>">
		    <img src="<?=$book['image']?>">
		       
	<? else: ?>
	    <? if ($book['type']=='expired'):?>
		    <li class="off_shelf">
		    	<a class="book_cover" title="<?=htmlspecialchars($book['title']).' - '.htmlspecialchars($book['author'])?>">
			    <h1><?=elibrary_plus::getEllipsisString(intranet_undo_htmlspecialchars($book['title']), 24)?></h1>
			    <em><?=elibrary_plus::getEllipsisString(intranet_undo_htmlspecialchars($book['author']), 24)?></em>	
	    <? else: ?>
		    <li class="no_cover<?=$showrank&&$i==0?' rank_01':''?>">
		    	<a class="book_cover" title="<?=htmlspecialchars($book['title']).' - '.htmlspecialchars($book['author'])?>">
			    <h1><?=elibrary_plus::getEllipsisString(intranet_undo_htmlspecialchars($book['title']), 24)?></h1>
			    <em><?=elibrary_plus::getEllipsisString(intranet_undo_htmlspecialchars($book['author']), 24)?></em>
	    <? endif; ?>
		

	<? endif; ?>
	
	    <? if ($book['type']=='html5'):?>
		<span class="icon_ebook_01"></span>
	    <? elseif ($book['type']=='flash'):?>
		<span class="icon_ebook_02"></span>
	    <? endif; ?>
		    
	    </a>
	    <? if ($book['type']=='expired'):?>
	    	<div class='off_shelf_tag'><?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?></div>
	    <? endif; ?>
		
	    <? if ($showrank&&$i==0): ?>
	    <div class="most_rank_book_info">
		<h1><?=$book['title']?></h1>
		<h2><?=$book['author']?></h2>
		
		<? if ($showrank=='review'): ?>
		    <div class="star_s"><ul>
		    <? for ($j=0; $j<5; $j++) : ?>
				  <li <?=$j<$book['rating']? 'class="star_on"': ''?>>
		    <? endfor; ?>
		    </ul></div>
		    <p class="spacer"></p>
		    <div class="rank_no_of_review"><?=$book['review_count']?> <?=$eLib_plus["html"]["reviews"]?></div>
		<? else: ?>
		    <div class="rank_no_of_hit"><?=$book['hit_rate']?$book['hit_rate'].' '.$eLib_plus["html"]['hits']:$book['loan_count'].' '.$eLib_plus["html"]['times']?></div>
		<? endif;?>
		
	    </div>
		
	    <? endif; ?>
	    
	    <? if ($showrank&&$i<3): ?>
		<div class="most_tag_<?=$i+1?>"></div>
	    <? endif; ?>
		
	    <? if ($book['type']=='html5'):?>
		<div class="book_over_btn">
		    <?=$eLib_plus["html"]["ebook"] ?> (<?=$eLib_plus["html"]["pcipad"]?>)
		    <a href="javascript:<?=$book['url']?>"><?=$eLib_plus["html"]["readnow"]?></a>
		    <a class='book_detail' title="<?=htmlspecialchars($book['title']).' - '.htmlspecialchars($book['author'])?>" rel="<?=$rel?>" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>"><?=$eLib_plus["html"]["bookdetail"] ?></a>
		</div>

	    <? elseif ($book['type']=='flash'):?>
		<div class="book_over_btn">
		    <?=$eLib_plus["html"]["ebook"] ?> (<?=$eLib_plus["html"]["pconly"]?>)
		    <a href="javascript:<?=$book['url']?>"><?=$eLib_plus["html"]["readnow"]?></a>
		    <a class='book_detail' title="<?=htmlspecialchars($book['title']).' - '.htmlspecialchars($book['author'])?>" rel="<?=$rel?>" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>"><?=$eLib_plus["html"]["bookdetail"] ?></a>
		</div>
	    
	    <? elseif ($book['type']=='physical'):?>
		<div class="book_over_btn"><?=$eLib_plus["html"]["book"]?>
		    <? if ($book['user_status']=='borrowed'): ?>
		    <span><?=$eLib_plus["html"]["youve"]?> <a href="./?page=record"><?=$eLib_plus["html"]["borrowed2"]?></a></span>
		    <? elseif ($book['user_status']=='reserved'): ?>
		    <span><?=$eLib_plus["html"]["youve"]?> <a href="./?page=record"><?=$eLib_plus["html"]["reserved"]?></a></span>
		    <? endif; ?>
		    
		    <a class='book_detail' title="<?=htmlspecialchars($book['title']).' - '.htmlspecialchars($book['author'])?>" rel="<?=$rel?>" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>"><?=$eLib_plus["html"]["bookdetail"] ?></a>
		</div>

		<? elseif ($book['type']=='expired'):?>
			<? if ($_GET['action'] != 'getBookDetail'): ?>
				<div class="book_over_btn"><?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>
					<a class='book_detail' title="<?=htmlspecialchars($book['title']).' - '.htmlspecialchars($book['author'])?>" rel="<?=$rel?>" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>"><?=$eLib_plus["html"]["bookdetail"] ?></a>
		    	</div>
	    	<? endif; ?>
	    <? endif; ?>
	    
	    <? if ($book['reason']): ?>
		<div class="most_recommend_reason">
		    
		    <h1><?=$book['title']?></h1>
		    <h2><?=$book['author']?></h2>
		   
		    <p class="spacer"></p>
		    
			<div class="most_recommend_reason_detail"><?=$book['reason']?></div>
		    
		</div>
	    <? endif; ?>
	</li>
    
    <? endforeach; ?>