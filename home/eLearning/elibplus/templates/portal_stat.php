<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script type="text/javascript">
$(document).ready(function() {
	
       $('.ranking_header_tab li a').click(function(){
	      var target=$(this).attr('href');
	      $(target).siblings().hide();
	      $(target).fadeIn();
	      
	      $(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
	      
	      return false;
       });
       
       $('.btn_back_arrow_s').click(function(){
		
		$('.ranking_slider').animate({'margin-left': 0});
		return false;
       });
       
       $('.btn_next_arrow_s').click(function(){
		
		$('.ranking_header .ranking_slider').animate({'margin-left': -165});
		$('.ranking>.ranking_container .ranking_slider').animate({'margin-left': -205});
		return false;
       });
});
</script>
<div class="ranking">
        	<div class="ranking_header">
            	<a href="#" class="btn_back_arrow_s"></a>
                <a href="#" class="btn_next_arrow_s"></a>
		
		<div class="ranking_container"><div class="ranking_slider">
				<div class="ranking_header_content">
						<h1><?=$eLib["html"]["most_active_reviewers"]?></h1>
						<p class="spacer"></p>
						<div class="ranking_header_tab">
					       
							<ul><li><a href="#active_classes"><?=$eLib["html"]["class"]?></a></li><li class="current_tab"><a href="#active_reviewers"><?=$eLib["html"]["student"]?></a></li><p class="spacer"></p></ul><p class="spacer"></p>
						</div>
				</div>
				<div class="ranking_header_content">
						<h1><?=$eLib_plus["html"]["mostactiveborrowers"]?></h1>
						<p class="spacer"></p>
						<div class="ranking_header_tab">
					       
						<ul><li class="current_tab"><a><?=$eLib["html"]["student"]?></a></li><p class="spacer"></p></ul><p class="spacer"></p>
						</div>
				</div>
		</div></div>
		
		</div>
		<div class="ranking_container"><div class="ranking_slider">
				<div class="ranking_content">
				<div class="ranking_list" id="active_reviewers">
				
				    <ul>
					<? if (empty($portal_data['active_reviewers']['students'])): ?>
						<li><?=$eLib_plus["html"]["norecord"] ?></li>
					<? else: ?>
						<? foreach ($portal_data['active_reviewers']['students'] as $num=>$student) :?>
						
						<li class="rank_<?=$num+1?>"> <span><?=$num+1?></span>
						       <div><?=$student['name']?><u><?=$student['class']?>  <em><?=$student['review_count']?> <?=$eLib["html"]["reviewsUnit"].$eLib_plus["html"]["reviews"]?></em></u></div>
						</li>
						
						<? endforeach; ?>
					<? endif; ?>
				    </ul>
				<a href="./?page=ranking&range=<?=$range?>#MostActiveUsers" class="btn_more"><?=$eLib_plus["html"]["more"]?>..</a>
				
				</div>
				
				<div class="ranking_list" id="active_classes" style="display:none">
				
				    <ul>
					<? if (empty($portal_data['active_reviewers']['classes'])): ?>
						<li><?=$eLib_plus["html"]["norecord"] ?></li>
					<? else: ?>
						<? foreach ($portal_data['active_reviewers']['classes'] as $num=>$class) :?>
						
						<li class="rank_<?=$num+1?>"> <span><?=$num+1?></span>
						       <div><?=$class['class']?><u><em><?=$class['review_count']?> <?=$eLib["html"]["reviewsUnit"].$eLib_plus["html"]["reviews"]?></em></u></div>
						</li>
						
						<? endforeach; ?>
					<? endif; ?>
				    </ul>
				<a href="./?page=ranking&range=<?=$range?>#MostActiveClasses" class="btn_more"><?=$eLib_plus["html"]["more"]?>..</a>
				
				</div>
				</div>
				<div class="ranking_content">
				<p class="spacer"></p>
				
				<div class="ranking_list" id="most_borrowers">
				
				    <ul>
					<? if (empty($portal_data['most_borrowers'])): ?>
						<li><?=$eLib_plus["html"]["norecord"] ?></li>
					<? else: ?>
						<? foreach ($portal_data['most_borrowers'] as $num=>$student) :?>
						
						<li class="rank_<?=$num+1?>"> <span><?=$num+1?></span>
						       <div><?=$student['name']?><u><?=$student['class']?>  <em><?=$student['loan_count']?> <?=$eLib_plus["html"]["borrows_unit"]?></em></u></div>
						</li>
						
						<? endforeach; ?>
					<? endif; ?>
				    </ul>
				<a href="./?page=ranking&range=<?=$range?>#MostBorrowUsers" class="btn_more"><?=$eLib_plus["html"]["more"]?>..</a>
				</div>
				</div>
		</div>
		</div>
            
        </div>	

		<div class="bookshelf_big"><div class="bookshelf_big_right"><div class="bookshelf_big_bg"> 
            	
            	<div class="bookshelf_big_content">
				
		<? if (!empty($portal_data['most_loan']) && $portal_data['selected_booktype'] != 'type_pc' && $portal_data['selected_booktype'] != 'type_ipad') :?>
                	
			<div class="most_rank_all" id="most_loan" >
                   	  <div class="most_rank_top">
                   		<div class="most_rank_top_detail">
						
                                <div class="most_rank_detail"><div class="most_rank_detail_right"><div class="most_rank_detail_bg"><div class="most_rank_detail_info">
                                <?=$portal_data['most_loan'][0]['loan_count']?><span> <?=$eLib_plus["html"]["times"] ?></span>
                                
                                </div></div></div></div>
				</div>
                          <div class="most_rank_first">
                               
                                  <ul class="book_list"> 
					  <li class="temp"> </li>
					  
						 <? if (empty($portal_data['most_loan'][0]['image'])) : ?>
						 
							<li class="no_cover"> 
							<a rel="portal_most_loan" href="ajax.php?action=getBookDetail&book_id=<?=$portal_data['most_loan'][0]['id']?>" class="book_cover">
							       
							<h1><?=$portal_data['most_loan'][0]['title']?></h1><span><?=$portal_data['most_loan'][0]['author']?></span>
						     
						 <? else: ?>
						 
							<li> 
							<a rel="portal_most_loan" href="ajax.php?action=getBookDetail&book_id=<?=$portal_data['most_loan'][0]['id']?>" class="book_cover">
						     
							<img src="<?=$portal_data['most_loan'][0]['image']?>" title="<?=$portal_data['most_loan'][0]['title']." (".$portal_data['most_loan'][0]['author'].')'?>"/>


						 <? endif; ?>
						 
						 </a>
						 
		
						 <? if (!empty($portal_data['most_loan'][0]['url'])) : ?>
							<a href="javascript:<?=$portal_data['most_loan'][0]['url']?>" class="icon_ebook"><span><?=$eLib_plus["html"]["readnow"]?></span></a>
						 <? endif; ?>
						 <div class="most_tag_1"></div>
					  </li>
				  </ul>
                               	 
			    </div>
                        
                      </div>
                        
			    <div class="most_rank_list">
                        	
				   <ul class="book_list"> 
					  <li class="temp"> </li>
					  
					  <? foreach (array_slice($portal_data['most_loan'],1) as $num=>$book): ?>
						 
						 <? if (empty($book['image'])) : ?>
						 
							<li class="no_cover"> 
							<a rel="portal_most_loan" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_cover">
							       
							<h1><?=$book['title']?></h1><span><?=$book['author']?></span>
						     
						 <? else: ?>
						 
							<li> 
							<a rel="portal_most_loan" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_cover">
						     
							<img src="<?=$book['image']?>"  title="<?=$book['title']." (".$book['author'].')'?>"/>
							
						 <? endif; ?>
							
						 </a>
						 
						 <? if (!empty($book['url'])) : ?>
						 <a href="javascript:<?=$book['url']?>" class="icon_ebook"><span><?=$eLib_plus["html"]["readnow"]?></span></a>
						 <? endif; ?>
						 
						 <div class="most_tag_<?=$num+2?>"></div>
					  </li>
					  <? endforeach; ?>
					  
					  
				   </ul>
                           
			    </div>
                        <a href="./?page=ranking#mostloan" class="btn_more"><?=$eLib_plus["html"]["more"] ?>..</a>
                    
                    </div>
		
		<? endif; ?>
		<? if (!empty($portal_data['best_rate'])) :?>
			      
                	<div class="most_rank_all" id="most_rate" >
                   	  <div class="most_rank_top">
                   			 <div class="most_rank_top_detail">
                                <div class="most_rank_detail"><div class="most_rank_detail_right"><div class="most_rank_detail_bg"><div class="most_rank_detail_info">
                                <span><?=$portal_data['best_rate'][0]['review_count'].' '.$eLib["html"]["reviewsUnit"].$eLib_plus["html"]["reviews"] ?></span>
                                <div class="star_s">
                                	<ul>
					  <? for ($i=0; $i<5; $i++) : ?>
						 <li <?=$i<$portal_data['best_rate'][0]['rating']? 'class="star_on"': ''?>>
					  <? endfor; ?>
					
					</ul>
                                </div>
                                
                                </div></div></div></div>
                        </div>
                            <div class="most_rank_first">
                               
                                  <ul class="book_list"> 
					  <li class="temp"> </li>
					  
						 <? if (empty($portal_data['best_rate'][0]['image'])) : ?>
						 
							<li class="no_cover"> 
							<a rel="portal_best_rate" href="ajax.php?action=getBookDetail&book_id=<?=$portal_data['best_rate'][0]['id']?>" class="book_cover">
							       
							<h1><?=$portal_data['best_rate'][0]['title']?></h1><span><?=$portal_data['best_rate'][0]['author']?></span>
						     
						 <? else: ?>
						 
							<li> 
							<a rel="portal_best_rate" href="ajax.php?action=getBookDetail&book_id=<?=$portal_data['best_rate'][0]['id']?>" class="book_cover">
						     
							<img src="<?=$portal_data['best_rate'][0]['image']?>" title="<?=$portal_data['best_rate'][0]['title']." (".$portal_data['best_rate'][0]['author'].')'?>"/>

						 <? endif; ?>
       	
						  </a>
						 <? if (!empty($portal_data['best_rate'][0]['url'])) : ?>						 
						  <a href="javascript:<?=$portal_data['best_rate'][0]['url']?>" class="icon_ebook"><span><?=$eLib_plus["html"]["readnow"]?></span></a>
						 <? endif; ?>
						  
						  <div class="most_tag_1"></div>
					  </li>
				  </ul>
                               	 
			    </div>
                        
                      </div>
                        
			    <div class="most_rank_list">
                        	
				   <ul class="book_list"> 
					  <li class="temp"> </li>
					  
					  <? foreach (array_slice($portal_data['best_rate'],1) as $num=>$book): ?>
						 
						 <? if (empty($book['image'])) : ?>
						 
							<li class="no_cover"> 
							<a rel="portal_best_rate" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_cover">
							       
							<h1><?=$book['title']?></h1><span><?=$book['author']?></span>
						     
						 <? else: ?>
						 
							<li> 
							<a rel="portal_best_rate" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_cover">
						     
							<img src="<?=$book['image']?>"  title="<?=$book['title']." (".$book['author'].')'?>"/>
						 
						 <? endif; ?>

						 </a>
						 
						 <? if (!empty($book['url'])) : ?>	
						 <a href="javascript:<?=$book['url']?>" class="icon_ebook"><span><?=$eLib_plus["html"]["readnow"]?></span></a>
						 <? endif; ?>
						 
						 <div class="most_tag_<?=$num+2?>"></div>
					  </li>
					  <? endforeach; ?>
					  
					  
				   </ul>
                           
			    </div>
                        <a href="./?page=ranking#bestreview" class="btn_more"><?=$eLib_plus["html"]["more"] ?>..</a>
                    
                    </div>
		
                <? endif; ?>
		<? if (!empty($portal_data['most_hit'])) :?>
		
                	<div class="most_rank_all" id="most_hit" >
                   	  <div class="most_rank_top">
                   			 <div class="most_rank_top_detail">
                                <div class="most_rank_detail"><div class="most_rank_detail_right"><div class="most_rank_detail_bg"><div class="most_rank_detail_info">
                                <?=$portal_data['most_hit'][0]['hit_rate']?>  <span><?=$eLib_plus["html"]["times"] ?></span>
                                
                                </div></div></div></div>
                        </div>
			    <div class="most_rank_first">
                               
                                  <ul class="book_list"> 
					  <li class="temp"> </li>
					  
						 <? if (empty($portal_data['most_hit'][0]['image'])) : ?>
						 
							<li class="no_cover"> 
							<a rel="portal_most_hit" href="ajax.php?action=getBookDetail&book_id=<?=$portal_data['most_hit'][0]['id']?>" class="book_cover">
							       
							<h1><?=$portal_data['most_hit'][0]['title']?></h1><span><?=$portal_data['most_hit'][0]['author']?></span>
						     
						 <? else: ?>
						 
							<li> 
							<a rel="portal_most_hit" href="ajax.php?action=getBookDetail&book_id=<?=$portal_data['most_hit'][0]['id']?>" class="book_cover">
						     
							<img src="<?=$portal_data['most_hit'][0]['image']?>" title="<?=$portal_data['most_hit'][0]['title']." (".$portal_data['most_hit'][0]['author'].')'?>"/>
						 
						 <? endif; ?>
							
						 </a>
						 
						 <? if (!empty($portal_data['most_hit'][0]['url'])) : ?>	
						  <a href="javascript:<?=$portal_data['most_hit'][0]['url']?>" class="icon_ebook"><span><?=$eLib_plus["html"]["readnow"]?></span></a>
						 <? endif; ?>
						  <div class="most_tag_1"></div>
					  </li>
				  </ul>
                               	 
			    </div>
                        
                      </div>
                        
			    <div class="most_rank_list">
                        	
				   <ul class="book_list"> 
					  <li class="temp"> </li>
					  
					  <? foreach (array_slice($portal_data['most_hit'],1) as $num=>$book): ?>
						 
						 <? if (empty($book['image'])) : ?>
						 
							<li class="no_cover"> 
							<a rel="portal_most_hit" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_cover">
							       
							<h1><?=$book['title']?></h1><span><?=$book['author']?></span>
						     
						 <? else: ?>
						 
							<li> 
							<a rel="portal_most_hit" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_cover">
						     
							<img src="<?=$book['image']?>"  title="<?=$book['title']." (".$book['author'].')'?>"/>
														
						 <? endif; ?>
							
						  </a>
						 
						 <? if (!empty($book['url'])) : ?>	
						 <a href="javascript:<?=$book['url']?>" class="icon_ebook"><span><?=$eLib_plus["html"]["readnow"]?></span></a>
						 <? endif; ?>
						 
						 <div class="most_tag_<?=$num+2?>"></div>
					  </li>
					  <? endforeach; ?>
					  
					  
				   </ul>
                           
			    </div>
                        <a href="./?page=ranking#mosthit" class="btn_more"><?=$eLib_plus["html"]["more"] ?>..</a>
                    
                    </div>
                </div>
 
	 </div></div></div>
 
		<? endif; ?>
			    
