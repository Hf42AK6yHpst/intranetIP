<? if (!isset($ebook_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>
$(function(){
	
	$('.record_tab li a').click(function(){
		
		
		var target=$(this).attr('href');
		$(target).siblings().fadeOut();
		$.fancybox.showLoading();
		$.get('ajax.php?action=get'+target.replace('#',''),{offset: 0}, function(data){
		    //$(target).fadeIn().find('table').append(data);
		    $(target).fadeIn().find('table').html(data);
		    $('.record_detail').scrollTop(0);
		    $.fancybox.hideLoading();
		});
		;
		$(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
		
		return false;
	});
	
	$('.record_tab li.current_tab a').click();
});	
</script>

<div class="record_tab">
	<ul>
	<li class="current_tab"><a href="#eBookReadRecord"><span><?=$eLib["html"]["my_reading_history"] ?></span></a></li>
	<li><a href="#eBookNotesRecord"><span><?=$eLib["html"]["my_notes"]?></span></a></li>
    </ul>
</div>
<div class="record_detail">
   <div id="eBookReadRecord" style='display:none'>
      <div class="table_board" >
	
	<table class="common_table_list common_table_notes_list">
		<col nowrap="nowrap" width="5%"/>
		<col nowrap="nowrap" width="30%"/>
		<col nowrap="nowrap" width="30%"/>
		<col nowrap="nowrap" width="15%"/>
		<col nowrap="nowrap" width="15%"/>
			<tr>
			  <th class="num_check">#</th>
			  <th><?=$eLib["html"]["book_title"]?></th>
			  <th><?=$eLib["html"]["author"]?></th>
			  <th><?=$eLib["html"]["Progress"]?></th>
			  <th><?=$eLib["html"]["last_read"]?></th>
			</tr>
			

			
	</table>

	</div>
   </div>
    <div id='eBookNotesRecord' style='display:none'>
	<div class="table_board" >

	<table class="common_table_list common_table_read_list">
		<col nowrap="nowrap" width="5%"/>
		<col nowrap="nowrap" width="30%"/>
		<col nowrap="nowrap" width="35%"/>
		<col nowrap="nowrap" width="10%"/>
		<col nowrap="nowrap" width="15%"/>
		
			<tr>
			  <th class="num_check">#</th>
			  <th><?=$eLib["html"]["book_title"]?></th>
			  <th><?=$eLib["html"]["notes_content"]?></th>
			  <th><?=$eLib["html"]["category"] ?></th>
			  <th><?=$eLib["html"]["last_modified"] ?></th>
			</tr>
			
			
			
	</table>

	</div>
    </div>

</div>
                              
<p class="spacer"></p>
                
              