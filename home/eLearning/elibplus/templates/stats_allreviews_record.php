<?php
/*
 * 	using: Paul
 * 	Log
 * 
 *  Date:	2015-12-16 [Paul] [ip.2.5.7.1.1]
 * 			- modify the UI for expire books display
 * 
 * 	Date:	2015-08-11 [Cameron]	
 * 			- fix bug on showing icon_ebook_s.png for eBook only
 * 
 * 	Date:	2015-04-28 [Cameron]
 * 			- add arguments searchFromDate and searchToDate to getAllReviewsRecord()
 */
?>
<? if (!isset($ebook_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? if (!$ebook_data['reading']&&$offset==0): ?>
    <tr><td colspan="5"><?=$eLib_plus["html"]["norecord"]?></td></tr>
<? else: ?>
<tr style="display:none;">
    <script>
    
	<? if (sizeof($ebook_data['reading'])>=$amount): ?>
	$('.record_detail').off('scroll').on('scroll',function(e){
	    
	    if ($(this).prop('scrollHeight')-$(this).scrollTop()-150<=$(this).height()){
			var filter = 'searchDataPeriod=' + $("input:radio[name='searchDataPeriod']:checked").val() + 'searchFromDate=' + $('#searchFromDate').val() + '&searchToDate=' + $('#searchToDate').val(); 
			var sort =$("#searchSort").val();
			var order = $("#searchOrder").val();
		
			$.fancybox.showLoading();
			$('.record_detail').off('scroll');
			$.get('ajax.php?action=getAllReviewsRecord&'+filter, {offset:'<?=$offset+$amount?>', order : order, sort : sort}, function(data){
			    $.fancybox.hideLoading();
			    $('.record_detail table').append(data);		     
			});
		
	    }
	});
	<? endif; ?>
	
	$('.sort_column').click(function(){		
		$.fancybox.showLoading();
		
		var sort = $(this).attr('name');
		var order = $(this).attr('class').replace("sort_column", "").replace(" ", "");
		
		if(order == ""|| order == "sort_asc") order = "sort_dec";
		else order = "sort_asc";
		
		var searchDataPeriod	= $("input:radio[name='searchDataPeriod']:checked").val();
		var searchFromDate 		= $('#searchFromDate').val();
		var searchToDate 		= $('#searchToDate').val();

		$("#DataPeriod").val($("input:radio[name='searchDataPeriod']:checked").val());
		$("#FromDate").val($("#searchFromDate").val());
		$("#ToDate").val($("#searchToDate").val());
		$("#searchSort").val(sort);
		$("#searchOrder").val(order);
		
		$.get('ajax.php?action=getAllReviewsRecord',{offset: 0, order : order, sort : sort, searchDataPeriod : searchDataPeriod, searchFromDate : searchFromDate, searchToDate : searchToDate}, function(data){	  
		    $.fancybox.hideLoading();
			$('.sort_column').attr("class","sort_column");
		     $('.record_detail table').html($("#all_reviews_temp").html());
		    $('.record_detail table').append(data);
		    $('.record_detail').scrollTop(0);
		});
		
		return false;
	});
	
	$(function(){
		$('[name="<?=$sort?>"]').attr("class", "sort_column <?=$order?>");
	});		
		
    </script>
</tr>
<? foreach ($ebook_data['reading'] as $num=>$progress) : ?>
    <tr>
	<td><?=$offset+$num+1?></td>
	<td><div class="ebook_title">
<? if ($progress['BookFormat'] != 'physical' && $progress['readable']): ?>
	<a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon_read_now" href="javascript:<?=$progress['url']?>" ></a>
<? elseif(!$progress['readable']): ?>
	<a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon_read_now" href="#" ></a>
<? endif; ?>
	<? if($progress['readable']): ?>	
		<a rel="reading_ebooks" href="ajax.php?action=getBookDetail&book_id=<?=$progress['id']?>" class="book_title book_detail"><?=$progress['title']?></a>
	<? else: ?>
		<a rel="reading_ebooks" href="ajax.php?action=getBookDetail&book_id=<?=$progress['id']?>" class="book_title book_detail off_shelf_list"><?=$progress['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a>
	<? endif; ?>
	</div></td>
	<? if($progress['readable']): ?>
		<td><?=$progress['author']?></td>
		<td><?=$progress['num_review']?></td>
		<td><?=$progress['last_submitted']?></td>
	<? else: ?>
		<td class="off_shelf_list"><?=$progress['author']?></td>
		<td class="off_shelf_list"><?=$progress['num_review']?></td>
		<td class="off_shelf_list"><?=$progress['last_submitted']?></td>
	<? endif; ?>
    </tr>
    <? endforeach; ?>
<? endif; ?>