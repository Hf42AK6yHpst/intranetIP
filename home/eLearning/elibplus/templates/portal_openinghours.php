<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<div class="portal_news_content portal_news_content_<?=$type?>">
    <div class="portal_news_content_title">Opening Hours<div class="news_tool">

	<a href="/home/library_sys/admin/settings/open_time.php" target="_blank" class="tool_manage">Manage</a>

    </div></div>
    <div class="portal_news_content_detail">
   
	<p class="spacer"></p>
	<ul>
	<? foreach ($portal_data['announcements'] as $announcement): ?>
	    <li>                	
		<h1><?=$announcement['title']?><span class="date_time"><?=$announcement['days']?></span></h1> 
	   
		<p class="spacer"></p>
		<?=$announcement['content']?>
	       
		<p class="spacer"></p>
		<div class="news_tool">
	     
		 <a href="news_list_new.htm" class="tool_edit">Edit</a>
	     
		</div>
	    </li>
	    <p class="spacer"></p>
	<? endforeach; ?>
	</ul>
	<p class="spacer"></p>

    </div>
     
</div>