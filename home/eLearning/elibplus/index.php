<?
//modifying By         

/********************** Change Log ***********************/
#
#	Date	:	2016-01-13	(Cameron)
#				add customize flag $sys_custom['eLibraryPlus']['HideBookCategoryTags'] to hide book category tags (case #U91042)
#
#	Date	:	2016-01-06	(Cameron)
#				replace session_register with session_register_intranet to support php5.4+
#
#	Date	:	2015-11-25 	(Cameron)
#				include libms.lang.$intranet_session_language.php because of adding $Lang["libms"]["portal"]["book_total"] 
#
#	Date	: 	2015-11-23 	(Cameron)
#				replace "ltltgtgt" back to "<>" for keyword in search 				
#
#	Date	: 	2015-10-23  (Cameron)
#				add $category_limit to avoid heavy loading of book category in front page
#
#	Date	: 	2015-10-16 	(Cameron)
#				call check_if_ebook_exists() and pass it to portal, don't show most hit book if there's no ebook 
#
#	Date	:	2015-09-22	(Cameron)
#				apply encodeURIComponent() to loadBookList()	
#
#	Date	: 	2015-09-16 	(Cameron)
#				fix bug on keyword search / advance search by special characters (apostrophe, double quotes and ampersand)
#
#	Date	:	2015-02-11	(Henry) 
#				add subtitle to function loadBookList()
#	Date	:	2014-11-05	(Charles Ma) 20141105-emag-provider [ej.5.0.4.11.1]
#				special redirect for emag user account 
/******************* End Of Change Log *******************/

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);


?>

<? if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']): ?>
    
<div class="elib_content">
    <div id="container">
	<div class="module_title module_<?=$intranet_session_language=='b5'?'chn':'en'?>">
	    <div class="module_title_icon"><a title="eLibrary plus" href="./">&nbsp;</a></div>
	</div>
	<?=$eLib_plus["html"]["closing"]?>
    </div>
</div>
    
<? die(); endif; ?>

<?

$lelibplus = new elibrary_plus($book_id, $UserID, $ck_eLib_bookType, $range);


$CustomDocType 			= "<!DOCTYPE HTML>";
$CurrentPageArr['eLibPLus'] 	= true;

$linterface = new interface_html("elibplus.html");
$linterface->LAYOUT_START();

$category_limit = 500;
$book_catalog		= $lelibplus->getBookCatrgories($category_limit);

if (!$sys_custom['eLibraryPlus']['HideBookCategoryTags']) {
	$book_catalog_tags	= $lelibplus->getBookTags();
}

$book_catalog_levels 	= $lelibplus->getBookLevels();
$notification_count 	= $lelibplus->getNotificationCount();
$theme 			= $lelibplus->getUserTheme();

switch($page){

  	case 'stats': 	//	2013-09-27 Charles Ma
  		
  		$ranking_data = true;
	
		$main_template = "templates/stats.php";
  	break;
  	
    case 'recommend':
	
        $category_data['selected_category'] 	= $eLib_plus["html"]["recommend"];
        $category_data['ajax_action']	 	= 'getRecommendList';
        
        $category_options=array(
            'no_sorting'	=> true,
            'no_showmore'	=> true,
        );
	
        $main_template = "templates/category.php";
        
    break;

    case 'new':
	
        $category_data['selected_category'] 	= $eLib_plus["html"]["new"];
        $category_data['ajax_action'] 		= 'getNewList';
        
        $category_options=array(
            'no_sorting'	=> true,
            'no_showmore'	=> true,
        );
	
        $main_template = "templates/category.php";
        
    break;

    case 'search':
	
	if ($categories){
		$categories= str_replace("&amp;","&",$categories);
	    
	    $category_array = $categories==',,'?array():explode(',',$categories);
	    
	    list($language, $category, $subcategory) = $category_array;
	    
	    $category_selected_name 	= implode(' -> ',array_slice($category_array,1));
	    $tag_selected_name 		= $lelibplus->returnTagNameByID($tag_id);
	}
	$keyword = str_replace("ltltgtgt","<>",$keyword);
	
	$title = trim($title);
	$subtitle = trim($subtitle);
	$author = trim($author);
	$level= str_replace("&amp;","&",$level);
	$tag_id= str_replace("&amp;","&",$tag_id);
      
	$keyword = trim($keyword);
    
	$category_data['selected_category'] 	= $eLib["html"]["search_result"];
        $category_data['ajax_action']	 	= 'getBookList';
	
	$main_template = "templates/category.php";
	
    break;

    case 'tag':
        
        $tag_id = (int)$tag_id;
	
	$category_data['selected_category'] 	= $eLib_plus["html"]["tag"].': '.$lelibplus->returnTagNameByID($tag_id);
        $category_data['ajax_action'] 		= 'getBookList';
	
	$main_template = "templates/category.php";
    break;

    case 'category':
	
	$ck_eLib_encryptsalt = md5(microtime());
	session_register_intranet('ck_eLib_encryptsalt',$ck_eLib_encryptsalt);//for random book list

	$selected_language = isset($language) ? $language : $selected_language;//default tab
	$category_data['selected_category'] 	= isset($category) ? $category : $eLib_plus["html"]["allbookcategories"];
	$category_data['selected_category']    .= isset($subcategory) ? ' -> '.$subcategory : "";
        $category_data['ajax_action'] 		= 'getBookList';

	$main_template = "templates/category.php";
	
    break;

    case 'ranking':
	$ranking_data = true;	
	$lelibplus->set_filter_ranking($sd,$ed,$ay);	//	2013-11-14 Charles Ma 2013-11-14-filterranking
	$main_template = "templates/ranking.php";
      
    break;
  
    case 'record':
	
	list($record_data['name'],$record_data['class'],$record_data['image'])=$lelibplus->getUserInfo();
	
	$main_template = "templates/record.php";
	
    break;

    default:
    	
	
	$timestamp = time();
	list($open_date, $end_date) = $lelibplus->getLibraryOpenEndDates();
	
	if ($open_date && $timestamp < strtotime($open_date)){
	    
	    $portal_data['open_days_replace_message'] = sprintf($eLib_plus["html"]["librarywillopenedon"], $open_date);
	    
	}else if($end_date && $timestamp > strtotime($end_date)){
	    
	    $portal_data['open_days_replace_message'] = sprintf($eLib_plus["html"]["libraryclosedsince"], $end_date);
	    
	    
	}else{
	    
	    $opening_days 		= $lelibplus->getLibraryOpeningHours();
	    $holidays 		= $lelibplus->getLibraryHolidays();
	    $special_days 		= $lelibplus->getLibrarySpecialOpenDays();
	    
	    $portal_data['selected_booktype'] 	= $ck_eLib_bookType;
	    $portal_data['opening_days'] 		= array();	   
    
	    for ($i = 0; $i<7; $i++, $timestamp += 86400){
		
		$date		= date('Y-m-d', $timestamp);
		$weekday		= date('l', $timestamp);
		$description	= '';
		
		foreach ($opening_days as $opening_day){
		    
		    if ($weekday == $opening_day['Weekday']){
			$is_open 		= $opening_day['Open'];
			$open 		= $opening_day['OpenTime'];
			$close 		= $opening_day['CloseTime'];
		    }
		    
		}
		foreach ($holidays as $holiday){
		    
		    if ($timestamp >= $holiday['start'] && $timestamp < $holiday['end']){
			$is_open 		= 0;
			$description 	= $holiday['event'];
		    }
		    
		}
		foreach ($special_days as $special_day){
		    
		    if ($timestamp >= $special_day['start'] && $timestamp < $special_day['end']){
			$is_open 		= $special_day['Open'];
			$open 		= $special_day['OpenTime'];
			$close 		= $special_day['CloseTime'];
			$description 	= $eLib_plus["html"]["specialtime"];
		    }
		    
		}
		
		$open_timestamp 	= strtotime($date.' '.$open);
		$close_timestamp 	= strtotime($date.' '.$close);
		
		$portal_data['opening_days'][] = array(
		    
		    'weekday'		=> $eLib_plus["html"]["weekday"][$weekday],
		    'open'			=> $is_open ? date('g:i a', $open_timestamp)  : 0,
		    'close'			=> $is_open ? date('g:i a', $close_timestamp) : 0,
		    'is_open'		=> $is_open,
		    'month'			=> date('M', $timestamp),
		    'day'			=> date('j', $timestamp),
		    'description'		=> $description
    
		);
		
		if ($i == 0) $portal_data['now_opening_status'] = $is_open && $timestamp >= $open_timestamp && $timestamp < $close_timestamp ? $eLib_plus["html"]["opening"] : $eLib_plus["html"]["closing"];
	    }
	    
	}
	
	$elib_install=new elibrary_install();
	$ebook_exist = $elib_install->check_if_ebook_exists();

	$main_template = "templates/portal.php";
	
    break;
}
?>
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.mouse.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.draggable.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.fancybox.pack.js">	</script>
<script type="text/javascript" src="/templates/jquery/jquery.datepick.js"></script>

<link rel="stylesheet" type="text/css" href="/templates/jquery/jquery.datepick.css" >
<link rel="stylesheet" type="text/css" href="/templates/jquery/jquery.fancybox.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<link href="/templates/<?=$LAYOUT_SKIN?>/css/elibplus.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" >

$(document).ready(function() {
    <? if($special_feature['emagUserAccountLimit'][$UserID]){ // 20141105-emag-provider?>
    	setTimeout(function(){
    		redirect_to_emag_site();
    		$(".fancybox-overlay").append("<div id='fancybox-block' style='width: 100%;height: 100%;'></div>");
    		$(".fancybox-close").toggleClass("fancybox-close");
    		$(document).bind('DOMNodeInserted', function(e) {
			    var element = e.target;
			    if($(element).attr("class") == "fancybox-item fancybox-close"){
			    	$(element).css("display","none");
			    }
			});
    	},1000);
    <?}?>
    $('#type_all, #type_physical, #type_pc, #type_ipad').click(function(){
        $.fancybox.showLoading();
        $.post('ajax.php?action=setBookType', {book_type: $(this).attr('id')}, function(){
            location.reload();
        });
        return false;
    });
    
       
    $(".dropdown dt a").click(function() {
           $(".dropdown dd ul").not($(this).closest(".dropdown").find("dd ul").slideToggle('fast')).hide();
           return false;
    });
    <? if ($page=='search'): ?>
    $('.search').submit(function(){
	 
	loadBookList(true, {
	    title: '',
	    subtitle: '',
	    author: '',
	    tag_id: '',
	    level: '',
	    sortby: '',
	    language: '',
	    category: '',
	    subcategory: '',
	    keyword: encodeURIComponent($(this).find('input[name="keyword"]').val()),
	    offset: 0
	    
	});
	return false;

    });
    
    $('.advancesearch').submit(function(){
	
	var category_array = $(this).find('input[name="categories"]').val().split(',');
	
	loadBookList(true, {
	    title: encodeURIComponent($(this).find('input[name="title"]').val()),
	    subtitle: encodeURIComponent($(this).find('input[name="subtitle"]').val()),
	    author: encodeURIComponent($(this).find('input[name="author"]').val()),
	    tag_id: encodeURIComponent($(this).find('input[name="tag_id"]').val()),
	    level: encodeURIComponent($(this).find('input[name="level"]').val()),
	    sortby: $(this).find('input[name="sortby"]').val(),
	    language: category_array[0],
	    category: encodeURIComponent(category_array[1]),
	    subcategory: encodeURIComponent(category_array[2]),
	    keyword: '',
	    offset: 0
	    
	})
	return false;

    })
    <? endif; ?>
    
    $('.show_advancesearch').click(function(){
	$('.advancesearch').fadeToggle();
    });
	
    $(document).click(function(e) {
           var clicked = $(e.target);
	  
           if (! clicked.parents().hasClass("dropdown"))
               $(".dropdown dd ul").slideUp('fast');
    });
   
    $('.setting_theme').click(function(){
	$.fancybox.open('ajax.php?action=getThemeSettings',{type: 'ajax', padding: 0, margin: 0, autoSize: false, width: 720 });
	return false;
    })
    $('.menu_icon_cat').click(function(){
	
	if ($('.book_cat_filter_board').css('opacity')<1){
	    $('.book_cat_filter_board').css({left:5, opacity:1});
	}else{
	    $('.book_cat_filter_board').css({left:-520, opacity:0});
	}
	
	return false;
    });
    


    $(document).keyup(function(e){var r=[83,85,76,80,66,73,76,69];if(typeof(k)=='undefined') k=r;if(e.keyCode==k[k.length-1] && $(':text:focus, textarea:focus').length==0) k.pop();else k=r;if(k.length==0){$.fancybox('<img style="max-width:100px;float:left" src="http://7.mshcdn.com/wp-content/uploads/2012/10/dancing_banana.gif"/><div style="float:right"><h2>eLibrary Plus</h2>by mickchiu@broadlearning 2012</div>');k=r;}});

    $('body').addClass('<?=$theme['background_theme']?> <?=$theme['bookshelf_theme']?>');
    document.title += ' > eLibrary Plus';

});
</script>
<div class="elib_content">
    <div id="container">
	<div id="seasonal_deco_00a" class="seasonal_deco"></div>
	<div id="seasonal_deco_00b" class="seasonal_deco"></div>
	<div class="module_title module_<?=($plugin['eLib_Lite']?'lite_':'')?><?=$intranet_session_language=='b5'?'chn':'en'?>">
	    <div class="module_title_icon"><a title="eLibrary plus" href="./">&nbsp;</a></div>
		<div class="module_filter">
	     
	    
		    <form class="search" method="post" action="../elibplus/">
			<a class="show_advancesearch" href="#"><?=$eLib_plus["html"]["advanced"]?></a>
		
			    <input type="input" name="keyword" value="<?=$page=='search'?intranet_htmlspecialchars($keyword):''?>" placeholder="<?=$eLib["html"]["search"]?>"/>
			    <input type="hidden" name="page" value="search" />
	
		    </form>
		    
		    <? include('templates/advancesearch_form.php'); ?>
		    
		    <div class="select_book_type"  <?if($page){?> style="display:none" <?}?>>
		    <dl class="dropdown" id="sample">
			<dt><a href="#"><span>
			    <? if ($ck_eLib_bookType=='type_physical'): ?>
			    
				<?=$eLib_plus["html"]["book"]?>
				
			    <? elseif ($ck_eLib_bookType=='type_pc'): ?>
			    
				<?=$eLib_plus["html"]["ebook"] ?>(<?=$eLib_plus["html"]["pconly"]?>)
				
			    <? elseif ($ck_eLib_bookType=='type_ipad'): ?>
			    
				<?=$eLib_plus["html"]["ebook"] ?>(<?=$eLib_plus["html"]["pcipad"]?>)
								
			    <? else: ?>
			    
				<?=$eLib["html"]["all_books"] ?>
				
			    <? endif; ?>
			</span></a></dt>
			<dd>
			    <ul style="display: none;">
				<li <?=($ck_eLib_bookType=='type_all') ? 'class="selected"' : ""?>>
				    <a href="#" id="type_all"><?=$eLib["html"]["all_books"] ?></a>
				</li>
				<li <?=($ck_eLib_bookType=='type_physical') ? 'class="selected"' : ""?>>
				    <a href="#" id="type_physical"><?=$eLib_plus["html"]["book"] ?></a>
				</li>
				<li <?=($ck_eLib_bookType=='type_pc') ? 'class="selected"' : ""?>>
				    <a href="#" id="type_pc"><?=$eLib_plus["html"]["ebook"] ?>(<?=$eLib_plus["html"]["pconly"]?>)</a>
				</li>
				<li <?=($ck_eLib_bookType=='type_ipad') ? 'class="selected"' : ""?>>
				    <a href="#" id="type_ipad"><?=$eLib_plus["html"]["ebook"] ?>(<?=$eLib_plus["html"]["pcipad"]?>)</a>
				</li>
			    </ul>
			</dd>
		  </dl>
		<em><?=$eLib["html"]["display"]?></em>
		</div>
	    <p class="spacer"></p>
	      
	    </div>
		<div class="eLib_menu">
		    <ul>
			<li <?=$page=='category'?'class="selected"':''	?>>
			    <div><a class="menu_icon_cat"><?=$eLib_plus["html"]["bookcategory"]?></a></div>
			</li>
			<li <?=$page=='ranking'?'class="selected"':''	?>>
			    <div><a class="menu_icon_ranking" href="./?page=ranking"><?=$eLib_plus["html"]["ranking"]?></a></div>
			</li>
						
			<li <?=$page=='stats'?'class="selected"':''	?>> <? //Charles Ma 2013-09-30 ?>
			    <div><a class="menu_icon_report" href="./?page=stats"><?=$eLib_plus["html"]["stats"]?></a></div>
			</li>			
			
			<li <?=$page=='record'?'class="selected"':''	?>>
			    <div><a class="menu_icon_record" href="./?page=record"><?=$eLib_plus["html"]["myrecord"]?>
			    <? if ($notification_count): ?>
				<span class="record_alert"><?=$notification_count?></span>
			    <? endif; ?>
			    </a></div>
			</li>
			
			<? if ($permission['circulation'] && !$plugin['eLib_Lite']): ?>
			    <li>
				<div><a class="menu_icon_admin" target="elib_circulations" href="/home/library_sys/management/circulation/"><?=$eLib_plus["html"]["circulations"]?></a></div>
			    </li>
			<? endif;?>
			<? if ($permission['setting']): ?>
				<? if($plugin['eLib_Lite']){?>					
			   		<li>
					<div><a class="menu_icon_admin_setting" href="/home/library_sys/admin/book/elib_setting_license.php"><span><?=$Lang['libms']['bookmanagement']['ebook_license'] ?></a></div>
				    </li>
				<?}else{?>
					<li>
					<div><a class="menu_icon_admin_setting" href="/home/library_sys/admin/book/"><span><?=$eLib_plus["html"]["adminsetting"] ?></a></div>
				    </li>
				<?}?>
			<? endif;?>
			
			<li>
			    <div><a class="setting_theme" href="#"><?=$eLib_plus["html"]["themesetting"]?></a></div>
			</li>
			
		    </ul>
		
		  <p class="spacer"></p>
		</div>
	   
    	</div>
    <? include('templates/book_catalog.php'); ?>
    </div>
 
    <? include_once($main_template); ?>

</div>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
