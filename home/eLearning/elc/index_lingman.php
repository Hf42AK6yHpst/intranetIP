<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libuser.php");
include("../../../includes/libeclass.php");
//include("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$li = new libuser($UserID);
$lo = new libeclass();

$courseArray = $lo->returnSpecialRooms($li->UserEmail,2,0);
$l1_course = 0;
$l2_course = 0;
$l3_course = 0;
$l1_id = "";
$l2_id = "";
$l3_id = "";

//hdebug_r($courseArray);


if ((!is_array($courseArray)) || (count($courseArray)<=0)){
  header ("Location: /home");
} else {
  for ($i=0;$i<count($courseArray);$i++)
  {
//    $level = substr($courseArray[$i][13],7);
    $level = substr($courseArray[$i][9],7);
    if ($level=="1"){
      ++$l1_course;
      
      # Check if the id loaded is same as uc_id from previous page
      # to prevent id overwrite
      if($l1_id != $uc_id)
      	$l1_id = $courseArray[$i][6];
    }
    else if ($level=="2"){
      ++$l2_course;
      
      # Check if the id loaded is same as uc_id from previous page
      # to prevent id overwrite
      if($l2_id != $uc_id)
      	$l2_id = $courseArray[$i][6];
    }
    else if ($level=="3"){
      ++$l3_course;
      
      # Check if the id loaded is same as uc_id from previous page
      # to prevent id overwrite
      if($l3_id != $uc_id)
      	$l3_id = $courseArray[$i][6];
    }
  }
}

if (($l1_course == 0) && ($l2_course == 0) && ($l3_course == 0)){
  header ("Location: /home");
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>凌文網上教室</title>
<style type="text/css">
<!--
.text1 {
	font-family: "Arial", "Verdana", "新細明體";
	font-size: 12px;
	color: #660000;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body background="<?=$eclass_url_root?>/images/cnecc/index/bg_index.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('<?=$eclass_url_root?>/images/cnecc/index/btn_level_l_on.gif','<?=$eclass_url_root?>/images/cnecc/index/btn_level_2_on.gif','<?=$eclass_url_root?>/images/cnecc/index/images/btn_level_3_on.gif','<?=$eclass_url_root?>/images/cnecc/index/index_b03_on.gif','<?=$eclass_url_root?>/images/cnecc/index/btn_empty.gif')">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="120" align="right" valign="bottom"><a href="#"><img src="<?=$eclass_url_root?>/images/cnecc/index/bl_logo.gif" width="91" height="30" border="0"></a></td>
          <td align="center"> <table border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><img src="<?=$eclass_url_root?>/images/cnecc/index/10x10.gif" width="10" height="30"></td>
              </tr>
              <tr> 
                <td align="center"><img src="<?=$eclass_url_root?>/images/cnecc/index/title.gif" width="489" height="74"></td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td><img src="<?=$eclass_url_root?>/images/cnecc/index/index_a01.gif" width="163" height="206"></td>
                      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr> 
                            <td><img src="<?=$eclass_url_root?>/images/cnecc/index/index_a02.gif" width="251" height="104"></td>
                          </tr>
                          <tr> 
                            <td height="102" valign="bottom" background="<?=$eclass_url_root?>/images/cnecc/index/index_a03.gif"> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                   <?php
                                    if ($l1_course >0){
                                  ?>
                                  <td align="center"><a href="../login.php?uc_id=<?=$l1_id?>"><img src="<?=$eclass_url_root?>/images/cnecc/index/btn_level_l.gif" name="L1" width="66" height="84" border="0" id="L1" onMouseOver="MM_swapImage('L1','','<?=$eclass_url_root?>/images/cnecc/index/btn_level_l_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
                                  <?php
                                    } else {
                                  ?>
                                  <td align="center"><img src="<?=$eclass_url_root?>/images/cnecc/index/btn_empty.gif" name="L1" width="66" height="84" border="0" id="L1" ></td>
                                  <?php
                                    }
                                  ?>

                                  <?php
                                    if ($l2_course >0){
                                  ?>
                                  <td align="center"><a href="../login.php?uc_id=<?=$l2_id?>"><img src="<?=$eclass_url_root?>/images/cnecc/index/btn_level_2.gif" name="L2" width="66" height="84" border="0" id="L2" onMouseOver="MM_swapImage('L2','','<?=$eclass_url_root?>/images/cnecc/index/btn_level_2_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
                                  <?php
                                    } else {
                                  ?>
                                  <td align="center"><img src="<?=$eclass_url_root?>/images/cnecc/index/btn_empty.gif" name="L2" width="66" height="84" border="0" id="L2" ></td>
                                  <?php
                                    }
                                  ?>

                                  <?php
                                    if ($l3_course >0){
                                  ?>
                                  <td align="center"><a href="../login.php?uc_id=<?=$l3_id?>"><img src="<?=$eclass_url_root?>/images/cnecc/index/btn_level_3.gif" name="L3" width="66" height="84" border="0" id="L3" onMouseOver="MM_swapImage('L3','','<?=$eclass_url_root?>/images/cnecc/index/btn_level_3_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
                                  <?php
                                    } else {
                                  ?>
                                  <td align="center"><img src="<?=$eclass_url_root?>/images/cnecc/index/btn_empty.gif" name="L3" width="66" height="84" border="0" id="L3" ></td>
                                  <?php
                                    }
                                  ?>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                      <td><img src="<?=$eclass_url_root?>/images/cnecc/index/index_a04.gif" width="199" height="206"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td><img src="<?=$eclass_url_root?>/images/cnecc/index/index_b01.gif" width="67" height="128"></td>
                      <td height="128"> <table width="100%" height="128" border="0" cellpadding="0" cellspacing="0">
                          <tr> 
                            <td height="7" valign="top" background="<?=$eclass_url_root?>/images/cnecc/index/index_b02_1.gif"><img src="<?=$eclass_url_root?>/images/cnecc/index/index_b02_1.gif" width="465" height="7"></td>
                          </tr>
                          <tr> 
                            <td width="465" height="115" background="<?=$eclass_url_root?>/images/cnecc/index/index_b02_2.gif"> 
                              <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                <tr> 
                                 <td class="text1">
								  <center><strong>「凌文」網上中文教室簡介</strong><br />
								  <br />
								  「凌文」網上中文教室，是幫助學生自學的園地，亦是方便老師教學的平台。在現在的階段中，「凌文」一則可以擴闊學生的閱讀面，增加他們的閱讀量，二則可以提高學生的閱讀能力，並進行自我評估；同時，老師也可以藉此系統了解學生的學習能力。在這教室裏，教學和閱讀都以教統局建議的六百篇篇章為素材，我們將篇章分為「古典」和「現代」兩部份，當中有三個進階層次，好讓老師和同學可以靈活選用。</center>
								  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr> 
                            <td height="6" valign="bottom" background="<?=$eclass_url_root?>/images/cnecc/index/index_b02_3.gif"><img src="<?=$eclass_url_root?>/images/cnecc/index/index_b02_3.gif" width="465" height="6"></td>
                          </tr>
                        </table></td>
                      <td align="left" valign="top"><img src="<?=$eclass_url_root?>/images/cnecc/index/index_b03.gif" name="thanks" width="80" height="128" border="0" usemap="#Map" id="thanks"></td>
                    </tr>
                  </table></td>
              </tr>
            </table> </td>
          <td width="90">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
<map name="Map">
  <area shape="poly" coords="5,59" href="#">
  <area shape="poly" coords="7,59" href="#">
  <area shape="poly" coords="7,57" href="#">
  <area shape="poly" coords="8,59,44,73,59,70,80,31,54,23,27,29,10,44" href="#" onClick="MM_openBrWindow('index_lingman_thx.php','','width=400,height=300')" onMouseOver="MM_swapImage('thanks','','<?=$eclass_url_root?>/images/cnecc/index/index_b03_on.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
</body>
</html>
