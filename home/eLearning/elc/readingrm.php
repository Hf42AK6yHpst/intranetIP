<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libeclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$li = new libuser($UserID);
$lo = new libeclass();

$ec_image_path = $eclass_url_root."/images";

$body_tags = " leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' ";
include("../../../templates/fileheader.php");

if ($intranet_session_language == "gb")
{
	$ImageSuffix = "_gb";
}
?>

<style type="text/css">
body{
	background-color: #F8E1A1;
	background-image:url('<?=$ec_image_path?>/reading_room_eng/bg.gif');
}
</style>

<script language="javascript">
function logoutIP(){
	self.location = "/logout_rr.php";
}
</script>


<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#000000">
  <tr>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td><td width="16">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" background="<?=$ec_image_path?>/reading_room_eng/header/teacher/library.gif">
  <tr>
    <td width="210"><img src="<?=$ec_image_path?>/reading_room_eng/header/teacher/logo_rr_common<?=$ImageSuffix?>.gif"></td>
    <td align="right" valign="bottom" style="vertical-align:bottom; background:url(<?=$ec_image_path?>/reading_room_eng/header/teacher/bg.gif)"><img src="<?=$ec_image_path?>/reading_room_eng/header/teacher/corner_right.gif"></td>
  </tr>
</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25" valign="top" style="background:url(<?=$ec_image_path?>/reading_room_eng/bg_cell_left.gif)"><img src="<?=$ec_image_path?>/reading_room_eng/header/teacher/bg_left.gif"></td>
    <td align="center" valign="top" bgcolor="#FDF8E9">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style="background:url(<?=$ec_image_path?>/reading_room_eng/header/teacher/bg_title.gif)"><img src="<?=$ec_image_path?>/reading_room_eng/header/teacher/bg_title.gif"></td>
        </tr>
      </table>
      <br>
	<table width="80%" border=0 cellpadding=6 cellspacing=0>
        <?=$lo->displayUserSpecialRoom($li->UserEmail,1)?>
      </table>
      <p>&nbsp;</p><p>&nbsp;</p></td>
    <td width="25" valign="top" style="background:url(<?=$ec_image_path?>/reading_room_eng/bg_cell_right.gif)"><img src="<?=$ec_image_path?>/reading_room_eng/header/teacher/bg_right.gif"></td>
  </tr>
  <tr>
    <td><img src="<?=$ec_image_path?>/reading_room_eng/bg_bottom_left.gif"></td>
    <td style="background:url(<?=$ec_image_path?>/reading_room_eng/bg_cell_bottom.gif)">&nbsp;</td>
    <td><img src="<?=$ec_image_path?>/reading_room_eng/bg_bottom_right.gif"></td>
  </tr>
</table>


</body>
</html>




<?php
include("../../../templates/filefooter.php");
intranet_closedb();
?>
<script language=javascript src=/templates/menu/sniffer.js></script>
<SCRIPT LANGUAGE=JAVASCRIPT>
var status = false;

function alterLayer(lay){
         if (status)
         {
             hideLayer(lay);
         }
         else
         {
             showLayer(lay);
         }
         status = !status;
}

function hideLayer(lay) {
     if (ie5 || ie4) {document.all[lay].style.visibility = "hidden";}
     else if (ns4) {document.layers[lay].visibility = "hide";}
     else if (ns6) {document.getElementById([lay]).style.display = "none";}
}
function showLayer(lay) {
     if (ie5 || ie4) {document.all[lay].style.visibility = "visible";}
     else if (ns4) {document.layers[lay].visibility = "show";}
     else if (ns6) {document.getElementById([lay]).style.display = "block";}
}
</SCRIPT>