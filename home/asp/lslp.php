<?php

# to access LSLP server


include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libasp.php");
include_once("../../includes/liblslp.php");
intranet_auth();
intranet_opendb();

$Project = "LSLP";
$IsServer =  FALSE;
$li_asp = new libasp($Project, $IsServer);
$ls = new lslp();

if($ls->hasLSLPAccess() && $ls->isInLicencePeriod())
{
	# do nothing
}
else
{
	//echo "<script>window.close()</script>";
	echo $i_general_no_access_right;
	die;
}

if ($li_asp->GET_CURRENT_LICENSE())
{
	list($url, $form_data) = $li_asp->CHECK_ASP_ACCESS();
}

# if open lslp from ebook
if($ebookAdd)
{
	$form_data.="<input type='hidden' name=\"ebookAdd\" value=\"1\" />";
}
if($ebookView)
{
	$form_data.="<input type='hidden' name=\"ebookView\" value=\"1\" />";
}



if($ebookEditLink)
{
	$object_id = str_replace($li_asp->ASP_Server.'/home/lslp/eclass/display.php?ID=',"",$ebookEditLink);
	$form_data.="<input type='hidden' name=\"ObjectID\" value=\"".$object_id."\" />";
	$form_data.="<input type='hidden' name=\"ebookEditLink\" value=\"".$ebookEditLink."\" />";
	$form_data.="<input type='hidden' name=\"element_id\" value=\"".$element_id."\" />";
	$form_data.="<input type='hidden' name=\"forum_id\" value=\"".$forum_id."\" />";
	$form_data.="<input type='hidden' name=\"survey_id\" value=\"".$survey_id."\" />";
	$form_data.="<input type='hidden' name=\"exercise_id\" value=\"".$exercise_id."\" />";
}

//ID=19&element_id=161&forum_id=&survey_id=&exercise_id= 
if ($ID>0)
{
	$form_data.="<input type='hidden' name=\"ID\" value=\"".$ID."\" />";
	$form_data.="<input type='hidden' name=\"element_id\" value=\"".$element_id."\" />";
	$form_data.="<input type='hidden' name=\"forum_id\" value=\"".$forum_id."\" />";
	$form_data.="<input type='hidden' name=\"survey_id\" value=\"".$survey_id."\" />";
	$form_data.="<input type='hidden' name=\"exercise_id\" value=\"".$exercise_id."\" />";
	$form_data.="<input type='hidden' name=\"redirect\" value=\"home/lslp/eclass/display.php\" />";
}


// The following two are come from assessment, since changing them to new logic required much work
if ($lslp_assessment){
	// for teacher create lslp assessment
	$form_data.="<input type='hidden' name=\"RecordID\" value=\"".$RecordID."\" />";
	$form_data.="<input type='hidden' name=\"try_login\" value=\"".$try_login."\" />";
	$form_data.="<input type='hidden' name=\"signature\" value=\"".$signature."\" />";
	$url = $li_asp->ASP_Server.'/home/lslp/exercise/assessment_lslp.php';
}
if ($lslp_assessment_exercise){
	// for student doing lslp exercise
	$form_data.="<input type='hidden' name=\"RecordID\" value=\"".$RecordID."\" />";
	$form_data.="<input type='hidden' name=\"try_login\" value=\"".$try_login."\" />";
	$form_data.="<input type='hidden' name=\"viewonly\" value=\"".$viewonly."\" />";
	$form_data.="<input type='hidden' name=\"noButton\" value=\"".$noButton."\" />";
	$form_data.="<input type='hidden' name=\"StudentID\" value=\"".$StudentID."\" />"; 
	$url = $li_asp->ASP_Server.'/home/lslp/exercise/std_submit_lslp.php';
}
intranet_closedb();


?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body onLoad="document.MainForm.submit();">
<form name="MainForm" id="MainForm" action="<?=$url?>" method="post">
<?=$form_data?>
</form>
</body>

</html>
