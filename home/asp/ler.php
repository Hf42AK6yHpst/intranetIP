<?php

# to access LER server


include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libasp.php");
include_once("../../includes/libportal.php");
//include_once("../../includes/liblslp.php");
intranet_auth();
intranet_opendb();

$Project = "LER";
$IsServer =  FALSE;
$li_asp = new libasp($Project, $IsServer);
$li_portal = new libportal();

$user_course_obj = $li_portal->returnEClassUserIDCourseIDStandard($ck_user_email);

for($a=0;$a<sizeof($user_course_obj);$a++)
{
	$user_course[] = array(($user_course_obj[$a]['course_id']),($user_course_obj[$a]['course_code']),($user_course_obj[$a]['course_name']));
}

$serialized_course = serialize($user_course);
$encoded_course = rawurlencode($serialized_course);


if ($li_asp->GET_CURRENT_LICENSE())
{
	list($url, $form_data) = $li_asp->CHECK_ASP_ACCESS();
}
intranet_closedb();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body onLoad="document.MainForm.submit();">
<form name="MainForm" id="MainForm" action="<?=$url?>" method="post">
<?=$form_data?>
<input type="hidden" name="user_course" value="<?=$encoded_course?>"/>
<input type="hidden" name="current_lang" value="<?=$ck_default_lang?>"/>
</form>
</body>

</html>
