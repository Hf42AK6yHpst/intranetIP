<?php
/*
 * Editing By
 * 
 * Modification Log:
 * 2015-06-10 (Jason)
 * 		- add the permission checking on access Flipped Channels Module
 */
$PATH_WRT_ROOT = "../../";
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libasp.php");
intranet_auth();
intranet_opendb();
	
$Project = "FlipChan";
$IsServer =  FALSE;
$li_asp = new libasp($Project, $IsServer);
# permission checking
if(!$plugin['FlippedChannels'] || $_SESSION["FlippedChannelsServerPath"] == ""){
	echo $i_general_no_access_right;
	die;
}

$LoginPlatform = "IP25 - ".$userBrowser->browsertype." ".$userBrowser->version." (".$userBrowser->platform.")";

# Main
list($url, $form_data) = $li_asp->CHECK_ASP_ACCESS();

$form_data.="<input type='hidden' name='ClientName' value='$BroadlearningClientName' />";
$form_data.="<input type='hidden' name='LoginPlatform' value='$LoginPlatform' />";

//
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body onLoad="document.MainForm.submit();">
<form name="MainForm" id="MainForm" action="<?=$url?>" method="post">
<?=$form_data?>
</form>
</body>

</html>