<?php
include_once ("../includes/global.php");
include_once ("../includes/libdb.php");
include_once ("../includes/libuser.php");
if ($BroadlearningSupportSite == "")
{
    $BroadlearningSupportSite = "http://support.broadlearning.com";
}
$cs_link = "$BroadlearningSupportSite/lookup/usersupport.php";

if (isset($UserID))
{
    intranet_opendb();
    $lu = new libuser($UserID);
    $US_Email = $lu->UserEmail;
    $US_UserLogin = $lu->UserLogin;
    $US_UserType = $lu->RecordType;
    intranet_closedb();
}
$US_Language = $intranet_session_language;
$US_SystemName=1;							//	0=eClass, 1=Intranet, 2=eClassJunior
$US_URL=$HTTP_HOST.$SCRIPT_NAME;			//the function that he is triggering
$US_ClientName=$BroadlearningClientName;	// client name

?>
<html>
<body ONLOAD=document.form1.submit()>
<form name=form1 action=<?=$cs_link?> method=post>
<input type=hidden name="US_SystemName" value="1">
<input type=hidden name="US_SystemType" value="<?=$US_SystemType?>">
<input type=hidden name="US_URL" value="<?=$US_URL?>">
<input type=hidden name="US_Email" value="<?=$US_Email?>">
<input type=hidden name="US_ClientName" value="<?=$US_ClientName?>">
<input type=hidden name="US_UserType" value="<?=$US_UserType?>">
<input type=hidden name="US_Language" value="<?=$US_Language?>">
<input type=hidden name="US_UserLogin" value="<?=$US_UserLogin?>">
</form>
</body>
</html>