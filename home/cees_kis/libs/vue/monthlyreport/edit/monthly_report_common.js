var formData = $.parseJSON(formDataJson);
var jsLang = $.parseJSON(jsLangJson);
var submitForm = {
    AppointmentTermination: [],
    SubstituteTeacher: [],
    TeacherSickLeave: [],
    TeacherOtherLeave: [],
    SchoolVisit: [],
    SchoolAdministration: [],
    TeacherCourse: [],
    StudentEca: {internal: [], external: []},
    advancedNotice: [],
    SchoolAwards: [],
    Others: [],
    EnrolQuota: []
};

var eventHub = new Vue({});

var myMixin = {
    methods: {
        addNewRows: function (num, row, target) {
            var contentArr = [];
            for (var i = 0; i < num; i++) {
                contentArr.push(Object.assign({}, row))
            }
            this.addRows(contentArr, target);
        },
        addRows: function (contents, target) {
            _.forEach(contents, function (contentObj) {
                target.push(contentObj);
            });
        },
        removeRow: function (index, target) {
            target.splice(index, 1);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker('refresh');
                $('.datepicker').datepicker('update');
            });
        },
        confirmDataNow: function(self, name, callback){
            /*
            callback :{
                arrayColumn: [1],
                contents: self.contents[tableType],
                checkResult: function(result){},
                before:
                addRow: f
            }
             */
            var uploadfile = $('#'+name+'File')[0].files[0];
            //File reading
            var fr = new FileReader();
            var resultAry = [];
            fr.onload = () => {
                var allResults = fr.result.split("\r\n");
                for( var x in allResults) {
                    if(allResults[x]!=""){
                        allResults[x] = allResults[x].replaceAll('(\\r\\n|\\n|\\r)', ''); // remove all line break character
                        allResults[x] = allResults[x].replaceAll('""', 'XcolonX');
                        allResults[x] = allResults[x].replaceInside('"', ',', 'XcommaX');
                        var ar = allResults[x].split(',');
                        for(var y in ar){
                            ar[y] = ar[y].replaceAll('XcolonX', '"');
                            ar[y] = ar[y].replaceAll('XcommaX', ',');
                        }
                        if(callback.arrayColumn != null){
                            for(var y in  callback.arrayColumn){
                                ar[callback.arrayColumn[y]] = ar[callback.arrayColumn[y]].split(';');
                            }
                        }
                        resultAry.push(ar);
                    }
                }
                //File reading
                $.ajax({
                    url: "/home/cees_kis/monthlyreport/ajaxImport/"+name,
                    method: "POST",
                    type: "POST",
                    data: {"content" : resultAry},
                    success: function(data){
                        var result = JSON.parse(data);
                        if(!window.prop){
                            window.prop = {};
                            window.prop.tempData = [];
                        }
                        window.prop.tempData[name+'TempData'] = result;
                        Vue.component(name+'ConfirmTable', {
                            template: self.$root.confirmTableTemplate,
                            data: function(){
                                return{
                                    TotalError : jsLang.TotalError,
                                    Remark : jsLang.Remark,
                                    result : {},
                                    error : 0,
                                    errorLang: self.$root.errorLang
                                };
                            },
                            mounted: function(){
                                var data = JSON.parse(JSON.stringify(window.prop.tempData[name+'TempData'].result));
                                var here = this;
                                here.error = window.prop.tempData[name+'TempData'].error;
                                here.result = data;
                                if(here.error != 0){
                                    $('button.btn-prev').show();
                                    $('button.btn-confirm').hide();
                                } else {
                                    $('button.btn-confirm').attr('data-dismiss', 'modal');
                                }
                                if(callback.checkResult != null){
                                    for(var x in here.result.checkResult){
                                        here.result.checkResult[x] = callback.checkResult(here.result.checkResult[x]);
                                    }
                                }

                                for(var x in data.data){
                                    data.data[x] = callback.before(data.data[x])
                                }
                                var here = this;
                                here.error = window.prop.tempData[name+'TempData'].error;
                                here.result = data;
                                if(here.error != 0){
                                    $('button.btn-prev').show();
                                    $('button.btn-confirm').hide();
                                }
                                eventHub.$once('confirm-data-'+name+'Modal', function(){
                                    if(here.error == 0){
                                        //eventHub.$emit('add-data-appointmentModal');
                                        var newArr = [];
                                        var data = window.prop.tempData[name+'TempData'].result;
                                        if(callback.special == null){
                                            for(var x in data.data){
                                                newArr.push(
                                                    callback.addRow(data.data[x])
                                                );
                                            }
                                            if(callback.contents !=null){
                                                self.addRows(newArr, callback.contents);
                                            } else {
                                                self.addRows(newArr, self.contents);
                                            }
                                        } else if(callback.special == 'enroll'){
                                            for(var x in data.data){
                                                callback.addRow(data.data[x], x);
                                            }
                                        }
                                        eventHub.$emit('close-'+name+'Modal');
                                        window.prop.tempData[name+'TempData'] = null;
                                        this.$nextTick(function () {
                                            $('.selectpicker').selectpicker();
                                        });
                                    }
                                });
                            },
                        });
                        eventHub.$emit('confirm-table-'+name+'Modal');
                    }
                });
            }
            fr.readAsText(uploadfile, 'BIG5');
        }
    }
};

Vue.component('date-input', {
    template: '<div :id="divId" class="input-group date datetimepicker datepicker">\
            <input type="text" class="form-control" v-model="this.date"/>\
            <span class="input-group-addon">\
            <i class="fa fa-calendar-o" aria-hidden="true"></i>\
            </span>\
            </div>\
            ',
    props: ['id', 'type', 'sourceKey'],
    // data: function () {
    //     return {
    //         date: this.$parent.contents[this.id][this.sourceKey]
    //     }
    // },
    methods: {
        assign: function () {
            this.$parent.contents[this.id][this.sourceKey] = this.date;
        }

    },
    computed: {
        divId: function () {
            return this.type + '_date_' + this.id;
        },
        date: {
            get: function(){
                return this.$parent.contents[this.id][this.sourceKey];
            },
            set: function (newValue){
                this.$parent.contents[this.id][this.sourceKey] = newValue;
            }
        }
    },
    mounted: function () {
        var self = this;
        $('#' + this.divId)
            .datepicker({
                format: 'yyyy-mm-dd',
                container: '#' + this.divId,
                orientation: "left auto"
            })
            .on("changeDate", function (e) {
                if (e.date) {
                   // self.date = e.date.format('YYYY-MM-DD');
                   //  self.date = e.format('YYYY-MM-DD')
                    self.date = e.format('yyyy-mm-dd')
                } else {
                    self.date = "";
                }
                // if(self.sourceKey == 'dateFrom' || self.sourceKey == 'fromdate'){
                //     eventHub.$emit('assign-startdate-'+self.type, {id :self.id, value :self.date} );
                // }
            })
            .on("hide", function (e) {
                if (e.date) {
                    //   self.date = e.date.format('YYYY-MM-DD');
                    // self.date = e.format('YYYY-MM-DD')
                    self.date = e.format('yyyy-mm-dd')
                } else {
                    self.date = "";
                }
                // if(self.sourceKey == 'dateFrom' || self.sourceKey == 'fromdate'){
                //     eventHub.$emit('assign-startdate-'+self.type, {id :self.id, value :self.date} );
                // }
            });
        eventHub.$on('collect-info', this.assign);
    }
});



Vue.component('multiple-date-select', {
    template: '<div :id="divId" class="input-group date datepicker" data-provide="datepicker-inline">\
            <input type="text" class="form-control" v-model="this.date"  />\
            <span class="input-group-addon">\
            <i class="glyphicon glyphicon-th fa fa-calendar-o" ></i>\
            </span>\
            </div>\
            ',
    props: ['id', 'type', 'sourceKey'],
    // data: function () {
    //     return {
    //         date: this.$parent.contents[this.id][this.sourceKey]
    //     }
    // },
    methods: {
        assign: function () {
            this.$parent.contents[this.id][this.sourceKey] = this.date;
        },
    },
    computed: {
        divId: function () {
            return this.type + '_date_' + this.id;
        },
        date: {
            get: function(){
                return this.$parent.contents[this.id][this.sourceKey];
            },
            set: function (newValue){
                this.$parent.contents[this.id][this.sourceKey] = newValue;
            }
        }
    },
    mounted: function () {
        var self = this;
        $('#' + this.divId)
            .datepicker({
                format: 'yyyy-mm-dd',
                multidate: true,
                container: '#' + this.divId,
                orientation: "left auto"
            })
            .on("changeDate", function (e) {
                var i;
                if(e.dates.length > 0){
                    self.datelist = '';
                    for(i=0;i < e.dates.length;i++ ){
                        if(i == 0){
                            self.datelist = moment(e.dates[i]).format('YYYY-MM-DD');
                        }else{
                            self.datelist = self.datelist + ',' + moment(e.dates[i]).format('YYYY-MM-DD');
                        }
                    }
                    self.date = self.datelist;
                }else{
                    self.date = "";
                }

            })
            .on("hide", function (e) {
                var i;
                if(e.dates.length > 0){
                    self.datelist = '';
                    for(i=0;i < e.dates.length;i++ ){
                        if(i == 0){
                            self.datelist = moment(e.dates[i]).format('YYYY-MM-DD');
                        }else{
                            self.datelist = self.datelist + ',' + moment(e.dates[i]).format('YYYY-MM-DD');
                        }
                    }
                    self.date = self.datelist;
                }else{
                    self.date = "";
                }

            });
        eventHub.$on('collect-info', this.assign);
    }
});


Vue.component('multi-date-input', {
    template: '<div :id="divId" class="input-group date datepicker" data-provide="datepicker-inline">\
            <input type="text" class="form-control" v-model="this.date" />\
            <span class="input-group-addon">\
            <i class="glyphicon glyphicon-th fa fa-calendar-o" ></i>\
            </span>\
            </div>\
            ',
    props: ['id', 'type', 'TypeKey'],
    // data: function () {
    //     return {
    //         date: this.$parent.contents[this.TypeKey][this.id].date
    //     }
    // },
    methods: {
        assign: function () {
            this.$parent.contents[this.TypeKey][this.id].date = this.date;
        }
    },
    computed: {
        divId: function () {
            return this.type + '_date_' + this.id;
        },
        date: {
            get: function(){
                return this.$parent.contents[this.TypeKey][this.id].date;
            },
            set: function (newValue){
                this.$parent.contents[this.TypeKey][this.id].date = newValue;
            }
        }
    },
    mounted: function () {
        var self = this;
        $('#' + this.divId)
            .datepicker({
                format: 'yyyy-mm-dd',
                container: '#' + this.divId,
                orientation: "left auto",
                autoclose: true
            })
            .on("changeDate", function (e) {
                // console.log(e);
                // var i;
                // if(e.dates.length > 0){
                //     self.datelist = '';
                    // for(i=0;i < e.dates.length;i++ ){
                    //     if(i == 0){
                    //         self.datelist = moment(e.dates[i]).format('YYYY-MM-DD');
                    //     }else{
                    //         self.datelist = self.datelist + ',' + moment(e.dates[i]).format('YYYY-MM-DD');
                    //     }
                    //
                    // }
                //     self.date = self.datelist;
                //
                // }else{
                //     self.date = "";
                // }

                self.date =  moment(e.dates[0]).format('YYYY-MM-DD');
            })
            .on('hide', function(e){
                self.date =  moment(e.dates[0]).format('YYYY-MM-DD');
            });
        eventHub.$on('collect-info', this.assign);
    }
});

Vue.component('eca-multi-date-input', {
    template: '<div :id="divId" class="input-group date datetimepicker datepicker">\
            <input type="text" class="form-control" v-model="this.date" />\
            <span class="input-group-addon">\
            <i class="fa fa-calendar-o" aria-hidden="true"></i>\
            </span>\
            </div>\
            ',
    props: ['id', 'type', 'TypeKey'],
    // data: function () {
    //     return {
    //         date: this.$parent.contents[this.TypeKey][this.id].date
    //     }
    // },
    methods: {
        assign: function () {
            this.$parent.contents[this.TypeKey][this.id].date = this.date;
        }

    },
    computed: {
        divId: function () {
            return this.type + '_date_' + this.id;
        },
        date: {
            get: function(){
                return this.$parent.contents[this.TypeKey][this.id].date;
            },
            set: function (newValue){
                this.$parent.contents[this.TypeKey][this.id].date = newValue;
            }
        }
    },
    mounted: function () {
        var self = this;
        $('#' + this.divId)
            .datepicker({
                format: 'yyyy-mm-dd',
                container: '#' + this.divId,
                multidate: true,
                orientation: "left auto"
            })
            .on("changeDate", function (e) {
                var i;
                if(e.dates.length > 0){
                    self.datelist = '';
                    for(i=0;i < e.dates.length;i++ ){
                        if(i == 0){
                            self.datelist = moment(e.dates[i]).format('YYYY-MM-DD');
                        }else{
                            self.datelist = self.datelist + ',' + moment(e.dates[i]).format('YYYY-MM-DD');
                        }

                    }
                    self.date = self.datelist;
                }else{
                    self.date = "";
                }

            })
            .on("hide", function (e) {
                var i;
                if(e.dates.length > 0){
                    self.datelist = '';
                    for(i=0;i < e.dates.length;i++ ){
                        if(i == 0){
                            self.datelist = moment(e.dates[i]).format('YYYY-MM-DD');
                        }else{
                            self.datelist = self.datelist + ',' + moment(e.dates[i]).format('YYYY-MM-DD');
                        }

                    }
                    // console.log(self.datelist);
                    self.date = self.datelist;
                }else{
                    self.date = "";
                }

            });
        eventHub.$on('collect-info', this.assign);
    }
});

Vue.component('teacher-multi-select', {
    template: '<select  v-model="seletedIds" :id="this.objId" class="selectpicker" data-live-search="true" data-live-search-placeholder="" data-actions-box="true" ' +'\
                title="'+jsLang.Select+'" multiple>\
               <optgroup  v-for="groupinfo in teacherWithGroup"  :label="groupinfo.groupName" >\
               <option v-for="teacher in groupinfo.teacherlist" :value="teacher.id"  :data-tokens="teacher.name">{{ teacher.name }}</option>\
               </optgroup>\
               </select >\
        ',
    props: ['id', 'type'],
    data: function () {
        return {
            teachers: this.$root.teachers,
            teacherWithGroup:[
                {
                    groupName: jsLang.TeachingStaff,
                    teacherlist: this.$root.teachers[1]
                },
                {
                    groupName: jsLang.NonTeachingStaff,
                    teacherlist: this.$root.teachers[0]
                },
                {
                    groupName: jsLang.ArchivedStaff,
                    teacherlist: this.$root.teachers[2]
                }
            ]
        };
    },
    computed: {
        objId: function () {
            return this.type + '_teachermulti_' + this.id;
        },
        // seletedIds: function () {
        //     return this.$parent.contents[this.id].teacherIds
        // }
        seletedIds: {
            get: function(){
                return this.$parent.contents[this.id].teacherIds;
            },
            set: function (newValue){
                this.$parent.contents[this.id].teacherIds = newValue;
            }
        }
    },
    methods: {
        assign: function () {
            this.$parent.contents[this.id].teacherIds = this.seletedIds;
        }

    },
    created: function () {

    },
    mounted: function () {
        $('#' + this.objId).selectpicker();
        eventHub.$on('collect-info', this.assign);
    }
});

Vue.component('teacher-select', {
    template: '<select  v-model="seletedId" :id="this.objId" class="selectpicker" data-live-search="true"  data-live-search-placeholder="" data-actions-box="true" >\
               <option disabled>'+jsLang.Select+'</option>\
               <optgroup  v-for="groupinfo in teacherWithGroup"  :label="groupinfo.groupName">\
                <option v-for="teacher in groupinfo.teacherlist" :value="teacher.id" >{{ teacher.name }}</option>\
               </optgroup>\
               </select>\
        ',
    props: ['id', 'type'],
    data: function () {
        return {
            teachers: this.$root.teachers,
            teacherWithGroup:[
                {
                    groupName: jsLang.TeachingStaff,
                    teacherlist: this.$root.teachers[1]
                },
                {
                    groupName: jsLang.NonTeachingStaff,
                    teacherlist: this.$root.teachers[0]
                },
                {
                    groupName: jsLang.ArchivedStaff,
                    teacherlist: this.$root.teachers[2]
                }

            ]
            // seletedId: this.$parent.contents[this.id].teacherId
        };
    },
    methods: {
        assign: function () {
            this.$parent.contents[this.id].teacherId = this.seletedId;
        }
    },
    computed: {
        objId: function () {
            return this.type + '_teacherselect_' + this.id;
        },
        seletedId: {
            get: function(){
                return this.$parent.contents[this.id].teacherId;
            },
            set: function (newValue){
                this.$parent.contents[this.id].teacherId = newValue;
            }
        }
    },
    mounted: function () {
        $('#' + this.objId).selectpicker();
        eventHub.$on('collect-info', this.assign);

    }
});

Vue.component('category-multi-select', {
    template: '<select v-model="seletedIds" :id="this.objId" class="selectpicker" data-live-search-placeholder="" title='+jsLang.Select+' multiple>\
               <option v-for="category in categories" :value="category.id">{{ category.name }}</option>\
               </select>\
        ',
    props: ['id', 'type'],
    data: function () {
        return {
            categories : this.$root.categories
        };
    },
    computed: {
        objId: function () {
            return this.type + '_categorymulti_' + this.id;
        },
        // seletedIds :function (){
        //     return this.$parent.contents[this.type][this.id].categoryID;
        // }
        seletedIds : {
            get: function(){
                return this.$parent.contents[this.type][this.id].categoryID;
            },
            set: function (newValue){
                this.$parent.contents[this.type][this.id].categoryID = newValue;
            }
        }
    },
    methods: {
        assign: function () {
            this.$parent.contents[this.type][this.id].categoryID = this.seletedIds;
        }

    },
    created: function () {

    },
    mounted: function () {
        $('#' + this.objId).selectpicker();
        eventHub.$on('collect-info', this.assign);
    }
});

Vue.component('time-select',{
    template: '\
    <div class="input-group date" :id="objId">\
        <input type="text" :id="\'Input\'+objId" class="form-control" v-model="time"/>\
        <span class="input-group-addon">\
            <span class="glyphicon glyphicon-time"></span>\
        </span>\
    </div>\
    ',
    props: ['id', 'type'],
    methods: {
        assign: function () {
            // console.log(this.time);
            this.$parent.contents[this.id].time = this.time;
        }
    },
    computed: {
        objId: function () {
            return this.type + '_timeselect_' + this.id;
        },
        time: {
            get: function(){
                return this.$parent.contents[this.id].time;
            },
            set: function (newValue){
                this.$parent.contents[this.id].time = newValue;
            }
        }
    },
    created: function () {

    },
    mounted: function(){
        let self = this;
        $('#'+this.objId).datetimepicker({
            format: 'HH:mm',
            allowInputToggle: true
        }).on('dp.change',function(e){
            self.time = $(this).find('input').val();
        });
        eventHub.$on('collect-info', this.assign);
    }
});

Vue.component('add-row', {
    template: '<td class="addRow" colspan="2">\
            <a  href="#"  v-on:click="addOneCloumn"><i class="fa fa-plus" aria-hidden="true"></i></a>\
            '+jsLang.Add+'\
            <select class="noOfRows otherRow" id="otherRow" v-on:change="add">\
                <option>1</option>\
                <option>2</option>\
                <option>3</option>\
                <option>4</option>\
                <option>5</option>\
                <option>6</option>\
                <option>7</option>\
                <option>8</option>\
                <option>9</option>\
            </select>\
            '+jsLang.Row+'\
            </td>\
        ',
    props: ['add','model'],
    methods: {
        addOneCloumn: function (){
            var model = this.model;

            if(model == 'appointment'){
                this.$parent.addNewRows(this.$el.lastElementChild.value, this.$parent.row.appointment, this.$parent.contents.appointment);
            }else if(model == 'termination'){
                this.$parent.addNewRows(this.$el.lastElementChild.value, this.$parent.row.termination, this.$parent.contents.termination);
            }else if(model == 'internal'){
                this.$parent.addNewRows(this.$el.lastElementChild.value, this.$parent.row.internal, this.$parent.contents.internal);
            }else if(model == 'external'){
                this.$parent.addNewRows(this.$el.lastElementChild.value, this.$parent.row.external, this.$parent.contents.external);
            } else {
                this.$parent.addNewRows(this.$el.lastElementChild.value, this.$parent.row, this.$parent.contents);
            }

            this.$parent.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });

        }
    }
});

Vue.component('del-btn', {
    template: '<a href="#" class="btn-delete"><i class="fa fa-trash-o" aria-hidden="true" v-on:click="remove(target)"></i></a>',
    props: ['remove', 'target']
});

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

String.prototype.replaceInside = function(quotation, target, replacement){
    var newStr = "";
    var str = this;
    var open = false;
    var location = str.indexOf(quotation);
    if(location!=-1){
        newStr = str.substr(0, location);
        str = str.substr(location+1);
        location = str.indexOf(quotation);
        open = !open;
        while(location!=-1){
            if(open){
                newStr += str.substr(0, location).replaceAll(target,replacement);
            } else {
                newStr += str.substr(0,location);
            }
            if(location == str.length){
                return newStr;
            } else {
                str = str.substr(location+1);
            }
            location = str.indexOf(quotation);
            if(location!=-1) {
                open = !open;
            } else {
                newStr += str;
            }
        }
        return newStr;
    } else {
        return this;
    }
}