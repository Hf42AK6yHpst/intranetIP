Vue.component('enrol-quota', {
    template: '<div id="enrolQuotaDiv">\
    <table class="table report-style" id="tbl_enrolQuota">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 47%">\
    <col style="width: 47%">\
    <col style="width: 3%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="4">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#enrolQuotaModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromAccountManagement+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
        <th colspan="4">\
                <ul>\
                    <li v-if="content.attachment!=\'\' && typeof content.attachment !== \'undefined\'">'+jsLang.enrolQuotaAttachment+': <a :href="content.attachment" target="_blank"><i class="form-icon fa fa-file"></i>{{getAttachmentName()}}</a>&nbsp;<i style="cursor: pointer" class="fa fa-trash" v-on:click="removeAttachment"></i></li>\
                    <li v-else>'+jsLang.enrolQuotaAttachmentHint+'<br/><input type="file" id="enrolQuotaAttachment" v-on:change="fileOnChanged" /></li>\
                </ul>\
        </th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="2" style="text-align:center;vertical-align:middle;">'+jsLang.ClassroomTotal+' <br/>(' + jsLang.AbleToCreateClass + ') <br/><span style="font-size:11px;color: #888888;">* '+jsLang.FillEverySept+'</span></th>\
        <th></th>\
    </tr>\
    <tr>\
        <th></th>\
        <th style="text-align:center">'+jsLang.HalfDaySystem+'</th>\
        <th style="text-align:center">'+jsLang.WholeDaySystem+'</th>\
        <th></th>\
    </tr>\
    </thead>\
    <tbody>\
    <tr>\
        <td></td>\
        <td style="text-align: center">\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.classroomHalf">\
            <span v-else>{{content.classroomHalf}}</span>\
        </td>\
        <td style="text-align: center">\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.classroomWhole">\
            <span v-else>{{content.classroomWhole}}</span>\
        </td>\
        <td></td>\
    </tr>\
    </tbody>\
    </table>\
    <table class="table report-style" id="tbl_enrolQuota">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 3%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th></th>\
        <th colspan="7" style="text-align:center;">'+jsLang.ApprovedQuota+' (a) <br/><span style="font-size:11px;color: #888888;">* '+jsLang.FillEverySept+'</span></th>\
        <th colspan="7" style="text-align:center;">'+jsLang.ApprovedEnrolment+' (b)</th>\
        <th rowspan="2" style="text-align:center;vertical-align: middle;">'+jsLang.QuotaVacancy+'<br/>(a) - (b) = (c)</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="2" style="text-align:center;">'+jsLang.TimeSlot.AM+'</th>\
        <th style="text-align:center;">'+jsLang.Summarize+'</th>\
        <th colspan="2" style="text-align:center;">'+jsLang.TimeSlot.PM+'</th>\
        <th style="text-align:center;">'+jsLang.Summarize+'</th>\
        <th style="text-align:center;">'+jsLang.Summary+'</th>\
        <th colspan="2" style="text-align:center;">'+jsLang.TimeSlot.AM+'</th>\
        <th style="text-align:center;">'+jsLang.Summarize+'</th>\
        <th colspan="2" style="text-align:center;">'+jsLang.TimeSlot.PM+'</th>\
        <th style="text-align:center;">'+jsLang.Summarize+'</th>\
        <th style="text-align:center;">'+jsLang.Summary+'</th>\
        <th></th>\
    </tr>\
    </thead>\
    <tbody>\
    <tr>\
        <td></td>\
        <td style="text-align: center">'+jsLang.MixedClass+'</td>\
        <td style="text-align: center">\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaAMMixed">\
            <span v-else>{{content.quotaAMMixed}}</span>\
        </td>\
        <td rowspan="4" style="text-align:center;vertical-align: middle">{{quotaAM()}}</td>\
        <td style="text-align: center">--</td>\
        <td></td>\
        <td rowspan="4" style="text-align:center;vertical-align: middle">{{quotaPM()}}</td>\
        <td rowspan="4" style="text-align:center;vertical-align: middle">{{quotaSummary()}}</td>\
        <td style="text-align:center;">'+jsLang.MixedClass+jsLang.HalfDay+'<br/>(i)</td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolMixedHalf">\
        </td>\
        <td rowspan="4" style="text-align:center;vertical-align: middle">{{enrolAM()}}</td>\
        <td rowspan="2" style="text-align:center;vertical-align: middle">'+jsLang.HalfDayClass+'<br/>(v)</td>\
        <td rowspan="2" style="text-align:center;vertical-align: middle">\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolPMHalf">\
        </td>\
        <td rowspan="4" style="text-align:center;vertical-align: center">{{enrolPM()}}</td>\
        <td rowspan="4" style="text-align:center;vertical-align: center">{{enrolSummary()}}</td>\
        <td rowspan="4" style="text-align:center;vertical-align: center">{{quotaVacancy()}}</td>\
        <td></td>\
    </tr>\
    <tr>\
        <td></td>\
        <td style="text-align: center">'+jsLang.HalfDayClass+'</td>\
        <td style="text-align: center">\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaAMHalf">\
            <span v-else>{{content.quotaAMHalf}}</span>\
        </td>\
        <td style="text-align: center">'+jsLang.HalfDayClass+'</td>\
        <td style="text-align: center">\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaPMHalf">\
            <span v-else>{{content.quotaPMHalf}}</span>\
        </td>\
        <td style="text-align:center;">'+jsLang.MixedClass+jsLang.WholeDay+'<br/>(ii)</td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolMixedWhole">\
        </td>\
        <td></td>\
    </tr>\
    <tr>\
        <td></td>\
        <td style="text-align: center">'+jsLang.WholeDayClass+'</td>\
        <td style="text-align: center">\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaAMWhole">\
            <span v-else>{{content.quotaAMWhole}}</span>\
        </td>\
        <td style="text-align: center">'+jsLang.WholeDayClass+'</td>\
        <td style="text-align: center">\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaPMWhole">\
            <span v-else>{{content.quotaPMWhole}}</span>\
        </td>\
        <td style="text-align:center;">'+jsLang.HalfDayClass+'<br/>(iii)</td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolAMHalf">\
        </td>\
        <td rowspan="2" style="text-align:center;vertical-align: middle">'+jsLang.WholeDayClass+'</td>\
        <td rowspan="2" style="text-align:center;vertical-align: middle">\
            {{enrolPMWhole()}}\
        </td>\
        <td></td>\
    </tr>\
    <tr>\
        <td></td>\
        <td></td>\
        <td></td>\
        <td></td>\
        <td></td>\
        <td style="text-align:center;">'+jsLang.WholeDayClass+'<br/>(iv)</td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolAMWhole">\
        </td>\
        <td></td>\
    </tr>\
    </tbody>\
    </table>\
    </div>',
    mixins: [myMixin],
    data: function () {
        return {
            year: currYear,
            month: currMonth,
            content: formData.EnrolQuota[0] ? formData.EnrolQuota[0] : {
                classroomHalf: 0,
                classroomWhole: 0,
                quotaAMMixed: 0,
                quotaAMHalf: 0,
                quotaAMWhole: 0,
                quotaPMHalf: 0,
                quotaPMWhole: 0,
                enrolMixedHalf: 0,
                enrolMixedWhole: 0,
                enrolAMHalf: 0,
                enrolAMWhole: 0,
                enrolPMHalf: 0,
                attachment: ''
            },
            uploadProgress: 100
        }
    },
    computed: {
    },
    methods: {
        removeAttachment: function(){
            this.content.attachment = '';
        },
        getAttachmentName: function(){
            if(typeof this.content.attachment !== 'undefined' && this.content.attachment != ''){
                return this.content.attachment.split('/').reverse()[0];
            } else {
                return '';
            }
        },
        fileOnChanged: function(){
            let files = $('#enrolQuotaAttachment').prop('files');
            let here = this;
            if(files.length > 0) {
                file = files[0];
                let fileName = file.name;
                let fileExt = fileName.split('.').reverse()[0];
                if(fileExt.toLowerCase() != 'pdf'){
                    // Wrong File extension
                    window.alert(jsLang.AcceptPDFOnly);
                    $('#enrolQuotaAttachment').val('');
                }  else {
                    let fileType = file.type;
                    let fileSize = file.size;
                    let formData = new FormData();
                    let reportID = document.getElementById("reportID").value;
                    formData.append("file", file, fileName);
                    formData.append('reportId', reportID);
                    formData.append("upload_file", true);

                    $.ajax({
                        type: "POST",
                        url: "/home/cees_kis/monthlyreport/ajaxUploadFile/enrolQuota",
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            if (myXhr.upload) {
                                myXhr.upload.addEventListener('progress', function (event) {
                                    var percent = 0;
                                    var position = event.loaded || event.position;
                                    var total = event.total;
                                    if (event.lengthComputable) {
                                        percent = Math.ceil(position / total * 100);
                                    }
                                    here.uploadProgress = percent;
                                }, false);
                            }
                            return myXhr;
                        },
                        success: function (data) {
                            // console.log(data);return;
                            if(data!='') {
                                data = JSON.parse(data);
                                here.content.attachment = data.filePath;
                            } else {
                                $('#enrolQuotaAttachment').val('');
                            }
                        },
                        error: function (error) {
                        },
                        async: true,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000
                    });
                }
            }
        },
        quotaAM: function(){
            return parseInt(this.content.quotaAMMixed ? this.content.quotaAMMixed : 0) +
                parseInt(this.content.quotaAMHalf ? this.content.quotaAMHalf : 0) +
                parseInt(this.content.quotaAMWhole ? this.content.quotaAMWhole : 0);
        },
        quotaPM: function(){
            return parseInt(this.content.quotaPMHalf ? this.content.quotaPMHalf : 0) +
                parseInt(this.content.quotaPMWhole ? this.content.quotaPMWhole : 0);
        },
        quotaSummary: function(){
            return parseInt(this.quotaAM()) +
                parseInt(this.quotaPM()) -
                parseInt(this.content.quotaPMWhole ? this.content.quotaPMWhole : 0);
        },
        enrolAM: function(){
            return parseInt(this.content.enrolMixedHalf ? this.content.enrolMixedHalf : 0) +
                parseInt(this.content.enrolMixedWhole ? this.content.enrolMixedWhole : 0) +
                parseInt(this.content.enrolAMHalf ? this.content.enrolAMHalf : 0) +
                parseInt(this.content.enrolAMWhole ? this.content.enrolAMWhole : 0);
        },
        enrolPMWhole: function(){
            return parseInt(this.content.enrolMixedWhole ? this.content.enrolMixedWhole : 0) +
                parseInt(this.content.enrolAMWhole ? this.content.enrolAMWhole : 0);
        },
        enrolPM: function(){
            return parseInt(this.content.enrolPMHalf ? this.content.enrolPMHalf : 0) +
                parseInt(this.enrolPMWhole());
        },
        enrolSummary: function(){
            return parseInt(this.enrolAM()) +
                parseInt(this.enrolPM()) -
                parseInt(this.enrolPMWhole());
        },
        quotaVacancy: function(){
            return parseInt(this.quotaSummary()) -
                parseInt(this.enrolSummary());
        },
        assign: function () {
            submitForm.EnrolQuota[0] = this.content;
        },
        confirmData: function () {
            var dbData;
            var self = this;
            var month = document.getElementById("reportMonth").value;
            var year = document.getElementById("reportYear").value;
            $.ajax({
                url: "/home/cees_kis/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStudentEnrolQuotaData',
                    Year: year,
                    Month: month
                },
                success: function (data) {
                    // console.log(data);
                    dbData = data;
                }
            })
                .done(function() {
                    self.content.enrolMixedHalf = dbData.Enrol_Mixed_Half;
                    self.content.enrolMixedWhole = dbData.Enrol_Mixed_Whole;
                    self.content.enrolAMHalf = dbData.Enrol_AM_Half;
                    self.content.enrolAMWhole = dbData.Enrol_AM_Whole;
                    self.content.enrolPMHalf = dbData.Enrol_PM_Half;
                });
        },
        add: function(){},
        addRow: function(){},
        addRows: function(){}
    },
    mounted: function () {
        $('#tbl_enrolQuota tr').find('td, th').css('border','1px solid lightgrey');
        $('#tbl_enrolQuota tr').find('td:first, td:last, th:first, th:last').css('border','none');

        eventHub.$on('collect-info', this.assign);

        eventHub.$on('confirm-info-enrolQuotaModal', this.confirmData);
    }
});

Vue.component('enrol-quota-old', {
    template: '<div id="enrolQuotaDiv">\
    <table class="table report-style" id="tbl_enrolQuota">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 3%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="12">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#enrolQuotaModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromAccountManagement+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="2" rowspan="2" style="text-align:center;vertical-align:middle;">'+jsLang.ClassroomTotal+' <br/>(' + jsLang.AbleToCreateClass + ')</th>\
        <th colspan="8" style="text-align:center">'+jsLang.ApprovedQuota+' (a)</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="4" style="text-align:center">'+jsLang.TimeSlot.AM+'</th>\
        <th colspan="3" style="text-align:center">'+jsLang.TimeSlot.PM+'</th>\
        <th rowspan="2" style="text-align:center;vertical-align:middle;">'+jsLang.Summary+'</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th>&nbsp;</th>\
        <th>'+jsLang.HalfDaySystem+'</th>\
        <th>'+jsLang.WholeDaySystem+'</th>\
        <th>'+jsLang.MixedClass+'</th>\
        <th>'+jsLang.HalfDayClass+'</th>\
        <th>'+jsLang.WholeDayClass+'</th>\
        <th>'+jsLang.Summarize+'</th>\
        <th>'+jsLang.HalfDayClass+'</th>\
        <th>'+jsLang.WholeDayClass+'</th>\
        <th>'+jsLang.Summarize+'</th>\
        <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
    <tr>\
        <td></td>\
        <td>\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.classroomHalf">\
            <span v-else>{{content.classroomHalf}}</span>\
        </td>\
        <td>\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.classroomWhole">\
            <span v-else>{{content.classroomWhole}}</span>\
        </td>\
        <td>\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaAMMixed">\
            <span v-else>{{content.quotaAMMixed}}</span>\
        </td>\
        <td>\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaAMHalf">\
            <span v-else>{{content.quotaAMHalf}}</span>\
        </td>\
        <td>\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaAMWhole">\
            <span v-else>{{content.quotaAMWhole}}</span>\
        </td>\
        <td>\
            <span>{{quotaAM()}}</span>\
        </td>\
        <td>\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaPMHalf">\
            <span v-else>{{content.quotaPMHalf}}</span>\
        </td>\
        <td>\
            <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.quotaPMWhole">\
            <span v-else>{{content.quotaPMWhole}}</span>\
        </td>\
        <td>\
            <span>{{quotaPM()}}</span>\
        </td>\
        <td style="text-align: center">\
            <span>{{quotaSummary()}}</span>\
        </td>\
    </tr>\
    </tbody>\
    </table>\
    <table class="table report-style" id="tbl_enrolQuota">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 3%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th></th>\
        <th colspan="9" style="text-align:center">'+jsLang.ApprovedEnrolment+' (b)</th>\
        <th rowspan="3" style="text-align:center;vertical-align:middle;">'+jsLang.QuotaVacancy+' <br/>(a) - (b) = (c)</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="2" style="text-align:center">'+jsLang.MixedClass+'</th>\
        <th colspan="3" style="text-align:center">'+jsLang.TimeSlot.AM+'</th>\
        <th colspan="3" style="text-align:center">'+jsLang.TimeSlot.PM+'</th>\
        <th rowspan="2" style="text-align:center;vertical-align:middle;">'+jsLang.Summary+'</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th>&nbsp;</th>\
        <th>'+jsLang.HalfDay+' (i)</th>\
        <th>'+jsLang.WholeDay+' (ii)</th>\
        <th>'+jsLang.HalfDayClass+' (iii)</th>\
        <th>'+jsLang.WholeDayClass+' (iv)</th>\
        <th>'+jsLang.Summarize+'</th>\
        <th>'+jsLang.HalfDayClass+' (v)</th>\
        <th>'+jsLang.WholeDayClass+'</th>\
        <th>'+jsLang.Summarize+'</th>\
        <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
    <tr>\
        <td></td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolMixedHalf">\
        </td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolMixedWhole">\
        </td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolAMHalf">\
        </td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolAMWhole">\
        </td>\
        <td>{{enrolAM()}}</td>\
        <td>\
            <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolPMHalf">\
        </td>\
        <td>{{enrolPMWhole()}}</td>\
        <td>{{enrolPM()}}</td>\
        <td style="text-align: center">{{enrolSummary()}}</td>\
        <td style="text-align: center">{{quotaVacancy()}}</td>\
        <td></td>\
    </tr>\
    </tbody>\
    </table>\
    </div>',
    mixins: [myMixin],
    data: function () {
        return {
            year: currYear,
            month: currMonth,
            content: formData.EnrolQuota[0] ? formData.EnrolQuota[0] : {
                classroomHalf: 0,
                classroomWhole: 0,
                quotaAMMixed: 0,
                quotaAMHalf: 0,
                quotaAMWhole: 0,
                quotaPMHalf: 0,
                quotaPMWhole: 0,
                enrolMixedHalf: 0,
                enrolMixedWhole: 0,
                enrolAMHalf: 0,
                enrolAMWhole: 0,
                enrolPMHalf: 0,
            }
        }
    },
    computed: {
    },
    methods: {
        quotaAM: function(){
            return parseInt(this.content.quotaAMMixed ? this.content.quotaAMMixed : 0) +
                parseInt(this.content.quotaAMHalf ? this.content.quotaAMHalf : 0) +
                parseInt(this.content.quotaAMWhole ? this.content.quotaAMWhole : 0);
        },
        quotaPM: function(){
            return parseInt(this.content.quotaPMHalf ? this.content.quotaPMHalf : 0) +
                parseInt(this.content.quotaPMWhole ? this.content.quotaPMWhole : 0);
        },
        quotaSummary: function(){
            return parseInt(this.quotaAM()) +
                parseInt(this.quotaPM()) -
                parseInt(this.content.quotaPMWhole ? this.content.quotaPMWhole : 0);
        },
        enrolAM: function(){
            return parseInt(this.content.enrolMixedHalf ? this.content.enrolMixedHalf : 0) +
                parseInt(this.content.enrolMixedWhole ? this.content.enrolMixedWhole : 0) +
                parseInt(this.content.enrolAMHalf ? this.content.enrolAMHalf : 0) +
                parseInt(this.content.enrolAMWhole ? this.content.enrolAMWhole : 0);
        },
        enrolPMWhole: function(){
            return parseInt(this.content.enrolMixedWhole ? this.content.enrolMixedWhole : 0) +
                parseInt(this.content.enrolAMWhole ? this.content.enrolAMWhole : 0);
        },
        enrolPM: function(){
            return parseInt(this.content.enrolPMHalf ? this.content.enrolPMHalf : 0) +
                parseInt(this.enrolPMWhole());
        },
        enrolSummary: function(){
            return parseInt(this.enrolAM()) +
                parseInt(this.enrolPM()) -
                parseInt(this.enrolPMWhole());
        },
        quotaVacancy: function(){
            return parseInt(this.quotaSummary()) -
                parseInt(this.enrolSummary());
        },
        assign: function () {
            submitForm.EnrolQuota[0] = this.content;
        },
        confirmData: function () {
            var dbData;
            var self = this;
            var month = document.getElementById("reportMonth").value;
            var year = document.getElementById("reportYear").value;
            $.ajax({
                url: "/home/cees_kis/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStudentEnrolQuotaData',
                    Year: year,
                    Month: month
                },
                success: function (data) {
                    // console.log(data);
                    dbData = data;
                }
            })
            .done(function() {
                self.content.enrolMixedHalf = dbData.Enrol_Mixed_Half;
                self.content.enrolMixedWhole = dbData.Enrol_Mixed_Whole;
                self.content.enrolAMHalf = dbData.Enrol_AM_Half;
                self.content.enrolAMWhole = dbData.Enrol_AM_Whole;
                self.content.enrolPMHalf = dbData.Enrol_PM_Half;
            });
        },
        add: function(){},
        addRow: function(){},
        addRows: function(){}
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);

        eventHub.$on('confirm-info-enrolQuotaModal', this.confirmData);
    }
});

Vue.component('enrolQuotaModal', {
    template: '\
     <div class="modal-body">\
        <div class="browse-folder-container">\
            <ul><li>'+jsLang.Month + '/ '+jsLang.Year+': {{month}}/{{year}}</li></ul>\
            <table class="table report-style" >\
                <colgroup>\
                    <col style="width: 32%;">\
                    <col style="width: 68%;">\
                </colgroup>\
                <tbody>\
                    <tr>\
                     <td>'+jsLang.ApprovedEnrolment+'</td>\
                     <td>{{content.StudentNumber}}</td>\
                   </tr>\
               </tbody>\
            </table>\
            <p class="question text-center">'+jsLang.EnsureImport+'</p>\
         </div>\
      </div>\
        ',
    data: function () {

        return {
            content: {
                StudentNumber: 0
            }
        }
    },
    computed: {
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    mounted: function () {
        var dbData;
        var self = this;

        $.ajax({
            url: "/home/cees_kis/api",
            type:"GET",
            datatype: 'json',
            data: {
                Method: 'GetStudentEnrolQuotaData',
                Year: this.year,
                Month: this.month
            },
            success: function (data) {
                dbData = data;
            }
        })
            .done(function() {
                self.content.StudentNumber = dbData.StudentCount;
            });
    }

});