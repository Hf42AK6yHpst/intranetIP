Vue.component('advanced-notice', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 2%">\
    <col style="width: 16%">\
    <col style="width: 16%">\
    <col style="width: 16%">\
    <col style="width: 16%">\
    <col style="width: 16%">\
    <col style="width: 16%">\
    <col style="width: 2%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="8">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#advancedNoticeModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>#</th>\
    <th>'+jsLang.Date+'</th>\
    <th>'+jsLang.Time+'</th>\
    <th>'+jsLang.Venue+'</th>\
    <th>'+jsLang.EventName+'</th>\
    <th>'+jsLang.OrganizedBy+'</th>\
    <th>'+jsLang.CeremonyGuest+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
                <div is="multiple-date-select" v-bind:id="key" v-bind:type="\'advanced_notice\'" v-bind:sourceKey="\'date\'">\
                </div>\
            </td>\
            <td>\
                <div is="time-select" v-bind:id="key" v-bind:type="\'advanced_notice\'">\
                </div>\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputVenue\'+key" v-model="content.venue">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inpuEventName\'+key" v-model="content.eventName">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputOrganizedBy\'+key" v-model="content.organizedBy">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputGuest\'+key" v-model="content.guest">\
            </td>\
            <td>\
            <div is="del-btn" v-bind:remove="remove" v-bind:target="key">\
            </div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="add"></tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.AdvancedNotice.slice(),
            row: {
                date: '',
                time: '',
                venue: '',
                eventName: '',
                organizedBy: '',
                guest: ''
            }
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            submitForm.AdvancedNotice = this.contents;
        },
        confirmData: function(){
            this.confirmDataNow(this, 'advancedNotice', {
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        date: data[0],
                        time: data[1],
                        venue: data[2],
                        eventName: data[3],
                        organizedBy: data[4],
                        guest: data[5]
                    };
                }
            });
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);

        eventHub.$on('confirm-info-advancedNoticeModal', this.confirmData);
    }
});

Vue.component('advancedNoticeModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}}: <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Date,
                jsLang.Time,
                jsLang.Venue,
                jsLang.EventName,
                jsLang.OrganizedBy,
                jsLang.CeremonyGuest
            ],
            popup:[
                null
            ],
            reminder:[
                "YYYY-MM-DD or DD/MM/YYYY",
                "HH:MM",
                null,
                null,
                null,
                null
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/advancedNotice",
            fileFieldId: "advancedNoticeFile"
        }
    },
    mounted: function(){
    }
});