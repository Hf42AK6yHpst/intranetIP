Vue.component('school-administration', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 19%">\
    <col style="width: 74%">\
    <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="3">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#schoolAdministrationModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>#</th>\
    <th>'+jsLang.Date+'</th>\
    <th>'+jsLang.AdministrationParticulars+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
            <div is="multiple-date-select" v-bind:id="key" v-bind:type="\'school_adminstration\'" v-bind:sourceKey="\'date\'">\
            </div>\
            </td>\
            <td>\
            <input type="text" class="form-control " id="\'inputParticulars\' + key " v-model="content.particulars">\
            </td>\
            <td>\
            <div is="del-btn" v-bind:remove="remove" v-bind:target="key" v-bind:location="contents">\
            </div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="add"></tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.SchoolAdministration.slice(),
            row: {
                date: '',
                particulars: ''
            }
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            //  console.log('teacher-course is assigning');
            submitForm.SchoolAdministration = this.contents;
        },
        confirmData: function(){
            this.confirmDataNow(this, 'schoolAdministration', {
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    data[1] = (data[1] != "") ? data[1] : "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        date: data[0],
                        particulars: data[1]
                    };
                }
            });
            /*
            var self = this;
            var uploadfile = $('#schoolAdministrationFile')[0].files[0];
            //File reading
            var fr = new FileReader();
            var resultAry = [];
            fr.onload = () => {
                var allResults = fr.result.split("\n");
                for( var x in allResults) {
                    if(allResults[x]!="")
                        resultAry.push(allResults[x].split(','));
                }
                //File reading
                $.ajax({
                    url: "/home/cees/monthlyreport/ajaxImport/schoolAdministration",
                    method: "POST",
                    type: "POST",
                    data: {"content" : resultAry},
                    success: function(data){
                        var result = JSON.parse(data);
                        if(!window.prop){
                            window.prop = {};
                            window.prop.tempData = [];
                        }
                        window.prop.tempData['schoolAdministrationTempData'] = result;
                        Vue.component('schoolAdministrationConfirmTable', {
                            template: self.$root.confirmTableTemplate,
                            data: function(){
                                return{
                                    TotalError : jsLang.TotalError,
                                    Remark : jsLang.Remark,
                                    result : {},
                                    error : 0,
                                    errorLang: self.$root.errorLang
                                };
                            },
                            mounted: function(){
                                var data = JSON.parse(JSON.stringify(window.prop.tempData['schoolAdministrationTempData'].result));
                                var here = this;
                                here.error = window.prop.tempData['schoolAdministrationTempData'].error;
                                here.result = data;
                                if(here.error != 0){
                                    $('button.btn-prev').show();
                                    $('button.btn-confirm').hide();
                                }
                                for(var x in data.data){
                                    data.data[x][0] = (data.data[x][0] != "") ? data.data[x][0] : "Error";
                                    data.data[x][1] = (data.data[x][1] != "") ? data.data[x][1] : "Error";
                                }
                                eventHub.$once('confirm-data-schoolAdministrationModal', function(){
                                    if(here.error == 0){
                                        //eventHub.$emit('add-data-appointmentModal');
                                        var newArr = [];
                                        var data = window.prop.tempData['schoolAdministrationTempData'].result;
                                        for(var x in data.data){
                                            newArr.push({
                                                date: data.data[x][0],
                                                particulars: data.data[x][1]
                                            });
                                        }
                                        self.addRows(newArr, self.contents);
                                        eventHub.$emit('close-schoolAdministrationModal');
                                        window.prop.tempData['schoolAdministrationTempData'] = null;
                                    }
                                });
                            },
                        });
                        eventHub.$emit('confirm-table-schoolAdministrationModal');
                    }
                });
            }
            fr.readAsText(uploadfile);*/
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);

        eventHub.$on('confirm-info-schoolAdministrationModal', this.confirmData);
    }
});

Vue.component('schoolAdministrationModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Date,
                jsLang.AdministrationParticulars,
            ],
            popup:[
                null,
                null
            ],
            reminder:[
                "YYYY-MM-DD or DD/MM/YYYY",
                null
            ],
            filetemp: "/home/cees/monthlyreport/template/schoolAdministration",
            fileFieldId: "schoolAdministrationFile"
        }
    },
    mounted: function(){
    }
});