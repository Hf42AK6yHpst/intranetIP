Vue.component('teacher-sick-leave', {
    template: '\
    <div>\
     <div class="import-menu">\
        <ul>\
            <li v-if="'+SickLeave+' == 1"><a href="#" data-toggle="modal" data-target="#sickLeaveModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromStaffAttendance+'</a></li>\
            <li><a href="#" data-toggle="modal" data-target="#sickLeaveCSVModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
        </ul>\
     </div>\
     <div>\
            <table class="table report-style" id="tbl_other">\
            <colgroup>\
                <col style="width: 3%">\
                <col style="width: 20%">\
                <col style="width: 20%">\
                <col style="width: 20%">\
                <col style="width: 13%">\
                <col style="width: 10%">\
                <col style="width: 10%">\
                <col style="width: 4%">\
            </colgroup>\
            <thead>\
            <tr>\
                <th>#</th>\
                <th>'+jsLang.StaffName +'</th>\
                <th>'+jsLang.Position +'</th>\
                <th>'+jsLang.Date +'</th>\
                <th>'+jsLang.SupportingCertificate + jsLang.SupportingCertificateSickLeaveMsg +'</th>\
                <th>'+jsLang.Absent +'</th>\
                <th>'+jsLang.AbsentSeptember +'</th>\
                <th>&nbsp;</th>\
            </tr>\
            </thead>\
            <tbody>\
                <tr v-for="(content, key) in contents" >\
                    <td>{{ key + 1 }}</td>\
                    <td>\
                         <input type="text" class="form-control" id="\'inputTeachername\'+ key" v-model="content.teacherName" v-bind:type="\'teacher_sick_leave\'">\
                    </td>\
                    <td>\
                        <select v-model="content.positionId" :id="\'_positionselect_\' + key" class="selectpicker" data-live-search="true">\
                            <option disabled>'+jsLang.Select+'</option>\
                            <option v-for="(position,id) in positions" :value="id">{{ position }}</option>\
                        </select>\
                    </td>\
                    <td>\
                        <div is="multiple-date-select" v-model="content.date" v-bind:id="key" v-bind:type="\'teacher_sick_leave\'" v-bind:sourceKey="\'date\'">\
                        </div>\
                    </td>\
                    <td>\
                        <select v-model="content.WithCer" :id="\'_withcerselect_\' + key" class="selectpicker" data-live-search="true">\
                            <option disabled>'+jsLang.Select+'</option>\
                            <option v-for="(text,id) in WithCertification" :value="id">{{ text }}</option>\
                        </select>\
                    </td>\
                    <td>\
                       <input :id="\'dayselect_\' + key" type="number" step="0.5" min="0" max="30" v-model="content.thisMonthDay">\
                    </td>\
                    <td>\
                       <div v-if="content.lastMonthDay == \'--\'"  >\
                         {{content.thisMonthDay}}\
                        </div>\
                        <div v-else v-bind="countSickLeaveDate(content.teacherName,key)">\
                            {{Number(content.thisMonthDay) + Number(content.lastMonthDay)}}\
                        </div>\
                    </td>\
                    <td>\
                        <div is="del-btn" v-bind:remove="remove" v-bind:target="key" v-bind:location="contents">\
                    </div>\
                    </td>\
                </tr>\
                <tr is="add-row" v-bind:add="add"></tr>\
            </tbody>\
            </table>\
           </div>\
      </div>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.TeacherSickLeave.slice(),
      //      sickleaveday: this.$root.sickleaveday,
            row: {
                teacherName: '',
                positionId: '',
                date: '',
                thisMonthDay: '',
                WithCer: '',
                lastMonthDay: ''
            },
            positions: this.$root.positions,
            WithCertification: this.$root.WithCertification
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            submitForm.TeacherSickLeave = this.contents;
        },
        countSickLeaveDate: function (teachername,id){
            if(teachername != '') { // avoid empty name
                var self = this;
                var reportID = document.getElementById("reportID").value;

                $.ajax({
                    type: 'POST',
                    url: '/home/cees/monthlyreport/ajax',
                    data: {
                        reportId: reportID,
                        Method: 'getSickLeaveData',
                        TeacherName: teachername
                    },
                    //  dataType: 'json',
                    success: function (data) {

                        if (data == '--') {
                            sum = parseInt(0);
                        } else {
                            sum = Number(data)
                        }

                        //            self.contents[id].lastMonthDay = sum;

                    }
                }).done(function () {
                    self.contents[id].lastMonthDay = sum;

                });
            }
        },
        confirmData: function () {
            var dbData;
            var self = this;
            var month = document.getElementById("reportMonth").value;
            var year = document.getElementById("reportYear").value;
            $.ajax({
                url: "/home/cees_kis/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStaffSickLeaveMonthlyData',
                    Year: year,
                    Month: month
                },
                success: function (data) {
                    dbData = data;
                }
            })
                .done(function() {
                    teachers = self.$root.teachers;
                    _.forEach(dbData, function (sickLeaveDB, key) {
                        // if(parseInt(sickLeaveDB.CertificateCount)==0){
                        //     dbWithCer = false;
                        // }else{
                        //     dbWithCer = true;
                        // }
                        dbWithCer = parseInt(sickLeaveDB.CertificateCount);

                        sickLeaveteacherid = sickLeaveDB.TeacherID;
                        var teacherName='';
                        _.forEach(teachers, function (teacherList, key) {
                            _.forEach(teacherList, function (teacherid, key) {
                                if (sickLeaveteacherid == teacherid.id) {

                                    teacherName = teacherid.name;
                                }
                            })
                        });

                        this.importdata = [{
                                teacherName: teacherName,
                                positionId: sickLeaveDB.PositionId,
                                date: sickLeaveDB.AbsentDates,
                                thisMonthDay:Number(sickLeaveDB.AbsentCount),
                                WithCer: dbWithCer > 0 ? '1' : '0',
                                lastMonthDay:''
                            }];

                       self.addrow(this.importdata);

                    });

                });
        },
        confirmData2 : function(){
            self = this;
            this.confirmDataNow(this, 'sickLeaveCSV', {
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    data[1] = jsLang.PositionName[data[1]];
                    if(data[1] == null)
                        data[1] = "Error";
                    data[2] = (data[2] != "") ? data[2] : "Error";
                    data[3] = self.$root.WithCertification[data[3]];
                    if(data[3]==null)
                        data[3] = "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        teacherName: data[0],
                        positionId: data[1],
                        date: data[2],
                        WithCer: data[3],
                        thisMonthDay: data[4],
                        lastMonthDay: ''
                    };
                }
            });
        },
        addrow: function(dbData){
            var self = this;
            var reportID = document.getElementById("reportID").value;
            $.ajax({
                type: 'POST',
                url: '/home/cees_kis/monthlyreport/ajax',
                data: {
                    reportId: reportID,
                    Method: 'getSickLeaveData',
                    TeacherName : dbData[0].teacherId
                },
                //  dataType: 'json',
                success: function (data) {

                    if (data == '--'){
                        sum = parseInt(0);
                    }else{
                        sum = Number(data)
                    }

                    dbData[0].lastMonthDay = sum;
                    self.addRows(dbData, self.contents);
                    self.$nextTick(function () {
                        $('.selectpicker').selectpicker();
                    });
                }
            }).done(function() {
          //      dbData[0].lastMonthDay = sum;
            });


        },
        selectedTeacher:function(seletedId,id){
            var self = this;
            var sum ;
            var reportID = document.getElementById("reportID").value;
            $.ajax({
                type: 'POST',
                url: '/home/cees_kis/api',
                data: {
                    reportId: reportID,
                    Method: 'getSickLeaveData',
                    TeacherID : seletedId
                },
                //  dataType: 'json',
                success: function (data) {

                    if (data == '--'){
                        sum = 0;
                    }else{
                        sum = Number(data)
                    }

                }
            }).done(function() {

                self.contents[id].teacherId = seletedId;
                self.contents[id].lastMonthDay = sum;
            });


        }
    },
    mounted: function () {

        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-sickLeaveModal', this.confirmData);
        eventHub.$on('selected-id', this.selectedTeacher);

        eventHub.$on('confirm-info-sickLeaveCSVModal', this.confirmData2);

    }
});

Vue.component('sickLeaveModal', {
    template: '\
     <div class="modal-body">\
        <div class="browse-folder-container">\
            <ul><li>'+jsLang.Month + '/ '+jsLang.Year+': {{month}}/{{year}}</li></ul>\
            <table class="table report-style" >\
                <colgroup>\
                    <col style="width: 32%;">\
                    <col style="width: 68%;">\
                </colgroup>\
                <tbody>\
                    <tr>\
                     <td>'+jsLang.SickLeaveNo+'</td>\
                     <td>{{Teachernumber}}</td>\
                   </tr>\
                   <tr>\
                    <td>'+jsLang.SickLeaveName+'</td>\
                    <td>{{TeacherName}}</td>\
                   </tr>\
               </tbody>\
            </table>\
            <p class="question text-center">'+jsLang.EnsureImport+'</p>\
         </div>\
      </div>\
        ',
    data: function () {

        return {
            sickLeaveModalcontents:
                [{
                    TeacherID: '',
                    AbsentCount: '',
                    CertificateCount: '',
                    AbsentCountSinceSeptember: ''
                }]
        }
    },
    computed: {
        Teachernumber: function () {
            return _.size(this.sickLeaveModalcontents);
        },
        TeacherName: function(){
            teachers = this.$root.teachers;
            var teachername = [];
            _.forEach(this.sickLeaveModalcontents, function (value, key) {
                sickLeaveteacherid = value.TeacherID;
                _.forEach(teachers, function(teacherSet, index){
                    _.forEach(teacherSet, function (teacherid, key) {
                        if(sickLeaveteacherid == teacherid.id){
                            teachername.push(teacherid.name);
                        }
                    });
                })

            });
            if(teachername == ''){
                return '---';
            }else {
                return teachername.join(",");
            }
        },
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    mounted: function () {
        var dbData;
        var self = this;

        $.ajax({
            url: "/home/cees_kis/api",
            type:"GET",
            datatype: 'json',
            data: {
                Method: 'GetStaffSickLeaveMonthlyData',
                Year: this.year,
                Month: this.month
            },
            success: function (data) {
                dbData = data;
            }
        })
            .done(function() {
                self.sickLeaveModalcontents = dbData;
            });
    }

});

Vue.component('sickLeaveCSVModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.StaffName,
                jsLang.Position,
                jsLang.Date,
                jsLang.SupportingCertificate,
                jsLang.Absent
            ],
            popup:[
                null,
                { title : jsLang.Position, content: this.$root.positions},
                null,
                { title : jsLang.SupportingCertificate, content: this.$root.WithCertification},
                null
            ],
            reminder:[
                null,
                null,
                "YYYY-MM-DD or DD/MM/YYYY",
                null,
                null
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/sickLeaveCSV",
            fileFieldId: "sickLeaveCSVFile"
        }
    },
    mounted: function(){
    }
});