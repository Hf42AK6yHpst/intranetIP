Vue.component('student-eca', {
    template: '\
    <div>\
        <span style="margin-left: 40px;"><i>'+jsLang.PleaseReferToECA+'</i></span>\
    </div>\
    <!--<div>\
        <div class="import-menu">\
            <ul>\
                <li  v-if="'+ enrollment +' == 1"><a href="#" data-toggle="modal" data-target="#extraActivitiesInternalModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromEnrolment+'</a></li>\
                <li><a href="#" data-toggle="modal" data-target="#studentECAModal" @click="internal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
            </ul>\
        </div>\
    	<table class="table report-style tbl_extraActivities" id="tbl_extraActivitiesA">\
            <colgroup>\
                <col style="width: 3%">\
                <col style="width: 19%">\
                <col style="width: 20%">\
                <col style="width: 40%">\
                <col style="width: 18%">\
                <col style="width: 4%">\
                </colgroup>\
        <thead>\
            <tr>\
                <th class="subTitle" colspan="5">(A) '+jsLang.Internal+'</th>\
            </tr>\
             <tr>\
                <th>#</th>\
                <th>'+jsLang.Date+'</th>\
                <th>'+jsLang.ActivityCategory+'</th>\
                <th>'+jsLang.ActivityName+'</th>\
                <th>'+jsLang.participantsNo+'</th>\
                <th>&nbsp;</th>\
            </tr>\
        </thead>\
        <tbody>\
            <tr v-for="(content, key) in contents.internal" >\
                <td>{{ key + 1 }}</td>\
                <td>\
                    <div is="eca-multi-date-input" v-bind:id="key" v-bind:type="\'student_eca_i\'" v-model="content.date" v-bind:TypeKey="\'internal\'">\
                    </div>\
                </td>\
                <td>\
                 <div is="category-multi-select" v-bind:id="key" v-bind:type="\'internal\'"> </div>\
                 </td>\
                <td>\
                     <input type="text" class="form-control " id="\'inputActivityName\' + key" v-model="content.activityName">\
                </td>\
                <td>\
                 <input type="text" class="form-control " id="\'inputNoParticipants\' + key" v-model="content.participantsNo">\
                </td>\
                <td>\
                    <div is="del-btn" v-bind:remove="removei"  v-bind:target="key"  v-bind:location="contents.internal">\
                    </div>\
                </td>\
                </tr>\
                <tr is="add-row" v-bind:add="addi" v-bind:model="\'internal\'"></tr>\
               </tbody>\
        </table>\
        <div class="import-menu">\
            <ul>\
                <li  v-if="'+ enrollment +' == 1"><a href="#" data-toggle="modal" data-target="#extraActivitiesExternalModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromEnrolment+'</a></li>\
                <li><a href="#" data-toggle="modal" data-target="#studentECAModal" @click="external"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
            </ul>\
        </div>\
        <table class="table report-style tbl_extraActivities coutinue" id="tbl_extraActivitiesB">\
            <colgroup>\
            <col style="width: 3%">\
            <col style="width: 19%">\
            <col style="width: 20%">\
            <col style="width: 40%">\
            <col style="width: 18%">\
            <col style="width: 4%">\
            </colgroup>\
            <thead>\
               <tr>\
                   <th class="subTitle" colspan="5">(B) '+jsLang.External+'</th>\
               </tr> \
                   <tr>\
                      <th>#</th>\
                      <th>'+jsLang.Date+'</th>\
                      <th>'+jsLang.ActivityCategory+'</th>\
                      <th>'+jsLang.ActivityName+'</th>\
                      <th>'+jsLang.participantsNo +'</th>\
                      <th>&nbsp;</th>\
                    </tr>\
            </thead>\
            <tbody>\
                <tr v-for="(content, key) in contents.external" >\
                    <td>{{ key + 1 }}</td>\
                    <td>\
                        <div is="eca-multi-date-input" v-bind:id="key" v-bind:type="\'student_eca_e\'" v-model="content.date"  v-bind:TypeKey="\'external\'">\
                        </div>\
                    </td>\
                    <td>\
                        <div is="category-multi-select" v-bind:id="key" v-bind:type="\'external\'"></div>\
                     </td>\
                    <td>\
                        <input type="text" class="form-control" id="\'inputActivityName\' + key" v-model="content.activityName">\
                    </td>\
                    <td>\
                        <input type="text" class="form-control" id="\'inputNoParticipants\' + key" v-model="content.participantsNo">\
                    </td>\
                    <td>\
                        <div is="del-btn" v-bind:remove="removee" v-bind:target="key" v-bind:location="contents.external">\
                        </div>\
                    </td>\
                </tr>\
                <tr is="add-row" v-bind:add="adde" v-bind:model="\'external\'"></tr>\
            </tbody>\
            </table>\
    	</div>-->\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: {
                internal: formData.StudentEca.internal.slice(),
                external: formData.StudentEca.external.slice()
            },
            ecacategories: this.$root.categories,
            ecacategoriesArr : this.$root.categoriesArray,
            row: {
                internal: {
                    date: '',
                    categoryID: [],
                    activityName: '',
                    type: 'I',
                    participantsNo: ''
                },
                external: {
                    date: '',
                    categoryID: [],
                    activityName: '',
                    type: 'E',
                    participantsNo: ''
                }
            }
        }
    },
    computed: {
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    methods: {
        addi: function (e) {
            // this.addNewRows(e.target.value, this.row.internal, this.contents.internal);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        adde: function (e) {
            // this.addNewRows(e.target.value, this.row.external, this.contents.external);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        removei: function (key) {
            this.removeRow(key, this.contents.internal)
        },
        removee: function (key) {
            this.removeRow(key, this.contents.external)
        },
        assign: function () {
            //  console.log('teacher-course is assigning');
            submitForm.StudentEca = this.contents;
        },
        confirmInternalData: function (){
            var dbData;
            var self = this;
            var AcademicYearID = document.getElementById("reportAcademicYearID").value;
            $.ajax({
                url: "/home/cees/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStudentECAData',
                    AcademicYearID:AcademicYearID,
                    IsSchoolActivity: 1,
                    month: this.month,
                    year: this.year
                },
                success: function (data) {
                    dbData = data;
                }
            })
            .done(function() {
                _.forEach(dbData, function (internalECA, key) {
                    this.importdata = [{
                        date: internalECA.date,
                        activityName: internalECA.activityName,
                        type: 'I',
                        categoryID: internalECA.categoryID.split(","),
                        participantsNo: ''+internalECA.participantsNo
                    }];
                    self.addInternalrow(this.importdata);
                });
            });
        },
        addInternalrow :function(dbData){
            this.addRows(dbData, this.contents.internal);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        confirmExternalData: function (){
            var dbData;
            var self = this;
            var AcademicYearID = document.getElementById("reportAcademicYearID").value;
            $.ajax({
                url: "/home/cees/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStudentECAData',
                    AcademicYearID:AcademicYearID,
                    month: this.month,
                    year: this.year
                },
                success: function (data) {
                    dbData = data;
                }
            })
            .done(function() {
                _.forEach(dbData, function (externalECA, key) {
                    this.importdata = [{
                        date: externalECA.date,
                        activityName: externalECA.activityName,
                        type: 'E',
                        categoryID: externalECA.categoryID.split(","),
                        participantsNo: ''+externalECA.participantsNo
                    }];

                    self.addExternalrow(this.importdata);
                });
            });
        },
        addExternalrow :function(dbData){
            this.addRows(dbData, this.contents.external);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        confirmData: function(){
            var self = this;
            var tableType = self.$root.ecaToggle ? 'internal' : 'external';
            this.confirmDataNow(this, 'studentECA', {
                arrayColumn: [1],
                contents: self.contents[tableType],
                checkResult: function(result){
                    var arrayCheck = null;
                    for(var z in result[1]){
                        if(result[1][z] != 1){
                            if(arrayCheck==null) arrayCheck = {};
                            arrayCheck[z] = 0;
                        }
                    }
                    if(arrayCheck==null)
                        result[1] = 1;
                    return result;
                },
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    for(var z in data[1]){
                        data[1][z] = self.ecacategoriesArr[data[1][z]];
                    }
                    data[2] = (data[2] != "") ? data[2] : "Error";
                    data[3] = (data[3] != "") ? data[3] : "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        date: data[0],
                        categoryID: data[1],
                        activityName: data[2],
                        type: self.$root.ecaToggle ? 'I' : 'E',
                        participantsNo: data[3]
                    };
                }
            });
            /*var self = this;
            var uploadfile = $('#studentECAFile')[0].files[0];
            //File reading
            var fr = new FileReader();
            var resultAry = [];
            fr.onload = () => {
                var allResults = fr.result.split("\n");
                for( var x in allResults) {
                    if(allResults[x]!="") {
                        var ar = allResults[x].split(',');
                        ar[1] = ar[1].split('|');
                        resultAry.push(ar);
                    }
                }
                //File reading
                $.ajax({
                    url: "/home/cees/monthlyreport/ajaxImport/studentECA",
                    method: "POST",
                    type: "POST",
                    data: {"content" : resultAry},
                    success: function(data){
                        var result = JSON.parse(data);
                        if(!window.prop){
                            window.prop = {};
                            window.prop.tempData = [];
                        }
                        window.prop.tempData['studentECATempData'] = result;
                        Vue.component('studentECAConfirmTable', {
                            template: self.$root.confirmTableTemplate,
                            data: function(){
                                return{
                                    TotalError : jsLang.TotalError,
                                    Remark : jsLang.Remark,
                                    result : {},
                                    error : 0,
                                    errorLang: self.$root.errorLang
                                };
                            },
                            mounted: function(){
                                var data = JSON.parse(JSON.stringify(window.prop.tempData['studentECATempData'].result));
                                var here = this;
                                here.error = window.prop.tempData['studentECATempData'].error;
                                here.result = data;
                                var arrayCheck = [];
                                if(here.error != 0){
                                    $('button.btn-prev').show();
                                    $('button.btn-confirm').hide();
                                }
                                for(var x in here.result.checkResult){
                                    for(var z in here.result.checkResult[x][1]){
                                        if(here.result.checkResult[x][1][z] != 1) {
                                            if(arrayCheck[x]==null) arrayCheck[x] = {};
                                            arrayCheck[x][z] = 0;
                                        }
                                    }
                                    if(arrayCheck[x]==null)
                                        here.result.checkResult[x][1] = 1;
                                }
                                for(var x in data.data){
                                        data.data[x][0] = (data.data[x][0] != "") ? data.data[x][0] : "Error";
                                    for(var z in data.data[x][1]){
                                        data.data[x][1][z] = self.ecacategoriesArr[data.data[x][1][z]];
                                    }
                                    data.data[x][2] = (data.data[x][2] != "") ? data.data[x][2] : "Error";
                                    data.data[x][3] = (data.data[x][3] != "") ? data.data[x][3] : "Error";
                                }
                                eventHub.$once('confirm-data-studentECAModal', function(){
                                    if(here.error == 0){
                                        //eventHub.$emit('add-data-appointmentModal');
                                        var newArr = [];
                                        var data = window.prop.tempData['studentECATempData'].result;
                                        var type = self.$root.ecaToggle ? 'I' : 'E';
                                        for(var x in data.data){
                                            newArr.push({
                                                date: data.data[x][0],
                                                categoryID: data.data[x][1],
                                                activityName: data.data[x][2],
                                                type: type,
                                                participantsNo: data.data[x][3]
                                            });
                                        }
                                        if(self.$root.ecaToggle) {
                                            self.addRows(newArr, self.contents.internal);
                                        } else {
                                            self.addRows(newArr, self.contents.external)
                                        }
                                        this.$nextTick(function () {
                                            $('.selectpicker').selectpicker();
                                        });
                                        eventHub.$emit('close-studentECAModal');
                                        window.prop.tempData['studentECATempData'] = null;
                                    }
                                });
                            },
                        });
                        eventHub.$emit('confirm-table-studentECAModal');
                    }
                });
            }
            fr.readAsText(uploadfile);*/
        },
        internal: function(){
            eventHub.$emit('toggle-ECAModal-internal');
        },
        external: function(){
            eventHub.$emit('toggle-ECAModal-external');
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-extraActivitiesInternalModal', this.confirmInternalData);
        eventHub.$on('confirm-info-extraActivitiesExternalModal', this.confirmExternalData);

        eventHub.$on('confirm-info-studentECAModal', this.confirmData);
    }
});

Vue.component('extraActivitiesInternalModal', {
    template: '\
     <div class="modal-body">\
        <div class="browse-folder-container">\
            <ul>\
                <li>'+jsLang.Month+'/'+jsLang.Year+': {{month}}/{{year}}</li>\
                <li>'+jsLang.Activity+': '+jsLang.Internal+'</li>\
                <li>'+jsLang.participantsNo+': {{totalStudent}}</li>\
            </ul>\
            <table class="table report-style" >\
                 <colgroup>\
                    <col style="width: 29%;">\
                    <col style="width: 30%;">\
                    <col style="width: 25%;">\
                    <col style="width: 16%;">\
                 </colgroup>\
                 <thead>\
                     <td>'+jsLang.Date+'</td>\
                     <td>'+jsLang.ActivityCategory+'</td>\
                     <td>'+jsLang.ActivityName+'</td>\
                     <td>'+jsLang.participantsNo+'</td>\
                 </thead>\
                 <tbody>\
                   <tr v-for="(content, key) in extraActivitiesInternalModalcontents">\
                     <td>{{content.date}}</td>\
                     <td>{{content.categoryID}}</td>\
                     <td>{{content.activityName}}</td>\
                     <td>{{content.participantsNo}}</td>\
                   </tr>\
                </tbody>\
            </table>\
            <p class="question text-center">'+jsLang.EnsureImport+'</p>\
         </div>\
      </div>\
        ',
    data: function () {
        return {
            contents: {
                internal: formData.StudentEca.internal.slice(),
                external: formData.StudentEca.external.slice()
            },
            ecacategories: this.$root.categories,
            ecacategoriesArr : this.$root.categoriesArray,
            extraActivitiesInternalModalcontents:
                [{
                    date: '',
                    activityName: '',
                    categoryID:[],
                    type: 'I',
                    participantsNo: ''
                }]
        }
    },
    computed: {
        totalStudent: function(){
            var totalNo = 0;
            _.forEach(this.extraActivitiesInternalModalcontents, function (value, key) {
                totalNo = parseInt(totalNo)+ parseInt(value.participantsNo);
            });
            return totalNo;
        },
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    mounted: function () {
        var dbData;
        var self = this;
        var AcademicYearID = document.getElementById("reportAcademicYearID").value;
        $.ajax({
            url: "/home/cees/api",
            type:"GET",
            datatype: 'json',
            data: {
                Method: 'GetStudentECAData',
                AcademicYearID:AcademicYearID,
                IsSchoolActivity: 1,
                month: this.month,
                year: this.year
            },
            success: function (data) {
                dbData = data;
            }
        })
        .done(function() {
            if(dbData != '') {
                for(var z in dbData){
                    var dbDataObj = dbData[z];
                    if(dbDataObj != '' && dbDataObj['categoryID'] != undefined && dbDataObj['categoryID'] != '') {
                        var dbDataCatIDArr = dbDataObj['categoryID'].split(",");
                        var dbDataCatNameArr = [];
                        for(var z1 in dbDataCatIDArr){
                            dbDataCatNameArr[z1] = self.ecacategoriesArr[dbDataCatIDArr[z1]];
                        }
                        dbData[z]['categoryID'] = dbDataCatNameArr;
                    }
                }
            }
            self.extraActivitiesInternalModalcontents = dbData;
        });
    }
});

Vue.component('extraActivitiesExternalModal', {
    template: '\
     <div class="modal-body">\
        <div class="browse-folder-container">\
            <ul>\
                <li>'+jsLang.Month+'/'+jsLang.Year+': {{month}}/{{year}}</li>\
                <li>'+jsLang.Activity+': '+jsLang.External+'</li>\
                <li>'+jsLang.participantsNo+': {{totalStudent}}</li>\
            </ul>\
            <table class="table report-style" >\
                <colgroup>\
                    <col style="width: 29%;">\
                    <col style="width: 30%;">\
                    <col style="width: 25%;">\
                    <col style="width: 16%;">\
                </colgroup>\
                <thead>\
                     <td>'+jsLang.Date+'</td>\
                     <td>'+jsLang.ActivityCategory+'</td>\
                     <td>'+jsLang.ActivityName+'</td>\
                     <td>'+jsLang.participantsNo+'</td>\
                 </thead>\
                 <tbody>\
                   <tr v-for="(content, key) in extraActivitiesExternalModalcontents">\
                     <td>{{content.date}}</td>\
                     <td>{{content.categoryID}}</td>\
                     <td>{{content.activityName}}</td>\
                     <td>{{content.participantsNo}}</td>\
                   </tr>\
                </tbody>\
            </table>\
            <p class="question text-center">'+jsLang.EnsureImport+'</p>\
         </div>\
      </div>\
        ',
    data: function () {
        return {
            contents: {
                internal: formData.StudentEca.internal.slice(),
                external: formData.StudentEca.external.slice()
            },
            ecacategories: this.$root.categories,
            ecacategoriesArr : this.$root.categoriesArray,
            extraActivitiesExternalModalcontents:
                [{
                    date: '',
                    activityName: '',
                    type: 'E',
                    categoryID: [],
                    participantsNo: ''
                }]
        }
    },
    computed: {
        totalStudent: function(){
            var totalNo = 0;
            _.forEach(this.extraActivitiesExternalModalcontents, function (value, key) {
                totalNo = parseInt(totalNo) + parseInt(value.participantsNo);
            });
            return totalNo;
        },
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    mounted: function () {
        var dbData;
        var self = this;
        var AcademicYearID = document.getElementById("reportAcademicYearID").value;
        $.ajax({
            url: "/home/cees/api",
            type:"GET",
            datatype: 'json',
            data: {
                Method: 'GetStudentECAData',
                AcademicYearID:AcademicYearID,
                month: this.month,
                year: this.year
            },
            success: function (data) {
                dbData = data;
            }
        })
        .done(function() {
            if(dbData != '') {
                for(var z in dbData){
                    var dbDataObj = dbData[z];
                    if(dbDataObj != '' && dbDataObj['categoryID'] != undefined && dbDataObj['categoryID'] != '') {
                        var dbDataCatIDArr = dbDataObj['categoryID'].split(",");
                        var dbDataCatNameArr = [];
                        for(var z1 in dbDataCatIDArr){
                            dbDataCatNameArr[z1] = self.ecacategoriesArr[dbDataCatIDArr[z1]];
                        }
                        dbData[z]['categoryID'] = dbDataCatNameArr;
                    }
                }
            }
            self.extraActivitiesExternalModalcontents = dbData;
        });
    }
});

Vue.component('studentECAModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Date,
                jsLang.ActivityCategory,
                jsLang.ActivityName,
                jsLang.participantsNo
            ],
            popup:[
                null,
                { title : jsLang.ActivityCategory, content: this.$root.categoriesArray},
                null,
                null
            ],
            reminder:[
                "YYYY-MM-DD or DD/MM/YYYY",
                jsLang.DivideSymbol,
                null
            ],
            filetemp: "/home/cees/monthlyreport/template/studentECA",
            fileFieldId: "studentECAFile"
        }
    },
    mounted: function(){
    }
});