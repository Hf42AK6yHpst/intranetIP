Vue.component('enrol-vacancy', {
    template: '<table class="table report-style" id="tbl_enrolVacancy">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 18%">\
    <col style="width: 18%">\
    <col style="width: 18%">\
    <col style="width: 18%">\
    <col style="width: 18%">\
    <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="7">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#enrolVacancyModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>&nbsp;</th>\
    <th>'+jsLang.NoOfClasses + '(' + jsLang.WholeDayAndSpecial + ')</th>\
    <th>'+jsLang.NoOfClasses + '(' + jsLang.HalfDayAndNormal + ')</th>\
    <th>'+jsLang.NoOfClassroom+'</th>\
    <th>'+jsLang.ApprovedEnrolmentOfThisYear+'</th>\
    <th>'+jsLang.EnrolmentOfCorrespondMonth+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr  >\
            <td></td>\
            <td>\
                <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputwholeDay\' + key" v-model="content.wholeDay">\
                <span v-else>{{content.wholeDay}}</span>\
            </td>\
            <td>\
                <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputhalfDay\' + key" v-model="content.halfDay">\
                <span v-else>{{content.halfDay}}</span>\
            </td>\
            <td>\
                <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputClassroom\' + key" v-model="content.classroom">\
                <span v-else>{{content.classroom}}</span>\
            </td>\
            <td>\
                <input v-if="month==9" type="number" step="1" min="0" class="form-control" id="\'inputApproved\' + key" v-model="content.approved">\
                <span v-else>{{content.approved}}</span>\
            </td>\
            <td>\
                <input type="number" step="1" min="0" class="form-control" id="\'inputEnrolment\' + key" v-model="content.enrolment">\
            </td>\
            <td></td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            year: currYear,
            month: currMonth,
            content: formData.EnrolVacancy[0] ? formData.EnrolVacancy[0] : {
                wholeDay: '0',
                halfDay: '0',
                classroom: '0',
                approved: '0',
                enrolment: '0'
            }
        }
    },
    computed: {
        yearDisplay: function(){
            return (parseInt(this.year) - 1) + "/" + this.year;
        },
        correspondMonth: function(){
            date = new Date(this.year+'-'+this.month);
            date.setDate(0);
            return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
        }
    },
    methods: {
        assign: function () {
            submitForm.EnrolVacancy[0] = this.content;
        },
        confirmData: function(){
            self = this;
            this.confirmDataNow(this, 'enrolVacancy', {
                special: 'enroll',
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    data[1] = (data[1] != "") ? data[1] : "Error";
                    data[2] = (data[2] != "") ? data[2] : "Error";
                    data[3] = (data[3] != "") ? data[3] : "Error";
                    data[4] = (data[4] != "") ? data[4] : "Error";
                    return data;
                },
                addRow: function(data, x){
                    self.content.wholeDay = data[0];
                    self.content.halfDay = data[1];
                    self.content.classroom = data[2];
                    self.content.approved = data[3];
                    self.content.enrolment = data[4];
                }
            });
        },
        add: function(){},
        addRow: function(){},
        addRows: function(){}
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);

        eventHub.$on('confirm-info-enrolVacancyModal', this.confirmData);
    }
});

Vue.component('enrolVacancyModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}}: <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            year: currYear,
            month: currMonth,
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.NoOfClasses + '(' + jsLang.WholeDayAndSpecial + ')',
                jsLang.NoOfClasses + '(' + jsLang.HalfDayAndNormal + ')',
                jsLang.NoOfClassroom
            ],
            popup:[
                null,
                null,
                null,
                null,
                null
            ],
            reminder:[
                null,
                null,
                null,
                null
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/enrolVacancy",
            fileFieldId: "enrolVacancyFile"
        }
    },
    mounted: function(){
        this.columns.push(this.ApprovedEnrolmentOfThisYear);
        this.columns.push(jsLang.EnrolmentOfCorrespondMonth2);
    },
    computed: {
        ApprovedEnrolmentOfThisYear: function(){
            return jsLang.ApprovedEnrolmentOfThisYear.replace('{{yearDisplay}}', this.yearDisplay);
        },
        EnrolmentOfCorrespondMonth: function(){
            return jsLang.EnrolmentOfCorrespondMonth.replace('{{correspondMonth}}', this.correspondMonth);
        },
        yearDisplay: function(){
            return (parseInt(this.year) - 1) + "/" + this.year;
        },
        correspondMonth: function(){
            date = new Date(this.year+'-'+this.month);
            date.setDate(0);
            return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
        }
    },
});