Vue.component('appointment-termination', {
    template: '\
    <div>\
    <table class="table report-style tbl_extraActivities" id="tbl_other">\
    <colgroup>\
	    <col style="width: 5%">\
        <col style="width: 23%">\
        <col style="width: 23%">\
        <col style="width: 15%">\
        <col style="width: 15%">\
        <col style="width: 17%">\
        <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th class="subTitle" colspan="7">(A) '+jsLang.Appointment+'</th>\
    </tr>\
    <tr>\
        <th colspan="6">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#appointmentRetrieveModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromAccountManagement+'</a></li>\
                    <li><a href="#" data-toggle="modal" data-target="#appointmentModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
        <th>#</th>\
        <th>' + jsLang.Name + '</th>\
        <th>' + jsLang.Position + '</th>\
        <th>' + jsLang.PermanentContract + '</th>\
        <th>' + jsLang.EffectiveDate + '</th>\
        <th>' + jsLang.Remark + '</th>\
        <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents.appointment" >\
            <td>{{ key + 1 }}</td>\
            <td>\
                <input type="text" class="form-control" id="\'inputTeachername\'+ key" v-model="content.teachername" v-bind:type="\'appointment_termination_a\'">\
            </td>\
            <td>\
    	  		<select v-model="content.positionId" :id="\'_positionselect_\' + key" class="selectpicker" data-live-search="true">\
    	  		<option disabled>'+jsLang.Select+'</option>\
                     <option v-for="(position,id) in positions" :value="id">{{ position }}</option>\
                </select>\
            </td>\
            <td>\
    	  		<select v-model="content.typeId" :id="\'_typeselect_\' + key" class="selectpicker" >\
    	  		<option disabled>'+jsLang.Select+'</option>\
                    <option v-for="(type,id) in contracttypes" :value="id">{{ type }}</option>\
                </select>\
            </td>\
	    	<td>\
    			<div is="multi-date-input" v-bind:id="key" v-bind:type="\'appointment_termination_a\'" v-bind:TypeKey="\'appointment\'">\
	        	</div>\
	        </td>\
	        <td>\
                <input type="text" class="form-control" id="inputRemark+ key" v-model="content.remark">\
            </td>\
            <td>\
            	<div is="del-btn" v-bind:remove="removea" v-bind:target="key" v-bind:location="contents.appointment">\
            	</div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="adda"  v-bind:model="\'appointment\'"></tr>\
    </tbody>\
    </table>\
    <table class="table report-style tbl_extraActivities" id="tbl_other">\
    <colgroup>\
    <col style="width: 5%">\
    <col style="width: 23%">\
    <col style="width: 23%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 17%">\
    <col style="width: 4%">\
    </colgroup>\
     <thead>\
      <tr>\
        <th class="subTitle" colspan="7">(B) '+jsLang.ServiceTermination+'</th>\
      </tr>\
      <tr>\
        <th colspan="7">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#terminationRetrieveModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromAccountManagement+'</a></li>\
                    <li><a href="#" data-toggle="modal" data-target="#terminationModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
       </tr>\
        <tr >\
            <th>#</th>\
            <th>' + jsLang.Name + '</th>\
            <th>' + jsLang.Position + '</th>\
            <th>' + jsLang.PermanentContract + '</th>\
            <th>' + jsLang.EffectiveDate + '</th>\
            <th>' + jsLang.Remark + '</th>\
            <th>&nbsp;</th>\
        </tr>\
      </thead>\
      <tbody>\
      <tr v-for="(content, key) in contents.termination" >\
          <td>{{ key + 1 }}</td>\
          <td>\
              <input type="text" class="form-control" id="\'inputTeachername\'+ key" v-model="content.teachername" v-bind:type="\'appointment_termination_t\'">\
          </td>\
          <td>\
  	  		<select v-model="content.positionId" :id="\'_positionselect_\' + key" class="selectpicker" data-live-search="true">\
  	  		<option disabled>'+jsLang.Select+'</option>\
                   <option v-for="(position,id) in positions" :value="id">{{ position }}</option>\
              </select>\
          </td>\
          <td>\
  	  		<select v-model="content.typeId" :id="\'_typeselect_\' + key" class="selectpicker" >\
  	  		    <option disabled>'+jsLang.Select+'</option>\
                <option v-for="(type,id) in contracttypes" :value="id">{{ type }}</option>\
              </select>\
          </td>\
	      <td>\
  			<div is="multi-date-input" v-bind:id="key" v-bind:type="\'appointment_termination_t\'" v-bind:TypeKey="\'termination\'">\
	         </div>\
	      </td>\
          <td>\
              <input type="text" class="form-control" id="\'inputRemark\'+ key" v-model="content.remark" >\
          </td>\
          <td>\
          	<div is="del-btn" v-bind:remove="removet" v-bind:target="key" v-bind:location="contents.termination">\
          	</div>\
          </td>\
      </tr>\
        <tr is="add-row" v-bind:add="addt" v-bind:model="\'termination\'"></tr>\
        </tbody>\
    </table>\
    </div>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
       //     contents: formData.AppointmentTermination.slice(),
            contents: {
                appointment: formData.AppointmentTermination.appointment.slice(),
                termination: formData.AppointmentTermination.termination.slice()
            },
   //         particulars: this.$root.particulars,
            positions: this.$root.positions,
            contracttypes: this.$root.contracttypes,
            terminationreasons: this.$root.terminationreasons,
            // row: {
  //              particularId: '',
  //               teachername: '',
  //               positionId: '',
  //               typeId: '',
  //               date: ''
  //           },
            row: {
                appointment: {
                    teachername: '',
                    positionId: '',
                    particularId: '1',
                    typeId: '',
                    date: '',
                    remark: ''
                },
                termination: {
                    teachername: '',
                    positionId: '',
                    particularId:'2',
                    typeId: '',
                    date: '',
                    remark: ''
                }
            }
        }
    },
    methods: {
        // add: function (e) {
        //     this.addNewRows(e.target.value, this.row, this.contents);
        //     this.$nextTick(function () {
        //         $('.selectpicker').selectpicker();
        //     });
        //
        // },
        adda: function (e) {
            // this.addNewRows(e.target.value, this.row.appointment, this.contents.appointment);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        addt: function (e) {
            // this.addNewRows(e.target.value, this.row.termination, this.contents.termination);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        removea: function (key) {
            this.removeRow(key, this.contents.appointment)
        },
        removet: function (key) {
            this.removeRow(key, this.contents.termination)
        },
        assign: function () {
            submitForm.AppointmentTermination = this.contents;
        },
        confirmData: function(){
            var self = this;
            this.confirmDataNow(this, 'appointment', {
                contents: self.contents.appointment,
                before: function(data){
                    data[1] = jsLang.PositionName[data[1]];
                    if(data[1] == null)
                        data[1] = "Error";
                    data[2] = self.$root.contracttypes[data[2]];
                    if(data[2] == null)
                        data[2] = "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        teachername: data[0],
                        positionId: data[1],
                        particularId: '1',
                        typeId: data[2],
                        date: data[3],
                        remark: data[4]
                    };
                }
            });
        },
        confirmData2: function(){
            var self = this;
            this.confirmDataNow(this, 'termination', {
                contents: self.contents.termination,
                before: function(data){
                    data[1] = jsLang.PositionName[data[1]];
                    if(data[1] == null)
                        data[1] = "Error";
                    data[2] = self.$root.contracttypes[data[2]];
                    if(data[2] == null)
                        data[2] = "Error";
                    data[3] = self.$root.terminationreasons[data[3]];
                    if(data[3] == null)
                        data[3] == "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        teachername: data[0],
                        positionId: data[1],
                        particularId: '2',
                        typeId: data[2],
                        date: data[3],
                        remark: data[4]
                    };
                }
            });

        },
        confirmData3: function(){
            var dbData;
            var self = this;
            var month = document.getElementById("reportMonth").value;
            var year = document.getElementById("reportYear").value;
            $.ajax({
                url: "/home/cees_kis/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStaffAppointmentTerminationData',
                    year: year,
                    month: month,
                    type: 'Appointment'
                },
                success: function (data) {
                    console.log(data);
                    dbData = data;
                }
            })
                .done(function() {
                    teachers = self.$root.teachers;
                    _.forEach(dbData, function (DB, key) {
                        appointmentTeacherid = DB.userId;
                        var teacherName='';
                        _.forEach(teachers, function (teacherList, key) {
                            _.forEach(teacherList, function (teacherid, key) {
                                if (appointmentTeacherid == teacherid.id) {
                                    teacherName = teacherid.name;
                                }
                            })
                        });

                        this.importdata = {
                            teachername: teacherName,
                            positionId: DB.positionId,
                            particularId: '1',
                            typeId: DB.typeId,
                            date: DB.date,
                        };

                        self.contents.appointment.push(this.importdata);
                        self.$nextTick(function () {
                            $('.selectpicker').selectpicker();
                        });

                    });

                });
        },
        confirmData4: function(){
            var dbData;
            var self = this;
            var month = document.getElementById("reportMonth").value;
            var year = document.getElementById("reportYear").value;
            $.ajax({
                url: "/home/cees_kis/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStaffAppointmentTerminationData',
                    year: year,
                    month: month,
                    type: 'Termination'
                },
                success: function (data) {
                    dbData = data;
                }
            })
                .done(function() {
                    teachers = self.$root.teachers;
                    _.forEach(dbData, function (DB, key) {
                        appointmentTeacherid = DB.userId;
                        var teacherName='';
                        _.forEach(teachers, function (teacherList, key) {
                            _.forEach(teacherList, function (teacherid, key) {
                                if (appointmentTeacherid == teacherid.id) {
                                    teacherName = teacherid.name;
                                }
                            })
                        });

                        this.importdata = {
                            teachername: teacherName,
                            positionId: DB.positionId,
                            particularId: '2',
                            typeId: DB.typeId,
                            date: DB.date,
                        };

                        // self.addt(this.importdata);
                        self.contents.termination.push(this.importdata);
                        self.$nextTick(function () {
                            $('.selectpicker').selectpicker();
                        });

                    });

                });
        }
    },

    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-appointmentModal', this.confirmData);
        eventHub.$on('confirm-info-terminationModal', this.confirmData2);
        eventHub.$on('confirm-info-appointmentRetrieveModal', this.confirmData3);
        eventHub.$on('confirm-info-terminationRetrieveModal', this.confirmData4);
    }
});
Vue.component('appointmentModal', {
   template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}}: <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder" \
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
       return {
           UploadFile : jsLang.UploadFile,
           TemplateFile : jsLang.TemplateFile,
           columns:[
               jsLang.Name,
               jsLang.Position,
               jsLang.ContractType ,
               jsLang.EffectiveDate,
               jsLang.Remark
           ],
           popup:[
               null,
               { title : jsLang.Position, content: this.$root.positions},
               { title : jsLang.ContractType, content: this.$root.contracttypes},
               null,
               null
           ],
           reminder:[
               null,
               null,
               null,
               "YYYY-MM-DD or DD/MM/YYYY",
               null
           ],
           filetemp: "/home/cees_kis/monthlyreport/template/appointment",
           fileFieldId: "appointmentFile"
       }
    }
});

Vue.component('terminationModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}}: <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Name,
                jsLang.Position,
                jsLang.ContractType,
                jsLang.EffectiveDate,
                jsLang.Remark
            ],
            popup:[
                null,
                { title : jsLang.Position, content: this.$root.positions},
                { title : jsLang.ContractType, content: this.$root.contracttypes},
                null,
                null
            ],
            reminder:[
                null,
                null,
                null,
                "YYYY-MM-DD or DD/MM/YYYY",
                null
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/termination",
            fileFieldId: "terminationFile"
        }
    }
});
Vue.component('appointmentRetrieveModal', {
    template: '\
     <div class="modal-body">\
        <div class="browse-folder-container">\
            <ul><li>'+jsLang.Month + '/ '+jsLang.Year+': {{month}}/{{year}}</li></ul>\
            <table class="table report-style" >\
                <colgroup>\
                    <col style="width: 32%;">\
                    <col style="width: 68%;">\
                </colgroup>\
                <tbody>\
                    <tr>\
                     <td>'+jsLang.AppointmentNo+'</td>\
                     <td>{{Teachernumber}}</td>\
                   </tr>\
                   <tr>\
                    <td>'+jsLang.AppointmentName+'</td>\
                    <td>{{TeacherName}}</td>\
                   </tr>\
               </tbody>\
            </table>\
            <p class="question text-center">'+jsLang.EnsureImport+'</p>\
         </div>\
      </div>\
        ',
    data: function () {

        return {
            appointmentModalcontents:
                [{
                    userId: '',
                    positionId: '',
                    typeId: '',
                    date: ''
                }]
        }
    },
    computed: {
        Teachernumber: function () {
            return _.size(this.appointmentModalcontents);
        },
        TeacherName: function(){
            teachers = this.$root.teachers;
            var teachername = [];
            _.forEach(this.appointmentModalcontents, function (value, key) {
                appointmentTeacherid = value.userId;
                _.forEach(teachers, function(teacherSet, index){
                    _.forEach(teacherSet, function (teacherid, key) {
                        if(appointmentTeacherid == teacherid.id){
                            teachername.push(teacherid.name);
                        }
                    });
                })

            });
            if(teachername == ''){
                return '---';
            }else {
                return teachername.join(",");
            }
        },
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    mounted: function () {
        var dbData;
        var self = this;

        $.ajax({
            url: "/home/cees_kis/api",
            type:"GET",
            datatype: 'json',
            data: {
                Method: 'GetStaffAppointmentTerminationData',
                year: this.year,
                month: this.month,
                type: 'Appointment'
            },
            success: function (data) {
                // console.log(data);
                dbData = data;
            }
        })
            .done(function() {
                self.appointmentModalcontents = dbData;
            });
    }

});
Vue.component('TerminationRetrieveModal', {
    template: '\
     <div class="modal-body">\
        <div class="browse-folder-container">\
            <ul><li>'+jsLang.Month + '/ '+jsLang.Year+': {{month}}/{{year}}</li></ul>\
            <table class="table report-style" >\
                <colgroup>\
                    <col style="width: 32%;">\
                    <col style="width: 68%;">\
                </colgroup>\
                <tbody>\
                    <tr>\
                     <td>'+jsLang.TerminationNo+'</td>\
                     <td>{{Teachernumber}}</td>\
                   </tr>\
                   <tr>\
                    <td>'+jsLang.TerminationName+'</td>\
                    <td>{{TeacherName}}</td>\
                   </tr>\
               </tbody>\
            </table>\
            <p class="question text-center">'+jsLang.EnsureImport+'</p>\
         </div>\
      </div>\
        ',
    data: function () {

        return {
            appointmentModalcontents:
                [{
                    userId: '',
                    positionId: '',
                    typeId: '',
                    date: ''
                }]
        }
    },
    computed: {
        Teachernumber: function () {
            return _.size(this.appointmentModalcontents);
        },
        TeacherName: function(){
            teachers = this.$root.teachers;
            var teachername = [];
            _.forEach(this.appointmentModalcontents, function (value, key) {
                appointmentTeacherid = value.userId;
                _.forEach(teachers, function(teacherSet, index){
                    _.forEach(teacherSet, function (teacherid, key) {
                        if(appointmentTeacherid == teacherid.id){
                            teachername.push(teacherid.name);
                        }
                    });
                })

            });
            if(teachername == ''){
                return '---';
            }else {
                return teachername.join(",");
            }
        },
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    mounted: function () {
        var dbData;
        var self = this;

        $.ajax({
            url: "/home/cees_kis/api",
            type:"GET",
            datatype: 'json',
            data: {
                Method: 'GetStaffAppointmentTerminationData',
                year: this.year,
                month: this.month,
                type: 'Termination'
            },
            success: function (data) {
                dbData = data;
            }
        })
            .done(function() {
                self.appointmentModalcontents = dbData;
            });
    }

});