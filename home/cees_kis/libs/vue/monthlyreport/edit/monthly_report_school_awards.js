Vue.component('school-awards', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 19%">\
    <col style="width: 15%">\
    <col style="width: 2%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="7">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#schoolAwardModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>#</th>\
    <th>'+jsLang.Date+'</th>\
    <th>'+jsLang.AwardedStaff+'</th>\
    <th>'+jsLang.OrganizedBy+'</th>\
    <th>'+jsLang.EventName+'</th>\
    <th>'+jsLang.Award+'</th>\
    <th>'+jsLang.Remark+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
            <div is="multiple-date-select" v-bind:id="key" v-bind:sourceKey="\'date\'"> \
            </div>\
            <td>\
            <input type="text" class="form-control" id="\'inputAwardAwardedStaff\' + key" v-model="content.awardedStaff">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputOrganizedBy\' + key" v-model="content.organizedBy">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputAwardEventName\' + key" v-model="content.eventName">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputAwardDes\' + key" v-model="content.text">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputAwardRemark\' + key" v-model="content.remark">\
            </td>\
            <td>\
            <div is="del-btn" v-bind:remove="remove" v-bind:target="key">\
            </div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="add"></tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.SchoolAwards.slice(),
            row: {
                date: '',
                awardedStaff: '',
                organizedBy: '',
                eventName: '',
                text: '',
                remark: ''
            }
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            submitForm.SchoolAwards = this.contents;
        },
        confirmData: function(){
            this.confirmDataNow(this, 'schoolAward', {
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    data[1] = (data[1] != "") ? data[1] : "Error";
                    data[2] = (data[2] != "") ? data[2] : "Error";
                    data[3] = (data[3] != "") ? data[3] : "Error";
                    data[4] = (data[4] != "") ? data[4] : "Error";
                    data[5] = (data[5] != "") ? data[5] : "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        date: data[0],
                        awardedStaff: data[1],
                        organizedBy: data[2],
                        eventName: data[3],
                        text: data[4],
                        remark: data[5]
                    };
                }
            });
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);

        eventHub.$on('confirm-info-schoolAwardModal', this.confirmData);
    }
});

Vue.component('schoolAwardModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Date,
                jsLang.AwardedStaff,
                jsLang.OrganizedBy,
                jsLang.EventName,
                jsLang.Award,
                jsLang.Remark
            ],
            popup:[
                null,
                null,
                null,
                null,
                null,
                null
            ],
            reminder:[
                "YYYY-MM-DD or DD/MM/YYYY",
                null,
                null,
                null,
                null,
                null
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/schoolAward",
            fileFieldId: "schoolAwardFile"
        }
    },
    mounted: function(){
    }
});

