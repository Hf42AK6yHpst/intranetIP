Vue.component('school-visit', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 15%">\
    <col style="width: 35%">\
    <col style="width: 35%">\
    <col style="width: 10%">\
    <col style="width: 2%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="6">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#visitInspectionModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
        <th>#</th>\
        <th>'+jsLang.Date+'</th>\
        <th>'+jsLang.OrganizedBy+'</th>\
        <th>'+jsLang.VIContent+'</th>\
        <th>'+jsLang.Remark+'</th>\
        <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
                <div is="multiple-date-select" v-bind:id="key" v-bind:type="\'school_visit\'" v-bind:sourceKey="\'date\'">\
                </div>\
            </td>\
            <td>\
                <input type="text" class="form-control " id="\'inputOrganizedBy\'+ key" v-model="content.organizedBy" \>\
            </td>\
            <td>\
                <input type="text" class="form-control " id="\'inputPurpose\'+ key" v-model="content.purpose" \>\
            </td>\
            <td>\
                <input type="text" class="form-control " id="\'inputRemark\'+ key" v-model="content.remark" \>\
            </td>\
            <td>\
                <div is="del-btn" v-bind:remove="remove" v-bind:target="key" v-bind:location="contents">\
                </div>\
            </td>\
            </tr>\
            <tr is="add-row" v-bind:add="add"></tr>\
        </tbody>\
        </table>\
        ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.SchoolVisit.slice(),
            row: {
                date: '',
                organizedBy: '',
                purpose: '',
                remark: ''
            }
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            submitForm.SchoolVisit = this.contents;
        },
        confirmData: function(){
            var self = this;
            this.confirmDataNow(this, 'visitInspection', {
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    data[1] = (data[1] != "") ? data[1] : "Error";
                    data[2] = (data[2] != "") ? data[2] : "Error";
                    data[3] = (data[3] != "") ? data[3] : "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        date: data[0],
                        organizedBy: data[1],
                        purpose: data[2],
                        remark: data[3]
                    };
                }
            });
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-visitInspectionModal', this.confirmData);
    }
});

Vue.component('visitInspectionModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Date,
                jsLang.OrganizedBy,
                jsLang.VIContent,
                jsLang.Remark
            ],
            popup:[
                null,
                null,
                null,
                null
            ],
            reminder:[
                "YYYY-MM-DD or DD/MM/YYYY",
                null,
                null,
                null
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/visitInspection",
            fileFieldId: "visitInspectionFile"
        }
    },
    mounted: function(){
    }
});