Vue.component('student-eca', {
    template: '\
    <div>\
        <span style="margin-left: 40px;"><i>'+jsLang.PleaseReferToECA+'</i></span>\
    </div>\
    ',
    data: function () {
        return {
            ReferenceHint: jsLang.PleaseReferToECA,
            contents: {
                internal: formData.StudentEca.internal.slice(),
                external: formData.StudentEca.external.slice()
            },
            ecacategories: this.$root.categories
        }
    },
    filters: {
        getCategoryName: function (categoryID, ecacategories) {
            var categoryname = [];
            _.forEach(categoryID, function (categoryIDs, key) {
                categoryname.push(ecacategories[categoryIDs]);
            });
            return categoryname.join(",");
        }
    }
});