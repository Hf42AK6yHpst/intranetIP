Vue.component('enrol-quota', {
    // language=HTML
    template: '<div id="enrolQuotaDiv" style="text-align:center;">\
    <table class="table report-style" id="tbl_enrolQuota" width="100%">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 47%">\
    <col style="width: 47%">\
    <col style="width: 3%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="4">\
                <ul>\
                    <li v-if="content.attachment!=\'\' && typeof content.attachment !== \'undefined\'">'+jsLang.enrolQuotaAttachment+': <a :href="content.attachment" target="_blank"><i class="form-icon fa fa-file"></i>{{getAttachmentName()}}</a></li>\
                </ul>\
        </th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="2" style="text-align:center;vertical-align:middle;">'+jsLang.ClassroomTotal+' <br/>(' + jsLang.AbleToCreateClass + ') <br/><span style="font-size:11px;color: #888888;">* '+jsLang.FillEverySept+'</span></th>\
        <th></th>\
    </tr>\
    <tr>\
        <th></th>\
        <th style="text-align:center">'+jsLang.HalfDaySystem+'</th>\
        <th style="text-align:center">'+jsLang.WholeDaySystem+'</th>\
        <th></th>\
    </tr>\
    </thead>\
    <tbody>\
    <tr>\
        <td></td>\
        <td style="text-align:center;">\
            <span>{{content.classroomHalf}}</span>\
        </td>\
        <td style="text-align:center;">\
            <span>{{content.classroomWhole}}</span>\
        </td>\
        <td></td>\
    </tr>\
    </tbody>\
    </table>\
    <table class="table report-style" id="tbl_enrolQuota">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 6.25">\
    <col style="width: 3%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th></th>\
        <th colspan="7" style="text-align:center;">'+jsLang.ApprovedQuota+' (a) <br/><span style="font-size:11px;color: #888888;">* '+jsLang.FillEverySept+'</span></th>\
        <th colspan="7" style="text-align:center;">'+jsLang.ApprovedEnrolment+' (b)</th>\
        <th rowspan="2" style="text-align:center;vertical-align: middle;">'+jsLang.QuotaVacancy+'<br/>(a) - (b) = (c)</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="2" style="text-align:center;">'+jsLang.TimeSlot.AM+'</th>\
        <th style="text-align:center;">'+jsLang.Summarize+'</th>\
        <th colspan="2" style="text-align:center;">'+jsLang.TimeSlot.PM+'</th>\
        <th style="text-align:center;">'+jsLang.Summarize+'</th>\
        <th style="text-align:center;">'+jsLang.Summary+'</th>\
        <th colspan="2" style="text-align:center;">'+jsLang.TimeSlot.AM+'</th>\
        <th style="text-align:center;">'+jsLang.Summarize+'</th>\
        <th colspan="2" style="text-align:center;">'+jsLang.TimeSlot.PM+'</th>\
        <th style="text-align:center;">'+jsLang.Summarize+'</th>\
        <th style="text-align:center;">'+jsLang.Summary+'</th>\
        <th></th>\
    </tr>\
    </thead>\
    <tbody>\
    <tr>\
        <td></td>\
        <td style="text-align:center;">'+jsLang.MixedClass+'</td>\
        <td style="text-align:center;">\
            <span>{{content.quotaAMMixed}}</span>\
        </td>\
        <td rowspan="4" style="text-align:center;vertical-align: middle">{{quotaAM()}}</td>\
        <td style="text-align:center;">--</td>\
        <td></td>\
        <td rowspan="4" style="text-align:center;vertical-align: middle">{{quotaPM()}}</td>\
        <td rowspan="4" style="text-align:center;vertical-align: middle">{{quotaSummary()}}</td>\
        <td style="text-align:center;">'+jsLang.MixedClass+jsLang.HalfDay+'<br/>(i)</td>\
        <td style="text-align:center;">\
            {{content.enrolMixedHalf}}\
        </td>\
        <td rowspan="4" style="text-align:center;vertical-align: middle">{{enrolAM()}}</td>\
        <td rowspan="2" style="text-align:center;vertical-align: middle">'+jsLang.HalfDayClass+'<br/>(v)</td>\
        <td rowspan="2" style="text-align:center;vertical-align: middle">\
            {{content.enrolPMHalf}}\
        </td>\
        <td rowspan="4" style="text-align:center;vertical-align: center">{{enrolPM()}}</td>\
        <td rowspan="4" style="text-align:center;vertical-align: center">{{enrolSummary()}}</td>\
        <td rowspan="4" style="text-align:center;vertical-align: center">{{quotaVacancy()}}</td>\
        <td></td>\
    </tr>\
    <tr>\
        <td></td>\
        <td style="text-align:center;">'+jsLang.HalfDayClass+'</td>\
        <td style="text-align:center;">\
            <span>{{content.quotaAMHalf}}</span>\
        </td style="text-align:center;">\
        <td style="text-align:center;">'+jsLang.HalfDayClass+'</td>\
        <td style="text-align:center;">\
            <span>{{content.quotaPMHalf}}</span>\
        </td>\
        <td style="text-align:center;">'+jsLang.MixedClass+jsLang.WholeDay+'<br/>(ii)</td>\
        <td style="text-align:center;">\
            {{content.enrolMixedWhole}}\
        </td>\
        <td></td>\
    </tr>\
    <tr>\
        <td></td>\
        <td style="text-align:center;">'+jsLang.WholeDayClass+'</td>\
        <td style="text-align:center;">\
            <span>{{content.quotaAMWhole}}</span>\
        </td>\
        <td style="text-align:center;">'+jsLang.WholeDayClass+'</td>\
        <td style="text-align:center;">\
            <span>{{content.quotaPMWhole}}</span>\
        </td>\
        <td style="text-align:center;">'+jsLang.HalfDayClass+'<br/>(iii)</td>\
        <td style="text-align:center;">\
            {{content.enrolAMHalf}}\
        </td>\
        <td rowspan="2" style="text-align:center;vertical-align: middle">'+jsLang.WholeDayClass+'</td>\
        <td rowspan="2" style="text-align:center;vertical-align: middle">\
            {{enrolPMWhole()}}\
        </td>\
        <td></td>\
    </tr>\
    <tr>\
        <td></td>\
        <td></td>\
        <td></td>\
        <td></td>\
        <td></td>\
        <td style="text-align:center;">'+jsLang.WholeDayClass+'<br/>(iv)</td>\
        <td style="text-align:center;">\
            {{content.enrolAMWhole}}\
        </td>\
        <td></td>\
    </tr>\
    </tbody>\
    </table>\
    </div>',
    data: function () {
        return {
            year: currYear,
            month: currMonth,
            content: formData.EnrolQuota[0] ? formData.EnrolQuota[0] : {
                classroomHalf: 0,
                classroomWhole: 0,
                quotaAMMixed: 0,
                quotaAMHalf: 0,
                quotaAMWhole: 0,
                quotaPMHalf: 0,
                quotaPMWhole: 0,
                enrolMixedHalf: 0,
                enrolMixedWhole: 0,
                enrolAMHalf: 0,
                enrolAMWhole: 0,
                enrolPMHalf: 0,
                attachment: ''
            }
        }
    },
    computed: {
    },
    methods: {
        getAttachmentName: function(){
            if(this.content.attachment != ''){
                return this.content.attachment.split('/').reverse()[0];
            } else {
                return '';
            }
        },
        quotaAM: function(){
            return parseInt(this.content.quotaAMMixed ? this.content.quotaAMMixed : 0) +
                parseInt(this.content.quotaAMHalf ? this.content.quotaAMHalf : 0) +
                parseInt(this.content.quotaAMWhole ? this.content.quotaAMWhole : 0);
        },
        quotaPM: function(){
            return parseInt(this.content.quotaPMHalf ? this.content.quotaPMHalf : 0) +
                parseInt(this.content.quotaPMWhole ? this.content.quotaPMWhole : 0);
        },
        quotaSummary: function(){
            return parseInt(this.quotaAM()) +
                parseInt(this.quotaPM()) -
                parseInt(this.content.quotaPMWhole ? this.content.quotaPMWhole : 0);
        },
        enrolAM: function(){
            return parseInt(this.content.enrolMixedHalf ? this.content.enrolMixedHalf : 0) +
                parseInt(this.content.enrolMixedWhole ? this.content.enrolMixedWhole : 0) +
                parseInt(this.content.enrolAMHalf ? this.content.enrolAMHalf : 0) +
                parseInt(this.content.enrolAMWhole ? this.content.enrolAMWhole : 0);
        },
        enrolPMWhole: function(){
            return parseInt(this.content.enrolMixedWhole ? this.content.enrolMixedWhole : 0) +
                parseInt(this.content.enrolAMWhole ? this.content.enrolAMWhole : 0);
        },
        enrolPM: function(){
            return parseInt(this.content.enrolPMHalf ? this.content.enrolPMHalf : 0) +
                parseInt(this.enrolPMWhole());
        },
        enrolSummary: function(){
            return parseInt(this.enrolAM()) +
                parseInt(this.enrolPM()) -
                parseInt(this.enrolPMWhole());
        },
        quotaVacancy: function(){
            return parseInt(this.quotaSummary()) -
                parseInt(this.enrolSummary());
        },
    },
    mounted: function(){
        $('#tbl_enrolQuota tr').find('td, th').css('border','1px solid lightgrey');
        $('#tbl_enrolQuota tr').find('td:first, td:last, th:first, th:last').css('border','none');

    }
});


Vue.component('enrol-quota-old', {
    // language=HTML
    template: '<div id="enrolQuotaDiv">\
    <table class="table report-style" id="tbl_enrolQuota" width="100%">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 3%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="12">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#enrolQuotaModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromAccountManagement+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="2" rowspan="2" style="text-align:center;vertical-align:middle;">'+jsLang.ClassroomTotal+' <br/>(' + jsLang.AbleToCreateClass + ')</th>\
        <th colspan="8" style="text-align:center">'+jsLang.ApprovedQuota+' (a)</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="4" style="text-align:center">'+jsLang.TimeSlot.AM+'</th>\
        <th colspan="3" style="text-align:center">'+jsLang.TimeSlot.PM+'</th>\
        <th rowspan="2" style="text-align:center;vertical-align:middle;">'+jsLang.Summary+'</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th>&nbsp;</th>\
        <th>'+jsLang.HalfDaySystem+'</th>\
        <th>'+jsLang.WholeDaySystem+'</th>\
        <th>'+jsLang.MixedClass+'</th>\
        <th>'+jsLang.HalfDayClass+'</th>\
        <th>'+jsLang.WholeDayClass+'</th>\
        <th>'+jsLang.Summarize+'</th>\
        <th>'+jsLang.HalfDayClass+'</th>\
        <th>'+jsLang.WholeDayClass+'</th>\
        <th>'+jsLang.Summarize+'</th>\
        <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
    <tr>\
        <td></td>\
        <td>\
            <span>{{content.classroomHalf}}</span>\
        </td>\
        <td>\
            <span>{{content.classroomWhole}}</span>\
        </td>\
        <td>\
            <span>{{content.quotaAMMixed}}</span>\
        </td>\
        <td>\
            <span>{{content.quotaAMHalf}}</span>\
        </td>\
        <td>\
            <span>{{content.quotaAMWhole}}</span>\
        </td>\
        <td>\
            <span>{{quotaAM()}}</span>\
        </td>\
        <td>\
            <span>{{content.quotaPMHalf}}</span>\
        </td>\
        <td>\
            <span>{{content.quotaPMWhole}}</span>\
        </td>\
        <td>\
            <span>{{quotaPM()}}</span>\
        </td>\
        <td style="text-align: center">\
            <span>{{quotaSummary()}}</span>\
        </td>\
    </tr>\
    </tbody>\
    </table>\
    <table class="table report-style" id="tbl_enrolQuota" width="100%">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 9.4%">\
    <col style="width: 3%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th></th>\
        <th colspan="9" style="text-align:center">'+jsLang.ApprovedEnrolment+' (b)</th>\
        <th rowspan="3" style="text-align:center;vertical-align:middle;">'+jsLang.QuotaVacancy+' <br/>(a) - (b) = (c)</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th></th>\
        <th colspan="2" style="text-align:center">'+jsLang.MixedClass+'</th>\
        <th colspan="3" style="text-align:center">'+jsLang.TimeSlot.AM+'</th>\
        <th colspan="3" style="text-align:center">'+jsLang.TimeSlot.PM+'</th>\
        <th rowspan="2" style="text-align:center;vertical-align:middle;">'+jsLang.Summary+'</th>\
        <th></th>\
    </tr>\
    <tr>\
        <th>&nbsp;</th>\
        <th>'+jsLang.HalfDay+' (i)</th>\
        <th>'+jsLang.WholeDay+' (ii)</th>\
        <th>'+jsLang.HalfDayClass+' (iii)</th>\
        <th>'+jsLang.WholeDayClass+' (iv)</th>\
        <th>'+jsLang.Summarize+'</th>\
        <th>'+jsLang.HalfDayClass+' (v)</th>\
        <th>'+jsLang.WholeDayClass+'</th>\
        <th>'+jsLang.Summarize+'</th>\
        <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
    <tr>\
        <td></td>\
        <td>\
            {{content.enrolMixedHalf}}\
        </td>\
        <td>\
            {{content.enrolMixedWhole}}\
        </td>\
        <td>\
            {{content.enrolAMHalf}}\
        </td>\
        <td>\
            {{content.enrolAMWhole}}\
        </td>\
        <td>{{enrolAM()}}</td>\
        <td>\
            {{content.enrolPMHalf}}\
        </td>\
        <td>{{enrolPMWhole()}}</td>\
        <td>{{enrolPM()}}</td>\
        <td style="text-align: center">{{enrolSummary()}}</td>\
        <td style="text-align: center">{{quotaVacancy()}}</td>\
        <td></td>\
    </tr>\
    </tbody>\
    </table>\
    </div>',
    data: function () {
        return {
            year: currYear,
            month: currMonth,
            content: formData.EnrolQuota[0] ? formData.EnrolQuota[0] : {
                classroomHalf: 0,
                classroomWhole: 0,
                quotaAMMixed: 0,
                quotaAMHalf: 0,
                quotaAMWhole: 0,
                quotaPMHalf: 0,
                quotaPMWhole: 0,
                enrolMixedHalf: 0,
                enrolMixedWhole: 0,
                enrolAMHalf: 0,
                enrolAMWhole: 0,
                enrolPMHalf: 0,
            }
        }
    },
    computed: {
    },
    methods: {
        quotaAM: function(){
            return parseInt(this.content.quotaAMMixed ? this.content.quotaAMMixed : 0) +
                parseInt(this.content.quotaAMHalf ? this.content.quotaAMHalf : 0) +
                parseInt(this.content.quotaAMWhole ? this.content.quotaAMWhole : 0);
        },
        quotaPM: function(){
            return parseInt(this.content.quotaPMHalf ? this.content.quotaPMHalf : 0) +
                parseInt(this.content.quotaPMWhole ? this.content.quotaPMWhole : 0);
        },
        quotaSummary: function(){
            return parseInt(this.quotaAM()) +
                parseInt(this.quotaPM()) -
                parseInt(this.content.quotaPMWhole ? this.content.quotaPMWhole : 0);
        },
        enrolAM: function(){
            return parseInt(this.content.enrolMixedHalf ? this.content.enrolMixedHalf : 0) +
                parseInt(this.content.enrolMixedWhole ? this.content.enrolMixedWhole : 0) +
                parseInt(this.content.enrolAMHalf ? this.content.enrolAMHalf : 0) +
                parseInt(this.content.enrolAMWhole ? this.content.enrolAMWhole : 0);
        },
        enrolPMWhole: function(){
            return parseInt(this.content.enrolMixedWhole ? this.content.enrolMixedWhole : 0) +
                parseInt(this.content.enrolAMWhole ? this.content.enrolAMWhole : 0);
        },
        enrolPM: function(){
            return parseInt(this.content.enrolPMHalf ? this.content.enrolPMHalf : 0) +
                parseInt(this.enrolPMWhole());
        },
        enrolSummary: function(){
            return parseInt(this.enrolAM()) +
                parseInt(this.enrolPM()) -
                parseInt(this.enrolPMWhole());
        },
        quotaVacancy: function(){
            return parseInt(this.quotaSummary()) -
                parseInt(this.enrolSummary());
        },
    }
});