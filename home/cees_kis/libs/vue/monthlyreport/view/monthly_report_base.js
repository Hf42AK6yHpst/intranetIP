Vue.component('section-item', {
    template: '\
    <div>\
        <div class="section-divider"></div>\
        <div class="component">\
          <div class="section-title">\
            <ol type="I" :start="id">\
             <li><span style="font-weight: bold;">{{ title }}</span></li>\
            </ol>\
           </div>\
          <div class="section-body">\
          <div :is="type"></div>\
          </div>\
        </div>\
    </div>\
  ',
    props: ['id', 'title', 'type']
})

new Vue({
    el: '#form-body',
    data: {
        teachers: []
        // sections: [
        //     {
        //         title: jsLang.Appoiontment,
        //         type: 'appointment-termination',
        //     },
        //     {
        //         title: jsLang.Substitute,
        //         type: 'substitute-teacher',
        //     },
        //     {
        //         title: jsLang.SickLeave,
        //         type: 'teacher-sick-leave',
        //     },
        //     {
        //         title: jsLang.StudentEnrol,
        //         type: 'student_enrolment',
        //     },
        //     {
        //         title: jsLang.Schoolvisit,
        //         type: 'school-visit',
        //     },
        //     {
        //         title: jsLang.Schooladministration,
        //         type: 'school-administration',
        //     },
        //     {
        //         title: jsLang.StaffCourse,
        //         type: 'teacher-course',
        //     },
        //     {
        //         title: jsLang.Studenteca,
        //         type: 'student-eca',
        //     },
        //     {
        //         title: jsLang.SchoolAwards,
        //         type: 'school-awards',
        //     },
        //     {
        //         title: jsLang.AdvancedNotice,
        //         type: 'advanced-notice',
        //     },
        //     {
        //         title: jsLang.Others,
        //         type: 'others',
        //     }
        //
        // ]
    },
    computed:{
        sections : function() {
            var reportID = document.getElementById("reportID").value;
            var yearid = document.getElementById("reportYear").value;
            var monthid = document.getElementById("reportMonth").value;

            var showsections = $.ajax({
                type: 'POST',
                url: '/home/cees_kis/monthlyreport/ajaxGetDisplaySection',
                data: {
                    reportId: reportID,
                    year: yearid,
                    month: monthid
                },
                async: false,
                success: function (data) {

                    return data;
                }
            });

            var SectionsList = $.parseJSON(showsections.responseText);
            var SectionsAry = [];
            var i = 0;
            SectionsList.forEach(function (element) {
                var nametest = String(element.title);
                SectionsAry.push({
                    title: jsLang[nametest],
                    type: element.type
                });

            });
            return SectionsAry;
        }
    },
    methods: {
        submit: function () {
            var reportID = document.getElementById("reportID").value;
            var yearid = document.getElementById("reportYear").value;
            var monthid = document.getElementById("reportMonth").value;

            $.ajax({
                type: 'POST',
                url: '/home/cees_kis/monthlyreport/sync',
                data: {
                    reportId: reportID,
                    year: yearid,
                    month: monthid,
                    SubmitStatus: '1'
                },
                success: function (data) {
                    // console.log(data);
                    window.location.href = "/home/cees_kis/monthlyreport/";
                }
            });
        },
        modification: function () {
            var reportID = document.getElementById("reportID").value;
            var yearid = document.getElementById("reportYear").value;
            var monthid = document.getElementById("reportMonth").value;
            $.ajax({
                type: 'POST',
                url: '/home/cees_kis/monthlyreport/update',
                data: {
                    reportId: reportID,
                    year: yearid,
                    month: monthid,
                    DraftStatus: '1'
                },
                success: function (data) {
                    window.location.href = "/home/cees_kis/monthlyreport/";
                }
            });
        },
        exportasDoc: function () {
            var reportID = document.getElementById("reportID").value;
            var yearid = document.getElementById("reportYear").value;
            var monthid = document.getElementById("reportMonth").value;


            $.ajax({
                type: 'POST',
                url: '/home/cees_kis/monthlyreport/exportasDoc',
                data: {
                    reportId: reportID,
                    year: yearid,
                    month: monthid
                },
                success: function (data) {

                    var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>" +
                        "<head><meta charset='utf-8'><title>Export HTML To Doc</title>" +
                        "<style type='text/css'>"+
                        "html, body { margin: 0px; padding: 0px; }"+
                        "table, th, td {border: 0.5px solid black;   border-collapse: collapse;}"+
                        ".section {page-break-inside: avoid;}"+
                        ".component {page-break-inside: avoid;}"+
                        "th {font-size: 10pt}"+
                        "td {font-size: 10pt}"+
                        "</style></head><body>";
                    var postHtml = "</body></html>";


                    document.getElementsByClassName('submission-area')[0].style.display = 'none';
                    // var html = preHtml+ document.getElementById('form-header').innerHTML + document.getElementById('form-body').innerHTML+postHtml;
                    var html = preHtml+ document.getElementsByClassName('content-body')[0].innerHTML + postHtml;

                    document.getElementsByClassName('submission-area')[0].style.display = 'block';


                    var blob = new Blob(['\ufeff', html], {
                        type: 'application/msword'
                    });
                    var url = URL.createObjectURL(blob);

                    // var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);

                    filename = 'MonthlyReport_'+ yearid + monthid +'.doc';

                    // Create download link element
                    var downloadLink = document.createElement("a");

                    document.body.appendChild(downloadLink);

//                     if(navigator.msSaveOrOpenBlob ){
//                         navigator.msSaveOrOpenBlob(blob, filename);
//                     }else{

                        downloadLink.href = url;

                        downloadLink.download = filename;


                        downloadLink.click();
                    // }

                    document.body.removeChild(downloadLink);
                }
            });
        }
    },
    created: function () {
        var TeacherList = JSON.parse(TeacherListJson);
        this.teachers = TeacherList;


        var OLECategoryList = $.parseJSON(OLECategoryJson);
        this.categories = OLECategoryList;

    //    this.sickleaveday = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        this.particulars = {1: jsLang.Appointment, 2: jsLang.ServiceTermination};
        // this.positions = {1: jsLang.GT, 2: jsLang.NGT};
        this.positions = {};
        for(n = 1; n<=161; n++ ){
            this.positions[n] = jsLang.PositionName[n];
        }

        this.ecacategories = {};
        for(m = 1; m<=7; m++ ){
            this.ecacategories[m] = jsLang.OLECategory[m];
        }

        this.contracttypes = {1:jsLang.Permanent, 2:jsLang.Establishment, 3:jsLang.Funding, 4:jsLang.Contract};
        this.terminationreasons = {1:jsLang.Expired, 2:jsLang.Resign, 3:jsLang.Retire, 4:jsLang.Termination};
        this.withCertifications = {0:jsLang.Without,1:jsLang.With};
        this.AdmissionType = {'Admission':jsLang.Admission, 'Withdrawal': jsLang.Withdrawal};
    },
    mounted: function () {

    }
})