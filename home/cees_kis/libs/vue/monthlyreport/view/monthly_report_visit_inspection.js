Vue.component('school-visit', {
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
        <col style="width: 3%">\
        <col style="width: 15%">\
        <col style="width: 35%">\
        <col style="width: 35%">\
        <col style="width: 10%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th style="text-align: left;">#</th>\
        <th>'+jsLang.Date+'</th>\
        <th>'+jsLang.OrganizedBy+'</th>\
        <th>'+jsLang.VIContent+'</th>\
        <th>'+jsLang.Remark+'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
                 {{content.date}}\
            </td>\
            <td style="text-align: left;">\
                {{content.organizedBy}}\
            </td>\
            <td style="text-align: left;">\
                {{content.purpose}}\
            </td>\
            <td style="text-align: left;">\
                {{content.remark}}\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.SchoolVisit.slice()
        }
    }
});