Vue.component('advanced-notice', {
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
        <col style="width: 2%">\
        <col style="width: 16%">\
        <col style="width: 16%">\
        <col style="width: 16%">\
        <col style="width: 16%">\
        <col style="width: 16%">\
        <col style="width: 16%">\
    </colgroup>\
    <thead>\
        <tr>\
        <th style="text-align: left;">#</th>\
        <th>'+jsLang.Date+'</th>\
        <th>'+jsLang.Time+'</th>\
        <th>'+jsLang.Venue+'</th>\
        <th>'+jsLang.EventName+'</th>\
        <th>'+jsLang.OrganizedBy+'</th>\
        <th>'+jsLang.CeremonyGuest+'</th>\
        </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
                {{content.date}}\
            </td>\
            <td style="text-align: left;">\
                {{content.time}}\
            </td>\
            <td style="text-align: left;">\
                {{content.venue}}\
            </td>\
            <td style="text-align: left;">\
                {{content.eventName}}\
            </td>\
            <td style="text-align: left;">\
                {{content.organizedBy}}\
            </td>\
            <td style="text-align: left;">\
                {{content.guest}}\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.AdvancedNotice.slice()
        }
    }
});