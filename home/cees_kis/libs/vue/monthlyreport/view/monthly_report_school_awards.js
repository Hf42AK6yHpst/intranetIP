Vue.component('school-awards', {
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 19%">\
    <col style="width: 15%">\
    </colgroup>\
    <thead>\
    <tr>\
    <th style="text-align: left;">#</th>\
    <th>'+jsLang.Date+'</th>\
    <th>'+jsLang.AwardedStaff+'</th>\
    <th>'+jsLang.OrganizedBy+'</th>\
    <th>'+jsLang.EventName+'</th>\
    <th>'+jsLang.Award+'</th>\
    <th>'+jsLang.Remark+'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
                {{content.date}}\
            </td>\
            <td style="text-align: left;">\
                {{content.awardedStaff}}\
            </td>\
            <td style="text-align: left;">\
                {{content.organizedBy}}\
            </td>\
            <td style="text-align: left;">\
                {{content.eventName}}\
            </td>\
            <td style="text-align: left;">\
                {{content.text}}\
            </td>\
            <td style="text-align: left;">\
                {{content.remark}}\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.SchoolAwards.slice()
        }
    }
});