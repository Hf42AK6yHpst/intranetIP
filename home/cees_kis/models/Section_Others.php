<?php
include_once 'Section.php';

class Section_Others extends Section
{
    var $db;
    var $table = 'CEES_KIS_MONTHLY_REPORT_OTHERS';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('Content');
    var $jsonToDbMapping = array('text' => 'Content');

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }
}