<?php
include_once 'Section.php';

class Section_SchoolAwards extends Section
{
    var $db;
    var $table = 'CEES_KIS_MONTHLY_REPORT_AWARDS_INFO';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('RecordDate', 'AwardedStaff', "OrganizedBy", 'EventName', 'Award', 'Remark');
    var $jsonToDbMapping = array(
        'date' => 'RecordDate',
        'awardedStaff' => 'AwardedStaff',
        'organizedBy' => 'OrganizedBy',
        'eventName' => 'EventName',
        'text' => 'Award',
        'remark' => 'Remark'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }
}