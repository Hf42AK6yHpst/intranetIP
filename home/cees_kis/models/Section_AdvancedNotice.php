<?php
include_once 'Section.php';

class Section_AdvancedNotice extends Section
{
    var $db;
    var $table = 'CEES_KIS_MONTHLY_REPORT_ADVANCED_NOTICE';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('RecordDate', 'RecordTime', 'Venue', 'EventName', 'OrganizedBy', 'Guest');
    var $jsonToDbMapping = array(
        'date' => 'RecordDate',
        'time' => 'RecordTime',
        'venue' => 'Venue',
        'eventName' => 'EventName',
        'organizedBy' => 'OrganizedBy',
        'guest' => 'Guest'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }
}