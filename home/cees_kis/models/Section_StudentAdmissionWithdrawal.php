<?php
include_once 'Section.php';

class Section_StudentAdmissionWithdrawal extends Section
{
    var $db;
    var $table = 'CEES_KIS_MONTHLY_REPORT_STUDENT_ADMISSION_WITHDRAWAL';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('Particular', 'RecordDate', 'StudentName', 'Class', 'Reason', 'Prospect', 'Remark');
    var $jsonToDbMapping = array(
        'particular' => 'Particular',
        'date' => 'RecordDate',
        'studentName' => 'StudentName',
        'class' => 'Class',
        'reason' => 'Reason',
        'prospect' => 'Prospect',
        'remark' => 'Remark'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }
}