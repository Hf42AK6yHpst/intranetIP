<?php

include_once 'Section.php';

class Section_EnrolQuota extends Section
{
    var $db;
    var $table = 'CEES_KIS_MONTHLY_REPORT_ENROL_QUOTA';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array(
        'ClassroomHalfDay',
        'ClassroomWholeDay',
        'Quota_AM_Mixed',
        'Quota_AM_Half',
        'Quota_AM_Whole',
        'Quota_PM_Half',
        'Quota_PM_Whole',
        'Enrol_Mixed_Half',
        'Enrol_Mixed_Whole',
        'Enrol_AM_Half',
        'Enrol_AM_Whole',
        'Enrol_PM_Half',
        'Attachment'
    );
    var $jsonToDbMapping = array(
        'classroomHalf' => 'ClassroomHalfDay',
        'classroomWhole' => 'ClassroomWholeDay',
        'quotaAMMixed' => 'Quota_AM_Mixed',
        'quotaAMHalf' => 'Quota_AM_Half',
        'quotaAMWhole' => 'Quota_AM_Whole',
        'quotaPMHalf' => 'Quota_PM_Half',
        'quotaPMWhole' => 'Quota_PM_Whole',
        'enrolMixedHalf' => 'Enrol_Mixed_Half',
        'enrolMixedWhole' => 'Enrol_Mixed_Whole',
        'enrolAMHalf' => 'Enrol_AM_Half',
        'enrolAMWhole' => 'Enrol_AM_Whole',
        'enrolPMHalf' => 'Enrol_PM_Half',
        'attachment' => 'Attachment'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();
        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}'";
        $this->contents = $this->db->returnResultSet($sql);

        // Load existed data
        $_thisYear = $this->getReportYear();
        $_thisMonth = $this->getReportMonth();
        if($_thisMonth != '9'){
            if($_thisMonth > 9){
                $_targetYear = $_thisYear;
            } else {
                $_targetYear = (int)$_thisYear - 1;
            }
            $sql = "SELECT ReportID FROM CEES_SCHOOL_MONTHLY_REPORT WHERE Year = '$_targetYear' AND Month = '9'";
            $info = $this->db->returnVector($sql);
            $_targetReportID = $info[0];
            $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '$_targetReportID'";
            $data = $this->db->returnResultSet($sql);

            /*
            $this->contents['0']['classroomHalf'] = $data['0']['ClassroomHalfDay'];
            $this->contents['0']['classroomWhole'] = $data['0']['ClassroomWholeDay'];
            $this->contents['0']['quotaAMMixed'] = $data['0']['Quota_AM_Mixed'];
            $this->contents['0']['quotaAMHalf'] = $data['0']['Quota_AM_Half'];
            $this->contents['0']['quotaAMWhole'] = $data['0']['Quota_AM_Whole'];
            $this->contents['0']['quotaPMHalf'] = $data['0']['Quota_PM_Half'];
            $this->contents['0']['quotaPMWhole'] = $data['0']['Quota_PM_Whole'];
            */
            $this->contents['0']['classroomHalf'] = $data['0']['classroomHalf'];
            $this->contents['0']['classroomWhole'] = $data['0']['classroomWhole'];
            $this->contents['0']['quotaAMMixed'] = $data['0']['quotaAMMixed'];
            $this->contents['0']['quotaAMHalf'] = $data['0']['quotaAMHalf'];
            $this->contents['0']['quotaAMWhole'] = $data['0']['quotaAMWhole'];
            $this->contents['0']['quotaPMHalf'] = $data['0']['quotaPMHalf'];
            $this->contents['0']['quotaPMWhole'] = $data['0']['quotaPMWhole'];

            # 2020-10-12 (Philips) - Must have default value to prevent sync undefined error
            $this->contents['0']['enrolMixedHalf'] = ($this->contents['0']['enrolMixedHalf'] ? $this->contents['0']['enrolMixedHalf'] : '0');
            $this->contents['0']['enrolMixedWhole'] = ($this->contents['0']['enrolMixedWhole'] ? $this->contents['0']['enrolMixedWhole'] : '0');
            $this->contents['0']['enrolAMHalf'] = ($this->contents['0']['enrolAMHalf'] ? $this->contents['0']['enrolAMHalf'] : '0');
            $this->contents['0']['enrolAMWhole'] = ($this->contents['0']['enrolAMWhole'] ? $this->contents['0']['enrolAMWhole'] : '0');
            $this->contents['0']['enrolPMHalf'] = ($this->contents['0']['enrolPMHalf'] ? $this->contents['0']['enrolPMHalf'] : '0');
        }
        if($_SESSION['isCeesSync']){
            $attachmentPath = $this->contents['0']['attachment'];
            if($attachmentPath!=''){
                $fileName = substr($attachmentPath, strrpos($attachmentPath, '/')+1);
                $this->contents['0']['attachment'] = $fileName;
                $this->contents['0']['attachmentFile'] = file_get_contents('../../'.$attachmentPath);
            } else {
                $this->contents['0']['attachment'] = '';
                $this->contents['0']['attachmentFile'] = '';
            }
        }
    }
}