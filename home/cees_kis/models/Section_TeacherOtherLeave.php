<?php
include_once 'Section.php';

class Section_TeacherOtherLeave extends Section
{
    var $db;
    var $table = 'CEES_KIS_MONTHLY_REPORT_STAFF_OTHER_LEAVE';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('StaffName', 'Position', 'RecordDate', 'DateNo', 'SupportingCertificate', 'Nature');
    var $jsonToDbMapping = array(
        'teacherName' => 'StaffName',
        'positionId' => 'Position',
        'date' => 'RecordDate',
        'thisMonthDay' => 'DateNo',
        'WithCer' => 'SupportingCertificate',
        'nature' => 'Nature'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();
        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}'";
        $SickLeaveResult = $this->db->returnResultSet($sql);
        $Result = array();
        $i = 0;
        foreach ($SickLeaveResult as $SickLeaveArr) {

            $Result[$i]['teacherName'] = $SickLeaveArr['teacherName'];
            $Result[$i]['positionId'] = $SickLeaveArr['positionId'];
            $Result[$i]['date'] = $SickLeaveArr['date'];
            $Result[$i]['thisMonthDay'] = floatval($SickLeaveArr['thisMonthDay']);
            $Result[$i]['WithCer'] = floatval($SickLeaveArr['WithCer']);
            $Result[$i]['lastMonthDay'] = $this->getBeforeAbsentDay($Result[$i]['teacherName']);
            $Result[$i]['nature'] = $SickLeaveArr['nature'];
            $i++;
        }
        $this->contents = $Result;
    }

    function getBeforeAbsentDay($StaffName)
    {
        $sql = "SELECT AcademicYearID,Month,Year FROM CEES_SCHOOL_MONTHLY_REPORT WHERE ReportID = '$this->reportId'";
        $thisMonthAry = $this->db->returnResultSet($sql);
        $AcademicYearID = $thisMonthAry[0]['AcademicYearID'];
        $thisMonth = $thisMonthAry[0]['Month'];
        $thisYear = $thisMonthAry[0]['Year'];

        // 2020-03-02 (Philips)
        $hasBracket = strpos($StaffName, '(');
        if($hasBracket){
            $StaffName = substr($StaffName, 0, $hasBracket);
            while(substr($StaffName,-1)==' '){
                $StaffName = substr($StaffName,0, -1);
            }
        }

        $StartYear = getStartDateOfAcademicYear($AcademicYearID);
        $EndYear = getEndDateOfAcademicYear($AcademicYearID);


        //$start = strtotime(date('Y-m-01', strtotime($StartYear)));
        $start = strtotime(date('Y-09-01', strtotime($StartYear)));
        $end = $loopMonth = strtotime(date('Y-m-t', strtotime($EndYear)));

        $date = array();
        while ($loopMonth >= $start):
            $_year = date("Y", $loopMonth);
            $_month = date("m", $loopMonth);
//            $loopMonth = strtotime("-1 month", $loopMonth);
            $loopMonth = strtotime($_year.'-'.$_month.'-01 -1 month');
            if (($thisYear >= $_year && $thisMonth > $_month) || $thisYear > $_year) {
                //     continue;

                $sql = "SELECT
                      mrssl.DateNo
                    FROM
                      CEES_KIS_MONTHLY_REPORT_STAFF_OTHER_LEAVE AS mrssl
                    LEFT JOIN CEES_SCHOOL_MONTHLY_REPORT AS mr ON (mr.ReportID = mrssl.ReportID)
                    WHERE mr.Month = '$_month' AND mr.Year = '$_year' AND mrssl.StaffName LIKE '%$StaffName%'
                   ";
                $DateNo = $this->db->returnVector($sql);
                $date[] = $DateNo[0];
            }
        endwhile;

        $sum = 0;
        for ($i = 0; $i < sizeof($date); $i++) {
            $sum += $date[$i];
        }
        $sum = $sum == '0' ? '--' : $sum;
        return $sum;
    }


}