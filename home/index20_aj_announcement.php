<?php
# using: 
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libannounce_ip30.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$la = new libannounce_ip30();
$lu = new libuser2007($UserID);
$ajax_target = ($GroupID == '-1') ? 'public' : (empty($GroupID) ? 'all' : 'group');
//$row = ($sys_custom['index_announce_group_select'] && $GroupID != "") ? array($GroupID) : $lu->returnGroupIDs();
$row = empty($GroupID) ? $lu->returnGroupIDs() : array($GroupID);
$x = $la->announcementListHtml($UserID, $row, $ajax_target);

echo $x;			

$benchmark['after group announcement'] = time();

intranet_closedb();

?>