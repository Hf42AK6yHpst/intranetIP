<?

################ Change Log [Start] #####################
#	Date	: 	2011-06-16 [Yuen]
#				fix the dimension of video for MediaPlay as it will be resized after loading by jquery
#
################ Change Log [End] #####################


$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampustv.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lcampustv = new libcampustv();

if($channel_id==0 && !$movieid)		
{
	$movie_url = $lcampustv->LiveURL;
}
else
{
	if(!$movieid)
	{
		$clipAry = $lcampustv->returnClips($channel_id);
		if(sizeof($clipAry))
		{
			$clipid = $clipAry[0]['ClipID'];
		}
	}
	else
	{
		$clipid = $movieid;	
	}
	
	if($clipid)
	{
		list($mTitle, $mDescription, $movie_url, $mRecommended, $misFile, $mUserID, $mRecordType, $mRecordStatus, $mIsSecure) = $lcampustv->retrieveClipInfo($clipid);
		
		if($mUserID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($mUserID);
			$movieBy = "(" . $lu->UserNameLang() .")";
		}
		
		$MovieTitle = $mTitle . $movieBy;
	}
}

if($movie_url)
{
	$live_width = $lcampustv->live_width;
	$live_height = $lcampustv->live_height;
	$live_auto_start = "true";
	
	if ($tv_playertype=="QuickTime")
	{
		$TVLink = "
					<OBJECT CLASSID=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\"
					WIDTH=\"{$live_width}\" HEIGHT=\"{$live_height}\"
					CODEBASE=\"http://www.apple.com/qtactivex/qtplugin.cab\">
					<PARAM name=\"SRC\" VALUE=\"{$movie_url}\">
					<PARAM name=\"AUTOPLAY\" VALUE=\"{$live_auto_start}\">
					<PARAM name=\"CONTROLLER\" VALUE=\"true\">
					<EMBED SRC=\"{$movie_url}\" WIDTH=\"{$live_width}\" HEIGHT=\"{$live_height}\" AUTOPLAY=\"{$live_auto_start}\"
					CONTROLLER=\"true\" PLUGINSPAGE=\"http://www.apple.com/quicktime/download/\"
					kioskmode=\"true\">
					</EMBED>
					</OBJECT>
					";
	} 
	else
	{
		$TVLink = "
	        			<OBJECT onresize=\"$('#MediaPlayer1').width({$live_width});$('#MediaPlayer1').height({$live_height});\" ID=\"MediaPlayer1\" width=\"{$live_width}\" height=\"{$live_height}\"
	             		classid=\"CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95\"
	             		codebase=\"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715\"
	                	standby=\"Loading MicrosoftR WindowsR Media Player components...\"
	                	type=\"application/x-oleobject\">
	          			<PARAM NAME=\"FileName\" VALUE=\"{$movie_url}\">
	          			<PARAM NAME=\"AutoStart\" VALUE=\"{$live_auto_start}\">
	          			<param name=\"AllowChangeDisplaySize\" value=\"-1\">
	          			<param name=\"ShowControls\" value=\"true\">
	          			<param name=\"Mute\" value=\"0\">
	                  	<PARAM NAME=\"ShowStatusBar\" VALUE=\"True\">
	                  	<PARAM NAME=\"windowlessVideo\" VALUE=\"True\">
	          			<EMBED type=\"application/x-mplayer2\" onresize=\"alert('size2');$('#MediaPlayer1').width({$live_width});$('#MediaPlayer1').height({$live_height});\"
	             		pluginspage = \"http://www.microsoft.com/Windows/Downloads/Contents/Products/MediaPlayer/\"
	             		SRC=\"{$movie_url}\"
	             		name=\"MediaPlayer1\"
	             		width=\"{$live_width}\"
	             		height=\"{$live_height}\"
	             		AutoStart=\"{$live_auto_start}\">
	          			</EMBED>
	        			</OBJECT>
	        			<script language='javascript'>
	        			setTimeout(\"$('#MediaPlayer1').width({$live_width});$('#MediaPlayer1').height({$live_height});\", 1000);
	        			setTimeout(\"$('#MediaPlayer1').width({$live_width});$('#MediaPlayer1').height({$live_height});\", 2000);
	        			setTimeout(\"$('#MediaPlayer1').width({$live_width});$('#MediaPlayer1').height({$live_height});\", 3000);
	        			</script>
						";
	}
	//<PARAM NAME=\"windowlessVideo\" VALUE=\"True\">
	//windowlessVideo =\"True\"
	
	$x .= "
			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
			<tr>
				<td width=\"10\" height=\"10\"><img src=\"/images/2009a/index/campusTV/tv_screen_01.gif\" width=\"10\" height=\"10\"></td>
				<td background=\"/images/2009a/index/campusTV/tv_screen_02.gif\" height=\"10\"><img src=\"/images/2009a/index/campusTV/tv_screen_02.gif\" width=\"10\" height=\"10\"></td>
				<td width=\"10\" height=\"10\"><img src=\"/images/2009a/index/campusTV/tv_screen_03.gif\" width=\"10\" height=\"10\"></td>
			</tr>
			<tr>
				<td width=\"10\" background=\"/images/2009a/index/campusTV/tv_screen_04.gif\"><img src=\"/images/2009a/index/campusTV/tv_screen_04.gif\" width=\"10\" height=\"10\"></td>
				<td width=\"280\" bgcolor=\"#000000\">
					<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr>
						<td class=\"indextvplaying\" align=\"center\">". $MovieTitle ."</td>
					</tr>
					<tr>
						<td>". $TVLink ."</td>
					</tr>
					</table>
				</td>
				<td width=\"10\" background=\"/images/2009a/index/campusTV/tv_screen_06.gif\"><img src=\"/images/2009a/index/campusTV/tv_screen_06.gif\" width=\"10\" height=\"10\"></td>
			</tr>
			<tr>
				<td width=\"10\" height=\"10\"><img src=\"/images/2009a/index/campusTV/tv_screen_07.gif\" width=\"10\" height=\"10\"></td>
				<td background=\"/images/2009a/index/campusTV/tv_screen_08.gif\" height=\"10\"><img src=\"/images/2009a/index/campusTV/tv_screen_08.gif\" width=\"10\" height=\"10\"></td>
				<td width=\"10\" height=\"10\"><img src=\"/images/2009a/index/campusTV/tv_screen_09.gif\" width=\"10\" height=\"10\"></td>
			</tr>
			</table>
		";
}
else
{
	$x = "";	
}
intranet_closedb();

echo $x;
?>