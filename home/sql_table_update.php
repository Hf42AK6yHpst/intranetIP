<?
# modifying by : 

// the schema update from 2010-03-xx to 2010-04-xx had all changed to 2010-04-29 
// as the mistakes made on eclass auto update that created by SOLUTION!!!!!

############################################################################################
################################# IntranetIP 2.5 schema update #################################
############################################################################################

$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Add Building Level",
	"CREATE TABLE IF NOT EXISTS INVENTORY_LOCATION_BUILDING (
		BuildingID int(11) NOT NULL auto_increment,
		Code varchar(10),
		NameChi varchar(255),
		NameEng varchar(255),
		DisplayOrder int(11),
		RecordType int(11),
		RecordStatus int(11),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY (BuildingID),
		INDEX BuildingID (BuildingID),
		INDEX InventoryBuildingNameChi (NameChi),
		INDEX InventoryBuildingNameEng (NameEng),
		UNIQUE ChineseName (NameChi),
		UNIQUE EnglishName (NameEng),
		UNIQUE BuildingIDWithCode (BuildingID, Code)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Add building-floor mapping in the floor table",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL ADD BuildingID int(11), ADD INDEX InventoryBuildingID (BuildingID);"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Record Last Modified UserID for Building",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING Add LastModifiedBy int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Record Last Modified UserID for Floor",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL Add LastModifiedBy int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Record Last Modified UserID for Room",
	"ALTER TABLE INVENTORY_LOCATION Add LastModifiedBy int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Add Description Field to Room",
	"ALTER TABLE INVENTORY_LOCATION Add Description text default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Change Engine of Floor Table",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL ENGINE = InnoDB;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Change Engine of Room Table",
	"ALTER TABLE INVENTORY_LOCATION ENGINE = InnoDB;"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create academic year table",
	"CREATE TABLE IF NOT EXISTS ACADEMIC_YEAR (
	  AcademicYearID int(8) NOT NULL auto_increment,
	  YearNameEN varchar(50) default NULL,
	  YearNameB5 varchar(255) default NULL,
	  Sequence int(2) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (AcademicYearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create academic year term table",
	"CREATE TABLE IF NOT EXISTS ACADEMIC_YEAR_TERM (
	  YearTermID int(8) NOT NULL auto_increment,
	  AcademicYearID int(8) default NULL,
	  TermID int(8) default NULL,
	  YearTermNameEN varchar(255) default NULL,
	  YearTermNameB5 varchar(255) default NULL,
	  TermStart datetime default NULL,
	  TermEnd datetime default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (YearTermID),
	  UNIQUE KEY YearTerm (AcademicYearID,TermID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

/*$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Name of Class table",
	"CREATE TABLE CLASS (
	  ClassID int(8) NOT NULL auto_increment,
	  DepartmentID int(8) default NULL,
	  ClassCode varchar(50) default NULL,
	  ClassNameB5 varchar(255) default NULL,
	  ClassNameEN varchar(255) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  Sequence int(2) default NULL,
	  PRIMARY KEY  (ClassID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject table",
	"CREATE TABLE SUBJECT (
	  SubjectID int(8) NOT NULL auto_increment,
	  SubjectShortFormB5 varchar(100) NOT NULL,
	  SubjectShortFormEN varchar(100) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  SubjectDescB5 text,
	  SubjectDescEN text,
	  Sequence int(2) default NULL,
	  WEBSAMSCode varchar(10) NOT NULL,
	  SubjectAbbrEN varchar(100) NOT NULL,
	  SubjectAbbrB5 varchar(100) NOT NULL,
	  PRIMARY KEY  (SubjectID),
	  UNIQUE KEY WEBSAMSCode (WEBSAMSCode)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject component table",
	"CREATE TABLE SUBJECT_COMPONENT (
	  SubjectComponentID int(8) NOT NULL auto_increment,
	  SubjectComponentNameB5 varchar(255) default NULL,
	  SubjectComponentNameEN varchar(255) default NULL,
	  SubjectComponentDescB5 text,
	  SubjectComponentDescEN text,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  SubjectCode varchar(5) default NULL,
	  SubjectComponentCode varchar(5) default NULL,
	  PRIMARY KEY  (SubjectComponentID),
	  UNIQUE KEY SubjectID (SubjectComponentID),
	  UNIQUE KEY SubjectCode (SubjectCode,ComponentSubjectCode)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
*/

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject term table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM (
	  SubjectTermID int(8) NOT NULL auto_increment,
	  SubjectID int(8) default NULL,
	  YearTermID int(8) default NULL,
	  SubjectGroupID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (SubjectTermID),
	  UNIQUE KEY SubjectTerm (SubjectID,YearTermID,SubjectGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject term class table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS (
	  SubjectGroupID int(8) NOT NULL,
	  ClassCode varchar(50) default NULL,
	  ClassTitleEN varchar(255) default NULL,
	  ClassTitleB5 varchar(255) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (SubjectGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject class teacher table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS_TEACHER (
	  SubjectClassTeacherID int(8) NOT NULL auto_increment,
	  SubjectGroupID int(8) NOT NULL,
	  UserID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifiedBy int(8) default NULL,
	  PRIMARY KEY  (SubjectClassTeacherID),
	  UNIQUE KEY YearTerm (SubjectGroupID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject class student table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS_USER (
	  SubjectClassUserID int(8) NOT NULL auto_increment,
	  SubjectGroupID int(8) NOT NULL,
	  UserID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifiedBy int(8) default NULL,
	  PRIMARY KEY  (SubjectClassUserID),
	  UNIQUE KEY YearTerm (SubjectGroupID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject class form relation table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS_YEAR_RELATION (
	  SubjectGroupID int(8) NOT NULL,
	  YearID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (SubjectGroupID,YearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Term table",
	"CREATE TABLE IF NOT EXISTS TERM (
	  TermID int(8) NOT NULL auto_increment,
	  TermNameEN varchar(255) default NULL,
	  TermNameB5 varchar(255) default NULL,
	  Sequence int(2) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (TermID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Form table",
	"CREATE TABLE IF NOT EXISTS YEAR (
	  YearID int(8) NOT NULL auto_increment,
	  YearName varchar(50) default NULL,
	  Sequence int(2) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  WEBSAMSCode varchar(255) default NULL,
	  PRIMARY KEY  (YearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Form Class table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS (
	  YearClassID int(8) NOT NULL auto_increment,
	  AcademicYearID int(8) default NULL,
	  YearID int(8) default NULL,
	  ClassID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  ClassTitleEN varchar(255) default NULL,
	  ClassTitleB5 varchar(255) default NULL,
	  Sequence int(2) default NULL,
	  PRIMARY KEY  (YearClassID),
	  UNIQUE KEY YearClass (AcademicYearID,YearID,ClassID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Form Class Subject table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS_SUBJECT (
	  YearClassSubjectID int(8) NOT NULL auto_increment,
	  YearClassID int(8) default NULL,
	  SubjectGroupID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (YearClassSubjectID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Form Class Teacher table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS_TEACHER (
	  YearClassTeacherID int(8) NOT NULL auto_increment,
	  YearClassID int(8) default NULL,
	  UserID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (YearClassTeacherID),
	  UNIQUE KEY YearClassUser (YearClassID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create form class student table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS_USER (
	  YearClassUserID int(8) NOT NULL auto_increment,
	  YearClassID int(8) default NULL,
	  UserID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  ClassNumber int(8) default NULL,
	  PRIMARY KEY  (YearClassUserID),
	  UNIQUE KEY YearClassUser (YearClassID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-25",
	"School Settings -> Subject - Add Subject-LearningCategory Mapping in the Subject Table",
	"ALTER Table SUBJECT add LearningCategoryID int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-25",
	"School Settings -> Subject - RecordStatus of the Subject: 0 - inactive; 1 - active",
	"ALTER Table SUBJECT add RecordStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array(
	"2009-05-25",
	"School Settings -> Subject - RecordStatus of the Subject Component: 0 - inactive; 1 - active",
	"ALTER Table SUBJECT_COMPONENT add RecordStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array(
	"2009-05-25",
	"School Settings -> Class - Create form class student table",
	"CREATE TABLE IF NOT EXISTS LEARNING_CATEGORY (
		LearningCategoryID int(11) auto_increment,
		Code varchar(10) default NULL,
		NameChi varchar(255) default NULL,
		NameEng varchar(255) default NULL,
		DisplayOrder int(11) default NULL,
		RecordType int(11) default NULL,
		RecordStatus int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		LastModifiedBy int(11) default NULL,
		PRIMARY KEY (LearningCategoryID),
		KEY LearningCategoryCode (Code),
		KEY LearningCategoryNameChi (NameChi),
		KEY LearningCategoryNameEng (NameEng)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Rename ComponentSubjectCode to SubjectComponentCode for consistency",
	"ALTER TABLE SUBJECT_COMPONENT CHANGE ComponentSubjectCode SubjectComponentCode varchar(5) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Add Sequence field for Subject Component",
	"ALTER TABLE SUBJECT_COMPONENT ADD Sequence int(5) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT drop SubjectComponentNameEN;"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT drop SubjectComponentNameB5;"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT add SubjectComponentAbbrB5 varchar(100) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT add SubjectComponentAbbrEN varchar(100) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT add SubjectComponentShortFormB5 varchar(100) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT add SubjectComponentShortFormEN varchar(100) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Unify the data type of SubjectCode of Subject and SubjectComponent",
	"ALTER TABLE SUBJECT_COMPONENT CHANGE SubjectCode SubjectCode varchar(10) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Unify the data type of SubjectCode of Subject and SubjectComponent",
	"ALTER TABLE SUBJECT_COMPONENT CHANGE SubjectComponentCode SubjectComponentCode varchar(10) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Set an unique code among Subject and Subject Component",
	"ALTER TABLE SUBJECT Add UniqueSubjectID int(8) NOT NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Set an unique code among Subject and Subject Component",
	"ALTER TABLE SUBJECT Add INDEX UniqueSubjectID (UniqueSubjectID);"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Set an unique code among Subject and Subject Component",
	"ALTER TABLE SUBJECT_COMPONENT Add UniqueSubjectID int(8) NOT NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Set an unique code among Subject and Subject Component",
	"ALTER TABLE SUBJECT_COMPONENT Add INDEX UniqueSubjectID (UniqueSubjectID);"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Location - Building English Name need not unique",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING DROP Index EnglishName;"
);
$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Location - Building Chinese Name need not unique",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING DROP Index ChineseName;"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Create subject table",
	"CREATE TABLE IF NOT EXISTS ASSESSMENT_SUBJECT (
		RecordID int(11) NOT NULL auto_increment,
		CODEID varchar(20) default NULL,
		EN_SNAME varchar(20) default NULL,
		CH_SNAME varchar(20) default NULL,
		EN_DES varchar(255) default NULL,
		CH_DES varchar(255) default NULL,
		EN_ABBR varchar(100) default NULL,
		CH_ABBR varchar(100) default NULL,
		DisplayOrder int(11) default NULL,
		AllLevelSubject int(11) default NULL,
		Level1Subject int(11) default NULL,
		Level2Subject int(11) default NULL,
		Level3Subject int(11) default NULL,
		Level4Subject int(11) default NULL,
		Level5Subject int(11) default NULL,
		Level6Subject int(11) default NULL,
		Level7Subject int(11) default NULL,
		Level8Subject int(11) default NULL,
		Level9Subject int(11) default NULL,
		InputDate datetime default NULL,
		ModifiedDate datetime default NULL,
		CMP_CODEID varchar(20) default NULL,
		LearningCategoryID varchar(8) default NULL,
		LastModifiedBy int(8) default NULL,
		RecordStatus tinyint(1) default '1',
		PRIMARY KEY (RecordID),
		UNIQUE KEY SBJ_CMP_CODEID (CODEID,CMP_CODEID),
		KEY DisplayOrder (DisplayOrder),
		KEY LearningCategoryID (LearningCategoryID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Add LearningCategory-Subject mapping in the subject table",
	"ALTER TABLE ASSESSMENT_SUBJECT ADD LearningCategoryID varchar(8) default null;"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Set LearningCategoryID as an index",
	"ALTER TABLE ASSESSMENT_SUBJECT ADD INDEX LearningCategoryID (LearningCategoryID);"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Add LastModifiedBy field to record the use who modified the record",
	"ALTER TABLE ASSESSMENT_SUBJECT ADD LastModifiedBy int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Add RecordStatus field to record active or inactive subject",
	"ALTER TABLE ASSESSMENT_SUBJECT ADD RecordStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array(
	"2009-06-05",
	"create ROLE",
	"CREATE TABLE IF NOT EXISTS ROLE (
	  RoleID int(8) NOT NULL auto_increment,
	  RoleName varchar(100) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (RoleID),
	  UNIQUE KEY RoleName (RoleName)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-05",
	"create ROLE_MEMBER",
	"CREATE TABLE IF NOT EXISTS ROLE_MEMBER (
	  RoleID int(8) NOT NULL,
	  UserID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  IdentityType varchar(20) default NULL,
	  PRIMARY KEY  (RoleID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$sql_eClassIP_update[] = array(
	"2009-06-05",
	"create ROLE_RIGHT table",
	"CREATE TABLE IF NOT EXISTS ROLE_RIGHT (
	  RoleID int(8) NOT NULL,
	  FunctionName varchar(200) NOT NULL,
	  RightFlag int(1) default '0',
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (RoleID,FunctionName)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$sql_eClassIP_update[] = array(
	"2009-06-05",
	"create ROLE_RIGHT table",
	"CREATE TABLE IF NOT EXISTS ROLE_TARGET (
	  RoleID int(8) NOT NULL,
	  TargetName varchar(200) NOT NULL,
	  RightFlag int(1) default '0',
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (RoleID,TargetName)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - Create Temp cycle period setting table > INTRANET_CYCLE_GENERATION_PERIOD",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_GENERATION_PERIOD (
	  PeriodID int(11) NOT NULL auto_increment,
	  PeriodStart date NOT NULL default '0000-00-00',
	  PeriodEnd date NOT NULL default '0000-00-00',
	  PeriodType int(11) default NULL,
	  CycleType int(11) default NULL,
	  PeriodDays int(11) default NULL,
	  FirstDay int(11) default NULL,
	  SaturdayCounted int(11) default NULL,
	  Sequence int(2) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY (PeriodID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create tmp cycle period preview table > INTRANET_CYCLE_TEMP_DAYS_VIEW",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_TEMP_DAYS_VIEW (
	  RecordDate date NOT NULL default '0000-00-00',
	  TextEng varchar(50) default NULL,
	  TextChi varchar(50) default NULL,
	  TextShort varchar(50) default NULL,
	  PRIMARY KEY (RecordDate)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create cycle period production view table > INTRANET_CYCLE_DAYS",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_DAYS (
	  RecordDate date NOT NULL default '0000-00-00',
	  TextEng varchar(50) default NULL,
	  TextChi varchar(50) default NULL,
	  TextShort varchar(50) default NULL,
	  PRIMARY KEY (RecordDate)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create cycle period import table > INTRANET_CYCLE_IMPORT_RECORD",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_IMPORT_RECORD (
	  RecordID int(11) NOT NULL auto_increment,
	  RecordDate date NOT NULL default '0000-00-00',
	  TextEng varchar(50) default NULL,
	  TextChi varchar(50) default NULL,
	  TextShort varchar(50) default NULL,
	  PRIMARY KEY  (RecordID),
	  UNIQUE KEY RecordDate (RecordDate)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create cycle period gen production view log table > INTRANET_CYCLE_DAYS_PRODUCTION_LOG",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_DAYS_PRODUCTION_LOG (
	  LogID int(11) NOT NULL auto_increment,
	  DateInput datetime default NULL,
	  PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - add column to store cycle period bgcolor in INTRANET_CYCLE_GENERATION_PERIOD",
	"ALTER TABLE INTRANET_CYCLE_GENERATION_PERIOD ADD COLUMN ColorCode varchar(10) AFTER SaturdayCounted"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create school event / holiday table > INTRANET_EVENT",
	"CREATE TABLE IF NOT EXISTS INTRANET_EVENT (
	  EventID int(8) NOT NULL auto_increment,
	  Title varchar(255) default NULL,
	  Description text,
	  EventDate datetime default NULL,
	  EventVenue varchar(100) default NULL,
	  EventNature varchar(100) default NULL,
	  ReadFlag text,
	  UserID int(8) default NULL,
	  isSkipCycle tinyint(4) default NULL,
	  Internal int(11) default NULL,
	  RecordType char(2) default NULL,
	  RecordStatus char(2) default NULL,
	  OwnerGroupID int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (EventID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-24",
	"add InternalClassCode for batch update subject group in Subject group mapping",
	"alter table SUBJECT_TERM_CLASS add InternalClassCode varchar(50) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-07-08",
	"School Settings -> Timetable - create table storing timetable information",
	"CREATE TABLE IF NOT EXISTS INTRANET_SCHOOL_TIMETABLE (
		TimetableID int(11) NOT NULL auto_increment,
		AcademicYearID int(11) default NULL,
		YearTermID int(11) default NULL,
		TimetableName varchar(255) default NULL,
		CycleDays tinyint(2) default '1',
		RecordType int(11) default NULL,
		RecordStatus int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (TimetableID),
		KEY AcademicYearID (AcademicYearID),
		KEY YearTermID (YearTermID),
		KEY CycleDays (CycleDays)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-07-08",
	"School Settings -> Timetable - create table storing timetable time slot",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMETABLE_TIMESLOT (
		TimeSlotID int(11) NOT NULL auto_increment,
		TimeSlotName varchar(255) NOT NULL,
		StartTime time NOT NULL,
		EndTime time NOT NULL,
		DisplayOrder tinyint(2) default NULL,
		RecordType int(11) default NULL,
		RecordStatus int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (TimeSlotID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-07-08",
	"School Settings -> Timetable - create table storing timetable time slot relationship",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMETABLE_TIMESLOT_RELATION (
		TimetableID int(11) NOT NULL,
		TimeSlotID int(11) NOT NULL,
		RecordType int(11) default NULL,
		RecordStatus int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-07-08",
	"School Settings -> Timetable - create table storing room allocation information",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMETABLE_ROOM_ALLOCATION (
		RoomAllocationID int(11) NOT NULL auto_increment,
		TimetableID int(11) NOT NULL,
		TimeSlotID int(11) NOT NULL,
		Day tinyint(2) NOT NULL,
		LocationID int(11) default NULL,
		OthersLocation varchar(255) default NULL,
		SubjectGroupID int(8) NOT NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		LastModifiedBy int(11) default NULL,
		PRIMARY KEY (RoomAllocationID),
		KEY TimetableID (TimetableID),
		KEY TimeSlotID (TimeSlotID),
		KEY Day (Day),
		KEY LocationID (LocationID),
		KEY SubjectGroupID (SubjectGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

/*
$sql_eClassIP_update[] = array(
	"2009-07-10",
	"Role setting -> role target -> add Identity field",
	"alter table ROLE_TARGET add Identity varchar(15) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-07-13",
	"Role Target Restructure",
	"alter table ROLE_TARGET DROP PRIMARY KEY"
);

$sql_eClassIP_update[] = array(
	"2009-07-13",
	"Role Target Restructure",
	"alter table ROLE_TARGET ADD PRIMARY KEY (RoleID,TargetName,Identity)"
);
*/

$sql_eClassIP_update[] = array(
	"2009-07-13",
	"Table for General Settings",
	"CREATE TABLE IF NOT EXISTS GENERAL_SETTING (
	  Module varchar(20) NOT NULL default '',
	  SettingName varchar(100) NOT NULL default '',
	  SettingValue text,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (Module,SettingName)
	) ENGINE=innoDB DEFAULT CHARSET=utf8");

$sql_eClassIP_update[] = array(
	"2009-07-13",
	"Change Class Number format in INTRANET_USER to integer",
	"alter table INTRANET_USER change ClassNumber ClassNumber int(8) default NULL;");

$sql_eClassIP_update[] = array(
	"2009-07-20",
	"Table for Campus Link",
	"CREATE TABLE IF NOT EXISTS INTRANET_CAMPUS_LINK (
	LinkID int(8) NOT NULL auto_increment,
	Title varchar(255) default NULL,
	URL varchar(255) default NULL,
	DisplayOrder int(11) default NULL,
	Description text,
	DateInput datetime default NULL,
	InputBy int(11) default NULL,
	DateModified datetime default NULL,
	ModifiedBy int(11) default NULL,
	PRIMARY KEY  (LinkID)
	)  ENGINE=innoDB DEFAULT CHARSET=utf8
");

$sql_eClassIP_update[] = array(
	"2009-07-21",
	"Change table PAYMENT_PAYMENT_ITEMSTUDENT field Amount to Double, to prevent overflow problem",
	"alter table PAYMENT_PAYMENT_ITEMSTUDENT change Amount Amount Double(20,2) default NULL");

$sql_eClassIP_update[] = array(
	"2009-07-21",
	"Change table PAYMENT_PAYMENT_ITEMSTUDENT field SubsidyAmount to Double, to prevent overflow problem",
	"alter table PAYMENT_PAYMENT_ITEMSTUDENT change SubsidyAmount SubsidyAmount Double(20,2) default NULL");

$sql_eClassIP_update[] = array(
	"2009-07-21",
	"Change table PAYMENT_SUBSIDY_UNIT field TotalAmount to Double, to prevent overflow problem",
	"alter table PAYMENT_SUBSIDY_UNIT change TotalAmount TotalAmount double(20,2) default NULL");

$sql_eClassIP_update[] = array(
"2009-07-23",
"create a LSLP user table for license control",
"CREATE TABLE IF NOT EXISTS LS_USER (
  UserID int(11) NOT NULL,
  LsUserID int(11),
  DateInput datetime NOT NULL,
  LastUsed datetime,
  PRIMARY KEY (UserID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-07-24",
	"Add column 'Module' to table ACCESS_RIGHT_GROUP",
	"ALTER TABLE ACCESS_RIGHT_GROUP ADD COLUMN Module varchar(50) DEFAULT 'DISCIPLINE';");

$sql_eClassIP_update[] = array(
	"2009-07-27",
	"Add column 'AcademicYearID' to INTRANET_GROUP",
	"alter table INTRANET_GROUP add AcademicYearID int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-07-29",
	"Add column 'YearTermID' to DISCIPLINE_MERIT_RECORD",
	"alter table DISCIPLINE_MERIT_RECORD add YearTermID int(8) default NULL AFTER Semester;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'AcademicYearID' to DISCIPLINE_MERIT_RECORD",
	"alter table DISCIPLINE_MERIT_RECORD add AcademicYearID int(8) default NULL AFTER Year;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'YearTermID' to DISCIPLINE_ACCU_RECORD",
	"alter table DISCIPLINE_ACCU_RECORD add YearTermID int(8) default NULL AFTER Semester;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'AcademicYearID' to DISCIPLINE_ACCU_RECORD",
	"alter table DISCIPLINE_ACCU_RECORD add AcademicYearID int(8) default NULL AFTER Year;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'GroupID' to YEAR_CLASS",
	"alter table YEAR_CLASS add GroupID int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'YearTermID' to DISCIPLINE_CASE",
	"alter table DISCIPLINE_CASE add YearTermID int(8) default NULL AFTER Semester;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'AcademicYearID' to DISCIPLINE_CASE",
	"alter table DISCIPLINE_CASE add AcademicYearID int(8) default NULL AFTER Year;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"Make INTRANET_USER Class Number field from varchar(20) to varchar(255)",
	"alter table INTRANET_USER change ClassName ClassName varchar(255) DEFAULT NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column InputBy int(8)",
	"ALTER TABLE INTRANET_EVENT ADD COLUMN InputBy int(8) after DateInput;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column ModifyBy int(8)",
	"ALTER TABLE INTRANET_EVENT ADD COLUMN ModifyBy int(8) after DateModified;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Time Zone - Add column InputBy int(8)",
	"alter table INTRANET_CYCLE_GENERATION_PERIOD add column InputBy int(8) after DateInput;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Time Zone - Add column ModifyBy int(8)",
	"alter table INTRANET_CYCLE_GENERATION_PERIOD add column ModifyBy int(8) after DateModified;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column RelatedTo int(8)",
	"alter table INTRANET_EVENT add column RelatedTo int(8) NULL AFTER Internal;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column EventLocationID int(8)",
	"alter table INTRANET_EVENT add column EventLocationID int(8) after EventDate;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column EventLocationID int(8)",
	"alter table INTRANET_EVENT add column EventLocationID int(8) after EventDate;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Time Zone - Add column Sequence int(2)",
	"alter table INTRANET_CYCLE_GENERATION_PERIOD add column Sequence int(2) after ColorCode;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Time Zone - Create table INTRANET_PERIOD_TIMETABLE_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_PERIOD_TIMETABLE_RELATION (
  	PeriodID int(11) NOT NULL,
  	TimetableID int(11) NOT NULL,
  	RecordType int(11) default NULL,
  	RecordStatus int(11) default NULL,
  	DateInput datetime default NULL,
  	DateModified datetime default NULL,
  	PRIMARY KEY (PeriodID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
"2009-08-10",
"SmartCard Lunch-box",
"CREATE TABLE IF NOT EXISTS CARD_STUDENT_LUNCH_CALENDAR (
 RecordID int NOT NULL auto_increment,
 Year int,
 Month int,
 DaysString text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE YearMonth (Year, Month)
) ENGINE=InnoDB Charset=utf8"
);


$sql_eClassIP_update[] = array(
"2009-08-10",
"SmartCard Lunch-box",
"CREATE TABLE IF NOT EXISTS CARD_STUDENT_LUNCH_BAD_LOG (
 RecordID int NOT NULL auto_increment,
 RecordDate date,
 StudentID int,
 ActionTime datetime,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StudentID (StudentID),
 INDEX RecordType (RecordType),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB Charset=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-08-11",
	"School Settings -> Class - Add WEBSAMSCode field",
	"alter table YEAR_CLASS add column WEBSAMSCode varchar(5)"
);

$sql_eClassIP_update[] = array(
	"2009-08-13",
	"Create table for deletion log",
	"CREATE TABLE IF NOT EXISTS MODULE_RECORD_DELETE_LOG
	(
		LogID int(11) NOT NULL auto_increment,
		Module varchar(20) NOT NULL,
		Section varchar(50) default NULL,
		RecordDetail text default NULL,
		DelTableName varchar(50) default NULL,
		DelRecordID int(11) default NULL,
		LogDate datetime default NULL,
		LogBy int(11) default NULL,
		PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_CALENDAR",
	"Alter Table CALENDAR_CALENDAR
		Add Column CalSharedType char(1),
		Add Column SyncURL varchar(200)
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_CALENDAR_VIEWER",
	"Alter Table CALENDAR_CALENDAR_VIEWER
		Add Column GroupPath varchar(255)
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_ENTRY",
	"Alter Table CALENDAR_EVENT_ENTRY
		Add Column UID varchar(100),
		Add Column ExtraIcalInfo mediumtext
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_ENTRY_REPEAT",
	"Alter Table CALENDAR_EVENT_ENTRY_REPEAT
		Add Column ICalIsInfinite char(1) Default 0
	"
);

$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_PERSONAL_NOTE",
	"Alter Table CALENDAR_EVENT_PERSONAL_NOTE
		Add Column CalType tinyint(4)
	"
);

$sql_eClassIP_update[] = array(
	"2009-08-20",
	"Lesson Attendance Default plan table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_ATTEND_DEFAULT_PLAN (
	  PlanID int(8) NOT NULL auto_increment,
	  SubjectGroupID int(8) NOT NULL default '0',
	  PlanName varchar(255) default NULL,
	  StudentPositionAMF text,
	  PlanLayoutAMF text,
	  LocationID int(8) default NULL,
	  CreateBy int(8) default NULL,
	  CreateDate datetime default NULL,
	  LastModifiedBy int(8) default NULL,
	  LastModifiedDate datetime default NULL,
	  PRIMARY KEY  (PlanID,SubjectGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
"2009-08-20",
"Change GroupCategoryType from char(2) to int(11)",
"alter table INTRANET_ROLE change RecordType RecordType int(11);"
);

$sql_eClassIP_update[] = array(
	"2009-08-21",
	"eClass update history table",
	"CREATE TABLE IF NOT EXISTS ECLASS_UPDATE_HISTORY (
	  HistoryID int(8) NOT NULL auto_increment,
	  EventTime datetime default NULL,
	  Events text,
	  Status varchar(16),
	  ReferenceID int(11),
	  PRIMARY KEY (HistoryID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-08-21",
	"Lesson attendance Default Plan - add plan id generate by Flash",
	"alter table SUBJECT_GROUP_ATTEND_DEFAULT_PLAN add FlashPlanID varchar(20) after SubjectGroupID;"
);

$sql_eClassIP_update[] = array(
	"2009-08-25",
	"[Sport Day] Add code to event",
	"alter table SPORTS_EVENTGROUP add EventCode varchar(20);"
);

$sql_eClassIP_update[] = array(
	"2009-08-25",
	"[Swimming Gala] Add code to event",
	"alter table SWIMMINGGALA_EVENTGROUP add EventCode varchar(20);"
);

$sql_eClassIP_update[] = array(
	"2009-08-26",
	"Add academic Year ID to Homework record",
	"alter table INTRANET_HOMEWORK add AcademicYearID int(8)"
);

$sql_eClassIP_update[] = array(
	"2009-08-28",
	"enlarge the size of UID in Calendar event entry",
	"alter table CALENDAR_EVENT_ENTRY modify COLUMN UID varchar(200)"
);

$sql_eClassIP_update[] = array(
	"2009-08-31",
	"Add academic Year Term ID to Homework record",
	"alter table INTRANET_HOMEWORK add YearTermID int(8)"
);

$sql_eClassIP_update[] = array(
	"2009-09-03",
	"Drop UNIQUE KEY on Title to INTRANET_GROUP",
	"alter table INTRANET_GROUP drop KEY title;"
);

$sql_eClassIP_update[] = array(
	"2009-09-03",
	"Add UNIQUE KEY to column 'Title,AcademicYearID' to INTRANET_GROUP",
	"alter table INTRANET_GROUP ADD KEY AcademicYearTitle (Title,AcademicYearID);"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"Add description to CALENDAR_CALENDAR",
	"alter table CALENDAR_CALENDAR ADD column Description mediumtext;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"Add groupType, groupID to CALENDAR_CALENDAR",
	"alter table CALENDAR_CALENDAR_VIEWER ADD column GroupID int(8), ADD column GroupType char(1)"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"Add CalType to CALENDAR_REMINDER",
	"alter table CALENDAR_REMINDER Add Column CalType tinyint(1)"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"Add groupType, groupID to CALENDAR_CALENDAR",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE Drop Key EventID, Drop Key UserID, Add Column PersonalNoteID int auto_increment primary key"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"RENAME TABLE SUBJECT_GROUP_ATTEND_DEFAULT_PLAN TO ATTEND_DEFAULT_PLAN;",
	"RENAME TABLE SUBJECT_GROUP_ATTEND_DEFAULT_PLAN TO ATTEND_DEFAULT_PLAN;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN change PlanID PlanID int(8) default NULL;",
	"alter table ATTEND_DEFAULT_PLAN change PlanID PlanID int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN drop PRIMARY KEY;",
	"alter table ATTEND_DEFAULT_PLAN drop PRIMARY KEY;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN ADD PRIMARY KEY (PlanID);",
	"alter table ATTEND_DEFAULT_PLAN ADD PRIMARY KEY (PlanID);"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN change PlanID PlanID int(8) NOT NULL auto_increment;",
	"alter table ATTEND_DEFAULT_PLAN change PlanID PlanID int(8) NOT NULL auto_increment;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN add YearClassID int (8) after SubjectGroupID;",
	"alter table ATTEND_DEFAULT_PLAN add YearClassID int (8) after SubjectGroupID;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN change LocationID LocationID int (8) after YearClassID;",
	"alter table ATTEND_DEFAULT_PLAN change LocationID LocationID int (8) after YearClassID;"
);

$sql_eClassIP_update[] = array(
	"2009-09-07",
	"create general log file to log down crond job working status",
	"CREATE TABLE IF NOT EXISTS GENERAL_LOG (
	  RecordID int(8) NOT NULL auto_increment,
	  Process varchar(50) NOT NULL default '',
	  Result varchar(1),
	  Remarks text,
	  RunDate TIMESTAMP not NULL default CURRENT_TIMESTAMP,
	  PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Alter SUBJECT_TERM_CLASS table for eclass mapping",
	"alter table SUBJECT_TERM_CLASS add course_id int(8)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index TimetableID in INTRANET_TIMETABLE_TIMESLOT_RELATION",
	"alter table INTRANET_TIMETABLE_TIMESLOT_RELATION add index TimetableID (TimetableID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index TimeSlotID in INTRANET_TIMETABLE_TIMESLOT_RELATION",
	"alter table INTRANET_TIMETABLE_TIMESLOT_RELATION add index TimeSlotID (TimeSlotID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM_CLASS",
	"alter table SUBJECT_TERM_CLASS add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearID in SUBJECT_TERM_CLASS_YEAR_RELATION",
	"alter table SUBJECT_TERM_CLASS_YEAR_RELATION add index YearID (YearID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM_CLASS_YEAR_RELATION",
	"alter table SUBJECT_TERM_CLASS_YEAR_RELATION add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM_CLASS_TEACHER",
	"alter table SUBJECT_TERM_CLASS_TEACHER add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index UserID in SUBJECT_TERM_CLASS_TEACHER",
	"alter table SUBJECT_TERM_CLASS_TEACHER add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM_CLASS_USER",
	"alter table SUBJECT_TERM_CLASS_USER add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index UserID in SUBJECT_TERM_CLASS_USER",
	"alter table SUBJECT_TERM_CLASS_USER add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectID in SUBJECT_TERM",
	"alter table SUBJECT_TERM add index SubjectID (SubjectID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearTermID in SUBJECT_TERM",
	"alter table SUBJECT_TERM add index YearTermID (YearTermID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM",
	"alter table SUBJECT_TERM add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index AcademicYearID in YEAR_CLASS",
	"alter table YEAR_CLASS add index AcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearID in YEAR_CLASS",
	"alter table YEAR_CLASS add index YearID (YearID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index GroupID in YEAR_CLASS",
	"alter table YEAR_CLASS add index GroupID (GroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearClassID in YEAR_CLASS_TEACHER",
	"alter table YEAR_CLASS_TEACHER add index YearClassID (YearClassID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index UserID in YEAR_CLASS_TEACHER",
	"alter table YEAR_CLASS_TEACHER add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearClassID in YEAR_CLASS_USER",
	"alter table YEAR_CLASS_USER add index YearClassID (YearClassID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index UserID in YEAR_CLASS_USER",
	"alter table YEAR_CLASS_USER add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index AcademicYearID in ACADEMIC_YEAR_TERM",
	"alter table ACADEMIC_YEAR_TERM add index AcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
"2009-09-10",
"Staff Smart Card Attendance System - Add Table to store user customized attendance status symbols",
"CREATE TABLE IF NOT EXISTS CARD_STAFF_ATTENDANCE_STATUS_SYMBOL(
	StatusID int(11) NOT NULL,
	StatusSymbol varchar(20),
	PRIMARY KEY (StatusID)
)"
);

$sql_eClassIP_update[] = array(
"2009-09-10",
"Staff Smart Card Attendance System - Add init symbols for users to customize attendance status symbols",
"INSERT INTO CARD_STAFF_ATTENDANCE_STATUS_SYMBOL (StatusID) VALUES (0),(1),(2),(3),(4),(5),(6)"
);

$sql_eClassIP_update[] = array(
"2009-09-10",
"Student Smart Card Attendance - Add Table to store user customized attendance status symbols",
"CREATE TABLE IF NOT EXISTS CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL(
	StatusID int(11) NOT NULL,
	StatusSymbol varchar(20),
	PRIMARY KEY (StatusID)
)"
);

$sql_eClassIP_update[] = array(
"2009-09-10",
"Student Smart Card Attendance - Add init symbols for users to customize attendance status symbols",
"INSERT INTO CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL (StatusID) VALUES (0),(1),(2),(3),(4)"
);

$sql_eClassIP_update[] = array(
"2009-09-23",
"iMail - add Column DateInFolder ",
"alter TABLE INTRANET_CAMPUSMAIL add column DateInFolder datetime after DateModified"
);

$sql_eClassIP_update[] = array(
"2009-09-28",
"iCalendar add GroupType ",
"alter TABLE CALENDAR_CALENDAR_VIEWER add column `GroupType` char(1)"
);

$sql_eClassIP_update[] = array(
"2009-09-28",
"iCalendar add GroupID ",
"alter TABLE CALENDAR_CALENDAR_VIEWER add column `GroupID` int(8)"
);

$sql_eClassIP_update[] = array(
"2009-09-28",
"iCalendar add GroupPath ",
"alter TABLE CALENDAR_CALENDAR_VIEWER add column `GroupPath` varchar(255)"
);

$sql_eClassIP_update[] = array(
"2009-10-05",
"Staff Smart Card Attendance System - change Table of store user customized attendance status symbols to IP25's Engine and Charset",
"alter table CARD_STAFF_ATTENDANCE_STATUS_SYMBOL TYPE=innoDB"
);

$sql_eClassIP_update[] = array(
"2009-10-05",
"Staff Smart Card Attendance System - change Table of store user customized attendance status symbols to IP25's Engine and Charset",
"alter table CARD_STAFF_ATTENDANCE_STATUS_SYMBOL CONVERT TO CHARACTER SET utf8"
);

$sql_eClassIP_update[] = array(
"2009-10-05",
"Student Smart Card Attendance System - change Table of store user customized attendance status symbols to IP25's Engine and Charset",
"alter table CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL TYPE=innoDB"
);

$sql_eClassIP_update[] = array(
"2009-10-05",
"Student Smart Card Attendance System - change Table of store user customized attendance status symbols to IP25's Engine and Charset",
"alter table CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL CONVERT TO CHARACTER SET utf8"
);

$sql_eClassIP_update[] = array(
	"2009-10-05",
	"eDiscipline - set tag for AP items",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_ITEM_TAG_SETTING (
		TagID int(11) NOT NULL auto_increment,
		TagName varchar(255),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY (TagID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-10-05",
	"eDiscipline - store the tag of AP items",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_AP_ITEM_TAG (
		APTagID int(11) NOT NULL auto_increment,
		APItemID int(11),
		TagID int(11),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY (APTagID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-10-06",
	"StudentAttendance - add table for customize attend reason symbols",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_ATTENDANCE_REASON_SYMBOL (
	  ReasonType int(8) NOT NULL,
	  Reason varchar(255) NOT NULL,
	  StatusSymbol varchar(20),
	  CreateDate datetime,
	  LastModified datetime,
	  PRIMARY KEY  (ReasonType,Reason)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-10-09",
"eEnrolment - Add isAmountTBC in INTRANET_ENROL_GROUPINFO to cater club fee not confirmed improvement",
"Alter Table INTRANET_ENROL_GROUPINFO Add isAmountTBC tinyint(2) default 0"
);

$sql_eClassIP_update[] = array(
"2009-10-09",
"eEnrolment - Add isAmountTBC in INTRANET_ENROL_EVENTINFO to cater activity fee not confirmed improvement",
"Alter Table INTRANET_ENROL_EVENTINFO Add isAmountTBC tinyint(2) default 0"
);

$sql_eClassIP_update[] = array(
	"2009-10-13",
	"Payment - Add Invoice number for POS detail",
	"alter table PAYMENT_PURCHASE_DETAIL_RECORD add InvoiceNumber varchar(100) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Conduct adjustment - Add AcademicYearID ",
	"alter table DISCIPLINE_CONDUCT_ADJUSTMENT add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Conduct adjustment - Add YearTermID ",
	"alter table DISCIPLINE_CONDUCT_ADJUSTMENT add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Conduct balance  - Add AcademicYearID ",
	"alter table DISCIPLINE_STUDENT_CONDUCT_BALANCE add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Conduct balance - Add YearTermID ",
	"alter table DISCIPLINE_STUDENT_CONDUCT_BALANCE add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Subscore balance  - Add AcademicYearID ",
	"alter table DISCIPLINE_STUDENT_SUBSCORE_BALANCE add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Subscore balance - Add YearTermID ",
	"alter table DISCIPLINE_STUDENT_SUBSCORE_BALANCE add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > conduct change log  - Add AcademicYearID ",
	"alter table DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > conduct change log - Add YearTermID ",
	"alter table DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > subscore change log  - Add AcademicYearID ",
	"alter table DISCIPLINE_SUB_SCORE_CHANGE_LOG add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > subscore change log - Add YearTermID ",
	"alter table DISCIPLINE_SUB_SCORE_CHANGE_LOG add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > DISCIPLINE_SEMESTER_RATIO  - Add AcademicYearID ",
	"alter table DISCIPLINE_SEMESTER_RATIO add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > DISCIPLINE_SEMESTER_RATIO - Add YearTermID ",
	"alter table DISCIPLINE_SEMESTER_RATIO add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-20",
	"SMS - Add index to RecordStatus in INTRANET_SMS2_SOURCE_MESSAGE to speed up the sms status retrieval",
	"alter table INTRANET_SMS2_SOURCE_MESSAGE add index RecordStatus (RecordStatus);"
);

$sql_eClassIP_update[] = array(
	"2009-10-20",
	"SMS - Add index to ReferenceID in INTRANET_SMS2_MESSAGE_RECORD to speed up the sms status retrieval",
	"alter table INTRANET_SMS2_MESSAGE_RECORD add index ReferenceID (ReferenceID);"
);

$sql_eClassIP_update[] = array(
	"2009-10-20",
	"SMS - Add index to RelatedUserID in INTRANET_SMS2_MESSAGE_RECORD to speed up the sms status retrieval",
	"alter table INTRANET_SMS2_MESSAGE_RECORD add index RelatedUserID (RelatedUserID);"
);

$sql_eClassIP_update[] = array(
	"2009-10-29",
	"eDiscipline > AP record add SubjectID ",
	"alter table DISCIPLINE_MERIT_RECORD add SubjectID int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-30",
	"StudentAttendance - Add new column AbsentSession to record the number of absent sessions for each student",
	"ALTER TABLE CARD_STUDENT_PROFILE_RECORD_REASON ADD COLUMN AbsentSession int(11);"
);

$sql_eClassIP_update[] = array(
	"2009-11-09",
	"eNotice - add print date for reference",
	"ALTER TABLE INTRANET_NOTICE_REPLY ADD COLUMN PrintDate datetime default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-11-10",
	"eDiscipline - Change the Merit count from integer to float",
	"alter table DISCIPLINE_MERIT_RECORD change ProfileMeritCount ProfileMeritCount float(8,2)"
);

$sql_eClassIP_update[] = array(
	"2009-11-16",
	"eDiscipline - create table for UCCKE Conduct Grade [CRM Ref No.: 2009-0923-0926]",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_STUDENT_CONDUCT_GRADE (
		StudentID int(11) NOT NULL,
		Year varchar(100),
		AcademicYearID int(8),
		Semester varchar(100),
		YearTermID int(8),
		IsAnnual int(11),
		GradeChar varchar(100),
		INDEX StudentID (StudentID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

/*
$sql_eClassIP_update[] = array(
	"2009-11-16",
	"eDiscipline - create table for control the category can access by which access group",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_CATEGORY_GROUP_ACCESSRIGHT
	(
		RecordID int(11) NOT NULL auto_increment,
		CategoryType char(2) NOT NULL,
		CategoryID int(11),
		GroupID int(11),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
*/

$sql_eClassIP_update[] = array(
	"2009-11-18",
	"Add RankPattern Setting for eSports",
	"ALTER TABLE SPORTS_SYSTEM_SETTING ADD COLUMN RankPattern VARCHAR(4) Default '1223' AFTER DefaultLaneArrangement"
);

$sql_eClassIP_update[] = array(
	"2009-11-18",
	"Add HiddenAutoArrangeButton Setting for eSports > SportDay",
	"ALTER TABLE SPORTS_SYSTEM_SETTING ADD COLUMN HiddenAutoArrangeButton tinyint(1) Default 0 AFTER RankPattern"
);

$sql_eClassIP_update[] = array(
	"2009-11-18",
	"Add HiddenAutoArrangeButton Setting for eSports > SwimmingGala",
	"ALTER TABLE SWIMMINGGALA_SYSTEM_SETTING ADD COLUMN HiddenAutoArrangeButton tinyint(1) Default 0 AFTER DefaultLaneArrangement"
);

/*
CREATE TABLE IF NOT EXISTS DISCIPLINE_GROUP_ACCESS_GM_ITEM
(
	RecordID int(11) NOT NULL auto_increment,
	GroupID int(11),
	CategoryID int(11),
	ItemID int(11),
	PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
*/

$sql_eClassIP_update[] = array(
	"2009-11-26",
	"School Settings -> Class - Add Class Group Table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS_GROUP (
		ClassGroupID int(11) NOT NULL auto_increment,
		Code varchar(10),
		TitleEn varchar(255),
		TitleCh varchar(255),
		DisplayOrder int(11),
		RecordStatus int(11),
		DateInput datetime,
		DateModified datetime,
		LastModifiedBy int(11),
		PRIMARY KEY (ClassGroupID),
		INDEX Code (Code),
		INDEX DisplayOrder (DisplayOrder)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-11-26",
	"School Settings -> Class - Add Class Group Mapping in Class Table",
	"ALTER TABLE YEAR_CLASS ADD COLUMN ClassGroupID int(11) Default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-11-30",
	"iMail - RawMessage Table",
	"CREATE TABLE IF NOT EXISTS INTRANET_RAWCAMPUSMAIL (
	CampusMailID int(8) NOT NULL,
	UserID int(8) default NULL,
	HeaderFilePath mediumtext,
	MessageFilePath mediumtext,
	MessageEncoding varchar(60) default NULL,
	DateInput datetime default NULL,
	PRIMARY KEY (CampusMailID),
	INDEX UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-12-02",
	"iCalendar - Missing column 1 ",
	"ALTER TABLE CALENDAR_EVENT_USER ADD COLUMN `GroupType` char(1) default NULL "
);

$sql_eClassIP_update[] = array(
	"2009-12-02",
	"iCalendar - Missing column 2 ",
	"ALTER TABLE CALENDAR_REMINDER ADD COLUMN `ReminderBefore` int(8) default '0' "
);

$sql_eClassIP_update[] = array(
	"2009-12-03",
	"Discipline - Different Access Group can access different GM items",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_GROUP_ACCESS_GM
	(
		RecordID int(11) NOT NULL auto_increment,
		GroupID int(11),
		CategoryID int(11),
		ItemID int(11),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-12-07",
	"Discipline - Different Access Group can access different AP items",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_GROUP_ACCESS_AP
	(
		RecordID int(11) NOT NULL auto_increment,
		GroupID int(11),
		CategoryID int(11),
		ItemID int(11),
		ConductScore int(11),
		Subscore int(11),
		MeritCount float(8,2),
		MeritType int(11),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-12-10",
	"alter table SPORTS_EVENT change SpecialLaneArrangment SpecialLaneArrangement mediumtext;",
	"alter table SPORTS_EVENT change SpecialLaneArrangment SpecialLaneArrangement mediumtext;"
);

$sql_eClassIP_update[] = array(
	"2009-12-14",
	"School Settings -> Subject Group - Record the Source of the Subject Group for the copy subject group function",
	"ALTER TABLE SUBJECT_TERM_CLASS ADD COLUMN CopyFromSubjectGroupID int(8) Default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-12-14",
	"School Settings -> Timetable - Record the Source of the Timetable for the copy timetable function from copy subject group",
	"ALTER TABLE INTRANET_SCHOOL_TIMETABLE ADD COLUMN CopyFromTimetableID int(11) Default NULL After CycleDays"
);

$sql_eClassIP_update[] = array(
	"2009-12-14",
	"School Settings -> Timetable - Record the Source of the TimeSlot for the copy timetable function from copy subject group",
	"ALTER TABLE INTRANET_TIMETABLE_TIMESLOT ADD COLUMN CopyFromTimeSlotID int(11) Default NULL After EndTime"
);

$sql_eClassIP_update[] = array(
	"2009-12-17",
	"Munsang Conduct Grade - add columns AcademicYearID and YearTermID to table DISCIPLINE_MS_STUDENT_CONDUCT",
	"Alter Table DISCIPLINE_MS_STUDENT_CONDUCT
		Add Column AcademicYearID int(8) default NULL AFTER Year,
		Add Column YearTermID int(8) default NULL AFTER Semester;
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-17",
	"Munsang Conduct Grade - add columns AcademicYearID and YearTermID to table DISCIPLINE_MS_STUDENT_CONDUCT_SEMI",
	"Alter Table DISCIPLINE_MS_STUDENT_CONDUCT_SEMI
		Add Column AcademicYearID int(8) default NULL AFTER Year,
		Add Column YearTermID int(8) default NULL AFTER Semester;
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-17",
	"Munsang Conduct Grade - add columns AcademicYearID and YearTermID to table DISCIPLINE_MS_STUDENT_CONDUCT_FINAL",
	"Alter Table DISCIPLINE_MS_STUDENT_CONDUCT_FINAL
		Add Column AcademicYearID int(8) default NULL AFTER Year,
		Add Column YearTermID int(8) default NULL AFTER Semester;
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Conduct Mark - create new table DISCIPLINE_CONDUCT_MARK_CALCULATION_METHOD to store the conduct mark calculation method",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_CONDUCT_MARK_CALCULATION_METHOD 
	(
	 	MethodType int(11) default 0,
		RecordType int(11),
		RecordStatus int(11),
		InputBy int(11),
		DateInput datetime,
		ModifiedBy int(11),
		DateModified datetime
	)ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Detention - add column RecordType to DISCIPLINE_DETENTION_STUDENT_SESSION ",
	"ALTER TABLE DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN RecordType int(11) after AttendanceTakenDate"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Detention - add column RecordStatus to DISCIPLINE_DETENTION_STUDENT_SESSION ",
	"ALTER TABLE DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN RecordStatus int(11) DEFAULT 1 after RecordType"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Detention - add column ModifiedBy to DISCIPLINE_DETENTION_STUDENT_SESSION ",
	"Alter Table DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN ModifiedBy int(11) after RecordStatus"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Detention - add column RelatedTo to DISCIPLINE_DETENTION_STUDENT_SESSION ",
	"Alter Table DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN RelatedTo int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Conduct Mark - create new table DISCIPLINE_CONDUCT_MARK_CALCULATION_METHOD to store the conduct mark calculation method",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_CONDUCT_MARK_CALCULATION_METHOD 
	(
	 	MethodType int(11) default 0,
		RecordType int(11),
		RecordStatus int(11),
		InputBy int(11),
		DateInput datetime,
		ModifiedBy int(11),
		DateModified datetime
	)ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Misconduct- add a new column LateMinutes to DISCIPLINE_ACCU_CATEGORY ",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY ADD COLUMN LateMinutes int(4) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-01-04",
	"eDiscipline - Change the Merit count from integer to float",
	"alter table DISCIPLINE_MERIT_ITEM change NumOfMerit NumOfMerit float(8,2)"
);

$sql_eClassIP_update[] = array(
	"2010-01-05",
	"eDiscipline - Change the Merit count from integer to float",
	"alter table DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING change UpgradeDemeritCount UpgradeDemeritCount float(8,2)"
);

$sql_eClassIP_update[] = array
(
	"2010-01-05",
	"add Allow website setting to eCommunity (sharing)",
	"alter table INTRANET_GROUP add AllowedWebsite tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array(
	"2010-01-05",
	"Add Receipt Number counting for payment > print pages > receipt with flag sys_custom['PaymentReceiptNumber']",
	"CREATE TABLE IF NOT EXISTS PAYMENT_RECEIPT_COUNT (
	  AcademicYear varchar(15) NOT NULL,
	  ReceiptNumber int(11) default NULL,
	  PRIMARY KEY (AcademicYear)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-01-06",
	"add ActionDueDate in DISCIPLINE_MERIT_RECORD",
	"alter table DISCIPLINE_MERIT_RECORD add ActionDueDate date"
);

$sql_eClassIP_update[] = array
(
	"2010-01-07",
	"Copy Subject Group - Add a field to store the old course id before unlink the subject group and the eClass",
	"alter table SUBJECT_TERM_CLASS add old_course_id int(8) default NULL After course_id"
);

$sql_eClassIP_update[] = array
(
	"2010-01-14",
	" Add DisplayInCommunity to INTRANET_GROUP ",
	"alter table INTRANET_GROUP add DisplayInCommunity int(8) default 1 "
);


//----- CALENDAR_CALENDAR -----//
$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `Owner` int(8) NOT NULL default '0' "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `Name` varchar(255) NOT NULL default '' "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `Description` mediumtext "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `ShareToAll` char(1) NOT NULL default '0' "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `CalType` tinyint(1) NOT NULL default '0' "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `DefaultEventAccess` char(1) default NULL "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `CalSharedType` char(1) default NULL "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `SyncURL` varchar(200) default NULL "
);


$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `SyncURL` varchar(200) default NULL "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `CalID` int(8) NOT NULL auto_increment primary key"
);

//---- CALENDAR_CALENDAR_VIEWER ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `CalID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `GroupID` int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `GroupType` char(1) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `Access` char(3) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `Color` char(6) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `Visible` char(1) default '1'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `GroupPath` varchar(255) default NULL"
);

//---- CALENDAR_EVENT_ENTRY ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar remove dummy field  ",
	"alter table CALENDAR_EVENT_ENTRY drop column `ParentID`"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `EventID` int(8) NOT NULL auto_increment primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `RepeatID` int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `CalID` int(10) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `EventDate` datetime NOT NULL default '0000-00-00 00:00:00'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `InputDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `ModifiedDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Duration` int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `IsImportant` char(1) default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `IsAllDay` char(1) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Access` char(1) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Title` varchar(255) NOT NULL default ''"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Description` mediumtext"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Location` varchar(255) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Url` mediumtext"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `UID` varchar(200) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `ExtraIcalInfo` mediumtext"
);

//---- CALENDAR_CONFIG ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CONFIG add `Setting` varchar(50) NOT NULL default '' primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CONFIG add `Value` varchar(100) default NULL"
);

//---- CALENDAR_EVENT_ENTRY_REPEAT ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar remove dummy field  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT drop column `EventID`"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `RepeatID` int(8) NOT NULL auto_increment primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `RepeatType` varchar(20) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `EndDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `Frequency` int(2) default '1'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `Detail` varchar(7) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `ICalIsInfinite` char(1) default '0'"
);

//---- CALENDAR_EVENT_PERSONAL_NOTE ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `EventID` int(10) NOT NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `UserID` int(8) NOT NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `PersonalNote` mediumtext"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `CreateDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `LastUpdateDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `CalType` tinyint(4) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `PersonalNoteID` int(11) NOT NULL auto_increment primary key"
);

//---- CALENDAR_EVENT_USER ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `EventID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `GroupID` int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `GroupType` char(1) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `Status` char(1) default 'A'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `Access` char(1) default 'R'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `InviteStatus` tinyint(4) default NULL"
);

//---- CALENDAR_REMINDER ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `ReminderID` int(10) NOT NULL auto_increment primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `EventID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `ReminderType` char(1) NOT NULL default ''"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `ReminderDate` datetime NOT NULL default '0000-00-00 00:00:00'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `ReminderBefore` int(8) default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `LastSent` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `TimesSent` int(10) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `CalType` tinyint(4) default NULL'"
);

//---- CALENDAR_USER_PREF ------//


$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar remove dummy field  ",
	"alter table CALENDAR_USER_PREF drop primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_USER_PREF add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_USER_PREF add `Setting` varchar(50) NOT NULL default ''"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_USER_PREF add `Value` varchar(100) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-20",
	" eLibrary - add field ContentTLF in INTRANET_ELIB_BOOK to store whole book content in Text Layout Framework xml format ",
	"alter table INTRANET_ELIB_BOOK add `ContentTLF` mediumtext default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-01-20",
	"School Settings -> Timetable - Record the Source of the TimeSlot for the copy timetable function from copy subject group (alter table again to make sure the field is here)",
	"ALTER TABLE INTRANET_TIMETABLE_TIMESLOT ADD COLUMN CopyFromTimeSlotID int(11) Default NULL After EndTime"
);

$sql_eClassIP_update[] = array(
	"2009-01-22",
	"Disciplinev12 - Munsang College 'Conduct Grade' meeting absent list",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST (
		RecordID int(11) NOT NULL auto_increment,
		UserID int(8) NOT NULL DEFAULT 0,
		RecordType int(11),
		RecordStatus int(11),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY  (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-01-22",
	"eDiscipline - Change the Merit count from integer to float",
	"alter table DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE change ProfileMeritNum ProfileMeritNum float(8,2)"
);

$sql_eClassIP_update[] = array(
	"2010-01-25",
	"eNotice - add column DebitMethod in INTRANET_NOTICE for \"Payment Notice\"",
	"alter table INTRANET_NOTICE add column DebitMethod int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2010-01-25",
	"eDiscipline - add column ConversionPeriod in DISCIPLINE_MERIT_ITEM_CATEGORY for AP conversion improvement",
	"alter table DISCIPLINE_MERIT_ITEM_CATEGORY ADD COLUMN ConversionPeriod int(2) Default 0 after RecordStatus"
);

$sql_eClassIP_update[] = array(
	"2010-01-28",
	"eEnrolment - add column SchoolActivity to indicate the activity is 'inSchool' if value = 1 (by YatWoon)",
	"alter table INTRANET_ENROL_EVENTINFO ADD COLUMN SchoolActivity tinyint(1) Default 1"
);

$sql_eClassIP_update[] = array(
	"2010-02-01",
	"for eNotice-Payment enhancement, add NoticeID to PAYMENT_PAYMENT_ITEM",
	"alter table PAYMENT_PAYMENT_ITEM add NoticeID int(11) after DefaultAmount"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"for eNotice-Payment enhancement, add Unique key for ItemID and StudentID to PAYMENT_PAYMENT_ITEM",
	"alter table PAYMENT_PAYMENT_ITEMSTUDENT add unique key ItemIDStudentID (ItemID,StudentID)"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'AcademicYearID' to PROFILE_STUDENT_ATTENDANCE",
	"alter table PROFILE_STUDENT_ATTENDANCE add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'AcademicYearID' to PROFILE_STUDENT_ATTENDANCE",
	"alter table PROFILE_STUDENT_ATTENDANCE add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'AcademicYearID' to PROFILE_STUDENT_MERIT",
	"alter table PROFILE_STUDENT_MERIT add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'AcademicYearID' to PROFILE_STUDENT_MERIT",
	"alter table PROFILE_STUDENT_MERIT add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'isDeleted' to INTRANET_NOTICE - for payment notice enhancement",
	"ALTER TABLE INTRANET_NOTICE ADD Column isDeleted int(11) default 0"
);


$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolGroupID in INTRANET_USERGROUP for Term-based Club enhancement",
	"ALTER TABLE INTRANET_USERGROUP ADD COLUMN EnrolGroupID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Drop old unique key (GroupID, UserID) in INTRANET_USERGROUP for Term-based Club enhancement",
	"Alter Table INTRANET_USERGROUP Drop Index GroupID;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add new unique key (GroupID, UserID, EnrolGroupID) in INTRANET_USERGROUP for Term-based Club enhancement",
	"Alter Table INTRANET_USERGROUP Add Unique (GroupID, UserID, EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolGroupID in INTRANET_USERGROUP for Term-based Club enhancement",
	"ALTER TABLE INTRANET_USERGROUP Add INDEX EnrolGroupID (EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolGroupID in INTRANET_ENROL_GROUP_ATTENDANCE for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUP_ATTENDANCE ADD COLUMN EnrolGroupID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolGroupID in INTRANET_ENROL_GROUP_ATTENDANCE for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUP_ATTENDANCE Add INDEX EnrolGroupID (EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add Semester in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO ADD COLUMN Semester int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add MinQuota in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO ADD COLUMN MinQuota int(11) default 0;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index Semester in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO Add INDEX Semester (Semester);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Drop old unique key GroupID in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"Alter Table INTRANET_ENROL_GROUPINFO Drop Index GroupID;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add new unique key (GroupID, Semester) in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"Alter Table INTRANET_ENROL_GROUPINFO Add Unique (GroupID, Semester);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Change from EnrolGroupID to EnrolGroupStudentID as the primary key in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT CHANGE EnrolGroupID EnrolGroupStudentID INT(11) auto_increment;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolGroupID in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT ADD COLUMN EnrolGroupID INT(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolGroupID in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT Add INDEX EnrolGroupID (EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolSemester in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT ADD COLUMN EnrolSemester int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolSemester in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT Add INDEX EnrolSemester (EnrolSemester);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add Table INTRANET_ENROL_GROUPINFO_MAIN to record the Club main info for Term-based Club enhancement",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUPINFO_MAIN (
		GroupMainInfoID int(11) NOT NULL auto_increment,
		GroupID int(11) NOT NULL default '0',
		ClubType char(2) default NULL,
		ApplyOnceOnly tinyint(1) default NULL,
		FirstSemEnrolOnly tinyint(1) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (GroupMainInfoID),
		UNIQUE KEY GroupID (GroupID)
	 )"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add OLE_ProgramID in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"Alter table INTRANET_ENROL_GROUPINFO Add OLE_ProgramID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolGroupID in INTRANET_ENROL_EVENTINFO for Term-based Club enhancement",
	"Alter table INTRANET_ENROL_EVENTINFO Add EnrolGroupID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolGroupID in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT Add INDEX EnrolGroupID (EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolSemester in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT ADD COLUMN EnrolSemester int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolSemester in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT Add INDEX EnrolSemester (EnrolSemester);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add Table INTRANET_ENROL_GROUPINFO_MAIN to record the Club main info for Term-based Club enhancement",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUPINFO_MAIN (
		GroupMainInfoID int(11) NOT NULL auto_increment,
		GroupID int(11) NOT NULL default '0',
		ClubType char(2) default NULL,
		ApplyOnceOnly tinyint(1) default NULL,
		FirstSemEnrolOnly tinyint(1) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (GroupMainInfoID),
		UNIQUE KEY GroupID (GroupID)
	 )"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add OLE_ProgramID in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"Alter table INTRANET_ENROL_GROUPINFO Add OLE_ProgramID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"Campus Location - Add Barcode in INVENTORY_LOCATION_BUILDING for Inventory Offline Stocktake use",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING ADD COLUMN Barcode varchar(10) after Code, Add UNIQUE KEY BuildingIDWithBarcode (BuildingID,Barcode);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"Campus Location - Add Barcode in INVENTORY_LOCATION_LEVEL for Inventory Offline Stocktake use",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL ADD COLUMN Barcode varchar(10) after Code, Add UNIQUE KEY LocationLevelIDWithBarcode (LocationLevelID,Barcode);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"Campus Location - Add Barcode in INVENTORY_LOCATION for Inventory Offline Stocktake use",
	"ALTER TABLE INVENTORY_LOCATION ADD COLUMN Barcode varchar(10) after Code, Add UNIQUE KEY LocationIDWithBarcode (LocationLevelID,Barcode);"
);

$sql_eClassIP_update[] = array(
	"2010-02-25",
	"For Gamma Mail - for temporary mapping between mail and attachment",
	"CREATE TABLE IF NOT EXISTS MAIL_ATTACH_FILE_MAP (
	  FileID int(8) NOT NULL auto_increment,
	  UserID int(11) NOT NULL,
	  UploadTime datetime NOT NULL,
	  DraftMailUID int(8) default NULL,
	  OrgDraftMailUID int(8) default NULL,
	  OriginalFileName varchar(200) default NULL,
	  EncodeFileName int(8) default NULL,
	  PRIMARY KEY  (FileID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-02-25",
	"For Gamma Mail - trigger to synchronize FileID and EncodeFileName ",
	"CREATE TRIGGER UPDATE_ENCODE_FILE_NAME BEFORE INSERT ON MAIL_ATTACH_FILE_MAP
	  FOR EACH ROW 
	  BEGIN
		SET NEW.EncodeFileName = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE
		TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'MAIL_ATTACH_FILE_MAP');
	  END;"
);

$sql_eClassIP_update[] = array(
	"2010-02-26",
	"eDisciplinev12 - add LOCK column in table DISCIPLINE_MS_STUDENT_CONDUCT_FINAL for Munsang Conduct Grade Meeting",
	"ALTER TABLE DISCIPLINE_MS_STUDENT_CONDUCT_FINAL ADD COLUMN LockStatus tinyint(1) DEFAULT 0 NOT NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-26",
	"eDisciplinev12 - add YearID (form) column in table DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST for Munsang Conduct Grade Absent List",
	"ALTER TABLE DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST ADD COLUMN YearID int(8) DEFAULT 0 NOT NULL AFTER RecordID;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Gamma Mail  - Add ImapUserEmail to store email address for gamma mail",
	"ALTER TABLE INTRANET_USER ADD ImapUserEmail varchar(100) ;"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" eLibrary - change field ContentTLF in INTRANET_ELIB_BOOK from mediumtext to LONGBLOB ",
	"ALTER TABLE INTRANET_ELIB_BOOK CHANGE ContentTLF ContentTLF LONGBLOB DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" eLibrary - add field styleTLF in INTRANET_ELIB_BOOK to store the style sheet for Book content's TLF ",
	"alter table INTRANET_ELIB_BOOK add `StyleTLF` text default NULL"
);
/*	Disabled By Ronald - 20100419
$sql_eClassIP_update[] = array
(
	"2010-05-09",
	" iMail - Set Message Data type to mediumtext to store more characters",
	"ALTER TABLE INTRANET_CAMPUSMAIL MODIFY Message MEDIUMTEXT"
);
*/
$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" eEnrolment - Add field to store different status of attendance record",
	"ALTER TABLE INTRANET_ENROL_GROUP_ATTENDANCE ADD COLUMN RecordStatus int(11) DEFAULT 1"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" eEnrolment - Add field to store different status of attendance record",
	"ALTER TABLE INTRANET_ENROL_EVENT_ATTENDANCE ADD COLUMN RecordStatus int(11) DEFAULT 1"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" iMail Gamma - Add field AutoReplyTxt to store Auto Reply Text",
	"ALTER TABLE INTRANET_IMAIL_PREFERENCE ADD COLUMN AutoReplyTxt TEXT DEFAULT NULL AFTER SkipCheckEmail"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"For Gamma Mail - create table MAIL_SHARED_MAILBOX to store shared mail",
	"CREATE TABLE IF NOT EXISTS MAIL_SHARED_MAILBOX (
	  MailBoxID int(11) NOT NULL auto_increment,
	  MailBoxName varchar(255) NOT NULL,
	  MailBoxPassword varchar(255) NOT NULL,
	  MailBoxType varchar(8) default NULL,
	  CreateDate datetime default NULL,
	  CreateBy int(8) default NULL,
	  LastModified datetime default NULL,
	  ModifiedBy int(8) default NULL,
	  PRIMARY KEY  (MailBoxID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$sql_eClassIP_update[] = array(
	"2010-04-29",
	"For Gamma Mail - create table MAIL_SHARED_MAILBOX_MEMBER to store shared mail member",
	"CREATE TABLE IF NOT EXISTS MAIL_SHARED_MAILBOX_MEMBER (
	  MailBoxMemberID int(11) NOT NULL auto_increment,
	  MailBoxID varchar(255) NOT NULL,
	  UserID varchar(255) NOT NULL,
	  CreateDate datetime default NULL,
	  CreateBy int(8) default NULL,
	  LastModified datetime default NULL,
	  ModifiedBy int(8) default NULL,
	  PRIMARY KEY  (MailBoxMemberID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REQ20100102 - Student eAttendance enhancement to support Entry/ Leave date for student",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_ENTRY_LEAVE_PERIOD (
	  RecordID int(11) NOT NULL auto_increment,
	  UserID int(11) default NULL,
	  PeriodStart date NOT NULL default '1970-01-01',
	  PeriodEnd date NOT NULL default '2099-12-31',
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModify datetime default NULL,
	  ModifyBy int(11) default NULL,
	  PRIMARY KEY  (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"eEnrolment - set club default attendance record as present",
	"Alter Table INTRANET_ENROL_GROUP_ATTENDANCE Change RecordStatus RecordStatus int(11) default 1"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"eEnrolment - set activity default attendance record as present",
	"Alter Table INTRANET_ENROL_EVENT_ATTENDANCE Change RecordStatus RecordStatus int(11) default 1"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add option 'all fields are required' for eCircular",
	"ALTER Table INTRANET_CIRCULAR add AllFieldsReq tinyint(1);"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add option 'all fields are required' for eNotice",
	"ALTER Table INTRANET_NOTICE add AllFieldsReq tinyint(1);"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add 'Remark' in CARD_STUDENT_PROFILE_RECORD_REASON",
	"ALTER TABLE CARD_STUDENT_PROFILE_RECORD_REASON ADD Remark mediumtext AFTER Reason;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add 'HandinLetter' in CARD_STUDENT_PROFILE_RECORD_REASON",
	"ALTER TABLE CARD_STUDENT_PROFILE_RECORD_REASON ADD HandinLetter int(11) AFTER AbsentSession;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add 'HandinLetter' in CARD_STUDENT_PROFILE_RECORD_REASON",
	"Alter Table CARD_STUDENT_PROFILE_RECORD_REASON
		Add Column Last_Discipline_RecordDate datetime,
		Add Column GM_RecordID_Str TEXT;
	"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" iMail Gamma - Add field AutoSaveInterval to save auto save draft interval time",
	"ALTER TABLE INTRANET_IMAIL_PREFERENCE ADD COLUMN AutoSaveInterval int(3) DEFAULT 0 AFTER AutoReplyTxt"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"update field 'NumberOfUnit' in PROFILE_STUDENT_MERIT to float(8,2)",
	"ALTER TABLE PROFILE_STUDENT_MERIT change NumberOfUnit NumberOfUnit float(8,2)"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" iMail Gamma - Add field GmailMode to save gmail view preference",
	"ALTER TABLE INTRANET_IMAIL_PREFERENCE ADD COLUMN GmailMode int(3) DEFAULT 1 AFTER AutoSaveInterval"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"Add UNIQUE KEY for UserID and RecordDate",
	"alter IGNORE table CARD_STUDENT_OUTING add UNIQUE KEY UserIDAndRecordDate (UserID,RecordDate)"
);




$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - book license ",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_LICENSE (
	  BookLicenseID int(11) NOT NULL auto_increment,
	  BookID int(11) default NULL,
	  NumberOfCopy int(5) default 0,
	  CheckKey varchar(128) default NULL,
	  InputDate datetime default NULL,	  
	  PRIMARY KEY  (BookLicenseID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - book license enable/disable",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_ENABLED (	  
	  BookID int(11) default NULL,
	  UserID int(11) default NULL,	  
	  InputDate datetime default NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);


$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - book license for student",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_STUDENT (
	  BookStudentID int(11) NOT NULL auto_increment,
	  BookID int(11) default NULL,
	  BookLicenseID int(11) default NULL,
	  StudentID int(11) default NULL,
	  InputDate datetime default NULL,	  
	  PRIMARY KEY  (BookStudentID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Management Group",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_GROUP (
	  GroupID int(11) NOT NULL auto_increment,
	  GroupTitle varchar(128),
	  GroupDescription mediumtext,
	  RecordStatus tinyint(1) default 1,
	  DateInput datetime,
	  DateModified datetime,
	  LastModifiedBy int(11),
	  PRIMARY KEY (GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Management Group Member",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_GROUP_MEMBER (
	  GroupMemberID int(11) NOT NULL auto_increment,
	  GroupID int(11) default 0,
	  UserID int(11) default 0,
	  DateInput datetime,
	  PRIMARY KEY (GroupMemberID),
	  INDEX GroupID (GroupID),
	  INDEX UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"Drop UNIQUE KEY SBJ_CMP_CODEID (CODEID, CMP_CODEID) in ASSESSMENT_SUBJECT",
	"Alter Table ASSESSMENT_SUBJECT Drop Index SBJ_CMP_CODEID;"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"Add UNIQUE KEY SBJ_CMP_CODEID (CODEID, CMP_CODEID, RecordStatus) in ASSESSMENT_SUBJECT",
	"Alter Table ASSESSMENT_SUBJECT Add Unique Index SBJ_CMP_CODEID (CODEID, CMP_CODEID, RecordStatus);"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Location",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_LOCATION (
	  LocationID int(11) NOT NULL auto_increment,
	  LocationName varchar(255),
	  RecordStatus tinyint(1),
	  DateInput datetime,
	  DateModified datetime,
	  LastModifiedBy int(11),
	  PRIMARY KEY (LocationID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Category",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_CATEGORY (
	  CategoryID int(11) NOT NULL auto_increment,
	  Name varchar(255),
	  GroupID int(11) default 0,
	  RecordStatus tinyint(1),
	  DateInput datetime,
	  DateModified datetime,
	  LastModifiedBy int(11),
	  PRIMARY KEY (CategoryID),
	  INDEX GroupID (GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Records",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_RECORDS (
	  RecordID int(11) NOT NULL auto_increment,
	  CategoryID int(11),
	  LocationID int(11),
	  DetailsLocation text,
	  UserID int(11),
	  Title varchar(255),
	  Content text,
	  RecordStatus tinyint(1),
	  Remark text,
	  DateInput datetime,
	  DateModified datetime,
	  LastModifiedBy int(11),
	  PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REQ20100205 - Forgot to bring card bad action should appears one and only one per day",
	"alter ignore table CARD_STUDENT_BAD_ACTION add UNIQUE KEY UniqueKey (StudentID,RecordDate,RecordType)"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eDisciplinev12 - add column PICID in table DISCIPLINE_DETENTION_STUDENT_SESSION",
	"ALTER TABLE DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN PICID varchar(255) AFTER Reason"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - new chapter table structure for Text Layout Framework",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_CHAPTER_TLF (
	  BookChapterID int(11) NOT NULL auto_increment,
	  BookID int(11) default -1,
	  ChapterIndex int(11) default -1,
	  ShowNumber int(11) default -1,
	  StartPageIndex int(11) default -1,
	  EndPageIndex int(11) default -1,
	  Title varchar(255) default NULL,
	  ContentTLF longblob default NULL,
	  StyleTLF text default NULL,
	  DateModified datetime default NULL,	  
	  PRIMARY KEY  (BookChapterID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - add a new column AllowBooking in table INVENTORY_ITEM",
	"ALTER TABLE INVENTORY_ITEM ADD COLUMN AllowBooking int(11) default 1 AFTER PhotoLink"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - add a new column AllowBooking in table INVENTORY_LOCATION",
	"ALTER TABLE INVENTORY_LOCATION ADD COLUMN AllowBooking int(11) default 1 after Description"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD (
	  PeriodID int(11) NOT NULL auto_increment,
	  RecordDate date default NULL,
	  TimeSlotID int(11) default NULL,
	  StartTime time default NULL,
	  EndTime time default NULL,
	  RepeatType tinyint(4) default NULL,
	  RepeatValue text,
	  RelatedPeriodID int(11) default NULL,
	  RelatedItemID int(11) default NULL,
	  RelatedSubLocationID int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (PeriodID)
	) ENGINE=InnoDB AUTO_INCREMENT=426 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS (
	  RecordID int(11) NOT NULL auto_increment,
	  BookingID int(11) default NULL,
	  ItemID int(11) default NULL,
	  PIC int(8) default NULL,
	  ProcessDate date default NULL,
	  BookingStatus int(8) default 0,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RecordID)
	) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_FACILITIES_BOOKING_RULE",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_FACILITIES_BOOKING_RULE (
	  RuleID int(11) NOT NULL auto_increment,
	  DaysBeforeUse int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RuleID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_FOLLOWUP_GROUP",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_FOLLOWUP_GROUP (
	  GroupID int(11) NOT NULL auto_increment,
	  GroupName varchar(255) default NULL,
	  Description text,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER (
	  UserID int(11) default NULL,
	  GroupID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  UNIQUE KEY UserIDWithGroupID (UserID,GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ITEM",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ITEM (
	  ItemID int(11) NOT NULL auto_increment,
	  ItemType int(11) default NULL,
	  CategoryID int(11) default NULL,
	  Category2ID int(11) default NULL,
	  NameChi varchar(255) default NULL,
	  NameEng varchar(255) default NULL,
	  DescriptionChi mediumtext,
	  DescriptionEng mediumtext,
	  ItemCode varchar(100) default NULL,
	  Barcode varchar(100) default NULL,
	  LocationID int(11) default NULL,
	  BookingDayBeforehand tinyint(3) default NULL,
	  AllowBooking tinyint(3) default 1,
	  IncludedWhenBooking int(11) default 0,
	  ShowForReference int(11) default 0,
	  eInventoryItemID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (ItemID),
	  UNIQUE KEY ItemCode (ItemCode),
	  KEY ItemType (ItemType),
	  KEY CategoryID (CategoryID),
	  KEY Category2ID (Category2ID),
	  KEY NameChi (NameChi),
	  KEY NameEng (NameEng),
	  KEY LocationID (LocationID)
	) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP (
	  ItemFollowUpGroupID int(11) NOT NULL auto_increment,
	  ItemID int(11) default NULL,
	  GroupID int(11) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (ItemFollowUpGroupID),
	  UNIQUE KEY ItemGroupMapping (ItemID,GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP (
	  ItemManagementGroupID int(11) NOT NULL auto_increment,
	  ItemID int(11) default NULL,
	  GroupID int(11) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (ItemManagementGroupID),
	  UNIQUE KEY ItemGroupMapping (ItemID,GroupID)
	) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE (
	  ItemUserBookingRuleID int(11) NOT NULL auto_increment,
	  ItemID int(11) default NULL,
	  RuleID int(11) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (ItemUserBookingRuleID),
	  UNIQUE KEY ItemUserBookingRuleMapping (ItemID,RuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_BOOKING_RULE (
	  RuleID int(11) NOT NULL auto_increment,
	  DaysBeforeUse int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION (
	  LocationBookingRuleID int(11) NOT NULL auto_increment,
	  LocationID int(11) default NULL,
	  BookingRuleID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (LocationBookingRuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION (
	  LocationID int(11) default NULL,
	  BulkItemID int(11) default NULL,
	  IncludedWhenBooking int(11) default 0,
	  ShowForReference int(11) default 0,
	  UNIQUE KEY LocationWithBulkItem (LocationID,BulkItemID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION (
	  LocationGroupID int(11) NOT NULL auto_increment,
	  LocationID int(11) default NULL,
	  GroupID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (LocationGroupID)
	) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION (
	  LocationUserBookingRuleID int(11) NOT NULL auto_increment,
	  LocationID int(11) default NULL,
	  UserBookingRuleID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (LocationUserBookingRuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_MANAGEMENT_GROUP",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_MANAGEMENT_GROUP (
	  GroupID int(11) NOT NULL auto_increment,
	  GroupName varchar(255) default NULL,
	  Description text,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (GroupID)
	) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER (
	  UserID int(11) default NULL,
	  GroupID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  UNIQUE KEY UserIDWithGroupID (UserID,GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_RECORD",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_RECORD (
	  BookingID int(11) NOT NULL auto_increment,
	  BookingRef text,
	  PeriodID int(11) default NULL,
	  Date date default NULL,
	  StartTime time default NULL,
	  EndTime time default NULL,
	  RelatedTo int(11) default NULL,
	  Remark text,
	  RequestedBy int(8) default NULL,
	  RequestDate date default NULL,
	  ResponsibleUID int(8) default NULL,
	  PIC int(8) default NULL,
	  ProcessDate date default NULL,
	  BookingStatus int(8) default 0,
	  InputBy int(8) default NULL,
	  ModifiedBy int(8) default NULL,
	  RecordStatus int(11) default NULL,
	  RecordType int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (BookingID)
	) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ROOM_BOOKING_DETAILS (
	  RecordID int(11) NOT NULL auto_increment,
	  BookingID int(11) default NULL,
	  RoomID int(11) default NULL,
	  PIC int(8) default NULL,
	  ProcessDate date default NULL,
	  BookingStatus int(8) default 0,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RecordID)
	) ENGINE=InnoDB AUTO_INCREMENT=323 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_USER_BOOKING_RULE",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_USER_BOOKING_RULE (
	  RuleID int(11) NOT NULL auto_increment,
	  RuleName varchar(255) default NULL,
	  NeedApproval tinyint(4) default NULL,
	  DaysBeforeUse int(11) default NULL,
	  CanBookForOthers tinyint(4) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER (
	  RuleID int(11) default NULL,
	  YearID int(11) default NULL,
	  UserType int(11) default NULL,
	  IsTeaching int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - add colume IsShown in table INTRANET_ELIB_BOOK_CHAPTER_TLF",
	"ALTER TABLE INTRANET_ELIB_BOOK_CHAPTER_TLF ADD COLUMN IsShown char(1) DEFAULT '1' AFTER ShowNumber;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - new table structure for book style",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_STYLE (
	  BookStyleID int(11) NOT NULL auto_increment,
	  BookID int(11) default -1,
	  PageNum int(11) default 0,
	  BookX float(8,2) default 0,
	  BookY float(8,2) default 0,
	  BookWidth float(8,2) default 0,
	  BookHeight float(8,2) default 0,
	  PageX float(8,2) default 0,
	  PageY float(8,2) default 0,
	  PageWidth float(8,2) default 0,
	  PageHeight float(8,2) default 0,
	  DateModified datetime default NULL,	  
	  PRIMARY KEY (BookStyleID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - new table structure for page media such as image and movie",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_PAGEMEDIA (
	  BookPageMediaID int(11) NOT NULL auto_increment,
	  BookID int(11) default -1,
	  Page int(11) default -1,
	  MediaX float(8,2) default 0,
	  MediaY float(8,2) default 0,
	  MediaWidth float(8,2) default 0,
	  MediaHeight float(8,2) default 0,
	  FilePath text default NULL,	
	  DateModified datetime default NULL,	  
	  PRIMARY KEY (BookPageMediaID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array
(
	"2010-04-30",
	"update field 'Title' in INTRANET_ELIB_BOOK to TEXT to support longer title",
	"ALTER TABLE INTRANET_ELIB_BOOK change Title Title text Default NULL;"
);

$sql_eClassIP_update[] = array
(
	"2010-04-30",
	"eBooking - enlarge the BookingDayBeforehand value",
	"Alter Table INTRANET_EBOOKING_ITEM Change BookingDayBeforehand BookingDayBeforehand int(4);"
);

$sql_eClassIP_update[] = array
(
	"2010-05-10",
	"Discipline - add approved date to conduct records",
	"alter table DISCIPLINE_ACCU_RECORD add ApprovedDate datetime;"
);

$sql_eClassIP_update[] = array
(
	"2010-05-10",
	"Discipline - add approved by to conduct records",
	"alter table DISCIPLINE_ACCU_RECORD add ApprovedBy int(11);"
);

$sql_eClassIP_update[] = array
(
	"2010-05-10",
	"Discipline - add rejected date to conduct records",
	"alter table DISCIPLINE_ACCU_RECORD add RejectedDate datetime;"
);

$sql_eClassIP_update[] = array
(
	"2010-05-10",
	"Discipline - add rejected by to conduct records",
	"alter table DISCIPLINE_ACCU_RECORD add RejectedBy int(11);"
);

$sql_eClassIP_update[] = array
(
	"2010-05-11",
	"eBooking - add Column - DeleteOtherRelatedIfReject",
	"ALTER TABLE INTRANET_EBOOKING_RECORD ADD COLUMN DeleteOtherRelatedIfReject int(11) DEFAULT 0 AFTER BookingStatus;"
);


$sql_eClassIP_update[] = array
(
	"2010-05-14",
	" eLibrary - add field IsChapterTLF in INTRANET_ELIB_BOOK to flag any content in Text Layout Framework xml format ",
	"alter table INTRANET_ELIB_BOOK add `IsTLF` char(2) default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-05-17",
	" eLibrary - add field ChapterID in INTRANET_ELIB_USER_BOOKMARK to point every bookmark to corresponding chapter (ID, not index). ",
	"alter table INTRANET_ELIB_USER_BOOKMARK add `ChapterID` int(11) default -1 AFTER USERID, Add INDEX ChapterID (ChapterID)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-17",
	" eLibrary - add field PageIndex in INTRANET_ELIB_USER_BOOKMARK to point every bookmark to corresponding page reference to index (not id). ",
	"alter table INTRANET_ELIB_USER_BOOKMARK add `PageIndex` int(11) default -1 AFTER PageID, ADD INDEX PageIndex (PageIndex)"
);

$sql_eClassIP_update[] = array(
	"2010-05-17",
	"eLibrary - new table structure for user's free drawings on page",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_USER_DRAWING (
	  DrawingID int(11) NOT NULL auto_increment,
	  BookID int(11) default -1,
	  UserID int(11) default -1,
	  ChapterID int(11) default -1,
 	  PageIndex int(11) default -1,
	  FileName varchar(255) default NULL,		  
	  DateModified datetime default NULL,	  
	  PRIMARY KEY (DrawingID),
	  INDEX BookID (BookID),
	  INDEX UserID (UserID),
	  INDEX ChapterID (ChapterID),
	  INDEX PageIndex (PageIndex)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array
(
	"2010-05-17",
	" eLibrary - add KEY in INTRANET_ELIB_BOOK_CHAPTER_TLF. ",
	"alter table INTRANET_ELIB_BOOK_CHAPTER_TLF ADD INDEX BookID (BookID)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-18",
	"Alter INTRANET_ARCHIVE_USER -> ClassNumber field to integer",
	"alter table INTRANET_ARCHIVE_USER change ClassNumber ClassNumber int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-05-20",
	"CREATE TABLE INVENTORY_BARCODE_SETTING_LOG For log down any inventory barcode change",
	"CREATE TABLE IF NOT EXISTS INVENTORY_BARCODE_SETTING_LOG (
	  LogID int(11) NOT NULL auto_increment,
	  MaxLengthChanged int(11) default NULL,
	  FormatChanged int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column InternalUserID to INTRANET_USER",
	"alter table INTRANET_USER add column InternalUserID varchar(255)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column IDNumber to INTRANET_USER",
	"alter table INTRANET_USER add column IDNumber varchar(255)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column PlaceOfBirth to INTRANET_USER",
	"alter table INTRANET_USER add column PlaceOfBirth varchar(255)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column InputBy to INTRANET_USER",
	"alter table INTRANET_USER add column InputBy int(8)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column ModifyBy to INTRANET_USER",
	"alter table INTRANET_USER add column ModifyBy int(8)"
	
);

$sql_eClassIP_update[] = array
(
	"2010-05-26",
	"Add column CompleteDate to REPAIR_SYSTEM_RECORDS",
	"alter table REPAIR_SYSTEM_RECORDS add column CompleteDate datetime"
	
);

$sql_eClassIP_update[] = array(
"2010-06-02",
"Add RFID field to INTRANET_USER for RFID attendance taking",
"alter table INTRANET_USER add RFID varchar(20) default NULL after CardID"
);


$sql_eClassIP_update[] = array(
	"2010-06-04",
	"MODULE LICENSE `INTRANET_MODULE`, store all the module",
	"CREATE TABLE IF NOT EXISTS `INTRANET_MODULE` (
		`ModuleID` int(11) NOT NULL auto_increment,
		`Code` varchar(255) default NULL,
		`Description` text,
		`DateInput` timestamp NULL default NULL,
		`DateModified` datetime NOT NULL default '0000-00-00 00:00:00',
		PRIMARY KEY  (`ModuleID`),
		KEY `ModuleID` (`ModuleID`),
		UNIQUE KEY `Code` (`Code`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2010-06-04",
	"MODULE LICENSE `INTRANET_MODULE_LICENSE`, store all the module license",
	"CREATE TABLE IF NOT EXISTS `INTRANET_MODULE_LICENSE` (
		`ModuleLicenseID` int(11) NOT NULL auto_increment,
		`ModuleID` int(11) default NULL,
		`NumberOfLicense` int(5) default '0',
		`CheckKey` varchar(128) default NULL,
		`InputDate` datetime default NULL,
		PRIMARY KEY  (`ModuleLicenseID`),
		KEY `ModuleID` (`ModuleID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2010-06-04",
	"MODULE LICENSE `INTRANET_MODULE_USER`, store all the module license user",
	"CREATE TABLE IF NOT EXISTS `INTRANET_MODULE_USER` (
		`ModuleStudentID` int(11) NOT NULL auto_increment,
		`ModuleID` int(11) default NULL,
		`UserID` int(8) default NULL,
		`InputDate` datetime default NULL,
		PRIMARY KEY  (`ModuleStudentID`),
		KEY `ModuleID` (`ModuleID`),
		KEY `UserID` (`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-06-04",
	"CREATE A NEW TABLE - INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS For eBooking",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS (
		EventID int(10) NOT NULL auto_increment,
		BookingID int(11) NOT NULL,
		RepeatID int(8) default NULL,
		UserID int(8) NOT NULL default '0',
		CalID int(10) NOT NULL default '0',
		EventDate datetime NOT NULL default '0000-00-00 00:00:00',
		InputDate datetime default NULL,
		ModifiedDate datetime default NULL,
		Duration int(8) default NULL,
		IsImportant char(1) default '0',
		IsAllDay char(1) default NULL,
		Access char(1) default NULL,
		Title varchar(255) NOT NULL default '',
		Description mediumtext,
		Location varchar(255) default NULL,
		Url mediumtext,
		UID varchar(200) default NULL,
		ExtraIcalInfo mediumtext,
		PRIMARY KEY  (EventID),
		UNIQUE KEY UserID_2 (UserID,EventDate,InputDate,Title,CalID),
		KEY RepeatID (RepeatID),
		KEY UserID (UserID),
		KEY CalID (CalID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-06-04",
	"CREATE A NEW TABLE - INTRANET_EBOOKING_CALENDAR_EVENT_RELATION For eBooking",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_CALENDAR_EVENT_RELATION (
		BookingID int(11) default NULL,
		EventID int(10) default NULL,
		UNIQUE KEY BookingID_EventID (BookingID,EventID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
"2010-06-07",
"Add Attachment field to DISCIPLINE_ACCU_RECORD",
"alter table DISCIPLINE_ACCU_RECORD add Attachment varchar(255) AFTER YearTermID"
);

$sql_eClassIP_update[] = array(
"2010-06-07",
"Add Attachment field to DISCIPLINE_MERIT_RECORD",
"alter table DISCIPLINE_MERIT_RECORD add Attachment varchar(255) AFTER PICID"
);

$sql_eClassIP_update[] = array(
"2010-06-08",
"Add TemplateID field to DISCIPLINE_ACCU_CATEGORY",
"alter table DISCIPLINE_ACCU_CATEGORY add TemplateID int(11)"
);

$sql_eClassIP_update[] = array(
"2010-06-09",
"Add TemplateID field to DISCIPLINE_MERIT_ITEM_CATEGORY",
"alter table DISCIPLINE_MERIT_ITEM_CATEGORY add TemplateID int(11) AFTER ConversionPeriod"
);

$sql_eClassIP_update[] = array(
"2010-06-11",
"Add unique key DateStudentDayType to table CARD_STUDENT_PROFILE_RECORD_REASON (key should be exist since the table created, this script is to make sure all client consist have this key on that table - CRM Ref No.: 2010-0611-0856)",
"alter ignore table CARD_STUDENT_PROFILE_RECORD_REASON ADD UNIQUE KEY DateStudentDayType (RecordDate,StudentID,DayType,RecordType)"
);

$sql_eClassIP_update[] = array(
"2010-06-11",
"Add Gamma Quota to INTRANET_CAMPUSMAIL_USERQUOTA to store mail quota for gamma mail",
"ALTER TABLE INTRANET_CAMPUSMAIL_USERQUOTA ADD COLUMN GammaQuota int(11)"
);

$sql_ip_update[] = array(
	"2010-06-29",
	"Avoid duplicate record with same module code",
	"ALTER TABLE INTRANET_MODULE ADD UNIQUE (Code)"
);

	/*
if($plugin['ischoolbag'])
{
	
	$sql_ip_update[] = array(
		"2010-06-29",
		"INSERT INTRANET MODULE iSchoolBag",
		"INSERT INTO INTRANET_MODULE (Code,Description,DateInput,DateModified) VALUES ('isb','iSCHOOLBAG MODULE',now(),now())"
	);
}
*/
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_FAQ, store IES FAQ",
	"CREATE TABLE IF NOT EXISTS  `IES_FAQ` (
	  `QuestionID` int(11) NOT NULL auto_increment,
	  `Question` mediumtext,
	  `Answer` text,
	  `FromSchemeID` int(8) NOT NULL,
	  `AskedBy` int(11) NOT NULL,
	  `UserType` int(8) NOT NULL,
	  `AssignType` int(8) NOT NULL default '0',
	  `AssignToStudent` int(11) default NULL,
	  `RecordStatus` int(8) default NULL,
	  `AnswerDate` datetime default NULL,
	  `DateInput` datetime default NULL,
	  `InputBy` int(11) default NULL,
	  `DateModified` datetime default NULL,
	  `ModifyBy` int(11) default NULL,
	  PRIMARY KEY  (`QuestionID`),
	  KEY `QuestionID` (`QuestionID`),
	  KEY `AskedID` (`AskedBy`),
	  KEY `SchemeID` (`FromSchemeID`),
	  KEY `FromSchemeID` (`FromSchemeID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_FAQ_RELATIONSHIP, store IES FAQ RELATIONSHIP",
	"CREATE TABLE IF NOT EXISTS  `IES_FAQ_RELATIONSHIP` (
	  `RelationID` int(11) NOT NULL auto_increment,
	  `QuestionID` int(11) NOT NULL,
	  `SchemeID` int(8) NOT NULL,
	  PRIMARY KEY  (`RelationID`),
	  KEY `QuestionID` (`QuestionID`),
	  KEY `SchemeID` (`SchemeID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
		
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_FILE, store IES FILE",
	"CREATE TABLE IF NOT EXISTS  `IES_FILE` (
	  `FileID` int(11) NOT NULL auto_increment,
	  `FileName` varchar(255) default NULL,
	  `FolderPath` varchar(255) default NULL,
	  `FileHashName` varchar(255) default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`FileID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_MARKING, store IES MARKING",
	"CREATE TABLE IF NOT EXISTS  `IES_MARKING` (
	  `MarkingID` int(11) NOT NULL auto_increment,
	  `AssignmentID` int(11) default NULL,
	  `AssignmentType` tinyint(1) default NULL,
	  `MarkCriteria` tinyint(1) default NULL,
	  `MarkerID` int(8) default NULL,
	  `Score` float default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`MarkingID`),
	  UNIQUE KEY `MarkerMark` (`AssignmentID`,`AssignmentType`,`MarkCriteria`,`MarkerID`),
	  KEY `AssignmentID` (`AssignmentID`),
	  KEY `MarkerID` (`MarkerID`),
	  KEY `TargetAssignment` (`AssignmentID`,`AssignmentType`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_QUESTION_HANDIN, store IES QUESTION HANDIN ",
	"CREATE TABLE IF NOT EXISTS  `IES_QUESTION_HANDIN` (
  `AnswerID` int(11) NOT NULL auto_increment,
  `StepID` int(11) NOT NULL,
  `UserID` int(8) NOT NULL,
  `QuestionType` varchar(255) default NULL,
  `QuestionCode` varchar(255) default NULL,
  `Answer` longtext,
  `AnswerActualValue` longtext COMMENT 'store the actual text (HTML) for checkbox , radio button (optional)',
  `AnswerExtra` longtext COMMENT 'store the extra input value for checkbox , radio button (optional)',
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`AnswerID`),
  UNIQUE KEY `StepUserQuestion` (`StepID`,`UserID`,`QuestionCode`),
  KEY `StepID` (`StepID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_REFLECT_NOTE, store IES REFLECT NOTE ",
	"CREATE TABLE IF NOT EXISTS  `IES_REFLECT_NOTE` (
  `NoteID` int(11) NOT NULL auto_increment,
  `SchemeStudentID` int(11) NOT NULL,
  `Content` mediumtext,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`NoteID`),
  KEY `SchemeStudentID` (`SchemeStudentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_SCHEME, store IES SCHEME ",
	"CREATE TABLE IF NOT EXISTS  `IES_SCHEME` (
  `SchemeID` int(8) NOT NULL auto_increment,
  `Title` varchar(255) default NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`SchemeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_SCHEME_STUDENT, store IES SCHEME STUDENT ",
	"CREATE TABLE IF NOT EXISTS  `IES_SCHEME_STUDENT` (
  `SchemeStudentID` int(11) NOT NULL auto_increment,
  `SchemeID` int(8) NOT NULL,
  `UserID` int(8) NOT NULL,
  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `InputBy` int(8) default NULL,
  PRIMARY KEY  (`SchemeStudentID`),
  UNIQUE KEY `SchemeStudent` (`SchemeID`,`UserID`),
  KEY `SchemeID` (`SchemeID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_SCHEME_TEACHER, store IES SCHEME TEACHER ",
	"CREATE TABLE IF NOT EXISTS  `IES_SCHEME_TEACHER` (
  `SchemeTeacherID` int(11) NOT NULL auto_increment,
  `SchemeID` int(8) NOT NULL,
  `UserID` int(8) NOT NULL,
  `TeacherType` int(8) default NULL,
  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `InputBy` int(8) default NULL,
  PRIMARY KEY  (`SchemeTeacherID`),
  UNIQUE KEY `SchemeTeacher` (`SchemeID`,`UserID`,`TeacherType`),
  KEY `SchemeID` (`SchemeID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE, store IES STAGE ",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE` (
  `StageID` int(11) NOT NULL auto_increment,
  `SchemeID` int(8) NOT NULL,
  `Title` varchar(255) default NULL,
  `Description` text,
  `Sequence` int(8) default NULL,
  `Deadline` date default NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`StageID`),
  KEY `SchemeID` (`SchemeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
		$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE_HANDIN_BATCH, store IES STAGE HANDIN BATCH ",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE_HANDIN_BATCH` (
  `BatchID` int(11) NOT NULL auto_increment,
  `StageID` int(11) NOT NULL,
  `UserID` int(8) NOT NULL,
  `BatchStatus` tinyint(1) default NULL,
  `SubmissionDate` timestamp NULL default NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`BatchID`),
  KEY `StageID` (`StageID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE_HANDIN_COMMENT, store IES STAGE HANDIN COMMENT ",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE_HANDIN_COMMENT` (
  `BatchCommentID` int(11) NOT NULL auto_increment,
  `BatchID` int(11) NOT NULL,
  `Comment` longtext,
  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `InputBy` int(8) default NULL,
  PRIMARY KEY  (`BatchCommentID`),
  KEY `BatchID` (`BatchID`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8"
	);
	
		
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE_HANDIN_FILE, store IES STAGE HANDIN FILET",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE_HANDIN_FILE` (
  `FileID` int(11) NOT NULL auto_increment,
  `FileName` varchar(255) default NULL,
  `FolderPath` varchar(255) default NULL,
  `FileHashName` varchar(255) default NULL,
  `BatchID` int(11) NOT NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
		$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE_STUDENT, store IES STAGE STUDENT",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE_STUDENT` (
  `StageID` int(11) NOT NULL,
  `UserID` int(8) NOT NULL default '0',
  `Approval` tinyint(1) default NULL,
  `ApprovedBy` int(8) default NULL,
  `BatchHandinStatus` tinyint(1) default NULL,
  `Score` float default NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`StageID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STEP, store IES STEP",
	"CREATE TABLE IF NOT EXISTS  `IES_STEP` (
	  `StepID` int(11) NOT NULL auto_increment,
	  `TaskID` int(11) NOT NULL,
	  `Title` varchar(255) default NULL,
	  `StepNo` int(8) default NULL,
	  `Status` tinyint(1) default '1',
	  `Sequence` int(8) default NULL,
	  `SaveToTask` tinyint(1) default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`StepID`),
	  KEY `StepNo` (`StepNo`),
	  KEY `TaskID` (`TaskID`)
	  )ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK, store IES TASK",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK` (
	  `TaskID` int(11) NOT NULL auto_increment,
	  `StageID` int(11) NOT NULL,
	  `Title` varchar(255) default NULL,
	  `Code` varchar(255) default NULL,
	  `Sequence` int(8) default NULL,
	  `Approval` tinyint(1) default '0',
	  `Description` text,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`TaskID`),
	  UNIQUE KEY `StageTask` (`StageID`,`Code`),
	  KEY `StageID` (`StageID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_HANDIN, store IES_TASK_HANDIN",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_HANDIN` (
	  `AnswerID` int(11) NOT NULL auto_increment,
	  `TaskID` int(11) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Answer` longtext,
	  `QuestionType` varchar(255) default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`AnswerID`),
	  UNIQUE KEY `TaskUser` (`TaskID`,`UserID`),
	  KEY `TaskID` (`TaskID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_HANDIN_COMMENT, store IES TASK HANDIN COMMENT",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_HANDIN_COMMENT` (
	  `SnapshotCommentID` int(11) NOT NULL auto_increment,
	  `SnapshotAnswerID` int(11) NOT NULL,
	  `Comment` longtext,
	  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
	  `InputBy` int(8) default NULL,
	  PRIMARY KEY  (`SnapshotCommentID`)
	) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_HANDIN_SNAPSHOT, store IES_TASK_HANDIN_SNAPSHOT",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_HANDIN_SNAPSHOT` (
	  `SnapshotAnswerID` int(11) NOT NULL auto_increment,
	  `TaskID` int(11) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Answer` longtext,
	  `AnswerTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `DateInput` timestamp NOT NULL default '0000-00-00 00:00:00',
	  `InputBy` int(8) default NULL,
	  PRIMARY KEY  (`SnapshotAnswerID`),
	  UNIQUE KEY `TaskUserSnapshot` (`TaskID`,`UserID`,`AnswerTime`),
	  KEY `TaskID` (`TaskID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8"
	);
	
		$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_STUDENT, store IES TASK STUDENT",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_STUDENT` (
	  `TaskID` int(11) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Score` float default NULL,
	  `Approved` tinyint(1) default NULL,
	  `ApprovedBy` int(8) default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`TaskID`,`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_STUDENT_DETAILS, store IES TASK STUDENT DETAILS",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_STUDENT_DETAILS` (
	  `RecordID` int(11) NOT NULL auto_increment,
	  `TaskID` int(11) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Status` tinyint(1) default '0',
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`RecordID`),
	  UNIQUE KEY `TaskUser` (`TaskID`,`UserID`),
	  KEY `TaskID` (`TaskID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
		$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_WORKSHEET, store IES WORKSHEET",
	"CREATE TABLE IF NOT EXISTS  `IES_WORKSHEET` (
	  `WorksheetID` int(11) NOT NULL auto_increment,
	  `Title` varchar(255) default NULL,
	  `StartDate` datetime default NULL,
	  `EndDate` datetime default NULL,
	  `SchemeID` int(11) default NULL,
	  `RecordStatus` int(11) NOT NULL,
	  `DateInput` datetime default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` datetime default NULL,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`WorksheetID`),
	  KEY `SchemeID` (`SchemeID`),
	  KEY `RecordStatus` (`RecordStatus`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
			$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_WORKSHEET_HANDIN_COMMENT, store IES WORKSHEET HANDIN COMMENT",
	"CREATE TABLE IF NOT EXISTS  `IES_WORKSHEET_HANDIN_COMMENT` (
	  `CommentID` int(11) NOT NULL auto_increment,
	  `WorksheetID` int(11) NOT NULL,
	  `StudentID` int(8) default NULL,
	  `Comment` text,
	  `DateInput` datetime default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` datetime default NULL,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`CommentID`),
	  KEY `WorksheetID` (`WorksheetID`),
	  KEY `StudentID` (`StudentID`),
	  CONSTRAINT `IES_WORKSHEET_HANDIN_COMMENT_ibfk_1` FOREIGN KEY (`WorksheetID`) REFERENCES `IES_WORKSHEET` (`WorksheetID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_WORKSHEET_HANDIN_FILE, store IES_WORKSHEET_HANDIN_FILE",
	"CREATE TABLE IF NOT EXISTS  `IES_WORKSHEET_HANDIN_FILE` (
	  `WorksheetFileID` int(11) NOT NULL auto_increment,
	  `WorksheetID` int(11) NOT NULL,
	  `FileName` varchar(255) default NULL,
	  `FolderPath` varchar(255) default NULL,
	  `FileHashName` varchar(255) default NULL,
	  `DateInput` datetime default NULL,
	  `StudentID` int(8) default NULL,
	  `DateModified` datetime default NULL,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`WorksheetFileID`),
	  KEY `WorksheetID` (`WorksheetID`),
	  CONSTRAINT `IES_WORKSHEET_HANDIN_FILE_ibfk_1` FOREIGN KEY (`WorksheetID`) REFERENCES `IES_WORKSHEET` (`WorksheetID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_WORKSHEET_TEACHER_FILE, store IES_WORKSHEET_TEACHER_FILE",
	"CREATE TABLE IF NOT EXISTS  `IES_WORKSHEET_TEACHER_FILE` (
  `WorksheetFileID` int(11) NOT NULL auto_increment,
  `WorksheetID` int(11) NOT NULL,
  `FileName` varchar(255) default NULL,
  `FolderPath` varchar(255) default NULL,
  `FileHashName` varchar(255) default NULL,
  `DateInput` datetime default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` datetime default NULL,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`WorksheetFileID`),
  KEY `WorksheetID` (`WorksheetID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
		$sql_eClassIP_update[] = array(
	"2010-07-07",
	"IP file management",
	"CREATE TABLE IF NOT EXISTS  `IPORTAL_FILE` (
  `FileID` int(11) NOT NULL auto_increment,
  `FileName` varchar(255) default NULL,
  `FolderPath` varchar(255) default NULL,
  `FileHashName` varchar(255) default NULL,
  `UserID` int(8) NOT NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  `Size` int(11) default NULL,
  `SizeMax` int(64) default NULL,
  PRIMARY KEY  (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);

$sql_eClassIP_update[] = array(
	"2010-07-07",
	"iMail Gamma - Table for tracking internal mail receipt status",
	"CREATE TABLE IF NOT EXISTS INTRANET_IMAIL_RECEIPT_STATUS(
	 RecordID int(11) NOT NULL auto_increment,
	 MailID varchar(255) NOT NULL,
	 UserID int(11) NOT NULL,
	 Status int(11),
	 TrashDate datetime,
	 RecordType int(11),
	 RecordStatus int(11),
	 DateInput datetime,
	 DateModified datetime,
	 PRIMARY KEY (RecordID),
	 UNIQUE (MailID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-07-07",
	"Class History - Add YearClassID to PROFILE_CLASS_HISTORY",
	"ALTER TABLE PROFILE_CLASS_HISTORY ADD COLUMN YearClassID int(11) default Null"
);

$sql_eClassIP_update[] = array(
	"2010-07-07",
	"Class History - Add AcademicYearID to PROFILE_CLASS_HISTORY",
	"ALTER TABLE PROFILE_CLASS_HISTORY ADD COLUMN AcademicYearID int(11) default Null"
);

$sql_eClassIP_update[] = array(
	"2010-07-08",
	"Class History - Change the ClassName field from varchar(20) to varchar(255) to match with table YEAR_CLASS",
	"ALTER TABLE PROFILE_CLASS_HISTORY Change ClassName ClassName varchar(255) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-07-08",
	"Class History - Change the AcademicYear field from varchar(20) to varchar(50) to match with table ACADEMIC_YEAR",
	"ALTER TABLE PROFILE_CLASS_HISTORY Change AcademicYear AcademicYear varchar(50) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-07-08",
	"Class History - Change the ClassNumber field from varchar(20) to int(8) to match with table INTRANET_USER",
	"ALTER TABLE PROFILE_CLASS_HISTORY Change ClassNumber ClassNumber int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-07-08",
	"Class History - Set Unique Key of Student, Academic Year, Class and ClassNumber",
	"Alter Table PROFILE_CLASS_HISTORY Add Unique Key StudentYearClass (UserID, AcademicYearID, YearClassID, ClassNumber)"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Class History - Add Index to YearClassID of PROFILE_CLASS_HISTORY",
	"Alter Table PROFILE_CLASS_HISTORY Add Index YearClassID (YearClassID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Class History - Add Index to AcademicYearID of PROFILE_CLASS_HISTORY",
	"Alter Table PROFILE_CLASS_HISTORY Add Index AcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Add Day Type for CARD_STUDENT_OUTING",
	"alter table CARD_STUDENT_OUTING add DayType int(2) default NULL after RecordDate"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Drop CARD_STUDENT_OUTING unique",
	"alter table CARD_STUDENT_OUTING drop key UserIDAndRecordDate"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Add Unique Key to CARD_STUDENT_OUTING (UserID,RecordDate,DayType)",
	"alter table CARD_STUDENT_OUTING add UNIQUE KEY UserIDRecordDateDayType (UserID,RecordDate,DayType)"
);

$sql_eClassIP_update[] = array(
	"2010-07-13",
	"eEnrol - Add table to log the copy clubs history",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_COPY_CLUB_LOG (
		RecordID int(11) NOT NULL auto_increment,
		UserID int(11),
		FromAcademicYearID int(11),
		ToAcadedmicYearID int(11),
		FromTermID text,
		ToTermID text,
		FromEnrolGroupID int(11),
		ToEnrolGroupID int(11),
		CopyMember tinyint(3),
		CopyActivity tinyint(3),
		CopyDateTime datetime,
		PRIMARY KEY (RecordID),
		KEY UserID (UserID),
		KEY FromEnrolGroupID (FromEnrolGroupID),
		KEY ToEnrolGroupID (ToEnrolGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Add Academic Year field for activities",
	"Alter table INTRANET_ENROL_EVENTINFO Add AcademicYearID int(11) default Null"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Add Index AcademicYearID To INTRANET_ENROL_EVENTINFO",
	"Alter table INTRANET_ENROL_EVENTINFO Add Index AcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Add Index EnrolGroupID To INTRANET_ENROL_EVENTINFO",
	"Alter table INTRANET_ENROL_EVENTINFO Add Index EnrolGroupID (EnrolGroupID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Add Index OLE_ProgramID To INTRANET_ENROL_EVENTINFO",
	"Alter table INTRANET_ENROL_EVENTINFO Add Index OLE_ProgramID (OLE_ProgramID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Change the Semester field from varchar(255) to int(11) in INTRANET_ENROL_GROUPINFO",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO Change Semester Semester int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"iPortal File - Remove field 'SizeMax' in IPORTAL_FILE",
	"ALTER TABLE IPORTAL_FILE DROP COLUMN SIZEMAX"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"iPortal File - Add field 'Code' in IPORTAL_FILE",
	"ALTER TABLE IPORTAL_FILE ADD Code VARCHAR(255)"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"iPortal File - Add a table IPORTAL_FILE_MODULE",
	"CREATE TABLE IF NOT EXISTS `IPORTAL_FILE_MODULE` (
  `ModuleID` int(11) NOT NULL auto_increment,
  `Code` varchar(255) default NULL,
  `ModuleMaxSize` int(65) default NULL,
  `DateInput` timestamp NULL default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `Description` text,
  PRIMARY KEY  (`ModuleID`),
  UNIQUE KEY `Code` (`Code`),
  KEY `ModuleID` (`ModuleID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-07-15",
	"iPortal File - Add a new module",
	"INSERT INTO IPORTAL_FILE_MODULE SET CODE = 'ies', DATEINPUT = NOW(),DATEMODIFIED = NOW()"
);

$sql_eClassIP_update[] = array(
"2010-07-15",
"improve icalevent performance",
"alter table INTRANET_EVENT add INDEX RecordType (RecordType);"
);

$sql_eClassIP_update[] = array(
"2010-07-15",
"improve icalevent performance",
"alter table INTRANET_EVENT add INDEX RecordStatus (RecordStatus);"
);

$sql_eClassIP_update[] = array(
"2010-07-15",
"improve icalevent performance",
"alter table INTRANET_GROUPEVENT add INDEX EventID (EventID);"
);

$sql_eClassIP_update[] = array(
	"2010-07-16",
	"IES IES_COMMENT, store IES COMMENT BANK",
	"CREATE TABLE  IF NOT EXISTS `IES_COMMENT` (
	  `CommentID` int(11) NOT NULL auto_increment,
	  `Comment` mediumtext,
	  `DateInput` datetime default NULL,
	  `InputBy` int(11) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(11) default NULL,
	  PRIMARY KEY  (`CommentID`),
	  KEY `InputBy` (`InputBy`),
	  KEY `ModifyBy` (`ModifyBy`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2010-07-18",
"Student Registry System - create table for student info",
"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_STUDENT
(
	SRS_ID int(8) NOT NULL auto_increment,
	UserID int(8) NOT NULL,
	STUD_ID varchar(20) default NULL,
	CODE varchar(9) default NULL,
	NAME_P varchar(255) default NULL,
	ADMISSION_DATE date default NULL,
	S_CODE int(8) default NULL,
	B_PLACE int(8) default NULL,
	BIRTH_CERT_NO varchar(10) default NULL,
	ID_NO varchar(20) default NULL,
	ID_TYPE int(8) default NULL,
	I_PLACE int(8) default NULL,
	I_DATE date default NULL,
	V_DATE date default NULL,
	S6_TYPE tinyint(1) default NULL,
	S6_IDATE date default NULL,
	S6_VDATE date default NULL,
	RACE varchar(20) default NULL,
	ANCESTRAL_HOME varchar(20) default NULL,
	NATION int(8) default NULL,
	ORIGIN varchar(20) default NULL,
	RELIGION int(8) default NULL,
	PAYMENT_TYPE int(8) default NULL,
	FAMILY_LANG int(8) default NULL,
	LODGING int(8) default NULL,
	ADDRESS1 varchar(255) default NULL,
	ADDRESS2 varchar(255) default NULL,
	ADDRESS3 varchar(255) default NULL,
	ADDRESS4 varchar(255) default NULL,
	ADDRESS5 varchar(255) default NULL,
	ADDRESS6 varchar(255) default NULL,
	PRI_SCHOOL varchar(255) default NULL,
	R_AREA char(1) default NULL,
	RA_DESC varchar(255) default NULL,
	AREA char(1) default NULL,
	ROAD varchar(255) default NULL,
	ADDRESS varchar(255) default NULL,
	EMAIL varchar(255) default NULL,
	DateInput datetime default NULL,
	InputBy int(8) default NULL,
	DateModified date default NULL,
	ModifyBy int(8) default NULL,
	PRIMARY KEY  (SRS_ID),
	KEY UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2010-07-18",
"Student Registry System - create table for PG info",
"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_PG
(
	SRPG_ID int(8) NOT NULL auto_increment,
	StudentID int(8) NOT NULL,
	PG_TYPE char(1) NOT NULL,
	NAME_C varchar(255),
	NAME_E varchar(255),
	NAME_P varchar(255),
	G_GENDER char(1) NOT NULL,
	GUARD char(1) NOT NULL,
	G_RELATION varchar(255),
	PROF varchar(255),
	JOB_NATURE varchar(255),
	JOB_TITLE varchar(255),
	MARITAL_STATUS int(8) NOT NULL,
	TEL varchar(40),
	MOBILE varchar(40),
	OFFICE_TEL varchar(40),
	LIVE_SAME tinyint(1),
	G_AREA char(1),
	G_ROAD varchar(255),
	G_ADDRESS varchar(255),
	POSTAL_ADDR1 varchar(255),
	POSTAL_ADDR2 varchar(255),
	POSTAL_ADDR3 varchar(255),
	POSTAL_ADDR4 varchar(255),
	POSTAL_ADDR5 varchar(255),
	POSTAL_ADDR6 varchar(255),
	POST_CODE varchar(255),
	EMAIL varchar(255),
	EDU_LEVEL varchar(255),
	DateInput datetime default NULL,
	InputBy int(8) default NULL,
	DateModified date default NULL,
	ModifyBy int(8) default NULL,
	PRIMARY KEY  (SRPG_ID),
	KEY StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2010-07-29",
"Student Registry System - Record UserID of the user who modify the access right settings",
"ALTER TABLE ACCESS_RIGHT_GROUP_SETTING add LastModifiedBy int(11);"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Student Profile - Add index for UserID",
	"Alter Table PROFILE_STUDENT_ACTIVITY Add INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Subject Group - Add Field to store Subject Component Mapping to Subject Group",
	"Alter Table SUBJECT_TERM Add SubjectComponentID int(11) Default Null After SubjectID;"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Subject Group - Add Index to Subject Component Mapping to Subject Group",
	"Alter Table SUBJECT_TERM Add Index SubjectComponentID (SubjectComponentID);"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Subject Group - Drop Unique Key SubjectTerm (SubjectID, YearTermID, SubjectGroupID)",
	"Alter Table SUBJECT_TERM Drop Index SubjectTerm;"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Subject Group - Add Unique Key SubjectTerm (SubjectID, SubjectComponentID, YearTermID, SubjectGroupID)",
	"Alter Table SUBJECT_TERM Add Unique Index SubjectTerm (SubjectID, SubjectComponentID, YearTermID, SubjectGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-08-03",
	"Subject Group - Temp Import Table",
	"CREATE TABLE IF NOT EXISTS TEMP_SUBJECT_GROUP_IMPORT
	 (
			TempID int(11) NOT NULL auto_increment,
			UserID int(11),
			RowNumber int(11),
			SubjectCode varchar(20),
			SubjectGroupCode varchar(50),
			SubjectGroupTitleEn varchar(255),
			SubjectGroupTitleCh varchar(255),
			ApplicableForm varchar(255),
			CreateEclass varchar(10),
			DateInput datetime,
			PRIMARY KEY (TempID)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-03",
	"Subject Group - Add Subject Component Code in the Temp Import Table",
	"Alter Table TEMP_SUBJECT_GROUP_IMPORT Add SubjectComponentCode varchar(20) Default Null After SubjectCode;"
);

$sql_eClassIP_update[] = array(
	"2010-08-03",
	"Intranet Module - Add a field UsageStatus to the table INTRANET_MODULE_USER",
	"ALTER TABLE INTRANET_MODULE_USER ADD UsageStatus int(1) default 0;"
);

$sql_eClassIP_update[] = array(
	"2010-08-03",
	"INSERT INTRANET MODULE IES",
	"INSERT IGNORE INTO INTRANET_MODULE (Code,Description,DateInput,DateModified) VALUES ('ies','IES MODULE',now(),now())"
);


$sql_eClassIP_update[] = array(
	"2010-08-04",
	"Add resize setting to eCommunity photo folder",
	"alter table INTRANET_FILE_FOLDER add PhotoResize tinyint(1) default 0"
);

$sql_eClassIP_update[] = array(
	"2010-08-04",
	"Add resize setting (x-demension) to eCommunity photo folder",
	"alter table INTRANET_FILE_FOLDER add DemensionX int(1);"
);

$sql_eClassIP_update[] = array(
	"2010-08-04",
	"Add resize setting (y-demension) to eCommunity photo folder",
	"alter table INTRANET_FILE_FOLDER add DemensionY int(1);"
);

$sql_eClassIP_update[] = array(
	"2010-08-04",
	"Student Registry Status (Malaysia)",
	"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_STATUS
	 (
		RecordID int(8) NOT NULL auto_increment,
		UserID int(8) NOT NULL,
		ApplyDate date,
		Reason int(4) NOT NULL,
		ReasonOthers varchar(100),
		NoticeID int(11),
		RecordStatus int(1) NOT NULL,
		IsCurrent int(1) NOT NULL default 0,
		DateInput datetime,
		InputBy int(11),
		DateModified datetime,
		ModifyBy int(11),
		PRIMARY KEY (RecordID),
		INDEX UserID (UserID),
		INDEX RecordStatus (RecordStatus),
		INDEX IsCurrent (IsCurrent)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-09",
	"eEnrol - Add CopyFromEnrolGroupID in INTRANET_ENROL_GROUPINFO",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO Add CopyFromEnrolGroupID int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-08-09",
	"Group - Add CopyFromGroupID in INTRANET_GROUP",
	"ALTER TABLE INTRANET_GROUP Add CopyFromGroupID int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-08-10",
	"IES - Add versioning for scheme",
	"ALTER TABLE IES_SCHEME ADD Version BIGINT(13) DEFAULT NULL;"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	" eLibrary - add field HeaderLeft in INTRANET_ELIB_BOOK_STYLE to store left page's header in TLF format",
	"alter table INTRANET_ELIB_BOOK_STYLE add `HeaderLeft` text default NULL after PageHeight"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	" eLibrary - add field HeaderRight in INTRANET_ELIB_BOOK_STYLE to store right page's header in TLF format",
	"alter table INTRANET_ELIB_BOOK_STYLE add `HeaderRight` text default NULL after HeaderLeft"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	" eLibrary - add field FooterLeft in INTRANET_ELIB_BOOK_STYLE to store left page's footer in TLF format",
	"alter table INTRANET_ELIB_BOOK_STYLE add `FooterLeft` text default NULL after HeaderRight"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	" eLibrary - add field FooterRight in INTRANET_ELIB_BOOK_STYLE to store right page's footer in TLF format",
	"alter table INTRANET_ELIB_BOOK_STYLE add `FooterRight` text default NULL after FooterLeft"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	"student reg - update field from code table",
	"alter table STUDENT_REGISTRY_STUDENT change ID_TYPE ID_TYPE varchar(5) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	"student reg - update field from code table",
	"alter table STUDENT_REGISTRY_STUDENT change NATION NATION varchar(2) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	"student reg - update field from code table",
	"alter table STUDENT_REGISTRY_STUDENT change S_CODE S_CODE varchar(3) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-08-11",
	" eLibrary - add field PageOneIndex in INTRANET_ELIB_BOOK_STYLE to store the page index of real Page No.1",
	"alter table INTRANET_ELIB_BOOK_STYLE add `PageOneIndex` int(6) default 1 after FooterRight"
);

$sql_eClassIP_update[] = array(
	"2010-08-13",
	"School Settings > Timetable - Import Lesson Temp Table",
	"CREATE TABLE IF NOT EXISTS TEMP_TIMETABLE_LESSON_IMPORT
	 (
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		TimetableID int(11),
		RowNumber int(11),
		Day tinyint(3),
		TimeSlot tinyint(3),
		TimeSlotID int(11),
		SubjectGroupCode varchar(50),
		SubjectGroupID int(11),
		BuildingCode varchar(10),
		FloorCode varchar(10),
		RoomCode varchar(10),
		LocationID int(11),
		OthersLocation varchar(255),
		DateInput datetime,
		PRIMARY KEY (TempID)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-13",
	"School Settings > Timetable - Import Lesson Temp Table",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP (
	     RoomAllocationID int(11) NOT NULL auto_increment,
	     TimetableID int(11) NOT NULL,
	     TimeSlotID int(11) NOT NULL,
	     Day tinyint(2) NOT NULL,
	     LocationID int(11) default NULL,
	     OthersLocation varchar(255) default NULL,
	     SubjectGroupID int(8) NOT NULL,
		 ImportUserID int(11) NOT NULL,
	     DateInput datetime default NULL,
	     DateModified datetime default NULL,
	     LastModifiedBy int(11) default NULL,
	     PRIMARY KEY (RoomAllocationID),
	     KEY TimetableID (TimetableID),
	     KEY TimeSlotID (TimeSlotID),
	     KEY Day (Day),
	     KEY LocationID (LocationID),
	     KEY SubjectGroupID (SubjectGroupID),
		 KEY ImportUserID (ImportUserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-08-17",
	"Student Registry System - add 1 more column to store 'Brother & Sister in school'",
	"alter table STUDENT_REGISTRY_STUDENT add column BRO_SIS_IN_SCHOOL varchar(255) AFTER ADDRESS6"
);

$sql_eClassIP_update[] = array
(
	"2010-08-18",
	" eLibrary - add field ChapterID in INTRANET_ELIB_USER_FORMAT to store the Chapter ID",
	"alter table INTRANET_ELIB_USER_FORMAT add `ChapterID` int(11) default -1 after UserID, Add INDEX ChapterID (ChapterID)"
);

$sql_eClassIP_update[] = array
(
	"2010-08-19",
	"iMail Gamma - add table for storing temporary mail inline images when composing mail",
	"CREATE TABLE IF NOT EXISTS MAIL_INLINE_IMAGE_MAP(
	 	FileID int(8) NOT NULL auto_increment,
	 	UserID int(11) NOT NULL,
	 	UploadTime datetime NOT NULL,
	 	OriginalImageFileName varchar(255) DEFAULT NULL,
	 	ContentID varchar(100) DEFAULT NULL,
	 	PRIMARY KEY (FileID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-08-19",
	"eEnrol - add field to store Active Member Percentage in Club",
	"alter table INTRANET_ENROL_GROUPINFO add ActiveMemberPercentage float(5,2) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-08-19",
	"eDiscipline - FollowUp Action [customization]",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_FOLLOWUP_ACTION (
	  ActionID int(8) NOT NULL auto_increment,
	  Title varchar(255) default NULL,
	  Sequence int(2) default NULL,
	  RecordStatus int(1) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (ActionID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-20",
	"eDisciplinev12 - add followup action to conduct settings",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_ITEM ADD ActionID int(11);"
);

$sql_eClassIP_update[] = array(
	"2010-08-20",
	"eDisciplinev12 - add followup action data to GM record",
	"alter table DISCIPLINE_ACCU_RECORD add ActionID int(11), add ActionTimes int(11), add ActionCompleted tinyint(1);"
);

$sql_eClassIP_update[] = array(
	"2010-08-23",
	"eDisciplinev12 - add last followup date to GM record",
	"alter table DISCIPLINE_ACCU_RECORD add LastFollowUpDate datetime, add LastFollowUpBy int(11);"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eBooking - add column GroupColor to INTRANET_EBOOKING_FOLLOWUP_GROUP",
	"ALTER TABLE INTRANET_EBOOKING_FOLLOWUP_GROUP ADD Column GroupColor text AFTER Description;"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eBooking - add column IsReserve to INTRANET_EBOOKING_RECORD",
	"ALTER TABLE INTRANET_EBOOKING_RECORD ADD Column IsReserve int(8) AFTER DeleteOtherRelatedIfReject;"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eBooking - create table INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION (
		LocationFollowUpGroupID int(11) NOT NULL auto_increment,
  		LocationID int(11) default NULL,
  		GroupID int(11) default NULL,
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY  (LocationFollowUpGroupID),
		UNIQUE KEY LocationGroupMapping (LocationID,GroupID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eSports (Sport Day) - Add AbsentReason for Absent - Waived ",
	"ALTER TABLE SPORTS_LANE_ARRANGEMENT ADD COLUMN AbsentReason VARCHAR(128) DEFAULT NULL AFTER Rank"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eSports (Swimminggala)- Add AbsentReason for Absent - Waived ",
	"ALTER TABLE SWIMMINGGALA_LANE_ARRANGEMENT ADD COLUMN AbsentReason VARCHAR(128) DEFAULT NULL AFTER Rank"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"eDisciplinev12 - Munsang Generate reference number when creating AP/GM records",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_REFERENCE_NUMBER (
	ReferenceID INT(11) NOT NULL auto_increment,
	AcademicYearID INT(11) not null,
	YearTermID INT(11) not null,
	ReferenceToTable varchar(200),
	RecordID INT(11),
	DateInput DATETIME,
	InputBy int(11),
	PRIMARY KEY (ReferenceID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Batch Edit Subject Group Info Temp Table",
	"CREATE TABLE IF NOT EXISTS TEMP_SUBJECT_GROUP_QUICK_EDIT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		SubjectGroupCode varchar(50),
		NewSubjectGroupCode varchar(50),
		NewTitleEn varchar(255),
		NewTitleCh varchar(255),
		SubjectGroupID int(11),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Add Form Info in the Batch Edit Subject Group Info Temp Table",
	"ALTER TABLE TEMP_SUBJECT_GROUP_QUICK_EDIT ADD COLUMN ApplicableFormList varchar(255) Default Null After SubjectGroupID"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Add Form Info in the Batch Edit Subject Group Info Temp Table",
	"ALTER TABLE TEMP_SUBJECT_GROUP_QUICK_EDIT ADD COLUMN ApplicableFormIDList varchar(255) Default Null After ApplicableFormList"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Add Subject Group Teacher Info in the Temp Import Table",
	"Alter Table TEMP_SUBJECT_GROUP_IMPORT Add TeacherList varchar(255) Default Null After CreateEclass;"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Add Subject Group Teacher Info in the Temp Import Table",
	"Alter Table TEMP_SUBJECT_GROUP_IMPORT Add TeacherIDList varchar(255) Default Null After TeacherList;"
);

$sql_eClassIP_update[] = array(
	"2010-08-31",
	"eDisciplinev12 - Munsang Generate reference number when creating AP/GM records (Add column)",
	"ALTER TABLE DISCIPLINE_REFERENCE_NUMBER ADD COLUMN ReferenceNo int(11) not null AFTER YearTermID"
);

$sql_eClassIP_update[] = array(
	"2010-08-31",
	"Staff attendance v2 - add session time field to admin table",
	"ALTER TABLE CARD_STAFF_ATTENDANCE_ADMIN ADD COLUMN Type tinyint default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-08-31",
	"Staff attendance v2 - add session time field to admin table",
	"UPDATE CARD_STAFF_ATTENDANCE_ADMIN SET Type=0 WHERE Username = 'admin'"
);

$sql_eClassIP_update[] = array(
	"2010-09-03",
	"add a field to store user's latest lang selection for IP",
	"ALTER TABLE INTRANET_USER ADD LastLang varchar(2)"
);


$sql_eClassIP_update[] = array(
	"2010-09-06",
	"Add DISCIPLINE_AP_APPROVAL_GROUP for AP Approval Group",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_AP_APPROVAL_GROUP
	(
		GroupID INT(11) not null auto_increment,
		Name varchar(200),
		Description text,
		RecordStatus int(11) default 1,
		DateInput datetime, 
		InputBy int(11),
		DateModified datetime,
		ModifyBy int(11),
		PRIMARY KEY (GroupID),
		INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-09-06",
	"Add DISCIPLINE_AP_APPROVAL_GROUP_MEMBER for AP Approval Group Member",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_AP_APPROVAL_GROUP_MEMBER
	(
		GroupMemberID INT(11) not null auto_increment,
		GroupID int(11) not null,
		UserID int(11) not null,
		DateInput datetime,
		PRIMARY KEY (GroupMemberID),
		INDEX GroupID (GroupID),
		INDEX UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY",
	"CREATE TABLE IF NOT EXISTS `IES_SURVEY` (
	  `SurveyID` int(11) NOT NULL auto_increment,
	  `Title` varchar(255) NOT NULL,
	  `Description` mediumtext,
	  `Question` mediumtext,
	  `DateStart` datetime default NULL,
	  `DateEnd` datetime default NULL,
	  `SurveyType` tinyint(4) default NULL,
	  `SurveyStatus` tinyint(4) default NULL,
	  `InputBy` int(11) NOT NULL,
	  `DateInput` timestamp NULL default NULL,
	  `ModifiedBy` int(11) NOT NULL,
	  `DateModified` timestamp NULL default NULL,
	  PRIMARY KEY  (`SurveyID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY_ANSWER",
	"CREATE TABLE IF NOT EXISTS `IES_SURVEY_ANSWER` (
	  `AnswerID` int(11) NOT NULL auto_increment,
	  `SurveyID` int(11) NOT NULL,
	  `Respondent` varchar(255) default NULL,
	  `Answer` mediumtext,
	  `DateInput` datetime default NULL,
	  `DateModified` datetime default NULL,
	  `Type` int(11) NOT NULL,
	  `status` int(11) default NULL,
	  PRIMARY KEY  (`AnswerID`),
	  UNIQUE KEY `AnswerUser` (`SurveyID`,`Respondent`),
	  KEY `SurveyID` (`SurveyID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY_EMAIL",
	"CREATE TABLE IF NOT EXISTS `IES_SURVEY_EMAIL` (
		  `SurveyEmailID` int(11) NOT NULL auto_increment,
		  `SurveyID` int(11) NOT NULL,
		  `DateInput` timestamp NULL default NULL,
		  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		  `Email` varchar(255) default NULL,
		  `Status` int(11) default NULL,
		  `EmailDisplayName` mediumtext,
		  PRIMARY KEY  (`SurveyEmailID`),
		  UNIQUE KEY `EmailUser` (`SurveyID`,`Email`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY_EMAILCONTENT",
	"CREATE TABLE IF NOT EXISTS `IES_SURVEY_EMAILCONTENT` (
	  `SurveyEmailCONTENTID` int(11) NOT NULL auto_increment,
	  `SurveyID` int(11) NOT NULL,
	  `Title` mediumtext,
	  `Content` longblob,
	  `DateInput` timestamp NULL default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  PRIMARY KEY  (`SurveyEmailCONTENTID`),
	  UNIQUE KEY `SurveyID` (`SurveyID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY_MAPPING",
			"CREATE TABLE IF NOT EXISTS `IES_SURVEY_MAPPING` (
		  `SurveyMappingID` int(11) NOT NULL auto_increment,
		  `SurveyID` int(11) NOT NULL,
		  `UserID` int(8) NOT NULL,
		  `XMAPPING` char(11) default NULL,
		  `YMAPPING` char(11) default NULL,
		  `XTitle` varchar(255) default NULL,
		  `YTitle` varchar(255) default NULL,
		  `DateInput` timestamp NULL default NULL,
		  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		  `MappingTitle` varchar(255) NOT NULL,
		  PRIMARY KEY  (`SurveyMappingID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : Add field InstantEdit to IES_TASK",
	"ALTER TABLE IES_TASK ADD COLUMN `InstantEdit` tinyint(1)  default 1 AFTER Description;"	
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : Add field Status to IES_TASK_HANDIN_COMMENT",
	"ALTER TABLE IES_TASK_HANDIN_COMMENT ADD COLUMN `Status` int(8) default '0' AFTER InputBy;"	
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : Add field QuestionType to IES_TASK_HANDIN_SNAPSHOT",
	"ALTER TABLE IES_TASK_HANDIN_SNAPSHOT ADD COLUMN `QuestionType` varchar(255) default NULL AFTER Answer;"	
);
/*
$sql_eClassIP_update[] = array(
	"2010-09-09",
	"eDisciplinev12 - Add ApprovalGroupID in DISCIPLINE_MERIT_ITEM",
	"ALTER TABLE DISCIPLINE_MERIT_ITEM ADD COLUMN ApprovalGroupID INT(11) NOT NULL AFTER DetentionMinutes, ADD INDEX ApprovalGroupID (ApprovalGroupID)"	
);
*/
$sql_eClassIP_update[] = array(
	"2010-09-10",
	"Student Attendance : Change session data type from int(11) to float",
	"ALTER TABLE CARD_STUDENT_STATUS_SESSION_COUNT MODIFY LateSession FLOAT DEFAULT NULL,MODIFY RequestLeaveSession FLOAT DEFAULT NULL,MODIFY PlayTruantSession FLOAT DEFAULT NULL,MODIFY OfficalLeaveSession FLOAT DEFAULT NULL "
);

$sql_eClassIP_update[] = array(
	"2010-09-10",
	"eDisciplinev12 - add ApprovalGroupID in DISCIPLINE_MERIT_TYPE_SETTING",
	"ALTER TABLE DISCIPLINE_MERIT_TYPE_SETTING ADD COLUMN ApprovalGroupID INT(11) NOT NULL, ADD INDEX ApprovalGroupID (ApprovalGroupID)"
);

$sql_eClassIP_update[] = array(
	"2010-09-10",
	"INTRANET_USER Personal Photo Link",
	"ALTER TABLE INTRANET_USER ADD PersonalPhotoLink varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-09-14",
	"IES : Change of default approval status",
	"ALTER TABLE IES_TASK MODIFY Approval tinyint(1) DEFAULT 1"	
);

$sql_eClassIP_update[] = array(
	"2010-09-15",
	"Import Class Student - Temp csv info Table",
	"CREATE TABLE IF NOT EXISTS TEMP_CLASS_STUDENT_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		UserLogin varchar(100),
		ClassNameEn varchar(100),
		ClassNumber varchar(100),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"	
);

$sql_eClassIP_update[] = array(
	"2010-09-15",
	"Import Class Student - Add ID fields to the temp table",
	"ALTER TABLE TEMP_CLASS_STUDENT_IMPORT Add StudentID int(11) After ClassNumber, Add YearClassID int(11) After StudentID"
);

$sql_eClassIP_update[] = array(
	"2010-09-16",
	"Import Class Student - Temp import class result Table",
	"CREATE TABLE IF NOT EXISTS TEMP_CLASS_STUDENT_RESULT_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		StudentID int(11),
		YearClassID int(11),
		ClassNumber varchar(100),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"	
);

$sql_eClassIP_update[] = array(
	"2010-09-16",
	"Import Class Student - Add OldYearClassID fields to the temp table",
	"ALTER TABLE TEMP_CLASS_STUDENT_IMPORT Add OldYearClassID int(11) After YearClassID"
);

$sql_eClassIP_update[] = array(
	"2010-09-17",
	"IES : Add task enable status",
	"ALTER TABLE IES_TASK ADD Enable tinyint(1) DEFAULT 1 AFTER Approval"	
);

$sql_eClassIP_update[] = array(
	"2010-09-21",
	"Form - Add RecordStatus field to Form Table",
	"ALTER TABLE YEAR Add RecordStatus tinyint(3) Default 1"
);

############## 2010-09-28 IES: IES_SURVEY_EMAIL START ##############
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Add UserID field to IES_SURVEY_EMAIL",
"ALTER TABLE IES_SURVEY_EMAIL ADD UserID INT(8) DEFAULT 0" 
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Add RespondentNature to IES_SURVEY_EMAIL",
"ALTER TABLE IES_SURVEY_EMAIL ADD RespondentNature tinyint(4) COMMENT '1. Email is sent through internal email; 2. Email is sent through external email' default 0"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Set the IES_SURVEY_EMAIL Email with default '' to let ON DUPLICATE KEY can function",
"ALTER TABLE IES_SURVEY_EMAIL CHANGE Email `Email` varchar(255) default ''"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : IES_SURVEY_EMAIL redefine Unique Key",
"ALTER TABLE IES_SURVEY_EMAIL DROP KEY EmailUser, ADD UNIQUE KEY EmailUser (SurveyID,UserID,Email,RespondentNature)"
);
############## 2010-09-28 IES: IES_SURVEY_EMAIL END ##############
############## 2010-09-28 IES: IES_SURVEY_ANSWER START ##############
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Add RespondentNature field to IES_SURVEY_ANSWER",
"ALTER TABLE IES_SURVEY_ANSWER ADD RespondentNature tinyint(4) COMMENT '1. Respondent is save as UserID; 2. Respondent is save as email; 3. Respondent is manually input' default 0"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Add UserID field to IES_SURVEY_ANSWER",
"ALTER TABLE IES_SURVEY_ANSWER ADD UserID int(8) default 0"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Set the IES_SURVEY_ANSWER Respondent with default '' to let ON DUPLICATE KEY can function",
"ALTER TABLE IES_SURVEY_ANSWER CHANGE Respondent `Respondent` varchar(255) default ''"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES :IES_SURVEY_ANSWER redefine Unique Key",
"ALTER TABLE IES_SURVEY_ANSWER DROP KEY AnswerUser, ADD UNIQUE KEY `AnswerUser` (`SurveyID`,`Respondent`,`UserID`,`RespondentNature`);"
);
############## 2010-09-28 IES: IES_SURVEY_ANSWER END ##############
$sql_eClassIP_update[] = array(
"2010-09-28",
"Add table IES_DEFAULT_SURVEY_QUESTION",
"CREATE TABLE IF NOT EXISTS `IES_DEFAULT_SURVEY_QUESTION` (
  `QuestionID` int(11) NOT NULL auto_increment,
  `QuestionStr` varchar(255) default NULL,
  `DateInput` datetime default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`QuestionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"Add table IES_BIBLIOGRAPHY",
"CREATE TABLE IF NOT EXISTS `IES_BIBLIOGRAPHY` (
  `bibliographyID` int(11) NOT NULL auto_increment,
  `UserID` int(11) NOT NULL,
  `Content` mediumtext,
  `Type` varchar(255) NOT NULL,
  `DateInput` timestamp NULL default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `SchemeID` int(8) default NULL,
  PRIMARY KEY  (`bibliographyID`),
  KEY `UserID` (`UserID`),
  KEY `SchemeID` (`SchemeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
"2010-09-29",
"add Todisplay field to IES_BIBLIOGRAPHY",
"ALTER TABLE IES_BIBLIOGRAPHY ADD Todisplay int(8) default '1'"
);

$sql_eClassIP_update[] = array
(
	"2010-10-04",
	"eSports(Sport day) - add InputBy to record the UserID of user who make enrolment.",
	"ALTER TABLE SPORTS_STUDENT_ENROL_EVENT ADD InputBy int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-10-04",
	"eSports(Swimming Gala) - add InputBy to record the UserID of user who make enrolment.",
	"ALTER TABLE SWIMMINGGALA_STUDENT_ENROL_EVENT ADD InputBy int(11) DEFAULT NULL"
);


$sql_eClassIP_update[] = array
(
	"2010-10-04",
	"eDisciplinev12 - add TemplateID in DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING, auto send eNotice when GM->AP (Statis)",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING ADD TemplateID int(11) not null default 0 AFTER UpgradeDeductSubScore1"
);


$sql_eClassIP_update[] = array
(
	"2010-10-04",
	"eDisciplinev12 - add TemplateID in DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE, auto send eNotice when GM->AP (Floating)",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE ADD TemplateID int(11) not null default 0 AFTER DetentionReason"
);

$sql_eClassIP_update[] = array
(
	"2010-10-06",
	"eDisciplinev12 - add sendNoticeAction in DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING, flag of send notice action(Statis)",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING ADD sendNoticeAction tinyint(1) not null default 0 AFTER TemplateID"
);


$sql_eClassIP_update[] = array
(
	"2010-10-06",
	"eDisciplinev12 - add sendNoticeAction in DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE, flag of send notice action (Floating)",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE ADD sendNoticeAction tinyint(1) not null default 0 AFTER TemplateID"
);

$sql_eClassIP_update[] = array
(
	"2010-10-08",
	"eSports - ADD ResultTrial1,ResultTrial2,ResultTrial3 to record trial record for field event.",
	"ALTER TABLE SPORTS_LANE_ARRANGEMENT ADD ResultTrial1 float default NULL AFTER ResultMetre , ADD ResultTrial2 float default NULL AFTER ResultTrial1 , ADD ResultTrial3 float default NULL AFTER ResultTrial2"
);

$sql_eClassIP_update[] = array
(
	"2010-10-11",
	"Staff Attendance V3 - add ReasonActive to CARD_STAFF_ATTENDANCE3_REASON for changing status of a reason.",
	"ALTER TABLE CARD_STAFF_ATTENDANCE3_REASON ADD COLUMN ReasonActive int(2) default 1 AFTER ReasonDescription"
);


$sql_eClassIP_update[] = array
(
	"2010-10-13",
	"Student Registry - add option \"Others\" on some fields in table STUDENT_REGISTRY_STUDENT",
	"ALTER TABLE STUDENT_REGISTRY_STUDENT 
		ADD COLUMN B_PLACE_OTHERS varchar(255) AFTER B_PLACE,
		ADD COLUMN RACE_OTHERS varchar(255) AFTER RACE,
		ADD COLUMN NATION_OTHERS varchar(255) AFTER NATION,
		ADD COLUMN RELIGION_OTHERS varchar(255) AFTER RELIGION,
		ADD COLUMN FAMILY_LANG_OTHERS varchar(255) AFTER FAMILY_LANG,
		ADD COLUMN LODGING_OTHERS varchar(255) AFTER LODGING
		;"
);

$sql_eClassIP_update[] = array
(
	"2010-10-13",
	"Student Registry - add option \"Others\" on some fields in table STUDENT_REGISTRY_PG",
	"ALTER TABLE STUDENT_REGISTRY_PG 
		ADD COLUMN JOB_NATURE_OTHERS varchar(255) AFTER JOB_NATURE,
		ADD COLUMN JOB_TITLE_OTHERS varchar(255) AFTER JOB_TITLE,
		ADD COLUMN EDU_LEVEL_OTHERS varchar(255) AFTER EDU_LEVEL
		;"
);

$sql_eClassIP_update[] = array(
	"2010-10-14",
	"Add Language to IES_SCHEME",
	"ALTER TABLE IES_SCHEME ADD Language varchar(5) default 'b5'"
	);	
$sql_eClassIP_update[] = array(
	"2010-10-14",
	"Add SchemeType to IES_SCHEME",
	"ALTER TABLE IES_SCHEME ADD SchemeType varchar(20) default 'ies'"
	);	
	
$sql_eClassIP_update[] = array
(
	"2010-10-15",
	"iMail Gamma - add DisplayName to MAIL_SHARED_MAILBOX as SharedMailBox sender name.",
	"ALTER TABLE MAIL_SHARED_MAILBOX add column DisplayName varchar(255) default NULL after MailBoxType"
);

$sql_eClassIP_update[] = array
(
	"2010-10-19",
	"IES - new table for marking criteria",
	"CREATE TABLE IF NOT EXISTS IES_MARKING_CRITERIA (
		MarkCriteriaID int(8) NOT NULL auto_increment,
		TitleChi varchar(255) default NULL,
		TitleEng varchar(255) default NULL,
		isDefault tinyint(1) default NULL,
		defaultWeight float default NULL,
		DateInput timestamp NULL default NULL,
		InputBy int(8) default NULL,
		DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (MarkCriteriaID)
	) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-10-19",
	"IES - new table for mapping marking criteria with stage",
	"CREATE TABLE IF NOT EXISTS IES_STAGE_MARKING_CRITERIA (
		StageMarkCriteriaID int(8) NOT NULL auto_increment,
		StageID int(8) default NULL,
		MarkCriteriaID int(8) default NULL,
		task_rubric_id int(8) default NULL,
		Weight float default NULL,
		DateInput timestamp NULL default NULL,
		InputBy int(8) default NULL,
		DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (StageMarkCriteriaID),
		UNIQUE KEY StageCriteria (StageID,MarkCriteriaID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-10-19",
	"IES - new field to store stage-criteria map ID",
	"ALTER TABLE IES_MARKING ADD COLUMN StageMarkCriteriaID int(8)"
);

$sql_eClassIP_update[] = array
(
	"2010-10-25",
	"iCalendar - new table for mapping School Calendar & iCal Event (Group Event Only))",
	" CREATE TABLE IF NOT EXISTS CALENDAR_EVENT_SCHOOL_EVENT_RELATION (
		  CalendarEventID int(11) default NULL,
		  SchoolEventID int(11) default NULL,
		  UNIQUE KEY CalendarSchoolEventMapID (CalendarEventID,SchoolEventID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array
(
	"2010-10-28",
	"Repair System, add default details content",
	"alter table REPAIR_SYSTEM_CATEGORY add DetailsContent text"
);

$sql_eClassIP_update[] = array
(
	"2010-10-28",
	"eHomework - Add creator user id",
	"alter table INTRANET_HOMEWORK add CreatedBy int(11)"
);

$sql_eClassIP_update[] = array
(
	"2010-10-29",
	"eNotice - Add notice type (parent / student)",
	"alter table INTRANET_NOTICE add TargetType char(1) default 'P'"
);

$sql_eClassIP_update[] = array
(
	"2010-11-02",
	"Student Profile - Add AcademicYearID, YearTermID, eEnrol-related fields and indices",
	"Alter Table PROFILE_STUDENT_ACTIVITY 	Add Column AcademicYearID int(8) Default Null, 
											Add Column YearTermID int(8) Default Null,
											Add Column YearClassID int(8) Default Null,
											Add Column FromModule varchar(64) Default Null, 
											Add Column eEnrolRecordType varchar(64) Default Null, 
											Add Column eEnrolRecordID int(11) Default Null, 
											Add Index AcademicYearID (AcademicYearID), 
											Add Index YearTermID (YearTermID), 
											Add Index YearClassID (YearClassID), 
											Add Index eEnrolRecordID (eEnrolRecordID)
	"
);

$sql_eClassIP_update[] = array
(
	"2010-11-02",
	"eNotice - Add Footer choice",
	"alter table INTRANET_NOTICE add column Footer text AFTER ReplySlipContent"
);

$sql_eClassIP_update[] = array(
	"2010-11-02",
	"Create table for data transfer log",
	"CREATE TABLE IF NOT EXISTS MODULE_RECORD_TRANSFER_LOG
	(
		LogID int(11) NOT NULL auto_increment,
		FromModule varchar(20) NOT NULL,
		ToModule varchar(20) NOT NULL,
		RecordDetail text default NULL,
		LogDate datetime default NULL,
		LogBy int(11) default NULL,
		PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student activity profile",
	"ALTER TABLE PROFILE_STUDENT_ACTIVITY MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student attendance profile",
	"ALTER TABLE PROFILE_STUDENT_ATTENDANCE MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student award profile",
	"ALTER TABLE PROFILE_STUDENT_AWARD MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student merit profile",
	"ALTER TABLE PROFILE_STUDENT_MERIT MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student service profile",
	"ALTER TABLE PROFILE_STUDENT_SERVICE MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-04",
	"Subject Group - Add field to record the locked Class for the Subject Group",
	"ALTER TABLE SUBJECT_TERM_CLASS Add Column LockedYearClassID int(8) default Null, Add Index LockedYearClassID (LockedYearClassID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-10",
	"IES : ADD Comment FOR IES_SURVEY_MAPPING",
	"ALTER TABLE IES_SURVEY_MAPPING ADD Comment longtext"
);

$sql_eClassIP_update[] = array(
	"2010-11-11",
	"Staff Attendance V3 : Create table for storing user settings (e.g. report option settings) for Staff Attendance",
	"CREATE TABLE IF NOT EXISTS CARD_STAFF_ATTENDANCE3_USER_SETTING(
		SettingID int(11) NOT NULL auto_increment, 
		UserID int(11) NOT NULL,
		SettingName varchar(100) NOT NULL,
		SettingValue text default NULL,
		DisplayName varchar(255) NOT NULL,
		DateModified datetime default NULL,
		PRIMARY KEY(SettingID),
		UNIQUE UniqueName(UserID, SettingName, DisplayName),
		INDEX UserID (UserID)
	) ENGINE=InnoDB CHARSET=utf8 "
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Use to Categorized users, Customization for eRC Rubrics (Shek Wu).",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_EXTRA_INFO_CATEGORY (
		CategoryID int(11) NOT NULL auto_increment,
		CategoryCode varchar(10),
		CategoryNameCh varchar(255),
		CategoryNameEn varchar(255),
		DisplayOrder int(11),
		RecordType int(2),
		RecordStatus int(2) DEFAULT 1,
		DateInput datetime,
		DateModified datetime,
		InputBy int(11) ,
		LastModifiedBy int(11) ,
		PRIMARY KEY (CategoryID),
		INDEX CategoryCode (CategoryCode),
		INDEX CategoryNameCh (CategoryNameCh),
		INDEX CategoryNameEn (CategoryNameEn)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Use to Categorized users, Customization for eRC Rubrics (Shek Wu).",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_EXTRA_INFO_ITEM (
		ItemID int(11) NOT NULL auto_increment,
		CategoryID int(11) NOT NULL,
		ItemCode varchar(10),
		ItemNameCh varchar(255),
		ItemNameEn varchar(255),
		DisplayOrder int(11),
		RecordStatus int(2) DEFAULT 1,
		DateInput datetime,
		DateModified datetime,
		InputBy int(11) ,
		LastModifiedBy int(11) ,
		PRIMARY KEY (ItemID),
		INDEX CategoryID (CategoryID),
		INDEX ItemCode (ItemCode),
		INDEX ItemNameCh (ItemNameCh),
		INDEX ItemNameEn (ItemNameEn)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Use to Categorized users, Customization for eRC Rubrics (Shek Wu).",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_EXTRA_INFO_MAPPING (
		UserItemID int(11) NOT NULL auto_increment,
		UserID int(11) NOT NULL,
		ItemID int(11) NOT NULL,
		DateModified datetime,
		LastModifiedBy int(11) ,
		PRIMARY KEY (UserItemID),
		UNIQUE INDEX UserItemMapping (UserID,ItemID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Add Sequence to  IES_SURVEY_MAPPING",
	"ALTER TABLE IES_SURVEY_MAPPING ADD Sequence int(8) default NULL"
);


$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Create Table IES_FAQ_FILE",
	"CREATE TABLE IF NOT EXISTS `IES_FAQ_FILE` (
  `FileID` int(11) NOT NULL auto_increment,
  `FileName` varchar(255) default NULL,
  `FolderPath` varchar(255) default NULL,
  `FileHashName` varchar(255) default NULL,
  `QuestionID` int(11) NOT NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Add column for storing scheme score",
	"ALTER TABLE IES_SCHEME_STUDENT 
	ADD COLUMN Score float AFTER UserID,
	MODIFY DateInput timestamp NULL DEFAULT NULL,
	ADD COLUMN DateModified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ADD COLUMN ModifyBy int(8)"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Add column for storing stage weight (for calculating scheme score)",
	"ALTER TABLE IES_STAGE ADD COLUMN Weight float AFTER Deadline"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Add column for storing criteria display sequence",
	"ALTER TABLE IES_MARKING_CRITERIA ADD COLUMN defaultSequence int(8) AFTER defaultWeight"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Create table for storing document section of stages",
	"CREATE TABLE IF NOT EXISTS IES_STAGE_DOC_SECTION(
	SectionID int(8) NOT NULL auto_increment,
	StageID int(8),
	Sequence int(8),
	Title varchar(255),
	DateInput timestamp NULL DEFAULT NULL,
	InputBy int(8),
	DateModified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	ModifyBy int(8),
	PRIMARY KEY (SectionID)
) ENGINE = InnoDB DEFAULT CHARSET = utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Create table for storing tasks of document sections",
	"CREATE TABLE IF NOT EXISTS IES_STAGE_DOC_SECTION_TASK(
	SectionID int(8),
	TaskID int(8),
	Sequence int(8),
	DateInput timestamp NULL DEFAULT NULL,
	InputBy int(8),
	DateModified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	ModifyBy int(8),
	PRIMARY KEY (SectionID, TaskID)
) ENGINE = InnoDB DEFAULT CHARSET = utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-17",
	"Create table for storing IES comment category",
	"CREATE TABLE IF NOT EXISTS `IES_COMMENT_CATEGORY` (
  `CategoryID` int(11) NOT NULL auto_increment,
  `Title` mediumtext,
  `DateInput` datetime default NULL,
  `InputBy` int(11) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(11) default NULL,
  PRIMARY KEY  (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-17",
	"Add categoryID to IES_COMMENT",
	"ALTER TABLE IES_COMMENT ADD `CategoryID` int(11) default NULL"
);
$sql_eClassIP_update[] = array(
	"2010-11-18",
	"Add TitleEN to IES_COMMENT_CATEGORY",
	"ALTER TABLE IES_COMMENT_CATEGORY ADD `TitleEN` mediumtext"
);
$sql_eClassIP_update[] = array(
	"2010-11-18",
	"Add CommentEN to IES_COMMENT",
	"ALTER TABLE IES_COMMENT ADD `CommentEN` mediumtext"
);
$sql_eClassIP_update[] = array(
	"2010-11-18",
	"Add Cache table on class/ group confirm status",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_CACHED_CONFIRM_STATUS (
	  TargetDate Date NOT NULL,
	  DayType int(2) NOT NULL,
	  Type varchar(20) NOT NULL,
	  ConfirmStatus varchar(2) default NULL,
	  PRIMARY KEY  (TargetDate,DayType,Type)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-22",
	"Create Table to Store User Signature Image",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_SIGNATURE (
		SignatureID int(11) NOT NULL auto_increment,
		UserLogin varchar(20) NOT NULL,
		SignatureLink text NOT NULL,
		DateInput datetime,
		InputBy int(11) ,
		PRIMARY KEY (SignatureID),
		INDEX UserLogin(UserLogin)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-22",
	"Subject Group - Change SubjectGroupID to auto increment",
	"ALTER TABLE SUBJECT_TERM_CLASS CHANGE SubjectGroupID SubjectGroupID int(8) NOT NULL auto_increment"
);

$sql_eClassIP_update[] = array(
	"2010-11-23",
	"Added DefaultFlag Field to CARD_STAFF_ATTENDANCE3_REASON for default reason",
	"alter table CARD_STAFF_ATTENDANCE3_REASON add DefaultFlag int(2) default NULL after ReasonActive"
);

$sql_eClassIP_update[] = array(
	"2010-11-23",
	"Add TAG table (can apply to all modules)",
	"CREATE TABLE IF NOT EXISTS TAG (
		TagID int(11) NOT NULL auto_increment,
		TagName varchar(200) NOT NULL,
		DateInput datetime,
		InputBy int(11),
		PRIMARY KEY (TagID)
	)  ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-11-23",
	"Add TAG_RELATIONSHIP table (can apply to all modules)",
	"CREATE TABLE IF NOT EXISTS TAG_RELATIONSHIP (
		TagRelationID int(11) not null auto_increment,
		TagID int(11) not null,
		Module varchar(20) not null,
		DateInput datetime,
		InputBy int(11),
		DateModified datetime,
		ModifyBy int(11),
		PRIMARY KEY (TagRelationID),
		INDEX TagID (TagID),
		INDEX Module (Module)
	)  ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-11-23",
	"Add column TAGID in STUDENT_REGISTRY_STUDENT",
	"alter table STUDENT_REGISTRY_STUDENT add column TAGID varchar(200) AFTER EMAIL"
);

$sql_eClassIP_update[] = array(
	"2010-11-24",
	"Add column CompulsoryInReport in table DISCIPLINE_ACCU_CATEGORY_ITEM",
	"alter table DISCIPLINE_ACCU_CATEGORY_ITEM add column CompulsoryInReport tinyint(1) default 0 not null, add index CompulsoryInReport(CompulsoryInReport)"
);

$sql_eClassIP_update[] = array(
	"2010-11-24",
	"Add column CompulsoryInReport in table DISCIPLINE_MERIT_ITEM",
	"alter table DISCIPLINE_MERIT_ITEM add column CompulsoryInReport tinyint(1) default 0 not null after RecordStatus, add index CompulsoryInReport(CompulsoryInReport)"
);

$sql_eClassIP_update[] = array(
	"2010-11-24",
	"Repair System - Add Request Summary settings",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_REQUEST_SUMMARY (
		RecordID int(8) NOT NULL auto_increment,
		CategoryID int(11) default NULL,
		Title varchar(255) default NULL,
		Sequence int(2) default NULL,
		RecordStatus int(1) default NULL,
		DateInput datetime default NULL,
		InputBy int(8) default NULL,
		DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (RecordID),
		INDEX CategoryID (CategoryID),
		INDEX Title (Title)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-11-25",
	"Subject Group - Add Index UserID, RowNumber to TEMP_SUBJECT_GROUP_IMPORT",
	"Alter Table TEMP_SUBJECT_GROUP_IMPORT Add Index UserID (UserID), Add Index RowNumber (RowNumber)"
);

$sql_eClassIP_update[] = array(
	"2010-11-25",
	"Class - Add Index UserID, RowNumber to TEMP_CLASS_STUDENT_IMPORT",
	"Alter Table TEMP_CLASS_STUDENT_IMPORT Add Index UserID (UserID), Add Index RowNumber (RowNumber)"
);

$sql_eClassIP_update[] = array(
	"2010-11-25",
	"Class - Add Index UserID, RowNumber, YearClassID to TEMP_CLASS_STUDENT_RESULT_IMPORT",
	"Alter Table TEMP_CLASS_STUDENT_RESULT_IMPORT Add Index UserID (UserID), Add Index RowNumber (RowNumber), Add Index YearClassID (YearClassID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-25",
	"Timetable - Add Index UserID, RowNumber, TimetableID to TEMP_TIMETABLE_LESSON_IMPORT",
	"Alter Table TEMP_TIMETABLE_LESSON_IMPORT Add Index UserID (UserID), Add Index RowNumber (RowNumber), Add Index TimetableID (TimetableID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-29",
	"INTRANET_HOMEWORK",
	"alter table INTRANET_HOMEWORK add index HWSubjectID (SubjectID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-29",
	"INTRANET_HOMEWORK",
	"alter table INTRANET_HOMEWORK add index HWClassGroupID (ClassGroupID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-29",
	"INTRANET_HOMEWORK",
	"alter table INTRANET_HOMEWORK add index HWYearTermID (YearTermID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-29",
	"INTRANET_HOMEWORK",
	"alter table INTRANET_HOMEWORK add index HWAcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-30",
	"INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS add column CurrentStatus int(11) default 0 after RecordStatus"
);

$sql_eClassIP_update[] = array(
	"2010-11-30",
	"INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS add column CheckInCheckOutRemark text after CurrentStatus"
);

$sql_eClassIP_update[] = array(
	"2010-11-30",
	"INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add column CurrentStatus int(11) default 0 after RecordStatus"
);

$sql_eClassIP_update[] = array(
	"2010-11-30",
	"INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add column CheckInCheckOutRemark text after CurrentStatus"
);

$sql_eClassIP_update[] = array(
	"2010-12-06",
	"add table DISCIPLINE_SKSS_CONDUCT_POTENTIALS_REPORT [CRM Ref No.: 2010-0811-1619]",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_SKSS_CONDUCT_POTENTIALS_REPORT (
		RecordID int(8) NOT NULL auto_increment,
		AcademicYearID int(8) NOT NULL,
		StudentID int(8) NOT NULL default 0,
		GradeString varchar(200),
		DateInput datetime default NULL,
		InputBy int(8) default NULL,
		DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (RecordID),
		INDEX AcademicYearID (AcademicYearID),
		INDEX StudentID (StudentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-12-06",
	"eEnrolment - Performance adjustment [Customization]",
	"CREATE TABLE IF NOT EXISTS ENROLMENT_ADJUSTMENT_PERFORMNAMCE_CUST (
	  RecordID int(8) NOT NULL auto_increment,
	  UserID int(8),
	  AcademicYearID int(8),
	  Adjustment int(8),
	  TotalPerformance float(8,2),
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY (RecordID),
	  INDEX UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"iMail - Performance issue [Add back a Index CampusMailFromID - Special Case only]",
	"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX CampusMailFromID (CampusMailFromID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"iMail - Performance issue [Drop Duplicate Index]",
	"ALTER TABLE INTRANET_CAMPUSMAIL drop index CampusMailFromID_2, drop index CampusMailFromID_3, drop index CampusMailFromID_4, drop index CampusMailFromID_5, drop index UserFolderID_2, drop index MailType_2"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - ResourceID]",
	"alter table INTRANET_BOOKING_RECORD add index ResourceID (ResourceID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - UserID]",
	"alter table INTRANET_BOOKING_RECORD add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - PeriodicBookingID]",
	"alter table INTRANET_BOOKING_RECORD add index PeriodicBookingID (PeriodicBookingID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - RecordStatus]",
	"alter table INTRANET_BOOKING_RECORD add index RecordStatus (RecordStatus)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - BookingDate]",
	"alter table INTRANET_BOOKING_RECORD add index BookingDate (BookingDate)"
);

$sql_eClassIP_update[] = array(
	"2010-12-08",
	"School Calendar - add Column SkipOnWeekday",
	"alter table INTRANET_CYCLE_GENERATION_PERIOD add column SkipOnWeekday text after SaturdayCounted"
);

$sql_eClassIP_update[] = array(
	"2010-12-09",
	"iMail Gamma - change MailBoxID type from varchar to int",
	"alter table MAIL_SHARED_MAILBOX_MEMBER modify MailBoxID int(11) NOT NULL"
);

$sql_eClassIP_update[] = array(
	"2010-12-13",
	"Add TitleChinese for Group",
	"alter table INTRANET_GROUP add TitleChinese varchar(100) default NULL after Title"
);

$sql_eClassIP_update[] = array(
	"2010-12-13",
	"create table for student attendance outing reason",
	"CREATE TABLE CARD_STUDENT_OUTING_REASON (
	  RecordID int(11) NOT NULL auto_increment,
	  ReasonText varchar(255) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11),
	  DateModify datetime default NULL,
	  ModifyBy int(11),
	  PRIMARY KEY  (RecordID),
	  UNIQUE KEY OutingReasonText (ReasonText)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-12-13",
	"Subject Settings -> Relationship between Form & Subject",
	"CREATE TABLE IF NOT EXISTS SUBJECT_YEAR_RELATION (
	  SubjectID int(11) NOT NULL,
	  YearID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (SubjectID, YearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-12-15",
	"iMail Gamma - Add ComposeTime to MAIL_ATTACH_FILE_MAP prevent opening multiple Compose Mail page delete uploaded attchment",
	"alter table MAIL_ATTACH_FILE_MAP add column ComposeTime datetime default '1970-01-01 00:00:00'"
);

$sql_eClassIP_update[] = array(
	"2010-12-15",
	"iMail Gamma - Add ComposeTime to MAIL_INLINE_IMAGE_MAP prevent opening multiple Compose Mail page delete inline images",
	"alter table MAIL_INLINE_IMAGE_MAP add column ComposeTime datetime default '1970-01-01 00:00:00'"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RelatedSubLocationID to TABLE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD",
	"alter table INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD add index RelatedSubLocationID (RelatedSubLocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RelatedItemID to TABLE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD",
	"alter table INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD add index RelatedItemID (RelatedItemID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RuleID to TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"alter table INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER add index RuleID (RuleID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index YearID to TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"alter table INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER add index YearID (YearID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index UserType to TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"alter table INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER add index UserType (UserType)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index IsTeaching to TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"alter table INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER add index IsTeaching (IsTeaching)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index LocationID to TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION add index LocationID (LocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index LocationID to TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION add index LocationID (LocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index BookingRuleID to TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION add index BookingRuleID (BookingRuleID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index LocationID to TABLE INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION add index LocationID (LocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index UserBookingRuleID to TABLE INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION add index UserBookingRuleID (UserBookingRuleID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index BookingID to TABLE INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS",
	"alter table INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS add index BookingID (BookingID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index BookingID to TABLE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS add index BookingID (BookingID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index ItemID to TABLE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS add index ItemID (ItemID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index LocationID to TABLE INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION add index LocationID (LocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index GroupID to TABLE INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION add index GroupID (GroupID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RelatedTo to TABLE INTRANET_EBOOKING_RECORD",
	"alter table INTRANET_EBOOKING_RECORD add index RelatedTo (RelatedTo)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RequestedBy to TABLE INTRANET_EBOOKING_RECORD",
	"alter table INTRANET_EBOOKING_RECORD add index RequestedBy (RequestedBy)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index ResponsibleUID to TABLE INTRANET_EBOOKING_RECORD",
	"alter table INTRANET_EBOOKING_RECORD add index ResponsibleUID (ResponsibleUID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index PIC to TABLE INTRANET_EBOOKING_RECORD",
	"alter table INTRANET_EBOOKING_RECORD add index PIC (PIC)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index BookingID to TABLE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add index BookingID (BookingID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RoomID to TABLE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add index RoomID (RoomID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index PIC to TABLE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add index PIC (PIC)"
);

$sql_eClassIP_update[] = array(
	"2010-12-17",
	"IES - Add Default MAX MARK for IES_MARKING_CRITERIA ",
	"ALTER TABLE IES_MARKING_CRITERIA ADD `defaultMaxScore`  float default NULL after `isDefault`;"
);

$sql_eClassIP_update[] = array(
	"2010-12-17",
	"IES - Add setting MaxScore for IES_SCHEME ",
	"ALTER TABLE IES_SCHEME ADD `MaxScore`  float default NULL after `Title`;"
);

$sql_eClassIP_update[] = array(
	"2010-12-17",
	"IES - Add setting MaxScore for IES_STAGE ",
	"ALTER TABLE IES_STAGE ADD `MaxScore`  float default NULL after `Deadline`;"
);

$sql_eClassIP_update[] = array(
	"2010-12-17",
	"IES - Add setting MaxScore for IES_STAGE_MARKING_CRITERIA",
	"ALTER TABLE IES_STAGE_MARKING_CRITERIA ADD `MaxScore`  float default NULL after `task_rubric_id`;"
);

$sql_eClassIP_update[] = array(
	"2010-12-20",
	"Add Preset waive field for preset absence record",
	"alter table CARD_STUDENT_PRESET_LEAVE add Waive int(1) default NULL after Reason"
);

$sql_eClassIP_update[] = array(
	"2010-12-21",
	"eComm - add annoumcnent settings - allow delete others records",
	"alter table INTRANET_GROUP add AllowDeleteOthersAnnouncement tinyint(1) default 1"
);

$sql_eClassIP_update[] = array(
	"2010-12-28",
	"Polling - add attachment",
	"alter table INTRANET_POLLING add Attachment varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2011-01-06",
	"Super system admin",
	"alter table INTRANET_USER add isSystemAdmin tinyint(1) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-01-11",
	"Create Mysql Function MINIMUM() - simulate php function min()",
	"
		CREATE FUNCTION MINIMUM(n1 double, n2 double) 
		RETURNS double DETERMINISTIC 
		RETURN IF(n1<=n2,n1,n2)
	"
);

$sql_eClassIP_update[] = array(
	"2011-01-11",
	"Create Mysql Function MAXIMUM() - simulate php function max()",
	"
		CREATE FUNCTION MAXIMUM(n1 double, n2 double) 
		RETURNS double DETERMINISTIC 
		RETURN IF(n1>=n2,n1,n2)
	"
);

$sql_eClassIP_update[] = array(
	"2011-01-17",
	"Notice Payment - add column TempItemAmount for \"Save Draft\" ",
	"alter table INTRANET_NOTICE_REPLY add TempItemAmount int(10) default 0 AFTER Answer"
);

$sql_eClassIP_update[] = array(
	"2011-01-24",
	"Resource Booking - add an index UserIDBookDateRecordStatusPeriodBookingID to improve the performance",
	"alter table INTRANET_BOOKING_RECORD add KEY UserIDBookDateRecordStatusPeriodBookingID (UserID,BookingDate,RecordStatus,PeriodicBookingID)"
);

$sql_eClassIP_update[] = array(
	"2011-01-31",
	"Create table ECLASS_UPDATE_LOG - for \"eClass\" update checking",
	"CREATE TABLE IF NOT EXISTS ECLASS_UPDATE_LOG (
		RecordID INT(8) NOT NULL AUTO_INCREMENT,
		Module varchar(100) NOT NULL,
		Version float(5,2) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (RecordID),
		INDEX Module (Module),
		INDEX Version (Version)
	) ENGINE=InnoDB Charset=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-01-31",
	"Create table ECLASS_MODULE_VERSION_LOG - store most update module version number",
	"CREATE TABLE IF NOT EXISTS ECLASS_MODULE_VERSION_LOG (
		RecordID INT(8) NOT NULL AUTO_INCREMENT,
		Module varchar(100) NOT NULL,
		VersionNo float(5,2) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (RecordID),
		INDEX Module (Module),
		INDEX VersionNo (VersionNo)
	) ENGINE=InnoDB Charset=utf8"
);


$sql_eClassIP_update[] = array(
	"2011-02-01",
	"eLibrary - add colume ExtensionTLF in table INTRANET_ELIB_BOOK_CHAPTER_TLF",
	"ALTER TABLE INTRANET_ELIB_BOOK_CHAPTER_TLF ADD COLUMN ExtensionTLF varchar(9) DEFAULT '.tlf' AFTER StyleTLF;"
);

$sql_eClassIP_update[] = array(
	"2011-02-02",
	"Create table ECLASS_BUBBLE_VERSION - bubble version",
	"CREATE TABLE IF NOT EXISTS ECLASS_BUBBLE_VERSION (
		VersionID INT(8) NOT NULL AUTO_INCREMENT,
		Version varchar(100) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (VersionID),
		INDEX Version (Version)
	) ENGINE=InnoDB Charset=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-02-02",
	"Create table ECLASS_BUBBLE_READ - bubble message read (don't remind again on same version)",
	"CREATE TABLE IF NOT EXISTS ECLASS_BUBBLE_READ (
		VersionID INT(8) NOT NULL AUTO_INCREMENT,
		UserID INT(8) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (VersionID, UserID),
		INDEX VersionID (VersionID),
		INDEX UserID (UserID)
	) ENGINE=InnoDB Charset=utf8"
);

?>