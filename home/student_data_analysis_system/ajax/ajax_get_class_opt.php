<?php
/*
 * Change Log: 
 *  Page using this ajax: DSE prediction
 * Date: 2016-12-12 Villa change the warning msg
 * Date: 2016-12-08 Villa add class teacher filtering
 * Date: 2016-09-19 Villa Open the file - generate checkbox object for selected Academic Year and Form
 */

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$class = $libpf_exam->getClassListByAcademicYearIDAndForm($ay_id,$form_id);
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();

if(count($class)!=0){  //check if the class return null
	if($isClassTeacher){
		$i = 0;
		foreach($accessRight['classTeacher'] as $_classInfo){
			foreach($class as $_class){
				if($_classInfo['AcademicYearID']==$ay_id){
					if($_class['YearClassID']==$_classInfo['YearClassID']){
						$temp[$i]['YearClassID'] = $_class['YearClassID'];
						$temp[$i]['ClassTitleEN'] = $_class['ClassTitleEN'];
						$temp[$i]['ClassTitleB5'] = $_class['ClassTitleB5'];
						$temp[$i]['WEBSAMSCode'] = $_class['WEBSAMSCode'];
						$i++;
					}
				}
			}
		}
		unset($class);
		$class = $temp;
	}
	if(count($class)){
		$classCheckBox = '<input type="checkbox" id="resultType_SelectAll" onChange="selectAll()" checked><label for="resultType_SelectAll">'.$Lang['Btn']['SelectAll'].'</label>&nbsp;&nbsp;&nbsp';
		for($i = 0; $i<count($class); $i++){  //generate the checkbox
			$name =Get_Lang_Selection($class[$i]["ClassTitleB5"], $class[$i]["ClassTitleEN"]);
			$enName = $class[$i]["ClassTitleEN"];
			$classCheckBox .= '<input type="checkbox" class="ClassSel" id="class_'.$name.'" name="Class[]" value="'.$enName.'" formName="'.$name.'" onChange="unChecked()" checked/><label for="class_'.$name.'">'.$name.'</label>&nbsp;';
			$classTeacherClassIdArr[] = $yearClass["YearClassID"];
		}
	}else{
		$classCheckBox = $Lang['SDAS']['NoTeachingClass'];
	}
}
else{
	$classCheckBox = $Lang['General']['JS_warning']['SelectAForm'];
}
echo $classCheckBox;
?>

