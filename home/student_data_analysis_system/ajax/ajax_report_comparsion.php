<?php
// Using:

/**
 * Change Log
 * 2018-07-26 Bill
 * 	-	New File
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");



intranet_auth();
intranet_opendb();

$fcm = new form_class_manage();
$sbj = new subject();

$lpf = new libportfolio();
$json = new JSON_obj();

$emptySymbol = '---';
$isSingleStudentMode = $type == 'Class' && count((array)$StudentID) == 1;

########## Access Right [START] ##########

########## Access Right [END] ##########

#### Function ####
function Compare($a, $b) {
	$temp_a = explode('_', $a);
	$temp_b = explode('_', $b);
	if($temp_a[0] > $temp_b[0]) {
		return 1;
	} else if($temp_a[0] == $temp_b[0]) {
		if(empty($temp_a[1])) {           // Main Subject
			return -1;
		} else {
			if($temp_a[1] > $temp_b[1]) {
				return 1;
			} else {
				return -1;
			}
		}
	} else {
		$abc = $temp_a[0]."<".$temp_b[0];
		return -1;
	}
}

function assessmentTermSort($a, $b) {
	// -1 => first, 1=> later
	if($a == '0') {
		return 1;
	} else if($b == '0') {
		return -1;
	} else if($a > $b) {
		return 1;
	} else {
		return -1;
	}
}

function calculateDataStat($dataAry) {
    $returnAry = array();
    $emptySymbol = '---';
    foreach((array)$dataAry as $student_data) {
        foreach((array)$student_data as $data_index => $data) {
            if($data != $emptySymbol) {
                $returnAry[$data_index][] = $data;
            }
        }
    }
    
    $dataAry = array();
    foreach($returnAry as $data_index => $this_data) {
        $dataAry[$data_index] = round((array_sum($this_data) / count($returnAry[$data_index])), 2);
        if(strpos($this_data[0], '%') !== false) {
            $dataAry[$data_index] .= '%';
        }
    }
    
    return $dataAry;
}
#### Function ####

$li = new libdb();
$lpf = new libpf_sturec();
$lpf->createTempAssessmentSubjectTable($li, true);

######## Get Data [START] ########

#### Get Target Year [START] ####
$sql = "SELECT
			ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
  		FROM
  			ACADEMIC_YEAR ay
    		INNER JOIN ACADEMIC_YEAR_TERM ayt ON ay.AcademicYearID = ayt.AcademicYearID
        WHERE
            ay.AcademicYearID IN ('".implode("', '", (array)$AcademicYearID)."')
        GROUP BY
            ay.AcademicYearID
  		ORDER BY
  			ayt.TermStart DESC";
$temp = $li->returnResultSet($sql);
$yearAssoc = BuildMultiKeyAssoc($temp, 'AcademicYearID');
#### Get Target Year [END] ####

#### Get Target Term [START] ####
$sql = "SELECT
			YearTermID, AcademicYearID, YearTermNameEN, YearTermNameB5
  		FROM
  			ACADEMIC_YEAR_TERM
        WHERE
            AcademicYearID IN ('".implode("', '", (array)$AcademicYearID)."')
  		ORDER BY
  			TermStart";
$temp = $li->returnResultSet($sql);
$temp = BuildMultiKeyAssoc($temp, 'AcademicYearID', 'YearTermID', 1, 1);

$targetTermIDAry = array();
foreach ($temp as $_yearTermIDAry) {
    $targetTermIDAry[] = $_yearTermIDAry[($targetTermNumber - 1)];
}
#### Get Target Term [END] ####

#### Get Target Assessment [START] ####
$assessmentAssoc = array();
$targetAssessmentAry = array();
foreach($targetAssessment as $thisAssessment) {
    if($thisAssessment == 'NULL'){
        $targetAssessmentAry[] = "T".$targetTermNumber;
        $assessmentAssoc["T".$targetTermNumber] = 'Year Grade';
    } else {
        $targetAssessmentAry[] = "T".$targetTermNumber.$thisAssessment;
        if($thisAssessment == 'A1') {
            $assessmentAssoc["T".$targetTermNumber.$thisAssessment] = 'CA';
        } else {
            $assessmentAssoc["T".$targetTermNumber.$thisAssessment] = 'Exam';
        }
    }
}
#### Get Target Assessment [END] ####

#### Get Target Compare Type [START] ####
$compareTypeAssoc = array();
foreach($compareColumn as $thisCompareType){
    if($thisCompareType == 'score') {
        $compareTypeDisplay = 'Score';
    } else if ($thisCompareType == 'sd') {
        $compareTypeDisplay = 'Standard score';
    } else if ($thisCompareType == 'percentile') {
        $compareTypeDisplay = 'Percentile';
    }
    $compareTypeAssoc[$thisCompareType] = $compareTypeDisplay;
}
#### Get Target Compare Type [END] ####

#### Get Target Form, Class, Student [START] ####
if($type == 'SubjectClass')
{
    $SubjectID = array();
    $StudentID = array();
    $studentGroupAssoc = array();
    foreach($SubjectGroupID as $_subjectGroupID) {
        $subjectGroup = new subject_term_class($_subjectGroupID, true, true);
        if(!empty($subjectGroup) && !empty($subjectGroup->ClassStudentList)) {
            if($singleTypeOnly) {
                if(empty($subjectGroup->ClassTeacherList)) {
                    continue;
                }
                
                $subjectGroupTeacherIDs = Get_Array_By_Key((array)$subjectGroup->ClassTeacherList, 'UserID');
                if(!in_array($UserID, $subjectGroupTeacherIDs)) {
                    continue;
                }
            }
            
            $subjectStudentIDs = Get_Array_By_Key((array)$subjectGroup->ClassStudentList, 'UserID');
            foreach($subjectStudentIDs as $thisStudentID) {
                $studentGroupAssoc[$thisStudentID][] = $subjectGroup->SubjectID;
            }
            $StudentID = array_merge($StudentID, $subjectStudentIDs);
            $SubjectID[] = $subjectGroup->SubjectID;
        }
    }
    
    $SubjectID = array_unique(array_unique(array_filter($SubjectID)));
    $StudentID = array_unique(array_unique(array_filter($StudentID)));
}

$sql = "SELECT
			y.YearID, y.YearName, yc.AcademicYearID,
            yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5, ycu.ClassNumber,
            iu.UserID, iu.EnglishName, iu.ChineseName, iu.NickName
		FROM
			YEAR y
    		INNER JOIN YEAR_CLASS yc on y.YearID = yc.YearID
    		INNER JOIN YEAR_CLASS_USER ycu on yc.YearClassID = ycu.YearClassID
            INNER JOIN INTRANET_USER iu ON ycu.UserID = iu.UserID
		WHERE
			ycu.UserID IN ('".implode("', '", (array)$StudentID)."') AND 
            yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		ORDER BY
			y.Sequence, yc.Sequence, ycu.ClassNumber ";
$temp = $li->returnResultSet($sql);
$formAssoc = BuildMultiKeyAssoc($temp, 'YearID');
// $classAssoc = BuildMultiKeyAssoc($temp, 'YearClassID');
$studentAssoc = BuildMultiKeyAssoc($temp, 'UserID');
#### Get Target Form, Class, Student [END] ####

#### Get Target Student Form History [START] ####
$sql = "SELECT
			y.YearID, y.YearName, yc.AcademicYearID,
            yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5, ycu.ClassNumber,
            iu.UserID, iu.EnglishName, iu.ChineseName, iu.NickName
		FROM
			YEAR y
    		INNER JOIN YEAR_CLASS yc on y.YearID = yc.YearID
    		INNER JOIN YEAR_CLASS_USER ycu on yc.YearClassID = ycu.YearClassID
            INNER JOIN INTRANET_USER iu ON ycu.UserID = iu.UserID
		WHERE
			ycu.UserID IN ('".implode("', '", (array)$StudentID)."')
		ORDER BY
			y.Sequence, yc.Sequence, ycu.ClassNumber ";
$temp = $li->returnResultSet($sql);
$studentFormAssoc = BuildMultiKeyAssoc($temp, array('AcademicYearID', 'UserID'), 'YearID', 1, 1);
$allFormIDAry = BuildMultiKeyAssoc($temp, 'YearID');
#### Get Target Form, Class, Student [END] ####

#### Get Target Subject [END] ####
$sql = "SELECT
            RecordID, EN_SNAME, CH_SNAME, EN_DES, CH_DES, EN_ABBR, CH_ABBR
        FROM
            {$intranet_db}.ASSESSMENT_SUBJECT
        WHERE
            RecordID IN ('".implode("', '", (array)$SubjectID)."')
        ORDER BY
            DisplayOrder ";
$temp = $li->returnResultSet($sql);
$subjectAssoc = BuildMultiKeyAssoc($temp, 'RecordID');
#### Get Target Subject [END] ####

#### Get Student Assessment [START] ####
$sql = "SELECT
			UserID, AcademicYearID, Year, YearTermID, Semester, YearClassID, ClassName, SubjectID, SubjectCode, SubjectComponentID, SubjectComponentCode, 
            Score, Grade, OrderMeritForm, OrderMeritFormTotal, 
			/*CONCAT(SubjectID, IF(SubjectComponentID is not null,CONCAT('-',SubjectComponentID),'')) as realSubjectID,*/
            IF(TermAssessment IS NULL, 'T".$targetTermNumber."', TermAssessment) as realTermAssessment,
            IF(TermAssessment IS NULL, 0, TermAssessment) as TermAssessment
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
		WHERE
			UserID IN ('".implode("', '", (array)$StudentID)."') AND
            AcademicYearID IN ('".implode("', '", (array)$AcademicYearID)."') AND 
            YearTermID IN ('".implode("', '", (array)$targetTermIDAry)."') AND 
            IF(TermAssessment IS NULL, 'T".$targetTermNumber."', TermAssessment) IN ('".implode("', '", (array)$targetAssessmentAry)."') AND
            SubjectID IN ('".implode("', '", (array)$SubjectID)."') AND SubjectComponentID IS NULL ";
$exam_records = $li->returnResultSet($sql);
$examAssoc = BuildMultiKeyAssoc($exam_records, array('realTermAssessment', 'SubjectID', 'UserID', 'AcademicYearID'), array(), 0);
#### Get Student Assessment records [END] ####

#### Get SD Mean [START] ####
$sql = "SELECT
			AcademicYearID, YearTermID, TermAssessment, ClassLevelID, YearClassID, SubjectID, SubjectComponentID, SD, MEAN, 
			/*CONCAT(SubjectID, IF(SubjectComponentID is not null,CONCAT('-',SubjectComponentID),'')) as realSubjectID,*/
            IF(TermAssessment IS NULL OR TermAssessment = '0', 'T".$targetTermNumber."', TermAssessment) as realTermAssessment
		FROM
			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
		WHERE
		    AcademicYearID IN ('".implode("', '", (array)$AcademicYearID)."') AND
			YearTermID IN ('".implode("', '", (array)$targetTermIDAry)."') AND 
            IF(TermAssessment IS NULL OR TermAssessment = '0', 'T".$targetTermNumber."', TermAssessment) IN ('".implode("', '", (array)$targetAssessmentAry)."') AND
			SubjectID IN ('".implode("', '", (array)$SubjectID)."') AND SubjectComponentID = 0 AND 
			ClassLevelID IN ('".implode("', '", array_keys((array)$allFormIDAry))."') AND YearClassID = 0 ";
$temp = $li->returnResultSet($sql);
$sdAssoc = BuildMultiKeyAssoc($temp, array('realTermAssessment', 'SubjectID', 'AcademicYearID', 'ClassLevelID'), array(), 0);
#### Get SD Mean [END] ####

#### Get Table Header [START] ####
$header = array();
foreach($targetAssessmentAry as $termAssessment) {
    $assessmentHeader = array();
    $assessmentHeader[] = $Lang['SDAS']['ReportComparison']['TableHeader']['Class'];
    $assessmentHeader[] = $Lang['SDAS']['ReportComparison']['TableHeader']['ClassNo'];
    $assessmentHeader[] = $Lang['SDAS']['ReportComparison']['TableHeader']['EnglishName'];
    $assessmentHeader[] = $Lang['SDAS']['ReportComparison']['TableHeader']['ChineseName'];
    $assessmentHeader[] = $Lang['SDAS']['ReportComparison']['TableHeader']['NickName'];
    $assessmentHeader[] = $Lang['SDAS']['ReportComparison']['TableHeader']['Subject'];
    
    $nextYear = '';
    $yearColAry = array();
    foreach($yearAssoc as $academicYearID => $yearInfo) {
        $currentYear = $yearInfo['YearNameEN'];
        if($nextYear != '') {
            $yearColAry[] = array($nextYear, $currentYear);
        }
        $nextYear = $currentYear;
    }
    
    $assessmentDisplay = $assessmentAssoc[$termAssessment];
    foreach($yearColAry as $thisYearCol) {
        foreach($compareColumn as $thisCompareType){
            $compareTypeDisplay = $compareTypeAssoc[$thisCompareType];
            $yearResultHeaderStr = $Lang['SDAS']['ReportComparison']['TableHeader']['DataInYear'];
            $yearResultHeaderStr = str_replace('<!--DataType-->', $compareTypeDisplay, str_replace('<!--AssessmentType-->', $assessmentDisplay, $yearResultHeaderStr));
            
            $assessmentHeader[] = str_replace('<!--Year-->', $thisYearCol[0], $yearResultHeaderStr);
            $assessmentHeader[] = str_replace('<!--Year-->', $thisYearCol[1], $yearResultHeaderStr);
            $assessmentHeader[] = str_replace('<!--DataType-->', strtolower($compareTypeDisplay), str_replace('<!--AssessmentType-->', $assessmentDisplay, $Lang['SDAS']['ReportComparison']['TableHeader']['ChangeInYear']));
        }
    }
    $header[$termAssessment] = $assessmentHeader;
}
#### Get Table Header [END] ####

#### Get Table Data [START] ####
$dataAry = array();
$statDataAry = array();
foreach($targetAssessmentAry as $termAssessment) {
    foreach($subjectAssoc as $subjectID => $subjectInfo) {
        foreach($studentAssoc as $studentID => $studentInfo) {
            if(isset($studentGroupAssoc)) {
                if(empty($studentGroupAssoc[$studentID]) || !in_array($subjectID, (array)$studentGroupAssoc[$studentID])) {
                    continue;
                }
            }
            
            $resultAry = array();
            foreach($yearAssoc as $academicYearID => $yearInfo)
            {
                $studentExamScore = $emptySymbol;
                $studentExamPercentile = $emptySymbol;
                $studentExamSD = $emptySymbol;
                
                $studentExamResult = $examAssoc[$termAssessment][$subjectID][$studentID][$academicYearID];
                if(!empty($studentExamResult))
                {
                    $studentExamScore = $studentExamResult['Score'] >= 0 ? $studentExamResult['Score'] : $emptySymbol;
                    $studentExamFormOrder = $studentExamResult['OrderMeritForm'];
                    $studentExamFormTotal = $studentExamResult['OrderMeritFormTotal'];
                    if($studentExamScore != $emptySymbol && $studentExamFormOrder > 0 && $studentExamFormTotal > 0) {
                        $studentExamPercentile = round(($studentExamFormOrder / $studentExamFormTotal * 100), 2).'%';
                    }
                    
                    $studentFormId = $studentFormAssoc[$academicYearID][$studentID][0];
                    $formStatResult = $sdAssoc[$termAssessment][$subjectID][$academicYearID][$studentFormId];
                    if($studentFormId && !empty($formStatResult))
                    {
                        $formSD = $formStatResult['SD'];
                        $formMean = $formStatResult['MEAN'];
                        if($studentExamScore != $emptySymbol && $formSD > 0 && $formMean > 0){
                            $studentExamSD = round(($studentExamScore - $formMean) / $formSD, 2);
                        }
                    }
                }
                
                if(isset($compareTypeAssoc['score'])) {
                    $resultAry['Score'][] = $studentExamScore;
                }
                if(isset($compareTypeAssoc['sd'])) {
                    $resultAry['SD'][] = $studentExamSD;
                }
                if(isset($compareTypeAssoc['percentile'])) {
                    $resultAry['Percentile'][] = $studentExamPercentile;
                }
            }
            
            $compareResultAry = array();
            for($i=0; $i < (count($yearAssoc) - 1); $i++) {
                foreach($resultAry as $result_type => $resultTypeAry) {
                    $currentResult = $resultTypeAry[$i];
                    $previousResult = $resultTypeAry[($i + 1)];
                    
                    $compareResultAry[] = $currentResult;
                    $compareResultAry[] = $previousResult;
                    if($currentResult != $emptySymbol && $previousResult != $emptySymbol) {
                        if($result_type == 'Score' || $result_type == 'SD') {
                            $compareResult = $currentResult - $previousResult;
                            $compareResultAry[] = $currentResult - $previousResult;
                        }
//                         else if($result_type == 'SD') {
//                             if($currentResult > 0 && $previousResult > 0) {
                                
//                             } else if($currentResult > 0 && $previousResult > 0) {
                                
//                             }
//                             $compareResultAry[] = ($currentResult - $previousResult).'%';
//                         }
                        else if($result_type == 'Percentile') {
                            $compareResult = $previousResult - $currentResult;
                            $compareResultAry[] = ($previousResult - $currentResult).'%';
                        }
                    } else {
                        $compareResultAry[] = $emptySymbol;
                    }
                }
            }
            $dataAry[$termAssessment][$subjectID][$studentID] = $compareResultAry;
        }
        $statDataAry[$termAssessment][$subjectID] = calculateDataStat($dataAry[$termAssessment][$subjectID]);
    }
}
#### Get Table Data [END] ####

######## Get Data [END] ########

######## Get Table [START] ########
$x = "";
$x .= "<div class='chart_tables'>";

$isFirstTable = true;
foreach($targetAssessmentAry as $termAssessment)
{
    $x .= "<table class='common_table_list_v30 view_table_list_v30' id='resultTable'>";
    if(isset($dataAry[$termAssessment]))
    {
        $x .= "<thead>";
            $x .= "<tr class='".($isFirstTable? '' : 'display_table_header')."'>";
        	foreach($header[$termAssessment] as $_header) {
        	    $x .= "<th colspan=".$YearColSpan." style='white-space: nowrap;'>".$_header."</th>";
        	}
        	$x .= "</tr>";
    	$x .= "</thead>";
    	
    	$x .= "<tbody>";
    	if(!$isFirstTable) {
        	$x .= "<tr class='export_table_header' style='display:none'>";
        	   $x .= "<td>".$assessmentAssoc[$termAssessment]."</td>";
        	$x .= "</tr>";
        	$x .= "<tr class='export_table_header' style='display:none'>";
        	foreach($header[$termAssessment] as $_header) {
        	    $x .= "<td colspan=".$YearColSpan." style='white-space: nowrap;'>".$_header."</td>";
        	}
        	$x .= "</tr>";
        }
    	if(!empty($dataAry[$termAssessment]))
    	{
    	    $_isFirstRow = true;
    	    $_studentRowSpan = $isSingleStudentMode? count((array)$dataAry[$termAssessment]) : 1;
        	foreach($dataAry[$termAssessment] as $subjectID => $_subjectDataAry)
        	{
        	    $_subjectName = $subjectAssoc[$subjectID]['EN_SNAME'];
        	    $_subjectRowSpan = count((array)$_subjectDataAry);
        	    foreach((array)$_subjectDataAry as $studentID => $_studentDataAry)
        	    {
        	        $_studentInfo = $studentAssoc[$studentID];
        	        
        	        $x .= "</tr>";
                    if(!$isSingleStudentMode || $_isFirstRow) {
            	        $x .= "<td rowspan=".$_studentRowSpan.">".$_studentInfo['ClassTitleEN']."</td>";
            	        $x .= "<td rowspan=".$_studentRowSpan.">".$_studentInfo['ClassNumber']."</td>";
            	        $x .= "<td rowspan=".$_studentRowSpan.">".$_studentInfo['EnglishName']."</td>";
            	        $x .= "<td rowspan=".$_studentRowSpan.">".$_studentInfo['ChineseName']."</td>";
            	        $x .= "<td rowspan=".$_studentRowSpan.">".$_studentInfo['NickName']."</td>";
                    }
        	        if($_subjectName != '') {
        	            $x .= "<td rowspan=".$_subjectRowSpan." style='white-space: nowrap;'>".$_subjectName."</td>";
                        $_subjectName = '';
        	        }
        	        
        	        foreach((array)$_studentDataAry as $_dataAry) {
        	            $x .= "<td colspan=".$YearColSpan." style='white-space: nowrap;'>".$_dataAry."</td>";
        	        }
        	        $x .= "</tr>";
        	        
        	        $_isFirstRow = false;
        	    }
        	    
        	    if(!$isSingleStudentMode && !empty($statDataAry[$termAssessment][$subjectID]))
        	    {
        	        $x .= "</tr>";
            	        $x .= "<td colspan='6'>&nbsp;</td>";
            	        foreach((array)$_studentDataAry as $_dataIndex => $_dataAry) {
            	            $stat_data = isset($statDataAry[$termAssessment][$subjectID][$_dataIndex])? $statDataAry[$termAssessment][$subjectID][$_dataIndex] : $emptySymbol;
            	            $x .= "<td>".$stat_data."</td>";
            	        }
        	        $x .= "</tr>";
        	    }
            }
    	} else {
    	    $x .= "<tr>";
    	       $x .= "<td>".$Lang['General']['NoRecordFound']."</td>";
    	    $x .= "</tr>";
    	}
        $x .= "</tbody>";
    }
    $x .= "</table>";
    $x .= "<br/>";
    
    $isFirstTable = false;
}

$x .= "</div>";
##### Get Table [END] #####

##### Get Style [START] #####
$x .= '<style>
            table.view_table_list_v30 th { white-space: nowrap; },
    	    table.view_table_list_v30 td { white-space: nowrap; }
        </style>';
##### Get Style [END] #####

?>

<!-- -------- Generate table START -------- -->
<?php 
echo $x;
?>
<script>$(function() {$('#resultTable').floatHeader();});</script>
<!-- -------- Generate table END -------- -->

<?php
intranet_closedb();
?>