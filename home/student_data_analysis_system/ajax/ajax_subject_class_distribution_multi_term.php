<?php
//Using: 
/**
 * Change Log
 * 2020-09-11 (Philips) [DM#3792]
 *  -	Add Default Color for distribution chart
 * 2020-07-29 (Philips)
 *  -	Added ksort to improve ordering of chart 2
 * 2020-05-04 (Philips) [2019-0710-1725-23085]
 *  -   Added distribution chart
 * 2020-01-08 (Philips) [2019-1218-1255-37206]
 *  -	Fixed: $sys_custom['SDAS']['SKHTST']['CustFullMarks'] change Subject Full Mark into 100
 * 2019-09-10 (Bill)    [2019-0710-1725-23085]
 *  -   Added Highest Marks & Lowest Marks in Data Table
 * 2019-07-25 (Bill)    [2019-0312-1523-03073]
 *  -   Added cust standard percentage range    ($sys_custom['SDAS']['Subject_Class_Distribution']['StandardArr'])
 * 2017-12-12 (Pun)
 *  -   Added cust T1A3 hardcode full mark
 *  -   Added Overall option to subject selection
 * 2017-08-14 Villa
 * 	-	Modified include file
 * 2017-06-01 Villa
 *  -	Modified  if Subject Full Mark has not been set checking to "default set the subject fullmark to 100, show the warning if and only if the highscore of the data>Subject Full Mark"
 * 2017-05-12 Villa
 *  -	Modified HighChart Table behaviour
 * 2017-05-10 Villa #E116806 
 * 	-	Fix PassingRate Problem
 * 2017-04-10 Villa
 *  -	Add ThickBox Related (spline)
 * 2017-04-07 Villa
 *  -	Add ThickBox Related (columnn)
 * 2017-03-24 Villa #T115142 
 *  -	Modified $scoreTitle : Change Wording
 *  2017-03-24 Villa #Y114949 
 *  -	break this file and return warning msg if Subject Full has not been set
 *  -	Change the no Result Wording
 * 2017-01-12 Villa
 *  - 	Support Subject Component
 * 2017-01-09 Villa
 *  -	add item Total Students, Average Score, Pass Percentage
 * 2016-12-23 Villa
 *  -	modified the range ditribution codong
 * 2016-12-14 Villa
 *  - 	Add baseOn/ bias mechanism 
 *  - 	modified GroupBy - reduce sql query
 * 2016-12-13 Villa
 *  -	modified maxscore getting from the db
 * 2016-12-06 Villa
 *  -	change the display for Spline Diagram
 * 2016-12-02 Villa
 *  -	Fix warning : division by zero
 * 2016-11-28 Villa
 * 	-	Sync the table Color with other tb
 * 2016-11-16 Villa
 * 	-	New File
 */

$PATH_WRT_ROOT = "../../../";
include_once($intranet_root."/includes/libpf-asr.php");

intranet_auth();
intranet_opendb();

$fcm = new form_class_manage();
$lpf = new libportfolio();
$json = new JSON_obj();

$YearTermID_Org = $YearTermID;

########## Access Right START ##########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$canAccess = true;
if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) && 
		in_array('form_student_performance',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
	$accessRight['admin'] = 1;
}
$currentAcademicYearID = Get_Current_Academic_Year_ID();
if(!$accessRight['admin']){
	if( count($accessRight['subjectPanel']) ){
		#### Subject Panel access right START ####
		
		$canAccess = true; // Subject Panel can always access this page
		$filterSubjectArr = array_keys($accessRight['subjectPanel']);
		
		#### Subject Panel access right END ####
	}else{
		#### Class teacher access right START ####
		
		if($academicYearID != $currentAcademicYearID){
			$canAccess = false;
		}
		$yearClassIdArr = array();
		foreach($accessRight['classTeacher'] as $yearClass){
		    $yearClassIdArr[] = $yearClass['YearClassID'];
		}
		foreach($accessRight['subjectGroup'] as $subjectGroup){
		    $sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
		    $studentList = $sGroup->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=1, $showClassID = true);
		    $classList = Get_Array_By_Key($studentList, "YearClassID");
		    $yearClassIdArr = array_merge($yearClassIdArr, $classList);
		}
		foreach((array)$YearClassID as $id){
		    if(!in_array($id, $yearClassIdArr)){
		        $canAccess = false;
		        break;
		    }
		}
		
		#### Class teacher access right END ####
	}
	
	if(!$canAccess){
		echo 'Access Denied.';
		die;
	}
}
########## Access Right END ##########

$li = new libdb();
$li_pf = new libpf_asr();
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $academicYearID);
$yearTermMaping = array();
foreach($academic_yearterm_arr as $ytInfo) $yearTermMaping[$ytInfo[0]] = $ytInfo[1];
echo $linterface->Include_Thickbox_JS_CSS();

######## Get Data START ########
#### Get YearID for class teacher START ####
if($yearID == ''){
	$objYearClass = new year_class($YearClassID[0]);
	$yearID = $objYearClass->YearID;
}
#### Get YearID for class teacher END ####

#### Get Class Name START ####
$rs = $fcm->Get_Class_List($academicYearID,$yearID);
$allClassArr = BuildMultiKeyAssoc($rs, array('YearClassID') , array('ClassTitleEN', 'ClassTitleB5'));
$classNameArr = array();
foreach($YearClassID as $class){
	$classNameArr[$class] = Get_Lang_Selection($allClassArr[$class]['ClassTitleB5'], $allClassArr[$class]['ClassTitleEN']);

}


#### Get Class Name END ####


// Assessment data retrieval
$subjMap_arr = array();
$temp = explode('_',$Subjects);
$MainSubject = $temp[0];
$CmpSubject = $temp[1];
if($CmpSubject){
	$isCMP= true;
}else{
	$isCMP= false;
}
	  $YearClassID_sql = implode(',', $YearClassID);
	  $groupCheckArr = array();
  foreach((array)$YearTermAry as $YearTermID){
  	$ytID = $YearTermID;
	  	if (strstr($YearTermID, "_"))
	  	{
	  		$tmpArr = explode("_", $YearTermID);
	  		$YearTermID = (int) $tmpArr[0];
	  		$TermAssessment = trim($tmpArr[1]);
	  	} else {
	  		$TermAssessment = '';
	  	}
  	$conds = "";
// 	  $conds .= ($_compSubj == "") ? "AND IFNULL(assr.SubjectComponentCode, '') = '' " : "AND assr.SubjectComponentCode = '{$_compSubj}' ";
	  $conds .= ($YearTermID == "") ? "AND IFNULL(assr.YearTermID, 0) = 0 " : "AND assr.YearTermID = {$YearTermID} ";
	  $conds .= "AND assr.Score <> -1 ";
	  $conds .= "AND yc.YearClassID in (".$YearClassID_sql.") ";
// 	  $conds .= "AND SubjectID={$Subjects} ";
	  
	  if($MainSubject == 0){
	      $sql = "SELECT assr.ClassName, assr.Score AS avgScore, assr.YearClassID, '0' AS SubjectID, '0' AS SubjectComponentID, assr.UserID ";
	      $sql .= "FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD assr ";
	  }else{
	      $sql = "SELECT assr.ClassName, assr.Score AS avgScore, assr.YearClassID, assr.SubjectID, assr.SubjectComponentID, assr.UserID ";
	      $sql .= "FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr ";
	      
    	  $conds .= " AND assr.SubjectID = $MainSubject ";
    	  if($isCMP){
    	  	$conds .= " AND assr.SubjectComponentID = $CmpSubject ";
    	  }else{
    	  	$conds .= " AND  assr.SubjectComponentID IS NULL ";
    	  }
	  }
	  
	  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON assr.UserID = ycu.UserID ";
	  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID ";
	  $sql .= "WHERE assr.AcademicYearID = {$academicYearID} AND yc.AcademicYearID = {$academicYearID} AND yc.YearID = {$yearID} ";
	  $sql .= $conds;
	  $sql .= "AND IF(assr.Score > 0, true, IF(assr.Score < 0, false, IFNULL(assr.Grade, '') <> '')) ";
	 
	  if ($TermAssessment=="")
	  {
	  	$sql .= " AND assr.TermAssessment IS NULL ";
	  }else{
	  	$sql .= " AND assr.TermAssessment = '".addslashes($TermAssessment)."' ";
	  }
  $rs = $li->returnResultSet($sql);
  $classArr = Get_Array_By_Key($rs, 'ClassName');
  $classArr = array_unique($classArr);
  $j=0;
  $groupBy = array();
  foreach ($classArr as $_classArr){
  	if(!in_array($_classArr, $groupCheckArr)){
  		$groupCheckArr[] = $_classArr;
  	}
  	$groupBy[$j]['ClassName'] = $_classArr;
  	$j++;
  }
	if($isCMP){
 		$SubID = $CmpSubject;
 	}else{
 		$SubID = $MainSubject;
 	}
  $sql = "SELECT 
  			FullMarkInt,PassMarkInt
  		FROM 
  			{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK 
  		WHERE 
  				AcademicYearID='{$academicYearID}'
  			AND
  				SubjectID='{$SubID}'
  			AND
  				YearID='{$yearID}'";
  $temp = $li->returnResultSet($sql);
  $temp2 = Get_Array_By_Key($temp, 'PassMarkInt');
  $temp = Get_Array_By_Key($temp, 'FullMarkInt');
  $SubjectFullMark = $temp[0]? $temp[0]:100;
 
  $maxScore = $temp[0]? $temp[0]:'100';
  $maxScore=$objSDAS->checkZeroDot($maxScore);
  $PassScore = $temp2[0]? $temp2[0]:$maxScore/2;
  $PassScore=$objSDAS->checkZeroDot($PassScore);
  
  if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
    $objYear = new Year($yearID);
  	if(
  	    ($TermAssessment == 'T1A3' && strpos($objYear->YearName, '6') !== false) || 
  	    ($TermAssessment == 'T2A3') 
  	){
  		$maxScore = '100';
  		// 2020-01-08 (Philips) [2019-1218-1255-37206] - fixed
  		$SubjectFullMark = 100;
  		$PassScore = '50';
	}
  }
  
  if($base_checkbox){
  	$bias = 100/$maxScore;
  	$maxScore = 100;
  	$percent_symbol = '%';
//   	$PassScore = $PassScore*$bias;
  }else{
  	$bias = 1;
  	$percent_symbol = '';
  }

  // [2019-0312-1523-03073]
  if(isset($sys_custom['SDAS']['Subject_Class_Distribution']['StandardArr'])){
    $standardArr = $sys_custom['SDAS']['Subject_Class_Distribution']['StandardArr'];
  }
  //$standardArr = array(85,75,60,30,0);

  $PassScore = $PassScore*$bias;
  $parts = sizeof($standardArr);
  //if no set $standardArr
  if($parts==0){
  	$parts = '10';
  	$division = $maxScore/$parts;
  	$division=$objSDAS->checkZeroDot($division);
  	for($i=$parts-1;$i>-1;$i--){
  		$standardArr = array_merge((array)$standardArr,array($division*$i));
  	}
  }
  $standardArr = array_merge(array($maxScore),$standardArr);
  if($chartType=='spline'){
  	sort($standardArr);
  }
  $parts = sizeof($standardArr);

  // Get the HighScore
  $HighestScore = 0;
  foreach ($groupBy as $value)
  {
  	unset($result[$value['ClassName']]);
  	unset($studentID_ary[$value['ClassName']]);
  	$result[$value['ClassName']]['total']=0;
  	foreach ($rs as $value2){
  		if($value['ClassName']==$value2['ClassName']){
  			if($value2['avgScore']>$HighestScore){
  				$HighestScore = $value2['avgScore'];
  			}
  			$value2['avgScore']=$value2['avgScore']*$bias; // bias

            // [2019-0710-1725-23085] Class Highest Marks & Lowest Marks
            if(!isset($result[$value['ClassName']]['highest']) || (isset($result[$value['ClassName']]['highest'])&&$value2['avgScore']>$result[$value['ClassName']]['highest'])) {
                $result[$value['ClassName']]['highest'] = $value2['avgScore'];
            }
            if(!isset($result[$value['ClassName']]['lowest']) || (isset($result[$value['ClassName']]['lowest'])&&$value2['avgScore']<$result[$value['ClassName']]['lowest'])) {
                $result[$value['ClassName']]['lowest'] = $value2['avgScore'];
            }

  			if($chartType=='spline'){ //chartType revser the data
  				for($i=$parts;$i>0;$i--){
  					$j=$i-1;
  					if($i=='1'){
  						if($value2['avgScore']<=$standardArr[$i]){
  							$result[$value['ClassName']][$i][] = $value2['avgScore'];
  							$studentID_ary[$value['ClassName']][$i][] = $value2['UserID'];
  						}
  					}elseif($i=='0'){
  						//do nothing
  					}else{
  						if($standardArr[$i]>=$value2['avgScore']&&$value2['avgScore']>$standardArr[$j]){
  							$result[$value['ClassName']][$i][] = $value2['avgScore'];
  							$studentID_ary[$value['ClassName']][$i][] = $value2['UserID'];
  						}
  					}
  				}
  			}else{
	  			for($i=0;$i<$parts;$i++){
	  				$j=$i+1;
	  				if($i==($parts-1)){
	  					if($value2['avgScore']<=$standardArr[$i]){
	  						$result[$value['ClassName']][$i][] = $value2['avgScore'];
	  						$studentID_ary[$value['ClassName']][$i][] = $value2['UserID'];
	  					}
	  				}elseif($i==$parts){
	  					//do nothing
	  				}else{
	  					if($standardArr[$i]>=$value2['avgScore']&&$value2['avgScore']>$standardArr[$j]){
	  						$result[$value['ClassName']][$i][] = $value2['avgScore'];
	  						$studentID_ary[$value['ClassName']][$i][] = $value2['UserID'];
	  					}
	  				}
	  			}
  			}
  			$result[$value['ClassName']]['total']=$result[$value['ClassName']]['total']+1;
  		}
  	}
  }
  $array = array();
  foreach ($groupBy as $_groupBy){
  	$tempMark = 0;
  	foreach((array)$result[$_groupBy['ClassName']] as $key=>$value){
        // if($key!=='total'){
  		if($key!=='total' && $key!=='lowest' && $key!=='highest'){
  			$finalResult[$_groupBy['ClassName'] . ' ' . $yearTermMaping[$ytID]][$key]['number'] = sizeof($result[$_groupBy['ClassName']][$key]);
  			$finalResult[$_groupBy['ClassName'] . ' ' . $yearTermMaping[$ytID]][$key]['studentIDAry'] = $studentID_ary[$_groupBy['ClassName']][$key];
  			$finalResult[$_groupBy['ClassName'] . ' ' . $yearTermMaping[$ytID]][$Lang['SDAS']['WholeForm']]['studentIDAry'] = array_merge((array)$finalResult[$_groupBy['ClassName'] . ' ' . $yearTermMaping[$ytID]][$Lang['SDAS']['WholeForm']]['studentIDAry'],(array)$finalResult[$_groupBy['ClassName'] . ' ' . $yearTermMaping[$ytID]][$key]['studentIDAry']);
	  		foreach ((array)$value as $_value){
	  			$tempMark += $_value;
	  			if($_value >= $PassScore){
	  				$finalResult[$_groupBy['ClassName'] . ' ' . $yearTermMaping[$ytID]]['Pass'][] = $_value;
	  			}
	  			else{
// 	  				debug_pr($_value);
	  			}
	  		}
  		} else {
  			$finalResult[$_groupBy['ClassName'] . ' ' . $yearTermMaping[$ytID]][$key] = $result[$_groupBy['ClassName']][$key];
  		}
  	}
  	$finalResult[$_groupBy['ClassName'] . ' ' . $yearTermMaping[$ytID]]['TotalMarks'] = $tempMark;
  }
  }
  $groupBy = array();
  foreach($groupCheckArr as $group){
  	$newArray = array();
  	$newArray['ClassName'] = $group;
  	$groupBy[] = $newArray;
  }
//   debug_pr($finalResult);die();
  //break the display if no data 
if(empty($finalResult)){
	$x = "	<div class='chart_tables_2'>
				<table class='common_table_list_v30 view_table_list_v30'>
					<thead>
						<tr>
							<td align='center'>
								".$Lang['SDAS']['NoRecord']['A']."
							</td>
						</tr>
					<thead>
				</table>
			</div>";
	echo $x;
	return false;
}
//Break the display if the full marks is incorrect
if($HighestScore>$SubjectFullMark){
	$x = "	<div class='chart_tables_2'>
				<table class='common_table_list_v30 view_table_list_v30'>
					<thead>
						<tr>
				  			<td align='center'>
				  				".$Lang['SDAS']['FullMarkSetting']['FullMarkNoSet']."
				  			</td>
			  			</tr>
		  			</thead>
	  			</table>
  			</div>";
	echo $x;
	return false;
}
######## Get Data END ########
######## Table Data START ########
if($option_checkbox||$chartType=='column'){
	$filter = $parts-1;
}else{
	$filter = 0;
}
$x = "";
$x .="<div class='chart_tables'>";
$x .="<table class='common_table_list_v30 view_table_list_v30'>";

if($chartType=='column'){
	$chartyAxisMax = 100;
	$chartyAxisTitle = $Lang['SDAS']['Amount'].$iPort['report_col']['improved_by_percentage'];
	### Table Head Start ### 
	$x .="<thead>";
	$x .= "<tr>";
	$x .= "<th style='width: 20%; white-space: nowrap;'>"." "."</th>";
	
	foreach((array)$YearTermAry as $ytID){
		$x .= "<th >".$Lang['SDAS']['WholeForm']. ' ' . $yearTermMaping[$ytID]."</th>";
	}
	foreach((array)$YearTermAry as $ytID){
		foreach($groupBy as $key){
			$x.= "<th>".$key['ClassName'] . ' ' . $yearTermMaping[$ytID]."</th>";
		}
	}
	$x .= "</tr>";
	$x .= "</thead>";
	### Table Head END ###
	### Table Body Start ###
	$x .= "<tbody>";
	
	for($i=0;$i<$parts;$i++){
		$score_all = 0;
		if($i!==$filter){
			if($SDAS['SKHTST_CUST']){
				$x .="<tr><td>".$standardArr[$i].$percent_symbol."</td>";
			}else{
				$last = isset($standardArr[$i+1])?false:true;
				if(!$last){
					if($standardArr[$i+1]){
// 						$scoreTitle = $standardArr[$i].$percent_symbol."-".($standardArr[$i+1]+0.1).$percent_symbol;
						$scoreTitle = ($standardArr[$i+1]+0.1).$percent_symbol."-".$standardArr[$i].$percent_symbol;
					}else{
// 						$scoreTitle = $standardArr[$i].$percent_symbol."-".($standardArr[$i+1]).$percent_symbol;
						$scoreTitle = ($standardArr[$i+1]).$percent_symbol."-".$standardArr[$i].$percent_symbol;
					}
					
					$x .="<tr><td>".$scoreTitle."</td>";
				}else{
// 					$scoreTitle = $standardArr[$i].$percent_symbol;
// 					$x .="<tr><td>".$scoreTitle."</td>";
				}
			}
		
			$data[$i]['name']=(string)$scoreTitle;
			$categories = array();
			//Score ALL Start
			foreach((array)$YearTermAry as $ytID){
				$StudentID_Pass_Ary = array();
				$StudentID_Pass = '';
				$numberOfStudentAll = 0;
				foreach ($groupBy as $key){
					if(empty($finalResult[$key['ClassName'] . ' ' . $yearTermMaping[$ytID]])) continue;
	// 				$score = $finalResult[$key['ClassName'] . ' ' . $yearTermMaping[$ytID]][$i]['number'];
	// 				$score = ($score)? $score : '0';
	// 				$score_all = $score_all + $score;
					
					$numberOfStudentAll += $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['total'];
					
					$StudentID_Pass_Ary =  array_merge((array)$finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['studentIDAry'],(array)$StudentID_Pass_Ary);
				}
				$StudentID_Pass_Ary = array_unique($StudentID_Pass_Ary);
				$score_all = count($StudentID_Pass_Ary);
				$StudentID_Pass = implode(",",(array)$StudentID_Pass_Ary);
				$x .= "<td>".$linterface->Get_Thickbox_Link('420', '620',"",$Lang['SDAS']['WholeForm'] .  ' ' . $yearTermMaping[$ytID]."&nbsp;".$data[$i]['name'], "jsOnloadDetailThickBox('$StudentID_Pass','$ytID')","FakeLayer",$score_all)."</td>";
				//HighChart Data for ALL
				$highChartDataAssoAry['meanChart']['xAxisItem'][] = $Lang['SDAS']['WholeForm'].  ' ' . $yearTermMaping[$ytID];
				$categories[$yearTermMaping[$ytID]][] = $Lang['SDAS']['WholeForm'];
				$data[$i]['data'][] = $numberOfStudentAll ? round($score_all*100/$numberOfStudentAll,2) : '0';
			}
			//Score ALL END
			
			foreach((array)$YearTermAry as $ytID){
				foreach ($groupBy as $key){
					$score = $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['number'];
					$score = ($score)? $score : '0';
					#ThickBoxRelated 
					$StudentID_Pass_Ary = array();
					$StudentID_Pass = '';
					$StudentID_Pass_Ary =  $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['studentIDAry'];
					$StudentID_Pass = implode(",",(array)$StudentID_Pass_Ary);
					if($i!==$filter){
						$x .= "<td>".$linterface->Get_Thickbox_Link('420', '620',"",$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]."&nbsp;".$data[$i]['name'], "jsOnloadDetailThickBox('$StudentID_Pass','$ytID')","FakeLayer",$score)."</td>";
					}
					//HighChart Data
					$highChartDataAssoAry['meanChart']['xAxisItem'][] = $key['ClassName'] .  ' ' . $yearTermMaping[$ytID];
					$categories[$yearTermMaping[$ytID]][] = $key['ClassName'];
					$data[$i]['data'][]= round($score*100/$result[$key['ClassName']]['total'],2);
				}
			}
				$x .="</tr>";
		}
		
	}
	//HighChart Setting
	$chartSetting = '<span style="color:{series.color}">{series.name}</span>: <b>{point.y:.2f}%</b><br/>';
	
}elseif($chartType=='spline'){
	$chartType = '';
	$chartyAxisMax = 100;
	$chartyAxisTitle= $Lang['SDAS']['Amount'].$iPort['report_col']['improved_by_percentage'];
	$AddonNumber_ALL = 0;
	### Header ###
	$x .="<thead>";
		$x .= "<tr>";
			$x.= "<th>"." "."</th>";
			for($i=0;$i<$parts;$i++){
				if($i!==$filter){
					if(!$SDAS['SKHTST_CUST']){
// 						$last = isset($standardArr[$i+1])?false:true;
						if($option_checkbox){
							$symbol = ">";
						}else{
							$symbol = "<";
						}
						$symbol .= "=";
					}
					$x.= "<th>".$symbol.$standardArr[$i].$percent_symbol."</th>";
				}
				//highChart Data xAxisItem
				$highChartDataAssoAry['meanChart']['xAxisItem'][] = $standardArr[$i].$percent_symbol;
			}
// 			$x .= "<th>";
// 				$x .= $Lang['SDAS']['totalStudents'];
// 			$x .= "</th>";
			
// 			$x .= "<th>";
// 				$x .= $Lang['SDAS']['averageScore'];
// 			$x .= "</th>";
			
// 			$x .= "<th>";
// 				$x .= $Lang['SDAS']['passPercent'];
// 			$x .= "</th>";
			
		$x .= "</tr>";
	$x .= "</thead>";
	
	### Body ###
	$x .= "<tbody>";
	### Print ALL START ###
	foreach((array)$YearTermAry as $ytID){
		$AddonNumber_ALL = 0;
		$number_ALL = array();
		$percent_ALL = array();
		$AddonNumber_i = 0;
		unset($StudentID_Pass_Ary);
		unset($StudentID_Pass);
		for($i=0;$i<$parts;$i++){
			foreach ($groupBy as $key){
				$number_ALL[$i] =	$number_ALL[$i]+$finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['number'];
				$AddonNumber_ALL = $AddonNumber_ALL+$finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['number'];
				$StudentID_Pass_Ary = array_merge((array)$StudentID_Pass_Ary,(array)$finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['studentIDAry']);
				$StudentID_Pass_Ary_ALL[$i] = $StudentID_Pass_Ary;
			}
			$number_ALL[$i] = $number_ALL[$i] + $AddonNumber_i;
			$AddonNumber_i = $number_ALL[$i];
		}
		for($i=0;$i<$parts;$i++){
			foreach ($groupBy as $key){
				if($AddonNumber_ALL>0){
					$percent_ALL[$i] = $number_ALL[$i]/$AddonNumber_ALL*100;
				}
			}
		}
		$x .="<tr>";
			$x .= "<td>";
			$x .= $Lang['SDAS']['WholeForm'].  ' ' . $yearTermMaping[$ytID];
			$x .= "</td>";
		for($i=0;$i<$parts;$i++){
			$StudentID_Pass = implode(',',(array)$StudentID_Pass_Ary_ALL[$i]);
				$percent_ALL_i = $percent_ALL[$i]? round($percent_ALL[$i]):0;
				if($option_checkbox){	//case reserve data
					$temp = array_diff((array)$StudentID_Pass_Ary, (array)$StudentID_Pass_Ary_ALL[$i]); //all - remaining
					$StudentID_Pass = implode(',',$temp);
					$percent_ALL_i = 100 - $percent_ALL_i;
				}
				if($i!==$filter){
// 					$x .= "<td>";
// 						$x .= $percent_ALL_i."%";
// 					$x .= "</td>";
					$x .= "<td>".$linterface->Get_Thickbox_Link('420', '620',"",$Lang['SDAS']['WholeForm']."&nbsp;".$standardArr[$i].'%', "jsOnloadDetailThickBox('$StudentID_Pass','$ytID')","FakeLayer",$percent_ALL_i.'%')."</td>";
				}
		
			$data[$Lang['SDAS']['WholeForm'].  ' ' . $yearTermMaping[$ytID]]['name']=$Lang['SDAS']['WholeForm'].  ' ' . $yearTermMaping[$ytID];
			$data[$Lang['SDAS']['WholeForm'].  ' ' . $yearTermMaping[$ytID]]['data'][] = $percent_ALL_i;
		}
// 		### Student Number Start###
// 			foreach ($groupBy as $key){
// 				//calculate whole form 
// 				$student_total += $result[$key['ClassName']]['total'];
// 				$totalScore_total += $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['TotalMarks'];
// 				$pass_Total += sizeof($finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['Pass']);
// 			}
// 			### Student Number Start###
// 			$x .= "<td>";
// 				$x .= $student_total;
// 			$x .= "</td>";
// 			### Student Number End###
// 			### Average Score Start###
// 			$x .= "<td>";
// 				$x .= round($totalScore_total/$student_total,2);
// 				if($base_checkbox){
// 					$x .= "%";
// 				}
// 			$x .= "</td>";
// 			### Average Score Start###
// 			### Pass Percentage Start###
// 			$x .= "<td>";
// 				$pass_Total_percentage = round($pass_Total*100/$student_total,2);
// 				$x .= $pass_Total_percentage;
// 				$x .= "%";
// 			$x .= "</td>";
// 			### Pass Percentage End###
// 			$highChartData2[0]['name'] = $Lang['SDAS']['WholeForm'];
// 			$highChartData2[0]['data'][] = $pass_Total_percentage;
			
		$x .="</tr>";
	}
	### Print ALL END ###
	### Print Class Start ###	
	$j=1;
	foreach((array)$YearTermAry as $ytID){
	foreach ($groupBy as $key){
		$highChartData2[$j]['name'] = $key['ClassName'] .  ' ' . $yearTermMaping[$ytID];
		unset($StudentID_Pass_Ary);
		unset($StudentID_Pass);
		$x .= "<tr>";
			$x .= "<td>";
			$x .= $key['ClassName'] .  ' ' . $yearTermMaping[$ytID];
			$x .= "</td>";
			for($i=0;$i<$parts;$i++){
				$StudentID_Pass_Ary = array_merge((array)$StudentID_Pass_Ary,(array)$finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['studentIDAry']);
				$StudentID_Pass = implode(',',(array)$StudentID_Pass_Ary);
				$number = $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['number'];
				$AddonNumber[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]] = $number+$AddonNumber[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]];
				if($result[$key['ClassName']]['total']>0){ //avoid division by zero
					$percent= $AddonNumber[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]/$result[$key['ClassName']]['total'] *100;
				}
				if($option_checkbox){	//case reserve data
					$temp = array_diff((array)$finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$Lang['SDAS']['WholeForm']]['studentIDAry'], (array)$StudentID_Pass_Ary);
					$StudentID_Pass = implode(',',$temp);
					$percent = 100 - $percent;
				}
				$percent = $percent? round($percent):0;
				if($i!==$filter){
// 					$x .= "<td>";
					$x .= "<td>".$linterface->Get_Thickbox_Link('420', '620',"",$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]."&nbsp;".$standardArr[$i], "jsOnloadDetailThickBox('$StudentID_Pass')","FakeLayer",$percent.'%')."</td>";
// 						$x .= $percent."%";
// 					$x .= "</td>";
				}
				$data[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['data'][] = $percent;
			}
			$data[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['name']=(string)$key['ClassName'] .  ' ' . $yearTermMaping[$ytID];
// 			### Student Number Start###
// 			$x .= "<td>";
// 				$x .= $result[$key['ClassName']]['total'];
// 			$x .= "</td>";
// 			### Student Number End###
// 			### Average Score Start###
// 			$x .= "<td>";
// 				$x .= round($finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['TotalMarks']/$result[$key['ClassName']]['total'],2);
// 				if($base_checkbox){
// 					$x .= "%";
// 				}
// 			$x .= "</td>";
// 			### Average Score End###
// 			### Pass Percentage Start###
// 			$x .= "<td>";
// 					$pass_percentage = round(sizeof($finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['Pass'])*100/$result[$key['ClassName']]['total'],2);
// 					$x .= $pass_percentage;
// 					$x .= "%";
// 					$highChartData2[$j]['data'][] = $pass_percentage;
// 			$x .= "</td>";
// 			### Pass Percentage End###
		$x .= "</tr>";
		$j++;
	}
	}
	### Print Class END ###
	$chartSetting = '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}%</b><br/>';

}elseif($chartType=='distribution_chart'){
	# 2020-05-04 (Philips) [2019-0710-1725-23085] - Improvement of chart
	$chartType = '';
	$chartyAxisMax = '';
	$chartyAxisTitle = $iPort['report_col']['improved_by_percentage'].$Lang['SDAS']['Amount'];
	
	### Header ###
	$x .="<thead>";
	$x .= "<tr>";
	$x.= "<th>"." "."</th>";
// 	debug_pr($standardArr);
	if(!$option_checkbox){ // for correct display order
		$standardArr = array_reverse($standardArr);
	}
	for($i=0;$i<$parts;$i++){
		if($i!==$filter){
			if(!$SDAS['SKHTST_CUST']){
				// 						$last = isset($standardArr[$i+1])?false:true;
				if($option_checkbox){
					$symbol = ">";
				}else{
					$symbol = "<";
				}
				$symbol .= "=";
			}
			$x.= "<th>".$symbol.$standardArr[$i].$percent_symbol."</th>";
		}
		//highChart Data xAxisItem
		$highChartDataAssoAry['meanChart']['xAxisItem'][] = $standardArr[$i].$percent_symbol;
	}
	$x .= "</tr>";
	$x .= "</thead>";
	### Print ALL END ###
	### Print Class Start ###
	$x .="<tbody>";
	$j=1;
	foreach((array)$YearTermAry as $ytID){
	foreach ($groupBy as $key){
		$highChartData2[$j]['name'] = $key['ClassName'];
		unset($StudentID_Pass_Ary);
		unset($StudentID_Pass);
		$x .= "<tr>";
		$x .= "<td>";
		$x .= $key['ClassName'] .  ' ' . $yearTermMaping[$ytID];
		$x .= "</td>";
		for($k=0;$k<$parts;$k++){
			if($option_checkbox){  // for correct display order
				$i = $k;
			} else {
				$i = $parts - 1 - $k;
				$filter = $parts - 1;
			}
			$StudentID_Pass_Ary = array_merge((array)$StudentID_Pass_Ary,(array)$finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['studentIDAry']);
			$StudentID_Pass = implode(',',(array)$StudentID_Pass_Ary);
			$number = $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]][$i]['number'];
			if($i!==$filter){
				$x .= "<td>".$linterface->Get_Thickbox_Link('420', '620',"",$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]."&nbsp;".$standardArr[$k], "jsOnloadDetailThickBox('$StudentID_Pass')","FakeLayer",$number ? $number : '0')."</td>";
			}
			$data[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['data'][] = $number ? $number : 0;
		}
// 		if(!$option_checkbox){
// 			$data[$key['ClassName']]['data'] = array_reverse($data[$key['ClassName']]['data']);
// 		}
		$data[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['name']=(string)$key['ClassName'] .  ' ' . $yearTermMaping[$ytID];
		$x .= "</tr>";
		$j++;
	}
	}
	$ChartColorAry = 'colors: ["#91e8e1","#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b"],';
	
	### Print Class END ###
	$chartSetting = '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>';
	
}
		$x .= "</tbody>";
	$x .= "</table>";
$x .= "</div>";

######## Table Data END ########
######## Table2 Data Start ########
$table2 = "";
$table2 .="<div class='chart_tables'>";
	$table2 .="<table class='common_table_list_v30 view_table_list_v30'>";
		### Table2 Head Start ###
		$table2 .="<thead>";
			$table2 .= "<tr>";
			$table2 .= "<th style='width: 20%; white-space: nowrap;'>"." "."</th>";
			foreach((array)$YearTermAry as $ytID){
				$table2 .= "<th width=>".$Lang['SDAS']['WholeForm']. ' ' . $yearTermMaping[$ytID]."</th>";
			}
			foreach((array)$YearTermAry as $ytID){
			foreach($groupBy as $key){
				$table2.= "<th>".$key['ClassName']. ' ' . $yearTermMaping[$ytID]."</th>";
			}
			}
			$table2 .= "</tr>";
		$table2 .= "</thead>";
		### Table2 Head END ###
		### Table Body Start ###
		$table2 .= "<tbody>";
			### Student Number Start###
			
			$table2 .= "<tr>";
				$table2 .= "<td>";
					$table2 .= $Lang['SDAS']['totalStudents'];
				$table2 .= "</td>";
				$student_totalAry = array();
				foreach((array)$YearTermAry as $ytID){
					$student_total = 0;
					foreach ($groupBy as $key){
						$student_total += $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['total'];
					}
					$table2 .= "<td>";
						$table2 .= $student_total;
					$table2 .= "</td>";
					$student_totalAry[$ytID] = $student_total;
				}
				foreach((array)$YearTermAry as $ytID){
				foreach ($groupBy as $key){
					$table2 .= "<td>";
					$studentTotal = $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['total'];
					$table2 .= $studentTotal ? $studentTotal : '0';
					$table2 .= "</td>";
				}
				}
			$table2 .= "</tr>";
			### Student Number End###
			
			### Average Start###
			$table2 .= "<tr>";
				$table2 .= "<td>";
					$table2 .= $Lang['SDAS']['averageScore'] ;
				$table2 .= "</td>";
				foreach((array)$YearTermAry as $ytID){
					$totalScore_total = 0;
					foreach ($groupBy as $key){
						$totalScore_total += $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['TotalMarks'];
					}
					$table2 .= "<td>";
						$table2 .=  $student_totalAry[$ytID] ? round($totalScore_total/$student_totalAry[$ytID],2) : '0';
						if($base_checkbox){
							$table2 .= "%";
						}
					$table2 .= "</td>";
				}
				foreach((array)$YearTermAry as $ytID){
				foreach ($groupBy as $key){
					$table2 .= "<td>";
						$table2 .= $finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['total'] ? round($finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['TotalMarks']/$finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['total'],2) : '0';
						if($base_checkbox){
							$table2 .= "%";
						}
					$table2 .= "</td>";
				}
				}
			$table2 .= "</tr>";
			### Average End###
			
			### Tatal Pass Start###
			$table2 .= "<tr>";
				$table2 .= "<td>";
					$table2 .= $Lang['SDAS']['passPercent'];
				$table2 .= "</td>";
				$highChartData2[0]['name'] = $Lang['SDAS']['WholeForm'];
				// 	$highChartDataAssoAry2['meanChart']['xAxisItem'][] = "ALL";
				$i = 1;
				foreach((array)$YearTermAry as $ytID){
					$pass_Total = 0;
					foreach ($groupBy as $key){
						$highChartData2[$i]['name'] = $key['ClassName'].  ' ' . $yearTermMaping[$ytID];
						// 		$highChartDataAssoAry2['meanChart']['xAxisItem'][] = $key['ClassName'];
						$pass_Total += sizeof($finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['Pass']);
						$i++;
					}
					$table2 .= "<td>";
						$pass_Total_percentage = $student_totalAry[$ytID] ? round($pass_Total*100/$student_totalAry[$ytID],2) : '0';
						$table2 .= $pass_Total_percentage;
						$table2 .= "%";
					$table2 .= "</td>";
				}
				$highChartData2[0]['data'][] = $pass_Total_percentage;
				$i = 1;
				foreach((array)$YearTermAry as $ytID){
				foreach ($groupBy as $key){
					$table2 .= "<td>";
						$pass_percentage = round(sizeof($finalResult[$key['ClassName'] .  ' ' . $yearTermMaping[$ytID]]['Pass'])*100/$result[$key['ClassName']]['total'],2);
						$table2 .= $pass_percentage;
						$table2 .= "%";
						$highChartData2[$i]['data'][] = $pass_percentage;
					$table2 .= "</td>";
					$i++;
				}
				}
			$table2 .= "</tr>";
			### Tatal Pass END###

            ### Highest Marks Start###
            if($display_checkbox_data['max']) {
                $table2 .= "<tr>";
                    $table2 .= "<td>";
                        $table2 .= $iPort["HighestMark"] ;
                    $table2 .= "</td>";
                    
                    foreach((array)$YearTermAry as $ytID){
	                    foreach ($groupBy as $key){
	                    	if(isset($finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['highest'])) {
	                    		if (!isset($highestMarks) || (isset($highestMarks) && $finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['highest']>$highestMarks)) {
	                    			$highestMarks = $finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['highest'];
	                            }
	                        }
	                    }
	
	                    $table2 .= "<td>";
	                        $table2 .= isset($highestMarks)? $highestMarks : '--';
	                        if($base_checkbox){
	                            $table2 .= "%";
	                        }
	                    $table2 .= "</td>";
                    }
                    foreach((array)$YearTermAry as $ytID){
                    foreach ($groupBy as $key){
                        $table2 .= "<td>";
                        $table2 .= isset($finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['highest'])? $finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['highest'] : '--';
                            if($base_checkbox){
                                $table2 .= "%";
                            }
                        $table2 .= "</td>";
                    }
                    }
                $table2 .= "</tr>";
            }
            ### Highest Marks End###

            ### Lowest Marks Start###
            if($display_checkbox_data['min']) {
                $table2 .= "<tr>";
                    $table2 .= "<td>";
                        $table2 .= $iPort["LowestMark"];
                    $table2 .= "</td>";

                    foreach((array)$YearTermAry as $ytID){
	                    foreach ($groupBy as $key){
	                    	if(isset($finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['lowest'])) {
	                    		if (!isset($lowestMarks) || (isset($lowestMarks) && $finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['lowest']<$lowestMarks)) {
	                    			$lowestMarks = $finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['lowest'];
	                            }
	                        }
	                    }
	
	                    $table2 .= "<td>";
	                        $table2 .= isset($lowestMarks)? $lowestMarks : '--';
	                        if($base_checkbox){
	                            $table2 .= "%";
	                        }
	                    $table2 .= "</td>";
                    }
                    foreach((array)$YearTermAry as $ytID){
                    foreach ($groupBy as $key){
                        $table2 .= "<td>";
                        $table2 .= isset($finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['lowest'])? $finalResult[$key['ClassName'].' '.$yearTermMaping[$ytID]]['lowest'] : '--';
                            if($base_checkbox){
                                $table2 .= "%";
                            }
                        $table2 .= "</td>";
                    }
                    }
                $table2 .= "</tr>";
            }
            ### Lowest Marks END###
		$table2 .= "</tbody>";
		### Table Body END ###
	$table2 .= "</table>";
$table2 .= "</div>";
######## Table2 Data END ########

######## Group Data START ########
### Chart 1 Start ###
if($chartType=='column'){
	foreach($data as $_data){
		$highChartDataAssoAry['meanChart']['series'][] = array('name' => $_data['name'], 'data' => $_data['data']);
	}
}else{
	foreach($data as $_data){
		$highChartDataAssoAry['meanChart']['series'][] = array('name' => $_data['name'], 'data' => $_data['data']);
	}
}

// convert highchart data to json
$highChartDataAssoAry['meanChart']['xAxisItemJson'] = $json->encode(array_unique($highChartDataAssoAry['meanChart']['xAxisItem']) );
$highChartDataAssoAry['meanChart']['seriesJson'] = $json->encode($highChartDataAssoAry['meanChart']['series']);
### Chart 1 End ###
// 2020-07-29 (Philips) - Sort total in first order
ksort($highChartData2);
### Chart 2 Start ###
foreach($highChartData2 as $_data){
	$highChartDataAssoAry2['meanChart']['series'][] = array('name' => $_data['name'], 'data' => $_data['data']);
}
$highChartDataAssoAry2['meanChart']['xAxisItemJson'] = $json->encode($highChartDataAssoAry2['meanChart']['xAxisItem']);
$highChartDataAssoAry2['meanChart']['seriesJson'] = $json->encode($highChartDataAssoAry2['meanChart']['series']);
### Chart 2 End ###
/* */
######## Group Data END ########


######## UI START ########

#### Color START ####
$colorArr = array();
if(count($subjMap_arr) > 19){
	foreach($subjMap_arr as $subjectCode=>$subjName){ 
		mt_srand((double)microtime()*1000000);
		$rand_c = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
		$colorArr[$subjectCode] = $rand_c;
	}
}
#### Color END ####
?>

<!-- -------- Generate bar chart START -------- -->

<style>
.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.bar {
  fill: steelblue;
}

.x.axis path {
  display: none;
}

</style>

<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<?php if($display_checkbox['chart']){?>
<div class="chart" style="height: 500px">
</div>


<script>
// var 

xAxis='<?=$highChartDataAssoAry['meanChart']['xAxisItemJson']?>';
series='<?=$highChartDataAssoAry['meanChart']['seriesJson']?>';

xAxis = JSON.parse(xAxis);
series = JSON.parse(series);

$('.chart').highcharts({
    chart: {
        type: '<?=$chartType?>'
    },
    title: {
        text: $('#subj_1 option:selected').text() + ' <?=$iPort["ClassComparison"]?>'
    },
    xAxis: {
        
     categories: xAxis,
     crosshair: true,
     title: {
         text: '<?=$Lang['SDAS']['Score']  ?>'
     }
},
    yAxis: {
        min: 0,
        <?php if($chartyAxisMax!=''){ echo 'max: ' . $chartyAxisMax.','; }?>
        title: {
            text: '<?=$chartyAxisTitle?>'
        },
        minTickInterval: 1
    },
    tooltip: {
        pointFormat: '<?=$chartSetting?>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },
    series: series,
    credits : {enabled: false,},
});

function jsOnloadDetailThickBox(StudentID, YearTermID){
	$.ajax({
		type: "POST",
		url: "?t=ajax.ajax_get_student_detail_thickbox", 
		data: {
			 "StudentID" : StudentID,
			 "AcademicYearID" : '<?=$academicYearID?>',
			 "YearTermID" : YearTermID ? YearTermID : '',
			 "SubjectID" : '<?=$Subjects?>'
		},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}
</script>

<br/>
<?php }?>
<?php 
    if($display_checkbox['table'])
		echo $x;
?>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<?php if($display_checkbox['chart']){?>
<div class="chart2" style="height: 500px">
</div>
<script>
xAxis='<?=$highChartDataAssoAry2['meanChart']['xAxisItemJson']?>';
series='<?=$highChartDataAssoAry2['meanChart']['seriesJson']?>';

xAxis = JSON.parse(xAxis);
series = JSON.parse(series);

$('.chart2').highcharts({
	<?=$ChartColorAry?>
    chart: {
        type: 'column'
    },
    tooltip:{
		formatter: function(){
			return '<span style="color:'+this.point.color+'">● </span>' + '<b> ' + this.series.name + '</b>: <b>' + this.y + '</b>';
		}
    },
    title: {
        text: $('#subj_1 option:selected').text() + ' <?=$Lang['SDAS']['passPercent']?>'
    },
    xAxis: {
    	categories: [' '],
    	crosshair: true
	},
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: '<?=$iPort['report_col']['improved_by_percentage'] ?>'
        }
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: series,
    credits : {enabled: false,},
});
</script>

<!-- -------- Generate bar chart END -------- -->

<br/>
<?php }?>

<!-- -------- Generate table START -------- -->
<?php 
    if($display_checkbox['table'])
		echo $table2;
?>

<script>$(function() {$('#resultTable').floatHeader();});</script>
<!-- -------- Generate table END -------- -->



<?php


intranet_closedb();
?>