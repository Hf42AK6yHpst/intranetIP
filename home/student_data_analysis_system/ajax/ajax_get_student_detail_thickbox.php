<?php 
/*
 * Change Log:
 * 2017-12-12 Pun
 * - Added support load overall result
 * Date 2017/5/4 Villa
 * - Fix Anuual Report cannot display correctly
 * Date 2017/4/7 Villa 
 * - Open the File
 * 
 */
$libSDAS = new libSDAS();
$libInterface = new interface_html();
echo $libInterface->Include_TableExport();
$IsAnnualCond= "";
$MainTermCond= "";
$AssessmentTermCond = "";
if($YearTermID){
	$YearTerm_Temp = explode('_',$YearTermID);
	$MainTermCond = " AND YearTermID = '$YearTerm_Temp[0]'";
	if($YearTerm_Temp[1]){
		$AssessmentTermCond = " AND TermAssessment = '$YearTerm_Temp[1]' ";
	}else{
		$AssessmentTermCond = " AND TermAssessment IS NULL ";
	}
}
if($Semester){
	$YearTerm_Temp = explode('_',$Semester);
	$MainTermCond = " AND Semester = '$YearTerm_Temp[0]'";
	if($YearTerm_Temp[1]){
		$AssessmentTermCond = " AND TermAssessment = '$YearTerm_Temp[1]' ";
	}else{
		$AssessmentTermCond = " AND TermAssessment IS NULL ";
	}
}
if(empty($YearTermID)&&empty($Semester)){
	$IsAnnualCond = " AND IsAnnual = 1";
}else{
	$IsAnnualCond = " AND IsAnnual = 0 ";
}

if($SubjectID){
    $Subject_Temp = explode('_',$SubjectID);
    $MainSubjectIDCond = " AND SubjectID = '$Subject_Temp[0]' ";
    if($Subject_Temp[1]){
    	$CMPSubjectIDCond = " AND SubjectComponentID = '$Subject_Temp[1]' ";
    }else{
    	$CMPSubjectIDCond= " AND SubjectComponentID IS NULL";
    }
    
    $table = 'ASSESSMENT_STUDENT_SUBJECT_RECORD';
}else{
    $MainSubjectIDCond = $CMPSubjectIDCond = '';
    $table = 'ASSESSMENT_STUDENT_MAIN_RECORD';
}

$StudentID = str_replace(',', "','", $StudentID);


$sql = 
"
	Select 
		* 
	From 
		{$eclass_db}.{$table} 
	WHERE 
		UserID in ('$StudentID')
		AND 
			AcademicYearID = '$AcademicYearID'
		$MainTermCond
		$AssessmentTermCond
		$MainSubjectIDCond
		$CMPSubjectIDCond
		$IsAnnualCond
	Order By 
		ClassName, LENGTH(ClassNumber), ClassNumber
";
$result = $libSDAS->returnResultSet($sql);

$sql = 
"Select 
	*
From 
	INTRANET_USER
WHERE
	UserID in  ('$StudentID')
";
$StudentInfo = BuildMultiKeyAssoc($libSDAS->returnResultSet($sql),'UserID');
#BtnAry
$btnAry[] = array('print', 'javascript:PrintThickBox()','',array(),' id="PrintThickBoxBtn"');
// $btnAry[] = array('export', 'javascript:void(0)','',array(),' id="csv_btn"');
$htmlAry['contentTool'] = $libInterface->Get_Content_Tool_By_Array_v30($btnAry);
$x ='';
$x .= $htmlAry['contentTool'];
$x .="<div class='chart_tables' id='Detail_Table'>";
$x .= "<table class='common_table_list_v30 view_table_list_v30' width='100%'>";
	if($result){
		$x .= "<thead>";
			$x .= "<th>".$Lang['SDAS']['Class']."</th>";
			$x .= "<th>".$Lang['SDAS']['ClassNumber']."</th>";
			$x .= "<th>".$Lang['SDAS']['StudentName']."</th>";
			$x .= "<th>".$Lang['SDAS']['Score']."</th>";
		$x .= "</thead>";
		$x .= "<tbody>";
		foreach ($result as $_result){
			$x .= "<tr>";
				$x .= "<td>".$_result['ClassName']."</td>";
				$x .= "<td>".$_result['ClassNumber']."</td>";
				$x .= "<td>".Get_Lang_Selection($StudentInfo[$_result['UserID']]['ChineseName'], $StudentInfo[$_result['UserID']]['EnglishName'])."</td>";
				$x .= "<td>".$_result['Score']."</td>";
			$x .= "</tr>";
		}
		$x .= "</tbody>";
	}else{
		$x .= "<tbody>";
			$x .= "<td><div align='center'>".$Lang['SDAS']['NoRecord']['A']."</div></td>";
		$x .= "</tbody>";
	}
$x .= "</table>";
$x .= "</div>";
echo $x;
?>
<script>
$('#csv_btn').click(function(){
	$('#Detail_Table').tableExport({type: 'csv' });
});
function PrintThickBox(){
	var options = { mode : "popup", popClose : false};
	$( '#Detail_Table' ).printArea(options);
}
</script>
