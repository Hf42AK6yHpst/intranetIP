<?php 
/*
 * Change Log:
 * Date	2017-05-23 :
 * -	Open the file
 */
// $libSDAS = new libSDAS();
$json = new JSON_obj();
if(!$YearID){
	$sql = 'Select YearID FROM YEAR_CLASS WHERE YearClassID = "'.$YearClassID.'"';
	$rs = $objSDAS->returnResultSet($sql);
	$YearID = $rs[0]['YearID'];
}
$accessRight = $objSDAS->getAssessmentStatReportAccessRight();
foreach ((array)$accessRight['classTeacher'] as $_accessRight){
	$ClassTeacher_YearClassID[] = $_accessRight['YearClassID'];
}
foreach ((array)$accessRight['subjectPanel'] as $SubjectID => $Subject_YearID){
	foreach ((array)$Subject_YearID as $_Subject_YearID=>$value){
		$SubjectPanel[$_Subject_YearID][] = $SubjectID;
	}
}
if(in_array($YearClassID,(array)$ClassTeacher_YearClassID)){
	//Is ClassTeacher
	$ReturnArr['NeedFilter'] = false;
	$ReturnArr['SubjectIDArr'] = array();
}else{
	//Subject
	$ReturnArr['NeedFilter'] = true;
	$ReturnArr['SubjectIDArr'] = $SubjectPanel[$YearID];
}
$ReturnArr = $json->encode($ReturnArr);
echo $ReturnArr;
?>
