<?php
//Using: Pun
/**
 * Change Log:
 * 2019-01-04 Pun [128759] [ip.2.5.10.1.1]
 *  - Fixed wrong result
 * 2018-12-19 Pun [128759] [ip.2.5.10.1.1]
 *  - Fixed wrong result
 * 2017-08-25 Pun
 *  - File Create
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");;
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/valueAddedData/libValueAddedData.php");

######## INIT START ########
intranet_auth();
intranet_opendb();
$objDB = new libdb();
$year = new Year();
$yc = new year_class();
$lpf = new libportfolio();
$sbj = new subject();
$lc = new Learning_Category();
$libSDAS = new libSDAS();
$vad = new libValueAddedData();

define('DECIMAL_PLACES', 4);

$accessRight = $lpf->getAssessmentStatReportAccessRight();
######## INIT END ########

######## Safe SQL START ########
$searchAcademicYearID = IntegerSafe($academicYearID);
if($reportType == libValueAddedData::VALUE_ADDED_TYPE_TEACHER){
    $searchAcademicYearID2 = IntegerSafe($academicYearID2);
}else{
    $searchAcademicYearID2 = $searchAcademicYearID;
}
/*if($reportType == libValueAddedData::VALUE_ADDED_TYPE_TEACHER){
    if($accessRight['admin']){
        $searchTeacherID = (array)IntegerSafe($teacherID);
    }else{
        $searchTeacherID = array($_SESSION['UserID']);
    }
}else{
    $searchLearningCategoryID = IntegerSafe($learningCategoryID);
}*/
######## Safe SQL END ########

######## Access Right START ########
$allowAccess = false;

if($accessRight['admin']){
    $allowAccess = true;
}else if($reportType == 1){
    $allowAccess = true;
}else if($reportType == 2){
    /*
    if($learningCategoryID){

        $allSubject = $vad->getAllSubject();
        $subjectLearningCategory = BuildMultiKeyAssoc($allSubject, array('SubjectID') , array('LearningCategoryID'), $SingleValue=1);
        
        $allowSubjectIdArr = array_keys($accessRight['subjectPanel']);
        $allowLearningCategoryIdArr = array();
        foreach($allowSubjectIdArr as $subjectID){
            $allowLearningCategoryIdArr[] = $subjectLearningCategory[$subjectID];
        }
        
        $allowYearIdArr = array();
        foreach($allowSubjectIdArr as $subjectID){
            $learningCategoryID = $subjectLearningCategory[$subjectID];
            $allowYearIdArr[$learningCategoryID] = array_merge(
                (array)$allowYearIdArr[$learningCategoryID], 
                array_keys((array)$accessRight['subjectPanel'][$subjectID])
            );
        }
        
        $allowAccess = in_array($searchLearningCategoryID, $allowLearningCategoryIdArr);
    }else{
        $allowAccess = !!count($accessRight['subjectPanel']);
    }
    */
}

if(!$allowAccess){
    echo 'Access Denied.';
    exit;
}
######## Access Right END ########


######################## Get Data START ########################
$filterArr = array();
if($FromSubjectID){
    $filterArr['SubjectId'] = $FromSubjectID;
}

$resultRow = $vad->getReportData($reportType, $searchAcademicYearID, $searchAcademicYearID2, $YearTermID, $YearTermID_VS, $filterArr);


$academicYear = getAcademicYearByAcademicYearID($searchAcademicYearID);
$vsAcademicYear = getAcademicYearByAcademicYearID($searchAcademicYearID2);
######################## Get Data END ########################

######################## Display Result START ########################
?>
<script src="/templates/jquery/jquery.floatheader.min.js"></script>

<style>
.noWrap{
    white-space: nowrap;
}
.totalCount{
    display: block;
}
</style>

<?php if(count($resultRow) == 0): ?>
<!-- ################ No Record START ################ -->

<div class="chart_tables">
	<table class="common_table_list_v30 view_table_list_v30 resultTable" id="chart_tables_1">
		<tr><td><?=$Lang['General']['NoRecordFound'] ?></td></tr>
	</table>
</div>

<!-- ################ No Record END ################ -->
<?php elseif($reportType == libValueAddedData::VALUE_ADDED_TYPE_SUBJECT_GROUP): ?>
<!-- ################ Subject Group Report START ################ -->


<style>
#resultTableContainer{
    width: 100%;
    
}
#resultTableContainer > tbody > tr > td{
    padding: 0 10px;
    margin: 0;
    border: 0;
}
.chart_tables{
    vertical-align: top;
}
#resultTableContainer > tbody > tr:not(:first-child) > td{
    padding-top: 20px;
}
.resultTable th, .resultTable td{
    white-space: nowrap;
}
/*@media print {
    #resultDiv{
        padding: 1cm;
    }
}*/
</style>

<div>
	<table id="resultTableContainer">
		<?php 
		foreach($resultRow as $yearId => $d1): 
		?>
			<tr>
				<?php foreach($d1 as $className => $d2): ?>
					<td class="chart_tables">
<!-- ######## Class Result START ########-->
<table class="common_table_list_v30 view_table_list_v30 resultTable">
	<thead>
		<tr>
			<th><?=$className ?></th>
			<th>
				<?php 
				list($_, $yearTermName) = explode('_', $YearTermID);
				echo "{$yearTermName} {$iPort["report_col"]["total_student"]}";
				?>
			</th>
			<th>
				<?php 
				list($_, $yearTermName) = explode('_', $YearTermID_VS);
				echo "{$yearTermName} {$iPort["report_col"]["total_student"]}";
				?>
			</th>
			<th><?=$iPort["report_col"]["total_attempt"] ?></th>
			<th><?=$Lang['SDAS']['ValueAddedData']['ValueAdded'] ?></th>
			<th><?=$iPort["Teacher"] ?></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		foreach($d2 as $subjectName => $result):
		    $teacherNameHTML = implode('<br />', array_unique($result['vsSubjectGroupTeacherName']));
		?>
			<tr>
				<td data-subject-group-id="<?=$result['vsSubjectGroupID'] ?>"><?=$subjectName ?></td>
				<td><?=$result['studentCount'] ?></td>
				<td><?=$result['vsStudentCount'] ?></td>
				<td><?=($result['attemptStudentCount'])?$result['attemptStudentCount']:'--' ?></td>
				<td data-correlation="<?=$result['correlation'] ?>"><?=($result['correlation']===null)?'--':number_format($result['correlation'], DECIMAL_PLACES) ?></td>
				<td><?= $teacherNameHTML ?></td>
			</tr>
		<?php 
		endforeach; 
		?>
	</tbody>
</table>
<!-- ######## Class Result END ########-->
					</td>
        		<?php endforeach; ?>
			</tr>
		<?php 
		endforeach; 
		?>
	</table>
</div>





<!-- ################ Subject Group Report END ################ -->
<?php elseif($reportType == libValueAddedData::VALUE_ADDED_TYPE_TEACHER): ?>
<!-- ################ Teacher Report START ################ -->

<style>
.resultTable th, .resultTable td{
    white-space: nowrap;
}
.brw{
    border-right-width: 5px !important;
}
.btw{
    border-top: 3px solid #CCCCCC !important;
}
@media print {
    #resultDiv{
        padding: 1cm;
    }
}
</style>

<!-- ######## Teacher Result START ########-->
<table class="common_table_list_v30 view_table_list_v30 resultTable">
	<thead>
		<!--tr>
			<th class="brw"><?=$iPort["Teacher"] ?></th>
			<?php 
			$totalClass = 0;
    		foreach($resultRow as $teacherName => $d1){
    		    $totalClass = max($totalClass, count($d1));
    		}
    		
    		for($i=0;$i<$totalClass;$i++):
    		?>
    			<th>
    				<?php 
    				list($_, $yearTermName) = explode('_', $YearTermID_VS);
    				echo "{$vsAcademicYear}<br />{$yearTermName} {$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']}";
    				?>
    			</th>
    			<th>
    				<?php 
    				list($_, $yearTermName) = explode('_', $YearTermID_VS);
    				echo "{$vsAcademicYear}<br />{$yearTermName} {$iPort["report_col"]["total_student"]}";
    				?>
    			</th>
    			<th>
    				<?php 
    				list($_, $yearTermName) = explode('_', $YearTermID);
    				echo "{$academicYear}<br />{$yearTermName} {$iPort["report_col"]["total_student"]}";
    				?>
    			</th>
    			<th><?=$iPort["report_col"]["total_attempt"] ?></th>
    			<th class="brw"><?=$Lang['SDAS']['ValueAddedData']['ValueAdded'] ?></th>
			<?php
			endfor;
			?>
		</tr-->
		<tr>
			<th><?=$iPort["Teacher"] ?></th>
			<th>
				<?php 
				list($_, $yearTermName) = explode('_', $YearTermID_VS);
				echo "{$vsAcademicYear} {$yearTermName} {$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']}";
				?>
			</th>
			<th>
				<?php 
				list($_, $yearTermName) = explode('_', $YearTermID);
				echo "{$academicYear} {$yearTermName} {$iPort["report_col"]["total_student"]}";
				?>
			</th>
			<th>
				<?php 
				list($_, $yearTermName) = explode('_', $YearTermID_VS);
				echo "{$vsAcademicYear} {$yearTermName} {$iPort["report_col"]["total_student"]}";
				?>
			</th>
			<th><?=$iPort["report_col"]["total_attempt"] ?></th>
			<th><?=$Lang['SDAS']['ValueAddedData']['ValueAdded'] ?></th>
		</tr>
	</thead>
	<tbody>
		<?php 
// 		debug_r($resultRow);
		foreach($resultRow as $teacherName => $d1): 
		    $hasEchoTeacherName = false;
		?>
			<!--tr>
				<td class="brw"><?=$teacherName ?></td>
				<?php 
				foreach($d1 as $className => $result):
				?>
    				<td data-vs-student-id="<?=$result['vsAttemptStudentId'] ?>"><?= $className ?></td>
    				<td><?=$result['vsStudentCount'] ?></td>
    				<td><?=$result['studentCount'] ?></td>
    				<td><?=($result['attemptStudentCount'])?$result['attemptStudentCount']:'--' ?></td>
    				<td class="brw" data-correlation="<?=$result['correlation'] ?>"><?=($result['correlation']===null)?'--':number_format($result['correlation'], DECIMAL_PLACES) ?></td>
    			<?php 
    			endforeach; 
    			for($i=count($d1);$i<$totalClass;$i++):
			    ?>
			    	<td>&nbsp;</td>
			    	<td>&nbsp;</td>
			    	<td>&nbsp;</td>
			    	<td>&nbsp;</td>
			    	<td class="brw">&nbsp;</td>
			    <?php
    			endfor;
    			?>
			</tr-->
			
			
			<?php 
			ksort($d1);
			foreach($d1 as $className => $result):
			    $cssClass = ($hasEchoTeacherName)?'':'btw';
			?>
			<tr>
    			<?php 
    			if(!$hasEchoTeacherName):
    			?>
    				<td class="<?=$cssClass ?>" rowspan="<?=count($d1) ?>"><?=$teacherName ?></td>
				<?php
				endif;
				?>
				<td class="<?=$cssClass ?>" data-vs-student-id="<?=$result['vsAttemptStudentId'] ?>"><?= $className ?></td>
				<td class="<?=$cssClass ?>"><?=$result['studentCount'] ?></td>
				<td class="<?=$cssClass ?>"><?=$result['vsStudentCount'] ?></td>
				<td class="<?=$cssClass ?>"><?=($result['attemptStudentCount'])?$result['attemptStudentCount']:'--' ?></td>
				<td class="<?=$cssClass ?>" data-correlation="<?=$result['correlation'] ?>"><?=($result['correlation']===null)?'--':number_format($result['correlation'], DECIMAL_PLACES) ?></td>
			</tr>
    		<?php 
			    $hasEchoTeacherName = true;
    		endforeach; 
    		?>
		<?php 
		endforeach; 
		?>
	</tbody>
</table>
<!-- ######## Teacher Result END ########-->







<!-- ################ Teacher Report END ################ -->
<?php endif; ?>




<script>
(function($) {
	$('.resultTable').floatHeader();
})(jQuery);
</script>




<?php
######################## Display Result END ########################

intranet_closedb();












