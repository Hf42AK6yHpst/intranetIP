<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
intranet_auth();
intranet_opendb();

$libSDAS = new libSDAS();
$li_pf = new libpf_asr();
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
// debug_pr($accessRight);
# get Class Level selection
$year_arr = $li_pf->getActivatedForm($ay_id, false);
$i=0;
if($isClassTeacher){
	foreach ($accessRight['classTeacher'] as $Class){
		if($ay_id==$Class['AcademicYearID']){
			foreach($year_arr as $_year_arr){
				if($_year_arr['YearID']==$Class['YearID']){
					$temp[$i]['0']= $_year_arr['YearID'];
					$temp[$i]['1']= $_year_arr['YearName'];
					$temp[$i]['YearID']= $_year_arr['YearID'];
					$temp[$i]['YearName']= $_year_arr['YearName'];
					$i++;
				}
			}
		}
	}
	unset($year_arr);
	$year_arr=$temp;
}
// debug_pr($year_arr);
$html_classlevel_selection = (sizeof($year_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($year_arr, "name='yearID'", "", 0, 0, "", 2);

echo $html_classlevel_selection;

//$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);
//echo $ayterm_selection_html;

?>