<?php
//Using: 
/**
 * Change Log
 * 2020-09-15 (Philips) [DM#3909] Use YearTerm Instead of YearTermID
 * 2020-08-06 (Philips)
 *  - 	Modified highchart get subject name from #Subjects to #subj_1
 * 2020-07-29 (Philips)
 *  -	Added ksort to improve ordering of chart 2
 * 2020-05-05 (Philips) [2019-0710-1725-23085]
 *  -   Added distribution chart
 * 2019-12-06 (Philips) [2019-1204-1057-10206]
 *  -	Sort data by year name
 * 2019-07-25 (Bill) [2019-0312-1523-03073]
 *  -   Added cust standard percentage range    ($sys_custom['SDAS']['Subject_Class_Distribution']['StandardArr'])
 * 2019-07-18 (Bill) [EJ DM#1259]
 *  - Fix cannot return result due to $Subjects
 * 2017-12-12 (Pun)
 *  -   Added cust T1A3 hardcode full mark
 *  -   Added Overall option to subject selection
 * 2017-08-14 Villa
 * 	-	Modified include file
 * 2017-08-01 Villa
 * -	Add ALLYEAR HANDLING
 * 2017-06-01 Villa
 *  -	Modified  if Subject Full Mark has not been set checking to "default set the subject fullmark to 100, show the warning if and only if the highscore of the data>Subject Full Mark"
 * 2017-05-12 Villa
 *  -	Modified HighChart Table behaviour
 * 2017-04-10 Villa
 *  -	Add ThickBox Related (spline)
 * 2017-04-07 Villa
 *  -	Add ThickBox Related (Column)
 * 2017-03-24 Villa #T115142 
 *  -	Modified $scoreTitle : Change Wording
 * 2017-03-24 Villa #Y114949 
 *  -	break this file and return warning msg if Subject Full has not been set
 *  -	Change the no Result Wording
 * 2017-01-13 Villa
 *  -	modified to support subject 
 * 2016-12-23 Villa
 *  -	modified the range ditribution codong
 *  -	filter out 0%
 * 2016-12-14 Villa
 *  - 	Add baseOn/ bias mechanism 
 *  - 	modified GroupBy - reduce sql query
 *  -	add yearterm filtering
 * 2016-12-13 Villa
 *  -	modified maxscore getting from the db
 * 2016-12-06 Villa
 * -	change the display for Spline Diagram
 * 2016-11-28 Villa
 * - 	Sync the table Color with other tb
 * 2016-11-25 Villa
 *  -	Fix the Whole Form data wrong in sql coding
 * 2016-11-16 Villa
 * 	-	New File
 */

// $PATH_WRT_ROOT = "../../../";
// include_once($PATH_WRT_ROOT."includes/global.php");
// include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
// include_once($PATH_WRT_ROOT."includes/libportfolio.php");
// include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.".$intranet_session_language.".php");
// include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
// include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
// include_once($PATH_WRT_ROOT."includes/json.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();
$fcm = new form_class_manage();
$lpf = new libportfolio();
$json = new JSON_obj();
// $objSDAS = new libSDAS();
$Semester = $YearTerm;
if (strstr($YearTerm, "_"))
{
	$tmpArr = explode("_", $YearTerm);
	$YearTerm =  $tmpArr[0];
	$TermAssessment = trim($tmpArr[1]);
}
########## Access Right START ##########
$accessRight = $objSDAS->getAssessmentStatReportAccessRight();
$canAccess = true;
if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) && 
		in_array('form_student_performance',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
	$accessRight['admin'] = 1;
}
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$academicYearID = Get_Current_Academic_Year_ID();
if(!$accessRight['admin']){
	if( count($accessRight['subjectPanel']) ){
		#### Subject Panel access right START ####
		
		$canAccess = true; // Subject Panel can always access this page
		$filterSubjectArr = array_keys($accessRight['subjectPanel']);
		
		#### Subject Panel access right END ####
	}else{
		#### Class teacher access right START ####
		
		if($academicYearID != $currentAcademicYearID){
			$canAccess = false;
		}
		$yearClassIdArr = array();
		foreach($accessRight['classTeacher'] as $yearClass){
			$yearClassIdArr[] = $yearClass['YearClassID'];
		}
		foreach($accessRight['subjectGroup'] as $subjectGroup){
		    $sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
		    $studentList = $sGroup->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=1, $showClassID = true);
		    $classList = Get_Array_By_Key($studentList, "YearClassID");
		    $yearClassIdArr = array_merge($yearClassIdArr, $classList);
		}
		foreach((array)$YearClassID as $id){
			if(!in_array($id, $yearClassIdArr)){
				$canAccess = false;
				break;
			}
		}
		
		#### Class teacher access right END ####
	}
	
	if(!$canAccess){
		echo 'Access Denied.';
		die;
	}
}
########## Access Right END ##########

$li = new libdb();
// $lpf = new libpf_sturec();
$linterface = new interface_html();
echo $linterface->Include_Thickbox_JS_CSS();

// $lpf->createTempAssessmentSubjectTable($li, true);


######## Get Data START ########
if(!empty($YearClassID)){
	$YearClassID = (array)$YearClassID;
}

#### Get YearID for class teacher START ####
if($yearID == ''){
	$objYearClass = new year_class($YearClassID[0]);
	$yearID = $objYearClass->YearID;
}
// $yearID = $YearClassID;
#### Get YearID for class teacher END ####

#### Get Class Name START ####
$yearID = $YearID;
$rs = $fcm->Get_Class_List($academicYearID,$yearID);
$allClassArr = BuildMultiKeyAssoc($rs, array('YearClassID') , array('ClassTitleEN', 'ClassTitleB5'));
$classNameArr = array();
if(!empty($YearClassID)){
	foreach($YearClassID as $class){
		$classNameArr = $allClassArr[$class]['ClassTitleEN'];
	
	}
}
#### Get Class Name END ####
#### Get Year START ####
$academicYearIDArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
$academicYearArr_sql = implode(',',$academicYearIDArr);
$sql = "SELECT YearNameEN, YearNameB5, AcademicYearID FROM ACADEMIC_YEAR WHERE AcademicYearID IN (".$academicYearArr_sql.")";
$rs = $lpf->returnResultSet($sql);
$academicYearArr = array(); //reset $academicYearArr
foreach ((array)$rs as $key=>$value){
	$academicYearArr[$value['AcademicYearID']] ['name'] = Get_Lang_Selection($value['YearNameB5'], $value['YearNameEN']);
	$academicYearArr[$value['AcademicYearID']] ['ID'] = $value['AcademicYearID'];
}
#### Get Year  END ####

####### Subject Component Related Start #######
$FromSubjectID = $Subjects;
$temp = explode('_',$FromSubjectID);
$MainSubject = $temp[0];
$CmpSubject = $temp[1];
if($CmpSubject){
	$isCMP= true;
}else{
	$isCMP= false;
}
####### Subject Component Related End #######

if(!empty($YearClassID)){
  $YearClassID_sql = implode(',', $YearClassID);
}
//   $conds .= ($_compSubj == "") ? " AND IFNULL(assr.SubjectComponentCode, '') = '' " : "AND assr.SubjectComponentCode = '{$_compSubj}' ";
  $conds .= " AND assr.Score <> -1 ";
  if(!empty($YearClassID)){
  	$conds .= " AND assr.ClassName = '".$classNameArr."' "; //using class name to filter in stead of class ID
  }
  if(!empty($YearTerm)){
  	$conds .= " AND assr.Semester='{$YearTerm}' ";
  	if(!empty($TermAssessment)){
  		$conds .= " AND assr.TermAssessment='{$TermAssessment}' ";
  	}else{
  		$conds .= " AND assr.TermAssessment IS NULL ";
  	}
  }else{
  	$conds .= " AND assr.YearTermID IS NULL ";
  }
  
  if($MainSubject == 0){
      $table = 'ASSESSMENT_STUDENT_MAIN_RECORD';
  }else{
      $conds .= " AND assr.SubjectID = {$MainSubject} ";
      if($isCMP){
      	$conds .= " AND assr.SubjectComponentID = {$CmpSubject} ";
      }else{
      	$conds .= " AND assr.SubjectComponentID IS NULL ";
      }
      
      $table = 'ASSESSMENT_STUDENT_SUBJECT_RECORD';
  }
  $conds .= "AND yc.AcademicYearID in (".$academicYearArr_sql.") AND assr.AcademicYearID in (".$academicYearArr_sql.") ";
  $sql = "SELECT assr.ClassName, assr.Score AS avgScore, assr.AcademicYearID, assr.UserID ";
  $sql .= "FROM {$eclass_db}.{$table} assr ";
  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON assr.UserID = ycu.UserID ";
  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON assr.YearClassID = yc.YearClassID AND yc.YearClassID = ycu.YearClassID ";
//   $sql .= "INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT s ON assr.SubjectID = s.RecordID ";
  $sql .= "WHERE yc.YearID = '{$yearID}'";
  $sql .= $conds;
  $sql .= "AND IF(assr.Score > 0, true, IF(assr.Score < 0, false, IFNULL(assr.Grade, '') <> '')) ";
  
//   if ($TermAssessment=="")
//   {
//   	$sql .= " AND assr.TermAssessment IS NULL ";
//   }else{
//   	$sql .= " AND assr.TermAssessment = '".addslashes($TermAssessment)."' ";
//   }
  $rs = $li->returnResultSet($sql);
//   debug_pr($sql);
  $AYear = Get_Array_By_Key($rs, 'AcademicYearID');
  $AYear = array_unique($AYear);
  $groupBy = array();
  $j=0;
  foreach ($AYear as $_AYear){
  	$groupBy[$j]['AcademicYearID'] = $_AYear;
  	$groupBy[$j]['AcademicYearName'] = getAcademicYearByAcademicYearID($_AYear);
  	$j++;
  }
  $groupBy = sortByColumn($groupBy, 'AcademicYearName');

  // [2019-0312-1523-03073]
  if(isset($sys_custom['SDAS']['Subject_Class_Distribution']['StandardArr'])){
      $standardArr = $sys_custom['SDAS']['Subject_Class_Distribution']['StandardArr'];
  }
  
//   $maxScore = '100';
//   $standardArr = array(90,80,50,20);
  if($isCMP){
  	$SubID = $CmpSubject;
  }else{
  	$SubID = $MainSubject;
  }
  $sql = "SELECT
 			 FullMarkInt
  		FROM
  			{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK
  		WHERE
  			AcademicYearID='{$academicYearID}'
  		AND
  			SubjectID='{$SubID}'
  		AND
  			YearID='{$yearID}'";
  $temp = $li->returnResultSet($sql);
  $temp = Get_Array_By_Key($temp, 'FullMarkInt');
  
  if($MainSubject == 0){
      $SubjectFullMark = $maxScore = '100';
  }else{
      $SubjectFullMark = $temp[0];
      $maxScore = $temp[0]? $temp[0]:'100';
  }
  
  $maxScore=$objSDAS->checkZeroDot($maxScore);

  if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
      $objYear = new Year($yearID);
      if(
          ($TermAssessment == 'T1A3' && strpos($objYear->YearName, '6') !== false) ||
          ($TermAssessment == 'T2A3')
      ){
      	$maxScore = '100';
      	// 2020-01-08 (Philips) [2019-1218-1255-37206] - fixed
      	$SubjectFullMark = 100;
      	$PassScore = '50';
      }
  }
  
  
  if($base_checkbox){
  	$bias = 100/$maxScore;
  	$maxScore = 100;
  	$percent_symbol = '%';
  }else{
  	$bias = 1;
  	$percent_symbol = '';
  }
  
  $parts = sizeof($standardArr);
  if($parts==0){
  	$parts = '10';
  	$division = $maxScore/$parts;
  	$division=$objSDAS->checkZeroDot($division);
  	for($i=$parts-1;$i>-1;$i--){
  		$standardArr = array_merge((array)$standardArr,array($division*$i));
  	}
  }
  $standardArr = array_merge(array($maxScore),$standardArr);
  
  if($chartType=='spline'){
  	sort($standardArr);
  }
  $parts = sizeof($standardArr);
  $highestScore = 0;
  
  foreach ($groupBy as $value){ //academic year
  	$result[$value['AcademicYearID']]['total']=0;
  	foreach ($rs as $value2){ 
  		if($value['AcademicYearID']==$value2['AcademicYearID']){
  			if($value2['avgScore']>$highestScore){
  				$highestScore = $value2['avgScore'];
  			}
  			$value2['avgScore']=$value2['avgScore']*$bias;
  			if($chartType=='spline'){ //chartType revser the data
  				for($i=$parts;$i>0;$i--){
  					$j=$i-1;
  					if($i=='1'){
  						if($value2['avgScore']<=$standardArr[$i]){
  							$result[$value['AcademicYearID']][$i][] = $value2['avgScore'];
  							$studentID_ary[$value['AcademicYearID']][$i][] = $value2['UserID'];
  						}
  					}elseif($i=='0'){
  						//do nothing
  					}else{
  						if($standardArr[$i]>=$value2['avgScore']&&$value2['avgScore']>$standardArr[$j]){
  							$result[$value['AcademicYearID']][$i][] = $value2['avgScore'];
  							$studentID_ary[$value['AcademicYearID']][$i][] = $value2['UserID'];
  						}
  					}
  				}
  			}else{
	  			for($i=0;$i<$parts;$i++){
	  				$j=$i+1;
	  				if($i==$parts-1){
	  					if($value2['avgScore']<=$standardArr[$i]){
	  						$result[$value['AcademicYearID']][$i][] = $value2['avgScore'];
	  						$studentID_ary[$value['AcademicYearID']][$i][] = $value2['UserID'];
	  					}
	  				}elseif($i==$parts){
	  					//do nothing
	  				}else{
	  					if($standardArr[$i]>=$value2['avgScore']&&$value2['avgScore']>$standardArr[$j]){
	  						$result[$value['AcademicYearID']][$i][] = $value2['avgScore'];
	  						$studentID_ary[$value['AcademicYearID']][$i][] = $value2['UserID'];
	  					}
	  				}
	  			}
  			}
  			$result[$value['AcademicYearID']]['total']=$result[$value['AcademicYearID']]['total']+1;
  		}
  	}
  }
  $sql = "SELECT
 			 AcademicYearID, FullMarkInt, PassMarkInt
		  FROM
		  	{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK
		  WHERE
		  	SubjectID='{$SubID}'
		  AND
		  	YearID='{$yearID}'";
  $temp = $objSDAS->returnResultSet($sql);
  foreach($temp as $_temp){
      if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
          $objYear = new Year($yearID);
          if(
              ($TermAssessment == 'T1A3' && strpos($objYear->YearName, '6') !== false) ||
              ($TermAssessment == 'T2A3')
          ){
              $_temp['PassMarkInt'] = '50';
          }
      }
      
  	$pass[$_temp['AcademicYearID']] ['PassMark'] = $_temp['PassMarkInt']*$bias;
  }
  $array = array();
  foreach ($groupBy as $value3){
  	$tempMarks = 0;
  	$finalResult[$value3['AcademicYearID']]['TotalNumber'] = $result[$value3['AcademicYearID']]['total'];
  	
  	foreach($result[$value3['AcademicYearID']] as $key=>$value){
//   		debug_pr($key);
  		if($key !== 'total'){
	  		foreach ((array)$value as $_value){
                if($MainSubject == 0){
                    $passMark = 50;
                }else{
                    $passMark = $pass[$value3['AcademicYearID']] ['PassMark'];
                }
                  
	  			$tempMarks = $tempMarks + $_value;
	  			if($_value>=$passMark){
	  				$finalResult[$value3['AcademicYearID']]['Pass'][] = $_value;
	  			}
	  		}
	  		$finalResult[$value3['AcademicYearID']][$key]['number'] = sizeof($result[$value3['AcademicYearID']][$key]);
	  		$finalResult[$value3['AcademicYearID']][$key]['studentIDAry'] = $studentID_ary[$value3['AcademicYearID']][$key];
  		}
  	}
  	
  	$finalResult[$value3['AcademicYearID']]['TotalMarks'] = $tempMarks;
  	$finalResult[$value3['AcademicYearID']]['PassNumber'] = sizeof($finalResult[$value3['AcademicYearID']]['Pass']);
  }
  //ALL YEARS HANDLING
  foreach ((array)$finalResult as $academicYearID=>$_finalResult){
  	foreach ((array)$_finalResult as $key => $__finalResult){
  		if($__finalResult['number']){
  			$finalResult['ALLYEAR'][$key]['number'] += $__finalResult['number'];
  		}
  	}
  	$finalResult['ALLYEAR']['TotalMarks'] += $_finalResult['TotalMarks'];
  	$finalResult['ALLYEAR']['PassNumber'] += $_finalResult['PassNumber'];
  	$finalResult['ALLYEAR']['TotalNumber'] += $_finalResult['TotalNumber'];
  }
  
  
  if(empty($finalResult)){
  	$x = "	<div class='chart_tables_2'>
				<table class='common_table_list_v30 view_table_list_v30'>
					<thead>
						<tr>
				  			<td align='center'>
				  				".$Lang['SDAS']['NoRecord']['A']."
				  			</td>
			  			</tr>
		  			</thead>
	  			</table>
  			</div>";
  	echo $x;
  	return false;
  }
  
  if($highestScore>$SubjectFullMark){
  	$x = "	<div class='chart_tables_2'>
				<table class='common_table_list_v30 view_table_list_v30'>
					<thead>
						<tr>
				  			<td>
				  				".$Lang['SDAS']['FullMarkSetting']['FullMarkNoSet']."
				  			</td>
			  			</tr>
		  			</thead>
	  			</table>
  			</div>";
  	echo $x;
  	return false;
  }
######## Get Data END ########
######## Table Data START ########
if($option_checkbox||$chartType=='column'){
 	$filter = 10;
}else{
 	$filter = 0;
}
$x = "";
$x .="<div class='chart_tables'>";
$x .="<table class = 'common_table_list_v30 view_table_list_v30'>";

if($chartType=='column'){
	$x .="<thead>";
	$x .="<tr>";
	$x .= "<th style='width: 20%; white-space: nowrap;'>"." "."</th>";
	//ALLYEAR
	$x.= "<th>".$Lang['SDAS']['averageValue']."</th>";
	foreach ($groupBy as $key){
		$x.= "<th>".$academicYearArr[$key['AcademicYearID']] ['name']."</th>";
	}
	$x .="</tr>";
	$x .="</thead>";
	$x .="<tbody>";
	
	for($i=0;$i<$parts;$i++){
		if($i!==$filter){
			$x .="<tr>";
			if($SDAS['SKHTST_CUST']){
				$x .="<td>".$standardArr[$i].$percent_symbol."</td>";
			}else{
				$last = isset($standardArr[$i+1])?false:true;
				if(!$last){
					if($standardArr[$i+1]){
// 						$scoreTitle = $standardArr[$i].$percent_symbol."-".($standardArr[$i+1]+0.1).$percent_symbol;
						$scoreTitle = ($standardArr[$i+1]+0.1).$percent_symbol."-".$standardArr[$i].$percent_symbol;
					}else{
// 						$scoreTitle = $standardArr[$i].$percent_symbol."-".($standardArr[$i+1]).$percent_symbol;
						$scoreTitle = ($standardArr[$i+1]).$percent_symbol."-".$standardArr[$i].$percent_symbol;
					}
						
					$x .="<tr><td>".$scoreTitle."</td>";
				}else{
					// 					$scoreTitle = $standardArr[$i].$percent_symbol;
					// 					$x .="<tr><td>".$scoreTitle."</td>";
				}
			}
			$data[$i]['name']=(string)$scoreTitle;
			
			//ALLYEAR HANDLING
			$score = $finalResult['ALLYEAR'][$i]['number'];
			$score = ($score)? $score : '0';
			#ThickBoxRelated
			// 				$StudentID_Pass_Ary = '';
			$StudentID_Pass = '';
// 			$CurrentAYID = $key['AcademicYearID'];
			if($i!==$filter){
				$x .="<td>".$score."</td>";
			}
			//HighChart Data
			$highChartDataAssoAry['meanChart']['xAxisItem'][] = $Lang['SDAS']['averageValue'];
			$data[$i]['data'][]=round($score*100/$finalResult['ALLYEAR']['TotalNumber']);
			//ALLYEAR HANDLING END
			
			foreach ($groupBy as $key){
				$score = $finalResult[$key['AcademicYearID']][$i]['number'];
				$score = ($score)? $score : '0';
				#ThickBoxRelated
				$StudentID_Pass_Ary = '';
				$StudentID_Pass = '';
				$StudentID_Pass_Ary =  $finalResult[$key['AcademicYearID']][$i]['studentIDAry'];
				$StudentID_Pass = implode(",",(array)$StudentID_Pass_Ary);
				$CurrentAYID = $key['AcademicYearID'];
				if($i!==$filter){
					$x .= "<td>".$linterface->Get_Thickbox_Link('420', '620',"",$academicYearArr[$key['AcademicYearID']]['name']."&nbsp;".$data[$i]['name'], "jsOnloadDetailThickBox('$StudentID_Pass','$CurrentAYID')","FakeLayer",$score)."</td>";
				}
				//HighChart Data
				$highChartDataAssoAry['meanChart']['xAxisItem'][] = $academicYearArr[$key['AcademicYearID']] ['name'];
				$data[$i]['data'][]=round($score*100/	$finalResult[$key['AcademicYearID']]['TotalNumber']);
			}
			$x .="</tr>";
		}
	}
// 	debug_pr($data);
	$chartSetting =  '<span style="color:{series.color}">{series.name}</span>: <b>{point.y:.2f}%</b><br/>';

}elseif($chartType=='spline'){
	$chartType = '';
	$x .="<thead>";
	$x .="<tr>";
	$x .= "<th style='width: 20%; white-space: nowrap;'>"." "."</th>";
	for($i=0;$i<$parts;$i++){
		if($i!==$filter){
			if($SDAS['SKHTST_CUST']){
				$symbol = "";
			}else{
				if($option_checkbox){
					$symbol = ">=";
				}else{
					$symbol = "<=";
				}
			}
			$x.= "<th>".$symbol.$standardArr[$i].$percent_symbol."</th>";
		}
		$highChartDataAssoAry['meanChart']['xAxisItem'][] = $standardArr[$i].$percent_symbol;
	}
	$x .="</tr>";
	$x .="</thead>";
	$x .="<tbody>";
	//ALLYEAR HANDING
	$x .="<tr>";
	$x .="<td>";
	$x .= $Lang['SDAS']['averageValue'];
	$x .="</td>";
	for($i=0;$i<$parts;$i++){
		$number = $finalResult["ALLYEAR"] [$i]['number'];
		$AddonNumber["ALLYEAR"] = $number+$AddonNumber["ALLYEAR"];
		
		if($finalResult["ALLYEAR"]['TotalNumber'] > 0){
			$percent= $AddonNumber["ALLYEAR"]/$finalResult["ALLYEAR"]['TotalNumber'] *100;
		}
		$percent = $AddonNumber["ALLYEAR"]? round($percent):0;
		if($option_checkbox){	//case reserve data
			$percent = 100 - $percent;
		}
		if($i!==$filter){
			$x .="<td>";
			$x .= $percent."%";
			$x .="</td>";
		}
		//HighChart Data
		$data["ALLYEAR"]['name']=(string) $Lang['SDAS']['averageValue'];
		$data["ALLYEAR"]['data'][] = $percent;
		
	}
	$x .="</tr>";
	
	foreach ($groupBy as $key){
		$CurrentAYID = $key['AcademicYearID'];
		unset($StudentID_Pass_Ary);
		unset($StudentID_Pass);
		$x .="<tr>";
			$x .="<td>";
				$x .=$academicYearArr[$key['AcademicYearID']] ['name'];
			$x .="</td>";
		for($i=0;$i<$parts;$i++){
			$StudentID_Pass_Ary =  array_merge((array)$StudentID_Pass_Ary,(array)$finalResult[$key['AcademicYearID']][$i]['studentIDAry']);
			$StudentID_Pass = implode(",",(array)$StudentID_Pass_Ary);
			$number = $finalResult[$key['AcademicYearID']] [$i]['number'];
			$AddonNumber[$key['AcademicYearID']] = $number+$AddonNumber[$key['AcademicYearID']];
			
			if($finalResult[$key['AcademicYearID']]['TotalNumber'] > 0){
				$percent= $AddonNumber[$key['AcademicYearID']]/$finalResult[$key['AcademicYearID']]['TotalNumber'] *100;
			}
			$percent = $AddonNumber[$key['AcademicYearID']]? round($percent):0;
			if($option_checkbox){	//case reserve data
				$percent = 100 - $percent;
			}
			if($i!==$filter){
// 				$x .="<td>";
// 					$x .= $percent."%";
// 				$x .="</td>";
				$x .= "<td>".$linterface->Get_Thickbox_Link('420', '620',"",$academicYearArr[$key['AcademicYearID']]['name']."&nbsp;".$data[$i]['name'], "jsOnloadDetailThickBox('$StudentID_Pass','$CurrentAYID')","FakeLayer",$percent.'%')."</td>";
			}
			//HighChart Data
			$data[$key['AcademicYearID']]['name']=(string) $academicYearArr[$key['AcademicYearID']] ['name'];
			$data[$key['AcademicYearID']]['data'][] = $percent;
				
		}
		$x .="</tr>";
	}
	$x .="</tr>";
	
	$chartSetting =  '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}%</b><br/>';
}elseif($chartType=='distribution_chart'){
	# 2020-05-04 (Philips) [2019-0710-1725-23085] - Improvement of chart
	$chartType = '';
	$chartyAxisMax = '';
	$chartyAxisTitle = $iPort['report_col']['improved_by_percentage'].$Lang['SDAS']['Amount'];
	
	### Header ###
	$x .="<thead>";
	$x .="<tr>";
	$x .= "<th style='width: 20%; white-space: nowrap;'>"." "."</th>";
	if(!$option_checkbox){ // for correct display order
		$standardArr = array_reverse($standardArr);
	}
	for($i=0;$i<$parts;$i++){
		if($i!==$filter){
			if($SDAS['SKHTST_CUST']){
				$symbol = "";
			}else{
				if($option_checkbox){
					$symbol = ">=";
				}else{
					$symbol = "<=";
				}
			}
			$x.= "<th>".$symbol.$standardArr[$i].$percent_symbol."</th>";
		}
		$highChartDataAssoAry['meanChart']['xAxisItem'][] = $standardArr[$i].$percent_symbol;
	}
	$x .="</tr>";
	$x .="</thead>";
	$x .="<tbody>";
	//ALLYEAR HANDING
	$x .="<tr>";
	$x .="<td>";
	$x .= $Lang['General']['Total'];
	$x .="</td>";
	for($k=0;$k<$parts;$k++){
		if($option_checkbox){  // for correct display order
			$i = $k;
		} else {
			$i = $parts - 1 - $k;
			$filter = $parts - 1;
		}
		$number = $finalResult["ALLYEAR"] [$i]['number'];
		$AddonNumber["ALLYEAR"] = $number;
		if($i!==$filter){
			$x .="<td>";
			$x .= $AddonNumber["ALLYEAR"] ? $AddonNumber["ALLYEAR"] : '0';
			$x .="</td>";
		}
		//HighChart Data
		$data["ALLYEAR"]['name']=(string) $Lang['General']['Total'];
		$data["ALLYEAR"]['data'][] = $AddonNumber["ALLYEAR"] ? $AddonNumber["ALLYEAR"] : 0;
		
	}
	$x .="</tr>";
	
	foreach ($groupBy as $key){
		$CurrentAYID = $key['AcademicYearID'];
		unset($StudentID_Pass_Ary);
		unset($StudentID_Pass);
		$x .="<tr>";
		$x .="<td>";
		$x .=$academicYearArr[$key['AcademicYearID']] ['name'];
		$x .="</td>";
		for($k=0;$k<$parts;$k++){
			if($option_checkbox){  // for correct display order
				$i = $k;
			} else {
				$i = $parts - 1 - $k;
				$filter = $parts - 1;
			}
			$StudentID_Pass_Ary =  array_merge((array)$StudentID_Pass_Ary,(array)$finalResult[$key['AcademicYearID']][$i]['studentIDAry']);
			$StudentID_Pass = implode(",",(array)$StudentID_Pass_Ary);
			$number = $finalResult[$key['AcademicYearID']] [$i]['number'];
			$AddonNumber[$key['AcademicYearID']] = $number;
			if($i!==$filter){
				$x .= "<td>".$linterface->Get_Thickbox_Link('420', '620',"",$academicYearArr[$key['AcademicYearID']]['name']."&nbsp;".$data[$i]['name'], "jsOnloadDetailThickBox('$StudentID_Pass','$CurrentAYID')","FakeLayer",$AddonNumber[$key['AcademicYearID']] ? $AddonNumber[$key['AcademicYearID']] : '0')."</td>";
			}
			//HighChart Data
			$data[$key['AcademicYearID']]['name']=(string) $academicYearArr[$key['AcademicYearID']] ['name'];
			$data[$key['AcademicYearID']]['data'][] = $AddonNumber[$key['AcademicYearID']] ? $AddonNumber[$key['AcademicYearID']] : 0;
			
		}
		$x .="</tr>";
	}
	$x .="</tr>";
	
	$chartSetting =  '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>';
	
}
$x .="</tbody>";
$x .="</table>";
$x .="</div>";

######## Table Data END ########

######## Group Data START ########
if(!empty($data)){
	foreach($data as $_data){
		$highChartDataAssoAry['meanChart']['series'][] = array('name' => $_data['name'], 'data' => $_data['data']);
	}
}

// convert highchart data to json
$highChartDataAssoAry['meanChart']['xAxisItemJson'] = $json->encode($highChartDataAssoAry['meanChart']['xAxisItem']);
$highChartDataAssoAry['meanChart']['seriesJson'] = $json->encode($highChartDataAssoAry['meanChart']['series']);

/* */
######## Group Data END ########

######## UI START ########
##### Table 2 Data Start #####
$table2 = "";
$table2 .="<div class='chart_tables'>";
	$table2 .="<table class = 'common_table_list_v30 view_table_list_v30'>";
	### Table2 Head Start ###
		$table2 .="<thead>";
			$table2 .= "<tr>";
				$table2 .= "<th style='width: 20%; white-space: nowrap;'>"." "."</th>";
				//ALL YEAR
				$table2 .= "<th>".$Lang['SDAS']['averageValue']."</th>";
				$highChartData2[0]['name'] = $Lang['SDAS']['averageValue'];
				// $table2 .= "<th width=15%>".$Lang['SDAS']['WholeForm']."</th>";
				$i = 1;
			foreach($groupBy as $key){
				$table2 .= "<th>".$academicYearArr[$key['AcademicYearID']] ['name']."</th>";
				$highChartData2[$i]['name'] = $academicYearArr[$key['AcademicYearID']] ['name'];
				$i++;
			}
			$table2 .= "</tr>";
		$table2 .= "</thead>";
		### Table2 Head END ###
		### Table Body Start ###
		$table2 .= "<tbody>";
			
			### Student Number Start###
			$table2 .= "<tr>";
				$table2 .= "<td>";
					$table2 .= $Lang['SDAS']['totalStudents'];
				$table2 .= "</td>";
				//ALL YEAR
				$table2 .= "<td>";
					$table2 .= $finalResult['ALLYEAR']['TotalNumber'];
				$table2 .= "</td>";
				
				foreach ($groupBy as $key){
					$table2 .= "<td>";
						$table2 .= $finalResult[$key['AcademicYearID']]['TotalNumber'];
					$table2 .= "</td>";
				}
			$table2 .= "</tr>";
			### Student Number End###
				
			### Average Start###
			$table2 .= "<tr>";
				$table2 .= "<td>";
					$table2 .= $Lang['SDAS']['averageScore'] ;
				$table2 .= "</td>";
				
				//ALL YEAR
				$table2 .= "<td>";
					$table2 .= round($finalResult['ALLYEAR']['TotalMarks']/$finalResult['ALLYEAR']['TotalNumber'],2);
				$table2 .= "</td>";
				
				foreach ($groupBy as $key){
					$table2 .= "<td>";
						$table2 .= round($finalResult[$key['AcademicYearID']]['TotalMarks']/$finalResult[$key['AcademicYearID']]['TotalNumber'],2);
						if($base_checkbox){
							$table2 .= "%";
						}
						$table2 .= "</td>";
				}
			$table2 .= "</tr>";
			### Average End###
				
			### Tatal Pass Start###
			$table2 .= "<tr>";
				$table2 .= "<td>";
					$table2 .= $Lang['SDAS']['passPercent'];
				$table2 .= "</td>";
				// 	$highChartDataAssoAry2['meanChart']['xAxisItem'][] = "ALL";
				$table2 .= "<td>";
					$pass_percentage = round($finalResult['ALLYEAR']['PassNumber']*100/$finalResult['ALLYEAR']['TotalNumber'],2);
					$table2 .= $pass_percentage;
					$table2 .= "%";
					$highChartData2[0]['data'][] = $pass_percentage;
				$table2 .= "</td>";
				$i = 1;
				foreach ($groupBy as $key){
					$table2 .= "<td>";
						$pass_percentage = round($finalResult[$key['AcademicYearID']]['PassNumber']*100/$finalResult[$key['AcademicYearID']]['TotalNumber'],2);
						$table2 .= $pass_percentage;
						$table2 .= "%";
						$highChartData2[$i]['data'][] = $pass_percentage;
					$table2 .= "</td>";
					$i++;
				}
			$table2 .= "</tr>";
		### Tatal Pass END###
		$table2 .= "</tbody>";
		### Table Body END ###
	$table2 .= "</table>";
$table2 .= "</div>";
##### Table 2 Data End #####
// 2020-07-29 (Philips) - Sort total in first order
ksort($highChartData2);
### Chart 2 Start ###
foreach($highChartData2 as $_data){
	$highChartDataAssoAry2['meanChart']['series'][] = array('name' => $_data['name'], 'data' => $_data['data']);
}
$highChartDataAssoAry2['meanChart']['xAxisItemJson'] = $json->encode($highChartDataAssoAry2['meanChart']['xAxisItem']);
$highChartDataAssoAry2['meanChart']['seriesJson'] = $json->encode($highChartDataAssoAry2['meanChart']['series']);
### Chart 2 End ###
?>

<!-- -------- Generate bar chart START -------- -->

<style>
.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.bar {
  fill: steelblue;
}

.x.axis path {
  display: none;
}

</style>

<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<?php if($display_checkbox['chart']){?>
<div class="chart" style="height: 500px">
</div>

<?php if(!empty($finalResult)){?>
<script>
// var 
xAxis='<?=$highChartDataAssoAry['meanChart']['xAxisItemJson']?>';
series='<?=$highChartDataAssoAry['meanChart']['seriesJson']?>';

xAxis = JSON.parse(xAxis);
series = JSON.parse(series);

$('.chart').highcharts({
    chart: {
        type: '<?=$chartType ?>'
    },
    title: {
        text: $('#subj_1 option:selected').text() + ' <?=$Lang['SDAS']['ClassSubjectComparsion']['YearComparison']?>'
    },
    xAxis: {
        
     categories: xAxis,
     crosshair: true
},
    yAxis: {
        min: 0,
        <?php if($chartyAxisMax!=''){ echo 'max: ' . $chartyAxisMax.','; }?>
        title: {
            text: '<?=$chartyAxisTitle?>'
        },
        minTickInterval: 1
        /*
        title: {
            text: '<?=$iPort['report_col']['improved_by_percentage'] ?>'
        }*/
    },
    tooltip: {
        pointFormat:'<?=$chartSetting?>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },
    series: series,
    credits : {enabled: false,},

});

function jsOnloadDetailThickBox(StudentID,AcademicYearID){
	$.ajax({
		type: "POST",
		url: "?t=ajax.ajax_get_student_detail_thickbox", 
		data: {
			 "StudentID" : StudentID,
			 "AcademicYearID" : AcademicYearID,
			 "Semester" : '<?=$Semester?>',
			 "SubjectID" : '<?=$FromSubjectID?>'
		},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}
</script>
<?php }else{?>
<script>
$('.chart').hide();
</script>
<?php }?>
<?php }?>
<br/>

<?php if($display_checkbox['table'])
		echo $x;
?>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<?php if($display_checkbox['chart']){?>
<div class="chart2" style="height: 500px">
</div>

<script>
xAxis='<?=$highChartDataAssoAry2['meanChart']['xAxisItemJson']?>';
series='<?=$highChartDataAssoAry2['meanChart']['seriesJson']?>';

xAxis = JSON.parse(xAxis);
series = JSON.parse(series);

$('.chart2').highcharts({
    chart: {
        type: 'column'
    },
    title: {
        text: $('#subj_1 option:selected').text() + ' <?=$Lang['SDAS']['passPercent']?>'
    },
    xAxis: {
    	categories: [
    	                '<?=$Lang['SDAS']['toCEES']['SchoolYear']?>'
    	            ],
    	crosshair: true
	},
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: '<?=$iPort['report_col']['improved_by_percentage'] ?>'
        }
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: series,
    credits : {enabled: false,},
});
</script>
<?php }?>
<!-- -------- Generate bar chart END -------- -->

<br/>


<!-- -------- Generate table START -------- -->
<?php if($display_checkbox['table'])
		echo $table2;
?>

<script>$(function() {$('#resultTable').floatHeader();});</script>
<!-- -------- Generate table END -------- -->



<?php


intranet_closedb();
?>