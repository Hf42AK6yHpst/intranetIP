<?php
/*
 * Change Log:
 * Date 2020-01-08 Philips Added firstItem [DM#3714]
 * Date	2017-02-07 Villa Add $tag to change the select tag
 * Date 2016-12-14 Villa Open the file
 */
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();
$lpf = new libpf_slp();
$sql = "
		SELECT 
			distinct assr.Semester, assr.TermAssessment, assr.YearTermID
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr
			INNER JOIN 
			ACADEMIC_YEAR_TERM ayt
				ON ayt.YearTermID = assr.YearTermID
		WHERE
			assr.AcademicYearID = {$AcademicYearID}
		AND 
			assr.Semester !=''
		ORDER BY
			ayt.TermStart, assr.TermAssessment";
			
$rs = $li_pf->returnResultSet($sql);
if($tag){
	//do nth
}else{
	$tag = " id='year_term' name='YearTermID'";
}
$selection_box = "<select $tag>";
if(!$hideOverall){
    $selection_box .= "<option value=''>". ($firstItem ? $firstItem : $ec_iPortfolio['overall_result']) ."</option>";
}
foreach ($rs as $_rs){
	if(!empty($_rs['TermAssessment'])){
		$selection_box .= "<option value='".$_rs['Semester']."_".$_rs['TermAssessment']."' data-year-term-id='{$_rs['YearTermID']}'>".$_rs['TermAssessment']."</option>";
	}else{
		$selection_box .= "<option value='".$_rs['Semester']."' data-year-term-id='{$_rs['YearTermID']}'>".$_rs['Semester']."</option>";
	}
}
$selection_box .= "</select>";
echo $selection_box;

?>