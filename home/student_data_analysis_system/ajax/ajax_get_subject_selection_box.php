<?php
//Using: 
/**
 * Change Log:
 * change the include for SDAS
 *
 * 2017-12-12 
 *  -	Add $firstItem for passing first item title
 *
 * 2017-05-10 Villa #T116811 
 *  -	Add FilterSubject For SubjectPanel Access Right
 *  
 * 2017-05-10 Villa #T116799
 *  -	Fix getting wrong y_id when  $class['ClassTitleEN']|| $class['ClassTitleB5'] is NULL
 *  
 * 2017-01-12 Villa
 * -	Add checking if no returning result
 * -	Support Subject Component
 * 2016-12-14 Villa
 * -	Add $FirstSelection
 *
 */

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");

// if($intranet_session_language == 'b5'){
// 	include_once($PATH_WRT_ROOT."lang/lang.b5.utf8.php");
// }

intranet_auth();
intranet_opendb();
$subject_selection_html = '';
$lib = new interface_html();
if($AllSubject){
	$FirstSelection = 'allSubject';
}else if($firstItem){
    $FirstSelection = $firstItem;
}else{
	$FirstSelection = '';
}

#### Get yearID for class teacher START ####
if(($yearClass || $yearClassId) && !$y_id){
	$objYear = new Year();
	$classArr = $objYear->Get_All_Classes();
	foreach((array)$classArr as $class){
		if($yearClass){
			if($class['ClassTitleEN'] == $yearClass || $class['ClassTitleB5'] == $yearClass){
				$y_id = $class['YearID'];
				break;
			}
		}elseif($yearClassId){
			if($class['YearClassID'] == $yearClassId){
				$y_id = $class['YearID'];
				break;
			}
		}
// 		if($class['ClassTitleEN'] == $yearClass || $class['ClassTitleB5'] == $yearClass || $class['YearClassID'] == $yearClassId){
// 			$y_id = $class['YearID'];
// 			break;
// 		}
	}
	$subject_selection_html .= '<input type="hidden" name="yearID" value="'.$y_id.'" />';
}
#### Get yearID for class teacher END ####

$li_pf = new libpf_asr();
$lpf = new libportfolio();

#### Include Group Subject START ####
$includeCodeIdArr = array_keys((array)$sys_custom['iPortfolio_Group_Subject']);
#### Include Group Subject END ####

#### Remove Group Subject START ####
## MSSCH Cust START ##
$removeCodeIdArr = array();
foreach ((array)$sys_custom['iPortfolio_Group_Subject'] as $codeArr){
	$removeCodeIdArr = array_merge($removeCodeIdArr, $codeArr);
}
## MSSCH Cust END ##
#### Remove Group Subject END ####

$subj_arr = $li_pf->returnAllSubjectWithComponents($ay_id, $y_id, $includeCodeIdArr, $removeCodeIdArr, true);

$subject_selection_html .= "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
// $subject_selection_html .= "<tr><td colspan=\"3\"><input type=\"checkbox\" id=\"SubjCheckMaster\" class=\"checkMaster\" /> <label for=\"SubjCheckMaster\" class=\"checkMaster\">{$button_check_all}</label></td></tr>";
foreach ((array)$subj_arr as $numOfArr =>$value){
	#Enquiry 
	if(isset($filterSubject) && !in_array($value['SubjectID'],(array)$filterSubject)){
		continue;
	}
	
	if($value['SubjectComponentID']==0){
		$SubjectID = $value['SubjectID']."_"."0";
		$SubjectName = $value['SubjectName'];
	}else{
		$SubjectID = $value['SubjectID']."_".$value['SubjectComponentID'];
		$SubjectName =  $value['SubjectName']."_".$value['SubjectComponentName'];
	}
	$_subj_arr[ $SubjectID] ['SubjectID'] = $SubjectID;
	$_subj_arr[ $SubjectID] ['SubjectName'] = $SubjectName;
}
$i =0;
foreach ((array)$_subj_arr as $value){
	$subject_arr[$i][] = $value['SubjectID'];
	$subject_arr[$i][] = $value['SubjectName'];
	$i++;
}
if(!($_subj_arr)){
	$subject_selection_html = "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" >";
	$subject_selection_html .= "<tr>";
	$subject_selection_html .= "<td>";
	$subject_selection_html .= $Lang['SDAS']['Error']['noSubject'] ;
	$subject_selection_html .= "</td>";
	$subject_selection_html .= "</tr>";
	$subject_selection_html .= "</table>";
	echo $subject_selection_html;
	return false;
}

$subject_selection_html .= "<tr>";
$subject_selection_html = $lib->GET_SELECTION_BOX($subject_arr,'name="Subjects" id="subj_1"',$FirstSelection);
$subject_selection_html .= "</tr>";
$subject_selection_html .= "</table>";

echo $subject_selection_html;

intranet_closedb();
?>