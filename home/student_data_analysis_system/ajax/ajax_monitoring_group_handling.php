<?php
//Using:
/**
 * Change Log:
 * 2020-06-11 Philips
 * 	- modified getGroupSubject, use returnAllSubjectWithComponents to avoid subject missing
 */
switch ($ACTION){
	case 'getCurrentYearGroup':
		if(!empty($GroupIDArr)){
			$result = '<select name="monitoringGroup" id="monitoringGroup">';
			foreach ((array)$GroupIDArr as $GroupID){
				$monitoringGroupInfo = $objSDAS->getMonitoringGroup($GroupID,$AcademicYearID);
				$monitoringGroupInfo = $monitoringGroupInfo[0];
				if($monitoringGroupInfo){
					$result .= '<option value="'.$monitoringGroupInfo['GroupID'].'">'.Get_Lang_Selection($monitoringGroupInfo['GroupNameCH'], $monitoringGroupInfo['GroupNameEN']).'</option>';
				}
			}
			$result .= '</select>';
		}else{
			$result = $Lang['SDAS']['NoRecord']['A'];
		}
		
		break;
	case 'getGroupSubject':
		$li_pf = new libpf_asr();
		#### Include Group Subject START ####
		$includeCodeIdArr = array_keys((array)$sys_custom['iPortfolio_Group_Subject']);
		#### Include Group Subject END ####
		
		#### Remove Group Subject START ####
		## MSSCH Cust START ##
		$removeCodeIdArr = array();
		foreach ((array)$sys_custom['iPortfolio_Group_Subject'] as $codeArr){
			$removeCodeIdArr = array_merge($removeCodeIdArr, $codeArr);
		}
		## MSSCH Cust END ##
		#### Remove Group Subject END ####
		
		$student = $objSDAS->getMonitoringGroupMember('2', $GroupID);
		$studentID = Get_Array_By_Key($student, 'UserID');
		# Get Student Class Level
		$sql = '
			SELECT DISTINCT
				y.YearID
			FROM YEAR_CLASS_USER ycu
			INNER JOIN YEAR_CLASS yc
				ON ycu.YearClassID = yc.YearClassID
			INNER JOIN YEAR y
				ON yc.YearID = y.YearID
			WHERE ycu.UserID IN ("'.implode('","',(array)$studentID).'")
			AND
				yc.AcademicYearID = '.$AcademicYearID.'
		';
		$yIDArr = $objSDAS->returnResultSet($sql);
		$yIDArr = Get_Array_By_Key($yIDArr, 'YearID');
		$allSubjectIDArr = array();
		$subjectArr = array();
		foreach($yIDArr as $y_id){
			$subj_arr = $li_pf->returnAllSubjectWithComponents($AcademicYearID, $y_id, $includeCodeIdArr, $removeCodeIdArr, true);
			$subjectIDArr = Get_Array_By_Key($subj_arr, 'SubjectID');
			$subjectCMPIDArr = Get_Array_By_Key($subj_arr, 'SubjectComponentID');
			$tempSubjectIDArr = array_merge($subjectIDArr,$subjectCMPIDArr);
			$subjectArr = array_merge($subjectArr, $subj_arr);
			$allSubjectIDArr = array_merge($allSubjectIDArr,$tempSubjectIDArr);
		}
		$allSubjectIDArr = array_unique($allSubjectIDArr);
		/*
		$sql = '
			SELECT 
				DISTINCT
					SubjectID, SubjectComponentID
			FROM 
				'.$eclass_db.'.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
			WHERE
				UserID IN ("'.implode('","',(array)$studentID).'")
			AND
				AcademicYearID = '.$AcademicYearID.'
		';
		$subjectArr = $objSDAS->returnResultSet($sql);
		$subjectIDArr = Get_Array_By_Key($subjectArr, 'SubjectID');
		$subjectCMPIDArr = Get_Array_By_Key($subjectArr, 'SubjectComponentID');
		$allSubjectIDArr = array_merge($subjectIDArr,$subjectCMPIDArr);
		*/
		$sql = 'Select 
					RecordID, EN_DES, CH_DES 
				FROM
					ASSESSMENT_SUBJECT
				WHERE 
					RecordID IN ("'.implode('","',$allSubjectIDArr).'")';
		$temp = $objSDAS->returnResultSet($sql);
		$subjectMappingArr = BuildMultiKeyAssoc($temp, 'RecordID');
		
		$subj_arr = '';
		foreach ((array)$subjectArr as $key=>$_subjectArr){
			$subj_arr[$key]['SubjectID'] = $_subjectArr['SubjectID'];
			$subj_arr[$key]['SubjectName'] = Get_Lang_Selection($subjectMappingArr[ $_subjectArr['SubjectID']]['CH_DES'], $subjectMappingArr[ $_subjectArr['SubjectID']]['EN_DES']);
			if($subjectArr[$key]['SubjectComponentID']){
				$subj_arr[$key]['SubjectComponentID'] = $_subjectArr['SubjectComponentID'];
				$subj_arr[$key]['SubjectComponentName'] = Get_Lang_Selection($subjectMappingArr[$_subjectArr['SubjectComponentID']]['CH_DES'], $subjectMappingArr[ $_subjectArr['SubjectComponentID']]['EN_DES']);
			}
		}
		if($subj_arr){
			foreach ((array)$subj_arr as $numOfArr =>$value){
				if($value['SubjectComponentID']){
					$_subj_arr[ $numOfArr] ['SubjectID'] = $value['SubjectID'].'_'.$value['SubjectComponentID'];
					$_subj_arr[ $numOfArr] ['SubjectName'] = $value['SubjectName'].'-'.$value['SubjectComponentName'];
					$_subj_arr[$numOfArr]['Checked'] = true;
					if($sys_custom['SDAS']['SKHTST']['form_student_performance']['defaultSbjSelect']['isActivate']){
						$_subj_arr[$numOfArr]['Checked'] = false;
					}
				}else{
					$_subj_arr[ $numOfArr] ['SubjectID'] = $value['SubjectID'].'_'.'0';
					$_subj_arr[ $numOfArr] ['SubjectName'] = $value['SubjectName'];
					$_subj_arr[$numOfArr]['Checked'] = true;
					if($sys_custom['SDAS']['SKHTST']['form_student_performance']['defaultSbjSelect']['isActivate']){
						$_subj_arr[$numOfArr]['Checked'] = false;
						if($task == "sbjSelect2" && in_array($value['SubjectID'], $sys_custom['SDAS']['SKHTST']['form_student_performance']['defaultSbjSelect']['defaultSubj2'])){
							$_subj_arr[$numOfArr]['Checked'] = true;
						}else if($task == "sbjSelect1"){
							$_subj_arr[$numOfArr]['Checked'] = true;
						}
					}
				}
			}
			$defaultChecked = 1;
			$subject_arr[0][0] = $CheckBoxName.'_0';
			$subject_arr[0][1] = $CheckBoxName.'[]';
			$subject_arr[0][2] = '0';
			$subject_arr[0][3] = $defaultChecked;
// 			$subject_arr[0][3] = '1';
			$subject_arr[0][4] = $CheckBoxName.'_arr';
			$subject_arr[0][5] = $ec_iPortfolio['overall_result'];
			$i =1;
			foreach ((array)$_subj_arr as $value){
				$subject_arr[$i][0] = $CheckBoxName.$value['SubjectID'];
				$subject_arr[$i][1] = $CheckBoxName.'[]';
				$subject_arr[$i][2] = $value['SubjectID'];
// 				$subject_arr[$i][3] = '1';
				$subject_arr[$i][3] = $value['Checked'];
				$subject_arr[$i][4] = $CheckBoxName.'_arr';
				$subject_arr[$i][5] = $value['SubjectName'];;
				$i++;
			}
			$subject_selection_html = '';
			$subject_selection_html .= "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
			$subject_selection_html .= "<tr>";
			$subject_selection_html .= "<td>";
			$subject_selection_html .= $linterface->Get_Checkbox_Table($CheckBoxName.'_arr',$subject_arr);
			$subject_selection_html .= "</tr>";
			$subject_selection_html .= "</td>";
			$subject_selection_html .= "</table>";
		}else{
			$subject_selection_html = '';
			$subject_selection_html .= "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
			$subject_selection_html .= "<tr>";
			$subject_selection_html .= "<td>";
			$subject_selection_html .= $Lang['SDAS']['Error']['noSubject'];
			$subject_selection_html .= "</tr>";
			$subject_selection_html .= "</td>";
			$subject_selection_html .= "</table>";
		}
		$result = $subject_selection_html;
		unset($subject_selection_html);
		break;
}
echo $result;
?>