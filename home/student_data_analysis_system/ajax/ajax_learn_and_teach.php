<?php 
//Using: Pun
/**
 * Change Log:
 * 2017-05-26 Pun
 *  - File Create
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");;
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/learnAndTeach/learnAndTeach.php");

######## INIT START ########
intranet_auth();
intranet_opendb();
$objDB = new libdb();
$year = new Year();
$yc = new year_class();
$lpf = new libportfolio();
$sbj = new subject();
$lc = new Learning_Category();
$libSDAS = new libSDAS();
$lnt = new learnAndTeach($objDB);

define('DECIMAL_PLACES', 2);

$accessRight = $lpf->getAssessmentStatReportAccessRight();
######## INIT END ########

######## Safe SQL START ########
$searchAcademicYearID = IntegerSafe($academicYearID);

if($reportType == 1){
    if($accessRight['admin']){
        $searchTeacherID = (array)IntegerSafe($teacherID);
    }else{
        $searchTeacherID = array($_SESSION['UserID']);
    }
}else{
    $searchLearningCategoryID = IntegerSafe($learningCategoryID);
}
######## Safe SQL END ########

######## Access Right START ########
$allowAccess = false;

if($accessRight['admin']){
    $allowAccess = true;
}else if($reportType == 1){
    $allowAccess = true;
}else if($reportType == 2){
    if($learningCategoryID){

        $allSubject = $lnt->getAllSubject();
        $subjectLearningCategory = BuildMultiKeyAssoc($allSubject, array('SubjectID') , array('LearningCategoryID'), $SingleValue=1);
        
        $allowSubjectIdArr = array_keys($accessRight['subjectPanel']);
        $allowLearningCategoryIdArr = array();
        foreach($allowSubjectIdArr as $subjectID){
            $allowLearningCategoryIdArr[] = $subjectLearningCategory[$subjectID];
        }
        
        $allowYearIdArr = array();
        foreach($allowSubjectIdArr as $subjectID){
            $learningCategoryID = $subjectLearningCategory[$subjectID];
            $allowYearIdArr[$learningCategoryID] = array_merge(
                (array)$allowYearIdArr[$learningCategoryID], 
                array_keys((array)$accessRight['subjectPanel'][$subjectID])
            );
        }
        
        $allowAccess = in_array($searchLearningCategoryID, $allowLearningCategoryIdArr);
    }else{
        $allowAccess = !!count($accessRight['subjectPanel']);
    }
}

if(!$allowAccess){
    echo 'Access Denied.';
    exit;
}
######## Access Right END ########


######################## Get Data START ########################

######## Helper function START ########
function getNumberInString($str){
    return preg_replace('/[^\d]/', '', $str);
}
######## Helper function END ########

#### Get Year START ####
$rs = $year->Get_All_Year_List();
$allYear = array();
foreach($rs as $r){
    $form = getNumberInString($r['YearName']);
    if($form){
        $allYear[ $form ] = $r['YearID'];
    }
}
#### Get Year END ####

######## Get Year START ########
$allClass = $yc->Get_All_Class_List($academicYearID);
$yearClassYearId = BuildMultiKeyAssoc($allClass, array('YearClassID') , array('YearID'), $SingleValue=1);
######## Get Year END ########

######## Get Question START ########
$allQuestion = $lnt->getAllQuestionByAcademicYearId($academicYearID);
$questionCount = count($allQuestion);
######## Get Question END ########

######## Get Answer START ########
$allAnswer = $lnt->getAllRecordByAcademicYearId($academicYearID);

#### Filter Normal Teacher Answer for Report 1 START ####
if($reportType == 1){
    $_allAnswer = array();
    foreach($allAnswer as $answer){
        if(in_array($answer['TeacherID'], $searchTeacherID)){
            $_allAnswer[] = $answer;
        }
    }
    $allAnswer = $_allAnswer;
}
#### Filter Normal Teacher Answer for Report 1 END ####

#### Filter Subject Panel Answer for Report 2 START ####
if($reportType == 2){
    $_allAnswer = array();
    foreach($allAnswer as $answer){
        $answerLearningCategoryID = $answer['LearningCategoryID'];
        $answerYearID = $answer['YearID'];
        
        if($answerLearningCategoryID != $searchLearningCategoryID){
            continue;
        }
        
        if(
            $accessRight['admin'] ||
            in_array($answerYearID, (array)$allowYearIdArr[$answerLearningCategoryID])
        ){
            $_allAnswer[] = $answer;
        }
    }
    
    $allAnswer = $_allAnswer;
}
$allAnswerAssoArr = BuildMultiKeyAssoc($allAnswer, array('YearClassID', 'SubjectID', 'TeacherID', 'QuestionSequence'));
#### Filter Subject Panel Answer for Report 2 END ####
######## Get Answer END ########


######## Get Teacher Name START ########
$teacherIds = array_unique(Get_Array_By_Key($allAnswer, 'TeacherID'));
$teacherIdsSql = implode("','", $teacherIds);

$nameField = getNameFieldByLang2('').' As Name';
$sql = "SELECT UserID, {$nameField} FROM INTRANET_USER WHERE UserID IN ('{$teacherIdsSql}') ORDER BY Name";
$rs = $objDB->returnResultSet($sql);

$allTeacherName = BuildMultiKeyAssoc($rs, array('UserID') , array('Name'), $SingleValue=1);
######## Get Teacher Name END ########

######## Get Subject Name START ########
$rs = $sbj->Get_All_Subjects();

$allSubjectName = array();
foreach($rs as $r){
    $allSubjectName[$r['RecordID']] = Get_Lang_Selection($r['CH_DES'],$r['EN_DES']);
}
/*$rs = $lc->Get_All_Learning_Category(1);
$allSubjectName = array();
foreach($rs as $learningCategoryID => $r){
    $allSubjectName[$learningCategoryID] = $r['Name'];
}*/
######## Get Subject Name END ########

######## Get Class Name START ########
$rs = $yc->Get_All_Class_List($academicYearID);
$allClassName = BuildMultiKeyAssoc($rs, array('YearClassID') , array('ClassTitle'), $SingleValue=1);

$rs = $lnt->getAllRecordClass();
foreach($rs as $r){
    if($r['YearClassID']){
        continue;
    }
    $allClassName[$r['SpecialClass']] = $r['SpecialClass'];
}
natsort($allClassName);
######## Get Class Name END ########
######################## Get Data END ########################

######################## Calculate Data START ########################
######## Calculate Min/Max/Avg START ########
if($reportType == 1){
    $allOverallRecordStat = $lnt->getTeacherOverallMeanDetailsByAcademicYearId($academicYearID);
    $allOverallQBasedRecordStat = $lnt->getTeacherOverallQBasedMeanDetailsByAcademicYearId($academicYearID);
    $allRecordStat = $lnt->getMeanDetailsByAcademicYearIdTeacherId($academicYearID, $searchTeacherID);
    $qBasedRecord = $lnt->getqBasedMeanDetailsByAcademicYearIdTeacherId($academicYearID, $searchTeacherID);
}else if($reportType == 2){
    $allOverallSubjectRecordStat = $lnt->getOverallMeanDetailsByAcademicYearIdLearningCategoryId($academicYearID, $searchLearningCategoryID);
    $allRecordStat = $lnt->getMeanDetailsByAcademicYearIdLearningCategoryId($academicYearID, $searchLearningCategoryID);
}
######## Calculate Min/Max/Avg END ########

######## Calculate Teacher/Subject Overall Average START ########
if($reportType == 1){
    $weightData = array();
    foreach($allSubjectName as $subjectID => $subjectName){
        foreach($allTeacherName as $teacherID => $teacherName){
            foreach($allClassName as $yearClassID => $className){
                $totalCount = $allRecordStat[$yearClassID][$subjectID][$teacherID]['TotalCount'];
                $averageScore = $allRecordStat[$yearClassID][$subjectID][$teacherID]['Mean'];
                $weightData[$teacherID][] = array(
                    $averageScore,
                    $totalCount
                );
            }
        }
    }
    
    $allTeacherOverallAverage = array();
    foreach($weightData as $teacherID=>$dataArr){
        $allTeacherOverallAverage[$teacherID] = $libSDAS->CalculateWeightedArithmeticMean($dataArr);
    }
}else{
    $weightData = array();
    foreach($allRecordStat as $yearClassID => $d1){
        foreach($d1 as $subjectID => $d2){
            foreach($d2 as $teacherID => $data){
                $learningCategoryID = $subjectLearningCategory[$subjectID];
                $totalCount = $data['TotalCount'];
                $averageScore = $data['Mean'];
                
                if(!isset($allAnswerAssoArr[$yearClassID][$subjectID][$teacherID])){
                    continue;
                }
                
                $weightData[] = array(
                    $averageScore,
                    $totalCount
                );
            }
        }
    }
    
    $allSubjectOverallAverage = $libSDAS->CalculateWeightedArithmeticMean($weightData);
}
######## Calculate Teacher/Subject Overall Average END ########
######################## Calculate Data END ########################


######################## Pack Data START ########################
$resultRow = array();
foreach($allSubjectName as $subjectID => $subjectName){
    foreach($allClassName as $yearClassID => $className){
        foreach($allTeacherName as $teacherID => $teacherName){
            if(!isset($allAnswerAssoArr[$yearClassID][$subjectID][$teacherID])){
                continue;
            }
            
            $questionStat = array();
            for($i=1;$i<=$questionCount;$i++){
                $mean = $allAnswerAssoArr[$yearClassID][$subjectID][$teacherID][$i]['Mean'];
                if($showActualValue){
                    $questionStat[] = $mean;
                }else{
                    $questionStat[] = number_format($mean, DECIMAL_PLACES);
                }
            }
            
            if($reportType == 1){
                $totalCount = $allRecordStat[$yearClassID][$subjectID][$teacherID]['TotalCount'];
                $averageScore = $allRecordStat[$yearClassID][$subjectID][$teacherID]['Mean'];
            }else{
                $totalCount = $allRecordStat[$yearClassID][$subjectID][$teacherID]['TotalCount'];
                $averageScore = $allRecordStat[$yearClassID][$subjectID][$teacherID]['Mean'];
            }
            
            $resultRow[$subjectID][$yearClassID][$teacherID] = array(
                'QuestionStat' => $questionStat,
                'AverageScore' => $averageScore,
                'TotalCount' => $totalCount
            );
        }
    }
}
######################## Pack Data END ########################


######################## Display Result START ########################
?>
<script src="/templates/jquery/jquery.floatheader.min.js"></script>

<style>
.noWrap{
    white-space: nowrap;
}
.totalCount{
    display: block;
}
</style>

<?php if(count($resultRow) == 0): ?>
<!-- ################ No Record START ################ -->

<div class="chart_tables">
	<table class="common_table_list_v30 view_table_list_v30 resultTable" id="chart_tables_1">
		<tr><td><?=$Lang['General']['NoRecordFound'] ?></td></tr>
	</table>
</div>

<!-- ################ No Record END ################ -->
<?php elseif($reportType == 1): ?>
<!-- ################ Teacher Report START ################ -->

<div class="chart_tables">
	<table class="common_table_list_v30 view_table_list_v30 resultTable" id="chart_tables_1">
        <colgroup>
            <col style="width: 6.25%" />
            <col style="width: 6.25%" />
			<?php for($i=1;$i<=$questionCount;$i++){ ?>
          		<col style="width: 6.25%" />
			<?php } ?>
            <col style="width: 6.25%" />
        </colgroup>
		<thead>
			<tr>
				<th class="noWrap"><?=$iPort["report_col"]["subject"] ?></th>
				<th class="noWrap"><?=$Lang['SysMgr']['FormClassMapping']['Form']?> / <?=$Lang['Header']['Menu']['Class'] ?></th>
				<?php for($i=0;$i<$questionCount;$i++){ ?>
					<th class="noWrap" title="<?=$allQuestion[$i]['Title'] ?>">Q<?=$i+1 ?></th>
				<?php } ?>
				<th class="noWrap"><?=$Lang['SDAS']['averageScore'] ?></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			foreach($resultRow as $subjectID=>$d1){
			    $subjectName = $allSubjectName[$subjectID];
			    
			    foreach($d1 as $yearClassID=>$d2){
			        $className = $allClassName[$yearClassID];
			        
    			    foreach($d2 as $teacherID=>$data){
    			        $teacherName = $allTeacherName[$teacherID];
	        ?>
	        
	        <tr>
	        	<td class="noWrap"><?=$subjectName ?></td>
	        	<td class="noWrap"><?=$className ?></td>
	        	<?php foreach($data['QuestionStat'] as $stat){ ?>
	        		<td><?=$stat ?></td>
        		<?php } ?>
	        	<td>
	        		<?php if($showActualValue){ ?>
    	        		<?=$data['AverageScore'] ?>
    	        		<span class="totalCount" style="display:none;">Total Count: <?=$data['TotalCount'] ?></span>
	        		<?php }else{ ?>
	        			<?=number_format($data['AverageScore'], DECIMAL_PLACES) ?>
	        		<?php } ?>
	        	</td>
	        </tr>
	        
	        <?php
    			    }
			    }
			}
			?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2" rowspan="6">&nbsp;</td>
				<td colspan="<?= $questionCount-3 ?>" rowspan="6"><?=$Lang['SDAS']['LearnAndTeach']['ScoreRemark'] ?></td>
				<td colspan="2" ><b><?=$Lang['SDAS']['LearnAndTeach']['ListByClass'] ?>:</b></td>
				<td colspan="2" ><b><?=$Lang['SDAS']['LearnAndTeach']['ListByStdAns'] ?>:</b></td>
			</tr>
			<tr>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['TotalAverage'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allTeacherOverallAverage[$teacherID] ?>
	        		<?php }else{ ?>
        				<?=number_format($allTeacherOverallAverage[$teacherID], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['TotalAverage'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$qBasedRecord[$teacherID]['Mean'] ?>
	        		<?php }else{ ?>
        				<?=number_format($qBasedRecord[$teacherID]['Mean'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['OverallTeacherMin'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallRecordStat['Min'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallRecordStat['Min'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['OverallTeacherMin'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallQBasedRecordStat['Min'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallQBasedRecordStat['Min'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['OverallTeacherAvg'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallRecordStat['Mean'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallRecordStat['Mean'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['OverallTeacherAvg'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallQBasedRecordStat['Mean'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallQBasedRecordStat['Mean'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['OverallTeacherMax'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallRecordStat['Max'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallRecordStat['Max'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['OverallTeacherMax'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallQBasedRecordStat['Max'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallQBasedRecordStat['Max'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['OverallTeacherSD'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallRecordStat['SD'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallRecordStat['SD'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
				<td style="text-align: right;" class="noWrap"><?=$Lang['SDAS']['LearnAndTeach']['OverallTeacherSD'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallQBasedRecordStat['SD'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallQBasedRecordStat['SD'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

<!-- ################ Teacher Report END ################ -->
<?php elseif($reportType == 2): ?>
<!-- ################ Subject Report START ################ -->


<div class="chart_tables">
	<table class="common_table_list_v30 view_table_list_v30 resultTable" id="chart_tables_1">
        <colgroup>
            <col style="width: 6.25%" />
            <col style="width: 6.25%" />
			<?php for($i=1;$i<=$questionCount;$i++){ ?>
          		<col style="width: 6.25%" />
			<?php } ?>
            <col style="width: 6.25%" />
        </colgroup>
		<thead>
			<tr>
				<th class="noWrap"><?=$iPort["report_col"]["subject"] ?></th>
				<th class="noWrap"><?=$iPort["report_col"]["teacher"] ?></th>
				<th class="noWrap"><?=$Lang['SysMgr']['FormClassMapping']['Form']?> / <?=$Lang['Header']['Menu']['Class'] ?></th>
				<?php for($i=0;$i<$questionCount;$i++){ ?>
					<th class="noWrap" title="<?=$allQuestion[$i]['Title'] ?>">Q<?=$i+1 ?></th>
				<?php } ?>
				<th class="noWrap"><?=$Lang['SDAS']['averageScore'] ?></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			foreach($resultRow as $subjectID=>$d1){
			    $subjectName = $allSubjectName[$subjectID];
			    
			    foreach($d1 as $yearClassID=>$d2){
			        $className = $allClassName[$yearClassID];
			        
    			    foreach($d2 as $teacherID=>$data){
    			        $teacherName = $allTeacherName[$teacherID];
	        ?>
	        
	        <tr>
	        	<td class="noWrap"><?=$subjectName ?></td>
	        	<td class="noWrap"><?=$teacherName ?></td>
	        	<td class="noWrap"><?=$className ?></td>
	        	<?php foreach($data['QuestionStat'] as $stat){ ?>
	        		<td><?=$stat ?></td>
        		<?php } ?>
	        	<td>
	        		<?php if($showActualValue){ ?>
        				<?=$data['AverageScore'] ?></span>
    	        		<span class="totalCount" style="display:none;">Total Count: <?=$data['TotalCount'] ?></span>
	        		<?php }else{ ?>
    	        		<?=number_format($data['AverageScore'], DECIMAL_PLACES) ?>
	        		<?php } ?>
	        	</td>
	        </tr>
	        
	        <?php
    			    }
			    }
			}
			?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3" rowspan="5">&nbsp;</td>
				<td colspan="<?= $questionCount-2 ?>" rowspan="5"><?=$Lang['SDAS']['LearnAndTeach']['ScoreRemark'] ?></td>
				<td colspan="2" style="text-align: right;"><?=$Lang['SDAS']['LearnAndTeach']['TotalAverage'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allSubjectOverallAverage ?>
	        		<?php }else{ ?>
    					<?=number_format($allSubjectOverallAverage, DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right;"><?=$Lang['SDAS']['LearnAndTeach']['OverallSubjectMin'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallSubjectRecordStat['Min'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallSubjectRecordStat['Min'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right;"><?=$Lang['SDAS']['LearnAndTeach']['OverallSubjectAvg'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallSubjectRecordStat['Mean'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallSubjectRecordStat['Mean'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right;"><?=$Lang['SDAS']['LearnAndTeach']['OverallSubjectMax'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallSubjectRecordStat['Max'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallSubjectRecordStat['Max'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right;"><?=$Lang['SDAS']['LearnAndTeach']['OverallSubjectSD'] ?>:</td>
				<td>
	        		<?php if($showActualValue){ ?>
        				<?=$allOverallSubjectRecordStat['SD'] ?>
	        		<?php }else{ ?>
    					<?=number_format($allOverallSubjectRecordStat['SD'], DECIMAL_PLACES) ?>
	        		<?php } ?>
				</td>
			</tr>
		</tfoot>
	</table>
</div>


<!-- ################ Subject Report END ################ -->
<?php endif; ?>




<script>
(function($) {
	$('#chart_tables_1').floatHeader();
})(jQuery);
</script>




<?php
######################## Display Result END ########################

intranet_closedb();












