<?php
//Using:
/**
 * Change Log
 * 2019-10-04 Philips
 * Added Overall Score Diff. (Academic Year)
 * 
 * 2019-04-04 Anna [#U157874] 
 * add subject status when select subject , only show in used subject
 * 
 * 2018-11-07 Anna [#151868]
 * Added check order by yearid when get SD and Mean
 * 
 * 2018-04-19 Omas
 *  -   Revised loop for fixing wrong overall result problem #X138145 
 * 2018-04-16 Omas 
 *  -   Revised sql for #R137794
 * 2017-12-12 (Pun)
 *  -   Add cust T1A3 hardcode full mark
 * 2017-07-13 Villa
 *  -	Improve standard score to decimal 2
 *  -	Hide standard score/ standard diff if score = -1
 * 2017-05-11 Villa
 * 	-	Filter DSE SubjectID -999
 * 2017-02-28 Villa
 * 	-	#J113672  Fix Calculation Z-Score Problem ClassSDMean -> YearSDMean
 * 2017-02-21 Villa
 *  -	ReWrite Subject ID Mapping Method as the SubjectID of MOCK EXAM might be CMPID
 *  -	ReCalculate All ColSpan
 *  -	Modified Overall Result to fit of the addition case
 * 2017-02-20 Villa
 *  -	merge PreS1 to the table
 *  -	To Support Subject CMP, Modified the key indox of $Result, $CMP_Parent_Mapping, $subjMap_arr, $PassArr, $SDMean_Arr
 *  - 	fix Sem sorting problem #SEMSORT ===> PreS1->PreS1
 *  				  MOCK_EXAM -> 998
 *  				  DSE -> 999
 *  				  Whole Year -> 997
 *  -	Add Cust Sorting function Compare sort Result by SubjectCmp
 * 2016-12-15 Villa
 *  -	Change the data setting machanism
 *  -	Change the printing machanism
 * 2016-12-14 Villa
 *  -	sem sort
 * 2016-11-16 Villa
 * 	-	New File
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/json.php");


intranet_auth();
intranet_opendb();

$fcm = new form_class_manage();
$lpf = new libportfolio();
$json = new JSON_obj();
$sbj = new subject();

########## Access Right START ##########
########## Access Right END ##########
#### Function ####
//sorting Subject and Subject CMP
function Compare($a ,$b){
	$temp_a = explode('_',$a);
	$temp_b = explode('_',$b);
	if($temp_a[0] > $temp_b[0]){
		return 1;
	}elseif($temp_a[0] == $temp_b[0]){
		if(empty($temp_a[1])){ //Main Subject
			return -1;
		}else{
			if($temp_a[1] >$temp_b[1]){
				return 1;
			}else{
				return -1;
			}
		}
	}else{
		$abc = $temp_a[0]."<".$temp_b[0];
		return -1;
	}
	
}
function assessmentTermSort($a,$b){
	// -1 => first, 1=> later
	if($a == '0'){
		return 1;
	}elseif($b == '0'){
		return -1;
	}
	elseif($a > $b){
		return 1;
	}else{
		return -1;
	}
}
#### Function ####

$li = new libdb();
$lpf = new libpf_sturec();

$lpf->createTempAssessmentSubjectTable($li, true);
$accessRight = $lpf->getAssessmentStatReportAccessRight();

######## Get Data START ########
#### Get YearID for class teacher START ####
if($yearID == ''){
	$objYearClass = new year_class($YearClassID[0]);
	$yearID = $objYearClass->YearID;
}
#### Get YearID for class teacher END ####

// Assessment data retrieval
$subjMap_arr = array();
$_mainSubj = $Subjects;
$_compSubj ="";

if(!$accessRight['admin']){
    $subjectIDAry = array();
    if($accessRight['classTeacher']){
        foreach($accessRight['classTeacher'] as $class){
            $cYearID = $class['YearID'];
            $subjects = $sbj->Get_Subject_By_Form($cYearID);
            $sIDAry = Get_Array_By_key($subjects, 'RecordID');
            $subjectIDAry = array_merge($subjectIDAry, $sIDAry);
        }
    }
    if($accessRight['subjectGroup']){
        foreach($accessRight['subjectGroup'] as $subjectGroup){
            $sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
            $subjectIDAry[] = $sGroup->SubjectID;
        }
    }
    if($accessRight['subjectPanel']){
        foreach($accessRight['subjectPanel'] as $subjectPanel){
            $subjectIDAry[] = $subjectPanel;
        }
    }
    $subjectIDAry = array_unique($subjectIDAry);
    $subjectIDAry = implode("','", $subjectIDAry);
    $subjectIDCond = "AND SubjectID IN ('$subjectIDAry')";
}
if($plugin['SDAS_module']['advanceMode']){
	#PreS1 Record Start
	$sql = "
	SELECT
		DISTINCT
			AcademicYearID, 'PreS1' as Year, 'PreS1' as YearTermID, 'PreS1' as YearClassID, 'PreS1' as Semester, 'PreS1' as TermAssessment, SubjectID, NULL as SubjectComponentID, Score, Grade, NULL as OrderMeritForm, NULL as OrderMeritFormTotal
	FROM
		EXAM_STUDENT_SCORE
	Where
		StudentID = '$StudentID'
	AND
		ExamID = '1'
    $subjectIDCond
	";
	$Pre_s1_result = $li->returnResultSet($sql);
	
	#PreS1 Record END
	#Mock Record Start
	$sql = "
	SELECT
		DISTINCT
			AcademicYearID, 'MOCK_EXAM' as Year, 'MOCK_EXAM' as YearTermID, 'MOCK_EXAM' as YearClassID, 'MOCK_EXAM' as Semester, 'MOCK_EXAM' as TermAssessment, SubjectID, NULL as SubjectComponentID, Score, Grade, NULL as OrderMeritForm, NULL as OrderMeritFormTotal
	FROM
		EXAM_STUDENT_SCORE
	Where
		StudentID = '$StudentID'
	AND
		ExamID = '3'
    $subjectIDCond
	";
	$Mock_result = $li->returnResultSet($sql);
	
	#Mock Record END
	#DSE Record Start
	$sql = "
	SELECT
		DISTINCT
			AcademicYearID, 'DSE' as Year, 'DSE' as YearTermID, 'DSE' as YearClassID, 'DSE' as Semester, 'DSE' as TermAssessment, IF(ParentSubjectID=0, SubjectID, ParentSubjectID) as SubjectID, IF(ParentSubjectID=0, NULL, SubjectID) as SubjectComponentID, Grade as Score, Grade, NULL as OrderMeritForm, NULL as OrderMeritFormTotal
	FROM
		EXAM_DSE_STUDENT_SCORE
	WHERE
		StudentID = '$StudentID'
	AND
		SubjectID !='-999'
    $subjectIDCond
	AND
		isArchive = '0'
	";
	$DSE_result =  $li->returnResultSet($sql);
	
	#DSE Record END
}
//Assessment Exam Record

$sql = "
		SELECT
			AcademicYearID, Year, YearTermID, YearClassID, Semester, TermAssessment, SubjectID, SubjectComponentID, Score, Grade, OrderMeritForm, OrderMeritFormTotal, 
			CONCAT(SubjectID, IF(SubjectComponentID is not null,CONCAT('-',SubjectComponentID),'')) as realSubjectID,
			ClassName
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
		Where
			UserID = '$StudentID'
            $subjectIDCond
		Order by
			SubjectID, SubjectComponentID
		";
$perfromance = $li->returnResultSet($sql);

//$perfAssoc = BuildMultiKeyAssoc($perfromance, array('AcademicYearID', 'YearTermID', 'TermAssessment','realSubjectID'), array(), 0);

#Merge Record Start
if($plugin['SDAS_module']['advanceMode']){
	if(!empty($Pre_s1_result)){
		$perfromance = array_merge($perfromance, $Pre_s1_result);
	}
	if(!empty($Mock_result)){
		$perfromance = array_merge($perfromance, $Mock_result);
	}
	if(!empty($DSE_result)){
		$perfromance = array_merge($perfromance, $DSE_result);
	}
	#Merge Record END
}

//Main Subject Arr
$subjectArr = Get_Array_By_Key($perfromance, 'SubjectID');
//CMP Arr
$subjectArr_CMP = Get_Array_By_Key($perfromance, 'SubjectComponentID');

//ALL SubjectArr
$subjectArr = array_merge($subjectArr, $subjectArr_CMP);
$subjectArr = array_unique($subjectArr);
$subjectArr_sql = implode("','",$subjectArr );
$assessmentArr = Get_Array_By_Key($perfromance, 'TermAssessment');
$haveAssessment = false;
foreach($assessmentArr as $_assessmentArr){
	if($_assessmentArr!=''){
		$haveAssessment = true;
	}
}
$ay_idArr = Get_Array_By_Key($perfromance, 'AcademicYearID');
$ay_idArr = array_unique ($ay_idArr);
$ay_idArr_sql = implode("','",$ay_idArr);

//STUDENT INFO
$sql = "Select
			y.YearID, yc.AcademicYearID, yc.ClassTitleB5, yc.ClassTitleEN, ay.YearNameB5, ay.YearNameEN
		From
			YEAR y
		inner join
			YEAR_CLASS  yc on y.YearID = yc.YearID
		inner join
			YEAR_CLASS_USER ycu on yc.YearClassID=ycu.YearClassID
		inner join
			ACADEMIC_YEAR_TERM ayt ON yc.AcademicYearID = ayt.AcademicYearID
		inner join
			ACADEMIC_YEAR ay on yc.AcademicYearID = ay.AcademicYearID
		WHERE
			ycu.UserID=".$StudentID."
		AND
			yc.AcademicYearID IN ('{$ay_idArr_sql}')
		GROUP BY
			yc.AcademicYearID
		ORDER BY
			ayt.TermStart
		";
$rs = $li->returnResultSet($sql);

foreach ($rs as $_rs){
	$header[$_rs['AcademicYearID']] ['AcademicYearName']= $_rs['AcademicYearName']."</br>".$_rs['ClassName'];
	$header[$_rs['AcademicYearID']] ['AcademicYearID'] =  $_rs['AcademicYearID'];
}
$AcademicYearMappingArr = BuildMultiKeyAssoc($rs, 'AcademicYearID');
$AcademicYearIDArr = Get_Array_By_Key($rs, 'AcademicYearID');
$AcademicYearIDArr_sql = implode("','",$AcademicYearIDArr);
$YearIDArr =  Get_Array_By_Key($rs, 'YearID');
$YearIDArr_sql = implode("','",$YearIDArr);
$YearClassIDArr = Get_Array_By_Key($rs, 'ClassID');
$YearClassIDArr_sql = implode("','", $YearClassIDArr);

//term sort
$sql = "SELECT 
			YearTermID, AcademicYearID, YearTermNameEN, YearTermNameB5
  		FROM
  			ACADEMIC_YEAR_TERM
  		ORDER BY
  			TermStart";
$temp = $li->returnResultSet($sql);

$YearTermMap = BuildMultiKeyAssoc($temp, 'YearTermID');
foreach ($temp as $_temp){
	$SemMatch[$_temp['AcademicYearID']] [$_temp['YearTermID']] = count($SemMatch[$_temp['AcademicYearID']])+1;
}

### SubjectMapping Arr Start
//SUBJECT ARR => mapping subject name
$Subject_name = Get_Lang_Selection('CH_SNAME', 'EN_SNAME');
// 2018-04-16 Omas Revised IsMainSubject for #R137794
$sql = "
        SELECT
            table1.CH_SNAME, 
            table1.EN_SNAME, 
            table1.CMP_CODEID, 
            IF(table1.CMP_CODEID = '' or table1.CMP_CODEID is null, 1,0) as IsMainSubject, 
            table1.RecordID, 
            table2.RecordID as MainSubjectID, 
            table2.CH_SNAME as MainSubjectNameCH, 
            table2.EN_SNAME as MainSubjectNameEN
        FROM
            {$intranet_db}.ASSESSMENT_SUBJECT table1
        INNER JOIN
            {$intranet_db}.ASSESSMENT_SUBJECT table2 ON (table1.CODEID = table2.CODEID 
                                                        AND (table2.CMP_CODEID IS NULL OR table2.CMP_CODEID =''))
        WHERE
            table1.RecordID IN ('{$subjectArr_sql}')  AND table1.RecordStatus = table2.RecordStatus= '1'
        ORDER BY
            table2.DisplayOrder, IsMainSubject desc, table1.DisplayOrder
";
$temp = $li->returnResultSet($sql);
foreach($temp as $_temp){
	if( !$_temp['CMP_CODEID']){
		$Subject_CMM_ID = $_temp['MainSubjectID'];
		$Subject_name = Get_Lang_Selection($_temp['MainSubjectNameCH'], $_temp['MainSubjectNameEN']);
	}else{
		$Subject_CMM_ID = $_temp['MainSubjectID'].'_'.$_temp['RecordID'];
		//  		$Subject_name =  '&nbsp;'.'&nbsp;'.Get_Lang_Selection($_temp['MainSubjectNameCH'], $_temp['MainSubjectNameEN']).'_'. Get_Lang_Selection($_temp['CH_SNAME'], $_temp['EN_SNAME']);
		$Subject_name =  '&nbsp;'.'&nbsp;'.'-'.Get_Lang_Selection($_temp['CH_SNAME'], $_temp['EN_SNAME']);
	}
	$CMP_Parent_Mapping[$_temp['RecordID']] = $_temp['MainSubjectID'];
	$subjMap_arr[$Subject_CMM_ID]['SubjectName'] = $Subject_name;
}
### SubjectMapping Arr EnD

#Pass Mark Start
$sql ="
	Select
		asf.SubjectID, asf.YearID, asf.AcademicYearID, asf.PassMarkInt, yc.YearClassID
	FROM
		{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK asf
	RIGHT JOIN
		{$intranet_db}.YEAR_CLASS yc on asf.YearID = yc.YearID and asf.AcademicYearID = yc.AcademicYearID
	WHERE
		asf.AcademicYearID IN ('{$AcademicYearIDArr_sql}')
	AND
		asf.YearID IN ('{$YearIDArr_sql}')
	AND
		asf.SubjectID IN ('{$subjectArr_sql}')
	";
$temp = $li->returnResultSet($sql);

foreach($temp as $_temp){
	if($_temp['SubjectID']==$CMP_Parent_Mapping[$_temp['SubjectID']]){
		$Subject_CMM_ID = $_temp['SubjectID'];
	}else{
		$Subject_CMM_ID = $CMP_Parent_Mapping[$_temp['SubjectID']].'_'.$_temp['SubjectID'];
	}
	if($_temp['PassMarkInt']>0){
		$PassArr[$Subject_CMM_ID][$_temp['AcademicYearID']] [$_temp['YearClassID']] ['PassMark'] = $_temp['PassMarkInt'];
	}else{
		$PassArr[$Subject_CMM_ID][$_temp['AcademicYearID']] [$_temp['YearClassID']] ['PassMark'] = '50';
	}
}
#Pass Mark End

#SD Mean Start
$sql ="
		SELECT
			SD, MEAN, AcademicYearID, YearTermID, TermAssessment, SubjectID, SubjectComponentID,ClassLevelID
		FROM
			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
		WHERE
		/*YearClassID IN ('{$YearClassIDArr_sql}')*/
			YearClassID = 0
		AND
			ClassLevelID IN ('{$YearIDArr_sql}')
		AND
			AcademicYearID IN ('{$AcademicYearIDArr_sql}')
		AND
			SubjectID IN ('{$subjectArr_sql}')
		";
$temp = $li->returnResultSet($sql);


for($i=0;$i<count($YearIDArr);$i++){
    foreach($temp as $_temp){
        
        if(!$_temp['SubjectComponentID']){
            $Subject_CMM_ID = $_temp['SubjectID'];
        }else{
            $Subject_CMM_ID = $_temp['SubjectID'].'_'.$_temp['SubjectComponentID'];
        }
        
        if($YearIDArr[$i] == $_temp['ClassLevelID']){
            $SDMeanArr[$Subject_CMM_ID][$_temp['AcademicYearID']] [$_temp['YearTermID']] [$_temp['TermAssessment']] ['SD'] = $_temp['SD'];
            $SDMeanArr[$Subject_CMM_ID][$_temp['AcademicYearID']] [$_temp['YearTermID']] [$_temp['TermAssessment']] ['MEAN'] = $_temp['MEAN'];
        }
    }
    
    // 	$SDMeanArr[$Subject_CMM_ID][$_temp['AcademicYearID']] [$_temp['YearTermID']] [$_temp['TermAssessment']] ['SD'] = $_temp['SD'];
    // 	$SDMeanArr[$Subject_CMM_ID][$_temp['AcademicYearID']] [$_temp['YearTermID']] [$_temp['TermAssessment']] ['MEAN'] = $_temp['MEAN'];
}

#SD Mean END

#Overall Result Start
$sql="
		SELECT
			AcademicYearID, YearTermID, TermAssessment, Score, Grade, OrderMeritForm
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
		WHERE
			UserID = $StudentID
		AND
			AcademicYearID IN ('{$AcademicYearIDArr_sql}')
		AND
			Score >= 0
		";
$overall_result = $li->returnResultSet($sql);

foreach ($overall_result as $_overall_result){
	if($_overall_result['YearTermID']==''){
		$_overall_result['YearTermID'] = '0';
	}
	if($_overall_result['TermAssessment'] == ''){
		$TermAssessment = '0';
	}else{
		$TermAssessment = $_overall_result['TermAssessment'];
	}
	$overall[$_overall_result['AcademicYearID']] [$_overall_result['YearTermID']] [$TermAssessment] ['Score'] = $_overall_result['Score'];
	$overall[$_overall_result['AcademicYearID']] [$_overall_result['YearTermID']] [$TermAssessment] ['Grade'] = $_overall_result['Grade'];
	$overall[$_overall_result['AcademicYearID']] [$_overall_result['YearTermID']] [$TermAssessment] ['OrderMeritForm'] = $_overall_result['OrderMeritForm'];
}
#Overall Result END

#Conduct Start
$comment = Get_Lang_Selection('CommentChi', 'CommentEng');
$sql ="
		SELECT
			AcademicYearID, YearTermID, ConductGradeChar, $comment as Comment
		FROM
			{$eclass_db}.CONDUCT_STUDENT
		WHERE
			UserID = $StudentID
		AND
			AcademicYearID IN ('{$AcademicYearIDArr_sql}')
		";
$temp = $li->returnResultSet($sql);

foreach($temp as $_temp){
	$conduct[$_temp['AcademicYearID']][$_temp['YearTermID']]['ConductGradeChar'] = $_temp['ConductGradeChar'];
	$conduct[$_temp['AcademicYearID']][$_temp['YearTermID']]['Comment'] = $_temp['Comment'];
}
#Conduct END
//performance
foreach($perfromance as $_perfromance){

	if($_perfromance['YearTermID']==''){
		$sem_sort = '997';
		$_perfromance['YearTermID'] = '0';
		$_perfromance['Semester'] =$Lang['General']['WholeYear'];
	}else{
		$sem_sort = $SemMatch[$_perfromance['AcademicYearID']][$_perfromance['YearTermID']];
	}
	
	if($plugin['SDAS_module']['advanceMode']){
		if($_perfromance['YearTermID']=='PreS1'){
			$sem_sort = 'PreS1';
		}
		if($_perfromance['YearTermID']=='MOCK_EXAM'){
			$sem_sort = '998';
		}
		if($_perfromance['YearTermID']=='DSE'){
			$sem_sort = '999';
		}
	}
	
	if($_perfromance['TermAssessment'] == ''){
		$TermAssessment = '0';
	}else{
		$TermAssessment = $_perfromance['TermAssessment'];
	}
	$header[$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] ['YearTermID'] = $_perfromance['YearTermID'];
	$header[$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] ['Main'] [$_perfromance['Semester']] = $_perfromance['Semester'];
	$header[$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] ['Assessment'] [$TermAssessment] = $TermAssessment;
	
	//set Subject_CMP ID => SubjectID + SubjectCmp ID
	//For case MOCK EXAM
	if($_perfromance['SubjectID']== $CMP_Parent_Mapping[$_perfromance['SubjectID']]){ //Main Subject
		$Cmp = '';
		$Subject_Cmp = $_perfromance['SubjectID'];
	}else{ //CMP Subject
		$Subject_Cmp = $CMP_Parent_Mapping[$_perfromance['SubjectID']].'_'.$_perfromance['SubjectID'];
	}
	//Normal Method(if set Subject CMP) -> Overwrite the Subject
	if($_perfromance['SubjectComponentID']){
		$Cmp = '_'.$_perfromance['SubjectComponentID'];
		$Subject_Cmp = $_perfromance['SubjectID'].$Cmp;
	}
	$result[$Subject_Cmp] ['RecordID'] = $Subject_Cmp;
	$result[$Subject_Cmp] ['Title'] = $subjMap_arr[$Subject_Cmp]['SubjectName'];
	
	$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['YearTermID'] = $_perfromance['YearTermID'];
	$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['Semester'] = $_perfromance['Semester'];
	$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['TermAssessment'] = $TermAssessment;
	$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['Grade'] = $_perfromance['Grade'];
	$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['Score'] = $_perfromance['Score'];
	$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['Order'] = $_perfromance['OrderMeritForm'];
	$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['OrderTotal'] = $_perfromance['OrderMeritFormTotal'];
	
	$SD = $SDMeanArr[$Subject_Cmp][$_perfromance['AcademicYearID']] [$_perfromance['YearTermID']] [$TermAssessment] ['SD'];
	$Mean =  $SDMeanArr[$Subject_Cmp][$_perfromance['AcademicYearID']] [$_perfromance['YearTermID']] [$TermAssessment] ['MEAN'];
	if($SD>0){
		$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['SD'] = round(($_perfromance['Score']-$Mean)/$SD,2);
	}
	
	$PassMark = $PassArr[$Subject_Cmp][$_perfromance['AcademicYearID']] [$_perfromance['YearClassID']] ['PassMark'];
    if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
    	if(
    		($TermAssessment == 'T1A3' && strpos($_perfromance['ClassName'], '6') !== false) ||
    		($TermAssessment == 'T2A3')
    	){
    		$PassMark = '50';
    	}
    }
    
	if($_perfromance['Score']>=$PassMark){
		$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['Pass']=true;
	}else{
		$result[$Subject_Cmp] [$_perfromance['AcademicYearID']] ['Term'] [$sem_sort] [$TermAssessment] ['Pass']=false;
	}
	$result[$Subject_Cmp] ['AcademicYearID'] =  $_perfromance['AcademicYearID'];
	
	if(!($_perfromance['YearTermID']=='PreS1'||$_perfromance['YearTermID']=='MOCK_EXAM'||$_perfromance['YearTermID']=='DSE')){
		$overall_data[$_perfromance['AcademicYearID']] [$sem_sort] [$TermAssessment]['Score'] = $overall[$_perfromance['AcademicYearID']] [$_perfromance['YearTermID']] [$TermAssessment] ['Score'];
		$overall_data[$_perfromance['AcademicYearID']] [$sem_sort] [$TermAssessment]['Grade'] = $overall[$_perfromance['AcademicYearID']] [$_perfromance['YearTermID']] [$TermAssessment] ['Grade'];
		$overall_data[$_perfromance['AcademicYearID']] [$sem_sort] [$TermAssessment]['OrderMeritForm'] = $overall[$_perfromance['AcademicYearID']] [$_perfromance['YearTermID']] [$TermAssessment] ['OrderMeritForm'];
		$overall_data[$_perfromance['AcademicYearID']] [$sem_sort] [$TermAssessment]['Conduct'] = $conduct[$_perfromance['AcademicYearID']][$_perfromance['YearTermID']]['ConductGradeChar'];
	}else{
		$overall_data[$_perfromance['AcademicYearID']] [$sem_sort] [$TermAssessment]['Score'] = Get_String_Display('');
		$overall_data[$_perfromance['AcademicYearID']] [$sem_sort] [$TermAssessment]['Grade'] = Get_String_Display('');
		$overall_data[$_perfromance['AcademicYearID']] [$sem_sort] [$TermAssessment]['OrderMeritForm'] = Get_String_Display('');
		$overall_data[$_perfromance['AcademicYearID']] [$sem_sort] [$TermAssessment]['Conduct'] = Get_String_Display('');
		
	}
	if($conduct[$_perfromance['AcademicYearID']][$_perfromance['YearTermID']]['Comment']!=''){
		$overall_comment[$_perfromance['AcademicYearID']] ['comment'] ['semester'][$_perfromance['YearTermID']] = $_perfromance['Semester'];
		$overall_comment[$_perfromance['AcademicYearID']] ['comment'] ['comment'][$_perfromance['Semester']] = $conduct[$_perfromance['AcademicYearID']][$_perfromance['YearTermID']]['Comment'] ;
	}
}
$numOfFilter = count($filterColumn);

foreach($rs as $value){
	if(empty($header[$value['AcademicYearID']] ['Term'])){
		$header[$value['AcademicYearID']] ['numberOfTerm'] = 1;
		$header[$value['AcademicYearID']] ['Term'] ['Empty'] ['Main'] ['Empty'] = $Lang['General']['EmptySymbol'];
		$header[$value['AcademicYearID']] ['Term'] ['Empty'] ['Assessment'] ['Empty'] = $Lang['General']['EmptySymbol'];
	}else{
		foreach((array)$header[$value['AcademicYearID']] ['Term'] as $_term){
			$numberOfAssessmentTerms = $numberOfAssessmentTerms+count($_term['Assessment']);
			$header[$value['AcademicYearID']] ['numberOfTerm'] = $numberOfAssessmentTerms;
		}
	}
	$numberOfAssessmentTerms = 0;
}

//Sorting the Header
$header_temp = $header;
unset($header);
foreach ((array)$header_temp as $_academicYear => $_header_temp){
	foreach ((array)$_header_temp['Term'] as $sortsem => $__header_temp){
		$header[$_academicYear]['Term'][$sortsem][$__header_temp['YearTermID']] = $__header_temp['Assessment'];
		uksort($header[$_academicYear]['Term'][$sortsem][$__header_temp['YearTermID']],"assessmentTermSort");
	}
	ksort($header[$_academicYear]['Term']);
	$header[$_academicYear]['numberOfTerm'] = $_header_temp['numberOfTerm'];
}
######## Get Data END ########
######## Table Data START ########
$x = "";
$x .="<div class='chart_tables'>";
$x .="<table class='common_table_list_v30 view_table_list_v30' id='resultTable'>";
if(isset($result)){
	$x .="<thead>";
	$x .="<tr>";
	$x .="<th>"; // print empty block in left hand corner
	$x .="</th>";
	foreach($header as $academicYearID =>$_header){ //academic year header
		$YearColSpan = 0;
		foreach((array)$_header['Term'] as $__header){ //Term In Year
			foreach($__header as $___header){//Term In Year
				foreach($___header as $____header){ //Assessment Term in Term
					if(!($____header=='PreS1'||$____header=='MOCK_EXAM'||$____header=='DSE')){
						$thisColSpan = $numOfFilter;
					}else{
						$thisColSpan = 1;
					}
					$YearColSpan += $thisColSpan;
				}
			}
		}
		$x .="	<th colspan=".$YearColSpan." style='white-space: nowrap;'>";
		$x .= Get_Lang_Selection($AcademicYearMappingArr[$academicYearID]['YearNameB5'], $AcademicYearMappingArr[$academicYearID]['YearNameEN']);
		$x .= '<br>';
		$x .= Get_Lang_Selection($AcademicYearMappingArr[$academicYearID]['ClassTitleB5'],$AcademicYearMappingArr[$academicYearID]['ClassTitleEN']);
		$x .= "</th>";
	}
	$x .= "</tr>";
	
	// term title
	$x .= "<tr>";
	$x .= "<th style='width: 20%; white-space: nowrap;'>".$Lang['General']['Term']."</th>";
	foreach($header as $academicYearID =>$_header){ //academic year header
		foreach((array)$_header['Term'] as $_header){ //Term In Year
			foreach ($_header as $YearTermID =>$__header){//Term In Year
				$numberOfsupTerm = count($__header);
				if(!($YearTermID==='PreS1'||$YearTermID==='MOCK_EXAM'||$YearTermID==='DSE')){ //normal case
					$x .="<th colspan=".$numberOfsupTerm*$numOfFilter.">";
					if($YearTermID){
						$x .= Get_Lang_Selection($YearTermMap[$YearTermID]['YearTermNameB5'], $YearTermMap[$YearTermID]['YearTermNameEN']);
					}else{
						$x .= $ec_iPortfolio['overall_result'];
					}
				}else{ //PreS1/ MockExam/ DSE
					$x .="	<th colspan='1'>";
					$x .= $Lang['SDAS']['Student_CrossYear_Result'][$YearTermID];
				}
				$x .= "</th>";
			}
		}
	}
	$x .="</tr>";
	if($haveAssessment){
		$x .= "<tr>"; //assessment term
		$x .= "<th style='width: 20%; white-space: nowrap;'>".$i_frontpage_assessment."</th>";
		foreach($header as $academicYearID =>$_header){ //academic year header
			foreach((array)$_header['Term'] as $_header){ //Term In Year(sort index)
				foreach ($_header as $YearTermID =>$__header){//Term In Year
					foreach ($__header as $_AssessmentTermHeader){//Assessment Term In Term
						if(!($_AssessmentTermHeader==='PreS1'||$_AssessmentTermHeader==='MOCK_EXAM'||$_AssessmentTermHeader==='DSE')){
							$x .="	<th colspan=".$numOfFilter.">";
							if($_AssessmentTermHeader=='0'){ //Whole Year
								$_AssessmentTermHeader=  $ec_iPortfolio['overall_result'];
							}
						}else{
							$x .="	<th colspan='1'>";
							$_AssessmentTermHeader= $Lang['SDAS']['Student_CrossYear_Result'][$_AssessmentTermHeader];
							
						}
						//Special Case String replace
						$x .= $_AssessmentTermHeader;
						$x .= "</th>";
					}
				}
			}
		}
		$x .="</tr>";
	}
	
	//filter title
	$x .="<tr>";
	$x .= "<th style='width: 20%; white-space: nowrap;'>".$ec_iPortfolio['display']."</th>";
	foreach($header as $academicYearID =>$_header){ //academic year header
		foreach((array)$_header['Term'] as $__header){ //Term In Year(sem sort)
			foreach ($__header as $YearTermID =>$AssessmentTermHeader){//Term In Year
				foreach ($AssessmentTermHeader as $_AssessmentTermHeader){//AssessmentTerm In Term
					if(!($_AssessmentTermHeader=='PreS1'||$_AssessmentTermHeader=='MOCK_EXAM'||$_AssessmentTermHeader=='DSE')){ //normal case
						foreach($filterColumn as $filter){
							switch($filter){
								case "filterMark":
									$filter = $Lang['iPortfolio']["Score"];
									break;
								case "filterMarkDiff":
									$filter = $Lang['iPortfolio']["MarkDifference"];
									break;
								case "filterStandardScore":
									$filter = $Lang['iPortfolio']["StandardScore"];
									break;
								case "filterStandardScoreDiff":
									$filter = $Lang['iPortfolio']["StandardScoreDifference"];
									break;
								case "filterFormPosition":
									$filter = $Lang['iPortfolio']["FormPosition"];
									break;
								case "filterFormPositionDiff":
									$filter = $Lang['iPortfolio']["FormPositionDifference"];
									break;
								case "filterGrade":
									$filter = $Lang['General']['Grade'];
									break;
							}
							$x .="<th style='width:8%'>";
							$x .=$filter;
							$x .="</th>";
						}
					}else{
						$filter = $Lang['iPortfolio']["Score"];
						$x .="<th style='width:8%'>";
						$x .=$filter;
						$x .="</th>";
					}
				}
			}
		}
	}
	$x .="</tr>";
	##### Table Body START #####
	$preverious_ayid = '';
	$x .= "<tbody>";
	foreach((array)$subjMap_arr as $subjectIndexID=>$subject){
		$x .= "<tr>";
		$x .= "<td style='white-space: nowrap;'>";
		$x .= $subject['SubjectName'];
		$x .="</td>";
		foreach($header as $academicYearID =>$_header){ //academic year header
			foreach((array)$_header['Term'] as $_header_semsort=> $__header){ //Term In Year(sem sort)
				foreach ((array)$__header as $YearTermID =>$AssessmentTermHeader){//Term In Year
					foreach ((array)$AssessmentTermHeader as $_AssessmentTermHeader){
						if(!($_AssessmentTermHeader=='PreS1'||$_AssessmentTermHeader=='MOCK_EXAM'||$_AssessmentTermHeader=='DSE')){
							foreach($filterColumn as $filter){
								switch($filter){
									case "filterMark":
										$display = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Score"];
										if($display=='-1'){
											$display = '';
										}
										if( $display !=''){
											$color = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Pass"]? "":"style='color:red'";
										}else{
											$color = '';
										}
										break;
										// 					case "filterMarkDiff":
										// 						$display = $_data;
										// 						break;
									case "filterStandardScore":
										$score = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Score"];
										if($score!='' && $score!='-1'){
											$display = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["SD"];
										}else{
											$display='';
										}
										$color ='';
										break;
									case "filterStandardScoreDiff":
										$score = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Score"];
										if($score!='' && $score!='-1'){
											if(!empty($result[$subjectIndexID][$preverious_ayid]['Term'][$_header_semsort][$_AssessmentTermHeader]['SD'])&&!empty($result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["SD"])){
												$display = round($result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["SD"]-$result[$subjectIndexID][$preverious_ayid]['Term'][$_header_semsort][$_AssessmentTermHeader]['SD'],2);
											}else{
												$display='';
											}
										}else{
											$display='';
										}
										$color ='';
										break;
									case "filterFormPosition":
										if($result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Order"]>0){
											$display = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Order"]."/".$result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["OrderTotal"];
											$display = trim($display, ' /');
										}else{
											$display ='';
										}
										$color ='';
										break;
									case "filterFormPositionDiff":
										if($result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Order"]>0 && $result[$subjectIndexID][$preverious_ayid]['Term'][$_header_semsort][$_AssessmentTermHeader]['Order']>0){
											$display = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Order"]-$result[$subjectIndexID][$preverious_ayid]['Term'][$_header_semsort][$_AssessmentTermHeader]['Order'];
										}else{
											$display='';
										}
										$color ='';
										break;
									case "filterGrade":
										$display = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Grade"];
										$color ='';
										break;
										
								}
								
								$x .="<td $color>";
								$x .= Get_String_Display($display);
								$x .="</td>";
							}
						}else{
							$display = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Score"];
							if($display=='-1'){
								$display = '';
							}
							if( $display !=''){
								$color = $result[$subjectIndexID][$academicYearID]['Term'][$_header_semsort][$_AssessmentTermHeader]["Pass"]? "":"style='color:red'";
							}else{
								$color = '';
							}
							$x .="<td $color>";
							$x .= Get_String_Display($display);
							$x .="</td>";
						}
					}
				}
			}
			$preverious_ayid = $academicYearID;
		}
		$preverious_ayid = '';
		$x .="</tr>";
	}
	// Omas #X138145
	// OverAll result start
	$overallResultTypeArr = array(
        'Score'          => $ec_iPortfolio['overall_score'],
	    'Grade'          => $Lang['General']['Grade'],
	    'OrderMeritForm' => $ec_iPortfolio['rank'],
	    'Conduct'        => $eDiscipline['DisciplineName']
	);
	foreach($overallResultTypeArr as $_dataType => $_dataTypeLang){
	    $x .="<tr>";
	    $x .="<td>";
	    $x .=$_dataTypeLang;
	    $x .="</td>";
	    if($filterOverallScoreDiff && $_dataType == 'Score'){
	    	// Overall Score Diff. title
	    	$fosd.="<tr>";
	    	$fosd.="<td>";
	    	$fosd.=$Lang['SDAS']['Student_CrossYear_Result']['overall_score_diff'];
	    	$fosd.="</td>";
	    }
	    foreach($header as $academicYearID =>$_header){ //academic year header
	        if(empty($overall_data[$value['AcademicYearID']])){
	            $x .="<td colspan=$numOfFilter>";
	            $x .=$Lang['General']['EmptySymbol'];
	            $x .="</td>";
	            if($filterOverallScoreDiff && $_dataType == 'Score'){
	            	$fosd.="<td colspan=$numOfFilter>";
	            	$fosd.=$Lang['General']['EmptySymbol'];
	            	$fosd.="</td>";
	            }
	        }else{
	        	if($filterOverallScoreDiff && $_dataType == 'Score'){
	        		// initialize first overall score and last overall score
	        		$first = array();
	        		$last = array();
	        	}
	            foreach((array)$_header['Term'] as $__semsort => $__header){ //Term In Year(sem sort)
	                foreach ($__header as $YearTermID =>$AssessmentTermHeader){//Term In Year
	                    foreach ($AssessmentTermHeader as $_AssessmentTermHeader){//AssessmentTerm In Term
	                        $__overall_data = $overall_data[$academicYearID][$__semsort][$_AssessmentTermHeader];
// 	                        if(!($___header=='PreS1'||$___header=='998'||$___header=='999')){
	                        if(!($__semsort=='PreS1'||$__semsort=='998'||$__semsort=='999')){
	                            $__colspan = $numOfFilter;
	                        }else{
	                            $__colspan = "'1'";
	                        }
	                        $x .="<td colspan=$__colspan>";
	                        $x .= Get_String_Display($__overall_data[$_dataType]);
	                        $x .="</td>";
	                        if($filterOverallScoreDiff && $_dataType == 'Score' && ($_AssessmentTermHeader != end($AssessmentTermHeader) )){
	                        	// Overall score of Assessment
	                        	$fosd.="<td colspan=$__colspan>";
	                        		$fosd .= Get_String_Display(""); // --
	                        	$fosd.="</td>";
	                        }
	                    }
	                }
	                if($filterOverallScoreDiff && $_dataType == 'Score' && ($__semsort!= '997') ){
	                	// Overall score of Term
	                	if(empty($first) && !($__semsort=='PreS1'||$__semsort=='998'||$__semsort=='999')) $first = array("semester" => $__semsort, "assessment" => $_AssessmentTermHeader);
	                	if(!($__semsort=='PreS1'||$__semsort=='998'||$__semsort=='999')) $last = array("semester" => $__semsort,"assessment" => $_AssessmentTermHeader);
	                	$fosd.="<td colspan=$__colspan>";
	                		$fosd .= Get_String_Display(""); // --
	                	$fosd.="</td>";
	                } else if($filterOverallScoreDiff && $_dataType == 'Score' && ($__semsort == '997') ){
	                	// Overall score of Academic Year
	                	$fdata = $overall_data[$academicYearID][$first["semester"]][$first["assessment"]][$_dataType];
	                	$ldata = $overall_data[$academicYearID][$last["semester"]][$last["assessment"]][$_dataType];
	                	$diff = $ldata - $fdata;
	                	$fosd.="<td colspan=$__colspan>";
	                		$fosd .= Get_String_Display(Number_format($diff, 2));
	                	$fosd.="</td>";
	                	// reset first and last overall score
	                	$first = array();
	                	$last = array();
	                }
	            }
	        }
	    }
	    $x .="</tr>";
	    if($filterOverallScoreDiff && $_dataType == 'Score'){
	    	// Insert Overall Score Diff. row
	    	$fosd .= "</tr>";
	    	$x .= $fosd;
	    }
	}
	
	if($filterTeacher){ //teacher comment
		$x .="<tr>";
		$x .="<td>";
		$x .=$ec_iPortfolio['teacher_comment'];
		$x .="</td>";
		foreach($header as $academicYearID =>$_header){ //academic year header
			$print = true;
			$YearColSpan = 0;
			if(empty($overall_comment[$academicYearID])){
				foreach((array)$_header['Term'] as $_header_semsort=> $__header){ //Term In Year(sem sort)
					foreach ((array)$__header as $YearTermID =>$AssessmentTermHeader){//Term In Year
						foreach ((array)$AssessmentTermHeader as $_AssessmentTermHeader){
							if(!($_AssessmentTermHeader=='PreS1'||$_AssessmentTermHeader=='MOCK_EXAM'||$_AssessmentTermHeader=='DSE')){
								$thisColSpan = $numOfFilter;
							}else{
								$thisColSpan = 1;
							}
							$YearColSpan += $thisColSpan;
						}
					}
				}
				$x .="<td colspan=".$YearColSpan.">";
				// 					$x .="<td colspan=".$header[$value['AcademicYearID']]['numberOfTerm']*$numOfFilter.">";
				$x .=$Lang['General']['EmptySymbol'];
				$x .="</td>";
			}else{
				foreach((array)$_header['Term'] as $_header_semsort=> $__header){ //Term In Year(sem sort)
					foreach ((array)$__header as $YearTermID =>$AssessmentTermHeader){//Term In Year
						foreach ((array)$AssessmentTermHeader as $_AssessmentTermHeader){
							if(!($_AssessmentTermHeader=='PreS1'||$_AssessmentTermHeader=='MOCK_EXAM'||$_AssessmentTermHeader=='DSE')){
								$thisColSpan = $numOfFilter;
							}else{
								$thisColSpan = 1;
							}
							$YearColSpan += $thisColSpan;
						}
					}
				}
				$x .="	<td colspan=".$YearColSpan.">";
				// 					$x .="<td colspan=".$header[$value['AcademicYearID']]['numberOfTerm']*$numOfFilter.">";
				$x .= "<table width=100%>";
				foreach((array)$overall_comment[$academicYearID] ['comment'] ['semester']as $_overall_comment){
					
					if(!empty($overall_comment[$academicYearID] ['comment']['comment'][$_overall_comment])){
						$x .= "<tr>";
						$x .= "<td>";
						if($_overall_comment=='0'){
							$x .= $Lang['General']['WholeYear'];
						}else{
							$x .= $_overall_comment;
						}
						$x .="</td>";
						$x .= "<td>";
						$x .= Get_String_Display($overall_comment[$academicYearID] ['comment']['comment'][$_overall_comment]);
						$x .="</td>";
						$x .= "</tr>";
					}else{
						if($print){
							$x .= $Lang['General']['EmptySymbol'];
						}
						$print = false;
					}
				}
				$x .= "</table>";
				$x .="</td>";
			}
		}
		$x .="</tr>";
	}
	$x .= "</tbody>";
}else{ //no data
	$x .= "<tr>";
		$x .= "<td>";
			$x .= "<div align='center'>";
				$x .= $Lang['General']['NoRecordFound'];
			$x .= "</div>";
		$x .= "</td>";
	$x .= "</tr>";
}
##### Table Body START #####
$x .= "</table>";
$x .= "</br>";
$x .= "<span class='tabletextremark'>";
$x .= $ec_iPortfolio['scoreHighlightRemarks'];
$x .= "</span>";
$x .= "</div>";

$x .= '
<style>
	table.view_table_list_v30 th{white-space: nowrap;},
	table.view_table_list_v30 td{white-space: nowrap;}
</style>
';

######## Table Data END ########


?>

<!-- -------- Generate table START -------- -->
<?php 
		echo $x;
?>

<script>$(function() {$('#resultTable').floatHeader();});</script>
<!-- -------- Generate table END -------- -->



<?php
intranet_closedb();
?>