<?php
//using 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$obj = new libportfolio2007();

if (!$obj->IS_ADMIN_USER($UserID) && !$obj->IS_TEACHER() && $plugin['StudentDataAnalysisSystem_Style'] != "tungwah")
{
    header ("Location: /");
    intranet_closedb();
    exit();
}

$objSDAS = new libSDAS();

$action = $_POST['action'];
$groupID = $_POST['groupID'];
if($action == 'update'){
    $name_ch = $_POST['name_ch'];
    $name_en = $_POST['name_en'];
    $description = $_POST['description'];
    $academicStatistic = $_POST['academicStatistic'];
    
    if($groupID == ""){
        $groupID = $objSDAS->InsertAccessGroup($arr = array(
            "Name_ch" => $name_ch,
            "Name_en" => $name_en,
            "Description" => $description
        ));
        $result = $objSDAS->InsertAccessGroupRight($academicStatistic, $groupID);
        $ReturnMsgKey = $result ? 'AddSuccess' : 'AddFailed';
    } else {
        $objSDAS->UpdateAccessGroup($arr = array(
            "Name_ch" => $name_ch,
            "Name_en" => $name_en,
            "Description" => $description,
            "GroupID" => $groupID
        ));
        $result = $objSDAS->UpdateAccessGroupRight($academicStatistic, $groupID);
        $ReturnMsgKey = $result ? 'UpdateSuccess' : 'UpdateUnsuccess';
    }
    
} else if($action == 'delete'){
    $groupID = $_POST['GroupID'];
    $result = $objSDAS->DeleteAccessGroup($groupID);
    $ReturnMsgKey = sizeof($result) > 0 ? 'DeleteSuccess' : 'DeleteUnsuccess';
} else if($action == 'member'){
    $memberArr = $_POST['member'] ? $_POST['member'] : array();
    foreach($memberArr as $k => $v){
        $memberArr[$k] = str_replace('U', '', $v);
    }
    $result = $objSDAS->updateAccessGroupMember($memberArr, $groupID);
    $ReturnMsgKey = ($result['insert']&&$result['delete']) ? 'UpdateSuccess' : 'UpdateUnsuccess';
}
header('location: ?t=settings.assessmentStatReport.accessRightConfigAccessGroup&ReturnMsgKey=' . $ReturnMsgKey);

intranet_closedb();
?>