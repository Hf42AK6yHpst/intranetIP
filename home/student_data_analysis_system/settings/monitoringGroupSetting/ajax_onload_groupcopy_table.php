<?php
###Data Getting START
#From AcademicYear Group List
$sql = 'SELECT 
			GroupID, GroupNameEN, GroupNameCH
		FROM
			MONITORING_GROUP 
		WHERE
			AcademicYearID = '.$FromAcademicYearID.'
		AND
			IsDeleted = "0"
		ORDER BY 
			GroupID desc';
$objSDAS->returnResultSet($sql);
$rs_From =  $objSDAS->returnResultSet($sql);
// $GroupEN_From = Get_Array_By_Key($rs_From, 'GroupNameEN');
// $GroupCH_From= Get_Array_By_Key($rs_From, 'GroupNameCH');
#To AcademicYEar Group List
$sql = 'SELECT
			GroupNameEN, GroupNameCH
		FROM
			MONITORING_GROUP
		WHERE
			AcademicYearID = '.$ToAcademicYearID.'
		AND
			IsDeleted = "0"';
$rs_To = $objSDAS->returnResultSet($sql);
$GroupEN_To = Get_Array_By_Key($rs_To, 'GroupNameEN');
$GroupCH_To = Get_Array_By_Key($rs_To, 'GroupNameCH');
###Data Getting END

###Table Start
$TableHTML = '<table style="width: 100%;" class="common_table_list_v30">';
$TableHTML .= '<tr>';
	#GroupNameEN
	$TableHTML .= '<th style="width: 35%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameEN'].'</th>';
	#GroupNameCH
	$TableHTML .= '<th style="width: 35%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameCH'].'</th>';
	#Copy
	$TableHTML .= '<th style="text-align: center; width: 15%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['Copy'].'<br>'.'<input type="checkbox" class="CopyGroup_master" onclick="CheckAll(\'CopyGroup\');" checked>'.'</th>';
	#CopyMember
	$TableHTML .= '<th style="text-align: center; width: 15%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['CopyMember'].'<br>'.'<input type="checkbox" class="CopyMember_master" onclick="CheckAll(\'CopyMember\');" checked>'.'</th>';
$TableHTML .= '</tr>';
if($rs_From){
	foreach ((array)$rs_From as $_rs_From){
		$TableHTML .= '<tr>';
			#GroupNameEN
			$TableHTML .= '<td>'.$_rs_From['GroupNameEN'].'</td>';
			#GroupNameCH
			$TableHTML .= '<td>'.$_rs_From['GroupNameCH'].'</td>';
			#Copy
			if(in_array($_rs_From['GroupNameEN'],(array)$GroupEN_To)||in_array($_rs_From['GroupNameCH'],(array)$GroupCH_To)){
				$TableHTML .= '<td>'.'<span class="tabletextremark">'.$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['HaveSameGroup'].'</span>'.'</td>';
			}else{
				$TableHTML .= '<td style="text-align: center;">'.'<input type="checkbox" id="CopyGroup_GroupID_'.$_rs_From['GroupID'].'" name="CopyGroup_GroupID[]" class="CopyGroup" value="'.$_rs_From['GroupID'].'" onclick="UpdateMemberCheckBox('.$_rs_From['GroupID'].')" checked>'.'</td>';
			}
			#CopyMember
			if(in_array($_rs_From['GroupNameEN'],(array)$GroupEN_To)||in_array($_rs_From['GroupNameCH'],(array)$GroupCH_To)){
				$TableHTML .= '<td>'.'<span class="tabletextremark">'.$Lang['SDAS']['MonitoringGroupSetting']['HaveSameGroup'].'</span>'.'</td>';
			}else{
				$TableHTML .= '<td style="text-align: center;">'.'<input type="checkbox" id="CopyMember_GroupID_'.$_rs_From['GroupID'].'" name="CopyMember_GroupID[]" class="CopyMember" value="'.$_rs_From['GroupID'].'" checked>'.'</td>';
			}
		$TableHTML .= '</tr>';
	}
}else{
	$TableHTML .= '<tr>';
		$TableHTML .= '<td colspan="4" style="text-align: center;">'.$Lang['SDAS']['MonitoringGroupSetting']['NoResult'].'</td>';
	$TableHTML .= '</tr>';
}
$TableHTML .= '</table>';
###Table END
echo $TableHTML;
?>