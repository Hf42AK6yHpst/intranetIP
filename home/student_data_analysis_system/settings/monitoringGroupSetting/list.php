<?php
/*
 * Using:	Villa
 * 
 * Change Log:
 * Date:	2017-06-05 Villa
 * 			- Open the file
 */


######## Page Setting START ########
$CurrentPage = "settings.monitoringGroup";
$CurrentPageName = $Lang['SDAS']['MonitoringGroupSetting']['MonitoringGroupSetting'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

###AcademicYear Selection Start###
$sql = 'SELECT 
			ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
		FROM 
			ACADEMIC_YEAR ay
		INNER JOIN 
			ACADEMIC_YEAR_TERM ayt
				ON	ay.AcademicYearID = ayt.AcademicYearID
		GROUP By
			ay.AcademicYearID
		ORDER BY
			ayt.TermStart
				DESC';
$rs = $objSDAS->returnResultSet($sql);
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
$htmlAry['AcademicYear']= '<select id="AcademicYearID" name="AcademicYearID" onChange="RecordListTable()">';
// $htmlAry['AcademicYear'].= '<option value>'.$Lang['SDAS']['MonitoringGroupSetting']['AllYear'].'</option>';
foreach((array)$rs as $_rs){
	if($CurrentAcademicYearID==$_rs['AcademicYearID']){
		$selected = 'Selected';
	}else{
		$selected = '';
	}
	$htmlAry['AcademicYear'].= '<option value="'.$_rs['AcademicYearID'].'" '.$selected.'>'.Get_Lang_Selection($_rs['YearNameB5'], $_rs['YearNameEN']).'</option>';
}
$htmlAry['AcademicYear'].= '</select>';
###AcademicYear Selection END###

###Functional Bar START ###
$btnAry[] = array('new', 'javascript: add_New();');
$btnAry[] = array('copy', '?t=settings.monitoringGroupSetting.copy_to_other_year',$Lang['SDAS']['MonitoringGroupSetting']['CopyGroupToOtherYear']);
// $btnAry[] = array('import', 'javascript: void(0);');
// $btnAry[] = array('export', 'javascript: void(0);');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);


###Functional Bar END ###
$linterface->LAYOUT_START($Msg);
echo $linterface->Include_Thickbox_JS_CSS();
?>
<script type="text/javascript">
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";
$( document ).ready(function() {
	RecordListTable();
});
function RecordListTable(){
	var AcademicYearID = $('#AcademicYearID').val();
	$('#List').html(loadingImage);
	$.ajax({
		url: "?t=settings.monitoringGroupSetting.ajax_onload_group_list",
		type: 'POST',
		data: {AcademicYearID: AcademicYearID},
		success: function(returnMSG){
			$('#List').html(returnMSG);
		}
	});
}
function CheckAll(Class){
	$('.'+Class).each(function(){
		if( $('.'+Class+'_master').attr("checked") ){
			$(this).attr("checked","checked");
		}else{
			$(this).removeAttr("checked");
		}
	});
}
function Delete(){
	var GroupID_string = '';
	var comma = '';
	$('.delete_checkbox').each(function(){
		if($(this).attr("checked")){
			GroupID_string += comma + $(this).val();
			comma = ',';
		}
	});
	if(GroupID_string){
		if(confirm('<?=$Lang['SDAS']['MonitoringGroupSetting']['DeleteConfirm']?>')){
			$.ajax({
				url: "?t=settings.monitoringGroupSetting.ajax_delete_function",
				type: 'POST',
				data: {GroupID_string: GroupID_string, Action: 'Delete_MONITORING_GROUP'},
				success: function(returnMSG){
					location.reload();
				}
			});
		}
	}else{
		alert('<?=$Lang['SDAS']['MonitoringGroupSetting']['PleaseSelectToDelete']?>');
	}
}
function add_New(GroupID){
	var thickboxTitle = '';
	if(GroupID){
		thickboxTitle = '<?=$Lang['SDAS']['MonitoringGroupSetting']['EditGroup']?>';
	}else{
		thickboxTitle = '<?=$Lang['SDAS']['MonitoringGroupSetting']['NewGroup']?>';
	}
	load_dyn_size_thickbox_ip(thickboxTitle, 'OnLoadAddNewThickBox('+GroupID+');', inlineID='', defaultHeight=600, defaultWidth=800);
}
function OnLoadAddNewThickBox(GroupID){
	$.ajax({
		url: "?t=settings.monitoringGroupSetting.ajax_add_group",
		type: 'POST',
		data: {GroupID: GroupID},
		success: function(ReturnHTML){
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			$('div#TB_ajaxContent').html(ReturnHTML);
		}
	});
}
</script>
<div class="content_top_tool">
	<?=$htmlAry['contentTool']?>
</div>
	<br style="clear:both;">
<?=$htmlAry['AcademicYear']?>
<div id='List' class="table_board"></div>
<?php
$linterface->LAYOUT_STOP();
?>