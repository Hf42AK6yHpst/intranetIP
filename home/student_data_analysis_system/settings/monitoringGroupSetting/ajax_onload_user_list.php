<?php 
/*
 * Using:
 * Change Log:
 * Date:	2017-06-09	Villa
 * 			-Add filtering Teacher/ Student
 * Date:	2017-06-06	Villa
 * 			-Open the file
 */

$GroupIdenity = substr($GroupCode,0,1);
switch ($RoleType){
	default:
		$RecordTypeCond = ' AND 1';
		break;
	case 'S':
		$RecordTypeCond = ' AND iu.RecordType = "2"';
		break;
	case 'T':
		$RecordTypeCond = ' AND iu.RecordType != "2"';
		break;
}
if($GroupIdenity=='G'){
	$GroupId = substr($GroupCode,1);
	if($FilterUserID){
		$Extra_cond = ' AND iug.UserID NOT IN ('.$FilterUserID.')';
	}
	$sql = 	"SELECT 
				iug.UserID,	iug.GroupID, iu.ChineseName, iu.EnglishName
			FROM 
				INTRANET_USERGROUP iug
			INNER JOIN
				INTRANET_USER iu
					ON iug.UserID = iu.UserID
			WHERE
				iug.GroupID = $GroupId
				$Extra_cond
				$RecordTypeCond
			";
}else{
	Switch($GroupIdenity){
		case 'I'://Staff
			$SubGroupIdenity = $GroupIdenity = substr($GroupCode,1,1);
			$RecordType = '1';
			Switch($GroupIdenity){
				case '1'://Teaching Staff
					$Extra_cond = ' AND Teaching = "1"';
					break;
				case '3'://Non Teaching Staff
					$Extra_cond = ' AND Teaching = "0"';
					break;
				case 'S'://supply teacher
					$Extra_cond = ' AND Teaching = "S"';
					break;
			}
			break;
		case 'P'://Parent
			$RecordType = '3';
			$Extra_cond = '';
			break;
		case 'S'://Student
			$RecordType = '2';
			$Extra_cond = '';
			break;
	}
	if($FilterUserID){
		$Extra_cond .= ' AND iu.UserID NOT IN ('.$FilterUserID.')';
	}
	$sql = "SELECT
		iu.UserID,iu.ChineseName, iu.EnglishName
	FROM
		INTRANET_USER iu
	WHERE
		RecordType = '{$RecordType}'
		AND RecordStatus = '1'
		$Extra_cond
	";
}
$data = $objSDAS->returnResultSet($sql);
###Building Selector START
$HTML = '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" multiple="true">';
foreach ((array)$data as $_data){
	$HTML .= '<option value="'.$_data['UserID'].'">'.Get_Lang_Selection($_data['ChineseName'], $_data['EnglishName']).'</option>';
}
$HTML .= '</select>';
echo $HTML;
###Building Selector END
?>