<?php
/*
 * Change Log:
 * Date:	2017-06-09 Villa
 * 			-Add AutoComplete part
 * Date:	2017-06-06 Villa
 * 			-Open the file
 */

//Get Group(ePCM) user: for group only
##Get Existing Member START
$sql = "SELECT
			mgm.UserID, iu.EnglishName, iu.ChineseName
		FROM 
			MONITORING_GROUP_MEMBER mgm
		INNER JOIN
			INTRANET_USER iu ON mgm.UserID = iu.UserID
		WHERE
			mgm.GroupID = $GroupID
			AND	mgm.RecordType = $RecordType
			AND mgm.IsDeleted = '0'
	";
$UserID_inGroup = $objSDAS->returnResultSet($sql);
##Get Exisiting Member END

##Get Intranet Group / Category
if($RecordType=='1'){
	//PIC
	$FilterGroupCat = '0,4,5';
}else{
	//Member
	$FilterGroupCat = '0,1,2,4,5';
}
$sql = "SELECT 
			GroupCategoryID, CategoryName 
		FROM 
			INTRANET_GROUP_CATEGORY
		Where 
			GroupCategoryID not IN ($FilterGroupCat) 
		ORDER BY 
			CategoryName";
$catInfoArr = $objSDAS->returnResultSet($sql);
$catAssoArr = BuildMultiKeyAssoc($catInfoArr,'GroupCategoryID');
$groupCatIdArr = Get_Array_By_Key($catInfoArr,'GroupCategoryID');
$sql = "SELECT 
			GroupID, Title as TitleEn, TitleChinese, RecordType as GroupCategoryID  
		FROM 
			INTRANET_GROUP 
		WHERE 
			RecordType IN ('".implode("','", $groupCatIdArr)."') 
		AND AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
			ORDER BY RecordType,Title";
$groupArr = $objSDAS->returnResultSet($sql);

$optionAry = array();
$role = $Lang['SysMgr']['RoleManagement']['UserType'];
$optionAry['']['-'] = $Lang['SDAS']['MonitoringGroupSetting']['PleaseSelect'];
if($RecordType=='1'){
	$optionAry[$role]["I1"] = $Lang['SysMgr']['RoleManagement']['TeachingStaff'];
	$optionAry[$role]["I3"] = $Lang['SysMgr']['RoleManagement']['SupportStaff'];
	$optionAry[$role]["P"] = $Lang['SysMgr']['RoleManagement']['Parent'];
}elseif($RecordType=='2'){
	$optionAry[$role]["S"] = $Lang['SysMgr']['RoleManagement']['Student'];
}
foreach((array)$groupArr as $_groupInfo){
    $_groupCatLang = $catAssoArr[$_groupInfo['GroupCategoryID']]['CategoryName'];
    $_optionValue = 'G'.$_groupInfo['GroupID'];
    $_optionLang = Get_Lang_Selection($_groupInfo['TitleChinese'], $_groupInfo['TitleEn']);
    $optionAry[$_groupCatLang][$_optionValue] = $_optionLang;
}
$htmlAry['groupCatSelection'] = getSelectByAssoArray($optionAry, 'name="groupCat" id="groupCat" onchange="OnLoadUserList();"', $SelectedType, $all=0, $noFirst=1);

$AddUserID_Box = '<select name="AddUserID[]" id="AddUserID[]" size="10" style="width: 99%" multiple="true">';
foreach((array)$UserID_inGroup as $_UserID_inGroup){
	$AddUserID_Box .= '<option value="'.$_UserID_inGroup['UserID'].'">'.Get_Lang_Selection($_UserID_inGroup['ChineseName'], $_UserID_inGroup['EnglishName']).'</option>';
}
$AddUserID_Box .= '</select>';
###Functional Bar END ###
?>
<script type="text/javascript" src="/templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="/templates/jquery/jquery.autocomplete.css" type="text/css" />
<script type="text/javascript">
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";
$( document ).ready(function() {
	OnLoadUserList();
});
function OnLoadUserList(){
	var GroupCode = $('#groupCat').val();
	var FilterUserID = '';
	var comma = '';
	var UserSelected = document.getElementById('AddUserID[]');
	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		FilterUserID += comma + UserSelected.options[i].value;
		comma = ',';
	}
	$('#AvalUserLayer').html(loadingImage);
	$.ajax({
		url: "?t=settings.monitoringGroupSetting.ajax_onload_user_list",
		data: {
			"GroupCode": GroupCode,
			"FilterUserID": FilterUserID,
			"RoleType": RoleType
			},
		type: "POST",
		success: function(ReturnHTML){
			$('#AvalUserLayer').html(ReturnHTML);
		}
		
	});
}
function Add_All_User(){
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		User = ClassUser.options[i];
		var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
		try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	   	}
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
		ClassUser.options[i] = null;
	}
}

function Add_Selected_Class_User(){
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	
	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		if (ClassUser.options[i].selected) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}
	//Reorder_Selection_List('AddUserID[]');
// 	Update_Auto_Complete_Extra_Para();
}

function Remove_Selected_User(){
//	alert('Remove_Selected_User');
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		if (UserSelected.options[i].selected)
			UserSelected.options[i] = null;
	}

	OnLoadUserList()
// 	Update_Auto_Complete_Extra_Para();
}
function Remove_All_User(){
//	alert('Remove_All_User');
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			UserSelected.options[i] = null;
	}
	OnLoadUserList()
// 	Update_Auto_Complete_Extra_Para();
}
function Add_Role_Member(){
	var GroupID = '<?=$GroupID?>';
	var RecordType = '<?=$RecordType?>';
	var UserID = '';
	var comma = '';
	var UserSelected = document.getElementById('AddUserID[]');
	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		UserID += comma + UserSelected.options[i].value;
		comma = ',';
	}
	$('#AddMemberSubmitBtn').attr('disabled','disabled');
// delete All Member in Group 
	$.ajax({
		url: "?t=settings.monitoringGroupSetting.ajax_delete_function",
		type: 'POST',
		async: false,
		data: {
			GroupID: GroupID, 
			RecordType: RecordType,
			Action: 'Delete_GROUP_RecordType_MONITORING_GROUP_MEMBER'
			},
		success: function(){
		}
	});
//Add UserToTheDb
	$.ajax({
		url: '?t=settings.monitoringGroupSetting.ajax_add_member_update',
		type: 'POST',
		async: false,
		data: {UserIDStr: UserID, GroupID: GroupID, RecordType: RecordType},
		success: function(ReturnMsg){
			window.top.tb_remove();
			Get_Return_Message(ReturnMsg);
			location.reload();
		}
	});
}
//AutoComplete
$("#UserSearch").autocomplete(
'settings/monitoringGroupSetting/ajax_add_member_autocomplete.php',
{
	delay:3,
	minChars:1,
	matchContains:1,
	extraParams: {'field':RoleType}, //RecordType is definded in previous page
	autoFill:false,
	overflow_y: 'auto',
	overflow_x: 'hidden',
	maxHeight: '200px',
	onItemSelect: function(li) {
		var UserSelected = document.getElementById('AddUserID[]');
		var elOptNew = document.createElement('option');
		var ExistedUserID = false;
		for(var i=0; i<UserSelected.length; i++){
			if(UserSelected.options[i].value == li.extra[1]){
				ExistedUserID = true;
			}
		}
		if(!ExistedUserID){
			UserSelected.options[UserSelected.length] = new Option(li.selectValue,li.extra[1]);
		}
		$("#UserSearch").val('');
	}
	
});
</script>
<table class="form_table" style="width: 100%">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td width="50%" bgcolor="#EEEEEE">
									<?=$Lang['Group']['GroupCategory'].':'.$htmlAry['groupCatSelection']?>
								</td>
								<td width="40"></td>
								<td width="50%" bgcolor="#EFFEE2" class="steptitletext"><?=$Lang['SysMgr']['RoleManagement']['SelectedUser']?></td>
							</tr>
							<tr>
								<td bgcolor="#EEEEEE" align="center">
									<div id="AvalUserLayer">
									</div>
									<span class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></span>
								</td>
								<td><input name="AddAll" onclick="Add_All_User();" type="button"
									class="formsmallbutton"
									onmouseover="this.className='formsmallbuttonon'"
									onmouseout="this.className='formsmallbutton'"
									value="&gt;&gt;" style="width: 40px;"
									title="<?=$Lang['Btn']['AddAll']?>" /> <br /> <input name="Add"
									onclick="Add_Selected_Class_User();" type="button"
									class="formsmallbutton"
									onmouseover="this.className='formsmallbuttonon'"
									onmouseout="this.className='formsmallbutton'" value="&gt;"
									style="width: 40px;" title="<?=$Lang['Btn']['AddSelected']?>" />
								<br />
								<br /> 
								<input name="Remove" onclick="Remove_Selected_User();"
									type="button" class="formsmallbutton"
									onmouseover="this.className='formsmallbuttonon'"
									onmouseout="this.className='formsmallbutton'" value="&lt;"
									style="width: 40px;"
									title="<?=$Lang['Btn']['RemoveSelected']?>'" /> <br /> <input
									name="RemoveAll" onclick="Remove_All_User();" type="button"
									class="formsmallbutton"
									onmouseover="this.className='formsmallbuttonon'"
									onmouseout="this.className='formsmallbutton'"
									value="&lt;&lt;" style="width: 40px;"
									title="<?=$Lang['Btn']['RemoveAll']?>" /></td>
								<td rowspan="2" bgcolor="#EFFEE2" id="SelectedUserCell" class="multipleSelectionOnly">
									<?=$AddUserID_Box?>
								</td>
							</tr>
							<tr>
								<td width="50%" bgcolor="#EEEEEE">
                                    <?=$Lang['SysMgr']['FormClassMapping']['Or']?> <br>
                                    <?=$Lang['SysMgr']['RoleManagement']['SearchUser']?> <br>
									<div style="float: left;">
										<input type="text" id="UserSearch" name="UserSearch" value="" />
									</div>
								</td>
								<td>&nbsp;</td>
							</tr>
						</table>
						<p class="spacer"></p>
					</td>
				</tr>
			</table> 
		</td>
	</tr>
</table>
<?php if($EnableSubmit){?>
<div class="edit_bottom">
	<span> </span>
	<p class="spacer"></p>
	<input type="hidden" name="groupID" id="groupID" value="<?$groupID?>">
	<input name="AddMemberSubmitBtn" id="AddMemberSubmitBtn" type="button"
		class="formbutton_v30 " onclick="Add_Role_Member(); return false;"
		value="<?=$Lang['Btn']['Add']?>" /> 
	<input name="AddMemberCancelBtn"
		id="AddMemberCancelBtn" type="button" class="formbutton_v30"
		onclick="window.top.tb_remove(); return false;"
		value="<?=$Lang['Btn']['Cancel']?>" />
</div>
<?php }?>