<?php 
/*
 * Change Log:
 * Date 2017-06-23	Villa
 * 			- Revised the data getting part
 * 
 * Date	2017-06-06	Villa
 * 			- Open the file
 * 
 */

######## Page Setting START ########
$CurrentPage = "settings.monitoringGroup";
$CurrentPageName = $Lang['SDAS']['MonitoringGroupSetting']['MonitoringGroupSetting'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

######## Data Getting START########
if($GroupID){
	$rs = $objSDAS->getMonitoringGroupMember($RecordType,$GroupID);
	foreach ((array)$rs as $_rs){
		$data[$_rs['UserID']]['UserID'] = $_rs['UserID'];
		$data[$_rs['UserID']]['ChineseName'] = $_rs['ChineseName'];
		$data[$_rs['UserID']]['EnglishName'] = $_rs['EnglishName'];
		$data[$_rs['UserID']]['DateModified'] = $_rs['DateModified'];
		$data[$_rs['UserID']]['ClassName'] = Get_Lang_Selection($_rs['ClassTitleB5'], $_rs['ClassTitleEN']);
		$data[$_rs['UserID']]['ClassNumber'] = $_rs['ClassNumber'];
	}
	#Get Group Name 
	$rs = $objSDAS->getMonitoringGroup($GroupID,'');
	$rs = $rs[0];
	$displayGroupName = Get_Lang_Selection($rs['GroupNameCH'], $rs['GroupNameEN']);
	$navigationArr[] = array($Lang['SDAS']['MonitoringGroupSetting']['GroupList'],'?t=settings.monitoringGroupSetting.list');
	$navigationArr[] = array($displayGroupName,'');
	$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationArr);
}
######## Data Getting END########

#### Header Config
if($RecordType=='1'){
	$HeaderConfig = array('ChineseName','EnglishName','DateModified');
}elseif($RecordType=='2'){
	$HeaderConfig = array('ClassName','ClassNumber','ChineseName','EnglishName');
}
$numOfHeader = sizeof($HeaderConfig)+2;

###Functional Btn Related START
$btnAry[] = array('new', 'javascript:addNew();');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
$table_tool = '<div nowrap="nowrap" class="common_table_tool"><a href="#" class="tool_delete" onclick="Delete()">'.$Lang['Btn']['Delete'].'</a></div>';
$CancelBtn  = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.history.back();");
###Functional Btn Related END

######## Table Printing START ######
$TableHTML = '<div class="table_board">';
		$TableHTML .= $table_tool;
		$TableHTML .= '<table style="width:100%;" id="dataTable" class="common_table_list_v30">';
		$TableHTML .= '<thead>';
		$TableHTML .= '<tr>';
			$TableHTML .= '<th style="width: 1px;">'.'#'.'</th>';
			foreach ($HeaderConfig as $_HeaderConfig){
				$TableHTML .= '<th>'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead'][$_HeaderConfig].'</th>';
			}
			$TableHTML .= '<th>'.'<input type="checkbox" onclick="CheckAll(\'delete_checkbox\')" class="delete_checkbox_master">'.'</th>';
		$TableHTML .= '</tr>';
		$TableHTML .= '</thead>';
		$TableHTML .= '<tbody>';
			if(!empty($data)){
				$i = 1;
				foreach ((array)$data as $_data){
					$TableHTML .= '<tr>';
					$TableHTML .= '<td>'.$i.'</td>';
					foreach ($HeaderConfig as $_HeaderConfig){
						$TableHTML .= '<td>'.Get_String_Display($_data[$_HeaderConfig]).'</td>';
					}
					$TableHTML .= '<td style="width: 1px;">'.'<input type="checkbox" class="delete_checkbox" value="'.$_data['UserID'].'">'.'</td>';
					$TableHTML .= '</tr>';
					$i++;
				}
			}else{
				$TableHTML .= '<tr>';
					$TableHTML .= '<td colspan="'.$numOfHeader.'" style="text-align:center;">'.$Lang['SDAS']['MonitoringGroupSetting']['NoMember'] .'</td>';
				$TableHTML .= '</tr>';
			}
		$TableHTML .= '</tbody>';
		$TableHTML .= '</table>';
$TableHTML .= '</div>';

######## Table Printing END ######
$linterface->LAYOUT_START($Msg);
echo $linterface->Include_Thickbox_JS_CSS();
?>
<?=$htmlAry['navigation']?>
<div class="content_top_tool">
	<?=$htmlAry['contentTool']?>
</div>
<br style="clear:both;">
<div id='List'><?=$TableHTML?></div>
<div class="edit_bottom_v30">
<?=$CancelBtn?>
</div>

<style>
.tablesorter-default .header,
.tablesorter-default .tablesorter-header {
	padding: 4px 20px 4px 4px;
	cursor: pointer;
	background-image: url(data:image/gif;base64,R0lGODlhFQAJAIAAAP///////yH5BAEAAAEALAAAAAAVAAkAAAIXjI+AywnaYnhUMoqt3gZXPmVg94yJVQAAOw==);
	background-position: center right;
	background-repeat: no-repeat;
}
.tablesorter-default .headerSortUp,
.tablesorter-default .tablesorter-headerSortUp,
.tablesorter-default .tablesorter-headerAsc {
	background-image: url(data:image/gif;base64,R0lGODlhFQAEAIAAAP///////yH5BAEAAAEALAAAAAAVAAQAAAINjI8Bya2wnINUMopZAQA7);
	color: #fff;
}
.tablesorter-default .headerSortDown,
.tablesorter-default .tablesorter-headerSortDown,
.tablesorter-default .tablesorter-headerDesc {
	color: #fff;
	background-image: url(data:image/gif;base64,R0lGODlhFQAEAIAAAP///////yH5BAEAAAEALAAAAAAVAAQAAAINjB+gC+jP2ptn0WskLQA7);
}
</style>
<!-- <script type="text/javascript" src="/templates/jquery/tablesorter/jquery.tablesorter.min.js"></script> -->
<script type="text/javascript">
var RoleType = 'S'; //setting for AutoComplete @ajax_add_member_thickbox
$( document ).ready(function() {
// 	$("#dataTable").tablesorter({
// 		 headers: { 
// 	            0: { 
// 	                // disable it by setting the property sorter to false 
// 	                sorter: false 
// 	            }, 
// 	            6: { 
// 	                // disable it by setting the property sorter to false 
// 	                sorter: false 
// 	            } 
// 	        } 
// 	});
});
function addNew(){
	load_dyn_size_thickbox_ip('<?=$Lang['SDAS']['MonitoringGroupSetting']['AddMember']?>', 'onloadThickBox();', inlineID='', defaultHeight=450, defaultWidth=800);
}
function onloadThickBox(){
	$.ajax({
		url: '?t=settings.monitoringGroupSetting.ajax_add_member_thickbox',
		type: 'POST',
		data: {GroupID : '<?=$GroupID?>', RecordType : '<?=$RecordType?>', EnableSubmit : true},
		success: function(ReturnHTML){
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			$('div#TB_ajaxContent').html(ReturnHTML);
		}
	});
}
function CheckAll(Class){
	$('.'+Class).each(function(){
		if( $('.'+Class+'_master').attr("checked") ){
			$(this).attr("checked","checked");
		}else{
			$(this).removeAttr("checked");
		}
	});
}

function Delete(){
	var User_string = '';
	var GroupID = '<?=$GroupID?>';
	var comma = '';
	$('.delete_checkbox').each(function(){
		if($(this).attr("checked")){
			User_string += comma + $(this).val();
			comma = ',';
		}
	});
	if(User_string){
		if(confirm('<?=$Lang['SDAS']['MonitoringGroupSetting']['DeleteConfirm']?>')){
			$.ajax({
				url: "?t=settings.monitoringGroupSetting.ajax_delete_function",
				type: 'POST',
				data: {
					UserID_string: User_string,
					GroupID: GroupID, 
					Action: 'Delete_MONITORING_GROUP_MEMBER'
					},
				success: function(returnMSG){
					location.reload();
				}
			});
		}
	}else{
		alert('<?=$Lang['SDAS']['MonitoringGroupSetting']['PleaseSelectToDelete']?>');
	}
}
</script>
<?php 
$linterface->LAYOUT_STOP();
?>