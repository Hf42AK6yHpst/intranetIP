<?php
# using:

/* Change Log
 * Date:	2017-07-12 Villa
 * -		Modified to only selected group will be shown  
 * Date		2017-06-08 Villa
 * -		Open the file
 */

######## Page Setting START ########
$CurrentPage = "settings.monitoringGroup";
$CurrentPageName = $Lang['SDAS']['MonitoringGroupSetting']['MonitoringGroupSetting'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########
### Data Getting
$sql = 'SELECT
			ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
		FROM
			ACADEMIC_YEAR ay
		INNER JOIN
			ACADEMIC_YEAR_TERM ayt
				ON	ay.AcademicYearID = ayt.AcademicYearID
		WHERE 
			ay.AcademicYearID = '.$FromAcademicYearID;
$rs = $objSDAS->returnResultSet($sql);
$FromYearName = Get_Lang_Selection($rs[0]['YearNameB5'], $rs[0]['YearNameEN']);
$sql = 'SELECT
			ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
		FROM
			ACADEMIC_YEAR ay
		WHERE 
			ay.AcademicYearID = '.$ToAcademicYearID;
$rs = $objSDAS->returnResultSet($sql);
$ToYearName= Get_Lang_Selection($rs[0]['YearNameB5'], $rs[0]['YearNameEN']);

$sql = 'SELECT
			GroupID, GroupNameEN, GroupNameCH
		FROM
			MONITORING_GROUP
		WHERE
			AcademicYearID = '.$FromAcademicYearID.'
		AND
			IsDeleted = "0"
		ORDER BY
			GroupID desc';
$rs_From =  $objSDAS->returnResultSet($sql);

$CopyGroup_GroupID_Str = implode(',',(array)$CopyGroup_GroupID);
$CopyMember_GroupID_Str = implode(',',(array)$CopyMember_GroupID);
### Data Getting 
$TableHTML = '<table style="width: 100%;" class="common_table_list_v30">';
$TableHTML .= '<tr>';
#GroupNameEN
$TableHTML .= '<th style="width: 35%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameEN'].'</th>';
#GroupNameCH
$TableHTML .= '<th style="width: 35%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameCH'].'</th>';
#Copy
$TableHTML .= '<th style="text-align: center; width: 15%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['Copy'].'</th>';
#CopyMember
$TableHTML .= '<th style="text-align: center; width: 15%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['CopyMember'].'</th>';
$rs_From = BuildMultiKeyAssoc($rs_From, 'GroupID');
$TableHTML .= '</tr>';
foreach ($rs_From as $_rs_From){
// foreach ($CopyGroup_GroupID as $GroupID){
	$TableHTML .= '<tr>';
	#GroupNameEN
	$TableHTML .= '<td>'.$_rs_From['GroupNameEN'].'</td>';
	#GroupNameCH
	$TableHTML .= '<td>'.$_rs_From['GroupNameCH'].'</td>';
	#Copy
	$TableHTML .= '<td>';
	$TableHTML .= in_array($_rs_From['GroupID'],(array)$CopyGroup_GroupID)?$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['Yes'] :('<span class="tabletextremark">'.$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['No'].'</span>');
// 	$TableHTML .= in_array($GroupID,(array)$CopyGroup_GroupID)?$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['Yes'] :('<span class="tabletextremark">'.$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['No'].'</span>');
	$TableHTML .= '</td>';
	
	#CopyMember
	$TableHTML .= '<td>';
	$TableHTML .= in_array($_rs_From['GroupID'],(array)$CopyMember_GroupID)?$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['Yes'] :('<span class="tabletextremark">'.$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['No'].'</span>');
// 	$TableHTML .= in_array($GroupID,(array)$CopyMember_GroupID)?$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['Yes'] :('<span class="tabletextremark">'.$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['No'].'</span>') ;
	
	$TableHTML .= '</td>';
	
	$TableHTML .= '</tr>';
}
$TableHTML .= '</table>';
$TableHTML .= '';
### Build table'

### Get Btn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", " window.history.back();","back"," class='formbutton_v30'  ");
$NextBtn = $linterface->GET_ACTION_BTN($button_continue, "button", "javascript:goSubmit()","submit2"," class='formbutton_v30'");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='?t=settings.monitoringGroupSetting.list'","back"," class='formbutton_v30'");
$Btn = $NextBtn."&nbsp;".$BackBtn."&nbsp;".$CancelBtn;

#Navigation
$navigationArr[] = array($Lang['SDAS']['MonitoringGroupSetting']['GroupList'],'?t=settings.monitoringGroupSetting.list');
$navigationArr[] = array($Lang['SDAS']['MonitoringGroupSetting']['CopyGroup'],'');
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationArr);
#Step Obj
$STEPS_OBJ[] = array($Lang['Group']['Options'], 0);
$STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 1);
$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 0);
$htmlAry['stepobj'] = $linterface->GET_STEPS($STEPS_OBJ);

$linterface->LAYOUT_START();
?>
<br />
<form id="form1" name="form1" method="POST" action="" >
<?=$htmlAry['navigation']?>
<div style="padding-top:25px;"><?= $htmlAry['stepobj']?></div>
<br />
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['Group']['CopyFrom']?></td>
		<td><?=$FromYearName?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['Group']['CopyTo']?></td>
		<td><?=$ToYearName?></td>
	</tr>
</table>
<br style="clear:both;" />
<table class="form_table_v30">
	<tr>
		<td style="border-bottom-style:none;"><?=$linterface->GET_NAVIGATION2($Lang['Group']['Group'])?></td>
	</tr>
	<tr>
		<td style="border-bottom-style:none;"><?=$TableHTML?></td>
	</tr>
</table>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$Btn?>
	<p class="spacer"></p>
</div>


<input type="hidden" value="<?=$FromAcademicYearID?>" name="FromAcademicYearID">
<input type="hidden" value="<?=$ToAcademicYearID?>" name="ToAcademicYearID">
<input type="hidden" value="<?=$CopyGroup_GroupID_Str?>" name="CopyGroup_GroupID_Str">
<input type="hidden" value="<?=$CopyMember_GroupID_Str?>" name="CopyMember_GroupID_Str">
<input type="hidden" name="data" id="data">
</form>
<br><br>
<script>
function goSubmit(){
	$('#submit2').attr("disabled","disabled");
	$.ajax({
		async: false,
		url : "?t=settings.monitoringGroupSetting.copy_to_other_year_update",
		type: "POST",
		data: $('#form1').serialize(),
		success: function(data){
			$('#data').val(data);
			$('#form1').attr('action','?t=settings.monitoringGroupSetting.copy_to_other_year_result');
			$('#form1').submit();
		}
	});
}
function jsGoBack(){
}
</script>

<?
$linterface->LAYOUT_STOP();
?>
