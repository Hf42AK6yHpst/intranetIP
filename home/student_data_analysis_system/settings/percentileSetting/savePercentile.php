<?php

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();

$isDataCorrect = true;
######## Init END ########


######## Access Right START ########
######## Access Right END ########


######## Validate START ########
$totalPercent = array_sum((array)$_POST['Percent']);
if($totalPercent != 100){
	$isDataCorrect = false;
}

foreach((array)$_POST['Title'] as $title){
	if(trim($title) == ''){
		$isDataCorrect = false;
		break;
	}
}
######## Validate END ########


######## Pack Data START ########
$data = array(
	'Percent' => $_POST['Percent'],
	'Title' => $_POST['Title'],
);
######## Pack Data END ########


######## Save Percentile START ########
if($isDataCorrect){
	$result = $lpf->updateAllPercentileSetting($data);
}else{
	$result = false;
}
######## Save Percentile END ########



######## Redirect Start ########
if(!in_array( false, (array)$result)){
	$Msg = 'UpdateSuccess';
}else{
	$Msg = 'UpdateUnsuccess';
}
header("Location: index.php?t=settings.percentileSetting.setting&Msg={$Msg}");
exit;

?>