<?php
## Using By : 
##############################################
##	Modification Log:
##	2016-07-06 (Omas)
##	- copy from iPo
##
##############################################

// $PATH_WRT_ROOT = "../../../../../";
// include_once($PATH_WRT_ROOT."includes/global.php");
// include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");


$lpf = new libportfolio();
##################################################
############  Cotnent Here #######################
##################################################

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");
$libpf_academic = new libpf_academic();

// $_POST['FromYearID'] $_POST['ToYearID']
$fromAcdYearID = $_POST['FromYearID'];
$toAcdYearID = $_POST['ToYearID'];

# Insert Record
$libpf_academic->Start_Trans();

// $yearScoreSettingArr[AcademicYearID][YearID][SubjectID]
$yearScoreSettingArr = $libpf_academic->Get_Subject_Full_Mark(array($fromAcdYearID, $toAcdYearID));
$fromYearSetting = $yearScoreSettingArr[$fromAcdYearID];
foreach((array)$fromYearSetting as $_yearID => $_subjectSettingArr){
	foreach((array)$_subjectSettingArr as $__subjectID => $__subjectInfoArr){
		$___targetfullMarkArr['FullMarkInt'] = $__subjectInfoArr['FullMarkInt'];
		$___targetfullMarkArr['PassMarkInt'] = $__subjectInfoArr['PassMarkInt'];
		$___targetfullMarkArr['FullMarkGrade'] = $__subjectInfoArr['FullMarkGrade'];
		
		if (isset($yearScoreSettingArr[$toAcdYearID][$_yearID][$__subjectID])) {
			// update
			$___targetfullMarkID = $yearScoreSettingArr[$toAcdYearID][$_yearID][$__subjectID]['FullMarkID'];
			$SuccessArr['Update'][$_yearID][$__subjectID] = $libpf_academic->Update_Subject_Full_Mark($___targetfullMarkID, $___targetfullMarkArr);
		}
		else{
			// insert
			$InsertValueArr[] = array(	
					'AcademicYearID' => $toAcdYearID,
					'YearID' => $_yearID,
					'SubjectID' => $__subjectID,
					'FullMarkInt' => $___targetfullMarkArr['FullMarkInt'],
					'PassMarkInt' => $___targetfullMarkArr['PassMarkInt'],
					'FullMarkGrade' => $___targetfullMarkArr['FullMarkGrade'],
			);
		}
		$SuccessArr['UpdatePassTotal'][$_yearID][$__subjectID] = $lpf->UpdatePassTotal(
				$toAcdYearID,
				$__subjectID,
				$_yearID,
				$___targetfullMarkArr['FullMarkInt'],
				$___targetfullMarkArr['PassMarkInt']
				);
	}
}

if (count($InsertValueArr) > 0) {
	$SuccessArr['Insert'] = $libpf_academic->Insert_Subject_Full_Mark($InsertValueArr);
}

if (in_multi_array(false, $SuccessArr)) {
	$libpf_academic->RollBack_Trans();
	$returnString = '0';
}
else {
	$libpf_academic->Commit_Trans();
	$returnString =  '1';
}
##################################################
##################################################
##################################################
intranet_closedb();
header("Location: index.php?t=settings.fullmarkSetting.subject_full_mark&msg=$returnString&AcademicYearID=$toAcdYearID");
exit;
?>