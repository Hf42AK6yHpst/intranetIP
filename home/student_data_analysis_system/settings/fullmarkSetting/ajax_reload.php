<?php
## Using By : 
##############################################
##	Modification Log:
##	2017-02-28 (Villa)
##	- Add Recalculate SD Mean BTN
##	2016-07-06 (Omas)
##	- copy from iPo
##
##############################################

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic-ui.php");


$libpf_academic = new libpf_academic();
$libpf_academic_ui = new libpf_academic_ui();

$Action = stripslashes($_REQUEST['Action']);
if ($Action == 'Reload_FullMark_Table')
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	
	$AcademicYearID = $_POST['AcademicYearID'];
	$YearID = $_POST['YearID'];
	$YearID = ($YearID==-1)? '' : $YearID;
	$ViewMode = $_POST['ViewMode'];
	//Get the first modified data for the Form Start
	$cond = "	AcademicYearID = '$AcademicYearID' ";
	if($YearID){
		$cond .= " AND YearID = '$YearID' ";
	}
	$full_mark_sql = "
			SELECT 
				SubjectID, YearID, AcademicYearID, DateModified
			FROM
				$eclass_db.ASSESSMENT_SUBJECT_FULLMARK 
			WHERE 
				$cond
			Order BY
				DateModified Desc
			Limit 1
			";
	$rs = $libpf_academic->returnResultSet($full_mark_sql);
	$LastDateModified_FULLMARK = $rs[0]['DateModified'];
	
	$cond = "	AcademicYearID = '$AcademicYearID' ";
	if($YearID){
		$cond .= " AND ClassLevelID = '$YearID' ";
	}
	$SD_MEAN_sql = "
			SELECT 
				SubjectID, ClassLevelID, AcademicYearID, InputDate
			FROM 
				$eclass_db.ASSESSMENT_SUBJECT_SD_MEAN
			WHERE
				$cond
			Order BY
				InputDate Desc
			Limit 1
			";
	$rs = $libpf_academic->returnResultSet($SD_MEAN_sql);
	$LastDateModified_SDMEAN = $rs[0]['InputDate'];
	//Get the first modified data for the Form END

	//Toolbar Start print only @the data is set and the update of fullmark is later than the update of FULL MARK
	if($LastDateModified_FULLMARK>$LastDateModified_SDMEAN && $LastDateModified_SDMEAN){
// 	$html = '/home/student_data_analysis_system/settings/fullmarkSetting/recalculate_sd_mean.php?AcademicYearID='.$AcademicYearID.'&YearID='.$YearID;
	$html = 'index.php?t=settings.fullmarkSetting.recalculate_sd_mean&AcademicYearID='.$AcademicYearID.'&YearID='.$YearID;
	$js = '<script>';
	$js .= 'function js_recalculate(){
			 window.location.href = "'.$html.'";
			}
			';
	$js .= '</script>';
	$Toolbar = '<div class="Conntent_tool">';
		$Toolbar .= $libpf_academic_ui->Get_Content_Tool_v30("copy", "javascript:js_recalculate();", $Lang['SDAS']['FullMarkSetting']['RecalculateSDMEAN'] );
	$Toolbar .= '</div>';
	$Toolbar .= '</br>';
	$Toolbar .= $js;
	}else{
		$Toolbar = '';
	}
	$x = $Toolbar;
	$x .=  $libpf_academic_ui->Get_Settings_Subject_FullMark_Table($AcademicYearID, $YearID, $ViewMode);
	
	echo $x;
}
?>