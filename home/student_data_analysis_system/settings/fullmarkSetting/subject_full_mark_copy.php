<?php
## Using By : 
##############################################
##	Modification Log:
##	2016-07-06 (Omas)
##	- copy from iPo
##
##############################################

// $PATH_WRT_ROOT = "../../../../../";
// include_once($PATH_WRT_ROOT."includes/global.php");
// include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");


######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "settings.fullmarkSetting";
$CurrentPageName = $Lang['iPortfolio']['SubjectFullMark'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

$AcademicYearID = stripslashes($_POST['AcademicYearID']);

$lpf = new libpf_slp();
$libfcm_ui = new form_class_manage_ui();

##################################################
############  Cotnent Here #######################
##################################################

// Academic Year Selection
$FromYearSelection = getSelectAcademicYear('FromYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value);"', $noFirst=1, $noPastYear=0, '');
$ToYearSelection = getSelectAcademicYear('ToYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value);"', $noFirst=1, $noPastYear=0, $AcademicYearID);

// Save Button
$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Go_Submit();", $id="Btn_Save");
$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();", $id="Btn_Cancel");

// hidden
$hidden = '';
$hidden .= $linterface->GET_HIDDEN_INPUT('AcademicYearID', 'AcademicYearID', $AcademicYearID);
$hidden .= $linterface->GET_HIDDEN_INPUT('YearID', 'YearID', $YearID);

##################################################
##################################################
##################################################

### Title ###

$linterface->LAYOUT_START();
?>
<script>
var canSubmit = true;
function js_Go_Submit(){
	if(canSubmit){
		$('div.warnMsgDiv').hide();
		
		if( $('#FromYearID').val() == $('#ToYearID').val() ){
	// 		alert('Can not copy to same year');
			$('div.warnMsgDiv').show();
			return false;	
		}
		canSubmit = false;
		$('#form1').submit();
	}
	else{
		return false;
	}
}

function js_Go_Back(){
// 	window.location = 'index.php?t=settings.fullmarkSetting.subject_full_mark';
	$('#form1').attr('action','index.php?t=settings.fullmarkSetting.subject_full_mark').submit();
}
</script>
<form id="form1" name="form1" action="index.php?t=settings.fullmarkSetting.subject_full_mark_copy_update" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td style="padding-left:7px;">
				<table class="form_table_v30" width="90%" align="center">
					<tr>	
						<td class="field_title" align="left" width="30%"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyFromAcademicYear']?></td>
						<td class="tabletext">
							<?php echo $FromYearSelection?>
							<?php echo $linterface->Get_Form_Warning_Msg('sameYearWarnDiv', $Lang['iPortfolio']['CannotSameYear'], $Class='warnMsgDiv')."\n" ?>
						</td>
					</tr>
					<tr>
						<td class="field_title" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear']?> </td>
						<td class="tabletext">
							<?php echo $ToYearSelection?>
							<?php echo $linterface->Get_Form_Warning_Msg('sameYearWarnDiv', $Lang['iPortfolio']['CannotSameYear'], $Class='warnMsgDiv')."\n" ?>
						</td>
					</tr>
				</table>
				
				<div class="edit_bottom_v30">
					<?=$SaveBtn?>
					<?=$BackBtn?>
				</div>
			</td>
		</tr>
	</table>
	<?php echo $hidden?>
</form>

<?php
$linterface->LAYOUT_STOP();
?>