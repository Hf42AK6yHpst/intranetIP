<?php
## Using By : 
##############################################
##	Modification Log:
##	2016-07-06 (Omas)
##	- copy from iPo
##
##############################################
// debug_pr($_COOKIE);
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "settings.fullmarkSetting";
$CurrentPageName = $Lang['iPortfolio']['SubjectFullMark'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

// Get View Mode
// $ViewMode = stripslashes($_REQUEST['ViewMode']);
$ViewMode = 'Mark';
$AcademicYearID = (isset($_REQUEST['AcademicYearID'])? $_REQUEST['AcademicYearID']: Get_Current_Academic_Year_ID());
$YearID = (isset($_REQUEST['YearID'])? $_REQUEST['YearID']: '');

$lpf = new libpf_slp();
$libfcm_ui = new form_class_manage_ui();

##################################################
############  Cotnent Here #######################
##################################################

// Export , Copy
$Toolbar = $linterface->Get_Content_Tool_v30("export", "javascript:js_Go_Export();");
$Toolbar .= $linterface->Get_Content_Tool_v30("copy", "javascript:js_Go_Copy();", $Lang['SysMgr']['FormClassMapping']['CopyFromOtherAcademicYear'] );

// View Mode
// $ViewModeArr = array();
// $ViewModeArr[] = array($Lang['General']['Grade'], "javascript:js_Change_ViewMode('Grade');", '', ($ViewMode=='Grade')? 1 : 0);
// $ViewModeArr[] = array($Lang['General']['Mark'], "javascript:js_Change_ViewMode('Mark');", '', ($ViewMode=='Mark')? 1 : 0);
// $ViewModeArr[] = array($Lang['General']['All'], "javascript:js_Change_ViewMode('');", '', ($ViewMode=='')? 1 : 0);
// $ViewModeButton = $linterface->GET_CONTENT_TOP_BTN($ViewModeArr); 

// Tips Instruction
$IipsBox = $linterface->Get_Warning_Message_Box($Lang['General']['Tips'], $Lang['iPortfolio']['SubjectFullMarkInputTips'], $others="");


// Academic Year Selection
$AcademicYearSelection = getSelectAcademicYear('AcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value);"', $noFirst=1, $noPastYear=0, $AcademicYearID);

// Form Selection
$FormSelection = $libfcm_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=1, $isMultiple=0);

// Save Button
$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Save_FullMark_Info();", $id="Btn_Save");

##################################################
##################################################
##################################################

if($msg === 1)
	$xmsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
else if($msg === 0)
	$xmsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
else
	$xmsg = '';

$linterface->LAYOUT_START($xmsg);
// echo $linterface->Include_Cookies_JS_CSS();
echo $linterface->Include_CopyPaste_JS_CSS('2016');
echo $linterface->Include_Excel_JS_CSS();
?>
<script>
var jsCurAcademicYearID = '<?php echo $AcademicYearID?>';
var jsCurYearID = '<?php echo $YearID?>';

// var arrCookies = new Array();
// arrCookies[arrCookies.length] = "sdas_fullmark_AcademicYearID";
// arrCookies[arrCookies.length] = "sdas_fullmark_YearID";

$(document).ready( function() {
	<? if($clearCoo) { ?>
// 		for(i=0; i<arrCookies.length; i++)
// 		{
// 			var obj = arrCookies[i];
// 			//alert('obj = ' + obj);
// 			$.cookies.del(obj);
// 		}
	<? } ?>
	
// 	Blind_Cookies_To_Object();
	
	// Store the cookies value and apply them to the selection
// 	jsCurAcademicYearID = $.cookies.get(arrCookies[0]);
	if (jsCurAcademicYearID == null || jsCurAcademicYearID == '')
		jsCurAcademicYearID = $('select#AcademicYearID').val();
	
// 	jsCurYearID = $.cookies.get(arrCookies[1]);
	if (jsCurYearID == null || jsCurYearID == '')
		jsCurYearID = $('select#YearID option:nth-child(2)').val();
		
	$('select#AcademicYearID').val(jsCurAcademicYearID);
	$('select#YearID').val(jsCurYearID);
	
	js_Reload_FullMark_Table();
});

function js_Changed_AcademicYear_Selection(jsAcademicYearID) {
	jsCurAcademicYearID = jsAcademicYearID;
	js_Reload_FullMark_Table();
}

function js_Changed_Form_Selection(jsYearID) {

	var jsReload = true;
	if (jsYearID == '-1') {
		if (confirm('<?=$Lang['iPortfolio']['LoadAllFullMarkWarning']?>')) {
			jsReload = true;
		}
		else {
			jsReload = false;
			$('select#YearID').val(jsCurYearID);
		}
	}
	
	if (jsReload) {
		jsCurYearID = jsYearID;
		js_Reload_FullMark_Table();
	} 
}

function js_Reload_FullMark_Table() {
	$('div#FullMarkDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php?t=settings.fullmarkSetting.ajax_reload", 
		{
			Action: 'Reload_FullMark_Table',
			AcademicYearID: jsCurAcademicYearID,
			YearID: jsCurYearID,
			ViewMode : '<?=$ViewMode?>'
		},
		function(ReturnData)
		{
			jQuery.excel('dataRow');
		}
	);
}

function js_Save_FullMark_Info() {
	
	Block_Element('FullMarkDiv');
	js_Disable_Button('Btn_Save');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	jsSubmitString += '&Action=Save_Subject_FullMark';
	$.ajax({  
		type: "POST",  
		url: "index.php?t=settings.fullmarkSetting.ajax_update",
		data: jsSubmitString,
		success: function(data) {
			
			if (data=='1')
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				js_Reload_FullMark_Table();
			}
			else
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
			}
			
			UnBlock_Element('FullMarkDiv');
			js_Enable_Button('Btn_Save');
			Scroll_To_Top();
		} 
	});  
	return false;
}

function js_Change_ViewMode(jsViewMode)
{
	$('input#ViewMode').val(jsViewMode);
	$('form#form1').submit();
}

function js_Go_Export()
{
	$('form#form1').attr('action', 'index.php?t=settings.fullmarkSetting.subject_full_mark_export').submit();
	$('form#form1').attr('action', 'index.php?t=settings.fullmarkSetting.subject_full_mark');
}

function js_Go_Copy()
{
	$('form#form1').attr('action', 'index.php?t=settings.fullmarkSetting.subject_full_mark_copy').submit();
}

</script>
<form id="form1" name="form1" action="index.php?t=settings.fullmarkSetting.subject_full_mark" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td style="padding-left:7px;">
				<div class="content_top_tool">
					<div class="Conntent_tool"><?=$Toolbar?></div>
				</div>
				
				<div class="content_top_tool">
					<?=$ViewModeButton?>
				</div>
				
				<div><?=$IipsBox?></div>
				
				<div class="table_filter">
					<?=$AcademicYearSelection?>
					&nbsp;
					<?=$FormSelection?>
				</div>
				<br style="clear:both;" />
				<br style="clear:both;" />
				
				<div id="FullMarkDiv"></div>
				<br style="clear:both;" />
				
				<div class="edit_bottom_v30">
					<?=$SaveBtn?>
				</div>
			</td>
		</tr>
	</table>
	
	<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>
	
	<input type="hidden" id="ViewMode" name="ViewMode" value="<?=$ViewMode?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
?>