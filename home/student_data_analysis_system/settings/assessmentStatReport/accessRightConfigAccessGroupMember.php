<?php
//using 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$obj = new libportfolio2007();

if (!$obj->IS_ADMIN_USER($UserID) && !$obj->IS_TEACHER() && $plugin['StudentDataAnalysisSystem_Style'] != "tungwah")
{
    header ("Location: /");
    intranet_closedb();
    exit();
}

$objSDAS = new libSDAS();

$CurrentPage = "settings.assessmentStatReport";
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['PIC'], "?t=settings.assessmentStatReport.accessRightConfigAdmin", 0);
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['SubjectPanel'] , "?t=settings.assessmentStatReport.accessRightConfigSubjectPanel", 0);
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessGroup']['Title'], "?t=settings.assessmentStatReport.accessRightConfigAccessGroup", 1);
if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    $TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['MonthlyReport']  , "?t=settings.assessmentStatReport.accessRightConfigMonthlyReport", 0);
    $TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['MonthlyReportSection'] , "?t=settings.assessmentStatReport.accessRightConfigMonthlyReportSection", 0);
}
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();

$groupID = $_GET['GroupID'];
$group = $objSDAS->getAccessGroup($groupID);
$group = $group[0];

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['SDAS']['Settings']['AccessGroup']['Title'], "");
$hiddenFields = "";

$PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

######################## Member Selection START #####################
$memberArr = $objSDAS->getAccessGroupMember($groupID);
$Sel = "<select name='member[]' id='member' size='6' multiple='multiple'>";
foreach($memberArr as $member){
    $Sel .= "<option value='$member[UserID]'>$member[Name]</option>";
}
$Sel .= "</select>";
$btn_remove = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['member[]'])");
######################## PIC Selection END #######################

// Buttons
$btnSubmit = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit");
$btnBack =  $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick = "window.location.href='?t=settings.assessmentStatReport.accessRightConfigAccessGroup'");

// Hidden form value
$hiddenFields .= "<input type='hidden' name='groupID' value='$groupID' />";
$hiddenFields .= "<input type='hidden' name='action' value='member' />";

$linterface->LAYOUT_START();
?>
<?php echo $PageNavigation;?>
<form name="form1" id="form1" method="POST" action="?t=settings.assessmentStatReport.accessRightConfigAccessGroupUpdate" >
    <table class="form_table_v30">
    <col class="field_title">
    <col class="field_c">
    	<tr>
        	<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['SDAS']['Settings']['AccessGroup']['Name_ch']?></td>
        	<td>
        		<?php echo $group['Name_ch']?>
        	</td>
        </tr>
        <tr>
        	<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['SDAS']['Settings']['AccessGroup']['Name_en']?></td>
        	<td>
        		<?php echo $group['Name_en']?>
        	</td>
        </tr>
        <tr>
        	<td class="field_title"><?php echo $Lang['SDAS']['Settings']['AccessGroup']['Description']?></td>
        	<td>
        		<?php echo $group['Description']?>
        	</td>
        </tr>
        <tr>
        	<td class="field_title"><?php echo $Lang['SDAS']['Settings']['AccessGroup']['Member']?></td>
        	<td>
        		<table border="0" cellpadding="0" cellspacing="0">
					<tr>
        				<td><?= $Sel?></td>
        				<td valign="bottom">
        					&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=member[]&permitted_type=1')")?><br />
        					&nbsp;<?=$btn_remove?>
						</td>
					</tr>
				</table>
        	</td>
        </tr>
        <?php echo $hiddenFields;?>
    </table>
    <div class="edit_bottom_v30">
    	<?php echo $btnSubmit;?>
    	&nbsp;
    	<?php echo $btnBack;?>
    </div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$('#form1').submit(function(){
			$('option[value=""]').remove();
			$('#member option').attr('selected', 'selected');
		});
	});
</script>
<?php 
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>