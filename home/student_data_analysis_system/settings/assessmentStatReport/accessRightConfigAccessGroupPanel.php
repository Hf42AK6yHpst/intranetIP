<?php
//using 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$obj = new libportfolio2007();

if (!$obj->IS_ADMIN_USER($UserID) && !$obj->IS_TEACHER() && $plugin['StudentDataAnalysisSystem_Style'] != "tungwah")
{
    header ("Location: /");
    intranet_closedb();
    exit();
}

$objSDAS = new libSDAS();

$CurrentPage = "settings.assessmentStatReport";
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['PIC'], "?t=settings.assessmentStatReport.accessRightConfigAdmin", 0);
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['SubjectPanel'] , "?t=settings.assessmentStatReport.accessRightConfigSubjectPanel", 0);
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessGroup']['Title'], "?t=settings.assessmentStatReport.accessRightConfigAccessGroup", 1);
if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    $TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['MonthlyReport']  , "?t=settings.assessmentStatReport.accessRightConfigMonthlyReport", 0);
    $TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['MonthlyReportSection'] , "?t=settings.assessmentStatReport.accessRightConfigMonthlyReportSection", 0);
}
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();

$groupID = $_GET['GroupID'];
if($groupID != ""){
    $group = $objSDAS->getAccessGroup($groupID);
    if(sizeof($group) > 0){
        $group = $group[0];
        $rights = $objSDAS->getAccessGroupRights($groupID);
    } else {
        unset($group);
    }
}

$academicStatisticNameArr = array();
foreach($MODULE_OBJ['menu']['academic']['Child'] as $statisticName => $statisticDetails){
    $academicStatisticArr[] = $statisticName;
}

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['SDAS']['Settings']['AccessRight']['AccessGroup'], "");
$checkboxTable = "";
$hiddenFields = "";

if(isset($rights)){
    $rightArr = array();
    $PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
    foreach($rights as $right){
        $rightArr[$right['AcademicStatisticName']] = $right['RightType'];
    }
    foreach($academicStatisticArr as $ap){
        $checked = $rightArr[$ap] ? 'checked' : '';
        $value = $rightArr[$ap] ? $rightArr[$ap] : '0';
        $checkboxTable .= "<tr>";
            $checkboxTable .= "<td class='field_title'>". $MODULE_OBJ['menu']['academic']['Child'][$ap][0] . "</td>";
            $checkboxTable .= "<td><input type='checkbox' class='ckb_accessRight' $checked alt='$ap' /></td>";
            $hiddenFields .= "<input type='hidden' class='ap' name='academicStatistic[$ap]' id='ap_$ap' value='$value' />";
        $checkboxTable .= "</tr>";
    }
} else {
    $PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
    foreach($academicStatisticArr as $ap){
        $checkboxTable .= "<tr>";
            $checkboxTable .= "<td class='field_title'>".$MODULE_OBJ['menu']['academic']['Child'][$ap][0]."</td>";
            $checkboxTable .= "<td><input type='checkbox' class='ckb_accessRight' alt='$ap'/></td>";
            $hiddenFields .= "<input type='hidden' class='ap' name='academicStatistic[$ap]' id='ap_$ap' value='0' />";
        $checkboxTable .= "</tr>";
    }
}
$PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

// Buttons
$btnSubmit = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit");
$btnBack =  $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick = "window.location.href='?t=settings.assessmentStatReport.accessRightConfigAccessGroup'");

// Hidden form value
$hiddenFields .= "<input type='hidden' name='groupID' value='$groupID' />";
$hiddenFields .= "<input type='hidden' name='action' value='update' />";

$linterface->LAYOUT_START();
?>
<?php echo $PageNavigation;?>
<form name="form1" id="form1" method="POST" action="?t=settings.assessmentStatReport.accessRightConfigAccessGroupUpdate" >
    <table class="form_table_v30">
    <col class="field_title">
    <col class="field_c">
    	<tr>
        	<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['SDAS']['Settings']['AccessGroup']['Name_ch']?></td>
        	<td>
        		<input type="text" name="name_ch" value="<?php echo $group['Name_ch']?>" required/>
        	</td>
        </tr>
        <tr>
        	<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['SDAS']['Settings']['AccessGroup']['Name_en']?></td>
        	<td>
        		<input type="text" name="name_en" value="<?php echo $group['Name_en']?>" required/>
        	</td>
        </tr>
        <tr>
        	<td class="field_title"><?php echo $Lang['SDAS']['Settings']['AccessGroup']['Description']?></td>
        	<td>
        		<textarea name="description" cols="70" rows="5"><?php echo $group['Description']?></textarea>
        	</td>
        </tr>
        <tr>
        	<td><?=$linterface->MandatoryField();?></td>
        </tr>
        <tr>
        	<td class="field_title"><?php echo $Lang['SDAS']['menu']['academic'];?></td>
        	<td><input type="checkbox" id="ckb_selectAll" /><?php echo $Lang['Btn']['SelectAll']?></td>
        </tr>
        <?php echo $checkboxTable;?>
        <?php echo $hiddenFields;?>
    </table>
    <div class="edit_bottom_v30">
    	<?php echo $btnSubmit;?>
    	&nbsp;
    	<?php echo $btnBack;?>
    </div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$('input.ckb_accessRight').change(function(){
			var alt = $(this).attr('alt');
			var value = $(this).attr('checked') == 'checked' ? 1 : 0;
			$('input#ap_' + alt).val(value);
		});

		$('#ckb_selectAll').change(function(){
			if($(this).attr('checked')){
				$('input.ckb_accessRight').attr('checked','checked');
				$('input.ap').val(1);
			} else {
				$('input.ckb_accessRight').removeAttr('checked');
				$('input.ap').val(0);
			}
		});
	});
</script>
<?php 
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>