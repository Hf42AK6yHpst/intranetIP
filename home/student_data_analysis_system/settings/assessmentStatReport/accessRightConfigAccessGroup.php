<?php
//using 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$obj = new libportfolio2007();

if (!$obj->IS_ADMIN_USER($UserID) && !$obj->IS_TEACHER() && $plugin['StudentDataAnalysisSystem_Style'] != "tungwah")
{
    header ("Location: /");
    intranet_closedb();
    exit();
}

$objSDAS = new libSDAS();

$CurrentPage = "settings.assessmentStatReport";
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['PIC'], "?t=settings.assessmentStatReport.accessRightConfigAdmin", 0);
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['SubjectPanel'] , "?t=settings.assessmentStatReport.accessRightConfigSubjectPanel", 0);
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessGroup']['Title'], "?t=settings.assessmentStatReport.accessRightConfigAccessGroup", 1);
if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    $TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['MonthlyReport']  , "?t=settings.assessmentStatReport.accessRightConfigMonthlyReport", 0);
    $TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['MonthlyReportSection'] , "?t=settings.assessmentStatReport.accessRightConfigMonthlyReportSection", 0);
}
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();


if($pageNo == "") $pageNo = 1;
if($field == "") $field = 1;
if($order == "") $order = 1;
$page_size = ($numPerPage == "")?50:$numPerPage;

$checkboxName = 'GroupID[]';
// Table sql
$cols = "CONCAT(
            '<a href=\"?t=settings.assessmentStatReport.accessRightConfigAccessGroupPanel&GroupID=', 
            aag.GroupID, 
            '\">',
            aag.Name_en,
            '</a>'
        ) As Name_ch,
        CONCAT(
            '<a href=\"?t=settings.assessmentStatReport.accessRightConfigAccessGroupPanel&GroupID=', 
            aag.GroupID, 
            '\">',
            aag.Name_ch,
            '</a>'
        ) As Name_en,
        aag.Description,
        CONCAT(
            '<a href=\"?t=settings.assessmentStatReport.accessRightConfigAccessGroupMember&GroupID=', 
            aag.GroupID, 
            '\">',
            COUNT( IF(aggm.ID IS NULL, NULL, 1)),
            '</a>'
        ) As members,
        CONCAT(
            '<input type=\"checkbox\" name=\"$checkboxName\" value=\"',
             aag.GroupID,
            '\" />'
        ) As checkbox
        ";
$table = "ASSESSMENT_ACCESS_GROUP AS aag";
$joinTable1 = "ASSESSMENT_ACCESS_GROUP_MEMBER AS aggm";
$joinCond1 = "aag.GroupID = aggm.GroupID";
$cond = "aag.isDeleted = '0'";
$group = "aag.GroupID";
$sql = "SELECT
            $cols
        FROM
            $table
        LEFT OUTER JOIN
            $joinTable1
            ON $joinCond1
        WHERE
            $cond
        GROUP BY
            $group
";
// debug_pr($sql);die();
// Table Sql
    
######################## Table Setting START ########################
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Name_en", "Name_ch", "Descripton", "members");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2; // +2 => Row number and checkbox
$li->title = "";
$li->IsColOff = 2;
######################## Table Setting END ########################

######################## Table Header START ########################
$li->column_list .= "<td width='1%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='30%'>" . $li->column(0, $Lang['SDAS']['Settings']['AccessGroup']['Name_en']) . "</td>\n";
$li->column_list .= "<td width='30%'>" . $li->column(0, $Lang['SDAS']['Settings']['AccessGroup']['Name_ch']) . "</td>\n";
$li->column_list .= "<td width='30%'>" . $li->column(0, $Lang['SDAS']['Settings']['AccessGroup']['Description']) . "</td>\n";
$li->column_list .= "<td width='8%'>" . $li->column(0, $Lang['SDAS']['Settings']['AccessGroup']['MemberCount']) . "</td>\n";
$li->column_list .= "<td width='1%'>" . $li->check($checkboxName) . "</td>\n";
######################## Table Header END ########################

######################## Main Toolbar START ########################
$toolbar = <<<TOOLBAR_HTML
	<div class="Conntent_tool">
		<a href="?t=settings.assessmentStatReport.accessRightConfigAccessGroupPanel" class="new" title="{$Lang['Btn']['New']}">{$Lang['Btn']['New']}</a>
	</div>
TOOLBAR_HTML;
######################## Main Toolbar END ########################

######################## Table Tools START ########################
$deleteTool = '<a class="tool_delete" onclick="formRemove()" href="javascript:checkRemove(document.form1,\'GroupID[]\',\'?t=settings.assessmentStatReport.accessRightConfigAccessGroupUpdate\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
$table_tool = <<<TOOLHTML
	<div nowrap="nowrap" class="common_table_tool">
		$deleteTool
	</div>
TOOLHTML;
######################## Table Tools END ########################

// Tool bar edit delete
// this page should show all group ( #, Name_ch, Name_en, Description, Member Count, checkbox)
// view.php memberList.php
// debug_pr($ReturnMsgKey);die();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$ReturnMsgKey]);
?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<form name="form1" id="form1" method="POST" action="?t=settings.assessmentStatReport.accessRightConfigAccessGroupUpdate">
	<input type="hidden" name="redirect" value="?t=settings.assessmentStatReport.accessRightConfigAccessGroup" />
	<div class ="content_top_tool">
		<?=$toolbar?>
		<br style="clear:both" />
	</div>
	<div class="table_board">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr class="table-action-bar">
				<td valign="bottom">
					<?=$table_tool?>
				</td>
			</tr>
		</table>
	<?php echo $li->display("100%"); ?>
	</div>
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
	<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
	<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
	<input type="hidden" name="page_size_change" value="">
	<input type="hidden" name="numPerPage" value="<?=$li->page_size; ?>">
	<input type="hidden" name="action" id="action" value="" />
</form>
<script type="text/javascript">
	function formRemove(){
		$('#action').val('delete');
	}
</script>
<?php 
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>