<?php 
/*
 * Change Log:
 * Date:		2017-07-11 Villa
 * 				-add task studentExamDetail
 */
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
$libpf_exam = new libpf_exam();
$lpf = new libportfolio();

// return html & script here
switch($_POST['task']){
	case 'trend':
		$AcademicYearArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
		$result = $libpf_exam->getDsePerformanceTrend($AcademicYearArr, $DSELevel, $Display, $SubjectIDList);
		break;
	
	case 'distribution':
		$AcademicYearArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
		$result = $libpf_exam->getDseDistribution($AcademicYearArr, $Display);
		break;
		
	case 'bestFive':
		$reportConfigArr = array('StartScore' => 10 , 'Interval' => 3);
		$AcademicYearArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
		$result = $libpf_exam->getDseBestFiveScoreStat($reportConfigArr, $AcademicYearArr, $Display);
		break;
		
	case 'university':
		$AcademicYearArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
		$result = $libpf_exam->getUniversityReqirementStat( $AcademicYearArr, $Display);
		break;
		
	case 'bestStudent':
		$reportConfigArr = array();
		$reportConfigArr['TopStudentScore'] = $topStudentConfig;
		$reportConfigArr['NumSubject'] = $subjectNumConfig;
		$reportConfigArr['scoreConfig'] = $scoreConfig;
		$result = $libpf_exam->getBestStudent($FromAcademicYearID ,$reportConfigArr );
		break;
		
	case 'attendance':
		$AcademicYearArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
		$result = $libpf_exam->getDseAttendace($AcademicYearArr );
		break;
	
	case 'studentExamDetail':
		$result = $libpf_exam->getStudentExamDetail($studentID,$academicYearID);
		break;
}

?>
<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};
</script>
<?php
function ColorGradient($ColorSteps) {
	switch($ColorSteps){
		case '9':
			$returnString = '"#F8696B", "#F98971", "#FBAA77", "#FDCA7D", "#FFEB84", "#D6D394", "#ABB9A3","#84A3B5", "#5A8AC6"';
			break;
		case '8':
			$returnString = '"#F8696B", "#F98971", "#FBAA77", "#FDCA7D", "#FFEB84", "#D6D394","#84A3B5", "#5A8AC6"';
			break;
		case '7':
			$returnString = '"#F8696B", "#F98971", "#FDCA7D", "#FFEB84", "#D6D394","#84A3B5", "#5A8AC6"';
			break;
		case '6':
			$returnString = '"#F8696B", "#FDCA7D", "#FFEB84", "#D6D394","#84A3B5", "#5A8AC6"';
			break;
		case '5':
			$returnString = '"#F8696B", "#FDCA7D", "#FFEB84", "#ABB9A3", "#5A8AC6"';
			break;
		case '4':
			$returnString = '"#F8696B", "#FDCA7D", "#FFEB84", "#5A8AC6"';
			break;
		case '3':
			$returnString = '"#F8696B", "#FFEB84", "#5A8AC6"';
			break;
		default:
			$returnString = '"#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee", "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"';
			break;
	}
	return $returnString;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// switch of report [Start] ////////////////////////////////////////////
// set common theme here
switch($_POST['task']){
	case 'trend':
	case 'university':
	case 'bestFive':
		$numDseLv = count($DSELevel);
		$colorString = ColorGradient($numDseLv); 
?>
<script>
/**
 * Grid-light theme for Highcharts JS
 * @author Torstein Honsi
 */

// Load the fonts
Highcharts.createElement('link', {
   href: 'https://fonts.googleapis.com/css?family=Dosis:400,600',
   rel: 'stylesheet',
   type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);

Highcharts.theme = {
//    colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
//       "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
colors: [<?php echo $colorString?>],
   chart: {
      backgroundColor: null,
      style: {
         fontFamily: "Dosis, sans-serif"
      }
   },
   title: {
      style: {
         fontSize: '16px',
         fontWeight: 'bold',
         textTransform: 'uppercase'
      }
   },
   tooltip: {
      borderWidth: 0,
      backgroundColor: 'rgba(219,219,216,0.8)',
      shadow: false
   },
   legend: {
      itemStyle: {
         fontWeight: 'bold',
         fontSize: '13px'
      }
   },
   xAxis: {
      gridLineWidth: 1,
      labels: {
         style: {
            fontSize: '12px'
         }
      }
   },
   yAxis: {
      minorTickInterval: 'auto',
      title: {
         style: {
            textTransform: 'uppercase'
         }
      },
      labels: {
         style: {
            fontSize: '12px'
         }
      }
   },
   plotOptions: {
      candlestick: {
         lineColor: '#404048'
      }
   },
   // General
   background2: '#F0F0EA'

	

};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);
//hide the download function
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});

</script>
<?php
		break;
	default:
		break;
}
//////////////////////////////////////////// switch of report [End] ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo $result;
exit;
?>