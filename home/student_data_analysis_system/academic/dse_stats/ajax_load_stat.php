<?
################# Change Log [Start] #####
# 2016-09-28 Omas
# - fixed to show individual student score table ($barCharTable)
#
# 2016-07-18 Omas
# - fixed cannot show boxplot , if some level don't have any data
#
#  2016-06-06 Cara
#  - Change data table's CSS
#
#  2016-06-06 Anna
#  add new array to have box plot's data and number of class student  
#
#	2016-05-12 Omas
#	- fixed can't get fullmark problem - Z95819 
#
#	2016-03-21 Omas
#	- Added passing fullmark into json 
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
$libpf_exam = new libpf_exam();
$lpf = new libportfolio();
$json = new JSON_obj();

//debug_pr($_POST);
if($action == 'DSE_analysis'){
	
	$AcademicYearArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
	
	$userIDArr = $libpf_exam->getStudentIDByClassNameAndYear($AcademicYearArr,$Class);
//	$userIDArr = array('26469', '26470', '26471', '26472');
	
	// DSE
	//$DSEResult = BuildMultiKeyAssoc($libpf_exam->getExamData(20 , 1 , 14),'StudentID',array('Score'),1 );
	$DSETemp = $libpf_exam->getExamData($AcademicYearArr , EXAMID_HKDSE , $FromSubjectID,$userIDArr);
	$DSEResult = BuildMultiKeyAssoc($DSETemp,array('AcademicYearID','StudentID'),array('Score'),1);
	if(!empty($DSEResult)){
			
		// Compare Target
		if($report_type=='int'){
			foreach((array)$DSETemp as $_infoArr){
				$_StudentID = $_infoArr['StudentID'];
				$_AcademicYearID = $_infoArr['AcademicYearID'];
				$StudentDSEAcademicYearArr[$_StudentID] = $_AcademicYearID; 				
			}
			$TempResult = $libpf_exam->getStudentSchoolExamResult($targetForm,$userIDArr,$FromSubjectID,$YearTermID);
			$MockTempResult = BuildMultiKeyAssoc($TempResult,array('StudentID'),array('Score'),1);
// 			debug_pr($TempResult);
			$MockResult = array();
			foreach($MockTempResult as $_sid => $_Score){
				$_DESacademicYearID = $StudentDSEAcademicYearArr[$_sid];
				$MockResult[$_DESacademicYearID][$_sid] = $_Score;
			}
		}
		else if($report_type=='mock'){
			//$MockResult = BuildMultiKeyAssoc($libpf_exam->getExamData(20 , 2 , 14),'StudentID',array('Score'),1 );
			$TempResult = $libpf_exam->getExamData($AcademicYearArr , EXAMID_MOCK , $FromSubjectID,$userIDArr);
			$MockResult = BuildMultiKeyAssoc($TempResult,array('AcademicYearID','StudentID'),array('Score'),1);
		}
		//$MockResult = array (  [k:Year] => Array( [k:User] => [v:UserScore] ))
// 		debug_pr($StudentDSEAcademicYearArr);
// 		debug_pr($MockResult);
// 		debug_pr($DSEResult);
		if(!empty($MockResult)){
			$DSE_ExamArr = array();
			$validCount = 0;
			$missMockUserID = array();
			foreach((array)$AcademicYearArr as $_AcademicYearID){
				foreach((array)$DSEResult[$_AcademicYearID] as $__studentID => $__DSEScore){
					if($__DSEScore=='5**'){
						$level = 7;
					}
					else if($__DSEScore=='5*'){
						$level = 6;
					}
					else if($__DSEScore == 'U' || $__DSEScore == 'X'){
						continue;
					}
					else{
						$level = $__DSEScore;						
					}
					
					$__MockScore = $MockResult[$_AcademicYearID][$__studentID];
					if($__MockScore!=''){
						$DSE_ExamArr[$level][] = $__MockScore;
						$validCount ++;
					}
					else{
						// for debug only
						//$missMockUserID[] = $__studentID;
					}
				}
			}
		}
		else{
// debug only
//			echo 'Missing exam data';
		}
	}
	else{
// debug only
//		echo 'Missing DSE data';
	}
	
	# ramdom data for testing
// 	for($x=0;$x<11;$x++)
// 	$DSE_ExamArr['7'][] = rand(80,100);
// 	for($x=0;$x<15;$x++)
// 	$DSE_ExamArr['6'][] = rand(65,90);
// 	for($x=0;$x<21;$x++)
// 	$DSE_ExamArr['5'][] = rand(50,80);
// 	for($x=0;$x<31;$x++)
// 	$DSE_ExamArr['4'][] = rand(50,70);
// 	for($x=0;$x<21;$x++)
// 	$DSE_ExamArr['3'][] = rand(40,60);
// 	for($x=0;$x<15;$x++)
// 	$DSE_ExamArr['2'][] = rand(20,50);
// 	for($x=0;$x<11;$x++)
// 	$DSE_ExamArr['1'][] = rand(0,30);
	
	// fix to $barCharTable
	$sortedScoreArr = array();
	$statsArr = array();
	$maxCount = 0;
	foreach((array)$DSE_ExamArr as $_level => $_scoreArr){
		$_tempScoreArr = $_scoreArr;
		sort($_tempScoreArr);
		$sortedScoreArr[$_level] = $_tempScoreArr;
		$numStudent = count($_tempScoreArr);
		if($numStudent> $maxCount){
			$maxCount = $numStudent;
		}
	}

	// for highchart
	$statsArr = array();
	$newArray = array();
	$numberArray = array();
// 	foreach((array)$DSE_ExamArr as $_level => $_scoreArr){
	for($x=1; $x<=7;$x++){
		$_level = $x;
		$_scoreArr = $DSE_ExamArr[$_level];
		if(empty($_scoreArr)){
			$newArray[$_level-1] = array(0,0,0,0,0);
			$numberArray[$_level] = 0;
			
			$statsArr[$_level]['Label'] = $_level;
			$statsArr[$_level]['Q1'] 	= 0;
			$statsArr[$_level]['Median']= 0;
			$statsArr[$_level]['Q3'] 	= 0;
			$statsArr[$_level]['IQR']	= 0;
			$statsArr[$_level]['Max'] 	= 0;
			$statsArr[$_level]['Min'] 	= 0;
			$statsArr[$_level]['Upper Outliers'] = 0 ;
			$statsArr[$_level]['Lower Outliers'] = 0 ;
			$statsArr[$_level]['Num'] 	= 0;
		}
		else{
			$_stats = $libpf_exam->getStatisData($_scoreArr);
			$statsArr[$_level]['Label'] = $_level;
			$statsArr[$_level]['Q1'] 	= $_stats['Q1'];
			$statsArr[$_level]['Median']= $_stats['Median'];
			$statsArr[$_level]['Q3'] 	= $_stats['Q3'];
			$statsArr[$_level]['IQR']	= $_stats['IQR'];
			$statsArr[$_level]['Max'] 	= $_stats['Max'];
			$statsArr[$_level]['Min'] 	= $_stats['Min'];
			$statsArr[$_level]['Upper Outliers'] = ($statsArr[$_level]['Max'] > $_stats['Upper Outliers'])?$_stats['Upper Outliers'] :0 ;
			$statsArr[$_level]['Lower Outliers'] = ($statsArr[$_level]['Min'] < $_stats['Lower Outliers'])?$_stats['Lower Outliers'] :0 ;
			$statsArr[$_level]['Num'] 	= $_stats['Num'];
			
			// for graph
			$newArray[$_level-1][] = (float)$_stats['Min'];
	  		$newArray[$_level-1][] = (float)$_stats['Q1'];
	  		$newArray[$_level-1][] = (float)$_stats['Median'];
	   		$newArray[$_level-1][] = (float)$_stats['Q3'];
	//  		$newArray[$i][] = $_stats['IQR'];
	  		$newArray[$_level-1][] = (float)$_stats['Max'];
	  	
	  		$numberArray[$_level] = $_stats['Num'];
		}
	}
	
	ksort($numberArray);
	$newNumber =array();
	foreach($numberArray as $_value){
		$newNumber[] = $_value;
	}
	$numberArray = $newNumber;
	ksort($newArray);

// 	debug_pr($statsArr);
// 	debug_pr($newArray);
//	debug_pr($numberArray);

	$numLevel = count($statsArr);
	if($numLevel>0){
		$dataTableWidth1 = (100-12)/$numLevel;
		$dataTableWidth2 = 100/$numLevel;
	}
// 	$data_squence = array('Label','Min','Q1','Median','Q3','Max','IQR','Upper Outliers','Lower Outliers','Num');
	$data_squence = array('Label','Min','Q1','Median','Q3','Max','Num');
	$boxplotTable = '<div class="chart_tables"><table class="common_table_list_v30 view_table_list_v30">';
	foreach((array)$data_squence as $_key=>$_fieldName){
		if($_key==0){
			$boxplotTable .= '<thead>';
		}elseif ($_key==1){
			$boxplotTable .= '</thead>';
			$boxplotTable .= '<tbody>';
		}
			
		for($z = 0; $z<8; $z++){
			
			if($z==0){
				$__style = '';
				$__content = $_fieldName;
			}
			else{
				if($_fieldName == 'Label'){
					$__content = 'Level '.$libpf_exam->getDSELevel($statsArr[$z][$_fieldName]);
				}
				else{
					$__content = round($statsArr[$z][$_fieldName]);
				}
			}
			
			if($z!=0){
				$__type = ($_fieldName == 'Label') ? 'th width="'.$dataTableWidth1.'" style="text-align:center;"':'td';
			}
			else{
				$__type = ($_fieldName == 'Label') ? 'th width="12%" style="text-align:center;"':'td style="font-weight:bold;"';				
			}
			
			if( $z==0 || !empty($statsArr[$z])){
				$boxplotTable .= '<'.$__type.'>'.$__content.'</'.$__type.'>';
			}
		}
		$boxplotTable .= '</tr>';
		if ($_key==6){
			$boxplotTable .= '</tbody>';}
	}
	$boxplotTable .= '<table></div>';
	$boxplotTable .= '<div>'.$Lang['SDAS']['DSEstat']['boxplot_remarks'].'</div>';

	$barCharTable = '<div class="chart_tables"><table class="common_table_list_v30 view_table_list_v30" style="text-align:center;">';
	$barCharTable .= '<thead><tr>';
	for($lv = 1; $lv<8; $lv++){
// 		if(!empty($sortedScoreArr[$lv])){
			$barCharTable .= '<th width="'.$dataTableWidth2.'" style="text-align:center;">Level '.$libpf_exam->getDSELevel($lv).'</th>';
// 		}
	}
	$barCharTable .= '</thead></tr>';
	for($y=0;$y<$maxCount;$y++){
		$barCharTable .= '<tr>';
		for($__lv=1;$__lv<8;$__lv++){
			$__score = $sortedScoreArr[$__lv][$y];
// 			if(!empty($sortedScoreArr[$__lv])){
			$barCharTable .= '<td style="font-weight:normal; text-align:center;">'.Get_String_Display($__score).'</td>';
// 			}
		}
		$barCharTable .= '</tr>';
	}
	$barCharTable .= '</table></div>';
	
	if(empty($DSE_ExamArr)){
		echo 'fail';
	}
	else{
		// set box-plot max
		$sql = "SELECT DISTINCT yr.YearID, FullMarkInt, PassMarkInt, SubjectID FROM {$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK as asf
				INNER JOIN YEAR as yr on yr.YearID = asf.YearID
				WHERE AcademicYearID IN ('".implode("','",$AcademicYearArr)."') AND SubjectID = '{$FromSubjectID}' AND yr.WEBSAMSCode like '%6%'
				ORDER BY FullMarkInt desc";
		
		$SubectFullMarkArr = Get_Array_By_Key($libpf_exam->returnResultSet($sql), 'FullMarkInt' );
		$max = (!empty($SubectFullMarkArr))? max($SubectFullMarkArr): 100;
		$max = ($max > 0 || $max < 100) ? $max : 100;
		
		
		// prevent empty arr
		for($x = 1 ; $x<8; $x++){
			if(empty($DSE_ExamArr[$x])){
				$DSE_ExamArr[$x] = array();
			}
		}
		
		$json_arr['data'] = $DSE_ExamArr;
		$json_arr['configMax'] = $max;
//		debug_pr($DSE_ExamArr);

// 		$data_graphName = $json->encode($json_arr);

		$data_json = $json->encode($newArray);
		$numberArray = $json->encode($numberArray);
		
		$separator = '<!--//js separator//-->';
		$returnString =$data_json.$separator.$boxplotTable.$separator.$barCharTable.$separator.$numberArray;
		
		echo $returnString;
	}
	
}
?>
