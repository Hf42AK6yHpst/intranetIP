<?php 
/**
 * Change Log:
 * 2017-11-17 Pun
 *  - File Create
 */
// ####### Init START ########
if(!$sys_custom['SDAS']['dse_vs_gpa']){
	No_Access_Right_Pop_Up('','/');
	die();
}
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
$libpf_exam = new libpf_exam ();
$libSDAS = new libSDAS ();
$lpf = new libPortfolio ();
$libFCM_ui = new form_class_manage_ui ();
$libSCM_ui = new subject_class_mapping_ui ();
// ####### Init END ########

// ####### Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight ();
if(!$accessRight ['admin']){
	No_Access_Right_Pop_Up('','/');
	die();
}
// ####### Access Right END ########

// ####### Page Setting START ########
$CurrentPage = "dse_stats";
$CurrentPageName = $Lang ['SDAS'] ['menu'] ['DseAnalysis'];
$CurrentTag = 'dse_vs_gpa';
include dirname(__FILE__).'/menu_tags.php';
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR ();
// ####### Page Setting END ########

// ####### UI Releated START ########
 $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
// year info

$aYearAssocArr = BuildMultiKeyAssoc ( GetAllAcademicYearInfo (), 'AcademicYearID', array (
		Get_Lang_Selection (  'YearNameB5', 'YearNameEN') 
), 1 );
$yearArr = $libpf_exam->getDseYearWithRecords ();

$yearSelectArr = array ();
foreach ( ( array ) $yearArr as $yearID ) {
	$yearSelectArr [$yearID] = $aYearAssocArr [$yearID];
}
$FromAcademicYearSelection = getSelectByAssoArray ( $yearSelectArr, "name='AcademicYearID' id='AcademicYearID'", $CurrentAcademicYearID, 0, 0, "" );
$FromYearSelection = $libFCM_ui->Get_Form_Selection('YearID', $SelectedYearID='', $Onchange='', $noFirst=1, $isAll=0, $isMultiple=0);

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
// ####### UI Releated END ########

// ####### UI START ########
$linterface->LAYOUT_START ();
?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<?php echo $linterface->Include_HighChart_JS()?>
<?php echo $linterface->Include_Thickbox_JS_CSS()?>

<script>
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
$(document).ready(function() {

	$('#AcademicYearID, #YearID').change(function(){
		if($('#AcademicYearID').val() == ''){
    		$('#yearClassSelectDiv').html('');
    		return;
		}
		
		$('#yearClassSelectDiv').html(ajaxImage);
		$.get(
			'index.php?t=academic.dse_stats.ajax_get_class_selection', 
			$('#form1').serialize(),
			function(res){
				$('#yearClassSelectDiv').html(res);
			}
		);
	}).change();
	
	$( "#Btn_View" ).click(function() {
		$( "#PrintBtn" ).hide();
		
		if( $('select#AcademicYearID').val() == '' ){
			alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>');
			return false;
		}
		
		$("#DBTableDiv").html(ajaxImage);
		var url = "index.php?t=academic.dse_stats.ajax_get_dse_vs_gpa";
		$.ajax({
		    type:"POST",
		    url: url,
		    data: $("#form1").serialize(),
		    success:function(responseText)
		    {
		    	$("#DBTableDiv").html(responseText);
		    	$("#PrintBtn").show();
		    }
		});  
	});
});
</script>
<form id="form1" name="form1">
	<?=$html_tab_menu?>
	<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top"><?=$FromAcademicYearSelection?></td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
			<td valign="top"><?=$FromYearSelection?></td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
			<td valign="top"><div id="yearClassSelectDiv"></div></td>
		</tr>
	</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>

	<div class="edit_bottom_v30">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>"
			class='formbutton' onmouseover="this.className='formbuttonon'"
			onmouseout="this.className='formbutton'" />
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>	
	</div>
	<br clear=“both”/>
	
	<div id="PrintArea">
		<div id="DBTableDiv"></div>
	</div>
</form>

<?php
$linterface->LAYOUT_STOP ();
intranet_closedb ();