<?php
/*
################### Change Log [Start] ###########
## Date: 2019-01-14 Isaac
## - Added export table function
##
## Date: 2017-09-01 Omas
## - add $sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject']
## Date: 2016-12-15 Omas
## - add a param for getClassListByYearName() to ensure only secondary class is included
##
## Date: 2016-08-09 Omas
## - added tab for new reports
##
## Date: 2016-07-18 Omas
## - fixed cannot show boxplot , if some level don't have any data
## - comment out un-used d3 related js code
##
## Date: 2016-06-23 Cara
## - changed default value of $FromAcademicYearSelection, $ToAcademicYearSelection and Class CheckBox
##
## Date: 2016-06-06 Anna
##-change box plot and bar chart view, add css for table
##
## Date : 2016-03-21 Omas
## - Added support to dynamic fullmark into json
##
## Date : 2016-02-29 Omas
## improved: only show academic year with dse record
##
#################### Change Log [End] ############
*/

// ####### Init START ########
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
$libpf_exam = new libpf_exam ();
$libSDAS = new libSDAS ();
$lpf = new libPortfolio ();
$libFCM_ui = new form_class_manage_ui ();
$libSCM_ui = new subject_class_mapping_ui ();
// ####### Init END ########

// ####### Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight ();
$subjectIdFilterArr = '';
if ($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject'] && ! $accessRight ['admin'] && count ( $accessRight ['subjectPanel'] )) {
	$subjectIdFilterArr = array_keys ( $accessRight ['subjectPanel'] );
}
// ####### Access Right END ########

// ####### Page Setting START ########
$CurrentPage = "dse_stats";
$CurrentPageName = $Lang ['SDAS'] ['menu'] ['DseAnalysis'];
$CurrentTag = 'dse_vs_mock';
include dirname(__FILE__).'/menu_tags.php';
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR ();
// ####### Page Setting END ########

// ####### UI Releated START ########
 $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
// year info

$aYearAssocArr = BuildMultiKeyAssoc ( GetAllAcademicYearInfo (), 'AcademicYearID', array (
		Get_Lang_Selection (  'YearNameB5', 'YearNameEN') 
), 1 );
$yearArr = $libpf_exam->getDseYearWithRecords ();

$yearSelectArr = array ();
foreach ( ( array ) $yearArr as $yearID ) {
	$yearSelectArr [$yearID] = $aYearAssocArr [$yearID];
}
$FromAcademicYearSelection = getSelectByAssoArray ( $yearSelectArr, "name='FromAcademicYearID' id='FromAcademicYearID'", $CurrentAcademicYearID, 0, 0, "" );
$ToAcademicYearSelection = getSelectByAssoArray ( $yearSelectArr, "name='ToAcademicYearID' id='ToAcademicYearID'", $CurrentAcademicYearID, 0, 0, "" );
// $FromAcademicYearSelection = getSelectByArray($YearArr, "name='FromAcademicYearID[]' id='FromAcademicYearID' multiple", $classSelect, 0, 1, "", 2);
// $FromAcademicYearSelection = getSelectAcademicYear('FromAcademicYearID', $tag='', $noFirst=0, $noPastYear=0);
// $ToAcademicYearSelection = getSelectAcademicYear('ToAcademicYearID', $tag='', $noFirst=0, $noPastYear=0);
$FromSubjectSelection = $libSCM_ui->Get_Subject_Selection ( 'FromSubjectID', $FromSubjectID, $OnChange = '', $noFirst = 1, '', '', $OnFocus = '', $FilterSubjectWithoutSG = 0, $IsMultiple = 0, $IncludeSubjectIDArr = $subjectIdFilterArr );

$targetFormSelection = getSelectByAssoArray ( $Lang ['SDAS'] ['DSEstat'] ['form'], "name='targetForm' id='targetForm'", '6', 0, 1 );

// $class_arr = $libpf_exam->getClassListByYearName ( 6 , true);

// $classCheckBox = '';
// foreach ( ( array ) $class_arr as $_class_info ) {
// 	// $_value = $_class_info['YearClassID'];
// 	$_value = $_class_info ['ClassTitleEN'];
// 	$_displayLang = Get_Lang_Selection ( $_class_info ['ClassTitleB5'], $_class_info ['ClassTitleEN'] );
// 	$_engDisplay = $_class_info ['ClassTitleEN'];
// 	$classCheckBox .= '<input type="checkbox" class="ClassSel" id="class_' . $_engDisplay . '" name="Class[]" value="' . $_value . '" /><label for="class_' . $_engDisplay . '">' . $_displayLang . '</label>&nbsp;';
// }

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

// ####### UI Releated END ########

// ####### UI START ########
$linterface->LAYOUT_START ();
echo $linterface->Include_TableExport();
?>

<!-- style>
body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

.box {
	font: 10px sans-serif;
}

.box line, .box rect, .box circle {
	fill: #487db4;
	stroke: #000;
	stroke-width: 1px;
}

.box .center {
	stroke-dasharray: 3, 3;
}

.bar, .box .outlier {
	fill: none;
	stroke: #000;
}

.axis {
	font: 12px sans-serif;
}

.axis path, .axis line {
	fill: none;
	stroke: #000;
	shape-rendering: crispEdges;
}

.x.axis path {
	fill: none;
	stroke: #000;
	shape-rendering: crispEdges;
}
</style-->

<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<script language="JavaScript" src="/templates/d3/d3_v3_5_16.min.js"></script>
<script language="JavaScript" src="academic/dse_stats/box.js"></script>
<?php echo $linterface->Include_HighChart_JS()?>
<!-- 
 -->

<script>

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {
	// for csv display multiple headers
	$("tr.display_table_header").hide();
	$("tr.export_table_header").show();
	
	$(".chart_tables table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'DSE vs mock'
	});

	$("tr.display_table_header").show();
	$("tr.export_table_header").hide();
}

var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
var readyToSubmit = false;
$(document).ready( function() {
	get_class_opt();

	$('#FromAcademicYearID').change(function(){
		get_class_opt();
    });
	$('#ToAcademicYearID').change(function(){
		get_class_opt();
    });    

	var url = '/home/portfolio/ajax/ajax_get_yearterm_opt.php';
	$('#termSel').html(ajaxImage);
	$.ajax({
           type:"POST",
           url: url,
           async: false,
           data: { "ay_id" : <?=Get_Current_Academic_Year_ID() ?>},
           success:function(responseText)
           {
				$('#termSel').html(responseText);
				$("select[name=YearTermID] option[value='']").text("<?=$ec_iPortfolio['overall_result']?>");
           }
    });
    $('#targetForm').attr('disabled','disabled');
	$("select[name=YearTermID]").attr('disabled','disabled');
	readyToSubmit = true;
	$("#resultType_SelectAll").attr("checked", "checked");
	$(".ClassSel").attr("checked", "checked");
});

function get_class_opt(){
	  var ay_id = $("#FromAcademicYearID").val();
	  var ay_to_id = $("#ToAcademicYearID").val();
	  $("#Class").html(ajaxImage);
	  	$.ajax({
	  		type: "GET",
	  		url: "index.php?t=ajax.ajax_get_class_opt_by_academicyear",
	  		data: "ay_id="+ay_id,
	  		success: function (msg) {
	        $("#Class").html(msg);
	  		}
	  	});  
	  	selectAll();
}

function selectAll(){
	if($("#resultType_SelectAll").attr("checked")){
		$(".ClassSel").attr("checked", "checked");
	}else{
		$(".ClassSel").removeAttr("checked");
	}
}

function unChecked(){
	if(!$("this").attr("checked")){
		$("#resultType_SelectAll").removeAttr("checked");
	}
	
}


var plainDataMode = false; 
function jsLoadGraph()
{
	if(readyToSubmit){
		if( $('select#FromAcademicYearID').val() == '' || $('select#TomAcademicYearID').val() == ''  ){
			alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>');
			return false;
		}
		if($('input[name="Class\[\]"]:checked').length == 0){
			alert('<?=$Lang['General']['JS_warning']['SelectClasses']?>');
			return false;
		}
		
		readyToSubmit = false;
		$('#DBTableDiv').html(ajaxImage);
		$('div#BoxPlot').html('');
		$('div#BarChart').html('');
		$('div#DataTableBox').html('');
		$('div#DataTableBar').html('');
		var url = "index.php?t=academic.dse_stats.ajax_load_stat";
		$.ajax({
	           type:"POST",
	           url: url,
	           async: false,
	           data: $("#form1").serialize(),
	           success:function(responseText)
	           {
//		           console.log(responseText);
		         		$('div#DBTableDiv').html(responseText);
 					if(plainDataMode){
 					}
 					else{
 						$('div#DBTableDiv').html('');
 						var separator = '<!--//js separator//-->';
 						var returnArr = responseText.split(separator);
 		           		if(responseText != 'fail'){
							
 							var json = returnArr[0];
//							console.log(json);
 							obj = JSON.parse(json);
// 							graphName = JSON.parse(json);
//						    console.log(level.data);
							number = JSON.parse(returnArr[3]);
 							js_generate_report(obj,number);

 							
//  						jsGetBoxplotName(graphName);
//						  console.log(number)   

							$( "#PrintBtn" ).show();
							$("#ExportBtn").show();
				
 							$('div#DataTableBox').html(returnArr[1]);
 							$('div#DataTableBar').html(returnArr[2]);
 							
 		           		}
 		           		else{
		           			$('div#DBTableDiv').html('<?=$Lang['SDAS']['DSEstat']['Alert']['NoData']?>');
 		           		}
 					}
	           }
	    });  
	    readyToSubmit = true; 
		
	}
}	
	
// function js_initilize_data(data){
// 	var data = [];
// 	data[0] = [];
// 	data[1] = [];
// 	data[2] = [];
// 	data[3] = [];
// 	data[4] = [];
// 	data[5] = [];
// 	data[6] = [];
// 	// add more rows if your csv file has more columns
	
// 	// add here the header of the csv file
// 	data[0][0] = "Level 1";
// 	data[1][0] = "Level 2";
// 	data[2][0] = "Level 3";
// 	data[3][0] = "Level 4";
// 	data[4][0] = "Level 5";
// 	data[5][0] = "Level 5*";
// 	data[6][0] = "Level 5**";

// 	// add more rows if your csv file has more columns
// 	data[0][1] = [];
// 	data[1][1] = [];
// 	data[2][1] = [];
// 	data[3][1] = [];
// 	data[4][1] = [];
// 	data[5][1] = [];
// 	data[6][1] = [];
	
// 	return data
// //  	var level = [];
// //  	for(i=0;i<data.length;i++){
// //  		level[i] = {};
// //  		level[i] = data[i][0];
// //  		}
// //  	return level;

 
// }

//Array Remove - By John Resig (MIT Licensed)
// Array.prototype.remove = function(from, to) {
//   var rest = this.slice((to || from) + 1 || this.length);
//   this.length = from < 0 ? this.length + from : from;
//   return this.push.apply(this, rest);
// };


// function js_prepare_data(obj){
	
// 	var data = js_initilize_data();
// 	arr = Object.keys(obj).map(function (key) {return obj[key]});

// 	arr.forEach(function(arr2,key) {
// 		arr2.forEach(function(value){
// 			data[key][1].push(value) ;
// 		});
// 	});

// 	// if that level do not have any data hide it by removing the array
// 	var removeArr = [];
// 	data.forEach(function(arr3,key){
// 		if(arr3[1].length==0){
// 			removeArr.push(key);
// 		}
// 	});
// 		var numEmptyLevel = removeArr.length;
// 	for(x = numEmptyLevel-1; x >= 0; x--){
// 		var key = removeArr[x];
// 		data.remove(key);
// 	}
	
// 	return data;
// }


function js_generate_report(obj,number){
// 	var data = js_prepare_data(obj);
// 	js_prepare_graph(data);
	
// 	js_initilize_data(data);

//console.log(number);


//   	var summary = [];
//   	for (i=0; i<obj.length; i++){
//   	 	summary[i] = {};
//   	 	summary[i] = obj[i];
		
//   	  	for (j=0; j<obj[i].length; j++){
//   	  		summary[i][j] = obj[i][j];
//   	  	}
	 	
//   	  	summry[i].data =obj[i].data;
//   	 }
//  	 console.log(summary);

	 Highcharts.setOptions({
    	lang: {
    		downloadSVG: "",
    		downloadPDF: "",
    		printChart: ""
    	}
    });  

    $('#BoxPlot').highcharts({

        chart: {
            type: 'boxplot',
            height: '460'
        },

        title: {
            text: '<?=$Lang['SDAS']['DSEstat']['box1']?>'
        },

        legend: {
            enabled: false
        },

        xAxis: {
           categories:  ["Level 1","Level 2","Level 3","Level 4","Level 5","Level 5*","Level 5**"],
      //categories:  graphName,
         },

        yAxis: {
            min:0,
            title: {
                text: '<?=$Lang['SDAS']['DSEstat']['box_y'] ?>'
            },
//             plotLines: [{
//                 value: 932,
//                 color: 'red',
//                 width: 1,
//                 label: {
//                     text: 'Theoretical mean: 932',
//                     align: 'center',
//                     style: {
//                         color: 'gray'
//                     }
//                 }
//             }]
        },

        series: [{
             name: 'Observations',
             data: obj,

             tooltip: {
                 headerFormat: '<em> {point.key}</em><br/>'
             }
         }, 
         {
             name: 'Outlier',
            color: Highcharts.getOptions().colors[0],
             type: 'scatter',
             data: [ // x, y positions where 0 is the first category
//                  [0, 644],
//                  [4, 718],
//                  [4, 951],
//                  [4, 969]
             ],
             marker: {
                 fillColor: 'white',
                 lineWidth: 1,
                 lineColor: Highcharts.getOptions().colors[0]
             },
             tooltip: {
                 pointFormat: 'Observation: {point.y}'
             }
        }],
        credits : {enabled: false,},

    });


    
    $('#BarChart').highcharts({
        chart: {
            type: 'column',
            height: '460'
        },
        title: {
            text:  '<?=$Lang['SDAS']['DSEstat']['barChartTitle']?>'
        },
        xAxis: {
            
         categories:["Level 1","Level 2","Level 3","Level 4","Level 5","Level 5*","Level 5**"],
         crosshair: true
    },
        yAxis: {
            min: 0,
            title: {
                text: '<?=$Lang['SDAS']['DSEstat']['bar_y'] ?>'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:[ {
            name: '<?=$Lang['SDAS']['DSEstat']['bar_y'] ?>' ,
            data:number
        }],
        credits : {enabled: false,},
//             [ 
//                   {
//     	     name: xAxis,
//              data: summary
//          }, 

//         ]
    }); 
    

}

function jsGetBoxplotName(graphName){
	
	if($('input[name=report_type]:checked').val() == 'int'){
		var form = '';
		if($('#targetForm').val()==4){
			form = '<?=$Lang['SDAS']['DSEstat']['form']['4']?>';
		} 
		else if($('#targetForm').val()==5){
			form = '<?=$Lang['SDAS']['DSEstat']['form']['5']?>';
		}
		else if($('#targetForm').val()==6){
			form = '<?=$Lang['SDAS']['DSEstat']['form']['6']?>';
		}
		graphName = '<?=$Lang['SDAS']['DSEstat']['box2']?>';
		graphName = graphName.replace(/"FormName"/, form)
	}
	else{
		graphName = '<?=$Lang['SDAS']['DSEstat']['box1']?>';
	}

	return graphName;
}

// function js_prepare_graph(data){

// 	var boxplotName = jsGetBoxplotName();
	var barChartName = '<?=$Lang['SDAS']['DSEstat']['barChartTitle']?>';

	
// 	var labels = true; // show the text labels beside individual boxplots?
// 	var masterWidth = $(document).width()-210;

// 	var yAxisTextXposition = -80;
// 	var yAxisTextYposition = -45;  
// 	var margin = {top: 30, right: 50, bottom: 70, left: 70};
// 	var  width = masterWidth - margin.left - margin.right;
// 	var height = 600 - margin.top - margin.bottom;
	
// 	var min = 0,
// 	max = obj.configMax > 0 ? obj.configMax: 100;
	
// 	var chart = d3.box()
// 	.whiskers(iqr(1.5))
// 	.height(height)	
// 	.domain([min, max])
// 	.showLabels(labels);
	
// 	var svg = d3.select("div#BoxPlot").append("svg")
// 	.attr("width", width + margin.left + margin.right)
// 	.attr("height", height + margin.top + margin.bottom)
// 	.attr("class", "box")    
// 	.append("g")
// 	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
// 	// the x-axis
// 	var x = d3.scale.ordinal()	   
// 	.domain( data.map(function(d) { 
// 		//console.log(d); 
// 		return d[0] } ) )	    
// 	.rangeRoundBands([0 , width], 0.7, 0.3); 		
// 			var xAxis = d3.svg.axis()
// 	.scale(x)
// 	.orient("bottom");
	
// 	// the y-axis
// 	var y = d3.scale.linear()
// 	.domain([min, max])
// 	.range([height + margin.top, 0 + margin.top]);
	
// 	var yAxis = d3.svg.axis()
// 	.scale(y)
// 	.orient("left");

	// draw the boxplots	


	
	
// 	svg.selectAll(".box")	   
// 	.data(data)
// 	.enter().append("g")
// // 	.transition() 
// // 	.duration(250)
// 	.attr("transform", function(d) { return "translate(" +  x(d[0])  + "," + margin.top + ")"; } )
// 	.call(chart.width(x.rangeBand())); 
	

// 	// add a title
// 	svg.append("text")
// 	.attr("x", (width / 2))             
// 	.attr("y", 0 + (margin.top / 4))
// 	.attr("text-anchor", "middle")  
// 	.style("font-size", "18px") 
// 	.style("text-decoration", "underline")  
// 	.text(boxplotName);

// 	 // draw y axis
// 	 svg.append("g")
// 	 .attr("class", "y axis")
// 	 .call(yAxis)
// 		.append("text") // and text1
// 		.attr("transform", "rotate(-90)")
// 		.attr("y", yAxisTextYposition)
// 		.attr("x", yAxisTextXposition)
// 		.attr("dy", ".71em")
// 		.style("text-anchor", "end")
// 		.style("font-size", "16px") 
//		.text('<?=$Lang['SDAS']['DSEstat']['box_y'] ?>');		

// 	// draw x axis	
//  	svg.append("g")
//  	.attr("class", "x axis")
//  	.attr("transform", "translate(0," + (height  + margin.top + 10) + ")")
//  	.call(xAxis)
//  	  .append("text")             // text label for the x axis
//  	  .attr("x", (width / 2) )
//  	  .attr("y",  10 )
//  	  .attr("dy", ".71em")
//  	  .style("text-anchor", "middle")
//  	  .style("font-size", "16px") 
	
	
	// Bar chart
	// 	var marginBar = {top: 20, right: 20, bottom: 50, left: 70},
	
//		var data = js_prepare_data(obj);
//		js_prepare_graph(data);


	
	








// 	var marginBar = margin,
// 		widthBar = masterWidth - marginBar.left - marginBar.right,
// 		heightBar = 600 - marginBar.top - marginBar.bottom;

// 	var xBar = d3.scale.ordinal()
// 	.rangeRoundBands([0, widthBar], .5);

// 	var yBar = d3.scale.linear()
// 	.range([heightBar, 0 + marginBar.top ]);
	
// 	var xAxisBar = d3.svg.axis()
// 	.scale(xBar)
// 	.orient("bottom");

// 	var yAxisBar = d3.svg.axis()
// 	.scale(yBar)
// 	.orient("left")
// 				      .ticks(10); //  .ticks(10, "%");

// 	// data count 
// 	var countArr = [];
// 	for(var xx = 0 ; xx< data.length; xx++){
// 		var levelString = '';
// // 		if(xx <5){
// // 			levelString = 'Level ' + (xx+1);
// // 		}
// // 		else if(xx==5){
// // 			levelString = 'Level 5*';
// // 		}
// // 		else if(xx==6){
// // 			levelString = 'Level 5**';
// // 		}
// // 		else{
// // 			levelString = (xx+1);
// // 		}
//  		levelString = data[xx][0];
		
// 		countArr[xx] = { "level" : levelString ,"count" : data[xx][1].length }; 	
// 	} 
	
// 	xBar.domain(countArr.map(function(d) { return d.level;}));
// 	yBar.domain([0, d3.max(countArr, function(d) { return d.count; })]);
	
// 	var bar_svg = d3.select("div#BarChart").append("svg")
// 	.attr("width", widthBar + marginBar.left + marginBar.right)
// 	.attr("height", heightBar + marginBar.top + marginBar.bottom)
// 	.append("g")
// 	.attr("transform", "translate(" + marginBar.left + "," + marginBar.top + ")");

// 	bar_svg.append("g")
// 	.attr("class", "x axis")
// 	.attr("transform", "translate(0," + heightBar + ")")
// 	.call(xAxisBar)
// 	.append("text")             // text label for the x axis
// 	.attr("x", (widthBar / 2) )
// 	.attr("y",  20 )
// 	.attr("dy", ".71em")
// 	.style("text-anchor", "middle")
// 	.style("font-size", "16px");
	
// 	// add a title
// 	bar_svg.append("text")
// 	.attr("x", (width / 2))             
// 	.attr("y", 0 + (margin.top / 4))
// 	.attr("text-anchor", "middle")  
// 	.style("font-size", "18px") 
// 	.style("text-decoration", "underline")  
// 	.text(barChartName);
	
// 	 // add y-axis
// 	 bar_svg.append("g")
// 	 .attr("class", "y axis")
// 	 .call(yAxisBar)
// 	    .append("text") // and text1
// 	    .attr("transform", "rotate(-90)")
// 	    .attr("y", yAxisTextYposition)
// 	    .attr("x", yAxisTextXposition)
// 	    .attr("dy", ".71em")
// 	    .style("text-anchor", "end")
// 	    .style("font-size", "16px") 
//	    .text('<?=$Lang['SDAS']['DSEstat']['bar_y'] ?>');

//     bar_svg.selectAll(".bar")
//     .data(countArr)
//     .enter().append("rect")
//     .attr("class", "bar")
//     .attr("style", function(d,i){ 
//     	var color = rainbow(7,i); 
//     	return "fill:"+ color +";";
//     })
//     .attr("x", function(d) { return xBar(d.level);})
//     .attr("width", xBar.rangeBand())
//     .attr("y", function(d,i) { return yBar(d.count); })
//     .attr("height", function(d) { return heightBar - yBar(d.count); });		
// }

// Returns a function to compute the interquartile range.
// function iqr(k) {
//   return function(d, i) {
//     var q1 = d.quartiles[0],
//         q3 = d.quartiles[2],
//         iqr = (q3 - q1) * k,
//         i = -1,
//         j = d.length;
//     while (d[++i] < q1 - iqr);
//     while (d[--j] > q3 + iqr);
//     return [i, j];
//   };
// }

// function rainbow(numOfSteps, step) {
//    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
//    // Adam Cole, 2011-Sept-14
//    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
//    var r, g, b;
//    var h = step / numOfSteps;
//    var i = ~~(h * 6);
//    var f = h * 6 - i;
//    var q = 1 - f;
//    switch(i % 6){
//        case 0: r = 1; g = f; b = 0; break;
//        case 1: r = q; g = 1; b = 0; break;
//        case 2: r = 0; g = 1; b = f; break;
//        case 3: r = 0; g = q; b = 1; break;
//        case 4: r = f; g = 0; b = 1; break;
//        case 5: r = 1; g = 0; b = q; break;
//    }
//    var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
// 		return (c);    
// //     return ('#487db4');
// }
</script>


<form id="form1" name="form1" method="POST" action="ajax_load_stat.php">
	<input type="hidden" name="action" value="DSE_analysis" />
	<?=$html_tab_menu?>
					<table border="0" cellspacing="0" cellpadding="5"
		class="form_table_v30">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top"><?=$Lang['General']['From'].' '.$FromAcademicYearSelection.' '.$Lang['General']['To'].' '.$ToAcademicYearSelection?></td>
		</tr>

		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
			<td valign="top"><?=$FromSubjectSelection?></td>
		</tr>

		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$iPort['class']?></span></td>
<!-- 			<td valign="top"><input type="checkbox" id="resultType_SelectAll" /> -->
<!-- 							</td> -->
	<td id = "Class"><?= $Lang['General']['JS_warning']['SelectAForm']?></td>
		</tr>

		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["type"]?></span></td>
			<td valign="top"><input type="radio" id="type_mock"
				name="report_type" value="mock" checked="checked" /> <label
				for="type_mock"><?=$Lang['SDAS']['DSEstat']['Mock']?></label> <br />
				<input type="radio" id="int_result" name="report_type" value="int" />
				<label for="int_result"><?=$Lang['SDAS']['DSEstat']['Int']?></label>
				<div id="formSel" style="display: inline;"><?=$targetFormSelection?></div>
				<div id="termSel" style="display: inline;"></div></td>
		</tr>
	</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>

	<div class="edit_bottom_v30">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>"
			class='formbutton' onmouseover="this.className='formbuttonon'"
			onmouseout="this.className='formbutton'" onclick="jsLoadGraph();" />
	</div>
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	<br>
	<div id="PrintArea">
		<div id="DBTableDiv"></div>
		<div id="BoxPlot"></div>
		<div id="DataTableBox"></div>
		<div id="BarChart"></div>
		<div id="DataTableBar"></div>
	</div>
</form>

<script>
$("#resultType_SelectAll").click(function(){
	if($(this).attr("checked")){
		$(".ClassSel").attr("checked", "checked");
	}else{
		$(".ClassSel").removeAttr("checked");
	}
});
$(".ClassSel").click(function(){
	if(!$(this).attr("checked")){
		$("#resultType_SelectAll").removeAttr("checked");
	}
});
$( "#int_result" ).click(function() {
  	if($(this).attr("checked")){
		$('#targetForm').removeAttr('disabled');
		$("select[name=YearTermID]").removeAttr('disabled');
	}
});
$( "#type_mock" ).click(function() {
  	if($(this).attr("checked")){
		$('#targetForm').attr('disabled','disabled');
		$("select[name=YearTermID]").attr('disabled','disabled');
	}
});

</script>

<?php
$linterface->LAYOUT_STOP ();
intranet_closedb ();
?>