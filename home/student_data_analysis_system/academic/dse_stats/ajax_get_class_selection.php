<?php
/**
 * This AJAX is for creating the classes checkbox
 */
/**
 * Change Log:
 * 2017-11-17 Pun
 *  - File create
 */
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
$libpf_exam = new libpf_exam();
$lpf = new libportfolio();

$libFCM_ui = new form_class_manage_ui();

echo $libFCM_ui->Get_Class_Selection($AcademicYearID, $YearID, $ID_Name='YearClassID', $SelectedYearClassID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=1, $TeachingOnly=0, $firstTitle='');