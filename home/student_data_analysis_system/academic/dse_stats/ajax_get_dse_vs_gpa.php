<?php 
/**
 * Change Log:
 * 2017-11-17 Pun
 *  - File create
 */
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
$libpf_exam = new libpf_exam();
$lpf = new libportfolio();
$fcm = new form_class_manage();
$jsonObj = new JSON_obj();

################ Get student START ################
$rs = $fcm->Get_Student_By_Form($AcademicYearID, $YearID, $YearClassID);
$studentIdArr = Get_Array_By_Key($rs, 'UserID');
$studentIdSql = implode("','", $studentIdArr);
################ Get student END ################


################ Get data START ################

######## Get best 5 START ########
$coreIdAssocArr= BuildMultiKeyAssoc($libpf_exam->getDseCompulsorySubjectID(), 'CODEID', array('SubjectID'), 1);

$sql = "SELECT
				StudentID, SubjectCode, Grade
			FROM
				EXAM_DSE_STUDENT_SCORE
			WHERE
				AcademicYearID = '".$AcademicYearID."'
				AND SubjectID = '".$coreIdAssocArr['080']."'
				AND isArchive = 0 
			    AND StudentID IN ('{$studentIdSql}')";
$chiScoreArr = $libpf_exam->returnResultSet($sql) ;
$sql = "SELECT
				StudentID, SubjectCode, Grade
			FROM
				EXAM_DSE_STUDENT_SCORE
			WHERE
				AcademicYearID = '".$AcademicYearID."'
				AND SubjectID = '".$coreIdAssocArr['165']."'
				AND isArchive = 0 
			    AND StudentID IN ('{$studentIdSql}')";
$engScoreArr = $libpf_exam->returnResultSet($sql) ;
$sql = "SELECT
				StudentID, SubjectCode, Grade
			FROM
				EXAM_DSE_STUDENT_SCORE
			WHERE
				AcademicYearID = '".$AcademicYearID."'
				AND SubjectID = '".$coreIdAssocArr['22S']."'
				AND isArchive = 0 
			    AND StudentID IN ('{$studentIdSql}')";
$mathScoreArr = $libpf_exam->returnResultSet($sql) ;
$sql = "SELECT
				StudentID, SubjectCode, Grade
			FROM
				EXAM_DSE_STUDENT_SCORE
			WHERE
				AcademicYearID = '".$AcademicYearID."'
				AND SubjectID = '".$coreIdAssocArr['265']."'
				AND isArchive = 0 
			    AND StudentID IN ('{$studentIdSql}')";
$lsScoreArr = $libpf_exam->returnResultSet($sql) ;

$studentIDArr1 = array_merge( Get_Array_By_Key($chiScoreArr,'StudentID'), Get_Array_By_Key($engScoreArr,'StudentID'), Get_Array_By_Key($mathScoreArr,'StudentID'), Get_Array_By_Key($lsScoreArr,'StudentID') );

$sql = "select 
    StudentID, 
    SubjectID, 
    SubjectCode, 
    Grade 
from 
    EXAM_DSE_STUDENT_SCORE 
where 
    AcademicYearID = '".$AcademicYearID."' 
and 
    ParentSubjectCode ='' 
and 
    SubjectCode NOT IN ('A010', 'A020', 'A030', 'A040') 
AND
    isArchive = 0 
AND 
    StudentID IN ('{$studentIdSql}')
Order by 
    StudentID,SubjectCode";
$result = $libpf_exam->returnResultSet($sql);
$studentIDArr2 = Get_Array_By_Key($result,'StudentID');
$AssocResult = BuildMultiKeyAssoc($result, 'StudentID','',0,1);

$studentIDArr = array_unique(array_merge((array)$studentIDArr2, (array)$studentIDArr1));
//$studentInfo = $libpf_exam->getUserNameByID($studentIDArr);
$studentInfoArr = $libpf_exam->getStudentsInfoByID($studentIDArr, $AcademicYearID);

// rebuild $coreResult
$coreSubjectArr = array( 'chi', 'eng', 'math', 'ls');
$coreResult = array();
$count = 0;
foreach($coreSubjectArr as $_subject){
    ${$_subject.'ScoreAssoc'} = BuildMultiKeyAssoc( ${$_subject.'ScoreArr'}, 'StudentID' );
}
foreach($studentIDArr as $_sid){
    $coreResult[$count]['StudentID'] = $_sid;
    foreach($coreSubjectArr as $_subject){
        $_thisSubjectScoreArr = ${$_subject.'ScoreAssoc'};
        $coreResult[$count][$_subject.'Code'] = $_thisSubjectScoreArr[$_sid]['SubjectCode'];
        $coreResult[$count][$_subject.'Grade'] = $_thisSubjectScoreArr[$_sid]['Grade'];
    }
    $count ++;
}
$coreAssoc = BuildMultiKeyAssoc($coreResult, 'StudentID');

$studentSubjectScoreAssoc = array();
foreach((array)$studentIDArr as  $_thisID){
    $_thisElectiveResultArr = $AssocResult[$_thisID];
    if(count($_thisElectiveResultArr)>1){
        sortByColumn2($_thisElectiveResultArr,'Grade',1);
    }
    $studentSubjectScoreAssoc[$_thisID][] = $coreAssoc[$_thisID]['chiGrade'];
    $studentSubjectScoreAssoc[$_thisID][] = $coreAssoc[$_thisID]['engGrade'];
    $studentSubjectScoreAssoc[$_thisID][] = $coreAssoc[$_thisID]['mathGrade'];
    $studentSubjectScoreAssoc[$_thisID][] = $coreAssoc[$_thisID]['lsGrade'];
    $coreAssoc[$_thisID]['StudentID'] = $_thisID;
    $count = 1;
    foreach((array)$_thisElectiveResultArr as $__resultInfoArr){
        $__thisSubjectId = $libpf_exam->getSubjectIDByDseSubjectCode($__resultInfoArr['SubjectCode']);
        $__thisSubjectInfoTemp = $libpf_exam->getSubjectCodeAndID($__thisSubjectId);
        $__thisSubjectInfo = $__thisSubjectInfoTemp[0];
        $__displaySubjectName = empty($__thisSubjectInfo) ? '' : ' ('.$__thisSubjectInfo['EN_SNAME'].')';
        $__thisElective = $__resultInfoArr['SubjectCode'].$__displaySubjectName;
        $__thisElectiveGrade =  $__resultInfoArr['Grade'];
        $coreAssoc[$_thisID]['elective'.$count.'Code'] = $__thisElective;
        $coreAssoc[$_thisID]['elective'.$count.'Grade'] = $__thisElectiveGrade;
        $studentSubjectScoreAssoc[$_thisID][] = $__thisElectiveGrade;
        $count++;
    }
}

// count best 5 score
$studentbest5ScoreArr = array();
foreach((array)$studentSubjectScoreAssoc as $stuID => $dseLvArr){
    $scoreArr = $libpf_exam->convertDseLevelToScore($dseLvArr);
    if(count($scoreArr)>0){
        rsort($scoreArr);
        array_splice($scoreArr,5);
        $studentbest5ScoreArr[$stuID] = array_sum($scoreArr);
    }
}

// debug_r($studentbest5ScoreArr);
######## Get best 5 END ########

######## Get overall score START ########
$sql = "SELECT 
    UserID,
    Score
FROM
    {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
WHERE
    AcademicYearID = '{$AcademicYearID}'
AND
    IsAnnual = 1
AND
    UserID IN ('{$studentIdSql}')
AND 
    Score <> -1
AND
    Score IS NOT NULL";
$rs = $libpf_exam->returnResultSet($sql);
$studentOverallScoreMapping = BuildMultiKeyAssoc($rs, array('UserID') , array('Score'), $SingleValue=1);
// debug_r($studentOverallScoreMapping);
######## Get overall score END ########


######## Get student info START ########
$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
$ArchiveSymbol = '*';
$NameField = getNameFieldByLang('u.');
$ArchiveNameField = getNameFieldByLang2('au.');
$sql = "SELECT
	ycu.UserID,
	CASE 
		WHEN au.UserID IS NOT NULL then CONCAT('<span class=\"tabletextrequire\">".$ArchiveSymbol."</span>', ".$ArchiveNameField.") 
		WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">".$ArchiveSymbol."</span>', ".$NameField.") 
		ELSE $NameField
	END as StudentName,
	$ClassNameField as ClassName,
	ycu.ClassNumber
From
	YEAR_CLASS_USER as ycu
Inner Join
	YEAR_CLASS as yc 
On 
    (ycu.YearClassID = yc.YearClassID)
LEFT JOIN 
	INTRANET_USER as u 
ON 
    (ycu.UserID = u.UserID)
LEFT JOIN 
	INTRANET_ARCHIVE_USER as au 
ON 
    (ycu.UserID = au.UserID) 
Where
	yc.AcademicYearID = '".$AcademicYearID."' 
AND
    ycu.UserID IN ('{$studentIdSql}')";
$rs = $libpf_exam->returnResultSet($sql);
$studentInfoArr = BuildMultiKeyAssoc($rs, array('UserID'));
// debug_r($studentName);
######## Get student info END ########
################ Get data END ################


################ Pack data START ################
$dataAry = array();
foreach($studentbest5ScoreArr as $studentID => $dse){
    $student = $studentInfoArr[$studentID];
    $overall = $studentOverallScoreMapping[$studentID];
    
    if($dse && $overall){
        $dataAry[] = array(
            'x' => floatval($overall),
            'y' => intval($dse),
            'name' => $student['StudentName'],
            'class_and_no' => "{$student['ClassName']}-{$student['ClassNumber']}"
        );
    }
}
################ Pack data END ################


################ Chart START ################
//graph title
$graphTitle = $Lang['SDAS']['DseVsGpa']['graphTitle'];

// xAxis var
$xAxisMin = 0;
$xAxisMax = 5;
$xAxisInterval = .5;
$xAxisName = $Lang['SDAS']['DseVsGpa']['gpaResult'];
$xAxisPlotLineVal = 2.5 ;
$xAxisPlotLineText = $Lang['SDAS']['DseVsGpa']['highChanceJupasOffer'];
$xAxisPlotLineDash = 'Dash';

// yAxis var
$yAxisMin = 0;
$yAxisMax = 40;
$yAxisInterval = 5;
$yAxisName = $Lang['SDAS']['DseVsGpa']['dseBest5'];
$yAxisJUPASlotLineText = $Lang['SDAS']['DseVsGpa']['jupasBaseLine'];
$yAxisJUPASPlotLineVal = 18 ;
$yAxisJUPASPlotLineDash = 'Dash';
$yAxisUniPlotLineText = $Lang['SDAS']['DseVsGpa']['universityBaseLink'];
$yAxisUniPlotLineVal = 22;
$yAxisUnilotLineDash = 'Solid';

//series
$UnexpectedName = $Lang['SDAS']['DseVsGpa']['unexpectedResult'];
$SlippedName = $Lang['SDAS']['DseVsGpa']['slipped'];
$resultTitle = $Lang['SDAS']['DseVsGpa']['studentResult'];


$unexpectedPolyAry = array(
    array(0, 40),
    array(0, 18),
    array(2.5, 18),
    array(2.5, 40)    
);

$slippedPolyAry = array(
    array(2.5, 18),
    array(2.5, 0),
    array(5, 0),
    array(5, 18)
);

$dataJsonString = $jsonObj->encode($dataAry);
$unexpectedPolyString = $jsonObj->encode($unexpectedPolyAry);
$slippedPolyString = $jsonObj->encode($slippedPolyAry);
//echo $dataJsonString

################ Chart END ################

?>
<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};




Highcharts.chart('container', {

    chart: {
        type: 'scatter',
        plotBorderWidth: 1,
        zoomType: 'xy'
    },

    legend: {

        symbolRadius: 0
    },

    title: {
        text: "<?php echo $graphTitle ?>"
    },

    xAxis: {
    	tickInterval: <?php echo $xAxisInterval ?>,
        gridLineWidth: 1,
        ceiling: 5,
        floor: 0,
        title: {
        	style: {
                color: 'black'
            },
            align: 'middle',
            text: "<?php echo $xAxisName ?>"
        },
        labels: {
            format: '{value} '
        },
        plotLines: [{
            color: 'black',
            dashStyle: "<?php echo $xAxisPlotLineDash ?>",
            width: 1.5,
            value: "<?php echo $xAxisPlotLineVal ?>",
            label: {
                rotation: 0,
                y: -5,
                style: {
//                     fontStyle: 'italic'
                },
                text: "<?php echo $xAxisPlotLineText ?>"
            },
            zIndex: 3
        }]
    },

    yAxis: {
    	tickInterval: <?php echo $yAxisInterval ?>,
        startOnTick: false,
        endOnTick: false,
        ceiling: 40,
        title: {
        	style: {
                color: 'black'
            },
        	offset: 50,
        	align: 'middle',
            text: "<?php echo $yAxisName ?>"
            	
        },
        labels: {
            format: '{value} '
        },
        maxPadding: 0.2,
        plotLines: [{
            color: 'black',
            dashStyle: "<?php echo $yAxisJUPASPlotLineDash ?>",
            width: 1.5,
            value: "<?php echo $yAxisJUPASPlotLineVal ?>",
            label: {
                align: 'left',
                style: {
//                     fontStyle: 'italic'
                },
                text: "<?php echo $yAxisJUPASlotLineText ?>", 
                x: 5,
                y: 12
            },
            zIndex: 1
        }, {
        	color: 'black',
            dashStyle: "<?php echo $yAxisUnilotLineDash ?>",
            width: 1,
            value: "<?php echo $yAxisUniPlotLineVal ?>", 
            label: {
                align: 'right',
                style: {
//                     fontStyle: 'italic'
                },
                text: "<?php echo $yAxisUniPlotLineText ?>", 
                x: -5,
                y: -5
            },
            zIndex: 1
            }]
    },

    tooltip: {
    	useHTML: true,
        headerFormat: '<table>',
        pointFormat: '<tr><th nowrap colspan="2"><h4 style="margin: 0;">{point.name} ({point.class_and_no})</h4></th></tr>' +
            '<tr><th nowrap><?=$Lang['SDAS']['DseVsGpa']['schoolResult'] ?>:</th><td nowrap>{point.x} G.P.A.</td></tr>' +
            '<tr><th nowrap><?=$Lang['SDAS']['DseVsGpa']['dseResult'] ?>:</th><td nowrap>{point.y} points</td></tr>',
        footerFormat: '</table>',
        followPointer: true
    },
    
    credits: {
        enabled: false
    },
    series: [ {
        name: "<?php echo $UnexpectedName ?>",
        type: 'polygon',
        data: <?php echo $unexpectedPolyString ?>,
        color: 	'rgba(0,255,0,0.3)',
        enableMouseTracking: false,
        marker: {
            symbol: 'square',
            lineWidth: 1
        },
        zIndex: 3

    }, {
    	name: "<?php echo $SlippedName ?>",
        type: 'polygon',
        data: <?php echo $slippedPolyString ?>,
        color: 	'rgba(0,0,255,0.3)',
        enableMouseTracking: false,
        marker: {
            symbol: 'square',
            lineWidth: 1
        },
        zIndex: 3

    },{
    	name: "<?php echo $resultTitle ?>",
        data: <?php echo $dataJsonString ?>,
        color: 	'rgb(255,0,0)',
        marker: {
            symbol: 'circle',
            lineWidth: 1
        }
    }],


});	
</script>
<div id="container"></div>