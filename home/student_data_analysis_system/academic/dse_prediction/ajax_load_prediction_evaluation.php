<?php
/*
 ###################
 ## 2018-06-22 Pun [ip.2.5.9.7.1]
 ##	Added display filter
 ##
 ## 2018-05-14 Pun [139436] [ip.2.5.9.5.1]
 ##	Added display no data
 ##
 ## 2017-12-08 Pun [ip.2.5.9.1.1]
 ##	Added edit dse prediction
 ##
 ## 2017-06-20 Pun
 ## New File
 ###################
 */

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
// include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();
$libpf_exam = new libpf_exam();
$scm = new subject_class_mapping();


######## Init START ########
define('DECIMAL_PLACES', 2);
define('SCORE_DECIMAL_PLACES', 1);

$display = $_POST['display'];
$AcademicYearID = $_POST['FromAcademicYearID'];
$SelectForm = $_POST['SelectForm'];
$searchSubjectID = $_POST['SubjectID'];
$searchSubjectGroupID = $_POST['subjectGroup_box'];
$searchStudentID = $_POST['StudentID'];

$accessRight = $objSDAS->getAssessmentStatReportAccessRight();
if(!$sys_custom['SDAS']['allTeacherAccessDSE']){
    if(!$accessRight['admin'] && count($accessRight['classTeacher'])){ //not admin and class teacher
        $IsClassTeacher = 1;
    }
}

if($display == 'student'){
    $searchSubjectID = '';
}
######## Init END ########


######## Get SubjectGroupInfo START ########
if($display == 'group'){
    $sql = "SELECT SubjectID FROM SUBJECT_TERM WHERE SubjectGroupID='{$searchSubjectGroupID}'";
    $rs = $scm->returnVector($sql);
    
    $searchSubjectID = $rs[0];
}
######## Get SubjectGroupInfo END ########


######## Get YearClassInfo START ########
if($display == 'class'){
    $allClassInfo = $libpf_exam->getClassListByAcademicYearIDAndForm($AcademicYearID,$SelectForm);
    
    if($isClassTeacher){
        $i = 0;
        $temp = array();
        foreach($accessRight['classTeacher'] as $_classInfo){
            foreach($allClassInfo as $class){
                if($_classInfo['AcademicYearID']==$AcademicYearID){
                    if($class['YearClassID']==$_classInfo['YearClassID']){
                        $temp[$i]['YearClassID'] = $class['YearClassID'];
                        $temp[$i]['ClassTitleEN'] = $class['ClassTitleEN'];
                        $temp[$i]['ClassTitleB5'] = $class['ClassTitleB5'];
                        $temp[$i]['WEBSAMSCode'] = $class['WEBSAMSCode'];
                        $i++;
                    }
                }
            }
        }
        unset($allClassInfo);
        $allClassInfo = $temp;
    }
    
    $yearClassNameEnArr = Get_Array_By_Key($allClassInfo, 'ClassTitleEN');
}
######## Get YearClassInfo END ########


######## Get StudentInfo START ########
if($display == 'class'){
    $yearClassNameEnArr = array_intersect($yearClassNameEnArr, (array)$Class);
    $userIDArr = $libpf_exam->getStudentIDByClassNameAndYear( $AcademicYearID ,$yearClassNameEnArr);
}elseif($display == 'group'){
    $rs = $scm->Get_Subject_Group_Student_List($searchSubjectGroupID);
    $userIDArr = Get_Array_By_Key($rs, 'UserID');
}else{
    $userIDArr = (array)$searchStudentID;
}

$studentInfoArr =  $libpf_exam->getStudentsYearInfoByID($userIDArr, $AcademicYearID);
$studentAssoc = BuildMultiKeyAssoc($studentInfoArr, 'UserID', array());
######## Get StudentInfo END ########


######## Get SubjectInfo START ########
$subjectInfoArr = $libpf_exam->getSubjectCodeAndID($searchSubjectID, array(), true, false);
$allSubjectDetail = BuildMultiKeyAssoc($subjectInfoArr, 'SubjectID');

$subjectDetail = array();
if(count($subjectInfoArr)){
    $subjectDetail = $subjectInfoArr[0];
}
######## Get SubjectInfo END ########


######## Get DSE data START ########
$previouseDse = $libpf_exam->getExamData( $AcademicYearID, EXAMID_HKDSE, $searchSubjectID, $userIDArr, $excludeCmp=false, $excludeUnmatchSubject=true);
$dseResultAssoc = BuildMultiKeyAssoc($previouseDse, array('SubjectID', 'StudentID'), array('Score'), 1, 0);
######## Get DSE data END ########


######## Get predict data START ########
$rs = $libpf_exam->getDsePredictResult($AcademicYearID, $searchSubjectID);

$predictResult = array();
foreach($rs as $r){
    if(in_array($r['StudentID'], $userIDArr)){
        $predictResult[$r['SubjectID']][$r['StudentID']] = $r;
    }
}
######## Get predict data END ########


######## Calculate result-predict different START ########
$predictDiffArr = array();
$predictDiffAbsArr = array();

foreach($dseResultAssoc as $subjectID => $d1){
    foreach($d1 as $studentId => $grade){
        $dseLevel[$subjectID] = $libpf_exam->getDSEScore($grade);
        
        if(!isset($predictResult[$subjectID][$studentId]['PredictGrade'])){
            continue;
        }
        
        $predictLevel[$subjectID] = $libpf_exam->getDSEScore($predictResult[$subjectID][$studentId]['PredictGrade']);
        $predictDiffArr[$subjectID][$studentId] = $dseLevel[$subjectID] - $predictLevel[$subjectID];
        $predictDiffAbsArr[$subjectID][$studentId] = abs($predictDiffArr[$subjectID][$studentId]);
    }
}
######## Calculate result-predict different END ########


######## Calculate summary START ########
if($display == 'student'){
    $levelCountArr = array();
    $noTotalSubject = count($predictResult);
    $noPassSubject = 0;
    foreach((array)$predictResult as $predictInfo){
        $level = $libpf_exam->getDSEScore($predictInfo[$searchStudentID]['PredictGrade']);
        
        if($level > 1){ // DSE grade 2~5** = pass
            $noPassSubject++;
        }
        
        if(!isset($levelCountArr[$level])){
            $levelCountArr[$level] = 0;
        }
        $levelCountArr[$level]++;
    }
    $passPercent = ($noTotalSubject)? ($noPassSubject/$noTotalSubject)*100 : 0;
    
    $_predictDiffArr = array();
    $_predictDiffAbsArr = array();
    foreach((array)$predictDiffArr as $predictInfo){
        $_predictDiffArr[] = $predictInfo[$searchStudentID];
        $_predictDiffAbsArr[] = abs($predictInfo[$searchStudentID]);
    }
    $predictDiffAvg = (count($_predictDiffArr))? (array_sum($_predictDiffArr) / count($_predictDiffArr)) : 0;
    $predictDiffAbsAvg = (count($_predictDiffAbsArr))? (array_sum($_predictDiffAbsArr) / count($_predictDiffAbsArr)) : 0;
}else{
    foreach((array)$allSubjectDetail as $subjectID => $subject){
        if(!isset($predictDiffArr[$subjectID])){
            //continue;
        }
        
        $predictDiffAvg[$subjectID] = (count($predictDiffArr[$subjectID]))? (array_sum($predictDiffArr[$subjectID]) / count($predictDiffArr[$subjectID])) : 0;
        $predictDiffAbsAvg[$subjectID] = (count($predictDiffAbsArr[$subjectID]))? (array_sum($predictDiffAbsArr[$subjectID]) / count($predictDiffAbsArr[$subjectID])) : 0;
        $noCandidate[$subjectID] = count($predictResult[$subjectID]);
        $noPass[$subjectID] = 0;
        $levelCountArr = array();
        foreach((array)$predictResult[$subjectID] as $r){
            $level = $libpf_exam->getDSEScore($r['PredictGrade']);
            if($level > 1){ // DSE grade 2~5** = pass
                $noPass[$subjectID]++;
            }
            
            if(!isset($levelCountArr[$subjectID][$level])){
                $levelCountArr[$subjectID][$level] = 0;
            }
            $levelCountArr[$subjectID][$level]++;
        }
        
        $passPercent[$subjectID] = ($noCandidate[$subjectID])? ($noPass[$subjectID] / $noCandidate[$subjectID]) * 100 : 0;
    }
}
######## Calculate summary END ########



######## UI START ########
?>
<style>
.footerTitle{
    text-align: right !important;
    font-weight: bold;
    margin-right: 5px;
}
tfoot td, tfoot tr{
    //border-bottom: 0 !important;
    //border-top: 0 !important;
}
tfoot tr:first-child td{
    border-top-width: 3px;
}
.currentGrade, .originalGrade {
    width: 25px;
    display: inline-block;
}
.originalGrade{
    color: grey;
}
.predictGradeEdit{
    display: none;
}
.editPredictGrade{
    margin-left: 5px;
}

@media only print{
    .printHide{
        display:none !important;
    }
}
</style>

<?php
if( $display == 'class' && !$searchSubjectID ): 
#################################### All subject summary report START ####################################
?>


<?= $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['DSEpredictionEvaluation']['EvaluationReport']) ?>
<table class="common_table_list_v30 view_table_list_v30" style="text-align:center;">
    <thead>
        <tr>
            <th width="50"><?=$Lang['SDAS']['Subject'] ?></th>
            <th width="50"><?=$Lang['SDAS']['DSEpredictionEvaluation']['MeanError'] ?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['MeanAbsoluteError'] ?></th>
        </tr>
    </thead>
    
    <tbody>
    	<?php 
    	$hasData = false;
    	foreach($allSubjectDetail as $subjectID => $subjectInfo){ 
    	    if($noCandidate[$subjectID] == 0){
    	        continue;
    	    }
	    ?>
            <tr>
            	<td><?= Get_Lang_Selection($subjectInfo['CH_SNAME'],$subjectInfo['EN_SNAME']) ?></td>
    			<td><?=number_format($predictDiffAvg[$subjectID], DECIMAL_PLACES) ?></td>
    			<td><?=number_format($predictDiffAbsAvg[$subjectID], DECIMAL_PLACES) ?></td>
            </tr>
        <?php 
            $hasData = true;
    	} 
    	
    	if(!$hasData){
        ?>
        	<tr>
        		<td colspan="3" style="text-align: center;"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
        	</tr>
        <?php
    	}
        ?>
    </tbody>
</table>

<script>
$(function(){
	$('#resultDiv table').floatHeader();
});
</script>


<?php 
#################################### All subject summary report END ####################################
elseif( $display == 'group' || $searchSubjectID ):
#################################### Single subject report START ####################################
?>

<?= $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['DSEpredictionEvaluation']['EvaluationReport']) ?>
<div class="printHide" style="display: inline-block;float:right;">
	<div class="table_row_tool row_content_tool">
    	<label>
    		<input type="checkbox" id="showEditButton" value="1" />
    		<?=$Lang['Btn']['Edit'] ?>
    	</label>
	</div>
</div>
<table class="common_table_list_v30 view_table_list_v30" style="text-align:center;">
	<colgroup>
		<col style="width:12.5%;"/>
		<col style="width:12.5%;"/>
		<col style="width:12.5%;"/>
		<col style="width:12.5%;"/>
		<col style="width:12.5%;"/>
		<col style="width:12.5%;"/>
		<col style="width:12.5%;"/>
		<col style="width:12.5%;"/>
	</colgroup>
    <thead>
        <tr>
            <th width="50"><?=$Lang['General']['Class'] ?></th>
            <th width="50"><?=$Lang['General']['ClassNo_ShortForm'] ?></th>
            <th width="60"><?=$Lang['AccountMgmt']['StudentName'] ?></th>
            <th width="60"><?= Get_Lang_Selection($subjectDetail['CH_SNAME'],$subjectDetail['EN_SNAME']); ?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['PredictGrade'] ?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['DseResult'] ?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['DsePredictDiff'] ?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['DsePredictDiffAbs'] ?></th>
        </tr>
    </thead>
    <tbody>
    	<?php 
    	if(count($studentAssoc)){
        	foreach((array)$studentAssoc as $studentID => $studentInfo){ 
        	    $recordID = $predictResult[$searchSubjectID][$studentID]['RecordID'];
        	    $academicYearID = $predictResult[$searchSubjectID][$studentID]['AcademicYearID'];
        	    $score = $predictResult[$searchSubjectID][$studentID]['ExamScore'];
        	    $predictGrade = $predictResult[$searchSubjectID][$studentID]['PredictGrade'];
        	    $predictGradeOriginal = $predictResult[$searchSubjectID][$studentID]['PredictGradeOriginal'];
        	    $dseResult = $dseResultAssoc[$searchSubjectID][$studentID];
        	?>
                <tr>
                	<td><?=$studentInfo['ClassName'] ?></td>
                	<td><?=$studentInfo['ClassNumber'] ?></td>
                	<td><?=$studentInfo['Name'] ?></td>
                	<td><?=(is_null($score))?Get_String_Display(''):number_format($score, SCORE_DECIMAL_PLACES) ?></td>
                	<td>
                		<?php if($predictGrade){ ?>
            				
                    		<div class="predictGradeLoading exportHide" style="display:none;">
                    			<?=$linterface->Get_Ajax_Loading_Image() ?>
                    		</div>
                    		<div class="predictGradeDisplay">
                        		<span class="currentGrade exportContent">
                        			<?=Get_String_Display($predictGrade) ?>
                        		</span>
                    		    <span class="originalGrade exportContent">
                    		    	<?= ( $predictGradeOriginal? '('.Get_String_Display($predictGradeOriginal).')' : '' ) ?>
                        		</span>
                        		
    	                		<input type="button" class="exportHide printHide editPredictGrade" value="<?=$Lang['Btn']['Edit'] ?>" style="display:none;" />
                    		</div>
                    		<div class="predictGradeEdit exportHide printHide">
                    			<form class="changePredictGradeForm">
        							<?=getSelectByValue($data=$DSE_GRADE,$tags=' name="DseGrade" class="dseGradeEdit"',$selected=$predictGrade,$all=0,$noFirst=1, $allName="", $ParQuoteValue=1) ?>
        							<input type="hidden" name="RecordID" value="<?=$recordID ?>" />
        							<input type="hidden" name="AcademicYearID" value="<?=$academicYearID ?>" />
        							<input type="hidden" name="SubjectID" value="<?=$searchSubjectID ?>" />
        							<input type="hidden" name="StudentID" value="<?=$studentID ?>" />
        							<input type="hidden" name="ActualDseResult" value="<?=$dseResult ?>" />
        							<input type="submit" value="<?=$Lang['Btn']['Submit'] ?>" />
        							<input type="button" class="predictGradeEditCancel" value="<?=$Lang['Btn']['Cancel'] ?>" />
                    			</form>
                    		</div>
                		<?php }else{ ?>
                			<?=Get_String_Display('') ?>
                		<?php } ?>
                	</td>
                	<td><?=Get_String_Display($dseResult) ?></td>
                	<td class="actualDseResult"><?=Get_String_Display($predictDiffArr[$searchSubjectID][$studentID]) ?></td>
                	<td class="actualDseResultAbs"><?=Get_String_Display($predictDiffAbsArr[$searchSubjectID][$studentID]) ?></td>
                </tr>
        <?php 
        	} 
    	}else{
	    ?>
        	<tr>
        		<td colspan="8" style="text-align: center;"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
        	</tr>
	    <?php
    	}
    	?>
    </tbody>
    <?php if(count($studentAssoc)){ ?>
        <tfoot>
        	<tr>
        		<td colspan="3" style="border-right: 0">&nbsp;</td>
        		<td class="footerTitle" style="border-left: 0"><?=$Lang['SDAS']['DSEpredictionEvaluation']['NumOfPass'] ?></td>
        		<td><?=$noPass[$searchSubjectID] ?></td>
        		<td class="footerTitle"><?=$Lang['SDAS']['averageValue'] ?></td>
        		<td><?=number_format($predictDiffAvg[$searchSubjectID], DECIMAL_PLACES) ?></td>
        		<td><?=number_format($predictDiffAbsAvg[$searchSubjectID], DECIMAL_PLACES) ?></td>
    		</tr>
        		<td colspan="3" style="border-right: 0">&nbsp;</td>
        		<td class="footerTitle" style="border-left: 0"><?=$Lang['SDAS']['DSEpredictionEvaluation']['NumOfCandidate'] ?></td>
        		<td><?=$noCandidate[$searchSubjectID] ?></td>
        		<td>&nbsp;</td>
        		<td>&nbsp;</td>
        		<td>&nbsp;</td>
        	</tr>
    		</tr>
        		<td colspan="3" style="border-right: 0">&nbsp;</td>
        		<td class="footerTitle" style="border-left: 0"><?=$Lang['SDAS']['DSEpredictionEvaluation']['PassPercent'] ?></td>
        		<td><?=number_format($passPercent[$searchSubjectID], DECIMAL_PLACES) ?>%</td>
        		<td>&nbsp;</td>
        		<td>&nbsp;</td>
        		<td>&nbsp;</td>
        	</tr>
        	
        	<?php
        	$dseLevelArr = $libpf_exam->getDseLevelSequenceArr();
        	foreach($dseLevelArr as $level){
        	    $level = $libpf_exam->getDSEScore($level);
        	    if(!$levelCountArr[$searchSubjectID][$level]){
        	        continue;
        	    }
        	    $levelTitle = str_replace('<!--Level-->', $libpf_exam->getDSELevel($level),$Lang['SDAS']['DSEpredictionEvaluation']['NumOfLevel']);
        	?>
        		<tr>
            		<td colspan="3" style="border-right: 0">&nbsp;</td>
            		<td class="footerTitle" style="border-left: 0"><?=$levelTitle ?></td>
            		<td><?=$levelCountArr[$searchSubjectID][$level] ?></td>
            		<td>&nbsp;</td>
            		<td>&nbsp;</td>
            		<td>&nbsp;</td>
        		</tr>
        	<?php 
        	}
        	?>
        	
        </tfoot>
    <?php } ?>
</table>

<?php 
#################################### Single subject report END ####################################
else:
#################################### Student report START ####################################
?>

<?= $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['DSEpredictionEvaluation']['EvaluationReport']) ?>
<div class="printHide" style="display: inline-block;float:right;">
	<div class="table_row_tool row_content_tool">
    	<label>
    		<input type="checkbox" id="showEditButton" value="1" />
    		<?=$Lang['Btn']['Edit'] ?>
    	</label>
	</div>
</div>
<table class="common_table_list_v30 view_table_list_v30" style="text-align:center;">
	<colgroup>
		<col style="width:16.6%;"/>
		<col style="width:16.6%;"/>
		<col style="width:16.6%;"/>
		<col style="width:16.6%;"/>
		<col style="width:16.6%;"/>
		<col style="width:16.6%;"/>
	</colgroup>
    <thead>
        <tr>
            <th width="60"><?=$Lang['SDAS']['Subject']?></th>
            <th width="60"><?=$Lang['Gamma']['Score']?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['PredictGrade'] ?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['DseResult'] ?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['DsePredictDiff'] ?></th>
            <th width="60"><?=$Lang['SDAS']['DSEpredictionEvaluation']['DsePredictDiffAbs'] ?></th>
        </tr>
    </thead>
    <tbody>
    	<?php 
    	$hasData = false;
    	foreach($allSubjectDetail as $subjectID => $subjectInfo){ 
    	    if(!isset($predictResult[$subjectID][$searchStudentID])){
    	        continue;
    	    }
    	    
    	    $recordID = $predictResult[$subjectID][$searchStudentID]['RecordID'];
    	    $academicYearID = $predictResult[$subjectID][$searchStudentID]['AcademicYearID'];
    	    $score = $predictResult[$subjectID][$searchStudentID]['ExamScore'];
    	    $predictGrade = $predictResult[$subjectID][$searchStudentID]['PredictGrade'];
    	    $predictGradeOriginal = $predictResult[$subjectID][$searchStudentID]['PredictGradeOriginal'];
    	    $dseResult = $dseResultAssoc[$subjectID][$searchStudentID];
    	?>
            <tr>
            	<td><?= Get_Lang_Selection($subjectInfo['CH_SNAME'],$subjectInfo['EN_SNAME']); ?></td>
            	<td><?=(is_null($score))?Get_String_Display(''):number_format($score, SCORE_DECIMAL_PLACES) ?></td>
            	<td>
            		<?php if($predictGrade){ ?>
        				
                		<div class="predictGradeLoading exportHide" style="display:none;">
                			<?=$linterface->Get_Ajax_Loading_Image() ?>
                		</div>
                		<div class="predictGradeDisplay">
                    		<span class="currentGrade exportContent">
                    			<?=Get_String_Display($predictGrade) ?>
                    		</span>
                		    <span class="originalGrade exportContent">
                		    	<?= ( $predictGradeOriginal? '('.Get_String_Display($predictGradeOriginal).')' : '' ) ?>
                    		</span>
                    		
	                		<input type="button" class="exportHide printHide editPredictGrade" value="<?=$Lang['Btn']['Edit'] ?>" style="display:none;" />
                		</div>
                		<div class="predictGradeEdit exportHide printHide">
                			<form class="changePredictGradeForm">
    							<?=getSelectByValue($data=$DSE_GRADE,$tags=' name="DseGrade" class="dseGradeEdit"',$selected=$predictGrade,$all=0,$noFirst=1, $allName="", $ParQuoteValue=1) ?>
    							<input type="hidden" name="RecordID" value="<?=$recordID ?>" />
    							<input type="hidden" name="AcademicYearID" value="<?=$academicYearID ?>" />
    							<input type="hidden" name="SubjectID" value="<?=$subjectID ?>" />
    							<input type="hidden" name="StudentID" value="<?=$searchStudentID ?>" />
    							<input type="hidden" name="ActualDseResult" value="<?=$dseResult ?>" />
    							<input type="submit" value="<?=$Lang['Btn']['Submit'] ?>" />
    							<input type="button" class="predictGradeEditCancel" value="<?=$Lang['Btn']['Cancel'] ?>" />
                			</form>
                		</div>
            		<?php }else{ ?>
            			<?=Get_String_Display('') ?>
            		<?php } ?>
            	</td>
            	<td><?=Get_String_Display($dseResult) ?></td>
            	<td class="actualDseResult"><?=Get_String_Display($predictDiffArr[$subjectID][$searchStudentID]) ?></td>
            	<td class="actualDseResultAbs"><?=Get_String_Display($predictDiffAbsArr[$subjectID][$searchStudentID]) ?></td>
            </tr>
        <?php 
            $hasData = true;
    	} 
    	
    	if(!$hasData){
        ?>
        	<tr>
        		<td colspan="6" style="text-align: center;"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
        	</tr>
        <?php
    	}
        ?>
    </tbody>
    <?php if($hasData){ ?>
        <tfoot>
        	<tr>
        		<td style="border-right: 0">&nbsp;</td>
        		<td class="footerTitle" style="border-left: 0"><?=$Lang['SDAS']['DSEpredictionEvaluation']['NumOfPassSubject'] ?></td>
        		<td><?=$noPassSubject?></td>
        		<td class="footerTitle"><?=$Lang['SDAS']['averageValue'] ?></td>
        		<td><?=number_format($predictDiffAvg, DECIMAL_PLACES) ?></td>
        		<td><?=number_format($predictDiffAbsAvg, DECIMAL_PLACES) ?></td>
    		</tr>
        	<tr>
        		<td style="border-right: 0">&nbsp;</td>
        		<td class="footerTitle" style="border-left: 0"><?=$Lang['SDAS']['DSEpredictionEvaluation']['NumOfSubject'] ?></td>
        		<td><?=$noTotalSubject?></td>
        		<td>&nbsp;</td>
        		<td>&nbsp;</td>
        		<td>&nbsp;</td>
    		</tr>
        	<tr>
        		<td style="border-right: 0">&nbsp;</td>
        		<td class="footerTitle" style="border-left: 0"><?=$Lang['SDAS']['DSEpredictionEvaluation']['PassPercent'] ?></td>
        		<td><?=number_format($passPercent, DECIMAL_PLACES) ?>%</td>
        		<td>&nbsp;</td>
        		<td>&nbsp;</td>
        		<td>&nbsp;</td>
    		</tr>
    
        	<?php
        	$dseLevelArr = $libpf_exam->getDseLevelSequenceArr();
        	foreach($dseLevelArr as $level){
        	    $level = $libpf_exam->getDSEScore($level);
        	    if(!$levelCountArr[$level]){
        	        continue;
        	    }
        	    $levelTitle = str_replace('<!--Level-->', $libpf_exam->getDSELevel($level),$Lang['SDAS']['DSEpredictionEvaluation']['NumOfLevelStudent']);
        	?>
        		<tr>
            		<td style="border-right: 0">&nbsp;</td>
            		<td class="footerTitle" style="border-left: 0"><?=$levelTitle ?></td>
            		<td><?=$levelCountArr[$level] ?></td>
            		<td>&nbsp;</td>
            		<td>&nbsp;</td>
            		<td>&nbsp;</td>
        		</tr>
        	<?php 
        	}
        	?>
        	
        </tfoot>
    <?php } ?>
</table>





<?php 
#################################### Student report END ####################################
endif;
?>


<script>
$(function(){
	$('#resultDiv table').floatHeader();

	/**** UI START ****/
	$('#showEditButton').click(function(){
		$('.editPredictGrade').toggle();
	});
	/**** UI END ****/
	
	
	/**** Change DSE Predict Result START ****/
	$('.editPredictGrade').click(function(){
		$('#resultDiv table').find('.predictGradeDisplay').show();
		$('#resultDiv table').find('.predictGradeEdit').hide();
		$(this).closest('td').find('.predictGradeDisplay, .predictGradeEdit').toggle();
	});
	$('.predictGradeEditCancel').click(function(){
		$('#resultDiv table').find('.predictGradeDisplay').show();
		$('#resultDiv table').find('.predictGradeEdit').hide();
	});

	$('.changePredictGradeForm').submit(function(e){
		e.preventDefault();

		var $tr = $(this).closest('tr');
		var $cancel = $(this).find('.predictGradeEditCancel');

		var newDseGrade = $(this).find('[name="DseGrade"]').val();
		var currentDseGrade = $tr.find('.currentGrade').html().trim();

		if(newDseGrade == currentDseGrade){ // Not modified
			$cancel.click();
			return;
		}

		var currentScroll = $(document).scrollTop();
		$('#PrintBtn, #ExportBtn').hide();
		$('#resultDiv').html(ajaxImage);

		$.post('?t=academic.dse_prediction.ajax_edit_prediction2', $(this).serialize(), function(res){
			jsLoadReport().done(function(){
				$('#showEditButton').click();
				$(document).scrollTop(currentScroll);
			});
		});		
	});
	/**** Change DSE Predict Result END ****/
});
</script>











