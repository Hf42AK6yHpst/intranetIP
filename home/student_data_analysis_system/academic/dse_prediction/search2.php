<?php
################# Change Log [Start] ###########
# 2018-06-22 Pun [ip.2.5.9.7.1]
#	Added display filter
#
# 2017-12-08 Pun [ip.2.5.9.1.1]
#	Added edit dse prediction
#
# 2017-06-20 Pun
#	File Create
#
################## Change Log [End] ############

######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########

$accessRight = $lpf->getAssessmentStatReportAccessRight();
$subjectIdFilterArr = '';
if(!$accessRight['admin'] && count($accessRight['subjectPanel'])){
	$subjectIdFilterArr = array_keys($accessRight['subjectPanel']);
}

if(!$sys_custom['SDAS']['allTeacherAccessDSE']){
	if(!$accessRight['admin'] && count($accessRight['classTeacher'])){ //not admin and class teacher
		$IsClassTeacher = 1;
		//do the filtering in the ajax 
	// 	$Year_Arr = Get_Array_By_Key($accessRight['classTeacher'],'YearID');
	// 	$Year_Arr = implode(',',$Year_Arr);
	// 	$ClassID_arr = Get_Array_By_Key($accessRight['classTeacher'],'YearClassID');
	// 	$ClassID_arr = implode(',',$ClassID_arr);
	}
}
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "dse_prediction";
$CurrentTag = 'Evaluation';
include dirname(__FILE__).'/menu_tags.php';
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
// year info
$aYearAssocArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo() , 'AcademicYearID',array(Get_Lang_Selection('YearNameEN','YearNameB5')),1);
$yearArr = $libpf_exam->getDsePredictResultAcademicYearId();

$yearSelectArr = array();
foreach((array)$aYearAssocArr as $yearID => $yearName){
	if(in_array($yearID, (array)$yearArr)){
		$yearSelectArr[$yearID] = $yearName;
	}
}
$AcademicYearSelection = getSelectByAssoArray($yearSelectArr, "name='FromAcademicYearID' id='FromAcademicYearID'", Get_Current_Academic_Year_ID(), 0, 0, "");

$class_arr = $libpf_exam->getClassListByYearName(array(4,5,6));
$TeachingOnly = 0;
if(!$accessRight['admin']){
	$classTeacherAccess = Get_Array_By_Key($accessRight['classTeacher'], 'ClassTitleEN');
	$TeachingOnly = 1;
}

foreach((array) $class_arr as $_class_info){
	$formName = $_class_info['formName'];
	$_value = $_class_info['ClassTitleEN'];

	if(!$accessRight['admin'] && !in_array($_value,$classTeacherAccess) && !$sys_custom['SDAS']['allTeacherAccessDSE']){
		continue;
	}
	$formNameList[] = $formName;	
}


$formNameList = implode(",",array_unique((array)$formNameList));//get a string to js


$filterSubjectSelection = $libSCM_ui->Get_Subject_Selection ( 'SubjectID', $filterSubjectID, $OnChange = '', $noFirst = 0, $Lang['SDAS']['DSEpredictionEvaluation']['AllSubject'], '', $OnFocus = '', $FilterSubjectWithoutSG = 0, $IsMultiple = 0, $IncludeSubjectIDArr = $subjectIdFilterArr );


### Subject Group Selection START ###
if($accessRight['admin'] ){
    $Subject_Group_Selection = $libSCM_ui->Get_Current_Year_Subject_Group_SelectionBox();
}else{
    // 	if( count($accessRight['subjectGroup'])){
    // 		$Subject_Group_Selection = $libSCM_ui->Get_Current_Year_Subject_Group_SelectionBox($accessRight['subjectGroup']);
    // 	}
    if(count($accessRight['subjectPanel'])){
        foreach ($accessRight['subjectPanel'] as $subjectPanel =>$_subjectPanel){
            $subjectIDKey[] = $subjectPanel;
        }
    }
    $Subject_Group_Selection = $libSCM_ui->Get_Current_Year_Subject_Group_SelectionBox($accessRight['subjectGroup'],$subjectIDKey);
}
### Subject Group Selection END ###


//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:void(0)','',array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
######## UI Releated END ########



######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<!-- PrintButton Function -->
<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};
</script>

<script language="JavaScript">
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
var readyToSubmit = true;

$(document).ready( function() {
	//////// UI START ////////
	$('#FromAcademicYearID').change(function(){
		get_form_opt();
		js_Reload_Class_Selection();
		js_Reload_SubjectGroup_Selection();
	}).change();
	
	$('#display_class').click(function(){
		$('#yearRow, #classRow, #subjectRow').show();
		$('#groupRow, #classRow2, #studentRow').hide();
	});
	$('#display_group').click(function(){
		$('#yearRow, #classRow, #subjectRow, #classRow2, #studentRow').hide();
		$('#groupRow').show();
	});
	$('#display_student').click(function(){
		$('#yearRow, #classRow, #subjectRow, #groupRow').hide();
		$('#classRow2, #studentRow').show();
	});
	//////// UI END ////////

	$('#ExportBtn').click(function(){
		var $cloneTable = $("#resultDiv table").clone();
		$("#resultDiv table").find('.exportContent').each(function(){
			var data = $(this).html().trim();
			var $parent = $(this).parent();
			$(this).remove();
			$parent.append(data);
		});

		$("#resultDiv table").find('.exportHide').remove();
		
		$("#resultDiv table").tableExport({
			type: 'csv',
			fileName: 'prediction_evaluation_report'
		});

		$("#resultDiv table").html($cloneTable.html());
	});
});

function get_form_opt()
{
	var ay_id = $("#FromAcademicYearID").val();
	var form_id = $("#SelectForm").val();
	var is_ClassTeacher = '<?=$IsClassTeacher?>';
	var Year_Arr = '<?=$Year_Arr?>';
	var formNameList = <?php echo '["'.$formNameList.'"]'?>

	if(ay_id != ""){
		$("#yform_cell, #yclass_cell").html(ajaxImage);
		$.ajax({
			type: "GET",
			url: "index.php?t=ajax.ajax_get_form_opt",
			data: "ay_id="+ay_id+"&form_id="+form_id+"&formNameList="+formNameList+"&is_ClassTeacher="+is_ClassTeacher,
			success: function (msg) {
				$("#yform_cell").html(msg);
				$("#yclass_cell").html("<?=$Lang['General']['JS_warning']['SelectAForm']?>");
			}
		});
	}
}
function get_class_opt(){
	var ay_id = $("#FromAcademicYearID").val();
	var form_id = $("#SelectForm").val();
	var is_ClassTeacher = '<?=$IsClassTeacher?>';
	if(ay_id != ""){
		$("#yclass_cell").html(ajaxImage);
		$.ajax({
			type: "GET",
			url: "index.php?t=ajax.ajax_get_class_opt",
			data: "ay_id="+ay_id+"&form_id="+form_id+"&isClassTeacher="+is_ClassTeacher,
			success: function (msg) {
				$("#yclass_cell").html(msg);
			}
		});
	}
}

function js_Reload_SubjectGroup_Selection()
{
	$('div#GroupSelectionDiv').html(ajaxImage).load(
		"index.php?t=ajax.ajax_get_subject_group_selection_box", 
		{
			AcademicYearID: $("#FromAcademicYearID").val(),
		}
	);
}

function js_Reload_Class_Selection()
{
	$('div#StudentSelectionDiv').html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
	$('div#ClassSelectionDiv').html(ajaxImage).load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: $("#FromAcademicYearID").val(),
			YearID: $("#SelectForm").val(),
			SelectedYearClassID: '>',
			SelectionID: 'YearClassID',
			OnChange: "js_Reload_Student_Selection()",
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0,
			TeachingOnly: <?=$TeachingOnly?>
		}
	);
}
function js_Reload_Student_Selection()
{
	$('div#StudentSelectionDiv').html(ajaxImage).load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Student_Selection',		
			SelectedYearClassID: $('select#YearClassID').val(),
			SelectionID: 'StudentID',
			OnChange: "",
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		}
	);
}

function jsLoadReport()
{
	if(readyToSubmit){
		$( "#PrintBtn" ).hide();
		$('#ExportBtn').hide();

		if($('#display_class:checked').length > 0){
    		if( $('select#FromAcademicYearID').val() == '' ){
    			alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>');
    			return false;
    		}
    		if($('#SelectForm').val() == ''){
    			alert('<?=$Lang['General']['JS_warning']['SelectAForm']?>');
    			return false;
    		}
		}
		if( $('#display_student:checked').length > 0){
    		if( $('select#YearClassID').val() == '' ){
    			alert('<?=$Lang['General']['JS_warning']['SelectAForm']?>');
    			return false;
    		}
    		if( $('select#StudentID').val() == '' ){
    			alert('<?=$Lang['General']['JS_warning']['SelectStudent']?>');
    			return false;
    		}
		}
		
		readyToSubmit = false;
		$('#resultDiv').html(ajaxImage);
		var url = 'index.php?t=academic.dse_prediction.ajax_load_prediction_evaluation';
		
		return $.ajax({
			 type:"POST",
			 url: url,
			 data: $("#form1").serialize(),
			 success:function(responseText)
			 {
				 $('#resultDiv').html(responseText);
				 $('#PrintBtn').show();
				 $('#ExportBtn').show();
			 }
		}).done(function(){
			readyToSubmit = true;
		}); 
	}
}	

function selectAll(){
	var checked = $('#yclass_cell').find('#resultType_SelectAll').prop('checked');
	$('#yclass_cell').find('input').prop('checked', checked);
}
function unChecked(){
	$('#yclass_cell').find('#resultType_SelectAll').prop('checked', false);
}
</script>

<form id="form1" name="form1" method="POST" action="ajax_load_stat.php">

	<?=$html_tab_menu ?>
	<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top"><?=$AcademicYearSelection?></td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletext"><?=	$Lang['SDAS']['DSEprediction']['Display']?></span></td>
			<td valign="top">
				<input type="radio" id="display_class" name="display" value="class" checked />
				<label for="display_class"><?=$iPort['class']?></label>
				&nbsp;
				<input type="radio" id="display_group" name="display" value="group" />
				<label for="display_group"><?=$iPort['SubjectGroup'] ?></label>
				&nbsp;
				<input type="radio" id="display_student" name="display" value="student" />
				<label for="display_student"><?=$iPort['usertype_s']?></label>
			</td>
		</tr>
		
		<tr id="subjectRow">
			<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Subject']?></span></td>
			<td valign="top"><?=$filterSubjectSelection?></td>
		</tr>
		<tr id="yearRow">
			<td class="field_title"><?=	$Lang['General']['Form']?></td>
			<td id=yform_cell></td>
		</tr>
		<tr id="classRow">
			<td class="field_title"><?=	$Lang['General']['Class']?></td>
			<td id=yclass_cell></td>
		</tr>
		
		<tr id="groupRow" style="display: none;">
			<td class="field_title"><?=$iPort['SubjectGroup'] ?></td>
			<td valign="top"><div id="GroupSelectionDiv"></div></td>
		</tr>
		
		<tr id="classRow2" style="display: none;">
			<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
			<td valign="top"><div id="ClassSelectionDiv"></div></td>
		</tr>
		<tr id="studentRow" style="display: none;">
			<td class="field_title"><span class="tabletext"><?=$iPort['usertype_s']?></span></td>
			<td valign="top"><div id="StudentSelectionDiv"></div></td>
		</tr>
	</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="jsLoadReport();" />
	</div>

	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	
	<br clear="both"/>
	
	
</form>
	<div id="PrintArea">
		<div id="resultDiv" class="chart_tables"></div>
	</div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>