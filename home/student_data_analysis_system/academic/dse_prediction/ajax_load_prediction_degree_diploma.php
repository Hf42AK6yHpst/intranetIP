<?php
/*
 ###################
 ## 2018-03-09 Pun [136473] [ip.2.5.9.3.1]
 ## Added display Higher Diploma for Elective Broken
 ## 2017-12-15 Pun [ip.2.5.9.1.1]
 ## New File
 ###################
 */

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$scm = new subject_class_mapping();
$json = new JSON_obj();


#### Get current data START ####
$sql = "SELECT 
    * 
FROM 
    EXAM_DSE_PREDICT_SCORE 
WHERE 
    AcademicYearID='{$AcademicYearID}'
AND
    ParentSubjectID IS NULL
";
//                                                             ORDER BY FIELD(SubjectID, 1,8,128,127,12,11,10)
$rs = $libpf_exam->returnResultSet($sql);
$predictScoreArr = $rs;
#### Get current data END ####


#### Get Student data START ####
$userIDArr = Get_Array_By_Key($rs, 'StudentID');
$studentInfoArr =  $libpf_exam->getStudentsYearInfoByID($userIDArr, $AcademicYearID);
$studentAssoc = BuildMultiKeyAssoc($studentInfoArr, 'UserID', array());
#### Get Student data END ####


#### Get Subject Info START ####
$rs = $scm->Get_Subject_List();
$subjectCodeArr = BuildMultiKeyAssoc($rs, array('SubjectID') , array('WEBSAMSCode'), $SingleValue=1);
#### Get Subject Info END ####


#### Calculate score START ####
$examPredictScoreArr = array();
foreach($predictScoreArr as $scoreInfo){
    $subjectId = $scoreInfo['SubjectID'];
    $studentId = $scoreInfo['StudentID'];
    $predictGrade = $scoreInfo['PredictGrade'];
    $subjectCode = $subjectCodeArr[$subjectId];
    $dseScore = $libpf_exam->getDSEScore($predictGrade);
    
    $examPredictScoreArr[$studentId]['Score'][$subjectCode] = $dseScore;
}

#### Calculate score END ####


#### Calculate pass START ####
$conditionArr = array(
    'Degree' => array(
        'Compulsory' => '3,3,2,2',
        'CompulsoryCode' => '080,165,22S,265',
        'Elective' => '2'
    ),
    'Diploma' => array(
        'Compulsory' => '2,2',
        'CompulsoryCode' => '080,165',
        'Elective' => '2,2,2'
    ),
);

foreach($examPredictScoreArr as $studentId => $_scoreInfo){
    /* * /
	$_scoreInfo['Score'] = array(
	    '080' => rand(1,3),//'3',
	    '165' => rand(1,3),//'3',
	    '22S' => rand(1,3),//'2',
	    '265' => '1',
	    
	    '045' => rand(1,3),//'2',
	    '070' => rand(1,3),//'2',
	    '315' => rand(1,3),//'2',
	);
	/* */
    arsort($_scoreInfo['Score'], SORT_NUMERIC);
	$examPredictScoreArr[$studentId]['Score'] = $_scoreInfo['Score'];
    foreach($conditionArr as $conditionType => $conditionInfo){
        $scoreInfo = $_scoreInfo['Score'];
        
        #### Get comuplsory/elective subject score START ####
        $compulsoryCodeArr = explode(',', $conditionInfo['CompulsoryCode']);
        $comuplsorySubjectScoreInfo = array();
        $electiveSubjectScoreInfo = array();
        foreach($scoreInfo as $subjectCode => $score){
            if(in_array($subjectCode,$compulsoryCodeArr)){
                $comuplsorySubjectScoreInfo[$subjectCode] = $score;
            }else{
                $electiveSubjectScoreInfo[$subjectCode] = $score;
            }
        }
        #### Get comuplsory/elective subject score END ####

        #### Get pass grade array START ####
        $condCompulsoryArr = explode(',', $conditionInfo['Compulsory']);
        $conditionElectiveArr = explode(',', $conditionInfo['Elective']);
        #### Get pass grade array END ####
        
        #### Check comuplsory subject pass START ####
        $chiPassGrade = $condCompulsoryArr[0];
        $engPassGrade = $condCompulsoryArr[1];
        $mathPassGrade = (count($condCompulsoryArr) >= 3)? $condCompulsoryArr[2] : min($conditionElectiveArr);
        $lsPassGrade = (count($condCompulsoryArr) >= 4)? $condCompulsoryArr[3] : min($conditionElectiveArr);
        
        $isChiPass = ($scoreInfo['080'] >= $chiPassGrade);
        $isEngPass = ($scoreInfo['165'] >= $engPassGrade);
        $isMathPass = ($scoreInfo['22S'] >= $mathPassGrade);
        $isLsPass = ($scoreInfo['265'] >= $lsPassGrade);

        $compulsoryPassCount = 0;
        if($conditionType == 'Degree'){
            $compulsoryPassCount += ($isChiPass)?1:0;
            $compulsoryPassCount += ($isEngPass)?1:0;
            $compulsoryPassCount += ($isMathPass)?1:0;
            $compulsoryPassCount += ($isLsPass)?1:0;
        }else{
            $compulsoryPassCount += ($isChiPass)?1:0;
            $compulsoryPassCount += ($isEngPass)?1:0;
        }
        
        /* * /
        $examPredictScoreArr[$studentId]['isChiPass'][$conditionType] = $isChiPass;
        $examPredictScoreArr[$studentId]['isEngPass'][$conditionType] = $isEngPass;
        $examPredictScoreArr[$studentId]['isMathPass'][$conditionType] = $isMathPass;
        $examPredictScoreArr[$studentId]['isLsPass'][$conditionType] = $isLsPass;
        /* */
        #### Check comuplsory subject pass END ####
        
        #### Check elective subject pass START ####
        $isElectivePass = true;
        $electivePassCount = 0;
        $electiveFailCount = 0;
        $index = 0;
        foreach($electiveSubjectScoreInfo as $subjectCode=>$electiveScore){
            if($index == count($conditionElectiveArr)){
                break;
            }
            
            $isElectivePass = ($electiveScore >= $conditionElectiveArr[$index++]);
            if($isElectivePass){
                $electivePassCount++;
            }
        }
        
        $index = 0;
        foreach($electiveSubjectScoreInfo as $subjectCode=>$electiveScore){
            if($subjectCode == '22S' || $subjectCode == '265'){
                $index++;
                continue;
            }
            
            $isElectiveFail = ($electiveScore < $conditionElectiveArr[$index++]);
            if($isElectiveFail){
                $electiveFailCount++;
            }
        }
        #### Check elective subject pass END ####
        
        
        #### Check one subject not pass START ####
        $isOneSubjectFail = (
            (
                $compulsoryPassCount == (count($condCompulsoryArr) - 1) &&
                $electivePassCount == (count($conditionElectiveArr))
            ) || (
                $compulsoryPassCount == (count($condCompulsoryArr)) &&
                $electivePassCount == (count($conditionElectiveArr) - 1)
            )
        );
        //$examPredictScoreArr[$studentId]['isOneSubjectFail'][$conditionType] = $isOneSubjectFail;
        #### Check one subject not pass END ####
        
        
        #### Check overall pass START ####
        if($conditionType == 'Degree'){
            $examPredictScoreArr[$studentId]['isOverallPass'][$conditionType] = (
                $isChiPass &&
                $isEngPass &&
                $isMathPass &&
                $isLsPass &&
                $isElectivePass &&
                (count($electiveSubjectScoreInfo) >= count($conditionElectiveArr))
            );
        }else{
            $examPredictScoreArr[$studentId]['isOverallPass'][$conditionType] = (
                $isChiPass &&
                $isEngPass &&
                $isElectivePass &&
                (count($electiveSubjectScoreInfo) >= count($conditionElectiveArr))
            );
        }
        #### Check overall pass END ####
        
        
        #### Check Chinese focus START ####
        $examPredictScoreArr[$studentId]['isChiFocus'][$conditionType] = (
            !$isChiPass &&
            $isOneSubjectFail
        );
        #### Check Chinese focus END ####
        
        
        #### Check English focus START ####
        $examPredictScoreArr[$studentId]['isEngFocus'][$conditionType] = (
            !$isEngPass &&
            $isOneSubjectFail
        );
        #### Check English focus END ####
        
        
        #### Check Math focus START ####
        $examPredictScoreArr[$studentId]['isMathFocus'][$conditionType] = (
            $isChiPass &&
            $isEngPass &&
            !$isMathPass &&
            $isOneSubjectFail
        );
        #### Check Math focus END ####
        
        
        #### Check Ls focus START ####
        $examPredictScoreArr[$studentId]['isLsFocus'][$conditionType] = (
            $isChiPass &&
            $isEngPass &&
            !$isLsPass &&
            $isOneSubjectFail
        );
        #### Check Ls focus END ####
        
        
        #### Check Chi & Eng broken START ####
        if($conditionType == 'Degree'){
            $examPredictScoreArr[$studentId]['isChiEngBroken'][$conditionType] = (
                !$isChiPass &&
                !$isEngPass &&
                $isMathPass &&
                $isLsPass &&
                $isElectivePass
            );
        }else{
            $examPredictScoreArr[$studentId]['isChiEngBroken'][$conditionType] = (
                !$isChiPass &&
                !$isEngPass &&
                $isElectivePass
            );
        }
        #### Check Chi & Eng broken END ####


        #### Check Elective broken START ####
        $examPredictScoreArr[$studentId]['isElectiveBroken'][$conditionType] = (
            !$isElectivePass &&
            $isOneSubjectFail &&
            $electiveFailCount == 1
        );
        #### Check Elective broken END ####
        
        
    } // End foreach($conditionArr as $conditionType => $conditionInfo){ ... }
    
    
    /* Debug code START * /
    if($studentId==26572){
        $examPredictScoreArr[$studentId]['debug']['Chinese'] = $_scoreInfo['Score']['080'];
        $examPredictScoreArr[$studentId]['debug']['English'] = $_scoreInfo['Score']['165'];
        $examPredictScoreArr[$studentId]['debug']['Math'] = $_scoreInfo['Score']['22S'];
        $examPredictScoreArr[$studentId]['debug']['Ls'] = $_scoreInfo['Score']['265'];
        $examPredictScoreArr[$studentId]['debug']['Bio'] = $_scoreInfo['Score']['045'];
        $examPredictScoreArr[$studentId]['debug']['Chem'] = $_scoreInfo['Score']['070'];
        $examPredictScoreArr[$studentId]['debug']['Phy'] = $_scoreInfo['Score']['315'];
        unset($examPredictScoreArr[$studentId]['Score']);
        echo ("({$studentAssoc[$studentId]['ClassName']} - {$studentAssoc[$studentId]['ClassNumber']}) {$studentAssoc[$studentId]['Name']}<br />");
    	debug_r($examPredictScoreArr[$studentId]);
    }
    /* Debug code END */

} // End foreach($examPredictScoreArr as $studentId => $_scoreInfo){ ... }
#### Calculate pass END ####


#### Output START ####
$displayFieldCount = count($display);
?>

<style>
.r_3{ border-right-width: 3px !important; }
.r_2{ border-right-width: 2px !important; }
.r_dot{ border-right-style: dotted !important; }
.fail_focus{ 
    background-color: #ff000030 !important; 
    color: red !important;
    text-align: center !important;
    font-weight: bold !important;
}
.center{
    text-align: center !important;
}
</style>

<?= $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['menu']['DsePredictionDegreeDiploma']) ?>
<table class="common_table_list_v30 view_table_list_v30" style="text-align:center;">
    <thead>
        <tr>
            <th colspan="3" class="r_3">&nbsp;</th>
            <th colspan="7" class="r_3"><?=$Lang['SDAS']['PredictDegreeDiploma']['DseScoreAfterAdj'] ?></th>
            <th colspan="<?=$displayFieldCount?>" class="r_3"><?=$Lang['SDAS']['PredictDegreeDiploma']['MeetRequirement'] ?></th>
            <th colspan="<?=$displayFieldCount?>" class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['ChiFocus'] ?></th>
            <th colspan="<?=$displayFieldCount?>" class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['EngFocus'] ?></th>
            <th colspan="<?=$displayFieldCount?>" class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['MathFocus'] ?></th>
            <th colspan="<?=$displayFieldCount?>" class="r_3"><?=$Lang['SDAS']['PredictDegreeDiploma']['LsFocus'] ?></th>
            <th colspan="<?=$displayFieldCount?>" class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['ChiEngFail'] ?></th>
            
        	<th colspan="<?=$displayFieldCount?>" class=""><?=$Lang['SDAS']['PredictDegreeDiploma']['ElectiveFail'] ?></th>
        </tr>
        <tr>
            <th><?=$Lang['General']['Class'] ?></th>
            <th><?=$Lang['General']['ClassNo_ShortForm'] ?></th>
            <th class="r_3"><?=$Lang['AccountMgmt']['StudentName'] ?></th>
            <th><?=$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Chi'] ?></th>
            <th><?=$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Eng'] ?></th>
            <th><?=$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Math'] ?></th>
            <th><?=$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Ls'] ?></th>
            <th><?=$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Elective'] ?> 1</th>
            <th><?=$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Elective'] ?> 2</th>
            <th class="r_3"><?=$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Elective'] ?> 3</th>
            
            <?php 
            if(in_array('degree', $display)){
            ?>
            	<th><?=$conditionArr['Degree']['Compulsory'] ?>,<?=$conditionArr['Degree']['Elective'] ?></th>
            <?php 
            }
            if(in_array('diploma', $display)){ 
            ?>
            	<th class="r_3"><?=$conditionArr['Diploma']['Compulsory'] ?>,<?=$conditionArr['Diploma']['Elective'] ?></th>
            <?php 
            } 
            ?>
            
            <?php 
            if(in_array('degree', $display)){
            ?>
            	<th><?=$Lang['SDAS']['PredictDegreeDiploma']['Degree'] ?></th>
            <?php 
            }
            if(in_array('diploma', $display)){ 
            ?>
            	<th class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] ?></th>
            <?php 
            } 
            ?>

            <?php 
            if(in_array('degree', $display)){
            ?>
            	<th><?=$Lang['SDAS']['PredictDegreeDiploma']['Degree'] ?></th>
            <?php 
            }
            if(in_array('diploma', $display)){ 
            ?>
            	<th class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] ?></th>
            <?php 
            } 
            ?>

            <?php 
            if(in_array('degree', $display)){
            ?>
            	<th><?=$Lang['SDAS']['PredictDegreeDiploma']['Degree'] ?></th>
            <?php 
            }
            if(in_array('diploma', $display)){ 
            ?>
            	<th class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] ?></th>
            <?php 
            } 
            ?>

            <?php 
            if(in_array('degree', $display)){
            ?>
            	<th><?=$Lang['SDAS']['PredictDegreeDiploma']['Degree'] ?></th>
            <?php 
            }
            if(in_array('diploma', $display)){ 
            ?>
            	<th class="r_3"><?=$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] ?></th>
            <?php 
            } 
            ?>

            <?php 
            if(in_array('degree', $display)){
            ?>
            	<th><?=$Lang['SDAS']['PredictDegreeDiploma']['Degree'] ?></th>
            <?php 
            }
            if(in_array('diploma', $display)){ 
            ?>
            	<th class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] ?></th>
            <?php 
            } 
            ?>

            <?php 
            if(in_array('degree', $display)){
            ?>
            	<th><?=$Lang['SDAS']['PredictDegreeDiploma']['Degree'] ?></th>
            <?php 
            } 
            if(in_array('diploma', $display)){ 
            ?>
            	<th class="r_2 r_dot"><?=$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] ?></th>
            <?php 
            } 
            ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($studentAssoc as $studentId => $studentInfo){ 
            $scoreInfo = $examPredictScoreArr[$studentId]['Score'];
            $passInfo = $examPredictScoreArr[$studentId]['PassInfo'];
        ?>
        	<tr>
        		<!-- #### Student Info START #### -->
        		<td><?=$studentInfo['ClassName'] ?></td>
        		<td><?=$studentInfo['ClassNumber'] ?></td>
        		<td class="r_3" data-student-id="<?=$studentId ?>"><?=$studentInfo['Name'] ?></td>
        		<!-- #### Student Info END #### -->
        		
        		
        		<!-- #### Subject score START #### -->
        		<td><?=$scoreInfo['080'] ?></td>
        		<td><?=$scoreInfo['165'] ?></td>
        		<td><?=$scoreInfo['22S'] ?></td>
        		<td><?=$scoreInfo['265'] ?></td>
        		
        		<?php
        		$electiveDisplayCount = 0;
        		foreach($scoreInfo as $subjectCode => $score){
        		    if(in_array($subjectCode, array('080', '165', '22S', '265'))){
        		        continue;
        		    }
        		    $class = ($electiveDisplayCount==2)?'r_3':'';
        		?>
        			<td class="<?=$class ?>" data-subject-code="<?=$subjectCode ?>"><?=$score ?></td>
        		<?php
        		    if($electiveDisplayCount++ == 3){
        		        break;
        		    }
        		}
        		for($i=$electiveDisplayCount;$i<3;$i++){
        		    $class = ($electiveDisplayCount==2)?'r_3':'';
        		    echo '<td class="'.$class.'">&nbsp;</td>';
        		}
        		?>
        		<!-- #### Subject score END #### -->
        		
        		
        		<!-- #### Pass START #### -->
                <?php 
                if(in_array('degree', $display)){
                ?>
            		<td class="center"><?=($examPredictScoreArr[$studentId]['isOverallPass']['Degree'])? 'Y' : '&nbsp;' ?></td>
                <?php 
                }
                if(in_array('diploma', $display)){ 
                ?>
            		<td class="center r_3"><?=($examPredictScoreArr[$studentId]['isOverallPass']['Diploma'])? 'Y' : '&nbsp;' ?></td>
        		<?php
                }
                ?>
        		<!-- #### Pass End #### -->
        		
        		
        		<!-- #### Focus Chi START #### -->
        		<?php
                if(in_array('degree', $display)){
            		if($examPredictScoreArr[$studentId]['isChiFocus']['Degree']){
                		echo '<td class="fail_focus">D</td>';
            		}else{
            		    echo '<td>&nbsp;</td>';
            		}
        		}

        		if(in_array('diploma', $display)){
            		if($examPredictScoreArr[$studentId]['isChiFocus']['Diploma']){
                		echo '<td class="fail_focus r_2 r_dot">H</td>';
            		}else{
            		    echo '<td class="r_2 r_dot">&nbsp;</td>';
            		}
        		}
        		?>
        		<!-- #### Focus Chi END #### -->
        		
        		
        		<!-- #### Focus Eng START #### -->
        		<?php
                if(in_array('degree', $display)){
            		if($examPredictScoreArr[$studentId]['isEngFocus']['Degree']){
                		echo '<td class="fail_focus">D</td>';
            		}else{
            		    echo '<td>&nbsp;</td>';
            		}
        		}

        		if(in_array('diploma', $display)){
            		if($examPredictScoreArr[$studentId]['isEngFocus']['Diploma']){
                		echo '<td class="fail_focus r_2 r_dot">H</td>';
            		}else{
            		    echo '<td class="r_2 r_dot">&nbsp;</td>';
            		}
        		}
        		?>
        		<!-- #### Focus Eng END #### -->
        		
        		
        		<!-- #### Focus Math START #### -->
        		<?php
                if(in_array('degree', $display)){
            		if($examPredictScoreArr[$studentId]['isMathFocus']['Degree']){
                		echo '<td class="fail_focus">D</td>';
            		}else{
            		    echo '<td>&nbsp;</td>';
            		}
        		}
        		
        		if(in_array('diploma', $display)){
            		if($examPredictScoreArr[$studentId]['isMathFocus']['Diploma']){
                		echo '<td class="fail_focus r_2 r_dot">H</td>';
            		}else{
            		    echo '<td class="r_2 r_dot">&nbsp;</td>';
            		}
        		}
        		?>
        		<!-- #### Focus Math END #### -->
        		
        		
        		<!-- #### Focus LS START #### -->
        		<?php
                if(in_array('degree', $display)){
            		if($examPredictScoreArr[$studentId]['isLsFocus']['Degree']){
                		echo '<td class="fail_focus">D</td>';
            		}else{
            		    echo '<td>&nbsp;</td>';
            		}
        		}

        		if(in_array('diploma', $display)){
            		if($examPredictScoreArr[$studentId]['isLsFocus']['Diploma']){
                		echo '<td class="fail_focus r_3">H</td>';
            		}else{
            		    echo '<td class="r_3">&nbsp;</td>';
            		}
        		}
        		?>
        		<!-- #### Focus LS END #### -->
        		
        		
        		<!-- #### Chi Eng Broken START #### -->
        		<?php
                if(in_array('degree', $display)){
            		if($examPredictScoreArr[$studentId]['isChiEngBroken']['Degree']){
                		echo '<td class="fail_focus">L</td>';
            		}else{
            		    echo '<td>&nbsp;</td>';
            		}
        		}

        		if(in_array('diploma', $display)){
            		if($examPredictScoreArr[$studentId]['isChiEngBroken']['Diploma']){
                		echo '<td class="fail_focus r_2 r_dot">H</td>';
            		}else{
            		    echo '<td class="r_2 r_dot">&nbsp;</td>';
            		}
        		}
        		?>
        		<!-- #### Chi Eng Broken END #### -->
        		
        		
        		<!-- #### Elective Broken START #### -->
        		<?php
                if(in_array('degree', $display)){
            		if($examPredictScoreArr[$studentId]['isElectiveBroken']['Degree']){
                		echo '<td class="">E</td>';
            		}else{
            		    echo '<td>&nbsp;</td>';
            		}
        		}
                if(in_array('diploma', $display)){
            		if($examPredictScoreArr[$studentId]['isElectiveBroken']['Diploma']){
                		echo '<td class="">E</td>';
            		}else{
            		    echo '<td>&nbsp;</td>';
            		}
        		}
        		?>
        		<!-- #### Elective Broken END #### -->
        	</tr>
    	<?php 
        } 
        ?>
    </tbody>
</table>

<script>
$(function(){
	$('#resultDiv table').floatHeader();
});
</script>







