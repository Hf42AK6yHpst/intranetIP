<?php 
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$lpf = new libportfolio();


$FromAcademicYearID = $_POST['FromAcademicYearID'];
$ToAcademicYearID = $_POST['ToAcademicYearID'];

$AcademicYearArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID); 

// return html & script here
switch($_POST['task']){
	case 'baisc_stat':
		$result = $libpf_exam->getJupasOfferReport($AcademicYearArr);
		break;
	case 'jupas_vs_dse':
		$result = $libpf_exam->getJupasVsDseReport($FromAcademicYearID);
		break;
	case 'jupas_vs_dse_avg':

		$result = $libpf_exam->getJupasVsDseAvgReport($AcademicYearArr);
		break;	
	case 'jupas_twgh':
		if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
			include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
			$lcees = new libcees();
			$dataArr = $lcees->getJUPASfromCentral($_POST['FromAcademicYearID'], $_POST['ToAcademicYearID']);
			
			$html = '';
// 			$html .= '<span class="tabletextremark" style="text-align:right; display:block;">'.$Lang['General']['LastUpdatedTime'].': '.$lastModified.'</span>';
			$html .= '<table id="dataTable" class="tablesorter" width="100%">';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th rowspan="2">'.$Lang['SDAS']['Jupas']['Institution'].'</th>';
			$html .= '<th rowspan="2">'.$Lang['SDAS']['Jupas']['ProgrammeName'].'</th>';
			$html .= '<th rowspan="2">'.$Lang['SDAS']['Jupas']['NumOffer'].'</th>';
			$html .= '<th colspan="3">Best 5</th>';
			$html .= '<th colspan="3">4C + 1E</th>';
			$html .= '<th colspan="3">4C + 2E</th>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Min'] . '</th>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Avg'] . '</th>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Max'] . '</th>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Min'] . '</th>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Avg'] . '</th>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Max'] . '</th>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Min'] . '</th>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Avg'] . '</th>';
			$html .= '<th>' . $Lang ['SDAS'] ['Jupas'] ['JupasVsDseAvg'] ['Max'] . '</th>';
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			if (count ( $dataArr ) == 0 ) {
				$html .= '<tr>';
				$html .= '<td colspan="12">' . $Lang ['SDAS'] ['DSEstat'] ['Alert'] ['NoData'] . '</td>';
				$html .= '</tr>';
			} else {
				$allJupasInfoArr = BuildMultiKeyAssoc($libpf_exam->getAllJupasProgrammeInfo(),'JupasCode' );
				foreach ( (array) $dataArr as $_jupasCode => $_infoArr ) {
					$html .= '<tr>';
// 					$html .= '<td>' . $_jupasCode . '</td>';
					$html .= '<td>' . $Lang['SDAS']['Jupas']['InstitutionName'][$allJupasInfoArr[$_jupasCode]['Institution']] . '</td>';
					$html .= '<td>' . $_jupasCode. ' - '.$allJupasInfoArr[$_jupasCode]['ProgrammeFullName'] . '</td>';
					foreach ( ( array ) $_infoArr as $__cellValue ) {
						$html .= '<td>' . $__cellValue . '</td>';
					}
					$html .= '</tr>';
				}
			}
			$html .= '</tbody>';
			$html .= '</table>';
			$result = $html;
		}
		break;
}


?>
<?php
// basic stat contain rowspan => cannot use tablesort
if($_POST['task'] != 'baisc_stat'){?>
<style>
.tablesorter-default .header,
.tablesorter-default .tablesorter-header {
	padding: 4px 20px 4px 4px;
	cursor: pointer;
	background-image: url(data:image/gif;base64,R0lGODlhFQAJAIAAAP///////yH5BAEAAAEALAAAAAAVAAkAAAIXjI+AywnaYnhUMoqt3gZXPmVg94yJVQAAOw==);
	background-position: center right;
	background-repeat: no-repeat;
}
.tablesorter-default .headerSortUp,
.tablesorter-default .tablesorter-headerSortUp,
.tablesorter-default .tablesorter-headerAsc {
	background-image: url(data:image/gif;base64,R0lGODlhFQAEAIAAAP///////yH5BAEAAAEALAAAAAAVAAQAAAINjI8Bya2wnINUMopZAQA7);
	color: #fff;
}
.tablesorter-default .headerSortDown,
.tablesorter-default .tablesorter-headerSortDown,
.tablesorter-default .tablesorter-headerDesc {
	color: #fff;
	background-image: url(data:image/gif;base64,R0lGODlhFQAEAIAAAP///////yH5BAEAAAEALAAAAAAVAAQAAAINjB+gC+jP2ptn0WskLQA7);
}
</style>
<script type="text/javascript" src="/templates/jquery/tablesorter/jquery.tablesorter.min.js"></script>
<script>
$(function(){
	$("#dataTable").tablesorter();
});
</script>
<?php }?>
<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// switch of report [Start] ////////////////////////////////////////////
//////////////////////////////////////////// switch of report [End] ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo $result;
exit;
?>