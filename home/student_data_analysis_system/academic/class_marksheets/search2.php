<?php
// using : Isaac
/**
 * Change Log:
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2017-06-27 Villa
 * -	Open the open with the reference of search.php only for MonitoringGroup View
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

$ayterm_selection_html = '';
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);
######## Access Right END ########

######## Page Setting START ########
$CurrentPage = "class_subject_performance_summary";
if(!($accessRight['admin']||$accessRight['classTeacher'])){
	$TAGS_OBJ[] = array($Lang['SDAS']['menu']['ClassPerformance'],"javascript: void(0);");
}else{
	$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['IndividualYear'], 'index.php?t=academic.class_subject_performance_summary.search');
	$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossYear'],"javascript: void(0);", true);
}
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
#### Get Year selection START ####
// $FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);
$objYear = new Year();
$allClassArr = $objYear->Get_All_Classes();
$allYearArr = BuildMultiKeyAssoc($allClassArr, array('YearID') , array('YearName'), $SingleValue=1);
$FormSelection = getSelectByAssoArray($allYearArr,' id="YearID" name="YearID" onchange="js_Changed_Form_Selection(this.value);"', $selected=$YearID, $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
#### Get Year selection END ####


$YearArr = $li_pf->returnAssessmentYear();
$FromAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FromAcademicYearID' id='FromAcademicYearID' onchange=\"\" ", $currentAcademicYearID, 0, 0, "", 2);
$ToAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='ToAcademicYearID' id='ToAcademicYearID' onchange=\"\" ", $currentAcademicYearID, 0, 0, "", 2);

// if($accessRight['admin'] || !$isSubjectPanelView){
$FromSubjectSelection = $libSCM_ui->Get_Subject_Selection('FromSubjectID', $FromSubjectID, $OnChange='', $noFirst=1, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);
//Print Button
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

### TypeRadioBtn START ###
if(count($accessRight['classTeacher'])|| $accessRight['admin'] || count($accessRight['subjectPanel'])){
	$TypeRadioBtn = $linterface->Get_Radio_Button('TypeBtn_Class','TypeBtn','Class','','',$iPort['Class'],'js_Changed_Type(this.value)');
}
if(count($accessRight['subjectTeacher'])||count($accessRight['subjectPanel'])|| $accessRight['admin']){
	$TypeRadioBtn .= "&nbsp";
	$TypeRadioBtn .= $linterface->Get_Radio_Button('TypeBtn_SubjectClass','TypeBtn','SubjectClass','','',$iPort['SubjectGroup'],'js_Changed_Type(this.value)');
}
if(count($accessRight['MonitoringGroupPIC'])|| $accessRight['admin']){
	$TypeRadioBtn .= "&nbsp";
	$TypeRadioBtn .= $linterface->Get_Radio_Button('TypeBtn_MonitoringGroup','TypeBtn','MonitoringGroup',1,'',$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] ,'js_Changed_Type(this.value)');
}
### TypeRadioBtn END ###

$MonintoringGroup = $libSDAS->getMonitoringGroup('',$CurrentAcademicYearID);
$MonintoringGroup_arr = BuildMultiKeyAssoc($MonintoringGroup, 'GroupID');
$MonintoringGroupSelection = '<select id="MonitoringGroupPIC" name="MonitoringGroupPIC">';
foreach ((array)$accessRight['MonitoringGroupPIC'] as $_GroupID){
	if($MonintoringGroup_arr[$_GroupID]){
		$MonintoringGroupSelection .= '<option value="'.$_GroupID.'">'.Get_Lang_Selection($MonintoringGroup_arr[$_GroupID]['GroupNameCH'], $MonintoringGroup_arr[$_GroupID]['GroupNameEN']).'</option>';
	}
}
$MonintoringGroupSelection .= '</select>';
$TypeBtn = 'MonitoringGroup';
######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {

	
	$(".chart_tables table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Class Marksheets'
	});

}
</script>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFromAcademicYearID = '<?=$FromAcademicYearID?>';
var jsCurFromYearTermID = '<?=$FromYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if (jsCurFromAcademicYearID == '') {
		jsCurFromAcademicYearID = $('select#FromAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	 $("#filterMark").attr("checked", "checked");
	
});

function js_Changed_Type(jsType){
	if(jsType == 'MonitoringGroup'){
		window.location.href = 'index.php?t=academic.class_marksheets.search_monitoringview';
	}else{
		window.location.href = 'index.php?t=academic.class_marksheets.search';
	}
}


function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_DBTable()
{
	$( "#PrintBtn" ).hide();
	$( "#ExportBtn" ).hide();
	if($('#FromAcademicYearID').val() == '' || $('#ToAcademicYearID').val() == ''){
		alert('<?=$Lang['General']['WarningArr']['PleaseSelectPeriod']?>');
		return;
	}

	if($('#FromAcademicYearID')[0].selectedIndex < $('#ToAcademicYearID')[0].selectedIndex){
		alert('<?=$Lang['General']['WarningArr']['EndPeriodCannotEarlierThanStartPeriod']?>');
		return;
	}

	if($('.displayField:checked').length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectColumn']?>');
		return;
	}

	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_get_mark_analysis.php", 
		$('#form1').serialize(),
		function(ReturnData)
		{
			$( "#PrintBtn" ).show();
			$( "#ExportBtn" ).show();
			
		}
	);
}

function js_Print()
{
	document.form1.submit();
}

</script>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" method="POST" action="stats_print.php" target="iport_stats">
	<?=$html_tab_menu ?>
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
					<?php if($accessRight['admin'] ||
					count($accessRight['subjectPanel']) || 
					(	count($accessRight['classTeacher'])  && count($accessRight['subjectTeacher']) 
						|| (count($accessRight['classTeacher'])  && count($accessRight['MonitoringGroupPIC'])) 
						|| (count($accessRight['MonitoringGroupPIC'])  && count($accessRight['subjectTeacher'])) 
					)
					){?>
						<tr>
							<td class="field_title"><span class="tabletext"><?=	$Lang['SDAS']['Mode'] ?></span></td>
							<td valign="top"><?=$TypeRadioBtn?></td>
						</tr>					
						<?php }?>
					<tr style="display: none;">
						<td><input type="hidden" name="TypeBtn" value="<?=$TypeBtn?>"></td>
					</tr>
			<!-- MonitoringGroup -->
			<?php if(count($accessRight['MonitoringGroupPIC'])||$accessRight['admin']){?>
			<tr id='MonitoringGroup' style='display: <?=$display2?>'>
				<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] ?></span></td>
				<td><?=$MonintoringGroupSelection?></td>
			<?php }?>
						
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["period"]?></span></td>
				<td valign="top"><?=$Lang['General']['From']?> <?=$FromAcademicYearSelection?> <?=$Lang['General']['To']?> <?=$ToAcademicYearSelection?></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
				<td valign="top">
					<div id="subjectSelection">
						<?=$FromSubjectSelection?>
					</div>
				</td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['display']?></span></td>
				<td valign="top">
					<input type="checkbox" class="filterAll" id="filterAllA"/>
					<label for="filterAllA"><?=$Lang['Btn']['SelectAll']?></label>&nbsp;&nbsp;&nbsp;
					
					<input type="checkbox" id="filterMark" name="filterColumn[]" value="filterMark" class="displayField" />
					<label for="filterMark"><?=$Lang['iPortfolio']["Score"]?></label>&nbsp;
					
					<?php if($sys_custom['iPf']['Report']['AssessmentStatisticReport']['showMarksDifferent']){ ?>
					<input type="checkbox" id="filterMarkDiff" name="filterColumn[]" value="filterMarkDiff" class="displayField" />
					<label for="filterMarkDiff"><?=$Lang['iPortfolio']["MarkDifference"]?></label>&nbsp;
					<?php } ?>
					
					<input type="checkbox" id="filterStandardScore" name="filterColumn[]" value="filterStandardScore" class="displayField" />
					<label for="filterStandardScore"><?=$Lang['iPortfolio']["StandardScore"]?></label>&nbsp;
					
					<input type="checkbox" id="filterStandardScoreDiff" name="filterColumn[]" value="filterStandardScoreDiff" class="displayField" />
					<label for="filterStandardScoreDiff"><?=$Lang['iPortfolio']["StandardScoreDifference"]?></label>&nbsp;
					
					<input type="checkbox" id="filterFormPosition" name="filterColumn[]" value="filterFormPosition" class="displayField" />
					<label for="filterFormPosition"><?=$Lang['iPortfolio']["FormPosition"]?></label>&nbsp;
					
					<input type="checkbox" id="filterFormPositionDiff" name="filterColumn[]" value="filterFormPositionDiff" class="displayField" />
					<label for="filterFormPositionDiff"><?=$Lang['iPortfolio']["FormPositionDifference"]?></label>&nbsp;
					
				</td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["additionalInformation"]?></span></td>
				<td valign="top">
					<input type="checkbox" class="filterAll" id="filterAllB"/>
					<label for="filterAllB"><?=$Lang['Btn']['SelectAll']?></label>&nbsp;&nbsp;&nbsp;
					
					
					<input type="checkbox" id="filterAge" name="filterColumn[]" value="filterAge" />
					<label for="filterAge"><?=$ec_iPortfolio['age']?></label>&nbsp;
					
					<?php if($sys_custom['StudentAccountAdditionalFields']){ ?>
						<input type="checkbox" id="filterPrimarySchool" name="filterColumn[]" value="filterPrimarySchool" />
						<label for="filterPrimarySchool"><?=$Lang['AccountMgmt']['PrimarySchool']?></label>&nbsp;
					<?php } ?>
					
					<input type="checkbox" id="filterPercentile" name="filterColumn[]" value="filterPercentile"/>
					<label for="filterPercentile"><?=$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating']?></label>&nbsp;
				</td>
			</tr>
		</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
		<?= $btn_Print ?>
	</div>
	
	<div id="PrintButton"><?php echo $htmlAry['contentTool']?></div>
	
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
	
	
	<input type="hidden" name="Action" id="Action" value="STUDENT_PROGRESS" />
</form>



<script>
$('.filterAll').change(function(){
	if($(this).attr('checked')){
		$(this).parent().find('input[name="filterColumn\[\]"]').attr('checked','checked');
	}else{
		$(this).parent().find('input[name="filterColumn\[\]"]').removeAttr('checked');
	}
});
$('input[name="filterColumn\[\]"]').click(function(){
	if(!$(this).attr("checked")){
		$(this).parent().find(".filterAll").removeAttr("checked");
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>