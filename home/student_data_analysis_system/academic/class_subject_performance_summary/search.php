<?php
// using : Isaac
/**
 * Change Log: 
 * 2019-01-14 Isaac
 * -    Added export table function
 *  
 * 2017-06-13 Villa
 * -	Add Remarks #Z112708
 * 2017-06-02 Villa
 * -	Add Convert to 100 tr #Z112708
 * -	Add HighChart Mode tr #B111270
 * 2017-05-24 Villa
 * -	Block Subject Panel View
 * 2017-05-10 Villa #T116799 
 * -	Fix wrong val of classSelection
 * 2016-12-14 Villa
 *  -   Add Only subject teacher redirecting
 * 2016-11-24 Villa
 *  -   Add Advanced Searching Filter
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-06-23 Anna
 * -	Change default value of acadamic year
 * 2016-01-26 Pun
 * 	-	Added support for subject panel
 * 	-	Added support display both score, grade, merit
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
######## Init END ########

######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
if(!($accessRight['admin']||$accessRight['classTeacher'])){
	header("Location: /home/student_data_analysis_system/index.php?t=academic.class_marksheets.search");
}
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');
$ayterm_selection_html = '';
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);

$classSelectionHTML = '<select name="ClassName">';
foreach($accessRight['classTeacher'] as $yearClass){
// 	$name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
	$name = Get_Lang_Selection($yearClass["ClassTitleB5"], $yearClass["ClassTitleEN"]);
	$val = $yearClass["ClassTitleEN"];
	$classSelectionHTML .= "<option value=\"{$val}\">{$name}</option>";
// 	$classSelectionHTML .= "<option value=\"{$name}\">{$name}</option>";
}
$classSelectionHTML .= '</select>';

##Convert to 100% Z112708 
$ConvertCheckBox = '<input type="checkbox" id="convertCheckBox" name="convertCheckBox">'.'<label for="convertCheckBox">'.$Lang['SDAS']['class_subject_performance_summary']['convertTo100'].'</label>';
##HighChart Mode B111270 
$HighChartMode = '<input type="radio" name="HighChartMode" id="HighChartMode_Column" value="Column" checked>'.'<label for="HighChartMode_Column">'.$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['column'].'</label>';
$HighChartMode .= '<input type="radio" name="HighChartMode" id="HighChartMode_Box" value="Box">'.'<label for="HighChartMode_Box">'.$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['box'].'</label>';

// $isSubjectPanelView = false;
// if(
// 	in_array('class_subject_performance_summary', (array)$plugin['SDAS_module']['accessRight']['subjectPanel']) &&
// 	count($accessRight['subjectPanel'])
// ){
// 	$isSubjectPanelView = true;
// }
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "class_subject_performance_summary";
// $CurrentPageName = $ec_iPortfolio['master_score_list'];
$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['IndividualYear'], 'javascript: void(0);', true);
$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossYear'],"index.php?t=academic.class_marksheets.search");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$YearArr = $li_pf->returnAssessmentYear();
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID'", $currentAcademicYearID, 0, 0, "", 2);

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);



######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>

<?php echo $linterface->Include_HighChart_JS()?>
<script>
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});
</script>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {
	
	var tableTitle = ['Summery', 'individual students']

 	$(".chart_tables table:visible").each(function(index, element) {
		$( this ).tableExport({
		type: 'csv',
		headings: true,
 		fileName: 'Class Performance '+tableTitle[index]
	});
 });
	
}
</script>

<script language="javascript">
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

function get_yearterm_opt()
{
  var ay_id = $("[name=academicYearID]").val();
  
  if(ay_id == "")
  {
    $("#ayterm_cell").html("");
  }
  else
  {
  	$.ajax({
  		type: "GET",
  		url: "/home/portfolio/profile/management/ajax_get_yearterm_opt.php",
  		data: "ay_id="+ay_id,
  		success: function (msg) {
        	$("#ayterm_cell").html(msg);
        	get_yearclass_opt();
  		}
  	});
  }
}

function get_yearclass_opt()
{
  var ay_id = $("[name=academicYearID]").val();
  
  if(ay_id == "")
  {
    $("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
  }
  else
  {
  	$.ajax({
  		type: "GET",
  		url: "/home/portfolio/profile/management/ajax_get_class_opt.php",
  		data: "ay_id="+ay_id,
  		success: function (msg) {
        $("#yclass_cell").html(msg);
  		}
  	});
  }
}

function get_score_list()
{
  $("#PrintBtn").hide();
  $( "#ExportBtn" ).hide();
  if(checkform())
  {
	$("#scoreListDiv").html('').append(
		$(loadingImage)
	);
  	$.ajax({
  		type: "POST",
//   		url: "/home/portfolio/profile/management/ajax_get_score_list.php",
  		url: "/home/portfolio/profile/management/ajax_get_score_list2.php",
  		data: $("#form1").serialize(),
  		success: function (msg) {
       		$("#scoreListDiv").html(msg);
       	 	$("#PrintBtn").show();
       	 $( "#ExportBtn" ).show();
  		}
  	});
   
  }
}

function checkform(){

	var obj = document.form1;
	var className = $("select[name=ClassName]").val();

	if(typeof className == "undefined" || className == "")
	{
		alert("<?=$ec_warning['please_select_class']?>");
		return false;
	}
	
	if('<?=$sys_custom['SDAS']['AdvanceFilter']?>'=='1'){
		if($('#gender_M').attr("checked")||$('#gender_F').attr("checked")){
		}else{
			$('#gender_M').attr("checked","checked");
			$('#gender_F').attr("checked","checked");
		}	
	}
	return true;
}

$(document).ready(function(){
  $("select[name=academicYearID]").change(function(){
	$("#ayterm_cell").html('').append(
		$(loadingImage).clone()
	);
	$("#yclass_cell").html('').append(
		$(loadingImage)
	);
    get_yearterm_opt();
  
    
    //get_yearclass_opt();
  });
  
  $("#reset_btn").click(function(){
    $("select[name=academicYearID]").val('');
  
    $("#ayterm_cell").html('');
    $("#yclass_cell").html('');
  });

  $("select[name=academicYearID]").change();
 
});



function AdvancedSearch(Btn){
	if(Btn.value=='1'){
		$('.advanceFilter').show();
	}else{
		$('.advanceFilter').hide();
	}
}


</script>

<form id="form1" name="form1" method="GET">

<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
<!-------- Form START --------> 
<?php if($accessRight['admin'] || $isSubjectPanelView ){ ?>
	<tr>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
		<td valign="top">
	        <span><?=$html_year_selection?>&nbsp;</span>
	        <span id='ayterm_cell'>&nbsp;</span>
	    </td>
	</tr>
	<tr id="classRow">
		<td class="field_title" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['class'] ?></td>
		<td valign="top" id='yclass_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
	</tr>
<?php }else{ ?>
	<tr>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
		<td valign="top">
	    	<input type="hidden" name="academicYearID" value="<?=$currentAcademicYearID?>" />
	    	<span><?=$currentAcademicYear?></span>
	        <span><?=$ayterm_selection_html?></span>
	    </td>
	</tr>
	<tr id="classRow">
		<td class="field_title" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['class'] ?></td>
		<td valign="top" id='yclass_cell'>
			<?=$classSelectionHTML?>
		</td>
	</tr>
<?php } ?>
	<tr id="classRow">
		<td class="field_title" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['display'] ?></td>
		<td valign="top" id='yclass_cell'>
			<span class='tabletext'>
				<input type='checkbox' name='ResultType[]' id='type_score' value='SCORE' checked="checked">
				<label for='type_score'><?=$ec_iPortfolio['score']?></label>
			</span><br />
			<span class='tabletext'>
				<input type='checkbox' name='ResultType[]' id='type_grade' value='GRADE' checked="checked">
				<label for='type_grade'><?=$ec_iPortfolio['grade']?></label>
			</span><br />
			<span class='tabletext'>
				<input type='checkbox' name='ResultType[]' id='type_rank' value='RANK' checked="checked">
				<label for='type_rank'><?=$ec_iPortfolio['rank']?></label>
			</span>
		</td>
	</tr>
	
	<tr id="classRow">
		<td class="field_title" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$i_general_others ?></td>
		<td valign="top" id='yclass_cell'>
			<input type="checkbox" id="showSubjectComponent" name="showSubjectComponent" value="1" checked="checked" />
			<label for="showSubjectComponent"><?=$Lang['iPortfolio']['DisplayComponentSubject'] ?></label>
		</td>
	</tr>
	
	<tr id="classRow">
		<td class="field_title" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['highlight_result'] ?></td>
		<td valign="top">
			<div id='table_1'>
				<?=$ec_iPortfolio['selected_grade']?>
				<input type='text' name='grade_highlight' value='E, F'/>
			</div>
			<br />
			<div>
				<?=$ec_iPortfolio['highlight_grade_remind']?>
				<input type='checkbox' name='range_bold' id='range_bold' value=1 />
				<label for='range_bold'><?=$ec_iPortfolio['bold']?></label>
				
				<input type='checkbox' name='range_italic' id='range_italic' value=1 />
				<label for='range_italic'><i><?=$ec_iPortfolio['italic']?></i></label>
				
				<input type='checkbox' name='range_underline' id='range_underline' value=1 />
				<label for='range_underline'><u><?=$ec_iPortfolio['underline']?></u></label>
								
				<input type='checkbox' name='range_color' id='range_color' value='1' checked/>
				<label for='range_color'><?=$ec_iPortfolio['color']?>&nbsp;
					<select name='r_color'>
						<option value='RED'><?=$ec_iPortfolio['red']?></option>
						<option value='GREEN'><?=$ec_iPortfolio['green']?></option>
						<option value='BLUE'><?=$ec_iPortfolio['blue']?></option>
					</select>
				</label>
							
			</div>
		</td>
	</tr>
	<tr>
		<td class="field_title"  style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top">
			<?=$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['HighChartMode']?>
		</td>
		<td valign="top">
			<?=$HighChartMode?>
		</td>
	</tr>
	<tr>
		<td class="field_title"  style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top">
			<?=$Lang['SDAS']['class_subject_performance_summary']['FullMark'] ?>
		</td>
		<td valign="top">
			<?=$ConvertCheckBox?>
			<br>
			<span class="tabletextremark"><?=$Lang['SDAS']['class_subject_performance_summary']['convertTo100_Remarks']?></span>
		</td>
	</tr>
	<?php if($sys_custom['SDAS']['AdvanceFilter']){?>
	<tr id="filterRow">
		<td class="field_title" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top">	<?=	$Lang['iPortfolio']['Filter'] ?>
		</td>
		<td valign="top">
		<?=$libSDAS->GET_FILTER_TABLE()?>
		</td>
	</tr>
	<?php }?>
<!-------- Form END -------->
</table>
  
	<span class="tabletextremark"></span>
	<p class="spacer"></p>
	<div class="edit_bottom_v30" style="max-width: 1024px;">
        <input onClick='get_score_list()' class='formbutton' type='button' value='<?=$button_view?>' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
        <!--input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"-->
	</div>
</form>

<div id="PrintButton">
<?php echo $htmlAry['contentTool']?>
</div>
<div id="PrintArea">
	<div id="scoreListDiv"></div>
</div>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>