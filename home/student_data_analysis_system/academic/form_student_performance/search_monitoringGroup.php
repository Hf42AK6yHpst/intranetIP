<?php
//Using: Isaac
/**
 * Change Log:
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2017-05-10 Villa #T116811
 * -	Fix Subject Panel Access Right Overwrite ClassTeacher (Subject Panel Cannot Access This Report Now)
 * 2017-02-22 Villa
 * -	Change the Compare Term can be in Different Academic Year
 * 2017-02-07 Villa
 * -	Update the UI wording
 * 2017-02-06 Villa
 * -	add tr for yearterm comparsion
 * 2016_12-23 Villa
 * -	add Position In Form Subject and Percentile Rank Subject
 * 2016-12-14 Villa
 * -	update the UI
 * 2016-12-12 Villa
 * -	modified get_overall_performace - add form submi checking
 * 2016-12-09
 * -	Villa Change access right
 * 2016-11-25 villa
 * -	Open the file - copy from other page
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$json = new JSON_obj();
######## Init END ########

// debug_pr($accessRight);
######## Access Right START ########
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

// OMAS 2017-08-07
// $ayterm_selection_html = '';
// if(!$sys_custom['SDAS']['EnableAllTeacher']){
// 	if(!$accessRight['admin'] ){
// 		if(count($accessRight['classTeacher'])){
// 			$isClassTeacher = 1;
// 		}
// 	}
// }
######## Access Right END ########


######## Subject Panel filter subject START ########
$isSubjectPanelView = false;
$subjectPanelAccessRightList = "''";
if(
		!$accessRight['admin'] &&
		count($accessRight['subjectPanel']) &&
		in_array('subject_performance_statistic', (array)$plugin['SDAS_module']['accessRight']['subjectPanel'])
		){
			include_once($PATH_WRT_ROOT."includes/json.php");
			$json = new JSON_obj();
			foreach($accessRight['subjectPanel'] as $_subjectId => $_yearIdArr){
				foreach($_yearIdArr as $_yearID=>$_){
					$subjectPanelAccessRightArr[$_yearID][] = $_subjectId;
				}
			}
			$isSubjectPanelView = true;
}

if(!$accessRight['admin'] && !$accessRight['MonitoringGroupPIC']){
	if($accessRight['MonitoringGroupPIC']){
		header('?t=academic.form_student_performance.search_monitoringGroup');
	}else{
		echo 'No access right';
		return false;
	}
}

if(count($accessRight['subjectPanel']) || $accessRight['admin']|| count($accessRight['classTeacher'])){
	$monitoringGroup = '<input type="radio" name="target_type" id="target_type_class" value="class" >'.'<label for="target_type_class">'.$Lang['SDAS']['Class'].'</label>';
	$monitoringGroup .= '<input type="radio" name="target_type" id="target_type_monitoring_group" value="monitoring_group" checked>'.'<label for="target_type_monitoring_group">'.$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] .'</label>';
}
if($accessRight['MonitoringGroupPIC']){
	$monitoringGroupSelection = '<select name="monitoringGroup" id="monitoringGroup">';
	foreach ($accessRight['MonitoringGroupPIC'] as $monitoringGroupID){
		$monitoringGroupInfo = $libSDAS->getMonitoringGroup($monitoringGroupID,'');
		$monitoringGroupInfo = $monitoringGroupInfo[0];
		$monitoringGroupSelection .= '<option value="'.$monitoringGroupInfo['GroupID'].'">'.Get_Lang_Selection($monitoringGroupInfo['GroupNameCH'], $monitoringGroupInfo['GroupNameEN']).'</option>';
	}
	$monitoringGroupSelection .= '</select>';
}
######## Subject Panel filter subject END ########

######## Page Setting START ########
$CurrentPage = "form_student_performance";
// $CurrentPageName = $Lang['SDAS']['ClassPerformance']['IndividualYear'];
$TAGS_OBJ[] = array($Lang['SDAS']['menu']['FormStudentPerformance'],"");
// $TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossYear'],"index.php?t=academic.subject_class_distribution.search2");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$YearArr = $li_pf->returnAssessmentYear();
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID' id='academicYearID'", $currentAcademicYearID, 0, 0, "", 2);
$html_year_selection_VS = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID_VS' id='academicYearID_VS'", $currentAcademicYearID, 0, 0, "", 2);

//$html_classlevel_selection = getSelectByArray(array(), "name='YearID'", $YearID, 0, 0, "", 2);
######## UI Releated END ########

$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();
echo $linterface->Include_TableExport();

?>

<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>


<form id="form1" name="form1" method="get" action="overall_performance_stat.php">


<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">

<!-------- Form START --------> 

	<tr>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?>/ <?=$Lang['General']['Term']?></span></td>
		<td valign="top">
			<?=$html_year_selection?>&nbsp;	<span id='ayterm_cell'><?=$ayterm_selection_html?></span> 
		</td>
	</tr>
	<tr>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Form_student_Performance']['Compare']?> <?=$ec_iPortfolio['year']?>/ <?=$Lang['General']['Term']?></span></td>
		<td valign="top">
		 	<?=$html_year_selection_VS?>&nbsp; <span id='ayterm_cell_VS'><?=$ayterm_selection_html?></span>
		</td>
	</tr>
	<?php if($monitoringGroup){?>
	<tr>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Mode']?></span></td>
		<td><?=$monitoringGroup?></td>
	</tr>
	<?php }?>
	<tr>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['AcademicProgress']['MonitoringGroup']?></span></td>
		<td id="monitoringGroupSelection"></td>
	</tr>
	<tr id='SubjectRow'>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['menu']['percentile']." ".$ec_iPortfolio['SAMS_subject']?></span></td>
		<td valign="top" id='subj_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></td>		
	</tr>
	<tr id='SubjectRow'>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['PositionInFormDiff'] ." ".$ec_iPortfolio['SAMS_subject']?></span></td>
		<td valign="top" id='subj_cell_2'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></td>		
	</tr>

<!-------- Form END -------->
</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
	    <input type="button" id="viewForm" value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
	    <!--input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"-->
	</div>
	<div id="PrintButton">
	<?php echo $htmlAry['contentTool']?>
	</div>
	<div id="PrintArea" style="text-align:center"><div id="overallPerformancDiv"></div></div>
</form>





<script language="javascript">
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});

var data;
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {

	$("#PrintArea table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Form Student Performance'
	});

}

$(function(){
	
	$('#option').hide();
////////For Class Teacher END ////////
	$('#academicYearID').change();
	$('#academicYearID_VS').change();
	
	$('input[name="target_type"]').change(function(){
		if($(this).val() == 'class'){
			window.location.href = '?t=academic.form_student_performance.search';
		}
	});
	
});

function get_yearterm_opt(){
	var ay_id = $('#academicYearID').val();
	var tag = "id=YearTermID name=YearTermID";
	if(ay_id == ''){
		$("#ayterm_cell").html("");
		return;
	}

	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_year_term_selection_box.php",
		data:	{
					"AcademicYearID":ay_id,
					"tag":tag
				},	
		success: function (msg) {
			$("#ayterm_cell").html(msg);
		}


	});
}
function get_yearterm2_opt(){
	var ay_id = $('#academicYearID_VS').val();
	var tag = "id=YearTermID_VS name=YearTermID_VS";
	if(ay_id == ''){
		$("#ayterm_cell_VS").html("");
		return;
	}

	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_year_term_selection_box.php",
		data:	{
					"AcademicYearID":ay_id,
					"tag":tag
				},
		success: function (msg) {
			$("#ayterm_cell_VS").html(msg);
		}


	});
}

function checkAll(){
	var checked = $(this).attr("checked");
	if(($(this).attr("checked"))=="checked" ){
		$(this).parent().parent().parent().find("input[name]").attr("checked", checked);
	
	}
	else{
		$(this).parent().parent().parent().find("input[name]").removeAttr("checked");
	}
	
};



$('#academicYearID').change(function(){
	var filterSubjectArr = '';
	$("#ayterm_cell").html('').append(
		$(loadingImage).clone()
	);
	$("#y_cell").html('').append(
		$(loadingImage)
	);
	$('#monitoringGroupSelection').html('').append(
		$(loadingImage)
	);
	
	get_yearterm_opt();
	get_monitoringGroup();
});

function get_monitoringGroup(){
	$.ajax({
		url : "?t=ajax.ajax_monitoring_group_handling",
		type: "POST",
		data:	{
					"ACTION": "getCurrentYearGroup",
					"GroupIDArr" : <?=$json->encode($accessRight['MonitoringGroupPIC'])?>,
					"AcademicYearID" : $('#academicYearID').val()	
				},
		success: function(respond){
			$('#monitoringGroupSelection').html(respond);
			$('#monitoringGroup').change(function(){
				get_monitoringGroupSubject();
			});
			$('#monitoringGroup').change();
		}
	});
}

function get_monitoringGroupSubject(){
	$('#subj_cell').html('').append(
		$(loadingImage)
	);
	$.ajax({
		url : "?t=ajax.ajax_monitoring_group_handling",
		type: "POST",
		data:	{
					"ACTION": "getGroupSubject",
					"GroupID" : $('#monitoringGroup').val(),
					"CheckBoxName" :"Subject1",
					"AcademicYearID" : $('#academicYearID').val(),
					'task' : 'sbjSelect1'
				},
		success: function(respond){
			$('#subj_cell').html(respond);
		}
	});
	$('#subj_cell_2').html('').append(
			$(loadingImage)
	);
	$.ajax({
		url : "?t=ajax.ajax_monitoring_group_handling",
		type: "POST",
		data:	{
					"ACTION": "getGroupSubject",
					"GroupID" : $('#monitoringGroup').val(),
					"CheckBoxName" :"Subject2",
					"AcademicYearID" : $('#academicYearID').val(),
					'task' : 'sbjSelect2'
				},
		success: function(respond){
			$('#subj_cell_2').html(respond);
		}
	});
}

$('#academicYearID_VS').change(function(){
	$("#ayterm_cell_VS").html('').append(
			$(loadingImage).clone()
		);
	get_yearterm2_opt();
});



$("#reset_btn").click(function(){
	$("select[name=academicYearID]").val('');

	$("#ayterm_cell").html('');
	$("#y_cell").html('');

});


$('#viewForm').click(get_overall_performace);
function get_overall_performace()
{
	var form = $('#form1');

	if($("[name='yearID']").val()==''){
		alert("<?=$ec_warning['please_select_class']?>");
		return false;
	}
	
  	$.ajax({
  		type: "POST",
  		url: "/home/student_data_analysis_system/ajax/ajax_form_student_performance.php",
  		data: $('#form1').serialize(),
  		beforeSend: function () {
  		  $("#overallPerformancDiv").parent().children().remove().end().append(
          $("<div></div>").attr("id", "overallPerformancDiv").append(
  		      $(loadingImage)
          )
        ); 
      },
  		success: function (msg) {
  			$("#overallPerformancDiv").html(msg);
  			$( "#PrintBtn" ).show();
  			$("#ExportBtn").show();	
  		}
  	});
}

</script>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
