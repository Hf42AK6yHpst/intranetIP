<?php
//Using:
/**
 * Change Log:
 * 2019-10-03 Philips [2019-0917-1535-36207]
 *  -	Removed subject relation checking
 * 2019-09-16 Philips [2019-0910-1515-58206]
 * Modified get_yearterm_opt() - avoid empty yearterm selection box
 * 2019-06-19 Philips
 * - Added Subject Group checking
 * 2019-01-14 Isaac
 * -    Added export table function
 * 2017-05-29 Villa #T117443 
 * - modified get_subject_multiopt: handle multi-access right problem
 * 2016-11-22 Villa
 * - add comparsion filter
 * 2016-09-13 Villa
 * - Modified checkAll(), fixing check all (uncheck all) problem
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-09-01 (Omas)
 * - modified get_subject_multiopt() - removing yearClassId from ajax
 * 2016-08-30 Pun
 * - Added chart type selection 
 * 2016-06-23 Anna
 * - Change default value of class, subject 
 * 2016-06-03 Anna
 * - Change view of table and bar chart
 * 2016-03-30 Pun
 *  - Modified get_year_opt(), get_yearclass_multiopt(), fixed does not reset the <select> while selected empty
 * 2016-03-21 Pun [94169, 94175]
 *  - Modified get_subject_multiopt(), fixed hardcoded academic year ID
 * 2016-02-23 Pun
 *  - Change the chart from Flash to d3.js
 */
include_once($PATH_WRT_ROOT . 'includes/libclass.php');
######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$lclass = new libclass();
######## Init END ########

// debug_pr($accessRight);
######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');
$currentAcademicYear = convert2unicode($currentAcademicYear,1,1);


$ayterm_selection_html = '';
//$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
//$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);

$classTeacherClassIdArr = array();
$yearClassArray = array();
$subjectIDAry = array();
$yearIDAry = array();
$subjectClassAry = array();
$yearAry = array();
foreach($accessRight['classTeacher'] as $yearClass){
	$yearClassArray[$yearClass['YearClassID']] = array("ClassTitleEN" => convert2unicode($yearClass["ClassTitleEN"], 1, 1), "ClassTitleB5" => convert2unicode($yearClass["ClassTitleB5"], 1, 1), "YearID" => $yearClass['YearID']);
	$yearName = $lclass->getLevelName($yearClass['YearID']);
	//     $yearAry[] = array($yearClass['YearID'], $yearName);
	$yearAry[$yearClass['YearID']] = $yearName;
	//$classTeacherClassIdArr[] = $yearClass["YearClassID"];
}
foreach($accessRight['subjectGroup'] as $subjectGroup){
	$sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
	$subjectIDAry[] = $sGroup->SubjectID;
	$studentList = $sGroup->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=1, $showClassID = true, $showYearName = true);
	$classList = BuildMultiKeyAssoc($studentList, array("YearClassID"), array("ClassTitleB5","ClassTitleEN","YearName","YearID"));
	if(!$subjectClassAry[$sGroup->SubjectID]){
		$subjectClassAry[$sGroup->SubjectID] = array();
	}
	foreach($classList as $classID => $classDetail){
		if(!in_array($classID,$subjectClassAry[$sGroup->SubjectID])){
			$subjectClassAry[$sGroup->SubjectID][] = $classID;
		}
		$yearIDAry[$classDetail['YearID']] = $classDetail['YearName'];
		if(!is_array($classTeacherClassIdArr[$classDetail['YearID']])) $classTeacherClassIdArr[$classDetail['YearID']] = array();
		$classTeacherClassIdArr[$classDetail['YearID']][] = $classID;
		if(!$accessRight['subjectPanel']) $yearClassArray[$classID] = array("ClassTitleEN" => $classDetail["ClassTitleEN"],"ClassTitleB5" => $classDetail["ClassTitleB5"], "YearID" => $classDetail['YearID']);
	}
}
foreach($yearClassArray as $yearClassID => $yearClass){
	$name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
	$classSelectionHTML .= "<option value=\"{$yearClassID}\">{$name}</option>";
	if(!is_array($classTeacherClassIdArr[$yearClass['YearID']])) $classTeacherClassIdArr[$yearClass['YearID']] = array();
	$classTeacherClassIdArr[$yearClass['YearID']][] = $yearClassID;
}
$subjectIDAry = array_unique($subjectIDAry);
$yAry = array();
foreach($yearIDAry as $yid => $yName){
	$yAry[$yid] = $yName;
}
foreach($yearAry as $yid => $yName){
	$yAry[$yid] = $yName;
}
$yearAry = array();
foreach($yAry as $yearID => $yearName){
	$yearAry[] = array($yearID, $yearName);
}
$html_classlevel_selection = (sizeof($yearAry)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($yearAry, "name='yearID'");
######## Access Right END ########

######## Subject Panel filter subject START ########
$isSubjectPanelView = false;
$subjectPanelAccessRightList = "''";
if(
		!$accessRight['admin'] &&
		count($accessRight['subjectPanel']) &&
		in_array('subject_performance_statistic', (array)$plugin['SDAS_module']['accessRight']['subjectPanel'])
		){
			include_once($PATH_WRT_ROOT."includes/json.php");
			$json = new JSON_obj();
			foreach($accessRight['subjectPanel'] as $_subjectId => $_yearIdArr){
				foreach($_yearIdArr as $_yearID=>$_){
					$subjectPanelAccessRightArr[$_yearID][] = $_subjectId;
				}
			}
			$subjectPanelAccessRightList = $json->encode($subjectPanelAccessRightArr); // For PHP 2D Array to JS Object
			
			$isSubjectPanelView = true;
}
######## Subject Panel filter subject END ########

######## Page Setting START ########
$CurrentPage = "subject_performance_statistic";
$CurrentPageName = $Lang['SDAS']['menu']['ClassSubjectComparison'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$YearArr = $li_pf->returnAssessmentYear();
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID' id='academicYearID'", $currentAcademicYearID, 0, 0, "", 2);

//$html_classlevel_selection = getSelectByArray(array(), "name='YearID'", $YearID, 0, 0, "", 2);
######## UI Releated END ########

$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();

?>

<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>


<form id="form1" name="form1" method="get" action="overall_performance_stat.php">


<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">

<!-------- Form START --------> 
<?php if($accessRight['admin'] || $isSubjectPanelView){ ?>

	<tr>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
		<td valign="top">
			<?=$html_year_selection?>&nbsp;
            <span id='ayterm_cell'><?=$ayterm_selection_html?></span>
		</td>
	</tr>
	<tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio_Report['form']?></span></td>
		<td valign="top" id='y_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
	</tr>
	<tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
    	<td valign="top" id='yclass_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>	</td>
	</tr>
    <tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['SAMS_subject']?></span></td>
		<td valign="top" id='subj_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?></td>
    </tr>
    <tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['based_on']?></span></td>
		<td>
			<div><span class="tabletext"><input type="radio" id="based_on_1" name="based_on" value="1" checked='checked'/><label for="based_on_1"><?=str_replace("<!--base-->",$ec_iPortfolio['SAMS_subject'],$iPort["stat_by"])?></label></span></div>
			<div><span class="tabletext"><input type="radio" id="based_on_0" name="based_on" value="0" /><label for="based_on_0"><?=str_replace("<!--base-->",$ec_iPortfolio['class'],$iPort["stat_by"])?></label></span></div>
		</td>
    </tr>
<?php } else{ ?>
<tr>
	<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
	<td valign="top">
		<input type="hidden" name="academicYearID" id="academicYearID" value="<?=$currentAcademicYearID?>" />
		<?=$currentAcademicYear?>&nbsp;&nbsp;
		<div nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></div>
	</td>
</tr>
<tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio_Report['form']?></span></td>
		<td valign="top" id='y_cell'><?php echo $html_classlevel_selection;?></td>
	</tr>
<tr>
<tr>
	<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class'] ?></span></td>
	<td valign="top">
		<div valign="top" id='yclass_cell'><?//$classSelectionHTML?></div>
	</td>
</tr>
<tr>
	<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['SAMS_subject']?></span></td>
	<td valign="top">
		<div valign="top" id='subj_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></div>
	</td>
</tr>
    <input type="hidden" id="based_on_1" name="based_on" value="1" />
<?php } ?>
<tr id='classRow'>
	<td class="field_title"><?=	$iPort['report_col']['compare']?></td>
	<td>
		<div><span class="tabletext"><input type="radio" id="compare_averager" name="compare" value="avgScore" checked='checked'/><label for="compare_averager"><?=$Lang['SDAS']['averageScore']?></label></span></div>
		<div><span class="tabletext"><input type="radio" id="compare_pass" name="compare" value="PassPercent" /><label for="compare_pass"><?=$iPort['report_col']['passing_rate']?></label></span></div>
		<div><span class="tabletext"><input type="radio" id="compare_sd" name="compare" value="SD" /><label for="compare_sd"><?=$iPort['SD']?></label></span></div>
		<div><span class="tabletext"><input type="radio" id="compare_highmarks" name="compare" value="HighestMark" /><label for="compare_highmarks"><?=$iPort['HighestMark']?></label></span></div>
		<div><span class="tabletext"><input type="radio" id="compare_lowest" name="compare" value="LowestMark" /><label for="compare_lowest"><?=$iPort['LowestMark']?></label></span></div>
	</td>
</tr>
<tr id='classRow'>
	<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['chart_type']?></span></td>
	<td>
		<div><span class="tabletext"><input type="radio" id="barchart" name="chartType" value="column" checked='checked'/><label for="barchart"><?=$ec_iPortfolio['barchart']?></label></span></div>
		<div><span class="tabletext"><input type="radio" id="linechart" name="chartType" value="line"/><label for="linechart"><?=$ec_iPortfolio['linechart']?></label></span></div>
	</td>
</tr>
<!-------- Form END -------->
</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
	    <input type="button" id="viewForm" value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
	    <!--input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"-->
	</div>
	<div id="PrintButton">
	<?php echo $htmlAry['contentTool']?>
	</div>
	<div id="PrintArea" style="text-align:center"><div id="overallPerformancDiv"></div></div>
</form>





<script language="javascript">
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});

var data;
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};



$(function(){
	$("#y_cell").find('[name="yearID"]').change(get_yearclass_multiopt);
	function get_yearterm_opt(){
		var ay_id = $('#academicYearID').val();
		if(ay_id == ''){
			$("#ayterm_cell").html("");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_yearterm_opt.php",
			data: "ay_id="+ay_id,
			success: function (msg) {
				$("#ayterm_cell").html(msg);
				if($('select[name="YearTermID"]').html().trim() == ''){
					$('#ayterm_cell').html('<?php echo $no_record_msg;?>');
					$('.formbutton').attr('disabled', true);
				} else {
					$('.formbutton').removeAttr('disabled');
				}
			}

	
		});
	}
	function get_year_opt(){
		var ay_id = $('#academicYearID').val();

		if(ay_id == ''){
			$("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>");
			$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class'] ?>");
			$("#y_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_year_opt.php",
			data: "ay_id="+ay_id,
			success: function (msg) {
				$("#y_cell").html(msg);
				$("#y_cell").find('[name="yearID"]').change(get_yearclass_multiopt);
			}
		
		});
	
	
	}
	function get_yearclass_multiopt()
	{

		var y_id = $("[name=yearID]").val();
		var ay_id = $("#academicYearID").val();
	
		$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
		if(typeof y_id == "undefined" || y_id == ""){
			$("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>");
			return;
		}

		$("#yclass_cell").html('').append(
			$(loadingImage)
		);
		
		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_class_multiopt2.php",
			data: "y_id="+y_id+"&ay_id="+ay_id,
			success: function (msg) {
				$("#yclass_cell").html(msg);
				//var classAry = [<?php echo "'" . implode("','", $classTeacherClassIdArr) . "'";?>];
				classAry = {};
				<?php foreach($classTeacherClassIdArr as $yearID => $classID){
					echo "classAry[$yearID] = ['" . implode("','", $classID) . "'];";
				}?>
				<?php if(!$accessRight['admin'] && !$accessRight['subjectPanel']){?>
					var filterClass = $('#yclass_cell').find('.yearClassId').filter(function(index){
						if(classAry[y_id] instanceof Array)
							return classAry[y_id].indexOf($(this).val()) == -1;
						else
							return false;
					});
					filterClass.next().remove();
					filterClass.remove();
				<?php }?>
				$("#yclass_cell").find('.checkMaster').change(checkAll);
				$("#yclass_cell").find('input[type="checkbox"]').change(updateSubjectSelectDisplay);
				
    			$("#yclass_cell").attr("checked", "checked");
    			$("#YCCheckMaster.checkMaster").attr("checked", "checked");
    			$(".yearClassId").attr("checked", "checked"); 
    			$("#yclass_cell").find('.checkMaster').change();
    		}

		});
		
	
		
	}

	function updateSubjectSelectDisplay(){
		if($('#yclass_cell').find('.yearClassId:checked').length == 0){
			$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');
			return;
		}
		$("#subj_cell").html('').append(
			$(loadingImage)
		);

		var selfClassArr = [<?=$classTeacherClassIdList ?>];
		y_id = $("[name=yearID]").val();
		classAry = {};
		<?php foreach($classTeacherClassIdArr as $yearID => $classID){
			echo "classAry[$yearID] = ['" . implode("','", $classID) . "'];";
		}?>

		var isOnlySelfClassChecked = true;
		$('#yclass_cell').find('.yearClassId:checked').each(function(index, ele){
			if($.inArray($(ele).val(), classAry[y_id]) == -1){
				isOnlySelfClassChecked = false;
				return false; // Break $.each()
			}
		});
		
		var filterSubjectArr = [];
		<?php if($isSubjectPanelView){ ?>
			if(!isOnlySelfClassChecked){
				var subjectPanelAccessRightList = <?=$subjectPanelAccessRightList ?>;
				filterSubjectArr = subjectPanelAccessRightList[ $('[name="yearID"]').val() ];
				if( typeof(filterSubjectArr) == 'undefined'){
					filterSubjectArr = [0];
				}
			}

		<?php } ?>
		var subjectClassArr = [];
		<?php foreach($subjectClassAry as $subjectID => $classIDAry){
    		    echo "subjectClassArr['$subjectID'] = [];\n";
    		    foreach($classIDAry as $classID){
    		        echo "subjectClassArr['$subjectID'].push('$classID');\n";
    		    }
		}?>
		<?php if(!$accessRight['admin']){?>
			var classInUse = [];
			$('#yclass_cell').find('.yearClassId:checked').filter(function(index){
				var classId = $(this).val();
				if(classInUse.indexOf(classId) == -1)
					classInUse.push(classId);
			});
			subjectClassArr.forEach(function(classAry, subjectID){
				var check = true;
				classInUse.forEach(function(v, x){
					if(classAry.indexOf(v) == -1){
						check = false;
					}
				})
				if(check) filterSubjectArr.push(subjectID);
			});
		<?php }?>
		get_subject_multiopt(filterSubjectArr);

	}
	
	function get_subject_multiopt(filterSubjectArr)
	{
		filterSubjectArr = filterSubjectArr || [];

		var y_id = $("[name=yearID]").val();
		var ay_id = $("#academicYearID").val();
	
		if(typeof y_id == "undefined" || y_id == "")
		{
			$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_subject_multiopt.php",
			data: {
				'ay_id': ay_id,//'<?=$currentAcademicYearID?>',
				'y_id': y_id,
				//'yearClassId': $(this).val(),
				'filterSubject[]': filterSubjectArr
			},
			success: function (msg) {
				if($('#yclass_cell').find('.yearClassId:checked').length == 0){
					$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');

					return;
 				}
				$("#subj_cell").html(msg);
				$("#subj_cell").find('.checkMaster').change(checkAll);

				
			
 				$("#subj_cell").attr("checked", "checked");
 				$("#SubjCheckMaster.checkMaster").attr("checked", "checked");
 				$(".subjectChk").attr("checked", "checked"); 
				
			}
		});

	
	}

	function checkAll(){
		var checked = $(this).attr("checked");
		if(($(this).attr("checked"))=="checked" ){
			$(this).parent().parent().parent().find("input[name]").attr("checked", checked);
		
		}
		else{
			$(this).parent().parent().parent().find("input[name]").removeAttr("checked");
		}
		
	};


	
	$('#academicYearID').change(function(){
		$("#ayterm_cell").html('').append(
			$(loadingImage).clone()
		);
		
		get_yearterm_opt();
// 		get_year_opt();
// 		get_yearterm_opt();
		<?php if($accessRight['admin'] || $isSubjectPanelView){ ?>
		get_year_opt();
		<?php }else{?>
		$('#yclass_cell > select').change();
		<?php }?>
	});
	
	
	$("#reset_btn").click(function(){
		$("select[name=academicYearID]").val('');

		$("#ayterm_cell").html('');
		$("#y_cell").html('');


	
		get_yearclass_multiopt();
		get_subject_multiopt();
	});


	$('#viewForm').click(get_overall_performace);
	function get_overall_performace()
	{
		var form = $('#form1');

		if($('#form1').find('.yearClassId').length > 0 && $('#form1').find('.yearClassId:checked').length==0)
		{
			alert("<?=$Lang['SDAS']['ClassSubjectComparsion']['ClassWarning']?>");
			return false;
		}
		else if($('#form1').find('#subj_cell [name]:checked').length==0)
		{
			alert("<?=$Lang['SDAS']['ClassSubjectComparsion']['SubjectWarning'] ?>");
			return false;
		}
		
	  	$.ajax({
	  		type: "POST",
	  		url: "/home/portfolio/profile/management/ajax_get_overall_performace2.php",
	  		data: $('#form1').serialize(),
	  		beforeSend: function () {
	  		  $("#overallPerformancDiv").parent().children().remove().end().append(
	          $("<div></div>").attr("id", "overallPerformancDiv").append(
	  		      $(loadingImage)
	          )
	        );
	      },
	  		success: function (msg) {
	  			$("#overallPerformancDiv").html(msg);
// 	  			$("#PrintButton").show();
	  			$( "#PrintBtn" ).show();
	  		}
	  	});
	}


	////////For Class Teacher START ////////
	$('#yclass_cell > select').change(function(){
// 	$('#yclass_cell').change(function(){
	var classID = $(this).val();
	if(classID==''){
		$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');
	}else{
		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_subject_multiopt.php",
			data: {
				'ay_id': '<?=$currentAcademicYearID?>',
				'y_id': '',
				'yearClassId': $(this).val()
			},
			beforeSend: function () {
				$("#subj_cell").html('').append(
					$(loadingImage)
				);
			},
			success: function (msg) {
				$("#subj_cell").html(msg);
				$("#subj_cell").find('.checkMaster').change(checkAll);
				
				
			}
		});
	}
	});
	
	

	////////For Class Teacher END ////////
	$('#academicYearID').change();

	
	
});
</script>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
