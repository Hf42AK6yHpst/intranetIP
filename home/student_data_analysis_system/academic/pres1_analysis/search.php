<?php
// using : 
/**
 * Change Log:
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
include_once($PATH_WRT_ROOT.'includes/libpf-exam.php');
$li_pf_exam = new libpf_exam();
######## Init END ########


######## Access Right START ########
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab

######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "pres1_analysis";

$TAGS_OBJ[] = array($Lang['SDAS']['menu']['PreS1Analysis'], 'index.php?t=academic.pres1_analysis.search');

$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();



$YearArr = $li_pf_exam->getExamYear(); // According to Attainment Test Record Table
// $YearArr = $li_pf->returnAssessmentYear();
$FromAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FromAcademicYearID' id='FromAcademicYearID' onchange=\"\" ", $currentAcademicYearID, 0, 0, "", 2);

$ToAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='ToAcademicYearID' id='ToAcademicYearID' onchange=\"\" ", $currentAcademicYearID, 0, 0, "", 2);

//Print Button
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
// $btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();
echo $linterface->Include_TableExport();
?>
<script src="/templates/d3/dc/crossfilter.min.js"></script>
<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

// function exportCSV() {
// 	$(".chart_tables table").tableExport({
// 		type: 'csv',
// 		headings: true,
// 		fileName: 'Class Marksheets'
// 	});

// }
</script>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFromAcademicYearID = '<?=$FromAcademicYearID?>';
var jsCurFromYearTermID = '<?=$FromYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

// $(document).ready( function() {
// 	if (jsCurYearID == '') {
// 		jsCurYearID = $('select#YearID').val();
// 	}	
// 	if (jsCurFromAcademicYearID == '') {
// 		jsCurFromAcademicYearID = $('select#FromAcademicYearID').val();
// 	}	
// 	if (jsCurToAcademicYearID == '') {
// 		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
// 	}	
	
// 	var jsRefreshDBTable = 0;
// 	if (jsClearCoo == '') {
// 		jsRefreshDBTable = 1;
// 	}	
// });


$(document).ready( function() {
	// show hide
// 	$( ".show_hide" ).click(function() {
// 		if( $('.accordion_head').hasClass('on')){
			$('#hideButton').text('<?php echo $Lang['CEES']['Hide']?>');
// 		}
// 		else{
			$('#hideButton').text('<?php echo $Lang['Btn']['Show']?>');
// 		}		
// 	});

	// generate report
	$("#generate").click(function() {
		$("#filterForm").submit();
	});

	// form submit
	$("#filterForm").submit(function(e){
		
		// check form before submit
		if(!checkForm())
		{
			return false;
		}

		$("div#graphDiv").addClass("board_main_content white");
	    $('div#graphDiv').html(ajaxImage);
	    var postData = $(this).serializeArray();
	
	    e.preventDefault(); //STOP default submit action
	});
});



function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_DBTable()
{
	$( "#PrintBtn" ).hide();
// 	$( "#ExportBtn" ).hide();
	if($('#FromAcademicYearID').val() == '' || $('#ToAcademicYearID').val() == ''){
		alert('<?=$Lang['General']['WarningArr']['PleaseSelectPeriod']?>');
		return;
	}

	if($('#FromAcademicYearID')[0].selectedIndex < $('#ToAcademicYearID')[0].selectedIndex){
		alert('<?=$Lang['General']['WarningArr']['EndPeriodCannotEarlierThanStartPeriod']?>');
		return;
	}
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php?t=ajax.ajax_pres1_analysis",
		$('#form1').serialize(),
    	function(jsonObj)
    	{	    	
		    	$("#DBTableDiv").html(jsonObj);
	    		$( "#PrintBtn" ).show();
    			
    	}
	);

}

function js_Print()
{
	document.form1.submit();
}
</script>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" method="POST" target="iport_stats">
	<?=$html_tab_menu ?>
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["period"]?></span></td>
				<td valign="top"><?=$Lang['General']['From']?> <?=$FromAcademicYearSelection?> <?=$Lang['General']['To']?> <?=$ToAcademicYearSelection?></td>
			</tr>
		</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
		<?= $btn_Print ?>
	</div>
	
	<div id="PrintButton"><?php echo $htmlAry['contentTool']?></div>
	
	
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>