<?php
// using : 
/**
 * Change Log:
 * 2020-08-05 Philips [2020-0706-0942-38235]
 * Fixed Class Selection into current academic year, import class select sorting
 * 2020-05-07 Philips [2019-0708-1528-22289]
 * Moved DeleteUserLegend remark from libportfolio
 * 2020-04-29 Philips [2019-0708-1528-22289]
 * Added FormPositionPercentile for $sys_custom['SDAS']['HKTLC']['FormPercentile']
 * 2020-01-08 Philips [DM#3712]
 * Move filterFormSplineDiagram to additional information
 * 2019-11-28 Philips [2019-0710-1725-23085]
 * Added Highchart
 * 2019-06-19
 * Added Subject Group class selection
 * 2019-04-04 Isaac  
 * Added passing file name for export function
 *  
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-06-23 Anna
 * -    Default display as mark
 *  
 * 2016-02-18 Pun
 * 	-	Added "Additional Information" filter
 * 2016-02-17 Pun
 * 	-	Added not select class alert
 * 2016-01-26 Pun
 * 	-	Added support for subject panel
 * 	-	Added export CSV
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) 
		&& in_array('student_marksheets',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
	$accessRight['admin'] = 1;
}

$ayterm_selection_html = '';
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);
$yearClassArray = array();
$classSelectionHTML = '<select id="YearClassID" name="YearClassID" onchange="js_Reload_Student_Selection()">';
// foreach($accessRight['classTeacher'] as $yearClass){
// 	$name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
// 	$classSelectionHTML .= "<option value=\"{$yearClass['YearClassID']}\">{$name}</option>";
// }
foreach($accessRight['classTeacher'] as $yearClass){
	$yearClassArray[$yearClass['YearClassID']] = array("YearClassID"=>$yearClass['YearClassID'],"ClassTitleEN" => $yearClass["ClassTitleEN"],"ClassTitleB5" => $yearClass["ClassTitleB5"]);
}
foreach($accessRight['subjectGroup'] as $subjectGroup){
	$sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
	// 2020-08-05 (Philips) - Check AcademicYearID - only allow current year
	$ytID = $sGroup->YearTermID[0];
	$layt = new academic_year_term($ytID);
	$_thisAcademicYearID = $layt->AcademicYearID;
	unset($layt);
	if($_thisAcademicYearID!=$currentAcademicYearID) continue;
    $studentList = $sGroup->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=1, $showClassID = true);
    $classList = BuildMultiKeyAssoc($studentList, array("YearClassID"), array("ClassTitleB5","ClassTitleEN"));
    foreach($classList as $classID => $classDetail){
    	$yearClassArray[$classID] = array("YearClassID"=>$classID,"ClassTitleEN" => $classDetail["ClassTitleEN"],"ClassTitleB5" => $classDetail["ClassTitleB5"]);
    }
}
// 2020-08-05 (Philips) - Sort by TitleEN
sortByColumn2($yearClassArray,'ClassTitleEN');
foreach($yearClassArray as $yearClass){
	$yearClassID = $yearClass['YearClassID'];
    $name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
    $classSelectionHTML .= "<option value=\"{$yearClassID}\">{$name}</option>";
    $classTeacherClassIdArr[] = $yearClassID;
}
$classSelectionHTML .= '</select>';
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "student_marksheets";
// $CurrentPageName = $Lang['SDAS']['menu']['StudentPerformanceTracking'];
$CurrentPageName = $Lang['SDAS']['StudentPerformanceTracking']['Marksheet'];
$TAGS_OBJ[] = array($CurrentPageName,"", true);
$TAGS_OBJ[] = array($Lang['SDAS']['StudentPerformanceTracking']['Percentile'],"?t=academic.student_marksheets.search2");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);

$FromAcademicYearSelection = getSelectAcademicYear('FromAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value, \'From\')"', $noFirst=1, $noPastYear=0, $FromAcademicYearID);
$ToAcademicYearSelection = getSelectAcademicYear('ToAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value, \'To\')"', $noFirst=1, $noPastYear=0, $ToAcademicYearID);

$FromSubjectSelection = $libSCM_ui->Get_Subject_Selection('FromSubjectID', $FromSubjectID, $OnChange='', $noFirst=0, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);
$ToSubjectSelection = $libSCM_ui->Get_Subject_Selection('ToSubjectID', $ToSubjectID, $OnChange='', $noFirst=0, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()','',array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();
?>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV(){
	$('#isExport').val(1);
	$('#form1').submit();
	$('#isExport').val(0);
}

</script>

<script language="JavaScript">
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});
var data;
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFromAcademicYearID = '<?=$FromAcademicYearID?>';
var jsCurFromYearTermID = '<?=$FromYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFromAcademicYearID == '') {
		jsCurFromAcademicYearID = $('select#FromAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	js_Reload_Term_Selection('From');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	
	js_Reload_Term_Selection('To', jsRefreshDBTable);
	 $("#filterMark").attr("checked", "checked");

});

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
	js_Reload_Student_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: "js_Reload_Student_Selection()",
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}



function js_Reload_Student_Selection()
{
	$('div#StudentSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Student_Selection',		
			SelectedYearClassID: $('select#YearClassID').val(),
			SelectionID: 'StudentID',
			OnChange: "",
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			//jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}
$(function(){
	js_Reload_Student_Selection();
});

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTermID = jsCur' + jsType + 'YearTermID;');
	eval('var jsSelectionID = "' + jsType + 'YearTermID";');
	
	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Term_Selection',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsYearTermID,
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
			NoFirst: 0,
			DisplayAll: 1,
			FirstTitle: '<?=$Lang['General']['WholeYear']?>'
		},
		function(ReturnData)
		{
			eval('jsCur' + jsType + 'YearTermID = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
}

function js_Reload_DBTable()
{
	$("#PrintBtn").hide();
	$("#ExportBtn").hide();
	if($('#YearClassID').length == 0 || $('#YearClassID').val() == ''){
		alert('<?=$Lang['General']['JS_warning']['SelectClass']?>');
		return;
	}
	if($('#StudentID').length == 0 || $('#StudentID').val() == ''){
		alert('<?=$Lang['General']['JS_warning']['SelectStudent']?>');
		return;
	}
	if($('.displayField:checked').length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectColumn']?>');
		return;
	}
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_get_mark_analysis.php", 
		/*{
			Action: 'STUDENT_RESULTS',
			StudentID: $('select#StudentID').val()			
		},*/
		$("#form1").serialize(),
		function(ReturnData)
		{
			if(ReturnData.search('<?=$Lang['SDAS']['NoRecord']['A']?>')==-1){
				$("#PrintBtn").show();
				$("#ExportBtn").show();
			} else{
				$("#PrintBtn").hide();
				$("#ExportBtn").hide();
			}
		}
	);
}



</script>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" action="/home/portfolio/profile/management/ajax_get_mark_analysis.php" method="POST" target="_blank">
	<input type="hidden" name="Action" value="STUDENT_RESULTS" />
	<input type="hidden" name="fileName" value="Student marksheets" />
	<input type="hidden" id="isExport" name="isExport" value="0" />
	<?=$html_tab_menu ?>
	
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
		
<?php if($accessRight['admin']){ ?>
			<tr>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
				<td valign="top"><?=$FormSelection?></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
				<td valign="top"><div id="ClassSelectionDiv"></div></td>
			</tr>
<?php }else{ ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
				<td valign="top">
					<?=$classSelectionHTML?>
				</td>
			</tr>
<?php } ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort['usertype_s']?></span></td>
				<td valign="top"><div id="StudentSelectionDiv"></div></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['display']?></span></td>
				<td valign="top">
					<input type="checkbox" id="filterAll" id="filterAllA" />
					<label for="filterAll"><?=$Lang['Btn']['SelectAll']?></label>&nbsp;&nbsp;&nbsp;
					
					<input type="checkbox" id="filterMark" name="filterColumn[]" value="filterMark" class="displayField" />
					<label for="filterMark"><?=$Lang['iPortfolio']["Score"]?></label>&nbsp;
					
					<?php if($sys_custom['iPf']['Report']['AssessmentStatisticReport']['showMarksDifferent']){ ?>
						<input type="checkbox" id="filterMarkDiff" name="filterColumn[]" value="filterMarkDiff" class="displayField" />
						<label for="filterMarkDiff"><?=$Lang['iPortfolio']["MarkDifference"]?></label>&nbsp;
					<?php } ?>
					
					<input type="checkbox" id="filterStandardScore" name="filterColumn[]" value="filterStandardScore" class="displayField" />
					<label for="filterStandardScore"><?=$Lang['iPortfolio']["StandardScore"]?></label>&nbsp;
					
					<input type="checkbox" id="filterStandardScoreDiff" name="filterColumn[]" value="filterStandardScoreDiff" class="displayField" />
					<label for="filterStandardScoreDiff"><?=$Lang['iPortfolio']["StandardScoreDifference"]?></label>&nbsp;
					
					<input type="checkbox" id="filterFormPosition" name="filterColumn[]" value="filterFormPosition" class="displayField" />
					<label for="filterFormPosition"><?=$Lang['iPortfolio']["FormPosition"]?></label>&nbsp;
					
					<input type="checkbox" id="filterFormPositionDiff" name="filterColumn[]" value="filterFormPositionDiff" class="displayField" />
					<label for="filterFormPositionDiff"><?=$Lang['iPortfolio']["FormPositionDifference"]?></label>&nbsp;
					<?php if($sys_custom['SDAS']['HKTLC']['FormPercentile']){?>
						<input type="checkbox" id="filterFormPositionPercentile" name="filterColumn[]" value="filterFormPositionPercentile" class="displayField" />
						<label for="filterFormPositionPercentile"><?=$Lang['SDAS']['StudentPerformanceTracking']['FormPercentile']?></label>&nbsp;
					<?php }?>
				</td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["additionalInformation"]?></span></td>
				<td valign="top">
					
			<?php if(!isset($sys_custom['SDAS']['SKHTST'])){ ?>
					<input type="checkbox" id="filterTeacher" name="filterColumn[]" value="filterTeacher" />
					<label for="filterTeacher"><?=$iPort['report_col']['teacher']?></label>&nbsp;
					
			<?php }?>
			
					<input type="checkbox" id="filterFormSplineDiagram" name="filterColumn[]" value="filterFormSplineDiagram">
					<label for="filterFormSplineDiagram"><?=$Lang['SDAS']['SplineDiagram']?></label>
					
					<?php if($sys_custom['SDAS']['HKTLC']['FormPercentile']){?>
						<input type="checkbox" id="filterFormPercentileSplineDiagram" name="filterColumn[]" value="filterFormPercentileSplineDiagram">
					<label for="filterFormPercentileSplineDiagram"><?=$Lang['SDAS']['StudentPerformanceTracking']['FormPercentile']?><?=$Lang['SDAS']['SplineDiagram']?></label>
					<?php }?>
				</td>
			</tr>
		</table>
	
	<span class="tabletextremark">
			<?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?>
	</span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
	
</form>


<script>
$('#filterAll').change(function(){
	if($(this).attr('checked')){
		$(this).parent().find('input[name="filterColumn\[\]"]').attr('checked','checked');
	}else{
		$(this).parent().find('input[name="filterColumn\[\]"]').removeAttr('checked');
	}
});
$('input[name="filterColumn\[\]"]').click(function(){
	if(!$(this).attr("checked")){
		$(this).parent().find("#filterAll").removeAttr("checked");
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>