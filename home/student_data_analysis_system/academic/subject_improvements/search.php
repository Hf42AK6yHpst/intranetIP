<?php
//Using:
/**
 * Change Log:
 * 2020-06-08 Philips [2019-0515-1356-44170]
 *  -   use YearTerm instead of YearTermID, Replace From by Fr in Subject Select box
 * 2020-05-15 Philips [DM3752]
 *  -	Replace From by Fr to avoid blocked ajax request
 * 2019-10-03 Philips
 *  -	Fix Subject Panel subject selection
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2017-03-13 Villa
 *  -	fifler out irresponsible subject if subject panel
 * 2017-01-12 Villa
 *  -	support subject component
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-06-23 (Cara):
 *  - Changed default value of FrYearTerm and ToYearTerm
 * 2015-02-17 (Pun):
 *  - Changed type filter lang
 * 
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
$sbj = new subject();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
######## Access Right END ########

######## Subject Selection START ########
if(!$accessRight['admin']){
	$subjectIDAry = array();
	foreach($accessRight['classTeacher'] as $class){
		$cYearID = $class['YearID'];
		$subjects = $sbj->Get_Subject_By_Form($cYearID);
		$sIDAry = Get_Array_By_key($subjects, 'RecordID');
		$subjectIDAry = array_merge($subjectIDAry, $sIDAry);
	}
	foreach($accessRight['subjectGroup'] as $subjectGroup){
		$sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
		$subjectIDAry[] = $sGroup->SubjectID;
	}
}

if($accessRight['admin']){
	$subjectIdArr = '';
}else{
	$subjectIdArr = array_keys((array)$accessRight['subjectPanel']);
	$subjectIDAry = array_merge($subjectIdArr, $subjectIDAry);
}
######## Subject Selection END ##########

######## Page Setting START ########
$CurrentPage = "subject_improvements";
$CurrentPageName = $Lang['SDAS']['menu']['SubjectImprovementStats'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$YearArr = $li_pf->returnAssessmentYear();

//encode $yearArr Fr b5 to UTF8
$NumOfYearArr = count($YearArr);
for($i=0;$i<$NumOfYearArr;$i++){
	$YearArr[$i]['Year'] = convert2unicode($YearArr[$i]['Year'], $isAbsOn="1", $direction=1);
}

$FrAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FrAcademicYearID' id='FrAcademicYearID' onchange=\"js_Reload_Term_Selection('Fr');js_Reload_Term_Selection('To');\"", $CurrentAcademicYearID, 0, 0, "", 2);

// $FrSubjectSelection = $libSCM_ui->Get_Subject_Selection('FrSubjectID', $FrSubjectID, $OnChange='', $noFirst=1, '', '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr=$subjectIdArr);
$FrSubjectSelection = $libSDAS->Get_Subject_Selection($subjectIDAry);
$FrSubjectSelection = str_replace('From','Fr',$FrSubjectSelection);
//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
?>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};
</script>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFrAcademicYearID = '<?=$FrAcademicYearID?>';
var jsCurFrYearTerm = '<?=$FrYearTerm?>';
var jsCurToAcademicYearID = '<?=$FrAcademicYearID?>';
var jsCurToYearTerm = '<?=$ToYearTerm?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFrAcademicYearID == '') {
		jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#FrAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	//js_Reload_Term_Selection('Fr');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	  $("select[name=FrAcademicYearID]").change();
	  $("#resultType_Class").attr("checked", "checked");
	
	//js_Reload_Term_Selection('To', jsRefreshDBTable);
});

function isInt(value) {
	  return !isNaN(value) && 
	         parseInt(Number(value)) == value && 
	         !isNaN(parseInt(value, 10));
	}

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: '',
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTerm;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTerm, jsType)
{
	eval('jsCur' + jsType + 'YearTerm = jsYearTerm;');
}

function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	jsCurToAcademicYearID = $('select#FrAcademicYearID').val();
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTerm = jsCur' + jsType + 'YearTerm;');
	eval('var jsSelectionID = "' + jsType + 'YearTerm";');

	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Term_Assessment_Selection',
			AcademicYearID: jsAcademicYearID,
			YearTerm: jsYearTerm,
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
			NoFirst: 1,
			DisplayAll: 1,
			FirstTitle: '',
			OnlyHasRecord: 1, //Show termAssignment that contains data
			showOverAll: 1 // Show the overall option of the selection box
		},
		function(ReturnData)
		{
			$('select#' + jsSelectionID + ' > option').each (function () {
				var _optionVal = $(this).val();	

				if (isInt(_optionVal) && parseInt(_optionVal) > 0) {
					$('select#' + jsSelectionID).val(_optionVal);

					if (jsType=='Fr') {
						return false;	// break;
					}
				}
			});

			eval('jsCur' + jsType + 'YearTerm = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
	js_Reload_Class_Selection();
}
function js_Reload_DBTable()
{
	$("#PrintBtn").hide();
	if( $('#FrAcademicYearID').val() == ""){
		alert("<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>");
		return;
	}
	
	if (typeof(document.form1.FrYearTerm)=="undefined" || typeof(document.form1.ToYearTerm)=="undefined")
	{
		alert("<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Term']?>");
		return;
	}

	var Fr = $('#FrYearTerm').val().split('_');
	var FrYearTerm = Fr[0];
	var FrTermAssessment = (Fr.length == 2)?Fr[1]:'';
	var to = $('#ToYearTerm').val().split('_');
	var toYearTerm = to[0];
	var toTermAssessment = (to.length == 2)?to[1]:'';
	initSearchDetail(
		$('#FrAcademicYearID').val(),
		FrYearTerm,
		FrTermAssessment,
		toYearTerm,
		toTermAssessment,
		$('#FrSubjectID').val()
	);
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_get_mark_analysis.php", 
		$("#form1").serialize(),
		function(ReturnData)
		{
			$("#PrintBtn").show();	
		}
	);

	
}
</script>

<form id="form1" name="form1" method="POST" action="advancement.php">
	<input type="hidden" name="Action" value="SUBJECT_IMPROVE" />
	<?=$html_tab_menu ?>
					<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
						<tr>
							<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
							<td valign="top"><?=$FrAcademicYearSelection?></td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["compare"]?></span></td>
							<td valign="top"><table border="0"><tr><td><div id="FrYearTermSelectionDiv" style="float:left;">--</div></td><td> VS </td><td><div id="ToYearTermSelectionDiv" style="float:left;">--</div></td></tr></table></td>
						</tr>
						<tr id='classRow'>
							<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
							<td valign="top"><?=$FrSubjectSelection?></td>
						</tr>
						<tr id='classRow'>
							<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["type"]?></span></td>
							<td valign="top">
								<input type="checkbox" id="resultType_SelectAll" />
								<label for="resultType_SelectAll"><?=$Lang['Btn']['SelectAll']?></label> &nbsp;&nbsp;&nbsp;
								
								<input type="checkbox" id="resultType_Form" name="ResultType[]" value="form" />
								<label for="resultType_Form"><?=$ec_iPortfolio['form']?></label> &nbsp;
								<input type="checkbox" id="resultType_Class" name="ResultType[]" value="class" />
								<label for="resultType_Class"><?=$ec_iPortfolio['class']?></label> &nbsp;
								<input type="checkbox" id="resultType_Group" name="ResultType[]" value="group" />
								<label for="resultType_Group"><?=$iPort["SubjectGroup"]?></label> &nbsp;
							</td>
						</tr>
					</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
				
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
	</div>
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
	
</form>

<form id="searchDetail" action="/home/portfolio/profile/management/ajax_get_improved_details.php" target="_blank" style="display:none">
	<input id="searchDetail_AcademicYearID" name="AcademicYearID" />
	<input id="searchDetail_FrYearTerm" name="FrYearTerm"/>
	<input id="searchDetail_FrTermAssessment" name="FrTermAssessment" />
	<input id="searchDetail_ToYearTerm" name="ToYearTerm" />
	<input id="searchDetail_ToTermAssessment" name="ToTermAssessment" />
	<input id="searchDetail_SubjectID" name="SubjectID" />
	
	<input type="submit" />
</form>

<script>

function resetSearchDetail(){
	$('#searchDetail input[name="StudentID\[\]"]').remove();
}
function initSearchDetail(AcademicYearID, FrYearTerm, FrTermAssessment, ToYearTerm, ToTermAssessment, SubjectID){
	$('#searchDetail_AcademicYearID').val(AcademicYearID);
	$('#searchDetail_FrYearTerm').val(FrYearTerm);
	$('#searchDetail_FrTermAssessment').val(FrTermAssessment);
	$('#searchDetail_ToYearTerm').val(ToYearTerm);
	$('#searchDetail_ToTermAssessment').val(ToTermAssessment);
	$('#searchDetail_SubjectID').val(SubjectID);
}
function submitSearchDetail(StudentIdArr){
	resetSearchDetail();
	$.each(StudentIdArr, function (index, element){
		$('#searchDetail').append('<input name="StudentID[]" value="' + element + '" />');
	});
	$('#searchDetail').submit();
}



$("#resultType_SelectAll").click(function(){
	if($(this).attr("checked")){
		$("#resultType_Form").attr("checked", "checked");
		$("#resultType_Class").attr("checked", "checked");
		$("#resultType_Group").attr("checked", "checked");
	}else{
		$("#resultType_Form").removeAttr("checked");
		$("#resultType_Class").removeAttr("checked");
		$("#resultType_Group").removeAttr("checked");
	}
});
$("#resultType_Form, #resultType_Class, #resultType_Group").click(function(){
	if(!$(this).attr("checked")){
		$("#resultType_SelectAll").removeAttr("checked");
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>