<?php
// using :
/**
 * Change Log:
 */
include_once ($PATH_WRT_ROOT . "includes/json.php");
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
$libSDAS = new libSDAS();
$Json = new JSON_obj();
$libpf_exam = new libpf_exam();
$subjectIdAssoc = $libpf_exam->getSubjectIDWebSAMSMapping();

$configArr = array();

// ######### Junior (Start)#############
$configArr['junior']['classLevel'] = $_POST['juniorFormCheckBoxs'];
$configArr['junior']['Repeat'][1]['Subject'] = $_POST['rule1Subject_Repeat'];
$configArr['junior']['Repeat'][2]['Subject'] = $_POST['rule2Subject_Repeat'];
$configArr['junior']['Repeat'][1]['ScoreRequire'] = $_POST['rule1SubjectScore_Repeat'];
$configArr['junior']['Repeat'][2]['ScoreRequire'] = $_POST['rule2SubjectScore_Repeat'];
$configArr['junior']['TryPromote'][1]['Subject'] = $_POST['rule1Subject_TryPromote'];
$configArr['junior']['TryPromote'][2]['Subject'] = $_POST['rule2Subject_TryPromote'];
$configArr['junior']['TryPromote'][1]['ScoreRequire'] = $_POST['rule1SubjectScore_TryPromote'];
$configArr['junior']['TryPromote'][2]['ScoreRequire'] = $_POST['rule2SubjectScore_TryPromote'];
// ######### Junior (End)#############
// ######### Senior (Start)#############
$configArr['senior']['classLevel'] = $_POST['seniorFormCheckBoxs'];
// CEML Wegiht
$configArr['senior']['coreSubject'] = $_POST['coreSubjectCheckBoxs'];
$configArr['senior']['CEMLWeighting'] = array(
    $subjectIdAssoc['080'] => $_POST['Chinese_Weighting'],
    $subjectIdAssoc['165'] => $_POST['English_Weighting'],
    $subjectIdAssoc['22S'] => $_POST['Math_Weighting'],
    $subjectIdAssoc['265'] => $_POST['LS_Weighting']
);
$configArr['senior']['Repeat']['CEMLScore'] = $_POST['CEML_Repeat'];
$configArr['senior']['Repeat']['electives']['numOfElectives'] = $_POST['NumOfElective_Repeat'];
$configArr['senior']['Repeat']['electives']['scoreRequirement'] = $_POST['ElectiveScore_Repeat'];
$configArr['senior']['TryPromote']['CEMLScore'] = $_POST['CEML_TryPromote'];
$configArr['senior']['TryPromote']['electives']['numOfElectives'] = $_POST['NumOfElective_TryPromote'];
$configArr['senior']['TryPromote']['electives']['scoreRequirement'] = $_POST['ElectiveScore_TryPromote'];

// ######### Senior (End)#############

$configJson = $libSDAS->Get_Safe_Sql_Query($Json->encode($configArr));
$rs = $libSDAS->GetPromotionConfig();
if (empty($rs)) { // NoRecord
    $sql = "INSERT INTO 
				GENERAL_SETTING 
				(Module, SettingName, SettingValue, DateInput, InputBy, DateModified, ModifiedBy)
			VALUES
				('SDAS', 'Promotion_Config', '$configJson', now(), '{$_SESSION['UserID']}', now(), '{$_SESSION['UserID']}');
			";
} else {
    $sql = "
			UPDATE
				GENERAL_SETTING
			SET
				SettingValue = '$configJson', DateModified = now(), ModifiedBy = '{$_SESSION['UserID']}'
			WHERE
				Module = 'SDAS'
			AND
				SettingName = 'Promotion_Config'
			";
}
$libSDAS->db_db_query($sql);
header('location:?t=reports.promotion.config');