<?php
// using :
/**
 * Change Log:
 */

// ####### Init START ########
include_once ($PATH_WRT_ROOT . "includes/json.php");
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$jsonDecoder = new JSON_obj();
// ####### Init END ########

// ####### Access Right START ########
// ####### Access Right END ########

// ####### Page Setting START ########
$CurrentPage = "report_promotion";
$CurrentPageName = $Lang['SDAS']['Promotion']['ClassPromotionAssessment'];
$TAGS_OBJ[] = array(
    $CurrentPageName,
    ""
);
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
// ####### Page Setting END ########

// ##Data Start
$configInfoArr = $jsonDecoder->decode($libSDAS->GetPromotionConfig());
$JuniorForm_Array = $configInfoArr['junior']['classLevel']; // Junior Form
$SeniorForm_Array = $configInfoArr['senior']['classLevel']; // Senior Form
                                                            
// ##Data End
                                                            
// ####### UI Releated START ########

$aYearAssocArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID', array(
    Get_Lang_Selection('YearNameEN', 'YearNameB5')
), 1);
$yearSelectArr = array();
foreach ((array) $aYearAssocArr as $yearID => $yearName) {
    $yearSelectArr[$yearID] = $yearName;
}
$AcademicYearSelection = getSelectByAssoArray($yearSelectArr, "name='FromAcademicYearID' id='FromAcademicYearID'", Get_Current_Academic_Year_ID(), 0, 1, "");

// ## Year Selection Start
$sql = "SELECT YearID, YearName, WebSAMSCode FROM YEAR ORDER BY WebSAMSCode, Yearname";
$yearArr = $libSDAS->returnResultSet($sql);
$yearAssocArr = array();
$yearSelection = '';
$yearSelection .= '<select name="YearID" id="YearID">';
foreach ((array) $yearArr as $_yearInfo) {
    $_yearID = $_yearInfo['YearID'];
    if (in_array($_yearID, (array) $JuniorForm_Array)) {
        $formType = 'J';
    } elseif (in_array($_yearID, (array) $SeniorForm_Array)) {
        $formType = 'S';
    } else {
        $formType = 'U';
    }
    $yearSelection .= '<option value="' . $_yearInfo['YearID'] . '" data-type="' . $formType . '">';
    $yearSelection .= $_yearInfo['YearName'];
    $yearSelection .= '</option>';
}
$yearSelection .= '</select>';
// ##Year Selection END

$sql = "
		SELECT
			ass.RecordID as SubjectID,
            lc.LearningCategoryID,
            if(ass.CH_DES != '' AND ass.CH_DES IS NOT NULL, ass.CH_DES, ass.EN_DES) as subjectChi,
            ass.EN_DES as subjectEn,
            if(lc.NameChi != '' AND lc.NameChi IS NOT NULL, lc.NameChi, lc.NameEng) as categoryChi,
            lc.NameEng as categoryEn
		FROM
			ASSESSMENT_SUBJECT ass
		INNER JOIN
			LEARNING_CATEGORY lc ON lc.LearningCategoryID = ass.LearningCategoryID
		WHERE
			CMP_CODEID IS NULL OR CMP_CODEID = ''
        ORDER BY
            lc.DisplayOrder, ass.DisplayOrder
		";
$subjectArr = $libSDAS->returnResultSet($sql); // subject name and subject category

$subjectWithCategoryAssoc = BuildMultiKeyAssoc($subjectArr, array(
    Get_Lang_Selection('categoryChi', 'categoryEn'),
    'SubjectID'
), array(
    Get_Lang_Selection('subjectChi', 'subjectEn')
), 1);
$selectInfoAssoc = array_merge(array(
    'Weighting Average' => array(
        '-999' => $Lang['SDAS']['Promotion']['WeightedAverage']
    )
), $subjectWithCategoryAssoc);

$ruleType = array(
    'Repeat',
    'TryPromote'
);
$juniorRuleHTMLs = array();
foreach ($ruleType as $_type) {
    for ($i = 1; $i <= 2; $i ++) {
        $__subjectId = $configInfoArr['junior'][$_type][$i]['Subject'];
        $__scoreRequire = $configInfoArr['junior'][$_type][$i]['ScoreRequire'];
        
        // for edit
        $_selectionName = 'rule' . $i . 'Subject_' . $_type;
        $_inputName = 'rule' . $i . 'SubjectScore_' . $_type;
        
        $_html = getSelectByAssoArray($selectInfoAssoc, "id='$_selectionName' name='$_selectionName'", $__subjectId, $all = 0, $noFirst = 1);
        $_html .= " ≤ ";
        $_html .= "<input type='number' class='required' id='$_inputName' name='$_inputName' min='0' max='999' size='3' value='$__scoreRequire'>";
        $juniorRuleHTMLs[$_type][$i] = $_html;
    }
}

$andOrOptions = array(
    'and' => 'And',
    'or' => 'Or'
);
$andOrSelections = array();
$andOrSelections['junior']['Repeat'] = getSelectByAssoArray($andOrOptions, 'id="juniorAndOr_Repeat" name="andOr[junior][Repeat]"', '', 0, 1);
$andOrSelections['junior']['TryPromote'] = getSelectByAssoArray($andOrOptions, 'id="juniorAndOr_TryPromote" name="andOr[junior][TryPromote]"', '', 0, 1);
$andOrSelections['senior']['Repeat'] = getSelectByAssoArray($andOrOptions, 'id="seniorAndOr_Repeat" name="andOr[senior][Repeat]"', '', 0, 1);
$andOrSelections['senior']['TryPromote'] = getSelectByAssoArray($andOrOptions, 'id="seniorAndOr_TryPromote" name="andOr[senior][TryPromote]"', '', 0, 1);

$htmlAry['generateBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['View'], "Button", "", 'generateBtn', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "formbutton ");

$htmlArr['hidden'] = '';
$htmlArr['hidden'] .= $linterface->GET_HIDDEN_INPUT('YearType', 'YearType', '');
// ####### UI Releated END ########

// ####### UI START ########

$linterface->LAYOUT_START();
?>
<style>
.tablesorter-default .header,
.tablesorter-default .tablesorter-header {
	padding: 4px 20px 4px 4px;
	cursor: pointer;
	background-image: url(data:image/gif;base64,R0lGODlhFQAJAIAAAP///////yH5BAEAAAEALAAAAAAVAAkAAAIXjI+AywnaYnhUMoqt3gZXPmVg94yJVQAAOw==);
	background-position: center right;
	background-repeat: no-repeat;
}
.tablesorter-default .headerSortUp,
.tablesorter-default .tablesorter-headerSortUp,
.tablesorter-default .tablesorter-headerAsc {
	background-image: url(data:image/gif;base64,R0lGODlhFQAEAIAAAP///////yH5BAEAAAEALAAAAAAVAAQAAAINjI8Bya2wnINUMopZAQA7);
	color: #fff;
}
.tablesorter-default .headerSortDown,
.tablesorter-default .tablesorter-headerSortDown,
.tablesorter-default .tablesorter-headerDesc {
	color: #fff;
	background-image: url(data:image/gif;base64,R0lGODlhFQAEAIAAAP///////yH5BAEAAAEALAAAAAAVAAQAAAINjB+gC+jP2ptn0WskLQA7);
}
</style>
<script type="text/javascript" src="/templates/jquery/tablesorter/jquery.tablesorter.min.js"></script>
<script>
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

$(document).ready( function() {

	// function
	var validateForm = function(){
		var valid = true; 
		$('.required').each( function() {
			if($(this).val() == ''){
				$(this).focus();
				alert('<?php echo $Lang['SDAS']['Promotion']['WarningEmpty']?>');
				valid = false;
				return false;
			}
		});
		return valid;
	}
	
	// listener
	$( "#FromAcademicYearID" ).change(function() {
		var ay_id = $('#FromAcademicYearID').val();
		var tag = "id=YearTermID name=YearTermID";
		$("#ayterm_cell").html(loadingImage);

		$.ajax({
			type: "GET",
			url: "index.php?t=ajax.ajax_get_year_term_id_selection",
			data:	{
						"AcademicYearID":ay_id,
						"tag":tag
					},	
			success: function (msg) {
				$("#ayterm_cell").html(msg);
			}
		});
	});
	
	$( "#YearID" ).change(function() {
		classyearChange();
	});

	$( "#generateBtn" ).click(function() {
		var valid = validateForm();
		if(valid){
			$('div#ajaxResult').html($(loadingImage));
			url = 'index.php?t=reports.promotion.ajax';
			$.ajax({
		           type:"POST",
		           url: url,
		           data: $("#form1").serialize(),
		           success:function(result)
		           {
		           		$('div#ajaxResult').html(result);
		           		$(".dataTable").each(function(){
		            		$(this).tablesorter();
		            	});
		           }
		         })
		}
	});

	// on ready do
	$("#FromAcademicYearID" ).change();
	$( "#YearID" ).change();
});

function classyearChange(){
	yearType = $('#YearID option:selected').data().type;
	$('#YearType').val(yearType);

	if( yearType == 'J'){
		$('.juniorRule').show();
		$('.seniorRule').hide();
	} else if ( yearType == 'S'){
		$('.juniorRule').hide();
		$('.seniorRule').show();
	}else{
		$('.juniorRule').hide();
		$('.seniorRule').hide();
	}
}
</script>
<form id="form1" name="form1" method="POST">
	<table border="0" cellspacing="0" cellpadding="5"
		class="form_table_v30" style="max-width: 1024px;">
		<tr>
			<td class="field_title"><span class="tabletext"><?php echo $ec_iPortfolio['year']?></span></td>
			<td valign="top"><?php echo $AcademicYearSelection ?>&nbsp;<span
				id='ayterm_cell'></span></td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletext"><?php echo $ec_iPortfolio_Report['form']?></span></td>
			<td valign="top"><?php echo $yearSelection?></td>
		</tr>
		<?php foreach ($ruleType as $_type):?>
    		<tr class="juniorRule">
			<td class="field_title">
    				<?php echo $Lang['SDAS']['Promotion'][$_type].$Lang['SDAS']['Promotion']['Rule']?>
    			</td>
			<td>
    				<?php echo implode('<br><br>'.$andOrSelections['junior'][$_type].'<br><br>', $juniorRuleHTMLs[$_type])?>
    			</td>
		</tr>
		<?php endforeach;?>
		<?php foreach ($ruleType as $_type):?>
    		<tr class="seniorRule">
			<td class="field_title"><?php echo $Lang['SDAS']['Promotion'][$_type].$Lang['SDAS']['Promotion']['Rule'] ?></td>
			<td><input type='number' class='required'
				id='NumOfElective_<?php echo $_type?>'
				name='NumOfElective_<?php echo $_type?>' min='1' max='9' size='1'
				value='<?php echo $configInfoArr['senior'][$_type]['electives']['numOfElectives']?>'>
    				<?php echo $Lang['SDAS']['Promotion']['ElectiveSubject']?>
    				 ≤ 
    				<input type='number' class='required'
				id='ElectiveScore_<?php echo $_type?>'
				name='ElectiveScore_<?php echo $_type?>' min='0' max='999' size='3'
				value='<?php echo $configInfoArr['senior'][$_type]['electives']['scoreRequirement']?>'>
				<br> <br>
    				<?php echo $andOrSelections['senior'][$_type]?>
    				<br> <br>
    				<?php echo $Lang['SDAS']['Promotion']['WeightedAverageCEML']?>
    				 ≤ 
    				<input type='number' class='required'
				id='CEML_<?php echo $_type?>' name='CEML_<?php echo $_type?>'
				min='0' max='999' size='3'
				value='<?php echo $configInfoArr['senior'][$_type]['CEMLScore']?>'>
			</td>
		</tr>
		<?php endforeach;?>
	</table>
	<?php echo $htmlArr['hidden']?>
	<div class="edit_bottom_v30">
		<?php echo $htmlAry['generateBtn']?>
	</div>
	<div id="ajaxResult"></div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();