<?php
# Modifing by: anna


############################
#	Change Log
#	Date	: 2017-06-21 Villa
#			modified reload_endorse_table - add DSE APPEAL
#	Date	: 2017-06-16 Villa
#			add DSE APPEAL
#			change $data[1][2] to defind EXAMTYPE in stead of using $i
#	Date	: 2017-06-06 Omas
#			fix php5.4 warning
#	Date	: 2016-03-21 Omas
#			add support to endorse, verify, resubmit workflow
#	Date	: 2016-03-22 Omas
#			add support to year-end exam
#	2016-02-19 Kenneth
#	Page Created
############################


// $PATH_WRT_ROOT = '../../../../';
// ######## Init START ########
// include_once($PATH_WRT_ROOT."includes/global.php");
// include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.en.php");

######## Init END ########

include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();


$academicYearId = $_POST['ay_id'];
$action = $_POST['action'];

if($action == "reload_table"){
	
	include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
	$lcees = new libcees();
	$submissionAssoAry = $lcees->getScoreSubmissionPeriodFromCentralServer($academicYearId);
	$verifyAry = $lcees->getVerifyStatusFromCentralServer($academicYearId,2);
// 	$SchoolInfo = $lcees->getAllSchoolInfoFromCentralServer('S');
/* 	debug_pr($verifyAry); */
	$dseEndorseInfo = $lcees->getCeesSyncLog($Cond=' And RecordStatus>0 ',$OrderBy='',$LogID='',$ExamType = 2,'',$academicYearId,'', 'CONFIRM');
	$dseEndorseInfoAssoc = BuildMultiKeyAssoc($dseEndorseInfo, 'AcademicYearID', array('RecordStatus'),1);
	
	# Header Row
	$HeaderRow="<tr class=\"tabletop\">";
		$HeaderRow .= "<td width='25%'>{$Lang['SDAS']['toCEES']['RecordType']}</td>";
		$HeaderRow .= "<td width='25%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$Lang['SDAS']['toCEES']['Duration']."</td>";
		$HeaderRow .= "<td width='10%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$Lang['SDAS']['toCEES']['DataFile']."</td>";
		$HeaderRow .= "<td width='15%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$iPort['btn']['submit']."</td>";
		$HeaderRow .= "<td width='25%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$Lang['SDAS']['toCEES']['LastUpdated']."</td>";
	$HeaderRow .= "</tr>";	
	
	
	# Data
	//$dataFileLink = $PATH_WRT_ROOT.'home/student_data_analysis_system/management/import_exam_result/data_export.php';
	/**
	 * Pre-S1 exam = 1
	 * HKDSE: exam = 2
	 * Mock-Exam = 3
	 */
	$data[0][0] = $Lang['SDAS']['Exam']['HKAT'];
	$data[0][1] = 'PreS1';	// key to map data from central server
	$data[0][2] = '1';
	$data[1][0] = $Lang['SDAS']['Exam']['Mock'];
	$data[1][1] = 'Mock';
	$data[1][2] = '3';
	$data[2][0] = $Lang['SDAS']['Exam']['HKDSE'];
	$data[2][1] = 'DSE';
	$data[2][2] = '2';
	$data[3][0] = $Lang['SDAS']['Exam']['HKDSE_APPEAL'];
	$data[3][1] = 'DSE_APPEAL';
	$data[3][2] = '7';
	$data[4][0] = $Lang['SDAS']['Exam']['FinalExam'];
	$data[4][1] = 'YearEndExam';
	$data[4][2] = '4';
	// missing 5 is for EXAMID_YEAREND_OVERALL
	$data[5][0] = 'JUPAS';
	$data[5][1] = 'JUPAS';
	$data[5][2] = '6';
	
	$data[6][0] = 'Exit To';
	$data[6][1] = 'ExitTo';
	$data[6][2] = '8';
	

	for($i=0; $i<7; $i++){
		$_examTypeCentral = $data[$i][1];
		$_submissionPeriodStart = $submissionAssoAry[$_examTypeCentral]['SubmitStartDate'];
		$_submissionPeriodEnd = $submissionAssoAry[$_examTypeCentral]['SubmitEndDate'];
		
		if ($_submissionPeriodStart=='' || $_submissionPeriodEnd=='') {
			// no submission period => just show "---"
			$_submissionPeriod = $Lang['General']['EmptySymbol'];
			$_updateBtn = $Lang['General']['EmptySymbol'];
		}
		else {
			// has submission period settings => show submission period
			$_submissionPeriod = $_submissionPeriodStart.' to '.$_submissionPeriodEnd;
			if (date('Y-m-d') >= $_submissionPeriodStart && date('Y-m-d') <= $_submissionPeriodEnd) {
				// within submission period => allow to submit score
				$_updateBtn = "<input name='button' type='button' value='".$iPort['btn']['submit']."' onClick='syncToCEES(\"".$data[$i][2]."\",\"".$academicYearId."\")' $ButtonClass>";
			}
			else {
				// outside submission period => don't allow to submit score
				$_updateBtn = $Lang['General']['EmptySymbol'];
			}
			
			// $i == 2 is DSE
// 			if($i == 2 ){
			if($data[$i][2] == 2){
				//$verifyAry
				// endorse 
				if(isset($verifyAry['endorse']) && $verifyAry['endorse'] == 1){
					$_updateBtn = $Lang['SDAS']['toCEES']['Endorsed'];
				}
				// verify
				if(isset($verifyAry['verify']) && $verifyAry['verify'] == 1){
					$_updateBtn = $Lang['SDAS']['toCEES']['VerifiedByCEES'];
				}
				// resubmit
				// if resubmitDate = endorseDate, that means already re-submit & re-endorsed
				//if(isset($verifyAry['resubmit']) && $verifyAry['resubmit'] == 1 && $verifyAry['resubmitDate'] != $verifyAry['endorseDate']){
				if((isset($verifyAry['resubmit']) && $verifyAry['resubmit'] == 1) && $verifyAry['endorse'] == 0){
					if($dseEndorseInfoAssoc[$academicYearId] > 0){
						// clear previouse dse endorse
						$lcees->cancelDseEndorsementOrSyncedLog($academicYearId, 2, 'CONFIRM');
						$lcees->cancelDseEndorsementOrSyncedLog($academicYearId, 2, '1');
					}
					$_updateBtn = "<input name='button' type='button' value='".$Lang['SDAS']['toCEES']['Resubmit']."' onClick='syncToCEES(\"".$data[$i][2]."\",\"".$academicYearId."\")' $ButtonClass>";
				}
			}elseif($data[$i][2] == 7){
				include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
				$libpf_exam = new libpf_exam();
				$haveAppealData = count($libpf_exam->getExamData($academicYearId , 7))> 0 ;
				
				$dseVerifyAry = $lcees->getVerifyStatusFromCentralServer($academicYearId,2);
// 				if($dseVerifyAry['endorse'] == 1){
				if(isset($dseVerifyAry['endorse']) && $dseVerifyAry['endorse'] == 1){
					$verifyAry = $lcees->getVerifyStatusFromCentralServer($academicYearId,7);
					if(isset($verifyAry['endorse']) && $verifyAry['endorse'] == 1){
						$_updateBtn = $Lang['SDAS']['toCEES']['Endorsed'];
					}
					// verify
					if(isset($verifyAry['verify']) && $verifyAry['verify'] == 1){
						$_updateBtn = $Lang['SDAS']['toCEES']['VerifiedByCEES'];
					}
					// resubmit
					// if resubmitDate = endorseDate, that means already re-submit & re-endorsed
					//if(isset($verifyAry['resubmit']) && $verifyAry['resubmit'] == 1 && $verifyAry['resubmitDate'] != $verifyAry['endorseDate']){
					if((isset($verifyAry['resubmit']) && $verifyAry['resubmit'] == 1) && $verifyAry['endorse'] == 0){
						if($dseEndorseInfoAssoc[$academicYearId] > 0){
							// clear previouse dse endorse
							$lcees->cancelDseEndorsementOrSyncedLog($academicYearId, 7, 'CONFIRM');
							$lcees->cancelDseEndorsementOrSyncedLog($academicYearId, 7, '1');
						}
						$_updateBtn = "<input name='button' type='button' value='".$Lang['SDAS']['toCEES']['Resubmit']."' onClick='syncToCEES(\"".$data[$i][2]."\",\"".$academicYearId."\")' $ButtonClass>";
					}
					if(!$haveAppealData && !(isset($verifyAry['endorse']) && $verifyAry['endorse'] == 1) && !(isset($verifyAry['verify']) && $verifyAry['verify'] == 1)){
						$_updateBtn = '<input type="checkbox" id="confirmAppealNoData"><label for="confirmAppealNoData">'.$Lang['SDAS']['toCEES']['confirmAppealNoData'].'</label><br>'.$_updateBtn;
					}
				}else{
					// not submitted DSE not allow to submit DSE Appeal
					if (date('Y-m-d') >= $_submissionPeriodStart && date('Y-m-d') <= $_submissionPeriodEnd) {
						// within submission period => allow to submit score
						$_updateBtn = $Lang['SDAS']['toCEES']['PleaseEndorseDSEfirst'];
					}else {
						// outside submission period => don't allow to submit score
						$_updateBtn = $Lang['General']['EmptySymbol'];
					}
				}
			}
		}
		
		##Last Update
		// Get Last Update (get last sync data log)
		$logInfoArr = $lcees-> getCeesSyncLog($Cond='',$OrderBy='DateInput DESC Limit 1',$LogID='',$data[$i][2],$UserID='',$academicYearId,$RecordStatus='1');
		
		$table[$i][0] = $data[$i][0];
		$table[$i][1] = $_submissionPeriod;
		$table[$i][2] = ($data[$i][2]!='')?'<a href="#" class="tablelink" onclick="downloadDataFile('.$data[$i][2].','.$academicYearId.');"> '.$Lang['Button']['Download'].'</a>':'-';
		$table[$i][3] = $_updateBtn;
// 		if($i == 2 ){
// show only sync time first
// 		if($data[$i][2] == 2 || $data[$i][2] == 7){
			//$table[$i][4] = (!empty($logInfoArr))? $logInfoArr[0]['DateInput'] : (!empty($verifyAry['endorseDate'])? $verifyAry['endorseDate'] : $Lang['General']['EmptySymbol']);
			//$table[$i][4] = (!empty($logInfoArr))? (!empty($verifyAry['endorseDate'])? $verifyAry['endorseDate'] :$logInfoArr[0]['DateInput']) : $Lang['General']['EmptySymbol'];
// 		}else{
			$table[$i][4] = (!empty($logInfoArr))? $logInfoArr[0]['DateInput'] : $Lang['General']['EmptySymbol'];
// 		}
	}
	
	$ButtonClass = "class=\"formsubbutton\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"";
	$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

	

	## AssessmentRow
	$AssessmentRow='';
	
	for($i=0; $i<7; $i++){
// 		if($i==3){
// 			continue;
// 		}
		$AssessmentRow .= "<tr valign=\"middle\" ".$PreviousClass.">";
			$AssessmentRow.="<td nowrap='nowrap'>".$table[$i][0]."</td>";
			$AssessmentRow.="<td nowrap='nowrap' align=center>".$table[$i][1]."</td>";
			$AssessmentRow.="<td nowrap='nowrap' align=center>".$table[$i][2]."</td>";
			$AssessmentRow.="<td nowrap='nowrap' align=center>".$table[$i][3]."</td>";
			$AssessmentRow.="<td nowrap='nowrap' align=center>".$table[$i][4]."</td>";
		$AssessmentRow.="</tr>";
	}
	
	$x =$HeaderRow.$AssessmentRow;
	echo $x;
	
}
else if($action == 'reload_endorse_table'){

	include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	$lcees = new libcees();
	$libdb = new libdb();

	$sql = "Select distinct ay.AcademicYearID From ACADEMIC_YEAR_TERM as ayt INNER JOIN ACADEMIC_YEAR as ay ON ayt.AcademicYearID = ay.AcademicYearID where YEAR(TermStart) > 2011 and YEAR(TermStart) < (Year(now())+1) ";
	$dseYearIDArr = $libdb->returnVector($sql);
	$submissionAssoAry = $lcees->getScoreSubmissionPeriodFromCentralServer($dseYearIDArr);
	$needtoSubmitYearIdArr = array_keys((array)$submissionAssoAry);
	$academicYearInfoAssoArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID', array('YearNameEN'),1);
	arsort($academicYearInfoAssoArr);
// 	$syncLogArr = $lcees->getCeesSyncLog($Cond=' And RecordStatus>0 ',$OrderBy='',$LogID='',$ExamType = 2,$UserID='',$ayID='',$RecordStatus='', $RecordType = '1');
// 	$syncLogAssocArr = BuildMultiKeyAssoc($syncLogArr, 'AcademicYearID', array('DateInput'),1);
// 	$endorseArr = $lcees->getCeesSyncLog($Cond=' And RecordStatus>0 ',$OrderBy='',$LogID='',$ExamType = 2,$UserID='',$ayID='',$RecordStatus='', $RecordType = 'CONFIRM');
// 	$endorseAssocArr = BuildMultiKeyAssoc($endorseArr, 'AcademicYearID', array('DateInput'),1);
	$verifyAry = array();
	$lastSyncDateAssoc = array();
	//DSE
	foreach((array)$needtoSubmitYearIdArr as $_ayID ){
		$tempVerifyAry = $lcees->getVerifyStatusFromCentralServer($_ayID,2);
		$verifyAry[$_ayID]['DSE'] = $tempVerifyAry;
		
		$tmpLog = $lcees->getCeesSyncLog($Cond='',$OrderBy='DateInput DESC Limit 1',$LogID='',2,$UserID='',$_ayID,$RecordStatus='1');
		if(!empty($tmpLog)){
			$lastSyncDateAssoc[$_ayID][2] = $tmpLog[0]['DateInput'];
		}
	}
	//DSE_APPEAL
	foreach((array)$needtoSubmitYearIdArr as $_ayID ){
		$tempVerifyAry = $lcees->getVerifyStatusFromCentralServer($_ayID,7);
		$verifyAry[$_ayID]['DSE_APPEAL'] = $tempVerifyAry;
		
		$tmpLog = $lcees->getCeesSyncLog($Cond='',$OrderBy='DateInput DESC Limit 1',$LogID='',7,$UserID='',$_ayID,$RecordStatus='1');
		if(!empty($tmpLog)){
			$lastSyncDateAssoc[$_ayID][7] = $tmpLog[0]['DateInput'];
		}
	}
	
	$sql = "SELECT AcademicYearID, ExamID, max(ModifiedDate) as lastUpdate from EXAM_DSE_STUDENT_SCORE where AcademicYearID IN ('".implode("','",(array)$needtoSubmitYearIdArr)."') GROUP BY ExamID, AcademicYearID";
	$lastUpdateAssoc = BuildMultiKeyAssoc($libdb->returnResultSet($sql), array('AcademicYearID','ExamID'),array('lastUpdate'),1 );
	
// 	$html = '';
	$html .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#CCCCCC\" id=\"ajax_reload_table\">";
	$html .= "<tr class=\"tabletop\">";
	$html .= "<td width='25%'>&nbsp;</td>";
	$html .= "<td width='25%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$Lang['SDAS']['toCEES']['Duration']."</td>";
	$html .= "<td nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$button_confirm."</td>";
	$html .= "<td width='25%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$Lang['SDAS']['toCEES']['LastConfirm']."</td>";
	$html .= "</tr>";
	
// 	foreach((array)$submissionAssoAry as $_ayID => $_resultArr){
	$count = 0;
	foreach((array)$academicYearInfoAssoArr as $_ayID => $_yearName){
		$_resultArr = $submissionAssoAry[$_ayID];
		if(!empty($_resultArr)){
			$count ++;
	// 		$_yearName = $academicYearInfoAssoArr[$_ayID];
			
			foreach ($_resultArr as $examType => $examData){
				if(!in_array($examType,array('DSE','DSE_APPEAL'))){
					continue;
				}
				$_rowName = $Lang['SDAS']['Exam']['HK'.$examType].' ('.$_yearName.')';
				$_submissionPeriod = $_resultArr[$examType]['SubmitStartDate'].' to '.$_resultArr[$examType]['SubmitEndDate'];
				if(isset($verifyAry[$_ayID][$examType]['endorse'])){
					$_logdate = $verifyAry[$_ayID][$examType]['endorseDate'];
					if($verifyAry[$_ayID][$examType]['endorse'] == 1){
						$_updateBtn = $Lang['SDAS']['toCEES']['Confirmed'];
					}
					if($verifyAry[$_ayID][$examType]['verify'] == 1){
						$_updateBtn = $Lang['SDAS']['toCEES']['VerifiedByCEES'];
					}
					//if($verifyAry[$_ayID][$examType]['endorse'] == 0 && $verifyAry[$_ayID][$examType]['resubmit'] == 1 && $verifyAry[$_ayID][$examType]['endorseDate'] != $verifyAry[$_ayID][$examType]['resubmitDate']){
					if($verifyAry[$_ayID][$examType]['endorse'] == 0){
						if($verifyAry[$_ayID][$examType]['resubmit'] == 1){
							$_updateBtn = $Lang['SDAS']['toCEES']['NeedResubmit'];
							$_logdate = $verifyAry[$_ayID][$examType]['endorseDate'];
						}
						if( $verifyAry[$_ayID][$examType]['resubmit']==0 && $verifyAry[$_ayID][$examType]['resubmitAlready'] == 1 ){
							if($examType == 'DSE'){
								$examID = '2';
							}elseif($examType == 'DSE_APPEAL'){
								$examID = '7';
							}
							// check if sync time > last import time
							if( strtotime($lastSyncDateAssoc[$_ayID][$examID]) > strtotime($lastUpdateAssoc[$_ayID][$examID]) ){
								$_updateBtn = "<input name='button' type='button' value='".$button_confirm."' onClick='endorse(\"".$examID."\",\"".$_ayID."\")' $ButtonClass>";
							}else{
								$_updateBtn = $Lang['SDAS']['toCEES']['PleaseSyncOnceMore'];
							}
						}
					}
				}else{
				    if(isset($verifyAry[$_ayID][$examType]['submit']) && $verifyAry[$_ayID][$examType]['submit'] == 1){
						if($examType == 'DSE'){
							$examID = '2';
							// check if sync time > last import time
							if( strtotime($lastSyncDateAssoc[$_ayID][$examID]) > strtotime($lastUpdateAssoc[$_ayID][$examID]) ){
								$_updateBtn = "<input name='button' type='button' value='".$button_confirm."' onClick='endorse(\"".$examID."\",\"".$_ayID."\")' $ButtonClass>";
							}else{
								$_updateBtn = $Lang['SDAS']['toCEES']['PleaseSyncOnceMore'];
							}
						}elseif($examType == 'DSE_APPEAL'){
							$examID = '7';
							if($verifyAry[$_ayID]['DSE']['endorse'] == 1){
								// check if sync time > last import time
								if( strtotime($lastSyncDateAssoc[$_ayID][$examID]) > strtotime($lastUpdateAssoc[$_ayID][$examID]) ){
									$_updateBtn = "<input name='button' type='button' value='".$button_confirm."' onClick='endorse(\"".$examID."\",\"".$_ayID."\")' $ButtonClass>";
								}else{
									$_updateBtn = $Lang['SDAS']['toCEES']['PleaseSyncOnceMore'];
								}
							}else{
								$_updateBtn = $Lang['SDAS']['toCEES']['PleaseEndorseDSEfirst'];
							}
						}
					}else{
						$_updateBtn = $Lang['SDAS']['toCEES']['NotSubmit'];
					}
					$_logdate = Get_String_Display("");
				}
				
				$html .="<tr class='tablerow1'>";
				$html .="<td>".$_rowName."</td>";
				$html .="<td align=center>".$_submissionPeriod."</td>";
				$html .="<td align=center>".$_updateBtn."</td>";
				$html .="<td align=center>".$_logdate."</td>";
				$html .="</tr>";
			}
		}
	}
	if($count == 0){
		$html .="<tr class='tablerow1'>";
		$html .="<td align=center colspan='4'>".$Lang['SDAS']['toCEES']['NoNeedConfirm']."</td>";
		$html .="</tr>";
	}
	
	$html .= '</table>';
	
	
	echo $html;
}

intranet_closedb();
?>
