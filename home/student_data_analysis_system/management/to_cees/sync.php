<?php
## using: anna
################# Change Log [Start] #####
#   Date    :   2018-07-04 Anna
#           - Change full mark as 100 
#
#	Date	:	2018-03-09 Omas
#			- FIX fail to sync DSE cat B, cat C subject problem
#
#	Date	:	2017-06-16 Villa
#			- add DSEAPPEAL
#
#	Date	:	2017-02-23 Omas
#			- add log for central rejected subject data
#
#	Date	:	2016-10-04 Omas
#			- added JUPAS
#
#	Date	:	2016-04-13 Omas
#			add recordstatus to $sql
#
#	Date	: 2016-03-22 Omas
#			add support to year-end exam	
#	Date	: 2016-03-22 Omas
#			add WebSAMSRegNo 
#
################## Change Log [End] ######

######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");
include_once($PATH_WRT_ROOT."/includes/subject_class_mapping.php");


$jsonObj = new JSON_obj();
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lcees = new libcees();
$libpf_academic = new libpf_academic();
$subject_class  = new subject();
######## Init END ########

// $academicYearID = 20;
// $exam = 3;
define("success", 1);
define("failed", 0);

if($academicYearID > 0 && $exam>0){
    $YearID = array();
	if($exam == EXAMID_JUPAS){
	// JUPAS
		$jupasDataArr = $libpf_exam->getStudentJupasOfferByYear($academicYearID);
		
		$dataArr = array();
		foreach((array)$jupasDataArr as $_key=> $_infoArr){
			// for reduce data size . Please don't change the sequence
			// if change the sequence you also have to change CEES 
			$count = 0;
			$dataArr[$_key][$count++] = $_infoArr['JupasAppNo'];
			$dataArr[$_key][$count++] = $_infoArr['JupasCode'];
			$dataArr[$_key][$count++] = $_infoArr['Institution'];
			$dataArr[$_key][$count++] = $_infoArr['Band'];
			$dataArr[$_key][$count++] = $_infoArr['Funding'];
			$dataArr[$_key][$count++] = $_infoArr['OfferRound'];
			$dataArr[$_key][$count++] = $_infoArr['AcceptanceStatus'];
			$dataArr[$_key][$count++] = $_infoArr['StudentID'];
		}
		
		$PrgramArr = $libpf_exam->getAllJupasProgrammeInfo();
		foreach((array)$PrgramArr as $_key=> $_pInfoArr){
			// for reduce data size . Please don't change the sequence
			// if change the sequence you also have to change CEES 
			$count = 0;
			$newProgramArr[$_key][$count++] = $_pInfoArr['JupasCode'];
			$newProgramArr[$_key][$count++] = $_pInfoArr['ProgrammeFullName'];
			$newProgramArr[$_key][$count++] = $_pInfoArr['Institution'];
			$newProgramArr[$_key][$count++] = $_pInfoArr['ProgrammeType'];
		}
		$dataArrTemp['Student'] = $dataArr;
		$dataArrTemp['Prog'] = $newProgramArr;
		
	}else if($exam == EXAMID_EXITTO){
	    
	    $exittoDataArr = $libpf_exam->getStudentExitToRecordByYear($academicYearID);

	    $dataArr = array();
	    foreach((array)$exittoDataArr as $_key=> $_infoArr){
	        // for reduce data size . Please don't change the sequence
	        $count = 0;
	        $dataArr[$_key][$count++] = $_infoArr['StudentID'];
	        $dataArr[$_key][$count++] = $_infoArr['FinalChoice'];
	        $dataArr[$_key][$count++] = $_infoArr['Remark'];
	    }	   
	
	    $dataArrTemp['Student'] = $dataArr;
 
	}
	else{
		// Real Exam	
		if($exam == EXAMID_YEAREND){
			// school final exam result here 
			$examDataArr = $libpf_exam->getSchoolExamData($academicYearID);
			$overallDataArr = $libpf_exam->getSchoolExamOverallData($academicYearID);
		}
		else{
		    if($exam == EXAMID_HKDSE || $exam == EXAMID_HKDSE){
                $sql = "SELECT
						RecordID,
						AcademicYearID,
						SubjectID,
						StudentID,
						Grade as Score, 
                        SubjectCode, 
                        ParentSubjectCode, 
                        IF( ParentSubjectCode = \"\", 
                        SubjectCode, 
                        CONCAT(ParentSubjectCode, \"-\",SubjectCode ) ) as realSubjectCode, 
                        isArchive, 
                        ExamID
					FROM
						EXAM_DSE_STUDENT_SCORE
					WHERE
						ExamID = '$exam'  
						AND AcademicYearID IN ('$academicYearID')";
                $examDataArr = $libpf_exam->returnResultSet($sql);
		        
		    }else{
		        $examDataArr = $libpf_exam->getExamData($academicYearID,$exam,'','',false,false,true);		
		    }
		}

		
		if(count($examDataArr)>0){
			// prepare data
			$sql = "SELECT RecordID as subjectID, CODEID, CMP_CODEID FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1";
			$subjectAssocArr = BuildMultiKeyAssoc($libpf_exam->returnResultSet($sql) ,'subjectID' );
		
			$includedUsers = Get_Array_By_Key($examDataArr, 'StudentID');
			$userAssoArr = $libpf_exam->getUserWebSAMSByID($includedUsers);
		
			
			$SubjectFullMarkAry = $libpf_academic->Get_Subject_Full_Mark($academicYearID);
			
			$dataArrTemp = array();
			$SubjectCodeAry = array();
			$SubjectParentCodeAry = array();
			foreach((array)$examDataArr as $_dataArr){
				// initialize
				$_studentArr = array();
					
				// subject Code related
				$_mainSubCode = $subjectAssocArr[ $_dataArr['SubjectID'] ]['CODEID'];
				$_cmpSubCode = $subjectAssocArr[ $_dataArr['SubjectID'] ]['CMP_CODEID'];
				if($_cmpSubCode != ''){
					$_subjectCode = $_cmpSubCode;
					$_parentSubjectCode = $_mainSubCode;
				}
				else{
					$_subjectCode = $_mainSubCode;
					$_parentSubjectCode = '';
				}
					
				// Identity related
				// assign value to $_studentArr
				$_studentWebsam = $userAssoArr['currentUser'][$_dataArr['StudentID']] == ''? $userAssoArr['archivedUser'][$_dataArr['StudentID']] : $userAssoArr['currentUser'][$_dataArr['StudentID']];
				$_studentArr['WebSAMSRegNo']= $_studentWebsam;
				$_studentArr['StudentID'] = $_dataArr['StudentID'];
				
				
				$_studentArr['PrimarySchoolCode'] = $_dataArr['PrimarySchoolCode'];
				$_studentArr['EnglishName'] = $_dataArr['EnglishName'];
				$_studentArr['ChineseName'] = $_dataArr['ChineseName'];

				
				// Subject Related field
				$_studentArr['SrcSubjectID'] = $_dataArr['SubjectID'];
				$_studentArr['SubjectCode'] = $_subjectCode;
				if($_parentSubjectCode !=''){
					$_studentArr['ParentSubjectCode'] = $_parentSubjectCode;
				}
					
				// Grade Score
				if($_dataArr['Grade']!= ''){
					$_studentArr['Grade'] = $_dataArr['Grade'];
				}
				if($_dataArr['Score']!= ''){
					$_studentArr['Score'] = $_dataArr['Score'];
				}
					
				// DSE only
				if($exam == EXAMID_HKDSE || $exam == EXAMID_HKDSE_APPEAL){
					if($_dataArr['SubjectCode']!= ''){
						$_studentArr['dseSubjectCode'] = $_dataArr['SubjectCode'];
					}
					if($_dataArr['ParentSubjectCode']!= ''){
						$_studentArr['dseParentSubjectCode'] = $_dataArr['ParentSubjectCode'];
					}
				}
					
				// final exam only
				//if($exam == 4){
				if($exam == EXAMID_YEAREND){
					$_studentArr['OrderClass'] = $_dataArr['OrderMeritClass'];
					$_studentArr['OrderClassTotal'] = $_dataArr['OrderMeritClassTotal'];
					$_studentArr['OrderForm'] = $_dataArr['OrderMeritForm'];
					$_studentArr['OrderFormTotal'] = $_dataArr['OrderMeritFormTotal'];
					$_studentArr['YearName'] = $_dataArr['YearName'];
					$_studentArr['YearID']  = $_dataArr['YearID'];
					
					
					
					$SubjectFullMark = $SubjectFullMarkAry[$academicYearID][$_studentArr['YearID']][$_studentArr['SrcSubjectID']]['FullMarkInt'];
					
					if($SubjectFullMark == '' || $SubjectFullMark == null){
					    $thisYearID = $_studentArr['YearID'];
					    
					    $sql = "SELECT YearName FROM {$intranet_db}.YEAR WHERE YearID = '". $thisYearID."'";					 
					    $thisYearNameArray = $libpf_exam->returnVector($sql);
					    $SubjectCodeAry[] =$_studentArr['SubjectCode'];
						$SubjectParentCodeAry[] = $_studentArr['ParentSubjectCode'];
					    $YearNameAry[] = $thisYearNameArray[0];
// 					    $SubjectIDAry[] = $_studentArr['SrcSubjectID'];
					   
					}else{
					    $nowScore = (100* $_dataArr['Score'])/$SubjectFullMark; 
					    $_studentArr['Score'] = $nowScore;			    
					}
					
					// 				if($_dataArr['SubjectComponentCode'] != ''){
					// 					$_studentArr['SubjectCode'] = $_dataArr['SubjectComponentCode'];
					// 					$_studentArr['ParentSubjectCode'] = $_dataArr['SubjectCode'];
					// 				}
					// 				else{
					// 					$_studentArr['SubjectCode'] = $_dataArr['SubjectCode'];
					// 				}
					// 				$_studentArr['SrcSubjectID'] = $_dataArr['SubjectID'];
				}
				// push $_studentArr to $dataArr
				$dataArrTemp[] = $_studentArr;
			}
		
			if(count($overallDataArr)>0){
				$oDataArr = array();
				foreach((array)$overallDataArr as $_overallArr){
					$_Websam = $userAssoArr['currentUser'][$_overallArr['StudentID']] == ''? $userAssoArr['archivedUser'][$_overallArr['StudentID']] : $userAssoArr['currentUser'][$_overallArr['StudentID']];
		
					$_tempOverall['WebSAMSRegNo'] = $_Websam;
					$_tempOverall['StudentID'] = $_overallArr['StudentID'];
					$_tempOverall['Score'] = $_overallArr['Score'];
					$_tempOverall['Grade'] = $_overallArr['Grade'];
					$_tempOverall['OrderClass'] = $_overallArr['OrderMeritClass'];
					$_tempOverall['OrderClassTotal'] = $_overallArr['OrderMeritClassTotal'];
					$_tempOverall['OrderForm'] = $_overallArr['OrderMeritForm'];
					$_tempOverall['OrderFormTotal'] = $_overallArr['OrderMeritFormTotal'];
					$_tempOverall['YearName'] = $_overallArr['YearName'];
		
					$oDataArr[] = $_tempOverall;
				}
			}
		
		}else{
			//$status = failed;
			//		echo 'no data';
			if($exam == EXAMID_HKDSE_APPEAL && $_POST['confirmAppealNoData'] == 1){
				$dataArrTemp = array('confirmWithNoData'); // no data
			}
		}
	}
	
	
	// common sendData to Central
	include_once($PATH_WRT_ROOT."includes/cees/libcees_aes.php");
	$lceesAes = new libcees_aes();
	if($exam != EXAMID_YEAREND){
		$dataArr = $dataArrTemp;
	}else{
		$dataArr['Subject'] = $dataArrTemp;
		$dataArr['Overall'] = $oDataArr;
	}
	
	if(!empty($SubjectCodeAry)){

	    for($i=0;$i<count($SubjectCodeAry);$i++){
	        $subjectInfoAry = array();
	        $ParentCode = $SubjectParentCodeAry[$i];
	        $Code = $SubjectCodeAry[$i];
	        if($ParentCode == ''){
	            $subjectInfoAry = $subject_class->Get_Subject_By_SubjectCode($Code);
	        }else{	            
	            $subjectInfoAry = $subject_class->Get_Subject_By_SubjectCode($Code,1,$ParentCode);
	        }
	        $thisSubjectName = Get_Lang_Selection($subjectInfoAry[0]['CH_SNAME'],$subjectInfoAry[0]['EN_SNAME']);
	        $thisYearName = $YearNameAry[$i];
	        $NoFuulMarkFormAry[$thisYearName][] = $thisSubjectName;
        }
	    foreach($NoFuulMarkFormAry as $YearName =>$SubjectName){
	        $SubjectNameAry = array_unique($SubjectName);
	        $returnMessageAry[] = $YearName . ': '.implode(' , ',$SubjectNameAry);
	    }
	    $returnMessage = "These forms need to fill in subject full mark:"."\n\n";
	    $returnMessage .= implode("\n",$returnMessageAry);
	    $status = $returnMessage;
	}else{
	    	$success = $lcees->syncScoreToCentral($academicYearID, $exam, $dataArr);
	    	if($success === 1){
	    	    $status = success;
	    	}else{
	    	    $status = failed;
	    	}
// 	    	$dataHash =  sha1(serialize($dataArr));
	}

// 	$aYearString = $lcees->getAcademicYearString($academicYearID);

	


	
// 	debug_pr('send out data: '.$dataHash);
// 	debug_pr('after: '. $success['rda']['checksum']);
// 	debug_pr($success);
	//$status = success;
	//echo $status;
	
	
	
	//check suceess?
// 	if($dataHash == '' || $success['rda']['checksum'] == ''){
// 		$status = failed;
// 	}else if( $exam == EXAMID_HKDSE_APPEAL||  empty($success['rda']['error']) && (  $exam == EXAMID_JUPAS || $dataHash==$success['rda']['checksum'])){
// 		// EXAMID_JUPAS checksum logic fail ... don't know why after adding $dataArrTemp['Prog'] checksum return from CEES is different
// 		$status = success;
// 	}else{
// 		$status = failed;
// 	}
	echo $status;
	//debug_pr($status);
// 	$lcees->takeCeesSyncLog($exam,$academicYearID,$status);
// 	if($success['rda']['SNF'] != ''){
// 		$additionLogContent = ' SNF: '.$success['rda']['SNF'];
// 	}else{
// 		$additionLogContent = '';
// 	}
// 	$log = 'success:'.$success['rda']['success'].', error:'.$success['rda']['error'].', '.$additionLogContent;
// 	$lcees->takeCeesSyncLog($exam,$academicYearID,$log);
// log inside libcees
// 	$lcees->takeCeesSyncLog($exam,$academicYearID,$status,$log);
}
?>