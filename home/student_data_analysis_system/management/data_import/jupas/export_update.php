<?php
// Modifing by 

############################
#
#   Date    :   2019-08-05 Bill [2019-0805-1114-22207]
#           - improved for column re-ordering [Programme + Funding Category]
#
#	Date	:	2017-05-22 Omas
#			- added websams regno to jupas template - #L117176
#
#	Date	:	2016-10-04 Omas
#			- created this page
#
############################

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."cust/lang/analysis_system_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_opendb();

$lexport = new libexporttext();
$libpf_exam = new libpf_exam();

if($task == 'jupas_template'){
	// $exportColumn = array('Application No. / WebSAMS RegNo.','Applicant Name','Class','Group','Offer','Institution','Funding Category','Programme Full Title','Band','Offer Round','Acceptance Status');
    $exportColumn = array('Application No. / WebSAMS RegNo.','Applicant Name','Class','Group','Offer','Institution','Programme Full Title','Funding Category','Band','Offer Round','Acceptance Status');
	$exportData = array();
    $exportData[] = array("1688912","CHAN Siu Ming","6A","","JS1204","CityU","BSc Computer Science","UGC-funded","","Subsequent Round","Pending");
    $exportData[] = array("1688913","CHAN Tai Ming","6B","","JS2030","HKBU","Bachelor of Arts in Music","UGC-funded","","Subsequent Round","Pending");
    $exportData[] = array("1688914","CHEUNG Tat Ming","6C","","JS7709","LingnanU","Bachelor of Arts (Honours) in History","UGC-funded","","Subsequent Round","Accepted");
    $exportData[] = array("1688915","FONG Siu Ming","6D","","JS4006","CUHK","Anthropology","UGC-funded","","Subsequent Round","Accepted");
    $exportData[] = array("1688916","HO Siu Ming","6A","","JS8663","EduHK","Bachelor of Arts (Honours) in Special Education","UGC-funded","","Clearing Round","Declined");
    $exportData[] = array("1688917","KO Tai Ming","6B","","JS3492","PolyU","BA (Hons) Scheme in Fashion and Textiles","UGC-funded","","Clearing Round","Declined");
    $exportData[] = array("1688918","LEUNG Siu Ming","6C","","JS5812","HKUST","BSc Environmental Management and Technology","UGC-funded","","Clearing Round","Declined");
    $exportData[] = array("1688919","MAN Ming","6D","","JS6107","HKU","Bachelor of Dental Surgery","UGC-funded","","Clearing Round","Pending");
    $exportData[] = array("1688920","NG John","6A","","JS9012","OUHK","Bachelor of Arts with Honours in Creative Writing and Film Arts","Self-financing","","Main Round","Pending");
    $exportData[] = array("1688921","PANG Siu Ming","6B","","JSST03","SSSDP","Offered by TWC: Bachelor of Science (Honours) in Radiation Therapy","SSSDP","","Main Round","Unknown");
    $exportData[] = array("1688922","SZE Siu Ming","6C","","JS8507","HKIED","HD in Early Childhood Education","UGC-funded","","Main Round","Unknown");
    $exportData[] = array("1688923","WONG Sheung","6D","","No offer","","","","","","");
	$filename = "JUPAS_Template.csv";
}
else{
	$exportColumn = array('ClassName', 'ClassNo', 'Student Name','Application No.', 'Offer', 'Institution', 'Funding Category','Band',	'Offer Round',	'Acceptance Status');
	$sql = "SELECT * FROM JUPAS_STUDENT_OFFER WHERE AcademicYearID = '$academicYearID'";
	$jupasResultArr = $libpf_exam->returnResultSet($sql);

	$studnetIDArr = array_unique((array)Get_Array_By_Key($jupasResultArr,'StudentID'));
	$userInfoAssocArr = BuildMultiKeyAssoc($libpf_exam->getStudentsInfoByID($studnetIDArr, $academicYearID), 'UserID');
	
	$ayName = getAcademicYearByAcademicYearID($academicYearID);
	for($i=0;$i<count($jupasResultArr);$i++){
		$thisUserId = $jupasResultArr[$i]['StudentID'];
		if($userInfoAssocArr[$thisUserId]['Name'] ==''){
			$nameArr = $libpf_exam->getUserNameByID($thisUserId);
			$displayName = ($nameArr['currentUser'][$thisUserId]=='')? $nameArr['archivedUser'][$thisUserId] : $nameArr['currentUser'][$thisUserId];
		}else{
			$displayName = $userInfoAssocArr[$thisUserId]['Name'];
		}
		$exportData[$i][] = $userInfoAssocArr[$thisUserId]['ClassName'];
		$exportData[$i][] = ($userInfoAssocArr[$thisUserId]['ClassNumber'] != '')? str_pad( $userInfoAssocArr[$thisUserId]['ClassNumber'], 2, '0',  STR_PAD_LEFT) : '';
		$exportData[$i][] = $displayName; 
		$exportData[$i][] = $jupasResultArr[$i]['JupasAppNo'];
		$exportData[$i][] = $jupasResultArr[$i]['JupasCode'] == '-999'? 'No Offer': $jupasResultArr[$i]['JupasCode'];
		$exportData[$i][] = $jupasResultArr[$i]['Institution'];
		$exportData[$i][] = $jupasResultArr[$i]['Funding'];
		$exportData[$i][] = $jupasResultArr[$i]['Band'];
		$exportData[$i][] = $jupasResultArr[$i]['OfferRound'];
		$exportData[$i][] = $jupasResultArr[$i]['AcceptanceStatus'];
	}
	if(!empty($exportData)){
		sortByColumn2($exportData,'0',0,0,'1');
	}
	
	$filename = 'JUPAS_'.$ayName.'.csv' ;
// 	$filename = iconv("UTF-8", "Big5", $filename);
}
$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, "", "\r\n", "", 0, "11",1);
$lexport->EXPORT_FILE($filename, $export_content);
intranet_closedb();
// Output the file to user browser
?>