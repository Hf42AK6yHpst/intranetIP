<?php
// Modifing by 

############################
#	Date	:	2016-10-04 Omas
#			- created this page
#
############################

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."cust/lang/analysis_system_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_opendb();

$lexport = new libexporttext();
$libpf_exam = new libpf_exam();
 
if($task == 'exitto_template'){
	$exportColumn = array('Application No. / WebSAMS RegNo.','Applicant Name', 'Class','Final Choice','Remarks');
	$exportData = array();
    $exportData[] = array("1688912","CHAN Siu Ming","6A","JUPAS");
    $exportData[] = array("1688913","CHAN Tai Ming","6B","OTHER","Harvard University");
    $exportData[] = array("1688914","CHEUNG Tat Ming","6C","EMPLOYMENT","GOODGOOD Ltd.");
    $exportData[] = array("1688915","FONG Siu Ming","6D","JUPAS");
    $exportData[] = array("1688916","HO Siu Ming","6A","JUPAS");
    $exportData[] = array("1688917","KO Tai Ming","6B","OTHER","University of Oxford");
    $exportData[] = array("1688918","LEUNG Siu Ming","6C","EMPLOYMENT","ABC Company");
    $exportData[] = array("1688919","MAN Ming","6D","JUPAS");
    $exportData[] = array("1688920","NG John","6A","OTHER","University of Oxford");
    $exportData[] = array("1688921","PANG Siu Ming","6B","EMPLOYMENT","Great Wall Ltd.");
    $exportData[] = array("1688922","SZE Siu Ming","6C","JUPAS");
    $exportData[] = array("1688923","WONG Sheung","6D","EMPLOYMENT","Example Company");
	$filename = "ExitTo_Template.csv";
}else{
    
    $exportColumn = array('ClassName', 'ClassNo', 'Student Name','Application No.','Final Choice','Remarks');
    $sql = "SELECT * FROM STUNDET_EXIT_TO_INFO WHERE AcademicYearID = '$academicYearID'";
    $exittoResultArr = $libpf_exam->returnResultSet($sql);

    $studnetIDArr = array_unique((array)Get_Array_By_Key($exittoResultArr,'StudentID'));
    $userInfoAssocArr = BuildMultiKeyAssoc($libpf_exam->getStudentsInfoByID($studnetIDArr, $academicYearID), 'UserID');
    
    $ayName = getAcademicYearByAcademicYearID($academicYearID);
    for($i=0;$i<count($exittoResultArr);$i++){
        $thisUserId = $exittoResultArr[$i]['StudentID'];
        if($userInfoAssocArr[$thisUserId]['Name'] ==''){
            $nameArr = $libpf_exam->getUserNameByID($thisUserId);
            $displayName = ($nameArr['currentUser'][$thisUserId]=='')? $nameArr['archivedUser'][$thisUserId] : $nameArr['currentUser'][$thisUserId];
        }else{
            $displayName = $userInfoAssocArr[$thisUserId]['Name'];
        }
        $exportData[$i][] = $userInfoAssocArr[$thisUserId]['ClassName'];
        $exportData[$i][] = ($userInfoAssocArr[$thisUserId]['ClassNumber'] != '')? str_pad( $userInfoAssocArr[$thisUserId]['ClassNumber'], 2, '0',  STR_PAD_LEFT) : '';
        $exportData[$i][] = $displayName;
        $exportData[$i][] = $exittoResultArr[$i]['JupasAppNo'];
//         $exportData[$i][] = $exittoResultArr[$i]['Institution'];
//         $exportData[$i][] = $exittoResultArr[$i]['ProgrammeFullName'];
        if($exittoResultArr[$i]['FinalChoice']=='1'){
            $FinalChoice = 'JUPAS';
        }else if ($exittoResultArr[$i]['FinalChoice']=='2'){
            $FinalChoice = 'OTHER';
            $Remarks = $exittoResultArr[$i]['Remark'];
        } else{
            $FinalChoice = 'EMPLOYMENT';
            $Remarks = $exittoResultArr[$i]['Remark'];
        }
        $exportData[$i][] = $FinalChoice;
        $exportData[$i][] = $Remarks;
    }
    if(!empty($exportData)){
        sortByColumn2($exportData,'0',0,0,'1');
    }
    
    $filename = 'ExitTo_'.$ayName.'.csv' ;   
}
$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, "", "\r\n", "", 0, "11",1);
$lexport->EXPORT_FILE($filename, $export_content);
intranet_closedb();
// Output the file to user browser
?>