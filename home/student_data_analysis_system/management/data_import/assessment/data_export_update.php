<?php
// Modifing by 
################# Change Log [Start] #####
#	Date	:	2016-02-19 Kenneth
#				If $ClassNames is empty, choose all classes in this academeic year
#
#	Date	:	2016-02-01	Omas
# 				Copy from iPortfolio
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
intranet_opendb();

//debug_pr($_POST);

$lpf = new libpf_slp();
$lpf_sturec = new libpf_sturec();

$lexport = new libexporttext();
if($ClassNames==''){
	//Get All Class
	$libclass = new libclass();
	$tempClassNames = $libclass->getClassList($academicYearID);
	//debug_pr($tempClassNames);
	for($i=0;$i<count($tempClassNames);$i++){
		$ClassNames[$i] = $tempClassNames[$i]['ClassName'];
	}
		
}

$ExportContent = $lpf->returnAssessmentExportContent($academicYearID, $YearTermID, $ClassNames, $report_type);

intranet_closedb();

// Output the file to user browser
$year_name = "_".$Year;
$sem_name = ($Semester!="") ? "_".$Semester : "";
$class_name = ($ClassName!="") ? "_".$ClassName : "";
$filename = ($report_type==0) ? "subject_assessment_{$academicYearID}_{$YearTermID}.csv" : "main_assessment_{$academicYearID}_{$YearTermID}.csv";
$filename = iconv("UTF-8", "Big5", $filename);

$lexport->EXPORT_FILE($filename, $ExportContent);
?>
