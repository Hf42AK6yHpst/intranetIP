<?php
// Modifing by 
################# Change Log [Start] #####
#	Date	:	2017-04-12	Villa
#				fixed duplicated T1A1
#
#	Date	:	2017-01-11	Omas
#				fixed template is not contain only one term problem
#
#	Date	:	2016-02-01	Omas
# 				Copy from iPortfolio
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

intranet_opendb();
$lpf = new libpf_sturec();

/*
list($SemesterArray, $ShortSemesterArray) = $lpf->getSemesterNameArrFromIP();
$targetSemester = trim($targetSemester);
$SemNumber = (in_array($targetSemester, $SemesterArray)) ? array_search($targetSemester, $SemesterArray)+1 : 0;
$SemCode = "T".$SemNumber."A1";
*/
$SemCode = "T0A1";
$layt = new academic_year_term($YearTermID);
$yt_arr = $layt->Get_Term_List(0);
// debug_pr($yt_arr);
$semNum = 0;
// die;
for($i=0; $i<count($yt_arr); $i++)
{
  if($YearTermID == $yt_arr[$i]["YearTermID"])
  {
  	$semNum = $i+1;
    $SemCode = "T".$semNum."A1";
    break;
  }
}

// if($semNum > 0){
// 	$SemCodeArr = array(
// 		("T".$semNum."A1"),
// //		("T".$semNum)
// 		("T".$semNum."A1")
// 	);
// }else{
// 	$SemCodeArr = array(
// 		("T".$semNum."A1")
// 	);
// }
$SemCodeArr = array(
		("T".$semNum)
);

//"The first row is the explanation of fields. Please remove the first two row before import."
$Title .= "School ID (can_be_blank),School Year (can_be_blank),School Level (can_be_blank),School Session (can_be_blank),Class Level (can_be_blank),Class Name,Class Number,Student Name,Registration Number";
$Content = "School ID,School Year,School Level,School Session,Class Level,Class,Class Number,Student Name,Reg. No.";

foreach($SemCodeArr as $SemCode){
	for($i=0; $i<sizeof($target); $i++)
	{
		//$subject = str_replace("###", "_", $target[$i]);
		$TmpArr = explode("###", $target[$i]);
		$SubejctCode = implode("_", $TmpArr);
		$desc = $lpf->returnSubjectDescription($TmpArr[0], $TmpArr[1]);
		$Title .= ",Score of ".$desc.",Grade of ".$desc.",Form Rank of ".$desc;
		$Content .= ",".$SemCode."_".$SubejctCode."_C_Score,".$SemCode."_".$SubejctCode."_C_Grade,".$SemCode."_".$SubejctCode."_C_OMF";
	}
	$Title .= ",Overall average score,Overall grade,Overall position in form,Overall position in stream (can_be_blank),Overall position in stream group(can_be_blank)";
	
	$Content .= ",".$SemCode."_Score,".$SemCode."_Grade,".$SemCode."_OMC,".$SemCode."_OMF,".$SemCode."_OMS,".$SemCode."_OMSG";
}
$Title .= "\n";
$Content .= "\n";
intranet_closedb();

// Output the file to user browser
$filename = "assessment_template.csv";
# Add BOM
$Content = iconv("Big5", "UTF-8", $Content);
//output2browser($Title.$Content, $filename);
# Suppress explanation title
output2browser($Content, $filename);

?>
