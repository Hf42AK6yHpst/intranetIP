<?php
// Modifing by 

############################
#	Date	:	2019-10-28 Philips
#			- created this page
#
############################

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."cust/lang/analysis_system_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/cees/libcees.php");

intranet_opendb();

$lexport = new libexporttext();
$libpf_exam = new libpf_exam();
$lcs = new libcees();
$schoolInfoAry = $lcs->getAllSchoolInfoFromCentralServer('S');
// debug_pr($choolInfoAry);die();
if($task == 'sspa_template'){	
	$exportColumn = array();
	$exportData = array();
    $exportData[] = array(
    		$Lang['SDAS']['SSPA']['ParticipateAllocation'] . " " . $Lang['SDAS']['SSPA']['TotalCount'],
    		$Lang['SDAS']['SSPA']['PlacesEMI'] . " " . $Lang['SDAS']['SSPA']['Count'],
    		$Lang['SDAS']['SSPA']['PlacesFirstChoice'] . " " . $Lang['SDAS']['SSPA']['Count'],
    		$Lang['SDAS']['SSPA']['PlacesFirsttoThirdChoice'] . " " . $Lang['SDAS']['SSPA']['Count']
    );
    $exportData[] = array("8","4","3","5");
    $exportData[] = array(
    		$Lang['SDAS']['SSPA']['Net'] . " Banding",
    		"Band 1",
    		"Band 2",
    		"Band 3"
    );
    $exportData[] = array("","2","4","3");
    $exportData[] = array(
    		$Lang['SDAS']['SSPA']['Territory'] . " Banding",
    		"Band 1",
    		"Band 2",
    		"Band 3"
    );
    $exportData[] = array("","4","3","2");
    
	$filename = "SSPA_Template.csv";
}else{
}
$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, "", "\r\n", "", 0, "11",1);
$lexport->EXPORT_FILE($filename, $export_content);
intranet_closedb();
// Output the file to user browser
?>