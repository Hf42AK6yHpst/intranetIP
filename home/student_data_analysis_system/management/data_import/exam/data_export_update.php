<?php
// Modifing by 

############################
#	Change Log
#	Date	:	2017-08-02	Omas
#				changed DSE appeal logic
#	Date	:	2017-07-13	Omas
#			 - add sorting to grade for DSE format 2
#	Date	:	2017-06-16	Villa
#			 - Add dse appeal
#	Date	:	2017-03-14	Omas
#			 - break Core subject SQL into 4 individual SQL
#			 - added classname, classno
#
#	Date	:	2017-03-09	Omas
#			 - added elective 4
#
#	Date	:	2016-04-28	Omas
#			 - add dse websams template
#
#	Date	:	2016-04-13	Omas
#			 - HKAT support export status (Grade field) if score = 0 
#
#	Date	: 2016-03-15 Omas
#			 - support to export archive user
#
#	2016-02-19 Kenneth
#	If $ClassNames is empty, choose all classes in this academeic year
############################

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."cust/lang/analysis_system_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_opendb();

$lexport = new libexporttext();
$libpf_exam = new libpf_exam($exam);

if($task == 'HKAT_template'){
	$exportColumn = array('WebSAMSRegNum','STRN','Chinese(100)','English(100)','Math(100)');
	$exportData[0] = array ('#10000001','',99,80,77);
	$filename = "Attainment_Test_Template.csv";
}
else if($task == 'HKDSE_websams_template'){
    $exportColumn = array('EXAMCODE', 'examyear','classcode','classno','regno','ENNAME', 'sex', 'Subject Code', 'Subject Name', 'Subject Grade', 'Subject Comp Code', 'Subject Comp Name', 'Subject Comp Grade' );
    $exportData[0] = array ('DSE','2015','6A','1','#s09002','CHAN TZE KWAN', 'F', 'A010', 'CHINESE LANGUAGE',	'5**',	'3', 'LISTENING',5);
    $exportData[1] = array ('DSE','2015','6A','1','#s09002','CHAN TZE KWAN', 'F', 'A140', 'CHEMISTRY', '3');
    $filename = "HKDSE_WebSAMS_Template.csv";
}
else if($task == 'exportDSE_format2'){
	
	// ####################### Get Core Subject Score (Start) ############################
	$coreIdAssocArr= BuildMultiKeyAssoc($libpf_exam->getDseCompulsorySubjectID(), 'CODEID', array('SubjectID'), 1);
// 	$sql = "Select
// 			chi.studentID as StudentID ,
// 			chi.SubjectCode as chiCode, chi.grade as chiGrade,
// 			eng.SubjectCode as engCode, eng.grade as engGrade,
// 			maths.SubjectCode as mathsCode, maths.grade as mathsGrade,
// 			ls.SubjectCode as lsCode, ls.grade as lsGrade
// 			From
// 			EXAM_DSE_STUDENT_SCORE as chi
// 			left join EXAM_DSE_STUDENT_SCORE as eng on (chi.StudentID = eng.studentID )
// 			left join EXAM_DSE_STUDENT_SCORE as maths on (chi.StudentID = maths.studentID )
// 			left join EXAM_DSE_STUDENT_SCORE as ls on (chi.StudentID = ls.studentID )
// 			where
// 			chi.AcademicYearID = '".$academicYearID."' and chi.SubjectID = '".$coreIdAssocArr['080']."'
// 			and eng.AcademicYearID = '".$academicYearID."' and eng.SubjectID = '".$coreIdAssocArr['165']."'
// 		    and maths.AcademicYearID = '".$academicYearID."' and maths.SubjectID = '".$coreIdAssocArr['22S']."'
// 		    and ls.AcademicYearID = '".$academicYearID."' and ls.SubjectID = '".$coreIdAssocArr['265']."'";
	
	$sql = "SELECT 
				StudentID, SubjectCode, Grade 
			FROM 
				EXAM_DSE_STUDENT_SCORE  
			WHERE 
				AcademicYearID = '".$academicYearID."'
				AND SubjectID = '".$coreIdAssocArr['080']."' 
				AND isArchive = 0 ";
	$chiScoreArr = $libpf_exam->returnResultSet($sql) ;
	$sql = "SELECT
				StudentID, SubjectCode, Grade
			FROM
				EXAM_DSE_STUDENT_SCORE
			WHERE
				AcademicYearID = '".$academicYearID."'
				AND SubjectID = '".$coreIdAssocArr['165']."' 
				AND isArchive = 0 ";
	$engScoreArr = $libpf_exam->returnResultSet($sql) ;
	$sql = "SELECT
				StudentID, SubjectCode, Grade
			FROM
				EXAM_DSE_STUDENT_SCORE
			WHERE
				AcademicYearID = '".$academicYearID."'
				AND SubjectID = '".$coreIdAssocArr['22S']."' 
				AND isArchive = 0 ";
	$mathScoreArr = $libpf_exam->returnResultSet($sql) ;
	$sql = "SELECT
				StudentID, SubjectCode, Grade
			FROM
				EXAM_DSE_STUDENT_SCORE
			WHERE
				AcademicYearID = '".$academicYearID."'
				AND SubjectID = '".$coreIdAssocArr['265']."' 
				AND isArchive = 0 ";
	$lsScoreArr = $libpf_exam->returnResultSet($sql) ;
	
	$studentIDArr1 = array_merge( Get_Array_By_Key($chiScoreArr,'StudentID'), Get_Array_By_Key($engScoreArr,'StudentID'), Get_Array_By_Key($mathScoreArr,'StudentID'), Get_Array_By_Key($lsScoreArr,'StudentID') );
	
	$sql = "select StudentID, SubjectID, SubjectCode, Grade from EXAM_DSE_STUDENT_SCORE where AcademicYearID = '".$academicYearID."' and ParentSubjectCode ='' and SubjectCode NOT IN ('A010', 'A020', 'A030', 'A040') AND isArchive = 0 Order by StudentID,SubjectCode";
	$result = $libpf_exam->returnResultSet($sql);
	$studentIDArr2 = Get_Array_By_Key($result,'StudentID');
	$AssocResult = BuildMultiKeyAssoc($result, 'StudentID','',0,1);
	
	$studentIDArr = array_unique(array_merge((array)$studentIDArr2, (array)$studentIDArr1));
	//$studentInfo = $libpf_exam->getUserNameByID($studentIDArr);
	$studentInfoArr = $libpf_exam->getStudentsInfoByID($studentIDArr, $academicYearID);
	
	// rebuild $coreResult
	$coreSubjectArr = array( 'chi', 'eng', 'math', 'ls');
	$coreResult = array();
	$count = 0;
	foreach($coreSubjectArr as $_subject){
		${$_subject.'ScoreAssoc'} = BuildMultiKeyAssoc( ${$_subject.'ScoreArr'}, 'StudentID' );
	}
	foreach($studentIDArr as $_sid){
		$coreResult[$count]['StudentID'] = $_sid;
		foreach($coreSubjectArr as $_subject){
			$_thisSubjectScoreArr = ${$_subject.'ScoreAssoc'};
			$coreResult[$count][$_subject.'Code'] = $_thisSubjectScoreArr[$_sid]['SubjectCode'];
			$coreResult[$count][$_subject.'Grade'] = $_thisSubjectScoreArr[$_sid]['Grade'];
		}
		$count ++;
	}
	$coreAssoc = BuildMultiKeyAssoc($coreResult, 'StudentID');
	// ####################### Get Core Subject Score (End) ############################
	
	$studentSubjectScoreAssoc = array();
	foreach((array)$studentIDArr as  $_thisID){
		$_thisElectiveResultArr = $AssocResult[$_thisID];
		if(count($_thisElectiveResultArr)>1){
			sortByColumn2($_thisElectiveResultArr,'Grade',1);
		}
		$studentSubjectScoreAssoc[$_thisID][] = $coreAssoc[$_thisID]['chiGrade'];
		$studentSubjectScoreAssoc[$_thisID][] = $coreAssoc[$_thisID]['engGrade'];
		$studentSubjectScoreAssoc[$_thisID][] = $coreAssoc[$_thisID]['mathGrade'];
		$studentSubjectScoreAssoc[$_thisID][] = $coreAssoc[$_thisID]['lsGrade'];
		$coreAssoc[$_thisID]['StudentID'] = $_thisID;
		$count = 1;
		foreach((array)$_thisElectiveResultArr as $__resultInfoArr){
			$__thisSubjectId = $libpf_exam->getSubjectIDByDseSubjectCode($__resultInfoArr['SubjectCode']);
			$__thisSubjectInfoTemp = $libpf_exam->getSubjectCodeAndID($__thisSubjectId);
			$__thisSubjectInfo = $__thisSubjectInfoTemp[0];
			$__displaySubjectName = empty($__thisSubjectInfo) ? '' : ' ('.$__thisSubjectInfo['EN_SNAME'].')';
			$__thisElective = $__resultInfoArr['SubjectCode'].$__displaySubjectName;
			$__thisElectiveGrade =  $__resultInfoArr['Grade'];
			$coreAssoc[$_thisID]['elective'.$count.'Code'] = $__thisElective;
			$coreAssoc[$_thisID]['elective'.$count.'Grade'] = $__thisElectiveGrade;
			$studentSubjectScoreAssoc[$_thisID][] = $__thisElectiveGrade;
			$count++;
		}
	}
	
	// count best 5 score
	$studentbest5ScoreArr = array();
	foreach((array)$studentSubjectScoreAssoc as $stuID => $dseLvArr){
		$scoreArr = $libpf_exam->convertDseLevelToScore($dseLvArr);
		if(count($scoreArr)>0){
			rsort($scoreArr);
			array_splice($scoreArr,5);
			$studentbest5ScoreArr[$stuID] = array_sum($scoreArr);
		}
	}

	$i = 0;
	// foreach((array) $coreAssoc as $_key=> $_stuArr){
	//foreach((array)$studentIDArr as  $_thisID){
	foreach((array)$studentInfoArr as  $_thisStudentInfo){
		$_thisID = $_thisStudentInfo['UserID'];
		$_stuArr = $coreAssoc[$_thisID];
		$_thisUserID = $_thisID;
		//$_thisUserName = isset($studentInfo['currentUser'][$_thisUserID])? $studentInfo['currentUser'][$_thisUserID]: $studentInfo['archivedUser'][$_thisUserID];
		$_thisUserName = $_thisStudentInfo['Name'];
		$rows[$i][] = $_thisStudentInfo['ClassName'];
		$rows[$i][] = $_thisStudentInfo['ClassNumber'];
		$rows[$i][] = $_thisUserName;
		$rows[$i][] = $_stuArr['chiCode'];
		$rows[$i][] = $_stuArr['chiGrade'];
		$rows[$i][] = $_stuArr['engCode'];
		$rows[$i][] = $_stuArr['engGrade'];
		$rows[$i][] = $_stuArr['mathCode'];
		$rows[$i][] = $_stuArr['mathGrade'];
		$rows[$i][] = $_stuArr['lsCode'];
		$rows[$i][] = $_stuArr['lsGrade'];
		$rows[$i][] = $_stuArr['elective1Code'];
		$rows[$i][] = $_stuArr['elective1Grade'];
		$rows[$i][] = $_stuArr['elective2Code'];
		$rows[$i][] = $_stuArr['elective2Grade'];
		$rows[$i][] = $_stuArr['elective3Code'];
		$rows[$i][] = $_stuArr['elective3Grade'];
		$rows[$i][] = $_stuArr['elective4Code'];
		$rows[$i][] = $_stuArr['elective4Grade'];
		$rows[$i][] = $studentbest5ScoreArr[$_thisUserID];
		$i ++;
	}
	if($exam == EXAMID_HKDSE){
	$filename = 'HKDSE.csv';
	}elseif($exam == EXAMID_HKDSE_APPEAL){
		$filename = 'HKDSE_APPEAL.csv';
	}
	$headers = array('Class','ClassNo','Name','Chi','Chi Grade','Eng','Eng Grade','Maths', 'Maths grade','LS','LS Grade','Elective 1', 'Elective 1 Grade','Elective 2', 'Elective 2 Grade','Elective 3', 'Elective 3 Grade', 'Elective 4', 'Elective 4 Grade' , 'Best 5 score');
	$export_content = $lexport->GET_EXPORT_TXT($rows, $headers,"","\r\n","",0,"11");
	$lexport->EXPORT_FILE($filename, $export_content);
}
else{
	$exportColumn = $libpf_exam->getExportHeader();
	$ExamResultArr = $libpf_exam->getClassExamResult($academicYearID,$exam, $ClassName);

	$studnetIDArr = array_unique((array)Get_Array_By_Key($ExamResultArr,'StudentID'));
	$studentInfoAssoc = BuildMultiKeyAssoc( $libpf_exam->getStudentsInfoByID($studnetIDArr, $academicYearID),'UserID');
	$subjectIDArr = array_unique((array)Get_Array_By_Key($ExamResultArr,'SubjectID'));
	$subjectInfoAssoc = BuildMultiKeyAssoc( $libpf_exam->getSubjectCodeAndID($subjectIDArr), 'SubjectID');
	$academicYearName = getAcademicYearByAcademicYearID($academicYearID);
	$exportData = array();
	for($i=0;$i<count($ExamResultArr);$i++){
		
		$_thisStudentId = $ExamResultArr[$i]['StudentID'];
		$_thisSubjectId = $ExamResultArr[$i]['SubjectID'];
		
		$exportData[$i][] = $academicYearName;
		$exportData[$i][] = $studentInfoAssoc[$_thisStudentId]['ClassName'];
		$exportData[$i][] = $studentInfoAssoc[$_thisStudentId]['ClassNumber'];
		$exportData[$i][] = $studentInfoAssoc[$_thisStudentId]['Name'];
		$exportData[$i][] = Get_Lang_Selection($subjectInfoAssoc[$_thisSubjectId]['displayNameCh'], $subjectInfoAssoc[$_thisSubjectId]['displayNameEn']);
		if($exam == EXAMID_HKAT){
			$exportData[$i][] = ($ExamResultArr[$i]['Score'] == 0 && $ExamResultArr[$i]['Grade'] != '')? $ExamResultArr[$i]['Grade']: $ExamResultArr[$i]['Score'];
		}
		else{
			$exportData[$i][] = ($ExamResultArr[$i]['Score'] == '')? $ExamResultArr[$i]['Grade']: $ExamResultArr[$i]['Score'];
		}
	}
	sortByColumn2($exportData,'1',0,0,'2');
	switch($exam){
		case EXAMID_HKAT:
			$exam_name = 'HKAT';
			break;
		case EXAMID_MOCK:
			$exam_name = 'MOCK';
			break;
		case EXAMID_HKDSE:
			$exam_name = 'HKDSE';
			break;
		case EXAMID_HKDSE_APPEAL:
			$exam_name = 'HKDSE_APPEAL';
			break;
	}
	$classDisplay = (empty($ClassName)) ? '' : '('.implode(',',(array)$ClassName).')';
	$filename = $exam_name.'_'.$academicYearName.''.$classDisplay.'.csv' ;
	$filename = iconv("UTF-8", "Big5", $filename);
}
if($task == 'HKDSE_websams_template'){
	$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, ",", "\r\n", ",", 0, "11",1);
	$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="Big5");
}
else if($task == 'exportDSE_format2'){
	
}
else{
	$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, "", "\r\n", "", 0, "11",1);
	$lexport->EXPORT_FILE($filename, $export_content);
}
intranet_closedb();
// Output the file to user browser
?>