<?php
// Modifing by 
################# Change Log [Start] #####
#
#	Date	:	2016-02-26	Ivan
#				modified: include archived user in the delete data logic now
#
#	Date	:	2016-02-01	Omas
# 				Create this page
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
intranet_opendb();

$linterface = new libPopupInterface("sdas_popup_template.html");
$libpf_exam = new libpf_exam($exam);
$liblog = new liblog();

if( $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && ($exam == EXAMID_HKDSE || $exam == EXAMID_HKDSE_APPEAL)){
	include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
	$lcees = new libcees();
	$endorseAlready = $lcees->checkIfEndorsed($academicYearID, $exam);
	if($endorseAlready){
		die();
	}
}


//$targetStudentIDArr = $libpf_exam->getStudentIDByClassNameAndYear((array)$academicYearID, (array)$className);

# ARCHIVE User Record not delete
// 20160226 - since now can import archive user data, ignore the exclude archive user logic first
//$sql = "SELECT UserID from INTRANET_ARCHIVE_USER where RecordType  = 2 and UserID In ('".implode("','",(array)$targetStudentIDArr)."')";
//$ArchivedUserAry = $libpf_exam->returnVector($sql);
// if(count($ArchivedUserAry)>0){
// 	$notInArchived_conds = " AND UserID not IN('".implode("','",$ArchivedUserAry)."') ";
// }

if(  $exam !=='' && $academicYearID>0){
	# delete records in EXAM_STUDENT_SCORE
// 	$sql = "DELETE FROM {$intranet_db}.EXAM_STUDENT_SCORE where ExamID = '$exam' AND AcademicYearID = '$academicYearID' AND StudentID IN ('".implode("','",(array)$targetStudentIDArr)."') $notInArchived_conds";
// 	$success = $libpf_exam->db_db_query($sql);
// 	$totalNum = $libpf_exam->db_affected_rows();
	
	// 20160226 - since now can import archive user data, ignore the exclude archive user logic first
	//$totalNum = $libpf_exam->deleteExamData($exam,$academicYearID,$targetStudentIDArr,$ArchivedUserAry);
	$totalNum = $libpf_exam->deleteExamData($exam,$academicYearID);
	
	
	# delete log
	$logDetails = '';
	$logDetails .= 'TargetAcademicYear = '.$academicYearID.' <br /> ';
	$logDetails .= 'TargetExam = '.$exam.' <br /> ';
	//$logDetails .= 'TargetStudent = '.implode("','",(array)$targetStudentIDArr).' <br /> ';
	$logDetails .= 'Total Deleted Record = '.$totalNum.' <br /> ';
	$logDetails .= 'Success = '.$success.' <br /> ';
	$liblog->INSERT_LOG('SDAS', 'erase other exam', $logDetails, 'EXAM_STUDENT_SCORE');
}

$CurrentPage = "management.importData";
$title = $ec_iPortfolio['remove_assessment_info'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

$display_content = "<table border='0' cellpadding='8' cellspacing='0'>";
$display_content .= "<tr><td class='tabletext'>".$ec_iPortfolio['number_of_removed_record']." : </td><td class='tabletext'><b>".($totalNum==''?'0':$totalNum)."</b></td></tr>\n";
$display_content .= "</table>\n";

?>

<FORM enctype="multipart/form-data" action="" method="POST" name="form1">
	<br>
	<?= $display_content ?>
	<br>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<p>
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$Lang['SDAS']['Exam']['ContinueToErase']?>" onClick="self.location='index.php?t=management.data_import.exam.data_remove'">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="javascript:window.close()">
	</p>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
