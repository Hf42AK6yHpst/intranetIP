<?php
// Modifing by 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
intranet_opendb();

$libpf_exam = new libpf_exam($_POST['examID']); 

$classArr = $libpf_exam->getClassWithRecords($_POST['academicYearID']);

$classCheckBox = "<table width=\"360\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
$classCheckBox .= "<tr><td colspan=\"3\"><input type=\"checkbox\" id=\"YCCheckMaster\" /> <label for=\"YCCheckMaster\">{$button_check_all}</label></td></tr>";
$count = 0;
foreach((array) $classArr as $_class_info){
	$_value = $_class_info['ClassTitleEN'];
	$_displayLang = Get_Lang_Selection($_class_info['ClassTitleB5'],$_class_info['ClassTitleEN']);
	$_engDisplay = $_class_info['ClassTitleEN'];
	
	if( ($count % 3) == 0){
		$classCheckBox .= "<tr>";
	}
	
	$classCheckBox .= "<td>";
	$classCheckBox .= '<input type="checkbox" class="ClassSel" id="class_'.$_engDisplay.'" name="ClassName[]" value="'.$_value.'" /><label for="class_'.$_engDisplay.'">'.$_displayLang.'</label>&nbsp;';
	$classCheckBox .= "</td>";
	
	if( ($count % 3) == 2){
		$classCheckBox .= "</tr>";
	}
	$count ++;
}
$classCheckBox .= "</table>";

echo $classCheckBox;
intranet_closedb();
?>