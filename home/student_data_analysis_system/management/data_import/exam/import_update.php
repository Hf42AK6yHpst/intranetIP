<?php
// Modifing by 
################# Change Log [Start] #####
#   Date    :   2018-07-11 Anna
#               Added DI, DII grade for DSE
#
#	Date	:	2017-08-02	Omas
#				changed DSE appeal logic
#
#	Date	:	2017-07-07	Omas
#				Add grade checking
#				show exam and year name after successful 
#
#	Date	:	2017-06-16	Villa
#				Add DSEAPPEAL
#
#	Date	:	2017-04-10	Omas -#M113647 
#				avoid insert 0 for - , * , / , N.A, N.T
#
#	Date	:	2017-03-09	Omas
#				fixed UTF8 csv problem - #C114368
#				added header checking for HKDSE websams format
#
#	Date	:	2016-08-01	Omas
#				add DSE clear data logic
#
#	Date	:	2016-07-08	Omas
#				add special detection for DSE - combined science 
#
#	Date	:	2016-06-14	Omas
#				all websamsNo checking now is case insensitive
#
#	Date	:	2016-04-11	Omas
# 				- DSE support websams format
#				- HKAT fix wrongly mapping to deleted subject - #J94740 
#				- HKAT support non-numeric score to be a grade
#
#	Date	:	2016-04-08	Omas
# 				DSE get userID from STRN (if fail from HKID)
#
#	Date	:	2016-02-26	Omas
# 				fixed mock exam infinity loop problem
#
#	Date	:	2016-02-26	Omas
# 				support import to archived student
#
#	Date	:	2016-02-01	Omas
# 				Create this page
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

intranet_opendb();
$examID = $_POST['exam'];
$academicYearID = $_POST['academicYearID'];
$libpf_exam = new libpf_exam($examID);

############# Skip Import Show Error for HKDSE (Start) ##########
$skipImport = false;
if( $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $examID == EXAMID_HKDSE || $examID == EXAMID_HKDSE_APPEAL){
	include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
	$lcees = new libcees();
	$endorseAlready = $lcees->checkIfEndorsed($academicYearID, $examID);
	if($endorseAlready){
		$skipImport = true;
	}
}
if( $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $examID == EXAMID_HKDSE){
	$haveAppealRecord = count($libpf_exam->getExamData($academicYearID, EXAMID_HKDSE_APPEAL)) > 0;
	if( $haveAppealRecord){
		$skipImport = true;
	}
}
if($examID == EXAMID_HKDSE_APPEAL){
	$noDseRecord = count($libpf_exam->getExamData($academicYearID, EXAMID_HKDSE)) == 0;
	if($noDseRecord){
		$skipImport = true;
	}
}
############# Skip Import Show Error for HKDSE (End) ##########
if(!$skipImport){
	$filepath = $_FILES['userfile']['tmp_name'];
	$filename = $_FILES['userfile']['name'];
	if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath)){
		header("Location: index.php?t=management.data_import.exam.import");
		exit;
	}
	else{
		
		############################################
		##	Step 0  - read file 				  ##
		############################################
		
		$lo = new libfilesystem();
		$limport = new libimporttext();
		
		$ext = strtoupper($lo->file_ext($filename));
		if($ext == ".CSV" ||$ext == ".TXT" )
		{
			if($examID == EXAMID_HKDSE || $examID == EXAMID_HKDSE_APPEAL){ //add EXAMID_HKDSE_APPEAL
				if( ($format == 1 || $format == '') && $ext == ".TXT" ){
					// txt (specific format) - fixed space length HKEAA format 
					$data = $lo->file_read($filepath);
					$useSTRN = true;
					$useWebSAM = false;
				}
				else if($format == 2 && $ext == ".CSV"){
					// comma separated websams format
					//$data = $lo->file_read_csv($filepath);
					$filePointer = fopen($filepath, "r");
					while (($csv = fgetcsv($filePointer, 0, ",")) !== FALSE) {
						$data[] = $csv;
					}
					$header_row = array_shift($data);
					$useSTRN = false;
					$useWebSAM = true;
				}
				else{
					header("Location: index.php?t=management.data_import.exam.import&xmsg=import_failed");
					exit;
				}
			}
			else if($examID == EXAMID_HKAT){
				if($format == 1 || $format == ''){
					// tab seperated
					$data = $limport->GET_IMPORT_TXT($filepath);
					$header_row = array_shift($data);
					$useSTRN = false;
					$useWebSAM = true;
				}
				else if($format == 2 && $ext == ".CSV"){
					// comma separated websams format
					$data = $lo->file_read_csv($filepath);
					$header_row = array_shift($data);
					$useSTRN = false;
					$useWebSAM = true;
					
					// AT format 2
					$tempDataArr = array();
					foreach((array)$data as $row){
						if(!empty($row)){
							$_regno = $row[1];
							$_strn = $row[2];
							$_subjectName = ucfirst(strtolower($row[6]));
							$_mark = $row[7];
					
							switch($_subjectName){
								case 'Chi':
									$_subjectCol = 2;
									break;
								case 'Eng':
									$_subjectCol = 3;
									break;
								case 'Math':
									$_subjectCol = 4;
									break;
					
							}
							$tempDataArr[$_regno][0] = $_regno;
							$tempDataArr[$_regno][1] = $_strn;
							$tempDataArr[$_regno][$_subjectCol] = $_mark;
						}
						$tempStudentWebSAMSArr[] = $_regno;
					}
					$data = $tempDataArr;
					unset($tempDataArr);
				}
				else{
					header("Location: index.php?t=management.data_import.exam.import&xmsg=import_failed");
					exit;
				}
			}
			else if($examID == EXAMID_MOCK){
				// comma separated
				$data = $lo->file_read_csv($filepath);
				$header_row = array_shift($data);
				$useSTRN = false;
				$useWebSAM = true;
			}
			
		}
		else{
			// wrong file format
			header("Location: index.php?t=management.data_import.exam.import&xmsg=import_failed");
			exit;
		}
		
		// 1st simple validation //add EXAMID_HKDSE_APPEAL exception
		if($examID != EXAMID_HKDSE && $examID != EXAMID_HKDSE_APPEAL){
			if(count($data) == 0){
				// file no record
				header("Location: index.php?t=management.data_import.exam.import&xmsg=import_no_record");
				exit;
			}
			if($format == 1 || $format == ''){
				// check header consistency
				if( !$libpf_exam->checkCompulsoryField($header_row) ){
					header("Location: index.php?t=management.data_import.exam.import&xmsg=import_header_failed");
					exit;
				}
			}
			else if($format == 2){
				// ONLY HKAT FORMAT 2
				if($examID == EXAMID_HKAT && count($header_row) != 8){
					header("Location: index.php?t=management.data_import.exam.import&xmsg=import_header_failed");
					exit;
				}
			}
		}
		else{
			// add header checking for HKDSE websams format //add EXAMID_HKDSE_APPEAL
			if(($examID == EXAMID_HKDSE || $examID == EXAMID_HKDSE_APPEAL) && $format == 2 && !$libpf_exam->checkCompulsoryField($header_row) ){
				header("Location: index.php?t=management.data_import.exam.import&xmsg=import_header_failed");
				exit;
			}
			
			if($format == 1 || $format == ''){
				// for DSE extract data here
				$dseDataArr = $libpf_exam->handleDseRawData($data);
			}
			else if($format == 2){
				// DSE format 2 here
				$dseDataArr = array();
				$tempStudentWebSAMSArr = array();
				foreach((array)$data as $key => $row){
					if(!empty($row)){
						
						$_websams = $row[4];
						$_websams = ($_websams[0] == '#')? $_websams : '#'.$_websams;
						$_subjectCode = $row[7];
						$_subjectGrade = $row[9];
					
						$dseDataArr[$_websams]['WebSams'] = $_websams;
						$dseDataArr[$_websams]['DSE'][$_subjectCode][0] = $_subjectGrade;
						if($row[10] != ''){
							$_cmpSubjectCode = $row[10];
							$_cmpSubjectGrade = $row[12];
							$dseDataArr[$_websams]['DSE'][$_subjectCode][] = $_cmpSubjectCode;
							$dseDataArr[$_websams]['DSE'][$_subjectCode][] = $_cmpSubjectGrade;
						}
						$tempStudentWebSAMSArr[] = $_websams;
					}
					$rowNum ++;
				}
			}
			if(empty($dseDataArr) || !is_array($dseDataArr)){
				header("Location: index.php?t=management.data_import.exam.import&xmsg=import_failed");
				exit;
			}
			else{
				// DSE appeal will not be deleted check inside deleteExamData()
				$libpf_exam->deleteExamData($examID, $_POST['academicYearID']);
			}
		}
	
		$insertArr = array();
		############################################
		##	1st Step - prepare data for validation##
		############################################
		
		// Get existing exam data
		if($examID == EXAMID_HKDSE_APPEAL){
			$examDataArr = $libpf_exam->getExamData($academicYearID, EXAMID_HKDSE);
		}else{
			$examDataArr = $libpf_exam->getExamData($academicYearID);
		}
		if($examID == EXAMID_HKDSE || EXAMID_HKDSE_APPEAL){
			// DSE use subjectCode to check
			//$existExamDataAssocArr = BuildMultiKeyAssoc($examDataArr,array('StudentID','realSubjectCode'),array('RecordID'),1);
			$existExamDataAssocArr = BuildMultiKeyAssoc($examDataArr,array('StudentID','realSubjectCode'),array(),1);
		}else{
			$existExamDataAssocArr = BuildMultiKeyAssoc($examDataArr,array('StudentID','SubjectID'),array('RecordID'),1);
		}
	
		if($useWebSAM){
			// Get student websams to userid mapping
			if($examID == EXAMID_HKAT && ($format == 1 || $format == '')){
				$tempStudentWebSAMSArr = Get_Array_By_Key($data,'0');
			}
			else if($examID == EXAMID_HKAT &&  $format == 2){
				// HKAT format 2 here
				$tempStudentWebSAMSArr = array_unique($tempStudentWebSAMSArr);
			}
			else if($examID == EXAMID_MOCK){
				$tempStudentWebSAMSArr = Get_Array_By_Key($data,8);
			}
			else if(($examID == EXAMID_HKDSE ||  $examID == EXAMID_HKDSE_APPEAL) && $format == 2){ //add EXAMID_HKDSE_APPEAL
				// from => DSE format 2 here
				$tempStudentWebSAMSArr = array_unique($tempStudentWebSAMSArr);
			}
	
			foreach((array)$tempStudentWebSAMSArr as $_key=>$_val){
				if($_val != ''){
					if($_val[0] == '#'){
						// first character is # , no need to add #
						$studentWebSAMSArr[$_key] = $_val;
					}
					else{
						$studentWebSAMSArr[$_key] = '#'.$_val;
					}
				}
			}
			$libuser = new libuser();
	// 		$userIDWebSamsAssoArr = BuildMultiKeyAssoc( $libuser->getUserIDsByWebSamsRegNoArr($studentWebSAMSArr,$isArchive, true) , 'WebSamsRegNo', array('UserID'),1);
			$tempArchiveArr = $libuser->getUserIDsByWebSamsRegNoArr($studentWebSAMSArr,1, true);
			$tempCurrentArr = $libuser->getUserIDsByWebSamsRegNoArr($studentWebSAMSArr,0, true);
			$userIDWebSamsAssoArr = BuildMultiKeyAssoc( array_merge($tempArchiveArr, $tempCurrentArr) , 'WebSamsRegNo', array('UserID'),1);
			unset($tempArchiveArr);
			unset($tempCurrentArr);
			unset($libuser);
		}
		else if($useSTRN){
			// Get student STRN to userid mapping
			$studenSTRNArr= Get_Array_By_Key($dseDataArr,'STRN');
			$libuser = new libuser();
	// 		$userIDSTRNAssoArr = BuildMultiKeyAssoc( $libuser->getUserIDsBySTRNArr($studenSTRNArr,$isArchive) , 'STRN', array('UserID'),1);
	// 		$userIDHKIDAssoArr = BuildMultiKeyAssoc( $libuser->getUserIDsByHKIDArr($studenSTRNArr,$isArchive) , 'HKID', array('UserID'),1);
			$tempArchiveSTRNArr = $libuser->getUserIDsBySTRNArr($studenSTRNArr,1);
			$tempCurrentSTRNArr = $libuser->getUserIDsBySTRNArr($studenSTRNArr,0);
			$userIDSTRNAssoArr = BuildMultiKeyAssoc( array_merge($tempArchiveSTRNArr, $tempCurrentSTRNArr) , 'STRN', array('UserID'),1);
			$tempArchiveHKIDArr = $libuser->getUserIDsByHKIDArr($studenSTRNArr,1);
			$tempCurrentHKIDArr = $libuser->getUserIDsByHKIDArr($studenSTRNArr,0);
			$userIDHKIDAssoArr = BuildMultiKeyAssoc( array_merge($tempArchiveHKIDArr,$tempCurrentHKIDArr) , 'HKID', array('UserID'),1);
			unset($tempArchiveSTRNArr);
			unset($tempCurrentSTRNArr);
			unset($tempArchiveHKIDArr);
			unset($tempCurrentHKIDArr);
			unset($libuser);
		}
		
		############################################
		##	2nd Step - prepare Arr for last step  ##
		############################################
		switch($examID){
			case EXAMID_HKAT:
				$SubjectCodeIDAssocArr = BuildMultiKeyAssoc($libpf_exam->getSubjectCodeAndID(array(), array('080','165','22S'), true), array('CODEID'),array('SubjectID'),1);
				
				foreach((array)$data as $_key => $_datainfo_arr){
					$_thisWebSAMSNum = $_datainfo_arr[0]; // websams
					if($_thisWebSAMSNum != ''){
						$_thisWebSAMSNum = ($_thisWebSAMSNum[0] == '#' )?  $_thisWebSAMSNum: '#'.$_thisWebSAMSNum;
					}
	// 				$_thisSTRN = $_datainfo_arr[1]; // strn
	// 				$_Score = $_datainfo_arr[2]; // chi
	// 				$_Score = $_datainfo_arr[3]; // eng
	// 				$_Score = $_datainfo_arr[4]; // math
	
					$_thisUserID = $userIDWebSamsAssoArr[strtolower($_thisWebSAMSNum)];
	//				STRN get map to userID Not yet test
	// 				if($_thisUserID == ''){
	// 					$_thisUserID = $userIDSTRNAssoArr[$_thisSTRN];
	// 					if($_thisUserID == ''){
	// 						$_thisUserID = $userIDHKIDAssoArr[$_thisSTRN];
	// 					}
	// 				}
					
					if($_thisUserID != ''){
						for($__x = 2 ; $__x < 5; $__x++){
							$__thisScore = $_datainfo_arr[$__x];
							$__thisGrade = (is_numeric($__thisScore))? '' : $__thisScore;
							switch($__x){
								case 2: $__thisSubjectID = $SubjectCodeIDAssocArr['080']; $errorReturn = 'Chi'; break; // Chi
								case 3: $__thisSubjectID = $SubjectCodeIDAssocArr['165']; $errorReturn = 'Eng'; break; // Eng
								case 4: $__thisSubjectID = $SubjectCodeIDAssocArr['22S']; $errorReturn = 'Math'; break; // Math
							}
							if($__thisSubjectID !=''){
								if(!empty($existExamDataAssocArr[$_thisUserID][$__thisSubjectID])){
									$__existRecordID = $existExamDataAssocArr[$_thisUserID][$__thisSubjectID];
									$updateArr[$__existRecordID]['Score'] = $__thisScore;
									$updateArr[$__existRecordID]['Grade'] = (is_numeric($__thisScore))? '' : $__thisScore;
								}
								else{
									if($__thisSubjectID != ''){
										$insertArr[] = "( '$academicYearID', '$examID', '$__thisSubjectID', '$_thisUserID', '$__thisScore', '$__thisGrade', now(),'$UserID', now(),'$UserID' )";
									}
								}							
							}
							else{
								$error_data[$_key]['subject'][] = $errorReturn;
							}
						}
					}
					else{
						// cannot found the user
						$error_data[$_key]['user'] = $_thisWebSAMSNum;
					}
				}
				break;
			case EXAMID_HKDSE_APPEAL: //add DSE appeal
			case EXAMID_HKDSE:
				$SubjectCodeIDAssocArr = $libpf_exam->getDseWebSAMSSubjectMapping(1);
				$SubjectIDAssocArr = $libpf_exam->getDseWebSAMSSubjectMapping(0);
				foreach($dseDataArr as $_key => $_userInfoArr){
					if($format == 1 || $format == ''){
					// HKID map to STRN / HKID
					$_STRN = $_userInfoArr['STRN'];
					$_thisUserID = $userIDSTRNAssoArr[$_STRN];
						if($_thisUserID == ''){
							$_thisUserID = $userIDHKIDAssoArr[$_STRN];
						}
					}else if($format == 2){
						$_webSams = $_userInfoArr['WebSams'];
						$_thisUserID = $userIDWebSamsAssoArr[strtolower($_webSams)];
					}
					
					if(  $_thisUserID != '' ){
						$_DSEinfoArr = $_userInfoArr['DSE'];
						foreach((array)$_DSEinfoArr as $__subjectCode => $__scoreInfoArr){
							$__mainSbjSkipInsert = false;
							$DSE_MAP_WEBSAMS = array_flip($libpf_exam->DSE_MAP_WEBSAMS);
							$__thisSubjectID = $libpf_exam->getSubjectIDByDseSubjectCode($__subjectCode);
							$__grade = $__scoreInfoArr[0];
	
							// grade checking
							$dseGradeArr['A'] = array('X','Z','U','1','2','3','4','5','5*','5**');
							$dseGradeArr['B'] = array('TD','TT','UT','X','Z','DI','DII','D1','D2');
							$dseGradeArr['C'] = array('A','B','C','D','E','UG','X','Z');
							if(!in_array(strtoupper($__grade),(array)$dseGradeArr[$__subjectCode[0]])){
								$error_data[$_key]['grade'] = $__subjectCode.': '.$__grade;
							}
							
							// handling combined science - Start
							if($__subjectCode == 'A165'){
								if (in_array('A161', $__scoreInfoArr)) {
									$bio = true;
								}
								if (in_array('A162', $__scoreInfoArr)) {
									$chem = true;
								}
								if (in_array('A163', $__scoreInfoArr)) {
									$phy = true;
								}
								$webSAMSAssoc = $libpf_exam->getSubjectIDWebSAMSMapping();
								if($bio && $chem && !$phy){
									//	22N
									$__thisSubjectID = $webSAMSAssoc['22N'];
								}else if($phy && $chem && !$bio){
									// 23N
									$__thisSubjectID = $webSAMSAssoc['23N'];
								}else if($bio && $phy && !$chem){
									// 24N
									$__thisSubjectID = $webSAMSAssoc['24N'];
								}else if($bio && $chem && $phy){
									$__thisSubjectID = $__thisSubjectID;
								}else{
									$__thisSubjectID = $webSAMSAssoc['21N'];
								}
								$combinedScParentID = $__thisSubjectID;
							}
							// handling combined science - End
							
							$SubjectID = ($__thisSubjectID > 0) ? $__thisSubjectID : '-999';
							$ParentSubjectID = '';
							// DSE APPEAL
							if($examID == EXAMID_HKDSE_APPEAL && !empty($existExamDataAssocArr[$_thisUserID][$__subjectCode])){
								$__existRecordInfo = $existExamDataAssocArr[$_thisUserID][$__subjectCode];
								$__existingGrade = $__existRecordInfo['Score'];
								
								$__mainSbjSkipInsert = false;
								if($__grade != $__existingGrade){
									if($__existRecordInfo['ExamID'] == EXAMID_HKDSE){
										// only original record will be archive
										$sql = "UPDATE EXAM_DSE_STUDENT_SCORE SET isArchive = 1 WHERE RecordID = '{$__existRecordInfo['RecordID']}'";
										$libpf_exam->db_db_query($sql);
									}else{
										// APPEAL record will be delete
										$libpf_exam->removeExistingAppealRecord($academicYearID, $_thisUserID, $__subjectCode);
									}
								}else{
									// same grade skip
									$__mainSbjSkipInsert = true;
								}
							}
							if(!$__mainSbjSkipInsert){
								// main subject new record (Start)
								$AcademicYearID = $academicYearID;
								$ExamID = $examID;
								$SubjectCategory = $__subjectCode[0];
								$SubjectCode = $__subjectCode;
								$ParentSubjectCode = '';
								$StudentID = $_thisUserID;
								$Grade = $__grade;
								
								$insertArr[] = "( '$AcademicYearID', '$ExamID',
												'$SubjectID', '$ParentSubjectID',
												'$SubjectCategory', '$SubjectCode', '$ParentSubjectCode',
												'$StudentID','$Grade', now(),'$UserID', now(),'$UserID' )";
		
								if($SubjectID == '-999'){
									// no this subject.. Db insert as -999
									$error_data[$_key]['subject'][] = $__subjectCode;
								}
							}
							// main subject new record (End) 
							
							
							// Component Subject add here (Start)
							$__numScore = count($__scoreInfoArr);
							if( in_array($__subjectCode, $libpf_exam->getWithComponentDseSubject()) && $__subjectCode!= '' &&  $__numScore > 2){
								/*
								 $__numScore > 1 == this sbj contain sbj cmp
								 $__scoreInfoArr = array('5**','1','5**','2','5**','3','5**')
								 $__scoreInfoArr[0] = parent sbj grade
								 $__scoreInfoArr[1] = parent sbj's sbj cmp code 
								 $__scoreInfoArr[2] = grade of parent sbj's sbj cmp - $__scoreInfoArr[1]
								 $__scoreInfoArr[3+] = same as 1,2 logic 
								 */
								$___x = 1;
								while( $___x < $__numScore){
									$___thisCmpCode = $__scoreInfoArr[$___x];
									$___thisCmpGrade = $__scoreInfoArr[$___x+1];
									$___thisCmpSubjectID = $libpf_exam->getSubjectIDByDseSubjectCode($__subjectCode,$___thisCmpCode);
									$___realSubjectCode = $__subjectCode.'-'.$___thisCmpCode;
									$SubjectID = ($___thisCmpSubjectID > 0) ? $___thisCmpSubjectID : '-999';
									$ParentSubjectID = $__thisSubjectID;
									
									if($___thisCmpCode != ''){
										if($examID == EXAMID_HKDSE_APPEAL && !empty($existExamDataAssocArr[$_thisUserID][$___realSubjectCode])){
											$__existRecordInfo = $existExamDataAssocArr[$_thisUserID][$___realSubjectCode];
											$__existingGrade = $__existRecordInfo['Score'];
											if($___thisCmpGrade != $__existingGrade){
												if($__existRecordInfo['ExamID'] == EXAMID_HKDSE){
													// only original record will be archive
													$sql = "UPDATE EXAM_DSE_STUDENT_SCORE SET isArchive = 1 WHERE RecordID = '{$__existRecordInfo['RecordID']}'";
													$libpf_exam->db_db_query($sql);
												}else{
													// APPEAL record will be delete
													$libpf_exam->removeExistingAppealRecord($academicYearID, $_thisUserID, $__subjectCode, $___thisCmpCode);
												}
											}else{
												// same grade skip
												$___x = $___x + 2;
												continue;
											}
										}
										// new record
										$AcademicYearID = $academicYearID;
										$ExamID = $examID;
										$SubjectCategory = $__subjectCode[0];
										$SubjectCode = $___thisCmpCode;
										$ParentSubjectCode = $__subjectCode;
										$StudentID = $_thisUserID;
										$Grade = $___thisCmpGrade;
										
										$insertArr[] = "( '$AcademicYearID', '$ExamID',
														'$SubjectID', '$ParentSubjectID',
														'$SubjectCategory', '$SubjectCode', '$ParentSubjectCode',
														'$StudentID','$Grade', now(),'$UserID', now(),'$UserID' )";
										if($SubjectID == '-999'){
											//	No this Cmp subject
											$error_data[$_key]['subject'][] = $___realSubjectCode;
										}
									}
									$___x = $___x + 2;
								}
							}
							// Component Subject add here (End)
						}
					}else{
						// No this user
						if($format == 1 || $format == ''){
							$error_data[$_key]['user'] = $_STRN;
						}else if($format == 2){
							$error_data[$_key]['user'] = $_webSams;
						}
					}
				}
				break;	
			case EXAMID_MOCK:
				
				// Prepare Subject Name Map to Subject ID
				$subjectArr = $libpf_exam->getSubjectCodeAndID();
				foreach((array)$subjectArr as $_subjectInfo){
					if($_subjectInfo['en_sname'] == ''){
						// skip those empty element
						continue;
					}
					if($_subjectInfo['code'] == ''){
						// subject
						$_subjectMappingCode = $_subjectInfo['en_sname'];
					}else{
						// subject component
						$_subjectMappingCode = $_subjectInfo['en_sname'].'###'.$_subjectInfo['code'];
					}
					$SubjectAssoArr[strtolower($_subjectMappingCode)] = $_subjectInfo;
				}
	
				// 			// Check Subject Existence
				// 			$numHeader = count($header_row);
				// 			for($y=3; $y<$numHeader; $y++){
				// 				$_subjectCode = $header_row[$y];
				// 				$isSubjectExist = $AssocArr[$_subjectCode];
				// 				if($isSubjectExist==''){
				// 					//	No this subject
				// 					$errorArr['subject'][] = $_subjectCode;
				// 				}
				// 			}
				
				// Extract subjectID by import file header
				//$header_row[8] = websams
				$i = 9;
				$numHeader = count($header_row);
				$SubjectIDAssoArr = array();
				$subjectErrorArr = array();
				while ($i < $numHeader){
					$target = $header_row[$i];
					$temp = explode("_", $target);
	
					switch(sizeof($temp)){
						case 2: 
							// Overall result can skip 
							$SubjectIDAssoArr[$i] = 'END';
							$i++;
							break;
						case 4:
							// main subject
							$_subjectCode = $temp[1];
							$_subjectID = $SubjectAssoArr[strtolower($_subjectCode)]['SubjectID'];
							$SubjectIDAssoArr[$i] = $_subjectID;
							$SubjectIDAssoArr[$i+1] = $_subjectID;
	// 						$SubjectIDAssoArr[$i+2] = $_subjectID;
							$i = $i + 3;
							break;
						case 5:
							// component subject
							$_subjectCode = $temp[1].'###'.$temp[2];
							$_subjectID = $SubjectAssoArr[strtolower($_subjectCode)]['SubjectID'];
							$SubjectIDAssoArr[$i] = $_subjectID;
							$SubjectIDAssoArr[$i+1] = $_subjectID;
	// 						$SubjectIDAssoArr[$i+2] = $_subjectID;
							$i = $i + 3;
							break;
						case 3:
							$SubjectIDAssoArr[$i] = 'END';
							$i++;
							break;
						default :
							$i++;
							break;
					}
					if(empty($_subjectID)){
						if(!in_array($_subjectCode,(array)$subjectErrorArr)){
							$error_data[]['subject'][] = str_replace('###','_',$_subjectCode);
							$subjectErrorArr[] = $_subjectCode;
						}
					}
				}unset($i);
				// end of while loop	
				
	// 			if(empty($error_data)){
				// no subject error - loop each student
				foreach((array)$data as $_key => $_datainfo_arr){
					// skip empty row
					if(!empty($_datainfo_arr)){
						$_thisWebSAMS = $_datainfo_arr[8];
						$_thisUserID = $userIDWebSamsAssoArr[strtolower($_thisWebSAMS)];
	// 					$_thisUserID = $userIDWebSamsAssoArr[$_thisWebSAMS];
						if( $_thisUserID != ''){
							$first_subject_pos = 9;
							$__x = $first_subject_pos;
							// loop each subject
							while($__x<$numHeader){
								$skipThisRecord = false;
								$__thisSubjectID = $SubjectIDAssoArr[$__x];
								
								if($__thisSubjectID != 'END' && $__thisSubjectID != '' ){
									// #M113647 
	// 								$__score = str_replace('*','',$_datainfo_arr[$__x]);
	// 								$__grade = str_replace('*','',$_datainfo_arr[$__x+1]);
	// 								$__rank  = str_replace('*','',$_datainfo_arr[$__x+2]);
									$__tScore = $_datainfo_arr[$__x];
									$__grade = $_datainfo_arr[$__x+1];
									$__rank = $_datainfo_arr[$__x+2];
	
									if (($__score == "N.T." && $__grade == "N.T." && $__rank == "N.T.") || ($__score == "N.A." && $__grade == "N.A." && $__rank == "N.A.")){
										$skipThisRecord = true;
									}
									$__score = ( $__tScore=="*" || $__tScore=="-" || $__tScore=="" || $__tScore=="/" || strtoupper($__tScore)=="N.T." || strtoupper($__tScore)=="N.A.") ? "-1" : $__tScore;
									$__score = $__score * 1;
										
									// prepare insert & update arr
									if(!$skipThisRecord){
										if(!empty($existExamDataAssocArr[$_thisUserID][$__thisSubjectID])){
											// old record
											$__existRecordID = $existExamDataAssocArr[$_thisUserID][$__thisSubjectID];
											$updateArr[$__existRecordID]['Grade'] = $__grade;
											$updateArr[$__existRecordID]['Score'] = $__score;
										}
										else{
											// new record
											if($__thisSubjectID != ''){
												$insertArr[] = "( '$academicYearID', '$examID', '$__thisSubjectID', '$_thisUserID', '$__score' ,'$__grade', now(),'$UserID', now(),'$UserID' )";
											}
										}
									}
									$__x = $__x + 3;
								}
								else if($__thisSubjectID == 'END'){
									break;
								}
								else{
									$__x ++;
								}
							}
						}
						else{
							// No this user
							$error_data[$_key]['user'] = $_thisWebSAMS;					
						}
					}
				}
	// 			}
				break;
		}
		############################################
		##	Last Step - changes to db			  ##
		############################################

		if(count($updateArr) > 0){
			$success_update = $libpf_exam->updateExamData($updateArr);		
		}
		if( count($insertArr) > 0){
			$success_insert = $libpf_exam->insertExamData($insertArr);
		}	
	}
}

$yearInfoArr = BuildMultiKeyAssoc( GetAllAcademicYearInfo(), 'AcademicYearID' );
$yearName = Get_Lang_Selection($yearInfoArr[$_POST['academicYearID']]['YearNameEN'], $yearInfoArr[$_POST['academicYearID']]['YearNameB5'] );
$navigationArr[] = array($yearName,'');
$navigationArr[] = array($libpf_exam->getExamName(),'');
$display_content = '';
$display_content .= $linterface->GET_NAVIGATION_IP25($navigationArr);
$display_content .= '<br>';
// Display Result
if(!empty($success_insert) || !empty($success_update) ){
	
// 	$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_add).'</td><td class="tabletext">'.count($insertArr).'</td></tr>';
// 	$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_update).'</td><td class="tabletext">'.count($updateArr).'</td></tr>';
	$display_content .= '<table border="0" cellpadding="5" cellspacing="0">';
	if($examID != EXAMID_HKDSE_APPEAL){
		$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_add).'</td><td class="tabletext">'.($success_insert==''?'0':$success_insert).'</td></tr>';
		$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_update).'</td><td class="tabletext">'.count($success_update).'</td></tr>';
	}else{
		$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_update).'</td><td class="tabletext">'.($success_insert==''?'0':$success_insert).'</td></tr>';
	}
	if(count($error_data) > 0){
		$display_content .= '<tr><td class="tabletext red">'.$Lang['SDAS']['Exam']['Import']['InvalidRecords'].' :'.'</td><td class="tabletext red">'.count($error_data).'</td></tr>';
	}
	$display_content .= '</table>';
}

if( $examID == EXAMID_HKDSE_APPEAL && empty($success_insert) && empty($success_update) && !$skipImport){
	$display_content .= "<div style='font-size:14px; text-align:center;'><br>".$Lang['SDAS']['Exam']['Import']['NoRecordToUpdate']."</div>";
}
if( $examID == EXAMID_HKDSE_APPEAL && $skipImport){
	if($noDseRecord){
		$display_content .= "<div style='font-size:14px; text-align:center;'><br>".$Lang['SDAS']['Exam']['Import']['NoDseRecord']."</div>";
	}else if($endorseAlready){
		$display_content .= "<div style='font-size:14px; text-align:center;'><br>".$Lang['SDAS']['Exam']['Import']['EndorsedNotAllowToImport_Appeal']."</div>";
	}
}
if( $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $examID == EXAMID_HKDSE && $skipImport){
	if($endorseAlready){
		$display_content .= "<div style='font-size:14px; text-align:center;'><br>".$Lang['SDAS']['Exam']['Import']['EndorsedNotAllowToImport']."</div>";
	}
	if($haveAppealRecord){
		$display_content .= "<div style='font-size:14px; text-align:center;'><br>".$Lang['SDAS']['Exam']['Import']['AlreadyAppealNotAllowToImport']."</div>";
	}
}

if (sizeof($error_data)!=0){
	$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
	$error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td nowrap class='tabletext'>".$ec_guide['import_error_data']."</td><td width='45%' class='tabletext'>".$ec_guide['import_error_reason']."</td><td width='55%' class='tabletext'>".$ec_guide['import_error_detail']."</td></tr>\n";
	
// 		for ($i=0; $i<sizeof($error_data); $i++)
	$error_count = 0;
	foreach ($error_data as $i => $data_array){
		list ($t_row, $t_type, $t_data) = $error_data[$i];
		
		$t_row = $i;
		if(isset($error_data[$i]['user'])){
			$t_type = 'user';
			$t_data = $error_data[$i]['user'];
			if( $error_data[$i]['user'] == ''){
				$t_data = $Lang['iPortfolio']['DynamicReport']['StatisticsArr']['Blank'];
			}
		}
		else if(isset($error_data[$i]['subject'])){
			$t_type = 'subject';
			$t_data = $error_data[$i]['subject'];
// 			$t_data = implode(', ',$error_data[$i]['subject']);
		}else if(isset($error_data[$i]['grade'])){
			$t_type = 'grade';
			$t_data = $error_data[$i]['grade'];
		}
		
		if ($t_row==-1){
			$t_row = "Column(s)";
		} else {
			if($format == 1 || $format ==''){
				$t_row++;     # set first row to 1
			}
		}
		$css_color = ($error_count%2==0) ? "#FFFFFF" : "#F3F3F3";
		$error_table .= "<tr bgcolor='$css_color'><td class='tabletext red'>$t_row</td><td class='tabletext red'>";
		$reason_string = $ec_guide['import_error_unknown'];
		switch ($t_type){
			case 'user':
				$reason_string = $ec_guide['import_error_no_user'];
				break;
			case 'subject':
				$reason_string = $Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectIsNotFound'];
				break;
			case 'grade':
				$reason_string = $Lang['SDAS']['DSE']['ImportError']['grade'];
				break;
		}
		$error_table .= $reason_string;

		$error_contents = (is_array($t_data)) ? implode(",",$t_data) : $t_data."&nbsp;";
		$error_table .= "</td><td class='tabletext red'>".$error_contents."</td></tr>\n";
		$error_count ++;
	}
	$error_table .= "</table>\n";
	$display_content .= $error_table;
}	
// }

$linterface = new libPopupInterface("sdas_popup_template.html");
$CurrentPage = "management.importData";
$title = $Lang['SDAS']['Exam']['ImportOtherExam'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;
$linterface->LAYOUT_START();
?>
<br>
<?= $display_content ?>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
</tr>
</table>

<p>
<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_guide['import_back']?>" onClick="self.location='index.php?t=management.data_import.exam.import'">
<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="self.close()">
</p>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>