<?php
# using 
################# Change Log [Start] #####
#   Date:   2019-06-26 Anna
#           - Change OtherExam no need aopen advance falg for general import at exam
#
#   Date    :   2018-11-23 Anna
#           - added ExitTo
#
#	Date	:	2016-10-04 Omas
#			- added JUPAS
#
#	Date	:	2016-07-07 Omas
#			- edited table header
#
#	Date	:	2016-04-21 Omas
#		 	- modified to only advance mode have exam import feature- $plugin['SDAS_module']['advanceMode']
################## Change Log [End] ######

######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "management.importData";
$CurrentPageName = $Lang['SDAS']['menu']['examMgmt'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

######## UI START ########
$linterface->LAYOUT_START();

# Header Row
$HeaderRow = "<tr class=\"tabletop\">";
$HeaderRow .= "<td width='20%'>&nbsp;</td>";
//$HeaderRow .= "<td nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$ec_iPortfolio['eClass_update']."<span style='color: red;'>*</span>&nbsp;</td>";
$HeaderRow .= "<td width='30%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$Lang['SDAS']['menu']['examMgmt']."&nbsp;</td>";
$HeaderRow .= "<td width='10%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$iPort['menu']['data_export']."&nbsp;</td>";
$HeaderRow .= "<td width='10%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$ec_iPortfolio['erase_info']."&nbsp;</td>";
$HeaderRow .= "<td width='30%' nowrap='nowrap' align=\"center\" class=\"tabletopnolink\">&nbsp;".$ec_iPortfolio['SAMS_last_record_change']."&nbsp;</td>";
$HeaderRow .= "</tr>";

$ButtonClass = "class=\"formbutton_v30\"";
$PreviousClass = ($PreviousClass=="" || $PreviousClass=="class=\"tablerow2\"") ? "class=\"tablerow1\"" : "class=\"tablerow2\"";

$UpdateBtn = ($plugin['ReportCard']) ? "<input name=\"button\" type=button value=\"".$button_update."\" onClick=\"doUpdateFromIP('assessment')\" $ButtonClass>" : "&nbsp;";

$AssessmentRow = "<tr valign=\"middle\" ".$PreviousClass.">
               <td nowrap='nowrap'>".$Lang['SDAS']['Exam']['SchoolExam']."</td>
               <td align=center><input onClick=\"doImport('int')\" type=button value=\"".$button_import."\" $ButtonClass></td>
			{$ReportCardButtonCell}
			<td align=center><input onClick=\"doExport('int')\" type=button value=\"".$button_export."\" $ButtonClass></td>
			<td align=center><input onClick=\"doRemove('int')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
               <td align=center nowrap='nowrap'>".$libpf_exam->getAssessmentLastInputDate()."</td>
             </tr>";
			
			$AssessmentRow .= "<tr valign=\"middle\" ".$PreviousClass.">
               <td nowrap='nowrap'>".$Lang['SDAS']['Exam']['OtherExam']."</td>
               <td align=center><input onClick=\"doImport('others')\" type=button value=\"".$button_import."\" $ButtonClass></td>
               {$ReportCardButtonCell}
               <td align=center><input onClick=\"doExport('others')\" type=button value=\"".$button_export."\" $ButtonClass></td>
               <td align=center><input onClick=\"doRemove('others')\" type=button value=\"".$button_erase."\" $ButtonClass></td>
               <td align=center nowrap='nowrap'>".$libpf_exam->getExamLastInputDate()."</td>
             </tr>";
if($plugin['SDAS_module']['advanceMode']){			

$AssessmentRow .= "<tr valign=\"middle\" ".$PreviousClass.">
               <td nowrap='nowrap'>".$Lang['SDAS']['JUPASRecord']."</td>
               <td align=center><input onClick=\"doImport('jupas')\" type=button value=\"".$button_import."\" $ButtonClass></td>
			               {$ReportCardButtonCell}
			               <td align=center><input onClick=\"doExport('jupas')\" type=button value=\"".$button_export."\" $ButtonClass></td>
			               <td align=center>--</td>
			               <td align=center nowrap='nowrap'>".$libpf_exam->getJupasLastInputDate()."</td>
             </tr>";
}

if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    $AssessmentRow .= "<tr valign=\"middle\" ".$PreviousClass.">
               <td nowrap='nowrap'>".$Lang['SDAS']['ExitToRecord']."</td>
               <td align=center><input onClick=\"doImport('exitto')\" type=button value=\"".$button_import."\" $ButtonClass></td>
               {$ReportCardButtonCell}
               <td align=center><input onClick=\"doExport('exitto')\" type=button value=\"".$button_export."\" $ButtonClass></td>
               <td align=center>--</td>
               <td align=center nowrap='nowrap'>".$libpf_exam->getExitToLastInputDate()."</td>
             </tr>";
               # SSPA Secondary School Places Allocation
}
// <td align=center><input onClick=\"doExport('jupas')\" type=button value=\"".$button_export."\" $ButtonClass></td>
// <td align=center><input onClick=\"doRemove('jupas')\" type=button value=\"".$button_erase."\" $ButtonClass></td>

# Table Open
$TableOpen =  "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">";
# Table Close
$TableClose = "</table>";
?>
<script>
var win_name = "ec_popup27";
var win_size = "width=610,height=400";
function doImport(type){
	var url;
	if(type == 'int'){
		url = "index.php?t=management.data_import.assessment.import_sams";
	}
	else if( type == 'jupas'){
		url = "index.php?t=management.data_import.jupas.import";
	}
	else if (type == 'exitto'){
		url = "index.php?t=management.data_import.exitto.import";	
	}
	else{
		url = "index.php?t=management.data_import.exam.import";		
	}
	
	window.open (url, win_name,win_size);
}
function doRemove(type){
	if(type == 'int'){
		url = "index.php?t=management.data_import.assessment.data_remove";
	}
	else if( type == 'jupas'){
// 		url = "index.php?t=management.data_import.jupas.data_remove";
	}
	else if(type == 'exitto'){
// 		url = "index.php?t=management.data_import.exitto.data_remove";
	}
	else{
		url = "index.php?t=management.data_import.exam.data_remove";
	}
	window.open (url, win_name,win_size);
}
function doExport(type){
	if(type == 'int'){
		url = "index.php?t=management.data_import.assessment.data_export";
	}
	else if( type == 'jupas'){
		url = "index.php?t=management.data_import.jupas.export";
	}
	else if (type == 'exitto' ){
		url = "index.php?t=management.data_import.exitto.export";
	}
	else{
		url = "index.php?t=management.data_import.exam.data_export";		
	}
	window.open (url, win_name,win_size);
}
</script>
<?=$TableOpen.$HeaderRow.$AssessmentRow.$TableClose?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>