<?php
//using : 
/*
 * Change Log [Start]
 * Date : 2018-08-08 [Carlos] - Apply cleanHtmlJavascript($html) to message to remove all script tags and onXXX event handlers.
 * Date : 2013-03-05 [Carlos] - Restore to previous design to load attachment in order to prevent forward mail fail to load attachment
 * Date : 2013-03-04 [Carlos] - cater $sys_custom['iMailComposeMailCompleteLoad'] to load msg to html editor
 * 
 * Date : 2012-12-13 [Carlos]
 * 			skip loading attachments and do js checking when load email content into html editor
 * 
 * Date	: 2012-11-30 [yuen]
 * 			fixed a strange problem in IE9 by adding "<p></p>" like (otherwise backspace will remove 2 characters in the text when replying external email)
 * 				<div name='BL_who_wrote' id='BL_who_wrote'><p></p><?=$preset_message_whowrite;?></div>
 * 
 * 
 * Date	: 2011-10-17 [Carlos]
 * 			added window.parent.setContent() to set parent compose mail content if request mode is feedback
 * 
 * Date : 2010-10-26 [Ronald]
 * 			use smartstripslashes() to remove backslash when showing the mail content
 * 
 * Date : 2010-12-01 [Ronald]
 * 			- if $preset_message is empty, return empty string to the div "BL_prev_msg"
 * 			- if the email is external mail, when Re/Fw, the sender will show in text format (e.g xxx <xxx@abc.com>) [Tempory Disabled] 
 * 
 * Date : 2010-10-26 [Ronald]
 * 			remove intranet_undo_htmlspecialchars() & stripslashes() when showing the mail content
 * 
 */

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libfiletable.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libcampusquota.php");
include_once("../../includes/libwebmail.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

if ($special_feature['imail_richtext'])
{
    $use_html_editor = true;
}

$li = new libcampusmail($CampusMailID);

if ($li->mailType==2)         # External
{
	//$mail_senderemail = intranet_htmlspecialchars($li->SenderEmail);
	$mail_senderemail = $li->SenderEmail;
	
    //$preset_message_whowrite = $mail_senderemail;
    $preset_message_whowrite = $li->returniMailSender();
    
    ### PENDING - Tempory disable by Ronald [2010-12-01]
    ### show Sender name with the sender email in full text format (e.g xxx <xxx@abc.com>)
    //$tempArray = $li->parseEmailAddress($mail_senderemail);
    //$preset_message_whowrite = $tempArray[1]."&lt;".$tempArray[0]."&gt;";
}
else
{
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID,$namefield FROM INTRANET_USER WHERE UserID = '".$li->SenderID."'";
    $temp = $li->returnArray($sql,2);
    list($mail_senderid,$mail_sendername) = $temp[0];
    $preset_message_whowrite = $mail_sendername;
}

if($CampusMailID=="" && $CampusMailReplyID!="")
{
	 # Get info from DB
     $namefield = getNameFieldWithClassNumberByLang("c.");
     $sql = "SELECT a.Message, b.UserID, $namefield, b.Subject
				FROM INTRANET_CAMPUSMAIL_REPLY as a
						LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON a.CampusMailID = b.CampusMailID
						LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
				WHERE a.CampusMailReplyID = '$CampusMailReplyID'";
     $temp = $li->returnArray($sql,6);
     list($replyMessage, $senderID, $receiverName, $mail_subj) = $temp[0];
     //$replyMessage = stripslashes($replyMessage);
     $replyMessage = $li->smartstripslashes($replyMessage);
     $preset_message = $replyMessage;
     $preset_message_whowrite = $receiverName;
}
else 
{
	# Message
	$preset_message = $li->Message;
	//$preset_message = stripcslashes($preset_message);
	$preset_message = $li->smartstripslashes($preset_message);
	
}
if ($use_html_editor)
{
	/*
	if($li->isHTML!=1){
		$preset_message = nl2br($preset_message);
	}
	*/
	if(!$li->isHTMLMessage($preset_message)){
		$preset_message = nl2br($preset_message);
	}	

/*  
	$preset_message = $preset_message_whowrite."$ui_text_wrote\n".
                      "<BLOCKQUOTE dir=ltr "."style=\"PADDING-RIGHT: 0px; PADDING-LEFT: 5px; MARGIN-LEFT: 5px; BORDER-LEFT: #000000 2px solid; MARGIN-RIGHT: 0px\"". " >".
                      $preset_message."</BLOCKQUOTE><br>";
*/

	if($action!="D"){
		if($preset_message != ""){
			$preset_message = "<BLOCKQUOTE dir='ltr' style='PADDING-RIGHT: 0px; PADDING-LEFT: 5px; MARGIN-LEFT: 5px; BORDER-LEFT: #000000 2px solid; MARGIN-RIGHT: 0px' >".$preset_message."</BLOCKQUOTE><br>";
		}else{
			$preset_message = "";
		}
	}
}
else
{
	//$preset_message = ">".str_replace("\n","\n>",$preset_message);
	$preset_message = str_replace("\n","\n>",$preset_message);
	$preset_message = intranet_htmlspecialchars($preset_message);
	$preset_message = str_replace("&amp;","&",$preset_message);
	//$preset_message = $preset_message_whowrite."$ui_text_wrote\n".$preset_message;
}

$preset_message = cleanHtmlJavascript($preset_message);

//$js_message = str_replace("\n","\\n",$preset_message);
//$js_message = str_replace("\n","\\n",$li->Message);

//$js_message = str_replace("\r","",$js_message);

# To
if($action=="RA"){
	$addrs = $li->SenderEmail!=""?$li->SenderEmail.", ".$li->ExternalTo:$li->ExternalTo;
	$addrs = $li->convertExternalEmailAddressComma($addrs);
	$js_sender = str_replace("\n","\\n",$addrs);
	$js_sender = str_replace("\r","",$addrs);
}
else{
	$js_sender = str_replace("\n","\\n",$li->SenderEmail);
	$js_sender = str_replace("\r","",$js_sender);
}

# CC
$js_cc = str_replace("\n","\\n",$li->ExternalCC);
$js_cc = str_replace("\r","",$js_cc);


# attachment
//echo "<p>$tmpFolder</p>";

//if( $_GET['mode'] != 'feedback' )
//{
	if($tmpFolder!="")
		$composeFolder2 = urldecode($tmpFolder);
	else 
	$composeFolder2= $li->Attachment;
	
	$path2 = "$file_path/file/mail/$composeFolder2";
	if($composeFolder2 !="")
	{
		$lo2 = new libfiletable("", $path2, 0, 0, "");
		$files2 = $lo2->files;
	}
//}

//echo "<p>attachment=".$li->Attachment."</p>";

# Subject
/*
if ($action == "R" || $action=="RA")  # Reply / Reply All
         $js_subject = "Re: ".$li->Subject;
if($action =="D")
         $js_subject = $li->Subject;
if($action=="F")
        $js_subject = "Fw: ".$li->Subject;
*/
?>
<?php
/*	if($li->MessageEncoding==""){
		if($intranet_session_language=="b5"||$intranet_session_language=="en")
			$encoding="BIG5";
		if($intranet_session_language=="gb")
			$encoding="GB2312";
	}
	else */
		$encoding=$li->MessageEncoding;
?>
<HTML>
<META http-equiv=Content-Type content='text/html; charset=<?=$encoding?>'>
<HEAD>
<SCRIPT LANGUAGE=Javascript>
//var message = "<?=$js_message?>";
//top.form1.plainMessage.value = message;
<?PHP 
/*
<?if ($js_sender != "" && ($action=="R" || $action=="RA")) { ?>
top.form1.ExternalTo.value = "<?=$js_sender?>";
<? } ?>
*/
?>

<?php 
/** External CC  
<? if($js_cc!="") {?>

	top.form1.ExternalCC.value = "<?=$js_cc?>";

<?}?>
**/
?>

<? if($js_subject!=""){?>
//top.form1.Subject.value ="<?=$js_subject?>";
<?}?>
</SCRIPT>
</HEAD>
<BODY>

<form name='BL_content_form'>
<? 

//if(strtolower($li->MessageEncoding)=="utf-8"){
if( /* $_GET['mode']!='feedback' && */($action=="F"||$action=="D")){?>
<span id="hidden_filename" style='visibility:hidden'></span>
<script language="javascript">
function format_filename(filename){
	return filename.replace(/&amp;/g,'&');
}
obj = parent.document.form1.elements["Attachment[]"];
obj_realname = parent.document.form1.elements["real_name[]"];
parent.checkOptionClear(obj);
parent.checkOptionClear(obj_realname);
<?php 
	if(sizeof($files2)>0)
	{
		while (list($key, $value) = each($files2)){
			$f_name = $files2[$key][0];
			echo "objFile= document.getElementById('hidden_filename');";
			echo "objFile.innerHTML='".str_replace("'","&#039;",$f_name)."';";
			echo "file_name = format_filename(objFile.innerHTML);";
		 	echo "parent.checkOptionAdd(obj, file_name+\" (".ceil($files2[$key][1]/1000)."Kb".")\", \"".urlencode($files2[$key][0])."\");\n";  
		 	echo "parent.checkOptionAdd(obj_realname, file_name, \"".urlencode($files2[$key][0])."\");\n";  
		 	echo "objFile.innerHTML='';";
	 	}
	}
?>
//parent.checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
parent.jCHECK_ATTACHMENT();
</script>
<?}
//}
?>

<?
/*
<textarea name='BL_prev_msg' id='BL_prev_msg'><?=$preset_message?></textarea>
<textarea name='BL_who_wrote' id='BL_who_wrote'><?=$preset_message_whowrite?></textarea>
<div name='BL_prev_msg' id='BL_prev_msg'><?=$preset_message?></div>
<div name='BL_who_wrote' id='BL_who_wrote'><?=$preset_message_whowrite?></div>
*/

/*
<div name='BL_prev_msg' id='BL_prev_msg'><?=intranet_undo_htmlspecialchars(stripslashes($preset_message));?></div>
<div name='BL_who_wrote' id='BL_who_wrote'><?=intranet_undo_htmlspecialchars(stripslashes($preset_message_whowrite));?></div>
*/
?>

<div name='BL_prev_msg' id='BL_prev_msg'><?=$preset_message;?></div>
<div name='BL_who_wrote' id='BL_who_wrote'><p></p><?=$preset_message_whowrite;?></div>

<? 
/*
<input type=hidden name=BL_prev_msg value="<?=intranet_htmlspecialchars($li->Message)?>"> 
<input type=hidden name=BL_prev_msg value="<?=$preset_message?>"> 
*/
?>
	<?php
	if($li->ExternalCC!="")
	{
		$ext_cc = $li->ExternalCC;
		$ext_cc = $li->convertExternalEmailAddressComma($ext_cc);
	?>
		<input type=text name='ext_cc' value='<?=$ext_cc?>'>
	<?php 
	}
	if($js_sender!="")
	{ ?>
		
		<input type=text name='ext_to' value='<?=$js_sender?>'>
	<?php
	}		
	?>
</form>
<script language='javascript'>
<?php
//if($_GET['mode']=='feedback' || $sys_custom['iMailComposeMailCompleteLoad']){
?>
if(window.parent.FCKeditorAPI) {
	window.parent.setContent(window.parent.FCKeditorAPI.GetInstance('Message'));
}
<?php
//}
?>
<?php 
	// add Exterenal CC 
	
	/*
	if($li->ExternalCC!="" && $action=="RA"  ){
		echo "top.form1.ExternalCC.value = document.content_form.ext_cc.value;\n";
	}
	if ($js_sender != "" && ($action=="R" || $action=="RA")) { 
		echo "top.form1.ExternalTo.value =document.content_form.ext_to.value;\n";
	}
	*/
?>
//parent.setContent();
</script>
</BODY>
</HTML>
<?php
intranet_closedb();
?>