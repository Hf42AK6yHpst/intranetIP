<?php
// Editing by 
/*
 * 2019-06-06 Carlos: Added CSRF token checking.
 */
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libcampusmail.php");
include_once("../../lang/lang.$intranet_session_language.php");

if(!verifyCsrfToken($csrf_token, $csrf_token_key)){
	header("Location: /");
	exit;
}

intranet_auth();
intranet_opendb();
auth_campusmail();

$fid = IntegerSafe($fid);

$lc = new libcampusmail();

$sql = "DELETE FROM INTRANET_CAMPUSMAIL_FOLDER WHERE FolderID = '$fid' AND OwnerID = '$UserID'";
$lc->db_db_query($sql);

$sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '$fid'";
$lc->db_db_query($sql);

$sql = "DELETE FROM INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT WHERE UserID='$UserID' AND FolderID='$fid'";
$lc->db_db_query($sql);

intranet_closedb();
header("Location: folder.php?msg=3");
?>