<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

## either called from Compose Page OR AddressBook > Internal Recipient Group
$caller = $AliasID==""?"compose":"addressbook";

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();


$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");

if ($file_content == "")
{
    $permitted = array(1,2,3);
}else{
	$content = explode("\n",$file_content);
	$content2[] = split(":",$content[0]);
    $content2[] = split(":",$content[1]);
    $content2[] = split(":",$content[3]);
	# Get row 1,2,4 only. Compatible with previous version
	if ($content2[0][0]==1) $permitted[] = 1;
	if ($content2[1][0]==1) $permitted[] = 2;
	if ($content2[2][0]==1) $permitted[] = 3;
}


$li = new libuser($UserID);

// echo "<p>Sender UserID=$UserID</p>";
# get toStaff, toStudent, toParent options
$sql ="
			SELECT 
				a.RecordType,a.Teaching,c.ClassLevelID 
			FROM 
				INTRANET_USER AS a 
			LEFT OUTER JOIN 
				INTRANET_CLASS AS b 
			ON
				(a.ClassName=b.ClassName) 
			LEFT OUTER JOIN 
				INTRANET_CLASSLEVEL AS c 
			ON 
				(b.ClassLevelID=c.ClassLevelID) 
			WHERE 
				a.UserID='$UserID'
		";
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_to_options = "	SELECT 
								ToStaffOption,ToStudentOption,ToParentOption 
							FROM 
								INTRANET_IMAIL_RECIPIENT_RESTRICTION 
							WHERE 
								TargetType='$usertype' AND Restricted=1
						";
if($usertype==1)
	$sql_to_options.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_to_options.=" AND ClassLevel='$class_level_id'";
$result_to_options = $li->returnArray($sql_to_options,3);
//echo "<p>$sql_to_options</p>";
list($toStaff,$toStudent,$toParent) = $result_to_options[0];

# end get toStaff, toStudent, toParent options

	
$x1  = ($CatID!="" && $CatID > 0) ? "<select name='CatID' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()' >\n" : "<select name='CatID' onChange='this.form.submit()' >\n";
$x1 .= "<option value='0' >--{$button_select}--</option>\n";

# Create Cat list
if (in_array(1,$permitted) && $toStaff!=-1)
{
    $x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
}
if (in_array(2,$permitted) && $toStudent!=-1)
{
    $x1 .= "<option value=2 ".(($CatID==2)?"SELECTED":"").">$i_identity_student</option>\n";
}
if (in_array(3,$permitted) && $toParent!=-1)
{
    $x1 .= "<option value=3 ".(($CatID==3)?"SELECTED":"").">$i_identity_parent</option>\n";
    if($toParent!=4)
    	$x1 .= "<option value=999 ".(($CatID==999)?"SELECTED":"").">$i_campusmail_select_parents_from_students</option>\n";
}
$x1 .= "</select>";


$name_field = getNameFieldWithClassNumberByLang("a.");
$direct_to_step3=false;
# Step 2

if($CatID!=""&&$CatID>0)
{
	
	$selectedCatID = $CatID;
	
	
	$fields = "DISTINCT a.GroupID, a.ClassName";
	$tables = "INTRANET_CLASS AS a";
	$conds  = "a.RecordStatus=1";
	$orderfield =" a.ClassName ";

	if($li->isParent()){
		$child_list = $li->getChildren();
		$child_list = implode(",",$child_list);
	}else if($li->isStudent()){
		$child_list = $UserID;
	}
		
	# if Parent / Select Parent From Students 
	if($selectedCatID==3 || $selectedCatID==999){
		switch($toParent){
			# parents in taught class
			case  1 :	//$tables.=",INTRANET_USERGROUP AS b";
						//$conds.=" AND b.UserID='$UserID' AND b.GroupID=a.GroupID";
						$t_sql =" SELECT DISTINCT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID='$UserID' ";
						$t_sql2 =" SELECT DISTINCT ClassID FROM INTRANET_SUBJECT_TEACHER WHERE UserID='$UserID' ";
						$t_result = $li->returnVector($t_sql);
						$t_result2 = $li->returnVector($t_sql2);
						for($x=0;$x<sizeof($t_result2);$x++){
							if(!in_array($t_result2[$x],$t_result))
								$t_result[] = $t_result2[$x];
						}
						$t_class_list = implode(",",$t_result);  // the class list taught by the teacher
						if($t_class_list=="") $t_class_list = "''";
						$conds.= " AND a.ClassID IN ($t_class_list) ";
						break;
			# parent in same class level
			case  2 : 	$tables.=", INTRANET_CLASS AS b,INTRANET_USER AS c "; 
						$conds.=" AND c.UserID IN ($child_list) AND c.ClassName=b.ClassName AND b.ClassLevelID=a.ClassLevelID ";
						break;
			# parent in same class
			case  3 : 	$tables.=", INTRANET_USER AS b "; 
						$conds.=" AND b.UserID IN($child_list) AND b.ClassName=a.ClassName ";
						break;
			# student's own parent
			case  4 : $sql="SELECT DISTINCT a.UserID,$name_field FROM INTRANET_USER AS a,INTRANET_PARENTRELATION AS b WHERE b.StudentID='$UserID' AND a.UserID=b.ParentID  ORDER BY a.EnglishName";
			          $direct_to_step3=true;
					  break;
			# all parent
			default : break;
		}
	}
	else if($selectedCatID==2){
		switch($toStudent){
			# students in taught class
			case  1 :	
						$t_sql =" SELECT DISTINCT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID='$UserID' ";
						$t_sql2 =" SELECT DISTINCT ClassID FROM INTRANET_SUBJECT_TEACHER WHERE UserID='$UserID' ";
						$t_result = $li->returnVector($t_sql);
						$t_result2 = $li->returnVector($t_sql2);
						for($x=0;$x<sizeof($t_result2);$x++){
							if(!in_array($t_result2[$x],$t_result))
								$t_result[] = $t_result2[$x];
						}
						$t_class_list = implode(",",$t_result);  // the class list taught by the teacher
						//$tables .=" LEFT OUTER JOIN INTRANET_CLASSTEACHER AS b ON (b.ClassID=d.ClassID) LEFT OUTER JOIN INTRANET_SUBJECT_TEACHER AS c ON (c.ClassID=d.ClassID) ,INTRANET_CLASS AS d,INTRANET_USER AS e ";
						//$conds .= " AND a.UserID IN (e.UserID='$UserID' AND e.ClassName = d.ClassName AND b.ClassID=d.ClassID AND c.ClassID=d.ClassID ";
						if($t_class_list=="") $t_class_list = "''";
						$conds.= " AND a.ClassID IN ($t_class_list) ";
						//$tables.=",INTRANET_USERGROUP AS c";
						//$conds.=" AND c.UserID='$UserID' AND c.GroupID=a.GroupID";
						break;
			# students in same class level
			case  2 : 	
						$tables.=", INTRANET_CLASS AS b,INTRANET_USER AS c "; 
						$conds.=" AND c.UserID IN($child_list) AND c.ClassName=b.ClassName AND b.ClassLevelID=a.ClassLevelID ";
						break;
			# students in same class
			case  3 : 	
						$tables.=", INTRANET_USER AS b "; 
						$conds.=" AND b.UserID IN($child_list) AND b.ClassName = a.ClassName ";
						break;
			# parent's own children
			case  4 : 	$sql="SELECT DISTINCT a.UserID,$name_field FROM INTRANET_USER AS a,INTRANET_PARENTRELATION AS b WHERE b.ParentID='$UserID' AND a.UserID=b.StudentID  ORDER BY a.ClassName,a.ClassNumber";
						$direct_to_step3=true; 
					  	break;
			# all students
			default : break;
		}
		
	}
	if(!$direct_to_step3)
	{
			$sql="SELECT $fields FROM $tables WHERE $conds  ORDER BY $orderfield";
//			echo "<p>$sql</p>";
			$x2  = "<select name='ChooseGroupID[]' size='10' multiple>\n";
		    $x2 .= "<option value='' >--{$button_select}--</option>\n";
			
			$row = $li->returnArray($sql,2);
			for($i=0; $i<sizeof($row); $i++){
		          $GroupCatID = $row[$i][0];
		          $GroupCatName = $row[$i][1];
		          $x2 .= "<option value=$GroupCatID";
		          for($j=0; $j<sizeof($ChooseGroupID); $j++){
		          		$x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
		          }
		          $x2 .= ">$GroupCatName</option>\n";
		    }
		    $x2 .= "</option>\n";
		    $x2 .= "</select>\n";
	}else{
			$row = $li->returnArray($sql,2);
//			echo "<p>$sql</p>";

			 $x3  = "<select name='ChooseUserID[]' size='10' multiple>\n";
			 $x3 .= "<option value='' >--{$button_select}--</option>\n";
		     for($i=0; $i<sizeof($row); $i++)
			     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
		     $x3 .= "</select>\n";
	}
}
else{
	$selectedCatID = 0-$CatID;
	//$name_field = getNameFieldByLang("a.");
	$fields = "DISTINCT a.UserID,$name_field";
	$tables = "INTRANET_USER AS a";
	$conds  = "a.RecordStatus=1";
	$orderfield=" a.EnglishName ";
	
	if($li->isParent()){
			$child_list = $li->getChildren();
			$child_list = implode(",",$child_list);
	}else if($li->isStudent()){
			$child_list = $UserID;
	}
	
	# Staff selected
	if($selectedCatID==1){
		 $conds .=" AND a.RecordType=1";
		 if($toStaff==1){
			switch($usertype){
				# sender is students
				case 2 : 	
							//$tables .=",INTRANET_USERGROUP AS b, INTRANET_USERGROUP AS c,INTRANET_CLASS AS d ";
							//$conds .=" AND a.UserID=c.UserID AND b.GroupID=c.GroupID AND b.UserID='$UserID' AND b.GroupID=d.GroupID";
							$t_sql = "SELECT b.ClassID FROM INTRANET_CLASS AS b, INTRANET_USER AS a WHERE a.UserID='$UserID' AND a.ClassName=b.ClassName ";
							$t_result = $li->returnVector($t_sql);
							$t_class_id = $t_result[0];
							$t_sql =" SELECT DISTINCT UserID FROM INTRANET_CLASSTEACHER WHERE ClassID='$t_class_id'";
							$t_sql2 =" SELECT DISTINCT UserID FROM INTRANET_SUBJECT_TEACHER WHERE ClassID='$t_class_id'";
							$t_result = $li->returnVector($t_sql);
							$t_result2 = $li->returnVector($t_sql2);
							for($x=0;$x<sizeof($t_result2);$x++){
								if(!in_array($t_result2[$x],$t_result))
									$t_result[] = $t_result2[$x];
							}
							$t_teacher_list = implode(",",$t_result);
							//$tables .=" LEFT OUTER JOIN INTRANET_CLASSTEACHER AS b ON (b.ClassID=d.ClassID) LEFT OUTER JOIN INTRANET_SUBJECT_TEACHER AS c ON (c.ClassID=d.ClassID) ,INTRANET_CLASS AS d,INTRANET_USER AS e ";
							//$conds .= " AND a.UserID IN (e.UserID='$UserID' AND e.ClassName = d.ClassName AND b.ClassID=d.ClassID AND c.ClassID=d.ClassID ";
							if($t_teacher_list=="") $t_teacher_list = "''";
							$conds .= " AND a.UserID IN ($t_teacher_list) ";
							break;
				
				# sender is parents
				case 3 : 	//$tables .=",INTRANET_CLASS AS b,INTRANET_USERGROUP AS c,INTRANET_USER AS d ";
							//$conds .=" AND d.UserID IN($child_list) AND d.ClassName=b.ClassName and b.GroupID=c.GroupID AND a.UserID=c.UserID";
							if($child_list=="") break;
							$t_sql = "SELECT b.ClassID FROM INTRANET_CLASS AS b, INTRANET_USER AS a WHERE a.UserID IN ($child_list) AND a.ClassName=b.ClassName ";
							$t_result = $li->returnVector($t_sql);
							$t_class_id = implode(",",$t_result);
							$t_sql =" SELECT DISTINCT UserID FROM INTRANET_CLASSTEACHER WHERE ClassID IN ($t_class_id) ";
							$t_sql2 =" SELECT DISTINCT UserID FROM INTRANET_SUBJECT_TEACHER WHERE ClassID IN ($t_class_id)";

							//$t_class_id = $t_result[0];
							//$t_sql =" SELECT DISTINCT UserID FROM INTRANET_CLASSTEACHER WHERE ClassID='$t_class_id'";
							//$t_sql2 =" SELECT DISTINCT UserID FROM INTRANET_SUBJECT_TEACHER WHERE ClassID='$t_class_id'";
							$t_result = $li->returnVector($t_sql);
							$t_result2 = $li->returnVector($t_sql2);
							for($x=0;$x<sizeof($t_result2);$x++){
								if(!in_array($t_result2[$x],$t_result))
									$t_result[] = $t_result2[$x];
							}
							$t_teacher_list = implode(",",$t_result);
							if($t_teacher_list!="")
								$conds .= " AND a.UserID IN ($t_teacher_list) ";
							break;
				default: break;
			}
		 }
	 	$sql="SELECT $fields FROM $tables WHERE $conds  ORDER BY $orderfield";
	 	$row = $li->returnArray($sql,2);
//		echo "<p>$sql</p>";

		 $x3  = "<select name='ChooseUserID[]' size='10' multiple>\n";		 
		 $x3 .= "<option value='' >--{$button_select}--</option>\n";
	     for($i=0; $i<sizeof($row); $i++)
		     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	     $x3 .= "</select>\n";
	}

}
if(isset($ChooseGroupID)){
	$group_id = implode(",",$ChooseGroupID);
	//$name_field = getNameFieldByLang("a.");
	$fields = "DISTINCT a.UserID, $name_field";
	$tables = "INTRANET_USER AS a";
	$conds ="a.RecordStatus=1";
	$orderfield="";
	
	# show student list
	if($CatID==2 || $CatID==999){
		$tables.=",INTRANET_CLASS AS b";
		$conds.=" AND a.RecordType=2 AND b.GroupID IN($group_id) AND a.ClassName=b.ClassName";
		$orderfield=" ORDER BY a.ClassName,a.ClassNumber ";
	}
	# show parent list
	if($CatID==3){
		$tables.=",INTRANET_USER AS b,INTRANET_PARENTRELATION AS c, INTRANET_CLASS AS d";
		$conds.=" AND a.RecordType=3 AND d.GroupID IN($group_id) AND b.ClassName=d.ClassName AND b.UserID=c.StudentID AND a.UserID=c.ParentID";
		$orderfield=" ORDER BY a.EnglishName ";
	}
	$sql ="SELECT $fields FROM $tables WHERE $conds $orderfield";
	$row = $li->returnArray($sql,2);
//	echo "<p>$sql</p>";

		 $x3  = "<select name='ChooseUserID[]' size='10' multiple>\n";		 
		 $x3 .= "<option value='' >--{$button_select}--</option>\n";
	     for($i=0; $i<sizeof($row); $i++)
		     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	     $x3 .= "</select>\n";
	
}

$step1 = getStepIcons(3,1,$i_SelectMemberSteps);
$step2 = getStepIcons(3,2,$i_SelectMemberSteps);
$step3 = getStepIcons(3,3,$i_SelectMemberSteps);
$suf_parent = $i_general_targetParent;

if ($fieldname == "Recipient[]")
{
	$ExtraJS = " par.displayTable('internalToTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalToRemoveBtnDiv','block'); \n";	
}
else if ($fieldname == "InternalCC[]")
{
	$ExtraJS = " par.displayTable('internalCCTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalCCRemoveBtnDiv','block'); \n";	
}
else if ($fieldname == "InternalBCC[]")
{
	$ExtraJS = " par.displayTable('internalBCCTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalBCCRemoveBtnDiv','block'); \n";	
}	

?>

<script language="javascript">
function AddOptions(obj, type){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
     if (type==0)    // Normal
         x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
     else
         x = (obj.name == "ChooseGroupID[]") ? "Q" : "P";

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;
     while(i!=-1){
           if (type==0)
               addtext = obj.options[i].text;
           else
               addtext = obj.options[i].text + "<?=$suf_parent?>";

          par.checkOptionAdd(parObj, addtext, x + obj.options[i].value);
          
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     
     <?=$ExtraJS?>
}

function checkOptionNone(obj){
	if(obj==null)return;
       for(i=0; i<obj.length; i++){
                obj.options[i].selected = false;
        }
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

// use in AddressBook Internal Recipient Group
function addRecipient(obj,type){
	 if (type==0)    // Normal
         x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
     else
         x = (obj.name == "ChooseGroupID[]") ? "Q" : "P";
     
     ids ='';
     delim='';
     for(i=0;i<obj.options.length;i++){
	     if(obj.options[i].selected){
	     	ids+=delim+x+obj.options[i].value;
	     	delim=',';
	     }
	 }
	 document.form1.InternalRecipientID.value = ids;
	 document.form1.action='addressbook_update.php';
	 document.form1.submit();
}
</script>

<form name="form1" action="index.php" method="post" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<br />
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td>

		<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
		<tr>
 			<tr>
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_GroupCategorySettings?>:</span>
			</td>
			<td >					
			<?=$x1?>
			</td>
		</tr>
		
		<?php 
		if($CatID!="" && $CatID > 0 && !$direct_to_step3) 
		{ 
		?>		
		<tr> 
			<td height="1" colspan="2" class="dotline" valign="middle" >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		
		<tr> 			
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$iDiscipline['class']?>:</span>
			</td>
			<td >					
			<table cellpadding="0" cellspacing="0" >
			<tr>
				<td><?=$x2?></td>
				<td style="vertical-align:bottom">
				<table cellpadding="0" cellspacing="6" >				
				<?php 
				if($CatID!=3 && $CatID!=999)
				{
				?>
				<tr>
					<td>
					<?php if($caller=="compose"){ ### called from Compose page
							echo $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],0)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
						  }else{ ### called from AddressBook
								echo $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseGroupID[]'],0)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
						  }
					?>
					</td>
				</tr>	
				<?php 
				}
				?>
				<tr>
					<td>
					<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td>
				</tr>	
				<?php 
				if($CatID==3 || $CatID==999)
				{
				?>
				<tr>
					<td>
					<?php if($caller=="compose"){
							echo $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],1)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."";
						}else{
							echo $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseGroupID[]'],1)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."";
						}
					?>
					</td>
				</tr>	
				<?php 
				} 
				if(!$sys_custom['Mail_NoSelectAllButton']) 
				{ 
				?>
				<tr>
					<td>
					<?= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseGroupID[]']); return false;","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ") ?>
					</td>
				</tr>					
				<?php 
				} 
				?>
				</table>	
				</td>
			</tr>
			</table>			
			</td>
		</tr>
		

		
		<?php 
		} 
		
		if($CatID!=""&&(isset($ChooseGroupID) || $CatID<0 ||$direct_to_step3)) 
		{ 
		?>
		<tr> 
			<td height="1" colspan="2" class="dotline">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>

		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
				
		<tr >
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_CampusMail_New_AddressBook_ByUser?>:</span>
			</td>
			<td >					
			<table border="0" cellpadding="0" cellspacing="0" align="left">		
			<tr >
				<td >				
				<?=$x3?>
				</td>
				<td valign="bottom" >
				<table cellpadding="0" cellspacing="6" >
				<?php
				if($CatID==999)
				{ 
				?>
				<tr >
					<td >													
					<?php if($caller=="compose")
						echo $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'],1)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."";					
						else echo $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseUserID[]'],1)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."";					
				 ?>
					</td >
				</tr >	
				<?php 
				} else {
				?>
				<tr >
					<td ><?php if($caller=="compose")																		
						echo  $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'],0)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
						else  echo  $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseUserID[]'],0)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;

						?>
					</td >
				</tr >	
				<?php 
				} 
				?>
				<?php 
				if(!$sys_custom['Mail_NoSelectAllButton']) 
				{ 
				?>
				<tr >
					<td >																		
					<?= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseUserID[]']); return false;","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ") ?>
					</td >
				</tr >	
				<?php 
				} 
				?>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<?php 
		} 
		?>
		</table>		
		</td>
	</tr>
	
	<tr>
		<td>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	
	</td>
</tr>
</table>
<input type="hidden" name=InternalRecipientID value="">
<input type="hidden" name=AliasID value="<?=$AliasID?>">
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>