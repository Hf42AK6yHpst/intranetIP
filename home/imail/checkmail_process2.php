<?php
// using : 
/**************************************** Changes *******************************************************
 * 2017-09-27 (Carlos): Delete new mail counter for inbox and spam box to recount. 
 * 2017-06-23 (Carlos): Use find command to clean internet emails raw source files that are older than 3 months ago.
 * 2017-03-06 (Carlos): Update new mail counter.
 * 2016-04-28 (Carlos): Improved to prevent repeatedly insert same email into db in case of fail to remove email from mail server. 
 * 2015-12-17 (Carlos): Fine tuned white list checking method.
 * 2015-03-17 (Carlos): Check if AVAS anti-spam system is on, then check spam score setting.
 * 2015-02-02 (Carlos): Call osapi $lwebmail->moveMails() to move Junk emails to INBOX before downloading emails
 * 2015-01-22 (Carlos): Cater app application - use parameter app=1 to identify using app
 * 2014-01-07 (Carlos): Remove mail by UID after downloaded
 * 2013-07-11 (Carlos): Redirect to portal if using iMail plus, prevent grab emails to DB
 * 2012-09-17 (Carlos): added libwebmail->SaveRawBody() to save down complete mail source for debug tracing
 * 2012-06-29 (Carlos): - set memory_limit to 300MB, default 100MB may not enough for mails with large attachments
 * 						- modified the download mails from mail server procedure to get 
 * 						  certain amount of mails per round instead of get all mails at the same time. 
 ********************************************************************************************************/
ini_set("memory_limit","300M");
$PATH_WRT_ROOT = "../../"; 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libcampusquota.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/libinterface.php");

$from_app = $_REQUEST['app'] == 1 && $UserID!='';
if(!$from_app){ // skip user authentication
	intranet_auth();
}
intranet_opendb();

if($plugin['imail_gamma']) {
	intranet_closedb();
	header("Location: /");
	exit;
}

if($from_app){
	include_once("../../includes/libpwm.php");
	$lpwm = new libpwm();
	$userIdToPwd = $lpwm->getData(array($UserID));
}

$linterface = new interface_html();

$lwebmail = new libwebmail();
$lc = new libcampusmail();
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
{
    $noWebmail = false;
}
else
{
    $noWebmail = true;
}

if (!$noWebmail )
{
	
	 # Retrieve Spam control level
     $bypass_spam_check = $lwebmail->readSpamCheck();
     $use_avas = $bypass_spam_check == "N";
     if($bypass_spam_check == "N"){
	     $custom_spam_control = $lwebmail->retriveSpamControlSetting();
	     if($custom_spam_control == 1){
		     $spam_control_score = $lwebmail->readSpamScore();
	     }
	     else
	     {
		     $spam_control_score = $lwebmail->readSpamScore();
	     }
     }
     ######
     
	# Grab Internet E-Mail
	$lc_temp = new libcampusquota($UserID);
	$total_quota = $lc_temp->returnQuota();
	$used_quota = $lc_temp->returnUsedQuota();
	$left = $total_quota*1024 - $used_quota;
	if ($left > 0)
	{
         include_once("../../includes/libuser.php");
         include_once("../../includes/libfiletable.php");
         $lu = new libuser($UserID);
         if($from_app){
         	$inbox = $lwebmail->openInbox($lu->UserLogin,$userIdToPwd[$UserID]);
         }else{
         	$inbox = $lwebmail->openInbox($lu->UserLogin,$lu->UserPassword);
         }
		// DEBUG , this block prints out the mail content received from server.
		/*
		if($UserID==1663)	{
			$mail_entry = $lwebmail->checkMail2($left);
			print_r($mail_entry);
			for ($i=0; $i<sizeof($mail_entry); $i++)
			{
				list($mail_id, $subject, $message, $curr_attach_size, $fromaddress, $toaddress,
					$ccaddress, $dateReceived, $attach_parts, $embed_cids,
					$message_charset , $is_html_message
				) = $mail_entry[$i];
				print_r($attach_parts);
				if (sizeof($attach_parts) > 0)
				{
					$is_attach = 1;
					$curr_attach_size = ceil($curr_attach_size/1000);
					//$attachment_path = $lwebmail->getNewAttachmentPath();
					//mkdir($intranet_root."/file/mail/".$attachment_path);
				}
				else
				{
					$is_attach = 0;
					//$curr_attach_size = 0;
					//$attachment_path = "";
				}
				if($is_attach) echo "is attache";
			}
			exit();
			//echo "$UserID";
			$mailCount = $lwebmail->checkMailCount();
			// if($mailCount<=0) exit();
			$overview = imap_fetch_overview($lwebmail->inbox, "1:".$mailCount);
			$num = sizeof($overview);
			//print_r($overview);
			for ($i=0; $i < sizeof($overview) && $i < $num; $i++)
			{
				$mail_msgno = $overview[$i]->msgno;
				//if($mail_msgno!=4) continue;
				$structure = imap_fetchstructure($lwebmail->inbox, $mail_msgno);
				echo "msgno = $mail_msgno\n";
				print_r($structure);
				echo "\n";
				$lwebmail->parse_message($structure);
				$msg = $lwebmail->retrieve_message($lwebmail->inbox, $mail_msgno, $lwebmail->parsed_part);
				//print_r($structure);
				//echo "Structure=".print_r($structure)."<Br>";
				echo "<br>encoding=".$lwebmail->message_charset."\n";
				//echo "msg=$msg<Br>";
				echo "\n\n-------------------------------------\n\n";
			}
			$lwebmail->close();
			exit();
		}
		*/
		// END DEBUG
      	if($webmail_info['bl_spam']){
      		$lwebmail->moveMails($lu->UserLogin); // move junk emails to inbox to cater mailfilter bug of filtering spam mails to INBOX.Junk
      	}
      	$max_per_turn = isset($sys_custom['iMailGetMailPerRound'])?$sys_custom['iMailGetMailPerRound']:2;
      	if($inbox /*&& $lwebmail->goToFolder($lwebmail->getInboxName())*/)
      	{
      		$imail_received = 0;
      		$newMailCount = $lwebmail->checkMailCount();
      		$imail_extotal = $newMailCount;
		 	$mailCount = min($newMailCount, $max_per_turn);
		 	$all_mail_uid_ary = array();
		 	while($mailCount > 0 && $left > 0)
		 	{
				//$left = number_format($left/1024,1);
				$mail_entry = $lwebmail->checkMail2($left,$mailCount);
				//debug_r($mail_entry);
				//die;
				if($sys_custom['iMail_MailRule']){
					$sql = "SELECT 
							RuleID, FromEmail, ToEmail, Subject, Message, HasAttachment, 
							ActionMarkAsRead, ActionDelete, ActionMoveToFolder, ActionForwardTo
					 FROM
							INTRANET_IMAIL_MAIL_RULES 
					 WHERE 
							UserID = '$UserID'";
					$arr_result = $lc_temp->returnArray($sql,9);
		
					if(sizeof($arr_result>0)){
						for($i=0; $i<sizeof($arr_result); $i++){
							list($rule_id, $from_email, $to_email, $subject, $message, $has_attachment, $action_mark_as_read, $action_delete, $action_move_to_folder, $action_forward) = $arr_result[$i];
							$condition_cnt = 0;
							$arr_rule[] = $rule_id;
							$arr_rule_condition[$rule_id]['From'] = $from_email;
							$arr_rule_condition[$rule_id]['To'] = $to_email;
							$arr_rule_condition[$rule_id]['Subject'] = $subject;
							$arr_rule_condition[$rule_id]['Message'] = $message;
							$arr_rule_condition[$rule_id]['HasAttachment'] = $has_attachment;
		
							if($from_email != ""){
								$condition_cnt++;
							}
							if($to_email != ""){
								$condition_cnt++;
							}
							if($subject != ""){
								$condition_cnt++;
							}
							if($message != ""){
								$condition_cnt++;
							}
							if($has_attachment != 0){
								$condition_cnt++;
							}
							$arr_rule_condition[$rule_id]['ConditionCount'] = $condition_cnt;
							$arr_rule_action[$rule_id]['MarkAsRead'] = $action_mark_as_read;
							$arr_rule_action[$rule_id]['Delete'] = $action_delete;
							$arr_rule_action[$rule_id]['MoveToFolder'] = $action_move_to_folder;
							$arr_rule_action[$rule_id]['Forward'] = $action_forward;
						}
					}
				}
				
				
		         $mail_id_to_remove = array();
		         //$lc = new libcampusmail();
		
		         # Put mails into INTRANET_CAMPUSMAIL , Put one ONLY
		         #$mail_id = $mail_entry[0];
		         #if ($mail_id != "")
		         if (sizeof($mail_entry)!=0)
		         {
			         #print_r($mail_entry);
					 // debug_r($mail_entry);
					 #exit;
		              for ($i=0; $i<sizeof($mail_entry); $i++)
		              {
		                   list($mail_id, $subject, $message, $curr_attach_size, $fromaddress, $toaddress,
		                        $ccaddress, $dateReceived, $attach_parts, $embed_cids,
		                        $message_charset , $is_html_message,
		                        $spam_flag, $spam_score, $spam_level, $spam_status, $rawmessage, $messagecharset,
								$text_header, $raw_mail_source , $mail_uid 
		                        ) = $mail_entry[$i];
		                    //debug_r($mail_entry[$i]);
		                   /*
		                   if ($subject=="" && $message=="")
		                   {
		                       # Remove the email if no content
		                       $imail_received++;
		                       $mail_id_to_remove[] = $mail_id;
		                       continue;
		                   }
		                   */
		                   
		                   // prevent repeatedly insert email to db in case of fail to remove downloaded email from mail server
		                   if(in_array($mail_uid, $all_mail_uid_ary)) continue; 
		                   $all_mail_uid_ary[] = $mail_uid;
		                   
		                   	## Check if there any double quote at the start and the end of the from address
		                   	if( (substr(ltrim($fromaddress),0,1) === "\"") )
		               		{
		               			$fromaddress = substr($fromaddress,1,strlen($fromaddress));
		               			$pos = strpos($fromaddress,"\"");
		               			
		               			$tmp_str1 = substr($fromaddress,0,$pos);
		               			$tmp_str2 = substr($fromaddress,$pos+1,strlen($fromaddress));
		               			$fromaddress = $tmp_str1.$tmp_str2;
		               		}
		               	                  
		                   if($sys_custom['iMail_MailRule']){
			                   if(sizeof($arr_rule)>0){
				                   foreach($arr_rule as $rule_id){
					                   $cnt = 0;
					                   if($arr_rule_condition[$rule_id]['From'] != ""){
						                   if(strpos($fromaddress,$arr_rule_condition[$rule_id]['From'])){
							                   $cnt++;
						                   }
					                   }
					                   if($arr_rule_condition[$rule_id]['To'] != ""){
						                   if(strpos($toaddress,$arr_rule_condition[$rule_id]['To'])){
							                   $cnt++;
						                   }
					                   }
					                   if($arr_rule_condition[$rule_id]['Subject'] != ""){
						                   if(strpos($subject,$arr_rule_condition[$rule_id]['Subject'])){
							                   $cnt++;
						                   }
					                   }
					                   if($arr_rule_condition[$rule_id]['Message'] != ""){
						                   if(strpos($message,$arr_rule_condition[$rule_id]['Message'])){
							                   $cnt++;
						                   }
					                   }
					                   if($arr_rule_condition[$rule_id]['HasAttachment'] != ""){
						                   if($attachment_path != ""){
							                   $cnt++;
						                   }
					                   }
					                   $arr_rule_matched[$rule_id]['count'] = $cnt;
				                   }
			                   }
		                   }
		                   
		                   if ($subject == "")
		                   {
		                       $subject = "[Untitled]";
		                   }
		                   $message_charset = strtolower($message_charset);
		
		                   $message = $lc->convert_image_embeded_message($message, $embed_cids);
		                   $subject = $lc->convertBadWords($subject);
		                   $message = $lc->convertBadWords($message);
		                   
		                   $subject = addslashes($subject);
		                   $message = addslashes($message);
						   // $rawmessage = addslashes($rawmessage); #handled by mysql_real_escape_string
		                   $fromaddress = addslashes($fromaddress);
		                   $toaddress = addslashes($toaddress);
		                   $ccaddress = addslashes($ccaddress);
		
		                   if (sizeof($attach_parts) > 0)
		                   {
		                       $is_attach = 1;
		                       $curr_attach_size = ceil($curr_attach_size/1024);
		                       $attachment_path = $lwebmail->getNewAttachmentPath();
		                       mkdir($intranet_root."/file/mail/".$attachment_path);
		                   }
		                   else
		                   {
		                       $is_attach = 0;
		                       $curr_attach_size = 0;
		                       $attachment_path = "";
		                   }
		                  
		                   ## Check spam and put to folder - New version (also consider for the white list)
		                   if ($webmail_info['bl_spam'] && $use_avas) 
		                   {
			                   $arr_whiteList = $lwebmail->readWhiteList();
			                   if(is_array($arr_whiteList)>0){
				                   $start = strpos($fromaddress, "<");
				                   $end = strpos($fromaddress, ">");
				                   if($start !== false && $end !== false)
								   {
								   		$final_senderaddress = substr($fromaddress, $start+1, $end-$start-1);
								   }else{
								   		$final_senderaddress = $fromaddress;
								   }
				                   $whitelist_cnt = 0;
				                   foreach($arr_whiteList as $valid_address){
					                   if($valid_address == $final_senderaddress){
						                   $whitelist_cnt = 1;
					                   }else{
						                   $tmp_address = substr($final_senderaddress,strpos($final_senderaddress,"@"));
						                   if($tmp_address == $valid_address){
							                   $whitelist_cnt = 1;
						                   }else{
							                   
						                   }
					                   }
				                   }
				                   if($whitelist_cnt == 1){
					                   $default_folder = 2;
					                   $spam_fields = "";
					                   $spam_values = "";
				                   }else{
					                   $default_folder = ($spam_score>$spam_control_score?-2:2);
					                   $spam_fields = ",SpamFlag, SpamScore, SpamLevel, SpamStatus";
					                   $spam_values = ",'$spam_flag','$spam_score','$spam_level','$spam_status'";
				                   }
			                   }else{
				                   $default_folder = ($spam_score>$spam_control_score?-2:2);
				                   $spam_fields = ",SpamFlag, SpamScore, SpamLevel, SpamStatus";
				                   $spam_values = ",'$spam_flag','$spam_score','$spam_level','$spam_status'";
			                   }
		                   }
		                   else
		                   {
		                       $default_folder = 2;     # Inbox
		                       $spam_fields = "";
		                       $spam_values = "";
		                   }
		                   
		                   # Move the Black list email to SPAM folder if BL_SPAM = true
		                   if(strpos(strtoupper($subject),"[SPAM]")>0){
			                   if ($webmail_info['bl_spam'] && $use_avas){
				                   $default_folder = -2;
			                   }
		                   }
		                   
		                   if($sys_custom['iMail_MailRule']){
			                   if(sizeof($arr_rule)>0){
				                   foreach($arr_rule as $rule_id){
					                   if($arr_rule_matched[$rule_id]['count'] == $arr_rule_condition[$rule_id]['ConditionCount']){
						                   //echo "<BR>Match: ".$rule_id."<BR>";
						                   
						                   if($arr_rule_action[$rule_id]['MarkAsRead'] == 1){
							                   //echo "Mark as read";
							                   $record_status = " 1 ";
						                   }
						                   if($arr_rule_action[$rule_id]['Delete'] == 1){
							                   //echo "Delete";
							                   $mark_as_delete_field = ",Deleted";
							                   $mark_as_delete_value = " ,1 ";
							                   $record_status = 'NULL';
						                   }
						                   if($arr_rule_action[$rule_id]['MoveToFolder'] > 0){
							                   $default_folder = $arr_rule_action[$rule_id]['MoveToFolder'];
						                   }
						                   if($arr_rule_action[$rule_id]['Forward'] != ""){
							                   //echo "Forward to :".$arr_rule_action[$rule_id]['Forward'];
							                   $from = $fromaddress;
							                   $recever_to = $toaddress;
							                   $lwebmail->sendMail($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$attachment_path,$IsImportant="",$mail_return_path="",$reply_address="");
						                   }
					                   }
				                   }
			                   }
		                                   
			                   if($record_status == ""){
				                   $record_status = 'NULL';
			                   }
			                   
								$sql = "INSERT INTO INTRANET_CAMPUSMAIL (
									   UserID,UserFolderID,SenderEmail,RecipientID,InternalCC,ExternalTo,ExternalCC,
									   Subject,Message,Attachment,IsAttachment,AttachmentSize,IsImportant,
									   IsNotification,MessageEncoding,isHTML,
									   MailType,RecordType,RecordStatus,DateInput,DateModified,DateInFolder
									   $spam_fields $mark_as_delete_field
									   )
									   VALUES
									   ('$UserID','$default_folder','$fromaddress','','','$toaddress','$ccaddress',
									   '$subject','$message','$attachment_path','$is_attach','$curr_attach_size',0,
									   0,'$message_charset', '$is_html_message',
									   2,2,'$record_status','$dateReceived','$dateReceived',now()
									   $spam_values $mark_as_delete_value
									   )";
							}else{
								$sql = "INSERT INTO INTRANET_CAMPUSMAIL (
									   UserID,UserFolderID,SenderEmail,RecipientID,InternalCC,ExternalTo,ExternalCC,
									   Subject,Message,Attachment,IsAttachment,AttachmentSize,IsImportant,
									   IsNotification,MessageEncoding,isHTML,
									   MailType,RecordType,RecordStatus,DateInput,DateModified,DateInFolder
									   $spam_fields
									   )
									   VALUES
									   ('$UserID','$default_folder','$fromaddress','','','$toaddress','$ccaddress',
									   '$subject','$message','$attachment_path','$is_attach','$curr_attach_size',0,
									   0,'$message_charset', '$is_html_message',
									   2,2,NULL,'$dateReceived','$dateReceived',now()
									   $spam_values
									   )";
							}
							
							$lwebmail->iMail_insertDB_debug_log($iMail_insertDB_debug,$sql);
							
							$lc_temp->db_db_query($sql);
							$temp_campusmail_id = $lc_temp->db_insert_id();
							$headerfilepath = $lwebmail->SaveOriginal($temp_campusmail_id, 'header', $text_header);
							$messagefilepath = $lwebmail->SaveOriginal($temp_campusmail_id, 'body', $rawmessage);
							$lwebmail->SaveRawBody($temp_campusmail_id,$raw_mail_source);
							
							$RawMessageSql = "INSERT INTO INTRANET_RAWCAMPUSMAIL (
											 CampusMailID, UserID, HeaderFilePath, MessageFilePath, MessageEncoding, DateInput)
											 VALUES 
											 ('$temp_campusmail_id', '$UserID', '$headerfilepath', '$messagefilepath', '$messagecharset', '$dateReceived')";
							$lc_temp->db_db_query($RawMessageSql);
									
							# Change Attachment Scheme
							if ($is_attach == 1)
							{
		                       $temp_sql_values = "";
		                       $delim = "";
		                       $total_filesize = 0;
		                       for($k=0; $k<sizeof($attach_parts); $k++)
		                       {
		                           $attach_entry = $attach_parts[$k];
								   
								   $attach_filename = ($attach_entry['filename'] == '') ? 'UnknownFile' : $attach_entry['filename'];
		                           $target_file = $intranet_root."/file/mail/".$attachment_path."/".$attach_filename;
		                           write_file_content($attach_entry['content'],$target_file);
								   $fs_filesSize = ceil(filesize($target_file) / 1024);
		                           
		                           $is_embed = ($attach_entry['cid']!=""?1:0);
		                           $temp_filename = htmlspecialchars(addslashes($lc_temp->Get_Safe_Sql_Query($attach_filename)));
		                           //$temp_filename = intranet_htmlspecialchars($attach_filename);
		                           
		                           $temp_filesize = ceil($attach_entry['size']/1024);
		                           $total_filesize = $total_filesize + $temp_filesize;
		                           
								   $temp_sql_values .= "$delim($temp_campusmail_id,'$attachment_path','$temp_filename','$fs_filesSize','$is_embed')";
		                           $delim = ",";
		                       }
		                       $temp_attach_sql = "INSERT IGNORE INTO INTRANET_IMAIL_ATTACHMENT_PART
		                                           (CampusMailID,AttachmentPath,FileName,FileSize,isEmbed)
		                                           VALUES $temp_sql_values";
		                       $lc_temp->db_db_query($temp_attach_sql);
		                       
		                       $update_storage_table_sql ="UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = QuotaUsed+$total_filesize WHERE UserID = '$UserID'";
		                       $lc_temp->db_db_query($update_storage_table_sql);
		                   }
		                   
		                   // increment new mail counter
		                   //$lc->UpdateCampusMailNewMailCount($UserID, $default_folder, 1);
		                   
		                   $imail_received++;
		                   //$mail_id_to_remove[] = $mail_id;
		                   $mail_id_to_remove[] = $mail_uid;
		                   $checkmail_status = 1;                      # Successful got, submit again  
		                   $left = $left - $curr_attach_size;                 
		              }
		              
		              for ($i=0; $i<sizeof($mail_id_to_remove); $i++)
		              {
		                   //$lwebmail->removeMail($mail_id_to_remove[$i]);
		                   $lwebmail->removeMailByUID($mail_id_to_remove[$i]);
		              }
		              imap_expunge ($inbox);
		         }
		         else
		         {
		             # Can't get mail
		             $exmail_left = $lwebmail->checkMailCount();
		             if ($exmail_left > 0)
		             {
		                 $checkmail_status = 2;                 # Not enough quota
		             }
		             else
		             {
		                 $checkmail_status = 0;                 # No mails
		             }
		             break; // stop while ($mailCount > 0)
		         }
		         $newMailCount = $lwebmail->checkMailCount();
		 	 	 $mailCount = min($newMailCount, $max_per_turn);
		 	 	 usleep(1);
		 	 } // new mails > 0
	         $lwebmail->close();
	         /*
	         $sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$UserID."'";
	       	 $sum_record = $lc->returnVector($sql);
	       	 $used_quota = round(intval($sum_record[0]),1);
	       	 $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed, RequestUpdate) VALUES ('".$UserID."','".$used_quota."','0') ON DUPLICATE KEY UPDATE QuotaUsed = '$used_quota',RequestUpdate='0'";
	       	 $lc->db_db_query($sql);
	       	 */
      	} // end connection of inbox	
     }else{
	      $checkmail_status = 2;
	 }
	 
	 $sql = "DELETE FROM INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT WHERE UserID='".$_SESSION['UserID']."' AND FolderID IN (2,-2)";
	 $lc_temp->db_db_query($sql);
	 
	 // clean internet emails raw source files that are older than 3 months ago
	 $cmd = 'sudo find '.(OsCommandSafe($file_path).'/file/mail/u'.OsCommandSafe($_SESSION['UserID'])).' -maxdepth 2 -type f -name "body_*.txt" -mtime +90 -exec rm "{}" \;';
	 shell_exec($cmd);
	 
     # Response
     if ($checkmail_status == 1 && !$from_app)
     {
         $body_tags = "onLoad=process(this)";
         if ($imail_extotal == 0)
         {
             $pused = "50";
             echo "re: $imail_received";
             echo "/total: $imail_extotal";
         }
         else $pused = floor($imail_received/$imail_extotal*100);
         $pbar = "<TABLE width=80% align=left><tr><td style=\"font-size:10px;\"></td>";
         $pbar .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
         $pbar .= "<td style=\"font-size:10px;\"></td></tr>";
         $pbar .= "</table>";
         $html_text = "$pbar <br>";
         $html_text .= "$i_CampusMail_New_Receiving1 $imail_received $i_CampusMail_New_Receiving2 $imail_extotal $i_CampusMail_New_Receiving3";
     }
     else if ($checkmail_status == 2 && !$from_app)
     {
          //$body_tags = "";
          $body_tags = "onLoad=process2(this)";
         if ($imail_extotal == 0)
         {
             $pused = "50";
             echo "re: $imail_received";
             echo "/total: $imail_extotal";
         }
         else $pused = floor($imail_received/$imail_extotal*100);
          $pbar = "<TABLE width=80% align=left>";
          $pbar.="<tr><td style=\"font-size:10px;\"></td>";
          $pbar .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
          $pbar .= "<td style=\"font-size:10px;\"></td></tr>";
          $pbar .= "</table>";
          $html_text = "$pbar <br>";
          $html_text .= "$i_CampusMail_New_Receiving1 $imail_received $i_CampusMail_New_Receiving2 $imail_extotal $i_CampusMail_New_Receiving3";
          //$html_text .= "<br><a href=\"javascript:goback()\">$i_CampusMail_New_BackMailbox</a>";
			$error_msg = $linterface->GET_SYS_MSG("",$i_CampusMail_OutOfQuota_Warning);
			$error_msg = str_replace("\n","",$error_msg);
			$error_msg = str_replace("\r","",$error_msg);
			$error_msg = addslashes($error_msg);
     }
     else
     {
         $body_tags = "onLoad=goback()";
     }
}
else
{
    #header("Location: viewfolder.php?FolderID=2&mailchk=1");
	// die;
}
intranet_closedb();
$html_text = str_replace("\n","",$html_text);
$html_text = str_replace("\r","",$html_text);
$html_text = addslashes($html_text);

if(!$from_app){
?>
<html>
<script LANGUAGE=Javascript>

function process(obj)
{
	if(top && top.display){
		msg = top.display.document.getElementById('msg');
	}else if(parent && parent.display){
		msg = parent.display.document.getElementById('msg');
	}
	msg.innerHTML = "<?=$html_text?>";
	goback();
}
function process2(obj){
	if(top && top.display){
		msg = top.display.document.getElementById('msg');
		errmsg = top.display.document.getElementById('errmsg');
	}else if(parent && parent.display){
		msg = parent.display.document.getElementById('msg');
		errmsg = parent.display.document.getElementById('errmsg');
	}
	msg.innerHTML = "<?=$html_text?>";
	errmsg.innerHTML="<?=$error_msg?>";
	
}
function goback()
{
	if(top){
        top.location.href = "viewfolder.php?FolderID=2&mailchk=1";
	}else if(parent){
		parent.location.href = "viewfolder.php?FolderID=2&mailchk=1";
	}
}
</script>
<body <?=$body_tags?>>
<form name=form1 method=get action="">
<input type=submit>
</form>
</body>
</html>
<?php } ?>