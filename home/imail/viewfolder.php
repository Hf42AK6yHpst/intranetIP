<?php
// using : 

/***************************************
 *  modification log
 * 		2020-07-20 Henry: hide Move To Draft option [Case#A115163]
 * 
 *      2019-09-05 Ray: add $subject_replace for display subject
 * 
 * 		2017-03-06 Carlos: Use new cached counting new mail methods to get number of new/unread emails for each folder.
 * 
 * 		2015-05-12 Carlos: Replace < and > with &lt; and &gt; in Subject, it may break the UI.
 * 
 * 		2014-11-10 Carlos: For Sentbox, merge and display ExternalTo and ExternalCC
 * 
 * 		20131219 Carlos: 
 * 			$sys_custom['iMailHideDeleteAllButton'] - hide [Delete ALL] button
 * 		20130416 Carlos: 
 * 			added flag $sys_custom['iMail']['ArchiveMailNumber'] to control how many mails to trigger archive (default 3000)
 * 		20121018 Carlos: 
 * 			in order to eliminate the need to recalculate total used quota every page load (it is quite slow),
 * 			added a flag RequestUpdate to INTRANET_CAMPUSMAIL_USED_STORAGE for notifying this user require to update or not,
 * 
 * 		20110408 Carlos:
 * 			added js functions to check if number of mails > 3000 to show remind message
 * 
 * 		20110329 Yuen:
 * 			improved the performance by skipping searching in SQL statement if $keyword is not given
 * 
 * 		20100621 Marcus:
 * 			check $SYS_CONFIG['Mail']['hide_imail'] , if true , open interface_html with "imail_archive.php", hide draft in $select_toChangefolder
 * 
 * **************************************/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

if(isset($sys_custom['custom_imail_page_size'])){
	$page_size = $sys_custom['custom_imail_page_size'];
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$curDateFormat = date("Y-m-d");

if ($FolderID == -1)
{
    header("Location: trash.php");
    exit();
}

$libcampusmail = new libcampusmail();
$lwebmail = new libwebmail();
if (!isset($FolderID) || $FolderID == "")
{
     $FolderID = 2;
}
//if ($FolderID == 2)
if (($FolderID != 1) && ($FolderID != -1) && ($FolderID != 0))
{
    if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID))
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}

$lc = new libcampusquota2007($UserID);

//if(!$sys_custom['iMail']['DoNotRecalculateUsedQuota'])
if($sys_custom['iMail']['RecalculateUsedQuota'])
{
	$sql = "SELECT QuotaUsed,RequestUpdate FROM INTRANET_CAMPUSMAIL_USED_STORAGE WHERE UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."'";
	$tmp = $lc->returnArray($sql);
	$QuotaUsed = $tmp[0]['QuotaUsed'];
	// RequestUpdate is a flag used to indicate if current user require to recalculate its total used quota
	// RequestUpdate is set to 1 when new intranet mails are added to this user
	$RequestUpdate = $tmp[0]['RequestUpdate']; 
	if($RequestUpdate == 1 || $RequestUpdate=='')
	{
		// recalculate total used quota takes quite long time, so update only when notify by flag RequestUpdate
		$LastQuotaUsed = $lc->returnUsedQuota(1);
		if($LastQuotaUsed==''){
			$LastQuotaUsed = 0;
		}
		if($QuotaUsed != $LastQuotaUsed)
		{
			## Update USED QUOTA TABLE ##
			/*
			$sql = "SELECT UserID, IFNULL(SUM(AttachmentSize),0) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' GROUP BY UserID";
			$result = $lc->returnArray($sql,2);
			if(sizeof($result)>0){
				for($i=0; $i<sizeof($result); $i++){
					list($UserID, $UsedQuota) = $result[$i];
					$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$UserID', '$UsedQuota') ON DUPLICATE KEY UPDATE QuotaUsed = '$UsedQuota'";
					$lc->db_db_query($sql);
				}
			}
			*/
			$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed, RequestUpdate) VALUES ('".$libcampusmail->Get_Safe_Sql_Query($UserID)."', '$LastQuotaUsed', '0') ON DUPLICATE KEY UPDATE QuotaUsed = '$LastQuotaUsed',RequestUpdate='0'";
			$lc->db_db_query($sql);
			## End ##
		}
	}
}
/* Disabled by Ronald - 20101207
 * Suppose this part is used to check if there is any missed FolderID
############## Find back missing links
# Get Missing links FolderID
$sql = "	SELECT 
				DISTINCT a.UserFolderID, b.OwnerID 
			FROM 
				INTRANET_CAMPUSMAIL as a
       				LEFT OUTER JOIN 
       			INTRANET_CAMPUSMAIL_FOLDER as b 
       		ON 
       			a.UserFolderID = b.FolderID
			WHERE 
				a.UserID = $UserID AND a.UserFolderID > 2 AND OwnerID != $UserID
		";
$missing_records = $lc->returnArray($sql,2);
if (sizeof($missing_records)!=0)
{
    $list = "";
    $delim = "";
    for ($i=0; $i<sizeof($missing_records); $i++)
    {
         list($t_fid, $t_ownerID) = $missing_records[$i];
         if ($t_ownerID != $UserID && $t_fid > 0)
         {
             $list .= "$delim $t_fid";
             $delim = ",";
         }
    }
    if ($list != "")
    {
        $sql = "	UPDATE INTRANET_CAMPUSMAIL 
        			SET 
        				UserFolderID = 2 
        			WHERE 
        				UserID = $UserID AND UserFolderID IN ($list)
        		";
        $lc->db_db_query($sql);
        
        $sql = "CREATE TABLE IF NOT EXISTS INTRANET_CAMPUSMAIL_FOLDER_MISSED_LINK_LOG(
        		UserID int(11) NOT NULL,
        		FolderID int(11) NOT NULL,
        		OwnerID int(11) NOT NULL,
        		RecordType int,
        		RecordStatus int,
        		DateInput datetime,
        		DateModified datetime
        		)";
        $lc->db_db_query($sql);
        
        for($i=0; $i<sizeof($missing_records); $i++){
	        	list($folder_id, $owner_id) = $missing_records[$i];
        		$sql = "INSERT INTO INTRANET_CAMPUSMAIL_FOLDER_MISSED_LINK_LOG (UserID, FolderID, OwnerID, DateInput, DateModified) VALUES ($UserID, $folder_id, $owner_id, NOW(), NOW())";
        		$lc->db_db_query($sql);
    	}
    }
}
*/

if ($FolderID == 0 || $FolderID == 1)
{
	$default_field = 3;
} 
else if ($FolderID == 2)
{
	if (!$noWebmail)
	{
		$default_field = 6;		
	} else {
		$default_field = 5;
	}
}
else {
	if (!$noWebmail)
	{
		$default_field = 6;		
	} else {
		$default_field = 5;
	}
}

//$default_field = ($noWebmail || ($FolderID == 0 || $FolderID == 1) ? 3:4);
if($field=="") $field = $default_field;

$li = new libdbtable2007($field, $order, $pageNo);

### checking mails over the days in trash / spam ###
$sql = "SELECT DaysInSpam, DaysInTrash FROM INTRANET_IMAIL_PREFERENCE WHERE UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."'";
$arr_result = $li->returnArray($sql,2);
if(sizeof($arr_result)>0){
	### $DaysInSpam & $DaysInTrash = -1 means keep in folder forever
	### else it means the days keep in the folder
    list($DaysInSpam, $DaysInTrash) = $arr_result[0];
    
    if($DaysInSpam == 0){
	    $DaysInSpam = -1;
    }
    if($DaysInTrash == 0){
	    $DaysInTrash = -1;
    }
}else{
	
    if($sys_custom['iMail_Admin_DayInTrashSettings']){
	    ### check any days in trash setted by Admin ###
    	$DaysInTrash = $lwebmail->retriveDayInTrashByAdmin();
	}else{
	    $DaysInTrash = -1;
    }
    $DaysInSpam = -1;
}

if($DaysInSpam != -1){		## Spam Mail ##

	$targetDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$DaysInSpam, date("Y")));
	
	###########################################
	### 		Variable Meaning 			###
	### *********************************** ###
	### UserFolderID: -2 (in spam folder)	###
	###########################################
	$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND UserFolderID = -2	 AND (DateInFolder < '".$libcampusmail->Get_Safe_Sql_Query($targetDate)."')";
	$arr_result = $li->returnVector($sql);
	
	if(sizeof($arr_result)>0){
		$targetMailID = implode(",",$arr_result);
		
		if($targetMailID != ""){		### Start to delete the 'expired' mail in spam folder ###
			$sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID='".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND CampusMailID IN ($targetMailID)";
			$tmp_ary = $li->returnVector($sql);
			$deleted_attachment_size = intval($tmp_ary[0]);
			
			$sql = "SELECT COUNT(*) as NewMailCount FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND CampusMailID IN ($targetMailID) AND (RecordStatus IS NULL OR RecordStatus='')";
			$count_record = $li->returnVector($sql);
			if($count_record[0] > 0){
			    $libcampusmail->UpdateCampusMailNewMailCount($_SESSION['UserID'], -2, -$count_record[0]);
			}
			
			$sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND UserFolderID = -2 AND CampusMailID IN ($targetMailID)";
			$li->db_db_query($sql);
			
			//$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET RequestUpdate='1' WHERE UserID = '$UserID'";
			//$li->db_db_query($sql);
			$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed=GREATEST(0,QuotaUsed-$deleted_attachment_size) WHERE UserID='".$libcampusmail->Get_Safe_Sql_Query($UserID)."' ";
			$li->db_db_query($sql);
		}
	}
}

if($DaysInTrash != -1){		## Trash Mail ##
			
	$targetDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$DaysInTrash, date("Y")));
	
	###########################################
	### 		Variable Meaning 			###
	### *********************************** ###
	### Deleted: 1 - in trash folder		###
	###########################################
	$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND Deleted = 1 AND (DateInFolder < '$targetDate')";
	$arr_result = $li->returnVector($sql);
	
	if(sizeof($arr_result)>0){
		$targetMailID = implode(",",$arr_result);
		
		if($targetMailID != ""){		### Start to delete the 'expired' mail in trash folder ###
			$sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID='".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND CampusMailID IN ($targetMailID)";
			$tmp_ary = $li->returnVector($sql);
			$deleted_attachment_size = intval($tmp_ary[0]);
			
			$sql = "SELECT COUNT(*) as NewMailCount FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND CampusMailID IN ($targetMailID) AND (RecordStatus IS NULL OR RecordStatus='')";
			$count_record = $li->returnVector($sql);
			if($count_record[0] > 0){
			    $libcampusmail->UpdateCampusMailNewMailCount($_SESSION['UserID'], -1, -$count_record[0]);
			}
			
			$sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND Deleted = 1 AND CampusMailID IN ($targetMailID)";
			$li->db_db_query($sql);
			
			//$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET RequestUpdate='1' WHERE UserID = '$UserID'";
			//$li->db_db_query($sql);
			$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed=GREATEST(0,QuotaUsed-$deleted_attachment_size) WHERE UserID='".$libcampusmail->Get_Safe_Sql_Query($UserID)."' ";
			$li->db_db_query($sql);
		}
	}
}
### End of checking mails over the days in trash / spam ###

# Find unread mails number
if ($exmsg == "1")
{
	/*
	$sql  = "
				SELECT
					count(a.CampusMailID)
	          	FROM 
	          		INTRANET_CAMPUSMAIL AS a 
				WHERE
	               a.UserID = '$UserID' AND
	               a.UserFolderID = 2 AND
				   (a.RecordStatus = '' OR a.RecordStatus IS NULL) AND 
	               a.Deleted != 1 
	          ";
	$row = $li->returnVector($sql);	          
	$unreadInboxNo = $row[0];
	$iNewCampusMail = $unreadInboxNo;
	*/
	$unreadInboxNo = $libcampusmail->GetCampusMailNewMailCount($_SESSION['UserID'], $FolderID);
	$iNewCampusMail = $unreadInboxNo;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);

$RecordType = $FolderID;
$li->IsColOff = "imail";

# Build Table and SQL
$username_field = getNameFieldWithClassNumberByLang("b."); //($chi? "ChineseName": "EnglishName");
if (!isset($MailType))
{
     # Retrieve customized flag
     $MailType = $sys_custom['iMail_viewfolder_default_MailType'];
}

if ($MailType=="")
{
    $conds_mailtype = "";
}
else
{
    $conds_mailtype = "AND a.MailType = '".$libcampusmail->Get_Safe_Sql_Query($MailType)."' ";
}

# Different Cases:
# Inbox

// New UI for IP 2.0


$subject_replace = "REPLACE(REPLACE(REPLACE(REPLACE(a.Subject,'<','&lt;'),'>','&gt;'),'\\\\\\\\', '\\\\'), '\\\\\'', '\'')";


if ($FolderID == 2)
{
	# Inbox field : important, notification, attachment, subject, size, sender, is read
	if (!$noWebmail)
	{
	     $array_mailtype = array( array(1,"$i_CampusMail_New_MailSource_Internal"), array(2,"$i_CampusMail_New_MailSource_External"));
	     $select_mailtype = getSelectByArray($array_mailtype,"name=MailType onChange=this.form.submit()",$MailType,1);
	     $li->field_array[] = "a.MailType";
	     $db_webmail_field = "IF(a.MailType=2,'$i_CampusMail_New_Icon_ExtMail','$i_CampusMail_New_Icon_IntMail'),";
	}
	else
	{
	    $db_webmail_field = "";
	}
	$li->field_array[] = "a.IsImportant";
	$li->field_array[] = "a.IsNotification";
	$li->field_array[] = "a.RecordStatus";	
	$li->field_array[] = "UserName";
	$li->field_array[] = "a.Subject";
	$li->field_array[] = "a.DateInput";
	$li->field_array[] = "a.AttachmentSize";	

	if ($unreadOnly == 1)
	{
		$unreadRestrict = " AND (a.RecordStatus='' OR a.RecordStatus IS NULL) ";
	}
	
	if($sys_custom['iMail_iFrameSubject'] == '' || $sys_custom['iMail_iFrameSubject'] == false)
	{
		## old version - without iFrame 
		$sql  = "SELECT
					   $db_webmail_field
					   IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),               
					   IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),
					   IF(a.RecordStatus='1','".addslashes($i_CampusMail_New_Icon_ReadMail_2007)."',IF(a.RecordStatus='2','".addslashes($i_CampusMail_New_Icon_RepliedMail_2007)."','".addslashes($i_CampusMail_New_Icon_NewMail_2007)."')),               	               
					   IF(a.MailType=1,
						  IF(a.RecordStatus='1' OR a.RecordStatus='2',
						  IF(a.SenderID = 0, '$i_general_sysadmin', IF(TRIM(CONCAT($username_field)) = '' OR CONCAT($username_field) IS NULL, IF(b.UserLogin IS NOT NULL, CONCAT('".$i_UserLogin." : ',b.UserLogin), CONCAT('<i>','".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."','</i>')),CONCAT($username_field))),
						  IF(a.SenderID = 0, '$i_general_sysadmin', IF(TRIM(CONCAT($username_field)) = '' OR CONCAT($username_field) IS NULL, IF(b.UserLogin IS NOT NULL, CONCAT('".$i_UserLogin." : ',b.UserLogin), CONCAT('<i>','".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."','</i>')),CONCAT($username_field)))),
							IF(a.RecordStatus='1' OR a.RecordStatus='2',CONCAT(a.SenderEmail),CONCAT(a.SenderEmail)) 
					   ) as UserNameWeb,
					   
					   
					   IF(a.RecordStatus='1' OR a.RecordStatus='2',
					   IF(a.IsAttachment=1,
					   CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(TRIM(a.Subject)='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>'),
					   CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' >', IF(TRIM(a.Subject)='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>')),
					   IF(a.IsAttachment=1,
					   CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(TRIM(a.Subject)='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>'),
					   CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' >', IF(TRIM(a.Subject)='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>'))),	               
					   
						IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),               		
					   '',
					   IF(a.IsAttachment=1,
					   IF (a.AttachmentSize IS NULL,'0', CONCAT('',a.AttachmentSize,'')),'-'),
					   
					   CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
					   a.RecordStatus,
					   IF(a.MailType=1,
						  IF(a.SenderID = 0, '$i_general_sysadmin', $username_field),
							a.SenderEmail
					   ) as UserName,
					   CONCAT('INBOX')
				  FROM 
						INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
				  ON 
						a.SenderID = b.UserID
				  WHERE
					   a.UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND
					   a.UserFolderID = '".$libcampusmail->Get_Safe_Sql_Query($FolderID)."' AND
					   a.Deleted != 1
					   $conds_mailtype
					   $unreadRestrict					   
				  ";
			if (trim($keyword)!="")
				$sql  .= "AND (a.Subject like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%' OR a.Message like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%'
						OR $username_field like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%')";
	}
	else
	{
		# new version - using iframe
		$sql  = "SELECT
					   $db_webmail_field
					   IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),               
					   IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),
					   IF(a.RecordStatus='1','".addslashes($i_CampusMail_New_Icon_ReadMail_2007)."',IF(a.RecordStatus='2','".addslashes($i_CampusMail_New_Icon_RepliedMail_2007)."','".addslashes($i_CampusMail_New_Icon_NewMail_2007)."')),               	               
					   IF(a.MailType=1,
						  IF(a.RecordStatus='1' OR a.RecordStatus='2',
						  IF(a.SenderID = 0, '$i_general_sysadmin', IF(TRIM(CONCAT($username_field)) = '' OR CONCAT($username_field) IS NULL, IF(b.UserLogin IS NOT NULL, CONCAT('".$i_UserLogin." : ',b.UserLogin), CONCAT('<i>','".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."','</i>')),CONCAT($username_field))),
						  IF(a.SenderID = 0, '$i_general_sysadmin', IF(TRIM(CONCAT($username_field)) = '' OR CONCAT($username_field) IS NULL, IF(b.UserLogin IS NOT NULL, CONCAT('".$i_UserLogin." : ',b.UserLogin), CONCAT('<i>','".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."','</i>')),CONCAT($username_field)))),
							IF(a.RecordStatus='1' OR a.RecordStatus='2',CONCAT(a.SenderEmail),CONCAT(a.SenderEmail)) 
					   ) as UserNameWeb,
					   
					   IF(a.RecordStatus='1' OR a.RecordStatus='2',
					   IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>')
							)),
					   IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>')
						))),	               
					   
						IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),               		
					   '',
					   IF(a.IsAttachment=1,
					   IF (a.AttachmentSize IS NULL,'0', CONCAT('',a.AttachmentSize,'')),'-'),
					   
					   CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
					   a.RecordStatus,
					   IF(a.MailType=1,
						  IF(a.SenderID = 0, '$i_general_sysadmin', $username_field),
							a.SenderEmail
					   ) as UserName,
					   CONCAT('INBOX')	               
				  FROM 
						INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
				  ON 
						a.SenderID = b.UserID
				  WHERE
					   a.UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND
					   a.UserFolderID = '".$libcampusmail->Get_Safe_Sql_Query($FolderID)."' AND
					   a.Deleted != 1
					   $conds_mailtype
					   $unreadRestrict
				  ";
			
			if (trim($keyword)!="")
				$sql  .= "AND (a.Subject like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%' OR a.Message like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%'
						OR $username_field like '%$keyword%')";
	}
	// echo '<pre>sql : '.$sql.'</pre>';
	// TABLE COLUMN
	$pos = 0;
	if (!$noWebmail)
	{
		$pos++;
	     $li->column_list .= "<td width='23' class='tabletop' >".$i_CampusMail_New_Icon_MailSource."</td>\n";
	     $width_subject = 177-23;
	}
	else
	{
	    $width_subject = 177;
	}	

	$li->column_list .= "<td width='9' class='tabletop' >".$li->column_image($pos, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important_2007)."</td>\n";	
	$li->column_list .= "<td width='20' class='tabletop' >".$li->column_image($pos+1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification_2007)."</td>\n";
	$li->column_list .= "<td width='20' class='tabletop' >".$li->column_image($pos+2, $i_frontpage_campusmail_status, $i_frontpage_campusmail_icon_status_2007)."</td>\n";
	$li->column_list .= "<td class='tabletop' >".$li->column($pos+3, $i_frontpage_campusmail_sender)."</td>\n";
	$li->column_list .= "<td width='350' class='tabletop' >".$li->column($pos+4, $i_frontpage_campusmail_subject)."</td>\n";
	$li->column_list .= "<td class='tabletop' >".$li->column($pos+5, $i_frontpage_campusmail_date)."</td>\n";
	$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
	$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos+6, $i_frontpage_campusmail_size)."</td>\n";
	$li->column_list .= "<td width='25' class='tabletop' >".$li->check("CampusMailID[]")."</td>\n";
	if (!$noWebmail)
	{
	    $li->column_array = array(0,0,0,0,5,16,11,0,0,0,0,0,0);
	    $li->wrap_array = array(0,0,0,0,20,14,0,0,0,0,0,0,0,0);
	}
	else
	{
	    $li->column_array = array(0,0,0,5,16,11,0,0,0,0,0);
	    $li->wrap_array = array(0,0,0,20,14,0,0,0,0,0,0,0);
	}

	$li->no_col = sizeof($li->field_array)+4;
	$CurrentPage = "PageCheckMail_Inbox";
}
else if ($FolderID == 0) 
{
	# Sent box	
	$li->field_array = array(
	                   "a.IsImportant"
	                   , "a.IsNotification"
	                   , "a.Subject"
	                   , "a.DateInput"	                   
	                   , "a.AttachmentSize"
	                   , "a.RecipientID"
	                   , "a.ExternalTo");
	if($sys_custom['iMail_iFrameSubject'] == '' || $sys_custom['iMail_iFrameSubject'] == false)
	{
		$sql  = "SELECT
					   IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
					   IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),
					   a.RecipientID,
					   CONCAT(a.ExternalTo,a.ExternalCC) as ExternalTo,

					   
					   IF(a.RecordStatus='1' OR a.RecordStatus='2',
					   IF(a.IsAttachment=1,
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n'),							
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
							),
					   IF(a.IsAttachment=1,
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n'),	
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
						)),	
						
						IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),               		
					   '',
					   IF(a.IsAttachment=1,
					   IF (a.AttachmentSize IS NULL,'0', CONCAT(a.AttachmentSize,'')),'-'),
					   
					   a.CampusMailID,
					   CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
					   IF (a.RecordStatus IS NULL, 1, a.RecordStatus),
					   '',
					   CONCAT('OUTBOX')
				  FROM INTRANET_CAMPUSMAIL AS a
				  WHERE
					   a.UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND
					   a.UserFolderID = '".$libcampusmail->Get_Safe_Sql_Query($FolderID)."' AND
					   a.Deleted != 1
				  ";
			if (trim($keyword)!="")
				$sql  .= "AND (a.Subject like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%' OR a.Message like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%')";
	}
	else
	{
		$sql  = "SELECT
					   IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
					   IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),
					   a.RecipientID,
					   CONCAT(a.ExternalTo,a.ExternalCC) as ExternalTo,

					   IF(a.RecordStatus='1' OR a.RecordStatus='2',
					   IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
							)),
					   IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
						))),	
						
						IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),               		
					   '',
					   IF(a.IsAttachment=1,
					   IF (a.AttachmentSize IS NULL,'0', CONCAT(a.AttachmentSize,'')),'-'),
					   
					   a.CampusMailID,
					   CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
					   IF (a.RecordStatus IS NULL, 1, a.RecordStatus),
					   '',
					   CONCAT('OUTBOX')
				  FROM INTRANET_CAMPUSMAIL AS a
				  WHERE
					   a.UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND
					   a.UserFolderID = '".$libcampusmail->Get_Safe_Sql_Query($FolderID)."' AND
					   a.Deleted != 1
				  ";
			if (trim($keyword)!="")
				$sql  .= "AND (a.Subject like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%' OR a.Message like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%')";
	}
	// TABLE COLUMN		
	
	$li->column_list .= "<td width='15' class='tabletop' >".$li->column_image(0, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important_2007)."</td> \n";
	$li->column_list .= "<td width='25' class='tabletop' >".$li->column_image(1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification_2007)."</td> \n";
	$li->column_list .= "<td width='100' class='tabletop' >".$li->column(5, $i_CampusMail_New_InternalRecipients)."</td> \n";
	$li->column_list .= "<td width='100' class='tabletop' >".$li->column(6, $i_CampusMail_New_ExternalRecipients)."</td> \n";
	$li->column_list .= "<td width='300' class='tabletop' >".$li->column(2, $i_frontpage_campusmail_subject)."</td>\n";
	$li->column_list .= "<td class='tabletop' >".$li->column(3, $i_frontpage_campusmail_date)."</td>\n";	
	$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
	$li->column_list .= "<td class='tabletop' >".$li->column(4, $i_frontpage_campusmail_size)."</td>\n";
	$li->column_list .= "<td width='60' class='tabletoplink' >$i_frontpage_campusmail_read / $i_frontpage_campusmail_total</td>\n";
	$li->column_list .= "<td width='25' class='tabletop' >".$li->check("CampusMailID[]")."</td>\n";
	$li->column_array = array(0,0,0,0,0,5,0,0,8);
	$li->wrap_array = array(0,0,0,0,0,40,0,0,0);
	
	$li->no_col = sizeof($li->field_array)+5;
	$CurrentPage = "PageCheckMail_Outbox";

}
else if ($FolderID == 1)  
{
	# Draft Box	
	$li->field_array = array("a.IsImportant","a.IsNotification","a.Subject","a.DateInput","a.AttachmentSize");
	$sql  = "	SELECT
               		IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
               		IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),               		
              "; 	
	if (auth_sendmail())
	{
		if($sys_custom['iMail_iFrameSubject'] == '' || $sys_custom['iMail_iFrameSubject'] == false)
		{
			$sql .= "
						IF(a.RecordStatus='1' OR a.RecordStatus='2',
						IF(a.IsAttachment=1,
							CONCAT('<a href=\'compose.php?action=D&CampusMailID=', a.CampusMailID, '\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n'),
							CONCAT('<a href=\'compose.php?action=D&CampusMailID=', a.CampusMailID, '\' class=\'iMailsubject\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
						),
					   IF(a.IsAttachment=1,
							CONCAT('<a href=\'compose.php?action=D&CampusMailID=', a.CampusMailID, '\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n'),
							CONCAT('<a href=\'compose.php?action=D&CampusMailID=', a.CampusMailID, '\' class=\'iMailsubjectunread\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
						)),	
						";
		}
		else
		{
			$sql .= "
						IF(a.RecordStatus='1' OR a.RecordStatus='2',
						IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<a href=\'compose.php?action=D&CampusMailID=', a.CampusMailID, '\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<a href=\'compose.php?action=D&CampusMailID=', a.CampusMailID, '\' class=\'iMailsubject\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
							)),
					   IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<a href=\'compose.php?action=D&CampusMailID=', a.CampusMailID, '\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<a href=\'compose.php?action=D&CampusMailID=', a.CampusMailID, '\' class=\'iMailsubjectunread\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
						))),	
						";
		}
	}
	else
	{
    	$sql .= "IF(a.IsAttachment=1,CONCAT('".addslashes($i_frontpage_campusmail_icon_attachment_2007)."',$subject_replace),$subject_replace),";
	}              
	$sql .= "               		
               		' &nbsp;',
               		IF ((DATE_FORMAT(a.DateModified, '%Y-%m-%d') = '{$curDateFormat}'),
					CONCAT('<span title=\"',DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateModified, '%H:%i'),'</span>'),
					CONCAT('<span title=\"',DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateModified, '%Y-%m-%d'),'</span>')),               		
               	";
	$sql .= "
	               ' &nbsp;',
	               
	               IF(a.IsAttachment=1,
	               IF (a.AttachmentSize IS NULL,'0', CONCAT(a.AttachmentSize,'')),'-'),
	               
	               CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
	               IF (a.RecordStatus IS NULL, 0, a.RecordStatus),
	               ''
	          FROM 
	          	INTRANET_CAMPUSMAIL AS a
	          WHERE
	               a.UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND
	               a.UserFolderID = '".$libcampusmail->Get_Safe_Sql_Query($FolderID)."' AND
	               a.Deleted != 1
	          ";
	if (trim($keyword)!="")
				$sql  .= "AND (a.Subject like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%' OR a.Message like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%')";
	// TABLE COLUMN
	$li->column_list .= "<td width='23' class='tabletop' >".$li->column_image(0, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important_2007)."</td>\n";
	$li->column_list .= "<td width='27' class='tabletop' >".$li->column_image(1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification_2007)."</td>\n";	
	$li->column_list .= "<td width='366' class='tabletop' >".$li->column(2, $i_frontpage_campusmail_subject)."</td>\n";
	$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
	$li->column_list .= "<td width='100' class='tabletop' >".$li->column(3, $i_frontpage_campusmail_date)."</td>\n";
	$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
	$li->column_list .= "<td width='75' class='tabletop' >".$li->column(4, $i_frontpage_campusmail_size)."</td>\n";
	$li->column_list .= "<td width='40' class='tabletop' >".$li->check("CampusMailID[]")."</td>\n";
	
	$li->column_array = array(0,0,0,0,11,0,0,0,0,0);
	$li->wrap_array = array(0,0,0,0,0,30,0,0,0,0);
	$li->no_col = sizeof($li->field_array)+5;	
	$CurrentPage = "PageCheckMail_Draft";
}
else
{	
	if($FolderID == -2) {
		$CurrentPage = "PageCheckMail_Spam";
	}
	# Self-defined
	# field : important, notification, attachment, subject, size, sender, is read
	if (!$noWebmail)
	{
     	 $array_mailtype = array( array(1,"$i_CampusMail_New_MailSource_Internal"), array(2,"$i_CampusMail_New_MailSource_External"));
         $select_mailtype = getSelectByArray($array_mailtype,"name=MailType onChange=this.form.submit()",$MailType,1);
	     $li->field_array[] = "a.MailType";
	     $db_webmail_field = "IF(a.MailType=2,'$i_CampusMail_New_Icon_ExtMail','$i_CampusMail_New_Icon_IntMail'),";
	}
	else
	{
	    $db_webmail_field = "";
	}
	$li->field_array[] = "a.IsImportant";
	$li->field_array[] = "a.IsNotification";	
	$li->field_array[] = "a.RecordStatus";
	$li->field_array[] = "UserName";
	$li->field_array[] = "a.Subject";
	$li->field_array[] = "a.DateInput";
	$li->field_array[] = "a.AttachmentSize";
	
	if($sys_custom['iMail_iFrameSubject'] == '' || $sys_custom['iMail_iFrameSubject'] == false)
	{
		$sql  = "	SELECT
						$db_webmail_field
						IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
						IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),
						
						IF(a.RecordStatus='1','".addslashes($i_CampusMail_New_Icon_ReadMail_2007)."',IF(a.RecordStatus='2','".addslashes($i_CampusMail_New_Icon_RepliedMail_2007)."','".addslashes($i_CampusMail_New_Icon_NewMail_2007)."')),	               
						
						IF(a.MailType=1,
							IF(a.RecordStatus='1' OR a.RecordStatus='2',
							IF(a.SenderID = 0, '$i_general_sysadmin', IF(TRIM(CONCAT($username_field)) = '' OR CONCAT($username_field) IS NULL,CONCAT('<i>','".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."','</i>'),CONCAT($username_field))),
							IF(a.SenderID = 0, '$i_general_sysadmin', IF(TRIM(CONCAT($username_field)) = '' OR CONCAT($username_field) IS NULL,CONCAT('<i>','".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."','</i>'),CONCAT($username_field)))),
							IF(a.RecordStatus='1' OR a.RecordStatus='2',CONCAT(a.SenderEmail),CONCAT(a.SenderEmail)) 
						) as UserNameWeb,               	
											
						IF(a.RecordStatus='1' OR a.RecordStatus='2',    				
						IF(a.IsAttachment=1,
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n'),							
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
							),
					    IF(a.IsAttachment=1,
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n'),	
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' >', IF(a.Subject='' OR a.Subject IS NULL, '[NO SUBJECT]', $subject_replace) , '</a>\n')
						)),
						
						IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),               		
						'',
					   IF(a.IsAttachment=1,
					   IF (a.AttachmentSize IS NULL,'0', CONCAT(a.AttachmentSize,'')),'-'),
						
						CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
						a.RecordStatus,
						
						IF(a.MailType=1,
							IF(a.SenderID IS NULL, '$i_general_sysadmin', $username_field),
							a.SenderEmail
						) as UserName,
						CONCAT('OTHERS')
					FROM 
						INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
					ON 
						a.SenderID = b.UserID
					WHERE
					   a.UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND
					   a.UserFolderID = '".$libcampusmail->Get_Safe_Sql_Query($FolderID)."' AND
					   a.Deleted != 1
					   $conds_mailtype
				  ";
		if (trim($keyword)!="")
				$sql  .= "AND (a.Subject like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%' OR a.Message like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%'
						OR $username_field like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%')";
		// TABLE COLUMN
	}
	else
	{
		$sql  = "	SELECT
						$db_webmail_field
						IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
						IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),
						
						IF(a.RecordStatus='1','".addslashes($i_CampusMail_New_Icon_ReadMail_2007)."',IF(a.RecordStatus='2','".addslashes($i_CampusMail_New_Icon_RepliedMail_2007)."','".addslashes($i_CampusMail_New_Icon_NewMail_2007)."')),	               
						
						IF(a.MailType=1,
							IF(a.RecordStatus='1' OR a.RecordStatus='2',
							IF(a.SenderID = 0, '$i_general_sysadmin', IF(TRIM(CONCAT($username_field)) = '' OR CONCAT($username_field) IS NULL,CONCAT('<i>','".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."','</i>'),CONCAT($username_field))),
							IF(a.SenderID = 0, '$i_general_sysadmin', IF(TRIM(CONCAT($username_field)) = '' OR CONCAT($username_field) IS NULL,CONCAT('<i>','".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."','</i>'),CONCAT($username_field)))),
							IF(a.RecordStatus='1' OR a.RecordStatus='2',CONCAT(a.SenderEmail),CONCAT(a.SenderEmail)) 
						) as UserNameWeb,               	
											
						IF(a.RecordStatus='1' OR a.RecordStatus='2',    				
						IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>')
							)),
					   IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=30 scrolling=no valign=middle src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>')
						))),
						
						IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
						CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),               		
						'',
					   IF(a.IsAttachment=1,
					   IF (a.AttachmentSize IS NULL,'0', CONCAT(a.AttachmentSize,'')),'-'),
						
						CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
						a.RecordStatus,
						
						IF(a.MailType=1,
							IF(a.SenderID IS NULL, '$i_general_sysadmin', $username_field),
							a.SenderEmail
						) as UserName,
						CONCAT('OTHERS')
					FROM 
						INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
					ON 
						a.SenderID = b.UserID
					WHERE
					   a.UserID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' AND
					   a.UserFolderID = '".$libcampusmail->Get_Safe_Sql_Query($FolderID)."' AND
					   a.Deleted != 1
					   $conds_mailtype
				  ";
		if (trim($keyword)!="")
				$sql  .= "AND (a.Subject like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%' OR a.Message like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%'
						OR $username_field like '%".$libcampusmail->Get_Safe_Sql_Like_Query($keyword)."%')";
		// TABLE COLUMN
	}
	$pos = 0;	
	if (!$noWebmail)
	{
		$pos++;
	     $li->column_list .= "<td width='23' class='tabletop' >".$i_CampusMail_New_Icon_MailSource."</td>\n";
	     $width_subject = 177-23;
	}
	else
	{
	    $width_subject = 177;
	}	

	$li->column_list .= "<td width='9' class='tabletop' >".$li->column_image($pos, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important_2007)."</td>\n";	
	$li->column_list .= "<td width='20' class='tabletop' >".$li->column_image($pos+1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification_2007)."</td>\n";
	$li->column_list .= "<td width='20' class='tabletop' >".$li->column_image($pos+2, $i_frontpage_campusmail_status, $i_frontpage_campusmail_icon_status_2007)."</td>\n";
	$li->column_list .= "<td class='tabletop' >".$li->column($pos+3, $i_frontpage_campusmail_sender)."</td>\n";
	$li->column_list .= "<td width='350' class='tabletop' >".$li->column($pos+4, $i_frontpage_campusmail_subject)."</td>\n";
	$li->column_list .= "<td class='tabletop' >".$li->column($pos+5, $i_frontpage_campusmail_date)."</td>\n";
	$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
	$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos+6, $i_frontpage_campusmail_size)."</td>\n";
	$li->column_list .= "<td width='25' class='tabletop' >".$li->check("CampusMailID[]")."</td>\n";
	if (!$noWebmail)
	{
	    $li->column_array = array(0,0,0,0,5,16,11,0,0,0,0,0,0);
	    $li->wrap_array = array(0,0,0,0,20,14,0,0,0,0,0,0,0,0);
	}
	else
	{
	    $li->column_array = array(0,0,0,5,16,11,0,0,0,0,0);
	    $li->wrap_array = array(0,0,0,20,14,0,0,0,0,0,0,0);
	}

	$li->no_col = sizeof($li->field_array)+4;
	//$CurrentPage = "PageCheckMail_FolderManager";
}

# TABLE INFO
$li->sql = $sql;
//echo $li->built_sql();
$li->title = "";

$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."' OR RecordType = 0 ORDER BY RecordType, FolderName";
$folders = $lc->returnArray($folder_sql,2);
$folders[] = array(-1,$i_admintitle_im_campusmail_trash);

### For new Interface ###
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information

	
if ($FolderID == 2)
{
	$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_inbox.gif' align='absmiddle' />";
	$iMailTitle1 = "<span class='imailpagetitle'>{$i_frontpage_campusmail_inbox}</span>";
	
	if ($unreadInboxNo > 0)
	{
		$unreadLink = "<a href='viewfolder.php?FolderID=2&unreadOnly=1' class='imailpagetitleunread' > ({$unreadInboxNo} {$i_frontpage_campusmail_unread}) </a>";
	}
} 
else if ($FolderID == 0)
{
	$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_outbox.gif' align='absmiddle' />";
	$iMailTitle1 = "<span class='imailpagetitle'>{$i_frontpage_campusmail_outbox}</span>";
} 
else if ($FolderID == 1)
{
	$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_draft.gif' align='absmiddle' />";
	$iMailTitle1 = "<span class='imailpagetitle'>{$i_frontpage_campusmail_draft}</span>";
} 
else {
	if(($webmail_info['bl_spam'])&&($FolderID == -2)) {
		
		$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_spam.gif' align='absmiddle' />";
		$iMailTitle1 = "<span class='imailpagetitle'>{$i_spam['FolderSpam']}</span>";
		
	} else {
		
		$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_folder_close.gif' align='absmiddle' />";
		for($i=0;$i<count($folders);$i++)
		{
			if ($folders[$i][0] == $FolderID)
			{
				$iMailTitle1 = "<span class='imailpagetitle'>".$folders[$i][1]."</span>";
			}
		}
		
	}
}
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1.$unreadLink."</td><td align='right' style=\"vertical-align: bottom;\" >".$lc->displayiMailFullBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

if($SYS_CONFIG['Mail']['hide_imail']===true)
	$linterface = new interface_html("imail_archive.html");
else
	$linterface = new interface_html("imail_default.html");
$linterface->LAYOUT_START();

if ($msg == "3")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_remove."</font>")."</td></tr>";
}else if($msg == "-1")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=red>".$Lang['iMail']['ErrorMsg']['MailCannotSendOutSuccessfully']."</font>")."</td></tr>";
} 
else if ($exmsg == "1")
{
	if ($signal == "2")
	{		
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_save."</font>")."</td></tr>";
	} 
	else 
	{
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_msg_email."</font>" )."</td></tr>";
	}
} 
else if ($exmsg == "4")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_restore."</font>")."</td></tr>";
}
else if ($exmsg == "0")			### Only For IP2.0
{
	if($signal == "1")
	{
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_msg_email."</font>" )."</td></tr>";
	}
} 			### End at here							
else 
{
	$msgarr = explode("|=|",$msg);
	if (count($msgarr)>1)
	{
		if($msgarr[0]=="1"){
			$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$msgarr[1]."</font>")."</td></tr>";
		}else if($msgarr[0]=="0"){
			$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=red>".$msgarr[1]."</font>")."</td></tr>";
		}else
			$xmsg = "";
	}else
		$xmsg = "";
}

if ($special_feature['imail_export']) 
{
	$exportBtn = "<a href='javascript:openExport()' class='contenttool' >".exportIcon3()." ".$button_export_all."</a>";
}
//$functionbar .= "<a href=javascript:checkRemove(document.form1,'CampusMailID[]','remove.php')><img src=$image_delete border=0></a>";
//$functionbar .= "<a href=\"javascript:AlertPost(document.form1,'remove_all.php','$i_campusmail_alert_trashall')\"><img src=\"$image_path/btn_deleteall_$intranet_session_language.gif\" border=0></a>";

$deleteBtn  = "<a href=\"javascript:checkRemove(document.form1,'CampusMailID[]','remove.php')\" class=\"tabletool\">";
$deleteBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
$deleteBtn .= $button_remove."</a>";

$deleteAllBtn  = "<a href=\"javascript:AlertPost(document.form1,'remove_all.php','$i_campusmail_alert_trashall')\" class=\"tabletool\">";
$deleteAllBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash_all.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
$deleteAllBtn .= $button_remove_all." </a>";

### Star to Gen Drop Down Menu - Move Folder List ###
### Variable Meaning : FolderID
### 					0 - Outbox
### 					1 - Draft
### 					2 - Inbox
### 					-2 - SPAM (Optional)

//if ($FolderID != 1)
//{
	$folders = array();
	
	if($FolderID != 2)
	{
		$tmp_arr = array(2,$i_CampusMail_New_Inbox);
		array_push($folders,$tmp_arr);
	}
	if($FolderID != 0)
	{
		$tmp_arr = array(0,$i_CampusMail_New_Outbox);
		array_push($folders,$tmp_arr);
	}
//	if($FolderID != 1 && $SYS_CONFIG['Mail']['hide_imail']!==true)
//	{
//		$tmp_arr = array(1,$i_frontpage_campusmail_draft);
//		array_push($folders,$tmp_arr);
//	}
	
	if($webmail_info['bl_spam']){
		if($FolderID != -2)
		{
			$tmp_arr[0] = -2;
			$tmp_arr[FolderID] = -2;
			$tmp_arr[1] = $i_spam['FolderSpam'];
			$tmp_arr[FolderName] = $i_spam['FolderSpam'];
			array_push($folders,$tmp_arr);
		}
	}
	
	$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE (OwnerID = '".$libcampusmail->Get_Safe_Sql_Query($UserID)."') AND (FolderID != '".$libcampusmail->Get_Safe_Sql_Query($FolderID)."') ORDER BY RecordType, FolderName";
	$tmp_folders = $lc->returnArray($folder_sql,2);
	
	if(sizeof($tmp_folders)>0){
		for($i=0; $i<sizeof($tmp_folders); $i++)
		{
			list($folderID, $folderName) = $tmp_folders[$i];
			$tmp_arr[0] = $folderID;
			$tmp_arr[FolderID] = $folderID;
			$tmp_arr[1] = $folderName;
			$tmp_arr[FolderName] = $folderName;
			array_push($folders,$tmp_arr);
		}
	}
	$select_toChangefolder = getSelectByArray($folders,"name='targetFolderID' onchange=\"if (this.selectedIndex != 0) {checkAlert(document.form1,'CampusMailID[]','changefolder.php','$i_CampusMail_New_alert_moveto'); this.selectedIndex=0}\" ","-999",0,0,$button_moveto);
//}
##################


?>
<script language="javascript">
function openExport()
{
     if(confirm('<?=$i_campusmail_alert_export?>'))
     {
         newWindow('export.php?UserFolderID=<?=urlencode($FolderID)?>',4);
     }
}

<?php
if($FolderID == 2 && $_SESSION['intranet_iMail_archived']!=1){ // Inbox
?>
function getMailCount()
{
	var limit = <?=(isset($sys_custom['iMail']['ArchiveMailNumber']) && $sys_custom['iMail']['ArchiveMailNumber']>0)? $sys_custom['iMail']['ArchiveMailNumber'] : '3000'?>;
	$.get(
		"aj_archive_mail_task.php",
		{
			"Action":"GetMailCount",
			"FolderID":"<?=intranet_htmlspecialchars($FolderID)?>"
		},
		function(response){
			if(response>limit){
				getArchiveMailMessageLayer();
			}
		}
	);
}

function getArchiveMailMessageLayer()
{
	$.get(
		"aj_archive_mail_task.php",
		{
			"Action":"GetArchiveMailMessageLayer"
		},
		function(ReturnData){
			$('body').prepend(ReturnData);
			var pos = $('#html_body_frame').position();
			$('div#sub_layer_imail_archive_mail_v30').css(
				{
					'position':'absolute',
					'left': pos.left+20,
					'top': pos.top+20
				}
			);
		}
	);
}

function goProceed()
{
	window.location = "./archive_mail.php";
}

function setRemindLater()
{
	$.get(
		"aj_archive_mail_task.php",
		{
			"Action":"SetRemindLater"
		},
		function(ReturnData){
			$('div#sub_layer_imail_archive_mail_v30').remove();
		}
	);
}

$(document).ready(function(){
	getMailCount();
});

<?php
}
?>
</script>

<form name="form1" method="POST" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td >
		<br />
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="table-action-bar">
			<td>
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td>
				<p><?=$exportBtn?></p>
				</td>
			</tr>
			</table>
			</td>		
			<td align="right" valign="bottom">
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td>
				<?=$select_mailtype?>
				<?=$select_toChangefolder?>
				</td>
				<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
	<?php if(!$sys_custom['iMailHideDeleteAllButton']) { ?>		
				<td><?=$deleteAllBtn?></td>
	<?php } ?>
				<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
				<td><p><?=$deleteBtn?></p></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>		
		</td>
	</tr>
	<tr>
		<td width="100%" >
		<?=$li->display()?>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="FolderID" value="<?=escape_double_quotes($FolderID)?>" />
</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>