<?php
// Editing by 
/*
 * 2013-04-08 (Carlos): validate reply to email address before submit form
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lwebmail = new libwebmail();
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}
//$noWebmail = false;


if ($UserID=="")
{
    header("Location: index.php");
    exit();
}
if($TabID=="") $TabID=1;
$li = new libdb();
$sql = "SELECT DisplayName,ReplyEmail,DaysInSpam,DaysInTrash FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
$temp = $li->returnArray($sql,4);
list($display_name,$reply_email,$days_in_spam,$days_in_trash) = $temp[0];

# create days in trash / spam selection box
$days_in_spam = ($days_in_spam==""||$days_in_spam==0)?-1:$days_in_spam;
$days_in_trash= ($days_in_trash==""||$days_in_trash==0)?-1:$days_in_trash;

$days = array(1,2,3,4,5,6,7,14,21,30,60);
$select_trash_days = "<select name='days_in_trash'  >";
$select_trash_days.="<option value='-1' ".($days_in_trash==-1?" SELECTED ":"").">$i_CampusMail_New_Settings_Forever</option>";
$select_spam_days = "<select name='days_in_spam'  >";
$select_spam_days.="<OPTION value='-1'".($days_in_trash==-1?" SELECTED ":"").">$i_CampusMail_New_Settings_Forever</option>";

for($i=0;$i<sizeof($days);$i++){
	$select_trash_days.="<option value='".$days[$i]."'".($days_in_trash==$days[$i]?" SELECTED ":"").">".$days[$i]."</option>";
	$select_spam_days.="<option value='".$days[$i]."'".($days_in_spam==$days[$i]?" SELECTED ":"").">".$days[$i]."</option>";
}
$select_trash_days.="</select>";
$select_spam_days.="</select>";

$navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;


$CurrentPage = "PageSettings_PersonalSetting";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_CampusMail_New_Settings_PersonalSetting, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = $linterface->GET_SYS_MSG("update");
} else {
	$xmsg = "&nbsp;";
}

?>
<script language="javascript">
function checkform(obj)
{
	var reply_email = obj.reply_email.value.Trim();
	obj.reply_email.value = reply_email;
	if(reply_email=='' || (reply_email != '' && validateEmail(obj.reply_email,'<?=$Lang['AccountMgmt']['ErrorMsg']['InvalidEmail']?>'))) {
		obj.submit();
	}
}
</script>
<form name="form1" method="get" action="pref_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td colspan="2"  align="right" ><?=$xmsg?></td>
	</tr>
	<?php 
	if(!$noWebmail)
	{
	?>
		<tr>
			<td valign="top" class="formfieldtitle tabletext" ><?=$i_CampusMail_New_Settings_DisplayName?></td>
	       	<td class="tabletext">
	       	<input type="text" name="display_name" value="<?=$display_name?>" class="textboxtext"  ><br />
	       	<?=$i_CampusMail_New_Settings_NoticeDefaultDisplayName?>
	       	</td>
		</tr>
		<tr>
			<td valign="top" class="formfieldtitle tabletext" ><?=$i_CampusMail_New_Settings_ReplyEmail?></td>
			<td class="tabletext" ><input type="text" name="reply_email" value="<?=$reply_email?>" class="textboxtext"  ><br />
			<?=$i_CampusMail_New_Settings_NoticeDefaultReplyEmail?>
			</td>
		</tr>
		<!--<TR><TD ALIGN=RIGHT><?=$i_CampusMail_New_Settings_DaysInSpam?> :</TD><TD><?=$select_spam_days?></TD></TR>-->
		<tr>
			<td class="formfieldtitle tabletext" ><?=$i_CampusMail_New_Settings_DaysInTrash?></td>
			<td width="70%" class="tabletext" ><?=$select_trash_days?></td>
		</tr>
		<? if($webmail_info['bl_spam']){ ?>
		<tr>
			<td class="formfieldtitle tabletext" ><?=$i_CampusMail_New_Settings_DaysInSpam?></td>
			<td width="70%" class="tabletext" ><?=$select_spam_days?></td>
		</tr>
		<? } ?>
	<?php 
	} else { 
	?>
		<tr>
			<td class="formfieldtitle tabletext" ><?=$i_CampusMail_New_Settings_DaysInTrash?></td>
			<td width="70%" ><?=$select_trash_days?></td>
		</tr>
	<?php 
	} 
	?>				
	</table>   	      
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "checkform(this.form);return false;") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "button", "this.form.reset(); return false;") ?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="TabID" value=<?=$TabID?> />
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>