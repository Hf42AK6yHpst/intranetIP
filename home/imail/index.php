<?php
/*
 * 2015-11-30 (Carlos): replace session related functions with xxxx_intranet defined in lib.php
 */
include_once("../../includes/global.php");
$go_checkmail = false;

if (session_is_registered_intranet("intranet_iMail_checked") && $intranet_iMail_checked==1)
{
    include_once("../../includes/libdb.php");
    include_once("../../includes/libuser.php");
    include_once("../../includes/libwebmail.php");
    intranet_opendb();
    $lu = new libuser($UserID);
    #$lwebmail = new libwebmail();

    $lwebmail = new libwebmail();
    if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
    
    if (!$noWebmail)
    {
         $inbox = $lwebmail->openInbox($lu->UserLogin,$lu->UserPassword);
         $mailCount = $lwebmail->checkMailCount();
         if ($mailCount == 0)
         {
             $go_checkmail = false;
         }
         else
         {
             $go_checkmail = true;
         }
         intranet_closedb();
    }
    else
    {
        $go_checkmail = false;
        session_register_intranet("intranet_iMail_checked", 1);
        $intranet_iMail_checked = 1;
    }

}
else
{
    $go_checkmail = true;
    session_register_intranet("intranet_iMail_checked", 1);
    $intranet_iMail_checked = 1;
}

if ($go_checkmail)
{
    header("Location: checkmail.php");
}
else
{
    header("Location: viewfolder.php");
}
exit();
?>
