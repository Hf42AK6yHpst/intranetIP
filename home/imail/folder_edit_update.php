<?php
// Editing by 
/*
 * 2019-06-06 Carlos: Added CSRF token checking.
 */
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libcampusmail.php");
include_once("../../lang/lang.$intranet_session_language.php");

if(!verifyCsrfToken($csrf_token, $csrf_token_key)){
	header("Location: /");
	exit;
}

intranet_auth();
intranet_opendb();
auth_campusmail();

$lc = new libcampusmail();

$FolderID = IntegerSafe($FolderID);
$FolderName = intranet_htmlspecialchars($FolderName);

# Check existing
$sql = "SELECT FolderID FROM INTRANET_CAMPUSMAIL_FOLDER WHERE FolderID = '$FolderID' AND OwnerID = '$UserID' AND RecordType = 1";
$temp = $lc->returnVector($sql);

if (sizeof($temp)==0)
{
    header("Location: folder.php");
    intranet_closedb();
    exit();
}
else
{
    $sql = "UPDATE INTRANET_CAMPUSMAIL_FOLDER
                   SET FolderName = '$FolderName', DateModified = now()
            WHERE FolderID = '$FolderID'";
    $lc->db_db_query($sql);
}
intranet_closedb();
header("Location: folder.php?msg=2");
?>