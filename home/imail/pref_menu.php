<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

 $anc = $special_option['no_anchor'] ?"":"#anc";
$lwebmail = new libwebmail();
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once("../../includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}
?>

<?php
$img_on_right ="<img src='$image_path/frontpage/imail/tab_menu_on_right.gif'>";
$img_off_right="<img src='$image_path/frontpage/imail/tab_menu_off_right.gif'>";

$tab_personal_setting = ($TabID==1)?"tabon":"taboff";
$img_personal_setting = ($TabID==1)?$img_on_right:$img_off_right;
$link_personal_setting= ($TabID==1)?"$i_CampusMail_New_Settings_PersonalSetting":"<a href='pref.php?TabID=1$anc'>$i_CampusMail_New_Settings_PersonalSetting</a>";

$tab_signature = ($TabID==2)?"tabon":"taboff";
$img_signature = ($TabID==2)?$img_on_right:$img_off_right;
$link_signature= ($TabID==2)?"$i_CampusMail_New_Settings_Signature":"<a href='pref_signature.php?TabID=2$anc'>$i_CampusMail_New_Settings_Signature</a>";

$tab_email_forwarding = ($TabID==3)?"tabon":"taboff";
$img_email_forwarding = ($TabID==3)?$img_on_right:$img_off_right;
$link_email_forwarding = ($TabID==3)?"$i_CampusMail_New_Settings_EmailForwarding":"<a href='pref_email_forwarding.php?TabID=3$anc'>$i_CampusMail_New_Settings_EmailForwarding</a>";
?>
<style type="text/css">
<!--
.tabon {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 13px;
	font-weight: bold;
	text-decoration: underline;
	vertical-align: middle;
	background:url(/images/frontpage/imail/tab_menu_on_bg.gif);
}
.taboff {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #663300;
	text-decoration: none;
	vertical-align: middle;
	background:url(/images/frontpage/imail/tab_menu_off_bg.gif);
}
.taboff:hover {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #663300;
	text-decoration: underline;
	vertical-align: middle;
	background:url(/images/frontpage/imail/tab_menu_off_bg.gif);
}

-->
</style>
<a name=anc></a>
<TABLE cellSpacing=0 cellPadding=0 width=685 border=0>
  <TBODY>
    <TR> 
      <TD> <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td><IMG height=10 src="<?=$image_path?>/frontpage/imail/papertop2.gif" 
          width=685></td>
          </tr>
          <tr> 
            <td><table background="<?=$image_path?>/frontpage/imail/tab_menu_bg.gif" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="8"><img src="<?=$image_path?>/frontpage/imail/tab_menu_left.gif" width="8" height="32"></td>
                  <td><table border="0" cellpadding="0" cellspacing="0">
                      <tr> 
                        <!-- general setting -->
                        <td width="8" class="<?=$tab_personal_setting?>"><img src="<?=$image_path?>/frontpage/imail/spacer.gif" width="8" height="32"></td>
                        <td class="<?=$tab_personal_setting?>"><?=$link_personal_setting?></td>
	                        <td width="26" class="<?=$tab_signature?>"><?=$img_personal_setting?></td>
	                        <!-- Signature -->
	                        <td class="<?=$tab_signature?>"><img src="<?=$image_path?>/frontpage/imail/spacer.gif" width="8" height="32"></td>
	                        <td class="<?=$tab_signature?>"><?=$link_signature?></td>
             	            <?php if(!$noWebmail){?>
	                        	<td width="26" class="<?=$tab_email_forwarding?>"><?=$img_signature?></td>
		                        <!-- Email forwarding -->
		                        <td width="8" class="<?=$tab_email_forwarding?>"><img src="<?=$image_path?>/frontpage/imail/spacer.gif" width="8" height="32"></td>
		                        <td class="<?=$tab_email_forwarding?>"><?=$link_email_forwarding?></td>
		                        <td width="32"><?=$img_email_forwarding?></td>
	                        <?php }else{ ?>
								<td width="32"><?=$img_signature?></td>
	                        <?php }?>
                      </tr>
                    </table>
                    </td>
                  <td width="8"><img src="<?=$image_path?>/frontpage/imail/spacer.gif" width="8" height="32"></td>
                </tr>
              </table></td>
          </tr>
        </table></TD>
    </TR>
  </TBODY>
</TABLE>

<!--<img src=/images/campusmail/papertop.gif width=685 height=10>-->
