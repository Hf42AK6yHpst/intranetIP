<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libcampusmail.php");
include("../../includes/libfilesystem.php");
include("../../includes/libfiletable.php");
include("../../includes/libcampusquota.php");
include("../../includes/libaccess.php");
intranet_opendb();
auth_campusmail();

$Subject = intranet_htmlspecialchars(trim($Subject));
$Message = intranet_htmlspecialchars(trim($Message));
$RecipientID = implode(",", $Recipient);

$li = new libcampusmail($CampusMailID);

$actual_recipient_array = $li->returnRecipientUserIDArray($RecipientID);
if (sizeof($actual_recipient_array)==0)
{
    header("Location: draft_edit.php?CampusMailID=$CampusMailID&cmsg=1");
    exit();
}

$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$path = "$file_path/file/mail/".$li->Attachment;
$lo = new libfiletable("", $path, 0, 0, "");
$lu = new libfilesystem();
$files = $lo->files;
while (list($key, $value) = each($files)) {
     if(!strstr($AttachmentStr,$files[$key][0])){
          $lu->file_remove($path."/".$files[$key][0]);
     }
}

$IsAttachment = (isset($Attachment)) ? 1 : 0;
$RecordType = $SubmitType;

# Check attachment size
if ($IsAttachment)
{
    # compute size of attachment
    $lsize = new libfiletable("",$path, 0,0,"");
    $files = $lsize->files;
    $size = 0;
    while (list($key, $value) = each($files)) {
           $thisSize = ceil(filesize($path."/".$files[$key][0]) /1000);
           $size += round($thisSize, 1);
    }

}
else
{
    if ($li->Attachment != "")
    {
        $lu->lfs_remove($path);
    }
    $size = 0;
}


$sql = "
     UPDATE INTRANET_CAMPUSMAIL
     SET RecipientID = '$RecipientID',
          Subject = '$Subject',
          Message = '$Message',
          IsAttachment = '$IsAttachment',
          IsImportant  = '$IsImportant',
          IsNotification = '$IsNotification',
          RecordType = '$RecordType',
          AttachmentSize = '$size',
          DateModified = now()
     WHERE CampusMailID = '$CampusMailID' AND UserID = '$UserID'
     ";
$li->db_db_query($sql);

if($SubmitType==0){

     $replies_values = "";
     $delimiter = "";
     $users_no_space = array();
     $users_count = 0;


     # return all recipients UserID
     #$sql = "INSERT INTO INTRANET_CAMPUSMAIL (CampusMailFromID, UserID, SenderID, RecipientID, Subject, Message, Attachment, IsAttachment, IsImportant, IsNotification, RecordType, DateInput, DateModified, AttachmentSize) VALUES ";
     $campusmail_sql_header = "INSERT INTO INTRANET_CAMPUSMAIL (CampusMailFromID, UserID, RecordType,RecordStatus) VALUES ";
     $replies_sql_header = "INSERT INTO INTRANET_CAMPUSMAIL_REPLY (CampusMailID, UserID, UserName,IsRead,DateInput,DateModified) VALUES ";
     $sql = $campusmail_sql_header;

     $row = $actual_recipient_array;
        //get all user's quota
        $ReceiversList = 0;
          for($i=0; $i<sizeof($row); $i++){
          $ReceiversList .= ",".$row[$i][0];
          }

        $sql_quota = "SELECT UserID, SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE RecordType IN (0,1,2,3,4,5) AND UserID IN ($ReceiversList) GROUP BY UserID";
        $row_quota = $li->returnArray($sql_quota, 2);
        for ($i=0; $i<sizeof($row_quota); $i++)
        {
                $user_quota_used[$row_quota[$i][0]] = (int)$row_quota[$i][1];
        }
     $lquota = new libcampusquota();
     $TotalQuota = $lquota->QuotaArray;

     for($i=0; $i<sizeof($row); $i++){
          $ReceiverID = $row[$i][0];
          $ReceiverName = $row[$i][1];
          $ReceiverType = $row[$i][2];
          $ReceiverQuota = $TotalQuota[$ReceiverType-1];

          #$lcTarget = new libcampusquota($ReceiverID);
                  //optimized
          //$left = $lcTarget->returnQuota()*1024 - $lcTarget->returnUsedQuota();
          $left = (is_integer($user_quota_used[$ReceiverID])) ? $ReceiverQuota*1024 - $user_quota_used[$ReceiverID] : $ReceiverQuota*1024;
          if ($size > $left)
          {
              $users_no_space[$users_count] = $ReceiverID;
              $users_count++;
              continue;
          }
          # CampusMailFromID, UserID, RecordType only
          $sql .= "$delimiter($CampusMailID, $ReceiverID, '2',1)";
          $replies_values .= "$delimiter($CampusMailID,$ReceiverID,'$ReceiverName',0,now(),now())";
          $delimiter = ",";
     }
     $li->db_db_query($sql);
     # Update all mails just inserted
     $sql = "UPDATE INTRANET_CAMPUSMAIL SET
             SenderID = $UserID, RecipientID = '$RecipientID',Subject='$Subject',Message='$Message'
             ,Attachment='".$li->Attachment."', IsAttachment='$IsAttachment'
             ,IsImportant='$IsImportant', IsNotification = '$IsNotification',RecordStatus = NULL
             ,AttachmentSize = '$size', DateInput = now(), DateModified = now()
             WHERE CampusMailFromID = '$CampusMailID'";
     $li->db_db_query($sql);

     if ($IsNotification)
     {
         $replies_sql = "INSERT INTO INTRANET_CAMPUSMAIL_REPLY (CampusMailID, UserID, UserName,IsRead,DateInput,DateModified) VALUES $replies_values";
         $li->db_db_query($replies_sql);
     }
     # Display undelivery
     if ($users_count != 0)
     {
         $username_field = getNameFieldWithClassNumberByLang("");
         $sql = "SELECT $username_field FROM INTRANET_USER WHERE UserID IN (".implode(",",$users_no_space).")";
         $list = $li->returnVector($sql);
         $x = "";
         for ($i=0; $i<sizeof($list); $i++)
         {
              $x .= "<tr><td></td><td>".$list[$i]."</td></tr>\n";
         }
         $navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;
         include("../../lang/lang.$intranet_session_language.php");
         include ("../../templates/homeheader.php");
?>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td><?=$i_frontpage_campusmail_title_compose?></td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="474" valign="top">
          <?php include("menu.php"); ?>
          </td>
          <td width="320" valign="top" align="center">
          </td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="66" valign="top">
            <p><br>
              <?=$i_campusmail_lettertext?><br>
              <br>
              <br>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <img src="/images/campusmail/letterbottom.gif"> </p>
          </td>
          <td width="728" align="left" valign="top">
            <table width=685 border=0 cellspacing=0 cellpadding=0>
              <tr>
                <td><img src=/images/campusmail/papertop2.gif></td>
              </tr>
            </table>
            <table width=685 border=0 cellpadding=0 cellspacing=0 background=/images/campusmail/paperbg3.gif class=body>
              <tr>
                <td width=80 align=right valign=top></td>
                <td width=565 align=left valign=top><?=$i_campusmail_nondelivery?></td>
              </tr>
            </table>
            <table height=150 width=685 border=0 cellpadding=0 cellspacing=0 background=/images/campusmail/paperbg4.gif class=body>
              <tr>
                <td width=80 align=right valign=top></td>
                <td width=605>&nbsp;</td>
              </tr>
              <?=$x?>
            </table>
            <table width=685 border=0 cellspacing=0 cellpadding=0>
              <tr>
                <td><img src=/images/campusmail/paperbottom2.gif></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

<?
         include ("../../templates/homefooter.php");
     }
}

$url = ($SubmitType==0) ? "outbox.php?signal=1" : "draft.php?signal=2";

intranet_closedb();
if ($users_count == 0)
{
    header("Location: $url");
}
?>