<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libaccess.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field=="") $field = 3;
$RecordType = 1;
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.IsImportant", "a.IsNotification", "a.IsAttachment", "a.DateInput", "a.Subject","a.AttachmentSize");
$sql  = "SELECT
               IF(a.IsImportant=1,'$i_frontpage_campusmail_icon_important','<br>'),
               IF(a.IsNotification=1,'$i_frontpage_campusmail_icon_notification','<br>'),
               IF(a.IsAttachment=1,'$i_frontpage_campusmail_icon_attachment','<br>'),
               DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),
               ' ',
               ";
if (auth_sendmail())
{
    $sql .= "CONCAT('<a href=draft_edit.php?CampusMailID=', a.CampusMailID, '>', a.Subject, '</a>'),";
}
else
{
    $sql .= "a.Subject,";
}

$sql .= "
               ' ',
               IF (a.AttachmentSize IS NULL, 0, a.AttachmentSize),
               CONCAT('<input type=checkbox name=CampusMailID[] value=', a.CampusMailID ,'>')
          FROM INTRANET_CAMPUSMAIL AS a
          WHERE
               a.UserID = '$UserID' AND
               a.RecordType = '$RecordType' AND
               (a.Subject like '%$keyword%' OR a.Message like '%$keyword%')
          ";
# TABLE INFO
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2+2;
$li->IsColOff = 1;
$li->title = "";
// TABLE COLUMN

/*
$li->column_list .= "<td width=23 class=campusmail_frame2>".$li->column_image(0, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important)."</td>\n";
$li->column_list .= "<td width=27 class=campusmail_frame2>".$li->column_image(1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification)."</td>\n";
$li->column_list .= "<td width=20 class=campusmail_frame2>".$li->column_image(2, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_icon_attachment)."</td>\n";
$li->column_list .= "<td width=100 class=campusmail_frame2>".$li->column(3, $i_frontpage_campusmail_date)."</td>\n";
$li->column_list .= "<td width=370 class=campusmail_frame2>".$li->column(4, $i_frontpage_campusmail_subject)."</td>\n";
$li->column_list .= "<td width=75 class=campusmail_frame2>".$li->column(5, $i_frontpage_campusmail_size)."</td>\n";
$li->column_list .= "<td width=40 class=campusmail_frame2>".$li->check("CampusMailID[]")."</td>\n";
*/
$li->column_list .= "<td width=23 bgcolor=#FFC29F>".$li->column_image(0, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important)."</td>\n";
$li->column_list .= "<td width=27 bgcolor=#FFC29F>".$li->column_image(1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification)."</td>\n";
$li->column_list .= "<td width=20 bgcolor=#FFC29F>".$li->column_image(2, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_icon_attachment)."</td>\n";
$li->column_list .= "<td width=100 bgcolor=#FFC29F>".$li->column(3, $i_frontpage_campusmail_date)."</td>\n";
$li->column_list .= "<td width=2 bgcolor=#FFC29F>&nbsp;&nbsp;</td>\n";
$li->column_list .= "<td width=366 bgcolor=#FFC29F>".$li->column(4, $i_frontpage_campusmail_subject)."</td>\n";
$li->column_list .= "<td width=2 bgcolor=#FFC29F>&nbsp;&nbsp;</td>\n";
$li->column_list .= "<td width=75 bgcolor=#FFC29F>".$li->column(5, $i_frontpage_campusmail_size)."</td>\n";
$li->column_list .= "<td width=40 bgcolor=#FFC29F>".$li->check("CampusMailID[]")."</td>\n";

$li->column_array = array(0,0,0,0,0,5,0,0);
$li->wrap_array = array(0,0,0,0,0,30,0,0);
// TABLE FUNCTION BAR
$functionbar .= "<a href=javascript:checkRemove(document.form1,'CampusMailID[]','remove.php')><img src=$image_delete border=0></a>";
$functionbar .= "<a href=\"javascript:AlertPost(document.form1,'remove_all.php','$i_campusmail_alert_trashall')\"><img src=\"$image_path/btn_deleteall_$intranet_session_language.gif\" border=0></a>";
//$functionbar .= "<input class=button type=button value='$button_remove' onClick=checkRemove(this.form,'CampusMailID[]','remove.php')>";
# $searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
# $searchbar .= "<input class=submit type=submit value='$button_search'>\n";

switch ($signal)
{
        case 2: $mail_res = "<p>$i_campusmail_save_successful</p>"; break;
        default: $mail_res = "";
}


$navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;
include_once("../../templates/homeheader.php");
echo $mail_res;
?>

<form name=form1 method="get">
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td><?=$i_frontpage_campusmail_title_draft?></td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="474" valign="top">
          <?php include_once("menu.php"); ?>
          </td>
          <td width="320" valign="top" align="center">
          </td>
        </tr>
      </table>
      <?=$li->display($functionbar)?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=RecordType value="<?=$RecordType?>">
</form>

<?php
include_once("../../templates/homefooter.php");
intranet_closedb();
?>