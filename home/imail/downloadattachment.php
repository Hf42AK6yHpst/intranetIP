<?php
//using : 
/*
 * ********** [Change Log - START] **********
 * 2019-10-25 [Carlos] : Added parameter $b_filename for base64 encoded filename in url. Avoid use plain text file name in url because some word are blocked by firewall.
 * 2018-09-06 [Carlos] : Added auth_campusmail(); and check campusmail UserID equals session UserID.
 * 2013-03-28 [Carlos] : Modified to use global function output2browser()
 * 2011-11-10 [Carlos] : Modified Firefox header Content-Disposition: attachment; filename*=utf8''encoded_filename
 * 2011-03-25 [Carlos] : Optimize join conditions of fidning AttachmentPath SQL 
 * 2011-03-03 [Carlos] : Fixed finding AttachmentPath SQL
 * 
 * Date		:	2010-11-15 [Ronald]
 * Details	:	modified the logic. now will cater the case in the filename stored in DB is empty.
 *    	
 * ********** [Change Log - END] **********
 */
$PATH_WRT_ROOT = "../../";
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libcampusmail2007a.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

## Increase the php memory to 300MB ##
ini_set("memory_limit", "300M");

session_write_close();

### Check user's browser ###
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
    $browser_version=$matched[1];
    $browser = 'IE';
} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
    $browser_version=$matched[1];
    $browser = 'Opera';
} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
	$browser_version=$matched[1];
	$browser = 'Firefox';
} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
	$browser_version=$matched[1];
	$browser = 'Safari';
} else {
	// browser not recognized!
    $browser_version = 0;
    $browser = 'other';
}

$valid = true;


//if (($PartID == "") || ($CampusMailID == ""))
if(($CampusMailID == "") && (($PartID == "") || ($filename == "") || ($b_filename == "")) )
{
    $valid = false;
}
else
{
    $li = new libdb();
    $lcampusmail = new libcampusmail2007($CampusMailID);
    
    if($lcampusmail->UserID != $_SESSION['UserID']){
    	$valid = false;
    	intranet_closedb();
    	exit;
    }
    
    if(isset($b_filename) && $b_filename != ''){
    	$filename = base64_decode($b_filename);
    }
    
    if($filename != ""){
    	//$sql = "SELECT DISTINCT AttachmentPath
        //          FROM INTRANET_IMAIL_ATTACHMENT_PART as a
        //               LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON b.UserID = $UserID 
		//				AND IF(b.CampusMailFromID IS NOT NULL AND b.CampusMailFromID <> '',a.CampusMailID = b.CampusMailFromID,a.CampusMailID = b.CampusMailID)
        //          WHERE b.CampusmailID = $CampusMailID AND b.UserID IS NOT NULL
        //    ";
        $sql = "SELECT DISTINCT AttachmentPath
                  FROM INTRANET_IMAIL_ATTACHMENT_PART as a
                       LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON b.UserID = '$UserID' AND 
						(
							(b.CampusMailFromID IS NOT NULL AND b.CampusMailFromID <> '' AND a.CampusMailID = b.CampusMailFromID)
							OR
							((b.CampusMailFromID IS NULL OR b.CampusMailFromID = '') AND a.CampusMailID = b.CampusMailID)
						)
                  WHERE b.CampusmailID = '$CampusMailID' AND b.UserID IS NOT NULL
            ";
        $arrFilePath = $li->returnVector($sql);
        
        if (sizeof($arrFilePath)==0)
	    {
	        $valid = false;
	    }
    }
    else
    {
    	//$sql = "SELECT AttachmentPath, FileName
        //          FROM INTRANET_IMAIL_ATTACHMENT_PART as a
        //               LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON b.UserID = $UserID 
		//				AND IF(b.CampusMailFromID IS NOT NULL AND b.CampusMailFromID <> '',a.CampusMailID = b.CampusMailFromID,a.CampusMailID = b.CampusMailID)
        //          WHERE PartID = $PartID AND b.UserID IS NOT NULL
        //    ";
        $sql = "SELECT AttachmentPath, FileName
                  FROM INTRANET_IMAIL_ATTACHMENT_PART as a
                       LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON b.UserID = '$UserID' AND 
						(
							(b.CampusMailFromID IS NOT NULL AND b.CampusMailFromID <> '' AND a.CampusMailID = b.CampusMailFromID)
							OR
							((b.CampusMailFromID IS NULL OR b.CampusMailFromID = '') AND a.CampusMailID = b.CampusMailID)
						)
                  WHERE PartID = '$PartID' AND b.UserID IS NOT NULL
            ";
        $files = $li->returnArray($sql,2);
        
        if (sizeof($files)==0)
	    {
	        $valid = false;
	    }
    }
}
intranet_closedb();

# functions for handling filename parameter for "Content-Disposition" in header() function.

# conver utf-8 octets (eg. &#12345) to the represented chars in UTF-8
function octets2char( $input ) {
	if(!function_exists("octets2char_callback")){
         function octets2char_callback($octets) {
                   $dec = $octets[1];
		           if ($dec < 128) {
		             $utf8Substring = chr($dec);
		           } else if ($dec < 2048) {
		             $utf8Substring = chr(192 + (($dec - ($dec % 64)) / 64));
		             $utf8Substring .= chr(128 + ($dec % 64));
		           } else {
		             $utf8Substring = chr(224 + (($dec - ($dec % 4096)) / 4096));
		             $utf8Substring .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
		             $utf8Substring .= chr(128 + ($dec % 64));
		           }
		           return $utf8Substring;
        }
    }
  return preg_replace_callback('|&#([0-9]{1,});|', 'octets2char_callback', $input );                                
  
}

# convert the string with octets to represted chars in UTF-8 form
function utf8Encode2($source) {
   $utf8Str = '';
   $entityArray = explode ("&#", $source);
   $size = count ($entityArray);
   for ($i = 0; $i < $size; $i++) {
       $subStr = $entityArray[$i];
       $nonEntity = strpos ($subStr, ';');
       if ($nonEntity !== false) {
			$str = substr($subStr,0,$nonEntity);
			$str = "&#".$str.";";
			$str_end=substr($subStr,$nonEntity+1);

			$convertedStr=rawurlencode(octets2char($str));
			
			$utf8Str.=$convertedStr.$str_end;
       }
       else {
           $utf8Str .= $subStr;
       }
   }
   return $utf8Str;

}

# end functions

if ($valid)
{
	if(sizeof($files)>0)
	{
		list($attachment_path, $filename) = $files[0];
		$filename = htmlspecialchars_decode(stripslashes($filename));
		$target_filepath = "$intranet_root/file/mail/$attachment_path/$filename";
	}
	else
	{
		$attachment_path = $arrFilePath[0];
		$filename = htmlspecialchars_decode(stripslashes($filename));
		$target_filepath = "$intranet_root/file/mail/$attachment_path/$filename";
	} 
    
    if (!is_file($target_filepath))
    {
         die("file not exists");
    }
    
    $content = get_file_content($target_filepath);
    $encoded_name = urlencode($filename);
    
    if ($encoded_name == $filename && false)
    {
        $url = "/file/mail/$attachment_path/$filename";
    ?>
<html>
<head>
        <meta http-equiv="REFRESH" content="0; URL=<?=$url?>">
        <title><?=$filename?></title>
</head>
</html>
    <?
    }
    else
    {
		## check if file valid
		$valid_file = false;
        clearstatcache();

        if(file_exists($target_filepath) && is_file($target_filepath)){
                $fd = fopen($target_filepath, "r");
                if ($fd){
                    fclose($fd);
                    $valid_file = true;
                }
        }
                
		if($valid_file)
		{
	         $content_type = "application/octet-stream";
	         
	         /*
	         header("Pragma: public");
	         header("Expires: 0"); // set expiration time
	         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	         header("Content-type: $content_type");
	         header("Content-Length: ".strlen($content));
	         
	        $encoded_filename = urlencode($filename);
			$encoded_filename = str_replace("+", "_", $encoded_filename);
			
			if ($browser == "IE") 
			{
				$encoded_filename = rawurlencode($filename);
				header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
			} 
			else if ($browser == "Firefox")
			{
				//header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
				// http://tools.ietf.org/html/rfc6266 : no double quote in filename*=utf8''filename_should_be_utf8_encoded
				header('Content-Disposition: attachment; filename*=utf8\'\'' . $encoded_filename);
			}
			else 
			{
				header('Content-Disposition: attachment; filename="' . $filename . '"');
			}
			*/
	        ### New method - user fread to get attachment content
	        ### because fgets cannot read .mp4 file
			clearstatcache();
            if(file_exists($target_filepath) && is_file($target_filepath) && filesize($target_filepath)!=0){
               $buffer =  ($fd = fopen($target_filepath, "r")) ? fread($fd,filesize($target_filepath)) : "";
               if ($fd)
                   fclose ($fd);
            }
	        //echo $buffer;
	        
	        output2browser($buffer, $filename,$content_type);
	        
     	}
	}
}
else
{
    echo "Bad Attachment/Mail already removed";
}
?>