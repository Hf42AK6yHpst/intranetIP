<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libdb();

$sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$_SESSION['UserID']."'";
$sum_record = $li->returnVector($sql);
$used_quota = round(intval($sum_record[0]),1);
$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed, RequestUpdate) VALUES ('".$_SESSION['UserID']."','".$used_quota."','0') ON DUPLICATE KEY UPDATE QuotaUsed = '$used_quota',RequestUpdate='0'";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT WHERE UserID='".$_SESSION['UserID']."'";
$li->db_db_query($sql);

echo $used_quota;

intranet_closedb();
?>