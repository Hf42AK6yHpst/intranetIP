<?php
//using by : 
/******************************************* Change Log ***********************************************
 * 2019-12-09 (Sam): Remove extra empty recipient displayed in iMail
 * 2017-08-15 (Carlos): $sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember'] - do not show categories without any group member, avoid selected groups but no group member inside. (Copied from iMail+)
 * 2017-05-29 (Carlos): $sys_custom['iMailHideAddSubCategoryButton'] hide sub-category/group second level [Add] button to avoid abuse use.
 * 2016-11-04 (Carlos): added getUserDisplayOrderField($prefix="",$suffix="") and select user queries to display greater priority users on top.
 * 2016-05-27 (Carlos): $sys_custom['iMailSelectRecipientAutoExpandSubCategory'] N95858 景嶺書院 request to auto expand the sub-category selections 
 * 						for student users, in order to avoid students mistakenly send to all selections. 
 * 2013-04-19 (Yuen): improve for Alumni for having filter by Year Of Left
 * 2012-08-21 (Carlos): Added $sys_custom['iMail']['SelectRecipientType'] to customize display and display order of [Identity] and [Group]
 * 2011-11-21 (Carlos): Added $sys_custom['iMailShowAllGroupsWithIdentityAll'] to cater when have mail target Identity-All will include to group options
 * 2011-10-20 (Carlos): Added Identity type Alumni
 * 2011-08-24 (Carlos): Replace INTRANET_GROUP Title with getGroupTitleByLang()
 * 2011-02-23 (Carlos): Modified show [Group] option only if [SSV_USER_TARGET][All-Yes] or [SSV_USER_TARGET][Identity-MyGroup] is on  
 * 2011-01-04 (Carlos): Fix problem of Identity > Parent > All Parents some parent names are blank
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

function getUserDisplayOrderField($prefix="",$suffix="")
{
	global $sys_custom;
	
	if($sys_custom['iMail']['UserDisplayOrder']){
		return $prefix."DisplayOrder".$suffix;
	}else{
		return "";
	}
}

## either called from Compose Page OR AddressBook > Internal Recipient Group
$caller = $AliasID==""?"compose":"addressbook";

$autoExpandSubCat = $sys_custom['iMailSelectRecipientAutoExpandSubCategory'] && ($_SESSION['UserType'] == USERTYPE_STUDENT || $sys_custom['iMailHideAddSubCategoryButton']);

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
$li = new libuser($UserID);
$lrole = new role_manage();
$fcm = new form_class_manage();
$lgrouping = new libgrouping();
$lwebmail = new libwebmail();

$name_field = getNameFieldWithClassNumberByLang("a.");
$identity = $lrole->Get_Identity_Type($UserID);
$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

## get toStaff, toStudent, toParent options - IP25 only ##
$result_to_options['ToTeachingStaffOption'] = 0;
$result_to_options['ToNonTeachingStaffOption'] = 0;
$result_to_options['ToStudentOption'] = 0;
$result_to_options['ToParentOption'] = 0;
$result_to_options['ToAlumniOption'] = 0;

## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
$result_to_options['ToTeacherAndStaff'] = 0;
if($sys_custom['iMail_RecipientCategory_StaffAndTeacher'])
{
	if(($identity == "Teaching") || ($identity == "NonTeaching"))
	{
		$result_to_options['ToTeacherAndStaff'] = 1;
	}
}

if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup']))
{
	if($sys_custom['iMail_RemoveTeacherCat']) {
		$result_to_options['ToTeachingStaffOption'] = 0;
	} else {
		$result_to_options['ToTeachingStaffOption'] = 1;
	}
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']))
{
	if($sys_custom['iMail_RemoveNonTeachingCat']) {
		$result_to_options['ToNonTeachingStaffOption'] = 0;
	} else {
		$result_to_options['ToNonTeachingStaffOption'] = 1;
	}
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Student-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup']))
{
	$result_to_options['ToStudentOption'] = 1;
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup']))
{
	$result_to_options['ToParentOption'] = 1;
}
if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['All-Yes'] || $_SESSION['SSV_USER_TARGET']['Alumni-All']) && is_array($webmail_identity_allowed) && in_array(USERTYPE_ALUMNI, $webmail_identity_allowed))
{
	$result_to_options['ToAlumniOption'] = 1;
}

### Check can send to Group ### 
/*if(	($_SESSION['SSV_USER_TARGET']['All-Yes']) || 
	($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) ||
	($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) ||
	($_SESSION['SSV_USER_TARGET']['Student-All']) ||
	($_SESSION['SSV_USER_TARGET']['Parent-All']) )*/
if(	($_SESSION['SSV_USER_TARGET']['All-Yes']) || 
	($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) || 
	($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']) || 
	(/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || 
	(/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || 
	(/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Student-All']) || 
	(/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Parent-All'])
)
{
	$result_to_options['ToGroupOption'] = 1;
	
	$result_to_group_options['ToTeacher'] = 0;
	$result_to_group_options['ToStaff'] = 0;
	$result_to_group_options['ToStudent'] = 0;
	$result_to_group_options['ToParent'] = 0;
	$result_to_group_options['ToAlumni'] = 0;
	
	if($_SESSION['SSV_USER_TARGET']['All-Yes']){
		$result_to_group_options['ToTeacher'] = 1;
		$result_to_group_options['ToStaff'] = 1;
		$result_to_group_options['ToStudent'] = 1;
		$result_to_group_options['ToParent'] = 1;
		if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
	}else{
		if( (/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
			$result_to_group_options['ToTeacher'] = 1;
		}
		if( (/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
			$result_to_group_options['ToStaff'] = 1;
		}
		if( (/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
			$result_to_group_options['ToStudent'] = 1;
		}
		if( (/*$sys_custom['iMailShowAllGroupsWithIdentityAll'] &&*/ $_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
			$result_to_group_options['ToParent'] = 1;
		}
		if($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'] ){
			$result_to_group_options['ToAlumni'] = 1;
		}
	}
}

$AlumniNoTargetCond = true;
if($special_feature['alumni'])
	$AlumniNoTargetCond = $_SESSION['SSV_USER_TARGET']['Alumni-All'] == '' && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'] == '';
### If no mail targeting is set in the front-end, than will assign some default targeting to user ###
if( ($_SESSION['SSV_USER_TARGET']['All-Yes'] == '') && 
($_SESSION['SSV_USER_TARGET']['All-No'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-All'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyGroup'] == '') &&
($_SESSION['SSV_USER_TARGET']['Parent-All'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'] == '') && 
$AlumniNoTargetCond 
)
{
	if($result_to_options['ToTeachingStaffOption'] == 0 && $result_to_options['ToNonTeachingStaffOption'] == 0 && $result_to_options['ToStudentOption'] == 0 && $result_to_options['ToParentOption'] == 0 && $result_to_options['ToAlumniOption'] == 0)
	{
		if(($identity == "Teaching") || ($identity == "NonTeaching"))
		{
			### If user is Teacher, then will have the follow targeting :
			###  - to All Teaching Staff
			###  - to All NonTeaching Staff
			###  - to All Student 
			###  - to All Parent 
			$result_to_options['ToTeachingStaffOption'] = 1;
			$result_to_options['ToNonTeachingStaffOption'] = 1;
			$result_to_options['ToStudentOption'] = 1;
			$result_to_options['ToParentOption'] = 1;
			if($special_feature['alumni']) $result_to_options['ToAlumniOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['All-Yes'] = true;
		}
		if($identity == "Student")
		{
			### If user is Student, then will have the follow targeting :
			###  - to Student Own Class Student
			###  - to Student Own Subject Group Student
			$result_to_options['ToStudentOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['Student-MyClass'] = true;	
			$_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] = true;
		}
		if($identity == "Parent")
		{
			### If user is Parent, then will have the follow targeting :
			###  - to Their Child's Own Class Teacher
			###  - to Their Child's Own Subject Group Teacher
			$result_to_options['ToTeachingStaffOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['Staff-MyClass'] = true;	
			$_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] = true;
		}
	}
	$result_to_options['ToGroupOption'] = 1;
	
	$result_to_group_options['ToTeacher'] = 0;
	$result_to_group_options['ToStaff'] = 0;
	$result_to_group_options['ToStudent'] = 0;
	$result_to_group_options['ToParent'] = 0;
	$result_to_group_options['ToAlumni'] = 0;
	
	if($_SESSION['SSV_USER_TARGET']['All-Yes']){
		$result_to_group_options['ToTeacher'] = 1;
		$result_to_group_options['ToStaff'] = 1;
		$result_to_group_options['ToStudent'] = 1;
		$result_to_group_options['ToParent'] = 1;
		if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
	}else{
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
			$result_to_group_options['ToTeacher'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
			$result_to_group_options['ToStaff'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
			$result_to_group_options['ToStudent'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
			$result_to_group_options['ToParent'] = 1;
		}
		if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'])){
			$result_to_group_options['ToAlumni'] = 1;
		}
	}
}

if($identity != "Student")
{
	$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE ParentID = '".$li->Get_Safe_Sql_Query($UserID)."'";
	$result = $li->returnVector($sql);
	if($result[0]>0)
		$result_to_options['ToMyChildrenOption'] = 1;
}
if($identity == "Student")
{
	$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE StudentID = '".$li->Get_Safe_Sql_Query($UserID)."'";
	$result = $li->returnVector($sql);
	if($result[0]>0)
		$result_to_options['ToMyParentOption'] = 1;
}

if(isset($sys_custom['iMail']['SelectRecipientType']) && is_array($sys_custom['iMail']['SelectRecipientType'])){
	$identity_group_display_order = $sys_custom['iMail']['SelectRecipientType'];
}else{
	$identity_group_display_order = array('identity','group');
}

## New 1st level selection box (By Identity / By Group)
$x1  = ($OptValue!="" && $OptValue > 0) ? "<select name='OptValue' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()' >\n" : "<select name='OptValue' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]); this.form.submit()' >\n";
$x1 .= "<option value='0' >--{$button_select}--</option>\n";

$x1_identity = "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByIdentity'],ENT_QUOTES)."'>";

# Create Cat list (Teacher, Staff, Student, Parent, Group) - IP25 Only #
## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
if ($result_to_options['ToTeacherAndStaff'])
{
	$x1_identity .= "<option value=-3 ".(($OptValue==-3)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['TeacherAndStaff']."</option>\n";
}
if ($result_to_options['ToTeachingStaffOption'])
{
    $x1_identity .= "<option value=-1 ".(($OptValue==-1)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Teacher']."</option>\n";
}
if ($result_to_options['ToNonTeachingStaffOption'])
{
    $x1_identity .= "<option value=-2 ".(($OptValue==-2)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['NonTeachingStaff']."</option>\n";
}
if ($result_to_options['ToStudentOption'])
{
    $x1_identity .= "<option value=2 ".(($OptValue==2)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Student']."</option>\n";
}
if ($result_to_options['ToParentOption'])
{
    $x1_identity .= "<option value=3 ".(($OptValue==3)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Parent']."</option>\n";
}
if ($special_feature['alumni'] && $result_to_options['ToAlumniOption'])
{
    $x1_identity .= "<option value=-4 ".(($OptValue==-4)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Alumni']."</option>\n";
}
if ($result_to_options['ToMyChildrenOption'])
{
	$x1_identity .= "<option value=5 ".(($OptValue==5)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['MyChildren']."</option>\n";
}
if ($result_to_options['ToMyParentOption'])
{
	$x1_identity .= "<option value=6 ".(($OptValue==6)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['MyParent']."</option>\n";
}
$x1_identity .= "</optgroup>";

//$lclubsenrol = new libclubsenrol();
$arrExcludeGroupCatID[] = 0;
//if(($plugin['eEnrollment'] == true) && ($lclubsenrol->isUsingYearTermBased == 1)){
//	$arrExcludeGroupCatID[] = 5;
//}
$targetGroupCatIDs = implode(",",IntegerSafe($arrExcludeGroupCatID));

if($_SESSION['SSV_USER_TARGET']['All-Yes'])
{
	//$sql = "SELECT DISTINCT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY CategoryName ASC";
		$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY as a ";
	if($sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember']){
		$sql.= " LEFT JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."') 
				LEFT JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) ";
	}
	$sql.= " WHERE a.GroupCategoryID NOT IN ($targetGroupCatIDs) ";
	if($sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember']){
		$sql.= " GROUP BY a.GroupCategoryID HAVING COUNT(c.UserID)>0 ";
	}
	$sql.= " ORDER BY a.CategoryName ASC";
}
else
{
	if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || $result_to_group_options['ToAlumni'])
	{
		//$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) WHERE c.UserID = '$UserID' AND b.RecordType != 0 AND a.GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY a.CategoryName ASC";
		$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) ";
		if($sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember']){
			$sql .= " AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' ";
		}
		$sql.= " INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID)
				 WHERE c.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.RecordType != 0 AND a.GroupCategoryID NOT IN ($targetGroupCatIDs) ";
		if($sys_custom['iMailPlusHideGroupCategoryWithoutGroupMember']){
			$sql.= " GROUP BY a.GroupCategoryID HAVING COUNT(c.UserID)>0 ";
		}
		$sql.= " ORDER BY a.CategoryName ASC";
	}
}
if($_SESSION['SSV_USER_TARGET']['All-Yes'] || $result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
{
	$result = $li->returnArray($sql);
	if(sizeof($result) > 0){
		$x1_group .= "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByGroup'],ENT_QUOTES)."'>";
		for($i=0; $i<sizeof($result); $i++)
		{
			//if($i == 0){
				//$x1 .= "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByGroup'],ENT_QUOTES)."'>";
			//}
			list($GroupCatID, $GroupCatName) = $result[$i];
			$GroupCatID = "GROUP_".$GroupCatID;
			
			$x1_group .= "<option value='$GroupCatID' ".(($GroupCatID == $OptValue)?"SELECTED":"").">".$GroupCatName."</option>";
	
			//if($i== sizeof($result)-1) {
			//	$x1 .= "</optgroup>";
			//}
		}
		$x1_group .= "</optgroup>";
	}
}

if($identity_group_display_order[0]=='identity'){
	$x1 .= $x1_identity;
	if($identity_group_display_order[1]=='group'){
		$x1 .= $x1_group;
	}
}else if($identity_group_display_order[0]=='group'){
	$x1 .= $x1_group;
	if($identity_group_display_order[1]=='identity'){
		$x1 .= $x1_identity;
	}
}

$x1 .= "</select>";

if($OptValue != "")
{
	if(strpos($OptValue,"GROUP_") !== false)
	{
		## GROUP 
		$GroupOpt = 2;
		$CatID = 4;
		$ChooseGroupCatID = substr($OptValue,6,strlen($OptValue));
		$x1 .= "<input type=\"hidden\" name=\"CatID\" value=\"".escape_double_quotes($CatID)."\">";
	}
	else
	{	
		## Identity
		$GroupOpt = 1;
		$CatID = $OptValue;
		$x1 .= "<input type=\"hidden\" name=\"CatID\" value=\"".escape_double_quotes($CatID)."\" >";
	}
}
else
{
	$GroupOpt = "";
	$CatID = "";
	$ChooseGroupCatID = "";
	$x1 .= "<input type='hidden' name='CatID' value=''>";
}

# 2nd Level Cat List - IP25 only #
if(($CatID != "") || ($CatID != 0) || ($CatID != 4)){
	if($CatID == -1){
		//$x2 = "<select name='Cat2ID' id='Cat2ID' multiple size='10'>";
		$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10' ".($autoExpandSubCat?'onchange="checkOption(this.form.elements[\'ChooseGroupID[]\']);document.form1.submit();"':'').">";
		## All Teaching Staff ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])){
			
			$num_of_all_teacher = $lwebmail->returnNumOfAllTeachingStaff($identity);
			if($num_of_all_teacher > 0){
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']."</option>";
				else
					$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']."</option>";
			}
		}
		## Form Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyForm'])){
			
			$num_of_form_teacher = $lwebmail->returnNumOfFormTeacher($identity);
			if($num_of_form_teacher > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']."</option>";
				else
					$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']."</option>";
			}
		}
		## Class Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyClass'])){
			
			$num_of_class_teacher = $lwebmail->returnNumOfClassTeacher($identity);
			if($num_of_class_teacher > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']."</option>";
				else
					$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']."</option>";
			}
		}
		## Subject Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubject'])){
			
			$num_of_subject_teacher = $lwebmail->returnNumOfSubjectTeacher($identity);
			if($num_of_subject_teacher > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']."</option>";
				else
					$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']."</option>";
			}
		}
		## Subject Group Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'])){
			
			$num_of_subject_group_teacher = $lwebmail->returnNumOfSubjectGroupTeacher($identity);
			if($num_of_subject_group_teacher > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']."</option>";
				else
					$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']."</option>";
			}
		}
		$x2 .= "</select>";
	}
	if($CatID == -2){
		## Non-teaching Staff ##
		$result = $fcm->Get_Non_Teaching_Staff_List(getUserDisplayOrderField(""," ,"));
		$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name) = $result[$i];
				$x3 .= "<option value='$u_id'>".$u_name."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";
	}
	## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
	## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
	if($CatID == -3){
		## Teachers / Staff ##
		$sql = "Select UserID, ".getNameFieldByLang()." as Name From INTRANET_USER WHERE RecordStatus = '1' AND RecordType = '1' ORDER BY ".getUserDisplayOrderField(""," ,")."EnglishName";
		$result = $fcm->returnArray($sql,2);
		
		$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name) = $result[$i];
				$x3 .= "<option value='$u_id'>".$u_name."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";
	}
	if($CatID == 2){
		### Category : Student ###
		$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10' ".($autoExpandSubCat?'onchange="checkOption(this.form.elements[\'ChooseGroupID[]\']);document.form1.submit();"':'').">";
		## All Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])){
			
			$num_of_all_student = $lwebmail->returnNumOfAllStudent($identity);
			if($num_of_all_student > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsStudent']."</option>";
				else
					$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsStudent']."</option>";
			}
		}
		## Form Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyForm'])){
			
			$num_of_form_subject = $lwebmail->returnNumOfFormStudent($identity);
			if($num_of_form_subject > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormStudent']."</option>";
				else
					$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormStudent']."</option>";
			}
		}
		## Class Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyClass'])){
			
			$num_of_class_student = $lwebmail->returnNumOfClassStudent($identity);
			if($num_of_class_student > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassStudent']."</option>";
				else
					$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassStudent']."</option>";
			}
		}
		## Subject Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubject'])){
			
			$num_of_subject_student = $lwebmail->returnNumOfSubjectStudent($identity);
			if($num_of_subject_student > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectStudent']."</option>";
				else
					$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectStudent']."</option>";
			}
		}
		## Subject Group Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'])){
			
			$num_of_subject_group_student = $lwebmail->returnNumOfSubjectGroupStudent($identity);
			if($num_of_subject_group_student > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']."</option>";
				else
					$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']."</option>";
			}
		}
		$x2 .= "</select>";
	}
	if($CatID == 3){
		### Category : Parent ###
		$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10' ".($autoExpandSubCat?'onchange="checkOption(this.form.elements[\'ChooseGroupID[]\']);document.form1.submit();"':'').">";
		## All Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])){
			
			$num_of_all_parent = $lwebmail->returnNumOfAllParent($identity);
			if($num_of_all_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsParents']."</option>";
				else
					$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsParents']."</option>";
			}
		}
		## Form Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyForm'])){
			
			$num_of_form_parent = $lwebmail->returnNumOfFormParent($identity);
			if($num_of_form_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormParents']."</option>";
				else
					$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormParents']."</option>";
			}
		}
		## Class Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyClass'])){
			
			$num_of_class_parent = $lwebmail->returnNumOfClassParent($identity);
			if($num_of_class_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassParents']."</option>";
				else
					$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassParents']."</option>";
			}
		}
		## Subject Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubject'])){
			
			$num_of_subject_parent = $lwebmail->returnNumOfSubjectParent($identity);
			if($num_of_subject_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectParents']."</option>";
				else
					$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectParents']."</option>";
			}
		}
		## Subject Group Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'])){
			
			$num_of_subject_group_parent = $lwebmail->returnNumOfSubjectGroupParent($identity);
			if($num_of_subject_group_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']."</option>";
				else
					$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']."</option>";
			}
		}
		$x2 .= "</select>";
	}
	if($special_feature['alumni'] && $CatID == -4){
		## All Alumni ##
		$NameField = getNameFieldWithClassNumberByLang();
		
		if ($alumniyear!="")
		{
			$sql_cond_alumni = " AND YearOfLeft='".$li->Get_Safe_Sql_Query($alumniyear)."' ";
		}
		$sql = "Select UserID, ".$NameField." as Name From INTRANET_USER where RecordStatus = 1 and RecordType = 4 {$sql_cond_alumni} ORDER BY ".getUserDisplayOrderField(","," ,")."EnglishName, ChineseName";
		$result = $li->returnArray($sql,2);
		$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name) = $result[$i];
				$x3 .= "<option value='$u_id'>".$u_name."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";
	}
	/*
	if($CatID == 4){
		### Category : Group ###
		$lclubsenrol = new libclubsenrol();
		
		$arrExcludeGroupCatID[] = 0;
		if($lclubsenrol->isUsingYearTermBased == 1){
			$arrExcludeGroupCatID[] = 5;
		}
		$targetGroupCatIDs = implode(",",$arrExcludeGroupCatID);
		
		if($_SESSION['SSV_USER_TARGET']['All-Yes'])
		{
			//$sql = "SELECT DISTINCT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID != 0 ORDER BY CategoryName ASC";
			$sql = "SELECT DISTINCT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY CategoryName ASC";
		}
		else
		{
			if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'])
			{
				//$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) WHERE c.UserID = $UserID AND b.RecordType != 0 ORDER BY a.CategoryName ASC";
				$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) WHERE c.UserID = $UserID AND b.RecordType != 0 AND a.GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY a.CategoryName ASC";
			}
		}
		$result = $li->returnArray($sql);
		$x2 = "<select name='ChooseGroupCatID' id='ChooseGroupCatID' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]); this.form.submit();'>";
		$x2 .= "<option value='' >--{$button_select}--</option>";
		if(sizeof($result) > 0){
			for($i=0; $i<sizeof($result); $i++)
			{
				list($GroupCatID, $GroupCatName) = $result[$i];
				if(sizeof($ChooseGroupCatID)>0)
					$x2 .= "<option value='$GroupCatID' ".(($GroupCatID == $ChooseGroupCatID)?"SELECTED":"").">".$GroupCatName."</option>";
				else
					$x2 .= "<option value='$GroupCatID' >".$GroupCatName."</option>";
			}
		}
		$x2 .= "</select>";
	}
	*/
	if($CatID == 5){
		### Category : My Children ###
		$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.")." FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."'";
		$result = $li->returnArray($sql,2);
		
		$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name) = $result[$i];
				$x3 .= "<option value='$u_id'>".$u_name."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";
	}
	if($CatID == 6){
		### Category : My Parent ###
		$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.")." FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.ParentID = b.UserID) WHERE a.StudentID = '".$li->Get_Safe_Sql_Query($UserID)."'";
		$result = $li->returnArray($sql,2);
		
		$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name) = $result[$i];
				$x3 .= "<option value='$u_id'>".$u_name."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";
	}
}

if($GroupOpt == 2 && $CatID == 4 && $ChooseGroupCatID!="")
{
	if($_SESSION['SSV_USER_TARGET']['All-Yes'])
	{
		if($ChooseGroupCatID != 5)
		{
			$title_field = $lgrouping->getGroupTitleByLang();
			$sql = "SELECT GroupID, $title_field as Title FROM INTRANET_GROUP WHERE RecordType = '".$li->Get_Safe_Sql_Query($ChooseGroupCatID)."' AND RecordType != 0 AND AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' ORDER BY $title_field";
		}
		else
		{
			$title_field = $lgrouping->getGroupTitleByLang("a.");
			$CurrentSemesterID = getCurrentSemesterID();
			$sql = "SELECT 
						a.GroupID, $title_field as Title 
					FROM 
						INTRANET_GROUP AS a
						LEFT OUTER JOIN INTRANET_ENROL_GROUPINFO AS b ON (a.GroupID = b.GroupID)
					WHERE 
						a.RecordType = '".$li->Get_Safe_Sql_Query($ChooseGroupCatID)."' 
						AND a.RecordType != 0 
						AND (
							(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND b.Semester = '') 
							OR
							(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND b.Semester IS NULL) 
							OR
							(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND b.Semester = '".$li->Get_Safe_Sql_Query($CurrentSemesterID)."') 
							)
					ORDER BY $title_field";
		}	
	}else{
		//$sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = ".$ChooseGroupCatID." AND RecordType != 0 AND AcademicYearID = $CurrentAcademicYearID ORDER BY Title";
		if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
		{
			if($ChooseGroupCatID != 5)
			{
				$title_field = $lgrouping->getGroupTitleByLang("a.");
				$sql = "SELECT a.GroupID, $title_field as Title FROM INTRANET_GROUP AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID) WHERE a.RecordType = '".$li->Get_Safe_Sql_Query($ChooseGroupCatID)."' AND a.RecordType != 0 AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND b.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' ORDER BY $title_field";
			}
			else
			{
				$title_field = $lgrouping->getGroupTitleByLang("a.");
				$CurrentSemesterID = getCurrentSemesterID();
				$sql = "SELECT 
							a.GroupID, $title_field as Title 
						FROM 
							INTRANET_GROUP AS a 
							INNER JOIN INTRANET_USERGROUP AS b ON a.GroupID = b.GroupID 
							LEFT OUTER JOIN INTRANET_ENROL_GROUPINFO AS enrol_group ON (a.GroupID = enrol_group.GroupID) 
						WHERE 
							a.RecordType = '".$li->Get_Safe_Sql_Query($ChooseGroupCatID)."' 
							AND a.RecordType != 0 
							AND (
								(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND enrol_group.Semester = '') 
								OR
								(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND enrol_group.Semester IS NULL) 
								OR
								(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND enrol_group.Semester = '".$li->Get_Safe_Sql_Query($CurrentSemesterID)."') 
								)
							AND b.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' 
						ORDER BY $title_field";
			}
		}
	}
	$GroupArray = $li->returnArray($sql,2);
	
	$x2_5 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10' ".($autoExpandSubCat?'onchange="checkOption(this.form.elements[\'ChooseGroupID[]\']);document.form1.submit();"':'').">";
	if(sizeof($GroupArray) > 0){
		for($i=0; $i<sizeof($GroupArray); $i++)
		{
			list($GroupID, $GroupName) = $GroupArray[$i];
			if(sizeof($ChooseGroupID)>0)
				$x2_5 .= "<option value='$GroupID' ".((in_array($GroupID,$ChooseGroupID))?"SELECTED":"").">".$GroupName."</option>";
			else
				$x2_5 .= "<option value='$GroupID' >".$GroupName."</option>";
		}
	}else{
		$x2_5 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
	}
	$x2_5 .= "</select>";
	$ShowSubGroupSelection = true;
}
else
{
	$ShowSubGroupSelection = false;
}

if($CatID != "" && sizeof($ChooseGroupID)>0)
{
	$sql = "";
	if($CatID == -1) // teaching staff
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{			
			if($ChooseGroupID[$i] == 1)
			{
				## All Teaching Staff ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || ($special_feature['alumni'] && $identity == "Alumni") )
				{
					$all_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.")." FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 ORDER BY ".getUserDisplayOrderField("all_user."," ,")."IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## Form Teacher ##
				if($identity == "Teaching")
				{
					$sql_year_class = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to form teacher
				}
				if($identity == "Student")
				{
					$sql_year_class = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$sql_year_class = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{		
				## Class Teacher ##
				if($identity == "Teaching")
				{
					$sql_year_class = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to class teacher
				}
				if($identity == "Student")
				{
					$sql_year_class = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$sql_year_class = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## Subject Teacher ##
				if($identity == "Teaching")
				{
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject teacher
				}
				if($identity == "Student")
				{
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{
				## Subject Group Teacher ##
				if($identity == "Teaching")
				{
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject group teacher
				}
				if($identity == "Student")
				{
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
										
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 2) // student
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{
			if($ChooseGroupID[$i] == 1)
			{
				## All Student ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
				{
					$all_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 ORDER BY ".getUserDisplayOrderField(""," ,")."IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## My Form Student ##
				if($identity == "Teaching")
				{
					$sql_year_class = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					 
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to form student
				}
				if($identity == "Student")
				{
					$sql_year_class = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$sql_year_class = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{
				## My Class Student ##
				if($identity == "Teaching")
				{
					$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to class student
				}
				if($identity == "Student")
				{
					$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## My Subject Student ##
				if($identity == "Teaching")
				{
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to Subject student
				}
				if($identity == "Student")
				{
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{		
				## My Subject Group Student ##		
				if($identity == "Teaching")
				{
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to subject group student
				}
				if($identity == "Student")
				{
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY ".getUserDisplayOrderField("b."," ,")."IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 3) // parent
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{
			if($ChooseGroupID[$i] == 1)
			{
				## All Parents ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
				{
					# modified by Kelvin
					//$all_sql = "(SELECT DISTINCT a.UserID, IF(c.EnglishName != '' OR c.EnglishName IS NOT NULL $temp, CONCAT('(',c.ClassName,'-',c.ClassNumber,') ', ".getNameFieldByLang2('c.').",'".$Lang['iMail']['FieldTitle']['TargetParent']."',' (',".getNameFieldByLang2('a.').",')'), ".getNameFieldByLang2('a.').") FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 ORDER BY IFNULL(c.ClassName,''), IFNULL(c.ClassNumber,0), c.EnglishName)";
					//$all_sql = "(SELECT DISTINCT a.UserID, IF((c.EnglishName != '' OR c.EnglishName IS NOT NULL) and (c.ClassName != '' OR c.ClassName IS NOT NULL) and (c.ClassNumber != '' OR c.ClassNumber IS NOT NULL) , CONCAT('(',c.ClassName,'-',c.ClassNumber,') ', ".getNameFieldByLang2('c.').",'".$Lang['iMail']['FieldTitle']['TargetParent']."',' (',".getNameFieldByLang2('a.').",')'), ".getNameFieldByLang2('a.').") FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 ORDER BY IFNULL(c.ClassName,''), IFNULL(c.ClassNumber,0), c.EnglishName)";
					$name_field = "IF(c.EnglishName != '' AND c.EnglishName IS NOT NULL,
									  IF(c.ClassName != '' AND c.ClassName IS NOT NULL and c.ClassNumber != '' AND c.ClassNumber IS NOT NULL,
											CONCAT('(',c.ClassName,'-',c.ClassNumber,') ', ".getNameFieldByLang2('c.').",'".$Lang['iMail']['FieldTitle']['TargetParent']."', IF(a.EnglishName !='' AND a.EnglishName IS NOT NULL,CONCAT(' (',".getNameFieldByLang2('a.').",')'),'')),
											".getNameFieldByLang2('a.')."),
									  IF(TRIM(a.EnglishName)='' AND TRIM(a.ChineseName)='',a.UserLogin,".getNameFieldByLang2('a.').")
									)";
					$all_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 ORDER BY ".getUserDisplayOrderField("c."," ,")."IFNULL(c.ClassName,''), IFNULL(c.ClassNumber,0), c.EnglishName)";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## My Form Parents ##
				if($identity == "Teaching")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_year = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearID = $li->returnVector($sql_year);
					$targetYearID = implode(",",$arrTargetYearID);
					
					$sql_year_class = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
					
					$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE a.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";

					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to form parent
				}
				if($identity == "Student")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_year = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearID = $li->returnVector($sql_year);
					$targetYearID = implode(",",$arrTargetYearID);
					
					$sql_year_class = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
									
					$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_year = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearID = $li->returnVector($sql_year);
					$targetYearID = implode(",",$arrTargetYearID);
					
					$sql_year_class = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
					
					$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{
				## My Class Parents ##
				if($identity == "Teaching")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$tartetYearClassID = implode(",",$arrTargetYearClassID);
					
					$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($tartetYearClassID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
					
					$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to class parent
				}
				if($identity == "Student")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID);
					
					$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
					
					$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."'";
					$arrTargetYearClassID = $li->returnVector($sql_year_class);
					$targetYearClassID = implode(",",$arrTargetYearClassID); 
					
					$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
										
					$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## My Subject Parents ##
				if($identity == "Teaching")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
										
					$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject parent
				}
				if($identity == "Student")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
										
					$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
										
					$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
					
					$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{
				## My Subject Group Parents ##
				if($identity == "Teaching")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);
					
					$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject group parent
				}
				if($identity == "Student")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID); 
					
					$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."'";
					$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
					$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
					
					$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
					$arrTargetUserID = $li->returnVector($sql_user);
					$targetUserID = implode(",",$arrTargetUserID);					
					
					$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY ".getUserDisplayOrderField("a."," ,")."IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}			
		}
		$result = $li->returnArray($sql);
	}
	
	if($CatID == 4) // Group
	{
		if(sizeof($ChooseGroupID)>0)
		{			
			$TargetGroupID = implode(",",$ChooseGroupID);
			
			$cond = "";
			if($result_to_group_options['ToTeacher'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 1 AND (a.Teaching = 1)) ";
			}
			if($result_to_group_options['ToStaff'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 1 AND (TEACHING = 0 OR a.Teaching IS NULL)) ";
			}
			if($result_to_group_options['ToStudent'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 2) ";
			}
			if($result_to_group_options['ToParent'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 3) ";
			}
			if($special_feature['alumni'] && $result_to_group_options['ToAlumni'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 4) ";
			}
			
			if($cond != "")
				$final_cond = " AND ( $cond ) ";
			
			$sql = "SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.GroupID IN ($TargetGroupID) $final_cond ORDER BY ".getUserDisplayOrderField("a."," ,")."a.RecordType, IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
			$result = $li->returnArray($sql,2);
		}
	}
	$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
	if(sizeof($result)>0){
		for($i=0; $i<sizeof($result); $i++){
			list($u_id, $u_name) = $result[$i];
			$x3 .= "<option value='$u_id'>$u_name</option>";
		}
	}else{
		$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
	}
	$x3 .= "</select>";

}

$direct_to_step3 = false;
if($CatID != "")
{
	if(($GroupOpt == 1) && (($CatID == -3)||($CatID == -2)||($CatID == 5)||($CatID == 6) || ($CatID == -4))){
		$direct_to_step3 = true;
	}else if(($GroupOpt == 2) && ($CatID == 4) && (sizeof($ChooseGroupID)>0) && (sizeof($ChooseGroupCatID)>0)) {
		$direct_to_step3 = true;
	} else {
		if(($GroupOpt == 1) && ($CatID != 4) && (sizeof($ChooseGroupID)>0)){
			$direct_to_step3 = true;
		}
	}
}

if ($fieldname == "Recipient[]")
{
	$ExtraJS = " par.displayTable('internalToTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalToRemoveBtnDiv','block'); \n";	
}
else if ($fieldname == "InternalCC[]")
{
	$ExtraJS = " par.displayTable('internalCCTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalCCRemoveBtnDiv','block'); \n";	
}
else if ($fieldname == "InternalBCC[]")
{
	$ExtraJS = " par.displayTable('internalBCCTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalBCCRemoveBtnDiv','block'); \n";	
}

?>

<script language="javascript">
	
function AddOptions(obj, type){
	
	par = window.opener;
    parObj = window.opener.document.form1.elements["<?php echo escape_double_quotes($fieldname); ?>"];
	var CatType = document.form1.CatID.value;
	if (type==1)    // Normal
	{
		if(CatType == "-1") {
			// To Teaching Group
			x = (obj.name == "ChooseGroupID[]") ? "T" : "U";
		}else if (CatType == "2") {
			// To Student Group
			x = (obj.name == "ChooseGroupID[]") ? "S" : "U";
		}else if (CatType == "3") {
			// To Parent Group
			x = (obj.name == "ChooseGroupID[]") ? "R" : "U";
		}else if (CatType == "4") {
			// To Intranet Group
			x = (obj.name == "ChooseGroupID[]") ? "O" : "U";
		}else{
			x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
		}
	}
    
	checkOption(obj);
	par.checkOption(parObj);
	
	i = obj.selectedIndex;
	while(i!=-1){
		if (type==0)
		{
			addtext = obj.options[i].text;			
		}
		else
		{
			flag = true;
			for (a=0; a<parObj.length; a++){
				if(parObj.options[a].text == obj.options[i].text)
				{
					flag = false;
					break;
				}
			}
			if(flag != false)
			{
				addtext = obj.options[i].text + "<?=escape_double_quotes($suf_parent)?>";
				if(addtext != "")
				{
					par.checkOptionAdd(parObj, addtext, x + obj.options[i].value);
					showSelectionBox = true;
				}
			}
			
		}
		//par.checkOptionAdd(parObj, addtext, x + obj.options[i].value);
		obj.options[i] = null;
		i = obj.selectedIndex;
	}
	//par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
	if(showSelectionBox == true)
	{
		<?=$ExtraJS?>     
	}
	SelectAll(parObj);
}



function checkOptionNone(obj){
	if(obj==null)return;
       for(i=0; i<obj.length; i++){
                obj.options[i].selected = false;
        }
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

// use in AddressBook Internal Recipient Group
function addRecipient(obj,type){

	var CatType = document.form1.CatID.value;
	
     if (type==1)    // Normal
     {
     	if(CatType == "-1") {
     		// To Teaching Group
     		x = (obj.name == "ChooseGroupID[]") ? "T" : "U";
     	}else if (CatType == "2") {
     		// To Student Group
     		x = (obj.name == "ChooseGroupID[]") ? "S" : "U";
     	}else if (CatType == "3") {
     		// To Parent Group
     		x = (obj.name == "ChooseGroupID[]") ? "R" : "U";
     	}else if (CatType == "4") {
     		// To Intranet Group
     		x = (obj.name == "ChooseGroupID[]") ? "O" : "U";
     	}else{
			x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
     	}
    }
    
	ids ='';
	delim='';
	for(i=0;i<obj.options.length;i++){
		if(obj.options[i].selected){
			ids+=delim+x+obj.options[i].value;
			delim=',';
		}
	}
	
	document.form1.HiddenCatID.value = document.form1.CatID.value;
	document.form1.InternalRecipientID.value = ids;
	document.form1.action='addressbook_update.php';
	document.form1.submit();
}
</script>

<form name="form1" action="index.php" method="post" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<br />
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td>

		<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
		<tr>
 			<tr>
			<td valign="top" nowrap="nowrap" width="30%" >
				<span class="tabletext"><?=$Lang['iMail']['FieldTitle']['Choose'];?>:</span>
			</td>
			<td>
				<!--<?=$OptionSelection;?>-->
				<?=$x1;?>
			</td>
		</tr>
		<? 
if($CatID==-4) 
{ 
	
$lclass = new libclass();
	$AlumniYears = $lclass->getSelectAlumniYears();
	
	$YearSelection = $linterface->GET_SELECTION_BOX($AlumniYears, "name='alumniyear' onChange='this.form.submit();'", $Lang['SysMgr']['RoleManagement']['AllAlumni'], $alumniyear);
	?>
<tr>
	<td class="field_title"><?=$Lang['iMail']['FieldTitle']['Alumni']?></td>
	<td><?=$YearSelection?></td>
</tr>
<? } ?>
		<!--
		<? if($GroupOpt != "") { ?>
			<tr> 
				<td height="5" colspan="2"  >
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
				</td>
			</tr>
			
			<tr> 
				<td height="1" colspan="2" class="dotline" valign="middle" >
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
				</td>
			</tr>
			
			<tr> 
				<td height="5" colspan="2"  >
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
				</td>
			</tr>
				
 			<tr>
				<td valign="top" nowrap="nowrap" width="30%" >
					<span class="tabletext"><?=$Lang['iMail']['FieldTitle']['Category'];?>:</span>
				</td>
				<td >
					<?=$x1;?>
				</td>
			</tr>
		<? } ?>
		-->
		<?php 
		//if($CatID!="" && $CatID > 0 && !$direct_to_step3) 
		//if($CatID!="" && $CatID!=-2 && !$direct_to_step3) 		
		if($CatID != "")
		{
			if($CatID != 0 && $CatID!=-2 && $CatID!=5 && $CatID!=6 && $CatID!=-3 && $CatID!=-4)
			{
		?>
		
		<? if(($GroupOpt == 1) && ($CatID != 4)) { ?>
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		<tr> 
			<td height="1" colspan="2" class="dotline"  valign="middle">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		
		<tr>
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$Lang['iMail']['FieldTitle']['SubCategory'];?>:</span>
			</td>
			<td >					
			<table cellpadding="0" cellspacing="0" >
			<tr>
				<td><?=$x2?></td>
				<td style="vertical-align:bottom">
				<table cellpadding="0" cellspacing="6" >				
				<?php 
				if($CatID!=4 && $CatID!=999)
				{
				?>
				<tr>
					<td>
						<!-- Button in Sub-Cat -->
					<?php if($caller=="compose"){ ### called from Compose page
								//echo $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],0)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
								if(!$sys_custom['iMailHideAddSubCategoryButton']){
									echo $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],1)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
								}
						  }else{ ### called from AddressBook
								echo $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseGroupID[]'],1)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
						  }
					?>
					</td>
				</tr>	
				<?php 
				}
				?>
				<?php 
				//if($CatID==4 || $CatID==999)
				if($CatID==999)
				{
				?>
				<tr>
					<td>
						<!-- Button in Sub-Cat (Parent Only) -->
					<?php 
						if($caller=="compose"){
							if(!$sys_custom['iMailHideAddSubCategoryButton']){
								echo $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],1)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."";
							}
						}else{
							//echo $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseGroupID[]'],1)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."";
							echo $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseGroupID[]'],1)","submit1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
						}
					?>
					</td>
				</tr>	
				<? 
				}
				if($CatID != 4)
				{ 
				?>
				<tr>
					<td>
					<!-- Button for add recipient (ONLY Teaching, Student & Parent) -->
					<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td>
				</tr>
				<?
				}
				?>
				
				<?php 
				if(!$sys_custom['Mail_NoSelectAllButton']) 
				{ 
				?>
				<tr>
					<td>
					<?//= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseGroupID[]']); return false;","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ") ?>
					</td>
				</tr>					
				<?php 
				} 
				?>
				</table>	
				</td>
			</tr>
			</table>			
			</td>
		</tr>
		<? } ?>
		<?php 
			}
		}
		?>
		<? 
		### Show Group Multi Selection Box
		if($ShowSubGroupSelection)
		{
		?>
			<tr> 
				<td height="5" colspan="2"  >
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
				</td>
			</tr>
			<tr> 
				<td height="1" colspan="2" class="dotline">
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
				</td>
			</tr>
			<tr> 
				<td height="5" colspan="2"  >
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
				</td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" width="30%" >
					<span class="tabletext"><?=$Lang['iMail']['FieldTitle']['Group'];?>:</span>
				</td>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" align="left">		
					<tr >
						<td ><?=$x2_5?></td>
						<td valign="bottom" >
							<table cellpadding="0" cellspacing="6" >
							<tr>
							<td >
								<!-- Button For Sub-Cat (Non-teaching) -->
								<?php 
								if($caller=="compose"){																		
									if(!$sys_custom['iMailHideAddSubCategoryButton']){
										echo $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],1)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
									}
								}else{ 
									echo $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseGroupID[]'],1)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
								}
								?>
							</td >
							</tr>
							<tr>
								<td>
								<!-- Button for add recipient (ONLY Teaching, Student & Parent) -->
								<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
								</td>
							</tr>
							<?php 
								if(!$sys_custom['Mail_NoSelectAllButton']) 
								{	 
							?>
							<tr >
								<td >
								<?= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseGroupID[]']); return false;","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
								</td >
							</tr >
							<?php 
								} 
							?>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<?
		}
		?>
		
		<?
		//if($CatID!=""&&(isset($ChooseGroupID) || $CatID<0 ||$direct_to_step3)) 
		//{ 
		if($direct_to_step3){
		?>
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		<tr> 
			<td height="1" colspan="2" class="dotline">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
				
		<tr >
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_CampusMail_New_AddressBook_ByUser?>:</span>
			</td>
			<td >					
			<table border="0" cellpadding="0" cellspacing="0" align="left">		
			<tr >
				<td >				
				<?=$x3?>
				</td>
				<td valign="bottom" >
				<table cellpadding="0" cellspacing="6" >
				<?php
				if($CatID==999)
				{ 
				?>
				<tr >
					<td >	
						<!-- Button for unknown -->
					<?php 
						if($caller=="compose"){
							echo $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'],1)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."";				
						}else{ 
							echo $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseUserID[]'],1)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."";
						}					
				 	?>
					</td >
				</tr >	
				<?php 
				} else {
				?>
				<tr >
					<td >
						<!-- Button For Sub-Cat (Non-teaching) -->
						<?php 
						if($caller=="compose"){																		
							echo  $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'],1)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
						}else{ 
							echo  $linterface->GET_BTN($button_add, "button", "addRecipient(this.form.elements['ChooseUserID[]'],1)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ;
						}
						?>
					</td >
				</tr >	
				<?php 
				} 
				?>
				<?php 
				if(!$sys_custom['Mail_NoSelectAllButton']) 
				{ 
				?>
				<tr >
					<td >		
					<!-- Button for add recipient (non-teaching) -->																
					<?= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseUserID[]']); return false;","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ") ?>
					</td >
				</tr >	
				<?php 
				} 
				?>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<?php 
		} 
		?>
		</table>		
		</td>
	</tr>
	
	<tr>
		<td>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	
	</td>
</tr>
</table>
<input type="hidden" name=InternalRecipientID value="">
<input type="hidden" name=HiddenCatID value="">
<input type="hidden" name=AliasID value="<?=escape_double_quotes($AliasID)?>">
<input type="hidden" name="fieldname" value="<?php echo escape_double_quotes($fieldname); ?>" />
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>
