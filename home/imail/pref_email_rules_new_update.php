<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libdb();

$FromEmail = str_replace(array("\r\n", "\r", "\n"), "",$from_email);
$FromEmail = str_replace(",",";",$from_email);
$FromEmail = intranet_htmlspecialchars(stripslashes(trim($from_email)));
$ToEmail = str_replace(array("\r\n", "\r", "\n"), "",$to_email);
$ToEmail = str_replace(",",";",$to_email);
$ToEmail = intranet_htmlspecialchars(stripslashes(trim($to_email)));
$SubjectKeyword = str_replace(array("\r\n", "\r", "\n"), "",$subject);
$SubjectKeyword = str_replace(",",";",$subject);
$SubjectKeyword = intranet_htmlspecialchars(stripslashes(trim($subject)));
$MessageKeyword = str_replace(array("\r\n", "\r", "\n"), "",$message);
$MessageKeyword = str_replace(",",";",$message);
$MessageKeyword = intranet_htmlspecialchars(stripslashes(trim($message)));
$ForwardTo = str_replace(array("\r\n", "\r", "\n"), "",$act_forward);
$ForwardTo = str_replace(",",";",$act_forward);
$ForwardTo = intranet_htmlspecialchars(stripslashes(trim($act_forward)));

if($has_attachment == ""){
	$has_attachment = 0;
}
if($act_mark_as_read == ""){
	$act_mark_as_read = 0;
}
if($act_delete_mail == ""){
	$act_delete_mail = 0;
}
if($targetFolder == ""){
	$targetFolder = 0;
}

$sql = "INSERT INTO INTRANET_IMAIL_MAIL_RULES 
			(UserID, FromEmail, ToEmail, Subject, Message, HasAttachment, ActionMarkAsRead, ActionDelete, ActionMoveToFolder, ActionForwardTo, DateInput, DateModified) 
		VALUES
			('$UserID', '$FromEmail', '$ToEmail', '$SubjectKeyword', '$MessageKeyword', '$has_attachment', '$act_mark_as_read', '$act_delete_mail', '$targetFolder', '$ForwardTo', NOW(), NOW())
		";
$result = $li->db_db_query($sql);

if($result){
	header("Location: pref_email_rules.php?msg=1");
}else{
	header("Location: pref_email_rules.php?msg=12");
}

intranet_closedb();
?>