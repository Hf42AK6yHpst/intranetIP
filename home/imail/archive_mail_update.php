<?php
// editing by 
/*************** Change Log ***************
 * Created on 2011-04-07
 * 2019-06-06 Carlos: Added CSRF token checking.
 ******************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if(!verifyCsrfToken($csrf_token, $csrf_token_key)){
	header("Location: /");
	exit;
}

intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libcampusmail();

$numOfYears = sizeof($archive);
$userFolders = array();
for($i=0;$i<$numOfYears;$i++){
	// 0=> Year as FolderName, 1=>FolderID 
	$userFolders[$archive[$i]] = array($archive[$i],"");
}

$sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID IS NOT NULL AND OwnerID = '".$_SESSION['UserID']."' AND RecordType = 1 ORDER BY FolderName ";
$existUserFolders = $li->returnArray($sql);

for($i=0;$i<sizeof($existUserFolders);$i++){
	list($folderID, $folderName) = $existUserFolders[$i];
	if(isset($userFolders[$folderName])) $userFolders[$folderName][1] = $folderID;
}

$Result = array();
if(sizeof($userFolders)>0){
	foreach($userFolders as $key => $arr){
		list($folderName, $folderID) = $arr;
		if($folderID=="")
		{ // Create new folder
			$sql = "INSERT INTO INTRANET_CAMPUSMAIL_FOLDER 
					(OwnerID,FolderName,RecordType,DateInput,DateModified) 
					VALUES('".$_SESSION['UserID']."','$folderName',1,NOW(),NOW())";
			$insert_success = $li->db_db_query($sql);
			if($insert_success)
			{
				$insertFolderID = $li->db_insert_id();
				$userFolders[$key][1] = $insertFolderID;
				$folderID = $insertFolderID;
			}
		}
		if($folderID!=""){
			$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = '$folderID' 
					WHERE UserID = '".$_SESSION['UserID']."' 
						AND UserFolderID = 2 
						AND DateInput IS NOT NULL 
						AND DateInput <> '' 
						AND Deleted != 1 
						AND DateInput LIKE '$folderName%' ";
			$$Result['ChangeFolder_'.$folderName] = $li->db_db_query($sql);
		}else{
			$Result[$folderName] = false;
		}
	}
}else{
	$Result[] = false;
}

if(!in_array(false,$Result)){
	$_SESSION['intranet_iMail_archived'] = 1;
	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else
	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];

intranet_closedb();
header("Location: viewfolder.php?FolderID=2&msg=".rawurlencode($msg));
?>