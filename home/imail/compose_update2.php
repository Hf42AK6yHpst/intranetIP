<?php

## using : 
/*
 * Change Log [Start]
 * Date : 2019-06-04 [Carlos] - Added CSRF token. 
 * Date : 2011-04-19 [Carlos] - added hidden field preFolder for REPLY/REPLY ALL/FORWARD to go back to previous folder
 * Date	: 2011-01-26 [Carlos]
 * 			- added composFolder hidden field 
 * 
 * Date : 2009-07-25 [Ronald]
 * 			- set "utf-8" in html charset : <META http-equiv=Content-Type content='text/html; charset=utf-8'>
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	header('Location: /');
	exit;
}

intranet_opendb();

$header_onload_js = "document.form1.submit();";

$Subject = intranet_htmlspecialchars(stripslashes(trim($Subject)));
$Message = intranet_htmlspecialchars(stripslashes(trim($Message)));
$ExternalTo = str_replace(array("\r\n", "\r", "\n"), "",$ExternalTo);
$ExternalTo = str_replace(",",";",$ExternalTo);
$ExternalTo = intranet_htmlspecialchars(stripslashes(trim($ExternalTo)));
$ExternalCC = str_replace(array("\r\n", "\r", "\n"), "",$ExternalCC);
$ExternalCC = str_replace(",",";",$ExternalCC);
$ExternalCC = intranet_htmlspecialchars(stripslashes(trim($ExternalCC)));
$ExternalBCC = str_replace(array("\r\n", "\r", "\n"), "",$ExternalBCC);
$ExternalBCC = str_replace(",",";",$ExternalBCC);
$ExternalBCC = intranet_htmlspecialchars(stripslashes(trim($ExternalBCC)));

$MODULE_OBJ['title'] = $i_frontpage_campusmail_attachment;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
//debug_r($Message);
//die;
?>
<!--
<?php if($intranet_session_language=="en") { ?><META http-equiv=Content-Type content='text/html; charset=big5'><?php } ?>
<?php if($intranet_session_language=="b5") { ?><META http-equiv=Content-Type content='text/html; charset=big5'><?php } ?>
<?php if($intranet_session_language=="gb") { ?><META http-equiv=Content-Type content='text/html; charset=GB2312'><?php } ?>
-->
<META http-equiv=Content-Type content='text/html; charset=utf-8'>
<form name="form1" method="post" action="compose_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td align="center" ><div id="msg"><?=$xcmsg?></div></td>
	</tr>
	<tr>
		<td align="center" >
		<table width="685" border="0" cellspacing="0" cellpadding="2" class="tabletext">
		<tr>
			<td align="right" valign="top" width="120" height="100">&nbsp;</td>
			<td width="230" align="left" valign="top">&nbsp;</td>
			<td width="335" align="left" valign="top">&nbsp;</td>
		</tr>
		<tr>
			<td align="right" valign="top">&nbsp;</td>
			<td align="left" valign="top" colspan="2">
			<?=$i_campusmail_processing?> &nbsp;<br />
			<?=$i_campusmail_processing_note?>
			</td>
		</tr>
		<tr>
			<td align="right" valign="top" width="120" height="100">&nbsp;</td>
			<td width="230" align="left" valign="top">&nbsp;</td>
			<td width="335" align="left" valign="top">&nbsp;</td>
		</tr>
		</table>		
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="Subject" value="<?=$Subject?>" />
<input type="hidden" name="Message" value="<?=$Message?>" />
<input type="hidden" name="SubmitType" value="<?=escape_double_quotes($SubmitType)?>" />
<input type="hidden" name="IsImportant" value="<?=escape_double_quotes($IsImportant)?>" />
<input type="hidden" name="IsNotification" value="<?=escape_double_quotes($IsNotification)?>" />
<?php
for ($i=0; $i<sizeof($Recipient); $i++)
{
     $temp = $Recipient[$i];
?>
<input type="hidden" name="Recipient[]" value="<?=escape_double_quotes($temp)?>" />
<?
}

for ($i=0; $i<sizeof($InternalCC); $i++)
{
     $temp = $InternalCC[$i];
?>
<input type="hidden" name="InternalCC[]" value="<?=escape_double_quotes($temp)?>" />
<?
}

for ($i=0; $i<sizeof($InternalBCC); $i++)
{
     $temp = $InternalBCC[$i];
?>
<input type="hidden" name="InternalBCC[]" value="<?=escape_double_quotes($temp)?>" />
<?
}

for ($i=0; $i<sizeof($Attachment); $i++)
{
     $temp = stripslashes($Attachment[$i]);
?>
<input type="hidden" name="Attachment[]" value="<?=escape_double_quotes($temp)?>" />
<?
}
?>
<input type="hidden" name="file_names" value="<?=escape_double_quotes(stripslashes($file_names))?>" />
<input type="hidden" name="ExternalTo" value="<?=escape_double_quotes($ExternalTo)?>" />
<input type="hidden" name="ExternalCC" value="<?=escape_double_quotes($ExternalCC)?>" />
<input type="hidden" name="ExternalBCC" value="<?=escape_double_quotes($ExternalBCC)?>" />
<input type="hidden" name="AllStaffTo" value="<?=escape_double_quotes($AllStaffTo)?>" />
<input type="hidden" name="AllStudentTo" value="<?=escape_double_quotes($AllStudentTo)?>" />
<input type="hidden" name="AllParentTo" value="<?=escape_double_quotes($AllParentTo)?>" />
<input type="hidden" name="AllStaffCC" value="<?=escape_double_quotes($AllStaffCC)?>" />
<input type="hidden" name="AllStudentCC" value="<?=escape_double_quotes($AllStudentCC)?>" />
<input type="hidden" name="AllParentCC" value="<?=escape_double_quotes($AllParentCC)?>" />
<input type="hidden" name="AllStaffBCC" value="<?=escape_double_quotes($AllStaffBCC)?>" />
<input type="hidden" name="AllStudentBCC" value="<?=escape_double_quotes($AllStudentBCC)?>" />
<input type="hidden" name="AllParentBCC" value="<?=escape_double_quotes($AllParentBCC)?>" />
<input type="hidden" name="action" value="<?=escape_double_quotes($action)?>" />
<input type="hidden" name="OriginalCampusMailID" value="<?=escape_double_quotes($OriginalCampusMailID)?>" />
<input type="hidden" name="isHTML" value="<?=escape_double_quotes($isHTML)?>" />
<input type="hidden" name="composeFolder" value="<?=escape_double_quotes($composeFolder)?>" />
<input type="hidden" name="preFolder" value="<?=escape_double_quotes($preFolder)?>" />
<?php echo csrfTokenHtml(generateCsrfToken()); ?>
</form>

<script language="javascript" >
<?=$header_onload_js?>
</script>

<?php
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>