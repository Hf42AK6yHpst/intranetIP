<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
#include_once("../../../../includes/libgrouping.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();


$li = new libdb();
# get blocked groups and blocked usertypes
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
	
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_blocked = "SELECT BlockedGroupCat,BlockedUserType FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Restricted!=1";
if($usertype==1)
	$sql_blocked.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_blocked.=" AND ClassLevel='$class_level_id'";
$result_blocked = $li->returnArray($sql_blocked,2);

list($blocked_groups,$blocked_usertypes) = $result_blocked[0];

$blocked_groups = explode(",",$blocked_groups);
$blocked_usertypes= explode(",",$blocked_usertypes);
# end get blocked groups / usertypes

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

# non-teaching staff
if ($content[0][0]==1) $permitted[] = 5;

if ($file_content == "")
{
    $permitted = array(1,2,3,5);
}

if (!in_array($ChooseUserType,$permitted)|| in_array($ChooseUserType,$blocked_usertypes))
{
    header("Location: index.php");
    exit();
}
if ($ChooseUserType == 1)   # Staff: Just show the list directly
{
    header("Location: list.php?ChooseUserType=$ChooseUserType&AliasID=$AliasID");
    exit();
}
if($ChooseUserType==5) # Non-Teaching Staff
{
    header("Location: list.php?ChooseUserType=$ChooseUserType&AliasID=$AliasID");
    exit();

}
if ($ChooseUserType == 2)
{
    header("Location: class.php?ChooseUserType=$ChooseUserType&AliasID=$AliasID");
    exit();
}
else if ($ChooseUserType == 3)
{
    header("Location: list.php?ChooseUserType=$ChooseUserType&AliasID=$AliasID");
    exit();

#include_once("../../../templates/fileheader.php");
#include_once("../../../templates/filefooter.php");

}
intranet_closedb();
?>