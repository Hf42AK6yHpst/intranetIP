<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if ($AliasID == "")
{
    header("Location: ".$PATH_WRT_ROOT."school/close.php");
    exit();
}

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;
if ($file_content == "")
{
    $permitted = array(1,2,3);
}

$li = new libdb();

# get blocked groups and blocked usertypes
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_blocked = "SELECT BlockedGroupCat,BlockedUserType FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Restricted!=1";
if($usertype==1)
	$sql_blocked.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_blocked.=" AND ClassLevel='$class_level_id'";
$result_blocked = $li->returnArray($sql_blocked,2);

list($blocked_groups,$blocked_usertypes) = $result_blocked[0];

$blocked_groups = explode(",",$blocked_groups);
$blocked_usertypes= explode(",",$blocked_usertypes);
# end get blocked groups / usertypes

$lgroupcat = new libgroupcategory();
$temp = $lgroupcat->returnAllCat();
$cats = array();
for($i=0;$i<sizeof($temp);$i++){
	
	if($temp[$i][0]!=0 &&!in_array($temp[$i][0],$blocked_groups)){
		$cats[]= $temp[$i];
	}
}
$select_usertype="";
$select_usertype .= "<option value=\"\" >-- $button_select --</option>\n";
if (in_array(1,$permitted) && !in_array(1,$blocked_usertypes)){
    $select_usertype .= "<option value=\"1\" ".(($CatID==1)?"SELECTED":"").">$i_teachingStaff</option>\n";
}
if (in_array(1,$permitted) && !in_array(5,$blocked_usertypes)){
    $select_usertype .= "<option value=\"5\" ".(($CatID==5)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
}
if (in_array(2,$permitted) && !in_array(2,$blocked_usertypes) )
    $select_usertype .= "<option value=\"2\" ".(($CatID==2)?"SELECTED":"").">$i_identity_student</option>\n";
if (in_array(3,$permitted) && !in_array(3,$blocked_usertypes))
    $select_usertype .= "<option value=\"3\" ".(($CatID==3)?"SELECTED":"").">$i_identity_parent</option>\n";
if($select_usertype!=""){
	$select_usertype = "<SELECT name=\"ChooseUserType\" onClick=\"changeType(this.form,'U')\" onChange=\"if (checkform(this.form)){this.form.submit();}\">\n$select_usertype</SELECT>\n";
}
$select_cat = getSelectByArray($cats,"name=\"GroupCategory\" onClick=\"changeType(this.form,'G')\" onChange=\"if (checkform(this.form)){this.form.submit();}\" ","",0,0);

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>

<script language="javascript">
function checkform(obj){
//	alert(obj.ChooseUserType.selectedIndex+" | "+obj.ChooseUserType.value);
//	return false;
	
	for(i=0;i<obj.type.length;i++)
	{
         if (obj.type[i].checked)
         {
             if(obj.type[i].value=='U')
             	obj.action = 'user.php';
             else obj.action='group.php';
             return true;
         }
    }
    return false;
}

function changeType(formObj,thisVal){
	if(formObj.type==null) return;
	objType = formObj.type;

	for(i=0;i<objType.length;i++)
		if(objType[i].value==thisVal){
			objType[i].checked=true;
			break;
		}
}
</script>


<form name="form1" action="" method="get" onSubmit="return checkform(this)">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<br />
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="center">
		<table width="80%" border="0" cellspacing="1" cellpadding="2" class="tabletext">
                <?php if($select_usertype!=""){?>
                <tr><td align="right"><input type="radio" name="type" value="U" CHECKED><?=$i_CampusMail_New_AddressBook_ByUser?>:</td><td><?=$select_usertype?></td></tr>
                <?php } ?>
                <?php if(sizeof($cats)>0){?>
                <tr><td align="right"><input type="radio" name="type" value="G"><?=$i_CampusMail_New_AddressBook_ByGroup?>:</td><td><?=$select_cat?></td></tr>
                <?php } ?>
                <?php if(sizeof($cats)<=0 && $select_usertype==""){?>
                <tr><td align="center" colspan="2"><?=$i_campusmail_no_users_available?></td></tr>
                
                <?php }?>
                </table>
               
                </td>
	</tr>
        
        <tr>
		<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
        
        </table>
        </td>
</tr>
</table>        



<input type="hidden" name="AliasID" value="<?=$AliasID?>">
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>