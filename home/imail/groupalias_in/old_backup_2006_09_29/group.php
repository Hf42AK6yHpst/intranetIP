<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
#include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgroupcategory.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../../../templates/fileheader.php");

$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();
$select_cat = getSelectByArray($cats,"name=GroupCategory onChange=\"this.form.submit()\"","$GroupCategory",0,1);


$libgroup = new libgroup();
$groups = $libgroup->returnGroupsByCategory($GroupCategory);

?>
<script language="javascript">
</script>
<form name=form1 action="group_update.php" method=post>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_t_l.gif" width="27" height="58"></td>
    <td class="imail_popup_blue_cell_t"><img src="<?=$image_path?>/frontpage/imail/popup_add_<?=$intranet_session_language?>.gif"></td>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_t_r.gif" width="27" height="58"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
      <td width="15" class="imail_popup_blue_cell_l"><img src="<?=$image_path?>/frontpage/imail/popupblue_cell_l.gif" width="15" height="10"></td>
      <td align="center" bgcolor="#E5F1FD">
<table width=100% border=0 cellspacing=0 cellpadding=0>
<tr><td ><b><?=$i_CampusMail_New_AddressBook_ByGroup?><b></td><td></td></tr>
<tr><td><img src="<?=$image_path?>/spacer.gif" width=10><?=$i_GroupCategorySettings?>:<?=$select_cat?></td><td align=right><a href=javascript:checkRestore(document.form1,'TargetID[]','group_update.php')><image src="<?=$image_path?>/btn_add_<?=$intranet_session_language?>.gif" border=0></a><img src="<?=$image_path?>/spacer.gif" width=10></td></tr>
<tr><td colspan=2>
<table width=100% border=1 cellpadding="3" cellspacing="0" bordercolorlight="#FFFFFF" bordercolordark="#8FC9F5" bgcolor="#D0E6FC" class="body">
<tr bgcolor=#3F9EE6><td><?=$i_CampusMail_New_AddressBook_TargetGroup?></td><td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'TargetID[]'):setChecked(0,this.form,'TargetID[]')></td></tr>
<?
for ($i=0; $i<sizeof($groups); $i++)
{
     list($id,$name) = $groups[$i];
     echo "<tr><td>$name</td><td><input type=checkbox name=TargetID[] value='$id'></td></tr>\n";
}
?>
</table>
</td></tr>
</table>

</td>
      <td width="15" class="imail_popup_blue_cell_r"><img src="<?=$image_path?>/frontpage/imail/popupblue_cell_r.gif" width="15" height="10"></td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_b_l.gif" width="27" height="30"></td>
    <td class=imail_popup_blue_cell_b>&nbsp;</td>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_b_r.gif" width="27" height="30"></td>
  </tr>
</table>


<input type=hidden name=AliasID value="<?=$AliasID?>">
</form>

<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>