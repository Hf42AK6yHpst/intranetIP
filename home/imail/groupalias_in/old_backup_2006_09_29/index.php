<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroupcategory.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($AliasID == "")
{
    header("Location: ../../school/close.php");
    exit();
}
include_once("../../../templates/fileheader.php");
$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

if ($file_content == "")
{
    $permitted = array(1,2,3);
}

$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$select_usertype = "<SELECT name=UserType onChange=\"this.form.type[0].checked=true\">\n";
for ($i=0; $i<sizeof($permitted); $i++)
{
     $type_id = $permitted[$i];
     $select_usertype .= "<OPTION value=".$type_id.">".$i_identity_array[$type_id]."</OPTION>\n";
}
$select_usertype .= "</SELECT>\n";

$select_cat = getSelectByArray($cats,"name=GroupCategory onChange=\"this.form.type[1].checked=true\"","",0,1);
?>
<script language="javascript">
function checkform(obj){
         if (obj.type[0].checked)
         {
             obj.action = 'user.php';
         }
         else
         {
             obj.action = 'group.php';
         }
}
</script>

<form name=form1 action="" method=get onSubmit="return checkform(this)">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_t_l.gif" width="27" height="58"></td>
    <td class="imail_popup_blue_cell_t"><img src="<?=$image_path?>/frontpage/imail/popup_add_<?=$intranet_session_language?>.gif"></td>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_t_r.gif" width="27" height="58"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
      <td width="15" class="imail_popup_blue_cell_l"><img src="<?=$image_path?>/frontpage/imail/popupblue_cell_l.gif" width="15" height="10"></td>
      <td align="center" bgcolor="#E5F1FD">

      <table width=80% border=0 cellspacing=1 cellpadding=2>
<tr><td align=right><input type=radio name=type value='U' CHECKED><?=$i_CampusMail_New_AddressBook_ByUser?>:</td><td><?=$select_usertype?></td></tr>
<tr><td align=right><input type=radio name=type value='G'><?=$i_CampusMail_New_AddressBook_ByGroup?>:</td><td><?=$select_cat?></td></tr>
<tr><td>&nbsp;</td><td><input type=image src="<?=$image_path?>/btn_next_<?=$intranet_session_language?>.gif"</td></tr>
</table>

        </td>
      <td width="15" class="imail_popup_blue_cell_r"><img src="<?=$image_path?>/frontpage/imail/popupblue_cell_r.gif" width="15" height="10"></td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_b_l.gif" width="27" height="30"></td>
    <td class=imail_popup_blue_cell_b>&nbsp;</td>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_b_r.gif" width="27" height="30"></td>
  </tr>
</table>

<input type=hidden name=AliasID value="<?=$AliasID?>">
</form>

<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>