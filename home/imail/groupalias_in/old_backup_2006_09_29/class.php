<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
#include_once("../../../includes/libgrouping.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../../../templates/fileheader.php");

if ($UserType == "")
{
    header("Location: index.php");
    exit();
}

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

if ($file_content == "")
{
    $permitted = array(1,2,3);
}
if (!in_array($UserType,$permitted))
{
    header("Location: index.php?AliasID=$AliasID");
    exit();
}

$lclass = new libclass();
$class_list = $lclass->getClassList();
?>
<script language="javascript">
function checkform(obj){
}
</script>
<form name=form1 action="" method=POST onSubmit="return checkform(this)">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_t_l.gif" width="27" height="58"></td>
    <td class="imail_popup_blue_cell_t"><img src="<?=$image_path?>/frontpage/imail/popup_add_<?=$intranet_session_language?>.gif"></td>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_t_r.gif" width="27" height="58"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
      <td width="15" class="imail_popup_blue_cell_l"><img src="<?=$image_path?>/frontpage/imail/popupblue_cell_l.gif" width="15" height="10"></td>
      <td align="center" bgcolor="#E5F1FD">

<table width=100% border="1" cellpadding="3" cellspacing="0" bordercolorlight="#FFFFFF" bordercolordark="#8FC9F5" bgcolor="#D0E6FC" class="body">
<tr><td colspan=2><b><?="$i_CampusMail_New_AddressBook_ByUser &gt; ".$i_identity_array[2]?><b></td></tr>
<tr bgcolor=#3F9EE6><td colspan=2><?=$i_UserParentLink_SelectClass?></td></tr>
<?
for ($i=0; $i<sizeof($class_list); $i++)
{
     list($id,$name) = $class_list[$i];
     $link = "<a href=list.php?UserType=$UserType&Class=$name&AliasID=$AliasID>$name</a>";
     if ($i%2==0)
     {
         echo "<tr>";
     }
     echo "<td>$link</td>";
     if ($i%2==1)
     {
         echo "</tr>\n";
     }
}
if ($i%2==1)
{
    echo "<td>&nbsp;</td>";
}

echo "</tr>";
?>
</table>


</td>
      <td width="15" class="imail_popup_blue_cell_r"><img src="<?=$image_path?>/frontpage/imail/popupblue_cell_r.gif" width="15" height="10"></td>
    </tr>
  </form>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_b_l.gif" width="27" height="30"></td>
    <td class=imail_popup_blue_cell_b>&nbsp;</td>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupblue_b_r.gif" width="27" height="30"></td>
  </tr>
</table>




</form>

<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>