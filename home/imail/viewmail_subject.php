<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$linterface = new interface_html("imail_default.html");
$li = new libcampusmail2007($CampusMailID);


function bite_str($string, $start, $len, $encoding)
{
	//utf-8:$byte=3 | gb2312:$byte=2 | big5:$byte=2
    $str     = "";
    $count   = 0;
    
    if($encoding == 'big5'){
	    $byte = 2;
    }else if($encoding == 'utf-8'){
	    $byte = 3;
    }else{
	    $byte = 2;
    }
    
    $str_len = strlen($string);
    for ($i=0; $i<$str_len; $i++) {
        if (($count+1-$start)>$len) {
            $str  .= "...";
            break;
        } elseif ((ord(substr($string,$i,1)) <= 128) && ($count < $start)) {
            $count++;
        } elseif ((ord(substr($string,$i,1)) > 128) && ($count < $start)) {
            $count = $count+2;
            $i     = $i+$byte-1;
        } elseif ((ord(substr($string,$i,1)) <= 128) && ($count >= $start)) {
            $str  .= substr($string,$i,1);
            $count++;
        } elseif ((ord(substr($string,$i,1)) > 128) && ($count >= $start)) {
            $str  .= substr($string,$i,$byte);
            $count = $count+2;
            $i     = $i+$byte-1;
        }
    }
    return $str;
}

$outType = ($li->SenderID == $UserID && $li->CampusMailFromID == "");
$mailFolderID = $li->UserFolderID;
if($mailFolderID>0){
	$outType = false;
}

if ($viewtype == "search")
{

}
else
{
    $nextID = $li->returniMailNextID($mailFolderID);
    $prevID = $li->returniMailPrevID($mailFolderID);
}

if($li->MessageEncoding==""){
	if(isset($intranet_default_lang_set) && is_array($intranet_default_lang_set)){
		  if (in_array("b5",$intranet_default_lang_set))
		  {
		      $thisEncoding = "big5";
		  }
		  else
		  {
		      $thisEncoding = "GB2312";
		  }
	}else{
		$thisEncoding="big5";
	}	
}else{
	$thisEncoding = $li->MessageEncoding;
}

if(strtolower($thisEncoding)=="us-ascii"){
	if(isset($intranet_default_lang_set) && is_array($intranet_default_lang_set)){	
	  if (in_array("b5",$intranet_default_lang_set))
	  {
	      $thisEncoding = "big5";
	  }
	  else
	  {
	      $thisEncoding = "GB2312";
	  }	
	}
	
}
?>

<META http-equiv=Content-Type content='text/html; charset=<?=$thisEncoding?>'>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/topbar.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/leftmenu.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/footer.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/barchar.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/misc.css" rel="stylesheet" type="text/css" />
<? if($unread==0){ ?>
<style type="text/css" >
html, body
{
	background-color:#f5f5f5;
	background-image: url("");
	background-repeat: no-repeat;
	background-position: left top;
}
</style>
<? } ?>
<? if($unread==1){ ?>
<style type="text/css" >
html, body
{
	background-color:#FFFFFF;
	background-image: url("");
	background-repeat: no-repeat;
	background-position: left top;
}
</style>
<? } ?>
<base target="_parent" >
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table id="subjectDiv" width="100%" height="30px" border="0" cellspacing="0" cellpadding="0" align="left" >
<tr>
	<td width="100%" align="left" valign="center" class="tabletext">
	<?php 
		if($unread==0){
			if($li->IsAttachment){
				if(mb_strlen($li->Subject) > DEFAULT_INBOX_SUBJECT_FIELD_WIDTH){
					$Subject = bite_str($li->Subject,0,DEFAULT_INBOX_SUBJECT_FIELD_WIDTH,$li->MessageEncoding);
					echo "<a class='iMailsubject' href='viewmail.php?CampusMailID=$CampusMailID&preFolder=$FolderID' title='&nbsp;".$li->Subject."&nbsp;'>".$i_frontpage_campusmail_icon_attachment_2007."&nbsp;".$Subject."&nbsp;</a>";
				}else{
					echo "<a class='iMailsubject' href='viewmail.php?CampusMailID=$CampusMailID&preFolder=$FolderID' title='&nbsp;".$li->Subject."&nbsp;'>".$i_frontpage_campusmail_icon_attachment_2007."&nbsp;".$li->Subject."&nbsp;</a>";
				}
			}else{
				if(mb_strlen($li->Subject) > DEFAULT_INBOX_SUBJECT_FIELD_WIDTH){
					$Subject = bite_str($li->Subject,0,DEFAULT_INBOX_SUBJECT_FIELD_WIDTH,$li->MessageEncoding);
					echo "<a class='iMailsubject' href='viewmail.php?CampusMailID=$CampusMailID&preFolder=$FolderID' title='&nbsp;".$li->Subject."&nbsp;'>&nbsp;".$Subject."&nbsp;</a>";
				}else{
					echo "<a class='iMailsubject' href='viewmail.php?CampusMailID=$CampusMailID&preFolder=$FolderID' title='&nbsp;".$li->Subject."&nbsp;'>&nbsp;".$li->Subject."&nbsp;</a>";
				}
			}
		}else{
			if($li->IsAttachment){
				
				if(mb_strlen($li->Subject) > DEFAULT_INBOX_SUBJECT_FIELD_WIDTH){
					$Subject = bite_str($li->Subject,0,DEFAULT_INBOX_SUBJECT_FIELD_WIDTH,$li->MessageEncoding);
					echo "<a class='iMailsubjectunread' href='viewmail.php?CampusMailID=$CampusMailID&preFolder=$FolderID' title='&nbsp;".$li->Subject."&nbsp;'>".$i_frontpage_campusmail_icon_attachment_2007."&nbsp;".$Subject ."&nbsp;</a>";
				}else{
					echo "<a class='iMailsubjectunread' href='viewmail.php?CampusMailID=$CampusMailID&preFolder=$FolderID' title='&nbsp;".$li->Subject."&nbsp;'>".$i_frontpage_campusmail_icon_attachment_2007."&nbsp;".$li->Subject."&nbsp;</a>";
				}
			}else{
				
				if(mb_strlen($li->Subject) > DEFAULT_INBOX_SUBJECT_FIELD_WIDTH){
					$Subject = bite_str($li->Subject,0,DEFAULT_INBOX_SUBJECT_FIELD_WIDTH,$li->MessageEncoding);
					echo "<a class='iMailsubjectunread' href='viewmail.php?CampusMailID=$CampusMailID&preFolder=$FolderID' title='&nbsp;".$li->Subject."&nbsp;'>&nbsp;".$Subject ."&nbsp;</a>";
				}else{
					echo "<a class='iMailsubjectunread' href='viewmail.php?CampusMailID=$CampusMailID&preFolder=$FolderID' title='&nbsp;".$li->Subject."&nbsp;'>&nbsp;".$li->Subject."&nbsp;</a>";
				}
			}
		}
	?>	
	</td>
</tr>
</table>
<?php
	intranet_closedb();	
?>