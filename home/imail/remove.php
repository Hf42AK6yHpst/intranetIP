<?php
// using : 
/*
 * 2017-05-31 (Carlos): When remove multiple emails, remove the cached new mail counting records for the folders to let the system do recounting. 
 * 2017-03-06 (carlos): Update new mail counter.
 * 2014-04-14 (Carlos): Log $_SERVER['HTTP_REFERER']
 */
$PATH_WRT_ROOT = "../../"; 
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libcampusmail();
$iMail_insertDB_debug = true;
if ($page_from == "viewmail.php")
{
	$sql = "SELECT UserFolderID,RecordStatus FROM INTRANET_CAMPUSMAIL WHERE CampusMailID='".$li->Get_Safe_Sql_Query($CampusMailID)."'";
	$status_record = $li->returnResultSet($sql);
	if($status_record[0]['RecordStatus'] != 1)
	{
		$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $status_record[0]['UserFolderID'], -1);
    	$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], -1, 1);
	}
	
	//$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 1,DateModified=NOW() WHERE UserID = $UserID AND CampusMailID = $CampusMailID";
	$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 1, DateInFolder = NOW() WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailID)."'";
	$li->db_db_query($sql);

	if($iMail_insertDB_debug) {
		$sql = "SELECT CampusMailID,CampusMailFromID,UserFolderID,Subject,Message,DateInput,Attachment FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailID)."'";
		$MailContent = $li->returnResultSet($sql);
		
		if(sizeof($MailContent)>0) {
			//list($mail_subject, $mail_content) = $MailContent[0];
			
			$log_content .= "--------------------------------------------------\n";
			$log_content .= "From Page : ".$_SERVER['HTTP_REFERER']."\n";
			$log_content .= "Action : Move to Trash Box"."\n";
			$log_content .= "Date : ".date("Y-m-d H:i:s")."\n";
			$log_content .= "CampusMailID : ".$MailContent[0]['CampusMailID']."\n";
			$log_content .= "CampusMailFromID : ".$MailContent[0]['CampusMailFromID']."\n";
			$log_content .= "FolderID : ".$MailContent[0]['UserFolderID']."\n";
			$log_content .= "Subject : ".$MailContent[0]['Subject']."\n";
			$log_content .= "Message : ".$MailContent[0]['Message']."\n";
			$log_content .= "DateInput : ".$MailContent[0]['DateInput']."\n";
			$log_content .= "Attachment : ".$MailContent[0]['Attachment']."\n\n";
		}
	}
    
    $targetID = ($nextID == ""? $prevID: $nextID);
    if ($targetID != "")
    {
        $url = $page_from."?CampusMailID=".urlencode($targetID)."&preFolder=".urlencode($preFolder)."&msg=3";
    }
    else
    {
        $sql = "SELECT UserFolderID FROM INTRANET_CAMPUSMAIL WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailID)."'";
        $temp = $li->returnVector($sql);
        $url = "viewfolder.php?FolderID=".urlencode($temp[0])."&preFolder=".urlencode($preFolder)."&msg=3";
    }
}
else
{
	if(isset($CampusMailID)){
		if(!is_array($CampusMailID)){
			$CampusMailID = array($CampusMailID);
		}
		/*
		$sql = "SELECT UserFolderID, COUNT(*) as NewMailCount FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN (".implode(",", $CampusMailID).") AND (RecordStatus IS NULL OR RecordStatus='') AND Deleted<>1 GROUP BY UserFolderID";
		$count_records = $li->returnResultSet($sql);
		
		for($i=0;$i<count($count_records);$i++)
		{
			$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $count_records[$i]['UserFolderID'], -$count_records[$i]['NewMailCount']);
    		$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], -1, $count_records[$i]['NewMailCount']);
		}
		*/
		$sql = "SELECT DISTINCT UserFolderID FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND CampusMailID IN (".implode(",", IntegerSafe($CampusMailID)).")";
		$involved_folder_id_ary = $li->returnVector($sql);
		$involved_folder_id_ary[] = -1;
		// remove the cached new mail counting records to recount again, this would be the most precise method
		$sql = "DELETE FROM INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT WHERE UserID='".$li->Get_Safe_Sql_Query($UserID)."' AND FolderID IN (".implode(",",IntegerSafe($involved_folder_id_ary)).")";
		$li->db_db_query($sql);
		
    	//$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 1,DateModified=NOW() WHERE UserID = $UserID AND CampusMailID IN (".implode(",", $CampusMailID).")";
    	$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 1, DateInFolder = NOW() WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND CampusMailID IN (".implode(",", IntegerSafe($CampusMailID)).")";
    	$li->db_db_query($sql);
    	
    	if($iMail_insertDB_debug) {
    		for($i=0; $i<sizeof($CampusMailID); $i++)
    		{
				$sql = "SELECT CampusMailID,CampusMailFromID,UserFolderID,Subject,Message,DateInput,Attachment FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailID[$i])."'";
				$MailContent = $li->returnResultSet($sql);
				
				if(sizeof($MailContent)>0) {
					//list($mail_subject, $mail_content) = $MailContent[0];
					$log_content .= "--------------------------------------------------\n";
					$log_content .= "From Page : ".$_SERVER['HTTP_REFERER']."\n";
					$log_content .= "Action : Move to Trash Box"."\n";
					$log_content .= "Date : ".date("Y-m-d H:i:s")."\n";
					$log_content .= "CampusMailID : ".$MailContent[0]['CampusMailID']."\n";
					$log_content .= "CampusMailFromID : ".$MailContent[0]['CampusMailFromID']."\n";
					$log_content .= "FolderID : ".$MailContent[0]['UserFolderID']."\n";
					$log_content .= "Subject : ".$MailContent[0]['Subject']."\n";
					$log_content .= "Message : ".$MailContent[0]['Message']."\n";
					$log_content .= "DateInput : ".$MailContent[0]['DateInput']."\n";
					$log_content .= "Attachment : ".$MailContent[0]['Attachment']."\n\n";
				}
			}
		}
    	
    }
    $url = $_SERVER["HTTP_REFERER"];
    if($url!=""){
	    $x = explode("?",$url);
	    $url = $x[0];
        $url.="?FolderID=".urlencode($FolderID)."&msg=3";
	}
	
}

if($iMail_insertDB_debug)
{
	include("../../includes/libfilesystem.php");
	$fs = new libfilesystem();
	
	if (!is_dir("$intranet_root/file/mailDeleteLog")) {
		mkdir("$intranet_root/file/mailDeleteLog",0777);
	}
	
	if (!is_dir("$intranet_root/file/mailDeleteLog/u".$UserID)) {
		mkdir("$intranet_root/file/mailDeleteLog/u$UserID",0777);
	}
	$maillogPath = $intranet_root.'/file/mailDeleteLog/u'.$UserID;
	
	$logFilename = date('Y-m-d').'.log';

	$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
	
	fwrite($logFileHandle, $log_content);
	fclose($logFileHandle);
}

intranet_closedb();

if($url=="")
	$url = "index.php";
header("Location: $url");
?>