<?php
// Editing by 
/*
 * 2017-03-06 (carlos): Update new mail counter.
 * 2016-05-25 (Carlos): Update INTRANET_CAMPUSMAIL.Delete=0 to cater moving trashed emails to other folders.
 */
$PATH_WRT_ROOT = "../../"; 
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libcampusmail();
if (!$fromSearch)
{
    $conds = " AND UserFolderID = '".$li->Get_Safe_Sql_Query($FolderID)."'";
}
/*
// Updating unread mail num
if ($FolderID == 2)
{
	$Sql1 = "	SELECT
					COUNT(CampusMailID)
				FROM	
					INTRANET_CAMPUSMAIL 
				WHERE	
					UserID = '$UserID' AND CampusMailID IN (".implode(",", $CampusMailID).") $conds					
					AND (RecordStatus='' OR RecordStatus IS NULL) AND Deleted
			  ";
	$ReturnArr1 = $li->returnVector($Sql1);
	
	$TmpUnreadNo =  $ReturnArr1[0];
	
	$iNewCampusMail -= $TmpUnreadNo;
}
else if ($targetFolderID == 2)
{
	$Sql1 = "	SELECT
					COUNT(CampusMailID)
				FROM	
					INTRANET_CAMPUSMAIL 
				WHERE	
					UserID = '$UserID' AND CampusMailID IN (".implode(",", $CampusMailID).") $conds					
					AND (RecordStatus='' OR RecordStatus IS NULL)					
			  ";
	$ReturnArr1 = $li->returnVector($Sql1);
	
	$TmpUnreadNo =  $ReturnArr1[0];
	
	$iNewCampusMail += $TmpUnreadNo;
}
// End of updating unread mail num
*/

// update new mail counter
$sql = "SELECT IF(Deleted=1,-1,UserFolderID) as UserFolderID, COUNT(*) as NewMailCount,Deleted FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."' $conds AND CampusMailID IN (".implode(",", IntegerSafe($CampusMailID)).") AND (RecordStatus IS NULL OR RecordStatus='') GROUP BY UserFolderID,Deleted";
$count_records = $li->returnResultSet($sql);

for($i=0;$i<count($count_records);$i++)
{
	$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $count_records[$i]['UserFolderID'], -$count_records[$i]['NewMailCount']);
	$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $targetFolderID, $count_records[$i]['NewMailCount']);
}

$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = '".$li->Get_Safe_Sql_Query($targetFolderID)."',Deleted=0, DateInFolder = NOW() WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND CampusMailID IN (".implode(",", IntegerSafe($CampusMailID)).") $conds";
$li->db_db_query($sql);


$url = $_SERVER["HTTP_REFERER"];

intranet_closedb();
header("Location: $url");
?>