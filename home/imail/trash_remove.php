<?php
#Using By : 
##########
# Log :
# 2018-04-11 (Carlos): Brute force to recalculate used quota.
# 2017-08-25 (Carlos): Recalculate quota usage if removed any attachments.
# 2017-03-06 (Carlos): Update trash box new mail counter.
# 2014-04-14 (Carlos): Log $_SERVER['HTTP_REFERER']
# 2012-09-17 (Carlos):  added libwebmail->RemoveRawBodyLogFile() to remove internet mail raw message source log files
# 2012-04-18 (Carlos):  Added code to clean mail delete log files that live longer than 2 months
#
# Date		:	2011-07-22
# By		: 	Carlos
# Details	: 	Added delete attachment log
# 
# Date		:	unknown
# By		:	Kenneth:
# Details	:	For any change in the logic of removing mail records and attachment, pls also apply changes to
# 					/home/imail/trash_remove.php and /login.php
#
# Date		: 	20090818
# By		: 	Ronald
# Details	:	update the mailbox total quota used 
##########

$PATH_WRT_ROOT = "../../"; 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once("../../includes/libwebmail.php");
intranet_auth();
intranet_opendb();
#auth_campusmail();

$li = new libcampusmail();
$lf = new libfilesystem();
$lwebmail = new libwebmail();
# Step 1: Extract valid mail IDs
$CampusMailID = IntegerSafe($CampusMailID);
$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND Deleted = 1 AND CampusMailID IN (".implode(",", $CampusMailID).")";
$targetID = $li->returnVector($sql);


$iMail_insertDB_debug = true;
if (sizeof($targetID)!=0)
{
    $CampusMailIDStr = implode(",",$targetID);
    
    // decrement trash box new mail counter
    $sql = "SELECT COUNT(*) as NewMailCount FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN (".$CampusMailIDStr.") AND (RecordStatus IS NULL OR RecordStatus='')";
	$count_record = $li->returnVector($sql);
	if($count_record[0] > 0){
	    $li->UpdateCampusMailNewMailCount($_SESSION['UserID'], -1, -$count_record[0]);
	}
    
    $sql = "SELECT HeaderFilePath, MessageFilePath FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID IN (".$CampusMailIDStr.")";
	$FilePathArr = $li->returnArray($sql);
	//$HeaderFilePath = $FilePathArr[0]['HeaderFilePath'];
	//$MessageFilePath = $FilePathArr[0]['MessageFilePath'];
    
    ## create a delete log
    if($iMail_insertDB_debug) {
		//$fs = new libfilesystem();
		
		$sql = "SELECT CampusMailID,CampusMailFromID,UserFolderID,Subject,Message,DateInput,Attachment FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)";
		$MailContent = $li->returnResultSet($sql);
		
		if(sizeof($MailContent)>0) {
			for($i=0; $i<sizeof($MailContent); $i++) {
				//list($mail_id, $mail_subject, $mail_content) = $MailContent[$i];
				
				$log_content .= "--------------------------------------------------\n";
				$log_content .= "From Page : ".$_SERVER['HTTP_REFERER']."\n";
				$log_content .= "Action : Delete from Trash Box"."\n";
				$log_content .= "Date : ".date("Y-m-d H:i:s")."\n";
				$log_content .= "CampusMailID : ".$MailContent[$i]['CampusMailID']."\n";
				$log_content .= "CampusMailFromID : ".$MailContent[$i]['CampusMailFromID']."\n";
				$log_content .= "FolderID : ".$MailContent[$i]['UserFolderID']."\n";
				$log_content .= "Subject : ".$MailContent[$i]['Subject']."\n";
				$log_content .= "Message : ".$MailContent[$i]['Message']."\n";
				$log_content .= "DateInput : ".$MailContent[$i]['DateInput']."\n";
				$log_content .= "Attachment : ".$MailContent[$i]['Attachment']."\n\n";
			}
		}
		
		if (!is_dir("$intranet_root/file/mailDeleteLog")) {
			mkdir("$intranet_root/file/mailDeleteLog",0777);
		}
		
		if (!is_dir("$intranet_root/file/mailDeleteLog/u".$UserID)) {
			mkdir("$intranet_root/file/mailDeleteLog/u$UserID",0777);
		}
		$maillogPath = $intranet_root.'/file/mailDeleteLog/u'.$UserID;
		
		$logFilename = date('Y-m-d').'.log';
	
		$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
		
		fwrite($logFileHandle, $log_content);
		fclose($logFileHandle);
	}
	
	# Select internet mails CampusMailID for removing raw meesage log file
	$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND MailType = 2 AND CampusMailID IN ($CampusMailIDStr)"; 
	$to_remove_rawlog_campusmailid = $li->returnVector($sql);
	for($i=0;$i<count($to_remove_rawlog_campusmailid);$i++) {
		$lwebmail->RemoveRawBodyLogFile($to_remove_rawlog_campusmailid[$i]);
	}
	
    # Step 2: Extract Attachment paths
    $sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND MailType = 1 AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND CampusMailID IN ($CampusMailIDStr)";
    $to_remove_attachmentpaths_int = $li->returnVector($sql);
    $sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND MailType = 2 AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND CampusMailID IN ($CampusMailIDStr)";
    $to_remove_attachmentpaths_ext = $li->returnVector($sql);

    if ($debug_mode)
    {
        echo "Result in Step 2\n";
        print_r($to_remove_attachmentpaths_int);
        print_r($to_remove_attachmentpaths_ext);
    }
    $benchmark['step2 ends'] = time();
    
    # Newly added by Ronald on 20090818 
	# update the mailbox total quota used 
	$sql = "SELECT IFNULL(SUM(AttachmentSize),0) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND MailType IN (1,2) AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND CampusMailID IN ($CampusMailIDStr)";
	$TotalAttachmentSize = $li->returnVector($sql);
	$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = QuotaUsed - ".$TotalAttachmentSize[0]." WHERE UserID = '$UserID'";
	$li->db_db_query($sql);

    # Step 3: Remove reply records
    # Remove the replies for notification mail sent
    $sql = "DELETE FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID IN (".$CampusMailIDStr.")";
    $li->db_db_query($sql);

    # Step 4: Remove Mail DB records
	$sql = "DELETE FROM INTRANET_RAWCAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)";
    $li->db_db_query($sql);
    $sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND Deleted = 1 AND CampusMailID IN ($CampusMailIDStr)";
    $li->db_db_query($sql);

    # Step 5: Update Notification
    $sql = "UPDATE INTRANET_CAMPUSMAIL SET IsNotification = 0 WHERE CampusMailFromID IN ($CampusMailIDStr)";
    $li->db_db_query($sql);
    $benchmark['step5 ends'] = time();
	
	## Log down deleted attachment
	if($iMail_insertDB_debug){
		$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
	}
	
    # Step 6: Search for attachment path (Internal)
    //$lf = new libfilesystem();
    unset($list_path_string_to_remove);
    $delim = "";
    for ($i=0; $i<sizeof($to_remove_attachmentpaths_int); $i++)
    {
         $target_path = $to_remove_attachmentpaths_int[$i];
         if ($target_path != "")
         {
             $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE Attachment = '$target_path'";
             $temp = $li->returnVector($sql);
             if (is_array($temp) && sizeof($temp)==1 && $temp[0]==0 && preg_match('/^u\d+\/.+$/',$target_path))
             {
                 # Remove Database records
                 $list_path_string_to_remove .= "$delim'$target_path'";
                 $delim = ",";

                 # Remove actual files
                 $full_path = "$file_path/file/mail/".$target_path;
                 if ($bug_tracing['imail_remove_attachment'])
                 {
                     $command ="mv ".OsCommandSafe($full_path)." ".OsCommandSafe($full_path)."_bak";
                     exec($command);
                 }
                 else
                 {
                     $lf->lfs_remove($full_path);
                 }
                 
                 if($iMail_insertDB_debug && $logFileHandle){
                 	$log_content = "$sql\n";
                 	$log_content .= "COUNT ".$temp[0]."\n";
                 	$log_content .= "Deleted Internal Mail Path: $full_path\n";
                 	fwrite($logFileHandle, $log_content);
                 }
             }

         }
    }
    $benchmark['step6 ends'] = time();

    # Step 7: Remove Attachment path (External Mail)
    for ($i=0; $i<sizeof($to_remove_attachmentpaths_ext); $i++)
    {
         $target_path = $to_remove_attachmentpaths_ext[$i];
         if ($target_path != "" && preg_match('/^u\d+\/.+$/',$target_path))
         {
             # Remove Database records
             $list_path_string_to_remove .= "$delim'$target_path'";
             $delim = ",";

             # Remove actual files
             $full_path = "$file_path/file/mail/".$target_path;
             if ($bug_tracing['imail_remove_attachment'])
             {
                 $command ="mv ".OsCommandSafe($full_path)." ".OsCommandSafe($full_path)."_bak";
                 exec($command);
             }
             else
             {
                 $lf->lfs_remove($full_path);
             }
             
             if($iMail_insertDB_debug && $logFileHandle){
                 $log_content = "External Mail Attachment Path: $full_path\n";
                 fwrite($logFileHandle, $log_content);
             }
         }
    }

    # Remove Database Records
    if ($list_path_string_to_remove != "")
    {
        $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE AttachmentPath IN ($list_path_string_to_remove)";
        $li->db_db_query($sql);
    }
    $benchmark['step7 ends'] = time();
	
	if($iMail_insertDB_debug && $logFileHandle){
		if($list_path_string_to_remove != ""){
			$log_content = "INTRANET_IMAIL_ATTACHMENT_PART AttachmentPath: $list_path_string_to_remove\n\n";
            fwrite($logFileHandle, $log_content);
		}
		fclose($logFileHandle);
	}
	
	#Remove Original Email Source
	for($i=0;$i<count($FilePathArr);$i++) {
		$HeaderFilePath = $FilePathArr[$i]['HeaderFilePath'];
		$MessageFilePath = $FilePathArr[$i]['MessageFilePath'];
		if (trim($HeaderFilePath) != '' && trim($MessageFilePath) != '') {
			$HeaderFullPath = "$file_path/file/mail/".$HeaderFilePath;
			$MessageFullPath = "$file_path/file/mail/".$MessageFilePath;
			
			if (trim($HeaderFilePath) != '') {
				$lf->lfs_remove($HeaderFullPath);
			}
			if (trim($MessageFullPath) != '') {
				$lf->lfs_remove($MessageFullPath);
			}
		}
	}
	/*
	# Recalculate quota usage if removed any attachments
	if(count($to_remove_attachmentpaths_int)>0 || count($to_remove_attachmentpaths_ext)>0){
		$sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$UserID."' ";
   	 	$sum_record = $li->returnVector($sql);
   	 	$used_quota = round(intval($sum_record[0]),1);
   	 	$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed, RequestUpdate) VALUES ('".$UserID."','".$used_quota."','0') ON DUPLICATE KEY UPDATE QuotaUsed = '$used_quota',RequestUpdate='0'";
   	 	$li->db_db_query($sql);
	}
	*/
}

# Recalculate used quota
$sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID = ".$_SESSION['UserID'];
$sum_record = $li->returnVector($sql);
$used_quota = round(intval($sum_record[0]),1);
$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed, RequestUpdate) VALUES ('".$_SESSION['UserID']."','".$used_quota."','0') ON DUPLICATE KEY UPDATE QuotaUsed = '$used_quota',RequestUpdate='0'";
$li->db_db_query($sql);

// Clean mail delete log file that over certain period
if($iMail_insertDB_debug){
	$maillogPath = $intranet_root.'/file/mailDeleteLog/u'.$UserID;
	$cmd = 'find '.OsCommandSafe($maillogPath).' -maxdepth 1 -type f -mtime +60 -exec rm -rf "{}" \;';
	shell_exec($cmd);
	/*
	$tmpComposeFolders = $lf->return_folderlist($maillogPath);
	$thisTime = time();
	$removeBufferTime = 60*60*24*30*2; // about 2 month 
    clearstatcache();
    for($i=0;$i<sizeof($tmpComposeFolders);$i++){
    	$file_modified_time = @filectime($tmpComposeFolders[$i]);
    	// conditions to remove
    	// 1). it is a file; 2). file exist; 3) file has been there longer than buffer time 
    	if(is_file($tmpComposeFolders[$i]) && file_exists($tmpComposeFolders[$i]) && ($thisTime-$file_modified_time)>$removeBufferTime){
    		$lf->file_remove($tmpComposeFolders[$i]);
    	}
    }
    */
}

intranet_closedb();
header("Location: trash.php?msg=3");
?>