<?php
// Editing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();

$MODULE_OBJ['title'] = $i_CampusMail_New_CheckMail;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>
<script language="javascript" >
window.onload = function()
{
	if(top && top.process){
		top.process.location='checkmail_process2.php';
	}else if(parent && parent.process){
		parent.process.location='checkmail_process2.php';
	}
}
function goback()
{
	if(top){
		top.location.href = "viewfolder.php?FolderID=2&mailchk=1";
	}else if(parent){
		parent.location.href = "viewfolder.php?FolderID=2&mailchk=1";
	}
}
</script>
<html>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td align="right" ><div id="errmsg"></div></td>
	</tr>
	<? /*
	<tr>
		<td align="center" ><div id="msg"><?=$xcmsg?></div></td>
	</tr>
	*/?>
	<tr>
		<td align="center" >
		<table width="685" border="0" cellspacing="0" cellpadding="2" class="tabletext">
		<tr>
			<td align="right" valign="top" width="120" height="100">&nbsp;</td>
			<td width="230" align="left" valign="top">&nbsp;</td>
			<td width="335" align="left" valign="top">&nbsp;</td>
		</tr>
		<tr>
			<td align="right" valign="top">&nbsp;</td>
			<td align="left" valign="top" colspan="2">
			<div name="msg" id="msg"><?=$i_CampusMail_New_ConnectingMailServer?></div>
			<br />
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "top.location='viewfolder.php?FolderID=2&mailchk=1'") ?>
			</td>
		</tr>
		<tr>
			<td align="right" valign="top" width="120" height="100">&nbsp;</td>
			<td width="230" align="left" valign="top">&nbsp;</td>
			<td width="335" align="left" valign="top">&nbsp;</td>
		</tr>
		</table>		
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<form name="form1" action=""></form>
<?php
	$linterface->LAYOUT_STOP();
?>	
