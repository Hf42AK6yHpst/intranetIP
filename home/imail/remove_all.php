<?php
// Editing by 
/*
 * 2017-05-31 (Carlos): Remove the cached new mail counting records for the folders to let the system do recounting. 
 * 2017-03-06 (carlos): Update new mail counter.
 * 2014-04-14 (Carlos): Log $_SERVER['HTTP_REFERER']
 */
$PATH_WRT_ROOT = "../../"; 
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libcampusmail();

$url = $_SERVER["HTTP_REFERER"];
#$type = $RecordType;
if ($FolderID == "")   # Missing type
{
    header ("Location: $url");
    exit();
}
$iMail_insertDB_debug = true;
# $type from inbox.php, outbox.php, draft.php
/*
$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE RecordType = $type";
$targetID = $li->returnVector($sql);

$sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordType=RecordType+3 WHERE UserID = $UserID AND CampusMailID IN (".implode(",", $targetID).")";
$li->db_db_query($sql);
*/

/*
$sql = "SELECT COUNT(*) as NewMailCount FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '$FolderID' AND (RecordStatus IS NULL OR RecordStatus='') AND Deleted<>1";
$count_record = $li->returnVector($sql);
if($count_record[0] > 0){
	$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $FolderID, -$count_record[0]);
    $li->UpdateCampusMailNewMailCount($_SESSION['UserID'], -1, $count_record[0]);
}
*/
// remove the cached new mail counting records to recount again, this would be the most precise method
$involved_folder_id_ary = array($FolderID,-1);
$sql = "DELETE FROM INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT WHERE UserID='$UserID' AND FolderID IN (".implode(",",$involved_folder_id_ary).")";
$li->db_db_query($sql);

//$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 1,DateModified=NOW() WHERE UserID = $UserID AND UserFolderID = $FolderID";
$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 1, DateInFolder = NOW() WHERE UserID = '$UserID' AND UserFolderID = '$FolderID'";
$li->db_db_query($sql);

if($iMail_insertDB_debug) {
	include("../../includes/libfilesystem.php");
	$fs = new libfilesystem();
	
	$sql = "SELECT CampusMailID,CampusMailFromID,UserFolderID,Subject,Message,DateInput,Attachment FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '$FolderID'";
	$MailContent = $li->returnResultSet($sql);
	
	if(sizeof($MailContent)>0) {
		for($i=0; $i<sizeof($MailContent); $i++) {
			//list($mail_id, $mail_subject, $mail_content) = $MailContent[$i];
			
			$log_content .= "--------------------------------------------------\n";
			$log_content .= "From Page : ".$_SERVER['HTTP_REFERER']."\n";
			$log_content .= "Action : Move to Trash Box (remove all)"."\n";
			$log_content .= "Date : ".date("Y-m-d H:i:s")."\n";
			$log_content .= "CampusMailID : ".$MailContent[$i]['CampusMailID']."\n";
			$log_content .= "CampusMailFromID : ".$MailContent[$i]['CampusMailFromID']."\n";
			$log_content .= "FolderID : ".$MailContent[$i]['UserFolderID']."\n";
			$log_content .= "Subject : ".$MailContent[$i]['Subject']."\n";
			$log_content .= "Message : ".$MailContent[$i]['Message']."\n";
			$log_content .= "DateInput : ".$MailContent[$i]['DateInput']."\n";
			$log_content .= "Attachment : ".$MailContent[$i]['Attachment']."\n\n";
		}
	}
	
	if (!is_dir("$intranet_root/file/mailDeleteLog")) {
		mkdir("$intranet_root/file/mailDeleteLog",0777);
	}
	
	if (!is_dir("$intranet_root/file/mailDeleteLog/u".$UserID)) {
		mkdir("$intranet_root/file/mailDeleteLog/u$UserID",0777);
	}
	$maillogPath = $intranet_root.'/file/mailDeleteLog/u'.$UserID;
	
	$logFilename = date('Y-m-d').'.log';

	$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
	
	fwrite($logFileHandle, $log_content);
	fclose($logFileHandle);
	
}

intranet_closedb();
header("Location: $url");
?>