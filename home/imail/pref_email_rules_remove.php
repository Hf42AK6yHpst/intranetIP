<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libdb();

if(sizeof($ruleID)>0){
	$targetRuleID = implode(",",$ruleID);
}else{
	header("Location: pref_email_rule.php?msg=13");
}

$sql = "DELETE FROM INTRANET_IMAIL_MAIL_RULES WHERE RuleID IN ($targetRuleID)";
$result = $li->db_db_query($sql);

if($result){
	header("Location: pref_email_rules.php?msg=3");
}else{
	header("Location: pref_email_rules.php?msg=13");
}
intranet_closedb();
?>