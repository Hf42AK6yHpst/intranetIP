<?php
## Using : 
## Change Log ##
## 2019-10-31 (Carlos): Added checking on total attachment size exceed the limit, in case front end ajax checking failed.
## 2019-06-04 (Carlos): Added CSRF token checking. 
## 2017-06-13 (Carlos): Fixed update used quota issue by recalculating the total consumed quota. 
## 2017-03-06 (Carlos): Update new mail counter.
## 2014-11-06 (Carlos): Use find command to delete tmp folders because php get folder list is too slow
## 2013-05-24 (Carlos): set SendMail() parameter $nl2br to false for html message 
## 2013-05-24 (Carlos): for send internet mail with html message, remove all \n \r \r\n to avoid extra line break
## 2013-04-22 (Carlos): cater php magic quote by get_magic_quotes_gpc() to handle string DB insertion
##
## 2012-10-18 (Carlos): set INTRANET_CAMPUSMAIL_USED_STORAGE.RequestUpdate='1' for receivers
##
## 2012-06-07 (Carlos)
## - Clean tmp attachment folders
##
## 2011-04-19 (Carlos) 
## - Changed return location:
## 	 (1) save draft, go to draft folder
##	 (2) Reply, Reply All, Forward, go to folder where the mail located
##	 (3) Compose new mail or compose from draft, go to Inbox 
##
## 2010-09-13 (Ronald)
##  - Add a parameter "0" when calling function externalRecipientFormatting() (Only for iMail 1.2) 
## 
## 2010-05-25 (Ronald)
##  - Change the redirect page to viewmail.php when reply, reply all, forward, save as draft
##
## 2009-07-25 (Ronald) 
##	- set "utf-8" as default charset, $MessageCharset = "utf-8"
##

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	header('Location: /');
	exit;
}

intranet_auth();
intranet_opendb();
auth_campusmail();

$benchmark['init'] = time();

$is_magic_quotes_on = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");

if ($special_feature['imail_richtext'] && !($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod"))
{
    $use_html_editor = true;
}

# Store original chars
$original_subject = $Subject;
#$Subject = intranet_htmlspecialchars(trim($Subject));
#$Message = intranet_htmlspecialchars(trim($Message));
#$Message = intranet_undo_htmlspecialchars(trim($Message));
$li = new libcampusmail();
$lwebmail = new libwebmail();

$update_mail_counter = method_exists($li,'UpdateCampusMailNewMailCount');

# Process Check Boxes
if ($AllStaffTo==1 || $AllStaffCC==1 || $AllStaffBCC==1)
{
    $sql = "SELECT CONCAT('U',UserID) FROM INTRANET_USER WHERE RecordType = 1";
    $all_staff = $li->returnVector($sql);
    if (sizeof($all_staff)!=0)
    {
        if ($AllStaffTo==1)
        {
            $Recipient = array_merge($Recipient,$all_staff);
        }
        if ($AllStaffCC==1)
        {
            $InternalCC = array_merge($InternalCC,$all_staff);
        }
        if ($AllStaffBCC==1)
        {
            $InternalBCC = array_merge($InternalBCC,$all_staff);
        }
    }
}
if ($AllStudentTo==1 || $AllStudentCC==1 || $AllStudentBCC==1)
{
    $sql = "SELECT CONCAT('U',UserID) FROM INTRANET_USER WHERE RecordType = 2";
    $all_students = $li->returnVector($sql);
    if (sizeof($all_students))
    {
        if ($AllStudentTo==1)
        {
            $Recipient = array_merge($Recipient,$all_students);
        }
        if ($AllStudentCC==1)
        {
            $InternalCC = array_merge($InternalCC,$all_students);
        }
        if ($AllStudentBCC==1)
        {
            $InternalBCC = array_merge($InternalBCC,$all_students);
        }
    }
}
if ($AllParentTo==1 || $AllParentCC==1 || $AllParentBCC==1)
{
    $sql = "SELECT CONCAT('U',UserID) FROM INTRANET_USER WHERE RecordType = 3";
    $all_parents = $li->returnVector($sql);
    if (sizeof($all_parents))
    {
        if ($AllParentTo==1)
        {
            $Recipient = array_merge($Recipient,$all_parents);
        }
        if ($AllParentCC==1)
        {
            $InternalCC = array_merge($InternalCC,$all_parents);
        }
        if ($AllParentBCC==1)
        {
            $InternalBCC = array_merge($InternalBCC,$all_parents);
        }
    }
}
$benchmark['check all'] = time();

$actual_receivers_id = array();

if (sizeof($Recipient)!=0)
{
    $Recipient = array_unique($Recipient);
    $Recipient = array_values($Recipient);
    $RecipientID = implode(",", $Recipient);
    $actual_receivers_id = array_merge($actual_receivers_id,$Recipient);
    
    ### Internal To ###
	$RecipientID = '';	
	$InternalToList = implode(",",$Recipient);
	$actual_internal_to_array = $li->returnRecipientUserIDArrayWithQuota($InternalToList);
	if(sizeof($actual_internal_to_array)>0){
		for($i=0; $i<sizeof($actual_internal_to_array); $i++){
			list($uid, $uname) = $actual_internal_to_array[$i];
			$uid = "U".$uid;
			
			if($i!=0)
				$RecipientID = $RecipientID.",";
			$RecipientID = $RecipientID.$uid;

		}
	}
}

if (sizeof($InternalCC)!=0)
{
    $InternalCC = array_unique($InternalCC);
    $InternalCC = array_values($InternalCC);
    $InternalCCID = implode(",", $InternalCC);
    $actual_receivers_id = array_merge($actual_receivers_id,$InternalCC);
    
    ### Internal CC ###
	$InternalCCID = '';
	$InternalCCList = implode(",",$InternalCC);
	$actual_internal_cc_array = $li->returnRecipientUserIDArrayWithQuota($InternalCCList);
	if(sizeof($actual_internal_cc_array)>0){
		for($i=0; $i<sizeof($actual_internal_cc_array); $i++){
			list($uid, $uname) = $actual_internal_cc_array[$i];
			$uid = "U".$uid;
			
			if($i!=0)
				$InternalCCID = $InternalCCID.",";
			$InternalCCID = $InternalCCID.$uid;
		}
	}
	//echo $InternalCCID."<BR>";
}
if (sizeof($InternalBCC)!=0)
{
    $InternalBCC = array_unique($InternalBCC);
    $InternalBCC = array_values($InternalBCC);
    $InternalBCCID = implode(",", $InternalBCC);
    $actual_receivers_id = array_merge($actual_receivers_id,$InternalBCC);
    
    ### Internal BCC ###
	$InternalBCCID = '';
	$InternalBCCList = implode(",",$InternalBCC);
	$actual_internal_bcc_array = $li->returnRecipientUserIDArrayWithQuota($InternalBCCList);
	if(sizeof($actual_internal_bcc_array)>0){
		for($i=0; $i<sizeof($actual_internal_bcc_array); $i++){
			list($uid, $uname) = $actual_internal_bcc_array[$i];
			$uid = "U".$uid;
			
			if($i!=0)
				$InternalBCCID = $InternalBCCID.",";
			$InternalBCCID = $InternalBCCID.$uid;
		}
	}
	//echo $InternalBCCID."<BR>";
}

$benchmark['check recipient'] = time();


if (sizeof($actual_receivers_id)!=0)
{
    $actual_receivers_id = array_unique($actual_receivers_id);
    $actual_receivers_id = array_values($actual_receivers_id);
}
$actual_receivers_list = implode(",", $actual_receivers_id);
$benchmark['link all recipient'] = time();

# Process External Addresses
$ExternalTo = trim($ExternalTo);
$ExternalCC = trim($ExternalCC);
$ExternalBCC = trim($ExternalBCC);

################## Formatting Recipient to  "name" <address> ###############


########################## End Formatting Recipient ##################

if ($ExternalTo!="")
{
    $to_address = explode(";",$ExternalTo);
    $to_address_list = implode(",",$to_address);
    $tt = $li->externalRecipientFormatting($ExternalTo, 0);
    $to_address = $tt[0];
    $to_address_list = $tt[1];
}
if ($ExternalCC!="")
{
    $cc_address = explode(";",$ExternalCC);
    $cc_address_list = implode(",",$cc_address);
    $tt = $li->externalRecipientFormatting($ExternalCC, 0);
    $cc_address = $tt[0];
    $cc_address_list = $tt[1];
}
else $cc_address = array();
if ($ExternalBCC!="")
{
    $bcc_address = explode(";",$ExternalBCC);
    $bcc_address_list = implode(",",$bcc_address);
    $tt = $li->externalRecipientFormatting($ExternalBCC, 0);
    $bcc_address = $tt[0];
    $bcc_address_list = $tt[1];
}
else $bcc_address = array();

$benchmark['check external'] = time();


$actual_recipient_array = $li->returnRecipientUserIDArrayWithQuota($actual_receivers_list);

$benchmark['get quota for internal'] = time();

if (sizeof($actual_recipient_array)==0 && $ExternalTo=="" && $ExternalCC=="" && $ExternalBCC =="" && $SubmitType != 1)
{
    header("Location: compose.php?cmsg=1");
    exit();
}
$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$AttachmentStr = stripslashes($AttachmentStr);
#$personal_path = "$file_path/file/mail/u$UserID";
#$path = "$personal_path/$composeFolder";
$path = "$file_path/file/mail/$composeFolder";

if ($composeFolder == "")
{
    header("Location: compose.php");
    exit();
}

$lu = new libfilesystem();
/*
$lu->lfs_copy($path."tmp", $path);
$lu->lfs_remove($path."tmp");
*/
if (substr($path,-3) == "tmp")
{
    $target_path = substr($path,0, strlen($path)-3);
    $command ="mv ".OsCommandSafe($path)." ".OsCommandSafe($target_path);
    exec($command);
    $path = $target_path;
}

if (substr($composeFolder,-3) == "tmp")
{
    $composeFolder = substr($composeFolder,0,strlen($composeFolder)-3);
}

# retrieve the real file names
$tmp_ary = explode("|||",stripslashes($file_names));
$filename_ary = array();
for($f=0;$f<sizeof($tmp_ary);$f++){
        $fa = explode(":",$tmp_ary[$f]);
        $f_key = $fa[0];
        $f_name = $fa[1];
        $filename_ary[$f_key] = $f_name;
}
# end retrieve the real file names

$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
while (list($key, $value) = each($files)) 
{
	$encoded_filename = urlencode($files[$key][0]);
    if(!strstr($AttachmentStr,$encoded_filename) && !strstr(urlencode($AttachmentStr),$encoded_filename))
    {
		$lu->file_remove($path."/".$files[$key][0]);
    }
    else
    {
		$real_filename = $filename_ary[$encoded_filename];
		if($real_filename!="" && $files[$key][0]!=$real_filename)
			$lu->file_rename($path."/".$files[$key][0],$path."/".$real_filename);
	}

/*
     if(!strstr($AttachmentStr,$files[$key][0])){
          $lu->file_remove($path."/".$files[$key][0]);
     }
*/
}
$IsAttachment = (isset($Attachment)) ? 1 : 0;
#$Attachment = "u$UserID/".$composeFolder;
$Attachment = $composeFolder;
$RecordType = $SubmitType;


if ($Attachment=="") $IsAttachment = 0;

$benchmark['handle attachment'] = time();

if ($IsAttachment)
{
	# compute size of attachment
	$lsize = new libfiletable("",$path, 0,0,"");
	$files = $lsize->files;
	$size = 0;
	while (list($key, $value) = each($files)) 
	{
		$thisSize = ceil(filesize($path."/".$files[$key][0])/1024); // bytes to KB
        $size += $thisSize; //round($thisSize, 1); // KB
	}
}
else
{
    if ($composeFolder != "")
        $lu->lfs_remove($path);
}

// Check if total attachment size exceed the limit
$MaxAttachmentSize = isset($webmail_info['max_attachment_size'])?$webmail_info['max_attachment_size']:10240; // in KB
if($IsAttachment && $size > $MaxAttachmentSize){
	// clean up the new attachments and redirect back to compose page
	if($composeFolder != "" && $path != ''){
        $lu->lfs_remove($path);
	}
	intranet_closedb();
	header("Location: compose.php");
    exit();
}


# Clear tmp folders 
// old approach
/*
$tmpComposeTime = time();
$tmpComposeBufferTime = 24*60*60; // 1 day long compose buffer, tmp dir will hold for 1 day
$tmp_personal_path = "$file_path/file/mail/u$UserID";
$tmpComposeFolders = $lu->return_folderlist($tmp_personal_path);
//$tmpFoldersToRemove = array();
clearstatcache();
for($i=0;$i<sizeof($tmpComposeFolders);$i++){
	$file_modified_time = filectime($tmpComposeFolders[$i]);
	// ready to be removed tmp dir should fulfil these conditions
	// 1). it is a directory; 2). end with tmp; 3). dir has been there longer than buffer time 
	if(is_dir($tmpComposeFolders[$i]) && file_exists($tmpComposeFolders[$i]) && substr($tmpComposeFolders[$i],-3)=="tmp" && ($tmpComposeTime-$file_modified_time)>$tmpComposeBufferTime){
		if($lu->deleteDirectory($tmpComposeFolders[$i])){
			//$tmpFoldersToRemove[] = $tmpComposeFolders[$i];
		}
	}
}
*/
// new approach
$tmp_personal_path = "$file_path/file/mail/u$UserID";
$cmd = 'sudo find '.OsCommandSafe($tmp_personal_path).' -maxdepth 1 -type d -name "*tmp" -mtime +1 -exec rm -R "{}" \;';
shell_exec($cmd);

# end of clear tmp folders

$benchmark['counted size'] = time();

#$Subject = intranet_htmlspecialchars($Subject);
#$Message = intranet_htmlspecialchars($Message);
$Subject = $li->convertBadWords($Subject);
$Message = $li->convertBadWords($Message);
#$Subject = intranet_undo_htmlspecialchars($Subject);
#$Message = intranet_undo_htmlspecialchars($Message);

# determine the encoding for internal mail
//$MessageCharset = (($intranet_session_language=="gb")?"gb2312":"big5");
// update on 2007-10-03
/*
if(isset($intranet_default_lang_set) && is_array($intranet_default_lang_set)){
 if (in_array("b5",$intranet_default_lang_set))
  {
      $MessageCharset = "big5";
  }
  else
  {
      $MessageCharset = "GB2312";
  }
}else{
	$MessageCharset = "big5";
}
*/
// modified by Ronald - set "utf-8" as default //
$MessageCharset = "utf-8";
$t_encoding=$SubmitType==1?'':$MessageCharset;

if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	$isHTML = 0;
}

if ($action != "D" || $OriginalCampusMailID == "")
{
$sql = "
     INSERT INTO INTRANET_CAMPUSMAIL (
          UserID, SenderID, RecipientID,InternalCC,InternalBCC,
          ExternalTo,ExternalCC,ExternalBCC,
          Subject, Message,".($t_encoding!=""?"MessageEncoding,":"")."Attachment,
          IsAttachment, IsImportant, IsNotification,
          MailType,UserFolderID,
          RecordType, DateInput, DateModified, DateInFolder, AttachmentSize,isHTML
     )
     VALUES (
          '".$li->Get_Safe_Sql_Query($UserID)."', '".$li->Get_Safe_Sql_Query($UserID)."', '".$li->Get_Safe_Sql_Query($RecipientID)."','".$li->Get_Safe_Sql_Query($InternalCCID)."','".$li->Get_Safe_Sql_Query($InternalBCCID)."',
          '".$li->Get_Safe_Sql_Query($to_address_list)."','".$li->Get_Safe_Sql_Query($cc_address_list)."','".$li->Get_Safe_Sql_Query($bcc_address_list)."',
          '".$li->Get_Safe_Sql_Query($Subject)."', '".$li->Get_Safe_Sql_Query($Message)."',".($t_encoding!=""?"'".$li->Get_Safe_Sql_Query($t_encoding)."',":"")."'".$li->Get_Safe_Sql_Query($Attachment)."',
          '".$li->Get_Safe_Sql_Query($IsAttachment)."', '".$li->Get_Safe_Sql_Query($IsImportant)."', '".$li->Get_Safe_Sql_Query($IsNotification)."',
          1,'".$li->Get_Safe_Sql_Query($RecordType)."',
          '".$li->Get_Safe_Sql_Query($RecordType)."', now(), now(), now(), '".$li->Get_Safe_Sql_Query($size)."','".$li->Get_Safe_Sql_Query($isHTML)."'
     )";
$li->db_db_query($sql);
$CampusMailFromID = $li->db_insert_id();

	if($update_mail_counter)
	{
		// increment one for new mail counter, $SubmitType maybe Outbox or Draft box
		$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $SubmitType, 1);
	}
}
else  # Draft Edit
{
$sql = "
     UPDATE INTRANET_CAMPUSMAIL
     SET RecipientID = '".$li->Get_Safe_Sql_Query($RecipientID)."',
         InternalCC = '".$li->Get_Safe_Sql_Query($InternalCCID)."',
         InternalBCC = '".$li->Get_Safe_Sql_Query($InternalBCCID)."',
         ExternalTo = '".$li->Get_Safe_Sql_Query($to_address_list)."',
         ExternalCC = '".$li->Get_Safe_Sql_Query($cc_address_list)."',
         ExternalBCC = '".$li->Get_Safe_Sql_Query($bcc_address_list)."',
          Subject = '".$li->Get_Safe_Sql_Query($Subject)."',
          Message = '".$li->Get_Safe_Sql_Query($Message)."',".
          ($t_encoding!=""?"MessageEncoding='".$li->Get_Safe_Sql_Query($t_encoding)."',":"")."
          IsAttachment = '".$li->Get_Safe_Sql_Query($IsAttachment)."',
          IsImportant  = '".$li->Get_Safe_Sql_Query($IsImportant)."',
          IsNotification = '".$li->Get_Safe_Sql_Query($IsNotification)."',
          RecordType = '".$li->Get_Safe_Sql_Query($RecordType)."',
          UserFolderID = '".$li->Get_Safe_Sql_Query($RecordType)."',
          AttachmentSize = '".$li->Get_Safe_Sql_Query($size)."',
          isHTML = '".$li->Get_Safe_Sql_Query($isHTML)."',
          DateInput = now(),
          DateModified = now()
     WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($OriginalCampusMailID)."' AND UserID = '".$li->Get_Safe_Sql_Query($UserID)."'
     ";
$li->db_db_query($sql);
$CampusMailFromID = $OriginalCampusMailID;

	if($SubmitType == 0) // drafted email is sent
	{
		if($update_mail_counter)
		{
			$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $SubmitType, 1); // increment inbox by one
			$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], 1, -1); // decrement draft box by one
		}
	}
}

if($CampusMailFromID == 0) {
	header("Location: viewfolder.php?msg=-1");
	intranet_closedb();
	exit;
}

$benchmark['save outbox'] = time();

if($SubmitType==0){			## $SubmitType: 0 - Send, 1 - Save As Draft
     # return all recipients UserID

     # check each recipients quota

     $campusmail_sql_header = "INSERT INTO INTRANET_CAMPUSMAIL (CampusMailFromID, UserID, RecordType) VALUES ";
     $replies_sql_header = "INSERT INTO INTRANET_CAMPUSMAIL_REPLY (CampusMailID, UserID, UserName,IsRead,DateInput,DateModified) VALUES ";
     $sql = $campusmail_sql_header;
     $row = $actual_recipient_array;
     $replies_values = "";
     $delimiter = "";
     $users_no_space = array();
     $users_count = 0;

        //get all user's quota
		$ReceiversList = 0;
		for($i=0; $i<sizeof($row); $i++){
			$ReceiversList .= ",".$row[$i][0];
		}
$benchmark['prepared user list'] = time();

		## Disabled By Ronald on 20090818 ##
		## Reason : Process time is too low ##
        ##$sql_quota = "SELECT UserID, SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID IN ($ReceiversList) GROUP BY UserID";
        ##$row_quota = $li->returnArray($sql_quota, 2);
        ##for ($i=0; $i<sizeof($row_quota); $i++)
        ##{
        ##        $user_quota_used[$row_quota[$i][0]] = (int)$row_quota[$i][1];
        ##}
        
        ## added by Ronald on 20090818 ##
        ## New method to check the recipient used quota ##
        $sql_quota = "SELECT UserID, QuotaUsed FROM INTRANET_CAMPUSMAIL_USED_STORAGE WHERE UserID IN ($ReceiversList)";
        $row_quota = $li->returnArray($sql_quota, 2);
        for ($i=0; $i<sizeof($row_quota); $i++)
        {
        	$user_quota_used[$row_quota[$i][0]] = (int)$row_quota[$i][1]; // KB
        }
$benchmark['got used quota'] = time();
	//echo "used quota: ";debug_pr($user_quota_used);
	//echo "receivers: ";debug_pr($row);
	//echo "attachment size: ";echo $size;
     #$lquota = new libcampusquota();
     #TotalQuota = $lquota->QuotaArray;
     for($i=0; $i<sizeof($row); $i++){
          $ReceiverID = $row[$i][0];
          $ReceiverName = $row[$i][1];
          $ReceiverQuota = str_replace(":","",$row[$i][2]); // MB

			//$lcTarget = new libcampusquota($ReceiverID);
			//optimized
			//$left = $lcTarget->returnQuota()*1024 - $lcTarget->returnUsedQuota();
			$left = (is_integer($user_quota_used[$ReceiverID])) ? $ReceiverQuota*1024 - $user_quota_used[$ReceiverID] : $ReceiverQuota*1024; // KB

			if ($size > $left)
			{
			    $users_no_space[$users_count] = $ReceiverID;
			    $users_count++;
			    continue;
			}
		  # CampusMailFromID, UserID, RecordType only
          $sql .= "$delimiter($CampusMailFromID, $ReceiverID,'2')";
          //$replies_values .= "$delimiter($CampusMailFromID,$ReceiverID,'".addslashes($ReceiverName)."',0,now(),now())";
          $replies_values .= "$delimiter($CampusMailFromID,$ReceiverID,'".$li->Get_Safe_Sql_Query($ReceiverName)."',0,now(),now())";
          $delimiter = ",";
          
          if($update_mail_counter)
          {
          	// increment inbox by 1 new mail for each receiver
          	$li->UpdateCampusMailNewMailCount($ReceiverID, 2, 1);
          }
     }
$benchmark['built mail sql'] = time();
	$li->db_db_query($sql);
	
	## Internal Mail SQL Log - part 1 ##
	$insert_log_content .= "built mail sql : ".$sql."\n";
$benchmark['exe mail sql'] = time();

     # Update all mails just inserted
     /*
     $sql = "UPDATE INTRANET_CAMPUSMAIL SET
             SenderID = $UserID, RecipientID = '$RecipientID',Subject='".$li->Get_Safe_Sql_Query($Subject)."',Message='".$li->Get_Safe_Sql_Query($Message)."'
             ,MessageEncoding = '$MessageCharset'
             ,Attachment='$Attachment', IsAttachment='$IsAttachment'
             ,IsImportant='$IsImportant', IsNotification = '$IsNotification'
             ,AttachmentSize = '$size', DateInput = now(), DateModified = now(), DateInFolder = now(), 
             UserFolderID = 2, MailType = 1,
             ExternalTo = '$to_address_list',ExternalCC = '$cc_address_list',
             InternalCC = '$InternalCCID',
             isHTML = '$isHTML'
             WHERE CampusMailFromID = $CampusMailFromID";
	*/
	$sql = "UPDATE INTRANET_CAMPUSMAIL SET
             SenderID = '".$li->Get_Safe_Sql_Query($UserID)."', RecipientID = '".$li->Get_Safe_Sql_Query($RecipientID)."',Subject='".$li->Get_Safe_Sql_Query($Subject)."',Message='".$li->Get_Safe_Sql_Query($Message)."'
             ,MessageEncoding = '".$li->Get_Safe_Sql_Query($MessageCharset)."'
             ,Attachment='".$li->Get_Safe_Sql_Query($Attachment)."', IsAttachment='".$li->Get_Safe_Sql_Query($IsAttachment)."'
             ,IsImportant='".$li->Get_Safe_Sql_Query($IsImportant)."', IsNotification = '".$li->Get_Safe_Sql_Query($IsNotification)."'
             ,AttachmentSize = '".$li->Get_Safe_Sql_Query($size)."', DateInput = now(), DateModified = now(), DateInFolder = now(), 
             UserFolderID = 2, MailType = 1,
             ExternalTo = '".$li->Get_Safe_Sql_Query($to_address_list)."',ExternalCC = '".$li->Get_Safe_Sql_Query($cc_address_list)."',
             InternalCC = '".$li->Get_Safe_Sql_Query($InternalCCID)."',
             isHTML = '".$li->Get_Safe_Sql_Query($isHTML)."'
             WHERE CampusMailFromID = '".$li->Get_Safe_Sql_Query($CampusMailFromID)."'";
	$li->db_db_query($sql);
	
	## Internal Mail SQL Log - part 2 ##
	$insert_log_content .= "update info sql : ".$sql."\n";
	
$benchmark['exe update info sql'] = time();
     if ($IsNotification)
     {
         $replies_sql = "$replies_sql_header $replies_values";
         $li->db_db_query($replies_sql);
         
         ## Internal Mail SQL Log - part 3 ##
         $insert_log_content .= "update campusmail reply sql : ".$replies_sql."\n";
     }
     $lwebmail->iMail_InternalMailSQL_log($iMail_insertDB_debug,$CampusMailFromID,$insert_log_content);
$benchmark['exe reply sql'] = time();

     #### Store Attachment details to DB     
     $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailFromID)."'";
     $li->db_db_query($sql);

     if ($IsAttachment)
     {
         # compute size of attachment
         $lsize = new libfiletable("",$path, 0,0,"");
         $files = $lsize->files;
         $size = 0;
         $values = "";
         $delim = "";
         $TotalFileSize = 0;
         while (list($key, $value) = each($files)) {
                list($filename, $filesize) = $files[$key];
                $filename = addslashes($filename);
                $filesize = ceil($filesize/1024);
                $values .= "$delim('".$li->Get_Safe_Sql_Query($CampusMailFromID)."','".$li->Get_Safe_Sql_Query($Attachment)."', '".$li->Get_Safe_Sql_Query($filename)."', '".$li->Get_Safe_Sql_Query($filesize)."')";
                $delim = ",";
                $TotalFileSize = $TotalFileSize+$filesize;
         }
         $sql = "INSERT IGNORE INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID, AttachmentPath, FileName, FileSize)
                        VALUES $values";
         $li->db_db_query($sql);
         // sent copy occupy some quota for the attachment
         //$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = QuotaUsed + $TotalFileSize WHERE UserID = '$UserID'";
         //$li->db_db_query($sql);
         // each recipient used some quota for the attacments
         for($i=0; $i<sizeof($row); $i++){
         	$ReceiverID = $row[$i][0];
          	$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = QuotaUsed + $TotalFileSize WHERE UserID = '".$li->Get_Safe_Sql_Query($ReceiverID)."'";
         	$li->db_db_query($sql);
         }
         
          $sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
       	  $sum_record = $li->returnVector($sql);
       	  $used_quota = round(intval($sum_record[0]),1);
       	  $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed, RequestUpdate) VALUES ('".$li->Get_Safe_Sql_Query($UserID)."','".$used_quota."','0') ON DUPLICATE KEY UPDATE QuotaUsed = '$used_quota',RequestUpdate='0'";
       	  $li->db_db_query($sql);
     }
$benchmark['exe attach sql'] = time();

     # Send Internet Email
     if ($ExternalTo != "" || $ExternalCC != "" || $ExternalBCC != "")
     {
         # get display name / reply email from preference
         $sql_pref = "SELECT DisplayName,ReplyEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='".$li->Get_Safe_Sql_Query($UserID)."'";
         $result_pref = $li->returnArray($sql_pref,2);
         list($p_display_name,$p_reply_email)=$result_pref[0];

         $exmail_tried = true;
         $lwebmail = new libwebmail();
         $luser = new libuser($UserID);
         $attachment_actual_path = "$intranet_root/file/mail/$Attachment";
         $sender_email = $luser->UserLogin."@".$lwebmail->mailaddr_domain;

         # determine sender name
         if($p_display_name !=""){
			 $sender_name = $p_display_name;
         }
         else if ($sender_email == "charles@broadlearning.com")
         {
             $sender_name = "Charles Cheng";
         }
         else
         {
         	if($luser->RecordType == 2)
         	{
				$sql = "SELECT ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
         		$StudentClassInfo = $li->returnVector($sql);
         	
         		if(sizeof($StudentClassInfo)>0){
	         		$sender_name = $StudentClassInfo[0];
    	     	}else{
        	 		$sender_name = $luser->UserName();
         		}	
         	}else{
         		$sender_name = $luser->UserName();
         	}
         }

         # determine reply email
         /*
         if($p_reply_email !="")
         {
            $reply_addr = "<".$p_reply_email.">";
         }
         else
         {
             $reply_addr = "<".$luser->UserLogin."@".$lwebmail->mailaddr_domain.">";
         }
         */

         # for undelivery
         $email_return_path = $luser->UserLogin."@".($iMail_Alias_Domain ? $webmail_info['aliasdomain'] : $lwebmail->mailaddr_domain);

         if ($sender_name != "")
         {

             $emailheader_from = "\"".$sender_name."\" <".$email_return_path.">";
         }
         else
         {
             $emailheader_from = $email_return_path; #$luser->UserLogin."@".$lwebmail->mailaddr_domain;
         }
         #$email_msg = intranet_undo_htmlspecialchars($Message);
         $email_subj = stripslashes($original_subject);
         $email_msg = stripslashes($Message);
         //if($use_html_editor) {
         //	$email_msg = str_replace(array("\n","\r","\r\n"),"",$email_msg);
         //}
         
         //$exmail_success = $lwebmail->SendMail($email_subj,$email_msg,$emailheader_from,$to_address,$cc_address,$bcc_address,$attachment_actual_path,$IsImportant,$email_return_path,$p_reply_email);
         $to = array();
         $cc = array();
         $bcc = array();
         for($i=0; $i<sizeof($to_address); $i++){
         		if($to_address[$i] != ""){
         				$to[] = $to_address[$i];
         		}
         }
         for($i=0; $i<sizeof($cc_address); $i++){
         		if($cc_address[$i] != ""){
         				$cc[] = $cc_address[$i];
         		}
         }
         for($i=0; $i<sizeof($bcc_address); $i++){
         		if($bcc_address[$i] != ""){
         				$bcc[] = $bcc_address[$i];
         		}
         }
         
         $total_recipient = sizeof($to)+sizeof($cc)+sizeof($bcc);
         
         if($iMail_NumOfRecipientPerTime == "")
         	$iMail_NumOfRecipientPerTime = 150;
         	
         if($total_recipient > $iMail_NumOfRecipientPerTime){
         	$toSend = array_merge($to,$cc,$bcc);
         	$toSend = split_array($toSend, $iMail_NumOfRecipientPerTime);
         	$toArray = array($iMail_DummyAddress);
         	$ccArray = array();
         	for($i=0; $i<sizeof($toSend); $i++){
         		$exmail_success = $lwebmail->SendMail($email_subj,$email_msg,$emailheader_from,$toArray,$ccArray,$toSend[$i],$attachment_actual_path,$IsImportant,$email_return_path,$p_reply_email,null,!$use_html_editor);
         	}
         }else{
         	## send email in normal way (no. of recipeints not exist the limit)
         	$exmail_success = $lwebmail->SendMail($email_subj,$email_msg,$emailheader_from,$to_address,$cc_address,$bcc_address,$attachment_actual_path,$IsImportant,$email_return_path,$p_reply_email,null,!$use_html_editor);
         }
     }
     else $exmail_tried = false;
$benchmark['sent ext mail'] = time();


     # Display undelivery
     if ($users_count != 0)
     {
         $username_field = getNameFieldWithClassNumberByLang("");
         $sql = "SELECT $username_field FROM INTRANET_USER WHERE UserID IN (".implode(",",IntegerSafe($users_no_space)).")";
         $list = $li->returnVector($sql);
         $x = "";
         for ($i=0; $i<sizeof($list); $i++)
         {
              $x .= "<tr><td></td><td>".$list[$i]."</td></tr>\n";
         }
         $navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;
         
         
         include($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
         //include ("../../templates/homeheader.php");
         
		$CurrentPage = "PageComposeMail";
		$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

		# tag information
		$TAGS_OBJ[] = array($i_CampusMail_New_ComposeMail, "", 0);

		$linterface = new interface_html();
		$linterface->LAYOUT_START();
         
         
?>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="left">	
		<tr>
			<td align="center" >
			<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabletext" >			
			<tr>
				<td align="right" valign="top" width="80" >&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			</tr>			
			<tr>
				<td align="right" valign="top">&nbsp;</td>
				<td align="left" valign="top" colspan="2" >								
				<table border="0" cellpadding="3" cellspacing="0" class="systemmsg">
				<tr>
					<td >&nbsp;<?=$Lang['iMail']['FieldTitle']['NonDelivery']?>&nbsp;</td>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td align="right" valign="top" >&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			</tr>			
			<?=$x?>
			<?php 
			if ($exmail_tried) 
			{ 
				if ($exmail_success) 
				{ 
			?>
				<tr>
					<td width="80" align="right" valign="top" ></td>
					<td  ><?=$i_CampusMail_New_ExternalSentSuccess?></td>
				</tr>
			<?php 
				} else { 
			?>
				<tr>
					<td width="80" align="right" valign="top"></td>
					<td ><?=$i_CampusMail_New_ExternalSentFailed?></td>
				</tr>
			<?php 
				} 
			} 
			?>
			
			<tr>
				<td align="right" valign="top" width="80" height="100">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			</tr>			
			</table>		
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>

<?
         //include ("../../templates/homefooter.php");
         $linterface->LAYOUT_STOP();	

     }
}
else  # SubmitType = 1  (Save as Draft)
{
     #### Store Attachment details to DB
     $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailFromID)."'";
     $li->db_db_query($sql);
     if ($IsAttachment)
     {
         # compute size of attachment
         $lsize = new libfiletable("",$path, 0,0,"");
         $files = $lsize->files;
         $size = 0;
         $values = "";
         $delim = "";
         $TotalFileSize = 0;
         while (list($key, $value) = each($files)) {
                list($filename, $filesize) = $files[$key];
                $filename = addslashes($filename);
                $filesize = ceil($filesize/1024);
                $values .= "$delim('".$li->Get_Safe_Sql_Query($CampusMailFromID)."','".$li->Get_Safe_Sql_Query($Attachment)."', '".$li->Get_Safe_Sql_Query($filename)."', '".$li->Get_Safe_Sql_Query($filesize)."')";
                $delim = ",";
                $TotalFileSize = $TotalFileSize + $filesize;
         }
         $sql = "INSERT IGNORE INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID, AttachmentPath, FileName, FileSize)
                        VALUES $values";
         $li->db_db_query($sql);
         
         //$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = QuotaUsed + $TotalFileSize WHERE UserID = '$UserID'";
         //$li->db_db_query($sql);
         $sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
       	 $sum_record = $li->returnVector($sql);
       	 $used_quota = round(intval($sum_record[0]),1);
       	 $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed, RequestUpdate) VALUES ('".$li->Get_Safe_Sql_Query($UserID)."','".$used_quota."','0') ON DUPLICATE KEY UPDATE QuotaUsed = '$used_quota',RequestUpdate='0'";
       	 $li->db_db_query($sql);
     }
$benchmark['exe attach sql'] = time();

}

// Notify all users to recalculate his/her total used quota 
$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET RequestUpdate='1'";
$li->db_db_query($sql);

$benchmark['after all'] = time();
$exmsg = (!$exmail_success?1:0);
$signal = ($SubmitType==0) ? 1:2;
#$url = ($SubmitType==0) ? "viewfolder.php?FolderID=0&signal=1&exmsg=$exmsg" : "viewfolder.php?FolderID=1&signal=2";
#$url = "viewfolder.php?FolderID=2&signal=$signal&exmsg=$exmsg";
/*
if($action == "D" || $action == "R" || $action == "RA" || $action == "F"){
	$url = "viewmail.php?CampusMailID=$CampusMailFromID&signal=$signal&exmsg=$exmsg";
}else{
	$url = "viewfolder.php?FolderID=2&signal=$signal&exmsg=$exmsg";
}
*/
if($SubmitType == 1){
	//Save as draft, go to Draft folder
	$url = "viewfolder.php?FolderID=1&signal=$signal&exmsg=$exmsg";
}else if($action == "R" || $action == "RA" || $action == "F"){
	//Reply, Reply all, Forward, go to previous folder
	if($preFolder!="")
		$url = "viewfolder.php?FolderID=".urlencode($preFolder)."&signal=$signal&exmsg=$exmsg";
	else 
		$url = "viewmail.php?CampusMailID=".urlencode($CampusMailFromID)."&signal=$signal&exmsg=$exmsg";
}else{
	//Compose new mail or from draft, go to Inbox 
	$url = "viewfolder.php?FolderID=2&signal=$signal&exmsg=$exmsg";
}

$composeFolder = "";
session_unregister_intranet("composeFolder");

intranet_closedb();

if ($users_count == 0)
{
    header("Location: $url");
$benchmark['finished all'] = time();
}

#print_r($benchmark);
function split_array($a, $count)
{
         if ($count == 0) return array($a);

         $i = 0;
         while (list ($key, $val) = each ($a))
         {
                $temp[] = $val;
                $i++;
                if ($i == $count)
                {
                    $result[] = $temp;
                    unset($temp);
                    $i = 0;
                }
         }
         if (sizeof($temp)!=0) $result[] = $temp;
         return $result;
}
?>