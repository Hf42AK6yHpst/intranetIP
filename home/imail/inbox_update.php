<?php
// editing by 
##########################################################
## Modification Log
## 2011-11-07: Carlos
## add preFolder to return url that can go back to the correct folder after submit reply
##
## 2010-09-17: Yuen
## control by $sys_custom['iMail1.2_Reply2Inbox'], whether to move replied mail from outbox to inbox
##
## 2009-12-01: Yuen
## when a user reply internal mail with message,
## sender's email will be set to unread and move to inbox for the sender to know it is a new reply


include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libcampusmail.php");
include("../../includes/libaccess.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$Message = htmlspecialchars(trim($Message));

$CampusMailID = IntegerSafe($CampusMailID);

$li = new libcampusmail($CampusMailID);
$CampusMailFromID = $li->CampusMailFromID;
$preFolder = $li->UserFolderID;

if (trim($Message))
{
    $IsRead = 1;
}

if ($li->isAutoReply())
{
    $IsRead = 1;
}

if($li->UserID && $li->IsNotification){
/*
     if($IsRead){
          $sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = '1' WHERE CampusMailID = $CampusMailID AND UserID = $UserID";
          $li->db_db_query($sql);
     }
     */
     $sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = '2' WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailID)."' AND UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
     $li->db_db_query($sql);

     $sql = "SELECT CampusMailReplyID, IsRead, Message FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailFromID)."' AND UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
     $row = $li->returnArray($sql, 3);

     if(sizeof($row)==0){
          # INSERT
          $sql = "INSERT INTO INTRANET_CAMPUSMAIL_REPLY (CampusMailID, UserID, UserName, Message, IsRead, DateInput, DateModified) VALUES ('".$li->Get_Safe_Sql_Query($CampusMailFromID)."', '".$li->Get_Safe_Sql_Query($UserID)."', '".$li->Get_Safe_Sql_Query($li->returnUserName())."', '".$li->Get_Safe_Sql_Query($Message)."', '1', now(), now())";
     } else {
          # UPDATE
          if ($row[0][1]=="" || $row[0][2]=="")
          {
              $sql = "UPDATE INTRANET_CAMPUSMAIL_REPLY SET Message = '".$li->Get_Safe_Sql_Query($Message)."', IsRead = '1', DateModified = now() WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailFromID)."' AND UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
          }
     }
     $li->db_db_query($sql);

     # when a user reply internal mail with message, sender's email will be set to unread and move to inbox for the sender to know it is a new reply
     if ($sys_custom['iMail1.2_Reply2Inbox'] && trim($Message)!="")
     {
		$sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = NULL, UserFolderID=2 WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailFromID)."'";
		$li->db_db_query($sql);
     }
}

intranet_closedb();
header("Location: viewmail.php?viewtype=".urlencode($viewtype)."&CampusMailID=".urlencode($CampusMailID)."&preFolder=".urlencode($preFolder));
?>