<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libaccess.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
#auth_campusmail();

# Get general information
$lc = new libcampusmail();
$username_field = getNameFieldWithClassNumberByLang("b."); //($chi? "ChineseName": "EnglishName");
$username_field2 = getNameFieldWithClassNumberByLang("a."); //($chi? "ChineseName": "EnglishName");

$sql = "SELECT
              a.CampusMailID, a.CampusMailFromID,
              DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i'),
              a.Subject, a.Message, a.SenderEmail,
              a.SenderID,
              IF(b.UserID IS NULL, '$i_general_sysadmin', $username_field),
              a.RecipientID, a.InternalCC, a.InternalBCC,
              a.ExternalTo, a.ExternalCC, a.ExternalBCC,
              a.IsNotification
        FROM INTRANET_CAMPUSMAIL AS a
                LEFT OUTER JOIN INTRANET_USER AS b
                     ON a.SenderID = b.UserID
                WHERE
                     a.UserID = '$UserID' AND
                     a.UserFolderID = '$UserFolderID'
                     ORDER BY a.DateModified
        ";
$data = $lc->returnArray($sql, 15);
$content = "";
$spliter = "";
for ($i=0; $i<sizeof($data); $i++)
{
     $content .= $spliter;
     list($t_CampusMailID, $t_CampusMailFromID, $t_Datetime, $t_Subject, $t_Message,
          $t_SenderEmail, $t_SenderID, $t_SenderName,
          $t_InternalTo, $t_InternalCC, $t_InternalBCC,
          $t_ExternalTo, $t_ExternalCC, $t_ExternalBCC, $t_isNotification) = $data[$i];
     $is_outgoing = ($t_SenderID == $UserID && $t_CampusMailFromID=="");
     if ($is_outgoing)
     {
         $t_sender_string = ($t_SenderEmail != ""? $t_SenderEmail: $t_SenderName);
         $content .= "$i_frontpage_campusmail_date: $t_Datetime\n";
         $content .= "$i_frontpage_campusmail_sender: $t_sender_string\n";
         if ($t_InternalTo!="")
         {
             $array_recipient = $lc->returnRecipientOption($t_InternalTo);
             if (sizeof($array_recipient)>0)
             {
                 $recip_delimiter = "";
                 $text_recipient = "";
                 for ($j=0; $j<sizeof($array_recipient); $j++)
                 {
                      list($id,$name) = $array_recipient[$j];
                      $text_recipient .= "$recip_delimiter $name";
                      $recip_delimiter = ",";
                 }
                 $content .= "$i_CampusMail_New_InternalRecipients ($i_CampusMail_New_To): ";
                 $content .= "$text_recipient\n";
             }
         }
         if ($t_InternalCC!="")
         {
             $array_recipient = $lc->returnRecipientOption($t_InternalCC);
             if (sizeof($array_recipient)>0)
             {
                 $recip_delimiter = "";
                 $text_recipient = "";
                 for ($j=0; $j<sizeof($array_recipient); $j++)
                 {
                      list($id,$name) = $array_recipient[$j];
                      $text_recipient .= "$recip_delimiter $name";
                      $recip_delimiter = ",";
                 }
                 $content .= "$i_CampusMail_New_InternalRecipients ($i_CampusMail_New_CC): ";
                 $content .= "$text_recipient\n";
             }
         }
         if ($t_InternalBCC!="")
         {
             $array_recipient = $lc->returnRecipientOption($t_InternalBCC);
             if (sizeof($array_recipient)>0)
             {
                 $recip_delimiter = "";
                 $text_recipient = "";
                 for ($j=0; $j<sizeof($array_recipient); $j++)
                 {
                      list($id,$name) = $array_recipient[$j];
                      $text_recipient .= "$recip_delimiter $name";
                      $recip_delimiter = ",";
                 }
                 $content .= "$i_CampusMail_New_InternalRecipients ($i_CampusMail_New_BCC): ";
                 $content .= "$text_recipient\n";
             }
         }
         if ($t_ExternalTo != "")
         {
             $content .= "$i_CampusMail_New_ExternalRecipients ($i_CampusMail_New_To): ";
             $content .= "$t_ExternalTo\n";
         }
         if ($t_ExternalCC != "")
         {
             $content .= "$i_CampusMail_New_ExternalRecipients ($i_CampusMail_New_CC): ";
             $content .= "$t_ExternalCC\n";
         }
         if ($t_ExternalBCC != "")
         {
             $content .= "$i_CampusMail_New_ExternalRecipients ($i_CampusMail_New_BCC): ";
             $content .= "$t_ExternalBCC\n";
         }

         $content .= "$i_frontpage_campusmail_subject: $t_Subject\n";
         $content .= "$i_frontpage_campusmail_message: $t_Message\n";
         if ($t_isNotification)
         {
             # Get reply
             $content .= "----------------------------------\n";
             $content .= "Reply:\n";

             $order_str = "a.ClassName, a.ClassNumber, a.EnglishName, b.UserName";
             $sql  = "SELECT IF(a.UserID IS NULL,CONCAT('<I>',b.UserName,'</I>'),$username_field2), b.IsRead, b.Message FROM INTRANET_CAMPUSMAIL_REPLY AS b LEFT OUTER JOIN INTRANET_USER AS a ON a.UserID = b.UserID WHERE b.CampusMailID = '$t_CampusMailID' ORDER BY $order_str";
             $replies = $lc->returnArray($sql, 3);
             for ($j=0; $j<sizeof($replies); $j++)
             {
                  list($rep_name,$rep_isread,$rep_msg) = $replies[$j];
                  $status = ($rep_isread==1? $i_frontpage_campusmail_read:$i_frontpage_campusmail_unread);
                  $content .= "$rep_name\t\t$status\t\t$rep_msg\n";
             }

         }

     }
     else
     {
         $t_sender_string = ($t_SenderEmail != ""? $t_SenderEmail: $t_SenderName);
         $content .= "$i_frontpage_campusmail_date: $t_Datetime\n";
         $content .= "$i_frontpage_campusmail_sender: $t_sender_string\n";
         if ($t_InternalTo!="")
         {
             $array_recipient = $lc->returnRecipientOption($t_InternalTo);
             if (sizeof($array_recipient)>0)
             {
                 $recip_delimiter = "";
                 $text_recipient = "";
                 for ($j=0; $j<sizeof($array_recipient); $j++)
                 {
                      list($id,$name) = $array_recipient[$j];
                      $text_recipient .= "$recip_delimiter $name";
                      $recip_delimiter = ",";
                 }
                 $content .= "$i_CampusMail_New_InternalRecipients ($i_CampusMail_New_To): ";
                 $content .= "$text_recipient\n";
             }
         }
         if ($t_InternalCC!="")
         {
             $array_recipient = $lc->returnRecipientOption($t_InternalCC);
             if (sizeof($array_recipient)>0)
             {
                 $recip_delimiter = "";
                 $text_recipient = "";
                 for ($j=0; $j<sizeof($array_recipient); $j++)
                 {
                      list($id,$name) = $array_recipient[$j];
                      $text_recipient .= "$recip_delimiter $name";
                      $recip_delimiter = ",";
                 }
                 $content .= "$i_CampusMail_New_InternalRecipients ($i_CampusMail_New_CC): ";
                 $content .= "$text_recipient\n";
             }
         }
         if ($t_ExternalTo != "")
         {
             $content .= "$i_CampusMail_New_ExternalRecipients ($i_CampusMail_New_To): ";
             $content .= "$t_ExternalTo\n";
         }
         if ($t_ExternalCC != "")
         {
             $content .= "$i_CampusMail_New_ExternalRecipients ($i_CampusMail_New_CC): ";
             $content .= "$t_ExternalCC\n";
         }

         $content .= "$i_frontpage_campusmail_subject: $t_Subject\n";
         $content .= "$i_frontpage_campusmail_message: $t_Message\n";
         if ($t_isNotification)
         {
             # Get reply
             # Get reply
             $content .= "----------------------------------\n";
             $content .= "Your Reply:\n";

             #$order_str = "a.ClassName, a.ClassNumber, a.EnglishName, b.UserName";
             #$sql  = "SELECT IF(a.UserID IS NULL,CONCAT('<I>',b.UserName,'</I>'),$username_field2), b.IsRead, b.Message FROM INTRANET_CAMPUSMAIL_REPLY AS b LEFT OUTER JOIN INTRANET_USER AS a ON a.UserID = b.UserID WHERE b.CampusMailID = $t_CampusMailID AND b.UserID = $UserID ORDER BY $order_str";
             $sql = "SELECT Message FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '$t_CampusMailFromID' AND UserID = '$UserID'";
             #echo $sql;
             $replies = $lc->returnVector($sql);
             $content .= $replies[0];
         }
     }
     $spliter = "\n=====================================================\n";
}

/*
$outType = ($li->SenderID == $UserID && $li->CampusMailFromID == "");

#$RecordType = 2;
$username_field = getNameFieldWithClassNumberByLang("b."); //($chi? "ChineseName": "EnglishName");
$username_field2 = getNameFieldWithClassNumberByLang("a."); //($chi? "ChineseName": "EnglishName");

$li = new libcampusmail();
if ($RecordType == 2)
{
    # Inbox
    $sql = "SELECT
               DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i'),
               IF(b.UserID IS NULL, '$i_general_sysadmin', $username_field),
               a.Subject,
               a.Message
          FROM INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
          ON a.SenderID = b.UserID
          WHERE
               a.UserID = '$UserID' AND
               a.RecordType = 2
          ORDER BY a.DateModified
           ";
    $messages = $li->returnArray($sql,4);
    $content = "";
    $delimiter = "";
    for ($i=0; $i<sizeof($messages); $i++)
    {
         list($mail_date,$mail_sender,$mail_subject,$mail_message) = $messages[$i];
         $content .= "$delimiter";
         $content .= "$i_frontpage_campusmail_date: $mail_date\n";
         $content .= "$i_frontpage_campusmail_sender: $mail_sender\n";
         $content .= "$i_frontpage_campusmail_subject: $mail_subject\n";
         $content .= "$i_frontpage_campusmail_message: $mail_message\n";
         $delimiter = "\n=====================================================\n";
    }
}
else if ($RecordType == 0)
{
    # Outbox
    $sql  = "SELECT
               a.CampusMailID,
               DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i'),
               a.RecipientID,
               a.Subject,
               a.Message,
               a.IsNotification
          FROM INTRANET_CAMPUSMAIL AS a
          WHERE
               a.UserID = '$UserID' AND
               a.RecordType = 0
          ORDER BY a.DateModified
          ";

    $messages = $li->returnArray($sql,6);
    $content = "";
    $delimiter = "";
    for ($i=0; $i<sizeof($messages); $i++)
    {
         list($mail_id,$mail_date,$mail_recipient,$mail_subject,$mail_message,$mail_notification) = $messages[$i];
         $array_recipient = $li->returnRecipientOption($mail_recipient);
         $recip_delimiter = "";
         $text_recipient = "";
         for ($j=0; $j<sizeof($array_recipient); $j++)
         {
              list($id,$name) = $array_recipient[$j];
              $text_recipient .= "$recip_delimiter $name";
              $recip_delimiter = ",";
         }
         $content .= "$delimiter";
         $content .= "$i_frontpage_campusmail_date: $mail_date\n";
         $content .= "$i_frontpage_campusmail_recipients: $text_recipient\n";
         $content .= "$i_frontpage_campusmail_subject: $mail_subject\n";
         $content .= "$i_frontpage_campusmail_message: $mail_message\n";

         if ($mail_notification==1)
         {
             $content .= "----------------------------------\n";
             $content .= "Reply:\n";

             $order_str = "a.ClassName, a.ClassNumber, a.EnglishName, b.UserName";
             $sql  = "SELECT IF(a.UserID IS NULL,CONCAT('<I>',b.UserName,'</I>'),$username_field2), b.IsRead, b.Message FROM INTRANET_CAMPUSMAIL_REPLY AS b LEFT OUTER JOIN INTRANET_USER AS a ON a.UserID = b.UserID WHERE b.CampusMailID = '$mail_id' ORDER BY $order_str";
             $replies = $li->returnArray($sql, 3);
             for ($j=0; $j<sizeof($replies); $j++)
             {
                  list($rep_name,$rep_isread,$rep_msg) = $replies[$j];
                  $status = ($rep_isread==1? $i_frontpage_campusmail_read:$i_frontpage_campusmail_unread);
                  $content .= "$rep_name\t\t$status\t\t$rep_msg\n";
             }
         }
         $delimiter = "\n=====================================================\n";
    }
}
*/
#echo $content;
intranet_closedb();
                // Output the file to user browser
                /*
                header("Content-Type: application/force-download");
                header("Content-Type: application/octet-stream");
                header("Content-Length: ".strlen($content));
                header("Content-Disposition: attachment; filename=\"export.txt\"");
                */
                echo "<pre>\n";
                echo $content;
                echo "</pre>\n";
?>
