<?php
## Using By :   Paul

/********************** Change Log ***********************/
#   Date:       2019-01-30 Isaac
#               added "powerClass_source_path" => $template_path ."PowerClass/"
#
#	Date:		2017-07-14
#				Project - Power Class
/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../";
$PATH_WRT_ROOT_ABS = "/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
if (!$sys_custom['PowerClass']) {
	header("Location: /home/index.php");
	exit;
}
if (!class_exists("libpowerclass_ui", false)) {
	include_once($intranet_root."/includes/PowerClass/libpowerclass_ui.php");
}

intranet_auth();
intranet_opendb();
$li = new libuser($UserID);
$lp = new libportal();
$ln = new libnotice();
$lo = new libannounce();
$libpowerclass_ui = new libpowerclass_ui();
$lschoolnews = new libschoolnews();
$linterface = new interface_html();

include_once($PATH_WRT_ROOT."includes/libuser.php");
$li_hp = new libuser($UserID);

if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{
	header("Location: " . $PATH_WRT_ROOT . "servermaintenance.php");
	exit();
}
if(isset($intranet_default_lang)){
	$title_lang = $intranet_default_lang;
}else{
	$title_lang = 'en';
}
$customLangVar = $sys_custom['Project_Label'];

$showHeader = false;
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/home_header.php");
# specially use to block the logins of the user account created
if (is_array($special_feature["login_control"]) && in_array($UserID, $special_feature["login_control"]))
{
	header("location: /logout.php");
	die();
}


// table setup
# change page size
if ($page_size_change == 1){
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") 
	$page_size = $ck_page_size;
else
	$page_size = 10;
	
$pageSizeChangeEnabled = true;
	
# Select sort field
$sortField = 0;
	
# Table initialization
$order = ($order == "") ? 0 : $order;
$field = ($field == "") ? $sortField : $field;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create a new dbtable instance
$ltable = new libdbtable2007($field, $order, $pageNo);

# TABLE SQL
$keyword = trim($keyword);
$keyword = addcslashes($keyword, '_');

$statusArr[] = array("0", $i_status_pending);
$statusArr[] = array("1", $i_status_publish);
if (!$sys_custom['eClassApp']['SFOC']) {
	$statusArr[] = array("2", $Lang['SysMgr']['SchoolNews']['NoApproval']);
	$statusArr[] = array("3", $Lang['SysMgr']['SchoolNews']['Reject']);
}

# Filter
$selectedStatus = $lschoolnews->getStatus("name=\"status\" onChange=\"reloadForm()\"", $statusArr, $status, $Lang['SysMgr']['SchoolNews']['AllStatus']);

$user_field = getNameFieldWithClassNumberByLang("b.");


# SQL statement
if(sizeof($statusArr)!=0){
	$allStatus = " a.RecordStatus IN (";
	for ($i=0; $i < sizeof($statusArr); $i++)
	{
		list($statusID)=$statusArr[$i];
		$allStatus .= $statusID.",";
	}
	$allStatus = substr($allStatus,0,strlen($allStatus)-1).")";
}
else{
	$allStatus ="";
}

$status = ($status == "") ? $allStatus : "a.RecordStatus = $status";

$conds .= " (a.Title like '%$keyword%') AND $status";
$groupby = " group by a.AnnouncementID ";

$sql = "SELECT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT ga INNER JOIN INTRANET_USERGROUP g ON ga.GroupID=g.GroupID WHERE g.UserID='".$UserID."'";
$groupAnnouncement = $lo->returnVector($sql);

$name_field = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT
			DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d') as PostDate, 
			CONCAT('<a target=\"_blank\" href=\"/home/view_announcement.php?AnnouncementID=', a.AnnouncementID,'\">',a.Title,'</a>') as Post,
			$name_field as NameField
			FROM
				INTRANET_ANNOUNCEMENT as a
			LEFT OUTER JOIN
				INTRANET_USER as b
				ON a.UserID = b.UserID
			LEFT OUTER JOIN
				INTRANET_GROUP as c
				ON a.OwnerGroupID = c.GroupID
			WHERE ((a.OwnerGroupID IS NULL AND a.RecordType = 1) OR a.AnnouncementID IN ('".implode("','",$groupAnnouncement)."'))
				AND $conds
			$groupby	";

# TABLE INFO
$ltable->field_array = array("PostDate", "Post", "NameField");
$ltable->sql = $sql;
$ltable->no_col = sizeof($ltable->field_array)+1;
$ltable->IsColOff = "IP25_table";

// TABLE COLUMN
$ltable->column_list .= "<th class='num_check'>#</td>\n";
$ltable->column_list .= "<th>".$ltable->column(0, $i_AnnouncementDate)."</th>\n";
$ltable->column_list .= "<th>".$ltable->column(1, $i_AnnouncementTitle)."</th>\n";
$ltable->column_list .= "<th>".$ltable->column(2, $i_AnnouncementOwner)."</th>\n";

# Start layout
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

$args = array(
		"Lang" => $Lang,
		"source_path" => $source_path,
        "powerClass_source_path" => $template_path ."PowerClass/",
    "powerClass_common_template_source_path"=>$powerClass_common_template_source_path,
		"topMenuCustomization" => $topMenuCustomization,
		"UserIdentity" => $UserIdentity,
		"ltable" => $ltable,
		"linterface" => $linterface,
		"searchTag" => $searchTag,
		"selectedStatus" => $selectedStatus
);

$portal_html = $libpowerclass_ui->loadView("schoolnews_more", $args);
intranet_closedb();
?>