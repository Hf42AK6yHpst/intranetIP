<?php
## Using By :   

/********************** Change Log ***********************/
#   Date:       2019-01-30 Isaac
#               added "powerClass_source_path" => $template_path ."PowerClass/"
#
#	Date:		2017-12-19 Cameron
#				show return error message for pre-requisite (MissingBasicSetting)
#
#	Date:		2017-04-18
#				Project - Power Class
/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../";
$PATH_WRT_ROOT_ABS = "/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
if (!$sys_custom['PowerClass']) {
	header("Location: /home/index.php");
	exit;
}
if (!class_exists("libpowerclass_ui", false)) {
	include_once($intranet_root."/includes/PowerClass/libpowerclass_ui.php");
}

intranet_auth();
intranet_opendb();
$li = new libuser($UserID);
$lp = new libportal();
$ln = new libnotice();
$lhomework = new libhomework2007();
$libpowerclass_ui = new libpowerclass_ui();

include_once($PATH_WRT_ROOT."includes/libuser.php");
$li_hp = new libuser($UserID);

if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{
	header("Location: " . $PATH_WRT_ROOT . "servermaintenance.php");
	exit();
}
if(isset($intranet_default_lang)){
	$title_lang = $intranet_default_lang;
}else{
	$title_lang = 'en';
}
$customLangVar = $sys_custom['Project_Label'];

$showHeader = false;
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/home_header.php");
# specially use to block the logins of the user account created
if (is_array($special_feature["login_control"]) && in_array($UserID, $special_feature["login_control"]))
{
	header("location: /logout.php");
	die();
}

$app_details = $libpowerclass_ui->getReportList($UserID);
$app_details = $libpowerclass_ui->orderAppDisplay($app_details, "reports");

$args = array(
		"Lang" => $Lang,
		"source_path" => $source_path,
        "powerClass_source_path" => $template_path ."PowerClass/",
    "powerClass_common_template_source_path"=>$powerClass_common_template_source_path,
		"topMenuCustomization" => $topMenuCustomization,
		"UserIdentity" => $UserIdentity,
		"app_details" => $app_details
);


$portal_html = $libpowerclass_ui->loadView("reports", $args);

if ($_GET['error'] == 'MissingBasicSetting') {
	print ' <div id="system_message_box" class="SystemReturnMessage" style="display:block; visibility: hidden;"> 
				<div class="msg_board_left">
			  		<div class="msg_board_right" id="message_body"> 
			  			<a href="#" onclick="document.getElementById(\'system_message_box\').style.display = \'none\'; return false;">[Clear]</a>
			  		</div>
				</div>
			</div>';
	
	print "<script>";
	print "Get_Return_Message('".$Lang['eInventory']['ReturnMessage']['MissingBasicSettings']."');";
	print "</script>";
}

intranet_closedb();
?>