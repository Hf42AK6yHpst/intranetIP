<?php
## Using By :   Paul

/********************** Change Log ***********************/
#   Date:       2019-01-30 Isaac
#               added "powerClass_source_path" => $template_path ."PowerClass/"
#
#	Date:		2017-07-14
#				Project - Power Class
/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../";
$PATH_WRT_ROOT_ABS = "/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
if (!$sys_custom['PowerClass']) {
	header("Location: /home/index.php");
	exit;
}
if (!class_exists("libpowerclass_ui", false)) {
	include_once($intranet_root."/includes/PowerClass/libpowerclass_ui.php");
}

intranet_auth();
intranet_opendb();
$li = new libuser($UserID);
$lp = new libportal();
$ln = new libnotice();
$lo = new libannounce();
$lhomework = new libhomework2007();
$libpowerclass_ui = new libpowerclass_ui();

include_once($PATH_WRT_ROOT."includes/libuser.php");
$li_hp = new libuser($UserID);

if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{
	header("Location: " . $PATH_WRT_ROOT . "servermaintenance.php");
	exit();
}
if(isset($intranet_default_lang)){
	$title_lang = $intranet_default_lang;
}else{
	$title_lang = 'en';
}
$customLangVar = $sys_custom['Project_Label'];

$showHeader = false;
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/home_header.php");
# specially use to block the logins of the user account created
if (is_array($special_feature["login_control"]) && in_array($UserID, $special_feature["login_control"]))
{
	header("location: /logout.php");
	die();
}
$sql = "SELECT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT ga INNER JOIN INTRANET_USERGROUP g ON ga.GroupID=g.GroupID WHERE g.UserID='".$UserID."'";
$groupAnnouncement = $lo->returnVector($sql);

$name_field = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT 
            				a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), 
            				a.Attachment, $name_field, c.Title, a.Description, a.EndDate
                    	FROM 
                    		INTRANET_ANNOUNCEMENT as a
								LEFT OUTER JOIN 
							INTRANET_USER as b 
								ON a.UserID = b.UserID
                         		LEFT OUTER JOIN 
                         	INTRANET_GROUP as c 
                         		ON a.OwnerGroupID = c.GroupID
                         	WHERE 
                         		(a.EndDate is null OR CURDATE()>=a.AnnouncementDate) 
                         		AND ((a.OwnerGroupID IS NULL AND a.RecordType = 1) OR a.AnnouncementID IN ('".implode("','",$groupAnnouncement)."'))
                         		AND (a.EndDate >= CURDATE() OR a.EndDate is null)		
                         	ORDER BY 
                         		a.AnnouncementDate DESC, a.AnnouncementID ASC LIMIT 3";
$schoolNewsArr = $lo->returnArray($sql);

$args = array(
		"Lang" => $Lang,
		"source_path" => $source_path,
        "powerClass_source_path" => $template_path ."PowerClass/",
    "powerClass_common_template_source_path"=>$powerClass_common_template_source_path,
		"topMenuCustomization" => $topMenuCustomization,
		"UserIdentity" => $UserIdentity,
		"schoolnews" => $schoolNewsArr
);

$portal_html = $libpowerclass_ui->loadView("schoolnews", $args);
intranet_closedb();
?>