<?php
## Using By : 

/********************** Change Log ***********************/
#
#   Date:   2020-05-06 Tommy
#           - created file
#
/********************** Change Log ***********************/

$PATH_WRT_ROOT = "../../";
$PATH_WRT_ROOT_ABS = "/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuserbrowser.php");

if (!$sys_custom['PowerClass']) {
	header("Location: /home/index.php");
	exit;
}

if (!class_exists("libpowerclass_ui", false)) {
	include_once($intranet_root."/includes/PowerClass/libpowerclass_ui.php");
}

intranet_auth();
intranet_opendb();
$li = new libuser($UserID);
$lp = new libportal();
$ln = new libnotice();
$lhomework = new libhomework2007();
$libpowerclass_ui = new libpowerclass_ui();
// $libbrowser = new browser();

include_once($PATH_WRT_ROOT."includes/libuser.php");
$li_hp = new libuser($UserID);

if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{
	header("Location: " . $PATH_WRT_ROOT . "servermaintenance.php");
	exit();
}
if(isset($intranet_default_lang)){
	$title_lang = $intranet_default_lang;
}else{
	$title_lang = 'en';
}
$customLangVar = $sys_custom['Project_Label'];

$AcademicYearID = Get_Current_Academic_Year_ID();
# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate(date("Y-m-d"));
$yearTermID = $currentYearTerm[0];
$yearTermID = $yearTermID ? $yearTermID : 0;

$showHeader = true;
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/home_header.php");
# specially use to block the logins of the user account created
if (is_array($special_feature["login_control"]) && in_array($UserID, $special_feature["login_control"]))
{
	header("location: /logout.php");
	die();
}

if($_SESSION['UserType']==USERTYPE_PARENT){
    $ParentOrStudent = 1;
    $msg = $Lang['PowerClass']['GoWith']['Parent'];
//     $android = "https://play.google.com/store/apps/details?id=com.broadlearning.eclass";
//     $ios = "https://apps.apple.com/hk/app/eclass-parent-app/id880950939";
}else{
    $ParentOrStudent = 0;
    $msg = $Lang['PowerClass']['GoWith']['Student'];
//     $android = "https://play.google.com/store/apps/details?id=com.broadlearning.eclassstudent";
//     $ios = "https://apps.apple.com/hk/app/eclass-student-app/id1088739934";
}

// $os = $libbrowser->getBrowserOS();
// if($os == "Android"){
//     if($ParentOrStudent == 1)
//         $link = "https://play.google.com/store/apps/details?id=com.broadlearning.eclass";
//     elseif ($ParentOrStudent == 0)
//         $link = "https://play.google.com/store/apps/details?id=com.broadlearning.eclassstudent";
// }elseif($os == "iPad"){
//     if($ParentOrStudent == 1)
//         $link = "https://apps.apple.com/hk/app/eclass-parent-app/id880950939";
//     elseif ($ParentOrStudent == 0)
//         $link = "https://apps.apple.com/hk/app/eclass-student-app/id1088739934";
// }

$args = array(
		"Lang" => $Lang,
		"source_path" => $source_path,
		"topMenuCustomization" => $topMenuCustomization,
        "powerClass_source_path" => $template_path ."PowerClass/",
        "powerClass_common_template_source_path"=>$powerClass_common_template_source_path,
        "ParentOrStudent"=>$ParentOrStudent,
        "msg"=>$msg,
        "android"=>$android,
        "ios"=>$ios
);

$portal_html = $libpowerclass_ui->loadView("gowith", $args);
intranet_closedb();
?>