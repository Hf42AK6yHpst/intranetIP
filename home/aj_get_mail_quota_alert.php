<?
// editing by 
// Get iMail plus quota usage info and alert setting, return alert message if over
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

$IMap = new imap_gamma();
$alert_msg = "";
if($IMap->ConnectionStatus==true)
{
	$quota_alert = floatval($IMap->getQuotaAlertSetting());
	
	if($quota_alert > 0) {
		$IMapCache = $IMap->getImapCacheAgent();
		$quota = $IMapCache->Check_Quota();
		$total_quota = $quota['TotalQuota'];
		$used_quota = $quota['UsedQuota'];
		if($total_quota > 0) {		
			$used_percent = sprintf("%.2f", $used_quota / $total_quota * 100);
			if($used_percent >= $quota_alert){
				$alert_msg = $Lang['Gamma']['Warning']['AlertQuotaUsageStatus'];
				$alert_msg = str_replace(array("<!--USED_PERCENT-->","<!-USED_QUOTA->","<!--TOTAL_QUOTA-->",'\n'),array($used_percent,$used_quota,$total_quota,chr(10)),$alert_msg);
			}
		}
	}
}
echo $alert_msg;

intranet_closedb();
die;
?>