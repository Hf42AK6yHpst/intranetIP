<?
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");


if ($plugin['PowerPad_Lite'] && is_file($PATH_WRT_ROOT.'plugins/pb_lite_config.php')) {
	include_once($PATH_WRT_ROOT."plugins/pb_lite_config.php");
}
else {
	No_Access_Right_Pop_Up();
	die();
}

intranet_opendb();

$luser = new libuser($_SESSION['UserID']);

$user_extra_info = array();
if($_SESSION['SSV_USER_ACCESS']['eAdmin-PB_Lite']){
	$user_extra_info['role'] = 'admin';
}else if($luser->isTeacherStaff()){
	$user_extra_info['role'] = 'teacher';
}else if($luser->isStudent()){
	$user_extra_info['role'] = 'student';
}
$user_extra_info['email'] = $luser->UserEmail;

$user_extra_info['platforms'] = "ios,android,mac,windows";
$extra=base64_encode(serialize($user_extra_info));
$school_code = $ssoservice["PowerPad_Lite"]["schoolCode"];
$userlogin = $luser->UserLogin;

$user="$userlogin@$school_code.eclass.com.hk";
$expire='1513269022';
$key=$ssoservice["PowerPad_Lite"]["sharedKey"]; // the shared-key
$timestamp='1493269022';
$preauthAttriString = $user."|".$expire."|".$timestamp."|".$extra;
$preauthToken=hash_hmac("sha1",$preauthAttriString,$key);

$ssoLink = $ssoservice["PowerPad_Lite"]["ssoUrl"]."auth?user=$user&timestamp=$timestamp&expire=$expire&preauth=$preauthToken&extra=$extra";
//$ssoLink = $ssoservice["PowerPad_Lite"]["ssoUrl"]."auth?tempLogin=eclass";

intranet_closedb();
//echo $ssoLink;
header ("Location: ".$ssoLink);
?>