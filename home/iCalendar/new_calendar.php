<?php
// page modifing by: 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
include_once("choose/new/User_Group_Transformer.php");
intranet_auth();
intranet_opendb();

$isIP25 = true; # IP25 school calendar is hidden

$transformer = new User_Group_Transformer();
$iCal = new icalendar();
$lui = new ESF_ui();

if (!isset($time))
	$time = $_SESSION["iCalDisplayTime"];

# Initialization
$editMode = false;
$defaultEventAccess = 'P';
$shareToAll = 0;
$calType = 0;

# use for checking existing guests before deletion occurs
$isExistGrpViewer = 0;  	
$isExistIndViewer = 0;

# Get Color Choices 
$ColorPickArr = $iCal->Get_Color_Selection();

// if editing calendar, retrieve calendar detail
if (isset($calID) && is_numeric($calID)) {
	$calID = IntegerSafe($calID);
	$ownDefaultCalID = $iCal->returnOwnerDefaultCalendar();
	$userCalAccess = $iCal->returnUserCalendarAccess($UserID, $calID);
	if (!$userCalAccess) {
		header("Location: index.php");
	}
	
	$editMode = true;
	$sql  = "SELECT * FROM CALENDAR_CALENDAR ";
	$sql .= "WHERE CalID = '$calID'";
	$result = $iCal->returnArray($sql);
	
	if (sizeof($result) > 0) {
		$calName 		= trim($result[0]["Name"]);
		$calDescription = trim($result[0]["Description"]);
		$ownerID 		= $result[0]["Owner"];
		$shareToAll 	= trim($result[0]["ShareToAll"]);
		$defaultEventAccess = trim($result[0]['DefaultEventAccess']);
		$calType 		= trim($result[0]['CalType']);
		$calSharedType 	= trim($result[0]['CalSharedType']);
		$synUrl			= (trim($result[0]['SyncURL'])=="NULL"?"":$result[0]['SyncURL']);
		$isOwner 		= ($ownerID == $UserID) ? true : false;
		
		$sql  = "SELECT * FROM CALENDAR_CALENDAR_VIEWER WHERE CalID = '$calID' AND UserID = '".$UserID."'";
		/*if (isset($groupID) && isset($groupType)) {
			$sql .= "WHERE CalID = $calID AND GroupID = $groupID AND GroupType='$groupType' AND UserID = ".$UserID;	
		} else {
			$sql .= "WHERE CalID = $calID AND GroupID IS NULL AND UserID = ".$UserID;
		}*/
		$result = $iCal->returnArray($sql);
		
		// echo "Group Info: ".$groupID." ".$groupType;
		// debug_r($result);
		
		$calColor = trim($result[0]["Color"]);
		$access   = trim($result[0]["Access"]);
	}
}

# Set Default Calendar Color if no color is selected
$calColor = ($calColor != "") ? $calColor : '2f75e9';


#$deleteCalButton  = $linterface->GET_ACTION_BTN($button_remove, "button", "javascript:window.location='delete_calendar.php?calID=#%calID%#';");
#$deleteCalButton .= $linterface->GET_ACTION_BTN($button_cancel, "button", "$('#deleteCalendarWarning').jqmHide();");
#$deleteCalButton = addslashes($deleteCalButton);

$deleteCalButton = "<input type='button' name='' value='$iCalendar_NewEvent_Reminder_Remove' onclick='confirmDeleteRedirect(calID)' />";
$deleteCalButton .= "<input type='button' name='' value='".$Lang['btn_cancel']."' onclick='hideDeleteAlertMsg()' />";

# Get Permission Selection Box
$SelectAllPermission  = '';
$SelectAllPermission .= '<select name="permission-all" id="permission-all" onclick="jSet_All_Permission()">';
$SelectAllPermission .= '<option value="">'.$iCalendar_NewCalendar_PermissionSetting.'</option>';
$SelectAllPermission .= '<option value="W">'.$iCalendar_NewCalendar_SharePermissionEdit.'</option>';
$SelectAllPermission .= '<option value="R">'.$iCalendar_NewCalendar_SharePermissionRead.'</option>';
$SelectAllPermission .= '</select>';

# Labeling
if ($editMode)
	$CalLabeling = "<div id='sub_page_title'>$iCalendar_EditCalendar_Header</div>";
else
	$CalLabeling = "<div id='sub_page_title'>$iCalendar_NewCalendar_Header</div>";

# Main UI
$CalMain = "
	<div id='cal_board'>
	  <span class='cal_board_02'></span> 
	  <div class='cal_board_content' style='float:left;'>";
	  
$CalMain .= "<form name='form1' action='".($editMode?"edit_calendar_update.php":"new_calendar_update.php")."' method='POST'>
<input type='hidden' value='$calType' name='CalType'>
			  <div id='calendar_wrapper' style='float:left;background-color:#dbedff'>";
			  
$CalMain .= "<div id='cal_manage_calendar'>
			  <div style='width: 95%; padding-left: 10px; padding-right: 10px;' id='cal_list_mycal'>
				<p class='cssform'>
					<label for='calName'>$iCalendar_NewCalendar_Name</label>
					<span id='imail_form_indside_choice'>";
					if (!$editMode){
						$CalMain .= $lui->Get_Input_Text("calName", $calName, "class='textbox' maxlength='255'");
					}else if($isOwner) {
						if($calID == $ownDefaultCalID){
							$CalMain .= $calName;
							$CalMain .= $lui->Get_Input_Hidden("calName", "calName", $calName);
						} else {
							$CalMain .= $lui->Get_Input_Text("calName", $calName, "class='textbox' maxlength='255'");
						}
						//$CalMain .= ($calID == $ownDefaultCalID) ? $calName : $lui->Get_Input_Text("calName", $calName, "class='textbox' maxlength='255'");
					} else {
						$CalMain .= $calName;
					}
	$CalMain .= "</span>";
	$CalMain .= "</p>";
	$CalMain .= "<br style='clear: both;'/>";
	
if ($editMode && !$isOwner) {
	$CalMain .= "<p class='cssform'>
					<br style='clear: both;'/>
					<label for='Sender'>$iCalendar_EditCalendar_Owner</label>
					<span id='imail_form_indside_choice'>";
	if ($calType >= 2)
		$CalMain .= $iCalendar_systemAdmin;
	else
		$CalMain .= $iCal->returnUserFullName($ownerID);
	$CalMain .= "</span>
				</p>";
	$CalMain .= "<br style='clear: both;'/>";
}			
	# Description
	$CalMain .= "<p class='cssform'>
					<br style='clear: both;'/>
					<label for='Sender'>$iCalendar_NewCalendar_Description</label>
					<span id='imail_form_indside_choice'>";
					if (!$editMode || $isOwner) {
						$CalMain .= $lui->Get_Input_Textarea("calDescription", $calDescription, 70, 5, "", "class='textarea_desc'");
					} else {
						$CalMain .= ($calDescription=="") ? "-" : $calDescription;
					}
	$CalMain .= "</span> 
				</p>";
	$CalMain .= "<br style='clear: both;'/>";
		//echo $access." hello.<br>";
	if (($access == 'A' || $access == 'W' || !$editMode) && $plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
	#sync url
	
		$CalMain .= "<p class='cssform'>
						<br style='clear: both;'/>
						<label for='Sender'>$iCalendar_NewEvent_SyncUrl</label>
						<span id='imail_form_indside_choice' style='width: 570px'>";
							if (strstr($_SERVER['HTTP_USER_AGENT'],"MSIE"))
								$CalMain .= $lui->Get_Input_Text("synURL", "$synUrl", "style='width:480px'");
							else
								$CalMain .= $lui->Get_Input_Text("synURL", "$synUrl", "style='width:500px'");
							//$CalMain .= $lui->Get_HyperLink_Open("#","style='padding-left:5px'");
							//$CalMain .= $iCalendar_NewEvent_NeedHelp;
							//$CalMain .= $lui->Get_HyperLink_Close();
		$CalMain .= "</span>".
		
		(strstr($_SERVER['HTTP_USER_AGENT'],"MSIE")?
		"<div style='color:#666666;padding-left:145px; width:627; font-style: italic'>".$iCalendar_ManageCalenadar_syncDes ."</div>":		
		"<div style='color:#666666;padding-left:145px; width:500; font-style: italic'>".$iCalendar_ManageCalenadar_syncDes ."</div>")
					."</p>";
		$CalMain .= "<br style='clear: both;'/>";
	}
	
	
	
	
	# Color Picker
	$CalMain .= "<p class='cssform'>
					<br style='clear: both;'/>
					<label for='Sender'>$iCalendar_NewEvent_ShareColor</label>
					<span id='imail_form_indside_choice' >";
		$CalMain .= '<div id="pickcolor">'; 
		$CalMain .= '<ul>';
		if(count($ColorPickArr) > 0){
			foreach($ColorPickArr as $key => $code){
				$color_num = $key;
				
				$setCurrent = "notcurrent";
				$setDisplay = 'none';
				if(strtolower($calColor) == strtolower($code)){
					$setCurrent = "current";
					$setDisplay = 'block';
				}
				
				#Michael Cheung (2009-10-30) - Changed hop value from 10 to 11
				$CalMain .= ((int)$key > 11 && (int)$key % 11 == 1) ? '<br style="clear:both">' : '';
				$CalMain .= '<li id="colorLi_'.$color_num.'" name="colorLi_'.$color_num.'">';
				$CalMain .= '<div id="'.$setCurrent.'">';
				
				$CalMain .= '<a href="javascript:void(0)" class="cal_calendar_'.$color_num.'" onclick="jPick_Color(\''.$color_num.'\')">';
				$CalMain .= '<span id="colorSpan[]" name="colorSpan[]" style="display:'.$setDisplay.'">';
				
				$CalMain .= '<img src="'.$ImagePathAbs.'calendar/icon_tick_white.gif" width="20" height="20" border="0">';
				$CalMain .= '</span>';
				$CalMain .= '</a>';
				$CalMain .= '</div>';
				$CalMain .= '</li>';
			}
		}
	$CalMain .= '</ul>';
	$CalMain .= '</div>';
	$CalMain .= $lui->Get_Input_Hidden("calColor", "calColor", $calColor);	
	$CalMain .= "</span>";
	$CalMain .= "</p>";
	$CalMain .= "<br style='clear: both;'/>";

	# Default Event Access 
	$CalMain .= $lui->Get_Input_Hidden("defaultEventAccess", "defaultEventAccess", "P");
	/*
	# old approach - providing selecting default value of event access: (P): Public, (R): Private
	$CalMain .= "<p class='cssform'>
					<br style='clear: both;'/>
					<label for='Sender'>$iCalendar_NewCalendar_DefaultEventAccess</label>
					<span id='imail_form_indside_choice'>";
					if (!$editMode || $isOwner) {
						$CalMain .= $lui->Get_Input_RadioButton("defaultEventAccess", "defaultEventAccess_P", "P", (($defaultEventAccess=="P")?"checked":"") ).$iCalendar_NewEvent_AccessPublic."&nbsp;";
						$CalMain .= $lui->Get_Input_RadioButton("defaultEventAccess", "defaultEventAccess_R", "R", (($defaultEventAccess=="R")?"checked":"") ).$iCalendar_NewEvent_AccessPrivate;
					} else {
						$CalMain .= ($defaultEventAccess=="P") ? $iCalendar_NewEvent_AccessPublic : $iCalendar_NewEvent_AccessPrivate;
					}
	$CalMain .= "</span>
				</p>";
	$CalMain .= "<br style='clear: both;'/>";	
	*/
	
	# Access Right of Shared Calendar User
	if($editMode && $access != "P" && !$isOwner){
		$userPermission = ($access == "W") ? $iCalendar_NewCalendar_SharePermissionEdit : $iCalendar_NewCalendar_SharePermissionRead;
		$CalMain .= "<p class='cssform'>
					<br style='clear: both;'/>
					<label for='Sender'>$iCalendar_NewCalendar_ShareTablePermission</label>
					<span id='imail_form_indside_choice'>$userPermission</span>
					</p>
					<br style='clear: both;'/>";
	}
	// 
	
	# Sharing 
	if ((!$editMode || $isOwner)&&(!$plugin["iCalendarDisableStudentSharing"]&&$UserType==2||$UserType!=2)) { 
		$CalMain .= "<p class='dotline_p'/>";
		$CalMain .= '<p class="cssform">
					  <label for="Sender">'.$iCalendar_NewCalendar_ShareTo.'</label>
						<span id="imail_form_indside_choice" >	
						  <table id="calViewerTable" width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
							<tr>
							  <td width="250" class="imail_entry_top">
								<img src="'.$ImagePathAbs.'spacer.gif" width="20" height="20" border="0" align="absmiddle">'.$iCalendar_NewCalendar_Name.'</td>
							  <td class="imail_entry_top" colspan="2">'.$SelectAllPermission.'</td>
							</tr>
							<tr>
							  <td nowrap class="cal_agenda_content_entry">
							    <img src="'.$ImagePathAbs.'imail/icon_ppl_ex.gif" width="20" height="20" border="0" align="absmiddle">'.$iCal->returnUserFullName($_SESSION['UserID']).'
							  </td>
							  <td nowrap class="cal_agenda_content_entry">'.$iCalendar_NewCalendar_SharePermissionFull.'</td>
							  <td class="cal_agenda_content_entry cal_agenda_content_entry_button">&nbsp;
							  </td>
							</tr>';
		if ($editMode) {
			# Get Unique Group Path
			$uniqueGrp = array();
			$viewerGrpPath = $iCal->returnCalViewerList($calID, false, true);
			if(count($viewerGrpPath) > 0){
				$isExistGrpViewer = 1;	# use for checking exist guests before deletion occurs
				$num = 0;
				foreach($viewerGrpPath as $viewgp){ #new approach
					$groupName = $transformer->get_group_name(trim($viewgp['GroupPath']));
					// debug_r($groupName);
					// exit;
					if (!empty($groupName)){
						$uniqueGrpList[$num] = $viewgp;
						$uniqueGrpList[$num]['Name'] = $groupName;
						$num++;
					}
				}
			/*	for ($i=0; $i<sizeof($viewerGrpPath); $i++) {
					$tmpInfo = array();
	                $tmpInfo[] = $viewerGrpPath[$i];
	                $uniqueGrp[$num] = $viewerGrpPath[$i];
	                for($j=$i+1 ; $j<count($viewerGrpPath) ; $j++){
	                    if($viewerGrpPath[$i]['GroupPath']==$viewerGrpPath[$j]['GroupPath']){
	                    	$tmpInfo[] = $viewerGrpPath[$j];

							# added on 2009-6-16 by Jason 
							# handle the access permission for the group if any "Read Write" permission is set
							if(trim($viewerGrpPath[$i]['Access']) == 'W' || trim($viewerGrpPath[$j]['Access']) == 'W'){ 
								$uniqueGrp[$num]['Access'] = 'W';
							}
	                    }else{
	                    	break;
	                    }
	                }
	                $i = $j - 1;
	                $num++;
				}
				
				# Get Group Name 
				for ($i=0; $i<sizeof($uniqueGrp); $i++) {
					$uniqueGrp[$i]['Name'] = $iCal->Get_Group_Viewer($uniqueGrp[$i]['GroupPath'], false);
					
					$path = explode(":=:", $uniqueGrp[$i]['GroupPath']);
					$pathPrefix = (count($path) > 1 && strlen($path[1]) > 1) ? substr($path[1], 0, 1) : "";
					if(strtoupper($path[0]) == "T" && strtoupper($pathPrefix) == "C"){
						$pathSuffix = substr($path[1], 1);
						if(strtoupper($pathPrefix) == "C"){
							$newPath = '';
							for($j=0 ; $j<count($path) ; $j++){
								$newPath .= ($j > 0) ? ":=:" : "";
								$newPath .= ($j == 1) ? $pathPrefix.$uniqueGrp[$i]['Name'] : $path[$j];
							}
							# Re-generated the GroupPath & Name for Sujbect cases
							$uniqueGrp[$i]['GroupPath'] = $newPath;
							$uniqueGrp[$i]['Name'] = $uniqueGrp[$i]['Name'].' ('.$Lang['email']['TeachingStaff'].')';
						}
					} 
				}
								
				# Get Unqiue Group Name 
				$uniqueGrpList = array();
				for ($i=0; $i<sizeof($uniqueGrp); $i++) {
	                $uniqueGrpList[] = $uniqueGrp[$i];
	                for($j=$i+1 ; $j<count($uniqueGrp) ; $j++){
	                    if($uniqueGrp[$i]['Name'] != $uniqueGrp[$j]['Name']){
	                    	break;
	                    }
	                }
	                $i = $j - 1;
				}*/
				
				# UI
				for ($i=0; $i<sizeof($uniqueGrpList); $i++) {
					switch ($uniqueGrpList[$i]["Access"]) {
						case "W":
							$curPermission = $iCalendar_NewCalendar_SharePermissionEdit;
							break;
						case "R":
							$curPermission = $iCalendar_NewCalendar_SharePermissionRead;
							break;
						default:
							$curPermission = "-";
							break;
					}
					$CalMain .= '<tr>
								  <td nowrap class="cal_agenda_content_entry">
								    <a href="#" class="imail_entry_read_link"><img src="'.$ImagePathAbs.'imail/icon_ppl_ex.gif" width="20" height="20" border="0" align="absmiddle">'.$uniqueGrpList[$i]['Name'].'</a>
								  </td>
								  <td nowrap class="cal_agenda_content_entry">
								    <select name="permission-'.$uniqueGrpList[$i]['GroupPath'].'" id="permission-'.$uniqueGrpList[$i]['GroupPath'].'">
										<option value="W"'.((trim($uniqueGrpList[$i]["Access"])=="W") ? " selected" : "").'>'.$iCalendar_NewCalendar_SharePermissionEdit.'</option>
										<option value="R"'.((trim($uniqueGrpList[$i]["Access"])=="R") ? " selected" : "").'>'.$iCalendar_NewCalendar_SharePermissionRead.'</option>
									</select>
								  </td>
								  <td class="cal_agenda_content_entry cal_agenda_content_entry_button">
								    <span onclick="removeViewer(this)" class="tablelink"><a href="javascript:void(0)">'.$Lang['btn_delete'].'</a></span>
								    <input type="hidden" value="'.$uniqueGrpList[$i]["GroupPath"].'" name="viewerID[]" />
								  </td>
								</tr>';
				}
			}
			
			# Get Individual Guest Data
			$viewerList = $iCal->returnCalViewerList($calID);
			if(count($viewerList) > 1) $isExistIndViewer = 1;	# use for checking exist guests before deletion occurs
			for ($i=0; $i<sizeof($viewerList); $i++) {
				if ($viewerList[$i]["UserID"]==$ownerID) {
					continue;
				}
				$curDisplayName = $iCal->returnUserFullName($viewerList[$i]["UserID"]);
				switch ($viewerList[$i]["Access"]) {
					case "W":
						$curPermission = $iCalendar_NewCalendar_SharePermissionEdit;
						break;
					case "R":
						$curPermission = $iCalendar_NewCalendar_SharePermissionRead;
						break;
					default:
						$curPermission = "-";
						break;
				}
				$CalMain .= '<tr>
							  <td nowrap class="cal_agenda_content_entry">
							    <a href="#" class="imail_entry_read_link"><img src="'.$ImagePathAbs.'imail/icon_ppl_ex.gif" width="20" height="20" border="0" align="absmiddle">'.$curDisplayName.'</a>
							  </td>
							  <td nowrap class="cal_agenda_content_entry">
							    <select name="permission-U'.$viewerList[$i]["UserID"].'" id="permission-U'.$viewerList[$i]["UserID"].'">
								  <option value="W" '.((trim($viewerList[$i]["Access"]) == "W") ? "selected" : "").'>'.$iCalendar_NewCalendar_SharePermissionEdit.'</option>
								  <option value="R" '.((trim($viewerList[$i]["Access"]) == "R") ? "selected" : "").'>'.$iCalendar_NewCalendar_SharePermissionRead.'</option>
								</select>
							  </td>
							  <td class="cal_agenda_content_entry cal_agenda_content_entry_button">
							    <span onclick="removeViewer(this)" class="tablelink"><a href="javascript:void(0)">'.$Lang['btn_delete'].'</a></span>
								<input type="hidden" value="U'.$viewerList[$i]["UserID"].'" name="viewerID[]" />
							  </td>
							</tr>';
			} 
		}
		
		//$select_link = "choose/index.php?calID=$calID";	
		// $iCalendar_NewCalendar_AddPeopleShare 
		$select_link = $PathRelative."choose/new/index.php?pageType=NewCalendar&calID=".$calID;
		$CalMain .= '	  </table><br>';
		$CalMain .= $lui->Get_Input_Button('AddUser', 'AddUser', $iCalendar_addUser, 'style="display:'.(($calType == 2) ? 'none' : 'block').'" class="button_main_act" onclick="window.open(\''.$select_link.'\',\'\',\'scrollbars=yes,width=500,height=500\');return false"')."&nbsp";
		$CalMain .= '&nbsp;<span id="FoundationBtnRemark" style="display:'.(($calType == 2) ? 'block' : 'none').'" ><font color="red">(*Cannot be able to share user in Foundation Calendar.)</font></span>';
		$CalMain .= ' </span>
					</p>';
		$CalMain .= "<br style='clear: both;'/>";

		## Calendar Type : 0-Personal , 1-School , 2-Foundation
		// debug_r($_SESSION);
		// exit;
		//$isDBAs = $iCal->Retrieve_DBA_User();
		$isDBAs = $_SESSION["SSV_USER_ACCESS"]["other-iCalendar"] == '1'?true:false;
		$CalMain .= '<p class="cssform">
					  <br style="clear: both;"/>
					  <label for="CalType">'.$iCalendar_NewCalendar_Type.'</label>';
		if($isDBAs == false || $isIP25){
			$CalMain .= '<span id="imail_form_indside_choice">'.$iCalendar_Calendar_PersonalCalendar.'</span>';
		} else {
			$CalMain .= '<table width="100%" cellspacing="1" cellpadding="0" border="0" style="color:#000000">';
			$CalMain .= '<tr>';		# Personal Calendar
			$CalMain .= '<td width="25">'.$lui->Get_Input_RadioButton("CalType", "CalType_0", 0, (($calType=="0")?"checked":""), "onclick=\"jToggle_Calendar_Subscriber(this);jToggle_Calendar_SharedType(this)\"").'</td>';
			$CalMain .= '<td nowrap>'.$iCalendar_Calendar_PersonalCalendar.'</td>';
			$CalMain .= '</tr>';
			$CalMain .= '<tr><td colspan="2">&nbsp;</td></tr>';
			
		//if (!$isIP25){
			$CalMain .= '<tr>';		# School Calendar
			$CalMain .= '<td width="25">'.$lui->Get_Input_RadioButton("CalType", "CalType_1", 1, (($calType=="1")?"checked":""), "onclick=\"jToggle_Calendar_Subscriber(this);jToggle_Calendar_SharedType(this)\"").'</td>';
			$CalMain .= '<td>'.$iCalendar_Calendar_SchoolCalendar.'</td>';
			$CalMain .= '</tr>';
			$CalMain .= '<tr>';
			$CalMain .= '<td>&nbsp;</td>';
			$CalMain .= '<td>('.$iCalendar_SetSchoolCalender_Remark.')</td>';
			$CalMain .= '</tr>';
			$CalMain .= '<tr>';
			$CalMain .= '<td>&nbsp;</td>';
			$CalMain .= '<td>';
			$CalMain .= $lui->Get_Input_CheckBox("SchoolSharedType[]", "SchoolSharedType_0", 'T', (($calType=="1" && ($calSharedType == 'T' || $calSharedType == 'A'))?"checked":""), "onclick=\"jToggle_Calendar_CalType(1)\"").$iCalendar_Calendar_Staff;
			$CalMain .= '&nbsp;';
			$CalMain .= $lui->Get_Input_CheckBox("SchoolSharedType[]", "SchoolSharedType_1", 'S', (($calType=="1" && ($calSharedType == 'S' || $calSharedType == 'A'))?"checked":""), "onclick=\"jToggle_Calendar_CalType(1)\"").$iCalendar_Calendar_Student;
			$CalMain .= '</td>';
			$CalMain .= '</tr>';
			$CalMain .= '<tr><td colspan="2">&nbsp;</td></tr>';
	//	}
			#  Only ESFC & DEV School can use Foundation Calendar Function
			/*if($_SESSION['SchoolCode'] == 'ESFC' || $_SESSION['SchoolCode'] == 'DEV'){
				$CalMain .= '<tr>';		# Foundation Calendar
				$CalMain .= '<td width="25">'.$lui->Get_Input_RadioButton("CalType", "CalType_2", 2, (($calType=="2")?"checked":""), "onclick=\"jToggle_Calendar_Subscriber(this);jToggle_Calendar_SharedType(this)\"").'</td>';
				$CalMain .= '<td>'.$iCalendar_Calendar_FoundationCalendar.'</td>';
				$CalMain .= '</tr>';
				$CalMain .= '<tr>';
				$CalMain .= '<td>&nbsp;</td>';
				$CalMain .= '<td>('.$iCalendar_SetFoundationCalender_Remark.')</td>';
				$CalMain .= '</tr>';
				$CalMain .= '<tr>';
				$CalMain .= '<td>&nbsp;</td>';
				$CalMain .= '<td>';
				$CalMain .= $lui->Get_Input_CheckBox("FoundationSharedType[]", "FoundationSharedType_0", 'T', (($calType=="2" && ($calSharedType == 'T' || $calSharedType == 'A'))?"checked":""), "onclick=\"jToggle_Calendar_CalType(2)\"").'Staff';
				$CalMain .= '&nbsp;';
				$CalMain .= $lui->Get_Input_CheckBox("FoundationSharedType[]", "FoundationSharedType_1", 'S', (($calType=="2" && ($calSharedType == 'S' || $calSharedType == 'A'))?"checked":""), "onclick=\"jToggle_Calendar_CalType(2)\"").'Student';
				$CalMain .= '</td>';
				$CalMain .= '</tr>';
			}*/
			$CalMain .= '</table>';
		}
		$CalMain .= '</p>';
		
		###########################################################
		#  Calendar Type : 1 - School Calendar , 0 - General Calendar
		/*
		if($isDBAs == true){
			$checked = ($calType == 1) ? true : false;
			$CalMain .= "<p class='cssform'>
						  <br style='clear: both;'/>
						  <label for='CalType'>$iCalendar_NewCalendar_Type</label>
							<span id='imail_form_indside_choice'>";
			$CalMain .= $lui->Get_Input_CheckBox("CalType", "CalType", 1, $checked, "onclick=\"jToggle_Calendar_Subscriber()\"");
			$CalMain .= "&nbsp;".$iCalendar_SchoolCalendar;
			$CalMain .= "<br>($iCalendar_SetSchoolCalender_Remark)";
			$CalMain .= "</span>";
			$CalMain .= "</p>";
			$CalMain .= "<br style='clear: both;'/>";
			$CalMain .= "<br style='clear: both;'/>";
		}
		*/
		###########################################################

		# Everyone can Subscirbe
		$other = ($calType == "1") ? "disabled" : "";
		$CalMain .= "<p class='cssform'>
					<br style='clear: both;'/>
					<label for='Sender'>$iCalendar_NewCalendar_ShareWithAll</label>
					<span id='imail_form_indside_choice'>";
		$CalMain .= $lui->Get_Input_RadioButton("shareToAll", "shareToAll_1", 1, (($shareToAll=="1")?"checked":"") , $other)."$iCalendar_Yes&nbsp;";
		$CalMain .= $lui->Get_Input_RadioButton("shareToAll", "shareToAll_0", 0, (($shareToAll=="0")?"checked":"") , $other).$iCalendar_No;
		$CalMain .= "</span>";
		$CalMain .= "</p>";
		$CalMain .= "<br style='clear: both;'/>";

		# Display the number of Public Subscribers From Search
		if ($editMode && $shareToAll=="1") {
			$CalMain .= "<p class='cssform'>
						<br style='clear: both;'/>
						<label for='Sender'>$iCalendar_EditCalendar_NumOfSubscribers</label>
						<span id='imail_form_indside_choice'>";
			$CalMain .= $iCal->Get_Public_Calendar_Subscribers($calID);
			$CalMain .= '</span></p>';
			$CalMain .= "<br style='clear: both;'/>";
		}
	}
	# Button 
	$CalMain .= "<br style='clear: both;'/>";
	$CalMain .= "<p class='dotline_p'/>
				<div id='form_btn'>";
	$CalMain .= $lui->Get_Input_Button("saveCalendar", "saveCalendar", $editMode ? $Lang['btn_save']:$Lang['btn_submit'], "class='button_main_act' onclick=\"document.form1.submitMode.value = 'Save';
		validateForm();\"")."&nbsp";
	$CalMain .= $lui->Get_Input_Button("cancelbtn", "cancelbtn", $Lang['btn_cancel'], "class=\"button_main_act\" onclick=\"window.location='manage_calendar.php'\" ")."&nbsp";
	if($editMode && $isOwner) {
		if($calID != $ownDefaultCalID){
			$CalMain .= $lui->Get_Input_Button("", "", $Lang['btn_delete'], "class='button_main_act' onclick='removeCal()'");
		}
	}

if($editMode) {
	$CalMain .= $lui->Get_Input_Hidden("calID", "calID", $calID);
}

if(isset($groupID)) {
	$CalMain .= $lui->Get_Input_Hidden("groupID", "groupID", $groupID);
}
				
$CalMain .= "</div></div></div>";


$CalMain .= " <div class='jqmWindow' id='deleteCalendarWarning'>
				<p id='delConfirmMsg' align='center'></p>
				<p id='delButton' align='center'></p>
			</div>";
$CalMain .= $lui->Get_Input_Hidden("submitMode", "submitMode");

$CalMain .= '</form>
		</div>
	  </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>';

# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain, '','','','','',false,false);
#if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
#if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
#if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<style type="text/css">@import url(css/main.css);</style>
<style type="text/css">
.removedGuest {
	background-color: #CCCCCC;
}

.undoRemovedGuest {
	background-color: none;
}

// style for error message
span.error {
	color: #FF0000;
}

input.error {
	border:1px dotted red;
}

#errMessageBox {
	text-align: left;
	font-family:"Arial","Helvetica","sans-serif";
	font-size: 12px;
	color: red;
}

#deleteCalendarWarning {
	font-family:"Arial","Helvetica","sans-serif";
}

.simpleColorDisplay {
	float:left;
}
</style>
<?php
// include_once($PathRelative."src/include/template/general_header.php");

echo '<script>';
echo 'var SessionExpiredWarning = "' . $Lang['Warning']['SessionExpired'] . '";';
echo 'var SessionKickedWarning = "' . $Lang['Warning']['SessionKicked'] . '";';
echo 'var SessionExpiredMsg = "' . $SYS_CONFIG['SystemMsg']['SessionExpired'] . '";';
echo 'var SessionKickedMsg = "' . $SYS_CONFIG['SystemMsg']['SessionKicked'] . '";';
echo '</script>';
// include_once("headerInclude.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<table height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr>
<td class="bg_shadow_left"> </td>
<td class="content_bg">

<script type="text/javascript" src="js/jquery.js"></script>
<!--<script type="text/javascript" src="js/jquery.blockUI.js"></script>-->

<script type="text/javascript" src="js/jqModal.js"></script>
<link rel="stylesheet" type="text/css" href="css/jqModal.css" />

<script type="text/javascript" src="js/jquery.simpleColor.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>

<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="css/main.css" />
<link href="/theme/css/common_cammy.css" rel="stylesheet" type="text/css">
<link href="/theme/css/layout_grey.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_size_normal.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_face_verdana.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_color_layout_grey.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="<?=$SYS_CONFIG['Image']['Abs']?>favicon.ico" />


<!-- ClueTip require files begin -->
<script type="text/javascript" src="cluetip/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="cluetip/jquery.cluetip.js"></script>

<script language="javascript">
var isBlocking = false;

function jSet_All_Permission(){
	var obj = document.getElementById("permission-all");
	var selectedIndex = obj.selectedIndex;
	
	var allObj = document.getElementsByTagName("Select");
	if(selectedIndex > 0 && allObj.length > 0){
		for(i=0 ; i<allObj.length ; i++){
			if(allObj[i].id.search("permission") != -1 && allObj[i].id != "permission-all"){
				var selObj = document.getElementsByName(allObj[i].id);
				if(selObj.length > 0){
					for(j=0 ; j<selObj.length ; j++){
						selObj[j].selectedIndex = parseInt(selectedIndex)-1;
					}
				}
			}
		}
	}
	obj.selectedIndex = 0;
}

function jPick_Color(colorID){	
	var colorObj = document.getElementById("color_"+colorID);
	var colorSpanObj = document.getElementsByName("colorSpan[]");
	var calColorObj = document.getElementById("calColor");
	
	calColorObj.value = colorObj.value;
	
	if(colorSpanObj.length > 0){
		for(var i=0 ; i<colorSpanObj.length ; i++){
			colorSpanObj[i].style.display = "none";
			
			var index = (i < 10) ? '0' + i : i;
			var colorLiObj = document.getElementById("colorLi_"+index);
			if(colorLiObj){
				//colorLiObj.id = "notcurrent";
				
				var dec = "<div id=\"notcurrent\">";
				dec += "<a href=\"javascript:void(0)\" class=\"cal_calendar_"+index+"\" onclick=\"jPick_Color('"+index+"')\">";
				dec += "</div>";
				colorLiObj.innertHTML = dec;
			}
		}
	}
	
	selectedIndex = parseInt(parseInt(colorID, 10) - 1, 10);
	colorSpanObj[selectedIndex].style.display = "block";	
	// document.getElementById("colorLi_"+colorID).id = "current";
	$("li[@name=colorLi_"+colorID+"]").id = "current";
}

function confirmDeleteRedirect(calID) {
	window.location='delete_calendar.php?calID='+calID;
}

function removeViewer(viewer) {
	/* Nodes transverse: a > td > tr > tbody */
	var tableViewer = viewer.parentNode.parentNode.parentNode;
	/* Nodes transverse: a > td > tr */
	var trToRemove = viewer.parentNode.parentNode;
	var removed = tableViewer.removeChild(trToRemove);
	var tr = tableViewer.getElementsByTagName("tr");
	
	rowClass = "tablerow1";
	noOfTr = tableViewer.getElementsByTagName("TR");
	
	for (i=1; i<noOfTr.length; i++) {
		noOfTr[i].className = rowClass;
		rowClass=="tablerow1"? rowClass="tablerow2" : rowClass="tablerow1";
	}
}

function validateForm() {
	var v = $("#form1").validate({
		errorLabelContainer: $("#errMessageBox"),
		errorElement: "span", 
		wrapper: "li",
		rules: {
			calName: "required", 
			calColor: "required"
		},
		messages: {
			calName: "<?=$iCalendar_CheckForm_NoCalName?>", 
			calColor: "<?=$iCalendar_CheckForm_NoCalColor?>"
		}
	});
	
	if(v.form()){
		document.form1.submit();
	} else {
		v.focusInvalid();
		window.scrollTo(0,0);
		return false;
	}
}

function hideDeleteAlertMsg() {
	$('#deleteCalendarWarning').jqmHide();
}

$().ready(function() {
	$('.simple_color').simpleColor({
			cellWidth: 9,
			cellHeight: 9,
			border: '1px solid #333333',
			defaultColor: '#<?=$calColor?>',
			buttonClass: 'formsubbutton',
			selectLabel: '<?=$button_select?>',
			cancelLabel: '<?=$button_cancel?>'
	});
	
	$('#deleteCalendarWarning').jqm({trigger: false});
	
	// $('#saveCalendar').click(function() {
		// document.form1.submitMode.value = 'Save';
		// validateForm();
	// });
	
	$('#applyCalendar').click(function() {
		document.form1.submitMode.value = 'apply';
		validateForm();
	});
});

function submitForm() {
	document.form1.submit();
}

function removeCal() {
	var remarkMsg = '';
<?php
if($isExistGrpViewer > 0 || $isExistIndViewer > 0){
?>
	remarkMsg += "<div style='text-align:left'><?=str_replace("#%calName%#", $calName, $icalender_deleteCalendar_existGuest_remark)?></div>";
	remarkMsg += "<br>";
<?php
}
?>
	var confirmMsg = "<?=str_replace("#%calName%#",$calName,$iCalendar_ManageCalendar_ConfirmMsg1)?>";
	$('#delConfirmMsg').html(remarkMsg+confirmMsg);
	
	var delButton = "<?=str_replace("calID", $calID, $deleteCalButton)?>";
	$('#delButton').html(delButton);
	// alert($('#deleteCalendarWarning').length);
	$('#deleteCalendarWarning').jqmShow();
}

function toggleInvolveEventVisibility(){
	$(".involve").toggle();
}

function toggleSchoolEventVisibility() {
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
}

function toggleFoundationEventVisibility(obj){ 
	$('.foundation').toggle();
}

function toggleCalVisibility(parClassID) {
	$('.cal-'+parClassID).toggle();
}

function toggleOwnDefaultCalVisibility(parClassID){
	toggleCalVisibility(parClassID);
	
	var obj = document.getElementById("toggleCalVisible-"+parClassID);
	var involObj = document.getElementById("toggleVisible");
	if(obj.checked == true){
		if(involObj.checked == false)
			toggleInvolveEventVisibility();
		involObj.checked = true;
	} else {
		if(involObj.checked == true)
			toggleInvolveEventVisibility();
		involObj.checked = false;
	}	
}

function toggleExternalCalVisibility(selfName,eventClass,CalType){
	//toggleCalVisibility(eventClass,selfName);
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calType : CalType		
	}, function(responseText) {
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	// isDisplayMoreFunction();
	// jHide_All_Layers();
}


function jToggle_Calendar_Subscriber(obj){
	var sub_obj = document.getElementsByName("shareToAll");
	
	if(sub_obj.length > 0){
		for(var i=0 ; i<sub_obj.length ; i++){
			if(obj.value != 0 && obj.checked == true){
				sub_obj[i].checked = (i == 0) ? true : false;
				sub_obj[i].disabled = true;
			} else {
				sub_obj[i].disabled = false;
			}
		}
	}
}

function jSet_CheckBox_Value(objName, selValue, chkStatus){
	var obj = document.getElementsByName(objName);
	if(obj.length > 0){
		for(var i=0 ; i<obj.length ; i++){
			if(selValue != ''){
				if(obj[i].value == selValue){
					obj[i].checked = (chkStatus == 1) ? true : false;
				}
			} else {
				obj[i].checked = (chkStatus == 1) ? true : false;
			}
		}
	}
}	

function jToggle_Calendar_SharedType(obj){
	if(obj.value == 0){
		jSet_CheckBox_Value('SchoolSharedType[]', '', 0);
		jSet_CheckBox_Value('FoundationSharedType[]', '', 0);
		jDisable_AddUser(0);
		$('span#FoundationBtnRemark').hide();
	} else if(obj.value == 1){
		jSet_CheckBox_Value('FoundationSharedType[]', '', 0);
		jDisable_AddUser(0);
		$('span#FoundationBtnRemark').hide();
	} else if(obj.value == 2){
		jSet_CheckBox_Value('SchoolSharedType[]', '', 0);
		jDisable_AddUser(1);
		$('span#FoundationBtnRemark').show();
	}
}

function jToggle_Calendar_CalType(calType){
	if(calType == 1){
		jSet_CheckBox_Value('CalType', 1, 1);
		jSet_CheckBox_Value('FoundationSharedType[]', '', 0);
		jDisable_AddUser(0);
		$('span#FoundationBtnRemark').hide();
	} else if(calType == 2){
		jSet_CheckBox_Value('CalType', 2, 1);
		jSet_CheckBox_Value('SchoolSharedType[]', '', 0);
		jDisable_AddUser(1);
		$('span#FoundationBtnRemark').show();
	}
}

function jDisable_AddUser(disable){
	var obj = document.getElementById('AddUser');
	//obj.disabled = (disable == 1) ? true : false;
	obj.style.display = (disable == 1) ? 'none' : 'block';
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>

<div id="module_bulletin" class="module_content">
<?php
echo $lui->Get_Sub_Function_Header("iCalendar",$Msg);
echo $display;
?>
</div>

<?php


if(count($ColorPickArr) > 0){
	foreach($ColorPickArr as $key => $code){
		echo $lui->Get_Input_Hidden("color_".$key, "color_".$key, $code)."\n";
	}
}


// include_once($PathRelative."src/include/template/general_footer.php");
include_once("footerInclude.php");
?>
