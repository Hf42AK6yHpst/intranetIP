<?php
// page modifing by: 
/**************************************** Changes ********************************************
 * 2017-01-24 (Carlos): [ip2.5.8.3.1] Current user would not be explicitly in participant list. Added send push msg btn. Added extra title info handling. 
 * 2016-09-23 (Carlos): Modified to make compulsory invited events to force join the event. No need to confirm invitation notification.
 * 2011-09-19 (Carlos): Set $location to INTRANET_EVENT EventVenue 
 *********************************************************************************************/
 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once("choose/new/User_Group_Transformer.php");

intranet_auth();
intranet_opendb();

$iCal = new icalendar();
$db = new libdb();
$transformer = new User_Group_Transformer();
####################################### START UPDATE #######################################

$result = array();

## Get Data


$eventTimeStamp = $iCal->sqlDatetimeToTimestamp($eventDate);

if($ShowInSchoolCalendar){
	$targetStartDate = $eventDate;
	$targetEndDate = $eventEndDate;
}

$datePieces = explode("-", $eventDate);

$datePieces2 = explode("-", $eventEndDate);

$m = strlen($datePieces[1])==1? "0".$datePieces[1]:$datePieces[1];
$d = strlen($datePieces[2])==1? "0".$datePieces[2]:$datePieces[2];
$h = "00";
if (isset($eventHr))
	$h = strlen($eventHr)==1? "0".$eventHr:$eventHr;
$i = "00";
if (isset($eventMin))
	$i = strlen($eventMin)==1? "0".$eventMin:$eventMin;
$startEvent = $datePieces[0].$m.$d."T".$h.$i."00";
$eventTimeStamp=mktime($h,$i,0,$m,$d,$datePieces[0]);//modified on 16 July 2009
$m = strlen($datePieces2[1])==1? "0".$datePieces2[1]:$datePieces2[1];
$d = strlen($datePieces2[2])==1? "0".$datePieces2[2]:$datePieces2[2];
$h = "00";
if (isset($eventEndHr))
	$h = strlen($eventEndHr)==1? "0".$eventEndHr:$eventEndHr;
$i = "00";
if (isset($eventEndMin))
	$i = strlen($eventEndMin)==1? "0".$eventEndMin:$eventEndMin;
$endEvent = $datePieces2[0].$m.$d."T".$h.$i."00";

$duration = $iCal->time_diff_UTC($startEvent,$endEvent);
if ($duration < 0){
	$Msg = $Lang['ReturnMsg']['EventCreateUnSuccess'];
	header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&Msg=$Msg");
	exit;
}

if (!isset($isAllDay) || $isAllDay != 1 || $isAllDay != "1") {
	$isAllDay = 0;
	$eventDate = "$eventDate $eventHr:$eventMin:00";
} else {
	$eventDate = "$eventDate 00:00:00";
	$duration = $duration + 1440; 
}

$access == ""?$access="D":$access=$access;

if (!$iCal->isValidURL($url))
	$url=NULL;
	

$eBookingRecordRemarksSql = '';
$eBookingRecordRemarksSql .= $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title));
if ($description) {
	$eBookingRecordRemarksSql .= "\n\n".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description));
}

$allEventIdAry = array();

if ($repeatSelect == "NOT" || !isset($repeatSelect)) {
	// Single Event
	# Insert main detail
	$fieldname  = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
	$fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID"; 
	
	//*********temporary approach**************
	//if($isAllDay=='1'&& $duration == 0)
	//	$duration = 1440;
	//************************************** 
	
	$fieldvalue  = $_SESSION['UserID'].", '$eventDate', NOW(), NOW(), '$duration', ";
	$fieldvalue .= "'$isImportant', '$isAllDay', '$access', '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
	$fieldvalue .= "'".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', '$url', '$calID'";
	
	$sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
	
	$result['insert_event_entry'] = $iCal->db_db_query($sql);
	$eventID = $iCal->db_insert_id();
	
	//global $schoolNameAbbrev;
	$uid = $_SESSION['UserID']."-{$calID}-{$eventID}@".$_SERVER['SERVER_NAME'];
	$sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where EventID = '$eventID'";
	$iCal->db_db_query($sql);
	
	$sql = "select * from  CALENDAR_CALENDAR where CalID = '".trim($calID)."' ";
	$resultSet = $iCal->returnArray($sql);
	
	// insert Personal Note
	if(!in_array(false, $result)){
		$pNote =$iCal->Get_Safe_Sql_Query($iCal->Get_Request($PersonalNote));
		$pNote = empty($pNote)?"NULL":"'".$pNote."'";
		$sql = 'insert into CALENDAR_EVENT_PERSONAL_NOTE (EventID, UserID, PersonalNote, CreateDate, CalType) 
						values 
						(\''.$eventID.'\',\''.$_SESSION['UserID'].'\','.$pNote.',NOW(),\''.$resultSet[0]["CalType"].'\')';
		$result['insert_personal_note'] = $iCal->db_db_query($sql);
	}
	
	## Show in school calendar (Group Event Only)##
	if($ShowInSchoolCalendar)
	{
		$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
		$arrTargetGroupID = $db->returnVector($sql);
		$targetGroupID = $arrTargetGroupID[0]; 
		
		if($targetGroupID != "")
		{
			$NumOfDay = round( abs(strtotime($targetEndDate)-strtotime($targetStartDate)) / 86400, 0 );
			
			for($i=0; $i<=$NumOfDay; $i++) 
			{
				## Calculate the Event Target Date
				$ts_EventStartDate = strtotime($targetStartDate);
				$ts_TargetEventDate = $ts_EventStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
				$TargetEventDate = date("Y-m-d",$ts_TargetEventDate);
				//$title = trim(stripslashes(urldecode($title)));
				//$description = trim(stripslashes(urldecode($description)));
				$sql = "INSERT INTO 
							INTRANET_EVENT 
							(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
						VALUES
							('$TargetEventDate',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$UserID',NOW(),'$UserID')";
				$db->db_db_query($sql);
			
				//if($i==0) {
					$ParentSchoolCalEventID = $db->db_insert_id();
					$CurrentSchoolCalEventID = $db->db_insert_id();
				//} else {
				//	$CurrentSchoolCalEventID = $db->db_insert_id();
				//}
								
				$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
				$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
				$db->db_db_query($sql);
				
				$sql = "UPDATE INTRANET_EVENT 
						SET 
							RelatedTo = '$ParentSchoolCalEventID',
							DateModified = NOW(),
							ModifyBy = '$UserID'
						WHERE 
							EventID = '$CurrentSchoolCalEventID'";
				$db->db_db_query($sql);
				
				$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$eventID','$CurrentSchoolCalEventID')";
				$db->db_db_query($sql);
			}
		}
		## update school calendar (calendar view) 
		include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
		$lcycleperiods = new libcycleperiods();
		$lcycleperiods->generatePreview();
		$lcycleperiods->generateProduction();
	}
	##### EBOOKING #####
	if($plugin['eBooking'])
	{
		if($hiddenBookingIDs != "")
		{
			include_once($PATH_WRT_ROOT."includes/libebooking.php");
			$lebooking = new libebooking();
			$hiddenBookingIDs = IntegerSafe($hiddenBookingIDs);
			$hiddenBookingIdAry = explode(',', $hiddenBookingIDs);
						
			$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
			$arrRoomID = $iCal->returnVector($sql);
			$roomId = $arrRoomID[0];
			
			$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($hiddenBookingIdAry);
			$bookingDate = $bookingInfoAry[0]['Date'];
			$bookingStartTime = $bookingInfoAry[0]['StartTime'];
			$bookingEndTime = $bookingInfoAry[0]['EndTime'];
			
			$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $roomId);
			$IsRoomAvailable = false;
			if ($RoomNeedApproval[$hiddenBookingIDs]) {
				$RoomBookingStatus = 0;
				
				$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($hiddenBookingIDs)";
				$iCal->db_db_query($sql);
			
//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($hiddenBookingIDs)";
//				$iCal->db_db_query($sql);
			} else {
				$RoomBookingStatus = 1;
				
				$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $bookingDate, $bookingStartTime, $bookingEndTime);
				if ($IsRoomAvailable) {
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($hiddenBookingIDs)";
					$iCal->db_db_query($sql);
				}
			
//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($hiddenBookingIDs)";
//				$iCal->db_db_query($sql);
			}
			
			$sql = "UPDATE INTRANET_EBOOKING_RECORD SET Remark = '".$eBookingRecordRemarksSql."' Where BookingID IN ($hiddenBookingIDs)";
			$iCal->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$eventID' WHERE BookingID IN ($hiddenBookingIDs)";
			$iCal->db_db_query($sql);
			
			include_once($PATH_WRT_ROOT."includes/libinventory.php");
			$linventory = new libinventory();
			$sql = "SELECT 
						CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
					FROM
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
						INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = 1)
					WHERE
						BookingID IN ($hiddenBookingIDs)";
						
			$arrLocation = $linventory->returnVector($sql);

			$location = $arrLocation[0];
			
			$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$eventID' ";
			$iCal->db_db_query($sql);
			
			if ($RoomNeedApproval[$hiddenBookingIDs] == false && $IsRoomAvailable) {
				$lebooking->Email_Booking_Result($hiddenBookingIDs);
			}
		}
	}
} else {
	# convert repeat end date to datetime format		//REPEATED EVENT 
	
	$repeatEndDatePieces = explode("-", $repeatEnd);
	
	if ($isAllDay != 1) {
		$repeatEndTime = mktime($eventHr,$eventMin,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
	} else {
		$repeatEndTime = mktime(0,0,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
	}
	$repeatEndDate = date("Y-m-d H:i:s", $repeatEndTime);
	
	// save the repeat pattern for future reference
	$fieldname  = "RepeatType, EndDate, Frequency, Detail";
	$detail = "";
	$frequency = 0;
	switch ($repeatSelect) {
		case "DAILY":
			$fieldvalue = "'$repeatSelect', '$repeatEndDate', $repeatDailyDay, NULL";
			$frequency = $repeatDailyDay;
			break;
		case "WEEKDAY":
		case "MONWEDFRI":
		case "TUESTHUR":
			$fieldvalue = "'$repeatSelect', '$repeatEndDate', NULL, NULL";
			break;
		case "WEEKLY":
			$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
			$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
			$fieldvalue = "'$repeatSelect', '$repeatEndDate', $repeatWeeklyRange, '$detail'";
			$frequency = $repeatWeeklyRange;
			break;
		case "MONTHLY":
			if ($monthlyRepeatBy == "dayOfMonth")
				$detail = "day-".$datePieces[2];
			else {
				$eventDateWeekday = date("w", $eventTimeStamp);
				$nth = 0;
				for($i=1; $i<6; $i++) {
					$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
					if ((int)$datePieces[2] == (int)$nthweekday) {
						$nth = $i;
						break;
					} 
				}
				
				$formatDate = $datePieces[0].$datePieces[1].$datePieces[2];
				if (date("m",strtotime("+7 day",$eventTimeStamp))!=$datePieces[1])
					$nth = -1;
				
				$detail = "$nth-$eventDateWeekday";
			}
			$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatMonthlyRange', '$detail'";
			$frequency = $repeatMonthlyRange;
			break;
		case "YEARLY":
			$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatYearlyRange', NULL";
			$frequency = $repeatYearlyRange;
			break;
	}
	
	$sql = "INSERT INTO CALENDAR_EVENT_ENTRY_REPEAT ($fieldname) VALUES ($fieldvalue)";
	$result['insert_event_repeat'] = $iCal->db_db_query($sql);
	$repeatID = $iCal->db_insert_id();
	
	# Insert the recurrence event series
	$detailArray = array($repeatID, $repeatSelect, $repeatEndDate, $frequency, $detail, $_SESSION['UserID'],
					$eventDate, $duration, $isImportant, $isAllDay, $access, $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title)), 
					$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description)), $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)), $url, $calID,$iCal->Get_Request($PersonalNote));
		
	if (!isset($monthlyRepeatBy))
		$monthlyRepeatBy = "";
	
	# Main function for inserting recurrence events
	$result['insert_repeat_event_entry'] = $iCal->insertRepeatEventSeries($detailArray, $monthlyRepeatBy, $isIgnoreModifiedRecord = 1);
	
	$sql = "select min(EventID) as minEvent from CALENDAR_EVENT_ENTRY where RepeatID = '$repeatID'";
	$minEventID = $iCal->returnArray($sql);
	
	$uid = $_SESSION['UserID']."-{$calID}-".$minEventID[0]["minEvent"]."@".$_SERVER['SERVER_NAME'];
	$sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where RepeatID = '$repeatID'";
	$iCal->db_db_query($sql);
	
	$sql = "select * from  CALENDAR_CALENDAR where CalID = '".trim($calID)."'";
	$resultSet = $iCal->returnArray($sql);
	
	if(!in_array(false, $result)){
	$pNote = $iCal->Get_Safe_Sql_Query($iCal->Get_Request($PersonalNote));
		$pNote = empty($pNote)?"NULL":"'".$pNote."'";
		
		$sql = "insert into CALENDAR_EVENT_PERSONAL_NOTE (EventID,UserID,PersonalNote,CreateDate,CalType) 
					select EventID, '".$_SESSION['UserID']."', ".$pNote.", NOW(), '".$resultSet[0]["CalType"]."'
					from CALENDAR_EVENT_ENTRY where RepeatID = '".$repeatID."'";
		$result['insert_repeat_event_personal_note'] = $iCal->db_db_query($sql);
	}
	
	## Show in school calendar (Group Event Only)##
	if($ShowInSchoolCalendar)
	{
		$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
		$arrTargetGroupID = $db->returnVector($sql);
		$targetGroupID = $arrTargetGroupID[0]; 
		
		if($targetGroupID != "")
		{
			$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
			$arrCalendarEvent = $iCal->returnArray($sql,2);
			
			if(sizeof($arrCalendarEvent)>0)
			{
				for($i=0; $i<sizeof($arrCalendarEvent); $i++)
				{
					list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
					
					//$title = trim(stripslashes(urldecode($title)));
					//$description = trim(stripslashes(urldecode($description)));
					
					$sql = "INSERT INTO 
									INTRANET_EVENT 
									(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
							VALUES
									('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$UserID',NOW(),'$UserID')";
					$iCal->db_db_query($sql);
					
					//if($i==0) {
						$ParentSchoolCalEventID = $db->db_insert_id();
						$CurrentSchoolCalEventID = $db->db_insert_id();
					//} else {
					//	$CurrentSchoolCalEventID = $db->db_insert_id();
					//}
								
					$value = "($targetGroupID,$CurrentSchoolCalEventID,NOW(),NOW())";
					$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
					$db->db_db_query($sql);
				
					$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
				
					$sql = "UPDATE INTRANET_EVENT 
							SET 
								RelatedTo = '$ParentSchoolCalEventID',
								DateModified = NOW(),
								ModifyBy = '$UserID'
							WHERE 
								EventID = '$CurrentSchoolCalEventID'";
					$db->db_db_query($sql);
					
					$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
					$db->db_db_query($sql);
				}
			}
		}
		## update school calendar (calendar view) 
		include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
		$lcycleperiods = new libcycleperiods();
		$lcycleperiods->generatePreview();
		$lcycleperiods->generateProduction();
	}
	
	##### EBOOKING ######
	if($plugin['eBooking'])
	{
		if($hiddenBookingIDs != "")
		{
			include_once($PATH_WRT_ROOT."includes/libebooking.php");
			$lebooking = new libebooking();
			$hiddenBookingIDs = IntegerSafe($hiddenBookingIDs);
			$hiddenBookingIdAry = explode(',', $hiddenBookingIDs);
			
			$sql = "UPDATE INTRANET_EBOOKING_RECORD SET Remark = '".$eBookingRecordRemarksSql."' Where BookingID IN ($hiddenBookingIDs)";
			$iCal->db_db_query($sql);
			
			$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
			$arrRoomID = $iCal->returnVector($sql);
			$roomId = $arrRoomID[0];
			
			$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($hiddenBookingIdAry);
			$bookingDate = $bookingInfoAry[0]['Date'];
			$bookingStartTime = $bookingInfoAry[0]['StartTime'];
			$bookingEndTime = $bookingInfoAry[0]['EndTime'];
			
			
			$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $roomId);
			$IsRoomAvailable = false;
			if ($RoomNeedApproval[$hiddenBookingIDs]) {
				$RoomBookingStatus = 0;
				
				$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($hiddenBookingIDs)";
				$iCal->db_db_query($sql);
				
//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($hiddenBookingIDs)";
//				$iCal->db_db_query($sql);
			} else {
				$RoomBookingStatus = 1;
				
				$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $bookingDate, $bookingStartTime, $bookingEndTime);
				if ($IsRoomAvailable) {
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($hiddenBookingIDs)";
					$iCal->db_db_query($sql);
				}
				
//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($hiddenBookingIDs)";
//				$iCal->db_db_query($sql);
			}
				
			$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
			$arrEventInfo = $iCal->returnArray($sql,2);
			
			$sql = "SELECT BookingID, CONCAT(Date,' ',StartTime) AS EventDate FROM INTRANET_EBOOKING_RECORD WHERE BookingID in ($hiddenBookingIDs)";
			$arrBookingInfo = $iCal->returnArray($sql,2);
			
			if(sizeof($arrEventInfo)>0)
			{
				for($i=0; $i<sizeof($arrEventInfo); $i++)
				{
					list($event_id, $event_date) = $arrEventInfo[$i];
					$arrEventDate[] = $event_date;
					$arrMergeInfo[$event_date]['EventID'] = $event_id;
				}
			}
			
			if(sizeof($arrBookingInfo)>0)
			{
				for($i=0; $i<sizeof($arrBookingInfo); $i++)
				{
					list($booking_id, $booking_date) = $arrBookingInfo[$i];
					$arrEventDate[] = $booking_date;
					$arrMergeInfo[$booking_date]['BookingID'] = $booking_id;
				}
			}
			$arrEventDate = array_unique($arrEventDate);
			
			foreach($arrEventDate as $key=>$date)
			{
				$targetEventID = $arrMergeInfo[$date]['EventID'];
				$targetBookingID = $arrMergeInfo[$date]['BookingID'];
				
				if(empty($targetBookingID))
					continue;
				
				$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$targetEventID' WHERE BookingID = '$targetBookingID'";
				$iCal->db_db_query($sql);
				
				include_once($PATH_WRT_ROOT."includes/libinventory.php");
				$linventory = new libinventory();
				$sql = "SELECT 
							CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
						FROM
							INVENTORY_LOCATION_BUILDING AS building 
							INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
							INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
							INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
						WHERE
							BookingID IN ($targetBookingID)";
				
				$arrLocation = $linventory->returnVector($sql);
				$location = $arrLocation[0];
				
				$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$targetEventID'";
				$iCal->db_db_query($sql);
				
				if ($RoomNeedApproval[$hiddenBookingIDs] == false && $IsRoomAvailable) {
					$lebooking->Email_Booking_Result($targetBookingID);
				}
			}
		}
	}
}

if (!isset($viewer)) {
	$viewer = array();
}
$viewer = array_values(array_unique($viewer));
################################################################################################
# When create NEW event
# Case 1: if no any guest is invited , EventType is regarded ar NOT Defined
# Case 2: if at least one guest is invited, EventType is set to be Compulsory / Optional
#
# if number of guest > 0, inviteStatus is valid
# if number of guest = 0, inviteStatus is not valid and set to NULL
$InviteStatus = (count($viewer) > 0) ? $InviteStatus : "NULL";
$InviteEventUserStatus = $InviteStatus == "1" ?  "W" : "A";
if($InviteStatus == '0') $InviteStatus="NULL"; // if it is compulsory, force to join the event. 
################################################################################################

# New guest list
# to be done
# Original Approach
$viewerList = array();
$otherSchoolViewer = array();

for ($i=0; $i<sizeof($viewer); $i++) {
	$viewerPrefix = substr($viewer[$i], 0, 1);
	$viewerID = substr($viewer[$i], 1);
	if ($viewerPrefix == 'U')
		$viewerList[] = $viewerID ;
	else{
		$tempGuest = $transformer->get_userID_from_group($viewerPrefix,$viewerID);
		$viewerList = array_merge($viewerList,$tempGuest);
	}
	# old approach
	/*$parts = split("-",$viewerID);
	if (count($parts)>1 && trim($parts[0]) != $UserID){ #other school guest
		$otherSchoolViewer[] = Array("userID"=> trim($parts[0]), "schoolCode"=>trim($parts[1]));
	}
	else if ($viewerPrefix == "U") {	// Single user
		$viewerList[] = $viewerID;
	} else if ($viewerPrefix == "P") {	// Parent from student ID
		$parents = $iCal->returnStudentParent($viewerID);
		if (sizeof($parents) > 0) {
			for($j=0; $j<sizeof($parent); $j++) {
				$viewerList[] = $parent[$j];
			}
		}
	} else if ($viewerPrefix == "G") {	// Class
		$classStudents = $iCal->returnClassStudentList($viewerID);
		if (sizeof($classStudents) > 0) {
			for($k=0; $k<sizeof($classStudents); $k++) {
				$viewerClassList[] = $classStudents[$k];
				$viewerInClass[$classStudents[$k]] = $viewerID;
			}
		}
	} else if ($viewerPrefix == "C") {	// Course
		$courseStudents = $iCal->returnCourseStudentList($viewerID);
		if (sizeof($courseStudents) > 0) {
			for($k=0; $k<sizeof($courseStudents); $k++) {
				$viewerCourseList[] = $courseStudents[$k];
				$viewerInCourse[$courseStudents[$k]] = $viewerID;
			}
		}
	} else if ($viewerPrefix == "E") {	// Intranet group
		$groupStudents = $iCal->returnGroupMemberList($viewerID);
		if (sizeof($groupStudents) > 0) {
			for($k=0; $k<sizeof($groupStudents); $k++) {
				$viewerGroupList[] = $groupStudents[$k];
				$viewerInGroup[$groupStudents[$k]] = $viewerID;
			}
		}
	}*/
}
################

# remove owner
//$viewerList = $iCal->removeAnArrayValue($viewerList, $_SESSION['UserID']);
//$viewerClassList = $iCal->removeAnArrayValue($viewerClassList, $_SESSION['UserID']);
//$viewerCourseList= $iCal->removeAnArrayValue($viewerCourseList, $_SESSION['UserID']);
//$viewerGroupList = $iCal->removeAnArrayValue($viewerGroupList, $_SESSION['UserID']);

$viewerList = array_values(array_unique($viewerList));

# remove duplicate reminder
if(count($reminderTime) > 0){
	for($i=0; $i<sizeof($reminderTime) ; $i++) {
		$reminders[] = $reminderType[$i].$reminderTime[$i];
	}
	array_unique($reminders);
}

# insert reminder
//if (isset($reminderType) && isset($reminderTime)) {
if (isset($reminders) && sizeof($reminders) > 0) {
	// if($CalType == 2 || $CalType == 3)
	// {
		// $fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore, CalType";
	// }else
	// {
		$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
	// }
	if ($repeatSelect == "NOT") {
		# Insert reminder for creator
		//$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
		$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) values ";
		$cnt = 0;
		//for ($i=0; $i<sizeof($reminderType); $i++) {
		for ($i=0; $i<sizeof($reminders); $i++) {
			$rType = substr($reminders[$i], 0 ,1);
			$rTime = substr($reminders[$i], 1);
			# convert duration to actual datetime of sending the reminder
			$reminderDate[$i] = $eventTimeStamp - ((int)$rTime)*60; //modified on 16 July 2009
		//	$reminderDate[$i] = $eventTimeStamp;//modified on 16 July 2009
			$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
			//$sql .= "($eventID, ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$i]."', ".$rTime."), ";
			// if($CalType == 2 || $CalType == 3)//modified on 20 July 2009
			// {
				// if($cnt == 0){
					// $sql .= "SELECT $eventID, ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$i]."', ".$rTime.", ".$CalType;
					// $cnt++;
				// } else {
					// $sql .= " UNION ALL SELECT $eventID, ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$i]."', ".$rTime.", ".$CalType;
				// }
			// }else
			// {
				if($cnt == 0){
					$sql .= "('$eventID', '".$_SESSION['UserID']."', '".$rType."', '".$reminderDate[$i]."', '".$rTime."')";
					$cnt++;
				} else {
					$sql .= ", ('$eventID', '".$_SESSION['UserID']."', '".$rType."', '".$reminderDate[$i]."', '".$rTime."')";
				}
			//}
		}
		
		####### Insert a default reminders for guests #######
		$guestReminderDate = $eventTimeStamp - 10*60;	# 10 minutes before the event
		$guestReminderDate = date("Y-m-d H:i:s", $guestReminderDate);
		
		if (isset($viewerList) && sizeof($viewerList) != 0) {
			for ($i=0; $i<sizeof($viewerList); $i++) {
				//$sql .= "($eventID, ".$viewerList[$i].", 'P', '$guestReminderDate', 10), ";
				/*if($CalType == 2 || $CalType ==3)//modified on 20 July 2009
				{
					if($cnt == 0){
						$sql .= "SELECT $eventID, ".$viewerList[$i].", 'P', '$guestReminderDate', 10, $CalType";
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT $eventID, ".$viewerList[$i].", 'P', '$guestReminderDate', 10, $CalType";
					}
				}else
				{*/
					if($cnt == 0){
						$sql .= " ('$eventID', '".$viewerList[$i]."', 'P', '$guestReminderDate', 10)";
						$cnt++;
					} else {
						$sql .= ", ('$eventID', '".$viewerList[$i]."', 'P', '$guestReminderDate', 10)";
					}
				//}
			}
		}
		
		if (isset($viewerClassList) && sizeof($viewerClassList) != 0) {
			for ($i=0; $i<sizeof($viewerClassList); $i++) {
				//$sql .= "($eventID, ".$viewerClassList[$i].", 'P', '$guestReminderDate', 10), ";
				/*if($CalType == 2 || $CalType ==3)//modified on 20 July 2009
				{
					if($cnt == 0){
						$sql .= "SELECT $eventID, ".$viewerClassList[$i].", 'P', '$guestReminderDate', 10, $CalType";
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT $eventID, ".$viewerClassList[$i].", 'P', '$guestReminderDate', 10, $CalType";
					}
				}else
				{*/
					if($cnt == 0){
						$sql .= " ('$eventID', '".$viewerClassList[$i]."', 'P', '$guestReminderDate', 10)";
						$cnt++;
					} else {
						$sql .= ", ('$eventID', '".$viewerClassList[$i]."', 'P', '$guestReminderDate', 10)";
					}
				//}
			}
		}
		
		if (isset($viewerCourseList) && sizeof($viewerCourseList) != 0) {
			for ($i=0; $i<sizeof($viewerCourseList); $i++) {
				//$sql .= "($eventID, ".$viewerCourseList[$i].", 'P', '$guestReminderDate', 10), ";
				/*if($CalType == 2 || $CalType ==3)//modified on 20 July 2009
				{
					if($cnt == 0){
						$sql .= "SELECT $eventID, ".$viewerCourseList[$i].", 'P', '$guestReminderDate', 10, $CalType";
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT $eventID, ".$viewerCourseList[$i].", 'P', '$guestReminderDate', 10, $CalType";
					}
				}else
				{*/
					if($cnt == 0){
						$sql .= "('$eventID', '".$viewerCourseList[$i]."', 'P', '$guestReminderDate', 10)";
						$cnt++;
					} else {
						$sql .= ", ('$eventID', '".$viewerCourseList[$i]."', 'P', '$guestReminderDate', 10)";
					}
				//}
			}
		}
		
		if (isset($viewerGroupList) && sizeof($viewerGroupList) != 0) {
			for ($i=0; $i<sizeof($viewerGroupList); $i++) {
				//$sql .= "($eventID, ".$viewerGroupList[$i].", 'P', '$guestReminderDate', 10), ";
				/*if($CalType == 2 || $CalType ==3)//modified on 20 July 2009
				{
					if($cnt == 0){
						$sql .= "SELECT $eventID, ".$viewerGroupList[$i].", 'P', '$guestReminderDate', 10, $CalType";
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT $eventID, ".$viewerGroupList[$i].", 'P', '$guestReminderDate', 10, $CalType";
					}
				}else
				{*/
					if($cnt == 0){
						$sql .= " ('$eventID', '".$viewerGroupList[$i]."', 'P', '$guestReminderDate', 10)";
						$cnt++;
					} else {
						$sql .= ", ('$eventID', '".$viewerGroupList[$i]."', 'P', '$guestReminderDate', 10)";
					}
				//}
			}
		}
		
		$sql = rtrim($sql, ", ");
		$result['insert_event_reminder'] = $iCal->db_db_query($sql);
	} else {
		$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
		//$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) ";
		$cnt = 0;
		$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
		for ($i=0; $i<sizeof($repeatSeries) ; $i++) {
		//	$eventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 16 July 2009
			$repeatEventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 17 July 2009
			# Insert reminder for creator
			for ($j=0; $j<sizeof($reminders); $j++) {
				$rType = substr($reminders[$j], 0 ,1);		// modified on 14 May 08	$i to $j
				$rTime = substr($reminders[$j], 1);			// modified on 14 May 08
			//	$reminderDate[$j] = $eventTimeStamp - ((int)$rTime)*60;//modified on 17 July 2009
				$reminderDate[$j] = $repeatEventTimeStamp - ((int)$rTime)*60;//modified on 17 July 2009
				$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
				//$sql .= "(".$repeatSeries[$i]["EventID"].", ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$j]."', ".$rTime."), ";
				/*if($CalType == 2 || $CalType == 3)//modified on 20 July 2009
				{
					if($cnt == 0){
						$sql .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$j]."', ".$rTime.", ".$CalType;
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$j]."', ".$rTime.", ".$CalType;
					}
				}else
				{*/
					if($cnt == 0){
						$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$_SESSION['UserID']."', '".$rType."', '".$reminderDate[$j]."', '".$rTime."')";
						$cnt++;
					} else {
						$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$_SESSION['UserID']."', '".$rType."', '".$reminderDate[$j]."', '".$rTime."')";
					}
				//}
			}
			
			$guestReminderDate = $eventTimeStamp - 10*60;	# 10 minutes before the event
			$guestReminderDate = date("Y-m-d H:i:s", $guestReminderDate);
			
			if (isset($viewerList) && sizeof($viewerList) != 0) {
				# Insert reminder for guests with default values
				for ($k=0; $k<sizeof($viewerList); $k++) {
					//$sql .= "(".$repeatSeries[$i]["EventID"].", ".$viewerList[$k].", 'P', '$guestReminderDate', 10), ";
					/*if($CalType == 2 || $CalType == 3)//modified on 20 July 2009
					{
						if($cnt == 0){
							$sql .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerList[$k].", 'P', '$guestReminderDate', 10, $CalType";
							$cnt++;
						} else {
							$sql .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerList[$k].", 'P', '$guestReminderDate', 10, $CalType";
						}
					}else
					{*/
						if($cnt == 0){
							$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$guestReminderDate', 10)";
							$cnt++;
						} else {
							$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$guestReminderDate', 10)";
						}
					//}
				}
			}
			/*
			if (isset($viewerClassList) && sizeof($viewerClassList) != 0) {
				for ($k=0; $k<sizeof($viewerClassList); $k++) {
					//$sql .= "(".$repeatSeries[$i]["EventID"].", ".$viewerClassList[$k].", 'P', '$guestReminderDate', 10), ";
					if($cnt == 0){
						$sql .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerClassList[$k].", 'P', '$guestReminderDate', 10";
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerClassList[$k].", 'P', '$guestReminderDate', 10";
					}
				}
			}
			
			if (isset($viewerCourseList) && sizeof($viewerCourseList) != 0) {
				for ($k=0; $k<sizeof($viewerCourseList); $k++) {
					//$sql .= "(".$repeatSeries[$i]["EventID"].", ".$viewerCourseList[$k].", 'P', '$guestReminderDate', 10), ";
					if($cnt == 0){
						$sql .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerCourseList[$k].", 'P', '$guestReminderDate', 10";
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerCourseList[$k].", 'P', '$guestReminderDate', 10";
					}
				}
			}
			
			if (isset($viewerGroupList) && sizeof($viewerGroupList) != 0) {
				for ($k=0; $k<sizeof($viewerGroupList); $k++) {
					//$sql .= "(".$repeatSeries[$i]["EventID"].", ".$viewerGroupList[$k].", 'P', '$guestReminderDate', 10), ";
					if($cnt == 0){
						$sql .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerGroupList[$k].", 'P', '$guestReminderDate', 10";
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerGroupList[$k].", 'P', '$guestReminderDate', 10";
					}
				}
			}
			*/
		}
		$sql = rtrim($sql, ", ");
		$result['insert_event_reminder'] = $iCal->db_db_query($sql);	
	}
}



# Sharing
if ($repeatSelect == "NOT" || !isset($repeatSelect)) {
	$allEventIdAry[] = $eventID;
	// insert the owner of the event with "A"(Full) access right
//	$fieldname  = "EventID, UserID, Status, Access, InviteStatus";
//	$fieldvalue = $eventID.", ".$_SESSION['UserID'].", 'A', 'A', ".$InviteStatus."";
//	$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ($fieldvalue)";
	
//	$result['insert_event_onwer'] = $iCal->db_db_query($sql);
	
	if (isset($viewerList) && sizeof($viewerList) != 0) {
		$fieldname  = "EventID, UserID, Status, Access, InviteStatus";
		$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
		//$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) ";
		$cnt = 0;
		for ($i=0; $i<sizeof($viewerList); $i++) {
			//$sql .= "(".$eventID.", '".$viewerList[$i]."', 'W'), ";
			if($cnt == 0){
				$sql .= "('".$eventID."', '".$viewerList[$i]."', '".($viewerList[$i]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$i]==$_SESSION['UserID']?'A':'R')."','".($InviteStatus)."')";
				$cnt++;
			} else {
				$sql .= ", ('".$eventID."', '".$viewerList[$i]."', '".($viewerList[$i]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$i]==$_SESSION['UserID']?'A':'R')."','".($InviteStatus)."')";
			}
		}
		$sql = rtrim($sql, ", ");
		$result['insert_event_guest'] = $iCal->db_db_query($sql);
		
		if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
			$title_extra_info = $iCal->getEventTitleExtraInfo($eventID);
			$iCal->updateEventTitleWithExtraInfo($eventID, $iCal->Get_Request($title), $title_extra_info);
		}
	}
	
	#other school guest
	/*if (!empty($otherSchoolViewer)){
		$fieldname = "EventID, UserID, Status, InviteStatus, ToSchoolCode";
		$fieldname2 = "EventID, UserID, Status, InviteStatus, FromSchoolCode";
		$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) ";
		$cnt = 0;
		for ($i = 0; $i < count($otherSchoolViewer); $i++){
			
			if($i == 0){
				$sql .= "SELECT ".$eventID.", '".$otherSchoolViewer[$i]["userID"]."', 'W', '".$InviteStatus."', '".$otherSchoolViewer[$i]["schoolCode"]."'";
			} else {
				$sql .= " UNION ALL SELECT ".$eventID.", '".$otherSchoolViewer[$i]["userID"]."', 'W', '".$InviteStatus."', '".$otherSchoolViewer[$i]["schoolCode"]."'";
			}
			
			$dbName = $iCal->get_corresponding_dbName($otherSchoolViewer[$i]["schoolCode"]);
			$sql2 = "INSERT INTO ".$dbName.".dbo.CALENDAR_EVENT_USER ($fieldname2) ";
			$sql2 .= "values ('".$eventID."','".$otherSchoolViewer[$i]["userID"]."','W', '".$InviteStatus."','".$_SESSION['SchoolCode']."')";
			$result['insert_event_guest'] = $db->db_db_query($sql2);
		}
		$sql = rtrim($sql, ", ");
		$result['insert_event_guest'] = $iCal->db_db_query($sql);
	}*/
	
	
	
	
	/*
	if (isset($viewerClassList) && sizeof($viewerClassList) != 0) {
		$fieldname  = "EventID, UserID, GroupID, GroupType, Status, InviteStatus";
		//$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
		$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) ";
		$cnt = 0;
		for ($i=0; $i<sizeof($viewerClassList); $i++) {
			//$sql .= "($eventID, ".$viewerClassList[$i].", ".$viewerInClass[$viewerClassList[$i]].", 'G', 'W'), ";
			if($cnt == 0){
				$sql .= "SELECT $eventID, ".$viewerClassList[$i].", ".$viewerInClass[$viewerClassList[$i]].", 'G', 'W', ".$InviteStatus."";
				$cnt++;
			} else {
				$sql .= " UNION ALL SELECT $eventID, ".$viewerClassList[$i].", ".$viewerInClass[$viewerClassList[$i]].", 'G', 'W', ".$InviteStatus."";
			}
		}
		$sql = rtrim($sql, ", ");
		$iCal->db_db_query($sql);
	}
	
	if (isset($viewerCourseList) && sizeof($viewerCourseList) != 0) {
		$fieldname  = "EventID, UserID, GroupID, GroupType, Status";
		//$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
		$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) ";
		$cnt = 0;
		for ($i=0; $i<sizeof($viewerCourseList); $i++) {
			//$sql .= "($eventID, ".$viewerCourseList[$i].", ".$viewerInCourse[$viewerCourseList[$i]].", 'C', 'W'), ";
			if($cnt == 0){
				$sql .= "SELECT $eventID, ".$viewerCourseList[$i].", ".$viewerInCourse[$viewerCourseList[$i]].", 'C', 'W', ".$InviteStatus."";
				$cnt++;
			} else {
				$sql .= " UNION ALL SELECT $eventID, ".$viewerCourseList[$i].", ".$viewerInCourse[$viewerCourseList[$i]].", 'C', 'W', ".$InviteStatus."";
			}
		}
		$sql = rtrim($sql, ", ");
		$iCal->db_db_query($sql);
	}
	
	if (isset($viewerGroupList) && sizeof($viewerGroupList) != 0) {
		$fieldname  = "EventID, UserID, GroupID, GroupType, Status";
		//$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
		$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) ";
		$cnt = 0;
		for ($i=0; $i<sizeof($viewerGroupList); $i++) {
			//$sql .= "($eventID, ".$viewerGroupList[$i].", ".$viewerInGroup[$viewerGroupList[$i]].", 'E', 'W'), ";
			if($cnt == 0){
				$sql .= "SELECT $eventID, ".$viewerGroupList[$i].", ".$viewerInGroup[$viewerGroupList[$i]].", 'E', 'W', ".$InviteStatus."";
				$cnt++;
			} else {
				$sql .= " UNION ALL SELECT $eventID, ".$viewerGroupList[$i].", ".$viewerInGroup[$viewerGroupList[$i]].", 'E', 'W', ".$InviteStatus."";
			}
		}
		$sql = rtrim($sql, ", ");
		$iCal->db_db_query($sql);
	}
	*/
} else {
	$fieldname1  = "EventID, UserID, Status, Access, InviteStatus";
	$sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) VALUES ";
	// $sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) ";
	$cnt1 = 0;
	$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
	for ($i=0; $i<sizeof($repeatSeries); $i++) {
		$allEventIdAry[] = $repeatSeries[$i]["EventID"];
		// insert the owner of the event with "A"(Full) access right
		//$sql1 .= "(".$repeatSeries[$i]["EventID"].", ".$_SESSION['UserID'].", 'A', 'A'), ";
		if($cnt1 == 0){
			$sql1 .= " ('".$repeatSeries[$i]["EventID"]."', '".$_SESSION['UserID']."', 'A', 'A', '".$InviteStatus."')";
			$cnt1++;
		} else {
			$sql1 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$_SESSION['UserID']."', 'A', 'A', '".$InviteStatus."')";
		}		
		
		if (isset($viewerList) && sizeof($viewerList) != 0) {
			$fieldname2  = "EventID, UserID, Status, Access, InviteStatus";
			$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
			// $sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) ";
			$cnt2 = 0;
			for ($j=0; $j<sizeof($viewerList); $j++) {
				//$sql2 .= "(".$repeatSeries[$i]["EventID"].", '".$viewerList[$j]."', 'W'), ";
				if($cnt2 == 0){
					$sql2 .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."','".($InviteStatus)."')";
					$cnt2++;
				} else {
					$sql2 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."','".($InviteStatus)."')";
				}
			}
			$sql2 = rtrim($sql2, ", ");
			$result['insert_event_guest'] = $iCal->db_db_query($sql2);
		}
		
		
		#other school guest
		/*if (!empty($otherSchoolViewer)){
			$fieldname = "EventID, UserID, Status, InviteStatus, ToSchoolCode";
			$fieldname2 = "EventID, UserID, Status, InviteStatus, FromSchoolCode";
			$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) ";
			$cnt = 0;
			for ($j = 0; $j < count($otherSchoolViewer); $j++){
				
				if($j == 0){
					$sql .= "SELECT ".$repeatSeries[$i]["EventID"].", '".$otherSchoolViewer[$j]["userID"]."', 'W', '".$InviteStatus."', '".$otherSchoolViewer[$j]["schoolCode"]."'";
				} else {
					$sql .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", '".$otherSchoolViewer[$j]["userID"]."', 'W', '".$InviteStatus."', '".$otherSchoolViewer[$j]["schoolCode"]."'";
				}
				
				$dbName = $iCal->get_corresponding_dbName($otherSchoolViewer[$j]["schoolCode"]);
				$sql2 = "INSERT INTO ".$dbName.".dbo.CALENDAR_EVENT_USER ($fieldname2) ";
				$sql2 .= "values ('".$repeatSeries[$i]["EventID"]."','".$otherSchoolViewer[$j]["userID"]."','".$InviteStatus."','".$_SESSION['SchoolCode']."')";
				$result['insert_event_guest'.$i.'-'.$j] = $db->db_db_query($sql2);
			}
			$sql = rtrim($sql, ", ");
			$result['insert_event_guest'] = $iCal->db_db_query($sql);
		}*/
		
		
		/*
		if (isset($viewerClassList) && sizeof($viewerClassList) != 0) {
			$fieldname2  = "EventID, UserID, GroupID, GroupType, Status, InviteStatus";
			//$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
			$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) ";
			$cnt2 = 0;
			for ($j=0; $j<sizeof($viewerClassList); $j++) {
				//$sql2 .= "(".$repeatSeries[$i]["EventID"].", ".$viewerClassList[$j].", ".$viewerInClass[$viewerClassList[$i]].", 'G', 'W'), ";
				if($cnt2 == 0){
					$sql2 .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerClassList[$j].", ".$viewerInClass[$viewerClassList[$i]].", 'G', 'W', ".$InviteStatus."";
					$cnt2++;
				} else {
					$sql2 .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerClassList[$j].", ".$viewerInClass[$viewerClassList[$i]].", 'G', 'W', ".$InviteStatus."";
				}
			}
			$sql2 = rtrim($sql2, ", ");
			$iCal->db_db_query($sql2);
		}
		
		if (isset($viewerCourseList) && sizeof($viewerCourseList) != 0) {
			$fieldname2  = "EventID, UserID, GroupID, GroupType, Status, InviteStatus";
			//$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
			$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) ";
			$cnt2 = 0;
			for ($j=0; $j<sizeof($viewerCourseList); $j++) {
				//$sql2 .= "(".$repeatSeries[$i]["EventID"].", ".$viewerCourseList[$j].", ".$viewerInCourse[$viewerCourseList[$i]].", 'C', 'W'), ";
				if($cnt2 == 0){
					$sql2 .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerCourseList[$j].", ".$viewerInCourse[$viewerCourseList[$i]].", 'C', 'W', ".$InviteStatus."";
					$cnt2++;
				} else {
					$sql2 .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerCourseList[$j].", ".$viewerInCourse[$viewerCourseList[$i]].", 'C', 'W', ".$InviteStatus."";
				}
			}
			$sql2 = rtrim($sql2, ", ");
			$iCal->db_db_query($sql2);
		}
		
		if (isset($viewerGroupList) && sizeof($viewerGroupList) != 0) {
			$fieldname2  = "EventID, UserID, GroupID, GroupType, Status, InviteStatus";
			//$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
			$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) ";
			$cnt2 = 0;
			for ($j=0; $j<sizeof($viewerGroupList); $j++) {
				//$sql2 .= "(".$repeatSeries[$i]["EventID"].", ".$viewerGroupList[$j].", ".$viewerInGroup[$viewerGroupList[$i]].", 'E', 'W'), ";
				if($cnt2 == 0){
					$sql2 .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerGroupList[$j].", ".$viewerInGroup[$viewerGroupList[$i]].", 'E', 'W', ".$InviteStatus."";
					$cnt2++;
				} else {
					$sql2 .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerGroupList[$j].", ".$viewerInGroup[$viewerGroupList[$i]].", 'E', 'W', ".$InviteStatus."";
				}
			}
			$sql2 = rtrim($sql2, ", ");
			$iCal->db_db_query($sql2);
		}
		*/
	}
	$sql1 = rtrim($sql1, ", ");
//	$result['insert_event_owner'] = $iCal->db_db_query($sql1);
	
	if($sys_custom['iCalendar']['EventTitleWithParticipantNames'])
	{
		for ($i=0; $i<sizeof($repeatSeries); $i++) {
			$title_extra_info = $iCal->getEventTitleExtraInfo($repeatSeries[$i]["EventID"]);
			$iCal->updateEventTitleWithExtraInfo($repeatSeries[$i]["EventID"], $iCal->Get_Request($title), $title_extra_info);
		}
	}
}

######################################## END UPDATE ########################################
// debug_r($result);


$eventTimeStamp = $iCal->sqlDatetimeToTimestamp($eventDate);
if(!in_array(false, $result)){
	$Msg = $Lang['ReturnMsg']['EventCreateSuccess'];
} else {
	$Msg = $Lang['ReturnMsg']['EventCreateUnSuccess'];
}

if($plugin['eClassTeacherApp'] && $submitMode == 'submitAndSendPushMsg' && count($allEventIdAry)>0){
	$iCal->sendPushMessageNotifyParticipants($allEventIdAry[0]);
}

intranet_closedb();

if ($submitMode == "saveNsend"){
	if($plugin['imail_gamma'])
		header("Location: ../imail_gamma/compose_email.php?eventID=".urlencode($eventID).'&isOptional='.urlencode($InviteStatus));
	else
		header("Location: ../imail/compose.php?eventID=".urlencode($eventID).'&isOptional='.urlencode($InviteStatus));
}
elseif (!isset($mode) || $mode != "quick")
	header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&msgType=event&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&Msg=$Msg");

exit;
//header("Location: new_event.php?msg=2&editEventID=event".$eventID);
?>