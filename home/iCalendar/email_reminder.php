#!/usr/bin/php -q
<?php
$PATH_WRT_ROOT = "../../";
include_once("cli_compatibility.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$isTest = true;

$name_field = getNameFieldByLang("c.");

$sql  = "SELECT a.ReminderID, a.ReminderDate, b.Title, b.EventDate, ";
$sql .= "$name_field AS Name, c.UserEmail ";
$sql .= "FROM CALENDAR_REMINDER AS a, CALENDAR_EVENT_ENTRY AS b, INTRANET_USER AS c ";
$sql .= "WHERE a.EventID=b.EventID AND a.UserID=c.UserID AND a.ReminderType = 'E' ";
$sql .= "AND UNIX_TIMESTAMP() > UNIX_TIMESTAMP(a.ReminderDate) ";
$sql .= "AND a.LastSent IS NULL ";
#$sql .= "AND c.UserEmail IS NOT NULL ";
$sql .= "ORDER BY a.ReminderDate";

$result = $li->returnArray($sql,3);

$eol = "\r\n";
$sender = "eClass iCalendar";
$fromaddress = "andychan@broadlearning.com";

// Mail Header
$headers  = "From: \"$sender\"<$fromaddress>".$eol;
$headers .= "Reply-To: \"$sender\"<$fromaddress>".$eol;
$headers .= "Return-Path: \"$sender\"<$fromaddress>".$eol;
$headers .= "Message-ID: <".time()."-$fromaddress>".$eol;
$headers .= "X-Mailer: PHP v".phpversion();


$result[0] = array(
							"UserEmail"=>"andychan@broadlearning.com",
							"Title"=>"Test Email Reminder",
							"Name"=>"Andy Chan",
							"EventDate"=>"2008-08-27 20:12:12",
							"ReminderID"=>"157"
						);


if (sizeof($result) > 0) {
	ini_set('sendmail_from',$fromaddress);
	for($i=0; $i<sizeof($result); $i++) {
		$to = $result[$i]["UserEmail"];
		if (trim($to) != "") {
			// Subject
			$title = $result[$i]["Title"];
			$date = date("D, M j, g:i", $result[$i]["EventDate"]);
			$subject = "[Reminder] $title @ $date";
			
			// Message body
			$name = $result[$i]["Name"];
			$body = "$name, this is a reminder for \"$title\" issued by iCalendar of eClass";
			if (!$isTest) {
				$success = mail($to, $subject, $body, $headers);
			} else {
				$success = true;
			}
			
			if ($success) {
				echo "Mail to $name <$to> delivered!\n";
				$sql  = "UPDATE CALENDAR_REMINDER SET LastSent = '".date("Y-m-d H:i:s")."' ";
				$sql .= "WHERE ReminderID = ".$result[$i]["ReminderID"];
				$success = $li->db_db_query($sql);
				if ($success)
					echo "Database updated.\n";
			} else {
				echo "Mail to $name <$to> failed!\n";
			}
		}
	}
	ini_restore('sendmail_from');
} else {
	echo "No reminder at this time: ".date("Y-m-d H:i:s")."\n";
}

intranet_closedb();

?>