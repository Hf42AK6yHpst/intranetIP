<?php
// page modifing by: 
/*
 * 2019-06-06 Carlos: Added CSRF token.
 */
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
intranet_auth();
intranet_opendb();

##################################################################################################################

## Use Library
$iCal = new icalendar();
$linterface = new interface_html();
$lui = new ESF_ui();
## Get Data 
if (!isset($time))
	$time = $_SESSION["iCalDisplayTime"];
	
		
## Initialization 
$calArr = array();


## Prepartion
if ($clear == 1) {
	$now = time();
	$sql  = "SELECT COUNT(*) FROM TEMP_CALENDAR_EVENT_ENTRY ";
	$sql .= "WHERE SID!='".session_id()."' OR UNIX_TIMESTAMP()-UNIX_TIMESTAMP(ImportDate) < 86400";
	$temp = $iCal->returnVector($sql);
	$count = $temp[0];
	
	if ($count > 0) {
		$sql ="TRUNCATE TABLE TEMP_CALENDAR_EVENT_ENTRY";
		$iCal->db_db_query($sql);
	}
}

$sql = "CREATE TABLE IF NOT EXISTS TEMP_CALENDAR_EVENT_ENTRY (
	SID varchar(64),
	Title varchar(255),
	EventDate datetime,
	ImportDate datetime,
	Duration int(8),
	IsAllDay char(1),
	Description text,
	Location varchar(255), 
	Private char(1) 
)";
$iCal->db_db_query($sql);

# Get Own Calendar
$ownCalendar = $iCal->returnViewableCalendar('', '', true);


# Button
$button  = $lui->Get_Div_Open("form_btn");
$button .= $lui->Get_Input_Submit("Submit", "Submit", $button_submit, "class=\"button_main_act\"");
$button .= '&nbsp;';
$button .= $lui->Get_Reset_Button("Reset", "Reset", $button_reset, "class=\"button_main_act\"");
$button .= '&nbsp;';
$button .= $lui->Get_Reset_Button("Cancel", "Cancel", $button_cancel, "class=\"button_main_act\" onclick=\"window.location='manage_calendar.php'\"");
$button .= $lui->Get_Div_Close();


	
# Main
$CalLabeling  = "<div id='sub_page_title' style='width:50%'>$i_Calendar_ImportCalendar</div>";
$CalLabeling .= "<div id='btn_link' style='float:right;clear:none'><a href='index.php'>$iCalendar_BackTo_Calendar</a></div>";

$CalMain = '
	<div id="cal_board">
	  <span class="cal_board_02"></span> 
	  <div class="cal_board_content">
	    <div id="calendar_wrapper" style="float:left;background-color:#dbedff">
	  	  <div id="cal_manage_calendar">
	  	    <form name="form1" enctype="multipart/form-data" action="import_confirm.php" method="POST">';
	  	  
# Content
// Input File
$CalMain .= '<p class="cssform">';
$CalMain .= '<label for="'.$iCalendar_ImportCalendarEvent_SelectFile.'">'.$iCalendar_ImportCalendarEvent_SelectFile.'</label>';
$CalMain .= '<span class="imail_form_indside_choice"><input type="file" name="userfile" class="textbox"></span>';
$CalMain .= '</p>';
$CalMain .= '<br style="clear:both">';
$CalMain .= '<br style="clear:both">';
// Format & Download Sample File 
$CalMain .= '<p class="cssform">';
$CalMain .= '<label for="'.$iCalendar_ImportCalendarEvent_Format.'">'.$iCalendar_ImportCalendarEvent_Format.'</label>';
$CalMain .= '<span class="imail_form_indside_choice">'.$i_Calendar_ImportCalendar_Instruction1;
$CalMain .= '<a href="get_sample_csv.php?file=sample_calendar.csv"><img src="'.$ImagePathAbs.'xls.gif" border="0" align="absmiddle">&nbsp;'.$iCalendar_ImportCalendarEvent_DownloadSample.'</a>';
$CalMain .= '</span>';
$CalMain .= '</p>';
$CalMain .= '<br style="clear:both">';
$CalMain .= '<br style="clear:both">';
// Choose Calendar
$CalMain .= '<p class="cssform">';
$CalMain .= '<label for="'.$i_Calendar_ImportCalendar_ChooseCalendar.'">'.$i_Calendar_ImportCalendar_ChooseCalendar.'</label>';
$CalMain .= '<span class="imail_form_indside_choice">';
if (sizeof($ownCalendar) == 1) {
	$CalMain .= $ownCalendar[0]["Name"];
	$CalMain .= '<input type="hidden" name="calID" id="calID" value="'.$ownCalendar[0]["CalID"].'" />';
} else {
	for ($i=0; $i<sizeof($ownCalendar); $i++) {
		$calArr[$i] = array($ownCalendar[$i]["CalID"], $ownCalendar[$i]["Name"]);
	}
	$CalMain .= $linterface->GET_SELECTION_BOX($calArr, 'name="calID" id="calID"', "", $calID);
}
$CalMain .= '</span>';
$CalMain .= '</p>';
$CalMain .= '<br style="clear:both">';
$CalMain .= '<br style="clear:both">';
// Button
$CalMain .= '<p class="dotline_p">'.$button.'</p>';
$CalMain .= csrfTokenHtml(generateCsrfToken());
$CalMain .= '
			</form>
		  </div>
		</div>
	  </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>';

# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain, '','','','','',false,false);


# Layout
$customLeftMenu = $iCal->printLeftMenu();
$MODULE_OBJ["title"] = $iCalendar_Main_Title;
$MODULE_OBJ["logo"] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_icalendar.gif";

$TAGS_OBJ[] = array("<div style='margin:42px 0px -15px 10px;'>$topNavigationMenu</div>", "", 0);

?>
<!--
<link href="/theme/css/common.css" rel="stylesheet" type="text/css">
<link href="/theme/css/common_cammy.css" rel="stylesheet" type="text/css">
<link href="/theme/css/layout_grey.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_size_normal.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_face_verdana.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_color_layout_grey.css" rel="stylesheet" type="text/css">
	-->
<style type="text/css">@import url(css/main.css);</style>
<style type="text/css">
#deleteCalendarWarning {
	font-family:"Arial","Helvetica","sans-serif";
}
</style>
<?php
// $linterface->LAYOUT_START();
include_once("headerInclude.php");
echo '<script>';
echo 'var SessionExpiredWarning = "' . $Lang['Warning']['SessionExpired'] . '";';
echo 'var SessionKickedWarning = "' . $Lang['Warning']['SessionKicked'] . '";';
echo 'var SessionExpiredMsg = "' . $SYS_CONFIG['SystemMsg']['SessionExpired'] . '";';
echo 'var SessionKickedMsg = "' . $SYS_CONFIG['SystemMsg']['SessionKicked'] . '";';
echo '</script>';

?>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<!--<script type="text/javascript" src="js/jquery.blockUI.js"></script>-->
<script type="text/javascript" src="js/interface.js"></script>
<script type="text/javascript" src="js/dimensions.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>

<script language="javascript">
function SessionExpired(data)
{
	return false;
}

$(document).ready( function() {
	
});

function toggleInvolveEventVisibility(){
	$(".involve").toggle();
}

function toggleSchoolEventVisibility() {
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
}

function toggleCalVisibility(parClassID) {
	$('.cal-'+parClassID).toggle();
}

function toggleOwnDefaultCalVisibility(parClassID){
	toggleCalVisibility(parClassID);
	
	var obj = document.getElementById("toggleCalVisible-"+parClassID);
	var involObj = document.getElementById("toggleVisible");
	if(obj.checked == true){
		if(involObj.checked == false)
			toggleInvolveEventVisibility();
		involObj.checked = true;
	} else {
		if(involObj.checked == true)
			toggleInvolveEventVisibility();
		involObj.checked = false;
	}	
}
function toggleExternalCalVisibility(selfName,eventClass,CalType){
	//toggleCalVisibility(eventClass,selfName);
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calType : CalType		
	}, function(responseText) {
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	// isDisplayMoreFunction();
	// jHide_All_Layers();
}
<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>

<div id="module_bulletin" class="module_content">
<?=$lui->Get_Sub_Function_Header("iCalendar",$Msg);?>
<?=$display;?>
</div>
<br>

<?php
// $linterface->LAYOUT_STOP();
intranet_closedb();
include_once("footerInclude.php");
?>
