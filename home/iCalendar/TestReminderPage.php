<?php
//page modifying by: Carlos Leung
$PathRelative = "../../../";
$CurSubFunction = "iCalendar";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");
include_once($PathRelative."src/include/class/UserInterface.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

Global $Lang, $SYS_CONFIG;

include_once($PathRelative."src/include/template/general_header.php");
$ui = new UserInterface();
$UserID = $_SESSION["SSV_USERID"];

echo $ui->Get_Sub_Function_Header("Reminder", $Msg);
//echo $ui->Get_Div_Open('Reminder','style="background-color:yellow;position:absolute; border-style:solid;border-width:2px;"');
//echo $ui->Get_Div_Close();
//echo $UserID;
//echo '<script type="text/javascript" language="javascript" >var EventReminderPath="'.$PathRelative.'"; EventReminder();</script>';
echo '<script type="text/javascript" language="javascript" >GlobalPathRelative="'.$PathRelative.'";EventReminder("'.$PathRelative.'");</script>';



include_once($PathRelative."src/include/template/general_footer.php");
?>