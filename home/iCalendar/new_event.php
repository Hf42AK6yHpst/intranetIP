<?php
// page modifing by: 
/*
 * 2020-06-04 (Tommy): fix: deleted user's record show 0 in created by row, add $Lang['iCalendar']['null_created_by']
 * 2017-06-21 (Omas):	[ ip2.5.8.7.1] fix eBooking reject icon cannot show problem, edit can book more than 1 room problem - #H118146 
 * 2017-01-24 (Carlos): [ip2.5.8.3.1] Current user would be default in participant list. Added send push msg btn. 
 * 2016-09-22 (Carlos): [ip2.5.7.10.1] Added js getCheckAvailableTimeForm(), getCheckAvailableTimeResult() for checking availability time. 
 * 2016-06-22 (Carlos): [ip2.5.7.8.1] Fixed to hide eBooking link if start date and end date are not the same date because eBooking cannot handle such span dates event, and would cause error booking records.  
 * 2015-09-11 (Carlos): [ip2.5.6.10.1] fixed edit event the minute selection selected value does not match with the event minute.
 * 2015-01-16 (Carlos): [ip2.5.6.3.1] called jShow_Insert_Into_School_Calendar() at $().ready() to init the display of [Show in school calendar] checkbox if group calendar is default selected
 * 2014-10-22 (Carlos): [ip2.5.5.10.1] Cater the display checkbox of [Show in school calendar] - shown when selected is group calendar
 * 2014-07-15 (Carlos): Changed repeat event message "last" to $Lang['iCalendar']['TheLast']
 */

$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");

intranet_auth();
intranet_opendb();

######################################################################################################################

## Use Library
$iCal = new icalendar();
$iCal_api = new icalendar_api();
$lui = new ESF_ui();
$linterface = new interface_html();
// $iCalApi = new icalendar_api();

## Get Data 
$CalType 		= (isset($CalType) && $CalType != "") ? $CalType : '';
// $isOtherCalType = $iCal->Is_Other_Calendar_Type($CalType);				# Check for other CalType like cpd, cas
//echo "CalType:".$CalType."<br />";

## Preparation
$calViewPeriodArr = array("daily","weekly","monthly","agenda");
$calViewPeriod = str_replace(array('<', '>', ';', 'javascript:', '"', '\'', '(', ')','&'),'',$calViewPeriod);
if (!isset($calViewPeriod) || !in_array($calViewPeriod, $calViewPeriodArr))
	$calViewPeriod = $iCal->systemSettings["PreferredView"];
if ($calViewPeriod == "")
	$calViewPeriod = "monthly";

if (isset($time) && is_numeric($time)) {
	$year = date('Y', $time);
	$month = date('m', $time);
	$day = date('d', $time);
} else if (!isset($year) && !isset($month)){
	$time = time();
	$year = date('Y');
	$month = date('m');
	$day = date('d');
}

# Insert a default calendar if user didn't have any calendar
if (!isset($_SESSION["iCalHaveCal"]) ||  $_SESSION["iCalHaveCal"]!= 1) {
	$iCal->insertMyCalendar();
}
$DefaultCalendar = $iCal->returnUserPref($_SESSION['UserID'], 'DefaultCalendar');
# Record the display view in session during each visit to this page
$_SESSION["iCalDisplayPeriod"] = $calViewPeriod;
$_SESSION["iCalDisplayTime"] = $time;



## Initialization
$editMode = false;
$access = "P";
$InviteStatus = 'NULL';		// InviteType: 0-Compulsory , 1-Optional, NULL: not yet defined


## Preparation
# Event date and time passed from other page
if (isset($calDate)) {
	$calDatePiece = explode("-",$calDate);
	// Bug Fix (17/12/2007): checkdate() only accept LONG (=INT), type cast the variable
	if (checkdate((int)$calDatePiece[1],(int)$calDatePiece[2],(int)$calDatePiece[0])) {
		$eventDate = $calDatePiece[0]."-".$calDatePiece[1]."-".$calDatePiece[2];
		
		# Set TimeStamp of Date when the time is passed from other page
		$_SESSION["iCalDisplayTime"] = strtotime($eventDate);
	}
	if (is_numeric($calDatePiece[3]) && $calDatePiece[3]>=0 && $calDatePiece[3]<=23) {
		$eventTime = $calDatePiece[3].":00";
		$eventHr = $calDatePiece[3];
		$eventMin = 0;
		$durationHr = 0;
		$durationMin = 0;
	}
}

# Get Current School DB Name
$current_dbname = $iCal->db;
// $central_db = new database(false, false, true);

# Check whether this is Foundation Calendar with Non-ESF School Site & Get Centralized Site DB Name
// $isFoundationCalNonESF = ($CalType == 2 && ($_SESSION['SchoolCode'] != 'ESFC' || $_SESSION['SchoolCode'] != 'DEV')) ? true : false;
// if($isFoundationCalNonESF){
	// $db = new database(false, false, true);
	// $central_dbname = $db->db;
// } else {
	// $central_dbname = $iCal->db;
// }

$isEditEvent = false;
## Get Event Details 
if(isset($editEventID))	
{	//echo "editEventID=".$editEventID."<br />";
	/*$isCalType4 = false;
	if ($CalType == 4){
		$isCalType4 = true;
		$eventID = substr($editEventID, 5);
		$editMode = true;
		$pieces = split("-",$eventID);
		$eventID = $pieces[0]; 
		$otherSchoolCode = $pieces[1];
		$result = $iCalApi->Get_Specific_Invitation_Event_Info($eventID, $otherSchoolCode);
		$title = $result[0]["Title"];
		$temp = trim($result[0]["Location"]);
		$location = empty($temp)?"-":$result[0]["Location"];
		$temp = trim($result[0]["Description"]);
		$description = empty($temp)?"-":$result[0]["Description"];
		$createdBy = $result[0]["UserID"];
		$eventTs = strtotime($result[0]["EventDate"]);
		$duration = $result[0]["Duration"];
		if ($result[0]["IsAllDay"] == 1 && $result[0]["Duration"] != 0){
			$duration = $duration-1440;
		}
		$endTs = strtotime("+{$duration} minute", $eventTs);
		$PersonalNote = UTF8_From_DB_Handler($result[0]["PersonalNote"]);
		$url = trim($result[0]["Url"]);
		$eventDateTime = $result[0]["EventDate"];
		$calAccess = "";
	}*/
	if ($CalType == 1 || $CalType > 3){ # external event
		$eventID = substr($editEventID, 5);
		$editMode = true;		
		$result = $iCal_api->getExternalEventDetail($CalType,$eventID);
		$isAllDay = !isset($result[0]["IsAllDay"])?true:($result[0]["IsAllDay"]==1?true:false);		
		$extEvent = $result;
		$title = trim(htmlspecialchars(intranet_undo_htmlspecialchars($result[0]["Title"])));
		$temp = trim(htmlspecialchars(intranet_undo_htmlspecialchars($result[0]["Location"])));
		$location = empty($temp)?"-":$result[0]["Location"];
		$temp = trim(htmlspecialchars(intranet_undo_htmlspecialchars($result[0]["Description"])));
		$description = empty($temp)?"-":$result[0]["Description"];
		$createdBy = $result[0]["UserID"];
		$eventTs = strtotime($result[0]["EventDate"]);
		$duration = $result[0]["Duration"];
		if ($result[0]["IsAllDay"] == 1 && $result[0]["Duration"] != 0){
			$duration = $duration-1440;
		}
		$endTs = strtotime("+{$duration} minute", $eventTs);
		$PersonalNote = trim(htmlspecialchars(intranet_undo_htmlspecialchars($result[0]["PersonalNote"])));
		$url = trim($result[0]["Url"]);
		$eventDateTime = $result[0]["EventDate"];
		$calAccess = "";
	}
	else if (substr($editEventID,0,5) == "event" && is_numeric(substr($editEventID, 5))) {
		$eventID = substr($editEventID, 5);
		$editMode = true;
	
	/*	if($isOtherCalType)		# get event detail from non-real record of CALENDAR_EVENT_ENTRY, but from other Module
		{
			if($CalType == ENROL_CPD_CALENDAR)		# CPD
			{
				$url_text = $iCalendar_OtherCalType_ViewDetails;
				$programmeID = (isset($programmeID) && $programmeID != "") ? $programmeID : "";
				
				$result = $iCalApi->Get_CPD_Enrolment_Programme_Info($eventID, $programmeID);
				if(count($result) > 0){
					if($result[0]['IsMyProgramme'] == 1){
						$page = "staff_view_enrol_detail.php?ProgrammeID=$programmeID";
					} else {
						if($result[0]['IsExpriedEnrolDate']){
							$page = "staff_view_detail.php?ProgrammeID=$programmeID"; 
						} else {
							$TbRowID = "Tr$programmeID";
							$page  = "enrolment_index.php?SortField=ProgrammeCode&SortOrder=1&ddlPageNo=1";
							$page .= "&ProgrammeID=$programmeID#$TbRowID";
							$url_text = $iCalendar_OtherCalType_EnrolNow;
						}
					}
					$link = $PathRelative."src/module/cpd/$page";
				}
			}
			//else if($CalType == ENROL_CAS_CALENDAR)	# CAS
			//{
			//}
		} else { */				# if editing event, retrieve event detail from real records of CALENDAR_EVENT_ENTRY
			# Get Basic Event Info
			
			
			//$fieldname  = "a.EventID, a.UserID, a.CalID, a.RepeatID, Date_Format(a.EventDate, '%Y-%m-%d %T') AS EventDate, ";
			$fieldname  = "a.EventID, a.UserID, a.CalID, a.RepeatID, Date_Format(a.EventDate, '%Y-%m-%d %H:%i:%s') AS EventDate, ";
			$fieldname .= "a.Title, a.Description, a.Duration, a.IsImportant, a.IsAllDay, a.Access, a.Location, a.Url, ";
			$fieldname .= "pn.PersonalNote ";
			
			$sql = "SELECT  $fieldname 
					FROM CALENDAR_EVENT_ENTRY AS a  
					LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE AS pn ON 
						a.EventID = pn.EventID and pn.UserID = '".$_SESSION['UserID']."' ";
			//$sql .= ($isFoundationCalNonESF) ? "AND pn.CalType = 2 " : " AND pn.CalType <> 3 AND pn.CalType <> 4 ";
			$sql .= "WHERE a.EventID='$eventID'";
			// echo $sql;
			// exit;
			// if($isFoundationCalNonESF){
				// $result = $db->returnArray($sql);
			// } else {
				$result = $iCal->returnArray($sql);
			//}
		/*	if (empty($CalType)){
				$sql = "select CalType from CALENDAR_CALENDAR where CalID =".$result[0]['CalID'];
				$r = $iCal->returnVector($sql);
				$CalType = $r[0];
			}*/
			 
		//}
		
		# Get Repeat Event Details
		if (sizeof($result) > 0) {
			$isEditEvent = true;
			$repeatID = $result[0]["RepeatID"];
			$calID = $result[0]["CalID"];
			if ($repeatID != NULL) {
				$isRepeatEvent = true;
			} else {
				$isRepeatEvent = false;
			}
			
			$title = trim(htmlspecialchars(intranet_undo_htmlspecialchars($result[0]["Title"])));
			//echo $result[0]["Title"];
			
			$description = trim(htmlspecialchars(intranet_undo_htmlspecialchars($result[0]["Description"])));
			$location = trim(htmlspecialchars(intranet_undo_htmlspecialchars($result[0]["Location"])));
			
			$url = ($link != '') ? $link : $result[0]["Url"];
			$eventDateTime = $result[0]["EventDate"];
			$eventTs = $iCal->sqlDatetimeToTimestamp($eventDateTime);
			//$endTs = $eventTs + $result[0]["Duration"]*60;
			$eventDateTimePiece = explode(" ", $eventDateTime);
			$eventDate = $eventDateTimePiece[0];
			
			# Set TimeStamp of Date whose event is editing
			$_SESSION["iCalDisplayTime"] = strtotime($eventDate);
			$duration = (int)$result[0]["Duration"];
			if ($result[0]["IsAllDay"] == 0) {
				$eventTime = $eventDateTimePiece[1];
				$eventTimePiece = explode(":", $eventTime);
				$eventHr = $eventTimePiece[0];
				$eventMin = (int)$eventTimePiece[1];
				//$durationHr = (int)($duration/60);
				//$durationMin = $duration-$durationHr*60;
			}
			else
				$duration = $duration - 1440;
			
			if ($duration < 0)
				$duration = 0; 
			
			$eventEndTs = strtotime("+".$duration." minute", $eventTs);
			$endDateTime = date("Y-m-d", $eventEndTs);
			$endTs = $eventEndTs;
			//echo "StartTs: $startEvent , endTs: $eventEndTs , duration: $duration <br><br>";
			//echo "end Date: $endDateTime <br><br>";
			$eventEndHr = date("H", $eventEndTs);
					
			$eventEndMin = (int)date("i", $eventEndTs);
			//$eventMin = date("j", $eventTs);
			
			$isImportant = trim($result[0]["IsImportant"]);
			$access = trim($result[0]["Access"]);
			$createdBy = $result[0]["UserID"];
			$PersonalNote = trim(htmlspecialchars(intranet_undo_htmlspecialchars($result[0]["PersonalNote"])));
	
			// Repeat Pattern
			if($isRepeatEvent) {
				# 1st event date in the recurrence series
				$FirstRepeatEventDateTime = $iCal->returnFirstEventDate($repeatID);
				$FirstRepeatEventDateTimePiece = explode(" ", $FirstRepeatEventDateTime);
				$FirstRepeatEventDate = $FirstRepeatEventDateTimePiece[0];	
				
				$fieldname  = "RepeatID, RepeatType, Date_Format(EndDate,'%Y-%m-%d %T') AS EndDate, Frequency, Detail ";
				
				$sql  = "SELECT $fieldname FROM CALENDAR_EVENT_ENTRY_REPEAT ";
				$sql .= "WHERE RepeatID='".$repeatID."' ";
				
				if($isFoundationCalNonESF){
					$result = $db->returnArray($sql);
				} else {
					$result = $iCal->returnArray($sql);
				}
				
				if (sizeof($result) > 0) {
					$repeatEndDateTime = $result[0]["EndDate"];
					$repeatEndPiece = explode(" ", $repeatEndDateTime);
					$repeatEndDate = $repeatEndPiece[0];
					$repeatType = $result[0]["RepeatType"];
					$repeatPattern = $repeatType."#".$repeatEndDateTime."#".$result[0]["Frequency"]."#".$result[0]["Detail"];
					
					$frequency = 0;
					switch($repeatType) {
						case "DAILY":
							$repeatDailyDay = $result[0]["Frequency"];
							$frequency = $repeatDailyDay;
							break;
						case "WEEKDAY":
						case "MONWEDFRI":
						case "TUESTHUR":
							break;
						case "WEEKLY":
							$repeatWeeklyRange = $result[0]["Frequency"];
							$frequency = $repeatWeeklyRange;
							$detail = $result[0]["Detail"];
							break;
						case "MONTHLY":
							$repeatMonthlyRange = $result[0]["Frequency"];
							$frequency = $repeatMonthlyRange;
							$detail = $result[0]["Detail"];
							break;
						case "YEARLY":
							$repeatYearlyRange = $result[0]["Frequency"];
							$frequency = $repeatYearlyRange;
							break;
					}
				}
			}
			/*
			// Reminder Info
			$sql  = "SELECT a.* FROM CALENDAR_REMINDER AS a ";
			$sql .= "WHERE a.EventID=$eventID AND a.UserID=".$UserID;
			$result = $iCal->returnArray($sql);
			echo "<PRE>";echo print_r($result);echo "</PRE>";
			echo "eventid=".$eventID."<br />".$UserID."<br />";
			if (sizeof($result) > 0) {
				for ($i=0; $i<sizeof($result); $i++) {
					$reminderID[$i] = $result[$i]["ReminderID"];
					$reminderType[$i] = $result[$i]["ReminderType"];
					$reminderDate[$i] = $result[$i]["ReminderDate"];
					$lastSent[$i] = $result[$i]["LastSent"];
					$timeSent[$i] = $result[$i]["TimeSent"];
				}
			}
			*/
			# Guest Info
		/*	if($CalType == 2 || $isOtherCalType){
				# Foundation Calendar Event does not invite guest temporarily
				$result = array();
				$userAccess = 'R';
			} else {*/
				$name_field = getNameFieldWithClassNumberByLang("b.");
				
				#local school guest
				//$name_field = "OfficialFullName";
				$sql  = "SELECT a.*, $name_field AS Name ";
				$sql .= "FROM CALENDAR_EVENT_USER AS a, INTRANET_USER AS b ";
				$sql .= "WHERE a.UserID=b.UserID AND a.EventID='$eventID' group by UserID";
				$result = $iCal->returnArray($sql);
				
								
				#other school guest
				// $sql = "select * from CALENDAR_EVENT_USER where EventID=$eventID AND FromSchoolCode is null AND ToSchoolCode is not null";
				// $otherSchoolGuest = $iCal->returnArray($sql);
				// foreach ($otherSchoolGuest as $g){
					// $dbName = $iCal->get_corresponding_dbName($g["ToSchoolCode"]);
					// $sql  = "SELECT a.*, $name_field AS Name ";
					// $sql .= "FROM CALENDAR_EVENT_USER AS a, ".$dbName.".dbo.INTRANET_USER AS b ";
					// $sql .= "WHERE a.UserID = b.UserID AND a.EventID=$eventID AND a.FromSchoolCode is null AND a.ToSchoolCode = '".$g["ToSchoolCode"]."' AND a.UserID = '".$g["UserID"]."'";
					// $detail = $central_db->returnArray($sql);
					// if (!empty($detail))
						// $result = array_merge($result,$detail);
				// }
			//}
			
			if (sizeof($result) > 0) {
				#if (sizeof($result) == 1 && $result[0]["UserID"] == $UserID) {
//				if (sizeof($result) == 1) {
//					$haveGuest = false;
//				} else {
//					$haveGuest = true;
//				}
				$haveGuest = true;
				$guestStatusWait = 0;
				$guestStatusYes = 0;
				$guestStatusNo = 0;
				$guestStatusMaybe = 0;
				
				$guestGroupList = array();
				for ($i=0; $i<sizeof($result); $i++) {
					$guestUserID[$i] = $result[$i]["UserID"];
					// if (!empty($result[$i]["ToSchoolCode"])){
						// $guestUserID[$i] .= "-".$result[$i]["ToSchoolCode"];
					// }
					$guestGroupID[$i] = $result[$i]["GroupID"];
					$guestGroupType[$i] = $result[$i]["GroupType"];
					$guestName[$i] = $result[$i]["Name"];
					$guestStatus[$i] = trim($result[$i]["Status"]);
					$guestAccess[$i] = trim($result[$i]["Access"]);
					$guestInviteStatus[$i] = trim($result[$i]["InviteStatus"]);
					if ($guestStatus[$i] == "W")
						$guestStatusWait++;
					if ($guestStatus[$i] == "A")
						$guestStatusYes++;
					if ($guestStatus[$i] == "R")
						$guestStatusNo++;
					if ($guestStatus[$i] == "M")
						$guestStatusMaybe++;
						
					// User own status
					if ($guestUserID[$i] == $UserID) {
						$userInviteStatus = $guestInviteStatus[$i];
						$userStatus = $guestStatus[$i];
						$userAccess = $guestAccess[$i];
					}
					
					# populate unique group ID
					if ($guestGroupID[$i] !="" && !in_array($guestGroupID[$i], $guestGroupList)) {
						$guestGroupList[] = $guestGroupID[$i];
						$guestGroupType[$guestGroupID[$i]] = $guestGroupType[$i];
					}					
				}
			}
			
			// Private Event - only owner can access
			/*
			if (($access=="R" && $userAccess!="A") && $userAccess != "R" ) {
				header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]);
			}
			*/
			
			# Get the Premission Right of user in this calendar
			// if($CalType == 2 || $isOtherCalType){
				// $calAccess = 'R';
			// } else {
				$calViewerInfo = $iCal->getCalDetail($calID);
				$calAccess = (count($calViewerInfo) > 0) ? trim($calViewerInfo[0]['Access']) : '';
			//}
			
			# Check if the event is in a calendar which is readable/editable by User
			# If user is both a guest and calendar viewer, $userAccess will determine the actual access right instead of $calAccess
			if (!isset($userAccess) && $access=="P") {
				$sql  = "SELECT Access, CASE Access WHEN 'A' THEN 1 WHEN 'W' THEN 2 ELSE 3 END as Priority FROM CALENDAR_CALENDAR_VIEWER ";
				$sql .= "WHERE CalID = '$calID' AND UserID = '".$UserID."' ORDER BY Priority";
				$result = $iCal->returnArray($sql);
				if (sizeof($result) > 0 && trim($result[0]["Access"])!="") {
					$calAccess = trim($result[0]["Access"]);
				} else { // No access right at all
					header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]);
				}
			}

			
			// if($CalType == 2 || $isOtherCalType){
				// $InviteStatus = 'NULL';
			// } else {
				# Check Whether the calendar belongs to SCHOOL Calendar or not
				$calInfo = $iCal->getCalBasicDetail($calID);
				$CalType = (count($calInfo) > 0) ? $calInfo[0]['CalType'] : 0;
				///$CalType = ($CalType != 2) ? $CalType : '';
				
				# Get Current EventType 
				$InviteStatus = $iCal->Get_Current_Event_Type($calID, $eventID);
			//}
			
			
			
		} else {
			header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]);
		}
	}
	// Reminder Info : modified on 20 July 2009
	$sql  = "SELECT a.* FROM CALENDAR_REMINDER AS a ";
	$sql .= "WHERE a.EventID='$eventID' AND a.UserID='".$UserID."'";
	$result = $iCal->returnArray($sql);
	//echo "<PRE>";echo print_r($result);echo "</PRE>";
	//echo "eventid=".$eventID."<br />".$UserID."<br />";
	if (sizeof($result) > 0) {
		for ($i=0; $i<sizeof($result); $i++) {
			$reminderID[$i] = $result[$i]["ReminderID"];
			$reminderType[$i] = $result[$i]["ReminderType"];
			$reminderDate[$i] = $result[$i]["ReminderDate"];
			$lastSent[$i] = $result[$i]["LastSent"];
			$timeSent[$i] = $result[$i]["TimeSent"];
		}
	}
	
	if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
		$title_extra_info = $iCal->getEventTitleExtraInfo($eventID);
		$title = str_replace($title_extra_info,'', $title);
	}
} else {
	# Get Default Event Access of Owner Calendars
	$ownCalendarList = $iCal->returnViewableCalendar('', '', true);
	$ownDefaultAccess = array();
	for ($i=0; $i<sizeof($ownCalendarList); $i++) {
		$ownDefaultAccess[$ownCalendarList[$i]["CalID"]] = trim($ownCalendarList[$i]["DefaultEventAccess"]);
	}

	//$access = trim($ownCalendarList[0]["DefaultEventAccess"]);
	$access = 'P';
	
	# Optional Calendar
	// nth
}

		
################################
#debug box 
/*
echo '<div style="display:none">';
echo "<pre>";
var_dump($access);
echo "</pre>";
echo '</div>';
*/
################################



# Main UI - Preparation
if($editMode){
	$CalLabeling = '<div id="sub_page_title">'.$iCalendar_EditEvent_Header.'</div>';
} else {
	$CalLabeling = '<div id="sub_page_title">'.$iCalendar_New_Event.'</div>';
}

# Set default url if url is nth
$url = (isset($url) && trim($url) != "") ? trim($url) : "";

# Input Action
// Event Start Hour
$eventHrArr = array();
$eventHrVal = (isset($eventTime)) ? (int)$eventHr : (int)date("H");
$eventEndHrVal = (isset($eventTime)) ? (int)$eventEndHr : (int)date("H");

for ($i=$iCal->systemSettings["WorkingHoursStart"]; $i<$iCal->systemSettings["WorkingHoursEnd"]+1; $i++) {
	$eventHrArr[] = array(sprintf('%02d',$i), $i);
}

$startHr = $lui->Get_Input_Select("eventHr", "eventHr", $eventHrArr, $eventHrVal, (isset($eventTime)?"":" disabled") );

if ($eventHrVal>0 && $eventEndHrVal<=0)
{
	$eventEndHrVal = $eventHrVal;
}
$endHr = $lui->Get_Input_Select("eventEndHr", "eventEndHr", $eventHrArr, $eventEndHrVal, (isset($eventTime)?"":" disabled") );


// Event Start Minute
$interval = 5;
$eventMinArr = array();
$defaultMin = (int)date("i");
$eventMinVal = (isset($eventTime)) ? $eventMin : (($defaultMin % 5 == 0) ? $defaultMin : (int)round($defaultMin, -1));
$eventMinVal = str_pad($eventMinVal, 2, 0, STR_PAD_LEFT);

$eventEndMinVal = (isset($eventTime)) ? $eventEndMin : (($defaultMin % 5 == 0) ? $defaultMin : (int)round($defaultMin, -1));
$eventEndMinVal = str_pad($eventEndMinVal, 2, 0, STR_PAD_LEFT);

for ($i=0; $i<(60/$interval); $i++){
	$min_value = $i * $interval;
	if ($min_value<10) $min_value = "0" . $min_value;
	$eventMinArr[] = array($min_value, $min_value);
}
$startMinArr = Array();
if ($eventMin%5 != 0){
	for ($i=0; $i<(60/$interval); $i++){
		$min_value = $i * $interval;
		if ($min_value > $eventMin && ($min_value-5) < $eventMin){
			$addition = ($eventMin<10)? "0" . $eventMin : $eventMin;
			$startMinArr[] = array($addition, $addition);
		}
		if ($min_value<10) $min_value = "0" . $min_value;
		$startMinArr[] = array($min_value, $min_value);
	}
}
$endMinArr = Array();
if ($eventEndMin%5 != 0){
	for ($i=0; $i<(60/$interval); $i++){
		$min_value = $i * $interval;
		if ($min_value > $eventEndMin && $min_value-5 < $eventEndMin){
			$addition = ($eventEndMin<10)? "0" . $eventEndMin : $eventEndMin;
			$endMinArr[] = array($addition, $addition);
		}
		if ($min_value<10) $min_value = "0" . $min_value;
		$endMinArr[] = array($min_value, $min_value);
	}
}



$startMin = $lui->Get_Input_Select("eventMin", "eventMin", $eventMin%5==0?$eventMinArr:$startMinArr, $eventMinVal, (isset($eventTime)?"":" disabled"));
//if ($eventMinVal>0 && $eventEndMinVal<=0)
//{
//	$eventEndMinVal = $eventMinVal;
//}
$endMin = $lui->Get_Input_Select("eventEndMin", "eventEndMin", $eventEndMin%5==0?$eventMinArr:$endMinArr, $eventEndMinVal, (isset($eventTime)?"":" disabled"));


// Event Duration Hour
$durationHrArr = array();
$durationHrVal = (isset($durationHr)) ? $durationHr : "";
$durationHrArr[] = array("0 hr.", 0);
$durationHrArr[] = array("1 hr.", 1);
for($i=2 ; $i<13 ; $i++) $durationHrArr[] = array($i." hrs.", $i);
$durationHr = $lui->Get_Input_Select("durationHr", "durationHr", $durationHrArr, $durationHrVal, (isset($eventTime)?"":" disabled"));

// Event Duration Minute
$durationMinArr = array();
$durationMinVal = (isset($durationMin)) ? $durationMin : "";
for ($i=0; $i<(60/$interval); $i++){
	$min_value = $i * $interval;
	$min_text = ($min_value>0) ? " mins." : " min.";
	$durationMinArr[] = array($min_value.$min_text, $min_value);
}
$durationMin = $lui->Get_Input_Select("durationMin", "durationMin", $durationMinArr, $durationMinVal, (isset($eventTime)?"":" disabled"));

// Repeat Type
$repeatTypeArr = array();
$repeatTypeArr[] = array($iCalendar_NewEvent_Repeats_Not, "NOT");
$repeatTypeArr[] = array($iCalendar_NewEvent_Repeats_Daily, "DAILY");
$repeatTypeArr[] = array($iCalendar_NewEvent_Repeats_EveryWeekday, "WEEKDAY");
$repeatTypeArr[] = array($iCalendar_NewEvent_Repeats_EveryMonWedFri, "MONWEDFRI");
$repeatTypeArr[] = array($iCalendar_NewEvent_Repeats_EveryTuesThur, "TUESTHUR");
$repeatTypeArr[] = array($iCalendar_NewEvent_Repeats_Weekly, "WEEKLY"); 
$repeatTypeArr[] = array($iCalendar_NewEvent_Repeats_Monthly, "MONTHLY"); 
$repeatTypeArr[] = array($iCalendar_NewEvent_Repeats_Yearly, "YEARLY");
//$select_repeat_type = $lui->Get_Input_Select("repeatSelect", "repeatSelect", $repeatTypeArr, $repeatType, "onChange='showRepeatCtrl(); showEbookingLocationSelection();'");
$select_repeat_type = $lui->Get_Input_Select("repeatSelect", "repeatSelect", $repeatTypeArr, $repeatType);

$repeatEndField = '';
if(isset($repeatType)) {
	$repeatEndField  = "<div id=\"repeatEndDiv\">";
	$repeatEndField .= "<span class=\"cssform\">";
	$repeatEndField .= $lui->Get_DatePicker("repeatEnd", "yy-mm-dd", $repeatEndDate, "class=\"textbox_date\" style=\"width:120px\" onchange=\"showRepeatCtrl()\"");
	$repeatEndField .= "</span>";
	$repeatEndField .= "</div>";						
	
	switch($repeatType) {
		case "DAILY":
			$repeatCtrlDaily = "<span class=\"cssform\" style=\"padding:10px\">".$iCalendar_NewEvent_Repeats_Every."</span>";
			$repeatCtrlDaily .= "<span class=\"cssform\"><select id='repeatDailyDay' name='repeatDailyDay' onChange='changeRepeatMsg()'>";
			$repeatCtrlDaily .= "<option value='1'".(($frequency==$i)?"selected":"").">1 $iCalendar_NewEvent_DurationDay</option>";
			for ($i=2; $i<=14; $i++) {
				$repeatCtrlDaily .= "<option value='$i'".(($frequency==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationDays</option>";
			}
			$repeatCtrlDaily .= "</select></span>";
			$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency);
			break;
		case "WEEKDAY":
			$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate);
			break;
		case "MONWEDFRI":
			$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate);
			break;
		case "TUESTHUR":
			$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate);
			break;
		case "WEEKLY":
			$weekday_flag = preg_split('#(?<=.)(?=.)#s', $detail);
			
			$repeatCtrlWeekly = "<span class=\"cssform\" style=\"padding:10px\">".$iCalendar_NewEvent_Repeats_Every."</span>";
			$repeatCtrlWeekly .= "<span class=\"cssform\"><select id='repeatWeeklyRange' name='repeatWeeklyRange' onChange='changeRepeatMsg()'>";
			$repeatCtrlWeekly .= "<option value='1'".(($frequency==$i)?"selected":"").">1 $iCalendar_NewEvent_DurationWeek</option>";
			for ($i=2; $i<=14; $i++) {
				$repeatCtrlWeekly .= "<option value='$i'".(($frequency==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationWeeks</option>";
			}
			$repeatCtrlWeekly .= "</select></span>";
			$repeatCtrlWeekly .= "<br />";
			$repeatCtrlWeekly .= "<span class=\"cssform\">".$iCalendar_NewEvent_Repeats_On."</span><br />";
			$repeatCtrlWeekly .= "<span class=\"cssform\">&nbsp;</span>";
			// Bug Fix(19/12/2007): Use English name for weekday checkbox name under all UI language
			
			$tempWeekDayArray = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
			for ($i=0; $i<sizeof($tempWeekDayArray); $i++) {
				$repeatCtrlWeekly .= "<input type='checkbox' name='".$tempWeekDayArray[$i]."' id='".$tempWeekDayArray[$i]."' onClick='changeRepeatMsg()'".(($weekday_flag[$i]=="1")?" CHECKED":"")." /> ";
				$repeatCtrlWeekly .= "<label for='".$tempWeekDayArray[$i]."'><span class=\"cssform\" style=\"padding:0px\">".$iCalendar_NewEvent_Repeats_WeekdayArray[$i]."</span></label>&nbsp;&nbsp;";
			}
			$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency, $detail);
			break;
		case "MONTHLY":
			$monthlyDetailPiece = explode("-",$detail);
			
			$repeatCtrlMonthly = "<span class=\"cssform\" style=\"padding:10px\">".$iCalendar_NewEvent_Repeats_Every."</span>";
			$repeatCtrlMonthly .= "<span class=\"cssform\"><select id='repeatMonthlyRange' name='repeatMonthlyRange' onClick='changeRepeatMsg()'>";
			$repeatCtrlMonthly .= "<option value='1'".(($frequency==$i)?"selected":"").">1 $iCalendar_NewEvent_DurationMonth</option>";
			for ($i=2; $i<=14; $i++) {
				$repeatCtrlMonthly .= "<option value='$i'".(($frequency==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationMonths</option>";
			}
			$repeatCtrlMonthly .= "</select></span>";
			$repeatCtrlMonthly .= "<br />";
			$repeatCtrlMonthly .= "<span class=\"cssform\" style=\"padding:10px\">".$iCalendar_NewEvent_Repeats_By."</span><br />";
			$repeatCtrlMonthly .= "<span class=\"cssform\">&nbsp;</span>";
			$repeatCtrlMonthly .= "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy0' onClick='changeRepeatMsg()' value='dayOfMonth'".(($monthlyDetailPiece[0]=="day")?" checked":"")." /> ";
			$repeatCtrlMonthly .= "<label for='monthlyRepeatBy0'><span class=\"cssform\" style=\"padding:0px\">$iCalendar_NewEvent_Repeats_Monthly3</span></label>";
			$repeatCtrlMonthly .= "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy1' onClick='changeRepeatMsg()' value='dayOfWeek'".(($monthlyDetailPiece[0]=="day")?"":" checked")." /> ";
			$repeatCtrlMonthly .= "<label for='monthlyRepeatBy1'><span class=\"cssform\" style=\"padding:0px\">$iCalendar_NewEvent_Repeats_Monthly4</span></label>";
			
			$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency, $detail);
			break;
		case "YEARLY":
			$repeatCtrlYearly = "<span class=\"cssform\" style=\"padding:10px\">".$iCalendar_NewEvent_Repeats_Every."</span>";
			$repeatCtrlYearly .= "<span class=\"cssform\"><select id='repeatYearlyRange' name='repeatYearlyRange' onClick='changeRepeatMsg()'>";
			$repeatCtrlYearly .= "<option value='1'>1 $iCalendar_NewEvent_DurationYear</option>";
			for ($i=2; $i<=14; $i++) {
				$repeatCtrlYearly .= "<option value='$i'".(($frequency==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationYears</option>";
			}
			$repeatCtrlYearly .= "</select></span>";
			$repeatCtrlYearly .= "<br />";
			
			$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency, "", $repeatEndDateTime);
			break;
	}
} else {
	$repeatEndField  = "<div id=\"repeatEndDiv\">";
	$repeatEndField .= "<span class=\"cssform\">";
	$repeatEndField .= $lui->Get_DatePicker("repeatEnd", "yy-mm-dd", date("Y-m-d", strtotime("+1 month")), "class=\"textbox_date\" style=\"width:120px\" onchange=\"showRepeatCtrl()\"");
	$repeatEndField .= "</span>";
	$repeatEndField .= "</div>";
}

$repeatMsgField = '';
$repeatMsgField .= '
	<div id="repeatMsgTable" style="display:'.(isset($repeatType) ? "block" : "none").'; margin-top:5px;">
	  <table border="0" cellpadding="5" cellspacing="0">
		<tr>
		  <td style="border: 0px solid black; padding-left: 0px; padding-right: 0px;">
			<span id="repeatMsgSpan" class="cssform" style="padding: 3px; color: red">'.$repeatMsg.'</span>
		  </td>
		</tr>
	  </table>
	</div>
	<div id="repeatCtrlDaily" style="display:'.(($repeatType=="DAILY")?"block":"none").'; margin-top:5px;">'.$repeatCtrlDaily.'</div>
	<div id="repeatCtrlWeekly" style="display:'.(($repeatType=="WEEKLY")?"block":"none").'; margin-top:5px;">'.$repeatCtrlWeekly.'</div>
	<div id="repeatCtrlMonthly" style="display:'.(($repeatType=="MONTHLY")?"block":"none").'; margin-top:5px;">'.$repeatCtrlMonthly.'</div>
	<div id="repeatCtrlYearly" style="display:'.(($repeatType=="YEARLY")?"block":"none").'; margin-top:5px;">'.$repeatCtrlYearly.'</div>
	<div id="repeatRange" style="display:'.(isset($repeatType)?"block":"none").'; margin-top:1px;">
	  <table border="0" cellpadding="0" cellspacing="0">
		<tr>
		 <td colspan="4"><span class="cssform" style="padding:10px">'.$iCalendar_NewEvent_Repeats_Range.'</span></td>
		</tr>
		<tr>
		  <td><span class="cssform" style="padding:10px">'.$iCalendar_NewEvent_Repeats_RangeStart.'&nbsp;</span></td>
		  <td><span class="cssform"><input class="textbox_date" type="text" name="repeatStart" disabled="true" style="background-color:#f0f0f0;color:#000000;width:120px" value="'.$FirstRepeatEventDate.'"></span></td>
		  <td><span class="cssform" style="padding:10px">&nbsp;'.$iCalendar_NewEvent_Repeats_RangeEnd.'&nbsp;</span></td>
		  <td>'.$repeatEndField.'<input type="hidden" name="oldRepeatPattern" value="'.$repeatPattern.'" /></td>
		</tr>
	  </table>
	</div>';

// Calendar Type
$calendarTypeArr = array();
if (!$editMode || (($userAccess == "A" || $userAccess == "W") || ($calAccess == 'W' || $calAccess == 'A'))&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID']) ) { 
	$select_calendar_type = "<label style=\"width: 40px;\" for=\"calendar\">$iCalendar_NewEvent_Calendar</label>";
	/*
	if ($calAccess == 'W') {
		$sql = "SELECT Name FROM CALENDAR_CALENDAR WHERE CalID=$calID";
		$result = $iCal->returnArray($sql);
		$select_calendar_type .= $result[0]["Name"];
		$select_calendar_type .="<input type='hidden' name='calID' value='".$calID."' />";
	} else {
	*/
		$DefaultOwnerCalID = $iCal->returnOwnerDefaultCalendar();			# not yet update to dev-new
		$ownCalendar = $iCal->returnViewableCalendar('', '', true);
		
		if (sizeof($ownCalendar) == 1) {
			$select_calendar_type .= $ownCalendar[0]["Name"];
			$select_calendar_type .= "<input type='hidden' name='calID' value='".$ownCalendar[0]["CalID"]."' />";
		} else {
			for ($i=0; $i<sizeof($ownCalendar); $i++) {
				if ($ownCalendar[$i]["CalType"] == 4)
					continue;
				$calName = trim(htmlspecialchars(intranet_undo_htmlspecialchars($ownCalendar[$i]["Name"])));
				$calName =  $ownCalendar[$i]["CalType"] == 3? intranet_undo_htmlspecialchars($calName):$calName;
				//$calName = strlen($calName)>30?substr($calName,0,30)." ...":$calName;
				$calName = chopString($calName, 30, " ...");
				$calendarTypeArr[] = array($calName, $ownCalendar[$i]["CalID"]);
				
				### developing by Ronald @ 2010-10-12
				if($ownCalendar[$i]["CalType"] == 0){
					if($ownCalendar[$i]["Owner"] != $_SESSION['UserID']){
						$arrOptionalCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
					}else{
						$arrPersonalCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
					}
				}else if($ownCalendar[$i]["CalType"] == 1){
					$arrSchoolCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
				}else if($ownCalendar[$i]["CalType"] == 2){
					$arrGroupCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
				}else if($ownCalendar[$i]["CalType"] == 3){
					$arrCourseCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
				}
			}
			
			//$others = (!$editMode) ? "onchange=\"jSet_Event_Default_Access()\"" : "";
			// $selectedCalID = ($editMode) ? $calID : $DefaultOwnerCalID;		# not yet update to dev-new
			
			if ($DefaultCalendar == '') 
				$DefaultCalendar = $DefaultOwnerCalID;
			$selectedCalID = ($editMode) ? $calID : $DefaultCalendar;
			
			$others = 'onchange="jDisplay_Invite_Guest(this); jShow_Insert_Into_School_Calendar(this.value);"';
			
			//$select_calendar_type .= $lui->Get_Input_Select("calID", "calID", $calendarTypeArr, $selectedCalID, $others);
			
			### developing by Ronald @ 2010-10-12
			$display_checkbox = "style='display: none;'";
			$setCheckBoxChecked = "";
			$select_calendar_type .= '<select name="calID" id="calID" '.$others.'>';
			if(sizeof($arrPersonalCalendar)>0){
				foreach($arrPersonalCalendar as $key=>$arrPersonal){
					if($key == 0){
						$select_calendar_type .= '<optgroup label="'.$iCalendar_Calendar_MyCalendar.'">';
					}
					$personal_cal_id = $arrPersonal[0];
					$personal_cal_name = $arrPersonal[1];
					$selected = ($personal_cal_id == $selectedCalID) ? " SELECTED " : "";
					$select_calendar_type .= '<option id="'.$personal_cal_id.'" class="calPersonal" value="'.$personal_cal_id.'" '.$selected.'>'.$personal_cal_name.'</option>';
				}
			}
			if(sizeof($arrSchoolCalendar)>0){
				foreach($arrSchoolCalendar as $key=>$arrSchool){
					if($key == 0){
						$select_calendar_type .= '<optgroup label="'.$iCalendar_Calendar_OtherCalendar_School.'">';
					}
					$school_cal_id = $arrSchool[0];
					$school_cal_name = $arrSchool[1];
					$selected = ($school_cal_id == $selectedCalID) ? " SELECTED " : "";
					$select_calendar_type .= '<option id="'.$school_cal_id.'" class="calSchool" value="'.$school_cal_id.'" '.$selected.'>'.$school_cal_name.'</option>';
				}
			}
			if(sizeof($arrGroupCalendar)>0){
				foreach($arrGroupCalendar as $key=>$arrGroup){
					if($key == 0){
						$select_calendar_type .= '<optgroup label="'.$iCalendar_group.'">';
					}
					$group_cal_id = $arrGroup[0];
					$group_cal_name = $arrGroup[1];
					//$selected = ($group_cal_id == $selectedCalID) ? " SELECTED " : "";
					if($group_cal_id == $selectedCalID){
						$selected = " SELECTED ";
						$display_checkbox = "";
					}else{
						$selected = "";
					}
					if(($isEditEvent) && ($group_cal_id == $selectedCalID)){
						$display_checkbox = "";
						
						$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = ".$eventID;
						$arrSchoolCalEventID = $iCal->returnVector($sql);
						if(sizeof($arrSchoolCalEventID) > 0){
							$setCheckBoxChecked = " checked ";
						} 
					}else if($group_cal_id == $selectedCalID){
						$display_checkbox = "";
					}
					else{
						//$display_checkbox = "style='display: none;'";
					}
					$select_calendar_type .= '<option id="'.$group_cal_id.'" class="calGroup" value="'.$group_cal_id.'" '.$selected.'>'.$group_cal_name.'</option>';
				}
			}
			if(sizeof($arrCourseCalendar)>0){
				foreach($arrCourseCalendar as $key=>$arrCourse){
					if($key == 0){
						$select_calendar_type .= '<optgroup label="'.$iCalendar_course.'">';
					}
					$course_cal_id = $arrCourse[0];
					$course_cal_name = $arrCourse[1];
					$selected = ($course_cal_id == $selectedCalID) ? " SELECTED " : "";
					$select_calendar_type .= '<option id="'.$course_cal_id.'" class="calCourse" value="'.$course_cal_id.'" '.$selected.'>'.($course_cal_name).'</option>';
				}
			}
			if(sizeof($arrOptionalCalendar)>0){
				foreach($arrOptionalCalendar as $key=>$arrOptional){
					if($key == 0){
						$select_calendar_type .= '<optgroup label="'.$iCalendar_Calendar_OtherCalendar.'">';
					}
					$optional_cal_id = $arrOptional[0];
					$optional_cal_name = $arrOptional[1];
					$selected = ($optional_cal_id == $selectedCalID) ? " SELECTED " : "";
					$select_calendar_type .= '<option id="'.$optional_cal_id.'" class="calOptional" value="'.$optional_cal_id.'" '.$selected.'>'.$optional_cal_name.'</option>';
				}
			}
			$select_calendar_type .= '</select>';
			
			$select_calendar_type .= '<br><span id="DIV_ShowInSchoolCalendar" '.$display_checkbox.'><input type="checkbox" id="ShowInSchoolCalendar" name="ShowInSchoolCalendar"  value="1" '.$setCheckBoxChecked.'>'.$Lang['iCalendar']['FieldTitle']['ShowInSchoolCalendar'].'</span>';
		}
	//}
} else {

	$select_calendar_type = "<label for=\"calendar\">$iCalendar_EditEvent_CreatedBy</label>";
	$select_calendar_type .= $createdBy==0?$iCalendar_systemAdmin:($iCal->returnUserFullName($createdBy)==0?$Lang['iCalendar']['null_created_by']:$iCal->returnUserFullName($createdBy));
}


// Reminder - Alert
$reminderArr = array();
$select_reminder = '';
$isAlert = 0;
if ($editMode) {
	if (isset($reminderType)) {
		$select_reminder .= '<span id="noReminderMsg" class="tabletextremark" style="display: none;">'.$iCalendar_NewEvent_Reminder_NotSet.'</span>';
		if(count($reminderType) > 0) $isAlert = 1;
		
		for ($i=0; $i<sizeof($reminderType); $i++) {
			$select_reminder .= "<span>";
			$reminderTs = $iCal->sqlDatetimeToTimestamp($reminderDate[$i]);
			$reminderTime = ($eventTs-$reminderTs) / 60;
			$select_reminder .= $iCal->generateReminderSelect($reminderType[$i], $reminderTime);
			$select_reminder .= '&nbsp;<span onClick="removeReminder(this)"><a href="javascript:void(0)" class="sub_layer_link">'.$iCalendar_NewEvent_Reminder_Remove.'</a></span> <br />';
			$select_reminder .= "</span>";
			$select_reminder .= "<input type='hidden' name='reminderID[]' value='".$reminderID[$i]."' />";
		}
	} else {
		$select_reminder = '<span id="noReminderMsg" class="tabletextremark" style="display: block;">'.$iCalendar_NewEvent_Reminder_NotSet.'</span>';
	}
} else {
	$select_reminder .= '<span id="noReminderMsg" class="tabletextremark" style="display:;">'.$iCalendar_NewEvent_Reminder_NotSet.'</span>';
	//$select_reminder .= '<span>';
	//$select_reminder .= $iCal->generateReminderSelect();
	//$select_reminder .= '&nbsp;<span onClick="removeReminder(this)"><a href="javascript:void(0)" class="sub_layer_link">'.$iCalendar_NewEvent_Reminder_Remove.'</a></span><br />';
	//$select_reminder .= '</span>';
}

# Invite Guest 
$ShareGuestResponse = '';
if ($editMode && $haveGuest && ($userAccess!="") && (!isset($userInviteStatus) || (isset($userInviteStatus) && $userInviteStatus == 1) ) ) {
	
	//$eventTs = $iCal->sqlDatetimeToTimestamp($eventDateTime);
	
	
 	$conflictArr = $iCal->returnEventConflictArr($eventID, $eventTs, $endTs);
 	
 	$ShareGuestResponse .= '<span class="new_event_sub_form_title">'.$iCalendar_NewEvent_ShareResponse;
 	
	if (sizeof($conflictArr) > 0) {
		$x = '';
		$timeForReplace = date("H:i",$eventTs)."-".date("H:i",$endTs);

		$ShareGuestResponse .= '
		  <div id="conflictTT" style="display:none;">
		    <span id="cluetip-close" style="display: block;"></span>
			<p>'.sprintf($i_Calendar_Conflict_Warning2, $timeForReplace).'</p>
			<ul style="text-align:left;">';
		for($i=0; $i<sizeof($conflictArr) ; $i++) {
			$tempStartTs = $iCal->sqlDatetimeToTimestamp($conflictArr[$i]["EventDate"]);
			$tempEndTs = $tempStartTs + $conflictArr[$i]["Duration"]*60;
			
			$x .= "<li style='margin-left:-20px;'>";
			$x .= "<a href='new_event.php?editEventID=event".$conflictArr[$i]["EventID"]."' class='sub_layer_link'>".$conflictArr[$i]["Title"]."</a>";
			$x .= " (".date("H:i",$tempStartTs)." $i_To ".date("H:i",$tempEndTs).")";
			$x .= " on ".$iCalendar_NewEvent_Repeats_WeekdayArray2[date("w",$tempStartTs)];
			$x .= ", ".date("Y-m-d",$tempStartTs)."</li>";
		}
		$ShareGuestResponse .= $x;
		$ShareGuestResponse .= '
  		    </ul>
		  </div>
		  <div><a class="tableorangelink" id="loadConflictTT" href="#conflictTT" rel="#conflictTT">! Conflict found</a></div>';
	}
	$ShareGuestResponse .= '<br><div class="new_event_sub_form_content">';
	//$ShareGuestResponse .= '<input type="radio" name="response" id="response_X" value=""'.(($userStatus=="W")?" checked":"").' />';
	//$ShareGuestResponse .= '<label for="response_X" class="tabletextremark">'.ucfirst($iCalendar_NewEvent_ShareCountMsgX).'</label><br />';
	$ShareGuestResponse .= '<input type="radio" name="response" id="response_0" value="A"'.(($userStatus=="A")?" checked":"").' />';
	$ShareGuestResponse .= '<label for="response_0">'.ucfirst($iCalendar_NewEvent_ShareCountMsg3).'</label><br />';
	$ShareGuestResponse .= '<input type="radio" name="response" id="response_1" value="R"'.(($userStatus=="R")?" checked":"").' />';
	$ShareGuestResponse .= '<label for="response_1">'.ucfirst($iCalendar_NewEvent_ShareCountMsg4).'</label><br />';
	$ShareGuestResponse .= '<input type="radio" name="response" id="response_2" value="M"'.(($userStatus=="M")?" checked":"").' />';
	$ShareGuestResponse .= '<label for="response_2">'.ucfirst($iCalendar_NewEvent_ShareCountMsg2).'</label><br />';
	$ShareGuestResponse .= '</div></span>';
}

# Invite Guest
$ShareGuestCount = '';
$ShareGuestList = '';
if($editMode && $haveGuest && ($userAccess!="" || $calAccess != "") ) {
	# Share Count 
	if ($guestStatusYes > 0)
		$guestCountMsg .= "$guestStatusYes $iCalendar_NewEvent_ShareCountMsg3";
	if ($guestStatusNo > 0)
		$guestCountMsg .= (($guestCountMsg!="")?", ":"")."$guestStatusNo $iCalendar_NewEvent_ShareCountMsg4";
	if ($guestStatusMaybe > 0)
		$guestCountMsg .= (($guestCountMsg!="")?", ":"")."$guestStatusMaybe $iCalendar_NewEvent_ShareCountMsg2";
	if ($guestStatusWait > 0)
		$guestCountMsg .= (($guestCountMsg!="")?", ":"")."$guestStatusWait $iCalendar_NewEvent_ShareCountMsg1";
	$ShareGuestCount = $guestCountMsg;
		
	# Exist invited guests
	$groupGuestYesList = array();
	$groupGuestNoList = array();
	$groupGuestMaybeList = array();
	$groupGuestWaitList = array();
	for($i=0; $i<sizeof($guestName); $i++) {
		if($guestStatus[$i] == "A") {
			if ($guestGroupID[$i] != "") {
				$groupGuestYesList[$guestGroupID[$i]][] = array("guestName"=>$guestName[$i], "guestUserID"=>$guestUserID[$i]);
			} else {
				//$guestYesList .= $iCal->generateEventGuestDiv($guestUserID[$i], $guestName[$i], $userAccess, $createdBy);
				$guestYesList .= $iCal->generateEventGuestDiv($guestUserID[$i], $guestName[$i], $userAccess, $createdBy, $calAccess);
			}
		}
		if($guestStatus[$i] == "R") {
			if ($guestGroupID[$i] != "") {
				$groupGuestNoList[$guestGroupID[$i]][] = array("guestName"=>$guestName[$i], "guestUserID"=>$guestUserID[$i]);
			} else
				$guestNoList .= $iCal->generateEventGuestDiv($guestUserID[$i], $guestName[$i], $userAccess, $createdBy, $calAccess);
		}
		if($guestStatus[$i] == "M") {
			if ($guestGroupID[$i] != "") {
				$groupGuestMaybeList[$guestGroupID[$i]][] = array("guestName"=>$guestName[$i], "guestUserID"=>$guestUserID[$i]);
			} else
				$guestMaybeList .= $iCal->generateEventGuestDiv($guestUserID[$i], $guestName[$i], $userAccess, $createdBy, $calAccess);
		}
		if($guestStatus[$i] == "W") {
			if ($guestGroupID[$i] != "") {
				$groupGuestWaitList[$guestGroupID[$i]][] = array("guestName"=>$guestName[$i], "guestUserID"=>$guestUserID[$i]);
			} else
				$guestWaitList .= $iCal->generateEventGuestDiv($guestUserID[$i], $guestName[$i], $userAccess, $createdBy, $calAccess);
		}
	}
	# Group or Individual Guest "Yes" List
	if (!empty($groupGuestYesList) || !empty($guestYesList)) {
		$ShareGuestList .= '<span class="new_event_sub_form_title">'.ucfirst($iCalendar_NewEvent_ShareCountMsg3).'?';
		if (!empty($groupGuestYesList)) {
			$ShareGuestList .= $iCal->generateEventGroupGuestList("groupGuestYesList", $groupGuestYesList, $userAccess, $createdBy);
		}
		if(!empty($guestYesList)) {
			$ShareGuestList .= $guestYesList;
		}
		$ShareGuestList .= '</span>';
	}
	# Group or Individual Guest "No" List
	if (!empty($groupGuestNoList) || !empty($guestNoList)) {
		$ShareGuestList .= '<span class="new_event_sub_form_title">'.ucfirst($iCalendar_NewEvent_ShareCountMsg4).'?';
		if (!empty($groupGuestNoList)) {
			$ShareGuestList .= $iCal->generateEventGroupGuestList("groupGuestNoList", $groupGuestNoList, $userAccess, $createdBy);
		}
		if(!empty($guestNoList)) {
			$ShareGuestList .= $guestNoList;
		}
		$ShareGuestList .= '</span>';
	}
	# Group or Individual Guest "May Be" List
	if (!empty($groupGuestMaybeList) || !empty($guestMaybeList)) {
		$ShareGuestList .= '<span class="new_event_sub_form_title">'.ucfirst($iCalendar_NewEvent_ShareCountMsg2).'?';
		if (!empty($groupGuestMaybeList)) {
			$ShareGuestList .= $iCal->generateEventGroupGuestList("groupGuestMaybeList", $groupGuestMaybeList, $userAccess, $createdBy);
		}
		if(!empty($guestMaybeList)) {
			$ShareGuestList .= $guestMaybeList;
		}
		$ShareGuestList .= '</span>';
	}
	# Group or Individual Guest "Wait" List
	if (!empty($groupGuestWaitList) || !empty($guestWaitList)) {
		$ShareGuestList .= '<span class="new_event_sub_form_title">'.ucfirst($iCalendar_NewEvent_ShareCountMsg1).'?';
		if (!empty($groupGuestWaitList)) {
			$ShareGuestList .= $iCal->generateEventGroupGuestList("groupGuestWaitList", $groupGuestWaitList, $userAccess, $createdBy);
		}
		if(!empty($guestWaitList)) {
			$ShareGuestList .= $guestWaitList;
		}
		$ShareGuestList .= '</span>';
	}
}


## Max
$ReminderLimit = $iCal->reminderLimit;



##############################################################################################################################
# Generate HTML for the buttons use in repeat event saving dialog
$button_name = $iCalendar_NewEvent_Repeats_SaveThisOnly;
$repeatSaveButtons  = $lui->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.form1.saveRepeatEvent.value='OTI';if(document.form1.elements['viewer[]']) checkOptionAll(document.form1.elements['viewer[]']);document.form1.submit();\"");
$button_name = $iCalendar_NewEvent_Repeats_SaveAll;
$repeatSaveButtons  .= $lui->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.form1.saveRepeatEvent.value='ALL';if(document.form1.elements['viewer[]']) checkOptionAll(document.form1.elements['viewer[]']);document.form1.submit();\"");
if ($userAccess == "A" || $userAccess == "W" || $calAccess == 'A' || $calAccess == 'W'){
	$button_name = $iCalendar_NewEvent_Repeats_SaveFollow;
	$repeatSaveButtons  .= $lui->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.form1.saveRepeatEvent.value='FOL';if(document.form1.elements['viewer[]']) checkOptionAll(document.form1.elements['viewer[]']);document.form1.submit();\"");
}
$repeatSaveButtons  .= $lui->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button\" onclick=\"$('#saveEventWarning').jqmHide();\"");
#$repeatSaveButtons = addslashes($repeatSaveButtons);

#Generate HTML for the button use in multiple repeated event saving dialog box 
$multipleRepeatButton = $lui->Get_Input_Button("Overlap", "overlap", $iCalendar_NewEvent_MultipleRepeat_Overlap, "class=\"button\" onclick=\"$('#saveMultipleEventWarning').jqmHide();if(document.form1.isRepeatEvent) $('#saveEventWarning').jqmShow();else document.form1.submit();\"");
$multipleRepeatButton .= $lui->Get_Input_Button("justOne", "justOne", $iCalendar_NewEvent_MultipleRepeat_One, "class=\"button\" onclick=\"$('#saveMultipleEventWarning').jqmHide();document.getElementById('repeatSelect').selectedIndex=0;if(document.form1.isRepeatEvent){document.form1.saveRepeatEvent.value='OTI';if(document.form1.elements['viewer[]']) checkOptionAll(document.form1.elements['viewer[]']);document.form1.submit();}else document.form1.submit();\"");
$multipleRepeatButton .= $lui->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button\" onclick=\"$('#saveMultipleEventWarning').jqmHide();\"");

# Generate HTML for the buttons use in event deleting dialog
$button_name = $iCalendar_NewEvent_Repeats_SaveThisOnly;
$deleteEventButtons1  = $lui->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.form1.deleteRepeatEvent.value='OTI';document.form1.submit();\"");
$button_name = $iCalendar_NewEvent_Repeats_SaveAll;
$deleteEventButtons1 .= $lui->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.form1.deleteRepeatEvent.value='ALL';document.form1.submit();\"");
$button_name = $iCalendar_NewEvent_Repeats_SaveFollow;
$deleteEventButtons1 .= $lui->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.form1.deleteRepeatEvent.value='FOL';document.form1.submit();\"");
$deleteEventButtons1 .= $lui->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button\" onclick=\"document.form1.action='edit_event_update.php';$('#deleteEventWarning1').jqmHide();\"");
#$deleteEventButtons1 = addslashes($deleteEventButtons1);

$deleteEventButtons2  = $lui->Get_Input_Button("Remove", "Remove", $Lang['btn_remove'], "class=\"button\" onclick=\"javascript:document.form1.submit();\"");
$deleteEventButtons2 .= $lui->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button\" onclick=\"document.form1.action='edit_event_update.php';$('#deleteEventWarning2').jqmHide();\"");
#$deleteEventButtons2 = addslashes($deleteEventButtons2);

$button_name = $i_Calendar_Conflict_Ignore;
$conflictBtn  = $lui->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:submitForm();\"")."&nbsp;";
$conflictBtn .= $lui->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button\" onclick=\"$('#eventConflictWarning').jqmHide();\"");
##############################################################################################################################


# UI - New Event Content
$CalMain = '
  <form name="form1" id="form1" action="'.(($editMode) ? "edit_event_update.php" : "new_event_update.php").'" method="POST">
	<div id="cal_board">
	  <span class="cal_board_02"></span> 
	  <div class="cal_board_content">
	    <div id="new_event_advance" style="float:none">
		  <div id="new_event_main_form">';
// Error Message Box		  
$CalMain .= '<div id="errMessageBox" style="padding:3px 0pt 3px 0px;"></div>';
// Title

//$CalMain .= '<p class="cssform"><label for="title" style="width: 40px">'.$iCalendar_NewEvent_Title.'</label>';

$CalMain .= '<p class="cssform"><label for="title" style="width: 40px">';
if ($plugin['eBooking']) {
	$CalMain .= '<span style="color:red; font-weight:string;">#</span>';
}
$CalMain .= $iCalendar_NewEvent_Title;
$CalMain .= '</label>';


$CalMain .= (!$editMode || (($userAccess == "A" || $userAccess == "W" || $calAccess == 'A'  || $calAccess == 'W')&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID']))) ? $lui->Get_Input_Text("title", $title, "maxlength=\"".($sys_custom['iCalendar']['EventTitleWithParticipantNames']?"1024":"255")."\" class=\"textbox\"") : (($title != '') ? $title : '<br>');
$CalMain .= '</p>';





$CalMain .= '<p class="cssform">';
$CalMain .= '<label for="date_time" style="width: 118px;clear:both">'.$iCalendar_NewEvent_DateTime.'</label>';
// Date/Time
if (!$editMode || (($userAccess == "A" || $userAccess == "W" || $calAccess == 'A'  || $calAccess == 'W')&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID']))) {  
	
	
	$CalMain .= '<span >';
	$CalMain .= $lui->Get_Input_CheckBox("isAllDay", "isAllDay", 1, (isset($eventTime)?false:true), "onClick=\"enableTimeSelect()\"" ).$iCalendar_NewEvent_IsAllDay.'<br>';
	$CalMain .= '</span>';
	
	//start input
	$CalMain .= '<span id="mail_form_label_inside" style="width: 40px">'.$iCalendar_NewEvent_StartTime.'</span>';
	# Date Picker
	$otherMember  = "class=\"textbox_date\"";
	if($plugin['iCalendarFull']) {
		if($plugin['eBooking']) {
			$otherMember .= (!$editMode) ? "onchange=\"$('input[@name=repeatStart]').val($('input[@name=eventDate]').val()); showEbookingLocationSelection();\" " : " onChange=\"showEbookingLocationSelection();\" ";	
		} else {
			$otherMember .= (!$editMode) ? "onchange=\"$('input[@name=repeatStart]').val($('input[@name=eventDate]').val());\"" : "";	
		}
	}
	$CalMain .= $lui->Get_DatePicker("eventDate", "yy-mm-dd", (isset($eventDate) ? $eventDate : date("Y-m-d") ), $otherMember, "style=\"display:inline\"");	
	$CalMain .=	'<span id="mail_form_textbox_inside" style="clear: left; float: none">'.$startHr.' : '.$startMin.'</span>';
	
	$CalMain .= "<br style=\"clear:both\">";
	
	//end input
	$CalMain .= '<span id="mail_form_label_inside" style="clear: left; width: 40px">'.$iCalendar_NewEvent_EndTime .'</span>';
	# Date Picker 
	$otherMember  = "class=\"textbox_date\"";
	if($plugin['iCalendarFull']) {
		if($plugin['eBooking']) {
			$otherMember .= (!$editMode) ? "onchange=\"$('input[@name=repeatStart]').val($('input[@name=eventDate]').val()); showEbookingLocationSelection();\"" : " showEbookingLocationSelection(); ";
		} else {
			$otherMember .= (!$editMode) ? "onchange=\"$('input[@name=repeatStart]').val($('input[@name=eventDate]').val());\"" : "";
		}
	}
	$CalMain .= $lui->Get_DatePicker("eventEndDate", "yy-mm-dd", ($isEditEvent? $endDateTime : (isset($eventDate) ? $eventDate : date("Y-m-d"))), $otherMember, "style=\"display:inline\"");	
	$CalMain .=	'<span id="mail_form_textbox_inside" style="clear: both; float: none">'.$endHr.' : '.$endMin.'</span>';
	
		
	$CalMain .= ($editMode) ? "<input type='hidden' name='oldEventDate' value='$eventDate' />" : "";
	if ($editMode && !$isAllDay) {
		$CalMain .=  "<input type='hidden' name='oldEventTime' value='$eventTime' />";
		$CalMain .=  "<input type='hidden' name='oldDuration' value='$duration' />";
	}
	
	
	
	//$CalMain .= '<span id="mail_form_radio">'.$lui->Get_Input_RadioButton("isAllDay", "is_allday_1", 0, (isset($eventTime)?true:false), "onClick=\"enableTimeSelect()\"" ).'</span>';
	/*$CalMain .= '<span id="mail_form_label_textbox_line">
					<span id="mail_form_textbox_inside">
					  
					  ';
	$CalMain .= '	  <br style="clear:both">	
					  <span id="mail_form_label_inside">'.$iCalendar_NewEvent_Duration.'</span>
					  <span id="mail_form_textbox_inside">'.$durationHr.' '.$durationMin.'</span>';
	$CalMain .= '   </span>
				</span>
				<br style="clear:both">
				</p>';*/
	$CalMain .= '<br style="clear:both">
				</p>';
	
	


	if ($iCal->systemSettings["DisableRepeat"] == "no" || $isRepeatEvent) {
		$CalMain .= '<div id="repeatDiv"><p class="cssform"><label for="title">'.$iCalendar_NewEvent_Repeats.'</label>';
		$CalMain .= $select_repeat_type;
		$CalMain .= $repeatMsgField;
		$CalMain .= '</p></div>';
	}
} else { 
	# Display repeat pattern
	if ($isAllDay && $CalType !=4) {
		$eventDateDisplay  = date("Y-m-d", $eventTs);
		$eventDateDisplay .= " ".$iCalendar_NewEvent_IsAllDay;
	} else {
		if ($CalType == 4){
			$eventDateDisplay  = date("Y-m-d", $eventTs);
			$eventDateDisplay .= " - ".date("Y-m-d", strtotime($extEvent[0]["DueDate"]));
		}
		else{
			$eventDateDisplay  = date("Y-m-d h:ia", $eventTs);
			$eventDateDisplay .= " - ".date("Y-m-d h:ia", $eventTs+$duration*60);
			}
	}
	if ($isRepeatEvent) {
		$eventDateDisplay .= " (".$iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency, $detail, $repeatEndDateTime).")";
	}
	$CalMain .= $eventDateDisplay;
	$CalMain .= '<input type="hidden" name="eventDateTime" value="'.$eventDateTime.'" />';
}
// Location
$CalMain .= '<p class="cssform"><label for="location" style="width: 40px">'.$iCalendar_NewEvent_Location.'</label>';
if(!$editMode || (($userAccess == "A" || $userAccess == "W" || $calAccess == 'A' || $calAccess == 'W')&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID']))){
	
	if(($plugin['eBooking']) && ($plugin['iCalendarFull']))
	{
		if($editMode)
		{
			include_once($PATH_WRT_ROOT."includes/libinventory.php");
			include_once($PATH_WRT_ROOT."includes/libebooking.php");	// for loading of constant
			$linventory = new libinventory();

			$eventID = IntegerSafe($eventID);

			if($repeatID == "") {
				$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
			}
			else {			
				$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
			}
			
			$cnt_rejected = 0;
			$cnt_pending = 0;
			$cnt_approved = 0;
				
			$sql = "SELECT 
						event_booking.EventID,
						CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.")."), 
						room_booking.BookingStatus
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
						LEFT OUTER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
						LEFT OUTER JOIN INTRANET_EBOOKING_CALENDAR_EVENT_RELATION AS event_booking ON (room_booking.BookingID = event_booking.BookingID)
					WHERE 
						event_booking.EventID IN ($eventID)";
			$arrBookingResult = $iCal->returnArray($sql);
					
			if(sizeof($arrBookingResult)>0)
			{
				list($temp_event_id, $location_name, $booking_result) = $arrBookingResult[0];
				if($booking_result == -1)
				{
					//$cnt_rejected++;
					global $image_path, $LAYOUT_SKIN;
					$result_img = "<img border='0' src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_reject.gif'>".$Lang['eBooking']['iCal']['FieldTitle']['Rejected'];
				}
				else if($booking_result == 0 || $booking_result == 999)
				{
					//$cnt_pending++;
					global $image_path, $LAYOUT_SKIN;
					$result_img = "<img border='0' src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_waiting.gif'>".$Lang['eBooking']['iCal']['FieldTitle']['Pending'];
				}
				else if($booking_result == 1)
				{
					//$cnt_approved++;
					global $image_path, $LAYOUT_SKIN;
					$result_img = "<img border='0' src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_approve.gif'>".$Lang['eBooking']['iCal']['FieldTitle']['Approved'];
				}
				
				$edit_link = '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
				$edit_link .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>';
				$edit_link .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
				$edit_link .= "<span id='DIV_eBookingLocation' style='display:inline'>".$linterface->Get_Thickbox_Link('500', '800', "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'], "js_Change_Selected_Location(0); return false;", "", $Lang['Btn']['Edit'])."</span>";
				
				$return_string = $location_name." [ ".$edit_link." | <a href='#' onClick='javascript:js_Cancel_Selected_Location()'>".$Lang['Btn']['Clear']."</a> ]<br>".$result_img;
				
				$return_content = "<span id='div_location' style='display:none;'>";
				$return_content .= $lui->Get_Input_Text("location", $location, " class=\"textbox\" maxlength=\"255\" style=\"width:60%\" ");
				## Under Development
				$return_content .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
				$return_content .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>';
				$return_content .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
				$return_content .= " <span id='DIV_eBookingLocation' style='display:inline'>".$linterface->Get_Thickbox_Link('500', '800', "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'], "js_Show_BookingRequest_Layer(); return false;", "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'])."</span>";
				if($repeatID != "") {
					$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
					$arrEventIDs = $iCal->returnVector($sql);
					$targetEventIDs = implode(",",$arrEventIDs);
					
					$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($targetEventIDs)";
					$arrHiddenBookingIDs = $iCal->returnVector($sql);
					$hiddenBookingIDs = implode(",",$arrHiddenBookingIDs);
				} else {
					$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '$eventID'";
					$arrHiddenBookingIDs = $iCal->returnVector($sql);
					$hiddenBookingIDs = implode(",",$arrHiddenBookingIDs);
				}
				$return_content .= "<input type=\"hidden\" id=\"hiddenBookingIDs\" name=\"hiddenBookingIDs\" value=\"$hiddenBookingIDs\">";
				$return_content .= "</span>";
	
				$return_content .= "<span id='div_selected_location' style='display:inline'>";
				$return_content .= $return_string;
				$return_content .= "</span>";
			}
			else
			{
				$booking_link_display_css = $isEditEvent && $eventDate != $endDateTime ? 'none':'inline';
				
				$return_content = "<span id='div_location' style='display:inline'>";
				$return_content .= $lui->Get_Input_Text("location", $location, " class=\"textbox\" maxlength=\"255\" style=\"width:60%\" ");
				## Under Development
				$return_content .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
				$return_content .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>';
				$return_content .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
				$return_content .= " <span id='DIV_eBookingLocation' style='display:".$booking_link_display_css."'>".$linterface->Get_Thickbox_Link('500', '800', "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'], "js_Show_BookingRequest_Layer(); return false;", "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'])."</span>";
				$return_content .= "<input type='hidden' id='hiddenBookingIDs' name='hiddenBookingIDs' value=''>";
				$return_content .= "</span>";
	
				$return_content .= "<span id='div_selected_location' style='display:none'>";
				$return_content .= "</span>";
			}
			$CalMain .= $return_content;
		}
		else
		{
			$CalMain .= "<span id='div_location' style='display:inline'>";
			$CalMain .= $lui->Get_Input_Text("location", $location, " class=\"textbox\" maxlength=\"255\" style=\"width:60%\" ");
			## Under Development
			$CalMain .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
			$CalMain .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>';
			$CalMain .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
			$CalMain .= " <span id='DIV_eBookingLocation' style='display:inline'>".$linterface->Get_Thickbox_Link('500', '800', "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'], "js_Show_BookingRequest_Layer(); return false;", "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'])."</span>";
			$CalMain .= "<input type='hidden' id='hiddenBookingIDs' name='hiddenBookingIDs' value=''>";
			$CalMain .= "</span>";
	
			$CalMain .= "<span id='div_selected_location' style='display:none'>";
			$CalMain .= "</span>";
		}
	}
	else
	{
		$CalMain .= "<span id='div_location' style='display:inline'>";
		$CalMain .= $lui->Get_Input_Text("location", $location, " class=\"textbox\" maxlength=\"255\" style=\"width:60%\" ");
		$CalMain .= "</span>";
	
		$CalMain .= "<span id='div_selected_location' style='display:none'>";
		$CalMain .= "</span>";
	}
	
	
} else {
	
	$CalMain .= (trim($location=="")) ? "-" : $location;
	
}
$CalMain .= '</p>';
# Calendar
$CalMain .= '<p class="cssform">'.$select_calendar_type.'</p>';
//$CalMain .= '<p class="cssform">'.$btn_new_as_school_calender_event.'</p>';

# Description
//$CalMain .= '<p class="cssform"><label for="description" style="width: 100px; clear: left; float: left; text-align:left">'.$iCalendar_NewEvent_Description.'</label>';

$CalMain .= '<p class="cssform"><label for="description" style="width: 100px; clear: left; float: left; text-align:left">';
if ($plugin['eBooking']) {
	$CalMain .= '<span style="color:red; font-weight:string;">#</span>';
}
$CalMain .= $iCalendar_NewEvent_Description;
$CalMain .= '</label>';


if (!$editMode || (($userAccess == "A" || $userAccess == "W" || $calAccess == 'A' || $calAccess == 'W')&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID']))) {
	$CalMain .= $lui->Get_Input_Textarea("description", $description, 20, 3, "", "class=\"textarea_desc\" style=\"font-family:Verdana;font-size:11px\"");
} else {
	$CalMain .= (trim($description)=="") ? "-" : $description;
}
$CalMain .= '</p>';
# Personal Note
if (!$editMode || $userAccess == "A") {
	$CalMain .= '<p class="cssform"><label for="description" style="width: 100px; text-align:left">'.$iCalendar_NewEvent_Personal_Note.'</label>';
	$CalMain .= $lui->Get_Input_Textarea("PersonalNote", $PersonalNote, 20, 6, "", "class=\"textarea_desc\" style=\"font-family:Verdana;font-size:11px\"");
	$CalMain .= '</p>';
} 
else {
	$CalMain .= '<p class="cssform"><label for="description" style="width: 100px; text-align:left">'.$iCalendar_NewEvent_Personal_Note.'</label>';
	$CalMain .= $lui->Get_Input_Textarea("PersonalNote", $PersonalNote, 20, 6, "", "class=\"textarea_desc\" style=\"font-family:Verdana;font-size:11px\"");
	$CalMain .= '</p>';
}
# Link
$CalMain .= '<p class="cssform"><label for="link" style="width: 100px">'.$iCalendar_NewEvent_Link.'</label>';
if (!$editMode || (($userAccess == "A" || $userAccess == "W" || $calAccess == 'A' || $calAccess == 'W')&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID']))) {
	$CalMain .= $lui->Get_Input_Text("url", $url, "class=\"textbox\" maxlength=\"255\"");
} else {
	$url_text = ($url_text != '') ? $url_text : $url;
	$CalMain .= (trim($url)=="") ? "-" : "<a class='tablelink' href='$url' target='_blank'>$url_text</a>";
}
$CalMain .= '</p>';
# Attachment
/*
$CalMain .= '<p class="cssform"><label for="Attachment">'.$iCalendar_NewEvent_Attachment.'</label>';
$CalMain .= $lui->Get_Input_Button("isAttach", "isAttach", $iCalendar_NewEvent_Attach, "class=\"button\"");
$CalMain .= '</p>';
*/
# Important
if (!$editMode || (($userAccess == "A" || $userAccess == "W" || $calAccess == 'A' || $calAccess == 'W')&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID']))) {
	$isImportant = ($isImportant == 1) ? 1 : 0;
	$CalMain .= '<p class="cssform"><label for="Important">'.$iCalendar_NewEvent_IsImportant.'</label>';
	$CalMain .= $lui->Get_Input_CheckBox("isImportant", "isImportant", $isImportant , (($isImportant == 1)?true:false), "onclick=\"this.value = (this.checked==true) ? 1 : 0\"" );
	$CalMain .= '</p>';
}
# Access
$CalMain .= $lui->Get_Input_Hidden('access', 'access', 'P');
/*
if (!$editMode || ($userAccess == "A" || $userAccess == "W")) {
	$CalMain .= '<p class="cssform">';
	$CalMain .= '<label for="Access">'.$iCalendar_NewEvent_Access.'</label>';
	//$CalMain .= $lui->Get_Input_RadioButton("access", "access_0", "D", (($access == "D") ? true : false) ).'Default&nbsp;';
	$CalMain .= $lui->Get_Input_RadioButton("access", "access_1", "R", (($access == "R") ? true : false) ).'Private&nbsp;';
	$CalMain .= $lui->Get_Input_RadioButton("access", "access_2", "P", (($access == "P") ? true : false) ).'Public&nbsp;';
	$CalMain .= '<br>('.$iCalendar_NewEvent_AccessRemark.')';
	$CalMain .= '</p>';
}
*/

$CalMain .= '</div>';

// Alert Box
//@@ Reactivate this reminder alert on 15 July 2009
// Hide it on 7 August 2008 temporarily
/*
$CalMain .= '
			<div id="new_event_alert_form" class="new_event_sub_form">
			  <span class="new_event_sub_form_title">'.$iCalendar_NewEvent_Alert.'<br>
			    <div width="96%" align="center">
			      <div id="reminderDiv">'.$select_reminder.'</div>
				</div>
				<span id="addReminderSpan"><a href="javascript:addReminder()" class="sub_layer_link">'.$iCalendar_NewEvent_AddAlert.'</a></span>					
			  </span>	
			</div>';
*/
if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF || $plugin["iCalendarFullToAllUsers"]){
$CalMain .= '
			<div id="new_event_alert_form" class="new_event_sub_form">
			  <span class="new_event_sub_form_title">'.$iCalendar_PopupAlert.'<br>
			    <div width="96%" align="center">
			      <div id="reminderDiv">'.$select_reminder.'</div>
				</div>
				<span id="addReminderSpan"><a href="javascript:addReminder()" class="sub_layer_link">'.$iCalendar_NewEvent_AddAlert.'</a></span>					
			  </span>	
			</div>';
//@@
if (!$plugin["iCalendarDisableStudentSharing"]&&$UserType==2||$UserType!=2){
// Invite Box 
// if(Auth(array("Communication-ActiveEvent-GeneralUsage")))
// {
if ($iCal->systemSettings["DisableGuest"] == "no" && (!$editMode || (($userAccess == "A" || $userAccess == "W") || ($calAccess == 'A' || $calAccess == "W"))&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID'])) ) {
	$CalMain .= '
			<div id="new_event_invite_form" class="new_event_sub_form">							
			  <span class="new_event_sub_form_title">'.$iCalendar_NewEvent_Guests.'<br>
				 <span class="new_event_sub_form_content">';
	$CalMain .= ' <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="width:220px">
				    <tr>
				  	  <td width="1" class="tabletext">
					    <select name="viewer[]" id="viewerList" style="width:220px" size="5" multiple="multiple">';
		if(!$isEditEvent){
			$CalMain .= '<option value="U'.$_SESSION['UserID'].'">'.Get_Lang_Selection($_SESSION['ChineseName'], $_SESSION['EnglishName']).'</option>';
		}
			$CalMain .= '</select>
					  </td>
					</tr>
					<tr>
					  <td valign="bottom" align="right">
					    <a href="javascript:jInvite_Guest('.$eventID.')" class="sub_layer_link">'.$Lang['btn_select'].'</a>&nbsp;&nbsp;
					    <a href="javascript:removeViewer()" class="sub_layer_link">'.$Lang['btn_remove'].'</a>
					  </td>
					</tr>
				  </table>';
	/*$CalMain .= '<!--
				  <div id="mail_form_content_board">
					<div id="added_item_ppl">
					  <ul>
					  	<li><a href="#"><span>Paul Day </span></a></li>
					  	<li><a href="#"><span>Paul Day</span></a></li>
						<li><a href="#"><span>Paul Day</span></a></li>
					  </ul>
					</div>
				  </div>
				  <br style="clear:both">
				  -->';*/
	# Event Type
	// if(Auth(array("Communication-ActiveEvent-CreateCompulEvent")) || Auth(array("Communication-ActiveEvent-CreateOptionalEvent")))
	//$CalMain .= $iCalendar_AddToGuest;
	$CalMain .= $Lang['iCalendar']['ParticipationOption'].': ';
	if($InviteStatus == "0" || $InviteStatus == "1"){
		$CalMain .= '<div class="new_event_sub_form_content">';
		$CalMain .= ($InviteStatus==0) ? $iCalendar_NewEvent_EventType_Compulsory : $iCalendar_NewEvent_EventType_Optional;
		$CalMain .= $lui->Get_Input_Hidden("InviteStatus", "InviteStatus", $InviteStatus);
		$CalMain .= '</div>';
	} else {
		$CalMain .= '<br>';
		// if(Auth(array("Communication-ActiveEvent-CreateCompulEvent")) && Auth(array("Communication-ActiveEvent-CreateOptionalEvent")) )
		// {
			# Both Events Type are valiable
			$other_para_compul = '';
			$other_para_option = '';
			$ActiveEventRemark = '';
		// } 
		// else if(Auth(array("Communication-ActiveEvent-CreateCompulEvent")) && !Auth(array("Communication-ActiveEvent-CreateOptionalEvent")) )
		// {
			// # Only Compulsory Event is allowed to create for the user
			// $other_para_compul = '';
			// $other_para_option = 'disabled';
			// $ActiveEventRemark = '<br>('.$i_Calendar_CompulsoryEvent_Remark.')';
			// $InviteStatus = 0;
		// }
		// else if(!Auth(array("Communication-ActiveEvent-CreateCompulEvent")) && Auth(array("Communication-ActiveEvent-CreateOptionalEvent")) )
		// {
			// # Only Optional Event is allowed to create for the user
			// $other_para_compul = 'disabled';
			// $other_para_option = '';
			// $ActiveEventRemark = '<br>('.$i_Calendar_OptionalEvent_Remark.')';
			// $InviteStatus = 1;
		// }
		
		$CalMain .= $lui->Get_Input_RadioButton("InviteStatus", "InviteStatus_0", 0, (($InviteStatus==0) ? true : false), $other_para_compul);
		$CalMain .= $iCalendar_NewEvent_EventType_Compulsory.'&nbsp;<br>';
		$CalMain .= $lui->Get_Input_RadioButton("InviteStatus", "InviteStatus_1", 1, (($InviteStatus==1) ? true : false), $other_para_option );
		$CalMain .= $iCalendar_NewEvent_EventType_Optional.'&nbsp;';
		$CalMain .= $ActiveEventRemark;
		
	} 
	$CalMain .= '</span>
			  
			  <div class="new_event_sub_form_content">
				<div style="line-height:5px">&nbsp;</div>
				<span>
					<a class="sub_layer_link" href="javascript:getCheckAvailableTimeForm()">'.$iCalendar_meeting_Check_Availability.'</a>
				</span>
				<!--
				<div>
					<span>
					<input type="radio" value="stricted" checked id="checkType1" name="checkType">'.$iCalendar_OfficeHR.'(7:00-21:00)
					</span>	<br>			
					<span>
					<input type="radio" value="selected" id="checkType2" name="checkType">'.$iCalendar_selectedTime.'
					</span>
					<span>
					<a class="sub_layer_link" href="javascript:getAvailableTimeslot()">'.$iCalendar_meeting_Check.'</a> 
					</span>
				</div>
				-->
				<div id="available_timeslot">
				</div>
			  </div>
			</div></span>';
}
}
// } // end if check User right to invit guest

# Share Count (Invite Guests Count)
if($editMode && $haveGuest && ($userAccess!="" || $calAccess != "")) {	
	$CalMain .= '<div id="new_event_invite_form" class="new_event_sub_form">';
	$CalMain .= $ShareGuestResponse;
	$CalMain .= '<span class="new_event_sub_form_title">'.$iCalendar_NewEvent_ShareCount.':<br>';
	$CalMain .= '<div class="new_event_sub_form_content">'.$ShareGuestCount.'</div></span>';
	$CalMain .= ($InviteStatus == 0) ? '<span class="new_event_sub_form_title">'.$iCalendar_NewEvent_ShareCountMsgA.':</span>' : '';
	$CalMain .= $ShareGuestList;
	$CalMain .= '</div>';
}
}
# Button
$CalMain .= '<br style="clear:both">
			<p class="dotline_p"></p>';
			
if ($plugin['eBooking']) {
	$CalMain .= '<div style="float:left;">';
		$CalMain .= '<span style="color:red; font-weight:string;"># </span><span class="tabletextremark">'.$i_Calendar_eBookingEvent_titleDescRemarks.'</span>';
	$CalMain .= '</div>';
	$CalMain .= '<br style="clear:both;" />';
}

$CalMain .= '<div id="form_btn">';
if($editMode && $isRepeatEvent) {
	$CalMain .= '<input type="hidden" name="saveRepeatEvent" value="OTI" />';
	$CalMain .= '<input type="hidden" name="deleteRepeatEvent" value="OTI" />';
}	


if($editMode) {
   //if($userAccess == 'A' || ($userAccess != 'A' && $CalType != 1) ){
	$CalMain .= $lui->Get_Input_Button("saveEvent", "saveEvent", $Lang['btn_save'], "class=\"button_main_act\"");
	$CalMain .= "&nbsp;";
   //}
	//$CalMain .= $lui->Get_Input_Button("applyEvent", "applyEvent", $i_Calendar_Apply, "class=\"button_main_act\"");
//	$CalMain .= $lui->Get_Reset_Button("Reset", "Reset", $Lang['btn_reset'], "class=\"button_main_act\"");
//	$CalMain .= "&nbsp;";
} else {
	$CalMain .= $lui->Get_Input_Button("saveEvent", "saveEvent", $Lang['btn_submit'], "class=\"button_main_act\"");
	$CalMain .= "&nbsp;";
}
//echo "guest".sizeof($guestName); 
$CalMain .= '<input id="guestNo" type="hidden" value="'.(sizeof($guestName)-1).'">';

if($editMode && (($userAccess == "A" || $userAccess == "W") || ($calAccess == "A" || $calAccess == "W"))&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID'])||!$editMode){
	if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF || $plugin["iCalendarFullToAllUsers"]){
		$CalMain .= $lui->Get_Input_Button("saveNsend", "saveNsend", $Lang['btn_submitAndSend'], "class=\"button_main_act\" ".(sizeof($guestName)>1?"":"disabled=\"true\""));
		$CalMain .= "&nbsp;";
		if($plugin['eClassTeacherApp']){
			$CalMain .= $lui->Get_Input_Button("submitAndSendPushMsg", "submitAndSendPushMsg", $Lang['iCalendar']['SubmitAndSendPushMessage'], "class=\"button_main_act\" ".(sizeof($guestName)>1?"":"disabled=\"true\""));
			$CalMain .= "&nbsp;";
		}
	}
}
$CalMain .= $lui->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button_main_act\" onclick=\"js_Delete_Temp_Booking_Record();\" ");
if($editMode && (($userAccess == "A" || $userAccess == "W") || ($calAccess == "A" || $calAccess == "W"))&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID']) ){
	$CalMain .= "&nbsp;";
	$CalMain .= $lui->Get_Input_Button("deleteEvent", "deleteEvent", $Lang['btn_delete'], "class=\"button_main_act\"");
}
$CalMain .= '</div>';
	  
$CalMain .= '
		  </div>
	    </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>';

# Assistance Tools
$CalMain .= '
<div class="jqmWindow" id="saveEventWarning">
	<p align="center">'.$iCalendar_NewEvent_Repeats_SaveMsg.'</p>
	<p align="center">'.$repeatSaveButtons.'</p>
</div>
<div class="jqmWindow" id="saveMultipleEventWarning">
	<p align="center" id="mulitpleEventWarningMsg"></p>
	<p align="center">'.$multipleRepeatButton.'</p>
</div>
<div class="jqmWindow" id="deleteEventWarning1">
	<p align="center">'.$iCalendar_DeleteEvent_ConfirmMsg1.'</p>
	<p align="center">'.$deleteEventButtons1.'</p>
</div>
<div class="jqmWindow" id="deleteEventWarning2">
	<p align="center">'.str_replace("#%EventTitle%#", $title, $iCalendar_DeleteEvent_ConfirmMsg2).'</p>
	<p align="center">'.$deleteEventButtons2.'</p>
</div>
<div class="jqmWindow" id="eventConflictWarning">
	<p align="center">'.$conflictBtn.'</p>
</div>';
	
# Hidden Values
if (isset($calAccess)) {
	$CalMain .= "<input type='hidden' name='calAccess' value='$calAccess' />";
	$CalMain .= "<input type='hidden' name='access' value='P' />";
	$CalMain .= "<input type='hidden' name='createdBy' value='$createdBy' />";
	$CalMain .= "<input type='hidden' name='response' id='response' value='A' />";
}
if ($editMode && $isRepeatEvent) {
	$CalMain .= "<input type='hidden' name='isRepeatEvent' value='1' />";
	$CalMain .= "<input type='hidden' name='repeatID' value='$repeatID' />";
}
$CalMain .= ($editMode) ? "<input type='hidden' name='eventID' value='$eventID' />" : "";
$CalMain .= "<input type='hidden' name='submitMode' value='' />";
$CalMain .= "<input type='hidden' name='CalType' id='CalType' value='$CalType' />";
$CalMain .= "<input type='hidden' name='ProgrammeID' id='ProgrammeID' value='$programmeID' />";
$CalMain .= "<input type='hidden' name='otherSchoolCode' id='otherSchoolCode' value='$otherSchoolCode' />";
$CalMain .= '</form>';
	

# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain, '','','','','',false,false);


# Choose CSS style sheet
$styleSheet = "css/display_calendar.css";

?>

<link rel="stylesheet" href="css/main.css" type="text/css" />
<link rel="stylesheet" href="<?=$styleSheet?>" type="text/css" />

<?php
# Start outputing the layout after CSS is loaded to prevent "FOUC"
// include_once($PathRelative."src/include/template/general_header.php");
include_once("headerInclude.php");
echo '<script>';
echo 'var SessionExpiredWarning = "' . $Lang['Warning']['SessionExpired'] . '";';
echo 'var SessionKickedWarning = "' . $Lang['Warning']['SessionKicked'] . '";';
echo 'var SessionExpiredMsg = "' . $SYS_CONFIG['SystemMsg']['SessionExpired'] . '";';
echo 'var SessionKickedMsg = "' . $SYS_CONFIG['SystemMsg']['SessionKicked'] . '";';
echo '</script>';

?>
<!--
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/dimensions.js"></script>

<script type="text/javascript" src="js/jqModal.js"></script>
<link rel="stylesheet" type="text/css" href="css/jqModal.css" />


<script type="text/javascript" src="cluetip/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="cluetip/jquery.cluetip.js"></script>
<style type="text/css">@import url(cluetip/jquery.cluetip.css);</style>

<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>
-->
<script language="javascript">
// Fix for IE6 image flicker (cannot solve the case of ":hover")
try {
  document.execCommand("BackgroundImageCache", false, true);
} catch(err) {}

function initMenu() {
	$('#groupGuestWaitList ul').hide();
	$('#groupGuestWaitList li a').click(
		function() {
			$(this).next().slideToggle('normal');
			return false;
		}
	);
}

function getAvailableTimeslot(){
	dateTobeCheck = document.getElementById("eventDate").value;
	startTime = '';
	endTime = '';
	if (!document.getElementById('isAllDay').checked){
		startHr = parseInt(document.getElementById("eventHr").value);
		startMin = document.getElementById("eventMin").value;
		startTime = startHr<10? '0'+startHr+':'+startMin+':00':startHr+':'+startMin+':00';
		
		endHr = parseInt(document.getElementById("eventEndHr").value);
		endMin = document.getElementById("eventEndMin").value;
		endTime = endHr<10? '0'+endHr+':'+endMin+':00':endHr+':'+endMin+':00';
	}
	user = $('select#viewerList > option').get();
	
	existGuest = $("input.existGuestList:hidden").get();
	removeGuestList = $("input.removeGuest:hidden").get(); 
	checkType = $("input[@name='checkType']:checked").get();
	
	requestStr = "dateTobeCheck="+dateTobeCheck;
	requestStr += "&startTime="+startTime;
	requestStr += "&endTime="+endTime;
	
	//userExist = false;
	cnt = 0;
	for (j = 0; j < removeGuestList.length; j++){
		if (removeGuestList[j].value != -1){
			cnt++;
		}
	}
	
	
	if (user.length == 0 && (existGuest.length == 0 || existGuest.length-1 == cnt)){
		document.getElementById("available_timeslot").innerHTML = "<?php echo $iCalendar_meeting_Reminder; ?>"; 
		return;
	}
	for (i = 0; i < user.length; i++){
		requestStr += ("&user[]="+user[i].value);
	}
	for (i = 0; i < existGuest.length; i++){
		requestStr += ("&existGuest[]="+existGuest[i].value);
	}
	for (i = 0; i < removeGuestList.length; i++){
		requestStr += ("&removeGuest[]="+removeGuestList[i].value);
	}
	requestStr += ("&checkType="+checkType[0].value);
	
	dataBlock = document.getElementById("available_timeslot");
	$.ajax({
		type: "POST",
		url: "getAvailableTimeslot.php",
		cache: false,
		data: requestStr,
		success: function(html){
			dataBlock.innerHTML = html;			
		}
	});
	
	
}


function toggleCalVisibility(parClassID) {
	//Reset_Timeout();
	//$('.cal-'+parClassID).toggle();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: parClassID
	}, function(responseText) {
	    // callback function
		responseText = responseText.replace("\n", "");
		if (!SessionExpired1(responseText)){ }
	});
}


function toggleExternalCalVisibility(selfName,eventClass,CalType){
	//toggleCalVisibility(eventClass,selfName);
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calType : CalType		
	}, function(responseText) {
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	// isDisplayMoreFunction();
	// jHide_All_Layers();
}

function toggleOwnDefaultCalVisibility(parClassID){
	toggleCalVisibility(parClassID);
	
	var obj = document.getElementById("toggleCalVisible-"+parClassID);
	var involObj = document.getElementById("toggleVisible");
	if(obj.checked == true){
		if(involObj.checked == false)
			toggleInvolveEventVisibility();
		involObj.checked = true;
	} else {
		if(involObj.checked == true)
			toggleInvolveEventVisibility();
		involObj.checked = false;
	}	
}

function toggleInvolveEventVisibility(){
	//Reset_Timeout();
	$(".involve").toggle();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: 'involve'
	}, function(responseText) {
	    // callback function
		responseText = responseText.replace("\n", "");
		if (!SessionExpired1(responseText)){ }
	});
}

function toggleSchoolEventVisibility(){
	//Reset_Timeout();
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: 'school'
	}, function(responseText) {
	    // callback function
		responseText = responseText.replace("\n", "");
		if (!SessionExpired1(responseText)){ }
	});
}

function Set_CheckBox(objName, checked){
	var obj = document.getElementsByName(objName);
	if(obj.length > 0){
		for(var i=0 ; i<obj.length ; i++){
			obj[i].checked = (checked == 1) ? true : false;
		}
	}
}

function toggleFoundationEventVisibility(obj){
	//Reset_Timeout();
	$('.foundation').toggle();
	var checked = (obj.checked == true) ? 1 : 0;
	Set_CheckBox('toggleFoundationCalVisible[]', checked);
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: 'foundation'
	}, function(responseText) {
	    // callback function
		if (!SessionExpired(responseText)){ }
	});
	
	isDisplayMoreFunction();
	jHide_All_Layers();
}

function checkOptionAll(obj){
    checkOption(obj);
    for(i=0; i<obj.length; i++){
    	obj.options[i].selected = true;
    }
}

function checkOption(obj){
    for(i=0; i<obj.length; i++){
        if(obj.options[i].value== ''){
            obj.options[i] = null;
        }
    }
}

function checkOptionRemove(obj){
    checkOption(obj);
    i = obj.selectedIndex;
    while(i!=-1){
        obj.options[i] = null;
        i = obj.selectedIndex;
	}
}

function jInvite_Guest(eventID){
	//window.open('choose/choose_event_guest.php?fieldname=viewer[]&eventID='+eventID,'','scrollbars=yes,width=350,height=350');
	window.open('choose/new/index.php?pageType=NewEvent&fieldname=viewer[]&eventID='+eventID,'','scrollbars=yes,width=500,height=500');
}

function removeViewer(){
	checkOptionRemove(document.form1.elements["viewer[]"]);
	guestList = $('select#viewerList > option').get();
	guestNo = parseInt(document.getElementById("guestNo").value);
	// alert(guestList.length);
	// alert(guestNo);
	// alert(guestList.length+guestNo);
	if (guestList.length+guestNo <= 0){
		document.getElementById("saveNsend").disabled = true;
	}
	var pushMsgBtn = document.getElementById("submitAndSendPushMsg");
	if(pushMsgBtn){
		pushMsgBtn.disabled = guestList.length+guestNo <= 0;
	}
}

function enableTimeSelect() {
	x = document.getElementById("isAllDay");
	
	if (js_Event_Have_Booking_Already()) {
		alert('<?=$i_Calendar_eBookingEvent_cancelBookingFirstWarning?>');
		x.checked = !x.checked;
	}
	else {
		if (x.checked){
			document.form1.eventHr.disabled = true;
			document.form1.eventMin.disabled = true;
			document.form1.eventEndHr.disabled = true;
			document.form1.eventEndMin.disabled = true;
		}
		else{
			document.form1.eventHr.disabled = false;
			document.form1.eventMin.disabled = false;
			document.form1.eventEndHr.disabled = false;
			document.form1.eventEndMin.disabled = false;
		}
		
		
		/* old version
		if (document.getElementById("is_allday_1").checked) {
			document.form1.eventHr.disabled = false;
			document.form1.eventMin.disabled = false;
			document.form1.durationHr.disabled = false;
			document.form1.durationMin.disabled = false;
		}
		
		if (document.getElementById("is_allday_0").checked) {
			document.form1.eventHr.disabled = true;
			document.form1.eventMin.disabled = true;
			document.form1.durationHr.disabled = true;
			document.form1.durationMin.disabled = true;
		}*/
	}
}

function removeReminder(reminder) {
	var divReminder = reminder.parentNode.parentNode;
	var spanToRemove = reminder.parentNode;
	
	var removed = divReminder.removeChild(spanToRemove);
	var spans = divReminder.getElementsByTagName("span");
	
	if (spans.length == 1) {
		document.getElementById("noReminderMsg").style.display = "block";
	}
	
	if (spans.length <= (<?=$ReminderLimit?>*2-1)) {
		document.getElementById("addReminderSpan").style.display = "block";
	}
}

function addReminder() {
	var divReminder = document.getElementById("reminderDiv");
	document.getElementById("noReminderMsg").style.display = "none";
	
	var spans = divReminder.getElementsByTagName("span");
	if (spans.length == (<?=$ReminderLimit?>*2-1))
		document.getElementById("addReminderSpan").style.display = "none";
	
	var newSpan = "<span>";
	newSpan += "<?=str_replace("\n", "", $iCal->generateReminderSelect())?>";
	newSpan += "&nbsp;<span onclick=\"removeReminder(this)\"><a href=\"javascript:void(0)\" class=\"sub_layer_link\"><?=$iCalendar_NewEvent_Reminder_Remove?></a></span> <br />";
	newSpan += "</span>";
	
	divReminder.innerHTML += newSpan;
}
<?php
	if ($iCal->systemSettings["DisableRepeat"] == "no" || $isRepeatEvent) {
?>
var repeatSelectIndex; 
function showRepeatCtrl() {
	if(document.getElementsByName('repeatEnd')[0].value < document.getElementsByName('repeatStart')[0].value){
		document.getElementsByName('repeatEnd')[0].value = document.getElementsByName('repeatStart')[0].value;
	}
	
	if (js_Event_Have_Booking_Already()) {
		alert('<?=$i_Calendar_eBookingEvent_cancelBookingFirstWarning?>');
		$('#repeatEnd').val(oldValueAry['repeatEnd']);
	}
	else {
		var repeatMsgTable = document.getElementById("repeatMsgTable");
		var repeatMsgSpan = document.getElementById("repeatMsgSpan");
		var repeatSelect = document.getElementById("repeatSelect");
		
		var repeatCtrl = document.getElementById("repeatCtrl");
		var repeatCtrlDaily = document.getElementById("repeatCtrlDaily");
		var repeatCtrlWeekly = document.getElementById("repeatCtrlWeekly");
		var repeatCtrlMonthly = document.getElementById("repeatCtrlMonthly");
		var repeatCtrlYearly = document.getElementById("repeatCtrlYearly");
		var repeatRange = document.getElementById("repeatRange");
		
		if (repeatSelectIndex != document.getElementById("repeatSelect").selectedIndex){
			startRepeat = document.form1.eventDate.value;
			startPieces = startRepeat.split('-');
			today = new Date();
			today.setFullYear(startPieces[0]);
			today.setDate(startPieces[2]);
			today.setMonth(parseInt(startPieces[1],10));
			today.setMonth(parseInt(startPieces[1],10)+1);
			fulldate = today.toDateString();
			datePieces = fulldate.split(' ');
			monthName = Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
			monthInNumber = "";
			for (i = 0; i < monthName.length; i++){
				if (monthName[i] == datePieces[1]){
					monthInNumber = (((i+1)<10)? '0'+(i+1):(i+1));
					break;
				}
			}
			
			document.form1.repeatEnd.value = datePieces[3]+"-"+monthInNumber+"-"+datePieces[2];
			repeatSelectIndex = document.getElementById("repeatSelect").selectedIndex;
		}
		
		document.form1.repeatStart.value = document.form1.eventDate.value;
		
		
		
		
		var repeatSelected = repeatSelect.options[repeatSelect.selectedIndex].value;
		
		/*if (repeatSelect.selectedIndex == 0){
			
			document.getElementById("eventEndDate").disabled = false;
			other = $('input#eventEndDate').siblings().get();
			other[0].style.visibility = "visible";
		}
		else{
		
			
			document.getElementById("eventEndDate").disabled = true;
			other = $('input#eventEndDate').siblings().get(); 
			other[0].style.visibility = "hidden";
		
		
		}*/
		
		
		switch(repeatSelected)
		{
		case "NOT":
			repeatCtrlDaily.style.display = "none";
			repeatCtrlWeekly.style.display = "none";
			repeatCtrlMonthly.style.display = "none";
			repeatCtrlYearly.style.display = "none";
			
			repeatRange.style.display = "none";
			repeatMsgTable.style.display = "none";
			break;
		case "DAILY":
			repeatCtrlWeekly.style.display = "none";
			repeatCtrlMonthly.style.display = "none";
			repeatCtrlYearly.style.display = "none";
			
			dailyCtrl = "<span class=\"cssform\" style=\"padding:10px\"><?=$iCalendar_NewEvent_Repeats_Every?></span>";
			dailyCtrl += "<span class=\"cssform\"><select id='repeatDailyDay' name='repeatDailyDay' onChange='changeRepeatMsg()'>";
			dailyCtrl += "<option value='1'>1 <?=$iCalendar_NewEvent_DurationDay?></option>";
			for (i=2; i<=14; i++)
				dailyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationDays?></option>";
			dailyCtrl += "</select></span>";
			
			if (repeatCtrlDaily.innerHTML=="") {
				repeatCtrlDaily.innerHTML = dailyCtrl;
			}
			repeatCtrlDaily.style.display = "block";
			repeatRange.style.display = "block";
			
			changeRepeatMsg();
			break;
		case "WEEKDAY":
			repeatCtrlDaily.style.display = "none";
			repeatCtrlWeekly.style.display = "none";
			repeatCtrlMonthly.style.display = "none";
			repeatCtrlYearly.style.display = "none";
			
			changeRepeatMsg();
			repeatRange.style.display = "block";
			break;
		case "MONWEDFRI":
			repeatCtrlDaily.style.display = "none";
			repeatCtrlWeekly.style.display = "none";
			repeatCtrlMonthly.style.display = "none";
			repeatCtrlYearly.style.display = "none";
			
			changeRepeatMsg();
			repeatRange.style.display = "block";
			break;
		case "TUESTHUR":
			repeatCtrlDaily.style.display = "none";
			repeatCtrlWeekly.style.display = "none";
			repeatCtrlMonthly.style.display = "none";
			repeatCtrlYearly.style.display = "none";
			
			changeRepeatMsg();
			repeatRange.style.display = "block";
			break;
		case "WEEKLY":
			weeklyCtrl = "<span class=\"cssform\" style=\"padding:10px\"><?=$iCalendar_NewEvent_Repeats_Every?></span>";
			weeklyCtrl += "<span class=\"cssform\"><select id='repeatWeeklyRange' name='repeatWeeklyRange' onChange='changeRepeatMsg()'>";
			weeklyCtrl += "<option value='1'>1 <?=$iCalendar_NewEvent_DurationWeek?></option>";
			for (i=2; i<=14; i++)
				weeklyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationWeeks?></option>";
			weeklyCtrl += "</select></span>";
			weeklyCtrl += "<br />";
			weeklyCtrl += "<span class=\"cssform\" style=\"padding:10px\"><?=$iCalendar_NewEvent_Repeats_On?></span><br />";
			weeklyCtrl += "<span class=\"cssform\">&nbsp;</span>";
			<?
				// Bug Fix(19/12/2007): Use English name for weekday checkbox name under all UI language
				$tempWeekDayArray = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
				for($i=0; $i<sizeof($tempWeekDayArray); $i++) {
			?>
			weeklyCtrl += "<input type='checkbox' name='<?=$tempWeekDayArray[$i]?>' id='<?=$tempWeekDayArray[$i]?>' onClick='changeRepeatMsg()' <?=($tempWeekDayArray[$i]==date("D"))?"CHECKED":""?> /> ";
			//weeklyCtrl += "<span class=\"cssform\"><label for='<?=$tempWeekDayArray[$i]?>'><?=$iCalendar_NewEvent_Repeats_WeekdayArray[$i]?></label></span>&nbsp;&nbsp;";
			weeklyCtrl += "<label for='<?=$tempWeekDayArray[$i]?>'><span class=\"cssform\" style=\"padding:0px\"><?=$iCalendar_NewEvent_Repeats_WeekdayArray[$i]?></span></label>&nbsp;&nbsp;";
			<?
				}
			?>
			repeatCtrlDaily.style.display = "none";
			repeatCtrlMonthly.style.display = "none";
			repeatCtrlYearly.style.display = "none";
			
			if (repeatCtrlWeekly.innerHTML=="") {
				repeatCtrlWeekly.innerHTML = weeklyCtrl;
			}
			repeatCtrlWeekly.style.display = "block";
			repeatRange.style.display = "block";
			
			changeRepeatMsg();
			break;
		case "MONTHLY":
			monthlyCtrl = "<span class=\"cssform\" style=\"padding:10px\"><?=$iCalendar_NewEvent_Repeats_Every?></span>";
			monthlyCtrl += "<span class=\"cssform\"><select id='repeatMonthlyRange' name='repeatMonthlyRange' onClick='changeRepeatMsg()'>";
			monthlyCtrl += "<option value='1'>1 <?=$iCalendar_NewEvent_DurationMonth?></option>";
			for (i=2; i<=14; i++)
				monthlyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationMonths?></option>";
			monthlyCtrl += "</select></span>";
			monthlyCtrl += "<br />";
			monthlyCtrl += "<span class=\"cssform\" style=\"padding:10px\"><?=$iCalendar_NewEvent_Repeats_By?><br /></span>";
			monthlyCtrl += "<span class=\"cssform\">&nbsp;</span>";
			monthlyCtrl += "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy0' onClick='changeRepeatMsg()' value='dayOfMonth' CHECKED> ";
			monthlyCtrl += "<label for='monthlyRepeatBy0'><span class=\"cssform\" style=\"padding:0px\"><?=$iCalendar_NewEvent_Repeats_Monthly3?></span></label>";
			monthlyCtrl += "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy1' onClick='changeRepeatMsg()' value='dayOfWeek'> ";
			monthlyCtrl += "<label for='monthlyRepeatBy1'><span class=\"cssform\" style=\"padding:0px\"><?=$iCalendar_NewEvent_Repeats_Monthly4?></span></label>";
			
			repeatCtrlDaily.style.display = "none";
			repeatCtrlWeekly.style.display = "none";
			repeatCtrlYearly.style.display = "none";
			
			if (repeatCtrlMonthly.innerHTML=="") {
				repeatCtrlMonthly.innerHTML = monthlyCtrl;
			}
			repeatCtrlMonthly.style.display = "block";
			repeatRange.style.display = "block";
			
			changeRepeatMsg();
			break;
		case "YEARLY":
			repeatCtrlDaily.style.display = "none";
			repeatCtrlWeekly.style.display = "none";
			repeatCtrlMonthly.style.display = "none";
			
			yearlyCtrl = "<span class=\"cssform\" style=\"padding:10px\"><?=$iCalendar_NewEvent_Repeats_Every?></span";
			yearlyCtrl += "<span class=\"cssform\"><select id='repeatYearlyRange' name='repeatYearlyRange' onClick='changeRepeatMsg()'>";
			yearlyCtrl += "<option value='1'>1 <?=$iCalendar_NewEvent_DurationYear?></option>";
			for (i=2; i<=14; i++)
				yearlyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationYears?></option>";
			yearlyCtrl += "</select></span>";
			yearlyCtrl += "<br />";
			
			if (repeatCtrlYearly.innerHTML=="") {
				repeatCtrlYearly.innerHTML = yearlyCtrl;
			}
			repeatCtrlYearly.style.display = "block";
			repeatRange.style.display = "block";
			
			changeRepeatMsg();
			break;
		}
		
		oldValueAry['repeatEnd'] = $('#repeatEnd').val();
	}
}

function changeRepeatMsg() {
	if (js_Event_Have_Booking_Already()) {
		alert('<?=$i_Calendar_eBookingEvent_cancelBookingFirstWarning?>');
		$('#repeatDailyDay').val(oldValueAry['repeatDailyDay']);
	}
	else {
		var repeatSelect = document.getElementById("repeatSelect");
		var repeatMsgSpan = document.getElementById("repeatMsgSpan");
		var repeatMsgTable = document.getElementById("repeatMsgTable");
		var repeatSelected = repeatSelect.options[repeatSelect.selectedIndex].value;
		var eventDate = document.form1.eventDate.value;
		var eventDatePieces = eventDate.split("-");
		var eventDateObj = new Date();
		eventDateObj.setFullYear(eventDatePieces[0],eventDatePieces[1]-1,eventDatePieces[2]);
		
		switch(repeatSelected)
		{
		case "DAILY":
			if (document.form1.repeatDailyDay.value==1)
				newMsg = "<?=$iCalendar_NewEvent_Repeats_Daily?>";
			else
				newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>" + document.form1.repeatDailyDay.value + " <?=$iCalendar_NewEvent_DurationDays?>";
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
			repeatMsgSpan.innerHTML = newMsg;
			repeatMsgTable.style.display = "block";
			break;
		case "WEEKDAY":
			newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryWeekday2?>";
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
			repeatMsgSpan.innerHTML = newMsg;
			repeatMsgTable.style.display = "block";
			break;
		case "MONWEDFRI":
			newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryMonWedFri2?>";
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
			repeatMsgSpan.innerHTML = newMsg;
			repeatMsgTable.style.display = "block";
			break;
		case "TUESTHUR":
			newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryTuesThur2?>";
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
			repeatMsgSpan.innerHTML = newMsg;
			repeatMsgTable.style.display = "block";
			break;
		case "WEEKLY":
			if (document.form1.repeatWeeklyRange.value == "1") {
				newMsg = "<?=$iCalendar_NewEvent_Repeats_Weekly2?>";
				checkSame = newMsg;
			} else {
				newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatWeeklyRange.value+"<?=$iCalendar_NewEvent_Repeats_Weekly3?>";
				checkSame = newMsg;
			}
			
			<?
				for($i=0; $i<sizeof($tempWeekDayArray); $i++) {
			?>
			if (document.form1.<?=$tempWeekDayArray[$i]?>.checked) {
				if (newMsg.match("day"))
					newMsg += ", <?=$iCalendar_NewEvent_Repeats_WeekdayArray2[$i]?>";
				else
					newMsg += "<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[$i]?>";
			}
			<?
				}
			?>
			var weekDayName = new Array("<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[0]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[1]?>",
							"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[2]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[3]?>",
							"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[4]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[5]?>",
							"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[6]?>");
			if (checkSame == newMsg)
				newMsg += weekDayName[eventDateObj.getDay()];
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
			repeatMsgSpan.innerHTML = newMsg;
			repeatMsgTable.style.display = "block";
			break;
		case "MONTHLY":
			var monthlyRepeatBy0 = document.getElementById("monthlyRepeatBy0");
			var monthlyRepeatBy1 = document.getElementById("monthlyRepeatBy1");
			
			if (document.form1.repeatMonthlyRange.value == "1")
				newMsg = "<?=$iCalendar_NewEvent_Repeats_Monthly2?>";
			else
				newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatMonthlyRange.value+" <?=$iCalendar_NewEvent_Repeats_Monthly5?>";
			
			if (monthlyRepeatBy0.checked){
				var range = document.form1.repeatMonthlyRange.value;
				range = range==1?'':range;
				newMsg = '<?=$iCalendar_NewEvent_Repeats_Monthly_general?>'.replace("%N%",range).replace("%D%",eventDateObj.getDate()+getDateSuffix(eventDateObj.getDate()));
			}
			if (monthlyRepeatBy1.checked) {
				var nth = 0;
				for(var j=1; j<6; j++) {
					if (eventDateObj.getDate() == NthDay(j, eventDateObj.getDay(), eventDateObj.getMonth(), eventDateObj.getFullYear())) {
						nth = j;
						break;
					}
				}
				
				var daysofmonth   = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
				var daysofmonthLY = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
				
				if (isLeapYear(eventDateObj.getFullYear()))
					var days = daysofmonthLY[eventDateObj.getMonth()];
				else
					var days = daysofmonth[eventDateObj.getMonth()];
				if (days - eventDateObj.getDate() < 7)
					nth = 6;
				
				var weekDayName = new Array("<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[0]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[1]?>",
							"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[2]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[3]?>",
								"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[4]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[5]?>",
								"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[6]?>");
				var countName =  new Array("<?=$iCalendar_NewEvent_Repeats_Count[0]?>", "<?=$iCalendar_NewEvent_Repeats_Count[1]?>", 
								"<?=$iCalendar_NewEvent_Repeats_Count[2]?>", "<?=$iCalendar_NewEvent_Repeats_Count[3]?>", 
								"<?=$iCalendar_NewEvent_Repeats_Count[4]?>", "<?=$Lang['iCalendar']['TheLast']?>");
				newMsg += countName[nth-1] + " " + weekDayName[eventDateObj.getDay()];
			}
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
			repeatMsgSpan.innerHTML = newMsg;
			repeatMsgTable.style.display = "block";
			break;
		case "YEARLY":
			if (document.form1.repeatYearlyRange.value == "1")
				newMsg = "<?=$iCalendar_NewEvent_Repeats_Yearly2?>";
			else
				newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatYearlyRange.value+" <?=$iCalendar_NewEvent_Repeats_Yearly3?>";
			var monthname = new Array("<?=$iCalendar_NewEvent_Repeats_MonthArray[0]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[1]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[2]?>",
							"<?=$iCalendar_NewEvent_Repeats_MonthArray[3]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[4]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[5]?>",
							"<?=$iCalendar_NewEvent_Repeats_MonthArray[6]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[7]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[8]?>",
							"<?=$iCalendar_NewEvent_Repeats_MonthArray[9]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[10]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[11]?>");
			newMsg += monthname[eventDateObj.getMonth()]+" "+eventDateObj.getDate();
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
			repeatMsgSpan.innerHTML = newMsg;
			repeatMsgTable.style.display = "block";
			break;
		}
		
		oldValueAry['repeatDailyDay'] = $('#repeatDailyDay').val();
	}
}
<?php
	}
?>

function getDateSuffix(date){
	if (date == 1 || date == 21 || date == 31)
		return '<?=$i_Calendar_firstDay?>';
	if (date == 2 || date == 22)
		return '<?=$i_Calendar_secondDay?>';
	if (date == 3)
		return '<?=$i_Calendar_thirdDay?>';
	return  '<?=$i_Calendar_otherDay?>';
}

function dayOfWeek(day,month,year) {
    var dateObj = new Date();
    dateObj.setFullYear(year,month,day);
    return dateObj.getDay();
}

function isLeapYear(year) {
  return new Date(year,2-1,29).getDate()==29;
}

function NthDay(nth,weekday,month,year) {
	var daysofmonth   = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var daysofmonthLY = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    if (nth > 0)
    	return (nth-1)*7 + 1 + (7+weekday-dayOfWeek((nth-1)*7+1, month, year))%7;
    if (isLeapYear(year))
    	var days = daysofmonthLY[month]; 
    else
    	var days = daysofmonth[month];
    return days - (dayOfWeek(days,month,year) - weekday + 7)%7;
}

InitialGuestNo = <?=sizeof($guestName)-1?>;
function removeGuest(spanElement,guestUserID,forceChange) {
	if (forceChange == "Remove All") {
		spanElement.innerHTML = "<a class=\"sub_layer_link\" href=\"javascript:void(0)\"><?=$iCalendar_NewEvent_Reminder_Remove?></a>";
		spanElement.parentNode.className = "undoRemovedGuest";
		document.getElementById("removeGuest-"+guestUserID).value = -1;
		guestNo = document.getElementById("guestNo");
		guestNo.value = 0;
	} else if (forceChange == "Undo All") {
		spanElement.innerHTML = "<a class=\"sub_layer_link\" href=\"javascript:void(0)\"><?=$i_Calendar_Undo?></a>";
		spanElement.parentNode.className = "removedGuest";
		document.getElementById("removeGuest-"+guestUserID).value = guestUserID;
		guestNo = document.getElementById("guestNo");
		guestNo.value = InitialGuestNo;
	} else {
		if (spanElement.parentNode.className == "" || spanElement.parentNode.className == "undoRemovedGuest") {
			spanElement.innerHTML = "<a class=\"sub_layer_link\" href=\"javascript:void(0)\"><?=$i_Calendar_Undo?></a>";
			spanElement.parentNode.className = "removedGuest";
			document.getElementById("removeGuest-"+guestUserID).value = guestUserID;
			guestNo = document.getElementById("guestNo");
			guestNo.value = parseInt(guestNo.value)-1;
		} else {
			spanElement.innerHTML = "<a class=\"sub_layer_link\" href=\"javascript:void(0)\"><?=$iCalendar_NewEvent_Reminder_Remove?></a>";
			spanElement.parentNode.className = "undoRemovedGuest";
			document.getElementById("removeGuest-"+guestUserID).value = -1;
			guestNo = document.getElementById("guestNo");
			guestNo.value = parseInt(guestNo.value)+1;
		}
	}
	/*id = "#removeGuest-"+guestUserID;
	hiddenInput = $(id).get();
	if (hiddenInput[0].value == -1){
		hiddenInput[0].value = guestUserID;
		hiddenNext = $(id).next().get();
		hiddenNext[0].innerHTML = "<a class=\"sub_layer_link\" href=\"javascript:void(0)\"><?=$i_Calendar_Undo?></a>";
		guestNo = $("#guestNo").get();
		guestNo[0].value = parseInt(guestNo[0].value)-1;
	}
	else{
		hiddenInput[0].value = "-1";
		hiddenNext = $(id).next().get();
		hiddenNext[0].innerHTML = "<a class=\"sub_layer_link\" href=\"javascript:void(0)\"><?=$iCalendar_NewEvent_Reminder_Remove?></a>";
		guestNo = $("#guestNo").get();
		guestNo[0].value = parseInt(guestNo[0].value)+1;
	}*/
	
	guestList = $('select#viewerList > option').get();
	guestNo = parseInt(document.getElementById("guestNo").value);
	if (guestList.length+guestNo==0){
		document.getElementById("saveNsend").disabled = true;
	}
	else{
		document.getElementById("saveNsend").disabled = false;
	} 
	var pushMsgBtn = document.getElementById('submitAndSendPushMsg');
	if(pushMsgBtn){
		pushMsgBtn.disabled = guestList.length+guestNo <= 0;
	}
}

function removeGroup(removeAllSpan) {
	if ($(removeAllSpan).html() == '<?=$i_Calendar_UndoAll?>') {
		$(removeAllSpan).html('<?=$i_Calendar_RemoveAll?>');
	} else {
		$(removeAllSpan).html('<?=$i_Calendar_UndoAll?>');
	}
	var allGuestLi = $(removeAllSpan).parent().siblings('li');
	for (var i=0; i<allGuestLi.size(); i++) {
		guestSpan = $(allGuestLi).eq(i).children('span').get();
		guestID = $(allGuestLi).eq(i).children('.existGuestList').attr('value');
		removeGuest(guestSpan[0], guestID, $(removeAllSpan).html());
	}
}

$.validator.addMethod("yyyymmdd", function(value) {
	return /(19[0-9][0-9]|20[0-9][0-9])-([1-9]|0[1-9]|1[012])-([1-9]|0[1-9]|[12][0-9]|3[01])/.test(value);
}, '<?=$iCalendar_CheckForm_DateWrong?>');

function validateForm() {
	if($("input[@name='url']").val() == "http://")
		$("input[@name='url']").val(""); 
	
	var v = $("#form1").validate({
		errorLabelContainer: $("#errMessageBox"),
		wrapper: "li",
		rules: {
			title: "required",
			eventDate: {
				required: true,
				yyyymmdd: true,
				minLength: 8,
				maxLength: 10
			},
			url: {
				url: true
			},
			repeatEnd: {
				required: function() {
					return $("input[@name='repeatSelect']").val() != "NOT";
				},
				yyyymmdd: true
			}
		},
		messages: {
			title: "<?=$iCalendar_CheckForm_NoTitle?>",
			eventDate: {
				required: "<?=$iCalendar_CheckForm_NoDate?>",
				minLength: "<?=$iCalendar_CheckForm_DateWrong?>",
				maxLength: "<?=$iCalendar_CheckForm_DateWrong?>"
			},
			url: {
				url: "<?=$iCalendar_CheckForm_UrlWrong?>"
			},
			repeatEnd: {
				required: "<?=$iCalendar_CheckForm_NoRepeatEnd?>"	
			}
		}
	});
	
	if(v.form()){
	<?php 
		if (!$editMode || (($userAccess == "A" || $userAccess == "W") || ($calAccess == 'W' || $calAccess == 'A'))&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID'])){
			echo 'startDate = document.getElementById("eventDate").value;
			startPieces = startDate.split("-");
			endDate = document.getElementById("eventEndDate").value; 
			endPieces = endDate.split("-");
			repeatType = document.getElementById("repeatSelect").selectedIndex;
			if (Date.UTC(startPieces[0],startPieces[1],startPieces[2]) != Date.UTC(endPieces[0],endPieces[1],endPieces[2]) && repeatType != 0){
				dateDiff = (Date.UTC(endPieces[0],endPieces[1],endPieces[2])-Date.UTC(startPieces[0],startPieces[1],startPieces[2]))/(1000*60*60*24)+1;
				testmsg = "'.$iCalendar_OverlapWarning.'"
				testmsg = testmsg.replace(/%numDay%/g,dateDiff);
				//"The event is "+dateDiff+" days long and repeated. Are you sure you want make "+dateDiff+" days long repeated event?";
				x = $("#saveMultipleEventWarning p:first").text(testmsg);
				//x[0].innerHTML = "hishihsishishis";
				$("#saveMultipleEventWarning").jqmShow();
			
			}
		else';}
		?> if (document.form1.isRepeatEvent) {			
			$("#saveEventWarning").jqmShow();
		} else {
			var blockMsg = "<h2 style='padding-top:10px;'><?=$i_Calendar_Loading?></h2>";
			if(jQuery_1_3_2){
				(function($){
					$.blockUI({message: blockMsg, overlayCSS: { color:'#ccc' ,border:'2px solid #ccc' }});
				})(jQuery_1_3_2);
			}else{
				$.blockUI(blockMsg, { color:'#ccc' ,border:'2px solid #ccc' });
			}
			limitedEdit = false;
		<?php 
		if ( $calAccess == 'R' || ($plugin["iCalendarEditByOwner"] && $createdBy!=$_SESSION['UserID']) || $CalType==1 || $CalType>3)
		echo 'limitedEdit = true;';
		?>
		if (!limitedEdit){
			startDate = document.getElementById("eventDate").value;
			startPieces = startDate.split("-");
			endDate = document.getElementById("eventEndDate").value;
			endPieces = endDate.split("-");
			isMultiple = Date.UTC(startPieces[0],startPieces[1],startPieces[2]) != Date.UTC(endPieces[0],endPieces[1],endPieces[2]);
		}
			if(jQuery_1_3_2){
				(function($){
					$.blockUI({message: blockMsg, overlayCSS: { color:'#ccc' ,border:'2px solid #ccc' }});
				})(jQuery_1_3_2);
			}else{
				$.blockUI(blockMsg, { color:'#ccc' ,border:'2px solid #ccc' });
			}	
			if ($("input[@name='isAllDay']:checked").val() != 1 && !limitedEdit&& !isMultiple ) {
				var startTime = $("[@name='eventHr']").val()+":"+$("[@name='eventMin']").val();
				// var durationHr = $("[@name='durationHr']").val();
				// var durationMin = $("[@name='durationMin']").val();
				// var duration = (durationMin*1) + (durationHr*60);
				var duration = getEventDuration(); 
				var oldEventDate = $("input[@name='oldEventDate']").val();
				var oldEventTime = $("input[@name='oldEventTime']").val();
				var oldDuration = $("input[@name='oldDuration']").val();
				
				if (oldEventDate!=$("input[@name='eventDate']").val() || 
						oldEventTime!=(startTime+":00") || 
						oldDuration!=duration) {
					$.ajax({ 
					  type: "POST", 
					  url: "check_conflict.php", 
					  data: "eventID="+$("input[@name='eventID']").val()+
					  			"&eventDate="+$("input[@name='eventDate']").val()+
					  			"&duration="+duration+
					  			"&eventHr="+$("[@name='eventHr']").val()+
					  			"&eventMin="+$("[@name='eventMin']").val()+
					  			"&eventEndHr="+$("[@name='eventEndHr']").val()+
					  			"&eventEndMin="+$("[@name='eventEndMin']").val()+
					  			"&hiddenBookingIDs="+$("[@name='hiddenBookingIDs']").val(), 
					  success: function(msg) {
					    if (msg == 'No conflict') {
					    	submitForm();
					    } else {
					    	if(jQuery_1_3_2){
					    		(function($){
					    			$.unblockUI();
					    		})(jQuery_1_3_2);
					    	}else{
					    		$.unblockUI();
					    	}
					    	$("#eventConflictWarning").children(":not(:last)").remove();
					    	$("#eventConflictWarning").children(":last").before(msg);
					    	$("#eventConflictWarning").jqmShow();
					    }
					  } ,
					  error: function() {
					  	if(jQuery_1_3_2){
				    		(function($){
				    			$.unblockUI();
				    		})(jQuery_1_3_2);
				    	}else{
				    		$.unblockUI();
				    	}
					  }
					});
				} else {
					submitForm();
				}
			} else {
				submitForm();
			}
		}
	} else {
		v.focusInvalid();
		window.scrollTo(0,0);
		return false;
	}
	if($("input[@name='url']").val() == "")
		$("input[@name='url']").val("http://");
}

function submitForm() {
	if (document.form1.elements["viewer[]"]) {
		checkOptionAll(document.form1.elements["viewer[]"]);
	}
	document.form1.submit();
}

function display12TimeFormat(Hour, Minute) {
	if (Hour == 12) {
		var AmPm = "pm";
	} else if (Hour > 12) {
		Hour = Hour-12;
		var AmPm = "pm";
	} else {
		var AmPm = "am";
	}
	var eventTime = "("+Hour+":"+Minute+" "+AmPm+")";
	$("#12_time_format").html(eventTime);
}

//return date of eventDate and eventEndDate different in minute
function getEventDuration(){
	startDate = document.getElementById("eventDate").value;
	startPieces = startDate.split("-");
	hrobj = document.getElementById("eventHr");
	starthr = parseInt(hrobj.options[hrobj.selectedIndex].value,10);
	minobj = document.getElementById("eventMin");
	startmin = parseInt(minobj.options[minobj.selectedIndex].value,10);
	endDate = document.getElementById("eventEndDate").value;
	endPieces = endDate.split("-");
	endhrobj = document.getElementById("eventEndHr");
	endhr = parseInt(endhrobj.options[endhrobj.selectedIndex].value,10);
	endminobj  = document.getElementById("eventEndMin")
	endmin = parseInt(endminobj.options[endminobj.selectedIndex].value,10);
	
	miniSec = Date.UTC(endPieces[0],endPieces[1],endPieces[2],endhr,endmin) - Date.UTC(startPieces[0],startPieces[1],startPieces[2],starthr,startmin);
	return miniSec/1000/60;
}

function startChangeDate(){
	startDate = document.getElementById("eventDate").value;
	startPieces = startDate.split("-");
	hrobj = document.getElementById("eventHr");
	starthr = parseInt(hrobj.options[hrobj.selectedIndex].value,10);
	minobj = document.getElementById("eventMin");
	startmin = parseInt(minobj.options[minobj.selectedIndex].value,10);
	endDate = document.getElementById("eventEndDate").value;
	endPieces = endDate.split("-");
	endhrobj = document.getElementById("eventEndHr");
	endhr = parseInt(endhrobj.options[endhrobj.selectedIndex].value,10);
	endminobj  = document.getElementById("eventEndMin")
	endmin = parseInt(endminobj.options[endminobj.selectedIndex].value,10);
	
		
	if (Date.UTC(startPieces[0],startPieces[1],startPieces[2],starthr,startmin) > Date.UTC(endPieces[0],endPieces[1],endPieces[2],endhr,endmin)){
		
		document.getElementById("eventEndDate").value = startDate;
		
		for (i = 0; i < endhrobj.options.length; i++){
			if (parseInt(endhrobj.options[i].value) >= starthr){
				document.getElementById("eventEndHr").selectedIndex = i;
				break;
			}
		}
		
		for (i = 0; i < endminobj.options.length; i++){
			if (parseInt(endminobj.options[i].value) >= startmin){
				document.getElementById("eventEndMin").selectedIndex = i;
				break;
			}
		}
		
	}
}


function endChangeDate(){
	startDate = document.getElementById("eventDate").value;
	startPieces = startDate.split("-");
	hrobj = document.getElementById("eventHr");
	starthr = parseInt(hrobj.options[hrobj.selectedIndex].value,10);
	minobj = document.getElementById("eventMin");
	startmin = parseInt(minobj.options[minobj.selectedIndex].value,10);
	endDate = document.getElementById("eventEndDate").value;
	endPieces = endDate.split("-");
	endhrobj = document.getElementById("eventEndHr");
	endhr = parseInt(endhrobj.options[endhrobj.selectedIndex].value,10);
	endminobj  = document.getElementById("eventEndMin")
	endmin = parseInt(endminobj.options[endminobj.selectedIndex].value,10);
	
	if (Date.UTC(startPieces[0],startPieces[1],startPieces[2],starthr,startmin) > Date.UTC(endPieces[0],endPieces[1],endPieces[2],endhr,endmin)){
		
		document.getElementById("eventDate").value = endDate;
		
		for (i = 1; i < hrobj.options.length; i++){
			if (parseInt(hrobj.options[i].value) > endhr){
			
				document.getElementById("eventHr").selectedIndex = i-1;
				break;
			}
		}
		
		for (i = 1; i < minobj.options.length; i++){
			if (parseInt(minobj.options[i].value) > endmin){
				document.getElementById("eventMin").selectedIndex = i-1;
				break;
			}
		}
		
	}
}


var oldValueAry = new Array();
$().ready(function() {
	// define the dialog DIVs
	$('#saveEventWarning').jqm({trigger: false});
	$('#saveMultipleEventWarning').jqm({trigger: false});
	$('#deleteEventWarning1').jqm({trigger: false});
	$('#deleteEventWarning2').jqm({trigger: false});
	$('#eventConflictWarning').jqm({trigger: false});
	
	
	//repeatSelectObj = document.getElementById("repeatSelect");
<?php	if (!$editMode || (($userAccess == "A" || $userAccess == "W") || ($calAccess == 'W' || $calAccess == 'A'))&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $createdBy==$_SESSION['UserID'])){
	echo "
		repeatSelectIndex = document.getElementById('repeatSelect').selectedIndex;
	
		$('input#eventDate').change(function(){
			if (js_Event_Have_Booking_Already()) {
				alert('".$i_Calendar_eBookingEvent_cancelBookingFirstWarning."');
				$(this).val(oldValueAry[$(this).attr('id')]);
			}
			else {
				index = document.getElementById('repeatSelect').selectedIndex;
				if(index != 0){
				//	document.getElementById('eventEndDate').value = this.value;
				//	startChangeDate();
					startRepeat = document.form1.eventDate.value;
					repeatPieces = startRepeat.split('-');
					today = new Date();
					today.setFullYear(repeatPieces[0]);
					today.setMonth(repeatPieces[1]);
					today.setDate(repeatPieces[2]);
					today.setMonth(parseInt(repeatPieces[1],10)+1);
					fulldate = today.toDateString();
					datePieces = fulldate.split(' ');
					monthName = Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
					monthNum = '';
					for (i = 0; i < monthName.length; i++){
						if (monthName[i] == datePieces[1]){
							monthNum = (((i+1)<10)? '0'+(i+1):(i+1));
							break;
						}
					}
			
					document.form1.repeatEnd.value = datePieces[3]+'-'+monthNum+'-'+datePieces[2];
				}
				//else{
					startChangeDate();
					
				//}	

				oldValueAry[$(this).attr('id')] = $(this).val();
			}
		});
		
		
		$('select#eventHr').change(function(){
			//startChangeDate();
			if (js_Event_Have_Booking_Already()) {
				alert('".$i_Calendar_eBookingEvent_cancelBookingFirstWarning."');
				$(this).val(oldValueAry[$(this).attr('id')]);
			}
			else {
				startChangeDate();
				oldValueAry[$(this).attr('id')] = $(this).val();
			}
		});
		
		
		
		$('select#eventMin').change(function(){
			//startChangeDate();
			if (js_Event_Have_Booking_Already()) {
				alert('".$i_Calendar_eBookingEvent_cancelBookingFirstWarning."');
				$(this).val(oldValueAry[$(this).attr('id')]);
			}
			else {
				startChangeDate();
				oldValueAry[$(this).attr('id')] = $(this).val();
			}
		});
	
	
	
	
	
	$('input#eventEndDate').change(function(){
		//index = document.getElementById('repeatSelect').selectedIndex;
		//if(index != 0){
			//document.getElementById('eventEndDate').value = document.getElementById('eventDate').value;
			//endChangeDate();
		//}
		//else{
			endChangeDate();
		//}	
		if (js_Event_Have_Booking_Already()) {
			alert('".$i_Calendar_eBookingEvent_cancelBookingFirstWarning."');
			$(this).val(oldValueAry[$(this).attr('id')]);
		}
		else {
			oldValueAry[$(this).attr('id')] = $(this).val();
		}
	});
	
	$('select#eventEndHr').change(function(){
		//endChangeDate();
		if (js_Event_Have_Booking_Already()) {
			alert('".$i_Calendar_eBookingEvent_cancelBookingFirstWarning."');
			$(this).val(oldValueAry[$(this).attr('id')]);
		}
		else {
			endChangeDate();
			oldValueAry[$(this).attr('id')] = $(this).val();
		}
	});
	
	$('select#eventEndMin').change(function(){
		//endChangeDate();
		if (js_Event_Have_Booking_Already()) {
			alert('".$i_Calendar_eBookingEvent_cancelBookingFirstWarning."');
			$(this).val(oldValueAry[$(this).attr('id')]);
		}
		else {
			endChangeDate();
			oldValueAry[$(this).attr('id')] = $(this).val();
		}
	});

	$('select#repeatSelect').change(function(){
		if (js_Event_Have_Booking_Already()) {
			alert('".$i_Calendar_eBookingEvent_cancelBookingFirstWarning."');
			$(this).val(oldValueAry[$(this).attr('id')]);
		}
		else {
			showRepeatCtrl();
			showEbookingLocationSelection();

			oldValueAry[$(this).attr('id')] = $(this).val();
		}
	});
	
		allObj = $(':checkbox:checked').get();
		cnt = 0;
		calObj = null;
		for (i=0;i<allObj.length;i++){
			if (allObj[i].name.match('^toggle')){
				calObj = allObj[i];
				cnt++;
			}
		}
		if (cnt == 1){
			calObjID = calObj.name.split('-');
			if (calObjID.length>1){
				calObjID = calObjID[1];				
				selCal = document.getElementById('calID');
				calOption = selCal.options;
				for (i=0;i<calOption.length;i++){
					if (calOption[i].value == calObjID){					
						selCal.selectedIndex = i;
						jShow_Insert_Into_School_Calendar(calOption[i].value);
						break;
					}
				}
			}
		}
	";}
	?>
	
	// show the confirm dialog for saving repeated event
	$('#saveEvent').click(function() {
		document.form1.submitMode.value = 'save';
		validateForm();
	});
	$('#applyEvent').click(function() {
		document.form1.submitMode.value = 'apply';
		validateForm();
	});
	$('#saveNsend').click(function() {
		document.form1.submitMode.value = 'saveNsend';
		validateForm();
	});
	$('#submitAndSendPushMsg').click(function() {
		document.form1.submitMode.value = 'submitAndSendPushMsg';
		validateForm();
	});
	
	// show the confirm dialog for deleting event
	if (document.form1.deleteEvent) {
		$('#deleteEvent').click(function() {
			document.form1.action = "delete_event.php";
			if (document.form1.isRepeatEvent) {
				$('#deleteEventWarning1').jqmShow();
			} else {
				$('#deleteEventWarning2').jqmShow();
			}
		});
	}
	
	if ($("[@name='isAllDay']").fieldValue() == "0")
		display12TimeFormat($("[@name='eventHr']").fieldValue(), $("[@name='eventMin']").fieldValue());
	
	// display 12-hour time format as additional info
	$("[@name='eventHr']").change(function() {
		display12TimeFormat($("[@name='eventHr']").fieldValue(), $("[@name='eventMin']").fieldValue());
	});
	
	$("[@name='eventMin']").change(function() {
		display12TimeFormat($("[@name='eventHr']").fieldValue(), $("[@name='eventMin']").fieldValue());
	});
	
	// fix firefox height problem
	$('#eclass_main_frame_table').attr("height","");
	
	initMenu();
	
	$('a#loadConflictTT').cluetip({
		local:true, 
		width: '400px',
		cluetipClass:'jtip', 
		dropShadow:false, 
		closeText: '<img width="13" height="13" border="0" src="images/layer_btn_close.gif" name="box_close" onmouseover="MM_swapImage(\'box_close\',\'\',\'images/layer_btn_close_on.gif\',1)" onmouseout="MM_swapImgRestore()"/>',
		showTitle:false, 
		cursor: 'pointer', 
		sticky: true, 
		activation: 'click',
		positionBy: 'mouse'
	});
	
	$('input, select').each( function() {
		var tempId = $(this).attr('id');
		
		if (tempId != '' && tempId != null) {
			oldValueAry[tempId] = $(this).val();
		}
	});
	
	if($('#viewerList').length>0){
		var numOfParticipants = $('#viewerList option').length;
		var existingParticipants = document.getElementsByName('existGuestList[]');
		$('#saveNsend').attr('disabled', numOfParticipants==0 && existingParticipants.length==0);
		$('#submitAndSendPushMsg').attr('disabled', numOfParticipants==0 && existingParticipants.length==0);
	}
});

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>

function jSet_Event_Default_Access(){
	var obj = document.getElementById("calID");
	var selIndex = obj.selectedIndex;
	var calID = obj[selIndex].value;

	var newAccess = document.getElementById("calDefaultAccess_"+calID).value;
	var accessObj = document.getElementsByName("access");
	if(accessObj.length > 0){
		for(var i=0 ; i<accessObj.length ; i++){
			if(accessObj[i].value == newAccess){
				accessObj[i].checked = true;
			} else {
				accessObj[i].checked = false;
			}
		}
	}
}

function jAdd_Options_Record(selIndex, text, value){
	document.form1.elements["viewerList"].options[selIndex]  = new Option(text, value, false, false);
	
}

function jDisplay_Invite_Guest(obj){
	var index = obj.selectedIndex;
	var selValue = obj[index].value;
	var obj1 = document.getElementsByName('FoundationCalID[]');
	if(obj1.length > 0){
		for(var i=0 ; i<obj1.length ; i++){
			if(obj1[i].value == selValue){
				$('#new_event_invite_form').hide();
				return;
			}
		}
	}
	$('#new_event_invite_form').show(); 
} 

function js_Show_BookingRequest_Layer()
{
	if($("#isAllDay:checked").val() !== null) {
		var AllDayEvent = 1;
	} else {
		var AllDayEvent = 0;
	}

	$.post(
		"ajax_show_booking_request_layer.php",
		{
			"RoomID" : $("#hiddenFacilityID").val(),
			"BookingStart" : $("#eventDate").val(),
			"BookingEnd" : $("#eventEndDate").val()
		},
		function(responseText){
			$('#TB_ajaxContent').html(responseText);
		}
	);
}

function ShowBookingDetails(val)
{
	$("#hiddenFacilityID").val(val);
	
	if($("#isAllDay:checked").val() !== null) {
		var AllDayEvent = 1;
	} else {
		var AllDayEvent = 0;
	}
	
	if($("#repeatSelect").val() == "WEEKLY") 
	{
		var repeatTimes = document.form1.repeatWeeklyRange.value;
		var arr_repeat_val = new Array()
		if( $("#Sun").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 0;
		}
		if( $("#Mon").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 1;
		}
		if( $("#Tue").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 2;
		}
		if( $("#Wed").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 3;
		}
		if( $("#Thu").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 4;
		}
		if( $("#Fri").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 5;
		}
		if( $("#Sat").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 6;
		}
		var repeatValue = arr_repeat_val.toString();
	}
	else if($("#repeatSelect").val() == "DAILY")
	{
		var repeatTimes = document.form1.repeatDailyDay.value;
		var repeatValue = "";
	}
	else if($("#repeatSelect").val() == "MONTHLY")
	{
		var repeatTimes = document.form1.repeatMonthlyRange.value;
		var repeatValue = "";
		if( $("#monthlyRepeatBy0").attr("checked") == true )
		{
			repeatValue = $("#monthlyRepeatBy0").val();
		}
		else 
		{
			repeatValue = $("#monthlyRepeatBy1").val();
		}
	}
	else if($("#repeatSelect").val() == "YEARLY")
	{
		var repeatTimes = "";
		var repeatValue = document.form1.repeatYearlyRange.value;
	}
	else
	{
		var repeatTimes = "";
		var repeatValue = "";
	}
			
	if(document.form1.repeatStart.value != "")
		var repeatStartDate = document.form1.repeatStart.value;
	else
		var repeatStartDate = "";
		
	if(document.form1.repeatEnd.value != "")
		var repeatEndDate = document.form1.repeatEnd.value;
	else
		var repeatEndDate = "";
	
	$.post(
		"ajax_check_location_avaliable.php",
		{
			"RoomID" : $("#hiddenFacilityID").val(),
			"BookingStart" : $("#eventDate").val(),
			"BookingEnd" : $("#eventEndDate").val(),
			"AllDayEvent" : AllDayEvent,
			"StartHour" : $("#eventHr").val(),
			"StartMins" : $("#eventMin").val(),
			"EndHour" : $("#eventEndHr").val(),
			"EndMins" : $("#eventEndMin").val(),
			"RepeatSelect" : $("#repeatSelect").val(),
			"RepeatTimes" : repeatTimes,
			"RepeatValue" : repeatValue,
			"RepeatStart" : repeatStartDate,
			"RepeatEnd" : repeatEndDate
		},
		function(responseText){
		
			$("#div_booking_details").html(responseText);
			$("#div_booking_details").show();
		
		}
	);
}

function showEbookingLocationSelection()
{
	
	y1 = parseInt($("#eventDate").val().substring(0,4),10);
    y2 = parseInt($("#eventEndDate").val().substring(0,4),10);
    m1 = parseInt($("#eventDate").val().substring(5,7),10);
    m2 = parseInt($("#eventEndDate").val().substring(5,7),10);
    d1 = parseInt($("#eventDate").val().substring(8,10),10);
    d2 = parseInt($("#eventEndDate").val().substring(8,10),10);
	var s1 = new Date(y1,m1,d1);
	var s2 = new Date(y2,m2,d2);
	
	if(s1 > s2){
		$("#eventEndDate").val($("#eventDate").val());
	}
	
	if($("#eventDate").val() == $("#eventEndDate").val()){
		$("#DIV_eBookingLocation").show();
	}else{
		if( $("#repeatSelect").val() != "NOT" ) {
			$("#DIV_eBookingLocation").show();
		} else { 
			$("#DIV_eBookingLocation").hide();
		}
	}
}

function js_Show_Item_Booking_Details(jsItemID, jsClickedObjID)
{
//	js_Hide_Detail_Layer();
//	js_Change_Layer_Position(jsItemID, jsClickedObjID);
	
//	$('div#ItemBookingDetailsLayerContentDiv').html('<?=$Lang['General']['Loading']?>').load(
//		"ajax_task.php", 
//		{ 
//			task: 'LoadItemBookingLayerTable',
//			ItemID: jsItemID,
//			DateTimeArr: $('input#DateTimeArr').val()
//		},
//		function(ReturnData)
//		{
			js_Show_Detail_Layer(jsItemID);
//		}
//	);
}

function js_Submit_Booking() {

	js_Change_Selected_Location(1);	

	if($("input[@name='CancelDayBookingIfOneItemIsNA']:checked").val() !== null) {
		var CancelDayBookingIfOneItemIsNA = 1;
	} else {
		var CancelDayBookingIfOneItemIsNA = 0;
	}
	
	if($("#isAllDay:checked").val() !== null) {
		var AllDayEvent = 1;
	} else {
		var AllDayEvent = 0;
	}
	
	if($("#repeatSelect").val() == "WEEKLY") 
	{
		var repeatTimes = document.form1.repeatWeeklyRange.value;
		var arr_repeat_val = new Array()
		if( $("#Sun").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 0;
		}
		if( $("#Mon").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 1;
		}
		if( $("#Tue").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 2;
		}
		if( $("#Wed").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 3;
		}
		if( $("#Thu").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 4;
		}
		if( $("#Fri").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 5;
		}
		if( $("#Sat").attr('checked') == true ) {
			arr_repeat_val[arr_repeat_val.length] = 6;
		}
		var repeatValue = arr_repeat_val.toString();
	}
	else if($("#repeatSelect").val() == "DAILY")
	{
		var repeatTimes = document.form1.repeatDailyDay.value;
		var repeatValue = "";
	}
	else if($("#repeatSelect").val() == "MONTHLY")
	{
		var repeatTimes = document.form1.repeatMonthlyRange.value;
		var repeatValue = "";
		if( $("#monthlyRepeatBy0").attr("checked") == true )
		{
			repeatValue = $("#monthlyRepeatBy0").val();
		}
		else 
		{
			repeatValue = $("#monthlyRepeatBy1").val();
		}
	}
	else if($("#repeatSelect").val() == "YEARLY")
	{
		var repeatTimes = "";
		var repeatValue = document.form1.repeatYearlyRange.value;
	}
	else
	{
		var repeatTimes = "";
		var repeatValue = "";
	}
			
	if(document.form1.repeatStart.value != "")
		var repeatStartDate = document.form1.repeatStart.value;
	else
		var repeatStartDate = "";
		
	if(document.form1.repeatEnd.value != "")
		var repeatEndDate = document.form1.repeatEnd.value;
	else
		var repeatEndDate = "";
	
	// handle Single Item Booking
	var arrSingleItem = new Array();
	var i = 0;
	$("input:checkbox[@name='SingleItemID[]']").each(
		function()
		{
			if (this.checked)
			{
				arrSingleItem[i] = this.value;
				i++;
			}
		}
	); 
	if(arrSingleItem != "") {
		var strSingleItem = arrSingleItem.toString();
	} else {
		var strSingleItem = "";
	}

	$.post(
		"ajax_submit_booking_request.php",
		{
			"RoomID" : $("#hiddenFacilityID").val(),
			"StartDate" : $("#eventDate").val(),
			"EndDate" : $("#eventEndDate").val(),
			"AllDayEvent" : AllDayEvent,
			"StartHour" : $("#eventHr").val(),
			"StartMins" : $("#eventMin").val(),
			"EndHour" : $("#eventEndHr").val(),
			"EndMins" : $("#eventEndMin").val(),
			"RepeatSelect" : $("#repeatSelect").val(),
			"RepeatTimes" : repeatTimes,
			"RepeatValue" : repeatValue,
			"RepeatStart" : repeatStartDate,
			"RepeatEnd" : repeatEndDate,
			"CancelDayBookingIfOneItemIsNA" : CancelDayBookingIfOneItemIsNA,
			"SingleItemID" : strSingleItem,
			"EventTitle" : $("#title").val(),
			"EventDesc" : $("#description").val()
		},
		function(responseText)
		{
			if(responseText != "")
			{
				//var location_name = responseText.substring(0,responseText.indexOf("||"));
				//var returnBookingID = responseText.substring(responseText.indexOf("||")+1,responseText.length);
				
				var arrResult = responseText.split("||");
				
				$("#hiddenBookingIDs").val(arrResult[1]);
				$("#div_location").hide();
				$("#div_selected_location").html(arrResult[0]);
				$("#div_selected_location").show();
				window.top.tb_remove();
				initThickBox();
			}
		}
	);
}

function js_Change_Selected_Location(deleteAll)
{
	var current_BookingIDs = $("#hiddenBookingIDs").val();
	
	$.post(
		"ajax_delete_booking_record.php",
		{
			"strBookingIDs" : current_BookingIDs,
			"deleteAll" : deleteAll
		},
		function(responseText)
		{
			js_Show_BookingRequest_Layer();
		}
	);
	
	initThickBox();
}

function js_Cancel_Selected_Location()
{
	if (confirm('<?=$i_Calendar_eBookingEvent_clearBookingWarning?>')) {
		var current_BookingIDs = $("#hiddenBookingIDs").val();
		
		$.post(
			"ajax_delete_booking_record.php",
			{
				"strBookingIDs" : current_BookingIDs,
				"deleteAll" : 1
			},
			function(responseText)
			{
				$("#hiddenBookingIDs").val("");
				$("#location").val("");
				$("#div_location").show();
				$("#div_selected_location").html("");
				$("#div_selected_location").hide();
				
				initThickBox();
			}
		);
	}
}

function js_Delete_Temp_Booking_Record()
{
	var current_BookingIDs = $("#hiddenBookingIDs").val();
	$.post(
		"ajax_delete_booking_record.php",
		{
			"strBookingIDs" : current_BookingIDs
		},
		function(responseText)
		{
			window.location='index.php?time='+'<?=$_SESSION["iCalDisplayTime"]?>'+'&calViewPeriod='+'<?=$_SESSION["iCalDisplayPeriod"]?>';
		}
	);
}

function jShow_Insert_Into_School_Calendar(val)
{
	var checked = $('#ShowInSchoolCalendar').length>0? $('#ShowInSchoolCalendar').is(':checked'): false;
	$("#ShowInSchoolCalendar").attr('checked', checked);
	if($("#"+val).attr('class') == "calGroup")
	{
		$("#DIV_ShowInSchoolCalendar").show();
	}else{
		$("#DIV_ShowInSchoolCalendar").hide();
		$("#ShowInSchoolCalendar").attr('checked', false);
	}
}

function js_Show_Detail_Layer(jsItemID)
{
	$('#ItemBookingDetailsLayer'+jsItemID).toggle();
//	$('#ItemBookingDetailsLayer'+jsItemID).show();
	MM_showHideLayers('ItemBookingDetailsLayer'+jsItemID,'','show');
}

function js_Event_Have_Booking_Already() {
	if ($('input#hiddenBookingIDs').val() != '' && $('input#hiddenBookingIDs').val() != null) {
		return true;
	}
	else {
		return false;
	}
}

function getCheckAvailableTimeForm()
{
	(function($,$1){
		var spinner = '<?=$linterface->Get_Ajax_Loading_Image()?>';
		var eventDate = $('#eventDate').val();
		var isAllDay = $('#isAllDay').is(':checked')? 1: 0;
		var startTime = $('#eventHr').val() + ':' + $('#eventMin').val();
		var endTime = $('#eventEndHr').val() + ':' + $('#eventEndMin').val();
		var viewer = [];
		var viewerSelection = document.getElementById('viewerList');
		for(var i=0;i<viewerSelection.options.length;i++){
			if(viewerSelection.options[i].value != ''){
				viewer.push(viewerSelection.options[i].value);
			}
		}
		if(viewer.length == 0){
			alert('<?=$Lang['iCalendar']['RequestSelectGuest']?>');
			return;
		}
		
		var w = Math.min($(window).width()*0.9, 960);
		var h = Math.min($(window).height()*0.9, 700);
		tb_show('<?=$Lang['iCalendar']['CheckAvailability']?>',"#TB_inline?height="+h+"&width="+w+"&inlineId=FakeLayer",'');
		
		$('#TB_ajaxContent').html(spinner);
		$.post(
			'ajax_check_available_task.php',
			{
				'task': 'getCheckAvailableTimeForm',
				'eventDate': eventDate,
				'isAllDay': isAllDay,
				'viewer[]': viewer,
				'startTime': startTime,
				'endTime': endTime 
			},
			function(returnHtml){
				$('#TB_ajaxContent').html(returnHtml);
				$('#ContainerDiv').css('height', '100%');
				
				(function($){
					$.datepicker.setDefaults({showOn: "both", buttonImageOnly: true, buttonImage: "<?=$image_path.'/'.$LAYOUT_SKIN.'/icon_calendar_off.gif'?>", buttonText: "Calendar"});
			    	$("#EventDate").datepicker({
					    dateFormat: "yy-mm-dd",
						dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
						changeFirstDay: false,
						firstDay: 0
				    });
			    })($1);
			    
			    $('#EventTimeline').click(function(){
			    	$('#StartTime_hour').val(7);
			    	$('#StartTime_min').val(0);
			    	$('#EndTime_hour').val(18);
			    	$('#EndTime_min').val(0);
			    });
			    
			    getCheckAvailableTimeResult();
			}
		);
		
	})(jQuery_1_3_2,jQuery_1_1_2);
}

function getCheckAvailableTimeResult()
{
	var spinner = '<?=$linterface->Get_Ajax_Loading_Image()?>';
	
	var checkType = $('#Availability').is(':checked')? $('#Availability').val() : $('#EventTimeline').val();
	var calTypeAry = [];
	var calTypeCheckbox = document.getElementsByName('CalType[]');
	var eventDate = $('#EventDate').val();
	var start_hour = parseInt($('#StartTime_hour').val());
	var start_min = parseInt($('#StartTime_min').val());
	var end_hour = parseInt($('#EndTime_hour').val());
	var end_min = parseInt($('#EndTime_min').val());
	var startTime = (start_hour < 10? '0'+ start_hour : start_hour) + ':' + (start_min < 10?'0'+start_min:start_min);
	var endTime = (end_hour < 10? '0'+ end_hour : end_hour) + ':' + (end_min < 10?'0'+end_min:end_min);
	var viewerCount = parseInt($('#viewerCount').val());
	var viewer = [];
	var viewerSelection = document.getElementById('viewerList');
	for(var i=0;i<viewerSelection.options.length;i++){
		if(viewerSelection.options[i].value != ''){
			viewer.push(viewerSelection.options[i].value);
		}
	}
	for(var i=0;i<calTypeCheckbox.length;i++){
		if(calTypeCheckbox[i].checked){
			calTypeAry.push(calTypeCheckbox[i].value);
		}
	}
	
	if(viewer.length == 0){
		alert('<?=$Lang['iCalendar']['RequestSelectGuest']?>');
		return;
	}
	
	$('#btnCheck').attr('disabled',true);
	$('#ContainerDiv').html(spinner);
	$.post(
		'ajax_check_available_task.php',
		{
			'task': 'getCheckAvailableTimeResult',
			'CheckType': checkType,
			'EventDate': eventDate,
			'StartTime': startTime,
			'EndTime': endTime,
			'viewer[]': viewer,
			'CalType[]': calTypeAry  
		},
		function(returnData){
			if(checkType == 'A'){
				$('#ContainerDiv').html(returnData);
				$('#ContainerDiv').css('height', '100%');
				$('#btnCheck').attr('disabled',false);
				return;
			}
			$('#ContainerDiv').css('height', (viewerCount + 1) * 50 + 'px');
			var data = {};
			if(JSON){
				data = JSON.parse(returnData);
			}else{
				eval('data="'+returnData+'";');
			}
			
			var ydata = [];
			for(var i=data.length-1;i>=0;i--){
				ydata.push(data[i]['name']);
			}
			
			(function ($) {
				Highcharts.setOptions({
					global: {
						useUTC: false
					}
				});
				
			   	// Define tasks
			   	var tasks = data;
			   	var now = new Date(Date.parse(eventDate));
			   	var timezone_offset = now.getTimezoneOffset() * 60 * 1000;
			   	var min_ts = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(),now.getUTCDate(),startTime.split(':')[0],startTime.split(':')[1],0) + timezone_offset;
			   	var max_ts = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(),now.getUTCDate(),endTime.split(':')[0],endTime.split(':')[1],59) + timezone_offset;
			   	
			   	// re-structure the tasks into line seriesvar series = [];
			   	var series = [];
			   	$.each(tasks.reverse(), function(i, task) {
			   	    var item = {
			   	        name: task.name,
			   	        data: []
			   	    };
			   	    $.each(task.intervals, function(j, interval) {
			   	    	
			   	        item.data.push({
			   	            x: interval.from,
			   	            y: i,
			   	            label: interval.label,
			   	            from: interval.from,
			   	            to: interval.to
			   	        }, {
			   	            x: interval.to,
			   	            y: i,
			   	            label: interval.label,
			   	            from: interval.from,
			   	            to: interval.to
			   	        });
			   	        
			   	        // add a null value between intervals
			   	        if (task.intervals[j + 1]) {
			   	            item.data.push(
			   	                [(interval.to + task.intervals[j + 1].from) / 2, null]
			   	            );
			   	        }
			   	    	
			   	    	if(interval.from < min_ts) min_ts = interval.from;
			   	    	if(interval.to > max_ts) max_ts = interval.to;
			   	    });
					
			   	    series.push(item);
			   	});
				
			   	// create the chart
			   	var chart = new Highcharts.Chart({
			   	    chart: {
			   	        renderTo: 'ContainerDiv'
			   	    },
	
			   	    title: {
			   	        text: ''
			   	    },
	
			   	    xAxis: {
			   	        type: 'datetime',
			   	        opposite: true,
			   	        title: {
			   	            text: ''
			   	        },
			   	        min: min_ts,
			   	        max: max_ts,
			   	        dateTimeLabelFormats: {
				           minute: '%H:%M',
							hour: '%H:%M',
							day: '%H:%M'
				        }
			   	    },
	
			   	    yAxis: {
	
			            //categories: ydata,
			            min: -0.5,
			            max: ydata.length-1 + 0.5,
			            showEmpty: true,
			   	        tickInterval: 1,            
			   	        //tickPixelInterval: 30,
			   	        labels: {
			   	            style: {
			   	                color: '#525151',
			   	                font: '12px Helvetica',
			   	                fontWeight: 'bold'
			   	            }
			   	            , 
			   	            formatter: function() {
			   	                if (tasks[this.value]) {
			   	                    return tasks[this.value].name;
			   	                }
			   	            }
			   	            
			   	        },
			   	        startOnTick: false,
			   	        endOnTick: false,
			   	        title: {
			   	            text: ''
			   	        },
			   	        //minPadding: 0.2,
			   	        //maxPadding: 0.2,
			            fontSize:'15px' 
			   	    },
	
			   	    legend: {
			   	        enabled: false
			   	    },
			   	    tooltip: {
			   	        formatter: function() {
			   	            return  (this.point.options.label!=''? '<b>' + this.point.options.label + '</b><br />' : '') + Highcharts.dateFormat('%H:%M', this.point.options.from)  + ' - ' + Highcharts.dateFormat('%H:%M', this.point.options.to); 
			   	        }
			   	    },
	
			   	    plotOptions: {
			   	        line: {
			   	            lineWidth: 10,
			   	            marker: {
			   	                enabled: true 
			   	            },
			   	            dataLabels: {
			   	                enabled: true,
			   	                align: 'left',
			   	                formatter: function() {
			   	                    return this.point.options && this.point.options.label;
			   	                } 
			   	            }
			   	        }
			   	    },
	
			   	    series: series
					,
					exporting: { enabled: false },
					credits: {
      					enabled: false
  					}
			   	});		   
		   })(jQuery_1_3_2);
			
			$('#btnCheck').attr('disabled',false);
		}
	);
	
}
</script>

<div id="eventDateDiv" style="position:absolute; width:165px; z-index:2; visibility: hidden;">
  <span class="small_cal_board_01"><span class="small_cal_board_02"></span></span>
	<div style="background-color:#DBEDFF;float:left;width:100%" align="right">
	  <div style="width:97%"><a href="javascript:jShow_Layer('', 'eventDateDiv', 'hide')" class="sub_layer_link">X</a>&nbsp;</div>
  	  <div id="eventDateDivContent" style="width:97%"></div>
  	</div>
  <span class="small_cal_board_05"><span class="small_cal_board_06"></span></span>
</div>
<div id="module_bulletin" class="module_content">
<?php 
  	echo $lui->Get_Sub_Function_Header("iCalendar",$Msg);
  	echo $display; 
?>
</div>

<?php
if((!isset($editEventID) || (isset($editEventID) && $editEventID == "")) && count($ownDefaultAccess) > 0){
	foreach($ownDefaultAccess as $key => $value){
		echo $lui->Get_Input_Hidden("calDefaultAccess_$key", "calDefaultAccess_$key", $value)."\n";
	}
}
?>

<script>
if(document.all){	// for IE
	$("[@name='description']").removeClass('textarea_desc');
	$("[@name='description']").attr("cols", "52");
	$("[@name='description']").css("padding-left", "3px");

	$("[@name='PersonalNote']").removeClass('textarea_desc');
	$("[@name='PersonalNote']").attr("cols", "52");
	$("[@name='PersonalNote']").css("padding-left", "3px");
} else {			// non IE
}
</script>
	
<?php
######################################################################################################################
// include_once($PathRelative."src/include/template/general_footer.php");
include_once("footerInclude.php");
?>