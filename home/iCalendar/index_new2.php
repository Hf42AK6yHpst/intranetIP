<?php
// page modifing by: 
$PathRelative = "../../../";
$CurSubFunction = "iCalendar";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
intranet_opendb();
#include_once($PathRelative."src/include/template/general_header.php");
include_once($PathRelative."src/include/class/icalendar.php");

$iCal = new icalendar();
$lui = new UserInterface();

$calViewPeriodArr = array("daily","weekly","monthly");
if (!isset($calViewPeriod) || !in_array($calViewPeriod, $calViewPeriodArr))
	$calViewPeriod = $iCal->systemSettings["PreferredView"];
if ($calViewPeriod == "")
	$calViewPeriod = "monthly";

if (isset($time) && is_numeric($time)) {
	$year = date('Y', $time);
	$month = date('m', $time);
	$day = date('d', $time);
} else if (!isset($year) && !isset($month)){
	$time = time();
	$year = date('Y');
	$month = date('m');
	$day = date('d');
}

# Insert a default calendar if user didn't have any calendar
if (!isset($_SESSION["iCalHaveCal"]) ||  $_SESSION["iCalHaveCal"]!= 1) {
	$iCal->insertMyCalendar();
}

# Record the display view in session during each visit to this page
$_SESSION["iCalDisplayPeriod"] = $calViewPeriod;
$_SESSION["iCalDisplayTime"] = $time;


# Main UI - Preparation
$CalLabeling = '
	<div id="caltabs">
	  <ul>
	    <li><a href="icalendar_agenda.php"><span>Agenda</span></a></li>
	    <li><a href="icalendar_day.php"><span>Day</span></a></li>
	    <li><a href="icalendar_week.php"><span>Week</span></a></li>
	    <li id="current"><a href="icalendar.htm"><span>Month</span></a></li>
	  </ul>
	</div>
	<div id="cal_day_range">
	  <a href="#"><img src="'.$ImagePathAbs.'layout_grey/btn_prev_off.gif" width="18" height="18" border="0" align="absmiddle"></a>April 2008							
	  <a href="#"><img src="'.$ImagePathAbs.'layout_grey/btn_next_off.gif" width="18" height="18" border="0" align="absmiddle"></a>
	</div>					
	<div style="float:right">
	  <a href="#" class="cal_select_today cal_select_today_selected">Today</a>
	</div>';

// Main Calendar
$CalMain = '
	<div id="cal_board">
	  <span class="cal_board_02"></span> 
	  <div class="cal_board_content">
	    <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
	  </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>';
	

# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain);


# Choose CSS style sheet
$styleSheet = "css/display_calendar.css";

?>

<link rel="stylesheet" href="css/main.css" type="text/css" />
<link rel="stylesheet" href="<?=$styleSheet?>" type="text/css" />

<?php
# Start outputing the layout after CSS is loaded to prevent "FOUC"
include_once($PathRelative."src/include/template/general_header.php");

?>


<div id="module_bulletin" class="module_content">
  <div id="module_content_header">
	<div id="module_content_header_title"><a href=""> iCalendar</a></div>
	<div id="module_content_relate"><a href="#">related information</a></div>
  </div>
  <br>
  <?php 
  	echo $display;
  /*
  <div id="ical_left">
  	<!-- Start Event Item -->
	<div id="btn_link">
	  <a href="icalendar_new_event_advance.htm"><img src="<?=$ImagePathAbs?>icon_add.gif" border="0" align="absmiddle">New Event</a> 
	  <a href="#"><img src="<?=$ImagePathAbs?>icon_print.gif" border="0" align="absmiddle">Print</a>
	</div>
	<!-- End Event Item -->
	<br style="clear:both">
	<div id="cal_left_act_event"> 
	  <!-- Start Event Notice -->
	  <div class="cal_act_event_board"> 
		<span class="cal_event_board_01"><span class="cal_event_board_02"></span></span>
		<div class="cal_event_board_03">
		  <div class="cal_event_board_04">
	  	  <span class="portal_cal_active_event_title"><img src="<?=$ImagePathAbs?>icon_event.gif" width="22" height="22" align="absmiddle">Event</span><br>
		  <span class="cal_act_event_board_content">
			<a href="icalendar_act_event.htm" class="invite">Invitation(0)</a><br>
			<a href="#" class="notify">Notification(0)</a>
		  </span>
		  </div>
		</div>
		  <span class="cal_event_board_05"><span class="cal_event_board_06"></span></span>
		</div>
	  </div>
	  <!-- End Event Notice -->
	  <br style="clear:both">
	  <!-- Start Small Calendar -->
	  <div id="small_cal">
		<span class="small_cal_board_01"><span class="small_cal_board_02"></span></span>
		<div class="cal_board_content" style="padding-left:5px">
		  <div id="cal_day_range">
		    <a href="#"><img src="<?=$ImagePathAbs?>layout_grey/btn_prev_off.gif" width="18" height="18" border="0" align="absmiddle"></a>
		    <span style="width:110px;">April 2008 </span>
		    <a href="#"><img src="<?=$ImagePathAbs?>layout_grey/btn_next_off.gif" width="18" height="18" border="0" align="absmiddle"></a>
		  </div>
		  <br style="clear:both">
		  <!-- Table of mall Calendar -->
		  <br><br><br>
		  <br><br><br>
		  <br>
		</div>
		<span class="small_cal_board_05"><span class="small_cal_board_06"></span></span>
	  </div>
	  <!-- End Small Calendar -->
	  <!-- Start Calendar Brief Setting-->
	  <div id="cal_list">
		<div id="cal_calendar_tab">
		  <ul>
			<li id="current"><a href="#"><span>Calendar</span></a></li>
			<li><a href="icalendar_timetable.htm"><span>Timetable</span></a></li>
		  </ul>
		</div>
		<span class="cal_list_manage"><a href="icalendar_manage_cal.htm" class="sub_layer_link">Manage Calendar</a></span>
		<br style="clear:both">
		<div id="cal_calendar_list">
		<!-- Manage Calendar Content -->
		</div>
	  </div>
	  <!-- End Calendar Brief Setting-->
	</div>
  </div>*/
  ?>
  
  <?php 
  /*
  <div id="ical_right">
	<br>
	<!-- Start Calendar Labeling -->
	<div id="caltabs">
	  <ul>
	    <li><a href="icalendar_agenda.php"><span>Agenda</span></a></li>
	    <li><a href="icalendar_day.php"><span>Day</span></a></li>
	    <li><a href="icalendar_week.php"><span>Week</span></a></li>
	    <li id="current"><a href="icalendar.htm"><span>Month</span></a></li>
	  </ul>
	</div>
	<div id="cal_day_range">
	  <a href="#"><img src="<?=$ImagePathAbs?>layout_grey/btn_prev_off.gif" width="18" height="18" border="0" align="absmiddle"></a>April 2008							
	  <a href="#"><img src="<?=$ImagePathAbs?>layout_grey/btn_next_off.gif" width="18" height="18" border="0" align="absmiddle"></a>
	</div>					
	<div style="float:right">
	  <a href="#" class="cal_select_today cal_select_today_selected">Today</a>
	</div>
	<!-- End Calendar Labeling -->
	<br style="clear:both">	
	<!-- Start Big Calendar -->
	<div id="cal_board">
	  <span class="cal_board_02"></span> 
	  <div class="cal_board_content">
	    <!-- Week, Day, Month Calendar -->
	    <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
	  </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>
	<!-- Start Big Calendar -->
  </div>
  */
  ?>
	
</div>



<?php

include_once($PathRelative."src/include/template/general_footer.php");

?>