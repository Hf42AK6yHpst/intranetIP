<?php
// page modifing by: 
/*
 * 2013-09-25 (Carlos): explicitly set jqmodal z-index to a very large number to overlay all other elements
 * 2013-09-06 (Carlos): modified removeCal(), changed warning msg for shared calender owner and let owner choose to
 * 						1) remove and keep other sharers view, or 2) remove calendar completely
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
intranet_auth();
intranet_opendb();


$iCal = new icalendar();
$iCal_api = new icalendar_api();
$lui = new ESF_ui();

if (!isset($time))
	$time = $_SESSION["iCalDisplayTime"];
	
# Initialization)
$ownDefaultCalID = '';
$ownCalendarList = array();
$ownCalendarURL = array();
$otherCalendarList = array();
$sharedByCalendarList = array();
$publicSubCalendarList = array();
$schoolCalendarList = array();
$groupCalendarList = array();
$courseCalendarList = array();
$otherCalTypePersonalNum = 0;
$otherCalTypeSchoolNum = 0;
$otherCalTypeFoundationNum = 0;
$otherCalTypePersonalStr = '';
$otherCalTypeSchoolStr = '';
$otherCalTypeFoundationStr = '';
	
# Preparation
# Owner's Calendar
$ownDefaultCalID = $iCal->returnOwnerDefaultCalendar();
$ownCalendarList = $iCal->returnViewableCalendar(TRUE);
for ($i=0; $i<sizeof($ownCalendarList); $i++) {
	// if ($ownCalendarList[$i]["CalType"] == 4)
		// continue;
	$viewableCalColor[$ownCalendarList[$i]["CalID"]] = $ownCalendarList[$i]["Color"];
	$viewableCalVisible[$ownCalendarList[$i]["CalID"]] = (int)$ownCalendarList[$i]["Visible"];
	if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
		$sql ="select SyncURL from CALENDAR_CALENDAR where CalID = ".$ownCalendarList[$i]["CalID"];
		$r = $iCal->returnArray($sql);
		$ownCalendarURL[$i] = !(empty($r[0]["SyncURL"])||$r[0]["SyncURL"] == " ");
	}
}

# Optional Calendar
$otherCalendarList = $iCal->returnViewableCalendar(false,true);
for ($i=0; $i<sizeof($otherCalendarList); $i++) {
	$viewableCalColor[$otherCalendarList[$i]["CalID"]] = $otherCalendarList[$i]["Color"];
	$viewableCalVisible[$otherCalendarList[$i]["CalID"]] = (int)$otherCalendarList[$i]["Visible"];
}


	# School Calendar
	$schoolCalendarList[] = Array("Name"=>$iCal_api->Public_Calendar["name"],"CalID"=>$iCal_api->Public_Calendar["recordType"],"Color"=>$iCal_api->Public_Calendar["color"],"CalType"=>1);
	$schoolCalendarList[] = Array("Name"=>$iCal_api->Group_Event["name"],"CalID"=>$iCal_api->Group_Event["recordType"],"Color"=>$iCal_api->Group_Event["color"],"CalType"=>1);
	$schoolCalendarList[] = Array("Name"=>$iCal_api->School_Event["name"],"CalID"=>$iCal_api->School_Event["recordType"],"Color"=>$iCal_api->School_Event["color"],"CalType"=>1);
	$schoolCalendarList[] = Array("Name"=>$iCal_api->School_Holiday["name"],"CalID"=>$iCal_api->School_Holiday["recordType"],"Color"=>$iCal_api->School_Holiday["color"],"CalType"=>1);
	$schoolCalendarList[] = Array("Name"=>$iCal_api->Academic_Event["name"],"CalID"=>$iCal_api->Academic_Event["recordType"],"Color"=>$iCal_api->Academic_Event["color"],"CalType"=>1);
	
if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF || $plugin["iCalendarFullToAllUsers"]){
	#other calendar
	$systemCalendarList[] = Array("Name"=>$iCal_api->Enrollment["name"],"CalID"=>0,"Color"=>$iCal_api->Enrollment["color"],"CalType"=>'5-6');
	$systemCalendarList[] = Array("Name"=>$iCal_api->Detention["name"],"CalID"=>0,"Color"=>$iCal_api->Detention["color"],"CalType"=>$iCal_api->Detention["CalType"]);
	$systemCalendarList[] = Array("Name"=>$iCal_api->Homework["name"],"CalID"=>0,"Color"=>$iCal_api->Homework["color"],"CalType"=>$iCal_api->Homework["CalType"]);
}

if (sizeof($otherCalendarList)>0) {
        for ($i=0; $i<sizeof($otherCalendarList); $i++) {
                if(trim($otherCalendarList[$i]['CalType']) == 1){
                        # School Calendar
                    /*    $schoolCalendarList[] = $otherCalendarList[$i];
						$sql ="select SyncURL from CALENDAR_CALENDAR where CalID = '".$otherCalendarList[$i]["CalID"]."'";
						$sql2 = "select CalID from CALENDAR_CALENDAR_VIEWER where UserID = '".$_SESSION['UserID']."' and (Access = 'A' or Access = 'W')";
						$sql .= " and CalID in (".$sql2.")";
						$r = $iCal->returnArray($sql);
						$schoolCalendarURL[] = !(empty($r[0]["SyncURL"])||$r[0]["SyncURL"] == " " || empty($r)); */
                } 
				else if (trim($otherCalendarList[$i]['CalType']) == 2){
					//if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
						$groupCalendarList[] = $otherCalendarList[$i];
						$sql ="select SyncURL from CALENDAR_CALENDAR where CalID = '".$otherCalendarList[$i]["CalID"]."'";
						$sql2 = "select CalID from CALENDAR_CALENDAR_VIEWER where UserID = '".$_SESSION['UserID']."' and (Access = 'A' or Access = 'W')";
						$sql .= " and CalID in (".$sql2.")";
						$r = $iCal->returnArray($sql);
						//echo "SyncURL: ".$r[0]["SyncURL"];
						$groupCalendarURL[] = !(empty($r[0]["SyncURL"])||$r[0]["SyncURL"] == " " || empty($r));
					//}
				}
				else if (trim($otherCalendarList[$i]['CalType']) == 3){
					//if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
						$courseCalendarList[] = $otherCalendarList[$i];
						$sql ="select SyncURL from CALENDAR_CALENDAR where CalID = '".$otherCalendarList[$i]["CalID"]."'";
						$sql2 = "select CalID from CALENDAR_CALENDAR_VIEWER where UserID = '".$_SESSION['UserID']."' and (Access = 'A' or Access = 'W')";
						$sql .= " and CalID in (".$sql2.")";
						$r = $iCal->returnArray($sql);
						//echo "SyncURL: ".$r[0]["SyncURL"];
						$courseCalendarURL[] = !(empty($r[0]["SyncURL"])||$r[0]["SyncURL"] == " " || empty($r));
					//}
				}
				else {
                        if(strtoupper(trim($otherCalendarList[$i]['Access'])) == 'P'){
                                # Publich Calendar
                                $publicSubCalendarList[] = $otherCalendarList[$i];
								if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
									$sql ="select SyncURL from CALENDAR_CALENDAR where CalID = '".$otherCalendarList[$i]["CalID"]."'";
									$sql2 = "select CalID from CALENDAR_CALENDAR_VIEWER where UserID = '".$_SESSION['UserID']."' and (Access = 'A' or Access = 'W')";
									$sql .= " and CalID in (".$sql2.")";
									$r = $iCal->returnArray($sql);
									$publicSubCalendarURL[] = !(empty($r[0]["SyncURL"])||$r[0]["SyncURL"] == " " || empty($r));
								}
                        } else {
                                # Shared Calendar
                                $sharedByCalendarList[] = $otherCalendarList[$i];
								if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
									$sql ="select SyncURL from CALENDAR_CALENDAR where CalID = '".$otherCalendarList[$i]["CalID"]."'";
									$sql2 = "select CalID from CALENDAR_CALENDAR_VIEWER where UserID = '".$_SESSION['UserID']."' and (Access = 'A' or Access = 'W')";
									$sql .= " and CalID in (".$sql2.")";
									$r = $iCal->returnArray($sql);
									//echo "SyncURL: ".$r[0]["SyncURL"];
									$sharedByCalendarURL[] = !(empty($r[0]["SyncURL"])||$r[0]["SyncURL"] == " " || empty($r));
								}
                        }
                }
        }
}

# Foundation Calendar
// $foundationCalendarList = $iCal->returnFoundationCalendar();

# Other CalType Calendar 
// $otherCalTypeCalendar = $iCal->returnOtherCalTypeCalendar();
// if(count($otherCalTypeCalendar) > 0){
	// for ($i=0; $i<sizeof($otherCalTypeCalendar); $i++){
		// $RoleRightCondition = trim($otherCalTypeClanedar[$i]['RoleRightCondition']);
		// $RoleRightList = $otherCalTypeClanedar[$i]['RoleRightName'];
		// $RoleRightArr = explode(",", $RoleRightList);
		
		// if(Auth($RoleRightArr, '', $RoleRightCondition)){	# Check CPD User Role Right
			// $calName = str_replace("'", "\'", $otherCalTypeCalendar[$i]["Name"]);
			// $createdBy = '-';
			// $colorID = $iCal->Get_Calendar_ColorID($otherCalTypeCalendar[$i]["Color"]);
			// $tmp .= '
				// <tr>
				  // <td class="cal_agenda_content_entry cal_agenda_content_entry_event" style="width:60%">
				  
				  // <div style="position:absolute;width:320px; overflow:hidden" >
						// <span style="line-height:17px;margin-left:'.(strstr($_SERVER['HTTP_USER_AGENT'],"MSIE")?"5":"25").'px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$otherCalTypeCalendar[$i]["Name"].'
						// </span>
					// </div>
					
					// <span style="float:left">
					// <img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					// <span>
				  
				  // <!--  old approach
					// <img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">&nbsp;
					// <span class=" cal_mange_calendar_list_item cal_calendar_'.$colorID.'">'.$otherCalTypeCalendar[$i]["Name"].'</span>
					// -->
					
					
				  // </td>
				  // <td class="cal_agenda_content_entry" style="width:20%">'.$createdBy.'</td>
				  // <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:100px">&nbsp;</td>
				// </tr>';
			// if($otherCalTypeCalendar[$i]['ManageType'] == 0){
				// $otherCalTypePersonalNum++;
				// $otherCalTypePersonalStr .= $tmp;
			// } else if($otherCalTypeCalendar[$i]['ManageType'] == 1){
				// $otherCalTypeSchoolNum++;
				// $otherCalTypeSchoolStr .= $tmp;
			// } else if($otherCalTypeCalendar[$i]['ManageType'] == 2){
				// $otherCalTypeFoundationNum++;
				// $otherCalTypeFoundationStr .= $tmp;
			// }		
		// }
	// }
// }

	
##########################################################
#$deleteCalButton  = $lui->Get_Input_Button($button_remove, "", $button_remove, "onclick=\"window.location='delete_calendar.php?calID=#%calID%#';\"");
#$deleteCalButton .= $lui->Get_Input_Button($button_cancel, "", $button_cancel, "$('#deleteCalendarWarning').jqmHide();");
#$deleteCalButton = addslashes($deleteCalButton);

# Button
$deleteCalButton = "<input type='button' name='' value='$iCalendar_NewEvent_Reminder_Remove' onclick='confirmDeleteRedirect(#%calID%#)' />";
$deleteCalButton .= "<input type='button' name='' value='".$Lang['btn_cancel']."' onclick='hideDeleteAlertMsg()' />";

$deleteOwnCalButton = "<input type='button' name='' value='".$Lang['iCalendar']['RemoveAndKeepSharersViewRight']."' onclick='confirmDeleteRedirect(#%calID%#)' />";
$deleteOwnCalButton .= "<input type='button' name='' value='".$Lang['iCalendar']['RemoveCalendarCompletely']."' onclick='confirmDeleteCompletelyRedirect(#%calID%#)' />";
$deleteOwnCalButton .= "<input type='button' name='' value='".$Lang['btn_cancel']."' onclick='hideDeleteAlertMsg()' />";


# Main
$CalLabeling  = "<div id='sub_page_title' style='width:50%'>$iCalendar_Calendar_ManageCalendar</div>";
$CalLabeling .= "<div id='btn_link' style='float:right;clear:none'><a href='index.php'>$iCalendar_BackTo_Calendar</a></div>";

$CalMain = '
	<div id="cal_board">
	  <span class="cal_board_02"></span> 
	  <div class="cal_board_content">
		<div id="calendar_wrapper" style="float:left;background-color:#dbedff">
	';

$CalMain .= "<div id='cal_manage_calendar'>";

# Own Calendar
$CalMain .= "	<div style='width: 95%; padding-left: 10px; padding-right: 10px;' id='cal_list_mycal'>
					<h1 style='padding: 0px; font-size: 11px; font-weight: normal; margin-top: 20px; margin-bottom: 0px;'>$iCalendar_Calendar_MyCalendar</h1>
					<div id='btn_link'>
					  <a href='new_calendar.php'><img border='0' align='absmiddle' src='$ImagePathAbs/icon_add.gif'/>$iCalendar_NewCalendar_Header </a>&nbsp;
					  <a href='import.php'><img border='0' align='absmiddle' src='$ImagePathAbs/icon_import.gif'/>$i_Calendar_ImportCalendar</a>
					</div>
					<br style='clear: both;'/>
					<div id='cal_list_mycal'>
						<table width='100%' cellspacing='0' cellpadding='0' border='0' style='background-color:#FFF'>
							<tbody><tr>
								<td class='imail_entry_top' style='width:50%'>$iCalendar_NewEvent_Calendar</td>
								<td class='imail_entry_top'>$iCalendar_NewEvent_HeaderShare</td>
								<td class='imail_entry_top' style='width:130px'>&nbsp;</td>
							</tr>
			";

if (sizeof($ownCalendarList)>0 || $otherCalTypePersonalNum > 0) {
	for ($i=0; $i<sizeof($ownCalendarList); $i++) {
		if ($ownCalendarList[$i]["CalType"] == 4)
			continue;
		$calName = str_replace("'", "\'", $ownCalendarList[$i]["Name"]);
		$colorID = $iCal->Get_Calendar_ColorID($viewableCalColor[$ownCalendarList[$i]["CalID"]]);
		$calViewList = $iCal->returnCalViewerList($ownCalendarList[$i]["CalID"]);
		$calViewerGrpPath = $iCal->returnCalViewerList($ownCalendarList[$i]["CalID"], false, true);
		if (sizeof($calViewList) > 1 || sizeof($calViewerGrpPath) > 0){
			$hidden = $lui->Get_Input_Hidden('isExistViewer_'.$ownCalendarList[$i]["CalID"], 'isExistViewer_'.$ownCalendarList[$i]["CalID"], 1);
			$shared = $iCalendar_ManageCalendar_SharedMsg1;
		} else {
			$hidden = $lui->Get_Input_Hidden('isExistViewer_'.$ownCalendarList[$i]["CalID"], 'isExistViewer_'.$ownCalendarList[$i]["CalID"], 0);
			$shared = $iCalendar_ManageCalendar_SharedMsg2;
		}
		
		$CalMain .= '
				<tr>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_event" style="width:50%">		 
					<div style="position:absolute;z-index:2">
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					</div>
					<div style="width:330px; overflow:hidden; clear:both;padding-left:25px;" >
						<span style="line-height:17px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$ownCalendarList[$i]["Name"].'
						</span>
					</div>
					
				  </td>
				  <td class="cal_agenda_content_entry">'.$shared.$hidden.'</td>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:280px">';
		if($ownCalendarList[$i]["CalID"] != $ownDefaultCalID){
			$CalMain .= '<a class="sub_layer_link" href="javascript:removeCal('.$ownCalendarList[$i]["CalID"].',\''.$calName.'\')">'.$Lang['btn_delete'].'</a> | ';
		}
		if ($ownCalendarURL[$i])
			$CalMain .= '<a class="sub_layer_link" href="synCalendar.php?calID='.$ownCalendarList[$i]["CalID"].'">'.$Lang['syncURL'].'</a> | ';
		$CalMain .= '<a class="sub_layer_link" href="new_calendar.php?calID='.$ownCalendarList[$i]["CalID"].'">'.$Lang['btn_edit'].'</a>';
		$CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$ownCalendarList[$i]["CalID"].'&calType=0&exportType=csv">'.$Lang['btn_export'].'</a>';
		if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
			$CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$ownCalendarList[$i]["CalID"].'&calType='.$ownCalendarList[$i]["CalType"].'&exportType=ical">'.$Lang['btn_export_ical'].'</a>'; 
		}
		$CalMain .= '
				  </td>
				</tr>';
	}
	$CalMain .= $otherCalTypePersonalStr;
} else {
	$CalMain .= "<tr><td colspan='3' class='cal_agenda_content_entry'>$iCalendar_Calendar_NoCalendar</td></tr>";
}
$CalMain .= "</tbody></table><br /></div></div>";
# End of Own Calendar

//if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
#Group Calendar
$CalMain .= '<div id="cal_Groupcal" style="width:95%; padding-left:10px; padding-right:10px">
			   <h1 style="font-size:11px; padding:0px; font-weight:normal; margin-top:20px; margin-bottom:00px">'.$iCalendar_group.'</h1>
			   <div id="cal_list_Groupcal">';
# Calendar which belongs to School assigned by DBAs
$CalMain .= '  	 <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#FFF">
				  <tbody><tr>
					<td class="imail_entry_top" style="width:26%">'.$iCalendar_NewEvent_Calendar.'</td>
					<td class="imail_entry_top" style="width:15%">'.$iCalendar_NewCalendar_ShareTablePermission.'</td>
					<td class="imail_entry_top" style="width:20%">'.$iCalendar_EditEvent_CreatedBy.'</td>
					<td class="imail_entry_top" style="width:100%">&nbsp;</td>
				  </tr>';
if (sizeof($groupCalendarList) > 0) {
	for ($i=0; $i<sizeof($groupCalendarList); $i++) {
		$calName = str_replace("'", "\'", $groupCalendarList[$i]["Name"]);
		$createdBy = $iCalendar_systemAdmin;
		$colorID = $iCal->Get_Calendar_ColorID($groupCalendarList[$i]["Color"]);
		$userPermission = (trim($groupCalendarList[$i]["Access"]) == "W") ? $iCalendar_NewCalendar_SharePermissionEdit : $iCalendar_NewCalendar_SharePermissionRead;
		$CalMain .= '
				<tr>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_event">
				  
				   <div style="position:absolute;z-index:2">
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					</div>
					<div style="width:170px; overflow:hidden; clear:both;padding-left:25px;" >
						<span style="line-height:17px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$groupCalendarList[$i]["Name"].'
						</span>
					</div>
				  
				  <!-- old approach
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">&nbsp;
					<span class=" cal_mange_calendar_list_item cal_calendar_'.$colorID.'">'.$groupCalendarList[$i]["Name"].'</span>
					-->
					
					
				  </td>
				   <td class="cal_agenda_content_entry" style="width:15%">'.$userPermission.'</td>
				  <td class="cal_agenda_content_entry" >'.$createdBy.'</td>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:40%" style="align="right">';
		if ($groupCalendarURL[$i])
				$CalMain .= '<a class="sub_layer_link" href="synCalendar.php?calID='.$groupCalendarList[$i]["CalID"].'">'.$Lang['syncURL'].'</a> | ';
			$CalMain .= '<a class="sub_layer_link" href="new_calendar.php?calID='.$groupCalendarList[$i]["CalID"].'">'.$Lang['btn_edit'].'</a> | ';
			$CalMain .= '<a href="calExport.php?calID='.$groupCalendarList[$i]["CalID"].'&calType='.$groupCalendarList[$i]["CalType"].'&exportType=csv" class="sub_layer_link">'.$Lang['btn_export'].'</a>';
		$CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$groupCalendarList[$i]["CalID"].'&calType='.$groupCalendarList[$i]["CalType"].'&exportType=ical">'.$Lang['btn_export_ical'].'</a>';
		$CalMain .= '
				  </td>
				</tr>';
	}
	$CalMain .= $otherCalTypeSchoolStr;
} else {
    $CalMain .= "<tr><td colspan='3' class='cal_agenda_content_entry'>$iCalendar_Calendar_NoCalendar</td></tr>";
}
$CalMain .= '</tbody></table>';
$CalMain .= '<br />';
$CalMain .= '</div></div>';
#End of Group Calendar


#Course Calendar
$CalMain .= '<div id="cal_schoolcal" style="width:95%; padding-left:10px; padding-right:10px">
			   <h1 style="font-size:11px; padding:0px; font-weight:normal; margin-top:20px; margin-bottom:00px">'.$iCalendar_course.'</h1>
			   <div id="cal_list_schoolcal">';
# Calendar which belongs to School assigned by DBAs
$CalMain .= '  	 <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#FFF">
				  <tbody><tr>
					<td class="imail_entry_top" style="width:26%">'.$iCalendar_NewEvent_Calendar.'</td>
					<td class="imail_entry_top" style="width:15%">'.$iCalendar_NewCalendar_ShareTablePermission.'</td>
					<td class="imail_entry_top" style="width:20%">'.$iCalendar_EditEvent_CreatedBy.'</td>
					<td class="imail_entry_top" style="width:100%">&nbsp;</td>
				  </tr>';
if (sizeof($courseCalendarList) > 0) {
	for ($i=0; $i<sizeof($courseCalendarList); $i++) {
		$calName = str_replace("'", "\'", $courseCalendarList[$i]["Name"]);
		$createdBy = $iCalendar_systemAdmin;
		$colorID = $iCal->Get_Calendar_ColorID($courseCalendarList[$i]["Color"]);
		$userPermission = (trim($courseCalendarList[$i]["Access"]) == "W") ? $iCalendar_NewCalendar_SharePermissionEdit : $iCalendar_NewCalendar_SharePermissionRead;
		$CalMain .= '
				<tr>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_event" >
				  
				   <div style="position:absolute;z-index:2">
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					</div>
					<div style="width:170px; overflow:hidden; clear:both;padding-left:25px;" >
						<span style="line-height:17px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$courseCalendarList[$i]["Name"].'
						</span>
					</div>
				  
				  <!-- old approach
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">&nbsp;
					<span class=" cal_mange_calendar_list_item cal_calendar_'.$colorID.'">'.$courseCalendarList[$i]["Name"].'</span>
					-->
					
					
				  </td>
				   <td class="cal_agenda_content_entry" style="width:15%">'.$userPermission.'</td>
				  <td class="cal_agenda_content_entry" style="width:20%">'.$createdBy.'</td>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:40%" align="right">';
		if ($courseCalendarURL[$i])
				$CalMain .= '<a class="sub_layer_link" href="synCalendar.php?calID='.$courseCalendarList[$i]["CalID"].'">'.$Lang['syncURL'].'</a> | ';		
			$CalMain .= '<a class="sub_layer_link" href="new_calendar.php?calID='.$courseCalendarList[$i]["CalID"].'">'.$Lang['btn_edit'].'</a> | ';
			$CalMain .= '
				  <a href="calExport.php?calID='.$courseCalendarList[$i]["CalID"].'&calType='.$courseCalendarList[$i]["CalType"].'&exportType=csv" class="sub_layer_link">'.$Lang['btn_export'].'</a>';
		$CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$courseCalendarList[$i]["CalID"].'&calType='.$courseCalendarList[$i]["CalType"].'&exportType=ical">'.$Lang['btn_export_ical'].'</a>';
		$CalMain .= '
				  </td>
				</tr>';
	}
	$CalMain .= $otherCalTypeSchoolStr;
} else {
    $CalMain .= "<tr><td colspan='3' class='cal_agenda_content_entry'>$iCalendar_Calendar_NoCalendar</td></tr>";
}
$CalMain .= '</tbody></table>';
$CalMain .= '<br />';
$CalMain .= '</div></div>';
#End of Course Calendar
//}

# Optional
$CalMain .= '<div id="cal_publiccal" style="width:95%; padding-left:10px; padding-right:10px">
			   <h1 style="font-size:11px; padding:0px; font-weight:normal; margin-top:20px; margin-bottom:00px">'.$iCalendar_Calendar_OtherCalendar.'</h1>
			   <div id="btn_link"> <a href="search_calendar.php"><img src="'.$ImagePathAbs.'icon_add.gif" border="0" align="absmiddle">'.$iCalendar_Search.'</a></div>
			   <br style="clear:both">
			   <div id="cal_list_publiccal">';
			   
# Calendar which is shared By Someone
$CalMain .= '  	 <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#FFF">
				  <tbody><tr>
					<td class="imail_entry_top" style="width:26%">'.$iCalendar_SharedCalendar.'</td>
					<td class="imail_entry_top" style="width:15%">'.$iCalendar_NewCalendar_ShareTablePermission.'</td>
					<td class="imail_entry_top" style="width:20%">'.$iCalendar_EditEvent_CreatedBy.'</td>
					<td class="imail_entry_top" style="width:100%">&nbsp;</td>
				  </tr>';
if (sizeof($sharedByCalendarList)>0) {
	for ($i=0; $i<sizeof($sharedByCalendarList); $i++) {
		$userPermission = (trim($sharedByCalendarList[$i]["Access"]) == "W") ? $iCalendar_NewCalendar_SharePermissionEdit : $iCalendar_NewCalendar_SharePermissionRead;
		$calName = str_replace("'", "\'", $sharedByCalendarList[$i]["Name"]);
		$createdBy = $iCal->returnUserFullName($sharedByCalendarList[$i]["Owner"]);
		$colorID = $iCal->Get_Calendar_ColorID($viewableCalColor[$sharedByCalendarList[$i]["CalID"]]);
		$CalMain .= '
				<tr>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_event" style="width:26%">
				  
					<div style="position:absolute;z-index:2">
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					</div>
					<div style="width:170px; overflow:hidden; clear:both;padding-left:25px;" >
						<span style="line-height:17px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$sharedByCalendarList[$i]["Name"].'
						</span>
					</div>
				
				<!--  old approach  
				    <img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">&nbsp;
				    <span class=" cal_mange_calendar_list_item cal_calendar_'.$colorID.'">'.$sharedByCalendarList[$i]["Name"].'</span>
				-->	
					
				  </td>
				  <td class="cal_agenda_content_entry" style="width:15%">'.$userPermission.'</td>
				  <td class="cal_agenda_content_entry" style="width:20%">'.$createdBy.'</td>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:100%" align="right">
				  	<a href="javascript:removeCalFromView('.$sharedByCalendarList[$i]["CalID"].',\''.$calName.'\')" class="sub_layer_link">'.$Lang['btn_remove'].'</a> | ';
					
		if ($sharedByCalendarURL[$i])
			$CalMain .= '<a class="sub_layer_link" href="synCalendar.php?calID='.$sharedByCalendarList[$i]["CalID"].'">'.$Lang['syncURL'].'</a> | ';
			
			
				$CalMain .=    '<a href="new_calendar.php?calID='.$sharedByCalendarList[$i]["CalID"].'" class="sub_layer_link">'.$Lang['btn_edit'].'</a> | 
					<a href="calExport.php?calID='.$sharedByCalendarList[$i]["CalID"].'&calType=0&exportType=csv" class="sub_layer_link">'.$Lang['btn_export'].'</a>';
		if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
			$CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$sharedByCalendarList[$i]["CalID"].'&calType='.$sharedByCalendarList[$i]["CalType"].'&exportType=ical">'.$Lang['btn_export_ical'].'</a>';	
		}
	$CalMain .= '		
				  </td>
				</tr>';
	}
} else {
	$CalMain .= "<tr><td colspan='4' class='cal_agenda_content_entry'>$iCalendar_Calendar_NoCalendar</td></tr>";
}		  
$CalMain .= '</tbody></table>';
$CalMain .= '<br />';
# End of Shared Calendar 

# Calendar which is subscribed in Public Search by User
$CalMain .= '  	 <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#FFF">
				  <tbody><tr>
					<td class="imail_entry_top" style="width:35%">'.$iCalendar_PublicCalendar.'</td>
					<td class="imail_entry_top" style="width:20%">'.$iCalendar_EditEvent_CreatedBy.'</td>
					<td class="imail_entry_top" style="width:100%">&nbsp;</td>
				  </tr>';
if (sizeof($publicSubCalendarList)>0) {
	for ($i=0; $i<sizeof($publicSubCalendarList); $i++) {
		$calName = str_replace("'", "\'", $publicSubCalendarList[$i]["Name"]);
		$createdBy = $iCal->returnUserFullName($publicSubCalendarList[$i]["Owner"]);
		$colorID = $iCal->Get_Calendar_ColorID($viewableCalColor[$publicSubCalendarList[$i]["CalID"]]);
		$CalMain .= '
				<tr>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_event" style="width:35%">
				  
				  <div style="position:absolute;z-index:2">
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					</div>
					<div style="width:240px; overflow:hidden; clear:both;padding-left:25px;" >
						<span style="line-height:17px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$publicSubCalendarList[$i]["Name"].'
						</span>
					</div>
				  
				<!--  old approach
				<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">&nbsp;
				    <span class=" cal_mange_calendar_list_item cal_calendar_'.$colorID.'">'.$publicSubCalendarList[$i]["Name"].'</span> 
				-->
					
				  </td>
				  <td class="cal_agenda_content_entry" style="width:20%">'.$createdBy.'</td>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:100%" align="right">
				  	<a href="javascript:removeCalFromView('.$publicSubCalendarList[$i]["CalID"].',\''.$calName.'\')" class="sub_layer_link">'.$iCalendar_Calendar_Unsubscribe.'</a> | ';
					
		if ($publicSubCalendarURL[$i])
			$CalMain .= '<a class="sub_layer_link" href="synCalendar.php?calID='.$publicSubCalendarList[$i]["CalID"].'">'.$Lang['syncURL'].'</a> | ';
					
			$CalMain .=	'<a href="new_calendar.php?calID='.$publicSubCalendarList[$i]["CalID"].'" class="sub_layer_link">'.$Lang['btn_edit'].'</a> | 
					<a href="calExport.php?calID='.$publicSubCalendarList[$i]["CalID"].'&calType=0&exportType=csv" class="sub_layer_link">'.$Lang['btn_export'].'</a>';
		if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
			$CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$publicSubCalendarList[$i]["CalID"].'&calType='.$publicSubCalendarList[$i]["CalType"].'&exportType=ical">'.$Lang['btn_export_ical'].'</a>';
		}
		$CalMain .= '
				  </td>
				</tr>';
	}
} else {
	$CalMain .= "<tr><td colspan='3' class='cal_agenda_content_entry'>$iCalendar_Calendar_NoCalendar</td></tr>";
}
$CalMain .= '</tbody></table>';
$CalMain .= '<br />';
# End of Public Calendar

if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF || $plugin["iCalendarFullToAllUsers"]){
#System Calendar
# Calendar which is subscribed in Public Search by User
$CalMain .= '  	 <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#FFF">
				  <tbody><tr>
					<td class="imail_entry_top" style="width:40%">'.$iCalendar_Calendar_SystemCalendar.'</td>
					<td class="imail_entry_top" style="width:20%">'.$iCalendar_EditEvent_CreatedBy.'</td>
					<td class="imail_entry_top" style="width:100%">&nbsp;</td>
				  </tr>';

	for ($i=0; $i<sizeof($systemCalendarList); $i++) {
		$calName = str_replace("'", "\'", $systemCalendarList[$i]["Name"]);
		$createdBy = $iCalendar_systemAdmin;
		$colorID = $iCal->Get_Calendar_ColorID($systemCalendarList[$i]['Color']);
		$CalMain .= '
				<tr>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_event" style="width:35%">
				  
				  <div style="position:absolute;z-index:2">
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					</div>
					<div style="width:240px; overflow:hidden; clear:both;padding-left:25px;" >
						<span style="line-height:17px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$systemCalendarList[$i]["Name"].'
						</span>
					</div>
				  
				<!--  old approach
				<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">&nbsp;
				    <span class=" cal_mange_calendar_list_item cal_calendar_'.$colorID.'">'.$$systemCalendarList[$i]["Name"].'</span> 
				-->
					
				  </td>
				  <td class="cal_agenda_content_entry" style="width:20%">'.$createdBy.'</td>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:100%" align="right">';
				  	
			$CalMain .= '<a href="calExport.php?calID='.$systemCalendarList[$i]["CalID"].'&calType='.$systemCalendarList[$i]["CalType"].'&exportType=csv" class="sub_layer_link">'.$Lang['btn_export'].'</a>';
		$CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$systemCalendarList[$i]["CalID"].'&calType='.$systemCalendarList[$i]["CalType"].'&exportType=ical">'.$Lang['btn_export_ical'].'</a>';
		$CalMain .= '
				  </td>
				</tr>';
	}

$CalMain .= '</tbody></table>';
}
$CalMain .= '<br />';





$CalMain .= '</div></div>';
# End of Optional / System Calendar

//if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
# School Calendar 
$CalMain .= '<div id="cal_schoolcal" style="width:95%; padding-left:10px; padding-right:10px">
			   <h1 style="font-size:11px; padding:0px; font-weight:normal; margin-top:20px; margin-bottom:00px">'.$iCalendar_SchoolCalendar.'</h1>
			   <div id="cal_list_schoolcal">';
# Calendar which belongs to School assigned by DBAs
$CalMain .= '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#FFF">
			  <tbody><tr>
					<td class="imail_entry_top" style="width:40%">'.$iCalendar_NewEvent_Calendar.'</td>
					<td class="imail_entry_top" style="width:20%">'.$iCalendar_EditEvent_CreatedBy.'</td>
					<td class="imail_entry_top" style="width:100%">&nbsp;</td>
			  </tr>';
if (sizeof($schoolCalendarList) > 0 || $otherCalTypeSchoolNum > 0) {
	for ($i=0; $i<sizeof($schoolCalendarList); $i++) {
		$calName = str_replace("'", "\'", $schoolCalendarList[$i]["Name"]);
		$createdBy = $iCalendar_systemAdmin;
		$colorID = $iCal->Get_Calendar_ColorID($schoolCalendarList[$i]["Color"]);
		$CalMain .= '
				<tr>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_event" style="width:40%">
				  
				   <div style="position:absolute;z-index:2">
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					</div>
					<div style="width:270px; overflow:hidden; clear:both;padding-left:25px;" >
						<span style="line-height:17px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$schoolCalendarList[$i]["Name"].'
						</span>
					</div>
				  
				  <!-- old approach
					<img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">&nbsp;
					<span class=" cal_mange_calendar_list_item cal_calendar_'.$colorID.'">'.$schoolCalendarList[$i]["Name"].'</span>
					-->
					
					
				  </td>
				  <td class="cal_agenda_content_entry" style="width:20%">'.$createdBy.'</td>
				  <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:40%" align="right">
				  <a href="calExport.php?calID='.$schoolCalendarList[$i]["CalID"].'&calType=1&exportType=csv" class="sub_layer_link">'.$Lang['btn_export'].'</a>';
		$CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$schoolCalendarList[$i]["CalID"].'&calType='.$schoolCalendarList[$i]["CalType"].'&exportType=ical">'.$Lang['btn_export_ical'].'</a>';
		$CalMain .= '
				  </td>
				</tr>';
	}
	$CalMain .= $otherCalTypeSchoolStr;
} else {
    $CalMain .= "<tr><td colspan='3' class='cal_agenda_content_entry'>$iCalendar_Calendar_NoCalendar</td></tr>";
}
$CalMain .= '</tbody></table>';
$CalMain .= '<br />';
$CalMain .= '</div></div>';
# End of School Calendar
//}
# Foundation Calendar - assigned by DBAs in ESFC School
// $CalMain .= '<div id="cal_schoolcal" style="width:95%; padding-left:10px; padding-right:10px">
			   // <h1 style="font-size:11px; padding:0px; font-weight:normal; margin-top:20px; margin-bottom:00px">'.$iCalendar_Calendar_FoundationCalendar.'</h1>
			   // <div id="cal_list_schoolcal">';
// $CalMain .= '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color:#FFF">
			  // <tbody><tr>
				// <td class="imail_entry_top" style="width:40%">'.$iCalendar_NewEvent_Calendar.'</td>
				// <td class="imail_entry_top" style="width:20%">'.$iCalendar_EditEvent_CreatedBy.'</td>
				// <td class="imail_entry_top" style="width:100%">&nbsp;</td>
			  // </tr>';
// if (sizeof($foundationCalendarList) > 0 || $otherCalTypeFoundationNum > 0) {
	// for ($i=0; $i<sizeof($foundationCalendarList); $i++){
		// $calName = str_replace("'", "\'", $foundationCalendarList[$i]["Name"]);
		// $createdBy = $iCal->returnUserFullName($foundationCalendarList[$i]["Owner"], 2);
		// $colorID = $iCal->Get_Calendar_ColorID($foundationCalendarList[$i]["Color"]);
		// $CalMain .= '
				// <tr>
				  // <td class="cal_agenda_content_entry cal_agenda_content_entry_event" style="width:40%"> 
				  // <div style="position:absolute;z-index:2">
					// <img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">
					// </div>
					// <div style="width:270px; overflow:hidden; clear:both;padding-left:25px;" > 
						// <span style="line-height:17px;" class="cal_mange_calendar_list_item cal_calendar_'.$colorID.'" > '.$foundationCalendarList[$i]["Name"].'
						// </span>
					// </div>
				  
				  // <!-- old approach
					// <img src="'.$ImagePathAbs.'calendar/event_'.$colorID.'.png" width="15" height="15" border="0" align="absmiddle" class="cal_event_png_'.$colorID.'">&nbsp;
					// <span class=" cal_mange_calendar_list_item cal_calendar_'.$colorID.'">'.$foundationCalendarList[$i]["Name"].'</span>
					
					// -->
					
				  // </td>
				  // <td class="cal_agenda_content_entry" style="width:20%">'.$createdBy.'</td>
				  // <td class="cal_agenda_content_entry cal_agenda_content_entry_button" style="width:40%" align="right">
				  // <a href="calExport.php?calID='.$foundationCalendarList[$i]["CalID"].'&calType=2&exportType=csv" class="sub_layer_link">'.$Lang['btn_export'].'</a>';
		// $CalMain .= ' | <a class="sub_layer_link" href="calExport.php?calID='.$foundationCalendarList[$i]["CalID"].'&calType='.$foundationCalendarList[$i]["CalType"].'&exportType=ical">'.$Lang['btn_export_ical'].'</a>';
		// $CalMain .= '
				  // </td>
				// </tr>';
	// } 
	// $CalMain .= $otherCalTypeFoundationStr;
// } else {
    // $CalMain .= "<tr><td colspan='3' class='cal_agenda_content_entry'>$iCalendar_Calendar_NoCalendar</td></tr>";
// }
// $CalMain .= '</tbody></table>';
// $CalMain .= '<br />';
// $CalMain .= '</div></div>';
# End of Foundation Calendar

$CalMain .= '</div>';
$CalMain .= 
	'</div>
	  </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>';

$CalMain .= "<div class='jqmWindow' id='deleteCalendarWarning'>
				<p id='delConfirmMsg' align='center'></p>
				<p id='delButton' align='center'></p>
			</div>";
# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain, '','','','','',false,false);
?>
<style type="text/css">@import url(css/main.css);</style>
<style type="text/css">
#deleteCalendarWarning {
	font-family:"Arial","Helvetica","sans-serif";
}
</style>
<?php
// include_once($PathRelative."src/include/template/general_header.php");

echo '<script>';
echo 'var SessionExpiredWarning = "' . $Lang['Warning']['SessionExpired'] . '";';
echo 'var SessionKickedWarning = "' . $Lang['Warning']['SessionKicked'] . '";';
echo 'var SessionExpiredMsg = "' . $SYS_CONFIG['SystemMsg']['SessionExpired'] . '";';
echo 'var SessionKickedMsg = "' . $SYS_CONFIG['SystemMsg']['SessionKicked'] . '";';
echo '</script>';
include_once("headerInclude.php");
?>
<!--
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>

<script type="text/javascript" src="js/jqModal.js"></script>
<link rel="stylesheet" type="text/css" href="css/jqModal.css" />

<script type="text/javascript" src="js/jquery.simpleColor.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
-->
<!-- End jqModal include -->
<script language="javascript">

var isBlocking = false;

$().ready(function() {
	// define the dialog DIVs
	$('#deleteCalendarWarning').jqm({trigger: false});
});

function hideDeleteAlertMsg() {
	$('#deleteCalendarWarning').jqmHide();
}

function confirmDeleteRedirect(calID) {
	window.location = 'delete_calendar.php?calID=' + calID;
}

function confirmDeleteCompletelyRedirect(calID) {
	window.location = 'delete_calendar.php?calID=' + calID + '&removeCompletely=1';
}

function removeCal(calID, calName) {
	var remarkMsg = '';
	var isExistViewer = $('input[@name=isExistViewer_'+calID+']').val();
	if(isExistViewer == 1){
		remarkMsg += "<div style='text-align:left'><?=$Lang['iCalendar']['RemoveSharedCalendarWarning']?></div>";
		remarkMsg += "<br>";
		remarkMsg = remarkMsg.replace("#%calName%#", calName);
	}
	
	var confirmMsg = "<?=$iCalendar_ManageCalendar_ConfirmMsg1?>";
	confirmMsg = confirmMsg.replace("#%calName%#", calName);
	$('#delConfirmMsg').html(remarkMsg+confirmMsg);
	
	var delButton = "";
	if(isExistViewer == 1){
		delButton = "<?=$deleteOwnCalButton?>";
	}else{
		delButton = "<?=$deleteCalButton?>";
	}
	delButton = delButton.replace(/#%calID%#/g, calID);
	$('#delButton').html(delButton);
	
	$('#deleteCalendarWarning').jqmShow();
	$('#deleteCalendarWarning').css('z-index',999999);
}
/*
function removeCalFromView(calID, calName) {
	var confirmMsg = "<?=$iCalendar_ManageCalendar_ConfirmMsg2?>";
	confirmMsg = confirmMsg.replace("#%calName%#", calName);
	$('#delConfirmMsg').html(confirmMsg);
	
	var delButton = "<?=$deleteCalButton?>";
	delButton = delButton.replace("#%calID%#", calID);
	$('#delButton').html(delButton);
	
	$('#deleteCalendarWarning').jqmShow();
}
*/
function removeCalFromView(calID, calName) {
	var confirmMsg = "<?=$iCalendar_ManageCalendar_ConfirmMsg2?>";
	confirmMsg = confirmMsg.replace("#%calName%#", calName);
	$('#delConfirmMsg').html(confirmMsg);
	
	var delButton = "<?=$deleteCalButton?>";
	delButton = delButton.replace("#%calID%#", calID);
	$('#delButton').html(delButton);
	
	$('#deleteCalendarWarning').jqmShow();
	$('#deleteCalendarWarning').css('z-index',999999);
}


function toggleExternalCalVisibility(selfName,eventClass,CalType){
	toggleCalVisibility(eventClass,selfName);
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calType : CalType		
	}, function(responseText) {
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	// isDisplayMoreFunction();
	// jHide_All_Layers();
}

function toggleInvolveEventVisibility(){
	$(".involve").toggle();
}

function toggleSchoolEventVisibility() {
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
}

function toggleCalVisibility(parClassID) {
	$('.cal-'+parClassID).toggle();
	/*
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: parClassID
	}, function() {
	    // callback function
	});
	*/
}

function toggleFoundationEventVisibility(obj){ 
	$('.foundation').toggle();
}

function toggleOwnDefaultCalVisibility(parClassID){
	toggleCalVisibility(parClassID);
	
	var obj = document.getElementById("toggleCalVisible-"+parClassID);
	var involObj = document.getElementById("toggleVisible");
	if(obj.checked == true){
		if(involObj.checked == false)
			toggleInvolveEventVisibility();
		involObj.checked = true;
	} else {
		if(involObj.checked == true)
			toggleInvolveEventVisibility();
		involObj.checked = false;
	}	
}

function submitForm() {
	document.form1.submit();
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>

<div id="module_bulletin" class="module_content">
<?php
	echo $lui->Get_Sub_Function_Header("iCalendar",$Msg);
	echo $display;
?>
</div>
<?php
// include_once($PathRelative."src/include/template/general_footer.php");
include_once("footerInclude.php");
?>
