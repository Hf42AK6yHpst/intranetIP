<?php
// page modifing by: Jason
$PathRelative = "../../../";

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Calendar-GeneralUsage"));

intranet_opendb();
include_once($PathRelative."src/include/class/icalendar.php");

$iCal = new icalendar();

##############################################################################

$userID = $_SESSION["SSV_USERID"];

$sql  = "SELECT a.ReminderID, a.ReminderDate, b.Title, CONVERT(VARCHAR(20), b.EventDate, 120) AS EventDate ";
$sql .= "FROM CALENDAR_REMINDER AS a, CALENDAR_EVENT_ENTRY AS b ";
$sql .= "WHERE a.EventID=b.EventID AND a.ReminderType = 'P' AND a.UserID = $userID ";
$sql .= "AND DATEDIFF(s, '19700101', a.ReminderDate) > (DATEDIFF(s, '19700101', CURRENT_TIMESTAMP)-5) AND DATEDIFF(s, '19700101', a.ReminderDate) < (DATEDIFF(s, '19700101', CURRENT_TIMESTAMP)+10) ";
$sql .= "AND a.LastSent IS NULL ";
$sql .= "ORDER BY a.ReminderDate";

$result = $iCal->returnArray($sql,4);


if (sizeof($result) > 0) {
	$sql = "UPDATE CALENDAR_REMINDER SET LastSent = GETDATE() WHERE ReminderID = ".$result[0]["ReminderID"];
	$iCal->db_db_query($sql);
	/*
	if ($intranet_session_language == "b5")
		echo "alert('".$result[0]["Title"]." 將會在".($amPm=="am"?"上午":"下午").$timeStr."開始');";
	else if ($intranet_session_language == "gb")
		echo "alert('".$result[0]["Title"]." 将会在".($amPm=="am"?"上午":"下午").$timeStr."开始');";
	else
	*/
	$eventDateTimePiece = array();
	$eventTimePiece = array();
	$amPm = '';
	
	$eventDateTime = $result[0]["EventDate"];
	$eventDateTimePiece = explode(" ", $eventDateTime);
	
	$eventTime = (count($eventDateTimePiece) > 0) ? $eventDateTimePiece[1] : '';
	$eventTimePiece = ($eventTime != '') ? explode(":", $eventTime) : '';
	$eventHr = (count($eventTimePiece) > 0) ? $eventTimePiece[0] : '';
	$eventMin = (count($eventTimePiece) > 0) ? $eventTimePiece[1] : '';
	
	if($eventHr > 12){
		$eventHr = $eventHr - 12;
		$amPm = "pm";
	} else {
		$amPm = "am";
	}
	$timeStr = $eventHr.':'.$eventMin.' ';
	
	echo "alert('".$result[0]["Title"]." will start at ".$timeStr.$amPm."');";
}

intranet_closedb();

?>