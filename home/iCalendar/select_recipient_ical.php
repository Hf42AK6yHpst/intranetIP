<?php
// page modifing by: 
$PathRelative = "../../../";
$CurSubFunction = "Communication";

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once("template/popup_header.php");
include_once($PATH_WRT_ROOT."includes/imap_ui.php");
include_once($PATH_WRT_ROOT."includes/imap_address_ui.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

// create UI instance
$lui = new imap_ui();
$LibAddress = new imap_address_ui();

// db instance
$LibDB = new libdb();


# default
# iMail is set as Default module using this page
# $isiCal : 1 (iCalendar uses this page)
# $isiCal : 0 (iMail uses this page)
$isiCal = (isset($isiCal) && $isiCal != "") ? $isiCal : 0; 
$isiCalEvent = (isset($isiCalEvent) && $isiCalEvent != "") ? $isiCalEvent : 0; 

# Set Name of Selection Box
$InputName = ($isiCal == 1) ? "UserID[]" : "Emails[]";

# Set Action of adding Records for icalendar
$UserSubAction = '';
$GroupSubAction = '';
$UserSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['UserID[]'],0);" : "";


if ($srcForm == "")
{
	$srcForm = "NewMailForm";
}

?>
<script language="javascript">
var UserArr = new Array();

function jRollBackStep(val)
{
	document.forma.RollBackStep.value = val;
	document.forma.submit();
}

<?php
	if($isiCal == 1){	// for iCalendar use only
		$fieldname = ($isiCalEvent == 1) ? $fieldname : "calViewerTable";
?>

function checkOption(obj){
    for(i=0; i<obj.length; i++){
        if(obj.options[i].value== ''){
            obj.options[i] = null;
        }
    }
}

function AddRecordsArray(UserName, UserId){
	var len = UserArr.length;
	UserArr[len] = new Array(2);
	UserArr[len][0] = UserName;
	UserArr[len][1] = UserId;
}

<?php 
		if($isiCalEvent == 1){ 
?>
function AddRecords(obj, type) {
	par = window.opener;
	parObj = window.opener.document.form1.elements["<?=$fieldname?>"];
	x = (obj.name == "UserID[]") ? "U" : "G";

	// Case of "Select parent from student"
	//selectedTeachType = document.forma.TeachType.selectedIndex;
	if(x == "G"){
		/*
		if (document.forma.TeachType.options[selectedTeachType].value!="1"){
			if(document.forma.TeachType.options[selectedTeachType].value=="3"){
				x = "P";
			} else {
				selectedUserType2 = document.forma.TeachType2.selectedIndex;
				
				if (document.forma.TeachType2.options[selectedTeachType2].value=="0")
					x = "C";
				else if (document.forma.TeachType2.options[selectedTeachType2].value=="1")
					x = "S";
				else if (document.forma.TeachType2.options[selectedTeachType2].value=="2")
					x = "E";
			}
		}
		*/
	}
	
	checkOption(obj);
	par.checkOption(parObj);
	i = obj.selectedIndex;
	if (i!= -1){
		 window.opener.document.getElementById("saveNsend").disabled = false;
	}
	while(i!=-1) {
		addtext = obj.options[i].text;
		/*
		if (x == "G")
			addtext += " (<?=$i_eClass_ClassLink?>)";
		else if (x == "C")
			addtext += " (Subject)";
		else if (x == "E")
			addtext += " (House)";
		else if (x == "P")
			addtext += "<?=$iCalendar_ChooseViewer_ParentSuffix?>";
		else if (x == "S")
			addtext +=  " (Section)";
		*/
		if (x == "U"){
			//parObj.options[parObj.length] = new Option(addtext, x + obj.options[i].value, false, false);
			par.jAdd_Options_Record(parObj.length, addtext, x + obj.options[i].value);
		}
		obj.options[i] = null;
		i = obj.selectedIndex;
	}
	
}

<?php 
		} else { 
?>

// GroupType
// Level 1 : T - Teaching Staff, 	 N - Non-Teaching Staff, B - School-based Group
// Level 2 : I - Individual, 	 	 C - Subjects, 			 Y - Year,				S - Section, H - House
// Level 3 : A - All teachers(year), R - RollGroup (Year)
function AddRecords(obj, type) {
	par = window.opener;
	parTable = par.document.getElementById("<?=$fieldname?>");
	noOfTr = parTable.tBodies[0].getElementsByTagName("TR");
	
	x = (obj.name == "UserID[]") ? "U" : "G";

	if(x == "G"){
		if(obj.name == "TeachType"){
			var selIndex = document.forma.TeachType.selectedIndex;
			if (document.forma.TeachType.options[selIndex].value == "0"){
				x = "T";
			} else if (document.forma.TeachType.options[selIndex].value =="1"){
				x = "N";
			} else if (document.forma.TeachType.options[selIndex].value =="5"){
				x = "B";
			}
		} else if(obj.name == "TeachType2"){
			var selIndex = document.forma.TeachType2.selectedIndex;
			if (document.forma.TeachType2.options[selIndex].value == "0"){
				x = "I";
			} else if (document.forma.TeachType2.options[selIndex].value == "1"){
				x = "C";
			} else if (document.forma.TeachType2.options[selIndex].value == "2"){
				x = "Y";
			} else if (document.forma.TeachType2.options[selIndex].value == "3"){
				//x = "S";
			} else if (document.forma.TeachType2.options[selIndex].value == "4"){
				//x = "H";
			}
		} else if(obj.name == "TeachSubject"){
			var selIndex = document.forma.TeachSubject.selectedIndex;
			x = "C";
		} else if(obj.name == "SchoolYear"){
			var selIndex = document.forma.SchoolYear.selectedIndex;
			x = "Y";
		} else if(obj.name == "SchoolGroup"){
			var selIndex = document.forma.SchoolGroup.selectedIndex;
			x = "B";
		} else if(obj.name == "Year_Teacher_Type"){
			hasValidID = 0;
			var selIndex = document.forma.Year_Teacher_Type.selectedIndex;
			x = "R";
		}
		
		var teach_type="", teach_type2="", school_group="", teach_subject="", school_year="", year_tea_type="";
		if(document.getElementById("TeachTypePath"))
			teach_type = document.getElementById("TeachTypePath").value;
		if(document.getElementById("TeachType2Path"))
			teach_type2 = document.getElementById("TeachType2Path").value;
		if(document.getElementById("SchoolGroupPath"))
			school_group = document.getElementById("SchoolGroupPath").value;
		if(document.getElementById("TeachSubjectPath"))
			teach_subject = document.getElementById("TeachSubjectPath").value;
		if(document.getElementById("SchoolYearPath"))
			school_year = document.getElementById("SchoolYearPath").value;
		if(document.getElementById("Year_Teacher_TypePath"))
			year_tea_type = document.getElementById("Year_Teacher_TypePath").value;
			
	}
	checkOption(obj); 
	i = obj.selectedIndex;
	
	/*if (i!= -1){
		 window.opener.document.getElementById("saveNsend").disabled = false;
	}*/
	
	while (i!=-1) {
		
		pathText = '';
		nameText = obj.options[i].text;
		if(x != "U"){
			if(teach_type != ""){
				if(teach_type == "T"){
					pathText += teach_type;
					if(obj.name == "TeachType2"){
						pathText += ":=:" + x;
						nameText += " (<?=$Lang['email']['TeachingStaff']?>)";
					} else if(teach_type2 != ""){
						pathText += ":=:" + teach_type2;
						if(teach_type2 == "C"){
							pathText += obj.options[i].value;
							nameText += " (<?=$Lang['email']['TeachingStaff']?>)";
						} else if(teach_type2 == "Y"){
							if(obj.name == "SchoolYear"){
								pathText += obj.options[i].value;
								nameText += " (<?=$Lang['email']['School_Year'].' - '.$Lang['email']['TeachingStaff']?>)";
							} else {
								pathText += school_year;
								if(year_tea_type == ""){
									nameText += " ("+school_year+" - <?=$Lang['email']['School_Year']?>)";
									if(obj.options[i].value == "0"){
										pathText += ":=:" + "A";
									} else if(obj.options[i].value == "1"){
										pathText += ":=:" + "R";
									}
								}
							}
						}
					}
				} else if(teach_type == "B"){
					pathText += teach_type;
					if(obj.name == "SchoolGroup"){
						pathText += ":=:" + x + obj.options[i].value;
						nameText += " (<?=$Lang['email']['SchoolBasedGroup']?>)";
					} 
				}
			} else {
				pathText += x;
			}
		}
		
		firstNode = "<a href='#' class='imail_entry_read_link'><img src='<?=$ImagePathAbs?>imail/icon_ppl_ex.gif' width='20' height='20' border='0' align='absmiddle'>";
		firstNode += nameText;
		firstNode += "</a>";
		//newName = par.document.createTextNode(nameText);
		
		if(x == 'U'){
			var viewerID   = x + obj.options[i].value;
			var permission = viewerID;
		} else {
			var viewerPath = pathText;
			var permission = viewerPath;
		}
		selPermission  = "<select name='permission-" + permission + "' id='permission-" + permission + "'>";
		selPermission += "<option value='W'><?=$Lang['calendar']['SharePermissionEdit']?></option>";
		selPermission += "<option value='R' selected><?=$Lang['calendar']['SharePermissionRead']?></option>";
		selPermission += "</select>";
		
		delViewer  = "<span onclick='removeViewer(this)' class='tablelink'><a href='javascript:void(0)'><?=$Lang['btn_delete']?></a></span>";
		if(x == 'U'){
			delViewer += "<input type='hidden' value='" + viewerID + "' name='viewerID[]' />";
		} else {
			delViewer += "<input type='hidden' value='" + viewerPath + "' name='viewerPath[]' />";
		}
				
		newRow = par.document.createElement("tr");
		//newRow.className = firstRowClass;
	
		newCellName = par.document.createElement("td");
		newCellName.className = "cal_agenda_content_entry";
		newCellName.nowrap = true;
		newCellName.innerHTML = firstNode;
		
		newCellPermission = par.document.createElement("td");
		newCellPermission.className = "cal_agenda_content_entry";
		newCellPermission.innerHTML = selPermission;
		
		newCellDelete = par.document.createElement("td");
		newCellDelete.className = "cal_agenda_content_entry cal_agenda_content_entry_button";
		newCellDelete.innerHTML = delViewer;
		
		newRow.appendChild(newCellName);
		newRow.appendChild(newCellPermission);
		newRow.appendChild(newCellDelete);
		
		parTable.tBodies[0].appendChild(newRow);

		obj.options[i] = null;
		i = obj.selectedIndex;
		
	}
}

<?php 
		}
	} else {	// for iMail use only
?>

function AddRecords(TmpStr)
{
	var ParWindow = window.opener;	
	var ParObj2;
	if (ParWindow)
	{		
		var ParObj2 = window.opener.document.forms["<?=$srcForm?>"].elements["<?php echo $fromSrc; ?>"];
	}
	
	if (ParObj2.value == "")
		ParObj2.value = TmpStr;
	else	
		ParObj2.value += ";"+TmpStr;
}

<?php 
	} 
?>

function jAdd_ACTION(jParAct, jParSrc)
{
	document.forma.specialAction.value = jParAct;
	document.forma.actionLevel.value = jParSrc;
	document.forma.submit();	
}

</script>


<?php

$LoadJavascript = "";

////////////////////////////////////////////////////
//	Step1: Fixed
///////////////////////////////////////////////////
$Step1Arr = array();
$Step1Arr[] = array($Lang['email']['TeachingStaff'],0);
$Step1Arr[] = array($Lang['email']['NonTeachingStaff'],1);

//if ($isMan !=1)
	//$Step1Arr[] = array($Lang['email']['OtherSchoolStaff'],2);
//$Step1Arr[] = array($Lang['email']['Subjects'],2);
//$Step1Arr[] = array($Lang['email']['Sections'],3);
//$Step1Arr[] = array($Lang['email']['Houses'],4);
if($isiCal == 0 && $_REQUEST["SchoolCode"] == $_SESSION["SchoolCode"]){
	$Step1Arr[] = array($Lang['email']['SchoolBasedGroup'],5);
}
$Step1Select = $lui->Get_Input_Select("TeachType","TeachType",$Step1Arr,$TeachType,"size=\"10\" style=\"width: 250px\"  ");

$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['TeachType'], 1);" : "";
$ActionStep1 = "onclick=\"this.form.action='select_recipient_ical.php#step2';".$GroupSubAction."jAdd_ACTION('add','TeachType')\"";
///////////////////////////////////////////////////

$IsEnding = false;
$IsEnding2 = false;
$IsEnding3 = false;
$IsEnding4 = false;
$IsEnding5 = false;

if ($TeachType == "1")
{
// Non-teaching staff	

	$Sql =  	"
					exec Search_StaffEmail 
					@Staff_Type='N,', 
					@Roll_Group=',',					
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' , 
					@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($Emails) > 0)
	{
		$LoadJavascript .= "<script language='javascript' > ";
	}
	}	
	$OutputArr = array();	
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($isiCal == 1)
		{	# iCalendar use 
			$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		
		} else 
		{	# iMail use
			$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
		}
		
		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{		
		if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			$TmpStr = str_replace("'","\'",$TmpStr);			
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}
		}
	}	
	
	$ActionStep2 = "onclick=\"this.form.action='select_recipient_ical.php#step2';".$UserSubAction."jAdd_ACTION('add','step1_2')\"";
	
	$Step2Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
	$IsEnding2 = true;
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($Emails) > 0)
	{
		$LoadJavascript .= "</script> ";
	}
	}	
}
else if ($TeachType == "0")
{
// Teaching staff


/*
	$Sql =  	"
					exec Search_StaffEmail 
					@Staff_Type='T,', 
					@Roll_Group=',',					
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' , 
					@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($Emails) > 0)
	{
		$LoadJavascript .= "<script language='javascript' > ";
	}
	}	
	$OutputArr = array();	
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);		
		
		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{		
		if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			$TmpStr = str_replace("'","\'",$TmpStr);	
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}
		}
	}	
	$ActionStep2 = "onclick=\"this.form.action='select_recipient_ical.php#step2';jAdd_ACTION('add','step1_2')\"";

	
	$Step2Select = $lui->Get_Input_Select_multi("Emails[]","Emails[]",$OutputArr,"","size=\"10\" style=\"width: 200px\" ");
	$IsEnding2 = true;
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($Emails) > 0)
	{
		$LoadJavascript .= "</script> ";
	}
	}
	*/

	///////////////////////////////////////////////////
	//	Step2: Fixed
	///////////////////////////////////////////////////
	$Step2Arr = array();
	$Step2Arr[] = array($Lang['email']['Individual'],0);
	$Step2Arr[] = array($Lang['email']['Subjects'],1);
	$Step2Arr[] = array($Lang['email']['Years'],2);
	//$Step2Arr[] = array($Lang['email']['Sections'],3);
	//$Step2Arr[] = array($Lang['email']['Houses'],4);
	$Step2Select = $lui->Get_Input_Select("TeachType2","TeachType2",$Step2Arr,$TeachType2,"size=\"10\" style=\"width: 250px\"");	
	
	$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['TeachType2'], 1);" : "";		
	$ActionStep2 = "onclick=\"this.form.action='select_recipient_ical.php#step3';".$GroupSubAction."jAdd_ACTION('add','TeachType')\"";
	///////////////////////////////////////////////////

	////////////////////////////////////////////////////
	//	Step3: From DB
	///////////////////////////////////////////////////
	if ($TeachType2 == "0")
	{
		// individual
			$Sql =  	"
					exec Search_StaffEmail 
					@Staff_Type='T,', 
					@Roll_Group=',',					
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' , 
					@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($Emails) > 0)
	{
		$LoadJavascript .= "<script language='javascript' > ";
	}
	}	
	$OutputArr = array();	
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($isiCal == 1)
		{	# iCalendar use 
			$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
		} else  
		{	# iMail use
			$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);		
		}
		
		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{		
		if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			$TmpStr = str_replace("'","\'",$TmpStr);	
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}
		}
	}	
	$ActionStep3 = "onclick=\"this.form.action='select_recipient_ical.php#step3';".$UserSubAction."jAdd_ACTION('add','step1_2')\"";

	$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 200px\" ");
	$IsEnding3 = true;
		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
		}
	} // end if individual
	else if ($TeachType2 == "1")
	{
	// Subjects		

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
		}			
		$Step3Select = $lui->Get_Input_Select("TeachSubject","TeachSubject",$OutputArr,$TeachSubject,"size=\"10\" style=\"width: 250px\" ");
		
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['TeachSubject'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_ical.php#step4';".$GroupSubAction."jAdd_ACTION('add','TeachSubject')\"";
		
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}	
		
		if ($TeachSubject != "")
		{
			$Sql =  	"
							exec Search_StaffEmail 
							@Staff_Type=',', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName='".$TeachSubject.",' , 
							@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
						";
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();	
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				if($isiCal == 1)
				{	# iCalendar use 
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
				} else  
				{	# iMail use
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}
				
				if (($specialAction == "add") && ($actionLevel == "step2_4"))
				{
				if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
				{
					$TmpName = $ReturnArr[$i]["OfficialFullName"];					
					$TmpEmail = $ReturnArr[$i]["Email"];
					if ($TmpName != "")
					{
						$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					}
					else
					{
						$TmpStr = $TmpEmail;
					}
					$TmpStr = str_replace("'","\'",$TmpStr);	
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				}
				
			}
			$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
			$IsEnding4 = true;
		}
		$ActionStep4 = "onclick=\"this.form.action='select_recipient_ical.php#step5';".$UserSubAction."jAdd_ACTION('add','step2_4')\"";		
		
		if (($specialAction == "add") && ($actionLevel == "step2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "</script> ";
		}
		}	
						
		///////////////////////////////////////////////////
	}
	else if ($TeachType2 == "2")
	{
		// Years
		
		$Sql =  "exec Get_SchoolYearList ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
		}
		
		$Step3Select = $lui->Get_Input_Select("SchoolYear","SchoolYear",$OutputArr,$SchoolYear,"size=\"10\" style=\"width: 250px\" ");
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['SchoolYear'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_ical.php#step4';".$GroupSubAction."jAdd_ACTION('add','SchoolYear')\"";
	
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////	
	
		if($SchoolYear != "")
		{
		$Step4Arr = array();
		$Step4Arr[] = array($Lang['email']['All_Teacher'],0);
		$Step4Arr[] = array($Lang['email']['Roll_Group_Teacher'],1);
		
		$Step4Select = $lui->Get_Input_Select("Year_Teacher_Type","Year_Teacher_Type",$Step4Arr,$Year_Teacher_Type,"size=\"10\" style=\"width: 250px\"");	
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['Year_Teacher_Type'], 1);" : "";		
		$ActionStep4 = "onclick=\"this.form.action='select_recipient_ical.php#step5';".$GroupSubAction."jAdd_ACTION('add','Year_Teacher_Type')\"";
		}
		
		/*
		if ($SchoolYear != "")
		{
							$Sql = "
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group=',',					
								@School_Year ='".$SchoolYear.",',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
				
				$OutputArr = array();					
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					if($isiCal == 1)
					{	# iCalendar use 
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					} else  
					{	# iMail use
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					}
					
					if (($specialAction == "add") && ($actionLevel == "step3_5"))
					{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);	
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					}
					
				}	
				
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='select_recipient_ical.php#step4';".$UserSubAction."jAdd_ACTION('add','step3_5')\"";	
			
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
			*/
			/////////////////////////////////////////////////////////////
			///////////////// old Method ////////////////////////////////
			/////////////////////////////////////////////////////////////
			/*
			$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);		
				if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$Roll_Group  = "";
			}			
						
			$Step4Select = $lui->Get_Input_Select("Roll_Group","Roll_Group",$OutputArr,$Roll_Group,"size=\"10\" style=\"width: 250px\" ");
			$ActionStep4 = "onclick=\"this.form.action='select_recipient_ical.php#step5';jAdd_ACTION('add','Roll_Group')\"";
			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			if ($Roll_Group != "")
			{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group='".$Roll_Group.",',					
								@School_Year =',',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();					
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					
					if (($specialAction == "add") && ($actionLevel == "step3_5"))
					{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);	
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					}
					
				}	
				
				$Step5Select = $lui->Get_Input_Select_multi("Emails[]","Emails[]",$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding5 = true;
				
				$ActionStep5 = "onclick=\"this.form.action='select_recipient_ical.php#step5';jAdd_ACTION('add','step3_5')\"";
			}		
			
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
			
			///////////////////////////////////////////////////
			
		} // end if check schoolList
		*/
		
		////////////////////////////////////////////////////
		//	Step5: From DB
		///////////////////////////////////////////////////	
		if($Year_Teacher_Type == "0")
		{
			//echo $SchoolYear;
		if ($SchoolYear != "")
		{
							$Sql = "
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group=',',					
								@School_Year ='".$SchoolYear.",',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				
				//echo "<pre>";
				//var_dump($Sql);
				//echo "</pre>";
							
				//echo "<pre>";
				//var_dump($ReturnArr);
				//echo "</pre>";
				
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
				
				$OutputArr = array();					
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					if($isiCal == 1)
					{	# iCalendar use 
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					} else  
					{	# iMail use
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					}
					
					if (($specialAction == "add") && ($actionLevel == "step3_5"))
					{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);	
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
					}
					}
				}	// end for
				
				$Step5Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding5 = true;
				
				$ActionStep5 = "onclick=\"this.form.action='select_recipient_ical.php#step5';".$UserSubAction."jAdd_ACTION('add','step3_5')\"";	
			
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
			} // end if check schoolList
		} // end if $Year_Teacher_Type == 0
		else if($Year_Teacher_Type == 1)
		{
			//echo $SchoolYear;
		if ($SchoolYear != "")
		{
							$Sql = "
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group=',',
								@School_Year =',',
								@House=',' ,
								@SubjectName=',' , 
								@Teach_Roll_Group=',' , 
								@Roll_Group_Year= '".$SchoolYear.",',
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				
				//echo "<pre>";
				//var_dump($Sql);
				//echo "</pre>";
							
				//echo "<pre>";
				//var_dump($ReturnArr);
				//echo "</pre>";
				
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
				
				$OutputArr = array();					
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					if($isiCal == 1)
					{	# iCalendar use 
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					} else  
					{	# iMail use
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					}
					
					if (($specialAction == "add") && ($actionLevel == "step3_5"))
					{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);	
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					}
					
				}	
				
				$Step5Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding5 = true;
				
				$ActionStep5 = "onclick=\"this.form.action='select_recipient_ical.php#step5';".$UserSubAction."jAdd_ACTION('add','step3_5')\"";	
			
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
			} // end if check schoolList
		} // end if $Year_Teacher_Type == 1
		///////////////////////////////////////////////////
	}
	else if ($TeachType2 == "3")
	{
	// Sections	
	}
	else if ($TeachType2 == "4")
	{
	// House
		$Sql =  "exec Get_HouseList";
		$ReturnArr = $LibDB->returnArray($Sql);
				
		$Step3Select = $lui->Get_Input_Select("House","House",$ReturnArr,$House,"size=\"10\" style=\"width: 250px\" ");		
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_ical.php#step4';jAdd_ACTION('add','House')\"";
		
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////		
		if ($House != "")
		{
/*
			$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
				if ($School_Year_House == $ReturnArr[$i]["School_Year"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$School_Year_House  = "";
			}			
						
			$Step4Select = $lui->Get_Input_Select("School_Year_House","School_Year_House",$OutputArr,$School_Year_House,"size=\"6\" style=\"width: 250px\" ");
			$ActionStep4 = "onclick=\"this.form.action='select_recipient_ical.php#step5';jAdd_ACTION('add','School_Year_House')\"";
*/			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step4_4"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			//if ($School_Year_House != "")
			//{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group=',',					
								@School_Year =',',
								@House='".$House.",' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
//echo $Sql;
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();	
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					if($isiCal == 1)
					{	# iCalendar use 
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					} else  
					{	# iMail use
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					}
					
					if (($specialAction == "add") && ($actionLevel == "step4_4"))
					{					
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);				
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					}
				}	
				
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='select_recipient_ical.php#step5';".$UserSubAction."jAdd_ACTION('add','step4_4')\"";
				
			//}		
			
			if (($specialAction == "add") && ($actionLevel == "step4_4"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}				
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////

		
	}
	///////////////////////////////////////////////////		
		
}
else if ($TeachType == "2")
{
	header("Location: select_school.php?startFrom=ical");
	// Subjects		

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		
		/*$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
		}			
		$Step2Select = $lui->Get_Input_Select("TeachSubject","TeachSubject",$OutputArr,$TeachSubject,"size=\"10\" style=\"width: 250px\" ");
		
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['TeachSubject'], 1);" : "";
		$ActionStep2 = "onclick=\"this.form.action='select_recipient_ical.php#step3';".$GroupSubAction."jAdd_ACTION('add','TeachSubject')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}	
		
		if ($TeachSubject != "")
		{
			$Sql =  	"
							exec Search_StaffEmail 
							@Staff_Type=',', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName='".$TeachSubject.",' , 
							@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
						";
						
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();	
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				if($isiCal == 1)
				{	# iCalendar use 
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
				} else  
				{	# iMail use
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				}
				
				if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
				{
				if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
				{
					$TmpName = $ReturnArr[$i]["OfficialFullName"];					
					$TmpEmail = $ReturnArr[$i]["Email"];
					if ($TmpName != "")
					{
						$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					}
					else
					{
						$TmpStr = $TmpEmail;
					}
					$TmpStr = str_replace("'","\'",$TmpStr);	
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				}
				
			}	
			
			$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
			$IsEnding3 = true;
		}
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_ical.php#step5';".$UserSubAction."jAdd_ACTION('add','step2_2_4')\"";		
		
		if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "</script> ";
		}
		}*/	
						
		///////////////////////////////////////////////////	
}
else if ($TeachType == "3")
{
	// Sections	
		$Sql =  "exec Get_SchoolYearList ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
		}	
		
		$Step2Select = $lui->Get_Input_Select("SchoolYear","SchoolYear",$OutputArr,$SchoolYear,"size=\"10\" style=\"width: 250px\" ");
		
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['SchoolYear'], 1);" : "";
		$ActionStep2 = "onclick=\"this.form.action='select_recipient_ical.php#step3';".$GroupSubAction."jAdd_ACTION('add','SchoolYear')\"";
	
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		if ($SchoolYear != "")
		{
			$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);		
				if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$Roll_Group  = "";
			}			
						
			$Step3Select = $lui->Get_Input_Select("Roll_Group","Roll_Group",$OutputArr,$Roll_Group,"size=\"10\" style=\"width: 250px\" ");
			
			$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['Roll_Group'], 1);" : "";
			$ActionStep3 = "onclick=\"this.form.action='select_recipient_ical.php#step4';".$GroupSubAction."jAdd_ACTION('add','Roll_Group')\"";
			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			if ($Roll_Group != "")
			{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group='".$Roll_Group.",',					
								@School_Year =',',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();					
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					if($isiCal == 1)
					{	# iCalendar use 
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					} else  
					{	# iMail use
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					}
					
					if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
					{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);				
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					}
					
				}	
				
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='select_recipient_ical.php#step4';".$UserSubAction."jAdd_ACTION('add','step3_3_5')\"";
			}		
			
			if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
						
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////
	
}
else if ($TeachType == "4")
{
	// Sections	
		$Sql =  "exec Get_HouseList";
		$ReturnArr = $LibDB->returnArray($Sql);
				
		$Step2Select = $lui->Get_Input_Select("House","House",$ReturnArr,$House,"size=\"10\" style=\"width: 250px\" ");	
		
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['House'], 1);" : "";	
		$ActionStep2 = "onclick=\"this.form.action='select_recipient_ical.php#step2';".$GroupSubAction."jAdd_ACTION('add','House')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		if ($House != "")
		{
			$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
				if ($School_Year_House == $ReturnArr[$i]["School_Year"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$School_Year_House  = "";
			}			
						
			$Step3Select = $lui->Get_Input_Select("School_Year_House","School_Year_House",$OutputArr,$School_Year_House,"size=\"10\" style=\"width: 250px\" ");
			
			$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['School_Year_House'], 1);" : "";	
			$ActionStep3 = "onclick=\"this.form.action='select_recipient_ical.php#step3';".$GroupSubAction."jAdd_ACTION('add','School_Year_House')\"";
			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			if ($School_Year_House != "")
			{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type='T,', 
								@Roll_Group=',',					
								@School_Year ='".$School_Year_House.",',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();	
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					if($isiCal == 1)
					{	# iCalendar use 
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					} else  
					{	# iMail use
						$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					}
					
					if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
					{					
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);				
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					}
				}	
				
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='select_recipient_ical.php#step3';".$UserSubAction."jAdd_ACTION('add','step4_4_4')\"";
				
			}		
			
			if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}				
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////	
	
}
else if ($TeachType == "5")
{
//School groups

	// Subjects		

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$Sql =  " SELECT GroupName, MailGroupID FROM MAIL_ADDRESS_GROUP WHERE GroupType='S' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);		
		}
		$Step2Select = $lui->Get_Input_Select("SchoolGroup","SchoolGroup",$OutputArr,$SchoolGroup,"size=\"10\" style=\"width: 250px\" ");
		
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['SchoolGroup'], 1);" : "";
		$ActionStep2 = "onclick=\"this.form.action='select_recipient_ical.php#step3';".$GroupSubAction."jAdd_ACTION('add','SchoolGroup')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}	
		
		if ($SchoolGroup != "")
		{
			$Sql =  	"
							SELECT
								b.UserName, b.UserEmail, b.MailUserID AS UserID 
							FROM
								MAIL_ADDRESS_MAPPING a,
								MAIL_ADDRESS_USER b
							WHERE
								a.MailUserID = b.MailUserID
								AND a.MailGroupID = '".$SchoolGroup."'	 
						";
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();	
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				if($isiCal == 1)
				{	# iCalendar use 
					$OutputArr[] = array($ReturnArr[$i]["UserName"], $ReturnArr[$i]["UserID"]);
				} else  
				{	# iMail use
					$OutputArr[] = array($ReturnArr[$i]["UserName"]." (".$ReturnArr[$i]["UserEmail"].")", $ReturnArr[$i]["UserID"]);
				}
				
				if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
				{
				if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
				{
					$TmpName = $ReturnArr[$i]["UserName"];					
					$TmpEmail = $ReturnArr[$i]["Email"];
					if ($TmpName != "")
					{
						$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					}
					else
					{
						$TmpStr = $TmpEmail;
					}
					$TmpStr = str_replace("'","\'",$TmpStr);	
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				}
				
			}	
			
			$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
			$IsEnding3 = true;
		}
		$ActionStep3 = "onclick=\"this.form.action='select_recipient_ical.php#step5';".$UserSubAction."jAdd_ACTION('add','step5_2_4')\"";		
		
		if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "</script> ";
		}
		}	
						
		///////////////////////////////////////////////////	
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Adding groups
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ($specialAction == "add")
{
	if ($actionLevel == "TeachType")
	{		
		if($TeachType == 5)
		{
			$Sql = "SELECT
						UserName AS OfficialFullName, UserEmail AS Email, 
						MailUserID AS UserID 
					FROM
						MAIL_ADDRESS_USER b
					WHERE
						UserType = 'S'";
		} 
		else 
		{
			if ($TeachType == "1")
			{
				$Staff_Type = "N";
			}
			else
			{
				$Staff_Type = "T";
			}
			$Sql =  	"
							exec Search_StaffEmail 
							@Staff_Type='".$Staff_Type.",', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName=',' , 
							@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
						";
		}
		$ReturnArr = $LibDB->returnArray($Sql);
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else 
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
			}
		}
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "TeachSubject")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year =',',
						@House=',' ,
						@SubjectName='".$TeachSubject.",' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else 
			{
				$TmpStr = str_replace("'","\'",$TmpStr);
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "SchoolYear")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year ='".$SchoolYear.",',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else 
			{
				$TmpStr = str_replace("'","\'",$TmpStr);	
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "Roll_Group")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group='".$Roll_Group.",',					
						@School_Year =',',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else 
			{
				$TmpStr = str_replace("'","\'",$TmpStr);			
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";	
			}
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "House")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year =',',
						@House='".$House.",' ,
						@SubjectName=',' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
					
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else 
			{
				$TmpStr = str_replace("'","\'",$TmpStr);	
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "School_Year_House")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year ='".$School_Year_House.",',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
						
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else 
			{
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "SchoolGroup")
	{				
		$Sql =  	"
						SELECT
							b.UserName, b.UserEmail
						FROM
							MAIL_ADDRESS_MAPPING a,
							MAIL_ADDRESS_USER b
						WHERE
							a.MailUserID = b.MailUserID
							AND a.MailGroupID = '".$SchoolGroup."'	 
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["UserName"];
			$TmpEmail = $ReturnArr[$i]["UserEmail"];
			$TmpID = '';		# Need UserID
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else 
			{
				$TmpStr = str_replace("'","\'",$TmpStr);			
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
		}			
		$LoadJavascript .= "</script> ";
	}
	else if($actionLevel == "Year_Teacher_Type" && $SchoolYear != "")
	{
		if($Year_Teacher_Type == "0")
		{
			$Sql = "
				exec Search_StaffEmail 
				@Staff_Type=',', 
				@Roll_Group=',',					
				@School_Year ='".$SchoolYear.",',
				@House=',' ,
				@SubjectName=',' , 
				@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
		}
		else if($Year_Teacher_Type == "1")
		{
			$Sql = "
				exec Search_StaffEmail 
				@Staff_Type=',', 
				@Roll_Group=',',
				@School_Year =',',
				@House=',' ,
				@SubjectName=',' , 
				@Teach_Roll_Group=',' , 
				@Roll_Group_Year= '".$SchoolYear.",',
				@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
		}
				
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			$TmpID = $ReturnArr[$i]["UserID"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
						
			if($isiCal == 1)
			{
				//$LoadJavascript .= "AddRecordsArray('".$TmpName."', '".$TmpID."'); \n";
			}
			else 
			{
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
		}
		$LoadJavascript .= "</script> ";
	} // end if Year_Teacher_Type
} // end if action add

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo $LoadJavascript;
echo $lui->Get_Div_Open("module_bulletin"," class=\"module_content\" " );
echo $lui->Get_Div_Open(""," style=\"width:99%\" " );

echo $lui->Get_Div_Open("module_content_header");				
echo $lui->Get_Div_Open("module_content_header_title");				
echo $lui->Get_HyperLink_Open("#");
echo ($isiCal == 1) ? $Lang['calendar']['HeaderTitle'] : $Lang['email']['HeaderTitle'];
echo $lui->Get_HyperLink_Close();
echo $lui->Get_Div_Close();
echo "<br />";
echo $lui->Get_Div_Close();

echo "<br style=\"clear:none\">";


////////////// DEBUG ///////////////////
if(Auth(array("MySchool-TeachingStaff"), "TARGET") || Auth(array("MySchool-NonTeachingStaff"), "TARGET"))
{
//////////// END DEBUG ///////////////////

//echo $lui->Get_Div_Open("commontabs");
echo $lui->Get_Div_Open();
//$SchoolLink .=  $lui->Get_HyperLink_Open("imail_address_book.php?ChooseType=1");
$SchoolLink .= $lui->Get_HyperLink_Open("#");
$SchoolLink .= $lui->Get_Span_Open();
$SchoolLink .= $Lang['email']['SelectRecipient'];
$SchoolLink .= $lui->Get_Span_Close();
$SchoolLink .= $lui->Get_HyperLink_Close();
/*
$PersonalLink .= $lui->Get_HyperLink_Open("imail_address_book.php?ChooseType=2");
$PersonalLink .= $lui->Get_Span_Open();
$PersonalLink .= $Lang['email']['Personal'];
$PersonalLink .= $lui->Get_Span_Close();
$PersonalLink .= $lui->Get_HyperLink_Close();
*/

echo $lui->Get_Div_Close();

//echo "<pre>";
//var_dump($_POST);
//echo "</pre>";
/////////////////////////////////////////
////   Gen the menu list ////////////////
if($RollBackStep == "")
$RollBackStep = 5; // MAX STEP SHOW

$NatArr = array();
$NatArr[]= array($Lang['email']['SelectRecipient'], 0);

if($TeachType != "" && $RollBackStep >= 1)
{
	if($TeachType == 0)
	$NatArr[] = array($Lang['email']['TeachingStaff'], 0);
	else if($TeachType == 1)
	$NatArr[] = array($Lang['email']['NonTeachingStaff'], 1);
	else if($TeachType == 2)
	$NatArr[] = array($Lang['email']['OtherSchoolStaff'], 2);
	else if($TeachType == 5)
	$NatArr[] = array($Lang['email']['SchoolBasedGroup'], 5);
}

if($TeachType2 != "" && $RollBackStep >= 2)
{
	if($TeachType2 == 0)
	$NatArr[] = array($Lang['email']['Individual'], 0);
	else if($TeachType2 == 1)
	$NatArr[] = array($Lang['email']['Subjects'], 1);
	else if($TeachType2 == 2)
	$NatArr[] = array($Lang['email']['Years'], 2);
	
	//else if($TeachType2 == 3)
	//$NatArr[] = array($Lang['email']['Sections'], 3);
}

if($SchoolGroup != "" && $RollBackStep >= 2)
{
	$Sql =  " SELECT GroupName, MailGroupID FROM MAIL_ADDRESS_GROUP WHERE GroupType='S' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($SchoolGroup == $ReturnArr[$i]["MailGroupID"])
		{
			$NatArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);
			break;
		}
	}
} // end school group

if($TeachSubject != "" && $RollBackStep >= 3)
{
	// Subject
	$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
		
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($TeachSubject == $ReturnArr[$i]["Subject_Code"])
		{
			$NatArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
			break;
		}
	}
} // end if Teach Subject

if($House != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_HouseList";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($House == $ReturnArr[$i]["HouseCode"])
		{
			$NatArr[] = array($ReturnArr[$i]["HouseName"], $ReturnArr[$i]["HouseCode"]);
			break;
		}
	}
} // end if Hosue

if($SchoolYear != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_SchoolYearList ";
	$ReturnArr = $LibDB->returnArray($Sql);
			
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($SchoolYear == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);	
			break;
		}	
	}
} // end if school year

if($SchoolYear != "" && $Roll_Group != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
						
	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($Roll_Group == $ReturnArr[$i]["Roll_Group"])
		{
			$NatArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);		
			if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
			{
				$IsFound = true;
			}
			break;
		}
	}	
	if (!$IsFound)
	{
		$Roll_Group  = "";
	}
} // end roll group

if($House != "" && $School_Year_House != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
						
	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($School_Year_House == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
			if ($School_Year_House == $ReturnArr[$i]["School_Year"])
			{
				$IsFound = true;
			}
			break;
		}
	}	
	if (!$IsFound)
	{
		$School_Year_House  = "";
	}
} // end if House->School Year

if($Year_Teacher_Type != "" && $RollBackStep >= 4)
{
	if($Year_Teacher_Type == 0)
	$NatArr[] = array($Lang['email']['All_Teacher'], 0);
	else if($Year_Teacher_Type == 1)
	$NatArr[] = array($Lang['email']['Roll_Group_Teacher'], 1);
}

echo $lui->Get_Div_Open("", 'class="imail_link_menu"');
echo $lui->Get_Span_Open("", 'class="imail_entry_read_link"');

for($i = 0; $i < count($NatArr); $i++)
{
	if($i%3 == 0 && $i != 0)
	$MenuList .= $lui->Get_Br();
	
	$MenuList .= $lui->Get_HyperLink_Open("#", 'onClick="jRollBackStep('.$i.')"');
	if($i == 0)
	$MenuList .= $NatArr[$i][0];
	else
	$MenuList .= " > ".$NatArr[$i][0];
	$MenuList .= $lui->Get_HyperLink_Close();
}

if (isset($_REQUEST["SchoolName"]))
{
	echo $lui->Get_HyperLink_Open("select_school.php?srcFrom=".$srcForm."&fromSrc=".$fromSrc, "") . $_REQUEST["SchoolName"] . $lui->Get_HyperLink_Close();
	echo " > ";
}
echo $MenuList;

echo $lui->Get_Span_Close();
echo $lui->Get_Div_Close();
echo $lui->Get_Br();

/////////////////////////////////////////////////////////////////////
if($RollBackStep < 1)
{
$Year_Teacher_Type = "";
$TeachType = "";
$TeachType2 = "";
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";
$SchoolGroup = "";

$Step2Select = "";
$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 2)
{
$Year_Teacher_Type = "";
$TeachType2 = "";
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";
$SchoolGroup = "";

$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 3)
{
$Year_Teacher_Type = "";
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";

$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 4)
{
$Year_Teacher_Type = "";
$Roll_Group = "";
$School_Year_House = "";
$Step5Select = "";
}

if($Step5Select != "")
{
	$StepSelect = $Step5Select;
	$ActionStep = $ActionStep5;
	$ShowStep = "step5";
	$IsEndingMode = $IsEnding5;
}
else if($Step4Select != "")
{
	$StepSelect = $Step4Select;
	$ActionStep = $ActionStep4;
	$ShowStep = "step5";
	$IsEndingMode = $IsEnding4;
}
else if($Step3Select != "")
{
	$StepSelect = $Step3Select;
	$ActionStep = $ActionStep3;
	$ShowStep = "step4";
	$IsEndingMode = $IsEnding3;
}
else if($Step2Select != "")
{
	$StepSelect = $Step2Select;
	$ActionStep = $ActionStep2;
	$ShowStep = "step3";
	$IsEndingMode = $IsEnding2;
}
else
{
	$StepSelect = $Step1Select;
	$ActionStep = $ActionStep1;
	$ShowStep = "step2";
	$IsEndingMode = $IsEnding;
}

if($isiCal == 1 && $isiCalEvent == 1){
	if( ($TeachType == 1 && $ShowStep == "step3") || 
		($TeachType == 0 && $TeachType2 == 0 && $ShowStep == "step4") || 
		($TeachType == 0 && $TeachType2 == 1 && $TeachSubject != "" && $ShowStep == "step5") || 
		($TeachType == 0 && $TeachType2 == 2 && $SchoolYear != "" && $Year_Teacher_Type != "" && $ShowStep == "step5") ){
		# Only allow add Individual User, not for Group user data
		$StepBtn .=  $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep.' ');
	} else {
		$StepBtn .= "";
	}
} else {
	$StepBtn .=  $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep.' ');
}

if (!$IsEndingMode)
{	
	$StepBtn .=  "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_ical.php#'.$ShowStep.'\';this.form.submit()" ');
}

$StepBtn = $lui->Get_Div_Open("form_btn").$StepBtn.$lui->Get_Div_Close();

$A_Link =  "<a name=\"".$ShowStep."\" ></a>";

//$StepSelect = "";
$Step1Select = "";
$Step2Select = "";
$Step3Select = "";
$Step4Select = "";
$Step5Select = "";

////////////////////////////////////////

echo $lui->Get_Div_Open("commontabs_board", "class=\"imail_mail_content\"");											
echo $lui->Get_Form_Open("forma","POST","select_recipient_ical.php",'');

if (isset($_REQUEST["SchoolCode"]))
{
	echo $lui->Get_Input_Hidden("SchoolCode", "SchoolCode", $_REQUEST["SchoolCode"]);
	echo $lui->Get_Input_Hidden("SchoolName", "SchoolName", $_REQUEST["SchoolName"]);
}
echo $lui->Get_Input_Hidden("isMan", "isMan", $isMan);
if ($StepSelect != "")
{	
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $StepSelect;
	echo $StepBtn;
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();		
}

if($TeachType != ""){
	echo $lui->Get_Input_Hidden("TeachType", "TeachType", $TeachType)."\n";
	if($isiCal == 1){
		switch($TeachType){
			case 0: $char = 'T'; break;
			case 1: $char = 'N'; break;
			case 5: $char = 'B'; break;
		}
		echo $lui->Get_Input_Hidden("TeachTypePath", "TeachTypePath", $char)."\n";
	}
}
if($TeachType2 != ""){
	echo $lui->Get_Input_Hidden("TeachType2", "TeachType2", $TeachType2)."\n";
	if($isiCal == 1){
		switch($TeachType2){
			case 0: $char = 'I'; break;
			case 1: $char = 'C'; break;
			case 2: $char = 'Y'; break;
		}
		echo $lui->Get_Input_Hidden("TeachType2Path", "TeachType2Path", $char)."\n";
	}
}
if($TeachSubject != ""){
	echo $lui->Get_Input_Hidden("TeachSubject", "TeachSubject", $TeachSubject)."\n";
	if($isiCal == 1){
		echo $lui->Get_Input_Hidden("TeachSubjectPath", "TeachSubjectPath", $TeachSubject)."\n";
	}
}
if($SchoolYear != ""){
	echo $lui->Get_Input_Hidden("SchoolYear", "SchoolYear", $SchoolYear)."\n";
	if($isiCal == 1){
		echo $lui->Get_Input_Hidden("SchoolYearPath", "SchoolYearPath", $SchoolYear)."\n";
	}
}
if($Roll_Group != ""){
	echo $lui->Get_Input_Hidden("Roll_Group", "Roll_Group", $Roll_Group)."\n";
	if($isiCal == 1){
		//echo $lui->Get_Input_Hidden("Roll_GroupPath", "Roll_GroupPath", $Roll_Group)."\n";
	}
}
if($House != ""){
	echo $lui->Get_Input_Hidden("House", "House", $House)."\n";
	if($isiCal == 1){
		//echo $lui->Get_Input_Hidden("HousePath", "HousePath", $House)."\n";
	}
}
if($School_Year_House != ""){
	echo $lui->Get_Input_Hidden("School_Year_House", "School_Year_House", $School_Year_House)."\n";
}
if($SchoolGroup != ""){
	echo $lui->Get_Input_Hidden("SchoolGroup", "SchoolGroup", $SchoolGroup)."\n";
	if($isiCal == 1){
		echo $lui->Get_Input_Hidden("SchoolGroupPath", "SchoolGroupPath", $SchoolGroup)."\n";
	}
}
if($Year_Teacher_Type != ""){
	echo $lui->Get_Input_Hidden("Year_Teacher_Type", "Year_Teacher_Type", $Year_Teacher_Type)."\n";
	if($isiCal == 1){
		switch($Year_Teacher_Type){
			case 0: $char = 'A'; break;
			case 1: $char = 'R'; break;
		}
		echo $lui->Get_Input_Hidden("Year_Teacher_TypePath", "Year_Teacher_TypePath", $char)."\n";
	}
}
# Check for iCalendar use only
if($isiCal == 1){
	echo $lui->Get_Input_Hidden("isiCal", "isiCal", $isiCal)."\n";
}
if($isiCalEvent == 1){
	echo $lui->Get_Input_Hidden("isiCalEvent", "isiCalEvent", $isiCalEvent)."\n";
	echo $lui->Get_Input_Hidden("fieldname", "fieldname", $fieldname)."\n";
}

////////////////////    Show Step 1    ////////////////////////////
if ($Step1Select != "")
{
echo $lui->Get_Paragraph_Open("align=\"center\"");
echo $Step1Select;

echo $lui->Get_Div_Open("form_btn");
echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep1.' ');
if (!$IsEnding)
{	
	echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_ical.php#step2\';this.form.submit()" ');
}
echo $lui->Get_Div_Close();

echo $lui->Get_Paragraph_Close();

echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
echo $lui->Get_Paragraph_Close();
}
////////////////////////////////////////////////////////////////

if ($Step2Select != "")
{	
	echo "<a name=\"step2\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step2Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep2.' ');
	if (!$IsEnding2)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_ical.php#step3\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();		
} // end if show step2

if ($Step3Select != "")
{
	echo "<a name=\"step3\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step3Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep3.' ');
	if (!$IsEnding3)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_ical.php#step4\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step3

if ($Step4Select != "")
{
	echo "<a name=\"step4\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step4Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep4.' ');
	if (!$IsEnding4)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_ical.php#step5\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step4

if ($Step5Select != "")
{
	echo "<a name=\"step5\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step5Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep5.' ');
	if (!$IsEnding5)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient_ical.php#step5\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
}

} //end check auth right

else
{
echo $lui->Get_Div_Open("commontabs");
echo $lui->Get_HyperLink_Open("#");
echo $lui->Get_Span_Open();
echo "You have no premission to select recipient";
echo $lui->Get_Span_Close();
echo $lui->Get_HyperLink_Close();
}


echo $lui->Get_Div_Open("form_btn");
echo $lui->Get_Input_Button("btn_close1","button",$Lang['btn_close_window'],'class="button_main_act" onclick="window.close()" ');
echo $lui->Get_Div_Close();

echo $lui->Get_Input_Hidden("actionLevel","actionLevel");
echo $lui->Get_Input_Hidden("specialAction","specialAction");
echo $lui->Get_Input_Hidden("fromSrc","fromSrc",$fromSrc);
echo $lui->Get_Input_Hidden("srcForm","srcForm",$srcForm);
echo $lui->Get_Input_Hidden("RollBackStep","RollBackStep"); // to roll back the last step
echo $lui->Get_Form_Close();

echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();

include_once("template/popup_footer.php");
?>
