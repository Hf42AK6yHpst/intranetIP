<?php
// using: Ronald

################################################## Change Log ##################################################
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "EditBookingRequest"
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "CancelBookingRequest"
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "DeleteBookingRequest"

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$li = new libebooking_ui();


$ItemID = $_REQUEST['ItemID'];

include_once($PATH_WRT_ROOT."includes/libinterface.php");
$linterface = new interface_html();

# Get Booking Day Beforehand
include_once($PATH_WRT_ROOT."includes/libebooking_item.php");
$objItem = new eBooking_Item($ItemID);
$BookingDayBeforehand = $objItem->BookingDayBeforehand;

# Get Booking Info
//$DateTimeArr = unserialize(rawurldecode($_REQUEST['DateTimeArr']));

if($AllDayEvent == 1) {
	$StartTime = date("H:i:s", mktime(0,0,0,0,0,0));
	$EndTime = date("H:i:s", mktime(23,55,0,0,0,0));
} else {
	$StartTime = date("H:i:s", mktime($StartHour,$StartMins,0,0,0,0));
	$EndTime = date("H:i:s", mktime($EndHour,$EndMins,0,0,0,0));
}

$DateArr = array($BookingStart);
$tempArr[0]["StartTime"] = $StartTime;
$tempArr[0]["EndTime"] = $EndTime;
$arrDate[$BookingStart][] = $tempArr[0];

$ItemBookingDetailsTable = $li->Get_Item_Booking_Details_Table($ItemID, $arrDate);

# Get Available Booking Period
$DateArr = array_keys($arrDate);

$SinceDate = $DateArr[0];
$UntilDate = $DateArr[count($DateArr)-1];

$ItemAvailableBookingPeriodTable = $li->Get_Available_Booking_Period_Layer_Table($FacilityType=1, $ItemID, $SinceDate, $UntilDate);

$thisReturn = '';
$thisReturn .= $Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDayBeforehand'].': '.$BookingDayBeforehand.' '.$Lang['eBooking']['Settings']['GeneralPermission']['DayBefore'];
$thisReturn .= '<span class="tabletextremark"> ('.$Lang['eBooking']['Settings']['GeneralPermission']['RemarksArr']['ZeroBookingDayBeforehandMeansNoLimit'].')</span>';
$thisReturn .= '<br />';
$thisReturn .= '<br />';
$thisReturn .= '<div class="edit_bottom" style="height:1px;"></div>';
$thisReturn .= $linterface->GET_NAVIGATION2($Lang['eBooking']['eService']['FieldTitle']['ExistingBookingRecord']);
$thisReturn .= '<br />';
$thisReturn .= $ItemBookingDetailsTable;
$thisReturn .= '<br />';
$thisReturn .= '<br />';
$thisReturn .= $linterface->GET_NAVIGATION2($Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod']);
$thisReturn .= $ItemAvailableBookingPeriodTable;
$thisReturn .= '<br />';

echo $thisReturn;

intranet_closedb();
die;
?>