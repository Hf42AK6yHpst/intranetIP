<?php
// page editing by:

class file_system {

        var $rs = array();

        function file_system(){
        }

        ######################################################################

        function file_escapeshellarg($file){
                return escapeshellarg($file);
        }

        function file_write($body, $file){
                $x = (($fd = fopen($file, "w")) && (fputs($fd, $body))) ? 1 : 0;
                fclose ($fd);
                return $x;
        }

        function file_read($file){
                 clearstatcache();
                 if(file_exists($file) && is_file($file) && filesize($file)!=0){
                    $x =  ($fd = fopen($file, "r")) ? fread($fd,filesize($file)) : "";
                    if ($fd)
                        fclose ($fd);
                 }
                 return $x;

        }

        function file_read_csv($file)
        {
                        $i = 0;
                        if(file_exists($file) && filesize($file)!=0)
                        {
                                $fp = fopen($file,"r");
                                # check if function mb_substr and mb_strlen are supported, don't use fgetcsv() for the special character sake
                                if(function_exists("mb_substr") && function_exists("mb_strlen"))
                                {
                                        while (!feof($fp))
                                        {                                         
												
												$buffer = trim(fgets($fp, 4096));	
												
                                                # combine contents that contain link break
                                                $LastCell = substr(strrchr($buffer, ","), 1);												
                                                if(substr($LastCell, 0, 1)=="\"")
                                                {                                                      
														while(!feof($fp) && substr($buffer, -1)!="\"")
                                                        {
                                                                $buffer .= "\n".trim(fgets($fp, 4096));
																
                                                        }
                                                }
                                                $data = $this->mb_csv_split($buffer);
                                                $x[$i++] = $data;
                                        }
                                }
                                else
                                {
                                        while($data = fgetcsv($fp,filesize($file))){
                                                $x[$i++] = $data;
                    }
                    }
                                fclose ($fp);
            }
            return $x;
        }
		
		function file_read_outlook_csv($file)
        {
            $i = 0;
            if(file_exists($file) && filesize($file)!=0)
            {
                $fp = fopen($file,"r");
                # check if function mb_substr and mb_strlen are supported, don't use fgetcsv() for the special character sake
                
				while($data = fgetcsv($fp)) {
					$x[$i++] = $data;			
				}	
				/*
				if(function_exists("mb_substr") && function_exists("mb_strlen"))
                {
                    while (!feof($fp))
                    {                                         
						$buffer = trim(fgets($fp, 4096));
						
						if($tmpBuffer != "") {							
							$buffer1 = $tmpBuffer.$buffer;							
						}else{
							$buffer1 = $buffer;	
						}
						print $buffer.'<br><br><br>';
                        # combine contents that contain link break
                        $LastCell = substr(strrchr($buffer, ","), 1);												
                        if(substr($LastCell, 0, 1)=="\"")
                        {                                                      
							while(!feof($fp) && substr($buffer, -1)!="\"")
                            {
                                $buffer .= "\n".trim(fgets($fp, 4096));																
                            }
                        }
												
                        $data = $this->mb_csv_split($buffer1);
						if($i==0) {
							$colNum = count($data);	
						}							
						
						if(count($data) < $colNum) {
							$tmpBuffer = $buffer1;	
						}elseif(count($data) > $colNum) {
							$data1 = $this->mb_csv_split($tmpBuffer);
							$x[$i++] = $data1;
							
							$data2 = $this->mb_csv_split($buffer);
							if(count($data2) < $colNum || count($data2) > $colNum) {
								$tmpBuffer = $buffer;								
							}else{
								$x[$i++] = $data2;
								$tmpBuffer = "";		
							}
						}else{
							$x[$i++] = $data;
							$tmpBuffer = "";	
						}						                     
                    }
					# get the last record
					if($tmpBuffer != "") {
						$data = $this->mb_csv_split($tmpBuffer);
						$x[$i++] = $data; 	
					}
                }
                else
                {
                    while($data = fgetcsv($fp,filesize($file))){
                        $x[$i++] = $data;
                    }
                }
				*/
                fclose ($fp);
            }
            return $x;
        }

        function mb_substr_ed($str, $start, $length=NULL, $encoding=NULL)
        {
        	global $g_encoding_unicode, $intranet_default_lang, $intranet_default_lang_set;

        	if ($length==NULL)
        	{
        		$length = $this->mb_strlen_ed($str);
        	}

        	if (!$g_encoding_unicode && ($intranet_default_lang=="en" || $intranet_default_lang=="b5") && (!isset($intranet_default_lang_set) || (isset($intranet_default_lang_set) && !in_array("gb", $intranet_default_lang_set))) )
        	{
        		# Explicitly specify BIG5 as encoding for ANSI case if system is using either Eng or Big5
				$strNow = (@mb_substr($str, $start, $length, "BIG5"));
        		if ($strNow=="")
        		{
        			$strNow = mb_substr($str, $start, $length);
				}
				return $strNow;
        	} else
        	{
        		return mb_substr($str, $start, $length);
        	}
        }

		function mb_strlen_ed($str, $encoding=NULL)
        {
        	global $g_encoding_unicode, $intranet_default_lang, $intranet_default_lang_set;

        	if (!$g_encoding_unicode && ($intranet_default_lang=="en" || $intranet_default_lang=="b5") && (!isset($intranet_default_lang_set) || (isset($intranet_default_lang_set) && !in_array("gb", $intranet_default_lang_set))) )
        	{
        		# Explicitly specify BIG5 as encoding for ANSI case if system is using either Eng or Big5
        		$length = (@mb_strlen($str, "BIG5"));
        		if ($length=="")
        		{
        			$length = mb_strlen($str);
				}
				return $length;
        	} else
        	{
        		return mb_strlen($str);
        	}
        }

		// new method to handle special characters of csv file content
		function mb_csv_split($line, $delim = ',', $removeQuotes = true)
		{
			$fields = array();
			$fldCount = 0;
			$inQuotes = false;

			for ($i = 0; $i < $this->mb_strlen_ed($line); $i++) {
				if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
				$tmp = $this->mb_substr_ed($line, $i, $this->mb_strlen_ed($delim));
				if ($tmp === $delim && !$inQuotes) {
					$fldCount++;
					$i+= $this->mb_strlen_ed($delim) - 1;
				}
				else if ($fields[$fldCount] == "" && $this->mb_substr_ed($line, $i, 1) == '"' && !$inQuotes) {
					if (!$removeQuotes) $fields[$fldCount] .= $this->mb_substr_ed($line, $i, 1);
					$inQuotes = true;
				}
				else if ($this->mb_substr_ed($line, $i, 1) == '"') {
					if ($this->mb_substr_ed($line, $i+1, 1) == '"') {
							$i++;
							$fields[$fldCount] .= $this->mb_substr_ed($line, $i, 1);
					} else {
							if (!$removeQuotes) $fields[$fldCount] .= $this->mb_substr_ed($line, $i, 1);
							$inQuotes = false;
					}
				}
				else {
					$fields[$fldCount] .= $this->mb_substr_ed($line, $i, 1);
				}
			}

			return $fields;
		}

        function file_name($file){
                $file = basename($file);
                return substr($file, 0, strpos($file,"."));
        }

        function file_ext($file){
                $file = basename($file);
                return strtolower(substr($file, strrpos($file,".")));
        }

        function file_remove($file)
        {
                return (file_exists($file)) ? unlink($file) : 0;
        }

        function file_copy($source, $dest){
                $dest .= (is_dir($dest)) ? "/".basename($source) : "";
                return copy($source, $dest);
        }

        function file_rename($oldfile, $newfile){
                return rename($oldfile, $newfile);
        }

        function file_unzip($file, $dest){
                if(is_file($file)) {
                    
         
                        $file = $this->file_escapeshellarg($file);
                        $dest = $this->file_escapeshellarg($dest);
                        
                        $file = OsCommandSafe($file);
                        $dest = OsCommandSafe($dest);

                                                exec("zip -F $file");
                        exec("unzip $file -d $dest");

                        return 1;
                }
                return 0;
        }


        function file_zip($file, $dest, $to_dir="")
        {
                 $file = $this->file_escapeshellarg($file);
                 $dest = $this->file_escapeshellarg($dest);
                 
                 $file = OsCommandSafe($file);
                 $dest = OsCommandSafe($dest);
                 
                 chdir("$to_dir");
                 $Str = exec("zip -r $dest $file");
        }

        function file_set_modification_time($file, $time=""){
                $time = ($time == "") ? time() : $time;
                return touch($file, $time);
        }

         function file_get_modification_time($file){
                return filemtime($file);
        }

                # Unzip File Using Shell Commands
                function unzipFile($myFilePath, $myDest, $myPassword=""){
                        $result_arr = array();

                        // copy to temporary file named
                        $tmp_file = session_id()."_".time().".zip";
                        $tmp_src = "/tmp/".$tmp_file;
                        copy($myFilePath, $tmp_src);

                        $tmp_src = OsCommandSafe($tmp_src);
                        $dest= OsCommandSafe($dest);
                        
                        $dirNow = getcwd();
                        // change to zip file
                        chdir($myDest);
                        exec("zip -F $myPassword \"{$tmp_src}\" ");
                        exec("unzip $myPassword \"{$tmp_src}\" ", $result_arr);

                        // avoid failure of file reading
                        $this->chmod_R($myDest, 0777);

                        // delete temporary file
                        unlink($tmp_src);
                        chdir($dirNow);

                        return $result_arr;
                }

                function chmod_R($path, $filemode)
                {
                        clearstatcache();
                        if (!is_dir($path))
                        {
                                return chmod($path, $filemode);
                        }

                        $dh = opendir($path);
                        while ($file = readdir($dh))
                        {
                                if ($file != '.' && $file != '..')
                                {
                                        $fullpath = $path.'/'.$file;
                                        chmod($fullpath, $filemode);
                                        if(!is_dir($fullpath))
                                        {
                                                if (!chmod($fullpath, $filemode))
                                                        return FALSE;
                                        } else
                                        {
                                                if (!$this->chmod_R($fullpath, $filemode))
                                                        return FALSE;
                                        }
                                }
                        }

                        closedir($dh);

                        if (chmod($path, $filemode))
                                return TRUE;
                        else
                                return FALSE;
                }
        ######################################################################

        function folderlist($location){
                # prevent error if folder does not exist
                clearstatcache();
                if (!file_exists($location))
                {
                        return false;
                }

                $d = dir($location);

                while($entry=$d->read()) {
                        $filepath = $location . "/" . $entry;
                        if (is_dir($filepath) && $entry<>"." && $entry<>".."){
                                        $this->return_folderlist($filepath);
                                $this->rs[sizeof($this->rs)] = $filepath;
                        }
                        else if (is_file($filepath))
                                $this->rs[sizeof($this->rs)] = $filepath;

                }
                $d->close();
        }

        function return_folderlist($location){
                        # added by PeterHo 23-08-2006

                        // Move to return_folder to avoid repeated clearing of result set
                        //$this->rs = array();              # clear rs

                        # end
                        $this->folderlist($location);

                        return $this->rs;
        }

        function return_folder($location)
        {

                        $this->rs = array();              # clear rs

                $folders = array();
                $j = 0;
                $row = $this->return_folderlist($location);


                for($i=0; $i<sizeof($row); $i++){
                        if(is_dir($row[$i])) $folders[$j++] = $row[$i];
                }


                return $folders;
        }

        ######################################################################

        function item_remove($file){
                return (is_file($file)) ? $this->file_remove($file) : $this->folder_remove($file);
        }

        function item_copy($file, $dest){
                return (is_file($file)) ? $this->file_copy($file, $dest) : $this->folder_new($dest);
        }

        ######################################################################

        function folder_new($location, $Mode=0777, $Recursive=false){
                #umask(0);				
                return (file_exists($location)) ? 0 : mkdir($location, $Mode, $Recursive);
        }

        function folder_remove($file){
                return (file_exists($file)) ? rmdir($file) : 0;
        }

        function folder_remove_recursive($location){
                if(is_dir($location)){
                        $row = $this->return_folderlist($location);
                        for($i=0; $i<sizeof($row); $i++){
                                $this->item_remove($row[$i]);
                        }
                        $this->item_remove($location);
                }
                return 1;
        }

        function folder_copy($source, $dest){
                $row = $this->return_folderlist($source);
                $ext = str_replace(substr($source,0,strrpos($source,"/")),'',$source);
                $this->item_copy($source, $dest.$ext);
                for($i=sizeof($row)-1; $i>=0; $i--){
                        $file = $row[$i];
                        if($file=="." || $file=="..") continue;
                        $ext = str_replace(substr($source,0,strrpos($source,"/")),'',$file);
                        $this->item_copy($file, $dest.$ext);
                }
                return 1;
        }

        function folder_size($location){
                $file_size = 0;
                $file_no = 0;
                $dir_no = 0;
                if(is_dir($location)){
                        $row1 = $this->return_folderlist($location);
                        for($i=0; $i<sizeof($row1); $i++){
                                if(is_file($row1[$i])){

                                        $file_size += filesize($row1[$i]); $file_no++;
                                }else{
                                        $dir_no++;
                                }
                        }
                }else{
                        $file_size += filesize($location); $file_no++;
                }
                $size = array($file_size, $file_no, $dir_no);
                return $size;
        }


        ###########################################################################
        function word2html ($wordfile, $dest, $output_file)
        {
                 global $external_path,$intranet_session_language;
                 $wv_path = $external_path['WV'];
                 $charset = ($intranet_session_language=="gb") ? "gb2312" : "big5";               
                
                 $dest = OsCommandSafe($dest);
                 $wordfile = OsCommandSafe($wordfile);
                 $output_file = OsCommandSafe($output_file);
                 
                 exec("$wv_path/wvHtml --targetdir=$dest --charset=$charset $wordfile $output_file");
        }

        function readWordFileContent ($wordfile)
        {
                 global $intranet_root;
                 $output_file = session_id()."-".time().".html";
                 $dest = "/tmp";
                 $this->word2html($wordfile,$dest,$output_file);
                 $document = $this->file_read("$dest/$output_file");
                 return $this->filterHTMLFile($document);
        }

        function convertWordFileWImage ($wordfile, $dest, $output_file)
        {
                 global $intranet_root, $intranet_httppath;

                 # Convert File
                 $exact_dest = "$intranet_root/$dest";
                 $this->word2html($wordfile,$exact_dest,$output_file);
                 $document = $this->file_read("$exact_dest/$output_file");
                 $filtered = $this->filterHTMLFile($document);

                 # Process images directory append
                 $image_dir = "$intranet_httppath/$dest";
                 $text = str_replace("src=\"","src=\"$image_dir/",$filtered);
                 return $text;

//check file attached (check for more than one file)
/*
$image_tag = '<img';
$image_src = 'src="';
$image_src_e = '"';
while (is_integer($pos_tag = strpos($tmpTxt, $image_tag)) && is_integer($pos_f = strpos($tmpTxt, $image_src))) {
     $pos_src = $pos_f+strlen($image_src);
     $pos_src_e = strpos($tmpTxt, $image_src_e, $pos_src);
     $fileInside[] = substr($tmpTxt, $pos_src, $pos_src_e-$pos_src);
     $tmpTxt = substr($tmpTxt, 0, $pos_tag) . substr($tmpTxt, $pos_src_e+2);
}
*/
        }


        function filterHTMLFile($sTxt)
        {
                 global $im_field;

                 $xStr = "";
                 $separator_s = 'White; ">';
                 $separator_e = '</p></div>';
                 $image_tag = '<img';
                 $image_src = 'src="';
                 $image_src_e = '"';
                 $table_t0 = '<table';
                 $table_tr0 = '<tr';
                 $table_td0 = '<td';
                 $table_t1 = '</table>';
                 $table_tr1 = '</tr>';
                 $table_td1 = '</td>';
                 $op_start_tag = '<ol type="';
                 $op_end_tag = '">';
                 $ol_end_tag = '</ol>';
                 $op_tag_s = '<li value="';
                 $op_tag_e = '"><p><div';
                 $option_letter_start = 65;

                 $pos_e = 0;
                 $pos_pre = 0;
                 $option_start = false;
                 $option_found = false;
                 $op_count = 0;

                 unset($fileInside);
                 while(is_integer($pos_s = strpos($sTxt, $separator_s, $pos_e)))
                 {

                       //capture question contents
                       if (is_integer($pos_e = strpos($sTxt, $separator_e, $pos_s))) {

                       if (!$option_start && $pos_pre!=0 && $pos_e != 0 )
                       {
                                # Check whether is multiple choice answer options
                                $check_str = substr($sTxt,$pos_pre,$pos_e-$pos_pre);
                                //echo "At $pos_pre to $pos_e : ".$check_str."\n<br>\n";
                                $num_op_found = substr_count($check_str,$op_start_tag);
                                $op_start_pos = 0;
                                if ($num_op_found != 0)
                                {
                                    //echo "Found ol for $num_op_found times \n<br>\n";
                                    for ($i=0; $i<$num_op_found; $i++)
                                    {
                                         $op_start_pos = strpos($check_str,$op_start_tag,$op_start_pos+1);
                                    }

                                    $option_start = true;
                                    $end_pos = strpos($check_str,$op_end_tag,$op_start_pos);
                                    $op_type = substr($check_str,$op_start_pos+10,$end_pos-$op_start_pos-10);
                                    $option_letter_start = ord($op_type);
                                    //echo "Got type as ".htmlspecialchars($op_type)." from $op_start_pos to $end_pos\n<br>\n";
                                    /*
                                    $check_str = substr($sTxt, $op_start_pos);
                                    echo "Found start on : $op_start_pos to ".($pos_e-$pos_pre)." : $check_str\n<br>\n";
                                    echo "Left that : ".substr($sTxt,$op_start_pos);
                                    echo "\n<br>\n\n";
                                    */
                                }
                       }

                       if ($option_start)
                       {
                           $check_str = substr($sTxt,$pos_pre,$pos_e-$pos_pre);
                           # Trace </ol>
                           $end_pos = strpos($check_str, $ol_end_tag);
                           if (is_integer($end_pos))
                           {
                               $option_start = false;
                               $option_count = 0;
                           }
                           else
                           {

                                    $op_pos = strpos($sTxt, $op_tag_s, $pos_pre);
                                    if (is_integer($op_pos))
                                    {
                                        $op_end = strpos($sTxt,$op_tag_e, $op_pos);
                                        $option_found = true;
                                    }
                           }
                       }


                           //capture table tags (mind the orders)
                           if (is_integer($pos_td1 = strpos($sTxt, $table_td1, $pos_pre)))
                               if ($pos_td1<=$pos_e && $pos_td1>=$pos_pre)
                                   $xStr .= $table_td1;
                           if (is_integer($pos_tr1 = strpos($sTxt, $table_tr1, $pos_pre)))
                               if ($pos_tr1<=$pos_e && $pos_tr1>=$pos_pre)
                                   $xStr .= $table_tr1;
                           if (is_integer($pos_table1 = strpos($sTxt, $table_t1, $pos_pre)))
                               if ($pos_table1<=$pos_e && $pos_table1>=$pos_pre)
                                   $xStr .= $table_t1;

                           if (is_integer($pos_table0 = strpos($sTxt, $table_t0, $pos_pre)))
                               if ($pos_table0<=$pos_e && $pos_table0>=$pos_pre) {
                                   if (is_integer($pos_table1 = strpos($sTxt, ">", $pos_table0)))
                                       $xStr .= str_replace(' bgcolor="White"', '',substr($sTxt, $pos_table0, $pos_table1-$pos_table0+1));
                               }
                           if (is_integer($pos_tr0 = strpos($sTxt, $table_tr0, $pos_pre)))
                               if ($pos_tr0<=$pos_e && $pos_tr0>=$pos_pre) {
                                   if (is_integer($pos_tr1 = strpos($sTxt, ">", $pos_tr0)))
                                       $xStr .= str_replace(' bgcolor="White"', '',substr($sTxt, $pos_tr0, $pos_tr1-$pos_tr0+1));
                               }
                           if (is_integer($pos_td0 = strpos($sTxt, $table_td0, $pos_pre)))
                               if ($pos_td0<=$pos_e && $pos_td0>=$pos_pre) {
                                   if (is_integer($pos_td1 = strpos($sTxt, ">", $pos_td0)))
                                       $xStr .= str_replace(' bgcolor="White"', '',substr($sTxt, $pos_td0, $pos_td1-$pos_td0+1));
                               }

                           $tmpTxt = substr($sTxt, $pos_s+strlen($separator_s), $pos_e-$pos_s-strlen($separator_s));
                           if (is_integer($pos_tt = strpos($tmpTxt, "\n")))
                               if ($pos_tt==0)
                                   $tmpTxt = substr($tmpTxt, strlen("\n"));

                           if ($option_start && $option_found)
                           {
                               $tmpTxt = chr($option_letter_start+$option_count).". ".$tmpTxt;
//                               echo "Option found : $tmpTxt\n<br>\n";
                               $option_found = false;
                               $option_count++;
                           }
                           $xStr .= $tmpTxt;

                           $pos_pre = $pos_e;
                       }
                 }

                 return $xStr;
        }

        ######################################################################

        function lfs_remove($file){
                return (is_file($file)) ? $this->file_remove($file) : $this->folder_remove_recursive($file);
        }

        function lfs_copy($file, $dest){
                 if ($file=="") return 0;
                if(basename(dirname($file)) == basename($dest)) return 0;
                                if(is_file($file))
                                        return $this->file_copy($file, $dest);
                                else if(is_dir($file))
                                        return $this->folder_copy($file, $dest);
        }

        function lfs_move($file, $dest){
                if(basename(dirname($file)) == basename($dest)) return 0;
                $this->lfs_copy($file, $dest);
                $this->lfs_remove($file);
                return 1;
        }

        ######################################################################
        # utility functions

        function validateEmail($email) {
                //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
                return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $email);
        }

        function validateURL($url) {
                //return eregi("^((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$", $url);
                return preg_match("/^((http|ftp|https):\/\/)((([a-z0-9-]+(\.[a-z0-9-]+)*(.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((\/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$/i", $url);
        }

        function convertURLS($text) {
                //$text = eregi_replace("((ht|f)tp://www\.|www\.)([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((/|\?)[a-z0-9~#%&\\/'_\+=:\?\.-]*)*)", "http://www.\\3", $text);
                //$text = eregi_replace("((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)", "<a href=\\0 target=_blank>\\0</a>", $text);
        //        $text = preg_replace("/((http|ftp|https):\/\/www\.|www\.)([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((\/|\?)[a-z0-9~#%&\\/'_\+=:\?\.-]*)*)/i", "\\1://www.\\3", $text);
                $text = preg_replace("/((http|ftp|https):\/\/)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((\/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)/i", "<a href=\"\\0\" target=\"_blank\">\\0</a>", $text);
                return $text;
        }

        function convertMail($text) {
                //$text = eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))", "<a href='mailto:\\0'>\\0</a>", $text);
                $text = preg_replace("/([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))/i", "<a href=\"mailto:\\0\">\\0</a>", $text);
                return $text;
        }

        function convertAllLinks($text) {
                $text = $this->convertURLS($text);
                $text = $this->convertMail($text);
                return $text;
        }

        ######################################################################

        function getMIMEType($filename) {
	       $ext = $this->file_ext($filename);

	       switch ($ext) {
	       	case ".ai": return "application/postscript";
	       	case ".aif": return "audio/x-aiff";
	       	case ".aifc": return "audio/x-aiff";
	       	case ".aiff": return "audio/x-aiff";
	       	case ".asc": return "text/plain";
	       	case ".au": return "audio/basic";
	       	case ".avi": return "video/x-msvideo";
	       	case ".bcpio": return "application/x-bcpio";
	       	case ".bin": return "application/octet-stream";
	       	case ".c": return "text/plain";
	       	case ".cc": return "text/plain";	
	       	case ".ccad": return "application/clariscad";
	       	case ".cdf": return "application/x-netcdf";
      		case ".class": return "application/octet-stream";
      		case ".cpio": return "application/x-cpio";
      		case ".cpt": return "application/mac-compactpro";
					case ".csh": return "application/x-csh";
					case ".css": return "text/css";
					case ".dcr": return "application/x-director";
					case ".dir": return "application/x-director";
					case ".dms": return "application/octet-stream";
					case ".doc": return "application/msword";
					case ".drw": return "application/drafting";
					case ".dvi": return "application/x-dvi";
					case ".dwg": return "application/acad";
					case ".dxf": return "application/dxf";
					case ".dxr": return "application/x-director";
					case ".eps": return "application/postscript";
					case ".etx": return "text/x-setext";
					case ".exe": return "application/octet-stream";
					case ".ez": return "application/andrew-inset";
					case ".f": return "text/plain";
					case ".f90": return "text/plain";
					case ".fli": return "video/x-fli";
					case ".gif": return "image/gif";
       		case ".gtar": return "application/x-gtar";
					case ".gz": return "application/x-gzip";
					case ".h": return "text/plain";
					case ".hdf": return "application/x-hdf";
					case ".hh": return "text/plain";
		    	case ".hqx": return "application/mac-binhex40";
		    	case ".htm": return "text/html";
		    	case ".html": return "text/html";
		     	case ".ice": return "x-conference/x-cooltalk";
		     	case ".ief": return "image/ief";
		    	case ".iges": return "model/iges";
		     	case ".igs": return "model/iges";
		     	case ".ips":	return "application/x-ipscript";
		     	case ".ipx": return "application/x-ipix";
		     	case ".jpe": return "image/jpeg";
		    	case ".jpeg": return "image/jpeg";
		     	case ".jpg": return "image/jpeg";
		      case ".js": return "application/x-javascript";
			   	case ".kar": return "audio/midi";
		   		case ".latex": return "application/x-latex";
		     	case ".lha": return "application/octet-stream";
		     	case ".lsp": return "application/x-lisp";
		     	case ".lzh": return "application/octet-stream";
		      case ".m": return "text/plain";
		     	case ".man": return "application/x-troff-man";
		      case ".me": return "application/x-troff-me";
		    	case ".mesh": return "model/mesh";
		     	case ".mid": return "audio/midi";
		    	case ".midi": return "audio/midi";
		     	case ".mif": return "application/vnd.mif";
		    	case ".mime": return "www/mime";
		     	case ".mov": return "video/quicktime";
		   		case ".movie": return "video/x-sgi-movie";
		     	case ".mp2": return "audio/mpeg";
		     	case ".mp3": return "audio/mpeg";
		     	case ".mpe": return "video/mpeg";
		    	case ".mpeg": return "video/mpeg";
		     	case ".mpg": return "video/mpeg";
		    	case ".mpga": return "audio/mpeg";
		      case ".ms": return "application/x-troff-ms";
		     	case ".msh": return "model/mesh";
		      case ".nc": return "application/x-netcdf";
		     	case ".oda": return "application/oda";
		     	case ".pbm": return "image/x-portable-bitmap";
		     	case ".pdb": return "chemical/x-pdb";
		     	case ".pdf": return "application/pdf";
		     	case ".pgm": return "image/x-portable-graymap";
		     	case ".pgn": return "application/x-chess-pgn";
		     	case ".png": return "image/png";
		     	case ".pnm": return "image/x-portable-anymap";
		     	case ".pot": return "application/mspowerpoint";
		     	case ".ppm": return "image/x-portable-pixmap";
		     	case ".pps": return "application/mspowerpoint";
		     	case ".ppt": return "application/mspowerpoint";
		     	case ".ppz": return "application/mspowerpoint";
		     	case ".pre": return "application/x-freelance";
		     	case ".prt": return "application/pro_eng";
		      case ".ps": return "application/postscript";
		      case ".qt": return "video/quicktime";
		      case ".ra": return "audio/x-realaudio";
		     	case ".ram": return "audio/x-pn-realaudio";
		     	case ".ras": return "image/cmu-raster";
		     	case ".rgb": return "image/x-rgb";
		      case ".rm": return "audio/x-pn-realaudio";
		    	case ".roff": return "application/x-troff";
		     	case ".rpm": return "audio/x-pn-realaudio-plugin";
		     	case ".rtf": return "text/rtf";
		     	case ".rtx": return "text/richtext";
		     	case ".scm": return "application/x-lotusscreencam";
		     	case ".set": return "application/set";
		     	case ".sgm": return "text/sgml";
		    	case ".sgml": return "text/sgml";
		      case ".sh": return "application/x-sh";
		    	case ".shar": return "application/x-shar";
		    	case ".silo": return "model/mesh";
		     	case ".sit": return "application/x-stuffit";
		     	case ".skd": return "application/x-koan";
		     	case ".skm": return "application/x-koan";
		     	case ".skp": return "application/x-koan";
		     	case ".skt": return "application/x-koan";
		     	case ".smi": return "application/smil";
		    	case ".smil": return "application/smil";
		     	case ".snd": return "audio/basic";
		     	case ".sol": return "application/solids";
		     	case ".spl": return "application/x-futuresplash";
		     	case ".src": return "application/x-wais-source";
		    	case ".step": return "application/STEP";
		     	case ".stl": return "application/SLA";
		     	case ".stp": return "application/STEP";
		 			case ".sv4cpio": return "application/x-sv4cpio";
		  		case ".sv4crc": return "application/x-sv4crc";
		     	case ".swf": return "application/x-shockwave-flash";
		      case ".t": return "application/x-troff";
		     	case ".tar": return "application/x-tar";
		     	case ".tcl": return "application/x-tcl";
		     	case ".tex": return "application/x-tex";
		    	case ".texi": return "application/x-texinfo";
		  		case ".texinfo": return "application/x-texinfo";
		     	case ".tif": return "image/tiff";
		    	case ".tiff": return "image/tiff";
		      case ".tr": return "application/x-troff";
		     	case ".tsi": return "audio/TSP-audio";
		     	case ".tsp": return "application/dsptype";
		     	case ".tsv": return "text/tab-separated-values";
		     	case ".txt": return "text/plain";
		     	case ".unv": return "application/i-deas";
		   		case ".ustar": return "application/x-ustar";
		     	case ".vcd": return "application/x-cdlink";
		     	case ".vda": return "application/vda";
		     	case ".viv": return "video/vnd.vivo";
		    	case ".vivo": return "video/vnd.vivo";
		    	case ".vrml": return "model/vrml";
		     	case ".wav": return "audio/x-wav";
		     	case ".wrl": return "model/vrml";
		     	case ".xbm": return "image/x-xbitmap";
		     	case ".xlc": return "application/vnd.ms-excel";
		     	case ".xll": return "application/vnd.ms-excel";
		     	case ".xlm": return "application/vnd.ms-excel";
		     	case ".xls": return "application/vnd.ms-excel";
		     	case ".xlw": return "application/vnd.ms-excel";
		     	case ".xml": return "text/xml";
		     	case ".xpm": return "image/x-xpixmap";
		     	case ".xwd": return "image/x-xwindowdump";
		     	case ".xyz": return "chemical/x-pdb";
		     	case ".zip": 
		     		return "application/zip";
		     	default:
			     	return "application/octet-stream";
			    }
        }

        function download($file_source, $file_target)
	    {
	        $rh = fopen($file_source, 'rb');
	        $wh = fopen($file_target, 'wb');
	        if ($rh===false || $wh===false) {
			// error reading or opening file
	           return true;
	        }
	        while (!feof($rh)) {
	            if (fwrite($wh, fread($rh, 1024)) === FALSE) {
	                   // 'Download error: Cannot write to file ('.$file_target.')';
	                   return true;
	               }
	        }
	        fclose($rh);
	        fclose($wh);
	        // No error
	        return false;
	    }

    function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
    {
      # Check Title Row
      $format_wrong = false;

      for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
      {
        if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
        {
          $format_wrong = true;
          break;
        }
      }
      return $format_wrong;
    }
    
    function Get_File_Size($File) {
    	if (is_file($File)) 
    		return filesize($File);
    	else
    		return false;
    }
}
?>
