<?php
// page modifing by: Jason
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once("choose/new/User_Group_Transformer.php");
intranet_auth();
intranet_opendb();

##################################################################################################################

 
## User Library
$iCal = new icalendar();
$transformer = new User_Group_Transformer();

## Get Data
$ownerID = $_SESSION['UserID'];
$calName = trim($calName);
$calDescription = trim($calDescription);
$calColor = ltrim(trim($calColor), "#");
if ($calColor == "")
	$calColor = "1e2cd8";

# Calendar Type
# 1 - School Calendar   , 0 - General Calendar
$CalType = (isset($CalType) && $CalType != "") ? $CalType : 0;

# Share to All
$isShareToAll = (isset($isShareToAll) && $isShareToAll != "") ? $isShareToAll : "";

# Everyone can Subscribe status
$shareToAll = ($CalType == 1) ? 0 : $shareToAll;
$synURL			  	  = (isset($synURL) && $synURL != "") ? trim($synURL) : "";

switch($CalType){
	case 1: 
		$CalSharedType = (count($SchoolSharedType) > 1) ? 'A' : $SchoolSharedType[0]; 
		break;
	// case 2: 
		// $CalSharedType = (count($FoundationSharedType) > 1) ? 'A' : $FoundationSharedType[0]; 
		// break;
	default: 
		$CalSharedType = ''; 
		break;
}

## Main 
if (isset($calName) &&  $calName != "") {
	$fieldname   = "Owner, Name, Description, ShareToAll, DefaultEventAccess, CalType, CalSharedType, SyncURL";
	$fieldvalue  = "$ownerID, '$calName', ";
	$fieldvalue .= "'$calDescription', '$shareToAll', '$defaultEventAccess', '$CalType', '$CalSharedType', '$synURL' ";
	$sql = "INSERT INTO CALENDAR_CALENDAR ($fieldname) VALUES ($fieldvalue)";

	$result['insert_new_calendar'] = $iCal->db_db_query($sql);
	$calID = $iCal->db_insert_id();
	
	$calViewerID = array();
	$calViewerAccess = array();
	$calDisplayColor = array();
	$calVisible = array();
	
	$calViewerID[] = $ownerID;
	$calViewerAccess[] = "A";
	$calDisplayColor[] = $calColor;
	$calVisible[] = "1";
	
	$viewerID = empty($viewerID)?array():$viewerID;
	if ($CalType == 1 && !empty($CalSharedType)){ #school calendar
		$fieldName = 'CalID, UserID, Access, Color, Visible, GroupPath';
		
		if ($CalSharedType == "A"){
			#Teaching staff
			$sql = "
				insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
				Select '$calID', UserID, 'R', '$calColor', '1', 'T1' from 
				INTRANET_USER 
				where UserID <> '".$ownerID."' and RecordStatus = '1' 
				and RecordType = '1' and Teaching = '1'";
			$result['schoolCal_T'] = $iCal->db_db_query($sql);
			#Non-Teaching staff
			$sql = "
				insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
				Select '$calID', UserID, 'R', '$calColor', '1', '-2' 
				from INTRANET_USER 
				where UserID <> '".$userID."' and 
				RecordStatus = '1' and RecordType = '1'
				and (Teaching = '0' OR Teaching IS NULL)";
			$result['schoolCal_NT'] = $iCal->db_db_query($sql);		
			#Student
			$sql = "
				insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
				Select '$calID', UserID, 'R', '$calColor', '1', 'S1' from 
				INTRANET_USER 
				where UserID <> '".$ownerID."' and RecordStatus = '1' and RecordType = '2'";
			$result['schoolCal_S'] = $iCal->db_db_query($sql);
			
		}
		else{
			
			if ($CalSharedType == "S"){
				#Student
				$sql = "
					insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
					Select '$calID', UserID, 'R', '$calColor', '1', 'S1' 
					from INTRANET_USER where UserID <> '".$ownerID."' 
					and RecordStatus = '1' and RecordType = 'S'";
				$result['schoolCal_S'] = $iCal->db_db_query($sql);
			}
			else{
				#Teaching staff
				$sql = "
					insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
					Select '$calID', UserID, 'R', '$calColor', '1', 'T1' from 
					INTRANET_USER 
					where UserID <> '".$ownerID."' and RecordStatus = '1' 
					and RecordType = '1' and Teaching = '1'";
				$result['schoolCal_T'] = $iCal->db_db_query($sql);
				#Non-Teaching staff
				$sql = "
					insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
					Select '$calID', UserID, 'R', '$calColor', '1', '-2' 
					from INTRANET_USER 
					where UserID <> '".$userID."' and 
					RecordStatus = '1' and RecordType = '1'
					and (Teaching = '0' OR Teaching IS NULL)";
				$result['schoolCal_NT'] = $iCal->db_db_query($sql);
			}
		}
		
	}
	
	 # to separate group user and indivial user
	$groupUser = Array();
	$viewerID = array_unique($viewerID);
	for ($i = 0; $i < count($viewerID); $i++){
		$prefix = substr($viewerID[$i],0,1);
		if ($prefix == 'U'){ # indivial user
			$calViewerID[] = substr($viewerID[$i],1);
			$permissionVariable = "permission-".$viewerID[$i];
			$calViewerAccess[] = $$permissionVariable;
			$calDisplayColor[] = $calColor;
			$calVisible[] = "1";
		}
		else{ # group user
			$groupUser[] = $viewerID[$i];
		}
	}
	
	# add group user
	foreach ($groupUser as $group){
		$prefix = substr($group,0,1);
		$suffix = substr($group,1);
		$groupUserList = $transformer->get_userID_from_group($prefix,$suffix);
		$permissionVariable = "permission-".$group;
		$accessRight =  $$permissionVariable;
		if (count($groupUserList) > 0){		
			# insert group user
			if ($CalType == 1 && ($CalSharedType == "A"&&($group=="T1"||$group=="S1"||$group=="-2") || $CalSharedType == "T1" && ($group=="T1"||$group=="-2") || $CalSharedType == "S1" && $group=="S1")){
			
				$sql = "update CALENDAR_CALENDAR_VIEWER set 
						Access = $accessRight
						where CalID = $calID and GroupPath = '$group'	
				";
				$result['UpdateViewer_Group_'.$group] = $iCal->db_db_query($sql);
			}
			else{
				$fieldName = 'CalID, UserID, Access, Color, Visible, GroupPath';
				$sql = "insert into CALENDAR_CALENDAR_VIEWER ($fieldName) values ";
				$cnt = 0;
				foreach($groupUserList as $gpUser){
					if ($cnt ==0){
						$sql .= "('$calID','$gpUser','$accessRight', '$calColor', 1, '$group')";
						$cnt++;
					}
					else{
						$sql .= ", ('$calID','$gpUser','$accessRight', '$calColor', 1, '$group')";
					}			
				}
				$result['InsertViewer_Group_'.$group] = $iCal->db_db_query($sql);
			}
		}
	}
	
	
	
	# Insert Individual viewers
	if (count($calViewerID) > 0){		
		#insert individual user 
		$fieldName = 'CalID, UserID, Access, Color, Visible';
			$sql = "insert into CALENDAR_CALENDAR_VIEWER ($fieldName) values ";
		$cnt = 0;
		for($i = 0; $i < count($calViewerID); $i++){
			if ($cnt ==0){
				$sql .= "('$calID','".$calViewerID[$i]."','".$calViewerAccess[$i]."', '".$calDisplayColor[$i]."', 1)";
				$cnt++;
			}
			else{
				$sql .= ", ('$calID','".$calViewerID[$i]."','".$calViewerAccess[$i]."', '".$calDisplayColor[$i]."', 1)";
			}			
		}
		$result['InsertViewer_Individual'] = $iCal->db_db_query($sql);
	}
	
	
		// debug_r($result);
		// exit;
	# Get Data of Individual viewers
	/*if (sizeof($viewerID) > 0) {
		for($i=0; $i<sizeof($viewerID); $i++) {
			$calViewerID[] = $viewerID[$i];
			$permissionVariable = "permission-".$viewerID[$i];
			$calViewerAccess[] = $$permissionVariable;
			$calDisplayColor[] = $calColor;
			$calVisible[] = "1";
		}
	}*/
	# Use for School Calendar only for auto inserting Staff Record including Teaching & Non-teaching Staffs
	// debug_r($_REQUEST);
	// echo $CalType." ".$CalSharedType."<br>";
	// debug_r($viewerPath);
	// if ($CalType == 1){
		// if(sizeof($viewerPath) == 0 && ($CalSharedType == 'A' || $CalSharedType == 'T')){
			// $viewerPath = array('T', 'N');
			// ${"permission-T"} = 'R';
			// ${"permission-N"} = 'R';
		// }
	// }
	// debug_r($viewerPath);
	// exit;	
	
	
	
	/*
	// TG
	# Get Data of Group Viewers
	$num = 0;
	if (sizeof($viewerPath) > 0) {
		for($i=0; $i<sizeof($viewerPath); $i++) {
			# Case for Adding Subject Group 
			# Convert subject name into subject code for new GroupPath
			$path = explode(":=:", $viewerPath[$i]);
			$pathPrefix = (count($path) > 1 && strlen($path[1]) > 1) ? substr($path[1], 0, 1) : "";
			if(strtoupper($path[0]) == "T" && strtoupper($pathPrefix) == "C"){
				$pathSuffix = substr($path[1], 1);
				if(strtoupper($pathPrefix) == "C"){
					$subjectName = $pathSuffix;
					# Get SubjectCode by Subject Name
					$Sql = "exec Get_SubjectCode_BySubjectName @SubjectName='".$subjectName."'";
					$return = $iCal->returnArray($Sql);
					
					$subjectPath = array();
					if(count($return) > 0){
						for($k=0 ; $k<count($return) ; $k++){
							$newPath = '';
							for($j=0 ; $j<count($path) ; $j++){
								$newPath .= ($j > 0) ? ":=:" : "";
								$newPath .= ($j == 1) ? $pathPrefix.$return[$k]['Subject_Code'] : $path[$j];
							}
							# Get Basic Data
							$calGrpViewerPath[$num] = $newPath;
							$permissionVariable = "permission-".$viewerPath[$i];
							$calGrpViewerAccess[$num] = $$permissionVariable;
							$calGrpDisplayColor[$num] = $calColor;
							$calGrpVisible[$num] = "1";
							
							$calGrpViewerID[$num] = $iCal->Get_Group_Viewer($newPath);
							$num++;
						}
					}
				}
			} else {
				# Get Basic Data
				$calGrpViewerPath[$num] = $viewerPath[$i];
				$permissionVariable = "permission-".$viewerPath[$i];
				$calGrpViewerAccess[$num] = $$permissionVariable;
				$calGrpDisplayColor[$num] = $calColor;
				$calGrpVisible[$num] = "1";
				
				$calGrpViewerID[$num] = $iCal->Get_Group_Viewer($viewerPath[$i]);
				$num++;
			}
		}
	}
	*/
	
	# Main 
	# Individual viewers
	/*$recordCount = 0;
	$fieldname = "CalID, UserID, Access, Color, Visible";
	$sql = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";	// mysql
	//$sql = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) ";		// mssql
	for($j=0; $j<sizeof($calViewerID); $j++) {
		$calViewerIDPrefix = substr($calViewerID[$j], 0, 1);
		$calViewerIDSuffix = substr($calViewerID[$j], 1);
		if ($calViewerIDPrefix == "U") {
			$sql .= "('$calID', ".$calViewerIDSuffix.", '$calViewerAccess[$j]', '$calDisplayColor[$j]', '$calVisible[$j]'), ";
			$recordCount++;
		
		} else if ($calViewerIDPrefix == "P") {
			$parentList = $iCal->returnStudentParent($calViewerIDSuffix);
			for($k=0; $k<sizeof($parentList); $k++) {
				$sql .= "('$calID', ".$parentList[$k].", '$calViewerAccess[$j]', '$calDisplayColor[$j]', '$calVisible[$j]'), ";
			
			}
		} else if (is_numeric($calViewerIDPrefix)) {	// including owner
			$sql .= "('$calID', $calViewerID[$j], '$calViewerAccess[$j]', '$calDisplayColor[$j]', '$calVisible[$j]'), ";
			$recordCount++;
		
		}
	}
	$sql = rtrim($sql, ", ");
	if ($recordCount > 0){
		$result['insert_individual_viewer'] = $iCal->db_db_query($sql);
	}
	*/
	/*
	// TG
	# Group Viewers - New Approach
	for($j=0; $j<sizeof($calGrpViewerPath); $j++) {
		$path = explode(":=:", $calGrpViewerPath[$j]);
		$pathPrefix = substr($path[0], 0, 1);
		$pathSuffix = substr($path[1], 0, 1);
		
		$calGrpType = "NULL";
		if(strtoupper($pathPrefix) == "B"){
			$calGrpType = "'B'";
		}
		
		$fieldname = "CalID, UserID, GroupID, GroupType, Access, Color, Visible, GroupPath";
		if(count($calGrpViewerID[$j]) > 0){
			for($k=0; $k<sizeof($calGrpViewerID[$j]); $k++) {
				$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
				$sql .= "($calID, ".$calGrpViewerID[$j][$k].", NULL, $calGrpType, '".$calGrpViewerAccess[$j]."', '".$calGrpDisplayColor[$j]."', '".$calGrpVisible[$j]."', '".$calGrpViewerPath[$j]."')";
				$result['insert_group_viewer_'.$j.'_'.$k] = $iCal->db_db_query($sql);
			}
		}
	}
	*/
	
	
	# Group viewers
	/*$recordCount = 0;
	$fieldname = "CalID, UserID, GroupID, GroupType, Access, Color, Visible";
	//$sql = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
	$sql = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) ";
	for($j=0; $j<sizeof($calViewerID); $j++) {
		$calViewerIDPrefix = substr($calViewerID[$j], 0, 1);
		$calViewerIDSuffix = substr($calViewerID[$j], 1);
		if ($calViewerIDPrefix == "G") {
			$classStudents = $iCal->returnClassStudentList($calViewerIDSuffix);
			for($k=0; $k<sizeof($classStudents); $k++) {
				$sql .= "($calID, $classStudents[$k], ".$calViewerIDSuffix.", 'G', '$calViewerAccess[$j]', '$calDisplayColor[$j]', '$calVisible[$j]'), ";
			
				$recordCount++;
			}
		} else if ($calViewerIDPrefix == "C") {
			$courseStudents = $iCal->returnCourseStudentList($calViewerIDSuffix);
			for($k=0; $k<sizeof($courseStudents); $k++) {
				$sql .= "($calID, $courseStudents[$k], ".$calViewerIDSuffix.", 'C', '$calViewerAccess[$j]', '$calDisplayColor[$j]', '$calVisible[$j]'), ";
				
				$recordCount++;
			}
		} else if ($calViewerIDPrefix == "E") {
			$groupStudents = $iCal->returnGroupMemberList($calViewerIDSuffix);
			for($k=0; $k<sizeof($groupStudents); $k++) {
				$sql .= "($calID, $groupStudents[$k], ".$calViewerIDSuffix.", 'E', '$calViewerAccess[$j]', '$calDisplayColor[$j]', '$calVisible[$j]'), ";
			
				$recordCount++;
			}
		}
	}
	$sql = rtrim($sql, ", ");
	if ($recordCount > 0)
		$iCal->db_db_query($sql);*/
	
	
	#############################################################################
	# Add All User if CheckBox is checked
	/*
        if($isShareToAll == 1){
        	$sql = "SELECT UserID, UserLogin FROM INTRANET_USER WHERE UserType = 'T' AND Status = 'A'";
                $teacherArr = $iCal->returnArray($sql, 2);

                if(count($teacherArr) > 0){
					$AdminArr = array(
                                         "parkerr1", "woog1", "manay1",
                                         "chillingworthp", "leungwe1"
                                         );

                	$field_name = "CalID, UserID, Access, Color, Visible";
                        $sql_1 = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($field_name) ";

                        $cnt_1 = 0;
                        for($k=0 ; $k<count($teacherArr) ; $k++){
                                $teaID = $teacherArr[$k]['UserID'];
                                $teaLogin = $teacherArr[$k]['UserLogin'];

                                //if($teaLogin == "parkerr1" || $teaLogin == "woog1" || $teaLogin == "manay1"){
				if(in_array($teaLogin, $AdminArr)){
                                        $permit = 'W';
                                } else {
                                        $permit = 'R';
                                }
                                $sql_1 .= ($cnt_1 > 0) ? "UNION ALL " : "";
                                $sql_1 .= "SELECT '$calID', $teaID, '$permit', '$calColor', '1' ";
                                $cnt_1++;
                        }
                        if($cnt_1 > 0){
                        	$result['insert_all_users'] = $iCal->db_db_query($sql_1);
                    	}
        	}
        }
	*/
	#############################################################################	
	
} else {
	//header("Location: index.php?calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&time=".$_SESSION["iCalDisplayTime"]);
	header("Location: new_calendar.php?calID=$calID&error=1");
}


if(!in_array(false, $result)){
	$Msg = $Lang['ReturnMsg']['CalendarCreateSuccess'];
} else {
	$Msg = $Lang['ReturnMsg']['CalendarCreateUnSuccess'];
}


######################################################################################################################################
intranet_closedb();
//header("Location: index.php?calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&time=".$_SESSION["iCalDisplayTime"]);
//header("Location: new_calendar.php?msg=2&calID=".$calID);
header("Location: manage_calendar.php?Msg=$Msg");

?>
