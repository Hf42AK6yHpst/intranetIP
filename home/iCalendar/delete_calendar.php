<?php
// page modifing by: 
/*
 * 2013-09-06 (Carlos): added parameter $removeCompletely, if ==1 will completely remove the calender and all other sharers view
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
intranet_auth();
intranet_opendb();

$iCal = new icalendar();

# Initialization
$result_1 = array();
$calIdSql = array();
$repeatIdSql = array();
$isExistSharedGuest = 0;

$calID = IntegerSafe($calID);

# Check whether any shared individual guests or group exist
$calViewList = $iCal->returnCalViewerList($calID);
$calViewerGrpPath = $iCal->returnCalViewerList($calID, false, true);

if (sizeof($calViewList) > 1 || sizeof($calViewerGrpPath) > 0){
	$isExistSharedGuest = 1;
}

//$UserID = $_SESSION['UserID'];
# User access right for this event
$userCalAccess = $iCal->returnUserCalendarAccess($UserID,$calID);
// echo $isExistSharedGuest." ".$userCalAccess ;
// exit;

######### Remove the User View from this calendar 
# case i : when the user is not calendar owner
# case ii: when the user is calendar owener but the calendar contains some shared guests
if ($userCalAccess != "A" || ($userCalAccess == "A" && $isExistSharedGuest == 1 && $removeCompletely!=1) ) {
	# Not the owner, only delete it from view
	$sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER WHERE CalID = '$calID' AND UserID = '".$UserID."'";
	$result_1['delete_user_view'] = $iCal->db_db_query($sql);
	
	if(!in_array(false, $result_1)){
		$Msg = $Lang['ReturnMsg']['OptionalCalendarRemoveSuccess'];
	} else {
		$Msg = $Lang['ReturnMsg']['OptionalCalendarRemoveUnSuccess'];
	}
	
	header("Location: manage_calendar.php?Msg=$Msg");
	die;
}

######### Remove all related Calendar items By The Calendar Owner
# Check if this is the only calendar the user have
$sql = "SELECT COUNT(*) AS NoOfCal FROM CALENDAR_CALENDAR WHERE Owner = '".$UserID."'";
$result = $iCal->returnArray($sql);

if ($result[0]["NoOfCal"] == 1)
	$oneCalLeft = true;

# retrieve all events belongs to the calendar
$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE CalID = '".$calID."'";
$result = $iCal->returnArray($sql);

if (sizeof($result) > 0) {
	for ($i=0; $i<sizeof($result); $i++){
		$calIdSql[] = $result[$i]["EventID"];
		if($result[$i]["RepeatID"] != "" && $result[$i]["RepeatID"] != NULL){
			$repeatIdArr[] = $result[$i]["RepeatID"];
		}
	}
	$calIdSql = "(".implode(",", $calIdSql).")";
	$repeatIdSql = "(".(count($repeatIdArr)>0?implode(",", $repeatIdArr):"").")";
} else {
	$calIdSql = "";
}

# delete main detail
$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE CalID = '$calID'";
$result_1['delete_event_detail'] = $iCal->db_db_query($sql);

# delete repeat pattern
if(count($repeatIdArr) > 0){
	$sql = "DELETE FROM CALENDAR_EVENT_ENTRY_REPEAT WHERE RepeatID IN $repeatIdSql";
	$result_1['delete_event_repeat_info'] = $iCal->db_db_query($sql);
}
/*
$sql = "DELETE FROM CALENDAR_EVENT_ENTRY_REPEAT WHERE EventID IN $calIdSql";
$result_1['delete_event_repeat_info'] = $iCal->db_db_query($sql);
*/

# Delete reminders & invite guests if having any events in this calendar
if (sizeof($result) > 0) {
	# delete reminders
	$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID IN $calIdSql";
	$result_1['delete_event_reminder'] = $iCal->db_db_query($sql);

	# delete guests
	$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID IN $calIdSql";
	$result_1['delete_event_guest'] = $iCal->db_db_query($sql);
}

# if it is the only calendar for the user, keep it and its sharing setting
if (!$oneCalLeft) {
	#delete calendar
	$sql = "DELETE FROM CALENDAR_CALENDAR WHERE CalID = '$calID'";
	$result_1['delete_calendar'] = $iCal->db_db_query($sql);
	
	#delete calendar viewer
	$sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER WHERE CalID = '$calID'";
	$result_1['delete_calendar_viewer'] = $iCal->db_db_query($sql);
}

// debug_r($result_1);
// exit;

if(!in_array(false, $result_1)){
	$Msg = $Lang['ReturnMsg']['PersonalCalendarDeleteSuccess'];
} else {
	$Msg = $Lang['ReturnMsg']['PersonalCalendarDeleteUnSuccess'];
}

intranet_closedb();
header("Location: manage_calendar.php?Msg=$Msg");
?>
