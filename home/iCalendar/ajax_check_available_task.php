<?php
// Editing by 
/*
 * 2016-09-22 (Carlos): Created for checking time availability.
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once("choose/new/User_Group_Transformer.php");
intranet_auth();
intranet_opendb();

//$iCal = new icalendar();
$iCal = new icalendar_api(); 
//$db = new libdb();
$transformer = new User_Group_Transformer();
$linterface = new interface_html();
$lui = new ESF_ui();

switch($task)
{
	case 'getCheckAvailableTimeForm':
	
		//$eventDate;
		//$isAllDay;
		//$startTime;
		//$endTime;
		//$viewer
		
		if($isAllDay){
			$startTime = '00:00';
			$endTime = '23:59';
		}
		
		$userList = array();
		for ($i=0; $i<sizeof($viewer); $i++) {
			$viewerID = '';
			$viewerPrefix = substr($viewer[$i], 0, 1);
			$viewerID = substr($viewer[$i], 1);
			if ($viewerPrefix == 'U')
				$userList[] = $viewerID ;
			else{
				$tempGuest = $transformer->get_userID_from_group($viewerPrefix,$viewerID);
				$userList = array_merge($userList,$tempGuest);
			}
		}
		$userList = array_values(array_unique($userList));
		
		$calendar_types = array();
		//$calendar_types = array('5,6'=>$eEnrollmentMenu['activity'],'7'=>$i_Discipline_System_Access_Right_Detention,'8'=>$Lang['SysMgr']['Timetable']['Timetable']);
		if($plugin['enrollment']){
			$calendar_types['5,6'] = $eEnrollmentMenu['activity'];
		}
		if($plugin['Disciplinev12']){
			$calendar_types['7'] = $i_Discipline_System_Access_Right_Detention;
		}
		$calendar_types['8'] = $Lang['SysMgr']['Timetable']['Timetable'];
		
		$date_selection = $lui->Get_Span_Open().$lui->Get_Input_Text("EventDate", $eventDate, " class=\"textbox_date\" style=\"width:120px\" ").$lui->Get_Span_Close();
		//$date_selection = $lui->Get_DatePicker("EventDate", "yy-mm-dd", $eventDate, " class=\"textbox_date\" style=\"width:120px\" ");
		//$date_selection = $linterface->GET_DATE_PICKER("EventDate",$eventDate,"","yy-mm-dd",$__MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$__ID="EventDate",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
		$start_time_selection = $linterface->Get_Time_Selection("StartTime", $startTime, $__others_tab='',$__hideSecondSeletion=1, $__intervalArr=array(1,1,1), $__parObjId=false);
		$end_time_selection = $linterface->Get_Time_Selection("EndTime", $endTime, $__others_tab='',$__hideSecondSeletion=1, $__intervalArr=array(1,1,1), $__parObjId=false);
		
		$x .= '<link rel="stylesheet" type="text/css" href="/templates/'.$LAYOUT_SKIN.'/css/content_30.css" />';
		$x .= $linterface->Include_HighChart_JS();
		
		$x .= '<form id="checktimeForm" name="checktimeForm" action="" method="post" onsubmit="return false;" >';
		$x .= '<input type="hidden" name="viewerCount" id="viewerCount" value="'.count($userList).'" />';
		$x .= '<br /><table width="100%" cellpadding="2" class="form_table_v30">';
			$x .= '<col class="field_title">'."\n";
			$x .= '<col class="field_c">'."\n";
			$x .= '<tr>';
				$x .= '<td class="field_title">'.$iCalendar_NewEvent_DateTime.'</td>';
				$x .= '<td>'.$date_selection.' '.$Lang['General']['From'].' '.$start_time_selection.' '.$Lang['General']['To'].' '.$end_time_selection.'</td>';
			$x .= '</tr>';
			$x .= '<tr>';
				$x .= '<td class="field_title">'.$Lang['iCalendar']['IncludingEventType'].'</td>';
				$x .= '<td>';
				foreach($calendar_types as $cal_type => $cal_name){
					$x .= '<label for="CalType['.$cal_type.']"><input type="checkbox" id="CalType['.$cal_type.']" name="CalType[]" value="'.$cal_type.'" checked="checked" /> '.$cal_name.'</label>&nbsp;';
				}
				$x .= '</td>';
			$x .= '</tr>';
			$x .= '<tr>';
				$x .= '<td class="field_title">'.$Lang['iCalendar']['CheckFor'].'</td>';
				$x .= '<td>
							<label for="Availability"><input type="radio" name="CheckType" id="Availability" value="A" checked="checked" />'.$Lang['iCalendar']['AvailableOrNot'].'</label>&nbsp;
							<label for="EventTimeline"><input type="radio" name="CheckType" id="EventTimeline" value="T" />'.$Lang['iCalendar']['EventTimeline'].'</label>
						</td>';
			$x .= '</tr>';
			$x .= '<tr><td colspan="2" align="center" style="text-align:center;">'.$lui->Get_Input_Button("btnCheck", "btnCheck", $iCalendar_meeting_Check, ' onclick="getCheckAvailableTimeResult();" ') .'</td></tr>';
			
		$x .= '</table><br />';
		$x .= '</form>';
		$x .= '<div id="ContainerDiv"></div>';

		echo $x;
		
	break;
	
	case 'getCheckAvailableTimeResult':
		
		//$EventDate;
		//$StartTime;
		//$EndTime;
		//$viewer
		//$CheckType
		//$CalType
		
		function __sortEventCmp($a, $b)
		{
			if(count($a['intervals']) == count($b['intervals']))
			{
				$result = strcmp($a['name'], $b['name']);
			}else{
				$result = count($a['intervals']) >= count($b['intervals'])? -1 : 1;
			}
			return $result;
		}
		
		$check_availability = $CheckType == 'A';
		
		$StartTime = $_POST['StartTime'];
		$EndTime = $_POST['EndTime'];
		//$StartTime .= ':00';
		//$EndTime .= ':00';
		
		// timestamp in second, javascript timestamp is in millisecond
		$start_ts = strtotime($EventDate.' '.$StartTime);
		$end_ts = strtotime($EventDate.' '.$EndTime);
		$min_ts = $start_ts;
		$max_ts = $end_ts; 
		
		$userList = array();
		for ($i=0; $i<sizeof($viewer); $i++) {
			$viewerID = '';
			$viewerPrefix = substr($viewer[$i], 0, 1);
			$viewerID = substr($viewer[$i], 1);
			if ($viewerPrefix == 'U')
				$userList[] = $viewerID ;
			else{
				$tempGuest = $transformer->get_userID_from_group($viewerPrefix,$viewerID);
				$userList = array_merge($userList,$tempGuest);
			}
		}
		$name_field = getNameFieldByLang2("u.");
		$events = $iCal->GetCalenderEventsForCheckingAvailability($userList, $start_ts, $end_ts, array(0,1,2));		
		
		if(in_array('5,6',(array)$CalType)){
			$enrol_events = $iCal->GetEnrolmentEventsForCheckingAvailability($userList, $start_ts, $end_ts);
			if(count($enrol_events)>0){
				$events = array_values(array_merge($events, $enrol_events));
			}
		}
		
		if(in_array(7,(array)$CalType)){
			$detention_events = $iCal->GetDetentionEventsForCheckingAvailability($userList, $start_ts, $end_ts);	
			if(count($detention_events)>0){
				$events = array_values(array_merge($events, $detention_events));
			}
		}
		
		if(in_array(8,(array)$CalType)){
			$lesson_events = $iCal->GetTimetableLessonsForCheckingAvailability($userList, $start_ts, $end_ts);
			if(count($lesson_events)>0){
				$events = array_values(array_merge($events, $lesson_events));
			}
		}
		
		sortByColumn2($events,'StartTimestamp',0,0,'');
		//debug_pr($events);
		$sql = "SELECT u.UserID, $name_field as UserName FROM INTRANET_USER as u WHERE u.UserID IN ('".implode("','",$userList)."')";
		$userAry = $iCal->returnResultSet($sql);
		
		$event_count = count($events);
		$eventAryForChart = array();
		$userIdToEvents = array();
		for($i=0;$i<$event_count;$i++){
			if(!isset($userIdToEvents[$events[$i]['UserID']])){
				$userIdToEvents[$events[$i]['UserID']] = array('id'=>$events[$i]['UserID'],'name'=>$events[$i]['UserName'],'intervals'=>array());
			}
			$userIdToEvents[$events[$i]['UserID']]['intervals'][] = array('from'=>$events[$i]['StartTimestamp']*1000,'to'=>$events[$i]['EndTimestamp']*1000,'label'=>$events[$i]['Title']);
			if($events[$i]['StartTimestamp'] < $min_ts){
				$min_ts = $events[$i]['StartTimestamp'];
			}
			if($events[$i]['EndTimestamp'] > $max_ts){
				$max_ts = $events[$i]['EndTimestamp'];
			}
		}
		
		for($i=0;$i<count($userAry);$i++){
			if(!isset($userIdToEvents[$userAry[$i]['UserID']])){
				$userIdToEvents[$userAry[$i]['UserID']] = array('id'=>$userAry[$i]['UserID'],'name'=>$userAry[$i]['UserName'],'intervals'=>array());
			}
		}
		
		$eventAry = array();
		foreach($userIdToEvents as $ary){
			$eventAry[] = $ary;
		}
		
		usort($eventAry,'__sortEventCmp');
		
		if($check_availability){
			$n = 0;
			$available_count = 0;
			$unavailable_count = 0;
			$y = '';
			for($i=0;$i<count($eventAry);$i++){
				$n++;
				$available = count($eventAry[$i]['intervals'])==0;
				if($available){
					$available_count++;
				}else{
					$unavailable_count++;
				}
				$y .= '<tr>';
					$y .= '<td>'.$n.'</td>';
					$y .= '<td>'.$eventAry[$i]['name'].'</td>';
					$y .= '<td'.($available?' style="background-color:green"':'').'>&nbsp;</td>';
					$y .= '<td'.(!$available?' style="background-color:red"':'').'>&nbsp;</td>';
				$y .= '</tr>';
			}
			$x = '<table class="common_table_list_v30">';
				$x .= '<tr>';
					$x .= '<th class="num_check">#</th>';
					$x .= '<th>'.$Lang['General']['Name'].'</th>';
					$x .= '<th>'.$Lang['iCalendar']['Available'].': '.$available_count.'</th>';
					$x .= '<th>'.$Lang['iCalendar']['Unavailable'].': '.$unavailable_count.'</th>';
				$x .= '</tr>';
				$x .= $y;
			$x.= '</table><br />';
			
			echo $x;
			
		}else
		{
			/*
			if(count($userIdToEvents)>0){
				
				if($check_freetime){ // fill in free time periods
					$min_ts_ms = $min_ts * 1000;
					$max_ts_ms = $max_ts * 1000;
					foreach($userIdToEvents as $uid => $ary){
						if(count($ary['intervals'])>0){
							$interval_ary = array();
							$interval_count = count($ary['intervals']);
							for($i=0;$i<$interval_count;$i++){
								if($i==0){
									if($ary['intervals'][$i]['from'] > $min_ts_ms){
										$interval_ary[] = array('from'=>$min_ts_ms,'to'=>$ary['intervals'][$i]['from'],'label'=>'');
									}
								}else if($i == $interval_count-1){
									if($ary['intervals'][$i]['to'] < $max_ts_ms){
										$interval_ary[] = array('from'=>$ary['intervals'][$i]['to'],'to'=>$max_ts_ms,'label'=>'');
									}
								}else{
									if($ary['intervals'][$i]['to'] < $ary['intervals'][$i+1]['from']){
										$interval_ary[] = array('from'=>$ary['intervals'][$i]['to'],'to'=>$ary['intervals'][$i+1]['from'],'label'=>'');
									}
								}
							}
							$userIdToEvents[$uid]['intervals'] = $interval_ary;
						}else{ // no any event, fill up a whole day time period
							$userIdToEvents[$uid]['intervals'][] = array('from'=>$min_ts_ms,'to'=>$max_ts_ms,'label'=>'');
						}
					}
				}
				
				foreach($userIdToEvents as $ary){
					$eventAryForChart[] = $ary;
				}
			}
			*/
			$jsonObj = new JSON_obj();
			$json_str = $jsonObj->encode($eventAry);
			echo $json_str;
		}
		
	break;
	
}

intranet_closedb();
?>