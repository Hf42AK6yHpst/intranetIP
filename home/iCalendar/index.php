<?php
// page modifing by:
/*
 * 2014-12-19 (Carlos): fix agenda view end date boundary timestamp bug which has extra 1 second that cause the 8th day all day events fetched
 * 2013-03-27 (Carlos): modified js eventSearchEngin() searchValue only store one search keyword for searching
 */
// $PathRelative = "../../../";
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");
$CurSubFunction = "iCalendar";

$ImagePathAbs = "/theme/image_en/";


// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");

// if (empty($_SESSION['UserID'])) {
	// header ("Location: ".$PATH_WRT_ROOT."login.php?CurPage=".$CurPage);
	// exit;
// }


 // debug_r($_SESSION);

// AuthPage(array("Communication-Calendar-GeneralUsage"));
intranet_auth();
intranet_opendb();
#include_once($PathRelative."src/include/template/general_header.php");
// include_once($PathRelative."src/include/class/icalendar.php");

$iCal = new icalendar();
$iCal_api = new icalendar_api();
// $lui = new interface_html();
$esfUI = new ESF_ui();

$icalendar_name = $plugin["iCalendarFull"]?$iCalendar_iCalendar :$iCalendar_iCalendarLite;
$calViewPeriodArr = array("daily","weekly","monthly","agenda");
$calViewPeriod = str_replace(array('<', '>', ';', 'javascript:', '"', '\'', '(', ')','&'),'',$calViewPeriod);
if (!isset($calViewPeriod) || !in_array($calViewPeriod, $calViewPeriodArr))
	$calViewPeriod = $iCal->systemSettings["PreferredView"];

if ($calViewPeriod == "")
	$calViewPeriod = "agenda";

$CalViewMode = $iCal->returnUserPref($_SESSION['UserID'],'CalViewMode');
if (trim($calViewSelect) == '') 
	$calViewSelect = ($CalViewMode == 1) ? 'full' : 'simple';
	
$time = IntegerSafe($time);
if (isset($time) && is_numeric($time)) {
	$year = date('Y', $time);
	$month = date('m', $time);
	$day = date('d', $time);
} else if (!isset($year) && !isset($month)){
	$time = time();
	$year = date('Y');
	$month = date('m');
	$day = date('d');
}

# Insert a default calendar if user didn't have any calendar
if (!isset($_SESSION["iCalHaveCal"]) ||  $_SESSION["iCalHaveCal"]!= 1) {
	$iCal->insertMyCalendar();
}

# check for the existance of external calendar, if not create it.
$iCal_api->externalCalendarChecking();
// debug_r($iCal_api->getRelatedAllEnrolActivity());
// $iCal_api->getAllSchoolEvent();
# Record the display view in session during each visit to this page
$_SESSION["iCalDisplayPeriod"] = $calViewPeriod;
$_SESSION["iCalDisplayTime"] = $time;

 
 # disable calendar view
 if (isset($action) && $action == 'CalDisable'){
 	// save original visible calendars before disable them
 	$sql = "SELECT CalID FROM CALENDAR_CALENDAR_VIEWER WHERE UserID = '".$_SESSION['UserID']."' AND Visible = '1' ";
 	$tmp_visible_cals = $iCal->returnVector($sql);
 	$sql = "SELECT Setting FROM CALENDAR_USER_PREF WHERE UserID = '".$_SESSION['UserID']."' AND Value = '1' ";
 	$tmp_visible_pref = $iCal->returnVector($sql);
 	
 	$tmp_visible_cals_csv = '';
 	if(count($tmp_visible_cals)>0){
 		$tmp_visible_cals_csv = implode(",",$tmp_visible_cals);
 	}
 	if(count($tmp_visible_pref) > 0){
 		$tmp_visible_pref_csv = "'".implode("','",$tmp_visible_pref)."'";
 		$tmp_visible_cals_csv .= ",".$tmp_visible_pref_csv;
 	}
 	if($tmp_visible_cals_csv != ''){
 		$safe_tmp_visible_cals_csv = $iCal->Get_Safe_Sql_Query($tmp_visible_cals_csv);
 		$sql = "INSERT INTO INTRANET_USER_PERSONAL_SETTINGS (UserID,iCalendarVisibleCals,iCalendarVisibleCals_DateModified) 
				VALUES ('".$_SESSION['UserID']."','".$safe_tmp_visible_cals_csv."',NOW()) 
				ON DUPLICATE KEY 
				UPDATE iCalendarVisibleCals = '".$safe_tmp_visible_cals_csv."', iCalendarVisibleCals_DateModified = NOW() ";
 		$iCal->db_db_query($sql);
 	}
 	// end of saving original visible Calendars
 	
	$sql = "update CALENDAR_USER_PREF set Value = 0 where UserID = ".$_SESSION['UserID'];
	$iCal->db_db_query($sql);
	if ($Caltype == 2){
		$sql = "update CALENDAR_CALENDAR_VIEWER set Visible = 0 where
				 UserID = ".$_SESSION['UserID'];
		$iCal->db_db_query($sql);
		$sql = "update CALENDAR_CALENDAR_VIEWER set Visible = 1 where
				 UserID = ".$_SESSION['UserID']." and
				 CalID in (
					select CalID from INTRANET_GROUP where GroupID = $GroupID
				 )";
		$iCal->db_db_query($sql);
	}
	else if ($Caltype == 3){
		$sql = "update CALENDAR_CALENDAR_VIEWER set Visible = 0 where
				 UserID = ".$_SESSION['UserID'];
		$iCal->db_db_query($sql);
		$sql = "update CALENDAR_CALENDAR_VIEWER set Visible = 1 where
				 UserID = ".$_SESSION['UserID']." and
				 CalID in (
					select CalID from {$eclass_db}.course where course_id = $CourseID
				 )";
		$iCal->db_db_query($sql);
	}
	
 }
 else{
 	// restore original visible Calendars 
 	$sql = "SELECT iCalendarVisibleCals FROM INTRANET_USER_PERSONAL_SETTINGS WHERE UserID = '".$_SESSION['UserID']."' AND iCalendarVisibleCals IS NOT NULL AND iCalendarVisibleCals <> '' ";
	$tmp_visible_cals = $iCal->returnVector($sql);
	if(trim($tmp_visible_cals[0]) != ''){
		//$safe_tmp_visible_cals = $iCal->Get_Safe_Sql_Query($tmp_visible_cals[0]);
		$sql = "UPDATE CALENDAR_CALENDAR_VIEWER SET Visible = '1' WHERE UserID = '".$_SESSION['UserID']."' AND CalID IN (".$tmp_visible_cals[0].")";
		$iCal->db_db_query($sql);
		$sql = "UPDATE CALENDAR_USER_PREF SET Value = '1' WHERE UserID = '".$_SESSION['UserID']."' AND Setting IN (".$tmp_visible_cals[0].")";
		$iCal->db_db_query($sql);
		$sql = "UPDATE INTRANET_USER_PERSONAL_SETTINGS SET iCalendarVisibleCals = NULL WHERE UserID = '".$_SESSION['UserID']."' ";
		$iCal->db_db_query($sql);
	}
 }

$msgCallBack = "false";
$msgType = str_replace(array('<', '>', ';', 'javascript:', '"', '\'', '(', ')','&'),'',$msgType);
if (isset($Msg) && $msgType=='event'){
	$pieces = explode('|=|',$Msg);	
	$msgCallBack = ($pieces[0]==1?"true":"false");
}

# Pre-load the events for quicker access
if ($calViewPeriod=="monthly") {
	$lower_bound = mktime(0,0,0,$month,1,$year);
	$upper_bound = mktime(0,0,0,$month+1,1,$year);
	//$events = $iCal->query_events($lower_bound, $upper_bound);					# old approach of getting events

	# Differ from a Offset 1970101 in DB
	$lower_bound += $iCal->offsetTimeStamp;
	$upper_bound += $iCal->offsetTimeStamp;

} else if ($calViewPeriod=="weekly") {
	$current_week = $iCal->get_week($year, $month, $day, "Y-m-d H:i:s");
	$lower_bound = $iCal->sqlDatetimeToTimestamp($current_week[0]);
	$upper_bound = $iCal->sqlDatetimeToTimestamp($current_week[6]);
	$upper_bound = $upper_bound + SECONDINADAY;
	
	$events = $iCal->query_events($lower_bound, $upper_bound);	
} else if ($calViewPeriod=="daily") {
	$lower_bound = mktime(0,0,0,$month,$day,$year);
	$upper_bound = $lower_bound+SECONDINADAY;
	
	# Differ from a Offset 1970101
	$lower_bound += $iCal->offsetTimeStamp;
	$upper_bound += $iCal->offsetTimeStamp;
	
	//$events = $iCal->query_events($lower_bound, $upper_bound);
} else if ($calViewPeriod=="agenda"){
	$response = '';
	$agendaPeriod = 1;
	
	$startDate = date("Y-m-d",$time);
	$endDate = date("Y-m-d", $time + (7*24*60*59));
	
	$lower_bound = $iCal->sqlDatetimeToTimestamp($startDate." 00:00:00");
	$upper_bound = $iCal->sqlDatetimeToTimestamp($endDate." 23:59:59");

	# Differ from a Offset 1970101
	$lower_bound += $iCal->offsetTimeStamp;
	$upper_bound += $iCal->offsetTimeStamp;

	# Old approach
	//$events = $iCal->query_events($lower_bound, $upper_bound); 
}

## Get Event 
$events = $iCal->Get_All_Related_Event($lower_bound, $upper_bound, true);
// debug_pr($events); 
## Preparation
$selectTime = "<select class=\"tabletext\" name=\"eventHr\">";
//for ($i=0; $i<24; $i++) {
for ($i=$iCal->systemSettings["WorkingHoursStart"]; $i<$iCal->systemSettings["WorkingHoursEnd"]+1; $i++) {
	$selectTime .= ((int)date("H")==$i) ? "<option value=\"$i\" selected>$i</option>" : "<option value=\"$i\">$i</option>\n";
}
$selectTime .= "</select>";
// debug_pr($selectTime);

$interval = 5;
## start time in minutes
$start_min .= "<select class=\"tabletext\" name=\"eventMin\">";
for ($i=0; $i<(60/$interval); $i++) {
	$min_value = $i * $interval;
	$min_value = sprintf("%02d",$min_value);
	$start_min .= "<option value=\"{$min_value}\">{$min_value}</option>";
}
$start_min .= "</select>";

## duration in minutes
$duration_min = "<select class=\"tabletext\" name=\"durationMin\">";
for ($i=0; $i<(60/$interval); $i++) {
	$min_value = $i * $interval;
	if ($intranet_session_language=="en" && $min_value>0) {
		$min_text = $iCalendar_NewEvent_DurationMins;
	} else {
		$min_text = $iCalendar_NewEvent_DurationMin;
	}
	$duration_min .= "<option value=\"{$min_value}\">{$min_value} {$min_text}</option>";
}
$duration_min .= "</select>";


if ($calViewPeriod == "monthly"){
	$topNavigationPeriod = $iCal->get_navigation("monthly", $year, $month);
	$topViewSelector = $iCal->getCalViewSelector($calViewSelect,true);
} else if ($calViewPeriod == "weekly"){
	$topNavigationPeriod = $iCal->get_navigation("weekly", $year, $month, $day);
	$topViewSelector = $iCal->getCalViewSelector($calViewSelect,false);
} else if ($calViewPeriod == "daily"){
	$topNavigationPeriod = $iCal->get_navigation("daily", $year, $month, $day);
	$topViewSelector = $iCal->getCalViewSelector($calViewSelect,false);
} else if ($calViewPeriod == "agenda"){
	$topNavigationPeriod = $iCal->get_navigation("agenda", $year, $month, $day);
	$topViewSelector = $iCal->getCalViewSelector($calViewSelect,false);
}

if ($calViewPeriod=="daily") {
	$dailyTabActive = " id='current'";
} else if ($calViewPeriod=="weekly") {
	$weeklyTabActive = " id='current'";
} else if ($calViewPeriod=="monthly") {
	$monthlyTabActive = " id='current'";
} else if ($calViewPeriod == "agenda") {
	$agendaTabActive = " id='current'";
}

$searchTab = "<li id='eventSearchTab' style='display:none'><a href='javascript:void(0)' onclick='searchEngin.tabActivate()'><span>".$iCalendar_searchTab."</span></a></li>";
$agendaTab = "<li".$agendaTabActive."><a name='tabAgenda' href=\"javascript:document.form1.time.value=$time;changePeriodTab('agenda')\"><span>".$iCalendar_ToolLink_Agenda."</span></a></li>";
$dailyTab = "<li".$dailyTabActive."><a name='tabDaily' href=\"javascript:document.form1.time.value=$time;changePeriodTab('daily')\"><span>".ucfirst($iCalendar_NewEvent_DurationDay)."</span></a></li>";
$weeklyTab = "<li".$weeklyTabActive."><a name='tabWeekly' href=\"javascript:document.form1.time.value=$time;changePeriodTab('weekly')\"><span>".ucfirst($iCalendar_NewEvent_DurationWeek)."</span></a></li>";
$monthlyTab = "<li".$monthlyTabActive."><a name='tabMonthly' href=\"javascript:document.form1.time.value=$time;changePeriodTab('monthly')\"><span>".ucfirst($iCalendar_NewEvent_DurationMonth)."</span></a></li>";
 
# Main UI - Preparation

$CalLabeling .= "
	<div id='caltabs'>
	  <ul>	  
		$agendaTab
		$dailyTab
		$weeklyTab
		$monthlyTab
		$searchTab
	  </ul>
	</div>
	<div id='cal_day_range' style='display:block;'>
	  $topNavigationPeriod
	</div>";

	$CalLabeling .= "
		<div id='cal_view_selector' style='display:block; float:left;'>
			$topViewSelector
		</div>";

	$CalLabeling .= "
		<div style='float:right''>
		  <a href='javascript:changeViewTime();changePeriodTab(\"\")' class='cal_select_today cal_select_today_selected'>$iCalendar_Calendar_Today</a>
		</div>";


if ($calViewPeriod == "monthly") {
	$calendarDisplay = $iCal->generateMonthlyCalendar($year, $month, false, $calViewSelect);
} else if ($calViewPeriod == "weekly") {
	//$calendarDisplay = $iCal->generateWeeklyCalendar($year, $month, $day);
	$calendarDisplay = $iCal->Generate_Weekly_Calendar($year, $month, $day);
} else if ($calViewPeriod == "daily") {
	//$calendarDisplay = $iCal->generateDailyCalendar($year, $month, $day);
	$calendarDisplay = $iCal->Generate_Daily_Calendar($year, $month, $day);
} else if ($calViewPeriod == "agenda"){
	// echo 'debug : '.date("Y-m-d",$time).'<br />';
//     debug_pr("Year || Month || Day" . $year . " " . $month . " " . $day);
//     $calendarDisplay = $iCal->Generate_Daily_Calendar($year, $month, $day);
//     $calendarDisplay = $iCal->Generate_Weekly_Calendar($year, $month, $day);
	$calendarDisplay = $iCal->Print_Agenda($startDate, $endDate, false, $response, $agendaPeriod);
}

// Main Calendar
$CalMain = '
	<div id="cal_board">
	  <span class="cal_board_02"></span> 
	  <div class="cal_board_content">
		<div id="calendar_wrapper">'.
	    $calendarDisplay.
		'</div>
	  </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>';
	// debug_r($_SESSION);

# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain, '', '', '', '', '', true);

##############################################################################################################################
# Generate HTML for the buttons use in event deleting dialog
$button_name = $iCalendar_NewEvent_Repeats_SaveThisOnly;
$deleteEventButtons1  = $esfUI->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.delete_form.deleteRepeatEvent.value='OTI';document.delete_form.submit();\"");
$button_name = $iCalendar_NewEvent_Repeats_SaveAll;
$deleteEventButtons1 .= $esfUI->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.delete_form.deleteRepeatEvent.value='ALL';document.delete_form.submit();\"");
$button_name = $iCalendar_NewEvent_Repeats_SaveFollow;
$deleteEventButtons1 .= $esfUI->Get_Input_Button($button_name, $button_name, $button_name, "class=\"button\" onclick=\"javascript:document.delete_form.deleteRepeatEvent.value='FOL';document.delete_form.submit();\"");
$deleteEventButtons1 .= $esfUI->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button\" onclick=\"document.delete_form.action='index.php';$('#deleteEventWarning1').jqmHide();\"");
#$deleteEventButtons1 = addslashes($deleteEventButtons1);

$deleteEventButtons2  = $esfUI->Get_Input_Button("Remove", "Remove", $Lang['btn_remove'], "class=\"button\" onclick=\"javascript:document.delete_form.submit();\"");
$deleteEventButtons2 .= $esfUI->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button\" onclick=\"document.delete_form.action='index.php';$('#deleteEventWarning2').jqmHide();\"");
#$deleteEventButtons2 = addslashes($deleteEventButtons2);
##############################################################################################################################

# Choose CSS style sheet
$styleSheet = "css/display_calendar.css";
?>
<link rel="stylesheet" href="css/main.css" type="text/css" />
<link rel="stylesheet" href="<?=$styleSheet?>" type="text/css" />
<?php
# Start outputing the layout after CSS is loaded to prevent "FOUC"
// include_once($PathRelative."src/include/template/general_header.php");

echo '<script>';
echo 'var SessionExpiredWarning = "' . $Lang['Warning']['SessionExpired'] . '";';
echo 'var SessionKickedWarning = "' . $Lang['Warning']['SessionKicked'] . '";';
echo 'var SessionExpiredMsg = "' . $SYS_CONFIG['SystemMsg']['SessionExpired'] . '";';
echo 'var SessionKickedMsg = "' . $SYS_CONFIG['SystemMsg']['SessionKicked'] . '";';
echo '</script>';

include_once("headerInclude.php");
?>
<!-- ClueTip require files end -->
<script language="javascript">
<?
$otherCalTypeSetting = $iCal->Get_Other_CalType_Settings();
?> 
var cal_map_name = new Array(<?=count($otherCalTypeSetting)?>);
<?
if(count($otherCalTypeSetting) > 0){ 
	$other_tooltip_div_name = '';
?>
	var num = 0;
<?	for($i=0 ;$i<count($otherCalTypeSetting) ; $i++){ 
		$other_tooltip_div_name .= ', div[@id^="'.$otherCalTypeSetting[$i]['CalTypeName'].'Event"] ';
?>
		cal_map_name[num] = new Array(2);
		cal_map_name[num][0] = '<?=$otherCalTypeSetting[$i]['CalType']?>';
		cal_map_name[num][1] = '<?=$otherCalTypeSetting[$i]['CalTypeName']?>';
		num++;
<?  }
}
?>
function jGet_Other_CalType_Name(cal_type){
	var map_name = '';
	if(cal_map_name.length > 0){
		for(var i=0 ; i<cal_map_name.length ; i++){
			if(cal_map_name[i][0] == cal_type){
				map_name = cal_map_name[i][1];
				break;
			}
		}
	}
	return map_name;
}
	
function jShow_More_Event_List(year, month, date){
	var obj = document.getElementById("date_"+date);
	var evt_date = year + "-" + month + "-" + date;
	
	var objLink = document.getElementById("create_"+evt_date);
 	//obj.style.top = getPostion(objLink, 'offsetTop');
 	obj.style.top = objLink.style.top;
 	
	$('#date_'+date).show('slow', 
		function(){
			$('#date_'+date).width("180px");
			if($('#date_detail_'+date).height() > 200){
				$('#date_detail_'+date).height("200px");
				$('#date_detail_'+date).css("overflow", "auto");
			}
		}
	);
}

function jHide_More_Event_List(date){
	var obj = document.getElementById("date_"+date);
	$('#date_'+date).hide("slow");

}

// Fix for IE6 image flicker (cannot solve the case of ":hover")
try {
  document.execCommand("BackgroundImageCache", false, true);
} catch(err) {}

$.validator.addMethod("yyyymmdd", function(value) {
	return /(19[0-9][0-9]|20[0-9][0-9])-([1-9]|0[1-9]|1[012])-([1-9]|0[1-9]|[12][0-9]|3[01])/.test(value);
}, '&nbsp;*<?=$iCalendar_QuickAdd_InvalidDate?>');

var isBlocking = false;

function validateForm() {
	var v = $("#quickAddEvent_form").validate({
		rules: {
			title: "required",
			eventDate: {
				required: true,
				yyyymmdd: true,
				minLength: 8,
				maxLength: 10
			}
		},
		messages: {
			title: "&nbsp;*<?=$iCalendar_QuickAdd_Required?>",
			eventDate: {
				required: "&nbsp;*<?=$iCalendar_QuickAdd_Required?>",
				minLength: "&nbsp;*<?=$iCalendar_QuickAdd_InvalidDate?>",
				maxLength: "&nbsp;*<?=$iCalendar_QuickAdd_InvalidDate?>"
			}
		}
	});
	
	if(v.form()){
		$("#quickAddEvent").fadeOut();
		popUpCal.hideCalendar(popUpCal._curInst, '');
		var blockMsg = "<h2 style='padding-top:10px;'><?=$i_campusmail_processing?></h2>";
		if(jQuery_1_3_2){
			(function($){
				$('div#content').block({message:blockMsg, overlayCSS: { color:'#ccc' ,border:'3px solid #ccc' }});
			})(jQuery_1_3_2);
		}else{
			$('div#content').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		}
		isBlocking = true;
		$("#quickAddEvent_form").ajaxSubmit({
			url: "new_event_update.php",
			success: function() {
				$("#quickAddEvent_form").resetForm();
				getCalendarDisplay($('input[@name=calViewPeriod]').val());
			}
		});
		//document.quickAddEvent_form.submit();
	} else {
		v.focusInvalid();
		return false;
	}
}

// wrap the code required to inititate the calendar
function initCalendar() {
	// create tooltips for DIV with class name: event, involoveEvent, schoolEvent & foundationEvent
	$('div[@id^="event"], div[@id^="involveEvent"], div[@id^="schoolEvent"], div[@id^="foundationEvent"] <?=$other_tooltip_div_name?> ').cluetip({local:true,cursor:'pointer', positionBy:'mouse', activation:'toggle', cluetipClass:'jtip', arrows:true, dropShadow:false, showTitle:false});
	
	// clickable table cell as a quick link to add new event
	// * disable(unbind) it when mouseover an event DIV, enable(bind) it when mouseout
	var period = $("input[@name='calViewPeriod']").val();
	createClickCell(period);
	
}

function createClickCell(period) {
	// clickable table cell as a quick link to add new event
	// disable(unbind) it when mouseover an event DIV, enable(bind) it when mouseout
	if (period == "monthly") {
		
		$(".monthCell, .monthTodayCell").bind("mouseover", function() {
			if(document.getElementById("new_"+$(this).attr("id")) != undefined) {
				document.getElementById("new_"+$(this).attr("id")).style.display = "block";
			}
		});
		
		$(".monthCell, .monthTodayCell").bind("mouseout", function() {
			if(document.getElementById("new_"+$(this).attr("id")) != undefined) {
				document.getElementById("new_"+$(this).attr("id")).style.display = "none";
			}
		});
		
		
		$(".monthCell, .monthTodayCell").click(function() {
			jHide_Event_ToolTips();
			//window.location = "new_event.php?calDate="+$(this).attr("id")+"&calViewPeriod=monthly";
		});
		/*
		$(".monthCell>div, .monthTodayCell>div").bind("mouseover", function() {
			$(this).parent().unbind("click");
		});
		
		$(".monthCell>div, .monthTodayCell>div").bind("mouseout", function() {
			//$(this).parent().click(function(){window.location = "new_event.php?calDate="+$(this).attr("id")+"&calViewPeriod=monthly";});
		});
		*/
	} else if (period == "weekly") {
		$(".weekCell, .weekCellToday, .weekFirstCell, .weekFirstCellToday, .weekLastCell, .weekLastCellToday").click(function()
		{
			window.location = "new_event.php?calDate="+$(this).attr("id")+"&calViewPeriod=weekly";
		});
		$(".weekCell>div, .weekCellToday>div, .weekFirstCell>div, .weekFirstCellToday>div, .weekLastCell>div, .weekLastCellToday>div").bind("mouseover", function()
		{
			$(this).parent().unbind("click");
			
		});
		$(".weekCell>div, .weekCellToday>div, .weekFirstCell>div, .weekFirstCellToday>div, .weekLastCell>div, .weekLastCellToday>div").bind("mouseout", function()
		{
			$(this).parent().click(function(){window.location = "new_event.php?calDate="+$(this).attr("id")+"&calViewPeriod=weekly";});
			
		});
	} else if (period == "daily") {
		$(".dayCell, .dayCellToday, .dayFirstCell, .dayFirstCellToday, .dayLastCell, .dayLastCellToday").click(function()
		{
			window.location = "new_event.php?calDate="+$(this).attr("id")+"&calViewPeriod=daily";
		});
		$(".dayCell>div, .dayCellToday>div, .dayFirstCell>div, .dayFirstCellToday>div, .dayLastCell>div, .dayLastCellToday>div").bind("mouseover", function()
		{
			$(this).parent().unbind("click");
			
		});
		$(".dayCell>div, .dayCellToday>div, .dayFirstCell>div, .dayFirstCellToday>div, .dayLastCell>div, .dayLastCellToday>div").bind("mouseout", function()
		{
			$(this).parent().click(function(){window.location = "new_event.php?calDate="+$(this).attr("id")+"&calViewPeriod=daily";});
			
		});
	}
}

function jDelete_Event(eventID, repeatID, oldEventDate, title){
	obj = document.delete_form;
	
	$("input[@name='eventTitle']").val(title);
	$("input[@name='eventID']").val(eventID);
	$("input[@name='oldEventDate']").val(oldEventDate);
	
	var msg = "<?=$iCalendar_DeleteEvent_ConfirmMsg2_js?>";
	eventTitle = msg.replace("#%EventTitle%#", '"'+title+'"');
	$("#deleteEventConfirmMsg2").html(eventTitle);
	
	if(repeatID != ""){
		$("input[@name='isRepeatEvent']").val(1);
		$("input[@name='repeatID']").val(repeatID);
	} else {
		$("input[@name='isRepeatEvent']").val(0);
	}
	document.delete_form.action = "delete_event.php";
	
	if ($("input[@name='isRepeatEvent']").val() == 1) {
		$('#deleteEventWarning1').jqmShow();
	} else {
		$('#deleteEventWarning2').jqmShow();
	}
}

TabHref = new Array();

$(document).ready( function() {
	// window.opener.TestingObj = 'hihi'
	//return;
	// define the dialog DIVs 
	
	$('#deleteEventWarning1').jqm({trigger: false});
	$('#deleteEventWarning2').jqm({trigger: false});
	$('.cal_month_event_item').css("margin","0px");
	
	document.getElementById('searchSystem').style.display = 'block';
	
	searchEngin = new eventSearchEngin('',document.getElementById('eventSearchTab'),$('input[@name=searchType]:checked').val());
	if (<?=$msgCallBack?>){
		if (window.opener != null){
			if (navigator.appName == 'Microsoft Internet Explorer'){
				if (window.opener.loadCalendarBoardContent != null)
				window.opener.loadCalendarBoardContent();
			}else
				window.opener.postMessage('updateCal','*');	
		}
	}

	tab = $("#caltabs > ul > li > a"); 
	tab.each(function(){
		TabHref[TabHref.length] = this.href;
	});
	
	/*
	// get the position (bottom-left) of quickAddEvent_button link to display the quick add dialog
	// **depends on the jQuery Dimension plugins
	var buttonBottom = $('#quickAddEvent_button').position().top + $('#quickAddEvent_button').height() + 5;
	var buttonLeft = $('#quickAddEvent_button').position().left;
	
	$("#quickAddEvent").hide();
	
	$('#quickAddEvent').Draggable( 
		{
			zIndex: 10,
			ghosting: false,
			opacity: 0.7,
			handle: '#quickAddEvent_handle'
		}
	);
	
	$('#quickAddEvent_form').ajaxForm();
	
	$('#quickAddEvent_button').click(function() 
	{
		$("#quickAddEvent").css("top",buttonBottom);
		$("#quickAddEvent").css("left",buttonLeft);
		// show(), slideUp(), slideDown(), slideToggle(), fadeIn(), fadeOut() can also be used
		$("#quickAddEvent").fadeIn();
		$("input[@name='title']").focus();
		return false;
	});
	
	$('#quickAddEvent_close').click(function() 
	{
		$("#quickAddEvent").fadeOut();
		popUpCal.hideCalendar(popUpCal._curInst, '');
		return false;
	});
	
	$('#quickAddSubmit').click(function() {
		validateForm();
	});
	*/
	// jQuery calendar widget
<?php
	if ($intranet_session_language == "b5") {
?>
	
<? } ?>
	
	
	// bind the show calendar trigger to #chooseDateLink
	$('#chooseDateLink').click(function() 
	{
		popUpCal.showFor($('#selectDisplayDate')[0]);
		return false;
	});
	
	initCalendar();
});

function changeAgendaPeriod() {
	changePeriodTab("agenda");
}

function changeDate(date) {
	datePiece = date.split("-");
	year = datePiece[0];
	month = datePiece[1] - 1;
	day = datePiece[2];
	newDate = new Date(year,month,day);
	newTime = (newDate.getTime())/1000;
	$("input[@name='time']").val(newTime);
	getCalendarDisplay($('input[@name=calViewPeriod]').val());
}

function toggleInvolveEventVisibility(){
	// Reset_Timeout();
	//$(".involve").toggle();
	toggleCalVisibility("involve","toggleVisible");
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: 'involve'
	}, function(responseText) {
		sessionExpire(responseText);
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	isDisplayMoreFunction();
	jHide_All_Layers();
}

function toggleOtherInvitaionVisibility(){
	// Reset_Timeout();
	//$(".involve").toggle();
	//toggleCalVisibility(parClassID)
	// Ajax request to update visible calendar
	//alert("hihi");
	toggleCalVisibility("cal-other","toggleOtherVisible");
	
	$.post('cal_visible.php', {
		calID: 'otherInvite'
	}, function(responseText) {
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	isDisplayMoreFunction();
	jHide_All_Layers();
}

function toggleExternalCalVisibility(selfName,eventClass,CalType){
	toggleCalVisibility(eventClass,selfName);
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calType : CalType		
	}, function(responseText) {
		sessionExpire(responseText);
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	isDisplayMoreFunction();
	jHide_All_Layers();
}

function toggleSchoolEventVisibility(selfName,eventClass,recordType){
	// Reset_Timeout();
	// $('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
	toggleCalVisibility(eventClass,selfName);
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: recordType,
		calType : 1		
	}, function(responseText) {
		sessionExpire(responseText);
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	isDisplayMoreFunction();
	jHide_All_Layers();
}

function Set_CheckBox(objName, checked){
	var obj = document.getElementsByName(objName);
	if(obj.length > 0){
		for(var i=0 ; i<obj.length ; i++){
			obj[i].checked = (checked == 1) ? true : false;
		}
	}
}

function toggleFoundationEventVisibility(obj, calID, calType, calColor){
	// Reset_Timeout();
	//$('.foundation-'+calID).toggle();
	//var checked = (obj.checked == true) ? 1 : 0;
	//Set_CheckBox('toggleFoundationCalVisible[]', checked);
	
	toggleCalVisibility('foundation-'+calID,obj.id);
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: calID, cal_type: calType, 
		color: calColor 
	}, function(responseText) {
	    // callback function
		if (!SessionExpired(responseText)){ }
	});
	
	isDisplayMoreFunction();
	jHide_All_Layers();
}

function toggleOtherCalTypeEventVisibility(cal_type){
	// Reset_Timeout();
	if(cal_map_name.length > 0){
		for(var i=0 ; i<cal_map_name.length ; i++){
			if(cal_map_name[i][0] == cal_type){
				var map_name = cal_map_name[i][1];
				break;
			}
		}
	} else {
		return;
	}
	
	var div_name = map_name;
	$('.' + div_name).toggle();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: map_name, cal_type: cal_type
	}, function(responseText) {
	    // callback function
		if (!SessionExpired(responseText)){ }
	});
	
	isDisplayMoreFunction();
	jHide_All_Layers();
}

function toggleCalVisibility(parClassID,calName){
	// Reset_Timeout();
	
	objValue = "";
	eventAssociate="";
	
	if (parClassID.match("^[0-9]+[0-9]*$")){
		//$('.cal-'+parClassID).toggle();
		var name = 'CalVisibility_'+parClassID;
		var obj = $('input[@name='+name+']');
		objValue = obj.val();
		eventAssociate = $('.cal-'+parClassID).get();
	}
	else{
		//$('.'+parClassID).toggle();
		//alert(parClassID);
		//var name = 'CalVisibility_'+parClassID;
		//var obj = $('input[@name='+name+']');
		objValue= !(document.getElementById(calName).checked);
		//alert(objValue);
		eventAssociate = $('.'+parClassID).get();
	}
	
	
	if(objValue == 1 || objValue == true){
		//if (objValue == 1)
			//
		if (parClassID.match("^[0-9]+[0-9]*$")){
			obj.val(0);
			$('.cal-'+parClassID).css("display", "none");
		}
		else
			$('.'+parClassID).css("display", "none");
			
		//alert(parClassID);
		//remove
		/*for(i=0;i<eventAssociate.length; i++){
			if ((eventAssociate[i].id).match("^dummy")){				
				pieces = (eventAssociate[i].id).split("-"); 
				dateID = "day-"+pieces[2];
				dateBlock = $('div#'+dateID);
				fakeBlock = dateBlock.find('div#'+eventAssociate[i].id).prev();
				
				while(fakeBlock.length > 0){
				
					if (!fakeBlock.attr('id').match("^fake"))
						break;
					idPieces = fakeBlock.attr('id').split('-'); 
					preFakeID = idPieces[0]+'-'+idPieces[1]+'-'+(pieces[2]-1);
					prevFake = $('div#'+preFakeID);
					if (prevFake.length > 0 && prevFake.css('display') == 'none' || !fakeBlockchecking(fakeBlock))
						fakeBlock.css("display","none");
					else
						fakeBlock.css("display","block");
					
					
					fakeBlock = fakeBlock.prev(); 
				}
			}
			
		}*/
		
		
	} else {
		//if (objValue == 0)
		//obj.val(1);
		if (parClassID.match("^[0-9]+[0-9]*$")){
			obj.val(1);
			$('.cal-'+parClassID).css("display", "block");
		}
		else
			$('.'+parClassID).css("display", "block");
			
		
		/*for(i=0;i<eventAssociate.length; i++){
			pieces = (eventAssociate[i].id).split("-");
			dateID = "day-"+pieces[2];
		
			if (eventAssociate[i].id.match("^dummy")){
				dateBlock = $('div#'+dateID);
				fakeBlock = dateBlock.find('div#'+eventAssociate[i].id).prev();
				
				while(fakeBlock.length > 0){
					fakeBlockDOM = fakeBlock.get();
						
					if (!fakeBlockDOM[0].id.match("^fake"))
						break;
					temp = dateBlock.find('div#'+fakeBlockDOM[0].id);
					listEvent = $('div.'+temp.attr('class')+':not([id^=dummy]):not([id^=fake])').get();
					
					temp.css("display",checkVisibility(listEvent[0])?"block":"none");
					
					fakeBlock = fakeBlock.prev();
				}
			
			}
			
			else if(eventAssociate[i].id.match("^fake")){ 				
				dateBlock = $('div#'+dateID);				
				fakeBlock = dateBlock.find('div#'+eventAssociate[i].id);
				
				idPieces = fakeBlock.attr('id').split('-'); 
				preFakeID = idPieces[0]+'-'+idPieces[1]+'-'+(pieces[2]-1);
				prevFake = $('div#'+preFakeID);
				if (prevFake.length > 0 && prevFake.css('display') == 'none' || !fakeBlockchecking(fakeBlock))
					fakeBlock.css("display","none");
				else
					fakeBlock.css("display","block");
			}
			
		}*/
		
		
	}
	
	if (eventAssociate.length>0){
		dayArray = null;
		relatedDays = document.getElementById('asso'+eventAssociate[0].className);
		if (relatedDays != null){			
			dayArray = relatedDays.value.split(',');
		}
		else{
			strvalue = '';
			for(i=0;i<eventAssociate.length; i++){
				dayBlock = eventAssociate[i].parentNode;
				if (!dayBlock.id.match('^day'))
					dayBlock = dayBlock.parentNode
				pieces = (dayBlock.id).split("-"); 
				searchValue = '@'+pieces[1]+',';
				if (!strvalue.match(searchValue)){
					strvalue += searchValue;
				}
			}
			//alert(strvalue)
			strvalue = strvalue.substr(1,strvalue.length-2);
			dayArray = strvalue.split(',@');
		}
		restoreHiddenEvent(dayArray);
	}
	
	// Ajax request to update visible calendar
	if (parClassID.match("[0-9]+")){
		$.post('cal_visible.php', {
			calID: parClassID
		}, function(responseText) {
			sessionExpire(responseText);
			// callback function
			//if (!SessionExpired(responseText)){ }
		});
		
		isDisplayMoreFunction();
		jHide_All_Layers();
	}
}

function fakeBlockchecking(fakeBlock){
	isDisplay = false;
	nextBlock = fakeBlock.next();
	while(nextBlock.get().length > 0){						
		nextBlockDom = nextBlock.get();
		if (!nextBlockDom[0].id.match("^fake") && !nextBlockDom[0].id.match("^dummy")&&checkVisibility(nextBlockDom[0])){						
			break;
		}						
		if (nextBlockDom[0].id.match("^dummy")){
			if (!checkVisibility(nextBlockDom[0])){
				nextBlock = nextBlock.next();
				continue;
			}			
			isDisplay = true;
			break;
		}
		nextBlock = nextBlock.next();
	}
	return isDisplay;
}

function checkVisibility(domObj){
	attrName = '';
	if (domObj.className == 'cal_month_event_item')	{	
		if (domObj.childNodes[0].nodeType == 1)
			attrName = 'V'+domObj.childNodes[0].className; 
		else{
			attrName = 'V'+domObj.childNodes[1].className;
		}
		/*if (navigator.appName=='Microsoft Internet Explorer')
			attrName = 'V'+domObj.childNodes[0].className; 
		else{		
			child = domObj.childNodes[1];
			if (child == null)
				child = domObj.childNodes[0];
			attrName = 'V'+child.className;
		}*/
	}else
		attrName = 'V'+domObj.className;
	attrValue = document.getElementById(attrName).value;
	return document.getElementById(attrValue).checked;
}

function restoreHiddenEvent(dayArray){
	if (document.getElementById('maxDays') == null && document.getElementById('startDay') == null || dayArray==null)
		return;
	end = parseInt(document.getElementById('maxDays').value);
	start = parseInt(document.getElementById('startDay').value);
	EndDay = end;

	if (end < start){
		EndDay = parseInt(document.getElementById('monthEnd').value);
		end = EndDay + end;
		
	}
	end = parseInt(document.getElementById('maxDays').value);
	start = parseInt(document.getElementById('startDay').value);
	for (i=0;i<dayArray.length;i++){
		curDay = dayArray[i];
		dayBlockID = 'day-'+curDay;
		allItem = $('div#'+dayBlockID).children('div');
		cnt = 0;	
		allItem.each(function(){		 
			if (this.id.match('^fake')){
				if (checkVisibility(this)){
					fakeBlock = $(this); 
					idPieces = this.id.split('-'); 
					preDay = ((idPieces[2]-1)<=0?EndDay:(idPieces[2]-1));
					preFakeID = idPieces[0]+'-'+idPieces[1]+'-'+preDay;
					prevFake = $('div#'+preFakeID);
					if (prevFake.length > 0 && prevFake.css('display') == 'none' || !fakeBlockchecking(fakeBlock)){
						fakeBlock.css("display","none");
					}
					else{
						fakeBlock.css("display","block");
						cnt++;
					}					
				}
				return;
			}			
			concernObj = this;
			calMonthobj = false;
			if (concernObj.className == 'cal_month_event_item'){
				/*if (navigator.appName=='Microsoft Internet Explorer')
					concernObj = concernObj.childNodes[0]; 
				else{
					child = concernObj.childNodes[1];
					if (child == null)
						child = concernObj.childNodes[0];
					concernObj = child; 
				}*/				
				if (concernObj.childNodes[0].nodeType == 1)
					concernObj = concernObj.childNodes[0]; 
				else{
					concernObj = concernObj.childNodes[1]; 
				}
				
				calMonthobj = true;
			}				
			if (cnt >= 5 && start==1 && end >= 28){
				concernObj.style.display = 'none';
				if (calMonthobj){
					$(concernObj).siblings().css('display','none');
				}
				return;
			}
			if (concernObj.style.display == 'none' && (cnt < 5 || !(start==1 && end >= 28))){
				isVisible = checkVisibility(concernObj);
				if (isVisible){
					concernObj.style.display = 'block';
					if (calMonthobj){
						$(concernObj).siblings().css('display','block');
					}
					cnt++;
				}
				return;
			}
			cnt++;
		});
	}
}

function toggleOwnDefaultCalVisibility(parClassID){
	toggleCalVisibility(parClassID);

	var obj = document.getElementById("toggleCalVisible-"+parClassID);
	var involObj = document.getElementById("toggleVisible");
	// var otherObj = document.getElementById("toggleOtherVisible");
	if(obj.checked == true){
		// if(otherObj.checked == false){
			// otherObj.checked = true;
			// toggleOtherInvitaionVisibility();
		// }
		
		if(involObj.checked == false){
			involObj.checked = true;
			toggleInvolveEventVisibility();
		}
		involObj.checked = true;
		// otherObj.checked = true;
	} else {
		// if(otherObj.checked == true){
			// otherObj.checked = false;
			// toggleOtherInvitaionVisibility();
		// }				
		
		if(involObj.checked == true){
			involObj.checked = false;
			toggleInvolveEventVisibility();
		}
		// otherObj.checked = false;
		involObj.checked = false;
	}	
	
	isDisplayMoreFunction();
	jHide_All_Layers();
}

function enableTimeSelect() {
	if ($('input[@name="isAllDay"]').attr('checked')) {
		$('#eventTimeDiv').hide();
	} else {
		$('#eventTimeDiv').show();
	}
}

function submitQuickAddEvent() {
	$("form")[0].submit();
}

function openPrintPage() {
	var ts = $("input[@name='time']").val();
	var period = (searchEngin.activate?'search':$("input[@name='calViewPeriod']").val());
	if (searchEngin.activate){
		params = "search=0&period="+searchEngin.period+"&startTime="+searchEngin.time+"&searchType="+searchEngin.searchType+"&agendaPeriod="+searchEngin.agendaPeriod+"&pageNo="+searchEngin.pageNo+"&maxPageNo="+searchEngin.maxPageNo;
		for (i=0;i<searchEngin.searchValue.length;i++){
			params += '&searchValue[]='+encodeURIComponent(searchEngin.searchValue[i]);
		}
		window.open("eventSearch.php?"+params,'','scrollbars=yes,width=870,height=600');
	}
	else{
		var params = "time="+ts+"&calViewPeriod="+period;
		if(period == "agenda"){
			var agendaPeriod = document.getElementById("selectAgendaPeriod").value;
			params += "&agendaPeriod="+agendaPeriod;		
		}
		window.open("print.php?"+params,'','scrollbars=yes,width=870,height=600');
	}
}

function toggleTab(period) {
	tab = $("#caltabs > ul > li").attr("id", "");
	/* i = 0;
	$("#caltabs > ul > li > a").each(function(){
		this.href = TabHref[i];
		i++;
	}); */
	var t;
	if (period == "monthly") {
		tab = $("#caltabs").children().find(":nth-child(4)").attr("id", "current");
		t = $("#caltabs").children().find(":nth-child(4) > a").get();
	} else if (period == "weekly") {
		tab = $("#caltabs").children().find(":nth-child(3)").attr("id", "current");
		t = $("#caltabs").children().find(":nth-child(3) > a").get();
	} else if (period == "daily") {
		tab = $("#caltabs").children().find(":nth-child(2)").attr("id", "current");
		t = $("#caltabs").children().find(":nth-child(2) > a").get();
	} else {
		tab = $("#caltabs").children().find(":nth-child(1)").attr("id", "current");
		t = $("#caltabs").children().find(":nth-child(1) > a").get();
	}
	// t[0].href = 'javascript:void(0);'
}

function changePeriodTab(period) {
	jHide_All_Layers();
	searchEngin.deactivate();
	if (period == "") {
		period = $("input[@name='calViewPeriod']").val();
	}
	getCalendarDisplay(period);
	if (period == "monthly") {
		toggleTab("monthly");
		$("input[@name='calViewPeriod']").val("monthly");
	} else if (period == "weekly") {
		toggleTab("weekly");
		$("input[@name='calViewPeriod']").val("weekly");
	} else if (period == "daily") {
		toggleTab("daily");
		$("input[@name='calViewPeriod']").val("daily");
	} else {
		toggleTab("agenda");
		$("input[@name='calViewPeriod']").val("agenda");
	}
}

function isDisplayMoreFunction(){
	// var calObj = document.getElementsByName("CalVisibleID[]");
	//var moreObj = document.getElementsByName("more[]");
	var calObj = $('[@name="CalVisibleID[]"]').get();
	var moreObj = $('[@name="more[]"]').get();
	//var moreObj = $('.moreFunction').get();
	var isShow = 0;
	
	// Check all calendar is invisible or not
	if(calObj.length > 0){
		for(var i=0 ; i<calObj.length ; i++){
			var calID = calObj[i].value;
			if($('input[@name=CalVisibility_'+calID+']').val() == 1)
				isShow = 1;
		}
	}
	
	if(moreObj.length > 0){
		for(var i=0 ; i<moreObj.length ; i++){
			if(isShow == 0){
				moreObj[i].style.display = "none";
			} else {
				moreObj[i].style.display = "block";
			}
			
			// Check for the Current Event is more than 5 or not
			var moreId = moreObj[i].id;
			var tmp = moreId.split('_');
			
			if(tmp.length > 0){
				var cnt = 0;
				objName = "date"+tmp[1]+"event[]";
				//var eventObj = document.getElementsByName("date"+tmp[1]+"event[]");
				eventObj = $('[@name="'+objName+'"]').get();
				for(var j=0; j<eventObj.length ; j++){
					if(eventObj[j].style.display == "block" || (checkVisibility(eventObj[j]) && !eventObj[j].id.match('^fake')))
						cnt++;
				}
		
				if(parseInt(cnt) < parseInt(6)){
					moreObj[i].style.display = "none";
				} else {
					moreObj[i].style.display = "block";
				}
			}
		}
	}
}

<?php
// create an JS array of month name
echo $iCal->createJsArray("monthName", $iCalendar_NewEvent_Repeats_MonthArray);

// create an JS array of weekday name
echo $iCal->createJsArray("weekdayName", $iCalendar_NewEvent_Repeats_WeekdayArray2);
?>

function changeViewTime() {
	var currentTime = new Date();
	var currentTimeInSec = Math.floor((currentTime.getTime())/1000);
	$("input[@name='time']").val(currentTimeInSec);
	//initCalendar();
	jQuery.datepicker._gotoToday(0);
}

var shownPeriod = "monthly";
var searchEngin;

function sessionExpire(response){
	if (response == 'SESSION_EXPIRED')
		window.location.assign('/login.php'); 
}

function eventSearchEngin(searchValue,searchTab, searchType){
	this.searchValue = searchValue; 
	this.searchTab = searchTab;
	this.searchResult = '';
	this.activate = false;
	this.searchType= searchType;
	this.period = '';
	this.time = '';
	this.agendaPeriod = '';
	this.pageNo=1;
	this.maxPage='';
	this.maxPageNo=50;
	
	this.search = function(searchValue){		
		$displayTitle = "<?=$i_Calendar_Loading?>";		
		document.getElementById('cal_day_range').style.display = 'none';
		if (!isBlocking) {
			var blockMsg = "<h2 style='padding-top:10px;'>"+$displayTitle+"</h2>";
			if(jQuery_1_3_2){		
				(function($){
					$('div#calendar_wrapper').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
				})(jQuery_1_3_2);
			}else{
				$('div#calendar_wrapper').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
			}
		}
// 		console.log("testing");
		if (this.activate && this.searchType!='A'){
			//this.searchValue[this.searchValue.length] = searchValue;
			this.searchValue = new Array();
			this.searchValue[0] = searchValue;
		}
		else {
			this.searchValue = new Array();
			this.searchValue[0] = searchValue;
			this.time = $('input[@name=time]:hidden').val();
			this.period = this.searchType=='A'?'A':$('input[@name=calViewPeriod]:hidden').val();
			this.agendaPeriod = $('select[@name=selectAgendaPeriod] option:selected').val();
		}
		//console.log(this.agendaPeriod);
		var serverScript = "eventSearch.php?search=1&agendaPeriod="+this.agendaPeriod;
		this.activate = true;		
		
		params = {};
		params['searchValue[]'] = this.searchValue;
		//params['searchValueLength'] = this.searchValue.length;
		params['period'] = this.period;
		params['startTime'] = this.time;
		params['searchType'] = this.searchType;
		params['pageNo'] = 1;
		params['maxPageNo'] = this.maxPageNo;
		
		$("#calendar_wrapper").load(serverScript,
		params,
		function(responseText){	
			sessionExpire(responseText);
			searchEngin.searchResult = responseText;
			searchEngin.searchTab.style.display = 'inline';
			tab = $("#caltabs > ul > li").attr("id", "");
			searchEngin.searchTab.id = 'current';
			document.getElementById("cal_day_range").style.display = "none";
			if(jQuery_1_3_2){
				(function($){
					$('div#calendar_wrapper').unblock();
				})(jQuery_1_3_2);
			}else{
				$('div#calendar_wrapper').unblock();
			}
			isBlocking = false;
			document.body.scrollTop = 0;
			searchEngin.maxPage = document.getElementById('resultMaxPage').value;
			searchEngin.pageNo = 1;
			if (searchEngin.maxPage == 0)
				document.getElementById('search_navigation').innerHTML = '';
			else
				searchEngin.createNavigation(searchEngin.pageNo,searchEngin.maxPageNo);
			document.getElementById('search_navigation').style.visibility = 'visible';
			initCalendar();
		});
	}
	
	this.tabActivate= function(){
		this.activate = true;
		document.getElementById('cal_day_range').style.display = 'none';
		tab = $("#caltabs > ul > li").attr("id", "");
		this.searchTab.id = 'current';
		document.getElementById('calendar_wrapper').innerHTML = this.searchResult;
		$("select[@name=selectAgendaPeriod]").remove();
		document.getElementById('search_navigation').style.visibility = 'visible';
		initCalendar();
	}
	this.setSearchType=function(type){
		this.searchType=type;
	}
	this.deactivate = function(){
		this.activate = false;
		document.getElementById('search_navigation').style.visibility = 'hidden';
	}
	this.createNavigation = function(currentPageNo,currentMaxPageNo){
		currentPageNo = currentPageNo==null?0:currentPageNo-1;
		currentMaxPageNo = currentMaxPageNo==null?50:currentMaxPageNo;
		pArrow = '<a href="javascript:searchEngin.prevPage()"><img id="page_previous" hspace="3" height="10" width="11" border="0" align="absmiddle" name="page_previous" src="/images/2009a/icon_prev_off.gif"/></a>';
		nArrow = '<a href="javascript:searchEngin.nextPage()"><img id="page_next" hspace="3" height="10" width="11" border="0" align="absmiddle" name="page_next" src="/images/2009a/icon_next_off.gif"/></a>';
		page = '<?=$list_page?>';	
		display = '<?=$i_StudentPromotion_mgt['DISPLAY']?>';
		
		selectPage = '<select name="pageNo" id="pageNo" onchange="searchEngin.setPage(this)">';				
		for (i=0;i<this.maxPage;i++){
			selectPage += '<option value="'+(i+1)+'" '+(i==currentPageNo?'selected':'')+'>'+(i+1)+'</option>'
		}
		selectPage += '</select>';
		
		selectMax = '<select name="maxPageNo" id="maxPageNo" onchange="searchEngin.setMaxPage(this)">';
		for (i=10;i<=100;i+=10){
			selectMax += '<option value="'+(i)+'" '+(i==currentMaxPageNo?'selected':'')+'>'+(i)+'</option>'
		}
		selectMax += '</select>';
		
		document.getElementById('search_navigation').innerHTML = (pArrow+page+selectPage+nArrow+'&nbsp;&nbsp;&nbsp;'+display+selectMax+'/'+page);
	}
	this.setPage=function(obj){
		this.pageNo = obj.selectedIndex+1;
		this.loadPage();
	}
	this.prevPage=function(){
		if (this.pageNo==1)
			return;
		this.pageNo--;
		document.getElementById('pageNo').selectedIndex = this.pageNo-1;
		this.loadPage();
	}
	this.nextPage=function(){
		if (this.pageNo == this.maxPage)
			return;
		this.pageNo++;
		document.getElementById('pageNo').selectedIndex = this.pageNo-1;
		this.loadPage();
	}
	this.loadPage = function(){
		$displayTitle = "<?=$i_Calendar_Loading?>";
		if (!isBlocking) {
			var blockMsg = "<h2 style='padding-top:10px;'>"+$displayTitle+"</h2>";		
			if(jQuery_1_3_2){
				(function($){
					$('div#calendar_wrapper').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
				})(jQuery_1_3_2);
			}else{
				$('div#calendar_wrapper').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
			}
		}
		params = {};
		params['searchValue[]'] = this.searchValue;
		//params['searchValueLength'] = this.searchValue.length;
		params['period'] = this.period;
		params['startTime'] = this.time;
		params['searchType'] = this.searchType;
		params['pageNo'] = this.pageNo;
		params['maxPageNo'] = this.maxPageNo;
		var serverScript = "eventSearch.php?search=1&agendaPeriod="+this.agendaPeriod;
		$("#calendar_wrapper").load(serverScript,
		params,
		function(responseText){		
			sessionExpire(responseText);
			searchEngin.searchResult = responseText;
			searchEngin.searchTab.style.display = 'inline';
			tab = $("#caltabs > ul > li").attr("id", "");
			searchEngin.searchTab.id = 'current';
			document.getElementById("cal_day_range").style.display = "none";
			searchEngin.maxPage = document.getElementById('resultMaxPage').value;
			if(jQuery_1_3_2){
				(function($){
					$('div#calendar_wrapper').unblock();
				})(jQuery_1_3_2);
			}else{
				$('div#calendar_wrapper').unblock();
			}
			isBlocking = false;
			document.body.scrollTop = 0;
			searchEngin.createNavigation(searchEngin.pageNo,searchEngin.maxPageNo);
			initCalendar();
		}); 
	}
	this.setMaxPage = function(obj){
		this.maxPageNo = (obj.selectedIndex+1)*10;
		this.pageNo = 1;
		this.loadPage();		
	}
}


function getCalendarDisplay(period, calID) {
	
	// Reset_Timeout();
	jHide_All_Layers();
	
	$displayTitle = "<?=$i_Calendar_Loading?>";
	if (calID != null){
		$displayTitle = "<?=$i_Calendar_Synchronizing?>";		
	}
	
	shownPeriod = period;
	if (!isBlocking) {
		var blockMsg = "<h2 style='padding-top:10px;'>"+$displayTitle+"</h2>";
		if (calID != null)
			blockMsg = "<h2 style='padding-top:10px; font-size: 13px'>"+$displayTitle+"</h2>";
			if(jQuery_1_3_2){
				(function($){
					$('div#calendar_wrapper').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
				})(jQuery_1_3_2);
			}else{
				$('div#calendar_wrapper').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
			}
	}
	var ts = $("input[@name='time']").val();
	var viewMode = (period == 'monthly') ? document.getElementById('calViewSelect').value : 'simple';
	// var viewMode;
	
	if (period == "agenda") {
		var agendaPeriod = "";
		if (document.getElementById("selectAgendaPeriod") != undefined) {
			for (var i=0;i<document.getElementById("selectAgendaPeriod").options.length;i++) {
				if (document.getElementById("selectAgendaPeriod").options[i].selected == true) {
					agendaPeriod = document.getElementById("selectAgendaPeriod").options[i].value;
				}
			}
		} else {
			agendaPeriod = 1;
		}
		var serverScript = "get_agenda.php?agendaPeriod="+agendaPeriod;
	} else {
		var serverScript = "get_calendar.php";
	}
	
	$("#calendar_wrapper").load(
		serverScript, 
	  {calViewPeriod: period, time: ts, syncCalId: calID, calViewMode: viewMode},
	  function(responseText) {
		sessionExpire(responseText);
		if (!SessionExpired(responseText)){
			re = responseText.replace(/(^\s*)|(\s*$)/g, ""); 
			fail = false;
			failType = "";
			if (re.match("^fail")){
				fail = true;
				if (re.substr(4,1) == "1"){
					failType = "<?=$iCalendar_Import_Error1?>";
				}
				else if (re.substr(4,1) == "2"){
					failType = "<?=$iCalendar_Import_Error2?>";
				}
				document.getElementById("calendar_wrapper").innerHTML= re.substr(5);
			}
			
			var theDate = new Date(ts*1000);
			var nextDate = new Date(ts*1000);
			var lastDate = new Date(ts*1000);
			
			// alert(theDate);
			if (searchEngin.activate){
				searchEngin.tabActivate()
			}
			else if (period == "monthly") {
				document.getElementById("cal_day_range").style.display = "block";
				
				nextDate.setMonth(nextDate.getMonth()+1);
				lastDate.setMonth(lastDate.getMonth()-1);
				
				var newMonth = theDate.getMonth();
				var newMonthName = monthName[newMonth];
				var newYear = theDate.getFullYear();
				
				$("#periodTitle").html(newMonthName+" "+newYear);
				//console.log(nextDate, " " , lastDate);
				//console.log(newMonthName, " " , newYear);
				
				var nextMonthTs = (nextDate.getTime())/1000;
				$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextMonthTs+"';getCalendarDisplay('monthly');jQuery.datepicker._adjustDate(0, +1, 'M');");
				
				var lastMonthTs = (lastDate.getTime())/1000;
				$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastMonthTs+"';getCalendarDisplay('monthly');jQuery.datepicker._adjustDate(0, -1, 'M');");
				initCalendar();
				document.getElementById("cal_view_selector").style.display = '';
				
				// Change Tab link
				$("a[@name='tabMonthly']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('monthly');");
				$("a[@name='tabWeekly']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('weekly');");
				$("a[@name='tabDaily']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('daily');");
				$("a[@name='tabAgenda']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('agenda');");
				
			} else if (period == "weekly") {
				document.getElementById("cal_day_range").style.display = "block";
				
				nextDate.setDate(nextDate.getDate()+7);
				lastDate.setDate(lastDate.getDate()-7);
				
				$("#periodTitle").html(getCurrentWeek(theDate));
				
				var nextWeekTs = (nextDate.getTime())/1000;
				$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextWeekTs+"';getCalendarDisplay('weekly');jQuery.datepicker._adjustDate(0,+7,'D');");
				
				var lastWeekTs = (lastDate.getTime())/1000;
				$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastWeekTs+"';getCalendarDisplay('weekly');jQuery.datepicker._adjustDate(0,-7,'D');");
				var inst = jQuery.datepicker._getInst(0);		
				inst._selectedDay = theDate.getDate();
				inst._drawMonth = inst._selectedMonth = theDate.getMonth();
				inst._drawYear = inst._selectedYear = theDate.getFullYear();
				jQuery.datepicker._adjustDate(inst);
				
				initCalendar();
				document.getElementById("cal_view_selector").style.display = 'none';
				
				// Change Tab link
				$("a[@name='tabMonthly']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('monthly');");
				$("a[@name='tabWeekly']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('weekly');");
				$("a[@name='tabDaily']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('daily');");
				$("a[@name='tabAgenda']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('agenda');");
				
			} else if (period == "daily") {
				document.getElementById("cal_day_range").style.display = "block";
				
				nextDate.setDate(nextDate.getDate()+1);
				lastDate.setDate(lastDate.getDate()-1);
				
				var newWeekday = weekdayName[theDate.getDay()];
				// $("#periodTitle").html(newMonthName+" "+newYear);
				$("#periodTitle").html(newWeekday+", "+theDate.getDate()+"-"+(theDate.getMonth()+1)+"-"+theDate.getFullYear());				
				var nextDayTs = (nextDate.getTime())/1000;
				$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextDayTs+"';getCalendarDisplay('daily');jQuery.datepicker._adjustDate(0,+1,'D');");
				
				var lastDayTs = (lastDate.getTime())/1000;
				$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastDayTs+"';getCalendarDisplay('daily');jQuery.datepicker._adjustDate(0,-1,'D');");
				//alert(theDate);
				var inst = jQuery.datepicker._getInst(0);
				inst._selectedDay = theDate.getDate();
				jQuery.datepicker._adjustDate(inst);				
				
				initCalendar();
				document.getElementById("cal_view_selector").style.display = 'none';
				
				// Change Tab link
				$("a[@name='tabMonthly']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('monthly');");
				$("a[@name='tabWeekly']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('weekly');");
				$("a[@name='tabDaily']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('daily');");
				$("a[@name='tabAgenda']").attr("href", "javascript:document.form1.time.value='"+ts+"';changePeriodTab('agenda');");
				
			} else if (period == "agenda") {
				document.getElementById("cal_day_range").style.display = "block";
				
				nextDate.setDate(nextDate.getDate()+7);
				lastDate.setDate(lastDate.getDate()-7);
				
				var displayNextDate = new Date(ts*1000);
					displayNextDate.setDate(theDate.getDate()+6);
				var thisWeekday = weekdayName[theDate.getDay()];
				var nextWeekday = weekdayName[displayNextDate.getDay()];
				$("#periodTitle").html(
					thisWeekday+", "+theDate.getDate()+"-"+(theDate.getMonth()+1)+"-"+theDate.getFullYear()+' - '+
					nextWeekday+", "+displayNextDate.getDate() +"-"+(displayNextDate.getMonth()+1)+"-"+displayNextDate.getFullYear()
				);
				// alert(theDate.getDate()+'\n'+Date(ts*1000));
				var nextDayTs = (nextDate.getTime())/1000;
				$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextDayTs+"';getCalendarDisplay('agenda');jQuery.datepicker._adjustDate(0,+7,'D');");
				
				var lastDayTs = (lastDate.getTime())/1000;
				$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastDayTs+"';getCalendarDisplay('agenda');jQuery.datepicker._adjustDate(0,-7,'D');");
				initCalendar();
				document.getElementById("cal_view_selector").style.display = 'none';
				
				// Change Tab link
				$("a[@name='tabMonthly']").attr("href", "javascript:document.form1.time.value='"+(theDate.getTime() /1000)+"';changePeriodTab('monthly');");
				$("a[@name='tabWeekly']").attr("href", "javascript:document.form1.time.value='"+(theDate.getTime() /1000)+"';changePeriodTab('weekly');");
				$("a[@name='tabDaily']").attr("href", "javascript:document.form1.time.value='"+(theDate.getTime() /1000)+"';changePeriodTab('daily');");
				$("a[@name='tabAgenda']").attr("href", "javascript:document.form1.time.value='"+(theDate.getTime() /1000)+"';changePeriodTab('agenda');");
				
			} else {
				document.getElementById("cal_day_range").style.display = "none";
				document.getElementById("cal_view_selector").style.display = 'none';
			}
		
			jSet_User_Interface_Style(period);
			if(jQuery_1_3_2){	
				(function($){
					$('div#calendar_wrapper').unblock();
				})(jQuery_1_3_2);
			}else{
				$('div#calendar_wrapper').unblock();
			}
			isBlocking = false;
			
			returnMsg = "";
			returnClass = "";
			if (fail){
				returnMsg = failType;
				returnClass = "return_status_warning";
			}
			else{
				returnMsg = "<?=$iCalendar_Import_Success?>";
				returnClass= "return_status_success";
			}
			if (calID != null){
				document.getElementById("return_status").style.display = "block";
				document.getElementById("return_status").innerHTML = '<span class="'+returnClass+'"><strong>'+returnMsg+'</strong><a onclick="document.getElementById(\'return_status\').style.display = \'none\';" href="#">[X]</a></span>';
			}
			
		}
		$('.cal_month_event_item').css('margin','0px');
	  }
	);
	
} 

function toggleViewMode(period) {
	// Reset_Timeout();
	var calID = null;
	var viewMode = null;
	jHide_All_Layers();
	
	$displayTitle = "<?=$i_Calendar_Loading?>";
	
	if (calID != null){
		$displayTitle = "<?=$i_Calendar_Synchronizing?>";		
	}
	
	shownPeriod = period;
	
	if (shownPeriod == 'monthly') {
		viewMode = document.getElementById('calViewSelect').value;
	}
	
	if (!isBlocking) {
		var blockMsg = "<h2 style='padding-top:10px;'>"+$displayTitle+"</h2>";
		if (calID != null)
			blockMsg = "<h2 style='padding-top:10px; font-size: 13px'>"+$displayTitle+"</h2>";
		if(jQuery_1_3_2){
			(function($){
				$('div#calendar_wrapper').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
			})(jQuery_1_3_2);
		}else{
			$('div#calendar_wrapper').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		}
	}
	var ts = $("input[@name='time']").val();
	
	var serverScript = "get_calendar.php";
	
	$("#calendar_wrapper").load(
		serverScript, 
	  {calViewPeriod: period, time: ts, syncCalId: calID, calViewMode: viewMode},
	  function(responseText) {
		sessionExpire(responseText);
		if (!SessionExpired(responseText)){
			re = responseText.replace(/(^\s*)|(\s*$)/g, ""); 
			fail = false;
			failType = "";
			if (re.match("^fail")){
				fail = true;
				if (re.substr(4,1) == "1"){
					failType = "<?=$iCalendar_Import_Error1?>";
				}
				else if (re.substr(4,1) == "2"){
					failType = "<?=$iCalendar_Import_Error2?>";
				}
				document.getElementById("calendar_wrapper").innerHTML= re.substr(5);
			}
			
			var theDate = new Date(ts*1000);
			var nextDate = new Date(ts*1000);
			var lastDate = new Date(ts*1000);
			if (searchEngin.activate){
				searchEngin.tabActivate()
			}
			else if (period == "monthly") {
				document.getElementById("cal_day_range").style.display = "block";
				
				nextDate.setMonth(nextDate.getMonth()+1);
				lastDate.setMonth(lastDate.getMonth()-1);
				
				var newMonth = theDate.getMonth();
				var newMonthName = monthName[newMonth];
				var newYear = theDate.getFullYear();
				$("#periodTitle").html(newMonthName+" "+newYear);
				
				var nextMonthTs = (nextDate.getTime())/1000;
				$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextMonthTs+"';getCalendarDisplay('monthly');jQuery.datepicker._adjustDate(0, +1, 'M');");
				
				var lastMonthTs = (lastDate.getTime())/1000;
				$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastMonthTs+"';getCalendarDisplay('monthly');jQuery.datepicker._adjustDate(0, -1, 'M');");
				initCalendar();
				document.getElementById("cal_view_selector").style.display = '';
			} else if (period == "weekly") {
				document.getElementById("cal_day_range").style.display = "block";
				
				nextDate.setDate(nextDate.getDate()+7);
				lastDate.setDate(lastDate.getDate()-7);
				
				$("#periodTitle").html(getCurrentWeek(theDate));
				
				var nextWeekTs = (nextDate.getTime())/1000;
				$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextWeekTs+"';getCalendarDisplay('weekly');jQuery.datepicker._adjustDate(0,+7,'D');");
				
				var lastWeekTs = (lastDate.getTime())/1000;
				$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastWeekTs+"';getCalendarDisplay('weekly');jQuery.datepicker._adjustDate(0,-7,'D');");
				initCalendar();
				document.getElementById("cal_view_selector").style.display = 'none';
			} else if (period == "daily") {
				document.getElementById("cal_day_range").style.display = "block";
				
				nextDate.setDate(nextDate.getDate()+1);
				lastDate.setDate(lastDate.getDate()-1);
				
				var newWeekday = weekdayName[theDate.getDay()];
				
				$("#periodTitle").html(newWeekday+", "+theDate.getDate()+"-"+(theDate.getMonth()+1)+"-"+theDate.getFullYear());
				
				var nextDayTs = (nextDate.getTime())/1000;
				$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextDayTs+"';getCalendarDisplay('daily');jQuery.datepicker._adjustDate(0,+1,'D');");
				
				var lastDayTs = (lastDate.getTime())/1000;
				$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastDayTs+"';getCalendarDisplay('daily');jQuery.datepicker._adjustDate(0,-1,'D');");
				initCalendar();
				document.getElementById("cal_view_selector").style.display = 'none';
			} else {
				document.getElementById("cal_day_range").style.display = 'none';
				document.getElementById("cal_view_selector").style.display = 'none';
			}
		
			jSet_User_Interface_Style(period);
			if(jQuery_1_3_2){	
				(function($){
					$('div#calendar_wrapper').unblock();
				})(jQuery_1_3_2);
			}else{
				$('div#calendar_wrapper').unblock();
			}
			isBlocking = false;
			
			returnMsg = "";
			returnClass = "";
			if (fail){
				returnMsg = failType;
				returnClass = "return_status_warning";
			}
			else{
				returnMsg = "<?=$iCalendar_Import_Success?>";
				returnClass= "return_status_success";
			}
			if (calID != null){
				document.getElementById("return_status").style.display = "block";
				document.getElementById("return_status").innerHTML = '<span class="'+returnClass+'"><strong>'+returnMsg+'</strong><a onclick="document.getElementById(\'return_status\').style.display = \'none\';" href="#">[X]</a></span>';
			}
			
		}
		$('.cal_month_event_item').css('margin','0px');
	  }
	);
}

function getCurrentWeek(theDate) {
	var day = theDate.getDate();
	var month = theDate.getMonth() + 1;
	var year = theDate.getFullYear();
	var shortYear = theDate.getYear();
	var offset = theDate.getDay();
	var week;
	
	if(offset != 0) {
		day = day - offset;
		if ( day < 1) {
			if (month==1 || month==2 || month==4 || month==6 || month==8 || month==9 || month==11) {
				day = 31 + day;
			}
			if (month == 3) {
				// Calculate leap year
				if (((year%4 == 0) && (year%100 != 0)) || (year%400 == 0)) {
					day = 29 + day;
				} else {
					day = 28 + day;
				}
			}
			if (month==5 || month==7 || month==10 || month==12) {
				day = 30 + day;
			}
			if (month == 1) {
				month = 12;
				year = year - 1;
			} else {
				month = month - 1;
			}
		}
	}
	firstWeeday = monthName[month-1] + " " + day + ", " + year;
	
	lastWeekday = new Date(year, month-1, day, 0, 0, 0);
	lastWeekday.setDate(lastWeekday.getDate()+6);
	lastWeekday = monthName[lastWeekday.getMonth()] + " " + lastWeekday.getDate() + ", " + lastWeekday.getFullYear();
	
	return firstWeeday + " - " + lastWeekday;
}

/* Hide all layers if loading other months of calendar or different views */
function jHide_All_Layers(){
	jHide_Event_ToolTips();
	jHide_Event_Entry_List();
}
/* End */

/* Event Tooltips Info Function */
function jGet_Event_Info(eventId, period, calType)
{
		$('#cluetip').css('display','none')
	// Reset_Timeout();
	if (!isBlocking) {
		var blockMsg = "";
		$('div#cluetip-inner').css({position:'static', width:'95%'});
		if(jQuery_1_3_2){
			(function($){
				$('div#cluetip-inner').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
			})(jQuery_1_3_2);
		}else{
			$('div#cluetip-inner').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		}
	}
	var serverScript = "get_event_info.php";
	$("#cluetip-inner").load(
		serverScript, 
		{calViewPeriod: period, eventID: eventId, 
		 calType: calType},
		function(responseText) {
			sessionExpire(responseText);
			//if (!SessionExpired(responseText)){ 
			if(jQuery_1_3_2){	
				(function($){
					$('div#cluetip-inner').unblock();
				})(jQuery_1_3_2);
			}else{
				$('div#cluetip-inner').unblock();
			}
				isBlocking = false;
							
			//}
		}
	);
}

function jHide_Event_ToolTips()
{
	var obj = document.getElementById("cluetip");
	obj.style.display = "none";
}

// CPD Programme - eventid = sessionid
function jGet_Other_CalType_Event_Info(eventId, otherId, period, calType)
{
	// Reset_Timeout();
	if (!isBlocking) {
		var blockMsg = "";
		if(jQuery_1_3_2){
			(function($){
				$('div#cluetip-inner').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
			})(jQuery_1_3_2);
		}else{
			$('div#cluetip-inner').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		}
	}
	var programmeId = (calType == <?=ENROL_CPD_CALENDAR?>) ? otherId : '';
	
	var serverScript = "get_other_caltype_event_info.php";
	$("#cluetip-inner").load(
		serverScript, 
		{calViewPeriod: period, eventID: eventId,
		 calType: calType, programmeID : programmeId},
		function(responseText) {
			if (!SessionExpired(responseText)){
				if(jQuery_1_3_2){ 
					(function($){
						$('div#cluetip-inner').unblock();
					})(jQuery_1_3_2);
				}else{
					$('div#cluetip-inner').unblock();
				}
				isBlocking = false;
			}
		}
	);
}

/* End of Event Tooltips Info Function */

/* Event More Tooltips Info Function */
function jGet_Event_More_Info(eventId, period, isInvolveEvt, calType){
	// Reset_Timeout();
	if (!isBlocking) {
		var blockMsg = "";
		if(jQuery_1_3_2){
			(function($){
				$('div#event_tooltips-inner').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
			})(jQuery_1_3_2);
		}else{
			$('div#event_tooltips-inner').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		}
	}
	var serverScript = "get_event_info.php";
	$("#event_tooltips-inner").load(
		serverScript, 
		{calViewPeriod: period, eventID: eventId, 
		 isEventMore: true, calType: calType},
		function(responseText) {
			if (!SessionExpired(responseText)){ 
				sessionExpire(responseText);
				if(jQuery_1_3_2){
					(function($){
						$('div#event_tooltips-inner').unblock();
					})(jQuery_1_3_2);
				}else{
					$('div#event_tooltips-inner').unblock();
				}
				isBlocking = false;
			}
		}
	);
	jShow_Event_More_Info(eventId, isInvolveEvt, '');
}

// CPD Programme - eventid = sessionid
function jGet_Other_CalType_Event_More_Info(eventId, otherId, period, calType)
{
	// Reset_Timeout();
	if (!isBlocking) {
		var blockMsg = "";
		if(Query_1_3_2){
			(function($){
				$('div#event_tooltips-inner').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
			})(jQuery_1_3_2);
		}else{
			$('div#event_tooltips-inner').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		}
	}
	
	var programmeId = (calType == 3) ? otherId : '';
	
	var serverScript = "get_other_caltype_event_info.php";
	$("#event_tooltips-inner").load(
		serverScript, 
		{calViewPeriod: period, eventID: eventId,
		 isEventMore: true, calType: calType, programmeID : programmeId},
		function(responseText) {
			if (!SessionExpired(responseText)){
				if(jQuery_1_3_2){ 
					(function($){
						$('div#event_tooltips-inner').unblock();
					})(jQuery_1_3_2);
				}else{
					$('div#event_tooltips-inner').unblock();
				}
				isBlocking = false;
			}
		}
	);
	jShow_Event_More_Info(eventId, 0, calType);
}

function jHide_Event_More_Info()
{
	$('div#event_tooltips').hide();
}

function jShow_Event_More_Info(eventId, isInvolveEvt, calType){
	var obj = document.getElementById("event_tooltips");
	var objName = (isInvolveEvt == 1) ? "involveEventMore" : "eventMore";
	objName = (calType != '') ? jGet_Other_CalType_Name(calType)+"EventMore" : objName;

	var objLink = document.getElementById(objName + eventId);
 	obj.style.top = getPostion(objLink, 'offsetTop') - 5;
	obj.style.left = getPostion(objLink, 'offsetLeft') + 45;
	$('div#event_tooltips').show();
}
/* End of Event More Tooltips Info Function */

/* Event Entry List Function */
function jShow_Event_Entry_List(evt_date){	
	jHide_Event_More_Info();

	var obj = document.getElementById("event_entry_list");
	var objLink = document.getElementById("create_"+evt_date);
 	obj.style.top = getPostion(objLink, 'offsetTop') + 14;
	obj.style.left = getPostion(objLink, 'offsetLeft');
 	
	$('#event_entry_list').show('slow', 
		function(){
			$('#event_entry_list').width("180px");
			if($('#event_entry_list_detail').height() > 200){
				$('#event_entry_list_detail').height("200px");
				$('#event_entry_list_detail').css("overflow", "auto");
			}
		}
	);
}

function jHide_Event_Entry_List(){
	$('#event_entry_list').hide('slow');
	jHide_Event_More_Info();
}

function jGet_Event_Entry_List(event_date){
	// Reset_Timeout();
	if (!isBlocking) {
		var blockMsg = "";
		if(jQuery_1_3_2){
			(function($){
				$('div#event_entry_list').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
			})(jQuery_1_3_2);
		}else{
			$('div#event_entry_list').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		}
	}
	var serverScript = "get_event_entry_list.php";
	$("#event_entry_list").load(
		serverScript, 
		{eventDate: event_date},
		function(responseText) {
			if (!SessionExpired(responseText)){ 
				sessionExpire(responseText);
				if(jQuery_1_3_2){
					(function($){
						$('div#event_entry_list').unblock();
					})(jQuery_1_3_2);
				}else{
					$('div#event_entry_list').unblock();
				}
				isBlocking = false;	
			}
		}
	);
	jShow_Event_Entry_List(event_date);
}
/* End of Event Entry List Function */

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	# Look for remainder in the next 12 hours
	//echo $iCal->haveFutureReminder(12);
?>

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->

function jHide_Div(obj){
	var status = obj.style.display;
	
	if(status == "none"){
		obj.style.display = "block";
	} else {
		obj.style.display = "none";
	}
}

/*
function jShow_Active_Event(objName, ActiveEventType, eventID){
	jGet_Active_Event(ActiveEventType, eventID);
	
	var objDiv = document.getElementById("cal_act_event_div");
	var objLink = document.getElementById(objName);
	
	objDiv.style.left = getPostion(objLink, 'offsetLeft') + 10;
 	objDiv.style.top = getPostion(objLink, 'offsetTop') + 10;
		
 	objDiv.style.visibility = "visible";
}

function jHide_Active_Event(){
	var objName = "cal_act_event_div";
	var objDiv = document.getElementById(objName);
	objDiv.style.visibility = "hidden";
}

function jGet_Active_Event(ActiveEventType, eventID) {
	if (!isBlocking) {
		var blockMsg = "<h2 style='padding-top:10px;'><?=$i_Calendar_Loading?></h2>";
		if(jQuery_1_3_2){
			(function($){
				$('div#cal_act_event_wrapper').block({message:blockMsg, overlayCSS: {color:'#ccc' ,border:'3px solid #ccc' }});
			})(jQuery_1_3_2);
		}else{
			$('div#cal_act_event_wrapper').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		}
	}
	
	var params = (eventID != "") ? "&eventID="+eventID : "";
	if(ActiveEventType == "Notification"){
		var serverScript = "get_active_event.php?ActiveEventType="+ActiveEventType+params;
	} else {
		var serverScript = "get_active_event.php?ActiveEventType="+ActiveEventType+params;
	}
	
	$("#cal_act_event_wrapper").load(
		serverScript, 
	  function() {
		  if(jQuery_1_3_2){
		  	(function($){
		  		$('div#calendar_wrapper').unblock();
		  	})(jQuery_1_3_2);
		  }else{
		  	$('div#calendar_wrapper').unblock();
		  }
	  	isBlocking = false;
	  }
	);
}*/
var cur_period = '<?=$calViewPeriod?>';
function jSet_User_Interface_Style(period){
	var view = period || cur_period;
	if(document.all){	// IE
		if(view == "monthly"){
			$('.cal_month_event_item').css("margin-top", "2px");
		}
	} else {			// non IE
	}
}
// function callbackWindow(){
// window.opener.document.getElementById('calender_content').innerHTML='Hello';
// }

</script>
<?php 
//echo $iCal->Get_Active_Event_Board_Div(); 
?>

<div id="event_tooltips" class="clue-right-jtip cluetip-jtip" style="z-index:96;display:none;position:absolute;width:275px;background-position: 0pt 3px;">
  <div id="event_tooltips-outer" style="overflow:visible;position:relative;z-index:97;height: auto;background-color:#FFFFFF;border:2px solid #CCCCCC;">
    <div id="event_tooltips-inner" style="position: relative;"></div>
  </div>
</div>
<div id="event_entry_list" style="display:none;position:absolute;z-index:56;width:180px;background-color:#FFFFFF;border:solid 1px #000000"></div>
<div id="module_bulletin" class="module_content">
	<?php 
		echo $esfUI->Get_Sub_Function_Header($icalendar_name,$Msg);
		echo $Lang['calendar']['HeaderTitle'];
		echo $display;
	?>
	<form name="delete_form" id="delete_form" action="" method="post">
	  <div class="jqmWindow" id="deleteEventWarning1">
		<p align="center"><?=$iCalendar_DeleteEvent_ConfirmMsg1?></p>
		<p align="center"><?=$deleteEventButtons1?></p>
	  </div>
	  <div class="jqmWindow" id="deleteEventWarning2">
		<p align="center"><div id="deleteEventConfirmMsg2" align="center"></div></p>
		<p align="center"><?=$deleteEventButtons2?></p>
	  </div>
	  <?php
	    # Parameters that need to pass to the delete page
	  	echo $esfUI->Get_Input_Hidden("eventID", "eventID", "");
	  	echo $esfUI->Get_Input_Hidden("deleteRepeatEvent", "deleteRepeatEvent", "OTI");
		echo $esfUI->Get_Input_Hidden("isRepeatEvent", "isRepeatEvent", "");
		echo $esfUI->Get_Input_Hidden("repeatID", "repeatID", "");
		echo $esfUI->Get_Input_Hidden("oldEventDate", "oldEventDate", "");
	  ?>
	</form>
</div>
<!-- <input type='button' value='test' onclick='callbackWindow'> -->
<script>
jSet_User_Interface_Style();
</script>
<?php
// include_once($PathRelative."src/include/template/general_footer.php");
intranet_closedb();
include_once("footerInclude.php");
?>

