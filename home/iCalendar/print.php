<?php
// page modifing by: 
/*
 * 2015-07-29 (Carlos): Fixed agenda view time period.
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");;
intranet_auth();
intranet_opendb();

#############################################################################################

$iCal = new icalendar();
$lui = new ESF_ui();

# Initiailization
$own_events = array();
$involve_events = array();
$total_events = array();

if($calViewPeriod != "agenda"){
	$calViewPeriodArr = array("daily","weekly","monthly");
	if (!isset($calViewPeriod) || !in_array($calViewPeriod, $calViewPeriodArr)&& $calViewPeriod!='search')
		$calViewPeriod = $iCal->systemSettings["PreferredView"];
	if ($calViewPeriod == "")
		$calViewPeriod = "monthly";
}
$time = IntegerSafe($time);
if (isset($time) && is_numeric($time)) {
	$year = date('Y', $time);
	$month = date('m', $time);
	$day = date('d', $time);
} else if (!isset($year) && !isset($month)){
	$time = time();
	$year = date('Y');
	$month = date('m');
	$day = date('d');
}

# Record the display view in session during each visit to this page
$_SESSION["iCalDisplayPeriod"] = $calViewPeriod;
$_SESSION["iCalDisplayTime"] = $time;


$i_title = "<div class=\"report_title\"><strong>";

# Get Event Data
// Pre-load the non-repeating events for quicker access 
switch ($calViewPeriod) {
	case "monthly":
		//$events = $iCal->query_events(mktime(0,0,0,$month,1,$year), mktime(0,0,0,$month+1,1,$year));		# old appraoch
		$lower_bound = mktime(0,0,0,$month,1,$year);
		$upper_bound = mktime(0,0,0,$month+1,1,$year);
		
		$i_title .= $iCalendar_NewEvent_Repeats_MonthArray[date('n', mktime(0,0,0,$month,$day,$year))-1]." ".$year;
		$lower_bound += $iCal->offsetTimeStamp;
		$upper_bound += $iCal->offsetTimeStamp;
		break;
	case "weekly":
		$current_week = $iCal->get_week($year, $month, $day, "Y-m-d H:i:s");
		$lower_bound = $iCal->sqlDatetimeToTimestamp($current_week[0]);
		$upper_bound = $iCal->sqlDatetimeToTimestamp($current_week[6]);
		$upper_bound = $upper_bound + 24*60*60;
		//$events = $iCal->query_events($lower_bound, $upper_bound);
		/*
		if ($intranet_session_language == "b5")
			$current_week = $iCal->get_week($year, $month, $day, "Y�~n��j��");
		else
		*/
		$current_week = $iCal->get_week($year, $month, $day, "F j, Y");
		$i_title .= "$current_week[0] - $current_week[6]";
		$lower_bound += $iCal->offsetTimeStamp;
		$upper_bound += $iCal->offsetTimeStamp;
		break;
	case "daily":
		$lower_bound = mktime(0,0,0,$month,$day,$year);
		$upper_bound = $lower_bound+24*60*60;
		# Differ from a Offset 1970101
		$lower_bound += $iCal->offsetTimeStamp;
		$upper_bound += $iCal->offsetTimeStamp;
		//$events = $iCal->query_events($lower_bound, $upper_bound);
		$i_title .= $iCalendar_NewEvent_Repeats_WeekdayArray2[date("w", $time)].", ";
		/*
		if ($intranet_session_language == "b5")
			$i_title .= date("Y�~n��j��",$time);
		else
		*/
		$i_title .= date("F j, Y",$time);
		break;
	case "agenda":
		/*
		 * $agendaPeriod: 0=today, 1=next 7 days, 2=this week
		 */
		if (!isset($startDate) || $startDate == "") {
			//$startDate = date("Y-m-d");
			$startDate = date("Y-m-d",$time);
		}
		if (!isset($endDate) || $endDate == "") {
			if ($agendaPeriod == "1") {
				$endDate = date("Y-m-d", strtotime($startDate." 00:00:00") + (7*24*60*60));
			} else if  ($agendaPeriod == "2") {
				//$currentWeek = $iCal->get_week(date("Y"), date("n"), date("j"), "Y-m-d");
				$currentWeek = $iCal->get_week($year, $month, $day, "Y-m-d");
				$startDate = $currentWeek[0];
				$endDate = $currentWeek[6];
			} else {
				//$endDate = date("Y-m-d", time(date("Y-m-d")." 00:00:00") + (24*60*60));
				$endDate = date("Y-m-d", strtotime($startDate." 00:00:00") + (24*60*60));
			}
			
			# Presentation of Date Period selected
			$yearUI = date('Y', strtotime($endDate));
			$monthUI = date('m', strtotime($endDate));
			$dateUI = date('j', strtotime($endDate));
			$dateUI = (strlen($dateUI) == 1) ? str_pad(($dateUI-1), 2, 0, STR_PAD_LEFT) : $dateUI;
			$endDateUI = $yearUI.'-'.$monthUI.'-'.$dateUI;
		}

		$startDatePiece = explode("-", $startDate);
		$endDatePiece = explode("-", $endDate);

		# TimeStamp
		$lower_bound = mktime(0,0,0,$startDatePiece[1],$startDatePiece[2],$startDatePiece[0]);
		$upper_bound = mktime(0,0,0,$endDatePiece[1],$endDatePiece[2],$endDatePiece[0]) - 1;
		# Differ from a Offset 1970101
		$lower_bound += $iCal->offsetTimeStamp;
		$upper_bound += $iCal->offsetTimeStamp;
		//$events = $iCal->query_events($lower_bound, $upper_bound);

		//$schoolEvents = $iCal->returnSchoolEventRecord($lower_bound, $upper_bound);
		/*
		if ($intranet_session_language == "b5") {
			$i_title .= $startDatePiece[0]."�~".$startDatePiece[1]."��".$startDatePiece[2]."�� �� ";
			$i_title .= $endDatePiece[0]."�~".$endDatePiece[1]."��".$endDatePiece[2]."��";
		} else
		*/
		# display title date
		$i_title .= ($agendaPeriod == "1" || $agendaPeriod == "2") ? "$startDate to $endDateUI" : "$startDate";
		break;
}

/*## Get Event
$own_events = $iCal->Query_User_Related_Events($lower_bound, $upper_bound, false, false, true,false,true); 
$involve_events = $iCal->Query_User_Related_Events($lower_bound, $upper_bound, true, false, true,false,true);
$foundation_events = $iCal->Query_User_Foundation_Events($lower_bound, $upper_bound, false, true);	# Foundation Event
$total_events = array_merge($own_events, $involve_events);
$total_events = array_merge($total_events, $foundation_events);

# Sorting after combining 2 events arrays
if(count($total_events) > 0){
	foreach($total_events as $key => $rows){
		$column[$key] = $rows['EventDate'];
	}
	array_multisort($column, SORT_ASC, SORT_STRING, $total_events);
}
$events = $iCal->Return_Query_Events_in_Associated_Array($total_events);*/
if ($calViewPeriod == 'search'){
	$events = $iCal->Get_All_Related_Event('', '', true, $searchValue);
	krsort($events);
}
else
	$events = $iCal->Get_All_Related_Event($lower_bound, $upper_bound, true);

## UI
$i_title .= "</strong></div>";

# Choose CSS style sheet
$styleSheet = "css/display_calendar.css";
?>

<link rel="stylesheet" href="css/main.css" type="text/css" />
<link rel="stylesheet" href="<?=$styleSheet?>" type="text/css" />

<style type="text/css" media="print">
div,table,td,tr,body,span{
	overflow : visible;
}
</style>

<script language="javascript">
// Fix for IE6 image flicker (cannot solve the case of ":hover")
try {
	document.execCommand("BackgroundImageCache", false, true);
} catch(err) {}
</script>

<?php
	include_once($PATH_WRT_ROOT."templates/2009a/layout/print_header.php");
?>

<?php
/*
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$lui->Get_Input_Button("print", "print", "Print", "onclick=\"window.print()\"")?></td>
	</tr>
</table>
*/
?>
<br>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="center"><?=$i_title?></td>
	</tr>
</table>
<div id="cal_board_content">
	<div id="calendar_wrapper" style="width:98%">
		<?php
			switch ($calViewPeriod) {
				case "monthly":
					print $iCal->generateMonthlyCalendar($year, $month, true);
					break;
				case "weekly":
					//print $iCal->generateWeeklyCalendar($year, $month, $day, true);
					print $iCal->Generate_Weekly_Calendar($year, $month, $day, true);
					break;
				case "daily":
					//print $iCal->generateDailyCalendar($year, $month, $day, true);
					print $iCal->Generate_Daily_Calendar($year, $month, $day, true);
					break;
				case "agenda":
					//print $iCal->printAgenda($startDate, $endDate, true, $response, $agendaPeriod);
					print $iCal->Print_Agenda($startDate, $endDate, true, $response, $agendaPeriod);
					break;
				case 'search':
					print $iCal->Print_Search_result($searchValue,true);
				break;
				default: 
					break;
			}
		?>
	</div>
</div>
<br/ >

<script type="text/javascript" defer="1">
window.print();
</script>

<?php
include_once($PATH_WRT_ROOT."templates/2009a/layout/print_footer.php");
intranet_closedb();
?>
