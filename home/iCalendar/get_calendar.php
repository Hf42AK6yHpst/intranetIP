<?php
// page modifing by: 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");

if (empty($_SESSION['UserID'])) {
	//header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	echo 'SESSION_EXPIRED';
	exit;
}

intranet_opendb();

######################################################################################################################

## Use Library
$iCal = new icalendar();
$lui = new ESF_ui();


$syncCalId = trim($syncCalId);
if (!empty($syncCalId) && $syncCalId != "undefined"){
	$stmt = "";
	
	$sql = "select * from CALENDAR_CALENDAR where CalID = '$syncCalId'";
	$resultSet = $iCal->returnArray($sql);
	
	error_reporting(0);
	ob_start();
	 
	$f = fopen($resultSet[0]["SyncURL"], "r");
	$stmt = "";
	$isFail = false;
	if ($f){
		stream_set_timeout($f,5);
		while (!feof($f)) {
			$stmt .= fgets($f);
		}
		fclose($f);
		$info = stream_get_meta_data($f);
		if ($info['timed_out']){
			echo "fail1";
			$isFail = true;
		}
	}
	else{
		echo "fail1";
		$isFail = true;
	} 
	/*
	$stmt = "";
	$isFail = false;
	$f = curl_init();
	curl_setopt($f, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($f, CURLOPT_URL, trim($resultSet[0]["SyncURL"]));
	curl_setopt($f, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($f, CURLOPT_TIMEOUT, 60);
	$stmt = curl_exec($f);
	curl_close($f);
	if ($stmt === false){
		echo "fail1";
		$isFail = true;
	}
	*/
	if (!$isFail){
		if (empty($stmt)){
			echo "fail2";
			$isFail = true;
		}
		
		if (!$isFail){
			$events = $iCal->extract_icalEvent($stmt);

			if (!$events){ 
				echo "fail2";
				$isFail = true;
			}
		}
	}
	if (!$isFail) 
		$iCal->icalEvent_to_db($events,$syncCalId);
}

## Preparation
$calViewPeriodArr = array("daily","weekly","monthly");
if (!isset($calViewPeriod) || !in_array($calViewPeriod, $calViewPeriodArr))
	$calViewPeriod = $iCal->systemSettings["PreferredView"];

if (isset($time) && is_numeric($time)) {
	$year = date('Y', $time);
	$month = date('m', $time);
	$day = date('d', $time);
} else if (!isset($year) && !isset($month)){
	$time = time();
	$year = date('Y');
	$month = date('m');
	$day = date('d');
}

# Record the display view in session during each visit to this page
$_SESSION["iCalDisplayPeriod"] = $calViewPeriod;
$_SESSION["iCalDisplayTime"] = $time;


# Pre-load the events for quicker access
if ($calViewPeriod=="monthly") {
	$lower_bound = mktime(0,0,0,$month,1,$year);
	$upper_bound = mktime(0,0,0,$month+1,1,$year);
	//$events = $iCal->query_events($lower_bound, $upper_bound);							# old approach of getting events

	# Differ from a Offset 1970101 in DB
	$lower_bound += $iCal->offsetTimeStamp;
	$upper_bound += $iCal->offsetTimeStamp;

} else if ($calViewPeriod=="weekly") {
	$current_week = $iCal->get_week($year, $month, $day, "Y-m-d H:i:s");
	$lower_bound = $iCal->sqlDatetimeToTimestamp($current_week[0]);
	$upper_bound = $iCal->sqlDatetimeToTimestamp($current_week[6]);
	$upper_bound = $upper_bound + 24*60*60;
	$lower_bound += $iCal->offsetTimeStamp;
	$upper_bound += $iCal->offsetTimeStamp;
	//$events = $iCal->query_events($lower_bound, $upper_bound);	
	
} else if ($calViewPeriod=="daily") {
	$lower_bound = mktime(0,0,0,$month,$day,$year);
	$upper_bound = $lower_bound+24*60*60;
	
	# Differ from a Offset 1970101
	$lower_bound += $iCal->offsetTimeStamp;
	$upper_bound += $iCal->offsetTimeStamp;
	
	//$events = $iCal->query_events($lower_bound, $upper_bound);
} 


## Get Events
$events = $iCal->Get_All_Related_Event($lower_bound, $upper_bound,true);
// debug_r($events);
## Main

if ($calViewPeriod == "monthly") {
	//echo 'calViewMode: '.$calViewMode;
	$output = $iCal->generateMonthlyCalendar($year, $month, false, $calViewMode);
} else if ($calViewPeriod == "weekly") {
	//$output = $iCal->generateWeeklyCalendar($year, $month, $day);
	$output = $iCal->Generate_Weekly_Calendar($year, $month, $day);
} else if ($calViewPeriod == "daily") {
	//$output = $iCal->generateDailyCalendar($year, $month, $day);
	$output = $iCal->Generate_Daily_Calendar($year, $month, $day);
}

intranet_closedb();
//echo iconv("BIG5", "UTF-8", $output);

echo $output;
?>