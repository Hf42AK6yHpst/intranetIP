<?php
// Using : 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$iCal = new icalendar();

$result = array();

# User access right for this event
$userEventAccess = $iCal->returnUserEventAccess($_SESSION["UserID"],$eventID);

if ($userEventAccess != "A") {
	header("Location: index.php");
}

if (isset($isRepeatEvent) && $deleteRepeatEvent!="OTI") {
	$repeatID = $iCal->returnRepeatID($eventID);
	# get the whole repeat series
	$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
	for ($i=0; $i<sizeof($repeatSeries); $i++) {
		$repeatSeriesSql[$i] = $repeatSeries[$i]["EventID"];
	}
	$repeatSeriesSql = implode(",", $repeatSeriesSql);
	
	if ($deleteRepeatEvent=="ALL") {		### Delete All events
		# Check any record in School Calendar
		$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN ($repeatSeriesSql)";
		$arrSchoolEventID = $iCal->returnVector($sql);
		# delete related event record in school calendar
		if(sizeof($arrSchoolEventID) >0){
			$targetSchoolEventID = implode(",",$arrSchoolEventID);
			$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$targetSchoolEventID.")";
			$iCal->db_db_query($sql);
			$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$targetSchoolEventID.")";
			$iCal->db_db_query($sql);
			$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN (".$targetSchoolEventID.")";
			$iCal->db_db_query($sql);
		}
	
		# delete main detail
		$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = $repeatID";
		$result['delete_event_entry'] = $iCal->db_db_query($sql);
		
		# delete repeat pattern
		$sql = "DELETE FROM CALENDAR_EVENT_ENTRY_REPEAT WHERE RepeatID = $repeatID";
		$result['delete_repeat_event'] = $iCal->db_db_query($sql);
		
		# delete reminders
		$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID IN ($repeatSeriesSql)";
		$result['delete_event_reminder'] = $iCal->db_db_query($sql);
		
		# delete guests
		$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID IN ($repeatSeriesSql)";
		$result['delete_event_guest'] = $iCal->db_db_query($sql);
		
		# delete guests
		$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID IN ($repeatSeriesSql)";
		$result['delete_event_Personal_NOTE'] = $iCal->db_db_query($sql);
		
		##### EBOOKING #####
		if($plugin['eBooking'])
		{
			$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($repeatSeriesSql)";
			$arrBookingID = $iCal->returnVector($sql);
			
			if(sizeof($arrBookingID) > 0)
			{
				$targetBookingID = implode(",",$arrBookingID);
				
				$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
				$iCal->db_db_query($sql);
					
				$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
				$iCal->db_db_query($sql);
				
//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
//				$iCal->db_db_query($sql);
				
				$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetBookingID)";
				$iCal->db_db_query($sql);
			}
		}
	} else if ($deleteRepeatEvent=="FOL") { 	### Delete All of the following events
		### Get the cut off date
		$cutOffDate = $oldEventDate." 00:00:00";
		
		##### EBOOKING ######
		if($plugin['eBooking'])
		{
			$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = $repeatID AND EventDate >= '$cutOffDate'";
			$arrTargetEventID = $iCal->returnVector($sql);
			
			if(sizeof($arrTargetEventID) > 0)
			{
				$strTargetEventID = implode(",",$arrTargetEventID);
				
				$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($strTargetEventID)";
				$arrBookingID = $iCal->returnVector($sql);
				
				if(sizeof($arrBookingID) > 0)
				{
					$targetBookingID = implode(",",$arrBookingID);
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
					$iCal->db_db_query($sql);
						
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
					$iCal->db_db_query($sql);
					
//					$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
//					$iCal->db_db_query($sql);
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetBookingID)";
					$iCal->db_db_query($sql);
				}
			}
		}
		
		# Check any record in School Calendar
		$sql = "SELECT
					relation.SchoolEventID 
				FROM 
					CALENDAR_EVENT_SCHOOL_EVENT_RELATION as relation 
				WHERE 
					relation.CalendarEventID IN (
													SELECT 
														cal.EventID 
													FROM 
														CALENDAR_EVENT_ENTRY as cal 
													WHERE cal.RepeatID = $repeatID AND cal.EventDate >= '$cutOffDate')";
		$arrSchoolEventID = $iCal->returnVector($sql);
		
		# delete related event record in school calendar
		if(sizeof($arrSchoolEventID) >0){
			$targetSchoolEventID = implode(",",$arrSchoolEventID);
			$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$targetSchoolEventID.")";
			$iCal->db_db_query($sql);
			$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$targetSchoolEventID.")";
			$iCal->db_db_query($sql);
			$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN (".$targetSchoolEventID.")";
			$iCal->db_db_query($sql);
		}
		
		# delete this and following events from the original recurrence series
		$result['delete_repeat_events_fol'] = $iCal->deleteRepeatFollowingEvents($cutOffDate, $repeatID);
	}
} else { # non-repeat event OR repeat event but delete only this instance
	# Check any record in School Calendar
	$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN ($eventID)";
	$arrSchoolEventID = $iCal->returnVector($sql);
	# delete related event record in school calendar
	if(sizeof($arrSchoolEventID) >0){
		$targetSchoolEventID = implode(",",$arrSchoolEventID);
		$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$targetSchoolEventID.")";
		$iCal->db_db_query($sql);
		$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$targetSchoolEventID.")";
		$iCal->db_db_query($sql);
		$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN (".$targetSchoolEventID.")";
		$iCal->db_db_query($sql);
	}

	# delete main detail
	$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = $eventID";
	$result['delete_event_entry'] = $iCal->db_db_query($sql);
	
	# delete Personal Note
	$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = $eventID";
	$result['delete_event_personal_note'] = $iCal->db_db_query($sql);
	
	# delete reminders
	$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID = $eventID";
	$result['delete_event_reminder'] = $iCal->db_db_query($sql);
	
	# delete guests
	$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID = $eventID";
	$result['delete_event_guest'] = $iCal->db_db_query($sql);
	
	##### EBOOKING #####
	if($plugin['eBooking'])
	{
		$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($eventID)";
		$arrBookingID = $iCal->returnVector($sql);
		
		if(sizeof($arrBookingID) > 0)
		{
			$targetBookingID = implode(",",$arrBookingID);
			
			$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
			$iCal->db_db_query($sql);
				
			$sql = "UPDATE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
			$iCal->db_db_query($sql);
			
//			$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
//			$iCal->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetBookingID)";
			$iCal->db_db_query($sql);
		}
	}
}

if(!in_array(false, $result)){
	$Msg = $Lang['ReturnMsg']['EventDeleteSuccess'];	
} else {
	$Msg = $Lang['ReturnMsg']['EventDeleteUnSuccess'];
}

intranet_closedb();
header("Location: index.php?calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&time=".$_SESSION["iCalDisplayTime"]."&msgType=event&Msg=$Msg");
?>