<?php
// page modifing by: 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
intranet_auth();
intranet_opendb();

# Class Library
$iCal = new icalendar();
$lui = new ESF_ui();

###############################################################################################################

# Get Post Data
$eventDate = (isset($eventDate) && $eventDate != "") ? $eventDate : "";

# Initialization
$output = '';


# Perparation
if($eventDate != ""){
	# Get Color Selection 
	$ColorSelectionArr = $iCal->Get_Color_Selection();

	# Get Start & End Date
	$eventDate	   = date("Y-m-d", strtotime($eventDate));		# Input Format: Y-n-j -> Output Format: Y-m-d
	$startDateTime = $eventDate.' 00:00:00';
	$ts			   = strtotime($startDateTime);
	$endDate	   = date("Y-m-d", $ts + (24*60*60));
	$endDateTime   = $endDate.' 00:00:00';
	$date		   = date("j", $ts);
	$endTs		   = strtotime($endDateTime);

	$ts += $iCal->offsetTimeStamp;
	$endTs += $iCal->offsetTimeStamp;

	# new apporach
	/*
	# get user related events excluding Involved Events
	$ownEventList = $iCal->Get_Viewable_Event_Entry($startDateTime, $endDateTime, false, false, true);

	# get user related involved events only
	$otherEventList = $iCal->Get_Viewable_Involved_Event_Entry($startDateTime, $endDateTime, true);

	# get user related foundation events only
	$foundationEventList = $iCal->Get_Viewable_Foundation_Event_Entry($startDateTime, $endDateTime, true);
	
	$totalEventList = array_merge($ownEventList, $otherEventList);
	$totalEventList = array_merge($totalEventList, $foundationEventList);

	$EventList = $iCal->Return_Events_List_in_Associated_Array($totalEventList);
	ksort($EventList);
	*/

	# Modified on 5 Mar 2009
	$EventList = $iCal->Get_All_Related_Event($ts, $endTs, true);
	
}

# Main
if($eventDate != ""){
	# UI - Start
	$output .= '<span>';
	$output .= '<a href="javascript:jHide_Event_Entry_List()" class="day_close">X</a><a class="day_st"> | </a>';
	$output .= '<a href="javascript:document.form1.time.value=\''.$ts.'\';changePeriodTab(\'daily\');" class="day_no">'.$date.'</a>';
	$output .= '<br style="clear:both">';
	$output .= '</span>';
	$output .= '<div id="event_entry_list_detail" class="cal_month_event_item" style="width:180px">';

	# new approach of displaying user relater calendar events & involved events by once 
	$output .= $iCal->Display_More_Event_Entry_List_By_EventDateTime($EventList, $eventDate);

	/*
	# old approach of displaying user relater calendar events & involved events
	# User related Calendar Events
	$output .= $iCal->Display_More_Event_Entry_List($ownEventList);
	
	# User Calendar Involved Events
	$output .= $iCal->Display_More_Event_Entry_List($otherEventList, true);
	*/

	# UI - End
	$output .= '</div>';

} else {
	$output .= '<span>';
	$output .= '<a href="javascript:jHide_Event_Entry_List()" class="day_close">X</a>';
	$output .= '<br style="clear:both">';
	$output .= '</span>';
	$output .= '<div id="event_entry_list_detail" class="cal_month_event_item" style="width:180px">';
	$output .= "No event reord is found.";
	$output .= '</div>';
}



###############################################################################################################
//echo iconv("BIG5", "UTF-8", $output);
echo $output;
?>