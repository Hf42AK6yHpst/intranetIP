<?php
// page modifing by: 
/*
 * 2012-08-29 (Carlos): Outlook cannot read UTF-16LE, so removed code to convert from UTF-8 to UTF-16LE and add BOM header, just output as UTF-8 content					
 */
$PATH_WRT_ROOT = "../../";
$CurSubFunction = "iCalendar";


// include_once($PathRelative."src/include/class/startup.php");
// include_once($PathRelative."src/include/class/database.php");
include_once($PATH_WRT_ROOT."includes/global.php");
// include_once("file_system.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
// if (empty($_SESSION['SSV_USERID'])) {
	// header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	// exit;
// }

$ExportStr = "";
$filename = "";
$exportType = trim($exportType);
intranet_auth();
intranet_opendb();


if ($exportType == "csv"){
	

	// $fs = new file_system();

	# column header
	$FileFieldArr = array();
	$FileFieldArr[0] = "Name";
	$FileFieldArr[1] = "Title";
	$FileFieldArr[2] = "Start Date";
	$FileFieldArr[3] = "Start Time";
	$FileFieldArr[4] = "End Date";
	$FileFieldArr[5] = "End Time";
	$FileFieldArr[6] = "All Day Event";
	$FileFieldArr[7] = "Location";
	$FileFieldArr[8] = "Description";
	$FileFieldArr[9] = "url";

	$FieldList = $FileFieldArr[0];
	for($i = 1; $i < count($FileFieldArr); $i++) {
		$FieldList .= "	".$FileFieldArr[$i];
	}

	//if($calType == 0 || $calType == 1) {
	if ($calType == 1 || $calType>3){
		$iCal_api = new icalendar_api();
		$result = $iCal_api->getFormattedExternalExportEvent($calType,$calID);
	}
	else{
		$sql = "Select 
					b.Name, a.Title, 
					DATE_FORMAT(a.EventDate,'%Y-%m-%d') as StartDate,
					DATE_FORMAT(a.EventDate,'%T') AS StartTime,
				Case When a.IsAllDay = 1 and a.Duration <> 0
				Then 	DATE_FORMAT(TIMESTAMPADD(MINUTE,(a.Duration-1440),a.EventDate),'%Y-%m-%d')	
				Else				DATE_FORMAT(TIMESTAMPADD(MINUTE,a.Duration,a.EventDate),'%Y-%m-%d') 
				End as EndDate,					DATE_FORMAT(TIMESTAMPADD(MINUTE,a.Duration,a.EventDate),'%T') AS EndTime,
					CASE 
						WHEN a.IsAllDay = 1 THEN 'Yes' 
						ELSE 'No' 
					END AS AllDayEvent,
					a.Location, a.Description, a.url
				FROM CALENDAR_EVENT_ENTRY AS a
				INNER JOIN CALENDAR_CALENDAR AS b ON b.CalID = a.CalID
				INNER JOIN CALENDAR_CALENDAR_VIEWER as c on b.CalID = c.CalID
				WHERE 
					b.CalID = ".$calID." 
				AND c.UserID = ".$UserID."
				ORDER By EventDate, Duration
				";
		$db = new libdb();
		$result = $db->returnArray($sql);
	}
		
		
		// echo $sql;
		// debug_r($result);
		// exit;
	// }
	// elseif($calType == 2) {	
		
		// $local_db = $SYS_CONFIG['DB']['DBName'];		
		
		// $sql = "Select 
					// b.Name, a.Title, 
					// Convert(char(10), a.EventDate, 111) as 'StartDate', 
					// Convert(char(5), a.EventDate, 114) AS 'StartTime',
					// Convert(char(10), DateAdd(minute, a.Duration, a.EventDate), 111) as 'EndDate',
					// Convert(char(5), DateAdd(minute, a.Duration, a.EventDate), 114) as 'EndTime',
					// CASE 
						// WHEN a.IsAllDay = 1 THEN 'Yes' 
						// ELSE 'No' 
					// END AS 'AllDayEvent',
					// a.Location, a.Description, a.url
				// FROM ".$SYS_CONFIG['ESF']['DB']['DBName'].".dbo.CALENDAR_EVENT_ENTRY AS a
				// INNER JOIN ".$SYS_CONFIG['ESF']['DB']['DBName'].".dbo.CALENDAR_CALENDAR AS b ON b.CalID = a.CalID
				// WHERE b.CalID = ".$calID." 
				// AND (
						// CalSharedType = 'A' 
					// OR 
						// (	CalSharedType = 'T' 
						// AND 
							// Exists(
								// SELECT 1 
								// FROM 
									// ".$local_db.".dbo.INTRANET_USER 
								// WHERE UserID = ".$_SESSION["SSV_USERID"]." 
								// AND UserType = 'T'
								// )
						// ) 
					// OR
						// (	CalSharedType = 'T' 
						// AND 
							// Exists(
								// SELECT 1 
								// FROM 
									// ".$local_db.".dbo.INTRANET_USER 
								// WHERE UserID = ".$_SESSION["SSV_USERID"]." 
								// AND UserType = 'S'
								// )
						// )
					// )
				// ORDER By EventDate, Duration
				// ";
			
			// $db = new database(false, false, true);
			// $result = $db->returnArray($sql);		
	// }

	$ExportStr = $FieldList."\n";

	for($i = 0; $i < count($result); $i++) {
		$Name = '"'.$result[$i]["Name"].'"';
		$Title = '"'.$result[$i]["Title"].'"';
		$StartDate = '"'.$result[$i]["StartDate"].'"';
		$StartTime = '"'.$result[$i]["StartTime"].'"';
		$EndDate = '"'.$result[$i]["EndDate"].'"';
		$EndTime = '"'.$result[$i]["EndTime"].'"';
		$AllDayEvent = '"'.$result[$i]["AllDayEvent"].'"';
		$Location = '"'.$result[$i]["Location"].'"';
		$Description = '"'.$result[$i]["Description"].'"';
		$url = '"'.$result[$i]["url"].'"';
			
		$ExportRow = "";
		$ExportRow .= "$Name	$Title	$StartDate	$StartTime	$EndDate	";
		$ExportRow .= "$EndTime	$AllDayEvent	$Location	$Description	$url,";	
		$ExportStr .= $ExportRow."\n";	
	}

	$filename = "calendar.csv";
} 
elseif ($exportType == "ical"){
	$ical = new icalendar();
	global $schoolNameAbbrev;
	
	//last check for UID such that it is not null
	$sql = "select RepeatID, min(EventID) as minEvent, min(CalID) as minCal, min(UserID) as minUser ";
	$sql .= "from CALENDAR_EVENT_ENTRY ";
	$sql .= "where RepeatID is not null and UID is null group by RepeatID";
	$resultSet = $ical->returnArray($sql);
	foreach ($resultSet as $result){
		$sql = "update CALENDAR_EVENT_ENTRY SET ";
		$sql .= "UID = '".$result["minUser"]."-".$result["minCal"]."-".$result["minEvent"]."@{$schoolNameAbbrev}.tg' ";
		$sql .= "where RepeatID = '".$result["RepeatID"]."'";
		$ical->db_db_query($sql);
	}
		
	$sql = "select EventID, CalID, UserID ";
	$sql .= "from CALENDAR_EVENT_ENTRY ";
	$sql .= "where UID is null and RepeatID is null";
	$resultSet = $ical->returnArray($sql);
	foreach ($resultSet as $result){
		$sql = "update CALENDAR_EVENT_ENTRY SET ";
		$sql .= "UID = '".$result["UserID"]."-".$result["CalID"]."-".$result["EventID"]."@{$schoolNameAbbrev}.tg' ";
		$sql .= "where EventID = '".$result["EventID"]."'";
		$ical->db_db_query($sql);
	}
	
	//echo $calID.' '.$calType;
	//exit;
	//****************************
	$ExportStr = $ical->db_to_icalStmt($calID, $calType);
	
	$filename = "calendar.ics";
}

intranet_closedb();

$content =  $ExportStr;

//debug_r(mb_list_encodings());
////$data = mb_convert_encoding($content,'UTF-16LE','UTF-8');
// Add BOM (Byte Order Mark) to identify the file is in UTF-16LE encoding
////$data = "\xFF\xFE".$data;
$data = $content;

// BOM header for UTF-8
$data = "\xEF\xBB\xBF".$data;
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

header('Content-type: application/octet-stream');
header('Content-Length: '.strlen($data));

header('Content-Disposition: attachment; filename="'.$filename.'";');

print $data;


//Output_To_Browser($content, $filename);
/*
$content_type = "application/octet-stream";

 while (@ob_end_clean());
 header("Pragma: public");
 header("Expires: 0"); // set expiration time
 header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
 header("Content-type: $content_type; charset:utf8");
 header("Content-Length: ".strlen($content));
 header("Content-Disposition: attachment; filename=\"".$filename."\"");
 echo $content; 
*/
?>