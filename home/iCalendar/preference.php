<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libicalendar.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$iCal = new libicalendar;
$linterface = new interface_html();

$MODULE_OBJ["title"] = $iCalendar_Main_Title;
$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eAttendance.gif";

$customLeftMenu = $iCal->printLeftMenu();
$topNavigationMenu = $iCal->printTopNavigationMenu($iCalendar_ToolLink_Preference);

## select default calendar view
$array_view_name = array(ucfirst($iCalendar_NewEvent_DurationMonth), ucfirst($iCalendar_NewEvent_DurationWeek), ucfirst($iCalendar_NewEvent_DurationDay));
$array_view_data = array("monthly", "weekly", "daily");
$view_select = getSelectByValueDiffName($array_view_data,$array_view_name,"name='view'",$view,0,1);

$TAGS_OBJ[] = array("<div style='margin:43px 0px -0px 10px;'>$topNavigationMenu</div>", "", 0);
?>
<link rel="stylesheet" href="css/agenda.css" type="text/css" />
<link rel="stylesheet" href="css/main.css" type="text/css" />
<?php
$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/interface.js"></script>
<script type="text/javascript" src="js/dimensions.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script language="javascript">
function validateForm() {
	var v = $("#quickAddEvent_form").validate({
		rules: {
			title: "required",
			eventDate: {
				required: true,
				yyyymmdd: true,
				minLength: 8,
				maxLength: 10
			}
		},
		messages: {
			title: "&nbsp;*<?=$iCalendar_QuickAdd_Required?>",
			eventDate: {
				required: "&nbsp;*<?=$iCalendar_QuickAdd_Required?>",
				minLength: "&nbsp;*<?=$iCalendar_QuickAdd_InvalidDate?>",
				maxLength: "&nbsp;*<?=$iCalendar_QuickAdd_InvalidDate?>"
			}
		}
	});
	
	if(v.form()){
		$("#quickAddEvent").fadeOut();
		popUpCal.hideCalendar(popUpCal._curInst, '');
		var blockMsg = "<h2 style='padding-top:10px;'><?=$i_campusmail_processing?></h2>";
		$('div#content').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		isBlocking = true;
		$("#quickAddEvent_form").ajaxSubmit({
			url: "new_event_update.php",
			success: function() {
				$("#quickAddEvent_form").resetForm();
				getAgendaDisplay();
			}
		});
	} else {
		v.focusInvalid();
		return false;
	}
}

$(document).ready( function() {
	
});

function toggleInvolveEventVisibility(){
	$(".involve").toggle();
}

function toggleSchoolEventVisibility() {
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
	resetTableRowClass();
}

function toggleCalVisibility(parClassID) {
	$('.cal-'+parClassID).toggle();
	resetTableRowClass();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: parClassID
	}, function() {
	    // callback function
	});
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>
<br />
<form name="form1" id="form1" action="agenda.php" method="POST">
<div id="wrapper" style="padding-left:10px;">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?= $iCalendar_Preference_View ?>
		</td>
		<td width="70%" class="tabletext">
			<?=$view_select?>
		</td>
	</tr>
</table>
</div>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
		</td>
	</tr>
</table>
</form>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>