<?php
// using : 
/*
 * Modification Log:
 * 2016-04-25 Carlos 
 *  - cater eBooking MONTHLY repeat event by dayOfWeek to follow iCalendar rule. 
 *    Wrong way e.g. iCalendar treat last Friday of a month count from the end of month in reverse way, while eBooking count from the start of month. The 4th Friday of a month is not must the last Friday of a month, it could be the 5th Friday of a month. 
 * 2013-11-15 Ivan [2013-1024-1127-39156]
 * 	- added user pending record checking ************ if upload this file before ip.2.5.4.11.1.0, please upload libebooking.php also
 */
$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();
$iCal = new icalendar();

if($AllDayEvent == 1) {
	$StartTime = date("H:i:s", mktime(0,0,0,0,0,0));
	$EndTime = date("H:i:s", mktime(23,55,0,0,0,0));
} else {
	$StartTime = date("H:i:s", mktime($StartHour,$StartMins,0,0,0,0));
	$EndTime = date("H:i:s", mktime($EndHour,$EndMins,0,0,0,0));
}


switch ($RepeatSelect){
	case "NOT":
		$targetDate = $StartDate;
		
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $targetDate, $targetDate, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
		$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		
		if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
		{
			### Available
			
			$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
			$_numOfPendingRecord = count((array)$_pendingRecordAry);
			$_havePendingRecord = false;
			for ($i=0; $i<$_numOfPendingRecord; $i++) {
				$_pendingRecordStartTime = $_pendingRecordAry[$i]['StartTime'];
				$_pendingRecordEndTime = $_pendingRecordAry[$i]['EndTime'];
				
				if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
					$_havePendingRecord = true;
					break;
				}
			}
			
			if ($_havePendingRecord) {
				// do nth
			}
			else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
			{
				$tempArr[0]["StartTime"] = $StartTime;
				$tempArr[0]["EndTime"] = $EndTime;
				$DateTimeArr[$targetDate][] = $tempArr[0];
			}
		}
		break;
		
	case "DAILY":
		//if($StartDate > $RepeatStart)
		//{
		//	$RepeatStart = $StartDate;
		//}
		
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
		$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		
		$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / ($RepeatTimes*86400), 0 );
		for($i=0; $i<=$NumOfDay; $i++)
		{
			$DaysAfter = $i * $RepeatTimes;
			$ts_RepeatStartDate = strtotime($RepeatStart);
			$targetDate = $ts_RepeatStartDate + ($DaysAfter * 86400);		## 86400 is UNIX Timestamp for 1 day
			$targetDate = date("Y-m-d",$targetDate);
			
			if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
			{
				$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
				$_numOfPendingRecord = count((array)$_pendingRecordAry);
				$_havePendingRecord = false;
				for ($j=0; $j<$_numOfPendingRecord; $j++) {
					$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
					$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
					
					if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
						$_havePendingRecord = true;
						break;
					}
				}
				
				if ($_havePendingRecord) {
					// do nth
				}
				else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
				{
					$tempArr[0]["StartTime"] = $StartTime;
					$tempArr[0]["EndTime"] = $EndTime;
					$DateTimeArr[$targetDate][] = $tempArr[0];
				}
			}
		}
		break;
		
	case "WEEKDAY":
		//if($StartDate > $RepeatStart)
		//{
		//	$RepeatStart = $StartDate;
		//}
		
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
		$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
		$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / (1 * 86400), 0 );
		for($i=0; $i<=$NumOfDay; $i++)
		{
			$ts_RepeatStartDate = strtotime($RepeatStart);
			$targetDate = $ts_RepeatStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day

			if((date("w",$targetDate) != "0") && (date("w",$targetDate) != "6"))
			{
				$targetDate = date("Y-m-d",$targetDate);
				if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
				{
					$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
					$_numOfPendingRecord = count((array)$_pendingRecordAry);
					$_havePendingRecord = false;
					for ($j=0; $j<$_numOfPendingRecord; $j++) {
						$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
						$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
						
						if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
							$_havePendingRecord = true;
							break;
						}
					}
					
					if ($_havePendingRecord) {
						// do nth 
					}
					else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
					{
						$tempArr[0]["StartTime"] = $StartTime;
						$tempArr[0]["EndTime"] = $EndTime;
						$DateTimeArr[$targetDate][] = $tempArr[0];
					}
				}
			}
		}
		break;
		
	case "MONWEDFRI":
		//if($StartDate > $RepeatStart)
		//{
		//	$RepeatStart = $StartDate;
		//}
		
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
		$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
		$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / (1 * 86400), 0 );
		for($i=0; $i<=$NumOfDay; $i++)
		{
			$ts_RepeatStartDate = strtotime($RepeatStart);
			$targetDate = $ts_RepeatStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
			if((date("w",$targetDate) == "1") || (date("w",$targetDate) == "3") || (date("w",$targetDate) == "5"))
			{
				$targetDate = date("Y-m-d",$targetDate);
				if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
				{
					$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
					$_numOfPendingRecord = count((array)$_pendingRecordAry);
					$_havePendingRecord = false;
					for ($j=0; $j<$_numOfPendingRecord; $j++) {
						$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
						$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
						
						if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
							$_havePendingRecord = true;
							break;
						}
					}
					
					if ($_havePendingRecord) {
						// do nth
					}
					else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
					{
						$tempArr[0]["StartTime"] = $StartTime;
						$tempArr[0]["EndTime"] = $EndTime;
						$DateTimeArr[$targetDate][] = $tempArr[0];
					}
				}
			}
		}
		break;
		
	case "TUESTHUR":
		//if($StartDate > $RepeatStart)
		//{
		//	$RepeatStart = $StartDate;
		//}
		
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
		$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
		$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / (1 * 86400), 0 );
		for($i=0; $i<=$NumOfDay; $i++)
		{
			$ts_RepeatStartDate = strtotime($RepeatStart);
			$targetDate = $ts_RepeatStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
			if((date("w",$targetDate) == "2") || (date("w",$targetDate) == "4"))
			{
				$targetDate = date("Y-m-d",$targetDate);
				
				if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
				{
					$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
					$_numOfPendingRecord = count((array)$_pendingRecordAry);
					$_havePendingRecord = false;
					for ($j=0; $j<$_numOfPendingRecord; $j++) {
						$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
						$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
						
						if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
							$_havePendingRecord = true;
							break;
						}
					}
					
					if ($_havePendingRecord) {
						// do nth
					}
					else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
					{
						$tempArr[0]["StartTime"] = $StartTime;
						$tempArr[0]["EndTime"] = $EndTime;
						$DateTimeArr[$targetDate][] = $tempArr[0];
					}
				}
			}
		}
		break;
		
	case "WEEKLY":
		//if($StartDate > $RepeatStart)
		//{
		//	$RepeatStart = $StartDate;
		//}
		
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
		$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		
		$arrRepeatValue = explode(",",$RepeatValue);
		$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / (1 * 86400), 0 );
		
		for($i=0; $i<=$NumOfDay; $i++)
		{
			if($i%($RepeatTimes*7) < 7)
			{
				$ts_RepeatStartDate = strtotime($RepeatStart);
				$targetDate = $ts_RepeatStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
				$targetDate = date("Y-m-d",$targetDate);
				
				if( in_array(date("w",strtotime($targetDate)),$arrRepeatValue) )
				{
					if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
					{
						$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
						$_numOfPendingRecord = count((array)$_pendingRecordAry);
						$_havePendingRecord = false;
						for ($j=0; $j<$_numOfPendingRecord; $j++) {
							$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
							$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
							
							if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
								$_havePendingRecord = true;
								break;
							}
						}
						
						if ($_havePendingRecord) {
							// do nth
						}
						else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
						{
							$tempArr[0]["StartTime"] = $StartTime;
							$tempArr[0]["EndTime"] = $EndTime;
							$DateTimeArr[$targetDate][] = $tempArr[0];
						}
					}
				}
			}
		}
		break;
		
	case "MONTHLY":
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
		$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			
		if($RepeatValue == 'dayOfWeek') {
			
			$datePieces = explode("-",$RepeatStart);
			$eventDateWeekday = date("w", strtotime($RepeatStart));
			$nth = 0;
			for($i=1; $i<6; $i++) {
				$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
				if ((int)$datePieces[2] == (int)$nthweekday) {
					$nth = $i;
					break;
				} 
			}
			
			if (date("m",strtotime("+7 day",strtotime($RepeatStart)))!=$datePieces[1])
				$nth = -1;
			
			$weekName = Array("SU","MO","TU","WE","TH","FR","SA");
			$weekday = date("w",strtotime($RepeatStart));
			$rule = array('interval'=>$RepeatTimes,'freq'=>"MONTHLY","byDay"=>''.$nth.$weekName[$weekday],'until'=>$RepeatEnd);
			$allDays = $iCal->get_all_repeated_day($iCal->toUTC($RepeatStart),$iCal->toUTC($RepeatEnd),$rule);
			
			
			/*
			$ts_start_time = strtotime($RepeatStart);
			$ts_end_time = strtotime($RepeatEnd);
			
			$RepeatValue = date("w",strtotime($RepeatStart));
			$repeat_frequency = date("n",$ts_end_time - $ts_start_time);
			
			$week_num = ceil( date('j', $ts_start_time) / 7 );
			
			for($i=0; $i<$repeat_frequency; $i++)
			{
				$repeat_val = $i*$RepeatTimes;

				$total_num_of_month = date("t",mktime(0,0,0,date("m",$ts_start_time)+$repeat_val,0,date("Y",$ts_start_time)));
				
				for($j=1; $j<=$total_num_of_month; $j++)
				{
					$curr_date = date("Y-m-d",mktime(0,0,0,date("m",$ts_start_time)+$repeat_val,$j,date("Y",$ts_start_time)));
					$ts_curr_timestamp = strtotime($curr_date);
					
					if(ceil( date('j', $ts_curr_timestamp) / 7 ) == $week_num)
					{
						if((date("w",$ts_curr_timestamp) == $RepeatValue) && ($ts_curr_timestamp <= $ts_end_time))
						{
							$targetDate = date("Y-m-d",$ts_curr_timestamp);
							*/
						for($i=0;$i<count($allDays);$i++)
						{	
							$targetDate = $allDays[$i];
							if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
							{
								$__pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
								$__numOfPendingRecord = count((array)$__pendingRecordAry);
								$__havePendingRecord = false;
								for ($k=0; $k<$__numOfPendingRecord; $k++) {
									$___pendingRecordStartTime = $__pendingRecordAry[$k]['StartTime'];
									$___pendingRecordEndTime = $__pendingRecordAry[$k]['EndTime'];
									
									if ($___pendingRecordStartTime==$StartTime && $___pendingRecordEndTime==$EndTime) {
										$__havePendingRecord = true;
										break;
									}
								}
								
								if ($__havePendingRecord) {
									// do nth
								}
								else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
								{
									$tempArr[0]["StartTime"] = $StartTime;
									$tempArr[0]["EndTime"] = $EndTime;
									$DateTimeArr[$targetDate][] = $tempArr[0];
								}
							}
						}
			/*
						}
					}
				}
			}
			*/
		} else {
			//if($StartDate > $RepeatStart)
			//{
			//	$RepeatStart = $StartDate;
			//}
			
			$ts_start_time = strtotime($RepeatStart);
			$ts_end_time = strtotime($RepeatEnd);
			
			$RepeatValue = date("d",$ts_start_time);
			$repeat_frequency = date("n",$ts_end_time - $ts_start_time);
			
			for($i=0; $i<$repeat_frequency; $i++)
			{
				$repeat_val = $i*$RepeatTimes;
				$ts_curr_timestamp = mktime(0,0,0,date("m",$ts_start_time)+$repeat_val,$RepeatValue,date("Y",$ts_start_time));
				
				if(($RepeatValue == date("d",$ts_curr_timestamp)) && ($ts_curr_timestamp <= $ts_end_time))
				{
					$targetDate = date("Y-m-d",$ts_curr_timestamp);
					if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
					{
						$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
						$_numOfPendingRecord = count((array)$_pendingRecordAry);
						$_havePendingRecord = false;
						for ($j=0; $j<$_numOfPendingRecord; $j++) {
							$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
							$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
							
							if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
								$_havePendingRecord = true;
								break;
							}
						}
						
						if ($_havePendingRecord) {
							// do nth
						}
						else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
						{
							$tempArr[0]["StartTime"] = $StartTime;
							$tempArr[0]["EndTime"] = $EndTime;
							$DateTimeArr[$targetDate][] = $tempArr[0];
						}
					}
				}
			}
		}
		break;
		
	case "YEARLY":
		//if($StartDate > $RepeatStart)
		//{
		//	$RepeatStart = $StartDate;
		//}
		
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
		$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		
		
		$ts_start_time = strtotime($RepeatStart);
		$ts_end_time = strtotime($RepeatEnd);
		$repeat_frequency = date("n",$ts_end_time - $ts_start_time);
		
		for($i=0; $i<$repeat_frequency; $i++)
		{
			$repeat_val = $i*$RepeatValue;
			$ts_curr_timestamp = mktime(0,0,0,date("m",$ts_start_time),date("d",$ts_start_time),date("Y",$ts_start_time)+$repeat_val);
			if($ts_curr_timestamp <= $ts_end_time)
			{
				$targetDate = date("Y-m-d",$ts_curr_timestamp);
				if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
				{
					$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
					$_numOfPendingRecord = count((array)$_pendingRecordAry);
					$_havePendingRecord = false;
					for ($j=0; $j<$_numOfPendingRecord; $j++) {
						$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
						$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
						
						if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
							$_havePendingRecord = true;
							break;
						}
					}
					
					if ($_havePendingRecord) {
						// do nth
					}
					else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
					{
						$tempArr[0]["StartTime"] = $StartTime;
						$tempArr[0]["EndTime"] = $EndTime;
						$DateTimeArr[$targetDate][] = $tempArr[0];
					}
				}
			}
		}
		break;
}
	
//$tempArr[0]["StartTime"] = $StartTime;
//$tempArr[0]["EndTime"] = $EndTime;
//$DateTimeArr[$StartDate][] = $tempArr[0];

if($SingleItemID != "") {
	$SingleItemIDArr = explode(",",$SingleItemID);
} else {
	$SingleItemIDArr = array();
}

$BookingRemarks = "";

$ResponsibleUID = $_SESSION['UserID'];

//echo $RepeatSelect." - ".$RepeatTimes." - ".$RepeatValue." - ".$RepeatStart." - ".$RepeatEnd;
//debug_r($DateTimeArr);


$SettingsArr['CancelDayBookingIfOneItemIsNA'] = $CancelDayBookingIfOneItemIsNA;

$result = $lebooking_ui->Create_Temp_Booking_Record_iCal($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr, $BookingRemarks, $ResponsibleUID, $SettingsArr);

intranet_closedb();
?>