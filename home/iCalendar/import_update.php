<?php
// page modifing by: 
/*
 * 2019-06-06 Carlos: Added CSRF token checking.
 */
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

if(!verifyCsrfToken($csrf_token, $csrf_token_key)){
	header("Location: /");
	exit;
}

intranet_auth();
intranet_opendb();

##################################################################################################################

## Use Library
$iCal = new icalendar();

## Get Data 
$userID = $_SESSION['UserID'];

## Initialization
$result_1 = array();

## Preparation
$sql = "SELECT * FROM TEMP_CALENDAR_EVENT_ENTRY WHERE SID='".session_id()."'";
$result = $iCal->returnArray($sql);


## Main
if ($confirm == 1 && count($result) > 0) {
	$access = "'D'";
	
	$fieldname  = "UserID, EventDate, InputDate, ModifiedDate, Duration, ";
	$fieldname .=	"IsAllDay, Access, Title, Description, Location, CalID";
	for($i=0; $i<sizeof($result); $i++) {
		$eventDate = "'".$result[$i]["EventDate"]."'";
		if ($result[$i]["IsAllDay"])
			$duration = "'".($result[$i]["Duration"]+1440)."'";
		else 
			$duration = "'".$result[$i]["Duration"]."'";
		$isAllDay = "'".$result[$i]["IsAllDay"]."'";
		$title = "'".trim($result[$i]["Title"])."'";
		$description = "'".trim($result[$i]["Description"])."'";
		$location = "'".trim($result[$i]["Location"])."'";
		$access = ($result[$i]["Private"]) ? "'R'" : "'P'";
		
		$fieldvalue = "$userID, $eventDate, NOW(), NOW(), $duration, 
						   $isAllDay, $access, $title, $description, $location, $calID";
		$sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
		$result_1['insert_event_entry_'.$i] = $iCal->db_db_query($sql);
		$eventID = $iCal->db_insert_id();
		
		$sql  = "INSERT INTO CALENDAR_EVENT_USER (EventID, UserID, Status, Access) VALUES ";
		$sql .= "($eventID, $userID, 'W', 'A')";
		$result_1['insert_event_user_'.$i] = $iCal->db_db_query($sql);
		
	}
	$sql  = "DELETE FROM TEMP_CALENDAR_EVENT_ENTRY WHERE SID='".session_id()."'";
	$result_1['delete_tmp_event_entry'] = $iCal->db_db_query($sql);
		
} 

if(!in_array(false, $result_1)){
	$Msg = $Lang['ReturnMsg']['EventImportSuccess'];
} else {
	$Msg = $Lang['ReturnMsg']['EventImportUnSuccess'];
}


##################################################################################################################
intranet_closedb();
header ("Location: import.php?Msg=$Msg");

?>
