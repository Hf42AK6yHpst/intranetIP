<?php
// page modifing by: 
$PathRelative = "../../../";
$CurSubFunction = "iCalendar";
$CurPage = $_SERVER["REQUEST_URI"];
$SkipAutoRedirect = true;

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	//header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	echo ($SessionStatus == 2 ? $SYS_CONFIG['SystemMsg']['SessionKicked'] : $SYS_CONFIG['SystemMsg']['SessionExpired']);
	exit;
}

intranet_opendb();
include_once($PathRelative."src/include/class/database.php");
include_once($PathRelative."src/include/class/icalendar.php");
include_once($PathRelative."src/include/class/icalendar_api.php");

# Class Library
$iCal = new icalendar();
$lui = new UserInterface();
$iCalApi = new icalendar_api();

###############################################################################################################

# Get Post Data
$calType   	   = (isset($calType) && $calType != "") ? $calType : '';
$eventID	   = (isset($eventID) && $eventID != "") ? $eventID : "";
$calViewPeriod = (isset($calViewPeriod) && $calViewPeriod != "") ? $calViewPeriod : "monthly";
$isEventMore   = (isset($isEventMore) && $isEventMore != "") ? $isEventMore : false;


# Initialization
$output = '';
$heading = '';
$eventDate = '';
$repeatID = '';
$userAccess = '';
$link_text = $iCalendar_OtherCalType_ViewDetails;

## Preparation
$isOtherCalType = $iCal->Is_Other_Calendar_Type($calType);

# Get Detail
if($isOtherCalType)
{
	$db = new database(false, true);
	# module - cpd
	$programmeID = (isset($programmeID) && $programmeID != "") ? $programmeID : "";
	
	$params  = "editEventID=event$eventID&calViewPeriod=$calViewPeriod&CalType=$calType";
	$params .= "&programmeID=$programmeID";

	if($calType == ENROL_CPD_CALENDAR)		# CPD Enrolment Programme Detail
	{
		$result = $iCalApi->Get_CPD_Enrolment_Programme_Info($eventID, $programmeID);
		if(count($result) > 0){
			if($result[0]['IsMyProgramme'] == 1){
				$page = "staff_view_enrol_detail.php?ProgrammeID=$programmeID";
			} else {
				if($result[0]['IsExpriedEnrolDate']){
					$page = "staff_view_detail.php?ProgrammeID=$programmeID"; 
				} else {
					$TbRowID = "Tr$programmeID";
					$page  = "enrolment_index.php?SortField=ProgrammeCode&SortOrder=1&ddlPageNo=1";
					$page .= "&ProgrammeID=$programmeID#$TbRowID";
					$link_text = $iCalendar_OtherCalType_EnrolNow;
				}
			}
			$link = $PathRelative."src/module/cpd/$page";
		}
		
	} 
	else if($calType == ENROL_CAS_CALENDAR)			# CAS
	{
		//...
	}	
}


# Main - ToolTips UI
if(count($result) > 0){
	# Value Manipulation
	$date = date('Y-m-d', strtotime($result[0]['EventDate']));
	$isAllDay = (strtotime($date) == strtotime($result[0]['EventDate'])) ? 1 : 0;
	
	# Date & Time
	$eventTs = strtotime($result[0]['EventDate']);
	$eventDate = date("Y-m-d", $eventTs);

	$title = trim($result[0]['Title']);
	$description = trim($result[0]['Description']);
	$PersonalNote =  trim($result[0]['PersonalNote']);
	$duration = $result[0]["Duration"];
	
	# Get Event Start Time & End Time
	if($isAllDay == 0){
		$endTs = $eventTs + $duration * 60;
		
		if ($iCal->systemSettings["TimeFormat"] == 12) {
			$eventTime = date("g:ia", $eventTs);
			$endTime = date("g:ia", $endTs);
		} else if ($iCal->systemSettings["TimeFormat"] == 24) {
			$eventTime = date("G:i", $eventTs);
			$endTime = date("G:i", $endTs);
		}
	}
	
	if($PersonalNote != ""){
		$PersonalNote = (strlen($PersonalNote) > 22) ? substr($PersonalNote, 0, 22).' ...' : $PersonalNote;
	} else {
		$PersonalNote = "<a class=\"sub_layer_link\" href=\"javascript:window.location='new_event.php?$params'\">Add</a>";
	}
	
	
	## Main
	$output .= "<table class='eventToolTipTable' border='0' cellpadding='2' cellspacing='0' width='100%'>";
	# Content
	$output .= "<tr>";
	$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">Title:</td>";
	$output .= "<td>$title</td>";
	$output .= "</tr>";
	$output .= "<tr>";
	$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">Description:</td>";
	$output .= "<td>$description</td>";
	$output .= "</tr>";
	$output .= "<tr>";
	$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">Event Date:</td>";
	$output .= "<td>$eventDate</td>";
	$output .= "</tr>";
	if($isAllDay == 0){
		$output .= "<tr>";
		$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_NewEvent_EventTime:</td>";
		$output .= "<td>".$eventTime.($eventTime == $endTime ? "" : " - $endTime")."</td>";
		$output .= "</tr>";
	}
	$output .= "<tr>";
	$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">Link:</td>";
	$output .= "<td><a href=\"$link\" class=\"sub_layer_link\">".$link_text."</a></td>";
	$output .= "</tr>";
	
	// personal note
	$output .= "<tr>";
	$output .= "<td class=\"eventToolTipLabel\" valign=\"top\" width=\"100\">Personal Note:</td>";
	$output .= "<td>$PersonalNote</td>";
	$output .= "</tr>";

	# Button
	$output .= "<tr><td colspan='2' align='right'>";
	# Edit 
	$output .= "<a class=\"sub_layer_link\" href=\"javascript:window.location='new_event.php?$params'\">Edit</a>";
	$output .= "&nbsp;&nbsp;";
	# Close 
	$hide_fnt = ($isEventMore) ? "jHide_Event_More_Info()" : "jHide_Event_ToolTips()";
	$output .= "<a class=\"sub_layer_link\" href=\"javascript:$hide_fnt\">Close</a>";
	$output .= "</td>";
	$output .= "</tr>";
	$output .= "</table>";
} else {
	$output .= "No Event record is found.";
}


###############################################################################################################
//echo iconv("BIG5", "UTF-8", $output);
echo $output;
?>
