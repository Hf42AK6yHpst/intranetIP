<?php
// page modifing by: 
/*
 * 2019-11-05 Ray: Added convertDateTimeToStandardFormat for startDate & endDate
 * 2019-06-06 Carlos: Added CSRF token checking.
 */
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

if(!verifyCsrfToken($csrf_token, $csrf_token_key)){
	header("Location: /");
	exit;
}

intranet_auth();
intranet_opendb();

##################################################################################################################

## Use Library
$iCal = new icalendar();
$linterface = new interface_html();
$lfs = new libfilesystem();
$lui = new ESF_ui();
$limport = new libimporttext();
## Get Data 
if (!isset($time))
	$time = $_SESSION["iCalDisplayTime"];
	
# Get File Date
$filepath = $userfile;
$filename = $userfile_name;


## Initialization 
$data = array();
$core_header = array();
$error_entries = array();
$result = array();
$valuesArr = array();


## Preparation
$file_format = array("Subject","Start Date","Start Time","End Date","End Time","All Day Event","Location","Description");	//"Private"
$core_header = $file_format;

if($filepath=="none" || $filepath == ""){          # import failed
	$Msg = $iCalendar_Import_Failure;
	header("Location: import.php?Msg=$Msg");
	exit();
} else {
	# Check File Extension
	//if(strtoupper(str_replace(".", "", $lfs->file_ext($filename))) == "CSV"){
		# Get File Date
		//$data = $lfs->file_read_csv($filepath);
		$data = $limport->GET_IMPORT_TXT($userfile); 
		//debug_r($data);exit;
		# Check Header Validation
		if($lfs->CHECK_CSV_FILE_FORMAT($data[0], $core_header)){
			//$Msg = $iCalendar_Import_InvalidFormat;
			$Msg = $Lang['ReturnMsg']['FormatOfImportedFileNotValid'];
			header ("Location: import.php?Msg=$Msg");
			exit();
		}
		
		array_shift($data);
		$error = false;
		$errorSymbol = "<font color='red'><i>*</i></font>";
		$values = "";
		$delim = "";
		$cnt = 0;
		
		for ($i=0; $i<sizeof($data); $i++) {
			# check for empty line
			$test = trim(implode("",$data[$i])); 
			if (empty($test)) 	continue; // break the loop
			list($title, $startDate, $startTime, $endDate, $endTime, $isAllDay, $location, $description) = array_map("trim", $data[$i]);	# Hidden field - isPrivate
			$isPrivate = "false";

			$startDate = convertDateTimeToStandardFormat($startDate, false);
			if(!empty($endDate)) {
				$endDate = convertDateTimeToStandardFormat($endDate, false);
			}

			//$regexDate = "/^(?:0?[1-9]|[12]\d|3[01])\/(?:1[012]|0?[1-9])\/[12]\d{3}$/";
			
			$regexDate = "/^[12]\d{3}-(?:1[012]|0?[1-9])-(?:0?[1-9]|[12]\d|3[01])$/";
			$regexTime = "/^(?:2[0-3]|1[0-9]|0?[0-9])\:(?:[0-5][0-9])$/";
			//$regexTime = "/^(?:2[0-3]|1[0-9]|0?[1-9])\:(?:[0-5][0-9])$/";
			
			$isStartDateValid = preg_match($regexDate, $startDate);
			$isIsAllDayValid = empty($isAllDay) || (strtolower($isAllDay)=="true") || (strtolower($isAllDay)=="false");
			$isStartTimeValid = empty($startTime) || preg_match($regexTime, $startTime);
			$isEndDateValid = empty($endDate) || preg_match($regexDate, $endDate);
			$isEndTimeValid = empty($endTime) || preg_match($regexTime, $endTime);
			$isPrivateValid = true;
			//$isPrivateValid = !empty($isPrivate) && ((strtolower($isPrivate)=="true") || (strtolower($isPrivate)=="false"));		# hidden field - default: false

			if (!empty($title) && $isStartDateValid && $isStartTimeValid && $isEndDateValid && 
				$isEndTimeValid && $isIsAllDayValid && $isPrivateValid )
			{
				$title = htmlspecialchars(addslashes(trim($title)));
				$description = htmlspecialchars(addslashes(trim($description)));
				$location = htmlspecialchars(addslashes(trim($location)));
				// $title = iconv(mb_detect_encoding($title), 'UTF-8', $title); 
				 // $location = iconv(mb_detect_encoding($location), 'UTF-8', $location);
				// $description = iconv(mb_detect_encoding($description), 'UTF-8', $description);
				
				/*$startDatePiece = explode("/", $startDate);
				$sDay = $startDatePiece[0];
				$sMonth = $startDatePiece[1];
				$sYear  = $startDatePiece[2];
				$sDate = "$sYear-$sMonth-$sDay";*/
				$eventDate = $startDate;
				# Default Event Access
				$private = "false";
				//$private = strtolower($isPrivate);
				
				if (!empty($startTime)) {
					$startTimePiece = explode(":", $startTime);
					$sHour = $startTimePiece[0];
					$sMin = $startTimePiece[1];
					//$sTime = " $sHour:$sMin:00";
					$sTime = ($sHour == "0" && $sMin == "00") ? " 00:00:00" : " $sHour:$sMin:00";
				} else {
					$sTime = " 00:00:00";
				}
				
				$eventDate .= $sTime;
				
				if (!empty($endDate)) {
					/*$endDatePiece = explode("/", $endDate);
					$eDay = $endDatePiece[0];
					$eMonth = $endDatePiece[1];
					$eYear  = $endDatePiece[2];*/
					$eDate = $endDate;//"$eYear-$eMonth-$eDay";
				} else {
					$eDate = $sDate;
				}
				
				if (!empty($endTime)) {
					$endTimePiece = explode(":", $endTime);
					$eHour = $endTimePiece[0];
					$eMin = $endTimePiece[1];
					$eDate .= " $eHour:$eMin:00";
				} else {
					$eDate .= $sTime;
				}
				
				if (!empty($isAllDay)) {
					$isAllDay = (strtolower($isAllDay)=="true") ? 1:0;
				} else {
					$isAllDay = empty($startTime) ? 1:0;
				}
				
				if ($isAllDay) {
					$duration = 0;
				} else {
					$eventTs = $iCal->sqlDatetimeToTimestamp($eventDate);
					$endTs = $iCal->sqlDatetimeToTimestamp($eDate);
					$duration = ($endTs-$eventTs)/60;
				}
				
				if (!empty($isPrivate)) {
					$isPrivate = ($private=="true") ? 1:0;
				} 
				
				$values .= ($cnt > 0) ? ", " : "";
				$values .= "('".session_id()."', '$title', '$eventDate', NOW(), '$duration', '$isAllDay', '$location', '$description', '$isPrivate')";
				$valuesArr[] = array($title, $startDate, $startTime, $endDate, $endTime, $isAllDay, $location, $description, $isPrivate);
				$cnt++;
			} else {
				$error_entries[] = array(
									empty($title) ? $errorSymbol : $title, 
									$isStartDateValid ? $startDate : $errorSymbol.$startDate, 
									$isStartTimeValid ? $startTime : $errorSymbol.$startTime, 
									$isEndDateValid ? $endDate : $errorSymbol.$endDate, 
									$isEndTimeValid ? $endTime : $errorSymbol.$endTime, 
									$isIsAllDayValid ? $isAllDay : $errorSymbol.$isAllDay, 
									$location, 
									$description,
									$isPrivateValid ? $isPrivate : $errorSymbol.$isPrivate 
								);
			}
		}
		
		if(count($valuesArr) > 0){
			$fieldnames = "SID,Title,EventDate,ImportDate,Duration,IsAllDay,Location,Description,Private";
			$sql = "INSERT INTO TEMP_CALENDAR_EVENT_ENTRY ($fieldnames) VALUES $values";
			$iCal->db_db_query($sql);
			$result = $valuesArr;
		}
		
	/*} else {
		$Msg = $iCalendar_Import_FileTypeNotSupport;
		header ("Location: import.php?Msg=$Msg");
		exit();
	}*/
}

# Button
$button  = '<div id="form_btn">';
if(sizeof($result) > 0){
	$button .= $lui->Get_Input_Submit("Submit", "Submit", $button_submit, "class=\"button_main_act\"").'&nbsp;';
}

$button .= $lui->Get_Reset_Button("Cancel", "Cancel", $button_cancel, "class=\"button_main_act\" onclick=\"window.location='import.php?clear=1'\"").'&nbsp;';
$button .= '</div>';

	
# Main
$CalLabeling  = "<div id='sub_page_title' style='width:50%'>$i_Calendar_ImportCalendar</div>";
	
$CalMain = '
	<div id="cal_board">
	  <span class="cal_board_02"></span> 
	  <div class="cal_board_content">
	    <div id="calendar_wrapper" style="float:left;background-color:#dbedff">
	  	  <div id="cal_manage_calendar">';
	  	    
$CalMain .= "<div style='width: 95%; padding-left: 10px; padding-right: 10px;' id='cal_list_mycal'>\n";
$CalMain .= "<p class='cssform' style='text-align:right'>$i_Calendar_ImportCalendar_IncorrectValue</p>\n";
$CalMain .= "<h1 style='padding: 0px; font-size: 11px; font-weight: normal; margin-bottom: 0px;'>$i_Calendar_ImportEvent_InvalidRecords</h1>\n";
# Failure Record
$CalMain .= "<div id='cal_list_mycal'>\n";
$CalMain .= "<table id=\"calViewerTable\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n";
# Title
$CalMain .= "<tr class=\"tabletop\">\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"15%\">$i_Calendar_Subject</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"10%\">$i_Calendar_StartDate</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"9%\">$i_Calendar_StartTime</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"10%\">$i_Calendar_EndDate</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"9%\">$i_Calendar_EndTime</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"9%\">$i_Calendar_AllDayEvent</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"10%\">$iCalendar_NewEvent_Location</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"14%\">$iCalendar_NewEvent_Description</td>\n";
//$CalMain .= "<td class=\"imail_entry_top\" width=\"5%\">$iCalendar_NewEvent_AccessPrivate</td>\n";
$CalMain .= "</tr>\n";

# Result
if(sizeof($error_entries) > 0){
	for ($i=0 ; $i<sizeof($error_entries) ; $i++){
	    list($subject, $sDate, $sTime, $eDate, $eTime, $isAllDay, $location, $description, $isPrivate) = $error_entries[$i];   
	    $CalMain .= "<tr>\n";
	    $CalMain .= "<td class=\"cal_agenda_content_entry\">$subject</td>\n";
	    $CalMain .= "<td class=\"cal_agenda_content_entry\">$sDate</td>\n";
	    $CalMain .= "<td class=\"cal_agenda_content_entry\">$sTime</td>\n";
	   	$CalMain .= "<td class=\"cal_agenda_content_entry\">$eDate</td>\n";
		$CalMain .= "<td class=\"cal_agenda_content_entry\">$eTime</td>\n";
		$CalMain .= "<td class=\"cal_agenda_content_entry\">$isAllDay</td>\n";
		$CalMain .= "<td class=\"cal_agenda_content_entry\">$location</td>\n";
		$CalMain .= "<td class=\"cal_agenda_content_entry\">$description</td>\n";
		//$CalMain .= "<td class=\"cal_agenda_content_entry\">$isPrivate</td>";
		$CalMain .= "</tr>\n";
	}
} else {
    $CalMain .= "<tr>\n";
    $CalMain .= "<td colspan=\"9\" class=\"cal_agenda_content_entry\">$i_Calendar_NoRecords_Remark</td>\n";
    $CalMain .= "</tr>\n";
}
$CalMain .= "</table>\n";
$CalMain .= "</div>\n";
$CalMain .= "</div>\n";	
$CalMain .= '<br style="clear:both">'; 
	  	    
# Success Record
$CalMain .= "<div style=\"width: 95%; padding-left: 10px; padding-right: 10px;\" id=\"cal_list_mycal\">\n";
$CalMain .= "<form name=\"FormMain\" method=\"get\" action=\"import_update.php\">\n";
$CalMain .= "<h1 style='padding: 0px; font-size: 11px; font-weight: normal; margin-top: 20px; margin-bottom: 0px;'>$i_Calendar_ImportEvent_ValidRecords</h1>\n";
$CalMain .= "<div id='cal_list_mycal'>\n";
$CalMain .= "<table id=\"calViewerTable\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n";
# Title
$CalMain .= "<tr class=\"tabletop\">\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"15%\">$i_Calendar_Subject</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"10%\">$i_Calendar_StartDate</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"9%\">$i_Calendar_StartTime</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"10%\">$i_Calendar_EndDate</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"9%\">$i_Calendar_EndTime</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"9%\">$i_Calendar_AllDayEvent</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"10%\">$iCalendar_NewEvent_Location</td>\n";
$CalMain .= "<td class=\"imail_entry_top\" width=\"14%\">$iCalendar_NewEvent_Description</td>\n";
//$CalMain .= "<td class=\"imail_entry_top\" width=\"5%\">$iCalendar_NewEvent_AccessPrivate</td>\n";
$CalMain .= "</tr>\n";

# Result
if(sizeof($result) > 0){
	for ($i=0 ; $i<sizeof($result) ; $i++){
	    list($subject, $sDate, $sTime, $eDate, $eTime, $isAllDay, $location, $description, $isPrivate) = $result[$i];
	    $isAllDay = ($isAllDay==1)? "True" : "False";
	    $isPrivate = ($isPrivate==1)? "True" : "False";
	    $description = empty($description) ? "-":$description;
	    $location = empty($location) ? "-":$location;
	    $sTime = ($sTime == "") ? "&nbsp;" : $sTime;
	    $eTime = ($eTime == "") ? "&nbsp;" : $eTime;
	   
	    $CalMain .= "<tr>\n";
	    $CalMain .= "<td class=\"cal_agenda_content_entry\">$subject</td>\n";
	    $CalMain .= "<td class=\"cal_agenda_content_entry\">$sDate</td>\n";
	    $CalMain .= "<td class=\"cal_agenda_content_entry\">$sTime</td>\n";
	   	$CalMain .= "<td class=\"cal_agenda_content_entry\">$eDate</td>\n";
		$CalMain .= "<td class=\"cal_agenda_content_entry\">$eTime</td>\n";
		$CalMain .= "<td class=\"cal_agenda_content_entry\">$isAllDay</td>\n";
		$CalMain .= "<td class=\"cal_agenda_content_entry\">$location</td>\n";
		$CalMain .= "<td class=\"cal_agenda_content_entry\">$description</td>\n";
		//$CalMain .= "<td class=\"cal_agenda_content_entry\">$isPrivate</td>\n";
		$CalMain .= "</tr>\n";
	}
} else {
    $CalMain .= "<tr>\n";
    $CalMain .= "<td colspan=\"9\" class=\"cal_agenda_content_entry\">$i_Calendar_NoRecords_Remark</td>\n";
    $CalMain .= "</tr>\n";
}
$CalMain .= "</table>\n";
$CalMain .= "</div>\n";
$CalMain .= '<input type="hidden" name="confirm" id="confirm" value="1" />'."\n";
$CalMain .= '<input type="hidden" name="calID" id="calID" value="'.$calID.'" />'."\n";
$CalMain .= '<br style="clear:both">';

// Button
$CalMain .= '<p class="dotline_p">'.$button.'</p>';
$CalMain .= csrfTokenHtml(generateCsrfToken());	
$CalMain .= "</form>\n";
$CalMain .= "</div>\n";

$CalMain .= '
		  </div>
		</div>
	  </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>';


	
# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain, '','','','','',false,false);

# Layout
$customLeftMenu = $iCal->printLeftMenu();
$MODULE_OBJ["title"] = $iCalendar_Main_Title;
$MODULE_OBJ["logo"] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_icalendar.gif";

$TAGS_OBJ[] = array("<div style='margin:42px 0px -15px 10px;'>$topNavigationMenu</div>", "", 0);

?>
<link href="<?=$PATH_WRT_ROOT?>theme/css/common.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>theme/css/common_cammy.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>theme/css/layout_grey.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>theme/css/font_size_normal.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>theme/css/font_face_verdana.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>theme/css/font_color_layout_grey.css" rel="stylesheet" type="text/css">

<style type="text/css">@import url(css/main.css);</style>
<style type="text/css">
#deleteCalendarWarning {
	font-family:"Arial","Helvetica","sans-serif";
}
</style>
<?php
// $linterface->LAYOUT_START();
include_once("headerInclude.php");
echo '<script>';
echo 'var SessionExpiredWarning = "' . $Lang['Warning']['SessionExpired'] . '";';
echo 'var SessionKickedWarning = "' . $Lang['Warning']['SessionKicked'] . '";';
echo 'var SessionExpiredMsg = "' . $SYS_CONFIG['SystemMsg']['SessionExpired'] . '";';
echo 'var SessionKickedMsg = "' . $SYS_CONFIG['SystemMsg']['SessionKicked'] . '";';
echo '</script>';
?>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<!--<script type="text/javascript" src="js/jquery.blockUI.js"></script>-->
<script type="text/javascript" src="js/interface.js"></script>
<script type="text/javascript" src="js/dimensions.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>

<script language="javascript">
function SessionExpired(data)
{
	return false;
}

function jSubmit_Form(){
	var obj = document.FormMain;
	obj.submit();
}

$(document).ready( function() {
	
});

function toggleInvolveEventVisibility(){
	$(".involve").toggle();
}

function toggleSchoolEventVisibility() {
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
}

function toggleCalVisibility(parClassID) {
	$('.cal-'+parClassID).toggle();
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>

<div id="module_bulletin" class="module_content">
<?=$lui->Get_Sub_Function_Header("iCalendar",$Msg);?>
<?=$display;?>
</div>
<br>

<?php
// $linterface->LAYOUT_STOP();
intranet_closedb();
include_once("footerInclude.php");
?>
