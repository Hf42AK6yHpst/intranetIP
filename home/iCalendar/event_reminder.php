<?php
//page editing by: Carlos Leung
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");

// Global $SYS_CONFIG;
intranet_auth();
intranet_opendb();
// $CurDBName=$SYS_CONFIG['DB']['DBName'];
//$UserID = $_SESSION["UserID"];
$db = new libdb();
$iCal=new icalendar();
$iCal_api=new icalendar_api();
$content = '';
if ($type == 'check'){
	$sql = "select ReminderID, CalType, EventID,
			if (UNIX_TIMESTAMP() >=  UNIX_TIMESTAMP(ReminderDate), 
			-1 ,UNIX_TIMESTAMP(ReminderDate)-UNIX_TIMESTAMP()) as TimeDiff
			From CALENDAR_REMINDER
			where UserID = $UserID and LastSent is null and 
			(
				UNIX_TIMESTAMP() >=  UNIX_TIMESTAMP(ReminderDate) OR
				UNIX_TIMESTAMP() <  UNIX_TIMESTAMP(ReminderDate) AND
				UNIX_TIMESTAMP(ReminderDate) >= all(
					select UNIX_TIMESTAMP(ReminderDate) 
					From CALENDAR_REMINDER
					where UserID = $UserID and LastSent is null and 
					UNIX_TIMESTAMP() <  UNIX_TIMESTAMP(ReminderDate)
				)
			) order by TimeDiff ASC
		"; //echo $sql;
	$result = $iCal->returnArray($sql);
	
	if (count($result) > 0){
		$expireReminderEx = Array();
		$expireReminder = Array();
		$futureReminder = Array();
		$futureReminderContent = '';
		$expireReminderContent = '';
		$nextReminderTime = 0;
		$erid = '';
		$rid = '';
		
		foreach($result as $r){
			if ($r['TimeDiff'] == -1){
				if ($iCal_api->isExternalEvent(trim($r['CalType'])))
					$expireReminderEx[] = $r;
				else{
					$expireReminder[] = $r;
					$erid .= $r['EventID'].',';
				}
				$rid .= $r['ReminderID'].',';
			} 
			else{
				$futureReminder = $r;
				$futureReminderContent .= '<ReminderID>'.$r['ReminderID'].'</ReminderID>';
				$nextReminderTime = $r['TimeDiff'];
			}
		}
		$erid = rtrim($erid,',');
		$rid = trim($rid,',');
		if (count($expireReminderEx)>0 || count($expireReminder)>0){
			$expiredReminderID = '';
			$expireReminderContent .= '<ReminderContent><![CDATA[';
			if (count($expireReminderEx)>0){
				
				foreach($expireReminderEx as $ere){
					$details = $iCal_api->getExternalEventDetail($ere['CalType'],$ere['EventID']);
						$expireReminderContent .="Event Reminder:\n".$details[0]['Title'].' - '.$details[0]['EventDate']." (expired)\n";				
					$expiredReminderID .= $ere['ReminderID'].',';
				}
				
				$expiredReminderID = rtrim($expiredReminderID,',');			
			}
			if (count($expireReminder)>0){
				$sql = "select Title, EventDate from CALENDAR_EVENT_ENTRY where 
						EventID in ($erid)";
				$allEvent = $iCal->returnArray($sql);
				foreach($allEvent as $ae){
					$expireReminderContent .= "Event Reminder:\n".$ae['Title'].' - '.$ae['EventDate']." (expired)\n";
				}
				
			}
			
			$sql = 'update CALENDAR_REMINDER set LastSent = now() where
					ReminderID in ('.$rid.')
					';
			$iCal->db_db_query($sql);			
			$expireReminderContent .= ']]></ReminderContent>';
		}
		
		header("Content-Type: text/xml;");
		header("Cache-Control: no-cache");
		$content .= '<?xml version="1.0" encoding="utf-8"?>';
		$content .= '<Reminder>';
			$content .= '<Expired>';
				$content .= $expireReminderContent;
			$content .= '</Expired>';
			$content .= '<Future>';
				$content .= $futureReminderContent;
				$content .= '<ReminderTime>'.$nextReminderTime.'</ReminderTime>';
			$content .= '</Future>';					
		$content .= '</Reminder>';
	}
}
else if($type == 'show'){
	if (count($reminderID) > 0){
		$rid = implode(',',$reminderID);
		$sql = "select CalType, EventID from CALENDAR_REMINDER where ReminderID in 
					($rid)
				";
		$result = $iCal->returnArray($sql);
		$expireReminderEx = Array();
		$expireReminder = Array();	
		$eid = '';
		foreach($result as $r){			
			if ($iCal_api->isExternalEvent($er['CalType']))
				$expireReminderEx[] = $r;
			else{
				$expireReminder[] = $r;	
				$eid .=  $r['EventID'].',';
			}
		}
		$eid = rtrim($eid,',');
		if (count($expireReminder) > 0){
			$sql = "select Title, EventDate from CALENDAR_EVENT_ENTRY where 
					EventID in ($eid)					
					";
			$allEvent = $iCal->returnArray($sql);
			foreach($allEvent as $ae){
				$content .= "Event Reminder:\n".$ae['Title'].' - '.$ae['EventDate']."\n";				
			}
		}
		
		if (count($expireReminderEx)>0){
			foreach($expireReminderEx as $ere){
				$details = $iCal_api->getExternalEventDetail($ere['CalType'],$ere['EventID']);
				$content .="Event Reminder:\n".$details['Title'].' - '.$details['EventDate']."\n";			
			}	
		}
		
		$sql = "update CALENDAR_REMINDER set LastSent = now() where
					ReminderID in ($rid)";
		$iCal->db_db_query($sql); 
		
	}
}
echo $content;
intranet_closedb();
?>