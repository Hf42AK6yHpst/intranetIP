<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

########################################################################################################################

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;

$linterface = new interface_html("popup.html");

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;


if ($file_content == "") {
	$permitted = array(1,2,3);
}

$li = new libuser($UserID);

# get toStaff, toStudent, toParent options
$sql = "SELECT 
			a.RecordType,a.Teaching,c.ClassLevelID 
		FROM 
			INTRANET_USER AS a 
		LEFT OUTER JOIN 
			INTRANET_CLASS AS b 
		ON
			(a.ClassName=b.ClassName) 
		LEFT OUTER JOIN 
			INTRANET_CLASSLEVEL AS c 
		ON 
			(b.ClassLevelID=c.ClassLevelID) 
		WHERE 
			a.UserID='$UserID'
		";
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_to_options = "	SELECT 
						ToStaffOption,ToStudentOption,ToParentOption 
					FROM 
						INTRANET_IMAIL_RECIPIENT_RESTRICTION 
					WHERE 
						TargetType='$usertype' AND Restricted=1
				";
if($usertype==1)
	$sql_to_options.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_to_options.=" AND ClassLevel='$class_level_id'";
$result_to_options = $li->returnArray($sql_to_options,3);

list($toStaff,$toStudent,$toParent) = $result_to_options[0];

# end get toStaff, toStudent, toParent options

$x1  = ($CatID!="" && $CatID > 0) ? "<select name='CatID' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()' >\n" : "<select name='CatID' onChange='this.form.submit()' >\n";
$x1 .= "<option value='0' >--{$button_select}--</option>\n";

# Create Cat list
if (in_array(1,$permitted) && $toStaff!=-1) {
	$x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
}
if (in_array(2,$permitted) && $toStudent!=-1) {
	$x1 .= "<option value=2 ".(($CatID==2)?"SELECTED":"").">$i_identity_student</option>\n";
}
if (in_array(3,$permitted) && $toParent!=-1) {
	$x1 .= "<option value=3 ".(($CatID==3)?"SELECTED":"").">$i_identity_parent</option>\n";
	if($toParent!=4)
  	$x1 .= "<option value=999 ".(($CatID==999)?"SELECTED":"").">$i_campusmail_select_parents_from_students</option>\n";
}

############# Course Selection ##############
$x1 .= "<option value='4' ".(($CatID==4)?"selected='selected'":"").">$iCalendar_ChooseViewer_Course</option>\n";
#############################################

############# Group (INTRANET_GROUP) Selection ##############
$x1 .= "<option value='5' ".(($CatID==5)?"selected='selected'":"").">$iCalendar_ChooseViewer_Group</option>\n";
#############################################

$x1 .= "</select>";

$name_field = getNameFieldWithClassNumberByLang("a.");
$direct_to_step3=false;

$exclude = "";
if (isset($eventID) && is_numeric($eventID)) {
	$sql  = "SELECT UserID FROM CALENDAR_EVENT_USER ";
	$sql .= "WHERE EventID=".$eventID;
	$excludeArr = $li->returnVector($sql);
	if (sizeof($excludeArr) > 0) {
		$exclude = " AND a.UserID NOT IN (".implode(",", $excludeArr).")";
	}
}

# Step 2
if($CatID!="" && $CatID>0) {
	$selectedCatID = $CatID;
	
	$fields = "DISTINCT a.GroupID, a.ClassName";
	$tables = "INTRANET_CLASS AS a";
	$conds  = "a.RecordStatus=1";
	$orderfield =" a.ClassName ";
	
	if($li->isParent()) {
		$child_list = $li->getChildren();
		$child_list = implode(",",$child_list);
	} else if($li->isStudent()) {
		$child_list = $UserID;
	}
	
	# if Parent / Select Parent From Students 
	if($selectedCatID==3 || $selectedCatID==999) {
		switch($toParent){
			# parents in taught class
			case 1 :
					$t_sql =" SELECT DISTINCT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID='$UserID' ";
					$t_sql2 =" SELECT DISTINCT ClassID FROM INTRANET_SUBJECT_TEACHER WHERE UserID='$UserID' ";
					$t_result = $li->returnVector($t_sql);
					$t_result2 = $li->returnVector($t_sql2);
					for($x=0;$x<sizeof($t_result2);$x++){
						if(!in_array($t_result2[$x],$t_result))
							$t_result[] = $t_result2[$x];
					}
					$t_class_list = implode(",",$t_result);  // the class list taught by the teacher
					if($t_class_list=="") $t_class_list = "''";
					$conds.= " AND a.ClassID IN ($t_class_list) ";
					break;
			# parent in same class level
			case 2 :
					$tables.=", INTRANET_CLASS AS b,INTRANET_USER AS c "; 
					$conds.=" AND c.UserID IN ($child_list) AND c.ClassName=b.ClassName AND b.ClassLevelID=a.ClassLevelID ";
					break;
			# parent in same class
			case 3 :
					$tables.=", INTRANET_USER AS b "; 
					$conds.=" AND b.UserID IN($child_list) AND b.ClassName=a.ClassName ";
					break;
			# student's own parent
			case 4 :
					$sql="SELECT DISTINCT a.UserID,$name_field FROM INTRANET_USER AS a,INTRANET_PARENTRELATION AS b WHERE b.StudentID='$UserID' AND a.UserID=b.ParentID  ORDER BY a.EnglishName";
			    $direct_to_step3=true;
					break;
			# all parent
			default : break;
		}
	} else if($selectedCatID==2) {
		switch($toStudent){
			# students in taught class
			case 1 :	
					$t_sql =" SELECT DISTINCT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID='$UserID' ";
					$t_sql2 =" SELECT DISTINCT ClassID FROM INTRANET_SUBJECT_TEACHER WHERE UserID='$UserID' ";
					$t_result = $li->returnVector($t_sql);
					$t_result2 = $li->returnVector($t_sql2);
					for ($x=0;$x<sizeof($t_result2);$x++) {
						if(!in_array($t_result2[$x],$t_result))
							$t_result[] = $t_result2[$x];
					}
					$t_class_list = implode(",",$t_result);  // the class list taught by the teacher
					if($t_class_list=="")
						$t_class_list = "''";
					$conds.= " AND a.ClassID IN ($t_class_list) ";
					break;
			# students in same class level
			case  2 : 	
					$tables.=", INTRANET_CLASS AS b,INTRANET_USER AS c "; 
					$conds.=" AND c.UserID IN($child_list) AND c.ClassName=b.ClassName AND b.ClassLevelID=a.ClassLevelID ";
					break;
			# students in same class
			case  3 : 	
					$tables.=", INTRANET_USER AS b "; 
					$conds.=" AND b.UserID IN($child_list) AND b.ClassName = a.ClassName ";
					break;
			# parent's own children
			case  4 :
					$sql="SELECT DISTINCT a.UserID,$name_field FROM INTRANET_USER AS a,INTRANET_PARENTRELATION AS b WHERE b.ParentID='$UserID' AND a.UserID=b.StudentID ORDER BY a.ClassName,a.ClassNumber";
					$direct_to_step3=true; 
				  break;
			# all students
			default : break;
		}
	} else if ($selectedCatID==4) {
		$sql  = "SELECT DISTINCT a.CourseID, a.CourseName FROM CALENDAR_COURSE AS a, CALENDAR_USER_COURSE AS b ";
		$sql .= "WHERE b.CourseID=a.CourseID "; #AND b.UserID=".$_SESSION["UserID"]." ";
		$sql .= "ORDER BY a.CourseName";
	} else if ($selectedCatID==5) {
		$sql  = "SELECT DISTINCT a.GroupID, a.Title FROM INTRANET_GROUP AS a ";
		#$sql .= "WHERE b.GroupID=a.GroupID ";
		$sql .= "ORDER BY a.Title";
	}
	
	if(!$direct_to_step3) {
		# Choose courses
		if ($selectedCatID==4) {
			$x2  = "<select name='ChooseGroupID[]' size='10' multiple>\n";
			$x2 .= "<option value='' >--{$button_select}--</option>\n";
			
			$row = $li->returnArray($sql,2);
			for ($i=0; $i<sizeof($row); $i++) {
				$CourseID = $row[$i][0];
				$CourseName = $row[$i][1];
				$x2 .= "<option value='$CourseID'";
				for ($j=0; $j<sizeof($ChooseGroupID); $j++) {
					$x2 .= ($CourseID == $ChooseGroupID[$j]) ? " SELECTED" : "";
				}
				$x2 .= ">$CourseName</option>\n";
			}
			$x2 .= "</select>\n";
		} else if ($selectedCatID==5) {
			$x2  = "<select name='ChooseGroupID[]' size='10' multiple>\n";
			$x2 .= "<option value='' >--{$button_select}--</option>\n";
			
			$row = $li->returnArray($sql,2);
			for ($i=0; $i<sizeof($row); $i++) {
				$GroupID = $row[$i][0];
				$GroupName = $row[$i][1];
				$x2 .= "<option value='$GroupID'";
				for ($j=0; $j<sizeof($ChooseGroupID); $j++) {
					$x2 .= ($GroupID == $ChooseGroupID[$j]) ? " SELECTED" : "";
				}
				$x2 .= ">$GroupName</option>\n";
			}
			$x2 .= "</select>\n";
		} else {
			$sql="SELECT $fields FROM $tables WHERE $conds  ORDER BY $orderfield";
			$x2  = "<select name='ChooseGroupID[]' size='10' multiple>\n";
		  $x2 .= "<option value='' >--{$button_select}--</option>\n";
		
			$row = $li->returnArray($sql,2);
			for($i=0; $i<sizeof($row); $i++){
				$GroupCatID = $row[$i][0];
				$GroupCatName = $row[$i][1];
				$x2 .= "<option value=$GroupCatID";
				for ($j=0; $j<sizeof($ChooseGroupID); $j++) {
					$x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
				}
				$x2 .= ">$GroupCatName</option>\n";
			}
	  	$x2 .= "</select>\n";
	  }
	} else {
		$row = $li->returnArray($sql,2);
		$x3  = "<select name='ChooseUserID[]' size='10' multiple>\n";
		$x3 .= "<option value='' >--{$button_select}--</option>\n";
	  for($i=0; $i<sizeof($row); $i++)
			$x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	  $x3 .= "</select>\n";
	}
} else {
	$selectedCatID = 0-$CatID;
	$fields = "DISTINCT a.UserID,$name_field";
	$tables = "INTRANET_USER AS a";
	$conds  = "a.RecordStatus=1";
	$orderfield=" a.EnglishName ";
	
	if ($li->isParent()) {
		$child_list = $li->getChildren();
		$child_list = implode(",",$child_list);
	} else if ($li->isStudent()) {
		$child_list = $UserID;
	}
	
	# Staff selected
	if ($selectedCatID==1) {
		 $conds .=" AND a.RecordType=1";
		 if ($toStaff==1) {
			switch($usertype) {
				# sender is students
				case 2 : 	$t_sql = "SELECT b.ClassID FROM INTRANET_CLASS AS b, INTRANET_USER AS a WHERE a.UserID='$UserID' AND a.ClassName=b.ClassName ";
							$t_result = $li->returnVector($t_sql);
							$t_class_id = $t_result[0];
							$t_sql =" SELECT DISTINCT UserID FROM INTRANET_CLASSTEACHER WHERE ClassID='$t_class_id'";
							$t_sql2 =" SELECT DISTINCT UserID FROM INTRANET_SUBJECT_TEACHER WHERE ClassID='$t_class_id'";
							$t_result = $li->returnVector($t_sql);
							$t_result2 = $li->returnVector($t_sql2);
							for($x=0;$x<sizeof($t_result2);$x++){
								if(!in_array($t_result2[$x],$t_result))
									$t_result[] = $t_result2[$x];
							}
							$t_teacher_list = implode(",",$t_result);
							if($t_teacher_list=="") $t_teacher_list = "''";
							$conds .= " AND a.UserID IN ($t_teacher_list) ";
							break;
				
				# sender is parents
				case 3 : 	if($child_list=="") break;
							$t_sql = "SELECT b.ClassID FROM INTRANET_CLASS AS b, INTRANET_USER AS a WHERE a.UserID IN ($child_list) AND a.ClassName=b.ClassName ";
							$t_result = $li->returnVector($t_sql);
							$t_class_id = implode(",",$t_result);
							$t_sql =" SELECT DISTINCT UserID FROM INTRANET_CLASSTEACHER WHERE ClassID IN ($t_class_id) ";
							$t_sql2 =" SELECT DISTINCT UserID FROM INTRANET_SUBJECT_TEACHER WHERE ClassID IN ($t_class_id)";

							$t_result = $li->returnVector($t_sql);
							$t_result2 = $li->returnVector($t_sql2);
							for($x=0;$x<sizeof($t_result2);$x++){
								if(!in_array($t_result2[$x],$t_result))
									$t_result[] = $t_result2[$x];
							}
							$t_teacher_list = implode(",",$t_result);
							if($t_teacher_list!="")
								$conds .= " AND a.UserID IN ($t_teacher_list) ";
							break;
				default: break;
			}
		 }
	 	$sql="SELECT $fields FROM $tables WHERE $conds $exclude ORDER BY $orderfield";
	 	$row = $li->returnArray($sql,2);

		$x3  = "<select name='ChooseUserID[]' size='10' multiple>\n";		 
		$x3 .= "<option value='' >--{$button_select}--</option>\n";
	    for($i=0; $i<sizeof($row); $i++)
		    $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	    $x3 .= "</select>\n";
	}

}
if(isset($ChooseGroupID)){
	$group_id = implode(",",$ChooseGroupID);
	$fields = "DISTINCT a.UserID, $name_field";
	$tables = "INTRANET_USER AS a";
	$conds ="a.RecordStatus=1";
	$orderfield="";
	
	# show student list
	if($CatID==2 || $CatID==999){
		$tables.=",INTRANET_CLASS AS b";
		$conds.=" AND a.RecordType=2 AND b.GroupID IN($group_id) AND a.ClassName=b.ClassName";
		$orderfield=" ORDER BY a.ClassName,a.ClassNumber ";
	}
	# show parent list
	if($CatID==3){
		$tables.=",INTRANET_USER AS b,INTRANET_PARENTRELATION AS c, INTRANET_CLASS AS d";
		$conds.=" AND a.RecordType=3 AND d.GroupID IN($group_id) AND b.ClassName=d.ClassName AND b.UserID=c.StudentID AND a.UserID=c.ParentID";
		$orderfield=" ORDER BY a.EnglishName ";
	}
	# show student list (in course)
	if ($CatID==4) {
		$tables .= ", CALENDAR_USER_COURSE AS b";
		$conds .= " AND a.RecordType=2 AND b.CourseID IN ($group_id) AND b.UserID=a.UserID";
		$orderfield = " ORDER BY a.EnglishName ";
	}
	# show student list (in group)
	if ($CatID==5) {
		$tables .= ", INTRANET_USERGROUP AS b";
		$conds .= " AND a.RecordType=2 AND b.GroupID IN ($group_id) AND b.UserID=a.UserID";
		$orderfield = " ORDER BY a.EnglishName ";
	}
	
	$sql ="SELECT $fields FROM $tables WHERE $conds $exclude $orderfield";
	$row = $li->returnArray($sql,2);

	$x3  = "<select name='ChooseUserID[]' size='10' multiple>\n";		 
	$x3 .= "<option value='' >--{$button_select}--</option>\n";
	for($i=0; $i<sizeof($row); $i++)
		$x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	$x3 .= "</select>\n";
}

$selectMethod1  = "<input type='radio' name='selectMethod' id='selectMethod1' value='select' ";
$selectMethod1 .= (($selectMethod=="select" || !isset($selectMethod)) ? "checked='checked'" : "")." style='margin-bottom:-3px;' />"; 

$selectMethod2  = "<input type='radio' name='selectMethod' id='selectMethod2' value='search' ";
$selectMethod2 .= ($selectMethod=="search" ? "checked='checked'" : "")." style='margin-bottom:-3px;' />"; 

$step1 = getStepIcons(3,1,$i_SelectMemberSteps);
$step2 = getStepIcons(3,2,$i_SelectMemberSteps);
$step3 = getStepIcons(3,3,$i_SelectMemberSteps);
$suf_parent = $i_general_targetParent;
?>
<style type="text/css">@import url(../css/main.css);</style>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script language="javascript">
function AddOptions(obj, type) {
	par = opener.window;
	parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
	if (type==0)    // Normal
		x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
	else
		x = (obj.name == "ChooseGroupID[]") ? "Q" : "P";
	
	// Case of "Select parent from student"
	selectedCat = document.form1.CatID.selectedIndex;
	if (document.form1.CatID.options[selectedCat].value=="999" && obj.name != "ChooseGroupID[]")
		x = "P";
	else if (document.form1.CatID.options[selectedCat].value=="4")
		x = "C";
	else if (document.form1.CatID.options[selectedCat].value=="5")
		x = "E";
	
	checkOption(obj);
	par.checkOption(parObj);
	i = obj.selectedIndex;
	
	while(i!=-1) {
		addtext = obj.options[i].text;
		if (x == "G")
			addtext += " (<?=$i_eClass_ClassLink?>)";
		else if (x == "C")
			addtext += " (<?=$iCalendar_ChooseViewer_Course?>)";
		else if (x == "E")
			addtext += " (<?=$iCalendar_ChooseViewer_Group?>)";
		else if  (x == "P")
			addtext += "<?=$iCalendar_ChooseViewer_ParentSuffix?>";
		
		par.checkOptionAdd(parObj, addtext, x + obj.options[i].value);
		obj.options[i] = null;
		i = obj.selectedIndex;
	}
}

function checkOptionNone(obj) {
	if(obj==null) return;
	for(i=0; i<obj.length; i++){
		obj.options[i].selected = false;
	}
}

function SelectAll(obj) {
	for (i=0; i<obj.length; i++) {
		obj.options[i].selected = true;
	}
}

function setRadioStatus() {
	if ($("[@name='selectMethod']").fieldValue() == "select") {
		$("[@name='CatID']").removeAttr("disabled");
		$("[@name='keyword']").attr("disabled", "true");
		$("[@name='submit1']").attr("disabled", "true");
	} else {
		$("[@name='CatID']").attr("disabled", "true");
		$("[@name='CatID']")[0].selectedIndex = 0;
		$("[@name='keyword']").removeAttr("disabled");
		$("[@name='submit1']").removeAttr("disabled");
	}
}

$(document).ready( function() {
	setRadioStatus();
	$("[@name='selectMethod']").click(function() {
		setRadioStatus();
	});
	
	$("form[@name='form1']").submit(function() {
		if ($("[@name='selectMethod']").fieldValue() == "select") {
			return true;
		}
		if ($.trim($("[@name=keyword]").val()) == "") {
			$("#errMessageBox").html("<li><?=$i_Calendar_InvalidSearch_NoKeyword?></li>")
			$("[@name=keyword]")[0].focus();
			$("[@name=keyword]").addClass("error");
		} else {
			$("#errMessageBox").html("");
			$("[@name=keyword]").removeClass("error");
			
			$.get("search_user_result.php",
				{
					eventID: $("input[@name=eventID]").val() , 
					keyword: encodeURIComponent($("input[@name=keyword]").val())
				},
				function(data){
					var curRow = $("[@name='keyword']").parent().parent();
					curRow.next().remove();
					curRow.next().remove();
					curRow.next().remove();
					var temp = "<tr><td height='1' colspan='2' class='dotline'>";
					temp += "<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1' />";
					temp += "</td></tr>";
					curRow.after(temp);
					curRow = curRow.next();
					temp = "<tr><td height='5' colspan='2'>";
					temp += "<img width='10' height='1' src='/images/2007a/10x10.gif'/>";
					temp += "</td></tr>";
					curRow.after(temp);
					curRow = curRow.next();
					temp = "<tr><td width='30%' valign='top' nowrap='nowrap'>";
					temp += "<span class='tabletext'><?=$i_adminmenu_user?>:</span></td>";
					temp += "<td>";
					temp += "<table cellspacing='0' cellpadding='0' border='0' align='left'>";
					temp += "<tbody><tr><td>";
					temp += data;
					temp += "</td></tr></tbody></table>";
					curRow.after(temp);
				}
			);
		}
		return false;
	});
});
</script>
<?php
	$linterface->LAYOUT_START();
?>
<br />
<ul id="errMessageBox"></ul>
<form name="form1" action="choose_event_guest.php" method="post" >
<table id="html_body_frame" width="480" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td>
						<table id="keepTable" width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
							<tbody>
							<tr class="keepRow">
								<td nowrap="nowrap" width="30%" >
									<?=$selectMethod1?> 
									<label for="selectMethod1"><?=$i_GroupCategorySettings?>:</label>
								</td>
								<td ><?=$x1?></td>
							</tr>
							<tr class="keepRow">
								<td nowrap="nowrap" width="30%" >
									<?=$selectMethod2?> 
									<label for="selectMethod2"><?=$button_find?>:</label>
								</td>
								<td >
									<input type="text" class="textboxnum" name="keyword" maxlength="50" />
									<?=$linterface->GET_BTN($button_submit, "submit", "","submit1"," class='formsubbutton'")?>
								</td>
							</tr>
							<?php 
							if($CatID!="" && $CatID > 0 && !$direct_to_step3) 
							{ 
							?>		
							<tr> 
								<td height="1" colspan="2" class="dotline" valign="middle" >
									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
								</td>
							</tr>
							<tr> 
								<td height="5" colspan="2"  >
									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
								</td>
							</tr>
							<tr> 			
								<td valign="top" nowrap="nowrap" width="30%" >
									<span class="tabletext"><?=$iDiscipline['class']?>:</span>
								</td>
								<td >					
									<table cellpadding="0" cellspacing="0" >
										<tr>
											<td><?=$x2?></td>
											<td style="vertical-align:bottom">
												<table cellpadding="0" cellspacing="6" >				
													<?php 
													if($CatID!=3 && $CatID!=999)
													{
													?>
													<tr>
														<td>
														<?php
															echo $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],0)","add"," class='formsubbutton'")."" ;
														?>
														</td>
													</tr>	
													<?php
													}
													?>
													<tr>
														<td>
															<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);","submit2"," class='formsubbutton'")."" ?>
														</td>
													</tr>	
													<?php 
													if($CatID==3 || $CatID==999)
													{
													?>
													<tr>
														<td>
														<?php
															echo $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],1)","add"," class='formsubbutton'")."";
														?>
														</td>
													</tr>	
													<?php 
													} 
													if(!$sys_custom['Mail_NoSelectAllButton']) 
													{ 
													?>
													<tr>
														<td>
														<?= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseGroupID[]']); return false;","submit3"," class='formsubbutton'") ?>
														</td>
													</tr>					
													<?php 
													} 
													?>
												</table>
											</td>
										</tr>
									</table>			
								</td>
							</tr>
							<?php 
							}
							if($CatID!=""&&(isset($ChooseGroupID) || $CatID<0 || $direct_to_step3)) 
							{ 
							?>
							<tr> 
								<td height="1" colspan="2" class="dotline">
									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
								</td>
							</tr>
							<tr> 
								<td height="5" colspan="2"  >
									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
								</td>
							</tr>
							<tr >
								<td valign="top" nowrap="nowrap" width="30%" >
									<span class="tabletext"><?=$i_CampusMail_New_AddressBook_ByUser?>:</span>
								</td>
								<td >					
									<table border="0" cellpadding="0" cellspacing="0" align="left">		
										<tr >
											<td ><?=$x3?></td>
											<td valign="bottom" >
												<table cellpadding="0" cellspacing="6" >
													<?php
													if($CatID==999)
													{ 
													?>
													<tr >
														<td >													
														<?php
															echo $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'],1)","submit2"," class='formsubbutton'")."";					
													 	?>
														</td >
													</tr >	
													<?php 
													} else {
													?>
													<tr >
														<td >
														<?php
															echo  $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'],0)","submit2"," class='formsubbutton'")."" ;
														?>
														</td >
													</tr >	
													<?php 
													}
													?>
													<?php 
													if(!$sys_custom['Mail_NoSelectAllButton']) 
													{ 
													?>
													<tr >
														<td >																		
														<?= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseUserID[]']); return false;","submit3"," class='formsubbutton'") ?>
														</td >
													</tr >	
													<?php 
													} 
													?>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<?php 
							}
							?>
							</tbody>
						</table>		
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
								<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td align="center">
								<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
<input type="hidden" name="eventID" value="<?php echo $eventID; ?>" />
</form>

<?php
########################################################################################################################
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
	