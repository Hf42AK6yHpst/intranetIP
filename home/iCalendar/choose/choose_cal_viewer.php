<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libicalendar.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$iCal = new libicalendar;

$MODULE_OBJ['title'] = $iCalendar_NewEvent_SelectGuest;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if ($targetClass != "") {
  $ts_record = strtotime($TargetDate);
	if ($ts_record == -1) {
	    $TargetDate = date('Y-m-d');
	    $ts_record = strtotime($TargetDate);
	}
	$txt_year = date('Y',$ts_record);
	$txt_month = date('m',$ts_record);
	$txt_day = date('d',$ts_record);
	
	if (isset($calID) && trim($calID) != "") {
		# find a list of existing viewers, exclude them for selection
		$existGuestSql = "NOT IN (".$iCal->returnCalViewerList($calID, true).")";
	}
	
	$name_field = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT 
					a.UserID, $name_field 
			FROM 
					INTRANET_USER AS a 
			WHERE 
					a.ClassName = '$targetClass' 
					$existGuestSql 
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";

	$list = $lclass->returnArray($sql,2);
	$select_students = getSelectByArray($list, "size=\"25\" multiple name=\"targetID[]\"");
}
?>

<script language="javascript">
function AddOptions(obj) {
	par = window.opener;
	parTable = par.document.getElementById("calViewerTable");
	noOfTr = parTable.tBodies[0].getElementsByTagName("TR");
	tableLastRowClass = noOfTr[noOfTr.length-1].className;
	
	if (tableLastRowClass=="" || tableLastRowClass=="tablerow1") {
		firstRowClass = "tablerow2";
	} else {
		firstRowClass = "tablerow1";
	}
	
	checkOption(obj);
	i = obj.selectedIndex;
	
	while (i!=-1) {
		newName = par.document.createTextNode(obj.options[i].text);
		
		selPermission =  "<select name='permission-" + obj.options[i].value + "' id='permission-" + obj.options[i].value + "'>";
		selPermission += "<option value='W'><?=$iCalendar_NewCalendar_SharePermissionEdit?></option>";
		selPermission += "<option value='R' selected><?=$iCalendar_NewCalendar_SharePermissionRead?></option>";
		selPermission += "</select>";
		
		delViewer  = "<span onclick='removeViewer(this)' class='tablelink'>";
		delViewer += "<img width='12' height='12' border='0' align='absmiddle' src='/images/2007a/icon_delete.gif' />";
		delViewer += "</span>";
		delViewer += "<input type='hidden' value='"+obj.options[i].value+"' name='viewerID[]' />";
		
		newRow = par.document.createElement("tr");
		newRow.className = firstRowClass;
	
		newCellName = par.document.createElement("td");
		newCellName.className = "tabletext";
		boldName = par.document.createElement("strong");
		boldName.appendChild(newName);
		newCellName.appendChild(boldName);
		
		newCellPermission = par.document.createElement("td");
		newCellPermission.className = "tabletext";
		newCellPermission.innerHTML = selPermission;
		
		newCellDelete = par.document.createElement("td");
		newCellDelete.className = "tabletext";
		newCellDelete.innerHTML = delViewer;
		
		newRow.appendChild(newCellName);
		newRow.appendChild(newCellPermission);
		newRow.appendChild(newCellDelete);
		
		parTable.tBodies[0].appendChild(newRow);
		obj.options[i] = null;
		i = obj.selectedIndex;
		
		if (firstRowClass == "tablerow1") {
			firstRowClass = "tablerow2";
		} else {
			firstRowClass = "tablerow1";
		}
	}
}

function SelectAll(obj) {
	for (i=0; i<obj.length; i++) {
		obj.options[i].selected = true;
	}
}
</script>
<br />
<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$iDiscipline['class']?>:
					</td>
					<td width="80%"><?=$select_class?></td>
				</tr>
				<?php if($targetClass != "") { ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?= $iDiscipline['students']?>:</span>
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><?= $select_students ?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<?php } ?>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>">
</form>
<br />
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>