<?php
$PATH_WRT_ROOT = "../../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

#$permitted[] = 2;
$permitted = explode(",",1);
$luser = new libuser();

$li = new libgrouping();
$lo = new libgroup($GroupID);

$teaching=0;
if($CatID < 0){
     unset($ChooseGroupID);
     //$ChooseGroupID[0] = 0-$CatID;
     switch($CatID){
	     case -2 : $ChooseGroupID[0]=2; $teaching=0;break;
	     case -99 : $ChooseGroupID[0]=1; $teaching=1;break;
	     case -100: $ChooseGroupID[0]=1; $teaching=0;break;
	     default:$ChooseGroupID[0]=1; $teaching=1;
	 }
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

/*$row = $lo->returnRoleType();
$RoleOptions .= "<select name=RoleID>\n";
for($i=0; $i<sizeof($row); $i++)
$RoleOptions .= (Isset($RoleID)) ? "<option value=".$row[$i][0]." ".(($row[$i][0]==$RoleID)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
$RoleOptions .= "</select>\n";*/


$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=\"CatID\" onChange=\"checkOptionNone(this.form.elements['ChooseGroupID[]']);this.form.submit()\">\n" : "<select name=\"CatID\" onChange=\"this.form.submit()\"s>\n";
$x1 .= "<option value=\"0\"></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}

$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";


    $x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
if (in_array(3,$permitted))
    $x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
    
$x1 .="<option value=-99 ".(($CatID==-99)?"SELECTED":"").">$i_teachingStaff</option>\n";
$x1 .="<option value=-100 ".(($CatID==-100)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";


if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          $x2 .= "<option value=$GroupCatID";
          for($j=0; $j<sizeof($ChooseGroupID); $j++){
          $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
          }
          $x2 .= ">$GroupCatName</option>\n";
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

if(isset($ChooseGroupID)) {
    if($CatID<0)
		$row = $luser->returnUsersByIdentity($ChooseGroupID[0],$teaching);
	else
   		$row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}

$step1 = getStepIcons(3,1,$i_SelectMemberSteps);
$step2 = getStepIcons(3,2,$i_SelectMemberSteps);
$step3 = getStepIcons(3,3,$i_SelectMemberSteps);
?>

<script language="javascript">
function AddOptions(obj){
    /*par = opener.window;
     parObj = opener.window.document.form1.elements["target"];
     x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
     checkOption(obj);
     //par.checkOption(parObj);
     i = obj.selectedIndex;
     while(i!=-1&&obj.options[i]!=null){
			parObj.value += (parObj.value==""?"":",")+ x + obj.options[i].value;
          //par.checkOptionAdd(parObj, obj.options[i].text, x + obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
	 par.document.form1.elements["role"].value=document.form1.elements["RoleID"].value;
	par.document.form1.action="add_user.php";
	par.document.form1.submit();
	window.close();
	 
     //par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");*/
	 par = window.opener;
	parTable = par.document.getElementById("calViewerTable");
	noOfTr = parTable.tBodies[0].getElementsByTagName("TR");
	tableLastRowClass = noOfTr[noOfTr.length-1].className;
	
	if (tableLastRowClass=="" || tableLastRowClass=="tablerow1") {
		firstRowClass = "tablerow2";
	} else {
		firstRowClass = "tablerow1";
	}
	
	x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
	// Case of "Select parent from student"
	selectedCat = document.form1.CatID.selectedIndex;
	if (document.form1.CatID.options[selectedCat].value=="999" && obj.name != "ChooseGroupID[]")
		x = "P";
	else if (document.form1.CatID.options[selectedCat].value=="4")
		x = "C";
	else if (document.form1.CatID.options[selectedCat].value=="5")
		x = "E";
	checkOption(obj);
	i = obj.selectedIndex;
	
	while (i!=-1) {
		nameText = obj.options[i].text;
		if (x == "G")
			nameText += " (<?=$i_eClass_ClassLink?>)";
		else if (x == "C")
			nameText += " (<?=$iCalendar_ChooseViewer_Course?>)";
		else if (x == "E")
			nameText += " (<?=$iCalendar_ChooseViewer_Group?>)";
		else if  (x == "P")
			nameText += "<?=$iCalendar_ChooseViewer_ParentSuffix?>";
		newName = par.document.createTextNode(nameText);
		
		selPermission =  "<select name='permission-" + x + obj.options[i].value + "' id='permission-" + x + obj.options[i].value + "'>";
		selPermission += "<option value='W'><?=$iCalendar_NewCalendar_SharePermissionEdit?></option>";
		selPermission += "<option value='R' selected><?=$iCalendar_NewCalendar_SharePermissionRead?></option>";
		selPermission += "</select>";
		
		delViewer  = "<span onclick='removeViewer(this)' class='tablelink'>";
		delViewer += "<img width='12' height='12' border='0' align='absmiddle' src='/images/2009a/icon_delete.gif' />";
		delViewer += "</span>";
		delViewer += "<input type='hidden' value='"+x+obj.options[i].value+"' name='viewerID[]' />";
		
		newRow = par.document.createElement("tr");
		newRow.className = firstRowClass;
	
		/*
		// eclass style
		newCellName = par.document.createElement("td");
		newCellName.className = "tabletext";
		boldName = par.document.createElement("strong");
		boldName.appendChild(newName);
		newCellName.appendChild(boldName);
		
		newCellPermission = par.document.createElement("td");
		newCellPermission.className = "tabletext";
		newCellPermission.innerHTML = selPermission;
		
		newCellDelete = par.document.createElement("td");
		newCellDelete.className = "tabletext";
		newCellDelete.innerHTML = delViewer;
		*/
		
		firstNode = "<a href='#' class='imail_entry_read_link'><img src='<?=$ImagePathAbs?>imail/icon_ppl_ex.gif' width='20' height='20' border='0' align='absmiddle'>";
		firstNode += nameText;
		firstNode += "</a>";
			
		// new style
		newCellName = par.document.createElement("td");
		newCellName.className = "cal_agenda_content_entry";
		newCellName.nowrap = true;
		newCellName.innerHTML = firstNode;
		
		newCellPermission = par.document.createElement("td");
		newCellPermission.className = "cal_agenda_content_entry";
		newCellPermission.innerHTML = selPermission;
		
		newCellDelete = par.document.createElement("td");
		newCellDelete.className = "cal_agenda_content_entry cal_agenda_content_entry_button";
		newCellDelete.innerHTML = delViewer;
		
		newRow.appendChild(newCellName);
		newRow.appendChild(newCellPermission);
		newRow.appendChild(newCellDelete);
		
		parTable.tBodies[0].appendChild(newRow);
		obj.options[i] = null;
		i = obj.selectedIndex;
		
		if (firstRowClass == "tablerow1") {
			firstRowClass = "tablerow2";
		} else {
			firstRowClass = "tablerow1";
		}
	}
	 
}

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

function add_role(){
     Obj = window.document.form1;
     role_name = prompt("", "New_Role");
     if(role_name!=null && Trim(role_name)!=""){
          Obj.elements["role"].value = role_name;

          Obj.action = "../info/add_role.php";
          Obj.submit();
     }
}
</script>

<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td></td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		<!--		<tr>
					<td colspan=2>
						<?=$RoleOptions?> <?= $linterface->GET_BTN($Lang['Group']['NewRole'] , "button","add_role()") . "<br>"; ?>
					</td>
				</tr>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr> -->

				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_category?></span>
					</td>
					<td ><?=$x1?></td>
				</tr>
				<? if($CatID!=0 && $CatID > 0) { ?>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_group?></span>
					</td>
					<td>
						<table border='0' cellspacing='1' cellpadding='1'>
							<tr>
								<td><?=$x2?></td>
								<td>
									<?= $linterface->GET_BTN($button_add, "button","checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
									<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
									<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseGroupID[]']); return false;") . "<br>"; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<? } ?>
				<? if(isset($ChooseGroupID)) { ?>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_user?></span>
					</td>
					<td>
						<table border='0' cellspacing='1' cellpadding='1'>
							<tr>
								<td><?=$x3?></td>
								<td>
									<?= $linterface->GET_BTN($button_add, "submit","checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])") . "<br>"; ?>
									<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseUserID[]']); return false;") . "<br>"; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<? } ?>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type=hidden name=role value="">
<input type=hidden name=fieldname value="<?=$fieldname?>">
<input type=hidden name=GroupID value="<?=$GroupID?>">

</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>