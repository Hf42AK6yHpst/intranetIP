<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$keyword = urldecode(addslashes(trim($keyword)));
$keyword = iconv("UTF-8", "BIG5", $keyword);

$eventID = trim($eventID);
$calID = trim($calID);

$cond = "";
if (!empty($eventID) && is_numeric($eventID)) {
	$sql  = "SELECT UserID FROM CALENDAR_EVENT_USER ";
	$sql .= "WHERE EventID = $eventID";
	$result = $li->returnVector($sql);
	if (sizeof($result) > 0) {
		$cond = "AND a.UserID NOT IN (".implode(",", $result).") ";
	}
}

if (!empty($calID) && is_numeric($calID)) {
	$sql  = "SELECT UserID FROM CALENDAR_CALENDAR_VIEWER ";
	$sql .= "WHERE CalID = $calID";
	$result = $li->returnVector($sql);
	if (sizeof($result) > 0) {
		$cond = "AND a.UserID NOT IN (".implode(",", $result).") ";
	}
}

if (empty($keyword)) {
	echo "<p class='tabletextremark'>$i_InventorySystem_Error</p>";
	exit;
}

$user_field = getNameFieldWithClassNumberByLang("a.");

$sql  = "SELECT a.UserID, $user_field AS Name FROM INTRANET_USER AS a ";
$sql .= "WHERE (a.EnglishName LIKE '%$keyword%' OR a.ChineseName LIKE '%$keyword%' OR a.NickName LIKE '%$keyword%') ";
$sql .= $cond;
$sql .= "ORDER BY a.EnglishName";

$result = $li->returnArray($sql);

if (sizeof($result) > 0) {
	$x = "<select multiple='multiple' size='10' name='ChooseUserID[]'>";
	$x .= "<option value=''>--{$button_select}--</option>";
	for($i=0; $i<sizeof($result); $i++) {
		$x .= "<option value='".$result[$i]["UserID"]."'>".$result[$i]["Name"]."</option>";
	}
	$x .= "</select>";
	$x .= "</td><td valign='bottom'>";
	$x .= "<table cellspacing='6' cellpadding='0'><tbody><tr><td>";
	$x .= "<input type='button' class='formsubbutton' value='Add' id='submit2' name='submit2' onclick='checkOption(this.form.elements[\"ChooseUserID[]\"]);AddOptions(this.form.elements[\"ChooseUserID[]\"],0)'/>";
	$x .= "</td></tr><tr><td>";
	$x .= "<input type='submit' class='formsubbutton' value='Select ALL' id='submit3' name='submit3' onclick='SelectAll(this.form.elements[\"ChooseUserID[]\"]); return false;'/>";
	$x .= "</td></tr></tbody></table>";
} else {
	$x = "<p class='tabletextremark'>$i_no_search_result_msg</p>";
}

echo iconv("BIG5", "UTF-8", $x);
intranet_closedb();
?>