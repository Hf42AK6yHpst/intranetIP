// Javascript functions for iCalendar
function enableTimeSelect() {
	if (document.getElementById("is_allday_1").checked) {
		document.form1.eventHr.disabled = false;
		document.form1.eventMin.disabled = false;
		document.form1.durationHr.disabled = false;
		document.form1.durationMin.disabled = false;
	}
	
	if (document.getElementById("is_allday_0").checked) {
		document.form1.eventHr.disabled = true;
		document.form1.eventMin.disabled = true;
		document.form1.durationHr.disabled = true;
		document.form1.durationMin.disabled = true;
	}
	
}

function removeReminder(reminder) {
	var divReminder = reminder.parentNode.parentNode;
	var spanToRemove = reminder.parentNode;
	
	var removed = divReminder.removeChild(spanToRemove);
	var spans = divReminder.getElementsByTagName("span");
	
	if (spans.length == 1) {
		document.getElementById("noReminderMsg").style.display = "block";
	}
	
	if (spans.length <= (<?=$ReminderLimit?>*2-1)) {
		document.getElementById("addReminderSpan").style.display = "block";
	}
}

function addReminder() {
	var divReminder = document.getElementById("reminderDiv");
	document.getElementById("noReminderMsg").style.display = "none";
	
	var spans = divReminder.getElementsByTagName("span");
	if (spans.length == (<?=$ReminderLimit?>*2-1))
		document.getElementById("addReminderSpan").style.display = "none";
	
	var newSpan = "<span>";
	newSpan += "<?= str_replace("\n", "", $iCal->generateReminderSelect()) ?>";
	newSpan += "&nbsp;<span class='tablelink' onClick='removeReminder(this)'><?=$iCalendar_NewEvent_Reminder_Remove?></span> <br />";
	newSpan += "</span>";
	
	divReminder.innerHTML += newSpan;
}

function showRepeatRangeEnd() {
	var repeatRangeEndNever0 = document.getElementById("repeatRangeEndNever0");
	var repeatRangeEndNever1 = document.getElementById("repeatRangeEndNever1");
	var repeatRangeEndDiv = document.getElementById("repeatRangeEndDiv");
	
	if (repeatRangeEndNever0.checked)
		repeatRangeEndDiv.style.visibility = "hidden";
	if (repeatRangeEndNever1.checked)
		repeatRangeEndDiv.style.visibility = "visible";
	
	changeRepeatMsg();
}

function showRepeatCtrl() {
	var repeatMsgTable = document.getElementById("repeatMsgTable");
	var repeatMsgSpan = document.getElementById("repeatMsgSpan");
	var repeatSelect = document.getElementById("repeatSelect");
	var repeatCtrl = document.getElementById("repeatCtrl");
	var repeatRange = document.getElementById("repeatRange");
	var repeatRangeEndNever1 = document.getElementById("repeatRangeEndNever1");
	
	document.form1.repeatRangeStart.value = document.form1.eventDate.value;
	
	var repeatSelected = repeatSelect.options[repeatSelect.selectedIndex].value;
	
	switch(repeatSelected)
	{
	case "NOT":
		repeatCtrl.style.display = "none";
		repeatRange.style.display = "none";
		repeatMsgTable.style.display = "none";
		break;
	case "DAILY":
		dailyCtrl = "<?=$iCalendar_NewEvent_Repeats_Every?>";
		dailyCtrl += "<select name='repeatDailyDay' onChange='changeRepeatMsg()'>";
		dailyCtrl += "<option value='1'>"+i+" <?=$iCalendar_NewEvent_DurationDay?></option>";
		for (i=2; i<=14; i++)
			dailyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationDays?></option>";
		dailyCtrl += "</select>";
		repeatCtrl.innerHTML = dailyCtrl;
		repeatCtrl.style.display = "block";
		repeatRange.style.display = "block";
		
		newMsg = "<?=$iCalendar_NewEvent_Repeats_Daily?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "WEEKDAY":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryWeekday2?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatCtrl.style.display = "none";
		repeatRange.style.display = "block";
		repeatMsgTable.style.display = "block";
		break;
	case "MONWEDFRI":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryMonWedFri2?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatCtrl.style.display = "none";
		repeatRange.style.display = "block";
		repeatMsgTable.style.display = "block";
		break;
	case "TUESTHUR":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryTuesThur2?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "WEEKLY":	
		weeklyCtrl = "<?=$iCalendar_NewEvent_Repeats_Every?>";
		weeklyCtrl += "<select name='repeatWeeklyRange' onChange='changeRepeatMsg()'>";
		weeklyCtrl += "<option value='1'>"+i+" <?=$iCalendar_NewEvent_DurationWeek?></option>";
		for (i=2; i<=14; i++)
			weeklyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationWeeks?></option>";
		weeklyCtrl += "</select>";
		weeklyCtrl += "<br />";
		weeklyCtrl += "<?=$iCalendar_NewEvent_Repeats_On?><br />";
		<?
			foreach ($iCalendar_NewEvent_Repeats_WeekdayArray as $value) {
		?>
		weeklyCtrl += "<input type='checkbox' name='<?=$value?>' id='<?=$value?>' onClick='changeRepeatMsg()' <?=($value==date("D"))?"CHECKED":""?> /> ";
		weeklyCtrl += "<label for='<?=$value?>'><?=$value?></label>&nbsp;&nbsp;";
		<?
			}
		?>
		repeatCtrl.innerHTML = weeklyCtrl;
		repeatCtrl.style.display = "block";
		repeatRange.style.display = "block";
		
		newMsg = "<?=$iCalendar_NewEvent_Repeats_Weekly2?>";
		newMsg += "<?=date("l")?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "MONTHLY":
		monthlyCtrl = "<?=$iCalendar_NewEvent_Repeats_Every?>";
		monthlyCtrl += "<select name='repeatMonthlyRange' onClick='changeRepeatMsg()'>";
		monthlyCtrl += "<option value='1'>"+i+" <?=$iCalendar_NewEvent_DurationMonth?></option>";
		for (i=2; i<=14; i++)
			monthlyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationMonths?></option>";
		monthlyCtrl += "</select>";
		monthlyCtrl += "<br />";
		monthlyCtrl += "<?=$iCalendar_NewEvent_Repeats_By?><br />";
		monthlyCtrl += "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy0' onClick='changeRepeatMsg()' value='dayOfMonth' CHECKED> ";
		monthlyCtrl += "<label for='monthlyRepeatBy0'><?=$iCalendar_NewEvent_Repeats_Monthly3?></label>";
		monthlyCtrl += "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy1' onClick='changeRepeatMsg()' value='dayOfWeek'> ";
		monthlyCtrl += "<label for='monthlyRepeatBy1'><?=$iCalendar_NewEvent_Repeats_Monthly4?></label>";
		
		repeatCtrl.innerHTML = monthlyCtrl;
		repeatCtrl.style.display = "block";
		repeatRange.style.display = "block";
		
		newMsg = "<?=$iCalendar_NewEvent_Repeats_Monthly2?>";
		newMsg += "<?=$iCalendar_NewEvent_DurationDay." ".date("j")?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "YEARLY":
		yearlyCtrl = "<?=$iCalendar_NewEvent_Repeats_Every?>";
		yearlyCtrl += "<select name='repeatYearlyRange' onClick='changeRepeatMsg()'>";
		yearlyCtrl += "<option value='1'>"+i+" <?=$iCalendar_NewEvent_DurationYear?></option>";
		for (i=2; i<=14; i++)
			yearlyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationYears?></option>";
		yearlyCtrl += "</select>";
		yearlyCtrl += "<br />";
		
		repeatCtrl.innerHTML = yearlyCtrl;
		repeatCtrl.style.display = "block";
		repeatRange.style.display = "block";
		
		newMsg = "<?=$iCalendar_NewEvent_Repeats_Yearly2?>";
		newMsg += "<?=date("F j")?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	}
}

function changeRepeatMsg() {
	var repeatSelect = document.getElementById("repeatSelect");
	var repeatMsgSpan = document.getElementById("repeatMsgSpan");
	var repeatSelected = repeatSelect.options[repeatSelect.selectedIndex].value;
	var repeatRangeEndNever1 = document.getElementById("repeatRangeEndNever1");
	var eventDate = document.form1.eventDate.value;
	var eventDatePieces = eventDate.split("-");
	var eventDateObj = new Date();
	eventDateObj.setFullYear(eventDatePieces[0],eventDatePieces[1]-1,eventDatePieces[2]);
	switch(repeatSelected)
	{
	case "DAILY":
		if (document.form1.repeatDailyDay.value == 1)
			newMsg = "<?=$iCalendar_NewEvent_Repeats_Daily?>";
		else
			newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>" + document.form1.repeatDailyDay.value + " <?=$iCalendar_NewEvent_DurationDays?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		break;
	case "WEEKDAY":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryWeekday2?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		break;
	case "MONWEDFRI":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryMonWedFri2?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		break;
	case "TUESTHUR":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryTuesThur2?>";
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		break;
	case "WEEKLY":
		if (document.form1.repeatWeeklyRange.value == "1")
			newMsg = "<?=$iCalendar_NewEvent_Repeats_Weekly2?>";
		else
			newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatWeeklyRange.value+"<?=$iCalendar_NewEvent_Repeats_Weekly3?>";
		<?
			for($i=0; $i<sizeof($iCalendar_NewEvent_Repeats_WeekdayArray); $i++) {
		?>
		if (document.form1.<?=$iCalendar_NewEvent_Repeats_WeekdayArray[$i]?>.checked) {
			if (newMsg.match("day"))
				newMsg += ", <?=$iCalendar_NewEvent_Repeats_WeekdayArray2[$i]?>";
			else
				newMsg += "<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[$i]?>";
		}
		<?
			}
		?>
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		break;
	case "MONTHLY":
		var monthlyRepeatBy0 = document.getElementById("monthlyRepeatBy0");
		var monthlyRepeatBy1 = document.getElementById("monthlyRepeatBy1");
		
		if (document.form1.repeatMonthlyRange.value == "1")
			newMsg = "<?=$iCalendar_NewEvent_Repeats_Monthly2?>";
		else
			newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatMonthlyRange.value+" <?=$iCalendar_NewEvent_Repeats_Monthly5?>";
		
		if (monthlyRepeatBy0.checked)
			newMsg += " <?=$iCalendar_NewEvent_DurationDay?> "+eventDateObj.getDate();
		if (monthlyRepeatBy1.checked) {
			var nth = 0;
			for(var j=1; j<6; j++) {
				if (eventDateObj.getDate() == NthDay(j, eventDateObj.getDay(), eventDateObj.getMonth(), eventDateObj.getFullYear())) {
					nth = j;
					break;
				}
			}
			var weekDayName = new Array("<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[0]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[1]?>",
						"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[2]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[3]?>",
							"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[4]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[5]?>",
							"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[6]?>");
			var countName =  new Array("<?=$iCalendar_NewEvent_Repeats_Count[0]?>", "<?=$iCalendar_NewEvent_Repeats_Count[1]?>", 
							"<?=$iCalendar_NewEvent_Repeats_Count[2]?>", "<?=$iCalendar_NewEvent_Repeats_Count[3]?>", 
							"<?=$iCalendar_NewEvent_Repeats_Count[4]?>");
			newMsg += countName[nth-1] + " " + weekDayName[eventDateObj.getDay()];
		}
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		break;
	case "YEARLY":
		if (document.form1.repeatYearlyRange.value == "1")
			newMsg = "<?=$iCalendar_NewEvent_Repeats_Yearly2?>";
		else
			newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatYearlyRange.value+" <?=$iCalendar_NewEvent_Repeats_Yearly3?>";
		var monthname = new Array("<?=$iCalendar_NewEvent_Repeats_MonthArray[0]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[1]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[2]?>",
						"<?=$iCalendar_NewEvent_Repeats_MonthArray[3]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[4]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[5]?>",
						"<?=$iCalendar_NewEvent_Repeats_MonthArray[6]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[7]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[8]?>",
						"<?=$iCalendar_NewEvent_Repeats_MonthArray[9]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[10]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[11]?>");
		newMsg += monthname[eventDateObj.getMonth()]+" "+eventDateObj.getDate();
		if (repeatRangeEndNever1.checked)
			newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatRangeEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		break;
	}
}

function dayOfWeek(day,month,year) {
    var dateObj = new Date();
    dateObj.setFullYear(year,month,day);
    return dateObj.getDay();
}

function isLeapYear(year) {
  return new Date(year,2-1,29).getDate()==29;
}

function NthDay(nth,weekday,month,year) {
	var daysofmonth   = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var daysofmonthLY = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    if (nth > 0)
    	return (nth-1)*7 + 1 + (7+weekday-dayOfWeek((nth-1)*7+1, month, year))%7;
    if (isLeapYear(year))
    	var days = daysofmonthLY[month];
    else
    	var days = daysofmonth[month];
    return days - (dayOfWeek(days,month,year) - weekday + 7)%7;
}