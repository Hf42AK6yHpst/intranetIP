// page editing by : kenneth chung
String.prototype.Trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.LTrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.RTrim = function() {
	return this.replace(/\s+$/,"");
}
String.prototype.count = function(match) {
var res = this.match(new RegExp(match,"g"));
if (res==null) { return 0; }
return res.length;
}


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function RemoveHTMLSpecialChar(str)
{
	var returnStr = str.replace(/&quot;/g,'"');
	returnStr = returnStr.replace(/&amp;/g,"&");
	returnStr = returnStr.replace(/&lt;/g,"<");
	returnStr = returnStr.replace(/&gt;/g,">");
	
	return returnStr;
}

function Confirm_Forward_Page(Msg,Page){
	if (confirm(Msg)) {
		window.location = Page;
	}
}

function Confirm_Submit_Page(Msg,FormName,TargetPage){
	var Obj = eval("document."+FormName);
	if (confirm(Msg)) {
		Obj.action = TargetPage;
		Obj.submit();
	}
	else {
		return false;
	}
}

function Submit_Page(FormName,TargetPage){
	var Obj = eval("document."+FormName);
	
	Obj.action = TargetPage;
	Obj.submit();
}

// checkbox checking function for mail and db table
function setChecked(val, obj, element_name){
	/*
		len=obj.elements.length;
		var i=0;
		for( i=0 ; i<len ; i++) {
				if (obj.elements[i].name==element_name)
				obj.elements[i].checked=val;
		}
		*/
	var theElements = document.getElementsByName(element_name);
	var len = theElements.length;
	var i=0;
	for( i=0 ; i<len ; i++) {
		if(theElements[i].form == obj && theElements[i].disabled == false){
			theElements[i].checked=val;
		}
	}
}

// check for selection before submit
function Check_Select(Obj,ElementToAffect) {
	var selected = false;
	
	for (var i=0; i< Obj.length; i++) {
		if (Obj[i].checked) selected = true;
	}
	
	if (selected) {
		for (var i=0; i< ElementToAffect.length; i++) {
			for (var j=0; j< ElementToAffect[i].length; j++) {
				ElementToAffect[i][j].disabled=false;
			}
		}
		return true;
	}
	else {
		for (var i=0; i< ElementToAffect.length; i++) {
			for (var j=0; j< ElementToAffect[i].length; j++) {
				ElementToAffect[i][j].disabled=true;
			}
		}
		return false;
	}
}

// sort column checking function for mail and db table
function sortPage(a, b, obj, pageNo, e){
		var CallAjax = e || false;
		obj.order.value=a;
		obj.field.value=b;
		obj.pageNo.value=pageNo;
		
		if (!CallAjax)
			obj.submit();
		else {
			order = a;
			field = b;
			pageNo = this.pageNo;
			Right_Panel_Ajax();
		}
}

function Check_Box_Checked(element_name) {
	var theElements = document.getElementsByName(element_name);
	var len = theElements.length;
	var i=0;
	for( i=0 ; i<len ; i++) {
		if(theElements[i].checked) {
			return true;
		}
	}
	
	return false;
}

// get onclick object position
function getPostion(obj, direction){
	var objStr = "obj";
	var pos_value = 0;

	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function GetXmlHttpObject()
{
  var xmlHttp=null;
  try
  {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
    {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  
  return xmlHttp;
} 

var ReturnID = "";
var SelectedEmailID = -1;
var EmailListFocus = false;
function AutoComplete_Contact(InputValue, SearchKey, e, page, ReturnAddressType, AddressList)
{
	var TargetID = "";
	
	if (!e) var e = window.event;

	if (ReturnAddressType == "")
	{
		TargetID = "ToAddress";
		ReturnID = "to_list";
	}
    else if (ReturnAddressType == "CC")
    {
	    TargetID = "CcAddress";
    	ReturnID = "cc_list";
	}
    else if (ReturnAddressType == "BCC")
    {
	    TargetID = "BccAddress";
    	ReturnID = "bcc_list";
	}
		
	if (e.keyCode == 40 || e.keyCode == 38)
	{
		var PrevSelectedEmailID = SelectedEmailID;

		if (e.keyCode == 40)
		{
			if (document.getElementById(AddressList+(SelectedEmailID+1)))
			{
				++SelectedEmailID;
			}
			else
			{
				return false;
			}
		}
		else if (e.keyCode == 38)
		{
			if (document.getElementById(AddressList+(SelectedEmailID-1)))
			{
				--SelectedEmailID;
			}
			else
			{
				return false;
			}
		}
		
		HighlightAddress(AddressList, PrevSelectedEmailID, SelectedEmailID);
		
		return false;
	}
	else if (e.keyCode == 13 || e.keyCode == 9)
	{
		if (document.getElementById(AddressList+SelectedEmailID))
		{
			if (document.getElementById(AddressList+SelectedEmailID).getAttribute("isGroup") == "1")
			{
				AddGroupAddress(TargetID, ReturnID, AddressList, SelectedEmailID);
			}
			else
			{
				AddEmailAddress(TargetID, ReturnID, AddressList, SelectedEmailID);
			}
		}
		return false;
	}
	
	var KeyValue = InputValue.substring(InputValue.lastIndexOf(";")+1).LTrim();
	
	if (KeyValue.length == 0)
    {
		document.getElementById(ReturnID).style.display = "none"; 
		return;
    }

    wordXmlHttp = GetXmlHttpObject();
    
    if (wordXmlHttp == null)
    {
      alert (errAjax);
      return;
    } 
    
    var url = page;
    var postContent = SearchKey + "=" + KeyValue;
    postContent += "&ReturnType=" + ReturnAddressType;

    wordXmlHttp.onreadystatechange = AutoComplete_Contact_Page_State_Changed;
    wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);    
	
	SelectedEmailID = -1;
}

function Check_Tab(e, ReturnAddressType)
{
	var TargetID = "";
	if (!e) var e = window.event;		
	
	if (ReturnAddressType == "")
	{
		TargetID = "ToAddress";
		ReturnID = "to_list";
	}
    else if (ReturnAddressType == "CC")
    {
	    TargetID = "CcAddress";
    	ReturnID = "cc_list";
	}
    else if (ReturnAddressType == "BCC")
    {
	    TargetID = "BccAddress";
    	ReturnID = "bcc_list";
	}

	return !((document.getElementById(ReturnID).style.visibility == "visible") && (e.keyCode==9));
	
	/*if (e.keyCode==9)
	{
		return false;	
	}*/
}

function AutoComplete_Contact_Page_State_Changed()
{ 
	var DivName = "";
	
    if (wordXmlHttp.readyState==4)
    {
    	if (ReturnID == "to_list")
    	{
    		DivName = "added_item_ppl_list";
		}
    	else if (ReturnID == "cc_list")
    	{
    		DivName = "added_item_ppl_list_CC";
		}
    	else if (ReturnID == "bcc_list")
    	{
	    	DivName = "added_item_ppl_list_BCC";
    	}
    	
    	if (wordXmlHttp.responseText.length < 5) //to handle hidden white characters
    	{
	    	document.getElementById(ReturnID).style.visibility = "hidden";
	    	document.getElementById(ReturnID).style.display = "none";
	    	document.getElementById(DivName).innerHTML = "";
    	}
    	else
    	{
	    	document.getElementById(ReturnID).style.visibility = "visible";
	    	document.getElementById(ReturnID).style.display = "inline";
	    	
	      	document.getElementById(DivName).innerHTML = wordXmlHttp.responseText;
  		}
    }
}

function AddAddress(InputName, InputValue) {
	var Obj =  document.getElementById(InputName);
	
	Obj.value += InputValue + ';';
}

function HideDiv(DivID)
{
	if (!EmailListFocus)
	{
		document.getElementById(DivID).style.visibility = "hidden";
		document.getElementById(DivID).style.display = "none";
	}
}

function AddEmailAddress(TargetID, HideDivID, AddressListID, AddressID)
{
	var Obj =  document.getElementById(TargetID);

	if (Obj.value.lastIndexOf(";") == -1)
	{
		Obj.value = RemoveHTMLSpecialChar(document.getElementById(AddressListID+AddressID).innerHTML) + "; ";
	}
	else
	{
		Obj.value = Obj.value.substring(0, Obj.value.lastIndexOf(";")+1);
		Obj.value += " " + RemoveHTMLSpecialChar(document.getElementById(AddressListID+AddressID).innerHTML) + "; ";
	}
	
	HideDiv(HideDivID);
	//document.getElementById(TargetID).focus();
	SelectedEmailID = -1;
}

function AddGroupAddress(TargetID, HideDivID, AddressListID, AddressID)
{
	var Obj =  document.getElementById(TargetID);

	var FirstNum = document.getElementById(AddressListID+AddressID).getAttribute("realContent").indexOf("[");
	var LastNum = document.getElementById(AddressListID+AddressID).getAttribute("realContent").lastIndexOf("]");
	var TmpValue = document.getElementById(AddressListID+AddressID).getAttribute("realContent").substring(FirstNum+1, LastNum);
		
	if (Obj.value.lastIndexOf(";") == -1)
	{
		if(TmpValue == "")
			Obj.value = "";
		else
			Obj.value = RemoveHTMLSpecialChar(TmpValue)+ "; ";		
	}
	else
	{		
		Obj.value = Obj.value.substring(0, Obj.value.lastIndexOf(";")+1);
		if(TmpValue != "")
			Obj.value += " " + RemoveHTMLSpecialChar(TmpValue) + "; ";
	}
	
	HideDiv(HideDivID);
	//document.getElementById(TargetID).focus();
	SelectedEmailID = -1;
}

function HighlightAddress(AddressLi, PrevAddressID, AddressID)
{
	ResetAddressHighlight(AddressLi, PrevAddressID);
	if (document.getElementById(AddressLi+AddressID))
	{
		document.getElementById(AddressLi+AddressID).style.background = "#5E85BF";
		SelectedEmailID = AddressID;
	}
	else
	{
		SelectedEmailID = -1;
	}
}

function ResetAddressHighlight(AddressLi, AddressID)
{
	if (document.getElementById(AddressLi+AddressID))
	{
		document.getElementById(AddressLi+AddressID).style.background = "#FFFFFF";
	}
}

function NavigateAddress(AddressList, e)
{
	return true; //temporarily disable this function
	
	if (!e) var e = window.event;
	
	if (e.keyCode == 9)
	{
		return false;
	}
	
	if (e.keyCode == 40 || e.keyCode == 38)
	{
		var PrevSelectedEmailID = SelectedEmailID;
		
		if (e.keyCode == 40)
		{
			if (document.getElementById(AddressList+(SelectedEmailID+1)))
			{
				++SelectedEmailID;
			}
			else
			{
				return false;
			}
		}
		else if (e.keyCode == 38)
		{
			if (document.getElementById(AddressList+(SelectedEmailID-1)))
			{
				--SelectedEmailID;
			}
			else
			{
				return false;
			}
		}

		HighlightAddress(AddressList, PrevSelectedEmailID, SelectedEmailID);
		
		return false;
	}
}

function Select_All(Obj,CheckStatus) {
	if (Obj.type == "select-multiple") {
		for (var i = 0; i < Obj.options.length; i++) {
			Obj.options[i].selected = CheckStatus.checked;
		}
	}
}

function newWindow(url, winType) 
{
	var newWin;
    win_name = "print_popup";
        
    if(winType==0)
    {
    	win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=700,height=400";
   	}
   	if(winType==1)
    {
    	win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=900,height=400";
   	}
   	
    if (navigator.appName=="Microsoft Internet Explorer" && navigator.appVersion >="4")
    if (newWin!=null) newWin.close();
    newWin = window.open (url, win_name, win_size);
    if (navigator.appName=="Netscape" && navigator.appVersion >= "3") newWin.focus();
} // end function new window popup

// validated Email pattern
function Validate_Email(EmailValue){
	var re;
	var FirstDoubleQuote = EmailValue.indexOf("\"");
	
	if (FirstDoubleQuote == -1) {
		if (EmailValue.indexOf("<") == -1 && EmailValue.indexOf(">") == -1) {
			re = /^.+@.+\..{2,3}$/;
		}
		else {
			re = /^<.+@.+\..{2,3}>$/;
		}
		
		if (re.test(EmailValue.Trim())) {
			return true;
	  }
	  else{
			return false;
	  }
	}
	else {
		var ReWithDisplayName = /^(\".+\") <.+@.+\..{2,3}>$/;
		if (ReWithDisplayName.test(EmailValue.Trim())) {
			return true;
	  }
	  else{
			return false;
	  }
	}
}

// auto resize iframe
function Resize_Iframe(IframeName) {
	scroll_size = 10;
	iframeObj = this.parent.document.getElementById(IframeName);
	//if(iframeObj==null) return;
	/*ch= this.document.body.clientHeight;
	cw = this.document.body.clientWidth;*/
	// Firefox 
	if( window.innerHeight && window.scrollMaxY ) {
		pageWidth = window.innerWidth + window.scrollMaxX;
		pageHeight = window.innerHeight + window.scrollMaxY;
	}
	// all but Explorer Mac
	else if( document.body.scrollHeight > document.body.offsetHeight ) {
		pageWidth = document.body.scrollWidth;
		pageHeight = document.body.scrollHeight;
	}
	// works in Explorer 6 Strict, Mozilla (not FF) and Safari
	else {
		pageWidth = document.body.offsetWidth + document.body.offsetLeft; 
		pageHeight = document.body.offsetHeight + document.body.offsetTop; 
	}
			
	/*this.scroll(scroll_size,scroll_size);	
	while(this.document.body.scrollTop>0 || this.document.body.scrollLeft > 0 || ch < 640)
	{
		ch+=scroll_size;
		cw+=scroll_size;
		this.scroll(scroll_size,scroll_size);
	}	*/	
	
	/*if (pageHeight > 640) {
		iframeObj.style.height = 640;	
		//iframeObj.style.width = pageWidth;
	}
	else {*/
	if (pageHeight < 500)
		iframeObj.style.height = pageHeight + 300;	
	else
		iframeObj.style.height = pageHeight;	
		//iframeObj.style.width = pageWidth;
	//}
	
	setTimeout("Resize_Iframe('"+IframeName+"');",1000*3);
}

// date format should be dd-mm-yyyy
function jCheckDateValidation(dateVal, dateFormat)
{
   // Regular expression used to check if date is in correct format
   var pattern = null;
   if(dateFormat == "dd/mm/yy")
   pattern = new RegExp([0-3][0-9]/0|1[0-9]/19|20[0-9]);
   else
   pattern = new RegExp([0-3][0-9]-0|1[0-9]-19|20[0-9]);
   
   if(dateVal.match(pattern))
   {
	   var date_array = new Array();
	    
      if(dateFormat == "dd/mm/yy")
	  date_array = dateVal.split('/');
	  else
      date_array = dateVal.split('-');
      
      var day = date_array[0];

      // Attention! Javascript consider months in the range 0 - 11
      var month = date_array[1] - 1;
      var year = date_array[2];

      // This instruction will create a date object
      source_date = new Date(year,month,day);

      if(year != source_date.getFullYear())
      {
         //alert('Year is not valid!');
         //$("#bulletin_alert_div").html($.ajax({url: file, async: false}).responseText);
         return false;
      }

      if(month != source_date.getMonth())
      {
         //alert('Month is not valid!');
         //$("#bulletin_alert_div").html($.ajax({url: file, async: false}).responseText);
         return false;
      }

      if(day != source_date.getDate())
      {
         //alert('Day is not valid!');
         //$("#bulletin_alert_div").html($.ajax({url: file, async: false}).responseText);
         return false;
      }
   }
   else
   {
	  //alert("Date format is not valid");
	  //$("#bulletin_alert_div").html($.ajax({url: file, async: false}).responseText);
      return false;
   }

   return true;
} // check date validation

function jDateDifference(FromDate, ToDate)
{
	var days = 0;
	var difference = 0;

    var str1  = FromDate;
    var str2  = ToDate;
    var dt1   = parseInt(str1.substring(0,2),10); 
    var mon1  = parseInt(str1.substring(3,5),10);
    var yr1   = parseInt(str1.substring(6,10),10);
    var dt2   = parseInt(str2.substring(0,2),10); 
    var mon2  = parseInt(str2.substring(3,5),10); 
    var yr2   = parseInt(str2.substring(6,10),10); 
    var date1 = new Date(yr1, mon1, dt1); 
    var date2 = new Date(yr2, mon2, dt2); 
    
	difference = date2 - date1;
	days = Math.round(difference/(1000*60*60*24));
	
	return days;
} // end function j date difference

function SessionExpired(msg)
{
	if (msg == SessionExpiredMsg)
	{
		window.location = "/login.php?Msg=" + SessionExpiredWarning;
	}
	else if (msg == SessionKickedMsg)
	{
		window.location = "/login.php?Msg=" + SessionKickedWarning;
	}
	
	return false;
}

// use by address book - school group
function jCheck_InputEmail(obj)
{
	var ContactList = obj.ToAddress.value;
	var ContactArr = new Array();
	
	/*if (ContactList.lastIndexOf(";") == (ContactList.length-1)) 
		ContactList = ContactList.substr(0,ContactList.length-1);
	alert(ContactList);*/
	ContactArr = ContactList.split(";");
	
	for(var i = 0; i < ContactArr.length; i++)
	{
		if (ContactArr[i].Trim() != "") {
			if(ContactArr[i].indexOf('" <') > 0 && ContactArr[i].charAt(0) == '"' && ContactArr[i].charAt(ContactArr[i].length-1) == '>')
			{
				var TmpContact = ContactArr[i].split("<");
				var TmpName = TmpContact[0];
				var TmpEmail = TmpContact[1].replace(/>/,"");
			
				if(TmpName == "" || TmpEmail == "")
				return false;
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

function Parent_To_Top() {
	if (window.parent) {
	 	var ParentLocation = window.parent.document.location.toString();
	 	
	 	if (ParentLocation.indexOf("#") == (ParentLocation.length-1)) 
	 		window.parent.document.location = ParentLocation; 
	 	else 
			window.parent.document.location = ParentLocation + '#';
	}
	else {
		var Location = document.location.toString();
		
		if (location.indexOf("#") == (location.length-1)) 
	 		document.location = Location; 
	 	else 
			document.location = Location + '#';
	}
}

function Get_Form_Values(fobj) {
   var str = "";
   var valueArr = null;
   var val = "";
   var cmd = "";
   
   for(var i = 0;i < fobj.elements.length;i++) {
   	 //alert(fobj.elements[i].name + ": " + fobj.elements[i].type);
     switch(fobj.elements[i].type) {
       case "text":
            str += fobj.elements[i].name +
             "=" + encodeURIComponent(fobj.elements[i].value) + "&";
             break;
       case "select-one":
            str += fobj.elements[i].name +
            "=" + fobj.elements[i].options[fobj.elements[i].selectedIndex].value + "&";
            break;
       case "hidden":
       			str += fobj.elements[i].name +
             "=" + encodeURIComponent(fobj.elements[i].value) + "&";
             break;
       case "checkbox":
       case "radio":
       			if (fobj.elements[i].checked) {
       				str += fobj.elements[i].name + "=" + escape(fobj.elements[i].value) + "&";
       			}
       			 break;
       case "select-multiple":
       			for (var j=0; j< fobj.elements[i].length; j++) {
       				if (fobj.elements[i].options[j].selected) {
       					str += fobj.elements[i].name +
            		"=" + fobj.elements[i].options[j].value + "&";
       				}
       			}
       			 break;
     }
   }
   str = str.substr(0,(str.length - 1));
   
   return str;
}

function Get_Radio_Value(ElementName) {
	var Obj = document.getElementsByName(ElementName);
	var ReturnVal = "";
	
	for (var j=0; j< Obj.length; j++) {
		if (Obj[j].checked) {
			ReturnVal = Obj[j].value;
			break;
		}
	}
	
	return ReturnVal;
}

function Check_All_Check_Box(Condition,CheckBoxName) {
	var CheckboxObj = document.getElementsByName(CheckBoxName);
	
	for (var i=0; i < CheckboxObj.length; i++) {
		CheckboxObj[i].checked = Condition;
	}
}

function Get_Check_Box_Value(CheckBoxID) {
	if (document.getElementsByName(CheckBoxID)) {
		var CheckboxElement = document.getElementsByName(CheckBoxID);
		var ValueStr = '';
		for (var i=0; i< CheckboxElement.length; i++) {
			if (CheckboxElement[i].checked) 
				ValueStr += CheckboxElement[i].value + ',';
		}
		
		if (ValueStr != "") {
			ValueStr = ValueStr.substring(0,ValueStr.lastIndexOf(','));
			
			var ValueArray = ValueStr.split(',');
			
			return ValueArray;
		}
		else 
			return Array();
	}
	else
		return Array();
}

/*--------------------------- Event Reminder ---------------------------*/
var GlobalPathRelative;
//Retrieve Event IDs and set Timeout to Show the Events Title and Date+Time
function EventReminder(PathRelative)
{
	var xmlhttp=GetXmlHttpObject();
	if(xmlhttp==null || xmlhttp==false)
	{
		alert ("Your browser does not support XMLHTTP!");
		return;
	}
	var url=GlobalPathRelative+"src/module/calendar/event_reminder.php?action=check";
	url=url+"&r="+Math.random();
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var response=xmlhttp.responseText;
			if(response != "" && response.replace(/[\n\r\t\s]/g,"").length > 0)
			{
			
				var countExpired=0;
				var countFuture=0;
				var expiredIDs="";
				var futureIDs="";
				var futureTime=0;
				var eventsArray=new Array();
				eventsArray=response.split("@");
				
				for(var i=0;i<eventsArray.length;i++)
				{
					var eventSubArray=new Array();
					eventSubArray=eventsArray[i].split("#");
					if(eventSubArray.length>2)
					{
						if(eventSubArray[2].search("-") >= 0)//Expired event
						{
							countExpired+=1;
							expiredIDs=expiredIDs+";"+eventSubArray[0];
						
						}else if(eventSubArray[1].search("-") >= 0)//Reminder time expired but Event has not expired yet
						{
							countExpired+=1;
							expiredIDs=expiredIDs+";"+eventSubArray[0];
						
						}else//not expired and with same reminder time
						{
							futureTime=parseInt(eventSubArray[1])*1000;
							countFuture+=1;
							futureIDs=futureIDs+";"+eventSubArray[0];
						
						}
					}
				}
				if(countFuture>0)
				{
					futureIDs=futureIDs.replace(/[;]/,"");
					futureIDs=futureIDs.replace(/[\n\r\t]/g,"");
					window.setTimeout('ShowEvents("'+futureIDs+'");', futureTime);
				}
				if(countExpired > 0)
				{
					expiredIDs=expiredIDs.replace(/[;]/,"");
					expiredIDs=expiredIDs.replace(/[\n\r\t]/g,"");
					window.setTimeout('ShowEvents("'+expiredIDs+'");', 500);
				}
			}
		}
	};
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
}

function checkOption(obj){
        for(i=0; i<obj.length; i++){
                if(obj.options[i].value== ''){
                        obj.options[i] = null;
                }
        }
}

function checkOptionAll(obj){
        checkOption(obj);
        for(i=0; i<obj.length; i++){
                obj.options[i].selected = true;
        }
}

//Show Event(s) Title and Date+Time for both expired events and non-expired events
function ShowEvents(ReminderIDs)
{
	var xmlhttp=GetXmlHttpObject();
	if(xmlhttp==null || xmlhttp==false)
	{
		alert ("Your browser does not support XMLHTTP!");
		return;
	}
//	var url=PathRelative+"src/module/calendar/event_reminder.php?action=show";
//	url=url+"&id="+ReminderID;
//	url=url+"&r="+Math.random();
	var url=GlobalPathRelative+"src/module/calendar/event_reminder.php";
	var data="action=show&id="+ReminderIDs+"&r="+Math.random();
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var response=xmlhttp.responseText;
			if(response != "" && response.replace(/[\n\r\t\s]/g,"").length > 0)
			{
				PlaySound("AlertSound");
				var message="Event Reminder:\n";
				var eventsArray=new Array();
				eventsArray=response.split("@");
				for(var i=0;i<eventsArray.length;i++)
				{
					var eventSubArray=new Array();
					eventSubArray=eventsArray[i].split("#");
					if(eventSubArray.length>1)
					{
						var title=eventSubArray[0].replace(/[\n\r\t]/g,"");
						if(title.charAt(0)==" ") title=title.replace(/[\s]/,"");
					//	message=message+eventSubArray[0]+" - "+eventSubArray[1]+"\n";
						message=message+title+" - "+eventSubArray[1]+"\n";
					}
				}
				
				window.alert(message);
				window.setTimeout('EventReminder("'+GlobalPathRelative+'")', 0);
			}
			
		}
	};
	xmlhttp.open("POST",url);
	xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	xmlhttp.send(data);
}

/* 
Play a wav file when an event is raised by the Reminder 
@param SoundObjId : Wav Sound Object defined in <Object> Tag
*/
function PlaySound(SoundObjId)
{
	var soundObj=document.getElementById(SoundObjId);
	try
	{
  		soundObj.DoPlay();
	}catch(e)
	{
		try
		{
			soundObj.Play();
		}catch(e)
		{
			
		}
	}
}
/*---------------------------End Event Reminder----------------------------*/