<?php
// Editing by
/*
 * if upload file to client site before v5.1, need to check regional case [2020-0407-1445-44292]
 */

/**
 * **************************** Change Log [Start] *******************************
 *
 * 2020-05-04 Bill:  support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])    [2020-0407-1445-44292]
 *                   add param js function onSearchAndInsertUsers() in '/home/common_choose/user_selection_plugin_js.php'
 * 2019-11-28 Carlos: For DHL $sys_custom['DHL'], if user is eClass Teacher App admin, allow to choose all groups in target selection.
 * 2018-04-26 Isaac: Added support UserType=3(parent) to the selection menu,  modifided plugin's wordings accordingly
 * 2018-04-09 Isaac: seperated and modifided $userSelectedFieldId and $userSelectedFieldName
 *
 * ****************************** Change Log [End] ********************************
 */

$PATH_WRT_ROOT = "../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/json.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lu = new libuser();
$jsonObj = new JSON_obj();

$targetUserTypeAry = $jsonObj->decode($targetUserTypeAry);
$recipient_name_ary = $jsonObj->decode(stripslashes($recipient_name_ary));
$exclude_user_ary = $jsonObj->decode(stripslashes($exclude_user_ary));

// debug_pr($exclude_user_ary);
if(empty($recipient_name_ary)){
    $recipient_name_ary = array();
}
$permittedType = implode(",", $targetUserTypeAry);
if($exclude_user_ary){
    $exclude_user_id_list = implode(",", $exclude_user_ary);
}

// [2020-0407-1445-44292]
$extra_data_ary = $jsonObj->decode(stripslashes($extra_data_ary));
$isAudienceLimited = !empty($extra_data_ary) && in_array('filterClassTeacherClass', (array)$extra_data_ary) && in_array('filterOwnClassesOnly', (array)$extra_data_ary);

// ## Auto-complete ClassName ClassNumber StudentName search
$UserClassNameClassNumberInput = '';
$UserClassNameClassNumberInput .= '<div style="align:left;">';
$UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
$UserClassNameClassNumberInput .= '</div>';

// ## Auto-complete login search
$UserLoginInput = '';
$UserLoginInput .= '<div style="align:left;">';
$UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
$UserLoginInput .= '</div>';

$UserParentInput = '';
$UserParentInput.= '<div style="align:left;">';
$UserParentInput.= '<input type="text" id="ParentSearchTb" name="ParentSearchTb" value="" />';
$UserParentInput.= '</div>';

// ## Student Selection Box & Remove all Btn
$MemberSelectionBox = $linterface->GET_SELECTION_BOX($recipient_name_ary, "name=".$userSelectedFieldName." id=".$userSelectedFieldId." class='select_userList' size='15' multiple='multiple'", "");
$btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student('$userSelectedFieldId')");

$or = $Lang['General']['Or'];
$searchStudentTextfield = '<p>'.$Lang['CommonChoose']['SearchByInputFormat'] . '</p>' . $UserClassNameClassNumberInput;
$searchStaffTextfield = '<p>'.$Lang['Circular']['SearchStaffName'].'</p>' . $UserLoginInput;
$searchParentTextfield = '<p>'.$Lang['CommonChoose']['SearchParentByInputFormat'].'</p>' . $UserParentInput;
$textareaDiscription = $Lang['General']['msgSearchAndInsertInfo'];
$userTypeAry = array(1,2,3,4);
if (!in_array(4,(array)$targetUserTypeAry)){
    $excluded_type=4;
}
if(in_array(1,(array)$targetUserTypeAry)){
    $labelSelectFrom = $Lang['General']['FromGroup'];
    $searchtextfield.=  $searchStaffTextfield;
    $targetUserType = 'teacher';
}
if(in_array(2,(array)$targetUserTypeAry)){
    $labelSelectFrom = $i_general_from_class_group;
    $textareaDiscription = $Lang['AppNotifyMessage']['msgSearchAndInsertInfo'];
    $searchtextfield.=  $searchStudentTextfield;
    $targetUserType = 'student';
}
if(in_array(3,(array)$targetUserTypeAry)){
    $labelSelectFrom = $i_general_from_class_group;
    $searchtextfield.=  $searchParentTextfield;
    $targetUserType = 'student';
}
if ( count ( array_intersect($userTypeAry,(array)$targetUserTypeAry))>1){
    $labelSelectFrom = $Lang['General']['FromGroup'];
    $targetUserType = 'targetUser';
}

$extra_params = '';
if($sys_custom['DHL'] && $_SESSION['SSV_USER_ACCESS']['eAdmin-eClassTeacherApp']){
	$extra_params .= "&isAdmin=1";
}
// ## Choose Member Btn
$common_choose_parm = "fieldname=".$userSelectedFieldName."&permitted_type=".$permittedType."&exclude_user_id_list=".$exclude_user_id_list."&excluded_type=".$excluded_type."&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1$extra_params";
if($isAudienceLimited) {
    // [2020-0407-1445-44292] add 2 params passed to 'home/common_choose/index.php' to show teaching class students only
    $common_choose_parm = "fieldname=".$userSelectedFieldName."&permitted_type=".$permittedType."&exclude_user_id_list=".$exclude_user_id_list."&excluded_type=".$excluded_type."&DisplayGroupCategory=0&filterClassTeacherClass=1&filterOwnClassesOnly=1$extra_params";
}
//$btn_ChooseMember = $linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=".$userSelectedFieldName."&permitted_type=".$permittedType."&exclude_user_id_list=".$exclude_user_id_list."&excluded_type=".$excluded_type."&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1$extra_params', 16)");
$btn_ChooseMember = $linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?".$common_choose_parm."', 16)");

// ## Textarea input
$textareaInputTarget= '';
$textareaInputTarget.= '<div style="align:left;" class="'.$targetUserType.'_textarea">';
$textareaInputTarget.= '<textarea style="height:100px;" id="'.$targetUserType.'AreaSearchTb" name="'.$targetUserType.'AreaSearchTb"></textarea><div id="'.$targetUserType.'AreaSearchTb_Error" class="red"></div><br>';
$extra_parm = '';
if($isAudienceLimited) {
    // [2020-0407-1445-44292] add param passed to js function onSearchAndInsertUsers() to show teaching class students only
    $extra_parm = 'filterOwnClassesOnly';
}
$textareaInputTarget.= $linterface->GET_SMALL_BTN($Lang['General']['searchAndInsertUser'], "button", "onSearchAndInsertUsers(this,document.form1['".$targetUserType."AreaSearchTb'],document.form1['".$userSelectedFieldName."'], '".$jsonObj->encode($targetUserTypeAry)."', '".$extra_parm."')");
$textareaInputTarget.= '</div>';

$userSelectSession .= '<table>';
$userSelectSession .= '<tr>';



    $userSelectSession .= '                    <td class="tablerow2" valign="top">
 						                <table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td class="tabletext">' . $labelSelectFrom. '</td>
										</tr>
										<tr>
											<td class="tabletext">' . $btn_ChooseMember . '</td>
										</tr>
										<tr>
											<td class="tabletext"><i>' . $or . '</i></td>
										</tr>
										<tr>
											<td class="tabletext">'
											    .$searchtextfield.	
											'</td>
										</tr>
										</table>
                                        </td>
                                     <td class="tabletext" ><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1"></td>
									<td class="tablerow2">
										<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td><i>' . $or . '</i></td>
										</tr>
										<tr>
											<td class="tabletext">
												' . $textareaDiscription . '
												<br />
												' . $textareaInputTarget . '
											</td>
										</tr>
										</table>
									</td>
									<td class="tabletext" ><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1"></td>
									<td align="left" valign="top">
										<table width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td align="left">
												' . $MemberSelectionBox . '
											</td>
                                            <td valign="top">';
$userSelectSession .=                           $btn_RemoveSelected .' 
                                            </td>
										</tr>
										<tr>
											<td>
												' . $linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* ' . $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']) . '
											</td>
										</tr>
										</table>
									</td>
								</tr>';
$userSelectSession .= "</table>";

intranet_closedb();
echo $userSelectSession;

?>

