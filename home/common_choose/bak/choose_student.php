<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard_ui = new libreportcard_ui();
 
if ($lreportcard->hasAccessRight()) 
{
	$linterface = new interface_html();
	$CurrentPage = "Management_ClassTeacherComment";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	# tag information
	if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
	{
		$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 0);
		$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 1);
	}
	else
	{
		$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
	}
	
	$linterface->LAYOUT_START();
	
	$Keyword = stripslashes($_POST['Keyword']);
	$CommentIDArr = $_POST['CommentIDArr'];
	echo $lreportcard_ui->Include_JS_CSS();
	echo $lreportcard_ui->Get_Management_ClassTeacherComment_CommentView_ChooseStudent_UI($CommentIDArr, $Keyword);
// 	echo '<div id="divSelectStudent"></div>';
?>

<?php include_once ($PATH_WRT_ROOT . "home/common_choose/user_selection_plugin_js.php");?>
<script language="javascript">

var AutoCompleteObj_ClassNameClassNumber;
var AutoCompleteObj_UserLogin;
var jsCurYearID;
$(document).ready( function() {
	
	// initialize the report selection
	jsCurYearID = $('select#YearID').val();
	js_Reload_Selection(jsCurYearID);
	
	// initialize jQuery Auto Complete plugin
	var divId ='divSelectStudent'
	var targetUserTypeAry = [2]; 
 	var userSelectedField = 'SelectedUserIDArr[]'; 
 	var selectedUsersData = '';
 	Init_User_Selection_Menu(divId,targetUserTypeAry,userSelectedField,selectedUsersData);
});




function js_Reload_Selection(jsYearID)
{
	if (Get_Selection_Value('SelectedUserIDArr[]', 'Array', true) != '')
	{
		if (confirm("<?=$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedStudentWillBeRemovedIfChangedForm']?>"))
		{
			// remove selected students
			checkOptionAll(document.getElementById('SelectedUserIDArr[]'));
			checkOptionRemove(document.getElementById('SelectedUserIDArr[]'));
		}
		else
		{
			$('select#YearID').val(jsCurYearID);
			return false;
		}
	}
	
	jsCurYearID = jsYearID;
	
	// reload the report card selection
	$('#ReportSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../reports/ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID'
		},
		function(ReturnData)
		{
			Update_Auto_Complete_Extra_Para();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function js_Select_Student_Pop_up()
{
	var jsYearID = $('select#YearID').val();
	newWindow('choose_student_popup.php?YearID=' + jsYearID + '&fieldname=SelectedUserIDArr[]', 9);
}

function js_Remove_Selected_Student()
{
	checkOptionRemove(document.getElementById('SelectedUserIDArr[]'));
	Update_Auto_Complete_Extra_Para();
}

function js_Go_Back_To_Comment_List()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'index_comment.php';
	ObjForm.submit();
}

function js_Add_Comment_To_Student()
{
	checkOptionAll(document.getElementById("SelectedUserIDArr[]"));
	
	var objForm = document.getElementById('form1');
	objForm.action = 'add_comment_to_student.php';
	objForm.submit();
}
</script>


<?
    $linterface->LAYOUT_STOP();
    intranet_closedb();
}
?>