<?php
// Editing by 
/*
 *  Date:   2020-05-04  Bill    [2020-0407-1445-44292]
 *          support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
 *
 *  Date:   2018-07-11 Isaac
 *          added if statement to fix the issue of unable to search parents when $excludeUserAryStr is empty
 *          
 *  Date:   2018-04-26 Isaac
 *          added support searching Parents with user ID
 * 
 *  Date:	2018-03-26 	Isaac
 *	        modified string replace use single quotts instead od double quotes for handling special unicode space characters
 *  Date:	2018-02-23 	Isaac
 *	        Create this File with the reference of eCircular 	
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$search_text = rawurldecode($_POST['SearchText']);
$userSearchAry = explode("\n",$search_text);
$li = new libdb();
$jsonObj = new JSON_obj();
$targetUserTypeAry =$jsonObj->decode($jsonObj->decode(stripslashes($_POST['TargetUserAry'])));
$excludeUserAry = $jsonObj->decode(stripslashes($_POST['ExcludeUserAry']));
if($excludeUserAry){
    $excludeUserAryStr = "'".implode("','",$excludeUserAry)."'";
}

// [2020-0407-1445-44292]
$extra_data_ary = $jsonObj->decode(stripslashes($_POST['ExtraDataAry']));
$isAudienceLimited = !empty($extra_data_ary) && $extra_data_ary == 'filterOwnClassesOnly';

$user_search_ary = array();
for($i=0;$i<count($userSearchAry);$i++){
    $user_search = trim(str_replace(array("\t"," "), "", $userSearchAry[$i]));
	if($user_search != '') $user_search_ary[] = $li->Get_Safe_Sql_Query($user_search);
}

$name_field = getNameFieldByLang2("u.");
  
##sql setup
$staffWhereClause = "u.RecordStatus='1' AND u.RecordType='1' AND u.UserLogin IN ('".implode("','",$user_search_ary)."')";
$studentWhereClause = "u.RecordStatus='1' AND u.RecordType='2' AND (REPLACE(CONCAT(u.ClassName,u.ClassNumber),' ','') IN ('".implode("','",$user_search_ary)."') or u.UserLogin IN ('".implode("','",$user_search_ary)."')) ";
$parentWhereClause = "b.RecordStatus='1' AND b.RecordType='3' AND b.UserLogin IN ('".implode("','",$user_search_ary)."')";
$records=array();
if(in_array(1,$targetUserTypeAry) && in_array(2,$targetUserTypeAry)){
    $WhereClause = "(".$staffWhereClause.") OR (".$studentWhereClause.")";
}elseif(in_array(1,$targetUserTypeAry)){
    $WhereClause = $staffWhereClause;
}elseif(in_array(2,$targetUserTypeAry)){
    $WhereClause = $studentWhereClause;
}   
if($excludeUserAryStr){
    $WhereClause = $WhereClause.'AND u.UserID NOT IN ('.$excludeUserAryStr.')';
}
// [2020-0407-1445-44292] ensure own class students can be selected
if($isAudienceLimited)
{
    include_once($PATH_WRT_ROOT."includes/libnotice.php");
    $lnotice = new libnotice();
    $teaching_class_list = $lnotice->getClassTeacherCurrentTeachingClass();
    $teaching_class_names = Get_Array_By_Key($teaching_class_list, 'ClassName');

    $WhereClause = $WhereClause." AND u.ClassName IN ('".implode("', '", (array)$teaching_class_names)."')";
}
$sql = "SELECT u.UserID,$name_field as UserName,u.UserLogin, REPLACE(CONCAT(u.ClassName,'-',u.ClassNumber),' ','') as ClassNameNumber FROM INTRANET_USER as u WHERE ".$WhereClause." ORDER BY u.EnglishName ";
$records = $li->returnResultSet($sql);

//## search parents by login
if(in_array(3,$targetUserTypeAry)){
    $name_field = getNameFieldWithClassNumberByLang("b.");
    $name_field2 = getNameFieldByLang("c.");
    if($excludeUserAryStr){
    $WhereClause='('.$parentWhereClause.')AND b.UserID NOT IN ('.$excludeUserAryStr.')';
    } else{
        $WhereClause = $parentWhereClause;
    }
    $sql = "SELECT distinct
    (a.ParentID) as UserID,
    $name_field AS UserName,
    b.UserLogin,
    $name_field2 AS StudentName
    FROM
    INTRANET_PARENTRELATION as a
    inner join INTRANET_USER as b on (b.UserID=a.ParentID)
    inner join INTRANET_USER as c on (c.UserID=a.StudentID)
    WHERE ".
    $WhereClause."ORDER BY b.EnglishName";
    
    if($li->returnResultSet($sql)){
        $records = array_merge($records, $li->returnResultSet($sql));  
    }  
    
}
$json_string = $jsonObj->encode($records); 

echo $json_string;
// debug_pr($records);

intranet_closedb();
?>