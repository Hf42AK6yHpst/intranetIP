<?php
// using 
################## Change Log [Start] ##############
#	Date:	2018-02-23 	Isaac
#           Added $excludeUserIDsStr to filter excluded users from the result list
#
#	Date:	2018-02-23 	Isaac
#			Create this File with the reference of eCircular
################## Change Log [End] ################
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();
$jsonObj = new JSON_obj();

# Get data
$YearID = $_REQUEST['YearID'];
//$SearchValue = stripslashes(urldecode($_REQUEST['q']));
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$SelectedUserIDList = str_replace('U','',$SelectedUserIDList);
$SelectedUserIDListArr = explode(',',$SelectedUserIDList);
$SelectedUserIDsStr = implode("','",$SelectedUserIDListArr);
if($_GET['excludeUserIDAry']){
$excludeUserIDsStr  = str_replace(",","','",$_GET['excludeUserIDAry']);
}

$libdb = new libdb();
$SearchValue = $libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($_REQUEST['q']))));
$name_field = getNameFieldWithClassNumberByLang("b.");
$name_field2 = getNameFieldByLang("c.");
$sql = "SELECT distinct
            (a.ParentID),
            $name_field AS OutputName,
            $name_field2 AS StudentName
        FROM
            INTRANET_PARENTRELATION as a
            inner join INTRANET_USER as b on (b.UserID=a.ParentID)
            inner join INTRANET_USER as c on (c.UserID=a.StudentID) 
		WHERE 
			b.RecordStatus = 1
            AND c.RecordStatus = 1
			AND b.RecordType IN ( 3 )
			AND b.USERID NOT IN ('".$SelectedUserIDsStr."','".$excludeUserIDsStr."') 
			AND ( b.ChineseName like'%".$SearchValue."%' 
					or b.EnglishName like '%".$SearchValue."%') 
		Order by b.EnglishName asc
		Limit 5";
$result = $libdb->returnResultSet($sql);
// debug_pr($sql);
$returnString = '';
foreach((array)$result as $_ParentInfoArr){
	$_UserID = $_ParentInfoArr['ParentID'];	
	$_StudentName = $_ParentInfoArr['StudentName'];
	$_Name = $_StudentName.$Lang['General']['s'].'('.$_ParentInfoArr['OutputName'].')';

	$returnString .= $_Name.'|'.'|'.'U'.$_UserID."\n";
}
    
intranet_closedb();
echo $returnString;
?>