<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.sunny.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();
$title = '';
$url = '';

if ($boxMode == 0)
{	
	$libdb = new libdb();
	$sql = "select Title,URL, VisibleTo from INTRANET_CAMPUS_LINK where `LinkID` ='".$linkID."'";
	$result = $libdb->returnArray($sql);
	$title = $result[0]['Title'];
	$url = $result[0]['URL'];
	$visibleTo = $result[0]['VisibleTo'];
} else
{
	# default for new
	$visibleTo="ALL";
}

if ($visibleTo=="ALL" || strstr($visibleTo, "STAFF_T"))
{
	$VisibleToCheckedT = "checked='checked'";
}

if ($visibleTo=="ALL" || strstr($visibleTo, "STAFF_NT"))
{
	$VisibleToCheckedNT = "checked='checked'";
}

if ($visibleTo=="ALL" || strstr($visibleTo, "STUDENT"))
{
	$VisibleToCheckedS = "checked='checked'";
}

if ($visibleTo=="ALL" || strstr($visibleTo, "PARENT"))
{
	$VisibleToCheckedP = "checked='checked'";
}

echo '<table align="center" style="width:95%;height:95%">
	<tr><td valign="center" align="center">
		<input type="hidden" id="editMode" name="editMode" value="'.$boxMode.'">
		<input type="hidden" id="linkID" name="linkID" value="'.$linkID.'">
		<table id="CampusLinkInputField" align="center" style="width:92%" cellspacing="10px">
			<tr>
				<td class="form_field" valign="top"><div style="width:50px">'.$i_CampusLinkTitle.':</div></td>
				<td valign="top"><input class="tabletext" style="width:400px" type="text" id="linkTitle" name="linkTitle" value="'.$title.'"></td>
			</tr>
			<tr>
				<td class="form_field" valign="top">'.$i_CampusLinkURL.':</td>
				<td valign="top"><input style="width:400px" class="tabletext" type="text" id="linkURL" name="linkURL" value="'.$url.'"><!--<span class="tabletextremark">[eclass_key], [eclass_loginID] and [eclass_language] can be used for single-logon. e.g. http://website.school.com/singlesignon.php?ec_key=[eclass_key]&ec_id=[eclass_loginID]&ec_lang=[eclass_language]</span>--></td>
			</tr>
			<tr>
				<td class="form_field" valign="top" nowrap="nowrap">'.$Lang['CampusLink']['VisibleTo'].':</td>
				<td valign="top"><input type="checkbox" name="VisibleTo[]" id="VisibleToT" value="STAFF_T" '.$VisibleToCheckedT.' /><label for="VisibleToT"> '.$Lang['Identity']['TeachingStaff'] ."</label>" . 
							'<br /><input type="checkbox" name="VisibleTo[]" id="VisibleToNT" value="STAFF_NT" '.$VisibleToCheckedNT.' /><label for="VisibleToNT"> '. $Lang['Identity']['NonTeachingStaff'] ."</label>" .
							'<br /><input type="checkbox" name="VisibleTo[]" id="VisibleToS" value="STUDENT" '.$VisibleToCheckedS.' /><label for="VisibleToS"> '. $Lang['Identity']['Student'] ."</label>" .
							'<br /><input type="checkbox" name="VisibleTo[]" id="VisibleToP" value="PARENT" '.$VisibleToCheckedP.' /><label for="VisibleToP"> '.$Lang['Identity']['Parent'] ."</label>" .
							'<br /><span class="tabletextremark">'.$Lang['CampusLink']['VisibleToRemark'].'</span></td>
			</tr>
			<tr>
				<td colspan="2" align="center" style="border:0px;border-top:1px;border-style:dashed;border-color:#cccccc;padding-top:5px">
					<input id="CampusLinkAdd" name="CampusLinkAdd" type="button" class="formsubbutton"  onMouseOver="this.className=\'formsubbuttonon\'" onMouseOut="this.className=\'formsubbutton\'" onclick="submitCampusLink()" value="'.$button_submit.'">	
					<input id="CampusLinkCancel" name="CampusLinkCancel" type="button" class="formsubbutton"  onMouseOver="this.className=\'formsubbuttonon\'" onMouseOut="this.className=\'formsubbutton\'" onclick="js_Hide_ThickBox();hiddenMsgBlock()" value="'.$button_cancel.'">
				</td>
			</tr>
		</table>
	</td></tr></table>
	<script language="javascript">
		document.getElementById("linkTitle").focus();
		function submitCampusLink(){	
			loadType = (document.getElementById("editMode").value == 0?"edit":"insert");
			linkIDValue = document.getElementById("linkID").value;
			title = document.getElementById("linkTitle").value;
			url = document.getElementById("linkURL").value;
			if (title == "" ||url == ""){
				alert("'.$i_CampusLink_validate.'");
				return;
			}
			var VisibleToValues="";
			$checkedCheckboxes = $("input:checkbox[name=VisibleTo[]]:checked");
			$checkedCheckboxes.each(function () {
				VisibleToValues +=  $(this).val() +",";
				});
			
			temp = url.toLowerCase();
			if (!temp.match("^http://")&&!temp.match("^https://")&&!temp.match("^ftp://")&&!temp.match("^file://")){
				alert("'.$i_CampusLink_wrong_addr.'");
				return;
			}
			serverURL = "/home/campus_link/campus_link_load_content.php";
			$("div#campusLinkContent").ajaxStart(
				function(){
					$("span#ajaxMsgBlock").text("'.$ip20_loading.' ...");
					$("span#ajaxMsgBlock").css("display","block");				
				});
			$("div#campusLinkContent").ajaxError(
				function(){
					$("span#ajaxMsgBlock").text("'.$i_CampusLink_connect_fail.'");
					$("span#ajaxMsgBlock").css("display","block");
					setTimeout(hiddenMsgBlock,1000);
				});
			$("div#campusLinkContent").load(serverURL,{"loadType":loadType,"title":title,"url":url,"linkID":linkIDValue,"VisibleToValues":VisibleToValues},function(){
				$("span#ajaxMsgBlock").text("'.$i_con_gen_msg_add.'");
				$("span#ajaxMsgBlock").css("display","block");
				setTimeout(hiddenMsgBlock,1000);
				js_Hide_ThickBox();
			});
		}		
		$("span#ajaxMsgBlock").css("display","none");
		document.getElementById("TB_window").style.zIndex = 10000;
/*if (navigator.appName == "Microsoft Internet Explorer")	
	document.getElementById("TB_closeWindowButton").attachEvent("onclick", function(){js_Hide_ThickBox();hiddenMsgBlock()});	
else	document.getElementById("TB_closeWindowButton").addEventListener("click",function(){js_Hide_ThickBox();hiddenMsgBlock()},false)*/
	</script>
	';
intranet_closedb();
?>