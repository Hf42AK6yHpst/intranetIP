<?php
// using 
################## Change Log [Start] ##############
#	Date:	2018-02-23 	Isaac
#           Added $excludeUserIDsStr to filter excluded users from the result list
#
#	Date:	2018-02-23 	Isaac
#			Create this File with the reference of eCircular
################## Change Log [End] ################
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();
$jsonObj = new JSON_obj();

# Get data
$YearID = $_REQUEST['YearID'];
//$SearchValue = stripslashes(urldecode($_REQUEST['q']));
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$SelectedUserIDList = str_replace('U','',$SelectedUserIDList);
$SelectedUserIDListArr = explode(',',$SelectedUserIDList);
$SelectedUserIDsStr = implode("','",$SelectedUserIDListArr);
if($_GET['excludeUserIDAry']){
$excludeUserIDsStr  = str_replace(",","','",$_GET['excludeUserIDAry']);
}

$libdb = new libdb();
$SearchValue = $libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($_REQUEST['q']))));
$sql = "SELECT  
			UserID, 
			".getNameFieldByLang()." as Name, 
			ClassName, 
			ClassNumber 
		FROM 
			INTRANET_USER 
		WHERE 
			RecordStatus = 1
			AND RecordType IN ( 1 )
			AND USERID NOT IN ('".$SelectedUserIDsStr."','".$excludeUserIDsStr."')
			AND ( ChineseName like'%".$SearchValue."%' 
					or EnglishName like '%".$SearchValue."%') 
		Order by EnglishName asc
		Limit 5";
$result = $libdb->returnResultSet($sql);

$returnString = '';
foreach((array)$result as $_StudentInfoArr){
	$_UserID = $_StudentInfoArr['UserID'];
	$_Name = $_StudentInfoArr['Name'];
	$_ClassName = $_StudentInfoArr['ClassName'];
	$_ClassNumber = $_StudentInfoArr['ClassNumber'];
	if($_ClassName != '' && $_ClassNumber != ''){
		$displayClass = ' ('.$_ClassName.'-'.$_ClassNumber.')';
	}
	$_thisUserNameWithClassAndClassNumber = $_Name.$displayClass;

	$returnString .= $_thisUserNameWithClassAndClassNumber.'|'.'|'.'U'.$_UserID."\n";
}

intranet_closedb();
echo $returnString;
?>