<?php
/**
 * **************************** Change Log [Start] *******************************
 * 2020-05-04 Bill:  support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])    [2020-0407-1445-44292]
 *                   modified Init_User_Selection_Menu(), onSearchAndInsertUsers()
 * 2019-04-29 Cameron: fix js error: don't apply checkOptionAll() and checkOptionRemove() to object that doesn't exist 
 * 2018-12-24 Isaac: fixed Init_User_Selection_Menu() handling passing empty parameters.
 * 2018-05-07 Isaac: added support to exclude userId from selection.
 * 2018-04-25 Isaac: added displying loading_image before the plugin fully loaded in Init_User_Selection_Menu()
 * 2018-04-25 Isaac: modifided onSearchAndInsertUsers(), added condition to support checking and insert parent user.
 * 2018-04-25 Isaac: modifided Init_User_Selection_Menu() where  Selected_Users_Data_Ary=""
 * 2018-03-26 Isaac: modifided not_found_values in onSearchAndInsertUsers() to show only the not_found_values.
 * 2018-03-27 Isaac: Added clear values to textArea when no Not found users.
 * ****************************** Change Log [End] ********************************
 */

include_once ($PATH_WRT_ROOT . "includes/json.php");
$jsonObj = new JSON_obj();
if (!isset($linterface))
{
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
    $linterface = new interface_html("");
}


// /* example of selected users data array */
// // Array
// // (
// //     [0]  Array
// //         (
// //             [0] =&gt; U26344
// //             [UserID] = U26344
// //             [1] = Isaac_s1 ~!@#$%^&amp;*()_+{}:
// //             [UserName] = Isaac_s1 ~!@#$%^&amp;*()_+{}:
// //         )

// //     [1]  Array
// //         (
// //             [0] = U27048
// //             [UserID] = U27048
// //             [1] = isaac cheng
// //             [UserName] = isaac cheng
// //         )

// // )
// /* example to add to your page's $(document).ready */
// // $(document).ready( function() {
// // 	var divId ='divSelectTarget'
// // 	var targetUserTypeAry = [1,2];
// // 	var userSelectedFieldName = 'target_PIC';
// // 	var userSelectedFieldId = 'target_PIC';
//	var selectedUsersData = <?php echo $jsonObj->encode($ExistUserOption)? >;
//  var excludeUserAry = <?php echo $jsonObj->encode($groupMemberUserIdAry)? >;
// 	Init_User_Selection_Menu(divId,targetUserTypeAry,userSelectedFieldName, userSelectedFieldId, selectedUsersData);
// });
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT;?>templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT;?>templates/jquery/jquery.autocomplete.css" type="text/css" />
 
<script language="javascript"> 
var AutoCompleteObj_staff;
var AutoCompleteObj_student;
var AutoCompleteObj_parent;
var excludeUserAry;
var userSelectdFieldId;

// define user selection plugin
function Init_User_Selection_Menu(Div_Id, User_Type_Ary, User_Select_Field_Name, User_Select_Field_Id, Selected_Users_Data_Ary, Exclude_User_Ary, Extra_Data_Ary){
	Selected_Users_Data_Ary = Selected_Users_Data_Ary || '';
	Exclude_User_Ary = Exclude_User_Ary || '';
	excludeUserAry = Exclude_User_Ary;
	userSelectdFieldId = User_Select_Field_Id;
    Extra_Data_Ary = Extra_Data_Ary || '';
	$('#'+Div_Id).html(loading_image);
	var loading_image = '<?=$linterface->Get_Ajax_Loading_Image()?>';
	$('#'+Div_Id).html(loading_image);
	
// 	document.getElementById('Btn_Submit').onclick = function(){
// 		 $("#"+userSelectdFieldId+" option").each(function(){
// 			 alert($.inArray($(this).val().replace('U','')));
// 		       if($.inArray($(this).val().replace('U',''),excludeUserAry)){
// 		    	   $(this).remove();
// 		       }
// 		    });
// 	}

	$('#'+Div_Id).load(
    		'<?=$PATH_WRT_ROOT;?>home/common_choose/user_selection_menu.php',
    		{
    		    targetUserTypeAry: JSON.stringify(User_Type_Ary),
                userSelectedFieldName: User_Select_Field_Name,
                userSelectedFieldId: User_Select_Field_Id,
                recipient_name_ary: JSON.stringify(Selected_Users_Data_Ary),
                exclude_user_ary: JSON.stringify(Exclude_User_Ary),
                extra_data_ary: JSON.stringify(Extra_Data_Ary)
    		},
    		function (data){
    				// initialize jQuery Auto Complete plugin
				var Target_Users;
				if(User_Type_Ary.includes(3)){
    				var SearchTb = 'ParentSearchTb';
    				Target_Users = 3;
    				Init_JQuery_AutoComplete(SearchTb, Target_Users, User_Select_Field_Id);
    			}
    			if(User_Type_Ary.includes(2)){
    				var SearchTb = 'UserClassNameClassNumberSearchTb';
    				Target_Users = 2;
    				Init_JQuery_AutoComplete(SearchTb, Target_Users, User_Select_Field_Id);
    			}
    			if(User_Type_Ary.includes(1)){
    				var SearchTb = 'UserLoginSearchTb';
    				Target_Users = 1;
    				Init_JQuery_AutoComplete(SearchTb, Target_Users, User_Select_Field_Id);
    			}
    			$('.form_table_v30 tr td tr td').css({"border-bottom":"0px"})
    		}
    );
}

function Init_JQuery_AutoComplete(Input_Search_Field, Target_User_Type, Target_Select_Field_Id) {
	if(Target_User_Type==3){
		var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_parent.php";
	} else if(Target_User_Type==2){
		var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_user_by_classname_classnumber.php";
	} else if(Target_User_Type==1){
		var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_user.php";
	}
	var AutoCompleteObj =$("input#" + Input_Search_Field).autocomplete(
			ajaxUrl,
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, Input_Search_Field, Target_Select_Field_Id);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 100,
			minChars: 1,
			delay: 0,
			width: 200,
			
		}
	);
	if(Target_User_Type==3){
		AutoCompleteObj_parent=AutoCompleteObj;
	}else if(Target_User_Type==2){
		AutoCompleteObj_student = AutoCompleteObj;
	}
	else if(Target_User_Type==1){
		AutoCompleteObj_staff = AutoCompleteObj;
	}
	Update_Auto_Complete_Extra_Para(Target_Select_Field_Id);
}

function Add_Selected_User(UserID, UserName, InputID, Target_Select_Field_Id) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	
	var UserSelected = document.getElementById(Target_Select_Field_Id);
	if($("#"+Target_Select_Field_Id+" option[value='"+UserID+"']").length == 0){
		if (UserSelected) {
			UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
		}
		Update_Auto_Complete_Extra_Para(Target_Select_Field_Id);
	}
	
	
	// reset and refocus the textbox
	$('input#' + InputID).val('').focus();
}

function Update_Auto_Complete_Extra_Para(Target_Select_Field_Id){
	if (document.getElementById(Target_Select_Field_Id)) {
		checkOptionAll(document.getElementById(Target_Select_Field_Id));
	}
	ExtraPara = new Array();
	ExtraPara['SelectedUserList'] = Get_Selection_Value(Target_Select_Field_Id, 'Array', true);
	ExtraPara['excludeUserIDAry'] = excludeUserAry;
	if(AutoCompleteObj_staff != null){
	AutoCompleteObj_staff[0].autocompleter.setExtraParams(ExtraPara);
	}
	if(AutoCompleteObj_student != null){
	AutoCompleteObj_student[0].autocompleter.setExtraParams(ExtraPara);
	}
	if(AutoCompleteObj_parent != null){
		AutoCompleteObj_parent[0].autocompleter.setExtraParams(ExtraPara);
	}
	
}

function js_Remove_Selected_Student(Target_Select_Field_Id){
	if (document.getElementById(Target_Select_Field_Id)) {
		checkOptionRemove(document.getElementById(Target_Select_Field_Id));
	}
	Update_Auto_Complete_Extra_Para(Target_Select_Field_Id);
}

function onSearchAndInsertUsers(btnObj,textObj,targetObj,targetUserTypeAry,extraDataAry)
{
    extraDataAry = extraDataAry || '';

	var loading_image = '<?=$linterface->Get_Ajax_Loading_Image(1)?>';
	var error_layer = $('#'+textObj.id+'_Error');
	var search_text = $.trim(textObj.value);
	error_layer.html('');
	if(search_text == ''){
		error_layer.html('<?=$Lang['AppNotifyMessage']['WarningRequestInputSearchLoginID']?>');
		return false;
	}
	
	error_layer.html(loading_image);
	btnObj.disabled = true;
	$.post(
			'<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_batch_search_users.php',
		{
			"SearchText":encodeURIComponent(search_text),
			"TargetUserAry": JSON.stringify(targetUserTypeAry),
			"ExcludeUserAry": JSON.stringify(excludeUserAry),
            "ExtraDataAry": JSON.stringify(extraDataAry)
		},
		function(returnJson){
// 			console.log(returnJson);
			var ary = [];
			if( JSON && JSON.parse){
				ary = JSON.parse(returnJson);
			}else{
				eval('ary='+returnJson);
			}
			var UserSelected = targetObj;
			var inputted_target_values = search_text.split("\n");
			var selected_target_values = Get_Selection_Value(targetObj.id,'Array',true);
			var searched_user = [];
			var not_found_values = [];
			var current_class_name_number = "";
			for(var i=0;i<ary.length;i++){
				var id_value = 'U'+ary[i]['UserID'];
				searched_user.push(ary[i]['UserLogin']);
				if(ary[i]['ClassNameNumber'] != null){
					searched_user.push(ary[i]['ClassNameNumber'].replace('-',''));
					current_class_name_number =" ("+ary[i]['ClassNameNumber']+")";
				}
				if(selected_target_values.indexOf(id_value)==-1)
				{	
					if(ary[i]['StudentName'] != null){
						var optionLabel = ary[i]['StudentName']+<?php echo $jsonObj->encode($Lang['General']['s']); ?>+'('+ary[i]['UserName']+')';

					}else{
						var optionLabel = ary[i]['UserName']+ current_class_name_number;
					}
					UserSelected.options[UserSelected.length] = new Option(optionLabel, id_value);
					selected_target_values.push(id_value);
				}
			}
			for(var i=0;i<inputted_target_values.length;i++){
				if($.trim(inputted_target_values[i].replace(/\s/g,""))!='' && searched_user.indexOf(inputted_target_values[i].replace(/\s/g,""))==-1){
					not_found_values.push(inputted_target_values[i].replace(/\s/g,""));
				}
			}
			js_Select_All(targetObj.id);
			if(not_found_values.length>0){
				error_layer.html('<?=$Lang['AppNotifyMessage']['WarningFollowingUsersCannotBeFoundOrAdded']?><br />'+not_found_values.join("<br />"));
			}else{
				error_layer.html('');
			}
			$(textObj).val('').focus();
			btnObj.disabled = false;
		}
	);
}

</script>