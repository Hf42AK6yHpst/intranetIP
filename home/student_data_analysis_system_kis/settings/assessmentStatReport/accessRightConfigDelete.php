<?php


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


$obj = new libportfolio2007();

if (!$obj->IS_ADMIN_USER($UserID) && !$obj->IS_TEACHER())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$redirect = $_REQUEST['redirect'];

$recordIdList = (array)$_REQUEST['RecordIdCheckBox'];
$deleteRecordIdAAry = $recordIdList;
$recordIdList = implode(",", $recordIdList);
$recordIdList = IntegerSafe($recordIdList);

if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
	$MontlyReport = $_REQUEST['monthlyReport'];
}else{
	$MontlyReport = '0';
}

$Msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
if($MontlyReport == '1'){
	$CurrentTeacherAry= explode(",", $_REQUEST['CurrentTeacherList']);
	$RemainTeacherAry = array_diff($CurrentTeacherAry,$deleteRecordIdAAry);
	$RemainTeacherList = implode(',',$RemainTeacherAry);
	$UserID = $_SESSION['UserID'];	
	$sql = "UPDATE GENERAL_SETTING Set SettingValue = '$RemainTeacherList',DateModified = CURRENT_TIMESTAMP,ModifiedBy='$UserID' where SettingName = 'MonthlyReportPIC'";
		
	$obj->db_db_query($sql);
	
}else{
	if($recordIdList != ''){
		$sql = "DELETE FROM ASSESSMENT_ACCESS_RIGHT WHERE RecordID IN ($recordIdList)";
		$obj->db_db_query($sql);
		
		$sql = "SELECT * FROM ASSESSMENT_ACCESS_RIGHT WHERE RecordID IN ($recordIdList)";
		$rs = $obj->returnResultSet($sql);
		if(count($rs)){
			$Msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		}
	}
}



header("Location: {$redirect}&Msg={$Msg}");
exit;
?>