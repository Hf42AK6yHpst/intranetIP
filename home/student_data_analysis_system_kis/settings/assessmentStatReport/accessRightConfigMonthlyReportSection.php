<?php
//using 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/cees_kis/models/MonthlyReportModel.php"); // Class MonthlyReportModel->getAllSections()
include_once($PATH_WRT_ROOT."home/cees_kis/lang/$intranet_session_language.php");

if($field == "") $field = 0;
if($order == "") $order = 1;
$page_size = ($numPerPage == "")?50:$numPerPage;

$htmlCheckBoxName = "RecordIdCheckBox[]";
######################## Initialization END ########################

######################## PHP Header START ########################
intranet_auth();
intranet_opendb();

$objSDAS = new libSDAS();
$obj = new libportfolio2007();
// $allSections = MonthlyReportModel::getAllSections();
$allSections = array(
	'AppointmentTermination',
	'SubstituteTeacher',
	'TeacherSickLeave',
	'TeacherOtherLeave',
	'EnrolVacancy',
	'StudentAdmissionWithdrawal',
	'SchoolVisit',
	'TeacherCourse',
	'StudentEca',
	'SchoolAwards',
	'AdvancedNotice',
	'Others'
);
$target_section = $_POST['target_section'];
$form_submit = $_POST['form_submit'];

if (!$obj->IS_ADMIN_USER($UserID) && !$obj->IS_TEACHER() && $plugin['StudentDataAnalysisSystem_Style'] != "tungwah")
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$luser = new libuser($UserID);


$CurrentPage = "settings.assessmentStatReport";
// $TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['PIC'], "?t=settings.assessmentStatReport.accessRightConfigAdmin", 0);
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['MonthlyReport'] , "?t=settings.assessmentStatReport.accessRightConfigMonthlyReport", 0);
$TAGS_OBJ[] = array($Lang['SDAS']['Settings']['AccessRight']['MonthlyReportSection'] , "", 1);
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();
######################## PHP Header END ########################

######################## FORM SUBMIT    ########################
if($form_submit){
    $picRequest = (array)$_POST['pic'];
    foreach($picRequest as $k => $v){
        $picRequest[$k] = str_replace('U', '', $v);
    }
    $picRequest = array_unique($picRequest);
    $picArr = $objSDAS->getMonthlyReportSectionPIC($target_section);
    $idArr = array();
    $insertArr = array();
    $deleteArr = array();
    if(!empty($picArr)){
        foreach($picArr as $pic){
            $uid = $pic['UserID'];
            $index = array_search($uid, $picRequest);
            if(is_numeric($index)){
                unset($picRequest[$index]);
            } else {
                $deleteArr[] = $uid;
            }
        }
    }
    $insertArr = $picRequest;
    $table = "CEES_SCHOOL_MONTHLY_REPORT_USER_ACCESS_RIGHT";
    // delete
    if(!empty($deleteArr)){
        $cond = "SectionName = '$target_section' ";
        $cond .= "AND UserID IN ('" . implode("','", $deleteArr) . "')";
        $sql = "DELETE FROM $table
            WHERE $cond";
        //debug_pr($sql);
        $deleteResult = $objSDAS->db_db_query($sql);
    }
    // insert
    if(!empty($insertArr)){
        $col = "SectionName, UserID, InputBy, InputDate";
        $value = "";
        foreach($insertArr as $insert){
            $value .= "('$target_section','$insert','$_SESSION[UserID]',NOW()),";
        }
        $value = substr($value,0,-1);
        $sql = "INSERT INTO $table ($col) VALUES $value";
        //debug_pr($sql);
        $insertResult = $objSDAS->db_db_query($sql);
    }
}

######################## FORM SUBMIT    ########################

######################## Main Toolbar START ########################

$monthlyReportSections = array(
	'AppointmentTermination',
	'SubstituteTeacher',
	'TeacherSickLeave',
	'TeacherOtherLeave',
	'EnrolVacancy',
	'StudentAdmissionWithdrawal',
	'SchoolVisit',
	'TeacherCourse',
	'StudentEca',
	'SchoolAwards',
	'AdvancedNotice',
	'Others'
);

$options = '';
foreach($allSections as $k => $section){
    $selected = $section == $target_section ? 'selected' : '';
    $options .= "<option value='$section' $selected>".$Lang['CEES']['MonthlyReport']['JSLang'][$monthlyReportSections[$k]]."</option>";
    //$Lang['CEES']['MonthlyReport']['JSLang'][$section]
}
if($target_section==''){
    $target_section = $allSections[0];
}
$toolbar = <<<TOOLBAR_HTML
    <div class="Conntent_tool">
        <select name='target_section' onchange='document.form1.submit()'>
            $options
        </select>
    </div>
TOOLBAR_HTML;
######################## Main Toolbar END ########################

######################## Table Tools START ########################
$table_tool = <<<TOOLHTML
	<div nowrap="nowrap" class="common_table_tool">
		<a href="#" class="tool_delete" onclick="deleteUser()">{$Lang['Btn']['Delete']}</a>
	</div>
TOOLHTML;
######################## Table Tools END ########################

######################## PIC Selection START #####################
$picArr = $objSDAS->getMonthlyReportSectionPIC($target_section);
$PICSel = "<select name='pic[]' id='pic' size='6' multiple='multiple'>";
foreach($picArr as $pic){
    $PICSel .= "<option value='$pic[UserID]'>$pic[Name]</option>";
}
$PICSel .= "</select>";
$btn_remove = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");
######################## PIC Selection END #######################

$linterface->LAYOUT_START($Msg);
?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<form name="form1" id="form1" method="POST" action="?t=settings.assessmentStatReport.accessRightConfigMonthlyReportSection">
	<input type="hidden" name="redirect" value="?t=settings.assessmentStatReport.accessRightConfigMonthlyReportSection" />
	<input type="hidden" name="CurrentTeacherList" value="<?=$TeacherList[0]?>" />
	<div class ="content_top_tool">
		<?=$toolbar?>
		<br style="clear:both" />
	</div>
	<div class="form_table">
		<table class="form_table_v30">
			<tr>
			 <td class="field_title"><?php echo $Lang['SDAS']['Settings']['AccessRight']['MonthlyReportSection']?></td>
			 <td>
			 	<table border="0" cellpadding="0" cellspacing="0">
					<tr>
        				<td><?= $PICSel?></td>
        				<td valign="bottom">
        					&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=pic[]&permitted_type=1')")?><br />
        					&nbsp;<?=$btn_remove?>
						</td>
					</tr>
				</table>
			 </td>
			</tr>
		</table>
	</div>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_submit, "button", "", "btn_submit")?>
		<p class="spacer"></p>
	</div>
	<input type="hidden" name="form_submit" value="">
</form>

<script>
$('#btn_submit').click(function(){
	$('input[name="form_submit"]').val(1);
	$('option[value=""]').remove();
	$('#pic option').prop('selected',true);
	document.form1.submit();
});

function deleteUser(){
	/*
		[Javascript] If user have not check at-least-one items, alert the error and return false;
		To use this templete, set the variable checkboxName first.
	*/
	var countCheckedItem = $('input[name="<?=$htmlCheckBoxName?>"]:checked').length;
	if( countCheckedItem < 1){
		alert(globalAlertMsg2); // 請最少選擇一個項目。
		return false;
	}
	
	if(!confirm(globalAlertMsg3)){ // 你是否確定要刪除項目?
		return false;
	}
	document.form1.method = 'POST';
	document.form1.action = '?t=settings.assessmentStatReport.accessRightConfigDelete';
	document.form1.submit();
}

</script>


<?php 
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>
