<?php
## Using By : 
##############################################
##	Modification Log:
##	2020-05-28 (Philips)
##	- create this file
##
##############################################

$AcademicYearID = $_POST['AcademicYearID'];
if(!$AcademicYearID){
	No_Access_Right_Pop_Up('','/');
	die();
}
######## Init START ########
$libSDAS = new libSDAS();
######## Init END ########
$Action = $_POST['action'];
$ReportStartDate = $_POST['ReportStartDate'];
$ReportEndDate = $_POST['ReportEndDate'];
$ReportID = $_POST['ReportID'];
$InfoData = array(
	'ReportStartDate' => $ReportStartDate,
	'ReportEndDate' => $ReportEndDate,
	'DraftStatus' => $Action == 'submit' ? '1' : '0'
);
$SectionCount = 3;
$DataAry = array();
for($i=1; $i<=$SectionCount; $i++){
	$dateAry = $_POST['Section'.$i.'_Date'];
	$eventAry = $_POST['Section'.$i.'_Event'];
	$venueAry = $_POST['Section'.$i.'_Venue'];
	$guestAry = $_POST['Section'.$i.'_Guest'];
	$psAry = $_POST['Section'.$i.'_ParticipatedSchool'];
	$targetAry = $_POST['Section'.$i.'_Target'];
	$participantAry = $_POST['Section'.$i.'_TotalParticipant'];
	foreach((array)$dateAry as $k => $v){
		$record = array(
			'Section' => $i,
			'RecordDate' => htmlspecialchars($v),
			'Event' => htmlspecialchars($eventAry[$k]),
			'Venue' => htmlspecialchars($venueAry[$k]),
			'Guest' => htmlspecialchars($guestAry[$k]),
			'ParticipatedSchool' => htmlspecialchars($psAry[$k]),
			'Target' => htmlspecialchars($targetAry[$k]),
			'TotalParticipant' => htmlspecialchars($participantAry[$k])
		);
		$DataAry[] = $record;
	}
}
$result = $libSDAS->insertInterSchoolECA($AcademicYearID, $InfoData, $DataAry, $ReportID);
if($result){
	// update success
	$xmsg = "UpdateSuccess";
} else {
	// update failed
	$xmsg = "UpdateUnsuccess";
}
header('location: ?t=management.inter_school_activity_report.index&AcademicYearID='.$AcademicYearID.'&ReportID='.$ReportID.'&xmsg='.$xmsg);