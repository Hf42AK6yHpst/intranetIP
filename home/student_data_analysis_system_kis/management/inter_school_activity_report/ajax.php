<?php
## Using By :
##############################################
##	Modification Log:
##	2020-06-02 (Philips)
##	- create this file
##
##############################################
include_once($PATH_WRT_ROOT . 'includes/json.php');
######## Init START ########
$libSDAS = new libSDAS();
$libJSON = new JSON_obj();
######## Init END ########
$Action = $_POST['Action'];
$ReportSection = $_POST['ReportSection'];
$ReportStartDate = $_POST['ReportStartDate'];
$ReportEndDate = $_POST['ReportEndDate'];
$AcademicYearID= $_POST['AcademicYearID'];
$ActivityPrefix = 'O';
$CategoryAry = array(
		'1' => 'ProfessionalDevelopmentAndTrainning',
		'2' => 'LearnAndTeach',
		'3' => 'StudentSupport'
);
switch($Action){
	case 'syncEnrolment':
		$table = 'INTRANET_ENROL_EVENTINFO';
		$joinTable = 'INTRANET_ENROL_EVENT_DATE';
		$joinTable2 = 'INTRANET_ENROL_EVENTSTUDENT';
		
		// 2020-07-06 (Philips) - Section 1 no need to check member
		if($ReportSection!='1'){
			$join2 = "INNER JOIN $joinTable2 c
			ON a.EnrolEventID = c.EnrolEventID";
		} else {
			$join2 = "LEFT OUTER JOIN $joinTable2 c
			ON a.EnrolEventID = c.EnrolEventID";
		}
		// GROUP_CONCAT(DATE_FORMAT(b.ActivityDateStart, "%Y-%m-%d")) AS EventDate,
		$cols = 'a.EventTitle, a.SDAS_Category, a.SDAS_AllowSync, GROUP_CONCAT(DISTINCT DATE_FORMAT(b.ActivityDateStart, "%Y-%m-%d")) AS EventDate, a.Location, IF(a.TotalParticipant NOT IN (0, ""), a.TotalParticipant, COUNT(*)) AS Total,
				 a.ActivityTarget, a.Guest, a.ParticipatedSchool';
		$targetCategory = $ActivityPrefix . $ReportSection . '_'.$CategoryAry[$ReportSection];
		$conds = " a.SDAS_AllowSync LIKE '%O%' AND a.SDAS_Category = '$targetCategory' ";
		$conds .= " AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') >= '$ReportStartDate' AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') <= '$ReportEndDate' ";
		$sql = "SELECT $cols FROM $table a
			 INNER JOIN $joinTable b
				ON a.EnrolEventID = b.EnrolEventID
			 $join2
			 WHERE $conds
			 Group By a.EnrolEventID
		";
// 		debug_pr($sql);die();
		$recordAry = $libSDAS->returnArray($sql);
		$result['record'] = $recordAry;
		echo $libJSON->encode($result);
		break;
	case 'addReport':
		$reportID = $libSDAS->insertInterSchoolECA($AcademicYearID, array('ReportStartDate'=> date('Y-m-d'), 'ReportEndDate' => date('Y-m-d')), array(),'', $isNew=true);
		echo $reportID;
		break;
	case 'removeReport':
		$ReportID = $_POST['ReportID'];
		$existReport = $libSDAS->getInterSchoolECA($AcademicYearID, $ReportID);
		$isSubmit = $existReport['SubmitStatus'];
		$isDraft =  $existReport['DraftStatus'];
		if(!$isSubmit && !$isDraft){
			$result = $libSDAS->removeInterSchoolECA($ReportID);
			echo $result;
		} else {
			echo false;
		}
		break;
	default:
		break;
}
?>