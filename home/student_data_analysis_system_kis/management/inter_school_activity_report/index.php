<?php
## Using By : 
##############################################
##	Modification Log:
##	2020-05-28 (Philips)
##	- create this file
##
##############################################

include_once($PATH_WRT_ROOT . 'includes/json.php');

######## Init START ########
$libSDAS = new libSDAS();
$libJSON = new JSON_obj();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "management.inter_school_activity_report";
$CurrentPageName =  $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool'] . ')';
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
$Page_NavArr = array();
######## Page Setting END ########

$ViewMode = 'Mark';
$AcademicYearID = $_REQUEST['AcademicYearID'];
if($AcademicYearID == ''){
	$AcademicYearID = Get_Current_Academic_Year_ID();
}
$ECAReportList = $libSDAS->getInterSchoolECA($AcademicYearID);
$ECAReportListData = array();
foreach($ECAReportList as $ecar){
	$ECAReportListData[] = array($ecar['ReportID'], $ecar['StartDate'] . ' ' . $Lang['General']['To'] . ' ' . $ecar['EndDate']);
}
$ECAReportSelect = getSelectByArray($ECAReportListData, 'name="ReportID" id="ReportID" onChange="js_Changed_ReportID_Selection(this.value)"', $ReportID, 0, $noFirst=0, $FirstTitle="");

$libfcm_ui = new form_class_manage_ui();
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
foreach((array)$academic_year_arr as $_key => $_infoArr){
	$academic_year_arr[$_key]['1'] = iconv('Big5-HKSCS','UTF-8',$_infoArr['1']);
	$academic_year_arr[$_key]['AcademicYearName'] = iconv('Big5-HKSCS','UTF-8',$_infoArr['AcademicYearName']);
}
$AcademicYearSelection = getSelectByArray($academic_year_arr, "name='AcademicYearID' id='AcademicYearID' onChange='js_Changed_AcademicYear_Selection(this.value);'", $AcademicYearID, 0, 1);

if($ReportID != ''){
	$ECAReport = $libSDAS->getInterSchoolECA($AcademicYearID, $ReportID);
	$isSubmit = $ECAReport['SubmitStatus'];
	$isDraft = $ECAReport['DraftStatus'];
	if($ECAReport['ReportID']){
		$recordAry = $libSDAS->getInterSchoolECARecord($ECAReport['ReportID']);
		$SectionDataAry = array();
		foreach($recordAry as $record){
			$sNum = $record['Section'];
			if(!isset($SectionDataAry[$sNum])) $SectionDataAry[$sNum] = array();
			$row = array();
			foreach($record as $k => $v){
				if(!is_int($k)){
					$row[$k] = $v;
				}
			}
			$SectionDataAry[$sNum][] = $row;
			unset($row);
		}
		$SectionCountAry = array(
				0, count($SectionDataAry[0]), count($SectionDataAry[1]), count($SectionDataAry[2])
		);
	} else {
		$recordAry = array();
		$SectionCountAry = array(0,0,0,0);
	}
	$xmsg = $_REQUEST['xmsg'];
	
	# Navigation Init
	$navs = array(
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section1'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['ManagementAndOrganization'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section2'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['LearnAndTeach'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section3'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentSupport']
	);
	$subNavs = array(
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning']
	);
	$headerAry = array(
			'order' => array('#','1'),
			'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Date'], '10%'),
			'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Event'], '15%'),
			'Venue' => array($Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Venue'], '15%'),
			'Guest' => array($Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Guest'], '10%'),
			'ParticipantSchool' => array($Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ParticipatedSchool'], '20%'),
			'Target' => array($Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Target'], '20%'),
			'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['TotalParticipant'], '10%'),
			'delete' => array('', '1')
	);
	$widthAry = array();
	foreach($headerAry as $header){
		$widthAry[] = $header[1];
	}
	foreach($navs as $nav){
		$Page_NavArr[] = $nav;
	}
	
	if($AcademicYearID){
		$ecaAry = array();
	}
	
	// Warning Msg Box
	
	// Submit Button
	$SubmitBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Submit_Report();", $id="Btn_Submit");
	// Submit Button
	$DraftBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Draft_Report();", $id="Btn_Draft");
	// Cancel Submit Button
	$CancelSubmitBtn = $linterface->GET_ACTION_BTN($Lang['CEES']['Management']['SchoolActivityReport']['Category']['CancelSubmit'], "button", "js_CancelSubmit_Report();", $id="Btn_CancelSubmit");
	
	if(!empty($SectionDataAry)){
		$AddRowScript = "";
		foreach($SectionDataAry as $section => $SectionData){
			foreach($SectionData as $data){
				$json = $libJSON->encode($data);
				$AddRowScript .= "js_AddRow($section, $json);\n";
			}
		}
	}
}
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
// echo $linterface->Include_Cookies_JS_CSS();
echo $linterface->Include_CopyPaste_JS_CSS('2016');
echo $linterface->Include_Excel_JS_CSS();
?>
<script>
var jsCurAcademicYearID = '<?php echo $AcademicYearID?>';
<?if($ReportID != ''){?>
var SectionRowCount = [<?=implode(',',$SectionCountAry)?>];
var widthSetting = ['<?=implode("','", $widthAry)?>'];
<?}?>
$(document).ready( function() {
	<?=$AddRowScript?>
	<?php if($isDraft || $isSubmit){?>
	$('input').attr('disabled',true);
	$('input[type=button]').removeAttr('disabled');
	$('textarea').attr('disabled',true);
	<?php }?>
});

function js_Draft_Report(){
	if(checkForm()){
		document.form1.action.value = 'draft';
		document.form1.setAttribute('action','?t=management.inter_school_activity_report.update');
		document.form1.submit();
	}
}
function js_Submit_Report(){
	if(checkForm()){
		document.form1.action.value = 'submit';
		document.form1.setAttribute('action','?t=management.inter_school_activity_report.update');
		document.form1.submit();
	}
}
function js_CancelSubmit_Report(){
	if(checkForm()){
		document.form1.action.value = 'release';
		document.form1.setAttribute('action','?t=management.inter_school_activity_report.update');
		document.form1.submit();
	}
}
function js_Changed_AcademicYear_Selection(id){
	window.location.href = '?t=management.inter_school_activity_report.index&AcademicYearID=' + id;
}
function js_Changed_ReportID_Selection(id){
	window.location.href = '?t=management.inter_school_activity_report.index&AcademicYearID=' + $('#AcademicYearID').val() + '&ReportID=' + id;
}
function js_AddRow(targetTable, data){
	let id = SectionRowCount[targetTable]++;
	tempData = {
		date: data['RecordDate'] ? data['RecordDate'] : '',
		evt: data['Event'] ? data['Event'] : '',
		venue: data['Venue'] ? data['Venue'] : '',
		guest: data['Guest'] ? data['Guest'] : '',
		schools: data['ParticipatedSchool'] ? data['ParticipatedSchool'] : '',
		target: data['Target'] ? data['Target'] : '',
		participant: data['TotalParticipant'] ? data['TotalParticipant'] : ''
	}
	tableEle = $('#Section'+targetTable+'Div').find('table')[0];
	tableBody = $(tableEle).find('tbody')[0];
	tableBody = $(tableBody);
	lastRow = $(tableBody).find('tr').last();

	let newRow = $('<tr>').attr('id', 'Section'+targetTable+'_Row'+id);
	let emptyTD = $('<td>');
	newRow.append(emptyTD);
	emptyTD = $('<td>');
	emptyTD.append( $('<input type="text" name="Section'+targetTable+'_Date['+id+']" id="Section'+targetTable+'_Date['+id+']" class="input_date" />').val(tempData.date));
	newRow.append(emptyTD);
	emptyTD = $('<td>');
	emptyTD.append( $('<input type="text" name="Section'+targetTable+'_Event['+id+']" id="Section'+targetTable+'_Event['+id+']" class="input_text" />').val(tempData.evt));
	newRow.append(emptyTD);
	emptyTD = $('<td>');
	emptyTD.append( $('<input type="text" name="Section'+targetTable+'_Venue['+id+']" id="Section'+targetTable+'_Venue['+id+']" class="input_text" />').val(tempData.venue));
	newRow.append(emptyTD);
	emptyTD = $('<td>');
	emptyTD.append( $('<input type="text" name="Section'+targetTable+'_Guest['+id+']" id="Section'+targetTable+'_Guest['+id+']" class="input_text" />').val(tempData.guest));
	newRow.append(emptyTD);
	emptyTD = $('<td>');
	emptyTD.append( $('<textarea name="Section'+targetTable+'_ParticipatedSchool['+id+']" id="Section'+targetTable+'_ParticipatedSchool['+id+']" class="input_text" rows=1 cols=30 ></textarea>').val(tempData.schools));
	newRow.append(emptyTD);
	emptyTD = $('<td>');
	emptyTD.append( $('<input type="text" name="Section'+targetTable+'_Target['+id+']" id="Section'+targetTable+'_Target['+id+']" class="input_text" />').val(tempData.target));
	newRow.append(emptyTD);
	emptyTD = $('<td>');
	emptyTD.append( $('<input type="text" name="Section'+targetTable+'_TotalParticipant['+id+']" id="Section'+targetTable+'_TotalParticipant['+id+']" class="input_number" />').val(tempData.participant));
	newRow.append(emptyTD);
	<?php if(!$isDraft && !$isSubmit){ ?>
	emptyTD = $('<td>');
	let linkDel = $('<?=$linterface->GET_LNK_DELETE('#')?>');
	linkDel.find('a').attr('href', 'javascript:js_RemoveRow("Section'+targetTable+'_Row'+id+'")');
	emptyTD.append(linkDel);
	newRow.append(emptyTD);
	<?php }?>
	newRow.insertBefore(lastRow);

	js_InitDatePicker('Section'+targetTable+'_Date['+id+']');

	js_RefreshRowNum();
	js_RefreshRowWidth();
}
function js_RefreshRowWidth(){
	$('.record_table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
}
function js_RefreshRowNum(){
	$('.record_table').each(function(k, table){
			$(table).find('tr:not(:last)').each(function(index, val) {
			$(val).find('td').first().text(index);
		});
	});
}
function js_RemoveRow(id){
	$('#'+id).remove();
	js_RefreshRowNum();
}
function checkForm(){
	$('input.input_date').each(function(i, ele){ if(!check_date_30(ele, '<?=$Lang['General']['InvalidDateFormat']?>')) return false;});
	return true;
}
function js_AddDatePicker(id, val){
	let dom = '<input type="text" name="'+id+'" id="'+id+'" value="'+val+'" size="10" maxlength="10" class="textboxnum hasDatepick" onkeyup="Date_Picker_Check_Date_Format(this,\'DPWL-'+id+'\',\'\');">';
	return dom;
}
function js_AddDatePickerSpan(id){
	let dom = '<span style="color:red;" id="DPWL-'+id+'"></span>';
	return dom;
}
function js_InitDatePicker(id){
	$('input#' + id).ready(function(){
		$('input#' + id).datepick({
			
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0,
			onSelect: function(dateText, inst) {
				Date_Picker_Check_Date_Format(document.getElementById(id),'DPWL-' + id,'');
			}
		});
	});
}
function js_RetrieveData(section, target){
	let href = '?t=management.inter_school_activity_report.ajax';
	$.ajax({
		url: href,
		data: {
			AcademicYearID: $('#AcademicYearID').val(),
			ReportStartDate: $('#ReportStartDate').val(),
			ReportEndDate: $('#ReportEndDate').val(),
			Action: 'sync'+section,
			ReportSection: target
		},
		dataType: 'JSON',
		method: 'POST',
		success: function(data){
// 			console.log(data);return;
			let confirm = window.confirm('<?=$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['RetrieveDataRow'] . ': '?>' + data.record.length + '<?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Row']?>');
			if(confirm){
				for(let i=0;i<data.record.length; i++){
					let recordRow = {
						RecordDate: data.record[i].EventDate,
						Event: data.record[i].EventTitle,
						Venue: data.record[i].Location,
						TotalParticipant: data.record[i].Total,
						Guest: data.record[i].Guest,
						ParticipatedSchool: data.record[i].ParticipatedSchool,
						Target: data.record[i].ActivityTarget
					};
					js_AddRow(target, recordRow);
				}
			}
		}
	});
}
function js_AddReport(){
	let AcademicYearID = $('#AcademicYearID').val();
	$.ajax({
		url: '?t=management.inter_school_activity_report.ajax',
		method: 'POST',
		data: {'AcademicYearID': AcademicYearID, 'Action': 'addReport'},
		success: function(data){
			if(data!=''){
				reportID = data;
				js_Changed_ReportID_Selection(reportID);
			} else {
			}
		}
	});
}
function js_RemoveReport(){
	let AcademicYearID = $('#AcademicYearID').val();
	let ReportID = $('#ReportID').val();
	let confirm = window.confirm('<?=$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ConfirmRemoveReport']?>');
	if(!confirm) return;
	$.ajax({
		url: '?t=management.inter_school_activity_report.ajax',
		method: 'POST',
		data: {'AcademicYearID': AcademicYearID,'ReportID': ReportID, 'Action': 'removeReport'},
		success: function(data){
			if(data!=''){
				js_Changed_AcademicYear_Selection(AcademicYearID);
			} else {
			}
		}
	});
}
function js_ImportData(target){
	href = "?t=management.import&ReportType=1&ReportSection=" + target;
	window.open(href,'_blank','width=550px,height=300px');
}
</script>
<form id="form1" name="form1" action="" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0" width='1000px'>
		<tr>
			<td style="padding-left:7px;">
				<br style="clear:both;" />
				<div class="table_filter">
					<?=$AcademicYearSelection?>
					<?=$ECAReportSelect?>
				</div>
				<div class='table_row_tool row_content_tool' style='float:left'>
					<?php if($ReportID!=''&&!$isDraft&&!$isSubmit){
						echo $linterface->GET_LNK_DELETE('javascript:js_RemoveReport()','','');
					}?>
				</div>
				<br style="clear:both;" />
				<div class='Conntent_tool'>
				<?php if($ReportID == ''){?>
					<?=$linterface->GET_LNK_NEW("javascript:js_AddReport()", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['NewReport'], $ParOnClick="")?>
				<?php }?>
				</div>
				<?php if($ReportID != ''){?>
				<br style="clear:both;" />
					<?php if($AcademicYearID!=''){?>
						<table class='form_table_v30' width='50%'>
							<tbody>
								<tr>
									<td class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ReportStartDate']?></td>
									<td class="field_content_short">
										<?=$linterface->GET_DATE_PICKER('ReportStartDate', $ECAReport['StartDate'])?>
									</td>
								</tr>
								<tr>
									<td class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ReportEndDate']?></td>
									<td class="field_content_short">
										<?=$linterface->GET_DATE_PICKER('ReportEndDate', $ECAReport['EndDate'])?>
									</td>
								</tr>
							</tbody>
						</table>
						<?php $i = 0;
						foreach($Page_NavArr as $Page_Nav){
							$i++; ?>
							<hr/>
							<?=$linterface->GET_NAVIGATION2($Page_Nav)?>
							<br style="clear:both;" />
							<?php if($i==1){?>
								<h3><?=$subNavs[0]?></h3>
							<?php }?>
							<div id="Section<?=$i?>Div">
								<br style="clear:both;" />
								<div class="Conntent_tool">
								<?php if(!$isDraft && !$isSubmit){ 
									echo $linterface->GET_LNK_IMPORT("javascript:js_RetrieveData('Enrolment', '$i')", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromeEnrolment'], $ParOnClick="");
									if($i==1) echo $linterface->GET_LNK_IMPORT("javascript:js_ImportData('$i')", $Lang['General']['Import'], $ParOnClick="");
								}?>
								</div>
								<table id="" class="common_table_list_v30 edit_table_list_v30 record_table" width='100%'>
									<thead>
										<?php foreach($headerAry as $key => $header){?>
											<th width='<?=$header[1]?>'><?=$header[0]?></th>
										<?php }?>
									</thead>
									<tbody>
										<tr><td colspan='<?=sizeof($headerAry)?>'>
										<?php if(!$isDraft && !$isSubmit){ ?>
											<div class="table_row_tool row_content_tool"><a class="add_dim" href="javascript:js_AddRow(<?=$i?>, {})"></a></div>
										<?php }?>
										</td></tr>
									</tbody>
								</table>
							</div>
							<br style="clear:both;" />
						<?php }?>
						<input type='hidden' name='action' id='action' value="" />
						<div class="edit_bottom_v30">
							<?php if(!$isDraft && !$isSubmit){
								echo $DraftBtn;
								if($ECAReport['ReportID']){
									echo '&nbsp;&nbsp;';
									echo $SubmitBtn;
								}
							} else if($isDraft && !$isSubmit){
								echo $CancelSubmitBtn;
							}?>
						</div>
					<?}?>
				<?}?>
			</td>
		</tr>
	</table>
	
</form>

<?php
$linterface->LAYOUT_STOP();
?>