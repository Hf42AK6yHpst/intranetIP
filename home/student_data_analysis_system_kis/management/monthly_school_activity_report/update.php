<?php
## Using By : 
##############################################
##	Modification Log:
##	2020-06-01 (Philips)
##	- create this file
##
##############################################

$AcademicYearID = $_POST['AcademicYearID'];
if(!$AcademicYearID){
	No_Access_Right_Pop_Up('','/');
	die();
}
######## Init START ########
$libSDAS = new libSDAS();
######## Init END ########
$Action = $_POST['action'];
$ReportDate = $_POST['ReportDate'];
$ReportMonth = $_POST['ReportMonth'];
$InfoData = array(
	'ReportDate' => $ReportDate,
	'DraftStatus' => $Action == 'submit' ? '1' : '0'
);
$SectionCount = 4;
$SectionAry = array(
	0,4,3,5,3
);
$DataAry = array();
for($i=1; $i<=$SectionCount; $i++){
	for($j=1;$j<=$SectionAry[$i]; $j++){
		if($i == 1){
			if($j==1){
				$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
				$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
				$venueAry = $_POST['Section'.$i.'_'.$j.'Venue'];
				$guestAry = $_POST['Section'.$i.'_'.$j.'Guest'];
				$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
				$schoolAry = $_POST['Section'.$i.'_'.$j.'ParticipatedSchool'];
				$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
				foreach((array)$dateAry as $k => $v){
					$record = array(
							'Section' => $i.'_'.$j,
							'RecordDate' => htmlspecialchars($v),
							'Event' => htmlspecialchars($eventAry[$k]),
							'Venue' => htmlspecialchars($venueAry[$k]),
							'Guest' => htmlspecialchars($guestAry[$k]),
							'Participant' => htmlspecialchars($participantAry[$k]),
							'ParticipatedSchool' => htmlspecialchars($schoolAry[$k]),
							'Remark' => htmlspecialchars($remarkAry[$k])
					);
					$DataAry[] = $record;
				}
			} else {
				$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
				$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
				$venueAry = $_POST['Section'.$i.'_'.$j.'Venue'];
				$guestAry = $_POST['Section'.$i.'_'.$j.'Guest'];
				$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
				$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
				foreach((array)$dateAry as $k => $v){
					$record = array(
						'Section' => $i.'_'.$j,
						'RecordDate' => htmlspecialchars($v),
						'Event' => htmlspecialchars($eventAry[$k]),
						'Venue' => htmlspecialchars($venueAry[$k]),
						'Guest' => htmlspecialchars($guestAry[$k]),
						'Participant' => htmlspecialchars($participantAry[$k]),
						'Remark' => htmlspecialchars($remarkAry[$k])
					);
					$DataAry[] = $record;
				}
			}
		} else if($i==2){
			if($j==1){
				$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
				$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
				$venueAry = $_POST['Section'.$i.'_'.$j.'Venue'];
				$guestAry = $_POST['Section'.$i.'_'.$j.'Guest'];
				$targetAry = $_POST['Section'.$i.'_'.$j.'Target'];
				$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
				$schoolAry = $_POST['Section'.$i.'_'.$j.'ParticipatedSchool'];
				$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
				foreach((array)$dateAry as $k => $v){
					$record = array(
							'Section' => $i.'_'.$j,
							'RecordDate' => htmlspecialchars($v),
							'Event' => htmlspecialchars($eventAry[$k]),
							'Venue' => htmlspecialchars($venueAry[$k]),
							'Guest' => htmlspecialchars($guestAry[$k]),
							'Target' => htmlspecialchars($targetAry[$k]),
							'Participant' => htmlspecialchars($participantAry[$k]),
							'ParticipatedSchool' => htmlspecialchars($schoolAry[$k]),
							'Remark' => htmlspecialchars($remarkAry[$k])
					);
					$DataAry[] = $record;
				}
			} else {
				$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
				$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
				$venueAry = $_POST['Section'.$i.'_'.$j.'Venue'];
				$guestAry = $_POST['Section'.$i.'_'.$j.'Guest'];
				$targetAry = $_POST['Section'.$i.'_'.$j.'Target'];
				$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
				$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
				foreach((array)$dateAry as $k => $v){
					$record = array(
							'Section' => $i.'_'.$j,
							'RecordDate' => htmlspecialchars($v),
							'Event' => htmlspecialchars($eventAry[$k]),
							'Venue' => htmlspecialchars($venueAry[$k]),
							'Guest' => htmlspecialchars($guestAry[$k]),
							'Target' => htmlspecialchars($targetAry[$k]),
							'Participant' => htmlspecialchars($participantAry[$k]),
							'Remark' => htmlspecialchars($remarkAry[$k])
					);
					$DataAry[] = $record;
				}
			}
		}else if($i==3){
			if($j==1){
				$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
				$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
				$venueAry = $_POST['Section'.$i.'_'.$j.'Venue'];
				$guestAry = $_POST['Section'.$i.'_'.$j.'Guest'];
				$targetAry = $_POST['Section'.$i.'_'.$j.'Target'];
				$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
				$schoolAry = $_POST['Section'.$i.'_'.$j.'ParticipatedSchool'];
				$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
				foreach((array)$dateAry as $k => $v){
					$record = array(
							'Section' => $i.'_'.$j,
							'RecordDate' => htmlspecialchars($v),
							'Event' => htmlspecialchars($eventAry[$k]),
							'Venue' => htmlspecialchars($venueAry[$k]),
							'Guest' => htmlspecialchars($guestAry[$k]),
							'Target' => htmlspecialchars($targetAry[$k]),
							'Participant' => htmlspecialchars($participantAry[$k]),
							'ParticipatedSchool' => htmlspecialchars($schoolAry[$k]),
							'Remark' => htmlspecialchars($remarkAry[$k])
					);
					$DataAry[] = $record;
				}
			} else {
				$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
				$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
				$venueAry = $_POST['Section'.$i.'_'.$j.'Venue'];
				$guestAry = $_POST['Section'.$i.'_'.$j.'Guest'];
				$targetAry = $_POST['Section'.$i.'_'.$j.'Target'];
				$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
				$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
				foreach((array)$dateAry as $k => $v){
					$record = array(
							'Section' => $i.'_'.$j,
							'RecordDate' => htmlspecialchars($v),
							'Event' => htmlspecialchars($eventAry[$k]),
							'Venue' => htmlspecialchars($venueAry[$k]),
							'Guest' => htmlspecialchars($guestAry[$k]),
							'Target' => htmlspecialchars($targetAry[$k]),
							'Participant' => htmlspecialchars($participantAry[$k]),
							'Remark' => htmlspecialchars($remarkAry[$k])
					);
					$DataAry[] = $record;
				}
			}
		} else if($j == 1){
			// Section 3_1
			$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
			$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
			$organization = $_POST['Section'.$i.'_'.$j.'Organization'];
			$pgAry = $_POST['Section'.$i.'_'.$j.'ParticipatedGroup'];
			$awardAry = $_POST['Section'.$i.'_'.$j.'Award'];
			$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
			foreach((array)$dateAry as $k => $v){
				$record = array(
						'Section' => $i.'_'.$j,
						'RecordDate' => htmlspecialchars($v),
						'Event' => htmlspecialchars($eventAry[$k]),
						'Organization' => htmlspecialchars($organization[$k]),
						'ParticipatedGroup' => htmlspecialchars($pgAry[$k]),
						'Award' => htmlspecialchars($awardAry[$k]),
						'Remark' => htmlspecialchars($remarkAry[$k])
				);
				$DataAry[] = $record;
			}
		} else if($j == 2) {
			// Section 3_2
			$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
			$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
			$organization = $_POST['Section'.$i.'_'.$j.'Organization'];
			$pgAry = $_POST['Section'.$i.'_'.$j.'ParticipatedGroup'];
			$nameAry = $_POST['Section'.$i.'_'.$j.'Name'];
			$positionAry = $_POST['Section'.$i.'_'.$j.'Position'];
			$awardAry = $_POST['Section'.$i.'_'.$j.'Award'];
			$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
			$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
			foreach((array)$dateAry as $k => $v){
				$record = array(
						'Section' => $i.'_'.$j,
						'RecordDate' => htmlspecialchars($v),
						'Event' => htmlspecialchars($eventAry[$k]),
						'Organization' => htmlspecialchars($organization[$k]),
						'ParticipatedGroup' => htmlspecialchars($pgAry[$k]),
						'Name' => htmlspecialchars($nameAry[$k]),
						'Position' => htmlspecialchars($positionAry[$k]),
						'Award' => htmlspecialchars($awardAry[$k]),
						'Participant' => htmlspecialchars($participantAry[$k]),
						'Remark' => htmlspecialchars($remarkAry[$k])
				);
				$DataAry[] = $record;
			}
		} else {
			// Section 3_3
			$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
			$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
			$organization = $_POST['Section'.$i.'_'.$j.'Organization'];
			$pgAry = $_POST['Section'.$i.'_'.$j.'ParticipatedGroup'];
			$classAry = $_POST['Section'.$i.'_'.$j.'Class'];
			$nameAry = $_POST['Section'.$i.'_'.$j.'Name'];
			$awardAry = $_POST['Section'.$i.'_'.$j.'Award'];
			$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
			$remarkAry = $_POST['Section'.$i.'_'.$j.'Remark'];
			foreach((array)$dateAry as $k => $v){
				$record = array(
						'Section' => $i.'_'.$j,
						'RecordDate' => htmlspecialchars($v),
						'Event' => htmlspecialchars($eventAry[$k]),
						'Organization' => htmlspecialchars($organization[$k]),
						'ParticipatedGroup' => htmlspecialchars($pgAry[$k]),
						'Class' => htmlspecialchars($classAry[$k]),
						'Name' => htmlspecialchars($nameAry[$k]),
						'Award' => htmlspecialchars($awardAry[$k]),
						'Participant' => htmlspecialchars($participantAry[$k]),
						'Remark' => htmlspecialchars($remarkAry[$k])
				);
				$DataAry[] = $record;
			}
		}
	}
}
$result = $libSDAS->insertMonthlyECA($AcademicYearID, $ReportMonth, $InfoData, $DataAry);
if($result){
	// update success
	$xmsg = "UpdateSuccess";
} else {
	// update failed
	$xmsg = "UpdateUnsuccess";
}
header('location: ?t=management.monthly_school_activity_report.index&AcademicYearID='.$AcademicYearID.'&ReportMonth='.$ReportMonth.'&xmsg='.$xmsg);