<?php
## Using By : 
##############################################
##	Modification Log:
##	2020-06-01 (Philips)
##	- create this file
##
##############################################

include_once($PATH_WRT_ROOT . 'includes/json.php');

######## Init START ########
$libSDAS = new libSDAS();
$libJSON = new JSON_obj();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "management.monthly_school_activity_report";
$CurrentPageName =  $Lang['CEES']['Management']['SchoolActivityReport']['Category']['MonthlyActivityReport'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
$Page_NavArr = array();
######## Page Setting END ########

$ViewMode = 'Mark';
$AcademicYearID = $_REQUEST['AcademicYearID'];
if($AcademicYearID == ''){
	$AcademicYearID = Get_Current_Academic_Year_ID();
}
if($ReportMonth == ''){
	$ReportMonth= date('m');
}
$xmsg = $_REQUEST['xmsg'];

$libfcm_ui = new form_class_manage_ui();
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
foreach((array)$academic_year_arr as $_key => $_infoArr){
	$academic_year_arr[$_key]['1'] = iconv('Big5-HKSCS','UTF-8',$_infoArr['1']);
	$academic_year_arr[$_key]['AcademicYearName'] = iconv('Big5-HKSCS','UTF-8',$_infoArr['AcademicYearName']);
}
$AcademicYearSelection = getSelectByArray($academic_year_arr, "name='AcademicYearID' id='AcademicYearID' onChange='js_Changed_AcademicYear_Selection(this.value);'", $AcademicYearID, 0, 1);
$MonthSelection = $libSDAS->getMonthSelection($AcademicYearID,$ReportMonth, "name='ReportMonth' id='ReportMonth' onChange='js_Changed_ReportMonth_Selection(this.value)'");
# Navigation Init
$navs = array(
		$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section1'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['ManagementAndOrganization'],
		$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section2'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['LearnAndTeach'],
		$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section3'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentSupport'],
		$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section4'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentArcheivement']
);
$subNav = array(
		array(
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAdministration'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterflowAndSharing']
		),
		array(
				'',
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InsideSchoolActivity'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OutsideSchoolActivity']
		),
		array(
				'',
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentSchoolActivity'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentEducation'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OtherSupportActivity'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Others']
		),
		array(
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAward'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StaffAward'],
				$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentAward']
		)
);


$ECAReport = $libSDAS->getMonthlyECA($AcademicYearID, $ReportMonth);
$isSubmit = $ECAReport['SubmitStatus'];
$isDraft = $ECAReport['DraftStatus'];
if($ECAReport['ReportID']){
	$recordAry = $libSDAS->getMonthlyECARecord($ECAReport['ReportID']);
// 	debug_pr($recordAry);die();
	// 	$SectionDataAry = BuildMultiKeyAssoc($recordAry, 'Section', array('ReportID', 'Section', 'RecordDate', 'Event', 'Venue', 'Guest', 'ParticipatedSchool', 'Target', 'TotalParticipant'));
	$SectionDataAry = array();
	foreach($recordAry as $record){
		$sNum = $record['Section'];
		if(!isset($SectionDataAry[$sNum])) $SectionDataAry[$sNum] = array();
		$row = array();
		foreach($record as $k => $v){
			if(!is_int($k)){
				$row[$k] = $v;
			}
		}
		$SectionDataAry[$sNum][] = $row;
		unset($row);
	}
	$SectionCountAry = array(array());
	foreach($subNav as $k => $sNav){
		foreach($sNav as $i => $item){
			$SectionCountAry[$k+1][$i+1] = count($SectionDataAry[($k+1).'_'.($i+1)]);
		}
	}
} else {
	$recordAry = array();
	$SectionCountAry = array(array());
	foreach($subNav as $k => $sNav){
		foreach($sNav as $i => $item){
			$SectionCountAry[$k+1][$i+1] = 0;
		}
	}
}
$sectionCountJson = $libJSON->encode($SectionCountAry);
$headerAry = array(
		array(
				'order' => array('#','1'),
				'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'], '20%'),
				'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'], '20%'),
				'Venue' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'], '20%'),
				'Guest' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'], '20%'),
				'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'], '10%'),
				'Remark' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'], '10%'),
				'delete' => array('', '1')
		),
		array(
				'order' => array('#','1'),
				'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'], '25%'),
				'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'], '15%'),
				'Venue' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'], '15%'),
				'Guest' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'], '10%'),
				'ParticipatedSchool' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedSchool'], '15%'),
				'Target' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Target'], '10%'),
				'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'], '10%'),
				'Remark' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'], '10%'),
				'delete' => array('', '1')
		),
		array(
				'order' => array('#','1'),
				'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'], '20%'),
				'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'], '15%'),
				'Venue' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'], '15%'),
				'Guest' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'], '15%'),
				'Target' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Target'], '15%'),
				'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'], '10%'),
				'Remark' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'], '10%'),
				'delete' => array('', '1')
		),
		array(
				'order' => array('#','1'),
				'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'], '20%'),
				'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'], '15%'),
				'Venue' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'], '15%'),
				'Guest' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'], '15%'),
				'ParticipatedSchool' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedSchool'], '15%'),
				'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'], '10%'),
				'Remark' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'], '10%'),
				'delete' => array('', '1')
		)
);
$section4HeaderAry = array(
		array(
			'order' => array('#','1'),
			'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['AwardDate'], '20%'),
			'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['EventName'], '20%'),
			'Organization' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Organization'], '15%'),
			'ParticipatedGroup' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedGroup'], '20%'),
			'Award' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Award'], '10%'),
			'Remark' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'], '15%'),
			'delete' => array('', '1')
		),
		array(
			'order' => array('#','1'),
			'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['AwardDate'], '15%'),
			'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['EventName'], '15%'),
			'Organization' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Organization'], '10%'),
			'ParticipatedGroup' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedGroup'], '10%'),
			'Name' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Name'], '10%'),
			'Position' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Position'], '10%'),
			'Award' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Award'], '10%'),
			'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['TotalParticipant'], '10%'),
			'Remark' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'], '10%'),
			'delete' => array('', '1')
		),
		array(
			'order' => array('#','1'),
			'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['AwardDate'], '15%'),
			'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['EventName'], '15%'),
			'Organization' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Organization'], '10%'),
			'ParticipatedGroup' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedGroup'], '10%'),
			'Class' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Class'], '10%'),
			'Name' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Name'], '10%'),
			'Award' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Award'], '10%'),
			'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['TotalParticipant'], '10%'),
			'Remark' => array($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'], '10%'),
			'delete' => array('', '1')
		)
);
$widthAry = array();
foreach($headerAry as $i => $headerRow){
	foreach($headerRow as $header){
		$widthAry[$i][] = $header[1];
	}
}

foreach($section4HeaderAry as $x => $ary){
	foreach($ary as $header){
		$widthAry[$x+sizeof($headerAry)][] = $header[1];
	}
}
foreach($navs as $nav){
	$Page_NavArr[] = $nav;
}

if($AcademicYearID){
	$ecaAry = array();
}

// Warning Msg Box

// Submit Button
$SubmitBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Submit_Report();", $id="Btn_Submit");
// Submit Button
$DraftBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Draft_Report();", $id="Btn_Draft");
// Cancel Submit Button
$CancelSubmitBtn = $linterface->GET_ACTION_BTN($Lang['CEES']['Management']['SchoolActivityReport']['Category']['CancelSubmit'], "button", "js_CancelSubmit_Report();", $id="Btn_CancelSubmit");

if(!empty($SectionDataAry)){
	$AddRowScript = "";
	foreach($SectionDataAry as $section => $SectionData){
		foreach($SectionData as $data){
			$json = $libJSON->encode($data);
			$AddRowScript .= "js_AddRow('$section', $json);\n";
		}
	}
}
// debug_pr($AddRowScript);die();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
// echo $linterface->Include_Cookies_JS_CSS();
echo $linterface->Include_CopyPaste_JS_CSS('2016');
echo $linterface->Include_Excel_JS_CSS();
?>
<script>
var jsCurAcademicYearID = '<?php echo $AcademicYearID?>';
var SectionRowCount = <?=$sectionCountJson?>;
<?php foreach($widthAry as $k => $widthSettings){
	echo 'var widthSetting' . ($k == 0 ? '' : $k) . ' = [\'' . implode("','",$widthSettings) .'\'];' . "\n";
}?>
$(document).ready( function() {
	<?=$AddRowScript?>
	<?php if($isDraft || $isSubmit){?>
		$('input').attr('disabled',true);
		$('input[type=button]').removeAttr('disabled');
		$('textarea').attr('disabled',true);
	<?php }?>
});

function js_Draft_Report(){
	if(checkForm()){
		document.form1.action.value = 'draft';
		document.form1.setAttribute('action','?t=management.monthly_school_activity_report.update');
		document.form1.submit();
	}
}
function js_Submit_Report(){
	if(checkForm()){
		document.form1.action.value = 'submit';
		document.form1.setAttribute('action','?t=management.monthly_school_activity_report.update');
		document.form1.submit();
	}
}
function js_CancelSubmit_Report(){
	if(checkForm()){
		document.form1.action.value = 'release';
		document.form1.setAttribute('action','?t=management.monthly_school_activity_report.update');
		document.form1.submit();
	}
}
function js_Changed_AcademicYear_Selection(id){
	window.location.href = '?t=management.monthly_school_activity_report.index&AcademicYearID=' + id;
}
function js_Changed_ReportMonth_Selection(value){
	window.location.href = '?t=management.monthly_school_activity_report.index&AcademicYearID=' + $('#AcademicYearID').val() + '&ReportMonth=' + value;
}
function js_AddRow(targetTable, data){
	let division = targetTable.split('_');
	let id = SectionRowCount[division[0]][division[1]]++;
	tempData = {
		date: data['RecordDate'] ? data['RecordDate'] : '',
		evt: data['Event'] ? data['Event'] : '',
		venue: data['Venue'] ? data['Venue'] : '',
		guest: data['Guest'] ? data['Guest'] : '',
		participant: data['Participant'] ? data['Participant'] : '',
		target: data['Target'] ? data['Target'] : '',
		organization: data['Organization'] ? data['Organization'] : '',
		participatedGroup: data['ParticipatedGroup'] ? data['ParticipatedGroup'] : '',
		school: data['ParticipatedSchool'] ? data['ParticipatedSchool'] : '',
		award: data['Award'] ? data['Award'] : '',
		name: data['Name'] ? data['Name'] : '',
		position: data['Position'] ? data['Position'] : '',
		class: data['Class'] ? data['Class'] : '',
		remark: data['Remark'] ? data['Remark'] : ''
	}
	tableEle = $('#Section'+targetTable+'Div').find('table')[0];
	tableBody = $(tableEle).find('tbody')[0];
	tableBody = $(tableBody);
	lastRow = $(tableBody).find('tr').last();

	let newRow = $('<tr>').attr('id', 'Section'+targetTable+'_Row'+id);
	let emptyTD = $('<td>');
	newRow.append(emptyTD);
	if(targetTable=='1_1' || targetTable == '3_1'){
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Venue['+id+']" id="Section'+targetTable+'Venue['+id+']" class="input_text" />').val(tempData.venue));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Guest['+id+']" id="Section'+targetTable+'Guest['+id+']" class="input_text" />').val(tempData.guest));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'ParticipatedSchool['+id+']" id="Section'+targetTable+'ParticipatedSchool['+id+']" class="input_text" />').val(tempData.school));
		newRow.append(emptyTD);
		if(targetTable.charAt(0) == '2' || targetTable.charAt(0)=='3'){
			emptyTD = $('<td>');
			emptyTD.append( $('<input type="text" name="Section'+targetTable+'Target['+id+']" id="Section'+targetTable+'Target['+id+']" class="input_text" />').val(tempData.target));
			newRow.append(emptyTD);
		}
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Participant['+id+']" id="Section'+targetTable+'Participant['+id+']" class="input_number" />').val(tempData.participant));
		newRow.append(emptyTD);
	} else if(targetTable=='2_1'){
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Venue['+id+']" id="Section'+targetTable+'Venue['+id+']" class="input_text" />').val(tempData.venue));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Guest['+id+']" id="Section'+targetTable+'Guest['+id+']" class="input_text" />').val(tempData.guest));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'ParticipatedSchool['+id+']" id="Section'+targetTable+'ParticipatedSchool['+id+']" class="input_text" />').val(tempData.school));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Target['+id+']" id="Section'+targetTable+'Target['+id+']" class="input_text" />').val(tempData.target));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Participant['+id+']" id="Section'+targetTable+'Participant['+id+']" class="input_number" />').val(tempData.participant));
		newRow.append(emptyTD);
	}else if(targetTable!='4_1' && targetTable!='4_2' && targetTable!='4_3'){
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Venue['+id+']" id="Section'+targetTable+'Venue['+id+']" class="input_text" />').val(tempData.venue));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Guest['+id+']" id="Section'+targetTable+'Guest['+id+']" class="input_text" />').val(tempData.guest));
		newRow.append(emptyTD);
		if(targetTable.charAt(0) == '2' || targetTable.charAt(0)=='3'){
			emptyTD = $('<td>');
			emptyTD.append( $('<input type="text" name="Section'+targetTable+'Target['+id+']" id="Section'+targetTable+'Target['+id+']" class="input_text" />').val(tempData.target));
			newRow.append(emptyTD);
		}
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Participant['+id+']" id="Section'+targetTable+'Participant['+id+']" class="input_number" />').val(tempData.participant));
		newRow.append(emptyTD);
	} else if(targetTable=='4_1'){
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Organization['+id+']" id="Section'+targetTable+'Organization['+id+']" class="input_text" />').val(tempData.organization));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'ParticipatedGroup['+id+']" id="Section'+targetTable+'ParticipatedGroup['+id+']" class="input_text" />').val(tempData.participatedGroup));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Award['+id+']" id="Section'+targetTable+'Award['+id+']" class="input_text" />').val(tempData.award));
		newRow.append(emptyTD);
	} else if(targetTable=='4_2'){
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Organization['+id+']" id="Section'+targetTable+'Organization['+id+']" class="input_text" />').val(tempData.organization));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'ParticipatedGroup['+id+']" id="Section'+targetTable+'ParticipatedGroup['+id+']" class="input_text" />').val(tempData.participatedGroup));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Name['+id+']" id="Section'+targetTable+'Name['+id+']" class="input_text" />').val(tempData.name));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Position['+id+']" id="Section'+targetTable+'Position['+id+']" class="input_txet" />').val(tempData.position));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Award['+id+']" id="Section'+targetTable+'Award['+id+']" class="input_text" />').val(tempData.award));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Participant['+id+']" id="Section'+targetTable+'Participant['+id+']" class="input_number" />').val(tempData.participant));
		newRow.append(emptyTD);
	} else {
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Organization['+id+']" id="Section'+targetTable+'Organization['+id+']" class="input_text" />').val(tempData.organization));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'ParticipatedGroup['+id+']" id="Section'+targetTable+'ParticipatedGroup['+id+']" class="input_text" />').val(tempData.participatedGroup));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Class['+id+']" id="Section'+targetTable+'Class['+id+']" class="input_txet" />').val(tempData.class));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Name['+id+']" id="Section'+targetTable+'Name['+id+']" class="input_text" />').val(tempData.name));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Award['+id+']" id="Section'+targetTable+'Award['+id+']" class="input_text" />').val(tempData.award));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Participant['+id+']" id="Section'+targetTable+'Participant['+id+']" class="input_number" />').val(tempData.participant));
		newRow.append(emptyTD);
	}
	emptyTD = $('<td>');
	emptyTD.append( $('<input type="text" name="Section'+targetTable+'Remark['+id+']" id="Section'+targetTable+'Remark['+id+']" class="input_text" />').val(tempData.remark));
	newRow.append(emptyTD);
	<?php if(!$isDraft && !$isSubmit){ ?>
	emptyTD = $('<td>');
	let linkDel = $('<?=$linterface->GET_LNK_DELETE('#')?>');
	linkDel.find('a').attr('href', 'javascript:js_RemoveRow("Section'+targetTable+'_Row'+id+'")');
	emptyTD.append(linkDel);
	newRow.append(emptyTD);
	<?php }?>
	newRow.insertBefore(lastRow);

	js_InitDatePicker('Section'+targetTable+'_Date['+id+']');

	js_RefreshRowNum();
	js_RefreshRowWidth();
}
function js_RefreshRowWidth(){
	$('.record_table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
	$('#Section2_1Table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting1[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
	for(let i = 2; i<=3;i++) {
		$('#Section2_'+i+'Table tr:not(:last)').each(function(k, ele){
			$(ele).find('td').each(function(index, val){
				$(val).css('width', widthSetting2[index]);
				$(val).find('input').css('width','90%');
				$(val).find('textarea').css('width','90%');
			})
		});
	}
	for(let i = 1; i<=5;i++) {
		$('#Section3_'+i+'Table tr:not(:last)').each(function(k, ele){
			$(ele).find('td').each(function(index, val){
				$(val).css('width', widthSetting3[index]);
				$(val).find('input').css('width','90%');
				$(val).find('textarea').css('width','90%');
			})
		});
	}
	$('#Section4_1Table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting4[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
	$('#Section4_2Table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting5[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
	$('#Section4_3Table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting5[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
}
function js_RefreshRowNum(){
	$('.record_table').each(function(k, table){
			$(table).find('tr:not(:last)').each(function(index, val) {
			$(val).find('td').first().text(index);
		});
	});
}
function js_RemoveRow(id){
	$('#'+id).remove();
	js_RefreshRowNum();
}
function checkForm(){
	$('input.input_date').each(function(i, ele){ if(!check_date_30(ele, '<?=$Lang['General']['InvalidDateFormat']?>')) return false;});
	return true;
}
function js_AddDatePicker(id, val){
	let dom = '<input type="text" name="'+id+'" id="'+id+'" value="'+val+'" size="10" maxlength="10" class="textboxnum hasDatepick" onkeyup="Date_Picker_Check_Date_Format(this,\'DPWL-'+id+'\',\'\');">';
	return dom;
}
function js_AddDatePickerSpan(id){
	let dom = '<span style="color:red;" id="DPWL-'+id+'"></span>';
	return dom;
}
function js_InitDatePicker(id){
	$('input#' + id).ready(function(){
		$('input#' + id).datepick({
			
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0,
			onSelect: function(dateText, inst) {
				Date_Picker_Check_Date_Format(document.getElementById(id),'DPWL-' + id,'');
			}
		});
	});
}
function js_RetrieveData(section, target){
	let href = '?t=management.monthly_school_activity_report.ajax';
	$.ajax({
		url: href,
		data: {
			AcademicYearID: $('#AcademicYearID').val(),
			ReportMonth: $('#ReportMonth').val(),
			Action: 'sync'+section,
			ReportSection: target
		},
		dataType: 'JSON',
		method: 'POST',
		success: function(data){
// 			console.log(data);return;
			let confirm = window.confirm('<?=$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['RetrieveDataRow'] . ': '?>' + data.record.length + '<?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Row']?>');
			if(confirm){
				for(let i=0;i<data.record.length; i++){
					let recordRow = {};
					if(section=='SchoolAward'||section=='StudentAward'){
						recordRow = {
							RecordDate: data.record[i].AwardDate,
							Event: data.record[i].SubjectArea,
							Organization: data.record[i].Organization,
							Award: data.record[i].AwardName,
							Remark: data.record[i].Remark,
							ParticipatedGroup: data.record[i].ParticipatedGroup
						};
						if(section=='StudentAward'){
							recordRow.Class = data.record[i].ClassTitleEN;
							<?php if($intranet_session_language == 'en'){?>
							recordRow.Name = data.record[i].EnglishName;
							<?php }else{?>
							recordRow.Name = data.record[i].ChineseName;
							<?php }?>
							recordRow.Participant = data.record[i].TotalParticipant;
						}
					} else {
						recordRow = {
							RecordDate: data.record[i].EventDate,
							Event: data.record[i].EventTitle,
							Venue: data.record[i].Location,
							Participant: data.record[i].Total,
							Guest: data.record[i].Guest,
							ParticipatedSchool: data.record[i].ParticipatedSchool,
							Target: data.record[i].ActivityTarget
						};
					}
					js_AddRow(target, recordRow);
				}
			}
		}
	});
}

function js_ImportData(target){
	href = "?t=management.import&ReportType=0&ReportSection=" + target;
	newWindow(href, 16);
}
</script>
<form id="form1" name="form1" action="" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0" width='1000px'>
		<tr>
			<td style="padding-left:7px;">
				<br style="clear:both;" />
				<div class="table_filter">
					<?=$AcademicYearSelection?>
					<?=$MonthSelection.$Lang['General']['Month']?>
				</div>
				<br style="clear:both;" />
				<br style="clear:both;" />
				<?php if($AcademicYearID!=''){?>
					<table class='form_table_v30' width='50%'>
						<tbody>
							<tr>
								<td class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ReportDate']?></td>
								<td class="field_content_short">
									<?=$linterface->GET_DATE_PICKER('ReportDate', $ECAReport['ReportDate'])?>
								</td>
							</tr>
						</tbody>
					</table>
					<?php $i = 0;
					foreach($Page_NavArr as $Page_Nav){
						$i++; 
						$y=0;
						?>
						<hr/>
						<?=$linterface->GET_NAVIGATION2($Page_Nav);
							foreach($subNav[$i-1] as $sNav){
								$y++;
								if( ($i=='1'&&$y=='1')) $tempHeaderAry = $headerAry[3];
								elseif(( $i=='2'&&$y=='1')  || ($i=='3'&&$y=='1'))$tempHeaderAry = $headerAry[1];
								else if($i=='1') $tempHeaderAry = $headerAry[0];
								else if($i!='1'&&!i!='4') $tempHeaderAry = $headerAry[2];
								if($i=='4') $tempHeaderAry = $section4HeaderAry[$y-1];
						?>
							<br style="clear:both;" />
							<?php if( ($i=='1'&&$y=='1') || ($i=='2'&&$y=='1')  || ($i=='3'&&$y=='1')){?>
								<h3><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool']?>: </h3>
							<?php } else if(($i=='1'&&$y=='2') || ($i=='2'&&$y=='2') || ($i=='3'&&$y=='2')) {?>
								<h3><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool']?>: </h3>
							<?php }?>
							<?php if($i==1||$i==2||$i==3){
									if($y!=1){?>
								<h3><?=chr(64+$y-1).'. '.$sNav?></h3>
								<?php }else {?>
								<h3><?=$sNav?></h3>
								<?php }
							} else {?>
								<h3><?=chr(64+$y).'. '.$sNav?></h3>
							<?php }?>
							<div id="Section<?=$i.'_'.$y?>Div">
								<div class="Conntent_tool">
								<?php  if(!$isDraft && !$isSubmit) {
									if($i!=4){
										echo $linterface->GET_LNK_IMPORT("javascript:js_RetrieveData('Enrolment', '$i".'_'."$y')", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromeEnrolment'], $ParOnClick="");
										if( ($i==1)){
											echo $linterface->GET_LNK_IMPORT("javascript:js_ImportData('$i".'_'."$y')", $Lang['General']['Import'], $ParOnClick="");
										}
									}else  if($i==4&&$y==1){
										echo $linterface->GET_LNK_IMPORT("javascript:js_RetrieveData('SchoolAward', '$i".'_'."$y')", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromiPortoflio'], $ParOnClick="");
									} else if($i==4&&$y==3){
										echo $linterface->GET_LNK_IMPORT("javascript:js_RetrieveData('StudentAward', '$i".'_'."$y')", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromiPortoflio'], $ParOnClick="");
									} else if($i==4&&$y==2){
										echo $linterface->GET_LNK_IMPORT("javascript:js_ImportData('$i".'_'."$y')", $Lang['General']['Import'], $ParOnClick="");
									}
								}
									?>
								</div>
								<table id="Section<?=$i.'_'.$y?>Table" class="common_table_list_v30 edit_table_list_v30 record_table" width='100%'>
									<thead>
										<?php foreach($tempHeaderAry as $key => $header){?>
											<th width='<?=$header[1]?>'><?=$header[0]?></th>
										<?php }?>
									</thead>
									<tbody>
										<tr><td colspan='<?=sizeof($tempHeaderAry)?>'>
										<?php if(!$isDraft && !$isSubmit){ ?>
										<div class="table_row_tool row_content_tool"><a class="add_dim" href="javascript:js_AddRow('<?=$i.'_'.$y?>', {})"></a></div>
										<?php }?>
										</td></tr>
									</tbody>
								</table>
							</div>
							<br style="clear:both;" />
						<?php }?>
					<?php }?>
					<input type='hidden' name='action' id='action' value="" />
					<div class="edit_bottom_v30">
						<?php if(!$isDraft && !$isSubmit){
							echo $DraftBtn;
							if($ECAReport['ReportID']){
								echo '&nbsp;&nbsp;';
								echo $SubmitBtn;
							}
						} else if($isDraft && !$isSubmit){
							echo $CancelSubmitBtn;
						}?>
					</div>
				<?}?>
			</td>
		</tr>
	</table>
	
</form>

<?php
$linterface->LAYOUT_STOP();
?>