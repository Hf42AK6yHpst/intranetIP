<?php 
## Using By :
##############################################
##	Modification Log:
##	2020-06-02 (Philips)
##	- create this file
##
##############################################
include_once($PATH_WRT_ROOT . 'includes/json.php');
######## Init START ########
$libSDAS = new libSDAS();
$libJSON = new JSON_obj();
######## Init END ########
$Action = $_POST['Action'];
$ReportStartDate = $_POST['ReportStartDate'];
$ReportEndDate = $_POST['ReportEndDate'];
$AcademicYearID= $_POST['AcademicYearID'];
$ReportSection = $_POST['ReportSection'];
$ActivityPrefix = 'I';
$CategoryAry = array(
	'1' => array('SchoolAdministration','ProfessionalDevelopmentAndTrainning','InterflowAndSharing'),
	'2' => array('InsideSchoolActivity', 'OutsideSchoolActivity'),
	'3' => array('ParentSchoolActivity','ParentEducation','OtherSupportActivity','Others'),
);
switch($Action){
	case 'syncEnrolment':
		$sectionAry = explode('_', $ReportSection);
		$table = 'INTRANET_ENROL_EVENTINFO';
		$joinTable = 'INTRANET_ENROL_EVENT_DATE';
		$joinTable2 = 'INTRANET_ENROL_EVENTSTUDENT';
		
		// 2020-07-06 (Philips) - Section 1 no need to check member
		if($sectionAry[0]!='1'){
			//$join2 = "INNER JOIN $joinTable2 c
			$join2 = "LEFT JOIN $joinTable2 c
			ON a.EnrolEventID = c.EnrolEventID";
		} else {
			$join2 = "LEFT OUTER JOIN $joinTable2 c
			ON a.EnrolEventID = c.EnrolEventID";
		}
		
		//GROUP_CONCAT(DATE_FORMAT(b.ActivityDateStart, "%Y-%m-%d"))
		$cols = 'a.EventTitle, a.SDAS_Category, a.SDAS_AllowSync, GROUP_CONCAT(DISTINCT DATE_FORMAT(b.ActivityDateStart, "%Y-%m-%d")) AS EventDate, a.Location, IF(a.TotalParticipant NOT IN (0, ""), a.TotalParticipant, COUNT(*)) AS Total,
				 a.ActivityTarget, a.Guest, a.ParticipatedSchool';
		$targetCategory = $ActivityPrefix . $sectionAry[0]. '_'.$CategoryAry[$sectionAry[0]][$sectionAry[1]-1];
		$conds = " a.SDAS_AllowSync LIKE '%I%' AND a.SDAS_Category = '$targetCategory' ";
		$conds .= " AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') >= '$ReportStartDate' AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') <= '$ReportEndDate' ";
		$sql = "SELECT $cols FROM $table a
		INNER JOIN $joinTable b
		ON a.EnrolEventID = b.EnrolEventID
		$join2
		WHERE $conds
		Group By a.EnrolEventID
		";
// 				debug_pr($sql);
		$recordAry = $libSDAS->returnArray($sql);
		$result['record'] = $recordAry;
		echo $libJSON->encode($result);
		break;
		break;
	case 'syncSchoolAward':
		$table = $eclass_db . '.AWARD_SCHOOL';
		$col = "AwardDate, AwardName, AwardFile, Details, Organization, SubjectArea, ParticipatedGroup";
		$conds = "AcademicYearID = '$AcademicYearID' AND AwardDate >= '$ReportStartDate' AND AwardDate <= '$ReportEndDate' AND SDAS_AllowSync LIKE '%I%' ";
		$sql = "SELECT $col FROM $table WHERE $conds";
		$recordAry = $libSDAS->returnArray($sql);
		$result['record'] = $recordAry;
		echo $libJSON->encode($result);
		break;
	case 'syncStudentAward':
		$table = $eclass_db . '.AWARD_STUDENT';
		$col = "a.AwardDate, a.AwardName, a.AwardFile, a.Details, a.Organization, a.SubjectArea, a.ParticipatedGroup,
				GROUP_CONCAT(iu.EnglishName) AS EnglishName, GROUP_CONCAT(iu.ChineseName) AS ChineseName,
				GROUP_CONCAT(DISTINCT yc.ClassTitleB5) AS ClassTitleB5, GROUP_CONCAT(DISTINCT yc.ClassTitleEN) AS ClassTitleEN, COUNT(*) AS TotalParticipant
				";
		$conds = "a.AcademicYearID = '$AcademicYearID' AND a.AwardDate >= '$ReportStartDate' AND a.AwardDate <= '$ReportEndDate' AND a.SDAS_AllowSync LIKE '%I%' ";
		$sql = "SELECT $col FROM $table a
				INNER JOIN INTRANET_USER iu
					ON iu.UserID = a.UserID
				INNER JOIN YEAR_CLASS_USER ycu
					ON iu.UserID = ycu.UserID
				INNER JOIN YEAR_CLASS yc
					ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = a.AcademicYearID
				WHERE $conds
				GROUP BY a.AwardDate, AwardName, a.Organization, a.ParticipatedGroup";
		$recordAry = $libSDAS->returnArray($sql);
// 		debug_pr($sql);die();
		$result['record'] = $recordAry;
		echo $libJSON->encode($result);
		break;
	case 'addReport':
		$reportID = $libSDAS->insertIntraSchoolECA($AcademicYearID, array('ReportStartDate'=> date('Y-m-d'), 'ReportEndDate' => date('Y-m-d')), array(),'', $isNew=true);
		echo $reportID;
		break;
	case 'removeReport':
		$ReportID = $_POST['ReportID'];
		$existReport = $libSDAS->getIntraSchoolECA($AcademicYearID, $ReportID);
		$isSubmit = $existReport['SubmitStatus'];
		$isDraft =  $existReport['DraftStatus'];
		if(!$isSubmit && !$isDraft){
			$result = $libSDAS->removeIntraSchoolECA($ReportID);
			echo $result;
		} else {
			echo false;
		}
		break;
	default:
		break;
}
?>