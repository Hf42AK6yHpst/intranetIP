<?php
## Using By : 
##############################################
##	Modification Log:
##	2020-05-29 (Philips)
##	- create this file
##
##############################################

$AcademicYearID = $_POST['AcademicYearID'];
if(!$AcademicYearID){
	No_Access_Right_Pop_Up('','/');
	die();
}
######## Init START ########
$libSDAS = new libSDAS();
######## Init END ########
$Action = $_POST['action'];
$ReportStartDate = $_POST['ReportStartDate'];
$ReportEndDate = $_POST['ReportEndDate'];
$ReportID = $_POST['ReportID'];
$InfoData = array(
	'ReportStartDate' => $ReportStartDate,
	'ReportEndDate' => $ReportEndDate,
	'DraftStatus' => $Action == 'submit' ? '1' : '0'
);
$SectionCount = 4;
$SectionAry = array(
	0,3,2,4,3
);
$DataAry = array();
for($i=1; $i<=$SectionCount; $i++){
	for($j=1;$j<=$SectionAry[$i]; $j++){
		if($i == 1){
			$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
			$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
			$venueAry = $_POST['Section'.$i.'_'.$j.'Venue'];
			$guestAry = $_POST['Section'.$i.'_'.$j.'Guest'];
			$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
			foreach((array)$dateAry as $k => $v){
				$record = array(
					'Section' => $i.'_'.$j,
					'RecordDate' => htmlspecialchars($v),
					'Event' => htmlspecialchars($eventAry[$k]),
					'Venue' => htmlspecialchars($venueAry[$k]),
					'Guest' => htmlspecialchars($guestAry[$k]),
					'Participant' => htmlspecialchars($participantAry[$k])
				);
				$DataAry[] = $record;
			}
		} else if($i==2 || $i==3){
			$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
			$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
			$venueAry = $_POST['Section'.$i.'_'.$j.'Venue'];
			$guestAry = $_POST['Section'.$i.'_'.$j.'Guest'];
			$targetAry = $_POST['Section'.$i.'_'.$j.'Target'];
			$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
			foreach((array)$dateAry as $k => $v){
				$record = array(
						'Section' => $i.'_'.$j,
						'RecordDate' => htmlspecialchars($v),
						'Event' => htmlspecialchars($eventAry[$k]),
						'Venue' => htmlspecialchars($venueAry[$k]),
						'Guest' => htmlspecialchars($guestAry[$k]),
						'Target' => htmlspecialchars($targetAry[$k]),
						'Participant' => htmlspecialchars($participantAry[$k])
				);
				$DataAry[] = $record;
			}
		} else if($j == 1){
			// Section 4_1
			$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
			$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
			$organization = $_POST['Section'.$i.'_'.$j.'Organization'];
			$pgAry = $_POST['Section'.$i.'_'.$j.'ParticipatedGroup'];
			$awardAry = $_POST['Section'.$i.'_'.$j.'Award'];
			foreach((array)$dateAry as $k => $v){
				$record = array(
						'Section' => $i.'_'.$j,
						'RecordDate' => htmlspecialchars($v),
						'Event' => htmlspecialchars($eventAry[$k]),
						'Organization' => htmlspecialchars($organization[$k]),
						'ParticipatedGroup' => htmlspecialchars($pgAry[$k]),
						'Award' => htmlspecialchars($awardAry[$k]),
				);
				$DataAry[] = $record;
			}
		} else if($j == 2) {
			// Section 4_2
			$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
			$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
			$organization = $_POST['Section'.$i.'_'.$j.'Organization'];
			$pgAry = $_POST['Section'.$i.'_'.$j.'ParticipatedGroup'];
			$nameAry = $_POST['Section'.$i.'_'.$j.'Name'];
			$positionAry = $_POST['Section'.$i.'_'.$j.'Position'];
			$awardAry = $_POST['Section'.$i.'_'.$j.'Award'];
			$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
			foreach((array)$dateAry as $k => $v){
				$record = array(
						'Section' => $i.'_'.$j,
						'RecordDate' => htmlspecialchars($v),
						'Event' => htmlspecialchars($eventAry[$k]),
						'Organization' => htmlspecialchars($organization[$k]),
						'ParticipatedGroup' => htmlspecialchars($pgAry[$k]),
						'Name' => htmlspecialchars($nameAry[$k]),
						'Position' => htmlspecialchars($positionAry[$k]),
						'Award' => htmlspecialchars($awardAry[$k]),
						'Participant' => htmlspecialchars($participantAry[$k])
				);
				$DataAry[] = $record;
			}
		} else {
			// Section 4_3
			$dateAry = $_POST['Section'.$i.'_'.$j.'Date'];
			$eventAry = $_POST['Section'.$i.'_'.$j.'Event'];
			$organization = $_POST['Section'.$i.'_'.$j.'Organization'];
			$pgAry = $_POST['Section'.$i.'_'.$j.'ParticipatedGroup'];
			$classAry = $_POST['Section'.$i.'_'.$j.'Class'];
			$nameAry = $_POST['Section'.$i.'_'.$j.'Name'];
			$awardAry = $_POST['Section'.$i.'_'.$j.'Award'];
			$participantAry = $_POST['Section'.$i.'_'.$j.'Participant'];
			foreach((array)$dateAry as $k => $v){
				$record = array(
						'Section' => $i.'_'.$j,
						'RecordDate' => htmlspecialchars($v),
						'Event' => htmlspecialchars($eventAry[$k]),
						'Organization' => htmlspecialchars($organization[$k]),
						'ParticipatedGroup' => htmlspecialchars($pgAry[$k]),
						'Class' => htmlspecialchars($classAry[$k]),
						'Name' => htmlspecialchars($nameAry[$k]),
						'Award' => htmlspecialchars($awardAry[$k]),
						'Participant' => htmlspecialchars($participantAry[$k])
				);
				$DataAry[] = $record;
			}
		}
	}
}
// Section 4_2
$result = $libSDAS->insertIntraSchoolECA($AcademicYearID, $InfoData, $DataAry,$ReportID);
// debug_pr($DataAry);die();
if($result){
	// update success
	$xmsg = "UpdateSuccess";
} else {
	// update failed
	$xmsg = "UpdateUnsuccess";
}
header('location: ?t=management.intra_school_activity_report.index&AcademicYearID='.$AcademicYearID.'&ReportID='.$ReportID.'&xmsg='.$xmsg);