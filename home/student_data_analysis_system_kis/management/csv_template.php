<?php
## Using By :
##############################################
##	Modification Log:
##	2020-06-19 (Philips)
##	- create this file
##
##############################################
include_once($PATH_WRT_ROOT . 'includes/libexporttext.php');
######## Init START ########
$libSDAS = new libSDAS();
$lexport = new libexporttext();
######## Init END ########
$section = $_REQUEST['section'];
$reportType = $_REQUEST['reportType'];
$headerAry = array();
$contentData = array();
$filename = 'csv_tempalte.csv';
$sectionAry = explode('_',$section);
if($reportType==1){
	$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Date'];
	$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Event'];
	$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Venue'];
	$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Guest'];
	$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ParticipatedSchool'];
	$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Target'];
	$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['TotalParticipant'];
}else if($reportType==2){ 
	if($sectionAry[0]=='1'){
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Date'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Venue'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Guest'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Participant'];
	} else if($sectionAry[0]=='4'){ 
		if($section[1]=='1'){ 
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['AwardDate'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Organization'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Award'];
		} else if($sectionAry[1]=='2'){
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['AwardDate'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Organization'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Name'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Position'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Award'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['TotalParticipant'];
		} else if($sectionAry[1]=='3') {
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['AwardDate'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Organization'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Class'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Name'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Award'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['TotalParticipant'];
		}
	} else {
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Date'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Venue'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Guest'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Target'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Participant'];
	}
} else if($reportType==0){
	if($sectionAry[0]=='1'){
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'];
		$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'];
	} else if($sectionAry[0]=='4'){
		if($sectionAry[1]=='1'){
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['AwardDate'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Organization'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedGroup'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Award'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'];
		} else if($sectionAry[1]=='2'){
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['AwardDate'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Organization'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedGroup'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Name'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Position'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Award'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['TotalParticipant'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'];
		} else if($sectionAry[1]=='3') {
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['AwardDate'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Organization'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedGroup'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Class'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Name'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Award'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['TotalParticipant'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'];
		}
	} else {
		if($sectionAry[0]=='2' && $sectionAry[1]=='1'){
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedSchool'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Target'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'];
		} else {
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Target'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'];
			$headerAry[] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'];
		}
	}
}
$exportData = $lexport->GET_EXPORT_TXT2($contentData, $headerAry);
$lexport->EXPORT_FILE($filename, $exportData);