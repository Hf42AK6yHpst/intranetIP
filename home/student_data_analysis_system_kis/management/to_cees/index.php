<?php
// Modifing by 
################# Change Log [Start] #####
#				Page Created
#
################## Change Log [End] ######


######## Init START ########
// include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
// $libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$libfcm_ui = new form_class_manage_ui();
$lay = new academic_year();
// $lpf = new libPortfolio();
// $li_pf = new libpf_asr();
// $libFCM_ui = new form_class_manage_ui();
// $libSCM_ui = new subject_class_mapping_ui();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "management.CEES";
$CurrentPageName = $Lang['SDAS']['menu']['CEES'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

######## UI START ########
$linterface->LAYOUT_START();
$academic_year_arr = $lay->Get_All_Year_List();
foreach((array)$academic_year_arr as $_key => $_infoArr){
	$academic_year_arr[$_key]['1'] = iconv('Big5-HKSCS','UTF-8',$_infoArr['1']);
	$academic_year_arr[$_key]['AcademicYearName'] = iconv('Big5-HKSCS','UTF-8',$_infoArr['AcademicYearName']);
}
if($ReportMonth == ''){
	$ReportMonth= date('m');
}
$AcademicYearSelection = '<label>'.$Lang['SDAS']['toCEES']['SchoolYear'].'</label>'."\r\n";;
$AcademicYearSelection .= getSelectAcademicYear('AcademicYearID', $tag='onchange="loadAjax();"', $noFirst=0, $noPastYear=0);
$AcademicYearSelection .= '&nbsp;<a href="javascript:void(0);" id="refresh_btn" class="tablelink">'.$i_general_refresh.'</a>';
$AcademicYearSelection .= '<span id="ajax_loading"></span>';

$MonthSelection= '<label>'.$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Month'].'</label>'."\r\n";;
$MonthSelection .= $libSDAS->getMonthSelection($AcademicYearID,$ReportMonth, "name='ReportMonth' id='ReportMonth' onChange='loadAjax_Monthly();'");
$MonthSelection.= '&nbsp;<a href="javascript:void(0);" id="refresh_btn_monthly" class="tablelink">'.$i_general_refresh.'</a>';
$MonthSelection.= '<span id="ajax_loading_monthly"></span>';
// $AcademicYearSelection = 'villa';
// $AcademicYearSelection = '<label>'.$Lang['SDAS']['toCEES']['SchoolYear'].'</label>'."\r\n";;
// $AcademicYearSelection .= getSelectAcademicYear('AcademicYearID', $tag='onchange="loadAjax();"', $noFirst=0, $noPastYear=0);
// $AcademicYearSelection .= '&nbsp;<a href="javascript:void(0);" id="refresh_btn" class="tablelink">'.$i_general_refresh.'</a>';
// $AcademicYearSelection .= '<span id="ajax_loading"></span>';

# Table Open
$TableOpen =  '<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC" id="ajax_reload_table">';
# Table Close
$TableClose = "</table>";

# Monthly Table Open
$MonthlyTableOpen =  '<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC" id="ajax_reload_table_monthly">';
# Monthly Table Close
$MonthlyTableClose = "</table>";
?>
<script>
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
$(document).ready(function(){
	loadAjax();
	$('#refresh_btn').click(function(){
		loadAjax();
	});
	$('#refresh_btn_monthly').click(function(){
		loadAjax_Monthly();
	});
});
function js_Changed_ReportMonth_Selection(value){
	window.location.href = '?t=management.monthly_school_activity_report.index&AcademicYearID=' + $('#AcademicYearID').val() + '&ReportMonth=' + value;
}
function loadAjax() {

	var ReportID = [];
	for(let i=0;i<=1;i++){
		ReportID.push($('#ReportID_'+i).val());
	}
	$('#refresh_btn').hide();
	$('#ajax_reload_table').html('');
	var url = '?t=management.to_cees.ajax_reload';
	var ay = $('#AcademicYearID').val();
	$('#ajax_loading').html(ajaxImage);
	$.ajax({
           type:"POST",
           url: url,
           data: { "ay_id" : ay,"action":"reload_table", "ReportAry": ReportID},
           success:function(responseText)
           {
				$('#ajax_reload_table').html(responseText);
				//$("select[name=YearTermID] option[value='']").text("<?=$ec_iPortfolio['overall_result']?>");
           		$('#ajax_loading').html('');
           		$('#refresh_btn').show();
           }
    });
	loadAjax_Monthly();
}
function loadAjax_Monthly() {
	
	$('#refresh_btn_monthly').hide();
	$('#ajax_reload_table_monthly').html('');
	var url = '?t=management.to_cees.ajax_reload';
	var ay = $('#AcademicYearID').val();
	var month = $('#ReportMonth').val();
	$('#ajax_loading_monthly').html(ajaxImage);
	$.ajax({
           type:"POST",
           url: url,
           data: { "ay_id" : ay,"month": month, "action":"reload_table_monthly"},
           success:function(responseText)
           {
				$('#ajax_reload_table_monthly').html(responseText);
           		$('#ajax_loading_monthly').html('');
           		$('#refresh_btn_monthly').show();
           }
    });
}

function syncToCEES(exam,academicYearId, month, reportID){
	if($('#confirmAppealNoData').length == 1 && $('#confirmAppealNoData:checked').length == 0 && exam==7){
		alert("<?php echo $Lang['SDAS']['toCEES']['PleaseConfirmAppealNoData']?>");
		console.log('herere22s');
		return false;
	} 

	var url = '?t=management.to_cees.sync';
	var confirmNoData = $('#confirmAppealNoData:checked').length == 1 ? 1:0;
	
	$.ajax({
        type:"POST",
        url: url,
        /*async: false,*/
        data: { "academicYearID" : academicYearId, "month": month ,"reportType": exam, "confirmAppealNoData":confirmNoData, "reportID": reportID},
        success:function(responseText)
        {
// 			$('div#debugOnly').html(responseText);
			
			if(responseText==1){
				//Successfully sync
				Get_Return_Message('<?= $Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['Sync'] ?>');
			}else if(responseText==0){
				//Failed
				Get_Return_Message('<?= $Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['Sync'] ?>');
			}else{
				alert(responseText);
			}
			loadAjax();
        }
        
 });
}
</script>

 <?= $AcademicYearSelection?>
  <br/>
 <br/>
 <?=$linterface->GET_NAVIGATION2($Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'])?>
 <?=$TableOpen.$TableClose?>
 <br/>
 <br/>
 <?=$linterface->GET_NAVIGATION2($Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title'])?>
 <br/>
 <?=$MonthSelection?>
 <?=$MonthlyTableOpen.$MonthlyTableClose?>

<form id="hiddenForm" method="POST" action="?t=management.data_import.exam.data_export_update">
<input type="hidden" name="exam" id="exam" value="">
<input type="hidden" name="academicYearID" id="academicYearID" value="">
<input type="hidden" name="report_type" id="report_type" value="">
</form>
<div id="debugOnly"></div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>