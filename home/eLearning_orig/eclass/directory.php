<?php
/* Modify by sandy */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");

$linterface 	= new interface_html();
$CurrentPage	= "PageDir";
$leclass 	= new libeclass2007();

$li = new libuser($UserID);
$lo = new libeclass();
$scm = new subject_class_mapping();
 
$directory = $scm->Get_Subject_Directory(); //debug_r($directory);
$selectedView = ($SubjectID == "") ? "" : "<tr><td>".$leclass->displayEClassDirectoryView12($SubjectID,$directory,$catID)."</td></tr>\n";
//$selectedView = ($SubjectID == "") ? "" : "<tr><td>".$leclass->displayEClassDirectoryView12($SubjectID)."</td></tr>\n";

### Title ###
$TAGS_OBJ[] = array("<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_directory.gif' align='absmiddle' /> " . $i_frontpage_eclass_eclass_dir,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center">
      <table width="96%" border="0" cellspacing="0" cellpadding="0">
        <?=$selectedView?>
        <tr>
          <td align="left" class="tabletext">
            <?=$leclass->displayEClassDirectory12($directory)?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br />


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>