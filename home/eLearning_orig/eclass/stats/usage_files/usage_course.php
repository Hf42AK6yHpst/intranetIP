<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once("$eclass40_filepath/src/includes/php/lib-quota.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageStatistics";

$lo = new libeclass($course_id);
$leclass = new libeclass2007();

# Left menu 
$title = $i_eClass_Admin_Stats;
$TAGS_OBJ[] = array($i_eClass_Admin_stats_files,"index.php",1);
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$classrm = $lo->course_code ." ". $lo->course_name;

$IP_image_path = $image_path; // tmp storage
$sys_db = $eclass_db;
$image_path = $eclass_url_root."/images";
$qo = new quota($course_id);
$fm = new fileManager($course_id, $categoryID, $folderID);
?>


<form name="form1" method="get">
<table width="80%" border="0" cellspacing="0" cellpadding="5">

<tr>
	<td align="left" valign="bottom"><?= $classrm ?></td>
</tr>
<tr> 
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2"><?= $qo->statusStorageDetailIP() ?></td>
			</tr>
		</table>
	</td>
</tr>
<?
$image_path = $IP_image_path; // re-define IP25 image path
?>
<tr>
	<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
</tr>

</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<p></p>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
