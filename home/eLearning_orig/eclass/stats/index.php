<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageStatistics";

$lo = new libeclass();
$leclass = new libeclass2007();

# Left menu 
$title = $i_eClass_Admin_Stats;
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<table width="80%" border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300 align=left>
<blockquote>
<!--
<?= displayOption($i_eClass_Admin_stats_login, 'usage_login/', 1,
                                $i_eClass_Admin_stats_participation, 'participation/', 1,
                                $i_eClass_Admin_stats_files, 'usage_files/', 1
                                ) ?>
-->
<?= displayOption($i_eClass_Admin_stats_files, 'usage_files/', 1) ?>
</blockquote>
</td></tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
