<?php
//editing: Stanley
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
eclass_opendb();
// debug_r($_SESSION);
# eClass
$lo 		= new libeclass();
$lelcass	= new libeclass2007();
$eclass_quota 	= $lo->status();
$lu 		= new libuser($UserID);

# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
  {
    header("Location: /");
  }
}

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

#### subject and subject group select box #####
$libdb = new libdb();
$fieldName = $intranet_session_language=="en"?"EN_DES":"CH_DES";
$sql = "select RecordID, $fieldName as SubjectName from ASSESSMENT_SUBJECT where RecordStatus = 1 AND
(CMP_CODEID IS NULL OR CMP_CODEID = '')";
$result = $libdb->returnArray($sql);
$subjectBox = '<select id="subjectID" name="subjectID" onchange="getSelectGroup()">';
$subjectBox .= "<option value=''>--{$button_select}--</option>";
foreach ($result as $r){
	$subjectBox .= '<option value="'.$r["RecordID"].'" '.$selected.'>'.$r["SubjectName"].'</option>';
}
$subjectBox .= '</select>';
$subjectBox .= '&nbsp;&nbsp;<div id="subjectGroupSelectBox" style="display:inline"></div>';
###############################################


$tool_select = "";
# Check Equation Editor
if ($lo->license_equation !=0)
{
	$content = trim(get_file_content($lo->filepath."/files/equation.txt"));
	$equation_class = ($content=="")? array(): explode(",",$content);
	$left = $lo->license_equation - sizeof($equation_class);
	if ($left > 0)
	{
		$tool_select .= "<input type=checkbox name=hasEquation value=1> $i_eClass_Tool_EquationEditor ($i_eClass_Tool_LicenseLeft: $left)";
	}
}

### Title ###
$TAGS_OBJ[] = array($i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $lelcass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new.($intranet_session_language=="en"?" ":""). $i_adminmenu_eclass, "");
?>
<script language="javascript">
function checkform(obj){
  if(!check_text(obj.course_code, "<?php echo $i_alert_pleasefillin.$i_eClassCourseCode; ?>.")) return false;
  if(!check_text(obj.course_name, "<?php echo $i_alert_pleasefillin.$i_eClassCourseName; ?>.")) return false;
  if (document.getElementById('subjectID').selectedIndex > 0 && document.getElementById('subjectGroupID').selectedIndex == 0){
	alert('<?=$Lang['eclass']['warning']['noSubjectGroup']?>');
  }
}

function getSelectGroup(){
	selectedValue = document.getElementById('subjectID').options[document.getElementById('subjectID').selectedIndex].value;
	document.body.style.cursor='wait';
	$('div#subjectGroupSelectBox').load(
		'getSelectSubjectGroup.php',
		{subjectID: selectedValue},
		function(){
			document.body.style.cursor='default';
		}
	);
}
</script>


<br />
<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
      <td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
    </tr>

    <tr>
      <td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td><br />
              <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
<? if($lo->license == "" || $lo->ticket()>0){ ?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseCode?> <span class='tabletextrequire'>*</span></span></td>
                  <td><input name="course_code" type="text" class="textboxnum" maxlength="10" value="" /></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseName?> <span class='tabletextrequire'>*</span></span></td>
                  <td><input name="course_name" type="text" class="textboxtext" maxlength="100" value="" /></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseDesc?> </span></td>
                  <td><?=$linterface->GET_TEXTAREA("course_desc", "");?></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassNumUsers?> </span></td>
                  <td><input name="max_user" type="text" class="textboxnum" maxlength="5" value="30" /></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassMaxStorage?> </span></td>
                  <td class="tabletext"><input name="max_storage" type="text" class="textboxnum" maxlength="5" value="30" /> MB</td>
                </tr>

				<!-- subject group -->
				<tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></span></td>
                  <td class="tabletext"><?=$subjectBox?></td>
                </tr>
				
<? if ($tool_select != "") { ?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClass_Tools?> </span></td>
                  <td class="tabletext"><?=$tool_select?></td>
                </tr>
<? } ?>
<!--
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
                  <td class="tabletext">
				  <?php //echo $lo->eClassCategory(); ?>
				  </td>
                </tr>
-->
<? } else {?>
                <tr valign="top">
                  <td class="tabletext" colspan="2" align="center"><?=$i_eClassLicenseFull?></td>
                </tr>
<? } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<? if($lo->license == "" || $lo->ticket()>0){ ?>
          <tr>
            <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
          </tr>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
              <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
<? } else {?>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
<? } ?>
        </table>
      </td>
    </tr>
  </table>
  <br />
</form>

<?php
print $linterface->FOCUS_ON_LOAD("form1.course_code");
$linterface->LAYOUT_STOP();
eclass_closedb();
?>