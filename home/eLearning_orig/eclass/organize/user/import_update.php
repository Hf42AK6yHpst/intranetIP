<?php
# editing : Kit
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
intranet_auth();
eclass_opendb();

$li = new libdb();
$lo = new libeclass($course_id);
$iCal = new icalendar();

# to avoid putting null ID
$ChooseUserIDTmp = $ChooseUserID;
unset($ChooseUserID);

for ($i=0; $i<sizeof($ChooseUserIDTmp); $i++)
{
	$UserIDCur = (int) trim($ChooseUserIDTmp[$i]);
	if ($UserIDCur>0)
	{
		$ChooseUserID[] = $UserIDCur;
	}
}

if(sizeof($ChooseUserID) > 0){
  //$sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, CASE Title WHEN 0 THEN 'MR' WHEN 0 THEN 'MR.' WHEN 1 THEN 'MISS.' WHEN 2 THEN 'MRS.' WHEN 3 THEN 'MS.' WHEN 4 THEN 'DR.' WHEN 5 THEN 'PROF.' END , EnglishName, ChineseName , NickName   FROM INTRANET_USER WHERE UserID IN (".implode(",", $ChooseUserID).")";
  $sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, Title, EnglishName, ChineseName , NickName   FROM INTRANET_USER WHERE UserID IN (".implode(",", $ChooseUserID).")";
  $row = $li->returnArray($sql,11);

  for($i=0; $i<sizeof($row); $i++){
    if($lo->max_user <> "" && $i == $lo->ticketUser()) break;

    $User_ID = $row[$i][0];
    $UserEmail = $row[$i][1];
    $UserPassword = $row[$i][2];
    $ClassNumber = $row[$i][3];
    $FirstName = $row[$i][4];
    $LastName = $row[$i][5];
    $ClassName = $row[$i][6];
    $Title = $row[$i][7];
    $EngName = $row[$i][8];
    $ChiName = $row[$i][9];
    $NickName = $row[$i][10];
    if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
      $eclassClassNumber = $ClassNumber;
    else
      $eclassClassNumber = $ClassName ." - ".$ClassNumber;
    $lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info, "", $User_ID);
  }
  $lo->eClassUserNumber($lo->course_id);

	# insert calendar viewer
	$sql = "select CalID from course
		where course_id = '$course_id'";
	$calID = $lo->returnVector($sql);
	$sql = "select UserID from CALENDAR_CALENDAR
			where CalID = '".$calID[0]."'";
	$existUser = $iCal->returnVector($sql);
	$newUser = array_diff($ChooseUserID,$existUser);
	$iCal->insertCalendarViewer($calID[0],$newUser,"W",$course_id,'C');
}
eclass_closedb();
?>
<body onLoad="document.form1.submit();opener.window.location=opener.window.location;">
<form name="form1" action="import.php" method="get">
  <input type="hidden" name="course_id" value="<?php echo $course_id; ?>">
</form>
</body>