<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

  
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  //if (!$lu->teaching || !$la->isAccessEClassMgtCourse())
  //{
    header("Location: /");
  //}
}

$ori_memberType = $ck_memberType;
$ck_memberType = "Z";
$courseID = $eclass_db;
$categoryID = 9;

$func_filepath =  $eclass40_filepath."/src/resource/eclass_files/files/";

$fm = new fileManager($courseID, $categoryID, $folderID);
/*
$toolbar  = "<a class='iconLink' href=\"javascript:submitPage(document.form1,'folder_new.php')\">".newIcon()."$button_newfolder</a>\n";
$functionbar .= "<a href=\"javascript:submitPage(document.form1,'file_upload.php')\"><img src='/images/admin/button/t_btn_upload_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:downloadFile(document.form1,'file_id[]','download.php')\"><img src='/images/admin/button/t_btn_download_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'file_id[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'file_id[]','unzip.php')\"><img src='/images/admin/button/t_btn_unzip_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:fs_copy(document.form1,'file_id[]','copy.php')\"><img src='/images/admin/button/t_btn_copy_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:fs_move(document.form1,'file_id[]','move.php')\"><img src='/images/admin/button/t_btn_move_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'file_id[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;<br>\n";
*/
# function link/ button

$button_unzip = "Unzip";
$button_copy = "Copy";
$button_move = "Move";
$button_edit = "Edit";
$button_newfolder = "New Folder";
$file_create_file = "Create File";
$file_upload_file = "Upload File";

$download = "<a href=\"javascript:downloadFile(document.form1,'file_id[]','download.php')\" class=\"tool_download\" alt=\"".$button_download."\" title=\"".$button_download."\">&nbsp;</a>";

$fm->field = ($field!="") ? $field : 0;
$fm->order = ($order!="") ? $order : 1;

$tmp_image_path = $image_path;
$display = $fm->getDBResult1($courseID);
$image_path = $tmp_image_path;

$linterface = new interface_html();
$CurrentPage = "PageShareArea";

$li = new libuser($UserID);
$lo = new libeclass();
$leclass = new libeclass2007();
$lp = new libportal();


### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_directory.gif' align='absmiddle' /> $i_eClass_Admin_Shared_Files";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($i_eClass_Admin_Shared_Files, "");	
?>

<script language="JavaScript" src="<?=$eclass_url_root?>/src/includes/js/tooltip.js" type="text/javascript" ></script>
<link rel=stylesheet href="<?=$eclass_url_root?>/css/master.css">
<link rel=stylesheet href="<?=$eclass_url_root?>/css/common.css">
<style type="text/css">
#ToolTip{position:absolute; top: 0px; left: 0px; z-index:1; visibility:hidden;}
body {
	background-color: #e3f3fe;
	background-image: none;
}
.common_table_list tr td {
border-bottom: medium none;
border-right: medium none;
}
.common_table_list tr th {
border-right: medium none;
}
.browser_table_content {
border-left:medium none;
}
.browser_table_top {
border-bottom:medium none;
border-left:medium none;
}
.edit_board {
padding-right:0;
}
.table_tool {
padding-top:7px;
padding-right:1px;
}
.Content_tool {
padding:2px 2px 3px;
}
</style>


<script language="JavaScript">
isToolTip = true;
var folder_field = new Array("<?=$file_location?>", "<?=$quizbank_desc?>", "<?=$quizbank_id?>", "<?=$file_owner?>", "<?=$file_owner_email?>", "<?=$file_modified_by?>", "<?=$file_folder_quota?>");
var file_field = new Array("<?=$file_location?>", "<?=$quizbank_desc?>", "<?=$quizbank_id?>", "<?=$file_owner?>", "<?=$file_owner_email?>", "<?=$file_modified_by?>");
function tipsNow(text, type)
{
  if (document.readyState=="complete")
  {

	if (type==0) {
		//folder
		title = "<?=$file_folder_info?>";
		arr_txt = "folder_field";
	} else {
		//folder
		title = "<?=$file_file_info?>";
		arr_txt = "file_field";
	}
	tt= "<table border='0' cellspacing='0' cellpadding='1'>\n";
	tt += "<tr><td bgcolor='#AA9977' align=center><b>"+title+"</b></td></tr>";
	tt += "<tr><td class='tipborder'><table width='100%' border='0' cellspacing='0' cellpadding='3'>\n";

	tmp_arr = text.split("|=|");
	for (var i=0; i<tmp_arr.length; i++){
		tt += "<tr><td class='tipbg' nowrap valign='top'>" + eval(arr_txt+"["+i+"]") + ":</td><td class='tipbg'>";
		tt += (tmp_arr[i]!="") ? tmp_arr[i]+"</td></tr>" : "--</tr>";
	}
	tt += "</table></td></tr></table>\n";
	showTip('ToolTip', tt);
  }
}

function sortPage(a, b, obj){
	obj.order.value=a;
	obj.field.value=b;
	obj.submit();
}

function submitPage(obj, url){
	obj.action = url;
	obj.submit();
	return;
}

function downloadFile(obj, element, url){
	if(countChecked(obj,element)==0)
	{
		alert(globalAlertMsg2);
	} else
	{
		obj.action = url;
		obj.submit();
	}
	return;
}
</script>

<form name="form1" method="get"> 
<br>  
  <div align="left"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?><div align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>&nbsp;</div></div>
        
  <div class="browser_table_top">            
    <div class="Content_tool">			  
      <a href="javascript:submitPage(document.form1,'folder_new.php')" class="new_folder" alt="<?=$button_newfolder?>" title="<?=$button_newfolder?>">
        <?=$button_newfolder?></a>			  
      <!--a href="javascript:checkEdit(document.form1,'edit.php')" class="new_file" alt="<?=$file_create_file?>" title="<?=$file_create_file?>">
        <?=$file_create_file?></a-->			  
      <a href="javascript:submitPage(document.form1,'file_upload.php')" class="upload_file" alt="<?=$file_upload_file?>" title="<?=$file_upload_file?>">
        <?=$file_upload_file?></a>			
    </div>			
    <div class="table_tool">			  
      <div class="table_tool_left">			    
        <div class="table_tool_right" <? if($browser == "MSIE"){ echo 'style="float:right"'; }?>>			      
          <?=$download?>				  
          <a href="javascript:checkEdit(document.form1,'file_id[]','unzip.php')" class="tool_zip" alt="<?=$button_unzip?>" title="<?=$button_unzip?>">&nbsp;</a>				  
          <span>|
          </span>  				  
          <a href="javascript:fs_copy(document.form1,'file_id[]','copy.php')" class="tool_copy" alt="<?=$button_copy?>" title="<?=$button_copy?>">&nbsp;</a>  				  
          <a href="javascript:fs_move(document.form1,'file_id[]','move.php')" class="tool_move_file" alt="<?=$button_move?>" title="<?=$button_move?>">&nbsp;</a>  				  
          <a href="javascript:checkEdit(document.form1,'file_id[]','edit.php')" class="tool_edit" alt="<?=$button_edit?>" title="<?=$button_edit?>">&nbsp;</a>  				  
          <a href="javascript:void(0)" onclick="checkRemove(document.form1,'file_id[]','remove.php')" class="tool_delete" alt="<?=$button_remove?>" title="<?=$button_remove?>">&nbsp;</a>
        </div>			  
      </div>  	        
    </div>		  
  </div>
  <!-- Header -->
	<div class="edit_board" id="resource_q_edit" height="100%">
	  <div>
		<div class="edit_board_right">
		  <div class="browser_table_content" style="height:500px" >
			<!-- End of Header -->
      <div id="ToolTip"></div>
      <?=$display?>
      </div>
    </div></div>
  </div>
<input name="courseID" value="<?=$courseID?>" type=hidden>
<input name="categoryID" value="<?=$categoryID?>" type=hidden>
<input name="folderID" value="<?=$folderID?>" type=hidden>
<input name="field" value="<?=$field?>" type=hidden>
<input name="order" value="<?=$order?>" type=hidden>
</form>

<?php
$ck_memberType = $ori_memberType;
$linterface->LAYOUT_STOP();
intranet_closedb();
?>