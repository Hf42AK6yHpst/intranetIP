<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentView";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$BookArr = $LibeLib->GET_BOOK_DETAILS($BookID);
$BookPageArr = $LibeLib->GET_BOOK_PAGE_TYPE_ARRAY($BookID);
$PrePageNum = ($BookPageArr["pre"] != "") ? $BookPageArr["pre"] : 0; 
$PageNum = ($BookPageArr["page"] != "") ? $BookPageArr["page"] : 0; 

if ($pageid == "")
{
	$pageid = 0;
}

if($pageid > 0)
{
  $sql = "SELECT *
          FROM
            INTRANET_ELIB_BOOK_PAGE
          WHERE
            BookID = '".$BookID."' AND
            PageID = '".$pageid."'
          ";
  $BookContent = $LibeLib->returnArray($sql);
}
else
{
	$sql = "	SELECT 
					Title, OrigPageID, IsChapter
				FROM
					INTRANET_ELIB_BOOK_CHAPTER
				WHERE
					BookID = '".$BookID."'
				ORDER BY
				  OrigPageID, BookChapterID
	";
	$ContentPage = $LibeLib->returnArray($sql);
 
}
/*
$sql = "SELECT MAX(PageID)
        FROM
          INTRANET_ELIB_BOOK_PAGE
        WHERE
          BookID = '".$book_id."'
        ";
        
$temp = $LibeLib->returnVector($sql);
$MaxPage = $temp[0];
*/
$MinPage = 0;

$MaxPage = $PrePageNum + $PageNum;

?>
<META http-equiv=Content-Type content='text/html; charset=UTF-8'>
<link href="/templates/style_mail_utf8.css" rel="stylesheet" type="text/css"  />
<link href="/templates/2009a/css/content_utf8.css" rel="stylesheet" type="text/css" />

<style type="text/css" >
html, body
{
	background-color:#FFFFFF;
	scrollbar-face-color:#F7F7F7;
	scrollbar-base-color:#F7F7F7;
	scrollbar-arrow-color:888888;
	scrollbar-track-color:#E2DFDF;
	scrollbar-shadow-color:#F7F7F7;
	scrollbar-highlight-color:#F7F7F7;
	scrollbar-3dlight-color:#C6C6C6;
	scrollbar-darkshadow-Color:#C6C6C6;
	background-image: url("");
	background-repeat: no-repeat;
	background-position: left top;
}
</style>

<base target="_parent" >
<script language="JavaScript">
jLinesArray = new Array();

function jCHANGE_PAGE(jParPageOffset)
{
	document.form1.pageid.value = <?=$pageid?> + jParPageOffset;
	if(document.form1.pageid.value < <?=$MinPage?>)
	{
		document.form1.pageid.value = <?=$MinPage?>;
	}
	else if(document.form1.pageid.value > <?=$MaxPage?>)
	{
		document.form1.pageid.value = <?=$MaxPage?>;
	}
	document.form1.submit();
}  


function resize_iframe()
{
	scroll_size = 10;
	iframeObj = this.parent.document.getElementById("content_frame");
	if(iframeObj==null) return;
	ch= this.document.body.clientHeight;
	cw = this.document.body.clientWidth;
			
	this.scroll(scroll_size,scroll_size);	
	while(this.document.body.scrollTop>0 || this.document.body.scrollLeft > 0)
	{
		if(this.document.body.scrollTop>0)
			iframeObj.style.height = ch;
		if(this.document.body.scrollLeft > 0)
			iframeObj.style.width = cw;
		ch+=scroll_size;
		cw+=scroll_size;
		this.scroll(scroll_size,scroll_size);
	}					
	
}

function jEDIT_PAGE()
{
	document.form1.CurrentMode.value=1;
	document.form1.submit();
}

</script>

<br />   
<form name="form1" method="post" enctype="multipart/form-data" action="view_content.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2" >
	<table  border="0" cellspacing="0" cellpadding="10" align="center" width="90%" >	
	<tr>
		<td align="right">
		<?php  
			echo $linterface->GET_ACTION_BTN($button_edit, "button", "jEDIT_PAGE()");  
		?>
		</td>
	</tr>				
	</table>
	</td>
</tr>		

<tr>
	<td colspan="2">
	<table  border="1" cellspacing="0" cellpadding="10" align="center" bordercolor="#" >	
  <tr>
    <td>
<?php
	if($pageid > 0)
	{
		$display_content = preg_replace("/(\<img src=\")/","\\1/file/elibrary/content/".$BookID."/",$BookContent[0]['Content']);
		echo nl2br($display_content);
	}
	else
	{
		echo "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">\n";
		echo "<tr>\n";
		echo "<td colspan=\"2\" align=\"center\" class=\"tabletext\" >".$eLib['Book']["TableOfContent"]."</td>\n";
		echo "</tr>\n";
		foreach($ContentPage as $key => $content)
		{	
			echo "<tr>\n";
			if ($content['IsChapter'])
			{
				echo "<td class=\"tabletext\" >".$content['Title']."</td>\n";
			}
			else
			{
				echo "<td class=\"tabletext\" >&nbsp;&nbsp;&nbsp;&nbsp;".$content['Title']."</td>\n";
			}
			echo "<td class=\"tabletext\" align=\"center\">".str_pad("", 110, ".")."</td>\n";
			echo "<td class=\"tabletext\" width=\"10%\">".$content['OrigPageID']."</td>\n";
			echo "</tr>\n";
		}
		echo "</table>\n";
	}
?>
    </td>
	</tr>
	</table>
	</td>
</tr>


<tr>
	<td colspan="2">        
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
    <tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
		<td align="center">
      	<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
    		<tr>
    			<td align='left' width='33%'><?= $pageid>$MinPage?$linterface->GET_ACTION_BTN($button_previous_page, "button", "jCHANGE_PAGE(-1)"):"&nbsp;" ?></td>
    			<td align="center" width='33%'><input type="text" name="pageid" value="<?=$pageid?>" /><?= $linterface->GET_ACTION_BTN($button_view, "submit") ?></td>
    			<td align='right' width='33%'><?= $pageid<$MaxPage?$linterface->GET_ACTION_BTN($button_next_page, "button", "jCHANGE_PAGE(1)"):"&nbsp;" ?></td>
    		</tr>			
		  </table>
		</td>
	</tr>

    <tr>
		<td align="right">
		<?=$pageid?> / <?=$MaxPage?>
		</td>
	</tr>
		
	
	</table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="BookID" value="<?=$BookID?>" />
<input type="hidden" name="CurrentMode" value="0" />

</form>

<script language="javascript" >
<!--
  //resize_iframe();
  setTimeout(resize_iframe,500);
-->
</script>

<?php
intranet_closedb();

echo $linterface->FOCUS_ON_LOAD("form1.InputFile");

?>
