<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$DisplayMode = 2; // list mode (no image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

if($sortField == "")
$sortField = "Date";

if($sortFieldOrder == "")
$sortFieldOrder = "DESC";

$inputArr["selectBookOrder"] = $selectBookOrder;
$inputArr["selectCatOrder"] = $selectCatOrder;

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Content";
$tmpArr[2]["value"] = "Category";
$tmpArr[3]["value"] = "Date";

$tmpArr[0]["text"] = $eLib["html"]["book_title"];
$tmpArr[1]["text"] = $eLib["html"]["notes_content"];
$tmpArr[2]["text"] = $eLib["html"]["category"];
$tmpArr[3]["text"] = $eLib["html"]["last_modified"];

$tmpArr[0]["width"] = "15%";
$tmpArr[1]["width"] = "55%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "15%";

$inputArr["sortFieldArray"] = $tmpArr;

$inputArr["selectNumField"] = 5;
$UserID = $_SESSION["UserID"];

$con = $sortField;
$order = $con." ".$sortFieldOrder;

$con1 = "";
$con2 = "";

if($selectBookOrder != "" && $selectBookOrder != "All")
$con1 = "AND a.BookID = '{$selectBookOrder}'";

if($selectCatOrder != "" && $selectCatOrder != "All")
{
	//$selectCatOrder = iconv("big-5","utf-8",$selectCatOrder);
	$con2 = "AND b.Category = '{$selectCatOrder}'";
}

$sql = "
		SELECT 
		a.BookID, 
		a.Title as Title, 
		CONCAT('<a href=\"#\" class=\"tablelink\" onClick=\"ViewNotesContent(',a.BookID,',\'',b.NoteType,'\',\'',b.NoteID,'\');\">',b.Content, '</a>') as Content,
		b.Category, DATE_FORMAT(b.DateModified,'%Y/%m/%d') as Date
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
		WHERE
		b.UserID = {$UserID}
		AND a.BookID = b.BookID
		AND a.Publish = 1
		$con1
		$con2
		ORDER BY
		$order
		";
		
		//debug_r($sql);

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

//////////////////////////////////////////////////////////
// set selection box
//////////////////////////////////////////////////////////
$ParArr["UserID"] = $_SESSION["UserID"];

$returnTitle = $lelib->getBookNotesTitle_ByUserID($ParArr);
$returnCategory = $lelib->getBookNotesCategory_ByUserID($ParArr);

$selectBook = "<select name=\"selectBook\" onChange=\"changeBookSelection(this);\">";

$selectBook .= "<option value=\"All\">".$eLib["html"]["all_books"]."</option>";

for($i = 0; $i < count($returnTitle); $i++)
{
	if($selectBookOrder == $returnTitle[$i]["BookID"])
	$checkVal = "selected";
	else
	$checkVal = "";
	
	//$selectBook .= "<option value=\"".$returnTitle[$i]["BookID"]."\"".$checkVal.">".iconv("utf-8","big-5",$returnTitle[$i]["Title"])."</option>";
	$selectBook .= "<option value=\"".$returnTitle[$i]["BookID"]."\"".$checkVal.">".$returnTitle[$i]["Title"]."</option>";
}

$selectBook .= "</select>";

$selectCategory = "<select name=\"selectCategory\" onChange=\"changeCategorySelection(this);\">";
$selectCategory .= "<option value=\"All\">".$eLib["html"]["all_categories"]."</option>";

for($i = 0; $i < count($returnCategory); $i++)
{
	if($selectCatOrder == $returnCategory[$i]["Category"])
	$checkVal = "selected";
	else
	$checkVal = "";
	
	//$selectCategory .= "<option value=\"".iconv("utf-8","big-5",$returnCategory[$i]["Category"])."\"".$checkVal.">".iconv("utf-8","big-5",$returnCategory[$i]["Category"])."</option>";
	$selectCategory .= "<option value=\"".$returnCategory[$i]["Category"]."\"".$checkVal.">".$returnCategory[$i]["Category"]."</option>";
}

$selectCategory .= "</select>";

$selectionTable = "<table border=\"0\" cellpadding=\"4\" cellspacing=\"0\">";
$selectionTable .= "<tr>";
$selectionTable .= "<td>".$selectBook."</td>";
$selectionTable .= "<td>".$selectCategory."</td>";
$selectionTable .= "</tr>";
$selectionTable .= "</table>";
//////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--

function changeBookSelection(obj)
{
	document.form1.selectBookOrder.value = obj.value;
	document.form1.submit();
	
} // end function

function changeCategorySelection(obj)
{
	document.form1.selectCatOrder.value = obj.value;
	document.form1.submit();
	
} // end function

function ViewNotesContent(id, notes, noteId)
{
	//alert(id + " " + notes);
	MM_openBrWindowFull('tool/index.php?BookID='+id+'&NoteType='+notes+'&NoteID='+noteId,'booktool','scrollbars=yes');
}

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">

<!-- Setting logo -->

<!-- End Setting logo -->

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- Record List Menu -->									
<?=$lelib->displayRecordListMenu($ParArr,$eLib);?>
<!-- End Record List Menu -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- catalogue Menu -->
<?=$eLib_Catalogue?>
<!-- end catalogue Menu -->
<br></td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="elib_record_mynotes.php" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<!--
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<?=$eLib["html"]["personal_records"]?>
-->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["my_notes"]?></td>
</tr>
</table>
<!-- end head menu -->


<table width="98%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
</tr>

<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>

<td style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif); background-repeat:repeat-x;">
<?=$selectionTable?>
<!-- show content -->
<?=$contentHtml?>
<!-- end show content -->
</td>


<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
</tr>
													
<tr>
<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
</tr>
</table>

<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >
<input type="hidden" name="selectBookOrder" value="<?=$selectBookOrder?>" >
<input type="hidden" name="selectCatOrder" value="<?=$selectCatOrder?>" >
</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
