<?php 
	$PATH_WRT_ROOT = "../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libeclass40.php");
	include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
	include_once($PATH_WRT_ROOT."includes/libportal.php");
		
	if($userBrowser->platform!="iPad" && $userBrowser->platform!="Andriod" && !strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') && !strstr($_SERVER['HTTP_USER_AGENT'],'iPod')){
	//	header('Location: index.php');
	}
	
	if($_SESSION['UserType'] != '2'){
		header('Location: ../logout.php');
	}
	session_start();
	session_register_general('app_entry', true);
	
	if(isset($app_lang) && $app_lang != ""){
		header('Location: ../lang.php?lang='.$app_lang.'&url=home/classrooms.php');
	}else{
		session_register_general('app_session_lang', $intranet_session_language);
	}
	
	intranet_auth();
	intranet_opendb();
	$db = new libdb();
	$lp = new libportal();
	$sql = "SELECT EnglishName, UserEmail, ClassName FROM INTRANET_USER WHERE UserID='".$_SESSION['UserID']."'";
	$result = $db->returnArray($sql);
	
	$userEmail = $result[0]['UserEmail'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link id="page_favicon" href="/images/favicon.ico" rel="icon" type="image/x-icon">
	<title>eClass IP 2.5</title>

	<!-- Bootstrap core CSS -->
	<link href="../templates/classroom_app/css/bootstrap.min.css" rel="stylesheet">
	<!-- Bootstrap theme -->
	<link href="../templates/classroom_app/css/bootstrap-theme.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="../templates/classroom_app/css/style.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Didact+Gothic' rel='stylesheet' type='text/css'>
	<script language=JavaScript1.2 src=/templates/script.js></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>	
	<div class="container" role="main">		
		<div class="row top-nav-bar"></div>
		<div class="row home-bg-gradient">
			<div class="updates-view">
				
				<?=$lp->classroom_list_appview($userEmail, 0);?>

			</div>
		</div>		
		
	</div> <!-- /container -->

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
</body>

</html>
