<?php
/**
 * Log :
 * Date: 	2013-12-17 [Henry]
 * 			modified parsePenaltyReportRequest()
 * Date: 	2013-05-30 [Rita]
 * 			modified checkAuthorized()
 * Date: 	2013-05-03 [Cameron]
 * 			Add Filter by language section in front end search interface   
 * Date: 	2013-04-25 [Cameron]
 * 			Fix bug for parseOverdueRequest() POST method part so that "Target" filter function works   
 * */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);

####### END  OF   Trim all request #######

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

include_once('OverdueReportView.php');
include_once('CirculationReportView.php');
include_once('IntegratedReportView.php');
include_once('LendingReportView.php');
include_once('ReaderRankReportView.php');
include_once('ReaderInactiveReportView.php');
include_once('BookRankingReportView.php');
include_once('BookReportReportView.php');
include_once('PenaltyReportView.php');
include_once('CategoryReportView.php');
include_once('ajaxView.php');
include_once('Reports.php');

global $Lang;

class ReportController{

	var $model;

	public function __construct(){
		intranet_auth();
		intranet_opendb();
		$this->model = new Reports();
	}
	

	 protected function parseIntegratedRequest(){

	 	$viewdata['subjects']=$this->model->getSubjectArray();
		$viewdata['category'] = $this->model->getCirCategoryArray();
		$viewdata['circulation'] = $this->model->getCirculationArray();
		
	 	if(!empty($_POST)){
	 		$viewdata['post'] =$_POST['data'];
	 		$data =& $viewdata['post'];
	 		if ( $data['Period'] == 'YEAR' ){
	 			$dateRange = getPeriodOfAcademicYear($data['AcademicYear']);
	 		}else{
	 			$dateRange['StartDate']  = $data['textFromDate'];
	 			$dateRange['EndDate']  = $data['textToDate'];
	 		}
	 		
			$filterArray['dateRange'] = $dateRange;
			$filterArray['display'] = $data['display'];
			$filterArray['CirCategory'] = $data['CirCategory'];
			$filterArray['BookCategory'] = $data['Categories'];
			$filterArray['Subject'] = $data['subject'];
			$filterArray['sortBy'] = $data['sortBy'];
			
	 		$viewdata['Result'] = $this->model->getIntegratedReport($filterArray);
//	 		debug_r($viewdata['Result']);
	 	}
//debug_r($viewdata['Result']);	 	
	 	
	 	$view = new IntegratedReportView();
	 	if ( $_REQUEST['action'] == 'print'){
	 		echo $view->getPrint($viewdata);
	 		return;
	 	}elseif ( $_REQUEST['action'] == 'export'){
	 		//echo $view->getCSV($viewdata);
	 		Exporter::string_to_file_download('IntegratedReport.csv', $view->getCSV($viewdata));
	 		return;
	 	}elseif($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}
		
		echo $view->getView($viewdata);
		//dump($viewdata);

	}

	protected function parseCirculationRequest(){
		$view = new CirculationReportView();
		$aryView = array();
		
		$viewdata['category'] = $this->model->getCirCategoryArray();
		$viewdata['circulation'] = $this->model->getCirculationArray();
		$viewdata['subjects'] = $this->model->getSubjectArray();
		$viewdata['language'] = $this->model->getLanguageArray();

		if(!empty($_POST)){
			$viewdata['post'] =$_POST['data'];
			$data =& $viewdata['post'];
			if ( $data['Period'] == 'YEAR' ){
				$dateRange = getPeriodOfAcademicYear($data['AcademicYear']);
			}else{
				$dateRange['StartDate']  = $_POST['textFromDate'];
				$dateRange['EndDate']  = $_POST['textToDate'];
			}
		
			$filterArray['dateRange'] = $dateRange;
			
			$filterArray['findBy'] = $data['findBy'];
			if( $data['findBy'] == 'Group'){
				$filterArray['groupTarget'] = $data['groupTarget'];
			}else{
				$filterArray['rankTarget'] = $data['rankTarget'];
				$filterArray['classname'] = $data['classname'];
				$filterArray['studentID'] = $data['studentID'];
			}
			
			$filterArray['CirCategory'] = $data['CirCategory'];
			$filterArray['Categories'] = $data['Categories'];
			$filterArray['Subject'] = $data['subject'];
			$filterArray['language'] = $data['language'];
			$filterArray['Status'] = $data['Status'];
			$filterArray['sortBy'] = $data['sortBy'];
			$filterArray['keyword'] = $data['keyword'];
				
			$viewdata['Result'] = $this->model->getCirculationReport($filterArray);
			
		}
		
		// route print/export
		if ( $_REQUEST['action'] == 'print'){
			echo $view->getPrint($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'export'){
			//echo $view->getCSV($viewdata);
			Exporter::string_to_file_download('CirculationReport.csv', $view->getCSV($viewdata));
			return;
		}elseif($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}
	
		echo $view->getView($viewdata);
		
		//dump($GLOBALS['DEBUG']);
		//dump($filterArray); 
	}

	protected function parseOverdueRequest(){
		$view = new OverdueReportView();
		
		if(!empty($_POST)){
/*			
			$_POST['data']['DueDateBefore'] = $_POST['DueDateBefore'];
			//$_POST['data']['rankTarget'] = $_POST['rankTarget'];
			
			$viewdata['post'] =& $_POST['data'];
			//$data =& $viewdata['post'];
			$filterArray =& $_POST['data'];
*/
			$viewdata['post'] =$_POST['data'];
			$data =& $viewdata['post'];
			$filterArray['DueDateBefore'] = $_POST['DueDateBefore'];
			$filterArray['DueDateFrom'] = $_POST['DueDateFrom'];
			$filterArray['findBy'] = $data['findBy'];
			if( $data['findBy'] == 'Group'){
				$filterArray['groupTarget'] = $data['groupTarget'];
			}else{
				$filterArray['rankTarget'] = $data['rankTarget'];
				$filterArray['classname'] = $data['classname'];
				$filterArray['studentID'] = $data['studentID'];
			}

			$viewdata['Result'] = $this->model->getOverdueReport($filterArray);
		}elseif(!empty($_GET)){
			if ($_GET['action'] == 'email'){
				/*if(!empty($_SESSION['LIBMS']['reporting']['post'])){
					$filterArray = $_SESSION['LIBMS']['reporting']['post'];
					unset($_SESSION['LIBMS']['reporting']['post']);
				}else{
					$filterArray = null;
				}*/
				//debug_r($_GET);die();
				$filterArray['DueDateBefore'] = $_GET['DueDateBefore'];
				$filterArray['DueDateFrom'] = $_GET['DueDateFrom'];
				$filterArray['findBy'] = $_GET['findBy'];
				if( $_GET['findBy'] == 'Group'){
					$filterArray['groupTarget'] = explode(",", $_GET['groupTarget']);
				}else{
					$filterArray['rankTarget'] = $_GET['rankTarget'];
					$filterArray['classname'] = explode(",", $_GET['classname']);
					$filterArray['studentID'] = explode(",", $_GET['studentID']);
				}
				
				$viewdata['Result'] = $this->model->getOverdueReport($filterArray);

				$view->do_overdue_email($viewdata, $_GET['mode'] );
				echo $view->getConfirmation($viewdata);
				return;
			}
		}
		
		echo $view->getView($viewdata);
// 		dump($_POST);
// 		dump($filterArray);
// 		dump($viewdata);
	}
	
	protected function parseLendingRequest(){
		$view = new LendingReportView();
		
		$viewdata['category'] = $this->model->getCirCategoryArray();
		$viewdata['circulation'] = $this->model->getCirculationArray();
		$viewdata['subjects'] = $this->model->getSubjectArray();
		$viewdata['language'] = $this->model->getLanguageArray();
		
		if(!empty($_POST)){
			$viewdata['post'] =$_POST['data'];
			
			$data =& $viewdata['post'];
			if ( $data['Period'] == 'YEAR' ){
				$dateRange = getPeriodOfAcademicYear($data['AcademicYear']);
			}else{
				$dateRange['StartDate']  = $_POST['textFromDate'];
				$dateRange['EndDate']  = $_POST['textToDate'];
			}
		
			$filterArray['dateRange'] = $dateRange;
			
			$filterArray['findBy'] = $data['findBy'];
			if( $data['findBy'] == 'Group'){
				$filterArray['groupTarget'] = $data['groupTarget'];
			}else{
				$filterArray['rankTarget'] = $data['rankTarget'];
				$filterArray['classname'] = $data['classname'];
				$filterArray['studentID'] = $data['studentID'];
			}
			
			$filterArray['CirCategory'] = $data['CirCategory'];
			$filterArray['Categories'] = $data['Categories'];
			$filterArray['Subject'] = $data['subject'];
			$filterArray['language'] = $data['language'];
			$filterArray['sortBy'] = $data['sortBy'];
			
			$viewdata['Result'] = $this->model->getLendingReport($filterArray);
		}

		
		if($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'print'){
			echo $view->getPrint($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'export'){
			//echo $view->getCSV($viewdata);
			Exporter::string_to_file_download('LendingReport.csv', $view->getCSV($viewdata));
			return;
		}
		
		//dump($_POST);
 		//dump($filterArray);
 		//dump($viewdata);
		echo $view->getView($viewdata);
	}
	
	protected function parseReaderRankingRequest(){
		$view = new ReaderRankingReportView();
	
		$viewdata['category'] = $this->model->getCirCategoryArray();
		$viewdata['circulation'] = $this->model->getCirculationArray();
		$viewdata['subjects'] = $this->model->getSubjectArray();
		$viewdata['language'] = $this->model->getLanguageArray();
	
		if(!empty($_POST)){
			$viewdata['post'] =$_POST['data'];
	
			$data =& $viewdata['post'];
			if ( $data['Period'] == 'YEAR' ){
				$dateRange = getPeriodOfAcademicYear($data['AcademicYear']);
			}else{
				$dateRange['StartDate']  = $_POST['textFromDate'];
				$dateRange['EndDate']  = $_POST['textToDate'];
			}
	
			$filterArray['dateRange'] = $dateRange;
	
			$filterArray['findBy'] = $data['findBy'];
			if( $data['findBy'] == 'Group'){
				$filterArray['groupTarget'] = $data['groupTarget'];
			}else{
				$filterArray['rankTarget'] = $data['rankTarget'];
				$filterArray['classname'] = $data['classname'];
				$filterArray['studentID'] = $data['studentID'];
			}
	
			$filterArray['CirCategory'] = $data['CirCategory'];
			$filterArray['Categories'] = $data['Categories'];
			$filterArray['Subject'] = $data['subject'];
			$filterArray['language'] = $data['language'];
			$filterArray['listNo'] = $data['listNo'];
	
			$viewdata['Result'] = $this->model->getReaderRankingReport($filterArray);
		}
	
		if($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}elseif ($_REQUEST['action'] == 'print'){
			echo $view->getPrint($viewdata);
			return;
		}elseif ($_REQUEST['action'] == 'export'){
			//echo $view->getCSV($viewdata);
			Exporter::string_to_file_download('ReaderRankingReport.csv', $view->getCSV($viewdata));
			return;
		}
	
		//dump($_POST);
		//dump($filterArray);
		//dump($viewdata);
		echo $view->getView($viewdata);
	}
	
	##### Book Ranking ######################################################
	protected function parseBookRankingRequest(){
		$view = new BookRankingReportView();
	
		$viewdata['category'] = $this->model->getCirCategoryArray();
		$viewdata['circulation'] = $this->model->getCirculationArray();
		$viewdata['subjects'] = $this->model->getSubjectArray();
		$viewdata['language'] = $this->model->getLanguageArray();
	
		if(!empty($_POST)){
			$viewdata['post'] =$_POST['data'];
	
			$data =& $viewdata['post'];
			if ( $data['Period'] == 'YEAR' ){
				$dateRange = getPeriodOfAcademicYear($data['AcademicYear']);
			}else{
				$dateRange['StartDate']  = $_POST['textFromDate'];
				$dateRange['EndDate']  = $_POST['textToDate'];
			}
	
			$filterArray['dateRange'] = $dateRange;
	
			$filterArray['findBy'] = $data['findBy'];
			if( $data['findBy'] == 'Group'){
				$filterArray['groupTarget'] = $data['groupTarget'];
			}else{
				$filterArray['rankTarget'] = $data['rankTarget'];
				$filterArray['classname'] = $data['classname'];
				$filterArray['studentID'] = $data['studentID'];
			}
	
			$filterArray['CirCategory'] = $data['CirCategory'];
			$filterArray['Categories'] = $data['Categories'];
			$filterArray['Subject'] = $data['subject'];
			$filterArray['language'] = $data['language'];
			$filterArray['listNo'] = $data['listNo'];
	
			$viewdata['Result'] = $this->model->getBookRankingReport($filterArray);
		}
	
		if($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'print'){
			echo $view->getPrint($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'export'){
			//echo $view->getCSV($viewdata);
			Exporter::string_to_file_download('BookRankingReport.csv', $view->getCSV($viewdata));
			return;
		}
	
		//dump($_POST);
		//dump($filterArray);
		//dump($viewdata);
		echo $view->getView($viewdata);
	}
	
	##### Book Report ######################################################
	protected function parseBookReportRequest(){
		$view = new BookReportReportView();
	
		if(!empty($_REQUEST)){
			//$viewdata['post'] =$_REQUEST['bookID'];
			//$data =& $viewdata['post'];		

				$filterArray['bookID'] = $_REQUEST['ID'];
				$filterArray['Search'] = $_REQUEST['book_search'];
				$filterArray['action'] = $_REQUEST['action'];
				//$filterArray['bookID'] = $_REQUEST['ID'];
				$viewdata['filterArray'] = $filterArray;
				$viewdata['Result'] = $this->model->getBookReportReport($filterArray);
		}
		
		if($_REQUEST['action'] == 'getBook' ){
			echo json_encode($viewdata['Result']);
			return;
		}elseif($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'print'){
			echo $view->getPrint($viewdata);
			return;
		}
		

	
		//dump($_POST);
		//dump($filterArray);
		//dump($viewdata);
		echo $view->getView($viewdata);
	}
	
	##### Penalty Report######################################################
	protected function parsePenaltyReportRequest(){
		$view = new PenaltyReportReportView();
	
		if(!empty($_POST)){
			//$viewdata['post'] =$_REQUEST['bookID'];
			//$data =& $viewdata['post'];
	
		$viewdata['post'] =$_POST['data'];
	
			$data =& $viewdata['post'];
			if ( $data['Period'] == 'YEAR' ){
				$dateRange = getPeriodOfAcademicYear($data['AcademicYear']);
			}else{
				$dateRange['StartDate']  = $_POST['textFromDate'];
				$dateRange['EndDate']  = $_POST['textToDate'];
			}
	
			$filterArray['dateRange'] = $dateRange;
	
			$filterArray['findBy'] = $data['findBy'];
			if( $data['findBy'] == 'Group'){
				$filterArray['groupTarget'] = $data['groupTarget'];
			}else{
				$filterArray['rankTarget'] = $data['rankTarget'];
				$filterArray['classname'] = $data['classname'];
				$filterArray['studentID'] = $data['studentID'];
			}
			//$filterArray['bookID'] = $_REQUEST['ID'];
			//Henry added 20131217
			$filterArray['RecordStatus'] = $_REQUEST['RecordStatus'];
			$viewdata['filterArray'] = $filterArray;
			$viewdata['Result'] = $this->model->getPenaltyReport($filterArray);
		}
		if($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'print'){
			echo $view->getPrint($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'export'){
			//echo $view->getCSV($viewdata);
			Exporter::string_to_file_download('BookRankingReport.csv', $view->getCSV($viewdata));
			return;
		}
	
	
		//dump($_POST);
		//dump($filterArray);
		//dump($viewdata);
		echo $view->getView($viewdata);
	}
	
	
	
	##### Penalty Report######################################################
	protected function parseReaderInactiveReportRequest(){
		$view = new ReaderInactiveReportView();
	
		if(!empty($_POST)){
			//$viewdata['post'] =$_REQUEST['bookID'];
			//$data =& $viewdata['post'];
	
			$viewdata['post'] =$_POST['data'];
	
			$data =& $viewdata['post'];
			if ( $data['Period'] == 'YEAR' ){
				$dateRange = getPeriodOfAcademicYear($data['AcademicYear']);
			}else{
				$dateRange['StartDate']  = $_POST['textFromDate'];
				$dateRange['EndDate']  = $_POST['textToDate'];
			}
	
			$filterArray['dateRange'] = $dateRange;
			$filterArray['TotalAtMost'] = $_POST['TotalAtMost'];
			
			
			$filterArray['findBy'] = $data['findBy'];
			if( $data['findBy'] == 'Group'){
				$filterArray['groupTarget'] = $data['groupTarget'];
			}else{
				$filterArray['rankTarget'] = $data['rankTarget'];
				$filterArray['classname'] = $data['classname'];
				$filterArray['studentID'] = $data['studentID'];
			}
			$filterArray['sortBy'] = $data['sortBy'];
			//$filterArray['bookID'] = $_REQUEST['ID'];
			$viewdata['filterArray'] = $filterArray;
			$viewdata['Result'] = $this->model->getReaderInactiveReport($filterArray);
		}
		if($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'print'){
			echo $view->getPrint($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'export'){
			//echo $view->getCSV($viewdata);
			Exporter::string_to_file_download('InactiveReaderReport.csv', $view->getCSV($viewdata));
			return;
		}
	
	
		//dump($_POST);
		//dump($filterArray);
		//dump($viewdata);
		echo $view->getView($viewdata);
	}
	
	##### Category Report######################################################
	protected function parseCategoryReportRequest(){
		$view = new CategoryReportView();
		$viewdata['category'] = $this->model->getCirCategoryArray();
		$viewdata['subjects'] = $this->model->getSubjectArray();
		$viewdata['language'] = $this->model->getLanguageArray();
	
			
		if(!empty($_POST)){
			//$viewdata['post'] =$_REQUEST['bookID'];
			//$data =& $viewdata['post'];
	
			$viewdata['post'] =$_POST['data'];
	
			$data =& $viewdata['post'];
			
			if ( $data['Period'] == 'YEAR' ){
				$dateRange = getPeriodOfAcademicYear($data['AcademicYear']);
			}else{
				$dateRange['StartDate']  = $_POST['textFromDate'];
				$dateRange['EndDate']  = $_POST['textToDate'];
			}
	
			$filterArray['dateRange'] = $dateRange;
	
			$filterArray['findBy'] = $data['findBy'];
			if( $data['findBy'] == 'Group'){
				$filterArray['groupTarget'] = $data['groupTarget'];
			}else{
				$filterArray['rankTarget'] = $data['rankTarget'];
				$filterArray['classname'] = $data['classname'];
				$filterArray['studentID'] = $data['studentID'];
			}

			
			if( $data['findBy_cat'] == 'categories'){
				$filterArray['categories'] = $data['Categories'];
			}
			else if ( $data['findBy_cat'] == 'subject'){
				$filterArray['subject'] = $data['subject'];
			}
			else {	// By Language				
				$filterArray['language'] = $data['language'];
			}
			$filterArray['findBy_cat'] = $data['findBy_cat'];			
			
			$viewdata['filterArray'] = $filterArray;
			$viewdata['Result'] = $this->model->getCategoryReport($filterArray);
		}
		
		if($_REQUEST['action'] == 'getResult' ){
			echo $view->getToolbar($viewdata);
			echo $view->getResult($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'print'){
			echo $view->getPrint($viewdata);
			return;
		}elseif ( $_REQUEST['action'] == 'export'){
			//echo $view->getCSV($viewdata);
			Exporter::string_to_file_download('BookRankingReport.csv', $view->getCSV($viewdata));
			return;
		}
	
	
		//dump($_POST);
		//dump($filterArray);
		//dump($viewdata);
		echo $view->getView($viewdata);
	}
	
	############################################# AJAX BLOCK #######################################################
	protected function parseAjax(){
		$function = $_REQUEST['function'];
		if (empty($function)){
			exit;
		}
		
		$x ='';
		switch($function){
			case 'getGroupName':
 				$x = $this->getGroupName();
 				break;
 			case 'getClassLevel':
 				$x = $this->getClassLevel();
 				break;
  			case 'getClassNameByLevel':
 				$x = $this->getClassNameByLevel();
 				break;
 			case 'getStudentByClass':
 				$x = $this->getStudentByClass();
 				break; 				
			default:
				exit;
		}
		if (empty($x))
			exit;
		
		echo $x;
	}
	
	protected function getGroupName(){
		$veiw_ary['function'] = $_REQUEST['function'];
		$veiw_ary['result']= $this->model->getGroupNameArray();
		$view = new ajaxView();
		return $view->getView($veiw_ary);	
	}
	
	protected function getClassLevel(){
		$veiw_ary['function'] = $_REQUEST['function'];
		$veiw_ary['result']= $this->model->getClassLevelArray();
		$view = new ajaxView();
		return $view->getView($veiw_ary);	
	}	
	
	protected function getClassNameByLevel(){
		$veiw_ary['function'] = $_REQUEST['function'];		
		$veiw_ary['result']= $this->model->getClassNameArrayByClassLevel();
		$view = new ajaxView();	
		return $view->getView($veiw_ary);
	}
	

	protected function getStudentByClass(){
		$veiw_ary['function'] = $_REQUEST['function'];
		$veiw_ary['result']= $this->model->getStudentArrayByClassName($_REQUEST['classNames']);
		$view = new ajaxView();
		return $view->getView($veiw_ary);	
		
	}	
	
	########################################### END OF AJAX BLOCK #######################################################
	
	protected function checkAuthorized(){
		global $PATH_WRT_ROOT;
		$libms = new liblms();
		$c_right = $_SESSION['LIBMS']['admin']['current_right'];
		
		$global_lockout = (BOOL)$libms->get_system_setting('global_lockout');
		//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && (!$c_right['circulation management']['report'] || $global_lockout))
		
		if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && (!$c_right['reports'] || !$c_right['statistics'] || $global_lockout))
		
		{
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;

		}
	}
	
/**
 * email_all_overdues - sent email to all overdues' users
 * @param string $mode - optional arguments  - "User", "CCParent", "ParentOnly"
 * 			default = 'User'
 */
	public static function email_all_overdues($mode = 'User'){
		$model = new Reports();
		$view = new OverdueReportView();
		$viewdata['Result'] = $model->getOverdueReport($filterArray);
		return $view->do_overdue_email($viewdata,$mode);
		
	}
	
	
	/**
	 * Controller index page 
	 * route request to different praser function according to the $_REQUEST['page'] 
	 */
	public function index(){
		$page = $_REQUEST['page'];
		//$rc = new ReportController();
		$this->checkAuthorized();
	
		switch ($page){
	
			case 'Circulation':
				$this->parseCirculationRequest();
				break;
	
			case 'Overdue':
				$this->parseOverdueRequest();
				break;
				
			case 'Lending':
				$this->parseLendingRequest();
				break;
				
			case 'ReaderRanking':
				$this->parseReaderRankingRequest();
				break;
				
			case 'BookRanking':
				$this->parseBookRankingRequest();
				break;
				
			case 'BookReport':
				$this->parseBookReportRequest();
				break;
				
			case 'ReaderInactive':
				$this->parseReaderInactiveReportRequest();
				break;
				
			case 'PenaltyReport':
				$this->parsePenaltyReportRequest();
				break;
				
			case 'CategoryReport':
				$this->parseCategoryReportRequest();
				break;
			
			case 'Ajax':
				$this->parseAjax();
				break;
	
			default:
				//default Integrated Report page
				$this->parseIntegratedRequest();
	
		}
	}
	
}


