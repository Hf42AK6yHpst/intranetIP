<?php

// Using: 

/***********************************************
 *
 * Date:	2018-02-13 (Cameron)
 * Details:	add group right "not_allow_class_management_circulation" under access_without_open_hour [case #E129637]
 * 
 * Date:	2017-11-23 (Henry)
 * Details:	add group right "lost" under circulation management section [case #F131622]
 * 
 * Date:	2017-06-06 (Cameron)
 * Details:	add group right "penalty report" under report section [case #C117984]
 * 
 * Date:	2016-11-11 (Cameron)
 * Details:	add group right "portal settings" under admin section [case #Z108442]
 * 
 * Date:	2016-04-25 (Cameron)
 * Details:	break "stock-take and write-off" into two access rights: "stock-take" and "write-off" [case #K94933]
 * 
 * Date:	2014-12-08 (Ryan)
 * Details:	added the group right "ebook statistics"
 * 
 * Date:	2013-11-29 (Henry)
 * Details:	added the group right "progress report"
 * 
 * Date:	2013-11-27 (Henry)
 * Details:	added the group right "write-off report"
 * 
 * Date:	2013-11-18 (Henry)
 * Details:	added the group right "accession report"
 * 
 * Date: 	2013-05-30 (Rita)
 * Details:	add Group Right
 * 
 ***********************************************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


global $libms_default;

/*
 * 	T - Teacher, NT - Non-Teacher, S - Student, P - Parent
 */ 
$libms_default['group_right']['admin']['book management']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['admin']['group management']=array( 'T' => 'na', 'NT' => 'na', 'S' => 'na', 'P' => 'na');
$libms_default['group_right']['admin']['stock-take']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['admin']['write-off']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['admin']['settings']=array( 'T' => 'na', 'NT' => 'na', 'S' => 'na', 'P' => 'na');
$libms_default['group_right']['admin']['portal settings']=array( 'T' => 'na', 'NT' => 'na', 'S' => 'na', 'P' => 'na');

$libms_default['group_right']['circulation management']['access_without_open_hour']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['circulation management']['not_allow_class_management_circulation']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['circulation management']['borrow']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['circulation management']['reserve']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['circulation management']['return']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['circulation management']['renew']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['circulation management']['overdue']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['circulation management']['lost']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
//$libms_default['group_right']['circulation management']['batch_return_and_renew']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
//$libms_default['group_right']['reports']['daily summary']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
//$libms_default['group_right']['reports']['overdue']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
//$libms_default['group_right']['reports']['income']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
//$libms_default['group_right']['reports']['borrow records']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);


########## Report Start ##########
$libms_default['group_right']['reports']['accession report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['book report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['loan report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['overdue report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['penalty report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['lost report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);

$libms_default['group_right']['reports']['reader ranking']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['inactive reader']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['book ranking']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['stock-take report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['write-off report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['reports']['progress report']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
########## Report End ##########


########## Statistic Start #########
$libms_default['group_right']['statistics']['summary']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['statistics']['general stats']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['statistics']['category vs class or form']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['statistics']['frequency by form']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['statistics']['ebook statistics']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);
$libms_default['group_right']['statistics']['penalty stats']=array( 'T' => false, 'NT' => false, 'S' => false, 'P' => false);

########## Statistic End #########

########## Service Start ############
$libms_default['group_right']['service']['reserve']=array( 'T' => true, 'NT' => true, 'S' => true, 'P' => false);
$libms_default['group_right']['service']['renew']=array( 'T' => true, 'NT' => true, 'S' => true, 'P' => false);

########## Service End ############



?>
