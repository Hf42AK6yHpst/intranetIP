<?php

## being modified by : 

/**
 * Log :
 * 
 * Date:	2017-11-23 [Henry]
 * 			allow user to access this page (get user by class and class number) if he/she lost circulation management right [Case#F131622]
 * 
 * Date:	2017-03-14 [Cameron]
 * 			Add IP checking for circulation
 * 
 * Date:	2016-11-01 [Cameron]
 * 			Fix bug: Need to show student name if class number is not assigned (null), Class number is show as dash - (case #A104493)
 * 
 * Date:	2016-02-02 [Cameron]
 * 			change access right: allow user to access this page (get user by class and class number) if he/she has one 
 * 			of the following circulation management right: borrow, reserve, return, renew and overdue
 * 
 * Date:	2015-06-09 [Henry]
 * 			apply class management permission
 * 
 * Date:	2013-10-07 [Yuen]
 * 			created this file
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();

intranet_opendb();
$libms = new liblms();

$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right,$c_m_right);


if (!$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && !($c_right['circulation management']['borrow'])
	&& !($c_right['circulation management']['reserve']) && !($c_right['circulation management']['return'])
	&& !($c_right['circulation management']['renew']) && !($c_right['circulation management']['overdue']) && !($c_right['circulation management']['lost'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libinterface = new interface_html();

$ClassNumberSql = (isset($junior_mck)) ? "IF(ClassNumber='' OR ClassNumber is null, '-',CONVERT( ClassNumber  USING utf8))" : "IF(ClassNumber='' OR ClassNumber is null, '-',ClassNumber)";

switch ($Task)
{
	case "ClassList":
//		$sql = "SELECT UserID, CONCAT({$ClassNumberSql}, ' ', ".$Lang['libms']['SQL']['UserNameFeild'].") As StudentName FROM LIBMS_USER WHERE ClassName='{$ClassName}' AND UserType='S'  AND RecordStatus='1' ORDER BY ClassName, ClassNumber";
		$sql = "SELECT UserID, CONCAT({$ClassNumberSql}, ' ', ".getNameFieldByLang2().") As StudentName FROM LIBMS_USER WHERE ClassName='{$ClassName}' AND UserType='S'  AND RecordStatus='1' ORDER BY ClassName, ClassNumber";
		$rows = $libms->returnArray($sql);
		$ReturnRx = $libinterface->GET_SELECTION_BOX($rows, "name=\"StudentID\" onChange=\"SelectStudent(this.value, this.form)\"", "-- ".$button_select." --");
		break;
	default:
		$ReturnRx = "No task is performed!";
		break;
}

echo $ReturnRx;

intranet_closedb();
?>
 
