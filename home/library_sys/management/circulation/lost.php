<?php
// Editing by
/*
 * 2019-10-11 (Cameron)
 * - use large font for common_table_list if $sys_custom['eLibraryPlus']['Circulation']['LargeFont'] is set [case #K155570]
 *
 * 2019-03-28 (Cameron)
 * - show BookSubTitle if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * 
 * 2018-02-22 (Cameron)
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 2017-11-23 (Henry)
 * - change permission check to lost [Case#F131622]
 *
 * 2017-03-14 (Cameron)
 * - add IP checking for circulation
 *
 * 2015-06-10 (Henry): apply class management permission
 * 2014-01-09 (Henry): disable the submit button if clicked
 *
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ("BookManager.php");
include_once ("User.php");
include_once ("RecordManager.php");
include_once ("RightRuleManager.php");

include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['lost'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}
if (! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID']))
    header("Location: index.php");
$interface = new interface_html("libms_circulation.html");

$GLOBALS['LIBMS']['PAGE'] = "LOST";
$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];

$User = new User($uID, $libms);

$borrowCount = 0;
$books = $User->borrowedBooks;

$xmsg = $_SESSION['LIBMS']['xmsg'];
unset($_SESSION['LIBMS']['xmsg']);

$interface->LAYOUT_START();

if ($sys_custom['eLibraryPlus']['Circulation']['LargeFont']) {
    echo '<link href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/elibplus_large_font.css" rel="stylesheet" type="text/css">'."\n";
}

?>
<?#############################################################################?>
<script type="text/javascript">
$(function(){
	$(window).on('beforeunload', function(){
		if ($('input.lost_book:checked').length > 0)
			return '<?=$Lang['libms']['CirculationManagement']['msg']['comfirm_leave']?>';
	});
	
	$('input#lost_submit').click(function(){
		$(window).off('beforeunload');
	});
	
	$('input.lost_book').change(function(){
		$(this).parents('tr:first').toggleClass('avaliable_row');
		$('em#lost_count').html($('input.lost_book:checked').length);
	});

	$('#reset').click(function(){
		$('.lost_book:checkbox').each(function(){
			THIS= $(this);
			if ( THIS.is(':checked') )
				THIS.click();
		});
	});
	
	$(document).bind('keydown',function(e){
			switch (e.keyCode)
			{
			    case 120:	// F9
					$('input#lost_submit').click();
					return false;
			    default:
			      return true;
			}
  
  
		});
});

function checkform(frmObj)
{
	document.getElementById('lost_submit').disabled = true;
	if ($('input.lost_book:checked').length<=0)
	{
		alert("<?=$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed']?>");
		document.getElementById('lost_submit').disabled = false;
		return false;
	}
}
</script>
<?#############################################################################?>

<? if(!empty($xmsg)): ?>
<center>
	<div align="center" style='width: 1%'>
		<table border="0" cellpadding="3" cellspacing="0" class="systemmsg">
			<tr>
				<td nowrap="nowrap"><font color="green"><?=$xmsg?></font></td>
			</tr>
		</table>
	</div>
</center>
<?endif;?>
<form name="lost_form" action='lost_action.php'
	onsubmit="return checkform(this)">
	<div class="table_board">
		<table class="common_table_list">
			<col nowrap="nowrap" />

			<tr>
				<th class="num_check">#</th>
				<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['borrowday']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
				<th nowrap="nowrap" class="num_check"><?=$Lang['libms']['CirculationManagement']['lost']?></th>
			</tr>
	
	<?

$i = 0;
foreach ($books as $book) :
    
    if (! $_SESSION['LIBMS']['admin']['current_right']['circulation management'] && $c_m_right['circulation management'] && ! $libms->get_class_mgt_can_handle_item($book['UniqueID'])) {
        continue; // Henry added
    }
    $displayBookTitle = $book['BookTitle'];
    if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
        $displayBookTitle .= $book['BookSubTitle'] ? " :" . $book['BookSubTitle'] : "";
    }
    $displayBookTitle .= " [".$book['BarCode']."]";
    
    $i ++;
    ?>
			<tr class=''>
				<td><?=$i?></td>
				<td><?=$displayBookTitle?></td>
				<td nowrap="nowrap"><?=date('Y-m-d',strtotime($book['BorrowTime']))?></td>
				<td nowrap="nowrap"><span
					<? if (time() - strtotime($book['DueDate']) > 60*60*24) echo 'class="alert_text"';?>><?=$book['DueDate']?></span></td>
				<td><input name="lost_book[]" class='lost_book' type="checkbox"
					value="<?=$book['UniqueID']?>" /></td>
			</tr>
	<?	endforeach; ?>
	
	</table>
	</div>
	<div class="op_btn">
		<div class="op_num">
			<span><?=$Lang['libms']['CirculationManagement']['no_lost']?> :</span><em
				id='lost_count'> 0</em>
		</div>
		<div class="form_btn">
			<input name="submit2" id='lost_submit' type="submit"
				class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['confirm'].$Lang["libms"]["CirculationHotKeys"]["Confirm"]?>" />
			<input id='reset' name="submit2" type="reset" class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['cancel'].$Lang["libms"]["CirculationHotKeys"]["Cancel"]?>" />
		</div>
	</div>
</form>
<?#############################################################################?><?php
$interface->LAYOUT_STOP();
exit;
?>
