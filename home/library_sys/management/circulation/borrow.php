<?php

// Modifying by:

// Note: Please use UTF-8 for editing

// ########### Change Log Start ###############
/*
 * 20200520 (Cameron)
 * - show error_msg and don't allow to use circulation function if it's not empty including user hasn't assigned group [case #H185529]
 *
 * 20191022 (Cameron)
 * - fix borrow_ajax_handler(): should apply double quotation mark to cirtype as it can contain space [case #S171763]
 * 
 * 20191011 (Cameron)
 * - use large font for common_table_list if $sys_custom['eLibraryPlus']['Circulation']['LargeFont'] is set [case #K155570]
 * 
 * 20190610 (Henry)
 * - access right down to borrow, reserve, return, renew, overdue and lost: 
 * 		$c_right['circulation management']['borrow']
 * 		$c_right['circulation management']['reserve']
 * 		$c_right['circulation management']['return']
 * 		$c_right['circulation management']['renew']
 * 		$c_right['circulation management']['overdue']
 * 		$c_right['circulation management']['lost']
 * 
 * 20190506 (Cameron)
 * - redirect to logout after borrow book(s) if leave_user_after_borrow_book is true [case #M153872]
 * 
 * 20190328 (Cameron)
 * - show BookSubTitle in overdue, reserved and penalty sections if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500] 
 * 
 * 20180307 (Cameron)
 * - fix: add intranet_closedb(); before exit and at the end so that it can log performance [case #H132329] 
 * 
 * 20180222 (Cameron)
 * - fix: access right down to borrow: $c_right['circulation management']['borrow']
 * 
 * 20180214 (Cameron)
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 20171127 (Cameron)
 * - set barcode search bar to left position if $sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] == true [case #F129893]
 * - unify code for ej change on 20160713
 *
 * 20171123 (Henry)
 * - redirect to lost.php if lost access right in circulation management is set
 *
 * 20170314 (Cameron)
 * - add IP checking for circulation
 *
 * 20170126 (Cameron)
 * - disable alert message of "already in the list!" when scan barcode [case #Z110645]
 *
 * 20161102 (Cameron)
 * - don't show "Loan limit is reached" on top of barcode search when number of borrow book reached loan limit [case #L105102]
 *
 * 20160729 (Cameron)
 * - append timestamp to jquery.alerts.js so that it can refresh to take effect
 *
 * 20160713 (Cameron)
 * - change js include file to script.utf8.* to support Chinese in jquery.alerts.js plugin [ej only]
 *
 * 20160422 (Cameron)
 * - fix bug on showing "ok" and "cancel" button in Chinese when click "force to loan" [ej only] [overwrite by method on 20160713]
 *
 * 20160202 (Cameron)
 * - add charset=utf-8 for showing message when library is closed [ej only]
 *
 * 20151201 (Cameron)
 * - add function updateRowNo() - reorder row number after removing row(s) but before confirmation
 *
 * 20151118 (Cameron)
 * - trim all leading and trailing space ( including multiple byte space) for barcode
 *
 * 20150814 Cameron
 * -- set confirm to borrow accompany material or not according to user's choice in borrow_ajax_handler()
 *
 * 20150609 Henry: apply class management permission
 *
 * 20150420 Henry: Fix: Student reach loan limit [Case#P76229]
 *
 * 20150318 Cameron:
 * -- Modify function allowBorrow() to use password prompt rather than plain text
 * -- Add function authenticate()
 *
 * 20141211 Ryan: Added "$" to Column Fine in Fine Records
 *
 * 20141201 Ryan: Added Suspended User Checking
 *
 * 20140728 Henry: implementation of the new work flow
 *
 * 20140509 Yuen: allow to loan (using force to loan password) even overall limit is reached
 *
 * ############ Change Log End ###############
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);

$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && (! ($c_right['circulation management']['borrow'] || $c_right['circulation management']['reserve'] || $c_right['circulation management']['return'] || $c_right['circulation management']['renew'] || $c_right['circulation management']['overdue'] || $c_right['circulation management']['lost']) || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    intranet_closedb();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
// if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && (!isset($c_right['circulation management']) || $global_lockout ))
// {
// include_once($PATH_WRT_ROOT."includes/libaccessright.php");
// $laccessright = new libaccessright();
// $laccessright->NO_ACCESS_RIGHT_REDIRECT();
// exit;
// }

include_once ('TimeManager.php');

$TM = new TimeManager();

$is_open_loan = $libms->IsSystemOpenForLoan($_SESSION['LIBMS']['CirculationManagemnt']['UID']);

// if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && !$TM -> isOpenNow(time()) ) {
// header("Location: index.php");
// }

// if(! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID']))
// header("Location: index.php");

// borrow is successful
if ($_SESSION['LIBMS']['xmsg'] == $Lang['libms']['Circulation']['action_success'] && $_GET['show_user_detail']) {
    $leave_user_after_borrow_book = $libms->get_system_setting('leave_user_after_borrow_book');
    if ($leave_user_after_borrow_book) {
        header("Location: borrow.php?logout=true");
    }
}

$interface = new interface_html("libms_circulation.html");

$GLOBALS['LIBMS']['PAGE'] = "BORROW";
// for new UI [start]
if (! empty($_REQUEST['logout'])) {
    unset($_SESSION['LIBMS']);
    header("Location: borrow.php");
    die();
}

$error_msg = '';

if (isset($_REQUEST['User_Barcode']) || isset($_REQUEST['StudentID'])) {
    
    $libms = new liblms();
    if (isset($_REQUEST['User_Barcode'])) {
        $result = $libms->SELECTFROMTABLE('LIBMS_USER', 'UserID', array(
            'BarCode' => PHPTOSQL($_REQUEST['User_Barcode']),
            'RecordStatus' => '1'
        ));
    } elseif (isset($_REQUEST['StudentID'])) {
        $result = $libms->SELECTFROMTABLE('LIBMS_USER', 'UserID', array(
            'UserID' => PHPTOSQL($_REQUEST['StudentID']),
            'RecordStatus' => '1'
        ));
    }
    
    if (! empty($result)) {
        if (! $_SESSION['LIBMS']['admin']['current_right']['circulation management'] && $c_m_right['circulation management'] && ! $libms->get_class_mgt_can_handle_by_user($result[0][0])) {
            $error_msg = $Lang["libms"]["Circulation"]["NoPermissionHandleUser"];
        } else {
            $_SESSION['LIBMS']['CirculationManagemnt']['UID'] = $result[0][0];
            $result = $libms->SELECTFROMTABLE('LIBMS_GROUP_USER', 'GroupID', array(
                'UserID' => PHPTOSQL($_SESSION['LIBMS']['CirculationManagemnt']['UID'])
            ));

            if (empty($result)) {
                $error_msg = $Lang['libms']['CirculationManagement']['user_group_not_found'];
            } else {
                $right_cm = $c_right['circulation management'];
                if ($right_cm['borrow'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    header("Location: borrow.php");
                    die();
                } else if ($right_cm['return'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: return.php");
                else if ($right_cm['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: renew.php");
                else if ($right_cm['reserve'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: reserve.php");
                else if ($right_cm['overdue'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: overdue.php");
                else if ($right_cm['lost'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: lost.php");
                else {
                    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
                    $laccessright = new libaccessright();
                    intranet_closedb();
                    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
                    exit();
                }
            }
        }
    } else {
        $error_msg = $Lang['libms']['CirculationManagement']['barcode_user_not_found'];
    }
}

if ($error_msg) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    echo "<script language='javascript'>";
    echo "alert('". $error_msg."');";
    echo "window.location='borrow.php?logout=true'";
    echo "</script>";
}

if (! ($c_right['circulation management']['access_without_open_hour'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $TM->isOpenNow(time()))):
    
    ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id='display_center'>
	<span class='alert_text'><?=$Lang['libms']['CirculationManagement']['library_closed']?></span>
</div>
<?
    intranet_closedb();
    exit();
endif;

// Check User is Suspended
$isSuspended = $libms->SELECTFROMTABLE('LIBMS_USER', 'Suspended', array(
    'UserID' => PHPTOSQL($_SESSION['LIBMS']['CirculationManagemnt']['UID'])
));
if (is_null($isSuspended[0]['Suspended'])) {
    $uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];
    $User = new User($uID, $libms);
    
    $recordManager = new RecordManager($libms);
    
    $userInfo = $User->userInfo;
    $cond[] = "ol.`RecordStatus` IN ('OUTSTANDING')";
    $overdues = $recordManager->getOverDueRecords($userInfo['UserID'], $cond);
    // for new UI [end]
}
$interface->LAYOUT_START();

if ($sys_custom['eLibraryPlus']['Circulation']['LargeFont']) {
    echo '<link href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/elibplus_large_font.css" rel="stylesheet" type="text/css">'."\n";
}
?>

<link href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.css"
	rel="stylesheet" type="text/css">
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>lang/script<?=$junior_mck?'.utf8':''?>.<?=$intranet_session_language?>.js"></script>
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.js?<?=time()?>"></script>

<script type="text/javascript">
var overlimit;
var cached_password=false;
var password_match=false;

function authenticate(caller, uniqueBookID) {
	if( cached_password == '' || cached_password == null){
		cached_password = false;
		return false;
	}		
	$.get(	'override_ajax.php',
			{ override_password : cached_password	},
			function(data){
				if(data !='1'){
					cached_password = false;
					jAlert("<?=$Lang["libms"]["Circulation"]["WrongPassword"]?>", "<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_borrow"]?>",function(){return allowBorrow(caller, uniqueBookID);});
				}else{
					password_match=true;
					allow_Burrow_switchon(caller,uniqueBookID);
				}
				return;
			}
	);
}

function allowBorrow(caller, uniqueBookID){
	<?
$tmp_override_password = $libms->get_system_setting('override_password');
if (! empty($tmp_override_password)) :
    ?>
			if( !cached_password  && !password_match ){
				jPromptPassword( "<?=$Lang["libms"]["Circulation"]["PleaseEnterPassword"]?>:", "", "<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_borrow"]?>",function(r) {
					if (r) {						
						cached_password = r;
						authenticate(caller, uniqueBookID);
					}	// get input password					
				});
			}
			
			authenticate(caller, uniqueBookID);

	<? else: ?>
		allow_Burrow_switchon(caller,uniqueBookID);
	<? endif; ?>

}
function allow_Burrow_switchon(caller,uniqueBookID){
		THIS=$(caller);
		THIS_ROW=THIS.parents('tr.book_record:first');
		THIS_ROW.find('.borrow_status.alert_text').html('<?=$Lang['libms']['CirculationManagement']['msg']['force_borrow'] ?>');
		THIS_ROW.prop('active',true).addClass('avaliable_row').find('.bookUniqueID').val(uniqueBookID);
		THIS.remove();
		updateTotal();	
}

function allow_Borrow_switchoff($row,button_html, borrow_limit_type){
	//THIS_ROW=$row;
		var sReason = (borrow_limit_type=="CIR") ? "<?=$Lang["libms"]["CirculationManagement"]["msg"]["over_borrow_cat_limit"]?>" : "<?=$Lang['libms']['CirculationManagement']['msg']['over_borrow_limit']?>";
	
		$row.find('.burrow_status').replaceWith('<span class="borrow_status alert_text"><?=$Lang['libms']['CirculationManagement']['n_borrow']?> </span><br />'+sReason);
		$row.removeProp('active').removeClass('avaliable_row').find('.bookUniqueID').val('');
		$row.find('.table_row_tool').append(button_html);
		//THIS.remove();
		updateTotal();	
	
}


function updateTotal(){
	total = $('#borrow_table tr.avaliable_row').length;
	$('em#borrowCount').html(total);
	
	if (($('em#borrow_count_balance').html() - total) <= 0){
		//$('span#OverLimit').show();
		$('em#borrowCount').addClass('alert_text');
		//$('#borrow_add_book').prop('disabled', true);
		//$("#input_code_text").prop('disabled', true);
	}
	else{
		$('span#OverLimit').hide();
		$('em#borrowCount').removeClass('alert_text');
		//$('#borrow_add_book').prop('disabled', false);
		//$("#input_code_text").prop('disabled', false);
	}
}


function borrow_ajax_handler(ajaxReturn){
	if (ajaxReturn.success){
		table = $('#borrow_table');
		$row = $(ajaxReturn.html);
		
		table.append($row);
		table.find('a.delete:last').click(function(){
			$(this).parents('tr:first').remove();
			updateRowNo();
			updateTotal();
		});

		cirtype = ajaxReturn.CirculationTypeCode;
		if ( $('tr[borrow][cirtype="'+cirtype+'"][active]').length > ajaxReturn.limit ){
			if($row.find('input.set_borrow').length <= 0){
				allow_Borrow_switchoff($row, ajaxReturn.button, "CIR");
			}
		} else if (($('em#borrow_count_balance').html() - total) <= 0){
			// disallow when overall limit is reached
			if($row.find('input.set_borrow').length <= 0){
				allow_Borrow_switchoff($row, ajaxReturn.button, "OVERALL");
			}
			
		}
		
		updateTotal();
		
		var s = ajaxReturn.script;
		if (s != null) {		
			s = s.replace("<script>","");
			s = s.replace("<\/script>","");
			eval(s);
		}
		
	}else{
		if(ajaxReturn.LocationIncorrect)
			alert('<?=$Lang["libms"]["Circulation"]["NoPermissionHandleBook"]?>');
		else
			alert('<?=$Lang['libms']['CirculationManagement']['msg']['n_barcode']?>');
	}
}

$().ready( function(){
	
	$.ajaxSetup({ cache: false});
	
	$(window).on('beforeunload', function(){
		if ($('#borrow_table tr').length > 1)
			return '<?=$Lang['libms']['CirculationManagement']['msg']['comfirm_leave']?>';
	});
	
	$('input#borrow_submit').click(function(){
		$(window).off('beforeunload');
	});
	
	if (typeof(NoRuleNoGroup)!="undefined")
	{
		$('span#OverLimit').html("<?=$Lang["libms"]["GroupManagement"]["Msg"]["no_group_right"]?><br />");
	}

	$('#borrow_add_book').click( function(){
//		$('#input_code_text').val($('#input_code_text').val().toUpperCase().replace(" ",''));
		$('#input_code_text').val($('#input_code_text').val().toUpperCase().replace(/^\s+|^　+|\s+$|　+$/gm,''));
		
		var codemod = ($('#input_code_text').val()).replace(/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~]/g,'-');
		if (codemod!=''){
			if ($('tr#'+codemod).length ==0){
				$.getJSON(
						'borrow_ajax_add.php',
						{ 
							barcode : $('#input_code_text').val() ,
							ID : $('#borrow_table tr').length
						},
						'JSON'
				)
				.success(borrow_ajax_handler)
				.error(function(){ alert('Error on AJAX call!'); });
				
				
				
			}else{
//				alert("<?=$Lang['libms']['CirculationManagement']['msg']['scanned']?>");
				
			}
		}
		$('#input_code_text').val('');		
		$('#input_code_text').focus();
		return false;
	});
	
	$('#input_code_text').keyup(function(event) {
	  if (event.which == 13) {
		$('#borrow_add_book').click();
	  }  
	});


	$('#reset').click(function(){
		$('#borrow_table tr').not('tr:first').remove();
		updateTotal();
	});
	$('#input_code_text').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});
	
	$(document).bind('keydown',function(e){
			
			switch (e.keyCode)
			{
			    case 120:	// F9
					$('input#borrow_submit').click();
					return false;
			    default:
			      return true;
			}
  
  
		});
	
	$('#input_code_text').focus();
	updateTotal();
	
	<?if(! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID']) || isset($_REQUEST['show_user_detail'])){?>
		<?if(! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])){?>
		$('div.user_record_detail').append('<div style="position: absolute;top:75px;left:200px;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50);background-color: #ffffff;"></div>');
		//$('<div style="position: absolute;top:75px;left:200px;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>').insertBefore('div.user_record_detail');
		<?}?>
		$('#input_code_text_barcode').focus();
	<?}?>
});


function checkform(frmObj)
{
	if ($('#borrow_table tr.avaliable_row').length<=0)
	{
		alert("<?=$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed']?>");
		$('#input_code_text').focus();
		return false;
	}
}

function updateRowNo() {
	var i = 1;
	$('#borrow_table tr span.row_id').each(function(){
		$(this).html(i++);
	});
}

//for new UI [start]
//function UpdateStudentList(ClassSelected)
//{
//	if (ClassSelected!="")
//	{
//		$( "#StudentListSpan" ).html( "<?=$Lang['General']['Loading']?>" );
//		$.ajax({
//	 	  type: "POST",
//		  url: "ajax_load.php",
//	  	  data: { Task: "ClassList", ClassName: ClassSelected }
//		}).done(function( html ) {
//		    $( "#StudentListSpan" ).html( html );
//		});
//	}
//}

//function checkFormBarcode(formObj)
//{
//	if(formObj.User_Barcode.value=="" || formObj.User_Barcode.value==" ")
//	{
//		formObj.User_Barcode.focus();
//		return false;
//	}
//}

//function SelectStudent(StudentID, FormObj)
//{
//	if (StudentID!="")
//	{
//		FormObj.submit();
//	}
//	
//}
//for new UI [end]

</script>


<?php
if (! $is_open_loan) {
    echo "<div style='margin: 5em;'><font size='3'>" . $libms->CloseMessage . "</font></div>";
} elseif ($isSuspended[0]['Suspended']) {
    // Suspended = 1
    echo "<div style='margin: 5em;'><font size='3' color='red'><strong>" . $Lang["libms"]["UserSuspend"]["Suspended"] . "</font></strong></div>";
} else {
    ?>
<form name="borrow_form" action='borrow_action.php'
	onsubmit="return checkform(this)">
	<div class="table_board">
		<div class="enter_code"
			<?=$sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] ? 'style="float:left;"':''?>>
			  <?if(isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])){?>
			  <span class="alert_text" id='OverLimit' style='display: none;'><?=$Lang['libms']['CirculationManagement']['msg']['over_borrow_limit']?><br />
			</span>
			  <?}?>
			  <?=$Lang['libms']['CirculationManagement']['barcode'] ?>
			  <input name="" type="text" id='input_code_text'
				class="input_code_text" /> <input name="submit2" type="button"
				id='borrow_add_book' class="formbutton"
				value="&crarr; <?=$Lang["libms"]["btn"]["enter"]?>" />
		</div>

		<table id='borrow_table' class="common_table_list">

			<col />

			<tr>
				<th class="num_check">#</th>
				<th><?=$Lang['libms']['CirculationManagement']['booktitle'] ?> </th>
				<th><?=$Lang['libms']['CirculationManagement']['borrowday']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['status']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['action']?></th>
			</tr>

		</table>
	</div>
	<div class="op_btn">
		<div class="op_num">
			<span><?=$Lang['libms']['CirculationManagement']['no_borrow']?> :</span><em
				id='borrowCount'>0</em>
		</div>
		<div class="form_btn">
			<input name="submit2" id='borrow_submit' type="submit"
				class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['confirm'].$Lang["libms"]["CirculationHotKeys"]["Confirm"]?>" />
			<input id="reset" type="reset" class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['cancel'].$Lang["libms"]["CirculationHotKeys"]["Cancel"]?>" />
		</div>
	</div>
</form>

<!--for new UI [start]-->
<?
    if (isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])) {
        if (! empty($User->borrowedBooks)) {
            ?>
<div class="table_board" id="current_loan">
	<h1 class="alert_header"><?=$Lang["libms"]["CirculationManagement"]["book_overdue"]?></h1>
	<table class="common_table_list">
		<col />
		<thead>
			<tr>
				<th class="num_check">#</th>
				<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['borrowday']?> </th>
				<th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
				<!--  <th><?=$Lang['libms']['CirculationManagement']['status']?></th> -->
			</tr>
		
		<?
            $i = 0;
            if (! empty($User->borrowedBooks))
                foreach ($User->borrowedBooks as $book) :
                    $displayBookTitle = $book['BookTitle'];
                    if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
                        $displayBookTitle .= $book['BookSubTitle'] ? " :" . $book['BookSubTitle'] : "";
                    }
                    $displayBookTitle .= " [".$book['BarCode']."]";
                
                    $duetime = strtotime($book['DueDate']);
                    $nowtime = time();
                    if (time() - $duetime > 60 * 60 * 24) {
                        $is_overdue = true;
                        $tm = new TimeManager($libms);
                        
                        $overdue_day_count = $tm->dayForPenalty($duetime, $nowtime);
                    } else {
                        $is_overdue = false;
                    }
                    $i ++;
                    if ($is_overdue) {
                        ?>
					<tr <? if ($is_overdue) echo 'class="alert_row"'; ?>>
				<td><?=$i?></td>
				<td><?=$displayBookTitle?></td>
				<td><?=date('Y-m-d',strtotime($book['BorrowTime']))?></td>
				<td>
					  	  <? if ($is_overdue): ?>
							<span class="alert_late"><?=$book['DueDate']?> ( <?=$Lang['libms']['CirculationManagement']['msg']['overdue1']?> <?=$overdue_day_count?><?=$Lang['libms']['CirculationManagement']['msg']['overdue2']?>)</span>
						  <? else: ?>
						    <?=$book['DueDate']?>
						  <? endif; ?>
					  </td>


				<!--
					  <td><? if ($is_overdue): ?>
							<span class="alert_late"><?=$Lang['libms']['CirculationManagement']['msg']['overdue1']?> <?=$overdue_day_count?><?=$Lang['libms']['CirculationManagement']['msg']['overdue2']?></span>
						  <? endif; ?>
					  </td>
					   -->
			</tr>
				<?
                    
}
                    unset($is_overdue);
                endforeach
            ;
            ?>
		
		</thead>

		</tbody>
	</table>
</div>
<?
        
}
        $is_ready = FALSE;
        foreach ($User->reservedBooks as $book) {
            if ($book['RecordStatus'] == 'READY') {
                $is_ready = TRUE;
                break;
            }
        }
        if ($is_ready) {
            ?>
<div class="table_board" id="current_reservation">
	<h1><?=$Lang['libms']['CirculationManagement']['reserved']?></h1>
	<table class="common_table_list">
		<col />
		<thead>
			<tr>
				<th class="num_check">#</th>
				<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['reserveday']?></th>
				<!-- <th><?=$Lang['libms']['CirculationManagement']['reserverperiod']?></th>  -->
				<th><?=$Lang['libms']['CirculationManagement']['status']?></th>
			</tr>
				<?
            $i = 0;
            if (! empty($User->reservedBooks))
                foreach ($User->reservedBooks as $book) :
                    if ($book['RecordStatus'] == 'READY')
                        $is_ready = TRUE;
                    else
                        $is_ready = FALSE;
                    $i ++;
                    if ($book['RecordStatus'] == 'READY') {
                        $displayBookTitle = $book['BookTitle'];
                        if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
                            $displayBookTitle .= $book['BookSubTitle'] ? " :" . $book['BookSubTitle'] : "";
                        }
                        
                        ?>
			<tr <? if ($is_ready) echo 'class="avaliable_row"'; ?>>
				<td><?=$i?></td>
				<td><?=$displayBookTitle?></td>
				<td><?=date('Y-m-d',strtotime($book['ReserveTime']))?></td>
				<!-- <td><?=$book['ExpiryDate']?></td> -->
				<td><? if ($is_ready): ?>
								<span class="alert_avaliable"><?=$Lang['libms']['CirculationManagement']['y_borrow']?></span>
							  <?else:?>
								<?=$Lang['libms']['CirculationManagement']['n_borrow']?>
							  <? endif; ?>
						  </td>
			</tr>
				<? } endforeach; ?>
			</thead>
		<tbody>
		</tbody>
	</table>
</div>
<?}if (!empty($overdues)){?>
<div class="table_board" id="current_penal">
	<h1 class="alert_header"><?=$Lang['libms']['CirculationManagement']['payment']?></h1>
	<table class="common_table_list">
		<col />
		<thead>
			<tr>
				<th class="num_check">#</th>
				<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['dueday']?> </th>
				<th><?=$Lang['libms']['CirculationManagement']['returnday']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['overdue_type']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['paied']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['payment']?></th>
			</tr>
		<?
            $i = 0;
            if (! empty($overdues))
                foreach ($overdues as $overdue) :
                    $i ++;
                    $displayBookTitle = $overdue['BookTitle'];
                    if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
                        $displayBookTitle .= $overdue['BookSubTitle'] ? " :" . $overdue['BookSubTitle'] : "";
                    }
                    $displayBookTitle .= " [".$overdue['BarCode']."]";
                    ?>
		<tr>
				<td><?=$i?></td>
				<td><?=$displayBookTitle?></td>
				<td><?=$overdue['DueDate']?></td>
				<td><?=(empty($overdue['ReturnedTime']) || $overdue['ReturnedTime'] == 0)?'-':date('Y-m-d',strtotime($overdue['ReturnedTime']))?></td>
				<td><?=empty($overdue['DaysCount'])?$Lang["libms"]["book_status"]["LOST"]:sprintf($Lang['libms']['CirculationManagement']['overdue_with_day'], $overdue['DaysCount'])?></td>
				<td nowrap="nowrap">$<label class='PaymentReceived'><?=empty($overdue['PaymentReceived'])?'0':$overdue['PaymentReceived']?></label></td>
				<td><span class="alert_text">$<?=$overdue['Payment']-$overdue['PaymentRecevied']?></span></td>
			</tr>
		<? endforeach; ?>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<?}}?>
<!--for new UI [end]-->
<?php
}

$interface->LAYOUT_STOP();
intranet_closedb();
?>
