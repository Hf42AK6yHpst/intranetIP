<?php
# modifying by: 

/*************************************************************
 * 
 * 	20170314 (Cameron)
 * 		- add IP checking for circulation
 * 
 * 	20160219 (Cameron)
 * 		- move and combine javascript to js2.php
 * 		- change search by Book_Barcode to Keyword
 * 
 *  20150609 (Henry)
 * 		- apply class management permission
 * 
 * ************************************************************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
if (!$_SESSION['LIBMS']['admin']['check_ip_for_circulation']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (strstr($_SERVER["SCRIPT_FILENAME"], "batch_return.php") || strstr($_SERVER["SCRIPT_FILENAME"], "batch_renew.php"))
{
	# show exit and normal circulation buttons only when in batch return page 
	
//	$libms = new liblms();
	$c_right = $_SESSION['LIBMS']['admin']['current_right'];
	$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
	$c_right = array_merge($c_right,$c_m_right);

	if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && !isset($c_right['circulation management']))
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	
	} 
	$class_str='class="op_function_btn_on"';
	
	
	$class= array(
	    'RENEW' => ($GLOBALS['LIBMS']['PAGE'] == "RENEW")?$class_str:'',
	    'RETURN' => ($GLOBALS['LIBMS']['PAGE'] == "RETURN")?$class_str:''
	);
}
else{
	$IsIndexPage = true;
}
//for new UI [start]
if( isset($_REQUEST['User_Barcode']) || isset($_REQUEST['StudentID'])){
	
//	$libms = new liblms();
	if (isset($_REQUEST['User_Barcode']))
	{
		$result = $libms->SELECTFROMTABLE('LIBMS_USER','UserID',array('BarCode' => PHPTOSQL($_REQUEST['User_Barcode']), 'RecordStatus' => '1'));
	} elseif (isset($_REQUEST['StudentID']))
	{
		$result = $libms->SELECTFROMTABLE('LIBMS_USER','UserID',array('UserID' => PHPTOSQL($_REQUEST['StudentID']), 'RecordStatus' => '1'));
	}
		
	if( !empty($result) ){
		$_SESSION['LIBMS']['CirculationManagemnt']['UID'] = $result[0][0];
		
		$result = $libms->SELECTFROMTABLE('LIBMS_GROUP_USER','GroupID',array('UserID' =>PHPTOSQL($_SESSION['LIBMS']['CirculationManagemnt']['UID'])));
		if( empty($result) ){
			$error_msg = $Lang['libms']['CirculationManagement']['user_group_not_found'];
		}else{
			$right_cm = $c_right['circulation management'];
			if ($right_cm['borrow'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
			{
				header("Location: borrow.php");
				die();
			}
			else if ($right_cm['return'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
				header("Location: return.php");
			else if ($right_cm['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
				header("Location: renew.php");
			else if ($right_cm['reserve'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
				header("Location: reserve.php");
			else if ($right_cm['overdue'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
				header("Location: overdue.php");
			else {
				include_once($PATH_WRT_ROOT."includes/libaccessright.php");
				$laccessright = new libaccessright();
				$laccessright->NO_ACCESS_RIGHT_REDIRECT();
				exit;
			}
		}
			
	}else{
		$error_msg = $Lang['libms']['CirculationManagement']['barcode_user_not_found'];
	}
}
$interface = new interface_html("libms_circulation_user_prompt.html");
include_once($PATH_WRT_ROOT."includes/libclass.php");
#Class List
$lc = new libclass();
$sql = "Select distinct ClassName, ClassName From LIBMS_USER WHERE ClassName IS NOT NULL AND ClassName <> '' ORDER BY ClassName";
$result = $libms->returnArray($sql);
$StudentClasses = $lc->getSelectClass("name=\"StudentClass\" id=\"StudentClass\" onChange=\"UpdateStudentList(this.value)\"",$readerClass,0);

$SaveBtn   = $interface->GET_ACTION_BTN($Lang["libms"]["report"]["Submit"], "submit", "", "SubmitBtn", "");
//$ResetBtn = $interface->GET_ACTION_BTN($button_reset, "reset", "", "CancelBtn", "");
//for new UI [end]

# determine batch returns / renewals rights
if ($c_right['circulation management']['return'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	$section_title_batch_return_renew = $Lang['libms']['CirculationManagement']['batch_return'];
	// ." / ".$Lang["libms"]["CirculationManagement"]["renews"];
	if ($c_right['circulation management']['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
	{
		$section_title_batch_return_renew .= " / ".$Lang["libms"]["CirculationManagement"]["renews"];
	}
	$batch_return_renew_path = "batch_return.php";
} elseif ($c_right['circulation management']['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	$section_title_batch_return_renew = $Lang['libms']['CirculationManagement']['batch_renew'];
	$batch_return_renew_path = "batch_renew.php";
}
?>
<?php 
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/js2.php");
?>

<div class="op_function_btn">
    <ul>
<?php if ($c_right['circulation management']['return'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) { ?>
        <li <?=$class['RETURN']?>><a href="batch_return.php"><span><?=$Lang['libms']['CirculationManagement']['batch_return']."<font size='-1'>".$Lang["libms"]["CirculationHotKeys"]["BatchReturn"]."</font>"?></span></a></li>
<?php } ?>
<?php if ($c_right['circulation management']['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) { ?>
        <li <?=$class['RENEW']?>><a href="batch_renew.php"><span><?=$Lang['libms']['CirculationManagement']['batch_renew']."<font size='-1'>".$Lang["libms"]["CirculationHotKeys"]["Renew"]."</font>"?></span></a></li>
<?php } ?>
        
		<div style="position:absolute;top:40px; left:30px;"></li>
		<?php if($c_right['circulation management']['return'] || $c_right['circulation management']['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']): ?>
        	<li class="btn_leave"><a href="borrow.php"><span><?=$Lang['libms']['CirculationManagement']['normal_circulation']."<font size='-1'>".$Lang["libms"]["CirculationHotKeys"]["Borrow"]."</font>"?></span></a></li>
        	<li class="btn_leave"><a href="<?=$batch_return_renew_path?>"><span><?=$section_title_batch_return_renew."<font size='-1'>".$Lang["libms"]["CirculationHotKeys"]["BatchReturn"]."</font>"?></span></a></li>
        	<!--<li class="btn_leave"><a href="borrow.php?logout=true"><span><?=$Lang['libms']['CirculationManagement']['leave']."<font size='-1'>".$Lang["libms"]["CirculationHotKeys"]["Exit"]."</font>"?></span></a></li>-->
        <?php endif;?>
        </div>
        <?if(!$IsIndexPage):?>
        <div style="position:absolute;top:0px; <?=Get_Lang_Selection('Right:50px;','Right:50px;')?>">
		<table width="200" border="0" cellpadding="2" cellspacing="0">
	        <tr>
				<td nowrap="nowrap">
					<div class="module_title_text"><span><?=$Lang['libms']['CirculationManagement']['search_book_record']?></span></div>
					<form name='book_search_form' id='book_search_form' onSubmit="return showSearchResult(this)">
						&nbsp; &nbsp; &nbsp; <?= $Lang["libms"]["CirculationManagement"]['search_book']["searchby"] ?> [F3] :
						<input type="text" id='input_keyword' name="Keyword" value="" />			
						<?= $interface->GET_ACTION_BTN($Lang["libms"]["pre_label"]["search"], "submit", "", "SearchBookBtn", "") ?>
					</form>
					<!--&nbsp; <span class='alert_text'><?=$error_msg2?></span>-->
				</td>
			</tr>
        </table>
        </div>
        <?endif;?>
    </ul>
</div>
