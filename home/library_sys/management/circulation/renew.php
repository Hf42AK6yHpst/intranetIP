<?php
/**
 * Using:      
 * 
 * Note:	Please use UTF-8 for editing
 * 
 * Log
 *
 * Date     2019-10-11 (Cameron)
 *          - use large font for common_table_list if $sys_custom['eLibraryPlus']['Circulation']['LargeFont'] is set [case #K155570]
 * 
 * Date     2019-03-19 (Cameron)
 *          - don't show colon if there's no book sub-title [ej.5.0.9.3.1]
 * 
 * Date     2019-03-05 (Cameron)
 *          - show BookSubTitle if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * 
 * Date     2018-02-22 (Cameron)
 *          - fix: add meta characterset (utf-8) to show no access right message
 *
 * Date		2018-02-05 (Henry)
 * 			- disable submit button when clicked [Case#Z135161]
 * 
 * Date		2017-11-27 (Cameron)
 * 			- set barcode search bar to left position if $sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] == true [case #F129893]
 *  		- unify code for ej change on 20160713
 * 
 * Date		2017-03-14 (Cameron)
 * 			- add IP checking for circulation
 * 
 * Date		2016-07-29 (Cameron)
 * 			- append timestamp to jquery.alerts.js so that it can refresh to take effect
 * 
 * Date		2016-07-13 (Cameron)
 * 			- change js include file to script.utf8.* to support Chinese in jquery.alerts plugin [ej only]
 * 
 * Date		2016-02-19 [Cameron]
 * 			modify allow_Renew_switchon() to stop further process when open the same renew page of a user in two sessions in the same browser,
 *  		then quit user in one session but press "force to renew" in another session
 * 
 * Date		2015-11-18 [Cameron]
 *			- trim all leading and trailing space ( including multiple byte space) for barcode  	
 *
 * Date		2015-11-05 [Cameron]
 *			- don't prompt user to renew accompany material or not when renew a book, the flag can be retrieved from borrow log 
 *			- repalce space to empty string for barcode input ( in #barcode_submit click event )
 *			- change input barcode to upper case so that both upper and lower case barcode can be searched
 *
 * Date		2015-10-30 [Cameron]
 * 			- fix bug on showing accompany material content in jConfirm
 *
 * Date		2015-10-16 [Cameron]
 * 			- fix set focus issue after calling promptToRenewAttachment()
 *  
 * Date		2015-10-13 [Cameron]
 * 			- add prompt confirm (jConfirm) if user want to renew accompany material or not if circulation_borrow_return_accompany_material 
 * 			is true and the book item has accompany material
 * 
 * Date		2015-07-13 [Cameron]
 * 				- show AccompanyMaterial if circulation_display_accompany_material is set
 * 
 * Date		20150609 (Henry)
 * 				- apply class management permission
 * 
 * Date		2015-03-18 [Cameron] 
 * 				-- Modify function allowRenew() to use password prompt rather than plain text
 * 				-- Add function authenticate()
 * 
 * Date		2014-12-01 [Ryan] Add Suspended User Checking  
 * Date		2013-06-13 [Cameron] Fix bug for tb_show in parsing argument with single quote /double quote 
 * Date 	2013-05-07 [Cameron]
 * 			Modify the parameter of tb_show, pass BarCode to book_loans_history.php 
 * 			for displaying the borrow history of a book 
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// error_reporting( E_ALL );
// using
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ("BookManager.php");
include_once ("User.php");
include_once ("RecordManager.php");
include_once ("RightRuleManager.php");
intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);

$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['renew'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}

$is_open_loan = $libms->IsSystemOpenForLoan($_SESSION['LIBMS']['CirculationManagemnt']['UID']);

if (! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID']))
    header("Location: index.php");

include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
$interface = new interface_html("libms_circulation.html");

$GLOBALS['LIBMS']['PAGE'] = "RENEW";

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];
// Check User is Suspended
$isSuspended = $libms->SELECTFROMTABLE('LIBMS_USER', 'Suspended', array(
    'UserID' => PHPTOSQL($_SESSION['LIBMS']['CirculationManagemnt']['UID'])
));
if (is_null($isSuspended[0]['Suspended'])) {
    $User = new User($uID, $libms);
    $BookManager = new BookManager($libms);
    $RightRuleManager = $User->rightRuleManager;
    
    $userInfo = $User->userInfo;
    
    $borrowCount = 0;
    $overdueCount = 0;
    $books = $User->borrowedBooks;
}
if (! empty($books))
    foreach ($books as &$book) {
        $borrowCount ++;
        $book['limit'] = $RightRuleManager->get_limits($book['CirculationTypeCode']);
        $book['canRenew'] = $RightRuleManager->canRenew($book['UniqueID'], $book['MSG']);
    }
unset($book);

$xmsg = $_SESSION['LIBMS']['xmsg'];
unset($_SESSION['LIBMS']['xmsg']);

$interface->LAYOUT_START();

if ($sys_custom['eLibraryPlus']['Circulation']['LargeFont']) {
    echo '<link href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/elibplus_large_font.css" rel="stylesheet" type="text/css">'."\n";
}
?>
<link href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.css"
	rel="stylesheet" type="text/css">
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>lang/script<?=$junior_mck?'.utf8':''?>.<?=$intranet_session_language?>.js"></script>
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.js?<?=time()?>"></script>

<script type="text/javascript">
var overlimit;
var cached_password=false;
var password_match=false;

function authenticate(caller, uniqueBookID) {
	if( cached_password == '' || cached_password == null){
		cached_password = false;
		return false;
	}
			
	$.get(	'override_ajax.php',								
			{ override_password : cached_password	},
			function(data){
				if(data !='1'){
					cached_password = false;										
					jAlert("<?=$Lang["libms"]["Circulation"]["WrongPassword"]?>", "<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"]?>",function(){return allowRenew(caller, uniqueBookID);});
				}else{
					password_match=true;
					allow_Renew_switchon(caller,uniqueBookID);
				}
				return;
			}
	);
}

function allowRenew(caller, uniqueBookID){
	<?
$tmp_override_password = $libms->get_system_setting('override_password');
if (! empty($tmp_override_password)) :
    ?>
			if( !cached_password  && !password_match ){
				jPromptPassword( "<?=$Lang["libms"]["Circulation"]["PleaseEnterPassword"]?>:", "", "<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"]?>",function(r) {
					if (r) {						
						cached_password = r;
						authenticate(caller, uniqueBookID);
					}	// get input password 					
				});
			}

			authenticate(caller, uniqueBookID);

	<? else: ?>
		allow_Renew_switchon(caller,uniqueBookID);
	<? endif; ?>

}
function allow_Renew_switchon(caller,uniqueBookID){
		$('#div_'+uniqueBookID).html('<input class="renewCheckbox" name="renew[]" type="checkbox" value="'+uniqueBookID+'" />');
		$('#div_'+uniqueBookID+' input.renewCheckbox').change( function(){
		var thisCheckbox = $(this);
		thisCheckbox.parents('tr:first').toggleClass('avaliable_row');
		updateTotal();
		if (thisCheckbox.is(':checked')){
			if ($('#AccomMatID_' + thisCheckbox.val()).length) {
				promptToRenewAttachment(thisCheckbox.val());
			}
			$.get('get_borrow_due_date.php',{ UniqueID : thisCheckbox.val() },function (data){
				if (data == 'AlreadyQuit') {
					thisCheckbox.parents('tr:first').toggleClass('avaliable_row');
					thisCheckbox.parent().parent().next().html("-").next().html("<span style=\"color:red; font-weight: bold\"><?=$Lang['libms']['CirculationManagement']['msg']['already_quit']?></span>");
					thisCheckbox.prop('checked', false);
					updateTotal();					
					thisCheckbox.prop('disabled',true);
				}
				else {
					if (thisCheckbox.is(':checked')){
						var currentTime = new Date();
						thisCheckbox.parent().parent().next().html(dateToYMD(currentTime)).next().html(data);
					}
				}
			})
		}else{
			thisCheckbox.parent().parent().next().html("-").next().html("-");
		}
	});
	$('#div_'+uniqueBookID+' input.renewCheckbox').prop('checked', true);
	$('#div_'+uniqueBookID+' input.renewCheckbox').change();
//		$('#div_'+uniqueBookID).append('<input class="renewCheckbox" name="renew[]" type="checkbox" value="'+uniqueBookID+'" />');
//		THIS=$(caller);
//		THIS_ROW=THIS.parents('tr.book_record:first');
//		THIS_ROW.find('.borrow_status.alert_text').html('<?=$Lang['libms']['CirculationManagement']['msg']['f_renew'] ?>');
//		THIS_ROW.prop('active',true).addClass('avaliable_row').find('.bookUniqueID').val(uniqueBookID);
//		THIS.remove();
		updateTotal();	
}

function updateTotal(){
	$('em#renewCount').html($('input.renewCheckbox:checked').length);
}

function dateToYMD(date)
{
    var d = date.getDate();
    var m = date.getMonth()+1;
    var y = date.getFullYear();
    return '' + y +'-'+ (m<=9?'0'+m:m) +'-'+ (d<=9?'0'+d:d);
}

$().ready( function(){
	$.ajaxSetup({ cache: false});
	
	$(window).on('beforeunload', function(){
		if ($('input.renewCheckbox:checked').length > 0)
			return '<?=$Lang['libms']['CirculationManagement']['msg']['comfirm_leave']?>';
	});
	
	$('input#renew_submit').click(function(){
		$(window).off('beforeunload');
	});
	
	$('input.renewCheckbox').change( function(){
		var thisCheckbox = $(this);
		thisCheckbox.parents('tr:first').toggleClass('avaliable_row');
		updateTotal();

		if (thisCheckbox.is(':checked')){
//			if ($('#AccomMatID_' + thisCheckbox.val()).length) {
//				promptToRenewAttachment(thisCheckbox.val());
//			}
//			else {
				$('#barcode').focus();
//			}			
			$.get('get_borrow_due_date.php',{ UniqueID : thisCheckbox.val() },function (data){
				if (data == 'AlreadyQuit') {
					thisCheckbox.parents('tr:first').toggleClass('avaliable_row');
					thisCheckbox.parent().parent().next().html("-").next().html("<span style=\"color:red; font-weight: bold\"><?=$Lang['libms']['CirculationManagement']['msg']['already_quit']?></span>");
					thisCheckbox.prop('checked', false);
					updateTotal();					
					thisCheckbox.prop('disabled',true);
				}
				else {
					if (thisCheckbox.is(':checked')){
						var currentTime = new Date();
						thisCheckbox.parent().parent().next().html(dateToYMD(currentTime)).next().html(data);
					}
				}
			})
		}else{
			thisCheckbox.parent().parent().next().html("-").next().html("-");
			$('#barcode').focus();
		}
	});
	
	$('input#barcode_submit').click(function (){
//		$('#barcode').val($('#barcode').val().toUpperCase().replace(" ",''));
		$('#barcode').val($('#barcode').val().toUpperCase().replace(/^\s+|^　+|\s+$|　+$/gm,''));
		var txtBarcode = $('input#barcode');
		var barcode = txtBarcode.val();
//		txtBarcode.val(txtBarcode.val().toUpperCase());
		var barcodemod = barcode.replace(/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~ ]/g,'-');
		
		if (barcodemod !=''){
			var rowTR = $('tr#' + barcodemod);
			if ( rowTR.length > 0 ){
				rowTR.find('input.renewCheckbox').click();
				rowTR.find('input.set_borrow').click();
			}else{
				alert ('<?=$Lang['libms']['CirculationManagement']['msg']['n_barcode']?>');
				$('#barcode').focus();
			}
		}
		else {
			$('#barcode').focus();
		}
		txtBarcode.val('');
	});
	
	$('input#barcode').keyup(function(event) {
	  if (event.which == 13) {
		$('input#barcode_submit').click();
	  }  
	});

	$('#reset').click(function(){
		$('.renewCheckbox').each(function(){
			THIS= $(this);
			if ( THIS.is(':checked') )
				THIS.click();
		});
	});
	$('#barcode').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});
	$('#barcode').focus();
	
	
	
	$(document).bind('keydown',function(e){
			switch (e.keyCode)
			{
			    case 120:	// F9
					$('input#renew_submit').click();
					return false;
			    default:
			      return true;
			}
  
  
		});
});


function checkform(frmObj)
{
	$('#renew_submit').attr("disabled", "disabled");
	if ($('input.renewCheckbox:checked').length<=0)
	{
		alert("<?=$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed']?>");
		$('#barcode').focus();
		$('input[type="submit"]').removeAttr('disabled');
		return false;
	}
}

/*
function promptToRenewAttachment(bookUniqueID) {

	jConfirm($('#AccomMatContent_' + bookUniqueID).text()+"<?="<br>".$Lang["libms"]["Circulation"]["RenewAccompayMaterial"]?>",
	 "<?=$Lang['libms']["settings"]['system']['circulation_borrow_return_accompany_material']?>", 
	 function(r){
	 	if (r) {
	 		$('#AccomMatID_' + bookUniqueID).attr('checked',true);
	 	}
	 	else {
	 		$('#AccomMatID_' + bookUniqueID).attr('checked',false);
	 	}
	 	$('#barcode').focus();
	 }, 
	 "<?=$Lang['libms']["settings"]['system']['yes']?>", 
	 "<?=$Lang['libms']["settings"]['system']['no']?>");
}
*/
</script>

<?php

if (! $is_open_loan) {
    echo "<div style='margin: 5em;'><font size='3'>" . $libms->CloseMessage . "</font></div>";
} elseif ($isSuspended[0]['Suspended']) {
    // Suspended = 1
    echo "<div style='margin: 5em;'><font size='3' color='red'><strong>" . $Lang["libms"]["UserSuspend"]["Suspended"] . "</font></strong></div>";
} else {
    ?>
<? if(!empty($xmsg)): ?>
<center>
	<div align="center" style='width: 1%'>
		<table border="0" cellpadding="3" cellspacing="0" class="systemmsg">
			<tr>
				<td nowrap="nowrap"><font color="green"><?=$xmsg?></font></td>
			</tr>
		</table>
	</div>
</center>
<?endif;?>
<form name="renew_form" action='renew_action.php'
	onsubmit="return checkform(this)">
	<div class="table_board">
		<div class="enter_code"
			<?=$sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] ? 'style="float:left;"':''?>><?=$Lang['libms']['CirculationManagement']['barcode']?> <input
				name="" type="text" id='barcode' class="input_code_text" /> <input
				id='barcode_submit' name="submit2" type="button"
				class="formsubbutton"
				value="&crarr; <?=$Lang["libms"]["btn"]["enter"]?>" />
		</div>

		<table class="common_table_list">
			<col nowrap="nowrap" />
			<tr>
				<th class="num_check">#</th>
				<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['borrowday']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['renew_left']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['status'] ?></th>
				<th nowrap="nowrap" class="num_check"><?=$Lang['libms']['CirculationManagement']['renew']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['renewday']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
			</tr>
  
  <?
    $i = 0;
    foreach ($books as $book) :
        
        if (! $_SESSION['LIBMS']['admin']['current_right']['circulation management'] && $c_m_right['circulation management'] && ! $libms->get_class_mgt_can_handle_item($book['UniqueID'])) {
            continue; // Henry added
        }
        
        $i ++;
        $searchChar = '/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~ ]/';
        
        $displayBookTitle = $book['BookTitle'];
        if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
            $displayBookTitle .= $book['BookSubTitle'] ? " :" . $book['BookSubTitle'] : "";
        }
        $displayBookTitle .= " [".$book['BarCode']."]";
        
        ?>
	  <tr
				id='<?=preg_replace($searchChar,'-',strtoupper($book['BarCode']))?>'>
				<td><?=$i?></td>
				<td><a
					href="javascript:tb_show('<?=mysql_real_escape_string(intranet_htmlspecialchars($Lang["libms"]["Circulation"]["BookBorrowHistory"]))?>', 'book_loans_history.php?BarCode=<?=urlencode($book['BarCode'])?>&height=500&width=600')"><?=$displayBookTitle?></a></td>
				<td nowrap="nowrap"><?=date('Y-m-d',strtotime($book['BorrowTime']))?></td>
				<td nowrap="nowrap"><?=$book['DueDate']?></td>
				<td nowrap="nowrap">
			  <?
        if ($book['canRenew']) {
            echo ($book['limit']['LimitRenew'] - $book['RenewalTime']);
        } else {
            echo '-';
        }
        ?>
		  </td>
				<td nowrap="nowrap"><span
					<? if (!$book['canRenew']) echo 'class="alert_text"'?>>
				<? if (!$book['canRenew']) : ?>
					<?=$Lang['libms']['CirculationManagement']['n_renew']?>
					</span>
					(<?=$book['MSG']?>)
				<? else: ?>
					<?=$Lang['libms']['CirculationManagement']['y_renew']?>
					</span>
				<? endif; ?>
				<? if (!empty($book['RemarkToUser'])): ?>
			 		<br /> <img src="/images/2009a/eLibplus/icon_alert.png" /><span><?=$book['RemarkToUser']?></span>
		 		<? endif; ?>
				<? if ($libms->get_system_setting("circulation_display_accompany_material") && !empty($book['AccompanyMaterial'])): ?>
					<br />
<?php
            if (($libms->get_system_setting("circulation_borrow_return_accompany_material")) && (! $book['BorrowAccompanyMaterial']) && (! empty($book['AccompanyMaterial']))) {
                $attachmentNote = $Lang["libms"]["Circulation"]["NotBorrowAccompayMaterial"];
            } else {
                $attachmentNote = $Lang["libms"]["book"]["AccompanyMaterial"];
            }
            ?>					
					<span><img src="/images/2009a/eLibplus/icon_alert.png" /><span
						id="AccomMatContent_<?=$book['UniqueID']?>"><?=$book['AccompanyMaterial']?></span><?=' ['.$attachmentNote.']'?></span>
				<? endif; ?>
				<?//if (($libms->get_system_setting("circulation_borrow_return_accompany_material")) && (!empty($book['AccompanyMaterial']))): ?>
<!--					<br />
					<span><input type="checkbox" name="AccomMat_<?=$book['UniqueID']?>" id="AccomMatID_<?=$book['UniqueID']?>" <?=($book['BorrowAccompanyMaterial'] ? 'checked' : '')?> value="checked"/><?=$Lang["libms"]["Circulation"]["RenewWithAccompayMaterial"]?></span>
-->					
				<?// endif; ?>
				
		  </td>

				<td><div id="div_<?=$book['UniqueID']?>">
		  <? if (!$book['canRenew']) : ?>
		  <input name="submit2" type="button" class="set_borrow"
							value="<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"]?>"
							onclick='allowRenew(this,<?=$book['UniqueID']?>)' />
		  <? else: ?>
		  <input class='renewCheckbox' name="renew[]" type="checkbox"
							value="<?=$book['UniqueID']?>" />
		  <? endif; ?>
		  </div></td>
				<td nowrap="nowrap" class='today'>-</td>
				<td nowrap="nowrap" class='newDueDate'>-</td>
			</tr>
  <? endforeach;//( $books as $book ): ?>


</table>
	</div>
	<div class="op_btn">
		<div class="op_num">
			<span><?=$Lang['libms']['CirculationManagement']['no_renew']?> :</span><em
				id='renewCount'> 0</em>
		</div>
		<div class="form_btn">
			<input name="submit2" id='renew_submit' type="submit"
				class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['confirm'].$Lang["libms"]["CirculationHotKeys"]["Confirm"]?>" />
			<input name="submit2" id='reset' type="reset" class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['cancel'].$Lang["libms"]["CirculationHotKeys"]["Cancel"]?>" />
		</div>
	</div>
</form>

<?php
}

$interface->LAYOUT_STOP();

?>