<?php
/*
 * Using: 
 * 
 * ###########################################################
 * Change Log
 * ###########################################################
 *
 * 2019-10-17 [Cameron]
 * - don't retrieve class list based on class management group if current login user is elibrary admin [case #C173182]
 *
 * 2019-05-06 [Cameron]
 * - show quota left in red bold style if it's zero [case #M153872]
 * 
 * 2018-02-23 [Cameron]
 * - fix: add meta characterset (utf-8) to show no access right message
 *  
 * 2017-11-01 [Cameron]
 * - add id UserBalance for update purpose [case #F130172]
 *
 * 2017-03-14 [Cameron]
 * - add IP checking for circulation
 *
 * 2017-02-08 [Cameron]
 * - check if file (photo) exist before display it [DM#770]
 *
 * 2016-07-14 [Cameron]
 * - fix bug on showing loan quota, should show 0 if it's null, otherwise, the row is overlaped
 *
 * 2015-06-09 [Henry]
 * apply class management permission
 *
 * 2015-06-08 [Cameron]
 * - Change IdentityType from "Teaching" to "Staff" in Get_Avaliable_User_List() because both teaching and non teaching staff can borrow book
 *
 * 2015-03-04 Ryan - Shows Student Remarks under barcode
 *
 * 2014-12-10 Henry - added flag $sys_custom['eLibraryPlus']['hideUserBarcodeInputHistory'] to hide the input barcode
 *
 * 2014-11-28 Ryan - Add Teacher selection list
 *
 *
 * ###########################################################
 * 
 */

if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
global $Lang, $sys_custom;

include_once ("User.php");
include_once ("RightRuleManager.php");
include_once ("../../lib/liblibrarymgmt_group_manage_ui.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');

// if (/*(!isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])) ||*/ (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && (!isset($c_right['circulation management']) || $global_lockout)))
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && (! isset($c_right['circulation management']) || $global_lockout))) {
    // dump($c_right);
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];
$ajax_update = isset($_REQUEST["update"]);

$User = new User($uID, $libms);
$userInfo = $User->userInfo;

// for new UI 20140728
if (isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])) {
    $borrowCount = 0;
    $overdueCount = 0;
    
    if (! empty($User->borrowedBooks))
        foreach ($User->borrowedBooks as $book) {
            $borrowCount ++;
            if (time() - strtotime($book['DueDate']) > 60 * 60 * 24)
                $overdueCount ++;
        }
    $revservedCount = count($User->reservedBooks);
    $limit = $User->rightRuleManager->get_limits();
} else {
    $borrowCount = '-';
    $overdueCount = '-';
    $revservedCount = '-';
    $limit = '-';
}

// for new UI [start]
if (! $ajax_update && (isset($_REQUEST['User_Barcode']) || isset($_REQUEST['StudentID']))) {
    $libms = new liblms();
    if (isset($_REQUEST['User_Barcode'])) {
        $result = $libms->SELECTFROMTABLE('LIBMS_USER', 'UserID', array(
            'BarCode' => PHPTOSQL($_REQUEST['User_Barcode']),
            'RecordStatus' => '1'
        ));
    } elseif (isset($_REQUEST['StudentID'])) {
        $result = $libms->SELECTFROMTABLE('LIBMS_USER', 'UserID', array(
            'UserID' => PHPTOSQL($_REQUEST['StudentID']),
            'RecordStatus' => '1'
        ));
    }
    
    if (! empty($result)) {
        if (! $_SESSION['LIBMS']['admin']['current_right']['circulation management'] && $c_m_right['circulation management'] && ! $libms->get_class_mgt_can_handle_by_user($result[0][0])) {
            $error_msg = $Lang["libms"]["Circulation"]["NoPermissionHandleUser"];
        } else {
            $_SESSION['LIBMS']['CirculationManagemnt']['UID'] = $result[0][0];
            
            $result = $libms->SELECTFROMTABLE('LIBMS_GROUP_USER', 'GroupID', array(
                'UserID' => PHPTOSQL($_SESSION['LIBMS']['CirculationManagemnt']['UID'])
            ));
            if (empty($result)) {
                $error_msg = $Lang['libms']['CirculationManagement']['user_group_not_found'];
            } else {
                $right_cm = $c_right['circulation management'];
                if ($right_cm['borrow'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    header("Location: borrow.php");
                    die();
                } else if ($right_cm['return'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: return.php");
                else if ($right_cm['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: renew.php");
                else if ($right_cm['reserve'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: reserve.php");
                else if ($right_cm['overdue'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                    header("Location: overdue.php");
                else {
                    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
                    $laccessright = new libaccessright();
                    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
                    exit();
                }
            }
        }
    } else {
        $error_msg = $Lang['libms']['CirculationManagement']['barcode_user_not_found'];
    }
}
if (! $ajax_update) {
    $interface = new interface_html("libms_circulation_user_prompt.html");
    include_once ($PATH_WRT_ROOT . "includes/libclass.php");
    // Class List
    $lc = new libclass();
    
    $sql = "Select distinct ClassName, ClassName From LIBMS_USER WHERE ClassName IS NOT NULL AND ClassName <> '' ORDER BY ClassName";
    $result = $libms->returnArray($sql);
    $StudentClasses = $lc->getSelectClass("name=\"StudentClass\" id=\"StudentClass\" onChange=\"UpdateStudentList(this.value)\"", $readerClass, 0);
    
    // Henry added 20150609
    if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && !$_SESSION['LIBMS']['admin']['current_right']['circulation management'] && $c_m_right['circulation management']) {
        $classArr = $libms->get_class_mgt_classes();
        $StudentClasses = '<select name="StudentClass" id="StudentClass" onchange="UpdateStudentList(this.value)">';
        for ($i = 0; $i < count($classArr); $i ++) {
            $StudentClasses .= '<option value="' . $classArr[$i] . '">' . $classArr[$i] . '</option>';
        }
        $StudentClasses .= '</select>';
        // $StudentClasses = $lc->getSelectClass("name=\"StudentClass\" id=\"StudentClass\" onChange=\"UpdateStudentList(this.value)\" disabled",$classArr[0],0);
    }
    
    $GroupManage = new liblibrarymgmt_group_manage();
    
    // 2015-06-08: both teaching and non teaching staff can borrow book
    // Teacher Selection
    $UserList = $GroupManage->Get_Avaliable_User_List($AddUserID, 'Staff', $YearClassSelect, $ParentStudentID, $GroupID);
    $TeacherList = array();
    foreach ($UserList as $row) {
        $TeacherList[] = array(
            $row['UserID'],
            $row['Name']
        );
    }
    $libinterface = new interface_html();
    $ReturnRx = $libinterface->GET_SELECTION_BOX($TeacherList, "name=\"StudentID\" onChange=\"SelectStudent(this.value, this.form)\"  style=\"width:12em\" ", "-- " . $Lang["libms"]["general"]["select"] . " --");
    
    $SaveBtn = $interface->GET_ACTION_BTN($Lang["libms"]["report"]["Submit"], "submit", "", "SubmitBtn", "");
}
// $ResetBtn = $interface->GET_ACTION_BTN($button_reset, "reset", "", "CancelBtn", "");
// for new UI [end]
?>
<?if(!isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])){?>
<div class="user_info"
	style="position: absolute; top: 100px; left: 150px;">
	<table width="200" border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td nowrap="nowrap">
				<!--<div class="module_title_text"><span><?=$Lang['libms']['CirculationManagement']['normal_circulation']?></span></div>-->

				<form id='user_form' name='user_form'
					onSubmit="return checkFormBarcode(this)" action="borrow.php">
					<br />&nbsp; &nbsp; &nbsp; <?=$Lang['libms']['CirculationManagement']['user_barcode']?> :
						<br />&nbsp; &nbsp; &nbsp; <input style="width: 150px" type="text"
						id='input_code_text_barcode' name="User_Barcode" value=""
						<?=$sys_custom['eLibraryPlus']['hideUserBarcode'] || $sys_custom['eLibraryPlus']['hideUserBarcodeInputHistory']?'autocomplete="off"':''?> />
					<br />&nbsp; &nbsp; &nbsp; <?=$SaveBtn?>
						<?=$ResetBtn?>
					</form> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class='alert_text'><?=$error_msg?></span>
				<!--<?php if ($libms->get_system_setting("circulation_student_by_selection")) {?>
				</td><td nowrap="nowrap">
					<form id='student_form' name='student_form' >
						&nbsp; <?=$Lang["libms"]["CirculationManagement"]["student_only"]?> :
						<?= $StudentClasses ?>
						<span id="StudentListSpan">&nbsp;</span>
					</form>
			<?php } ?>-->
			</td>
		</tr>
			<?php if ($libms->get_system_setting("circulation_student_by_selection")) {?>
			<tr>
			<td nowrap="nowrap" colspan="2">
				<form id='student_form' name='student_form' action="borrow.php">
						&nbsp; &nbsp; &nbsp; <?=$Lang["libms"]["CirculationManagement"]["student_only"]?> :
						<br />&nbsp; &nbsp; &nbsp; <?= $StudentClasses ?>
						<br />&nbsp; &nbsp; &nbsp; <span id="StudentListSpan">&nbsp;</span>
				</form>
			</td>
		</tr>
			<?php } ?>
			<?php if (($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $_SESSION['LIBMS']['admin']['current_right']['circulation management']) && $libms->get_system_setting("circulation_teacher_by_selection")) {?>
			<tr>
			<td nowrap="nowrap" colspan="2">
				<form id='student_form' name='student_form' action="borrow.php">
						&nbsp; &nbsp; &nbsp; <?=$Lang["libms"]["CirculationManagement"]["teacher_only"]?> :
						<br />&nbsp; &nbsp; &nbsp; <?= $ReturnRx ?>
					</form>
			</td>
		</tr>
			<?php } ?>
        </table>
</div>
<?} else {?>
	
<? if(!$ajax_update): ?>


<div class="user_info" style="position: relative; top: -5px; left: 0px;">
	<input type='button' onclick="location.href='borrow.php?logout=true'"
		name=""
		value="<?=$Lang['libms']['CirculationManagement']['leave']?> <?=$Lang["libms"]["CirculationHotKeys"]["Exit"]?>" />
</div>
<div class="user_info" style="position: relative; top: 40px; left: 0px;">
	<div class="user_title">
		<div class="photo">
			 <? if (!empty($userInfo['UserPhoto']) && is_file($intranet_root.$userInfo['UserPhoto'])): ?>
				<img src="<?=$userInfo['UserPhoto']?>" /><?php else: ?>
				<img src="/images/sample_photo.jpg" /><?php endif; ?>
		</div>
		<span>
			<?=$userInfo[$Lang['libms']['SQL']['UserNameFeild']]?>
			<? if ($userInfo['UserType'] =='S'):?>
				<br />(<?=$userInfo['ClassName']?> - <?=$userInfo['ClassNumber']?>)
			<? endif;
         // ($userInfo['UserType'] =='S'):
                      // 20150304 - Show user Remarks
        $remarks = $libms->getRemarks($userInfo['UserID']);
        $remarks = $remarks[0]['RemarkContent'];
        ?>
				<br /><?=$sys_custom['eLibraryPlus']['hideUserBarcode']?'':'#'.$userInfo['BarCode']?>
				<br /><?= $remarks ? '<span style="color:black">* '.nl2br($remarks).'</span>' : ''?>
		</span>
	</div>
	<a href="#" id="user_detail" class="user_detail_on"><?=$Lang['libms']['CirculationManagement']['moredetail']."<font size='-1'>".$Lang["libms"]["CirculationHotKeys"]["Details"]."</font>"?></a>
<? endif;//($ajax_update): ?>

<?php 
    $quotaLeft = $limit['LimitBorrow'] - $borrowCount;
    $displayQuotaLeft = ($quotaLeft > 0) ? '>'.$quotaLeft : 'class="alert_text">'.$quotaLeft; 
?>
	<div class="user_record">
		<ul>
			<li><span><?=$Lang['libms']['CirculationManagement']['borrowed']?> :</span><em> <?=$borrowCount?></em></li>
			<li><span><?=$Lang['libms']['CirculationManagement']['book_overdue'] ?> :</span>  
			<?if ($overdueCount > 0): ?>
				<em class="alert_text">
			<? else:?>
				<em>
			<? endif; ?> 
				<?=$overdueCount?> </em></li>
			<li><span><?=$Lang['libms']['CirculationManagement']['borrow_limit']?> :</span>
				<em> <?=$limit['LimitBorrow']?$limit['LimitBorrow']:'0'?></em></li>
			<li><span><?=$Lang['libms']['CirculationManagement']['limit_left']?> :</span>
				<em id='borrow_count_balance' <?php echo $displayQuotaLeft;?></em></li>
			<li class="sep"></li>
			<li><span><?=$Lang['libms']['CirculationManagement']['book_reserve']?> :</span>
				<em> <?=$revservedCount?></em></li>
			<li class="sep"></li>
			<li><span><?=$Lang['libms']['CirculationManagement']['left_pay']?> :</span>
				<span id="UserBalance" style="margin-left: 0px;"> <?if ($userInfo['Balance'] < 0 ): ?> <em
					class="alert_text"><? else:?><em><? endif; ?> $<?=-$userInfo['Balance']?></em></span></li>
		</ul>
	</div>
<? if(!$ajax_update): ?>
</div>
<? endif;//($ajax_update): ?>

<?}?>
