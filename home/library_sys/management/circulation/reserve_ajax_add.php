<?php

// # being modified by : 

/**
 * Log :
 *
 * 20190423 (Henry)
 * - added IntegerSafe to prevent SQL injection 
 * 
 * 20190328 (Cameron)
 * - retrieve BookSubTitle, show it in gen_reserve_html_row() if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * - add order by BookSubTitle, ACNO after BookTitle in search result  
 * 
 * 20180910 (Cameron)
 * - fix: pass pendingBookID to reserve book checking (reserve_add_book.click event) to check current list against book reservation rule
 *
 * 2018-02-22 [Cameron]
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 2017-04-06 [Cameron]
 * - handle apostrophe problem by applying stripslashes() for search field when get_magic_quotes_gpc() is not set (php5.4)
 *
 * 2017-03-14 [Cameron]
 * - add IP checking for circulation
 *
 * 2016-07-29 [Cameron]
 * - fix bug: should use INNER JOIN instead of LEFT JOIN to filter out deleted book record
 *
 * 2016-04-06 [Cameron]
 * - fix bug on filtering LIBMS_BOOK_UNIQUE.RecordStatus to exclude DELETE,LOST,WRITEOFF and SUSPECTEDMISSING record
 *
 * 2015-12-01 [Cameron]
 * - show row number and BarCode in list
 * - add row_id class for re-order row number
 * - set BarCode to cell id to avoid duplicate input
 *
 * 2015-07-14 [Cameron]
 * - show AccompanyMaterial if circulation_display_accompany_material is set
 * - special handle backslash for return json ($bookInfo)
 *
 * 20150610 (Henry)
 * - apply class management permission
 *
 * 20140731 (Henry)
 * overwriting the remark for user when item have RemarkForUser
 *
 * Date: 2013-08-12 [Yuen]
 * revised searching ACNO from table LIBMS_BOOK_UNIQUE
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);

// ###### END OF Trim all request #######
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ('User.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');

include_once ('User.php');
intranet_auth();

intranet_opendb();
$libms = new liblms();

$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);

$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['reserve'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    $error_msg = $Lang['libms']['CirculationManagement']['library_closed'];
    echo $error_msg;
    exit();
}

// $book_search = PHPToSQL($_REQUEST['book_search'],false,false);

// $book_search = "AT0000";
$ID = IntegerSafe($_REQUEST['ID']);

// $interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "RESERVE";

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];
$User = new User($uID, $libms);

$fields[] = 'b.BookID';
$fields[] = 'b.CallNum';
// $fields[] = 'bu.ACNO AS BookCode';
$fields[] = 'b.BookTitle';
$fields[] = 'b.Publisher';
$fields[] = 'b.ResponsibilityBy1';
$fields[] = 'IF(bu.RemarkToUser != NULL OR TRIM(bu.RemarkToUser) != "", bu.RemarkToUser, b.RemarkToUser) as RemarkToUser';
$fields[] = 'b.CirculationTypeCode';
$fields[] = 'b.CallNum2';
$fields[] = 'bu.AccompanyMaterial';
$fields[] = 'bu.BarCode';
$fields[] = 'b.BookSubTitle';

$book_search = (isset($_REQUEST['book_search']) && $_REQUEST['book_search'] != '') ? trim($_REQUEST['book_search']) : '';
$pendingBookID = IntegerSafe($_POST['pendingBookID']);

if (! get_magic_quotes_gpc()) {
    $book_search = stripslashes($book_search);
}

if ($book_search != "") {
    $unconverted_book_search_sql = mysql_real_escape_string(str_replace("\\", "\\\\", $book_search)); // A&<>'"\B ==> A&<>\'\"\\\\B
    $converted_book_search_sql = special_sql_str($book_search); // A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
} else {
    $unconverted_book_search_sql = '';
    $converted_book_search_sql = '';
}

$conds[] = "bu.ACNO LIKE '%{$unconverted_book_search_sql}%'";
$conds[] = "b.`BookTitle` LIKE '%{$unconverted_book_search_sql}%'";
$conds[] = "b.`CallNum` LIKE '%{$unconverted_book_search_sql}%'";
$conds[] = "b.`ResponsibilityBy1` LIKE '%{$unconverted_book_search_sql}%'";
$conds[] = "b.`ResponsibilityBy2` LIKE '%{$unconverted_book_search_sql}%'";
$conds[] = "b.`ResponsibilityBy3` LIKE '%{$unconverted_book_search_sql}%'";

if ($unconverted_book_search_sql != $converted_book_search_sql) { // keyword does not contain any special character: &<>"
    $conds[] = "bu.ACNO LIKE '%{$converted_book_search_sql}%'";
    $conds[] = "b.`BookTitle` LIKE '%{$converted_book_search_sql}%'";
    $conds[] = "b.`CallNum` LIKE '%{$converted_book_search_sql}%'";
    $conds[] = "b.`ResponsibilityBy1` LIKE '%{$converted_book_search_sql}%'";
    $conds[] = "b.`ResponsibilityBy2` LIKE '%{$converted_book_search_sql}%'";
    $conds[] = "b.`ResponsibilityBy3` LIKE '%{$converted_book_search_sql}%'";
}
$cond[] = implode(' OR ', $conds);

// $results = $libms->SELECTFROMTABLE('LIBMS_BOOK b',$fields,$cond,null,MYSQL_ASSOC, "b.BookTitle");

$sql = " SELECT DISTINCT " . implode(", ", $fields) . " " . " FROM " . "	LIBMS_BOOK AS b INNER JOIN LIBMS_BOOK_UNIQUE AS bu ON bu.BookID=b.BookID AND bu.RecordStatus NOT IN ('LOST','WRITEOFF','DELETE','SUSPECTEDMISSING')" . " WHERE " . implode(" OR ", $conds) . " ORDER BY b.BookTitle, b.BookSubTitle, bu.ACNO ";
$results = $libms->returnArray($sql, null, MYSQL_ASSOC);

if (! empty($results)) {
    $json_output['is_success'] = true;
    foreach ($results as $bookInfo) {
        
        foreach ($bookInfo as &$value) {
            if (is_null($value)) {
                $value = '';
            }
            if (get_magic_quotes_gpc()) {
                $value = str_replace("\\", "\\\\", $value);
            }
        }
        unset($value);
        
        $MSG = '';
        $bookID = $bookInfo['BookID'];
        
        $canReserve = $User->rightRuleManager->canReserve($bookID, $MSG, $pendingBookID);
        $reservedCount = $User->recordManager->getReservationCountByBookID($bookID, array(
            'WAITING',
            'READY'
        ));
        // $bookInfo = $BookManager->getBookFields($bookID);
        
        $limits = $User->rightRuleManager->get_limits($bookInfo['CirculationTypeCode']);
        $view_data = compact('bookInfo', 'canReserve', 'reservedCount', 'MSG');
        
        $bookInfo['canReserve'] = $canReserve;
        $bookInfo['reservedCount'] = (int) $reservedCount;
        $bookInfo['html_row'] = gen_reserve_html_row($view_data);
        $bookInfo['html_button'] = gen_force_reserve_button_html($view_data);
        $bookInfo['limit'] = $limits['LimitReserve'];
        
        $json_output['bookInfo'][] = $bookInfo;
    }
} else {
    $json_output['is_success'] = false;
}

echo json_encode($json_output);

function gen_force_reserve_button_html($view_data)
{
    global $Lang;
    
    extract($view_data);
    
    $x = '';
    $bookID = $bookInfo['BookID'];
    ob_start();
    ?>
<input name="submit2" type="button" class="set_borrow"
	value="<?=$Lang['libms']['CirculationManagement']['msg']['f_reserve']?>"
	onclick='allowreserve(this,<?=$bookID?>)' />
<?php
    $x = ob_get_contents();
    ob_end_clean();
    $x = str_replace("\n", '', $x);
    $x = str_replace("\t", '', $x);
    
    return $x;
}

function gen_reserve_html_row($view_data)
{
    global $Lang, $libms, $ID, $sys_custom;
    
    extract($view_data);
    
    $x = '';
    $bookID = $bookInfo['BookID'];
    $displayBookTitle = $bookInfo['BookTitle'];
    if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
        $displayBookTitle .= $bookInfo['BookSubTitle'] ? " :" . $bookInfo['BookSubTitle'] : "";
    }
    $displayBookTitle .= " [".$bookInfo['BarCode']."]";
    
    ob_start();
    
    ?>
<tr reserve cirtype='<?=$bookInfo['CirculationTypeCode']?>'
	<?=($canReserve)?'active':''?> id='<?=$bookID?>'
	class="<? if($canReserve) echo ' avaliable_row ';?> book_record ">
	<td><span class="row_id"><?=$ID?></span> <input type='hidden'
		value='<?=($canReserve)?$bookID:""?>' name='bookID[]' class='bookID' />
			<? $searchChar = '/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~ ]/'; ?>
			<div id="<?=preg_replace($searchChar,'-',$bookInfo['CallNum'])?>"
			class='reservelist' style='display: none'></div>
		<div id="<?=preg_replace($searchChar,'-',$bookInfo['BookCode'])?>"
			class='reservelist' style='display: none'></div></td>
	<td id="<?=$bookInfo['BarCode']?>"><?=$displayBookTitle?></td>
	<td nowrap="nowrap"><?=date('Y-m-d')?></td>
	<td nowrap="nowrap"><?=(int)$reservedCount?></td>
	<td><? if ($canReserve): ?>
			<span class="reserve_status alert_avaliable"><?=$Lang['libms']['CirculationManagement']['y_reserve']?></span>
		  <? else: ?>
			<span class="reserve_status alert_text"><?=$Lang['libms']['CirculationManagement']['n_reserve']?></span>(<?=$MSG?>)<br />
		  <? endif; ?>
		  
			<? if (!empty($bookInfo['RemarkToUser'])): ?>
		 		<br /> <span class="alert_text"><img
			src="/images/2009a/eLibplus/icon_alert.png" /><?=$bookInfo['RemarkToUser']?></span>
	 		<? endif; ?>
		  <? if ($libms->get_system_setting("circulation_display_accompany_material") && !empty($bookInfo['AccompanyMaterial'])): ?>
		 		<br /> <span class="alert_text"><img
			src="/images/2009a/eLibplus/icon_alert.png" /><?=$bookInfo['AccompanyMaterial']?></span>
		  <? endif; ?>
	  </td>
	<td>
		<div class="table_row_tool">
			<a href="#" class="delete" title="Delete"></a>
	 	<?
    // 2014-10-23 Yuen - no force reserve to avoid complicated issues....
    /*
     * if (!$canReserve):
     * echo gen_force_reserve_button_html($view_data);
     * endif;
     */
    ?>
	 </div>
	</td>
</tr>
<?
    $x = ob_get_contents();
    ob_end_clean();
    
    $x = str_replace("\n", '', $x);
    $x = str_replace("\t", '', $x);
    
    // $x = addslashes($x);
    return $x;
}
?> 
