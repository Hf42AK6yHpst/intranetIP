<?php
/*
 * This file should be run daily for sending the reminder push message to reader when the due date is TODAY
 * 
 * 2020-05-29 Cameron
 * - call getNotificationSettings() from libms to avoid including daily_update_reserved_status.php
 * - update NotifyMessageTargetID in borrow log so that it can be used to check if the book has been returned or not before the push message is to be sent out [case #F175877]
 *
 * 2016-11-17 [Cameron]
 * 		- fix bug: don't always set $sendPushMessage = true  [case #L108517]
 * 
 * Created by Kenneth
 */
$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($WRT_ROOT."includes/global.php");
include_once($WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($WRT_ROOT."includes/libdb.php");
include_once($WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");
include_once($WRT_ROOT."includes/libuser.php");
include_once($WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($WRT_ROOT."home/library_sys/management/circulation/daily_send_common_function.php");

$luser = new libuser();
$libeClassApp = new libeClassApp();
intranet_opendb();
$libms = new liblms();
echo "<br/>This file should be run daily for sending the reminder push message(s) to reader when the due date is TODAY<br/><br/>";
echo "Program started!<br/>";

$notifcation_settings	= array(
    'push_message_overdue' => $libms->getNotificationSettings('push_message_overdue')
	);

$sendPushMessage = false;
if (!$notifcation_settings['push_message_overdue']) {
	echo "The setting is not allow to send overdue push message!<br/>";
}
else {
	$sql = "SELECT id FROM LIBMS_DAILY_SCRIPT WHERE DATE(DateCreated) >= DATE(now()) AND name = 'push_message_overdue' limit 1";
	$result = $libms->returnArray($sql);
	
	if ($result[0]['id']) {
		echo "Overdue push message(s) is/are already sent!<br/>";
	}
	else {
		$sendPushMessage = true;
	}
}

if ($sendPushMessage) {
	$sql2 = "select distinct UserID from LIBMS_BORROW_LOG WHERE RecordStatus = 'BORROWED' AND DueDate = DATE_FORMAT(NOW(),'%Y-%m-%d')";
	$ReaderIDs = $libms->returnArray($sql2);
	
	$result2 = $libms->SELECTFROMTABLE('LIBMS_SYSTEM_SETTING', '*', array('name'=>'"push_message_time"'),'',1); // for Day(s) of the Email Reminder before Due Date (0 means not remind)
	$timeOfPushMessage = $result2[0]['value'];
	
	$messageTitle = $Lang['libms']['daily']['push_message']['overdue']['title'];
	$individualMessageInfoAry = array();
	for($i=0; $i < sizeof($ReaderIDs); $i++){
		//debug_pr($ReaderIDs);
		$ReaderID = $ReaderIDs[$i]['UserID'];
		
		$sql = "SELECT lb.BookTitle, lb.ResponsibilityBy1, lb.ResponsibilityBy2, lb.ResponsibilityBy3, lb.CallNum, lb.CallNum2, lbu.ACNO, lbl.DueDate, lbl.BorrowLogID FROM LIBMS_BORROW_LOG as lbl JOIN LIBMS_BOOK_UNIQUE as lbu ON lbl.UniqueID = lbu.UniqueID JOIN LIBMS_BOOK as lb ON lbl.BookID = lb.BookID WHERE lbl.RecordStatus = 'BORROWED' AND DueDate = DATE_FORMAT(NOW(),'%Y-%m-%d') AND UserID = '".$ReaderID."'";
		$result = $libms->returnArray($sql);
		
		$bookTitleArr = array();
		$borrowLogIDArr = array();

		for($j=0; $j < sizeof($result); $j++){
			$bookCount++;
			$bookTitleArr[$j] = $result[$j]['BookTitle'];
			$dueDateArr[$j] = $result[$j]['DueDate'];
			$borrowLogIDArr[$j] = $result[$j]['BorrowLogID'];
		}
		//sendOverScheduledPushMessage($ReaderID,$timeOfPushMessage,$bookTitleArr,$dueDateArr);
		//continue;
		$parentStudentAssoAry = getParentStudentAssoAry($ReaderID);
		if(empty($parentStudentAssoAry)){
			continue;
		}
		$messageArray['relatedUserIdAssoAry'] = $parentStudentAssoAry;
		
		$messageContent = getPushMessageContent($bookTitleArr,$dueDateArr);
		$messageArray['messageTitle'] = $messageTitle;
		$messageArray['messageContent'] = $messageContent;
		$messageArray['borrowLogIDArr'] = $borrowLogIDArr;
		//debug_pr($messageArray);
		$individualMessageInfoAry[] = $messageArray;
		
		//sendMultiplePushMessage($individualMessageInfoAry,$timeOfPushMessage);
	}
	//debug_pr($timeOfPushMessage);
	$forceUseClass = true;
	if(!empty($individualMessageInfoAry)){
		sendMultiplePushMessage($individualMessageInfoAry,$timeOfPushMessage);
	}
	$forceUseClass = false;
	$sql = "INSERT INTO LIBMS_DAILY_SCRIPT (name) Values ('push_message_overdue') ";
	$result = $libms->db_db_query($sql);
	
	
	echo "The push message is sent to ".sizeof($ReaderIDs)." reader(s)!<br/>";
	echo "Program ended!<br/>";
}
function sendMultiplePushMessage($individualMessageInfoAry,$timeOfPushMessage){
    global $Lang, $libms;
	$isPublic = "";
	$sendTimeMode = 'scheduled';
	$sendTimeString = date('Y-m-d').' '.$timeOfPushMessage.':00';
	$appType = 'P';
	$messageTitle = $Lang['libms']['daily']['push_message']['overdue']['title'];
	$messageContent = 'MULTIPLE MESSAGES';
	
	//Compare timeString with time now
	if((time() - strtotime($sendTimeString))>0){
		echo "Time set to push message is later than the time right now, Program End.";
	}else{
		$libeClassApp = new libeClassApp();
		$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eLibPlus_overdue', date('Ymd'));
		
		#####UPDATE INPUT DATE
		$sql = "UPDATE INTRANET_APP_NOTIFY_MESSAGE  Set DateInput  = '$sendTimeString'  Where NotifyMessageID = '$notifyMessageId'";
		$libdb = new libdb();
		$libdb->db_db_query($sql);

		// update NotifyMessageTargetID in borrow log so that it can be used to check if the book has been returned or not before the push message is to be sent out
		foreach((array)$individualMessageInfoAry as $_individualMessageInfoAry) {
		    $borrowLogIDArr = $_individualMessageInfoAry['borrowLogIDArr'];
		    $relatedUserIdAssoAry = $_individualMessageInfoAry['relatedUserIdAssoAry'];
		    $parentIDAry = array_keys($relatedUserIdAssoAry);
		    if (count($parentIDAry)) {
		        $sql = "SELECT NotifyMessageTargetID FROM INTRANET_APP_NOTIFY_MESSAGE_TARGET WHERE NotifyMessageID='".$notifyMessageId."' AND TargetID='".$parentIDAry[0]."'";
		        $rs = $libdb->returnResultSet($sql);

		        if (count($rs)) {
		            $notifyMessageTargetID = $rs[0]['NotifyMessageTargetID'];
		            $libms->updateOverdueNotifyMessageTargetID($borrowLogIDArr, $notifyMessageTargetID);
		        }
		    }

		}

		//$forceUseClass = false;
	}
}

function getPushMessageContent($bookTitleArr,$dueDateArr){
	global $Lang;
	$messageContent = $Lang['libms']['daily']['push_message']['overdue']['content'].'
			';
	
	$countOfBooks = count($bookTitleArr);
	for($i=0;$i<$countOfBooks;$i++){
		$messageContent .= '
				'.($i+1).'. '.$bookTitleArr[$i].' ('.$dueDateArr[$i].')';
	}
	return $messageContent;
}



?>