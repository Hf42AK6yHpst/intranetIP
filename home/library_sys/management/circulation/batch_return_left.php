<?php
/**
 * Log
 *
 * 20180222 (Cameron)
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 20170314 (Cameron)
 * - add IP checking for circulation
 *
 */
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");

if (strstr($_SERVER["SCRIPT_FILENAME"], "batch_return.php")) {
    $page_instruction_left = $Lang['libms']['CirculationManagement']['msg']['batch_return'];
} elseif (strstr($_SERVER["SCRIPT_FILENAME"], "batch_renew.php")) {
    $page_instruction_left = $Lang['libms']['CirculationManagement']['msg']['batch_renew'];
} else {
    $IsIndexPage = true;
    $page_instruction_left = $Lang['libms']['CirculationManagement']['msg']['circulation_index'];
}

// for new UI [start]
$libms = new liblms();
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation']) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (isset($_REQUEST['User_Barcode']) || isset($_REQUEST['StudentID'])) {
    
    $libms = new liblms();
    if (isset($_REQUEST['User_Barcode'])) {
        $result = $libms->SELECTFROMTABLE('LIBMS_USER', 'UserID', array(
            'BarCode' => PHPTOSQL($_REQUEST['User_Barcode']),
            'RecordStatus' => '1'
        ));
    } elseif (isset($_REQUEST['StudentID'])) {
        $result = $libms->SELECTFROMTABLE('LIBMS_USER', 'UserID', array(
            'UserID' => PHPTOSQL($_REQUEST['StudentID']),
            'RecordStatus' => '1'
        ));
    }
    
    if (! empty($result)) {
        $_SESSION['LIBMS']['CirculationManagemnt']['UID'] = $result[0][0];
        
        $result = $libms->SELECTFROMTABLE('LIBMS_GROUP_USER', 'GroupID', array(
            'UserID' => PHPTOSQL($_SESSION['LIBMS']['CirculationManagemnt']['UID'])
        ));
        if (empty($result)) {
            $error_msg = $Lang['libms']['CirculationManagement']['user_group_not_found'];
        } else {
            $right_cm = $c_right['circulation management'];
            if ($right_cm['borrow'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                header("Location: borrow.php");
                die();
            } else if ($right_cm['return'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                header("Location: return.php");
            else if ($right_cm['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                header("Location: renew.php");
            else if ($right_cm['reserve'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                header("Location: reserve.php");
            else if ($right_cm['overdue'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
                header("Location: overdue.php");
            else {
                include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
                $laccessright = new libaccessright();
                $laccessright->NO_ACCESS_RIGHT_REDIRECT();
                exit();
            }
        }
    } else {
        $error_msg = $Lang['libms']['CirculationManagement']['barcode_user_not_found'];
    }
}
$interface = new interface_html("libms_circulation_user_prompt.html");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
// Class List
$lc = new libclass();
$sql = "Select distinct ClassName, ClassName From LIBMS_USER WHERE ClassName IS NOT NULL AND ClassName <> '' ORDER BY ClassName";
$result = $libms->returnArray($sql);
$StudentClasses = $lc->getSelectClass("name=\"StudentClass\" id=\"StudentClass\" onChange=\"UpdateStudentList(this.value)\"", $readerClass, 0);

$SaveBtn = $interface->GET_ACTION_BTN($Lang["libms"]["report"]["Submit"], "submit", "", "SubmitBtn", "");
// $ResetBtn = $interface->GET_ACTION_BTN($button_reset, "reset", "", "CancelBtn", "");
// for new UI [end]
?>
<?if(!isset($_SESSION['LIBMS']['CirculationManagemnt']['UID']) && !$IsIndexPage && false){?>
<div class="user_info"
	style="position: absolute; top: 100px; left: 150px;">
	<table width="200" border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td nowrap="nowrap">
				<!--<div class="module_title_text"><span><?=$Lang['libms']['CirculationManagement']['normal_circulation']?></span></div>-->

				<form id='user_form' name='user_form'
					onSubmit="return checkFormBarcode(this)" action="borrow.php">
					<br />&nbsp; &nbsp; &nbsp; <?=$Lang['libms']['CirculationManagement']['user_barcode']?> :
						<br />&nbsp; &nbsp; &nbsp; <input style="width: 150px" type="text"
						id='input_code_text_barcode' name="User_Barcode" value="" /> <br />&nbsp; &nbsp; &nbsp; <?=$SaveBtn?>
						<?=$ResetBtn?>
					</form> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class='alert_text'><?=$error_msg?></span>
				<!--<?php if ($libms->get_system_setting("circulation_student_by_selection")) {?>
				</td><td nowrap="nowrap">
					<form id='student_form' name='student_form' >
						&nbsp; <?=$Lang["libms"]["CirculationManagement"]["student_only"]?> :
						<?= $StudentClasses ?>
						<span id="StudentListSpan">&nbsp;</span>
					</form>
			<?php } ?>-->
			</td>
		</tr>
			<?php if ($libms->get_system_setting("circulation_student_by_selection")) {?>
			<tr>
			<td nowrap="nowrap" colspan="2">
				<form id='student_form' name='student_form' action="borrow.php">
						&nbsp; &nbsp; &nbsp; <?=$Lang["libms"]["CirculationManagement"]["student_only"]?> :
						<br />&nbsp; &nbsp; &nbsp; <?= $StudentClasses ?>
						<br />&nbsp; &nbsp; &nbsp; <span id="StudentListSpan">&nbsp;</span>
				</form>
			</td>
		</tr>
			<?php } ?>
        </table>
</div>
<?} else {?>
<div class="user_info">

	<div class="user_title"><?=$page_instruction_left?>
	</div>
</div>
<?}?>