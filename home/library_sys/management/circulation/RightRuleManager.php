<?php
// using:   

/**
 * Log :
 *
 * Date 2018-09-21 [Cameron]
 * - add parameter $checkAvailableBookReservation to canReserve() to check if user is allowed reserving available books or not
 *
 * Date 2018-09-10 [Cameron]
 * - fix bug: add parameter $pendingBookID to canReserve() so that it can check the book reservation rule for circulation type code for book to be reserved
 *
 * Date 2018-09-08 [Cameron]
 * - fix bug: number of books reserved by circulation type (calling User->total_reserveded_book_count) 
 * 
 * Date 2018-09-06 [Cameron]
 * - change error message from $Lang['libms']['CirculationManagement']['msg']['user_reserved'] to $Lang["libms"]["CirculationManagement"]["msg"]["Hold_Book"] in canReserve()
 * 
 * Date 2018-02-14 [Cameron]
 * - add function isAllowClassManagementCirculation()
 * - modify canRenew()- check if it is allowed to borrow class management books or not [Case #E129637]
 *
 * Date 2018-02-13 [Cameron]
 * modify canBorrow() - check if not allow to borrow class management books or not [Case #E129637]
 *
 * Date 2017-11-14 [Henry]
 * modified canBorrow() to disallow borrow book which is damaged or shelving [Case#L130434]
 *
 * Date 2016-07-29 [Cameron]
 * fix bug: use explode to replace preg_split because it's faster
 * (first parameter should use pattern match format '/#/' instead of '#' if use preg_split)
 *
 * Date 2015-11-11 [Henry]
 * modified canRenew() to add para $renewDateTime
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$WRT_ROOT = "{$_SERVER['DOCUMENT_ROOT']}/";

include_once ($WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($WRT_ROOT . "includes/libdb.php");
include_once ($WRT_ROOT . "includes/pdump/pdump.php");

include_once ($WRT_ROOT . 'home/library_sys/management/circulation/BookManager.php');
include_once ($WRT_ROOT . 'home/library_sys/management/circulation/User.php');

class RightRuleManager
{

    // contructor of RightRuleManager
    var $User, $GID;

    var $Rules;

    var $Rights;

    var $libms;

    // CONSTRUCTOR
    public function __construct($User, $libms)
    {
        if (! empty($libms))
            $this->libms = $libms;
        else
            $this->libms = new liblms();
        
        if (! empty($User)) {
            $this->User = $User;
            // debug("inside");
        } else
            $this->User = new User(null, $this->libms);
        
        // dump("");
        $this->_init_group();
        $this->_init_rights();
        $this->_init_limits();
    }

    private function _init_group()
    {
        $uID = $this->User->userInfo['UserID'];
        if (! empty($uID)) {
            $sql = "SELECT `GroupID` FROM `LIBMS_GROUP_USER` WHERE `UserID`={$uID} LIMIT 1";
            $result = $this->libms->returnArray($sql);
            // dump($result);
            // if (!empty($result)){
            
            $this->GID = $result[0][0];
            // }
        }
        // dump($this->GID);
    }
 // _init_group
    private function _init_limits()
    {
        $this->get_limits('-1Def');
    }
 // _init_limits
    private function _init_rights()
    {
        $sql = "SELECT `Section`, `Function`, `IsAllow` FROM `LIBMS_GROUP_RIGHT` WHERE `GroupID`={$this->GID}";
        $result = $this->libms->returnArray($sql);
        if (! empty($result)) {
            foreach ($result as $row) {
                $this->Rights[$row['Section']][$row['Function']] = $row['IsAllow'];
            }
        }
    }
 // _init_rights
    public function get_limits($circulationTypeCode = null, $useCache = false)
    {
        // if null get default rule
        // tolog($circulationTypeCode);
        if (empty($circulationTypeCode)) {
            $circulationTypeCode = '-1Def';
        }
        // check and return cached data
        // if ($useCache && !empty($this->Rules[$circulationTypeCode])){
        // return $this->Rules[$circulationTypeCode];
        // }
        
        $sql = "SELECT `LimitBorrow`, `LimitRenew`, `LimitReserve`,`ReturnDuration`,`OverDueCharge`,`MaxFine` FROM `LIBMS_GROUP_CIRCULATION` WHERE `GroupID` = '{$this->GID}' AND `CirculationTypeCode`='{$circulationTypeCode}'";
        $result = $this->libms->returnArray($sql);
        // tolog($result);
        // if no match, duplicate and return Default limit
        if (empty($result)) {
            if ($circulationTypeCode != '-1Def') {
                $result = $this->get_limits();
            } else {
                // trigger_error("DB data error, no default setting found. {$sql}\n");
                if (strstr($_SERVER['SCRIPT_NAME'], "/management/circulation/borrow.php")) {
                    echo "<script>var NoRuleNoGroup = true;</script>\n";
                }
            }
        } else {
            $result = $result[0];
        }
        
        $this->Rules[$circulationTypeCode]['LimitBorrow'] = $result['LimitBorrow'];
        $this->Rules[$circulationTypeCode]['LimitRenew'] = $result['LimitRenew'];
        $this->Rules[$circulationTypeCode]['LimitReserve'] = $result['LimitReserve'];
        $this->Rules[$circulationTypeCode]['ReturnDuration'] = $result['ReturnDuration'];
        $this->Rules[$circulationTypeCode]['OverDueCharge'] = $result['OverDueCharge'];
        if ($result['MaxFine'] == - 1 || trim($result['MaxFine']) == "" || $result['MaxFine'] == 0) {
            $this->Rules[$circulationTypeCode]['MaxFine'] = 99999;
        } else {
            $this->Rules[$circulationTypeCode]['MaxFine'] = $result['MaxFine'];
        }
        // tolog($this->Rules);
        return $this->Rules[$circulationTypeCode];
    }
 // get_limits
    public function canBorrow($uniqueBookID, &$MSG = null)
    {
        global $Lang, $sys_custom;
        $BookManager = new BookManager($this->libms);
        $circulationTypeCode = $BookManager->getCirculationTypeCodeFromUniqueBookID($uniqueBookID);
        $bookID = $BookManager->getBookIDFromUniqueBookID($uniqueBookID);
        // Bug must be fix (Reserved book can not be borrowed by the user)
        // if($this->User->userInfo["UserID"] == "3014")
        // debug_pr("abc");
        // if ($this->User->reservedBooks)
        // debug_pr("abc");
        // check if the book should be borrowed out
        $uniqueBookRecordStatus = $BookManager->getUniqueBookRecordStatus($uniqueBookID);
        // Henry Added
        // $tempBookID = $BookManager->getBookIDFromUniqueBookID($uniqueBookID);
        // $tempBookRecordStatus = $BookManager->getUniqueBookRecordStatusByBookID($tempBookID);
        // $isReserved = false;
        // foreach($tempBookRecordStatus as $recordStatus){
        // if($recordStatus == 'NORMAL'){
        // $isReserved = false;
        // break;
        // }
        // else if($recordStatus == 'RESERVED')
        // $isReserved = true;
        // }
        // Henry Added
        // $tempUser = new User($this->User->userInfo["UserID"],$this->libms);
        // foreach($tempUser->borrowedBooks as $aBook){
        // if($aBook['UniqueID'] == $uniqueBookID){
        // $MSG = "You have borrowed the copy of the book!";
        // return false;
        // }
        // }

        if (/*$isReserved == true ||*/ $uniqueBookRecordStatus == 'RESERVED') {
            $pass = false;
            $tempUser = new User($this->User->userInfo["UserID"], $this->libms);
            // if (!empty($this->User->reservedBooks)){ //Henry Modified
            if (! empty($tempUser->reservedBooks)) {
                foreach ($tempUser->reservedBooks as $reservedBook) {
                    if ($reservedBook['RecordStatus'] == 'READY' && $reservedBook['BookID'] == $bookID)
                        $pass = true;
                }
            }
            
            if (! $pass) {
                $MSG = $Lang['libms']['CirculationManagement']['msg']['reserved'];
                return false;
            }
        } elseif ($uniqueBookRecordStatus == "BORROWED") {
            $recordManager = new RecordManager($this->libms);
            $borrowed_by_user = $recordManager->getBorrowerByUniqueID($uniqueBookID);
            if ($sys_custom['eLibraryPlus']['hideUserBarcode']) {
                // $no_barcode_user = preg_split("#",$borrowed_by_user);
                $no_barcode_user = explode("#", $borrowed_by_user);
                $borrowed_by_user = $no_barcode_user[0];
            }
            $MSG = str_replace("[user_name]", $borrowed_by_user, $Lang["libms"]["CirculationManagement"]["msg"]["already_borrowed"]);
            return false;
        } elseif ($uniqueBookRecordStatus != 'NORMAL') {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['cannot_borrow_book'] . " - " . $Lang['libms']['book_status'][$uniqueBookRecordStatus];
            return false;
        } elseif (! empty($this->User->borrowedBooks) && (($max_overdue_limit = $this->libms->get_system_setting('max_overdue_limit')) >= 0)) {
            $oldest_duedate = PHP_INT_MAX;
            $oldest_book = null;
            foreach ($this->User->borrowedBooks as $book) {
                $this_book_duedate = strtotime($book['DueDate']);
                if ($this_book_duedate < $oldest_duedate) {
                    $oldest_duedate = $this_book_duedate;
                }
            }
            
            $tm = new TimeManager();
            $dayForPenalty = $tm->dayForPenalty($oldest_duedate, time());
            
            if ($dayForPenalty > $max_overdue_limit) {
                $MSG = $Lang['libms']['CirculationManagement']['msg']['overdue_limit'];
                return false;
            }
        }
        
        $book = $BookManager->getBookFields($bookID, array(
            'OpenBorrow',
            'LocationCode'
        ), $uniqueBookID);
        
        // $bookRecordStatus = $book['RecordStatus'];
        // if ($bookRecordStatus != 'ACTIVE' AND $bookRecordStatus != 'NORMAL'){
        // $MSG = $Lang['libms']['CirculationManagement']['msg']['cannot_borrow'];
        // return false;
        // }
        
        if (empty($book['OpenBorrow'])) {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['cannot_borrow'];
            return false;
        }
        
        // check if the user is over limit
        // Global limit
        $limits = $this->get_limits();
        if ($this->User->total_borrowed_book_count() >= $limits['LimitBorrow']) {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['over_borrow_limit'];
            return false;
        }
        if (! empty($circulationTypeCode)) {
            $limits = $this->get_limits($circulationTypeCode);
            if ($limits['LimitBorrow'] == 0) {
                $MSG = $Lang['libms']['CirculationManagement']['msg']['zero_borrow_limit'];
                return false;
            }
            if ($this->User->total_borrowed_book_count($circulationTypeCode) >= $limits['LimitBorrow']) {
                $MSG = $Lang['libms']['CirculationManagement']['msg']['over_borrow_cat_limit'];
                return false;
            }
        }
        
        // check if it's allowed to borrow class management books or not
        if (! $this->isAllowClassManagementCirculation($book['LocationCode'], $circulationAction = 'borrow')) {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['notAllowBorrowReturnClassManagementBooks'];
            return false;
        }
        
        return true;
    }

    public function canReserveItem($CirculationTypeCode)
    {
        $limits = $this->get_limits($CirculationTypeCode);
        // dump($limits);
        if ($limits['LimitReserve'] == 0) {
            // not allowed
            return false;
        }
        
        return true;
    }

    public function canReserve($bookID, &$MSG = null, $pendingBookID='', $checkAvailableBookReservation=false)
    {
        global $Lang, $eLib_plus;
        $BookManager = new BookManager($this->libms);
        
        // Check the book is reserved by own (Henry)
        $tempUser = new User($this->User->userInfo["UserID"], $this->libms);
        if (! empty($tempUser->reservedBooks)) {
            foreach ($tempUser->reservedBooks as $reservedBook) {
                if ($reservedBook['BookID'] == $bookID) {
                    $MSG = $Lang["libms"]["CirculationManagement"]["msg"]["Hold_Book"];
                    // $MSG = $Lang['libms']['CirculationManagement']['msg']['user_reserved'];
                    return false;
                }
            }
        }
        
        // check if the book should be borrowed out
        
        $book = $BookManager->getBookFields($bookID, array(
            'CirculationTypeCode',
            'OpenReservation',
            'NoOfCopyAvailable'
        ));
        // //error_log('RRM 141: $book' . var_export($book,TRUE));
        // $bookRecordStatus = $book['RecordStatus'];
        // if ($bookRecordStatus != 'ACTIVE' AND $bookRecordStatus != 'NORMAL' ){
        // $MSG = $Lang['libms']['CirculationManagement']['msg']['cannot_reserve'];
        // return false;
        // }
        
        if (empty($book['OpenReservation'])) {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['cannot_reserve'];
            return false;
        }
        
        // $circulationTypeCode = (trim($book['Item_CirculationTypeCode'])<>"") ? $bookInfo['Item_CirculationTypeCode'] : $bookInfo['CirculationTypeCode'];
        $BookCirculationTypeCode = $book['CirculationTypeCode'];
        
        // check if the user is over limit
        // Global limit
        $limits = $this->get_limits();
        
        if ($this->User->total_reserveded_book_count() >= $limits['LimitReserve']) {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['over_reserve_limit'];
            return false;
        }
        
        // check if user is allowed reserving available books or not
        if ($checkAvailableBookReservation) {
            $allow_reserve_available_books_in_portal = $this->libms->get_system_setting('allow_reserve_available_books_in_portal');
            if (!$allow_reserve_available_books_in_portal) {
                $noOfCopyAvailable = $book['NoOfCopyAvailable'];
                $noOfReservedBooks = $this->libms->getBookReservedCount($bookID);
                if ($noOfCopyAvailable - $noOfReservedBooks > 0) {
                    $MSG = $eLib_plus["html"]["cannothold_book_available"];
                    return false;
                }
            }
        }
        
        // ToDo :: check all circulation type!!!
        
        // get CirculationTypeCodes from all item if set
        $ItemSpecialCirculationTypeArr = $BookManager->getAllItemCirculationTypeCode($bookID);
        
        // check if the user is over limit
        // CirculationType limit
        if (sizeof($ItemSpecialCirculationTypeArr) <= 0) {
            // no items for loan! and so no reservation allowed!
            $MSG = $Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve_no_copy"];
            return false;
        } else {
            
            if (count($pendingBookID)) {
                $circulationCodeAssoc = $BookManager->getNumberOfBooksByCirculationTypeCode($pendingBookID);
            }
            else {
                $circulationCodeAssoc = array();
            }
            
            // check all explicitly
            $AllowReserveCount = 0;
            for ($i = 0; $i < sizeof($ItemSpecialCirculationTypeArr); $i ++) {
                $ItemObj = $ItemSpecialCirculationTypeArr[$i];
                $ItemCirculationTypeCode = (! empty($ItemObj["CirculationTypeCode"])) ? $ItemObj["CirculationTypeCode"] : $BookCirculationTypeCode;
                
                $limits = $this->get_limits($ItemCirculationTypeCode);
                // dump($limits);
                
                $totalReservationByCode = $this->User->total_reserveded_book_count($ItemCirculationTypeCode);
                
                if ($circulationCodeAssoc[$ItemCirculationTypeCode]) {
                    $totalReservationByCode += $circulationCodeAssoc[$ItemCirculationTypeCode];
                }
                
                if ($limits['LimitReserve'] == 0) {
                    // not allowed
                } elseif ($totalReservationByCode >= $limits['LimitReserve']) {
                    $MSG = $Lang["libms"]["CirculationManagement"]["msg"]["exceeds_reserve"];
                } else {
                    $AllowReserveCount ++;
                }
            }
        }
        
        if ($AllowReserveCount > 0) {
            return true;
        } else {
            if ($MSG == "") {
                $MSG = $Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve_no_copy"];
            }
            return false;
        }
    }

    public function canRenew($uniqueBookID, &$MSG = null, $renewDateTime = '')
    {
        global $Lang;
        $BookManager = new BookManager($this->libms);
        $recordManager = new RecordManager($this->libms);
        
        $circulationTypeCode = $BookManager->getCirculationTypeCodeFromUniqueBookID($uniqueBookID);
        $bookID = $BookManager->getBookIDFromUniqueBookID($uniqueBookID);
        // check if there are reserves
        // $reservedRecord = $recordManager->getReservationByBookID($bookID,'RESERVED');
        // Fixed bug on renew a reserved book (Henry)
        // $reservedRecord = $recordManager->getReservationByBookID($bookID,'READY');
        // if (!empty($reservedRecord)){
        // $MSG = $Lang['libms']['CirculationManagement']['msg']['reserved'];
        // return false;
        // }
        $reservedRecord = $recordManager->getReservationByBookID($bookID, 'WAITING');
        if (! empty($reservedRecord)) {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['reserved'];
            return false;
        }
        
        $borrowID = $recordManager->getBorrowLogIDFromBookUniqueID($uniqueBookID, 'BORROWED');
        $borrowInfo = $recordManager->getFieldsFromBorrowLog($borrowID);
        // $bookRecordStatus = $BookManager->getBookRecordStatus($bookID);
        // if ($bookRecordStatus != 'ACTIVE' AND $bookRecordStatus != 'NORMAL'){
        // $MSG = $Lang['libms']['CirculationManagement']['msg']['cannot_borrow'];
        // return false;
        // }
        // ##################
        // //check if the user is over limit
        $renewCount = $borrowInfo['RenewalTime'];
        $limit = $this->get_limits($circulationTypeCode);
        // tolog($limit);
        if (! empty($limit)) {
            if ($limit['LimitRenew'] == 0) {
                $MSG = $Lang['libms']['CirculationManagement']['msg']['zero_renew_limit'];
                return false;
            }
            if ($renewCount >= $limit['LimitRenew']) {
                $MSG = $Lang['libms']['CirculationManagement']['msg']['over_renew_cat_limit'];
                return false;
            }
        }
        
        // check if over due happens
        if ($borrowInfo["DueDate"] != "" && ($renewDateTime ? date("Y-m-d", $renewDateTime) : date("Y-m-d")) > $borrowInfo["DueDate"]) {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['renew_but_overdue'];
            return false;
        }
        
        $book = $BookManager->getBookFields($bookID, array(
            'LocationCode'
        ), $uniqueBookID);
        
        // check if it's allowed to renew class management books or not
        if (! $this->isAllowClassManagementCirculation($book['LocationCode'], $circulationAction = 'renew')) {
            $MSG = $Lang['libms']['CirculationManagement']['msg']['notAllowBorrowReturnClassManagementBooks'];
            return false;
        }
        
        // if all passed
        return true;
    }

    /**
     * Get same day return is enabled for Group *
     */
    function isSameDayReturnNotAllowed($uID)
    {
        $sql = "SELECT gu.GroupID,  gp.SameDayReturnNotAllowed
					FROM  LIBMS_GROUP_USER gu
					INNER JOIN  LIBMS_GROUP_PERIOD gp on gp.GroupID = gu.GroupID 
					WHERE gu.UserID={$uID} LIMIT 1";
        $result = $this->libms->returnArray($sql);
        return $result[0]['SameDayReturnNotAllowed'] ? true : false;
    }

    /**
     * purpose: check if it is allowed to borrow/return class management books or not
     * Following user return true: (is allowed)
     * 1. super admin (eLib+ administrator)
     * 2. class management user
     * 3. user that's not specifically deny class management circulation
     *
     * @param
     *            $bookLocationCode
     * @param $circulationAction (borrow/return/renew)            
     * @return boolean
     */
    public function isAllowClassManagementCirculation($bookLocationCode, $circulationAction = 'borrow')
    {
        if ($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
            return true;
        } else {
            if ($_SESSION['LIBMS']['admin']['class_mgt_right']['circulation management'][$circulationAction]) {
                if ($bookLocationCode != '') {
                    $isLocationCodeInClassManagementGroup = $this->libms->isLocationCodeInClassManagementGroup($bookLocationCode, $isClassManagementUser = true);
                    if ($isLocationCodeInClassManagementGroup) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                if ($_SESSION['LIBMS']['admin']['current_right']['circulation management']['not_allow_class_management_circulation']) { // this var is set in liblms.get_current_user_right()
                    if ($bookLocationCode != '') {
                        $isLocationCodeInClassManagementGroup = $this->libms->isLocationCodeInClassManagementGroup($bookLocationCode, $isClassManagementUser = false);
                        if ($isLocationCodeInClassManagementGroup) {
                            return false;
                        }
                    }
                }
            }
        }
        
        return true; // default
    }
}

?>