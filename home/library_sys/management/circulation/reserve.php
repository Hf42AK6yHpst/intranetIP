<?php
// Using 
// ######################################################################################
// # Change Log
// ######################################################################################
/*
 *
 * 20191011 (Cameron)
 * - use large font for common_table_list if $sys_custom['eLibraryPlus']['Circulation']['LargeFont'] is set [case #K155570]
 * 
 * 20190328 (Cameron)
 * - show BookSubTitle in json_result_handler() if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * 
 * 20180910 (Cameron)
 * - fix: pass pendingBookID to reserve book checking (reserve_add_book.click event) to check current list against book reservation rule
 *
 * 20180222 (Cameron)
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 20171127 Cameron
 * - set barcode search bar to left position if $sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] == true [case #F129893]
 * - unify code for ej change on 20160713
 *
 * 20170407 Cameron
 * - change ajax method to post when click reserve_add_book so that it accepts special characters like <> in BookTitle
 *
 * 20170314 Cameron
 * - add IP checking for circulation
 *
 * 20160729 (Cameron)
 * - append timestamp to jquery.alerts.js so that it can refresh to take effect
 *
 * 20160713 (Cameron)
 * - change js include file to script.utf8.* to support Chinese in jquery.alerts plugin [ej only]
 *
 * 20151201 Cameron
 * - add function updateRowNo() - reorder row number after removing row(s) but before confirmation
 * - modify append_a_reserve_row() to avoid adding duplicate record in the list
 * - fix bug on not allow reserve books if book reservation limit is reached ( modify allow_Reserve_switchoff())
 *
 * 20150610 Henry:
 * - apply class management permission
 *
 * 20150318 Cameron:
 * - Modify function allowreserve() to use password prompt rather than plain text
 * - Add function authenticate()
 *
 * 20141201 Ryan : Add Suspended User Checking
 *
 * #####################################################################################
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
// include_once("libms_interface.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

$interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "RESERVE";
intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['reserve'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}

$is_open_loan = $libms->IsSystemOpenForLoan($_SESSION['LIBMS']['CirculationManagemnt']['UID']);

if (! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID']))
    header("Location: index.php");

include_once ("User.php");
$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];

intranet_opendb();

// Check User is Suspended
$isSuspended = $libms->SELECTFROMTABLE('LIBMS_USER', 'Suspended', array(
    'UserID' => PHPTOSQL($_SESSION['LIBMS']['CirculationManagemnt']['UID'])
));
if (is_null($isSuspended[0]['Suspended'])) {
    $User = new User($uID, $libms);
    $limits = $User->rightRuleManager->get_limits();
    
    $reserve_quota_left = $limits['LimitReserve'] - count($User->reservedBooks);
    $xmsg = $_SESSION['LIBMS']['xmsg'];
    unset($_SESSION['LIBMS']['xmsg']);
}

$interface->LAYOUT_START();

if ($sys_custom['eLibraryPlus']['Circulation']['LargeFont']) {
    echo '<link href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/elibplus_large_font.css" rel="stylesheet" type="text/css">'."\n";
}
?>
<link href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.css"
	rel="stylesheet" type="text/css">
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>lang/script<?=$junior_mck?'.utf8':''?>.<?=$intranet_session_language?>.js"></script>
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.js?<?=time()?>"></script>

<script type="text/javascript">
var ajaxReturn=null;
var overlimit;
var cached_password=false;
var password_match=false;

function authenticate(caller, uniqueBookID) {
	if( cached_password == '' || cached_password == null){
		cached_password = false;
		return false;
	}		
	$.get(	'override_ajax.php',
			{ override_password : cached_password	},
			function(data){
				if(data !='1'){
					cached_password = false;
					jAlert("<?=$Lang["libms"]["Circulation"]["WrongPassword"]?>", "<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_reserve"]?>",function(){return allowreserve(caller, uniqueBookID);});
				}else{
					password_match=true;
					allow_Reserve_switchon(caller,uniqueBookID);
				}
				return;
			}
	);
}

function allowreserve(caller, uniqueBookID){
	<?
$tmp_override_password = $libms->get_system_setting('override_password');
if (! empty($tmp_override_password)) :
    ?>
			if( !cached_password  && !password_match ){
				jPromptPassword( "<?=$Lang["libms"]["Circulation"]["PleaseEnterPassword"]?>:", "", "<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_reserve"]?>",function(r) {
					if (r) {
						cached_password = r;
						authenticate(caller, uniqueBookID);
					}	// get input password 					
				});
			}
			authenticate(caller, uniqueBookID);
	<? else: ?>
		allow_Burrow_switchon(caller,uniqueBookID);
	<? endif; ?>
		
}

function allow_Reserve_switchon(caller, uniqueBookID){
	THIS=$(caller);
	THIS_ROW=THIS.parents('tr.book_record:first');
	THIS_ROW.find('.reserve_status').html("<?=$Lang['libms']['CirculationManagement']['msg']['force_reserve'] ?>");
	THIS_ROW.prop('active',true).addClass('avaliable_row').find('.bookID').val(uniqueBookID);
	THIS.remove();
	updateTotal();
}

function updateTotal(){
	total = $('#reserve_table tr.avaliable_row').length;
	$('em#reserve_Count').html(total);
	if (total >= <?=$reserve_quota_left?>)
		$('span#overLimit').show();
	else
		$('span#overLimit').hide();
}


function allow_Reserve_switchoff($row,button_html,reason){
	//THIS_ROW=$row;
		$row.find('.reserve_status').replaceWith('<span class="reserve_status alert_text"><?=$Lang['libms']['CirculationManagement']['n_reserve']?> </span><br />'+reason);
		$row.removeProp('active').removeClass('avaliable_row').find('.bookID').val('');
		$row.find('.table_row_tool').append(button_html);
		//THIS.remove();
		updateTotal();	
}



function append_a_reserve_row(bookInfo){
	if ($('td#'+bookInfo.BarCode).length ==0){
		$row = $(bookInfo.html_row);
	
		table = $('#reserve_table');
		table.append($row);
		table.find('a.delete:last').click(function(){
			$(this).parents('tr:first').remove();
			updateRowNo();
			updateTotal();
		});	
	
		cirtype = bookInfo.CirculationTypeCode;
		//if ( $('tr[reserve][cirtype='+cirtype+'][active]').length > bookInfo.limit ){
		//	allow_Reserve_switchoff($row, bookInfo.html_button,'<?=$Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_cat_limit"]?>');
		//}
		total = $('#reserve_table tr.avaliable_row').length;
		if (total > <?=$reserve_quota_left?>){
			allow_Reserve_switchoff($row, bookInfo.html_button,'<?=$Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_limit"]?>');
		}
	
		updateTotal();
	}
	else {
		alert("<?=$Lang["libms"]["CirculationManagement"]["msg"]["inlist"]?>");
	}
}


function json_result_handler(ajax_return_obj){
	var bookTitle;	
	$modal = $(this);
	ajaxReturn = ajax_return_obj; 
    
	if (ajax_return_obj != null && ajax_return_obj.is_success){
		if (ajax_return_obj.bookInfo.length ==1){
			append_a_reserve_row(ajaxReturn.bookInfo[0]);
		}else{
			$popup_div = $('<div title="<?=$Lang['libms']['CirculationManagement']['msg']['please_pick_a_book']?>"/>');
			$table = $('<table  />');
			$table.append('<thead><tr style="height: 30px; font-size:13pt;"><td><?=$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle']?></th>'
							+'<th><?=$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author']?></th>'
							+'<th><?=$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['CallNum']?></th>'
							+'<th></th></tr></thead>');
			
			$.each(ajax_return_obj.bookInfo, function (i,v){
				bookTitle = v.BookTitle;
			<?php if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']):?>
				if (v.BookSubTitle != '') {
					bookTitle += ' :' + v.BookSubTitle;  
				}
			<?php endif;?>
				$row_div = $('<tr style="height: 26px;"/>');
				$row_div.append('<td>'+bookTitle+'</td>'+'<td>'+v.ResponsibilityBy1+'</td>'+'<td>'+v.CallNum +' ' + v.CallNum2 +'</td>'+'<td><a class="formbutton" href="#"><span>&nbsp; <?=$Lang["libms"]["CirculationManagement"]["add"]?> &nbsp;</span></a></td>');
				$table.append($row_div);
				$row_div.find('a').click(function(){append_a_reserve_row(ajaxReturn.bookInfo[i]); $popup_div.dialog("close"); });
				//append_a_reserve_row(this.html_row);
			});
			$table.find('tr:even').css("background-color", "#EEEEFF");
			$table.find('thead tr').css("background-color", "#FFEEDD");
			$popup_div.append($table);
			$popup_div.dialog({
				width : 900,
				height: 500,
				modal : true,
				close: function() {$popup_div.remove();}
			});
		}
	}else{
		alert('<?=$Lang['libms']['CirculationManagement']['msg']['n_callno_acno']?>');
	}

}

$().ready( function(){
	$.ajaxSetup({ cache: false});
	
	$(window).on('beforeunload', function(){
		if ($('#reserve_table tr.avaliable_row').length > 0)
			return '<?=$Lang['libms']['CirculationManagement']['msg']['comfirm_leave']?>';
	});
	
	$('input#reserve_submit').click(function(){
		$(window).off('beforeunload');
	});
	
	$('input#reserve_add_book').click( function(){
		searchcode = $('input#book_search').val();
		searchcodemod = searchcode.replace(/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~ ]/g,'-');

		if ( searchcode.length >=2){
			if ($('div.reservelist#'+searchcodemod).length == 0){
				var pendingBookID = []; 
				$('input.bookID').each(function(){
					pendingBookID.push($(this).val());
				});
					 
				$.ajax({
					dataType: "json",
					type: "POST",								
					url: 'reserve_ajax_add.php',
					data : {
								book_search : searchcode,
								pendingBookID : pendingBookID,
								ID : $('#reserve_table tr').length
						   },			
					success: json_result_handler,
					error: function(){ alert('Error on AJAX call!'); }
				});

				$('#book_search').val('');
				$('#book_search').focus();
				
				
			}else{
				alert("<?=$Lang['libms']['CirculationManagement']['msg']['scanned']?>");
				
			}
		}else{
			alert("<?=$Lang['libms']['CirculationManagement']['msg']['searching_string_too_short']?>");
		}
		return false;
	});
	
	$('input#book_search').keyup(function(event) {
	  if (event.which == 13) {
		$('input#reserve_add_book').click();
	  }  
	});
	
	$('#reset').click(function(){
		$('#reserve_table tr').not('tr:first').remove();
		updateTotal();
	});
	
	$('#book_search').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});
	$('#book_search').focus();

	
	$(document).bind('keydown',function(e){
			
			switch (e.keyCode)
			{
			    case 120:	// F9
					$('input#reserve_submit').click();
					return false;
			    default:
			      return true;
			}
  
  
		});
});


function checkform(frmObj)
{
	if ($('#reserve_table tr.avaliable_row').length<=0)
	{
		alert("<?=$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed']?>");
		$('#book_search').focus();
		return false;
	}
}

function updateRowNo() {
	var i = 1;
	$('#reserve_table tr span.row_id').each(function(){
		$(this).html(i++);
	});
}
</script>

<?php

if (! $is_open_loan) {
    echo "<div style='margin: 5em;'><font size='3'>" . $libms->CloseMessage . "</font></div>";
} elseif ($isSuspended[0]['Suspended']) {
    // Suspended = 1
    echo "<div style='margin: 5em;'><font size='3' color='red'><strong>" . $Lang["libms"]["UserSuspend"]["Suspended"] . "</font></strong></div>";
} else {
    ?>
<? if(!empty($xmsg)): ?>
<center>
	<div align="center" style='width: 1%'>
		<table border="0" cellpadding="3" cellspacing="0" class="systemmsg">
			<tr>
				<td nowrap="nowrap"><font color="green"><?=$xmsg?></font></td>
			</tr>
		</table>
	</div>
</center>
<?endif;?>

<!-------------------------------------------------------------------->
<form name="borrow_form" action='reserve_action.php'
	onsubmit="return checkform(this)">
	<div class="table_board">
		<div class="enter_code"
			<?=$sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] ? 'style="float:left;"':''?>>
			<span id='overLimit'
				<? if ($reserve_quota_left > 0 ) echo 'style="display :none;"' ?>
				class="alert_text"><?=$Lang['libms']['CirculationManagement']['msg']['over_reserve_limit']?><br /></span>
		<?=$Lang["libms"]["CirculationManagement"]['reserve_page']["searchby"]?>: <input
				name="" type="text" id='book_search' class="input_code_text" /> <input
				name="submit2" type="button" id='reserve_add_book'
				class="formbutton"
				value="&crarr; <?=$Lang["libms"]["btn"]["enter"]?>" />
		</div>

		<table id='reserve_table' class="common_table_list">
			<col nowrap="nowrap" />
			<tr>
				<th class="num_check">#</th>
				<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></a></th>
				<th><?=$Lang['libms']['CirculationManagement']['reserveday']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['ppl_reserved']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['status']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['action']?></th>
			</tr>

		</table>
	</div>
	<div class="op_btn">
		<div class="op_num">
			<span><?=$Lang['libms']['CirculationManagement']['no_reserve']?> :</span><em
				id='reserve_Count'>0</em>
		</div>
		<div class="form_btn">
			<input name="submit2" id='reserve_submit' type="submit"
				class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['confirm'].$Lang["libms"]["CirculationHotKeys"]["Confirm"]?>" />
			<input name="submit2" id='reset' type="reset" class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['cancel'].$Lang["libms"]["CirculationHotKeys"]["Cancel"]?>" />
		</div>
	</div>
</form>

<?
}

$interface->LAYOUT_STOP();
