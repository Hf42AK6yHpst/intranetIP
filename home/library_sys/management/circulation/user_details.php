<?php
//Using :  
######################################################################################
#									Change Log 
######################################################################################
#
#   20190328 (Cameron): show BookSubTitle in overdue, reserved and penalty sections if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
#
# 	20170314 Cameron: add IP checking for circulation
# 
#	20151223 Cameron: fix bug on showing overdue type in penalty session
#
#	20151111 Cameron: show column "borrow_accompany_material" if circulation_borrow_return_accompany_material is true
#
#	20150609 Henry: apply class management permission
#
#	20141211 Ryan : Missing "$" in Fine Column 
#
######################################################################################


if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

//error_reporting( E_ALL );

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
global $Lang;
include_once("BookManager.php");
include_once("User.php");
include_once("RecordManager.php");
include_once("RightRuleManager.php");
intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right,$c_m_right);
$global_lockout = (BOOL)$libms->get_system_setting('global_lockout');

if (!$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && (!isset($c_right['circulation management']) || $global_lockout)))
{
	//dump($c_right);
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

//if(! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID']))
//	header("Location: index.php");

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];

$User = new User($uID, $libms);
$BookManager = new BookManager($libms);


$userInfo = $User->userInfo;

$borrowCount = 0;
$overdueCount = 0;

if (!empty ($User->borrowedBooks))
	foreach ( $User->borrowedBooks as $book){
		$borrowCount++;
		if ((time() - $book['DueDate']) > 60*60*24)
			$overdueCount++;
		//$borrowbooks[] = $BookManager->getBookFields($book['BookID']);
	}


$revservedCount = count($User->reservedBooks);
//$limits=$User->rightRuleManager->get_limits();
$recordManager=new RecordManager($libms);

$cond[] = "ol.`RecordStatus` IN ('OUTSTANDING')";
$overdues = $recordManager->getOverDueRecords($userInfo['UserID'],$cond);

$xmsg = $_SESSION['LIBMS']['xmsg'];
unset($_SESSION['LIBMS']['xmsg']);

?>
<? if(!empty($xmsg)): ?>
<center>
<div align="center" style='width: 1%'>
	<table border="0" cellpadding="3" cellspacing="0" class="systemmsg">
		<tr>
			<td nowrap="nowrap"><font color="green"><?=$xmsg?></font></td>
		</tr>
	</table>
</div>
</center>
<?endif;?>

<script type='text/javascript'>

var IsLoaded = false;

<?if(isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])){?>
function jsShowDetails(isCurrent){
	if (isCurrent)
	{
		$('#all_loans').slideUp(250);
		
		$('#current_loan').slideDown(250);
		$('#current_reservation').slideDown(250);
		$('#current_penal').slideDown(250);
		
		$('#tag_current').attr('class', 'page_tab_on');
		$('#tag_all').attr('class', 'page_tab_off');
		
	} else
	{
		
		if (!IsLoaded)
		{
			$.ajax({
				type: "POST",
			    url: 'reader_loans_history.php',
			    data: { user_id: <?=$uID?>, full_details: 1 },
			    error: function(xhr) {
			      alert('Ajax request error' + xhr);
			    },
			    success: function(response) {
					$('#all_loans').html(response);
			    }
		  	});
			IsLoaded = true;	
		}
		
		$('#current_loan').slideUp(250);
		$('#current_reservation').slideUp(250);
		$('#current_penal').slideUp(250);
		
		$('#all_loans').slideDown(250);
		$('#tag_current').attr('class', 'page_tab_off');
		$('#tag_all').attr('class', 'page_tab_on');
		
	}
}
<?}?>
</script>

<div class="user_details_info" style='display : none;'>
	<div class="page_tab">
		<ul>
			<li class="page_tab_on" id="tag_current"><a href="#" onclick="jsShowDetails(1)"><span><?=$Lang['libms']['CirculationManagement']['statusnow']?>  [&larr;]</span></a></li>
			
			<li class="page_tab_off" id="tag_all"><a href="#" onclick="jsShowDetails(0)"><span><?=$Lang['libms']['CirculationManagement']['all_loan_records']?>  [&rarr;]</span></a></li>
		</ul>
										
	</div>
							  
							  
	<div class="table_board" id="current_loan">
		<h1><?=$Lang['libms']['CirculationManagement']['borrowed']?></h1>
		<table class="common_table_list">
		<col  />
		<thead>
		<tr>
		  <th class="num_check">#</th>
		  <th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
<?php
	if ($libms->get_system_setting("circulation_borrow_return_accompany_material")) {
		echo '<th>'.$Lang["libms"]["CirculationManagement"]["borrow_accompany_material"].'</th>';
	}
?>		  
		  <th><?=$Lang['libms']['CirculationManagement']['borrowday']?> </th>
		  <th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
		  <!--  <th><?=$Lang['libms']['CirculationManagement']['status']?></th> -->
		</tr>
		
		<? 
			$i = 0;
			if (!empty($User->borrowedBooks))
				foreach ($User->borrowedBooks as $book) :
    				$displayBookTitle = $book['BookTitle'];
    				if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
    				    $displayBookTitle .= $book['BookSubTitle'] ? " :" . $book['BookSubTitle'] : "";
    				}
    				$displayBookTitle .= " [".$book['BarCode']."]";
				
					$duetime = strtotime($book['DueDate']);
					$nowtime = time();
					if ( time() - $duetime > 60*60*24 ){
						$is_overdue = true;
						$tm = new TimeManager($libms);
			
						$overdue_day_count = $tm->dayForPenalty($duetime, $nowtime);
					}else{
						$is_overdue = false;
					}
					$i++;
		?>
					<tr borrow cirtype='<?=$book['CirculationTypeCode']?>' active <? if ($is_overdue) echo 'class="alert_row"'; ?>>
					  <td><?=$i?></td>
					  <td><?=$displayBookTitle?></td>
<?php
	if ($libms->get_system_setting("circulation_borrow_return_accompany_material")) {
		$borrow_accompany_material = $book['BorrowAccompanyMaterial'] ? $Lang["libms"]["CirculationManagement"]["borrow_accompany_material_yes"] : $Lang["libms"]["CirculationManagement"]["borrow_accompany_material_no"];
		echo '<td>'.$borrow_accompany_material.'</td>';
	}
?>					  
					  <td><?=date('Y-m-d',strtotime($book['BorrowTime']))?></td>
					  <td>
					  	  <? if ($is_overdue): ?>
							<span class="alert_late"><?=$book['DueDate']?> ( <?=$Lang['libms']['CirculationManagement']['msg']['overdue1']?> <?=$overdue_day_count?><?=$Lang['libms']['CirculationManagement']['msg']['overdue2']?>)</span>
						  <? else: ?>
						    <?=$book['DueDate']?>
						  <? endif; ?>
					  </td>
					  
					  
					  <!--
					  <td><? if ($is_overdue): ?>
							<span class="alert_late"><?=$Lang['libms']['CirculationManagement']['msg']['overdue1']?> <?=$overdue_day_count?><?=$Lang['libms']['CirculationManagement']['msg']['overdue2']?></span>
						  <? endif; ?>
					  </td>
					   -->
					</tr>
				<? 
					unset($is_overdue);
					endforeach;
				?>
		
		</thead>

		</tbody>
		</table>
	</div>
								  
	<div class="table_board" id="current_reservation">
		<h1><?=$Lang['libms']['CirculationManagement']['reserved']?></h1>
		<table class="common_table_list">
			<col />
			<thead>
				<tr>
					<th class="num_check">#</th>
					<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
					<th><?=$Lang['libms']['CirculationManagement']['reserveday']?></th>
					<!-- <th><?=$Lang['libms']['CirculationManagement']['reserverperiod']?></th>  -->
					<th><?=$Lang['libms']['CirculationManagement']['status']?></th>
				</tr>
				<?
					$i = 0;
					if (!empty($User->reservedBooks))
					foreach ($User->reservedBooks as $book) :
						if ( $book['RecordStatus'] == 'READY' )
							$is_ready = TRUE;
						else
							$is_ready = FALSE;
						$i++;

						$displayBookTitle = $book['BookTitle'];
						if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
						    $displayBookTitle .= $book['BookSubTitle'] ? " :" . $book['BookSubTitle'] : "";
						}
					?>
						<tr reserve cirtype='<?=$book['CirculationTypeCode']?>' active <? if ($is_ready) echo 'class="avaliable_row"'; ?>>
						  <td><?=$i?></td>
						  <td><?=$displayBookTitle?></td>
						  <td><?=date('Y-m-d',strtotime($book['ReserveTime']))?></td>
						  <!-- <td><?=$book['ExpiryDate']?></td> --> 
						  <td><? if ($is_ready): ?>
								<span class="alert_avaliable"><?=$Lang["libms"]["book_reserved_status"]["READY"]?></span>
							  <?else:?>
								<?=$Lang["libms"]["book_reserved_status"]["WAITING"]?>
							  <? endif; ?>
						  </td>
						</tr>
				<? endforeach; ?>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
								  
	<div class="table_board" id="current_penal">
		<h1 class="alert_header"><?=$Lang['libms']['CirculationManagement']['payment']?></h1>
		<table class="common_table_list">
		<col   />
		<thead>
		<tr>
		  <th class="num_check">#</th>
		  <th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
		  <th><?=$Lang['libms']['CirculationManagement']['dueday']?> </th>
		  <th><?=$Lang['libms']['CirculationManagement']['returnday']?></th>
		  <th><?=$Lang['libms']['CirculationManagement']['overdue_type']?></th>
		  <th><?=$Lang['libms']['CirculationManagement']['paied']?></th>
		  <th><?=$Lang['libms']['CirculationManagement']['payment']?></th>
		</tr>
		<?
			$i = 0;
			if (!empty($overdues))
			foreach ($overdues as $overdue) :
			$i++;
			$displayBookTitle = $overdue['BookTitle'];
			if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
			    $displayBookTitle .= $overdue['BookSubTitle'] ? " :" . $overdue['BookSubTitle'] : "";
			}
			$displayBookTitle .= " [".$overdue['BarCode']."]";
		?>
		<tr>
		  <td><?=$i?></td>
		  <td><?=$displayBookTitle?></td>
		  <td><?=$overdue['DueDate']?></td>
		  <td><?=(empty($overdue['ReturnedTime']) || $overdue['ReturnedTime'] == 0)?'-':date('Y-m-d',strtotime($overdue['ReturnedTime']))?></td>
		  <td><?=empty($overdue['DaysCount'])?$Lang["libms"]["book_status"]["LOST"]:sprintf($Lang['libms']['CirculationManagement']['overdue_with_day'], $overdue['DaysCount'])?></td>		  
		  <td nowrap="nowrap">$<label class='PaymentReceived'><?=empty($overdue['PaymentReceived'])?'0':$overdue['PaymentReceived']?></label></td>
		  <td><span class="alert_text">$<?=$overdue['Payment']-$overdue['PaymentRecevied']?></span></td>
		</tr>
		<? endforeach; ?>
		</thead>
		<tbody>
		</tbody>
		</table>
	</div>
	
	
								  
	<div class="table_board" id="all_loans" style="display:none">	
	</div>
</div>
