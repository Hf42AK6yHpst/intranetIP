<?php
/*
 * Using: 
 * 
 * This file should be run daily for upating the reserved status when the reserved book is expired
 * Created by Henry
 * 
 * 	2015-12-03 [Cameron]
 * 		- fix bug: should pass today in sendReservationEmail() for cancel reserved book by system
 */
$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($WRT_ROOT."includes/global.php");
include_once($WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($WRT_ROOT."includes/libdb.php");
include_once($WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");
intranet_opendb();
$libms = new liblms();
echo "This file is for upating the reserved status when the reserved book is expired<br/><br/>";
echo "Program started!<br/>";
$sql = "Select UserID, ReservationID, BookID FROM `LIBMS_RESERVATION_LOG` WHERE ExpiryDate < NOW() - INTERVAL 1 DAY AND ExpiryDate <> 0 AND RecordStatus = 'READY'";
//$libms->db_db_query($SQL);
$result = $libms->returnArray($sql);
//list($reservationID, $bookID) = $libms->returnArray($sql);

echo "There are ".sizeof($result)." expired reserved book!<br/>";
for($i=0; $i < sizeof($result); $i++){
$sql = "UPDATE LIBMS_RESERVATION_LOG SET 
			    RecordStatus = 'EXPIRED', DateModified = now()
			    WHERE ReservationID = ".$result[$i]['ReservationID'];
$libms->db_db_query($sql);		
	    
///////////////announce user the reservation is expired
$sql = "SELECT b.BookTitle from LIBMS_BOOK b
                LEFT JOIN LIBMS_RESERVATION_LOG a on a.BookID=b.BookID 
                where b.BookID=".$result[$i]['BookID']." 
                GROUP BY b.BookID";//see if any no of reservation > no of book copies
		
        	list($book_title) = current($libms->returnArray($sql));
			
			//debug_pr($sql);
			$notifcation_settings	= array(
	    'reserve' => getNotificationSettings('reserve_email'),
	    'overdue' => getNotificationSettings('overdue_email')
	);
			if ($result[$i]['UserID'] && $notifcation_settings['reserve']){//off temporary
		    	sendReservationEmail(array($result[$i]['UserID']), $book_title, 1);
			}
///////////////////////////////////////////////////////			    
			    
			 $sql = "SELECT UserID, ReservationID FROM LIBMS_RESERVATION_LOG
			    WHERE BookID = '".$result[$i]['BookID']."' AND RecordStatus='WAITING'
			    ORDER BY ReserveTime ASC LIMIT 1";
			    
		    list($notify_user, $reservation_id) = current($libms->returnArray($sql));    

			$sql = "SELECT `Value` FROM `LIBMS_SYSTEM_SETTING` WHERE name='max_book_reserved_limit'";
			$result1 = $libms->returnArray($sql);
			$timeManager = new TimeManager(new liblms());
			if (empty($result1)){
				$day_limit = 0;
			}else
				$day_limit = $result1[0][0];
				if($day_limit > 0)
					$newExpiryDate = date('Y-m-d',$timeManager->findOpenDate(strtotime('+'.$day_limit.' day')));
				else
					$newExpiryDate = "0000-00-00";
			$sql = "UPDATE LIBMS_RESERVATION_LOG SET 
			    RecordStatus = 'READY', DateModified = now(), ExpiryDate = '".$newExpiryDate."'
			    WHERE ReservationID = $reservation_id";
		    $libms->db_db_query($sql);
		    
		    if(!$reservation_id){
			    $sql = "UPDATE LIBMS_BOOK_UNIQUE SET 
				    RecordStatus = 'NORMAL'
				    WHERE BookID=".$result[$i]['BookID']." AND RecordStatus = 'RESERVED'
					LIMIT 1";
			    $libms->db_db_query($sql);
			}
			$sql = "SELECT b.BookTitle from LIBMS_BOOK b
                LEFT JOIN LIBMS_RESERVATION_LOG a on a.BookID=b.BookID 
                where b.BookID=".$result[$i]['BookID']." 
                GROUP BY b.BookID";//see if any no of reservation > no of book copies
		
        	list($book_title) = current($libms->returnArray($sql));
			
			//debug_pr($sql);
			$notifcation_settings	= array(
	    'reserve' => getNotificationSettings('reserve_email'),
	    'overdue' => getNotificationSettings('overdue_email')
	);
			if ($notify_user && $notifcation_settings['reserve']){//off temporary
		    	sendReservationEmail(array($notify_user), $book_title);
			}
			echo "Upate the ".($i+1)." record success!<br/>";
}
echo "Program ended!<br/>";
intranet_closedb();

function getNotificationSettings($type){
	global $libms;
	$sql = "SELECT enable FROM LIBMS_NOTIFY_SETTING
		WHERE name = '$type'";
	
	return current($libms->returnVector($sql));
	
}
function sendReservationEmail($user_id, $book_title, $unReserved = 0){
	
	global $intranet_session_language, $intranet_root, $Lang;
	    	  
	include_once($intranet_root.'/home/library_sys/lib/liblibrarymgmt.php');
	include_once($intranet_root."/lang/libms.lang.$intranet_session_language.php");
	include_once($intranet_root."/includes/libemail.php");
	include_once($intranet_root."/includes/libsendmail.php");
	include_once($intranet_root."/includes/libwebmail.php");
	
	global $Lang;
	
	$lwebmail = new libwebmail();
	$mailSubject = $Lang["libms"]["report"]["overdue_email"]["Subject"];
	if($unReserved == 0){
		$lwebmail->sendModuleMail(
		    (array) $user_id,
		    $Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'],
		    sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'] , $book_title),
		1,'', 'User');
	}
	else{
		$lwebmail->sendModuleMail(
		    (array) $user_id,
		    $Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_system']['subject'],
		    sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_system']['mail_body'], date("Y-m-d"),$book_title),
		1,'', 'User');
	}
		    
    }
?>