<?php
// modifying by:

// Note: Please use UTF-8 for editing

/**
 * ***********************************************************
 *
 * 20191011 (Cameron)
 * - use large font for common_table_list if $sys_custom['eLibraryPlus']['Circulation']['LargeFont'] is set [case #K155570]
 * 
 * 20181128 (Henry)
 * - modified keydown event to pervent multiple request to server
 * 
 * 20180222 (Cameron)
 * - fix: updateTotal(), change the class to get total, add updateTotalAfterConfirm()
 * - fix: add meta characterset (utf-8) to show no access right message
 * 
 * 20171127 (Cameron)
 * - set barcode search bar to left position if $sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] == true [case #F129893]
 * - unify code for ej change on 20160713
 *
 * 20170314 (Cameron)
 * - add IP checking for circulation
 *
 * 20160729 (Cameron)
 * - append timestamp to jquery.alerts.js so that it can refresh to take effect
 *
 * 20160713 (Cameron)
 * - change js include file to script.utf8.* to support Chinese in jquery.alerts plugin [ej only]
 *
 * 20160307 (Cameron)
 * - improvement: add confirm process before renew
 *
 * 20160219 (Cameron)
 * - add quick-access to search book by pressing F3
 *
 * 20151118 (Cameron)
 * - trim all leading and trailing space ( including multiple byte space) for barcode
 *
 * 20151016 (Cameron)
 * - fix bug on checking if input_code_text is empty, do not submit if it's empty
 *
 * 20151015 (Cameron)
 * - set focus to input_code_text field after user click Yes / No button about borrow accompany material or not
 *
 * 20151014 (Cameron)
 * - add function renew_ajax_handler()
 * - modify onClick event of return_a_book button (Enter) to support prompting user to renew accompany material or not
 *
 * 20150623 (Henry)
 * - disable escape key
 * 20150609 (Henry)
 * - apply class management permission
 *
 * ***********************************************************
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");

// include_once("libms_interface.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
intranet_auth();
intranet_opendb();

$libms = new liblms();

$is_open_loan = $libms->IsSystemOpenForLoan();

$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['renew'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}

// unset!
$_SESSION['LIBMS']['CirculationManagemnt']['UID'] = "";
unset($_SESSION['LIBMS']['CirculationManagemnt']['UID']);

$interface = new interface_html("libms_circulation_user_prompt.html");
$GLOBALS['LIBMS']['PAGE'] = "RENEW";

$xmsg = $_SESSION['LIBMS']['xmsg'];
unset($_SESSION['LIBMS']['xmsg']);

if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['overdue'] || $global_lockout)) {} else {
    $CanHandleOverduePayment = true;
}

$needConfirm = $libms->get_system_setting("batch_book_renew_by_confirm");

$interface->LAYOUT_START();

if (! $is_open_loan) {
    echo "<div style='margin: 5em;'><font size='3'>" . $libms->CloseMessage . "</font></div>";
} else {
    
    if ($sys_custom['eLibraryPlus']['Circulation']['LargeFont']) {
        echo '<link href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/elibplus_large_font.css" rel="stylesheet" type="text/css">'."\n";
    }
    ?>

<link href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.css"
	rel="stylesheet" type="text/css">
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>lang/script<?=$junior_mck?'.utf8':''?>.<?=$intranet_session_language?>.js"></script>
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.js?<?=time()?>"></script>

<script type="text/javascript">
var cached_password=false;
var password_match=false;

function dateToYMD(date)
{
    var d = date.getDate();
    var m = date.getMonth()+1;
    var y = date.getFullYear();
    return '' + y +'-'+ (m<=9?'0'+m:m) +'-'+ (d<=9?'0'+d:d);
}

function authenticate(caller, uniqueBookID) {
	if( cached_password == '' || cached_password == null){
		cached_password = false;
		return false;
	}
			
	$.get(	'override_ajax.php',								
			{ override_password : cached_password	},
			function(data){
				if(data !='1'){
					cached_password = false;										
					jAlert("<?=$Lang["libms"]["Circulation"]["WrongPassword"]?>", "<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"]?>",function(){return allowRenew(caller, uniqueBookID);});
				}else{
					password_match=true;
					allow_Renew_switchon(caller,uniqueBookID);
				}
				return;
			}
	);
}

function allowRenew(caller, uniqueBookID){
	<?
    $tmp_override_password = $libms->get_system_setting('override_password');
    if (! empty($tmp_override_password)) :
        ?>
			if( !cached_password  && !password_match ){
				jPromptPassword( "<?=$Lang["libms"]["Circulation"]["PleaseEnterPassword"]?>:", "", "<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"]?>",function(r) {
					if (r) {						
						cached_password = r;
						authenticate(caller, uniqueBookID);
					}	// get input password 					
				});
			}

			authenticate(caller, uniqueBookID);

	<? else: ?>
		allow_Renew_switchon(caller,uniqueBookID);
	<? endif; ?>

}
function allow_Renew_switchon(caller,uniqueBookID){
		$('#div_'+uniqueBookID).html('<input class="renewCheckbox" name="renew[]" type="checkbox" value="'+uniqueBookID+'" />');
		$('#div_'+uniqueBookID+' input.renewCheckbox').change( function(){
		var thisCheckbox = $(this);
		thisCheckbox.parents('tr:first').toggleClass('avaliable_row');
		updateTotal();
		if (thisCheckbox.is(':checked')){
			$.get('get_borrow_due_date.php',{ UniqueID : thisCheckbox.val(), Mode : "ByBatch" },function (data){
				if (data == 'AlreadyQuit') {
					thisCheckbox.parents('tr:first').toggleClass('avaliable_row');
					thisCheckbox.parent().parent().next().html("-").next().html("<span style=\"color:red; font-weight: bold\"><?=$Lang['libms']['CirculationManagement']['msg']['already_quit']?></span>");
					thisCheckbox.prop('checked', false);
					updateTotal();					
					thisCheckbox.prop('disabled',true);
				}
				else {
					if (thisCheckbox.is(':checked')){
						var currentTime = new Date();
						thisCheckbox.parent().parent().next().html(dateToYMD(currentTime)).next().html(data);
						var force_id = $("#ForceRenewBookUniqueID").val();
						var this_uid = thisCheckbox.val();
						if (force_id.indexOf(this_uid) < 0) {
							force_id = force_id + ',' + this_uid;
							$("#ForceRenewBookUniqueID").val(force_id);
						}
					}
				}
			})
		}else{
			thisCheckbox.parent().parent().next().html("-").next().html("-");
		}
	});
	$('#div_'+uniqueBookID+' input.renewCheckbox').prop('checked', true);
	$('#div_'+uniqueBookID+' input.renewCheckbox').change();
	updateTotal();	
}
	
$().ready( function(){
	$.ajaxSetup({ cache: false});
	
<? if ($needConfirm):?>
	$(window).on('beforeunload', function(){
		if ($('#return_table tr').length > 1)
		if ($('input.renewCheckbox:checked').length > 0)
			return '<?=$Lang['libms']['CirculationManagement']['msg']['comfirm_leave']?>';
	});
	
	$('input#renew_submit').click(function(e){
		e.preventDefault();
		if (checkform()) {
			$('#ConfirmRenew').val('1');
			$(window).off('beforeunload');
			
			$.ajax({
			  type: "POST",
			  url: 'batch_renew_ajax_action.php',
 			  Mode : "ByBatch", 
			  HandleOverdue : "<?=$libms->get_system_setting("batch_return_handle_overdue")?>", 
			  HasChosenRenewAttachment : "false" ,
			  data : $('#renew_form').serialize(),		  
			  success: after_confirm_renew_books,
			  error: function(){ alert('Error on AJAX call!'); }
			});
		}
	});
	
	$('#reset_btn').on('click', $('#return_table .renewCheckbox'), function(){
		self.location = self.location;
	});
	
	$("#return_table").on('click', ".renewCheckbox", function(){
		var thisCheckbox = $(this);
		thisCheckbox.parents('tr:first').toggleClass('avaliable_row');
		updateTotal();

		if (thisCheckbox.is(':checked')){
			$('#input_code_text').focus();
			$.get('get_borrow_due_date.php',{ UniqueID : thisCheckbox.val(), Mode : 'ByBatch' },function (data){
				if (data == 'AlreadyQuit') {
					thisCheckbox.parents('tr:first').toggleClass('avaliable_row');
					thisCheckbox.parent().parent().next().html("-").next().html("<span style=\"color:red; font-weight: bold\"><?=$Lang['libms']['CirculationManagement']['msg']['already_quit']?></span>");
					thisCheckbox.prop('checked', false);
					updateTotal();					
					thisCheckbox.prop('disabled',true);
				}
				else {
					if (thisCheckbox.is(':checked')){
						var currentTime = new Date();
						thisCheckbox.parent().parent().next().html(dateToYMD(currentTime)).next().html(data);
					}
				}
			})
		}else{
			thisCheckbox.parent().parent().next().html("-").next().html("-");
			$('#input_code_text').focus();
		}
	});
	
<? endif;?>
	
	$('div.table_row_tool a.delete').click(function(){
		$(this).parents('tr:first').remove();
		updateTotal();
	});
	
	$('#return_a_book').click( function(){
<? if ($needConfirm):?>
	if ($('#confirm_div').css('display') == 'none') {
		$('#return_table tr').not('tr:first').remove();
		$('#col_status').text('<?=$Lang['libms']['CirculationManagement']['status']?>');
		$('#col_status').after('<th id="col_check" nowrap="nowrap" class="num_check"><?=$Lang['libms']['CirculationManagement']['renew']?></th>');
		updateTotal();
		$("#total_and_clear").css("display","none");
		$('#confirm_div').css({'display':'block'});
	}
<? endif;?>		
//		$('#input_code_text').val($('#input_code_text').val().toUpperCase().replace(" ",''));
		$('#input_code_text').val($('#input_code_text').val().toUpperCase().replace(/^\s+|^　+|\s+$|　+$/gm,''));
		var codemod = ($('#input_code_text').val()).replace(/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~]/g,'-');
		if (codemod!=''){
			if ($('tr#'+codemod).length ==0){
				if ($('#input_code_text').val() !='' ){
					$.getJSON(
							'batch_renew_ajax_action.php',
							{ 
								barcode : $('#input_code_text').val(),
								ID : $('table#return_table tr.returned').length,
								Mode : "ByBatch",
								HandleOverdue : "<?=$libms->get_system_setting("batch_return_handle_overdue")?>", 
								HasChosenRenewAttachment : "false" 
							},
							'JSON'
					)
					.success(renew_ajax_handler)
					.error(function(){ alert('Error on AJAX call!'); });
				
					$('#input_code_text').val('');
					$('#input_code_text').focus();
		//			return false;
				}
			}else{
				alert("<?=$Lang['libms']['CirculationManagement']['msg']['scanned']?>");
			}
		}
	});
	$('#input_code_text').keyup(function(event) {
	  if (event.which == 13) {
		$('#return_a_book').click();
	  }  
	});
	
	$('#input_code_text').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});
	
//	$('#input_code_text').focus();
	
	var isKeyDown = false;
	$(document).bind('keydown',function(e){
			if(!isKeyDown){
				isKeyDown = true;
			switch (e.keyCode)
			{
//				case 27:	// Esc
//					self.location = "borrow.php?logout=true";
//					return false;
<?php if ($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $c_right['circulation management']['return']) { ?>
			    case 112:	// F1
					self.location = "batch_return.php";
					return false;
<?php } ?>
			    case 113:	// F2
					self.location = "borrow.php";
					return false;
				case 114:	// F3
					document.book_search_form.Keyword.focus();
					isKeyDown = false;
					return false;
			    case 115:	// F4
					self.location = "batch_renew.php";
					return false;

			    case 116:	// F5
					self.location = self.location;
					return false;					
			    case 119:	// F8
					HandleOverdue();
					isKeyDown = false;
					return false;
			    case 120:	// F9
					$('input#renew_submit').click();
					isKeyDown = false;
					return false;
			    default:
					//alert(e.keyCode);
					isKeyDown = false;
			      return true;
			}
			}
  
		});
	
	$('#input_code_text').focus();
	updateTotal();
			
	if (typeof($.browser.msie)!="undefined" && $.browser.msie)
	{
		document.onhelp = new Function("return false;");
		window.onhelp = new Function("return false;");
	}
});

function updateTotal(){
	total = $('input.renewCheckbox:checked').length;
	$('.op_num em, .op_num_return b ').html(total);
}

function updateTotalAfterConfirm() 
{
	total = $('table#return_table tr.returned').length;
	$('.op_num em, .op_num_return b ').html(total);
}

function HandleOverdue(){
<?php if ($CanHandleOverduePayment) { ?>
	var last_user_id = $('.not_class_but_id').val();
	if (last_user_id!="" && last_user_id>0)
	{
		self.location = "overdue.php?user_id=" + last_user_id;
	} else
	{
		alert("<?=$Lang['libms']['CirculationManagement']['msg']['no_overdue_record']?>");
	}
<?php } ?>
}

<? if ($needConfirm):?>
function after_confirm_renew_books(ajaxReturn) {
	$('#col_check').remove();
	$('#col_status').text('<?=$Lang['libms']['bookmanagement']['result']?>');
	$('#return_table tr').not('tr:first').remove();
	obj = JSON.parse(ajaxReturn);
	renew_ajax_handler(obj);
	$("#total_and_clear").css("display","inline");
	$('#confirm_div').css({'display':'none'});
}
<? endif;?>

function renew_ajax_handler(ajaxReturn){
	if (ajaxReturn.success){
		var s = ajaxReturn.script;
		if (s != null) {
			s = s.replace("<script>","");
			s = s.replace("<\/script>","");
			eval(s);
		}
		else {
			if (ajaxReturn.html != null) {
				table = $('#return_table tbody');
				row = $(ajaxReturn.html);
				table.find('tr:first').after(row);
				updateTotalAfterConfirm();
			}
			else {
				alert('<?=$Lang["libms"]["CirculationManagement"]["return_ajax"]["no_borrow_record"]?>');
			}
			$('#input_code_text').focus();
		}
	}else{
		alert('<?=$Lang["libms"]["CirculationManagement"]["return_ajax"]["no_borrow_record"]?>');
	}
}

function checkform()
{
	if ($('input.renewCheckbox:checked').length <= 0) {
		alert("<?=$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed']?>");
		$('#input_code_text').focus();
		return false;
	}
	else {
		return true;
	}
}

</script>

<? if ($needConfirm):?>
<form name="renew_form" id="renew_form">
	<input type="hidden" name="ConfirmRenew" id="ConfirmRenew" value=""> <input
		type="hidden" name="Mode" id="Mode" value="ByBatch"> <input
		type="hidden" name="ForceRenewBookUniqueID"
		id="ForceRenewBookUniqueID" value="">
<? endif;?>

<div class="table_board">

		<div class="enter_code"
			<?=$sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] ? 'style="float:left;"':''?>><?=$Lang['libms']['CirculationManagement']['barcode']?> <input
				name="" type="text" id='input_code_text' class="input_code_text" />
			<input name="submit2" id='return_a_book' type="button"
				class="formsubbutton"
				value="&crarr; <?=$Lang["libms"]["btn"]["enter"]?>" />
		</div>
		<div class="op_num_return">
			<div id="total_and_clear" style="display:<?=((!$needConfirm) || ($ConfirmRenew))?"inline":"none"?>">
				<span><?=$Lang["libms"]["CirculationManagement"]["no_batch_renew"]?> :</span><font
					size="3"><b> 0</b></font> &nbsp; &nbsp; <input name="submit2"
					id='clear_records' type="button" class="formsubbutton"
					value="<?=$Lang['Btn']['Clear'].$Lang["libms"]["CirculationHotKeys"]["Cancel"]?>"
					onClick="self.location='batch_renew.php'" />
			</div>
		</div>
		<table id="return_table" class="common_table_list">
			<tbody>
				<tr>
					<th class="num_check">#</th>
					<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
					<th><?=$Lang["libms"]["CirculationManagement"]["borrowed_by"]?></th>
					<th><?=$Lang['libms']['CirculationManagement']['borrowday']?></th>
					<th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
					<th><?=$Lang['libms']['CirculationManagement']['renew_left']?></th>
<? if ($needConfirm):?>
			  <th id="col_status"><?=$Lang['libms']['CirculationManagement']['status'] ?></th>
					<th id="col_check" nowrap="nowrap" class="num_check"><?=$Lang['libms']['CirculationManagement']['renew']?></th>
<? else: ?>			  
			  <th><?=$Lang['libms']['bookmanagement']['result'] ?></th>
<? endif;?>			  
			  <th><?=$Lang['libms']['CirculationManagement']['renewday']?></th>
					<th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="op_btn">
		<div class="form_btn"></div>

	</div>

<? if ($needConfirm):?>
		<div class="op_btn" id="confirm_div" style="display: block;">
		<div class="op_num">
			<span><?=$Lang['libms']['CirculationManagement']['no_renew']?> :</span><em
				id='renewCount'> 0</em>
		</div>
		<div class="form_btn">
			<input name="submit2" id='renew_submit' type="submit"
				class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['confirm'].$Lang["libms"]["CirculationHotKeys"]["Confirm"]?>" />
			<input name="reset_btn" id='reset_btn' type="button"
				class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['cancel'].$Lang["libms"]["CirculationHotKeys"]["Cancel"]?>" />
		</div>
	</div>
</form>
<? endif;?> 
<?=	$interface->FOCUS_ON_LOAD("input_code_text") ?>
<?php
}


$interface->LAYOUT_STOP();
?>
