<?php

// # modifying by:   

/**
 * Log :
 *
 * Date: 2020-05-28 (Cameron)
 * add function getNotifyContentOfBookToBeOverdue(), updateNotifyMessageContent(), isNotifyScheduleExpired(), isOnlyOneNotifyMessageTarget(), getNotifyMessageID(),
 *  getNotifyMessageTarget(), getNotifyMessageReference(), deleteNotificationTargetAndReference()
 *
 * Date: 2020-05-27 (Cameron)
 * add function isNotifyMessageIncludeOtherBooks()
 *
 * Date: 2019-11-11 (Henry)
 * modified loss_a_book() to support lost book with overdue penalty
 * 
 * Date: 2019-03-28 (Cameron)
 * retrieve BookSubTitle for used in renew book in _get_reserved_books() [case #K154500]
 * 
 * Date: 2019-03-26 (Cameron)
 * - fix: retrieve unique BorrowLogID from LIBMS_OVERDUE_LOG in _get_borrowed_books_history()
 * 
 * Date: 2019-03-05 (Cameron)
 * retrieve BookSubTitle for used in renew book in _get_borrowed_books() [case #K154500]
 * 
 * Date: 2019-03-04 (Cameron)
 * filter out DELETE LIBMS_OVERDUE_LOG record in _get_borrowed_books_history() [case #X157459]
 * 
 * Date: 2018-12-13 (Cameron)
 * add public function getLatestBalance() (used in RecordManager.php > returnBook) [case #L154330]
 * 
 * Date: 2018-11-01 (Henry)
 * modified loss_a_book() to extract number form the list and purchase price
 *
 * Date: 2018-10-02 (Cameron)
 * add TransAmountBefore and TransAmountAfter to pay_overdue(), cancel_overdue(), wavie_overdue() [case #T139412] 
 *
 * Date: 2018-09-21 (Cameron)
 * pass $uniqueBookID and $uID to getReservationByBookID() in borrow_book() to exclude user who borrows this book that is reserved and the status is READY
 * add case current user has READY reserved book in borrow_book()
 * 
 * Date: 2018-09-05 (Cameron)
 * set $newExpiryDate = "0000-00-00" in reserve_book() for case of library is closed for all dates
 * 
 * Date: 2018-02-14 (Cameron)
 * retrieve LocationCode in _get_borrowed_books() [case #E129637]
 *
 * Date: 2018-02-02 (Henry)
 * modified return_book() for returning lost book logic [Case#M130018]
 *
 * Date: 2018-01-31 (Cameron)
 * show ItemSeriesNum after Book Title in _get_borrowed_books(), _get_borrowed_books_history() and _get_overdue_records() [case #L130405]
 *
 * Date: 2017-09-14 (Cameron)
 * pass borrow timestamp to findOpenDateSchoolDays() in borrow_book(), return_book() (case #R125534)
 *
 * Date: 2017-09-07 (Cameron)
 * skip updating LIBMS_BOOK_UNIQUE.RecordStatus book reservation process in return_book() if the book item is 'WRITEOFF' [case #L123983]
 *
 * Date: 2017-03-24 (Cameron)
 * add log transaction for tracing error
 *
 * Date: 2016-09-13 (Cameron)
 * - fix bug: should call SYNC_PHYSICAL_BOOK_TO_ELIB in loss_a_book() to set Publish=0
 *
 * Date: 2016-08-31 (Cameron)
 * - store additional charge by method and the amount for lost book in loss_a_book()
 * - add function settle_zero_overdue()
 *
 * Date: 2016-04-11 (Cameron)
 * - fix bug when book status is "on display" but force to loan by admin, NoOfCopyAvailable should still keep 0 (case# K90505)
 * - apply UPDATE_BOOK_COPY_INFO to borrow_book() and return_book()
 *
 * Date: 2016-02-22 (Cameron)
 * - apply get_number_from_text() to bookPrice in loss_a_book(),
 * Assume ListPrice, Purchase and Penalty use the same currency
 *
 * Date: 2016-02-11 (Cameron)
 * - add $lost_book_price_by (indicate compensation amount of lost book is based on which price) to loss_a_book()
 *
 * Date 2015-11-11 [Henry]
 * modified return_book() to add para $returnDateTime
 * modified renew_book() to add para $renewDateTime
 * modified borrow_book() to add para $borrowDateTime
 *
 * Date: 2015-11-05 (Cameron)
 * - cancel parameter AccompanyMaterial in renew_book() because it should be retrieved from borrow log
 *
 * Date: 2015-10-13 (Cameron)
 * - retrieve BorrowAccompanyMaterial in _get_borrowed_books()
 * - add parameter AccompanyMaterial to renew_book()
 *
 * Date 2015-08-14 (Cameron)
 * - add parameter AccompanyMaterial to borrow_book()
 *
 * Date 2015-07-13 (Cameron)
 * - fix logic to get RemarkToUser from LIBMS_BOOK_UNIQUE first, then from LIBMS_BOOK in _get_borrowed_books()
 * - retrieve AccompanyMaterial in _get_borrowed_books()
 *
 * Date 2015-07-07 (Henry)
 * Modified function pay_overdue()
 * Date 2015-03-26 (Henry)
 * fix bug when $newExpiryDate == "0000-00-00"
 * Date 2014-12-03 (Ryan)
 * Modified _get_reserved_books CHANGE TO LEFT JOIN LIBMS_BOOK_UNIQUE to hanlde 'Waiting' Records( which has no book assigned yet)
 * Date 2014-12-03 (Ryan)
 * Modified _get_overdue_records & _get_reserved_books added Barcode
 *
 * Date 2014-12-03 (Ryan)
 * Added AdminMode, to override some of rules such as "allow same day return, waive Password etc."
 *
 * Date 2014-12-03 (Ryan)
 * Added OverdueRecords, userGroupTitle
 * Date 2013-11-01 (Carlos)
 * Added cancel_overdue(), to void an overdue payment
 * Date 2013-09-02 [Carlos]
 * Modified pay_overdue(), add optional parameter to use ePayment to pay overdue payment
 * Date 2013-09-02 [Yuen]
 * Updated loss_a_book() for getting book item information from LIBMS_BOOK_UNIQUE table
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$WRT_ROOT = "{$_SERVER['DOCUMENT_ROOT']}/";

// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once ("{$WRT_ROOT}includes/global.php");
include_once ($WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($WRT_ROOT . "includes/libdb.php");
include_once ($WRT_ROOT . "includes/pdump/pdump.php");

include_once ($WRT_ROOT . "home/library_sys/management/circulation/RightRuleManager.php");

include_once ($WRT_ROOT . "home/library_sys/management/circulation/RecordManager.php");

include_once ($WRT_ROOT . "home/library_sys/management/circulation/TimeManager.php");
include_once ($WRT_ROOT . "includes/libelibrary.php");

class User
{

    public $userInfo;

    public $borrowedBooks;

    public $reservedBooks;

    public $overdueRecords;

    public $userGroupTitle;

    public $borrowedBooksHistory;

    private $adminMode = false;

    // OBJ
    public $rightRuleManager;

    public $timeManager;

    public $recordManager;

    public $libms;

    public function __construct($UID = null, $libms = null)
    {
        // global $eclass_prefix;
        // database for this module
        // $this->db = $eclass_prefix . "eClass_LIBMS";
        // tolog($UID);
        if (! empty($libms))
            $this->libms = $libms;
        else
            $this->libms = new liblms();
        
        $this->userInfo = $this->_get_user_info($UID);
        if (! $this->userInfo) {
            return;
        }
        
        $this->borrowedBooks = $this->_get_borrowed_books();
        $this->reservedBooks = $this->_get_reserved_books();
        $this->rightRuleManager = new RightRuleManager($this, $this->libms);
        $this->recordManager = new RecordManager($this->libms);
        $this->timeManager = new TimeManager($this->libms);
    }

    function setAdminMode($bool)
    {
        $this->AdminMode = $bool;
    }

    function getBorrowedHistory()
    {
        $this->borrowedBooksHistory = $this->_get_borrowed_books_history();
    }

    private function _get_user_info($UID = null)
    {
        if (isset($UID)) {
            $UID = PHPTOSQL($UID);
            // should include deleted user in order to get all records!
            // $sql = "SELECT * FROM `LIBMS_USER` WHERE `UserID`={$UID} AND `RecordStatus`='1' LIMIT 1";
            $sql = "SELECT * FROM `LIBMS_USER` WHERE `UserID`={$UID} LIMIT 1";
            $result = $this->libms->returnArray($sql);
            if ($result) {
                $result = $result[0];
            } else {}
        }
        // $x=array($sql, $result,mysql_error());
        // tolog( $x );
        return $result;
    }

    private function _get_latest_balance($UID = null)
    {
        $result = 0;
        if ($UID) {
            $UID = PHPTOSQL($UID);
            $sql = "SELECT Balance FROM `LIBMS_USER` WHERE `UserID`=$UID";
            $result = $this->libms->returnResultSet($sql);
            if ($result) {
                $result = $result[0]['Balance'];
            }
        }
        return $result;
    }

    private function _get_group_title()
    {
        $sql = "
				SELECT g.GroupTitle from LIBMS_GROUP_USER gu
				inner join LIBMS_GROUP g on g.GroupID = gu.GroupID
				where gu.UserID = {$this->userInfo['UserID']}
			";
        $result = $this->libms->returnArray($sql);
        $result = current($result);
        
        return $result['GroupTitle'];
    }

    private function _get_overdue_records()
    {
        $sql = "
			SELECT ol.*, bl.UserID, 
			CONCAT(b.BookTitle, IF(bu.ItemSeriesNum<>'' AND bu.ItemSeriesNum<>'1' AND bu.ItemSeriesNum IS NOT NULL,CONCAT(' ',bu.ItemSeriesNum),'')) as BookTitle,
			bu.BarCode FROM LIBMS_OVERDUE_LOG ol
			inner join LIBMS_BORROW_LOG bl on bl.BorrowLogID = ol.BorrowLogID
			left join LIBMS_BOOK b on b.BookID = bl.BookID 
			left join LIBMS_BOOK_UNIQUE bu on bu.UniqueID = bl.UniqueID
			where bl.userid = {$this->userInfo['UserID']} and ol.RecordStatus='OUTSTANDING'
			";
        $result = $this->libms->returnArray($sql);
        return $result;
    }

    private function _get_borrowed_books()
    {
        $sql = "
		SELECT
			bl.*, bl.AccompanyMaterial as BorrowAccompanyMaterial, 
			if (bu.CirculationTypeCode='' OR bu.CirculationTypeCode is NULL, b.CirculationTypeCode, bu.CirculationTypeCode) as CirculationTypeCode,
			CONCAT(b.`BookTitle`, IF(bu.`ItemSeriesNum`<>'' AND bu.`ItemSeriesNum`<>'1' AND bu.`ItemSeriesNum` IS NOT NULL,CONCAT(' ',bu.`ItemSeriesNum`),'')) as BookTitle,
			if (bu.`RemarkToUser`='' or bu.`RemarkToUser` is null, b.`RemarkToUser`, bu.`RemarkToUser`) as RemarkToUser,
			bu.`AccompanyMaterial`, bu.`UniqueID`, bu.`BarCode`, b.`BookCode`, bu.`LocationCode`, b.`BookSubTitle`
		FROM
			`LIBMS_BORROW_LOG` bl
		JOIN
			`LIBMS_BOOK` b
				ON b.`BookID` = bl.`BookID`
		JOIN
			`LIBMS_BOOK_UNIQUE` bu
				ON bu.`UniqueID` = bl.`UniqueID`
		WHERE
			bl.`UserID`={$this->userInfo['UserID']}
			AND bl.`RecordStatus` ='BORROWED'
		ORDER BY
			BorrowLogID
		";
        
        $result = $this->libms->returnArray($sql);
        
        return $result;
    }

    // get borrow history(borrowing books not included )
    private function _get_borrowed_books_history()
    {
        $sql = "
		SELECT
			bl.*, if (bu.CirculationTypeCode='' OR bu.CirculationTypeCode is NULL, b.CirculationTypeCode, bu.CirculationTypeCode) as CirculationTypeCode,
			CONCAT(b.`BookTitle`, IF(bu.`ItemSeriesNum`<>'' AND bu.`ItemSeriesNum`<>'1' AND bu.`ItemSeriesNum` IS NOT NULL,CONCAT(' ',bu.`ItemSeriesNum`),'')) as BookTitle,
			b.`RemarkToUser`,bu.`UniqueID`, bu.`BarCode`,b.`BookCode`,ol.Payment, ol.PaymentReceived, ol.RecordStatus as PaymentStatus
		FROM
			`LIBMS_BORROW_LOG` bl
		JOIN
			`LIBMS_BOOK` b
				ON b.`BookID` = bl.`BookID`
		JOIN
			`LIBMS_BOOK_UNIQUE` bu
				ON bu.`UniqueID` = bl.`UniqueID`
		LEFT JOIN ( 
                SELECT 
                        v1.`Payment`, 
                        v1.`PaymentReceived`,
                        v1.`RecordStatus`,
                        v1.`BorrowLogID`, 
                        v1.`OverDueLogID`
                FROM
                        `LIBMS_OVERDUE_LOG` v1
                INNER JOIN (
                        SELECT 
                            `BorrowLogID`, 
                            MAX(`OverDueLogID`) AS OverDueLogID
                        FROM
                            `LIBMS_OVERDUE_LOG`
                        WHERE
                            `RecordStatus`<>'DELETE'
                        GROUP BY `BorrowLogID`
                    ) AS v2 ON v2.`OverDueLogID`=v1.`OverDueLogID`
              ) AS ol ON bl.BorrowlogID = ol.BorrowlogID
		WHERE
			bl.`UserID`={$this->userInfo['UserID']}
			AND bl.`RecordStatus` <> 'BORROWED'
		ORDER BY
			bl.BorrowLogID desc
		";
        
        $result = $this->libms->returnArray($sql);
        return $result;
    }

    // pls note item now has 2nd level of circulation type
    private function _get_reserved_books()
    {
        $sql = "SELECT rl.ReservationID,rl.BookID,rl.UserID,rl.ReserveTime,if(rl.ExpiryDate='0000-00-00', '', rl.ExpiryDate),rl.RecordStatus,
						rl.DateModified,rl.LastModifiedBy,rl.BookUniqueID,
						b.`CirculationTypeCode`,b.`BookTitle`,b.`BookCode`,bu.BarCode, b.BookSubTitle
		FROM `LIBMS_RESERVATION_LOG` rl
		JOIN `LIBMS_BOOK` b ON b.`BookID` = rl.`BookID`
		LEFT JOIN `LIBMS_BOOK_UNIQUE` bu ON bu.`UniqueID` = rl.`BookUniqueID`
		WHERE rl.`UserID`={$this->userInfo['UserID']}
		AND rl.`RecordStatus` IN ('WAITING','READY')
		ORDER BY ReservationID ";
        $result = $this->libms->returnArray($sql);
        
        return $result;
    }

    // Henry Modified
    // public function get_reserved_books_UniqueID(){
    // $sql = "SELECT rl.*, b.`CirculationTypeCode`,b.`BookTitle`,b.`BookCode`, bu.UniqueID
    // FROM `LIBMS_RESERVATION_LOG` rl
    // JOIN `LIBMS_BOOK` b ON b.`BookID` = rl.`BookID`
    // JOIN `LIBMS_BOOK_UNIQUE` bu ON b.`BookID` = bu.`BookID`
    // WHERE rl.`UserID`={$this->userInfo['UserID']}
    // AND rl.`RecordStatus` IN ('WAITING','READY')
    // ORDER BY ReservationID ";
    //
    // $result = $this->libms->returnArray($sql);
    //
    // return $result[0]['UniqueID'];
    // }
    public function getGroupTitle()
    {
        $this->userGroupTitle = $this->_get_group_title();
    }

    public function getOverdueRecords()
    {
        $this->overdueRecords = $this->_get_overdue_records();
    }

    public function total_borrowed_book_count($circulationTypeCode = null)
    {
        if (empty($circulationTypeCode)) {
            if (! empty($this->borrowedBooks))
                return count($this->borrowedBooks);
            else
                return 0;
        } else {
            if (! empty($this->borrowedBooks)) {
                $count = 0;
                foreach ($this->borrowedBooks as $borrowedBook) {
                    if ($borrowedBook['CirculationTypeCode'] == $circulationTypeCode)
                        $count ++;
                }
                return $count;
            } else {
                return 0;
            }
        }
    }

    public function total_reserveded_book_count($circulationTypeCode = null)
    {
        
        if (empty($circulationTypeCode)) {
            if (! empty($this->reservedBooks))
                return count($this->reservedBooks);
            else
                return 0;
        } else {
            if (! empty($this->reservedBooks)) {
                $count = 0;
                foreach ($this->reservedBooks as $reservedBook) {
                    if ($reservedBook['CirculationTypeCode'] == $circulationTypeCode)
                        $count ++;
                }
                // $debug_out = " User 141 : CirType:$circulationTypeCode count: $count";
                // dump ($debug_out);
                return $count;
            } else {
                return 0;
            }
        }
    }

    public function borrow_book($uniqueBookID = '', $force = false, $renewalTime = 0, $AccompanyMaterial = 0, $borrowDateTime = '')
    {
        global $Lang, $sys_custom;
        
        if (empty($uniqueBookID))
            return false;
        
        // safe guard: do not allow multiple borrow record for the same book at the same time!
        $sql = "SELECT count(*) FROM LIBMS_BORROW_LOG WHERE RecordStatus='BORROWED' AND UniqueID='{$uniqueBookID}'";
        $rows = $this->libms->returnVector($sql);
        
        // not returned yet
        if ($rows[0] > 0)
            return false;
        
        // dump($uniqueBookID);
        if (! $force && ! $this->rightRuleManager->canBorrow($uniqueBookID))
            return false;
        
        $timeManager = new TimeManager();
        $strToday = ($borrowDateTime ? date('Y-m-d', $borrowDateTime) : date('Y-m-d'));
        
        $BookManager = new BookManager($this->libms);
        $circulationTypeCode = $BookManager->getCirculationTypeCodeFromUniqueBookID($uniqueBookID);
        
        $limits = $this->rightRuleManager->get_limits($circulationTypeCode);
        
        // special customization for a KIS client tbcpk
        if ($sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] != "" && isset($sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'])) {
            $weekday_today = ($borrowDateTime ? date("w", $borrowDateTime) : date("w"));
            if ($weekday_today < $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday']) {
                $daysDifference = $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] - $weekday_today;
            } else {
                $daysDifference = 7 + $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] - $weekday_today;
            }
            
            $limits['ReturnDuration'] = $daysDifference;
        }
        
        if ($this->libms->get_system_setting("circulation_duedate_method")) {
            $dueDate = $timeManager->findOpenDateSchoolDays(strtotime($strToday), $limits['ReturnDuration'], '', $borrowDateTime);
        } else {
            $dueDate = $timeManager->findOpenDate(strtotime("{$strToday} + {$limits['ReturnDuration']} days"), 0, '+1 day', $borrowDateTime);
        }
        
        if (empty($dueDate))
            return false;
        
        $uID = $this->userInfo['UserID'];
        $BookManager = new BookManager($this->libms);
        $bookID = $BookManager->getBookIDFromUniqueBookID($uniqueBookID);
        $uniqueBookRecordStatus = $BookManager->getUniqueBookRecordStatus($uniqueBookID);
        
        $excludeBookUniqueID = '';      // BookUniqueID that's not need to update RecordStatus
        
        if ($force && $uniqueBookRecordStatus == 'RESERVED') {
            
            // check if the reserved item is the item to be borrowed
            $sql = "SELECT ReservationID, BookUniqueID FROM LIBMS_RESERVATION_LOG WHERE BookID='".$bookID."' AND RecordStatus='READY' AND UserID='".$uID."'";
            $currentReservationAry = $this->libms->returnResultSet($sql);
            
            if (count($currentReservationAry)) {    // current user has READY reserved book
                $currentReservationID = $currentReservationAry[0]['ReservationID'];
                $currentReservedBookID = $currentReservationAry[0]['BookUniqueID'];
                
                if ($currentReservedBookID != $uniqueBookID) {      // borrow a copy that's not the reserved one
                    
                    $sql = "SELECT ReservationID FROM LIBMS_RESERVATION_LOG WHERE BookUniqueID='".$uniqueBookID."' AND RecordStatus='READY' AND UserID<>'".$uID."' LIMIT 1";
                    $otherReservationAry = $this->libms->returnResultSet($sql);
                    
                    if (count($otherReservationAry)) {  // swap BookUniqueID in reservation log if the copy to be borrowed is also in READY reserved status by other user
                        $otherReservationID = $otherReservationAry[0]['ReservationID'];
                        $result = $this->recordManager->updateReservation($otherReservationID, array(
                            'BookUniqueID' => $currentReservedBookID
                        ));
                        $excludeBookUniqueID = $currentReservedBookID;
                        $result = $this->recordManager->updateReservation($currentReservationID, array(
                            'BookUniqueID' => $uniqueBookID
                        ));
                    }
                    else {
                        // the copy to be borrowed is in WAITING reserved status
                    }
                }
                else {
                    // the borrow copy is the reserved one
                }
            }
            else {      // current user has no READY reserved book
                
                // check if it is ready to pick by someone revert READY setting
                // 2018-09-21: should exclude user who borrows this book that is reserved and the status is READY
                // it changes the status from READY TO WAITING for other users because the book is forced to borrow by current user
                $reserved = $this->recordManager->getReservationByBookID($bookID, 'READY', 'asc', $uID, false, $uniqueBookID, '', true); // 2013/09/16
    
                if ($reserved) { // Count the number of the book status with ready (Henry)
                    $dataAry['RecordStatus'] = PHPTOSQL('WAITING');
                    $dataAry['ExpiryDate'] = PHPTOSQL('');
                    $this->recordManager->updateReservation($reserved['ReservationID'], $dataAry);
                }
                
                // check if need to send email 2013/9/16
                $result = $this->libms->SELECTFROMTABLE('LIBMS_NOTIFY_SETTING', 'enable', array(
                    'name' => PHPTOSQL('reserve_email')
                ), 1);
                // tolog($result);
                $reserve_email = $result[0][0];
                if (! empty($reserve_email) && $reserved) {
                    $result = $this->libms->SELECTFROMTABLE('LIBMS_BOOK', 'BookTitle', array(
                        'BookID' => $reserved['BookID']
                    ), 1);
                    $title = $result[0][0];
                    $mail_to = array(
                        $reserved['UserID']
                    );
                    $mailSubject = $Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['subject'];
                    $mail_body = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['mail_body'], date("Y-m-d"), $title);
                    do_email($mail_to, $mailSubject, $mail_body);
                }
            }
        }
        
        $result = array();
        
        $dataAry = Array(
            'UserID' => PHPToSQL($uID),
            'BookID' => PHPToSQL($bookID),
            'UniqueID' => PHPToSQL($uniqueBookID),
            'DueDate' => PHPToSQL($dueDate, 'DATE'),
            'RenewalTime' => PHPToSQL($renewalTime),
            'BorrowTime' => ($borrowDateTime ? PHPToSQL(date('Y-m-d H:i:s', $borrowDateTime)) : 'now()'),
            'RecordStatus' => PHPToSQL('BORROWED'),
            'AccompanyMaterial' => PHPToSQL($AccompanyMaterial)
        );
        $this->recordManager->start_Trans();
        $result[] = $this->recordManager->newBorrowLog($dataAry);
        if (empty($renewalTime)) {
            $bookInfo = $this->libms->GET_BOOK_INFO($bookID);
            $NoOfCopyAvailable = count($bookInfo) ? $bookInfo[0]['NoOfCopyAvailable'] : 0;
            if ($NoOfCopyAvailable > 0) { // case when book is "on display" but force to loan, not need to update NoOfCopyAvailable
                $result[] = $this->recordManager->updateBook($bookID, array(
                    'NoOfCopyAvailable' => '`NoOfCopyAvailable` - 1'
                ));
            }
            $result[] = $this->recordManager->updateUnquieBook($uniqueBookID, array(
                'RecordStatus' => PHPToSQL('BORROWED')
            ));
        }
        if (! empty($this->reservedBooks))
            foreach ($this->reservedBooks as $reservation) {
                if ($bookID == $reservation['BookID']) {
                    if ($reservation['BookUniqueID'] != "") {
                        // free the item which was reserved but user borrow another copy
                        $sql = "UPDATE LIBMS_BOOK_UNIQUE SET RecordStatus='NORMAL' WHERE BookID='{$bookID}' AND UniqueID='" . $reservation['BookUniqueID'] . "' AND RecordStatus='RESERVED' ";
                        if ($excludeBookUniqueID) {
                            $sql .= "AND UniqueID<>'".$excludeBookUniqueID."' ";
                        }
                        $this->libms->db_db_query($sql);
                    }
                    $result[] = $this->recordManager->updateReservation($reservation['ReservationID'], array(
                        'RecordStatus' => PHPToSQL('BORROWED')
                    ));
                    
                    // ToDo Yuen
                    break;
                }
            }
        
        // update NoOfCopy and NoOfCopyAvailable to avoid inconsistency
        $result[] = $this->libms->UPDATE_BOOK_COPY_INFO($bookID);
        
        if (! in_array(false, $result)) {
            $this->recordManager->commit_Trans();
            return true;
        } else {
            $this->recordManager->rollback_Trans();
            return false;
        }
    }

    public function renew_book($uniqueBookID = '', $force = false, $renewDateTime = '')
    {
        if (empty($uniqueBookID))
            return false;
        
        if (! $force && ! $this->rightRuleManager->canRenew($uniqueBookID, null, $renewDateTime))
            return false;
        
        $borrowID = $this->recordManager->getBorrowLogIDFromBookUniqueID($uniqueBookID);
        $borrowLog = $this->recordManager->getFieldsFromBorrowLog($borrowID);
        $renewalTime = $borrowLog['RenewalTime'] + 1;
        $this->recordManager->start_Trans();
        $result[] = $this->return_book($uniqueBookID, true, '', 1, $renewDateTime);
        $result[] = $this->borrow_book($uniqueBookID, true, $renewalTime, $borrowLog['AccompanyMaterial'], $renewDateTime);
        
        if (! in_array(false, $result)) {
            $this->recordManager->commit_Trans();
            return true;
        } else {
            $this->recordManager->rollback_Trans();
            return false;
        }
    }

    public function reserve_book($bookID = '', $force = false)
    {
        global $Lang;
        
        if (empty($bookID))
            return false;
        
        if (! $force && ! $this->rightRuleManager->canReserve($bookID, $MSG))
            return false;
        
        // if($force){ // 2013/09/16
        // //check if it is ready to pick by someone revert READY setting
        // $reserved=$this->recordManager->getReservationByBookID($bookID,'READY');
        // if(sizeof($reserved) <= 1){ // Count the number of the book status with ready (Henry)
        // $dataAry['RecordStatus']=PHPTOSQL('WAITING');
        // $dataAry['ExpiryDate']=PHPTOSQL('');
        // $this->recordManager->updateReservation($reserved['ReservationID'], $dataAry);
        // }
        //
        // //check if need to send email
        // $result = $this->libms->SELECTFROMTABLE( 'LIBMS_NOTIFY_SETTING', 'enable', array( 'name' => PHPTOSQL('reserve_email') ),1 );
        // //tolog($result);
        // $reserve_email = $result[0][0];
        // if (!empty($reserve_email)){
        // $result=$this->libms->SELECTFROMTABLE('LIBMS_BOOK', 'BookTitle',array('BookID' => $reserved['BookID']),1);
        // $title = $result[0][0];
        // $mail_to = array($reserved['UserID']);
        // $mailSubject = $Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['subject'] ;
        // $mail_body = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['mail_body'] , $title, $title);
        // do_email($mail_to,$mailSubject,$mail_body);
        // }
        // }
        $reserveStatus = 'WAITING';
        
        $BookManager = new BookManager($this->libms);
        
        $dataAry = Array(
            'UserID' => $this->userInfo['UserID'],
            'BookID' => $bookID,
            'ReserveTime' => 'now()',
            'RecordStatus' => PHPToSQL($reserveStatus)
        );
        $UniqueID = $BookManager->isAvaiable($bookID);
        tolog($UniqueID);
        if ($UniqueID || $force) { // 2013/09/16
                                  // Henry Added
            $day_limit = $this->libms->get_system_setting('max_book_reserved_limit');
            if ($day_limit > 0) {
                
                if ($this->libms->get_system_setting("circulation_duedate_method")) {
                    if ($strToday == "") {
                        $strToday = date("Y-m-d");
                    }
                    $newExpiryDate = $this->timeManager->findOpenDateSchoolDays(strtotime($strToday), $day_limit);
                } else {
                    $newExpiryDate = $this->timeManager->findOpenDate(strtotime('+' . $day_limit . ' day'));
                }
                if ($newExpiryDate == false) {          // case: library is closed for all dates
                    $newExpiryDate = "0000-00-00";
                }
            } else {
                $newExpiryDate = "0000-00-00";
            }
            
            $dataAry['RecordStatus'] = PHPToSQL('READY');
            $dataAry['BookUniqueID'] = PHPToSQL($UniqueID);
            // $dataAry['ExpiryDate'] = PHPToSQL(strtotime('+ 2 week'),'DATE');
            // Henry Modified
            $dataAry['ExpiryDate'] = ($newExpiryDate == "0000-00-00" ? $newExpiryDate : PHPToSQL($newExpiryDate, 'DATE'));
            $result[] = $BookManager->setUniqueBookRecordStatus($UniqueID, 'RESERVED');
            tolog($dataAry);
        }
        // error_log('User.php: 232: $UniqueID: '.var_export($UniqueID,true));
        $result[] = $this->recordManager->newReservation($dataAry);
        
        // dump($result);
        if (! in_array(false, $result)) {
            $this->recordManager->Commit_Trans();
            return true;
        } else {
            $this->recordManager->RollBack_Trans();
            return false;
        }
    }

    public function return_book($uniqueBookID = '', $renew = false, $setBookStatus = '', $handle_overdue = 1, $returnDateTime = '')
    {
        global $Lang, $WRT_ROOT, $intranet_root;
        
        if (! is_int($handle_overdue) && $handle_overdue == "") {
            $handle_overdue = 1;
        }
        
        if (empty($uniqueBookID))
            return false;
        $borrowID = $this->recordManager->getBorrowLogIDFromBookUniqueID($uniqueBookID, 'BORROWED');
        if ($borrowID <= 0) {
            $borrowID = $this->recordManager->getBorrowLogIDFromBookUniqueID($uniqueBookID, 'LOST');
        }
        $borrowInfo = $this->recordManager->getFieldsFromBorrowLog($borrowID);
        $dueDate = strtotime($borrowInfo['DueDate']);
        $today = ($returnDateTime ? $returnDateTime : time());
        // check if overdue
        $this->recordManager->Start_Trans();
        
        $BookManager = new BookManager($this->libms);
        $circulationTypeCode = $BookManager->getCirculationTypeCodeFromUniqueBookID($uniqueBookID);
        
        // Return at the same day Checking
        $uID = $this->userInfo['UserID'];
        $BorrowDate = strtotime($borrowInfo['BorrowTime']);
        
        // Checkbox Checked -> not allow to return book at the same day && not in admin mode (search by user)
        if ($this->rightRuleManager->isSameDayReturnNotAllowed($uID) && ! $this->AdminMode) {
            if (date('Y-m-d', $BorrowDate) == date('Y-m-d', $today)) {
                return - 2;
            }
        }
        if ($today - $dueDate > 60 * 60 * 24) {
            
            if (! $handle_overdue) {
                return - 1;
            }
            
            $penaltyday = $this->timeManager->dayForPenalty($dueDate, $today);
            // error_log("User : 260 : duetime= $dueDate , nowtime = $today , penaltyday = $penaltyday");
            if ($penaltyday > 0) {
                
                // $circulationTypeCode = $BookManager->getCirculationTypeCodeFromBookID($borrowInfo['BookID']);
                
                $limits = $this->rightRuleManager->get_limits($circulationTypeCode);
                $amount = $limits['OverDueCharge'] * $penaltyday;
                if ($limits['MaxFine'] > 0) {
                    $amount = ($limits['MaxFine'] < $amount) ? $limits['MaxFine'] : $amount;
                }
                if ($amount > 0) {
                    $dataAry = array(
                        'BorrowLogID' => PHPToSQL($borrowID),
                        'DaysCount' => PHPToSQL($penaltyday),
                        'Payment' => PHPToSQL($amount),
                        'RecordStatus' => PHPToSQL('OUTSTANDING')
                    );
                    $result[] = $this->recordManager->newOverDueLog($dataAry);
                    
                    // log transaction for tracing error
                    $balanceLogAry = array();
                    $balanceLogAry['UserID'] = PHPToSQL($borrowInfo['UserID']);
                    $balanceLogAry['BalanceBefore'] = PHPToSQL($this->_get_latest_balance($borrowInfo['UserID']));
                    
                    $result[] = $this->recordManager->addToUserBalance($borrowInfo['UserID'], 0 - $amount);
                    
                    $balanceLogAry['TransAmount'] = PHPToSQL(0 - $amount);
                    $balanceLogAry['BalanceAfter'] = PHPToSQL($this->_get_latest_balance($borrowInfo['UserID']));
                    $balanceLogAry['TransDesc'] = PHPToSQL('return book');
                    $balanceLogAry['TransType'] = PHPToSQL('BorrowLog');
                    $balanceLogAry['TransRefID'] = PHPToSQL($borrowID);
                    $this->libms->INSERT2TABLE('LIBMS_BALANCE_LOG', $balanceLogAry);
                    unset($balanceLogAry);
                }
            }
        }
        // return process
        if ($renew) {
            $dataAry = array(
                'RecordStatus' => PHPToSQL('RENEWED'),
                'ReturnedTime' => ($returnDateTime ? PHPToSQL(date('Y-m-d H:i:s', $returnDateTime)) : 'now()')
            );
        } else {
            
            // get RecordStatus in Book Unique
            $bookInfo = $BookManager->getBookFields($borrowInfo['BookID'], "BookID", $uniqueBookID);
            if ($bookInfo && $bookInfo['RecordStatus'] == 'WRITEOFF') {
                // do nothing for WRITEOFF
            } else {
                // Added the parameter "asc" (Henry)
                $reserve_row = $this->recordManager->getReservationByBookID($borrowInfo['BookID'], array(
                    'WAITING'
                ), "asc", '', false, $uniqueBookID, $circulationTypeCode);
                // tolog($reserve_row);
                if (! empty($reserve_row)) {
                    
                    // Henry Added
                    $day_limit = $this->libms->get_system_setting('max_book_reserved_limit');
                    if ($day_limit > 0) {
                        
                        if ($this->libms->get_system_setting("circulation_duedate_method")) {
                            if ($strToday == "") {
                                $strToday = date("Y-m-d");
                            }
                            $newExpiryDate = date('Y-m-d', $this->timeManager->findOpenDateSchoolDays(strtotime($strToday), $day_limit, '', $returnDateTime));
                        } else {
                            $newExpiryDate = date('Y-m-d', $this->timeManager->findOpenDate(strtotime('+' . $day_limit . ' day'), 0, '+1 day', $returnDateTime));
                        }
                    } 
                    else
                        $newExpiryDate = "0000-00-00";
                    
                    // find the user allow to borrow the book comparing CirculationType
                    
                    $result[] = $this->recordManager->updateUnquieBook($uniqueBookID, array(
                        'RecordStatus' => PHPTOSQL('RESERVED')
                    ));
                    
                    $result[] = $this->recordManager->updateReservation($reserve_row['ReservationID'], array(
                        'RecordStatus' => PHPToSQL('READY'),
                        'BookUniqueID' => PHPToSQL($uniqueBookID),
                        // 'ExpiryDate' => PHPToSQL(date('Y-m-d',strtotime('+2 week')))
                        // Henry Modified
                        'ExpiryDate' => PHPToSQL($newExpiryDate)
                    ));
                    // check if need to send email
                    $result = $this->libms->SELECTFROMTABLE('LIBMS_NOTIFY_SETTING', 'enable', array(
                        'name' => PHPTOSQL('reserve_email')
                    ), 1);
                    // tolog($result);
                    $reserve_email = $result[0][0];
                    if (! empty($reserve_email)) {
                        $result = $this->libms->SELECTFROMTABLE('LIBMS_BOOK', 'BookTitle', array(
                            'BookID' => $reserve_row['BookID']
                        ), 1);
                        $title = $result[0][0];
                        $mail_to = array(
                            $reserve_row['UserID']
                        );
                        $mailSubject = $Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'];
                        $mail_body = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'], $title);
                        $mail_body_with_due_date = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body_with_due_date'], $title, $newExpiryDate);
                        if ($newExpiryDate == 0)
                            do_email($mail_to, $mailSubject, $mail_body);
                        else
                            do_email($mail_to, $mailSubject, $mail_body_with_due_date);
                    }
                    // Henry Added to increase the number of Available no matter the book is reserved
                    $result[] = $this->recordManager->updateBook($borrowInfo['BookID'], array(
                        'NoOfCopyAvailable' => '`NoOfCopyAvailable` + 1'
                    ));
                } else {
                    if (empty($setBookStatus)) {
                        $setBookStatus = PHPToSQL('NORMAL');
                    }
                    
                    $result[] = $this->recordManager->updateBook($borrowInfo['BookID'], array(
                        'NoOfCopyAvailable' => '`NoOfCopyAvailable` + 1'
                    ));
                    $result[] = $this->recordManager->updateUnquieBook($uniqueBookID, array(
                        'RecordStatus' => $setBookStatus
                    ));
                }
            } // end not WRITEOFF
            
            $dataAry = array(
                'RecordStatus' => PHPToSQL('RETURNED'),
                'ReturnedTime' => ($returnDateTime ? PHPToSQL(date('Y-m-d H:i:s', $returnDateTime)) : 'now()')
            );
        }
        $result[] = $this->recordManager->updateBorrowLog($borrowID, $dataAry);
        
        // update NoOfCopy and NoOfCopyAvailable to avoid inconsistency
        $result[] = $this->libms->UPDATE_BOOK_COPY_INFO($borrowInfo['BookID']);
        
        ## check if push message of reminder is set in schedule, remove it from schedule if it covers this book only,
        ## remove the book from message content if it covers other books.
        $notifyFieldAry = array('NotifyMessageTargetID', 'OverdueNotifyMessageTargetID');
        foreach($notifyFieldAry as $notifyField) {
            if ($borrowInfo[$notifyField]) {
                if ($this->isNotifyScheduleExpired($borrowInfo[$notifyField])) {     // notify message hasn't been sent when return book
                    if ($this->isNotifyMessageIncludeOtherBooks($borrowID, $notifyField)) {       // notify message include other books
                        $newNotifyContent = $this->getNotifyContentOfBookToBeOverdue($borrowInfo[$notifyField], $notifyField, $borrowInfo['UserID']);
                        $result[] = $this->updateNotifyMessageContent($borrowInfo[$notifyField], $newNotifyContent);
                    }
                    else {
                        if ($this->isOnlyOneNotifyMessageTarget($borrowInfo[$notifyField])) {        // only one target, cancel the message
                            $notifyMessageID = $this->getNotifyMessageID($borrowInfo[$notifyField]);
                            $PATH_WRT_ROOT = $WRT_ROOT;
                            include_once($WRT_ROOT."includes/eClassApp/libeClassApp.php");
                            $libeClassApp = new libeClassApp();
                            $result[] = $libeClassApp->deleteScheduledPushMessageInServer(array($notifyMessageID));
                        }
                        else {      // include other target user, delete this target user and response
                            // store log before delete
                            include_once($WRT_ROOT."includes/liblog.php");
                            $liblog = new liblog();

                            $messageTargetAry = $this->getNotifyMessageTarget($borrowInfo[$notifyField]);
                            $detailAry = array();
                            foreach((array)$messageTargetAry as $k=>$v) {
                                $detailAry[] = $k."^~".$v;
                            }
                            $result[] = $liblog->INSERT_LOG('eLib+', 'Delete_Parent_Push_Notification_Overdue', implode("<br>",$detailAry), 'INTRANET_APP_NOTIFY_MESSAGE_TARGET', $borrowInfo[$notifyField]);

                            $messageReferenceAry = $this->getNotifyMessageReference($borrowInfo[$notifyField]);
                            $detailAry = array();
                            foreach((array)$messageReferenceAry as $k=>$v) {
                                $detailAry[] = $k."^~".$v;
                            }
                            $recordID = count($messageReferenceAry) ? $messageReferenceAry['RecordID'] : 0;
                            $liblog = new liblog();
                            $result[] = $liblog->INSERT_LOG('eLib+', 'Delete_Parent_Push_Notification_Overdue', implode("<br>",$detailAry), 'INTRANET_APP_NOTIFY_MESSAGE_REFERENCE', $recordID);

                            $result[] = $this->deleteNotificationTargetAndReference($borrowInfo[$notifyField]);

                        }
                    }
                }

                // empty $notifyField in borrow log
                $dataAry = array(
                    $notifyField => 0
                );
                $result[] = $this->recordManager->updateBorrowLog($borrowID, $dataAry);
            }
        }

        if (! in_array(false, $result)) {
            $this->recordManager->Commit_Trans();
            return true;
        } else {
            $this->recordManager->RollBack_Trans();
            return false;
        }
    }

    public function pay_overdue($amount = '', $overdueLogID = '', $payBy_ePayment = false)
    {
        if (! isset($amount) || empty($overdueLogID)) {
            return false;
        }
        
        $paymentInfo = $this->recordManager->getOverDuePaymentInfo($overdueLogID);
        if ($paymentInfo['RecordStatus'] == "SETTLED" || $paymentInfo['RecordStatus'] == "WAIVED") {
            return true;
        }
        $paymentInfo['PaymentReceived'] = floatval($paymentInfo['PaymentReceived']);
        $paymentInfo['Payment'] = floatval($paymentInfo['Payment']);
        
        $transAmountBefore = floatval($paymentInfo['Payment']) - floatval($paymentInfo['PaymentReceived']);
        $transAmountAfter = $transAmountBefore - floatval($amount);
        
        $dataAry['PaymentReceived'] = $paymentInfo['PaymentReceived'] + floatval($amount);
        $diff = $dataAry['PaymentReceived'] - $paymentInfo['Payment'];
        
        if ($diff >= 0 || abs($diff) <= 0.005) {
            $dataAry['RecordStatus'] = PHPToSQL('SETTLED');
        }
        
        $this->recordManager->Start_Trans();
        
        if ($payBy_ePayment) {
            $overDueDetail = $this->recordManager->getOverDueRecords($paymentInfo['UserID'], array(
                'OverDueLogID' => $overdueLogID
            ));
            if (count($overDueDetail) > 0) {
                $lostOrOverdue = trim($overDueDetail[0]['DaysCount']) == '' ? 'Lost' : 'Overdue ' . $overDueDetail[0]['DaysCount'] . ' day(s)';
                $details = $overDueDetail[0]['BookTitle'] . ' [' . $overDueDetail[0]['ACNO'] . '] (' . $lostOrOverdue . ')';
            }
            
            $logID = $this->recordManager->addOverduePaymentRecord($paymentInfo['UserID'], $amount, $details);
            if ($logID === false) {
                $this->recordManager->RollBack_Trans();
                return false;
            }
            $dataAry['PaymentMethod'] = PHPToSQL('ePayment');
            $dataAry['PaymentLogID'] = $logID;
        }
        
        // log transaction for tracing error
        $balanceLogAry = array();
        $balanceLogAry['UserID'] = PHPToSQL($paymentInfo['UserID']);
        $balanceLogAry['BalanceBefore'] = PHPToSQL($this->_get_latest_balance($paymentInfo['UserID']));
        
        $result['addToUserBalance'] = $this->recordManager->addToUserBalance($paymentInfo['UserID'], $amount);
        
        $balanceLogAry['TransAmount'] = PHPToSQL($amount);
        $balanceLogAry['BalanceAfter'] = PHPToSQL($this->_get_latest_balance($paymentInfo['UserID']));
        $balanceLogAry['TransDesc'] = PHPToSQL('pay overdue');
        $balanceLogAry['TransType'] = PHPToSQL('OverdueLog');
        $balanceLogAry['TransRefID'] = PHPToSQL($overdueLogID);
        $balanceLogAry['TransAmountBefore'] = PHPToSQL($transAmountBefore);
        $balanceLogAry['TransAmountAfter'] = PHPToSQL($transAmountAfter);
        $this->libms->INSERT2TABLE('LIBMS_BALANCE_LOG', $balanceLogAry);
        unset($balanceLogAry);
        
        $result['updateOverDueLog'] = $this->recordManager->updateOverDueLog($overdueLogID, $dataAry);
        unset($dataAry);
        $dataAry2['Amount'] = PHPToSQL(floatval($amount), 'FLOAT');
        $dataAry2['UserID'] = $this->userInfo['UserID'];
        $dataAry2['OverDueLogID'] = $overdueLogID;
        
        $result['newTransaction'] = $this->recordManager->newTransaction($dataAry2);
        unset($dataAry2);
        
        if (! in_array(false, $result)) {
            $this->recordManager->Commit_Trans();
            return true;
        } else {
            $this->recordManager->RollBack_Trans();
            return false;
        }
    }

    public function wavie_overdue($overdueLogID = '')
    {
        if (empty($overdueLogID)) {
            return false;
        }
        
        $paymentInfo = $this->recordManager->getOverDuePaymentInfo($overdueLogID);
        $dataAry['RecordStatus'] = PHPToSQL('WAIVED');
        $this->recordManager->Start_Trans();
        $still_owe = $paymentInfo['Payment'] - $paymentInfo['PaymentReceived'];
        
        $transAmountBefore = $still_owe;
        $transAmountAfter = 0;
        
        // log transaction for tracing error
        $balanceLogAry = array();
        $balanceLogAry['UserID'] = PHPToSQL($paymentInfo['UserID']);
        $balanceLogAry['BalanceBefore'] = PHPToSQL($this->_get_latest_balance($paymentInfo['UserID']));
        
        $result[] = $this->recordManager->addToUserBalance($paymentInfo['UserID'], $still_owe);
        
        $balanceLogAry['TransAmount'] = PHPToSQL($still_owe);
        $balanceLogAry['BalanceAfter'] = PHPToSQL($this->_get_latest_balance($paymentInfo['UserID']));
        $balanceLogAry['TransDesc'] = PHPToSQL('waive overdue');
        $balanceLogAry['TransType'] = PHPToSQL('OverdueLog');
        $balanceLogAry['TransRefID'] = PHPToSQL($overdueLogID);
        $balanceLogAry['TransAmountBefore'] = PHPToSQL($transAmountBefore);
        $balanceLogAry['TransAmountAfter'] = PHPToSQL($transAmountAfter);
        $this->libms->INSERT2TABLE('LIBMS_BALANCE_LOG', $balanceLogAry);
        unset($balanceLogAry);
        
        $result[] = $this->recordManager->updateOverDueLog($overdueLogID, $dataAry);
        unset($dataAry);
        
        if (! in_array(false, $result)) {
            $this->recordManager->Commit_Trans();
            return true;
        } else {
            $this->recordManager->RollBack_Trans();
            return false;
        }
    }

    public function loss_a_book($uniqueBookID, $manualPenaltyFee = null, $returnDateTime = '')
    {
        if (empty($uniqueBookID)) {
            return false;
        }
        $borrowID = $this->recordManager->getBorrowLogIDFromBookUniqueID($uniqueBookID);
        $borrowInfo = $this->recordManager->getFieldsFromBorrowLog($borrowID);
        $BookManager = new BookManager($this->libms);
        $bookInfo = $BookManager->getBookFields($borrowInfo['BookID'], "*", $uniqueBookID);
        
        $dueDate = strtotime($borrowInfo['DueDate']);
        $today = ($returnDateTime ? $returnDateTime : time());
        if ($today - $dueDate > 60 * 60 * 24) {
            if($this->libms->get_system_setting('circulation_lost_book_overdue_charge')){
            $penaltyday = $this->timeManager->dayForPenalty($dueDate, $today);
            // error_log("User : 260 : duetime= $dueDate , nowtime = $today , penaltyday = $penaltyday");
            if ($penaltyday > 0) {
        		$circulationTypeCode = $BookManager->getCirculationTypeCodeFromUniqueBookID($uniqueBookID);
                $limits = $this->rightRuleManager->get_limits($circulationTypeCode);
                $amount = $limits['OverDueCharge'] * $penaltyday;
                if ($limits['MaxFine'] > 0) {
                    $amount = ($limits['MaxFine'] < $amount) ? $limits['MaxFine'] : $amount;
                }
                if ($amount > 0) {
                    $dataAry = array(
                        'BorrowLogID' => PHPToSQL($borrowID),
                        'DaysCount' => PHPToSQL($penaltyday),
                        'Payment' => PHPToSQL($amount),
                        'RecordStatus' => PHPToSQL('OUTSTANDING')
                    );
                    $result[] = $this->recordManager->newOverDueLog($dataAry);
                    
                    // log transaction for tracing error
                    $balanceLogAry = array();
                    $balanceLogAry['UserID'] = PHPToSQL($borrowInfo['UserID']);
                    $balanceLogAry['BalanceBefore'] = PHPToSQL($this->_get_latest_balance($borrowInfo['UserID']));
                    
                    $result[] = $this->recordManager->addToUserBalance($borrowInfo['UserID'], 0 - $amount);
                    
                    $balanceLogAry['TransAmount'] = PHPToSQL(0 - $amount);
                    $balanceLogAry['BalanceAfter'] = PHPToSQL($this->_get_latest_balance($borrowInfo['UserID']));
                    $balanceLogAry['TransDesc'] = PHPToSQL('return book');
                    $balanceLogAry['TransType'] = PHPToSQL('BorrowLog');
                    $balanceLogAry['TransRefID'] = PHPToSQL($borrowID);
                    $this->libms->INSERT2TABLE('LIBMS_BALANCE_LOG', $balanceLogAry);
                    unset($balanceLogAry);
                }
            }
            }
        }
        
        $lost_book_price_by = $this->libms->get_system_setting('circulation_lost_book_price_by');
        $additional_charge_by = $this->libms->get_system_setting('circulation_lost_book_additional_charge');
        
        switch ($lost_book_price_by) {
            case 1: // list price
                $compensationalPrice = ($bookInfo['ListPrice'] != '') ? $bookInfo['ListPrice'] : $bookInfo['PurchasePrice'];
                break;
            case 2: // fixed price
                $compensationalPrice = $this->libms->get_system_setting('circulation_lost_book_price');
                break;
            case 0: // purchase price
            default:
                $compensationalPrice = ($bookInfo['PurchasePrice'] != '') ? $bookInfo['PurchasePrice'] : $bookInfo['ListPrice'];
                break;
        }
        
        $compensationalPrice = preg_replace("/[^0-9\.]/", '', $compensationalPrice);
        
        if ($additional_charge_by > 0) {
            if ($additional_charge_by == 1) {
                $additional_charge_amount = $this->libms->get_system_setting('circulation_lost_book_additional_charge_fixed');
            } else if ($additional_charge_by == 2) {
                $ratio = $this->libms->get_system_setting('circulation_lost_book_additional_charge_ratio');
                $additional_charge_amount = round($compensationalPrice * ($ratio / 100), 2);
            } else { // should be error
                $additional_charge_amount = 0;
            }
            $compensationalPrice += $additional_charge_amount;
        } else {
            $additional_charge_amount = 0;
        }
        
        $bookPrice = is_null($manualPenaltyFee) ? $compensationalPrice : $manualPenaltyFee;
        $bookPrice = get_number_from_text($bookPrice);
        // $bookPrice = is_null($manualPenaltyFee)?$bookInfo['PurchasePrice']: $manualPenaltyFee;
        // # in case, only list price is input
        // if ($bookPrice=="" && $bookInfo['ListPrice']!="")
        // {
        // $bookPrice = $bookInfo['ListPrice'];
        // }
        
        $this->recordManager->Start_Trans();
        if ($bookPrice < 0 || $bookPrice == "") {
            $bookPrice = 0;
        }
        if ($bookPrice >= 0) {
            $dataAry = array(
                'BorrowLogID' => PHPToSQL($borrowID),
                'Payment' => PHPToSQL($bookPrice),
                'LostBookPriceBy' => PHPToSQL($lost_book_price_by),
                'AdditionalChargeBy' => PHPToSQL($additional_charge_by),
                'AdditionalCharge' => PHPToSQL($additional_charge_amount),
                'RecordStatus' => PHPToSQL('OUTSTANDING')
            );
            $result[] = $this->recordManager->newOverDueLog($dataAry);
            unset($dataAry);
            
            // log transaction for tracing error
            $balanceLogAry = array();
            $balanceLogAry['UserID'] = PHPToSQL($borrowInfo['UserID']);
            $balanceLogAry['BalanceBefore'] = PHPToSQL($this->_get_latest_balance($borrowInfo['UserID']));
            
            $result[] = $this->recordManager->addToUserBalance($borrowInfo['UserID'], 0 - $bookPrice);
            
            $balanceLogAry['TransAmount'] = PHPToSQL(0 - $bookPrice);
            $balanceLogAry['BalanceAfter'] = PHPToSQL($this->_get_latest_balance($borrowInfo['UserID']));
            $balanceLogAry['TransDesc'] = PHPToSQL('lost book');
            $balanceLogAry['TransType'] = PHPToSQL('BorrowLog');
            $balanceLogAry['TransRefID'] = PHPToSQL($borrowID);
            $this->libms->INSERT2TABLE('LIBMS_BALANCE_LOG', $balanceLogAry);
            unset($balanceLogAry);
        }
        $result[] = $this->recordManager->updateUnquieBook($uniqueBookID, array(
            'RecordStatus' => PHPToSQL('LOST')
        ));
        $result[] = $this->recordManager->updateBook($borrowInfo['BookID'], array(
            'NoOfCopy' => '`NoOfCopy` - 1'
        ));
        
        $dataAry['RecordStatus'] = PHPToSQL('LOST');
        $result[] = $this->recordManager->updateBorrowLog($borrowID, $dataAry);
        
        unset($dataAry);
        if (! in_array(false, $result)) {
            $this->recordManager->Commit_Trans();
            $libel = new elibrary();
            $libel->SYNC_PHYSICAL_BOOK_TO_ELIB($borrowInfo['BookID']);
            return true;
        } else {
            $this->recordManager->RollBack_Trans();
            return false;
        }
    }

    public function cancel_overdue($amount = '', $overdueLogID = '', $payBy_ePayment = false)
    {
        if (! isset($amount) || empty($overdueLogID)) {
            return false;
        }
        
        $paymentInfo = $this->recordManager->getOverDuePaymentInfo($overdueLogID);
        
        $paymentInfo['PaymentReceived'] = floatval($paymentInfo['PaymentReceived']);
        $paymentInfo['Payment'] = floatval($paymentInfo['Payment']);

        $transAmountBefore = floatval($paymentInfo['Payment']) - floatval($paymentInfo['PaymentReceived']);
        $transAmountAfter = $transAmountBefore + floatval($amount);
        
        $dataAry['PaymentReceived'] = $paymentInfo['PaymentReceived'] - floatval($amount);
        
        $dataAry['RecordStatus'] = PHPToSQL('OUTSTANDING');
        
        $this->recordManager->Start_Trans();
        
        if ($payBy_ePayment) {
            $overDueDetail = $this->recordManager->getOverDueRecords($paymentInfo['UserID'], array(
                'OverDueLogID' => $overdueLogID
            ));
            if (count($overDueDetail) > 0) {
                $lostOrOverdue = trim($overDueDetail[0]['DaysCount']) == '' ? 'Lost' : 'Overdue ' . $overDueDetail[0]['DaysCount'] . ' day(s)';
                $details = $overDueDetail[0]['BookTitle'] . ' [' . $overDueDetail[0]['ACNO'] . '] (' . $lostOrOverdue . ')';
            }
            
            $logID = $this->recordManager->addCancelOverduePaymentRecord($paymentInfo['UserID'], $amount, $details);
            if ($logID === false) {
                $this->recordManager->RollBack_Trans();
                return false;
            }
            $dataAry['PaymentMethod'] = PHPToSQL('ePayment');
            $dataAry['PaymentLogID'] = $logID;
        }
        
        // log transaction for tracing error
        $balanceLogAry = array();
        $balanceLogAry['UserID'] = PHPToSQL($paymentInfo['UserID']);
        $balanceLogAry['BalanceBefore'] = PHPToSQL($this->_get_latest_balance($paymentInfo['UserID']));
        
        $result['addToUserBalance'] = $this->recordManager->addToUserBalance($paymentInfo['UserID'], - $amount);
        
        $balanceLogAry['TransAmount'] = PHPToSQL(- $amount);
        $balanceLogAry['BalanceAfter'] = PHPToSQL($this->_get_latest_balance($paymentInfo['UserID']));
        $balanceLogAry['TransDesc'] = PHPToSQL('cancel overdue');
        $balanceLogAry['TransType'] = PHPToSQL('OverdueLog');
        $balanceLogAry['TransRefID'] = PHPToSQL($overdueLogID);
        $balanceLogAry['TransAmountBefore'] = PHPToSQL($transAmountBefore);
        $balanceLogAry['TransAmountAfter'] = PHPToSQL($transAmountAfter);
        $this->libms->INSERT2TABLE('LIBMS_BALANCE_LOG', $balanceLogAry);
        unset($balanceLogAry);
        
        $result['updateOverDueLog'] = $this->recordManager->updateOverDueLog($overdueLogID, $dataAry);
        unset($dataAry);
        
        $dataAry2['Amount'] = PHPToSQL(- floatval($amount), 'FLOAT');
        $dataAry2['UserID'] = $this->userInfo['UserID'];
        $dataAry2['OverDueLogID'] = $overdueLogID;
        
        $result['newTransaction'] = $this->recordManager->newTransaction($dataAry2);
        unset($dataAry2);
        
        if (! in_array(false, $result)) {
            $this->recordManager->Commit_Trans();
            return true;
        } else {
            $this->recordManager->RollBack_Trans();
            return false;
        }
    }

    /*
     * set LIBMS_OVERDUE_LOG.RecordStatus to 'SETTLED' and add a transaction to LIBMS_TRANSACTION with zero amount
     */
    public function settle_zero_overdue($overdueLogID = '')
    {
        if (empty($overdueLogID)) {
            return false;
        }
        
        $paymentInfo = $this->recordManager->getOverDuePaymentInfo($overdueLogID);
        if ($paymentInfo['RecordStatus'] == "SETTLED" || $paymentInfo['RecordStatus'] == "WAIVED") {
            return true;
        }
        $paymentInfo['Payment'] = floatval($paymentInfo['Payment']);
        if ($paymentInfo['Payment'] != 0) {
            return false;
        }
        $dataAry['RecordStatus'] = PHPToSQL('SETTLED');
        
        $this->recordManager->Start_Trans();
        $result['updateOverDueLog'] = $this->recordManager->updateOverDueLog($overdueLogID, $dataAry);
        unset($dataAry);
        $dataAry2['Amount'] = PHPToSQL(0, 'FLOAT');
        $dataAry2['UserID'] = $this->userInfo['UserID'];
        $dataAry2['OverDueLogID'] = $overdueLogID;
        
        $result['newTransaction'] = $this->recordManager->newTransaction($dataAry2);
        unset($dataAry2);
        
        if (! in_array(false, $result)) {
            $this->recordManager->Commit_Trans();
            return true;
        } else {
            $this->recordManager->RollBack_Trans();
            return false;
        }
    }
    
    public function getLatestBalance($userID)
    {
        return $this->_get_latest_balance($userID);
    }

    ## check if other borrow book exist except $borrowLogID for the same $notifyField
    public function isNotifyMessageIncludeOtherBooks($borrowLogID, $notifyField)
    {
        $sql = "SELECT
                        a.BorrowLogID
                FROM
                        LIBMS_BORROW_LOG a
                        INNER JOIN LIBMS_BORROW_LOG b ON b.".$notifyField."=a.".$notifyField."
                WHERE
                        b.BorrowLogID=".PHPToSQL($borrowLogID)."
                        AND a.BorrowLogID<>b.BorrowLogID";
        $rs = $this->libms->returnResultSet($sql);
        return count($rs) ? true : false;
    }

    ## check if the notify message has been expired or not compare to current time
    public function isNotifyScheduleExpired($notifyMessageTargetID)
    {
        global $intranet_db;

        $sql = "SELECT
                        m.NotifyMessageID
                FROM
                        ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_TARGET t
                        INNER JOIN ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE m ON m.NotifyMessageID=t.NotifyMessageID
                WHERE
                        m.NotifyDateTime >= NOW()
                        AND t.NotifyMessageTargetID=".PHPToSQL($notifyMessageTargetID);
        $rs = $this->libms->returnResultSet($sql);
        return count($rs) ? true : false;
    }

    public function getNotifyContentOfBookToBeOverdue($notifyMessageTargetID, $notifyField, $userID)
    {
        global $Lang;

        if ($notifyField == 'NotifyMessageTargetID') {
            $heading = $Lang['libms']['daily']['push_message']['return_date']['content'];
        }
        else {
            $heading = $Lang['libms']['daily']['push_message']['overdue']['content'];
        }
        $sql = "SELECT
                        b.BookTitle,
                        r.DueDate
                FROM
                        LIBMS_BORROW_LOG r
                        INNER JOIN LIBMS_BOOK b ON b.BookID=r.BookID
                WHERE
                        r.".$notifyField."=".PHPToSQL($notifyMessageTargetID)."
                        AND r.RecordStatus = 'BORROWED'
                        AND r.UserID=".PHPToSQL($userID);

        $rs = $this->libms->returnResultSet($sql);

        $messageContent = $heading. '
			';

        $nrBooks = count($rs);
        for ($i=0; $i<$nrBooks; $i++) {
            $messageContent .= '
				' . ($i + 1) . '. ' . $rs[$i]['BookTitle'] . ' (' . $rs[$i]['DueDate'] . ')';
        }
        return $messageContent;
    }

    public function updateNotifyMessageContent($notifyMessageTargetID, $messageContent)
    {
        global $intranet_db;

        $sql = "UPDATE ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_TARGET SET MessageContent=".PHPToSQL($messageContent)." WHERE NotifyMessageTargetID=".PHPToSQL($notifyMessageTargetID);

        $result = $this->libms->db_db_query($sql);
        return $result;
    }

    public function isOnlyOneNotifyMessageTarget($notifyMessageTargetID)
    {
        global $intranet_db;

        $sql = "SELECT
                        COUNT(DISTINCT a.NotifyMessageTargetID) AS NrTarget
                FROM (
                        SELECT
                            NotifyMessageTargetID, NotifyMessageID
                        FROM
                            ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_TARGET
                     ) AS a
                     INNER JOIN ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_TARGET b ON b.NotifyMessageID=a.NotifyMessageID
                WHERE
                        b.NotifyMessageTargetID=".PHPToSQL($notifyMessageTargetID);
        $rs = $this->libms->returnResultSet($sql);

        return count($rs) ? (($rs[0]['NrTarget'] == 1) ? true : false) : false;
    }

    public function getNotifyMessageID($notifyMessageTargetID)
    {
        global $intranet_db;

        $sql = "SELECT
                    NotifyMessageID
            FROM
                    ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_TARGET
            WHERE
                    NotifyMessageTargetID=".PHPToSQL($notifyMessageTargetID);
        $rs = $this->libms->returnResultSet($sql);
        return count($rs) ? $rs[0]['NotifyMessageID'] : '';
    }

    public function getNotifyMessageTarget($notifyMessageTargetID)
    {
        global $intranet_db;

        $sql = "SELECT
                    *
                FROM
                        ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_TARGET
                WHERE
                        NotifyMessageTargetID=".PHPToSQL($notifyMessageTargetID);
        $rs = $this->libms->returnResultSet($sql);
        return count($rs) ? $rs[0] : '';
    }

    public function getNotifyMessageReference($notifyMessageTargetID)
    {
        global $intranet_db;

        $sql = "SELECT
                    *
                FROM
                        ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_REFERENCE
                WHERE
                        NotifyMessageTargetID=".PHPToSQL($notifyMessageTargetID);
        $rs = $this->libms->returnResultSet($sql);
        return count($rs) ? $rs[0] : '';
    }

    public function deleteNotificationTargetAndReference($notifyMessageTargetID)
    {
        global $intranet_db;

        $result = array();
        $sql = "DELETE FROM ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_TARGET WHERE NotifyMessageTargetID=".PHPToSQL($notifyMessageTargetID);
        $result[] = $this->libms->db_db_query($sql);

        $sql = "DELETE FROM ".$intranet_db.".INTRANET_APP_NOTIFY_MESSAGE_REFERENCE WHERE NotifyMessageTargetID=".PHPToSQL($notifyMessageTargetID);
        $result[] = $this->libms->db_db_query($sql);

        return !in_array(false,$result) ? true : false;
    }

}

?>