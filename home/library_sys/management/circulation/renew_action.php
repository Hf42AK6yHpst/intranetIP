<?php
// Modifying by:

/*
 * ########### Change Log Start ###############
 *
 * 20180222 Cameron
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 20170801 Cameron
 * - fix bug: should initialize $result, otherwize it causes "wrong datatype for second argument in ..." error when used in PowerClass
 *
 * 20170314 Cameron: add IP checking for circulation
 *
 * 20160219 Cameron: check if $uID exist before process
 *
 * 20151105 Cameron: cancel passing AccompanyMaterial to renew_book() because it should be retrieved from borrow log
 *
 * 20151013 Cameron: add AccompanyMaterial when renew books
 *
 * 20150722 Henry: apply class management permission
 *
 * ########### Change Log End ###############
 *
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);
// ###### END OF Trim all request #######
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once ('User.php');
include_once ('RecordManager.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');
include_once ('TimeManager.php');
include_once ('User.php');
intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);

$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['renew'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}
$bookUniqieIDs = $_REQUEST['renew'];

// $interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "RENEW";

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];
intranet_opendb();
$libms = new liblms();
$result = array();
if ($uID) {
    $User = new User($uID, $libms);
    // $BookManager = new BookManager($libms);
    
    foreach ($bookUniqieIDs as $bookUniqieID) {
        if (! empty($bookUniqieID)) {
            // $AccompanyMaterial = ($_REQUEST['AccomMat_'.$bookUniqieID] == 'checked') ? 1 : 0;
            $result[$bookUniqieID] = $User->renew_book($bookUniqieID, true);
        }
    }
} else {
    $result[] = false;
}

$error = in_array(false, $result);
if ($error)
    $_SESSION['LIBMS']['xmsg'] = $Lang['libms']['Circulation']['action_fail'];
else
    $_SESSION['LIBMS']['xmsg'] = $Lang['libms']['Circulation']['action_success'];

if ($uID) {
    $recordManager = new RecordManager($libms);
    $cond[] = "ol.`RecordStatus` IN ('OUTSTANDING')";
    $overdues = $recordManager->getOverDueRecords($uID, $cond);
} else {
    $overdues = "";
}
// dump($overdues);
If (! empty($overdues)) {
    header('Location: overdue.php');
    exit();
}

header('Location: renew.php?show_user_detail=1');

exit();
?>

<script type="text/javascript">
<!--
window.location = "renew.php??show_user_detail=1"
//-->
</script>
