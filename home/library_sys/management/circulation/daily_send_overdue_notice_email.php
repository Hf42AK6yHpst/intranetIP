<?php
/*
 * This file should be run daily for sending the reminder mail to reader when the due date is TODAY
 * Created by Henry
 */
$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($WRT_ROOT."includes/global.php");
include_once($WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($WRT_ROOT."includes/libdb.php");
include_once($WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");
intranet_opendb();
$libms = new liblms();
echo "<br/>This file should be run daily for sending the reminder email(s) to reader when the due date is TODAY<br/><br/>";
echo "Program started!<br/>";

//$remind_date = $libms->get_system_setting('due_date_reminder');

$notifcation_settings	= array(
	    'overdue' => getNotificationSettings('overdue_email')
	);

$sql = "SELECT id FROM LIBMS_DAILY_SCRIPT WHERE DATE(DateCreated) >= DATE(now()) AND name = 'overdue_email' limit 1";
$result = $libms->returnArray($sql);

if(!$notifcation_settings['overdue'] || $result[0]['id']){
	if($result[0]['id']){
		echo "Overdue email(s) is/are already sent!<br/>";
	}else{
		echo "The setting is not allow to send overdue email!<br/>";
	}
	echo "Program ended!<br/>";
	//exit();
}
else{
$sql2 = "select distinct UserID from LIBMS_BORROW_LOG WHERE RecordStatus = 'BORROWED' AND DueDate = DATE_FORMAT(NOW(),'%Y-%m-%d')";
$ReaderIDs = $libms->returnArray($sql2);
$bookCount = 0;
for($i=0; $i < sizeof($ReaderIDs); $i++){
	$ReaderID = $ReaderIDs[$i]['UserID'];
	//send email here...
	//debug_pr($ReaderID);
	
	$sql = "SELECT lb.BookTitle, lb.ResponsibilityBy1, lb.ResponsibilityBy2, lb.ResponsibilityBy3, lb.CallNum, lb.CallNum2, lbu.ACNO, lbl.DueDate FROM LIBMS_BORROW_LOG as lbl JOIN LIBMS_BOOK_UNIQUE as lbu ON lbl.UniqueID = lbu.UniqueID JOIN LIBMS_BOOK as lb ON lbl.BookID = lb.BookID WHERE lbl.RecordStatus = 'BORROWED' AND DueDate = DATE_FORMAT(NOW(),'%Y-%m-%d') AND UserID = '".$ReaderID."'";
	$result = $libms->returnArray($sql);
	
	$bookTitleArr = array();
	
	for($j=0; $j < sizeof($result); $j++){
		$bookCount++;
		$bookTitleArr[$j] = $result[$j]['BookTitle'];
		$bookTitleArr[$j] .= ($result[$j]['ResponsibilityBy1']?" / ".$result[$j]['ResponsibilityBy1']:'').($result[$j]['ResponsibilityBy2']?", ".$result[$j]['ResponsibilityBy2']:'').($result[$j]['ResponsibilityBy3']?", ".$result[$j]['ResponsibilityBy3']:'');
		$bookTitleArr[$j] .= ($result[$j]['CallNum']?" / ".$result[$j]['CallNum']:'').($result[$j]['CallNum2']?" ".$result[$j]['CallNum2']:'');
		$bookTitleArr[$j] .= ($result[$j]['ACNO']?" / ".$result[$j]['ACNO']:'');
		$dueDateArr[$j] = $result[$j]['DueDate'];
	}
	
	sendOverdueEmail($ReaderID, $bookTitleArr, $dueDateArr);
	
	//debug_pr($result);
}

$sql = "INSERT INTO LIBMS_DAILY_SCRIPT (name) Values ('overdue_email') ";
$result = $libms->db_db_query($sql);
	
echo "There are ".$bookCount." due date reminder record(s)!<br/>";
echo "The email is sent to ".sizeof($ReaderIDs)." reader(s)!<br/>";
echo "Program ended!<br/>";
}
// The function is in daily_update_reserved_status.php
//function getNotificationSettings($type){
//	global $libms;
//	$sql = "SELECT enable FROM LIBMS_NOTIFY_SETTING
//		WHERE name = '$type'";
//	
//	return current($libms->returnVector($sql));
//	
//}

function sendOverdueEmail($user_id, $bookTitleArr, $due_date){
	
	global $intranet_default_lang, $intranet_root;
	    	  
	include_once($intranet_root.'/home/library_sys/lib/liblibrarymgmt.php');
	include_once($intranet_root."/includes/libemail.php");
	include_once($intranet_root."/includes/libsendmail.php");
	include_once($intranet_root."/includes/libwebmail.php");
	include_once($intranet_root."/lang/libms.lang.$intranet_default_lang.php");
	global $Lang;
	
	$lwebmail = new libwebmail();
	
	$bookTitleList = '<u>';
	$bookTitleList .= implode("</u>, <u>",$bookTitleArr);
	$bookTitleList .= '</u>';
	$mailSubject = $Lang["libms"]["report"]["overdue_email"]["Subject_ByLin"];
	//$mailContent = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['due_date_reminder']['mail_body'], $bookTitleList,'<u>'.$due_date.'</u>');
	$mailContent = '你所借的下列圖書已到期, 請今天內交還. 如你已交還有關圖書, 請無需理會此通知書.<br/>The following library material(s) which you borrowed will be due today. Please return it/them today. Please ignore this notice if you have already returned.<br/><br/>';
	$mailContent .= '<table width="100%" border="1" style="color: rgb(34, 34, 34);">';
	$mailContent .= '<tr valign="top"><td bgcolor="#dddddd" width="80%"> <div align="center"><font size="3"><b>資料名稱<br />Title</b></font></div></td><td bgcolor="#dddddd" width="20%"><div align="center"><font size="3"><b>到期日<br />Due Date</b></font></div></td></tr>';
    for($i=0; $i<sizeof($bookTitleArr); $i++){
    	$mailContent .= ' <tr valign="top"><td ><div align="left"><font size="3">'.$bookTitleArr[$i].'</font></div></td><td><font size="3">'.$due_date[$i].'</font></td></tr>';
    }    
	$mailContent .= '</table><br/>';
	$mailContent .= "請注意: 這郵件的寄出地址只用作發出通知, 請不要回覆此郵件.<br/>This email is for notification only. Please do not reply to this message.";
	
		$lwebmail->sendModuleMail(
		    (array) $user_id,
		    $mailSubject,
		    $mailContent,
		1,'', 'User');
		    
    }
?>