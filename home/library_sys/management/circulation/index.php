<?php

//Modifying by: 

############ Change Log Start ###############
#
# 	20170314 Cameron
# 		- add IP checking for circulation
# 
#	20150619 Henry:	add flag to control the default page
# 	20140530 Henry:	Admin have circulation permission by default
#
#
############ Change Log End ###############

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
//include_once("libms_interface.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php"); 
include_once($PATH_WRT_ROOT."includes/libclass.php");

include_once('RightRuleManager.php');
include_once('RecordManager.php');
include_once('TimeManager.php');

if(!$sys_custom['eLibraryPlus']['defaultBatchReturn']){
	header("Location: borrow.php");
	exit();
}

intranet_auth();
intranet_opendb();

$libms = new liblms();

$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right,$c_m_right);
if($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $c_right['circulation management']['return']){
	header("Location: batch_return.php");
}
else{
	header("Location: borrow.php");
}
exit();
$global_lockout = (BOOL)$libms->get_system_setting('global_lockout');
if (!$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && (!isset($c_right['circulation management']) || $global_lockout )))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!empty($_REQUEST['logout']))
	unset($_SESSION['LIBMS']);

if( isset($_REQUEST['User_Barcode']) || isset($_REQUEST['StudentID'])){
	
	$libms = new liblms();
	if (isset($_REQUEST['User_Barcode']))
	{
		$result = $libms->SELECTFROMTABLE('LIBMS_USER','UserID',array('BarCode' => PHPTOSQL($_REQUEST['User_Barcode']), 'RecordStatus' => '1'));
	} elseif (isset($_REQUEST['StudentID']))
	{
		$result = $libms->SELECTFROMTABLE('LIBMS_USER','UserID',array('UserID' => PHPTOSQL($_REQUEST['StudentID']), 'RecordStatus' => '1'));
	}
		
	if( !empty($result) ){
		$_SESSION['LIBMS']['CirculationManagemnt']['UID'] = $result[0][0];
		
		$result = $libms->SELECTFROMTABLE('LIBMS_GROUP_USER','GroupID',array('UserID' =>PHPTOSQL($_SESSION['LIBMS']['CirculationManagemnt']['UID'])));
		if( empty($result) ){
			$error_msg = $Lang['libms']['CirculationManagement']['user_group_not_found'];
		}else{
			$right_cm = $c_right['circulation management'];
			if ($right_cm['borrow'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
			{
				header("Location: borrow.php");
				die();
			}
			else if ($right_cm['return'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
				header("Location: return.php");
			else if ($right_cm['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
				header("Location: renew.php");
			else if ($right_cm['reserve'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
				header("Location: reserve.php");
			else if ($right_cm['overdue'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
				header("Location: overdue.php");
			else {
				include_once($PATH_WRT_ROOT."includes/libaccessright.php");
				$laccessright = new libaccessright();
				$laccessright->NO_ACCESS_RIGHT_REDIRECT();
				exit;
			}
		}
			
	}else{
		$error_msg = $Lang['libms']['CirculationManagement']['barcode_user_not_found'];
	}
}

global $Lang;
$interface = new interface_html("libms_circulation_user_prompt.html");
$interface->LAYOUT_START();
include_once('TimeManager.php');
$TM = new TimeManager();


if (!($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $TM -> isOpenNow(time()) )) {

	?>
	<div id='display_center' >
		<span class='alert_text'><?=$Lang['libms']['CirculationManagement']['library_closed']?></span>
	</div>
	<?
	
	exit;
}

# determine batch returns / renewals rights
if ($c_right['circulation management']['return'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	$section_title_batch_return_renew = $Lang['libms']['CirculationManagement']['batch_return'];
	// ." / ".$Lang["libms"]["CirculationManagement"]["renews"];
	if ($c_right['circulation management']['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
	{
		$section_title_batch_return_renew .= " / ".$Lang["libms"]["CirculationManagement"]["renews"];
	}
	$EnterBtn = $interface->GET_ACTION_BTN($Lang['libms']['CirculationManagement']['batch_return_enter'].$Lang["libms"]["CirculationHotKeys"]["BatchReturn"], "button", "self.location='batch_return.php'", "CancelBtn", "");
} elseif ($c_right['circulation management']['renew'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	$section_title_batch_return_renew = $Lang['libms']['CirculationManagement']['batch_renew'];
	$EnterBtn = $interface->GET_ACTION_BTN($Lang['libms']['CirculationManagement']['batch_return_enter'].$Lang["libms"]["CirculationHotKeys"]["BatchReturn"], "button", "self.location='batch_renew.php'", "CancelBtn", "");
}

#Class List
$lc = new libclass();
$sql = "Select distinct ClassName, ClassName From LIBMS_USER WHERE ClassName IS NOT NULL AND ClassName <> '' ORDER BY ClassName";
$result = $libms->returnArray($sql);
$StudentClasses = $lc->getSelectClass("name=\"StudentClass\" id=\"StudentClass\" onChange=\"UpdateStudentList(this.value)\"",$readerClass,0);

$SaveBtn   = $interface->GET_ACTION_BTN($button_submit, "submit", "", "SubmitBtn", "");
$ResetBtn = $interface->GET_ACTION_BTN($button_reset, "reset", "", "CancelBtn", "");

?>

<script type="text/javascript">
$(function(){
	$('#input_code_text').focus();
	
	$(document).bind('keydown',function(e){
			 if (e.keyCode==112)
			{
				// F1
				self.location = "batch_return.php";
				return false;
			} else if (e.keyCode==113)
			{
				document.user_form.User_Barcode.focus();
				return false;
			}else if (e.keyCode==114)
			{
				document.book_search_form.Book_Barcode.focus();
				return false;
			}
		});
		
	if (typeof($.browser.msie)!="undefined" && $.browser.msie)
	{
		document.onhelp = new Function("return false;");
		window.onhelp = new Function("return false;");
	}
});


function checkForm(formObj)
{
	if(formObj.User_Barcode.value=="" || formObj.User_Barcode.value==" ")
	{
		formObj.User_Barcode.focus();
		return false;
	}
}

function urlencode (str) {
  str = (str + '').toString();
  return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
  replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function showSearchResult(formObj)
{
	if(formObj.Book_Barcode.value=="" || formObj.Book_Barcode.value==" ")
	{
		formObj.Book_Barcode.focus();
		return false;
	} else
	{
		tb_show('<?=mysql_real_escape_string(htmlspecialchars($Lang["libms"]["Circulation"]["BookBorrowHistory"]))?>', 'book_loans_history.php?BarCode='+urlencode(formObj.Book_Barcode.value)+'&KeepThis=true&height=500&width=680');
		formObj.Book_Barcode.value="";
	}
	
	return false;
}

function UpdateStudentList(ClassSelected)
{
	if (ClassSelected!="")
	{
		$( "#StudentListSpan" ).html( "<?=$Lang['General']['Loading']?>" );
		$.ajax({
	 	  type: "POST",
		  url: "ajax_load.php",
	  	  data: { Task: "ClassList", ClassName: ClassSelected }
		}).done(function( html ) {
		    $( "#StudentListSpan" ).html( html );
		});
	}
}

function SelectStudent(StudentID, FormObj)
{
	if (StudentID!="")
	{
		FormObj.submit();
	}
	
}
</script>

<table width="200" border="0" cellpadding="10" cellspacing="5">
<?php if ($section_title_batch_return_renew!="") { ?>
<tr>
	<td width="25%"> <div class="module_title_text"><span><?=$section_title_batch_return_renew ?></span></div></td>
</tr>
<tr>
	<td>
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?=$EnterBtn?>
	</td>
</tr>
<tr>
	<td><hr /></td>
</tr>
<?php } ?>
<tr>
	<td width="25%"> <div class="module_title_text"><span><?=$Lang['libms']['CirculationManagement']['normal_circulation']?></span></div></td>
</tr>
<tr>
	<td nowrap="nowrap">
		<form id='user_form' name='user_form'  onSubmit="return checkForm(this)">
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?=$Lang['libms']['CirculationManagement']['user_barcode']?> [F2] :
			<input type="text" id='input_code_text' name="User_Barcode" value="" />
			<?=$SaveBtn?>
			<?=$ResetBtn?>
		</form>
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class='alert_text'><?=$error_msg?></span>
<?php if ($libms->get_system_setting("circulation_student_by_selection")) {?>
	
		<form id='student_form' name='student_form' >
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?=$Lang["libms"]["CirculationManagement"]["student_only"]?> :
			<?= $StudentClasses ?>
			<span id="StudentListSpan">&nbsp;</span>
		</form>
<?php } ?>
	</td>
</tr>
<tr>
	<td><hr /></td>
</tr>
<tr>
	<td width="25%"> <div class="module_title_text"><span><?=$Lang['libms']['CirculationManagement']['search_book_record']?></span></div></td>
</tr>
<tr>
	<td>
		
		<form name='book_search_form' onSubmit="return showSearchResult(this)">
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?= $Lang["libms"]["book"]["barcode"] ?> [F3] :
			<input type="text" id='input_book_barcode' name="Book_Barcode" value="" />			
			<?= $interface->GET_ACTION_BTN($Lang["libms"]["pre_label"]["search"], "submit", "", "SearchBookBtn", "") ?>
		</form>
		&nbsp; &nbsp; &nbsp; <span class='alert_text'><?=$error_msg2?></span>
	</td>
</tr>
</table>


<br />



<?php
$interface->LAYOUT_STOP();

