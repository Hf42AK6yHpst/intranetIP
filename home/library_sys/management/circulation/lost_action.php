<?php
// Modifying by:

/**
 * ########### Change Log Start ###############
 *
 * 20180222 Cameron
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 20170801 Cameron
 * - fix bug: should initialize $result, otherwize it causes "wrong datatype for second argument in ..." error when used in PowerClass
 *
 * 20170314 Cameron
 * - add IP checking for circulation
 *
 * 20160913 Cameron: set $PATH_WRT_ROOT so that it can call SYNC_PHYSICAL_BOOK_TO_ELIB when doing loss_a_book process
 *
 * 20150722 Henry: apply class management permission
 *
 * ########### Change Log End ###############
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);
// ###### END OF Trim all request #######
$PATH_WRT_ROOT = "../../../../"; // use in SYNC_PHYSICAL_BOOK_TO_ELIB
$WRT_ROOT = "{$_SERVER['DOCUMENT_ROOT']}/";
include_once ($WRT_ROOT . "includes/global.php");
include_once ($WRT_ROOT . "includes/pdump/pdump.php");
include_once ('User.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');
include_once ('TimeManager.php');
include_once ('User.php');
intranet_auth();
intranet_opendb();
global $Lang;

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);

$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['return'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}
$bookUniqieIDs = $_REQUEST['lost_book'];

// $interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "RENEW";

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];

$User = new User($uID, $libms);
// $BookManager = new BookManager($libms);
$result = array();
foreach ($bookUniqieIDs as $bookUniqieID) {
    if (! empty($bookUniqieID)) {
        $result[$bookUniqieID] = $User->loss_a_book($bookUniqieID);
    }
}

$error = in_array(false, $result);
if ($error)
    $_SESSION['LIBMS']['xmsg'] = $Lang['libms']['Circulation']['action_fail'];
else
    $_SESSION['LIBMS']['xmsg'] = $Lang['libms']['Circulation']['action_success'];

$recordManager = new RecordManager($libms);
$cond[] = "ol.`RecordStatus` IN ('OUTSTANDING')";
$overdues = $recordManager->getOverDueRecords($uID, $cond);

If (! empty($overdues)) {
    header('Location: overdue.php');
    exit();
}

header('Location: lost.php');

?>

<script type="text/javascript">
<!--
window.location = "lost.php"
//-->
</script>
