<?php
/**
 * 	Using: 
 * 
 * Log :
 * 
 * Date		2019-04-23 (Henry)
 * 			- added IntegerSafe to prevent SQL injection
 * 
 * Date		2017-03-14 [Cameron]
 * 			- add IP checking for circulation
 * 
 * Date		2016-03-07 [Cameron]
 * 			- add Parameter $Mode and retrieve uID by bookUniqueID from borrow log
 * 
 * Date		2016-02-19 [Cameron]
 * 			- check if $uID exist before process, occurs when open the same renew page of a user in two sessions in the same browser
 * 				then quit user in one session but press "force to renew" in another session
 * 
 * Date		2013-12-16 [yuen] support counting on school days only 
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
####### END  OF   Trim all request #######
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once('User.php');
include_once('BookManager.php');
include_once('RightRuleManager.php');
include_once('TimeManager.php');
include_once('User.php');
intranet_auth();

$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right,$c_m_right);

if (!$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && !isset($c_right['circulation management'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];
intranet_opendb();

$bookUniqueID = IntegerSafe($_REQUEST['UniqueID']);
$mode = $_REQUEST['Mode'];

$libms = new liblms();
if ($mode == 'ByBatch') {	// from change checkbox in batch renew 
	# should find the userID from borrow record
	if ($bookUniqueID) {
		$sql = "SELECT UserID FROM `LIBMS_BORROW_LOG` WHERE `UniqueID`='{$bookUniqueID}' AND RecordStatus='BORROWED' ORDER BY `BorrowLogID` DESC LIMIT 1";
		$rows = $libms->returnArray($sql);
		//global $UserID;
		if (count($rows) > 0) {
			$uID = $rows[0][0];
		}
	}
}


if ($uID) {
	$User = new User($uID, $libms);
	$BookManager = new BookManager($libms);

	$canBorrow=$User->rightRuleManager->canBorrow($bookUniqueID);
	$bookID = $BookManager->getBookIDFromUniqueBookID($bookUniqueID);
	$bookInfo = $BookManager->getBookFields($bookID, '*', $bookUniqueID); //for overwriting the remark for user when item have RemarkForUser
	
	$uniqueBookStatus = $BookManager->getUniqueBookRecordStatus($bookUniqueID);
	
	
	$CirculationTypeApplied = (trim($bookInfo['Item_CirculationTypeCode'])<>"") ? $bookInfo['Item_CirculationTypeCode'] : $bookInfo['CirculationTypeCode'];
	
	$limits=$User->rightRuleManager->get_limits($CirculationTypeApplied);
	
	$timeManager = new TimeManager();
	
	if ($strToday=="")
	{
		$strToday = date("Y-m-d");	
	}
	if ($libms->get_system_setting("circulation_duedate_method"))
	{
		$dueDate = $timeManager->findOpenDateSchoolDays(strtotime($strToday), $limits['ReturnDuration']);
	} else
	{
		$dueDate = $timeManager->findOpenDate(strtotime("{$strToday} + {$limits['ReturnDuration']} days"));
	}
	 
	echo date('Y-m-d', $dueDate);
}
else {
	echo 'AlreadyQuit';
}