<?php
/*
 * Modifying by:
 *
 * Log
 *
 * Date: 2018-02-22 [Cameron]
 * - fix: circulation access right should be 'return' but not 'borrow'
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * Date: 2017-03-14 [Cameron]
 * - add IP checking for circulation
 *
 * Date: 2016-12-05 [Cameron]
 * - add empty checking for $overDue when set $payment_text_link
 *
 * Date: 2015-10-15 [Cameron]
 * - show return book list in "latest return on top"
 *
 * Date: 2015-10-14 [Cameron]
 * - set $json['alertReservePerson'] to true
 *
 * Date: 2015-08-18 [Cameron]
 * - create this file
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);
// ###### END OF Trim all request #######

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ('User.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');
include_once ('TimeManager.php');

intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['return'] || $global_lockout))) 
{
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}

$bookUniqueIDs = $_REQUEST['bookUniqueID'];
$Mode = $_REQUEST['Mode'];

$GLOBALS['LIBMS']['PAGE'] = "RETURN";
$json['success'] = FALSE;
$json['script'] = '';
$json['alertReservePerson'] = TRUE;

$RecordManager = new RecordManager($libms);

$rowNo = 1;
// $error = false;
// $errorBookUniqueID = array();
$list = '';

if (! empty($bookUniqueIDs)) {
    $bookUID = array();
    while (count($bookUniqueIDs) > 0) {
        $bookUID[] = array_pop($bookUniqueIDs);
    }
    
    foreach ($bookUID as $bookUniqueID) {
        if (! empty($bookUniqueID)) {
            $uID = $RecordManager->getUserIDByBookUniqueID($bookUniqueID);
            if ($Mode != "ByBatch" && ! empty($_SESSION['LIBMS']['CirculationManagemnt']['UID'])) {
                $uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];
            }
            
            $result = $RecordManager->returnBook($bookUniqueID, 'return_book', $Mode, $uID);
            extract($result);
            
            $barcode = $bookInfo['UniqueBarCode'];
            if ($returnResult == false) {
                // $error = true;
                // $errorBookUniqueID[] = $bookUniqueID;
            } else {
                $json['success'] = true;
            }
            if (empty($ID)) {
                $ID = $rowNo;
            }
            
            $rowNo ++;
            
            $payment_text_link = empty($overDue) ? '' : '$' . $overDue['Payment'];
            
            if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['overdue'] || $global_lockout)) {} else {
                // able to handle over due payment
                $payment_text_link = '<a href="overdue.php?user_id=' . urlencode($borrowLog["UserID"]) . '&borrow_log_id=' . $borrowLogID . '"> ' . $payment_text_link . " </a><input type='hidden' class='not_class_but_id' name='over_due_users[]' id='over_due_users[]' value='" . $borrowLog["UserID"] . "' /><input type='hidden' class='borrow_log_id_class' name='borrow_log_id[]' id='borrow_log_id[]' value='" . $borrowLogID . "' />";
            }
            
            $view_data = compact('bookInfo', 'borrowLog', 'return_row_class', 'IsReturnBookWithoutBorrowLog', 'borrow_by_user', 'alert_text', 'not_allow_overdue_return', 'isOverDue', 'overDue', 'BookLocation', 'reserves', 'User', 'ID', 'barcode', 'payment_text_link', 'bookUniqueID', 'junior_mck');
            
            $list = $RecordManager->gen_return_book_list($view_data) . $list;
        }
    }
    
    $json['html'] = $list;
    $RecordManager->output($json);
}

intranet_closedb();

?>
