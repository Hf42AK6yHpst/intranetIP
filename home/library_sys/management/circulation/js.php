<?php
//Modifying by: 

############ Change Log Start ###############
#
# 	20181128 (Henry)
# 		- modified keydown event to pervent multiple request to server
# 	 
# 	20170314 (Cameron)
# 		- add IP checking for circulation
#	20160217 Cameron
#		- Change Book_Barcode to Keyword in book_search_form 
#	20150619 Henry: allow press F3 to search book
#	20150609 Henry: apply class management permission
#
#
############ Change Log End ###############

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();

$libms = new liblms();

$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right,$c_m_right);
$a = isset($c_right['circulation management']);

if (!$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']&& !isset($c_right['circulation management'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

# determine batch returns / renewals rights
if ($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $c_right['circulation management']['return'])
{
	$batch_return_renew_path = "batch_return.php";
} elseif ($c_right['circulation management']['renew'])
{
	$batch_return_renew_path = "batch_renew.php";
}

if(isset($_REQUEST['show_user_detail']))
	$_onready_js="toggle_userdetail();";
	
?>
<script type='text/javascript'>
	function toggle_userdetail(){
		var my_background = "-webkit-linear-gradient(right, #CDF9CD, #B4F5C6)";
		
		
		detail_on = ! detail_on;
		if (detail_on){
			User_deatil = $('.user_detail_on');
			saved_background_css = User_deatil.css('background-image');
			User_deatil.css('color', 'red').css('background-image',my_background);
			$('.HTML_BODY').slideUp(250);
			$('.user_details_info').toggle(250);			
			
		} else { 
			$('.user_detail_on').css('color','').css('background-image',saved_background_css);
			$('.user_details_info').toggle(250);
			$('.HTML_BODY').slideDown(250);
			
		}
	}
	
	function ajax_update_user_info(){
		$.get('user_details.php', function (data){ $('div.user_details_info').replaceWith(data); })
		$.get('user_info_left.php',{update : 1}, function (data){ $('div.user_record').replaceWith(data); })
	}
	
	$().ready(function(){
		detail_on = false;
		var isKeyDown = false;
		$('.user_detail_on').click(toggle_userdetail);
		<?=$_onready_js?>
		$(document).bind('keydown',function(e){
			if(!isKeyDown){
				isKeyDown = true;
				switch (e.keyCode)
				{
					case 27:	// Esc
						self.location = "borrow.php?logout=true";
						return false;
				    case 112:	// F1
						self.location = "<?=$batch_return_renew_path?>";
						return false;
						<?if(isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])){?>
				    case 113:	// F2
						self.location = "borrow.php";
						return false;
				    case 114:	// F3
						self.location = "return.php";
						return false;
				    case 115:	// F4
						self.location = "renew.php";
						return false;
				    case 116:	// F5
						self.location = self.location;
						return false;
				    case 117:	// F6
						self.location = "lost.php";
						return false;
				    case 118:	// F7
						self.location = "reserve.php";
						return false;
				    case 119:	// F8
						self.location = "overdue.php";
						return false;
				    case 121:	// F10
						toggle_userdetail();
						isKeyDown = false;
						return false;
						<?} else {?>
					case 113:	// F2
						document.user_form.User_Barcode.focus();
						isKeyDown = false;
						return false;
					case 114:	// F3
						document.book_search_form.Keyword.focus();
						isKeyDown = false;
						return false;
						<?}?>
				    case 37:	// left
				    	isKeyDown = false;
						if ($('.user_details_info').is(':visible'))
						{
							jsShowDetails(1);
							return false;
						}
				    case 39:	// right
				    	isKeyDown = false;
						if ($('.user_details_info').is(':visible'))
						{
							jsShowDetails(0);
							return false;
						}
				    default:
						//alert(e.keyCode);
						isKeyDown = false;
				      return true;
				}
			}
  
		});
		if (typeof($.browser.msie)!="undefined" && $.browser.msie)
		{
			document.onhelp = new Function("return false;");
			window.onhelp = new Function("return false;");
		}
	});
</script>
