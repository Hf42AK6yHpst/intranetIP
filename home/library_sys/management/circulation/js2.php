<?php
/*
 * 	Purpose:	common js for both "Circulation for Patron" and "Batch Book Returns / Renews"
 * 				use for "Book Search" function
 * 	Using:
 * 
 *  2019-03-28 [Cameron] show BookSubTitle in book_search_result_handler() if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * 
 * 	2017-04-06 [Cameron] modify BackToSearchList() to support backslash for php5.4
 * 
 * 	2016-09-26 [Cameron] trim space for User_Barcode in checkFormBarcode() [case #L104252]
 * 
 * 	2016-02-19 [Cameron] create this file
 *  
 */
?>
<script type='text/javascript'>
function showSearchResult(formObj)
{
	if(formObj.Keyword.value=="" || formObj.Keyword.value==" ")
	{
		formObj.Keyword.focus();
		return false;
	} else
	{
//		tb_show('<?=mysql_real_escape_string(htmlspecialchars($Lang["libms"]["Circulation"]["BookBorrowHistory"]))?>', 'book_loans_history.php?BarCode='+encodeURIComponent(formObj.Keyword.value)+'&KeepThis=true&height=500&width=680');

		$.ajax({
			type: "post",						
			dataType: "json",					
			url: 'book_loans_history.php',					
			data: {					
					'Keyword':formObj.Keyword.value			
				  },
			success : book_search_result_handler,
			fail : function() {
				alert('Error on AJAX call!');
			}					
		});						

		formObj.Keyword.value="";
		
	}
	
	return false;
}
function UpdateStudentList(ClassSelected)
{
	if (ClassSelected!="")
	{
		$( "#StudentListSpan" ).html( "<?=$Lang['General']['Loading']?>" );
		$.ajax({
	 	  type: "POST",
		  url: "ajax_load.php",
	  	  data: { Task: "ClassList", ClassName: ClassSelected }
		}).done(function( html ) {
		    $( "#StudentListSpan" ).html( html );
		});
	}
}
function SelectStudent(StudentID, FormObj)
{
	if (StudentID!="")
	{
		FormObj.submit();
	}
	
}
function checkFormBarcode(formObj)
{
	formObj.User_Barcode.value = $.trim(formObj.User_Barcode.value);
	if(formObj.User_Barcode.value=="" || formObj.User_Barcode.value==" ")
	{
		formObj.User_Barcode.focus();
		return false;
	}
}

function book_search_result_handler (ajax_return_obj) {
	
	if (ajax_return_obj.is_success){
		
		if (ajax_return_obj.nrResults == 1) {
			$popup_div = $('<div id="popup_div" title="<?=$Lang["libms"]["Circulation"]["BookBorrowHistory"]?>"/>');
			$table = ajax_return_obj.bookInfo;
		}
		else {
			var bookTitle;
			$popup_div = $('<div id="popup_div" title="<?=$Lang["libms"]["Circulation"]["ClickToViewBorrowHistory"]?>"/>');	
							
			$table = $('<table />');
			$table.append('<thead><tr style="height: 30px; font-size:13pt;">'
							+'<th><?=$Lang['libms']['CirculationManagement']['book_search']['title']['BookTitle']?></th>'
							+'<th><?=$Lang['libms']['CirculationManagement']['book_search']['title']['Author']?></th>'
							+'<th><?=$Lang['libms']['CirculationManagement']['book_search']['title']['CallNum']?></th>'
							+'<th><?=$Lang['libms']['CirculationManagement']['book_search']['title']['ACNO']?></th>'
							+'</tr></thead>');
							
							
			$.each(ajax_return_obj.bookInfo, function (i,v){
				bookTitle = v.BookTitle;
				<?php if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']):?>
					if (v.BookSubTitle != '') {
						bookTitle += ' :' + v.BookSubTitle;  
					}
				<?php endif;?>
				
				$row_div = $('<tr style="height: 26px;"/>');
				$row_div.append('<td><a style="cursor:pointer;">'+bookTitle+'</a></td>'+
								'<td>'+v.ResponsibilityBy1+'</td>'+
								'<td>'+v.CallNum+'</td>'+
								'<td>'+v.ACNO+'</td></tr>');
				$table.append($row_div);
				$row_div.find('a').click(function(){BookInfoByBarCode(v.BarCode,ajax_return_obj.Keyword);});
			});
			$table.find('tr:even').css("background-color", "#EEEEFF");
			$table.find('thead tr').css("background-color", "#FFEEDD");
			$table.find('a').css("color", "#2286C5");
			$table.find('a').hover(
				function(){
					$(this).css("color","#FF0000");
				},
				function(){
					$(this).css("color","#2286C5");
				}			
			);
		}
		
		$popup_div.prepend(ajax_return_obj.script);
		$popup_div.append($table);
		$popup_div.dialog({
			width : 900,
			height: 500,
			modal : true,
			close: function() {$popup_div.remove();}
		});
	}else{
		alert('<?=$Lang["libms"]["CirculationManagement"]["msg"]["search_book_not_found"]?>');
	}
}

function BookInfoByBarCode(BarCode, Keyword) {
	$("#popup_div").remove();
	$.ajax({
		type: "post",						
		dataType: "json",					
		url: "book_loans_history.php",					
		data: {					
				"SpecificBarCode": BarCode,	
				"Keyword": Keyword		
			  },
		success : book_search_result_handler,
		fail : function() {
			alert("Error on AJAX call!");
		}					
	});						
}

function BackToSearchList() {
	var keyword = $("#SearchKeyword").val();
	$("#popup_div").remove();
	formObj = document.getElementById('book_search_form');
	keyword = keyword.replace("&quot;",'"');
	keyword = keyword.replace("&#039","'");
	keyword = keyword.replace("&lt;","<");
	keyword = keyword.replace("&gt;",">");
	keyword = keyword.replace("&amp;","&");
	
<? if (!get_magic_quotes_gpc()):?>
	keyword = keyword.replace("\\\\\\\\","\\");
<? endif;?>	

	formObj.Keyword.value=keyword;
	showSearchResult(formObj);
}

</script>