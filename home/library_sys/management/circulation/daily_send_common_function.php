<?php 
/*
 *  move common functions to here so that it can test individual daily_send_* file
 *  
 *  2020-05-28 Cameron
 *      - move functions to this file
 */

function getParentStudentAssoAry($user_id)
{
    global $intranet_root;
    include_once ($intranet_root . "/includes/libuser.php");
    include_once ($intranet_root . "/includes/eClassApp/libeClassApp.php");
    $luser = new libuser();
    $libeClassApp = new libeClassApp();
    $studentsWithParentUsingParentApp = $luser->getStudentWithParentUsingParentApp();
    $userIdArr = array_intersect((array) $user_id, (array) $studentsWithParentUsingParentApp);
    if (! empty($userIdArr)) {
        $parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($userIdArr), 'ParentID', $IncludedDBField = array(
            'StudentID'
        ), $SingleValue = 1, $BuildNumericArray = 1);
    } else {
        $parentStudentAssoAry = array();
    }
    return $parentStudentAssoAry;
}

?>