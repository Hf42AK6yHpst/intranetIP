<?php
// using: Henry

/*************************************************************
 *  20171117 (Henry)
 * 			added user barcode checking [Case#Z131267]
 * 
 *	20140623 (Henry)
 * 			Created this file
 * 
 *************************************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementBookList";

//$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php");
$PAGE_NAVIGATION[] = array($Lang["libms"]["bookmanagement"]["import"]." ".$Lang["libms"]["Circulation"]["BookBorrowHistory"], "");

$TAGS_OBJ[] = array($Lang['libms']['action']['book'], "index.php", 1);

$linterface = new interface_html("libms.html");

# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$filepath = $file;
$filename = $file_name;

$uploadSuccess = true;
if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename))
	{
		$uploadSuccess = false;
		$xmsg2 = $Lang['libms']['import_book']['upload_fail']." ".$Lang['plupload']['invalid_file_ext'];
	}

if($uploadSuccess){
	
	$file_format = array(
			'ACNO',
			'OUTDATE',
			'OUTTIME',
			'DUEDATE',
			'RTNDATE',
			'STATUS',
			'FINE',
			'ENAME',
			'CNAME',
			'USERBARCODE',
			'CLASSNAME',
			'CLASSNUMBER'
	);
	
//	$file_format = array(
//			'User Barcode',
//			'ACNO',
//			'Borrow Date',
//			'Borrow Time',
//			'Due Date',
//			'Renewal Time',
//			'Returned Date',
//			'Returned Time',
//			'Record Status'
//	);

	$flagAry = array(
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1'
	);
	$format_wrong = false;

	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($filepath,"","",$file_format,$flagAry);

	$counter = 1;
	$insert_array = array();
	$my_key = array();

	if(is_array($data))
	{
		$col_name = array_shift($data);
	}

	for($i=0; $i<sizeof($file_format); $i++)
	{
		if (strtoupper($col_name[$i])!=strtoupper($file_format[$i]))
		{
			$format_wrong = true;
			break;
		}
	}


	if($format_wrong)
	{
		header("location: import_borrow_record.php?xmsg=wrong_header");
		exit();
	}
	if(sizeof($data)==0)
	{
		header("location: import_borrow_record.php?xmsg=import_no_record");
		exit();
	}
	
	//create a tmp table [start]
	$sql = "DROP TABLE LIBMS_LOAN_RECORD_TMP";
    $libms->db_db_query($sql);
//	$sql ="CREATE TABLE IF NOT EXISTS LIBMS_LOAN_RECORD_TMP (
//	     BorrowLogID int(11) NOT NULL auto_increment,
//	     BookID int(11) NOT NULL,
//	     UniqueID int(11) NOT NULL,
//	     UserID int(11) NOT NULL,
//	     BorrowTime datetime NOT NULL,
//	     DueDate date NOT NULL COMMENT 'it will be calculated according user group, circulation type, ReturnDuration and Holidays ',
//	     DueDate_BAK date default NULL,
//	     RenewalTime int(2) default '0' COMMENT '0 for the first borrow; 1 for the first renewal; it should not exceed LimitRenew',
//	     ReturnedTime datetime NOT NULL,
//	     RecordStatus varchar(16) default NULL COMMENT '1 for active; 0 for returned;',
//	     DateModified datetime default NULL,
//	     LastModifiedBy int(11) default NULL,
//	     DueDateModifiedBy int(11) default NULL,
//	     PRIMARY KEY (BorrowLogID),
//	     KEY BookID (BookID),
//	     KEY UserID (UserID)
//	) ENGINE=InnoDB DEFAULT CHARSET=utf8";

	$sql ="CREATE TABLE IF NOT EXISTS LIBMS_LOAN_RECORD_TMP (
	     LoanRecordID int(11) NOT NULL auto_increment,
	     ACNO varchar(16) default NULL,
	     BorrowTime datetime NOT NULL,
	     DueDate date NOT NULL,
	     ReturnedTime datetime NOT NULL,
	     RecordStatus varchar(16) default NULL,
		 Fine decimal(10,2) default NULL COMMENT 'payment = overdue charge per day * DaysCount',
	     EnglishName varchar(128) default NULL,
	     ChineseName varchar(128) default NULL,
	     BarCode varchar(64) default NULL,
		 ClassName varchar(255) default NULL,
		 ClassNumber int(8) default NULL,
	     DateModified datetime default NULL,
	     LastModifiedBy int(11) default NULL,
	     PRIMARY KEY (LoanRecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
    $libms->db_db_query($sql);
    //create a tmp table [end]
    
    ### List out the import result
	$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang["libms"]["report"]["ACNO"] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['bookmanagement']['barcode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['General']['Warning']."</td>";
	$x .= "</tr>";
	
    $error_occured = 0;
    $hiddenField = "";
    $itemBarCodeArr = array();
	foreach ($data as $record) {
		$sql = "Select UserID From LIBMS_USER WHERE BarCode = '".$record['9']."'";
		//$sql = "Select UserID From LIBMS_USER WHERE ACNO = '".$record['0']."'";
		$result = current($libms->returnArray($sql));
		
		$sql = "Select BookID, UniqueID, RecordStatus From LIBMS_BOOK_UNIQUE WHERE ACNO = '".$record['0']."'";
		$result2 = current($libms->returnArray($sql));
		
		$record[1] = getDefaultDateFormat($record[1]);
		//check borrow datetime
		if ($record[1] =='' && $record[2] ==''){
			$validDate = true;
		}
		else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $record[1]) ) {
	       list($year , $month , $day) = explode('-',$record[1]);
	       $validDate = checkdate($month , $day , $year);
	    } else {
	       $validDate =  false;
	    }

	    if ($record[1] =='' && $record[2] ==''){
			$validTime = true;
		}
		else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/', $record[2]) ) {
	       $validTime = true;
	    } else {
	       $validTime =  false;
	    }
	    $validBorrowDatetime = $validDate && $validTime;
	    
	    $record[3] = getDefaultDateFormat($record[3]);
	    //check due date
		if ($record[3] ==''){
			$validDate = true;
		}
		else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $record[3]) ) {
	       list($year , $month , $day) = explode('-',$record[3]);
	       $validDate = checkdate($month , $day , $year);
	    } else {
	       $validDate =  false;
	    }

	    $validDueDate = $validDate;
	    
	    $record[4] = getDefaultDateFormat($record[4]);
	    //check return datetime
		if ($record[4] =='' /*&& $record[7] ==''*/){
			$validDate = true;
		}
		else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $record[4]) ) {
	       list($year , $month , $day) = explode('-',$record[4]);
	       $validDate = checkdate($month , $day , $year);
	    } else {
	       $validDate =  false;
	    }

//	    if ($record[4] =='' && $record[7] ==''){
//			$validTime = true;
//		}
//		else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/', $record[7]) ) {
//	       $validTime = true;
//	    } else {
//	       $validTime =  false;
//	    }
	    $validReturnDatetime = $validDate /*&& $validTime*/;
		
//		if(in_array($record[0],$itemBarCodeArr)){
//			$error_occured = 1;
//			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
//					$x .= "<td class=\"$css\">".($y)."</td>";
//					$x .= "<td class=\"$css\">".$record['0']."</td>";
//					$x .= "<td class=\"$css\">".$record['9']."</td>";
//					$x .= "<td class=\"$css\">";
//					$x .= "<div style='color:red'>item ACNO is used before</div>";	
//					$x.="</td>";
//					$x .= "</tr>";
//		}
		if($record[7] =='' && $record[9]==''){
			$error_occured = 1;
			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
					$x .= "<td class=\"$css\">".($y)."</td>";
					$x .= "<td class=\"$css\">".$record['0']."</td>";
					$x .= "<td class=\"$css\">".$record['9']."</td>";
					$x .= "<td class=\"$css\">";
					$x .= "<div style='color:red'>No user barcoade and english name</div>";	
					$x.="</td>";
					$x .= "</tr>";
		}
		else if(!$result || $record[9]==''){
//			$error_occured = 1;
			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
					$x .= "<td class=\"$css\">".($y)."</td>";
					$x .= "<td class=\"$css\">".$record['0']."</td>";
					$x .= "<td class=\"$css\">".$record['9']."</td>";
					$x .= "<td class=\"$css\">";
					$x .= "<div style='color:red'>".$Lang["libms"]["btn"]["enter"].' '.$Lang["libms"]["CirculationManagement"]["user_barcode"]."</div>";	
					$x.="</td>";
					$x .= "</tr>";
		}
		else if(!$result2){
			$error_occured = 1;
			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
					$x .= "<td class=\"$css\">".($y)."</td>";
					$x .= "<td class=\"$css\">".$record['0']."</td>";
					$x .= "<td class=\"$css\">".$record['9']."</td>";
					$x .= "<td class=\"$css\">";
					$x .= "<div style='color:red'>".$Lang['libms']['invalid_acno']."</div>";	
					$x.="</td>";
					$x .= "</tr>";
		}
//		else if($result2['RecordStatus'] == 'BORROWED'){
//			$error_occured = 1;
//			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
//					$x .= "<td class=\"$css\">".($y)."</td>";
//					$x .= "<td class=\"$css\">".$record['0']."</td>";
//					$x .= "<td class=\"$css\">".$record['9']."</td>";
//					$x .= "<td class=\"$css\">";
//					$x .= "<div style='color:red'>".str_replace(' [user_name]', ' others', str_replace(' [user_name] ', '',$Lang["libms"]["CirculationManagement"]["msg"]["already_borrowed"]))."</div>";	
//					$x.="</td>";
//					$x .= "</tr>";
//		}# todo [add more checking]
		else if(!$validBorrowDatetime){
			$error_occured = 1;
			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
					$x .= "<td class=\"$css\">".($y)."</td>";
					$x .= "<td class=\"$css\">".$record['0']."</td>";
					$x .= "<td class=\"$css\">".$record['9']."</td>";
					$x .= "<td class=\"$css\">";
					$x .= "<div style='color:red'>Invalid borrow date or time</div>";	
					$x.="</td>";
					$x .= "</tr>";
		}
		else if(!$validDueDate){
			$error_occured = 1;
			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
					$x .= "<td class=\"$css\">".($y)."</td>";
					$x .= "<td class=\"$css\">".$record['0']."</td>";
					$x .= "<td class=\"$css\">".$record['9']."</td>";
					$x .= "<td class=\"$css\">";
					$x .= "<div style='color:red'>Invalid due date</div>";	
					$x.="</td>";
					$x .= "</tr>";
		}
		else if($record[4] != '' && !$validReturnDatetime){
			$error_occured = 1;
			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
					$x .= "<td class=\"$css\">".($y)."</td>";
					$x .= "<td class=\"$css\">".$record['0']."</td>";
					$x .= "<td class=\"$css\">".$record['9']."</td>";
					$x .= "<td class=\"$css\">";
					$x .= "<div style='color:red'>Invalid return date</div>";	
					$x.="</td>";
					$x .= "</tr>";
		}
		else{
			if($record[4] == '' /*&& $record[7] == ''*/){
				$returnTime = '';
			}
			else{
				$returnTime = $record[4].' 00:00:00';
			}
			$insert_to_db = array(
//					'BookID'=> CCTOSQL($result2['BookID']),
//					'UniqueID'=> CCTOSQL($result2['UniqueID']),
//					'UserID'=> CCTOSQL($result['UserID']),
//					'BorrowTime'=> CCTOSQL($record[1].' '.$record[2]),
//					'DueDate'=> CCTOSQL($record[3]),
//					'RenewalTime'=> CCTOSQL('0'),
//					'ReturnedTime'=> CCTOSQL($returnTime),
//					'RecordStatus'=> CCTOSQL($record[6])
					 'ACNO'=> CCTOSQL($record[0]),
					 'BorrowTime'=> CCTOSQL($record[1].' '.$record[2]),
				     'DueDate'=> CCTOSQL($record[3]),
				     'ReturnedTime'=> CCTOSQL($returnTime),
				     'RecordStatus'=> CCTOSQL($record[5]),
				     'Fine'=> CCTOSQL($record[6]),
				     'EnglishName'=> CCTOSQL($record[7]),
				     'ChineseName'=> CCTOSQL($record[8]),
				     'BarCode'=> CCTOSQL($record[9]),
					 'ClassName'=> CCTOSQL($record[10]),
					 'ClassNumber'=> CCTOSQL($record[11])
			);
			//debug_pr($insert_to_db);
			$result3 = $libms->INSERT2TABLE('LIBMS_LOAN_RECORD_TMP', $insert_to_db);
		}
		
		

		if ($result3==false){
			//capture error
		}

		$insert_to_db = array();
		$itemBarCodeArr[] = $record['0'];
	}

}


$sql = "SELECT LoanRecordID
		FROM `LIBMS_LOAN_RECORD_TMP` ";
$result = $libms->returnArray($sql);

$y = 3;
 
//$error_occured = 0;
//foreach($result as $record){
//	$error = array();
//	
//	$css = (sizeof($error)==0) ? "tabletext":"red";
//
//	if (sizeof($error)>0){
//		
//		$itemInfo = current($libms->GET_BOOK_ITEM_INFO($record['BookUniqueID']));
//		$bookInfo = current($libms->GET_BOOK_INFO($itemInfo['BookID']));
//		$locationInfo = current($libms->GET_LOCATION_INFO($itemInfo['LocationCode']));
//		
//		$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
//		$x .= "<td class=\"$css\">".($y)."</td>";
//		$x .= "<td class=\"$css\">".$itemInfo['ACNO']."</td>";
//		$x .= "<td class=\"$css\">".$itemInfo['BarCode']."</td>";
//		$x .= "<td class=\"$css\">".$bookInfo['BookTitle']."</td>";
//		$x .= "<td class=\"$css\">".Get_Lang_Selection($locationInfo['DescriptionChi'],$locationInfo['DescriptionEn'])."</td>";
//		$x .= "<td class=\"$css\">".$Lang["libms"]["book_status"][$itemInfo['RecordStatus']]."</td>";
//		$x .= "<td class=\"$css\">";
//
//
//
//		if(sizeof($error)>0)
//		{
//			foreach($error as $Key=>$Value)
//			{
//				$x .=$Value.'<br/>';
//			}
//		}
//
//		$x.="</td>";
//		$x .= "</tr>";
//	}else{
//		 
//	}
//	$y++;
//}

$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_borrow_record.php'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import/*.' via AJAX'*/, "button", '$(\'#send_by_ajax\').click()')." &nbsp;";
	$import_button .= /*$linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".*/$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_borrow_record.php'");
	$prescan_result =  $Lang['libms']['import_book']['upload_success_ready_to_import'].count($result);
	$x = $prescan_result;
}


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script> 

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" /> 

<form name="form1" action="import_borrow_record_result.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><?=$x?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $import_button ?>
			<a style="display:none" id="send_by_ajax" name="send_by_ajax" href="ajax_send.php?height=200&width=500&modal=true" class="thickbox">Thickbox</a>
			</td>
		</tr>
	</table>
	<?=$hiddenField?>
</form>
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>