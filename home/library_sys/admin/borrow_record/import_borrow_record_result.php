<?php
// using: Henry

/*************************************************************
 *	2014-06-24 (Henry)
 * 			Created this file
 * 
 *************************************************************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/RecordManager.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();

$limport = new libimporttext();

$timeManager = new TimeManager($libms);
$recordManager = new RecordManager($libms);

$CurrentPage = "PageBookManagementBookList";

//$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php");
$PAGE_NAVIGATION[] = array($Lang["libms"]["bookmanagement"]["import"]." ".$Lang["libms"]["Circulation"]["BookBorrowHistory"], "");

$TAGS_OBJ[] = array($Lang['libms']['action']['book'], "index.php", 1);

$linterface = new interface_html("libms.html");

# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='import_borrow_record.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_borrow_record.php'");



//===================================================================================

### Function [Start] ###
function get_left_over_from_tmp(){
	global $libms;
	$result = $libms->returnArray("select count(*) as leftover from LIBMS_LOAN_RECORD_TMP");
	return $result[0]['leftover'];
}
### Function [End] ###

$libel = new elibrary();
if (isset($junior_mck))
{
	$libel->dbConnectionLatin1();	
}



$counter = 0;
$counterItem = 0;
while (get_left_over_from_tmp() > 0){
	$error_happen = false;
	$result = $libms->returnArray("select * from LIBMS_LOAN_RECORD_TMP order by LoanRecordID limit 1000 ");
	
	foreach ($result as $borrowrecord){
		# to be modify...
		//$sql = "Select UserID From LIBMS_USER WHERE (BarCode = '".$borrowrecord['BarCode']."' AND BarCode != '') OR (BarCode = '' AND EnglishName = ".CCTOSQL($borrowrecord['EnglishName'])." AND ChineseName = ".CCTOSQL($borrowrecord['ChineseName'])." AND  ClassNumber = ".CCTOSQL($borrowrecord['ClassNumber'])." AND ClassName = ".CCTOSQL($borrowrecord['ClassName']).")";
		$sql = "Select UserID From LIBMS_USER WHERE (BarCode = '".$borrowrecord['BarCode']."' AND BarCode != '')";
		$result = current($libms->returnArray($sql));
		//debug_pr($result);
		//add unknown barcode user...
		if(!$result/* || !$borrowrecord['BarCode']*/){
			$insert_to_db = array(
				'EnglishName'=> CCTOSQL($borrowrecord['EnglishName']),
				'ChineseName'=> CCTOSQL($borrowrecord['ChineseName']),
				'BarCode'=> CCTOSQL($borrowrecord['BarCode']),
				'ClassNumber'=> CCTOSQL($borrowrecord['ClassNumber']),
				'ClassName'=> CCTOSQL($borrowrecord['ClassName']),
				'RecordStatus'=> CCTOSQL('3')
			);
			$result_insert_USER = $libms->INSERT2TABLE('LIBMS_USER', $insert_to_db);
			$result['UserID'] = mysql_insert_id();
			////debug_pr($insert_to_db);
		}
		// maybe change to items barcode
		$sql = "Select BookID, UniqueID, RecordStatus From LIBMS_BOOK_UNIQUE WHERE ACNO = '".$borrowrecord['ACNO']."'";
		$result2 = current($libms->returnArray($sql));
		
		//handle renew time ...
		$renewalTime = 0;
		
		$borrowrecord['RecordStatus'] = ($borrowrecord['RecordStatus'] =='L'?'BORROWED':'RETURNED');
		$insert_to_db = array(
				'BookID'=> CCTOSQL($result2['BookID']),
				'UniqueID'=> CCTOSQL($result2['UniqueID']),
				'UserID'=> CCTOSQL($result['UserID']),
				'BorrowTime'=> CCTOSQL($borrowrecord['BorrowTime']),
				'DueDate'=> CCTOSQL($borrowrecord['DueDate']),
				'RenewalTime'=> CCTOSQL($renewalTime),
				'ReturnedTime'=> CCTOSQL($borrowrecord['ReturnedTime']),
				'RecordStatus'=> CCTOSQL($borrowrecord['RecordStatus']),
				'IsImportData'=> CCTOSQL('1')
		);
		$result_insert_BORROW = $libms->INSERT2TABLE('LIBMS_BORROW_LOG', $insert_to_db);
		////debug_pr($insert_to_db);
		$borrowID = mysql_insert_id();
		if($borrowrecord['RecordStatus'] == 'BORROWED'){
			$sql = "SELECT BorrowTime FROM LIBMS_BORROW_LOG WHERE UniqueID = '".$result2['UniqueID']."' ORDER BY BorrowTime desc LIMIT 1";
			$result3 = current($libms->returnArray($sql));
			if(strtotime($borrowrecord['BorrowTime']) > strtotime($result3['BorrowTime'])){
				////debug_pr('Need to update record status!');
				$result_update_status = $libms->UPDATE_UNIQUE_BOOK($result2['UniqueID'], $borrowrecord['RecordStatus']);
			}
		}
			//if($result_insert_STOCKTAKE){
				$counter++;
			//}
			
//		$now = time();
//	    $your_date = strtotime("2010-01-01");
//	    $datediff = $now - $your_date;
//	    echo floor($datediff/(60*60*24));
	    $penaltyday = $timeManager->dayForPenalty(strtotime($borrowrecord['DueDate']), strtotime($borrowrecord['ReturnedTime']));
	    
		//calcuate the penalty day...
		if ($penaltyday > 0 && $borrowrecord['Fine'] > 0){
//			$BookManager = new BookManager($this->libms);
//			$circulationTypeCode = $BookManager->getCirculationTypeCodeFromBookID($borrowInfo['BookID']);
//			$limits = $this->rightRuleManager->get_limits($circulationTypeCode);
//			$amount = $limits['OverDueCharge'] * $penaltyday;
//			if (  $limits['MaxFine'] > 0 ){
//				$amount = ($limits['MaxFine'] < $amount)? $limits['MaxFine'] : $amount;
//			}
//			if ($amount > 0){
			$dataAry = array(
					'BorrowLogID' => PHPToSQL($borrowID),
					'DaysCount' => PHPToSQL($penaltyday),
					'Payment' => PHPToSQL($borrowrecord['Fine']),
					'PaymentReceived' => PHPToSQL($borrowrecord['Fine']), // to be confirm
					'RecordStatus' => PHPToSQL('SETTLED'),
					'DateCreated' => PHPToSQL($borrowrecord['ReturnedTime']),
					'DateModified' => PHPToSQL($borrowrecord['ReturnedTime']),
					'IsImportData'=> PHPToSQL('1')
					//'RecordStatus' => PHPToSQL('OUTSTANDING')
			);
			//$result[] = $recordManager->newOverDueLog($dataAry);
			$sql = "INSERT INTO `LIBMS_OVERDUE_LOG` SET ";
		 
			foreach($dataAry as $field=>$value)
				$sql .= "`" . $field . "`= ". $value." , ";
			 
			$sql .= "`LastModifiedBy`='{$UserID}'";
			if(!empty($condition)){
				foreach($condition as $field=>$value)
					$tmp_cond[] = "`{$field}` = {$value}";
				$sql .= " WHERE ". implode(' AND ',$tmp_cond);
			}
			if($is_debug){
				echo $sql.'<br>';
			} else {
				$result[] = $libms->db_db_query($sql);
			}
			
//			$result[] = $recordManager->addToUserBalance($result['UserID'],0-$borrowrecord['Fine']);
			////debug_pr($dataAry);
//			}
		}	
		
		
	} //for each 1000
	$libms->db_db_query("delete from `LIBMS_LOAN_RECORD_TMP` order by LoanRecordID limit 1000 ");

} //LOOP until all gone.
$libms->db_db_query("DROP TABLE LIBMS_LOAN_RECORD_TMP");

//update the book location
//debug_pr($book_unique_id);
//debug_pr($location_code);
//for($i=0;$i<count($book_unique_id);$i++){
//	$libms->UPDATE_BOOK_LOCATION($book_unique_id[$i], $location_code[$i]);
//}

$lang_space = (($_SESSION['intranet_session_language'] == 'en') ? " " : "") ? " " : "";

$x = $Lang['libms']['import_book']['Result']['Success'].",";
$x .= $lang_space.$Lang['libms']['import_book']['total_record'].":";
$x .= $counter;
$x .= $lang_space.$Lang["libms"]["Circulation"]["BookBorrowHistory"];
$i = 1;



if($is_debug) die();


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center"><br /> <br /> <br /> <?= $x ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5"
					align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><?=$import_button?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>