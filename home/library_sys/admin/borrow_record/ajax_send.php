<?php
# using: Henry

#################################
#	Date:	2014-07-03	Henry
#			first version
#################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

# iMail Plus enabled, assigned user only
# to be changed

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");

intranet_auth();
intranet_opendb();

# Create a new interface instance
$linterface = new interface_html();
?>

<script language="javascript">

function jsCancel()
{
	if (confirm("<?=$Lang['libms']["bookmanagement"]['ConfirmToTerminate']?>"))
	{
		//parent.location.reload();	
		self.location='import_borrow_record.php';
	}		
}

function jsClose()
{
	//parent.location.reload();
	self.location='import_borrow_record.php';
}

function jsSendEmail(StartIndex, e) {
  $.ajax({
    url: 'ajax_send_update.php?ProgressIndex='+StartIndex,
    error: function(xhr) {
      //alert('Ajax request error');
    },
    success: function(response) {
      rData = response.split("|");
      CurStatus = rData[0];
      TotalRecipients = rData[1];
      CurRecipients = rData[2];
      if (TotalRecipients>0)
      {
	      ProgressPercentage = Math.round(100*(CurRecipients/TotalRecipients));
	      $('#progressbarA').progressBar(ProgressPercentage);
      }
      $('#ProgressMessage').html("<strong><?=$Lang["libms"]["bookmanagement"]['SendingTo1']?> "+TotalRecipients+" <?=$Lang["libms"]["bookmanagement"]['SendingTo2']?> </strong>");
	  if (CurStatus=="ToContinue")
	  {
	  	jsSendEmail(CurRecipients);
	  } else
	  {
	  	$('#ActionButtonCancel').hide();
	  	$('#ActionButtonClose').show();
	  }
    }
  });
}

$(container).ready(function() {
  jsSendEmail(0);
});

</script>

<form name="formAJ">
<script language="JavaScript" src="/templates/jquery/jquery.progressbar.min.js"></script> 
<script>
	$(document).ready(function() {
		$("#progressbarA").progressBar({barImage: '/images/progress_bar_green.gif'} );
	});
</script>
<style>
	#container { width: 80%; margin-left: 10%; margin-top: 30px;}
</style>
<div id="container" align="center">
	<div style="padding-bottom: 25px;">
		<div id="ProgressMessage"><?=$Lang['MassMailing']['Start2Send']?></div>
		<table align="center">
			<tr><td width="240" align="center"><span class="progressBar" id="progressbarA">0%</span></td></tr>
		</table>
		<p id="ActionButtonCancel"><?= $linterface->GET_ACTION_BTN($button_cancel, "button", "jsCancel()")?></p>
		<p id="ActionButtonClose" style="display:none"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "jsClose()")?></p>
	</div>
</div>


</form>
<?
	
	intranet_closedb();
?>