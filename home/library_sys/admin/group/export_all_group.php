<?php
// using: 
/**
 * Log :
 *
 * Date: 2019-07-30 [Tommy]
 * - File Created
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$rm = new liblibrarymgmt_group_manage();
$lexport = new libexporttext();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (! $admin_right['group management'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$rows = array();
$lexport = new libexporttext();
$result_data = array();

$exportColumn = array(
    $Lang['libms']['group_management'],
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
);
$rows[] = array('');

$groupList = $rm->Get_Group_List();
for ($i = 0; $i < sizeof($groupList); $i++){
    list($groupID, $groupTitle, $groupCode) = $groupList[$i];
    
    $rows[] = array($Lang['libms']['GroupManagement']['GroupTitle'] . ": " . $groupTitle, $Lang['libms']['GroupManagement']['GroupCode'] . ": " . $groupCode);
    $rows[] = array(
        $Lang["libms"]["GroupManagement"]["Name"],
        $Lang['libms']['CirculationManagement']['user_barcode'],
        $Lang['libms']['GroupManagement']['ClassName'],
        $Lang['libms']['GroupManagement']['ClassNumber'],
        $Lang['libms']['GroupManagement']['UserType']
    );
    
    $groupMember = $rm->Get_Group_Member_For_Export($groupID);
    //debug_pr($groupMember);
    
    if(sizeof($groupMember) > 0){
        for($j = 0; $j < sizeof($groupMember); $j++){
            list($user_id, $user_name, $user_barcode, $class_name, $class_number, $user_type) = $groupMember[$j];
            
            $rows[] = array(
                $user_name, 
                $user_barcode ? $user_barcode:' -- ', 
                $class_name ? $class_name: ' -- ', 
                $class_number ? $class_number: ' -- ', 
                $user_type
            );
        }
    }else{
        $rows[] = array(
            $i_no_record_exists_msg
            );
    }
    $rows[] = array(''
        
    );
}

intranet_closedb();

$filename = "all_group_member" . ".csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content, false, false);

?>