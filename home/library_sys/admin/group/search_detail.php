<?php
// IMPORTANT: UTF-8 Encoding
# using:  

######################################################################################
#									Change Log 
######################################################################################
#
#	20160826 Cameron: allow user to edit and update barcode 
#
#	20150203 Ryan : Training Feedback  - Changed to xmsg,
#										 remove alert,
#										 Add Back button,
#										 [Case Number : 2015-0203-1543-49054]
#	20141201 Ryan : eLibrary plus pending developments Item 205 
#
######################################################################################


if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// error_reporting(E_ERROR | E_WARNING | E_PARSE);
// ini_set('display_errors', 1);


### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/User.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['group management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
## Init 
$CurTab = 'Borrowed';
$Page = (isset($Page) && $Page != '') ? $Page : 'Borrowed';
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagement";
$uID = (isset($uID) && $uID != '') ? $uID : '';
$libms->MODULE_AUTHENTICATION($CurrentPage);
$home_header_no_EmulateIE7 = true;
$linterface = new interface_html();

# User Object
$user = new User($uID);
$user->getGroupTitle();
$user->getOverdueRecords();
$user->getBorrowedHistory();

# For Ajax  
$userString = base64_encode(serialize($user));


# Fields 
$NameField = $Lang['libms']['SQL']['UserNameFeild'];
$MemberName = $user->userInfo[$NameField];
$GroupTitle = $user->userGroupTitle;
$ClassName = $user->userInfo['ClassName'];
$ClassNo = $user->userInfo['ClassNumber'];
$BarCode = $user->userInfo['BarCode'];
$Balance = $user->userInfo['Balance'];
$isSuspended = ($user->userInfo['Suspended']);
$SuspendedDate = $user->userInfo['SuspendedDate'];
$Remarks = $libms->getRemarks($uID);


//$TAGS_OBJ[] = array($Lang['libms']['action']['edit_book']);
//$CurrentPage = "PageGroupManagement";
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["ModuleTitle"],"index.php",0);
$TAGS_OBJ[] = array($Lang["libms"]["SearchUser"]["ModuleTitle"],"search.php?clearCoo=1",1);
$TAGS_OBJ[] = array($Lang["libms"]["UserSuspend"]["ModuleTitle"],"suspend.php",0);
if (isset($IsCopy) && $IsCopy)
{
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Copy'], "");
} else
{
	$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
}
$CurrentPage = $libms->get_book_manage_cur_page($FromPage);
$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$BookSubTab = $libms->get_search_detail_sub_tab($FromPage,$CurTab,$Code,$UniqueID);
?>

<script language="javascript">
var RemarksOri ;
var BarCodeOri;	// user barcode
$().ready( function(){
	ResetsubTab();
	loadInfo('<?=$Page?>');
	SelectSubTab('<?=$Page?>');
	$('.UserSubTab').click(function(){
		var Page = $(this).attr('data-page');
		ResetsubTab();
		SelectSubTab(Page);
	})
	RemarksOri = $('#Remarks').val();
	BarCodeOri = $('#BarCode').val();
});


function submitChange(){
	var remarks = $('#Remarks').val();
	if (($.trim($('#BarCode').val()) != BarCodeOri) && ($.trim(remarks) != RemarksOri)) {
		UpdateBarCodeAndRemarks('BarCodeAndRemarks');
	}
	else if ($.trim($('#BarCode').val()) != BarCodeOri) {
		UpdateBarCodeAndRemarks('BarCode');
	}
	else if ($.trim(remarks) != RemarksOri) {
		UpdateRemarks();
	}
}

function UpdateBarCodeAndRemarks(updateFields) {
	var remarks = $('#Remarks').val();
	$.ajax({
		dataType: "json",
        type : "POST",
        url : "ajax_update_user_barcode.php",
        data: {	Remarks: remarks, 
        		uID: "<?=base64_encode($uID)?>",
        		BarCode: $.trim($('#BarCode').val()),
        		UpdateFields: updateFields},
        success : show_update_result,
        error: show_ajax_error
    });
}

function show_update_result(ajaxReturn) {
	if (ajaxReturn != null){
		if (ajaxReturn.success) {
			RemarksOri = $('#Remarks').val();
			$('#Remarks').val($.trim(RemarksOri));
			BarCodeOri = $('#BarCode').val();
			$('#BarCode').val($.trim(BarCodeOri));
			Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
		}
		else {
			Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>: ' + ajaxReturn.failReason);
		}
	}
}

function UpdateRemarks(){
	var remarks = $('#Remarks').val();	 
		$.ajax({
	        type : "POST",
	        url : "ajax_save_remarks.php",
	        data: {Remarks: remarks, uID: '<?=base64_encode($uID)?>'},
	        success : function(msg) {
					if(msg == 1){
						RemarksOri = remarks;
						Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
						$('#Remarks').val($.trim(RemarksOri));
					}else{
						Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
					}
					
	        }
	    });
}

function SelectSubTab(Tab){
	$('#'+Tab+'Info').parent().addClass('selected');
	$('#'+Tab).show();
}

function ResetsubTab(){
	$('.UserSubTab').parent().removeClass('selected');
	$('#Reserved').hide();
	$('#Borrowed').hide();
	$('#Overdue').hide();
}


function loadInfo(Page){
	$.ajax({
	        type : "POST",
	        url : "ajax_search_user_detail.php",
	        data: {userObj : '<?=htmlspecialchars($userString)?>', curPage : Page},
	        success : function(msg) {
					$('#Content').html(msg);
	        }
	    });
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
<table class="form_table" width="100%">
<tr>
  <td align="left"><font face="微軟正黑體" size="+1"><b><?=$MemberName?></b></font></td>
</tr>
</table>
<br style="clear:both" />	
<table width="90%" border="0" align="center">
	<tr> 
		<td class="main_content">
			<div class="table_board">
                <p class="spacer"></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><?=$Lang["libms"]["report"]["username"]?></td>
                        	<td width="33%"><?=$MemberName?></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?=$Lang["libms"]["GroupManagement"]["GroupTitle"]?></span></td>
                         	<td width="33%"><?=$GroupTitle ? $GroupTitle : '--'?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?=$Lang["libms"]["report"]["ClassName"]?></span></td>
                        	<td><?=$ClassName ? $ClassName : '--'?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?=$Lang["libms"]["report"]["ClassNumber"]?></span></td>
                         	<td><?=$ClassNo ? $ClassNo : '--'?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?=$Lang["libms"]["CirculationManagement"]["user_barcode"]?></span></td>
                        	<td><input type="text" id="BarCode" name="BarCode" value="<?=($BarCode ? $BarCode : '--')?>"></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?=$Lang["libms"]["CirculationManagement"]["left_pay"] ?></span></td>
                         	<td id="UserBalance"><?echo ($Balance==0) ? '--' : '<font color="red">$'.-$Balance.'</font>'?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?=$Lang["libms"]["UserSuspend"]["ModuleTitle"]?></span></td>
                        	<td><?=($isSuspended) ? $Lang["libms"]["UserSuspend"]["SuspendedStatus"] : '--'?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?=$Lang["libms"]["UserSuspend"]["SuspendedDate"] ?></span></td>
                         	<td><?=($isSuspended) ? $SuspendedDate : '--'?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?=$Lang["libms"]["SearchUser"]["Remark"]?></span></td>
                        	<td colspan="4"><textarea id="Remarks" name="Remarks" class="textboxtext"><?echo (!$Remarks)? '' : $Remarks[0]['RemarkContent'];?></textarea></td>
						</tr>		
                  	</tbody>
				</table>
				<div style="text-align:center">
                    <p class="spacer"></p>
                    <input type="button" name="SubmitBtn" id="SubmitBtn" class="formbutton_v30 print_hide " onclick="submitChange();" value="<?=$Lang["libms"]["SearchUser"]["UpdateRemarks"]?>">
				</div>
				<?=$BookSubTab?>
				<p></p>
                <div id="Content">
                </div>
		</td>
	</tr>
	<tr>
		<td style="text-align:center">
			<input type="submit" name="SubmitBtn" id="SubmitBtn" class="formbutton_v30 print_hide " value="<?=$Lang['Btn']['Back']?>">
		</td>
	</tr>
</table>
<?php
intranet_closedb();
?>