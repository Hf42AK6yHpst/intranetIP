<?php
######################################################################################
#									Change Log 
######################################################################################
#
#	20170209 Cameron: strip slashes after call lib.php for php5.4+
#
#	20141201 Ryan : eLibrary plus pending developments Item 205
#
######################################################################################

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
intranet_opendb();
$libms = new liblms();
if(!get_magic_quotes_gpc()) {
	$_POST['Remarks'] = stripslashes($_POST['Remarks']);
}
$Remarks = trim($_POST['Remarks']);
$uID = base64_decode($uID);
$update = $libms->getRemarks($uID);
if($update){
	$rs = $libms->updateRemarks($uID,$Remarks);
}else{
	$rs = $libms->saveRemarks($uID,$Remarks);
}

intranet_closedb();
echo $rs;
?>
