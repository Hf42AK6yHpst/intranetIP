<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}



// using kenneth chung
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage_ui.php");

$GroupID = $_REQUEST['GroupID'];

intranet_opendb();

$GroupManage = new liblibrarymgmt_group();

$GroupManageUI = new liblibrarymgmt_group_manage_ui();

$NameField = $Lang['libms']['SQL']['UserNameFeild'];
$sql = 'Select
			u.UserID,						
			u.'.$NameField.' as MemberName,
			u.BarCode,
			u.UserType,
			u.ClassNumber,
			u.ClassName
			
		From
			LIBMS_CLASS_MANAGEMENT_GROUP_USER gu
		INNER JOIN
		    LIBMS_USER u
		ON
		    u.UserID = gu.UserID
		WHERE 
			GroupID = \''.$GroupID.'\'
		AND
			u.RecordStatus = 1	
		ORDER BY u.ClassLevel, u.ClassName, u.ClassNumber
		';
	//debug_r($sql);
	$GroupMemberList = $GroupManage->returnArray($sql);

for($i=0; $i< sizeof($GroupMemberList); $i++) {
	$class_info = ($GroupMemberList[$i]['ClassName']!="" && $GroupMemberList[$i]['ClassNumber']!="") ?  '('.$GroupMemberList[$i]['ClassName']."-".$GroupMemberList[$i]['ClassNumber']. ')' : "";			
	$AddUserID[] = $GroupMemberList[$i]['MemberName'].$class_info;
}

echo ($AddUserID?implode(', ',$AddUserID):'--');

intranet_closedb();
?>
