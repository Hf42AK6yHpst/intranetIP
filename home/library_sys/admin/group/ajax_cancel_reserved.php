<?php
/*
 * 	20170802 Cameron: fix book title containing back slash problem by applying remove_dummy_chars_for_json() to return table
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// using 

/*
 * 	2015-12-02 [Cameron]
 * 		- fix bug on showing $ReturnMsg
 * 
 *	26-01-2010 Ivan
 *	- Show ClassName and ClassNumber for Student only
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/User.php");
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();
$uID = base64_decode($_POST['uID']);

$libms->REMOVE_RESERVATION($ReservationIDs);


# Copy from reserved_remove.php , email notice
		$CodesList = implode('","',$ReservationIDs);  
	$sql = "
		SELECT BookTitle, UserID
		FROM LIBMS_RESERVATION_LOG rl
		JOIN LIBMS_BOOK b
			ON rl.BookID = b.BookID
		WHERE ReservationID IN (\"$CodesList\");
	";
	
	$result = $libms->returnArray($sql);
if (!$result)
 	$ReturnMsg = '0|=|'.$Lang['libms']['Circulation']['action_fail'];
else{
	$ReturnMsg = '1|=|'.$Lang['libms']['Circulation']['action_success'];
	
	foreach($result as $row){
		$subject = $Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['subject'];
		$body  =  sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['mail_body'], date("Y-m-d"),$row['BookTitle']);
		
		do_email($row['UserID'], $subject, $body); //Henry Modified 20130916
	}
}

$User = new User($uID);
$ReservedTable = $libms->Get_Reserved_Books_Table($User,'Reserved');

$return = array();
$return [] = $ReturnMsg;         //Return Msg
$return [] = remove_dummy_chars_for_json($ReservedTable);	
$jsonObj = new JSON_obj();
$jsonencode = $jsonObj->encode($return); 
echo $jsonencode;
intranet_closedb();
?>
