<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}




$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
//include_once($PATH_WRT_ROOT."includes/ChromePhp.php");

$GroupID = $_REQUEST['GroupID'];
$Show = $_REQUEST['Show'];

intranet_auth();
intranet_opendb();

$GroupManageUI = new liblibrarymgmt_group_manage_ui();


$result = $GroupManageUI->Get_Group_Edit_Form($GroupID,$Show);
echo $result;

 
 
intranet_closedb();
?>

<script language="JavaScript">
	Thick_Box_Init();
	  Init_JEdit_Input("div.jEditInput");
	  Init_JEdit_InputCode("div.jEditInputCode");
</script>