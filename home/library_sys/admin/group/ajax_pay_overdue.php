<?php
// using 
######################################################################################
#									Change Log 
######################################################################################
#
#	20170802 Cameron: fix book title containing back slash problem by applying remove_dummy_chars_for_json() to return table  
#
#	20150204 Ryan : Training Feedback  - Changed Return Msg to IP25 xmsg  
#										 [Case Number : 2015-0203-1543-49054]
#	20141201 Ryan : eLibrary plus pending developments Item 205
#
######################################################################################

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/User.php");
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();

$uID = base64_decode($_POST['uID']);
$pay_by_ePayment = $plugin['payment'] && $sys_custom['eLibraryPlus']['PayBy_ePayment'];
$User = new User($uID);

$result =array();
if(!empty($OverdueLogIDs)){
	foreach ((array)$OverdueLogIDs as $OverdueLogID){
		$amount = $payAmount[$OverdueLogID];
		$result[$overdueID]=$User->pay_overdue($amount,$OverdueLogID,$pay_by_ePayment);
	}
}

$error=in_array(false, $result);
if ($error)
 	$ReturnMsg = '0|=|'.$Lang['libms']['Circulation']['action_fail'];
else
	$ReturnMsg = '1|=|'.$Lang['libms']['Circulation']['action_success'];

$User = new User($uID);
$User->getOverdueRecords();
$User->getBorrowedHistory();
$OverdueTable = $libms->Get_Overdue_Records_Table($User,'Overdue');
$borrowedTable = $libms->Get_Borrowed_Books_Table($User,'Overdue');
$OverdueBalance = $User->userInfo['Balance'];

$return = array();
$return [] = $ReturnMsg;         //Return Msg
$return [] = remove_dummy_chars_for_json($OverdueTable);		 //Refresh Overdue Table
$return [] = remove_dummy_chars_for_json($borrowedTable);		 //Refresh Borrowed Table
$return [] = ($OverdueBalance==0) ? '--' : '<font color="red">$'.-$OverdueBalance.'</font>'; // Refresh User Balance
$jsonObj = new JSON_obj();
$jsonencode = $jsonObj->encode($return); 
echo $jsonencode;
intranet_closedb();
?>
