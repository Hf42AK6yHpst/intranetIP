<?php
// using: Rita

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['stock-take and write-off'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();
$linterface = new interface_html();

# Page Settings
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageWriteOff";

//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 0);
$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 1);

$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

$btnAry[] = array('import', 'import_write_off.php');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

#################### Stocktake Scheme Selection Box Start ######################
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('',1);
$stockTakeSchemeArr = $libms->returnArray($sql);
## Default StocktakeSchemeID
if($stocktakeSchemeID=='')
{
	$stocktakeSchemeID = $stockTakeSchemeArr[0]['StocktakeSchemeID'];
}
$numOfstockTakeSchemeArr = count($stockTakeSchemeArr);
for($i=0;$i<$numOfstockTakeSchemeArr;$i++){
	$thisStockTakeSchemeId = $stockTakeSchemeArr[$i]['StocktakeSchemeID'];
	$thisStockTakeDisplay = $stockTakeSchemeArr[$i]['SchemeTitle'] . ' ~ (' .  $stockTakeSchemeArr[$i]['StartDate']  . ' - ' .  $stockTakeSchemeArr[$i]['EndDate'] . ')';
	$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);

	$thisStartDateTime = strtotime($stockTakeSchemeArr[$i]['StartDate']);;
	$thisEndDateTime = strtotime($stockTakeSchemeArr[$i]['EndDate']);
	$currentDate = strtotime(date("Y-m-d"));
	
	if(($thisStartDateTime<=$currentDate) && ($thisEndDateTime>=$currentDate)){
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = true;
	}else{
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = false;
	}
	
}

$stockTakeSelectionBox = $linterface->GET_SELECTION_BOX($stockTakeSchemeSelectionArr, 'name="StocktakeSchemeID" id="StocktakeSchemeID"' , '', $stocktakeSchemeID);
#################### Stocktake Scheme Selection Box End ########################

//############################### Book Status Start ###############################
//$y = 0;
//foreach ($Lang['libms']['book_status'] as $key => $value)
//{
//	$BookStatus[$y][0] = $key;
//	$BookStatus[$y][1] = $value;
//	$y++;
//}
//$bookStatusSelectionBox = $linterface->GET_SELECTION_BOX($BookStatus, " name='BookStatus' id='BookStatus' ", '', 'NORMAL');
//
//############################### Book Status End ###############################
//

############################### Stocktake Status End ###############################
$i = 0;
$stocktakeStatus = array();
foreach ($Lang['libms']['stocktake_status'] as $key => $value)
{
	$stocktakeStatus[$i][0] = $key;
	$stocktakeStatus[$i][1] = $value;
	$i++;
}
$stockTakeStatusSelectionBox = $linterface->GET_SELECTION_BOX($stocktakeStatus, " name='selectedStocktakeStatus' id='stocktakeStatus' ", '', 'NORMAL');
############################### Stocktake Status End ###############################

		
$htmlAry['writeOffTable'] = '';
$htmlAry['writeOffTable'] .= '<table class="form_table">';
	# Stocktake Scheme
	$htmlAry['writeOffTable'] .= '<tr>';
	$htmlAry['writeOffTable'] .= '<td class="field_title">'.$Lang['libms']['bookmanagement']['stocktakeRecord'].'</td>';
	$htmlAry['writeOffTable'] .= '<td>' .$stockTakeSelectionBox;
	$htmlAry['writeOffTable'] .= '</td>';
	$htmlAry['writeOffTable'] .= '</tr>';
	
//	# Book Status
//	$htmlAry['writeOffTable'] .= '<tr>';
//	$htmlAry['writeOffTable'] .= '<td class="field_title">'.$Lang["libms"]["report"]["book_status"].'</td>';
//	$htmlAry['writeOffTable'] .= '<td>' .$bookStatusSelectionBox;
//	$htmlAry['writeOffTable'] .= '</td>';
//	$htmlAry['writeOffTable'] .= '</tr>';
	
	# Stock Take Status
	$htmlAry['writeOffTable'] .= '<tr>';
	$htmlAry['writeOffTable'] .= '<td class="field_title">'.$Lang['libms']['bookmanagement']['StocktakeStatus'].'</td>';
	$htmlAry['writeOffTable'] .= '<td >' .$stockTakeStatusSelectionBox;
	$htmlAry['writeOffTable'] .= '</td>';
	$htmlAry['writeOffTable'] .= '</tr>';
		
//	# Scope
//	$htmlAry['writeOffTable'] .= '<tr>';
//	$htmlAry['writeOffTable'] .= '<td class="field_title">'.$Lang['libms']['bookmanagement']['scope'].'</td>';
//	$htmlAry['writeOffTable'] .= '<td>';
//	$htmlAry['writeOffTable'] .= $linterface->Get_Radio_Button('SelectedBooks','scope', 'SelectedBooks', 0, 'SelectedBooks',$Lang['libms']['bookmanagement']['selectedBooks'] );
//	$htmlAry['writeOffTable'] .= $linterface->Get_Radio_Button('AllBooks','scope', 'AllBooks', 1, 'AllBooks', $Lang['libms']['bookmanagement']['allBooks']);
//	$htmlAry['writeOffTable'] .= '</td>';
//	$htmlAry['writeOffTable'] .= '</tr>';
	
	# Submit
	$htmlAry['writeOffTable'] .= '<tr>';
	$htmlAry['writeOffTable'] .= '<td colspan="2" align="center"><center>';
	$htmlAry['writeOffTable'] .= $linterface->GET_ACTION_BTN($button_submit, "submit");
	$htmlAry['writeOffTable'] .= '<center></td>';
	$htmlAry['writeOffTable'] .= '</tr>';
	
$htmlAry['writeOffTable'] .= '</table>';	


?>
<script>
function js_form_submit(obj, type, locationCode){
	
//	if(type=="changeScheme"){
//		$('#form1').attr("action", "index.php");		
//	}else{
//		$('#form1').attr("action", "stock_take_list.php");
//		$('#selectedLocationCode').val(locationCode);
//	}
	obj.submit();	
}
</script>
<form name="form1" id="form1" method="post" action="write_off_list.php" >
<!--###### Content Board Start ######-->
                  <div class="content_top_tool">
				  	<?=$htmlAry['contentTool']?>
					<br style="clear:both;">
				  </div>
				  
                  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                  
                    <tr> 
                      <td width="13" >&nbsp;</td>
                      <td class="main_content"><!---->
                       <!--#### Navigator start ####-->
                		<?=$htmlAry['writeOffTable'] ?>	
					  
				      <p>&nbsp;</p></td>
                      <td width="11">&nbsp;</td>
                    </tr>
                  </table>
			  <!--###### Content Board End ######-->
			  
</form>

<?php
//echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>