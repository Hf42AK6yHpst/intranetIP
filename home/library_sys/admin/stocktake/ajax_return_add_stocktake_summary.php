<?php
// Using: Henry

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();
$barcode = $_POST['barcode'];
$ID = $_POST['ID'];
$stocktakeSchemeID = $_POST['stocktakeSchemeID'];
$pageLocationCode = $_POST['pageLocationCode'];

$sql="select RecordStartDate, RecordEndDate from LIBMS_STOCKTAKE_SCHEME where StocktakeSchemeID='".$stocktakeSchemeID."'";
$result = $libms->returnArray($sql);
$stocktakeRecordEndDate = $result[0][1];
$stocktakeRecordStartDate = $result[0][0];
// 20140408
$uniqueBookArr = $libms->GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION_PERIOD($stocktakeRecordStartDate,$stocktakeRecordEndDate);
// 20140408
//for the item that has no location
$tempCount = 0;
$tempUniqueBookArr = $uniqueBookArr;
foreach($tempUniqueBookArr as $key => $aUniqueBorrArr){
	if($aUniqueBorrArr['LocationCode'] == ''){
		$tempCount += $aUniqueBorrArr['UniqueBookAmount'];
		$tempUniqueBookArr[$key]['UniqueBookAmount'] = $tempCount;
	}
}
$uniqueBookLocationCodeAssocArr = BuildMultiKeyAssoc($tempUniqueBookArr, 'LocationCode'); 
$totalBook = $uniqueBookLocationCodeAssocArr[$pageLocationCode]['UniqueBookAmount'];

# get the removed location (Henry20140502)
$locationNotExist = $libms->GET_LOCATION_NOT_EXIST();
//Herny 20140430
if($pageLocationCode == '-1'){
	$totalBook = 0;
	for($i=0;$i<count($locationNotExist);$i++){
		$totalBook += $uniqueBookLocationCodeAssocArr[$locationNotExist[$i]]['UniqueBookAmount'];
	}
}
$thisStocktakeLogInfo = $libms->GET_STOCKTAKE_LOG('', $stocktakeSchemeID, '', '', $pageLocationCode, 1);
$FoundBook = count($thisStocktakeLogInfo);
// 20140408
$thisWriteoffLogInfo = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', 'Array','','','',$pageLocationCode,false);
$FoundBook += count($thisWriteoffLogInfo);
// 20140408
echo $FoundBook.",".$totalBook;
?>

