<?php
// using: 

/*************************************************************
 * 
 * 	20200720 (Henry)
 * 			Bug fix for fail to import writeoff book [Case#F190223]
 *  
 * 	20160426 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 *  
 * 	20160425 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "write-off" [Case#K94933]
 * 
 *  20150610 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 *	20131126 (Henry)
 * 			Created this file
 * 
 *************************************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['write-off'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
//$PAGE_NAVIGATION[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php");
$PAGE_NAVIGATION[] = array($Lang["libms"]["bookmanagement"]["import"], "");
$CurrentPage = "PageWriteOff";

$linterface = new interface_html("libms.html");

# Top menu highlight setting
//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 0);
$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 1);
//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);


# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$filepath = $_FILES['file']['tmp_name'];
$filename = $file_name;
$uploadSuccess = true;
if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename))
	{
		$uploadSuccess = false;
		$xmsg2 = $Lang['libms']['import_book']['upload_fail']." ".$Lang['plupload']['invalid_file_ext'];
	}

if($uploadSuccess){
	
	$file_format = array(
			'Barcode',
			'WriteOffDate',
			'Reason'
	);

	$flagAry = array(
			'1',
			'1',
			'1'
	);
	$format_wrong = false;


	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($filepath,"","",$file_format,$flagAry);

	$counter = 1;
	$insert_array = array();
	$my_key = array();

	if(is_array($data))
	{
		$col_name = array_shift($data);
	}

	for($i=0; $i<sizeof($file_format); $i++)
	{
		if (strtoupper($col_name[$i])!=strtoupper($file_format[$i]))
		{
			$format_wrong = true;
			break;
		}
	}


	if($format_wrong)
	{
		header("location: import_write_off.php?xmsg=wrong_header");
		exit();
	}
	if(sizeof($data)==0)
	{
		header("location: import_write_off.php?xmsg=import_no_record");
		exit();
	}
	
	//create a tmp table [start]
	$sql = "DROP TABLE LIBMS_WRITEOFF_LOG_TMP";
    $libms->db_db_query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS `LIBMS_WRITEOFF_LOG_TMP` (
	  `WriteOffID` int(11) NOT NULL auto_increment,
	  `StocktakeSchemeID` int(11) NOT NULL,
	  `BookID` int(11) NOT NULL,
	  `BookUniqueID` int(11) NOT NULL,
	  `Result` varchar(64) default NULL,
      `PreviousBookStatus` varchar(64) default NULL,
      `Reason` varchar(128) default NULL,
      `WriteOffDate` datetime default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (WriteOffID),
	  index BookUniqueID (BookUniqueID),
  		UNIQUE KEY `StocktakeLog` (`StocktakeSchemeID`, `BookID`, `BookUniqueID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    $libms->db_db_query($sql);
    //create a tmp table [end]
    
    ### List out the import result
	$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['bookmanagement']['barcode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['bookmanagement']['WriteOffDate'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['bookmanagement']['Reason'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['General']['Warning']."</td>";
	$x .= "</tr>";
	
    $error_occured = 0;
    $hiddenField = "";
   
	foreach ($data as $record) {
		$sql = "Select BookID, UniqueID, RecordStatus From LIBMS_BOOK_UNIQUE WHERE BarCode = '".$record['0']."'";
		$result = current($libms->returnArray($sql));
		
		if(!$result){
			$error_occured = 1;
			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
					$x .= "<td class=\"$css\">".($y)."</td>";
					$x .= "<td class=\"$css\">".$record['0']."</td>";
					$x .= "<td class=\"$css\">".$record['1']."</td>";
					$x .= "<td class=\"$css\">".$record['2']."</td>";
					$x .= "<td class=\"$css\">";
					$x .= "<div style='color:red'>".$Lang["libms"]["bookmanagement"]["BarcodeNotExist"]."</div>";	
					$x.="</td>";
					$x .= "</tr>";
		}
		else if(trim($record['1']) != "" && !preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', trim($record['1']))){
			$error_occured = 1;
			$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
					$x .= "<td class=\"$css\">".($y)."</td>";
					$x .= "<td class=\"$css\">".$record['0']."</td>";
					$x .= "<td class=\"$css\">".$record['1']."</td>";
					$x .= "<td class=\"$css\">".$record['2']."</td>";
					$x .= "<td class=\"$css\">";
					$x .= "<div style='color:red'>".$Lang["libms"]["open_time"]["datemsg"]."</div>";	
					$x.="</td>";
					$x .= "</tr>";
		}
		else{
			$insert_to_db = array(
					'StocktakeSchemeID'=> CCTOSQL($StocktakeSchemeID),
					'BookID'=> CCTOSQL($result['BookID']),
					'BookUniqueID'=> CCTOSQL($result['UniqueID']),
					'Result'=> CCTOSQL("WRITEOFF"),
					'PreviousBookStatus'=> CCTOSQL($result['RecordStatus']),
					'Reason' => CCTOSQL($record['2']),
					'WriteOffDate' => (trim($record['1'])!=""?CCTOSQL(trim($record['1'])):'now()')
			);
			$result2 = $libms->INSERT2TABLE('LIBMS_WRITEOFF_LOG_TMP', $insert_to_db);
		}
		
		if ($result2==false){
			//capture error
			 //debug_pr($insert_to_db);
		}

		$insert_to_db = array();
	}

}


$sql = "SELECT BookUniqueID
		FROM `LIBMS_WRITEOFF_LOG_TMP` ";
$result = $libms->returnArray($sql);

$y = 3;
 

$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_write_off.php'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_write_off.php'");
	 
	$prescan_result =  $Lang['libms']['import_book']['upload_success_ready_to_import'].count($result);
	$x = $prescan_result;
}


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" action="import_write_off_result.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><?=$x?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $import_button ?>
			</td>
		</tr>
	</table>
	<?=$hiddenField?>
</form>
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>