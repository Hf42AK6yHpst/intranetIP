<?php
/*
 * Using:
 *
 * Purpose: export invalid records in import stock take confirm step
 *
 * 2018-05-04 Cameron
 * - create this file
 */
@SET_TIME_LIMIT(216000);
ini_set('memory_limit', - 1);

$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

// in all book management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (! $admin_right['stock-take'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$sql = "SHOW TABLES LIKE 'LIBMS_STOCKTAKE_LOG_ERROR_TMP'";
$rs = $libms->returnResultSet($sql);
if (count($rs[0])) {
    if ($_POST['HasLocationCode']) {
        $sql = "SELECT Barcode, LocationCode, Error FROM LIBMS_STOCKTAKE_LOG_ERROR_TMP ORDER BY RecordID";
    } else {
        $sql = "SELECT Barcode, Error FROM LIBMS_STOCKTAKE_LOG_ERROR_TMP ORDER BY RecordID";
    }
} else {
    $sql = '';
}

$exportColumn = array();
$exportColumn[0][] = $Lang['libms']['bookmanagement']['barcode'];
if ($_POST['HasLocationCode']) {
    $exportColumn[0][] = $Lang['libms']['bookmanagement']['newLocationCode'];
}
$exportColumn[0][] = $Lang['General']['Warning'];
$nrFields = count($exportColumn[0]);

$limit_upper = 2500; // reduce the memory usage in each loop
$limit_i = 0;
$max_count = 800;
$retrieve_done = false;
$rows = array();
$export_content = "";

$lexport = new libexporttext();

if ($sql != '') {
    while (! $retrieve_done && $limit_i < $max_count) {
        $sql_limit_start = $limit_i * $limit_upper;
        
        $rows = $libms->returnArray($sql . " limit {$sql_limit_start}, {$limit_upper}", null, 2); // return numeric array
        $nrRows = count($rows);
        $limit_i ++;
        $retrieve_done = ($nrRows < $limit_upper);
        
        $result_data = array();
        for ($i = 0; $i < $nrRows; $i ++) {
            $tmpRow = array();
            for ($j = 0; $j < $nrFields; $j ++) { // retrieved fields are more than expected fields, get only the requested fields
                $tmpRow[] = $rows[$i][$j];
            }
            $result_data[] = $tmpRow;
            unset($tmpRow);
        }
        unset($rows);
        $rows = array();
        
        $export_content .= ($export_content ? "\r\n" : "") . $lexport->GET_EXPORT_TXT($result_data, $exportColumn, "\t", "\r\n", "\t", 0, '11');
        
        $exportColumn = array(); // pass column header for the 1st time only
    }
}

intranet_closedb();

$filename = "Invalid_record_for_import_stocktake_" . date("Ymd") . ".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");
?>