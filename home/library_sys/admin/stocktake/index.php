<?php
// using: 

/*************************************************************
 * 
 * 	2017-01-12 (Cameron)
 * 			add parameter clearCoo=1 to import_stock_take.php
 * 
 * 	20160426 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 *  
 * 	20160425 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "stock-take" [Case#K94933]
 * 
 *  20150610 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 *	20150317 (Henry)
 *			add para to call GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION_PERIOD()
 * 
 *	20140409 (Henry)
 *			for the amendance of stocktake report
 * 
 *	20140307 (Henry)
 * 			fix bug with the percentage of the progress. (exclude the count of wirteoff book in STOCKTAKE_LOG)
 * 
 *	20131125 (Henry)
 * 			added import button to allow user import the stock-take record
 * 
 *	20131122 (Henry)
 * 			fix bug with the percentage of the progress. (also count the write-off book)
 * 
 *************************************************************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['stock-take'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();
$linterface = new interface_html();

# Page Settings
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageStockTake";

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 1);
//$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 0);
$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-takeProcess'], "stock_take_process.php", 0);

$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

$btnAry[] = array('import', 'import_stock_take.php?clearCoo=1');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

# Get Post Value
$stocktakeSchemeID = $_POST['StocktakeSchemeID'];

$thisShowStocktakeBtn[$stocktakeSchemeID] = true;
#################### Stocktake Scheme Selection Box Start ######################
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('',1);
$stockTakeSchemeArr = $libms->returnArray($sql);
## Default StocktakeSchemeID
if($stocktakeSchemeID=='')
{
	$stocktakeSchemeID = $stockTakeSchemeArr[0]['StocktakeSchemeID'];
}
$numOfstockTakeSchemeArr = count($stockTakeSchemeArr);
for($i=0;$i<$numOfstockTakeSchemeArr;$i++){
	$thisStockTakeSchemeId = $stockTakeSchemeArr[$i]['StocktakeSchemeID'];
	$stocktakeRecordEndDate[$thisStockTakeSchemeId] = $stockTakeSchemeArr[$i]['RecordEndDate'];
	$stocktakeRecordStartDate[$thisStockTakeSchemeId] = $stockTakeSchemeArr[$i]['RecordStartDate'];
	$thisStockTakeDisplay = $stockTakeSchemeArr[$i]['SchemeTitle'] . ' ~ (' .  $stockTakeSchemeArr[$i]['StartDate']  . ' - ' .  $stockTakeSchemeArr[$i]['EndDate'] . ')';
	$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);

	$thisStartDateTime = strtotime($stockTakeSchemeArr[$i]['StartDate']);;
	$thisEndDateTime = strtotime($stockTakeSchemeArr[$i]['EndDate']);
	$currentDate = strtotime(date("Y-m-d"));
	
	if(($thisStartDateTime<=$currentDate) && ($thisEndDateTime>=$currentDate)){
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = true;
	}else{
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = false;
	}
	
}

$stockTakeSelectionBox = $linterface->GET_SELECTION_BOX($stockTakeSchemeSelectionArr, 'name="StocktakeSchemeID" id="StocktakeSchemeID" onChange="javascript:js_form_submit(document.form1, \'changeScheme\')"', '', $stocktakeSchemeID);
#################### Stocktake Scheme Selection Box End ########################

# Location Info
$htmlAry['locationRowDisplay'] = '';
$locationInfo = $libms->GET_LOCATION_INFO('',1);

# get the removed location (Henry20140502)
$locationNotExist = $libms->GET_LOCATION_NOT_EXIST();

# Unique Book Info 
//$uniqueBookArr = $libms->GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION();
//for the amendance of stocktake report [Start] 20140408
$uniqueBookArr = $libms->GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION_PERIOD($stocktakeRecordStartDate[$stocktakeSchemeID],$stocktakeRecordEndDate[$stocktakeSchemeID], $stocktakeSchemeID);
//for the amendance of stocktake report [End] 20140408
//for the item that has no location
$tempCount = 0;
$tempUniqueBookArr = $uniqueBookArr;
foreach($tempUniqueBookArr as $key => $aUniqueBorrArr){
	if($aUniqueBorrArr['LocationCode'] == ''){
		$tempCount += $aUniqueBorrArr['UniqueBookAmount'];
		$tempUniqueBookArr[$key]['UniqueBookAmount'] = $tempCount;
	}
}
$uniqueBookLocationCodeAssocArr = BuildMultiKeyAssoc($tempUniqueBookArr, 'LocationCode'); 
//debug_pr($uniqueBookLocationCodeAssocArr);
//debug_pr($locationInfo);
################################### Table Headers ###########################################
$htmlAry['stocktakeLocationDisplay'] = '';
$htmlAry['stocktakeLocationDisplay'] .= '<table class="common_table_list view_table_list">';
	$htmlAry['stocktakeLocationDisplay'] .= '<thead>';
		$htmlAry['stocktakeLocationDisplay'] .= '<tr>';
			$htmlAry['stocktakeLocationDisplay'] .= '<th>'.$Lang['libms']['bookmanagement']['location'].'</th>';
			$htmlAry['stocktakeLocationDisplay'] .= '<th><center>'.$Lang['libms']['bookmanagement']['total'].'</center></th>';
			$htmlAry['stocktakeLocationDisplay'] .= '<th><center>'.$Lang['libms']['stocktake_status']['FOUND'].' / '.$Lang['libms']['stocktake_status']['WRITEOFF'].'</center></th>';
			$htmlAry['stocktakeLocationDisplay'] .= '<th><center>'.$Lang['libms']['bookmanagement']['progress'].'</center></th>';
			$htmlAry['stocktakeLocationDisplay'] .= '<th>'.$Lang['libms']['bookmanagement']['lastRecord'].'</th>';
			$htmlAry['stocktakeLocationDisplay'] .= '<th>'.$Lang['libms']['bookmanagement']['lastTakenBy'] .'</th>';
			$htmlAry['stocktakeLocationDisplay'] .= '<th>&nbsp;</th>';
		$htmlAry['stocktakeLocationDisplay'] .= '</tr>';
	$htmlAry['stocktakeLocationDisplay'] .= '</thead>';
################################### Table Headers End ###########################################

$htmlAry['stocktakeLocationDisplay'] .= '<tbody>';
$numOflocation = count($locationInfo);
$overallTotalArr = array();
$overallNumOfStocktakeLogArr = array();

//Herny 20140430
$locationInfo[$numOflocation]['DescriptionChi'] = '('.$Lang['libms']['bookmanagement']['NoLocation'].')';
$locationInfo[$numOflocation]['DescriptionEn'] = '('.$Lang['libms']['bookmanagement']['NoLocation'].')';
$locationInfo[$numOflocation]['LocationCode'] = '-1';
$numOflocation++;

for($j=0;$j<$numOflocation;$j++){
	$thisProgress = 0;
	
	$thisLocationName = Get_Lang_Selection($locationInfo[$j]['DescriptionChi'],$locationInfo[$j]['DescriptionEn']);
	$thisLocationCode = $locationInfo[$j]['LocationCode'];
	
	# Unique Book Amount 
	$thisLocationUniqueBookAmount = $uniqueBookLocationCodeAssocArr[$thisLocationCode]['UniqueBookAmount'];
	
	//Herny 20140430
	if($thisLocationCode == '-1'){
		$thisLocationUniqueBookAmount = 0;
		for($i=0;$i<count($locationNotExist);$i++){
			$thisLocationUniqueBookAmount += $uniqueBookLocationCodeAssocArr[$locationNotExist[$i]]['UniqueBookAmount'];
			//debug_pr($locationNotExist[$i]);
		}
	}
//	if($thisLocationCode == "LIB")
//		$thisLocationUniqueBookAmount += $uniqueBookLocationCodeAssocArr['']['UniqueBookAmount'];
	# Get Stocktake Log Info 
	$thisStocktakeLogInfo = $libms->GET_STOCKTAKE_LOG('', $stocktakeSchemeID, '', '', $thisLocationCode, 1);
	$numOfthisStocktakeLog = count($thisStocktakeLogInfo);
	//Henry Added 2013-11-22 [start]
	$thisWriteoffLogInfo = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', 'Array','','','',$thisLocationCode,false);
	$numOfthisStocktakeLog += count($thisWriteoffLogInfo);
	/*$thisLocationUniqueBookAmount += count($thisWriteoffLogInfo);*/
	//debug_pr($thisLocationCode);
	//debug_pr($thisStocktakeLogInfo);
	//Henry Added 2013-11-22 [end]

	# Progress
	if($thisLocationUniqueBookAmount!='' && $numOfthisStocktakeLog!=''){
		$thisProgress = number_format($numOfthisStocktakeLog/$thisLocationUniqueBookAmount * 100,2) . '%' ; 
	}
	
	# Modified Date 
	$thisLastRecord = $thisStocktakeLogInfo[0]['DateModified'];
	
	# Modified By 
	$thisUserName = $thisStocktakeLogInfo[0]['UserName'];

	############## Each Location Info Start ################
	$htmlAry['stocktakeLocationDisplay'] .= '<tr>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td width="25%">'. $thisLocationName .'</td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td width="7%" align="center">'.($thisLocationUniqueBookAmount?$thisLocationUniqueBookAmount:$Lang['General']['EmptySymbol']).'</td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td width="7%" align="center">'.($numOfthisStocktakeLog?$numOfthisStocktakeLog:$Lang['General']['EmptySymbol']).'</td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td width="10%" align="center">' . ($thisProgress?$thisProgress:$Lang['General']['EmptySymbol']). '</td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td width="16%">' . ($thisLastRecord?$thisLastRecord:$Lang['General']['EmptySymbol']) . '</td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td width="18%">' . ($thisUserName?$thisUserName:$Lang['General']['EmptySymbol']) . '</td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td width="18%" align="center">';
		$htmlAry['stocktakeLocationDisplay'] .= ($thisShowStocktakeBtn[$stocktakeSchemeID]==true?$linterface->GET_ACTION_BTN($Lang['libms']['bookmanagement']['stock-take'], "button", "javascript:js_form_submit(document.form1, 'stocktakelist', '$thisLocationCode')"):$Lang['General']['EmptySymbol']) ;
		$htmlAry['stocktakeLocationDisplay'] .= '</td>';
	$htmlAry['stocktakeLocationDisplay'] .= '</tr>';
	################ Each Location Info End  #################

}


################################# Over All Info Start ################################
foreach ($uniqueBookArr as $overallUniqueBookArrKey=>$overallUniqueBookArrInfo){
	$thisOverallUniqueBookAmount = $overallUniqueBookArrInfo['UniqueBookAmount'];
	$overallTotalArr[]= $thisOverallUniqueBookAmount;
}

$thisOverallBookUniqueAmount = array_sum($overallTotalArr);
$allLocationStocktakeLogInfo = $libms->GET_STOCKTAKE_LOG('', $stocktakeSchemeID, '', '', '',1);
$numOfAllLocationStocktake = count($allLocationStocktakeLogInfo);
// 20140408 [Start]
$thisOverallWriteoffLogInfo = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', 'Array','','','','',false);
$numOfAllLocationStocktake += count($thisOverallWriteoffLogInfo);
// 20140408 [End]
$totalProgress = number_format($numOfAllLocationStocktake/$thisOverallBookUniqueAmount * 100, 2); 

# Modified Date 
$thisOverallLastRecord = $allLocationStocktakeLogInfo[0]['DateModified'];
	
# Modified By 
$thisOverallUserName = $allLocationStocktakeLogInfo[0]['UserName'];

	$htmlAry['stocktakeLocationDisplay'] .= '<tr>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td align="right"><b>'.$Lang['libms']['bookmanagement']['overall'].'</b></td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td align="center"><b>'.$thisOverallBookUniqueAmount.'</b></td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td align="center"><b>'.$numOfAllLocationStocktake.'</b></td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td align="center"><b>' . $totalProgress . ' %</b></td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td>'.($thisOverallLastRecord?$thisOverallLastRecord:$Lang['General']['EmptySymbol']).'</td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td width="18%">' . ($thisOverallUserName?$thisOverallUserName:$Lang['General']['EmptySymbol']) . '</td>';
		$htmlAry['stocktakeLocationDisplay'] .= '<td align="center">';
		$htmlAry['stocktakeLocationDisplay'] .= $thisShowStocktakeBtn[$stocktakeSchemeID]==true?$linterface->GET_ACTION_BTN($Lang['libms']['bookmanagement']['stock-take'], "button" , "javascript:js_form_submit(document.form1, 'stocktakelist');"):$Lang['General']['EmptySymbol'];
		$htmlAry['stocktakeLocationDisplay'] .= '</td>';
	$htmlAry['stocktakeLocationDisplay'] .= '</tr>';

$htmlAry['stocktakeLocationDisplay'] .= '</tbody>';
$htmlAry['stocktakeLocationDisplay'] .= '</table>';

?>
<script>
function js_form_submit(obj, type, locationCode){
	
	if(type=="changeScheme"){
		$('#form1').attr("action", "index.php");		
	}else{
		$('#form1').attr("action", "stock_take_list.php");
		$('#selectedLocationCode').val(locationCode);
	}
	obj.submit();	
}
</script>
<form name="form1" id="form1"  method="post">
<!--###### Content Board Start ######-->
	              <div class="content_top_tool">
				  	<?=$htmlAry['contentTool']?>
					<br style="clear:both;">
				  </div>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                  
                    <tr> 
                      <td width="13" >&nbsp;</td>
                      <td class="main_content"><!---->
                       <!--#### Navigator start ####-->
                     <table class="form_table">
                        <tr>
                          <td class="field_title"><?=$Lang['libms']['bookmanagement']['stocktakeRecord']?></td>
                          <td colspan="4"><?=$stockTakeSelectionBox?>
							</td>
                        </tr>
                     </table>		
                                
                        <?=$htmlAry['stocktakeLocationDisplay']?>
					  
				      <p>&nbsp;</p></td>
                      <td width="11">&nbsp;</td>
                    </tr>
                  </table>
			  <!--###### Content Board End ######-->
			  
<input type="hidden" name="selectedLocationCode" id="selectedLocationCode"></input>

</form>

<?php
//echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>