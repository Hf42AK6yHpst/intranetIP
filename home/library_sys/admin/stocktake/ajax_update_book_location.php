<?php
// Using: Rita

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();

$bookID = $_POST['bookID'];
$bookUniqueID = $_POST['bookUniqueID'];
$locationCode = $_POST['locationCode'];

$result['success'] = $libms->UPDATE_BOOK_LOCATION($bookUniqueID, $locationCode);


$locationDisplay ='';
//if(!in_array('FALSE',$result)){
	$newLocationInfo = $libms->GET_LOCATION_INFO($locationCode);
	$locationEngName = $newLocationInfo[0]['DescriptionEn'];
	$locationChiName = $newLocationInfo[0]['DescriptionChi'];
	$thislocationName = Get_Lang_Selection($locationChiName,$locationEngName);
	$locationDisplay = $thislocationName . ' (' . $Lang['libms']['bookmanagement']['BookLocationUpdated'] . ')';
	echo $locationDisplay;
//}
?>

