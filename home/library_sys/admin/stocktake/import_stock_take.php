<?php
// using:   

/*************************************************************
 * 
 * 	20170112 (Cameron)
 * 			retrieve fileFormat from cookie by previous action
 * 
 * 	20160426 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 *  
 * 	20160425 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "stock-take" [Case#K94933]
 * 
 *  20150610 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 * 	20141217 (Henry)
 * 			Add remarks and hide "Continue" button if not in stocktake period
 *  20140319 (Tiffany)
 *          Add txt import.
 *          Add js function upload_function, showDiv, hideDiv.
 *	20131125 (Henry)
 * 			Created this file
 * 
 *************************************************************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['stock-take'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

### Clear Cookies
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_stock_take_fileFormat", "fileFormat");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}


$libms = new liblms();
$linterface = new interface_html();

# Page Settings
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageStockTake";

//$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php");
$PAGE_NAVIGATION[] = array($Lang["libms"]["bookmanagement"]["import"], "");

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 1);
//$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 0);
//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);

$btnAry[] = array('import', 'javascript: void(0);');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$sample_file = "stock_take_import.csv";
$csvFile = "<a class=\"tablelink\" href=\"../book/import/csv/get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$sample_file_txt1 = "stock_take_import_txt1.txt";
$txtFile1 = "<a class=\"tablelink\" href=\"../book/import/csv/get_sample_file.php?file=$sample_file_txt1\" target=\"_self\">".$Lang['libms']['bookmanagement']['ClickHereToDownloadSample1']."</a>";

$sample_file_txt2 = "stock_take_import_txt2.txt";
$txtFile2 = "<a class=\"tablelink\" href=\"../book/import/csv/get_sample_file.php?file=$sample_file_txt2\" target=\"_self\">".$Lang['libms']['bookmanagement']['ClickHereToDownloadSample2']."</a>";

$csv_format = "";
$delim = "<br>";
for($i=0; $i<sizeof($Lang["libms"]["bookmanagement"]["ImportStockTake"]); $i++){
	if($i!=0) $csv_format .= $delim;
	$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".numberToLetter($i+1, true)." : ".$Lang["libms"]["bookmanagement"]["ImportStockTake"][$i];
}


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN(); 

$linterface->LAYOUT_START();

#################### Stocktake Scheme Selection Box Start ######################
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('',1);
$stockTakeSchemeArr = $libms->returnArray($sql);
## Default StocktakeSchemeID
if($stocktakeSchemeID=='')
{
	$stocktakeSchemeID = $stockTakeSchemeArr[0]['StocktakeSchemeID'];
}
$numOfstockTakeSchemeArr = count($stockTakeSchemeArr);
for($i=0;$i<$numOfstockTakeSchemeArr;$i++){
	$thisStockTakeSchemeId = $stockTakeSchemeArr[$i]['StocktakeSchemeID'];
	$thisStockTakeDisplay = $stockTakeSchemeArr[$i]['SchemeTitle'] . ' ~ (' .  $stockTakeSchemeArr[$i]['StartDate']  . ' - ' .  $stockTakeSchemeArr[$i]['EndDate'] . ')';
	//$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);

	$thisStartDateTime = strtotime($stockTakeSchemeArr[$i]['StartDate']);;
	$thisEndDateTime = strtotime($stockTakeSchemeArr[$i]['EndDate']);
	$currentDate = strtotime(date("Y-m-d"));
	
	if(($thisStartDateTime<=$currentDate) && ($thisEndDateTime>=$currentDate)){
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = true;
		$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);
	}else{
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = false;
	}
	
}

$stockTakeSelectionBox = $linterface->GET_SELECTION_BOX($stockTakeSchemeSelectionArr, 'name="StocktakeSchemeID" id="StocktakeSchemeID" onChange="javascript:js_form_submit(document.form1, \'changeScheme\')"', '', $stocktakeSchemeID);
if(count($stockTakeSchemeSelectionArr) == 0){
	$stockTakeSelectionBox = '<font color = red>('.$Lang['libms']['bookmanagement']['StocktakeConductedDuringPeriod'].')</font>';
}
#################### Stocktake Scheme Selection Box End ########################

?> 
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<script>

function upload_function(){
	var t="<?=$Lang["libms"]['General']['warnSelectcsvFile']?>";
	if($(':radio[name="fileFormat"]:checked').val()==1){
	   if(!document.getElementById('file').value){
	   	  alert(t);
	   }
	   else{
	      document.form1.action="import_stock_take_confirm.php";	
	      document.form1.submit();
	   }

	}
    else 
	{
       if(!document.getElementById('file_txt').value){
	   	  alert(t);
	   }
	   else{
	      document.form1.action="import_txt_stock_take_confirm.php";	
	      document.form1.submit();
	   }

	}	
}
function showDiv(layername) {
	$('#'+layername).show();
}

function hideDiv(layername) {
	$('#'+layername).hide();
}

$(document).ready(function() {
<?	
	if ($fileFormat=='2') {
		echo "showDiv('txt');hideDiv('csv');";
	}
?>	
});

</script>
<form name="form1" method="post" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['libms']['import_format']?>: </td>
					<td class="tabletext" width="70%">
						<input type="radio" name="fileFormat" id="fileFormat1" value="1" <?=$fileFormat=='1' || empty($fileFormat)? ' checked':''?> onClick="showDiv('csv');hideDiv('txt');" /> <label for="fileFormat1"><?=$Lang['libms']['import_CSV']?></label>  &nbsp;
						<input type="radio" name="fileFormat" id="fileFormat2" value="2" <?=$fileFormat=='2'? ' checked':''?> onClick="showDiv('txt');hideDiv('csv');"/> <label for="fileFormat2"><?=$Lang['libms']['import_TXT']?></label>
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['libms']['bookmanagement']['stocktakeRecord']?></td>
					<td class="tabletext"><?=$stockTakeSelectionBox?></td>
				</tr>
				<tbody id="csv">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$iDiscipline['Import_Source_File']?>: 
						<span class="tabletextremark">
						<?=$Lang['General']['CSVFileFormat']?></span>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="file" id="file">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format?></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
				</tr>
				</tbody>
				<tbody id="txt" style="display:none;">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$Lang['libms']['bookmanagement']['ImportTXT']?>: 
						<span class="tabletextremark">
						<?=$Lang['libms']['bookmanagement']['TXTFormat']?></span>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="file_txt" id="file_txt">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left" rowspan="2"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$txtFile1?> (<?=$Lang["libms"]["book"]["barcode"]?>)</td>
				</tr>
				<tr>
					<td class="tabletext"><?=$txtFile2?> (<?=$Lang["libms"]["book"]["barcode"]?>, <?=$Lang["libms"]["bookmanagement"]["ImportStockTake"][1]?>)</td>									
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= (count($stockTakeSchemeSelectionArr) == 0?'':$linterface->GET_ACTION_BTN($button_continue, "button","javascript:upload_function();")) ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="action" value="simple" />
</form>
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>