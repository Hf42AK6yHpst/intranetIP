<?php
/*
 * 	Log
 * 	
 * 	Date:	2017-03-30 [[Cameron]]
 * 		- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 	
 */
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


### cookies handling
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_periodical_page_field", "field");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_page_order", "order");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_page_number", "pageNo");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
$keyword = trim($keyword);
if (!get_magic_quotes_gpc()) {
	$keyword = stripslashes($keyword);
}


### toolbar
$htmlAry['toolbar'] = $linterface->GET_LNK_NEW("javascript: goEdit('');", $Lang['Btn']['New'], "", "", "", 0);


### search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);


### db table toolbar
$btnAry = array();
$btnAry[] = array('edit', 'javascript:checkEdit2(document.form1, \'PeriodicalBookIdAry[]\', \'goEdit();\');');
$btnAry[] = array('delete', 'javascript:checkRemove2(document.form1, \'PeriodicalBookIdAry[]\', \'goDelete();\');');
$htmlAry['dataTableToolbar'] = $linterface->Get_DBTable_Action_Button_IP25($btnAry);


### db table
$order = ($order == '') ? 1 : $order;
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("PeriodicalCode", "BookTitle", "ISSN", "OrderCount", "ItemCount");
$li->sql = $libms_periodical->GET_PERIODICAL_LIST_SQL($keyword);
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["PeriodicalCode"])."</td>\n";
$li->column_list .= "<th width='35%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["PeriodicalTitle"])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["ISSN"])."</td>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["NoOfOrder"])."</td>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["NoOfItem"])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("PeriodicalBookIdAry[]")."</td>\n";
$htmlAry['dataTable'] = $li->display();



$linterface->LAYOUT_START($indexAry['returnMsg']);
include_once('template/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>