<?php
$periodicalBookIdAry = $_POST['PeriodicalBookIdAry'];
$numOfBook = count($periodicalBookIdAry);

$successAry = array();
for ($i=0; $i<$numOfBook; $i++) {
	$_periodicalBookId = $periodicalBookIdAry[$i];
	
	$_bookObj = new liblms_periodical_book($_periodicalBookId);
	$_bookObj->setRecordStatus(0);
	$successAry[$_periodicalBookId] = $_bookObj->save();
}

if (in_array(false, (array)$successAry)) {
	$returnMsgKey = 'DeleteUnsuccess';
}
else {
	$returnMsgKey = 'DeleteSuccess';
}

header('Location: ?task=periodicalList&returnMsgKey='.$returnMsgKey);
?>