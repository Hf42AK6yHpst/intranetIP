<?php
$periodicalBookId = $_POST['PeriodicalBookID'];
$orderIdList = $_POST['OrderIDList'];
$orderIdAry = explode(',', $orderIdList);
$numOfOrder = count($orderIdAry);


$successAry = array();
$successAry['inactivateOrder'] = $libms_periodical->INACTIVATE_ORDER_ITEM($orderIdAry);


for ($i=0; $i<$numOfOrder; $i++) {
	$_orderId = $orderIdAry[$i]; 
	$_itemAry = $libms_periodical->GET_TEMP_PUBLISH_ITEM_INFO_BY_ORDERID($_orderId);
	
	$_numOfItem = count($_itemAry);
	for ($j=0; $j<$_numOfItem; $j++) {
		$__publishDate = $_itemAry[$j]['PublishDate'];
		$__issueNum = $_itemAry[$j]['IssueNum'];
		
		$__objItem = new liblms_periodical_item();
		$__objItem->setPeriodicalBookId($periodicalBookId);
		$__objItem->setOrderId($_orderId);
		$__objItem->setQuantity(0);
		$__objItem->setIssue($__issueNum);
		$__objItem->setPublishDate($__publishDate);
		$__objItem->setRecordStatus(1);
		$__itemId = $__objItem->save();
		
		$successAry[$_orderId]['createItem'][] = ($__itemId > 0)? true : false;
	}
	
	if (!in_array(false, $successAry[$_orderId])) {
		$_objOrder = new liblms_periodical_order($_orderId);
		$_objOrder->setIsGenerated(1);
		$_objOrder->setGenerateDate('now()');
		$_objOrder->setGeneratedBy($_SESSION['UserID']);
		$successAry[$_orderId]['saveOrderGenerate'] = $_objOrder->save();
	}
}


if (in_multi_array(false, ($successAry))) {
	$returnMsgKey = 'UpdateUnsuccess';
}
else {
	$returnMsgKey = 'UpdateSuccess';
}

header('Location: ?task=registrationList&PeriodicalBookID='.$periodicalBookId.'&returnMsgKey='.$returnMsgKey);
?>