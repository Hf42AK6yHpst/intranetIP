<?php
$periodicalBookId = ($_POST['PeriodicalBookID'])? $_POST['PeriodicalBookID'] : $_GET['PeriodicalBookID'];
$orderId = ($_POST['orderIdAry'])? $_POST['orderIdAry'][0] : $_GET['OrderID'];


### get periodical info
$periodicalInfoAry = $libms_periodical->GET_PERIODICAL_INFO($periodicalBookId);
$periodicalCode = $periodicalInfoAry[0]['PeriodicalCode'];
$bookTitle = $periodicalInfoAry[0]['BookTitle'];


### get order info
if ($orderId > 0) {
	//edit
	$orderInfoAry = $libms_periodical->GET_PERIODICAL_ORDER_INFO($orderId);
	$orderStartDate = $orderInfoAry[0]['OrderStartDate']; 
	$orderEndDate = $orderInfoAry[0]['OrderEndDate'];
	$price = $orderInfoAry[0]['Price'];
	$frequencyUnit = $orderInfoAry[0]['FrequencyUnit'];
	$frequencyNum = $orderInfoAry[0]['FrequencyNum'];
	$publishDateFirst = $orderInfoAry[0]['PublishDateFirst'];
	$publishDateLast = $orderInfoAry[0]['PublishDateLast'];
	$firstIssue = $orderInfoAry[0]['FirstIssue'];
	$lastIssue = $orderInfoAry[0]['LastIssue'];
	$distributor = $orderInfoAry[0]['Distributor'];
	$remarks = $orderInfoAry[0]['Remarks'];
	$alertDay = $orderInfoAry[0]['AlertDay'];
}
else {
	// new
	$frequencyUnit = 'M';
	$firstIssue = 1;
	$lastIssue = $Lang['General']['EmptySymbol'];
	$orderStartDate = date('Y-m-d');
	$orderEndDate = date('Y-m-d', strtotime("+1 month", strtotime($orderStartDate)));
	$publishDateFirst = date('Y-m-d');
	$publishDateLast = date('Y-m-d', strtotime("+1 month", strtotime($publishDateFirst)));
}


### navigation
$PAGE_NAVIGATION[] = array($Lang["libms"]["periodicalMgmt"]["Periodical"], "javascript: goCancel();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### sub-tag
$htmlAry['subTag'] = $indexAry['subTagHtml'];


### publish frequency display
$frequencyUnitSel = $libms_periodical->GET_PERIODICAL_PUBLISH_FREQUENCY_UNIT_SELECTION('FrequencyUnitSel', 'FrequencyUnit', $frequencyUnit, 'updateLastIssueDisplay();');
$frequencyNumSel = $linterface->Get_Number_Selection('FrequencyNum', $MinValue=1, $MaxValue=12, $frequencyNum, 'updateLastIssueDisplay();', $noFirst=1);
$orderFrequencyDisplay = $Lang["libms"]["periodicalMgmt"]["PublishFrequencyPerUnit"];
$orderFrequencyDisplay = str_replace('<!--frequencyUnit-->', $frequencyUnitSel, $orderFrequencyDisplay); 
$orderFrequencyDisplay = str_replace('<!--frequencyNum-->', $frequencyNumSel, $orderFrequencyDisplay);


### current periodical info display
$htmlAry['currentPeriodicalInfoTable'] = $libms_periodical->GET_PERIODICAL_BASIC_INFO_DISPLAY($periodicalBookId);


### edit table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// Order Period
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title" rowspan="2">'.$linterface->RequiredSymbol().$Lang["libms"]["periodicalMgmt"]["OrderPeriod"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30" style="width:100px;">('.$Lang["libms"]["periodicalMgmt"]["StartDate"].')</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $linterface->GET_DATE_PICKER('OrderStartDate', $orderStartDate);
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30" style="width:100px;">('.$Lang["libms"]["periodicalMgmt"]["EndDate"].')</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $linterface->GET_DATE_PICKER('OrderEndDate', $orderEndDate);
				$x .= $linterface->Get_Form_Warning_Msg('OrderDateInvalidWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["StartDateMustBeEarlierThanEndDate"], $Class='warnMsgDiv')."\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Order Price
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang["libms"]["periodicalMgmt"]["OrderPrice"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->GET_TEXTBOX_NUMBER('PriceTb', 'Price', $price);
			$x .= $linterface->Get_Form_Warning_Msg('PricePositiveWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputPositiveNum"], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Publish Frequency
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang["libms"]["periodicalMgmt"]["PublishFrequency"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $orderFrequencyDisplay;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Publish Date
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title" rowspan="2">'.$linterface->RequiredSymbol().$Lang["libms"]["periodicalMgmt"]["PublishDate"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30" style="width:100px;">('.$Lang["libms"]["periodicalMgmt"]["FirstIssue"].')</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $linterface->GET_DATE_PICKER('PublishDateFirst', $publishDateFirst,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="", 'updateLastIssueDisplay();');
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30" style="width:100px;">('.$Lang["libms"]["periodicalMgmt"]["LastIssue"].')</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $linterface->GET_DATE_PICKER('PublishDateLast', $publishDateLast,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="", 'updateLastIssueDisplay();');
				$x .= $linterface->Get_Form_Warning_Msg('PublishDateInvalidWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["StartDateMustBeEarlierThanEndDate"], $Class='warnMsgDiv')."\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Issue Number
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title" rowspan="2">'.$Lang["libms"]["periodicalMgmt"]["IssueOrCode"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30" style="width:100px;">('.$Lang["libms"]["periodicalMgmt"]["FirstIssue"].')</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $linterface->GET_TEXTBOX_NUMBER('FirstIssueTb', 'FirstIssue', $firstIssue, $OtherClass='', array('onkeyup'=>'updateLastIssueDisplay();'));
				$x .= $linterface->Get_Form_Warning_Msg('FirstIssuePositiveWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputPositiveNum"], $Class='warnMsgDiv')."\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30" style="width:100px;">('.$Lang["libms"]["periodicalMgmt"]["LastIssue"].')</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= '<span id="lastIssueSpan">'.$lastIssue.'</span>'."\r\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Distributor
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang["libms"]["book"]["distributor"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->GET_TEXTBOX('DistributorTb', 'Distributor', $distributor, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['Distributor']));
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Remarks
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang["libms"]["periodicalMgmt"]["Remarks"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->GET_TEXTBOX('RemarksTb', 'Remarks', $remarks, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['PeriodicalRemarks']));
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Renew Alert Days
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang["libms"]["periodicalMgmt"]["RenewAlertDay"].' <span class="tabletextremark">('.$Lang["libms"]["periodicalMgmt"]["RenewAlertDayRemarks"].')</span></td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->Get_Number_Selection('AlertDay', $MinValue=0, $MaxValue=100, $alertDay, $Onchange='', $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
$x .= '</table>'."\r\n";
$htmlAry['orderEditTable'] = $x;


### action buttons
$htmlAry['saveBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Save'], "button", "goSave()", 'saveBtn');
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn');


$linterface->LAYOUT_START($indexAry['returnMsg']);
include_once('template/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>