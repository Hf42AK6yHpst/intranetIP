<?php
/*
 * 	2017-02-09 [Cameron]
 * 		- strip slashes after call lib.php for php5.4+
 */

//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
			$value = stripslashes($value);
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
}
//****** end special handle for php5.4+

$periodicalBookId = $_POST['PeriodicalBookID'];
$itemId = $_POST['ItemID'];
$publishDate = trim($_POST['PublishDate']);
$issue = trim($_POST['Issue']);
$details = trim($_POST['Details']);

$objItem = new liblms_periodical_item($itemId);
$objItem->setPeriodicalBookId($periodicalBookId);
$objItem->setIssue($issue);
$objItem->setPublishDate($publishDate);
$objItem->setDetails($details);
$objItem->setRecordStatus(1);
$itemId = $objItem->save();

if ($itemId > 0) {
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$returnMsgKey = 'UpdateUnsuccess';
}

header('Location: ?task=registrationList&PeriodicalBookID='.$periodicalBookId.'&returnMsgKey='.$returnMsgKey);
?>