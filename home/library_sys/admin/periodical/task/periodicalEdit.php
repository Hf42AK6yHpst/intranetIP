<?php
$periodicalBookId = ($_POST['PeriodicalBookID'])? $_POST['PeriodicalBookID'] : $_GET['PeriodicalBookID'];
$isNew = ($periodicalBookId > 0)? false : true;


### navigation
$PAGE_NAVIGATION[] = array($Lang["libms"]["periodicalMgmt"]["Periodical"], "javascript: goCancel();");
if ($isNew) {
	$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
}
else {
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
}
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### sub-tag
$htmlAry['subTag'] = $indexAry['subTagHtml'];



### book info
$bookObj = new liblms_periodical_book($periodicalBookId);
$periodicalCode = $bookObj->getPeriodicalCode();
$resourcesTypeCode = $bookObj->getResourcesTypeCode();
$circulationTypeCode = $bookObj->getCirculationTypeCode();
$bookTitle = $bookObj->getBookTitle();
$publisher = $bookObj->getPublisher();
$publishPlace = $bookObj->getPublishPlace();
$issn = $bookObj->getIssn();
//$alert = $bookObj->getAlert();
$bookCategoryCode = $bookObj->getBookCategoryCode();
$purchaseByDepartment = $bookObj->getPurchaseByDepartment();
$remarks = $bookObj->getRemarks();
$openBorrow = $bookObj->getOpenBorrow();
$openReservation = $bookObj->getOpenReservation();




if ($isNew) {
	// for new periodical => can be borrow and reserve be default
	$openBorrow = 1;
	$openReservation = 1;
}


### resources type selection 
$BookResourcesArray = $libms->BOOK_GETOPTION('LIBMS_RESOURCES_TYPE', 'ResourcesTypeCode', $Lang['libms']['sql_field']['Description'], 'ResourcesTypeCode', '');
$resourcesTypeSel = $linterface->GET_SELECTION_BOX($BookResourcesArray, " name='ResourcesTypeCode' id='ResourcesTypeCode' ", $Lang['libms']['status']['na'], $resourcesTypeCode);


### circulation type selection
$BookCirculationArray = $libms->BOOK_GETOPTION('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', $Lang['libms']['sql_field']['Description'], 'CirculationTypeCode', '');
$circulationTypeSel = $linterface->GET_SELECTION_BOX($BookCirculationArray, " name='CirculationTypeCode' id='CirculationTypeCode' ", $Lang['libms']['status']['na'], $circulationTypeCode);


### book category selection
$BookCategoryArray = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', '');
$bookCategorySel = $linterface->GET_SELECTION_BOX($BookCategoryArray, " name='BookCategoryCode' id='BookCategoryCode' ", $Lang['libms']['status']['na'], $bookCategoryCode);


### periodic info table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			// Periodical Code
			$x .= '<td class="field_title_short" style="width:15%;">'."\r\n";
				$x .= $linterface->RequiredSymbol().' '.$Lang["libms"]["periodicalMgmt"]["PeriodicalCode"]."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td style="width:33%;">'."\r\n";
				$x .= $linterface->GET_TEXTBOX('PeriodicalCodeTb', 'PeriodicalCode', $periodicalCode, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['PeriodicalCode']))."\r\n";
				$x .= $linterface->Get_Form_Warning_Msg('PeriodicalCodeEmptyWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputValue"], $Class='warnMsgDiv')."\n";
				$x .= $linterface->Get_Form_Warning_Msg('PeriodicalCodeInUserWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["CodeInUse"], $Class='warnMsgDiv')."\n";
			$x .= '</td>'."\r\n";
			
			$x .= '<td style="width:4%;">&nbsp;</td>'."\r\n";
			
			$x .= '<td style="width:15%;">&nbsp;</td>'."\r\n";
			$x .= '<td style="width:33%;">&nbsp;</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$x .= '<tr>'."\r\n";
			// Resources Type
			$x .= '<td class="field_title_short">'."\r\n";
				$x .= $Lang['libms']['settings']['resources_type']."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $resourcesTypeSel."\r\n";
			$x .= '</td>'."\r\n";
			
			$x .= '<td style="width:4%;">&nbsp;</td>'."\r\n";
			
			// Circulation Type 
			$x .= '<td class="field_title_short">'.$Lang['libms']['settings']['circulation_type'].'</td>'."\r\n";
			$x .= '<td>'.$circulationTypeSel.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$x .= '<tr>'."\r\n";
			// Periodical Title
			$x .= '<td class="field_title_short">'."\r\n";
				$x .= $linterface->RequiredSymbol().' '.$Lang["libms"]["periodicalMgmt"]["PeriodicalTitle"]."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td colspan="4">'."\r\n";
				$x .= $linterface->GET_TEXTBOX('BookTitleTb', 'BookTitle', $bookTitle, $OtherClass='', $OtherPar=array('maxlength'=>min($lmsConfigAry['maxLengthAry']['BookTitle'], $lmsConfigAry['maxLengthAry']['Series'])))."\r\n";
				$x .= $linterface->Get_Form_Warning_Msg('BookTitleEmptyWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputValue"], $Class='warnMsgDiv')."\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$x .= '<tr>'."\r\n";
			// Publisher
			$x .= '<td class="field_title_short">'."\r\n";
				$x .= $Lang['libms']['book']['publisher']."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td colspan="4">'."\r\n";
				$x .= $linterface->GET_TEXTBOX('PublisherTb', 'Publisher', $publisher, $OtherClass='inputselect', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['Publisher']))."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$x .= '<tr>'."\r\n";
			// Publish Place
			$x .= '<td class="field_title_short">'."\r\n";
				$x .= $Lang['libms']['book']['publish_place']."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td colspan="4">'."\r\n";
				$x .= $linterface->GET_TEXTBOX('PublishPlaceTb', 'PublishPlace', $publishPlace, $OtherClass='inputselect', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['PublishPlace']))."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$x .= '<tr>'."\r\n";
			// ISSN
			$x .= '<td class="field_title_short">'."\r\n";
				$x .= $Lang["libms"]["periodicalMgmt"]["ISSN"]."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $linterface->GET_TEXTBOX('ISSNTb', 'ISSN', $issn, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['ISSN']))."\r\n";
			$x .= '</td>'."\r\n";
			
			$x .= '<td>&nbsp;</td>'."\r\n";
			
			$x .= '<td>&nbsp;</td>'."\r\n";
			$x .= '<td>&nbsp;</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
//		$x .= '<tr>'."\r\n";
//			// Alert Message
//			$x .= '<td class="field_title_short">'."\r\n";
//				$x .= $Lang["libms"]["periodicalMgmt"]["Alert"]."\r\n";
//			$x .= '</td>'."\r\n";
//			$x .= '<td colspan="4">'."\r\n";
//				$x .= $linterface->GET_TEXTBOX('alertTb', 'alert', $alert, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['RemarkToUser']))."\r\n";
//			$x .= '</td>'."\r\n";
//		$x .= '</tr>'."\r\n";
		
		$x .= '<tr>'."\r\n";
			// Book Category Code
			$x .= '<td class="field_title_short">'."\r\n";
				$x .= $Lang['libms']['settings']['book_category']."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $bookCategorySel."\r\n";
			$x .= '</td>'."\r\n";
			
			$x .= '<td style="width:4%;">&nbsp;</td>'."\r\n";
			
			// Purchase By Department
			$x .= '<td class="field_title_short">'.$Lang['libms']['book']['purchase_by_department'].'</td>'."\r\n";
			$x .= '<td>'.$linterface->GET_TEXTBOX('PurchaseByDepartmentTb', 'PurchaseByDepartment', $purchaseByDepartment, $OtherClass='inputselect', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['PurchaseByDepartment'])).'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$x .= '<tr>'."\r\n";
			// Remarks
			$x .= '<td class="field_title_short">'."\r\n";
				$x .= $Lang['libms']['book']['remark_internal']."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td colspan="4">'."\r\n";
				$x .= $linterface->GET_TEXTBOX('RemarksTb', 'Remarks', $remarks, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['RemarkInternal']))."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$x .= '<tr>'."\r\n";
			// Status
			$x .= '<td class="field_title_short">'."\r\n";
				$x .= $Lang["libms"]["periodicalMgmt"]["DefaultStatus"]."\r\n";
			$x .= '</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $linterface->Get_Checkbox('OpenBorrowChk', 'OpenBorrow', $Value=1, $openBorrow, $Class='', $Lang['libms']['book']['open_borrow'])."\r\n";
				$x .= '&nbsp;'."\r\n";
				$x .= $linterface->Get_Checkbox('OpenReservationChk', 'OpenReservation', $Value=1, $openReservation, $Class='', $Lang['libms']['book']['open_reserve'])."\r\n";
			$x .= '</td>'."\r\n";
			
			$x .= '<td>&nbsp;</td>'."\r\n";
			
			$x .= '<td>&nbsp;</td>'."\r\n";
			$x .= '<td>&nbsp;</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['periodicalEditTable'] = $x;


### action buttons
$htmlAry['saveBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Save'], "button", "goSave()", 'saveBtn');
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn');

$linterface->LAYOUT_START($indexAry['returnMsg']);
include_once('template/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>