<?php
$periodicalBookId = ($_POST['PeriodicalBookID'])? $_POST['PeriodicalBookID'] : $_GET['PeriodicalBookID'];
$itemId = ($_POST['itemIdAry'])? $_POST['itemIdAry'][0] : $_GET['ItemID'];


### get item info
$itemObj = new liblms_periodical_item($itemId);
$publishDate = $itemObj->getPublishDate();
$issue = $itemObj->getIssue();
$details = $itemObj->getDetails();


### navigation
$PAGE_NAVIGATION[] = array($Lang["libms"]["periodicalMgmt"]["Periodical"], "javascript: goCancel();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### sub-tag
$htmlAry['subTag'] = $indexAry['subTagHtml'];


### current periodical info display
$htmlAry['currentPeriodicalInfoTable'] = $libms_periodical->GET_PERIODICAL_BASIC_INFO_DISPLAY($periodicalBookId);


### edit table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// Publish Date
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang["libms"]["periodicalMgmt"]["PublishDate"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->GET_DATE_PICKER('PublishDate', $publishDate)."\r\n";
			$x .= $linterface->Get_Form_Warning_Msg('PublishDateEmptyWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputValue"], $Class='warnMsgDiv')."\n";
			$x .= $linterface->Get_Form_Warning_Msg('PublishDateInUserWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["PublishDateInUse"], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Issue Number
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang["libms"]["periodicalMgmt"]["IssueOrCode"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->GET_TEXTBOX_NUMBER('IssueTb', 'Issue', $issue, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['SeriesNum']));
			$x .= $linterface->Get_Form_Warning_Msg('IssueWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputValue"], $Class='warnMsgDiv')."\n";
			$x .= $linterface->Get_Form_Warning_Msg('IssuePositiveWarnDiv', $Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputPositiveNum"], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Details
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang["libms"]["periodicalMgmt"]["Details"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->GET_TEXTBOX('DetailsTb', 'Details', $details, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['PurchaseNote']));
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['orderEditTable'] = $x;


### action buttons
$htmlAry['saveBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Save'], "button", "goSave()", 'saveBtn');
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn');


$linterface->LAYOUT_START($indexAry['returnMsg']);
include_once('template/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>