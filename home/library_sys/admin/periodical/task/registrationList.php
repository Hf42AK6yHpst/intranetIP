<?php
/*
 * 	Log
 * 	
 * 	Date:	2017-03-30 [[Cameron]]
 * 		- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 	
 */

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


### cookies handling
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_periodical_registrationList_page_field", "field");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_registrationList_page_order", "order");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_registrationList_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_registrationList_page_number", "pageNo");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_registrationList_registerStatus", "registerStatus");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_registrationList_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}


### form post / get value
$periodicalBookId = ($_POST['PeriodicalBookID'])? $_POST['PeriodicalBookID'] : $_GET['PeriodicalBookID'];
$keyword = trim($keyword);
if (!get_magic_quotes_gpc()) {
	$keyword = stripslashes($keyword);
}


### navigation
$PAGE_NAVIGATION[] = array($Lang["libms"]["periodicalMgmt"]["Periodical"], "javascript: goCancel();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### sub-tag
$htmlAry['subTag'] = $indexAry['subTagHtml'];


### toolbar
$htmlAry['toolbar'] = $linterface->GET_LNK_NEW("javascript: goEdit();", $Lang['Btn']['New'], "", "", "", 0);


### filter
if ($registerStatus === null) {
	// default shows not registered items
	$registerStatus = 'NR';
}
$htmlAry['registerStatusSel'] = $libms_periodical->GET_PERIODICAL_ITEM_REGISTERED_SELECTION('registerStatusSel', 'registerStatus', $registerStatus, 'reloadPage();');


### search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);


### current periodical info display
$htmlAry['currentPeriodicalInfoTable'] = $libms_periodical->GET_PERIODICAL_BASIC_INFO_DISPLAY($periodicalBookId);


### db table toolbar
$btnAry = array();
$btnAry[] = array('generate', 'javascript: checkEdit2(document.form1, \'itemIdAry[]\', \'goRegister();\');', $Lang["libms"]["periodicalMgmt"]["Register"]);
$btnAry[] = array('edit', 'javascript:checkEdit2(document.form1, \'itemIdAry[]\', \'goEdit();\');');
$btnAry[] = array('delete', 'javascript:checkRemove2(document.form1, \'itemIdAry[]\', \'goDelete();\');');
$htmlAry['dataTableToolbar'] = $linterface->Get_DBTable_Action_Button_IP25($btnAry);


### db table
$order = ($order == '') ? 1 : $order;
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("lpi.PublishDate", "lpi.Issue", "Details", "IsRegistered", "RegistrationDate", "RegisteredItemNum");
$li->sql = $libms_periodical->GET_PERIODICAL_ORDER_ITEM_LIST_SQL($periodicalBookId, $keyword, $registerStatus);
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["PublishDate"])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["IssueOrCode"])."</td>\n";
$li->column_list .= "<th width='30%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["Details"])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["Registered"])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["LastRegistrationDate"])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["RegisteredItemNum"])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("itemIdAry[]")."</td>\n";
$htmlAry['dataTable'] = $li->display();


### action buttons
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn');


$linterface->LAYOUT_START($indexAry['returnMsg']);
include_once('template/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>