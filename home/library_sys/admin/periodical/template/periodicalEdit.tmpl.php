<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
$(document).ready( function() {
	$("input.inputselect").inputselect({
		image_path: '/images/2009a/', 
		ajax_script: '../book/ajax_get_selection_list.php', 
		js_lang_alert: {'no_records' : '<?=$Lang['libms']['NoRecordAtThisMoment']?>' }
	});
	
	saveFormValues();	// for edited form checking
});

function clickedSubTag(targetTask) {
	var periodicalBookId = $('input#PeriodicalBookID').val();
	var canSubmit = true;
	
	if (periodicalBookId == '') {
		alert('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["SaveBasicInfoFirst"]?>');
		canSubmit = false;
	}
	
	if (canSubmit && isFormChanged()) {
		if (confirm('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"]?>')) {
			// do nth
		}
		else {
			canSubmit = false;
		}
	}
	
	if (canSubmit) {
		window.location = '?clearCoo=1&task=' + targetTask + '&PeriodicalBookID=' + periodicalBookId;
	}
}

function goCancel() {
	var canSubmit = true;
	if (isFormChanged()) {
		if (confirm('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"]?>')) {
			// do nth
		}
		else {
			canSubmit = false;
		}
	}
	
	if (canSubmit) {
		window.location = 'index.php';
	}
}

function goSave() {
	var canSubmit = true;
	
	$('input#saveBtn').attr('disabled', 'disabled');
	$('input#cancelBtn').attr('disabled', 'disabled');
	$('div.warnMsgDiv').hide();
	
	if ($.trim($('input#PeriodicalCodeTb').val()) == '') {
		$('input#PeriodicalCodeTb').focus();
		$('div#PeriodicalCodeEmptyWarnDiv').show();
		canSubmit = false;
	}
	
	if ($.trim($('input#BookTitleTb').val()) == '') {
		$('input#BookTitleTb').focus();
		$('div#BookTitleEmptyWarnDiv').show();
		canSubmit = false;
	}
	
	$.post(
		"index.php", 
		{ 
			task_ajax: 'ajax_task',
			ActionType: "validatePeriodicalCode",
			PeriodicalCode: $('input#PeriodicalCodeTb').val(),
			PeriodicalBookID: $('input#PeriodicalBookID').val()
		},
		function(ReturnData)
		{
			if (ReturnData == '1') {
				if (canSubmit) {
					$('input#task').val('');
					$('input#task_update').val('periodicalEditUpdate');
					$('form#form1').submit();
				}
			}
			else {
				$('input#PeriodicalCodeTb').focus();
				$('div#PeriodicalCodeInUserWarnDiv').show();
				canSubmit = false;
			}
			
			if (!canSubmit) {
				$('input#saveBtn').attr('disabled', '');
				$('input#cancelBtn').attr('disabled', '');
			}
		}
	);
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
<div class="navigation"><?=$htmlAry['navigation']?></div>
<br style="clear:both;" />

<?=$htmlAry['subTag']?>
<p class="spacer"></p>

<div class="table_board">
	<div>
		<?=$htmlAry['periodicalEditTable']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<?=$htmlAry['saveBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
</div>

<input type="hidden" id="task" name="task" value="" />
<input type="hidden" id="task_update" name="task_update" value="" />
<!--input type="hidden" id="Code" name="Code" value="<?=$bookId?>" /-->	<!-- for save book info use -->
<input type="hidden" id="PeriodicalBookID" name="PeriodicalBookID" value="<?=$periodicalBookId?>" />
<!--input type="hidden" id="IsPeriodical" name="IsPeriodical" value="1" /-->
<!--input type="hidden" id="FromPeriodical" name="FromPeriodical" value="1" /-->
<input type="hidden" id="clearCoo" name="clearCoo" value="" />
</form>