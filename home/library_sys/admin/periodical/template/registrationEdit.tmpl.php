<script type="text/javascript">
$(document).ready( function() {
	saveFormValues();	// for edited form checking
});

function clickedSubTag(targetTask) {
	var periodicalBookId = $('input#PeriodicalBookID').val();
	var canSubmit = true;
	
	if (isFormChanged()) {
		if (confirm('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"]?>')) {
			// do nth
		}
		else {
			canSubmit = false;
		}
	}
	
	if (canSubmit) {
		window.location = '?clearCoo=1&task=' + targetTask + '&PeriodicalBookID=' + periodicalBookId;
	}
}

function goCancel() {
	var canSubmit = true;
	if (isFormChanged()) {
		if (confirm('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"]?>')) {
			// do nth
		}
		else {
			canSubmit = false;
		}
	}
	
	if (canSubmit) {
		$('input#task').val('registrationList');
		$('input#task_update').val('');
		$('form#form1').submit();
	}
}

function goSave() {
	var canSubmit = true;
	
	$('input#saveBtn').attr('disabled', 'disabled');
	$('input#cancelBtn').attr('disabled', 'disabled');
	$('div.warnMsgDiv').hide();
	
	if ($.trim($('input#PublishDate').val()) == '') {
		$('input#PublishDate').focus();
		$('div#PublishDateEmptyWarnDiv').show();
		canSubmit = false;
	}
	
	$.post(
		"index.php", 
		{ 
			task_ajax: 'ajax_task',
			ActionType: "validatePublishDate",
			PublishDate: $('input#PublishDate').val(),
			PeriodicalBookID: $('input#PeriodicalBookID').val(),
			ItemID: $('input#ItemID').val()
		},
		function(ReturnData)
		{
			if (ReturnData == '1') {
				// do nth
			}
			else {
				$('input#PublishDate').focus();
				$('div#PublishDateInUserWarnDiv').show();
				canSubmit = false;
			}
			
			var issueVal = $.trim($('input#IssueTb').val());
			if (issueVal == '') {
				$('input#IssueTb').focus();
				$('div#IssueEmptyWarnDiv').show();
				canSubmit = false;
			}
			
			if (!isNaN(issueVal) && parseInt(issueVal) < 0) {
				$('input#IssueTb').focus();
				$('div#IssuePositiveWarnDiv').show();
				canSubmit = false;
			}
			
			if (!canSubmit) {
				$('input#saveBtn').attr('disabled', '');
				$('input#cancelBtn').attr('disabled', '');
			}
			else {
				$('input#task').val('');
				$('input#task_update').val('registrationEditUpdate');
				$('form#form1').submit();
			}
		}
	);
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<div class="navigation"><?=$htmlAry['navigation']?></div>
	<br style="clear:both;" />
	
	<?=$htmlAry['subTag']?>
	<p class="spacer"></p>
	<br style="clear:both" />
	
	<div class="table_board">
		<div>
			<?=$htmlAry['currentPeriodicalInfoTable']?>
		</div>
	
		<div>
			<?=$htmlAry['orderEditTable']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<?=$htmlAry['saveBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
	</div>
	<br />
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="task_update" name="task_update" value="" />
	<input type="hidden" id="PeriodicalBookID" name="PeriodicalBookID" value="<?=$periodicalBookId?>" />
	<input type="hidden" id="ItemID" name="ItemID" value="<?=$itemId?>" />
	<input type="hidden" id="clearCoo" name="clearCoo" value="" />
</form>