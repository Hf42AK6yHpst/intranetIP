<script type="text/javascript">
$(document).ready( function() {

});

function clickedSubTag(targetTask) {
	$('input#task').val(targetTask);
	$('input#task_update').val('');
	$('form#form1').submit();
}

function goCancel() {
	$('input#task').val('orderList');
	$('input#task_update').val('');
	$('form#form1').submit();
}

function goGenerate() {
	$('input#generateBtn').attr('disabled', 'disabled');
	
	$('input#task').val('');
	$('input#task_update').val('orderGenerateUpdate');
	$('form#form1').submit();
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<div class="navigation"><?=$htmlAry['navigation']?></div>
	<br style="clear:both;" />
	
	<?=$htmlAry['subTag']?>
	<p class="spacer"></p>
	<br style="clear:both" />
	
	<div class="table_board">
		<div>
			<?=$htmlAry['currentPeriodicalInfoTable']?>
		</div>
	
		<div>
			<?=$htmlAry['remarksTable']?>
			<br style="clear:both" />
			<?=$htmlAry['orderItemInfoTable']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<?=$htmlAry['generateBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
	</div>
	<br />
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="task_update" name="task_update" value="" />
	<input type="hidden" id="PeriodicalBookID" name="PeriodicalBookID" value="<?=$periodicalBookId?>" />
	<input type="hidden" id="OrderIDList" name="OrderIDList" value="<?=implode(',',(array)$orderIdAry)?>" />
</form>