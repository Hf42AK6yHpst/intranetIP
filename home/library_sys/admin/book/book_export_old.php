<?php
// using: 

/************************************
 * 
 * Date:	2013-08-21 (Henry)
 * Details: add Publisher filter
 * 
 * Date:	2013-08-13 (Jason)
 * Details: review the whole page, follow IP25 standard, fix the cookies problem
 * 			add thickbox link for no. of copy column
 * 
 * Date:	2013-05-15 (Rita)
 * Details:	add Location filter
 ************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

### set cookies
//if ($page_size_change == 1)
//{
//    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
//    $ck_page_size = $numPerPage;
//}
//
//# preserve table view
//if ($ck_right_page_number!=$pageNo && $pageNo!="")
//{
//	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
//	$ck_right_page_number = $pageNo;
//} else if (!isset($pageNo) && $ck_right_page_number!="")
//{
//	$pageNo = $ck_right_page_number;
//}
//
//if ($ck_right_page_order!=$order && $order!="")
//{
//	setcookie("ck_right_page_order", $order, 0, "", "", 0);
//	$ck_right_page_order = $order;
//} else if (!isset($order) && $ck_right_page_order!="")
//{
//	//$order = $ck_right_page_order;
//	$order = 1;		# default in ascending order
//}
//
//if ($ck_right_page_field!=$field && $field!="")
//{
//	setcookie("ck_right_page_field", $field, 0, "", "", 0);
//	$ck_right_page_field = $field;
//} else if (!isset($field) && $ck_right_page_field!="")
//{
//	$field = $ck_right_page_field;
//}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");


//global $intranet_db;

intranet_auth();
intranet_opendb();





# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$sql = "SELECT lb.BookTitle, lb.BookSubtitle, lb.BookCode, lbu.BarCode, lb.CallNum, lb.CallNum2, lb.ISBN, ISBN2, lb.Introduction, lb.Language, lb.Country, lb.Edition, lb.PublishPlace, lb.Publisher," .
		"lb.PublishYear, lb.NoOfPage, lb.RemarkInternal, lb.RemarkToUser, lb.PurchaseDate, lb.Distributor, lb.PurchaseNote, lb.PurchaseByDepartment," .
		"lb.ListPrice, lb.Discount, lb.PurchasePrice, lb.AccountDate, lb.BookCategoryCode, lb.LocationCode, lb.Subject, lb.Series, lb.SeriesNum, lb.URL, lb.ResponsibilityBy1, lb.ResponsibilityBy2, lbu.RecordStatus  FROM LIBMS_BOOK AS lb LEFT JOIN LIBMS_BOOK_UNIQUE AS lbu ON lbu.BookID=lb.BookID WHERE lb.RecordStatus<>'DELETE' AND lbu.RecordStatus<>'DELETE' ORDER BY lb.BookCode limit 300";
$rows = $libms->returnArray($sql);


$lexport = new libexporttext();
//debug_r($rows);
$columnStr = "lb.BookTitle, lb.BookSubtitle, lb.BookCode, lbu.BarCode, lb.CallNum, lb.CallNum2, lb.ISBN, ISBN2, lb.Introduction, lb.Language, lb.Country, lb.Edition, lb.PublishPlace, lb.Publisher," .
		"lb.PublishYear, lb.NoOfPage, lb.RemarkInternal, lb.RemarkToUser, lb.PurchaseDate, lb.Distributor, lb.PurchaseNote, lb.PurchaseByDepartment," .
		"lb.ListPrice, lb.Discount, lb.PurchasePrice, lb.AccountDate, lb.BookCategoryCode, lb.LocationCode, lb.Subject, lb.Series, lb.SeriesNum, lb.URL, lb.ResponsibilityBy1, lb.ResponsibilityBy2, lbu.RecordStatus";
$tmpArr = explode(",", $columnStr);
for ($i=0; $i<sizeof($tmpArr); $i++)
{
	$exportColumn[0][] = str_replace("lbu.", "", str_replace("lb.", "", $tmpArr[$i]));
}

$filename = "book_list.csv";
$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn, "\t", "\r\n", "\t", 0);
intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>