<?php
// using:
/*
 *  2018-10-10 Cameron
 *      - fix: should update LIBMS_BOOK_UNIQUE.RecordStatus only when LIBMS_BORROW_LOG.RecordStatus='BORROWED' and
 *       LIBMS_BOOK_UNIQUE.RecordStatus ='BORROWED', also handle LOST case [case #Z150077]
 *      - apply UPDATE_BOOK_COPY_INFO() after update 
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");


//global $intranet_db;

intranet_auth();
intranet_opendb();
################################################################################################################
if(!isset($BookID) || empty($BookID)){
	header("Location: index.php");
}

################################################################################################################


$FromPage = (isset($FromPage) && $FromPage != '') ? $FromPage : 'book';

## Use Library
# in all boook management PHP pages
$libms = new liblms();

# Access Checking
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# remove borrow log and 

# if the payment is settled (money is received), the record cannot be removed!!!
$sql = "SELECT * FROM LIBMS_OVERDUE_LOG WHERE BorrowLogID='{$BorrowLogID}' AND PaymentReceived>0  ";
$rows = $libms->returnVector($sql);

if ($rows[0]==0)
{
	
	$sql = "SELECT BookID, UniqueID, RecordStatus FROM LIBMS_BORROW_LOG WHERE BorrowLogID='{$BorrowLogID}'";
	$SrcRows = $libms->returnResultSet($sql);
	if (count($SrcRows)) {
	    $SrcRows = current($SrcRows);
	    $bookID = $SrcRows['BookID'];
	    $BookUniqueID = $SrcRows['UniqueID'];
	    $borrowLogStatus = $SrcRows['RecordStatus'];
	}
	else {
	    $bookID = 0;
	    $BookUniqueID = 0;
	    $borrowLogStatus = '';
	}
	
	if ($BookUniqueID>0)
	{
		
		$sql = "DELETE FROM LIBMS_OVERDUE_LOG WHERE BorrowLogID='{$BorrowLogID}'";
		$libms->db_db_query($sql);
		
		$sql = "DELETE FROM LIBMS_BORROW_LOG WHERE BorrowLogID='{$BorrowLogID}'";
		$libms->db_db_query($sql);
		
		$sql = "SELECT RecordStatus FROM LIBMS_BOOK_UNIQUE WHERE UniqueID='{$BookUniqueID}'";
		$bookUniqueAry = $libms->returnResultSet($sql);
		if (count($bookUniqueAry)) {
		    $bookUniqueStatus = $bookUniqueAry[0]['RecordStatus'];
		}
		else {
		    $bookUniqueStatus = '';
		}
		
		# change the book status to normal
		if ($borrowLogStatus == 'BORROWED') {
		    if ($bookUniqueStatus == 'BORROWED') {
        		$sql = "UPDATE LIBMS_BOOK_UNIQUE SET RecordStatus='NORMAL' WHERE UniqueID='{$BookUniqueID}' ";
        		$libms->db_db_query($sql);
		    }
    		
    		// also update previous book status in write-off log
    		$sql = "UPDATE LIBMS_WRITEOFF_LOG SET PreviousBookStatus='NORMAL' WHERE BookUniqueID='{$BookUniqueID}' AND PreviousBookStatus='BORROWED'";
    		$libms->db_db_query($sql);
    		
		}
		else if ($borrowLogStatus == 'LOST') {        // borrow first, then write-off, report loss later, now cancel the circulation record
		    if ($bookUniqueStatus == 'LOST') {
		        $sql = "SELECT WriteOffID FROM LIBMS_WRITEOFF_LOG WHERE BookUniqueID='".$BookUniqueID."' ORDER BY WriteOffID DESC LIMIT 1";
		        $writeOffAry = $libms->returnResultSet($sql);
		        if (count($writeOffAry)) {
		            $writeOffID = $writeOffAry[0]['WriteOffID'];
		            $sql = "UPDATE LIBMS_BOOK_UNIQUE SET RecordStatus='WRITEOFF' WHERE UniqueID='{$BookUniqueID}' ";
		            $libms->db_db_query($sql);
		        }
		    }
		    // also update previous book status in write-off log
		    $sql = "UPDATE LIBMS_WRITEOFF_LOG SET PreviousBookStatus='NORMAL' WHERE BookUniqueID='{$BookUniqueID}' AND PreviousBookStatus='BORROWED'";
		    $libms->db_db_query($sql);
		}
		
		$libms->UPDATE_BOOK_COPY_INFO($bookID);
		
		$xmsg = "DeleteSuccess";
	} else
	{
		$xmsg = "DeleteUnsuccess";
	}
} else
{
	$xmsg = "FailedToDelLoanAsPaymentSettled";
}


################################################################################################################
intranet_closedb();

header("Location: book_item_borrow_record.php?BookID={$BookID}&FromPage={$FromPage}&UniqueID={$UniqueID}&xmsg=".$xmsg);
?>