<?php
/*
 *	Log
 *
 *	2020-10-14 [Cameron]
 *	    - create this page [case #Y190573]
 *
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();

### Set Cookies
$arrCookies = array();
$arrCookies[] = array("eBook_eBook_show_hide_field", "field");							// navigation var start
$arrCookies[] = array("eBook_eBook_show_hide_order", "order");
$arrCookies[] = array("eBook_eBook_show_hide_number", "pageNo");						// navigation var end
$arrCookies[] = array("eBook_eBook_show_hide_keyword", "keyword");
if ($page_size_change == 1)
{
    $arrCookies[] = array("eBook_eBook_show_hide_numPerPage", "numPerPage");
}

if(isset($clearCoo) && $clearCoo == 1)
{
    clearCookies($arrCookies);
}
else
{
    updateGetCookies($arrCookies);
}

## Get Data



# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$QuotationID = IntegerSafe($_POST['QuotationID'] ? $_POST['QuotationID'] : $_GET['QuotationID']);
$keyword = trim($_POST['keyword']);
$keyword = (isset($keyword) && $keyword != '') ? $keyword : '';
$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;       // default sort by ascending order
$field = ($field == "") ? 0 : $field;       // default sort by Book ID
if (isset($eBook_eBook_show_hide_numPerPage) && $eBook_eBook_show_hide_numPerPage != "") $page_size = $eBook_eBook_show_hide_numPerPage;
$pageSizeChangeEnabled = true;

$linterface = new interface_html('libms.html');
$libelibinstall = new elibrary_install();

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageEBookLicense";

$libms->MODULE_AUTHENTICATION($CurrentPage);	// always return true
$TAGS_OBJ = $libms->get_ebook_tabs("LICENSE");
# Start layout
if (!$plugin['eLib_Lite']){
    $MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
}


$quo_info = $libelibinstall->get_quotation_info($QuotationID);

if ($quo_info != false) {
    $QuotationNumber = $quo_info["QuotationNumber"];
    $QuotationBookCount = $quo_info["QuotationBookCount"];
    if($quo_info["PeriodFrom"] == "0000-00-00 00:00:00" && $quo_info["PeriodTo"] == "0000-00-00 00:00:00"){
        $disp_quo_info = "(".$Lang["ebook_license"]["quotation_number"]." <b>$QuotationNumber</b> : ".$Lang["ebook_license"]["permanent_use"].")<br/><br/>";
    }
    else {
        $PeriodFrom =  date('Y-m-d',strtotime($quo_info["PeriodFrom"]));
        $PeriodTo =  date('Y-m-d',strtotime($quo_info["PeriodTo"]));
        $disp_quo_info = "(".$Lang["ebook_license"]["quotation_number"]." <b>$QuotationNumber</b> : $PeriodFrom - $PeriodTo)<br/><br/>";
    }
}
else {
    $disp_quo_info = $Lang['libms']['bookmanagement']['ebook_license_quotation_error'];
    $QuotationBookCount = 0;
}


# get filter
$filterIsShow = IntegerSafe($_POST['filterIsShow']);


#########################
## Build sql for list view

$sql = $libelibinstall->get_show_or_hide_eBook_sql($QuotationID, $keyword, $filterIsShow);


$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("b.Title", "b.Author", "b.Publisher", "b.Language", "i.IsShow");
$li->sql = $sql;
$li->no_col = 7;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";
$li->no_navigation_row = true;      // don't show navigation
$li->page_size = 9999;              // max page size

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='40%' >".$li->column($pos++, $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle'])."</th>\n";
$li->column_list .= "<th width='22%' >".$li->column($pos++, $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Publisher'])."</th>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang["ebook_license"]["language"])."</th>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['show_or_hide'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("BookID[]")."</td>\n";

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$keyword = htmlspecialchars($keyword);

# Fileter - Book Format
$isShowSelect = '
    <select class="form-control" id="filterIsShow" name="filterIsShow" onchange="document.form1.submit()">
      <option value="" '.(($filterIsShow=='')?'selected':'').'>'.$Lang['libms']['bookmanagement']['alleBooks'].'</option>
      <option value="1" '.(($filterIsShow=='1')?'selected':'').'>'.$Lang['libms']['bookmanagement']['showneBooks'].'</option>
      <option value="0" '.(($filterIsShow=='0')?'selected':'').'>'.$Lang['libms']['bookmanagement']['hiddeneBooks'].'</option>
    </select>';

?>
<head>
    <script>
        $(document).ready( function(){
            <?if ($plugin['eLib_Lite']){?>
            $("#leftmenu").css("display","none");
            $("#content").css("padding-left","0px");
            <?}?>

            $('#keyword').live( 'keyup', function(e){
                var key = e.which || e.charCode || e.keyCode;
                if ( key == 13 ) {
                    $('#form1').submit();
                }
            });

            $('#keyword').live( 'keypress', function(e){
                if ( e.which == 13 ) {
                    e.preventDefault();
                    return false;
                }
            });

        });

        function js_show_book()
        {
            $('#IsShow').val('1');
            checkAlert(document.form1,'BookID[]','elib_setting_show_hide_ebooks_update.php','<?php echo $Lang['libms']['bookmanagement']['ConfirmShoweBook'];?>');
        }

        function js_hide_book()
        {
            $('#IsShow').val('0');
            checkAlert(document.form1,'BookID[]','elib_setting_show_hide_ebooks_update.php','<?php echo $Lang['libms']['bookmanagement']['ConfirmHideeBook'];?>');
        }

    </script>
</head>
<body>
<form name="form1" id="form1" method="post" action="elib_setting_show_hide_ebooks.php">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <?=$disp_quo_info?>
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                <div style="float:right;">
                    <div class="Conntent_search">
                        <input autocomplete="off" name="keyword" id="keyword" type="text" value="<?=$keyword?>" />
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td height="28" valign="bottom" >
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <?=$isShowSelect?>
                        </td>
                        <td valign="bottom">
                            <div class="common_table_tool">
                                <a href="javascript: js_show_book();" class="tool_approve"><?php echo $Lang['Btn']['Show']; ?></a>
                                <a href="javascript: js_hide_book();" class="tool_reject"><?php echo $Lang['Btn']['Hide']; ?></a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>

    <div id="result_table">
        <?=$li->displayFormat_eBook_license_table();?>
    </div>
    <br />

    <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
    <input type="hidden" name="order" value="<?php echo $li->order; ?>" />
    <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
    <input type="hidden" name="page_size_change" value="" />
    <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

    <input type="hidden" name="QuotationID" value="<?=$QuotationID?>" />
    <input type="hidden" name="IsShow" id="IsShow" value="" />

</form>
</body>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
