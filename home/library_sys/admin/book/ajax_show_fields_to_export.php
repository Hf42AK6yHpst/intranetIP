<?php
/*
 * Editing by  
 * 
 * Modification Log: 
 * 
 * 2019-06-14 (Henry)
 * 			- added ItemSeriesNum field
 * 
 * 2016-12-21 (Cameron)
 * 			- create this file
 * 		
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
####### END  OF   Trim all request #######
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
################################################################################################################

## Get Data
$AdvanceSearch = (isset($AdvanceSearch) && $AdvanceSearch != '') ? $AdvanceSearch : '';
$ExcludeWriteoff = (isset($ExcludeWriteoff) && $ExcludeWriteoff != '') ? $ExcludeWriteoff : '';

## Use Library
$libms = new liblms();
$linterface = new interface_html();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$fields = array();
$fields[] = 'BookTitle';
$fields[] = 'BookSubtitle';
$fields[] = 'CallNum';
$fields[] = 'CallNum2';
$fields[] = 'ISBN';
$fields[] = 'ISBN2';
$fields[] = 'Language';
$fields[] = 'Country';
$fields[] = 'Introduction';
$fields[] = 'Edition';
$fields[] = 'PublishYear';
$fields[] = 'Publisher';
$fields[] = 'PublishPlace';
$fields[] = 'Series';
$fields[] = 'SeriesNum';
$fields[] = 'ResponsibilityCode1';
$fields[] = 'ResponsibilityBy1';
$fields[] = 'ResponsibilityCode2';
$fields[] = 'ResponsibilityBy2';
$fields[] = 'ResponsibilityCode3';
$fields[] = 'ResponsibilityBy3';
$fields[] = 'Dimension';
$fields[] = 'ILL';
$fields[] = 'NoOfPage';
$fields[] = 'BookCategoryCode';
$fields[] = 'CirculationTypeCode';
$fields[] = 'ResourcesTypeCode';
$fields[] = 'Subject';
$fields[] = 'BookInternalremark';
$fields[] = 'OpenBorrow';
$fields[] = 'OpenReservation';
$fields[] = 'BookRemarkToUser';
$fields[] = 'Tags';
$fields[] = 'URL';
$fields[] = 'ACNO';
$fields[] = 'BarCode';
$fields[] = 'LocationCode';
$fields[] = 'AccountDate';
$fields[] = 'PurchaseDate';
$fields[] = 'PurchasePrice';
$fields[] = 'ListPrice';
$fields[] = 'Discount';
$fields[] = 'Distributor';
$fields[] = 'PurchaseByDepartment';
$fields[] = 'PurchaseNote';
$fields[] = 'InvoiceNumber';
$fields[] = 'AccompanyMaterial';
$fields[] = 'ItemRemarkToUser';
$fields[] = 'ItemStatus';
$fields[] = 'ItemCirculationTypeCode';
$fields[] = 'ItemSeriesNum';

if ($intranet_session_language == 'en') {
	$fieldTitle = $fields;		
}
else {
	include_once($PATH_WRT_ROOT."lang/libms.lang.b5.php");	
	$fieldTitle = $Lang["libms"]["import_book"]["ImportCSVDataCol_New2"];
	for ($i=0,$iMax=count($fieldTitle); $i<$iMax; $i++)
	{
		$fieldTitle[$i] = strip_tags($fieldTitle[$i]);
		$fieldTitle[$i] = preg_replace('(\+|\*|\@|\#|\^)', "", $fieldTitle[$i]);
		$tmpArr = explode(" ", $fieldTitle[$i] );
		if (is_array($tmpArr) && sizeof($tmpArr)>1)
		{
			$fieldTitle[$i] = trim($tmpArr[0]);
		}
	}
	include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");	
}
$asso_field_title = array();
foreach((array)$fieldTitle as $k=>$v) {
	$asso_field_title[$fields[$k]] = $v;
}
unset($fields);
unset($fieldTitle);

$field_titles = array_chunk($asso_field_title,4,true) ;	// 4 columns


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


$x = '<table width="100%" border=0>';
//$x .= '<tr><td colspan="4"><input type="checkbox" checked name="check_all_fields" id="check_all_fields" onclick="(this.checked)?setChecked(1,this.form,\'ExportFields[]\'):setChecked(0,this.form,\'ExportFields[]\')">'.$Lang['Btn']['All'].'</td></tr>';
$x .= '<tr><td colspan="4"><input type="checkbox" checked name="check_all_fields" id="check_all_fields">'.$Lang['Btn']['All'].' [' .$Lang["libms"]["export_book"]["select_all_remark"].']</td></tr>';

foreach((array)$field_titles as $row) {
	$x .= '<tr>';
	foreach((array)$row as $k=>$v) {
		$x .='<td width="25%" valign="top"><input type="checkbox" checked name="ExportFields[]" id="ExportFields[]" class="fields" value="'.$k.'">'.$v.'</td>';
	}
	$x .= '</tr>';
}
$x .= '</table>';

$x .= '	<br>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			'.$htmlAry['submitBtn'].'
			'.$htmlAry['cancelBtn'].'
			<p class="spacer"></p>
		</div>';

$x .= '
<script>	
	$().ready( function(){
		$("#check_all_fields").click(function(){
			$(".fields").prop("checked",$(this).prop("checked"));
		});

		$("#submitBtn_tb").click(function(){
			if ($(".fields:checked").length<=0) {
				alert(globalAlertMsg2);
				return;
			}
			else {
				var x=\'\';
				$(".fields:checked").each(function(){
					x += \'<input type="hidden" name="hidSelFields[]" id="hidSelFields[]" value="\'+$(this).val()+\'" />\';
				});
				$("#hidFieldSelection").html(x);
				';
if ($ExcludeWriteoff) {
	$x .= 'jsExportBooksWithoutWriteoff("'.$AdvanceSearch.'");';
}
else {
	$x .= 'jsExportBooks("'.$AdvanceSearch.'");';
}					
				
$x .= '			js_Hide_ThickBox();
			}			
		});


	});	
</script>';
echo $x;

################################################################################################################
intranet_closedb();
?>