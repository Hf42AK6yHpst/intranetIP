<?php
/*
 * 	Log
 * 	
 * 	Date:	2015-09-02 [Cameron] 
 * 			- remove argument $BookID_arr from update_ebook_installation() because of using 'IsSelected' field in db
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 


$PATH_WRT_ROOT = "../../../../";


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();

//$LibeLib = new elibrary();
$Libelibinstall = new elibrary_install();

$QuotationID = $_REQUEST['QuotationID'];
//$BookID_arr = $_REQUEST['BookID'];

//if($QuotationID && $BookID_arr && count($BookID_arr)){
//	$Libelibinstall->update_ebook_installation($QuotationID, $BookID_arr);		
if($QuotationID){	
	$Libelibinstall->update_ebook_installation($QuotationID);
}

include_once('elib_setting_license.php');
intranet_closedb();

?>
