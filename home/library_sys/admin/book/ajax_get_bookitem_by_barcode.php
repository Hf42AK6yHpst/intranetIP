<?php
/*
 * 	modifying: 
 * 	Log
 * 	Purpose:	get a row of book record by barcode
 *	@return:	array['html']		-- a table row of record
 *				array['success']	-- successful / failed 
 * 	 
 * 	Date:	2015-07-06 [Cameron] create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$barcode = $_REQUEST['barcode'];
$ID = $_REQUEST['ID'];
$searchChar = '/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~ ]/';
$ret = array();

$sql = "SELECT 
			IF(b.BookTitle IS NULL OR b.BookTitle = '','".$Lang['General']['EmptySymbol']."',b.BookTitle) AS BookTitle,
			IF(bu.ACNO IS NULL OR bu.ACNO = '','".$Lang['General']['EmptySymbol']."',bu.ACNO) AS ACNO,			 
			bu.UniqueID 
		FROM 
			LIBMS_BOOK as b 
		INNER JOIN LIBMS_BOOK_UNIQUE as bu ON bu.BookID=b.BookID
		WHERE bu.BarCode='".$barcode."'	
		LIMIT 1";

$rs = $libms->returnArray($sql);

if(count($rs)>0){
	$curr_rs = $rs[0];		
	$x = '<tr id="'.preg_replace($searchChar,'-',$barcode).'">
				<td>'.$ID.'</td>
				<td>'.$curr_rs['BookTitle'].'</td>
				<td>'.$curr_rs['ACNO'].'</td>
				<td><input type="checkbox" id="BookItemID[]" name="BookItemID[]" value="' . $curr_rs['UniqueID'] . '" checked /></td>
			</tr>';
	$ret['success'] = true;			
}
else{
	$x = '';
	$ret['success'] = false;
}
//error_log("\n\n x before-->".print_r($x,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");

$x = str_replace("\r","",$x);
$x = str_replace("\n","",$x);
$x = str_replace("\t","",$x);

$ret['html'] = $x;
$json = new JSON_obj();
echo $json->encode($ret);

//echo json_encode($ret);

intranet_closedb();
?>