<?php
# being modified by:   
/*
 * Change log:
 * 
 *  Date: 2019-03-28 (Cameron)
 *      - also show BookSubTitle if it's not empty
 *      
 *  Date: 2016-07-29 (Cameron)
 * 		- append timestamp to jquery.alerts.js so that it can refresh to take effect
 * 
 * 	Date: 2016-04-15 [Cameron]
 * 		- Show next ready user to pick up book when prompt to confirm delete reservation record
 *   
 * 	Date: 2015-01-05 [Cameron] Fix to showing ExpiryDate by CAST(rl.ExpiryDate AS CHAR)
 * 
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementReserveList";
$RespondInfo = $libms->GET_BOOK_INFO($Code);
$BookTitle = $RespondInfo[0]['BookTitle'];
$BookSubTitle = $RespondInfo[0]['BookSubTitle'];
if ($BookSubTitle != '') {
    $BookTitle .= '-'.$BookSubTitle;
}

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_reserve']);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_reserve'], "reserve_book.php");
$PAGE_NAVIGATION[] = array($BookTitle, "");

//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == '') ? 1 : $order;
$field = ($field == "") ? 2 : $field;

$conds=' AND rl.`BookID` = ' . PHPToSQL($Code). ' ';
// $keyword = mysql_real_escape_string(htmlspecialchars($_POST['keyword']));
// if($keyword!="")
// 	$conds .= " AND (
// 	`BookCode` LIKE '%$keyword%' OR
// 	`CallNum` LIKE '%$keyword%' OR
// 	`ISBN` LIKE '%$keyword%' OR
// 	`BookTitle` LIKE '%$keyword%' OR
// 	`ResponsibilityBy1` LIKE '%$keyword%'
// 	)";

$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(`BookID` USING utf8)" : "`BookID`";

$sql = <<<EOL

SELECT
	CONCAT(u.ClassName,'-', u.ClassNumber) AS class,
	CASE 
		WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']}) 
		ELSE u.{$Lang['libms']['SQL']['UserNameFeild']}
	END  AS UserName,
	rl.ReserveTime,
	if (rl.RecordStatus='WAITING', '{$Lang["libms"]["book_reserved_status"]["WAITING"]}', if(rl.RecordStatus='READY', CONCAT('{$Lang["libms"]["book_reserved_status"]["READY"]}', if(rl.ExpiryDate<>'' && rl.ExpiryDate<>'0000-00-00' && rl.ExpiryDate IS NOT NULL, CONCAT(' (',CAST(rl.ExpiryDate AS CHAR), ') '), '') ), '&nbsp;')) AS StateAndExpiry,
	CONCAT('<input type=\'checkbox\' name=\'Code[]\' id=\'Code[]\' value=\'', rl.ReservationID ,'\' />')
FROM
	LIBMS_RESERVATION_LOG rl
JOIN    LIBMS_BOOK b ON b.BookID = rl.BookID 
JOIN    LIBMS_USER u ON u.UserID = rl.UserID
WHERE
		
	(
		rl.RecordStatus LIKE 'WAITING'
		OR rl.RecordStatus LIKE 'READY'
	)
	
{$conds}


		

EOL;
/**
		
SELECT
	CONCAT ( u.`ClassName`, u.`ClassNumber) as class,
	b.{$Lang['libms']['SQL']['UserNameFeild']} as UserName,
	rl.`ReserveTime`,
	CONCAT('<input type=\'checkbox\' name=\'Code[]\' id=\'Code[]\' value=\'', `BookID` ,'\' />')

FROM
	`LIBMS_RESERVATION_LOG` rl
JOIN
	`LIBMS_BOOK` b
	ON 
		b.`BookCode`= rl.`BookCode`
JOIN
	`LIBMS_USER` u
	ON 
		u.`UserID`= rl.`UserID`
WHERE
		(b.`RecordStatus` NOT LIKE 'DELETE' OR b.`RecordStatus` IS NULL)
	AND (rl.`RecordStatus LIKE 'WAITING' OR rl.`RecordStatus LIKE 'READY')
	{$conds}
 */

$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array("class", "UserName","ReserveTime", "StateAndExpiry");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='30%' >".$li->column($pos++, $Lang['libms']['book']['class'])."</td>\n";
$li->column_list .= "<th width='35%' >".$li->column($pos++, $Lang['libms']['book']['user_name'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['book']['reserve_time'])."</td>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang['libms']['CirculationManagement']['status'])." (".$Lang["libms"]["CirculationManagement"]["valid_date"].")</td>\n";
$li->column_list .= "<th width='1%'>".$li->check("Code[]")."</td>\n";

//$keyword = htmlspecialchars($_POST['keyword']);
//$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";
//*/
############################################################################################################

# Top menu highlight setting



$linterface->LAYOUT_START();



?>
<link href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?=$intranet_session_language?>.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.js?<?=time()?>"></script>

<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
//                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
//                }
        }
}
//function Check_Go_Search(evt)
//{
//	var key = evt.which || evt.charCode || evt.keyCode;
//	
//	if (key == 13) // enter
//		document.form1.submit();
//	else
//		return false;
//}

$(document).ready(function(){
	$.ajaxSetup({ cache: false});
	
	$('#BtnRemove').click(function(e){
        if(countChecked(document.form1,'Code[]')==0) {
            alert(globalAlertMsg2);
            return false;
        }
        else{
        	e.preventDefault();		
			$.ajax({
				dataType: "json",			
				url : "ajax_getNextReserveUser.php",		
				async: false,		
				type : "POST",		
		        data: $("#form1").serialize(),				
				timeout:1000,
				success: after_getNextReserveUser,
				error: function(){ alert('<?=$Lang["libms"]["general"]["ajaxError"]?>'); }
			});
        }
	});
});

function after_getNextReserveUser(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		var s = ajaxReturn.script;
		if (s != '') {
			s = s.replace("<script>","");
			s = s.replace("<\/script>","");
			eval(s);
		}
	}	
}

//-->
</script>


<!--  <form name="form1" method="post" action=""> -->
<form name="form1" id="form1" method="POST" action="reserve_book_more.php?Code=<?=$Code?>">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
		<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="40%"></td>
											<td width="30%" align="center"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
											<td width="30%" align="right">
												<div class="content_top_tool">
													<div class="Conntent_search"><?=$searchTag?></div>     
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="table-action-bar">
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<!--<td nowrap="nowrap"><a id="BtnRemove" href="javascript:checkRemove(document.form1,'Code[]','reserve_remove.php?BookID=<?=$Code?>')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>-->
																	<td nowrap="nowrap"><a id="BtnRemove" href="" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				<tr><td><br/><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></td></tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
				<input type="hidden" name="BookID" value="<?=$Code?>" />
			</td>
		</tr>
	</table>
</form><?php
//dump ($sql);
$linterface->LAYOUT_STOP();
 
 
 
intranet_closedb();



?>