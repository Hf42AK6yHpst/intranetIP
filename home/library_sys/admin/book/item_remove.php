<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);

# using:   

############ Change Log Start ###############
#
# 20160414 (Cameron) : pass success / fail msg after delete record
#
# 20160406 (Cameron) : check if selected book item is being loaned, don't allow to process if yes. case #S93992
#
# 20130917 (Henry) : Added to Call the function UPDATE_BOOK_COPY_INFO() after deleting the item record
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical_item.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
############################################################################################################

## Get Data
$UniqueID = (isset($UniqueID) && count($UniqueID) > 0) ? $UniqueID : array();


## Use Library
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

$BookInfoAry = $libms->GET_BOOK_INFO($BookID);
$IsPeriodical = $BookInfoAry[0]['IsPeriodical']; 
$PeriodicalBookID = $BookInfoAry[0]['PeriodicalBookID'];
$PeriodicalItemID = $BookInfoAry[0]['PeriodicalItemID'];


# Access Checking
if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if(sizeof($UniqueID)==0) {
	header("Location: index.php");	
}

## check if book is being loaned, don't allow to process if yes
$loanBook = $libms->BookInLoanByUniqueID($UniqueID);
$nrLoanBook = count($loanBook);
if ($nrLoanBook > 0) {
	$ACNO_List = array();
	for($i=0;$i<$nrLoanBook;$i++) {
		$ACNO_List[] = $loanBook[$i]['ACNO'];
	}
	$xmsg = implode(",",$ACNO_List);
	$xmsg = sprintf($Lang['General']['ReturnMessage']['CannotDeleteOnLoanACNO'],$xmsg);
	header("Location: book_item_list.php?BookID=".$BookID."&FromPage=".$FromPage."&DirectMsg=T&xmsg=".$xmsg);
	exit;
}			


## Main
$result = array();
$result['remove_book_item'] = $libms->REMOVE_BOOK_ITEM($UniqueID);

$tempArr = $libms->GET_BOOK_ITEM_INFO($UniqueID[0]);
$result['book_copy'] = $libms->UPDATE_BOOK_COPY_INFO($tempArr[0]["BookID"]);

if ($IsPeriodical && $PeriodicalBookID > 0 && $PeriodicalItemID > 0) {
	$objItem = new liblms_periodical_item($PeriodicalItemID);
	$objItem->setQuantity($objItem->getQuantity() - sizeof($UniqueID));
	$itemId = $objItem->save();
	
	$result['update_periodical_item_quantity'] = ($itemId > 0)? true : false; 
}

$xmsg = in_array(false,$result) ? 'DeleteUnsuccess': 'DeleteSuccess'; 
 
############################################################################################################
intranet_closedb();
header("Location: book_item_list.php?BookID=".$BookID."&FromPage=".$FromPage."&xmsg=".$xmsg);


?>