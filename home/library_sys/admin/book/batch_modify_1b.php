<?php

//modifying: 

/************************************
 * Change Log
 * Date:	2017-02-16 [Henry]
 * 			- change method to POST at form submit [Case#C111879]
 *
 * Date:	20140728 (Henry)
 * Details: display Chinese reader name when the page language is Chinese
 * 
 ************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);
$TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_return_date']);
//$TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_return_date'] , "batch_modify_1.php", 1);
//$TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_penal_sum'], "batch_modify_2.php", 0);
//$TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_book_info'], "batch_modify_3.php", 0);

//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

# date range
$FromDate=$FromDate==""?date('Y-m-d'):$FromDate;
$ToDate=$ToDate==""?date('Y-m-d'):$ToDate;

if($readerClass){
	$classList ='"';
	if(is_array($readerClass)){
		$classList .= implode('","',$readerClass);
	}
	else{
		$classList .= $readerClass;
	}
	$classList .='"';
}

if($readerGroup){
	$groupList ='"';
	if(is_array($readerGroup)){
		$groupList .= implode('","',$readerGroup);
	}
	else{
		$groupList .= $readerGroup;
	}
	$groupList .='"';
}
$classCond = "";
$groupTable = "";
$groupCond = "";
$dateCond = "";
if($radios == 1)
	$classCond = 'AND a.ClassName IN('.$classList.')';
else if($radios == 2){
	$groupTable = 'JOIN LIBMS_GROUP_USER as e ON a.UserID = e.UserID';
	$groupCond = 'AND e.GroupID IN('.$groupList.')'; //should modify
}	
$tempFromDate = $FromDate." 00:00:00";
$tempToDate = $ToDate." 23:59:59";
if($periodBy == 2){
	$dateCond = "AND b.DueDate >= '{$FromDate}' AND b.DueDate <= '{$ToDate}'";
}
else{
	$dateCond = "AND b.BorrowTime >= '{$tempFromDate}' AND b.BorrowTime <= '{$tempToDate}'";
}

//get the circulation type code
$circulationCond= "";
if($CirculationTypeCode){
	$circulationCond = "AND c.CirculationTypeCode = '{$CirculationTypeCode}' ";
}
//$userName = getNameFieldByLang("a.");
//debug_pr($userName);
$sql = "select IF(a.ClassName IS NULL OR a.ClassName = '','".$Lang['General']['EmptySymbol']."',a.ClassName) AS ClassName, 
			IF(a.ClassNumber IS NULL OR a.ClassNumber = '','".$Lang['General']['EmptySymbol']."', a.ClassNumber) AS ClassNumber,
		 IF(a.ChineseName IS NULL OR a.ChineseName = '','".$Lang['General']['EmptySymbol']."', a.ChineseName) AS ChineseName,
a.EnglishName, a.BarCode, d.ACNO, c.BookTitle, b.BorrowTime, b.DueDate, b.BorrowLogID from LIBMS_USER as a 
JOIN LIBMS_BORROW_LOG as b
ON a.UserID = b.UserID
JOIN LIBMS_BOOK as c
ON b.BookID = c.BookID
JOIN LIBMS_BOOK_UNIQUE as d
ON b.UniqueID = d.UniqueID
{$groupTable}
WHERE b.RecordStatus = 'BORROWED'
{$classCond}
{$groupCond}
{$dateCond}
{$circulationCond}
ORDER BY a.ClassName, a.ClassNumber";
//debug_pr($sql);

$result = $libms->returnArray($sql);
$x = '<table class="common_table_list_v30  view_table_list_v30" width="100%">
			<tr>
				<th width="1">#</th>
				<th width="5%">'.$Lang["libms"]["report"]["ClassName"].'</th>
				<th width="5%">'.$Lang["libms"]["report"]["ClassNumber"].'</th>
				<!--<th>中文全名</th>-->
				<th width=\"25%\">'.Get_Lang_Selection($Lang["libms"]["report"]["ChineseName"],$Lang["libms"]["report"]["EnglishName"]).'</th>
				<th width=\"10%\">'.$Lang["libms"]["CirculationManagement"]["user_barcode"].'</th>
				<th width=\"10%\">ACNO</th>
				<th width="25%">'.$Lang["libms"]["report"]["booktitle"].'</th>
				<th width="15%">'.$Lang["libms"]["report"]["borrowdate"].'</th>
				<th width="10%">'.$Lang["libms"]["batch_edit"]["DueDate"].'</th>
				<th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'BorrowLogID[]\',this.checked)" checked /></th>
			</tr>';
			
$BorrowLogID = array();
//$hiddenStr ="";
if(sizeof($result)>0){		
	for ($i=0; $i<sizeof($result); $i++){
		$x .= '<tr>
					<td>'.($i+1).'</td>
					<td>'.$result[$i]['ClassName'].'</td>
					<td>'.$result[$i]['ClassNumber'].'</td>
					<!--<td>'.$result[$i]['ChineseName'].'</td>-->
					<td>'.Get_Lang_Selection($result[$i]['ChineseName'],$result[$i]['EnglishName']).'</td>
					<td>'.$result[$i]['BarCode'].'</td>
					<td>'.$result[$i]['ACNO'].'</td>
					<td>'.$result[$i]['BookTitle'].'</td>
					<td>'.$result[$i]['BorrowTime'].'</td>
					<td>'.$result[$i]['DueDate'].'</td>
					<td><input type="checkbox" id="BorrowLogID[]" name="BorrowLogID[]" value="' . $result[$i]['BorrowLogID'] . '" checked /></td>
				</tr>';
			
		$BorrowLogID[$i] = $result[$i]['BorrowLogID'];
		 //$hiddenStr .= '<input name="BorrowLogID[]" type="hidden" value="'.$BorrowLogID[$i].'"/>';
	}
}
else{
	 # no record
	$x.="<tr class=\"tablebluerow2 tabletext\"><td class=\"tabletext\" colspan=\"10\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td></tr>";
}
$x .= '</table>';
//debug_pr($BorrowLogID);
$display = $x;

$dropDownList = '<select name="ChangeDay">';
for($i=1; $i<=30; $i++){
	$dropDownList .= "<option value=".$i.">".$i."</option>";
}
$dropDownList .= "</select>";
############################################################################################################

# Top menu highlight setting

//$TAGS_OBJ[] = array($Lang["libms"]["stats"]["frequency"]);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--

function checkForm(formObj){
        if(formObj==null) return false;
        //alert(formObj);
	    fromV = formObj.ChangeDate;
	    
	    if(!checkDate(fromV)){
            //formObj.FromDate.focus();
            return false;
	    }
	    else if(fromV.value < "<?=date('Y-m-d')?>"){
	    	alert("<?=$Lang['libms']['batch_edit']['PleaseSelectDateAfterToday']?>");
	    	return false;
	    }
	    //alert(fromV.value);
	    return true;
}

function checkDate(obj){
        if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
        return true;
}


function submitForm(obj){
        if(checkForm(obj)) {
			obj.submit();
		}
}

//-->
</script>

<form name="form1" method="post" action="batch_modify_1b_update.php">
<!--###### Content Board Start ######-->

<table width="99%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="13" >&nbsp;</td>
                      <td ><!---->
                        <!--#### Navigator start ####-->
                        <!--<div class="navigation"><a href="#">User List</a>New User<br />
                      </div>-->
                        <!--#### Navigator end ####-->
                       <!--<div class="report_option report_option_title">- 選項 - </div>-->
       			    <div class="table_board">
                      <table class="form_table">
                        <tr>
                          <td colspan="2"><?=sprintf($Lang['libms']['batch_edit']['record_found'],sizeof($result))?></td>
                        </tr>
                        <tr>
                          <td class="field_title"><?=$Lang['libms']['batch_edit']['modify_due_date']?></td>
                          <td>
                          	<input type="radio" name="periodTo" id="periodTo2" value="2" onChange="document.getElementById('div_dateSelect').style.display='';document.getElementById('div_daySelect').style.display='none';" checked="checked" /> <label for="periodTo2"><?=$Lang['libms']['batch_edit']['edit_return_date_same']?></label>
                          　<input type="radio" name="periodTo" id="periodTo1" value="1" onChange="document.getElementById('div_dateSelect').style.display='none';document.getElementById('div_daySelect').style.display='';" /> <label for="periodTo1"><?=$Lang['libms']['batch_edit']['edit_return_date_defer']?></label>
                          　<input type="radio" name="periodTo" id="periodTo3" value="3" onChange="document.getElementById('div_dateSelect').style.display='none';document.getElementById('div_daySelect').style.display='';" /> <label for="periodTo3"><?=$Lang['libms']['batch_edit']['edit_return_date_early']?></label>
                          <div id="div_dateSelect" style="<?=($periodTo == 2 || !$periodTo?"":"display:none")?>">
                          <p> &nbsp; <?=$linterface->GET_DATE_PICKER("ChangeDate",$ChangeDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span></p>
						</div>
						<div id="div_daySelect" style="<?=($periodTo == 1 || $periodTo == 3?"":"display:none")?>">
                          <p> &nbsp;  
                          <?=$dropDownList?>
			<span class="tabletextremark"><?=$Lang["libms"]["batch_edit"]["Day"]?></span></p>
						</div>
							</td>
                        </tr>
                        <tr>
                          <td class="field_title"><?=$Lang["libms"]["batch_edit"]["send_email"]?></td>
                          <td>
                          	<input type="radio" name="noticeBy" id="noticeBy2" value="2" checked="checked" /> <label for="noticeBy2"><?=$Lang["libms"]["batch_edit"]["send_email_yes"]?></label>
                          　<input type="radio" name="noticeBy" id="noticeBy1" value="1" /> <label for="noticeBy1"><?=$Lang["libms"]["batch_edit"]["send_email_no"]?></label>
							</td>
                        </tr>
                      </table>
   			        </div>
   			       <br/>
   	<?=$display?>		 <br/>        
                      <div class="edit_bottom">        
                        <p class="spacer"></p>
                        
                        
                        <?if(sizeof($result)>0){
                        	echo $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);","btn_submit");
                        }?>
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='batch_modify_1.php'","btn_cancel") ?>
                        
                      </div>
				      </td>
                      <td width="11" >&nbsp;</td>
                    </tr>
                  </table>
                  			  
			  
			  
			  <!--###### Content Board End ######-->
			  
</form><?php

$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
