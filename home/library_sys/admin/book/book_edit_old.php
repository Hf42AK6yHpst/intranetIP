<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// error_reporting(E_ERROR | E_WARNING | E_PARSE);
// ini_set('display_errors', 1);

# using: 

############ Change Log Start ###############
#
#	Date:	2012-06-07	Jason
#			
#
############ Change Log End ###############

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";





include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

$home_header_no_EmulateIE7 = true;

intranet_auth();
intranet_opendb();
# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagement";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$linterface = new interface_html("libms.html");

$Code = (is_array($Code)) ? $Code[0] : $Code;

$RespondInfo = $libms->GET_BOOK_INFO($Code);



$BookTitle = $RespondInfo[0]['BookTitle'];
$BookSubTitle = $RespondInfo[0]['BookSubTitle'];
if (isset($IsCopy) && $IsCopy)
{
	$NoOfCopy = 0;
	$NoOfCopyAvailable = 0;
	
} else
{
	$BookCode = $RespondInfo[0]['BookCode'];
	$NoOfCopy = $RespondInfo[0]['NoOfCopy'];
	$NoOfCopyAvailable = $RespondInfo[0]['NoOfCopyAvailable'];
$BarCodeNewStr = "[new]";
	
}

$CallNum = $RespondInfo[0]['CallNum'];
$CallNum2 = $RespondInfo[0]['CallNum2'];
$ISBN = $RespondInfo[0]['ISBN'];
$ISBN2 = $RespondInfo[0]['ISBN2'];
$BarCode = $RespondInfo[0]['BarCode'];
$Introduction = $RespondInfo[0]['Introduction'];
$Language = $RespondInfo[0]['Language'];
$Country = $RespondInfo[0]['Country'];
$Edition = $RespondInfo[0]['Edition'];
$PublishPlace = $RespondInfo[0]['PublishPlace'];
$Publisher = $RespondInfo[0]['Publisher'];
$PublishYear = $RespondInfo[0]['PublishYear'];
$NoOfPage = $RespondInfo[0]['NoOfPage'];
$RemarkInternal = $RespondInfo[0]['RemarkInternal'];
$RemarkToUser = $RespondInfo[0]['RemarkToUser'];
$PurchaseDate = $RespondInfo[0]['PurchaseDate'];
$Distributor = $RespondInfo[0]['Distributor'];
$PurchaseNote = $RespondInfo[0]['PurchaseNote'];
$PurchaseByDepartment = $RespondInfo[0]['PurchaseByDepartment'];
$ListPrice = $RespondInfo[0]['ListPrice'];
$Discount = $RespondInfo[0]['Discount'];
$PurchasePrice = $RespondInfo[0]['PurchasePrice'];
$AccountDate = $RespondInfo[0]['AccountDate'];
$CirculationTypeCode = $RespondInfo[0]['CirculationTypeCode'];
$ResourcesTypeCode = $RespondInfo[0]['ResourcesTypeCode'];
$BookCategoryCode = $RespondInfo[0]['BookCategoryCode'];
$eClassBookCode = $RespondInfo[0]['eClassBookCode'];
$LocationCode = $RespondInfo[0]['LocationCode'];
$Subject = $RespondInfo[0]['Subject'];
$Series = $RespondInfo[0]['Series'];
$SeriesNum = $RespondInfo[0]['SeriesNum'];
$URL = $RespondInfo[0]['URL'];
$ResponsibilityCode1 = $RespondInfo[0]['ResponsibilityCode1'];
$ResponsibilityBy1 = $RespondInfo[0]['ResponsibilityBy1'];
$ResponsibilityCode2 = $RespondInfo[0]['ResponsibilityCode2'];
$ResponsibilityBy2 = $RespondInfo[0]['ResponsibilityBy2'];
$ResponsibilityCode3 = $RespondInfo[0]['ResponsibilityCode3'];
$ResponsibilityBy3 = $RespondInfo[0]['ResponsibilityBy3'];
$CoverImage = $RespondInfo[0]['CoverImage'];
$OpenBorrow = $RespondInfo[0]['OpenBorrow'];
$OpenReservation = $RespondInfo[0]['OpenReservation'];
$RecordStatus = $RespondInfo[0]['RecordStatus'];

############################################################################################################


$TAGS_OBJ[] = array($Lang['libms']['action']['edit_book']);

$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_list'], "index.php");
if (isset($IsCopy) && $IsCopy)
{
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Copy'], "");
} else
{
	$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
}
$CurrentPage = "PageBookManagementBookList";

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

#Save and Cancel Button ...........
$SaveBtn   = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "SubmitBtn", "");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'", "CancelBtn", "");

# Responsibility ........... 
$ResponsibilityCodeArrary = $libms->BOOK_GETOPTION('LIBMS_RESPONSIBILITY', 'ResponsibilityCode', $Lang['libms']['sql_field']['Description'], 'ResponsibilityCode', '');
$ResponsibilityCodeOption1 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption1' id='ResponsibilityCodeOption1' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $ResponsibilityCode1);
$ResponsibilityCodeOption2 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption2' id='ResponsibilityCodeOption2' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $ResponsibilityCode2);
$ResponsibilityCodeOption3 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption3' id='ResponsibilityCodeOption3' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $ResponsibilityCode3);

# Option ..........
$BookCategoryArray = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', '');
$BookCategoryOption = $linterface->GET_SELECTION_BOX($BookCategoryArray, " name='BookCategory' id='BookCategory' ", $Lang['libms']['status']['na'], $BookCategoryCode);

$BookCirculationArray = $libms->BOOK_GETOPTION('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', $Lang['libms']['sql_field']['Description'], 'CirculationTypeCode', '');
$BookCirculationOption = $linterface->GET_SELECTION_BOX($BookCirculationArray, " name='BookCirclation' id='BookCirclation' ", $Lang['libms']['status']['na'], $CirculationTypeCode);

$BookResourcesArray = $libms->BOOK_GETOPTION('LIBMS_RESOURCES_TYPE', 'ResourcesTypeCode', $Lang['libms']['sql_field']['Description'], 'ResourcesTypeCode', '');
$BookResourcesOption = $linterface->GET_SELECTION_BOX($BookResourcesArray, " name='BookResources' id='BookResources' ", $Lang['libms']['status']['na'], $ResourcesTypeCode);

$BookLocationArray = $libms->BOOK_GETOPTION('LIBMS_LOCATION', 'LocationCode', $Lang['libms']['sql_field']['Description'], 'LocationCode', '');
$BookLocationOption = $linterface->GET_SELECTION_BOX($BookLocationArray, " name='BookLocation' id='BookLocation' ", $Lang['libms']['status']['na'], $LocationCode);

# Book Status ......
$y = 0;
foreach ($Lang['libms']['book_status'] as $key => $value)
{
	$BookStatus[$y][0] = $key;
	$BookStatus[$y][1] = $value;
	$y++;
}
$BookStatusOption = $linterface->GET_SELECTION_BOX($BookStatus, " name='BookStatus' id='BookStatus' ", '', 'NORMAL');


# Book Tag ..........
$BookTagArray = $libms->SELECTVALUE('LIBMS_TAG', 'TagName', 'TagName');
foreach ($BookTagArray as $value) $Taglist .="'".addslashes($value[TagName])."', ";
$Taglist = rtrim($Taglist,', ');

# Book selected Tag..............................
$SelectedBookTag = $libms->SELECTVALUE('LIBMS_BOOK_TAG JOIN LIBMS_TAG', 'TagName', 'TagName', 'LIBMS_BOOK_TAG.TagID = LIBMS_TAG.TagID AND BookID='.$Code);
foreach ($SelectedBookTag as $value) $SelectedTaglist .="'".addslashes($value[TagName])."', ";
$SelectedTaglist = rtrim($SelectedTaglist,', ');


?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		
<script src="./TextExt.js" type="text/javascript" charset="utf-8"></script>

<script language="javascript">
<!--
	
function checknumeric(){
	var checking = true;
	$('.numberic').each(function() {
		if (($(this).val() != "") && (isNaN(Number($(this).val())) || $(this).val() < 0)){
			var field = $(this).parent().prev().find('span').html();
			alert("<?=$Lang["libms"]["book"]["alert"]["numeric"]?> "+field);	
			$(this).focus(); 
			checking = false;
			return false;	
	}});
	return checking;
};


function checkForm(form1) {

var selfcheck = true;

	
	if(form1.BookTitle.value=='')	 {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['title'] ?>");	
		form1.BookTitle.focus();
		return false;
	} else if($('#BookCode').val()=="") {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['code'] ?>");	
		$('#BookCode').focus(); 
		return false;
	} else if(!checknumeric()) {
		return false;
	//} else if(!selfcheck || !unique_barcode_check || !unique_bookcode){
	//	alert("<?= $Lang['libms']['book']['code']."/".$Lang['libms']['book']['barcode'] ." incorrect" ?>");	
	//	return false;
	} else {
		return true;
	}
}
//-->


var count_available ;
var unique_bookcode = true;
var fSubmit = false; 

var timerobj;

function jsValidateACNO()
{
	//var tmerObj = setInterval('$(\'#BookCode\').change()',1000);
	//clearInterval(tmerObj);
	
	timerobj = setTimeout(validateAction,1000);
}

function validateAction()
{
	$('#BookCode').change();
	clearTimeout(timerobj);
}


//Press to next barcode if not find add a row...................................................................
function next_barcode(THIS){
		parent_TR = THIS.parents('tr:first');
		NEXT_barcode = parent_TR.next('tr').find('.book_barcode');
		if (NEXT_barcode.length == 0) {
			add_row(THIS);
			parent_TR.next('tr').find('.book_barcode').focus();
		} else {
			NEXT_barcode.focus();
		}
}
function autogen_on_click(JQueryObj){
	THIS =JQueryObj;
	txtBarcode=THIS.parents('tr:first').find('.book_barcode');
	if (THIS.is(":checked"))
		txtBarcode.val("***AUTO GEN***").hide();
	else
		txtBarcode.val("").show();
	txtBarcode.trigger('change');
	next_barcode(THIS);
}
function barcode_change(mememe) {
	mememe.val(mememe.val().toUpperCase());
	count_no_of_book();
	count_availbale_book();
	//Unique barcode...................................................................
	mememe.parent('td').next('td').html('<img src="loader.gif" align="absmiddle"/>');
	
	var unique_barcode_check = true;
	if (mememe.val() != '' && mememe.val() !='***AUTO GEN***'){
		barcodeRespond = $.get('ajax_check_barcode.php', { barcode : mememe.val()},
			function(barcodeRespond){
				barcodeRespond = $.trim(barcodeRespond);
				if (barcodeRespond == '0'){
					unique_barcode_check = true;
				}else if (barcodeRespond == '1'){
					unique_barcode_check = false;
				}
				
			}
		);
	}
	//Count total no of book......................................................................
	var selfcheck = true;

	
	$('.book_barcode').each(function(){

		//Selfcheck............................................................................................
		if(selfcheck){
			if (mememe.val() != '' && mememe.val() !='***AUTO GEN***'){
				if (!$(this).is(mememe)){
					if($(this).val() == mememe.val() )
					{
						selfcheck = false;

					}
				}
			}
		}
		//Selfcheck end ....................................................................................
	});

	//Show status box......................................................................
	if(mememe.val() != ''){
	mememe.parents('tr:first').find('.unqiue_bk_status').show();
	} else {
	mememe.parents('tr:first').find('.unqiue_bk_status').hide();
	}

	//toggle ajax check img
	if (!(selfcheck && unique_barcode_check))
	{
		mememe.parent('td').next('td').html('<img src="not_available.png" align="absmiddle"/>');
	}else if (mememe.val() == '' || mememe.val() =='***AUTO GEN***'){
		mememe.parent('td').next('td').html('');
	}else{
		mememe.parent('td').next('td').html('<img src="available.png" align="absmiddle"/>');
	}
	
}

function count_no_of_book(){
	var count = 0 ;
	
	//$('tr.record .book_barcode').each(function(){
	$('.book_barcode').each(function(){
		//Count total no of book......................................................................
		if ($(this).val() != '')
		{ 
				count++; 
		}
	});

	$('#BookNocopy').val(count);
	$('#lbl_BookNocopy').html(count);
}
//new row added...................................................................
function add_row(jThis){
	<?
		$selectionbox=$linterface->GET_SELECTION_BOX($BookStatus, ' name="barcode_bookstatus'.$BarCodeNewStr.'[]" style="display:none" class=" unqiue_bk_status barcode_bookstatus" ', '', 'NORMAL');
		$selectionbox = str_replace("\n",'', $selectionbox);
	
	?>
	
		
		$('table#barcode > tbody:last').append('<tr><td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['barcode'] ?><label class="barcode_number" /></span></td><td> <?=$Lang['libms']['general']['auto']?>: <input name="GenBarcode[]" type="checkbox" class="gen_barcode" value=""/></td><td ><input name="BookBarcode<?=$BarCodeNewStr?>[]" type="text" class="textboxtext book_barcode" maxlength="64" /></td><td class="icon">&nbsp;</td><td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['book_status'] ?></span></td><td ><?=$selectionbox?></td><td><input type="button" class="btn_deleteRow" value="<?=$Lang['libms']['book']['delete']?>" style="display:none" /><input type="button" class="btn_addRow" value="<?= $Lang['libms']['book']['add'] ?>" /></td></tr>');
		$("input.book_barcode:last").change(function(){barcode_change($(this));});
		$('input.gen_barcode:last').change(function(){autogen_on_click($(this));});
		$('input.book_barcode:last').keypress(function(e) { onKeyUp($(this),e); });
		$('input.btn_deleteRow:last').click(function () { del_row($(this)); });
		$('input.btn_addRow:last').click(function () { add_row($(this)); });
		
		parent_TR = jThis.parents('tr:first');
		parent_TR.find('input.btn_addRow').hide();
		parent_TR.find('input.btn_deleteRow').show();
		parent_TR.addClass('record');
		

		count_no_of_book();
		count_availbale_book();
		update_barcode_number();
}

function del_barcode(jThis){
	thisTR = jThis.parents('tr:first');
	thisTR.hide();
	thisTR.find('.barcode_bookstatus').append($('<option>', { 
	    value: 'DELETE', 
	    text : 'DELETE' 
	  })).val('DELETE');
	thisTR.find('.book_barcode').val('');
	thisTR.removeClass('record');
	count_no_of_book();
	count_availbale_book();
	update_barcode_number();
}

function update_barcode_number(){
	counter =1;
	$('tr.record').each( function() {
		$(this).find('label.barcode_number').html(counter);
		counter++;
	});
}

function count_availbale_book(){
	count_available = 0 ;
	$('.barcode_bookstatus').each( function() {
		parent_tr=$(this).parents('tr:first');
		if(parent_tr.find('.book_barcode').val() != "")
		if ((($(this).val() == 'NORMAL') || ($(this).val() == 'SHELVING'))&&($(this).val() != ''))
			{ count_available++; }
		});
	$('#BookNoavailable').val(count_available);
	$('#lbl_BookNoavailable').html(count_available);
}

//Key up handler for barcode ................................
function onKeyUp(THIS,event){
	if (event.which == 13) {
	    event.preventDefault();
	    next_barcode(THIS);
	    return false;
	  }  
}

$().ready( function(){
	$.ajaxSetup({	cache:false,
					async:false
	});

	//disable enter as submit
	$('input').keydown(function(e) {
		if (event.which == 13) {
		event.preventDefault();
		return false;
		}
	});
	// autocomplete book tag
	$('#BookTag')
		.textext({
			plugins : 'tags autocomplete',
			tagsItems: [ <?= $SelectedTaglist?> ] //selected tags
		})
		.bind('getSuggestions', function(e, data)
		{
			var list = [<?= $Taglist?>],
				textext = $(e.target).textext()[0],
				query = (data ? data.query : '') || ''
				;

			$(this).trigger(
				'setSuggestions',
				{ result : textext.itemManager().filter(list, query) }
			);
		});
	
	// barcode field add and delete
	$('.btn_addRow').click(function () {
		add_row($(this));
	});
	
	
   $('.btn_deleteRow').click(function () {
	   del_row($(this));
    });
	
	//Barcode input ...................................................
	$('.book_barcode').change(function() {barcode_change($(this));});
	
	//auto gen bookcode....................................................
	$('#BookCode')
        .textext({
            plugins : 'autocomplete ajax',
            ajax : {
                url : 'ajax_ACNO_suggestion.php',
                dataType : 'json',
                cacheResults : false
            }
        });
	
	//BookCode unique................................................
	
	$('#BookCode').change(function(){
		$('#BookCode').parent('td').next('td').html('<img src="loader.gif" align="absmiddle"/>');
		$.get('ajax_check_bookcode.php', { bookcode : $('#BookCode').val() , ID : <?= $Code ?>},
				function(bookcodeRespond){
					if (bookcodeRespond == '0'){
						$('#BookCode').parents('tr:first').find('td#bookcode_error span.err_img').html('<img src="available.png" align="absmiddle"/>');
						unique_bookcode = true; 
					}else if (bookcodeRespond == '1'){
						$('#BookCode').parents('tr:first').find('td#bookcode_error span.err_img').html('<img src="not_available.png" align="absmiddle"/>');
						unique_bookcode = false;
					}
					
				}
		);
	});
		
	//Count total available no of book......................................................................
	$('.barcode_bookstatus').change( function(){
		count_availbale_book();
	});
	
	
	$('.book_barcode').keypress(function(e) {
			return onKeyUp($(this),e);
	});
	$('.btn_delete_Record').click(function(){del_barcode($(this));});
	//Auto-gen Barcode
	$('.gen_barcode').change(function(){autogen_on_click($(this));});
		
	
});

function del_row(JQueryObj){
	JQueryObj.parents('tr:first').remove();
	update_barcode_number();
}
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<?php if (isset($IsCopy) && $IsCopy) { ?>
<form name="form1" method="POST" action="book_new_update.php"  onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data" >
<?php } else { ?>
<form name="form1" method="POST" action="book_edit_update.php"  onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data" >
<input name="Code" type="hidden" class="textboxtext" value="<?=$Code?>" />
<?php } ?>
<table width="90%" border="0" align="center">
	<tr> 
		<td class="main_content">
			<div class="table_board">
                <p class="spacer"></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="tabletextrequire">*</span> <?= $Lang['libms']['book']['title'] ?></td>
                        	<td width="33%"><input name="BookTitle" type="text" class="textboxtext" maxlength="128" value="<?=$BookTitle?>"/></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['subtitle'] ?></span></td>
                         	<td width="33%"><input name="BookSubtitle" type="text" class="textboxtext" maxlength="64" value="<?=$BookSubTitle?>"/></td>
						</tr>
						<!--
						<tr>
                        	<td class="field_title_short"><span class="tabletextrequire">*</span> <?= $Lang['libms']['book']['code'] ?></td>                        	
                        	<td><input autocomplete="off" id="BookCode" name="BookCode" type="text" class="textboxtext" maxlength="16" value="<?=$BookCode?>"/></td>
                        	<td id='bookcode_error' colspan="3"><span class="tabletextremark"><?=$Lang["libms"]["book"]["code_format"]?></span></td>
							
                        </tr>
                        -->
                        <tr>
								<td class="field_title_short"><span class="tabletextrequire">*</span>
									<?= $Lang['libms']['book']['code'] ?></td>
								<td><input autocomplete="off" id="BookCode" name="BookCode"
									type="text" class="textboxtext" maxlength="16" onFocusOut="jsValidateACNO();"  value="<?=$BookCode?>" /></td>
								
								<td id='bookcode_error' colspan="3"><span class='err_img'></span><span class="tabletextremark"><?=$Lang["libms"]["book"]["code_format"]?></span></td>

								
							</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['call_number'] ?></span></td>
                        	<td><input name="BookCallno" type="text" class="textboxtext" maxlength="64" value="<?=$CallNum?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['call_number2'] ?></span></td>
                         	<td><input name="BookCallno2" type="text" class="textboxtext" maxlength="64" value="<?=$CallNum2?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ISBN'] ?></span></td>
                        	<td><input name="BookISBN" type="text" class="textboxtext" maxlength="64" value="<?=$ISBN?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ISBN2'] ?></span></td>
                         	<td><input name="BookISBN2" type="text" class="textboxtext" maxlength="64" value="<?=$ISBN2?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['language'] ?></span></td>
                        	<td><input name="BookLang" type="text" class="textboxtext" maxlength="32" value="<?=$Language?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['country'] ?></span></td>
                         	<td><input name="BookCountry" type="text" class="textboxtext" maxlength="128" value="<?=$Country?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['introduction'] ?></span></td>
                        	<td colspan="4"><textarea name="BookIntro" class="textboxtext"><?=$Introduction?></textarea></td>
						</tr>		
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_publish'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['edition'] ?></span></td>
                        	<td width="33%"><input name="BookEdition" type="text" class="textboxtext" maxlength="32" value="<?=$Edition?>"/></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publish_year'] ?></span></td>
                         	<td width="33%"><input name="BookPublishyear" id="BookPublishyear" type="text" class="textboxnum numberic" maxlength="4" value="<?=$PublishYear?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publisher'] ?></span></td>
                        	<td><input name="BookPublisher" type="text" class="textboxtext" maxlength="32" value="<?=$Publisher?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publish_place'] ?></span></td>
                         	<td><input name="BookPublishplace" type="text" class="textboxtext" maxlength="64" value="<?=$PublishPlace?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['series'] ?></span></td>
                        	<td><input name="BookSeries" type="text" class="textboxtext" maxlength="64" value="<?=$Series?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['series_number'] ?></span></td>
                         	<td><input name="BookSeriesno" type="text" class="textboxtext" maxlength="32" value="<?=$SeriesNum?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code1'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption1?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by1'] ?></span></td>
                         	<td><input name="BookResponb1" type="text" class="textboxtext" maxlength="64" value="<?=$ResponsibilityBy1?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code2'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption2?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by2'] ?></span></td>
                         	<td><input name="BookResponb2" type="text" class="textboxtext" maxlength="64" value="<?=$ResponsibilityBy2?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code3'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption3?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by3'] ?></span></td>
                         	<td><input name="BookResponb3" type="text" class="textboxtext" maxlength="64" value="<?=$ResponsibilityBy3?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_page'] ?></span></td>
                        	<td><input name="BookNopage" type="text" class="textboxnum" maxlength="4" value="<?=$NoOfPage?>"/></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_category'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" ><span class="field_title"><?= $Lang['libms']['settings']['book_category'] ?></span></td>
                        	<td width="33%"><?=$BookCategoryOption?></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" ><span class="field_title"><?= $Lang['libms']['settings']['circulation_type'] ?></span></td>
                         	<td width="33%"><?=$BookCirculationOption?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['settings']['resources_type'] ?></span></td>
                        	<td><?=$BookResourcesOption?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['settings']['book_location'] ?></span></td>
                         	<td><?=$BookLocationOption?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['subject'] ?></span></td>
                        	<td><input name="BookSubj" type="text" class="textboxtext" maxlength="32" value="<?=$Subject?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['account_date'] ?></span></td>
                         	<td><?=$linterface->GET_DATE_PICKER("BookAccdate", $AccountDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_internal'] ?></span></td>
                         	<td><input name="BookInternalremark" type="text" class="textboxtext" maxlength="32" value="<?=$RemarkInternal?>"/></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_purchase'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_date'] ?></span></td>
                        	<td width="33%"><?=$linterface->GET_DATE_PICKER("BookPurchasedate", $PurchaseDate)?><span class="tabletextremark">(YYYY-MM-DD)</span></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_price'] ?></span></td>
                         	<td width="33%"><input name="BookPurchaseprice" type="text" class="textboxnum numberic" maxlength="8" value="<?=$PurchasePrice?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['list_price'] ?></span></td>
                        	<td><input name="BookListprice" type="text" class="textboxnum numberic" maxlength="8" value="<?=$ListPrice?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['discount'] ?></span></td>
                         	<td><input name="BookDiscount" type="text" class="textboxnum numberic" maxlength="8" value="<?=$Discount?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['distributor'] ?></span></td>
                        	<td><input name="BookDistributor" type="text" class="textboxtext" maxlength="64" value="<?=$Distributor?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_by_department'] ?></span></td>
                         	<td><input name="BookPurchasedept" type="text" class="textboxtext" maxlength="32" value="<?=$PurchaseByDepartment?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_note'] ?></span></td>
                        	<td><input name="BookPurchasenote" type="text" class="textboxtext" maxlength="32" value="<?=$PurchaseNote?>"/></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				

				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_other'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td  class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['cover_image'] ?></span></td>
                        	<td colspan="3"><input name="BookCover" id="BookCover" type="file" /><?= $CoverImage?></td>
						</tr>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['tags'] ?></span></td>
                        	<td width="70%"><textarea autocomplete="off" id="BookTag" name="BookTag" class="textboxtext" rows="1" style='width :99%'
							></textarea></td>
                        	<td width="15%"> <span class="tabletextremark"><?=$Lang['libms']['book']['tags_input']?></span></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['URL'] ?></span></td>
                        	<td ><input name="BookUrl" type="text" class="textboxtext" maxlength="255" value="<?=$URL?>"/></td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_book'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table id="barcode" class="form_table_v30">
					<tbody>	
						<tr height='0px'>
							<td width="8%"></td>
							<td width="7%"></td>
							<td width="30%"></td>
							<td width="5%"></td>
							<td width="15%"></td>
							<td width="30%"></td>
							<td width="5%"></td>
						</tr>
							<? 
							if (isset($IsCopy) && $IsCopy)
							{
								
							} else
							{
								$Barcode = $libms->GET_BARCODE($Code);
								$counter=1;
								foreach($Barcode as $value)
								{
									echo "<tr class='record'>";
									echo "<td  class=\"field_title_short\"><span class=\"field_title\">".$Lang['libms']['book']['barcode']."<label class='barcode_number'>{$counter}</label></span></td>";
									echo "<td > ".$Lang['libms']['general']['auto']." : <input name=\"GenBarcode[{$value['UniqueID']}]\" type=\"checkbox\" class=\"gen_barcode\" value=\"{$value['UniqueID']}\"/></td>";
									echo "<td ><input name=\"BookBarcode[{$value['UniqueID']}]\" type=\"text\" class=\"textboxtext book_barcode\" maxlength=\"64\" value=\"".$value['BarCode']."\"/></td>";
									echo "<td  class='icon'>&nbsp; <input name=\"updateBarcode[{$value['UniqueID']}]\" type=\"hidden\"  value=\"{$value['UniqueID']}\"/></td>";
									echo "<td  class=\"field_title_short\"><span class=\"field_title\">".$Lang['libms']['book']['book_status']."</span></td>";
									echo "<td  class=\"unqiue_bk_status\">".$linterface->GET_SELECTION_BOX($BookStatus, " name='barcode_bookstatus[]' class='barcode_bookstatus' ", '', $value['RecordStatus'])."</td>";
									echo "<td><input type='button' class='btn_delete_Record' value='{$Lang['libms']['book']['delete']}' /></td>";
									echo "<td></td>";
									echo "</tr>";
									$counter++;
								}
							}
							?>
                        <tr>
							<td  class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['barcode'] ?><label class='barcode_number'></label></span></td>
                        	<td > <?=$Lang['libms']['general']['auto']?>: <input name="GenBarcode[]" type="checkbox" class="gen_barcode" value=""/></td>
                        	<td ><input name="BookBarcode<?=$BarCodeNewStr?>[]" type="text" class="textboxtext book_barcode" maxlength="64" /></td>
                        	<td  class='icon'>&nbsp;</td>
                        	<td  class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['book_status'] ?></span></td>
                         	<td><?= $linterface->GET_SELECTION_BOX($BookStatus, " style='display:none' name='barcode_bookstatus".$BarCodeNewStr."[]'  class='unqiue_bk_status barcode_bookstatus' ", '', 'NORMAL')?>
							</td>
							<td>
								<input type="button" class="btn_deleteRow" value="<?= $Lang['libms']['book']['delete'] ?>" style='display:none' />
								<input type="button" class="btn_addRow" value="<?= $Lang['libms']['book']['add'] ?>" />	
							</td>
						</tr>
                  	</tbody> 
				</table>
				
				<p></p>
								
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_circulation'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_copy'] ?></span></td>
                        	<td width="33%"><label id='lbl_BookNocopy' ><?=$NoOfCopy?></label><input id="BookNocopy" name="BookNocopy" type="hidden" class="textboxnum" maxlength="2" value="<?=$NoOfCopy?>" readonly/></td>
                        	<td width="4%" >&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_copy_available'] ?></span></td>
                         	<td width="33%"><label id='lbl_BookNoavailable' ><?=$NoOfCopyAvailable?></label><input id="BookNoavailable" name="BookNoavailable" type="hidden" class="textboxnum" maxlength="2" value="<?=$NoOfCopyAvailable?>" readonly/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['borrow_reserve'] ?></span></td>
                        	<td><input name="BookBorrow" type="checkbox" name="BookBorrow" value="1" <?if($OpenBorrow == "1") echo "checked"; ?> /> <label for="BookBorrow"><?=$Lang['libms']['book']['open_borrow']?></label> &nbsp; 
                        		<input type="checkbox" name="BookReserve" value="1" <?if($OpenReservation == "1") echo "checked"; ?>/> <label for="BookReserve"><?=$Lang['libms']['book']['open_reserve']?></label>
                        	</td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_to_user'] ?></span></td>
                         	<td><input name="BookUserremark" type="text" class="textboxtext" maxlength="32" value="<?=$RemarkToUser?>"/></td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
				
	<?=$linterface->MandatoryField();?>
				<center>
                    <p class="spacer"></p>
                    <?= $SaveBtn ?>
                    <?= $CancelBtn ?>
                </center>
					<p class="spacer"></p>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="userID" id="userID" value="<?=$userID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
</form><?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
