<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 


$PATH_WRT_ROOT = "../../../../";


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html('libms.html');

$libeLib = new elibrary();
$libelibinstall = new elibrary_install();

$result = $libms->SELECTFROMTABLE("LIBMS_LABEL_FORMAT",array('id','name'));

foreach ($result as $value)
	$Labels[] = array($value['name'] =>$value['id']);

$LabelFormatOption = $linterface->GET_SELECTION_BOX($result, " name='label_id' id='select_label_id' ",'','');

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageEBookLicense";

$libms->MODULE_AUTHENTICATION($CurrentPage);
//$TAGS_OBJ[] = array($Lang["libms"]["action"]["ebook_license"]);

$TAGS_OBJ = $libms->get_ebook_tabs("LICENSE");
//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_label'], "prepare_label.php", 1);
//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_labelformat'], "label_format.php", 0);
//
//
//$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_list'], "index.php");
//$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
# Start layout
if (!$plugin['eLib_Lite']){
	$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
}
$linterface->LAYOUT_START();

?>

<script type="text/javascript">
<?if ($plugin['eLib_Lite']){?>
	$( document ).ready(function(){
		$("#leftmenu").css("display","none");
		$("#content").css("padding-left","0px");
	});
<?}?>
</script>
<div class="table_board">

	<?= $libelibinstall->retrieve_elib_license_setting_html() ?>

</div>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
