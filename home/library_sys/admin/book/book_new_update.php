<?php
ini_set('memory_limit', '256M');
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 0);

# using: 

############ Change Log Start ###############
#
#	Date:	2017-02-08 	Cameron
#			- fix bug: 	not need to apply mysql_real_escape_string() for php5.4+
#
#	Date:	2016-03-02  Cameron
#			- apply recursive_trim to $_POST [case #F93085] 		
#
#	Date:	2015-04-15	Cameron
#			- redirect to current edit page after save record 
#
#	Date:	2013-09-12	Ivan
#			- group the logic as function NEW_BOOK_RECORD() in liblibrarymgmt.php
#
#	Date:	2013-08-12	Jason
#			- comment some info as they no longer exist in Book page and move to item page such as ACNO, Purchase info, Barcode info 
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
recursive_trim($_POST);
####### END  OF   Trim all request #######
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libimage.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//include($PATH_WRT_ROOT."includes/Upload.class.php");
//$upload = new Upload;


intranet_auth();
intranet_opendb();


//## Init
$BookNoavailable = 0;
$BookNocopy = 0;



# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

//dump ($_POST);
//dump ($_File);


if(sizeof($_POST)==0) {
	header("Location: item_list.php?xmsg=AddUnsuccess");
	exit;
}

//////////////////////update cover image////////////////////
///*if (!empty($BookCover)) {
//	
//	$count = $libms->SELECTVALUE('LIBMS_BOOK', 'max(`BookID`) as max', 'max');
//	$count = (int) ($count[0][max]/2000);
//		$upload->uploadFile("/file/lms/cover/".$count."/", 'latin', 100,date('U'));
//	//dump($upload->_files);
//	if (isset($upload->_files['BookCover'])){
//		// print "Upload Completed: ".   $upload->_files['file1'] . "<br>Check result<br>";
//		$cover_image =  "/file/lms/cover/".$count."/".$upload->_files['BookCover'];
//	}else{
//		//no Image/
//		$cover_image ='';
//	}
//
//}*/
//////////////////////end cover image////////////////////
///*
////dump($BookCover); 
////dump($upload->_files['BookCover']); 
////dump ($_POST);
////dump ($_FILES);*/
/////upload new ////////////////////////////////////////
//if (!empty($BookCover)) {
//
//$count = $libms->SELECTVALUE('LIBMS_BOOK', 'max(`BookID`) as max', 'max');
//# fixed by increasing by 1
//$book_id_predicted = $count[0][max]+1;
//$count = (int) ($count[0][max]/2000);
//require('import/api/class.upload.php');
//	$handle = new Upload($_FILES['BookCover']);
//	if ($handle->uploaded) {
//		//$handle->Process($intranet_root."/home/library_sys/admin/book/import/tmp/");
//		//$handle->Process("./import/pear/file/lms_book_import/");
//		/*
//		if (!(is_dir($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count))){
//			mkdir($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count, 0775, true);
//			}
//		$handle->Process($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count."/");
//		*/
//		$cover_folder = $intranet_root."/file/lms/cover/".$count."/".$book_id_predicted;
//		$handle->Process($cover_folder);
//
//		if ($handle->processed) {
//			// everything was fine !
//			//$handle->file_dst_name . '">' . $handle->file_dst_name . '</a>';
//			$uploadSuccess = true;
//			//$cover_image = str_replace("../../../../../intranetdata", "", $handle->file_dst_pathname);
//			$image_obj = new SimpleImage();
//			$image_obj->load($handle->file_dst_pathname);
//			$image_obj->resizeToMax(140, 200);
//			$image_obj->save($handle->file_dst_pathname);
//			$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
//		} else {
//			// one error occured
//			$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];
//			$uploadSuccess = false;
//			echo $xmsg;
//		}
//
//		// we delete the temporary files
//		$handle-> Clean();
//	}
//
//}
//
////upload new end///////////////////////////////////////
///*
//$dataAry['BookCode'] = str_replace('\\"','',$BookCode);
//$dataAry['BookCode'] = str_replace('"','',$dataAry['BookCode']);
//
//preg_match('/^([a-zA-Z]+)(\d+)$/',$dataAry['BookCode'],$matches);
//if (!empty($matches)){
//	$prefix = $matches[1];
//	$number = $matches[2];
//	
//	$sql = "SELECT `Key`, `Next_Number` FROM  `LIBMS_ACNO_LOG` WHERE `Key` LIKE '{$prefix}' AND  `Next_Number` > '{$number}' ";
//	$result = $libms->returnArray($sql);
//	if (empty($result)){
//		$nextnum = $number+1;
//		$sql = "INSERT INTO `LIBMS_ACNO_LOG` (`Key`, `Next_Number`) VALUES ('{$prefix}','{$nextnum}') ON DUPLICATE KEY UPDATE `Next_Number`= '{$nextnum}'";
//		//$sql = "UPDATE `LIBMS_ACNO_LOG` SET `Next_Number`= '{$number}'  WHERE `Key` LIKE '{$prefix}' LIMIT 1";
//		$libms->db_db_query($sql);
// 
//	}
//	$dataAry['ACNO_Prefix'] = mysql_real_escape_string(htmlspecialchars($prefix));
//	$dataAry['ACNO_Num'] = mysql_real_escape_string(htmlspecialchars($number));
//	
//}else{
//	intranet_closedb();
//	header("Location: index.php?xmsg=update_failed");
//	die();
//}
//*/
//

function get_libms_element_code($ElementType, $Code='', $DescEn='', $DescChi=''){
	global $libms, $is_debug;

	switch($ElementType){
		case 'Circulation':
			$table_name = 'LIBMS_CIRCULATION_TYPE';
			$field_name = 'CirculationTypeCode';
			break;
		case 'Resources':
			$table_name = 'LIBMS_RESOURCES_TYPE';
			$field_name = 'ResourcesTypeCode';
			break;
		case 'BookCategory':
			$table_name = 'LIBMS_BOOK_CATEGORY';
			$field_name = 'BookCategoryCode';
			break;
		case 'Location':
			$table_name = 'LIBMS_LOCATION';
			$field_name = 'LocationCode';
			break;
		case 'Responsibility':
			$table_name = 'LIBMS_RESPONSIBILITY';
			$field_name = 'ResponsibilityCode';
			break;
		case 'Language':
			$table_name = 'LIBMS_BOOK_LANGUAGE';
			$field_name = 'BookLanguageCode';
			break;
		default: return ''; break;
	}
	$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL($Code) ;
	if ($Code != ''){
		$result1 = $libms->returnArray($sql);
		if (sizeof($result1)<1)
		{
			$DescEn = ($DescEn == '') ? $Code : $DescEn;
			$DescChi = ($DescChi == '') ? $Code : $DescChi;
			$sql = "INSERT INTO ".$table_name." (".$field_name.", DescriptionEn, DescriptionChi, DateModified, LastModifiedBy) " .
					" VALUES ('".addslashes($Code)."', '".addslashes($DescEn)."', '".addslashes($DescChi)."', now(), 0) ";
			if($is_debug){
				echo $sql.'<br>';
			} else {
				$libms->db_db_query($sql);
			}
			return $Code;
		} else
		{
			return $result1[0][$field_name];
		}
	}
	return "";
}


$BookLang = get_libms_element_code('Language', $BookLang);
$BookResources = get_libms_element_code('Resources', $BookResources);

//////////////////////start insert table "LIBMS_htmlspecialcharsBOOK"////////////////////
if (get_magic_quotes_gpc()) {
	$dataAry['CallNum'] = mysql_real_escape_string(htmlspecialchars($BookCallno));
	$dataAry['CallNum2'] = mysql_real_escape_string(htmlspecialchars($BookCallno2));
	$dataAry['ISBN'] = mysql_real_escape_string(htmlspecialchars($BookISBN));
	$dataAry['ISBN2'] = mysql_real_escape_string(htmlspecialchars($BookISBN2));
	$dataAry['NoOfCopy'] = mysql_real_escape_string(htmlspecialchars($BookNocopy));
	$dataAry['NoOfCopyAvailable'] = mysql_real_escape_string(htmlspecialchars($BookNoavailable));
	$dataAry['BookTitle'] = mysql_real_escape_string(htmlspecialchars($BookTitle));
	$dataAry['BookSubTitle'] = mysql_real_escape_string(htmlspecialchars($BookSubtitle));
	$dataAry['Introduction'] = mysql_real_escape_string(htmlspecialchars($BookIntro));
	$dataAry['Language'] = mysql_real_escape_string(htmlspecialchars($BookLang));
	$dataAry['Country'] = mysql_real_escape_string(htmlspecialchars($BookCountry));
	$dataAry['Edition'] = mysql_real_escape_string(htmlspecialchars($BookEdition));
	$dataAry['PublishPlace'] = mysql_real_escape_string(htmlspecialchars($BookPublishplace));
	$dataAry['Publisher'] = mysql_real_escape_string(htmlspecialchars($BookPublisher));
	$dataAry['PublishYear'] = mysql_real_escape_string(htmlspecialchars($BookPublishyear));
	$dataAry['NoOfPage'] = mysql_real_escape_string(htmlspecialchars($BookNopage));
	$dataAry['RemarkInternal'] = mysql_real_escape_string(htmlspecialchars($BookInternalremark));
	$dataAry['RemarkToUser'] = mysql_real_escape_string(htmlspecialchars($BookUserremark));
	//$dataAry['PurchaseDate'] = mysql_real_escape_string(htmlspecialchars($BookPurchasedate));
	//$dataAry['Distributor'] = mysql_real_escape_string(htmlspecialchars($BookDistributor));
	//$dataAry['PurchaseNote'] = mysql_real_escape_string(htmlspecialchars($BookPurchasenote));
	//$dataAry['PurchaseByDepartment'] = mysql_real_escape_string(htmlspecialchars($BookPurchasedept));
	//$dataAry['ListPrice'] = mysql_real_escape_string(htmlspecialchars($BookListprice));
	//$dataAry['Discount'] = mysql_real_escape_string(htmlspecialchars($BookDiscount));
	//$dataAry['PurchasePrice'] = mysql_real_escape_string(htmlspecialchars($BookPurchaseprice));
	//$dataAry['AccountDate'] = mysql_real_escape_string(htmlspecialchars($BookAccdate));
	$dataAry['CirculationTypeCode'] = mysql_real_escape_string(htmlspecialchars($BookCirclation));
	$dataAry['ResourcesTypeCode'] = mysql_real_escape_string(htmlspecialchars($BookResources));
	$dataAry['BookCategoryCode'] = mysql_real_escape_string(htmlspecialchars($BookCategory));
	$dataAry['LocationCode'] = mysql_real_escape_string(htmlspecialchars($BookLocation));
	$dataAry['Subject'] = mysql_real_escape_string(htmlspecialchars($BookSubj));
	$dataAry['Series'] = mysql_real_escape_string(htmlspecialchars($BookSeries));
	$dataAry['SeriesNum'] = mysql_real_escape_string(htmlspecialchars($BookSeriesno));
	$dataAry['URL'] = mysql_real_escape_string(htmlspecialchars($BookUrl));
	$dataAry['ResponsibilityCode1'] = mysql_real_escape_string(htmlspecialchars($ResponsibilityCodeOption1));
	$dataAry['ResponsibilityBy1'] = mysql_real_escape_string(htmlspecialchars($BookResponb1));
	$dataAry['ResponsibilityCode2'] = mysql_real_escape_string(htmlspecialchars($ResponsibilityCodeOption2));
	$dataAry['ResponsibilityBy2'] = mysql_real_escape_string(htmlspecialchars($BookResponb2));
	$dataAry['ResponsibilityCode3'] = mysql_real_escape_string(htmlspecialchars($ResponsibilityCodeOption3));
	$dataAry['ResponsibilityBy3'] = mysql_real_escape_string(htmlspecialchars($BookResponb3));
	$dataAry['OpenBorrow'] = mysql_real_escape_string(htmlspecialchars($BookBorrow));
	$dataAry['OpenReservation'] = mysql_real_escape_string(htmlspecialchars($BookReserve));
	$dataAry['RecordStatus'] = mysql_real_escape_string(htmlspecialchars($BookStatus));
	$dataAry['CoverImage'] = $cover_image;
	//$dataAry['eClassBookCode'] = mysql_real_escape_string(htmlspecialchars($DescriptionChi));
	
	if ($IsPeriodical) {
		$dataAry['IsPeriodical'] = mysql_real_escape_string(htmlspecialchars($IsPeriodical));
		$dataAry['PeriodicalCode'] = mysql_real_escape_string(htmlspecialchars($PeriodicalCode));
		$dataAry['ISSN'] = mysql_real_escape_string(htmlspecialchars($ISSN));
		$dataAry['PurchaseByDepartment'] = mysql_real_escape_string(htmlspecialchars($BookPurchasedept));
	}

}
else {
	$dataAry['CallNum'] = htmlspecialchars($BookCallno);
	$dataAry['CallNum2'] = htmlspecialchars($BookCallno2);
	$dataAry['ISBN'] = htmlspecialchars($BookISBN);
	$dataAry['ISBN2'] = htmlspecialchars($BookISBN2);
	$dataAry['NoOfCopy'] = htmlspecialchars($BookNocopy);
	$dataAry['NoOfCopyAvailable'] = htmlspecialchars($BookNoavailable);
	$dataAry['BookTitle'] = htmlspecialchars($BookTitle);
	$dataAry['BookSubTitle'] = htmlspecialchars($BookSubtitle);
	$dataAry['Introduction'] = htmlspecialchars($BookIntro);
	$dataAry['Language'] = htmlspecialchars($BookLang);
	$dataAry['Country'] = htmlspecialchars($BookCountry);
	$dataAry['Edition'] = htmlspecialchars($BookEdition);
	$dataAry['PublishPlace'] = htmlspecialchars($BookPublishplace);
	$dataAry['Publisher'] = htmlspecialchars($BookPublisher);
	$dataAry['PublishYear'] = htmlspecialchars($BookPublishyear);
	$dataAry['NoOfPage'] = htmlspecialchars($BookNopage);
	$dataAry['RemarkInternal'] = htmlspecialchars($BookInternalremark);
	$dataAry['RemarkToUser'] = htmlspecialchars($BookUserremark);
	//$dataAry['PurchaseDate'] = htmlspecialchars($BookPurchasedate);
	//$dataAry['Distributor'] = htmlspecialchars($BookDistributor);
	//$dataAry['PurchaseNote'] = htmlspecialchars($BookPurchasenote);
	//$dataAry['PurchaseByDepartment'] = htmlspecialchars($BookPurchasedept);
	//$dataAry['ListPrice'] = htmlspecialchars($BookListprice);
	//$dataAry['Discount'] = htmlspecialchars($BookDiscount);
	//$dataAry['PurchasePrice'] = htmlspecialchars($BookPurchaseprice);
	//$dataAry['AccountDate'] = htmlspecialchars($BookAccdate);
	$dataAry['CirculationTypeCode'] = htmlspecialchars($BookCirclation);
	$dataAry['ResourcesTypeCode'] = htmlspecialchars($BookResources);
	$dataAry['BookCategoryCode'] = htmlspecialchars($BookCategory);
	$dataAry['LocationCode'] = htmlspecialchars($BookLocation);
	$dataAry['Subject'] = htmlspecialchars($BookSubj);
	$dataAry['Series'] = htmlspecialchars($BookSeries);
	$dataAry['SeriesNum'] = htmlspecialchars($BookSeriesno);
	$dataAry['URL'] = htmlspecialchars($BookUrl);
	$dataAry['ResponsibilityCode1'] = htmlspecialchars($ResponsibilityCodeOption1);
	$dataAry['ResponsibilityBy1'] = htmlspecialchars($BookResponb1);
	$dataAry['ResponsibilityCode2'] = htmlspecialchars($ResponsibilityCodeOption2);
	$dataAry['ResponsibilityBy2'] = htmlspecialchars($BookResponb2);
	$dataAry['ResponsibilityCode3'] = htmlspecialchars($ResponsibilityCodeOption3);
	$dataAry['ResponsibilityBy3'] = htmlspecialchars($BookResponb3);
	$dataAry['OpenBorrow'] = htmlspecialchars($BookBorrow);
	$dataAry['OpenReservation'] = htmlspecialchars($BookReserve);
	$dataAry['RecordStatus'] = htmlspecialchars($BookStatus);
	$dataAry['CoverImage'] = $cover_image;
	//$dataAry['eClassBookCode'] = htmlspecialchars($DescriptionChi);
	
	if ($IsPeriodical) {
		$dataAry['IsPeriodical'] = htmlspecialchars($IsPeriodical);
		$dataAry['PeriodicalCode'] = htmlspecialchars($PeriodicalCode);
		$dataAry['ISSN'] = htmlspecialchars($ISSN);
		$dataAry['PurchaseByDepartment'] = htmlspecialchars($BookPurchasedept);
	}
	
}
$dataAry['Dimension'] = htmlspecialchars($BookDimension);
$dataAry['ILL'] = htmlspecialchars($BookILL);
//
////dex($dataAry);
//
//foreach ($dataAry as $key => $feild){
//	if ( $feild == '' ){
//		unset ($dataAry[$key]);
//	}
//}
//
//
//$libms->INSERT2DB($dataAry, 'LIBMS_BOOK');
////tolog (mysql_error());
//$book_id = mysql_insert_id();
////tolog ("book_new: ".$book_id);
//
//////////////////////end insert table "LIBMS_BOOK"////////////////////
//
//
//////////////////////start insert table "LIBMS_BOOK_TAG"////////////////////
//// function trim_1(&$tmp_array){
//// 	$tmp_array   =  str_replace("\"", "" ,$tmp_array); 
//// }
//// tolog($BookTag);
//// //$TagArray = mysql_real_escape_string(htmlspecialchars($BookTag));
//// $TagArray = explode(",", trim($BookTag,"[]"));
//// array_walk($TagArray, 'trim_1');
//
//if (!$IsPeriodical) {
//	$TagArray =  json_decode ($BookTag);
//	
//	
//	foreach($TagArray as &$field)
//	{
//		$field = mysql_real_escape_string(stripcslashes($field));
//		$tag_id = $libms->SELECTVALUE("LIBMS_TAG", "TagID", "TagID", "TagName = '".$field."'");
//		if (empty($tag_id))	{
//			$newTag['TagName'] = $field;
//			$libms->INSERT2DB($newTag, 'LIBMS_TAG');
//			$tag_id = $libms->SELECTVALUE("LIBMS_TAG", "TagID", "TagID", "TagName = '".$field."'");
//		}
//		$newBookTag['BookID'] = $book_id;
//		$newBookTag['TagID'] = $tag_id[0]['TagID'];
//		$libms->INSERT2DB($newBookTag, 'LIBMS_BOOK_TAG');
//	}
//}
//
//////////////////////end insert table "LIBMS_BOOK_TAG"////////////////////
//
//
//
//////////////////////start insert table "LIBMS_BOOK_UNIQUE"////////////////////
///*
//$b = 0;
//foreach($BookBarcode as $key => $field)
//{
//	if(!empty($field)){
//		if('***AUTO GEN***' != $field){
//			$bookUniqueAry['BarCode'] = PHPTOSQL($field);
//		}else{
//			$bookUniqueAry['BarCode'] = PHPToSQL(rand().time());
//		}
//		
//		$bookUniqueAry['BookID'] = $book_id;
//		$bookUniqueAry['RecordStatus'] = PHPTOSQL($barcode_bookstatus[$b]);
//		$bookUniqueAry['CreationDate'] = 'now()';
//		
//		$libms->INSERT2TABLE('LIBMS_BOOK_UNIQUE', $bookUniqueAry);
//		
//		if('***AUTO GEN***' == $field){
//			$uniqueID = mysql_insert_id();
//			$barcode = $uniqueID +1000000;
//			$libms->UPDATE2TABLE('LIBMS_BOOK_UNIQUE',array('BarCode' => PHPToSQL("A+{$barcode}")),array('UniqueID' =>$uniqueID));
//		}
//	}
//	$b++;
//}
//*/
//
// 
//$libel = new elibrary();
//$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($book_id);
//////////////////////end insert table "LIBMS_BOOK_UNIQUE"////////////////////
//
///*
////////////////////////correct the no copy of book /////////////////////////////
//$sql=<<<EOF
//UPDATE LIBMS_BOOK b SET  `NoOfCopyAvailable` = ( 
//	SELECT COUNT( * ) 
//	FROM  `LIBMS_BOOK_UNIQUE` bu
//	WHERE  `RecordStatus` 
//	IN (
//		'NORMAL',  'SHELVING'
//	)
//	AND b.`BookID` = bu.`BookID` 
//	GROUP BY bu.`BookID` 
//) ,  `NoOfCopy` = ( 
//	SELECT COUNT( * ) 
//	FROM  `LIBMS_BOOK_UNIQUE` bu
//	WHERE b.`BookID` = bu.`BookID` 
//	AND  `RecordStatus` NOT 
//	IN (
//		'LOST',  'WRITEOFF',  'SUSPECTEDMISSING'
//	)
//	GROUP BY bu.`BookID`
//)
//WHERE BookID =  '{$book_id}'
//EOF;
//
//$libms->db_db_query($sql);
//*/
//////////////////////end correct the no copy of book /////////////////////////////

$book_id = $libms->NEW_BOOK_RECORD($dataAry, $BookCover, $BookTag);
$xmsg = ($book_id > 0)? 'AddSuccess' : 'AddUnsuccess';

intranet_closedb();

if($setItem == 1){
	//header("Location: item_edit.php?BookID=".$book_id."&xmsg=AddSuccess");
	header("Location: item_edit.php?BookID=".$book_id."&xmsg=".$xmsg);
} else {
	//header("Location: index.php?xmsg=AddSuccess");
//	header("Location: index.php?xmsg=".$xmsg);
	header("Location: book_edit.php?Code=$book_id&xmsg=".$xmsg);
}
?>