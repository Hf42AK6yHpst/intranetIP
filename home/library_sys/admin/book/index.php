<?php
// using:  

/************************************
 * 
 *  # important!!!!
 *  filters, keyword are applied to book_export.php; if you change them or add more, please also update book_export.php
 * 
 * 	Known Issue: ACNO is regarded to contain word characters only (no punctuation characters except underscore), therefore, keyword search 
 * 		won't cover following methods if there's punctuation characters in ACNO 
 * 		(1) Exact ACNO search with double quote / single quote embraced. e.g. "20150930&03"
 * 		(2) Range ACNO search separated by ~. e.g. 20150930&02~20150930&03
 * 		(3) Max ACNO search by prefix ^ e.g. 20150930&^
 * 		However, keyword search without double quote / single quote embraced still work. e.g. 20150930&03
 * 
 *  # important!!!!
 *  
 * Date:    2020-04-20 (Cameron)
 *          - add advanced search fields: bookSeries and bookSeriesno [case #Z183643] 
 *          
 * Date:    2019-12-11 (Tommy)
 *          fix: some book record does not output book names
 *          - record does not have callnumber will be null in book name columns -> delete callNumber and callNumber2 in select sql (onMouseOver, id, name)
 *  
 * Date:    2019-12-10 (Tommy)
 *          - added only show records have CoverImage function
 *          
 * Date:    2019-10-04 (Tommy)
 *          - change get book sql to get coverimage path in database for outputing coverimage in table
 *          
 * Date:	2019-07-23 (Henry)
 *			- use the local library of jquery
 *
 * Date:	2017-12-06 (Cameron)
 *			- allow keyword search to get result of any combination of single word in the field [case #F132432]
 *			apply to following fields only: Publisher, BookTitle, BookSubTitle, ResponsibilityBy1~3, Series
 *
 * Date:	2017-08-04 (Cameron)
 * 			- should filter out deleted book unique record (in left join condition) [case #N121394]
 *
 * Date:	2017-03-30 (Cameron)
 * 			- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 				by stripslashes($keyword)
 *			- fix bug in $SubjectSelect, $PublisherSelect & $LanguageSelect in sql
 *
 * Date:	2016-12-28 (Cameron)
 * 			- add fields selection page for export / export (exclude write-off books) function [Email Req on 2016-09-22]
 * 
 * Date:	2016-11-11 (Cameron)
 *			- add group right "portal settings" under admin section [case #Z108442]
 *
 * Date:	2016-09-30 (Cameron)
 * 			- add export books without write-off button [case #F98576]
 *
 * Date:	2016-07-05 (Cameron)
 * 			- show description rather than code for Language option list $languageSelectionOption [case #Z97773]
 *
 * Date:	2016-04-26 (Cameron) 
 * 			- break admin access right from "stock-take and write-off" into "stock-take" and "write-off" [Case#K94933]
 * 			- add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 * 
 * Date:	2016-04-14 (Cameron)
 * 			- pass DirectMsg to LAYOUT_START()
 * 
 * Date:	2016-02-15 (Henry)
 * 			- add 'NA' to book category filter
 * 
 * Date:	2015-10-05 (Cameron)
 * 			- trim advance search fields before pass it to sql
 * 			- fix bug on basic search / advance search for string like A&<>'"\B, support comparing unconverted string A&<>'"\B ( created by imoport)
 * 				and converted string A&amp;'&quot;&lt;&gt;\B (created by add / edit)
 *
 * Date:	2015-09-30 (Cameron)
 *			- fix bug on option list filter for Subject, Publisher and Language so that they support item containing special characters: '"<>&\ 
 *
 * Date:	2015-09-29 (Cameron)
 * 			- fix bug on keyword search / advance search to support following characters: '"<>&\
 * 			- advance search: ACNO does not contain double quote, and it's stored in db for original characters like: '<>&\
 *
 * Date:	2015-07-31 (Cameron)
 * 			- remove restriction on BookCategoryType=1 when filter BookCategory record which Language is not set
 * 			- fix javascript checking in function jsExportBooks() and jsExportBooksMarc21()
 *
 * Date:	2015-07-30 (Cameron)
 *			- fix bug of unable to filter BookCategoryCode that contains tab or space in get_xxxConditions_SQL()
 *
 * Date:	2015-07-27 (Cameron)
 * 			- modify get_xxxFilter_html() and get_xxxConditions_SQL() to accept '0' as selected value  
 *
 * Date:	2015-04-01 (Cameron)
 * Details:	- support navigation of records ( prev, next, sorting, filter by book category etc.) after advance search
 * 				( add hidden field advanceSearch)
 * 			- reset to basic search if press enter in keyword field (advanceSearch=0)
 * 			- keep all filter condition, basic search / advance search condition, page navigation condition after return from 
 * 				inner linking page (e.g. Book title, Available/Number of copies)
 *   
 * Date:	2015-03-25 (Cameron)
 * Details:	add argument advanceSearch to jsExportBooks() and jsExportBooksMarc21()		
 * 
 * Date:	2015-01-30 (Henry)
 * 			modified get_BookCatFilter_html()
 * 
 * Date:	2014-12-02 (Cameron)
 * Details: add BookSubTitle (Book title 2) under BookTitle in list view
 * 			add search max(ACNO) by pattern of {prefix}^ in keywords field
 * 
 * Date:	2014-10-22 (Yuen) #ip.2.5.5.10.1
 * Details: default sorting by book title (third column) 
 * 
 * Date:	2014-09-30 (Henry)
 * Details: add advance search button #ip.2.5.5.10.1
 * 
 * Date:	2014-09-15 (Henry)
 * Details: add autocomplete at keywords field
 * 
 * Date:	2014-01-14 (Henry)
 * Details: add Language filter
 * 
 * Date:	2013-11-15 (Yuen)
 * Details: implemented EXPORT function
 * 
 * Date:	2013-10-21 (Yuen)
 * Details: display call numbers 1 & 2 and also being searchable
 * 
 * Date:	2013-09-27 (Yuen)
 * Details: indicate the Loan setting in item status using * (if the book is not allow for loaning)
 * 
 * Date:	2013-08-21 (Henry)
 * Details: add Publisher filter
 * 
 * Date:	2013-08-13 (Jason)
 * Details: review the whole page, follow IP25 standard, fix the cookies problem
 * 			add thickbox link for no. of copy column
 * 
 * Date:	2013-05-15 (Rita)
 * Details:	add Location filter
 ************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

### set cookies
//if ($page_size_change == 1)
//{
//    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
//    $ck_page_size = $numPerPage;
//}
//
//# preserve table view
//if ($ck_right_page_number!=$pageNo && $pageNo!="")
//{
//	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
//	$ck_right_page_number = $pageNo;
//} else if (!isset($pageNo) && $ck_right_page_number!="")
//{
//	$pageNo = $ck_right_page_number;
//}
//
//if ($ck_right_page_order!=$order && $order!="")
//{
//	setcookie("ck_right_page_order", $order, 0, "", "", 0);
//	$ck_right_page_order = $order;
//} else if (!isset($order) && $ck_right_page_order!="")
//{
//	//$order = $ck_right_page_order;
//	$order = 1;		# default in ascending order
//}
//
//if ($ck_right_page_field!=$field && $field!="")
//{
//	setcookie("ck_right_page_field", $field, 0, "", "", 0);
//	$ck_right_page_field = $field;
//} else if (!isset($field) && $ck_right_page_field!="")
//{
//	$field = $ck_right_page_field;
//}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();

### Set Cookies
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_book_page_field", "field");							// navigation var start
$arrCookies[] = array("elibrary_plus_mgmt_book_page_order", "order");
$arrCookies[] = array("elibrary_plus_mgmt_book_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_mgmt_book_page_number", "pageNo");							// navigation var end
//$arrCookies[] = array("elibrary_plus_mgmt_book_location_code", "LocationCode");
$arrCookies[] = array("elibrary_plus_mgmt_book_keyword", "keyword");
$arrCookies[] = array("elibrary_plus_mgmt_book_circulation_type_code", "CirculationTypeCode");	// option list filter start
$arrCookies[] = array("elibrary_plus_mgmt_book_resources_type_code", "ResourcesTypeCode");
$arrCookies[] = array("elibrary_plus_mgmt_book_category_code", "BookCategoryCode");
$arrCookies[] = array("elibrary_plus_mgmt_book_subject_select", "SubjectSelect");
$arrCookies[] = array("elibrary_plus_mgmt_book_publisher_select", "PublisherSelect");
$arrCookies[] = array("elibrary_plus_mgmt_book_language_select", "LanguageSelect");				// option list filter end
$arrCookies[] = array("elibrary_plus_mgmt_book_advanceSearch", "advanceSearch");				// advance search start
$arrCookies[] = array("elibrary_plus_mgmt_book_ACNO", "ACNO");
$arrCookies[] = array("elibrary_plus_mgmt_book_bookTitle", "bookTitle");
$arrCookies[] = array("elibrary_plus_mgmt_book_author", "author");
$arrCookies[] = array("elibrary_plus_mgmt_book_callNumber", "callNumber");
$arrCookies[] = array("elibrary_plus_mgmt_book_ISBN", "ISBN");
$arrCookies[] = array("elibrary_plus_mgmt_book_publisher", "publisher");
$arrCookies[] = array("elibrary_plus_mgmt_book_bookSeries", "bookSeries");
$arrCookies[] = array("elibrary_plus_mgmt_book_bookSeriesno", "bookSeriesno");                  // advance search end
$arrCookies[] = array("elibrary_plus_mgmt_book_cover", "bookCover");
$arrCookies[] = array("elibrary_plus_mgmt_book_cover_temp", "bookCover_temp");

//$arrCookies[] = array("elibrary_plus_mgmt_book_record_status", "BookStatus");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}

//if(isset($LocationCode)){
//	$LocationCode = $elibrary_plus_mgmt_book_location_code;
//}

## Get Data
$keyword = (isset($keyword) && $keyword != '') ? trim($keyword) : '';
if (!get_magic_quotes_gpc()) {
	$keyword 	    = stripslashes($keyword);
	$ACNO		    = stripslashes($ACNO);
	$bookTitle	    = stripslashes($bookTitle);
	$author		    = stripslashes($author);
	$callNumber	    = stripslashes($callNumber);
	$ISBN		    = stripslashes($ISBN);
	$publisher	    = stripslashes($publisher);
	$bookSeries	    = stripslashes($bookSeries);
	$bookSeriesno   = stripslashes($bookSeriesno);
	
	$SubjectSelect 		= stripslashes($SubjectSelect);
	$PublisherSelect 	= stripslashes($PublisherSelect);
	$LanguageSelect 	= stripslashes($LanguageSelect);
	$bookCover          = stripslashes($bookCover);
}

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];


if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	
	if ($_SESSION['LIBMS']['admin']['current_right']['admin']['group management'])
	{
		$GoTo = "admin/group/index.php";
	} elseif ($_SESSION['LIBMS']['admin']['current_right']['admin']['stock-take'])
	{
		$GoTo = "admin/stocktake/index.php";
	} elseif ($_SESSION['LIBMS']['admin']['current_right']['admin']['write-off'])
	{
		$GoTo = "admin/stocktake/write_off.php";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['reports']['book report'])
	{
		$GoTo = "reports/?page=BookReport";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['reports']['loan report'])
	{
		$GoTo = "reports/?page=Circulation";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['reports']['overdue report'])
	{
		$GoTo = "reports/?page=Overdue";
		// reports/fine_report.php
	} elseif($_SESSION['LIBMS']['admin']['current_right']['reports']['lost report'])
	{
		$GoTo = "admin/book/lost_book.php";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['reports']['reader ranking'])
	{
		$GoTo = "reports/?page=ReaderRanking";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['reports']['inactive reader'])
	{
		$GoTo = "reports/?page=ReaderInactive";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['reports']['book ranking'])
	{
		$GoTo = "reports/?page=BookRanking";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['reports']['stock-take report'])
	{
		$GoTo = "reports/stocktake_report/index.php";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['statistics']['summary'])
	{
		$GoTo = "reports/?page=Integrated";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['statistics']['general stats'])
	{
		$GoTo = "reports/?page=Lending";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['statistics']['category vs class or form'])
	{
		$GoTo = "reports/?page=CategoryReport";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['statistics']['frequency by form'])
	{
		//$GoTo = "reports/?page=CategoryReport";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['statistics']['penalty stats'])
	{
		$GoTo = "reports/?page=PenaltyReport";
	} elseif($_SESSION['LIBMS']['admin']['current_right']['admin']['settings'])
	{
		$GoTo = "admin/settings/system_settings.php";
	}elseif($_SESSION['LIBMS']['admin']['current_right']['admin']['portal settings'])
	{
		$GoTo = "home/eLearning/elibplus2/portal_settings_edit.php";
		header("location: ".$PATH_WRT_ROOT.$GoTo);
		exit;
	}
	
	if ($GoTo!="")
	{
		header("location: ".$PATH_WRT_ROOT."home/library_sys/".$GoTo);
		exit;
	}
	
	# normal access right denied!
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementBookList";

$TAGS_OBJ[] = array($Lang['libms']['action']['book']);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);

//$linterface = new interface_html("libms.html");
$linterface = new interface_html();


## Location Code Sql Cond
/*
if($LocationCode != ''){
	if($LocationCode =='NA'){
		$locationCode_cond = " AND bu.LocationCode = '' " ;
	}else{
		$locationCode_cond = " AND bu.LocationCode = '".$LocationCode."' " ;
	}
}
*/
$locationCode_cond = '';

## SubjectSelect Sql Cond
if($SubjectSelect != ''){
	if($SubjectSelect =='NA'){
		$subjectSelect_cond = " AND (Subject = '' OR Subject is null)" ;
	}else{
		$subjectSelect_cond = " AND (Subject = '".addslashes(htmlspecialchars($SubjectSelect))."' or Subject = '".addslashes($SubjectSelect)."') " ;
	}
}


## PublisherSelect Sql Cond
if($PublisherSelect != ''){
	if($PublisherSelect =='NA'){
		$publisherSelect_cond = " AND (Publisher = '' OR Publisher is null)" ;
	}else{
		$publisherSelect_cond = " AND (Publisher = '".addslashes(htmlspecialchars($PublisherSelect))."' or Publisher = '".addslashes($PublisherSelect)."') " ;
	}
}

//Henry sdded 20140114
## LanguageSelect Sql Cond
$languageSelect_cond = '';
if($LanguageSelect != ''){
	if($LanguageSelect =='NA'){
		$languageSelect_cond = " AND (Language = '' OR Language is null)" ;
	}else{
		$languageSelect_cond = " AND (Language = '".addslashes(htmlspecialchars($LanguageSelect))."' or Language = '".addslashes($LanguageSelect)."') " ;
	}
}

$bookCover_cond = '';
$bookCover = $bookCover_temp;
if($bookCover != 0 && $bookCover != ''){
    $bookCover_cond = " AND b.CoverImage != '' ";
}else{
    $bookCover_cond = "";
}

############################################################################################################
///*
if (isset($elibrary_plus_mgmt_book_page_size) && $elibrary_plus_mgmt_book_page_size != "") $page_size = $elibrary_plus_mgmt_book_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 2 : $field;

$conds='';

if(!$advanceSearch){
	if($keyword!="") {
		$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));		// A&<>'"\B ==> A&<>\'\"\\\\B
		$converted_keyword_sql = special_sql_str($keyword);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
	}

	$keyword_sql = $unconverted_keyword_sql;
	preg_match('/^(\s*)((\w)+)(\s*)~(\s*)((\w)+)(\s*)$/',$keyword_sql,$matches_range);
	preg_match('/^(\s*)\\\"(\s*)((\w)+)(\s*)\\\"(\s*)$/',$keyword_sql,$matches_strict);
	preg_match('/^(\s*)((\w)+)(\^)(\s*)$/',$keyword_sql,$matches_max);
	if(!isset($matches_strict[3])||$matches_strict[3]==""){
		preg_match("/^(\s*)\\\'(\s*)((\w)+)(\s*)\\\'(\s*)$/",$keyword_sql,$matches_strict);
	}
	$acno_strict = $matches_strict[3];
	if($matches_range[0]!=""){
		$iACNO_start = trim($matches_range[2]);
        $iACNO_end = trim( $matches_range[6]);
        if ($iACNO_start<>"" || $iACNO_end<>"")
        {
//	        if (preg_match('/^([a-z]+)([0-9]+)?$/i',$iACNO_start,$matches)){
//			    $sACNO = $matches[1];
//			    $iACNO_number_start = $matches[2];
//			}
//    		if (preg_match('/^([a-z]+)([0-9]+)?$/i',$iACNO_end,$matches)){
//	    		$iACNO_number_end = $matches[2];
//    		}	
        	
		    $conditions[] ="bu.`RecordStatus` NOT LIKE 'DELETE'";
		    
	     	if ($iACNO_start<>"" && $iACNO_end<>"")
		    {
		     	$conditions[] = "bu.`ACNO` >= '{$iACNO_start}'";
		    	$conditions[] = "bu.`ACNO` <= '{$iACNO_end}'";
		    } elseif ($iACNO_start<>"")
		    {
		    	$conditions[] = "bu.`ACNO` = '{$iACNO_start}'";
		    } elseif ($iACNO_end<>"")
		    {
			    $conditions[] = "bu.`ACNO` = '{$iACNO_end}'";
		    }
//		    if (!empty($sDate_start)){
//			    $conditions[] = "bu.`PurchaseDate` >= '{$sDate_start}'";
//		    }
//    		if (!empty($sDate_end)){
//	    		$conditions[] = "bu.`PurchaseDate` <= '{$sDate_end}'";
//    		}
		    if (empty($conditions)){
			   	exit;
		    }
		    
		    $acno_filter = '(' . implode(') AND (', $conditions) . ')';
        }
	}
	elseif(isset($acno_strict)&&$acno_strict!=""){
		$acno_filter = "bu.ACNO = '$acno_strict'";
	}
	elseif($matches_max[0]!=""){
		if ($matches_max[2] != "") {
			$acno_filter = "bu.ACNO = (select max(ACNO) from `LIBMS_BOOK_UNIQUE` where ACNO LIKE '".$matches_max[2]."%' limit 1)";
		}
		else {
			exit;
		}
	} 
	else{
		if ($unconverted_keyword_sql == $converted_keyword_sql) {
			$acno_filter = "bu.ACNO LIKE '%$keyword_sql%'";
		}
		else {
			$acno_filter = "(bu.ACNO LIKE '%$unconverted_keyword_sql%' OR bu.ACNO LIKE '%$converted_keyword_sql%')";
		}
	}
	
	if ($unconverted_keyword_sql == $converted_keyword_sql) {		// keyword does not contain any special character: &<>"
		$words = getWords($unconverted_keyword_sql);
		$conds .=" AND ( ";
		$conds .= "bu.`BarCode` LIKE '%$unconverted_keyword_sql%' OR ";
		$conds .= "(b.`Publisher` like '%".implode("%' AND b.`Publisher` LIKE'%",$words)."%') OR ";
		$conds .= $acno_filter ? $acno_filter." OR " : "";
		$conds .= "`CallNum` LIKE '%$unconverted_keyword_sql%' OR ";
		$conds .= "`CallNum2` LIKE '%$unconverted_keyword_sql%' OR ";
		$conds .= "CONCAT(`CallNum`, ' ', `CallNum2`) LIKE '%$unconverted_keyword_sql%' OR ";
		$conds .= "`ISBN` LIKE '%$unconverted_keyword_sql%' OR ";
		$conds .= "`ISBN2` LIKE '%$unconverted_keyword_sql%' OR ";
		$conds .= "(BookTitle like '%".implode("%' AND BookTitle LIKE'%",$words)."%') OR ";
		$conds .= "(BookSubTitle like '%".implode("%' AND BookSubTitle LIKE'%",$words)."%') OR ";
		$conds .= "(ResponsibilityBy1 like '%".implode("%' AND ResponsibilityBy1 LIKE'%",$words)."%') OR ";
		$conds .= "(ResponsibilityBy2 like '%".implode("%' AND ResponsibilityBy2 LIKE'%",$words)."%') OR ";
		$conds .= "(ResponsibilityBy3 like '%".implode("%' AND ResponsibilityBy3 LIKE'%",$words)."%') OR ";
		$conds .= "(Series like '%".implode("%' AND Series LIKE'%",$words)."%')";
		$conds .= ")";
	} 
	
	else {	// keyword contains at least one of the special character: &<>'"\
		$words_1 = getWords($unconverted_keyword_sql);
		$words_2 = getWords($converted_keyword_sql);

		$conds .=" AND ( ";
		$conds .= "bu.`BarCode` LIKE '%$unconverted_keyword_sql%' OR bu.`BarCode` LIKE '%$converted_keyword_sql%' OR ";
		$conds .= "(b.`Publisher` like '%".implode("%' AND b.`Publisher` LIKE'%",$words_1)."%') OR (b.`Publisher` like '%".implode("%' AND b.`Publisher` LIKE'%",$words_2)."%') OR ";
		$conds .= $acno_filter ? $acno_filter." OR " : "";
		$conds .= "`CallNum` LIKE '%$unconverted_keyword_sql%' OR `CallNum` LIKE '%$converted_keyword_sql%' OR ";
		$conds .= "`CallNum2` LIKE '%$unconverted_keyword_sql%' OR `CallNum2` LIKE '%$converted_keyword_sql%' OR ";
		$conds .= "CONCAT(`CallNum`, ' ', `CallNum2`) LIKE '%$unconverted_keyword_sql%' OR CONCAT(`CallNum`, ' ', `CallNum2`) LIKE '%$converted_keyword_sql%' OR ";
		$conds .= "`ISBN` LIKE '%$unconverted_keyword_sql%' OR `ISBN` LIKE '%$converted_keyword_sql%' OR ";
		$conds .= "`ISBN2` LIKE '%$unconverted_keyword_sql%' OR `ISBN2` LIKE '%$converted_keyword_sql%' OR ";
		$conds .= "(BookTitle like '%".implode("%' AND BookTitle LIKE'%",$words_1)."%') OR (BookTitle like '%".implode("%' AND BookTitle LIKE'%",$words_2)."%') OR ";
		$conds .= "(BookSubTitle like '%".implode("%' AND BookSubTitle LIKE'%",$words_1)."%') OR (BookSubTitle like '%".implode("%' AND BookSubTitle LIKE'%",$words_2)."%') OR ";
		$conds .= "(ResponsibilityBy1 like '%".implode("%' AND ResponsibilityBy1 LIKE'%",$words_1)."%') OR (ResponsibilityBy1 like '%".implode("%' AND ResponsibilityBy1 LIKE'%",$words_2)."%') OR ";
		$conds .= "(ResponsibilityBy2 like '%".implode("%' AND ResponsibilityBy2 LIKE'%",$words_1)."%') OR (ResponsibilityBy2 like '%".implode("%' AND ResponsibilityBy2 LIKE'%",$words_2)."%') OR ";
		$conds .= "(ResponsibilityBy3 like '%".implode("%' AND ResponsibilityBy3 LIKE'%",$words_1)."%') OR (ResponsibilityBy3 like '%".implode("%' AND ResponsibilityBy3 LIKE'%",$words_2)."%') OR ";
		$conds .= "(Series like '%".implode("%' AND Series LIKE'%",$words_1)."%') OR (Series like '%".implode("%' AND Series LIKE'%",$words_2)."%')";
		$conds .= ")";
	
	}		// end keyword contains at least one of the special character: &<>'"\
}		// basic search
else{	// advance search
	$ACNO = trim($ACNO);
	$publisher = trim($publisher);
	$author = trim($author);
	$callNumber =trim($callNumber);
	$ISBN = trim($ISBN);
	$bookTitle = trim($bookTitle);
	$bookSeries = trim($bookSeries);
	$bookSeriesno = trim($bookSeriesno);
	
	if($ACNO!=""){
		// ACNO does not contain double quote, and it's stored in db for original characters like: '<>&\
		$unconverted_ACNO = mysql_real_escape_string(str_replace("\\","\\\\",$ACNO));
		$converted_ACNO = special_sql_str($ACNO);									
		$conds .= " AND (
			`ACNO` LIKE '%$unconverted_ACNO%' OR `ACNO` LIKE '%$converted_ACNO%'
			)";
	}
	
	if($publisher!=""){
		$unconverted_publisher = mysql_real_escape_string(str_replace("\\","\\\\",$publisher));
		$converted_publisher = special_sql_str($publisher);									
		$conds .= " AND (
			`Publisher` LIKE '%$unconverted_publisher%' OR `Publisher` LIKE '%$converted_publisher%'
			)";
	}
	
	if($author!=""){
		$unconverted_author = mysql_real_escape_string(str_replace("\\","\\\\",$author));
		$converted_author = special_sql_str($author);									
		$conds .= " AND (
				`ResponsibilityBy1` LIKE '%$unconverted_author%' OR `ResponsibilityBy1` LIKE '%$converted_author%' OR
				`ResponsibilityBy2` LIKE '%$unconverted_author%' OR `ResponsibilityBy2` LIKE '%$converted_author%' OR
				`ResponsibilityBy3` LIKE '%$unconverted_author%' OR `ResponsibilityBy3` LIKE '%$converted_author%'
			)";
	}
		
	if($callNumber!=""){
		$unconverted_callNumber = mysql_real_escape_string(str_replace("\\","\\\\",$callNumber));
		$converted_callNumber = special_sql_str($callNumber);									
		$conds .= " AND (
			`CallNum` LIKE '%$unconverted_callNumber%' OR `CallNum` LIKE '%$converted_callNumber%' OR
			`CallNum2` LIKE '%$unconverted_callNumber%' OR `CallNum2` LIKE '%$converted_callNumber%' OR
			CONCAT(`CallNum`, ' ', `CallNum2`) LIKE '%$unconverted_callNumber%' OR 
			CONCAT(`CallNum`, ' ', `CallNum2`) LIKE '%$converted_callNumber%'
			)";
	}
	
	if($ISBN !=""){
		$unconverted_ISBN = mysql_real_escape_string(str_replace("\\","\\\\",$ISBN));
		$converted_ISBN = special_sql_str($ISBN);									
		$conds .=" AND
			(`ISBN` LIKE '%$unconverted_ISBN%' OR `ISBN` LIKE '%$converted_ISBN%' OR
			`ISBN2` LIKE '%$unconverted_ISBN%' OR `ISBN2` LIKE '%$converted_ISBN%')";
	}
	
	if($bookTitle !=""){
		$unconverted_bookTitle = mysql_real_escape_string(str_replace("\\","\\\\",$bookTitle));		// A&<>'"\B ==> A&<>\'\"\\\\B
		$converted_bookTitle = special_sql_str($bookTitle);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
		$conds .=" AND
			(`BookTitle` LIKE '%$unconverted_bookTitle%' OR `BookTitle` LIKE '%$converted_bookTitle%' OR
			`BookSubTitle` LIKE '%$unconverted_bookTitle%' OR `BookSubTitle` LIKE '%$converted_bookTitle%')";
	}
	
	if($bookSeries !=""){
	    $unconverted_bookSeries = mysql_real_escape_string(str_replace("\\","\\\\",$bookSeries));
	    $converted_bookSeries = special_sql_str($bookSeries);
	    $conds .=" AND
	    (`Series` LIKE '%$unconverted_bookSeries%' OR `Series` LIKE '%$converted_bookSeries%')";
	}
	
	if($bookSeriesno !=""){
	    $unconverted_bookSeriesno = mysql_real_escape_string(str_replace("\\","\\\\",$bookSeriesno));
	    $converted_bookSeriesno = special_sql_str($bookSeriesno);
	    $conds .=" AND
	    (`SeriesNum` LIKE '%$unconverted_bookSeriesno%' OR `SeriesNum` LIKE '%$converted_bookSeriesno%')";
	}
	
}

//Henry Added [2015-01-30]
$BookCategoryCodeTemp = explode(":",$BookCategoryCode);
$BookCategoryType = $BookCategoryCodeTemp[0];
$BookCategoryCode = $BookCategoryCodeTemp[1];
if($BookCategoryType =='NA'){
	$conds .= " AND (BookCategoryCode = '' OR BookCategoryCode is null) ";
}
else if($BookCategoryCode!=""){
//	$conds .= ' AND (Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$BookCategoryType.'") OR '.$BookCategoryType.' = 1 AND (Language IS NULL OR Language = "")) ';
	$conds .= ' AND (Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$BookCategoryType.'") OR Language IS NULL OR Language = "") ';
}
//$conds .= get_BookStatusConditions_SQL($BookStatus);
$conds .= get_xxxConditions_SQL('BookCategoryCode', $BookCategoryCode);
$conds .= get_xxxConditions_SQL('ResourcesTypeCode', $ResourcesTypeCode);
//$conds .= " ".get_xxxConditions_SQL('B.CirculationTypeCode', $CirculationTypeCode);
// below is the 
if ($CirculationTypeCode!="")
{
	$conds .= " AND ((b.CirculationTypeCode is not null AND b.CirculationTypeCode='".addslashes($CirculationTypeCode)."')". 
				" OR (bu.CirculationTypeCode is not null AND bu.CirculationTypeCode='".addslashes($CirculationTypeCode)."') ) ";
}

$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(b.`BookID` USING utf8)" : "b.`BookID`";

// $sql = "SELECT 
// 	`ISBN`, CONCAT(IF(`CallNum` IS NULL,'',`CallNum`), ' ', IF(`CallNum2` IS NULL,'',`CallNum2`)) As CallNumberMixed,
// 	CONCAT('<a href=\'book_edit.php?Code=', {$BookID_For_CONCAT}, '\' >', `BookTitle`,IF(`BookSubTitle` IS NULL OR `BookSubTitle`='','',CONCAT('<br>- ',`BookSubTitle`)), '</a>'), 
// 	`ResponsibilityBy1`, `Publisher`, `Edition`,
// 	CONCAT(if(b.OpenBorrow=1, '', '* '), '<a href=\"#TB_inline?height=450&width=850&inlineId=FakeLayer\" class=\"thickbox\" title=\"".$Lang['libms']['book']['view_item']."\" onclick=\"js_Show_Book_Item(\'',b.`BookID`,'\')\">', `NoOfCopyAvailable`, '/', `NoOfCopy`, '</a>'), 
// 	CONCAT('<input type=\'checkbox\' name=\'Code[]\' id=\'Code[]\' value=\'', b.`BookID` ,'\' />')
// FROM
// 	`LIBMS_BOOK` b
// LEFT JOIN 
// 	`LIBMS_BOOK_UNIQUE` bu
// 	ON b.`BookID` = bu.`BookID` AND (bu.`RecordStatus` NOT LIKE 'DELETE' OR bu.`RecordStatus` IS NULL)
// WHERE
// 	(b.`RecordStatus` NOT LIKE 'DELETE' OR b.`RecordStatus` IS NULL)
// 	{$conds}
// 	{$locationCode_cond}
// 	{$publisherSelect_cond}	
// 	{$subjectSelect_cond} 
// 	{$languageSelect_cond}
// Group By
// 	b.BookID
// ";
$sql = "SELECT
	`ISBN`, CONCAT(IF(`CallNum` IS NULL,'',`CallNum`), ' ', IF(`CallNum2` IS NULL,'',`CallNum2`)) As CallNumberMixed,
    IF(
    	b.CoverImage != '', CONCAT('<a href=\"book_edit.php?&Code=',{$BookID_For_CONCAT},'\">',  `BookTitle`,IF(`BookSubTitle` IS NULL OR `BookSubTitle`='','',CONCAT('<br>- ',`BookSubTitle`)),'</a>&nbsp;<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pic.gif\" onMouseOver=\"javascript:MM_swapImage(''book_photo_',b.BookID,'','','','',''','''',''',b.CoverImage,''',1)\" onMouseOut=\"javascript:MM_swapImgRestore()\" id=\"book_photo_',b.BookID,'','','','','\" name=\"book_photo_',b.BookID,'','','','','\" style=\"position: absolute;top: auto;left: auto;max-height: 250px;max-width: 300px;\">'),
    	CONCAT('<a href=\'book_edit.php?Code=', {$BookID_For_CONCAT}, '\' >', `BookTitle`,IF(`BookSubTitle` IS NULL OR `BookSubTitle`='','',CONCAT('<br>- ',`BookSubTitle`)), '</a>')
    ),
	`ResponsibilityBy1`, `Publisher`, `Edition`,
	CONCAT(if(b.OpenBorrow=1, '', '* '), '<a href=\"#TB_inline?height=450&width=850&inlineId=FakeLayer\" class=\"thickbox\" title=\"".$Lang['libms']['book']['view_item']."\" onclick=\"js_Show_Book_Item(\'',b.`BookID`,'\')\">', `NoOfCopyAvailable`, '/', `NoOfCopy`, '</a>'),
	CONCAT('<input type=\'checkbox\' name=\'Code[]\' id=\'Code[]\' value=\'', b.`BookID` ,'\' />')
FROM
	`LIBMS_BOOK` b
LEFT JOIN
	`LIBMS_BOOK_UNIQUE` bu
	ON b.`BookID` = bu.`BookID` AND (bu.`RecordStatus` NOT LIKE 'DELETE' OR bu.`RecordStatus` IS NULL)
WHERE
	(b.`RecordStatus` NOT LIKE 'DELETE' OR b.`RecordStatus` IS NULL)
	{$conds}
	{$locationCode_cond}
	{$publisherSelect_cond}
	{$subjectSelect_cond}
	{$languageSelect_cond}
    {$bookCover_cond}
Group By
	b.BookID
";

//debug ($sql);


$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array("ISBN", "CallNumberMixed","BookTitle", "ResponsibilityBy1", "Publisher", "Edition", "NoOfCopyAvailable");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['libms']['book']['ISBN'])."</td>\n";
$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['libms']['book']['call_number'])."</td>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['libms']['book']['title'])."</td>\n";
$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['libms']['book']['responsibility_by1'])."</td>\n";
$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['libms']['book']['publisher'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['edition'])."</td>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang["libms"]["book"]["number_of_copy_available"]."/".$Lang['libms']['book']['number_of_copy'])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("Code[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("book_new.php",$button_new,"","","",0);
$toolbar .= " ". $linterface->GET_LNK_IMPORT("import/csv/import_csv_data2.php",$button_import,"","","",0);
$toolbar .= " ". $linterface->GET_LNK_IMPORT("import/cover/index.php",$Lang['libms']['import_book_cover'],"","","",0);
//$toolbar .= " ". $linterface->GET_LNK_EXPORT("javascript:jsExportBooks('".trim($advanceSearch)."')",$button_export,"","","",0);
$toolbar .= " ". $linterface->GET_LNK_EXPORT("javascript:tb_show('".$Lang['libms']['bookmanagement']['SelectItemToExport']."','ajax_show_fields_to_export.php?AdvanceSearch=".trim($advanceSearch)."&height=500&width=800')",$button_export,"","","",0);
$toolbar .= " ". $linterface->GET_LNK_EXPORT("javascript:jsExportBooksMarc21('".trim($advanceSearch)."')",$Lang['libms']['book_export_marc21'],"","","",0);
//$toolbar .= " ". $linterface->GET_LNK_EXPORT("javascript:jsExportBooksWithoutWriteoff('".trim($advanceSearch)."')",$Lang["libms"]["book_export_without_writeoff"],"","","",0);
$toolbar .= " ". $linterface->GET_LNK_EXPORT("javascript:tb_show('".$Lang['libms']['bookmanagement']['SelectItemToExport']."','ajax_show_fields_to_export.php?AdvanceSearch=".trim($advanceSearch)."&ExcludeWriteoff=1&height=500&width=800')",$Lang["libms"]["book_export_without_writeoff"],"","","",0);
$keyword = htmlspecialchars($keyword);
$searchTag = "<input autocomplete=\"off\" name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";	//  onkeyup=\"Check_Go_Search(event);\"
# Search Box
$advanceSearchDiv = $libms -> getAdvanceSearchDiv();
//*/
############################################################################################################

# Top menu highlight setting

$linterface->LAYOUT_START(($DirectMsg == 'T') ? $xmsg : $Lang['General']['ReturnMessage'][$xmsg]);

echo $linterface->Include_Thickbox_JS_CSS();

# Table Action Button
# Edit button
$btnOption .= "<a href=\"javascript:checkEdit(document.form1,'Code[]','book_edit.php')\" class=\"tool_edit\">" . $button_edit . "</a>";

# Copy button
$btnOption .= "<a href=\"javascript:checkEdit(document.form1,'Code[]','book_edit.php?IsCopy=1')\" class=\"tool_copy\">" . $button_copy . "</a>";

# Del button
$btnOption .= "<a href=\"javascript:checkRemove(document.form1,'Code[]','book_remove.php')\" class=\"tool_delete\">" . $button_delete . "</a>";


$filters_html = get_filters_html();

# Location filter 
$locationSelectionBox = '';
$locationInfo = $libms->GET_LOCATION_INFO('',1);
$numOfLocationInfo = count($locationInfo);
$locationSelectionOption = array();

$locationSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);

for($i=0;$i<$numOfLocationInfo;$i++){

	$thisLocationCode = $locationInfo[$i]['LocationCode'];
	$thisLocationEngName = $locationInfo[$i]['DescriptionEn'];
	$thisLocationChiName = $locationInfo[$i]['DescriptionChi'];
	$thisOptionlocationName = Get_Lang_Selection($thisLocationChiName,$thisLocationEngName);
	$locationSelectionOption[]= array($thisLocationCode,$thisOptionlocationName);
}

# Subject filter
$publisherSelectionBox = '';

$sql1 = "SELECT DISTINCT `Subject`
			FROM
				`LIBMS_BOOK` b
			LEFT JOIN 
				`LIBMS_BOOK_UNIQUE` bu
				ON b.`BookID` = bu.`BookID`
			WHERE
				(b.`RecordStatus` NOT LIKE 'DELETE' OR b.`RecordStatus` IS NULL)
				{$conds}
				{$locationCode_cond}
			ORDER by Subject ASC
			";
$subjectInfo = $libms->returnResultSet($sql1);

$subjectSelectionOption = array();

$subjectSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);

for($i=0;$i<count($subjectInfo);$i++){
	if($subjectInfo[$i]['Subject'] != '')
	$subjectSelectionOption[]= array($subjectInfo[$i]['Subject'], $subjectInfo[$i]['Subject']);
}


# Publisher filter
//Henry Modifying
$publisherSelectionBox = '';

$sql1 = "SELECT DISTINCT `Publisher`
			FROM
				`LIBMS_BOOK` b
			LEFT JOIN 
				`LIBMS_BOOK_UNIQUE` bu
				ON b.`BookID` = bu.`BookID`
			WHERE
				(b.`RecordStatus` NOT LIKE 'DELETE' OR b.`RecordStatus` IS NULL)
				{$conds}
				{$locationCode_cond}
			ORDER by trim(Publisher) ASC
			";
//debug_pr($sql1);
$publisherInfo = $libms->returnArray($sql1);

//$publisherInfo = GET_PUBLISHER($conds, $locationCode_cond);
$numOfPublisherInfo = count($publisherInfo);
$publisherSelectionOption = array();

$publisherSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);

for($i=0;$i<$numOfPublisherInfo;$i++){
	if($publisherInfo[$i]['Publisher'] != '')
	$publisherSelectionOption[]= array($publisherInfo[$i]['Publisher'], $publisherInfo[$i]['Publisher']);
}

//Henry added 20140114
# Language filter
$languageSelectionBox = '';

$sql1 = "SELECT DISTINCT `Language`
			FROM
				`LIBMS_BOOK` b
			LEFT JOIN 
				`LIBMS_BOOK_UNIQUE` bu
				ON b.`BookID` = bu.`BookID`
			WHERE
				(b.`RecordStatus` NOT LIKE 'DELETE' OR b.`RecordStatus` IS NULL)
				{$conds}
				{$locationCode_cond}
			ORDER by Language ASC
			";
$languageInfo = $libms->returnResultSet($sql1);

	$desc_field = $Lang["libms"]["sql_field"]["Description"];
	$sql1 = "SELECT DISTINCT `BookLanguageCode`, ".$desc_field." 
		FROM
			`LIBMS_BOOK_LANGUAGE` 
		ORDER by BookLanguageCode ASC";
	$rs2 = $libms->returnResultSet($sql1);
	$rs2 = BuildMultiKeyAssoc($rs2,"BookLanguageCode");
	
	$result = array();
	if (count($languageInfo) > 0) {
		foreach((array)$languageInfo as $k=>$v) {
			if ($v['Language'] != '') {
				$description = $rs2[$v['Language']][$desc_field] ? $rs2[$v['Language']][$desc_field] : $v['Language'];
				$result[$v['Language']] = $description;
			}
		}
	}

$languageSelectionOption = array();

$languageSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);
foreach((array)$result as $k=>$v) {
	$languageSelectionOption[]= array($k, $v);
}
//for($i=0;$i<count($languageInfo);$i++){
//	if($languageInfo[$i]['Language'] != '')
//	$languageSelectionOption[]= array($languageInfo[$i]['Language'], $languageInfo[$i]['Language']);
//}

//--Should add to the lib
//function GET_PUBLISHER($conds, $locationCode_cond){
//	$sql = "SELECT DISTINCT `Publisher`
//			FROM
//				`LIBMS_BOOK` b
//			LEFT JOIN 
//				`LIBMS_BOOK_UNIQUE` bu
//				ON b.`BookID` = bu.`BookID`
//			WHERE
//				(b.`RecordStatus` NOT LIKE 'DELETE' OR b.`RecordStatus` IS NULL)
//				{$conds}
//				{$locationCode_cond}
//			GROUP BY 
//				b.`BookCode`
//			";
//			
//	return $this->returnArray($sql);
//}


//$locationSelectionBox = $linterface->GET_SELECTION_BOX($locationSelectionOption, 'name="LocationCode" id="LocationCode" onchange="document.form1.submit()"', "-- {$Lang['libms']['bookmanagement']['location']} --", $LocationCode);

//$filters_html.= $locationSelectionBox;

$subjectSelectionBox = $linterface->GET_SELECTION_BOX($subjectSelectionOption, 'name="SubjectSelect" id="SubjectSelect" onchange="document.form1.submit()"', "-- {$Lang['libms']['book']['subject']} --", htmlspecialchars($SubjectSelect));

$filters_html.= $subjectSelectionBox;

$publisherSelectionBox = $linterface->GET_SELECTION_BOX($publisherSelectionOption, 'name="PublisherSelect" id="PublisherSelect" onchange="document.form1.submit()"', "-- {$Lang['libms']['book']['publisher']} --", htmlspecialchars($PublisherSelect));

$filters_html.= $publisherSelectionBox;

//Henry added 20140114
$languageSelectionBox = $linterface->GET_SELECTION_BOX($languageSelectionOption, 'name="LanguageSelect" id="LanguageSelect" onchange="document.form1.submit()"', "-- {$Lang['libms']['book']['language']} --", htmlspecialchars($LanguageSelect));

$filters_html.= $languageSelectionBox;

$showBookCover = "<br><input type='checkbox' name='bookCover' value='1' id='bookCover' onClick='displayCoverRecordOnly();' " . ($bookCover_temp ? "checked" : "") . "> <label for ='bookCover'>".$Lang["libms"]["book"]["showOnlyBookCoverRecord"]."</label>";
$showBookCover .= "<input type='hidden' name='bookCover_temp' value='" . $bookCover_temp . "'>";

$filters_html .= $showBookCover;
?>
<script src="/templates/jquery/jquery-1.6.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="javascript">
/*
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}*/
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{ 
		$("#advanceSearch").val(0);
		document.form1.submit();
	}
	else
		return false;
}


function js_Show_Book_Item(jsBookID){
	$.post(
		"ajax_item_list.php", 
		{
			BookID: jsBookID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
		}
	);
}

function jsExportBooks(advanceSearch)
{
	var original_action = document.form1.action;
	var url = "book_export.php";
	if (parseInt(advanceSearch)) {
		url += "?advanceSearch=1";
	}
	document.form1.action = url;
	document.form1.submit();
	document.form1.action = original_action;
}

function jsExportBooksWithoutWriteoff(advanceSearch)
{
	var original_action = document.form1.action;
	var url = "book_export.php?ExcludeWriteoff=1";
	if (parseInt(advanceSearch)) {
		url += "&advanceSearch=1";
	}
	document.form1.action = url;
	document.form1.submit();
	document.form1.action = original_action;
}

function jsExportBooksMarc21(advanceSearch)
{
    var original_action = document.form1.action;
	var url = "book_export_marc21.php";
	if (parseInt(advanceSearch)) {
		url += "?advanceSearch=1";
	}    
	document.form1.action = url;
	document.form1.submit();
	document.form1.action = original_action;

}
var imgObj=0;
var callback = {
        success: function ( o )
        {
                writeToLayer('ToolMenu2',o.responseText);
                showMenu2("img_"+imgObj,"ToolMenu2");
        }
}
function hideMenu2(menuName){
		objMenu = document.getElementById(menuName);
		if(objMenu!=null)
			objMenu.style.visibility='hidden';
		setDivVisible(false, menuName, "lyrShim2");
}
function showMenu2(objName,menuName){
		  hideMenu2('ToolMenu2');
           objIMG = document.getElementById(objName);
           objToolMenu2 = document.getElementById('ToolMenu2');
           objkeyword = document.getElementById('keyword');
			offsetX = (objIMG==null)?0:objIMG.width;
			offsetY =0;            
            var pos_left = getPostion(objIMG,"offsetLeft");
			var pos_top  = getPostion(objIMG,"offsetTop");
			
			objDiv = document.getElementById(menuName);
			
			if(objDiv!=null){
				objDiv.style.visibility='visible';
//				objDiv.style.top = pos_top+offsetY+"px";
//				objDiv.style.left = pos_left+offsetX+"px";
				objDiv.style.top = pos_top+30+"px";
				objDiv.style.left = pos_left-282+"px";
				setDivVisible(true, menuName, "lyrShim2");
			}
}
function showInfo(val)
{
        obj = document.form1;

        var myElement = document.getElementById("ToolMenu2");

        writeToLayer('ToolMenu2','');
        imgObj = val;
        
		showMenu2("img_"+val,"ToolMenu2");
        YAHOO.util.Connect.setForm(obj);

        var path = "getHelpInfo.php?Val=" + val;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

var timerobj;
function jsShowBookHide()
{
	timerobj = setTimeout(ShowBookHideAction,1000);
}

function ShowBookHideAction()
{
	$("#keyword").focus();
	clearTimeout(timerobj);
}

$().ready( function(){
//	$.ajaxSetup({	
//		cache:false, async:false
//	});
	
	if($("#keyword").length > 0){
		$("#keyword").autocomplete(
	      "ajax_get_book_suggestion.php",
	      {
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			onItemSelect: function() { jsShowBookHide(); },
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width:'175px'
	  		}
	    );
	}
	
});

function DisplayAdvanceSearch() {
	
	var pos = $("#AdvanceSearchText").position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	
	topPos = parseInt(topPos) + 25;
	leftPos = parseInt(leftPos) - 482;
	
	//MM_showHideLayers('DR_search','','show');
	$("#advance_search").show();$("#advance_search").css({"position": "absolute", "top": topPos, "left": leftPos, "z-index":99, "background":"none repeat scroll 0% 0% rgb(244, 244, 244)", "padding":"7px 7px 0px","border":"1px solid rgb(204, 204, 204)","width":"500px"});
}

function displayCoverRecordOnly(){
	var obj=document.form1;
	
	if(obj.bookCover.checked==true)
		obj.bookCover_temp.value=1;
	else
		obj.bookCover_temp.value=0;
	obj.submit();	
}

function HiddenAdvanceSearch(){
	$("#advance_search").hide();
}
function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];x.style.zIndex = 1;}
	}
function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) {x.src=x.oSrc;x.style.zIndex = 0;};
	}
</script>
<!--  <form name="form1" method="post" action=""> -->
<form name="form1" id="form1" method="POST" action="index.php" onSubmit="return checkForm()">
	<div class="content_top_tool"  style="float: left;">
		<?=$toolbar?>
		</div
	<div class="content_top_tool"  style="float: right;">
		<?=$advanceSearchDiv?><div class="Conntent_search"> | </div>
	    <a href="javascript:showInfo(1)" style="float:right;"><img id='img_1' src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/inventory/icon_help.gif" border=0></a>     
		<div class="Conntent_search"><?=$searchTag?></div>     
		<br style="clear:both" />
	</div>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="table-action-bar">
	<td valign="bottom">
		<?=$filters_html?>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
		<?=$btnOption?>
		</div>
	</td>
	</tr>
	</table>

	<?=$li->display();?>
	
	<br />
	<span class="tabletextrequire">*</span> <?= $Lang['libms']['book']['status']['remark'] ?>
	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="advanceSearch" id="advanceSearch" value="<?=$advanceSearch?>" />
	<div id="hidFieldSelection"></div>
	
	<iframe id='lyrShim2'  scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; visibility:hidden; display:none;'></iframe>
    <div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"></div>
<? /*
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td colspan='2'>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="40%"><?=$toolbar ?></td>
											<td width="30%" align="center">
											<? $linterface->GET_SYS_MSG($xmsg) ?>
											</td>
											<td width="30%" align="right">
												<div class="content_top_tool">
													<div class="Conntent_search"><?=$searchTag?></div>     
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<div class='filtering_options'>
										<?=$filters_html?>
									</div>
								</td>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'Code[]','book_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'Code[]','book_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'Code[]','book_edit.php?IsCopy=1')" class="tabletool" title="<?= $Lang['Btn']['Copy'] ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_copy.gif" width="12" height="12" border="0" align="absmiddle"> <?= $Lang['Btn']['Copy'] ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
	*/ ?>
</form>

<?php

$linterface->LAYOUT_STOP();
 
intranet_closedb();

function get_filters_html(){
	global $libms,$Lang;
	$x  = get_CirTypeFilter_html();
	$x .= get_ResTypeFilter_html();
	$x .= get_BookCatFilter_html();
	//$x .= get_BookStatusFilter_html();
	return $x;
}

/**
 * get Circulation Type Filter html of <select>
 */
function get_BookStatusFilter_html(){
	global $libms,$Lang,$linterface, $BookStatus;
	$y = 0;
	
	$BookStatusArr = array();
	$BookStatusArr[$y][0] = "";
	$BookStatusArr[$y][1] = "-- {$Lang['libms']['book']['select']['Book_Status']} --";
	
	foreach ($Lang['libms']['book_status'] as $key => $value)
	{
		$y++;
		$BookStatusArr[$y][0] = $key;
		$BookStatusArr[$y][1] = $value;
		
	}
	$BookStatusOption = $linterface->GET_SELECTION_BOX($BookStatusArr," name='BookStatus' class='' onchange='document.form1.submit()' ", '', $BookStatus);
	
	return $BookStatusOption;
}
function get_BookStatusConditions_SQL ($selected=''){
	$conds = '';
	if (!empty($selected)){
		//echo $feildname;
		if($selected=='NULL'){
			$v  = '';
		}else{
			$v = $selected;
		}
		$sql_safe_CODE = PHPToSQL($v);
		$conds = " AND bu.`RecordStatus` LIKE {$sql_safe_CODE} ";
	}

	return $conds;
}





/**
 * get Circulation Type Filter html of <select>
 */
function get_CirTypeFilter_html(){
	global $libms, $Lang, $CirculationTypeCode;
	return get_xxxFilter_html('LIBMS_CIRCULATION_TYPE','CirculationTypeCode', $CirculationTypeCode);
}

/**
 * get Resource Type Filter html of <select>
 */
function get_ResTypeFilter_html(){
	global $libms, $Lang, $ResourcesTypeCode;
	return get_xxxFilter_html('LIBMS_RESOURCES_TYPE','ResourcesTypeCode', $ResourcesTypeCode);
}

/**
 * get Book Category Filter html of <select>
 */
//function get_BookCatFilter_html(){
//	global $libms,$Lang, $BookCategoryCode;	
//	return get_xxxFilter_html('LIBMS_BOOK_CATEGORY','BookCategoryCode', $BookCategoryCode);
//}
function get_BookCatFilter_html(){
	global $libms,$Lang, $BookCategoryCode, $BookCategoryType;	
	$BookCategoryTypeCode = $BookCategoryType.':'.$BookCategoryCode;
	
	$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
	foreach ($result as $row){
		$book_category_name[] = htmlspecialchars($row['value']);
	}
	
	if(!$BookCategoryType && !$BookCategoryCode){
		$BookCategoryTypeCode = '';
	}
	$fields[] = 'BookCategoryType';
	$fields[] = 'BookCategoryCode';
	$fields[] = $Lang["libms"]["sql_field"]["Description"];

	$result =  $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY',$fields, null, null, 0,array('BookCategoryType`','`BookCategoryCode'));
	$x = '<select name="BookCategoryCode" id="BookCategoryCode" onchange="document.form1.submit()">
			<option value="" '.(!$BookCategoryTypeCode?'selected="selected"':'').' >-- '.$Lang['libms']['book']['select']['BookCategoryCode'].' --</option>
			<option value="NA" '.($BookCategoryType=='NA'?'selected="selected"':'').' >'.$Lang["libms"]["status"]["na"].'</option>
			<optgroup label="'.($book_category_name[0]?$book_category_name[0]:$Lang["libms"]["report"]["book_category"].' 1').'">';
			
		$tempBookCategoryType = 1;
		foreach( $result as $row ){
			$code = $row['BookCategoryType'].':'.$row['BookCategoryCode'];
			$name = $row[$Lang["libms"]["sql_field"]["Description"]];
			if ($code == $BookCategoryTypeCode) {
				$selected = 'selected="selected"';
				$default_sel = '';
			}else{
				$selected = '';
			}
			if($tempBookCategoryType != $row['BookCategoryType'])
				$x .='</optgroup><optgroup label="'.($book_category_name[1]?$book_category_name[1]:$Lang["libms"]["report"]["book_category"].' 2').'">';
			$tempBookCategoryType = $row['BookCategoryType'];
			$x .='<option value="'.$code.'" '.$selected.' >'.$name.'</option>';
		}
		
		$x .='</optgroup></select>';
	return $x;
}

function get_xxxConditions_SQL($fieldname, $selected=''){
	$conds = '';
	if ($selected != ''){		// 2015-07-27: don't use !empty() as $selected value can be '0' 
		$v = trim($selected);
		$sql_safe_CODE = PHPToSQL($v);
		$conds = " AND TRIM(`{$fieldname}`) LIKE {$sql_safe_CODE} ";
	}
	
	return $conds;
}

function get_xxxFilter_html($table, $fieldname, $selectedVal=''){
	global $libms,$Lang;

	$fields[] = $fieldname;
	$fields[] = $Lang["libms"]["sql_field"]["Description"];

	$result =  $libms->SELECTFROMTABLE($table,$fields);
	//dump($result);
	$options='';
	// generate selection
	if (!empty($result)){
		$default_sel = 'selected="selected"';
		//$selected ='';
		foreach( $result as $row ){
			$code = $row[$fieldname];
			$name = $row[$Lang["libms"]["sql_field"]["Description"]];
//			if (empty($code)) {
//				$code = "NULL";
//			}
			if ($code === $selectedVal) {
				$selected = 'selected="selected"';
				$default_sel = '';
			}else{
				$selected = '';
			}
			

			$options .= <<<EOL
<option value="{$code}" {$selected} >{$name}</option>
EOL;
		}
	}
	$x = <<<EOL
<select name="{$fieldname}" id="{$fieldname}" onchange="document.form1.submit()">
<option value='' {$default_sel} > -- {$Lang['libms']['book']['select'][$fieldname]} -- </option>
	{$options}</select>
EOL;

	return $x;
}
