<?php
/*
 * 	modifying: 
 * 	Log
 * 	Purpose:	process update a field at a time for selected BookID
 *	@return:	array['result']		-- success (true) / fail (false) 
 * 				array['nrAffected']	-- number of record affected
 *
 * 	Date:	2017-02-09 	Cameron
 * 			- strip slashes after call lib.php for php5.4+
 * 
 *	Date:	2016-03-03  Cameron
 *			- use $_POST['Var'] instead of global variable $Var when assign value to $DataArr [case #F93085] 		
 *
 * 	Date:	2015-07-27 [Cameron]
 * 			- add LastModifiedBy and DateModified
 * 			- sync data to IP by calling SYNC_PHYSICAL_BOOK_TO_ELIB()
 * 
 * 	Date:	2015-07-08 [Cameron] get BookCategory value to update according to BookCategoryType
 * 	Date:	2014-12-12 [Cameron] create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/json.php");


intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

####### Trim all request and convert html special chars #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=htmlspecialchars(trim($item));
		}
	}
}

recursive_trim($_POST);

//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
			$value = stripslashes($value);
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
}
//****** end special handle for php5.4+

$BookID = $_POST['BookID'];
$UpdateField = $_POST['UpdateField'];
if ($UpdateField == 'BookCategory'){
	$ApplyBookCategoryType = $_POST['ApplyBookCategoryType'];
	$UpdateValue = ($ApplyBookCategoryType == '1')? $_POST['BookCategory1']:$_POST['BookCategory2'];
	$BookCategory = $UpdateValue;
}
else {
	$UpdateValue = $_POST["$UpdateField"];
}

$result = 0;	// number of records updated
$nrAffected = 0;

if (is_array($BookID) && $UpdateField != '') {
	$bookID = implode("','",$BookID);

	$DataArr = array();
	switch ($UpdateField) {
		case "BookLang":
			$DataArr["Language"] = $libms->pack_value($_POST['BookLang'],'str');
			break;
		case "BookCountry":
			$DataArr["Country"] = $libms->pack_value($_POST['BookCountry'],'str');
			break;
		case "BookCategory":
			$DataArr["BookCategoryCode"] = $libms->pack_value($BookCategory,'str');
			break;
		case "BookCirculation":
			$DataArr["CirculationTypeCode"] = $libms->pack_value($_POST['BookCirculation'],'str');
			break;
		case "BookResources":
			$DataArr["ResourcesTypeCode"] = $libms->pack_value($_POST['BookResources'],'str');
			break;
		case "BookSubj":
			$DataArr["Subject"] = $libms->pack_value($_POST['BookSubj'],'str');
			break;
		case "BookBorrow":
			$DataArr["OpenBorrow"] = $libms->pack_value($_POST['BookBorrow'],'int');
			break;
		case "BookReserve":
			$DataArr["OpenReservation"] = $libms->pack_value($_POST['BookReserve'],'int');
			break;
		default:
//			$DataArr["$UpdateField"] = $libms->pack_value($UpdateValue,'str');	
			break;
	}			
	
	$updateDetails = '';
	foreach ($DataArr as $fieldName => $data)
	{
		$updateDetails .= $fieldName."=".$data.",";
	}

	// Remvoe last occurrence of ",";
//	$updateDetails = substr($updateDetails,0,-1);

	if ($updateDetails) {
		$updateDetails .= "LastModifiedBy='{$UserID}',";
		$updateDetails .= "DateModified=now()";
		
		$sql = "UPDATE LIBMS_BOOK SET ".$updateDetails." WHERE BookID IN ('{$bookID}')";

		$result = $libms->db_db_query($sql);
		$nrAffected = count($BookID);	// all selected rows, not mysql_affected_rows() 

		// sync to IP
		$libel = new elibrary();
		foreach($BookID as $bid) {
			$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($bid);
		}
	}
}

$ret = array();
$ret['result'] = $result;
$ret['nrAffected'] = $nrAffected;
$json = new JSON_obj();
echo $json->encode($ret);

intranet_closedb();
?>