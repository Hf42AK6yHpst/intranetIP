<?php
/*
 * 	Using: 
 * 	
 * 	Log
 *	
 *	2016-04-19	[Cameron]
 *		- redirect to tag list (batch_modify_tags.php) if TagID is lost (case #P94412)
 *		- fix bug on showing record: should include records which RecordStatus is null in sql condition 
 * 	
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (empty($TagID)) {
	header("Location: batch_modify_tags.php");
	exit;
}

$linterface = new interface_html();


//$groupSelection = $libms->getGroupSelection();

# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";


$PAGE_NAVIGATION[] = array($Lang['libms']['batch_edit']['edit_tags'], "javascript:history.back()");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");

# Left menu 
$TAGS_OBJ = $libms->get_batch_edit_tabs("TAGS");
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();

$FieldArray = array("t.TagName", "BookTotal", "t.DateModified");
$sql_oder = " ORDER by ".$FieldArray[$field] . " ". (($order==1) ? "ASC" : "DESC");
$tags_id = implode(", ", $TagID);
$sql = "SELECT 
			t.TagName, count(bt.BookID) As BookTotal, t.TagID
		FROM 
			LIBMS_TAG AS t, LIBMS_BOOK_TAG AS bt, LIBMS_BOOK AS b
		WHERE
			t.TagID IN ($tags_id) AND  bt.TagID=t.TagID  AND b.BookID=bt.BookID 
		AND (b.RecordStatus<>'DELETE' OR b.RecordStatus IS NULL)
		 Group by t.TagID
		 $sql_oder
        ";
        
$rows = $libms->returnResultSet($sql);

if (sizeof($TagID)==1)
{
	$TagNameEdit = str_replace('"', '&quot;', $rows[0]["TagName"]);
} else
{
	# multiple
}

for ($i=0; $i<sizeof($rows); $i++)
{
	$origin_tags .= $rows[$i]["TagName"] . " - <a href=\"book_list.php?TagID=". $rows[$i]["TagID"]."&width=780&height=500\" class=\"thickbox\" title=\"".str_replace('"', '&quot;', $rows[$i]["TagName"])."\">".$rows[$i]["BookTotal"]." ".$Lang["libms"]["book"]["book_total"]."</a><br />";
}
?>



<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="javascript">
function checkForm(form1) {

	if (form1.TagName.value=="" || form1.TagName.value==" " || form1.TagName.value=="  ")
	{
		alert("<?=$Lang["libms"]["book"]["warning_no_new_tag"]?>");
		form1.TagName.focus();
		return false;
	} elseif (confirm("<?=$Lang["libms"]["book"]["warning_tag_edit_confirm"]?>"))
	{
		return true;
	}
	
	return false;
}

</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" method="post" action="batch_modify_tags_edit_update.php" onSubmit="return checkForm(this)">

<table width="90%" align="center" border="0">
<tr><td>
	<input name="Code" type="hidden" class="textboxnum" value="<?=$Code?>" />
	<table class="form_table_v30">
		<tr valign="top">
			<td class='field_title'><?=$Lang["libms"]["book"]["title_origin"] ?></td>
			<td><?= $origin_tags ?></td>
		</tr>
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang["libms"]["book"]["title_new"] ?></td>
			<td><input id="TagName" name="TagName" type="text" class="textboxtext" value="<?=$TagNameEdit?>"></td>
		</tr>
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()")?>
	</div>	

</td></tr>
</table>

<input type="hidden" name="TagIDs" value="<?=$tags_id?>" />

</form><?php
echo $linterface->FOCUS_ON_LOAD("form1.TagName"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
