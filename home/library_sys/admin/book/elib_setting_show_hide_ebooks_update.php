<?php
/*
 * 	Log
 *
 * 	@purpose: show/hide eBook
 *
 * 	2020-10-15 [Cameron] create this file
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/libelibrary_install.php');

intranet_auth();
intranet_opendb();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$QuotationID = IntegerSafe($_POST['QuotationID']);
$BookID = IntegerSafe($_POST['BookID']);
$IsShow = IntegerSafe($_POST['IsShow']);

if(sizeof($_POST)==0) {
    header("Location: elib_setting_show_hide_ebooks.php?QuotationID={$QuotationID}");
}

$libelibinstall = new elibrary_install();


$result = array();


if (count($BookID)) {
    $result = $libelibinstall->updateShowHideOfeBook($QuotationID, $BookID, $IsShow);
}

$xmsg = $result ? 'UpdateSuccess' : 'UpdateUnsuccess';

intranet_closedb();

header("Location: elib_setting_show_hide_ebooks.php?QuotationID={$QuotationID}&xmsg=".$xmsg);

?>
