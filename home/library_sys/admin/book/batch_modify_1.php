<?php

//modifying: Henry

/************************************
 * Change Log
 * Date:	2017-03-03 [Henry]
 * 			- auto select all classes of target in batch edit [Case#E81339]
 * 
 * Date:	2017-02-16 [Henry]
 * 			- change method to POST at form submit [Case#C111879]
 *
 * Date:	2015-07-27 [Cameron]
 * 			- disable change $code to 'NULL' if $code is empty in get_xxxFilter_html() 
 * 
 ************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);
/*
$TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_return_date']);
$TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_return_date'] , "batch_modify_1.php", 1);
$TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_penal_sum'], "batch_modify_2.php", 0);
//$TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_book_info'], "batch_modify_3.php", 0);
 * 
 */

$TAGS_OBJ = $libms->get_batch_edit_tabs("DUEDATE");


//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

# date range
$FromDate=$FromDate==""?date('Y-m-d'):$FromDate;
$ToDate=$ToDate==""?date('Y-m-d'):$ToDate;

# Group List
$sql = "SELECT GroupID, GroupTitle FROM LIBMS_GROUP ORDER BY GroupTitle";
$result = $libms->returnArray($sql);
$ReaderGroupSelection = getSelectByArray($result,"name=\"readerGroup[]\" id=\"readerGroup\" size=\"15\" multiple",$readerGroup,0,1);

#Class List
$lc = new libclass();
$sql = "Select distinct ClassName, ClassName From LIBMS_USER WHERE ClassName IS NOT NULL AND ClassName <> '' ORDER BY ClassName";
$result = $libms->returnArray($sql);
$ReaderClassSelection = $lc->getSelectClass("name=\"readerClass[]\" id=\"readerClass\" size=\"15\" multiple",$readerClass,1);
//$ReaderClassSelection = getSelectByArray($result,"name=\"readerClass[]\" id=\"readerClass\" size=\"10\" multiple",$readerClass,0,1);

############################################################################################################

# Top menu highlight setting

//$TAGS_OBJ[] = array($Lang["libms"]["stats"]["frequency"]);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);



?>
<script language="javascript">
<!--
$(function() {
	SelectAll(document.getElementById('readerClass'));
	SelectAll(document.getElementById('readerGroup'));
});

function checkForm(formObj){
        if(formObj==null) return false;
	    fromV = formObj.FromDate;
	    toV= formObj.ToDate;
	    if(!checkDate(fromV)){
            //formObj.FromDate.focus();
            return false;
	    }
	    else if(!checkDate(toV)){
            //formObj.ToDate.focus();
            return false;
	    }
	    else if(fromV.value > toV.value){
	    	alert("<?=$Lang['General']['WrongDateLogic']?>");
	    	return false;
	    }
	    else if(document.getElementById('radio6').checked && document.getElementById('readerClass').value ==""){
	    	alert("<?=$Lang['libms']['batch_edit']['PleaseSelectClass']?>");
	    	return false;
	    }
	    else if(document.getElementById('radio5').checked && document.getElementById('readerGroup').value ==""){
	    	alert("<?=$Lang['libms']['batch_edit']['PleaseSelectGroup']?>");
	    	return false;
	    }
	    return true;
}

function checkDate(obj){
        if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
        return true;
}

function submitForm(obj){
        if(checkForm(obj)) {
	        
			obj.submit();
		}
}

function SelectAll(obj)
{
	for (i=0; i<obj.length; i++)
	{
		obj.options[i].selected = true;
	}
}

//-->
</script>

<form name="form1" method="post" action="batch_modify_1b.php">
<!--###### Content Board Start ######-->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="13" >&nbsp;</td>
                      <td class="main_content"><!---->
                        <!--#### Navigator start ####-->
                        <!--<div class="navigation"><a href="#">User List</a>New User<br />
                      </div>-->
                        <!--#### Navigator end ####-->
                       <!--<div class="report_option report_option_title">- 選項 - </div>-->
       			  <div class="table_board">
       			    <div class="table_board">
                      <table class="form_table_v30">
                        <tr>
                          <td class="field_title"><span class="status_alert"><strong>*</strong></span><?=$Lang["libms"]["report"]["time_period"]?></td>
                          <td>
                          <input type="radio" name="periodBy" id="periodBy2" value="2" checked="checked" /> <label for="periodBy2"><?=$Lang["libms"]["batch_edit"]["ByReturnDate"]?></label>
                          　<input type="radio" name="periodBy" id="periodBy1" value="1" /> <label for="periodBy1"><?=$Lang["libms"]["batch_edit"]["ByBorrowDate"]?></label>
                          <p>&nbsp;  <?=$i_Profile_From?> <?=$linterface->GET_DATE_PICKER("FromDate",$FromDate)?>
			 <?=$i_Profile_To?> 
			 <?=$linterface->GET_DATE_PICKER("ToDate",$ToDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</p>
							</td>
                        </tr>
                        <col class="field_title" />
                        <col  class="field_c" />
                        <tr>
                          <td class="field_title"><span class="status_alert"><strong>*</strong></span><?=$Lang["libms"]["report"]["target"]?></td>
                          <td>
                          	<input name="radios" type="radio" id="radio6" value="1" onChange="document.getElementById('div_classSelect').style.display='';document.getElementById('div_groupSelect').style.display='none';" <?=($radios =="" || $radios == 1?"checked":"")?> />
                            		<label for="radio6"><?=$Lang["libms"]["report"]["Class"]?></label>
                         
                            <input name="radios" type="radio" id="radio5" value="2" onChange="document.getElementById('div_classSelect').style.display='none';document.getElementById('div_groupSelect').style.display='';" <?=($radios == 2?"checked":"")?> />
                              <label for="radio5"><?=$Lang["libms"]["report"]["findByGroup"]?></label>
                               <br />
                            <div id="div_classSelect" style="<?=($radios == 1 || !$radios?"":"display:none")?>">
                            <span class="row_content">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;</span> <span class="row_content">
                                &nbsp;
                                <?=$ReaderClassSelection?>
                                <?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('readerClass'))"); ?>
                                <br />
                                <span class="tabletextremark"><?=$Lang["libms"]["report"]["PressCtrlKey"]?></span><br />
                              </span></div>
                              
                              <div id="div_groupSelect" style="<?=($radios == 2?"":"display:none")?>">
                            <span class="row_content">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;</span> <span class="row_content">
                                &nbsp;
                                <?=$ReaderGroupSelection?>
                                <?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('readerGroup'))"); ?>
                                <br />
                                <span class="tabletextremark"><?=$Lang["libms"]["report"]["PressCtrlKey"]?></span><br />
                              </span></div>
                              </td>
                        </tr>
                        
                        <tr>
                        	<td class="field_title"><span class="status_alert"><strong>*</strong></span><?=$Lang["libms"]["settings"]["circulation_type"]?></td>
                          	<td><?=get_xxxFilter_html('LIBMS_CIRCULATION_TYPE','CirculationTypeCode', $CirculationTypeCode)?></td>
                        </tr>
                        
                      </table>
       			      <p class="spacer"></p>
   			        </div>
       			    <p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                        <p class="spacer"></p>
                        
                        <?= $linterface->GET_ACTION_BTN($button_search, "button", "submitForm(document.form1);","submit2") ?>
						<p class="spacer"></p>
                      </div>
				      <p>&nbsp;</p></td>
                      <td width="11" >&nbsp;</td>			      
                    </tr>
                    
                  </table>
			  <!--###### Content Board End ######-->
</form><?php
function get_xxxFilter_html($table, $fieldname, $selectedVal=''){
	global $libms,$Lang;

	$fields[] = $fieldname;
	$fields[] = $Lang["libms"]["sql_field"]["Description"];

	$result =  $libms->SELECTFROMTABLE($table,$fields);
	//dump($result);
	$options='';
	// generate selection
	if (!empty($result)){
		$default_sel = 'selected="selected"';
		//$selected ='';
		foreach( $result as $row ){
			$code = $row[$fieldname];
			$name = $row[$Lang["libms"]["sql_field"]["Description"]];
//			if (empty($code)) {
//				$code = "NULL";
//			}
			if ($code == $selectedVal) {
				$selected = 'selected="selected"';
				$default_sel = '';
			}else{
				$selected = '';
			}
			

			$options .= <<<EOL
<option value="{$code}" {$selected} >{$name}</option>
EOL;
		}
	}
	$x = <<<EOL
<select name="{$fieldname}" id="{$fieldname}" >
<option value='' {$default_sel} > -- {$Lang["libms"]["report"]["selectAll"]} -- </option>
	{$options}</select>
EOL;

	return $x;
}

$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
