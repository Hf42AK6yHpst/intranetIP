<?php
/* 
 * Editing by 
 * 
 * Modification Log: 
 * 2013-09-10 (Jason)
 * 		- as it is allowed to input ACNO without alphahat as the prefix of ACNO, remove the checking ACNO prefix
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
####### END  OF   Trim all request #######
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
##################################################################################################################

## Get Data
$UniqueID = (isset($UniqueID) && $UniqueID != '') ? $UniqueID : '';


## Use Library
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


## Main
if(isset($_REQUEST['acno']))//If a username has been submitted 
{
	if(trim($_REQUEST['acno']) == '')
	{
		echo '1';
	} 
	else
	{
		$acno = mysql_real_escape_string($_REQUEST['acno']);//Some clean up :)
		
		$sub_sql  = " ACNO = '".$acno."' ";
		if($UniqueID >0)
		{
			$sub_sql .= " AND UniqueID NOT LIKE '".$UniqueID."' ";
		} else {
		}
		$check_for_acno =  $libms->SELECTVALUE('LIBMS_BOOK_UNIQUE', '*', 'UniqueID', $sub_sql);
		
		if(!empty($check_for_acno))
		{
			echo '1';//If there is a  record match in the Database - Not Available
		} else {
			echo '0';
			/*
			# as it is allowed to input ACNO without alphahat as the prefix of ACNO
			preg_match('/^([a-zA-Z]+)(\d+)$/',$acno,$matches);
			if (!empty($matches))
				echo '0';//No Record Found - record is available
			else
				echo '1';
			*/ 
		}
	}
}

intranet_closedb();
?>