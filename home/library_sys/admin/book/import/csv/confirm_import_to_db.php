<?php
/**
 * NOT in use!!!
 * 
 * Log :
 * Date:	2013-05-02 [Cameron]
 * 			Fix bug for copying "Introduction" and "RemarkInternal"
 * Date:	2013-04-24 [Cameron]
 * 			Fix bug for OpenBorrow and OpenReservation when confirm copying record from LIBMS_CSV_TMP to LIMB_BOOK
 * 			0 -- not allow, 1 -- allow
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
 if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
 {
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
$laccessright = new libaccessright();
$laccessright->NO_ACCESS_RIGHT_REDIRECT();
exit;
}
*/
$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*


global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);
 






# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='../../'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_csv_data.php'");



//===================================================================================

############################################################################################################
function get_next_acno_number($acno = ''){
	global $libms;
	$sql = "select Next_Number from LIBMS_ACNO_LOG where LIBMS_ACNO_LOG.Key =  " . CCToSQL($acno);
	$result = $libms->returnArray($sql);
	if (empty($result)){
		$result[0]['Next_Number'] =0;
		return $result[0]['Next_Number'] ;
	}else{
		return $result[0] ;
	}
}



function set_next_acno_number($prefix='', $number = ''){
	global $libms;
	$sql = "INSERT INTO `LIBMS_ACNO_LOG` (`Key`, `Next_Number`) VALUES ('{$prefix}','{$number}') ON DUPLICATE KEY UPDATE `Next_Number`= '{$number}'";
	
 	$libms->db_db_query($sql);
	return 1;
	
}

function get_next_book_unique(){
	global $libms;
	$result = $libms->returnArray("select max(UniqueID)+1 as UniqueID  from LIBMS_BOOK_UNIQUE");
	return $result[0]['UniqueID'] ;
}

function check_exist_bookCode($given_bookCode){
	global $libms;
	$result = $libms->returnArray("select BookCode from LIBMS_BOOK where BookCode=".CCToSQL($given_bookCode)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

 
function is_new_tag($Description = ''){
	global $libms;
	$result = $libms->returnArray("select TagID from LIBMS_TAG where TagName = ".CCtosql($Description));
	return $result[0]['TagID'] ;
}


function get_LIBMS_TAG($Description = ''){
	global $libms;
	$result = $libms->returnArray("select * from LIBMS_TAG");
	return $result ;
}


function get_LIBMS_BOOK_CATEGORY($Description = ''){
	global $libms;
	$sql = "SELECT
	LIBMS_BOOK_CATEGORY.BookCategoryCode
	FROM
	LIBMS_BOOK_CATEGORY
	where DescriptionEn  like ".CCToSQL($Description. "%")." or DescriptionChi like ".CCToSQL($Description. "%") ;

	if ($Description != ''){
		$result = $libms->returnArray($sql);
		return $result[0]['BookCategoryCode'] ;
	}
	return "";
}


function set_LIBMS_BOOK_CATEGORY($BookCategoryCode, $DescriptionEn, $DescriptionChi){
	global $libms;
	$db_LIBMS_BOOK_CATEGORY = array(
			'BookCategoryCode' => CCToSQL($BookCategoryCode),
			'DescriptionEn' => CCToSQL($DescriptionEn),
			'DescriptionChi' =>  CCToSQL($DescriptionChi)
	);

	$result = $libms->INSERT2TABLE('LIBMS_BOOK_CATEGORY', $db_LIBMS_BOOK_CATEGORY);
	return mysql_insert_id() ;
}



function get_new_Category(){
	global $libms;
	$result = $libms->returnArray("select distinct(f690) from LIBMS_CSV_TMP where f690 != '' and not isnull(f690)");

	$all_category = array();

	foreach($result as $cat){
		$cat_list = explode(',', $cat['f690']);
		$all_category = array_merge($cat_list,$all_category  );
	}
	$all_category = array_unique($all_category);
	return $all_category ;
}

function get_new_Tag(){


}

function get_max_bookCategoryCode(){
	global $libms;
	$result  = $libms->returnArray('SELECT max(LIBMS_BOOK_CATEGORY.BookCategoryCode) +0.001 as code
			from LIBMS_BOOK_CATEGORY
			where LIBMS_BOOK_CATEGORY.BookCategoryCode like "999.%"
			');

	if ($result[0]['code'] == '')
		return "999.001";

	return $result[0]['code'];
}

function set_LIBMS_TAG($TagID,$TagName){
	global $libms;
	$db_LIBMS_TAG = array(
			'TagID' => CCToSQL($TagID),
			'TagName' => CCToSQL($TagName)
	);
	$result = $libms->INSERT2TABLE('LIBMS_TAG', $db_LIBMS_TAG);
	return mysql_insert_id() ;
}


function walk_quote(&$item1)
{
	if (empty($item1)){
		$item1 =  "''";
	}else{
		$item1 = CCTOSQL($item1);
	}
}

function get_left_over_from_tmp(){
	global $libms;
	$result = $libms->returnArray("select count(*) as leftover from LIBMS_CSV_TMP");
	return $result[0]['leftover'];
}

function check_exist_barCode_single($given_barCode){
	global $libms;
	$result = $libms->returnArray("select BarCode from LIBMS_BOOK_UNIQUE where BarCode = ".CCTOSQL($given_barCode)." limit 1 " );



	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}



############################################################################################################


$libel = new elibrary();
if (isset($junior_mck))
{
	$libel->dbConnectionLatin1();	
}

$counter = 0;
while (get_left_over_from_tmp() > 0){
	$error_happen = false;
	$result = $libms->returnArray("select * from LIBMS_CSV_TMP order by id limit 1000 ");
	foreach ($result as $book){
		//dex($book);
		//lookup  $next_acno_number;
		$import_record['BookID'] =   '0' ;

		//if it is auto or EMPTY, do auto generate
		if ((strtoupper($book['BookCode'])=='AUTO')or(trim($book['BookCode'])=='')){

			$next_acno_number = get_next_acno_number('IMPORT');
			$bookCode = 'IMPORT' . $next_acno_number['Next_Number'];
			$import_record['BookCode'] = $bookCode;
			$import_record['ACNO_Prefix'] = 'IMPORT';
			$import_record['ACNO_Num'] = $next_acno_number['Next_Number']++;
			set_next_acno_number('IMPORT', $next_acno_number['Next_Number'] );


		}else{
			//check the given BookCode.
			//$bookCode = 'IMPORT' . $next_acno_number['Next_Number'];
			if (check_exist_bookCode($book['BookCode'])){
				//existing one pop error
				//error happen
				$error_happen=true;
			}else{
					
				preg_match('/^([a-zA-Z]+)(\d+)$/',strtoupper($book['BookCode']),$matches);

				
				if (!empty($matches)){
					$prefix = $matches[1];
					$number = $matches[2];


					
					$import_record['BookCode'] = $book['BookCode'];
					$import_record['ACNO_Prefix'] = $prefix;
					$import_record['ACNO_Num'] = $number;
					//check duplication.
					set_next_acno_number($prefix,  ($number+1) );

				}else{
					$error_happen=true;
					//error happen/
					//ombit record

				}
			}

		}


		$import_record['CallNum'] = $book['CallNum'];

		$import_record['CallNum2'] =  $book['CallNum2'];

		$import_record['ISBN'] =  $book['ISBN'];

		$import_record['ISBN2'] = $book['ISBN2'];

		$import_record['BarCode'] =   $book['barcode1'];

		$book['NoOfCopy'] = intval($book['NoOfCopy']);

		$import_record['NoOfCopy'] =  ($book['NoOfCopy'])?($book['NoOfCopy']):1;

		$import_record['NoOfCopyAvailable'] =  $import_record['NoOfCopy'];

		$import_record['BookTitle'] =   $book['BookTitle'];

		$import_record['BookSubTitle'] =  $book['BookSubTitle'];

		$import_record['Introduction'] =     $book['Introduction'];

		$import_record['Language'] =   $book['Language'];

		$import_record['Country'] =    $book['Country'];

		$import_record['Edition'] =    $book['Edition'];

		$import_record['PublishPlace'] =  $book['PublishPlace'];

		$import_record['Publisher'] =     $book['Publisher'];

		$import_record['PublishYear'] =   $book['PublishYear'];

		$import_record['NoOfPage'] = $book['NoOfPage'];

		$import_record['RemarkInternal'] =  $book['BookInternalremark'];

		$import_record['RemarkToUser'] =   $book['RemarkToUser'];

		$import_record['PurchaseDate'] =   $book['PurchaseDate'];

		$import_record['Distributor'] =  $book['Distributor'];

		$import_record['PurchaseNote'] =   $book['PurchaseNote'];

		$import_record['PurchaseByDepartment'] =  $book['PurchaseByDepartment'];

		$import_record['ListPrice'] = $book['ListPrice'];

		$import_record['Discount'] =  $book['Discount'];

		$import_record['PurchasePrice'] =   $book['PurchasePrice'];

		$import_record['AccountDate'] =  $book['AccountDate'];

		$import_record['CirculationTypeCode'] = $book['BookCirclation'];

		$import_record['ResourcesTypeCode'] = $book['BookResources'];

		$import_record['BookCategoryCode'] = $book['BookCategory'];

		$import_record['eClassBookCode'] =  '';//???

		$import_record['LocationCode'] = $book['BookLocation'];

		$import_record['Subject'] =    $book['Subject'];

		$import_record['Series'] =   $book['Series'];

		$import_record['SeriesNum'] =    $book['SeriesNum'];

		$import_record['URL'] =    $book['URL'];

		$import_record['ResponsibilityCode1'] =  $book['ResponsibilityCode1'];

		$import_record['ResponsibilityBy1'] =  $book['ResponsibilityBy1'];

		$import_record['ResponsibilityCode2'] = $book['ResponsibilityCode2'];

		$import_record['ResponsibilityBy2'] =   $book['ResponsibilityBy2'];

		$import_record['ResponsibilityCode3'] =  $book['ResponsibilityCode3'];

		$import_record['ResponsibilityBy3'] =  $book['ResponsibilityBy3'];

		$import_record['CoverImage'] =  '';

		// 0 -- not allow borrow, 1 -- allow borrow (default)
		$import_record['OpenBorrow'] =    ($book['OpenBorrow'] == '0')?($book['OpenBorrow']):1;

		// 0 -- not allow reserve, 1 -- allow reserve (default)
		$import_record['OpenReservation'] =   ($book['OpenReservation'] == '0')?($book['OpenReservation']):1;

		$import_record['RecordStatus'] = 'NORMAL';


		$db_LIBMS_BOOK = array(  
				
				'BookID' => $import_record['BookID'],
				'BookCode' => $import_record['BookCode'] ,
				
				'ACNO_Prefix' => $import_record['ACNO_Prefix'] ,
				'ACNO_Num' => $import_record['ACNO_Num'],
				
				'CallNum' => $import_record['CallNum'] ,
				'CallNum2' => $import_record['CallNum2'],
				'ISBN' => $import_record['ISBN'],
				'ISBN2' => $import_record['ISBN2'],
				'BarCode' =>  $import_record['BarCode'],
				'NoOfCopy' => $import_record['NoOfCopy'],
				'NoOfCopyAvailable' => $import_record['NoOfCopyAvailable'],
				'BookTitle' => $import_record['BookTitle'],
				'BookSubTitle' => $import_record['BookSubTitle'],
				'Introduction' => $import_record['Introduction'],
				'Language' =>  $import_record['Language'],
				'Country' =>  $import_record['Country'],
				'Edition' => $import_record['Edition'],
				'PublishPlace' =>  $import_record['PublishPlace'],
				'Publisher' =>  $import_record['Publisher'],
				'PublishYear' =>  $import_record['PublishYear'],
				'NoOfPage' =>  $import_record['NoOfPage'],
				'RemarkInternal' => $import_record['RemarkInternal'],
				'RemarkToUser' => $import_record['RemarkToUser'],
				'PurchaseDate' => $import_record['PurchaseDate'],
				'Distributor' =>  $import_record['Distributor'],
				'PurchaseNote' => $import_record['PurchaseNote'],
				'PurchaseByDepartment' => $import_record['PurchaseByDepartment'],
				'ListPrice' => $import_record['ListPrice'],
				'Discount' => $import_record['Discount'],
				'PurchasePrice' => $import_record['PurchasePrice'],
				'AccountDate' => $import_record['AccountDate'],
				'CirculationTypeCode' => $import_record['CirculationTypeCode'],
				'ResourcesTypeCode' => $import_record['ResourcesTypeCode'],
				'BookCategoryCode' =>  $import_record['BookCategoryCode'],
				'eClassBookCode' =>  $import_record['eClassBookCode'],
				'LocationCode' =>  $import_record['LocationCode'],
				'Subject' => $import_record['Subject'],
				'Series' => $import_record['Series'],
				'SeriesNum' => $import_record['SeriesNum'],
				'URL' => $import_record['URL'],
				'ResponsibilityCode1' => $import_record['ResponsibilityCode1'],
				'ResponsibilityBy1' => $import_record['ResponsibilityBy1'],
				'ResponsibilityCode2' => $import_record['ResponsibilityCode2'],
				'ResponsibilityBy2' =>  $import_record['ResponsibilityBy2'],
				'ResponsibilityCode3' => $import_record['ResponsibilityCode3'],
				'ResponsibilityBy3' => $import_record['ResponsibilityBy3'],
				'CoverImage' =>  $import_record['CoverImage'],
				'OpenBorrow' =>  $import_record['OpenBorrow'],
				'OpenReservation' => $import_record['OpenReservation'],
				'RecordStatus' => $import_record['RecordStatus']
		);


		array_walk($db_LIBMS_BOOK, walk_quote );

		if ($error_happen==false){
			$result_insert_LIBMS_BOOK  = $libms->INSERT2TABLE('LIBMS_BOOK', $db_LIBMS_BOOK);

			if ($result_insert_LIBMS_BOOK === false){
				//echo (mysql_error());
			}
			$lastID_LIBMS_BOOK = mysql_insert_id();

			if ( $lastID_LIBMS_BOOK ){

				for ($i=1; $i<=$import_record['NoOfCopy'] ; $i++){

					if ((strtoupper($book['barcode' . $i ]) == 'AUTO')||( empty($book['barcode' . $i ]))){
						//if auto or empty, do auto generate
						$db_LIBMS_BOOK_UNIQUE = array(
								'UniqueID' => CCToSQL('0'),
								'BarCode' => CCToSQL('A+' . (get_next_book_unique() + 1000000)),
								'BookID' => CCToSQL($lastID_LIBMS_BOOK),
								'CreationDate' => 'NOW()' , 
								'RecordStatus' => CCToSQL('NORMAL')
						);
						$result_insert_BOOK_UNIQUE  = $libms->INSERT2TABLE('LIBMS_BOOK_UNIQUE', $db_LIBMS_BOOK_UNIQUE);
							
					}else{
						if (!check_exist_barCode_single($book['barcode'.$i ])){
							//not exist barcode, can insert
							$db_LIBMS_BOOK_UNIQUE = array(
									'UniqueID' => CCToSQL('0'),
									'BarCode' => CCToSQL($book['barcode'.$i ]),
									'BookID' => CCToSQL($lastID_LIBMS_BOOK),
									'RecordStatus' => CCToSQL('NORMAL'),
									'CreationDate' => 'NOW()' ,
							);
							$result_insert_BOOK_UNIQUE  = $libms->INSERT2TABLE('LIBMS_BOOK_UNIQUE', $db_LIBMS_BOOK_UNIQUE);
						}else{
							//do nothing
						}

					}
				}

				//insert Tag if any.
				if ($book['Tags']) {

					$tag_list = explode(',', $book['Tags']);
					$tag_list = array_unique($tag_list);
						
					foreach($tag_list as $aTag){
						$existing_tag = is_new_tag($aTag);

						
						if ($existing_tag){
							$db_LIBMS_BOOK_TAG = array(
									'BookTagID' => cctosql('0'),
									'BookID' =>  cctosql($lastID_LIBMS_BOOK),
									'TagID' => cctosql($existing_tag)
							);
						}else{
							$new_tag_id = set_LIBMS_TAG('0', $aTag);
							$db_LIBMS_BOOK_TAG = array(
									'BookTagID' => cctosql('0'),
									'BookID' =>  cctosql($lastID_LIBMS_BOOK),
									'TagID' => cctosql($new_tag_id)
							);
								
						}

						 
						 $libms->INSERT2TABLE('LIBMS_BOOK_TAG', $db_LIBMS_BOOK_TAG);


					}
				}

			}

			if (($result_insert_LIBMS_BOOK == false) || ($result_insert_BOOK_UNIQUE == false)){
				//false to import, check database.
				//print_r($db_LIBMS_BOOK);
				//print "<br>|<br>";
				//print_r($db_LIBMS_BOOK_UNIQUE);
				//print "<br><hr><br>";
			}else{
				$counter ++;
				$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($lastID_LIBMS_BOOK);
				if (isset($junior_mck))
				{
					$libel->dbConnectionLatin1();	
				}
			}

		}


		$db_LIBMS_BOOK=array();
		$error_happen = false;

	} //for each 1000

	//echo "Parse: " . $counter . <br/>;
	//dex($db_LIBMS_BOOK);
	//$db->query("delete * from LIBMS_CSV_TMP");
	//$result = db2array($rs);
	$libms->db_db_query("delete from `LIBMS_CSV_TMP` order by id limit 1000 ");

} //LOOP until all gone.




$x = $Lang['libms']['import_book']['Result']['Success']. "," . $Lang['libms']['import_book']['total_record'].":".$counter;

$i = 1;






$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center"><br /> <br /> <br /> <?= $x ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5"
					align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><?=$import_button?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();


?>