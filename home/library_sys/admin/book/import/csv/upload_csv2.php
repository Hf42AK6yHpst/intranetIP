<?php

# Using by Henry

/**
 * Log :  	 
 *
 * Note:	use LIBMS_CSV_TMP_2 since 2013-08-30 ; LIBMS_CSV_TMP >> no longer use after 2013-08-30
 * 
 * Date:	2019-06-13 [Henry]
 * 			- add item series number field
 * 
 * Date:	2018-01-04 [Henry]
 * 			- add a preview when teachers import books [Case#F130077]
 * 
 * Date:	2017-10-27 [Cameron]
 * 			- add checking for duplicate ACNO and BarCode in the same lot of import record
 * 			- add checking for scientific numeric format for ACNO and BarCode [case #N129738]
 * 
 * Date:	2017-01-20 [Cameron]
 * 			add checking for following symbols in code fields: !\"#$%&\'()*+,./:;<=>?@[]^`{|}~
 * 	
 * Date:	2015-10-28 [Cameron]
 * 			apply trim_space() function to $insert_to_db to replace Chinese space character (exlcude "Introduction" field)
 * 
 * Date:	2015-10-08 [Cameron]
 * 			add checking of ISBN and ISBN2, show error if these fields are in scientific numeric format
 * 
 * Date:	2013-05-02 [Cameron]
 * 			Fix bug for barcode17
 * 			Change RemarkInternal to Introduction, change its type from varchar(255) to text
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."lang/libms.lang.en.php");	# for getting the item status values
if (is_array($Lang["libms"]["book_status"]) && sizeof($Lang["libms"]["book_status"])>0)
{
	foreach ($Lang["libms"]["book_status"] AS $db_value => $column_data)
	{
		$ItemStatusMapping[strtoupper($column_data)] = $db_value;
	}
}

include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/import/csv/import_common.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];




if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


function GetStatusValueToDB($inputValue)
{
	global $ItemStatusMapping;
	
	$db_value = $ItemStatusMapping[strtoupper($inputValue)];
	
	if ($db_value=="")
	{
		$db_value = "NORMAL";
	}
	
	return $db_value;
}

/*
if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/




$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*


global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);


# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


function check_exist_ACNO($given_bookCode){
	global $libms;
	
	if ($given_bookCode!="" && $given_bookCode!="AUTO")
	{
		$sql = "select ACNO from LIBMS_BOOK_UNIQUE where ACNO=".CCToSQL($given_bookCode)." limit 1";
		$result = $libms->returnArray($sql);
		//error_log(var_export($result, true));
		//return $result[0]['BookCode'] ;
		if (empty($result)){
			return FALSE;
		}else{
			return true;
		}
	} else
	{
		return false;
	}
}

function check_exist_barCode($given_barCode_array){
	global $libms;
	$result = $libms->returnArray("select BarCode from LIBMS_BOOK_UNIQUE where BarCode in ".$given_barCode_array );



	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_ResponsibilityCode($code){  
	global $libms;
	
	if (empty($code)){
		return true;
	}
	
	$result = $libms->returnArray("select ResponsibilityCode from LIBMS_RESPONSIBILITY 
			where ResponsibilityCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_BookCategory($code){
	global $libms;

	if (empty($code)){
		return true;
	}

	$result = $libms->returnArray("select BookCategoryCode from LIBMS_BOOK_CATEGORY
			where BookCategoryCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_BookCirclation($code){
	 
	global $libms;

	if (empty($code)){
		return true;
	}

	$result = $libms->returnArray("select CirculationTypeCode from LIBMS_CIRCULATION_TYPE
			where CirculationTypeCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}


function check_exist_BookResources($code){
	 
	global $libms;

	if (empty($code)){
		return true;
	}

	$result = $libms->returnArray("select ResourcesTypeCode from LIBMS_RESOURCES_TYPE
			where ResourcesTypeCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_BookLocation($code){
 
	global $libms;

	if (empty($code)){
		return true;
	}

	$result = $libms->returnArray("select LocationCode from LIBMS_LOCATION
			where LocationCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

//===================================================================================
$libms->db_db_query("delete from `LIBMS_CSV_TMP_2`");

## ADD PEAR in the INCLUDE path
set_include_path(get_include_path() . PATH_SEPARATOR . $intranet_root .'/home/library_sys/admin/book/import/pear/');

include('../api/class.upload.php');

if ((isset($_POST['actionSimple']) ? $_POST['actionSimple'] : (isset($_GET['actionSimple']) ? $_GET['actionSimple'] : '')) == 'simple') {

	$handle = new Upload($_FILES['file']);
	$ext = strtoupper($handle->file_src_name_ext);

	if($ext != "CSV" && $ext != "TXT")
	{
		$uploadSuccess = false;
		$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];
	}


	if ($handle->uploaded) {
		//$handle->Process($intranet_root . "/home/library_sys/admin/book/import/tmp/");
		$handle->Process($intranet_root."/file/lms_book_import/");
		if ($handle->processed) {
			// everything was fine !
			//$handle->file_dst_name . '">' . $handle->file_dst_name . '</a>';
			//echo 'tmp/'.$handle->file_dst_name;
			//$csvfile = "../tmp/". $handle->file_dst_name;
			$csvfile = $intranet_root."/file/lms_book_import/" . $handle->file_dst_name;
			
			$uploadSuccess = true;
		} else {
			// one error occured
			$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];

			$uploadSuccess = false;
		}

		// we delete the temporary files
		$handle-> Clean();
	}


}

if ($uploadSuccess){

	$file_format = array(
			'BookTitle',
			'BookSubtitle',
			'CallNum',
			'CallNum2',
			'ISBN',
			'ISBN2',
			'Language',
			'Country',
			'Introduction',
			'Edition',
			'PublishYear',
			'Publisher',
			'PublishPlace',
			'Series',
			'SeriesNum',
			'ResponsibilityCode1',
			'ResponsibilityBy1',
			'ResponsibilityCode2',
			'ResponsibilityBy2',
			'ResponsibilityCode3',
			'ResponsibilityBy3',
			'Dimension',
			'ILL',
			'NoOfPage',
			'BookCategoryCode',
			'CirculationTypeCode',
			'ResourcesTypeCode',
			'Subject',
			'BookInternalremark',
			'OpenBorrow',
			'OpenReservation',
			'BookRemarkToUser',
			'Tags',
			'URL',
			'ACNO',
			'barcode',
			'LocationCode',
			'AccountDate',
			'PurchaseDate',
			'PurchasePrice',
			'ListPrice',
			'Discount',
			'Distributor',
			'PurchaseByDepartment',
			'PurchaseNote',
			'InvoiceNumber',
			'AccompanyMaterial', 
			'ItemRemarkToUser',
			'ItemStatus',
			'ItemCirculationTypeCode',
			'ItemSeriesNum'
	);

	$flagAry = array(
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1'
	);
	$format_wrong = false;
	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,"","",$file_format,$flagAry);
	
	$counter = 1;
	$insert_array = array();
	$my_key = array();

	if(is_array($data))
	{
		$col_name = array_shift($data);
	}

	for($i=0; $i<sizeof($file_format); $i++)
	{
		if (strtoupper($col_name[$i])!=strtoupper($file_format[$i]))
		{
			$format_wrong = true;
			break;
		}
	}


	if($format_wrong)
	{
		header("location: import_csv_data2.php?xmsg=wrong_header");
		exit();
	}
	if(sizeof($data)==0)
	{
		header("location: import_csv_data2.php?xmsg=import_no_record");
		exit();
	}


	foreach ($data as $record) {
		//echo $record[3];

		$insert_to_db = array(
				'BookTitle'=> CCTOSQL(trim_space($record['0'])),
				'BookSubTitle'=> CCTOSQL(trim_space($record['1'])),
				'CallNum'=> CCTOSQL(trim_space($record['2'])),
				'CallNum2'=> CCTOSQL(trim_space($record['3'])),
				'ISBN'=> CCTOSQL(trim_space($record['4'])),
				'ISBN2'=> CCTOSQL(trim_space($record['5'])),
				'Language'=> CCTOSQL(trim_space($record['6'])),
				'Country'=> CCTOSQL(trim_space($record['7'])),
				'Introduction'=> CCTOSQL($record['8']),
				'Edition'=> CCTOSQL(trim_space($record['9'])),
				'PublishYear'=> CCTOSQL(trim_space($record['10'])),
				'Publisher'=> CCTOSQL(trim_space($record['11'])),
				'PublishPlace'=> CCTOSQL(trim_space($record['12'])),
				'Series'=> CCTOSQL(trim_space($record['13'])),
				'SeriesNum'=> CCTOSQL(trim_space($record['14'])),
				'ResponsibilityCode1'=> CCTOSQL(trim_space($record['15'])),
				'ResponsibilityBy1'=> CCTOSQL(trim_space($record['16'])),
				'ResponsibilityCode2'=> CCTOSQL(trim_space($record['17'])),
				'ResponsibilityBy2'=> CCTOSQL(trim_space($record['18'])),
				'ResponsibilityCode3'=> CCTOSQL(trim_space($record['19'])),
				'ResponsibilityBy3'=> CCTOSQL(trim_space($record['20'])),
				'Dimension'=> CCTOSQL(trim_space($record['21'])),
				'ILL'=> CCTOSQL(trim_space($record['22'])),
				'NoOfPage'=> CCTOSQL(trim_space($record['23'])),
				'BookCategory'=> CCTOSQL(trim_space($record['24'])),
				'BookCirclation'=> CCTOSQL(trim_space($record['25'])),
				'BookResources'=> CCTOSQL(trim_space($record['26'])),
				'Subject'=> CCTOSQL(trim_space($record['27'])),
				'BookInternalremark'=> CCTOSQL(trim_space($record['28'])),
				'OpenBorrow'=> CCTOSQL(trim_space($record['29'])),
				'OpenReservation'=> CCTOSQL(trim_space($record['30'])),
				'RemarkToUser'=> CCTOSQL(trim_space($record['31'])),
				'Tags'=> CCTOSQL(trim_space($record['32'])),
				'URL'=> CCTOSQL(trim_space($record['33'])),
				'ACNO'=> CCTOSQL(strtoupper(trim_space($record['34']))),
				'barcode'=> CCTOSQL(trim_space($record['35'])),
				'BookLocation'=> CCTOSQL(trim_space($record['36'])),
				'AccountDate'=> CCTOSQL(trim_space($record['37'])),
				'PurchaseDate'=> CCTOSQL(trim_space($record['38'])),
				'PurchasePrice'=> CCTOSQL(trim_space($record['39'])),
				'ListPrice'=> CCTOSQL(trim_space($record['40'])),
				'Discount'=> CCTOSQL(trim_space($record['41'])),
				'Distributor'=> CCTOSQL(trim_space($record['42'])),
				'PurchaseByDepartment'=> CCTOSQL(trim_space($record['43'])),
				'PurchaseNote'=> CCTOSQL(trim_space($record['44'])),
				'InvoiceNumber'=> CCTOSQL(trim_space($record['45'])),
				'AccompanyMaterial'=> CCTOSQL(trim_space($record['46'])),
				'ItemRemarkToUser'=> CCTOSQL(trim_space($record['47'])),
				'RecordStatus'=> CCTOSQL(GetStatusValueToDB(trim_space($record['48']))),
				'ItemCirculationTypeCode'=> CCTOSQL(trim_space($record['49'])),
				'ItemSeriesNum'=> CCTOSQL(trim_space($record['50']))
		);
			
		//dont insert column title and dummy row
		/* if (($insert_to_db['BookTitle'])&&
				($insert_to_db['BookTitle']!= CCTOSQL('BookTitle') )&&
				($insert_to_db['BookSubTitle']!= CCTOSQL('BookSubTitle'))){
		$result = $libms->INSERT2TABLE('LIBMS_CSV_TMP_2', $insert_to_db);
		}
		*/

		$result = $libms->INSERT2TABLE('LIBMS_CSV_TMP_2', $insert_to_db);
		// dex($insert_to_db);

		if ($result==false){
			//capture error
		}

		$insert_to_db = array();
	}
	//print "Processe:" . $counter;
}

//dex($data);

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";

$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][0] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][1] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][2] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][3] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][4] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][5] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>";
$x .= "</tr>";

//row

$sql = "SELECT * FROM `LIBMS_CSV_TMP_2` ";
$result = $libms->returnArray($sql);

$y = 3;
 
$error_occured = 0;
foreach($result as $i=>$record){
	$error = array();
	//check error here
	
	
	if (empty($record['BookTitle'])){
		$error['BookCode'] = $Lang['libms']['import_book']['BookTitleEmpty'];
		$error_occured++;
	}
	
	$isDuplicateACNO = false;
	$record['ACNO'] = trim($record['ACNO']);		
	if (($i>0) && ($record['ACNO']!="" && $record['ACNO']!="AUTO")) {		// check in the same lot of import records
		if (isScientificFormat($record['ACNO'])) {
			$error['ACNO'] = $Lang["libms"]["import_book"]["AncoWrongFormat"];
			$error_occured++;
		}
		else {
			for($j=0;$j<$i;$j++) {
				if ($record['ACNO'] == trim($result[$j]['ACNO'])) {
					$error['ACNO'] = $Lang['libms']['import_book']['BookCodeAlreadyExist'];
					$isDuplicateACNO = true;
					$error_occured++;
					break;				
				}
			}
		}	
	}
	
	if (!$isDuplicateACNO) {		// check in db
		if (check_exist_ACNO($record['ACNO'])){
			$error['ACNO'] = $Lang['libms']['import_book']['BookCodeAlreadyExist'];
			$error_occured++;
		}
	}
	
	$record['barcode'] = trim($record['barcode']);
	if ($record['barcode']!="" && $record['barcode']!="AUTO")
	{
		if ($record['barcode']=="ACNO" && $record['ACNO']!="AUTO")
		{
			$check_duplicate_barcode = "('".addslashes($record['ACNO']) . "')";
			if (check_exist_barCode($check_duplicate_barcode)){
				$error['BarCode'] = $Lang['libms']['import_book']['BarcodeAlreadyExist'];
				$error_occured++;
			}
		} else
		{
			if (isScientificFormat($record['barcode'])) {
				$error['BarCode'] = $Lang["libms"]["import_book"]["BarcodeWrongFormat"];
				$error_occured++;
			}
			else {
				$isDuplicateBarCode = false;
				if ($i>0) {		// check in the same lot of import records
					for($j=0;$j<$i;$j++) {
						if ($record['barcode'] == trim($result[$j]['barcode'])) {
							$error['BarCode'] = $Lang['libms']['import_book']['BarcodeAlreadyExist'];
							$isDuplicateBarCode = true;
							$error_occured++;
							break;				
						}
					}	
				}
				
				if (!$isDuplicateBarCode) {		// check in db
					$check_duplicate_barcode = "('".addslashes($record['barcode']) . "')";
					if (check_exist_barCode($check_duplicate_barcode)){
						$error['BarCode'] = $Lang['libms']['import_book']['BarcodeAlreadyExist'];
						$error_occured++;
					}
				}
			}
		}
	}
	
	if (trim($record['ACNO'])!="" && trim($record['barcode'])=="")
	{
		$error['BarCode'] = $Lang["libms"]["import_book"]["BarcodeMissing"];
		$error_occured++;		
	}

	if (!empty($record['ISBN'])) {
		if (isScientificFormat($record['ISBN'])) {
			$error['ISBN'] = $Lang["libms"]["import_book"]["ISBNWrongFormat"];
			$error_occured++;
		}
	}

	if (!empty($record['ISBN2'])) {
		if (isScientificFormat($record['ISBN2'])) {		
			$error['ISBN2'] = $Lang["libms"]["import_book"]["ISBN2WrongFormat"];
			$error_occured++;
		}
	}

	if (isIncludingSymbol($record['ResponsibilityCode1'])) {
		$error['ResponsibilityCode1'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode1"];
		$error_occured++;
	}

	if (isIncludingSymbol($record['ResponsibilityCode2'])) {
		$error['ResponsibilityCode2'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode2"];
		$error_occured++;
	}

	if (isIncludingSymbol($record['ResponsibilityCode3'])) {
		$error['ResponsibilityCode3'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode3"];
		$error_occured++;
	}
	
	if (isIncludingSymbol($record['BookCategory'])) {
		$error['BookCategory'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["BookCategory"];
		$error_occured++;
	}

	if (isIncludingSymbol($record['BookCirclation'])) {
		$error['BookCirclation'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["BookCirclation"];
		$error_occured++;
	}

	if (isIncludingSymbol($record['BookResources'])) {
		$error['BookResources'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["BookResources"];
		$error_occured++;
	}

	if (isIncludingSymbol($record['BookLocation'])) {
		$error['BookLocation'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["BookLocation"];
		$error_occured++;
	}

	if (isIncludingSymbol($record['ItemCirculationTypeCode'])) {
		$error['ItemCirculationTypeCode'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["ItemCirculationTypeCode"];
		$error_occured++;
	}

	if (isIncludingSymbol($record['Language'])) {
		$error['Language'] = $Lang["libms"]["import_book"]["ExcludeSymbols"]["Language"];
		$error_occured++;
	}

	/* no need to check ACNO format	
	preg_match('/^([a-zA-Z]+)(\d+)$/',strtoupper($record['ACNO']),$matches);
	if ( (strtoupper($record['ACNO'])!='AUTO') && ($record['ACNO']!= '') &&(empty($matches))){
		$error['ACNO'] = $Lang['libms']['import_book']['AncoWrongFormat'];
		$error_occured++;
	}
	*/
	/*
	if (!check_exist_ResponsibilityCode($record['ResponsibilityCode1'])){
		$error['ResponsibilityCode1'] = $Lang['libms']['import_book']['ResponsibilityNotExist'];
		$error_occured++;
	}
	
	if (!check_exist_ResponsibilityCode($record['ResponsibilityCode2'])){
		$error['ResponsibilityCode1'] = $Lang['libms']['import_book']['ResponsibilityNotExist'];
		$error_occured++;
	}
	
	if (!check_exist_ResponsibilityCode($record['ResponsibilityCode3'])){
		$error['ResponsibilityCode1'] = $Lang['libms']['import_book']['ResponsibilityNotExist'];
		$error_occured++;
	}
	

	
	if (!check_exist_BookCategory($record['BookCategory'])){
		$error['BookCategory'] = $Lang['libms']['import_book']['BookCategoryNotExist'];
		$error_occured++;
	}
	
	if (!check_exist_BookCirclation($record['BookCirclation'])){
		$error['BookCirclation'] = $Lang['libms']['import_book']['BookCirclationNotExist'];
		$error_occured++;
	}
	
	if (!check_exist_BookResources($record['BookResources'])){
		$error['BookResources'] = $Lang['libms']['import_book']['BookResourcesNotExist'];
		$error_occured++;
	}
	
 	if (!check_exist_BookLocation($record['BookLocation'])){
		$error['BookLocation'] = $Lang['libms']['import_book']['BookLocationNotExist'];
		$error_occured++;
	}
	*/
	
	$css = (sizeof($error)==0) ? "tabletext":"red";

	if (sizeof($error)>0){
		$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
		$x .= "<td class=\"$css\">".($y)."</td>";
		$x .= "<td class=\"$css\">".$record['BookTitle']."</td>";
		$x .= "<td class=\"$css\">".$record['BookSubTitle']."</td>";
		$x .= "<td class=\"$css\">".$record['ACNO']."</td>";
		$x .= "<td class=\"$css\">".$record['CallNum']."</td>";
		$x .= "<td class=\"$css\">".$record['CallNum2']."</td>";
		$x .= "<td class=\"$css\">".$record['ISBN']."</td>";
		$x .= "<td class=\"$css\">";



		if(sizeof($error)>0)
		{
			foreach($error as $Key=>$Value)
			{
				$x .=$Value.'<br/>';
			}
		}

		$x.="</td>";
		$x .= "</tr>";
	}else{
		 
	}
	$y++;
}

$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data2.php'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data2.php'");
	 
	$prescan_result =  $Lang['libms']['import_book']['upload_success_ready_to_import'].count($result);
	$x = $prescan_result;
	
	if($result){
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
									    <td align="center" style="color:red"><p>'.$Lang["libms"]["import_book"]["ImportMarc21Alert"].'</p></td>
									</tr>				
								</table>';
		for($n=0;$n<3;$n++){
			$BookInfoArr = $result[$n];
			if(!$BookInfoArr){
				break;
			}
			if($n==0)$x .="<p>".$Lang["libms"]["import_book"]["FirstBook"]."</p>";	
			if($n==1)$x .="<p>".$Lang["libms"]["import_book"]["SecondBook"]."</p>";	
			if($n==2)$x .="<p>".$Lang["libms"]["import_book"]["ThirdBook"]."</p>";	
			
			$x .= "<table class='common_table_list_v30 view_table_list_v30' style='width:700px;'>";
			
			$x .= "<tr><th COLSPAN='2'>". $Lang["libms"]["report"]["bookinfo"]."</th></tr>";
			$x .= "<tr><td style='width:30%'>".$Lang["libms"]["book"]["title"]."</td><td style='width:70%;'>".$BookInfoArr["BookTitle"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["subtitle"]."</td><td>".$BookInfoArr["BookSubtitle"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["call_number"]."</td><td>".$BookInfoArr["CallNum"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["call_number2"]."</td><td>".$BookInfoArr["CallNum2"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["ISBN"]."</td><td>".$BookInfoArr["ISBN"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["ISBN2"]."</td><td>".$BookInfoArr["ISBN2"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["language"]."</td><td>".$BookInfoArr["Language"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["country"]."</td><td>".$BookInfoArr["Country"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["introduction"]."</td><td>".$BookInfoArr["Introduction"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["edition"]."</td><td>".$BookInfoArr["Edition"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["publish_year"]."</td><td>".$BookInfoArr["PublishYear"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["publisher"]."</td><td>".$BookInfoArr["Publisher"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["publish_place"]."</td><td>".$BookInfoArr["PublishPlace"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["series"]."</td><td>".$BookInfoArr["Series"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["series_number"]."</td><td>".$BookInfoArr["SeriesNum"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_code1"]."</td><td>".$BookInfoArr["ResponsibilityCode1"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_by1"]."</td><td>".$BookInfoArr["ResponsibilityBy1"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_code2"]."</td><td>".$BookInfoArr["ResponsibilityCode2"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_by2"]."</td><td>".$BookInfoArr["ResponsibilityBy2"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_code3"]."</td><td>".$BookInfoArr["ResponsibilityCode3"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_by3"]."</td><td>".$BookInfoArr["ResponsibilityBy3"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["DIMEN"]."</td><td>".$BookInfoArr["Dimension"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["ILL"]."</td><td>".$BookInfoArr["ILL"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["number_of_page"]."</td><td>".$BookInfoArr["NoOfPage"]."</td></tr>";
			$x .= "<tr><td>".$Lang['libms']['book']['select']['BookCategoryCode']."</td><td>".$BookInfoArr["BookCategory"]."</td></tr>";
			$x .= "<tr><td>".$Lang['libms']['book']['select']['CirculationTypeCode']."</td><td>".$BookInfoArr["BookCirclation"]."</td></tr>";
			$x .= "<tr><td>".$Lang['libms']['book']['select']['ResourcesTypeCode']."</td><td>".$BookInfoArr["BookResources"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["subject"]."</td><td>".$BookInfoArr["Subject"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["remark_internal"]."</td><td>".$BookInfoArr["BookInternalremark"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["batch_edit"]["AllowBorrow"]."</td><td>".$BookInfoArr["OpenBorrow"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["batch_edit"]["AllowReserve"]."</td><td>".$BookInfoArr["OpenReservation"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["remark_to_user"]."</td><td>".$BookInfoArr["RemarkToUser"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["tags"]."</td><td>".$BookInfoArr["Tags"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["URL"]."</td><td>".$BookInfoArr["URL"]."</td></tr>";
			
			$x .= "<tr><th COLSPAN='2'>". $Lang["libms"]["book"]["iteminfo"]."</th></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["code"]."</td><td>".$BookInfoArr["ACNO"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["barcode"]."</td><td>".$BookInfoArr["barcode"]."</td></tr>";   
			$x .= "<tr><td>".$Lang["libms"]["label"]["LocationCode"]."</td><td>".$BookInfoArr[" BookLocation"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["account_date"]."</td><td>".$BookInfoArr["AccountDate"]."</td></tr>"; 
			$x .= "<tr><td>".$Lang["libms"]["book"]["purchase_date"]."</td><td>".$BookInfoArr["PurchaseDate"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["purchase_price"]."</td><td>".$BookInfoArr["PurchasePrice"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["list_price"]."</td><td>".$BookInfoArr["ListPrice"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["discount"]."</td><td>".$BookInfoArr["Discount"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["distributor"]."</td><td>".$BookInfoArr["Distributor"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["purchase_by_department"]."</td><td>".$BookInfoArr["PurchaseByDepartment"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["purchase_note"]."</td><td>".$BookInfoArr["PurchaseNote"]."</td></tr>";
			$x .= "<tr><td>".$Lang['libms']['book']['invoice_number']."</td><td>".$BookInfoArr["InvoiceNumber"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["AccompanyMaterial"]."</td><td>".$BookInfoArr["AccompanyMaterial"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["item_remark_to_user"]."</td><td>".$BookInfoArr["ItemRemarkToUser"]."</td></tr>";
			$RecordStatus = $BookInfoArr['RecordStatus'];
			$x .= "<tr><td>".$Lang['libms']['book']['select']['Book_Status']."</td><td>".$Lang["libms"]["book_status"]["$RecordStatus"]."</td></tr>";
			$x .= "<tr><td>".$Lang['libms']['book']['select']['CirculationTypeCode']."</td><td>".$BookInfoArr["ItemCirculationTypeCode"]."</td></tr>";
			$x .= "<tr><td>".$Lang["libms"]["book"]["item_series_number"]."</td><td>".$BookInfoArr["ItemSeriesNum"]."</td></tr>";
			
			$x .= "</table><br>";
		}
	}
}

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" action="confirm_import_to_db2.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><?=$x?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $import_button ?>
			</td>
		</tr>
	</table>
</form>
<br />
<?php

if (file_exists($csvfile))
{
	unlink($csvfile);
}


$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();


?>