<?php
$PATH_WRT_ROOT = "../../../../../../";
	
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
	
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
	
?>
<script language="javascript">

function jsCancel()
{
	if (confirm("<?=$Lang["libms"]["import_book"]["CancelCheck"]?>"))
	{
		parent.location.reload();	
	}		
}

function jsClose()
{
    document.formAJ.action="confirm_import_to_db_marc21_new.php";
	document.formAJ.method="post";
	document.formAJ.submit();
}

function jsMarcInsert(book,line) {
  $.ajax({
    url: 'ajax_marc_insert.php?file=<?=$file?>&book='+book+'&line='+line, 
    success: function(response) { 
      rData = response.split("|");
      curBook = rData[0];
      curLine = rData[1];
      TotalLine = rData[2];
      alert_exist_acno = rData[3];
      alert_exist_barcode = rData[4];
      if (TotalLine>0)
      {
	      ProgressPercentage = Math.round(100*(curLine/TotalLine));
	      $('#progressbarA').progressBar(ProgressPercentage);
      }
      $('#ProgressMessage').html("<strong><?=$Lang["libms"]["import_book"]["AlreadyChecked"]?>"+curBook+"</strong>");
	  if (curLine/TotalLine!=1)
	  {
	  	jsMarcInsert(curBook,curLine);
	  }
	  else
	  {
	  	//jump to confirm_import_to_db_marc21.php
	  	
	  	  var div = document.getElementById("div1");	
      	  var input1 = document.createElement("input");
     	  input1.type = "hidden";
     	  input1.name = "alert_exist_acno";
	      input1.value = alert_exist_acno;
	      div.appendChild(input1);	
	      
	      var input2 = document.createElement("input");
	      input2.type = "hidden";
          input2.name = "alert_exist_barcode";
          input2.value = alert_exist_barcode;
	      div.appendChild(input2);	
	      	  	      
	  	  $('#ActionButtonCancel').hide();
	  	  $('#ActionButtonClose').show();
	  	}


    }
  });
}


$(container).ready(function() {
  jsMarcInsert(0,0);
});

</script>

<form name="formAJ">
<script language="JavaScript" src="/templates/jquery/jquery.progressbar.min.js"></script> 
<script>
	$(document).ready(function() {
		$("#progressbarA").progressBar({barImage: '/images/progress_bar_green.gif'} );
	});
</script>
<style>
	#container { width: 80%; margin-left: 10%; margin-top: 30px;}
</style>
<div id="container" align="center">
	<div style="padding-bottom: 25px;">
		<div id="ProgressMessage"><?=$Lang["libms"]["import_book"]["StartToChecking"]?></div>
		<table align="center">
			<tr><td width="240" align="center"><span class="progressBar" id="progressbarA">0%</span></td></tr>
		</table>
		<p id="ActionButtonCancel"><?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:jsCancel()")?></p>	
		<p id="ActionButtonClose" style="display:none"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "javascript:jsClose()")?></p>
	</div>
</div>
<div id="div1">
</div>
</form>
<?
	intranet_closedb();
?>