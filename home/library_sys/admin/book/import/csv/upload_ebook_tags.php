<?php
/**
 * Log :	 
 *
 * edit in utf8
 *
 * Purpose:	read csv file that contains Book Title vs Book Tags, then insert it into INTRANET_ELIB_BOOK_TAG and INTRANET_ELIB_TAG
 * 			case #M83167
 * 
 * !!! Note:	the provided file format must have two rows of header (the 1st is English Column Title, 2nd is Chinese)
 * 
 * Date:	2016-09-23 [Cameron]
 * 			- show error if books not found
 * 			- call getBookIDByBookTitle for Publish book only
 * 
 * Date:	2015-10-07 [Cameron]
 * 			- create this file
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$LibeLib = new elibrary();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_list'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

# Top menu highlight setting
$TAGS_OBJ[] = array($Lang["libms"]["transfer_book"]["book_transfer_format"]);

# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

//===================================================================================

## step 1: save uploaded file
## ADD PEAR in the INCLUDE path
set_include_path(get_include_path() . PATH_SEPARATOR . $intranet_root .'/home/library_sys/admin/book/import/pear/');
include('../api/class.upload.php');

	$handle = new Upload($_FILES['file']);
	$ext = strtoupper($handle->file_src_name_ext);

	if($ext != "CSV" && $ext != "TXT")
	{
		$uploadSuccess = false;
		$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];
	}

	if ($handle->uploaded) {
		$handle->Process($intranet_root."/file/lms_book_import/");
		if ($handle->processed) {
			// everything was fine !
			$csvfile = $intranet_root."/file/lms_book_import/" . $handle->file_dst_name;
			$uploadSuccess = true;
		} else {
			// one error occured
			$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];
			$uploadSuccess = false;
		}
		// we delete the temporary files
		$handle-> Clean();
	}

## step 2: read content from csv file and add / update it into database

$nrImport = 0;

if ($uploadSuccess){
	$file_format = array(
		'Number',
		'BookTitle',
		'Author',
		'Publisher',
		'Language',
		'Tags'
	);
			
//	$file_format = array(
//		'#',
//		'書名',
//		'著 / 作者',
//		'出版商',
//		'語言',
//		'標籤'
//	);

	// 1 will read the column, 0 will ignore
	$flagAry = array(
			'0',
			'1',
			'0',
			'0',
			'0',
			'1'
	);
	$format_wrong = false;
	
	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,"","",$file_format,$flagAry);
	
	$counter = 1;
	$insert_array = array();
	$my_key = array();

	if(is_array($data))
	{
		$col_name = array_shift($data);
	}
	$col_name[0] = empty($col_name[0]) ? 'BookTitle' : $col_name[0];
	$file_format[0] = $col_name[0];
	  
	$j = 0;
	for($i=0, $iMax=count($file_format); $i<$iMax; $i++)
	{
		if ($flagAry[$i] <> '0') {		// compare for flaged fields only
			if (strtoupper($col_name[$j])!=strtoupper($file_format[$i]))
			{
				$format_wrong = true;
				break;
			}
			$j++;
		}
	}

	if($format_wrong)
	{
		header("location: import_ebook_tags.php?xmsg=wrong_header");
		exit();
	}
	if(sizeof($data)==0)
	{
		header("location: import_ebook_tags.php?xmsg=import_no_record");
		exit();
	}

	$ret = array();
	$i=0;
	$nrErrorRow = 0;
	$x = '';
	foreach ($data as $record) {
		$bookTitle = trim($record[0]);
		$tagName = trim($record[1]);
		
		$tagID_array = $LibeLib->returnTagIDByTagName($tagName);
		$bookID = $LibeLib->getBookIDByBookTitle($bookTitle,1);	// Publish book only
		
		if ($bookID) {		
			foreach((array)$tagID_array as $tagID) {
				if (!$LibeLib->isBookTagExist($bookID,$tagID)) {
					$result = $LibeLib->AssignTagToBooks($tagID, array($bookID));
					$ret[] = $result;
					if ($result) {
						$nrImport++;	
					}
					else {
						$x .= "<tr class=\"tablebluerow".($nrErrorRow%2+1)."\">";
						$x .= "<td class=\"red\">".++$nrErrorRow."</td>";
						$x .= "<td class=\"red\">".$bookTitle."</td>";
						$x .= "<td class=\"red\">".$tagName."</td>";
						$x .= "<td class=\"red\">".$Lang['libms']['bookmanagement']['import_ebook_tags_error_add']."</td>";
						$x .= "</tr>";
						$nrErrorRow++;
					}
				}
			}
		}
		else {
			$ret[] = false;
			$x .= "<tr class=\"tablebluerow".($nrErrorRow%2+1)."\">";
			$x .= "<td class=\"red\">".++$nrErrorRow."</td>";
			$x .= "<td class=\"red\">".$bookTitle."</td>";
			$x .= "<td class=\"red\">".$tagName."</td>";
			$x .= "<td class=\"red\">".$Lang['libms']['bookmanagement']['import_ebook_tags_error_no_book']."</td>";
			$x .= "</tr>";
		}
	}

	
	if (in_array(false,$ret)) {
		$y = $Lang['General']['Error']."<br>";
		$y .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\">";
		$y .= "<tr>";
		$y .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";		
		$y .= "<td class=\"tablebluetop tabletopnolink\">". $file_format[1] ."</td>";
		$y .= "<td class=\"tablebluetop tabletopnolink\">". $file_format[5] ."</td>";
		$y .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Error'] ."</td>";
		$y .= "</tr>";
		$x = $y.$x;
		$x .= "</table>";
	}
}

//if ($x == '') {
	$x = $Lang['libms']['import_book']['Result']['Success']. "," . $Lang['libms']['import_book']['total_record'].":".$nrImport . '<br>'.$x;
//}

$action_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_ebook_tags.php'");

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><?=$x?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $action_button ?>
			</td>
		</tr>
	</table>
</form>
<br />
<?php

if (file_exists($csvfile))
{
	unlink($csvfile);
}


$linterface->LAYOUT_STOP();
intranet_closedb();


?>