<?php
/**
 * Log :
 * Date:	2013-05-02 [Cameron]
 * 			Fix bug for barcode17
 * 			Change RemarkInternal to Introduction, change its type from varchar(255) to text
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/




$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*


global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);
 






# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


function check_exist_bookCode($given_bookCode){
	global $libms;
	$result = $libms->returnArray("select BookCode from LIBMS_BOOK where BookCode=".CCToSQL($given_bookCode)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_barCode($given_barCode_array){
	global $libms;
	$result = $libms->returnArray("select BarCode from LIBMS_BOOK_UNIQUE where BarCode in ".$given_barCode_array );



	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_ResponsibilityCode($code){  
	global $libms;
	
	if (empty($code)){
		return true;
	}
	
	$result = $libms->returnArray("select ResponsibilityCode from LIBMS_RESPONSIBILITY 
			where ResponsibilityCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_BookCategory($code){
	global $libms;

	if (empty($code)){
		return true;
	}

	$result = $libms->returnArray("select BookCategoryCode from LIBMS_BOOK_CATEGORY
			where BookCategoryCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_BookCirclation($code){
	 
	global $libms;

	if (empty($code)){
		return true;
	}

	$result = $libms->returnArray("select CirculationTypeCode from LIBMS_CIRCULATION_TYPE
			where CirculationTypeCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}


function check_exist_BookResources($code){
	 
	global $libms;

	if (empty($code)){
		return true;
	}

	$result = $libms->returnArray("select ResourcesTypeCode from LIBMS_RESOURCES_TYPE
			where ResourcesTypeCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_BookLocation($code){
 
	global $libms;

	if (empty($code)){
		return true;
	}

	$result = $libms->returnArray("select LocationCode from LIBMS_LOCATION
			where LocationCode=".CCToSQL($code)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

//===================================================================================
$libms->db_db_query("delete from `LIBMS_CSV_TMP`");


## ADD PEAR in the INCLUDE path
set_include_path(get_include_path() . PATH_SEPARATOR . $intranet_root .'/home/library_sys/admin/book/import/pear/');

include('../api/class.upload.php');

if ((isset($_POST['action']) ? $_POST['action'] : (isset($_GET['action']) ? $_GET['action'] : '')) == 'simple') {

	$handle = new Upload($_FILES['file']);
	$ext = strtoupper($handle->file_src_name_ext);

	if($ext != "CSV" && $ext != "TXT")
	{
		$uploadSuccess = false;
		$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];
	}


	if ($handle->uploaded) {
		//$handle->Process($intranet_root . "/home/library_sys/admin/book/import/tmp/");
		$handle->Process($intranet_root."/file/lms_book_import/");
		if ($handle->processed) {
			// everything was fine !
			//$handle->file_dst_name . '">' . $handle->file_dst_name . '</a>';
			//echo 'tmp/'.$handle->file_dst_name;
			//$csvfile = "../tmp/". $handle->file_dst_name;
			$csvfile = $intranet_root."/file/lms_book_import/" . $handle->file_dst_name;
			
			$uploadSuccess = true;
		} else {
			// one error occured
			$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];

			$uploadSuccess = false;
		}

		// we delete the temporary files
		$handle-> Clean();
	}


}

if ($uploadSuccess){

	$file_format = array(
			'BookTitle',
			'BookSubTitle',
			'BookCode',
			'CallNum',
			'CallNum2',
			'ISBN',
			'ISBN2',
			'Language',
			'Country',
			'RemarkInternal',
			'Edition',
			'PublishYear',
			'Publisher',
			'PublishPlace',
			'Series',
			'SeriesNum',
			'ResponsibilityCode1',
			'ResponsibilityBy1',
			'ResponsibilityCode2',
			'ResponsibilityBy2',
			'ResponsibilityCode3',
			'ResponsibilityBy3',
			'NoOfPage',
			'BookCategory',
			'BookCirclation',
			'BookResources',
			'BookLocation',
			'Subject',
			'AccountDate',
			'BookInternalremark',
			'PurchaseDate',
			'PurchasePrice',
			'ListPrice',
			'Discount',
			'Distributor',
			'PurchaseByDepartment',
			'PurchaseNote',
			'NoOfCopy',
			'OpenBorrow',
			'OpenReservation',
			'RemarkToUser',
			'Tags',
			'URL',
			'barcode1',
			'barcode2',
			'barcode3',
			'barcode4',
			'barcode5',
			'barcode6',
			'barcode7',
			'barcode8',
			'barcode9',
			'barcode10',
			'barcode11',
			'barcode12',
			'barcode13',
			'barcode14',
			'barcode15',
			'barcode16',
			'barcode17',
			'barcode18',
			'barcode19',
			'barcode20'
	);

	$flagAry = array(
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1'
	);
	$format_wrong = false;
	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,"","",$file_format,$flagAry);

	$counter = 1;
	$insert_array = array();
	$my_key = array();

	if(is_array($data))
	{
		$col_name = array_shift($data);
	}

	for($i=0; $i<sizeof($file_format); $i++)
	{
		if ($col_name[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}


	if($format_wrong)
	{
		header("location: import_csv_data.php?xmsg=wrong_header");
		exit();
	}
	if(sizeof($data)==0)
	{
		header("location: import_csv_data.php?xmsg=import_no_record");
		exit();
	}


	foreach ($data as $record) {
		//echo $record[3];

		$insert_to_db = array(
				'BookTitle'=> CCTOSQL($record['0']),
				'BookSubTitle'=> CCTOSQL($record['1']),
				'BookCode'=> CCTOSQL(strtoupper($record['2'])),
				'CallNum'=> CCTOSQL($record['3']),
				'CallNum2'=> CCTOSQL($record['4']),
				'ISBN'=> CCTOSQL($record['5']),
				'ISBN2'=> CCTOSQL($record['6']),
				'Language'=> CCTOSQL($record['7']),
				'Country'=> CCTOSQL($record['8']),
				'Introduction'=> CCTOSQL($record['9']),
				'Edition'=> CCTOSQL($record['10']),
				'PublishYear'=> CCTOSQL($record['11']),
				'Publisher'=> CCTOSQL($record['12']),
				'PublishPlace'=> CCTOSQL($record['13']),
				'Series'=> CCTOSQL($record['14']),
				'SeriesNum'=> CCTOSQL($record['15']),
				'ResponsibilityCode1'=> CCTOSQL($record['16']),
				'ResponsibilityBy1'=> CCTOSQL($record['17']),
				'ResponsibilityCode2'=> CCTOSQL($record['18']),
				'ResponsibilityBy2'=> CCTOSQL($record['19']),
				'ResponsibilityCode3'=> CCTOSQL($record['20']),
				'ResponsibilityBy3'=> CCTOSQL($record['21']),
				'NoOfPage'=> CCTOSQL($record['22']),
				'BookCategory'=> CCTOSQL($record['23']),
				'BookCirclation'=> CCTOSQL($record['24']),
				'BookResources'=> CCTOSQL($record['25']),
				'BookLocation'=> CCTOSQL($record['26']),
				'Subject'=> CCTOSQL($record['27']),
				'AccountDate'=> CCTOSQL($record['28']),
				'BookInternalremark'=> CCTOSQL($record['29']),
				'PurchaseDate'=> CCTOSQL($record['30']),
				'PurchasePrice'=> CCTOSQL($record['31']),
				'ListPrice'=> CCTOSQL($record['32']),
				'Discount'=> CCTOSQL($record['33']),
				'Distributor'=> CCTOSQL($record['34']),
				'PurchaseByDepartment'=> CCTOSQL($record['35']),
				'PurchaseNote'=> CCTOSQL($record['36']),
				'NoOfCopy'=> CCTOSQL($record['37']),
				'OpenBorrow'=> CCTOSQL($record['38']),
				'OpenReservation'=> CCTOSQL($record['39']),
				'RemarkToUser'=> CCTOSQL($record['40']),
				'Tags'=> CCTOSQL($record['41']),
				'URL'=> CCTOSQL($record['42']),
				'barcode1'=> CCTOSQL($record['43']),
				'barcode2'=> CCTOSQL($record['44']),
				'barcode3'=> CCTOSQL($record['45']),
				'barcode4'=> CCTOSQL($record['46']),
				'barcode5'=> CCTOSQL($record['47']),
				'barcode6'=> CCTOSQL($record['48']),
				'barcode7'=> CCTOSQL($record['49']),
				'barcode8'=> CCTOSQL($record['50']),
				'barcode9'=> CCTOSQL($record['51']),
				'barcode10'=> CCTOSQL($record['52']),
				'barcode11'=> CCTOSQL($record['53']),
				'barcode12'=> CCTOSQL($record['54']),
				'barcode13'=> CCTOSQL($record['55']),
				'barcode14'=> CCTOSQL($record['56']),
				'barcode15'=> CCTOSQL($record['57']),
				'barcode16'=> CCTOSQL($record['58']),
				'barcode17'=> CCTOSQL($record['59']),
				'barcode18'=> CCTOSQL($record['60']),
				'barcode19'=> CCTOSQL($record['61']),
				'barcode20'=> CCTOSQL($record['62'])
		);
			
		//dont insert column title and dummy row
		/* if (($insert_to_db['BookTitle'])&&
				($insert_to_db['BookTitle']!= CCTOSQL('BookTitle') )&&
				($insert_to_db['BookSubTitle']!= CCTOSQL('BookSubTitle'))){
		$result = $libms->INSERT2TABLE('LIBMS_CSV_TMP', $insert_to_db);
		}
		*/

		$result = $libms->INSERT2TABLE('LIBMS_CSV_TMP', $insert_to_db);
			
		// dex($insert_to_db);

		if ($result==false){
			//capture error
		}

		$insert_to_db = array();
	}
	//print "Processe:" . $counter;
}

//dex($data);

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";

$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][0] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][1] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][2] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][3] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][4] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['libms']['import_book']['ImportCSVDataCol_New'][5] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>";
$x .= "</tr>";

//row

$sql = "SELECT BookTitle, BookSubTitle, BookCode, CallNum, CallNum2, ISBN,
		ResponsibilityCode1,
		ResponsibilityCode2,
		ResponsibilityCode3,
		BookCategory,
		BookCirclation,
		BookResources,
		BookLocation,
		barcode1,
		barcode2,
		barcode3,
		barcode4,
		barcode5,
		barcode6,
		barcode7,
		barcode8,
		barcode9,
		barcode10,
		barcode11,
		barcode12,
		barcode13,
		barcode14,
		barcode15,
		barcode16,
		barcode17,
		barcode18,
		barcode19,
		barcode20
		FROM `LIBMS_CSV_TMP` ";
$result = $libms->returnArray($sql);


$y = 3;
 
$error_occured = 0;
foreach($result as $record){
	$error = array();
	//check error here
	
	
	if (empty($record['BookTitle'])){
		$error['BookCode'] = $Lang['libms']['import_book']['BookTitleEmpty'];
		$error_occured++;
	}
	
	
	if (check_exist_bookCode($record['BookCode'])){
		$error['BookCode'] = $Lang['libms']['import_book']['BookCodeAlreadyExist'];
		$error_occured++;
	}

	$check_duplicate_barcode = "(";

	for ($i=1; $i <= 20; $i ++){
		if ((strtoupper($record['barcode'. $i])!='AUTO') && ($record['barcode' . $i]!= '')){
			$check_duplicate_barcode .=  CCtoSQL($record['barcode' . $i]).  ",";
		}
	}

	$check_duplicate_barcode=rtrim($check_duplicate_barcode, ",");
	$check_duplicate_barcode .= ")";


	if (check_exist_barCode($check_duplicate_barcode)){
		$error['BarCode'] = $Lang['libms']['import_book']['BarcodeAlreadyExist'];
		$error_occured++;
	}


	
	preg_match('/^([a-zA-Z]+)(\d+)$/',strtoupper($record['BookCode']),$matches);
	if ( (strtoupper($record['BookCode'])!='AUTO') && ($record['BookCode']!= '') &&(empty($matches))){
		$error['BookCode'] = $Lang['libms']['import_book']['AncoWrongFormat'];
		$error_occured++;
	}
	
	if (!check_exist_ResponsibilityCode($record['ResponsibilityCode1'])){
		$error['ResponsibilityCode1'] = $Lang['libms']['import_book']['ResponsibilityNotExist'];
		$error_occured++;
	}
	
	if (!check_exist_ResponsibilityCode($record['ResponsibilityCode2'])){
		$error['ResponsibilityCode1'] = $Lang['libms']['import_book']['ResponsibilityNotExist'];
		$error_occured++;
	}
	
	if (!check_exist_ResponsibilityCode($record['ResponsibilityCode3'])){
		$error['ResponsibilityCode1'] = $Lang['libms']['import_book']['ResponsibilityNotExist'];
		$error_occured++;
	}
	

	
	if (!check_exist_BookCategory($record['BookCategory'])){
		$error['BookCategory'] = $Lang['libms']['import_book']['BookCategoryNotExist'];
		$error_occured++;
	}
	
	if (!check_exist_BookCirclation($record['BookCirclation'])){
		$error['BookCirclation'] = $Lang['libms']['import_book']['BookCirclationNotExist'];
		$error_occured++;
	}
	
	if (!check_exist_BookResources($record['BookResources'])){
		$error['BookResources'] = $Lang['libms']['import_book']['BookResourcesNotExist'];
		$error_occured++;
	}
	
 	if (!check_exist_BookLocation($record['BookLocation'])){
		$error['BookLocation'] = $Lang['libms']['import_book']['BookLocationNotExist'];
		$error_occured++;
	}
	
	
	$css = (sizeof($error)==0) ? "tabletext":"red";

	if (sizeof($error)>0){
		$x .= "<tr class=\"tablebluerow".($y%2+1)."\">";
		$x .= "<td class=\"$css\">".($y)."</td>";
		$x .= "<td class=\"$css\">".$record['BookTitle']."</td>";
		$x .= "<td class=\"$css\">".$record['BookSubTitle']."</td>";
		$x .= "<td class=\"$css\">".$record['BookCode']."</td>";
		$x .= "<td class=\"$css\">".$record['CallNum']."</td>";
		$x .= "<td class=\"$css\">".$record['CallNum2']."</td>";
		$x .= "<td class=\"$css\">".$record['ISBN']."</td>";
		$x .= "<td class=\"$css\">";



		if(sizeof($error)>0)
		{
			foreach($error as $Key=>$Value)
			{
				$x .=$Value.'<br/>';
			}
		}

		$x.="</td>";
		$x .= "</tr>";
	}else{
		 
	}
	$y++;
}

$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data.php'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data.php'");
	 
	$prescan_result =  $Lang['libms']['import_book']['upload_success_ready_to_import'].count($result);
	$x = $prescan_result;
}


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" action="confirm_import_to_db.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><?=$x?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $import_button ?>
			</td>
		</tr>
	</table>
</form>
<br /><?php
 unlink($csvfile);


$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();


?>
