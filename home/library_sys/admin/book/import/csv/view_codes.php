<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

### set cookies
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementBookList";
 

$linterface = new interface_html("libms.html");
  
//$linterface = new interface_html();




$libms->MODULE_AUTHENTICATION($CurrentPage);


//$linterface->LAYOUT_START();

switch ($CodeType)
{
	case 1:
			$sql = "SELECT 
						`ResponsibilityCode`, `DescriptionEn`,`DescriptionChi`
					FROM 
						LIBMS_RESPONSIBILITY ORDER BY ResponsibilityCode ASC ";
			break;
	case 2:
			$sql = "SELECT 
						`BookCategoryCode`, `DescriptionEn`,`DescriptionChi`
					FROM 
						LIBMS_BOOK_CATEGORY ORDER BY BookCategoryCode ASC ";
			break;
	case 3:
			$sql = "SELECT 
						`CirculationTypeCode`, `DescriptionEn`,`DescriptionChi`
					FROM 
						LIBMS_CIRCULATION_TYPE ORDER BY CirculationTypeCode ASC ";
			break;
	case 4:
			$sql = "SELECT 
						`ResourcesTypeCode`, `DescriptionEn`,`DescriptionChi`
					FROM 
						LIBMS_RESOURCES_TYPE ORDER BY ResourcesTypeCode ASC ";
			break;
	case 5:
			$sql = "SELECT 
						`LocationCode`, `DescriptionEn`,`DescriptionChi`
					FROM 
						LIBMS_LOCATION ORDER BY LocationCode ASC ";
			break;
	case 6:
			
			if ($intranet_session_language=="en")
			{
				$book_status["en"] = $Lang["libms"]["book_status"];
				include_once($PATH_WRT_ROOT."lang/libms.lang.b5.php");	# for getting the item status values
				$book_status["b5"] = $Lang["libms"]["book_status"];
				include($PATH_WRT_ROOT."lang/libms.lang.en.php");
			} else
			{
				
				$book_status["b5"] = $Lang["libms"]["book_status"];
				include_once($PATH_WRT_ROOT."lang/libms.lang.en.php");	# for getting the item status values
				$book_status["en"] = $Lang["libms"]["book_status"];
				include($PATH_WRT_ROOT."lang/libms.lang.b5.php");
			}
			
			if (is_array($book_status["en"]) && sizeof($book_status["en"])>0)
			{
				foreach ($book_status["en"] as $CurKey => $CurValue)
				{
					$Rows[] = array(strtoupper($CurValue), $CurValue, $book_status["b5"][$CurKey]);
				}
			}
			break;
}

$table_contents = "";
if ($sql!="")
{
	$Rows = $libms->returnArray($sql);
}


for ($i=0; $i<sizeof($Rows); $i++)
{
	$tr_class = ($i%2==0) ? 'class="row_waiting"' : '';
	$table_contents .= "<tr {$tr_class} ><td>".($i+1)."</td><td>".$Rows[$i][0]."</td><td>".$Rows[$i][1]."</td><td>".$Rows[$i][2]."</td></tr>\n";
}

?>

<table class="common_table_list_v30 view_table_list_v30">
	<thead>
        <tr>
	   	  <th class="num_check">#</th>
	   	  <th><?=$Lang["libms"]["import_book"]["Code"]?></th>
	   	  <th><?=$Lang["libms"]["import_book"]["TitleEng"]?></th>
	   	  <th><?=$Lang["libms"]["import_book"]["TitleChi"]?></th>
       </tr>
   </thead>
   <tbody>
   <?= $table_contents ?>
   </tbody>
</table>

<?php

intranet_closedb();


?>
