<?php
function Handle_marc21($readfile,$n,$linenum,$limit){
   
$count=0;
$str= " ,;/:\r.+";
$str1=" /:\r";

while($count<$limit&&isset($readfile[$linenum])){
$arthor=0;
$unique=0;
$format=false;
$have_subject=false;
$have_ISBN=false;

while (trim( $readline =mb_convert_encoding($readfile[$linenum],"UTF-8","Big5,UTF-8"))!=""){

preg_match('#(\d{3}) ([\d ]{2}) (.*)$#', $readline, $match);
$tag = $match[1];
$field = $match[3];
$fieldArr = explode("|", $field);

   if($tag=="020"){
   	 if($have_ISBN){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["ISBN2"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") $BookInfoArr["ISBN2"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	  	 
   	   	  }
   	   }
   	 }
   	 else{
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["ISBN"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") $BookInfoArr["ISBN"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	  	 
   	   	  }
   	   }
   	   $have_ISBN=true;
   	 }
   } 
      
   if($tag=="090"||$tag=="095"||$tag=="097"||$tag=="099"){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["CallNum"][$n] = trim($fieldArr[0]);
   	   }
   	   
   	   for($i=1;$i<=count($fieldArr);$i++){
   	   	  if($fieldArr[$i][0]=="a") $BookInfoArr["CallNum"][$n] = trim(substr($fieldArr[$i],1));
   	   	  if($fieldArr[$i][0]=="b") $BookInfoArr["CallNum2"][$n] = trim(substr($fieldArr[$i],1));
   	   }
   	      	
   }
   
   if($tag=="100"){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["ResponsibilityBy1"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") $BookInfoArr["ResponsibilityBy1"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	  	 
   	   	  }
   	   }   	   
   }
   
   if($tag=="245"){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["BookTitle"][$n] = trim("$fieldArr[0]",$str1);
   	   }
   	   for($i=1;$i<=count($fieldArr);$i++){
   	   	  if($fieldArr[$i][0]=="a") $BookInfoArr["BookTitle"][$n] = trim("".substr($fieldArr[$i],1)."",$str1);
   	   	  if($fieldArr[$i][0]=="b") $BookInfoArr["BookSubtitle"][$n] = trim("".substr($fieldArr[$i],1)."",$str1);
   	   }
   	   
   	   $format=true;
   	   
   }
   
   if($tag=="250"){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["Edition"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") $BookInfoArr["Edition"][$n]= trim("".substr($fieldArr[$i],1)."",$str);
   	   	  	 
   	   	  }
   	   }
   }
   
   if($tag=="260"){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["PublishPlace"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   for($i=1;$i<=count($fieldArr);$i++){
   	   	  if($fieldArr[$i][0]=="a") $BookInfoArr["PublishPlace"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	  if($fieldArr[$i][0]=="b") $BookInfoArr["Publisher"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	  if($fieldArr[$i][0]=="c") $BookInfoArr["PublishYear"][$n] = trim("".substr($fieldArr[$i],1)."",$str);   
   	   }
   }
   
   if($tag=="300"){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["NoOfPage"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   //else{
   	   for($i=1;$i<=count($fieldArr);$i++){
   	   	 if($fieldArr[$i][0]=="a") $BookInfoArr["NoOfPage"][$n]= trim("".substr($fieldArr[$i],1)."",$str);
   	   	 if($fieldArr[$i][0]=="b") $BookInfoArr["ILL"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	 if($fieldArr[$i][0]=="c") $BookInfoArr["Dimension"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	 if($fieldArr[$i][0]=="e") $BookInfoArr["AccompanyMaterial"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	  
   	   }
   	   // }   	
   }
   
   if($tag=="440"||$tag=="490"){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["Series"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   for($i=1;$i<=count($fieldArr);$i++){
   	   	  if($fieldArr[$i][0]=="a") $BookInfoArr["Series"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
   	   	  if($fieldArr[$i][0]=="v") $BookInfoArr["SeriesNum"][$n] = trim(substr($fieldArr[$i],1));
   	   }   	
   }
   
   if($tag=="500"||$tag=="502"||$tag=="504"||$tag=="505"){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["Introduction"][$n] = trim($fieldArr[0]);
   	   }
   	   else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") $BookInfoArr["Introduction"][$n]= trim(substr($fieldArr[$i],1));
   	   	  	 
   	   	  }
   	   }   	
   }  
   
   if($tag=="650"){
   	   if($have_subject){
     	 if($fieldArr[0]!=""){
   	   	  $BookInfoArr["Tags"][$n].= ",".trim($fieldArr[0]);
   	     }
   	     else{
   	   	    for($i=1;$i<=count($fieldArr);$i++){
   	   	     	 if($fieldArr[$i][0]=="a") {
   	   	  	     $BookInfoArr["Tags"][$n].= ",".trim(substr($fieldArr[$i],1));
   	   	  	 }
   	   	  	 
   	   	   }
   	      }    	   	   	   	   	   	
   	   }
   	   else{
   	   	 if($fieldArr[0]!=""){
   	   	  $BookInfoArr["Subject"][$n] = trim($fieldArr[0]);
   	   	  $BookInfoArr["Tags"][$n].= trim($fieldArr[0]);
   	     }
   	     else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") {
   	   	  	 	$BookInfoArr["Subject"][$n]= trim(substr($fieldArr[$i],1));
   	   	  	    $BookInfoArr["Tags"][$n].= trim(substr($fieldArr[$i],1));
   	   	  	 }
   	   	  	 
   	   	   }
   	      }   
   	     $have_subject=true;	
   	   }   	     	  
   }  
   
   if($tag=="690"){
   	   if($have_subject){
     	 if($fieldArr[0]!=""){
   	   	  $BookInfoArr["Tags"][$n].= ",".trim($fieldArr[0]);
   	     }
   	     else{
   	   	    for($i=1;$i<=count($fieldArr);$i++){
   	   	     	 if($fieldArr[$i][0]=="a") {
   	   	  	     $BookInfoArr["Tags"][$n].= ",".trim(substr($fieldArr[$i],1));
   	   	  	 }
   	   	  	 
   	   	   }
   	      }    	   	   	   	   	   	
   	   }
   	   else{
   	   	 if($fieldArr[0]!=""){
   	   	  $BookInfoArr["Subject"][$n] = trim($fieldArr[0]);
   	   	  $BookInfoArr["Tags"][$n].= trim($fieldArr[0]);
   	     }
   	     else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") {
   	   	  	 	$BookInfoArr["Subject"][$n]= trim(substr($fieldArr[$i],1));
   	   	  	    $BookInfoArr["Tags"][$n].= trim(substr($fieldArr[$i],1));
   	   	  	 }
   	   	  	 
   	   	   }
   	      }   
   	     $have_subject=true;	
   	   }   	     	  
   }  
    
   if($tag=="700"){
   	 if($arthor==0){
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["ResponsibilityBy2"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") $BookInfoArr["ResponsibilityBy2"][$n]= trim("".substr($fieldArr[$i],1)."",$str);
   	   	  	 
   	   	  }
   	   }
   	 }
   	 elseif($arthor==1) {
   	   if($fieldArr[0]!=""){
   	   	  $BookInfoArr["ResponsibilityBy3"][$n] = trim("$fieldArr[0]",$str);
   	   }
   	   else{
   	   	  for($i=1;$i<=count($fieldArr);$i++){
   	   	  	 if($fieldArr[$i][0]=="a") $BookInfoArr["ResponsibilityBy3"][$n]= trim("".substr($fieldArr[$i],1)."",$str);
   	   	  	 
   	   	  }
   	   }
   	 	
   	 }  	
   $arthor++;
   }
   
   if($tag=="991"){
   	   for($i=1;$i<=count($fieldArr);$i++){
   	   	  if($fieldArr[$i][0]=="a") $BookInfoArr["ACNO"][$n][$unique] = trim(substr($fieldArr[$i],1));
          if($fieldArr[$i][0]=="b") $BookInfoArr["BarCode"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="c") $BookInfoArr["PurchaseDate"][$n][$unique] = trim(substr($fieldArr[$i],1));  	   	  
          if($fieldArr[$i][0]=="d") $BookInfoArr["LocationCode"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="i") $BookInfoArr["InvoiceNumber"][$n][$unique] = trim(substr($fieldArr[$i],1));  	   	  
          if($fieldArr[$i][0]=="n") $BookInfoArr["Discount"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="o") $BookInfoArr["Distributor"][$n][$unique] =trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="p") $BookInfoArr["PurchasePrice"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="t") $BookInfoArr["ResourcesTypeCode"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="s") $BookInfoArr["RecordStatus"][$n][$unique] = GetMappedStatus(trim(substr($fieldArr[$i],1)));   	   	  
          if($fieldArr[$i][0]=="e") $BookInfoArr["PurchaseByDepartment"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="g") $BookInfoArr["ListPrice"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="h") $BookInfoArr["PurchaseNote"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  
          if($fieldArr[$i][0]=="w") $BookInfoArr["CreationDate"][$n][$unique] = trim(substr($fieldArr[$i],1));   	   	  

   	   }   	
   	   $unique++;
   }
   $linenum++;
   $BookInfoArr['unique'][$n]=$unique;
      
}
   $linenum++;

   $count++;
   if($format==true){
   	 $n++;
   }
   
 
}
   $BookInfoArr['n']=$n;//個數
   $BookInfoArr['count']=$count;
   $BookInfoArr['linenum']=$linenum-1;
   return $BookInfoArr;

}
function GetMappedStatus($StatusKey)
{
	$import_record = array();
	switch ($StatusKey) 
	{
		case "m": 
			$import_record['RecordStatus'] = 'LOST' ;
			break;
		case "o":	// order 
		case "p": 
			$import_record['RecordStatus'] = 'SHELVING' ;
			break;
		case "r": 
			$import_record['RecordStatus'] = 'REPAIRING' ;
			break;
		case "r": 
			$import_record['RecordStatus'] = 'RESERVED' ;
			break;
		case "d": 
			$import_record['RecordStatus'] = 'RESTRICTED' ;
			break;
		case "s": 
			$import_record['RecordStatus'] = 'SUSPECTEDMISSING' ;
			break;
		case "n": 
		case "x": 
			$import_record['RecordStatus'] = 'WRITEOFF' ;
			break;
		case "l":
			$import_record['RecordStatus'] = 'BORROWED' ;
			break;
		case "o":
			$import_record['RecordStatus'] = 'BORROWED' ;
			break;
		case "a":
		default: 
			$import_record['RecordStatus'] = 'NORMAL' ;
			break;
	}
	
				
			/*
			SLS
			
				a  Available
				
				p  Processing
				
				d  on Display
				
				l  on Loan
				
				s  Suspected missing
				
				m  Missing
				
				x  Withdrawn
				
				o  on Order
				
				y  For reference only
			*/
	
	return $import_record['RecordStatus'];
}
?>