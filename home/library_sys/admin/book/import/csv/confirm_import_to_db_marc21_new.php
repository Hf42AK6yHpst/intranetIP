<?php

@SET_TIME_LIMIT(216000);

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");


intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

############################################################################################################
///*

//$is_debug = true;		# for debug only - it would not insert any record but just echo sql query if set 

global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);


//# step information
//$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
//$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
//$STEPS_OBJ[] = array($i_general_imported_result, 0);

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='../../'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_csv_data2.php'");



//===================================================================================

############################################################################################################
	
function get_left_over_from_tmp(){
	global $libms;
	$result = $libms->returnArray("select count(*) as leftover from LIBMS_MARC_TMP_2");
	return $result[0]['leftover'];
}

############################################################################################################
$total=get_left_over_from_tmp();
//if($alert_exist_acno!=""||$alert_duplicate_acno!=""||$alert_exist_barcode!=""||$alert_duplicate_barcode)
if($alert_exist_acno!=""||$alert_exist_barcode!="")
{  
    $x=$Lang["libms"]["import_book"]["Result"]["Fail"]."<br>";
	if($alert_exist_acno!="")$x.=$Lang["libms"]["label"]["BookCode"].": ".rtrim($alert_exist_acno,", ")." ".$Lang["libms"]["import_book"]["alreadyexist"]."<br>";
	if($alert_exist_barcode!="")$x.=$Lang["libms"]["label"]["BarCode"].": ".rtrim($alert_exist_barcode,", ")." ".$Lang["libms"]["import_book"]["alreadyexist"]."<br>";
//	if($alert_duplicate_acno!="")$x.=$Lang["libms"]["label"]["BookCode"].": ".rtrim($alert_duplicate_acno,", ")." ".$Lang["libms"]["import_book"]["doubleused"]."<br>";
//	if($alert_duplicate_barcode)$x.=$Lang["libms"]["label"]["BarCode"].": ".rtrim($alert_duplicate_barcode,", ")." ".$Lang["libms"]["import_book"]["doubleused"]."<br>";
	
	//$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data2.php'");
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data2.php'")."&nbsp;".$linterface->GET_ACTION_BTN($Lang["libms"]["import_book"]["ContinueImport"], "button","javascript:confirmToSend();");
	
	# step information
	$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
	$STEPS_OBJ[] = array($i_general_imported_result, 0);
}
else{

if(isset($counter)||isset($counterItem)){
	$lang_space = (($_SESSION['intranet_session_language'] == 'en') ? " " : "") ? " " : "";

    $x = $Lang['libms']['import_book']['Result']['Success']."<br>";
    $x .= $lang_space.$Lang['libms']['import_book']['total_record'].":";
    $x .= $counter;
    $x .= $lang_space.$Lang['libms']['import_book']['book_records'];
    $x .= $lang_space.$Lang['libms']['import_book']['and'];
    $x .= $lang_space;
    $x .= $counterItem;
    $x .= $lang_space.$Lang['libms']['import_book']['item_records'];
	
	$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='../../'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_csv_data2.php'");
		
	# step information
	$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 1);
}
else{
	$x=$Lang["libms"]["import_book"]["CheckImport"];
    $import_button = $linterface->GET_ACTION_BTN($button_import, "button","javascript:confirmToSend();")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data2.php'");
    //$total=get_left_over_from_tmp();
    
    # step information
	$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
	$STEPS_OBJ[] = array($i_general_imported_result, 0);
}
}



if($is_debug) die();


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script> 
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" /> 
<script>
function confirmToSend(){
    $('#send_by_ajax').click();    
}
</script>
<br />
<form name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center"><br /> <?= $x ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5"
					align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><?=$import_button?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	    <a style="display:none" id="send_by_ajax" name="send_by_ajax" href="ajax_send_db.php?height=200&width=500&modal=true&total=<?=$total?>" class="thickbox">Thickbox</a>	
</form><?php

$linterface->LAYOUT_STOP();

intranet_closedb();


?>