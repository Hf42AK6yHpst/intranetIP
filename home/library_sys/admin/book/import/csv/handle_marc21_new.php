<?php
function Handle_marc21($readfile,$n,$linenum,$limit){
	global $sys_custom;
	$PATH_WRT_ROOT = "../../../../../../";
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");	
	intranet_opendb();
    $libms = new liblms();
    $sql = "select DescriptionChi from LIBMS_RESPONSIBILITY";
    $responsibility = $libms->returnArray($sql);
    $responsibility_count = count($responsibility);
    $responsibilityby_count_tmp = -1;
    $responsibility_tmp = "";
	$readfile[0] = remove_utf8_bom($readfile[0]);
	$count=0;
	$str= " ,;/:\r.+-";
	$str1=" /:\r";

	while($count<$limit&&isset($readfile[$linenum])){

		
		$ResponsibilityByArray=array();
		$unique=0;
		$format=false;
		$have_subject= ($sys_custom['eLibraryPlus']['Marc21ChangeTag650To690'] ? true : false);
		//$first_line = false;
	
		while (trim( $readline =mb_convert_encoding($readfile[$linenum],"UTF-8","UTF-8,Big5"))!=""){
// 			if($first_line==true){
			
// 				$match_array = preg_split("/[\s]+/", $readline);
// 				$BookInfoArr["ResourcesTypeCode"][$n] = trim($match_array[0],$str);						
// 	   			$BookInfoArr["Country"][$n] = trim($match_array[1],$str);		
// 				$BookInfoArr["Language"][$n]  = trim($match_array[2],$str);		
// 				$linenum++;		
// 				$first_line = false;
			
// 			}		
// 			else{
				
				preg_match('#(\d{3}) ([\d ]{2}) (.*)$#', $readline, $match);
				$tag = $match[1];
				$field = $match[3];
				$fieldArr = explode("|", $field);
				if($tag==""){
					$match_array = preg_split("/[\s]+/", $readline);
				    $BookInfoArr["ResourcesTypeCode"][$n] = trim($match_array[0],$str);						
	   			    $BookInfoArr["Country"][$n] = trim($match_array[1],$str);		
				    $BookInfoArr["Language"][$n]  = trim($match_array[2],$str);	
				}
				if($tag=="020"){
					if($BookInfoArr["ISBN"][$n]!=""){
						if($fieldArr[0]!=""){
							
							if($BookInfoArr["ISBN2"][$n]=="")
								$BookInfoArr["ISBN2"][$n] = trim("$fieldArr[0]",$str);
							else{
								$BookInfoArr["ISBN2"][$n] = $BookInfoArr["ISBN2"][$n]."; ".trim("$fieldArr[0]",$str);
							}
						}
						else{
							for($i=1;$i<=count($fieldArr);$i++){
								if($fieldArr[$i][0]=="a") 
						        {
						        	if($BookInfoArr["ISBN2"][$n]=="")
						        		$BookInfoArr["ISBN2"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
						        	else{
						        		$BookInfoArr["ISBN2"][$n] = $BookInfoArr["ISBN2"][$n]."; ".trim("".substr($fieldArr[$i],1)."",$str);
						        	}
									
								}
							}
						}
					}
					else{
						if($fieldArr[0]!=""){
							$BookInfoArr["ISBN"][$n] = trim("$fieldArr[0]",$str);
						}
						else{
							for($i=1;$i<=count($fieldArr);$i++){
								if($fieldArr[$i][0]=="a") $BookInfoArr["ISBN"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
								 
							}
						}
						
					}
				}

				if($tag=="090"||$tag=="095"||$tag=="097"||$tag=="099"){
					if($fieldArr[0]!=""){
						//$BookInfoArr["CallNum"][$n] = trim($fieldArr[0]);
						$callnumArr = explode(" ",$fieldArr[0], 2);
						if($callnumArr[0]!="")$BookInfoArr["CallNum"][$n] = trim($callnumArr[0]);
						if($callnumArr[1]!="")$BookInfoArr["CallNum2"][$n] = trim($callnumArr[1]);
						
					}
				
					for($i=1;$i<=count($fieldArr);$i++){
						if($fieldArr[$i][0]=="a") $BookInfoArr["CallNum"][$n] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="b") $BookInfoArr["CallNum2"][$n] = trim(substr($fieldArr[$i],1));
					}
				
				}
				
				
				if($tag=="100"){
					if($fieldArr[0]!=""){
						$ResponsibilityByArray[] = trim("$fieldArr[0]",$str);
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") $ResponsibilityByArray[] = trim("".substr($fieldArr[$i],1)."",$str);
						}
					}
				}
				
				
				if($tag=="245"){
					if($fieldArr[0]!=""){
						$BookInfoArr["BookTitle"][$n] = "$fieldArr[0]";
					}
					for($i=1;$i<=count($fieldArr);$i++){
						if($sys_custom['eLibraryPlus']['PLKWCC_Marc21']){
							if($fieldArr[$i][0]=="a") $BookInfoArr["BookTitle"][$n] .= "".substr($fieldArr[$i],1);
							if($fieldArr[$i][0]=="b") $BookInfoArr["BookTitle"][$n] .= " ".substr($fieldArr[$i],1);
							if($fieldArr[$i][0]=="c") $BookInfoArr["BookTitle"][$n] .= " ".substr($fieldArr[$i],1);
						}
						else{
						if($fieldArr[$i][0]=="a") $BookInfoArr["BookTitle"][$n] = trim("".substr($fieldArr[$i],1)."",$str1);
						if($fieldArr[$i][0]=="b") $BookInfoArr["BookSubtitle"][$n] = trim("".substr($fieldArr[$i],1)."",$str1);
						if($fieldArr[$i][0]=="c") {						
							$authers = explode(";",trim("".substr($fieldArr[$i],1)."",$str));
							for($j=0;$j<count($authers);$j++){
								if($BookInfoArr["ResponsibilityBy1"][$n]!=""&&$BookInfoArr["ResponsibilityBy2"][$n]!=""&&$BookInfoArr["ResponsibilityBy3"][$n]==""){
									$BookInfoArr["ResponsibilityBy3"][$n] =trim($authers[$j],$str);
								}
								if($BookInfoArr["ResponsibilityBy1"][$n]!=""&&$BookInfoArr["ResponsibilityBy2"][$n]==""){
									$BookInfoArr["ResponsibilityBy2"][$n] = trim($authers[$j],$str);
								}
								if($BookInfoArr["ResponsibilityBy1"][$n]==""){
									$BookInfoArr["ResponsibilityBy1"][$n] = trim($authers[$j],$str);									
								}
								/*
								$authers[$j] = trim($authers[$j],$str).";";
								for($k=0;$k<$responsibility_count;$k++){
									if(strpos($authers[$j],$responsibility[$k][0].";")){
										if($responsibilityby_count_tmp==-1||strpos($authers[$j],$responsibility[$k][0].";")<$responsibilityby_count_tmp){
											$responsibilityby_count_tmp = strpos($authers[$j],$responsibility[$k][0].";");
											$responsibility_tmp = $responsibility[$k][0];
										}								
									}
								}
								
								if($responsibilityby_count_tmp!=-1){								
									if($BookInfoArr["ResponsibilityBy1"][$n]!=""&&$BookInfoArr["ResponsibilityBy2"][$n]!=""&&$BookInfoArr["ResponsibilityBy3"][$n]==""){
										$BookInfoArr["ResponsibilityBy3"][$n] = substr($authers[$j],0,$responsibilityby_count_tmp);
										$BookInfoArr["Responsibility3"][$n] = $responsibility_tmp;
									}
									if($BookInfoArr["ResponsibilityBy1"][$n]!=""&&$BookInfoArr["ResponsibilityBy2"][$n]==""){
										$BookInfoArr["ResponsibilityBy2"][$n] = substr($authers[$j],0,$responsibilityby_count_tmp);
										$BookInfoArr["Responsibility2"][$n] = $responsibility_tmp;
									}
									if($BookInfoArr["ResponsibilityBy1"][$n]==""){
										$BookInfoArr["ResponsibilityBy1"][$n] = substr($authers[$j],0,$responsibilityby_count_tmp);
										$BookInfoArr["Responsibility1"][$n] = $responsibility_tmp;
									}
								}else{
									if($BookInfoArr["ResponsibilityBy1"][$n]!=""&&$BookInfoArr["ResponsibilityBy2"][$n]!=""&&$BookInfoArr["ResponsibilityBy3"][$n]==""){
										$BookInfoArr["ResponsibilityBy3"][$n] =trim($authers[$j],$str);
									}
									if($BookInfoArr["ResponsibilityBy1"][$n]!=""&&$BookInfoArr["ResponsibilityBy2"][$n]==""){
										$BookInfoArr["ResponsibilityBy2"][$n] = trim($authers[$j],$str);
									}
									if($BookInfoArr["ResponsibilityBy1"][$n]==""){
										$BookInfoArr["ResponsibilityBy1"][$n] = trim($authers[$j],$str);									
									}
								}
								$responsibilityby_count_tmp = -1;
								$responsibility_tmp = "";
								*/
							}							
						}
						}
					}
				
					$format=true;
				
				}
				
			    if($tag=="246"){
			    			    	
					if($fieldArr[0]!=""){
						$varying_form_title = trim("$fieldArr[0]",$str);
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") $varying_form_title= trim("".substr($fieldArr[$i],1)."",$str);							 
						}
					}
					
					if($BookInfoArr["BookSubtitle"][$n]==""){
			    		$BookInfoArr["BookSubtitle"][$n] = $varying_form_title;
			    	}else{
			    		$BookInfoArr["BookSubtitle"][$n] = $BookInfoArr["BookSubtitle"][$n]."; ".$varying_form_title;			    		
			    	}
				}
				
				if($tag=="250"){
					if($fieldArr[0]!=""){
						$BookInfoArr["Edition"][$n] = trim("$fieldArr[0]",$str);
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") $BookInfoArr["Edition"][$n]= trim("".substr($fieldArr[$i],1)."",$str);
							 
						}
					}
				}
				
				if($tag=="260"){
					if($fieldArr[0]!=""){
						$BookInfoArr["PublishPlace"][$n] = trim("$fieldArr[0]",$str);
					}
					for($i=1;$i<=count($fieldArr);$i++){
						if($fieldArr[$i][0]=="a") $BookInfoArr["PublishPlace"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
						if($fieldArr[$i][0]=="b") $BookInfoArr["Publisher"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
						if($fieldArr[$i][0]=="c") $BookInfoArr["PublishYear"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
					}
				}
				
				if($tag=="300"){
					if($fieldArr[0]!=""){
						$BookInfoArr["NoOfPage"][$n] = "$fieldArr[0]";
					}
					//else{
					if($sys_custom['eLibraryPlus']['PLKWCC_Marc21']){
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") $BookInfoArr["NoOfPage"][$n] .= "".substr($fieldArr[$i],1);
							if($fieldArr[$i][0]=="b") $BookInfoArr["NoOfPage"][$n] .= " ".substr($fieldArr[$i],1)."";
							if($fieldArr[$i][0]=="c") $BookInfoArr["NoOfPage"][$n] .= " ".substr($fieldArr[$i],1)."";
							if($fieldArr[$i][0]=="e") $accompany_material[$n] .= " ".substr($fieldArr[$i],1)."";
						}
					}
					else{
					for($i=1;$i<=count($fieldArr);$i++){
						if($fieldArr[$i][0]=="a") $BookInfoArr["NoOfPage"][$n]= trim("".substr($fieldArr[$i],1)."",$str);
						if($fieldArr[$i][0]=="b") $BookInfoArr["ILL"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
						if($fieldArr[$i][0]=="c") $BookInfoArr["Dimension"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
						if($fieldArr[$i][0]=="e") $accompany_material[$n] = trim("".substr($fieldArr[$i],1)."",$str);
						 
					}
					}
					// }
				}
				
				if($tag=="440"||$tag=="490"){
					if($fieldArr[0]!=""){
						$BookInfoArr["Series"][$n] = trim("$fieldArr[0]",$str);
					}
					$have_seriesNum = false;
					for($i=1;$i<=count($fieldArr);$i++){						
						if($fieldArr[$i][0]=="a") $BookInfoArr["Series"][$n] = trim("".substr($fieldArr[$i],1)."",$str);				
						if($fieldArr[$i][0]=="p") {
							if($BookInfoArr["SeriesNum"][$n]=="")
								$BookInfoArr["SeriesNum"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
							else{
								$BookInfoArr["SeriesNum"][$n] = $BookInfoArr["SeriesNum"][$n].trim("".substr($fieldArr[$i],1)."",$str);
							}
							$have_seriesNum = true;
						}
						if($fieldArr[$i][0]=="v") {
							if($BookInfoArr["SeriesNum"][$n]=="")
								$BookInfoArr["SeriesNum"][$n] = trim("".substr($fieldArr[$i],1)."",$str);
							else{
								if(substr($BookInfoArr["SeriesNum"][$n],-2,1)==";"){
									$BookInfoArr["SeriesNum"][$n] = $BookInfoArr["SeriesNum"][$n].trim("".substr($fieldArr[$i],1)."",$str);
								}else{
									$BookInfoArr["SeriesNum"][$n] = $BookInfoArr["SeriesNum"][$n].trim("[".substr($fieldArr[$i],1)."]",$str);										
								}
							}
							$have_seriesNum = true;								
						}
					}
					if($have_seriesNum){
						$BookInfoArr["SeriesNum"][$n] = $BookInfoArr["SeriesNum"][$n]."; ";	

					}
				}
				
				if($tag=="500"||$tag=="502"||$tag=="504"||$tag=="505"){
					if($fieldArr[0]!=""){
						if($BookInfoArr["Introduction"][$n]=="")
							$BookInfoArr["Introduction"][$n] = trim($fieldArr[0],$str);
						else{
							$BookInfoArr["Introduction"][$n] = $BookInfoArr["Introduction"][$n]."; ".trim($fieldArr[0],$str);
						}
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a"){
								if($BookInfoArr["Introduction"][$n]=="")
									$BookInfoArr["Introduction"][$n] = trim(substr($fieldArr[$i],1),$str);
								else{
									$BookInfoArr["Introduction"][$n] = $BookInfoArr["Introduction"][$n]."; ". trim(substr($fieldArr[$i],1),$str);
								}
							} 
						}
					}
				}
		
				if($tag=="650"){
					if($have_subject){
						if($fieldArr[0]!=""){
							$BookInfoArr["Tags"][$n].= (!$BookInfoArr["Tags"][$n]?"":",").trim($fieldArr[0],$str);
						}
						else{
							for($i=1;$i<=count($fieldArr);$i++){
								if($fieldArr[$i][0]=="a") {
									$BookInfoArr["Tags"][$n].= ",".trim(substr($fieldArr[$i],1),$str);
								}
							}
						}
//						if($sys_custom['eLibraryPlus']['Marc21ChangeTag650To690']){
//							$have_subject=false;
//						}
					}
					else{
						if($fieldArr[0]!=""){
							$BookInfoArr["Subject"][$n] = trim($fieldArr[0],$str);
							$BookInfoArr["Tags"][$n].= trim($fieldArr[0],$str);
						}
						else{
							for($i=1;$i<=count($fieldArr);$i++){
								if($fieldArr[$i][0]=="a") {
									$BookInfoArr["Subject"][$n]= trim(substr($fieldArr[$i],1),$str);
									$BookInfoArr["Tags"][$n].= trim(substr($fieldArr[$i],1),$str);
								}
							}
						}
						$have_subject=true;
					}
				}
				
				if($tag=="690"){
					if($sys_custom['eLibraryPlus']['Marc21ChangeTag650To690']){
						$have_subject = false;
					}
					
					if($have_subject){
						if($fieldArr[0]!=""){
							$BookInfoArr["Tags"][$n].= ",".trim($fieldArr[0],$str);
						}
						else{
							for($i=1;$i<=count($fieldArr);$i++){
								if($fieldArr[$i][0]=="a") {
									$BookInfoArr["Tags"][$n].= ",".trim(substr($fieldArr[$i],1),$str);
								}
							}
						}
					}
					else{
						if($fieldArr[0]!=""){
							$BookInfoArr["Subject"][$n] = trim($fieldArr[0],$str);
							if(!$sys_custom['eLibraryPlus']['Marc21ChangeTag650To690']){
								$BookInfoArr["Tags"][$n].= trim($fieldArr[0],$str);
							}
						}
						else{
							for($i=1;$i<=count($fieldArr);$i++){
								if($fieldArr[$i][0]=="a") {
									$BookInfoArr["Subject"][$n]= trim(substr($fieldArr[$i],1),$str);
									if(!$sys_custom['eLibraryPlus']['Marc21ChangeTag650To690']){
										$BookInfoArr["Tags"][$n].= trim(substr($fieldArr[$i],1),$str);
									}
								}
							}
						}
						$have_subject=true;
					}
				}
				 
				if($tag=="700"){
					if($fieldArr[0]!=""){				
					   $ResponsibilityByArray[] = trim("$fieldArr[0]",$str);					
					}					
					else{					
						for($i=1;$i<=count($fieldArr);$i++){						
		    				if($fieldArr[$i][0]=="a") $ResponsibilityByArray[] = trim("".substr($fieldArr[$i],1)."",$str);						
						}					
					}						
				} 
				
				/*
				if($tag=="700"){
					if($arthor==0){
						if($fieldArr[0]!=""){
							$BookInfoArr["ResponsibilityBy2"][$n] = trim("$fieldArr[0]",$str);
						}
						else{
							for($i=1;$i<=count($fieldArr);$i++){
								if($fieldArr[$i][0]=="a") $BookInfoArr["ResponsibilityBy2"][$n]= trim("".substr($fieldArr[$i],1)."",$str);
								 
							}
						}
					}
					elseif($arthor==1) {
						if($fieldArr[0]!=""){
							$BookInfoArr["ResponsibilityBy3"][$n] = trim("$fieldArr[0]",$str);
						}
						else{
							for($i=1;$i<=count($fieldArr);$i++){
								if($fieldArr[$i][0]=="a") $BookInfoArr["ResponsibilityBy3"][$n]= trim("".substr($fieldArr[$i],1)."",$str);
								 
							}
						}
						 
					}
					$arthor++;
				}
				*/
				
				if($tag=="991"){
					for($i=1;$i<=count($fieldArr);$i++){
						if($fieldArr[$i][0]=="a") $BookInfoArr["ACNO"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="b") $BookInfoArr["BarCode"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="c") $BookInfoArr["PurchaseDate"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="d") $BookInfoArr["LocationCode"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="i") $BookInfoArr["InvoiceNumber"][$n][$unique] = trim(substr($fieldArr[$i],1));
						//if($fieldArr[$i][0]=="n") $BookInfoArr["Discount"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="n"){
							if($sys_custom['eLib']['MARC21_991_n']=="DISCOUNT"){
								$BookInfoArr["Discount"][$n][$unique] = trim(substr($fieldArr[$i],1));
							}else{
								if($accompany_material[$n]!=""){
								   $BookInfoArr["AccompanyMaterial"][$n][$unique] = $accompany_material[$n]."; ".trim(substr($fieldArr[$i],1));							
								}else{
								   $BookInfoArr["AccompanyMaterial"][$n][$unique] = trim(substr($fieldArr[$i],1));												
								}	
							}					
						}
						if($fieldArr[$i][0]=="o") $BookInfoArr["Distributor"][$n][$unique] =trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="p") $BookInfoArr["PurchasePrice"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="t") $BookInfoArr["CirculationTypeCode"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="s") $BookInfoArr["RecordStatus"][$n][$unique] = GetMappedStatus(trim(substr($fieldArr[$i],1)));
						if($fieldArr[$i][0]=="e") $BookInfoArr["PurchaseByDepartment"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="g") $BookInfoArr["ListPrice"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="h") $BookInfoArr["PurchaseNote"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="w") $BookInfoArr["CreationDate"][$n][$unique] = trim(substr($fieldArr[$i],1));
						if($fieldArr[$i][0]=="v") $BookInfoArr["ItemSeriesNum"][$n][$unique] = trim(substr($fieldArr[$i],1));
						
					}
					if($accompany_material[$n]!=""&&$BookInfoArr["AccompanyMaterial"][$n][$unique]==""){
					   $BookInfoArr["AccompanyMaterial"][$n][$unique] = $accompany_material[$n];
					}
					
					$unique++;
				}
				$linenum++;
				$BookInfoArr['unique'][$n]=$unique;				
			//}
			
		}
	   $linenum++;
	
	   $count++;
	   
	   if(count($BookInfoArr["ACNO"][$n]) > 0 && !$BookInfoArr["BookTitle"][$n]){
			$BookInfoArr["BookTitle"][$n] = "(Untitled book)";
			$format = true;
	   }
	   
	   if($format==true){	
	     $BookInfoArr["SeriesNum"][$n] = rtrim($BookInfoArr["SeriesNum"][$n],$str);
	     if($BookInfoArr["Tags"][$n]!=""){
	     	$Middle_tag = explode(",",$BookInfoArr["Tags"][$n]);	 
	     	$Middle_tag = array_unique($Middle_tag);
	     	$BookInfoArr["Tags"][$n] = implode(",",$Middle_tag);
	     }
	     
	     for($i=0;$i<count($ResponsibilityByArray);$i++){
	     	$duplicate=false;
	     	if($BookInfoArr["ResponsibilityBy1"][$n]!=""&&$ResponsibilityByArray[$i]!=""&&strstr($BookInfoArr["ResponsibilityBy1"][$n],$ResponsibilityByArray[$i])!==false)
	     		$duplicate = true;
	     	if($BookInfoArr["ResponsibilityBy2"][$n]!=""&&$ResponsibilityByArray[$i]!=""&&strstr($BookInfoArr["ResponsibilityBy2"][$n],$ResponsibilityByArray[$i])!==false)
	        	$duplicate = true;
	        if($BookInfoArr["ResponsibilityBy3"][$n]!=""&&$ResponsibilityByArray[$i]!=""&&strstr($BookInfoArr["ResponsibilityBy3"][$n],$ResponsibilityByArray[$i])!==false)
	        	$duplicate = true;
	       	
	        if(!$duplicate) $new_responsibilityByArray[] = $ResponsibilityByArray[$i];
	     }	     

	     if($BookInfoArr["ResponsibilityBy1"][$n]==""){
             $BookInfoArr["ResponsibilityBy1"][$n] = $new_responsibilityByArray[0];
             $BookInfoArr["ResponsibilityBy2"][$n] = $new_responsibilityByArray[1];
             $BookInfoArr["ResponsibilityBy3"][$n] = $new_responsibilityByArray[2];
	     }else if($BookInfoArr["ResponsibilityBy2"][$n]==""){
	     	 $BookInfoArr["ResponsibilityBy2"][$n] = $new_responsibilityByArray[0];
             $BookInfoArr["ResponsibilityBy3"][$n] = $new_responsibilityByArray[1];
	     }else if($BookInfoArr["ResponsibilityBy3"][$n]==""){
             $BookInfoArr["ResponsibilityBy3"][$n] = $new_responsibilityByArray[0];     	
	     }
         unset($new_responsibilityByArray);
	     //handle responsibility								
		 if($BookInfoArr["ResponsibilityBy1"][$n]!=""&&$responsibility_count!=0){
		 	$authers = trim($BookInfoArr["ResponsibilityBy1"][$n],$str).";";
		 	for($k=0;$k<$responsibility_count;$k++){
				if(strpos($authers,$responsibility[$k][0].";")){
					if($responsibilityby_count_tmp==-1||strpos($authers,$responsibility[$k][0].";")<$responsibilityby_count_tmp){
						$responsibilityby_count_tmp = strpos($authers,$responsibility[$k][0].";");
						$responsibility_tmp = $responsibility[$k][0];
					}								
			     }			 
		    }
		    if($responsibilityby_count_tmp!=-1){
		    	$BookInfoArr["ResponsibilityBy1"][$n] = substr($authers,0,$responsibilityby_count_tmp);
				$BookInfoArr["Responsibility1"][$n] = $responsibility_tmp;		    	
		    }			 	
		    $responsibilityby_count_tmp = -1;
		    $responsibility_tmp = "";	
		 }
		 if($BookInfoArr["ResponsibilityBy2"][$n]!=""&&$responsibility_count!=0){
		 	$authers = trim($BookInfoArr["ResponsibilityBy2"][$n],$str).";";
		 	for($k=0;$k<$responsibility_count;$k++){
				if(strpos($authers,$responsibility[$k][0].";")){
					if($responsibilityby_count_tmp==-1||strpos($authers,$responsibility[$k][0].";")<$responsibilityby_count_tmp){
						$responsibilityby_count_tmp = strpos($authers,$responsibility[$k][0].";");
						$responsibility_tmp = $responsibility[$k][0];
					}								
			     }			 
		    }
		    if($responsibilityby_count_tmp!=-1){
		    	$BookInfoArr["ResponsibilityBy2"][$n] = substr($authers,0,$responsibilityby_count_tmp);
				$BookInfoArr["Responsibility2"][$n] = $responsibility_tmp;		    	
		    }			 	
		    $responsibilityby_count_tmp = -1;
		    $responsibility_tmp = "";	
		 }
		 if($BookInfoArr["ResponsibilityBy3"][$n]!=""&&$responsibility_count!=0){
		 	$authers = trim($BookInfoArr["ResponsibilityBy3"][$n],$str).";";
		 	for($k=0;$k<$responsibility_count;$k++){
				if(strpos($authers,$responsibility[$k][0].";")){
					if($responsibilityby_count_tmp==-1||strpos($authers,$responsibility[$k][0].";")<$responsibilityby_count_tmp){
						$responsibilityby_count_tmp = strpos($authers,$responsibility[$k][0].";");
						$responsibility_tmp = $responsibility[$k][0];
					}								
			     }			 
		    }
		    if($responsibilityby_count_tmp!=-1){
		    	$BookInfoArr["ResponsibilityBy3"][$n] = substr($authers,0,$responsibilityby_count_tmp);
				$BookInfoArr["Responsibility3"][$n] = $responsibility_tmp;		    	
		    }			 	
		    $responsibilityby_count_tmp = -1;
		    $responsibility_tmp = "";	
		 }
					
	   	 $n++;
	   }
	   
	}
   $BookInfoArr['n']=$n;
   $BookInfoArr['count']=$count;
   $BookInfoArr['linenum']=$linenum-1;
   return $BookInfoArr;

}

function remove_utf8_bom($text)
{
	$bom = pack('H*','EFBBBF');
	$text = preg_replace("/^$bom/", '', $text);
	return $text;
}

function GetMappedStatus($StatusKey)
{
	$import_record = array();
	switch ($StatusKey) 
	{
		case "m": 
			$import_record['RecordStatus'] = 'LOST' ;
			break;
		case "o":	// order 
		case "p": 
			$import_record['RecordStatus'] = 'SHELVING' ;
			break;
		case "r": 
			$import_record['RecordStatus'] = 'REPAIRING' ;
			break;
		case "r": 
			$import_record['RecordStatus'] = 'RESERVED' ;
			break;
		case "d": 
			$import_record['RecordStatus'] = 'RESTRICTED' ;
			break;
		case "s": 
			$import_record['RecordStatus'] = 'SUSPECTEDMISSING' ;
			break;
		case "n": 
		case "x": 
			$import_record['RecordStatus'] = 'WRITEOFF' ;
			break;
		case "l":
			$import_record['RecordStatus'] = 'BORROWED' ;
			break;
		case "o":
			$import_record['RecordStatus'] = 'BORROWED' ;
			break;
		case "a":
		default: 
			$import_record['RecordStatus'] = 'NORMAL' ;
			break;
	}
	
				
			/*
			SLS
			
				a  Available
				
				p  Processing
				
				d  on Display
				
				l  on Loan
				
				s  Suspected missing
				
				m  Missing
				
				x  Withdrawn
				
				o  on Order
				
				y  For reference only
			*/
	
	return $import_record['RecordStatus'];
}

?>