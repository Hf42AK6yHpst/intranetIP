<?php
@SET_TIME_LIMIT(216000);

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."lang/libms.lang.en.php");	# for getting the item status values
if (is_array($Lang["libms"]["book_status"]) && sizeof($Lang["libms"]["book_status"])>0)
{
	foreach ($Lang["libms"]["book_status"] AS $db_value => $column_data)
	{
		$ItemStatusMapping[strtoupper($column_data)] = $db_value;
	}
}

include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once("handle_marc21_new.php");
//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();

$limport = new libimporttext();

function check_exist_ACNO($given_bookCode){
	global $libms;
	//$sql = "select ACNO from LIBMS_BOOK_UNIQUE where ACNO=".CCToSQL($given_bookCode)." limit 1";
	$sql = "select BookTitle from LIBMS_BOOK_UNIQUE as lbu left join LIBMS_BOOK as lb on lb.BookID = lbu.BookID where ACNO=".CCToSQL($given_bookCode)." limit 1";
	$result = $libms->returnArray($sql);
    return $result;
//	if (empty($result)){
//		return FALSE;
//	}else{
//
//		return true;
//	}
}

function check_exist_Barcode($given_barcode){
	global $libms;
	$sql = "select BookTitle from LIBMS_BOOK_UNIQUE as lbu left join LIBMS_BOOK as lb on lb.BookID = lbu.BookID where BarCode=".CCToSQL($given_barcode)." limit 1";
	$result = $libms->returnArray($sql);
    return $result;

//	if (empty($result)){
//		return FALSE;
//	}else{
//
//		return true;
//	}
}

###########################insert into TMP DB################################

$marc21file = $intranet_root."/file/lms_book_import/" . $file;

$readfile=file($marc21file);
$totalline=count($readfile);
if(!isset($book))$book=0;
if(!isset($line))$line=0;

$BookInfoArr = Handle_marc21($readfile,$book,$line,500);

//insert into the Temporary table
for($n=$book;$n<$BookInfoArr['n'];$n++){
		$insert_to_db1 = array(
				'BookTitle'=> CCTOSQL($BookInfoArr["BookTitle"][$n]),
				'BookSubTitle'=> CCTOSQL($BookInfoArr["BookSubtitle"][$n]),
				'CallNum'=> CCTOSQL($BookInfoArr["CallNum"][$n]),
				'CallNum2'=> CCTOSQL($BookInfoArr["CallNum2"][$n]),
				'ISBN'=> CCTOSQL($BookInfoArr["ISBN"][$n]),
				'ISBN2'=> CCTOSQL($BookInfoArr["ISBN2"][$n]),				
				'Introduction'=> CCTOSQL($BookInfoArr["Introduction"][$n]),
				'Edition'=> CCTOSQL($BookInfoArr["Edition"][$n]),
				'PublishYear'=> CCTOSQL($BookInfoArr["PublishYear"][$n]),
				'Publisher'=> CCTOSQL($BookInfoArr["Publisher"][$n]),
				'PublishPlace'=>CCTOSQL( $BookInfoArr["PublishPlace"][$n]),
				'Series'=> CCTOSQL($BookInfoArr["Series"][$n]),
				'SeriesNum'=> CCTOSQL($BookInfoArr["SeriesNum"][$n]),
				'Tags'=> CCTOSQL($BookInfoArr["Tags"][$n]),
				'ResponsibilityBy1'=> CCTOSQL($BookInfoArr["ResponsibilityBy1"][$n]),
				'ResponsibilityBy2'=>CCTOSQL( $BookInfoArr["ResponsibilityBy2"][$n]),
				'ResponsibilityBy3'=> CCTOSQL($BookInfoArr["ResponsibilityBy3"][$n]),
				'ResponsibilityCode1'=> CCTOSQL($BookInfoArr["Responsibility1"][$n]),
				'ResponsibilityCode2'=>CCTOSQL( $BookInfoArr["Responsibility2"][$n]),
				'ResponsibilityCode3'=> CCTOSQL($BookInfoArr["Responsibility3"][$n]),
				'NoOfPage'=>CCTOSQL( $BookInfoArr["NoOfPage"][$n]),	
				'Dimension'=>CCTOSQL( $BookInfoArr["Dimension"][$n]),
				'ILL'=>CCTOSQL( $BookInfoArr["ILL"][$n]),		
				'Country'=>CCTOSQL( $BookInfoArr["Country"][$n]),
				'Language'=>CCTOSQL( $BookInfoArr["Language"][$n]),		
				'BookResources'=>CCTOSQL( $BookInfoArr["ResourcesTypeCode"][$n]),				
				'Subject'=> CCTOSQL($BookInfoArr["Subject"][$n]),);
		if($BookInfoArr['unique'][$n]==0)
		   $libms->INSERT2TABLE('LIBMS_MARC_TMP_2', $insert_to_db1);
		else{
           for($i=0;$i<$BookInfoArr['unique'][$n];$i++){		
            if($BookInfoArr["ACNO"][$n][$i]=="")$BookInfoArr["ACNO"][$n][$i] = $BookInfoArr["BarCode"][$n][$i];
            if($BookInfoArr["BarCode"][$n][$i]=="")$BookInfoArr["BarCode"][$n][$i] = $BookInfoArr["ACNO"][$n][$i];
            
	       $insert_to_db2 =	array('ACNO'=> CCTOSQL($BookInfoArr["ACNO"][$n][$i]),
	            'BookCirclation'=> CCTOSQL($BookInfoArr["CirculationTypeCode"][$n][$i]),	            
				'barcode'=> CCTOSQL($BookInfoArr["BarCode"][$n][$i]),
				'BookLocation'=> CCTOSQL($BookInfoArr["LocationCode"][$n][$i]),
				'AccountDate'=> CCTOSQL($BookInfoArr["CreationDate"][$n][$i]),
				'PurchaseDate'=> CCTOSQL($BookInfoArr["PurchaseDate"][$n][$i]),
				'PurchasePrice'=>CCTOSQL( $BookInfoArr["PurchasePrice"][$n][$i]),
				'ListPrice'=> CCTOSQL($BookInfoArr["ListPrice"][$n][$i]),
				'Discount'=>CCTOSQL( $BookInfoArr["Discount"][$n][$i]),
				'Distributor'=> CCTOSQL($BookInfoArr["Distributor"][$n][$i]),
				'PurchaseByDepartment'=> CCTOSQL($BookInfoArr["PurchaseByDepartment"][$n][$i]),
				'PurchaseNote'=> CCTOSQL($BookInfoArr["PurchaseNote"][$n][$i]),
				'InvoiceNumber'=> CCTOSQL($BookInfoArr["InvoiceNumber"][$n][$i]),
	       		'AccompanyMaterial'=> CCTOSQL($BookInfoArr["AccompanyMaterial"][$n][$i]),
				'RecordStatus'=> CCTOSQL($BookInfoArr["RecordStatus"][$n][$i]),
	       		'ItemSeriesNum'=> CCTOSQL($BookInfoArr["ItemSeriesNum"][$n][$i])
	       	);
	    	$insert_to_db=array_merge($insert_to_db1,$insert_to_db2);
	    	
	    	$libms->INSERT2TABLE('LIBMS_MARC_TMP_2', $insert_to_db);	
	    	$exist_bookTitle1 = check_exist_ACNO($BookInfoArr["ACNO"][$n][$i]);
	        	if (!empty($exist_bookTitle1)){
		            $exist_acno=true;		            
        		    $alert_exist_acno.=$BookInfoArr["ACNO"][$n][$i]."(".$BookInfoArr["BookTitle"][$n]." repeated by ".$exist_bookTitle1[0]["BookTitle"].")".", ";		
	            }
	        
//         	if($ACNO_tag[$BookInfoArr["ACNO"][$n][$i]]==true){
//		        $duplicate_acno=true;
//		        $alert_duplicate_acno.=$BookInfoArr["ACNO"][$n][$i].", ";				
//	        }
	    	$exist_bookTitle2 = check_exist_ACNO($BookInfoArr["BarCode"][$n][$i]);

                if (!empty($exist_bookTitle2)){
		            $exist_barcode=true;
		            $alert_exist_barcode.=$BookInfoArr["BarCode"][$n][$i]."(".$BookInfoArr["BookTitle"][$n]." repeated by ".$exist_bookTitle2[0]["BookTitle"].")".", ";				
	            }
//        	if($BarCode_tag[$BookInfoArr["BarCode"][$n][$i]]==true){
//		        $duplicate_barcode=true;
//		        $alert_duplicate_barcode.=$BookInfoArr["BarCode"][$n][$i].", ";				
//	        }
	
//            $ACNO_tag[$BookInfoArr["ACNO"][$n][$i]]=true;
//         	
//            $BarCode_tag[$BookInfoArr["BarCode"][$n][$i]]=true;	
            }	
		}
}
$book=$BookInfoArr["n"];
$line=$BookInfoArr["linenum"];
if($line==$totalline-1)$totalline--;
echo "$book|$line|$totalline|$alert_exist_acno|$alert_exist_barcode";
intranet_closedb();
?>
