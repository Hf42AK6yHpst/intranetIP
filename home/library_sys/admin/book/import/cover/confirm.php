<?php

/**
 * Modifying by 
 * 
 * Log :
 * 
 * Date:    2019-11-08 [Cameron]
 * Details: show all books of the same isbn number rather than show only one book
 * 
 * Date:	2014-12-10 [Henry Chan]
 * Details:	add option to allow cover image input by ISBN
 * 
 * Date:	2013-07-23 [Henry Chan]
 * Details:	finished the function of import book cover
 * 
 * Date:	2013-07-17 [Henry Chan]
 * Details:	File created
 * 			
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
	function magicQuotes_awStripslashes(& $value, $key) {
		$value = stripslashes($value);
	}
	$gpc = array (
		& $_GET,
		& $_POST,
		& $_COOKIE,
		& $_REQUEST
	);
	array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libimporttext.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/liblibrarymgmt.php");

include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");

include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libimage.php");
//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
	include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/

$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
//$PAGE_NAVIGATION[] = array (
//	$Lang['libms']['bookmanagement']['book_ist'],
//	"index.php"
//);
//$PAGE_NAVIGATION[] = array (
//	$Lang['libms']['general']['edit'],
//	""
//);
$PAGE_NAVIGATION[] = array (
	$Lang['libms']['bookmanagement']['book_list'],
	"../../index.php"
);
$PAGE_NAVIGATION[] = array (
	$Lang['libms']['import_book_cover'],
	""
);

$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

$lfs = new libfilesystem();

//$linterface = new interface_html();
############################################################################################################
///*

global $eclass_prefix;

$toolbar = ''; //$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array (
	$Lang['libms']['import_book_cover']
);

# step information
$STEPS_OBJ[] = array (
	$Lang['libms']['select_image_file'],
	0
);
$STEPS_OBJ[] = array (
	$iDiscipline['Confirmation'],
	1
);
$STEPS_OBJ[] = array (
	$i_general_imported_result,
	0
);

//===================================================================================
$libms->db_db_query("delete from `LIBMS_CSV_TMP`");

## ADD PEAR in the INCLUDE path
set_include_path(get_include_path() . PATH_SEPARATOR . $intranet_root . '/home/library_sys/admin/book/import/pear/');

include ('../api/class.upload.php');

//if ((isset($_POST['action']) ? $_POST['action'] : (isset($_GET['action']) ? $_GET['action'] : '')) == 'simple') {

$handle = new Upload($_FILES['file']);
//debug_pr();
$ext = strtoupper($handle->file_src_name_ext);

//file type checking
if ($ext != "JPG" && $ext != "PNG" && $ext != "ZIP" && $ext != "JPEG") {
	intranet_closedb();
	header("location: index.php?xmsg=import_failed");
	exit ();
}
$uniDirName = uniqid();
//check whether the file is uploaded successfully
if ($handle->uploaded) {
	$handle->Process($intranet_root . "/file/lms_book_cover_import/" . $uniDirName . "/");
	if ($handle->processed) {
		// everything was fine !
		$imagefile = $intranet_root . "/file/lms_book_cover_import/" . $uniDirName . "/" . $handle->file_dst_name;
		//$x .= 'upload success!';
		$uploadSuccess = true;
	} else {
		// one error occured
		$xmsg = $Lang['libms']['import_book']['upload_fail'] . $handle->error . $Lang['libms']['import_book']['contact_admin'];
		//$x .= 'upload unsuccess!';
		$uploadSuccess = false;
	}
}

if ($uploadSuccess && ($ext == "JPG" || $ext == "PNG" || $ext == "JPEG" || $ext == "ZIP")) {
	$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"2\">#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"30%\">" . $Lang['libms']['bookmanagement']['bookTitle'] . "</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"20%\">" . ($filenameFormat == 2?$Lang["libms"]["book"]["ISBN"]:$Lang["libms"]["book"]["code"]) . "</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"20%\">" . $Lang['General']['FileNames'] . "</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"28%\">" . $Lang['General']['Error'] . "</td>";
	$x .= "</tr>";

	$dirPath = $intranet_root . "/file/lms_book_cover_import/" . $uniDirName;

	//unzip the file
	if ($ext == "ZIP") {
		$lfs->file_unzip($imagefile, $dirPath);
		unlink($imagefile);
	}

	$handle->clean();
	//remove the zip file

	//check the file type of the unzipped file

	$files = glob($dirPath . '/*');

	//get the lis of file name
	$counter = 0;
	$correctFileNo = 0;
	foreach ($files as $file) {
		$counter++;
		if (is_file($file)) {
			$uploadError = 0;
			if (!@ exif_imagetype($file)) {
				$uploadError = 1;
			}
			//show the result of the validation

			$file1 = basename($file);
			$file1 = iconv('big5', 'utf-8', strtoupper(substr($file1, 0, strrpos($file1, '.'))));

			$libms = new liblms();
			
			$bookIDAry = array();
			if($filenameFormat == 2) {     // by ISBN
			    $tempBookIDs = $libms->SELECTVALUE('LIBMS_BOOK', 'BookID', 'BookID', 'TRIM(ISBN) = "' . $file1 . '" AND TRIM(ISBN) <> ""');
			    if (count($tempBookIDs)) {
			        $bookIDAry = Get_Array_By_Key($tempBookIDs,'BookID');
			    }
			}
			else {     // by ACNO
			    $tempBookIDs = $libms->SELECTVALUE('LIBMS_BOOK_UNIQUE', 'BookID', 'BookID', 'TRIM(ACNO) = "' . $file1 . '"');
			    if (count($tempBookIDs)) {
			        $bookIDAry = array($tempBookIDs[0]['BookID']);
			    }
			}
			
			if (count($bookIDAry) == 0) {
				$css = "red";
				$x .= "<tr class=\"tablebluerow" . $counter . "\">";
				$x .= "<td class=\"$css\">" . $counter . "</td>";
				$x .= "<td class=\"$css\">" . $Lang['General']['EmptySymbol'] . "</td>";
				$x .= "<td class=\"$css\">" . $Lang['General']['EmptySymbol'] . "</td>";
				$x .= "<td class=\"$css\">" . iconv('big5', 'utf-8',basename($file)) . "</td>";
				$x .= "<td class=\"$css\">";
				$x .= ($filenameFormat == 2?$Lang['libms']['invalid_isbn']:$Lang['libms']['invalid_acno']);
				$x .= "</td>";
				$x .= "</tr>";

			} else {
				if (!$uploadError) {
					$correctFileNo++;
					$css = "tabletext";
					$ext = $Lang['General']['EmptySymbol'];
				} else {
				    $ext = $Lang['plupload']['invalid_file_ext'];
				    $css = "red";
				}
				
				$bookNameAry = $libms->SELECTVALUE('LIBMS_BOOK', 'BookTitle', 'BookID', "BookID IN ('" . implode("','",(array)$bookIDAry) . "')");
				
				if (count($bookNameAry)) {
				    foreach($bookNameAry as $_bookNameAry) {
				        $x .= "<tr class=\"tablebluerow" . $counter . "\">";
				        $x .= "<td class=\"$css\">" . $counter . "</td>";
				        $x .= "<td class=\"$css\">" . $_bookNameAry[BookTitle] . "</td>";
				        $x .= "<td class=\"$css\">" . $file1 . "</td>";
				        $x .= "<td class=\"$css\">" . iconv('big5', 'utf-8',basename($file)) . "</td>";
				        $x .= "<td class=\"$css\">";
				        $x .= $ext;
				        $x .= "</td>";
				        $x .= "</tr>";
				        $counter++;
				    }
				}
			}		
		}
	}

} else
	$x = $Lang["libms"]["import"]['BookCat']['error_msg']['upload_fail'].'!';

if ($correctFileNo == 0) {
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");
	$lfs->deleteDirectory($intranet_root . "/file/lms_book_cover_import/".$uniDirName);
} else
	if ($uploadError || !$bookIDs) {
		//$x .= "</table>";
		//$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");
		//	foreach ($files as $file) { // iterate files
		//		if (is_file($file))
		//			unlink($file); // delete file
		//	}
		//	rmdir($dirPath);
		//deleteDirectory($intranet_root . "/file/lms_book_cover_import");
		$import_button = $linterface->GET_ACTION_BTN($button_import, "submit") . " &nbsp;" . $linterface->GET_ACTION_BTN($button_back, "button", "window.location='result.php?remove_temp=1&uni_dir_name=" . $uniDirName . "'");
	} else {
		$import_button = $linterface->GET_ACTION_BTN($button_import, "submit") . " &nbsp;" . $linterface->GET_ACTION_BTN($button_back, "button", "window.location='result.php?remove_temp=1&uni_dir_name=" . $uniDirName . "'");
		//	$prescan_result = $Lang['libms']['import_book']['upload_success_ready_to_import'] . count($files);
		//	$x = $prescan_result.$x;

	}
$x .= "</table>";
$prescan_result = $Lang["libms"]["import_book"]["upload_success_file_ready_to_import"].' '. $correctFileNo . ' (' . $Lang['libms']['bookmanagement']['total']. ': ' . count($files) . ')<br/><br/>';
$x = $prescan_result . $x;

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
?>


<form name="form1" action="result.php" method="POST">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><?=$x?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $import_button ?>
			</td>
		</tr>
	</table>
	<input type='hidden' name='uniDirName' value='<?=$uniDirName?>'/>
	<input type='hidden' name='filenameFormat' value='<?=$filenameFormat?>'/>
</form>
<br /><?php


//unlink($csvfile);

$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();
//unset($_SESSION['file']);
?>
