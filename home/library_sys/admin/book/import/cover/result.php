<?php

/**
 * Modifying by
 * 
 * Log :
 *
 * Date:    2019-10-16 [Cameron]
 * Details: can import the same book cover image for the same ISBN but in different bibliography [case #C89767]
 *
 * Date:	2014-12-10 [Henry Chan]
 * Details:	add option to allow cover image input by ISBN
 * 
 * Date:	2014-09-18 [Henry Chan]
 * Details:	commented unused code
 * 
 * Date:	2013-07-23 [Henry Chan]
 * Details:	finished the function of import book cover
 * 
 * Date:	2013-07-17 [Henry Chan]
 * Details:	File Created
 * 
 */

/*if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}*/

$PATH_WRT_ROOT = "../../../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libimporttext.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($PATH_WRT_ROOT . "includes/libelibrary.php");
include_once ($PATH_WRT_ROOT . "includes/libimage.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include ('../api/class.upload.php');
//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$lfs = new libfilesystem();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
	include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
 if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
 {
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
$laccessright = new libaccessright();
$laccessright->NO_ACCESS_RIGHT_REDIRECT();
exit;
}
*/

if ($_REQUEST['remove_temp']) {
	//	$dirPath = $intranet_root . "/file/lms_book_cover_import/" . $_REQUEST['uni_dir_name'];
	//	$files = glob($dirPath . '/*');
	//	foreach ($files as $file) { // iterate files
	//		if (is_file($file))
	//			unlink($file); // delete file
	//	}
	//	rmdir($dirPath);
	$lfs->deleteDirectory($intranet_root . "/file/lms_book_cover_import/".$_REQUEST['uni_dir_name']);
	header("Location: index.php");
	exit;
}
$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
//$PAGE_NAVIGATION[] = array (
//	$Lang['libms']['bookmanagement']['book_ist'],
//	"index.php"
//);
//$PAGE_NAVIGATION[] = array (
//	$Lang['libms']['general']['edit'],
//	""
//);
$PAGE_NAVIGATION[] = array (
	$Lang['libms']['bookmanagement']['book_list'],
	"../../index.php"
);
$PAGE_NAVIGATION[] = array (
	$Lang['libms']['import_book_cover'],
	""
);

$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*

global $eclass_prefix;

$toolbar = ''; //$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array (
	$Lang['libms']['import_book_cover']
);

# step information
$STEPS_OBJ[] = array (
	$Lang['libms']['select_image_file'],
	0
);
$STEPS_OBJ[] = array (
	$iDiscipline['Confirmation'],
	0
);
$STEPS_OBJ[] = array (
	$i_general_imported_result,
	1
);

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button", "self.location='../../'") . "&nbsp;" . $linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='index.php'");

//===================================================================================

$dirPath = $intranet_root . "/file/lms_book_cover_import/" . $uniDirName;
$files = glob($dirPath . '/*');
$counter = 0;
foreach ($files as $file) {
	//$counter++;
	$file1 = basename($file);
	$file1 = iconv('big5', 'utf-8',strtoupper(substr($file1, 0, strrpos($file1, '.'))));
	//debug_pr($file);
	//add the copy image script here...
	//--------------------------------------------------   upload file processing  [START]	--------------------------------------------------	
	$debug123 = 0;
	if ($debug123 == 0) {
		////////////////////update cover image////////////////////		
		$libms = new liblms();
		//$Code = '11501';
		//$bookIDs = $libms->SELECTVALUE('LIBMS_BOOK', 'BookID', 'BookID', 'BookCode="' . $file1 . '"');
		if($filenameFormat == 2)
			$bookIDs = $libms->SELECTVALUE('LIBMS_BOOK', 'BookID', 'BookID', 'TRIM(ISBN) = "' . $file1 . '" AND TRIM(ISBN) <> "" AND (RecordStatus <> "DELETE" OR RecordStatus IS NULL)');
		else
			$bookIDs = $libms->SELECTVALUE('LIBMS_BOOK_UNIQUE', 'BookID', 'BookID', 'TRIM(ACNO) = "' . $file1 . '"');
		//$bookIDs = $libms->SELECTVALUE('LIBMS_BOOK', 'BookID', 'BookID', 'BookID="' . $tempBookIDs[0]['BookID'] . '"');

        $handle = new Upload($file);
        $isFileUploaded = false;

		foreach ($bookIDs as $bookID) {
			$Code = $bookID[BookID];
			//debug_pr($bookIDs[0][BookID]);

			$result = $libms->SELECTVALUE('LIBMS_BOOK', 'CoverImage', 'BookID', 'BookID=' . $Code);
			$coverimg_path = $result[0][CoverImage];

			//if (!empty($BookCover)) {

			if (!empty ($coverimg_path) && file_exists($coverimg_path)) {
				unlink($coverimg_path);
			}

			$count = (int) ($Code / 2000);
			
			if ($handle->uploaded) {
				$cover_folder = $intranet_root . "/file/lms/cover/" . $count . "/" . $Code;
				$handle->Process($cover_folder);
				if (!@ exif_imagetype($handle->file_dst_pathname)) {
				}
				else if ($handle->processed) {
					//	$lfs->item_copy($file, $cover_folder);
					//	
					//	//require('../api/class.upload.php');
					//	//for resizing the image
					//	$image_obj = new SimpleImage();
					//	$image_obj->load($cover_folder.basename($file));
					//	$image_obj->resizeToMax(140, 200);
					//	$image_obj->save($cover_folder.basename($file));
					//	$cover_image = str_replace($intranet_root, "", $cover_folder.basename($file));
					//	$dataAry['CoverImage'] = $cover_image;
					$uploadSuccess = true;
					//$cover_image = str_replace("../../../../../intranetdata", "", $handle->file_dst_pathname);
					$image_obj = new SimpleImage();
					$image_obj->load($handle->file_dst_pathname);
					$image_obj->resizeToMax(140, 200);
					$image_obj->save($handle->file_dst_pathname);
					//debug_pr($handle->file_dst_pathname);
					$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
					//debug_pr($cover_image);
					$dataAry['CoverImage'] = $cover_image;
					$counter++;
				} else {
					// one error occured
					$xmsg = $Lang['libms']['import_book']['upload_fail'] . $handle->error . $Lang['libms']['import_book']['contact_admin'];
					$uploadSuccess = false;
					echo $xmsg;
					exit;
				}

                $isFileUploaded = true;
			}

			if (@ exif_imagetype($handle->file_dst_pathname)) {
			//Remove other old cover image in the directory
			$files2 = glob($cover_folder . '/*');
			foreach ($files2 as $file2) { // iterate files
				//debug_pr($file1);
				//debug_pr(basename($file));
				if (is_file($file2) && basename($handle->file_dst_pathname) != basename($file2))
					unlink($file2); // delete file
			}
			//End reomove other old cover image in the directory	
			}
			else{
				$files2 = glob($cover_folder . '/*');
				foreach ($files2 as $file2) { // iterate files
					//debug_pr($file1);
					//debug_pr(basename($file));
					if (is_file($file2) && basename($handle->file_dst_pathname) == basename($file2))
						unlink($file2); // delete file
				}
			}
			################## ACNO suggestion LOG insert/update ##################

//			$dataAry['BookCode'] = str_replace('\\"', '', $BookCode);
//			$dataAry['BookCode'] = str_replace('"', '', $dataAry['BookCode']);
//
//			//$dataAry['BookCode'] = 'b00999993';
//			//debug_pr(basename($file).'\n'.$file1);
//
//			$dataAry['BookCode'] = $file1;
//
//			preg_match('/^([a-zA-Z]+)(\d+)$/', $dataAry['BookCode'], $matches);
//
//			if (!empty ($matches)) {
//				$prefix = $matches[1];
//				$number = $matches[2];
//
//				$sql = "SELECT `Key`, `Next_Number` FROM  `LIBMS_ACNO_LOG` WHERE `Key` LIKE '{$prefix}' AND  `Next_Number` > '{$number}' ";
//				$result = $libms->returnArray($sql);
//				if (empty ($result)) {
//					$nextnum = $number +1;
//					$sql = "INSERT INTO `LIBMS_ACNO_LOG` (`Key`, `Next_Number`) VALUES ('{$prefix}','{$nextnum}') ON DUPLICATE KEY UPDATE `Next_Number`= '{$nextnum}'";
//					//$sql = "UPDATE `LIBMS_ACNO_LOG` SET `Next_Number`= '{$number}'  WHERE `Key` LIKE '{$prefix}' LIMIT 1";
//					if (@ exif_imagetype($handle->file_dst_pathname)) {
//					$libms->db_db_query($sql);
//					}
//				}
//				$dataAry['ACNO_Prefix'] = htmlspecialchars($prefix);
//				$dataAry['ACNO_Num'] = htmlspecialchars($number);
//			} else {
//				intranet_closedb();
//				header("Location: index.php?xmsg=update_failed");
//				die();
//
//			}
			#################### end of ACNO suggestion LOG insert/update ##################

			foreach ($dataAry as $key => & $feild) {
				$feild = PHPToSQL($feild);
			}

			if (@ exif_imagetype($handle->file_dst_pathname)) {
                $result = $libms->UPDATE2TABLE("LIBMS_BOOK", $dataAry, array (
                    "BookID" => $Code
                ));
			}
			////////////////////end update cover image////////////////////
		}

		if ($isFileUploaded) {
            // we delete the temporary files
            $handle->Clean();
        }

	}
	//--------------------------------------------------   upload file processing [END] --------------------------------------------------	
	//------------------------------- something should move to the result page [END] ----------------------------------------------	
}

foreach ($files as $file) { // iterate files
	if (is_file($file))
		unlink($file); // delete file
}
rmdir($dirPath);

//debug_pr($dirPath);

$x = $Lang['libms']['import_book']['Result']['Success'] . ", " . $Lang['libms']['import_book']['total_record'] . ": " . $counter;

//$i = 1;

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
?>

<form name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center"><br /><?= $x ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5"
					align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><?=$import_button?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form><?php


$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();
?>