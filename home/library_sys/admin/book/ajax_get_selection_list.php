<?php
/* 
 * Editing by  
 * 
 * 	2016-02-18 [Cameron]
 * 		- add case get_BookResponb1 ~ get_BookResponb3
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
########################################################################################################

$action = (isset($action) && $action != '') ? $action : '';


## Use Library
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']  )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$data = array();


switch($action){
	case 'get_ACNO':
		$sql = "select `Key`, `Next_Number`, 
					case when `Key` = '".$libms->acno_default_sys_prefix."' then 1 
					else 2 end as ordering 
				from LIBMS_ACNO_LOG order by ordering, `Key` ";
		$result = $libms->returnArray($sql);
		
		for($i=0 ; $i<count($result) ; $i++){
			if($result[$i]['Key'] == $libms->acno_default_sys_prefix){
				$value = $result[$i]['Next_Number'];
			} else {
				$value = $result[$i]['Key'].$result[$i]['Next_Number'];
			}
			$data[] = array('value' => handle_chars($value), 'title' => handle_chars($value));
		}
		break;
	case 'get_Distributor':
		$sql = "select distinct Distributor from LIBMS_BOOK_UNIQUE order by Distributor ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookLang':
		$sql = "select distinct Language from LIBMS_BOOK where (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL) and Language is not null and Language <> '' order by Language ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookCountry':
		$sql = "select distinct Country from LIBMS_BOOK where Country is not null and Country <> '' order by Country ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookPublisher':
	case 'get_PublisherTb':
		$sql = "select distinct Publisher from LIBMS_BOOK where Publisher is not null and Publisher <> '' order by Publisher ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookPublishplace':
	case 'get_PublishPlaceTb':
		$sql = "select distinct PublishPlace from LIBMS_BOOK where PublishPlace is not null and PublishPlace <> '' order by PublishPlace ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookSeries':
		$sql = "select distinct Series from LIBMS_BOOK where Series is not null and Series <> '' order by Series ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookSubj':
		$sql = "select distinct Subject from LIBMS_BOOK where (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL) and Subject is not null and Subject <> '' order by Subject ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_Purchasedept':
	case 'get_PurchaseByDepartmentTb':
		$sql = "select distinct PurchaseByDepartment from LIBMS_BOOK_UNIQUE where PurchaseByDepartment is not null and PurchaseByDepartment <> '' order by PurchaseByDepartment ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookResponb1':
		$sql = "select distinct ResponsibilityBy1 from LIBMS_BOOK where ResponsibilityBy1 is not null and ResponsibilityBy1 <> '' order by ResponsibilityBy1 ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookResponb2':
		$sql = "select distinct ResponsibilityBy2 from LIBMS_BOOK where ResponsibilityBy2 is not null and ResponsibilityBy2 <> '' order by ResponsibilityBy2 ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	case 'get_BookResponb3':
		$sql = "select distinct ResponsibilityBy3 from LIBMS_BOOK where ResponsibilityBy3 is not null and ResponsibilityBy3 <> '' order by ResponsibilityBy3 ";
		$result = $libms->returnArray($sql);
		
		if(!empty($result)){
			foreach ($result as $row){
				$data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
			}
		}
		break;
	
}

function handle_chars($str){
	$x = '';
	if($str != ''){
		$x = str_replace("\\", "\\\\", $str);
	}
	return $x;
}


echo json_encode($data);

?>