<?php
// using: Siuwan

/************************************
 * 
 * Date:	2013-08-21 (Henry)
 * Details: add Distributer filter
 * 
 ************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
################################################################################################################

### Set Cookies
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_item_page_field", "field");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_order", "order");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_number", "pageNo");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_keyword", "keyword");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_book_status", "BookStatus");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}
################################################################################################################

## Get Data
$BookStatus = (isset($BookStatus) && $BookStatus != '') ? $BookStatus : '';
$keyword = (isset($keyword) && $keyword != '') ? trim($keyword) : '';

if (isset($elibrary_plus_mgmt_book_page_size) && $elibrary_plus_mgmt_book_page_size != "") 
	$page_size = $elibrary_plus_mgmt_book_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

## Use Library
# in all boook management PHP pages
$libms = new liblms();

# Access Checking
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$li = new libdbtable2007($field, $order, $pageNo);


## Init 
$btnOption = ''; 
$FromPage = 'item';

## Preparation
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementItemList";

$TAGS_OBJ[] = array($Lang["libms"]["action"]["item"]);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);


$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(i.UniqueID USING utf8)" : "i.UniqueID";

## DB SQL
$sql = "select 
			i.ACNO, 
			CONCAT('<a href=\"book_item_edit.php?FromPage=".$FromPage."&UniqueID=',".$BookID_For_CONCAT.",'\">',b.BookTitle,'</a>') as BookTitleLink,  
			i.BarCode, 
			i.Distributor, ";
# Column - Book Status 
$sql .= "	case ";
if(!empty($Lang["libms"]["book_status"])){
	foreach($Lang["libms"]["book_status"] as $status_key => $status_val){
		$sql .= "	when i.RecordStatus = '".$status_key."' then '".$status_val."' ";
	}
}
$sql .= "	else '--' end as RecordStatus, ";
$sql .= "	IF(i.CreationDate <> '0000-00-00' and i.CreationDate <> '', DATE_FORMAT(i.CreationDate, '%Y-%m-%d'), '') as CreationDate, 
			CONCAT('<input type=\"checkbox\" name=\"UniqueID[]\" id=\"UniqueID[]\" value=\"', i.UniqueID ,'\">') as checkbox, 
			b.BookTitle 
		from LIBMS_BOOK_UNIQUE as i 
		inner join LIBMS_BOOK as b on 
			b.BookID = i.BookID 
		where (i.RecordStatus NOT LIKE 'DELETE' OR i.RecordStatus IS NULL) ";
# DB - condition
$bookStatus_cond = '';
if($BookStatus != ''){
	$sql .= " and i.RecordStatus = '".$BookStatus."' ";
	
	//for Distributor filter
	$bookStatus_cond = " and i.RecordStatus = '".$BookStatus."' ";
}
$keyword_cond ='';
if($keyword != "")
	$keyword_sql = mysql_real_escape_string($keyword);
	$sql .= " AND (
		i.BarCode LIKE '%$keyword_sql%' OR 
		i.ACNO LIKE '%$keyword_sql%' OR 
		b.BookTitle LIKE '%$keyword_sql%' 
	) "; 

	//for Distributor filter
	$keyword_cond = " AND (
		i.BarCode LIKE '%$keyword_sql%' OR 
		i.ACNO LIKE '%$keyword_sql%' OR 
		b.BookTitle LIKE '%$keyword_sql%' 
	) "; 
//echo $sql;die();

## Location Code Sql Cond
if($LocationCode != ''){
	if($LocationCode =='NA'){
		$sql .= " AND (i.LocationCode = '' OR i.LocationCode is null)" ;
	}else{
		$sql .= " AND i.LocationCode = '".$LocationCode."' " ;
	}
}

## DistributorSelect Sql Cond
if($DistributorSelect != ''){
	if($DistributorSelect =='NA'){
		$sql .= " AND (i.Distributor = '' OR i.Distributor is null)" ;
	}else{
		$sql .= " AND i.Distributor = '".$DistributorSelect."' " ;
	}
}

## Data Table
//global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

// BookdCode = ACNO 
$li->field_array = array("ACNO", "BookTitle", "BarCode", "Distributor", "RecordStatus", "CreationDate");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['book']['code'])."</td>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['libms']['book']['title'])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['book']['barcode'])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['book']['distributor'])."</td>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['bookStatus'])."</td>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang['libms']['book']['item_account_date'])."</td>\n";
//$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang["libms"]["book"]["number_of_copy_available"]."/".$Lang['libms']['book']['number_of_copy'])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("UniqueID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("book_item_edit.php?FromPage=".$FromPage,$button_new,"","","",0);
//$toolbar .= $linterface->GET_LNK_IMPORT("import/csv/import_csv_data.php",$button_import,"","","",0);
//if ($plugin["elib_plus_demo"])
//	$toolbar .= $linterface->GET_LNK_IMPORT("import/cover/index.php",$Lang['libms']['import_book_cover'],"","","",0);
$keyword = htmlspecialchars($keyword);
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" />";	// onkeyup=\"Check_Go_Search(event);\"



############################################################################################################

# Top menu highlight setting
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

# Table Action Button
# Edit button
$btnOption .= "<a href=\"javascript:checkEdit(document.form1,'UniqueID[]','book_item_edit.php?FromPage=".$FromPage."')\" class=\"tool_edit\">" . $button_edit . "</a>";

# Copy button
$btnOption .= "<a href=\"javascript:checkEdit(document.form1,'UniqueID[]','book_item_edit.php?FromPage=".$FromPage."&IsCopy=1')\" class=\"tool_copy\">" . $button_copy . "</a>";

# Del button
$btnOption .= "<a href=\"javascript:checkRemove(document.form1,'UniqueID[]','item_remove.php?FromPage=".$FromPage."')\" class=\"tool_delete\">" . $button_delete . "</a>";



# Fileter - Book Status
$ParTags = ' id="BookStatus" name="BookStatus" onchange="document.form1.submit()"';
$BookStatusSelection = $libms->GET_BOOK_ITEM_SELECTION($ParTags, '', $BookStatus);

# Location filter 
$locationSelectionBox = '';
$locationInfo = $libms->GET_LOCATION_INFO('',1);
$numOfLocationInfo = count($locationInfo);
$locationSelectionOption = array();

$locationSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);

for($i=0;$i<$numOfLocationInfo;$i++){

	$thisLocationCode = $locationInfo[$i]['LocationCode'];
	$thisLocationEngName = $locationInfo[$i]['DescriptionEn'];
	$thisLocationChiName = $locationInfo[$i]['DescriptionChi'];
	$thisOptionlocationName = Get_Lang_Selection($thisLocationChiName,$thisLocationEngName);
	$locationSelectionOption[]= array($thisLocationCode,$thisOptionlocationName);
}
$locationSelectionBox = $linterface->GET_SELECTION_BOX($locationSelectionOption, 'name="LocationCode" id="LocationCode" onchange="document.form1.submit()"', "-- {$Lang['libms']['bookmanagement']['location']} --", $LocationCode);
$BookStatusSelection .= $locationSelectionBox;

# Distributor filter
$distributorSelectionBox = '';

$sql1 = "select DISTINCT i.Distributor
		from LIBMS_BOOK_UNIQUE as i 
		inner join LIBMS_BOOK as b on 
			b.BookID = i.BookID 
		where (i.RecordStatus NOT LIKE 'DELETE' OR i.RecordStatus IS NULL) 
		{$bookStatus_cond}
		{$keyword_cond}";
//debug_pr($sql1);
$distributorInfo = $libms->returnArray($sql1);

//--Should add to the lib
//function GET_DISTRIBUTOR($bookStatus_cond, $keyword_cond){
//	$sql = "select DISTINCT i.Distributor
//		from LIBMS_BOOK_UNIQUE as i 
//		inner join LIBMS_BOOK as b on 
//			b.BookID = i.BookID 
//		where (i.RecordStatus NOT LIKE 'DELETE' OR i.RecordStatus IS NULL) 
//		{$bookStatus_cond}
//		{$keyword_cond}";
//			
//	return $this->returnArray($sql);
//}

//$publisherInfo = GET_PUBLISHER($conds, $locationCode_cond);
$numOfDistributorInfo = count($distributorInfo);
$distributorSelectionOption = array();

$distributorSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);

for($i=0;$i<$numOfDistributorInfo;$i++){
	if($distributorInfo[$i]['Distributor'] != '')
	$distributorSelectionOption[]= array($distributorInfo[$i]['Distributor'], $distributorInfo[$i]['Distributor']);
}
$distributorSelectionBox = $linterface->GET_SELECTION_BOX($distributorSelectionOption, 'name="DistributorSelect" id="DistributorSelect" onchange="document.form1.submit()"', "-- {$Lang['libms']['book']['distributor']} --", $DistributorSelect);
$BookStatusSelection .= $distributorSelectionBox;
?>
<script language="javascript">

</script>

<form name="form1" id="form1" method="POST" action="item_list.php" onSubmit="return checkForm()">
	<div class="content_top_tool"  style="float: left;">
		<?=$toolbar?>
		</div>
		<div class="Conntent_search"><?=$searchTag?></div>     
		<br style="clear:both" />

	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="bottom">
		<?=$BookStatusSelection?>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
		<?=$btnOption?>
		</div>
	</td>
	</tr>
	</table>

	<?=$li->display();?>
	
	<br />
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
	<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
	<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
	<input type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
	
</form>


<?php
$linterface->LAYOUT_STOP();

################################################################################################################
intranet_closedb();
?>