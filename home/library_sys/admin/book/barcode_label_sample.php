<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

require_once('LabelPrinterTCPDF.php');

$label_format =array(
		'name' => 'A200',
		'paper-size' => array(205, 165),
		'metric' => 'mm',
		'lMargin' => 1.25,
		'tMargin' => 3.5,
		'NX' => 2, 'NY' => 4,
		'SpaceX' => 2,
		'SpaceY' => 2,
		'width' => 100,
		'height' => 38,
		'font-size' => 10,
		'printing_margin_h' => 2,
		'printing_margin_v' => 1,
		'max_barcode_width' => 90,
		'max_barcode_height' => 30
);

$pdf = new LabelPrinterTCPDF($label_format);

$barcode = 'X';


for($i=1; $i<=20; $i++){
	$barcode .= $i;
	$pdf->AddLabel(array('+/line 1 '.$i,'第貳行 2 '.$i,'第三行 3 '.$i ),$barcode.$i,true);
	
}

$pdf->Output('example_001.pdf', 'I');
