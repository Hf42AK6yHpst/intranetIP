<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']  )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$data = array();

if(isset($_REQUEST['q']))//If a q has been submitted 
{
	$q = mysql_real_escape_string($_REQUEST['q']);//Some clean up
	
	$sql = "SELECT `BookTitle` FROM  `LIBMS_BOOK` WHERE `BookTitle` LIKE '%{$q}%' and 
				(`RecordStatus` is null || `RecordStatus` not in ('DELETE')) ";
	$result = $libms->returnArray($sql,null,2);
	
	//$array[][0] => array[]
	foreach ($result as $row){
		$data[] = $row[0];
	}
}

echo json_encode($data);
