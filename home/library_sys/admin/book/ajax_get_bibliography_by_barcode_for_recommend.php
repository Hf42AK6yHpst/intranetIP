<?php
/*
 * 	modifying: 
 * 	Log
 * 	Purpose:	get a row of book record by barcode
 *	@return:	array['html']		-- a table row of record
 *				array['success']	-- successful / failed 
 *
 * 	Date:	2016-11-14 [Cameron] 
 * 			- Add filter condition "b.`NoOfCopy`>0 AND b.`OpenBorrow`=1" (same effect as INTRANET_ELIB_BOOK.Publish=1) in sql
 * 			- Use INTRANET_ELIB_BOOK.BookID for BookID
 *  	 
 * 	Date:	2016-09-22 [Cameron] create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
$physical_book_init_value = $lelibplus->physical_book_init_value ? $lelibplus->physical_book_init_value : 10000000;
$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK_RECOMMEND WHERE BookID>".$physical_book_init_value. " ORDER BY BookID";
$recommend_list = $lelibplus->returnVector($sql);

$barcode = $_REQUEST['barcode'];
$ID = $_REQUEST['ID'];
$ret = array();

$sql = "SELECT 
			IF(b.BookTitle IS NULL OR b.BookTitle = '','".$Lang['General']['EmptySymbol']."',b.BookTitle) AS BookTitle,
			IF((b.CallNum='' OR b.CallNum is null) AND (b.CallNum2='' OR b.CallNum2 is null),'".$Lang['General']['EmptySymbol']."',
				CONCAT(IF(b.CallNum IS NULL,'',b.CallNum), ' ', IF(b.CallNum2 IS NULL,'',b.CallNum2))) as CallNumber,
			b.BookID + ".$physical_book_init_value." AS BookID
		FROM 
			LIBMS_BOOK as b 
		INNER JOIN LIBMS_BOOK_UNIQUE as bu ON bu.BookID=b.BookID
		WHERE bu.BarCode='".$barcode."' 
		AND b.`NoOfCopy`>0 AND b.`OpenBorrow`=1
		LIMIT 1";	

$rs = $libms->returnResultSet($sql);

if(count($rs)>0){
	$curr_rs = $rs[0];
	$bookTitle = in_array($curr_rs['BookID'],(array)$recommend_list) ? $curr_rs['BookTitle']."<span class=\"tabletextrequire\"> * </span>" : $curr_rs['BookTitle'];
	$x = '<tr id="'.$curr_rs['BookID'].'">
				<td>'.$ID.'</td>
				<td>'.$bookTitle.'</td>
				<td>'.$curr_rs['CallNumber'].'</td>
				<td><input type="checkbox" id="BookID[]" name="BookID[]" value="' . $curr_rs['BookID'] . '" checked /></td>
			</tr>';
	$ret['success'] = true;		
	$ret['BookID'] = $curr_rs['BookID'];	
}
else{
	$x = '';
	$ret['success'] = false;
	$ret['BookID'] = '';
}

$ret['html'] = remove_dummy_chars_for_json($x);
$json = new JSON_obj();
echo $json->encode($ret);

intranet_closedb();
?>