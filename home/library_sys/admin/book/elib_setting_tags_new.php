<?php

# being modified by: 
/*
 * Change log:
 * 
 * 201308-12 Yuen
 * 		fixed the search function by correcting the action path of the form 
 */

 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$LibeLib = new elibrary();

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageEBookTags";

$NavTitle = ($TagID!="" && $TagID>0) ? $Lang["libms"]["general"]["edit"] : $Lang["libms"]["general"]["new"];
$PAGE_NAVIGATION[] = array($NavTitle, "");

if ($TagID!="" && $TagID>0)
{
	$TagName = $LibeLib->returnTagNameByID($TagID);
	$button_save_or_submit = $button_save;
} else
{
	$button_save_or_submit = $button_submit;
}


//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['ebook_tags']);
if (!$plugin['eLib_Lite']){
	$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
}
$libms->MODULE_AUTHENTICATION($CurrentPage);

$TAGS_OBJ = $libms->get_ebook_tabs("TAGS");

$linterface = new interface_html();

$toolbar = $linterface->GET_LNK_ADD("elib_setting_tags_new.php",$button_new,"","","",0);



//*/
############################################################################################################

# Top menu highlight setting
$linterface->LAYOUT_START();
?>
<script language="javascript">
<?if ($plugin['eLib_Lite']){?>
	$( document ).ready(function(){
		$("#leftmenu").css("display","none");
		$("#content").css("padding-left","0px");
	});
<?}?>
function goSubmit()
{
	if (document.form1.TagName.value=="")
	{
		alert("<?=$eLib["SysMgr"]["tag_name"]?>");
		document.form1.TagName.focus();
		return false;
	}
	
	var bookLength = document.form1.elements['BookIDSelected[]'].length;
	for(i=0; i<bookLength; i++) {
		document.form1.elements['BookIDSelected[]'][i].selected = true;	
	}
		
	document.form1.submit();
}
</script>

<form name="form1" action="elib_setting_tags_new_update.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center">	
<tr>
	<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?>
	</td>
</tr>
</table>
	
	
<table width="92%" border="0" cellspacing="0" cellpadding="5" align="center">	
		<tr><td>
	<div class="table_board">
							<table class="form_table">
							  <tr>
							    <td class="formfieldtitle" nowrap="nowrap"><?=$Lang['StudentRegistry']['Tag']?> <span class="tabletextrequire">*</span></td>
							    <td >
								    <input name="TagName" id="TagName" type="text"  value="<?=$TagName?>"  />
							    </td>
							  </tr>
							  <tr>
							    <td class="formfieldtitle" nowrap="nowrap"><?=$eLib["html"]["book_of_tag"]?></td>
							    <td >
								   <table width="100%" border="0">
								   <tr><td><?= $LibeLib->DisplayTagBookSelections($TagID, true, "BookIDSelected[]") ?></td>
								   		<td>
								   		<?=
								   		$linterface->GET_BTN("<< ".$i_eNews_AddTo, "button", "checkOptionTransfer(this.form.elements['BookSource[]'],this.form.elements['BookIDSelected[]']);return false;", "submit2") . "<br /><br />" .
                        				$linterface->GET_BTN($i_eNews_DeleteTo . " >>", "button", "checkOptionTransfer(this.form.elements['BookIDSelected[]'],this.form.elements['BookSource[]']);return false;", "submit23")
                        				?>							   		
								   		
								   		</td>
								   		<td><?= $LibeLib->DisplayTagBookSelections($TagID, false, "BookSource[]") ?></td>
								   	</tr>
								   	</table>
							    </td>
							  </tr>
							</table>
							<span class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></span>
							<div class="edit_bottom">
	                          <p class="spacer"></p>
	                          <?= $linterface->GET_ACTION_BTN($button_save_or_submit, "button", "goSubmit()")?>
	                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='elib_setting_tags.php'")?>
								<p class="spacer"></p>
	                    </div>
	</div>
	</td></tr></table>
<input type="hidden" name="TagID" id="TagID" value="<?=$TagID?>" />
</form>

<?= $linterface->FOCUS_ON_LOAD("form1.TagName") ?>
<?php

$linterface->LAYOUT_STOP();
 
 
 
intranet_closedb();





?>