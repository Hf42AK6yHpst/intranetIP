<?php
/*
 * 	modifying: 
 * 	Log
 * 
 * 	Date:	2016-04-11 [Cameron] create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

function gen_confirm_script($data) {
	global $Lang,$i_Discipline_System_alert_remove_record;
	$x = '';
	if ($data['UserName']) {
		$msg = $Lang['libms']['bookmanagement']['ConfirmDeleteReservationAndAssignNext']."<br>".$Lang['libms']['bookmanagement']['NextReserve'].str_replace("\\","\\\\\\\\",$data['UserName']);
	}
	else {
		$msg = $i_Discipline_System_alert_remove_record;
	}
	ob_start();
?>	
<script>
jConfirm("<?=$msg?>",
		 "<?=$Lang["libms"]["SearchUser"]["CancelReserved"]?>", 
		 function(r){
		 	if (r) {
				checkRemove(document.form1,'Code[]','reserve_remove.php');		 		
		 	}
		 }, 
		 "<?=$Lang['libms']["settings"]['system']['yes']?>", 
		 "<?=$Lang['libms']["settings"]['system']['no']?>");
</script>
<?php
	$x = ob_get_contents();
	ob_end_clean();
	$x = str_replace("\n", '', $x);
	$x = str_replace("\r", '', $x);	
	$x = str_replace("\t", '', $x);
	return $x;
}


$BookID = trim($_POST['BookID']);
$Code = $_POST['Code'];		// ReservationID

$rs = $libms->getNextUserOfBookReservation($BookID,$Code);

$nrRs = count($rs);

$data = array();
if($nrRs>0){
	$data['UserName'] = $rs[0]['UserName'];		
}
else{
	$data['UserName'] = '';
}

$json['script'] = gen_confirm_script($data);
$json['success'] = TRUE;
echo json_encode($json);

intranet_closedb();
?>