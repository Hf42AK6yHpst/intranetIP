<?php

// Modifying by 

############ Change Log [Start]
#
#	Date:	2018-01-19 Cameron
#			add "Item Series Number" to print option for "Book Back" label [case #C134185]
#
#	Date:	2017-09-08 Cameron
#			add <label></label> for tag selection and print info selection so that it can be more easy to choose 
#
#	Date:	2017-07-18	Henry
#			support sort by BookID, ACNO, CallNum
#	Date:	2016-03-14	Henry
#			support chi and eng shcool name
#
#	Date:	2015-05-11	Henry
#			modified book_delete()
#
#	Date:	2013-09-24	Yuen
#			revised the ACNO comparison logic (not using ACNO_NUM anymore!)
#
############ Change Log [End] 

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 


$PATH_WRT_ROOT = "../../../../";


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage_ui.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html('libms.html');
$GroupManageUI = new liblibrarymgmt_group_manage_ui();

$result = $libms->SELECTFROMTABLE("LIBMS_LABEL_FORMAT",array('id','name'));

foreach ($result as $value)
	$Labels[] = array($value['name'] =>$value['id']);

$LabelFormatOption = $linterface->GET_SELECTION_BOX($result, " name='label_id' id='select_label_id' ",'','');

//$ReaderSelection = getSelectByArray
//$sql = "SELECT `GroupID` FROM `LIBMS_GROUP_USER` WHERE `UserID`={$c_user_id} order by DateModified desc LIMIT 1";
//		$result = $this->returnArray($sql);
$sql = "SELECT GroupID, GroupTitle FROM LIBMS_GROUP ORDER BY GroupTitle";
$result = $libms->returnArray($sql);
$ReaderGroupSelection = getSelectByArray($result,"name=\"readerGroup\" onChange=\"this.form.action='';this.form.target='';this.form.submit()\"",$readerGroup,0);
//$ReaderSelection = "The reader list";

if($readerGroup){
$Group = new liblibrarymgmt_group($readerGroup,false,true);
$memberList = $Group->MemberList;

////////////////////////////////////////////////////////////////
$ReaderSelection = '<tbody><tr class="tr_memberlist" valign="top">
						<td class="field_title book_selection">'.$Lang["libms"]["pre_label"]["PleaseSelectReader"].'</td>							
						<td>';
								
//		$ReaderSelection .= '<select name="MemberList[]" id="MemberList[]" size="10" style="min-width:200px;" MULTIPLE >';
//		for ($i = 0; $i < sizeof($memberList); $i++) {
//			list ($StudentID, $StudentName) = $memberList[$i];
//
////			if ($PrevClass != $ClassName) {
////				if ($i != 0) {
////					//$Return .= '</optgroup>';
////				}
////
////				//$Return .= '<optgroup label="' . $StudentName . '">';
////			}
//			//if(in_array($StudentID, $CheckedValue))
//				//$DefaultChecked = true;
//				
//			$ReaderSelection .= '<option value="' . $StudentID . '" ' . (($DefaultChecked) ? 'Selected' : '') . '>' . $StudentName . '</option>';
//			
//			//if(in_array($StudentID, $CheckedValue))
//				//$DefaultChecked = false;
//				
////			if ($i == (sizeof($StudentList) - 1)) {
////				//$Return .= '</optgroup>';
////			}
////			$PrevClass = $ClassName;
//		}
//		$ReaderSelection .= '</select>';
//		//$ReaderSelection .= '<input type="button" id="btn_usersearch" value="'.$Lang['Btn']['AddSelected'].'" onChange=\"this.form.action=\"\";this.form.target=\"\";this.form.submit()\" />';
				
////////////////////////////////////////////////////////////////

////////////////---------------------------------------------------//////////////////////////////
$GroupID = $readerGroup;
//$x = '<input type="hidden" name="GroupID" id="GroupID" value="'.$GroupID.'">';
$x = '
							    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
							    </td>
							</tr>    					
				              <tr>
				              	<td bgcolor="#EEEEEE">									
				';

		// get class list
//		$x .= '<select name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_User_List('.$GroupID.');" style="display:none;">';
//		$x .= '<option value="">'.$Lang['libms']['SubjectClassMapping']['SelectClass'].'</option>';
//		  for ($i=0; $i< sizeof($YearClassList); $i++) {
//		  	
//	
//		  	$x .= '<option value="'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'">'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
//		  }
//	  $x .= '</select>';


		$x .= $Lang["libms"]["GroupManagement"]["UserList"].'
														</td>
													<td width="40" style="border-bottom:0px"></td>
													<td width="50%" bgcolor="#EFFEE2" class="steptitletext" style="border-bottom:0px">'.$Lang['libms']['GroupManagement']['SelectedUser'].' </td>
												</tr>
												<tr>
												<td bgcolor="#EEEEEE" align="center" style="border-bottom:0px">';
		// get user list with selected criteria
		$x .= '							<div id="AvalUserLayer">';
		for ($i=0; $i< sizeof($Group->MemberList); $i++) {
			//$AddUserID[] = $Group->MemberList[$i]['UserID'];
		}
		//debug_pr($GroupManageUI->Get_User_Selection(array(),"","","",$GroupID));
		
		////////////////////////The Member list of the selected group [Start]/////////////////////////////////

			$UserList = $memberList;
			$y = '<select name="AvalUserList[]" class="AvalUserList" id="AvalUserList[]" size="10" style="width:99%" multiple="true">';

		for ($i=0; $i< sizeof($UserList); $i++) {
			$class_info = ($UserList[$i]['ClassName']!="" && $UserList[$i]['ClassNumber']!="") ? $UserList[$i]['ClassName']."-".$UserList[$i]['ClassNumber']. " " : "";			
			$y .= '<option value="'.$UserList[$i][0].'">';
			$y .=   $class_info.addslashes($UserList[$i][1]);
			$y .= '</option>';
		}
		$y .= '</select>';

		$x.= $y;
		////////////////////////The Member list of the selected group [End]/////////////////////////////////
		
		//$x .= $GroupManageUI->Get_User_Selection(array(),"","","",$GroupID);
		$x .= '
												</div>';

		$x .= '
												<span class="tabletextremark">'.$Lang['libms']['FormClassMapping']['CtrlMultiSelectMessage'].' </span>
												</td>
												<td style="border-bottom:0px"><input name="AddAll" onclick="Add_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;&gt;" style="width:40px;" title="'.$Lang['Btn']['AddAll'].'"/>
													<br />
													<input name="Add" onclick="Add_Selected_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;" style="width:40px;" title="'.$Lang['Btn']['AddSelected'].'"/>
													<br /><br />
													<input name="Remove" onclick="Remove_Selected_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;" style="width:40px;" title="'.$Lang['Btn']['RemoveSelected'].'"/>
													<br />
													<input name="RemoveAll" onclick="Remove_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;&lt;" style="width:40px;" title="'.$Lang['Btn']['RemoveAll'].'"/>
												</td>
												<td bgcolor="#EFFEE2" id="SelectedUserCell" style="border-bottom:0px">';

			// selected User list
			$x .= '<select name="AddUserID[]" id="AddUserID[]" class="AddUserID" size="10" style="width:99%" multiple="true">';
			$x .= '</select>';
			$x .= '		
	              	</td>
	              </tr>
							</table>
						  ';
			$ReaderSelection .= $x;
////////////////---------------------------------------------------//////////////////////////////
$ReaderSelection .= '</td></tr></tbody>';
}
if($readerGroup == ""){
	$check = "checked";
	$check1 = "";
}
if($radio_label == "readerbarcode"){
	$check = "";
	$check1 = "checked";
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementLabel";

$libms->MODULE_AUTHENTICATION($CurrentPage);
//$TAGS_OBJ[] = array($Lang['libms']['action']['new_book']);

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_label'], "prepare_label.php", 1);
$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_labelformat'], "label_format.php", 0);


$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_list'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
# Start layout
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

$schDisplayName = $libms->get_system_setting('school_display_name');
$schDisplayNameEng = $libms->get_system_setting('school_display_name_eng');
$radioSchDisplayName = '';
if(trim($schDisplayName)!='' && trim($schDisplayNameEng)!=''){
	$radioSchDisplayName = '<span id="span_radio_school_name_lang" style="display:none">(<input type="radio" checked value="chi"
							name="radio_school_name_lang" id="radio_school_name_eng" class="radio_label" /><label for="radio_school_name_eng">'.$Lang["libms"]["general"]["chinese"].'</label> <input type="radio" value="eng"
							name="radio_school_name_lang" id="radio_school_name_chi" class="radio_label" /><label for="radio_school_name_chi">'.$Lang["libms"]["general"]["english"].'</label> )</span>';
}
?>


<script type="text/javascript">
<!--
	function book_delete(tHIS){
		$(tHIS).parents("tr:first").next().hide().remove();
		$(tHIS).parents("tr:first").hide().remove();
	}
	//On Ready
	$(function(){
		$('#btn_add').click(function(){
			rows=$(".chk_book_search_result:checked").parents('tr.search_result_row').appendTo('#bookAdd');
			rows.find('.chk_book_search_result').hide();
			rows.find('.btn_book_del').show();
		});
		$('#book_search_result_toggle').click(function (){
			$(".chk_book_search_result").click();
		});
		$('#btn_booksearch').click(function(){
			sACNO_start = $('#txt_bookcode_start').val();
			sACNO_end = $('#txt_bookcode_end').val();
			//var prefix_start=sACNO_start.match(/[a-zA-Z]+/g);
			//var prefix_end=sACNO_end.match(/[a-zA-Z]+/g);
			//if (prefix_end[0]  == prefix_start[0]){			
			if (sACNO_start!="" || sACNO_end!=""){
				$('#td_loading').html('<img src="loader.gif" align="absmiddle"/>');
				$('#not_found').html('');
				$.post('ajax_book_search.php', { ACNO_start : sACNO_start, ACNO_end : sACNO_end },
					function(responseText){
						responseText = $.trim(responseText);
						if (responseText != ''){
							result_table = $('#bookAdd');
							result_table.append(responseText);
						}else{
							$('#not_found').html('<?=$Lang['libms']['pre_label']['no_match']?>');
						}
						$('#td_loading').html('');
					}
	
				);
			}else{
				$('#not_found').html('<?=$Lang['libms']['pre_label']['prefix_not_match']?>');
			}
		});
		$('input.radio_label').change(function(){
			if ($('input#radio_full_label').is(':checked')){
					$("tr.tr_memberlist").hide();
					$('div.reader_barcode_option').hide();
					$('div.hidden_all_option').show();
				$('div.hidden_option').show();
				$('div.hidden_option input').removeAttr("disabled");
				$('div.show_series_num_option').hide();
				$('div.show_series_num_option input').attr("disabled", "disabled");
				
			}
			else if($('input#radio_reader_barcode').is(':checked')){
					$("tr.tr_memberlist").show();
					$('div.hidden_all_option').hide();
					$('div.show_series_num_option').hide();
					$('div.reader_barcode_option').show();
			}
			else {
					$("tr.tr_memberlist").hide();
					$('div.reader_barcode_option').hide();
					$('div.hidden_all_option').show();
				$('div.hidden_option').hide();
				$('div.hidden_option input').attr("disabled", "disabled");
				$('div.show_series_num_option').show();
				$('div.show_series_num_option input').removeAttr("disabled");
			}
		});

		
		$('#txt_booksearch').keyup(function(event){
			if (event.which == 13){
				$('#txt_bookcode_number_start').focus();
				event.preventDefault();
				return false;
			}
		});

		$('#txt_bookcode_number_start').keyup(function(event){
			if (event.which == 13){
				$('#txt_bookcode_number_end').focus();
				event.preventDefault();
				return false;
			}
		});
			
		$('#txt_bookcode_number_end').keyup(function(event){
			if (event.which == 13) {
				$('#btn_booksearch').click();
				event.preventDefault();
				return false;
			}
		});
		
		$('form').keypress(function(event){
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
		
		//Henry Modifying 20131030
		getLabelNum();
		$('#select_label_id').change(function(){
			getLabelNum();
		});
		<?if(trim($schDisplayName)!='' && trim($schDisplayNameEng)!=''){?>
		$('#print_school_name').click(function(){
			if($('#print_school_name').attr("checked") == true){
				$('#span_radio_school_name_lang').show();
			}
			else{
				$('#span_radio_school_name_lang').hide();
			}
				
		});
		<?}?>
	});

// UI function for add/ remove class teacher/ student in add/edit class
function Remove_Selected_User() {
document.getElementById('AvalUserLayer').innerHTML = '<?=$y?>';
var ClassUser = document.getElementById('AddUserID[]');
	var UserSelected = document.getElementById('AvalUserList[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		if (ClassUser.options[i].selected) {
			ClassUser.options[i] = null;
		}
	}
	//selectAll('AddUserID[]',true);
	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		for (var j = (ClassUser.length -1); j >= 0 ; j--) {
			if (ClassUser.options[j].value == UserSelected.options[i].value) {
				UserSelected.options[i] = null;
			}
		}
	}
	//selectAll('AddUserID[]',false);
}
//function Remove_Selected_User1() {
//var ClassUser = document.getElementById('AddUserID[]');
//	var UserSelected = document.getElementById('AvalUserList[]');
//	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;
//
//	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
//		if (ClassUser.options[i].selected) {
//			User = ClassUser.options[i];
//			var elOptNew = document.createElement('option');
//	    elOptNew.text = User.text;
//	    elOptNew.value = User.value;
//	    $(elOptNew).attr('has_group',$(User).attr('has_group'));
//			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
//			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
//			try {
//	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
//	    }
//	    catch(ex) {
//	      UserSelected.add(elOptNew, UserSelected.length); // IE only
//	    }
//			ClassUser.options[i] = null;
//		}
//	}
//	//sortSelect(UserSelected);
//	//Reorder_Selection_List('AvalUserList[]');
//
//	//Update_Auto_Complete_Extra_Para();
//}

function Remove_All_User() {
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			UserSelected.options[i] = null;
	}

	document.getElementById('AvalUserLayer').innerHTML = '<?=$y?>';
	
}

function Add_Selected_User() {
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		if (ClassUser.options[ClassUser.length-1-i].selected) {
			User = ClassUser.options[ClassUser.length-1-i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
	    $(elOptNew).attr('has_group',$(User).attr('has_group'));
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[ClassUser.length-1-i] = null;
		}
	}

	//Reorder_Selection_List('AddUserID[]');

	//Update_Auto_Complete_Extra_Para();
}

function Add_All_User() {
	
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
	//for (var i = 0; i < (ClassUser.length) ; i++) {
		//if (ClassUser.options[i].selected) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
	    //$(elOptNew).attr('has_group',$(User).attr('has_group'));
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[0]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, 0); // IE only
	    }
			ClassUser.options[i] = null;
		//}
	}

}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);
	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
		//alert(selectList.options[i].text);
		/*if (opt.selected) {
			selectList.removeChild(opt);
			selectList.insertBefore(opt, selectOptions[i - 1]);
		}*/
  }
}
function sortSelect(selElem) {
        var tmpAry = new Array();
        for (var i=0;i<selElem.options.length;i++) {
            tmpAry[i] = new Array();
            tmpAry[i][0] = selElem.options[i].text;
            tmpAry[i][1] = selElem.options[i].value;
        }
        tmpAry.sort();
        while (selElem.options.length > 0) {
            selElem.options[0] = null;
        }
        for (var i=0;i<tmpAry.length;i++) {
            var op = new Option(tmpAry[i][0], tmpAry[i][1]);
            selElem.options[i] = op;
        }
        return;
    }
function selectAll(selectBox,selectAll) { 
    // have we been passed an ID 
    if (typeof selectBox == "string") { 
        selectBox = document.getElementById(selectBox);
    } 
    // is the select box a multiple select box? 
    if (selectBox && selectBox.type == "select-multiple") { 
        for (var i = 0; i < selectBox.options.length; i++) { 
             selectBox.options[i].selected = selectAll; 
        } 
    }
}

//Henry add 20131030
function getLabelNum(){
	var labelID = $('#select_label_id').val();
			//alert(labelID);
			$.post('ajax_get_label_number.php', { label_id : labelID},
					function(responseText){
						responseText = $.trim(responseText);
						if (responseText != ''){
							//result_table = $('#bookAdd');
							//result_table.append(responseText);
							$('#div_label_num').html(responseText);
						}else{
							//$('#not_found').html('<?=$Lang['libms']['pre_label']['no_match']?>');
							alert("<?=$Lang["libms"]["label"]["msg"]["no_template"]?>!");
						}
						//$('#td_loading').html('');
					}
	
			);
}
//-->
</script>

<form id='form' name="form1" method="post" action="label_generator.php" target="_blank">

	<table width="90%" align="center"  id='print_label'>
		<tr>
			<td>

				<table class="form_table_v30">
					<tr valign="top">
						<td class='field_title label_fomat'><?=$Lang['libms']['pre_label']['label_format']?>
						</td>
						<td><?=$LabelFormatOption?></td>
						
					</tr>
					<tr valign="top">
						<td class='field_title label_text'><?=$Lang['libms']['pre_label']['select_label']?>
						</td>
						<td><input type="radio" <?=$check?> value="full"
							name="radio_label" id='radio_full_label' class='radio_label' /><label for="radio_full_label"> <?=$Lang['libms']['pre_label']['barcode']?></label><br />
							<input type="radio" value="bookback" name="radio_label" id="spine_label"
							class='radio_label' /><label for="spine_label"> <?=$Lang['libms']['pre_label']['spine_label']?></label><br />
							<input type="radio" value="readerbarcode" name="radio_label" id='radio_reader_barcode'
							class='radio_label' <?=$check1?> /><label for="radio_reader_barcode"> <?=$Lang['libms']['pre_label']['ReaderBarcode']?></label><br />
						</td>
						
					</tr>
					<tr valign="top">
						<td class='field_title label_text'><?=$Lang['libms']['pre_label']['print_value']?>
						</td>
						<td>
							<div class="hidden_all_option" <?=($readerGroup || $radio_label == "readerbarcode"?'style="display:none"':'')?>>
								<label><input type='checkbox' value='true' name='print_label_border' /><?=$Lang['libms']['pre_label']['print_label_border']?></label>
								<br />
								<div class='hidden_option'>
									<label><input type="checkbox" class='' value="SchoolName"
										name="print_school_name" id="print_school_name" />
									<?=$Lang['libms']['pre_label']['school_name']?></label>
									<?=$radioSchDisplayName?>
									<br /> <label><input type="checkbox" class='' value="BookTitle"
										name="fields_to_print[]" checked='checked' />
									<?=$Lang['libms']['pre_label']['title']?></label>
									<br />
									<label><input type="checkbox" class='' value="full_barcode_text" name="fields_to_print[]" checked/> <?=$Lang['libms']['pre_label']['BarcodeNo']?></label><br />
								</div>
<?php
if ($sys_custom["elib_show_callno_first"]) {
?>
								<label><input type="checkbox" class='' value="CallNum" name="fields_to_print[]" /> <?=$Lang['libms']['pre_label']['callno']?></label><br />
								<label><input type="checkbox" class='' value="CallNum2" name="fields_to_print[]" /> <?=$Lang['libms']['pre_label']['callno2']?></label><br />
								<label><input type="checkbox" class='' value="BookCode" name="fields_to_print[]" checked='checked' /> <?=$Lang['libms']['pre_label']['bookcode']?></label><br />
<?php } else { ?>
								<label><input type="checkbox" class='' value="BookCode" name="fields_to_print[]" checked='checked' /> <?=$Lang['libms']['pre_label']['bookcode']?></label><br />
								<label><input type="checkbox" class='' value="CallNum" name="fields_to_print[]" /> <?=$Lang['libms']['pre_label']['callno']?></label><br />
								<label><input type="checkbox" class='' value="CallNum2" name="fields_to_print[]" /> <?=$Lang['libms']['pre_label']['callno2']?></label><br />							
<?php } ?>
								<label><input type="checkbox" class='' value="LocationCode" name="fields_to_print[]" /> <?=$Lang['libms']['pre_label']['location']?></label><br />
							</div>
								
							<div class='show_series_num_option' style='display:none'>
								<label><input type="checkbox" class='' value="SeriesNum" name="fields_to_print[]" disabled /> <?=$Lang["libms"]["book"]["book_series_number"]?></label><br />
								<label><input type="checkbox" class='' value="ItemSeriesNum" name="fields_to_print[]" disabled /> <?=$Lang["libms"]["book"]["item_series_number"]?></label><br />
							</div>
						
							<div class="reader_barcode_option" <?=($readerGroup || $radio_label == "readerbarcode"?'':'style="display:none"')?>>
								<label><input type='checkbox' value='true' name='print_label_border' <?=($fields_to_print?(in_array("readerChiName",$fields_to_print)?"checked":""):"")?>/><?=$Lang['libms']['pre_label']['print_label_border']?></label><br />
								<label><input type="checkbox" class='' value="reader_barcode_text" name="fields_to_print[]" <?=($fields_to_print?(in_array("readerChiName",$fields_to_print)?"checked":""):"checked")?>/> <?=$Lang['libms']['pre_label']['BarcodeNo']?></label><br />
								<label><input type="checkbox" class='' value="readerChiName" name="fields_to_print[]" <?=($fields_to_print?(in_array("readerChiName",$fields_to_print)?"checked":""):"checked")?> /> <?=$Lang["libms"]["report"]["ChineseName"]?></label><br />
								<label><input type="checkbox" class='' value="readerEngName" name="fields_to_print[]" <?=($fields_to_print?(in_array("readerEngName",$fields_to_print)?"checked":""):"")?> /> <?=$Lang["libms"]["report"]["EnglishName"]?></label><br />
								<label><input type="checkbox" class='' value="readerClassName" name="fields_to_print[]" <?=($fields_to_print?(in_array("readerClassName",$fields_to_print)?"checked":""):"")?> /> <?=$Lang["libms"]["report"]["ClassName"]?></label><br />
								<label><input type="checkbox" class='' value="readerClassNumber" name="fields_to_print[]" <?=($fields_to_print?(in_array("readerClassNumber",$fields_to_print)?"checked":""):"")?> /> <?=$Lang["libms"]["report"]["ClassNumber"]?></label><br />
							</div>
						</td>
					</tr>
					<!-- Henry Modified 20131030 [Start]-->
					<tr valign="top">
						<td class='field_title label_text'><?=$Lang['libms']['pre_label']['StartPosition']?>
						</td>
						<td><div id="div_label_num"><?=$linterface->Get_Number_Selection("startPosition", 1, 1, 1, $Onchange='', $noFirst=0, $isAll=0, $FirstTitle='', $Disabled=0)?>
						</div></td>	
					</tr>					
					<!-- Henry Modifying 20131030 [End]-->
					<tr valign="top">
						<td class='field_title label_text'><div class="hidden_all_option" <?=($readerGroup || $radio_label == "readerbarcode"?'style="display:none"':'')?>><?=$Lang["libms"]["report"]["sortby"]?></div>
						</td>
						<td><div class="hidden_all_option" <?=($readerGroup || $radio_label == "readerbarcode"?'style="display:none"':'')?>>
							<input type="radio" value="book_id" name="radio_sort_by" id='radio_sort_by_book_id' 
							class='radio_label' /> <label for="radio_sort_by_book_id"><?=$Lang['libms']['pre_label']['bookid']?></label><br />
							<input type="radio" value="acno" name="radio_sort_by" id='radio_sort_by_acno'
							class='radio_label' checked /> <label for="radio_sort_by_acno"><?=$Lang['libms']['pre_label']['bookcode']?></label><br />
							<input type="radio" value="call_num" name="radio_sort_by" id='radio_sort_by_call_num'
							class='radio_label' /> <label for="radio_sort_by_call_num"><?=$Lang['libms']['pre_label']['callno']?></label>
							</div>
						</td>	
					</tr>
					<tr valign="top">
							
						<td class='field_title book_selection'><div class="hidden_all_option" <?=($readerGroup || $radio_label == "readerbarcode"?'style="display:none"':'')?>><?=$Lang['libms']['pre_label']['print_book']?>
						</div><div class="reader_barcode_option" <?=($readerGroup || $radio_label == "readerbarcode"?'':'style="display:none"')?>><?=$Lang['libms']['pre_label']['PleaseSelectAGroup']?></div></td>							
						<td>
								<div class="hidden_all_option" <?=($readerGroup || $radio_label == "readerbarcode"?'style="display:none"':'')?>>
							<table id='book_search' >
								<tr valign="top">
									<td style='vertical-align: middle; border-bottom: none'><?=$Lang['libms']['pre_label']['bookcode']?>
										:</td>
									<td style='border-bottom: none' >
										<input type="text" id="txt_bookcode_start" /> ~ <input type="text" id="txt_bookcode_end" />
									</td>
									<td style='border-bottom: none'id='td_loading' width='25px'>&nbsp;</td>
									
									<td style='border-bottom: none'>&nbsp;</td>
									
									<td style='border-bottom: none'><input type="button" id="btn_booksearch" value="<?=$Lang['libms']['pre_label']['search']?>" />
									</td>
									<td style='border-bottom: none' id='not_found'>&nbsp;
									</td>
								</tr>
								<tr valign="top">
									<td colspan='5' style='border-bottom: none'> <span class='tabletextremark'><?=$Lang['libms']['pre_label']['search_comment']?></span></td>
								</tr>
							</table>
								</div>
								<div class="reader_barcode_option" <?=($readerGroup || $radio_label == "readerbarcode"?'':'style="display:none"')?>>
									<?=$ReaderGroupSelection?>
									
								</div>
						</td>
					</tr><?=($readerGroup || $radio_label == "readerbarcode"?$ReaderSelection:'')?>
					<tr>
					<td colspan='2'>
					<div class="hidden_all_option" <?=($readerGroup || $radio_label == "readerbarcode"?'style="display:none"':'')?>>
						<table class='field_title ' id='bookAdd' width='100%'>
							<tr valign="top">
								<th><?=$Lang['libms']['pre_label']['bookid']?></th>
								<th><?=$Lang['libms']['pre_label']['bookcode']?></th>
								<th><?=$Lang['libms']['pre_label']['callno'] ?></th>
								<th><?=$Lang['libms']['pre_label']['title']?></th>
								<th><?=$Lang['libms']['pre_label']['action']?></th>
							</tr>
						</table>
					</div>
					<!--<div class="reader_barcode_option" <?=($readerGroup || $radio_label == "readerbarcode"?'':'style="display:none"')?>>
						<table class='field_title ' id='bookAdd' width='100%'>
							<tr valign="top">
								<th>UserID</th>
								<th>User Chinese Name</th>
								<th>User English Name</th>
								<th>Class</th>
								<th>Class Number</th>
							</tr>
						</table>			
					</div>-->	
					</td>
					</tr>
				</table>
				<div class="edit_bottom_v30">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "selectAll('AddUserID[]',true)")?>
					&nbsp;
				</div>
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
