<?php



// Modifying by 

############ Change Log [Start] 
#
#	Date:	2016-01-18  Cameron
#			add order by ACNO for the list		
#
#	Date:	2013-09-24	Yuen
#			revised the ACNO comparison logic (not using ACNO_NUM anymore!)
#
############ Change Log [End] 



if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
####### END  OF   Trim all request #######
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$libms = new liblms();
global $Lang;
$data = array();

$iACNO_start = trim(mysql_real_escape_string($_REQUEST['ACNO_start']));//Some clean up
$iACNO_end = trim(mysql_real_escape_string($_REQUEST['ACNO_end']));//Some clean up

//echo "($iACNO_start, $iACNO_end)";
//if(!empty($_REQUEST['ACNO_start']))//If a term has been submitted
if ($iACNO_start<>"" || $iACNO_end<>"")
{
	//$sACNO = mysql_real_escape_string($_REQUEST['ACNO']);//Some clean up

	//$sDate_start = mysql_real_escape_string($_REQUEST['Date_start']);//Some clean up
	//$sDate_end = mysql_real_escape_string($_REQUEST['Date_end']);//Some clean up
	
	if (preg_match('/^([a-z]+)([0-9]+)?$/i',$iACNO_start,$matches)){
		$sACNO = $matches[1];
		$iACNO_number_start = $matches[2];
	}
	if (preg_match('/^([a-z]+)([0-9]+)?$/i',$iACNO_end,$matches)){
		$iACNO_number_end = $matches[2];
	}	
	

	$conditions[] ="bu.`RecordStatus` NOT LIKE 'DELETE'";
	
	/*
	if (!empty($sACNO)){
		$conditions[] = "bu.`ACNO_Prefix` LIKE '{$sACNO}%'";
	}
	if (!empty($iACNO_number_start)){
		$conditions[] = "bu.`ACNO_Num` >= '{$iACNO_number_start}'";
	}
	if (!empty($iACNO_number_end)){
		$conditions[] = "bu.`ACNO_Num` <= '{$iACNO_number_end}'";
	}
	*/
	if ($iACNO_start<>"" && $iACNO_end<>"")
	{
		$conditions[] = "trim(bu.`ACNO`) >= trim('{$iACNO_start}')";
		$conditions[] = "trim(bu.`ACNO`) <= trim('{$iACNO_end}')";
	} elseif ($iACNO_start<>"")
	{
		$conditions[] = "trim(bu.`ACNO`) = trim('{$iACNO_start}')";
	} elseif ($iACNO_end<>"")
	{
		$conditions[] = "trim(bu.`ACNO`) = trim('{$iACNO_end}')";
	}
	if (!empty($sDate_start)){
		$conditions[] = "bu.`PurchaseDate` >= '{$sDate_start}'";
	}
	if (!empty($sDate_end)){
		$conditions[] = "bu.`PurchaseDate` <= '{$sDate_end}'";
	}
	
	if (empty($conditions)){
		exit;
	}
			
	$condition = '(' . implode(') AND (', $conditions) . ')';
	
	$sql = 
<<<EOF000EOF
SELECT 
	bu.`UniqueID`,bu.`ACNO` AS BookCode, b.`CallNum`, b.`BookTitle`
FROM `LIBMS_BOOK` b
JOIN `LIBMS_BOOK_UNIQUE` bu
	ON b.`BookID` = bu.`BookID`
WHERE
	{$condition}
ORDER BY bu.`ACNO`	
EOF000EOF;
 
//debug($sql);
	$result = $libms->returnArray($sql,null,1);


	$display = '';
	if(!empty($result))
	foreach ($result as $row){
		$display .= 
<<<EOF000EOF
	<tr	class='search_result_row' id='{$row['UniqueID']}'>
		<td>{$row['UniqueID']}<input type="hidden" value="{$row['UniqueID']}" name="unique_book_ids[]"></td>
		<td>{$row['BookCode']}</td>
		<td>{$row['CallNum']}</td>
		<td>{$row['BookTitle']}</td>
		<td>
			<!-- <input class="chk_book_search_result" type="checkbox" /> -->
			<input class="btn_book_del" type="button" value='{$Lang['libms']['general']['del']}'  onClick="book_delete(this)" />
		</td>
	<tr>
EOF000EOF;
	}
	echo $display;
 
 
 
}
