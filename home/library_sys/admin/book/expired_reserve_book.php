<?php

# being modified by: 
/*
 * Change log:
 *
 * 2019-03-28 [Cameron]
 *      - show BookSubTitle if it's not empty
 *      
 * 2018-09-05 Cameron
 *      - use function getReserveBookTabs() to assign $TAGS_OBJ
 * 
 * 2017-06-06 Cameron
 * 		- place set cookie function after include global.php to support php5.4 [case #E117788]
 * 
 * 2017-05-15 Cameron
 * 		- replace htmlspecialchars by intranet_htmlspecialchars
 * 
 * 2017-04-06 Cameron
 * 		- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 			by stripslashes($keyword)
 * 
 * 2013-11-08 Henry
 * 		File Created
 * 
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
	function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
	$gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
	array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

### set cookies
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 0;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

//global $intranet_db;

intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementReserveList";

// $TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_reserve'], 'reserve_book.php', false);
// $TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['expired_book_reserve'],'#', true);
$TAGS_OBJ = $libms->getReserveBookTabs('EXPIRE');
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);


//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = (!isset($order)) ? 1 : $order;
$field = ($field == "") ? 0 : $field;

//Get the academic year start date
$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());

foreach($current_period as &$date){
	$date = date('Y-m-d',strtotime($date));
}


$_REQUEST['DueDateAfter'] = empty($_REQUEST['DueDateAfter'])? $current_period['StartDate']:$_REQUEST['DueDateAfter'];
$_REQUEST['DueDateBefore'] = empty($_REQUEST['DueDateBefore'])? date('Y-m-d'):$_REQUEST['DueDateBefore'];


$conds='';
$keyword = trim($_POST['keyword']);
if (!get_magic_quotes_gpc()) {
	$keyword = stripslashes($keyword);
}

if($keyword!="") {
	$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));
	$converted_keyword_sql = special_sql_str($keyword);
	if ($unconverted_keyword_sql == $converted_keyword_sql){
		$conds .= " AND (
		CONCAT(b.`CallNum`,' ', b.`CallNum2`) LIKE '%$unconverted_keyword_sql%' OR
		b.`ISBN` LIKE '%$unconverted_keyword_sql%' OR
		b.`BookTitle` LIKE '%$unconverted_keyword_sql%' OR
		rl.ExpiryDate LIKE '%$unconverted_keyword_sql%' OR
		u.EnglishName LIKE '%$unconverted_keyword_sql%' OR
		u.ChineseName LIKE '%$unconverted_keyword_sql%' OR
		u2.EnglishName LIKE '%$unconverted_keyword_sql%' OR
		u2.ChineseName LIKE '%$unconverted_keyword_sql%'
		)";
	}
	else {
		$conds .= " AND (
		CONCAT(b.`CallNum`,' ', b.`CallNum2`) LIKE '%$unconverted_keyword_sql%' OR
		b.`ISBN` LIKE '%$unconverted_keyword_sql%' OR
		b.`BookTitle` LIKE '%$unconverted_keyword_sql%' OR
		rl.ExpiryDate LIKE '%$unconverted_keyword_sql%' OR
		u.EnglishName LIKE '%$unconverted_keyword_sql%' OR
		u.ChineseName LIKE '%$unconverted_keyword_sql%' OR
		u2.EnglishName LIKE '%$unconverted_keyword_sql%' OR
		u2.ChineseName LIKE '%$unconverted_keyword_sql%' OR
		CONCAT(b.`CallNum`,' ', b.`CallNum2`) LIKE '%$converted_keyword_sql%' OR
		b.`ISBN` LIKE '%$converted_keyword_sql%' OR
		b.`BookTitle` LIKE '%$converted_keyword_sql%' OR
		rl.ExpiryDate LIKE '%$converted_keyword_sql%' OR
		u.EnglishName LIKE '%$converted_keyword_sql%' OR
		u.ChineseName LIKE '%$converted_keyword_sql%' OR
		u2.EnglishName LIKE '%$converted_keyword_sql%' OR
		u2.ChineseName LIKE '%$converted_keyword_sql%'
		)";
	}
}

//for the reserved by other filter
if($reservedByOther==1)
	$conds .= " AND u2.UserID IS NOT NULL";
else if($reservedByOther==2)
	$conds .= " AND u2.UserID IS NULL";
	
if(!empty($_REQUEST['DueDateAfter'])){
	$conds .= ' AND rl.`ExpiryDate` >=  ' . PHPToSQL($_REQUEST['DueDateAfter']);
	
}
if(!empty($_REQUEST['DueDateBefore'])){
	$conds .= ' AND rl.`ExpiryDate` <=  ' . PHPToSQL($_REQUEST['DueDateBefore']);
}

$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(b.`BookID` USING utf8)" : "b.`BookID`";
//$sql = <<<EOL
//SELECT  CONCAT(b.`CallNum`,' ', b.`CallNum2`) AS CallNumber,
//		BookTitle AS BookTitle,
//		CONCAT(u.ClassName,'-', u.ClassNumber) AS class,
//	CASE 
//		WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']}) 
//		ELSE u.{$Lang['libms']['SQL']['UserNameFeild']}
//	END  AS UserName,
//	 rl.ExpiryDate,
//		b.BookTitle as BookTitleForSort
//FROM  `LIBMS_RESERVATION_LOG` rl
//JOIN  `LIBMS_BOOK` b ON b.`BookID` = rl.`BookID` 	
//JOIN    LIBMS_USER u ON u.UserID = rl.UserID
//WHERE (
//rl.`RecordStatus` LIKE  'EXPIRED'
//OR rl.`RecordStatus` LIKE  'EXPIRED'
//)		
//{$conds}
//
//GROUP BY rl.`BookID` 
//
//EOL;

$sql = <<<EOL
SELECT  rl.ExpiryDate,
		CONCAT(b.`CallNum`,' ', b.`CallNum2`) AS CallNumber,
        CONCAT(b.`BookTitle`, IF(BookSubTitle IS NULL OR BookSubTitle='','',CONCAT('-',BookSubTitle))) AS BookTitle,
		CONVERT( CONCAT(CASE 
			WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']}) 
			ELSE u.{$Lang['libms']['SQL']['UserNameFeild']}
		END,IF(u.ClassName <> "",CONCAT(' (',u.ClassName,'-', u.ClassNumber,')'),'')) USING utf8) AS UserName,
		CONVERT( IF(u2.UserID IS NOT NULL,CONCAT(CASE 
			WHEN u2.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">*</span>', u2.{$Lang['libms']['SQL']['UserNameFeild']}) 
			ELSE u2.{$Lang['libms']['SQL']['UserNameFeild']}
		END,IF(u2.ClassName <> "",CONCAT(' (',u2.ClassName,'-', u2.ClassNumber,')'),'')),'--') USING utf8) AS ReservedUser
FROM  	`LIBMS_RESERVATION_LOG` rl 
JOIN  	`LIBMS_BOOK` b ON b.`BookID` = rl.`BookID` 
JOIN    LIBMS_USER u ON u.UserID = rl.UserID 
LEFT JOIN `LIBMS_RESERVATION_LOG` rl2 ON rl2.BookID = rl.BookID AND rl2.`RecordStatus` LIKE  'READY' 
LEFT JOIN LIBMS_USER u2 ON u2.UserID = rl2.UserID 
WHERE (rl.`RecordStatus` LIKE  'EXPIRED')
{$conds}
GROUP BY rl.`BookID` 
EOL;

$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array("ExpiryDate", "CallNumber", "BookTitle", "UserName", "ReservedUser");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
//$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['libms']['book']['code'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['CirculationManagement']['msg']['expiry_date'])."</td>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['libms']['book']['call_number'])."</td>\n";
$li->column_list .= "<th width='32%' >".$li->column($pos++, $Lang['libms']['book']['title'])."</td>\n";
//$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['book']['responsibility_by1'])."</td>\n";
//$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['book']['class'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['CirculationManagement']['msg']['reserved_by'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang["libms"]["CirculationManagement"]["booked"])."</td>\n";
//$li->column_list .= "<th width='1'>".$li->check("Code[]")."</td>\n";

$keyword = intranet_htmlspecialchars($_POST['keyword']);
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}

$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";
//*/
############################################################################################################

# Top menu highlight setting
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function checkForm2() {
	var pass = check_date(document.getElementById("DueDateAfter"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
	 	pass = check_date(document.getElementById("DueDateBefore"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
		pass = pass & compare_date(document.getElementById("DueDateAfter"), document.getElementById("DueDateBefore"),"<?=$Lang['General']['WrongDateLogic']?>");
	if ( pass == true )	// true
	{
		document.form1.submit();
	}
}

//-->
</script>

<form name="form1" id="form1" method="POST" action="expired_reserve_book.php" onSubmit="return checkForm()">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%" align="left">
												<?=$linterface->GET_SELECTION_BOX(array(array("","-- ".$Lang['libms']['book']['allRecord']." --"),array(1,$Lang['libms']['book']['expiredAndPassedToNext']),array(2,$Lang['libms']['book']['expired'])), 'id="reservedByOther" name="reservedByOther" onchange="document.form1.submit()"', "", $reservedByOther)?>&nbsp;
												<?=$linterface->GET_DATE_PICKER("DueDateAfter",$_REQUEST['DueDateAfter'] )?>
												<?=$linterface->GET_DATE_PICKER("DueDateBefore",$_REQUEST['DueDateBefore'])?>
												<input type='button' value='<?=$Lang["libms"]["book"]["setting"]["filter_submit"]?>' onclick='checkForm2();' />										
											</td>				
											<td width="30%" align="right">
												<div class="content_top_tool">
													<div class="Conntent_search"><?=$searchTag?></div>     
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form>

<?php
//dump($sql);
$linterface->LAYOUT_STOP();
 
 
 
intranet_closedb();





?>
