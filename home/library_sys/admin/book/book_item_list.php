<?php
// using:

/**
 * **********************************
 * Date: 2019-07-23 (Henry)
 * - use the local library of jquery
 * 
 * Date: 2019-04-09 (Henry)
 * - added flag $sys_custom['eLibraryPlus']['HideBookSeriesNumberOnItem'] to control the display of book series number on the item
 *
 * Date: 2018-05-09 (Cameron)
 * - pass Val=2 to getHelpInfo
 *
 * Date: 2017-12-07 (Cameron)
 * - allow keyword search to get result of any combination of single word in the field [case #F132432]
 * apply to following fields only: Distributor, Publisher and BookTitle
 *
 * Date: 2017-04-03 (Cameron)
 * - handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * by stripslashes($keyword)
 * - fix bug in $LocationCode & $DistributorSelect in sql
 *
 * Date: 2016-04-06 (Cameron)
 * - pass DirectMsg to LAYOUT_START()
 *
 * Date: 2016-02-11 (Henry)
 * - add button for Add from existing item
 *
 * Date: 2015-10-05 (Cameron)
 * - fix bug on basic search / advance search for string like A&<>'"\B, support comparing unconverted string A&<>'"\B ( created by imoport)
 * and converted string A&amp;'&quot;&lt;&gt;\B (created by add / edit)
 *
 * Date: 2015-09-30 (Cameron)
 * - fix bug on keyword search / advance search to support following characters: '"<>&\
 * - fix bug on option list filter for Distributor so that it support item containing special characters: '"<>&\
 * - add sorting order in distributor list
 *
 * Date: 2015-07-27 (Cameron)
 * - add parameter $CheckType=true to options list of GET_SELECTION_BOX() for Location
 *
 * Date: 2015-04-15 (Cameron)
 * - add hidden BookID for pass in advance search
 *
 * Date: 2015-04-01 (Cameron)
 * Details: - support navigation of records ( prev, next, sorting, filter by book category etc.) after advance search
 * ( add hidden field advanceSearch)
 * - reset to basic search if press enter in keyword field (advanceSearch=0)
 * - keep all filter condition, basic search / advance search condition, page navigation condition after return from
 * inner linking page (e.g. ACNO)
 *
 * Date: 2015-03-23 (Cameron)
 * Details: show series number and circulation type description from LIBMS_BOOK if that of items is not set (follow bibliography)
 *
 * Date: 2014-12-02 (Cameron)
 * Details: add search max(ACNO) by pattern of {prefix}^ in keywords field
 *
 * Date: 2014-10-22 (Yuen)
 * Details: add column Circulation Type
 *
 * Date: 2014-09-30 (Henry)
 * Details: add advance search button #ip.2.5.5.10.1
 *
 * Date: 2014-09-15 (Henry)
 * Details: add autocomplete at keywords field
 *
 * Date: 2014-05-30 (Henry)
 * Details: Disable user to change the status to Write-off
 *
 * Date: 2013-09-27 (Yuen)
 * Details: indicate the Loan setting in item status using * (if the book is not allow for loaning)
 *
 * Date: 2013-09-17 (Yuen)
 * Details: changed hyperlink from book title to ACNO
 *
 * Date: 2013-08-21 (Henry)
 * Details: add Distributer filter
 *
 * **********************************
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");

include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");

// global $intranet_db;

intranet_auth();
intranet_opendb();
// ###############################################################################################################

// ## Set Cookies
$arrCookies = array();
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_field",
    "field"
); // navigation var start
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_order",
    "order"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_number",
    "pageNo"
); // navigation var end
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_keyword",
    "keyword"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_book_status",
    "BookStatus"
); // option list filter start
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_location_code",
    "LocationCode"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_distributor_select",
    "DistributorSelect"
); // option list filter end

$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_advanceSearch",
    "advanceSearch"
); // advance search start
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_ACNO",
    "ACNO"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_Barcode",
    "Barcode"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_bookTitle",
    "bookTitle"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_author",
    "author"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_callNumber",
    "callNumber"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_ISBN",
    "ISBN"
);
$arrCookies[] = array(
    "elibrary_plus_mgmt_item_page_publisher",
    "publisher"
); // advance search end

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}
// ###############################################################################################################

// # Get Data
$BookStatus = (isset($BookStatus) && $BookStatus != '') ? $BookStatus : '';
$keyword = (isset($keyword) && $keyword != '') ? trim($keyword) : '';
$FromPage = (isset($FromPage) && $FromPage != '') ? $FromPage : 'item';
if (! get_magic_quotes_gpc()) {
    $keyword = stripslashes($keyword);
    $ACNO = stripslashes($ACNO);
    $Barcode = stripslashes($Barcode);
    $bookTitle = stripslashes($bookTitle);
    $author = stripslashes($author);
    $callNumber = stripslashes($callNumber);
    $ISBN = stripslashes($ISBN);
    $publisher = stripslashes($publisher);
    
    $LocationCode = stripslashes($LocationCode);
    $DistributorSelect = stripslashes($DistributorSelect);
}

if (isset($elibrary_plus_mgmt_item_page_size) && $elibrary_plus_mgmt_item_page_size != "")
    $page_size = $elibrary_plus_mgmt_item_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == '') ? 1 : $order;
$field = ($field == "") ? 0 : $field;

// # Use Library
// in all boook management PHP pages
$libms = new liblms();

// Access Checking
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (! $admin_right['book management'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$linterface = new interface_html();
$li = new libdbtable2007($field, $order, $pageNo);

// # Init
$btnOption = '';
$CurTab = 'Item';

// # Preparation
$CurrentPageArr['LIBMS'] = 1;
// $CurrentPage = "PageBookManagementBookList";
$CurrentPage = $libms->get_book_manage_cur_page($FromPage);
$TAGS_OBJ[] = array(
    $Lang["libms"]["action"][strtolower($FromPage)]
);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);

$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(i.UniqueID USING utf8)" : "i.UniqueID";

// # DB SQL

$DBCirculationTitle = ($intranet_session_language == "en") ? "DescriptionEn" : "DescriptionChi";

$sql = "select ";
if (strtolower($FromPage) == 'item') {
    $sql .= "
			CONCAT('<a href=\"book_item_edit.php?FromPage=" . $FromPage . "&UniqueID='," . $BookID_For_CONCAT . ",'\">',i.ACNO,'</a>') as BookTitleLink,
			b.BookTitle,
";
} else {
    $sql .= "
			CONCAT('<a href=\"book_item_edit.php?FromPage=" . $FromPage . "&BookID=" . $BookID . "&UniqueID='," . $BookID_For_CONCAT . ",'\">',i.ACNO,'</a>') as ACNO, ";
}
$sql .= " 
			i.BarCode, 
			 if (i.ItemSeriesNum='' OR i.ItemSeriesNum is NULL,
				".(!$sys_custom['eLibraryPlus']['HideBookSeriesNumberOnItem']?"concat('" . $Lang["libms"]["bookmanagement"]["follow_parent"] . "',' - ',if (b.SeriesNum='' OR b.SeriesNum is NULL,'" . $Lang["libms"]["bookmanagement"]["unnumbered"] . "', b.SeriesNum))":"''").", i.ItemSeriesNum) as ItemSeriesNum,
				i.Distributor, " . " if (i.CirculationTypeCode='' OR i.CirculationTypeCode is NULL, 
				concat('" . $Lang["libms"]["bookmanagement"]["follow_parent"] . "',' - ',if (kc." . $DBCirculationTitle . "='' OR kc." . $DBCirculationTitle . " is null,'" . $Lang["libms"]["bookmanagement"]["unclassified"] . "',kc." . $DBCirculationTitle . ")), 
				bc." . $DBCirculationTitle . ") as CirculationType, ";
// Column - Book Status
$sql .= "	CONCAT(if(b.OpenBorrow=1, '', '* '), case ";
if (! empty($Lang["libms"]["book_status"])) {
    foreach ($Lang["libms"]["book_status"] as $status_key => $status_val) {
        $sql .= "	when i.RecordStatus = '" . $status_key . "' then '" . $status_val . "' ";
    }
}
$sql .= "	else '--' end) as RecordStatus, ";
$sql .= "	IF(i.CreationDate <> '0000-00-00' and i.CreationDate <> '', DATE_FORMAT(i.CreationDate, '%Y-%m-%d'), '') as CreationDate, 
			CONCAT('<input type=\"checkbox\" name=\"UniqueID[]\" id=\"UniqueID[]\" value=\"', i.UniqueID ,'\">') as checkbox, 
			b.BookTitle as BookTitle_ForSort,
			i.ACNO as ACNO_ForSort		 
		from LIBMS_BOOK_UNIQUE as i 
		inner join LIBMS_BOOK as b on 
			b.BookID = i.BookID " . ((strtolower($FromPage) == 'book' || strtolower($FromPage) == 'periodical') ? "and i.BookID = '" . $BookID . "'" : "") . "
		LEFT JOIN LIBMS_CIRCULATION_TYPE AS bc ON bc.CirculationTypeCode=i.CirculationTypeCode
		LEFT JOIN LIBMS_CIRCULATION_TYPE AS kc ON kc.CirculationTypeCode=b.CirculationTypeCode
		where (i.RecordStatus NOT LIKE 'DELETE' OR i.RecordStatus IS NULL) ";
// DB - condition
$bookStatus_cond = '';
if ($BookStatus != '') {
    $sql .= " and i.RecordStatus = '" . $BookStatus . "' ";
    
    // for Distributor filter
    $bookStatus_cond = " and i.RecordStatus = '" . $BookStatus . "' ";
}
// # Siuwan 20130909
$acno_cond = '';
if ($acnoSelect != '') {
    $sql .= " AND i.acno = '" . $acnoSelect . "'";
    // for Distributor filter
    $acno_cond = " and i.acno = '" . $acnoSelect . "' ";
}

$keyword_cond = '';

if (! $advanceSearch) { // basic search
    if ($keyword != "") {
        $unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\", "\\\\", $keyword)); // A&<>'"\B ==> A&<>\'\"\\\\B
        $converted_keyword_sql = special_sql_str($keyword); // A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
    } else {
        $unconverted_keyword_sql = "";
        $converted_keyword_sql = "";
    }
    
    $keyword_sql = $unconverted_keyword_sql;
    preg_match('/^(\s*)((\w)+)(\s*)~(\s*)((\w)+)(\s*)$/', $keyword_sql, $matches_range);
    preg_match('/^(\s*)\\\"(\s*)((\w)+)(\s*)\\\"(\s*)$/', $keyword_sql, $matches_strict);
    preg_match('/^(\s*)((\w)+)(\^)(\s*)$/', $keyword_sql, $matches_max);
    if (! isset($matches_strict[3]) || $matches_strict[3] == "") {
        preg_match("/^(\s*)\\\'(\s*)((\w)+)(\s*)\\\'(\s*)$/", $keyword_sql, $matches_strict);
    }
    $acno_strict = $matches_strict[3];
    if ($matches_range[0] != "") {
        $iACNO_start = trim($matches_range[2]);
        $iACNO_end = trim($matches_range[6]);
        if ($iACNO_start != "" || $iACNO_end != "") {
            // if (preg_match('/^([a-z]+)([0-9]+)?$/i',$iACNO_start,$matches)){
            // $sACNO = $matches[1];
            // $iACNO_number_start = $matches[2];
            // }
            // if (preg_match('/^([a-z]+)([0-9]+)?$/i',$iACNO_end,$matches)){
            // $iACNO_number_end = $matches[2];
            // }
            
            $conditions[] = "i.`RecordStatus` NOT LIKE 'DELETE'";
            
            if ($iACNO_start != "" && $iACNO_end != "") {
                $conditions[] = "i.`ACNO` >= '{$iACNO_start}'";
                $conditions[] = "i.`ACNO` <= '{$iACNO_end}'";
            } elseif ($iACNO_start != "") {
                $conditions[] = "i.`ACNO` = '{$iACNO_start}'";
            } elseif ($iACNO_end != "") {
                $conditions[] = "i.`ACNO` = '{$iACNO_end}'";
            }
            // if (!empty($sDate_start)){
            // $conditions[] = "i.`PurchaseDate` >= '{$sDate_start}'";
            // }
            // if (!empty($sDate_end)){
            // $conditions[] = "i.`PurchaseDate` <= '{$sDate_end}'";
            // }
            
            if (empty($conditions)) {
                exit();
            }
            
            $acno_filter = '(' . implode(') AND (', $conditions) . ')';
        }
    } elseif (isset($acno_strict) && $acno_strict != "") {
        $acno_filter = "i.ACNO = '$acno_strict'";
    } elseif ($matches_max[0] != "") {
        if ($matches_max[2] != "") {
            $acno_filter = "i.ACNO = (select max(ACNO) from `LIBMS_BOOK_UNIQUE` where ACNO LIKE '" . $matches_max[2] . "%' limit 1)";
        } else {
            exit();
        }
    } else {
        if ($unconverted_keyword_sql == $converted_keyword_sql) {
            $acno_filter = "i.ACNO LIKE '%$keyword_sql%'";
        } else {
            $acno_filter = "(i.ACNO LIKE '%$unconverted_keyword_sql%' OR i.ACNO LIKE '%$converted_keyword_sql%')";
        }
    }
    
    if ($unconverted_keyword_sql == $converted_keyword_sql) { // keyword does not contain any special character: &<>"
                                                              // Distributor and Publisher are not specified in the available keyword search list?
                                                              // for Distributor filter
        
        $words = getWords($unconverted_keyword_sql);
        $keyword_cond = " AND (";
        $keyword_cond .= "i.BarCode LIKE '%$keyword_sql%' OR ";
        $keyword_cond .= $acno_filter ? $acno_filter . " OR " : "";
        $keyword_cond .= "(i.Distributor like '%" . implode("%' AND i.Distributor LIKE'%", $words) . "%') OR ";
        $keyword_cond .= "(b.BookTitle like '%" . implode("%' AND b.BookTitle LIKE'%", $words) . "%') OR ";
        $keyword_cond .= "(b.Publisher like '%" . implode("%' AND b.Publisher LIKE'%", $words) . "%')";
        $keyword_cond .= ") ";
        
        $sql .= $keyword_cond;
    } else {
        $words_1 = getWords($unconverted_keyword_sql);
        $words_2 = getWords($converted_keyword_sql);
        $keyword_cond = " AND (";
        $keyword_cond .= "i.BarCode LIKE '%$unconverted_keyword_sql%' OR i.BarCode LIKE '%$converted_keyword_sql%' OR ";
        $keyword_cond .= $acno_filter ? $acno_filter . " OR " : "";
        $keyword_cond .= "(i.Distributor like '%" . implode("%' AND i.Distributor LIKE'%", $words_1) . "%') OR (i.Distributor like '%" . implode("%' AND i.Distributor LIKE'%", $words_2) . "%') OR ";
        $keyword_cond .= "(b.BookTitle like '%" . implode("%' AND b.BookTitle LIKE'%", $words_1) . "%') OR (b.BookTitle like '%" . implode("%' AND b.BookTitle LIKE'%", $words_2) . "%') OR ";
        $keyword_cond .= "(b.Publisher like '%" . implode("%' AND b.Publisher LIKE'%", $words_1) . "%') OR (b.Publisher like '%" . implode("%' AND b.Publisher LIKE'%", $words_2) . "%')";
        $keyword_cond .= ") ";
        
        $sql .= $keyword_cond;
    }
} else { // advance search, does not include keyword search in advance search mode
      // if($keyword!="")
      // $keyword_sql = mysql_real_escape_string($keyword);
      
    // ACNO does not contain double quote, and it's stored in db for original characters like: '<>&\
    $ACNO = (isset($ACNO) && $ACNO != '') ? trim($ACNO) : '';
    if ($ACNO != "") {
        $unconverted_ACNO = mysql_real_escape_string(str_replace("\\", "\\\\", $ACNO));
        $converted_ACNO = special_sql_str($ACNO);
        $sql .= $keyword_cond .= " AND (
			i.`ACNO` LIKE '%$unconverted_ACNO%' OR i.`ACNO` LIKE '%$converted_ACNO%'
			)";
    }
    
    $Barcode = (isset($Barcode) && $Barcode != '') ? trim($Barcode) : '';
    if ($Barcode != "") {
        $unconverted_Barcode = mysql_real_escape_string(str_replace("\\", "\\\\", $Barcode));
        $converted_Barcode = special_sql_str($Barcode);
        $sql .= $keyword_cond .= " AND (
			i.`BarCode` LIKE '%$unconverted_Barcode%' OR i.`BarCode` LIKE '%$converted_Barcode%'
			)";
    }
    
    $publisher = (isset($publisher) && $publisher != '') ? trim($publisher) : '';
    if ($publisher != "") {
        $unconverted_publisher = mysql_real_escape_string(str_replace("\\", "\\\\", $publisher));
        $converted_publisher = special_sql_str($publisher);
        $sql .= $keyword_cond .= " AND (
			b.`Publisher` LIKE '%$unconverted_publisher%' OR b.`Publisher` LIKE '%$converted_publisher%'
			)";
    }
    
    $author = (isset($author) && $author != '') ? trim($author) : '';
    if ($author != "") {
        $unconverted_author = mysql_real_escape_string(str_replace("\\", "\\\\", $author));
        $converted_author = special_sql_str($author);
        $sql .= $keyword_cond .= " AND (
				b.`ResponsibilityBy1` LIKE '%$unconverted_author%' OR b.`ResponsibilityBy1` LIKE '%$converted_author%' OR 
				b.`ResponsibilityBy2` LIKE '%$unconverted_author%' OR b.`ResponsibilityBy2` LIKE '%$converted_author%' OR 
				b.`ResponsibilityBy3` LIKE '%$unconverted_author%' OR b.`ResponsibilityBy3` LIKE '%$converted_author%'
			)";
    }
    
    $callNumber = (isset($callNumber) && $callNumber != '') ? trim($callNumber) : '';
    if ($callNumber != "") {
        $unconverted_callNumber = mysql_real_escape_string(str_replace("\\", "\\\\", $callNumber));
        $converted_callNumber = special_sql_str($callNumber);
        $sql .= $keyword_cond .= " AND (
			b.`CallNum` LIKE '%$unconverted_callNumber%' OR b.`CallNum` LIKE '%$converted_callNumber%' OR 
			b.`CallNum2` LIKE '%$unconverted_callNumber%' OR b.`CallNum2` LIKE '%$converted_callNumber%' OR 
			CONCAT(b.`CallNum`, ' ', b.`CallNum2`) LIKE '%$unconverted_callNumber%' OR CONCAT(b.`CallNum`, ' ', b.`CallNum2`) LIKE '%$converted_callNumber%'
			)";
    }
    
    $ISBN = (isset($ISBN) && $ISBN != '') ? trim($ISBN) : '';
    if ($ISBN != "") {
        $unconverted_ISBN = mysql_real_escape_string(str_replace("\\", "\\\\", $ISBN));
        $converted_ISBN = special_sql_str($ISBN);
        $sql .= $keyword_cond .= " AND
			(b.`ISBN` LIKE '%$unconverted_ISBN%' OR b.`ISBN` LIKE '%$converted_ISBN%' OR
			b.`ISBN2` LIKE '%$unconverted_ISBN%' OR b.`ISBN2` LIKE '%$converted_ISBN%')";
    }
    
    $bookTitle = (isset($bookTitle) && $bookTitle != '') ? trim($bookTitle) : '';
    if ($bookTitle != "") {
        $unconverted_bookTitle = mysql_real_escape_string(str_replace("\\", "\\\\", $bookTitle));
        $converted_bookTitle = special_sql_str($bookTitle);
        $sql .= $keyword_cond .= " AND
			(b.`BookTitle` LIKE '%$unconverted_bookTitle%' OR b.`BookTitle` LIKE '%$converted_bookTitle%' OR 
			b.`BookSubTitle` LIKE '%$unconverted_bookTitle%' OR b.`BookSubTitle` LIKE '%$converted_bookTitle%')";
    }
}

// echo $sql;die();

// # Location Code Sql Cond
if ($LocationCode != '') {
    if ($LocationCode == 'NA') {
        $noLocationArr = $libms->GET_LOCATION_NOT_EXIST();
        $sql .= " AND (i.LocationCode = '' OR i.LocationCode is null OR i.LocationCode IN ('" . implode("','", $libms->Get_Safe_Sql_Query($noLocationArr)) . "'))";
    } else {
        $sql .= " AND i.LocationCode = '" . $LocationCode . "' ";
    }
}

// # DistributorSelect Sql Cond
$distributor_cond = '';
if ($DistributorSelect != '') {
    if ($DistributorSelect == 'NA') {
        $sql .= " AND (i.Distributor = '' OR i.Distributor is null)";
    } else {
        $sql .= " AND i.Distributor = '" . addslashes(htmlspecialchars($DistributorSelect)) . "' ";
    }
    
    // for ACNO filter
    $distributor_cond = " and i.Distributor = '" . addslashes(htmlspecialchars($DistributorSelect)) . "' ";
}

// # Data Table
// global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

// BookdCode = ACNO
if (strtolower($FromPage) == 'item') {
    $li->field_array = array(
        "ACNO",
        "BookTitle_ForSort",
        "BarCode",
        "ItemSeriesNum",
        "Distributor",
        "CirculationType",
        "RecordStatus",
        "CreationDate"
    );
} else {
    $li->field_array = array(
        "ACNO_ForSort",
        "BarCode",
        "ItemSeriesNum",
        "Distributor",
        "CirculationType",
        "RecordStatus",
        "CreationDate"
    );
}
$li->sql = $sql;

$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(
    0,
    0
);
$li->wrap_array = array(
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='5' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th>" . $li->column($pos ++, $Lang['libms']['book']['code']) . "</th>\n";
if (strtolower($FromPage) == 'item') {
    $li->column_list .= "<th>" . $li->column($pos ++, $Lang['libms']['book']['title']) . "</th>\n";
}
$li->column_list .= "<th>" . $li->column($pos ++, $Lang['libms']['book']['barcode']) . "</th>\n";
$li->column_list .= "<th>" . $li->column($pos ++, $Lang["libms"]["book"]["series_number"]) . "</th>\n";
$li->column_list .= "<th>" . $li->column($pos ++, $Lang['libms']['book']['distributor']) . "</th>\n";
$li->column_list .= "<th>" . $li->column($pos ++, $Lang["libms"]["settings"]["circulation_type"]) . "</th>\n";
$li->column_list .= "<th>" . $li->column($pos ++, $Lang['libms']['bookmanagement']['bookStatus']) . "</th>\n";
$li->column_list .= "<th>" . $li->column($pos ++, $Lang['libms']['book']['item_account_date']) . "</th>\n";
$li->column_list .= "<th width='5'>" . $li->check("UniqueID[]") . "</th>\n";
$bookID_para = (strtolower($FromPage) == 'item') ? "" : "&BookID=" . $BookID;
$toolbar = $linterface->GET_LNK_NEW("book_item_edit.php?FromPage=" . $FromPage . $bookID_para, $button_new, "", "", "", 0);
// $toolbar .= $linterface->GET_LNK_IMPORT("import/csv/import_csv_data.php",$button_import,"","","",0);
// if ($plugin["elib_plus_demo"])
// $toolbar .= $linterface->GET_LNK_IMPORT("import/cover/index.php",$Lang['libms']['import_book_cover'],"","","",0);
$keyword = htmlspecialchars($keyword);
$searchTag = "<input autocomplete=\"off\" name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";

// Search Box
$advanceSearchDiv = $libms->getAdvanceSearchDiv('item');

// ###########################################################################################################

// Top menu highlight setting
$linterface->LAYOUT_START(($DirectMsg == 'T') ? $xmsg : $Lang['General']['ReturnMessage'][$xmsg]);

// update filter Henry added 20131112
$ParTags = ' id="BookStatusUpdate" name="BookStatusUpdate" onchange="checkRemove(document.form1,\'UniqueID[]\',\'item_status_update.php?FromPage=' . $FromPage . '&BookID=' . $BookID . '\',\'' . $Lang['libms']['book']['updateBookStatusTo'] . ' [\'+this.options[this.selectedIndex].text+\']\n' . $Lang["libms"]["book"]["warning_tag_edit_confirm"] . '\');this.selectedIndex=0"';
$BookStatusUpdateSelection = $libms->GET_BOOK_ITEM_SELECTION($ParTags, '', '', $Lang['libms']['book']['updateBookStatusTo'], 'WRITEOFF');

// Table Action Button
// Edit button
$btnOption .= "<a href=\"javascript:checkEdit(document.form1,'UniqueID[]','book_item_edit.php?FromPage=" . $FromPage . "&BookID=" . $BookID . "')\" class=\"tool_edit\">" . $button_edit . "</a>";

// Copy button
$btnOption .= "<a href=\"javascript:checkEdit(document.form1,'UniqueID[]','book_item_edit.php?FromPage=" . $FromPage . "&BookID=" . $BookID . "&IsCopy=1')\" class=\"tool_copy\">" . $button_copy . "</a>";

$btnOption .= "<a>" . $BookStatusUpdateSelection . "</a>";

// Del button
$btnOption .= "<a href=\"javascript:checkRemove(document.form1,'UniqueID[]','item_remove.php?FromPage=" . $FromPage . "&BookID=" . $BookID . "')\" class=\"tool_delete\">" . $button_delete . "</a>";

// Fileter - Book Status
$ParTags = ' id="BookStatus" name="BookStatus" onchange="document.form1.submit()"';
$BookStatusSelection = $libms->GET_BOOK_ITEM_SELECTION($ParTags, '', $BookStatus);

// Location filter
$locationSelectionBox = '';
$locationInfo = $libms->GET_LOCATION_INFO('', 1);
$numOfLocationInfo = count($locationInfo);
$locationSelectionOption = array();

$locationSelectionOption[] = array(
    'NA',
    '(' . $Lang['libms']['bookmanagement']['NoLocation'] . ')'
);

for ($i = 0; $i < $numOfLocationInfo; $i ++) {
    
    $thisLocationCode = $locationInfo[$i]['LocationCode'];
    $thisLocationEngName = $locationInfo[$i]['DescriptionEn'];
    $thisLocationChiName = $locationInfo[$i]['DescriptionChi'];
    $thisOptionlocationName = Get_Lang_Selection($thisLocationChiName, $thisLocationEngName);
    $locationSelectionOption[] = array(
        $thisLocationCode,
        $thisOptionlocationName
    );
}
$locationSelectionBox = $linterface->GET_SELECTION_BOX($locationSelectionOption, 'name="LocationCode" id="LocationCode" onchange="document.form1.submit()"', "-- {$Lang['libms']['bookmanagement']['location']} --", $LocationCode, true);
$BookStatusSelection .= $locationSelectionBox;

// Distributor filter
$distributorSelectionBox = '';

$sql1 = "select DISTINCT i.Distributor
		from LIBMS_BOOK_UNIQUE as i 
		inner join LIBMS_BOOK as b on 
			b.BookID = i.BookID
		where (i.RecordStatus NOT LIKE 'DELETE' OR i.RecordStatus IS NULL) 
		{$bookStatus_cond}
		{$keyword_cond}
		{$acno_cond}
		order by i.Distributor";

$distributorInfo = $libms->returnArray($sql1);

$numOfDistributorInfo = count($distributorInfo);
$distributorSelectionOption = array();

$distributorSelectionOption[] = array(
    'NA',
    $Lang["libms"]["status"]["na"]
);

for ($i = 0; $i < $numOfDistributorInfo; $i ++) {
    if ($distributorInfo[$i]['Distributor'] != '')
        $distributorSelectionOption[] = array(
            $distributorInfo[$i]['Distributor'],
            $distributorInfo[$i]['Distributor']
        );
}
$distributorSelectionBox = $linterface->GET_SELECTION_BOX($distributorSelectionOption, 'name="DistributorSelect" id="DistributorSelect" onchange="document.form1.submit()"', "-- {$Lang['libms']['book']['distributor']} --", htmlspecialchars($DistributorSelect));
$BookStatusSelection .= $distributorSelectionBox;

// ACNO Filter
// $sql1 = "select DISTINCT i.ACNO
// from LIBMS_BOOK_UNIQUE as i
// inner join LIBMS_BOOK as b on
// b.BookID = i.BookID and i.BookID = '".$BookID."'
// where (i.RecordStatus NOT LIKE 'DELETE' OR i.RecordStatus IS NULL)
// {$bookStatus_cond}
// {$keyword_cond}
// {$distributor_cond}
// ";
// $acnoInfo = $libms->returnVector($sql1);
// $acnoOption = array();
// for($i=0;$i<count($acnoInfo);$i++){
// if($acnoInfo[$i] != '')
// $acnoOption[]= array($acnoInfo[$i], $acnoInfo[$i]);
// }
// $acnoSelectionBox = $linterface->GET_SELECTION_BOX($acnoOption, 'name="acnoSelect" id="acnoSelect" onchange="document.form1.submit()"', "-- {$Lang['libms']['book']['code']} --", $acnoSelect);
// $BookStatusSelection .= $acnoSelectionBox;
// Henry added 20131112
// $BookStatusSelection .= $BookStatusUpdateSelection;

$BookSubTab = $libms->get_book_manage_sub_tab($FromPage, $CurTab, $BookID, $UniqueID);
$RespondInfo = $libms->GET_BOOK_INFO($BookID);
$BookTitle = $RespondInfo[0]['BookTitle'];

if (strtolower($FromPage) != 'item') {
    $toolbar .= ' ' . $linterface->GET_LNK_NEW("book_item_add_existing.php?FromPage=" . $FromPage . $bookID_para . "&keyword=" . urlencode(trim($BookTitle)), $Lang['libms']['bookmanagement']['AddFromExistingItem'], "", "", "", 0);
}

$htmlAry['cancelBtn'] = '';
if (strtolower($FromPage) == 'periodical') {
    $htmlAry['cancelBtn'] = $libms->get_book_manage_cancel_button($BookID, $FromPage);
}

?>
<script
	src="/templates/jquery/jquery-1.6.1.min.js"
	type="text/javascript" charset="utf-8"></script>
<script src="/templates/jquery/jquery.autocomplete.js"
	type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet"
	type="text/css">
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="javascript">
var imgObj=0;
var callback = {
        success: function ( o )
        {
                writeToLayer('ToolMenu2',o.responseText);
                showMenu2("img_"+imgObj,"ToolMenu2");
        }
}
function hideMenu2(menuName){
		objMenu = document.getElementById(menuName);
		if(objMenu!=null)
			objMenu.style.visibility='hidden';
		setDivVisible(false, menuName, "lyrShim2");
}
function showMenu2(objName,menuName){
		  hideMenu2('ToolMenu2');
           objIMG = document.getElementById(objName);
           objToolMenu2 = document.getElementById('ToolMenu2');
           objkeyword = document.getElementById('keyword');
			offsetX = (objIMG==null)?0:objIMG.width;
			offsetY =0;            
            var pos_left = getPostion(objIMG,"offsetLeft");
			var pos_top  = getPostion(objIMG,"offsetTop");
			
			objDiv = document.getElementById(menuName);
			
			if(objDiv!=null){
				objDiv.style.visibility='visible';
//				objDiv.style.top = pos_top+offsetY+"px";
//				objDiv.style.left = pos_left+offsetX+"px";
				objDiv.style.top = pos_top+30+"px";
				objDiv.style.left = pos_left-282+"px";
				setDivVisible(true, menuName, "lyrShim2");
			}
}
function showInfo(val)
{
        obj = document.form1;

        var myElement = document.getElementById("ToolMenu2");

        writeToLayer('ToolMenu2','');
        imgObj = val;
        
		showMenu2("img_"+val,"ToolMenu2");
        YAHOO.util.Connect.setForm(obj);

        var path = "getHelpInfo.php?Val=2";
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

var timerobj;
function jsShowBookHide()
{
	timerobj = setTimeout(ShowBookHideAction,1000);
}

function ShowBookHideAction()
{
	$("#keyword").focus();
	clearTimeout(timerobj);
}

$().ready( function(){
//	$.ajaxSetup({	
//		cache:false, async:false
//	});
	
	if($("#keyword").length > 0){
		$("#keyword").autocomplete(
	      "ajax_get_book_suggestion.php",
	      {
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			onItemSelect: function() { jsShowBookHide(); },
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width:'175px'
	  		}
	    );
	}
});

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		$("#advanceSearch").val(0);
		document.form1.submit();
	}		
	else
		return false;
}

function DisplayAdvanceSearch() {
	
	var pos = $("#AdvanceSearchText").position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	
	topPos = parseInt(topPos) + 25;
	leftPos = parseInt(leftPos) - 482;
	
	//MM_showHideLayers('DR_search','','show');
	$("#advance_search").show();$("#advance_search").css({"position": "absolute", "top": topPos, "left": leftPos, "z-index":99, "background":"none repeat scroll 0% 0% rgb(244, 244, 244)", "padding":"7px 7px 0px","border":"1px solid rgb(204, 204, 204)","width":"500px"});
}

function HiddenAdvanceSearch(){
	$("#advance_search").hide();
}
</script>
<?php
if (strtolower($FromPage) == 'book' || strtolower($FromPage) == 'periodical') {
    ?>


<table class="form_table" width="100%">
	<tr>
		<td align="left"><font face="微軟正黑體" size="+1">《 <b><?=$BookTitle?></b>》
		</font></td>
	</tr>
</table>
<br style="clear: both" />
<?=$BookSubTab?>
<?php } ?>
<form name="form1" id="form1" method="POST"
	action="book_item_list.php?FromPage=<?=$FromPage?>&BookID=<?=$BookID?>"
	onSubmit="return checkForm()">
	<div class="content_top_tool" style="float: left;">
		<?=$toolbar?>
		</div>
		<?=$advanceSearchDiv?><div class="Conntent_search">|</div>
	<a href="javascript:showInfo(1)" style="float: right;"><img id='img_1'
		src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/inventory/icon_help.gif"
		border=0></a>
	<div class="Conntent_search"><?=$searchTag?></div>
	<br style="clear: both" />

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">
		        <?=$BookStatusSelection?>
	        </td>
        </tr>
        <tr class="table-action-bar">
			<td valign="bottom">
				<div class="common_table_tool">
		            <?=$btnOption?>
		        </div>
			</td>
		</tr>
	</table>

	<?=$li->display();?>
	<br /> <span class="tabletextrequire">*</span> <?= $Lang['libms']['book']['status']['remark'] ?>
	<div class="edit_bottom_v30">
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	<br style="clear: both;" /> <input type="hidden" name="pageNo"
		id="pageNo" value="<?=$li->pageNo; ?>" /> <input type="hidden"
		name="order" id="order" value="<?=$li->order; ?>" /> <input
		type="hidden" name="field" id="field" value="<?=$li->field; ?>" /> <input
		type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage"
		value="<?=$li->page_size?>" /> <input type="hidden" name="FromPage"
		id="FromPage" value="<?=$FromPage?>" /> <input type="hidden"
		name="advanceSearch" id="advanceSearch" value="<?=$advanceSearch?>" />
	<input type="hidden" name="BookID" id="BookID" value="<?=$BookID?>" />


	<iframe id='lyrShim2' scrolling='no' frameborder='0'
		style='position: absolute; top: 0px; left: 0px; visibility: hidden; display: none;'></iframe>
	<div id="ToolMenu2"
		style="position: absolute; width =0px; height =0px; visibility: hidden; z-index: 999999"></div>

</form>


<?php
$linterface->LAYOUT_STOP();

################################################################################################################
intranet_closedb();
?>