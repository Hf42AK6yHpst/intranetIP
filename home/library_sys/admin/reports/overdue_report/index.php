<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();

/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $libms->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

$linterface = new interface_html();

//$groupSelection = $libms->getGroupSelection();

# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsSpecialOpenTime";

$TAGS_OBJ[] = array($Lang['libms']['settings']['responsibility']);

$PAGE_NAVIGATION[] = array($Lang['libms']['settings']['PageSettingsSpecialOpenTime'], "special_open_time.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['new'], "");

# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();


?>


<form name="form1" method="post" action="overdue_result.php" onSubmit="return checkForm();">

<table class="form_table_v30">

<!-- Target -->
<tr valign="top">
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Target"]?></td>
	<td>
		<table class="inside_form_table">
			<tr>
				<td valign="top">
					<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
						<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
						<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
						<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
					</select>
				</td>
				<td valign="top" nowrap>
					<!-- Form / Class //-->
					<span id='rankTargetDetail'>
					<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="7"></select>
					</span>
					<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
						
					<!-- Student //-->
					<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
						<select name="studentID[]" multiple size="7" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
					</span>
					
				</td>
			</tr>
			<tr><td colspan="3" class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
		</table>
		<span id='div_Target_err_msg'></span>
	</td>
</tr>

<!-- Period -->
<tr valign="top">
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
	<td>
		<table class="inside_form_table">
			<tr>
				<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true; getActivityCategory();">
					<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?>  onClick="getActivityCategory();">
					<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
					<?=$selectSchoolYearHTML?>&nbsp;
					<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
					<span id="spanSemester"><?=$selectSemesterHTML?></span>
				</td>
			</tr>
			<tr>
				<td onClick="document.form1.radioPeriod_Date.checked=true; getActivityCategory();"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE")?" checked":"" ?> onClick="getActivityCategory();"> <?=$i_From?> </td>
				<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='getActivityCategory();' ")?>
				<br><span id='div_DateEnd_err_msg'></span></td>
				<td><?=$i_To?> </td>
				<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='getActivityCategory();' ")?>
				
			</tr>
		</table>
	</td>
</tr>



<!-- Category -->
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$eEnrollment['add_activity']['act_category']?></td>
	<td>
		<table class="inside_form_table" width="100%">
			<tr>
				<td><?=$category_selection?></td>
			</tr>
			<tr>
				<td>
					<span id="CategoryElements"></span> <?=$activityCategoryHTMLBtn?>
				</td>
			</tr>
		</table>
		<span id='div_Category_err_msg'></span>
	</td>
</tr>

<!-- Time(s) -->
<tr>
	<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"]?></td>
	<td><input type="text" name="times" id="times" size="3" value="0"/>
		<?=$linterface->Get_Radio_Button("timesOnAbove", "timesRange", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrAbove"])?>
		<?=$linterface->Get_Radio_Button("timesOnBelow", "timesRange", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrBelow"])?>
		<br><span id='div_Times_err_msg'></span>
	</td>
</tr>

<!-- Hour(s) -->
<tr>
	<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"]?></td>
	<td><input type="text" name="hours" id="hours" size="3" value="0" />
		<?=$linterface->Get_Radio_Button("hoursOnAbove", "hoursRange", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrAbove"])?>
		<?=$linterface->Get_Radio_Button("hoursOnBelow", "hoursRange", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrBelow"])?>
		<br><span id='div_Hours_err_msg'></span>
	</td>
</tr>

<!-- Display Unit -->
<tr>
	<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["DisplayUnit"]?></td>
	<td>
		<?=$linterface->Get_Radio_Button("displayUnitStudent", "displayUnit", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Student"])?>
		<?=$linterface->Get_Radio_Button("displayUnitClass", "displayUnit", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Class"])?>
	</td>
</tr>

<!-- Sort By -->
<tr>
	<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SortBy"]?></td>
	<td>
		<?=$linterface->Get_Radio_Button("sortByClassNameNumber", "sortBy", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"] . " / " . $Lang["eEnrolment"]["ActivityParticipationReport"]["ClassNumber"])?>
		<?=$linterface->Get_Radio_Button("sortByTimes", "sortBy", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"])?>
		<?=$linterface->Get_Radio_Button("sortByHours", "sortBy", 2, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"])?>
		<br>
		<?=$linterface->Get_Radio_Button("sortByOrderAsc", "sortByOrder", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["AscendingOrder"])?>
		<?=$linterface->Get_Radio_Button("sortByOrderDesc", "sortByOrder", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["DescendingOrder"])?>				
	</td>
</tr>
</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit")?>
<p class="spacer"></p>
</div>

<? /* ?>
<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="categoryID" id="categoryID" value="<?=$categoryID?>" />
<input type="hidden" name="Period" id="Period" value="<?=$Period?>" />
<input type="hidden" name="submitSelectBy" id="submitSelectBy" value="<?=$rankTarget?>" />
<input type="hidden" name="StatType" id="StatType" value="<?=$StatType?>" />
<input type="hidden" name="PeriodField1" id="PeriodField1" value="<?=$parPeriodField1?>" />
<input type="hidden" name="PeriodField2" id="PeriodField2" value="<?=$parPeriodField2?>" />
<? if($submit_flag=="YES") {?>
<input type="hidden" name="ByField" id="ByField" value="<?=implode(",", $parByFieldArr)?>" />
<input type="hidden" name="StatsField" id="StatsField" value="<?=implode(",", $parStatsFieldArr)?>" />
<input type="hidden" name="StatsFieldText" id="StatsFieldText" value="<?=intranet_htmlspecialchars(stripslashes(implode(",", $parStatsFieldTextArr)))?>" />
<input type="hidden" name="newItemID" id="newItemID" value="<? if(sizeof($parStatsFieldArr)>0) echo implode(",", $parStatsFieldArr)?>">
<? } ?>
<input type="hidden" name="SelectedForm" id="SelectedForm" />
<input type="hidden" name="SelectedFormText" id="SelectedFormText" />
<input type="hidden" name="SelectedClass" id="SelectedClass" />
<input type="hidden" name="SelectedClassText" id="SelectedClassText" />
<input type="hidden" name="SelectedItemID" id="SelectedItemID" />
<input type="hidden" name="SelectedItemIDText" id="SelectedItemIDText" />
<input type="hidden" name="id" id="id" value="">
<input type="hidden" name="itemNameColourMappingArr" id="itemNameColourMappingArr" value="<?=rawurlencode(serialize($itemNameColourMappingArr));?>">
<input type="hidden" name="showStatButton" id="showStatButton" value="0" />


<? */ ?>

<input type="hidden" name="studentFlag" id="studentFlag" value="0">
</form>
	
<script language="javascript">
$(document).ready(function(){
	changeTerm($("#selectYear").val());
 	showRankTargetResult($("#rankTarget").val(), '');
 	getActivityCategory();
});
</script>	

<?
intranet_closedb();
$linterface->LAYOUT_STOP();

?>
