<?php
// using:
/*
 * Log:
 * 
 * 	2017-06-06 (Cameron)
 * 		- place set cookie function after include global.php to support php5.4 [case #E117788]
 * 
 * 	2013-08-30:	Ivan [2013-0829-1531-18066]
 * 	improved: show holidays of future academic year also now instead of show holidays from current academic year only 
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

//global $intranet_db;

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsHoliday";

$libms->MODULE_AUTHENTICATION($CurrentPage);

//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 1 : $field;

$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());



//dump($current_period);
//$sql = "SELECT 
//			`Event`, DATE_FORMAT(`DateFrom`, '%Y-%m-%d'),DATE_FORMAT(`DateEnd`, '%Y-%m-%d')
//            
//		FROM 
//			LIBMS_HOLIDAY
//		
//		WHERE
//			`DateEnd` BETWEEN '{$current_period['StartDate']}' AND '{$current_period['EndDate']}'
//			OR
//			`DateFrom` BETWEEN '{$current_period['StartDate']}' AND '{$current_period['EndDate']}'
//        ";
$sql = "SELECT 
			`Event`, DATE_FORMAT(`DateFrom`, '%Y-%m-%d'),DATE_FORMAT(`DateEnd`, '%Y-%m-%d')
            
		FROM 
			LIBMS_HOLIDAY
		
		WHERE
			`DateFrom` >= '{$current_period['StartDate']}'
        ";
//dump($sql);;

$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array("Event", "DateFrom","DateEnd");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff =  "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='33%' >".$li->column($pos++, $Lang['libms']['holiday']['Event'])."</td>\n";
$li->column_list .= "<th width='33%' >".$li->column($pos++, $Lang['libms']['holiday']['DateFrom'])."</td>\n";
$li->column_list .= "<th width='33%' >".$li->column($pos++, $Lang['libms']['holiday']['DateEnd'])."</td>\n";
//$li->column_list .= "<th width='1'>".$li->check("Code[]")."</td>\n";


//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['settings']['holidays']);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();



?>
<script language="javascript">
<!--
//-->
</script>



<form name="form1" method="post" action="">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				<tr>
					<td>
					<?=($junior_mck>0)? $Lang['libms']['holiday']['disclaimer_ej'] : $Lang['libms']['holiday']['disclaimer']?> <br />
					<?=$Lang["libms"]["S_OpenTime"]["remark"]?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();
 
 
 
intranet_closedb();


?>
