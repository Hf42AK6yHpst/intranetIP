<?php

# using by  
/*
 * 	2017-03-03 [Cameron]
 * 		- create this file
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$linterface = new interface_html();
# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsSecurity";

$TAGS_OBJ[] = array($Lang['libms']['settings']['security']['circulation_terminals']);
# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$_GET['xmsg']]);

## retrieve IPList
$table = 'LIBMS_SECURITY_SETTING'; 
$field = 'SettingValue';
$condition['SettingName'] = PHPToSQL('IPList'); 
$rs = $libms->SELECTFROMTABLE($table,$field,$condition);
if (count($rs)==1) {
	$IPList = $rs[0][$field];
}
else {
	$IPList = '';
}

$x = '';
$text=$Lang['libms']['settings']['security'];
$current_ip=isset($_SERVER)?$_SERVER['REMOTE_ADDR']:$HTTP_SERVER_VARS['REMOTE_ADDR'];
$txt_ip_list=$linterface->GET_TEXTAREA("IPList", $IPList);
$display_value=($IPList==''?$Lang['General']['EmptySymbol']:nl2br($IPList));
$instruction=$linterface->Get_Warning_Message_Box($text['InstructionTitle'],$text['InstructionContent']);

$x = $instruction.'
<table class="form_table_v30">
	<tr>
		<td class="field_title">
			'.$text['SettingTitle'].'
		</td>
		<td>
			<span class="tabletextremark">'.$text['SettingControlTitle'].': '.$current_ip.'</span><br />
			<div class="EditView" style="display:none">
				'.$txt_ip_list.'
			</div>
			<div class="DisplayView">'.$display_value.'</div>
		</td>
	</tr>
</table>';

$htmlAry['formTable'] =$x;


### buttons
$htmlAry['editBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['resetBtn'] = $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();",'resetBtn');

?>
<script language="javascript" type="text/javascript">

$( document ).ready(function() {
  $('#submitBtn').hide();
  $('#resetBtn').hide();
});

function goEdit(){
	$('.EditView').show();
	$('.DisplayView').hide();
	$('#editBtn').hide();
	$('#submitBtn').show();
	$('#resetBtn').show();
}

function goSubmit(){
	$('#form1').submit();
}

function ResetInfo()
{
	document.form1.reset();
	$('.EditView').hide();
	$('.DisplayView').show();
	$('#submitBtn').hide();
    $('#resetBtn').hide();
    $('#editBtn').show();
}

</script>

<form name="form1" id="form1" method="post" action="terminal_ip_setting_update.php">
	<div id="show_div">
	
		<div class="table_board">
			<?=$htmlAry['formTable']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
			
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['editBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<p class="spacer"></p>
		</div>
	</div>

</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

