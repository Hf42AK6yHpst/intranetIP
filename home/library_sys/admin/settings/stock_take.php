<?php
// using: 

/************************************
 * 
 * Date:	2017-04-12 (Cameron)
 * Details:	change parameter from 0 to 23 in $li->column_array() for SchemeTitle to handle special character [DM#3171]
 * 
 * Date:	2014-06-06 (Henry)
 * Details: change wordings and add remarks
 * Version: ip.2.5.5.7.1
 * 
 ************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

################## Set Cookies Start ###################
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_stocktake_admin_page_field", "field");
$arrCookies[] = array("elibrary_plus_stocktake_admin_page_order", "order");
$arrCookies[] = array("elibrary_plus_stocktake_admin_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_stocktake_admin_page_number", "pageNo");

if(!isset($field)) $field = 2;
		
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}
		
################## Set Cookies End  ###################
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();
$result = $libms->SELECTFROMTABLE('LIBMS_NOTIFY_SETTING', '*', '','',1,'name');
$linterface = new interface_html();

# Page Settings
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsStockTake";
$TAGS_OBJ[] = array($Lang["libms"]["settings"]["stock-take"]);

$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();

# Toolbar
$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'stock_take_scheme_new.php')",$button_new,"","","",0);
//$toolbar .= $linterface->GET_LNK_IMPORT("book_location_import/import_csv_data.php",$button_import,"","","",0);

########################## DB Table Start ##############################
if (isset($elibrary_plus_stocktake_admin_page_size) && $elibrary_plus_stocktake_admin_page_size != "") $page_size = $elibrary_plus_stocktake_admin_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL();

$li = new libdbtable2007($field, $order, $pageNo);
global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";
$li->field_array = array("SchemeTitle","StartDate", "EndDate","RecordStartDate", "RecordEndDate", "DateModified", "CheckBox");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->column_array = array(23,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff =  "IP25_table";
$pos = 0;

$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='34%' >".$li->column($pos++, $Lang['libms']['settings']['stocktake']['name'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['settings']['stocktake']['stocktakeDate'].' ('.$Lang['libms']["settings"]['stocktake']['from'].')')."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['settings']['stocktake']['stocktakeDate'].' ('.$Lang['libms']["settings"]['stocktake']['to'].')')."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['settings']['stocktake']['recordStartDate'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['settings']['stocktake']['recordEndDate'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['settings']['stocktake']['LastUpdated'])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("StocktakeSchemeID[]")."</td>\n";

########################## DB Table Start End ##############################
?>

<form name="form1" method="post" action="stock_take.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="table-action-bar">
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'StocktakeSchemeID[]','stock_take_scheme_new.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'StocktakeSchemeID[]','stock_take_scheme_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><?=$li->display();?>
					</td>
				</tr>
				<tr>
					<td>
					<span class='tabletextrequire'>*</span> <?=$Lang['libms']['settings']['stocktake']['recordStartDate']?>: <?=$Lang['libms']['settings']['stocktake']['recordStartDateRemarks']?><br />
					<span class='tabletextrequire'>*</span> <?=$Lang['libms']['settings']['stocktake']['recordEndDate']?>: <?=$Lang['libms']['settings']['stocktake']['recordEndDateRemarks']?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form>

<?php
//echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>