<?php
# using: Henry

/***************************************
 * Date: 	2015-01-15 (Henry)
 * Details: added bookLan
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");



$map['bookLoc'] = array( 'table' => 'LIBMS_LOCATION', 'key' => 'LocationCode');
$map['bookCat'] = array( 'table' => 'LIBMS_BOOK_CATEGORY', 'key' => 'BookCategoryCode');
$map['resType'] = array( 'table' => 'LIBMS_RESOURCES_TYPE', 'key' => 'ResourcesTypeCode');
$map['cirType'] = array( 'table' => 'LIBMS_CIRCULATION_TYPE', 'key' => 'CirculationTypeCode');
$map['bookLan'] = array( 'table' => 'LIBMS_BOOK_LANGUAGE', 'key' => 'BookLanguageCode');

$needle = $_REQUEST['needle'];
$type =  $_REQUEST['type'];


$return['success'] = FALSE; 
if (isset($map[$type])){
	//echo 1;
	$tableinfo = $map[$type];
	$return['unique'] = checkUniqueness($tableinfo['table'],$tableinfo['key'], $needle);
	$return['success'] = TRUE;
}

echo json_encode($return);


function checkUniqueness($table, $kayStackKey, $needle){
	global $Lang;
	
	intranet_auth();
	intranet_opendb();
	
	if ( empty($table) || empty($kayStackKey) ){
		return false;
	}
	
	$libms = new liblms();
	$filter[$kayStackKey] = PHPToSQL($needle);
	if($_REQUEST['type'] == 'bookCat'){
		$filter['BookCategoryType'] = ($_REQUEST['bookcategorytype']?$_REQUEST['bookcategorytype']:1);
	}
	//dump($filter);
	$result = $libms-> SELECTFROMTABLE( $table,$kayStackKey,$filter);
	//dump($result);
	return ( empty($result) )? TRUE : FALSE;
		
}