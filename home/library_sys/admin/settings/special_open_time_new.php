<?php
// using: 

/*
 * 	2017-07-17 (Henry)
 * 		- add validation of open time and close time [Case#C117663]
 * 
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $libms->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

$linterface = new interface_html();

//$groupSelection = $libms->getGroupSelection();

# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsSpecialOpenTime";

$TAGS_OBJ[] = array($Lang['libms']['settings']['PageSettingsSpecialOpenTime']);

$PAGE_NAVIGATION[] = array($Lang['libms']['settings']['PageSettingsSpecialOpenTime'], "special_open_time.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['new'], "");

# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">

function checkForm(form1) {
	var filltext, valid_opent, valid_closet, valid_date_from, valid_date_end ;
	filltext = valid_opent = valid_closet = valid_date_from = valid_date_end = true;
	
	if($('#DateFrom').val() == "")	{
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['S_OpenTime']['DateFrom'] ?>");	
		$('#DateFrom').focus();
		return false;
	} else if($('#DateEnd').val() == "") {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['S_OpenTime']['DateEnd'] ?>");	
		$('#DateEnd').focus();
		return false;
	} 
	
	
	
	valid_date_from = IsValidDate($('#DateFrom').val());
	valid_date_end = IsValidDate($('#DateEnd').val());
	
		
	if(($('#checkopen').val() == "1" )&& valid_date_from && valid_date_end ) {
		if($('#OpenTime').val() == "") {
			alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['S_OpenTime']['OpenTime'] ?>");	
			$('#OpenTime').focus();
			return false;
		} else if($('#CloseTime').val() == "") {
			alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['S_OpenTime']['CloseTime'] ?>");	
			$('#CloseTime').focus();
			return false;
		} 
			
		valid_opent = IsValidTime($('#OpenTime').val());
		valid_closet = IsValidTime($('#CloseTime').val());
		
		if(valid_opent && valid_closet && $('#OpenTime').val() >= $('#CloseTime').val()){
			alert("<?=$Lang["libms"]["open_time"]["time_rangemsg"]?>");	
			$('#OpenTime').focus();
			return false;
		}
	}
	
	return (valid_opent && valid_closet && valid_date_from && valid_date_end);
	
	
}


// Checks if time is in HH:MM:SS format.
function IsValidTime(timeStr) {
// Checks if time is in HH:MM:SS AM/PM format.
// The seconds and AM/PM are optional.

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?$/;

var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("<?= $Lang['libms']['open_time']['time_timemsg'] ?>");
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];

if (second=="") { second = null; }

if (hour < 0  || hour > 23) {
alert("<?= $Lang['libms']['open_time']['time_hourmsg'] ?>");
return false;
}
if (minute<0 || minute > 59) {
alert ("<?= $Lang['libms']['open_time']['time_minsmsg'] ?>");
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("<?= $Lang['libms']['open_time']['time_secmsg'] ?>");
return false;
}
return true;
}

function IsValidDate(dateStr) {
	var DatePat = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;

	var matchArray = dateStr.match(DatePat);
	if (matchArray == null) {
		alert("<?= $Lang['libms']['open_time']['datemsg'] ?>");
		return false;
	}
	year = matchArray[1];
	month = matchArray[2];
	day = matchArray[3];

	if (year < 1  || year > 2300) {
		alert("<?= $Lang['libms']['S_OpenTime']['yearmsg'] ?>");
		return false;
	}
	if (month<1 || month > 12) {
		alert ("<?= $Lang['libms']['S_OpenTime']['monthsmsg'] ?>");
		return false;
	}
	if (day < 1 || day > 31) {
		alert ("<?= $Lang['libms']['S_OpenTime']['daymsg'] ?>");
		return false;
	}
	if ((month==4 || month==6 || month==9 || month==11) && day==31) {
	alert("Month "+month+" doesn`t have 31 days!")
	return false;
	}
	
	if (month == 2) { // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day > 29 || (day==29 && !isleap)) {
			alert("<?= $Lang['libms']['S_OpenTime']['daymsg'] ?>");
			return false;
		}
	}	
	
	return true;
}

function check_open(meme) {
	if((meme.val()) == 0){
		$('#OpenTime').val('');
		$('#OpenTime').attr("disabled", true);
		$('#OpenTime').parents('tr:first').hide();
		$('#CloseTime').val('');
		$('#CloseTime').attr("disabled", true);
		$('#CloseTime').parents('tr:first').hide(); 
	}else if((meme.val()) == 1){
		$('#OpenTime').attr("disabled", false); 	
		$('#OpenTime').parents('tr:first').show();
		$('#CloseTime').attr("disabled", false); 
		$('#CloseTime').parents('tr:first').show();	
	}
}

$().ready( function(){
	$('.checkopen').change(function () {
		check_open($(this));
	});
});

</script>

<?
# option for open  ......
$CheckOpen[] = array("0", $Lang['libms']['open_time']['close']);
$CheckOpen[] = array("1", $Lang['libms']['open_time']['open']);
?>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" method="post" action="special_open_time_new_update.php" onSubmit="return checkForm(this)">

<table width="90%" align="center" border="0">
<tr><td>

	<table class="form_table_v30">
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['DateFrom']?></td>
			<td><?=$linterface->GET_DATE_PICKER("DateFrom", "")?></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['DateEnd']?></td>
			<td><?=$linterface->GET_DATE_PICKER("DateEnd", "")?></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['open']." / ".$Lang['libms']['S_OpenTime']['close']?></td>
			<td><?=$linterface->GET_SELECTION_BOX($CheckOpen, " id ='checkopen' name='checkopen' class='checkopen' ", '', '1');?></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['OpenTime']?></td>
			<td><input id="OpenTime" name="OpenTime" type="text" class="textboxnum" value=""></td>
		</tr>
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['CloseTime']?></td>
			<td><input id="CloseTime" name="CloseTime" type="text" class="textboxnum" value=""></td>
		</tr>
		
		
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='special_open_time.php'")?>
	</div>	

</td></tr>
</table>

</form><?php
echo $linterface->FOCUS_ON_LOAD("form1.ResponsibilityCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
