<?php

# using by  

########## Change Log [Start] ###########
#
#   Date    :   2019-12-11 Henry
#               - add setting circulation_lost_book_overdue_charge
#
#   Date    :   2019-05-06 Cameron
#               - add setting leave_user_after_borrow_book [case #M153872]
#
#   Date    :   2018-11-14 Cameron
#               - add setting show_number_of_loans_when_borrow_book [case #Y150892]
#
#	Date	: 	2018-09-26 Cameron
#				- add setting allow_edit_own_book_review [case #W139304]
#
#	Date	: 	2018-09-20 Cameron
#				- add setting allow_reserve_available_books_in_portal [case #A135894]
#
# 	Date	:	2017-04-06 Cameron
# 				- handle apostrophe problem for school name by applying stripslashes() when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
#
#	Date	: 	2017-01-10 Cameron
#				- add setting preference_photo_show
#
#	Date	: 	2016-08-29 Cameron
#				- add setting: apply additional charge for lost book or not
#				- add setting: allow to change penalty amount for lost book or not
#
#	Date	:	2016-03-11 Henry
#				- add setting school_display_name_eng
#
#	Date	:	2016-02-19 Cameron
#				- add setting batch_book_renew_by_confirm
#
#	Date	:	2016-02-15 Cameron
#				- add function check_form() to avoid inputing negative value for fixed price punishment of lost book
#
#	Date	:	2016-02-04 Cameron
#				- add setting circulation_lost_book_price_by and circulation_lost_book_price
#				- include jquery.validate.min.js in local server rather than access it from original hoster		  
#
#	Date	: 	2015-11-10 Cameron
#				- add setting preference_recommend_book_order
#
#	Date	: 	2015-08-12 Cameron
#				- add setting circulation_return_book_by_confirm & circulation_borrow_return_accompany_material
#
#	Date	:	2015-07-13 	Cameron
#				- add setting circulation_display_accompany_material
#
#	Date	:	2015-05-20	Henry
#				Change the order of each setting with different category [Case#E74700]
########## Change Log [End] ###########

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();

$result = $libms->SELECTFROMTABLE('LIBMS_SYSTEM_SETTING', '*', '','',1);

/*global_lockout
school_display_name

override_password
waive_password

circulation_duedate_method
max_overdue_limit

max_book_reserved_limit

circulation_student_by_selection
circulation_teacher_by_selection
batch_return_handle_overdue
*/

$fixed_lost_book_price = 0;

$tempResult = array();
for($i=0;$i<count($result);$i++){
	if($result[$i]['name'] == 'global_lockout'){
		$tempResult[0] = $result[$i];
		$tempResult[0]['header'] = $Lang['libms']["settings"]['system']['general'];
		continue;
	}
	if($result[$i]['name'] == 'school_display_name'){
		$tempResult[1] = $result[$i];
		if (!get_magic_quotes_gpc()) {
			$tempResult[1]['value'] = stripslashes($tempResult[1]['value']);
		}
		continue;
	}
	if($result[$i]['name'] == 'school_display_name_eng'){
		$tempResult[2] = $result[$i];
		if (!get_magic_quotes_gpc()) {
			$tempResult[2]['value'] = stripslashes($tempResult[2]['value']);
		}
		$tempResult[2]['footer'] = 1;
		continue;
	}
	if($result[$i]['name'] == 'override_password'){
		$tempResult[3] = $result[$i];
		$tempResult[3]['header'] = $Lang['libms']["settings"]['system']['password_setting'];
		continue;
	}
	if($result[$i]['name'] == 'waive_password'){
		$tempResult[4] = $result[$i];
		$tempResult[4]['footer'] = 1;
		continue;
	}
	
	if($result[$i]['name'] == 'show_number_of_loans_when_borrow_book'){
	    $tempResult[5] = $result[$i];
	    $tempResult[5]['header'] = $Lang['libms']["settings"]['system']['book_loan_setting'];
	    continue;
	}
	
	if($result[$i]['name'] == 'leave_user_after_borrow_book'){
	    $tempResult[6] = $result[$i];
	    $tempResult[6]['footer'] = 1;
	    continue;
	}
	
	if($result[$i]['name'] == 'circulation_duedate_method'){
		$tempResult[7] = $result[$i];
		$tempResult[7]['header'] = $Lang['libms']["settings"]['system']['book_return_setting'];
		continue;
	}
	if($result[$i]['name'] == 'max_overdue_limit'){
		$tempResult[8] = $result[$i];
		continue;
	}
	
	if($result[$i]['name'] == 'circulation_return_book_by_confirm'){
		$tempResult[9] = $result[$i];
		$tempResult[9]['footer'] = 1;
		continue;
	}
	
	if($result[$i]['name'] == 'batch_book_renew_by_confirm'){
		$tempResult[10] = $result[$i];
		$tempResult[10]['header'] = $Lang['libms']["settings"]['system']['book_renew_setting'];
		$tempResult[10]['footer'] = 1;
		continue;
	}
	
	if($result[$i]['name'] == 'max_book_reserved_limit'){
		$tempResult[11] = $result[$i];
		$tempResult[11]['header'] = $Lang['libms']["settings"]['system']['book_reserve_setting'];
		continue;
	}

	if($result[$i]['name'] == 'allow_reserve_available_books_in_portal'){
	    $tempResult[12] = $result[$i];
	    $tempResult[12]['footer'] = 1;
	    continue;
	}
	
	if($result[$i]['name'] == 'circulation_student_by_selection'){
		$tempResult[13] = $result[$i];
		$tempResult[13]['header'] = $Lang['libms']["settings"]['system']['circulation_setting'];
		continue;
	}
	if($result[$i]['name'] == 'circulation_teacher_by_selection'){
		$tempResult[14] = $result[$i];
		continue;
	}
	if($result[$i]['name'] == 'batch_return_handle_overdue'){
		$tempResult[15] = $result[$i];
		continue;
	}
	if($result[$i]['name'] == 'circulation_display_accompany_material'){
		$tempResult[16] = $result[$i];
		continue;
	}
	if($result[$i]['name'] == 'circulation_borrow_return_accompany_material'){
		$tempResult[17] = $result[$i];				
		continue;
	}
	if($result[$i]['name'] == 'circulation_lost_book_price_by'){
		$tempResult[18] = $result[$i];
		continue;
	}
	if($result[$i]['name'] == 'circulation_lost_book_additional_charge'){
		$tempResult[19] = $result[$i];
		continue;
	}
	if($result[$i]['name'] == 'circulation_lost_book_overdue_charge'){
		$tempResult[20] = $result[$i];
		continue;
	}
	if($result[$i]['name'] == 'circulation_lost_book_allow_change_penalty'){
		$tempResult[21] = $result[$i];
		$tempResult[21]['footer'] = 1;		
		continue;
	}

	if($result[$i]['name'] == 'book_review_allow_edit_own_book_review'){
		$tempResult[22] = $result[$i];
		$tempResult[22]['header'] = $Lang['libms']["settings"]['system']['book_review_setting'];
		$tempResult[22]['footer'] = 1;
		continue;
	}

	if($result[$i]['name'] == 'preference_recommend_book_order'){
	    $tempResult[23] = $result[$i];
	    $tempResult[23]['header'] = $Lang['libms']["settings"]['system']['preference_setting'];
	    continue;
	}
	
	if($result[$i]['name'] == 'preference_photo_show'){
		$tempResult[24] = $result[$i];
		$tempResult[24]['footer'] = 1;		
		continue;
	}

	if($result[$i]['name'] == 'circulation_lost_book_price'){
		$fixed_lost_book_price = $result[$i]['value']; 
		continue;
	}
	if($result[$i]['name'] == 'circulation_lost_book_additional_charge_fixed'){
		$lost_book_additional_charge_fixed = $result[$i]['value']; 
		continue;
	}
	if($result[$i]['name'] == 'circulation_lost_book_additional_charge_ratio'){
		$lost_book_additional_charge_ratio = $result[$i]['value']; 
		continue;
	}
	
}


ksort($tempResult);

$result = $tempResult;

//dump ($result);
$linterface = new interface_html();
# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsSystemSettings";

$TAGS_OBJ[] = array($Lang["libms"]["settings"]["title"]);
# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();

?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<!--<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>-->
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.validate.min.js"></script>

<script language="javascript">
<!--

function EditInfo()
{
	$('#EditBtn').hide(); 
	$('.notify_viewer').hide();
	$('.notify_editor').show();
	$('#buttons_div').show();

}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').show();
	$('.notify_viewer').show();
	$('.notify_editor').hide();
	
	$('#buttons_div').hide();
}

function update_FalseOrInt(){
	$this=$(this);
	$input = $this.next('input.in_FalseOrInt');
	if( $this.is(':checked') ) {
		if ($input.val() <= 0 || $input.val() == '' ){
			$input.val(0);
		}
		$input.show();
	}else{
		$input.val(-1);
		$input.hide();
	}
}


//ready
$(function (){
	$('#submit').addClass('submit');
	$('.chk_FalseOrInt')
	.change( update_FalseOrInt)
	.each( update_FalseOrInt);
	$('#form1').validate();
	
	setAdditionalChargeRatioBased();
	
	$("input:radio[name='notify[circulation_lost_book_price_by]']").click(function(){
		if ($(this).val() == '2') {
			$('#id_input_circulation_lost_book_price').css({'display':'inline-block'});
			$('#id_input_circulation_lost_book_price').addClass('required');	
		}
		else {
			$('#id_input_circulation_lost_book_price').css({'display':'none'});
			$('#id_input_circulation_lost_book_price').removeClass('required');
		}
		setAdditionalChargeRatioBased();
	});
	
	$("input:radio[name='notify[circulation_lost_book_additional_charge]']").click(function(){
		if ($(this).val() == '1') {
			$('#id_input_circulation_lost_book_additional_charge_fixed').css({'display':'inline-block'});
			$('#id_input_circulation_lost_book_additional_charge_fixed').addClass('required');
			$('#id_input_circulation_lost_book_additional_charge_ratio').css({'display':'none'});
			$('#id_input_circulation_lost_book_additional_charge_ratio').removeClass('required');
		}
		else if ($(this).val() == '2') {
			$('#id_input_circulation_lost_book_additional_charge_ratio').css({'display':'inline-block'});
			$('#id_input_circulation_lost_book_additional_charge_ratio').addClass('required');
			$('#id_input_circulation_lost_book_additional_charge_fixed').css({'display':'none'});
			$('#id_input_circulation_lost_book_additional_charge_fixed').removeClass('required');
		}
		else {
			$('#id_input_circulation_lost_book_additional_charge_fixed').css({'display':'none'});
			$('#id_input_circulation_lost_book_additional_charge_fixed').removeClass('required');
			$('#id_input_circulation_lost_book_additional_charge_ratio').css({'display':'none'});
			$('#id_input_circulation_lost_book_additional_charge_ratio').removeClass('required');
		}
	});
	
});

function setAdditionalChargeRatioBased() {
	var penalty_based_on = $("input:radio[name='notify[circulation_lost_book_price_by]']:checked");
	$('.notify_editor #ratio_based, .notify_viewer #ratio_based').html($("label[for='"+penalty_based_on.attr("id")+"']").text());
}

function check_form() {
	if ($("input:radio[name='notify[circulation_lost_book_price_by]']:checked").val() == '2') {
		if ($('#id_input_circulation_lost_book_price').val() < 0 ) {
			alert("<?=$Lang['libms']["settings"]['system']['circulation_input_non_negative_value']?>");
			$('#id_input_circulation_lost_book_price').focus();
			return false;
		}
	}
	if ($("input:radio[name='notify[circulation_lost_book_additional_charge]']:checked").val() == '1') {
		if ($('#id_input_circulation_lost_book_additional_charge_fixed').val() < 0 ) {
			alert("<?=$Lang['libms']["settings"]['system']['circulation_input_non_negative_value']?>");
			$('#id_input_circulation_lost_book_additional_charge_fixed').focus();
			return false;
		}
	}
	if ($("input:radio[name='notify[circulation_lost_book_additional_charge]']:checked").val() == '2') {
		if ($('#id_input_circulation_lost_book_additional_charge_ratio').val() < 0 ) {
			alert("<?=$Lang['libms']["settings"]['system']['circulation_input_non_negative_value']?>");
			$('#id_input_circulation_lost_book_additional_charge_ratio').focus();
			return false;
		}
	}
	
	return true;
}

//-->
</script>
<style type="text/css">
label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }
.submit { margin-left: 12em; }
</style>
<div style=' width: 10% ; margin-left: auto ; margin-right: auto ;'><?= $linterface->GET_SYS_MSG($xmsg) ?></div>
	<div class="table_row_tool row_content_tool" >
		<input type="button" class="formsmallbutton " onclick="javascript:EditInfo();" name="EditBtn" id="EditBtn" value="<?=$Lang["libms"]["general"]["edit"]?>">
	</div>
	<div>
<form id='form1' name="form1" method="post" action="system_settings_update.php" onSubmit="return check_form();">
<table width="90%" align="center" border="0">
<tr><td>
	
		<?php foreach ($result as $row){
			if (($row['name'] == 'due_date_reminder') || ($row['name'] == 'circulation_lost_book_price') || ($row['name'] == 'circulation_lost_book_additional_charge_fixed') || ($row['name'] == 'circulation_lost_book_additional_charge_ratio'))
				continue;
			$bool_option_no = ($Lang['libms']["settings"]['system'][$row['name']."_no"]!="") ? $Lang['libms']["settings"]['system'][$row['name']."_no"] : $Lang['libms']["settings"]['system']['no'];
			$bool_option_yes = ($Lang['libms']["settings"]['system'][$row['name']."_yes"]!="") ? $Lang['libms']["settings"]['system'][$row['name']."_yes"] : $Lang['libms']["settings"]['system']['yes'];
			
			if ($row['name']=="system_open_date" || $row['name']=="system_end_date" )
			{
				continue;
			}
			
			 $name = $Lang['libms']["settings"]['system'][$row['name']];
			 
			 $value = htmlspecialchars($row['value']);
			 if($row['type'] == 'BOOL'){
				$value = empty($value)?$bool_option_no:$bool_option_yes;
			 }
			 
			 if ($row['name'] == 'circulation_lost_book_price_by') {
			 	switch ($row['value']) {
			 		case 1:	// by list price
			 			$value = $Lang['libms']["settings"]['system']['circulation_lost_book_price_by_list'];
			 			break;
			 		case 2:	// by fixed price
			 			$value = $Lang['libms']["settings"]['system']['circulation_lost_book_price_by_fixed'].' '.$fixed_lost_book_price;
			 			break;
			 		case 0:	// by purchase price
			 		default:
			 			$value = $Lang['libms']["settings"]['system']['circulation_lost_book_price_by_purchase'];
			 			break;
			 	}
			 }

			 if ($row['name'] == 'circulation_lost_book_additional_charge') {
			 	switch ($row['value']) {
			 		case 1:	// by fixed amount
			 			$value = $Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_by_fixed'].' '.$lost_book_additional_charge_fixed;
			 			break;
			 		case 2:	// by ratio of circulation_lost_book_price_by
			 			$value = $Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_by_ratio'].' '.$lost_book_additional_charge_ratio.'%';
			 			break;
			 		case 0:	// no additional charge
			 		default:
			 			$value = $Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_none'];
			 			break;
			 	}
			 }
			 
		?>
		<?if($row['header']){?>
		<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$row['header']?> </span>-</em>
		<p class="spacer"></p>
		</div>
		<table class="form_table_v30">
		<?}?>
		<tr>
			<td class="field_title" >
				<?= $name ?>
			</td>
			<td>
				<div class='notify_viewer'>
					<?=$value?>
				</div>
				
				<div class='notify_editor' style='display:none'>
					<?php if($row['type'] == 'STRING'): ?>
						<input id='id_input_<?= $row['name'] ?>' name="notify[<?= $row['name'] ?>]" value="<?=$value?>" />
					<?php elseif($row['type'] == 'INT'): ?>
						<?php if ($row['name'] == 'circulation_lost_book_price_by') { ?>
							<input type="radio" id='id_input_<?= $row['name'] ?>_0' name="notify[<?= $row['name'] ?>]" value="0"  <?=($row['value'] == '0')?'checked':'';?>> <label for="id_input_<?= $row['name'] ?>_0"><?=$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_purchase']?></label>&nbsp; 
							<input type="radio" id='id_input_<?= $row['name'] ?>_1' name="notify[<?= $row['name'] ?>]" value="1"  <?=($row['value'] == '1')?'checked':'';?>> <label for="id_input_<?= $row['name'] ?>_1"><?=$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_list']?></label>&nbsp;
							<input type="radio" id='id_input_<?= $row['name'] ?>_2' name="notify[<?= $row['name'] ?>]" value="2"  <?=($row['value'] == '2')?'checked':'';?>> <label for="id_input_<?= $row['name'] ?>_2"><?=$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_fixed']?></label>&nbsp;
							<input id='id_input_circulation_lost_book_price' name="notify[circulation_lost_book_price]" value="<?=$fixed_lost_book_price?>" class='number' style='display:<?=($row['value'] == '2')?'inline-block':'none'?>'/>
						<?php } else if ($row['name'] == 'circulation_lost_book_additional_charge') { ?>
							<input type="radio" id='id_input_<?= $row['name'] ?>_0' name="notify[<?= $row['name'] ?>]" value="0"  <?=($row['value'] == '0')?'checked':'';?>> <label for="id_input_<?= $row['name'] ?>_0"><?=$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_none']?></label>&nbsp; 
							<input type="radio" id='id_input_<?= $row['name'] ?>_1' name="notify[<?= $row['name'] ?>]" value="1"  <?=($row['value'] == '1')?'checked':'';?>> <label for="id_input_<?= $row['name'] ?>_1"><?=$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_by_fixed']?></label>&nbsp;
							<input id='id_input_circulation_lost_book_additional_charge_fixed' name="notify[circulation_lost_book_additional_charge_fixed]" value="<?=$lost_book_additional_charge_fixed?>" class='number' style='display:<?=($row['value'] == '1')?'inline-block':'none'?>'/>
							<input type="radio" id='id_input_<?= $row['name'] ?>_2' name="notify[<?= $row['name'] ?>]" value="2"  <?=($row['value'] == '2')?'checked':'';?>> <label for="id_input_<?= $row['name'] ?>_2"><?=$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_by_ratio']?></label>&nbsp;
							<input id='id_input_circulation_lost_book_additional_charge_ratio' name="notify[circulation_lost_book_additional_charge_ratio]" value="<?=$lost_book_additional_charge_ratio?>" class='number' style='display:<?=($row['value'] == '2')?'inline-block':'none'?>'/>%
						<?php } else {?>
							<input type='number' id='id_input_<?= $row['name'] ?>' name="notify[<?= $row['name'] ?>]" value="<?=$value?>" class='required number' />
						<?php }?>							
					<?php elseif($row['type'] == 'FALSEORINT'): ?>	
						
						<input type='checkbox' class='chk_FalseOrInt' id='id_input_<?= $row['name'] ?>_chk' name="input_<?= $row['name'] ?>_chk" <?=($value>=0)?'checked':''?> />
						&nbsp; <input type='number'  class='in_FalseOrInt required number' min='-1' max='999' id='id_input_<?= $row['name'] ?>' name="notify[<?= $row['name'] ?>]" value=<?=$value?> />
					<?php elseif($row['type'] == 'BOOL'): ?>
						<input type="radio" id='id_input_<?= $row['name'] ?>_1' name="notify[<?= $row['name'] ?>]" value="1"  <?=empty($row['value'])?'':'checked';?>> <label for="id_input_<?= $row['name'] ?>_1"><?=$bool_option_yes?></label> 
						&nbsp; <input type="radio" id='id_input_<?= $row['name'] ?>_0' name="notify[<?= $row['name'] ?>]" value="0"  <?=!empty($row['value'])?'':'checked';?> > <label for="id_input_<?= $row['name'] ?>_0"><?=$bool_option_no?></label>
					<?php endif;//($row['type'] == 'BOOL'): ?>
					<?php if ($Lang['libms']["settings_remark"][$row['name']]!="") { ?>
							<p><span class='tabletextrequire'>*</span> <?=$Lang['libms']["settings_remark"][$row['name']]?></p>
					<?php } ?>
				</div>				

			</td>
		</tr>
<?php if (false && $name==$Lang['libms']["settings"]['system']['max_overdue_limit']) { ?>
	
		<tr>
			<td class="field_title" >
				<?=$Lang['libms']["settings"]['system']['overdue_epayment']?></td>
			<td>
				<div class='notify_viewer'>
					<?=$Lang['libms']["settings"]['system']['overdue_epayment_b']?>			</div>
				
				<div class='notify_editor' style='display:none'>
										<input type="radio" name="notify[global_payment_method]" value="0" id="global_payment_method_0" > <label for="global_lockout_0"><?=$Lang['libms']["settings"]['system']['overdue_epayment_a']?></label> 
						<input type="radio" name="notify[global_payment_method]" value="1" id="global_payment_method_1" checked > <label for="global_lockout_1"><?=$Lang['libms']["settings"]['system']['overdue_epayment_b']?></label>
									</div>				

			</td>
		</tr>
	<?php } ?>
		<?if($row['footer']){?>
		</table>
		<?}?>
		<?php }?>
		
	
	<div class="edit_bottom_v30">
		<div id='buttons_div' style='display: none'>
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit",false,'submit')?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();",'reset_btn')?>
		</div>
	</div>	

</td></tr>
</table>

</form>
</div>
<?php
echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

