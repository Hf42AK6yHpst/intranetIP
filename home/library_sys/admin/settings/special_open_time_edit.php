<?php
// using: 

/*
 * 	2017-07-17 (Henry)
 * 		- add validation of open time and close time [Case#C117663]
 * 
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();

/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $libms->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

$linterface = new interface_html();

//$Code = (is_array($Code)) ? $Code[0] : $Code;
$Code = implode(",", $Code);
$Code = mysql_real_escape_string($Code);
$RespondInfo = $libms->GET_SPECIALOPENTIME_INFO($Code);

//$groupSelection = $libms->getGroupSelection();

# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsSpecialOpenTime";

$TAGS_OBJ[] = array($Lang['libms']['settings']['PageSettingsSpecialOpenTime']);

$PAGE_NAVIGATION[] = array($Lang['libms']['settings']['PageSettingsSpecialOpenTime'], "special_open_time.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");

# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">

function checkForm(form1) {
	var filltext, valid_time, valid_date ;
	filltext = valid_time = valid_date = true;

	$('.hasDatepick').each(function(){
		if($(this).val() == "")	{
			alert("<?= $i_alert_pleasefillin.' '.$Lang["libms"]["S_OpenTime"]["Date"] ?>");	
			$(this).focus();
			valid_date = false;
			return false;
		};
		
		if (IsValidDate($(this).val()) == false){
			$(this).focus();
			valid_date = false;
			return false;
		}

		});

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	function checkdate(meme){
			meme.parents('table:first').find('.openhour').each(function(){
				if($(this).val() == "") {
					alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['S_OpenTime']['Time'] ?>");		
					$(this).focus();
					valid_time = false;
					return false;
				}else{
					if (IsValidTime($(this).val()) == false){
						$(this).focus();
						valid_time = false;
						return false;
					}
					else if($(this).hasClass('OpenTime')){
						if($(this).val() >= $(this).parent().parent().next().find('.CloseTime')[0].value){
							alert("<?=$Lang["libms"]["open_time"]["time_rangemsg"]?>");		
							$(this).focus();
							valid_time = false;
							return false;
						}
					}
				}
			});
		}	
	
	$('.checkopen').each(function(){	
		if(($(this).val() == "1" ) && valid_date && valid_time) {
			if (checkdate($(this)) == false)
				return false;
		}
	});

	return (valid_time && valid_date);
}

// Checks if time is in HH:MM:SS format.
function IsValidTime(timeStr) {
// Checks if time is in HH:MM:SS AM/PM format.
// The seconds and AM/PM are optional.

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?$/;

var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("<?= $Lang['libms']['open_time']['time_timemsg'] ?>");
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];

if (second=="") { second = null; }

if (hour < 0  || hour > 23) {
alert("<?= $Lang['libms']['open_time']['time_hourmsg'] ?>");
return false;
}
if (minute<0 || minute > 59) {
alert ("<?= $Lang['libms']['open_time']['time_minsmsg'] ?>");
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("<?= $Lang['libms']['open_time']['time_secmsg'] ?>");
return false;
}
return true;
}

function IsValidDate(dateStr) {
	var DatePat = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;

	var matchArray = dateStr.match(DatePat);
	if (matchArray == null) {
		alert("<?= $Lang['libms']['open_time']['datemsg'] ?>");
		return false;
	}
	year = matchArray[1];
	month = matchArray[2];
	day = matchArray[3];

	if (year < 1  || year > 2300) {
		alert("<?= $Lang['libms']['S_OpenTime']['yearmsg'] ?>");
		return false;
	}
	if (month<1 || month > 12) {
		alert ("<?= $Lang['libms']['S_OpenTime']['monthsmsg'] ?>");
		return false;
	}
	if (day < 1 || day > 31) {
		alert ("<?= $Lang['libms']['S_OpenTime']['daymsg'] ?>");
		return false;
	}
	if ((month==4 || month==6 || month==9 || month==11) && day==31) {
	alert("<?= $Lang['libms']['S_OpenTime']['daymsg'] ?>")
	return false;
	}
	
	if (month == 2) { // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day > 29 || (day==29 && !isleap)) {
			alert("February " + year + " doesn`t have " + day + " days!");
			return false;
		}
	}	
	
	return true;
}

function check_open(meme) {
	if((meme.val()) == 0){
		meme.parents('table:first').find('.OpenTime').val('');
		meme.parents('table:first').find('.OpenTime').attr("disabled", true);
		meme.parents('table:first').find('.OpenTime').parents('tr:first').hide();
		meme.parents('table:first').find('.CloseTime').val('');
		meme.parents('table:first').find('.CloseTime').attr("disabled", true);
		meme.parents('table:first').find('.CloseTime').parents('tr:first').hide();
	}else if((meme.val()) == 1){
		meme.parents('table:first').find('.OpenTime').attr("disabled", false); 	
		meme.parents('table:first').find('.OpenTime').parents('tr:first').show();
		meme.parents('table:first').find('.CloseTime').attr("disabled", false); 	
		meme.parents('table:first').find('.CloseTime').parents('tr:first').show();
	}
}

$().ready( function(){

	$('.checkopen').each(function () {
		check_open($(this));
	});
	
	$('.checkopen').change(function () {
		check_open($(this));
	});
});

</script>

<?
# option for open  ......
$CheckOpen[] = array("0", $Lang['libms']['open_time']['close']);
$CheckOpen[] = array("1", $Lang['libms']['open_time']['open']);
?>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" method="post" action="special_open_time_edit_update.php" onSubmit="return checkForm(this)">

<table width="90%" align="center" border="0">
<tr><td>

	<?php 
	$count = 0;
	foreach ($RespondInfo as $value){?>
	<input name="Code_<?=$count?>" type="hidden" class="textboxnum" value="<?=$value['OpenTimeSpecialID']?>" />
	<table class="form_table_v30">
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['DateFrom']?></td>
			<td><?=$linterface->GET_DATE_PICKER("DateFrom_".$count, $value['DateFrom'])?></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['DateEnd']?></td>
			<td><?=$linterface->GET_DATE_PICKER("DateEnd_".$count, $value['DateEnd'])?></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['open']." / ".$Lang['libms']['S_OpenTime']['close']?></td>
			<td><?=$linterface->GET_SELECTION_BOX($CheckOpen, " id ='checkopen' name='checkopen_{$count}' class='checkopen' ", '', $value['Open']);?></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['OpenTime']?></td>
			<td><input name="OpenTime_<?=$count?>" type="text" class="textboxnum OpenTime openhour" value="<?= $value['OpenTime'] ?>"></td>
		</tr>
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['S_OpenTime']['CloseTime']?></td>
			<td><input name="CloseTime_<?=$count?>" type="text" class="textboxnum CloseTime openhour" value="<?= $value['CloseTime'] ?>"></td>
		</tr>
		
		
	</table>
	<hr />
	
	<?php 
	$count++;
	}
	?>
	<input name="count" type="hidden" class="textboxnum" value="<?=$count?>" />
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='special_open_time.php'")?>
	</div>	

</td></tr>
</table>

</form><?php
echo $linterface->FOCUS_ON_LOAD("form1.ResponsibilityCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
