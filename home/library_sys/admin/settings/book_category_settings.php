<?php

# using by Henry

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();

//dump ($result);
$linterface = new interface_html();
# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsBookCategory";

$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
foreach ($result as $row){
	$book_category_name[] = htmlspecialchars($row['value']);
}

$TAGS_OBJ[] = array($book_category_name[0]?$book_category_name[0]:$Lang['libms']['settings']['book_category']." 1",'book_category.php', false);
$TAGS_OBJ[] = array($book_category_name[1]?$book_category_name[1]:$Lang['libms']['settings']['book_category']." 2",'book_category2.php', false);
$TAGS_OBJ[] = array($Lang["libms"]["settings"]["book_category_type"],'#', true);

# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();

?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
<script language="javascript">
<!--

function EditInfo()
{
	$('#EditBtn').hide(); 
	$('.notify_viewer').hide();
	$('.notify_editor').show();
	$('#buttons_div').show();

}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').show();
	$('.notify_viewer').show();
	$('.notify_editor').hide();
	
	$('#buttons_div').hide();
}

function update_FalseOrInt(){
	$this=$(this);
	$input = $this.next('input.in_FalseOrInt');
	if( $this.is(':checked') ) {
		if ($input.val() <= 0 || $input.val() == '' ){
			$input.val(0);
		}
		$input.show();
	}else{
		$input.val(-1);
		$input.hide();
	}
}


//ready
$(function (){
	$('#submit').addClass('submit');
	$('.chk_FalseOrInt')
	.change( update_FalseOrInt)
	.each( update_FalseOrInt);
	$('#form1').validate();

});


//-->
</script>
<style type="text/css">
label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }
</style>
<div style=' width: 10% ; margin-left: auto ; margin-right: auto ;'><?= $linterface->GET_SYS_MSG($xmsg) ?></div>
	<div class="table_row_tool row_content_tool" >
		<input type="button" class="formsmallbutton " onclick="javascript:EditInfo();" name="EditBtn" id="EditBtn" value="<?=$Lang["libms"]["general"]["edit"]?>">
	</div>
	<div>
<form id='form1' name="form1" method="post" action="book_category_settings_update.php" >
<table width="90%" align="center" border="0">
<tr><td>
	<table class="form_table_v30">
		<?php foreach ($result as $row){
			if($row['name'] != 'book_category_type_1' && $row['name'] != 'book_category_type_2')
				continue;
			$bool_option_no = ($Lang['libms']["settings"]['system'][$row['name']."_no"]!="") ? $Lang['libms']["settings"]['system'][$row['name']."_no"] : $Lang['libms']["settings"]['system']['no'];
			$bool_option_yes = ($Lang['libms']["settings"]['system'][$row['name']."_yes"]!="") ? $Lang['libms']["settings"]['system'][$row['name']."_yes"] : $Lang['libms']["settings"]['system']['yes'];
			
			$name = $Lang["libms"]["settings"]["book_category_name"]." (".$row['id'].")";
			 
			$value = htmlspecialchars($row['value']);
			if($row['type'] == 'BOOL'){
				$value = empty($value)?$bool_option_no:$bool_option_yes;
			}
		?>
		<tr>
			<td class="field_title" >
				<?= $name ?>
			</td>
			<td>
				<div class='notify_viewer'>
					<?=$value?>
				</div>
				
				<div class='notify_editor' style='display:none'>
					<?php if($row['type'] == 'STRING'): ?>
						<input id='id_input_<?= $row['name'] ?>' name="notify[<?= $row['name'] ?>]" value="<?=$value?>" />
					<?php elseif($row['type'] == 'INT'): ?>
						<input type='number' id='id_input_<?= $row['name'] ?>' name="notify[<?= $row['name'] ?>]" value="<?=$value?>" class='required number' />
					<?php elseif($row['type'] == 'FALSEORINT'): ?>	
						
						<input type='checkbox' class='chk_FalseOrInt' id='id_input_<?= $row['name'] ?>_chk' name="input_<?= $row['name'] ?>_chk" <?=($value>=0)?'checked':''?> />
						&nbsp; <input type='number'  class='in_FalseOrInt required number' min='-1' max='999' id='id_input_<?= $row['name'] ?>' name="notify[<?= $row['name'] ?>]" value=<?=$value?> />
					<?php elseif($row['type'] == 'BOOL'): ?>
						<input type="radio" id='id_input_<?= $row['name'] ?>_1' name="notify[<?= $row['name'] ?>]" value="1"  <?=empty($row['value'])?'':'checked';?>> <label for="id_input_<?= $row['name'] ?>_1"><?=$bool_option_yes?></label> 
						&nbsp; <input type="radio" id='id_input_<?= $row['name'] ?>_0' name="notify[<?= $row['name'] ?>]" value="0"  <?=!empty($row['value'])?'':'checked';?> > <label for="id_input_<?= $row['name'] ?>_0"><?=$bool_option_no?></label>
					<?php endif;//($row['type'] == 'BOOL'): ?>
					<?php if ($Lang['libms']["settings_remark"][$row['name']]!="") { ?>
							<p><span class='tabletextrequire'>*</span> <?=$Lang['libms']["settings_remark"][$row['name']]?></p>
					<?php } ?>
				</div>				

			</td>
		</tr>
<?php if (false && $name==$Lang['libms']["settings"]['system']['max_overdue_limit']) { ?>
	
		<tr>
			<td class="field_title" >
				<?=$Lang['libms']["settings"]['system']['overdue_epayment']?></td>
			<td>
				<div class='notify_viewer'>
					<?=$Lang['libms']["settings"]['system']['overdue_epayment_b']?>			</div>
				
				<div class='notify_editor' style='display:none'>
										<input type="radio" name="notify[global_payment_method]" value="0" id="global_payment_method_0" > <label for="global_lockout_0"><?=$Lang['libms']["settings"]['system']['overdue_epayment_a']?></label> 
						<input type="radio" name="notify[global_payment_method]" value="1" id="global_payment_method_1" checked > <label for="global_lockout_1"><?=$Lang['libms']["settings"]['system']['overdue_epayment_b']?></label>
									</div>				

			</td>
		</tr>
	<?php } ?>
		<?php }?>
		</table>
	
	<div class="edit_bottom_v30">
		<div id='buttons_div' style='display: none'>
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit",false,'submit')?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();",'reset_btn')?>
		</div>
	</div>	

</td></tr>
</table>

</form>
</div>
<?php
echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

