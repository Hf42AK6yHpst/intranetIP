<?php

# using by  
/*
 * 	2016-11-17 [Cameron]
 * 	- show push message setting only when $plugin['eClassApp'] = true
 * 
 * 	2016-09-29 [Cameron]
 * 	- show return message after save record
 * 
 * 	2016-09-02 [Cameron]
 * 	- fix bug: should allow to save form if "Notify user by email before due date" is 'No' and number of days is empty
 * 		restrict to input non-negative integer only, same rule applied to "Notify user by push message before due date"
 * 		[case ref: #R102098] 
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

//$libms = new liblms();
//
//$result = $libms->SELECTFROMTABLE('LIBMS_NOTIFY_SETTING', '*', '','',1,'name','desc');
////dump ($result);
//$result2 = $libms->SELECTFROMTABLE('LIBMS_SYSTEM_SETTING', '*', '','',1); // for Day(s) of the Email Reminder before Due Date (0 means not remind) 
////debug_pr($result);
////debug_pr($result2);
$linterface = new interface_html();
# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsNotification";

$TAGS_OBJ[] = array($Lang['libms']["settings"]['notification']['title']);
# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$_GET['xmsg']]);

########################################
### Start: construct table content #####
########################################
########################################
## Start: EMAIL          ###############
########################################
$libms = new liblms();
$result = $libms->SELECTFROMTABLE('LIBMS_NOTIFY_SETTING', '*', '','',1,'name','desc');
$result = BuildMultiKeyAssoc($result, array('name'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$numOfRow = count($titleArray);
		$x .=$linterface->GET_NAVIGATION2($Lang['General']['Email']);
		
		$key = 'reserve_email';
		$content= ($result[$key]['enable'])?$Lang['libms']["settings"]['notification']['yes']:$Lang['libms']["settings"]['notification']['no'];
		$yesIsChecked='';
		$noIsChecked='';
		if($result[$key]['enable']){
			$yesIsChecked = 'checked';
		}else{
			$noIsChecked = 'checked';
		}
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['libms']["settings"]['notification'][$key].'</td>'."\r\n";
			$x .= '<td class="showTable">'.$content.'</td>'."\r\n";
			$x .= '<td class="editTable"> <input type="radio" name="notify['.$key.']" value="1" '.$yesIsChecked.' onclick="onClickRadio(this);"><label>'.$Lang['libms']["settings"]['notification']['yes'].'</label>
					<input type="radio" name="notify['.$key.']" value="0" '.$noIsChecked.' onclick="onClickRadio(this);" ><label>'.$Lang['libms']["settings"]['notification']['no'].'</label> </td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$key = 'overdue_email';
		$content= ($result[$key]['enable'])?$Lang['libms']["settings"]['notification']['yes']:$Lang['libms']["settings"]['notification']['no'];
		$yesIsChecked='';
		$noIsChecked='';
		if($result[$key]['enable']){
			$yesIsChecked = 'checked';
		}else{
			$noIsChecked = 'checked';
		}
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['libms']["settings"]['notification'][$key].'</td>'."\r\n";
			$x .= '<td class="showTable">'.$content.'</td>'."\r\n";
			$x .= '<td class="editTable"> <input type="radio" name="notify['.$key.']" value="1" '.$yesIsChecked.' onclick="onClickRadio(this);"><label>'.$Lang['libms']["settings"]['notification']['yes'].'</label>
					<input type="radio" name="notify['.$key.']" value="0" '.$noIsChecked.' onclick="onClickRadio(this);" ><label>'.$Lang['libms']["settings"]['notification']['no'].'</label> </td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		$key = 'due_date_reminder_email';
		$content= ($result[$key]['enable'])?$Lang['libms']["settings"]['notification']['yes']:$Lang['libms']["settings"]['notification']['no'];
		$yesIsChecked='';
		$noIsChecked='';
		if($result[$key]['enable']){
			$yesIsChecked = 'checked';
		}else{
			$noIsChecked = 'checked';
		}	
			//second layer table
			$secondLayerTitle = 'due_date_reminder';
			$condition = array('name'=>'"'.$secondLayerTitle.'"');
			$result2 = $libms->SELECTFROMTABLE('LIBMS_SYSTEM_SETTING', '*', $condition,'',1); // for Day(s) of the Email Reminder before Due Date (0 means not remind) 
			
			$titleSecondLayer=$Lang['libms']["settings"]['system'][$result2[0]['name']];
			$contentSecondLayer= $result2[0]['value'];
			$y .= '<div>'."\r\n";
				$y .= '<table class="common_table_list_v30"}>'."\r\n";
					$y .= '<tr>'."\r\n";
						$y .= '<td class="rights_not_select_sub"  style="width:30%">'.$titleSecondLayer.'</td>'."\r\n";
						$y .= '<td>'.$contentSecondLayer.'</td>'."\r\n";
					$y .= '</tr>'."\r\n";
				$y .= '</table>'."\r\n";
			$y .= '</div>'."\r\n";
		$content.=$y ;
		
			//Editable second layer 
			$y = '<div>'."\r\n";
				$y .= '<table class="common_table_list_v30"}>'."\r\n";
					$y .= '<tr>'."\r\n";
						$y .= '<td class="rights_not_select_sub"  style="width:30%">'.$titleSecondLayer.'</td>'."\r\n";
						$y .= '<td>'.'<input type="number" name="notify2['.$secondLayerTitle.']" id="'.$secondLayerTitle.'" value="'.$contentSecondLayer.'">'.'</td>'."\r\n";
					$y .= '</tr>'."\r\n";
				$y .= '</table>'."\r\n";
			$y .= '</div>'."\r\n";
		$editableContent = $y ;
		
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['libms']["settings"]['notification'][$key].'</td>'."\r\n";
			$x .= '<td class="showTable">'.$content.'</td>'."\r\n";
			$x .= '<td class="editTable"> <input type="radio" name="notify['.$key.']" value="1" '.$yesIsChecked.' onclick="onClickRadio(this);"><label>'.$Lang['libms']["settings"]['notification']['yes'].'</label>
					<input type="radio" name="notify['.$key.']" value="0" '.$noIsChecked.' onclick="onClickRadio(this);" ><label>'.$Lang['libms']["settings"]['notification']['no'].'</label> '.$editableContent.' </td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
########################################
## End  : EMAIL          ###############
########################################

########################################
## Start: PUSH MESSAGE   ###############
########################################

if ($plugin['eClassApp']) {
	$x .= '<table class="form_table_v30">'."\r\n";
		$x .= '<tbody>'."\r\n";
			$numOfRow = count($titleArray);
			$x .=$linterface->GET_NAVIGATION2($Lang['General']['PushMessage']);
			
			$key = 'push_message_overdue';
			$content= ($result[$key]['enable'])?$Lang['libms']["settings"]['notification']['yes']:$Lang['libms']["settings"]['notification']['no'];
			$yesIsChecked='';
			$noIsChecked='';
			if($result[$key]['enable']){
				$yesIsChecked = 'checked';
			}else{
				$noIsChecked = 'checked';
			}
			$x .= '<tr>'."\r\n";
				$x .= '<td class="field_title">'.$Lang['libms']["settings"]['notification'][$key].'</td>'."\r\n";
				$x .= '<td class="showTable">'.$content.'</td>'."\r\n";
				$x .= '<td class="editTable"> <input type="radio" name="notify['.$key.']" value="1" '.$yesIsChecked.' onclick="onClickRadio(this);"><label>'.$Lang['libms']["settings"]['notification']['yes'].'</label>
						<input type="radio" name="notify['.$key.']" value="0" '.$noIsChecked.' onclick="onClickRadio(this);" ><label>'.$Lang['libms']["settings"]['notification']['no'].'</label> </td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
		
			$key = 'push_message_due_date';
			$content= ($result[$key]['enable'])?$Lang['libms']["settings"]['notification']['yes']:$Lang['libms']["settings"]['notification']['no'];
			$yesIsChecked='';
			$noIsChecked='';
			if($result[$key]['enable']){
				$yesIsChecked = 'checked';
			}else{
				$noIsChecked = 'checked';
			}	
			//second layer table
			$secondLayerTitle = 'push_message_due_date_reminder';
			$condition = array('name'=>'"'.$secondLayerTitle.'"');
			$result2 = $libms->SELECTFROMTABLE('LIBMS_SYSTEM_SETTING', '*', $condition,'',1); // for Day(s) of the Email Reminder before Due Date (0 means not remind) 
			
			$titleSecondLayer=$Lang['libms']["settings"]['system'][$result2[0]['name']];
			$contentSecondLayer= $result2[0]['value'];
			$y ='';
			$y .= '<div>'."\r\n";
					$y .= '<table class="common_table_list_v30"}>'."\r\n";
					$y .= '<tr>'."\r\n";
						$y .= '<td class="rights_not_select_sub"  style="width:30%">'.$titleSecondLayer.'</td>'."\r\n";
						$y .= '<td> '.$contentSecondLayer.'</td>'."\r\n";
					$y .= '</tr>'."\r\n";
				$y .= '</table>'."\r\n";
			$y .= '</div>'."\r\n";
			$content.=$y ;
			
			//Editable second layer 
				$y = '<div>'."\r\n";
					$y .= '<table class="common_table_list_v30"}>'."\r\n";
						$y .= '<tr>'."\r\n";
							$y .= '<td class="rights_not_select_sub"  style="width:30%">'.$titleSecondLayer.'</td>'."\r\n";
							$y .= '<td>'.'<input type="number" name="notify2['.$secondLayerTitle.']" id="'.$secondLayerTitle.'" value="'.$contentSecondLayer.'">'.'</td>'."\r\n";
						$y .= '</tr>'."\r\n";
					$y .= '</table>'."\r\n";
				$y .= '</div>'."\r\n";
			$editableContent = $y ;
			
			$x .= '<tr>'."\r\n";
				$x .= '<td class="field_title">'.$titleArray[] =  $Lang['libms']["settings"]['notification'][$key].'</td>'."\r\n";
				$x .= '<td class="showTable">'.$content.'</td>'."\r\n";
				$x .= '<td class="editTable"> <input type="radio" name="notify['.$key.']" value="1" '.$yesIsChecked.' onclick="onClickRadio(this);"><label>'.$Lang['libms']["settings"]['notification']['yes'].'</label>
						<input type="radio" name="notify['.$key.']" value="0" '.$noIsChecked.' onclick="onClickRadio(this);" ><label>'.$Lang['libms']["settings"]['notification']['no'].'</label> '.$editableContent.' </td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			
			$key = 'push_message_time';
			$condition = array('name'=>'"'.$key.'"');
			$result2 = $libms->SELECTFROMTABLE('LIBMS_SYSTEM_SETTING', '*', $condition,'',1); // for Day(s) of the Email Reminder before Due Date (0 means not remind) 
			$content= $result2[0]['value'];
			$title = $Lang['libms']["settings"]['system'][$result2[0]['name']];
			$x .= '<tr>'."\r\n";
				$x .= '<td class="field_title">'.$title.'</td>'."\r\n";
				$x .= '<td class="showTable">'.$content.'</td>'."\r\n";
				$x .= '<td class="editTable"> ';
					
					#### Time selection boxes
					$time = explode(":",$content);
						$y = $linterface->Get_Time_Selection_Box('hour', 'hour', $time[0]);
						$y .= " : ";
						$y .= $linterface->Get_Time_Selection_Box('minute', 'min', $time[1], $others_tab='', $interval=15);
					$x .= $y.' <input type="hidden" name="notify2['.$key.']" id="'.$key.'" value="'.$content.'">';
					
					
				$x .= ' </td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			
			
		$x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
}
########################################
## END   :PUSH MESSAGE   ###############
########################################
########################################
### END:   construct table content #####
########################################
$htmlAry['formTable'] =$x;


### buttons
$htmlAry['editBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['resetBtn'] = $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();",'resetBtn');



?>
<style>
.editTable{
	display:none;
}
</style>
<script language="javascript">
<!--
$( document ).ready(function() {
  $('#submitBtn').hide();
  $('#resetBtn').hide();
});

function goEdit(){
	$('.editTable').show();
	$('.showTable').hide();
	$('#editBtn').hide();
	$('#submitBtn').show();
	$('#resetBtn').show();
	<?php 
	if(!$result['due_date_reminder_email']['enable']){
	?>
	$('#due_date_reminder').attr('disabled','disabled');
	<?php 
	}
	?>
	
	<?php 
	if(!$result['push_message_due_date']['enable']){
	?>
	$('#push_message_due_date_reminder').attr('disabled','disabled');
	<?php 
	}
	?>
	
}
function goSubmit(){
	var hour = $('#hour :selected').val();
	var min = $('#minute :selected').text();
	var timeString = hour+':'+min;
	
	$('#push_message_time').val(timeString);
	
	//formCheck
	var due_date_reminder = $('#due_date_reminder').val();
	
	if (($("input:radio[name='notify\[due_date_reminder_email\]']:checked").val() == 1) && ((due_date_reminder =='') || (due_date_reminder < 0) || (due_date_reminder != parseInt(due_date_reminder)))) {
		alert("<?=$Lang['libms']["settings"]['system']['notification_input_non_negative_integer']?>");
		$('#due_date_reminder').focus();
		return false;
	}
	
	var push_message_due_date_reminder = $('#push_message_due_date_reminder').val();
	
	if (($("input:radio[name='notify\[push_message_due_date\]']:checked").val() == 1) && ((push_message_due_date_reminder =='') || (push_message_due_date_reminder < 0) || (push_message_due_date_reminder != parseInt(push_message_due_date_reminder)))) {
		alert("<?=$Lang['libms']["settings"]['system']['notification_input_non_negative_integer']?>");
		$('#push_message_due_date_reminder').focus();
		return false;
	}
	
	$('#form1').submit();
}
function onClickRadio(obj){
	var name = obj.getAttribute("name");
	if(name=='notify[due_date_reminder_email]'){
		if(obj.value==1){
			$('#due_date_reminder').removeAttr('disabled');
		}else{
			$('#due_date_reminder').attr('disabled','disabled');
		}
	}else if(name=='notify[push_message_due_date]'){
		if(obj.value==1){
			$('#push_message_due_date_reminder').removeAttr('disabled');
		}else{
			$('#push_message_due_date_reminder').attr('disabled','disabled');
		}
	}
}
function ResetInfo()
{
	document.form1.reset();
	$('.editTable').hide();
	$('.showTable').show();
	$('#submitBtn').hide();
    $('#resetBtn').hide();
    $('#editBtn').show();
}
//-->
</script>
<form name="form1" id="form1" method="post" action="notification_update.php">
	<div id="show_div">
	
		<div class="table_board">
			<?=$htmlAry['formTable']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
			
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['editBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<p class="spacer"></p>
		</div>
	</div>

</form>
<?php
echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

