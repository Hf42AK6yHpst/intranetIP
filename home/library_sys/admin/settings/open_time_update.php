<?php


# modifying by: 


/*************************************************************
 *  20130624 (Yuen)
 * 			added start and end dates
 *
 * ************************************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();

/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/
if($Code=="") {
	header("Location: open_time.php");	
}
 
for ($i=0; $i < 7; $i++)
	{
	$OpenTimeID = $Code[$i];
	$dataAry['OpenTime'] = (($opentime[$i]=="")?'NULL':$opentime[$i]);
	$dataAry['CloseTime'] = (($closetime[$i]=="")?'NULL':$closetime[$i]);
	$dataAry['Open'] = (($checkopen[$i]=="1")?1:0);
	
	$result = $libms->UPDATE_OPENTIME($OpenTimeID, $dataAry);
	unset($dataAry);
 
	}

# system start and end dates
$system_settings_period_arr["system_open_date"] = $SystemDateFrom;
$system_settings_period_arr["system_end_date"] = $SystemDateEnd;

foreach( $system_settings_period_arr as $key => $value)
{
		$sanitized_value = PHPToSQL($value);
		$sanitized_name = PHPToSQL($key);
		$SQL = "UPDATE `LIBMS_SYSTEM_SETTING` SET `value` = {$sanitized_value} WHERE `name` = {$sanitized_name} ";
		$libms->db_db_query($SQL);
}


intranet_closedb();

header("Location: open_time.php?xmsg=update");

?>
