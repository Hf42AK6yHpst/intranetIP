<?php

// using: Henry 

/************************************
 * 
 * Date:	2014-06-06 (Henry)
 * Details: change wordings
 * Version: ip.2.5.5.7.1
 * 
 ************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();
$linterface = new interface_html();

# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsStockTake";

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take']);

$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['stock-take'], "stock_take.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['new'], "");

# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();


# Get Data
$stocktakeSchemeID = $_POST['StocktakeSchemeID'][0]?$_POST['StocktakeSchemeID'][0]:$_GET['StocktakeSchemeID'][0];

if($stocktakeSchemeID!=''){
	
	$stockTakeSchemeSql = $libms->GET_STOCKTAKE_SCHEME_SQL($stocktakeSchemeID);
	$stockTakeSchemeDataArr = $libms->returnArray($stockTakeSchemeSql);
	
	$thisSchemeTitle = $stockTakeSchemeDataArr[0]['SchemeTitle'];
	$thisEndDate = $stockTakeSchemeDataArr[0]['EndDate'];
	$thisStartDate = $stockTakeSchemeDataArr[0]['StartDate'];
	$thisRecordEndDate = $stockTakeSchemeDataArr[0]['RecordEndDate'];
	$thisRecordStartDate = $stockTakeSchemeDataArr[0]['RecordStartDate'];
}


//# FCK editor
//$objHtmlEditor = new FCKeditor ( 'description' , "100%", "320", "", "Basic2", $description);
//$descriptionFCKDisplay= $objHtmlEditor->Create2();
			
# Start Date
if($thisStartDate!=''){
	$startDate = $thisStartDate;
}else{
	$startDate = date("Y-m-d");
}

# End Date
if($thisEndDate!=''){ 				
	$endDate = $thisEndDate;				
}else{
	$endDate = date("Y-m-d",time() + 86400);
}

# Record Start Date
if($thisRecordStartDate!=''){
	$recordsStartDate = $thisRecordStartDate;
}else{
	$recordsStartDate = date("Y-m-d");
}

# Record End Date
if($thisRecordEndDate!=''){ 				
	$recordsEndDate = $thisRecordEndDate;				
}else{
	$recordsEndDate = date("Y-m-d",time() + 86400);
}								
?>
<script language="javascript">

function checkForm(form1) {
	var schemeTitle = $('#schemeTitle').val();
	if(schemeTitle=='')
	{
		alert("<?=$Lang['libms']['settings']['stocktake']['inputname']?>");
		$('#schemeTitle').focus();
		return false;
		
	} else if($('#startDate').val() > $('#endDate').val())
	{
		alert("<?=$Lang['General']['WrongDateLogic']?>");
		$('#startDate').focus();
		return false;
	} else
	{
		return true;
	}
}

function submitCallback(json){
	if (json.success && json.unique){
		document.form1.submit();
	}else{
		$('#in_LocationCode').focus().css('background-color','yellow');
		alert('<?=$Lang['libms']['settings']['msg']['duplicated']?>');
	}
}



</script>


<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form id='form1' name="form1" method="post" action="stock_take_scheme_new_update.php" onSubmit="return checkForm(this)">

<table width="90%" align="center" border="0">
<tr><td>

	<table class="form_table_v30">
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['settings']['stocktake']['name']?></td>
			<td><?=$linterface->GET_TEXTBOX('schemeTitle', 'schemeTitle', $thisSchemeTitle, 'type="text" class="textboxtext"'); ?></td>
		</tr>
	<!--
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang["libms"]["book"]["introduction"]?></td>
			<td><?=$descriptionFCKDisplay?></td>
		</tr>
	!-->
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['settings']['stocktake']['stocktakeDate'].'<br/> ('.$Lang['libms']['settings']['stocktake']['stocktakeDateRemarks'].')'?></td>
			<td><?=$linterface->GET_DATE_PICKER("startDate", $startDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span> ~ 
			<?=$linterface->GET_DATE_PICKER("endDate", $endDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
		</tr>
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['settings']['stocktake']['recordStartDate'].'<br/> ('.$Lang['libms']['settings']['stocktake']['recordStartDateRemarks'].')'?></td>
			<td><?=$linterface->GET_DATE_PICKER("recordsStartDate", $recordsStartDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
		</tr>
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['settings']['stocktake']['recordEndDate'].'<br/> ('.$Lang['libms']['settings']['stocktake']['recordEndDateRemarks'].')'?></td>
			<td><?=$linterface->GET_DATE_PICKER("recordsEndDate", $recordsEndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
		</tr>
	</table>
				<input type='hidden' name='StocktakeSchemeID' value='<?=$stocktakeSchemeID?>'>
				
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
		<?// $linterface->GET_ACTION_BTN($button_reset, "reset");?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='stock_take.php'")?>
	</div>	

</td></tr>
</table>

</form><?php
echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
