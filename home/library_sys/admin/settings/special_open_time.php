<?php
/*
 * 	2017-06-07 (Cameron)
 * 		- set default form action so that navigation after action (e.g. remove) won't show previous action result
 * 
 * 	2017-06-06 (Cameron)
 * 		- place set cookie function after include global.php to support php5.4 [case #E117788]
 * 
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}




$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsSpecialOpenTime";

$libms->MODULE_AUTHENTICATION($CurrentPage);

//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;
$sql = "SELECT 
	    `DateFrom`, `DateEnd`, case `Open` when '1' then '".$Lang["libms"]["S_OpenTime"]["open"]."' when '0' then '".$Lang["libms"]["S_OpenTime"]["close"]."' END ,`OpenTime`, `CloseTime`,
            CONCAT('<input type=\'checkbox\' name=\'Code[]\' class=\'Code\' value=\'', OpenTimeSpecialID ,'\'>')
		FROM 
		    `LIBMS_OPEN_TIME_SPECIAL`
		WHERE
            1=1
        ";
$li = new libdbtable2007($field, $order, $pageNo);

function checkopen($x){
	if($x == '1')
		return $Lang["libms"]["S_OpenTime"]["open"];
	else
		return $Lang["libms"]["S_OpenTime"]["close"];
}

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";
$li->field_array = array("DateFrom", "DateEnd", "Open" ,"OpenTime","CloseTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='24%' >".$li->column($pos++, $Lang['libms']['S_OpenTime']['DateFrom'])."</td>\n";
$li->column_list .= "<th width='24%' >".$li->column($pos++, $Lang['libms']['S_OpenTime']['DateEnd'])."</td>\n";
$li->column_list .= "<th width='24%' >".$li->column($pos++, $Lang["libms"]["S_OpenTime"]["close"].'/'.$Lang["libms"]["S_OpenTime"]["open"])."</td>\n";
$li->column_list .= "<th width='24%' >".$li->column($pos++, $Lang['libms']['S_OpenTime']['OpenTime'])."</td>\n";
$li->column_list .= "<th width='24%' >".$li->column($pos++, $Lang['libms']['S_OpenTime']['CloseTime'])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("Code[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'special_open_time_new.php')",$button_new,"","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("special_open_time_import/import_csv_data.php",$button_import,"","","",0);
//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['settings']['PageSettingsSpecialOpenTime']);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();



?>
<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
//-->
$(document).ready(function(){
	$('#editall').click(function(){
		if(($('.Code:checked').length)==0)
			 alert(globalAlertMsg2);
		else{
			form1.action="special_open_time_edit.php";
			document.form1.submit();
		}
	});
});



</script>
<form name="form1" method="post" action="special_open_time.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="table-action-bar">
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="#" id="editall" class="tabletool" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$Lang["libms"]["S_OpenTime"]["editall"] ?></a></td>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'Code[]','special_open_time_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'Code[]','special_open_time_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				<!--
				<tr>
					<td><?=$Lang["libms"]["S_OpenTime"]["remark"]?></td>
				</tr>
				-->
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();


intranet_closedb();


?>
