<?php
/*
 * 	Modifying: 
 * 	Log
 * 
 * 	Date: 2015-10-20 [Cameron]
 * 		- add function getGroupMembersView()
 * 	
 * 
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");


require_once('ReportView.php');

global $Lang;

class ajaxView extends ReportView{
	public function __construct(){
		$CurrentPageArr['LIBMS'] = 1;
		$CurrentPage = "PAGEReportAjax";
		parent::__construct($CurrentPage);
	}
	
	public function getView($viewdata = null){
		global $Lang;
		if (empty($viewdata)){
			return;
		}	
		$function = $viewdata['function'];
		if (empty($function)){
			return;
		}
		switch($function){
			case 'getGroupName':
				return $this->getGroupNameView($viewdata);
				break;
			case 'getGroupMembers':
				return $this->getGroupMembersView($viewdata);
				break;
			case 'getClassLevel':
				return $this->getClassLevelView($viewdata);
				break;
			case 'getClassNameByLevel':
				return $this->getClassNameByLevelView($viewdata);
				break;
			case 'getStudentByClass':
				return $this->getStudentByClassView($viewdata);
				break;
			default:
				return;
		}
	}
	
	protected function getGroupNameView($viewdata = null){
		global $Lang;
		if (empty($viewdata)){
			return;
		}
		$x = '';
		foreach($viewdata['result'] as $key => $name){
			$x .= "<option value='$key'>$name</option>\n";
		}
		//var_dump($viewdata['result']);
		return $x;
	}
	
	protected function getClassLevelView($viewdata = null){
		global $Lang;
		if (empty($viewdata)){
			return;
		}
		$x = '';
		foreach($viewdata['result'] as $key => $name){
			if (trim($key)!="" && trim($name)!="")
			{
				$x .= "<option value='$key'>$name</option>\n";
			}
		}
		return $x;
	}
	
	protected function getClassNameByLevelView($viewdata = null){
		global $Lang;
		if (empty($viewdata)){
			return;
		}
		$x = '';
		foreach($viewdata['result'] as $key => $name){
			if (trim($key)!="" && trim($name)!="")
			{
				$x .= "<option value='$key'>$name</option>\n";
			}
		}
		return $x;
	}
	
	protected function getStudentByClassView($viewdata = null){
		global $Lang;
		if (empty($viewdata)){
			return;
		}
		$x = '';
		foreach($viewdata['result'] as $key => $display_name){
			if (trim($key)!="" && trim($display_name)!="")
			{
				$x .= "<option value='$key'>$display_name</option>\n";
			}
		}
		return $x;
	}	
	
	protected function getGroupMembersView($viewdata = null){
		global $Lang;
		if (empty($viewdata)){
			return;
		}
		$x = '';
		foreach($viewdata['result'] as $key => $display_name){
			if (trim($key)!="" && trim($display_name)!="")
			{
				$x .= "<option value='$key'>$display_name</option>\n";
			}
		}
		return $x;
	}	
	
}