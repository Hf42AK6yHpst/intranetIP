<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once('ajaxView.php');
include_once('Reports.php');

intranet_auth();
intranet_opendb();

$libms = new liblms();

function getGroupName(){
    $reports = new Reports();
	$veiw_ary['function'] = $_REQUEST['function'];
	$veiw_ary['result']= $reports->getGroupNameArray();	
	$view = new ajaxView();
	return $view->getView($veiw_ary);	
}
	
function getClassLevel(){
	$reports = new Reports();
	$veiw_ary['function'] = $_REQUEST['function'];
	$veiw_ary['result']= $reports->getClassLevelArray();
	$view = new ajaxView();
	return $view->getView($veiw_ary);	
}	
	
function getClassNameByLevel(){
	$reports = new Reports();
	$veiw_ary['function'] = $_REQUEST['function'];		
	$veiw_ary['result']= $reports->getClassNameArrayByClassLevel();
	$view = new ajaxView();	
	return $view->getView($veiw_ary);
}
	

function getStudentByClass(){
	$reports = new Reports();
	$veiw_ary['function'] = $_REQUEST['function'];
	$veiw_ary['result']= $reports->getStudentArrayByClassName($_REQUEST['classNames']);
	$view = new ajaxView();
	return $view->getView($veiw_ary);	
		
}	
	
if (empty($function)){
	exit;
}
		
$x ='';
switch($function){
	case 'getGroupName':
 		$x = getGroupName();
 		break;
 	case 'getClassLevel':
 		$x = getClassLevel();
 		break;
  	case 'getClassNameByLevel':
 		$x = getClassNameByLevel();
 		break;
 	case 'getStudentByClass':
 		$x = getStudentByClass();
 		break; 				
	default:
		exit;
}
if (empty($x))
	exit;
		
echo $x;
?>