/**
 * 
 */


Number.prototype.roundTo = function(nTo) {
    nTo = nTo || 10;
    return Math.round(this * (1 / nTo) ) * nTo;
}
//console.log("roundto ", (925.50).roundTo(100));

Number.prototype.ceilTo = function(nTo) {
    nTo = nTo || 10;
    return Math.ceil(this * (1 / nTo) ) * nTo;
}
//console.log("ceilTo ", (925.50).ceilTo(100));

Number.prototype.floorTo = function(nTo) {
    nTo = nTo || 10;
    return Math.floor(this * (1 / nTo) ) * nTo;
}
//console.log("floorTo ", (925.50).floorTo(100));