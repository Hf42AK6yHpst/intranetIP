<?php

//	Using:   

/***************************************
 *
 * Date:    2018-04-20 (Cameron)
 *          - retrieve AccountDate(CreationDate) [case #W120117]
 * 
 * Date:	2016-06-15 (Cameron)
 * 			- Set "Back" button to the value in lang file
 *
 * Date:	2016-05-05 (Cameron)
 * 			- add column "Call Number", show Total number of write-off record (case #E95394)
 *  
 * Date:	2015-07-21 (Cameron)
 * 			- Add radio option "All" to BookCategory
 * 			- show selected BookCategory based on BookCategoryType
 * 			- Add order by BookCategoryCode in retrieving BookCategory option list to compatible with other reports
 *
 * Date:	2015-04-27 (Cameron)
 * Details:	Show Date Of Purchase for a book in the result list. (case ref: 2015-0421-0956-43206)
 * 
 * Date:	2015-01-23 (Henry)
 * Details:	Added Category2 filter
 * 
 * Date:	2013-11-27 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "&nbsp;";

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['write-off report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["Right"]["reports"]["write-off report"]);
$CurrentPage = "PageReportingWriteoffReport";
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

## Obtain POST Value
//$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
//$selectedStocktakeStatus = $_POST['selectedStocktakeStatus'];
$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$resourcesTypeCode = $_POST['selectedResourcesTypeCode'];
$selectedCirculationType = $_POST['selectedCirculationType'];
$selectedLocation = $_POST['selectedLocation'];
$selectedBookCategory = $_POST['selectedBookCategory'];

if($StartDate=='' || $EndDate==''){
	header('Location: index.php');	
}


$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;


############################### Resources Type Start ###############################
$resourceTypeArr = $libms->GET_RESOURCES_TYPE_INFO();	
$numOfResourceTypeArr = count($resourceTypeArr);	
$thisResourcesTypeSelectionArr = array();
$thisResourcesTypeSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfResourceTypeArr;$i++){
	$thisResourcesTypeCode = $resourceTypeArr[$i]['ResourcesTypeCode'];
	$thisDescriptionEn = $resourceTypeArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $resourceTypeArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisResourcesTypeSelectionArr[]= array($thisResourcesTypeCode,$thisDescription);				
}	
$resourcesTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisResourcesTypeSelectionArr, 'name="selectedResourcesTypeCode[]" id="ResourcesTypeCode" size="10" multiple', '', is_array($resourcesTypeCode)?$resourcesTypeCode:'-1');

$selectAllBtn2 = '<input name="submit2" type="button" id="selectAllBtnResourcesTypeCode" onClick="javascript:Js_Select_All(\'ResourcesTypeCode\')" class="formsmallbutton" value="'.$Lang["libms"]["report"]["selectAll"].'" />';
$resourcesTypeCodeSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($resourcesTypeSelectionBox, $selectAllBtn2);
############################### Stocktake Status End ###############################

###############################  Circulation Start ###############################
$circulationArr = $libms->GET_CIRCULATION_TYPE_INFO();	

$numOfResourceTypeArr = count($circulationArr);	
$thisCirculationTypeSelectionArr = array();
$thisCirculationTypeSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfResourceTypeArr;$i++){
	$thisCirculationTypeCode = $circulationArr[$i]['CirculationTypeCode'];
	$thisDescriptionEn = $circulationArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $circulationArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCirculationTypeSelectionArr[]= array($thisCirculationTypeCode,$thisDescription);				
}	
$circulationTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCirculationTypeSelectionArr, 'name="selectedCirculationType[]" id="CirculationType" size="10" multiple', '', is_array($selectedCirculationType)?$selectedCirculationType:'-1');
$selectAllBtn3 = '<input name="submit2" type="button" id="selectAllBtnCirculationType" onClick="javascript:Js_Select_All(\'CirculationType\')" class="formsmallbutton" value="'.$Lang["libms"]["report"]["selectAll"].'" />';
$circulationTypeSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($circulationTypeSelectionBox, $selectAllBtn3);
###############################  Circulation End   ###############################

###############################  Category Start ###############################
$sql = "SELECT * FROM LIBMS_BOOK_CATEGORY WHERE BookCategoryType=1 ORDER BY BookCategoryCode";	
$categoryArr = $libms->returnArray($sql);

$numOfCategoryTypeArr = count($categoryArr);	
$thisCategoryTypeSelectionArr = array();
$thisCategoryTypeSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfCategoryTypeArr;$i++){
	$thisCategoryTypeCode = trim($categoryArr[$i]['BookCategoryCode']);
	$thisDescriptionEn = $categoryArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $categoryArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCategoryTypeSelectionArr[]= array($thisCategoryTypeCode,$thisDescription);				
}	
$categoryTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCategoryTypeSelectionArr, 'name="selectedBookCategory[]" id="BookCategory" size="10" multiple ', '', (is_array($selectedBookCategory) && ($BookCategoryType==1))?$selectedBookCategory:'-1',true);
$selectAllBtn5 = '<input name="submit2" type="button" id="selectAllBtnBookCategory" onClick="javascript:Js_Select_All(\'BookCategory\')" class="formsmallbutton" value="'.$Lang["libms"]["report"]["selectAll"].'" />';
$bookCategorySelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($categoryTypeSelectionBox, $selectAllBtn5);	
###############################  Category End   ###############################

###############################  Category2 Start ###############################
$sql = "SELECT * FROM LIBMS_BOOK_CATEGORY WHERE BookCategoryType=2 ORDER BY BookCategoryCode";	
$categoryArr = $libms->returnArray($sql);

$numOfCategoryTypeArr = count($categoryArr);	
$thisCategoryTypeSelectionArr = array();
$thisCategoryTypeSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfCategoryTypeArr;$i++){
	$thisCategoryTypeCode = trim($categoryArr[$i]['BookCategoryCode']);
	$thisDescriptionEn = $categoryArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $categoryArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCategoryTypeSelectionArr[]= array($thisCategoryTypeCode,$thisDescription);				
}	
$categoryTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCategoryTypeSelectionArr, 'name="selectedBookCategory[]" id="BookCategory2" size="10" multiple ', '', (is_array($selectedBookCategory) && ($BookCategoryType==2))?$selectedBookCategory:'-1',true);
$selectAllBtn5 = '<input name="submit2" type="button" id="selectAllBtnBookCategory2" onClick="javascript:Js_Select_All(\'BookCategory2\')" class="formsmallbutton" value="Select All" />';
$bookCategorySelectionBoxDiv2 = $linterface->Get_MultipleSelection_And_SelectAll_Div($categoryTypeSelectionBox, $selectAllBtn5);	
###############################  Category2 End   ###############################

###############################  Location Start ###############################
$locationArr = $libms->GET_LOCATION_INFO();	

$numOfLocationArr = count($locationArr);	
$thisLocationSelectionArr = array();
$thisLocationSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfLocationArr;$i++){
	$thisLocationCode = $locationArr[$i]['LocationCode'];
	$thisDescriptionEn = $locationArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $locationArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisLocationSelectionArr[]= array($thisLocationCode,$thisDescription);				
}	

$locationSelectionBox = $linterface->GET_SELECTION_BOX($thisLocationSelectionArr, 'name="selectedLocation[]" id="Location" size="10" multiple', '', is_array($selectedLocation)?$selectedLocation:'-1');
$selectAllBtn4 = '<input name="submit2" type="button" id="selectAllBtnCirculationType" onClick="javascript:Js_Select_All(\'Location\')" class="formsmallbutton" value="'.$Lang["libms"]["report"]["selectAll"].'" />';
$locationSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($locationSelectionBox, $selectAllBtn4);
	
###############################  Location End   ###############################
$linterface->LAYOUT_START();

########################## DB Table Start ###############################
$result = array();
$foundResult = array();
$writeoffResult = array();
$nottakeResult = array();

$writeoffResult = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID,'','',$returnType='Array','',$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $order=true, 'WriteOffDate, lbu.ACNO, lbu.BarCode', $selectedBookCategory, $StartDate, $EndDate, $BookCategoryType);	

$displayTable = '';
$displayTable .= '<table class="common_table_list view_table_list" width="100%" align="center"  border="0" cellSpacing="0" cellPadding="4">';
$displayTable .= '<thead>';
$displayTable .= '<tr>';
	$displayTable .= '<th width="1" class="tabletop tabletopnolink">';
	$displayTable .= '#';
	$displayTable .= '</th>';
	$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['bookmanagement']['WriteOffDate'];
	$displayTable .= '</th>';
	$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["report"]["ACNO"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['book']['account_date'];
	$displayTable .= '</th>';	
	$displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["call_number"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="15%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['bookmanagement']['bookTitle'];
	$displayTable .= '</th>';
	$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["purchase_date"];
	$displayTable .= '</th>';	
	$displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["purchase_by_department"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["list_price"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["discount"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["purchase_price"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="15%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['bookmanagement']['Reason'];
	$displayTable .= '</th>';
$displayTable .= '</tr>';
$displayTable .= '</thead>';
$displayTable .= '<tbody>';
$i=0;
foreach($writeoffResult as $resultInfo)
{
	$writeoffDateRange = $resultInfo['WriteOffDate']?($resultInfo['WriteOffDate']=='0000-00-00'?substr($resultInfo['DateModified'], 0, 10):$resultInfo['WriteOffDate']):substr($resultInfo['DateModified'], 0, 10);
	if($writeoffDateRange >= $StartDate && $writeoffDateRange <= $EndDate){
		$i++;
		$thisWriteoffDate = $writeoffDateRange;
		$thisACNO = $resultInfo['ACNO']?$resultInfo['ACNO']:$Lang['General']['EmptySymbol'];
		
		$callNum = $resultInfo['CallNum']?$resultInfo['CallNum']:'';
		if ($resultInfo['CallNum2']) {
			$callNum .= ' '.$resultInfo['CallNum2'];
		}
		$thisCallNum = $callNum;
				
		$thisBookTitle = $resultInfo['BookTitle']?$resultInfo['BookTitle']:$Lang['General']['EmptySymbol'];
		$thisBookPurchaseByDepartment = $resultInfo['PurchaseByDepartment']?$resultInfo['PurchaseByDepartment']:$Lang['General']['EmptySymbol'];
		$thisBookListPrice = $resultInfo['ListPrice']?$resultInfo['ListPrice']:$Lang['General']['EmptySymbol'];
		$thisBookDiscount = $resultInfo['Discount']?$resultInfo['Discount']:$Lang['General']['EmptySymbol'];
		$thisBookPublisher = $resultInfo['PurchasePrice']?$resultInfo['PurchasePrice']:$Lang['General']['EmptySymbol'];
		$thisWriteoffReason = $resultInfo['Reason']?$resultInfo['Reason']:$Lang['General']['EmptySymbol'];
		$thisBookPurchaseDate = $resultInfo['PurchaseDate']?$resultInfo['PurchaseDate']:$Lang['General']['EmptySymbol'];
		$thisBookAccountDate = $resultInfo['AccountDate']?$resultInfo['AccountDate']:$Lang['General']['EmptySymbol'];
	
		$displayTable .= '<tr>';	
			$displayTable .= '<td>';
			$displayTable .= $i ;
			$displayTable .= '</td>';	
			$displayTable .= '<td>';
			$displayTable .= $thisWriteoffDate;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisACNO;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookAccountDate;
			$displayTable .= '</td>';			
			$displayTable .= '<td>';
			$displayTable .= $thisCallNum;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookTitle;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookPurchaseDate;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookPurchaseByDepartment;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookListPrice;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookDiscount;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookPublisher;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisWriteoffReason;
			$displayTable .= '</td>';
		$displayTable .= '</tr>';
	}
}
if ($i==0)
{
	$displayTable .= '<tr>';	
		$displayTable .= '<td colspan="12" align="center">';
		$displayTable .= $Lang['libms']['NoRecordAtThisMoment'];
		$displayTable .= '</td>';
	$displayTable .= '</tr>';
}
$displayTable .= '</tbody>';

if ($i > 0) {	// show grand total
	$displayTable .= '<tfoot>';
		$displayTable .= '<tr>';	
			$displayTable .= '<td colspan="12" align="right">';
			$displayTable .= $Lang["libms"]["report"]["total"] . ': ' . $i;
			$displayTable .= '</td>';
		$displayTable .= '</tr>';
	$displayTable .= '</tfoot>';		
}
$displayTable .= '</table>';

?>

<script language="javascript">
$(document).ready(function(){	
	$('#spanShowOption').show();
	
	$('input[name="BookCategoryType"]').change(function(){
		if($('#BookCategoryTypeAll').is(':checked'))
		{
			$('#table_CategoryCode1').attr('style','display:none');
			$('#table_CategoryCode2').attr('style','display:none');
			$('#BookCategory option').attr('selected', false);
			$('#BookCategory2 option').attr('selected', false);
		}
		else if ($('#BookCategoryType1').is(':checked'))
		{				
			$('#table_CategoryCode1').attr('style','display:block');
			$('#table_CategoryCode2').attr('style','display:none');
			$('#BookCategory2 option').attr('selected', false);
		}
		else if ($('#BookCategoryType2').is(':checked'))
		{
			$('#table_CategoryCode1').attr('style','display:none');
			$('#table_CategoryCode2').attr('style','display:block');
			$('#BookCategory option').attr('selected', false);
		}
	});
	
	$('#BookCategory option').click(function(){
		$('#BookCategory2 option').attr('selected', false);
	});	
	
	$('#BookCategory2 option').click(function(){
		$('#BookCategory option').attr('selected', false);
	});		
});

function hideOptionLayer()
{
	$('#formContent').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	//document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('#formContent').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	//document.getElementById('div_form').className = 'report_option report_show_option';
}

function Js_Select_All(id){
	$('#'+id + ' option').attr('selected', 'selected');	
}

function Js_Check_Form(obj){
	var error = 0 ;
	var stocktakeLength = $('#stocktakeStatus option:selected').length;
//	var resourcesTypeCodeLength = $('#ResourcesTypeCode option:selected').length;
//	var circulationTypeLength = $('#CirculationType option:selected').length;
//	var locationLength = $('#Location option:selected').length;
	
//	if(stocktakeLength==0){
//		alert('<?=$i_alert_pleaseselect.$Lang['libms']['bookmanagement']['stocktakeRecord']?>');
//		error++;
//	}
//	else if(resourcesTypeCodeLength==0)
//	{		 
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["resources_type"]?>');
//		error++;
//	}
//	else if(circulationTypeLength==0)
//	{
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["circulation_type"]?>');
//		error++;
//	}
//	else if(locationLength==0)
//	{
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["book_location"]?>');		
//		error++;
//	}
//	
	if(document.getElementById("StartDate").value > document.getElementById('EndDate').value){
		document.getElementById("StartDate").focus();
		alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
		error++;
	}
	
	if ($('#BookCategoryType1').is(':checked')) {
		if($('#BookCategory option:checked').length == 0) {
			$('#BookCategory option').attr('selected', true);	
		}						
	}
	if ($('#BookCategoryType2').is(':checked')) {
		if($('#BookCategory2 option:checked').length == 0) {
			$('#BookCategory2 option').attr('selected', true);	
		}						
	}
	
	if(error==0)
	{
		obj.submit();
	}
}

function click_export()
{
	document.form1.action = "export.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
	document.form1.action = "result.php";
	document.form1.target="_self";
}

function click_print()
{
	document.form1.action="print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action = "result.php";
	document.form1.target="_self";
}	
</script>

<form name="form1" method="post" action="result.php">
<!--###### Content Board Start ######-->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td width="13" >&nbsp;</td>
  <td class="main_content"><!---->
 	<div class="report_option report_show_option">
 			<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>
 	</div>

	<span id="formContent" style="display:none" >
		<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td width="13">&nbsp;</td>
			<td class="main_content">
	       	<div class="report_option report_option_title"></div>
			<div class="table_board">
			<div class="table_board">
			<table class="form_table">
		        <tr class="form_table ">
		          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['bookmanagement']['WriteOffDate']?></td>
		          <td><?=$linterface->GET_DATE_PICKER("StartDate", $StartDate)?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
		        </tr>
		        <tr class="form_table ">
		          <td class="field_title"><?=$Lang["libms"]["settings"]["book_location"]?></td>
		          <td><span class="row_content">
		               <?=$locationSelectionBoxDiv?>
		          </td>
		        </tr>
				<tr class="form_table ">
		    		<td class="field_title"><?=$Lang["libms"]["settings"]["resources_type"]?></td>
		          	<td>
					<span class="row_content">					
					<?=$resourcesTypeCodeSelectionBoxDiv?>
					</td>
				</tr>			
		        <tr class="form_table ">
		          <td class="field_title"><?=$Lang["libms"]["settings"]["circulation_type"]?></td>
		          <td>
					<span class="row_content">
					<?=$circulationTypeSelectionBoxDiv?>
				  </td>
		        </tr>
		        <tr class="form_table ">
		          <td class="field_title"><?=$Lang['libms']['report']['book_category']?></td>
		          <td><span class="row_content">
		               <table class="inside_form_table">
						<tr>
							<td valign="top">
								<?
								$libms = new liblms();
								$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
								foreach ($result as $row){
									$book_category_name[] = htmlspecialchars($row['value']);
								}
								?>
								<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", 0, ($BookCategoryType==0?true:false), "", $Lang["libms"]["report"]["fine_status_all"])?>								
								<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, ($BookCategoryType==1?true:false), "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
								<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, ($BookCategoryType==2?true:false), "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode1" style='<?=($BookCategoryType==1?'':'display:none;')?>'>
							<td valign="top">
								<?=$bookCategorySelectionBoxDiv?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode2" style='<?=($BookCategoryType==2?'':'display:none;')?>'>
							<td valign="top">
								<?=$bookCategorySelectionBoxDiv2?>
							</td>
						</tr>
					</table>
		          </td>
		        </tr>
		     </table>
		     <?=$linterface->MandatoryField();?>
	     	<p class="spacer"></p>
	    	</div>
	    	<p class="spacer"></p>
	      	</div>
	      	<div class="edit_bottom">
	      
	        <p class="spacer"></p>
	        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
	        <p class="spacer"></p>
	      </div>
	      <p>&nbsp;</p></td>
	      <td width="11">&nbsp;</td>
	    </tr>
	  </table>
	</span>

	<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
		<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
		</div>
		<br style="clear:both" />
	</div>

   	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$linterface->GET_NAVIGATION2($Lang["libms"]["GroupManagement"]["Right"]["reports"]["write-off report"]);?>
				<p class="spacer"></p>
			    <p class="spacer"></p>
			    </td>
				<td valign="bottom"><div align="right"><?=$Lang['General']['Date'] . ': ' .$thisStockTakeSchemeDisplay?></div></td>
			</tr>
		</table> 					    
		
		<?=$displayTable?>
				
		<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="button" type="button" class="formbutton" onclick="window.location='index.php'" value="<?=$Lang['Btn']['Back']?>" /> 	
		<p class="spacer"></p>
  </div>
  <p>&nbsp;</p></td>
  <td width="11" >&nbsp;</td>
</tr>
</table>
<!--###### Content Board End ######-->
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
