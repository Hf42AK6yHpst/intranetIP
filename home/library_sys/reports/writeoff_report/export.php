<?php
# using:

/***************************************
 * 
 * Date:    2018-04-20 (Cameron)
 *          - retrieve AccountDate(CreationDate) [case #W120117]
 *          
 * Date:	2016-05-05 (Cameron)
 * 			- add column "Call Number" (case #E95394)
 *  
 * Date:	2015-09-01 (Henry)
 * Details:	added signature blank
 * 
 * Date:	2015-04-27 (Cameron)
 * Details:	Show Date Of Purchase for a book in the result list. (case ref: 2015-0421-0956-43206)
 * 
 * Date:	2015-01-23 (Henry)
 * Details:	Added Category2 filter
 * Date:	2013-11-27 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "";


if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['write-off report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$libms = new liblms();
$filename = "writeoff_report.csv";
$ExportArr = array();

## Obtain POST Value
$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$resourcesTypeCode = $_POST['selectedResourcesTypeCode'];
$selectedCirculationType = $_POST['selectedCirculationType'];
$selectedLocation = $_POST['selectedLocation'];
$selectedBookCategory = $_POST['selectedBookCategory'];

$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;
# Define Column Title
$exportColumn = array();
$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
$exportColumn[0][] = $schoolName;
$exportColumn[1][] = $Lang["libms"]["GroupManagement"]["Right"]["reports"]["write-off report"];
$exportColumn[2][] = $Lang['General']['Date'] . ':' ;
$exportColumn[2][] = $thisStockTakeSchemeDisplay ;

$exportColumn[4][] = '#';
$exportColumn[4][] = $Lang['libms']['bookmanagement']['WriteOffDate'];
$exportColumn[4][] = $Lang["libms"]["report"]["ACNO"];
$exportColumn[4][] = $Lang['libms']['book']['account_date'];
$exportColumn[4][] = $Lang["libms"]["book"]["call_number"];
$exportColumn[4][] = $Lang['libms']['bookmanagement']['bookTitle'];
$exportColumn[4][] = $Lang["libms"]["book"]["purchase_date"];
$exportColumn[4][] = $Lang["libms"]["book"]["purchase_by_department"];
$exportColumn[4][] = $Lang["libms"]["book"]["list_price"];
$exportColumn[4][] = $Lang["libms"]["book"]["discount"];
$exportColumn[4][] = $Lang["libms"]["book"]["purchase_price"];
$exportColumn[4][] = $Lang['libms']['bookmanagement']['Reason'];


$writeoffResult = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID,'','',$returnType='Array','',$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $order=true, 'WriteOffDate, lbu.ACNO, lbu.BarCode', $selectedBookCategory, $StartDate, $EndDate, $BookCategoryType);	

$numOfResult = count($writeoffResult);
$counter = 0;
for($i=0;$i<$numOfResult;$i++){
	$writeoffDateRange = $writeoffResult[$i]['WriteOffDate']?($writeoffResult[$i]['WriteOffDate']=='0000-00-00'?substr($writeoffResult[$i]['DateModified'], 0, 10):$writeoffResult[$i]['WriteOffDate']):substr($writeoffResult[$i]['DateModified'], 0, 10);
	if($writeoffDateRange >= $StartDate && $writeoffDateRange <= $EndDate){
		
	$thisWriteoffDate = $writeoffDateRange;
		$thisACNO = $writeoffResult[$i]['ACNO']?$writeoffResult[$i]['ACNO']:$Lang['General']['EmptySymbol'];

		$callNum = $writeoffResult[$i]['CallNum']?$writeoffResult[$i]['CallNum']:'';
		if ($writeoffResult[$i]['CallNum2']) {
			$callNum .= ' '.$writeoffResult[$i]['CallNum2'];
		}
		$thisCallNum = $callNum;		
		
		$thisBookTitle = $writeoffResult[$i]['BookTitle']?$writeoffResult[$i]['BookTitle']:$Lang['General']['EmptySymbol'];
		$thisBookPurchaseByDepartment = $writeoffResult[$i]['PurchaseByDepartment']?$writeoffResult[$i]['PurchaseByDepartment']:$Lang['General']['EmptySymbol'];
		$thisBookListPrice = $writeoffResult[$i]['ListPrice']?$writeoffResult[$i]['ListPrice']:$Lang['General']['EmptySymbol'];
		$thisBookDiscount = $writeoffResult[$i]['Discount']?$writeoffResult[$i]['Discount']:$Lang['General']['EmptySymbol'];
		$thisBookPublisher = $writeoffResult[$i]['PurchasePrice']?$writeoffResult[$i]['PurchasePrice']:$Lang['General']['EmptySymbol'];
		$thisWriteoffReason = $writeoffResult[$i]['Reason']?$writeoffResult[$i]['Reason']:$Lang['General']['EmptySymbol'];
		$thisBookPurchaseDate = $writeoffResult[$i]['PurchaseDate']?$writeoffResult[$i]['PurchaseDate']:$Lang['General']['EmptySymbol'];
		$thisBookAccountDate = $writeoffResult[$i]['AccountDate']?$writeoffResult[$i]['AccountDate']:$Lang['General']['EmptySymbol'];
	
	
	$ExportArr[$counter][] = $counter+1;
	$ExportArr[$counter][] = $thisWriteoffDate;
	$ExportArr[$counter][] = $thisACNO;
	$ExportArr[$counter][] = $thisBookAccountDate;
	$ExportArr[$counter][] = $thisCallNum;
	$ExportArr[$counter][] = $thisBookTitle;
	$ExportArr[$counter][] = $thisBookPurchaseDate;
	$ExportArr[$counter][] = $thisBookPurchaseByDepartment;
	$ExportArr[$counter][] = $thisBookListPrice;
	$ExportArr[$counter][] = $thisBookDiscount;
	$ExportArr[$counter][] = $thisBookPublisher;
	$ExportArr[$counter][] = $thisWriteoffReason;

		$counter++;
	}
}

$exportColumn[3][] = $Lang['libms']['report']['WriteoffPrintTotal'];
$exportColumn[3][] = $counter;

$ExportArr[$counter][] = '';
$counter++;
$ExportArr[$counter][] = $Lang['libms']['report']['ApprovedBy'].':';
$ExportArr[$counter][] = '';
$ExportArr[$counter][] = '';
$ExportArr[$counter][] = $Lang['libms']['reporting']['date'];
$ExportArr[$counter][] = '';
$ExportArr[$counter][] = '';
$counter++;
$ExportArr[$counter][] = '';
$counter++;
$ExportArr[$counter][] = $Lang['libms']['report']['SubmittedBy'].':';
$ExportArr[$counter][] = '';
$ExportArr[$counter][] = '';
$ExportArr[$counter][] = $Lang['libms']['reporting']['date'];
$ExportArr[$counter][] = '';

$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "9");

intranet_closedb();

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="UTF-8");

?>