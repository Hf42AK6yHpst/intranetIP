<?php
/*
 * 	Log
 * 	Purpose:	save record to LIBMS_OVERDUE_REMARK, do insert if it's not already been added, else do update
 * 				then return remark
 * 	Date:	2015-02-16 [Cameron] create this file
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$overdueRemark = trim($_POST['OverdueRemark']);

$libms = new liblms();
$ret = array();

if ($overdueRemark) {
	$sRemark = htmlentities($overdueRemark,ENT_QUOTES,"UTF-8");
	$sql = "SELECT 
					LogID 
			FROM
					LIBMS_OVERDUE_REMARK
			WHERE 
					Remark='".$libms->Get_Safe_Sql_Query($sRemark)."'
			LIMIT 1";
	$rs = $libms->returnArray($sql);
	
	if (count($rs) > 0) {	// record exist, update
		$sql = "UPDATE 
						LIBMS_OVERDUE_REMARK
				SET 
						DateModified=NOW(),
						LastModifiedBy='".$_SESSION['UserID']."'
				WHERE 	LogID='".$rs[0]['LogID']."'";
		$result = $libms->db_db_query($sql);
		$overdueRemarkID = $rs[0]['LogID'];					
	}	
	else {	// record not exist, insert
		$sql = "INSERT INTO
						LIBMS_OVERDUE_REMARK 
					(
						Remark,
						DateModified,
						LastModifiedBy
					)			
				VALUES (
						'".$libms->Get_Safe_Sql_Query($sRemark)."',
						NOW(),	
						'".$_SESSION['UserID']."'
					)";
		$result = $libms->db_db_query($sql);
		$overdueRemarkID = $libms->db_insert_id();					
	}		
	
	
	if ($result) {	// successful
		
		$ret['OverdueRemark'] = nl2br($sRemark);
		$ret['OverdueRemarkID'] = $overdueRemarkID; 
	}
}

$json = new JSON_obj();
echo $json->encode($ret);

intranet_closedb();
?>