<?php

// Using: 

/***************************************
 * 
 * Date:	2017-05-09 [Henry]
 * 			add remarks for date range [Case#F115789]
 * 
 * Date:	2016-07-15 [Cameron]
 * 			don't allow to run report if date fields are invalid (IP2.5.7.7.1)
 * 
 * Date:	2013-11-29 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['progress report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["Right"]["reports"]["progress report"]);
$CurrentPage = "PageReportingProgressReport";

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();

?>
<script language="javascript">

$(document).ready(function(){
	$('#selectAllBtnStocktakeStatus').click(function(){ 		
		$('#stocktakeStatus option').attr('selected', true);
	});
});	
	
function Js_Select_All(id){
	$('#'+id + ' option').attr('selected', 'selected');	
}

function Js_Check_Form(obj){
	var error = 0 ;
	var stocktakeLength = $('#stocktakeStatus option:selected').length;
	
	if (!check_date(document.getElementById("StartDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
		return;
	if (!check_date(document.getElementById("EndDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
		return;
	
	if(document.getElementById("StartDate").value > document.getElementById('EndDate').value){
		document.getElementById("StartDate").focus();
		alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
		error++;
	}
	if(error==0)
	{
		obj.submit();
	}

}

</script>

<form name="form1" method="post" action="result.php?clearCoo=1">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="13">&nbsp;</td>
		<td>
       	<div class="report_option report_option_title"></div>
		<div class="table_board">
		<table class="form_table">
	        <tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['dateRange']?><br/>(<?=$Lang['libms']['reporting']['progressDateRangeRemarks']?>)</td>
	          <td><?=$linterface->GET_DATE_PICKER("StartDate", ($StartDate?$StartDate:date('Y-m-01')))?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
	        </tr>
	     </table>
	     <?=$linterface->MandatoryField();?>
      <p class="spacer"></p>
    </div>
    <p class="spacer"></p>
      </div>
      <div class="edit_bottom">
        <p class="spacer"></p>
        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
        <p class="spacer"></p>
      </div>
      <p>&nbsp;</p></td>
      <td width="11">&nbsp;</td>
    </tr>
  </table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
