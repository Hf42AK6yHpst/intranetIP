<?php

include_once($intranet_root."/includes/libexporttext.php");

class Exporter {
	
	/**
	 * convert array to CSV
	 * @param 2D array of $data
	 * @return string of CSV presentation
	 */
	public static function array_to_CSV($data)
    {
        $outstream = fopen("php://temp", 'r+');
        
        $length=0;
        foreach($data as $row){
        	$length += fputcsv($outstream, $row, "\t", '"');
        }
        rewind($outstream);
        $contents = fread($outstream, $length);
        fclose($outstream);
        return $contents;
    }
    
    /**
     * HHTTP return a file of the string 
     * @param $filename
     * @param $string - the file content.
     */
 	public static function string_to_file_download($filename , $string){
 		$le = new libexporttext();

 		$le->EXPORT_FILE($filename, $string, false, false, "UTF-8");
 	}   
}

?>