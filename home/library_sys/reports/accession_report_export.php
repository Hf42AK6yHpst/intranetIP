<?php
# using: 

/***************************************
 * 
 * Date:    2018-09-28 (Cameron)
 * Details:	add argument tags to get_accession_report() [case #X138957]
 * 
 * Date:	2016-12-06 (Cameron)
 * Details:	add argument PurchaseByDepartment to get_accession_report()
 * 
 * Date:	2016-03-03 (Henry)
 * Details: set memory_limit to 300M 
 * 
 * Date: 	2015-03-02 (Henry)
 * Details: added para book category type
 * 
 * Date: 	2013-11-14 (Henry)
 * Details: Create this page
 ***************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['accession report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

# Temp Assign memory of this page
ini_set("memory_limit", "300M"); 

$lexport = new libexporttext();
$libms = new liblms();
$filename = "accession_report.csv";

# Define Column Title
$exportColumn = array();
$exportColumn[0][] = $Lang["libms"]["book"]["account_date"];
$exportColumn[0][] = $Lang["libms"]["book"]["code"];
$exportColumn[0][] = $Lang["libms"]["book"]["barcode"];
$exportColumn[0][] = $Lang["libms"]["report"]["ResponsibilityBy"];
$exportColumn[0][] = $Lang["libms"]["book"]["title"];
$exportColumn[0][] = $Lang["libms"]["book"]["call_number"];
$exportColumn[0][] = $Lang["libms"]["book"]["publisher"];
$exportColumn[0][] = $Lang["libms"]["book"]["publish_year"];
$exportColumn[0][] = $Lang["libms"]["book"]["distributor"];
$exportColumn[0][] = $Lang["libms"]["book"]["invoice_number"];
$exportColumn[0][] = $Lang["libms"]["book"]["list_price"];
$exportColumn[0][] = $Lang["libms"]["book"]["discount"];
$exportColumn[0][] = $Lang["libms"]["book"]["purchase_price"];
$exportColumn[0][] = $Lang["libms"]["book"]["purchase_by_department"];
$exportColumn[0][] = $Lang["libms"]["book"]["remark_internal"];

if($searchType == 0){
	$StartDate = "";
	$EndDate = "";
}
else if($searchType == 1){
	$from_acno = "";
	$to_acno = "";
}
else if($searchType == 2){
	$StartDate = "";
	$EndDate = "";
	$from_acno = "";
	$to_acno = "";
}
$tags = $_POST['tags'];

$ExportArr = $libms->get_accession_report($from_acno, $to_acno, $StartDate, $EndDate, $CirculationTypeCode, $BookCategoryCode, $BookSubject, $BookLanguage, $BookStatus, "EXPORT", $enableLine, $BookCategoryType, $PurchaseByDepartment, $tags);
	
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="UTF-8");

?>