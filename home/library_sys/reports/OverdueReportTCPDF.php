<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($MODULE_ROOT.'lib/tcpdf/tcpdf.php');

class OverdueReportTCPDF extends TCPDF {

	public $LabelName  = '';       // Name of format
	public $marginLeft = 0;        // Left margin of labels
	public $marginTop  = 0;        // Top margin of labels
	public $charSize   = 10;       // Character size
	public $lineHeight = 0;        // Default line height
	public $metric     = 'mm';     // Type of metric for labels.. Will help to calculate good values
	public $metricDoc  = 'mm';     // Type of metric for the document
	public $smallest_bar = 0.3;		// width of the smallest bar in user units (empty string = default value = 0.4mm)
	public $cursor_Y     = 0;
	public $max_barcode_width = 90;
	public $max_barcode_height = 20;
	/**
	 * Constructor
	 *
	 * @param $format type of label 
	 * 
	 * @param unit type of unit used we can define your label properties in inches by setting metric to 'in'
	 *
	 * @access public
	 */

	function __construct ($format=Array(), $smallest_bar =null) {
		if (is_array($format)) {
			// Custom format
			$tFormat = $format;
		} else {
			
		}
		 
		parent::__construct('', $tFormat['metric'], $tFormat['paper-size'], true, 'UTF8');
		$this->metricDoc = $tFormat['metric'];
		$this->SetFormat($tFormat);
		$this->SetFontName('arialuni'); // MS Arial Unicode
		$this->SetMargins(0,0);
		$this->SetAutoPageBreak(false);
		$this->smallest_bar = $smallest_bar;
		
		
	}

	/*
	 * function to convert units (in to mm, mm to in)
	*
	*/
	function ConvertMetric ($value, $src, $dest) {
		if ($src != $dest) {
			$tab['in'] = 39.37008;
			$tab['mm'] = 1000;
			return $value * $tab[$dest] / $tab[$src];
		} else {
			return $value;
		}
	}
	/*
	 * function to Give the height for a char size given.
	*/
	function GetHeightChars($pt) {
			$x = 0.186 + 0.20 * $pt;
			$x *= $x;
		return $x;
// 		// Array matching character sizes and line heights
// 		$tableHauteurChars = array(6 => 2, 7 => 2.5, 8 => 3, 9 => 4, 10 => 5, 11 => 6, 12 => 7, 13 => 8, 14 => 9, 15 => 10);
// 		if (in_array($pt, array_keys($tableHauteurChars))) {
// 			return $tableHauteurChars[$pt];
// 		} else {
// 			return 100; // There is a prob..
// 		}
	}
	/*
	* Set Formating in member variable
	* $format Type of $averyName
	*/
	function SetFormat($format) {
		$this->metric     = $format['metric'];
		$this->LabelName  = $format['name'];
		$this->marginLeft = $this->ConvertMetric ($format['lMargin'], $this->metric, $this->metricDoc);
		$this->marginTop  = $this->ConvertMetric ($format['tMargin'], $this->metric, $this->metricDoc);
		$this->lineHeight = $this->ConvertMetric ($format['lineHeight'], $this->metric, $this->metricDoc);
		
		$this->LabelSetFontSize($format['font-size']);
	}
	/*
	* function to set the character size
	* $pt weight of character
	*/
	function LabelSetFontSize($pt) {
		if ($pt > 3) {
			$this->charSize = $pt;
			if( $this->lineHeight == 0 )
				$this->lineHeight = $this->ConvertMetric ($this->GetHeightChars($pt), 'mm', $this->metricDoc);
			$this->SetFontSize($this->charSize);
		}
	}
	/*
	 * Method to change font name
	*
	* $fontname name of font
	*/
	function SetFontName($fontname) {
		if ($fontname != '') {
			$this->fontName = $fontname;
			$this->SetFont($this->fontName);
		}
	}
	
	
	
	/*
	 * function to Print a notification slip 
	*/	
	function AddBlock(){
		$printing_margin_h = $this->printing_margin_h;
		$printing_margin_v = $this->printing_margin_v;

		
		$this->AddPage();
		
		$this-> SetLineStyle ();
		$this -> Line();
		
	}
	
		
	
	
	
	
###### to be DEL 
	/*
	 * function to Print a label
	*/
	function AddLabel($text=array(),$barcode='', $border=false) {
		// We are in a new page, then we must add a page
		if (($this->countX ==0) and ($this->countY==0)) {
			$this->AddPage();
		}
		
		$printing_margin_h = $this->printing_margin_h;
		$printing_margin_v = $this->printing_margin_v;
		
		$posX = $this->marginLeft+($this->countX*($this->width+$this->xSpace));
		$posY = $this->marginTop+($this->countY*($this->height+$this->ySpace));
		
		if ($border){
			$this->Rect($posX, $posY,$this->width,$this->height);
			
		}
		$this->SetXY($posX+$printing_margin_h, $posY + $printing_margin_v);
		$this->maxwidth = $this->width - $printing_margin_h *2;
		//wrap the text if it's width is greater than maxwidth
		//      $this->wordWrap( $texte, $maxwidth); not supported by TCPDF, which does its own wrapping
		// define barcode style
		$line=0;
		if(!empty($text)){
			if(is_array($text)){
				$fontheight = $this->lineHeight ;
				foreach ($text as $row){
					if(!empty($row)){
						$printing_width = $this->width-$printing_margin_v*2;
						$printing_height = $this->lineHeight;
						
						$line +=$this->MultiCell($printing_width, $printing_height, $row, 0 ,'C');
						
						//$line++;
						$this->SetXY($posX+$printing_margin_h, $posY+($line * $this->lineHeight)+$printing_margin_v);
						
					}
				}	
			}else{
				$line++;
				$this->MultiCell($this->width, $this->lineHeight, $text , 0 ,'C');
			}
		}
		
		$barcode_width = $this->width - $printing_margin_h *2; // 6 for margins
		if ($barcode_width > $this->max_barcode_width){
			$barcode_width = $this->max_barcode_width;
			//centering
			$new_margin = (($this->maxwidth - $barcode_width )/2)+$printing_margin_h;
			
		}else{
			$new_margin = $printing_margin_h;
		}

		$this->SetXY($posX+$new_margin, $posY+($line * $this->lineHeight)+$printing_margin_v);
		
		$barcode_height =$this->height -($line * $this->lineHeight);
		$barcode_height -= $printing_margin_v; // 6 for margins
		
		if($barcode_height > $this->max_barcode_height)
			$barcode_height = $this->max_barcode_height;
		
		
		$this->write1DBarcode($barcode, 'C39', '', '', $barcode_width, $barcode_height, $this->smallest_bar, $this->barcode_style, 'N');
		
		//shift to next label
		$this->countY++;

		if ($this->countY == $this->yNumber) {
			// End of column reached, we start a new one
			$this->countX++;
			$this->countY=0;
		}

		if ($this->countX == $this->xNumber) {
			// Page full, we start a new one
			$this->countX=0;
			$this->countY=0;
		}


	}

}
