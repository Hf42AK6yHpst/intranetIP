<?php
#using: 

/***************************************
 * 
 * Date:    2018-09-28 (Cameron)
 * Details:	add argument tags to get_accession_report() [case #X138957]
 * 
 * Date:	2016-12-06 (Cameron)
 * Details:	add argument PurchaseByDepartment to get_accession_report()
 * 
 * Date:	2015-08-28 (Henry)
 * Details: change wordings
 * 
 * Date: 	2015-03-02 (Henry)
 * Details: added para book category type
 * 
 * Date: 	2014-05-13 (Henry)
 * Details: Added school name at top of the page
 * 
 * Date: 	2013-11-14 (Henry)
 * Details: Create this page
 ***************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['accession report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

if($searchType == 1)
	$reportPeriodDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;

if(trim($from_acno) != "" && trim($to_acno) !=""){
		$reportACNODisplay = strtoupper(trim($from_acno)).' '.$Lang['General']['To'].' '.strtoupper(trim($to_acno));
	}
	else if(trim($from_acno) != ""){
		$reportACNODisplay = strtoupper(trim($from_acno));
	}
	else{
		$reportACNODisplay = strtoupper(trim($to_acno));
	}

if($searchType == 0){
	$StartDate = "";
	$EndDate = "";
}
else if($searchType == 1){
	$from_acno = "";
	$to_acno = "";
}
else if($searchType == 2){
	$StartDate = "";
	$EndDate = "";
	$from_acno = "";
	$to_acno = "";
}
$tags = $_POST['tags'];

$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

$displayTable = '';
$displayTable .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">';
$displayTable .= '<thead><tr>';
$displayTable .= '<td align="right" class="print_hide">';
$displayTable .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$displayTable .= '</td>'; 
$displayTable .= '</tr>'; 
$displayTable .= '<tr><td>'.$schoolName.'</td></tr>';
$displayTable .= '<tr>';
$displayTable .= '<td>';
$displayTable .= '<center><h2>' .$Lang['libms']['reporting']['accessionReport'] . '</h2></center>';
$displayTable .= '</td>';
$displayTable .= '</tr></thead>';
//$displayTable .= '<tr>';
//$displayTable .= '<td align="right">';
//$displayTable .= '<table>';
//if($searchType == 0)
//	$displayTable .= '<tr><td>'.$Lang["libms"]["report"]["ACNO"] . ': </td><td>'.$reportACNODisplay.'</td></tr>';
//if($searchType == 1)
//	$displayTable .= '<tr><td>'.$Lang['General']['Date'] . ': </td><td>'.$reportPeriodDisplay.'</td></tr>';
//$displayTable .= '</table>';	
//$displayTable .= '</td>'; 
//$displayTable .= '</tr>';
$displayTable .= '<tbody><tr>';
$displayTable .= '<td align="center">';

$displayTable .= $libms->get_accession_report($from_acno, $to_acno, $StartDate, $EndDate, $CirculationTypeCode, $BookCategoryCode, $BookSubject, $BookLanguage, $BookStatus, "PRINT", $enableLine, $BookCategoryType, $PurchaseByDepartment, $tags);

$numOfPages = substr_count($displayTable, 'page-break20')+1;
	
$displayTable .= '</td>';
$displayTable .= '</tr>';

$displayTable .= '</tbody>';
$displayTable .= '<tfoot><tr><td>';
$displayTable .= '<table id="footer" style="border-top:solid;border-bottom:solid;width:100%"><tr><td>登錄編號(Acc. No.): '.$reportACNODisplay.'</td><td align="right">Printing Date: '.date('Y-m-d').'</td></tr>';
$displayTable .= '<tr><td>登錄日期(Acc. Date): '.$reportPeriodDisplay.'</td><td id="page_num" align="right"></td></tr>';
$displayTable .= '</table>';
$displayTable .= '</td></tr></tfoot>';
$displayTable .= '</table>';

//$displayTable .= '<div id="footer">登錄編號(Acc. No.): '.$reportACNODisplay.'<br/>';
//$displayTable .= '登陸日期(Acc. Date): '.$reportPeriodDisplay;
//$displayTable .= '</div>';

//$displayTable .= '<table id="footer" style="border-top:solid;border-bottom:solid;width:100%"><tr><td>登錄編號(Acc. No.): '.$reportACNODisplay.'</td><td align="right">Printing Date: '.date('Y-m-d').'</td></tr>';
//$displayTable .= '<tr><td>登陸日期(Acc. Date): '.$reportPeriodDisplay.'</td><td id="page_num" align="right"></td></tr>';
//$displayTable .= '</table>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">

<style type='text/css'>

html, body { margin: 0px; padding: 0px;} 


@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
       size: landscape;
    }
 }
 
@media print
{
	@page {size: A4 landscape; margin:.1cm .3cm;}
	table {  font-size:8.5px;width:29cm}
	tr.page-break20 { page-break-inside:avoid;page-break-before:always;}
	td { line-height:100% }
	thead { display:table-header-group }
	tfoot { display:table-footer-group }
	/*#footer {
		position:fixed;
		bottom:5px;
		width:100%;
	}*/
	#footer td { width:50% ;align:right}
	#page_num:after {
		counter-increment: page_num;
		content: "Page " counter(page_num) " of <?=$numOfPages?>";
	}  
}

</style>
</head>
<body>
<?php echo $displayTable;?>
</body>
</html>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>