<?php

//	Using: 

/***************************************
 * 
 * Date:    2018-04-26 (Cameron)
 *          - retrieve AccountDate(CreationDate) [case #E89629]
 *          
 * Date:	2016-06-15 (Cameron)
 * 			- Set "Back" button to the value in lang file
 * Date:	2016-04-16 (Henry)
 * Details:	add columns List Price and Purchase Price
 * 
 * Date:	2015-07-21 (Cameron)
 * 			- Add radio option "All" to BookCategory
 * 			- show selected BookCategory based on BookCategoryType
 * 			- Add order by BookCategoryCode in retrieving BookCategory option list to compatible with other reports
 * 
 * Date:	2015-01-23 (Henry)
 * Details:	Added Category2 filter
 * 
 * Date: 	2015-01-15 (Henry)
 * Details: Added memory_limit
 * 
 * Date:	2014-02-13 (Henry)
 * Details:	Added filter Book Category
 * 
 * Date:	2013-11-25 (Henry)
 * Details:	Added column Write-off Date and Reason
 * 
 * Date:	2013-06-19 (Rita)
 * Details: Amend access right checking
 * 
 * Date: 2013-06-04 (Rita)
 * Details: Create this page
 ***************************************/
 
ini_set('memory_limit','500M');
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "&nbsp;";

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['stock-take report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang['libms']['reporting']['stocktakeReport']);
$CurrentPage = "PageReportingStocktakeReport";
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

## Obtain POST Value
$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
$selectedStocktakeStatus = $_POST['selectedStocktakeStatus'];
$resourcesTypeCode = $_POST['selectedResourcesTypeCode'];
$selectedCirculationType = $_POST['selectedCirculationType'];
$selectedLocation = $_POST['selectedLocation'];
$selectedBookCategory = $_POST['selectedBookCategory'];

if($stocktakeSchemeID==''){
	header('Location: index.php');	
}

################### Stocktake Scheme Selection Box Start ######################
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('',1);
$stockTakeSchemeArr = $libms->returnArray($sql);
$stockTakeSchemeIDArr = BuildMultiKeyAssoc($stockTakeSchemeArr, 'StocktakeSchemeID');
## Default StocktakeSchemeID

$numOfstockTakeSchemeArr = count($stockTakeSchemeArr);
for($i=0;$i<$numOfstockTakeSchemeArr;$i++){
	$thisStockTakeSchemeId = $stockTakeSchemeArr[$i]['StocktakeSchemeID'];
	$thisStockTakeDisplay = $stockTakeSchemeArr[$i]['SchemeTitle'] . ' ~ (' .  $stockTakeSchemeArr[$i]['StartDate']  . ' - ' .  $stockTakeSchemeArr[$i]['EndDate'] . ')';
	$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);
}
$stockTakeSelectionBox = $linterface->GET_SELECTION_BOX($stockTakeSchemeSelectionArr, 'name="StocktakeSchemeID" id="StocktakeSchemeID" ', '', $stocktakeSchemeID);
#################### Stocktake Scheme Selection Box End ########################

# Stocktake Date Display
$thisStockTakeSchemeDisplayArr = $stockTakeSchemeIDArr[$stocktakeSchemeID];
$thisStockTakeSchemeDisplay = $thisStockTakeSchemeDisplayArr['StartDate']  . ' ' . $Lang['General']['To']  . ' ' .  $thisStockTakeSchemeDisplayArr['EndDate'] ;

############################### Stocktake Status Start ###############################
$i = 0;
$stocktakeStatusArr = array();
foreach ($Lang['libms']['stocktake_status'] as $key => $value)
{
	$stocktakeStatusArr[$i][0] = $key;
	$stocktakeStatusArr[$i][1] = $value;
	$i++;
}
if($selectedStocktakeStatus==''){
	$selectedStocktakeStatus = $stocktakeStatusArr[0][0];
}
$stockTakeStatusSelectionBox = $linterface->GET_SELECTION_BOX($stocktakeStatusArr, " name='selectedStocktakeStatus[]' id='stocktakeStatus' multiple", '', $selectedStocktakeStatus);
$selectAllBtn = '<input name="submit1" type="button" id="selectAllBtnStocktakeStatus" onClick="javascript:Js_Select_All(\'stocktakeStatus\')" class="formsmallbutton" value="Select All" />';
$stockTakeStatusSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($stockTakeStatusSelectionBox, $selectAllBtn);
############################### Stocktake Status End ###############################

############################### Resources Type Start ###############################
$resourceTypeArr = $libms->GET_RESOURCES_TYPE_INFO();	
$numOfResourceTypeArr = count($resourceTypeArr);	
$thisResourcesTypeSelectionArr = array();
$thisResourcesTypeSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfResourceTypeArr;$i++){
	$thisResourcesTypeCode = $resourceTypeArr[$i]['ResourcesTypeCode'];
	$thisDescriptionEn = $resourceTypeArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $resourceTypeArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisResourcesTypeSelectionArr[]= array($thisResourcesTypeCode,$thisDescription);				
}	

$resourcesTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisResourcesTypeSelectionArr, 'name="selectedResourcesTypeCode[]" id="ResourcesTypeCode" size="10" multiple', '', is_array($resourcesTypeCode)?$resourcesTypeCode:'-1');

$selectAllBtn2 = '<input name="submit2" type="button" id="selectAllBtnResourcesTypeCode" onClick="javascript:Js_Select_All(\'ResourcesTypeCode\')" class="formsmallbutton" value="Select All" />';
$resourcesTypeCodeSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($resourcesTypeSelectionBox, $selectAllBtn2);
############################### Stocktake Status End ###############################

###############################  Circulation Start ###############################
$circulationArr = $libms->GET_CIRCULATION_TYPE_INFO();	

$numOfResourceTypeArr = count($circulationArr);	
$thisCirculationTypeSelectionArr = array();
$thisCirculationTypeSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfResourceTypeArr;$i++){
	$thisCirculationTypeCode = $circulationArr[$i]['CirculationTypeCode'];
	$thisDescriptionEn = $circulationArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $circulationArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCirculationTypeSelectionArr[]= array($thisCirculationTypeCode,$thisDescription);				
}
	
$circulationTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCirculationTypeSelectionArr, 'name="selectedCirculationType[]" id="CirculationType" size="10" multiple', '', is_array($selectedCirculationType)?$selectedCirculationType:'-1');
$selectAllBtn3 = '<input name="submit2" type="button" id="selectAllBtnCirculationType" onClick="javascript:Js_Select_All(\'CirculationType\')" class="formsmallbutton" value="Select All" />';
$circulationTypeSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($circulationTypeSelectionBox, $selectAllBtn3);
###############################  Circulation End   ###############################

###############################  Category Start ###############################
$sql = "SELECT * FROM LIBMS_BOOK_CATEGORY WHERE BookCategoryType=1 ORDER BY BookCategoryCode";	
$categoryArr = $libms->returnArray($sql);

$numOfCategoryTypeArr = count($categoryArr);	
$thisCategoryTypeSelectionArr = array();
$thisCategoryTypeSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);	
for($i=0;$i<$numOfCategoryTypeArr;$i++){
	$thisCategoryTypeCode = trim($categoryArr[$i]['BookCategoryCode']);
	$thisDescriptionEn = $categoryArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $categoryArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCategoryTypeSelectionArr[]= array($thisCategoryTypeCode,$thisDescription);				
}

$categoryTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCategoryTypeSelectionArr, 'name="selectedBookCategory[]" id="BookCategory" size="10" multiple ', '', (is_array($selectedBookCategory) && ($BookCategoryType==1))?$selectedBookCategory:'-1',true);
$selectAllBtn5 = '<input name="submit2" type="button" id="selectAllBtnBookCategory" onClick="javascript:Js_Select_All(\'BookCategory\')" class="formsmallbutton" value="Select All" />';
$bookCategorySelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($categoryTypeSelectionBox, $selectAllBtn5);	
###############################  Category End   ###############################

###############################  Category2 Start ###############################
$sql = "SELECT * FROM LIBMS_BOOK_CATEGORY WHERE BookCategoryType=2 ORDER BY BookCategoryCode";	
$categoryArr = $libms->returnArray($sql);

$numOfCategoryTypeArr = count($categoryArr);	
$thisCategoryTypeSelectionArr = array();
$thisCategoryTypeSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfCategoryTypeArr;$i++){
	$thisCategoryTypeCode = trim($categoryArr[$i]['BookCategoryCode']);
	$thisDescriptionEn = $categoryArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $categoryArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCategoryTypeSelectionArr[]= array($thisCategoryTypeCode,$thisDescription);				
}	
$categoryTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCategoryTypeSelectionArr, 'name="selectedBookCategory[]" id="BookCategory2" size="10" multiple ', '', (is_array($selectedBookCategory) && ($BookCategoryType==2))?$selectedBookCategory:'-1',true);
$selectAllBtn5 = '<input name="submit2" type="button" id="selectAllBtnBookCategory2" onClick="javascript:Js_Select_All(\'BookCategory2\')" class="formsmallbutton" value="Select All" />';
$bookCategorySelectionBoxDiv2 = $linterface->Get_MultipleSelection_And_SelectAll_Div($categoryTypeSelectionBox, $selectAllBtn5);	
###############################  Category2 End   ###############################

###############################  Location Start ###############################
$locationArr = $libms->GET_LOCATION_INFO();	

$numOfLocationArr = count($locationArr);	
$thisLocationSelectionArr = array();
$thisLocationSelectionArr[]= array('',$Lang["libms"]["report"]["cannot_define"]);
for($i=0;$i<$numOfLocationArr;$i++){
	$thisLocationCode = $locationArr[$i]['LocationCode'];
	$thisDescriptionEn = $locationArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $locationArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisLocationSelectionArr[]= array($thisLocationCode,$thisDescription);				
}	
	
$locationSelectionBox = $linterface->GET_SELECTION_BOX($thisLocationSelectionArr, 'name="selectedLocation[]" id="Location" size="10" multiple', '', is_array($selectedLocation)?$selectedLocation:'-1');
$selectAllBtn4 = '<input name="submit2" type="button" id="selectAllBtnCirculationType" onClick="javascript:Js_Select_All(\'Location\')" class="formsmallbutton" value="Select All" />';
$locationSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($locationSelectionBox, $selectAllBtn4);
	
###############################  Location End   ###############################
$linterface->LAYOUT_START();

########################## DB Table Start ###############################
$result = array();
$foundResult = array();
$writeoffResult = array();
$nottakeResult = array();

//if(is_array($selectedLocation)){
//	for($i=0;$i<count($selectedLocation);$i++)
//	if($selectedLocation[$i] == ''){
//		$selectedLocation[$i] = -1;
//	}
//}else{
//	if($selectedLocation == ''){
//		$selectedLocation = -1;
//	}
//}
//debug_pr($selectedLocation);
foreach ((array)$selectedStocktakeStatus as $key=>$StocktakeStatus)
{	
	if($StocktakeStatus=='FOUND')
	{
		//$foundResult = $libms->GET_STOCKTAKE_REPORT_STOCKTAKE_LOG_SQL($stocktakeSchemeID,$selectedStocktakeStatus,$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $returnType='array');
		
	    $foundResult = $libms->GET_STOCKTAKE_LOG_BOOK_INFO($barcode='',$stocktakeSchemeID, $returnType='Array', $keyword='', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order=true, $selectedBookCategory, $BookCategoryType, '', $getAccountDate=true);
		$result = array_merge($result,(array)$foundResult);
	}
	elseif($StocktakeStatus=='WRITEOFF')
	{
		$writeoffResult = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID,'','',$returnType='Array','',$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $order=true, '', $selectedBookCategory,'','',$BookCategoryType);	
		$result = array_merge($result,(array)$writeoffResult);
	}
	elseif($StocktakeStatus=='NOTTAKE')
	{
		$stocktakeLogArr =  $libms->GET_STOCKTAKE_LOG_BOOK_INFO($barcode='',$stocktakeSchemeID, $returnType='Array',$keyword='',$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $order=false, $selectedBookCategory, $BookCategoryType);
		$stocktakeLogUniqueBookIDArr = array_keys(BuildMultiKeyAssoc($stocktakeLogArr, 'UniqueID'));
		
		$writeoffLogArr = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID,'','',$returnType='Array','',$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $order=false, '', $selectedBookCategory,'','',$BookCategoryType);	
		$writeoffLogUniqueBookIDArr = array_keys(BuildMultiKeyAssoc($writeoffLogArr, 'UniqueID'));
		
		$excludedUniqueBookIDArr = '';
		$excludedUniqueBookIDArr = array_merge((array)$stocktakeLogUniqueBookIDArr, (array)$writeoffLogUniqueBookIDArr);
		
		$nottakeResult = $libms->GET_NOT_STOCKTAKE_BOOK_INFO($excludedUniqueBookIDArr,'',$resourcesTypeCode,$selectedCirculationType,$selectedLocation,$returnType='array',$order=true, $selectedBookCategory, $thisStockTakeSchemeDisplayArr['RecordEndDate'],'',$BookCategoryType, false,false,$getAccountDate=true);
		$result = array_merge($result,(array)$nottakeResult);
	}	
}


$displayTable = '';
$displayTable .= '<table class="common_table_list view_table_list" width="100%" align="center"  border="0" cellSpacing="0" cellPadding="4">';
$displayTable .= '<thead>';
$displayTable .= '<tr>';
	$displayTable .= '<th width="1%" class="tabletop tabletopnolink">';
	$displayTable .= '#';
	$displayTable .= '</th>';
	$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["report"]["ACNO"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['book']['account_date'];
	$displayTable .= '</th>';
	$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["barcode"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="15%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["title"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['book']['ISBN'];
	$displayTable .= '</th>';
	$displayTable .= '<th width="9%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["call_number"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="9%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author'];
	$displayTable .= '</th>';
	$displayTable .= '<th width="9%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["publisher"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["publish_year"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="2%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["edition"] ;
	$displayTable .= '</th>';
	$displayTable .= '<th width="6%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['bookmanagement']['location'];
	$displayTable .= '</th>';
	$displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["list_price"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["book"]["purchase_price"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["report"]["book_status"];
	$displayTable .= '</th>';
	$displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang['libms']['bookmanagement']['StocktakeStatus'];
	$displayTable .= '</th>';
	if($writeoffResult[0]){
		$displayTable .= '<th width="6%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang['libms']['bookmanagement']['WriteOffDate'];
		$displayTable .= '</th>';
		$displayTable .= '<th width="6%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang['libms']['bookmanagement']['Reason'];
		$displayTable .= '</th>';
	}
$displayTable .= '</tr>';
$displayTable .= '</thead>';
$displayTable .= '<tbody>';
$i=0;
foreach($result as $resultKey=>$resultInfo)
{
	$i++;
	$thisBookCode = $resultInfo['BookCode']?$resultInfo['BookCode']:$Lang['General']['EmptySymbol'];
	$thisBookTitle = $resultInfo['BookTitle']?$resultInfo['BookTitle']:$Lang['General']['EmptySymbol'];
	$thisBookISBN = $resultInfo['ISBN']?$resultInfo['ISBN']:$Lang['General']['EmptySymbol'];
	$thisBookCallNum = $resultInfo['CallNum'] . " " .$resultInfo['CallNum2'];
	$thisBookAuthor = $resultInfo['ResponsibilityBy1']?$resultInfo['ResponsibilityBy1']:$Lang['General']['EmptySymbol'];
	$thisBookPublishYear = $resultInfo['PublishYear']?$resultInfo['PublishYear']:$Lang['General']['EmptySymbol'];
	$thisBookPublisher = $resultInfo['Publisher']?$resultInfo['Publisher']:$Lang['General']['EmptySymbol'];
	$thisBookEdition = $resultInfo['Edition']?$resultInfo['Edition']:$Lang['General']['EmptySymbol'];
	$thisLocationName = $resultInfo['LocationName']?$resultInfo['LocationName']:$Lang['General']['EmptySymbol'];
	$thisBookListPrice = $resultInfo['ListPrice']?$resultInfo['ListPrice']:$Lang['General']['EmptySymbol'];
	$thisBookPurchasePrice = $resultInfo['PurchasePrice']?$resultInfo['PurchasePrice']:$Lang['General']['EmptySymbol'];
	$thisResult = $resultInfo['Result'];
	$thisBarcode = $resultInfo['BarCode']?$resultInfo['BarCode']:$Lang['General']['EmptySymbol'];
	$thisRecordStatus = $resultInfo['RecordStatus'];
	$thisBookAccountDate = $resultInfo['AccountDate'];
//	$thisBookAccountDate = $resultInfo['AccountDate']?$resultInfo['AccountDate']:$Lang['General']['EmptySymbol'];
	
	if($writeoffResult[0]){
		$thisReason = $resultInfo['Reason']?$resultInfo['Reason']:$Lang['General']['EmptySymbol'];
		$thisWriteOffDate = $resultInfo['WriteOffDate']?$resultInfo['WriteOffDate']:$Lang['General']['EmptySymbol'];
	}

	$displayTable .= '<tr>';	
		$displayTable .= '<td>';
		$displayTable .= $i ;
		$displayTable .= '</td>';	
		$displayTable .= '<td>';
		$displayTable .= $thisBookCode;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookAccountDate;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBarcode;
		$displayTable .= '</td>';	
		$displayTable .= '<td>';
		$displayTable .= $thisBookTitle;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookISBN;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookCallNum;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookAuthor;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookPublisher;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookPublishYear;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookEdition;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisLocationName;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookListPrice;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisBookPurchasePrice;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisRecordStatus;
		$displayTable .= '</td>';
		$displayTable .= '<td>';
		$displayTable .= $thisResult;
		$displayTable .= '</td>';
		if($writeoffResult[0]){
			$displayTable .= '<td>';
			$displayTable .= $thisWriteOffDate;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisReason;
			$displayTable .= '</td>';
		}
	$displayTable .= '</tr>';
}
$displayTable .= '</tbody>';
$displayTable .= '</table>';

//Create the summary of stocktake [start]
$displaySummary = '';
$displaySummary .= "<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
//$displaySummary .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang["libms"]["report"]["total"]."</td>";
//$displaySummary .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".count($result)."</td></tr>";
if(count($foundResult)){
	$displaySummary .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang['libms']['stocktake_status']['FOUND']."</td>";
	$displaySummary .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".count($foundResult)."</td></tr>";
}
if(count($writeoffResult)){
	$displaySummary .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang['libms']['stocktake_status']['WRITEOFF']."</td>";
	$displaySummary .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".count($writeoffResult)."</td></tr>";
}
if(count($nottakeResult)){
	$displaySummary .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang['libms']['stocktake_status']['NOTTAKE']."</td>";
	$displaySummary .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".count($nottakeResult)."</td></tr>";
}
$displaySummary .= "</table><br/>";
//Create the summary of stocktake [end]
?>

<script language="javascript">
$(document).ready(function(){	
	$('#spanShowOption').show();
	
	$('input[name="BookCategoryType"]').change(function(){
		if($('#BookCategoryTypeAll').is(':checked'))
		{
			$('#table_CategoryCode1').attr('style','display:none');
			$('#table_CategoryCode2').attr('style','display:none');
			$('#BookCategory option').attr('selected', false);
			$('#BookCategory2 option').attr('selected', false);
		}
		else if ($('#BookCategoryType1').is(':checked'))
		{				
			$('#table_CategoryCode1').attr('style','display:block');
			$('#table_CategoryCode2').attr('style','display:none');
			$('#BookCategory2 option').attr('selected', false);
		}
		else if ($('#BookCategoryType2').is(':checked'))
		{
			$('#table_CategoryCode1').attr('style','display:none');
			$('#table_CategoryCode2').attr('style','display:block');
			$('#BookCategory option').attr('selected', false);
		}
	});
	
	$('#BookCategory option').click(function(){
		$('#BookCategory2 option').attr('selected', false);
	});	
	
	$('#BookCategory2 option').click(function(){
		$('#BookCategory option').attr('selected', false);
	});
});

function hideOptionLayer()
{
	$('#formContent').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	//document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('#formContent').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	//document.getElementById('div_form').className = 'report_option report_show_option';
}

function Js_Select_All(id){
	$('#'+id + ' option').attr('selected', 'selected');	
}

function Js_Check_Form(obj){
	var error = 0 ;
	var stocktakeLength = $('#stocktakeStatus option:selected').length;
//	var resourcesTypeCodeLength = $('#ResourcesTypeCode option:selected').length;
//	var circulationTypeLength = $('#CirculationType option:selected').length;
//	var locationLength = $('#Location option:selected').length;
	
	if(stocktakeLength==0){
		alert('<?=$i_alert_pleaseselect.$Lang['libms']['bookmanagement']['stocktakeRecord']?>');
		error++;
	}
//	else if(resourcesTypeCodeLength==0)
//	{		 
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["resources_type"]?>');
//		error++;
//	}
//	else if(circulationTypeLength==0)
//	{
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["circulation_type"]?>');
//		error++;
//	}
//	else if(locationLength==0)
//	{
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["book_location"]?>');		
//		error++;
//	}
//	
	if ($('#BookCategoryType1').is(':checked')) {
		if($('#BookCategory option:checked').length == 0) {
			$('#BookCategory option').attr('selected', true);	
		}						
	}
	if ($('#BookCategoryType2').is(':checked')) {
		if($('#BookCategory2 option:checked').length == 0) {
			$('#BookCategory2 option').attr('selected', true);	
		}						
	}

	if(error==0)
	{
		obj.submit();
	}
}

function click_export()
{
	document.form1.action = "export.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
	document.form1.action = "result.php";
	document.form1.target="_self";
}

function click_print()
{
	document.form1.action="print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action = "result.php";
	document.form1.target="_self";
}	
</script>

<form name="form1" method="post" action="result.php">
<!--###### Content Board Start ######-->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td width="13" >&nbsp;</td>
  <td class="main_content"><!---->
 	<div class="report_option report_show_option">
 			<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>
 	</div>

	<span id="formContent" style="display:none" >
		<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td width="13">&nbsp;</td>
			<td class="main_content">
	       	<div class="report_option report_option_title"></div>
			<div class="table_board">
			<div class="table_board">
			<table class="form_table">
		        <tr>
		          <td class="field_title"><?=$Lang['libms']['bookmanagement']['stocktakeRecord']?></td>
		          <td><?=$stockTakeSelectionBox?> </td>
		        </tr>
		        <col class="field_title" />
		        <col  class="field_c" />
		        <tr>
		          <td class="field_title"><?=$Lang['libms']['bookmanagement']['StocktakeStatus']?></td>
		          <td>
		           <?=$stockTakeStatusSelectionBoxDiv?>
		          </td>
		        </tr>
		        <tr class="form_table ">
		          <td class="field_title"><?=$Lang["libms"]["settings"]["book_location"]?></td>
		          <td><span class="row_content">
		               <?=$locationSelectionBoxDiv?>
		          </td>
		        </tr>
				<tr class="form_table ">
		    		<td class="field_title"><?=$Lang["libms"]["settings"]["resources_type"]?></td>
		          	<td>
					<span class="row_content">					
					<?=$resourcesTypeCodeSelectionBoxDiv?>
					</td>
				</tr>			
		        <tr class="form_table ">
		          <td class="field_title"><?=$Lang["libms"]["settings"]["circulation_type"]?></td>
		          <td>
					<span class="row_content">
					<?=$circulationTypeSelectionBoxDiv?>
				  </td>
		        </tr>
		        <tr class="form_table ">
		          <td class="field_title"><?=$Lang['libms']['report']['book_category']?></td>
		          <td><span class="row_content">
		           <table class="inside_form_table">
						<tr>
							<td valign="top">
								<?
								$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
								foreach ($result as $row){
									$book_category_name[] = htmlspecialchars($row['value']);
								}
								?>
								<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", 0, ($BookCategoryType==0?true:false), "", $Lang["libms"]["report"]["fine_status_all"])?>								
								<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, ($BookCategoryType==1?true:false), "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
								<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, ($BookCategoryType==2?true:false), "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode1" style='<?=($BookCategoryType==1?'':'display:none;')?>'>
							<td valign="top">
								<?=$bookCategorySelectionBoxDiv?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode2" style='<?=($BookCategoryType==2?'':'display:none;')?>'>
							<td valign="top">
								<?=$bookCategorySelectionBoxDiv2?>
							</td>
						</tr>
					</table>
		          </td>
		        </tr>
		     </table>
	     	<p class="spacer"></p>
	    	</div>
	    	<p class="spacer"></p>
	      	</div>
	      	<div class="edit_bottom">
	      
	        <p class="spacer"></p>
	        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
	        <p class="spacer"></p>
	      </div>
	      <p>&nbsp;</p></td>
	      <td width="11">&nbsp;</td>
	    </tr>
	  </table>
	</span>

	<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
		<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
		</div>
		<br style="clear:both" />
	</div>

   	<div class="table_board">
   	<!-- Create the summary of stocktake [start] -->
   		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$linterface->GET_NAVIGATION2($Lang['libms']['reporting']['stocktakeSummary']);?>
				<p class="spacer"></p>
			    <p class="spacer"></p>
			    </td>
				
			</tr>
		</table>
		
		<?=$displaySummary?>
		
   	<!-- Create the summary of stocktake [end] -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$linterface->GET_NAVIGATION2($Lang['libms']['reporting']['stocktakeReport']);?>
				<p class="spacer"></p>
			    <p class="spacer"></p>
			    </td>
				<td valign="bottom"><div align="right"><?=$Lang['General']['Date'] . ': ' .$thisStockTakeSchemeDisplay?></div></td>
			</tr>
		</table> 					    
		
		<?=$displayTable?>
				
		<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="button" type="button" class="formbutton" onclick="window.location='index.php'" value="<?=$Lang['Btn']['Back']?>" /> 	
		<p class="spacer"></p>
  </div>
  <p>&nbsp;</p></td>
  <td width="11" >&nbsp;</td>
</tr>
</table>
<!--###### Content Board End ######-->
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
