<?php
// using:

/**
 * *************************************
 * Date: 2018-05-03 (Cameron)
 * - retrieve AccountDate(CreationDate) [case #E89629]
 *
 * Date: 2016-04-16 (Henry)
 * Details: add columns List Price and Purchase Price
 *
 * Date: 2015-01-23 (Henry)
 * Details: Added Category2 filter
 *
 * Date: 2015-01-15 (Henry)
 * Details: increased the memory_limit and added SET_TIME_LIMIT
 *
 * Date: 2014-02-13 (Henry)
 * Details: Added filter Book Category
 *
 * Date: 2013-11-25 (Henry)
 * Details: Added column Write-off Date and Reason
 *
 * Date: 2013-06-19 (Rita)
 * Details: Amend access right checking
 *
 * Date: 2013-06-04 (Rita)
 * Details: Create this page
 * *************************************
 */
@SET_TIME_LIMIT(216000);
ini_set('memory_limit', '500M');

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "";

if (! $_SESSION['LIBMS']['admin']['current_right']['reports']['stock-take report'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$lexport = new libexporttext();
$libms = new liblms();
$filename = "stocktake_report.csv";
$ExportArr = array();

// # Obtain POST Value
$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
$selectedStocktakeStatus = $_POST['selectedStocktakeStatus'];
$resourcesTypeCode = $_POST['selectedResourcesTypeCode'];
$selectedCirculationType = $_POST['selectedCirculationType'];
$selectedLocation = $_POST['selectedLocation'];
$selectedBookCategory = $_POST['selectedBookCategory'];

// Stocktake Date Display
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('', 1);
$stockTakeSchemeArr = $libms->returnArray($sql);
$stockTakeSchemeIDArr = BuildMultiKeyAssoc($stockTakeSchemeArr, 'StocktakeSchemeID');
$thisStockTakeSchemeDisplayArr = $stockTakeSchemeIDArr[$stocktakeSchemeID];
$thisStockTakeSchemeDisplay = $thisStockTakeSchemeDisplayArr['StartDate'] . ' ' . $Lang['General']['To'] . ' ' . $thisStockTakeSchemeDisplayArr['EndDate'];

// Define Column Title
$exportColumn = array();
$schoolName = (isset($junior_mck)) ? iconv("big5", "utf-8//IGNORE", $_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
$exportColumn[0][] = $schoolName;
$exportColumn[1][] = $Lang['libms']['reporting']['stocktakeReport'];
$exportColumn[2][] = $Lang['General']['Date'] . ':';
$exportColumn[2][] = $thisStockTakeSchemeDisplay;

$result = array();
$foundResult = array();
$writeoffResult = array();
$nottakeResult = array();

foreach ((array) $selectedStocktakeStatus as $key => $StocktakeStatus) {
    if ($StocktakeStatus == 'FOUND') {
        // $foundResult = $libms->GET_STOCKTAKE_REPORT_STOCKTAKE_LOG_SQL($stocktakeSchemeID,$selectedStocktakeStatus,$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $returnType='array');
        
        $foundResult = $libms->GET_STOCKTAKE_LOG_BOOK_INFO($barcode = '', $stocktakeSchemeID, $returnType = 'Array', $keyword = '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order = true, $selectedBookCategory, $BookCategoryType, '', $getAccountDate = true);
        $result = array_merge($result, (array) $foundResult);
    } elseif ($StocktakeStatus == 'WRITEOFF') {
        $writeoffResult = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', $returnType = 'Array', '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order = true, '', $selectedBookCategory, '', '', $BookCategoryType);
        $result = array_merge($result, (array) $writeoffResult);
    } elseif ($StocktakeStatus == 'NOTTAKE') {
        $stocktakeLogArr = $libms->GET_STOCKTAKE_LOG_BOOK_INFO($barcode = '', $stocktakeSchemeID, $returnType = 'Array', $keyword = '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order = false, $selectedBookCategory, $BookCategoryType);
        $stocktakeLogUniqueBookIDArr = array_keys(BuildMultiKeyAssoc($stocktakeLogArr, 'UniqueID'));
        
        $writeoffLogArr = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', $returnType = 'Array', '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order = false, '', $selectedBookCategory, '', '', $BookCategoryType);
        $writeoffLogUniqueBookIDArr = array_keys(BuildMultiKeyAssoc($writeoffLogArr, 'UniqueID'));
        
        $excludedUniqueBookIDArr = '';
        $excludedUniqueBookIDArr = array_merge((array) $stocktakeLogUniqueBookIDArr, (array) $writeoffLogUniqueBookIDArr);
        $nottakeResult = $libms->GET_NOT_STOCKTAKE_BOOK_INFO($excludedUniqueBookIDArr, '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $returnType = 'array', $order = true, $selectedBookCategory, $thisStockTakeSchemeDisplayArr['RecordEndDate'], '', $BookCategoryType, false, false, $getAccountDate = true);
        $result = array_merge($result, (array) $nottakeResult);
    }
}

// Create the summary of stocktake [start]
$columnCount = 3;
if (count($foundResult)) {
    $exportColumn[$columnCount][] = $Lang['libms']['stocktake_status']['FOUND'];
    $exportColumn[$columnCount][] = count($foundResult);
    $columnCount ++;
}
if (count($writeoffResult)) {
    $exportColumn[$columnCount][] = $Lang['libms']['stocktake_status']['WRITEOFF'];
    $exportColumn[$columnCount][] = count($writeoffResult);
    $columnCount ++;
}
if (count($nottakeResult)) {
    $exportColumn[$columnCount][] = $Lang['libms']['stocktake_status']['NOTTAKE'];
    $exportColumn[$columnCount][] = count($nottakeResult);
    $columnCount ++;
}
// Create the summary of stocktake [end]

$exportColumn[$columnCount][] = '#';
$exportColumn[$columnCount][] = $Lang["libms"]["report"]["ACNO"];
$exportColumn[$columnCount][] = $Lang['libms']['book']['account_date'];
$exportColumn[$columnCount][] = $Lang["libms"]["book"]["barcode"];
$exportColumn[$columnCount][] = $Lang["libms"]["book"]["title"];

$exportColumn[$columnCount][] = $Lang['libms']['book']['ISBN'];
$exportColumn[$columnCount][] = $Lang["libms"]["book"]["call_number"];
$exportColumn[$columnCount][] = $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author'];
$exportColumn[$columnCount][] = $Lang["libms"]["book"]["publisher"];
$exportColumn[$columnCount][] = $Lang["libms"]["book"]["publish_year"];
$exportColumn[$columnCount][] = $Lang["libms"]["book"]["edition"];

$exportColumn[$columnCount][] = $Lang['libms']['bookmanagement']['location'];
$exportColumn[$columnCount][] = $Lang['libms']["book"]['list_price'];
$exportColumn[$columnCount][] = $Lang['libms']["book"]['purchase_price'];
$exportColumn[$columnCount][] = $Lang["libms"]["report"]["book_status"];
$exportColumn[$columnCount][] = $Lang['libms']['bookmanagement']['StocktakeStatus'];

if ($writeoffResult[0]) {
    $exportColumn[$columnCount][] = $Lang['libms']['bookmanagement']['WriteOffDate'];
    $exportColumn[$columnCount][] = $Lang['libms']['bookmanagement']['Reason'];
}

$numOfResult = count($result);
$counter = 0;
for ($i = 0; $i < $numOfResult; $i ++) {
    $resultInfo = $result[$i];
    $thisBookCode = $result[$i]['BookCode'] ? $result[$i]['BookCode'] : $Lang['General']['EmptySymbol'];
    $thisBookTitle = $result[$i]['BookTitle'] ? $result[$i]['BookTitle'] : $Lang['General']['EmptySymbol'];
    $thisLocationName = $result[$i]['LocationName'] ? $result[$i]['LocationName'] : $Lang['General']['EmptySymbol'];
    $thisResult = $result[$i]['Result'];
    $thisBookISBN = $resultInfo['ISBN'] ? $resultInfo['ISBN'] : $Lang['General']['EmptySymbol'];
    $thisBookCallNum = $resultInfo['CallNum'] . " " . $resultInfo['CallNum2'];
    $thisBookAuthor = $resultInfo['ResponsibilityBy1'] ? $resultInfo['ResponsibilityBy1'] : $Lang['General']['EmptySymbol'];
    $thisBookPublishYear = $resultInfo['PublishYear'] ? $resultInfo['PublishYear'] : $Lang['General']['EmptySymbol'];
    $thisBookPublisher = $resultInfo['Publisher'] ? $resultInfo['Publisher'] : $Lang['General']['EmptySymbol'];
    $thisBookEdition = $resultInfo['Edition'] ? $resultInfo['Edition'] : $Lang['General']['EmptySymbol'];
    $thisBarcode = $result[$i]['BarCode'] ? $result[$i]['BarCode'] : $Lang['General']['EmptySymbol'];
    $thisBookListPrice = $resultInfo['ListPrice'] ? $resultInfo['ListPrice'] : $Lang['General']['EmptySymbol'];
    $thisBookPurchasePrice = $resultInfo['PurchasePrice'] ? $resultInfo['PurchasePrice'] : $Lang['General']['EmptySymbol'];
    $thisRecordStatus = $resultInfo['RecordStatus'];
    $thisBookAccountDate = $resultInfo['AccountDate'];
    
    if ($writeoffResult[0]) {
        $thisReason = $resultInfo['Reason'] ? $resultInfo['Reason'] : $Lang['General']['EmptySymbol'];
        $thisWriteOffDate = $resultInfo['WriteOffDate'] ? $resultInfo['WriteOffDate'] : $Lang['General']['EmptySymbol'];
    }
    
    $counter ++;
    $ExportArr = array();
    $ExportArr[$i][] = $counter;
    $ExportArr[$i][] = $thisBookCode;
    $ExportArr[$i][] = $thisBookAccountDate;
    $ExportArr[$i][] = $thisBarcode;
    $ExportArr[$i][] = $thisBookTitle;
    $ExportArr[$i][] = $thisBookISBN;
    $ExportArr[$i][] = $thisBookCallNum;
    $ExportArr[$i][] = $thisBookAuthor;
    $ExportArr[$i][] = $thisBookPublisher;
    $ExportArr[$i][] = $thisBookPublishYear;
    $ExportArr[$i][] = $thisBookEdition;
    $ExportArr[$i][] = $thisLocationName;
    $ExportArr[$i][] = $thisBookListPrice;
    $ExportArr[$i][] = $thisBookPurchasePrice;
    $ExportArr[$i][] = $thisRecordStatus;
    $ExportArr[$i][] = $thisResult;
    if ($writeoffResult[0]) {
        $ExportArr[$i][] = $thisWriteOffDate;
        $ExportArr[$i][] = $thisReason;
    }
    $exportData = array();
    $exportData[] = $ExportArr[$i];
    $export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportData, $exportColumn, "\t", "\r\n", "\t", 0, "11");
    $export_content .= "\r\n";
    $exportColumn = array();
}

// $export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

// Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode = "UTF-8");

?>