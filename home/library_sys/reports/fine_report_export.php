<?php
# using: 

/***************************************
 *
 * Date:    2019-03-27 (Cameron)
 * Details: change column title from paid to paid_or_waive
 * 
 * Date:    2019-03-20 (Cameron)
 * Details: fix column name from fine_handle_date to fine_handle_date_or_last_updated [ej5.0.9.3.1] 
 * 
 * Date:    2018-10-03 (Cameron)
 * Details: add column Payment Left [case #T139412]
 * 
 * Date:	2016-09-23 (Cameron)
 * Details:	add column Payment Amended		
 * 
 * Date: 	2013-10-08 (Yuen)
 * Details: Create this page
 ***************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['overdue report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$libms = new liblms();
$filename = "fine_report.csv";

# Define Column Title
$exportColumn = array();
$exportColumn[0][] = $Lang["libms"]["book"]["title"];
$exportColumn[0][] = $Lang["libms"]["CirculationManagement"]["overdue_day"];
$exportColumn[0][] = $Lang["libms"]["report"]["fine_date"];
$exportColumn[0][] = $Lang["libms"]["CirculationManagement"]["payment"]." (\$)";
$exportColumn[0][] = $Lang["libms"]["report"]["fine_status"];
$exportColumn[0][] = $Lang["libms"]["CirculationManagement"]["paid_or_waive"]." (\$)";
$exportColumn[0][] = $Lang["libms"]["CirculationManagement"]["left"]." (\$)";
$exportColumn[0][] = $Lang["libms"]["book"]["user_name"];
$exportColumn[0][] = $Lang["libms"]["report"]["ClassName"];
$exportColumn[0][] = $Lang["libms"]["report"]["ClassNumber"];
$exportColumn[0][] = $Lang["libms"]["report"]["fine_handle_date_or_last_updated"];
$exportColumn[0][] = $Lang["libms"]["report"]["handled_by"];
$exportColumn[0][] = $Lang["libms"]["report"]["fine_amended"];
			
			

$ExportArr = $libms->get_fine_report($RecordType, $RecordStatus, $RecordDate, $StartDate, $EndDate, $FindBy, $groupTarget, $rankTarget, $classnames,$studentIDs, "EXPORT", $RecordPeriod);

$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="UTF-8");

?>