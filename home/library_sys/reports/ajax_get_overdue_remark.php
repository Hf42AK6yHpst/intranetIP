<?php
/*
 * 	Log
 * 	Purpose:	get overdue remark according to template ID
 * 	Date:	2015-02-16 [Cameron] create this file
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$historyRemark = trim($_POST['HistoryRemark']);
$libms = new liblms();

if ($historyRemark) {	
	$sql = "SELECT 
					Remark 
			FROM
					LIBMS_OVERDUE_REMARK
			WHERE 
					LogID='".$historyRemark."'";
			
	$rs = $libms->returnArray($sql);
	if (count($rs) > 0) {	// record exist
		echo html_entity_decode($rs[0]['Remark'],ENT_QUOTES,"UTF-8");		
	}
	else {
		echo '';
	}
}
else {
	echo '';
}
intranet_closedb();
?>