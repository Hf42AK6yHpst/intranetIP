<?php
/*
 * 	Log
 *	Date:	2019-07-23 [Henry]
 *	- use the local library of jquery
 * 	Purpose:	interface for input overdue remark
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


$historyRemarkID = $_REQUEST['HistoryRemarkID'];
$historyRemarkID = ($historyRemarkID == 'undefined') ? '' : $historyRemarkID;
$overdueRemark = $_REQUEST['OverdueRemark'];
$overdueRemark = ($overdueRemark == 'undefined') ? '' : $overdueRemark;

$linterface = new interface_html();


$libms = new liblms();

$sql = "SELECT 
				LogID as ID,
				Remark as Name
		FROM
				LIBMS_OVERDUE_REMARK
		ORDER BY 
				DateModified DESC 
				LIMIT 20
		";
$rs = $libms->returnArray($sql);
$nrRs = count($rs);
if ($nrRs >0) {
	for ($i=0; $i<$nrRs; $i++) {
		$rs[$i][1] = mb_substr(html_entity_decode($rs[$i]['Name'],ENT_QUOTES,"UTF-8"),0,40,"UTF-8");	// use numeric array because list uses it
		$rs[$i][1] = str_replace("<","&lt;",$rs[$i][1]);		// all other special characters except "<" and ">" can show in innerhtml of <option></option>
		$rs[$i][1] = str_replace(">","&gt;",$rs[$i][1]);		// therefore, need to convert it back to html code for these two characters
	}
}
$historyRemark = $rs;

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<div id="thickboxContainerDiv" class="edit_pop_board">
	<form id="thickboxForm" name="thickboxForm">
		<div id="thickboxContentDiv" class="edit_pop_board_write">
			<table borde=0>
				<tr>
					<td class="field_title"><?=$Lang["libms"]["report"]["overdue_remark"]["history"]?></td>
					<td>
						<?=$linterface->GET_SELECTION_BOX($historyRemark, $ParTags='id="HistoryRemark"', $ParDefault=$Lang["libms"]["report"]["overdue_remark"]["please_select"], $ParSelected=$historyRemarkID)?>
					</td>
				</tr>		
		
				<tr>
					<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang["libms"]["report"]["overdue_remark"]["remark"]?></td>
					<td>
						<textarea class="tabletext requiredField" name="OverdueRemark" ID="OverdueRemark" cols="80" rows="10" wrap="virtual" onFocus="this.rows=10;"><?=$overdueRemark?></textarea>						
						<?=$linterface->Get_Form_Warning_Msg('textareaEmptyWarnDiv', $Lang["libms"]["report"]["overdue_remark"]["mandatory"], $Class='warnMsgDiv')?>
					</td>
				</tr>		
			</table>
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</form>
</div>

<script src="/templates/jquery/jquery-1.6.1.min.js" type="text/javascript" charset="utf-8"></script>
<script>
	function checkMandatoryField() {
		var ret = true;
		$(".requiredField").each (function() {
			if ($.trim($(this).val()) == '') {
				$(this).next().css('display','block');
				ret = false;
			}
		});
		return ret;
	}
	
	$(document).ready(function() {
		var ret;
		var rs= [];

		$(".warnMsgDiv").css('display','none');
		$("#submitBtn_tb").click(function(){
			ret = checkMandatoryField();
			if (ret) {
				$.ajax({
					type: "POST",
					async: false,				
					url: 'ajax_save_overdue_remark.php',
					data:$('#thickboxForm').serialize(),
						timeout:1000,
						success:function( json ){
							rs = jQuery.parseJSON(json);
							if ( rs['OverdueRemarkID'] != undefined ) {				
								$('.overdueRemarkContent').html(rs['OverdueRemark']);			
								$('#CurrHistoryRemarkID').val(rs['OverdueRemarkID']);
								$('#CurrOverdueRemark').val($("#OverdueRemark").val());
								$('.overdueRemark').css('display','table-row');
								js_Hide_ThickBox();
							}
						},
						error:function( msg ){
							alert('<?=$Lang["libms"]["report"]["overdue_remark"]["server_error"]?>');
							return false;
						}
				});
			}
		});	
		
		$("#HistoryRemark").click(function(){
			if ($("#HistoryRemark option:selected").val() != '') {
				$.ajax({
					type: "POST",
					async: false,				
					url: 'ajax_get_overdue_remark.php',
					data: {'HistoryRemark':$("#HistoryRemark option:selected").val()},
						timeout:1000,
						success:function( msg ){
							$('#OverdueRemark').val(msg);
						},
						error:function( msg ){
							alert('<?=$Lang["libms"]["report"]["overdue_remark"]["server_error"]?>');
							return false;
						}
				});
			}
		});
	});
</script>

<?php
intranet_closedb();
?>