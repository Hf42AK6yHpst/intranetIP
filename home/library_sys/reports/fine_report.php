<?php

// Using: 

/***************************************
 * 
 * Date:	2019-07-23 [Henry]
 * Details:	use the local library of jquery
 * 
 * Date: 2016-07-15 [Cameron]
 * 			- hide the warning and empty option list if it returns nothing in ajaxSelectbox (IP2.5.7.7.1)
 * 			- don't allow to run report if user do not select target and penalty date
 * 
 * Date: 2014-04-09 (Tiffany)
 * Details: Add Sort by add Target.
 *          Add JS function ajaxSelectbox(),student_list(),target_switch(),rankTarget_switch(),studentIDs_switch(),$(document).ready
 * 
 * Date: 2013-10-08 (Yuen)
 * Details: Create this page
 ***************************************/
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once('Reports.php');
include_once('ajaxView.php');

intranet_auth();
intranet_opendb();

$libms = new liblms();
$reports = new Reports();
		
if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['overdue report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang["libms"]["reporting"]["PageReportingFine"]);
$CurrentPage = "PageReportingFine";

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

################### Stocktake Scheme Selection Box Start ######################

$linterface->LAYOUT_START();

if ($RecordStatus=="")
{
	$RecordStatusSet["ALL"] = true;
} else
{
	$RecordStatusSet[$RecordStatus] = true;
}

if ($RecordType=="")
{
	$RecordTypeSet["ALL"] = true;
} else
{
	$RecordTypeSet[$RecordType] = true;
}

if ($RecordDate=="")
{
    $RecordStatusSet["FINEDATE"] = true;
} else
{
	$RecordStatusSet[$RecordDate] = true;
}

if ($FindBy=="")
{
    $RecordStatusSet["Group"] = true;
} else
{
	$RecordStatusSet[$FindBy] = true;
}


if ($RecordType!="" && $RecordStatus!="" && $RecordDate!="" && $StartDate!="" && $EndDate!=""&&$FindBy!="")
{
	$displayTable = $libms->get_fine_report($RecordType, $RecordStatus, $RecordDate, $StartDate, $EndDate, $FindBy, $groupTarget, $rankTarget, $classnames,$studentIDs, "DISPLAY", $RecordPeriod);
	
	$reportPeriodDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;
	$ShowReport = true;
} else
{
	$ShowReport = false;
}
?>
<script src="/templates/jquery/jquery-1.8.0.min.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">


function Js_Check_Form(obj){

	if (($('#findByGroup').attr('checked')) && (!$("#groupTarget option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_group']?>");
		return;					
	}
	if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'form') && (!$("#classnames option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_form']?>");
		return;					
	}										
	if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'class') && (!$("#classnames option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_class']?>");
		return;					
	}										
	if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'student') && (!$("#studentIDs option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_student']?>");
		return;					
	}

	if (!check_date(document.getElementById("StartDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
		return;
	if (!check_date(document.getElementById("EndDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
		return;
	if (!compare_date(document.getElementById("StartDate"), document.getElementById("EndDate"),"<?=$Lang['General']['WrongDateLogic']?>"))
		return;	
	
	obj.submit();
}

function click_export()
{
	document.form1.action = "fine_report_export.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
	document.form1.action = "fine_report.php";
	document.form1.target="_self";
}

function click_print()
{
	document.form1.action="fine_report_print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action = "fine_report.php";
	document.form1.target="_self";
}	

<?php if ($ShowReport) {?>
$(document).ready(function(){	
	$('#spanShowOption').show();	
});
<?php } ?>

function hideOptionLayer()
{
	$('#formContent').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
}

function showOptionLayer()
{
	$('#formContent').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
}

//----------Tiffany Added-----------------

function ajaxSelectbox(page, fieldName){
	$.get(page, {},
		function(insertRespond){
			if (insertRespond == ''){
//				alert('Ajax call return nothing. Please try again. If problem persist, make sure the setting is completed / contact your administrator');
				fieldName.html('');
			}else {
				fieldName.html(insertRespond);
			}
		}
	);
}

function student_list(){
	if ($('#rankTarget').val() == 'student')
	{
		page = "TargetList.php?function=getStudentByClass&classNames="+$('#classnames :selected').text();
		ajaxSelectbox(page, $('#studentIDs'));
		$('#studentIDs option').attr('selected', true);
	}
}

function target_switch(){
		
	if($('#findByGroup').attr("checked")){
		$('#findBy_group').show();
		$('#findBy_rank').hide();
	}
	if($('#findByStudent').attr("checked")){
		$('#findBy_group').hide();
		$('#findBy_rank').show();
	}
	
}

function rankTarget_switch(){
	
   if($('#rankTarget').val()=="form"){
    	page = "TargetList.php?function=getClassLevel";
	    ajaxSelectbox(page, $('#classnames'));
		$('#classnames').attr('multiple',true);
		$('#classnames option').attr('selected', true);
		$('#selectAllBtnClassnames').attr('style','display:inline');
		$('#Student').attr('style','display:none');	
    }
  
   if($('#rankTarget').val()=="class"){
        page= "TargetList.php?function=getClassNameByLevel";
		ajaxSelectbox(page, $('#classnames'));
		$('#classnames').attr('multiple',true);
		$('#classnames option').attr('selected', true);
		$('#selectAllBtnClassnames').attr('style','display:inline');
		$('#Student').attr('style','display:none');	       
    }
    
   if($('#rankTarget').val()=="student"){
		page= "TargetList.php?function=getClassNameByLevel";
		ajaxSelectbox(page, $('#classnames'));
		$('#classnames').attr('multiple',false);
		$("#classname option:first").attr('selected','selected');
		student_list();
		$('#selectAllBtnClassnames').attr('style','display:none');
		$('#Student').attr('style','display:inline');	    
		student_list();
    }
}

function studentIDs_switch(){
    if($('#rankTarget').val()=="student"){	
		page = "TargetList.php?function=getStudentByClass&classNames="+$('#classnames :selected').text();
		ajaxSelectbox(page, $('#studentIDs'));
		$('#studentIDs option').attr('selected', true);
    }
}

$(document).ready(function(){
    $.ajaxSetup({ cache:false,async:false });
			
    ajaxSelectbox('TargetList.php?function=getGroupName',$('#groupTarget'));
    $('#groupTarget option').attr('selected', true);
	ajaxSelectbox('TargetList.php?function=getClassLevel', $('#classnames'));
		        
	<?php 
	if(count($groupTarget)!=0||$rankTarget!=""){
	   $string_groupTarget = implode(",",$groupTarget);
    ?>
       $("#groupTarget").val([<?=$string_groupTarget?>]); 
       $("#rankTarget").val("<?=$rankTarget?>");
    
	<?php 	
	}
	?>		
	

	if($('#findByGroup').attr("checked")){
		$('#findBy_group').show();
		$('#findBy_rank').hide();
	}
	
	if($('#findByStudent').attr("checked")){
		$('#findBy_group').hide();
		$('#findBy_rank').show();
	}
	
    if($('#rankTarget').val()=="form"){
    	page = "TargetList.php?function=getClassLevel";
	    ajaxSelectbox(page, $('#classnames'));
		$('#classnames').attr('multiple',true);
		$('#classnames option').attr('selected', true);
		$('#selectAllBtnClassnames').attr('style','display:inline');
		$('#Student').attr('style','display:none');	
    }
  
   if($('#rankTarget').val()=="class"){
        page= "TargetList.php?function=getClassNameByLevel";
		ajaxSelectbox(page, $('#classnames'));
		$('#classnames').attr('multiple',true);
		$('#classnames option').attr('selected', true);
		$('#selectAllBtnClassnames').attr('style','display:inline');
		$('#Student').attr('style','display:none');	       
    }
    
   if($('#rankTarget').val()=="student"){
		page= "TargetList.php?function=getClassNameByLevel";
		ajaxSelectbox(page, $('#classnames'));
		$('#classnames').attr('multiple',false);
		$("#classname option:first").attr('selected','selected');
		student_list();
		$('#selectAllBtnClassnames').attr('style','display:none');
		$('#Student').attr('style','display:inline');	    
    }
    <?php
      if (($FindBy == 'Student')&&($rankTarget == 'form')&&(!empty($classnames)))
      {
      	for($i=0;$i<count($classnames);$i++){
      		$classnames[$i]='"'.$classnames[$i].'"';
      	}       	
      	$string_classnames  = implode(",",$classnames);   
    ?>
        $("#classnames").val([<?=$string_classnames?>]); 
        
    <?php
      }     

      if (($FindBy == 'Student')&&($rankTarget == 'class')&&(!empty($classnames)))
      {
      	for($i=0;$i<count($classnames);$i++){
      		$classnames[$i]='"'.$classnames[$i].'"';
      	}       	
      	$string_classnames  = implode(",",$classnames);   
    ?>
        $("#classnames").val([<?=$string_classnames?>]); 
        
    <?php
      }     
      
      if (($FindBy == 'Student')&&($rankTarget == 'student')&&(!empty($classnames)))
      { 
      	for($i=0;$i<count($classnames);$i++){
      		$classnames[$i]='"'.$classnames[$i].'"';
      	}       	
      	$string_classnames  = implode(",",$classnames);   
      	$string_studentIDs  = implode(",",$studentIDs);   
    ?>
        $("#classnames").val([<?=$string_classnames?>]); 
        student_list();
        $("#studentIDs").val([<?=$string_studentIDs?>]); 
        
    <?php
      }     
    ?>
    
	$('#selectAllBtnClassnames').click(function(){ 			$('#classnames option').attr('selected', true);  		});
	$('#selectAllBtnStudent').click(function(){ 			$('#studentIDs option').attr('selected', true);  		});
	$('#selectAllBtnGroups').click(function(){ 			$('#groupTarget option').attr('selected', true);  		});

});
</script>


<form name="form1" method="post" action="fine_report.php">

<?php if ($ShowReport) { ?>

<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td width="13" >&nbsp;</td>
  <td class="main_content"><!---->
 	<div class="report_option report_show_option">
 			<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>
 	</div>

	<span id="formContent" style="display:none" >
		<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td width="13">&nbsp;</td>
			<td >
		       	<div class="report_option report_option_title"></div>
				<div class="table_board">
				<table class="form_table">
	                <tr>
			          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['report']['target']?></td>
			          <td>
			          	<table class="inside_form_table">
					      <tr>
					      	<td valign="top">
						       <?=$linterface->Get_Radio_Button("findByGroup", "FindBy", 'Group', $RecordStatusSet["Group"] , "", $Lang['libms']['report']['findByGroup'],"target_switch()")?>
							   <?=$linterface->Get_Radio_Button("findByStudent", "FindBy", 'Student', $RecordStatusSet["Student"] , "", $Lang['libms']['report']['findByStudent'],"target_switch()")?>							
					    	</td>
					      </tr>
				          <tr class="table_findBy" id="findBy_group">
						    <td valign="top">
							   <select name="groupTarget[]" id="groupTarget" multiple size="7">
							      <!-- Get_Select  -->
						       </select>
						       <?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>
						    </td>
					     </tr>
					     <tr class="table_findBy" id="findBy_rank">
						    <td valign="top">
							   <select name="rankTarget" id="rankTarget" onchange="rankTarget_switch();">
								  <option value="form"><?=$Lang['libms']['report']['Form']?></option>
								  <option value="class"><?=$Lang['libms']['report']['Class']?></option>
								  <option value="student"><?=$Lang['libms']['report']['Student']?></option>
							   </select>
						    </td>
						    <td valign="top" nowrap>
							   <!-- Form / Class //-->
							   <span id='classname'>
							      <select name="classnames[]" id="classnames" size="7" multiple onchange="studentIDs_switch();"></select>
							   </span>
							   <?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnClassnames');?>
							   <!-- Student //-->
							   <span id='Student' style='display:none;'>
								  <select name="studentIDs[]" multiple size="7" id="studentIDs"></select> 
								  <?= $linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnStudent')?>
							   </span>							
						    </td>
					     </tr>
					     <tr><td colspan="3" class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></td></tr>
				       </table>
				       <span id='div_Target_err_msg'></span>
			          </td>
		            </tr>				
			        <tr>
			          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_date"]?></td>
			          <td>
			          <?=$linterface->Get_Radio_Button("RecordPeriodFineDate", "RecordPeriod", 'FINEDATE', ($RecordPeriod == "FINEDATE" || !$RecordPeriod?1:0), "", $Lang["libms"]["report"]["fine_date"])?>
			          &nbsp; <?=$linterface->Get_Radio_Button("RecordPeriodFineHandleDate", "RecordPeriod", 'FINEHANDLEDATE', ($RecordPeriod == "FINEHANDLEDATE"?1:0), "", $Lang["libms"]["report"]["fine_handle_date"])?>
			          <br/>
			          <?=$linterface->GET_DATE_PICKER("StartDate", $StartDate)?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span>
			          </td>
			        </tr>
			        <col class="field_title" />
			        <col class="field_c" />
			        <tr>
			          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_type"]?></td>
			          <td>
			          	<?=$linterface->Get_Radio_Button("RecordTypeAll", "RecordType", 'ALL', $RecordTypeSet["ALL"], "", $Lang["libms"]["report"]["fine_status_all"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordTypeOutstanding", "RecordType", 'OVERDUE', $RecordTypeSet["OVERDUE"], "", $Lang["libms"]["report"]["fine_type_overdue"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordTypeSettled", "RecordType", 'LOST', $RecordTypeSet["LOST"], "", $Lang["libms"]["report"]["fine_type_lost"])?>
			          </td>
			        </tr>
			        <tr>
			          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_status"]?></td>
			          <td>
			          	<?=$linterface->Get_Radio_Button("RecordStatusAll", "RecordStatus", 'ALL', $RecordStatusSet["ALL"], "", $Lang["libms"]["report"]["fine_status_all"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusOutstanding", "RecordStatus", 'OUTSTANDING', $RecordStatusSet["OUTSTANDING"], "", $Lang["libms"]["report"]["fine_outstanding"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusSettled", "RecordStatus", 'SETTLED', $RecordStatusSet["SETTLED"], "", $Lang["libms"]["CirculationManagement"]["paied"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusWaived", "RecordStatus", 'WAIVED', $RecordStatusSet["WAIVED"], "", $Lang["libms"]["report"]["fine_waived"])?>
			          </td>
			        </tr>
	                <tr>
	                  <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["sortby"]?></td>
	                  <td>
	                   	<?=$linterface->Get_Radio_Button("RecordStatusFineDate", "RecordDate", 'FINEDATE', $RecordStatusSet["FINEDATE"], "", $Lang["libms"]["report"]["fine_date"])?>
	                   	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusFineHandleDate", "RecordDate", 'FINEHANDLEDATE', $RecordStatusSet["FINEHANDLEDATE"], "", $Lang["libms"]["report"]["fine_handle_date"])?>
	                  </td>
	                </tr>			      
			     </table>
	     <?=$linterface->MandatoryField();?>
			    </div>
		      	<div class="edit_bottom">
		      
		        <p class="spacer"></p>
		        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
		        <p class="spacer"></p>
		      </div>
	      </td>
	      <td width="11">&nbsp;</td>
	    </tr>
	  </table>
	</span>

	<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
		<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
		</div>
		<br style="clear:both" />
	</div>

   	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$linterface->GET_NAVIGATION2($Lang["libms"]["reporting"]["PageReportingFine"]);?>
				<p class="spacer"></p>
			    <p class="spacer"></p>
			    </td>
				<td valign="bottom"><div align="right"><?=$Lang['General']['Date'] . ': ' .$reportPeriodDisplay?></div></td>
			</tr>
		</table> 					    
		
		<?=$displayTable?>
  <p>&nbsp;</p></td>
  <td width="11" >&nbsp;</td>
</tr>
</table>

<?php } else { ?>
	
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="13">&nbsp;</td>
		<td class="main_content">
       	<div class="report_option report_option_title"></div>
		<div class="table_board">
		<table class="form_table">
	        <tr>
			  <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['report']['target']?></td>
			    <td>
			      <table class="inside_form_table">
				    <tr>
					  <td valign="top">
						 <?=$linterface->Get_Radio_Button("findByGroup", "FindBy", 'Group', $RecordStatusSet["Group"] , "", $Lang['libms']['report']['findByGroup'],"target_switch()")?>
						 <?=$linterface->Get_Radio_Button("findByStudent", "FindBy", 'Student', $RecordStatusSet["Student"], "", $Lang['libms']['report']['findByStudent'],"target_switch()")?>							
					  </td>
					</tr>
				    <tr class="table_findBy" id="findBy_group">
					  <td valign="top">
					    <select name="groupTarget[]" id="groupTarget" multiple size="7">
						   <!-- Get_Select  -->
						</select>
						<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>
					  </td>
					</tr>
					<tr class="table_findBy" id="findBy_rank">
					  <td valign="top">
						<select name="rankTarget" id="rankTarget" onchange="rankTarget_switch();">
						   <option value="form" selected><?=$Lang['libms']['report']['Form']?></option>
						   <option value="class"><?=$Lang['libms']['report']['Class']?></option>
						   <option value="student"><?=$Lang['libms']['report']['Student']?></option>
						</select>
					  </td>
					  <td valign="top" nowrap>
					     <!-- Form / Class //-->
					     <span id='classname'>
							<select name="classnames[]" id="classnames" size="7" multiple onchange="studentIDs_switch();"></select>
					     </span>
					     <?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnClassnames');?>
						 <!-- Student //-->
						 <span id='Student' style='display:none;'>
						    <select name="studentIDs[]" multiple size="7" id="studentIDs"></select> 
						    <?= $linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnStudent')?>
					     </span>							
					  </td>
					</tr>
					<tr><td colspan="3" class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></td></tr>
				  </table>
				  <span id='div_Target_err_msg'></span>
			   </td>
		    </tr>			      	        
	        <tr>
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_date"]?></td>
	          <td><?=$linterface->Get_Radio_Button("RecordPeriodFineDate", "RecordPeriod", 'FINEDATE', ($RecordPeriod == "FINEDATE" || !$RecordPeriod?1:0), "", $Lang["libms"]["report"]["fine_date"])?>
			          &nbsp; <?=$linterface->Get_Radio_Button("RecordPeriodFineHandleDate", "RecordPeriod", 'FINEHANDLEDATE', ($RecordPeriod == "FINEHANDLEDATE"?1:0), "", $Lang["libms"]["report"]["fine_handle_date"])?>
			          <br/><?=$linterface->GET_DATE_PICKER("StartDate", $StartDate)?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
	        </tr>
	        <col class="field_title" />
	        <col  class="field_c" />
	        <tr>
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_type"]?></td>
	          <td>
	          	<?=$linterface->Get_Radio_Button("RecordTypeAll", "RecordType", 'ALL', $RecordTypeSet["ALL"], "", $Lang["libms"]["report"]["fine_status_all"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordTypeOutstanding", "RecordType", 'OVERDUE', $RecordTypeSet["OVERDUE"], "", $Lang["libms"]["report"]["fine_type_overdue"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordTypeSettled", "RecordType", 'LOST', $RecordTypeSet["LOST"], "", $Lang["libms"]["report"]["fine_type_lost"])?>
	          </td>
	        </tr>
	        <tr>
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_status"]?></td>
	          <td>
	          	<?=$linterface->Get_Radio_Button("RecordStatusAll", "RecordStatus", 'ALL', $RecordStatusSet["ALL"], "", $Lang["libms"]["report"]["fine_status_all"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusOutstanding", "RecordStatus", 'OUTSTANDING', $RecordStatusSet["OUTSTANDING"], "", $Lang["libms"]["report"]["fine_outstanding"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusSettled", "RecordStatus", 'SETTLED', $RecordStatusSet["SETTLED"], "", $Lang["libms"]["CirculationManagement"]["paied"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusWaived", "RecordStatus", 'WAIVED', $RecordStatusSet["WAIVED"], "", $Lang["libms"]["report"]["fine_waived"])?>
	          </td>
	        </tr>
	        <tr>
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["sortby"]?></td>
	          <td>
	          	<?=$linterface->Get_Radio_Button("RecordStatusFineDate", "RecordDate", 'FINEDATE', $RecordStatusSet["FINEDATE"], "", $Lang["libms"]["report"]["fine_date"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusFineHandleDate", "RecordDate", 'FINEHANDLEDATE', $RecordStatusSet["FINEHANDLEDATE"], "", $Lang["libms"]["report"]["fine_handle_date"])?>
	          </td>
	        </tr>
	     </table>
	     <?=$linterface->MandatoryField();?>
      <p class="spacer"></p>
    </div>
    <p class="spacer"></p>
      </div>
      <div class="edit_bottom">
        <p class="spacer"></p>
        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
        <p class="spacer"></p>
      </div>
      <p>&nbsp;</p></td>
      <td width="11">&nbsp;</td>
    </tr>
  </table>
<?php } ?>
  
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
