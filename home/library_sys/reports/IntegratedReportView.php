<?php
# modifying by:   

/**
 * Log :
 * 
 * Date:	2019-07-23 [Henry]
 *			- use the local library of jquery
 * Date:	2016-09-06 [Cameron]
 * 			fix bug: show book category by category type in getRecord_book_category() [case #K102034]
 * Date:	2016-07-15 [Cameron]
 * 			force to make choice for mandatory fields except Book Category (auto select all if no selection) (IP2.5.7.7.1)
 * Date:	2015-07-21 [Cameron] 
 * 			Add radio option "All" to BookCategory
 * Date: 	2015-03-09 [Ryan] DM#2853
 * 			Fixed export csv dipslay blanks if result is 0
 * Date: 	2015-02-09 [Henry]
 * 			Fixed: can click 'Chart' button when the page is showing chart
 * Date: 	2014-05-14 [Henry]
 * 			Added school name at top of the page
 * Date:	2013-11-20 [Henry]
 * 			Fix bug: show and print the bar chart correctly
 * Date 	2013-05-06 [Cameron]
 * 			Show filter period when print
 * Date:	2013-04-24 [yuen]
 * 			preset date range as current year
 * 
 * Date:	2013-04-23 [Cameron]
 * 			Add date validation function before submit criteria for report: $('#submit_result').click(function()
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

require_once('ReportView.php');
require_once('Exporter.php');

global $Lang;
$linterface = new interface_html("libms.html");

class IntegratedReportView extends ReportView{
	public function __construct(){
		$CurrentPageArr['LIBMS'] = 1;
		$CurrentPage = "PageReportingIntragration";
		parent::__construct($CurrentPage);
	}
	public function getView($viewdata = null){
		$x .= $this->getHeader();
		$x .= $this->getJS($viewdata);
		$x .= $this->getFrom($viewdata);
// 		$x .= $this->getToolbar($viewdata);
// 		$x .= $this->getResult($viewdata);
		$x .= $this->getFooter();
		return $x;
	}
	
	public function getJS($viewdata){
		global $Lang;
		ob_start();
		?>
		
		<link href="css/visualize.css" type="text/css" rel="stylesheet" />
		<link href="css/visualize-light.css" type="text/css" rel="stylesheet" />
		<link href="css/reports.css" type="text/css" rel="stylesheet" />
		
			
		<script src="/templates/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/number_rounding.js"></script>
		<script type="text/javascript" src="js/visualize.jQuery.js"></script>	
		
		<script type="text/javascript">

		$().ready( function(){
			
			$('#selectAllBtnCirCat').click( function(){ 				$('#CirCategory option').attr('selected', true);  		} );
			$('#selectAllBtnSubject').click( function(){ 			$('#subject option').attr('selected', true);			} );
			$('#selectAllBtnCategories').click( function(){ 			$('#Categories option').attr('selected', true);			} );
			$('#selectAllBtnCategories2').click(function(){ 		$('#Categories2 option').attr('selected', true);		});
			
			<?php if(is_null($viewdata['Result'])){ ?>
				$('input[type=checkbox]').attr('checked', true);
				$('#CirCategory option').attr('selected', true);
				$('#subject option').attr('selected', true);	
//				$('#Categories option').attr('selected', true);
			<?php }?>
			
			<?php if (isset($viewdata['Result'])) {?>
				$('#formContent').attr('style','display: none');
			<? }?>
			
			$('input[name="BookCategoryType"]').change(function(){
				if($('#BookCategoryTypeAll').is(':checked'))
				{
					$('#table_CategoryCode1').attr('style','display:none');
					$('#table_CategoryCode2').attr('style','display:none');
					$('#Categories option').attr('selected', false);
					$('#Categories2 option').attr('selected', false);
				}
				else if ($('#BookCategoryType1').is(':checked'))
				{				
					$('#table_CategoryCode1').attr('style','display:block');
					$('#table_CategoryCode2').attr('style','display:none');
					$('#Categories option').attr('selected', true);
					$('#Categories2 option').attr('selected', false);
				}
				else if ($('#BookCategoryType2').is(':checked'))
				{
					$('#table_CategoryCode1').attr('style','display:none');
					$('#table_CategoryCode2').attr('style','display:block');
					$('#Categories option').attr('selected', false);
					$('#Categories2 option').attr('selected', true);
				}
			});
			
			$('#submit_result').click(function(){
				
				if (($('#radioPeriod_Date').attr('checked'))) {
					if (!check_date(document.getElementById("textFromDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!check_date(document.getElementById("textToDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!compare_date(document.getElementById("textFromDate"), document.getElementById("textToDate"),"<?=$Lang['General']['WrongDateLogic']?>"))
						return;	
				}
				else {
					if ( ($('#radioPeriod_Year').attr('checked')) && $("#data\\[AcademicYear\\]").val() == '' ) {					
						alert("<?=$Lang['libms']['report']['msg']['please_select_academic_year']?>");
						return;					
					}										
				}

				if (!$(":input[name^='data[display]']:checked").length) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_display_item']?>");
					return;
				}
				
				if (!$("#CirCategory option:selected").length ) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_circategory']?>");
					return;					
				}

				if (!$("#subject option:selected").length ) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_subject']?>");
					return;					
				}
				
				if ($('#BookCategoryType1').is(':checked')) {
					if($('#Categories option:checked').length == 0) {
						$('#Categories option').attr('selected', true);	
					}						
				}
				if ($('#BookCategoryType2').is(':checked')) {
					if($('#Categories2 option:checked').length == 0) {
						$('#Categories2 option').attr('selected', true);	
					}						
				}
				
				$.post("?page=Integrated&action=getResult", $("#formContent").serialize(),
					function(data) {
						$('#formContent').attr('style','display: none');
						$('#form_result').html(data);		
						$('#spanShowOption').show();
						$('#spanHideOption').hide();
						$('#report_show_option').addClass("report_option report_hide_option");
						$('.visual_chart').each(function (){
							$div = $('<div style="page-break-inside: avoid;" />')
							barchart_colors = ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744','#ce1e2d','#766699','#a2d5ea','#fe8310','#9d10ee','#6a3b16','#36a4ed','#045a90','#f9e744'];
							$(this).visualize({type: 'bar', width: '660px', height : '200px', colors : barchart_colors}).appendTo($div);
							$div.appendTo($('#result_chart'));
						});
						window.scrollTo(0, 0);
						$('.result_toggler').click(function(){
							if(this.className == 'result_toggler thumb_list_tab_on')
								return false;
							$('.result_toggler').toggleClass('thumb_list_tab_on');
							$('#result_stat').toggle();
							$('#result_chart').toggle();
							if($('#result_chart').css('display') == 'none'){
								$('#hidden_table').css("display","none");
							}
							else{
								$('#hidden_table').css("display","");
							}
							return false;
						});
	
						$('#result_chart').toggle();
					});
				
			});
		});

		

		function click_export()
		{
			document.form1.action = "?page=Integrated&action=export";
			document.form1.submit();
			document.form1.action = "?page=Integrated";
		}

		function click_print()
		{
			var url = "?page=Integrated&action=print&show=list";
			if ($('#result_stat').css('display') == 'none') {
				url = "?page=Integrated&action=print&show=chart";
			}
			document.form1.action=url;
			document.form1.target="_blank";
			document.form1.submit();
			
			document.form1.action="?page=Integrated";
			document.form1.target="_self";
		}
		
		function hideOptionLayer()
		{
			$('#formContent').attr('style', 'display: none');
			$('.spanHideOption').attr('style', 'display: none');
			$('.spanShowOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_hide_option';
		}
		
		function showOptionLayer()
		{
			$('#formContent').attr('style', '');
			$('.spanShowOption').attr('style', 'display: none');
			$('.spanHideOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_show_option';
		}
		
		
		</script>
		<?
		$x = ob_get_contents();
		ob_end_clean();
	
		
		return $x;
	}
	
	/**
	 * get Toolbar HTML code:  e.g. export print  ...... 
	 * @param array $viewdata
	 */
	public function getToolbar($viewdata){
		global $Lang;
		//don't show if not posted.
		if (!isset($viewdata['post']))
			return;
		
		ob_start();
		?>

		<div id='toolbox' class="content_top_tool">
			<div class="Conntent_tool">
				<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
				<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
			</div>
			<br style="clear:both" />
		</div>
		<? 
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}
	
	public function getFrom($viewdata){
		global $Lang;
	
		ob_start();
?>

			<div id='report_show_option' class="" >
				<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>
			
			
			<form name="form1" method="post" action="?page=Integrated" id="formContent">
				
				<table class="form_table_v30">
			<? 
					echo $this->getForm_Period($viewdata);
					echo $this->getForm_Display($viewdata);
					echo $this->getForm_BookCirculationType($viewdata);
					echo $this->getForm_BookCategory($viewdata);
					echo $this->getForm_BookSubject($viewdata);
					echo $this->getForm_Order($viewdata);
			?>
				</table>
				
				<?=$this->linterface->MandatoryField();?>
				
				<div class="edit_bottom_v30">
					<p class="spacer"></p>
					<?= $this->linterface->GET_ACTION_BTN($Lang['libms']['report']['Submit'], "button",'',"submit_result")?>
					<p class="spacer"></p>
				</div>
			</form>
		</div>
		
		<div id='form_result'></div>
<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_Period($viewdata){
		global $Lang, $junior_mck;
		
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		if ($textFromDate=="" && $textToDate=="")
		{
			# preset date range as current school year
			$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());

			if (is_array($current_period) && sizeof($current_period)>0)
			{
				foreach($current_period as &$date){
					$date = date('Y-m-d',strtotime($date));
				}
				$textFromDate = $current_period['StartDate'];
				$textToDate = $current_period['EndDate'];
			} else
			{
				$textFromDate = (date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01';
				$textToDate = date('Y-m-d');
			}
			
		}
		ob_start();
		?>	
		<!-- Period -->
		<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['time_period']?></td>
			<td>
				<table class="inside_form_table" style='width: 350px;'>
<?php if (!isset($junior_mck)) {
	 ?> 
					<tr>
						<td colspan="6" >
							<input name="data[Period]" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?>  >
							<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
							<?=getSelectAcademicYear("data[AcademicYear]", "");?>&nbsp;
							<?//=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
							
						</td>
					</tr>
<?php } ?>			
					<tr>
						<td>
							<input name="data[Period]" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE" || isset($junior_mck))?" checked":"" ?> >
							<?=$Lang['libms']['report']['period_date_from']?>
						</td>
						<td >
							<?=$this->linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
							<br />
							<span id='div_DateEnd_err_msg'></span>
						</td>
						<td><?=$Lang['libms']['report']['period_date_to']?> </td>
						<td >
							<?=$this->linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}
	
	public function getForm_Display($viewdata){
		global $Lang;
		$libms = new liblms();
		$Subjects =$viewdata['display'];
	
		ob_start();
		?>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['display']?></td>
				<td>
								<span id="Subjects">
									<input type="checkbox" id="circulation_type" name="data[display][circulation_type]" value="1" <?= $libms->isitchecked($viewdata[post][display][circulation_type], 1)?> > <?=$Lang['libms']['report']['circulation_type'] ?><br>
									<input type="checkbox" id="book_category" name="data[display][book_category]" value="1" <?= $libms->isitchecked($viewdata[post][display][book_category], 1)?> > <?=$Lang['libms']['report']['book_category']?><br>
									<input type="checkbox" id="book_csubject" name="data[display][subject]" value="1" <?= $libms->isitchecked($viewdata[post][display][subject], 1)?> > <?=$Lang['libms']['report']['subject'] ?><br>
									<input type="checkbox" id="payment" name="data[display][payment]" value="1" <?= $libms->isitchecked($viewdata[post][display][payment], 1)?> > <?=$Lang['libms']['report']['payment']['title']?><br>
									<input type="checkbox" id="bookrecord" name="data[display][bookrecord]" value="1" <?= $libms->isitchecked($viewdata[post][display][bookrecord], 1)?> > <?=$Lang['libms']['report']['bookrecord']['title']?><br>
								</span>
					<span id='div_Category_err_msg'></span>
				</td>
			</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_BookCategory($viewdata){
		global $Lang;
		global $linterface;
		
		
		$Category =$viewdata['category'];
		//Henry Added [20150122]
		$Category1 =$viewdata['category1'];
		$Category2 =$viewdata['category2'];
		
		ob_start();
		?>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['book_category']?></td>
				<td><!--
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories" size="10" multiple><?php
 									
										if (!empty($Category)){
											foreach ($Category as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
					<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories');?>
					<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
					<span id='div_Category_err_msg'></span>
					-->
					
	        		<table class="inside_form_table">
						<tr>
							<td valign="top">
								<?
								$libms = new liblms();
								$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
								foreach ($result as $row){
									$book_category_name[] = htmlspecialchars($row['value']);
								}
								?>
								<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", 0, true, "", $Lang["libms"]["report"]["fine_status_all"])?>								
								<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, false, "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
								<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, false, "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode1" style='<?=($viewdata['post']['Categories']==1?'':'display:none;')?>'>
							<td valign="top">
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories" size="10" multiple><?php
 									
										if (!empty($Category1)){
											foreach ($Category1 as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
								<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories');?>
								<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
								<span id='div_Category_err_msg'>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode2" style='<?=($viewdata['post']['Categories']==2?'':'display:none;')?>'>
							<td valign="top">
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories2" size="10" multiple><?php
 									
										if (!empty($Category2)){
											foreach ($Category2 as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
								<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories2');?>
								<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
								<span id='div_Category_err_msg'>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	
	
	public function getForm_BookCirculationType($viewdata){
		global $Lang;
		global $linterface;
		
		$categoryElements =$viewdata['categoryElements'];
		$circulationLists = $viewdata['circulation'];

		ob_start();
		?>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['circulation_type']?></td>
			<td>
							<span id="CategoryElements" >
							<select name="data[CirCategory][]" id="CirCategory" size="10" multiple >
							<?
									//if (!empty($categoryElements))
										foreach ($circulationLists as $key => $item) {
											if (isset($viewdata['post']['CirCategory']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['CirCategory']))
												$select=" selected ";
											else
												$select="";
											echo ("<option value='$key' $select>$item</option>");
										}
							?>
							</select>
							</span>
				<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCirCat');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
		
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_BookSubject($viewdata){
		global $Lang;
		global $linterface;
		
		$subjectList =$viewdata['subjects'];
		ob_start();
		?>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['subject']?></td>
			<td>
							<span id="Subjects">
								<select name="data[subject][]" id="subject" size="10" multiple >
								<?php
									//if (!empty($subject))
										foreach ($subjectList as $key => $item){
											if (isset($viewdata['post']['subject']) && in_array(htmlspecialchars_decode($item,ENT_QUOTES), $viewdata['post']['subject']))
												$select=" selected ";
											else{
												$select="";
// 												tolog(htmlspecialchars_decode($item,ENT_QUOTES));
// 												tolog($viewdata['post']['subject']);
											}
											echo "<option value='$key' $select>$item</option>";
										}
								?>
								</select>
							</span>
					<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnSubject');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
		
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_Order($viewdata){
		global $Lang;
		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SortBy"]?></td>
			<td>
				<?=$this ->linterface->Get_Radio_Button("sortByNo", "data[sortBy][type]", 'count', true, "", $Lang['libms']['report']['number'])?>
				<?=$this ->linterface->Get_Radio_Button("sortByName", "data[sortBy][type]", 'name', false, "", $Lang['libms']['report']['name'])?>
				<br>
				<?=$this ->linterface->Get_Radio_Button("sortByOrderAsc", "data[sortBy][desc]", 'Asc', true, "", $Lang['libms']['report']['ascending'])?>
				<?=$this ->linterface->Get_Radio_Button("sortByOrderDesc", "data[sortBy][desc]", 'Desc', false, "", $Lang['libms']['report']['descending'])?>				
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	
	public function getResult($viewdata){
		global $Lang, $junior_mck;
		ob_start();
		echo '<div id="result_body"  >';
		if ( $_REQUEST['action'] != 'print')
		{
		?> 
		<div class="thumb_list_tab print_hide">
			<a class='result_toggler' href="#">
				<span><?=$Lang["libms"]["reporting"]["span"]['graph']?></span>
			</a>
			<em>|</em>
			<a href="#" class="result_toggler thumb_list_tab_on">
				<span><?=$Lang["libms"]["reporting"]["span"]['table']?></span>
			</a>
		</div>
<?php 	
		}
		
		if ( $_REQUEST['action'] == 'print')
		{
			//Added school name at top of the page
			$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
			echo '<p>'.$schoolName.'</p><center><h1>'.$Lang["libms"]["reporting"]["PageReportingIntragration"].'</h1></center>';
		
			// Print filter period info
			echo '<table border="0" width="100%"><tr><td>';
			echo  $Lang['libms']['report']['period_date_from'] . " " .
				$viewdata['post']['textFromDate'] . $Lang['libms']['report']['period_date_to'] . " " . $viewdata['post']['textToDate'];
			echo '</td><td align="right">';
			echo $Lang["libms"]["report"]["printdate"] . " ".date("Y-m-d").'</td></tr></table>';
		}

		echo '<div id="result_stat"  >';
		
		//dump($viewdata['Result']);
		if ($viewdata['post']['display']['circulation_type'] == '1')
			echo $this->getRecord_circulation_type($viewdata);
		if ($viewdata['post']['display']['book_category'] == '1')
			echo $this->getRecord_book_category($viewdata);
		if ($viewdata['post']['display']['subject'] == '1')
			echo $this->getRecord_Subject($viewdata);
		if ($viewdata['post']['display']['payment'] == '1')
			echo $this->getRecord_Overdue($viewdata);
		if ($viewdata['post']['display']['bookrecord'] == '1')
			echo $this->getRecord_BookRecord($viewdata);
		
		//echo $this->getRecord_TEST();
		echo '</div>';
		echo '<div id="result_chart" style="text-align:left" >';
		echo '</div>';
		echo '</div>';
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	
	public function getRecord_Overdue($viewdata){
		global $Lang;

		### build display table
			$x = "<div style='page-break-inside: avoid;'>";
			$x .= "<table class='common_table_list_v30 view_table_list_v30 ' width='100%'>";
			$x .= "<caption class='caption_font'>{$Lang['libms']['report']['payment']['title']}</caption>";
			$x .= "<thead><tr>";
			$x .= "<td class='cell_bg_blue'  width='200'>&nbsp;</td>";
			$x .= "<th class='cell_bg_blue'>".$Lang['libms']['report']['amount']."</th>";
			$x .= "</tr></thead>";
			//tolog( $viewdata['Result']['Overdue']);
			foreach($viewdata['Result']['Overdue'] as $items)
			{
				//tolog($items);				
				$x .= "<tr>";
				$recordstatus = $items['RecordStatus'];
				$x .= "<th >".$Lang["libms"]["report"]["payment"][$recordstatus]."</th>";
				$x .= "<td> $ ".$items['count']."</td>";
				$x .= "</tr>";
			}
			$x .= "</table>";
			$x .= "</div>";
			$x .= "<br/>";
		### build display table
		
	 return $x;
	}
	
	public function getRecord_BookRecord($viewdata){
		global $Lang;
			
		### build display table
			$x = "<div style='page-break-inside: avoid;'>";
			$x .= "<table class='common_table_list_v30 view_table_list_v30' width='100%' >";
			$x .= "<caption class='caption_font'>{$Lang['libms']['report']['bookrecord']['title']}</caption>";
			$x .= "<thead><tr>";
			$x .= "<td class='cell_bg_blue'  width='200'>&nbsp;</td>";
			$x .= "<th class='cell_bg_blue'>".$Lang['libms']['report']['number']."</th>";
			$x .= "</tr></thead>";
			foreach($viewdata['Result']['BookCount'] as $items)
			{
				//tolog($items);				
				$x .= "<tr>";
				$recordstatus = $items['RecordStatus'];
				$x .= "<th>".$Lang['libms']['report']['bookrecord'][$recordstatus]."</th>";
				$x .= "<td>".$items['count']."</td>";
				$x .= "</tr>";
			}
		
			$x .= "</table>";
			$x .= "</div>";
			$x .= "<br/>";
		### build display table
		
	 return $x;
	}
	
	public function getRecord_book_category($viewdata){
		global $Lang;
		$total_no = 0;
		if (count($viewdata['Result']['Categories']) > 0) {
			if (isset($viewdata['Result']['Categories'][0]['BookCategoryType'])) {
				$libms = new liblms();
				$cat_type_array = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
				foreach ($cat_type_array as $k=>$v){
					$category_type[$v['id']] = htmlspecialchars($v['value']);
				}
				$show_category_type = true;			
			}	
		}
		else {
			$show_category_type = false;
		}
				
		### build display table
			$x = "<div style='page-break-inside: avoid;'>";
			$x .= "<table class='common_table_list_v30 view_table_list_v30 visual_chart'  width='100%'>";
			$x .= "<caption class='caption_font'>{$Lang["libms"]["report"]["book_category"]}</caption>";
			$x .= "<thead><tr>";
			$x .= "<td class='cell_bg_blue' width='200'>&nbsp;</td>";
			$x .= "<th class='cell_bg_blue'>".$Lang['libms']['report']['number']."</th>";
			$x .= "</tr></thead>";
			if ($show_category_type) {
				foreach($viewdata['Result']['Categories'] as $items)
				{
					//tolog($items);
					if ($items['BookCategory']) {
						$x .= "<th>". $items['BookCategory'];
						if ($category_type[$items['BookCategoryType']]) {
							$x .= " (".$category_type[$items['BookCategoryType']].")";
						}
						$x .= "</th>";  						
					}	
					else {
						$x .= "<th>". $Lang["libms"]["report"]["cannot_define"]."</th>";						
					}			
					$x .= "<td>".$items['count']."</td>";
					$total_no = $total_no + $items['count'];
					$x .= "</tr>";
				}
			}
			else {
				foreach($viewdata['Result']['Categories'] as $items)
				{
					//tolog($items);				
					$x .= "<th>".($items['BookCategory'] ? $items['BookCategory'] : $Lang["libms"]["report"]["cannot_define"])."</th>";
					$x .= "<td>".$items['count']."</td>";
					$total_no = $total_no + $items['count'];
					$x .= "</tr>";
				}
			}
			
			$x .="<table class='common_table_list_v30 view_table_list_v30' width='100%'>";
			$x .= "<tr><td style='text-align: right;' width='200'>".$Lang["libms"]["report"]["total"]."</td><td>".$total_no."</td></tr>";
			$x .= "</table>";
			$x .= "</table>";
			$x .= "</div>";
			$x .= "<br/>";
		### build display table
		
	 return $x;
	}
	
	public function getRecord_Subject($viewdata){
		global $Lang;
		$total_no = 0;
			
		### build display table
			$x = "<div style='page-break-inside: avoid;'>";
			$x .= "<table class='common_table_list_v30 view_table_list_v30 visual_chart'  width='100%'>";
			$x .= "<caption class='caption_font'>{$Lang['libms']['report']['subject']}</caption>";
			$x .= "<thead><tr>";
			$x .= "<td class='cell_bg_blue'  width='200'></td>";
			$x .= "<th class='cell_bg_blue'>".$Lang['libms']['report']['number']."</th>";
			$x .= "</tr></thead>";
			foreach($viewdata['Result']['Subject'] as $items)
			{
				//tolog($items);				
				$x .= "<th>".$items['Subject']."</th>";
				$x .= "<td>".$items['count']."</td>";
				$total_no = $total_no + $items['count'];
				$x .= "</tr>";
			}
			$x .="<table class='common_table_list_v30 view_table_list_v30' width='100%'>";
			$x .= "<tr><td style='text-align: right;' width='200'>".$Lang["libms"]["report"]["total"]."</td><td>".$total_no."</td></tr>";
			$x .= "</table>";
			$x .= "</table>";
			$x .= "</div>";
			$x .= "<br/>";
		### build display table
		
	 return $x;
	}
	
		public function getRecord_circulation_type($viewdata){
		global $Lang;
		$total_no = 0;
			
		### build display table
			$x = "<div style='page-break-inside: avoid;'>";
			$x .= "<table class='common_table_list_v30 view_table_list_v30 visual_chart' width='100%'>";
			$x .= "<caption class='caption_font'>{$Lang['libms']['report']['circulation_type']}</caption>";
			$x .= "<thead><tr>";
			$x .= "<td class='cell_bg_blue' width='200'>&nbsp;</td>";
			$x .= "<th class='cell_bg_blue'>".$Lang['libms']['report']['number']."</th>";
			$x .= "</tr></thead>";
			foreach($viewdata['Result']['CirCategory'] as $items)
			{
				//tolog($items);
				$x .= "<tr>";
				$x .= "<th class='cell_bg_blue'>".$items['CirCategory']."</th>";
				$x .= "<td>".$items['count']."</td>";
				$total_no = $total_no + $items['count'];
				$x .= "</tr>";
			}
			$x .="<table class='common_table_list_v30 view_table_list_v30' width='100%'>";
			$x .= "<tr><td style='text-align: right;' width='200'>".$Lang["libms"]["report"]["total"]."</td><td>".$total_no."</td></tr>";
			$x .= "</table>";
			$x .= "</table>";
			$x .= "</div>";
			$x .= "<br/>";
		### build display table
		
	 return $x;
	}
	
	
	/**
	 * get Print view 
	 * @param array $viewdata
	 * @return string
	 */
	public function getPrint($viewdata){
		global $Lang, $LAYOUT_SKIN;
		ob_start();
		include_once("{$_SERVER['DOCUMENT_ROOT']}/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		?>
		<link href="css/visualize.css" type="text/css" rel="stylesheet" />
		<link href="css/visualize-light.css" type="text/css" rel="stylesheet" />
		<link href="css/reports.css" type="text/css" rel="stylesheet" />
		<script src="/templates/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/number_rounding.js"></script>
		<script type="text/javascript" src="js/visualize.jQuery.js"></script>
		<script type="text/javascript" src="js/number_rounding.js"></script>
		<script type="text/javascript" >
			$(function(){

				$('.visual_chart').each(function (){
					$div = $('<div style="page-break-inside: avoid;" />')
					barchart_colors = ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744','#ce1e2d','#766699','#a2d5ea','#fe8310','#9d10ee','#6a3b16','#36a4ed','#045a90','#f9e744'];
					$(this).visualize({type: 'bar', width: '660px', height : '200px', colors : barchart_colors}).appendTo($div);
					$div.appendTo($('#result_chart'));
				});
				window.scrollTo(0, 0);
				$('.result_toggler').click(function(){
					$('.result_toggler').toggleClass('thumb_list_tab_on');
					$('#result_stat').toggle();
					$('#result_chart').toggle();
					return false;
				});

				$('#result_chart').toggle();

				if ('<?=$_GET['show']?>' == 'chart'){
					$('.result_toggler').toggleClass('thumb_list_tab_on');
					$('#result_stat').toggle();
					$('#result_chart').toggle();
				}
				
			});
		</script>
		<div id='toolbar' class= 'print_hide' style='text-align: right; width: 100%'>
				<? 
					$linterface = new interface_html();
					echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Print'], "button", "javascript:window.print();","print");
					
				?>
		</div>
				
		<?=$this->getResult($viewdata);?>
		</body>
		</html>
		<?
		
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	/**
	 * get CSV string
	 * @param array $viewdata
	 * @return string
	 */
	public function getCSV($viewdata){
		global $Lang;
		
		ob_start();
		
		if ($viewdata['post']['display']['circulation_type'] == '1'){
			echo $this->getRecord_circulation_type_CSV($viewdata);
			echo "\n";
		}
		if ($viewdata['post']['display']['book_category'] == '1'){
			echo $this->getRecord_book_category_CSV($viewdata);
			echo "\n";
		}
		if ($viewdata['post']['display']['subject'] == '1'){
			echo $this->getRecord_Subject_CSV($viewdata);
			echo "\n";
		}
		if ($viewdata['post']['display']['payment'] == '1'){
			echo $this->getRecord_Overdue_CSV($viewdata);
			echo "\n";
		}
		if ($viewdata['post']['display']['bookrecord'] == '1'){
			echo $this->getRecord_BookRecord_CSV($viewdata);
			echo "\n";
		}
		$csv = ob_get_contents();
		ob_end_clean();
		return $csv;
	
	}
	
	
	public function getRecord_Overdue_CSV($viewdata){
		global $Lang;
		$headers[] = $Lang['libms']['report']['payment']['title'];
		$headers[] = $Lang['libms']['report']['amount'];
		foreach($viewdata['Result']['Overdue'] as &$items)
		{
			$recordstatus = $items['RecordStatus'];
			$items['RecordStatus'] = $Lang["libms"]["report"]["payment"][$recordstatus];
			if(!$items['count']){//20150209 fixed displays blank if no record
				$items['count'] = 0;
			}
		}
		//$Ary2d = $this->SQL_result_to_2Darray ($viewdata['Result']['Overdue'],$headers);
		array_unshift($viewdata['Result']['Overdue'],$headers);
		$csv = Exporter::array_to_CSV($viewdata['Result']['Overdue']);
		return $csv;
	}
	
	public function getRecord_BookRecord_CSV($viewdata){
		global $Lang;
		
		$headers[] = $Lang['libms']['report']['bookrecord']['title'];
		$headers[] = $Lang['libms']['report']['number'];
		foreach($viewdata['Result']['BookCount'] as &$items)
		{
			$recordstatus = $items['RecordStatus'];
			$items['RecordStatus'] = $Lang["libms"]["report"]["bookrecord"][$recordstatus];
		}
		unset($items);
		//$Ary2d = $this->SQL_result_to_2Darray ($viewdata['Result']['BookCount'], $headers);
		array_unshift($viewdata['Result']['BookCount'],$headers);
		$csv = Exporter::array_to_CSV($viewdata['Result']['BookCount']);
		return $csv;
		
	}
	
	public function getRecord_book_category_CSV($viewdata){
		global $Lang;
		
		$headers[] = $Lang['libms']['report']['book_category'];
		$headers[] = $Lang['libms']['report']['number'];
		$total_no = 0;//20150209 fixed displays blank if no record
		foreach($viewdata['Result']['Categories'] as &$items)
		{
			$total_no += $items['count'];
		}
		$viewdata['Result']['Categories'][] = array ('BookCategory' => $Lang["libms"]["report"]["total"], 'count' =>  $total_no);
		array_unshift($viewdata['Result']['Categories'],$headers);
		$csv = Exporter::array_to_CSV($viewdata['Result']['Categories']);
		return $csv;

	}
	
	public function getRecord_Subject_CSV($viewdata){
		global $Lang;
		$headers[] = $Lang['libms']['report']['subject'];
		$headers[] = $Lang['libms']['report']['number'];
		$total_no = 0; //20150209 fixed displays blank if no record  
		foreach($viewdata['Result']['Subject'] as &$items)
		{
			$total_no += $items['count'];
		}
		$viewdata['Result']['Subject'][] = array ('Subject' => $Lang["libms"]["report"]["total"], 'count' =>  $total_no);
		array_unshift($viewdata['Result']['Subject'],$headers);
		$csv = Exporter::array_to_CSV($viewdata['Result']['Subject']);
		return $csv;
		
	}
	
	public function getRecord_circulation_type_CSV($viewdata){
		global $Lang;
		$total_no = 0;
		$headers[] = $Lang['libms']['report']['circulation_type'];
		$headers[] = $Lang['libms']['report']['number'];

		foreach($viewdata['Result']['CirCategory'] as &$items)
		{
			$total_no += $items['count'];
		}
		$viewdata['Result']['CirCategory'][] = array ('CirCategory' => $Lang["libms"]["report"]["total"], 'count' =>  $total_no);
		array_unshift($viewdata['Result']['CirCategory'],$headers);
		$csv = Exporter::array_to_CSV($viewdata['Result']['CirCategory']);
		return $csv;
	}
}

	
?>