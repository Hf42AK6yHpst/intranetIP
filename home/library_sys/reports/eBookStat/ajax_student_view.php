<?php

// Using:  
######################################################################################
#									Change Log 
######################################################################################
#
#   2019-06-17 Cameron
#       - add $progressCond
#
#	2018-01-22 Cameron
#		- restrict to eBook only (case #W133701)
#
#	20160504 Cameron : 
#		- correct Time Condition sql
#		- add condition Publish=1 so that it's consistent to "eBooks Statistics" report
#		- fix counting number of book hit	
#
#	20141201 Ryan : eLibrary plus pending developments Item 203
#
######################################################################################
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['ebook statistics'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}


$linterface = new interface_html();
$lelibplus = new elibrary_plus($book_id, $UserID, $book_type="type_pc", $range="accumulated");

# User Condition
$UserCond = "bh.UserID = '{$uID}'";
$physical_book_init_value = 10000000;
  
# Time Condition
//$TimeCond = "bh.DateModified BETWEEN '$StartDate' AND '$EndDate' + INTERVAL 1 DAY";
$TimeCond = "bh.DateModified BETWEEN '$StartDate 00:00:00' AND '$EndDate 23:59:59' ";

switch ($_POST['readingFilter']) {
    case 'readup':
        $progressCond = " AND p.Percentage>='".$percentage."' ";
        break;
        
    case 'readdown':
        $progressCond = " AND p.Percentage<'".$percentage."' ";
        break;
        
    default:
        $progressCond = "";
        break;
}

$sql = "select bh.BookID, eb.Title, eb.Author, 
				count(*) as ReadCount, 		
				IFNULL(p.Percentage , 0) as Percentage,
				max(bh.DateModified) as LastRead
		From INTRANET_ELIB_BOOK_HISTORY bh
		inner join INTRANET_ELIB_BOOK eb on eb.BookID = bh.BookID
		left join (select Max(IFNULL(Percentage,0)) as Percentage, UserID, BookID from INTRANET_ELIB_USER_PROGRESS
					group by BookID, UserID ) p on p.BookID = bh.BookID and p.UserID=bh.UserID 
		Where
			{$UserCond}
		And
			{$TimeCond} 
			{$progressCond}
		And eb.Publish=1
		And bh.BookID<={$physical_book_init_value}
		Group By bh.BookID order by eb.Title ";
		
$StudentRecords = $lelibplus->returnArray($sql);

echo $libms->Get_Student_View($StudentRecords);
intranet_closedb();
?>