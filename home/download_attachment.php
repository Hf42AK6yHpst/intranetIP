<?php
// Using :

#######################################
#
#   Date:   2020-05-22 Bill     [2020-0522-1459-13207]
#           commented log function & output buffer handling    (need to upload with lib.php)
#
#	Date:	2020-03-17 Philips	[2020-0316-1050-17066]
#			Whitelist the download of csv sample files at /home/portfolio/teacher/data_handling/ for iPortfolio
#
#   Date:   2020-03-04  Bill    [2020-0301-1713-26035]
#           - removed temp assign memory, prevent server from out of memory if many request made in a short time
#
#   Date:   2020-02-14  Sam
#           - change the file output function from output2browser to output2browserByPath for better memory consumption
#
#   Date:   2020-01-24  Bill    [2020-0104-1404-47254]
#           - permission denied if $target_e (line 88) / $target (line 103) is empty str
#
#   Date:   2020-01-08  Bill    [2020-0106-0947-49235]
#           - fixed display garbled content using IOS 13 to open doc/docx file
#
#   Date:   2019-09-16  Bill    [2019-0902-1730-23206]
#           - fixed cannot download pdf using IOS 13
#
#	Date:	2019-08-13 Carlos
#			Whitelist the download of csv sample files at /home/common_reply_slip/ and /home/common_table_reply_slip/ for Digital Routing.
#
#   Date:   2019-07-10 Anna [#164748]
#           - ADDED iPortfolio as valid path when download dynamic report 
#
#   Date:	2019-05-18  Ryan
#           Security fix:
#           - Implemented decryption attempt and failure handling using getDecryptedText()
#
#	Date:	2019-05-17  Carlos
#			Security fix:
#			- must use $target_e and not use $target.
#			- check real full path contains pattern 'intranetdata' and '/file'
#			- ban .php file type
#
#   Date:   2019-03-06  Bill
#           using app > perform auth checking using token   ($special_feature['download_attachment_add_auth_checking'])
#
#	Date:	2016-10-11	Carlos
#			added performance log to observe memory usage.
#
#	Date:	2013-03-22	Carlos
#			added encrypted parameter filename_e as the real output file name
#
#	Date:	2013-02-08	Carlos
#			play video file in new window for iPad
#
#	Date:	2013-02-08	YatWoon
#			improved: increase the momery limit during download file [Case#2013-0207-0948-28156]
#
#	Date:	2012-05-04	YatWoon
#			add $special_feature['download_attachment_add_auth_checking'] (ignore the last flag setting)
#
#	Date:	2012-05-04	YatWoon
#			add $special_feature['download_attachment_ignore_auth_checking']
#
#	Date:	2012-03-16	YatWoon
#			add intranet_auth() ensure download the file within login session. [Case#2012-0316-1554-09132]
#
#######################################

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if($special_feature['download_attachment_add_auth_checking'])
{
    if(!isset($_SESSION['UserID']) && isset($token) && $token != '')
    {
        if(performSimpleTokenByDateTimeChecking($token)) {
            // allow app access > passed token checking
        } else {
            echo 'This attachment is expired.';
            exit;
        }
    }
    else
    {
	    intranet_auth();
    }
}

// start_log();

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

# Temp assign memory of this page
//ini_set("memory_limit", "999M");
//$libfilesytem = new libfilesystem();

// [2020-0104-1404-47254] add checking - permission denied if $target_e is empty str
// if(!isset($target_e) || isset($target)) {
if(!isset($target_e) || trim($target_e) == '' || isset($target)) {
	echo "Permission denied.";
	exit;
}

// $target = (isset($target_e) && $target_e!="") ? getDecryptedText($target_e): stripslashes($target);
try {
    $target = getDecryptedText($target_e);
}
catch (ExpiredContentExpection $exception) {
    echo $exception->getMessage();
    exit();
}

// [2020-0104-1404-47254] add checking - permission denied if $target is empty str
if(trim($target) == '') {
    echo "Permission denied.";
    exit;
}

$real_fullpath = realpath($target);
$valid_path = (strpos($real_fullpath, 'intranetdata') !== false && (strpos($real_fullpath, '/file') !== false || strpos($real_fullpath, '/iPortfolio') !== false )) || strpos($real_fullpath, '/home/common_reply_slip/') !== false || strpos($real_fullpath, '/home/common_table_reply_slip/') !== false || strpos($real_fullpath, '/home/portfolio/teacher/data_handling/') !== false;
// Ban .php file
$lc_file_ext_with_dot = strtolower(substr($target, strrpos($target,".")));
if(!$valid_path || in_array($lc_file_ext_with_dot,array('.php'))){
	echo "Permission denied.";
	exit;
}

$real_filename = stripslashes(substr($target,strrpos($target,"/")+1,strlen($target)));
$output_filename = (isset($filename_e) && $filename_e != "") ? getDecryptedText($filename_e) : $real_filename;

### Check user's browser ###
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
    $browser_version=$matched[1];
    $browser = 'IE';
} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
    $browser_version=$matched[1];
    $browser = 'Opera';
} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
	$browser_version=$matched[1];
	$browser = 'Firefox';
} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
	$browser_version=$matched[1];
	$browser = 'Safari';
} else {
	// browser not recognized!
    $browser_version = 0;
    $browser = 'other';
}

# Play video file in new window on iPad
if(isset($userBrowser))
{
	$platform = $userBrowser->platform;
	if($platform == "iPad" && isVideoFile($target))
	{
		header("Content-Type: text/html;charset=".returnCharset());
		$x = '<script type="text/javascript" language="JavaScript">'."\n";
		$x.= 'window.location.href=\''.str_replace($file_path,"",$target).'\';';
		$x.= '</script>'."\n";

		echo $x;
		exit;
	}
}

session_write_close();

//$target = "../../../../../file/reading_garden/announcement/1299827689/!@#$%^[]'&()_+}{;,.txt";
//$content = get_file_content($target);
//debug_pr("../../../../../file/reading_garden/announcement/1299827689/!@#$%^[]'&()_+}{;,.txt");
//debug_pr(file_exists("../../../../../file/reading_garden/announcement/1299827689/!@#$%^[]'&()_+}{;,.txt"));
//$real_filename = utf8Encode2($real_filename); # convert octets to represented chars in UTF-8, see above utf8Encode2()
$filename = $output_filename;
//$mime_type = $libfilesytem->getMIMEType($filename);

// stop_log();

// [2020-0106-0947-49235] IOS 13 - for doc file
// [2019-0902-1730-23206] IOS 13 - for pdf file
$content_type = mime_content_type($real_fullpath);
$file_extension = getFileExtention($real_fullpath, 1);
if ($content_type != 'application/pdf' && $file_extension != 'doc' && $file_extension != 'docx') {
    $content_type = '';
}

// Important!
// Close the output buffer before calling readfile function
// https://www.php.net/readfile#refsect1-function.readfile-notes
// while (@ob_end_flush());
// output2browser($content, $output_filename, $content_type);
output2browserByPath($real_fullpath, $output_filename, $content_type);

# convert the string with octets to represted chars in UTF-8 form
function utf8Encode2($source) {
   $utf8Str = '';
   $entityArray = explode ("&#", $source);
   $size = count ($entityArray);
   for ($i = 0; $i < $size; $i++) {
       $subStr = $entityArray[$i];
       $nonEntity = strpos ($subStr, ';');
       if ($nonEntity !== false) {
            $str = substr($subStr,0,$nonEntity);
            $str = "&#".$str.";";
            $str_end=substr($subStr,$nonEntity+1);

            $convertedStr=rawurlencode(octets2char($str));
            //$convertedStr=octets2char($str);

            $utf8Str.=$convertedStr.$str_end;
       }
       else {
           $utf8Str .= $subStr;
       }
   }
   //return preg_replace('/[:\\x5c\\/*?"<>|]/', '_', $utf8Str);
   return $utf8Str;
}

# end functions
?>