<?php
// Editing by 

/*
 * 2018-08-08 (Henry): fixed sql injection 
 */
 
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libevent.php");

intranet_auth();
intranet_opendb();

$EventID = IntegerSafe($EventID);

$lo = new libevent($EventID);

$MODULE_OBJ['title'] = $i_Events;
$template_page = $NoBack ? "popup_index.html" : "popup_index2.html";
$linterface = new interface_html($template_page);
$linterface->LAYOUT_START();
?>

<?
if ($lo->hasRightToView()) 
{
        echo $lo->display();
}
else
{
	echo "<div class='tabletext' align='center'>$i_general_no_privileges</div>";
}        
?>

<?php
$sql = "UPDATE INTRANET_EVENT SET ReadFlag = CONCAT(ifnull(ReadFlag,''),';$UserID;') WHERE EventID = $EventID";
$lo->db_db_query($sql);

intranet_closedb();
$linterface->LAYOUT_STOP();
?>