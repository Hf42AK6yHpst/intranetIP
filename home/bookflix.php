<?php

include_once("../includes/global.php");
include_once("../includes/libdb.php");


if (date("Y-m-d")<=$sys_integration['bookflix_school_expiry'])
{
	header("location: ".$sys_integration['bookflix_school_access']);
	die();
} else
{
	echo str_replace("<expiry_date>", $sys_integration['bookflix_school_expiry'], $Lang['bookflix_expired']);
}


?>