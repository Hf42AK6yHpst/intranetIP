<?php

$headers = "Content-Type: application/json; charset=UTF-8";

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_api.php");   //for token
include_once('../astri/dbindex/db_config.php'); //for db param config


$folder_name = $_GET["Folder_Name"];
$mailStatus = $_GET["MailStatus"];
$page_no = ($_GET["page"]) ? intval($_GET["page"]) : 1 ;
$offset = ($page_no - 1)*25;
// $userid = $_GET["userID"];
$token = $_GET['token'];

intranet_opendb();

//############# check token to get the user account info  #############//
$api = new libeClassAppApi();
//$token = "fqFKNE419o4OE2RWk/sm3ySoomU5c1xMNSR4MqVBqjLmKRHL1aysDToqNzOjxIZP";  //token for astridev2
$mailUserInfoAry = $api->retrieveMailUserInfoByToken($token);

// updated param by carlos, to do validation and to access to the user email 
//$user_email = isset($_REQUEST['user_email'])? $_REQUEST['user_email'] : "astridev2@astridev2.eclasscloud.hk";
//$user_password = isset($_REQUEST['user_password'])? $_REQUEST['user_password'] : "vh5gGPsQ";

//$user_email = strpos($mailUserInfoAry["userLogin"], "@") ? $mailUserInfoAry["userLogin"] : $mailUserInfoAry["userLogin"]."@astridev2.eclasscloud.hk";
$user_email = $mailUserInfoAry['email'];
$user_password = $mailUserInfoAry["userPw"];
$user_id = $mailUserInfoAry["userId"];



// mailstatus : SEEN
// 				FLAGGED
// 				UNSEEN
intranet_closedb();


// $servername = "padmin.biz21.hk";
// $username = "c21biz30_imail2";
// $password = "dLeAE@5#Wk]$";
// $dbname = "c21biz30_imail";


// 创建连接
$conn = new mysqli($servername, $username, $password,$dbname);
 
// 检测连接
if ($conn->connect_error) {
    die("连接失败: " . $conn->connect_error);
} 

//check new email to update indexdb 
$sql = "SELECT email_id FROM emails 
		WHERE folder_name = '$folder_name' AND user_id = '$user_id' 
		ORDER BY email_id DESC LIMIT 1";

$indexdb_lastuid = $conn->query($sql);
$indexdb_lastuid = $indexdb_lastuid->fetch_assoc();
$indexdb_lastuid = $indexdb_lastuid["email_id"];

$IMap = new imap_gamma(false,$user_email,$user_password);
switch ($folder_name) {
	case 'InboxFolder':
		$Folder = $IMap->InboxFolder;
		break;
	case 'SentFolder':
		$Folder = $IMap->SentFolder;
		break;
	case 'DraftFolder':
		$Folder = $IMap->DraftFolder;
		break;
	case 'TrashFolder':
		$Folder = $IMap->TrashFolder;
		break;
	default:
		$Folder = $folder_name;
		break;
}


	$email_lastuid = $IMap->Get_Last_Msg_UID($IMap->$folder_name);

if ($email_lastuid && ($email_lastuid > $indexdb_lastuid)) {

	$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));

	$array_uid = $IMap->Get_Sort_Index($IMap, $imapInfo=array('displayMode'=>'folder','sort'=>"SORTARRIVAL",'reverse'=>1), $search="", $seen="ALL", $useUID=1);
	$array_msgno = $IMap->Get_Sort_Index($IMap, $imapInfo=array('displayMode'=>'folder','sort'=>"SORTARRIVAL",'reverse'=>1), $search="", $seen="ALL");

	$IMap->getImapCacheAgent();

	$diff_uid = $email_lastuid - $indexdb_lastuid;
	for($i = 0; $i < $diff_uid; $i++)
	{

	 	if ($array_uid[$i] > $indexdb_lastuid) {
	 	
			$MailHeaderList = imap_headerinfo($IMap->inbox, $array_msgno[$i]);
			
			$uid = $IMap->Get_UID($MailHeaderList->Msgno);	
			
			$Flags = $IMap->IMapCache->Get_Message_Status($uid,$Folder);				//get uid
			$MailPriority = $IMap->GetMailPriority($uid,$Folder,1);						//get the important flag;
			$full_mail_detail = $IMap->getMailRecord($Folder, $uid);					//get the mail content

			########## DB fields handling #############
			$date = $MailHeaderList->date;
			$date = date("Y-m-d H:i:s",strtotime($date));
			
			$from = ($MailHeaderList->from==null) ? null : serialize($MailHeaderList->from) ;
			$to = ($MailHeaderList->to==null) ? null : serialize($MailHeaderList->to) ;
			$cc = ($MailHeaderList->cc==null) ? null : serialize($MailHeaderList->cc) ;
			$bcc = ($MailHeaderList->bcc==null) ? null : serialize($MailHeaderList->bcc) ;

			$subject = $MailHeaderList->subject;  //=?UTF-8?B?Y2MgYmNj?=
			if (strpos($subject, "=?UTF-8?B?")!==false) {
				$subject = substr($subject, 10);
				$subject = strstr($subject, '?=', true);
				$subject = base64_decode($subject);
			}
			$subject = strip_tags($subject);
			$subject = codeconvert($subject);
			

			$contents = $full_mail_detail[0]['message'];
			$contents = strip_tags($contents);
			$contents = str_replace("'", "\\'", $contents);
			$contents = codeconvert($contents);

			$attachments = (count($full_mail_detail[0]['attach_parts'])>0) ? serialize($full_mail_detail[0]['attach_parts']) : null ;  //array

			$importance = $MailPriority[$uid]['ImportantFlag'];   //string
			$importance = ($importance=="true") ? 1 : 0 ;

			$flagged = $MailHeaderList->Flagged; //string
			$flagged = ($flagged=="F") ? 1 : 0 ;

			$has_read = $MailHeaderList->Unseen; //string
			$has_read = ($has_read=="U") ? 0 : 1 ;
			if ($has_read==0) {    //for indexdb. change back the status after getMailRecord() if original unread.
				$IMap->unreadMail($Folder, $uid);
			}

			$has_replied = $MailHeaderList->Answered; //string
			$has_replied = ($has_replied=="A") ? 1 : 0 ;

			$has_forwarded = $Flags[$uid]['forwarded']; //boolean
			$has_forwarded = ($has_forwarded==true) ? 1 : 0 ;

			$system_tags;


			//************* insert new record ***************
			$sql = "INSERT INTO emails (user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward, system_tags) 
					VALUES ('$user_id', '$date', '$folder_name', '$uid', '$from', '$to', '$cc', '$bcc', '$subject', '$contents', '$attachments', '$importance', '$flagged', '$has_read', '$has_replied', '$has_forwarded', '$system_tags')";

			
			if ($conn->query($sql) === false) {
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}
        }
	}
}



//check for 25 emails per page
$sql = "SELECT COUNT(*) AS count FROM emails 
		WHERE folder_name = '$folder_name' AND user_id = '$user_id' ";

$rs = $conn->query($sql);
$count = $rs->fetch_assoc();
$total_num_email = $count['count'];

$total_page = ceil($total_num_email/25);


//************* get specific record *************
if ($mailStatus=='SEEN') {
	$sql = "SELECT user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward FROM emails 
		WHERE folder_name = '$folder_name' AND user_id = '$user_id' AND has_read = '1'
		ORDER BY email_time DESC LIMIT 25 OFFSET $offset"; 
}elseif ($mailStatus=='UNSEEN') {
	$sql = "SELECT user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward FROM emails 
		WHERE folder_name = '$folder_name' AND user_id = '$user_id' AND has_read = '0'
		ORDER BY email_time DESC LIMIT 25 OFFSET $offset"; 
}elseif ($mailStatus=='FLAGGED') {
	$sql = "SELECT user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward FROM emails 
		WHERE folder_name = '$folder_name' AND user_id = '$user_id' AND flagged = '1'
		ORDER BY email_time DESC LIMIT 25 OFFSET $offset"; 
}else{
	$sql = "SELECT user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward FROM emails 
		WHERE folder_name = '$folder_name' AND user_id = '$user_id' 
		ORDER BY email_time DESC LIMIT 25 OFFSET $offset"; 
}

$result = $conn->query($sql);


if ($result->num_rows > 0) {
    
    while($row = $result->fetch_assoc()) {
    	
        
        $row["email_from"] = ($row["email_from"]==null) ? null : unserialize($row["email_from"]) ;
        $row["sent_to"] = ($row["sent_to"]==null) ? null : unserialize($row["sent_to"]) ;
        $row["cc"] = ($row["cc"]==null) ? null : unserialize($row["cc"]) ;
        $row["bcc"] = ($row["bcc"]==null) ? null : unserialize($row["bcc"]) ;
        
        $row["attachments"] = ($row["attachments"]==null) ? 0 : count(unserialize($row["attachments"])) ;
        $row["email_time"] = date('r',strtotime($row["email_time"]));
        $row["uid"] = $row["email_id"];

        $db_return[]=$row;
        
    }

} 

$conn->close();


$return_total_page = array("TotalPageNumber" => $total_page,"CurrentPageNumber"=>$page_no);

header("Content-Type: application/json; charset=UTF-8");
echo json_encode(array("PageInfo" => $return_total_page, "MailList"=>$db_return));

//########## token log ##########
// $url = "http://api.inno.biz21.hk/test.php";
// $jsonresult = json_encode(array($token,$indexdb_lastuid,$email_lastuid,$email_lastuid-$indexdb_lastuid));

// $pdf = http_post_json($url, $jsonresult);

// function http_post_json($url, $jsonStr)
// {
//     $ch = curl_init();
//     curl_setopt($ch, CURLOPT_POST, 1);
//     curl_setopt($ch, CURLOPT_URL, $url);
//     curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//             'Content-Type: application/json; charset=utf-8',
//             'Content-Length: ' . strlen($jsonStr)
//         )
//     );
//     $response = curl_exec($ch);
//     $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//     curl_close($ch);
 
//     //return array($httpCode, $response);
//     return $response;
// }
//########## token log ##########

function codeconvert($input){

  if( !empty($input) ){

    $encodetype = mb_detect_encoding($input , array('UTF-8','GBK','LATIN1','BIG5')) ;

    if( $encodetype != 'UTF-8'){

      $input = mb_convert_encoding($input ,'utf-8' , $encodetype);
      echo $encodetype."<br>";
      echo $input;
    }

  }

  return $input;

}

?>
