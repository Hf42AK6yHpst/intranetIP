<?php

//$headers = "Content-Type: application/json; charset=UTF-8";
set_time_limit(0);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_api.php");   //for token
include_once('./db_config.php'); //for db param config


//$folder_name = $_GET["Folder_Name"];
//$mailStatus = $_GET["MailStatus"];
//$page_no = intval($_GET["page"]);
// $userid = $_GET["userID"];
//$token = $_GET['token'];

intranet_opendb();

//############# check token to get the user account info  #############//

$api = new libeClassAppApi();
//$token = "fqFKNE419o4OE2RWk/sm3ySoomU5c1xMNSR4MqVBqjLmKRHL1aysDToqNzOjxIZP";  //token for astridev2
$access_time=time();
$salt_key = 'LcOgIWpKTxblsY61TsPWs5jPp26K9oL7';
$verify_code = sha1($access_time.'###'.$salt_key);

//$url_userlist='http://127.0.0.1/api/imailplus/mail_user_list.php';
$post_string='access_time='.$access_time.'&verify_code='.$verify_code;

$user_list = json_decode(request_by_curl($url_userlist,$post_string));

for ($i=0; $i < sizeof($user_list); $i++) { 
	$user[$i]=(array)$user_list[$i];
}

//DB connection
$conn = mysqli_connect($servername, $username, $password, $dbname, $dbport);
		 
if ($conn->connect_error) {
	die("DB connection failed: " . $conn->connect_error);
}
$last_db_connection_time = time();
$connection_period = 10 * 60; // seconds

echo "Start at ".date("Y-m-d H:i:s").".<br />\n";
flush();

$total_emails_count = 0;
$total_emails_imported = 0;
//########### start loop user emails ##############
for ($user_num=0; $user_num < sizeof($user); $user_num++) 
{
	if(time() - $last_db_connection_time > $connection_period){ // reconnect db in one hour interval
		intranet_closedb();
		intranet_opendb();
		
		$conn->close();
		$conn = mysqli_connect($servername, $username, $password, $dbname, $dbport);
		if ($conn->connect_error) {
			die("DB connection failed: " . $conn->connect_error);
		}
		$last_db_connection_time = time();
	}
	
	$mailUserInfoAry = $api->retrieveMailUserInfoByToken($user[$user_num]['Token']);
	
	// updated param by carlos, to do validation and to access to the user email 
	//$user_email = isset($_REQUEST['user_email'])? $_REQUEST['user_email'] : "astridev2@astridev2.eclasscloud.hk";
	//$user_password = isset($_REQUEST['user_password'])? $_REQUEST['user_password'] : "vh5gGPsQ";

	//$user_email = strpos($mailUserInfoAry["userLogin"], "@") ? $mailUserInfoAry["userLogin"] : $mailUserInfoAry["userLogin"]."@astridev2.eclasscloud.hk";
	$user_email = $mailUserInfoAry['email'];
	$user_password = $mailUserInfoAry["userPw"];
	$user_id = $mailUserInfoAry["userId"];

	if(isset($filter_email_list) && count($filter_email_list)>0){
		if(!in_array($user_email, $filter_email_list)){
			continue;
		}
	}
	if($user_password == ''){
		echo "Cannot connect to $user_email, password incorrect.<br />\n";
		flush();
		continue;
	}
	
	$IMap = new imap_gamma(false,$user_email,$user_password);

	if(!$IMap->ConnectionStatus){
		echo "Cannot connect to $user_email.<br />\n";
		flush();
		continue;
	}
	
	echo "Processing $user_email <br />\n";
	flush();
	
	$user_folders = null;
	$imapInfo['displayMode'] = "folder";
	$user_folders = $IMap->Get_Folder_Structure(true,false);    /////// Get user define folder list.


	// mailstatus : SEEN
	// 				FLAGGED
	// 				UNSEEN
	

	//$IMap->openInbox($useremail, $userpassword,'', $isHalfOpen);

	//predefine folder array
	$Folder[0] = $IMap->InboxFolder;
	$Folder[1] = $IMap->SentFolder;
	$Folder[2] = $IMap->DraftFolder;
	$Folder[3] = $IMap->TrashFolder;
	for ($i=0; $i < count($user_folders); $i++) { 
		$Folder[4+$i] = $user_folders[$i][1];
	}

		//*********** DB managing ***************
		// // $servername = "localhost";
		// // $username = "astri";
		// // $password = "J8hmds22XZ";
		// // $dbname = "astridev2";

		// // $servername = "localhost";
		// // $username = "astri";
		// // $password = "VBU3Nv798R";
		// // $dbname = "astridev1";

		// $servername = "padmin.biz21.hk";
		// $username = "c21biz30_imail2";
		// $password = "dLeAE@5#Wk]$";
		// $dbname = "c21biz30_imail";


		$IMap->getImapCacheAgent();

	for ($j = 0; $j < count($Folder); $j++) 
	{ 
		
		$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder[$j]));	
		$imapInfo['displayMode'] = "folder"; //"message" or "folder"
		$imapInfo['reverse'] = 1;				//reverse = true ---->list the new emails in front

		// retrieve the email ID list and save in $array
		if ($mailStatus != ""){
			$array = $IMap->Get_Sort_Index($IMap, $imapInfo, $search, $mailStatus);
		}else{
			$array = $IMap->Get_Sort_Index($IMap, $imapInfo);
		}
			

		 for($i = 0; $i< count($array); $i++)
		 {
			if(time() - $last_db_connection_time > $connection_period){ // reconnect db in one hour interval
				intranet_closedb();
				intranet_opendb();
				
				$conn->close();
				$conn = mysqli_connect($servername, $username, $password, $dbname, $dbport);
				if ($conn->connect_error) {
					die("DB connection failed: " . $conn->connect_error);
				}
				$last_db_connection_time = time();
			}
			
			$MailHeaderList = imap_headerinfo($IMap->inbox, $array[$i], 80, 80);
			$uid = $IMap->Get_UID($MailHeaderList->Msgno);	
			$Flags = $IMap->IMapCache->Get_Message_Status($uid,$Folder[$j]);				//get uid
			$MailPriority = $IMap->GetMailPriority($uid,$Folder[$j],1);						//get the important flag;
			$full_mail_detail = $IMap->getMailRecord($Folder[$j], $uid);					//get the mail content


			########## DB fields handling #############
			//$user_id;
			$date = $MailHeaderList->date;
			$date = date("Y-m-d H:i:s",strtotime($date));

			if ($j==0) {
				$folder_name = "InboxFolder";
			}
			if ($j==1) {
				$folder_name = "SentFolder";
			}
			if ($j==2) {
				$folder_name = "DraftFolder";
			}
			if ($j==3) {
				$folder_name = "TrashFolder";
			}if($j>3)  {
				$folder_name = $conn->real_escape_string($Folder[$j]);
			}

			//$from = ($MailHeaderList->from==null) ? null : $conn->real_escape_string(serialize($MailHeaderList->from)) ;
			//$to = ($MailHeaderList->to==null) ? null : $conn->real_escape_string(serialize($MailHeaderList->to)) ;
			//$cc = ($MailHeaderList->cc==null) ? null : $conn->real_escape_string(serialize($MailHeaderList->cc)) ;
			//$bcc = ($MailHeaderList->bcc==null) ? null : $conn->real_escape_string(serialize($MailHeaderList->bcc)) ;

			$from = '';
			if(isset($full_mail_detail[0]['fromDetail'][0])){
				$fromObj = $full_mail_detail[0]['fromDetail'][0];
				$from .= $fromObj->mailbox."@".$fromObj->host;
			}
			$from = $conn->real_escape_string($from);
			
			$to = "";
			if(isset($full_mail_detail[0]['toDetail']))
			{
				foreach($full_mail_detail[0]['toDetail'] as $key => $obj){
					if($to!="") $to .= ",";
					if(isset($obj->personal))
						$to .= ($IMap->MIME_Decode($obj->personal)."<".$obj->mailbox."@".$obj->host.">");
					else
						$to .= $obj->mailbox."@".$obj->host;
				}
			}
			$to = $conn->real_escape_string($to);
			
			$cc = "";
			if(isset($full_mail_detail[0]['ccDetail']))
			{
				foreach($full_mail_detail[0]['ccDetail'] as $key => $obj){
					if($cc!="") $cc .= ",";
					if(isset($obj->personal))
						$cc .= ($IMap->MIME_Decode($obj->personal)."<".$obj->mailbox."@".$obj->host.">");
					else
						$cc .= $obj->mailbox."@".$obj->host;
				}
			}			
			$cc = $conn->real_escape_string($cc);
			
			$bcc = "";
			if(isset($full_mail_detail[0]['bccDetail']))
			{
				foreach($full_mail_detail[0]['bccDetail'] as $key => $obj){
					if($bcc!="") $bcc .= ",";
					if(isset($obj->personal))
						$bcc .= ($IMap->MIME_Decode($obj->personal)."<".$obj->mailbox."@".$obj->host.">");
					else
						$bcc .= $obj->mailbox."@".$obj->host;
				}
			}			
			$bcc = $conn->real_escape_string($bcc);
			
			/*
			$subject = $MailHeaderList->subject;  //=?UTF-8?B?Y2MgYmNj?=
			if (strpos($subject, "=?UTF-8?B?")!==false) {
				$subject = substr($subject, 10);
				$subject = strstr($subject, '?=', true);
				$subject = base64_decode($subject);
			}
			*/
			$subject = $IMap->MIME_Decode($full_mail_detail[0]['subject']);
			$subject = strip_tags($subject);
			$subject = codeconvert($subject);
			$subject = $conn->real_escape_string($subject);
			
			$contents = $full_mail_detail[0]['message'];
			$contents = strip_tags($contents);
			$contents = codeconvert($contents);
			$contents = $conn->real_escape_string($contents);
			
			//$attachments = (count($full_mail_detail[0]['attach_parts'])>0) ? $conn->real_escape_string(serialize($full_mail_detail[0]['attach_parts'])) : null ;  //array
			//$attachments = (count($full_mail_detail[0]['attach_parts'])>0) ? $conn->real_escape_string(implode('|',Get_Array_By_Key($full_mail_detail[0]['attach_parts'],'FileName'))) : null ;  //array

			$attachments = '';
			$attachDelim = '';
			if(isset($full_mail_detail[0]['attach_parts']) && sizeof($full_mail_detail[0]['attach_parts'])>0){
				foreach($full_mail_detail[0]['attach_parts'] as $attach_ind => $attach_detail){
					$attachments .= $attachDelim.$attach_detail['FileName'];
					$attachDelim = '|';
				}
			}
			$attachments = $conn->real_escape_string($attachments);

			$importance = $MailPriority[$uid]['ImportantFlag'];   //string
			$importance = ($importance=="true") ? 1 : 0 ;

			$flagged = $MailHeaderList->Flagged; //string
			$flagged = ($flagged=="F") ? 1 : 0 ;

			$has_read = $MailHeaderList->Unseen; //string
			$has_read = ($has_read=="U") ? 0 : 1 ;
			if ($has_read==0) {    //for indexdb. change back the status after getMailRecord() if original unread.
				$IMap->unreadMail($Folder[$j], $uid);
			}

			$has_replied = $MailHeaderList->Answered; //string
			$has_replied = ($has_replied=="A") ? 1 : 0 ;

			$has_forwarded = $Flags[$uid]['forwarded']; //boolean
			$has_forwarded = ($has_forwarded==true) ? 1 : 0 ;

			$system_tags;

			$total_emails_count++;

			//************* insert new record ***************
			$sql = "INSERT INTO emails (user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward, system_tags) 
					VALUES ('$user_id', '$date', '$folder_name', '$uid', '$from', '$to', '$cc', '$bcc', '$subject', '$contents', '$attachments', '$importance', '$flagged', '$has_read', '$has_replied', '$has_forwarded', '$system_tags')";
			
			if ($conn->query($sql) === TRUE) {
			    //echo $j." ".$i." ".$folder_name." ";
			    $total_emails_imported++;
			} else {
			    echo "Error: " . $sql . "<br />\n" . $conn->error."<br />\n";
			}


			if( $i % 50 == 0){
				echo "\n";
				flush();
				usleep(10);
			}
		}
	}
	
	echo "$user_email processed.<br />\n";
	flush();
}
intranet_closedb();
$conn->close();

echo 'Total emails imported: '.$total_emails_imported." / $total_emails_count.<br />\n";

echo "Stop at ".date("Y-m-d H:i:s").".<br />\n";

function codeconvert($input){

  if( !empty($input) ){

    $encodetype = mb_detect_encoding($input , array('UTF-8','BIG5','GB2312','GBK','ASCII')) ;

	if( $encodetype != 'UTF-8' && $encodetype != false){

      $input = mb_convert_encoding($input ,'utf-8' , $encodetype);
      //echo $encodetype."<br>";
      //echo $input;
    }
  }

  return $input;

}
//for user list retrieving
function request_by_curl($remote_server, $post_string) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $remote_server);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //curl_setopt($ch, CURLOPT_USERAGENT, "jb51.net's CURL Example beta");
  $data = curl_exec($ch);
  curl_close($ch);
 
  return $data;
}

function escape_sql_value($value) {
    return str_replace(array("\\","'"),array("\\\\","\\'"), $value);
}

?>