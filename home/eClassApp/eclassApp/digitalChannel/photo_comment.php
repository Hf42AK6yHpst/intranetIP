<?php 

/* change log:
 * 2017-01-16 Roy:
 * 		- scroll to bottom after data load
 * 		- add margin for the input box
 * 		- disable click content to toggle input box
 * 		- hide input box if not allow leave comment flag is enabled
 * 		- use custom submit function when user press keyboard enter key
 * */
$PATH_WRT_ROOT = '../../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);
$intranet_session_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/DigitalChannels/libdigitalchannels.php');
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}
$libdigitalchannels = new libdigitalchannels();
$libDB = new libdb();
intranet_opendb();
$libuser = new libuser();   
$albumPhotoComment = $libdigitalchannels->AppGetAlbumPhotoComment($photoID);

$uidAry = array();
for($i=0;$i<count($albumPhotoComment);$i++){
    	$uidAry[] = $albumPhotoComment[$i]['UserID'];
}
$uidAry[] = $uid;
$uidAryString = implode(",", array_unique($uidAry));

$userObj = new libuser('', '', array_values(array_unique($uidAry)));

$lgs = new libgeneralsettings();
$settings = $lgs->Get_General_Setting("DigitalChannels", array("'DisallowUserToComment'"));


$x = "<div id='my-wrapper' style='height:100%; margin-bottom:64px'>";
  for($i=0;$i<count($albumPhotoComment);$i++){
  	
  	$tempComment = $albumPhotoComment[$i];
  	
  	$userObj->LoadUserData($tempComment['UserID']);
  	$tempName = Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName);

	$_personalphotoPathAry = $libuser->GET_USER_PHOTO($userObj->UserLogin);
	$tempUserPhoto = $_personalphotoPathAry[1];
  	
  	$tempDate = date('d/m/Y', strtotime($albumPhotoComment[$i]['DateInput']));
  	$tempContent = $albumPhotoComment[$i]['Content'];
          
   $x .= '<table style="width:100%;padding-bottom:0px;background-color:white;"><tbody><tr>'.
         '<td style="width:50px;height:50px;padding-left: 20px;">'.
         '<img src="'.$tempUserPhoto.'" align="left" style="height: 50px;!important;background-size: 50px 50px!important;"></td>'.
         '<th class="my-column">'.
         '<table style="width:100%;padding-bottom:0px;"><tbody><tr>'.
         '<td style="width:50%;"><p align="left" style="margin-top: 4px;margin-bottom: 4px;">'.$tempName.'</p></td>'.
         '<td style="width:30%;padding-right: 20px;"><p align="right" style="margin-top: 4px;margin-bottom: 4px;">'.$tempDate.'</p></td>'.
         '</tr><tr><td colspan="2" style="width:75%;max-width:10px;padding-right: 20px;" class="my-commentcolumn"><div class="my-messageWarpContent"><p class="ui-li-text-my-h3" style="text-align:left;margin-top: 4px;margin-bottom: 4px;">'.$tempContent.'</p></div></td>'.
         '</tr></tbody></table></th></tr></tbody></table>';
   $x .= '<div class="horizon-line"></div>';      
  }
$x .= "<div id='bottom'></div>";
$x .= "</div>";
$_personalphotoPathAry = array();
$tempUserPhoto="";
$userObj->LoadUserData($uid);
$_personalphotoPathAry = $libuser->GET_USER_PHOTO($userObj->UserLogin);
$tempUserPhoto = $_personalphotoPathAry[1];

$submitCommentIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/eClassApp/parentApp/digital_channels/icon_photoAlbum_arrow_up.png"."\"";
$submitCommentLink = "submitComment.php?token=$token&uid=$uid&ul=$ul&parLang=$intranet_hardcode_lang&photoID=$photoID";

if ($settings['DisallowUserToComment']=='1') {
	$x .= "<div data-role='footer' data-position='fixed' data-tap-toggle='false' data-fullscreen='true' style='background-color:#FFFFFF !important;'>
			</div>";
} else {
	$x .= "<div data-role='footer' data-position='fixed' data-tap-toggle='false' data-fullscreen='true' style='background-color:#FFFFFF !important;'>
				<form class='ui-filterable' id='submit_comment_form' name='submit_comment_form' method='post' action='$submitCommentLink' enctype='multipart/form-data'>
					<table style='width:100%;'>
						<tr>
							<td style='width:50px;height:50px;padding-left: 10px;'><img src='$tempUserPhoto' align='left' style='height: 50px;!important;background-size: 50px 50px!important;'></td>
							<td><input type='text' id='Comment' name='Comment' style='width:80%;'></td>
							<td style='width:50px;height:50px;'><a href='#' id = 'SubmitCommentButton' data-position-to='window' data-role='button'  class =' ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon' data-icon='myapp-submit-comment' style='width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;margin-bottom: 15px;'></a></td>
						</tr>
					</table>
				</form>
			</div>";
}

$albumPhotoAry = $libdigitalchannels->AppGetPhoto($uid,'',$photoID);

$CommentTotal = $albumPhotoAry[0]['CommentTotal'];
$FavoriteTotal = $albumPhotoAry[0]['FavoriteTotal'];
$ViewTotal = $albumPhotoAry[0]['ViewTotal'];
echo $libeClassApp->getAppWebPageInitStart();
?>
	<style type="text/css">
	 .ui-icon-myapp-submit-comment {
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$submitCommentIconUrl?>) no-repeat !important;
	 }
	.ui-page{
			background:url(<?php echo $photoLink?>) no-repeat;
			background-size:cover;
     }

	 #photoInfo{
			position: absolute;
			left: 0;
			bottom: 0;
			opacity:0.8;
			padding-bottom:80px
	 }
	 #photo_navbar{
	 	    position: absolute;
			bottom: 0;
			width:100% !important;
	 }
 	 .ui-icon-myapp-star {
 	 	     width:30px;
 	 	     height:30px;
			 border: 0 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$star_icon_url?>) no-repeat !important;
	 }
	.ui-icon-myapp-comment {
		 	 width:30px;
 	 	     height:30px;
			 border: 0 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$comment_icon_url?>) no-repeat !important;
	 }
	.ui-icon-myapp-download {
		 	 width:30px;
 	 	     height:30px;
			 border: 0 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$download_icon_url?>) no-repeat !important;
	 }
	 .ui-icon-myapp-i {
	 	 	 width:30px;
 	 	     height:30px;
			 border: 0 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$i_icon_url?>) no-repeat !important;
	 }
	 
    div.my-messageWarpContent{
	   margin:0 0 0 0 !important;
	   height:auto;
	   word-wrap: break-word;
	}
	   
   	td.my-commentcolumn{
       margin:0 0 0 0 !important;
	   height:auto;
	   word-wrap: break-word;
    }
	 th.my-column{
    	width:-moz-calc(100% - 100px);
    }
    
	.ui-li-text-my-h3{
      	color:black;
    	font-size:0.95em!important;
    	font-weight: normal !important
    }
    
	.horizon-line {
	   width: 100%;
	   height: 1px;
	   background-color: #DDDDDD;
	   margin: 0 auto;
	}
	</style>
	<script>
	 $(document).ready( function() {
	 	
		$("#SubmitCommentButton").bind('mousedown', function(){
	  		SubmitComment();
	  	});

		// disable default submit function, use custom one
		$("#submit_comment_form").submit(function(event) {
			event.preventDefault();
			SubmitComment();
		});
	});

	$(document).on("pagecontainershow", function(){
		ScrollToBottom();
	});
		
	function SubmitComment(){
		// this handle form submission from pressing send button in UI
		var commentContent = $('#Comment').val();
     	commentContent = commentContent.replace(/\s+/g, "");
		if(commentContent.length == 0) {
			$('#Comment').val('');
			alert("<?=$Lang['eClassApp']['DigitalChannels']['Comment_no_empty']?>");
		} else {
			// submit form
			document.submit_comment_form.submit();
		}
	}
	
    function get_Comment_Fav_Data(){
      return "<?='CommentTotal='.$CommentTotal.'&FavoriteTotal='.$FavoriteTotal.'&ViewTotal='.$ViewTotal?>";
    }
    
    function ScrollToBottom() {
// 		window.scrollTo(0, document.body.scrollHeight);

    	var last_li = $("#bottom").offset().top;
        $("body").animate({
            scrollTop: last_li
        }, 1000);
    }
	
	</script>
</head>
	<body>	
		<div data-role="page" id="body">			
   	        <div data-role="content" style="padding-left:0px;">
   	        <?=$x?>
   	        </div>
   	    </div>
   	</body>
</html>
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>