<?php
/*
 *	Modification Log:
 *
 *  2020-07-28 (Bill)   [2020-0728-1728-56066]
 *      - teacher app > support bulk mode to send push msg & pass id for module relation
 *
 *  2019-12-05 Philips  [2019-0628-1034-52206]
 *  	- Send Push Message without cheching app login status
 * 
 *	2019-10-18 (Philips)
 *		- fixed: cannot send to parent and student
 *
 *  2019-05-10 (Bill)
 *      - prevent SQL Injection
 *      - apply intranet_auth(), token checking
 *      
 */

    $PATH_WRT_ROOT = '../../../../';
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libgrouping.php");
	include_once($PATH_WRT_ROOT."includes/libemail.php");
	include_once($PATH_WRT_ROOT."includes/libsendmail.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
	
	intranet_auth();
	intranet_opendb();
	
	$libeClassApp = new libeClassApp();
	$isTokenValid = $libeClassApp->isTokenValid($token, $UserID, $ul);
	if(!$isTokenValid) {
	    $libeClassApp->handleInvalidToken();
	}
	
	$li = new libgrouping();
    
    $Updated = "N";
    
    $pushMsgTitle = standardizeFormPostValue($Title);
	$Title = intranet_htmlspecialchars(trim($Title));
	if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
	{
		if ($Description==strip_tags($Description))
		{
			$Description = nl2br($Description);
		}
	}
	$Description = intranet_htmlspecialchars(trim($Description));
	$AnnouncementDate = intranet_htmlspecialchars(trim($AnnouncementDate));
	$EndDate = intranet_htmlspecialchars(trim($EndDate));
	$startStamp = strtotime($AnnouncementDate);
	$endStamp = strtotime($EndDate);
	$AnnouncementDate = date('Y-m-d',$startStamp);
	$EndDate = date('Y-m-d',$endStamp);
	$RecordStatus = IntegerSafe($RecordStatus);
	
	$name_field = getNameFieldForRecord("");
	$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$UserID' ";
	$temp = $li->returnVector($sql);
	$PosterName = $temp[0];
	
	if (compareDate($startStamp,$endStamp)>0)   // start > end
	{
        $valid = false;
	}
	else if (compareDate($startStamp,time())<0)      // start < now
	{
        $valid = false;
	}
	else
	{
        $valid = true;
	}
    $folder = session_id()."_a";
    
	if ($valid)
	{
		$sql = "INSERT INTO INTRANET_ANNOUNCEMENT (Title, Description, AnnouncementDate, ReadFlag, UserID, PosterName, RecordStatus, DateInput, DateModified, EndDate) VALUES ('$Title', '$Description', '$AnnouncementDate', ';$UserID;', $UserID, '$PosterName', '$RecordStatus', now(), now(),'$EndDate')";
		$success = $li->db_db_query($sql);
		if($success){
			$Updated = "Y";
		}
		$AnnouncementID = $li->db_insert_id();
		
		$GroupID = $_POST['GroupID'];
		$GroupID = IntegerSafe($GroupID);
		for($i=0; $i<sizeof($GroupID); $i++){
             $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES ('".$GroupID[$i]."', '$AnnouncementID')";
             $li->db_db_query($sql);
		}
		
	    # Set attachment
		$lf = new libfilesystem();
		$announcementdir = "$file_path/file/announcement/";
		$path = "$file_path/file/announcement/$folder$AnnouncementID";
		$hasAttachment = false;
		$attachment_size = count($_FILES[multiplefile][name]);
		for ($i=0; $i<$attachment_size; $i++)
		{
			$loc = $_FILES[multiplefile][tmp_name][$i];
			// Getting file name
			$file = $_FILES[multiplefile][name][$i];	
			
			// for ios upload
			$fileexplode = explode(".",$file);
			$filename = $fileexplode[0];
			$fileext = $fileexplode[1];
            if($filename == "image"){
            	$file = "image".($i+1).".".$fileext;
            }
            
			$des = "$path"."/".stripslashes($file);	
			if ($loc == "none" || trim($loc) == "")
			{
			    // do nothing
			} 
			else
			{
				if (strpos($file,"."==0))
				{
				    // do nothing
				} 
				else
				{
					if (!$hasAttachment)
					{
						 $lf->folder_new ($announcementdir);
						 $lf->folder_new ($path);
						 $hasAttachment = true;
					}
					$lf->lfs_copy($loc, $des);
				}
			}
		}
		
		# Update attachment in DB
		if ($hasAttachment)
		{
			$sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$folder' WHERE AnnouncementID = '$AnnouncementID'";
			$li->db_db_query($sql);
		}
	    if ($publicdisplay == 1)
		{
			$sql = "UPDATE INTRANET_ANNOUNCEMENT SET RecordType = 1 WHERE AnnouncementID = '$AnnouncementID'";
			$li->db_db_query($sql);
		}
		
		# publish notification:
		$type = (sizeof($GroupID)==0? $Lang['EmailNotification']['SchoolNews']['School']: $Lang['EmailNotification']['SchoolNews']['Group']);
		if (sizeof($GroupID)>0)
		{
			$Groups = $li->returnGroupNames($GroupID);
		}
		if(sizeof($GroupID) == 0)
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus = '1'";
		}
		else
		{
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID IN (".implode(",",$GroupID).")";
		}
		$ToArray = $li->returnVector($sql);
		
		include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
		$lschoolnews = new libschoolnews();
		
		# Call sendmail function
		if($email_alert==1)
		{		
			list($mailSubject, $mailBody) = $lschoolnews->returnEmailNotificationData($AnnouncementDate,$pushMsgTitle,$type, $Groups);
			
			$lwebmail = new libwebmail();
			$lwebmail->sendModuleMail($ToArray,$mailSubject,$mailBody);
		}
		
		# send push message
		if ($push_message_alert == 1)
		{
			include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			
			$libeClassApp = new libeClassApp();
			
			# build teacher array and student array
			$teacherAry = array();
			$studentAry = array();
			$studentAry_parent = array();
			foreach ($ToArray as $userId) {
				$luser = new libuser($userId);

				if ($luser->RecordType == 1) { // teacher
					$teacherAry[] = $userId;
				}
				else if ($luser->RecordType == 2) { // student
					$studentAry[] = $userId;

					// [2017-0904-1103-25073]
					if(!$sys_custom['schoolNews']['disableSendToParentAlert'] && $stu_parent_alert == 1) {
					    $studentAry_parent[] = $userId;
					}
				}
				else if ($luser->RecordType == 3) { // parent
					$childrenIds = $luser->getChildren();
                    //$studentAry = array_merge($studentAry, $childrenIds);
					$studentAry_parent = array_merge($studentAry_parent, $childrenIds);
				}
			}
// 			$studentsWithParentUsingParentApp = $luser->getStudentWithParentUsingParentApp();
// 			$studentsUsingStudentApp = $luser->getStudentWithStudentUsingStudentApp($includeNoPushMessageUser=true);
// 			$teachersUsingTeacherApp = $luser->getTeacherWithTeacherUsingTeacherApp();
// 			$studentAryForParentApp = array_intersect($studentAry_parent, $studentsWithParentUsingParentApp);
// 			$studentAry = array_intersect($studentAry, $studentsUsingStudentApp);
// 			$teacherAry = array_intersect($teacherAry, $teachersUsingTeacherApp);
			
			### 20191205 Philips [2019-0628-1034-52206]
			### Send Push Message Whether app login status
			if(!$sys_custom['schoolNews']['disableSendToParentAlert'] && $stu_parent_alert == 1) {
				$studentAryForParentApp = array_merge($studentAry_parent, $studentAry);
			} else {
				$studentAryForParentApp = $studentAry_parent;
			}
			$studentAryForStudentApp = $studentAry;
			
			# Build message content
			$isPublic = "N";
			$sendTimeMode = standardizeFormPostValue($_POST['sendTimeMode']);
			$sendTimeString = standardizeFormPostValue($_POST['sendTimeString']);
			list($pushmessage_subject, $pushmessage_body) = $lschoolnews->returnPushMessageNotificationData($AnnouncementDate,$pushMsgTitle,$type, $Groups);
		    
			# Send Message to Parent App
			$appType = $eclassAppConfig['appType']['Parent'];
			$individualMessageInfoAry = array();
			if (!empty($studentAryForParentApp)) {
			    $parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentAryForParentApp), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			}
			else {
			    $parentStudentAssoAry = array();
			}
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;

			if (!empty($parentStudentAssoAry)) {
			    if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
			        $notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
			    }
			    else {
			        $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
			    }
			}
			
			/*
			# send message to parent app
			$appType = $eclassAppConfig['appType']['Parent'];
			$individualMessageInfoAry = array();
			if (!empty($studentAry)) {
				$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentAry), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			} else {
				$parentStudentAssoAry = array();
			}
	    	$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
	    	
			if (!empty($parentStudentAssoAry)) {
				$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
			}*/
			
			# Send message to teacher app
			$appType = $eclassAppConfig['appType']['Teacher'];
			$teacherIndividualMessageInfoAry = array();
			if (!empty($teacherAry)) {
				foreach ($teacherAry as $teacherId) {
					$_targetTeacherId = $libeClassApp->getDemoSiteUserId($teacherId);
					// link the message to be related to oneself
					$teacherAssoAry[$teacherId] = array($_targetTeacherId);
				}
			}
			else {
				$teacherAssoAry = array();
			}
	    	$teacherIndividualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
			if (!empty($teacherAssoAry)) {
                // [2020-0728-1728-56066]  teacher app > support bulk mode to send push msg & pass record id for module relation
				//$notifyMessageId = $libeClassApp->sendPushMessage($teacherIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
                if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
                    $notifyMessageId = $libeClassApp->sendPushMessageByBatch($teacherIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
                }
                else {
                    $notifyMessageId = $libeClassApp->sendPushMessage($teacherIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
                }
			}
			
			# Send Message to Student App
			$appType = $eclassAppConfig['appType']['Student'];
			$studentAssoAry = array();
			$studentIndividualMessageInfoAry = array();
			if (!empty($studentAry)) {
				foreach ($studentAry as $_studentId) {
					$_targetStudentId = $libeClassApp->getDemoSiteUserId($_studentId);
					// link the message to be related to oneself
					$studentAssoAry[$_studentId] = array($_targetStudentId);
				}
			}
			else {
				$studentAssoAry = array();
			}
			$studentIndividualMessageInfoAry[0]['relatedUserIdAssoAry'] = $studentAssoAry;
			if (!empty($studentAssoAry)) {
				if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
					$notifyMessageId = $libeClassApp->sendPushMessageByBatch($studentIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
				}
				else {
					$notifyMessageId = $libeClassApp->sendPushMessage($studentIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
				}
			}
		}
	}
	
	intranet_closedb();
	header ("Location: schoolNews_new.php?token=$token&uid=$UserID&ul=$ul&parLang=$parLang&Updated=".$Updated);
?>