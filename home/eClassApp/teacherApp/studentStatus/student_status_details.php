<?php
/*
 *  2020-04-06 Cameron
 *      - show email for emergency contact person if $sys_custom['eClassTeacherApp']['StudentListShowGuardianEmail'] is set
 */
$PATH_WRT_ROOT = '../../../../';

//include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
//$leClassApp_init = new libeClassApp_init();
//$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT.'includes/libteaching.php');

intranet_auth();
intranet_opendb();

$uid = IntegerSafe($_GET['uid']);
$year_class_id = IntegerSafe($_GET['year_class_id']);
$user_id = IntegerSafe($_GET['user_id']);

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

$libDB = new libdb();
$libuser = new libuser($uid,$ul);
$libteaching = new libteaching();
$AcademicYearID = Get_Current_Academic_Year_ID();
$result = $libteaching->returnTeacherClassWithLevel($uid,$AcademicYearID);
if(count($result)>0){
	#classTeacher
	$allowViewStudentContactKey = 'intranetAPPClassTeacherAllowViewStudentContact';
}else{
	$allowViewStudentContactKey = 'intranetAPPNonClassTeacherAllowViewStudentContact';
}
    $eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
    $isViewStudentContact = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName'][$allowViewStudentContactKey]);
if(!$isViewStudentContact) {
	if(count($result)>0) {
		$isViewStudentContact = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContactClass']);
	}
}

//  /$sql = "select * from INTRANET_USER WHERE UserID =".$user_id;
 $sql = "select * from INTRANET_USER WHERE UserID = '".$user_id."'";
 $result = $libDB->returnArray($sql);
 $SQL = "SELECT EnName,ChName,Relation,EmPhone FROM $eclass_db.GUARDIAN_STUDENT WHERE UserID = '".$user_id."'";
 $emInfo = $libDB->returnArray($SQL);
 
 if ($sys_custom['eClassTeacherApp']['StudentListShowGuardianEmail']) {
    $sql = "SELECT EMAIL, PG_TYPE FROM STUDENT_REGISTRY_PG WHERE StudentID='".$user_id."' ORDER BY SEQUENCE";
    $guardianInfo = $libDB->returnResultSet($sql);
 }
 
 $sql = "Select AdmissionDate,Nationality From INTRANET_USER_PERSONAL_SETTINGS WHERE UserID = '".$user_id."'";
 $extraInfoResult = $libDB->returnArray($sql);
 $_groupLangrage = Get_Lang_Selection('TitleChinese', 'Title');
 $phoneIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/eClassApp/teacherApp/teacherStatus/icon_phone.png"."\"";
 $emailIconUrl =  "\""."{$image_path}/{$LAYOUT_SKIN}"."/eClassApp/teacherApp/teacherStatus/icon_email.png"."\"";
 $myBackIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_outbox_white.png"."\"";
 $sql = "Select ig.".$_groupLangrage." as Title From INTRANET_USERGROUP As iu INNER JOIN INTRANET_GROUP as ig ON iu.GroupID=ig.GroupID  WHERE ig.RecordType = 5 And ig.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND iu.UserID=".$user_id;
 $intranet_group = $libDB->returnArray($sql);
 if(!empty($extraInfoResult[0])){
  $_student = array_merge($result[0],$extraInfoResult[0]);
 }else{
  $_student = $result[0];
 }
// UserLogin
 $_personalphotoPathAry = $libuser->GET_USER_PHOTO($_student['UserLogin']);
 $_personalphotoPath = $_personalphotoPathAry[1];//!empty($_student['PhotoLink'])? $_student['PhotoLink'] : '/images/myaccount_personalinfo/samplephoto.gif';
 $_chName = !empty($_student['ChineseName'])? $_student['ChineseName'] : '---';
 $_enName = !empty($_student['EnglishName'])? $_student['EnglishName'] : '---';
 $_gender = '---';
 if($_student['Gender'] == 'M'){
 	 $_gender = $Lang['eClassApp']['StudentStatus']['MALE'];
 }else if($_student['Gender'] == 'F'){
 	 $_gender = $Lang['eClassApp']['StudentStatus']['FEMALE'];
 }
 
  if($_student['DateOfBirth'] == "0000-00-00 00:00:00"){
 	$_dateOfBirth = "---";
 }else{
 	 $_dateOfBirth = !empty($_student['DateOfBirth'])? date('Y-m-d',strtotime($_student['DateOfBirth'])) : '---';
 }
 $_homeTelNo = !empty($_student['HomeTelNo'])?$_student['HomeTelNo']:'---';
 $_mobileTelNo = !empty($_student['MobileTelNo'])?$_student['MobileTelNo']:'---';
 $_imapUserEmail = !empty($_student['ImapUserEmail'])?$_student['ImapUserEmail']:'---';
 if($plugin['imail_gamma'] && (!empty($_student['ImapUserEmail']))){
 	$_imapUserEmail = $_student['ImapUserEmail'];
 }else {
 	$_imapUserEmail = !empty($_student['UserEmail'])?$_student['UserEmail']:'---';
 }
 $_address = !empty($_student['Address'])?$_student['Address']:'---';
 
 #early leave icon
 $earlyleaveicon="";
 if($leave_status == 1 || $leave_status == 2){
	//$earlyleaveicon = "<img src=\"$image_path/{$LAYOUT_SKIN}/attendance/icon_early_leave_bw.gif\" width=\"12\" height=\"12\" />";
 	$earlyleaveicon= $Lang['eClassApp']['StudentStatus']['EARLYLEAVE'] ;
 }
 if($user_status =='0'){
		$_currentStatus = $Lang['eClassApp']['StudentStatus']['ONTIME'];
		$statusColorCode ="#7EC400";
	}else if($user_status =='2'){
		$_currentStatus = $Lang['eClassApp']['StudentStatus']['LATE'];
		$statusColorCode ="#2792C1";
	}else if($user_status =='3'){
		$_currentStatus = $Lang['eClassApp']['StudentStatus']['OUT'];
		$statusColorCode ="#888888";
	}else{
		$_currentStatus = $Lang['eClassApp']['StudentStatus']['UNAVAILABLE'];
		$statusColorCode ="#D3292B";
	}
 if($intranet_hardcode_lang =='b5'){
  $_studentName = $_chName;
 }else{
  $_studentName = $_enName;
 }
  $linterface = new interface_html();
  $refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);
 $header = ' <a  href="#" data-icon="myapp-myBack" data-position-to="window" data-role="button" data-rel="back" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 10px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;"></a>';
 $header .= "<h1 style ='text-align: center;width=100%;font-size:20px;color:white;text-shadow: none!important;'>".$_studentName."</h1>";
 $x = "<div id='my-wrapper'><ul data-role='listview' id='defalt_detail_list' data-inset='false' data-split-icon='plus' style = 'margin-bottom: 0px;'>";
 $x .= "<li >".
                       "<table style = 'width:100%'><tr>" .
                       "<th style ='width:20%;'><img src=".$_personalphotoPath." style = 'height: 145px;width:110px!important;background-size: 100px 130px!important;top:5px;'></th>" .
                       "<td style ='width:80%;'>".
                       "<table style = 'width:100%'><tr>".
                       "<td style ='width:100%;padding-bottom:0px;'><h1 style='font-size:25px;color:#429DEA;margin-top:0px;margin-bottom:0px;'>".$_chName."</h1></td>".
                       "</tr>".
                       "<tr>".
                       "<td style ='width:100%;padding-top:0px;'><h1 style='color:#429DEA;margin-top:0px;margin-bottom:0px;'>".$_enName."</h1></td>".
                       "</tr>".
                       "<tr>".
                       "<td style ='width:100%;'><h1 style='margin-top:10px;margin-bottom:10px;'>".$_student['ClassName']." (".$_student['ClassNumber']."), ".$_gender."</h1></td>".
                       "</tr>".
                       "<tr><td style ='padding-left:0px'><table style = 'width:100%'><tr>";
                       if(!$sys_custom['eClassApp']['studentListHideBirthday']){
                       	  $x .= "<td style ='padding-left:0px;width:60%'>".$Lang['eClassApp']['StudentStatus']['BIRTHDATE'].":"."</td>";                       	
                       }
                       if($plugin['attendancestudent']){
                       	  $x .= "<td style = 'width:40%'>".$Lang['eClassApp']['StudentStatus']['STATUS'].":"."</td>";
                       }                      
 $x .=                 "</tr>".
                       "<tr>";
                       if(!$sys_custom['eClassApp']['studentListHideBirthday']){
                          $x .="<td style ='padding-left:0px;width:60%'>".$_dateOfBirth."</td>";
                       }
                       if($plugin['attendancestudent']){
                       	  $x .= "<td style = 'width:30%'><h1 style= 'color:".$statusColorCode.";margin-top:0px;margin-bottom:0px;'>".$_currentStatus."</h1><h1 style= 'color:#808080;margin-top:0px;margin-bottom:0px;'> ".$earlyleaveicon."</td>";
                       }                       
 $x .=                 "</tr></table></td></tr>".
                       "</table></td>".
                       "</tr></table></li>"."\r\n";
    if($isViewStudentContact){
    	 $x .= "<li style ='background-color:#f1f1f1;padding-top:0px;padding-bottom:0px'><h1>".$Lang['eClassApp']['StudentStatus']['CONTACT']."</h1></li>";
		 $x .= "<li style ='padding-top:0px;padding-bottom:0px'>".
	                   "<table style='width:100%'><tr>" .
	                   "<td style ='width:10%;'><h1 style='color:#888888;'>".$Lang['eClassApp']['StudentStatus']['HOME']."</h1></td>".
	                   "<td style ='width:75%;'><h1 style='color:#7EC400; text-align: center;'>".$_homeTelNo."</h1></td>".
	                   "<td style ='width:15%;'><a href='#' data-icon='myapp-phone' id = '".$_student['HomeTelNo']."' data-position-to='window' data-role='button' class = 'ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon' style='width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 5px;margin-top:0px;border:none;' onclick = 'Dial(this.id);'></a></td>".
	                   "</tr></table></li>"."\r\n";;
		 $x .= "<li data-icon='false' style = 'white-space:normal!important;padding-top:0px;padding-bottom:0px;'> <table style='width:100%'><tr>".
	                   "<td style ='width:10%;'><h1 style='color:#888888;'>".$Lang['eClassApp']['StudentStatus']['MOBILEPHONE']."</h1></td>".
	                   "<td style ='width:75%;'><h1 style='color:#7EC400;text-align: center;'>".$_mobileTelNo."</h1></td>".
	                   "<td style ='width:15%;'><a href='#' data-icon='myapp-phone' id = '".$_student['MobileTelNo']."' data-position-to='window' data-role='button' class = 'ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon' style='width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 5px;margin-top:0px;border:none' onclick = 'Dial(this.id);'></a></td>" .
	                   "</tr></table></li>"."\r\n";
		 $_emTelNo = array();
		 
		 $i=1;
		 if(empty($emInfo)){
		 	
// 		 	$x .= "<li data-icon='false' style = 'white-space:normal!important;padding-top:0px;padding-bottom:0px;'> <table style='width:100%'><tr>".
// 				 	"<td style ='width:10%;'><h1 style='color:#888888;'>".$Lang['eClassApp']['StudentStatus']['EMERGRNCYPHONE']." ".$i."</h1></td>".
// 				 	"<td style ='width:75%;'><h1 style='color:#7EC400;text-align: center;'>---</h1></td>".
// 				 	"<td style ='width:15%;'><a href='#' data-icon='myapp-phone' id = '".$emTelNoAry['EmPhone']."' data-position-to='window' data-role='button' class = 'ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon' style='width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 5px;margin-top:0px;border:none' onclick = 'Dial(this.id);'></a></td>" .
// 				 	"</tr></table></li>"."\r\n";
		 }else{
		 	$x .= "<li style ='background-color:#f1f1f1;padding-top:0px;padding-bottom:0px'><h1>".$Lang['eClassApp']['StudentStatus']['EMERGENCYCONTACT']."</h1></li>";
		 	foreach($emInfo as $emTelNoAry){
		 		$_emTelNo = $emTelNoAry['EmPhone'];
		 		$_emName = $intranet_hardcode_lang=='b5'? $emTelNoAry['ChName']:$emTelNoAry['EnName'];
		 		$_emRelation = $emTelNoAry['Relation'];
		 		
		 		$x .= "<li data-icon='false' style = 'white-space:normal!important;padding-top:0px;padding-bottom:0px;'> <table style='width:100%'><tr>".
				 		"<td style ='width:10%;'><h1 style='color:#888888;'>".$_emName."<br>( ".$ec_guardian[$_emRelation]." )</h1></td>".
				 		"<td style ='width:75%;'><h1 style='color:#7EC400;text-align: right; padding-right: 70px;'>".$_emTelNo."</h1></td>".
				 		"<td style ='width:15%;'><a href='#' data-icon='myapp-phone' id = '".$_emTelNo."' data-position-to='window' data-role='button' class = 'ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon' style='width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 5px;margin-top:0px;border:none' onclick = 'Dial(this.id);'></a></td>" .
				 		"</tr>";

		 		if ($sys_custom['eClassTeacherApp']['StudentListShowGuardianEmail']) {
		 		    foreach((array)$guardianInfo as $_guardianAry) {
		 		        $_email = $_guardianAry['EMAIL'];
		 		        $_guardianType = $_guardianAry['PG_TYPE'];
		 		        if ((($_guardianType == 'F') && ($_emRelation == '01')) || (($_guardianType == 'M') && ($_emRelation == '02'))){
		 		            $x .= "<tr>".
    		 		            "<td colspan='3' class='my-wrapTextTable'>".$_email."</td>".
    		 		            "</tr>";
		 		        }
		 		    }
		 		}
		 		
				$x .= "</table></li>"."\r\n";
		 		$i++;
		 	}

		 }
		
		 
		 $x .= "<li style ='background-color:#f1f1f1;padding-top:0px;padding-bottom:0px'><h1>".$Lang['eClassApp']['StudentStatus']['EMAIL']."</h1></li></ul>";
		 
		 $x .= "<table style='width:100%'><tr>" .
		               "<td style='width:85%;max-width:10px;padding-right: 10px;padding-left: 0px;padding-bottom: 15px;' class='my-wrapTextTable'>".$_imapUserEmail."</td>".
		               "<td style='width:36px;height:36px;padding-bottom: 15px;'><a href='#' data-icon='myapp-imail' id = '".$user_id."' data-position-to='window' data-role='button' class = 'ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon' style='width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 5px;margin-top:0px;border:none;' onclick = 'SendEmail(this.id);'></a></td>".
		               "</tr></table>"."\r\n";
		 
		 $x .= "<ul data-role='listview' data-inset='false' data-split-icon='plus' style = 'margin-bottom: 0px;'>";                  
		 $x .= "<li style ='background-color:#f1f1f1;padding-top:0px;padding-bottom:0px;'><h1>".$Lang['eClassApp']['StudentStatus']['ADDRESS']."</h1></li>";
		 $x .= "<li data-icon='false' style = 'white-space:normal!important;'>".$_address."</li>"."\r\n";
    }
 
 $x .= "<li style ='background-color:#f1f1f1;padding-top:0px;padding-bottom:0px;'><h1>".$Lang['Group']['DefaultCategory'][5]."</h1></li>";
	 if(count($intranet_group)>0){
	 	for($i=0;$i<count($intranet_group);$i++){
	 	    $x .= "<li data-icon='false' style = 'white-space:normal!important;'>".$intranet_group[$i]['Title']."</li>"."\r\n";
	 	}
	 }else{
	 	    $x .= "<li data-icon='false' style = 'white-space:normal!important;'>---</li>"."\r\n";
	 }        
 
 $x .= "</ul></div>";
echo $libeClassApp->getAppWebPageInitStart();
?>
<style type="text/css">
 .ui-icon-myapp-phone {
	 background-color: transparent !important;
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$phoneIconUrl?>) no-repeat !important;
}
.ui-icon-myapp-imail{
	 background-color: transparent !important;
	 border: 1 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$emailIconUrl?>) no-repeat !important;
 }
 td.my-hiddenTextTable{ 
	  text-align:left; 
	  text-shadow: none!important;
	  max-width: 45px;
	  overflow: hidden;
	  text-overflow: ellipsis;
	  white-space: nowrap;
  } 
 ul li{
	  data-icon='false';
 }
.ui-icon-myapp-myBack{			
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$myBackIconUrl?>) no-repeat !important;
}
#loadingmsg {
	  color: black;
	  background:tranparent!important;
	  position: fixed;
	  top: 50%;
	  left: 50%;
	  z-index: 100;
	  margin: -8px 0px 0px -8px;
}
 #loadingover {
	  background: white;
	  z-index: 99;
	  width: 100%;
	  height: 100%;
	  position: fixed;
	  top: 0;
	  left: 0;
	  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
	  filter: alpha(opacity=80);
	  -moz-opacity: 0.8;
	  -khtml-opacity: 0.8;
	  opacity: 0.8;
}
td.my-wrapTextTable{
	   margin:0 0 0 0 !important;
	   height:auto;
	   word-wrap: break-word;
	   color:#7EC400;
}
</style>
<script>
function Dial(mobileTelNo){
	  if(mobileTelNo !=''){
	  var url = "tel:"+mobileTelNo.replace(/ /g,"");
	  location.href = url;
	  }else{
	  alert("<?=$Lang['eClassApp']['TeacherStatus_UserPhoneNo']?>");
	  }
}
function SendEmail(recipientID){
	showLoading();
	var url = "<?=$PATH_WRT_ROOT."home/imail_gamma/app_view/email_reply_forward_page.php?MailAction=Compose&TargetFolderName=StudentStatus&token=$token&uid=$uid&ul=$ul&parLang=$parLang&recipientID="?>"+recipientID;
			window.location.assign(url);
			}
//		function Back(){
//			    window.location = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentStatus/student_status_studentlist.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&class_name=<?=$_student['ClassName']?>&year_class_id=<?=$year_class_id?>";
//		}
function showLoading() {
    document.getElementById('loadingmsg').style.display = 'block';
    document.getElementById('loadingover').style.display = 'block';
}	
 </script>
</head>
	<body>	
		<div data-role="page" id="body">
		 <div data-role="header"  data-position="fixed" style ='background-color:#3689E4;'>	
	     <div id="loadingmsg" style="display: none;"><br><?=$refreshIconUrl?><br></div>
	     <div id="loadingover" style="display: none;"></div>
		 <?=$header?>
		 </div>	
   	        <div data-role="content">
   	         <?=$x?>
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>