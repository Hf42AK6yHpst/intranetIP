<?php
/*
 *	Modification Log:
 *
 *  2019-05-10 (Bill)
 *      - prevent SQL Injection
 *      - apply intranet_auth()
 */

$PATH_WRT_ROOT = '../../../../';
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");

$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$StudentID = IntegerSafe($StudentID);
$SubjectGroupID = IntegerSafe($SubjectGroupID);
### Handle SQL Injection + XSS [END]

$libeClassApp = new libeClassApp();
$libDB = new libdb(); 
//$_libUser = new libuser($uid,$ul);

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

intranet_auth();
intranet_opendb();

$libuser = new libuser($StudentID);
$student = $libuser->User;
$student = $student[0];

$positiveBG = $image_path."/".$LAYOUT_SKIN."/eClassApp/teacherApp/studentPerformance/btn_positive_score.png";
$negativeBG =  $image_path."/".$LAYOUT_SKIN."/eClassApp/teacherApp/studentPerformance/btn_negative_score.png";

$NameLangTag = Get_Lang_Selection('ChineseName', 'EnglishName');
$x = $libeClassApp->getCenterHeader($student['ClassName']."-".$student['ClassNumber']." ".$student[$NameLangTag]);

$score_active ='';
$comment_active ='';

$sql = "select PerformanceRecordID,Score, Comment,DateInput from INTRANET_APP_PERFORMANCE_STUDENT where StudentID='".$StudentID."' and SubjectGroupID='".$SubjectGroupID."'";
$result = $libDB->returnResultSet($sql);
$hashRecord = array();
for($i=0;$i<count($result);$i++){
	$tempDate = date('Y-m-d', strtotime($result[$i]['DateInput']));
	$tempPerformance = array();
	$tempPerformance['Score'] = $result[$i]['Score'];
	$tempPerformance['Comment'] = $result[$i]['Comment'];
	$tempPerformance['PerformanceRecordID'] =  $result[$i]['PerformanceRecordID'];
	$hashRecord[$tempDate][] = $tempPerformance;
}

	$score_content = '';
	$comment_content = '';
	$studentTotalScore = 0;
	#Score And Comment Nav List
    foreach ($hashRecord as $key => $value){
        $date_head = '<tr><td style="width:85px;text-align:left;vertical-align:top;padding-top:5px;padding-right:0px;">'.$key.'</td>';
        $tempPerformanceAry = $value;
        $tempScoreCounter = 0;$tempCommentCounter = 0;
        $score_list = '';$comment_list = '';
			for($j=0;$j<count($tempPerformanceAry);$j++){
				
				$tempPerformanceRecordID = $tempPerformanceAry[$j]['PerformanceRecordID'];
				
		        #SCORE
				$tempScore = $tempPerformanceAry[$j]['Score'];
				//if($tempScore != 0){
				if($tempScore !== null){
    				$tempScoreCounter++;
    				$studentTotalScore = $studentTotalScore + $tempScore;
    		    	$tempScoreBG = '';
    		    	$tempScoreTextColor = '';
    		    	if($tempScore<0){
    		    		$tempScoreBG = $negativeBG;
    		    		$tempScoreTextColor = '#F80008';
    		    	}else{
    		    		$tempScore = '+'.$tempScore;
    		    		$tempScoreBG = $positiveBG;
    		    		$tempScoreTextColor = '#5E9800';
    		    	}
    			    $score_list .= '<div id="'.$tempPerformanceRecordID.'" onClick="EditPerformanceRecord(this.id)" style="background-image: url(\''.$tempScoreBG.'\'); background-size: 100% 100%; height: 50px; width: 50px; float:left; line-height: 50px;color:'.$tempScoreTextColor.'">'.$tempScore.'</div>';
				}
			    #COMMENT
			    $tempComment = $tempPerformanceAry[$j]['Comment'];
			    if($tempComment!=null){
			   	$tempCommentCounter++;
			   	$comment_list .= '<tr><td style="padding-bottom:10px;" id="'.$tempPerformanceRecordID.'" onClick="EditPerformanceRecord(this.id)" >'.$tempComment.'<div id="comment_separator" style="margin-top:10px;"></div></td></tr>';
			    }
			}
		#add a list if there is at least one score.
		if($tempScoreCounter>0){
		$score_content .= $date_head.'<td><div id="ButtonBarDivUnit" class="ButtonBarDiv">'.$score_list.'</div></td></tr><tr><td></td><td><div id="comment_separator" style="margin-top:10px;"></div></td></tr>';
		}
		#add a list if there is at least one comment.
		if($tempCommentCounter>0){
		$comment_content .= $date_head.'<td style="-webkit-calc(100% - 100px);"><table style="width:100%">'.$comment_list.'</table></td></tr>';
		}
    }
	$_personalphotoPathAry = $libuser->GET_USER_PHOTO($student['UserLogin']);
	$student['PhotoLink'] = $_personalphotoPathAry[1];
	$x .= '<table style="width:100%;padding-left:5px"><tr>
		   <td style="width:100px"><img src='.$student['PhotoLink'].' align="left" style="height:130px;"></td>
		   <td style="-webkit-calc(100% - 100px);padding-top:0px"><table style="width:100%;"><tr>
		   <td style="width:100px;text-align:left;vertical-align:top;">'.$Lang['eClassApp']['StudentPerformance']['TotalActScore'].':</td></tr>
		   <tr><td style="width:100px;height:100px;text-align:center;vertical-align:middle;font-size:2.50em">'.$studentTotalScore.'</td></tr></table></td>
		   </tr></table>';
	$performance_content ='<div id="container">';
	$score_display = '';
	$comment_display = '';
	if($DetailTag=='Score'){
	    $score_active = 'class="ui-btn-active"';
		$score_display = 'show';
	    $comment_display = 'hidden';
	}else{
		$comment_active = 'class="ui-btn-active"';
		$score_display = 'hidden';
		$comment_display = 'show';
	}
	$performance_content .= '<div class='.$score_display.' id="container_score"><table style ="width:100%">'.$score_content.'</table></div><div class='.$comment_display.' id="container_comment"><table style ="width:100%">'.$comment_content.'</table></div></div>';
	$x .= '<div style="margin-top:10px"><div data-role="navbar">
	   <ul><li><a href="#" '.$score_active.' onClick="ShowScores();">'.$Lang['eClassApp']['StudentPerformance']['TotalScore'].'</a></li><li><a href="#" '.$comment_active.' onClick="ShowComments();">'.$Lang['eClassApp']['StudentPerformance']['Comment'].'</a></li></ul>
	   </div></div>';
	$x .= $performance_content;
#For navigation bar change
$score_content = str_replace("'","\'",$score_content);
echo $libeClassApp->getAppWebPageInitStart();
?>
	<style type="text/css">
	 .ui-icon-my-positiveBG{			
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$positiveBG?>) no-repeat !important;
	}
	.ui-icon-my-negativeBG{			
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$negativeBG?>) no-repeat !important;
	}
	.ButtonBarDiv div {
	display:table-cell!important;
	text-align:center !important;
	vertical-align:middle!important; 
	}
	
	.ButtonBarDiv input[type=image]
	{
	   vertical-align:middle !important;
	}
	#text
	{
	    z-index:100;
	    position:absolute;    
	    color:red;
	    font-size:10px;
	    font-weight:bold;
	    left:12px;
	    top:8px;
	}
	#comment_separator{
		background-color:#E3E3E3;
		height:1px;
	}
	
	div.hidden{
		display:none ;
	}
	
	div.show{
		display:block ;
	}
	</style>
	<script>
	 $(document).ready( function() {
	});
	
	function ShowScores(){
	$("#container_score").show();
	$("#container_comment").hide();
	}
	
	function ShowComments(){
	$("#container_score").hide();
	$("#container_comment").show();
	}
	
	function goBack(){
    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/group_student_performanceList.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&SubjectGroupID=<?=$SubjectGroupID?>&selectedSortID=<?=$selectedSortID?>&ChosenTermID=<?=$ChosenTermID?>";
	window.location = url;
    }
	function EditPerformanceRecord(id){
	    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/score_student_performance_page.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&SubjectGroupID=<?=$SubjectGroupID?>&DetailTag=<?=$DetailTag?>&StudentID=<?=$StudentID?>&selectedSortID=<?=$selectedSortID?>&ChosenTermID=<?=$ChosenTermID?>&PerformanceRecordID="+id;
	    window.location = url;
	}
	</script>
</head>
	<body>	
		<div data-role="page" id="body">			
   	        <div data-role="content">
   	        <?=$x?>
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>