<?php
/*
 *	Modification Log:
 *
 *  2019-05-10 (Bill)
 *      - prevent SQL Injection
 *      - apply intranet_auth()
 */

$PATH_WRT_ROOT = '../../../../';
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_stuPerformance.php');

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$libeClassAppStuPerformance = new libeClassApp_stuPerformance();

$uid = IntegerSafe($uid);
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

$StudentID = IntegerSafe($_GET["StudentID"]);
$SubjectGroupID = IntegerSafe($_GET["SubjectGroupID"]);
$Score = IntegerSafe(standardizeFormPostValue($_POST["Score"]));
$Comment = standardizeFormPostValue($_POST["Comment"]);
$PerformanceRecordID = IntegerSafe($_GET["PerformanceRecordID"]);
if($PerformanceRecordID != ""){
	$success = $libeClassAppStuPerformance->updateRecord($PerformanceRecordID, $Score, $Comment, $uid);
}else{
	$success = $libeClassAppStuPerformance->insertRecord($StudentID, $SubjectGroupID, $Score, $Comment, $uid);
}
  
intranet_closedb();
header('Location: '.$PATH_WRT_ROOT.'home/eClassApp/teacherApp/studentPerformance/group_student_performanceList.php?uid='.$uid.'&token='.$token.'&ul='.$ul.'&parLang='.$parLang.'&SubjectGroupID='.$SubjectGroupID.'&ChosenTermID='.$ChosenTermID.'&selectedSortID='.$selectedSortID);
exit;
?>