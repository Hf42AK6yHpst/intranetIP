<?php
## Using By: 
######################################
##	Modification Log:
##	2010-02-01: Max (201001271658)
##	- adjust the send email condition

## 2010-01-08 Max (201001071635)
## - Case the email send by "OnceEmail"
########################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

if ($plugin['eEnrollment'])
{
	$libenroll->Syn_Student_Number_Of_Approved_Club();
	
	$StuArr['EnrolGroupID'] = $EnrolGroupID;
	$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
	
	# all students who are time-crashed
	$crashedStudentArr = unserialize(rawurldecode($crashedStudentArr));
	
	# set the student list array
	if ($from == "addMemberResult")	
	{
		$UserArr = $crashedStudentArr;
	}
	else
	{
		$UserArr = $uid;
	}
	
	$result_ary = array();
	$imported_student = array();
	$crash_EnrolGroupID_ary = array();
	$crash_EnrolEventID_ary = array();	
	
	$QuotaLeft = $libenroll->GET_GROUP_QUOTA_LEFT($EnrolGroupID);
	
	for ($i = 0; $i < sizeof($UserArr); $i++) {
		$thisStudentID = $UserArr[$i];
		
		// do checking if not come back from add_member_result
		if ($from != "addMemberResult")	
		{
			// check if club quota exceeded
			if ($i >= $QuotaLeft)
			{
				//no quota left
				$result_ary[$i] = "club_quota_exceeded";
				continue;	
			}
	
			//check quota of student
			//$studentQuota = $libenroll->GET_STUDENT_QUOTA($thisStudentID);
			$studentQuotaLeft = $libenroll->GET_STUDENT_QUOTA_LEFT($thisStudentID);
			
			if ($libenroll->disableCheckingNoOfClubStuWantToJoin == false && $studentQuotaLeft <= 0)
			{
				//no quota left
				$result_ary[$i] = "student_quota_exceeded";
				continue;
			}
	
			//check if the student is approved already
			$sql = "SELECT RecordStatus FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '$EnrolGroupID' AND StudentID = ".$UserArr[$i];
			$result = $libenroll->returnArray($sql,1);
			if ($result[0][0]==2)
			{
				$result_ary[$i] = "student_is_member";
				continue;
			}
	
			$crashClub = false;
			// check for group crash
			$StudentClub = $libenroll->STUDENT_ENROLLED_CLUB($UserArr[$i]);  
			$numOfStudentClub = count($StudentClub);          	
			for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
				if ( ($libenroll->IS_CLUB_CRASH($EnrolGroupID, $StudentClub[$CountCrash]['EnrolGroupID'], $StudentClub)) ) {
					$crashClub = true;
					$crash_EnrolGroupID_ary[$i][] = $StudentClub[$CountCrash]['EnrolGroupID'];
				}
			}
			if ($crashClub)
			{
				$result_ary[$i] = "crash_group";
			}
	
			// check for event group crash
			// get event list
			$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($UserArr[$i]);
			$crashActivity = false;
			for ($CountEvent = 0; $CountEvent < sizeof($StudentEvent); $CountEvent++) {
				if ($libenroll->IS_EVENT_GROUP_CRASH($EnrolGroupID, $StudentEvent[$CountEvent][0])) {
					$crashActivity = true; 
					$crash_EnrolEventID_ary[$i][] = $StudentEvent[$CountEvent][0];
				}
			}
			if ($crashActivity)
			{
				$result_ary[$i] = "crash_activity";
			}
	
			if ($crashActivity || $crashClub)
			{
				continue;
			}
			
			/*
			# approve student
			$StuArr['StudentID'] = $UserArr[$i];
			$libenroll->APPROVE_GROUP_STU($StuArr);
	
			# add to INTRANET_USERGROUP
			$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, ApprovedBy) VALUES ($GroupID, ".$UserArr[$i].", $UserID)";
			$libenroll->db_db_query($sql);
			*/
			$libenroll->Add_Student_To_Club_Member_List($UserArr[$i], $EnrolGroupID);
			$libenroll->UpdateRole_UserGroup();
			array_push($imported_student, $UserArr[$i]);
			$email_student[] = $UserArr[$i];
		}
		else
		{
			// $uid is the approved students array
			if (in_array($UserArr[$i],$uid))
			{
				/*
				# approve student
				$StuArr['StudentID'] = $UserArr[$i];
				$libenroll->APPROVE_GROUP_STU($StuArr);
		
				# add to INTRANET_USERGROUP
				$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, ApprovedBy) VALUES ($GroupID, ".$UserArr[$i].", $UserID)";
				$libenroll->db_db_query($sql);
				*/
				$libenroll->Add_Student_To_Club_Member_List($UserArr[$i], $EnrolGroupID);
				$libenroll->UpdateRole_UserGroup();
				array_push($imported_student, $UserArr[$i]);
				$email_student[] = $UserArr[$i];
			}
			else
			{
				$result_ary[$i] = "rejected";
			}	
		}
	}
	
	$libenroll->UPDATE_GROUP_APPROVED($EnrolGroupID);
	
	// send email
	$RecordType = "club";
    $successResult = true;
	$libenroll->Send_Enrolment_Result_Email($email_student, $GroupID, $RecordType, $successResult);
	
	intranet_closedb();
	?>
	
	<body>
	<form name="form1" method="post" action="add_member_result.php">
		<input type="hidden" name="from" value="ClubEnrolList">
		<input type="hidden" name="type" value="-1">
		<input type="hidden" name="GroupID" value="<?=$GroupID?>">
		<input type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>">
		<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
		<input type="hidden" name="data" value="<?=rawurlencode(serialize($UserArr));?>">
		<input type="hidden" name="result_ary" value="<?=rawurlencode(serialize($result_ary));?>">
		<input type="hidden" name="imported_student" value="<?=rawurlencode(serialize($imported_student));?>">
		<input type="hidden" name="enrolStatus" value="2">
		<input type="hidden" name="crash_EnrolGroupID_ary" value="<?=rawurlencode(serialize($crash_EnrolGroupID_ary));?>">
		<input type="hidden" name="crash_EnrolEventID_ary" value="<?=rawurlencode(serialize($crash_EnrolEventID_ary));?>">
		<input type="hidden" name="filter" id="filter" value="<?= $filter?>" />
		<input type="hidden" name="field" id="field" value="<?= $field?>" />
		<input type="hidden" name="order" id="order" value="<?= $order?>" />
		<input type="hidden" name="keyword" id="keyword" value="<?= $keyword?>" />
	</form>

	<script language="Javascript">
		document.form1.submit();
	</script>
	</body>
<?
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>