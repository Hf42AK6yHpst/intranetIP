// JavaScript Document
// Change Status Color
function search(){
	$('div#filter').removeClass('in');
}
function filter(){
	$('div#search').removeClass('in');
}
function approved(){
	$('#header').removeClass('processing');
	$('#header').removeClass('rejected');
	$('#header').addClass('approved');

	$('#ole-tab').removeClass('processing');
	$('#ole-tab').removeClass('rejected');
	$('#ole-tab').addClass('approved');
}
// Change Status Color
function processing(){
	$('#header').removeClass('approved');
	$('#header').removeClass('rejected');
	$('#header').addClass('processing');

	$('#ole-tab').removeClass('approved');
	$('#ole-tab').removeClass('rejected');
	$('#ole-tab').addClass('processing');
}

function rejected(){
	$('#header').removeClass('approved');
	$('#header').removeClass('processing');
	$('#header').addClass('rejected');

	$('#ole-tab').removeClass('approved');
	$('#ole-tab').removeClass('processing');
	$('#ole-tab').addClass('rejected');
}

function setHeight(){
	var headerHeight = document.getElementById('header').offsetHeight;
	var tabHeight = document.getElementById('tab_header').offsetHeight;

	//var mainHeight = screen.height - headerHeight - tabHeight;
	var mainHeight = window.innerHeight - headerHeight - tabHeight;
	document.getElementById('tab-content').style.height = mainHeight+'px';
}

function setHeight2(){
	var headerHeight = document.getElementById('header').offsetHeight;
	var alertMsgHeight = document.getElementById('alertMsg').offsetHeight;
	var schoolEventTitleHeight = document.getElementById('schoolEventTitle').offsetHeight;

	//var mainHeight = screen.height - headerHeight - alertMsgHeight - schoolEventTitleHeight;
	var mainHeight = window.innerHeight - headerHeight - alertMsgHeight - schoolEventTitleHeight;
	document.getElementById('main').style.height = mainHeight+'px';
}

var element = $("#tab-content");
var lastHeight = $("#tab-content").css('height');

function checkForChanges(){
	setHeight();
    if (element.css('height') != lastHeight) {
        lastHeight = element.css('height'); 
        setHeight();
    }

    setTimeout(checkForChanges, 500);
}

var element2 = $("#main");
var lastHeight2 = $("#main").css('height');

function checkForChanges2(){
	setHeight2();
    if (element2.css('height') != lastHeight2) {
        lastHeight2 = element.css('height'); 
        setHeight2();
    }

    setTimeout(checkForChanges2, 500);
}

function setSelfAccount() {
	$('#setSelfAccountArea').show();
}