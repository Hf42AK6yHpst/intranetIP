// JavaScript Document
/* Filter function start */
$(function() {
    $('#button .search').click(function(){
        $('div#filter').removeClass('in');
        $('#content').removeClass('filterOpen');
        $('#content').toggleClass('searchOpen');
    });

    $('#button .filter .filter').click(function(){
        $('div#search').removeClass('in');
        $('#content').removeClass('searchOpen');
        $('#content').toggleClass('filterOpen');
    });
});
/* Filter function end */

