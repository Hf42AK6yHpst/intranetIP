<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$lay = new academic_year();
$filterSchoolYear = IntegerSafe($_GET["filterSchoolYear"]);
$filterCategory = IntegerSafe($_GET["filterCategory"]);
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "id='filterSchoolYear' class='selectpicker' name='filterSchoolYear' onChange='document.form1.submit()' color='black'", $filterSchoolYear, 1, 0, $i_Attendance_AllYear, 2);
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("id='filterCategory' class='selectpicker' name='filterCategory' onChange='document.form1.submit()' color='black'", $filterCategory, 1);

$cond = empty($filterSchoolYear) ? "" : " AND ay.AcademicYearID = '".$filterSchoolYear."'";
$cond .= empty($filterCategory) ? "" : " AND op.Category = '".$filterCategory."'";

$StudentID = $ck_current_children_id;

//handle for search title
$inputSearch = trim($inputSearch);
if(trim($inputSearch) !="")
{
    $cond .= " AND (op.Title LIKE '%".$inputSearch."%')";
}

if($status!=""&&$status!=0)
{
    if($status==2){
        $condSatus .= " AND (os.RecordStatus = '2' OR os.RecordStatus = '4')";
    }else{
        $condSatus .= " AND os.RecordStatus = '".$status."'";
    }
}

$sql_count = " SELECT os.RecordStatus, COUNT(*)         
     
              FROM
                {$eclass_db}.OLE_STUDENT AS os
              INNER JOIN
                {$eclass_db}.OLE_PROGRAM AS op
              ON
                os.ProgramID = op.ProgramID
              LEFT JOIN
                {$intranet_db}.INTRANET_USER AS iu
              ON
                op.CreatorID = iu.UserID
              LEFT JOIN
                {$intranet_db}.ACADEMIC_YEAR AS ay
              ON
                op.AcademicYearID = ay.AcademicYearID
              LEFT JOIN
                {$eclass_db}.OLE_CATEGORY AS oc
              ON
                op.Category = oc.RecordID
              WHERE
                op.IntExt = 'INT'
                AND os.UserID = '{$StudentID}'
                $cond                    
            GROUP BY os.RecordStatus;";
$count_result= $libdb->returnArray($sql_count);
$pendingcount=0;
$approvedcount=0;
$rejectedcount=0;
for($i=0;$i<count($count_result);$i++){
    if($count_result[$i][RecordStatus]==1) $pendingcount = $count_result[$i][1];
    if($count_result[$i][RecordStatus]==2) $approvedcount += $count_result[$i][1];
    if($count_result[$i][RecordStatus]==3) $rejectedcount = $count_result[$i][1];
    if($count_result[$i][RecordStatus]==4) $approvedcount += $count_result[$i][1];
}

$sql =  "
          SELECT
          	os.RecordID,
            ay.AcademicYearID,
            op.Title,
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period,
            ".Get_Lang_Selection("oc.ChiTitle","oc.EngTitle")." as category,
            os.RecordStatus,
            DATE_FORMAT(os.ProcessDate,'%Y-%m-%d') as ProcessDate
          FROM
            {$eclass_db}.OLE_STUDENT AS os
          INNER JOIN
            {$eclass_db}.OLE_PROGRAM AS op
          ON
            os.ProgramID = op.ProgramID
          LEFT JOIN
            {$intranet_db}.INTRANET_USER AS iu
          ON
            op.CreatorID = iu.UserID
          LEFT JOIN
            {$intranet_db}.ACADEMIC_YEAR AS ay
          ON
            op.AcademicYearID = ay.AcademicYearID
          LEFT JOIN
            {$eclass_db}.OLE_CATEGORY AS oc
          ON
            op.Category = oc.RecordID
          WHERE
            op.IntExt = 'INT'
            AND os.UserID = '{$StudentID}'
            $cond
            $condSatus
         Order By op.StartDate desc
        ";
$result = $libdb->returnArray($sql);

for($i=0;$i<count($result);$i++){
    if($result[$i][RecordStatus]==2){
        $programme_list .= "<a href='parent_ole_programme_detail.php?recordID={$result[$i][RecordID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}'>
                              <li class='event'>
                                <div class='eventTitle'>{$result[$i][Title]}</div>
                                <div class='eventDate'><span class='label'>{$ec_iPortfolio['date']}︰</span>{$result[$i][period]}</div>
                                <div class='eventCategory'><span class='label'>{$ec_iPortfolio['category']}︰</span>{$result[$i][category]}</div>
                                <div class='status approved'><span class='icon'></span>{$ec_iPortfolio['approved']}</div>
                              </li></a>";
    }else if($result[$i][RecordStatus]==1){
        $programme_list .= "<a href='parent_ole_programme_detail.php?recordID={$result[$i][RecordID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}'>
                                <li class='event'>
                                  <div class='eventTitle'>{$result[$i][Title]}</div>
                                  <div class='eventDate'><span class='label'>{$ec_iPortfolio['date']}︰</span>{$result[$i][period]}</div>
                                  <div class='eventCategory'><span class='label'>{$ec_iPortfolio['category']}︰</span>{$result[$i][category]}</div>
                                  <div class='status pending'><span class='icon'></span>{$ec_iPortfolio['pending']}</div>
                                </li></a>";
    }else if($result[$i][RecordStatus]==3){
        $programme_list .= "<a href='parent_ole_programme_detail.php?recordID={$result[$i][RecordID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}'>
                              <li class='event'>
                                <div class='eventTitle'>{$result[$i][Title]}</div>
                                <div class='eventDate'><span class='label'>{$ec_iPortfolio['date']}︰</span>{$result[$i][period]}</div>
                                <div class='eventCategory'><span class='label'>{$ec_iPortfolio['category']}︰</span>{$result[$i][category]}</div>
                                <div class='rejectedDate'><span class='label'>{$Lang['eDiscipline']['RejectedDate']}︰</span>{$result[$i][ProcessDate]}</div>
                                <div class='status rejected'><span class='icon'></span>{$ec_iPortfolio['rejected']}</div>
                              </li></a>";
    }else if($result[$i][RecordStatus]==4){
        $programme_list .= "<a href='parent_ole_programme_detail.php?recordID={$result[$i][RecordID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}'>
                              <li class='event'>
                                <div class='eventTitle'>{$result[$i][Title]}</div>
                                <div class='eventDate'><span class='label'>{$ec_iPortfolio['date']}︰</span>{$result[$i][period]}</div>
                                <div class='eventCategory'><span class='label'>{$ec_iPortfolio['category']}︰</span>{$result[$i][category]}</div>
                                <div class='status approved'><span class='icon'></span>{$ec_iPortfolio['teacher_submit_record']}</div>
                              </li></a>";
    }

}

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_parent.css">
    <script type="text/javascript" src="../../js/ole_parent.js"></script>
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>
<script>

    function change_status(status){
        $('#status').val(status);
        document.form1.submit();
    }

</script>

<body>
<form name="form1" id="form1" method="GET">
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top ole edit">
        <div id="function"></div>
        <div id="headerTitle" class="filterStatus">
            <select id="filterStatus" class="selectpicker" onchange="change_status(this.value)">
                <option value="0" <?php if($status==0||$status=="") echo "selected"?>><?=$ec_iPortfolio['all_status']?></option>
                <option value="1" <?php if($status==1) echo "selected"?>><?=$ec_iPortfolio['pending']?>  (<?=$pendingcount?>)</option>
                <option value="2" <?php if($status==2) echo "selected"?>><?=$ec_iPortfolio['approved']?> (<?=$approvedcount?>)</option>
                <option value="3" <?php if($status==3) echo "selected"?>><?=$ec_iPortfolio['rejected']?> (<?=$rejectedcount?>)</option>
            </select>
        </div>
        <div id="button">
            <div class="headerIcon"><a href="parent_ole_chart.php"><span class="oleGraph" id="oleGraph"></span></a></div>
            <div class="headerIcon"><a class="search" role="button" data-toggle="collapse" href="#search" aria-expanded="false" aria-controls="search"><i class="fas fa-search"></i></a></div>
            <div class="headerIcon"><a class="filter" role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter"> <span class="filter"></span> </a></div>
        </div>
        <div id="search" class="collapse">
            <input type="text" class="form-control" id="inputSearch" name="inputSearch" placeholder="" value="<?php echo stripslashes($inputSearch);?>">
        </div>
        <div class="collapse" id="filter">
            <div class="schoolYear">
                <?php echo $ay_selection_html; ?>
            </div>
            <div class="category">
                <?php echo $category_selection_html; ?>
            </div>
        </div>
    </nav>
    <div id="content">
        <ul class="eventList">
           <?=$programme_list?>
        </ul>
    </div>
    <input type="hidden" value = "<?=$status?>" name="status" id="status">
</div>
</form>
</body>
</html>
