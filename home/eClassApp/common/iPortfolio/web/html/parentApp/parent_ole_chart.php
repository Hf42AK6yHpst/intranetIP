<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();
$libdb = new libdb();
$LibPortfolio = new libpf_slp();

$ELEArray = $LibPortfolio->GET_ELE();
$ELECount = count($ELEArray);
$status = "2,4";
$StudentID = ($ck_memberType == "P") ? $ck_current_children_id : $UserID;

$sql = "SELECT AcademicYearID
        FROM  {$intranet_db}.YEAR_CLASS AS yc
        INNER JOIN  {$intranet_db}.YEAR_CLASS_USER as ycu ON (yc.YearClassID = ycu.YearClassID)
        WHERE  ycu.UserID =  '{$StudentID}'";
$studentAcademicYearIDAry = $LibPortfolio->returnVector($sql);
$studentAcademicYearIDList = implode('\',\'',$studentAcademicYearIDAry);


function stringToColorCode($str) {
    $code = dechex(crc32($str));
    $code = substr($code, 0, 6);
    return $code;
}

$count = 0;
foreach($ELEArray as $ELECode => $ELETitle)
{
    $sql="SELECT
                 COUNT(*)
          FROM
            {$eclass_db}.OLE_PROGRAM AS op      
          INNER JOIN
            {$eclass_db}.OLE_STUDENT AS os
          ON
            os.ProgramID = op.ProgramID AND
            os.RecordStatus IN (2,4) AND
            os.UserID = '{$StudentID}'
            AND op.AcademicYearID IN ('{$studentAcademicYearIDList}')
          WHERE
            op.IntExt = 'INT'
            AND op.ELE LIKE '%{$ELECode}%'";

    $result = $LibPortfolio->returnVector($sql);
    if(!$result[0]>0)$result[0]=0;

    $ELECountSum[] = $result[0];
    $ELETitleSum[] = "'".$ELETitle."'";

    $showRecordTable .="
                    <div class='category'>
                        <div class='label'><span class='color{$count}'><i class='fas fa-square-full'></i></span></div>
                        <div class='text'>$ELETitle</div>
                        <div class='hours'>$result[0]</div>
                    </div>
    ";
    $count++;
}
$ELECountString = implode(",",$ELECountSum);
$ELETitleString = implode(",",$ELETitleSum);

$sql="SELECT
                 COUNT(*)
          FROM
            {$eclass_db}.OLE_PROGRAM AS op      
          INNER JOIN
            {$eclass_db}.OLE_STUDENT AS os
          ON
            os.ProgramID = op.ProgramID AND
            os.RecordStatus IN (2,4) AND
            os.UserID = '{$StudentID}'
            AND op.AcademicYearID IN ('{$studentAcademicYearIDList}')
          WHERE
            op.IntExt = 'INT'";
$totalRecordCount = $LibPortfolio->returnVector($sql);
if(!$totalRecordCount[0]>0)$totalRecordCount[0] = 0;


//Hour

$count = 0;
foreach($ELEArray as $ELECode => $ELETitle)
{
    $sql="SELECT
                cast( 
                    sum(Hours) AS DECIMAL (9,1) 
                ) 
          FROM
            {$eclass_db}.OLE_PROGRAM AS op      
          INNER JOIN
            {$eclass_db}.OLE_STUDENT AS os
          ON
            os.ProgramID = op.ProgramID AND
            os.RecordStatus IN (2,4) AND
            os.UserID = '{$StudentID}'
            AND op.AcademicYearID IN ('{$studentAcademicYearIDList}')
          WHERE
            op.IntExt = 'INT'
            AND op.ELE LIKE '%{$ELECode}%'";

    $result = $LibPortfolio->returnVector($sql);
    if(!$result[0]>0)$result[0]=0;
    $ELETitleHourSum[] = "['".$ELETitle."',".$result[0]."]";
    $showHourTable .="
                    <div class='category'>
                        <div class='label'><span class='color{$count}'><i class='fas fa-square-full'></i></span></div>
                        <div class='text'>$ELETitle</div>
                        <div class='hours'>$result[0]</div>
                    </div>                   
    ";
    $count++;
}
$ELETitleHourString = implode(",",$ELETitleHourSum);

$sql="SELECT
                 cast( 
                    sum(Hours) AS DECIMAL (9,1) 
                ) 
          FROM
            {$eclass_db}.OLE_PROGRAM AS op      
          INNER JOIN
            {$eclass_db}.OLE_STUDENT AS os
          ON
            os.ProgramID = op.ProgramID AND
            os.RecordStatus IN (2,4) AND
            os.UserID = '{$StudentID}'
            AND op.AcademicYearID IN('{$studentAcademicYearIDList}')
          WHERE
            op.IntExt = 'INT'";
$totalHourCount = $LibPortfolio->returnVector($sql);
if(!$totalHourCount[0]>0)$totalHourCount[0] = 0;


?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_parent.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="http://www.chartjs.org/dist/2.7.2/Chart.bundle.js"></script>
    <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <script>
        var color = Chart.helpers.color;
        var config = {
            data: {
                datasets: [{
                    data: [
                        <?= $ELECountString ?>
                    ],
                    backgroundColor: [
                        color('#f28b8b').alpha(0.85).rgbString(),
                        color('#f49769').alpha(0.85).rgbString(),
                        color('#f9c74b').alpha(0.85).rgbString(),
                        color('#e4ea78').alpha(0.85).rgbString(),
                        color('#b6e09b').alpha(0.85).rgbString(),
                        color('#88d66e').alpha(0.85).rgbString(),
                        color('#69b577').alpha(0.85).rgbString(),
                        color('#75d3ba').alpha(0.85).rgbString(),
                        color('#93f2f9').alpha(0.85).rgbString(),
                        color('#69d2f7').alpha(0.85).rgbString(),
                        color('#3d9be8').alpha(0.85).rgbString(),
                        color('#6575ea').alpha(0.85).rgbString(),
                        color('#8382db').alpha(0.85).rgbString(),
                        color('#b980dd').alpha(0.85).rgbString(),
                        color('#efa3c8').alpha(0.85).rgbString(),
                    ],
                    label: 'My dataset', // for legend
                    borderWidth: 0
                }],
                labels: [
                    <?=$ELETitleString?>,
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                title: {
                    display: true,
                    text: ''
                },
                scale: {
                    ticks: {
                        beginAtZero: true,
                        backdropColor: '#f1f1f1',
                    },
                    reverse: false
                },
                animation: {
                    animateRotate: false,
                    animateScale: true
                },
                layout: {
                    padding: {
                        left: -10,
                        right: 0,
                        top: -20,
                        bottom: 0
                    }
                },
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('chart-area');
            window.myPolarArea = Chart.PolarArea(ctx, config);
        };
    </script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['類別', '時數'],
                <?=$ELETitleHourString?>
            ]);
            var options = {
                backgroundColor: 'transparent',
                colors: ['#f28b8b', '#f49769', '#f9c74b', '#e4ea78', '#b6e09b', '#88d66e' , '#69b577', '#75d3ba', '#93f2f9', '#69d2f7', '#3d9be8', '#6575ea', '#8382db', '#b980dd', '#efa3c8'],
                height: 176,
                legend: {position: "none"},
                pieHole: 0.6 ,
                pieSliceBorderColor: "none",
                pieSliceText: "none",
                tooltip: {trigger: "none"},
                width: 161,
                chartArea:{
                    left:7,
                    top: 15,
                    right: 7,
                    bottom: 15,
                }
            };

            var chart = new google.visualization.PieChart(document.getElementById('totalHourChart'));
            chart.draw(data, options);
        }
    </script>
</head>

<body>
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top ole noTitle">
        <div id="function"></div>
        <div class="headerTitle"></div>
        <div id="button">
            <div class="headerIcon"><a onclick="window.history.go(-1)"><span class="close"></span></a></div>
        </div>
    </nav>
    <div id="chartTab">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#record" aria-controls="record" role="tab" data-toggle="tab"><?=$Lang['eClassApp']['iPortfolio']['TotalRecords']?></a></li>
            <li role="presentation"><a href="#totalHr" aria-controls="totalHr" role="tab" data-toggle="tab"><?=$Lang['eClassApp']['iPortfolio']['ActivityTotalHours']?></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="record">
                <div class="chart col-xs-12">
                    <div class="row">
                        <div class="chart col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-3">
                            <div id="totalRecordChart">
                                <canvas id="chart-area"></canvas>
                            </div>
                        </div>
                        <div class="chartInfo col-xs-6 col-sm-4 col-md-3">
                            <div class="totalHour"><?=$totalRecordCount[0]?><span class="small"><span class="small"> <?=$Lang['eClassApp']['iPortfolio']['Items']?></span></div>
                            <div class="chartLabel"><?=$Lang['eClassApp']['iPortfolio']['TotalRecordsApproved']?></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="statistics">
                            <?=$showRecordTable?>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="totalHr">
                <div class="chart col-xs-12" data-page="2">
                    <div class="row">
                        <div class="chart col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-3">
                            <div id="totalHourChart"></div>
                        </div>
                        <div class="chartInfo col-xs-6 col-sm-4 col-md-3">
                            <div class="totalHour"><?=$totalHourCount[0]?><span class="small">&nbsp;<?=$Lang['SysMgr']['Homework']['Hours']?></span></div>
                            <div class="chartLabel"><?=$Lang['eClassApp']['iPortfolio']['ActivityTotalHours']?></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="statistics">
                            <?=$showHourTable?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
