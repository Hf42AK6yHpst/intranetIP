<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

$recordID = IntegerSafe($_GET["recordID"]);
$programID = IntegerSafe($_GET["programID"]);
$filterSchoolYear = IntegerSafe($_GET["filterSchoolYear"]);
$filterCategory = IntegerSafe($_GET["filterCategory"]);

intranet_auth();
intranet_opendb();
$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$LibUserStudent = new libuser($UserID);
$approvalSettings = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2("INT");
$isAllowEditOLE = checkAllowEditOLE($recordID,$approvalSettings);

if($recordID!=""){
    $data = $LibPortfolio->RETURN_OLE_RECORD_BY_RECORDID($recordID);
    list($startdate, $enddate, $title, $category, $ele, $role, $hours, $organization, $achievement, $details, $ole_file, $approved_by, $remark, $process_date, $status, $user_id, $p_intext, $programid, $subcat_id, $maxhours, $compulfields, $request_approve_by, $IsSAS, $IsOutsideSchool) = $data;
}else if($programID!=""){
    $data = $LibPortfolio->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($programID);
    $startdate = $data['StartDate'];
    $enddate = $data['EndDate'];
    $title = $data['Title'];
    $chiTitle = $data['TitleChi'];
    $category = $data['Category'];
    $subCategoryID = $data['SubCategoryID'];
    $ele = $data['ELE'];
    $organization = $data['Organization'];
    $details = $data['Details'];
    $chiDetails = $data['DetailsChi'];
    $remark = $data['SchoolRemarks'];
    $input_date = $data['InputDate'];
    $modified_date = $data['ModifiedDate'];
    $period = $data['Period'];
    $int_ext = $data['IntExt'];
    $canJoin = $data['CanJoin'];
    $canJoinStartDate = $data['CanJoinStartDate'];
    $canJoinEndDate = $data['CanJoinEndDate'];
    $userName = $data['UserName'];
    $autoApprove = $data['AUTOAPPROVE'];
    $maximumHours = $data['MaximumHours'];
    $defaultHours = $data['DefaultHours'];
    $compulsoryFields = $data['CompulsoryFields'];
    $request_approve_by = $data['DefaultApprover'];
    $IsSAS = $data['IsSAS'];
    $IsOutsideSchool = $data['IsOutsideSchool'];
    $hours=$defaultHours;
}

# get ELE Array
if($ele!="")
{
    # get the ELE code array
    $DefaultELEArray = $LibPortfolio->GET_ELE();
    $ELEArr = explode(",", $ele);
    for($i=0; $i<sizeof($ELEArr); $i++)
    {
        $t_ele = trim($ELEArr[$i]);
        $ele_display .= "- ".$DefaultELEArray[$t_ele]."<br />";
    }
}
else
    $ele_display = "--";

$statusArr[1] = $ec_iPortfolio['pending'];
$statusArr[2] = $ec_iPortfolio['approved'];
$statusArr[3] = $ec_iPortfolio['rejected'];
# get status

$sql_status= " SELECT os.RecordStatus
              FROM
                {$eclass_db}.OLE_STUDENT AS os
            
              WHERE
                os.RecordID = '{$recordID}'";
$status_result = $libdb->returnArray($sql_status);
if($status_result[0][RecordStatus]==1){
    $showStatus = "<div class='status pending'><span class='icon'></span>".$ec_iPortfolio['pending']."</div>";
}
if($status_result[0][RecordStatus]==2){
    $showStatus = "<div class='status approved'><span class='icon'></span>".$ec_iPortfolio['approved']."</div>";
}
if($status_result[0][RecordStatus]==3){
    $showStatus = "<div class='status rejected'><span class='icon'></span>".$ec_iPortfolio['rejected']."</div>";
}
if($status_result[0][RecordStatus]==4){
    $showStatus = "<div class='status approved'><span class='icon'></span>".$ec_iPortfolio['teacher_submit_record']."</div>";
}

# get category display
$CategoryArray = $LibPortfolio->GET_OLR_CATEGORY();
$category_display = $CategoryArray[trim($category)];

if($startdate=="" || $startdate=="0000-00-00")
    $date_display = "--";
else if($enddate!="" && $enddate != "0000-00-00")
    $date_display = $startdate."&nbsp;".$profiles_to."&nbsp;".$enddate;
else
    $date_display = $startdate;

$attach_count = 0;

$tmp_arr = explode(":", $ole_file);
$folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$recordID;
$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$recordID;
for ($i=0; $i<sizeof($tmp_arr); $i++)
{
    $attach_file = $tmp_arr[$i];
    if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
    {
        # Eric Yip (20090522): Handle chinese-character file name
        $attach_file_url = urlencode($attach_file);
        $attach_file_url = str_replace(array("%2F", "%26", "+"), array("/", "&", " "), $attach_file_url);

        $file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;
        $attachments_html .= "<a href=\"".$folder_url."/".$attach_file_url."\" target='_blank' id='a_$attach_count' class='tablelink attachName' >".get_file_basename($attach_file)." ($file_size)</a>";
        $attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
        $attach_count ++;
    }
}

if ($intranet_hardcode_lang == "en")
{
    $StudentInfo = $LibUserStudent->EnglishName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")";
}
else
{
    $StudentInfo = $LibUserStudent->ChineseName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")";
}
function checkAllowEditOLE($record_id,$approvalSettings) {
    global $eclass_db,$ipf_cfg,$LibPortfolio;
    ### References diagram (logic derived from old function)
    ### /addon/script/max/doc/References/checkAllowEditOLE.jpeg
    ### Refined Logic
    ### /addon/script/max/doc/References/checkAllowEditOLE2.jpeg
    ############################################################
    if (!empty($record_id)) {
        $sql = "SELECT RecordStatus FROM {$eclass_db}.OLE_STUDENT WHERE RecordID = '$record_id'";
        $status = current($LibPortfolio->returnVector($sql));
    }

    if (
        empty($record_id)
        || (isset($status) && $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"] || $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"])
        || (isset($status) && $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]) && (!$approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Editable'])
    ) {
        $isAllow = 1;
    } else {
        $isAllow = 0;
    }
    return $isAllow;
}
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_parent.css">
    <script type="text/javascript" src="../../js/ole_parent.js"></script>
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>
<body>
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top ole">
        <div id="function"><div class="headerIcon"><a href="<?="parent_ole_programme.php?inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}"?>"><i class="fas fa-arrow-left"></i></a></div></div>
        <div id="headerTitle">
        </div>
        <div id="button">
        </div>
    </nav>
    <div id="content" class="infoPage">
        <span class="eventTitle"><?=$title?></span>
        <?=$showStatus?>
        <ul class="eventInfo">
            <li>
                <span class="label"><?=$ec_iPortfolio['date']?></span>
                <span class="text"><?=$date_display?></span>
            </li>
            <li>
                <span class="label"><?php echo $ec_iPortfolio['category']; ?></span>
                <span class="text"><?=$category_display?></span>
            </li>
            <li>
                <span class="label"><?php echo $ec_iPortfolio['ele']; ?></span>
                <span class="text"><?=$ele_display?></span>
            </li>
            <li>
                <span class="label"><?=$ec_iPortfolio['organization']?></span>
                <span class="text"><?=($organization==""?"--":$organization)?></span>
            </li>
            <li>
                <span class="label"><?=$ec_iPortfolio['hours']?></span>
                <span class="text"><?=($hours==""?"--":$hours)?></span>
            </li>
            <li>
                <span class="label"><?=$ec_iPortfolio['ole_role']?></span>
                <span class="text"><?=($role==""?"--":$role)?></span>
            </li>
            <li>
                <span class="label"><?=$ec_iPortfolio['achievement']?></span>
                <span class="text"><?=($achievement==""?"--":$achievement)?></span>
            </li>
            <li class="attachment">
                <span class="label"><?=$ec_iPortfolio['attachment']?></span>
                <span class="text"><span class="attachIcon"></span> <?=($attachments_html==""?"--":$attachments_html)?></span>
            </li>
            <?php
            if($approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Self'])
            {
            ?>
                <li>
                    <span class="label"><?=$Lang['iPortfolio']['preferred_approver']?></span>
                    <span class="text"><?=(($request_approve_by=="" || $request_approve_by==0)?"--":$LibPortfolio->GET_TEACHER_NAME($request_approve_by))?></span>
                </li>
             <?php
            }
            ?>
        </ul>
    </div>
</div>
</body>
</html>