<?php

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$studentsString = IntegerSafe($_POST['studentsString']);
$programID = IntegerSafe($_POST['programID']);

$fromPage = trim($fromPage);
$fromAction = trim($fromAction);

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");

$fs = new libfilesystem();

// $UserID is a sesssion variable
$setApprovalPerson = $UserID;
$studentRecordComeFrom = $ipf_cfg["OLE_STUDENT_COMEFROM"]["teacherInput"];
$ApprovalPerson = ($Status != 1) ? $setApprovalPerson: "";

$data = $LibPortfolio->RETURN_OLE_PROGRAM_BY_PROGRAMID($programID);
list($startdate, $enddate, $title, $category, $ele, $organization, $details, $remark, $input_date, $modified_date, $period) = $data;
$SortStudentID = explode( ',', $studentsString );

for ($count=0;$count<count($SortStudentID);$count++)
{
    $TmpStudentID = $SortStudentID[$count];

    $TmpRecordID = $LibPortfolio->RETURN_RECORDID_BY_EVENT_STUDENT($programID,$TmpStudentID);

    if($TmpRecordID=="") {


        $fields = "(UserID, Title, Category, ELE, Role, Hours, Achievement, Organization";
        $fields .= ", ApprovedBy";
        $fields .= ", Details";
        $fields .= ", RecordStatus, StartDate, EndDate, ProcessDate, InputDate,InputBy, ModifiedDate, TeacherComment, ProgramID, IntExt, ComeFrom)";

        $values = "('" . $TmpStudentID . "', '" . addslashes($title) . "', '" . $category . "', '" . $ele . "', '" . $Role . "', '" . $Hours . "', '" . $Achievement . "', '" . addslashes($organization) . "'";
        $values .= ", '" . $ApprovalPerson . "'";
        $RecorStatus = ($ApprovalSetting == 2) ? 2 : 1;
        $values .= ", '".addslashes($TmpDetails)."'";
        $values .= ", '{$Status}', '" . $startdate . "', '" . $enddate . "', now(), now(),'" . $_SESSION['UserID'] . "', now(), '" . $TeacherComment . "', '" . $programID . "', '" . "INT" . "', " . $studentRecordComeFrom . ")";

        $sql = "INSERT INTO {$eclass_db}.OLE_STUDENT $fields VALUES $values";
        $LibPortfolio->db_db_query($sql);
        $TmpRecordID = $LibPortfolio->db_insert_id();


    }else{

        $fields_values = "Title = '".addslashes($title)."', ";
        $fields_values .= "Category = '$category', ";
        $fields_values .= "ELE = '$ele', ";
        $fields_values .= "Role = '$Role', ";
        $fields_values .= "Hours = '$Hours', ";
        $fields_values .= "Achievement = '$Achievement', ";
        $fields_values .= "Organization = '".addslashes($organization)."', ";
        $fields_values .= "TeacherComment = '$TeacherComment', ";
        // Before record status update
        $fields_values .= "ApprovedBy = IF(IFNULL(ApprovedBy, 0) = 0 OR RecordStatus <> '{$Status}', '{$setApprovalPerson}', ApprovedBy), ";
        $fields_values .= "RecordStatus = '$Status', ";
        $fields_values .= "ProcessDate = now(), ";
        $fields_values .= "ModifiedDate = now()";
        $sql = "UPDATE {$eclass_db}.OLE_STUDENT SET $fields_values WHERE RecordID = '$TmpRecordID' AND UserID='$TmpStudentID' ";
        $LibPortfolio->db_db_query($sql);
    }


    ///////////////// START FILE ATTACHMENT ////////////////////

        $fileArr = "";

        $attachments = "";
    // use previous session_id if exists
        if ($attachment_size_current>0)
        {
            $tmp_arr = explode("/", ${"file_current_0"});
            $SessionID = trim($tmp_arr[0]);
        }
        if ($SessionID=="")
        {
            $SessionID = session_id();
        }
        $folder_prefix = $eclass_filepath."/files/portfolio/ole/r".$TmpRecordID."/".$SessionID;
    //echo "-->$folder_prefix";
    # remove unwanted files

        for ($i=0; $i<$attachment_size_current; $i++)
        {
            $attach_file = ${"file_current_".$i};

            if (${"is_delete".$i})
            {
                // remove the corresponding file
                $fs->file_remove($folder_prefix."/".stripslashes(basename($attach_file)));
            } else
            {
                $attachments .= (($attachments=="")?"":":") . $attach_file;
                $fileArr[] = $attach_file;
            }
        }
    # add new files
        if ($attachment_size>0) {
            $fs->folder_new($eclass_filepath . "/files/portfolio/ole");
            $fs->folder_new($eclass_filepath . "/files/portfolio/ole/r" . $TmpRecordID);
            $fs->folder_new($folder_prefix);

            # copy the files
            for ($i = 1; $i <= $attachment_size; $i++) {
                $loc = ${"ole_file" . ($i)};
                $name_hidden = ${"ole_file" . $i . "_hidden"};

                $filename = (trim($name_hidden) != "") ? $name_hidden : ${"ole_file" . ($i) . "_name"};

                if ($loc != "none" && file_exists($loc)) {
                    if (strpos($filename, ".") == 0) {
                        // Fail
                        $isOk = false;
                    } else {
                        // Success
                        $fs->item_copy($loc, stripslashes($folder_prefix . "/" . $filename));

                        $tmp_attach = $SessionID . "/" . $filename;
                        $exist_flag = 0;
                        for ($j = 0; $j < sizeof($fileArr); $j++) {
                            if ($tmp_attach == $fileArr[$j]) {
                                $exist_flag = 1;
                                break;
                            }
                        }
                        if ($exist_flag != 1) {
                            $attachments .= (($attachments == "") ? "" : ":") . $tmp_attach;
                            $fileArr[] = $tmp_attach;
                        }
                    }
                }
            }
        }
        $award_file_sql = (trim($attachments)=="") ? "NULL" : "'$attachments'";
        $sql = "	UPDATE
                        {$eclass_db}.OLE_STUDENT
                    SET
                        Attachment=$award_file_sql
                    WHERE
                        RecordID = '$TmpRecordID'
                ";
        $LibPortfolio->db_db_query($sql);

} // end for

intranet_closedb();


header("Location: teacher_ole_view_programme.php");
?>