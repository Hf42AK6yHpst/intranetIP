<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$programID = IntegerSafe($_GET['programID']);
$YearID = IntegerSafe($_GET['YearID']);
$YearClassID = IntegerSafe($_GET['YearClassID']);

$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$lay = new academic_year();
$lpf_mgmt_grp = new libpf_mgmt_group();

$currentAcademicYear = Get_Current_Academic_Year_ID();

//year dropdown list

$lpf_fc = new libpf_formclass();
$AcademicYearID = Get_Current_Academic_Year_ID();
$libdb = new libdb();

$year_arr = $lpf_fc->GET_CLASSLEVEL_LIST();

$html_year_selection = "<select name='YearID' id='YearID' class='selectpicker' onChange='cleanClass();reload();'>";
$html_year_selection .= "<option value='' selected>{$Lang['eClassApp']['iPortfolio']['AllForms']}</option>";

for($i=0, $i_max=count($year_arr); $i<$i_max; $i++)
{
    $_yID = $year_arr[$i]["YearID"];
    $_yName = $year_arr[$i]["YearName"];
    $showselected = ($YearID==$_yID)?'selected':'';
    $html_year_selection .= "<option value='{$_yID}' $showselected>{$_yName}</option>";
}

$html_year_selection .= "</select>";

//yearclass dropdown list

$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." AS ClassTitle 
        FROM {$intranet_db}.YEAR y
        INNER JOIN {$intranet_db}.YEAR_CLASS yc ON y.YearID = yc.YearID 
        WHERE yc.YearID = '{$YearID}' 
        AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
        ORDER BY y.Sequence, yc.Sequence";

$yc_arr = $libdb->returnArray($sql);

$html_yearclass_selection = "<select name='YearClassID' id='YearClassID' class='selectpicker' onChange='reload()'>";
$html_yearclass_selection .= "<option value=''>{$Lang['SysMgr']['FormClassMapping']['AllClass']}</option>";

for($i=0, $i_max=count($yc_arr); $i<$i_max; $i++)
{
    $_ycID = $yc_arr[$i]["YearClassID"];
    $_ycName = $yc_arr[$i]["ClassTitle"];
    $showselected = ($YearClassID==$_ycID)?'selected':'';

    $html_yearclass_selection .= "<option value='{$_ycID}' $showselected>{$_ycName}</option>";
}
$html_yearclass_selection .= "</select>";

$cond = empty($YearID) ? "" : " AND y.YearID = '".$YearID."'";
$cond .= empty($YearClassID) ? "" : " AND ycu.YearClassID = '".$YearClassID."'";

if($status!=""&&$status!=0)
{
    $cond .= " AND a.RecordStatus = '".$status."'";
}

$programme_sql = " SELECT          
            op.Title,
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period
          FROM
            {$eclass_db}.OLE_PROGRAM AS op
          WHERE
            op.ProgramID = '".$programID."'
        ";

$programme_result = $libdb->returnArray($programme_sql);

//Student List

$sql_count = " SELECT a.RecordStatus, COUNT(*) 
             FROM
              {$eclass_db}.OLE_STUDENT as a
            INNER JOIN {$eclass_db}.OLE_PROGRAM as c
              ON a.ProgramID = c.ProgramID
            INNER JOIN {$intranet_db}.INTRANET_USER as iu
              ON a.UserID = iu.UserID
            INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
          	  ON iu.UserID = ycu.UserID
            INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
			  ON ycu.YearClassID = yc.YearClassID
			INNER JOIN {$intranet_db}.YEAR y 
              ON yc.YearID = y.YearID 
            WHERE             
              yc.AcademicYearID = '{$currentAcademicYear}'
              And a.ProgramID = '{$programID}'
            GROUP BY a.RecordStatus;";
$count_result= $libdb->returnArray($sql_count);
$pendingcount=0;
$approvedcount=0;
$rejectedcount=0;
for($i=0;$i<count($count_result);$i++){
    if($count_result[$i][RecordStatus]==1) $pendingcount = $count_result[$i][1];
    if($count_result[$i][RecordStatus]==2) $approvedcount = $count_result[$i][1];
    if($count_result[$i][RecordStatus]==3) $rejectedcount = $count_result[$i][1];
}


$statusSql = "  CASE a.RecordStatus
                WHEN 1 THEN '<div class=\"status pending\"><span class=\"icon\"></span> {$ec_iPortfolio['pending']}</div>'
                WHEN 2 THEN '<div class=\"status approved\"><span class=\"icon\"></span> {$ec_iPortfolio['approved']}</div>'
                WHEN 3 THEN '<div class=\"status rejected\"><span class=\"icon\"></span> {$ec_iPortfolio['rejected']}</div>'
                ELSE '--' END
              as RecordStatus";

$sql =  "
            SELECT
              iu.UserID,
              ".getNameFieldByLang2("iu.")." AS DisplayName,
		      CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', LPAD(iu.ClassNumber, 2, '0')) AS ClassNumber,
		      a.Role,
		      a.Hours,
		      a.RecordStatus,
		      a.RecordID
            FROM
              {$eclass_db}.OLE_STUDENT as a
            INNER JOIN {$eclass_db}.OLE_PROGRAM as c
              ON a.ProgramID = c.ProgramID
            INNER JOIN {$intranet_db}.INTRANET_USER as iu
              ON a.UserID = iu.UserID
            INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
          	  ON iu.UserID = ycu.UserID
            INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
			  ON ycu.YearClassID = yc.YearClassID
			INNER JOIN {$intranet_db}.YEAR y 
              ON yc.YearID = y.YearID 
            WHERE             
              yc.AcademicYearID = '{$currentAcademicYear}'
              And a.ProgramID = '{$programID}'
              {$cond}
          ";
$result= $libdb->returnArray($sql);

for($i=0;$i<count($result);$i++){
    if($result[$i][RecordStatus]==2) {

        $showStatus = "<div class='status approved'><span class='icon'></span>{$ec_iPortfolio['approved']}</div>";

    }else if($result[$i][RecordStatus]==1){

        $showStatus = "<div class='status pending'><span class='icon'></span>{$ec_iPortfolio['pending']}</div>";

    }else if($result[$i][RecordStatus]==3){

        $showStatus = "<div class='status rejected'><span class='icon'></span>{$ec_iPortfolio['rejected']}</div>";

    }
    $student_list .= " <li class='student'>
                                <a href='student_record_detail.php?studentID={$result[$i][UserID]}&recordID={$result[$i][RecordID]}&programID={$programID}'>
                                <div class='stdName'>{$result[$i][DisplayName]}<span class='classClassNo'> {$result[$i][ClassNumber]}</span></div>
                                <div class='noOfHrs'><span class='label'>{$ec_iPortfolio['hours']}︰</span>{$result[$i][Hours]} {$Lang['SysMgr']['Homework']['Hours']}</div>
                                <div class='participate'><span class='label'>{$ec_iPortfolio['ole_role']}︰</span>{$result[$i][Role]}</div>
                                {$showStatus}
                                <div class='form-check form-check-inline'>
                                    <label class='form-check-label'>
                                        <input class='form-check-input' type='checkbox' name='record_id[]' value='{$result[$i][RecordID]}'>
                                        <span>&nbsp;</span>
                                    </label>
                                </div></a>
                         </li>";
}

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_teacher.css">
    <script type="text/javascript" src="../../js/ole_teacher.js"></script>
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>
<script>

    function change_status(status){
        $('#status').val(status);
        document.form1.submit();
    }

    function submit_status(submitStatus){
        $('#form1').attr({"action":"teacher_ole_programme_student_list_update.php"});
        $('#form1').attr({"method":"POST"});
        $('#submitStatus').val(submitStatus);
        document.form1.submit();
    }

    function reload(){
        document.form1.submit();
    }

    function cleanClass(){
        $("#YearClassID").val("");
    }

</script>
<body>
<form name="form1" id="form1" method="GET">
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top ole edit">
        <div id="function"><div class="headerIcon"><a onclick="window.history.go(-1)"><i class="fas fa-arrow-left"></i></a></div></div>
        <div id="headerTitle" class="filterStatus withFunction btn-2">
            <select id="filterStatus" class="selectpicker" onchange="change_status(this.value)">
                <option value="0" <?php if($status==0||$status=="") echo "selected"?>><?=$ec_iPortfolio['all_status']?></option>
                <option value="1" <?php if($status==1) echo "selected"?>><?=$ec_iPortfolio['pending']?>  (<?=$pendingcount?>)</option>
                <option value="2" <?php if($status==2) echo "selected"?>><?=$ec_iPortfolio['approved']?> (<?=$approvedcount?>)</option>
                <option value="3" <?php if($status==3) echo "selected"?>><?=$ec_iPortfolio['rejected']?> (<?=$rejectedcount?>)</option>
            </select>
        </div>
        <div id="button">
            <div class="headerIcon"><a class="filterStd" role="button" data-toggle="collapse" href="#filterStd" aria-expanded="false" aria-controls="filterStd"> <span class="filterStd"></span> </a></div>
            <div class="headerIcon"><a class="addStd" href="teacher_ole_add_students.php?programID=<?=$programID?>"> <span class="addStd"></span> </a></div>
        </div>
        <div class="collapse" id="filterStd">
            <div class="grade">
                <?=$html_year_selection?>
            </div>
            <div class="class">
                <?=$html_yearclass_selection?>
            </div>
        </div>
    </nav>
    <div id="content">
        <div class="eventBriefInfo">
            <div class="eventTitle"><?=$programme_result[0]['Title']?></div>
            <div class="eventDate"><span class='label'><?=$ec_iPortfolio['date']?>︰</span><?=$programme_result[0]['period']?></div>
            <a href="program_detail.php?programID=<?=$programID?>"><div class="eventDetails"><?=$Lang['eDiscipline']['App']['RecordDetails']?></div></a>
        </div>
        <ul class="eventStdList">
            <?php echo $student_list;?>
        </ul>
        <div class="functionBtn">
            <div class="btn approve" onclick="submit_status('2');">
                <span class="icon"></span>
            </div>
            <div class="btn reject" onclick="submit_status('3');">
                <span class="icon"></span>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value = "<?=$status?>" name="status" id="status">
<input type="hidden" value = "<?=$programID?>" name="programID" id="programID">
<input type="hidden" value = "<?=$submitStatus?>" name="submitStatus" id="submitStatus">
</form>
</body>
</html>