<?php
# modifing by

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");


intranet_auth();
intranet_opendb();

# Get parameters from previous page
$allowStudentsToJoin = $_POST['allowStudentsToJoin'];
$allowStudentsToJoin = (isset($allowStudentsToJoin)) ? $allowStudentsToJoin : 0 ;
//echo "This is the allow student to join -> $allowStudentsToJoin<br/>";die;
//HANDLE USER INPUT VARIABLE
if ($allowStudentsToJoin == 0) {
    $jp_periodStart = "null";
    $jp_periodEnd = "null";
    $joinableYear = "null";
    $autoApprove = 0;
} else {
    $autoApprove			= $_POST['autoApprove'];
//	$jp_periodStart			= $_POST['jp_periodStart'];
//	$jp_periodEnd			= $_POST['jp_periodEnd'];
    $joinableYear			= (sizeof($_POST['Year'])!=0) ? implode(",", $_POST['Year']) : "";
    $jp_periodStart			= ($_POST['jp_periodStart'] == "")? "null": $_POST['jp_periodStart'];
    $jp_periodEnd			= ($_POST['jp_periodEnd'] == "") ? "null": $_POST['jp_periodEnd'];
}
$programID = IntegerSafe($_POST['programID']);
$academicYearID = IntegerSafe($_POST['academicYearID']);
$yearTermID = IntegerSafe($_POST['YearTermID']);
$title = HTMLtoDB($title);
$category = HTMLtoDB($category);
$role = HTMLtoDB($role);
$SchoolRemarks = HTMLtoDB($SchoolRemarks);
$organization = HTMLtoDB($organization);
$details = HTMLtoDB($details);
$ELEList = (sizeof($ele)!=0) ? implode(",", $ele) : "";
$startdate = $startdate ? date($startdate) : date("Y-m-d");
$enddate = ($startdate=="" || $startdate=="0000-00-00") ? "" : $enddate; // set end date as empty if start date is empty
$enddate = ($enddate=="" || $enddate=="0000-00-00") ?"null":$enddate;
$maximumHours = $_POST['maximumHours'];
$defaultHours = $_POST['defaultHours'];
$compulsoryFields = is_array($_POST['compulsoryFields'])?implode("",$_POST['compulsoryFields']):"";
$MergeProgramIDs = unserialize(stripslashes($MergeProgramIDs));
$Action = $_POST['Action'];
//$comeFrom = $_POST['comeFrom'];
// Added: 2015-01-12
$SASPoint = ($SASCategory == 0)? '' : $SASPoint;
$SASPoint = (trim($SASPoint) == '')? '' : intval($SASPoint);

$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");


# Find the academic school year
$li = new libdb();

if($programID=="")
{
    $isEdit=false;

    $objOLEPROGRAM = new ole_program();
    $objOLEPROGRAM->setProgramType($ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);
    $objOLEPROGRAM->setTitle($englishTitle);
    $objOLEPROGRAM->setTitleChi($chineseTitle);
    $objOLEPROGRAM->setStartDate($startdate);
    $objOLEPROGRAM->setEndDate($enddate);
    $objOLEPROGRAM->setCategory($category);
    $objOLEPROGRAM->setSubCategoryID($subCategory);
    $objOLEPROGRAM->setOrganization($organization);
    $objOLEPROGRAM->setDetails($englishDetails);
    $objOLEPROGRAM->setDetailsChi($chineseDetails);
    $objOLEPROGRAM->setSchoolRemarks($SchoolRemarks);
    $objOLEPROGRAM->setELE($ELEList);
    $objOLEPROGRAM->setModifyBy($UserID);
    $objOLEPROGRAM->setCreatorID($UserID);
    $objOLEPROGRAM->setIntExt("INT");
    $objOLEPROGRAM->setAcademicYearID($academicYearID);
    $objOLEPROGRAM->setYearTermID($yearTermID);
    $objOLEPROGRAM->setCanJoin($allowStudentsToJoin);
    $objOLEPROGRAM->setCanJoinStartDate($jp_periodStart);
    $objOLEPROGRAM->setCanJoinEndDate($jp_periodEnd);
    $objOLEPROGRAM->setAutoApprove($autoApprove);
    $objOLEPROGRAM->setJoinableYear($joinableYear);
    $objOLEPROGRAM->setMaximumHours($maximumHours);
    $objOLEPROGRAM->setDefaultHours($defaultHours);
    $objOLEPROGRAM->setCompulsoryFields($compulsoryFields);
    $objOLEPROGRAM->setDefaultApprover($request_approved_by);
    $objOLEPROGRAM->setIsSAS($IsSAS);
    $objOLEPROGRAM->setIsOutsideSchool($InOutSideSchool);
    // Added: 2015-01-12
    if($sys_custom['NgWah_SAS']){
        $objOLEPROGRAM->setSASCategory($SASCategory);
        $objOLEPROGRAM->setSASPoint($SASPoint);
    }

//	if (strtoupper($Action)=="MERGE") {
//		$objOLEPROGRAM->setComeFrom($comeFrom);
//	} else {
    $objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherInput"]);
//	}


    $programID = $objOLEPROGRAM->SaveProgram();	// RecordID here is the ProgramID indeed

    if ($programID && is_array($MergeProgramIDs)) {
        $mergeResult = ole_program::MergePrograms($programID,$MergeProgramIDs);
    }
}
else
{
    $isEdit=true;

    $objOLEPROGRAM = new ole_program($programID);

    $objOLEPROGRAM->setTitle($englishTitle);
    $objOLEPROGRAM->setTitleChi($chineseTitle);
    $objOLEPROGRAM->setStartDate($startdate);
    $objOLEPROGRAM->setEndDate($enddate);
    $objOLEPROGRAM->setCategory($category);
    $objOLEPROGRAM->setSubCategoryID($subCategory);
    $objOLEPROGRAM->setOrganization($organization);
    $objOLEPROGRAM->setDetails($englishDetails);
    $objOLEPROGRAM->setDetailsChi($chineseDetails);
    $objOLEPROGRAM->setSchoolRemarks($SchoolRemarks);
    $objOLEPROGRAM->setELE($ELEList);
    $objOLEPROGRAM->setModifyBy($UserID);
//	$objOLEPROGRAM->setCreatorID($UserID);  //SINCE IT IS A UPDATE ACTION, SUPPOSE CANNOT UPDATE THE PROGRAM CREATOR ID
    $objOLEPROGRAM->setIntExt("INT");
    $objOLEPROGRAM->setAcademicYearID($academicYearID);
    $objOLEPROGRAM->setYearTermID($yearTermID);
    $objOLEPROGRAM->setCanJoin($allowStudentsToJoin);
    $objOLEPROGRAM->setCanJoinStartDate($jp_periodStart);
    $objOLEPROGRAM->setCanJoinEndDate($jp_periodEnd);
    $objOLEPROGRAM->setAutoApprove($autoApprove);
    $objOLEPROGRAM->setJoinableYear($joinableYear);
    $objOLEPROGRAM->setMaximumHours($maximumHours);
    $objOLEPROGRAM->setDefaultHours($defaultHours);
    $objOLEPROGRAM->setCompulsoryFields($compulsoryFields);
    $objOLEPROGRAM->setDefaultApprover($request_approved_by);
    $objOLEPROGRAM->setIsSAS($IsSAS);
    $objOLEPROGRAM->setIsOutsideSchool($InOutSideSchool);

    // Added: 2015-01-12
    if($sys_custom['NgWah_SAS']){
        $objOLEPROGRAM->setSASCategory($SASCategory);
        $objOLEPROGRAM->setSASPoint($SASPoint);
    }

    $objOLEPROGRAM->SaveProgram();

    //SYNC THE PROGRAM TITLE NAME FOR OLE_STUDENT ALSO
    $fields_values = "Title = '$englishTitle', ";
    $fields_values .= "Category = '$category', ";
    $fields_values .= "ELE = '$ELEList', ";
    $fields_values .= "Organization = '$organization', ";
    $fields_values .= "StartDate = '$startdate', ";
    $fields_values .= "EndDate = '$enddate', ";
    $fields_values .= "ModifiedDate = now(),";
    $fields_values .= "IntExt = 'INT' ";
    $sql = "UPDATE {$eclass_db}.OLE_STUDENT SET $fields_values WHERE ProgramID = '$programID'  ";
    $LibPortfolio->db_db_query($sql);
}

# Add Program mapping
if($programID != "")
{
    $lpf_ole_program_tag = new libpf_ole_program_tag();
    $lpf_ole_program_tag->SET_CLASS_VARIABLE("program_id", $programID);
    $lpf_ole_program_tag->SET_CLASS_VARIABLE("tag_id", $tagID);
    $lpf_ole_program_tag->DELETE_PROGRAM_MAPPING(); // 1.delete all program mapping
    if($tagID !="")
    {
        $lpf_ole_program_tag->ADD_PROGRAM_MAPPING(); // 2.re-add program mapping
    }

    # OEA Mapping
    $liboea = new liboea();
    if ($liboea->isEnabledOEAItem())
    {

        if ($oea==1)
        {
            $_MappingDataArr = array();

            if($ItemCode=="")$ItemCode = "00000";
            $_MappingDataArr['OEA_ProgramCode'] = $ItemCode;
            $_MappingDataArr['OLE_ProgramID'] = $programID;
            $_MappingDataArr['DefaultAwardBearing'] = $awardBearing;

            //$_MappingDataArr['DefaultParticipation'] = $_OLE_ProgramNature;
            if ($liboea->Check_If_OLE_OEA_Mapping_Exist($programID))
            {
                // update
                $resultOEA = $liboea->Update_OLE_OEA_Mapping($programID, $_MappingDataArr);
            }
            else
            {

                // insert
                $resultOEA = $liboea->Insert_OLE_OEA_Mapping($_MappingDataArr);
            }
        } else
        {
            # try to remove OEA mapping!
            if ($liboea->Check_If_OLE_OEA_Mapping_Exist($programID))
            {
                $resultOEA = $liboea->Remove_OLE_OEA_Mapping($programID);
            }
        }
    }

    # [2015-0324-1420-39164] - Add Teacher-in-charge to DB
    if($sys_custom['iPortfolio_ole_record_tic']){

        // remove current PICs
        $sql = "DELETE FROM {$eclass_db}.OLE_PROGRAM_TIC WHERE ProgramID = $programID";
        $LibPortfolio->db_db_query($sql);

        // add PICs
        if(isset($teacher_PIC) && sizeof($teacher_PIC) != 0)
        {
            $fieldname = "ProgramID,TeacherID,InputBy,DateInput,ModifiedBy,DateModified";
            $values = "";

            $delim = "";
            $teacher_PIC = array_unique($teacher_PIC);
            for($i=0; $i<sizeof($teacher_PIC); $i++) {
                $this_PIC = $teacher_PIC[$i];
                $this_PIC = str_replace("&#160;", "", $this_PIC);
                $this_PIC = str_replace("U", "", $this_PIC);
                if(trim($this_PIC) != "") {
                    $values .= "$delim ('$programID','$this_PIC','$UserID',now(),'$UserID',now())";
                    $delim = ",";
                }
            }

            $sql = "INSERT INTO {$eclass_db}.OLE_PROGRAM_TIC ($fieldname) VALUES $values";
            $LibPortfolio->db_db_query($sql);
        }
    }
}

intranet_closedb();

header("Location: teacher_ole_add_students.php?programID=$programID");

?>
