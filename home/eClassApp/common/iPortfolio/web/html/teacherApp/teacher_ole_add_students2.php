<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting_search.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

$programID = IntegerSafe($_POST['programID']);
$studentsString = IntegerSafe($_POST['studentsString']);
$recordID = IntegerSafe($_POST['recordID']);

// Initializing classes
$LibUser = new libuser($UserID);
$lpf = new libpf_slp(); // for libwordtemplates_ipf
# template for activity title
$LibWord = new libwordtemplates_ipf(3);
$LibPortfolio = new libpf_slp();
$libdb = new libdb();
$status = "";
$programdata = $LibPortfolio->RETURN_OLE_PROGRAM_BY_PROGRAMID($programID);
if($isEdit==1)
{
    $student = array($studentsString);
    $data = $LibPortfolio->GET_STUDENT_OLE_INFO($programID, $student);

    $showName = "<div class='stdName'>".$data[0][1]."<span class='classClassNo'>".$data[0][ClassNumberText]."</span></div>";

    # attachment

    $tmp_arr = explode(":", $data[0][Attachment]);
    $folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$recordID;
    $folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$recordID;
    $attach_count = 0;
    for ($i=0; $i<sizeof($tmp_arr); $i++)
    {
        $attach_file = $tmp_arr[$i];
        $filename = get_file_basename($attach_file);
        if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
        {
            $file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;

            $attachments_html .= "<span class='text attachment'><div id='div_attachment_$attach_count'><span class='attachIcon'></span><a href=\"".$folder_url."/".$attach_file."\" id='a_$attach_count' class='attachName'>".$filename."</a><a class='removeBtn' data-toggle='modal' data-target='#reject'><span class='removeIcon' onClick='deleteAttachment($attach_count)'></span></a></div></span>";
            $attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\">\n";


            $attach_count ++;
        }
    }

    # get status
    $sql_status= " SELECT os.RecordStatus
              FROM
                {$eclass_db}.OLE_STUDENT AS os         
              WHERE
                os.RecordID = '{$recordID}'";
    $status_result = $libdb->returnArray($sql_status);
    $status = $status_result[0][RecordStatus];

}


# role
//default role
$LibWord2 = new libwordtemplates_ipf(4);
$role_array = $LibWord2->getWordList(0);
for ($i=0; $i<sizeof($role_array); $i++) {
    $RoleName = str_replace("&amp;", "&", undo_htmlspecialchars($role_array[$i]));
    $RoleList .= "<option value='{$RoleName}'>{$RoleName}</option>";
}

if($data[0][Role]!=""){
    $roleSelectDisplay = "style='display: none;'";
    $roleTextDisplay = "style='display: block;'";
}else{
    $roleSelectDisplay = "style='display: block;'";
    $roleTextDisplay = "style='display: none;'";
}
$roleFieldDisplay .= " <span class='label '>{$ec_iPortfolio['ole_role']}</span>
				        <div id='selectRoleField' class='fieldInput' $roleSelectDisplay>
				          <select id='roleSelection' name='roleSelection' class='selectpicker' title='{$Lang['General']['PleaseSelect']}' onChange='hideSelectRoleField()'>
				            <option class='inputBox' value=''>{$Lang['iPortfolio']['OEA']['SelfInput']}</option>
				            {$RoleList}
				          </select>
				        </div>
                        <div id='inputRoleField' class='fieldInput' $roleTextDisplay>
                             <input type='text' class='form-control inputField returnSelection' id='Role' name='Role' placeholder='' value='{$data[0][Role]}'>
                             <span class='returnIcon' onClick='hideInputRoleField()'></span>
                        </div>";

# achievement

//default achievement
$LibWordAward = new libwordtemplates_ipf(5);
$awards_array = $LibWordAward->getWordList(0);
for ($i=0; $i<sizeof($awards_array); $i++) {
    $AwardName = str_replace("&amp;", "&", undo_htmlspecialchars($awards_array[$i]));
    $AwardList .= "<option value='{$AwardName}'>{$AwardName}</option>";
}

if($data[0][Achievement]!=""){
    $roleSelectDisplay = "style='display: none;'";
    $roleTextDisplay = "style='display: block;'";
}else{
    $roleSelectDisplay = "style='display: block;'";
    $roleTextDisplay = "style='display: none;'";
}

$achievementFieldDisplay .= " <span class='label'>{$ec_iPortfolio['achievement']}</span>
                                <div id='selectAwardsField' class='fieldInput' $roleSelectDisplay>
                                  <select id='awardsSelection' name='awardsSelection' class='selectpicker' title='{$Lang['General']['PleaseSelect']}' onChange='hideSelectAwardsField()'>
                                    <option class='inputBox' value=''>{$Lang['iPortfolio']['OEA']['SelfInput']}</option>
                                    {$AwardList}
                                  </select>
                                </div>
                                <div id='inputAwardsField' class='fieldInput' $roleTextDisplay>
                                     <input type='text' class='form-control inputField returnSelection' id='Achievement' name='Achievement' placeholder='' value='{$data[0][Achievement]}'>
                                     <span class='returnIcon' onClick='hideInputAwardsField()'></span>
                                </div>";

# attachment

$attachmentFieldDisplay = "<span class='label'>{$ec_iPortfolio['attachment']}</span>
                          
						   	 {$attachments_html}
						   
						   <div id='attachment_list'>
						   </div>
						   <span class='text addAttachment'>
						      <a href='javascript:add_field();'>{$Lang['iPortfolio']['OLE']['AddCertificationFile']}</a>
						   </span>
						   ";



?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>其他學習經歷</title>
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_teacher.css">
    <script type="text/javascript" src="../../js/ole_teacher.js"></script>
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>

<script>

    function hideSelectRoleField() {
        if ($('#roleSelection').val() == '') {
            $('#selectRoleField').hide();
            $('#inputRoleField').show();
        }

        $('#Role').val($('#roleSelection').val());
    }
    function hideInputRoleField() {
        $('#inputRoleField').hide();
        $('#selectRoleField').show();
    }

    function hideSelectAwardsField() {
        if ($('#awardsSelection').val() == '') {
            $('#selectAwardsField').hide();
            $('#inputAwardsField').show();
        }

        $('#Achievement').val($('#awardsSelection').val());

    }
    function hideInputAwardsField() {
        $('#inputAwardsField').hide();
        $('#selectAwardsField').show();
    }


    var no_of_upload_file = 1;

    function add_field()
    {
        var list = document.getElementById("attachment_list");
        x= '<input class="file" type="file" name="ole_file'+no_of_upload_file+'" size="40">';
        x+='<input type="hidden" name="ole_file'+no_of_upload_file+'_hidden">';

        $("#attachment_list").append(x);
        no_of_upload_file++;
        document.form1.attachment_size.value = no_of_upload_file-1;
    }

    function deleteAttachment(id)
    {
        $("#div_attachment_"+id).hide();
        var list = document.getElementById("attachment_list");
        x='<input type="hidden" name="is_delete'+id+'" value="1">';
        $("#attachment_list").append(x);
    }
</script>

<body>
<form name="form1" method="post" action="teacher_ole_add_students_update.php" enctype="multipart/form-data">
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top ole edit">
        <div id="function"><div class="headerIcon"><a onclick="window.history.go(-1)"><i class="fas fa-arrow-left"></i></a></div></div>
        <div id="headerTitle" class="withFunction"><?=$programdata[Title]?></div>
        <div id="button">
            <div class="headerIcon"></div>
        </div>
    </nav>
    <div id="content" class="approveInfoPage">
        <?=$showName?>
        <ul class="approveInfoList">
            <li>
                <span class="label">狀態</span>
                <div id="selectStatusField" class="fieldInput">
                    <select id="Status" name="Status" class="selectpicker">
                        <option value="1" <?php if($status==1||$status=="") echo "selected"?>><?=$ec_iPortfolio['pending']?></option>
                        <option value="2" <?php if($status==2) echo "selected"?>><?=$ec_iPortfolio['approved']?></option>
                        <option value="3" <?php if($status==3) echo "selected"?>><?=$ec_iPortfolio['rejected']?></option>
                        <option value="4" <?php if($status==4) echo "selected"?>><?=$ec_iPortfolio['teacher_submit_record']?></option>
                    </select>
                </div>
            </li>
            <li>
                <span class="label"><?=$ec_iPortfolio['hours']?></span>
                <div id="inputHoursField" class="fieldInput">
                    <input type="text" class="form-control inputField" id="Hours" name="Hours" placeholder="" value="<?=$data[0][Hours]?>">
                </div>
            </li>
            <li>
                <?=$roleFieldDisplay?>
            </li>
            <li>
                <?=$achievementFieldDisplay?>
            </li>
            <li>
                <?=$attachmentFieldDisplay?>
            </li>
            <li>
                <span class="label"><?=$ec_iPortfolio['comment']?></span>
                <div id="inputCommentField" class="fieldInput">
                    <input type="text" class="form-control inputField" id="TeacherComment" name="TeacherComment" placeholder="" value="<?=$data[0][TeacherComment]?>">
                </div>
            </li>
        </ul>
        <div class="row submitBtn">
            <a class="btn btn-primary" role="button" data-toggle="modal" data-target="#save"><?=$Lang['Btn']['Submit']?></a>
        </div>
    </div>

</div>
<div class="modal fade" id="save" tabindex="-1" role="dialog" aria-labelledby="reject" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modelLabel"><?=$Lang['eClassApp']['iPortfolio']['SaveRecord']?></h5>
            </div>
            <div class="modal-body"><?=$Lang['eClassApp']['iPortfolio']['AreYouSureToSaveTheRecord']?></div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal"><?=$Lang['General']['No']?></button>
                <button class="btn" data-dismiss="modal" onclick="document.form1.submit();"><?=$Lang['General']['Yes']?></button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="programID" id="programID" value="<?=$programID?>" >
<input type="hidden" value = "<?=$studentsString?>" name="studentsString"  id="studentsString">
<input type="hidden" name='attachment_size' id='attachment_size' value='0'>
<input type="hidden" name='attachment_size_current' id='attachment_size_current' value="<?=$attach_count?>">
</form>
</body>
</html>
