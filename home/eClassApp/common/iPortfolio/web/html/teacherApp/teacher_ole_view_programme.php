<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");

intranet_auth();
intranet_opendb();
$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$lay = new academic_year();
$lpf_mgmt_grp = new libpf_mgmt_group();

define ("IS_EDITABLE", 1);
define ("IS_NOT_EDITABLE", 0);

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;

//if(!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")){
    # new button
    $addButton = "<div class=\"headerIcon\"><a class=\"addProg\" href=\"teacher_ole_add_programme.php\"> <span class=\"addProg\"></span> </a></div>";
//}

$currentAcademicYear = Get_Current_Academic_Year_ID();
$filterSchoolYear = IntegerSafe($_GET['filterSchoolYear']);
$filterCategory = IntegerSafe($_GET['filterCategory']);

$IntExtType = "INT";

$academic_year_arr = $lay->Get_All_Year_List();
$filterSchoolYear = (isset($filterSchoolYear))? $filterSchoolYear : Get_Current_Academic_Year_ID();
$ay_selection_html = getSelectByArray($academic_year_arr, "id='filterSchoolYear' class='selectpicker' name='filterSchoolYear' onChange='document.form1.submit()' color='black'", $filterSchoolYear, 1, 0, $i_Attendance_AllYear, 2);
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("id='filterCategory' class='selectpicker' name='filterCategory' onChange='document.form1.submit()' color='black'", $filterCategory, 1);

$cond = empty($filterSchoolYear) ? "" : " AND op.AcademicYearID = '".$filterSchoolYear."'";
$cond .= empty($filterCategory) ? "" : " AND op.Category = '".$filterCategory."'";

//handle for search title
$inputSearch = trim($inputSearch);
if(trim($inputSearch) !="")
{
    $cond .= " AND (op.Title LIKE '%".$inputSearch."%')";
}

$approvalSettings  = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2("INT");
if($approvalSettings["Elements"]["ClassTeacher"] == 1){
    $allowClassTeacherApprove = 1;
}

//1) the teacher is a OLE ADMIN and
//2a) the teacher is big admin "strstr($ck_function_rights, ":manage:")" OR
//2b) the teacher in a group without ELE component restriction "sizeof($mgmt_ele_arr) == 0"
$isAdmin = $LibPortfolio->isOLEMasterAdmin($mgmt_ele_arr);
//debug_r($isAdmin);
//debug_r($action);
$sql = "CREATE TEMPORARY TABLE tempProgramList (ProgramID int(11),editable int , PRIMARY KEY (ProgramID))";
$libdb->db_db_query($sql);
if($isAdmin || ($action == $ipf_cfg["OLE_ACTION"]["view"]))
{
//	echo "is admin<br/>";
    //if login is a admin , insert all record into the temp table with editable is true
    $sql = "insert ignore into tempProgramList(ProgramID , editable) select distinct ProgramID , ".IS_EDITABLE." from {$eclass_db}.OLE_PROGRAM where IntExt  = '{$IntExtType}'";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
    $libdb->db_db_query($sql);
//	debug_r(" admin == >".$sql);
    $sqlDebug[] = $sql;
    $sql = "";
}else
{
    if($allowClassTeacherApprove == 1){

        $sql = "";
        //insert all the record is his / her class student ,
        // if the program is created by a student , it is editable
        // if the program is created by a teacher , it cannot be edited
        $sql = "INSERT IGNORE INTO tempProgramList (ProgramID , editable) ";
        $sql .= "SELECT DISTINCT op.ProgramID , if(op.ComeFrom = ".$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"]." ,".IS_EDITABLE." ,".IS_NOT_EDITABLE.") as 'editValue' ";
        $sql .= "FROM {$eclass_db}.OLE_PROGRAM op ";
        $sql .= "INNER JOIN {$eclass_db}.OLE_STUDENT os ON op.ProgramID = os.ProgramID ";
        $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON os.UserID = ycu.UserID ";
        $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON ycu.YearClassID = yct.YearClassID ";
        $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = yct.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
        $sql .= "WHERE yct.UserID = {$UserID} ";
        $sql .= "ON DUPLICATE KEY UPDATE editable = values(editable)";
//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");

        $libdb->db_db_query($sql);

        $sqlDebug[] = $sql;
//		debug_r(" class teacher ===>".$sql);
    }
    $sql = "";

    //insert all the record is request approve the teacher , including program is created by teacher or student, editable is depend on t.ComeFrom
    // if the program is created by a student , it is editable
    // if the program is created by a teacher , it cannot be edited
    $sql = "INSERT IGNORE INTO tempProgramList (ProgramID , editable) 
				SELECT DISTINCT t.ProgramID , if(t.ComeFrom = ".$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"]." ,".IS_EDITABLE." ,".IS_NOT_EDITABLE.") as 'editValue'
					from {$eclass_db}.OLE_PROGRAM as t 
					inner join {$eclass_db}.OLE_STUDENT as s on s.programid = t.programid 
					where s.RequestApprovedBy = {$UserID}  or s.ApprovedBy = {$UserID}
				 	ON DUPLICATE KEY UPDATE editable = values(editable)
					";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
    $libdb->db_db_query($sql);
    $sqlDebug[] = $sql;
//	debug_r(" request approve by self ===>".$sql);

    //insert all the record for the program is created by the teacher , editable is true
    $sql = "insert ignore into tempProgramList (ProgramID , editable) SELECT DISTINCT ProgramID , ".IS_EDITABLE." FROM {$eclass_db}.OLE_PROGRAM WHERE CreatorID = '{$UserID}'
					ON DUPLICATE KEY UPDATE editable = ".IS_EDITABLE."
				 ";
//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
//	debug_r(" create by self ===>".$sql);

    $libdb->db_db_query($sql);
    $sqlDebug[] = $sql;
//debug_r($group_id_arr);
//debug_r($mgmt_ele_arr);
    //insert all the record for the group component program for the teacher , editable is true
    if(!empty($group_id_arr))
    {
        $conds = "";
        for($i=0, $i_max=count($mgmt_ele_arr); $i<$i_max; $i++)
        {
            $mgmt_ele = $mgmt_ele_arr[$i];

            $conds .= "OR INSTR(op.ELE, '{$mgmt_ele}') > 0 ";
        }

        $sql = "INSERT IGNORE INTO tempProgramList (ProgramID,editable) ";
        $sql .= "SELECT DISTINCT op.ProgramID , ".IS_EDITABLE." FROM {$eclass_db}.OLE_PROGRAM op ";
        $sql .= "WHERE 0 {$conds}";
        $sql .= "ON DUPLICATE KEY UPDATE editable = values(editable)";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
//debug_r(" component ===>".$sql);
        $libdb->db_db_query($sql);
        $sqlDebug[] = $sql;
    }

}

/*Temporary table for storing records counts of programmes [Start]*/
// for improving performance F110154
if($academicYearID != ''){
    $sql = "SELECT ProgramID FROM {$eclass_db}.OLE_PROGRAM where AcademicYearID = '$academicYearID'";
    $targetYearProgramIDArr = $libdb->returnVector($sql);
    $yearProgConds = " AND os.ProgramID IN ('".implode("','", $targetYearProgramIDArr)."') ";
}else{
    $yearProgConds = "";
}

// Temp table for record count
$sql = "CREATE TEMPORARY TABLE tempRecCount (ProgramID int(11), allCount int(8), approveCount int(8), pendingCount int(8), PRIMARY KEY (ProgramID))";
//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");

$libdb->db_db_query($sql);
$sqlDebug[] = $sql;
// for improving performance F110154
//$sql = "INSERT INTO tempRecCount (ProgramID, allCount) SELECT os.ProgramID, count(*) FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' GROUP BY os.ProgramID";
$sql = "INSERT INTO tempRecCount (ProgramID, allCount) SELECT os.ProgramID, count(*) FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' $yearProgConds GROUP BY os.ProgramID";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$libdb->db_db_query($sql);
$sqlDebug[] = $sql;
// for improving performance F110154
//$sql = "INSERT INTO tempRecCount (ProgramID, approveCount) SELECT os.ProgramID, count(*) as cnt FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' AND os.RecordStatus IN (2,4) GROUP BY os.ProgramID ON DUPLICATE KEY UPDATE approveCount = VALUES(approveCount)";
$sql = "INSERT INTO tempRecCount (ProgramID, approveCount) SELECT os.ProgramID, count(*) as cnt FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' AND os.RecordStatus IN (2,4)  $yearProgConds  GROUP BY os.ProgramID ON DUPLICATE KEY UPDATE approveCount = VALUES(approveCount)";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$libdb->db_db_query($sql);
$sqlDebug[] = $sql;
// for improving performance F110154
//$sql = "INSERT INTO tempRecCount (ProgramID, pendingCount) SELECT os.ProgramID, count(*) as cnt FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' AND os.RecordStatus = 1 GROUP BY os.ProgramID ON DUPLICATE KEY UPDATE pendingCount = VALUES(pendingCount)";
$sql = "INSERT INTO tempRecCount (ProgramID, pendingCount) SELECT os.ProgramID, count(*) as cnt FROM {$eclass_db}.OLE_STUDENT os INNER JOIN {$intranet_db}.INTRANET_USER iu ON os.UserID = iu.UserID WHERE os.IntExt = '".($IntExt==1?"EXT":"INT")."' AND os.RecordStatus = 1 $yearProgConds GROUP BY os.ProgramID ON DUPLICATE KEY UPDATE pendingCount = VALUES(pendingCount)";

//error_log($sql."<---\n\n", 3, "/tmp/aaa.txt");
$libdb->db_db_query($sql);
$sqlDebug[] = $sql;
/* Temporary table for storing records counts of programmes [End]*/

$sql = " SELECT 
            op.ProgramID,
            op.Title,
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period,
            IF(tempRecCount.allCount IS NULL, 0, tempRecCount.allCount) AS stuCount,
            IF(tempRecCount.approveCount IS NULL, 0, tempRecCount.approveCount) AS approveCount,
            IF(tempRecCount.pendingCount IS NULL, 0, tempRecCount.pendingCount) AS pendingCount
          FROM
            {$eclass_db}.OLE_PROGRAM AS op
          INNER JOIN tempProgramList AS t_pl
          	ON op.ProgramID = t_pl.ProgramID
          LEFT JOIN {$eclass_db}.OLE_CATEGORY AS oc
            ON op.Category = oc.RecordID
          LEFT JOIN tempRecCount
            ON op.ProgramID = tempRecCount.ProgramID
          WHERE
            op.IntExt = 'INT'
            $cond
          Order By op.StartDate desc
        ";

$result = $libdb->returnArray($sql);

for($i=0;$i<count($result);$i++){
    $programme_list .= "<a href='teacher_ole_programme_student_list.php?programID={$result[$i][ProgramID]}'>
                             <li class='event'>
                                <div class='eventTitle'>{$result[$i][Title]}</div>
                                <div class='eventDate'><span class='label'>{$ec_iPortfolio['date']}︰</span>{$result[$i][period]}</div>
                                <div class='noOfRecord'><span class='icon'></span><span class='num'>{$result[$i][stuCount]}</span> {$Lang['iPortfolio']['OLE']['Records']}</div>";

    if($result[$i][pendingCount]>0){
        $programme_list .= "<div class='status pending'><span class='icon'></span><span class='num'>{$result[$i][pendingCount]}</span> {$Lang['iPortfolio']['OLE']['PendingApprovals']}</div>";
    }
    $programme_list .= "</li></a>";
}

/********************************************************************************
 * Student List View
 ********************************************************************************/
//$filterSchoolYearStudent = (isset($filterSchoolYearStudent))? $filterSchoolYearStudent : Get_Current_Academic_Year_ID();
//
//$ay_selection_Student_html = getSelectByArray($academic_year_arr, "id='filterSchoolYearStudent' class='selectpicker' name='filterSchoolYearStudent' onChange='document.form1.submit()' color='black'", $filterSchoolYearStudent, 1, 0, $i_Attendance_AllYear, 2);


//year dropdown list

$lpf_fc = new libpf_formclass();
$AcademicYearID = Get_Current_Academic_Year_ID();
$libdb = new libdb();

$year_arr = $lpf_fc->GET_CLASSLEVEL_LIST();

$html_year_selection = "<select name='YearID' id='YearID' class='selectpicker' onChange='cleanClass();reload();'>";
$html_year_selection .= "<option value='' selected>{$Lang['eClassApp']['iPortfolio']['AllForms']}</option>";

for($i=0, $i_max=count($year_arr); $i<$i_max; $i++)
{
    $_yID = $year_arr[$i]["YearID"];
    $_yName = $year_arr[$i]["YearName"];
    $showselected = ($YearID==$_yID)?'selected':'';
    $html_year_selection .= "<option value='{$_yID}' $showselected>{$_yName}</option>";
}

$html_year_selection .= "</select>";

//yearclass dropdown list

$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." AS ClassTitle 
        FROM {$intranet_db}.YEAR y
        INNER JOIN {$intranet_db}.YEAR_CLASS yc ON y.YearID = yc.YearID 
        WHERE yc.YearID = '{$YearID}'
        AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
        ORDER BY y.Sequence, yc.Sequence";

$yc_arr = $libdb->returnArray($sql);

$html_yearclass_selection = "<select name='YearClassID' id='YearClassID' class='selectpicker' onChange='reload()'>";
$html_yearclass_selection .= "<option value=''>{$Lang['SysMgr']['FormClassMapping']['AllClass']}</option>";

for($i=0, $i_max=count($yc_arr); $i<$i_max; $i++)
{
    $_ycID = $yc_arr[$i]["YearClassID"];
    $_ycName = $yc_arr[$i]["ClassTitle"];
    $showselected = ($YearClassID==$_ycID)?'selected':'';

    $html_yearclass_selection .= "<option value='{$_ycID}' $showselected>{$_ycName}</option>";
}
$html_yearclass_selection .= "</select>";


$ttable_cond = empty($filterSchoolYearStudent) ? "" : " AND (op.AcademicYearID= '".$filterSchoolYearStudent."')";
$ttable_cond .= ($IntExt == "1") ? " AND op.IntExt = 'EXT'" : " AND op.IntExt = 'INT'";


$YearID = IntegerSafe($_GET['YearID']);
$YearClassID = IntegerSafe($_GET['YearClassID']);
$cond = empty($YearID) ? "" : " AND yc.YearID = '".$YearID."'";
$cond .= empty($YearClassID) ? "" : " AND ycu.YearClassID = '".$YearClassID."'";

$inputStdSearch = trim($inputStdSearch);
if(trim($inputStdSearch) !="")
{
    $cond .= " And (iu.EnglishName Like '%".$libdb->Get_Safe_Sql_Like_Query($inputStdSearch)."%' Or iu.ChineseName Like '%".$libdb->Get_Safe_Sql_Like_Query($inputStdSearch)."%') ";
}

//GET PASSING VARIABLE
$IntExtType = ($IntExt == 1) ? "EXT" : "INT";

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;

$approvalSettings  = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2("INT");
if($approvalSettings["Elements"]["ClassTeacher"] == 1){
    $allowClassTeacherApprove = 1;
}

$mgmt_ele_arr = array();

$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($UserID);
$group_ids = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);

if($IntExtType == "INT"){
    // for the component group , it support with INT only
    //get USER's OLE component
    $group_id_arr = $group_ids;
    $mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
}
$isAdmin = $LibPortfolio->isOLEMasterAdmin($mgmt_ele_arr);

// Group Component
$groupComponentConds = "";
if(!empty($group_id_arr))
{

    for($i=0, $i_max=count($mgmt_ele_arr); $i<$i_max; $i++)
    {
        $mgmt_ele = $mgmt_ele_arr[$i];
        $_tmp = " INSTR(op.ELE, '{$mgmt_ele}') > 0 ";
        $groupComponentConds .= $groupComponentConds == "" ? $_tmp : "OR ".$_tmp;
    }

    $groupComponentConds = $groupComponentConds == "" ? "": " OR ({$groupComponentConds})";
}

if ($allowClassTeacherApprove == 1){
// class student
    $classStudentSql = "SELECT DISTINCT UserID ";
    $classStudentSql .= "FROM {$intranet_db}.YEAR_CLASS_USER ";
    $classStudentSql .= "WHERE YearClassID IN (";
    $classStudentSql .= "SELECT yc.YearClassID FROM {$intranet_db}.YEAR_CLASS yc INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON yc.YearClassID = yct.YearClassID WHERE yct.UserID = '{$UserID}' AND yc.AcademicYearID = '{$currentAcademicYear}'";
    $classStudentSql .= ")";
}

// Temporary table for accessible student
$sql = "CREATE TEMPORARY TABLE tempStudentList (StudentID int(11), YearClassID int(8), PRIMARY KEY (StudentID))";
$libdb->db_db_query($sql);

// Handle admin
if(
    (
        //strstr($ck_function_rights, "Profile:OLR") && count($mgmt_ele_arr) == 0		// OLE admin
        $isAdmin == true // OLE admin
        && (!isset($student_own) || $student_own == "all")		// all student display
    ) || $isReadOnly)
{
    $sql = "INSERT IGNORE INTO tempStudentList (StudentID) ";
    $sql .= "SELECT DISTINCT UserID ";
    $sql .= "FROM {$intranet_db}.INTRANET_USER ";
    $sql .= " WHERE RecordType = 2";
    $libdb->db_db_query($sql);
}
// Handle group teacher
else if($groupComponentConds != "")
{
    $sql = "INSERT IGNORE INTO tempStudentList (StudentID) ";
    $sql .= "SELECT DISTINCT os.UserID ";
    $sql .= "FROM {$eclass_db}.OLE_STUDENT os ";
    $sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM op ON op.ProgramID = os.ProgramID ";
    $sql .= "WHERE 0 {$groupComponentConds}";
    $libdb->db_db_query($sql);
}
// Handle class teacher
if($allowClassTeacherApprove == 1)
{
    $sql = "INSERT IGNORE INTO tempStudentList (StudentID) ";
    $sql .= $classStudentSql;
    $libdb->db_db_query($sql);
}
// Handle approve teacher
$sql = "INSERT IGNORE INTO tempStudentList (StudentID) ";
$sql .= "SELECT DISTINCT UserID ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT ";
$sql .= "WHERE ApprovedBy = '{$UserID}' OR RequestApprovedBy = '{$UserID}'";
$libdb->db_db_query($sql);

// Set current YearClassID for students
$sql = "UPDATE tempStudentList tsl ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON tsl.StudentID = ycu.UserID ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '{$currentAcademicYear}'";
$sql .= "SET tsl.YearClassID = yc.YearClassID";
$libdb->db_db_query($sql);


$sql =  "  CREATE TEMPORARY TABLE tempRecCountStudent(
            UserID int(11),
            editRecCount int(8),
            recCount int(8),
            pendingCount int(8),
            totalPendingCount int(8),
            totalApproveCount int(8),
            PRIMARY KEY (UserID)
          )
        ";
$libdb->db_db_query($sql);

// total rec count of student
$sql = "INSERT INTO tempRecCountStudent (UserID, recCount) ";

$sql .= "SELECT os.UserID, count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE 1 {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
$sql .= "ON DUPLICATE KEY UPDATE recCount = VALUES(recCount)";
$libdb->db_db_query($sql);

// total pending rec count of student
$sql = "INSERT INTO tempRecCountStudent (UserID, totalPendingCount) ";

$sql .= "SELECT os.UserID, count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE os.RecordStatus = {$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]} {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
$sql .= "ON DUPLICATE KEY UPDATE totalPendingCount = VALUES(totalPendingCount)";
$libdb->db_db_query($sql);

// total approve rec count of student
$sql = "INSERT INTO tempRecCountStudent (UserID, totalApproveCount) ";

$sql .= "SELECT os.UserID, count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE os.RecordStatus = {$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]} {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
$sql .= "ON DUPLICATE KEY UPDATE totalApproveCount = VALUES(totalApproveCount)";
$libdb->db_db_query($sql);

// pending rec count of student which can be approved by user
$sql = "INSERT INTO tempRecCountStudent (UserID, pendingCount) ";

$sql .= "SELECT os.USERID, count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE os.RecordStatus = {$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]} ";
$sql .= "AND (op.CreatorID = '{$UserID}' OR os.RequestApprovedBy = '{$UserID}' OR os.ApprovedBy = '{$UserID}' {$groupComponentConds} ";
if($allowClassTeacherApprove == 1){
    $sql .= "OR os.UserID IN ($classStudentSql)";
}
$sql .= ") {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
$sql .= "ON DUPLICATE KEY UPDATE pendingCount = values(pendingCount)";
$libdb->db_db_query($sql);

// rec count of student which can be edited by user
$sql = "INSERT INTO tempRecCountStudent (UserID, editRecCount) ";

$sql .= "SELECT os.USERID,count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE (op.CreatorID = '{$UserID}' OR os.RequestApprovedBy = '{$UserID}' OR os.ApprovedBy = '{$UserID}' {$groupComponentConds} ";

if($allowClassTeacherApprove == 1){
    $sql .= "OR os.UserID IN ($classStudentSql)";
}

$sql .= ") {$ttable_cond} ";

$sql .= "GROUP BY os.USERID ";
$sql .= "ON DUPLICATE KEY UPDATE editRecCount = values(editRecCount)";

$libdb->db_db_query($sql);

$sql = "  SELECT
            iu.UserID,
            ".getNameFieldByLang2("iu.")." AS DisplayName,
		    CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', LPAD(iu.ClassNumber, 2, '0')) AS ClassNumber,
            IF(tempRecCountStudent.totalApproveCount IS NULL, 0, tempRecCountStudent.totalApproveCount) AS totalApproveCount,
            IF(tempRecCountStudent.totalPendingCount IS NULL, 0, tempRecCountStudent.totalPendingCount) AS totalPendingCount
          FROM
          	tempStudentList tsl
          INNER JOIN {$intranet_db}.INTRANET_USER AS iu
          	ON iu.UserID = tsl.StudentID 
		  INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
          	ON iu.UserID = ps.UserID 
          INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
          	ON iu.UserID = ycu.UserID
          INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
			ON ycu.YearClassID = yc.YearClassID
		  LEFT JOIN tempRecCountStudent
			ON iu.UserID = tempRecCountStudent.UserID
          WHERE
            yc.AcademicYearID = '{$currentAcademicYear}'
			And ps.IsSuspend = 0
            $cond
		  Order By ClassNumber asc
        ";
$result = $libdb->returnArray($sql);
for($i=0;$i<count($result);$i++){
    $students_list .= "<a href='student_programme_list.php?studentID={$result[$i][UserID]}'>
                             <li class='student'>
                                <div class='stdName'>{$result[$i][DisplayName]} <span class='classClassNo'>{$result[$i][ClassNumber]}</span></div>
                                <div class='noOfApprovedRecord'>  
                                <div class='noOfApproved'><span class='icon'></span><span class='num'>{$result[$i][totalApproveCount]}</span> {$Lang['iPortfolio']['OLE']['Approvals']}</div>";

    if($result[$i][totalPendingCount]>0){
        $students_list .= "<div class='status pending'><span class='icon'></span><span class='num'>{$result[$i][totalPendingCount]}</span> {$Lang['iPortfolio']['OLE']['PendingApprovals']}</div>";
    }
    $students_list .= "</div></li></a>";
}
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_teacher.css">
    <script type="text/javascript" src="../../js/ole_teacher.js"></script>
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>


<body">
<form name="form1" method="GET">
<div id="wrapper" class="tap">
    <div id="oleTab">
        <!-- Nav tabs -->
        <ul id="tab_header" class="nav nav-tabs navbar-fixed-top" role="tablist">
            <li role="presentation" class="active"><a href="#programme" aria-controls="programme" role="tab" data-toggle="tab"><?=$Lang['eClassApp']['iPortfolio']['Programme']?></a></li>
            <li role="presentation"><a href="#student" aria-controls="student" role="tab" data-toggle="tab"><?=$Lang['eClassApp']['iPortfolio']['Student']?></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="programme">
                <!-- Header -->
                <nav class="navbar tapHeader">
                    <div class="button">
                        <?=$addButton?>
                        <div class="headerIcon"><a class="searchProg" role="button" data-toggle="collapse" href="#searchProg" aria-expanded="false" aria-controls="search"><i class="fas fa-search"></i></a></div>
                        <div class="headerIcon"><a class="filterProg" role="button" data-toggle="collapse" href="#filterProg" aria-expanded="false" aria-controls="filter"> <span class="filter"></span> </a></div>
                    </div>
                    <div id="searchProg" class="collapse">
                        <input type="text" class="form-control" id="inputSearch" name="inputSearch" placeholder="" value="<?php echo stripslashes($inputSearch);?>" onkeydown="if(event.keyCode==13){document.form1.submit()}">
                    </div>
                    <div class="collapse" id="filterProg">
                        <div class="schoolYear">
                           <?php echo $ay_selection_html; ?>
                        </div>
                        <div class="category">
                           <?php echo $category_selection_html; ?>
                        </div>
                    </div>
                </nav>
                <div id="progContent">
                    <ul class="eventList">
                        <?php echo $programme_list;?>
                    </ul>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="student">
                <!-- Header -->
                <nav class="navbar tapHeader">
                    <div class="button">
                        <div class="headerIcon"><a class="searchStd" role="button" data-toggle="collapse" href="#searchStd" aria-expanded="false" aria-controls="search"><i class="fas fa-search"></i></a></div>
                        <div class="headerIcon"><a class="filterStd" role="button" data-toggle="collapse" href="#filterStd" aria-expanded="false" aria-controls="filter"> <span class="filter"></span> </a></div>
                    </div>
                    <div id="searchStd" class="collapse">
                        <input type="text" class="form-control" id="inputStdSearch" name="inputStdSearch" placeholder="" value="<?php echo stripslashes($inputStdSearch);?>" onkeydown="if(event.keyCode==13){document.form1.submit()}">
                    </div>
                    <div class="collapse" id="filterStd">
                        <div class="grade">
                            <?php echo $html_year_selection; ?>
                        </div>
                        <div class="class">
                            <?php echo $html_yearclass_selection; ?>
                        </div>
                    </div>
                </nav>
                <div id="stdContent">
                    <ul class="stdList">
                        <?php echo $students_list;?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name='tab' value='programme'>
</form>
</body>
<script>

    jQuery(document).ready( function() {
        var tab = "<?=$tab?>";
        if(tab=="programme"){
            $('#tab_header a[href="#programme"]').tab('show');
            document.form1.tab.value = 'programme';

        }
        if(tab=="student"){
            $('#tab_header a[href="#student"]').tab('show');
            document.form1.tab.value = 'student';
        }
    });

    $(function(){
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var activeTab = $(e.target).attr('aria-controls');
            document.form1.tab.value = activeTab;
        });
    });

    function cleanClass(){
        $("#YearClassID").val("");
    }
    function reload(){
        document.form1.submit();
    }
</script>
</html>
