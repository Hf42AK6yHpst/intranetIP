<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

$AcademicYearID = IntegerSafe($_GET["AcademicYearID"]);
$category = IntegerSafe($_GET["category"]);

intranet_auth();
intranet_opendb();
$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$lay = new academic_year();
$LibUser = new libuser($UserID);
$StudentLevel = $LibPortfolio->getClassLevel($LibUser->ClassName);
$approvalSettings = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2("INT");

$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "id='filterSchoolYear' class='selectpicker' name='AcademicYearID' onChange='document.form1.submit()'", $AcademicYearID, 1, 0, $i_Attendance_AllYear, 2);
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("id='filterCategory' class='selectpicker' name='category' onChange='document.form1.submit()'", $category, 1);

if($AcademicYearID!="")
{
	$yearConds .= " AND OP.AcademicYearID = '" . $AcademicYearID . "' ";
}

$inputSearch = trim($inputSearch);
if(trim($inputSearch) !="")
{
	$search_title_cond = " AND (OP.Title LIKE '%".$inputSearch."%')";
}

# Get OLE period settings data from DB
$objIpfPeriodSetting = new iportfolio_period_settings();
$OLESettingsArray = $objIpfPeriodSetting->getSettingsArray("OLE", $StudentLevel['YearID']);
# Set start and end date format
$OLESettingsArray['StartDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$OLESettingsArray['StartDate'], (array)$OLESettingsArray['StartHour'], (array)$OLESettingsArray['StartMinute']);
$OLESettingsArray['StartDateTime'] = $OLESettingsArray['StartDateTime'][0];
$SubmissionPeriod_startTime = $OLESettingsArray['StartDateTime'];
$OLESettingsArray['EndDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$OLESettingsArray['EndDate'], (array)$OLESettingsArray['EndHour'], (array)$OLESettingsArray['EndMinute']);
$OLESettingsArray['EndDateTime'] = $OLESettingsArray['EndDateTime'][0];
$SubmissionPeriod_endTime = $OLESettingsArray['EndDateTime'];
$SubmissionPeriod_isAllowed = $OLESettingsArray['AllowSubmit'];

$dateNow = date("Y-m-d H:i:s");
//debug_pr($studentOleConfigDataArray);

if($SubmissionPeriod_startTime!='' && $SubmissionPeriod_endTime!='')
{
	$cond =($SubmissionPeriod_startTime<$dateNow && $SubmissionPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
}
else if($SubmissionPeriod_startTime!='' && $SubmissionPeriod_endTime=='')
{
	$cond =($SubmissionPeriod_startTime<$dateNow)&& $SubmissionPeriod_isAllowed==true;
}
else if($SubmissionPeriod_startTime=='' && $SubmissionPeriod_endTime!='')
{
	$cond =($SubmissionPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
}

if(($SubmissionPeriod_startTime!="" && $SubmissionPeriod_startTime!="0000-00-00 00:00:00") && ($SubmissionPeriod_endTime!="" && $SubmissionPeriod_endTime!="0000-00-00 00:00:00"))
  $Period_Msg = $iPort['period_start_end']."<br>".$SubmissionPeriod_startTime." ".$iPort['to']." ".$SubmissionPeriod_endTime;
else if($SubmissionPeriod_startTime!="" && $SubmissionPeriod_startTime!="0000-00-00 00:00:00")
  $Period_Msg = $iPort['period_start']."<br>".$SubmissionPeriod_startTime;
else if($SubmissionPeriod_endTime!="" && $SubmissionPeriod_endTime!="0000-00-00 00:00:00")
  $Period_Msg = $iPort['period_end']."<br>".$SubmissionPeriod_endTime;
else
  $Period_Msg = "";
	
if($cond==true)
{
	$oleJoinNotes = "<tr><td><br/>". $ec_iPortfolio['SLP']['OleJoinNotes']['WithinSubmissionPeriod'] ."</td></tr>";
}
else
{
	$new_btn = '';
	$oleJoinNotes = "<tr><td><br/>". $ec_iPortfolio['SLP']['OleJoinNotes']['WithoutSubmissionPeriod'] ."</td></tr>";
}

$sql = "SELECT			
		    OP.TITLE,
			AY.AcademicYearID,
			OP.PROGRAMID,
			OSE.RECORDID,
			" . Get_Lang_Selection("OC.CHITITLE", "OC.ENGTITLE") . " as category,
            IF(DATE_FORMAT(OP.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(OP.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(OP.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(OP.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(OP.EndDate, '%Y-%m-%d'))) AS period,
			IF (OP.CANJOINENDDATE='0000-00-00', ' -- ', OP.CANJOINENDDATE) as canJoinEndDate,
			IF (OP.CANJOINENDDATE='0000-00-00', '3000-00-00', OP.CANJOINENDDATE) as canJoinEndDateOrder,			
			CONCAT(CASE OSE.RECORDSTATUS
			WHEN 1 THEN '".$ec_iPortfolio['pending']."'
		    WHEN 2 THEN '".$ec_iPortfolio['approved']."'
		    WHEN 3 THEN '".$ec_iPortfolio['rejected']."'
			WHEN 4 THEN '".$ec_iPortfolio['teacher_submit_record']."'
		   	ELSE '--' END, IF (OSE.PROCESSDATE,CONCAT('<br>',DATE_FORMAT(OSE.PROCESSDATE,'%Y-%m-%d')),'')) as 'RECORDSTATUS'
		FROM $intranet_db.YEAR_CLASS_USER YCU, $intranet_db.YEAR_CLASS YC, $eclass_db.OLE_PROGRAM OP
		LEFT JOIN $eclass_db.OLE_CATEGORY OC ON OP.CATEGORY = OC.RECORDID
		LEFT JOIN $eclass_db.OLE_STUDENT OSE ON (OSE.PROGRAMID = OP.PROGRAMID and OSE.UserID = ".$UserID.")
		LEFT JOIN $intranet_db.ACADEMIC_YEAR AY ON (OP.AcademicYearID = AY.AcademicYearID)
		WHERE
			OP.CANJOIN = 1
		AND
			/*OP.PROGRAMTYPE = 'T' and */ OP.IntExt ='Int' " . 
			(($category=="") ? "" : " AND OP.CATEGORY = '$category'") .$yearConds.
		"AND
		  YC.YEARCLASSID = YCU.YEARCLASSID AND
		  YC.ACADEMICYEARID = '".Get_Current_Academic_Year_ID()."' AND
		  YCU.USERID = '".$UserID."' AND
		  INSTR(OP.JoinableYear, YC.YearID) > 0
		AND
			OP.CANJOINSTARTDATE <= DATE(NOW())
		AND
		(
		    OP.CANJOINENDDATE >= DATE(NOW())
		  OR
		    OP.CANJOINENDDATE = '0000-00-00'
		)".$search_title_cond."
		Order by canJoinEndDateOrder asc"; 
$result = $libdb->returnArray($sql);
for($i=0;$i<count($result);$i++){
	
	$withoutPerdiod = false;	

	if(!$LibPortfolio->isStudentSubmitOLESetting(0, $ck_memberType, $result[$i][PROGRAMID], (array)$OLESettingsArray))
	{
		$withoutPerdiod = true;
	}
	
    $isAllowEditOLE = checkAllowEditOLE($result[$i][RECORDID],$approvalSettings);
	$isAllowEditOLE = ($isAllowEditOLE) && (!$withoutPerdiod);

   if($result[$i][RECORDID]!=""){
   	   if($isAllowEditOLE){
   	   	   $content_url = "iportfolio_ole_newRecord_add.php?recordID={$result[$i][RECORDID]}&isSchoolEvent=1";   	   	
   	   }else{
   	   	   $content_url = "iportfolio_ole_recordDetails.php?recordID={$result[$i][RECORDID]}&isSchoolEvent=1";   	   	  	   	
   	   }
   }else{
   	   if($isAllowEditOLE){
   	   	   $content_url = "iportfolio_ole_newRecord_add.php?programID={$result[$i][PROGRAMID]}&isSchoolEvent=1";   	  	   	   	   	    
   	   }else{
   	   	   $content_url = "iportfolio_ole_recordDetails.php?programID={$result[$i][PROGRAMID]}&isSchoolEvent=1";   	  	   	
   	   }
   }
   $content_list .= "<a href='{$content_url}'><li class='event'>
		                <div class='eventTitle'>{$result[$i][TITLE]}</div>
		                <div class='eventDate'>{$ec_iPortfolio['date']}︰{$result[$i][period]}</div>
		                <div class='eventCategory'>{$ec_iPortfolio['category']}︰{$result[$i][category]}</div>
		                <div class='eventDate'>{$Lang['iPortfolio']['OLE']['JoinEndDate']}︰{$result[$i][canJoinEndDate]}</div>
		              </li></a>";
}

function checkAllowEditOLE($record_id,$approvalSettings) {
	global $eclass_db,$ipf_cfg,$LibPortfolio;
	### References diagram (logic derived from old function)
	### /addon/script/max/doc/References/checkAllowEditOLE.jpeg
	### Refined Logic
	### /addon/script/max/doc/References/checkAllowEditOLE2.jpeg
	############################################################
	if (!empty($record_id)) {
		$sql = "SELECT RecordStatus FROM {$eclass_db}.OLE_STUDENT WHERE RecordID = '$record_id'";
		$status = current($LibPortfolio->returnVector($sql));
	}

	if (
		empty($record_id)
		|| (isset($status) && $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"] || $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"])
		|| (isset($status) && $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]) && (!$approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Editable'])
		) {
		$isAllow = 1;
	} else {
		$isAllow = 0;
	}
	return $isAllow;
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>iPortfolio OLE</title>
<script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../../js/moment.js"></script>
<script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../../js/offcanvas.js"></script>
<link rel="stylesheet" href="../../css/offcanvas.css">
<link rel="stylesheet" href="../../css/ole.css">
<link rel="stylesheet" href="../../css/font-awesome.min.css">
<script type="text/javascript" src="../../js/ole.js"></script>
</head>

<body onLoad="checkForChanges2()">
<form name="form1" method="GET">
<div id="wrapper"> 
  <!-- Header -->
  <div id="header" class="inner">
    <div id="function"><a href="iportfolio_ole.php"><span class="backIcon"></span></a> <span class="text"><?php echo $ec_iPortfolio['SLP']['ReportOtherActivities'];?></span></div>
  </div>
  <div id="alertMsg"> <a class="alertMsg" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><?php echo $Period_Msg;?></a>    
    <div class="collapse in" id="collapseExample">
      <div class="alertDetails">
        <p><?php echo $oleJoinNotes;?></p>
      </div>
    </div>
  </div>
  <div id="schoolEventTitle">
    <div class="text col-xs-8"><span class="text"><?php echo $Lang['iPortfolio']['OLE']['SchoolPresetActivities'];?></span></div>
    <div id="button" class="col-xs-4"> <a role="button" data-toggle="collapse" href="#search" aria-expanded="false" aria-controls="search"> <span class="searchIcon" onClick="search()"></span></a> <a role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter"> <span class="filterIcon" onClick="filter()"></span> </a>
    </div>
    <div class="collapse" id="search" onChange="setHeight2()">
      <input type="text" class="form-control" id="inputSearch" name="inputSearch" placeholder="" value="<?php echo stripslashes($inputSearch);?>">
    </div>
    <div class="collapse" id="filter">
      <?php echo $ay_selection_html; ?>
      <?php echo $category_selection_html; ?>
    </div>
  </div>
  <div id="main">
	  <div class="main"> 
          <div class="eventListArea">
            <ul class="eventList">
               <?php echo $content_list;?>
            </ul>
          </div>
        </div>
  </div> 
  <?php if($cond==true){ ?> 
	<div class="applyNew">
		<a href="iportfolio_ole_newRecord_add.php?&EventType=myEvent&isSchoolEvent=1"><span class="addIcon"></span></a>
	</div>
  <?php }?>   
</div>
</form>
</body>
</html>
