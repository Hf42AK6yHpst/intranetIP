<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

$recordID = IntegerSafe($_GET["recordID"]);
$programID = IntegerSafe($_GET["programID"]);
$isSchoolEvent = $_GET["isSchoolEvent"];

intranet_auth();
intranet_opendb();
$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$LibUserStudent = new libuser($UserID);
$approvalSettings = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2("INT");
$isAllowEditOLE = checkAllowEditOLE($recordID,$approvalSettings);

if($recordID!=""){
	$data = $LibPortfolio->RETURN_OLE_RECORD_BY_RECORDID($recordID);
	list($startdate, $enddate, $title, $category, $ele, $role, $hours, $organization, $achievement, $details, $ole_file, $approved_by, $remark, $process_date, $status, $user_id, $p_intext, $programid, $subcat_id, $maxhours, $compulfields, $request_approve_by, $IsSAS, $IsOutsideSchool) = $data;
}else if($programID!=""){
	$data = $LibPortfolio->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($programID);	
		$startdate = $data['StartDate'];
		$enddate = $data['EndDate'];
		$title = $data['Title'];
		$chiTitle = $data['TitleChi'];
		$category = $data['Category'];
		$subCategoryID = $data['SubCategoryID'];
		$ele = $data['ELE'];
		$organization = $data['Organization'];
		$details = $data['Details'];
		$chiDetails = $data['DetailsChi'];
		$remark = $data['SchoolRemarks'];
		$input_date = $data['InputDate'];
		$modified_date = $data['ModifiedDate'];
		$period = $data['Period'];
		$int_ext = $data['IntExt'];
		$canJoin = $data['CanJoin'];
		$canJoinStartDate = $data['CanJoinStartDate'];
		$canJoinEndDate = $data['CanJoinEndDate'];
		$userName = $data['UserName'];
		$autoApprove = $data['AUTOAPPROVE'];
		$maximumHours = $data['MaximumHours'];
		$defaultHours = $data['DefaultHours'];
		$compulsoryFields = $data['CompulsoryFields'];
		$request_approve_by = $data['DefaultApprover'];
		$IsSAS = $data['IsSAS'];
		$IsOutsideSchool = $data['IsOutsideSchool'];
		$hours=$defaultHours;
}

# get ELE Array
if($ele!="")
{
	# get the ELE code array
	$DefaultELEArray = $LibPortfolio->GET_ELE();
	$ELEArr = explode(",", $ele);
	for($i=0; $i<sizeof($ELEArr); $i++)
	{
		$t_ele = trim($ELEArr[$i]);
		$ele_display .= "- ".$DefaultELEArray[$t_ele]."<br />";
	}
}
else
	$ele_display = "--";
	
$statusArr[1] = $ec_iPortfolio['pending'];
$statusArr[2] = $ec_iPortfolio['approved'];
$statusArr[3] = $ec_iPortfolio['rejected'];

# get category display
$CategoryArray = $LibPortfolio->GET_OLR_CATEGORY();
$category_display = $CategoryArray[trim($category)];

if($startdate=="" || $startdate=="0000-00-00")
	$date_display = "--";
else if($enddate!="" && $enddate != "0000-00-00")
	$date_display = $startdate."&nbsp;".$profiles_to."&nbsp;".$enddate;
else
	$date_display = $startdate;

$attach_count = 0;

$tmp_arr = explode(":", $ole_file);
$folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$recordID;
$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$recordID;
for ($i=0; $i<sizeof($tmp_arr); $i++)
{
	$attach_file = $tmp_arr[$i];
	if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
	{
		# Eric Yip (20090522): Handle chinese-character file name
		$attach_file_url = urlencode($attach_file);
		$attach_file_url = str_replace(array("%2F", "%26", "+"), array("/", "&", " "), $attach_file_url);
	
		$file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;
		$attachments_html .= "<a href=\"".$folder_url."/".$attach_file_url."\" target='_blank' id='a_$attach_count' class='tablelink' >".get_file_basename($attach_file)." ($file_size)</a>";
		$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
		$attach_count ++;
	}
}

if ($intranet_hardcode_lang == "en")
{
	$StudentInfo = $LibUserStudent->EnglishName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")";
}
else
{
	$StudentInfo = $LibUserStudent->ChineseName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")";
}

if($status==1&&$isAllowEditOLE&&!$isSchoolEvent){
	$showEditButton=true;
}else{
	$showEditButton=false;	
}

function checkAllowEditOLE($record_id,$approvalSettings) {
	global $eclass_db,$ipf_cfg,$LibPortfolio;
	### References diagram (logic derived from old function)
	### /addon/script/max/doc/References/checkAllowEditOLE.jpeg
	### Refined Logic
	### /addon/script/max/doc/References/checkAllowEditOLE2.jpeg
	############################################################
	if (!empty($record_id)) {
		$sql = "SELECT RecordStatus FROM {$eclass_db}.OLE_STUDENT WHERE RecordID = '$record_id'";
		$status = current($LibPortfolio->returnVector($sql));
	}

	if (
		empty($record_id)
		|| (isset($status) && $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"] || $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"])
		|| (isset($status) && $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]) && (!$approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Editable'])
		) {
		$isAllow = 1;
	} else {
		$isAllow = 0;
	}
	return $isAllow;
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>iPortfolio OLE</title>
<script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../../js/moment.js"></script>
<script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../../js/offcanvas.js"></script>
<link rel="stylesheet" href="../../css/offcanvas.css">
<link rel="stylesheet" href="../../css/ole.css">
<link rel="stylesheet" href="../../css/font-awesome.min.css">
<script type="text/javascript" src="../../js/ole.js"></script>
</head>

<body>
<div id="wrapper">
<!-- Header -->
<div id="header" class="inner navbar-fixed-top">
  <div id="function"><a href="<?php if($isSchoolEvent) echo "iportfolio_ole_newRecord.php";else echo "iportfolio_ole.php?inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}&tab={$tab}"?>"><span class="backIcon"></span></a> <span class="text"><?php echo $iPort['menu']['ole'];?></span></div>
  <?php if(	$showEditButton){?>
  <div id="button"><a href="iportfolio_ole_newRecord_add.php?recordID=<?=$recordID?>&EventType=myEvent&isEdit=1&inputSearch=<?=$inputSearch?>&filterSchoolYear=<?=$filterSchoolYear?>&filterCategory=<?=$filterCategory?>&tab=<?=$tab?>"><span class="editIcon"></span></a></div>  	
  <?php }?>
</div>
<div id="main" class="inner detail">
  <div class="main">
	  <div class="pageTitle"><?=$title?></div>
	  <table class="tbl_recordDetails">
		  <tr>
			  <td class="fieldTitle"><?=$ec_iPortfolio['date']?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=$date_display?></td>
		  </tr>
		  <tr>
			  <td class="fieldTitle"><?php echo $ec_iPortfolio['category']; ?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=$category_display?></td>
		  </tr>
		  <tr>
			  <td class="fieldTitle"><?php echo $ec_iPortfolio['ele']; ?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=$ele_display?></td>
		  </tr>
		  <tr>
			  <td class="fieldTitle"><?=$ec_iPortfolio['organization']?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=($organization==""?"--":$organization)?></td>
		  </tr>
		  <tr>
			  <td class="fieldTitle"><?=$ec_iPortfolio['hours']?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=($hours==""?"--":$hours)?></td>
		  </tr>
		  <tr>
			  <td class="fieldTitle"><?=$ec_iPortfolio['ole_role']?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=($role==""?"--":$role)?></td>
		  </tr>
		  <tr>
			  <td class="fieldTitle"><?=$ec_iPortfolio['achievement']?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=($achievement==""?"--":$achievement)?></td>
		  </tr>
		  <tr>
			  <td class="fieldTitle"><?=$ec_iPortfolio['attachment']?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=($attachments_html==""?"--":$attachments_html)?></td>
		  </tr>
		  <tr>
			  <td class="fieldTitle"><?=$ec_iPortfolio['details']?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=($details==""?"--":nl2br($details))?></td>
		  </tr>
		  <?php
			if($approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Self'])
		    { 
		  ?>
		  <tr>
			  <td class="fieldTitle"><?=$Lang['iPortfolio']['preferred_approver']?></td>
		  </tr>
		  <tr>
			  <td class="fieldInfo"><?=(($request_approve_by=="" || $request_approve_by==0)?"--":$LibPortfolio->GET_TEACHER_NAME($request_approve_by))?></td>
		  </tr>
		  <?php
			 }
		   ?>
	  </table>
	  <div></div>
	  <div class="fieldInfo"></div>
    
  </div>
</div>
</div>
</body>
</html>
