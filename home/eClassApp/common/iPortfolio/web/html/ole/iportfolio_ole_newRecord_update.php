<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");


intranet_auth();
intranet_opendb();
$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$linterface = new interface_html("iportfolio_default.html");
$IntExt = "INT";
$approvalSettings = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($IntExt);

//////// START SETTING COMMON PARAMETERS ///////
define("OLE_SID", intval($_POST["RecordID"]));	// UPDATE OLE_STUDENT ONLY
define("OLE_PID", intval($_POST["ProgramID"]));	// CAN UPDATE OLE_PROGRAM
$StudentID = intval($_POST["StudentID"]);

$isSchoolEvent = $_POST["isSchoolEvent"];
$StudentID = IntegerSafe($_POST["StudentID"]);
$RecordID = IntegerSafe($_POST["RecordID"]);
$ProgramID = IntegerSafe($_POST["ProgramID"]);
$title = $_POST["title"];
$startdate = $_POST["startdate"];
$enddate = $_POST["enddate"];
$category = IntegerSafe($_POST["category"]);
$subCategory = IntegerSafe($_POST["subCategory"]);
$ele = $_POST["ele"];
$ELEList = (sizeof($ele)!=0) ? implode(",", $ele) : "";
$organization = $_POST["organization"];
$hours = $_POST["hours"];
$ole_role = $_POST["ole_role"];
$achievement = $_POST["achievement"];
$details = $_POST["details"];
$request_approved_by = IntegerSafe($_POST["request_approved_by"]);
$attachment_size = $_POST["attachment_size"];
$attachment_size_current = $_POST["attachment_size_current"];




//debug_pr($StudentID);
//debug_pr($RecordID);
//debug_pr($ProgramID);
//debug_pr($title);
//debug_pr($startdate);
//debug_pr($enddate);
//debug_pr($category);
//debug_pr($subCategory);
//debug_pr($ELEList);
//debug_pr($organization);
//debug_pr($hours);
//debug_pr($ole_role);
//debug_pr($achievement);
//debug_pr($details);
//debug_pr($request_approved_by);
//debug_pr($attachment_size);
//debug_pr($attachment_size_current);
//die();
//IN STUDENT VIEW  (20091231 fai)
//FOR GET_OLR_APPROVAL_SETTING IS "AUTO APPROVE" OR "APPROVE BY CLASS TEACHER OR ADMIN"
//NO MATTER WHO CREATE THE OLE_PROGRAM , ASSUME ALL APPROVED RECORD FOR STUDENT IS APPROVE BY HIS / HER CLASS TEACHER (NOT BY OLE_PROGRAM CREATOR)
//FOR OLE_STUDENT.APPROVEDBY , IT MEAN SUPPOSE THE RECORD WILL APPROVE BY WHOM. I.E IT WILL DEFAULT SAVE A VALUE TO IT ALTHOUGH THE RECORD IS NOT APPROVED
$approvedBy		= getApprovedBy($LibPortfolio, OLE_PID, $StudentID, $approvalSettings);


//$ApprovalSetting == 1 , iportfolio setting for approval is allow student to request a teacher to approve
$requestApprovedBy = ($approvalSettings['Elements']['Self'] == 1) ? $request_approved_by : "-1" ;

// to check whether the teacher selected Default Approver. If Yes, $requestApprovedBy is equal to teacher selected Default Approver ($request_approved_by).
$requestApprovedBy = ($DefaultApprover_IsSelectedbyTeacher==true) ? $request_approved_by : $requestApprovedBy ;


$title = intranet_htmlspecialchars(htmlspecialchars_decode($title));

$category = HTMLtoDB($category);
$subCategory = HTMLtoDB($subCategory);
$ole_role = intranet_htmlspecialchars(HTMLtoDB($ole_role));
$achievement = intranet_htmlspecialchars(HTMLtoDB($achievement));
$organization = intranet_htmlspecialchars(HTMLtoDB($organization));
$details = intranet_htmlspecialchars(HTMLtoDB($details));



$ELEList = (sizeof($ele)!=0) ? implode(",", $ele) : "";

$enddate = ($startdate=="" || $startdate=="0000-00-00") ? "" : $enddate; // set end date as empty if start date is empty
$enddate = ($enddate=="" || $enddate=="0000-00-00") ?"null":$enddate;



# Academic Year ID & Year Term ID
$aytArr = getAcademicYearInfoAndTermInfoByDate($startdate);
$ayID = $aytArr[0];
$ytID = $aytArr[2];


# get the current time to insert both into the OLE_PROGRAM and OLE_STUDENT
$now = date("Y-m-d H:i:s");
$inputDate			= $now;
$modifiedDate		= $now;
$recordType			= $ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"];
$schoolRemarks		= "";
$canJoinStartDate	= "null";
$canJoinEndDate		= "null";
$creatorID			= $StudentID;
			

$user_id			= $StudentID;
$processDate	= $now;
////// END SETTING COMMON PARAMETERS ///////
$programType = "";

if (hasStringValue(OLE_SID)) {
	$programDetails = getProgramDetailsByStudentRecordID(OLE_SID, "", $LibPortfolio);

	//for data migration , if some old data of PROGRAMTYPE in OLE_PROGRAM is empty , default set to T (for safe , since if $programType == "" ,it will create a new program)
	$programType = (trim($programDetails["PROGRAMTYPE"]) == "") ? $ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]: trim($programDetails["PROGRAMTYPE"]);

} else if (hasStringValue(OLE_PID)) {

	$programDetails = getProgramDetailsByProgramId(OLE_PID);

	//for data migration , if some old data of PROGRAMTYPE in OLE_PROGRAM is empty , default set to T (for safe , since if $programDetails ,it will create a new program)
	$programType = (trim($programDetails["PROGRAMTYPE"]) == "") ? $ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]: trim($programDetails["PROGRAMTYPE"]);

} else {
	//There is no PROGRAM ID OR STUDENT ID pass to this program , action create both OLE PROGRAM and OLE STUDENT
	// do nothing for the $programType
}
/* The program logic is over here
if no program type
	- insert OLE_PROGRAM
	- insert OLE_STUDENT
else 
	if type == T [only can modify OLE_STUDENT]
		if OLE_SID
			- update OLE_STUDENT
		else
			- insert OLE_STUDENT
	else if type == S [can modify OLE_PROGRAM and OLE_STUDENT]
		if OLE_SID
			- update OLE_PROGRAM
			- update OLE_STUDENT
	else 
		do nothing
 */
if (empty($programType)) {
	$ProgramID = createProgram(	$recordType,
					$title, $startdate, $enddate, $category, $subCategory,
					$organization, $details, $schoolRemarks, $ELEList, $inputDate,
					$modifiedDate, $IntExt, $ayID, $canJoinStartDate, $canJoinEndDate,
					$ytID, $creatorID,$requestApprovedBy,$IsSAS,$InOutSideSchool);
					
	if($ProgramID > 0)
	{
		//$ProgramID > 0 , program created successfully
		$RecordID = registerProgram($ProgramID, $approvalSettings,
					$user_id, $title, $category, $ELEList, $ole_role,
					$hours, $achievement, $organization, $approvedBy, $details,
					$processDate, $startdate, $enddate, $inputDate,
					$modifiedDate, $IntExt,$requestApprovedBy);
		$msg = "add";
	}
	else
	{	
		//$ProgramID <= 0 , create Program failed , so student will not registerProgram
		$msg = "add_failed";
	}
  # Eric Yip (20100201): parameter for new msg

}
else 
{ 
	if ($programType == $ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]){ 	
		if (hasStringValue(OLE_SID)) {
			updateRegisteredProgram(OLE_SID, $StudentID,
									$title, $category, $ELEList, $ole_role, $hours,
									$achievement, $organization, $approvedBy, $details, $startdate,
									$enddate, $modifiedDate, $IntExt,$requestApprovedBy,$approvalSettings);
								
		# Eric Yip (20100201): parameter for update msg
		$msg = "update";
		} else {
			$RecordID = registerProgram(OLE_PID, $approvalSettings,
							$user_id, $title, $category, $ELEList, $ole_role,
							$hours, $achievement, $organization, $approvedBy, $details,
							$processDate, $startdate, $enddate, $inputDate,
							$modifiedDate, $IntExt,$requestApprovedBy);
			
			# Eric Yip (20100201): parameter for new msg
		$msg = "add";
		}
	}
	else if ($programType == $ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]) {
		
		if (hasStringValue(OLE_SID)) {		
			updateProgram(	OLE_PID,
							$title, $startdate, $enddate, $category, $subCategory,
							$organization, $details, $ELEList, $modifiedDate, $ayID,
							$ytID,$requestApprovedBy,$IsSAS,$InOutSideSchool);
			updateRegisteredProgram(OLE_SID, $user_id,
									$title, $category, $ELEList, $ole_role, $hours,
									$achievement, $organization, $approvedBy, $details, $startdate,
									$enddate, $modifiedDate, $IntExt,$requestApprovedBy,$approvalSettings);
									
		# Eric Yip (20100201): parameter for update msg
		$msg = "update";
		}else {
			$RecordID = registerProgram(OLE_PID, $approvalSettings,
					$user_id, $title, $category, $ELEList, $ole_role,
					$hours, $achievement, $organization, $approvedBy, $details,
					$processDate, $startdate, $enddate, $inputDate,
					$modifiedDate, $IntExt,$requestApprovedBy);
				
			# Eric Yip (20100201): parameter for new msg
			$msg = "add";
		
		}
	}else{
		// do nothing
	}
}




///////////////// START FILE ATTACHMENT ////////////////////

$LibFS = new libfilesystem();

$fileArr = "";

$attachments = "";
// use previous session_id if exists
if ($attachment_size_current>0)
{
	$tmp_arr = explode("/", ${"file_current_0"});
	$SessionID = trim($tmp_arr[0]);
}
if ($SessionID=="")
{
	$SessionID = session_id();
}
$folder_prefix = $eclass_filepath."/files/portfolio/ole/r".$RecordID."/".$SessionID;
//echo "-->$folder_prefix";
# remove unwanted files
for ($i=0; $i<$attachment_size_current; $i++)
{
	$attach_file = ${"file_current_".$i};
	if (${"is_delete".$i})
	{
	    // remove the corresponding file
		$LibFS->file_remove($folder_prefix."/".stripslashes(basename($attach_file)));		
	} else
	{		
		$attachments .= (($attachments=="")?"":":") . $attach_file;
		$fileArr[] = $attach_file;		
	}
}
# add new files
if ($attachment_size>0)
{
	$LibFS->folder_new($eclass_filepath."/files/portfolio/ole");
	$LibFS->folder_new($eclass_filepath."/files/portfolio/ole/r".$RecordID);
	$LibFS->folder_new($folder_prefix);
	
	# copy the files
	for ($i=1; $i<=$attachment_size; $i++)
	{
		$loc = ${"ole_file".($i)};
		$name_hidden = ${"ole_file".$i."_hidden"};
		$filename = (trim($name_hidden)!="") ? $name_hidden : ${"ole_file".($i)."_name"};

		if ($loc!="none" && file_exists($loc))
		{
			if(strpos($filename, ".")==0)
			{
				// Fail
				$isOk = false;
			}
			else
			{
				// Success
				$LibFS->item_copy($loc, stripslashes($folder_prefix."/".$filename));

				$tmp_attach = $SessionID."/".$filename;
				$exist_flag=0;
				for($j=0; $j<sizeof($fileArr); $j++)
				{
					if($tmp_attach==$fileArr[$j])
					{
						$exist_flag = 1;
						break;
					}
				}
				if($exist_flag!=1)
				{
					$attachments .= (($attachments=="")?"":":") . $tmp_attach;
					$fileArr[] = $tmp_attach;
				}
			}
		}
	}
}

$award_file_sql = (trim($attachments)=="") ? "NULL" : "'$attachments'";
$sql = "	UPDATE
				{$eclass_db}.OLE_STUDENT
			SET
				Attachment=$award_file_sql
			WHERE
				RecordID = '$RecordID'
		";
$LibPortfolio->db_db_query($sql);

//# Update Program ID in OLE_STUDENT for records before 2.5 or newly added records
//$LibPortfolio->setProgramType("S");
//$LibPortfolio->INSERT_STUDENT_RECORD();
///////////////// END FILE ATTACHMENT ////////////////////

# inform the corresponding teacher to approve the record
# $requestApprovedBy
if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE'])
{
	include_once($PATH_WRT_ROOT."includes/libemail.php");
	include_once($PATH_WRT_ROOT."includes/libsendmail.php");
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	
	$libuser = new libuser($user_id);
	$StudentName = $libuser->UserNameClassNumber();
	$DateNow = date("Y-m-d");
	if ($requestApprovedBy>0)
	{
			$lwebmail = new libwebmail();
			$ToArray = array($requestApprovedBy);
			$mailSubject = str_replace("[DateNow]", $DateNow, str_replace("[StudentName]", $StudentName, $ec_iPortfolio['SLP']['OLE_approval_mail']['title']));

			$mailBody = str_replace("[Title]", $title, str_replace("[DateNow]", $DateNow, str_replace("[StudentName]", $StudentName, $ec_iPortfolio['SLP']['OLE_approval_mail']['body']))); 
			$mailBody = str_replace("<!--schoolUrl-->", curPageURL($withQueryString=false, $withPageSuffix=false), $mailBody);
			
			$lwebmail->sendModuleMail($ToArray,$mailSubject,$mailBody);
			//die("send on {$DateNow}");
	} else
	{
		# find the class teacher??
		//debug($requestApprovedBy);
		//die();
	}
}
	

intranet_closedb();

header("Location: iportfolio_ole_recordDetails.php?recordID={$RecordID}&isSchoolEvent={$isSchoolEvent}&nputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}&tab={$tab}");
	
///////////////function ////////////
/////// START CORE FUNCTIONS ///////
function createProgram(	$ParProgramType,
						$ParTitle, $ParStartDate, $ParEndDate, $ParCategory, $ParSubCategory,
						$ParOrganization, $ParDetails, $ParSchoolRemarks, $ParELEList,$ParInputDate,
						$ParModifiedDate, $ParIntExt, $ParAcademicYearId, $ParCanJoinStartDate, $ParCanJoinEndDate,
						$ParYearTermId, $ParCreatorID,$ParApprover, $IsSAS, $InOutSideSchool) {

	global $ipf_cfg, $UserID;
	# insert a new OLE_PROGRAM record
	$objOLEPROGRAM = new ole_program();

	$objOLEPROGRAM->setProgramType($ParProgramType);
	$objOLEPROGRAM->setTitle($ParTitle);
	$objOLEPROGRAM->setStartDate($ParStartDate);
	$objOLEPROGRAM->setEndDate($ParEndDate);
	$objOLEPROGRAM->setCategory($ParCategory);
	$objOLEPROGRAM->setSubCategoryID($ParSubCategory);
	$objOLEPROGRAM->setOrganization($ParOrganization);
	$objOLEPROGRAM->setDetails($ParDetails);
	$objOLEPROGRAM->setSchoolRemarks($ParSchoolRemarks);
	$objOLEPROGRAM->setELE($ParELEList);
	$objOLEPROGRAM->setModifyBy($UserID);
	$objOLEPROGRAM->setInputDate($ParInputDate);
	$objOLEPROGRAM->setModifiedDate($ParModifiedDate);
	$objOLEPROGRAM->setIntExt($ParIntExt);
	$objOLEPROGRAM->setAcademicYearID($ParAcademicYearId);
	$objOLEPROGRAM->setCanJoinStartDate($ParCanJoinStartDate);
	$objOLEPROGRAM->setCanJoinEndDate($ParCanJoinEndDate);
	$objOLEPROGRAM->setYearTermID($ParYearTermId);
	$objOLEPROGRAM->setCreatorID($ParCreatorID);
	$objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["studentInput"]);	
	$objOLEPROGRAM-> setDefaultApprover($ParApprover);
	$objOLEPROGRAM->setIsSAS($IsSAS);
	$objOLEPROGRAM->setIsOutsideSchool($InOutSideSchool);

	$ProgramID = $objOLEPROGRAM->SaveProgram();
//debug_pr($objOLEPROGRAM);
	return $ProgramID;
}



function registerProgram(	$ParProgramId, $approvalSettings,
							$ParUserId, $ParTitle, $ParCategory, $ParELEList, $ParRole,
							$ParHour, $ParAchievement, $ParOrganization, $ParApprovedBy, $ParDetails, 
							$ParProcessDate, $ParStartDate, $ParEndDate, $ParInputDate,
							$ParModifiedDate, $ParIntExt,$requestApprovedBy) {
	global $eclass_db;
	global $ipf_cfg;

	$libdb = new libdb();
	
	# insert the student record
	$programIDField = "";
	if (!empty($ParProgramId)) {
		$programIDField = "ProgramID,";
		$programIDValue = "'".$ParProgramId."',";
	}


	$recordStatus = getRecordStatus($libdb, $ParProgramId, $approvalSettings);

//echo "dsdfasd -->".$requestApprovedBy."<Br/>";
	$fields = "
				(UserID, Title, Category, ELE, Role,
				Hours, Achievement, Organization, ApprovedBy, Details,
				" . $programIDField . " RecordStatus, ";

	if($approvalSettings['IsApprovalNeed'] == 1){
		//DO NOTHING
	}else{
		//SINCE THIS IS WITH A AUTO APPROVE , should record the processdate 
		$fields .= " ProcessDate, ";
	}

	$fields .= " StartDate, EndDate,
				InputDate, ModifiedDate, IntExt,ComeFrom,RequestApprovedBy)
			";


	$values = "
				('".$ParUserId."', '".$ParTitle."', '".$ParCategory."', '".$ParELEList."', '".$ParRole."',
				'".$ParHour."', '".$ParAchievement."', '".$ParOrganization."', '".$ParApprovedBy."', '".$ParDetails."',
				" . $programIDValue . "'" . $recordStatus . "',";

	if($approvalSettings['IsApprovalNeed'] == 1){
		//DO NOTHING
	}else{
		$values .= "'" . $ParProcessDate . "', ";
	}

	$values .= "'".$ParStartDate."', '".$ParEndDate."',
				'" . $ParInputDate . "', '" . $ParModifiedDate . "', '" . $ParIntExt . "',".$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"].",'{$requestApprovedBy}')
			";

	$sql = "INSERT INTO {$eclass_db}.OLE_STUDENT $fields VALUES $values";
	$libdb->db_db_query($sql) or die(mysql_error());
	$RecordID = $libdb->db_insert_id();
	$msg = 1;
	return $RecordID;
}

function updateRegisteredProgram(	$ParRecordId, $ParUserId,
									$ParTitle, $ParCategory, $ParELEList, $ParRole, $ParHours,
									$ParAchievement, $ParOrganization, $ParApprovedBy, $ParDetails, $ParStartDate,
									$ParEndDate, $ParModifiedDate, $ParIntExt,$requestApprovedBy,$approvalSettings) {
	global $eclass_db,$LibPortfolio,$ipf_cfg;

	$cond_RecordStatus_Editable = "";
	if (!$approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Editable']) {
		$cond_RecordStatus_Editable = " OR (RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]." AND ComeFrom=".$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"].") ";
	}
	
	$libdb = new libdb();
	$fields_values = "Title = '$ParTitle', ";
	$fields_values .= "Category = '$ParCategory', ";
	$fields_values .= "ELE = '$ParELEList', ";
	$fields_values .= "Role = '$ParRole', ";
	$fields_values .= "Hours = '$ParHours', ";
	$fields_values .= "Achievement = '$ParAchievement', ";
	$fields_values .= "Organization = '$ParOrganization', ";
	$fields_values .= "ApprovedBy = '$ParApprovedBy', ";
	$fields_values .= "Details = '$ParDetails', ";
	$fields_values .= "StartDate = '$ParStartDate', ";
	$fields_values .= "EndDate = '$ParEndDate', ";
	$fields_values .= "ModifiedDate = '$ParModifiedDate', ";
	$fields_values .= "IntExt = '$ParIntExt',";
	$fields_values .= "RequestApprovedBy = '{$requestApprovedBy}'";
	$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET $fields_values WHERE RecordID = '$ParRecordId' AND UserID='$ParUserId' AND (RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]."$cond_RecordStatus_Editable) ";
	$libdb->db_db_query($sql); 
}


function updateProgram(	$ParProgramID,
						$ParTitle, $ParStartDate, $ParEndDate, $ParCategory, $ParSubCategory,
						$ParOrganization, $ParDetails, $ParELEList, $ParModifiedDate, $ParAcademicYearId,
						$ParYearTermId,$ParApprover,$IsSAS,$InOutSideSchool) {
	global $UserID;

	$objOLEPROGRAM = new ole_program($ParProgramID);

	$objOLEPROGRAM->setTitle($ParTitle);
	$objOLEPROGRAM->setStartDate($ParStartDate);
	$objOLEPROGRAM->setEndDate($ParEndDate);
	$objOLEPROGRAM->setCategory($ParCategory);
	$objOLEPROGRAM->setSubCategoryID($ParSubCategory);
	$objOLEPROGRAM->setOrganization($ParOrganization);
	$objOLEPROGRAM->setDetails($ParDetails);
	$objOLEPROGRAM->setELE($ParELEList);
	$objOLEPROGRAM->setModifyBy($UserID);
	$objOLEPROGRAM->setModifiedDate($ParModifiedDate);
	$objOLEPROGRAM->setAcademicYearID($ParAcademicYearId);
	$objOLEPROGRAM->setYearTermID($ParYearTermId);
	$objOLEPROGRAM-> setDefaultApprover($ParApprover);
	$objOLEPROGRAM->setIsSAS($IsSAS);
	$objOLEPROGRAM->setIsOutsideSchool($InOutSideSchool);

	$ProgramID = $objOLEPROGRAM->SaveProgram();
	return $ProgramID;
}
/////// END CORE FUNCTIONS ///////

/////// START MISC FUNCTIONS ///////
function hasStringValue($ParRecordId="") {
	return (!empty($ParRecordId));
}

function getRecordStatus($objDB, $ParProgramID=-1, $approvalSettings) {
	global $eclass_db;
	global $ipf_cfg;

	$programId = -1;

	//SET THE DEFAULT OLE_STUDENT RECORDSTATUS EQUAL TO SYSTEM APPROVAL SETTING
	//$returnRecordStatus = ($approvalSettings['IsNeedApproval']) ? $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] : $ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"];
	$returnRecordStatus = ($approvalSettings['IsApprovalNeed'] == 1) ? $ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"] : $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"];

	
	if (!empty($ParProgramID)) {
		$programId = $ParProgramID;
	}	
	$sql = "select DefaultApprover,
				case 
					when (CanJoin = 1 and autoapprove = 1) then ".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."
					when (CanJoin = 1 and autoapprove <> 1) then ".$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]."
					else ".$returnRecordStatus."
				end
			as \"status\" 
			from ".$eclass_db.".OLE_PROGRAM  where programid  = '".$programId."'";
//debug_r($sql);
	$returnArr = $objDB->returnArray($sql);

	//suppose it should be get only one program id
	if (count($returnArr) == 1 && is_array($returnArr)) {
		$returnRecordStatus = $returnArr[0]["status"];		
	}
	return $returnRecordStatus;
}


function getApprovedBy($objDB, $ParProgramID=-1, $StudentID, $approvalSettings) {
	global $eclass_db;

##################################################################################
##						|Previous Created by teacher	|
##----------------------+-------------------------------+---------------------
##						|自動認可		|不自動認可		|Student Create
##----------------------+---------------+---------------+---------------------
##	班主任/ADMIN Approve	|自己 1st if		|0				|NULL
##----------------------+---------------+---------------+---------------------
##	學生選擇批核老師		|自己 1st if		|0				|NULL
##----------------------+---------------+---------------+---------------------
##	不用批核				|自己 1st if		|0				|自己 2nd if
##----------------------+---------------+---------------+---------------------
##################################################################################


$sql = "SELECT IF ((SELECT COUNT(*) FROM $eclass_db.OLE_PROGRAM OPI WHERE OPI.PROGRAMID = '" . ( !empty($ParProgramID) ? $ParProgramID : '-1' ) . "' AND AUTOAPPROVE = 1)>0,
					$StudentID, 
					IF (".(!$approvalSettings['IsApprovalNeed']?1:0)."=1 AND $ParProgramID<1,$StudentID,NULL))";
	
	$returnArr = current($objDB->returnVector($sql));
	
	return $returnArr;
}

function getProgramDetailsByProgramId($ParProgramId=0) {
	global $eclass_db;
	$libdb = new libdb();
	$returnArray = array();
	
	$sql = "
SELECT PROGRAMID, PROGRAMTYPE
FROM $eclass_db.OLE_PROGRAM
WHERE PROGRAMID = '$ParProgramId'
			";
	$result = $libdb->returnArray($sql) or die(mysql_error());
	
	if(sizeof($result) == 1 && is_array($result))
	{
		//SUPPOSE CAN GET ONLY ONE PROGRAMID
		return $result[0];
	}else
	{
		//IF THE RESULT IS NOT EQUAL TO 1 , SHOULD HAVE ERROR
		return null;
	}
}

//$creatorID can be empty 
function getProgramDetailsByStudentRecordID($ole_student_recordId,$creatorID,$objDB)
{
	
	global $eclass_db;
	$createdByCond = "";
	if(intval($creatorID) > 0)
	{
		//$creatorID must be a integer and cannot empty
		$createdByCond = " and p.CreatorID = '".$creatorID."'";
	}

	$sql = "select 
				p.ProgramID as PROGRAMID,
				p.ProgramType	as PROGRAMTYPE
			from 
				$eclass_db.OLE_PROGRAM as p inner join 
				$eclass_db.OLE_STUDENT as s on s.programid = p.programid 
			where 
				s.RecordID = '".$ole_student_recordId."'".
				$createdByCond." 
			";
	$result = $objDB->returnArray($sql);
	if(sizeof($result) == 1 && is_array($result))
	{
		//SUPPOSE CAN GET ONLY ONE PROGRAMID
		return $result[0];
	}else
	{
		//IF THE RESULT IS NOT EQUAL TO 1 , SHOULD HAVE ERROR
		return null;
	}

}
///// END MISC FUNCTIONS ///////
//----------------------------------------------------------------
?>