<?php 
# using:anna

###################################################### Change Log ######################################################

# Date      : 2019-01-11 (Tiffany)
# Detail    : Support Description for Eng by $sys_custom['eClassApp']['SchoolInfo']['SupportEnglishContent'] = true
# Deploy    :
#
# Date      : 2017-08-09 (Tiffany)
# Detail    : Hide the icon if the page didn't use icon
# Deploy    : 
#
# Date      : 2017-07-20 (Roy)
# Detail    : add pdf file support
# Deploy    : 
#
########################################################################################################################
$PATH_WRT_ROOT = '../../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_schoolInfo.php");

$uid = IntegerSafe($_GET['uid']);
$ParentMenuID = IntegerSafe($_GET['ParentMenuID']);

$UserID = $uid;
$_SESSION['UserID'] = $uid;
$_SESSION['intranet_session_language'] = $intranet_hardcode_lang;
$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

intranet_auth();
intranet_opendb();

$libdb = new libdb();
$linterface = new interface_html();
$libeca_si = new libeClassApp_schoolInfo();

$schoolDomain = curPageURL($withQueryString=false, $withPageSuffix=false);
if(substr($schoolDomain,-1)=="/") {
    $schoolDomain = substr($schoolDomain,0,-1);
}
$PDFSsoParam = $libeClassApp->getSSOParam($uid, 'SchoolInfoPDF');


$menu_list = "";

if(!isset($AppType) || $AppType==""){

	$AppType = "T";
}

if ($sys_custom['eClassTeacherApp']['iMailToolbarColorCode']) {
	$color = $sys_custom['eClassTeacherApp']['iMailToolbarColorCode'];
}
else {
	if($AppType=="T"){
		$color = "#3689E4";
	}else if($AppType=="S"){
		$color = "#F8BD10";
	}else if($AppType=="dhl"){
		$color = "#FFCC00";
	}else{
		$color = "#16A30D";
	}
}

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."/includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isAdmin = 0;
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]){
        $isAdmin = 1;
    }
    $UserPICInfoAry = $libdhl->getPICResponsInfo();
    $PICCompanyIDAry = $UserPICInfoAry['Department'];
    
} 
if(!isset($ParentMenuID) || $ParentMenuID==""){
	
	$ParentMenuID = 0;
}
if($ParentMenuID!=0){

	$result =$libeca_si->getSchoolInfoDetail($ParentMenuID);
	$Title = Get_Lang_Selection($result[0][TitleChi], $result[0][TitleEng]);
	$Type = $result[0][IsItem];
	
	if($sys_custom['DHL']){
	    $resultAry = array();
	
	    for($i=0;$i<count($result);$i++){
	        $targetGroupType = $libdhl->getSchoolNewsTargetGroup($result[$i][MenuID]);
	        if($targetGroupType == '1' || $targetGroupType == ''){  // all user
	            $resultAry[] = $result[$i];
	        }	      
	    }
	    
	    for($i=0;$i<count($result);$i++){
	        $targetGroupType = $libdhl->getSchoolNewsTargetGroup($result[$i][MenuID]);
	        
	        if($isAdmin){
	            if($targetGroupType == '0'){ // specific user
	                $resultAry[] = $result[$i];
	            }
	        }else{
	            
	            $DepartmentIDAry = $libdhl->getSchoolNewsDepartmentList($result[$i][MenuID]);
	            if(!empty($DepartmentIDAry)){
	                for($j=0;$j<count($DepartmentIDAry);$j++){
	                    $DepartmentStaff = $libdhl->getDepartmentUserRecords(array('DepartmentID'=>$DepartmentIDAry[$j]));
	                    
	                    $inDartmentStaffIDAry = Get_Array_By_Key($DepartmentStaff,'UserID');;
	                    if($targetGroupType == '0' && (in_array($DepartmentIDAry[$j],$PICCompanyIDAry) || in_array($_SESSION['UserID'],$inDartmentStaffIDAry))){
	                        $resultAry[] = $result[$i];
	                        break;
	                    }
	                }
	            }
	        }
	    }
	    $result = $resultAry;
	}

}

/*
if($Type == '1'){
	$menu_list.="<table width='100%' style = 'text-align:center;background:".$color.";color:white;text-shadow: none!important;'><tr><td width='5px' ><a href='javascript:goback();'> <img src='".$image_path."/".$LAYOUT_SKIN."/iMail/app_view/white/icon_outbox_white.png' style='width:2em; height:2em;' /></a>
			                     </td><td  style = 'padding-right:12%'>".$Title."</td></tr></table>";
	$menu_list.="<div id='descriptionDiv' style='overflow-x: auto; overflow-y: hidden;'><table width='100%' ><tr><td style = 'border-bottom:0px'>".htmlspecialchars_decode($result[0][Description])."</td></tr></table></div>";
	
}else {
	if($ParentMenuID!=0){
	    $menu_list.="<table width='100%' style = 'text-align:center;background:#E4E4E4;'><tr><td width='5px' ><a href='javascript:goback();'> <img src='".$image_path."/".$LAYOUT_SKIN."/iMail/app_view/white/icon_outbox_white.png' style='width:2em; height:2em;' /></a>
			                     </td><td  style = 'padding-right:12%'>".$Title."</td></tr></table>";
	}

	$result=$libeca_si->getItemDetail($ParentMenuID);

	if(COUNT($result)!=0)$menu_list.="<table class='form_table' width='100%'>";
	for($i=0;$i<COUNT($result);$i++){
		$MenuID = $result[$i][MenuID];
		$TitleChi = $result[$i][TitleChi];
		$TitleEng = $result[$i][TitleEng];
		$Icon = $result[$i][Icon];
		$IconType = $result[$i][IconType];
		
		$Title = Get_Lang_Selection($TitleChi, $TitleEng);
		if($IconType==0){
			$menu_list .="<tr><td width='70px' style = 'text-align:center;'>&nbsp;</td>";
	
		}else if($IconType==1){
			$menu_list .="<tr><td width='70px' style = 'text-align:center;'><img src='".$image_path."/".$LAYOUT_SKIN."/eClassApp/school_info/icon_default".$Icon.".png' style='width:50px; height:50px;' /></td>";
		}else if($IconType==2){
			$menu_list .="<tr><td  width='70px' style = 'text-align:center;'><img src='".$PATH_WRT_ROOT.$eclassAppConfig['urlBrowsePath'].$imagePath.$Icon."' style='width:50px; height:50px;' /></td>";
		}
	
		$menu_list.= "<td>".$Title."</td><td width='50px'  style='text-align:center'><a href='menu.php?ParentMenuID=".$MenuID."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AppType=".$AppType."' data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td></tr>";
	
	}	
	if(COUNT($result)!=0)$menu_list.="</table>";
	
}
*/

if($Type == '1'){
	$menu_list.="<table width='100%' style = 'text-align:center;background:".$color.";color:white;text-shadow: none!important;'><tr><td width='5px' height='30px' ><a href='javascript:goback();'> <img src='".$image_path."/".$LAYOUT_SKIN."/iMail/app_view/white/icon_outbox_white.png' style='width:2em; height:2em;' /></a>
			                     </td><td  style = 'padding-right:12%'>".$Title."</td></tr></table>";
	if($sys_custom['eClassApp']['SchoolInfo']['SupportEnglishContent']){
	    if($parLang=="en"){
            $Description = $result[0][DescriptionEng];
        }else{
            $Description = $result[0][Description];
        }
    }else{
        $Description = $result[0][Description];
    }
	$menu_list.="<div id='descriptionDiv' style='overflow-x: auto; overflow-y: hidden;'><table width='100%' ><tr width='100%'><td style = 'border-bottom:0px'>".htmlspecialchars_decode($Description)."</td></tr></table></div>";

}else {
	if($ParentMenuID!=0){
		$menu_list.="<table width='100%' style = 'text-align:center;background:#E4E4E4;'><tr><td width='5px' height='30px'><a href='javascript:goback();'> <img src='".$image_path."/".$LAYOUT_SKIN."/iMail/app_view/white/icon_outbox_white.png' style='width:2em; height:2em;' /></a>
			                     </td><td  style = 'padding-right:12%'>".$Title."</td></tr></table>";
	}

	$result = $libeca_si->getItemDetail($ParentMenuID);

	if($sys_custom['DHL']){
	    $resultAry = array();
	    for($i=0;$i<count($result);$i++){
	        $targetGroupType = $libdhl->getSchoolNewsTargetGroup($result[$i][MenuID]);
	        if($targetGroupType == '1' || $targetGroupType == ''){  // all user
	            $resultAry[] = $result[$i];
	        }
	        
	      
	    } 
	    for($i=0;$i<count($result);$i++){
	        $targetGroupType = $libdhl->getSchoolNewsTargetGroup($result[$i][MenuID]);
	        
	        if($isAdmin){
	            if($targetGroupType == '0'){ // specific user
	                $resultAry[] = $result[$i];
	            }
	        }else{
	            
	            $DepartmentIDAry = $libdhl->getSchoolNewsDepartmentList($result[$i][MenuID]);
	            if(!empty($DepartmentIDAry)){
	                for($j=0;$j<count($DepartmentIDAry);$j++){
	                    $DepartmentStaff = $libdhl->getDepartmentUserRecords(array('DepartmentID'=>$DepartmentIDAry[$j]));
	                    
	                    $inDartmentStaffIDAry = Get_Array_By_Key($DepartmentStaff,'UserID');;
	                    if($targetGroupType == '0' && (in_array($DepartmentIDAry[$j],$PICCompanyIDAry) || in_array($_SESSION['UserID'],$inDartmentStaffIDAry))){
	                        $resultAry[] = $result[$i];
	                        break;
	                    }
	                }
	            }
	        }
	    }
	    $result = $resultAry; 
	}

	if(COUNT($result)!=0)$menu_list.="<ul data-role='listview'>";
	$hideIcon = true;
	for($i=0;$i<COUNT($result);$i++){
		$IconType = $result[$i][IconType];
		if($IconType!=0) $hideIcon = false;		
	}
	if($hideIcon){
		$icon_field_width = "10px";
	}else{
		$icon_field_width = "70px";
	}
	for($i=0;$i<COUNT($result);$i++){
		$MenuID = $result[$i][MenuID];
		$TitleChi = $result[$i][TitleChi];
		$TitleEng = $result[$i][TitleEng];
		$Icon = $result[$i][Icon];
		$IconType = $result[$i][IconType];
		$ItemType = $result[$i][IsItem];
		$ItemUrl = $result[$i][Url];
		$pdfPath = $result[$i][PdfPath];
		$pdfName = $result[$i][PdfName];

		$Title = Get_Lang_Selection($TitleChi, $TitleEng);

        $pdfAuthUrl = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$PDFSsoParam.'&&RecordID='.$MenuID;

        if ($ItemType == 2) {
			$menu_list.="<li data-icon='false' style='padding-left: 0px;'><a href='".$ItemUrl."' data-role='button' style='background: transparent !important;font-weight:normal!important;border-top-width: 0px;border-bottom-width: 0px;margin-top: 0px;margin-bottom: 0px;padding: 0px;' data-transition='slide';><table width='100%'>";			
		} else if ($ItemType == 3) {
            $menu_list.="<li data-icon='false' style='padding-left: 0px;'><a href='".$pdfAuthUrl."' target='blank' data-role='button' style='background: transparent !important;font-weight:normal!important;border-top-width: 0px;border-bottom-width: 0px;margin-top: 0px;margin-bottom: 0px;padding: 0px;' data-transition='slide';><table width='100%'>";
//			$menu_list.="<li data-icon='false' style='padding-left: 0px;'><a href='"."pdfjs_viewer_custom/web/viewer.php?MenuID=".$MenuID."&uid=".$UserID."&token=".$pdf_token."' target='blank' data-role='button' style='background: transparent !important;font-weight:normal!important;border-top-width: 0px;border-bottom-width: 0px;margin-top: 0px;margin-bottom: 0px;padding: 0px;' data-transition='slide';><table width='100%'>";
		} else {
			$menu_list.="<li data-icon='false' style='padding-left: 0px;'><a href='menu.php?ParentMenuID=".$MenuID."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AppType=".$AppType."' data-role='button' style='background: transparent !important;font-weight:normal!important;border-top-width: 0px;border-bottom-width: 0px;margin-top: 0px;margin-bottom: 0px;padding: 0px;' data-transition='slide';><table width='100%'>";			
		}
		
		if($IconType==0){
			$menu_list .="<tr><td class='icon_field' width='".$icon_field_width."' height='60px' style = 'text-align:center;'>&nbsp;</td>";

		}else if($IconType==1){
			$menu_list .="<tr><td width='70px' height='60px' style = 'text-align:center;'><img src='".$image_path."/".$LAYOUT_SKIN."/eClassApp/school_info/icon_default".$Icon.".png' style='width:50px; height:50px;' /></td>";
		}else if($IconType==2){
			$menu_list .="<tr><td  width='70px' height='60px' style = 'text-align:center;'><img src='".$PATH_WRT_ROOT.$eclassAppConfig['urlBrowsePath'].$imagePath.$Icon."' style='width:50px; height:50px;' /></td>";
		}

        if($AppType=="dhl"){
			$menu_list.= "<td style = 'text-align:left;'><div class='titleDiv' style='overflow-x:auto;'>".$Title."</div></td><td style = 'text-align:right;'><img src='".$image_path."/".$LAYOUT_SKIN."/eClassApp/school_info/dhl_arrow_right.png' style='width:15px; height:30px;' /></td></tr>";
        }else{
			$menu_list.= "<td style = 'text-align:left;'><div class='titleDiv' style='overflow-x:auto;'>".$Title."</div></td><td style = 'text-align:right;'><img src='".$image_path."/".$LAYOUT_SKIN."/eClassApp/school_info/action_next.png' style='width:30px; height:30px;' /></td></tr>";
        }
		$menu_list.="</table></a></li>";
		
	}

	if(COUNT($result)!=0)$menu_list.="</ul>";
}



//echo $libeClassApp->getAppWebPageInitStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no" /> -->


<script>

	jQuery(document).ready( function() {
		$('div#descriptionDiv').css('width', $(window).width() - 10);
	});
	
	$(document).bind('pageinit', function() {
		var screen_width = $(window).width()-10;
		$('#descriptionDiv img').each( function () {
			var old_width = $(this).width();
			var old_height = $(this).height();
			var pro = old_height/old_width;
		
			$(this).width(screen_width);
			$(this).height(screen_width*pro);
			
		});
	    $('div.titleDiv').css('width', $(window).width() - 100);
	   
    });
    
	function goback(){
		 window.history.back()
    }
    
</script>
<style type="text/css">
#body{
            
   background:#ffffff;
}
.ui-btn-active 
{ 
    color: #000000 !important; 
    text-shadow: 0px 1px 1px #000000 !important;

}

.form_table tr td{vertical-align:center; line-height:50px; padding-top:5px; padding-bottom:5px;	border-bottom:2px solid #EFEFEF; padding-left:2px; padding-right:2px}
</style>
</head>
	<body>
	
		<div data-role="page" id="body">			
   	      				
		    <form id="form1" name="form1" method="get" >		   
                 
                      <?=$menu_list?>                                                                        
                                                                                       
			</form>
				 

	    </div>
	   
	</body>
</html>

<?php

intranet_closedb();

?>