<?php
# using: Roy

###################################################### Change Log ######################################################
# Date      : 2017-07-20 (Roy)
# Detail    : Create file
# Deploy    :
#
########################################################################################################################

$PATH_WRT_ROOT = '../../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_schoolInfo.php");

intranet_opendb();

// passed in values:
$MenuID = $_GET['MenuID'];

// init helper classes:
$libdb = new libdb();
$libeca_si = new libeClassApp_schoolInfo();

// get school info item details
$itemDetails = $libeca_si->getEditDetail($MenuID);
$itemType = $itemDetails[0]['IsItem'];

if ($itemType == 3) {
	// this is pdf
	$pdfPath = $itemDetails[0]['PdfPath'];
	$pdfLocation = $PATH_WRT_ROOT.$eclassAppConfig['urlBrowsePath'].$pdfPath;

	$fileName = basename($pdfLocation);
	output2browserByPath($pdfLocation, $fileName);
	
} else {
	// this is not pdf type
}

intranet_closedb();
?>