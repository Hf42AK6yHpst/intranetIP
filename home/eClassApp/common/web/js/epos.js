
jQuery(document).ready(function() {

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            $("#blkTopBar").addClass("scrolled");
        }else{
            $("#blkTopBar").removeClass("scrolled");
        }
    });

  /*Text input*/
  var $tbxFloat = $('.textbox-floatlabel input[type="text"], .textbox-floatlabel input[type="password"], .textbox-floatlabel textarea');
  $tbxFloat.blur(function(){
      tmpval = $(this).val();
      if(tmpval == '') {
          $(this).addClass('empty');
          $(this).closest(".itemInput").addClass('empty');
          $(this).removeClass('notEmpty');
          $(this).closest(".itemInput").removeClass('notEmpty');
      } else {
          $(this).addClass('notEmpty');
          $(this).closest(".itemInput").addClass('notEmpty');
          $(this).removeClass('empty');
          $(this).closest(".itemInput").removeClass('empty');
      }
  });

    /* Calculate height of .notificationBox*/
    var ht;
    $(".notificationBox")
        .addClass("calcHeight")
        .height(function(i,h){return ht=h})
        .hide()
        .removeClass("calcHeight hidden");

  /*Top Bar*/

    $(document).on('click', '#blkTopBar #btnHistory', function(e) {
  //$("#blkTopBar #btnHistory").click(function() {
		$("#blkTopBar #blkHistory").fadeToggle(150).toggleClass("popUp-active").siblings(".notificationBox.popUp-active").fadeToggle(150).toggleClass("popUp-active");
	});

    $(document).on('click', '#blkTopBar #btnPending', function(e) {
  //$("#blkTopBar #btnPending").click(function() {
		$("#blkTopBar #blkPending").fadeToggle(150).toggleClass("popUp-active").siblings(".notificationBox.popUp-active").fadeToggle(150).toggleClass("popUp-active");
	});

    $(document).on('click', '#blkTopBar #btnSearch', function(e) {
  //$("#blkTopBar #btnSearch").click(function() {
		$("#blkTopBar #blkTopSearch").fadeToggle(150).toggleClass("popUp-active").siblings(".notificationBox.popUp-active").fadeToggle(150).toggleClass("popUp-active");
	});

    $(document).on('click', '.product-item-row', function(e) {
        e.preventDefault();
        $this = $(this);
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=management.ajax',
            data : {
                'action': 'getOrderDetails',
                'TransactionLogID': $(this).attr('data-TransactionLogID')
            },
            success: function(ajaxReturn) {
                showOrderDetails(ajaxReturn,$this);
            },
            error: show_ajax_error
        });
    });

    $(document).on('click', '#DownloadReceipt', function(e) {
        e.preventDefault();
//        alert('test');
        $this = $(this);
        $this.parent().submit();
    });

    /* Disable body scroll when dialog displays */
    $(window).scroll(function() {
        var offsetY = $(window).scrollTop();
        var $body = $("body");

        $.fn.bodyLock = function(){
            // Block scrolling
            $body.css({
                'top': -offsetY + 'px'
            });
            $body.addClass("bodyLock");
        }

        $.fn.bodyUnlock = function(){
            var scroll = $body.position().top;
            // Allow scrollings
            $body.css({
                'top': '0px'
            });
            $body.removeClass("bodyLock");
            $(window).scrollTop(-scroll)
        }
    });

    // initialize bodyLock and bodyUnlock
    $(window).scroll();

    return false;
});     // end document ready function

