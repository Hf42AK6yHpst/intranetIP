
/* Set content height start */
function adjustContentHeight()
{
    var deviceHeight = document.documentElement.clientHeight;
    var subTitleHeight = document.getElementsByClassName('subTitleFix')[0].clientHeight;
    var x = deviceHeight - subTitleHeight - 53;
    document.getElementsByClassName('contentFixHeight')[0].style.marginTop = subTitleHeight + 51 + 'px';
    document.getElementsByClassName('contentFixHeight')[0].style.height = x + 'px';
    /*eventStdListHeight = x + 'px';*/
}

function adjustStocktakeDetailHeight()
{
	var deviceHeight = document.documentElement.clientHeight;
	var subTitleHeight = document.getElementsByClassName('subTitle')[0].clientHeight;
	var x = deviceHeight - subTitleHeight - 53;
	document.getElementsByClassName('stockingDetails')[0].style.marginTop = subTitleHeight + 51 + 'px';
	document.getElementsByClassName('stockingDetails')[0].style.height = x + 'px';
}

function showMore()
{
	$("#showMore a").click(function() {
		$("#moreInfo").show('slow');
		$("#moreInfo").focus();
		$('#showMore').hide();
		return false;
	});
	$("#hideInfo a").click(function() {
		$("#showMore").show('slow');
		$("#showMore").focus();
		$('#moreInfo').hide();
		return false;
    });
}
/* Set content height end */