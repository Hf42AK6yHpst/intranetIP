
jQuery(document).ready(function() {
    var itemIDToRemove;
    var qtyToRemove;

    /*Cart Tray slide effect*/

//    $("#cHome #blkTray #btnTrayToggle").click(function () {
    $(document).on('click', '#btnTrayToggle', function() {
        $(this).toggleClass("showTray");
        $(this).parent("#blkTray").toggleClass("showTray");
        $("body").toggleClass("showTray");
    });

    /*Add class*/
    $(".product-item-row").addClass("transition");
    $(".product-item-block").addClass("transition");

    /* Fill photo in .product-item-thumbnail and keep scale */
    $('.product-item-row .product-item-thumbnail > img').each(function(_, img) {

        var $this = $(this);

        if( $this.height() / $this.width() > 0.8 ) {
            $this.addClass("portrait");
        }
    });

    $('.product-item-block .product-item-thumbnail > img').each(function(_, img) {

        var $this = $(this);

        if( $this.height() / $this.width() > 0.6667 ) {
            $this.addClass("portrait");
        }
    });

    /* Calculate cHome item width */

    $.fn.setHomeItemWidth = function(){
        var $itemContainer = $("#cHome .product-item-container");
        if(( $itemContainer.width() >= 1254 )) {
            $itemContainer.find("> .product-item-block").addClass("in-6");
        } else {
            $itemContainer.find("> .product-item-block").removeClass("in-6");
        };
        if(( $itemContainer.width() > 950 ) && ( $itemContainer.width() < 1254 )) {
            $itemContainer.find("> .product-item-block").addClass("in-5");
        } else {
            $itemContainer.find("> .product-item-block").removeClass("in-5");
        };
        if(( $itemContainer.width() > 750 ) && ( $itemContainer.width() <= 950 )) {
            $itemContainer.find("> .product-item-block").addClass("in-4");
        } else {
            $itemContainer.find("> .product-item-block").removeClass("in-4");
        };
        if(( $itemContainer.width() > 550 ) && ( $itemContainer.width() <= 750 )) {
            $itemContainer.find("> .product-item-block").addClass("in-3");
        } else {
            $itemContainer.find("> .product-item-block").removeClass("in-3");
        };
        if(( $itemContainer.width() <= 550 )) {
            $itemContainer.find("> .product-item-block").addClass("in-2");
        } else {
            $itemContainer.find("> .product-item-block").removeClass("in-2");
        };
    }

    $("#cHome #blkPage").setHomeItemWidth();

    $(window).resize(function(){
        $("#cHome #blkPage").setHomeItemWidth();
    });

    /* Modal dialog close */
    //$(".closeDialog-button").click(function()
    $(document).on('click', '.closeDialog-button', function() {
        $(".dialog-container").fadeOut(100);
        $("body").bodyUnlock();
    });

    /* Photo dialog related */
    //$(".dialog-productPhoto-expand").on('click', function()
    $(document).on('click', 'span.dialog-productPhoto-expand', function() {
        $(this).closest(".dialog-productPhoto-content").toggleClass("expand");
    });

    //$(".dialog-productPhoto .closeDialog-button").on('click', function()
    $(document).on('click', 'span.dialog-productPhoto div.closeDialog-button', function() {
        $(this).siblings(".dialog-productPhoto-content").removeClass("expand");
    });

    /*=============================*/
    /* Below about .tabScrollable */

    /* Fill photo in .product-item-thumbnail and keep scale */
    $('.tabScrollable .tabScrollable-thumbnail .product-item-thumbnail > img').each(function(_, img) {

        var $this = $(this);

        if( $this.height() / $this.width() > 1 ) {
            $this.addClass("portrait");
        }
    });

    var SETTINGS = {
        navBarTravelling: false,
        navBarTravelDirection: "",
        navBarTravelDistance: 150
    }

    document.documentElement.classList.remove("no-js");
    document.documentElement.classList.add("js");

    // Out buttons
    var tabScrollableLeftBtn = document.getElementById("tabScrollableLeftBtn");
    var tabScrollableRightBtn = document.getElementById("tabScrollableRightBtn");

    var tabScrollable = document.getElementById("tabScrollable");
    var tabScrollableContents = document.getElementById("tabScrollableContents");

    tabScrollable.setAttribute("data-overflowing", determineOverflow(tabScrollableContents, tabScrollable));

    // Handle the scroll of the horizontal container
    var last_known_scroll_position = 0;
    var ticking = false;

    function doSomething(scroll_pos) {
        tabScrollable.setAttribute("data-overflowing", determineOverflow(tabScrollableContents, tabScrollable));
    }

    tabScrollable.addEventListener("scroll", function() {
        last_known_scroll_position = window.scrollY;
        if (!ticking) {
            window.requestAnimationFrame(function() {
                doSomething(last_known_scroll_position);
                ticking = false;
            });
        }
        ticking = true;
    });


    tabScrollableLeftBtn.addEventListener("click", function() {
        // If in the middle of a move return
        if (SETTINGS.navBarTravelling === true) {
            return;
        }
        // If we have content overflowing both sides or on the left
        if (determineOverflow(tabScrollableContents, tabScrollable) === "left" || determineOverflow(tabScrollableContents, tabScrollable) === "both") {
            // Find how far this panel has been scrolled
            var availableScrollLeft = tabScrollable.scrollLeft;
            // If the space available is less than two lots of our desired distance, just move the whole amount
            // otherwise, move by the amount in the settings
            if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
                tabScrollableContents.style.transform = "translateX(" + availableScrollLeft + "px)";
            } else {
                tabScrollableContents.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
            }
            // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
            tabScrollableContents.classList.remove("tabScrollableContents-no-transition");
            // Update our settings
            SETTINGS.navBarTravelDirection = "left";
            SETTINGS.navBarTravelling = true;
        }
        // Now update the attribute in the DOM
        tabScrollable.setAttribute("data-overflowing", determineOverflow(tabScrollableContents, tabScrollable));
    });

    tabScrollableRightBtn.addEventListener("click", function() {
        // If in the middle of a move return
        if (SETTINGS.navBarTravelling === true) {
            return;
        }
        // If we have content overflowing both sides or on the right
        if (determineOverflow(tabScrollableContents, tabScrollable) === "right" || determineOverflow(tabScrollableContents, tabScrollable) === "both") {
            // Get the right edge of the container and content
            var navBarRightEdge = tabScrollableContents.getBoundingClientRect().right;
            var navBarScrollerRightEdge = tabScrollable.getBoundingClientRect().right;
            // Now we know how much space we have available to scroll
            var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
            // If the space available is less than two lots of our desired distance, just move the whole amount
            // otherwise, move by the amount in the settings
            if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
                tabScrollableContents.style.transform = "translateX(-" + availableScrollRight + "px)";
            } else {
                tabScrollableContents.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
            }
            // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
            tabScrollableContents.classList.remove("tabScrollableContents-no-transition");
            // Update our settings
            SETTINGS.navBarTravelDirection = "right";
            SETTINGS.navBarTravelling = true;
        }
        // Now update the attribute in the DOM
        tabScrollable.setAttribute("data-overflowing", determineOverflow(tabScrollableContents, tabScrollable));
    });

    tabScrollableContents.addEventListener(
        "transitionend",
        function() {
            // get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
            var styleOfTransform = window.getComputedStyle(tabScrollableContents, null);
            var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
            // If there is no transition we want to default to 0 and not null
            var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
            tabScrollableContents.style.transform = "none";
            tabScrollableContents.classList.add("tabScrollableContents-no-transition");
            // Now lets set the scroll position
            if (SETTINGS.navBarTravelDirection === "left") {
                tabScrollable.scrollLeft = tabScrollable.scrollLeft - amount;
            } else {
                tabScrollable.scrollLeft = tabScrollable.scrollLeft + amount;
            }
            SETTINGS.navBarTravelling = false;
        },
        false
    );

    $('.tabScrollable-link').each(function()
    {
        $(this).click(function(){
            $(this).attr("aria-selected", true);
            $(this).siblings(".tabScrollable-link").attr("aria-selected", "false");
        });
    });

    function determineOverflow(content, container) {
        var containerMetrics = container.getBoundingClientRect();
        var containerMetricsRight = Math.floor(containerMetrics.right);
        var containerMetricsLeft = Math.floor(containerMetrics.left);
        var contentMetrics = content.getBoundingClientRect();
        var contentMetricsRight = Math.floor(contentMetrics.right);
        var contentMetricsLeft = Math.floor(contentMetrics.left);
        if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
            return "both";
        } else if (contentMetricsLeft < containerMetricsLeft) {
            return "left";
        } else if (contentMetricsRight > containerMetricsRight) {
            return "right";
        } else {
            return "none";
        }
    }

    (function (root, factory) {
        if (typeof define === 'function' && define.amd) {
            define(['exports'], factory);
        } else if (typeof exports !== 'undefined') {
            factory(exports);
        } else {
            factory((root.dragscroll = {}));
        }
    }(this, function (exports) {
        var _window = window;
        var _document = document;
        var mousemove = 'mousemove';
        var mouseup = 'mouseup';
        var mousedown = 'mousedown';
        var EventListener = 'EventListener';
        var addEventListener = 'add'+EventListener;
        var removeEventListener = 'remove'+EventListener;
        var newScrollX, newScrollY;

        var dragged = [];
        var reset = function(i, el) {
            for (i = 0; i < dragged.length;) {
                el = dragged[i++];
                el = el.container || el;
                el[removeEventListener](mousedown, el.md, 0);
                _window[removeEventListener](mouseup, el.mu, 0);
                _window[removeEventListener](mousemove, el.mm, 0);
            }

            // cloning into array since HTMLCollection is updated dynamically
            dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
            for (i = 0; i < dragged.length;) {
                (function(el, lastClientX, lastClientY, pushed, scroller, cont){
                    (cont = el.container || el)[addEventListener](
                        mousedown,
                        cont.md = function(e) {
                            if (!el.hasAttribute('nochilddrag') ||
                                _document.elementFromPoint(
                                    e.pageX, e.pageY
                                ) == cont
                            ) {
                                pushed = 1;
                                lastClientX = e.clientX;
                                lastClientY = e.clientY;

                                e.preventDefault();
                            }
                        }, 0
                    );

                    _window[addEventListener](
                        mouseup, cont.mu = function() {pushed = 0;}, 0
                    );

                    _window[addEventListener](
                        mousemove,
                        cont.mm = function(e) {
                            if (pushed) {
                                (scroller = el.scroller||el).scrollLeft -=
                                    newScrollX = (- lastClientX + (lastClientX=e.clientX));
                                scroller.scrollTop -=
                                    newScrollY = (- lastClientY + (lastClientY=e.clientY));
                                if (el == _document.body) {
                                    (scroller = _document.documentElement).scrollLeft -= newScrollX;
                                    scroller.scrollTop -= newScrollY;
                                }
                            }
                        }, 0
                    );
                })(dragged[i++]);
            }
        }

        if (_document.readyState == 'complete') {
            reset();
        } else {
            _window[addEventListener]('load', reset, 0);
        }

        exports.reset = reset;
    }));

    /*=============================*/


    /* Drag scroll */
    (function (root, factory) {
        if (typeof define === 'function' && define.amd) {
            define(['exports'], factory);
        } else if (typeof exports !== 'undefined') {
            factory(exports);
        } else {
            factory((root.dragscroll = {}));
        }
    }(this, function (exports) {
        var _window = window;
        var _document = document;
        var mousemove = 'mousemove';
        var mouseup = 'mouseup';
        var mousedown = 'mousedown';
        var EventListener = 'EventListener';
        var addEventListener = 'add'+EventListener;
        var removeEventListener = 'remove'+EventListener;
        var newScrollX, newScrollY;

        var dragged = [];
        var reset = function(i, el) {
            for (i = 0; i < dragged.length;) {
                el = dragged[i++];
                el = el.container || el;
                el[removeEventListener](mousedown, el.md, 0);
                _window[removeEventListener](mouseup, el.mu, 0);
                _window[removeEventListener](mousemove, el.mm, 0);
            }

            // cloning into array since HTMLCollection is updated dynamically
            dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
            for (i = 0; i < dragged.length;) {
                (function(el, lastClientX, lastClientY, pushed, scroller, cont){
                    (cont = el.container || el)[addEventListener](
                        mousedown,
                        cont.md = function(e) {
                            if (!el.hasAttribute('nochilddrag') ||
                                _document.elementFromPoint(
                                    e.pageX, e.pageY
                                ) == cont
                            ) {
                                pushed = 1;
                                lastClientX = e.clientX;
                                lastClientY = e.clientY;

                                e.preventDefault();
                            }
                        }, 0
                    );

                    _window[addEventListener](
                        mouseup, cont.mu = function() {pushed = 0;}, 0
                    );

                    _window[addEventListener](
                        mousemove,
                        cont.mm = function(e) {
                            if (pushed) {
                                (scroller = el.scroller||el).scrollLeft -=
                                    newScrollX = (- lastClientX + (lastClientX=e.clientX));
                                scroller.scrollTop -=
                                    newScrollY = (- lastClientY + (lastClientY=e.clientY));
                                if (el == _document.body) {
                                    (scroller = _document.documentElement).scrollLeft -= newScrollX;
                                    scroller.scrollTop -= newScrollY;
                                }
                            }
                        }, 0
                    );
                })(dragged[i++]);
            }
        }


        if (_document.readyState == 'complete') {
            reset();
        } else {
            _window[addEventListener]('load', reset, 0);
        }

        exports.reset = reset;
    }));

    /*=============================*/



    $(document).on('click', 'a.tabScrollable-link', function(e) {
//    $('a.tabScrollable-link').click(function(e){
        e.preventDefault();

        var categoryID = $(this).attr('data-CategoryID')
        var itemInCart = [];
        $('.cart-item').each(function(){
            itemInCart.push($(this).attr('data-ItemID'));
        });

        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=management.ajax',
            data : {
                'action': 'getCategoryItem',
                'CategoryID': categoryID,
                'ItemInCart': itemInCart
            },
            success: updateCategoryItem,
            error: show_ajax_error
        });
    });

    // add cart function
    $.fn.addCart = function(itemID, existInCart, qty) {
        if ($('div.cart-item').length == 0) {
            $('div#blkBlankCart').fadeOut(300);
        }

        // use async=false to avoid quickly click the same item and add multiple rows of the same item in cart
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=management.ajax',
            async: false,
            data: {
                'action': 'addItemToCart',
                'ItemID': itemID,
                'ExistInCart': existInCart,
                'Qty': qty
            },
            success: function(ajaxReturn) {
                updateCartList(ajaxReturn);
            },
            error: show_ajax_error
        });
    }

    // deduct cart function
    $.fn.deductCart = function(itemID, qty) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=management.ajax',
            data: {
                'action': 'deductCartItem',
                'ItemID': itemID,
                'Qty': qty
            },
            success: updateCartList,
            error: show_ajax_error
        });
    }

    // click tray to add the item to cart
    $(document).on('click', '.product-item-block .product-item-addToCart', function(e) {
        e.preventDefault();
        if ($('#StudentID').length == 0 || $('#StudentID').val() == '') {
            alert(_langtxt["epos"]["error"]["PleaseSelectStudent"]);
        }
        else {
            var $this = $(this);
            var $parent = $this.closest(".product-item-block");
            var itemID = $this.attr('data-ItemID');
            var existInCart = false;
            var qty = 0;

            $this.addClass("adding");
            setTimeout(function () {
                $this.removeClass("adding");
            }, 320);
            $parent.addClass("addedToCart");
            $('span#btnClearCart').fadeIn(300);
            $('div#blkCartPrice').fadeIn(500);

            existInCart = $('#CartItemID_' + itemID).length ? true : false;
            if (existInCart) {
                qty = parseInt($('div#CartItemID_' + itemID).find('.cart-item-amount').text());  // qty before this action
            }
            $("#blkTray").addCart(itemID, existInCart, qty);

        }
    });

    // deduct qty by 1
    $(document).on('click', '.cart-item-minus', function(e) {
        e.preventDefault();
        var itemID = $(this).parent().parent().attr('data-ItemID');
        var qty = parseInt($('div#CartItemID_'+itemID).find('.cart-item-amount').text());  // qty before this action
        itemIDToRemove = itemID;
        qtyToRemove = qty;

        if ($(this).hasClass('fa-trash')) {
            $(".dialog-container").fadeIn(150);
            $("body").bodyLock();
            $('span#blkDelItem').show().siblings().hide();
        }
        else {
            $("#blkTray").deductCart(itemIDToRemove, qtyToRemove);
        }
    });

    // increase qty by 1
    $(document).on('click', '.cart-item-plus', function(e) {
        e.preventDefault();
        var itemID = $(this).parent().parent().attr('data-ItemID');
        var qty = parseInt($('div#CartItemID_'+itemID).find('.cart-item-amount').text());  // qty before this action
        var existInCart = 'true';
        $("#blkTray").addCart(itemID, existInCart, qty);
    });

    // cancel shopping cart order
    $(document).on('click', 'span#btnClearCart', function(e) {
        e.preventDefault();
        $(".dialog-container").fadeIn(150);
        $("body").bodyLock();
        $('span#blkClearCart').show().siblings().hide();
    });

    // confirm cancel shopping cart order
    $(document).on('click', 'span#blkClearCart .confirm-button', function(e) {
        var itemID;
        $(".dialog-container").fadeOut(100);
        $("body").bodyUnlock();

        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=management.ajax',
            data : {
                'action': 'cancelCart'
            },
            success: function(ajaxReturn) {
                if (ajaxReturn != null && ajaxReturn.success) {
                    emptyCart();
                }
            },
            error: show_ajax_error
        });
    });

    // confirm delte item from cart
    $(document).on('click', 'span#btnDelItem', function(e) {
        e.preventDefault();
        $(".dialog-container").fadeOut(100);
        $("body").bodyUnlock();
        $("#blkTray").deductCart(itemIDToRemove, qtyToRemove);
    });

    // zoom in item photo
    $(document).on('click', '.product-item-block .product-item-thumbnail', function(e) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=management.ajax',
            data: {
                'action': 'zoomInItemPhoto',
                'ItemID': $(this).find('img').attr('data-ItemID')
            },
            success: showZoomInPhoto,
            error: show_ajax_error
        });
    });

    $(document).on('keypress', '#tbxTopSearch, #tbxItemSearch', function(e) {
        if (e.which == 13 || e.keyCode == 13) {
            e.preventDefault();
        }
    });

    $(document).on('keyup', '#tbxTopSearch, #tbxItemSearch', function(e) {
        var keyword = $.trim($(this).val());
        if(e.keyCode == 13) {
            e.preventDefault();
            if (keyword.length>0) {
                var itemInCart = [];
                $('.cart-item').each(function () {
                    itemInCart.push($(this).attr('data-ItemID'));
                });

                $("#pMain").hide();
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: '?task=management.ajax',
                    data: {
                        'action': 'getSearchResult',
                        'Keyword': keyword,
                        'ItemInCart': itemInCart
                    },
                    success: updateSearchResult,
                    error: show_ajax_error
                });
            }
        }
    });

    $(document).on('click', '#pSearch .page-close-btn', function(e) {
        e.preventDefault();
        $("#pMain").show();
        $("#pSearch").hide();
    });

    $(document).on('click', '#orderDetalCloseBtn', function(e) {
        e.preventDefault();
        $('#orderDetails').hide();
        $("#pMain").show();
        $("#cHome #blkPage").setHomeItemWidth();
        $("#blkTray").show();
    });


    setTimeout(function() {
        $("#cHome").removeClass("opacity0");
    }, 150);

    // show first category items
    $('a.tabScrollable-link:first').click();

    return false;
});     // end document ready function


function updateSearchResult(ajaxReturn)
{
    if (ajaxReturn != null && ajaxReturn.success){
        $('#orderDetails').fadeOut(150);
        $("#blkTopSearch").fadeOut(150).removeClass("popUp-active");
        $('#pSearch').html(ajaxReturn.searchResult).fadeIn(150);
        $('#blkTray').fadeIn(150);
    }
}

function updateCategoryItem(ajaxReturn)
{
    if (ajaxReturn != null && ajaxReturn.success){
        $('#blkPage').html(ajaxReturn.categoryLayout);
        $("#cHome #blkPage").setHomeItemWidth();
    }
}

function updateCartList(ajaxReturn)
{
    if (ajaxReturn != null && ajaxReturn.success){

        if (ajaxReturn.existInCart == 'true') {
            $('div#CartItemID_' + ajaxReturn.itemID).replaceWith(ajaxReturn.cartItem);
        }
        else if (ajaxReturn.removeItemFromCart == 'true') {
            $('div#CartItemID_' + ajaxReturn.itemID).remove();
            $('span.product-item-addToCart[data-ItemID='+ajaxReturn.itemID+']').parent().parent().removeClass('addedToCart');
        }
        else {
            $('#blkCart').append(ajaxReturn.cartItem);
        }
        updateTotalQtyAndAmount();
    }
}

function formatWithCommas(num) {
    var parts = num.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function updateTotalQtyAndAmount()
{
    var qty = 0;
    var amount = 0.0;
    var amountStr, qtyStr;
    $('.cart-item').each(function(){
        qtyStr = $(this).find('.cart-item-amount').text();
        qty += parseInt(qtyStr.replace(',',''));
        amountStr = $(this).find('.cart-item-price').text();
        amount += parseFloat(amountStr.replace(',','').replace('$',''));
    });
    amount = amount.toFixed(2);
//    amountStr = formatWithCommas(amount);
    //amount = (Math.round(amount * 100)/100).toFixed(2);

    $('span#lblNoOfSelectedItems2').html(qty);
    $('#lblPrice2').html('$' + amount.toString());
//    $('#lblPrice2').html('$' + amountStr);
}

function showZoomInPhoto(ajaxReturn)
{
    if (ajaxReturn != null && ajaxReturn.success){
        $(".dialog-productPhoto").replaceWith(ajaxReturn.photoDetail);
        $(".dialog-container").fadeIn(150);
        $("body").bodyLock();
        $(".dialog-productPhoto").show().siblings().hide();
    }
}

function showOrderDetails(ajaxReturn, obj)
{
    if (ajaxReturn != null && ajaxReturn.success){
        $this = obj;
        var id = $this.parent().parent().attr('id');
        if (id.substr(0,3) == 'blk') {
            var len = id.length;
            id = 'btn' + id.substr(3,len-3);
        }
        $('#' + id).click();    // toggle (hide) blkPending or blkHistory
        $('#pMain').fadeOut(150);
        $('#blkTray').fadeOut(150);
        $('#pSearch').fadeOut(150);
        $('div#orderDetails').html(ajaxReturn.orderDetails).fadeIn(250);
    }
}

function emptyCart()
{
    $('div.cart-item').each(function () {
        itemID = $(this).attr('data-ItemID');
        $('span.product-item-addToCart[data-ItemID=' + itemID + ']').parent().parent().removeClass('addedToCart');
        $('div#CartItemID_' + itemID).remove();
    });

    $('span#btnClearCart').fadeOut(300);
    $('span#lblNoOfSelectedItems2').html(0);
    $('#lblPrice2').html('$0.00');
    $('div#blkCartPrice').fadeOut(300);
    $('div#blkBlankCart').fadeIn(600);
}
