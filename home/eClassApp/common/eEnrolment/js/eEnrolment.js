/* Filter function start */
$(function() {
	$('.selectedClub').click(function(){ 
      $(this).toggleClass('active');
	})
});
/* Filter function end */

/* Check check box onclick start */
$(document).ready(function(){
    var $checkboxes = $('.applyClub input[type="checkbox"]');
        
    $checkboxes.change(function(){
        var countClubNum = $checkboxes.filter(':checked').length;
        $('.clubNum').text(countClubNum);
        
        $('.clubNum').val(countClubNum);
		
		if (countClubNum > 0) {
			$('.headerIcon .next').removeClass("disabled" );
			$('.headerIcon .clubNum').show('fast');
		} else {
			$('.headerIcon .next').addClass("disabled" );
			$('.headerIcon .clubNum').hide('fast');
		}
    });
});
/* Check check box onclick end */

/* Process bar animation start */
$(function() {
	$(function () { 
  $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
});  

// $( window ).scroll(function() {   
 // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
  $(".progress-bar").each(function(){
    each_bar_width = $(this).attr('aria-valuenow');
    $(this).width(each_bar_width + '%');
  });
       
 //  }  
// });
});
/* Process bar animation end */

/* Show and hide club information start */
$(function() {
	$("#showMore a").click(function() {
		$("#moreInfo").show('slow');
		$("#moreInfo").focus();
		$('#showMore').hide();
		return false;
	});
	$("#hideInfo a").click(function() {
		$("#showMore").show('slow');
		$("#showMore").focus();
		$('#moreInfo').hide();
		return false;
    });
});
/* Show and hide club information page end */

/* Show all club activities dates and times in applying club information start*/
$(function() {
	$("#showMoreDate a").click(function() {
		$("#moreDate").show('slow');
		$("#moreDate").focus();
		$('#showMoreDate').hide();
		return false;
	});
});
/* Show all club activities dates and times in applying club information end*/