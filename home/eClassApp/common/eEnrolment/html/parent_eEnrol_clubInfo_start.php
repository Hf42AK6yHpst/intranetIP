<?php 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$enrolClubInfo = $libenroll->Get_Club_Info_By_EnrolGroupID($EnrolGroupID);

$currentDate = date('Y-m-d');
$thisClubDisplayName = Get_Lang_Selection($enrolClubInfo['TitleChinese'],$enrolClubInfo['Title']);

$thisClubSemester = $enrolClubInfo['SemesterTitle'];
$thisClubContent = $enrolClubInfo['Description'];
if ($enrolClubInfo['Gender'] == 'F'){
    $thisClubGendar = $Lang['General']['Male'];
}
else if ($enrolClubInfo['Gender'] == 'M'){
    $thisClubGendar = $Lang['General']['Male'];
}
else { 
    $thisClubGendar =$Lang['eEnrolment']['NoLimit'];
}


$thisClubQuato = $enrolClubInfo['Quota']=='0'?$Lang['eEnrolment']['NoLimit']:$enrolClubInfo['Quota'];
$thisClubAgeLimit = $enrolClubInfo['LowerAge']=='0' ? $Lang['eEnrolment']['NoLimit']:$enrolClubInfo['LowerAge'] .' - '.$enrolClubInfo['UpperAge'];


$CategoryID = $enrolClubInfo['GroupCategory'];
$CategoryInfoAry = $libenroll-> GET_CATEGORYINFO($CategoryID);
$CategoryName = $CategoryInfoAry['CategoryName'];

$isAmountTBC = $enrolClubInfo['isAmountTBC'];
$thisClubPaymentAmount = $enrolClubInfo['PaymentAmount'];
$paymentAmountTBC = $isAmountTBC=='1'?'('.$Lang['eEnrolment']['Tentative'].')':'';

//OLE CATEGORY 
$thisClubOLEInfoAry = $libenroll->Get_Enrolment_Default_OLE_Setting('club',array($EnrolGroupID));
$thisClubOLECategoryList = $thisClubOLEInfoAry[0]['OLE_Component'];
$thisClubOLECategoryAry = explode(',',$thisClubOLECategoryList);
$sql = "SELECT ".Get_Lang_Selection("ChiTitle","EngTitle").
        " FROM {$eclass_db}.OLE_ELE 
        WHERE DefaultID IN ('".implode('\',\'',$thisClubOLECategoryAry)."')";
$OLECategory = $libenroll->returnVector($sql);
$thisClubOLECategory = implode(',',$OLECategory);


$libenrolllass = new libclass();
$ClassLvlArr = $libenrolllass->getLevelArray();
$GroupArr = $libenroll->GET_GROUPCLASSLEVEL($EnrolGroupID);
$GroupYearID = Get_Array_By_Key($GroupArr,"ClassLevelID");

for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
    for ($j = 0; $j < sizeof($GroupArr); $j++) {     
        if (in_array($ClassLvlArr[$i][0],$GroupYearID)) {
           $className[] =  $ClassLvlArr[$i]['LevelName'];
            break;
        }
    }
}
$dispPlayClassName = implode(',',$className);


$StudentAttendanceInfoAry = $libenroll->Get_Student_Club_Attendance_Info(array($StudentID),array($EnrolGroupID));
$thisStudentAttendanceInfo = $StudentAttendanceInfoAry[$StudentID][$EnrolGroupID];
$AttendanceRate = $thisStudentAttendanceInfo['AttendancePercentageRounded'];
$AttendanceRate= intval($AttendanceRate);

$AttendanceHours= $thisStudentAttendanceInfo['Hours'];


//student attendance status
$Present= 0;
$Exempt = 0;
$Absent = 0;
$Late = 0;
foreach($thisStudentAttendanceInfo as $GroupDateID => $MettingDateInfo){
    if(is_int($GroupDateID)){

        if($MettingDateInfo['RecordStatus'] == '1'){
            $Present++;
        }
        else if($MettingDateInfo['RecordStatus'] == '2'){
            $Exempt++;
        }
        else if($MettingDateInfo['RecordStatus'] == '3'){       
            $Absent++;
        }
        else if($MettingDateInfo['RecordStatus'] == '4'){
            $Late++;
        }
        
    }
}
//get student performace and comment 
$ParDataArr= array();
$ParDataArr['StudentID'] = $StudentID;
$ParDataArr['EnrolGroupID'] = $EnrolGroupID;
$_thisStudentComment = $libenroll->GET_GROUP_STUDENT_COMMENT($ParDataArr);
$_thisStudentPerformance = $libenroll->GET_GROUP_STUDENT_PERFORMANCE($ParDataArr);


$MettingDateBefore = $libenroll->getEnrolGroupDateByConditions($EnrolGroupID,'1');
$MettingDateAfter = $libenroll->getEnrolGroupDateByConditions($EnrolGroupID,'');
$MettingDateBeforeCount = count($MettingDateBefore);
$MettingDateAfterCount = count($MettingDateAfter);
$_thisClubMeetingDateCount = $MettingDateBeforeCount + $MettingDateAfterCount;

$displayDateCount = 0;
if($_thisClubMeetingDateCount != 0){
    $displayDateCount = ($MettingDateBeforeCount* 100)/ $_thisClubMeetingDateCount;
}


$_nextMeetingDate = date("Y-m-d H:i", strtotime($MettingDateAfter[0]['ActivityDateStart'])).' - '.date("H:i", strtotime($MettingDateAfter[0]['ActivityDateEnd']));
$_nextMeetingMonthday =  date("d", strtotime($MettingDateAfter[0]['ActivityDateStart']));
// $currentMonthDay = date("d");

$now = date("Y-m-d");
$StartDate = date("Y-m-d", strtotime($MettingDateAfter[0]['ActivityDateStart']));

$_dayDifferent =  round(abs(strtotime($StartDate)-strtotime($now))/86400);
// $_dayDifferent = $_nextMeetingMonthday- $currentMonthDay;
$_dayDifferentDisplay = $_dayDifferent== '0' || $_dayDifferent == '1' || $_dayDifferent=='2'? $Lang['eEnrolment']['AfterDayDisplay'][$_dayDifferent]:$_dayDifferent.' '.$Lang['eEnrolment']['AfterDay'];




$_nextMeetingWeekday =  date("N", strtotime($MettingDateAfter[0]['ActivityDateStart']));
$_nextMeetingWeekday = $Lang['eEnrolment']['WeekDay'][$_nextMeetingWeekday];

// for ($i=0;$i<count($MettingDateBefore);$i++){
//     $Month =  date("n", strtotime($MettingDateBefore[$i]['ActivityDateStart']));
//     $MeetingWeekday =  date("N", strtotime($MettingDateBefore[$i]['ActivityDateStart']));
//     $MeetingWeekday = $Lang['eEnrolment']['Weekday'][$MeetingWeekday];
//     $MeetingDate = date("Y-m-d", strtotime($MettingDateBefore[$i]['ActivityDateStart'])).' ('.$MeetingWeekday.') '.date("H:i", strtotime($MettingDateBefore[$i]['ActivityDateStart'])).' - '.date("H:i", strtotime($MettingDateBefore[$i]['ActivityDateEnd']));
//     $OneMonthRecord[] = $MeetingDate;  
// }

for ($i=0;$i<count($MettingDateAfter);$i++){
    $Month =  date("n", strtotime($MettingDateAfter[$i]['ActivityDateStart']));
    $MeetingWeekday =  date("N", strtotime($MettingDateAfter[$i]['ActivityDateStart']));
    
    $MeetingWeekday = $Lang['eEnrolment']['ShortWeekday'][$MeetingWeekday];
    $MeetingDate = date("Y-m-d", strtotime($MettingDateAfter[$i]['ActivityDateStart'])).' ('.$MeetingWeekday. ') '.date("H:i", strtotime($MettingDateAfter[$i]['ActivityDateStart'])).' - '.date("H:i", strtotime($MettingDateAfter[$i]['ActivityDateEnd']));
    $MeetingWeekdayAry[] = $MeetingWeekday;
    $MeetingTimeAry[] = date("H:i", strtotime($MettingDateAfter[$i]['ActivityDateStart'])).' - '.date("H:i", strtotime($MettingDateAfter[$i]['ActivityDateEnd']));
    
    $ClubAfterMeetingDate[] =  date("Y-m-d", strtotime($MettingDateAfter[$i]['ActivityDateStart']));
    $OneMonthRecord[] = $MeetingDate;
    
}

// get time crash club
$crashClub = false;

$StudentClub = $libenroll->Get_Student_Club_Info($StudentID);
$numOfStudentClub = count($StudentClub);

for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
    if ( ($libenroll->IS_CLUB_CRASH($StudentClub[$CountCrash]['EnrolGroupID'], $EnrolGroupID))) {
        $crashClub = true;
        $crash_EnrolGroupID_ary[] = $StudentClub[$CountCrash]['EnrolGroupID'];
        continue;
    }
}

$GroupDateArr1 = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);
$numOfGroupDate1 = count($GroupDateArr1);
$TimeCrashTable = array();
for ($i=0; $i<count($crash_EnrolGroupID_ary); $i++)
{
    $thisEnrolGroupID = $crash_EnrolGroupID_ary[$i];
    if($thisEnrolGroupID != $EnrolGroupID){  
        $thisGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
        $ClubInfo = $libenroll->getGroupInfo($thisGroupID);
        $TimeCrashTable[$i]['Title'] = Get_Lang_Selection($ClubInfo[0]['TitleChinese'],$ClubInfo[0]['Title']);
       
        //get clash dates  
        $GroupDateArr2 = $libenroll->GET_ENROL_GROUP_DATE($thisEnrolGroupID);   
        $numOfGroupDate2 = count($GroupDateArr2);
        for ($j = 0; $j < $numOfGroupDate2; $j++)
        {
            for ($k = 0; $k < $numOfGroupDate1; $k++)
            {
                $StartDate1 = date("Y-m-d", strtotime($GroupDateArr1[$k]['ActivityDateStart']));
                $StartDate2 = date("Y-m-d", strtotime($GroupDateArr2[$j]['ActivityDateStart']));
                $EndDate1 = date("Y-m-d", strtotime($GroupDateArr1[$k]['ActivityDateEnd']));
                $EndDate2 = date("Y-m-d", strtotime($GroupDateArr2[$j]['ActivityDateEnd']));             
                /*
                 *	assumption:
                 *	1. activity will not across two days
                 *	2. time is 24-hour based
                 *	3. time range: 0000-2359
                 */
                if ($StartDate1 == $StartDate2 && $EndDate1 == $EndDate2)
                {
                    $StartTime1 = date("Hi", strtotime($GroupDateArr1[$k]['ActivityDateStart']));
                    $StartTime2 = date("Hi", strtotime($GroupDateArr2[$j]['ActivityDateStart']));
                    $EndTime1 = date("Hi", strtotime($GroupDateArr1[$k]['ActivityDateEnd']));
                    $EndTime2 = date("Hi", strtotime($GroupDateArr2[$j]['ActivityDateEnd']));
                    
                    if ($StartTime2 == $StartTime1)
                    {
                        # must be time crashed if both clubs start at the same time
                    //    $TimeCrashTable[$i]['clashTime'][] = date("Y-m-d H:i", strtotime($GroupDateArr2[$j]['ActivityDateStart'])).' - '.date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateEnd']));
                        $TimeCrashTable[$i]['Date'][$j] = $StartDate2;
                        $TimeCrashTable[$i]['OnlyTime'][$j] = date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateStart'])).' - '.date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateEnd']));
                          
                    }
                    else if ($StartTime2 < $StartTime1)
                    {
                        # if club2 starts earlier than club1
                        # time crash if club2 ends later than the start of club1
                        # e.g. 1000-1100 vs 0930-1000 => no time crash
                        # e.g. 1000-1100 vs 0930-1015 => time crash
                        if ($EndTime2 > $StartTime1)
                        {
                     //       $TimeCrashTable[$i]['clashTime'][] = date("Y-m-d H:i", strtotime($GroupDateArr2[$j]['ActivityDateStart'])).' - '.date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateEnd']));
                            $TimeCrashTable[$i]['Date'][$j] = $StartDate2;
                            $TimeCrashTable[$i]['OnlyTime'][$j] = date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateStart'])).' - '.date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateEnd']));
                        }
                    }
                    else if ($StartTime2 > $StartTime1)
                    {
                        # if club2 starts later than club1
                        # time crash if club2 starts before the end of club1
                        # e.g. 1000-1100 vs 1100-1200 => no time crash
                        # e.g. 1000-1100 vs 1045-1115 => time crash
                        if ($StartTime2 < $EndTime1)
                        {
                     //       $TimeCrashTable[$i]['clashTime'][] = date("Y-m-d H:i", strtotime($GroupDateArr2[$j]['ActivityDateStart'])).' - '.date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateEnd']));
                            $TimeCrashTable[$i]['Date'][$j] = $StartDate2;
                            $TimeCrashTable[$i]['OnlyTime'][$j] = date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateStart'])).' - '.date("H:i", strtotime($GroupDateArr2[$j]['ActivityDateEnd']));
                        }
                    }                  
                }
            }             
        }       
    }  
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $Lang['eEnrolment']['Header']['ClubInfo'];?></title>
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../js/offcanvas.js"></script>
<link rel="stylesheet" href="../css/offcanvas.css">
<script type="text/javascript" src="../js/eEnrolment.js"></script>
<link rel="stylesheet" href="../css/eEnrolment.css">
<?php if($isStudentApp){?>
    <link rel="stylesheet" href="../css/eEnrolmentStudent.css">
<? } ?>
<!-- <script type="text/javascript" src="../../js/eEnrol.js"></script> -->
<link rel="stylesheet" href="../css/font-awesome.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
	
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['出席情況', '次數'],
          ['<?php echo $Lang['StudentAttendance']['Present']?>', <?php echo $Present;?>],
          ['<?php echo $Lang['StudentAttendance']['Late'];?>', <?php echo $Late;?>],
          ['<?php echo $Lang['StudentAttendance']['Waived'];?>', <?php echo $Exempt;?>],
          ['<?php echo $Lang['StudentAttendance']['Absent'];?>', <?php echo $Absent;?>]
        ]);

      var options = {
		  colors: ['#69b577', '#69b577', '#69b577', '#f46060' ],
		  height: 176,
		  legend: {position: "none"},
		  pieHole: 0.9,
		  pieSliceBorderColor: "none",
		  pieSliceText: "none",
		  tooltip: {trigger: "none"},
		  width: 161,
		  chartArea:{left:7, top: 15, right: 7, bottom: 15,}
      };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);	  
      }
    </script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <nav id="header" class="navbar navbar-fixed-top enrolment">
    <div id="function">
      <div class="headerIcon"><a href="javascript:history.go(-1)"><i class="fas fa-arrow-left"></i></a></div>
    </div>
    <div class="headerTitle withFunction"><?php echo $Lang['eEnrolment']['Header']['ClubInfo'];?></div>
    <div id="button"></div>
  </nav>
  <div id="filter" class="invisible"></div>
  <div id="content">
    <div class="row marginB10">
      <div class="classDateDetails <?php echo $SemesterClass;?> col-xs-12">
        <div class="pageTitle"> <span><?php echo $thisClubDisplayName;?></span> <span class="tag label"><?php echo $thisClubSemester.' '.$Lang['eEnrolment']['Club'];?></span> </div>
        <?php if($displayDateCount != 0 ){ ?>
            
        
        <div class="progressArea col-xs-12">
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $displayDateCount;?>" aria-valuemin="0" aria-valuemax="100"> <span  class="popOver" data-toggle="tooltip" data-placement="top" title="<?php echo $Lang['eEnrolment']['Finished'].' '.$MettingDateBeforeCount.' '.$Lang['eEnrolment']['Times'];?>"> </span> </div>
            <span class="totalClass"><?php echo $Lang['eEnrolment']['Total'].' '.$_thisClubMeetingDateCount.' '.$Lang['eEnrolment']['Times'];?></span> </div>
        </div>
        <?php }?>
        
        <?php if(!empty($MettingDateAfter)){
        ?>
            
         <div class="classDate"> 
          <span><?php echo $Lang['eEnrolment']['NextMeeting'];?>︰<br class="break" /><?php echo $_dayDifferentDisplay.' ('.$_nextMeetingWeekday.') '.$_nextMeetingDate;?></span>
          <?php 
          foreach ($TimeCrashTable as $TimeCrashInfoAry){
             // for($j=0;$j<count($TimeCrashInfoAry['Date']);$j++){
              foreach ($TimeCrashInfoAry['Date'] as $Count=>$TimeCrashDate){
                  if($ClubAfterMeetingDate[0] == $TimeCrashDate){
                      $Clash[0] = '<a data-toggle="modal" data-target="#timeClashDetails0" class="timeClash"><span class="icon"></span></a>';
                  }
              }
          }  
          echo $Clash[0];
          ?>
          <a data-toggle="collapse" href="#showDate" role="button" aria-expanded="false" aria-controls="collapseExample" class="more"></a>
          
          <div class="collapse" id="showDate">
            <ol class="dateList" start="<?php echo $MettingDateBeforeCount+2;?>">
            <?php for($i=1;$i<count($OneMonthRecord);$i++){

                $Clash = array();
                    foreach ($TimeCrashTable as $TimeCrashInfoAry){
                        foreach ($TimeCrashInfoAry['Date'] as $Count=>$TimeCrashDate){                          
                            if($ClubAfterMeetingDate[$i] == $TimeCrashDate){ 
                                $Clash[$i] = '<a data-toggle="modal" data-target="#timeClashDetails'.$i.'" class="timeClash"><span class="icon"></span></a>';                                                       
                            }    
                        }                      
                    }  
               ?>          
              <li><?php echo $OneMonthRecord[$i].$Clash[$i];?> 
              </li>
            <? }?>
            </ol>
          </div>
        </div>
        <? } ?>        
      </div>
    </div>
	<div class="row">
      <div class="subTitle attendance col-xs-12">
        <div class="title"><?php echo  $Lang['eEnrolment']['attendance_percent'];?> <span class="small">(<?php echo $Lang['eEnrolment']['Deadline'].' '.$currentDate;?>)</span></div>      
        <?php if($displayDateCount != 0){ ?> 
        	<a href="parent_eEnrol_attendanceDetails.php?StudentID=<?php echo $StudentID;?>&EnrolGroupID=<?php echo $EnrolGroupID;?>">  
        	<?php echo $Lang['eEnrolment']['Details'];?></a>
        <?php }?>
      </div>
    </div>
	<div class="row marginB10">
      <div class="attendanceDetails col-xs-12">
		  <div class="row">
			  <div class="chart col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-3">
				  <div id="piechart"></div>
			  
			  <div class="chartAttendanceRate">
				  <span class="chartLabel"><?php echo  $Lang['eEnrolment']['attendance_percent'];?></span>
				  <div class="chartRate"><?php echo $AttendanceRate;?><span class="small"> %</span></div>
			  </div>
				  </div>
			  <div class="statistics col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-3">
				  <div class="onTime col-xs-6">
					  <div class="num"><?php echo $Present;?><span class="small"> <?php echo $Lang['eEnrolment']['Times'];?></span></div>
					  <div class="icon small"><?php echo $Lang['StudentAttendance']['Present'];?></div>
				  </div>
				  <div class="late col-xs-6">
					  <div class="num"><?php echo $Late;?><span class="small"> <?php echo $Lang['eEnrolment']['Times'];?></span></div>
					  <div class="icon small"><?php echo $Lang['StudentAttendance']['Late'];?></div>
				  </div>
				  <div class="exempt col-xs-6">
					  <div class="num"><?php echo $Exempt;?><span class="small"> <?php echo $Lang['eEnrolment']['Times'];?></span></div>
					  <div class="icon small"><?php echo $Lang['StudentAttendance']['Waived'];?></div>
				  </div>
				  <div class="absent col-xs-6">
					  <div class="num"><?php echo $Absent;?><span class="small"> <?php echo $Lang['eEnrolment']['Times'];?></span></div>
					  <div class="icon small"><?php echo $Lang['StudentAttendance']['Absent'];?></div>
				  </div>
				  <div class="totalHour col-xs-12">
					  <span class="small"><?php echo $Lang['eEnrolment']['AttendanceHour'];?>︰</span><?php echo $AttendanceHours;?><span class="small"> <?php echo $Lang['eEnrolment']['Hour'];?></span>
				  </div>
			  </div>
		  </div>
	  </div>
    </div>
	<div class="row">
      <div class="subTitle comment col-xs-12">
        <div class="title"><?php echo $Lang['eEnrolment']['PerformanceandComment'] ;?></div>
      </div>
    </div>
	<div class="row marginB10">
      <div class="commentDetails col-xs-12">
		  <div class="commentLabel col-xs-12"><?php echo $Lang['eEnrolment']['Performance'];?></div>
		  <div class="commentContent col-xs-12">
<!-- 			  <i class="far fa-thumbs-up"></i>  -->
			  <?php echo $_thisStudentPerformance;?>
		  </div>
		  <div class="commentLabel col-xs-12"><?php echo $Lang['eEnrolment']['TeacherComment'];?></div>
		  <div class="commentContent col-xs-12"><?php echo $_thisStudentComment;?></div>
	  </div>
	</div>
    <div class="row">
      <div class="subTitle info col-xs-12">
        <div class="title"><?php echo $Lang['eEnrolment']['Header']['ActivityInfo'];?></div>
      </div>
    </div>
    <div class="row marginB10">
      <div class="classInfoDetails col-xs-12">
		  <div class="infoLabel"><?php echo $eEnrollment['ClubType'];?></div>
		  <div class="infoContent"><?php echo $CategoryName;?></div>
		  <div class="showMore" id="showMore"><a href="#" onClick="showMoreInfo(this)">(<?php echo $Lang['eEnrolment']['ShowMore'];?>)</a></div>
		  <div class="moreInfo" id="moreInfo">
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['ClubContent'];?></div>
			  <div class="infoContent"><?php echo $thisClubContent;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['OLECategory'];?></div>
			  <div class="infoContent"><?php echo $thisClubOLECategory;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['TargetForm'];?></div>
			  <div class="infoContent"><?php echo $dispPlayClassName;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['TargetForm'];?></div>
			  <div class="infoContent"><?php echo $thisClubAgeLimit;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['Gender'];?></div>
			  <div class="infoContent"><?php echo $thisClubGendar;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['Quato'];?></div>
			  <div class="infoContent"><?php echo $thisClubQuato;?></div>
			  <div class="hideInfo" id="hideInfo"><a href="#" onClick="hideInfo(this)">(<?php echo $Lang['Btn']['Hide'];?>)</a></div>
		  </div>
	  </div>
    </div>
	<div class="row">
      <div class="subTitle fee col-xs-12">
        <div class="title"><?php echo $Lang['eEnrolment']['Fee'];?></div>
      </div>
    </div>
  <div class="row marginB10">
      <div class="feeDetails col-xs-12">
		  <div class="feeContent"><?php echo $thisClubPaymentAmount;?> <?php echo $paymentAmountTBC;?></div>
	  </div>
    </div>
  </div>
</div>

<?php 
for($i=0;$i<count($OneMonthRecord);$i++){    ?>
   <div class="modal fade" id="timeClashDetails<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="timeClashLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="timeClashlLabel"><?php echo $ClubAfterMeetingDate[$i].' ('.$MeetingWeekdayAry[$i].')';?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">
         <div class="icon"></div>
        </span> </button>
      </div>
       <div class="modal-body"> <span><?php echo $thisClubDisplayName;?><br />
               <?php echo str_replace("[__count__]",  $MettingDateBeforeCount+1+$i,$Lang['eEnrolment']['MeetingCount']);?>　<?php echo $MeetingTimeAry[$i];?><br />
        </span> </div>
        <?php 
        foreach ($TimeCrashTable as $TimeCrashInfoAry){
            $ClashTimeAry = $TimeCrashInfoAry['OnlyTime'];
           
            $ClashClubDateAry = $TimeCrashInfoAry['Date'];  
           // $ClashMeetingTimeAry = $TimeCrashInfoAry
            $ClashClubTitle = '';
           
            foreach ($ClashClubDateAry as $CurrentCount => $ClashClubDate){
                
                if( $ClashClubDate == $ClubAfterMeetingDate[$i]){
                    $ClashClubTitle = $TimeCrashInfoAry['Title'];
                    $MeetingBeforeCount = $CurrentCount;
                    $ClashMeetingTime = $ClashTimeAry[$CurrentCount];
                }
            }
           
            if($ClashClubTitle != ''){
              
            ?>
              <div class="modal-body"> <span><?php echo $ClashClubTitle;?><br />
                	<?php echo str_replace("[__count__]",  $MeetingBeforeCount+1,$Lang['eEnrolment']['MeetingCount']).$ClashMeetingTime;?><br />
             </span> </div>
            
        <?php  }
        } ?>



    </div>
  </div>
</div>
    
    
<?php } ?>

</body>
</html>
