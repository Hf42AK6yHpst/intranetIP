<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();


$clubInfoAry = $libenroll->Get_All_Club_Info();
$clubInfoAssoAry = BuildMultiKeyAssoc($clubInfoAry, array('Semester','EnrolGroupID'));

$clubInfoEnrolGroupIDAssoAry = BuildMultiKeyAssoc($clubInfoAry, 'EnrolGroupID');

foreach($clubInfoAssoAry as $Semester=>$canApplyclubInfoAry){
    $Semester = $Semester==''||$Semester=='null'?$Lang['General']['WholeYear']:$Semester;
    $thisClubSemesterAry[] = $Semester;
}

// $i=0;
// foreach($thisClubSemesterAry as $thisClubSemester){
//     $DisplaySemester[$thisClubSemester] ='term'.$i;
//     $i++;
// }

$LibUser = new libuser($_SESSION['UserID']);
$studentEnrolledListAry = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($StudentID);

// foreach ($studentEnrolledListAry as $studentEnrolledList){
//     $_thisEnrolGroupID = $studentEnrolledList['EnrolGroupID'];   
    
//     $thisEnrolClubInfo = $libenroll->Get_Club_Info_By_EnrolGroupID($_thisEnrolGroupID);
//     $thisClubSemester = $thisEnrolClubInfo['SemesterTitle'];
//     $thisClubSemesterAry[] = $thisClubSemester;
   
// }
// $thisClubSemesterAry = array_unique($thisClubSemesterAry);

// $i=0;
// foreach ($thisClubSemesterAry as $thisClubSemesterTitleAry){
//     $DisplaySemester[$thisClubSemesterTitleAry] ='term'.$i;
//     $i++;
// }


$AnnounceEnrolmentResultDate = $libenroll->AnnounceEnrolmentResultDate;
$StartTime = $libenroll->AppStart." ".$libenroll->AppStartHour.":".$libenroll->AppStartMin;
$EndTime= $libenroll->AppEnd." ".$libenroll->AppEndHour.":".$libenroll->AppEndMin;
$inApplicationPeriod = (strtotime(date("Y-m-d H:i")) > strtotime($StartTime) && strtotime(date("Y-m-d H:i")) < strtotime($EndTime));
$ApplicantType = $libenroll->enrollPersontype;

$CorrectApplicantType = false;
if ($libenroll->enrollPersontype == '0' && $LibUser->IsStudent() && $isStudentApp) {
    $CorrectApplicantType = true;
}
else if ($libenroll->enrollPersontype == '1' && $LibUser->IsParent()) {
    $CorrectApplicantType= true;
}

$StudentAppliedClubInfoAry = $libenroll->Get_Student_Applied_Club_Info($StudentID, '', '', '', '', true);

$AppliedClubStatusAry = Get_Array_By_Key($StudentAppliedClubInfoAry, 'RecordStatus');
$AppliedClubInfoAssoAry = BuildMultiKeyAssoc($StudentAppliedClubInfoAry,array('RecordStatus','EnrolGroupID'));
$AppliedClubEnrolGroupIDsAry = Get_Array_By_Key($StudentAppliedClubInfoAry, 'EnrolGroupID');
// strtotime(date("Y-m-d")) > strtotime($StartTime)
$PastAnnounceDate = strtotime(date("Y-m-d"))>strtotime($AnnounceEnrolmentResultDate);

if ($AnnounceEnrolmentResultDate == '') {
    $PastAnnounceDate = 0;
}

$withinProcessStage = $libenroll->WITHIN_ENROLMENT_PROCESS_STAGE();
$withinEnrolStage = $libenroll->WITHIN_ENROLMENT_STAGE("club");

// $ShowStartApplyDiv = false;
if($inApplicationPeriod){ 
    if(empty($StudentAppliedClubInfoAry)){
        $ShowStartApplyDiv = true;
    }else{       
        $ShowApplicationResultandButton = true;
    }   
}else{
    if(strtotime(date("Y-m-d")) == strtotime($AnnounceEnrolmentResultDate)){
        
        if(in_array('1',$AppliedClubStatusAry) || in_array('2',$AppliedClubStatusAry)){
            $ShowResult = true;
        }
    }
    
}


$AcademicYearID = Get_Current_Academic_Year_ID();
$libAcademicYear = new academic_year($AcademicYearID);
$termNameAry = $libAcademicYear->Get_Term_List(1);
$termAry[0]['YearTermID'] = 0;
$termAry[0]['YearTermName'] = $Lang['General']['WholeYear'];
$i=0;
$DisplaySemester[''] ='term'.$i;
foreach($termNameAry as $TermID => $TermName){
    $i++;
    $termAry[$i]['YearTermID'] = $i;
    $termAry[$i]['YearTermName'] = $TermName;
    $DisplaySemester[$TermName] ='term'.$i;

}

// check max can enrolled successfully 
// $quotaSettingsAry = $libenroll->Get_Application_Quota_Settings($enrolConfigAry['Club']);

$quotaSettingsAry = $libenroll->Get_Application_Quota_Settings($enrolConfigAry['Club']);
$quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('CategoryID','SettingName','TermNumber'));
$quotaSettingsAssoAry = $quotaSettingsAssoAry[0];
// $quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('TermNumber', 'CategoryID','SettingName'));

// $quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('SettingName', 'TermNumber'), $IncludedDBField=array('SettingValue'), $SingleValue=1);

$quotaTypeAry = $libenroll->Get_Application_Quota_Settings_Type();
$numOfQuotaType = count($quotaTypeAry);
$minQuatoClubAry = array();
$quotaSettingsType = $libenroll->quotaSettingsType;
for ($i=0; $i<$numOfQuotaType; $i++) {
    $_quotaType = $quotaTypeAry[$i];
  
  
    if ($_quotaType == 'ApplyMin') {
        $_title = $Lang['eEnrolment']['MinStudentApply'];
                
        $numOfTerm = count($termAry);
               
        for ($j=0; $j<$numOfTerm; $j++) {
            $_yearTermId = $termAry[$j]['YearTermID'];
            $_yearTermName = $termAry[$j]['YearTermName'];
            
            $_yearTermNum = $_yearTermId;
            
            if ($_yearTermId > 0 && count($includeYearTermNumAry) > 0 && !in_array($_yearTermNum, $includeYearTermNumAry)) {
                continue;
            }
            
            if ($_yearTermId == 0) {
                $_termName = $Lang['eEnrolment']['WholeYearClub'];
                $_termNum = 0;
            }
            else{
                $_termName = $_yearTermName.' '.$Lang['eEnrolment']['Club'];
                $_termNum = $j;
            }           
            $_quotaTypeValue = $quotaSettingsAssoAry[$_quotaType][$_termNum]['SettingValue'];
       
            $minQuatoClubAry[] = $_termName .' : '.$_quotaTypeValue;
        }
    }
    
    if ($_quotaType == 'EnrollMax') {
        $_title = $Lang['eEnrolment']['MinStudentApply'];
        
        
        $numOfTerm = count($termAry);
        
        
        for ($j=0; $j<$numOfTerm; $j++) {
            $_yearTermId = $termAry[$j]['YearTermID'];
            $_yearTermName = $termAry[$j]['YearTermName'];
            $_yearTermNum = $_yearTermId;
            
            if ($_yearTermId > 0 && count($thisClubSemesterAry) > 0 && !in_array($_yearTermName, $thisClubSemesterAry)) {
                continue;
            }
            
            if ($_yearTermId == 0) {
                $_termName = $Lang['eEnrolment']['WholeYear'];
                $_termNum = 0;
            }
            else  if($quotaSettingsType == 'TermBase'){
                $_termName = $_yearTermName;
                $_termNum = $j;
            }
                       
            $_quotaTypeValueMax = $quotaSettingsAssoAry[$_quotaType][$_termNum]['SettingValue'];
          
            $maxcEnrolledClubAry[$_termName] = $_quotaTypeValueMax;
        }
    }  
}
$displayMinQuatoClub = implode('<br>',$minQuatoClubAry);

if($FinishApplication){
    $sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT
    WHERE StudentID = '$StudentID'";
    $libenroll->db_db_query($sql);
    
    
    $EnrolGroupIDAry = explode(',',$EnrolGroupIDList);
    $TimeClashGroupIDAry = explode(',',$TimeClashGroupID);
    

    $clubNo = count($EnrolGroupIDAry);
    
    for($i=0;$i<$clubNo;$i++){
        foreach($clubInfoEnrolGroupIDAssoAry as $_enrolGroupID=>$_enrolGroupInfoAry){
            if($EnrolGroupIDAry[$i] == $_enrolGroupID){
                $canApplyclubInfoAry2[$_enrolGroupID] = $_enrolGroupInfoAry;
            }
        }
    }
    
    $descriptionText = ($libenroll->mode == 0 || trim($libenroll->Description)=='')? $Lang['General']['EmptySymbol'] : nl2br($libenroll->Description);
    $tieBreakRuleText = ($libenroll->tiebreak == 1)? $i_ClubsEnrollment_AppTime : $i_ClubsEnrollment_Random;
   
    
    $displayButton = strtotime(date('Y-m-d')) < strtotime($EndTime)? true:false;
    
    if($WishClubCount != '' && !$libenroll->disableCheckingNoOfClubStuWantToJoin){
        $ParInfo['StudentID'] = $StudentID;
        $ParInfo['maxwant'] = $WishClubCount;
        $success = $libenroll->Update_Enrol_Student_Max_Club($ParInfo);
    }
    
}

// get student in participating and finished club info
foreach ($studentEnrolledListAry as $studentEnrolledList){
    $_thisEnrolGroupID = $studentEnrolledList['EnrolGroupID'];

    $thisEnrolClubInfo = $libenroll->Get_Club_Info_By_EnrolGroupID($_thisEnrolGroupID);
    $thisClubSemester = $thisEnrolClubInfo['SemesterTitle'];

    $SemesterClass = $DisplaySemester[$thisClubSemester];
    $_thisDisplayTitle = Get_Lang_Selection($studentEnrolledList['TitleChinese'],$studentEnrolledList['1']);
    
    // $thisClubSemester = $thisClubSemester=='全年'||strtolower($thisClubSemester)=='whole year'?'term0':strtolower($thisClubSemester);
    $MettingDateBefore = $libenroll->getEnrolGroupDateByConditions($_thisEnrolGroupID,'1');
    $MettingDateAfter = $libenroll->getEnrolGroupDateByConditions($_thisEnrolGroupID,'');
    
    
    
    $StudentAttendanceInfoAry = $libenroll->Get_Student_Club_Attendance_Info(array($StudentID),array($_thisEnrolGroupID));
    $_thisStudentAttendanceInfoAry = $StudentAttendanceInfoAry[$StudentID][$_thisEnrolGroupID];
    $_thisStudentAttendanceRate = $_thisStudentAttendanceInfoAry['AttendancePercentageRounded'];
    $_thisStudentAttendanceHours = $_thisStudentAttendanceInfoAry['Hours'];
    
    
    if(empty($MettingDateAfter)){
        $thisfinishedClub[] = $_thisEnrolGroupID;
        $thisfinishedClubSemester[] = $thisClubSemester;
        $thisfinishedClubSemesterClass[] = $SemesterClass;
        $thisfinishedClubTitle[] = $_thisDisplayTitle;
        $thisfinishedClubDateCount[] = count($MettingDateBefore);
        $thisfinishedClubAttendanceRate[] = $_thisStudentAttendanceRate;
        $thisfinishedClubAttendanceHours[] = $_thisStudentAttendanceHours;
    }else{
        $InParticipatingClubID[] = $_thisEnrolGroupID;
        $InParticipatingSemester[] = $thisClubSemester;
        $InParticipatingClubTitle[] = $_thisDisplayTitle;
        
        $MettingDateBeforeCount = count($MettingDateBefore);
        $MettingDateAfterCount = count($MettingDateAfter);
        $_thisClubMeetingDateCount = $MettingDateBeforeCount + $MettingDateAfterCount;    
        $displayDateCount = ($MettingDateBeforeCount* 100)/ $_thisClubMeetingDateCount;
     
        $InParticipatingMettingDateBeforeCount[] = $MettingDateBeforeCount;
        $InParticipatingClubMeetingCount[] = $_thisClubMeetingDateCount;
        $InParticipatingClubAttendanceHours[] = $_thisStudentAttendanceHours;
        $InParticipatingClubAttendanceRate[] = $_thisStudentAttendanceRate;       
        $InParticipatingDisplayDateCount[] = $displayDateCount;
        $InParticipatingSemesterClass[] = $SemesterClass;
        
        $_nextMeetingDate = date("H:i", strtotime($MettingDateAfter[0]['ActivityDateStart'])).' - '.date("H:i", strtotime($MettingDateAfter[0]['ActivityDateEnd']));
        $_nextMeetingMonthday =  date("d", strtotime($MettingDateAfter[0]['ActivityDateStart']));
        //                 $currentMonthDay = date("d");
       
        
        $now = date("Y-m-d");
        $StartDate = date("Y-m-d", strtotime($MettingDateAfter[0]['ActivityDateStart']));
        
        $_dayDifferent =  round(abs(strtotime($StartDate)-strtotime($now))/86400);
        //                 $_dayDifferent = $_nextMeetingMonthday- $currentMonthDay;
        $_dayDifferentDisplay = $_dayDifferent== '0' || $_dayDifferent == '1' || $_dayDifferent=='2'? $Lang['eEnrolment']['AfterDayDisplay'][$_dayDifferent]:$_dayDifferent.' '.$Lang['eEnrolment']['AfterDay'];
      
        
        $_nextMeetingWeekday =  date("N", strtotime($MettingDateAfter[0]['ActivityDateStart']));
        $_nextMeetingWeekday = $Lang['eEnrolment']['WeekDay'][$_nextMeetingWeekday];
       
        $InParticipatingNextMeetingDate[] = $_nextMeetingDate;
        $InParticipatingNextMeetingMonthday[] = $_nextMeetingMonthday;
        $InParticipatingDayDifferent[] = $_dayDifferentDisplay;
        $InParticipatingNextMeetingWeekday[] = $_nextMeetingWeekday;
    }
} 


$InParticipatingClubCount = count($InParticipatingClubID);
$FinishedClubCount = count($thisfinishedClub);

// check if enrolled club exceed max limit 

foreach($maxcEnrolledClubAry as $semesterName => $MaxEnrolledCount){
   $count = 0;
   for($i=0;$i<$InParticipatingClubCount;$i++){
       if($InParticipatingSemester[$i] == $semesterName){
            $count++;
       }
   }
   
   for($i=0;$i<$FinishedClubCount;$i++){
       if($thisfinishedClubSemester[$i] == $semesterName){
           $count++;
       }
   }

   if($count < $MaxEnrolledCount || $MaxEnrolledCount==0){
       $canClickEnrol[$semesterName] = true;
   }else{
       $canClickEnrol[$semesterName] = false;
   }    
}
if(in_array(1,$canClickEnrol)){
    $CanEnrolNew = 1;
}else{
    $CanEnrolNew = 0;
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$Lang['eEnrolment']['Club']?></title>
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../js/offcanvas.js"></script>
<link rel="stylesheet" href="../css/offcanvas.css">

 <script type="text/javascript" src="../js/eEnrolment.js"></script>

<?php if($isStudentApp){?>
    <link rel="stylesheet" href="../css/eEnrolmentStudent.css">
<? }else{ ?>
	<link rel="stylesheet" href="../css/eEnrolment.css">
<?php }?>
<link rel="stylesheet" href="../css/font-awesome.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>

<body>
<form name="form1" id="form1" method="post" enctype="multipart/form-data">
<div id="wrapper"> 
  <!-- Header -->
  <nav id="header" class="navbar navbar-fixed-top enrolment">
    <div id="function"></div>
    <div class="headerTitle"><?php echo $Lang['eEnrolment']['Club'];?></div>
    <div id="button"></div>
  </nav>
  <div id="filter" class="invisible"></div>
  <div id="content">
  <?php if($ShowStartApplyDiv  && !$FinishApplication && $CorrectApplicantType){?>
    <div class="row">
      <div class="clubApplication col-xs-12 marginB10">
		  <div class="pageTitle"><span><?php echo $targetSemester.$i_ClubsEnrollment;?></span></div>
		  <div class="applicationPeriod"><span><?php echo $Lang['eEnrolment']['FromTodayTo'].' '.$EndTime;?></span></div>
		  <a id="nextStep" onclick="javascript:StartApplication();" class="btn btn-primary" role="button"><?php echo $Lang['eEnrolment']['ViewApplicationInstructionsAndStartApply2'];?></a>
	  </div>
   </div>
   <?php } ?>
   
     <?php 
     if(!$PastAnnounceDate){      
         if((in_array('0',$AppliedClubStatusAry) && !$FinishApplication) || (!$ShowResult && !$FinishApplication)){?>
      	<div class="row">
          <div class="subTitle pending col-xs-12">
            <div class="title"><?php echo $Lang['eEnrolment']['InApplication'];?> <small>(<?php echo $eEnrollment['waiting'];?>)</small></div>
          </div>
        </div>
    	<div class="row">
    		<div class="clubApplication col-xs-12">
        		<ul class="club pending withOrder col-xs-12">
        		  <?php     
        		  $i = 0;
                  foreach ($AppliedClubInfoAssoAry as $AppliedClubRecordStatus=>$AppliedClubIDInfoAry){
                      foreach($AppliedClubIDInfoAry as $AppliedEnrolGroupID=>$AppliedClubInfoAry){
                          $Title = Get_Lang_Selection($AppliedClubInfoAry['TitleChinese'],$AppliedClubInfoAry['Title']);
                          $Semester = $AppliedClubInfoAry['Semester'];
                        
                          $SemesterDisplay = $Semester==''?$i_ClubsEnrollment_WholeYear:$Semester;
                        
                          if($AppliedClubRecordStatus == '0'){ 
                          $i++;?>
            		
            			<a href="parent_eEnrol_applyClubDetials.php?isStudentApp=<?=$isStudentApp?>&EnrolGroupID=<?php echo $AppliedEnrolGroupID;?>">
            			<li class="<?php echo $DisplaySemester[$Semester];?>">
                			<div class="orderNum"><?php echo $i;?></div> 
                			<div class="clubTitle"> 
                    			<span><?php echo $Title;?></span> 
                    			<span class="tag label"><?php echo $SemesterDisplay;?></span> 
                			</div>
            			</li>
            			</a>
            			<?
                          }
                      }
                  }	?>			
        			<li><div class="infoTxt text-center"><span> <?php echo str_replace("[__date__]", $AnnounceEnrolmentResultDate,$Lang['eEnrolment']['ClubApplyResultWillShow']);?></span></div></li>
        		</ul>
        		<?php if($ShowApplicationResultandButton && $CorrectApplicantType){ ?>
                <a id="nextStep" onclick="javascript:checkSubmit()"  class="btn btn-primary" role="button"><?php echo $Lang['eEnrolment']['EditClubApply'];?></a>
    		  	<p class="small text-center"><a href="parent_orderingSelectClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&EnrolGroupIDList=<?php echo implode(',',$AppliedClubEnrolGroupIDsAry);?>"><?php echo $Lang['eEnrolment']['EditClubOrder'];?></a></p>
          
              <?  }
                ?>
    		</div>
    	</div>	
	<? }
     }?>
	

	<?php if($FinishApplication){?>
	<div class="row">
      <div class="clubApplication col-xs-12">
      
      <?php if($ShowApplicationResultandButton){?>
     
		  <div class="pageTitle"><span><?php echo $i_ClubsEnrollment;?></span></div>
		  <div class="applicationPeriod"><span><?php echo $Lang['eEnrolment']['FromTodayTo'].' '.$EndTime;?></span></div>
		  <div class="showMore" id="showMore"><a href="#">(<?php echo $Lang['eEnrolment']['ShowApplyInstruction'] ;?>)</a></div>
		  <div class="moreInfo" id="moreInfo">
			  <div class="row">
				<div class="applicationLabel col-xs-12">
					<span><?php echo $Lang['General']['Instruction'];?></span>
				</div>
				<div class="applicationContent col-xs-12">
					<span><?php echo $descriptionText;?></span>
				</div>
				<div class="applicationLabel col-xs-12">
					<span><?php echo $i_ClubsEnrollment_TieBreakRule;?></span>
				</div>
				<div class="applicationContent col-xs-12">
					<span><?php echo $tieBreakRuleText;?></span>
				</div>
				<div class="applicationLabel col-xs-12">
					<span><?php echo ($sys_custom['eEnrolment']['Application']['MinApply']?$Lang['eEnrolment']['NoOfClubMustBeApply']:$i_ClubsEnrollment_MinForStudent);?></span>
				</div>
				<div class="applicationContent col-xs-12">
					<span><?php echo $displayMinQuatoClub;?></span>
				</div>
				<div class="hideInfo" id="hideInfo"><a href="#">(<?php echo $Lang['General']['Hide'];?>)</a></div>
				<p>&nbsp;</p>
			  </div>
		  </div>
		  
		  <?php } ?>
		  <div class="row">
			  <div class="subTitle pending col-xs-12">
				  <div class="title"><?php echo $Lang['eEnrolment']['InApplication2'];?> <!--  <small>(<?php echo $eEnrollment['waiting'];?>)</small>--></div>
			  </div>
		  </div>
		  <div class="row">
			  <ul class="club pending withOrder col-xs-12">
			  
			  <?php 
			  $i=0;
			  foreach ($canApplyclubInfoAry2 as $thisEnrolGroupID=>$EnrolGlubInfoAry){
			      $i++;			      
			      $StudentEnrolClub = $libenroll->Add_Student_To_Club_Enrol_List($StudentID,$thisEnrolGroupID,$i);
			      $thisSemester = $EnrolGlubInfoAry['Semester'];
			      $thisClubTitle = Get_Lang_Selection($EnrolGlubInfoAry['TitleChinese'],$EnrolGlubInfoAry['Title']);  
			      
			      $thisDisplaySemester = $DisplaySemester[$thisSemester];
			      $thisSemesterName = $thisSemester==""?$i_ClubsEnrollment_WholeYear: $thisSemester;
			      ?>
			      
			      <a href="parent_eEnrol_applyClubDetials.php?isStudentApp=<?=$isStudentApp?>&EnrolGroupID=<?php echo $thisEnrolGroupID;?>">
					 <li class="<?php echo $thisDisplaySemester;?>">
						<div class="orderNum"><?php echo $i;?></div>
						<div class="clubTitle"><span><?php echo $thisClubTitle;?></span> <span class="tag label"><?php echo $thisSemesterName;?></span></div>
						  
        			     <? if(in_array($thisEnrolGroupID,$TimeClashGroupIDAry)){ ?>
        			       <div class="clubInfo">
        					 <div class="row"><span class="icon timeClash"></span><span class="timeClash"><?php echo $eEnrollment['time_crash'];?></span> </div>
        				  </div>
        			      <? } ?>
			     	</li>
			      </a>
			  <?php } ?>
 
				  <li><div class="infoTxt text-center"><span> <?php echo str_replace("[__date__]", $AnnounceEnrolmentResultDate,$Lang['eEnrolment']['ClubApplyResultWillShow']);?></span></div></li>
			  </ul>
	
		  
		  <?php if($ShowApplicationResultandButton  && $CorrectApplicantType){ ?>
            <a id="nextStep" onclick="javascript:checkSubmit()" class="btn btn-primary" role="button"><?php echo $Lang['eEnrolment']['EditClubApply'];?></a>
		  	<p class="small text-center"><a href="parent_orderingSelectClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&EnrolGroupIDList=<?php echo implode(',',$EnrolGroupIDAry);?>"><?php echo $Lang['eEnrolment']['EditClubOrder'];?></a></p>      
          <?  }
            ?>
		  </div>
    </div>
	
	
	<?php } ?>
	
    <?php  if($InParticipatingClubCount> 0 && !$withinProcessStage && !$withinEnrolStage){?>
    <div class="row">
      <div class="subTitle participating col-xs-12">
        <div class="title"><?php echo $Lang['eEnrolment']['InParticipating'];?></div>
      </div>
    </div>
   
    <div class="row">
      <ul class="club participating col-xs-12">       
        <?php    
         for($i=0;$i<$InParticipatingClubCount;$i++){
             $semesterDisplay =  $InParticipatingSemester[$i]==''?$i_ClubsEnrollment_WholeYear: $InParticipatingSemester[$i];
             ?>

         <a href="parent_eEnrol_clubInfo_start.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&EnrolGroupID=<?php echo $InParticipatingClubID[$i];?>&SemesterClass=<? echo $InParticipatingSemesterClass[$i];?>">   
        	<li class="<?php echo $InParticipatingSemesterClass[$i];?>">
          	<div class="clubTitle"> <span><?php echo $InParticipatingClubTitle[$i];?></span> <span class="tag label"><?=$semesterDisplay?></span> </div>              
               <?php if($InParticipatingClubMeetingCount[$i] != 0){ ?>
           
               <div class="progressArea col-xs-12">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $InParticipatingDisplayDateCount[$i];?>" aria-valuemin="0" aria-valuemax="100" > <span  class="popOver" data-toggle="tooltip" data-placement="top" title="<?php echo $Lang['eEnrolment']['Finished'].' '.$InParticipatingMettingDateBeforeCount[$i].' '.$Lang['eEnrolment']['Times'];?>"> </span> </div>
                  <span class="totalClass"><?php echo $Lang['eEnrolment']['Total'].' '.$InParticipatingClubMeetingCount[$i].' '.$Lang['eEnrolment']['Times'];?></span> </div>
               </div>
             
			  <div class="clubInfo">
			  <?php if($InParticipatingClubAttendanceHours[$i] != '0'){
			  
			   ?>
				<div class="row marginB10">
				  <div class="attendanceRate col-xs-6">
					<div class="icon"></div>
					<span class="labelTitle"><?php echo $Lang['eEnrolment']['attendance_percent'];?></span> <span><?php echo $InParticipatingClubAttendanceRate[$i];?> %</span> </div>
				  <div class="attendanceHour col-xs-6">
					<div class="icon"></div>
					<span class="labelTitle"><?php echo $Lang['eEnrolment']['AttendanceHour'];?></span> <span><?php echo $InParticipatingClubAttendanceHours[$i].' '.$Lang['eEnrolment']['Hour'];?></span> </div>
				</div>
				<?php } ?>
				<div class="nextClass">
				  <div class="icon"><?php echo $InParticipatingNextMeetingMonthday[$i];?></div>
				  <span class="labelTitle"><?php echo $Lang['eEnrolment']['NextMeeting'];?></span> <span><?php echo $InParticipatingDayDifferent[$i].' ('.$InParticipatingNextMeetingWeekday[$i].') '.$InParticipatingNextMeetingDate[$i];?></span> </div>
			  </div>
			   <?php }?>            
            </li>
        </a> 
        <?php  }    ?>     
      </ul>
    </div>
    <?php 
    }
    if($FinishedClubCount>0 &&  !$withinProcessStage && !$withinEnrolStage){?>  
     <div class="row">
      <div class="subTitle finished col-xs-12">
        <div class="title"><?php echo $Lang['eEnrolment']['FinishedClub'];?></div>
      </div>
    </div>
    <div class="row">
      <ul class="club finish col-xs-12">      
        <?php     
        for($i=0;$i<$FinishedClubCount;$i++){ 
            $_thisFinishedClubID = $thisfinishedClub[$i];
            $_thisFinishedClubTitle = $thisfinishedClubTitle[$i];
            $_thisFinishedClubCount = $thisfinishedClubDateCount[$i];
            $_thisFinishedClubAttendanceRate = $thisfinishedClubAttendanceRate[$i];
            $_thisFinishedClubAttendanceHours = $thisfinishedClubAttendanceHours[$i];
        ?>
         <a href="parent_eEnrol_clubInfo_start.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&EnrolGroupID=<?php echo $_thisFinishedClubID;?>&SemesterClass=<? echo $thisfinishedClubSemesterClass[$i];?>">   
        <li class="<?php echo $thisfinishedClubSemesterClass[$i];?>">
          <div class="clubTitle"> <span><?php echo $_thisFinishedClubTitle;?></span> <span class="tag label"><?php echo $thisfinishedClubSemester[$i];?></span> </div>
          <?php if($_thisFinishedClubCount != 0 ){ ?>                   
          <div class="progressArea col-xs-12">
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" > <span  class="popOver" data-toggle="tooltip" data-placement="top" title="<?php echo $Lang['eEnrolment']['FinishedMeeting'];?>"> </span> </div>
              <span class="totalClass"><?php echo $Lang['eEnrolment']['Total'].' '.$_thisFinishedClubCount.' '.$Lang['eEnrolment']['Times'];?></span> </div>
          </div>
          <?php if($_thisFinishedClubAttendanceRate){?>
          <div class="clubInfo">
            <div class="row marginB10">
              <div class="attendanceRate col-xs-6">
                <div class="icon"></div>
                <span class="labelTitle"><?php echo $Lang['eEnrolment']['attendance_percent'];?></span> <span><?php echo $_thisFinishedClubAttendanceRate;?> %</span> </div>
              <div class="attendanceHour col-xs-6">
                <div class="icon"></div>
                <span class="labelTitle"><?php echo $Lang['eEnrolment']['AttendanceHour'];?></span> <span><?php echo $_thisFinishedClubAttendanceHours.' '.$Lang['eEnrolment']['Hour'];?> </span> </div>
            </div>
          </div>
           <? }
        } ?>
        </li>
        </a>
        <? } ?>
      </ul>
    </div>
    <?php }?>
  </div>
</div>

<div class="modal fade" id="infoDetails" tabindex="-1" role="dialog" aria-labelledby="infoDetails" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infolLabel"><span class="icon applyResult"></span><?php echo $Lang['eEnrolment']['ClubApplicationResult'];?></h5>
      </div>
      <div class="modal-body">
      
      <table class="tbl_result succeed">
      <?php      
      foreach ($AppliedClubInfoAssoAry as $AppliedClubRecordStatus=>$AppliedClubIDInfoAry){
          foreach($AppliedClubIDInfoAry as $AppliedEnrolGroupID=>$AppliedClubInfoAry){
              $Title = $AppliedClubInfoAry['Title']; 
        
              if($AppliedClubRecordStatus == '2'){                            
                  ?>
                
    			  <tr>
    				  <td><?php echo $Title;?></td>
    				  <td><span class="icon"></span> <span><?php echo $i_ClubsEnrollment_StatusApproved;?></span></td>
    			  </tr>
              
         	<?  }
           }
      }?>
		 </table>
	   
		 <table class="tbl_result rejected">
		 <?php  
		 foreach ($AppliedClubInfoAssoAry as $AppliedClubRecordStatus=>$AppliedClubIDInfoAry){
		     foreach($AppliedClubIDInfoAry as $AppliedEnrolGroupID=>$AppliedClubInfoAry){
		         $Title = $AppliedClubInfoAry['Title']; 
		
    			  if($AppliedClubRecordStatus == '1'){ ?>
              		 <tr>
        				  <td><?php echo $Title;?></td>
        				  <td><span class="icon"></span> <span><?php echo $i_ClubsEnrollment_StatusRejected;?></span></td>
        			  </tr>        
        		<? }
		     }
		 }?>		 
		  </table>
	  </div>
	  <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal"><?php echo $eEnrollment['confirm'];?></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="warninArrivedMax" tabindex="-1" role="dialog" aria-labelledby="warninArrivedMax" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warninglLabelMin"><i class="fas fa-exclamation-triangle"></i><?=$Lang['eEnrolment']['CannotEnrolledClub']?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">
        <div class="icon"></div>
        </span> </button>
      </div>
      <div class="modal-body">
			<span id="alertClubMaxCountMessage"></span>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$(window).on('load',function(){
    <?php if($ShowResult){ ?>
    $('#infoDetails').modal('show');
    <? } ?>
   
});

function StartApplication(){
	if(<?=$CanEnrolNew?> == 1){
		document.form1.action =  "parent_eEnrol_club_apply.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>";
		document.form1.method = "POST";
    	document.form1.submit();
	}else{
		$("#nextStep").attr("data-target","#warninArrivedMax"),
		$("#nextStep").attr("data-toggle","modal"),
		document.getElementById("alertClubMaxCountMessage").innerHTML =  '<?=$Lang['eEnrolment']['AlertArchivedMaxCanEnrolledClub']?>';		
	}	
}
function checkSubmit(){
	if(<?=$CanEnrolNew?> == 1){

    	<?php  if(!$PastAnnounceDate){   ?>
    	document.form1.action ="parent_eEnrol_applyClub_chooseClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&EnrolGroupIDList=<?php echo implode(',',$AppliedClubEnrolGroupIDsAry);?>";
    	<?php } ?>
    
        <?php if($FinishApplication){?>
        document.form1.action = "parent_eEnrol_applyClub_chooseClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&EnrolGroupIDList=<?php echo $EnrolGroupIDList;?>" ;
        <?php }?>
    
    
    
    
    	document.form1.method = "POST";
    	document.form1.submit();
	}
	else{
		$("#nextStep").attr("data-target","#warninArrivedMax"),
		$("#nextStep").attr("data-toggle","modal"),
		document.getElementById("alertClubMaxCountMessage").innerHTML =  '<?=$Lang['eEnrolment']['AlertArchivedMaxCanEnrolledClub']?>';
		
	}
}    
</script>
</form>
</body>
</html>
