<?php 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$enrolClubInfo = $libenroll->Get_Club_Info_By_EnrolGroupID($EnrolGroupID);

$currentDate = date('Y-m-d');
$thisClubDisplayName = Get_Lang_Selection($enrolClubInfo['TitleChinese'],$enrolClubInfo['Title']);
$thisClubSemester = $enrolClubInfo['Semester'];
$thisClubSemester = $thisClubSemester==''?$i_ClubsEnrollment_WholeYear:$thisClubSemester;

$thisClubContent = $enrolClubInfo['Description'];
if ($enrolClubInfo['Gender'] == 'F'){
    $thisClubGendar = $Lang['General']['Male'];
}
else if ($enrolClubInfo['Gender'] == 'M'){
    $thisClubGendar = $Lang['General']['Male'];
}
else {
    $thisClubGendar =$Lang['eEnrolment']['NoLimit'];
}


$thisClubQuato = $enrolClubInfo['Quota']=='0'?$Lang['eEnrolment']['NoLimit']:$enrolClubInfo['Quota'];
$thisClubAgeLimit = $enrolClubInfo['LowerAge']=='0' ? $Lang['eEnrolment']['NoLimit']:$enrolClubInfo['LowerAge'] .' - '.$enrolClubInfo['UpperAge'];


$CategoryID = $enrolClubInfo['GroupCategory'];
$CategoryInfoAry = $libenroll-> GET_CATEGORYINFO($CategoryID);
$CategoryName = $CategoryInfoAry['CategoryName'];

$isAmountTBC = $enrolClubInfo['isAmountTBC'];
$thisClubPaymentAmount = $enrolClubInfo['PaymentAmount'];
$paymentAmountTBC = $isAmountTBC=='1'?'('.$Lang['eEnrolment']['Tentative'].')':'';

//OLE CATEGORY
$thisClubOLEInfoAry = $libenroll->Get_Enrolment_Default_OLE_Setting('club',array($EnrolGroupID));


$thisClubOLECategoryList = $thisClubOLEInfoAry[0]['OLE_Component'];
$thisClubOLECategoryAry = explode(',',$thisClubOLECategoryList);
$sql = "SELECT ".Get_Lang_Selection("ChiTitle","EngTitle").
" FROM {$eclass_db}.OLE_ELE
WHERE DefaultID IN ('".implode('\',\'',$thisClubOLECategoryAry)."')";
$OLECategory = $libenroll->returnVector($sql);
$OLECategoryName = array();
foreach($OLECategory as $OLECategoryAry){
    $OLECategoryName[] =  $OLECategoryAry;
}
$thisClubOLECategory = implode(',',$OLECategoryName);
$thisClubOLECategory = $thisClubOLECategory==''?'-':$thisClubOLECategory;

$libenrolllass = new libclass();
$ClassLvlArr = $libenrolllass->getLevelArray();
$GroupArr = $libenroll->GET_GROUPCLASSLEVEL($EnrolGroupID);
$GroupYearID = Get_Array_By_Key($GroupArr,"ClassLevelID");

$className = array();
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
    for ($j = 0; $j < sizeof($GroupArr); $j++) {
        if (in_array($ClassLvlArr[$i][0],$GroupYearID)) {
            $className[] =  $ClassLvlArr[$i]['LevelName'];
            break;
        }
    }
}
$dispPlayClassName = implode(',',$className);
$EnrolGroupDateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);
$WeekDay = array();
foreach($EnrolGroupDateArr as $EnrolGroupDateInfoAry){
    $WeekDay[] = date("N", strtotime($EnrolGroupDateInfoAry['ActivityDateStart']));  
}
$ClubMeetingDateCountAry = array_count_values($WeekDay);

ksort($ClubMeetingDateCountAry);

$MeetingDateCountDisplay = '';
foreach($ClubMeetingDateCountAry as $MeetingDay=>$MeetingDateCount){
    $MeetingDateCountDisplay .= $Lang['eEnrolment']['WeekDay'][$MeetingDay].', '.$Lang['eEnrolment']['Total'].' '.$MeetingDateCount.' '.$Lang['eEnrolment']['Times'].'<br>';
    
}

$MeetingDateNo = count($EnrolGroupDateArr);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $Lang['eEnrolment']['Header']['ClubInfo'];?></title>
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../js/offcanvas.js"></script>
<link rel="stylesheet" href="../css/offcanvas.css">
<script type="text/javascript" src="../js/eEnrolment.js"></script>
<link rel="stylesheet" href="../css/eEnrolment.css">
<?php if($isStudentApp){?>
    <link rel="stylesheet" href="../css/eEnrolmentStudent.css">
<? } ?>
<!-- <script type="text/javascript" src="../js/eEnrol.js"></script> -->
<link rel="stylesheet" href="../css/font-awesome.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <nav id="header" class="navbar navbar-fixed-top enrolment">
    <div id="function"></div>
    <div class="headerTitle"><?php echo $Lang['eEnrolment']['Header']['ClubInfo'];?></div>
    <div id="button"><div class="headerIcon"><a href="javascript:history.go(-1)"><span class="close"></span></a></div></div> 
  </nav>
  <div id="filter" class="invisible"></div>
  <div id="content">
	<div class="row">
      <div class="classDateDetails term2 col-xs-12">
        <div class="pageTitle"> <span><?php echo $thisClubDisplayName;?></span></div>
      </div>
    </div>
    <?php if($MeetingDateNo != 0){?>
    	<div class="row">
          <div class="subTitle clubDateTime col-xs-12">
            <div class="title"><?php echo $Lang['eEnrolment']['MeetingSchedule'];?></div>
          </div>
        </div>
    	<div class="row marginB10">
          <div class="classDateDetails term1 col-xs-12">
    		<div class="classDateContent">
            <div class="classDate"><span><?php echo $MeetingDateCountDisplay;?></span>
              <div>
                <ol class="dateList">             
                  <?php 
                  for ($i=0;$i<($MeetingDateNo-1);$i++){
                      $MeetingWeekday =  date("N", strtotime($EnrolGroupDateArr[$i]['ActivityDateStart']));               
                      $MeetingWeekday = $Lang['eEnrolment']['ShortWeekday'][$MeetingWeekday];
                      $MeetingDate = date("Y-m-d", strtotime($EnrolGroupDateArr[$i]['ActivityDateStart'])).' ('.$MeetingWeekday. ') '.date("H:i", strtotime($EnrolGroupDateArr[$i]['ActivityDateStart'])).' - '.date("H:i", strtotime($EnrolGroupDateArr[$i]['ActivityDateEnd']));
                    
                      if($i==0){?>
                       <li><?php echo $MeetingDate;?></li>   
        			  	<div class="moreDate" id="moreDate">    
                    <? }else {?>                 
                   	<li><?php echo $MeetingDate;?></li>                               
                  <?php }
                  } ?>              
    				</div>
    			</ol>
    			<?php 
    			$MeetingWeekday =  date("N", strtotime($EnrolGroupDateArr[$MeetingDateNo-1]['ActivityDateStart']));
    			$MeetingWeekday = $Lang['eEnrolment']['ShortWeekday'][$MeetingWeekday];
    			$MeetingDate = date("Y-m-d", strtotime($EnrolGroupDateArr[$MeetingDateNo-1]['ActivityDateStart'])).' ('.$MeetingWeekday. ') '.date("H:i", strtotime($EnrolGroupDateArr[$MeetingDateNo-1]['ActivityDateStart'])).' - '.date("H:i", strtotime($EnrolGroupDateArr[$MeetingDateNo-1]['ActivityDateEnd']));   			
    			if($MeetingDateNo == 2 || $MeetingDateNo == 1){ 
    			    
    			}else{?>
		            <div class="showMoreDate" id="showMoreDate"><a href="#" class="icon"></a></div>
            		<? }  ?>	
            		<ol class="dateList lastDate" start="<?php echo $MeetingDateNo;?>">
                       <li><?php echo $MeetingDate;?></li>
                    </ol>
    		  </div>	
            </div>   		 
    		</div>
        </div>
       </div>
      <?php  }?>
    </div>
    <div class="row">
      <div class="subTitle info col-xs-12">
        <div class="title"><?php echo $Lang['eEnrolment']['Header']['ActivityInfo'] ;?></div>
      </div>
    </div>
    <div class="row marginB10">
      <div class="classInfoDetails col-xs-12">
		  <div class="infoLabel"><?php echo $eEnrollment['ClubType'];?></div>
		  <div class="infoContent"><?php echo $CategoryName;?></div>
		  <div class="showMore" id="showMore"><a href="#">(<?php echo $Lang['eEnrolment']['ShowMore'];?>)</a></div>
		  <div class="moreInfo" id="moreInfo">
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['ClubContent'];?></div>
			  <div class="infoContent"><?php echo $thisClubContent;?></div>
			  <div class="infoLabel"><?php echo $eEnrollment['OLE_Components']?></div>
			  <div class="infoContent"><?php echo $thisClubOLECategory;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['TargetForm'];?></div>
			  <div class="infoContent"><?php echo $dispPlayClassName;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['TargetAge'];?></div>
			  <div class="infoContent"><?php echo $thisClubAgeLimit;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['Gender'];?></div>
			  <div class="infoContent"><?php echo $thisClubGendar;?></div>
			  <div class="infoLabel"><?php echo $Lang['eEnrolment']['Quato'];?></div>
			  <div class="infoContent"><?php echo $thisClubQuato;?></div>
			  <div class="hideInfo" id="hideInfo"><a href="#">(<?php echo $button_hide;?>)</a></div>
		  </div>
	  </div>
    </div>
	<div class="row">
      <div class="subTitle fee col-xs-12">
        <div class="title"><?php echo $Lang['eEnrolment']['Fee'];?></div>
      </div>
    </div>
	<div class="row marginB10">
      <div class="feeDetails col-xs-12">
		  <div class="feeContent"><?php echo $thisClubPaymentAmount.$paymentAmountTBC;?></div>
	  </div>
    </div>
  </div>
</div>
</body>
</html>
