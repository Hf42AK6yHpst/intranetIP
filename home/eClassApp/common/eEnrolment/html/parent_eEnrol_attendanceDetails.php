<?php 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$enrolClubInfo = $libenroll->Get_Club_Info_By_EnrolGroupID($EnrolGroupID);

$thisClubDisplayName = Get_Lang_Selection($enrolClubInfo['TitleChinese'],$enrolClubInfo['Title']);

$StudentAttendanceInfoAry = $libenroll->Get_Student_Club_Attendance_Info(array($StudentID),array($EnrolGroupID));
$thisStudentAttendanceInfo = $StudentAttendanceInfoAry[$StudentID][$EnrolGroupID];
$AttendanceRate = $thisStudentAttendanceInfo['AttendancePercentageRounded'];
$AttendanceHours= $thisStudentAttendanceInfo['Hours'];
$AttendanceRate = intval($AttendanceRate);
$MettingDateBefore = $libenroll->getEnrolGroupDateByConditions($EnrolGroupID,'1');
$MettingDateAfter = $libenroll->getEnrolGroupDateByConditions($EnrolGroupID,'');
$AssocMettingDateBefore = BuildMultiKeyAssoc($MettingDateBefore, array('GroupDateID'));


$Present= 0;
$Exempt = 0;
$Absent = 0;
$Late = 0;
foreach($thisStudentAttendanceInfo as $GroupDateID => $MettingDateInfo){
    if(is_int($GroupDateID)){
        $HaveRecordDate[] = $AssocMettingDateBefore[$GroupDateID]['ActivityDateStart'];
        $HaveRecordStatus[] = $MettingDateInfo['RecordStatus'];
        if($MettingDateInfo['RecordStatus'] == '1'){
            $Present++;
        }
        else if($MettingDateInfo['RecordStatus'] == '2'){
            $Exempt++;
        }
        else if($MettingDateInfo['RecordStatus'] == '3'){
            $Absent++;
        }
        else if($MettingDateInfo['RecordStatus'] == '4'){
            $Late++;
        }
        
    }
}

for ($i=0;$i<count($MettingDateBefore);$i++){
    $Month =  date("n", strtotime($MettingDateBefore[$i]['ActivityDateStart']));
    $MeetingWeekday =  date("N", strtotime($MettingDateBefore[$i]['ActivityDateStart']));
    $MeetingWeekday = $Lang['eEnrolment']['ShortWeekday'][$MeetingWeekday];
    $MeetingDate = date("Y-m-d", strtotime($MettingDateBefore[$i]['ActivityDateStart'])).' ('.$MeetingWeekday.') '.date("H:i", strtotime($MettingDateBefore[$i]['ActivityDateStart'])).' - '.date("H:i", strtotime($MettingDateBefore[$i]['ActivityDateEnd']));
 
    $MeetingGroupDateID = $MettingDateBefore[$i]['GroupDateID'];

    $DateRecordStatus = $thisStudentAttendanceInfo[$MeetingGroupDateID]['RecordStatus'];
    if($DateRecordStatus== '1'){
        $thisDateRecordStatus = 'onTime';
    }
    else if($DateRecordStatus== '2'){
        $thisDateRecordStatus = 'exempt';
    }
    else if($DateRecordStatus== '3'){
        $thisDateRecordStatus = 'absent';
    }
    else if($DateRecordStatus== '4'){
        $thisDateRecordStatus = 'late';
    }else{
        $thisDateRecordStatus = '';
    }
    

    $OneMonthRecord[$Month]['RecordStatus'][] = $thisDateRecordStatus;
    $OneMonthRecord[$Month]['MeetingDate'][] = $MeetingDate;  
    
}

for ($i=0;$i<count($MettingDateAfter);$i++){
    $Month =  date("n", strtotime($MettingDateAfter[$i]['ActivityDateStart']));
    $MeetingWeekday =  date("N", strtotime($MettingDateAfter[$i]['ActivityDateStart']));
    
    $MeetingWeekday = $Lang['eEnrolment']['ShortWeekday'][$MeetingWeekday];
    $MeetingDate = date("Y-m-d", strtotime($MettingDateAfter[$i]['ActivityDateStart'])).' ('.$MeetingWeekday. ') '.date("H:i", strtotime($MettingDateAfter[$i]['ActivityDateStart'])).' - '.date("H:i", strtotime($MettingDateAfter[$i]['ActivityDateEnd']));
    
    $MeetingGroupDateID = $MettingDateAfter[$i]['GroupDateID'];
    $DateRecordStatus = $thisStudentAttendanceInfo[$MeetingGroupDateID]['RecordStatus'];
    if($DateRecordStatus== '1'){
        $thisDateRecordStatus = 'onTime';
    }
    else if($DateRecordStatus== '2'){
        $thisDateRecordStatus = 'exempt';
    }
    else if($DateRecordStatus== '3'){
        $thisDateRecordStatus = 'absent';
    }
    else if($DateRecordStatus== '4'){
        $thisDateRecordStatus = 'late';
    }else{
        $thisDateRecordStatus = '';
    }
    $OneMonthRecord[$Month]['RecordStatus'][] = $thisDateRecordStatus;
    $OneMonthRecord[$Month]['MeetingDate'][] = $MeetingDate;

}


$MettingDateBeforeCount = count($MettingDateBefore);
$MettingDateAfterCount = count($MettingDateAfter);
$_thisClubMeetingDateCount = $MettingDateBeforeCount + $MettingDateAfterCount;
$displayDateCount = ($MettingDateBeforeCount* 100)/ $_thisClubMeetingDateCount;

$currentDate = date('Y-m-d');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $Lang['eEnrolment']['Header']['ClubInfo'];?></title>
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../js/offcanvas.js"></script>
<link rel="stylesheet" href="../css/offcanvas.css">
<link rel="stylesheet" href="../css/eEnrolment.css">
<!-- <script type="text/javascript" src="../../js/eEnrol.js"></script> -->
<link rel="stylesheet" href="../css/font-awesome.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
	
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['出席情況', '次數'],
          ['<?php echo $Lang['StudentAttendance']['Present']?>', <?php echo $Present;?>],
          <?php if($sys_custom['eEnrolment']['Attendance_Late']){ ?>
          ['<?php echo $Lang['StudentAttendance']['Late'];?>', <?php echo $Late;?>],
          <?php } ?>
          ['<?php echo $Lang['StudentAttendance']['Waived'];?>', <?php echo $Exempt;?>],
          ['<?php echo $Lang['StudentAttendance']['Absent'];?>', <?php echo $Absent;?>]
        ]);

      var options = {
		  colors: ['#69b577', '#69b577', '#69b577', '#f46060' ],
		  height: 176,
		  legend: {position: "none"},
		  pieHole: 0.9,
		  pieSliceBorderColor: "none",
		  pieSliceText: "none",
		  tooltip: {trigger: "none"},
		  width: 161,
		  chartArea:{left:7, top: 15, right: 7,bottom: 15}
      };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }
    </script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <nav id="header" class="navbar navbar-fixed-top enrolment">
    <div id="function">
      <div class="headerIcon"><a href="javascript:history.go(-1)"><i class="fas fa-arrow-left"></i></a></div>
    </div>
    <div class="headerTitle withFunction"><?php echo $Lang['eEnrolment']['Header']['ClubInfo'];?></div>
    <div id="button"></div>
  </nav>
  <div id="filter" class="invisible"></div>
  <div id="content">
    <div class="row marginB10">
      <div class="attendanceDetails term1 col-xs-12">
        <div class="pageTitle"> <span><?php echo $thisClubDisplayName;?></span> <span class="small">(<?php echo $Lang['eEnrolment']['Deadline'].' '.$currentDate;?>)</span> </div>
		  <div class="row">
			  <div class="chart col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-3">
				  <div id="piechart"></div>
			  
			  <div class="chartAttendanceRate">
				  <span class="chartLabel"><?php echo $Lang['eEnrolment']['attendance_percent'];?></span>
				  <div class="chartRate"><?php echo $AttendanceRate;?><span class="small"> %</span></div>
			  </div>
				  </div>
			  <div class="statistics col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-3">
				  <div class="onTime col-xs-6">
					  <div class="num"><?php echo $Present;?><span class="small"> <?php echo $Lang['eEnrolment']['Times'];?></span></div>
					  <div class="icon small"><?php echo $Lang['StudentAttendance']['Present'];?></div>
				  </div>
				  <?php if($sys_custom['eEnrolment']['Attendance_Late']){ ?>
				  <div class="late col-xs-6">
					  <div class="num"><?php echo $Late;?><span class="small"> <?php echo $Lang['eEnrolment']['Times'];?></span></div>
					  <div class="icon small"><?php echo $Lang['StudentAttendance']['Late'];?></div>
				  </div>
				  <?php } ?>
				  <div class="exempt col-xs-6">
					  <div class="num"><?php echo $Exempt;?><span class="small"> <?php echo $Lang['eEnrolment']['Times'];?></span></div>
					  <div class="icon small"><?php echo $Lang['StudentAttendance']['Waived'];?></div>
				  </div>
				  <div class="absent col-xs-6">
					  <div class="num"><?php echo $Absent;?><span class="small"> <?php echo $Lang['eEnrolment']['Times'];?></span></div>
					  <div class="icon small"><?php echo $Lang['StudentAttendance']['Absent'];?></div>
				  </div>
				  <div class="totalHour col-xs-12">
					  <span class="small"><?php echo $Lang['eEnrolment']['AttendanceHour'];?>︰</span><?php echo $AttendanceHours;?><span class="small"> <?php echo $Lang['eEnrolment']['Hour'];?></span>
				  </div>
			  </div>
		  </div>
	  </div>
    </div>
	<div class="row">
      <div class="subTitle attendanceRecord col-xs-12">
        <div class="title"><?php echo $Lang['eEnrolment']['AttendanceRecord'];?></div>
      </div>
    </div>
	<div class="row marginB10">
      <div class="attendanceRecord col-xs-12">
		  <div class="panel-group" id="attendanceAccordion" role="tablist" aria-multiselectable="true">
			 
			<?php 
			$i = 0;
			foreach ($OneMonthRecord as $Month => $MeetingInfoAry){
			    $i++;
			    $Number = $Lang['eEnrolment']['Number'][$i];
			   
			    $MeetingDateAry = $MeetingInfoAry['MeetingDate'];
			    $MeetingStatusAry = $MeetingInfoAry['RecordStatus'];
			    $MonthDisplay = $Lang['General']['month'][$Month];
			   
			    if($Month <= date('n') || ( $MettingDateAfterCount =='0')){  ?>
			   		
    			  <div class="panel panel-default">
    				  <div class="panel-heading" role="tab" id="heading<?php echo $Number;?>">
    					  <h4 class="panel-title">
    						  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $Number;?>" aria-expanded="true" aria-controls="collapse<?php echo $Number;?>"><?php echo $MonthDisplay;?></a>
    					  </h4>
    				  </div>
    				  <div id="collapse<?php echo $Number;?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?php echo $Number;?>">
    					  <div class="panel-body">
    						  <ul class="monthRecord">
    						<?php for($j=0;$j<count($MeetingDateAry);$j++){ 
    						//    if($MeetingStatusAry[$j]!= ''){?>
    							  <li class="<?php echo $MeetingStatusAry[$j];?>"><?php echo $MeetingDateAry[$j];?></li>
    						<?php //}
    						   }?>							 
    						  </ul>
    					  </div>
    				  </div>
    			  </div>			    
			<?  }else{ ?>
			    
			     <div class="panel panel-default">
				  <div class="panel-heading" role="tab" id="heading<?php echo $Number;?>e">
					  <h4 class="panel-title">
						  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $Number;?>" aria-expanded="false" aria-controls="collapse<?php echo $Number;?>"><?php echo $MonthDisplay;?></a>
					  </h4>
				  </div>
				  <div id="collapse<?php echo $Number;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $Number;?>">
					  <div class="panel-body">
						  <ul class="monthRecord">
							<?php for($j=0;$j<count($MeetingDateAry);$j++){ ?>
    							 <li class="<?php echo $MeetingStatusAry[$j];?>"><?php echo $MeetingDateAry[$j];?></li>
    						<?php }?>		
						  </ul>
					  </div>
				  </div>
			  </div>
			    
			<?  }
			} ?>
		  </div>
	  </div>
	</div>
  </div>
</div>

</body>
</html>
