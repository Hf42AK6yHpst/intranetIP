<?php
// using :

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eSchoolBus_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/schoolBusConfig.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_ui.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();
$indexVar['linterface'] = new libSchoolBus_ui();
$indexVar['libSchoolBus'] = new libSchoolBus();
$indexVar['db'] = new libSchoolBus_db();
$indexVar['leClassApp'] = new libeClassApp();

if (!$_SESSION['UserType']) {
    $user = new libuser($_SESSION['UserID']);
    $isTeacherStaff = $user->isTeacherStaff();
    $isParent = $user->isParent();
    $isStudent = $user->isStudent();
    
    if ($isTeacherStaff) {
        $userType = USERTYPE_STAFF;
    }
    else if ($isParent) {
        $userType = USERTYPE_PARENT;
    }
    else if ($isStudent) {
        $userType = USERTYPE_STUDENT;
    }
    else {
        $userType = '';
    }
}
else {
    $userType = $_SESSION['UserType'];
}

$task = '';
if ($_POST['task'] != '') {
	$task = $_POST['task'];
}
else if ($_GET['task'] != '') {
	$task = $_GET['task'];
}

if ($task == '') {
    switch ($userType) {
        case USERTYPE_STAFF:
            $task = 'teacherApp.index';
            break;
        case USERTYPE_PARENT:
            $task = 'parentApp.index';
            break;
//         case USERTYPE_STUDENT:
//             $task = '';
//             break;
    }
}
// Prevent searching parent directories, like 'task=../../index'.
$task = str_replace("../", "", $task);
$task = str_replace('.', '/', $task);

$indexVar['libSchoolBus']->checkAppAccessRight();

### Include corresponding php files
$indexVar['taskScript'] = '';
$indexVar['taskScript'] .= $task.'.php';

if (file_exists($indexVar['taskScript'])){
	include_once($indexVar['taskScript']);
}
else {
	$indexVar['taskScript'] = $task.'/index.php';
	if (file_exists($indexVar['taskScript'])){
		include_once($indexVar['taskScript']);
	}else{
		No_Access_Right_Pop_Up();
	}
}

intranet_closedb();
?>