<?php
/*
 *  Using:
 *  
 *  Purpose: update reason of applying leave for not taking school bus 
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2018-06-20 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$resultAry = array();
$dataAry = array();
$conditionAry = array();

$applicationID = $_POST['ApplicationID'];
$reason = $_POST['reason'];

if ($applicationID) {
    $dataAry['Reason'] = standardizeFormPostValue($reason);
    $dataAry['ModifiedBy'] = $_SESSION['UserID'];
    
    $conditionAry['ApplicationID'] = $applicationID;
    
    $ldb->Start_Trans();
    $sql = $ldb->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE',$dataAry,$conditionAry,false);
//debug_pr($sql);
    $resultAry[] = $ldb->db_db_query($sql);
            
    if (!in_array(false,$resultAry)) {
        $ldb->Commit_Trans();
        $returnMsgKey = 'UpdateSuccess';
    }
    else {
        $ldb->RollBack_Trans();
        $returnMsgKey = 'UpdateUnsuccess';
    }
}
else {
    $returnMsgKey = 'MissingApplicationID';
}

header("location: ?task=parentApp.index&tab=applyLeave&returnMsgKey=".$returnMsgKey);

?>
