<?
/*
 * Log
 *
 * Description: output json format data
 *
 * 2020-06-19 [Cameron] pass argument $studentID to getAvailableTimeSlotByDateRange() [case #Y188036]
 *
 * 2020-02-11 [Cameron] fix: avoid direct access the page (not via parameter) by checking $indexVar
 * 
 * 2019-10-15 [Cameron] avoid direct access the page (not via parameter) by checking $indexVar
 *
 * 2018-11-23 [Cameron] modify case checkApplication, return daysBeforeApplyLeave and cutoff time
 * 
 * 2018-11-05 [Cameron] not need to apply remove_dummy_chars_for_json, e.g. remark field
 *  
 * 2018-10-24 [Cameron] add paremeter isShowNoRecord to getApplyLeaveResult() in case getApplicationListForParent 
 *
 * 2018-06-14 [Cameron] create this file
 */

if (!$indexVar) {
    header("location: /");
}

$characterset = 'utf-8';

header('Content-Type: text/html; charset=' . $characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$y = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
//$remove_dummy_chars = ($junior_mck) ? false : true; // whether to remove new line, carriage return, tab and back slash
if ($indexVar) {
    $linterface = $indexVar['linterface'];
    $ldb = $indexVar['db'];

// debug_pr($_SESSION['SchoolBusStudentID']);
// debug_pr($_POST);

    switch ($action) {

        /*
         *  check
         *  1. is duplicate ?
         *  2. is before cut-off time ?
         *  3. is timeslot available ?
         */
        case 'checkApplication':
            $studentID = $_SESSION['SchoolBusStudentID'];
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
            $startDateType = $_POST['startDateType'];
            $endDateType = $_POST['endDateType'];
            $isTimeSlotAppliedLeave = $ldb->isTimeSlotAppliedLeave($studentID, $startDate, $endDate, $startDateType, $endDateType);

            $x = '';
            if ($isTimeSlotAppliedLeave) {
                $x = $schoolBusConfig['ApplyLeaveError']['DuplicateApplication'];
            } else {
                $isAllowedApplyLeaveAry = $ldb->isAllowedApplyLeave($studentID, $startDate, $startDateType);

                if (!$isAllowedApplyLeaveAry['isAllowed']) {
                    if ($isAllowedApplyLeaveAry['error'] == 'cutoffDate') {
                        $json['cutoffDateDescription'] = sprintf($Lang['eSchoolBus']['App']['Warning']['PassedCutoffDate'], $isAllowedApplyLeaveAry['daysBeforeApplyLeave']);
                        $x = $schoolBusConfig['ApplyLeaveError']['AfterCutoffDate'];
                    } else {
                        $json['cutoffTimeDescription'] = sprintf($Lang['eSchoolBus']['App']['Warning']['PassedCutoffTime'], $isAllowedApplyLeaveAry['cutoffTime']);
                        $x = $schoolBusConfig['ApplyLeaveError']['AfterCutoffTime'];
                    }
                }
            }

            if ($x == '') {
                $availableTimeSlotAry = $ldb->getAvailableTimeSlotByDateRange($startDate, $endDate, $startDateType, $endDateType, $studentID);
                if (count($availableTimeSlotAry) == 0) {
                    $x = $schoolBusConfig['ApplyLeaveError']['InvalidTimeSlot'];
                }
            }
            $json['success'] = true;
            break;

        case 'getBusTakeScheduleBodyForParent':
            $x = $linterface->getBusTakeScheduleBodyForParent($_SESSION['SchoolBusStudentID'], $_POST['offset'], $_POST['numberOfRecord']);
            $json['success'] = true;
            break;

        case 'getBusTakeScheduleLayoutForParent':
            $x = $linterface->getBusTakeScheduleLayoutForParent($_SESSION['SchoolBusStudentID'], $_POST['offset'], $_POST['numberOfRecord']);
            $json['success'] = true;
            break;

        case 'getApplicationListForParent':
            $x = $linterface->getApplyLeaveResult($_SESSION['SchoolBusStudentID'], $_POST['offset'], $_POST['numberOfRecord'], $_POST['isShowNoRecord']);
            $json['success'] = true;
            break;

    }
}
// if ($remove_dummy_chars) {
//     $x = remove_dummy_chars_for_json($x);
//     $y = remove_dummy_chars_for_json($y);
// }

$json['html'] = $x;
$json['html2'] = $y;
echo $ljson->encode($json);

?>