<?php
/*
 *  Using:
 *  
 *  Purpose: contains two tabs: schedule and apply leave.
 *      schedule lists child's school bus taking plan in the comming two weeks by default
 *
 *  2020-09-15 Cameron
 *      - add $numberOfRecordScheduleInit for schedule tab so that it can load faster at first time (with less record loaded)
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-09-23 Cameron
 *      - add argument display to showApplyLeave() so that it can hide add button when a student not need to take bus
 *
 *  2018-11-26 Cameron 
 *      - hide add button if student does not take school bus [case #Y153073]
 *      - remove class row in scheduleList (case #Y152924)
 *  
 *  2018-10-24 Cameron
 *      - handle retrieve next set of data when scroll down to the bottom
 *      
 *  2018-06-07 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
    $linterface = $indexVar['linterface'];
}
else {
    header("location: /");
}

if ($_GET['studentId']) {
    $isChild = $ldb->isChild($_SESSION['UserID'], $_GET['studentId']);
    if ($isChild) {
        $_SESSION['SchoolBusStudentID'] = $_GET['studentId'];            // get from api
    }
}

if (!$_SESSION['SchoolBusStudentID']) {
    $studentID = $ldb->getChild($_SESSION['UserID']);
    if ($studentID) {
        $_SESSION['SchoolBusStudentID'] = $studentID;
    }
}

if ($_GET['tab']) {
    $tab = $_GET['tab'];
}
else {
    $tab = 'schedule';
}

$returnMsgKey = $_GET['returnMsgKey'];
switch ($returnMsgKey ) {
    case 'AddSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddSuccess'];
        break;
    case 'AddUnSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddUnSuccess'];
        break;
    case 'InvalidTimeSlot':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['InvalidTimeSlot'];
        break;
//     case 'NotAllowedApplyLeave':
//         $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['NotAllowedApplyLeave'];
//         break;
    case 'MissingStudentID':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingStudentID'];
        break;
    case 'UpdateSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateSuccess'];
        break;
    case 'UpdateUnsuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateUnsuccess'];
        break;
    case 'MissingApplicationID':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingApplicationID'];
        break;
    case 'CancelSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelSuccess'];
        break;
    case 'CancelUnsuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelUnsuccess'];
        break;
    default:
        $msg = '';
        break;
}

$offsetInit = 0;            // starting index of record
$numberOfRecordInit = 14;   // number of record to retrieve each time
$numberOfRecordScheduleInit = 3;    // number of record to retrieve for first time
$isShowNoRecord = 1;        // show "There is no record at the moment" or not if there's no record

$isTakingBusStudent = $ldb->isTakingBusStudent($_SESSION['SchoolBusStudentID']);


include($PATH_WRT_ROOT."home/eClassApp/common/eSchoolBus/header.php");
?>

<script type="text/javascript">
var offsetInit = 7;				// initial value of offset
var numberOfRecord = 7;			// number of record to retrieve on subsequent scroll down to bottom
var offset = offsetInit;				 
var isGetAllRecord = false;
var element = $("#tab-content");
var lastHeight = $("#tab-content").css('height');

jQuery(document).ready( function() {
	var tab = "<?=$tab?>";
	if(tab=="schedule"){
		$('#tab_header a[href="#schedule"]').tab('show');
		showSchedule();		
	}
	if(tab=="applyLeave"){
		var display = '';
        <?php if (!$isTakingBusStudent):?>
            display = 'none';
        <?php endif;?>
        $('#tab_header a[href="#applyLeave"]').tab('show');
        showApplyLeave(display);
	}

	$('a[href="#schedule"]').click(function(e){
		e.preventDefault();
		isGetAllRecord = false;
		offset = offsetInit;				// reset offset
		$('#tab-content').scrollTop(0);		// must scroll to top so that it won't trigger scroll event on this click
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?task=parentApp.ajax',
			data : {
				'offset': <?php echo $offsetInit;?>,
				'numberOfRecord': <?php echo $numberOfRecordInit; ?>,
				'action': 'getBusTakeScheduleLayoutForParent'
			},		  
			success: updateScheduleTable,
			error: show_ajax_error
		});
	});

	$('a[href="#applyLeave"]').click(function(e){
		e.preventDefault();
    <?php if (!$isTakingBusStudent):?>
		$('div#addBtn').css({'display':'none'});
	<?php endif;?>
		
		isGetAllRecord = false;		
		offset = offsetInit;				// reset offset
		$('#tab-content').scrollTop(0);		// must scroll to top so that it won't trigger scroll event on this click
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?task=parentApp.ajax',
			data : {
				'offset': <?php echo $offsetInit;?>,
				'numberOfRecord': <?php echo $numberOfRecordInit; ?>,
				'isShowNoRecord': <?php echo $isShowNoRecord;?>,
				'action': 'getApplicationListForParent'
			},		  
			success: updateApplicationList,
			error: show_ajax_error
		});
	});

	$('#tab-content').on('scroll', function(e) {
		
 		var contentHeight = $(this).height();					// the height of the content that can be seen
 		var scrollHeight = $(this).prop('scrollHeight');		// the height of the content that need to scroll to view
 		var scrollTop = $(this).scrollTop();
 		
 		if ((scrollTop > 0) && (scrollHeight - scrollTop - 100 <= contentHeight)){
 	 		if (!isGetAllRecord) { 	 			 	 	 		
 	 			offset += numberOfRecord;
 				getFilteredResult(offset, numberOfRecord, true);
 	 		}
 		}
	});

// console.log('scrollHeight = ' + $('#tab-content').prop('scrollHeight'));
// console.log('scrollTop = ' + $('#tab-content').scrollTop());
// console.log('appHeight = ' + $('#tab-content').height());

    if (tab == 'schedule') {
        getFilteredResult(<?php echo $numberOfRecordScheduleInit;?>, <?php echo ($numberOfRecordInit - $numberOfRecordScheduleInit);?>, true);
    }

	<?php if ($msg):?>
		window.alert('<?php echo $msg;?>');
	<?php endif;?>
});

function getFilteredResult(_offset, _numberOfRecord, _append)
{
	var tab = $('#tab_header li.active a').attr('href');
	tab = tab.replace('#','');
	var action = (tab == 'schedule') ? ((_append == true) ? 'getBusTakeScheduleBodyForParent' : 'getBusTakeScheduleLayoutForParent') : 'getApplicationListForParent';
	$.ajax({
		dataType: "json",
		async: false,		 	// keep in sequence for showing result
		type: "POST",
		url: '?task=parentApp.ajax',
		data : {
			'action': action,
			'offset': _offset,
			'numberOfRecord': _numberOfRecord
		}
	})
	.done(function(ajaxReturn) {
		if (_append == true) {
			if (ajaxReturn.html == '') {
				isGetAllRecord = true;
			}
			else {
				if (tab == 'schedule') {
					$('#scheduleTable').append(ajaxReturn.html);
				}
				else {
					$('#applicationList').append(ajaxReturn.html);
				}
			}
		}
		else {
			if (tab == 'schedule') {
				updateScheduleTable(ajaxReturn);
			}
			else {
				updateApplicationList(ajaxReturn);
			}
		}
	})
	.fail(show_ajax_error);
}

function updateScheduleTable(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#scheduleList').html(ajaxReturn.html);
	}
}

function updateApplicationList(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#applicationList').html(ajaxReturn.html);
	}
}

function show_ajax_error() {
	alert('<?php echo $Lang['eSchoolBus']['App']['Error']['Ajax'];?>');
}

function setHeight(){
	var tabHeight = document.getElementById('tab_header').offsetHeight;
	var mainHeight = window.innerHeight - tabHeight;
	document.getElementById('tab-content').style.height = mainHeight+'px';
}

function checkForChanges(){
	setHeight();
    if (element.css('height') != lastHeight) {
        lastHeight = element.css('height'); 
        setHeight();
    }

    setTimeout(checkForChanges, 500);
}

</script>
</head>

<body onLoad="checkForChanges()">
<form name="form1" method="GET">
<div id="wrapper"> 
  
  <div id="esb-tab" class="<?php echo $tab == 'schedule' ? 'schedule' : 'applyLeave';?>"> 
    <!-- Nav tabs -->
    <ul id="tab_header" class="nav nav-tabs radial-out" role="tablist">
      <li role="presentation" class="col-xs-6"><a href="#schedule" aria-controls="schedule" role="tab" data-toggle="tab" onClick="showSchedule()"><?php echo $Lang['eSchoolBus']['App']['Tab']['Schedule'];?></a></li>
      <li role="presentation" class="col-xs-6"><a href="#applyLeave" aria-controls="applyLeave" role="tab" data-toggle="tab" onClick="showApplyLeave('<?php echo ($isTakingBusStudent) ? '' : 'none';?>')"><?php echo $Lang['eSchoolBus']['App']['Tab']['ApplyLeave'];?></a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content" id="tab-content" style="width:100%">
      
      <div role="tabpanel" class="tab-pane fade in <?php $tab == 'schedule' ? ' active ' : '';?>" id="schedule">
        <div id="main">         
          <div class="eventListArea">
            <ul class="eventList marginB10" id="scheduleList">
               	<?php echo $linterface->getBusTakeScheduleLayoutForParent($_SESSION['SchoolBusStudentID'], $offsetInit, $numberOfRecordScheduleInit);?>
            </ul>
          </div>
        </div>
      </div>
      
      <div role="tabpanel" class="tab-pane fade in <?php $tab == 'applyLeave' ? ' active ' : '';?>" id='applyLeave'>
		  <div class="main"> 
          <div class="eventListArea">
            <ul class="eventList" id="applicationList">
            	<?php echo $linterface->getApplyLeaveResult($_SESSION['SchoolBusStudentID'], $offsetInit, $numberOfRecordInit, $isShowNoRecord);?>
            </ul>
          </div>
        </div>
	  </div>
    </div>
  </div>


  <?php //if($allowAdd): ?> 
	<div id="addBtn" class="applyNew" style="display:<?php echo ($tab == 'schedule' || !$isTakingBusStudent) ? ' none ': '';?>;">
		<a href="?task=parentApp.applyLeaveNew"><span class="addIcon"></span></a>
	</div>
  <?php //endif;?>   
  
</div>
</form>
</body>
</html>
