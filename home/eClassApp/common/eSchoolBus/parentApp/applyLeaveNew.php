<?php
/*
 *  Using:
 *  
 *  Purpose: interface for applying leave of not taking school bus in a period 
 *
 *  2020-09-15 Cameron
 *      - show loading image when click goBack arrow
 *
 *  2019-12-06 Cameron
 *      - disable submit button after click to avoid input duplicate record [case #Y176496]
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-01-28 Cameron
 *      - only allow select 'From School' option dateType [case #Y154191]
 *      - Reason is not mandatory field
 *      
 *  2018-11-23 Cameron
 *      - show days before apply leave and cutoff time before submit if time is over [case #Y152928]
 *      
 *  2018-06-11 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$today = date('Y-m-d');

include($PATH_WRT_ROOT."home/eClassApp/common/eSchoolBus/header.php");
?>

<script type="text/javascript">
jQuery(document).ready( function() {
	$('#submitButton').click(function(e) {
		e.preventDefault();
        $('#submitButton').attr('disabled', true);
		if (checkForm()) {
	        $.ajax({
				dataType: "json",
				type: "POST",
				url: '?task=parentApp.ajax&action=checkApplication',
				data: $('#form1').serialize(),
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		if (ajaxReturn.html == "<?php echo $schoolBusConfig['ApplyLeaveError']['DuplicateApplication']; ?>") {
		        			alert("<?php echo $Lang['eSchoolBus']['App']['Warning']['DuplicateApplication'];?>");
		        			enableSubmitButton();
		        		} 
		        		else if (ajaxReturn.html == "<?php echo $schoolBusConfig['ApplyLeaveError']['AfterCutoffDate']; ?>") {
		        			alert(ajaxReturn.cutoffDateDescription);
		        			enableSubmitButton();
		        		}
		        		else if (ajaxReturn.html == "<?php echo $schoolBusConfig['ApplyLeaveError']['AfterCutoffTime']; ?>") {
		        			alert(ajaxReturn.cutoffTimeDescription);
		        			enableSubmitButton();
		        		}
		        		else if (ajaxReturn.html == "<?php echo $schoolBusConfig['ApplyLeaveError']['InvalidTimeSlot']; ?>") {
		        			alert("<?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['InvalidTimeSlot'];?>");
		        			enableSubmitButton();
		        		}
		        		else {
		        			var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['ConfirmSubmit'];?>");
		        			if (isConfirmed) {
		            			$('#form1').attr('action','?task=parentApp.applyLeaveNewUpdate');
		            			$('#form1').submit();
		        			}
		        			else {
                                enableSubmitButton();
                            }
		        		}
		        	}
		        	else {
                        enableSubmitButton();
                    }
		        },
				error: function(){
                    enableSubmitButton();
				    show_ajax_error();
                }
	        });
		}
		else {
            enableSubmitButton();
        }
	});
});

$(function () {
  	$('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
  	});
});

function enableSubmitButton()
{
    $('#submitButton').attr('disabled', false);
}

function checkForm()
{
	if($('#startDate').val() == '') {
		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['InputStartDate'];?>');
		form1.startDate.focus();
		return false;
	}

	if($('#endDate').val() == '') {
		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['InputEndDate'];?>');
		form1.endDate.focus();
		return false;
	}
	
    if ($('#startDate').val() < '<?php echo date('Y-m-d')?>') {
		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['AllowFutureDatesOnly'];?>');
		form1.startDate.focus();
		return false;
    }

    if ($('#startDate').val() > $('#endDate').val()) {
		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['StartDateIsLaterThanEndDate'];?>');
		form1.startDate.focus();
		return false;
    }

    if ($('#startDate').val() == $('#endDate').val()) {
        if (($('#startDateType').val() == 'PM') && (($('#endDateType').val() == 'AM') || ($('#endDateType').val() == 'WD'))) { 
			window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['StartDateTimeSlotIsLaterThanEndDateTimeSlot'];?>');
			form1.startDateType.focus();
			return false;
        }
        else if (($('#startDateType').val() == 'WD') && ($('#endDateType').val() == 'AM')) {
			window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['StartDateTimeSlotIsLaterThanEndDateTimeSlot'];?>');
			form1.startDateType.focus();
			return false;
        }
    }

//	if($('#reason').val() == '') {
//		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['InputLeaveReason'];?>');
// 		form1.reason.focus();
// 		return false;
// 	}
    
    return true;	
}

function goBackToList()
{
    var loadingImg = '<?php echo $indexVar['linterface']->Get_Ajax_Loading_Image(); ?>';
    $('#main .submission-area').html(loadingImg).siblings().remove();
    window.location.href = "?task=parentApp.index&tab=applyLeave";
}

function show_ajax_error() {
	alert('<?php echo $Lang['eSchoolBus']['App']['Error']['Ajax'];?>');
}

</script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <div id="header" class="inner edit navbar-fixed-top">
    <div id="function"><a href="javascript:goBackToList();"><span class="backIcon"></span></a> <span class="text"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Apply'];?></span></div>
  </div>
  <div id="main" class="edit">
    <div class="main">
      <form class="applicationForm" name="form1" id="form1" method="post">

        <div id='selectDateField' class='fieldInput date'>
        	<table width="100%">
				<tr>
			        <td width="10%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['General']['From'];?></span></td>
			        <td width="55%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="startDate" name="startDate" value="<?php echo $today;?>" />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="30%">
                		<div>
            	          	<select id='startDateType' name='startDateType' class='selectpicker' title=''>
<!--
             	            	<option value="WD" selected><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
 -->            	            	
            	            	<option value="PM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
                
				<tr>
			        <td width="10%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['General']['To'];?></span></td>
			        <td width="55%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="endDate" name="endDate" value="<?php echo $today;?>" />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="30%">
                		<div>
            	          	<select id='endDateType' name='endDateType' class='selectpicker' title=''>
<!--             	          	
            	            	<option value="WD" selected><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
 -->            	            	
            	            	<option value="PM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
            </table>
 		</div>
     		
		<div class="fieldTitle"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Reason'];?></div>
		<div id="inputPartnerField" class="fieldInput infoMissing">
			<input type="text" class="form-control inputField" id="reason" name="reason" placeholder="" value="">
		</div>		        
        
        <div class="mandatoryMsg"><?php echo $Lang['eSchoolBus']['App']['RequiredField'];?></div>
        <div class="submission-area">
          <button id="submitButton" class="btn btn-blue actionBtn" data-dismiss="modal" aria-label="Close"><?php echo $Lang['Btn']['Submit'];?></button>
        </div>
        
      </form>
    </div>
  </div>
</div>

</body>
</html>
