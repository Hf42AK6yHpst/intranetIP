<?php
/*
 *  Using:
 *  
 *  Purpose: approve leave application by class teacher / eSchool Bus admin 
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-03-07 Cameron
 *      - not need to call pushApplyLeaveResultToParent() as it has been done in approveAppliedLeave() [case #Y156983]
 *  
 *  2018-11-29 Cameron
 *      - send push message to parent when approve applied leave
 *      
 *  2018-06-22 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$resultAry = array();
$dataAry = array();
$conditionAry = array();

$applicationID = $_POST['ApplicationID'];
$remark = $_POST['remark'];

if ($applicationID) {
    $remark = standardizeFormPostValue($remark);
    $ret = $ldb->approveAppliedLeave($applicationID, $remark);
    $returnMsgKey = $ret ? 'ApproveSuccess' : 'ApproveUnsuccess';
}
else {
    $returnMsgKey = 'MissingApplicationID';
}

header("location: ?task=teacherApp.index&tab=handled&returnMsgKey=".$returnMsgKey);

?>
