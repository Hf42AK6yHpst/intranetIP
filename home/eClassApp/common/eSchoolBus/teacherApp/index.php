<?php
/*
 *  Using:
 *  
 *  Purpose: contains two tabs: applications to be handled and handled application list
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-09-26 Cameron
 *      - change default EndDate value to today [case Y170976]
 *
 *  2019-04-03 Cameron
 *      - add checking for StartDate < EndDate, empty the result if such logic violates
 *      
 *  2019-03-07 Cameron
 *      - classSelection and studentSelection should be filter by taking bus students only [case #A156825]
 *        
 *  2019-01-30 Cameron
 *      - add filter: startDate, endDate and applyLeaveStatus [case #Y154190]
 *      
 *  2018-10-24 Cameron
 *      - handle retrieve next set of data when scroll down to the bottom
 *      
 *  2018-06-21 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
    $linterface = $indexVar['linterface'];
}
else {
    header("location: /");
}

if ($_GET['tab']) {
    $tab = $_GET['tab'];
}
else {
    $tab = 'pending';
}

$returnMsgKey = $_GET['returnMsgKey'];
switch ($returnMsgKey ) {
    case 'AddSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddSuccess'];
        break;
    case 'AddUnSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddUnSuccess'];
        break;
    case 'ApproveSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['ApproveSuccess'];
        break;
    case 'ApproveUnSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['ApproveUnSuccess'];
        break;
    case 'InvalidTimeSlot':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['InvalidTimeSlot'];
        break;
    case 'MissingStudentID':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingStudentID'];
        break;
    case 'UpdateSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateSuccess'];
        break;
    case 'UpdateUnsuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateUnsuccess'];
        break;
    case 'MissingApplicationID':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingApplicationID'];
        break;
    case 'CancelSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelSuccess'];
        break;
    case 'CancelUnsuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelUnsuccess'];
        break;
    case 'RejectSuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['RejectSuccess'];
        break;
    case 'RejectUnsuccess':
        $msg = $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['RejectUnsuccess'];
        break;
        
    default:
        $msg = '';
        break;
}

// for filter
$isSchoolBusAdmin = $indexVar['libSchoolBus']->isSchoolBusAdmin();

if ($isSchoolBusAdmin) {
    $classSelection = $linterface->getTakingBusClassSelection("name='className' id='className' class='selectpicker'", '', $Lang['SysMgr']['FormClassMapping']['All']['Class']);
    $studentAry = $ldb->getAllStudentOfCurrentYear();
}
else {
    $studentAry = $ldb->getStudentsByClassTeacher($_SESSION['UserID']);
}
$studentSelection = getSelectByArray($studentAry,"name='studentID' id='studentID' class='selectpicker'", $selected="", $all=0, $noFirst=0, $FirstTitle=$Lang['eSchoolBus']['TeacherApp']['AllStudent']);

$statusSelection = $linterface->getApplyLeaveStatusSelection("name='applyLeaveStatus' id='applyLeaveStatus' class='selectpicker'", $selected="", $all=0, $noFirst=0, $FirstTitle=$Lang['eSchoolBus']['App']['ApplyLeave']['AllStatus']);

$offset = 0;                // starting index of record
$numberOfRecord = 10;       // number of record to retrieve each time
$isShowNoRecord = 1;        // show "There is no record at the moment" or not if there's no record

$startDate = date('Y-m-d');
$endDate = $startDate;
//$endDate = date('Y-m-d',strtotime("+1 month"));

include($PATH_WRT_ROOT."home/eClassApp/common/eSchoolBus/header.php");
?>

<script type="text/javascript">
var offset = <?php echo $offset;?>;
var numberOfRecord = <?php echo $numberOfRecord; ?>;
var isGetAllRecord = false;
var element = $("#tab-content");
var lastHeight = $("#tab-content").css('height');

jQuery(document).ready( function() {
	var tab = "<?=$tab?>";
	if(tab=="pending"){
		$('#tab_header a[href="#pending"]').tab('show');
		showPending4Teacher();		
	}
	if(tab=="handled"){
		$('#tab_header a[href="#handled"]').tab('show');
		showHandled4Teacher();		
	}

	$('a[href="#pending"]').click(function(e){
		e.preventDefault();
		doPendingClick(true);
	});

	$('a[href="#handled"]').click(function(e){
		e.preventDefault();
		doHandledClick(true);
	});

	$('#className').change(function(){
		isGetAllRecord = false;
		offset = 0;
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?task=teacherApp.ajax',
			data : {
				'action': 'getStudentListByClass',
				'className': $('#className').val()
			},		  
			success: updateStudentList,
			error: show_ajax_error
		});
		
	});

	$(document).on('change','#studentID',function(e){
		isGetAllRecord = false;
		offset = 0;
		$('#tab-content').scrollTop(0);
		getFilteredResult(offset, numberOfRecord, false);
	});

	$('#inputSearch').keyup(function(e) {
	  	if (e.which == 13) {
	  		isGetAllRecord = false;
	  		offset = 0;
	  		$('#tab-content').scrollTop(0);
	  		getFilteredResult(offset, numberOfRecord, false);
	  	}  
	});

	$('#inputSearch').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});

	$('#tab-content').on('scroll', function(e) {
 		var contentHeight = $(this).height();					// the height of the content that can be seen
 		var scrollHeight = $(this).prop('scrollHeight');		// the height of the content that need to scroll to view
 		var scrollTop = $(this).scrollTop();

// console.log('scrollHeight = ' + $('#tab-content').prop('scrollHeight'));
// console.log('scrollTop = ' + $('#tab-content').scrollTop());
// console.log('appHeight = ' + $('#tab-content').height());

		if ((scrollTop > 0) && (scrollHeight - scrollTop - 100 <= contentHeight)){
 	 		if (!isGetAllRecord) {
 	 			offset += numberOfRecord;
 				getFilteredResult(offset, numberOfRecord, true);
 	 		}
 		}
	});
	
	<?php if ($msg):?>
		window.alert('<?php echo $msg;?>');
	<?php endif;?>

		
  	$('#startDate, #endDate').on('change',function(e){
  		if ($('#startDate').val() > $('#endDate').val()) {
			alert("<?php echo $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'];?>");
			var tab = $('#tab_header li.active a').attr('href');
			tab = tab.replace('#','');
			if (tab == 'pending') {
				$('#pendingList').html("<?php echo '<br><br><div class=\"text-center\">'.$Lang['General']['NoRecordAtThisMoment'].'</div>';?>");
			}
			else {
				$('#applicationList').html("<?php echo '<br><br><div class=\"text-center\">'.$Lang['General']['NoRecordAtThisMoment'].'</div>';?>");
			}					
  		}
  		else {
      	    offset = 0;
      		getFilteredResult(offset, numberOfRecord, false);
  		}
  	});

  	$('#applyLeaveStatus').on('change',function(e){
  	    offset = 0;
  		getFilteredResult(offset, numberOfRecord, false);
  	});
  	
  	$('span#filterIcon, span#searchIcon').on('click',function(e){

        var tab = $('#tab_header li.active a').attr('href');
        tab = tab.replace('#','');
		if (tab == 'pending') {
			if ($(this).attr('id') == 'searchIcon') {
				doPendingClick(true);
			}
			else {
				doPendingClick(false);
			}	
		}
		else if (tab == 'handled') {
			doHandledClick(false);
		}
  	});
  	
});

$(function () 
{
  	$('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
  	}).on('dp.change',function(e){
  		//console.log(e.date);
  		$(this).parent().parent().find('.inputField').change();
  	});
});

function doPendingClick(checkClass)
{
	var inputSearch;
	var className;
	var studentID;
	var startDate;
	var endDate;
	var applyLeaveStatus;

	var checkSearch;
	var checkFilter;
	
	if (checkClass == true) {
		if ($('div#search').hasClass('in')) {
			checkSearch = true
		}
		else {
			checkSearch = false;
		}

		if ($('div#filter').hasClass('in')) {
			checkFilter = true;
		}
		else {
			checkFilter = false;
		}
	}
	else {
		if ($('div#search').hasClass('in')) {
			checkSearch = false
		}
		else {
			checkSearch = true;
		}

		if ($('div#filter').hasClass('in')) {
			checkFilter = false;
		}
		else {
			checkFilter = true;
		}
	}
	
	if (checkSearch == true) {
		inputSearch = $('#inputSearch').val();
	}
	else {
		inputSearch = '';
	}
	
    className = '';
    studentID = '';
    startDate = '';
    endDate = '';
    applyLeaveStatus = '';

	if (checkFilter == true) {
        className = ($('#className').length > 0 ) ? $('#className').val() : '';
        studentID = $('#studentID').val();
        startDate = $('#startDate').val();
        endDate = $('#endDate').val();
        applyLeaveStatus = $('#applyLeaveStatus').val();
        inputSearch = '';
	}
	else {
		// do nothing
	}

	$('#applyLeaveDiv').css('display','none');
	isGetAllRecord = false;
	offset = <?php echo $offset;?>;
	$('#tab-content').scrollTop(0);		// must scroll to top so that it won't trigger scroll event on this click
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?task=teacherApp.ajax',
		data : {
			'offset': 0,
			'numberOfRecord': numberOfRecord,
			'isShowNoRecord': <?php echo $isShowNoRecord;?>,
			'inputSearch': inputSearch,
			'className': className,
			'studentID': studentID,
			'startDate': startDate,
			'endDate': endDate,
			'applyLeaveStatus': applyLeaveStatus,		
			'action': 'getPendingListForTeacher'
		},		  
		success: updatePendingList,
		error: show_ajax_error
	});
}

function doHandledClick(checkClass)
{
	$('#applyLeaveDiv').css('display','');

	var inputSearch;
	var className;
	var studentID;
	var startDate;
	var endDate;
	var applyLeaveStatus;

	var checkSearch;
	var checkFilter;
	
	if (checkClass == true) {
		if ($('div#search').hasClass('in')) {
			checkSearch = true
		}
		else {
			checkSearch = false;
		}

		if ($('div#filter').hasClass('in')) {
			checkFilter = true;
		}
		else {
			checkFilter = false;
		}
	}
	else {
		if ($('div#search').hasClass('in')) {
			checkSearch = false
		}
		else {
			checkSearch = true;
		}

		if ($('div#filter').hasClass('in')) {
			checkFilter = false;
		}
		else {
			checkFilter = true;
		}
	}
	
	if (checkSearch == true) {
		inputSearch = $('#inputSearch').val();
	}
	else {
		inputSearch = '';
	}

	className = '';
	studentID = '';
	startDate = '';
	endDate = '';
	applyLeaveStatus = '';
	
	if (checkFilter == true) {
		className = ($('#className').length > 0 ) ? $('#className').val() : '';
		studentID = $('#studentID').val();
		startDate = $('#startDate').val();
		endDate = $('#endDate').val();
		applyLeaveStatus = $('#applyLeaveStatus').val();
		inputSearch = '';
	}
	else {
		// do nothing
	}

	isGetAllRecord = false;
	offset = <?php echo $offset;?>;
	$('#tab-content').scrollTop(0);		// must scroll to top so that it won't trigger scroll event on this click
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?task=teacherApp.ajax',
		data : {
			'offset': 0,
			'numberOfRecord': numberOfRecord,
			'isShowNoRecord': <?php echo $isShowNoRecord;?>,
			'inputSearch': inputSearch,
			'className': className,
			'studentID': studentID,
			'startDate': startDate,
			'endDate': endDate,
			'applyLeaveStatus': applyLeaveStatus,		
			'action': 'getHandledListForTeacher'
		},		  
		success: updateApplicationList,
		error: show_ajax_error
	});
}

function getFilteredResult(_offset, _numberOfRecord, _append)
{
	var tab = $('#tab_header li.active a').attr('href');
	tab = tab.replace('#','');
	var action = (tab == 'pending') ? 'getPendingListForTeacher' : 'getHandledListForTeacher';
	if ($('#applyLeaveStatus').val() == '1') {
		action = 'getPendingListForTeacher';	// overwrite above setting
	}	

	var inputSearch;
	var className;
	var studentID;
	var startDate;
	var endDate;
	var applyLeaveStatus;
	
	if ($('div#search').hasClass('in')) {
		inputSearch = $('#inputSearch').val();
	}
	else {
		inputSearch = '';
	}

	className = '';
	studentID = '';
	startDate = '';
	endDate = '';
	applyLeaveStatus = '';
	
	if ($('div#filter').hasClass('in')) {
		className = ($('#className').length > 0 ) ? $('#className').val() : '';
		studentID = $('#studentID').val();
		startDate = $('#startDate').val();
		endDate = $('#endDate').val();
		applyLeaveStatus = $('#applyLeaveStatus').val();
		inputSearch = '';
	}
	else {
		// do nothing
	}
	
	$.ajax({
		dataType: "json",
		type: "POST",
		async: false,		// keep in sequence for showing result
		url: '?task=teacherApp.ajax',
		data : {
			'action': action,
			'offset': _offset,
			'numberOfRecord': _numberOfRecord,
			'inputSearch': inputSearch,
			'className': className,
			'studentID': studentID,
			'startDate': startDate,
			'endDate': endDate,
			'applyLeaveStatus': applyLeaveStatus		
		},		  
		success: function(ajaxReturn) {
			if (_append == true) {
				if (ajaxReturn.html == '') {
					isGetAllRecord = true;
				}
				else {
    				if (tab == 'pending') {
    					$('#pendingList').append(ajaxReturn.html);
    				}
    				else {
    					$('#applicationList').append(ajaxReturn.html);
    				}
				}
			}
			else {
    			if (tab == 'pending') {
    				updatePendingList(ajaxReturn);
    			}
    			else {
    				updateApplicationList(ajaxReturn);
    			}
			}
		},
		error: show_ajax_error
	});
}

function updateStudentList(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#studentNameSpan').html(ajaxReturn.html);
		$('#studentID').selectpicker();
		getFilteredResult(<?php echo "$offset, $numberOfRecord";?>, false);
	}
}

function updatePendingList(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#pendingList').html(ajaxReturn.html);
	}
}

function updateApplicationList(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#applicationList').html(ajaxReturn.html);
	}
}

function show_ajax_error() 
{
	alert('<?php echo $Lang['eSchoolBus']['App']['Error']['Ajax'];?>');
}

function setTeacherAppHeight()
{
	var headerHeight = document.getElementById('teacher-header').offsetHeight;
	var tabHeight = document.getElementById('tab_header').offsetHeight;

	var mainHeight = window.innerHeight - headerHeight - tabHeight;
	document.getElementById('tab-content').style.height = mainHeight+'px';
}

function checkTeacherAppForChanges()
{
	setTeacherAppHeight();
    if (element.css('height') != lastHeight) {
        lastHeight = element.css('height'); 
        setHeight();
    }

    setTimeout(checkTeacherAppForChanges, 500);
}

function showPending4Teacher()
{
	$('#esb-teacher-tab').removeClass('handled');
	$('#esb-teacher-tab').addClass('pending');
	$('#addBtn').css({'display':''});
}

function showHandled4Teacher()
{
	$('#esb-teacher-tab').removeClass('pending');
	$('#esb-teacher-tab').addClass('handled');
	$('#addBtn').css({'display':'none'});
}

</script>

<style>
.datetimepicker {
    background-color: #429eeb;
    color: #666666;
}
.filterFont {
    font-size: 16px;
    padding-left: 6px;    
}
</style>
</head>

<body onLoad="checkTeacherAppForChanges()">
<form name="form1" method="">
<div id="wrapper"> 
  <!-- Header -->
  <div id="teacher-header">
    <div id="function" class="headerTitle"><span class="text"><?php echo $Lang['eSchoolBus']['App']['Tab']['ApplyLeave'];?></span></div>
    <div id="button"> <a role="button" data-toggle="collapse" href="#search" aria-expanded="false" aria-controls="search"> <span class="searchIcon" id="searchIcon" onClick="search()"></span> </a> <a role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter"> <span class="filterIcon" id="filterIcon" onClick="filter()"></span> </a>
    </div>
    <div class="collapse" id="search">
      <input type="text" class="form-control" id="inputSearch" name="inputSearch" placeholder="" value="<?php echo intranet_htmlspecialchars($inputSearch);?>">
    </div>
    <div class="collapse" id="filter">
  <?php 
        if ($isSchoolBusAdmin) {
            echo $classSelection;
            echo '<span id="studentNameSpan">'.$studentSelection.'</span>';
        }
        else {
            echo '<span id="studentNameSpan">'.$studentSelection.'</span>';
        }
  ?>
		<div id='selectDateField' class='fieldInput date'>
        	<table width="80%">
				<tr>
			        <td width="10%" class="fieldTitle"><span class="filterFont"><?php echo $Lang['General']['From'];?></span></td>
			        <td width="65%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="startDate" name="startDate" value="<?php echo $startDate;?>" />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                </tr>
                
				<tr>
			        <td width="10%" class="fieldTitle"><span class="filterFont"><?php echo $Lang['General']['To'];?></span></td>
			        <td width="65%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="endDate" name="endDate" value="<?php echo $endDate;?>" />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                </tr>
            </table>
 		</div>
 		
 		<div id="applyLeaveDiv" style="display:<?php echo $tab == 'pending' ? 'none' : '';?>">
		<?php echo $statusSelection;?>
		</div>
		
    </div>
  </div>  
  
  <div id="esb-teacher-tab" class="<?php echo $tab == 'pending' ? 'pending' : 'handled';?>"> 
    <!-- Nav tabs -->
    <ul id="tab_header" class="nav nav-tabs radial-out" role="tablist">
      <li role="presentation" class="col-xs-6"><a href="#pending" aria-controls="pending" role="tab" data-toggle="tab" onClick="showPending4Teacher()"><?php echo $Lang['eSchoolBus']['TeacherApp']['Tab']['Pending'];?></a></li>
      <li role="presentation" class="col-xs-6"><a href="#handled" aria-controls="handled" role="tab" data-toggle="tab" onClick="showHandled4Teacher()"><?php echo $Lang['eSchoolBus']['TeacherApp']['Tab']['Handled'];?></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content" id="tab-content" style="width:100%">
      
      <div role="tabpanel" class="tab-pane fade in <?php $tab == 'pending' ? ' active ' : '';?>" id="pending">
        <div id="main">         
          <div class="eventListArea">
            <ul class="eventList">
            	<div id="pendingList" class="pendingApplication col-xs-12 table-responsive">
                	<?php echo $linterface->getPendingApplicationForTeacher($offset,$numberOfRecord, $className='', $studentID='', $inputSearch, $isShowNoRecord);?>
                </div>
            </ul>
          </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane fade in <?php $tab == 'handled' ? ' active ' : '';?>" id='handled'>
		  <div class="main"> 
          <div class="eventListArea">
            <ul class="eventList">
            	<div id="applicationList" class="handledApplication col-xs-12 table-responsive">
               		<?php  echo $linterface->getHandledApplicationForTeacher($offset,$numberOfRecord, $className='', $studentID='', $inputSearch, $isShowNoRecord);?>
               	</div>
            </ul>
          </div>
        </div>
	  </div>
    </div>
  </div>
  
  
  <?php //if($allowAdd): ?> 
	<div id="addBtn" class="applyNew teacher" style="display:<?php echo $tab == 'handled' ? ' none ': '';?>;">
		<a href="?task=teacherApp.applyLeaveNew"><span class="addIcon"></span></a>
	</div>
  <?php //endif;?>   
  
</div>
         
</form>
</body>
</html>
