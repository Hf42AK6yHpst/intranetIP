<?php
/*
 *  Using:
 *  
 *  Purpose: interface for teacher/admin to handle applying leave of not taking school bus in a period 
 *
 *  2019-12-06 Cameron
 *      - disable submit button after click to avoid input duplicate record [case #Y176496]
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-03-07 Cameron
 *      - classSelection and studentSelection should be filter by taking bus students only [case #A156825]
 *        
 *  2019-01-30 Cameron
 *      - reason field is not mandatory
 *      - change word display for remark
 *      
 *  2019-01-28 Cameron
 *      - align from and to date
 *       
 *  2018-06-26 Cameron
 *      - create this file
 */

if ($indexVar) {
    $libdb = $indexVar['db'];
    $linterface = $indexVar['linterface'];
    $isSchoolBusAdmin = $indexVar['libSchoolBus']->isSchoolBusAdmin();
}
else {
    header("location: /");
}

if ($isSchoolBusAdmin) {
    $classSelection = $linterface->getTakingBusClassSelection("name='className' id='className' class='selectpicker'");
    $studentSelection = getSelectByArray(array(),"name='studentID' id='studentID' class='selectpicker'");
}
else {
    $studentAry = $libdb->getStudentsByClassTeacher($_SESSION['UserID']);
    $studentSelection = getSelectByArray($studentAry,"name='studentID' id='studentID' class='selectpicker'");
}

$today = date('Y-m-d');

include($PATH_WRT_ROOT."home/eClassApp/common/eSchoolBus/header.php");
?>

<script type="text/javascript">
jQuery(document).ready( function() {

	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';
	
	$('#teacherSubmitButton').click(function(e) {
		e.preventDefault();
        $('#teacherSubmitButton').attr('disabled', true);
		if (checkForm()) {
	        $.ajax({
				dataType: "json",
				type: "POST",
				url: '?task=teacherApp.ajax&action=checkApplication',
				data: $('#form1').serialize(),
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		if (ajaxReturn.html == '<?php echo $schoolBusConfig['ApplyLeaveError']['DuplicateApplication']; ?>') {
		        			alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['DuplicateApplication'];?>');
                            enableSubmitButton();
		        		} 
		        		else if (ajaxReturn.html == '<?php echo $schoolBusConfig['ApplyLeaveError']['InvalidTimeSlot']; ?>') {
		        			alert('<?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['InvalidTimeSlot'];?>');
                            enableSubmitButton();
		        		}
		        		else {
		        			var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmSubmit'];?>");
		        			if (isConfirmed) {
		            			$('#form1').attr('action','?task=teacherApp.applyLeaveNewUpdate');
		            			$('#form1').submit();
		        			}
		        			else {
                                enableSubmitButton();
                            }
		        		}
		        	}
		        	else {
                        enableSubmitButton();
                    }
		        },
				error: function(){
                    enableSubmitButton();
                    show_ajax_error();
                }
	        });
		}
		else {
            enableSubmitButton();
        }
	});

	$('#className').change(function(){
        isLoading = true;
		$('#studentNameSpan').html(loadingImg);
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?task=teacherApp.ajax',
			data : {
				'action': 'getStudentListByClass',
				'className': $('#className').val()
			},		  
			success: updateStudentList,
			error: show_ajax_error
		});
		
	});
	
});

$(function () {
  	$('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
  	});
});

function enableSubmitButton()
{
    $('#teacherSubmitButton').attr('disabled', false);
}

function checkForm()
{
	if($('#studentID').val() == '') {
		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['SelectStudent'];?>');
		form1.studentID.focus();
		return false;
	}

	if($('#startDate').val() == '') {
		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['InputStartDate'];?>');
		form1.startDate.focus();
		return false;
	}
	
	if($('#endDate').val() == '') {
		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['InputEndDate'];?>');
		form1.endDate.focus();
		return false;
	}
	
    if ($('#startDate').val() > $('#endDate').val()) {
		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['StartDateIsLaterThanEndDate'];?>');
		form1.startDate.focus();
		return false;
    }

    if ($('#startDate').val() == $('#endDate').val()) {
        if (($('#startDateType').val() == 'PM') && (($('#endDateType').val() == 'AM') || ($('#endDateType').val() == 'WD'))) { 
			window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['StartDateTimeSlotIsLaterThanEndDateTimeSlot'];?>');
			form1.startDateType.focus();
			return false;
        }
        else if (($('#startDateType').val() == 'WD') && ($('#endDateType').val() == 'AM')) {
			window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['StartDateTimeSlotIsLaterThanEndDateTimeSlot'];?>');
			form1.startDateType.focus();
			return false;
        }
    }

// 	if($('#reason').val() == '') {
//		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['InputLeaveReason'];?>');
// 		form1.reason.focus();
// 		return false;
// 	}
    
    return true;	
}

function updateStudentList(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#studentNameSpan').html(ajaxReturn.html);
		$('#selectStudentRow').css('display','');
		$('#studentID').selectpicker();
		isLoading = false;
	}
}

function show_ajax_error() 
{
	alert('<?php echo $Lang['eSchoolBus']['App']['Error']['Ajax'];?>');
}

</script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <div id="teacher-header" class="inner edit navbar-fixed-top">
    <div id="function"><a href="?task=teacherApp.index&tab=pending"><span class="backIcon"></span></a> <span class="text"><?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['Add'];?></span></div>
  </div>
  <div id="main" class="edit">
    <div class="main">
      <form class="applicationForm" name="form1" id="form1" method="post">

        <div id='selectStudentField' class="fieldTitle teacher">
        	<table width="100%">
			<?php if ($isSchoolBusAdmin):?>
				<tr>
					<td width="20%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['Header']['Menu']['Class'];?></span></td>
					<td width="80%"><span id="classNameSpan"><?php echo $classSelection;?></span></td>
				</tr>					
				<tr id="selectStudentRow" style="display:none;">				
			        <td width="20%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['Identity']['Student'];?></span></td>
			        <td width="80%"><span id="studentNameSpan"><?php echo $studentSelection;?></span></td>
			    </tr>
			<?php else:?>
				<tr>				
			        <td width="20%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['Identity']['Student'];?></span></td>
			        <td width="80%"><span id="studentNameSpan"><?php echo $studentSelection;?></span></td>
			    </tr>
			<?php endif;?>
            </table>
 		</div>

        <div id='selectDateField' class='fieldInput date'>
        	<table width="100%">
				<tr>
			        <td width="10%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['General']['From'];?></span></td>
			        <td width="65%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="startDate" name="startDate" value="<?php echo $today;?>" />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="20%">
                		<div>
            	          	<select id='startDateType' name='startDateType' class='selectpicker' title=''>
            	            	<option value="WD" selected><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
            	            	<option value="PM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
                
				<tr>
			        <td width="10%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['General']['To'];?></span></td>
			        <td width="65%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="endDate" name="endDate" value="<?php echo $today;?>" />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="20%">
                		<div>
            	          	<select id='endDateType' name='endDateType' class='selectpicker' title=''>
            	            	<option value="WD" selected><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
            	            	<option value="PM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
            </table>
 		</div>
     		
		<div class="fieldTitle"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Reason'];?></div>
		<div id="inputPartnerField" class="fieldInput infoMissing">
			<input type="text" class="form-control inputField teacher" id="reason" name="reason" placeholder="" value="">
		</div>		        
        
		<div class="fieldTitle"><?php echo $Lang['eSchoolBus']['TeacherApp']['Remark'];?></div>
		<div class="fieldInput infoMissing">
			<input type="text" class="form-control inputField teacher" id="remark" name="remark" placeholder="" value="">
		</div>		        
        
        <div class="mandatoryMsg"><?php echo $Lang['eSchoolBus']['App']['RequiredField'];?></div>
        <div class="submission-area">
          <button id="teacherSubmitButton" class="btn btn-blue" data-dismiss="modal" aria-label="Close"><?php echo $Lang['Btn']['Submit'];?></button>
        </div>
        
      </form>
    </div>
  </div>
</div>

</body>
</html>
