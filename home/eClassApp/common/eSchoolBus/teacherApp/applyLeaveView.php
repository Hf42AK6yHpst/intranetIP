<?php
/*
 *  Using:
 *  
 *  Purpose: interface for viewing apply leave details by class teacher / admin
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-10-14 Cameron
 *      - show record status by timeslot level,
 *      - allow admin to cancel the leave application by timeslot level ($timeSlotStatusLayout)
 *
 *  2019-01-28 Cameron
 *      - align from and to date
 *       
 *  2018-11-29 Cameron
 *      - allow teacher to cancel approved record
 *  
 *  2018-11-05 Cameron
 *      - fix: apply intranet_htmlspecialchars for text input field
 *      
 *  2018-06-22 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
    $linterface = $indexVar['linterface'];
    $isSchoolBusAdmin = $indexVar['libSchoolBus']->isSchoolBusAdmin();
}
else {
    header("location: /");
}
$today = date('Y-m-d');
$applicationID = $_GET['ApplicationID'];
$applicationIDAry = array($applicationID);
$applicationDetailAry = $ldb->getApplyLeaveRecord($applicationIDAry);

if (count($applicationDetailAry) && $applicationDetailAry[0]['RecordStatus'] == 2) {  // Record Status is 'confirmed', allow to cancel
    $isAllowedChange = true;
}
else {
    $isAllowedChange = false;
//     $isAllowedApplyLeaveAry = $ldb->isAllowedApplyLeave($applicationDetail['StudentID'],$applicationDetail['StartDate']);

//     if (($applicationDetail['StartDateType'] == 'AM' || $applicationDetail['StartDateType'] == 'WD' ) && $isAllowedApplyLeaveAry['gotoSchool']) {
//         $isAllowedChange = true;
//     }
//     else if ($applicationDetail['StartDateType'] == 'PM' && $isAllowedApplyLeaveAry['leaveSchool']) {
//         $isAllowedChange = true;
//     }
//     else {
//         $isAllowedChange = false;
//     }
}

$isStatusConsistent = $ldb->isRecordStatusConsistent($applicationID);

$timeSlotStatusLayout = '';
$atLeastOneStatusAllowToCancel = false;
//debug_pr($applicationDetailAry);
if (count($applicationDetailAry)) {
    $applicationDetail = $applicationDetailAry[0];
    $studentInfo = $applicationDetail['StudentName'] . ' ('.$applicationDetail['ClassName'].'-'.$applicationDetail['ClassNumber'].')';
    $timeSlotStatusAssoc = BuildMultiKeyAssoc($applicationDetailAry,array('LeaveDate','TimeSlot'),array('DetailRecordStatus', 'TimeSlotID'));
//debug_pr($timeSlotStatusAssoc);
//    debug_pr(count($timeSlotStatusAssoc));
    if (count($timeSlotStatusAssoc) > 1) {      // more than one day
//        debug_pr('true');
        $canbeCancelStatusAry = array(
            $schoolBusConfig['ApplyLeaveStatus']['Waiting'],
            $schoolBusConfig['ApplyLeaveStatus']['Confirmed']
        );

        $timeSlotStatusLayout = '<div class="fieldInput"><table class="table" width="100%">';
        $i = 0;
        foreach($timeSlotStatusAssoc as $_leaveDate=>$_timeSlotStatusAssoc) {
            foreach($_timeSlotStatusAssoc as $_timeSlot=>$__timeSlotStatusAssoc) {
//                $isAllowedChange = false;   // disable CancelApplyLeave for the ApplicationID
                if (($_timeSlot == 1) && $sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly']) {
                    continue;
                }
                else {
                    $timeSlotStatusLayout .= '<tr>';
                    $timeSlotStatusLayout .= '<td width="30%">' . $_leaveDate . '</td>';
                    if (!$sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly']) {
                        if ($_timeSlot == 1) {
                            $__timeSlotStr = $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];
                        } else {
                            $__timeSlotStr = $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];
                        }
                        $timeSlotStatusLayout .= '<td width="20%">' . $__timeSlotStr . '</td>';
                    }

                    $__recordStatus = $linterface->getLeaveApplicationStatusHistoryDisplay($__timeSlotStatusAssoc['DetailRecordStatus']);
                    $timeSlotStatusLayout .= '<td width="30%" class="recordStatus">' . $__recordStatus . '</td>';

                    if (in_array($__timeSlotStatusAssoc['DetailRecordStatus'], $canbeCancelStatusAry)) {
                        $atLeastOneStatusAllowToCancel = true;
                        if ($isSchoolBusAdmin) {
                            $timeSlotStatusLayout .= '<td width="20%"><button class="btn btn-danger delete-record-status cancelByTimeSlotID" data-dismiss="modal" aria-label="Cancel" data-TimeSlotID="' . $__timeSlotStatusAssoc['TimeSlotID'] . '">' . $Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplyLeave'] . '</button></td>';
                        }
                    }
                    $timeSlotStatusLayout .= '</tr>';
                }
            }
        }

        $timeSlotStatusLayout .= '</table></div>';
    }
}
else {
    header("location: ?task=teacherApp.index&tab=pending");
}
//debug_pr($timeSlotStatusLayout);

include($PATH_WRT_ROOT."home/eClassApp/common/eSchoolBus/header.php");
?>

<script type="text/javascript">
jQuery(document).ready( function() {
// 	$('#approveButton2').click(function(e) {
// 		e.preventDefault();
//		var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmApproval'];?>");
// 		if (isConfirmed) {
// 			$("[id$='Button2']").each(function(){
// 				console.log($(this));
// 				$(this).attr('disabled', true);
// 			});
// 			$('#form1').attr('action','?task=teacherApp.approveApplyLeave');
// 			$('#form1').submit();
// 		}
// 	});

	$('#cancelButton2').click(function(e) {
		e.preventDefault();
		var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmCancel'];?>");
		if (isConfirmed) {
			$("[id$='Button2']").each(function(){
//				console.log($(this));
				$(this).attr('disabled', true);
			});
			$('#form1').attr('action','?task=teacherApp.cancelApplyLeave');
			$('#form1').submit();
		}
	});

	$('.cancelByTimeSlotID').click(function(e){
        e.preventDefault();
        var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmCancel'];?>");
        if (isConfirmed) {
            var cancelBtn = $(this);
            $.ajax({
                dataType: "json",
                type: "POST",
                url: '?task=teacherApp.ajax',
                data : {
                    'TimeSlotID': cancelBtn.attr('data-TimeSlotID'),
                    'action': 'cancelApplicationByTimeSlotID'
                },
                success: function(ajaxReturn) {
                    if (ajaxReturn != null && ajaxReturn.success) {
                        cancelBtn.parent().parent().find('.recordStatus').html(ajaxReturn.html);
                        cancelBtn.hide();
                    }
                },
                error: show_ajax_error
            });


  //          console.log($(this));
            // $(this).attr('disabled', true);
            // $('#form1').attr('action','?task=teacherApp.cancelApplyLeave');
            // $('#form1').submit();
        }
    });
// 	$('#rejectButton2').click(function(e) {
// 		e.preventDefault();
//		var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmReject'];?>");
// 		if (isConfirmed) {
// 			$("[id$='Button2']").each(function(){
// 				console.log($(this));
// 				$(this).attr('disabled', true);
// 			});
// 			$('#form1').attr('action','?task=teacherApp.rejectApplyLeave');
// 			$('#form1').submit();
// 		}
// 	});
	
});

$(function () {
  	$('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
  	});
});

function show_ajax_error()
{
    alert('<?php echo $Lang['eSchoolBus']['App']['Error']['Ajax'];?>');
}

</script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <div id="teacher-header" class="inner edit navbar-fixed-top">
    <div id="function"><a href="?task=teacherApp.index&tab=handled"><span class="backIcon"></span></a> <span class="text"><?php echo $Lang['eSchoolBus']['TeacherApp']['Tab']['Handled'];?></span></div>
  </div>
  <div id="main" class="edit">
    <div class="main">
      <form class="applicationForm" name="form1" id="form1" method="post">

		<div id='studentInfo' class="fieldTitle teacher">
			<?php echo $studentInfo;?>
		</div>
		
        <div id='selectDateField' class='fieldInput teacher date'>
        	<table width="100%">
				<tr>
			        <td width="10%" class="fieldTitle teacher"><span><?php echo $Lang['General']['From'];?></span></td>
			        <td width="65%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="startDate" name="startDate" value="<?php echo $applicationDetail['StartDate'];?>" disabled />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="20%">
                		<div>
            	          	<select id='startDateType' name='startDateType' class='selectpicker' title='' disabled>
            	            	<option value="WD" <?php if ($applicationDetail['StartDateType'] == 'WD') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM" <?php if ($applicationDetail['StartDateType'] == 'AM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
            	            	<option value="PM" <?php if ($applicationDetail['StartDateType'] == 'PM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
                
				<tr>
			        <td width="10%" class="fieldTitle teacher"><span><?php echo $Lang['General']['To'];?></span></td>
			        <td width="65%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="endDate" name="endDate" value="<?php echo $applicationDetail['EndDate'];?>" disabled/>
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="20%">
                		<div>
            	          	<select id='endDateType' name='endDateType' class='selectpicker' title='' disabled>
            	            	<option value="WD" <?php if ($applicationDetail['EndDateType'] == 'WD') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM" <?php if ($applicationDetail['EndDateType'] == 'AM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
            	            	<option value="PM" <?php if ($applicationDetail['EndDateType'] == 'PM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
            </table>
 		</div>
     		
		<div class="fieldTitle teacher"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Reason'];?></div>
		<div class="fieldInput infoMissing">
			<input type="text" class="form-control inputField teacher" id="reason" name="reason" placeholder="" value="<?php echo intranet_htmlspecialchars($applicationDetail['Reason']);?>" disabled>
		</div>		        
        
		<div class="fieldTitle teacher"><?php echo $Lang['eSchoolBus']['TeacherApp']['Remark'];?></div>
		<div class="fieldInput infoMissing">
			<input type="text" class="form-control inputField teacher" id="remark" name="remark" placeholder="" value="<?php echo intranet_htmlspecialchars($applicationDetail['Remark']);?>">
		</div>

    <?php if ($timeSlotStatusLayout) { echo $timeSlotStatusLayout; } ?>

    <?php if ($isStatusConsistent && $atLeastOneStatusAllowToCancel):?>
        <div class="submission-area">
            <button id="cancelButton2" class="btn btn-danger" data-dismiss="modal" aria-label="Cancel" style="width:270px;"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['CancelAllApplyLeave'];?></button>
        </div>
    <?php endif;?>

    <?php if ($isAllowedChange && !$timeSlotStatusLayout):?>
        <div class="submission-area">
        	<button id="cancelButton2" class="btn btn-danger" data-dismiss="modal" aria-label="Cancel"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplyLeave'];?></button>
        </div>

    <?php endif;?>  
        <input type="hidden" name="ApplicationID" value="<?php echo $applicationID;?>" >
      </form>
    </div>
  </div>
</div>

</body>
</html>
