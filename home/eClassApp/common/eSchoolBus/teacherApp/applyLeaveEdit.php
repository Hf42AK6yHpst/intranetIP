<?php
/*
 *  Using:
 *  
 *  Purpose: interface for handling apply leave by class teacher / admin
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2018-11-05 Cameron
 *      - fix: apply intranet_htmlspecialchars for text input field
 *
 *  2019-01-28 Cameron
 *      - align from and to date
 *       
 *  2018-06-22 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$today = date('Y-m-d');
$applicationID = $_GET['ApplicationID'];
$applicationIDAry = array($applicationID);
$applicationDetailAry = $ldb->getApplyLeaveRecord($applicationIDAry);

if (count($applicationDetailAry)) {
    $applicationDetail = $applicationDetailAry[0];
    $studentInfo = $applicationDetail['StudentName'] . ' ('.$applicationDetail['ClassName'].'-'.$applicationDetail['ClassNumber'].')';
}
else {
    header("location: ?task=teacherApp.index&tab=pending");
}

if ($applicationDetail['RecordStatus'] != 1) {  // Record Status is not 'waiting approval', not allow to change
    $isAllowedChange = false;
}
else {
    $isAllowedChange = true;
//     $isAllowedApplyLeaveAry = $ldb->isAllowedApplyLeave($applicationDetail['StudentID'],$applicationDetail['StartDate']);
    
//     if (($applicationDetail['StartDateType'] == 'AM' || $applicationDetail['StartDateType'] == 'WD' ) && $isAllowedApplyLeaveAry['gotoSchool']) {
//         $isAllowedChange = true;
//     }
//     else if ($applicationDetail['StartDateType'] == 'PM' && $isAllowedApplyLeaveAry['leaveSchool']) {
//         $isAllowedChange = true;
//     }
//     else {
//         $isAllowedChange = false;
//     }
}

include($PATH_WRT_ROOT."home/eClassApp/common/eSchoolBus/header.php");
?>

<script type="text/javascript">
jQuery(document).ready( function() {
	$('#approveButton2').click(function(e) {
		e.preventDefault();
		var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmApproval'];?>");
		if (isConfirmed) {
			$("[id$='Button2']").each(function(){
				console.log($(this));
				$(this).attr('disabled', true);
			});
			$('#form1').attr('action','?task=teacherApp.approveApplyLeave');
			$('#form1').submit();
		}
	});

	$('#cancelButton2').click(function(e) {
		e.preventDefault();
		var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmCancel'];?>");
		if (isConfirmed) {
			$("[id$='Button2']").each(function(){
				console.log($(this));
				$(this).attr('disabled', true);
			});
			$('#form1').attr('action','?task=teacherApp.cancelApplyLeave');
			$('#form1').submit();
		}
	});

	$('#rejectButton2').click(function(e) {
		e.preventDefault();
		var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmReject'];?>");
		if (isConfirmed) {
			$("[id$='Button2']").each(function(){
				console.log($(this));
				$(this).attr('disabled', true);
			});
			$('#form1').attr('action','?task=teacherApp.rejectApplyLeave');
			$('#form1').submit();
		}
	});
	
});

$(function () {
  	$('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
  	});
});

</script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <div id="teacher-header" class="inner edit navbar-fixed-top">
    <div id="function"><a href="?task=teacherApp.index&tab=pending"><span class="backIcon"></span></a> <span class="text"><?php echo $Lang['eSchoolBus']['TeacherApp']['Tab']['Pending'];?></span></div>
  </div>
  <div id="main" class="edit">
    <div class="main">
      <form class="applicationForm" name="form1" id="form1" method="post">

		<div id='studentInfo' class="fieldTitle teacher">
			<?php echo $studentInfo;?>
		</div>
		
        <div id='selectDateField' class='fieldInput teacher date'>
        	<table width="100%">
				<tr>
			        <td width="10%" class="fieldTitle teacher"><span><?php echo $Lang['General']['From'];?></span></td>
			        <td width="65%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="startDate" name="startDate" value="<?php echo $applicationDetail['StartDate'];?>" disabled />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="20%">
                		<div>
            	          	<select id='startDateType' name='startDateType' class='selectpicker' title='' disabled>
            	            	<option value="WD" <?php if ($applicationDetail['StartDateType'] == 'WD') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM" <?php if ($applicationDetail['StartDateType'] == 'AM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
            	            	<option value="PM" <?php if ($applicationDetail['StartDateType'] == 'PM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
                
				<tr>
			        <td width="10%" class="fieldTitle teacher"><span><?php echo $Lang['General']['To'];?></span></td>
			        <td width="65%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="endDate" name="endDate" value="<?php echo $applicationDetail['EndDate'];?>" disabled/>
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="20%">
                		<div>
            	          	<select id='endDateType' name='endDateType' class='selectpicker' title='' disabled>
            	            	<option value="WD" <?php if ($applicationDetail['EndDateType'] == 'WD') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM" <?php if ($applicationDetail['EndDateType'] == 'AM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
            	            	<option value="PM" <?php if ($applicationDetail['EndDateType'] == 'PM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
            </table>
 		</div>
     		
		<div class="fieldTitle teacher"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Reason'];?></div>
		<div class="fieldInput infoMissing">
			<input type="text" class="form-control inputField teacher" id="reason" name="reason" placeholder="" value="<?php echo intranet_htmlspecialchars($applicationDetail['Reason']);?>" disabled>
		</div>		        
        
		<div class="fieldTitle teacher"><?php echo $Lang['eSchoolBus']['TeacherApp']['Remark'];?></div>
		<div class="fieldInput infoMissing">
			<input type="text" class="form-control inputField teacher" id="remark" name="remark" placeholder="" value="<?php echo intranet_htmlspecialchars($applicationDetail['Remark']);?>">
		</div>		        

    <?php if ($isAllowedChange):?>
        <div class="submission-area">
        	<button id="cancelButton2" class="btn btn-danger" data-dismiss="modal" aria-label="Cancel"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplyLeave'];?></button>
        	<button id="rejectButton2" class="btn btn-warning" data-dismiss="modal" aria-label="Reject"><?php echo $Lang['Btn']['Reject'];?></button>
          	<button id="approveButton2" class="btn" data-dismiss="modal" aria-label="Submit"><?php echo $Lang['Btn']['Approve'];?></button>
        </div>
    <?php endif;?>  
        <input type="hidden" name="ApplicationID" value="<?php echo $applicationID;?>" >
      </form>
    </div>
  </div>
</div>

</body>
</html>
