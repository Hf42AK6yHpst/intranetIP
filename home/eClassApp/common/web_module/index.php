<?php
$PATH_WRT_ROOT = "../../../../";
include_once '../../../../' . 'includes/global.php';
include_once '../../../../' . 'includes/libdb.php';
// include_once '../../../../'.'includes/libuser.php';

include_once '../../../../' . 'includes/eClassApp/libeClassApp_init.php';
include_once '../../../../' . 'includes/eClassApp/libeClassApp.php';

// App init
if (isset($parLang)) {
    $leClassApp_init = new libeClassApp_init();
    $intranet_session_language = $leClassApp_init->getPageLang($parLang);
    session_register_intranet("intranet_session_language", $intranet_session_language);
} else {
    $intranet_session_language = $_SESSION['intranet_session_language'];
}

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

if ($intranet_session_language == '') {
    $intranet_session_language = 'en';
}
if ($intranet_version == "2.0" || isset($junior_mck)) {
    include_once '../../../../' . 'lang/eclassapp_lang.' . $intranet_session_language . '.utf8.php';
    include_once '../../../../' . 'lang/lang.'.$intranet_session_language . '.utf8.php';
} else {
    include_once '../../../../' . 'lang/eclassapp_lang.' . $intranet_session_language . '.php';
}

if ($uid) {
    $_SESSION['UserID'] = $uid;
} else {
    $isTokenValid = true;
}
if (! $isTokenValid) {
    echo $i_general_no_access_right;
    exit();
}

// $http_host = $_SERVER["HTTP_HOST"];
// $req_uri = explode("?", $_SERVER["REQUEST_URI"]);
// $curr_domainpath = "http://" . str_replace("http://", "", $http_host) . $req_uri[0];

// Get Server Page URL
$curr_domainpath = curPageURL($withQueryString = false, $withPageSuffix = true);
$curr_domainpath = str_replace("/index.php", "/", $curr_domainpath);

// $userID = 1;
$token_key = "taist23657";
$token = md5($userID . " " . $token_key);
$time = time();

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>eClass App WebModule</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet"
	href="assets/lib/bootstrap-3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="assets/lib/bootstrap-3.3.7/css/bootstrap.flatly.min.css" />
<link rel="stylesheet" href="assets/js/themes/default.css" />
<link rel="stylesheet" href="assets/js/themes/default.date.css" />
<link rel="stylesheet" href="assets/js/jquery-clockpicker.min.css" />
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700"
	rel="stylesheet">

<link rel="stylesheet" href="assets/css/animate.css" />
<link rel="stylesheet" href="assets/css/vendor.css">
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/datatables.min.css">
<link rel="stylesheet" href="assets/css/materialize.min.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/custom.css">
<link rel="stylesheet" href="assets/css/style.css">
<script language="javascript">
		var initSvrLink = "<?php echo $curr_domainpath; ?>";
		var initID = "<?php echo $userID; ?>";
		var initToken = "<?php echo $token; ?>";
	</script>
<script language="javascript" type="text/javascript"
	src="assets/js/lang.<?=$_SESSION['intranet_session_language']?>.js?_=<?php echo $time; ?>"></script>
<script language="javascript" type="text/javascript"
	src="assets/lib/jquery-3.1.1.min.js"></script>
<script language="javascript" type="text/javascript"
	src="assets/lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script language="javascript" type="text/javascript"
	src="assets/js/datatables.min.js"></script>
<script language="javascript" type="text/javascript"
	src="assets/js/jquery.cookie.js"></script>
<script language="javascript" type="text/javascript"
	src="assets/js/materialize.min.js"></script>
<script language="javascript" type="text/javascript"
	src="assets/js/picker.js"></script>
<script language="javascript" type="text/javascript"
	src="assets/js/picker.date.js"></script>
<script language="javascript" type="text/javascript"
	src="assets/js/jquery-clockpicker.min.js"></script>
<script language="javascript" type="text/javascript"
	src="assets/js/app.js?_=<?php echo $time; ?>"></script>
<script language="javascript" type="text/javascript"
	src="assets/js/init.js?_=<?php echo $time; ?>"></script>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body class="loading">
	<div id="pageWrap">
		<div class="fiuld-container">
			<div class="row">
				<div class="col-xs-12 pageHldrWrap">
					<div class="pageHldr"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="myModal" style="bottom: auto;">
		<div class="modal-content">
			<h4 id='modal_header'><?=$Lang['eClassApp']['WebModules']['DeleteConfirm']?></h4>
			<div id='modal_content'>
				<p><?=$Lang['eClassApp']['WebModules']['DeleteConfirm2']?></p>
				<br>
				<br> <a class="modal-action waves-effect btn btn-sm red darken-1"
					value='yes'><?=$Lang['eClassApp']['WebModules']['Delete']?></a> <a
					class="modal-action modal-close waves-effect btn btn-sm grey lighten-1"
					value='no'><?=$Lang['eClassApp']['WebModules']['Close']?></a>
			</div>
		</div>
	</div>
	<div class="loadingEm">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			height="90%">
			<tr>
				<td valign="middle" class="text-center">
					<div class="loading_con">
						<div class="circle circle_ani1 color1"></div>
						<div class="circle circle_ani2 color1"></div>
						<div class="circle circle_ani3 color2"></div>
						<div class="circle circle_ani4 color2"></div>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>