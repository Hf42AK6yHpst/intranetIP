<?php
global $Lang;
ob_start();
$data = $this->infoArr['postData']['formData'];
#BookingDatails
$BookingDatails = $this->outputJSON($this->infoArr["api"]['data']['BookingDetails'],false);

?>
<div class="row header">
	<div class="col-xs-12">
		<h3 class="text-info">
			<div class="pull-left">
				<a href="#" rel='<?php echo $viewData["controllerLink"]; ?>/list' class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span></a>
			</div>
			<nobr><?=$Lang['eBooking']['App']['FieldTitle']['NewBooking']?></nobr>
		</h3>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<ol class="breadcrumb">
			<li class="active"><?=str_replace('<--Step Num-->', '3', $Lang['eBooking']['App']['FieldTitle']['Step'])?><?=$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingTime']?></li>
		</ol>
		<div class="card active">
			<div class="card-content teal lighten-5">
				<form class="col s12" id='new_step3bb' rel='<?php echo $viewData["controllerLink"]; ?>/new_step4' data-success='<?php echo $viewData["controllerLink"]; ?>/new_step4'>
					 <?=$this->infoArr['formSelectorHTML']?>
					 <div class="col-xs-12">
					 	<div class="btn-group-justified">
							<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel="<?php echo $viewData["controllerLink"]; ?>/new_step2bb1"><?=$Lang['eBooking']['App']['Button']['Cancel']?></a>
							<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="new_step3bb" name="action"><?=$Lang['eBooking']['App']['Button']['Continue']?><span class="glyphicon glyphicon-chevron-right"></span> </a> 
						</div>
					</div>
					<input type="hidden" name="BookingDetails" id="BookingDetails" value='<?=$BookingDatails?>'>
					<input type="hidden" name="roomID" id="roomID" value='<?=$data['roomID']?>'>
					<input type="hidden" name="SubmitFrom" id="SubmitFrom" value="step3bb">
				</form>
				<div class='clearfix'></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$.fn.bindFirst = function(name, fn) {
	  var elem, handlers, i, _len;
	  this.bind(name, fn);
	  for (i = 0, _len = this.length; i < _len; i++) {
	    elem = this[i];
	    handlers = jQuery._data(elem).events[name.split('.')[0]];
	    handlers.unshift(handlers.pop());
	  }
	};
	
$( document ).ready(function(){
	//function to earse the unclick booking details
	$('.submitBtn').bindFirst("click",function(){
		var BookingDatails = JSON.parse($('#BookingDetails').val());
//		for (var i=0; i<BookingDatails.length; i++){
		for (var index in BookingDatails){
			if($('#timeslot').val().indexOf(index) == -1){
//				BookingDatails.splice(i, 1);
				delete BookingDatails[index];
			}
		}
		$('#BookingDetails').val( JSON.stringify(BookingDatails) );
	});
});
</script>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>