<?php
/*
 *      2019-06-19 Cameron
 *          - fix $displayTitle and $displayStep for edit
 *          
 *      2019-01-24 Cameron
 *          fix: send email function [case #X155789]
 *          - add form 
 *          - change serializeArray() to serialize() in $.ajax
 *          - add parameter id and token in $.ajax
 */
ob_start();
global $Lang;

$data = $this->infoArr['postData']['formData'];
if ($data['SubmitFrom'] == 'edit') {
    $displayTitle = $Lang['eBooking']['App']['FieldTitle']['EditBooking'];
    $displayStep = str_replace('<--Step Num-->', '2', $Lang['eBooking']['App']['FieldTitle']['Step'] );
}
else {
    $displayTitle = $Lang['eBooking']['App']['FieldTitle']['NewBooking'];
    $displayStep = str_replace('<--Step Num-->', '4', $Lang['eBooking']['App']['FieldTitle']['Step'] );
}
$Status = $this->infoArr["api"]['respond'][0]['Status']? 'Success':'Fail';
$ErrorCode = $this->infoArr["api"]['respond'][0]['Error'][0];

$ApiData = $this->infoArr["api"];
//get
$ResultHTML = '';
//objectDetail
$ResultHTML .= '<div class="table-header">';
$comma = '';
foreach ((array)$ApiData['objectDetail'] as $_objectDetail){
	$ResultHTML .= $comma.Get_Lang_Selection($_objectDetail['NameChi'], $_objectDetail['NameEng']);
	$comma = ',';
}
$ResultHTML .= '</div>';

$ResultHTML .= '<form class="col s12" id="resultForm">';
$ResultHTML .= '<table>';
	$ResultHTML .= '<thead>';
		$ResultHTML .= '<tr>';
			$ResultHTML .= '<th>'.$Lang['eBooking']['App']['FieldTitle']['BookingDate'] .'</th>';
			$ResultHTML .= '<th>'.$Lang['eBooking']['App']['FieldTitle']['StartTime'].'</th>';
			$ResultHTML .= '<th>'.$Lang['eBooking']['App']['FieldTitle']['EndTime'].'</th>';
			$ResultHTML .= '<th>'.$Lang['eBooking']['App']['FieldTitle']['Status'].'</th>';
		$ResultHTML .= '</tr>';
	$ResultHTML .= '</thead>';

	$ResultHTML .= '<tbody>';
	foreach ((array)$ApiData['Request']['BookingDetails'] as $indexOfBooking => $_bookingDetails){
			//booking info
		$ResultHTML .= '<tr>';
			$ResultHTML .= '<td>'.$_bookingDetails['Date'].'</td>';
			$ResultHTML .= '<td>'.date('H:i',strtotime($_bookingDetails['StartTime'])).'</td>';
			$ResultHTML .= '<td>'.date('H:i',strtotime($_bookingDetails['EndTime']) ).'</td>';
			$ResultHTML .= '<td>';
				//booking status
				$Status = $ApiData['respond'][$indexOfBooking]['Status']? 'Success':'Fail';
				$ResultHTML .= $Lang['eBooking']['App']['Result'][$Status];
				//error, if any
				$ErrorCode = $ApiData['respond'][$indexOfBooking]['Error'][0];
				if($ErrorCode){
					$ResultHTML .='<br>'.$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['RejectReason'].':'.$Lang['eBooking']['App']['FieldTitle']['ErrorCode'][$ErrorCode];
				}
				if($ApiData['respond'][$indexOfBooking]['emailBookingIDArr']){
					$ResultHTML .= '<input type="hidden" name="emailBookingIDArr['.$indexOfBooking.']" value="'.$ApiData['respond'][$indexOfBooking]['emailBookingIDArr'].'">';
				}
				if($ApiData['respond'][$indexOfBooking]['emailApprovedIDArr']){
					$ResultHTML .= '<input type="hidden" name="emailApprovedIDArr['.$indexOfBooking.']" value="'.$ApiData['respond'][$indexOfBooking]['emailApprovedIDArr'].'">';
				}
			$ResultHTML .= '</td>';
		$ResultHTML .= '</tr>';
	}
	$ResultHTML .= '</tbody>';
$ResultHTML .= '</table>';
$ResultHTML .= '</form>';

?>
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel='<?php echo $viewData["controllerLink"]; ?>/list' class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</div>
				<nobr><?=$displayTitle?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="active"><?=$displayStep?> <?=$Lang['eBooking']['App']['FieldTitle']['Finish']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<?=$ResultHTML?>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="btn-group-justified">
			<a class='ajlink btn btn-sm waves-effect waves-orange' type="submit" rel="<?php echo $viewData["controllerLink"]; ?>/list" name="action"><?=$Lang['eBooking']['App']['FieldTitle']['Finish']?></a> 
		</div>
	</div>
<script type="text/javascript">
$(document).ready(function(){
	$.ajax({
		url: pageJS.vrs.url + "?page=" + "<?=$viewData["controllerLink"]; ?>"+"/requestSendingEmail",
		timeout: 0,
		type: "POST",
		data:  $("#resultForm").serialize() + '&id=' + initID + '&token=' + initToken
	});
});
</script>
<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>