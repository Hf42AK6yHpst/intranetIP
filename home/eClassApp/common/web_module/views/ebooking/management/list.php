<?php
/*
 *  2019-01-07 Cameron
 *      - don't show delete button if the booking status is 1 (approved)
 */
ob_start();
global $Lang;
?>
<div class="fiuld-container cusView">
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-right">
					<?php if($this->infoArr['postData']['formData']['eBookingSetting']['AllowBookingStatus']!='2'){?>
					<a href="#" rel="<?=$viewData["controllerLink"]; ?>/new_step1" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-plus"></span></a>
					<?php }?>
					<a href="#" class="filterBTN btn btn-sm white-text"><span class="glyphicon glyphicon-filter"></span></a>
				</div>
				<nobr><?=$Lang['eBooking']['App']['FieldTitle']['MyBooking']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="card hidden" id="filter">
				<div class="card-content grey lighten-4">
					<form id="filterForm" rel="<?=$viewData["controllerLink"]; ?>/list" data-success="<?=$viewData["controllerLink"]; ?>/list" >
						<div class="input-field col col-xs-12 col-sm-6">
							<input id="startDate" name="startDate" class="datepicker" value="<?=date('Y/m/d')?>">
							<label for="startDate" class='active'><?=$Lang['eBooking']['App']['FieldTitle']['StartDate']?></label>
						</div>
						<div class="input-field col col-xs-12 col-sm-6">
							<input id="endDate" name="endDate" class="datepicker" value="">
							<label for="endDate" class='active'><?=$Lang['eBooking']['App']['FieldTitle']['EndDate']?></label>
						</div>
						<div class="text-right col-xs-12">
							<a href="#" rel="ebooking/management/actionBtn" class="btn btn-sm teal lighten-1 white-text actionBtn" data-action="filter"><?=$Lang['Btn']['Submit']?></a>
							<a href="#" rel="filter" class="boxlink btn btn-sm red darken-1 white-text"><?=$Lang['Btn']['Cancel']?></a>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
			<table id="ebookingTable" class="dTtable table table-striped table-hover nowrap" cellspacing="0" width="100%">
			<thead>
				<tr class=''>
					<th class='text-center'><?=$Lang['eBooking']['App']['FieldTitle']['Room'].'/'.$Lang['eBooking']['App']['FieldTitle']['Item']?></th>
					<th class='text-center'><?=$Lang['eBooking']['App']['FieldTitle']['BookingDate']?></th>
					<th class='text-center'><?=$Lang['eBooking']['App']['FieldTitle']['Status']?></th>
					<th class='text-center'><?=$Lang['eBooking']['App']['FieldTitle']['BookedBy'] ?></th>
					<th class='text-center'><?=$Lang['eBooking']['App']['FieldTitle']['Remarks'] ?></th>
					<th class='text-center'><?=$Lang['eBooking']['App']['FieldTitle']['Action']?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
	                <th>Room</th>
	                <th>BookingDate</th>
	                <th>Status</th>
	                <th>BookedBy</th>
	                <th>remarks</th>
	                <th>action</th>
	            </tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
<script>
var ebookingtable = "";
$(document).ready(function() {
	ebookingtable = $('#ebookingTable').DataTable( { 
		 "dom": '<"top"f>rt<"bottom"l>p<"clear">',
		 //"dom": '<"top"f>rt<"bottom"l>ip<"clear">',
    	 "language": {
    		 "lengthMenu": '<?php echo $Lang['eBooking']['App']['DataTable']['RecordPerPage']?>',
             "zeroRecords": '<?php echo $Lang['eBooking']['App']['DataTable']['DataNotFound']?>',
             "info": '<?php echo $Lang['eBooking']['App']['DataTable']['PageInfo']?>',
             "infoEmpty": '<?php echo $Lang['eBooking']['App']['DataTable']['EmptyPageInfo']?>',
             "infoFiltered": '<?php echo $Lang['eBooking']['App']['DataTable']['PageInfoFilter']?>',
             "search": '<?php echo $Lang['eBooking']['App']['DataTable']['Search']?>',
             "paginate": {
                 "next":       '<?php echo $Lang['eBooking']['App']['DataTable']['Next']?>',
                 "previous":   '<?php echo $Lang['eBooking']['App']['DataTable']['Previous']?>' 
             },
             "loadingRecords": '<?php echo $Lang['eBooking']['App']['DataTable']['Loading']?>',
             "processing": '<?php echo $Lang['eBooking']['App']['DataTable']['Processing']?>'
         },
         processing: true,
         responsive: true,
         bStateSave: true,
         columns :[
        	 {"data": "room"},
        	 {"data": "datetime"},
        	 {"data": "status"},
        	 {"data": "bookingBy"},
        	 {"data": "remarks"},
        	 {"data": "bookingID"},
        	
         ],
         columnDefs: [
                        {className: "dt-body-middle", "targets": [ 0 ]},
                        {className: "dt-body-center dt-body-middle", "targets": [  1, 2, 3, 4, 5  ]},
                        {
                        	"targets": 2,
                        	"render": function ( data, type, row ) {
		                            	switch(data){
			                            	case '0':
			                                	return '<img src="/home/eClassApp/common/web_module/assets/img/icon_waiting.png" width="20px">';
			                                	break;
			                            	case '1':
			                                	return '<img src="/home/eClassApp/common/web_module/assets/img/icon_approve.png" width="20px">';
			                                	break;
			                            	case '-1':
			                            		return '<img src="/home/eClassApp/common/web_module/assets/img/icon_reject.png" width="20px">';
			                                	break;
		                            	}
	                            	}
                        },
                        {
                        	"targets": 5,
                        	"render": function ( data, type, row ) {
                            		var x;
                            		
	                            	x = '<td class="text-center" style="display: none;">';
	                            	x += '<a rel="ebooking/management/edit&bookingid='+data+'" title="Edit" class="ajlink" ><img src="/home/eClassApp/common/web_module/assets/img/icon_edit.png" width="20px"></a> ';
									if (row['status'] != 1) {
	            						x += '<a rel="ebooking/management/actionBtn" title="Delete" class="actionBtn" data-bookingID="'+data+'" data-action="delete"><img src="/home/eClassApp/common/web_module/assets/img/icon_trash.png" width="20px"></a>';
									}
	            					x += '</td>';
	            					return x;
                            		}
                        },
                   ],
         ajax: {
            "url" : pageJS.vrs.url + '?page=ebooking/management/mydata',
            "type" : "POST",
            "data" : function(){ 
            	var serialized = $('#filterForm').serializeArray();
		        var ajaxData = JSON.stringify(serialized);
				var ajax_submitdata = pageJS.vrs.data;
				ajax_submitdata.ajaxData = ajaxData
				ajax_submitdata.srhtxt = "formSubmit";	
                return ajax_submitdata;
                },
            "complete" : function(response){
                //
            	if ($("i.view").length > 0) {
            		$("i.view").each(function() {
            			$(this).on("click", function(e) {
            				alert( "The target is " + $(this).attr('target'));				
            			});
            		});
            	}
            	//bind the listen again
            	pageJS.cfn.bindListener();
            }
         },
    } );

	ebookingtable.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
		pageJS.cfn.bindListener();
	} );
} );
</script>
<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>