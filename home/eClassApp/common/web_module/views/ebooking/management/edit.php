<?php
/*
 *  2019-06-19 Cameron
 *      - change $Lang['eBooking']['App']['Button']['Cancel'] to $Lang['eBooking']['App']['Button']['RealCancel']
 *      - change $displayStep
 *  
 *  2019-03-15 Cameron
 *      - fix: should display step title according to booking type
 */

ob_start();
global $Lang;

$data = $this->infoArr['postData']['formData'];

// if ($data['bookingType'] == 'item') {
//     $displayStep = $Lang['eBooking']['App']['FieldTitle']['ComfirmBookingItem'];
// }
// else {
//     $displayStep = $Lang['eBooking']['App']['FieldTitle']['ComfirmBookingRoom'];
// }
$displayStep = $Lang['eBooking']['App']['FieldTitle']['EditRemark'];

$BookingDatails = $this->outputJSON($this->infoArr["api"]['data']['BookingDetails'],false);
?>
<style>
.dropdown-content{
	max-height: 400px;
}
</style>
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel='<?php echo $viewData["controllerLink"]; ?>/list' class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</div>
				<nobr><?=$Lang['eBooking']['App']['FieldTitle']['EditBooking']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><a href="#" class="ajlink" rel="<?php echo $viewData["controllerLink"]; ?>/list"><?=$Lang['eBooking']['App']['FieldTitle']['eBooking']?></a></li>
				<!--
				<li><a href="#" class="ajlink" rel="<?php echo $viewData["controllerLink"]; ?>/new_step1"><?=$Lang['eBooking']['App']['FieldTitle']['EditBooking'] ?></a></li>
				-->
				<li class="active"><?=$Lang['eBooking']['App']['FieldTitle']['EditBooking'] ?></li>
				<li class="active"><?=str_replace('<--Step Num-->', '1', $Lang['eBooking']['App']['FieldTitle']['Step'])?> <?=$displayStep?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id="step3a_form" rel="<?php echo $viewData["controllerLink"]; ?>/new_step4" data-success="<?php echo $viewData["controllerLink"]; ?>/new_step4">
					<?= $this->infoArr['formSelectorHTML']?>
					<div class="col-xs-12">
						<div class="btn-group-justified">
							<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel="<?php echo $viewData["controllerLink"]; ?>/list"><?=$Lang['eBooking']['App']['Button']['Back']?></a>
							<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="step3a_form" name="action"><?=$Lang['eBooking']['App']['Button']['Continue']?> <span class="glyphicon glyphicon-chevron-right"></span> </a>
						</div>
					</div>
					<div class="input-field col s12 row">
						<input type="hidden" name="BookingDetails" id="BookingDetails" value='<?=$BookingDatails?>'>
						<input type="hidden" name="SubmitFrom" id="SubmitFrom" value="edit">
						<input type="hidden" name="roomID" id="roomID" value='<?=$data['roomID']?>'>
						<input type="hidden" name="bookingID" id="bookingID" value='<?=$data['bookingID']?>'>
						<?php foreach ((array)$data['itemID'] as $_itemID){?>
							<input type="hidden" name="itemID[]" id="itemID" value='<?=$_itemID?>'>
						<?php }?>
					</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
$(document).ready(function(){
	  Materialize.updateTextFields();
});
</script>
<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>