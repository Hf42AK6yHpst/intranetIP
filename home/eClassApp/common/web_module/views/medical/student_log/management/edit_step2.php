<?php
/*
 * // Using: 
 * 
 * 2018-04-30 Cameron
 * - fix Lang of Remarks for ej
 *
 * 2018-02-28 Cameron
 * - hide Add Body Part button if level2 item is not require to show body parts
 *
 */
ob_start();
// debug_pr($this->infoArr);

global $Lang, $image_path;
global $medical_cfg, $plugin;

$isShowAddBodyPart = ($plugin['medical_module']['AlwaysShowBodyPart']) ? '' : 'display:none;';
?>

<div class="fiuld-container cusView">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<!--<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$Lang['medical']['menu']['studentLog']?></a></li>-->
				<li class="active"><?=$Lang['medical']['menu']['studentLog']?></li>
				<li class="active"><?=$Lang['medical']['general']['new']?></li>
				<li class="active"><?=$Lang['General']['Steps']?>2: <?=$Lang['medical']['general']['new']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id='step2_form'
						rel="<?=$viewData["controllerLink"]?>/edit_step3"
						data-success="<?=$viewData["controllerLink"]?>/mylist&Success=1"
						enctype="multipart/form-data">
						<div class="row">
							<ul class="collection with-header">
								<li class="collection-header"><h6><?=$this->infoArr["UIContent"]["SummaryTable"]?></h6></li>
							</ul>
						</div>
						<div class="row">
							<div class="input-field col col-xs-12 col-sm-6">
								<input id="date" name="date" class="datepicker"
									value="<?=date("Y-m-d")?>"> <label for="date" class="active"><?=$Lang['General']['Date']?></label>
							</div>

							<div class="input-field col col-xs-12 col-sm-6">
								<input type="text" name="startTime" id="startTime"
									class="req-fromtime timepicker form-control"
									value="<?=$this->infoArr["UIContent"]["StudentLogTime"]?>"> <label
									for="time" class="active"><?=$Lang['medical']['studentLog']['time']?></label>
							</div>

							<div class="input-field col col-xs-8 col-sm-4">
								<div class="select-wrapper">
									<?=$this->infoArr["UIContent"]["StudentLogLevel1Select"]?>
								</div>
								<label><?=$Lang['medical']['studentLog']['behaviour']?></label>
							</div>

							<div class="input-field col col-xs-4 col-sm-2">
								<div class="select-wrapper">
									<?=$this->infoArr["UIContent"]["StudentLogLevel2Select"]?>
								</div>
							</div>

							<span id="bodyPartsDiv" style="<?php echo $isShowAddBodyPart;?>">
								<div class="col col-xs-12 col-sm-12">
									<a href="#" id="AddBodyPart"
										class="ajlink btn btn-sm white-text"><span
										class="glyphicon glyphicon-plus"></span> (<?=$Lang['medical']['studentLog']['bodyParts']?>) <span
										class="hidden-xs"> </span></a>
								</div>
							</span>

							<div class="input-field col col-xs-4 col-sm-2">
								<?=$this->infoArr["UIContent"]["StudentLogLastedMinuteSelect"]?>
								<label><?=$Lang['medical']['studentLog']['timelasted']?></label>
							</div>
							<div class="input-field col-xs-2 col-sm-1"
								style="margin-top: 35px; text-align: left;">
								<span><?=$Lang['medical']['studentLog']['minute']?></span>
							</div>
							<div class="input-field col col-xs-4 col-sm-2">
								<?=$this->infoArr["UIContent"]["StudentLogLastedSecondSelect"]?>
							</div>
							<div class="input-field col-xs-2 col-sm-1"
								style="margin-top: 35px; text-align: left;">
								<span><?=$Lang['medical']['studentLog']['second']?></span>
							</div>

							<div class="col col-xs-12 col-sm-6" style="margin-top: 5px;">
								<img src="<?=$image_path?>/icon/attachment_blue.gif" border=0 />
								<input type="file" id="FileUploaded[]"
									name="event[n1][fileUploaded][]" onChange="changeFile()"
									style="text-overflow: ellipsis; max-width: 220px; margin-top: -20px; margin-left: 15px;"
									multiple />
								<div id="ShowAttachments"></div>
								<div class='form_sep_title'><?=$Lang['medical']['studentLog']['attachmentRemarks']?></div>
							</div>

							<!--
							<div class="col col-xs-12 col-sm-12 file-field input-field">
						      <div class="btn">
						        <span>File</span>
						        <input type="file" name="FileUploaded" id="FileUploaded" multiple>
						      </div>
						      <div class="file-path-wrapper">
						        <input class="file-path validate" type="text" placeholder="Upload one or more files">
						      </div>
						    </div>
-->
							<div class="input-field col col-xs-12 col-sm-6">
								<?=$this->infoArr["UIContent"]["StudentLogDefaultRemarks"]?>
								<div class="remark_top">
									<label class="remark_title"><a href="#"
										onClick="toggleDefaultRemarks()"><img
											src="/images/icon/preset_option.gif" id="posimg" border="0"
											alt="<?=$Lang['Btn']['Select']?>">&nbsp;<?=$Lang['medical']['App']['CopyDefaultRemarks']?></a></label>
								</div>
							</div>

							<div class="input-field col col-xs-12 col-sm-12">
								<textarea id="remarks" name="remarks"
									class="materialize-textarea" data-length="120"></textarea>
								<label id="remarks_label" for="remarks" class="remark_title"><?=$Lang['medical']['App']['Remark']?></label>
							</div>
							
							<? if ($this->infoArr["PageData"]["HiddenPICSelect"]=="") { ?>
								<div class="input-field col col-xs-12 col-sm-6"
								id="dropdown_opt" rel="sld_student">
								<div class="select-wrapper"></div>
								<input type="text" id="autocomplete-teacher-name"
									rel='dropdown_opt' class="autocomplete"> <label
									for="autocomplete-name" class="remark_title"><?=$Lang['medical']['App']['SearchPICsUsingName']?></label>
							</div>
							<? } ?>
							<div class="input-field col col-xs-12 col-sm-6">
								<select multiple class="required" id="sld_student" name="pics[]"
									readyonly>
									<option value="" disabled selected><?=$Lang['medical']['App']['PleaseSearch']?></option>
									<?=$this->infoArr["UIContent"]["PICDefultOption"]?>
								</select> <label><?=$Lang['medical']['App']['SelectedPICs']?></label>
							</div>

							<div class="input-field col col-xs-12 col-sm-12"
								style="margin-bottom: 1rem">
								<input type='checkbox' name="email2pic" id="email2pic" value='1'>
								<label for="email2pic"><?=$Lang['medical']['studentLog']['email2pic']?></label>
							</div>

							<div class="col-xs-12 text-right">
								<div class="btn-group-justified">
									<a
										class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn'
										rel="<?=$viewData["controllerLink"]?>/edit"><?=$Lang['Btn']['Back']?></a>
									<a class='btn btn-sm waves-effect waves-orange submitFileBtn'
										type="submit" rel="step2_form" id="AddSubmit" name="action"><?=$Lang['medical']['general']['new']?></a>
								</div>
							</div>
							<div class="input-field col s12 row">
								<?=$this->infoArr["UIContent"]["HiddenInputField"]?>
							</div>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.default_remarks {
	display: none;
}
</style>
<script>
var files = "";

function toggleDefaultRemarks(){
	$('#default_remarks').toggleClass('default_remarks');
}

function SetRemarks(obj){
	var new_val = $(obj).text();
	var org_val = $('#remarks').val();
	if (org_val.indexOf(new_val) == -1) {
		org_val = org_val + ((org_val != '') ? '\n' : '') + new_val;
		$('#remarks').val(org_val);
		$('#remarks').focus(); 
		$('#remarks_label').css('margin-top','-10px');
	} 
	toggleDefaultRemarks();
}

$(document).ready(function(){
	var nrBodyPart = 0;
	$('#AddBodyPart').on('click',function(){
		nrBodyPart = $('#NrItem').val();
		var labelCount = $("label[id^='LabelItemNr_n']").length + 1;

		$.ajax({
			dataType: "json",
			type: "POST",
			url: pageJS.vrs.url + '?page=medical/student_log/management/get_body_part',
			data: { id: initID, token: initToken, itemNr: nrBodyPart, labelNr: labelCount},		  
			success: function(ajaxReturn){
				if (ajaxReturn.resultData.length > 0) {
					$('#AddBodyPart').parent().before(ajaxReturn.resultData);
					$('#event_level3_select\\[n'+nrBodyPart+'\\]').material_select();
					$('#event_level4_list\\[n'+nrBodyPart+'\\]').material_select();
					
					if ($('select.select_ajaxloadlist').length > 0) {
						if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.init >> select.select_reloadlist", "info");
						$('select.select_ajaxloadlist').on('change', formJS.ltr.autoAjaxListContent);
					}
					$('#NrItem').val(parseInt($('#NrItem').val()) + 1);
				}
			},
			error: function(){
				console.log('Error on Add Body Part');
			}
		});
	});

	$('#event_level2_select').on('change',function(){
		if(($(this).find('option:selected').html()=='<?php echo $medical_cfg['studentLog']['level2']['specialStatus']?>') || (<?=intval($plugin['medical_module']['AlwaysShowBodyPart'])?> > 0)){
			$('#bodyPartsDiv').css('display','');			
			$('#bodyPartsDiv').show();
		}
		else{
			$('#bodyPartsDiv').css('display','none');
			$('#bodyPartsDiv').hide();
		}
	});

	$('#AddSubmit').on('click',uploadFiles);

	
});

function changeFile(e) {
	e = e || window.event;
	files = e.target.files;
	var x = '';
	var input = $("#FileUploaded\\[\\]")[0];
	
	x = '<ul>';
  	for (var i = 0; i < input.files.length; ++i) {
    	x += '<li style="word-break:break-all;">' + input.files.item(i).name + '</li>';
  	}
  	x += '</ul>';
  	$('#ShowAttachments').html(x);
}

function DeleteNewItem(itemNr) {
//	var labelCount = $("label[id^='LabelItemNr_n']").length;
//	var j;
	
	$('#dropdown_opt3\\[n'+itemNr+'\\]').remove();
	$('#del_dropdown_opt3\\[n'+itemNr+'\\]').remove();
	$('#event_level4_div_n'+itemNr).remove();
	
	ReorderItem();
}

// re-order label after delete new item
function ReorderItem(){
	var labelCount = $("label[id^='LabelItemNr_n']").length + 1;	// number of labels before delete
	var start, i, j;
	for(i=1; i<=labelCount; i++) {
		if (typeof($('#LabelItemNr_n'+i).attr('id')) == 'undefined') {
			start = i;
			break;
		}
	}
	if (start != labelCount) {
		for(i=start; i<labelCount; i++) {
			j = i + 1;
			$('#LabelItemNr_n'+j).attr('id','LabelItemNr_n'+i).html("<?=$Lang['medical']['studentLog']['bodyParts']?> "+i);
		}
	}
}


// Catch the form submit and upload the files
function uploadFiles(e) {
	$('#AddSubmit').attr('disabled', 'disabled');
	e.stopPropagation();	// Stop stuff happening
	e.preventDefault();		// Totally stop stuff happening
	
	var maxAttachment = 6;
	var fileMaxSize = 1024 * 1024 * maxAttachment;
	var fileSizePass = true;

	// START A LOADING SPINNER HERE
	Materialize.toast("<?=$Lang['General']['Loading']?>", 4000);

	if (files.length > maxAttachment) {
		$('.toast').remove();
		Materialize.toast("<?=$Lang['medical']['general']['Hint']['MaxAttachment']?>", 4000);
		$('#AddSubmit').removeAttr('disabled');
	}
	else {
	    // Create a formdata object and add the files
	    var data = new FormData();
	    $.each(files, function(key, value) {
	    	if($(this)[0].size > fileMaxSize){
	    		$('.toast').remove();
				Materialize.toast("<?=$Lang['medical']['general']['AlertMessage']['MaxAttachmentError']?>" , 4000);
				$('#AddSubmit').removeAttr('disabled');
				fileSizePass = false;
				return false;	    		
	    	}
	        data.append(key, value);
	   	});

	   	if (fileSizePass == true) {
		    data.append('id',initID);			// must pass this para to api.php
		    data.append('token',initToken);		// must pass this para to api.php

		    $.ajax({
		        url: pageJS.vrs.url + '?page=medical/student_log/management/get_uploaded_files',
		        type: 'POST',
		        data: data,
		        cache: false,
		        dataType: '',		// can't set this type to json, don't know why
		        processData: false, // Don't process the files
		        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
		        success: function(data, textStatus, jqXHR)
		        {
		            if(typeof data.error === 'undefined')
		            {
		                submitForm(e, data);
		            }
		            else
		            {
		                Materialize.toast(_langtxt['medical']["error"][data.error] , 4000);
	    	        }
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		            Materialize.toast(_langtxt["errorToLoadThePage"], 4000);
		        }
		    });
	   	}
	}	
}

function submitForm(e,data) {
    var jData = JSON.parse(data);
	var $form = $('#step2_form');
	var studentLogDocumentID = [];
	if (typeof jData.studentLogDocumentID != 'undefined') {
		for(var i=0; i<jData.studentLogDocumentID.length; i++) {
			studentLogDocumentID.push(jData.studentLogDocumentID[i]);
		}
		$('<input>').attr('type','hidden').attr('name','StudentLogDocumentID[]').attr('value',studentLogDocumentID).appendTo($form);
	}
    
    // Serialize the form data
	var serialized = $form.serializeArray();
    var formData = JSON.stringify(serialized);
	var strUrl = "medical/student_log/management/edit_step3";

	pageJS.cfn.destroyListener();
	formJS.cfn.destroyListener();

	var formObj = 'step2_form';
	if ($("#" + formObj).length > 0) {
		var isAllowSubmit = true;
		if ($("#" + formObj).find('input.required').length > 0) {
			$("#" + formObj).find('input.required').each(function() {
				if ($(this).val() == "" || typeof $(this).val() == "undefined") {
					isAllowSubmit = false;
				}
			});
		}
		
		if ($("#" + formObj).find('select.required').length > 0) {
			$("#" + formObj).find('select.required').each(function() {
				if ($(this).find('option:selected').length == 0) {
					isAllowSubmit = false;
				}
			});
		}

		if (!isAllowSubmit) {
			$('#AddSubmit').removeAttr('disabled');
			pageJS.cfn.bindListener();
			formJS.cfn.bindListener();
		} else {
			var successURL = $("#" + formObj).attr('data-success');
			var strUrl = $("#" + formObj).attr('rel');
			var xhr = formJS.vrs.xhr;
//			if (xhr != "") {
//				xhr.abort();
//			}
			//global
			formJS.vrs.glob_data[strUrl] = formData;
			
			var form_submitdata = pageJS.vrs.data;
			form_submitdata.formData = formData
			form_submitdata.srhtxt = "formSubmit";
			form_submitdata.stepData = formJS.vrs.glob_data;
			
		    formJS.vrs.xhr = $.ajax({
		        url: pageJS.vrs.url + "?page=" + strUrl,
		        type: 'POST',
		        data: form_submitdata,
		        timeout: 25000,
		        cache: false,
		        dataType: 'json',
				success: function(respdata, status, jqXHR) {
					$('.toast').remove();
					if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.submitHandler > success", "listener");
					if (typeof respdata.status !="undefined" && respdata.status == "success") {
						if(strUrl == successURL){
							pageJS.cfn.pageOut();
							appJS.vrs.detectHashChange = false;
							window.location.hash = successURL;
							pageJS.vrs.currHash = window.location.hash;
							pageJS.ltr.successHandler(respdata, status, jqXHR);
							appJS.vrs.detectHashChange = true;
						}								
						else if (pageJS.vrs.currHash != successURL) {
							pageJS.cfn.pageOut();
							appJS.vrs.detectHashChange = false;
							window.location.hash = successURL;
							appJS.vrs.detectHashChange = true;
							pageJS.cfn.pageLoad(successURL);
						}
					}else{
						if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.submitHandler > error", "listener");
						var fromModules = strUrl.substring(0, strUrl.search('/'));
						Materialize.toast(_langtxt[fromModules]["error"][respdata.error] , 4000);
						$('#AddSubmit').removeAttr('disabled');
						//bind listener
						pageJS.cfn.bindListener();
						formJS.cfn.bindListener();
					}
				},
				error: function(jqXHR, status, err) {
					if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.submitHandler > error", "listener");
					$('.toast').remove();
					Materialize.toast(_langtxt["errorToLoadThePage"], 4000);
					$('#AddSubmit').removeAttr('disabled');
					//bind listener
					pageJS.cfn.bindListener();
					formJS.cfn.bindListener();
				}
		    });
		}
	}
	e.preventDefault();
		
}
</script>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>