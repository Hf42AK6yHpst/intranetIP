<?php
/*
 *  2018-05-21 [Cameron]
 *      - fix: filter student who's RecordStatus=1 only
 */
class Common extends Controller
{
	public function __construct($info = array()) {
		parent::__construct($info);
	}
	
	public function __destruct()
	{
		if(isset($this->ApiDBConnection)){
			$this->closeApiDBConnection();
		}
	}
	
	private function isEJ()
	{
		global $junior_mck;
		return isset($junior_mck);
	}
	
	public function _remap($method)
	{
		$ApiDBConnection = $this->getApiDBConnection();
		
		$method = $this->str_replace_first("cus_", "", $method);
		$methodData = array (
			"controllerLink" => "ajdata/student",
			"pageLimit" => isset($_POST["cus"]["limit"]) ? $_POST["cus"]["limit"] : 10,
			"currPage" => isset($_POST["cus"]["page"]) ? $_POST["cus"]["page"] : 1
		);
		$this->infoArr["postData"]["limit"] = $methodData["pageLimit"];
		$this->infoArr["postData"]["page"] = $methodData["currPage"];
		
		$data = array();
		$param = array(
			"pageType" => $method,
			"respType" => "DATA",
			"pData" => $this->infoArr["postData"],
			"resultData" => $data
		);
		
		switch ($method) {
			case "autocomplete-class":
				// Get Search String
				$searchStr = $this->infoArr["postData"]["srhtxt"];
				$searchStr = strtolower(trim($searchStr));
				if($this->isEJ()) {
					$searchStr = convert2unicode($searchStr, 1, 0);
				}
				$searchStr = $ApiDBConnection->Get_Safe_Sql_Like_Query($searchStr);
				
				// Search by Class Name and Class Number
				if(!$this->isEJ())
				{
					$ayid = Get_Current_Academic_Year_ID();
					$cond = " AND (CONCAT(LOWER(yc.ClassTitleB5), ycu.ClassNumber) LIKE '%".$searchStr."%' OR CONCAT(LOWER(yc.ClassTitleEN), ycu.ClassNumber) LIKE '%".$searchStr."%')";
					
					$sql= "	SELECT 
								yc.ClassTitleEN, yc.ClassTitleB5, ycu.ClassNumber, iu.EnglishName, iu.ChineseName, iu.UserID, iu.UserLogin
							FROM
								INTRANET_USER iu
							INNER JOIN
								YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
							INNER JOIN
								YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID and yc.AcademicYearID = '".$ayid."'
							WHERE
                                iu.RecordStatus='1'
								".$cond."
							ORDER BY	
								yc.ClassTitleEN, ycu.ClassNumber
							LIMIT 30 ";
					$rs = $ApiDBConnection->returnResultSet($sql);
					foreach($rs as $_rs)
					{
					    $_rs_student_name = Get_Lang_Selection($_rs["ChineseName"], $_rs["EnglishName"]);
					    $_rs_class_name_number = "(".Get_Lang_Selection($_rs["ClassTitleB5"], $_rs["ClassTitleEN"])."-".$_rs["ClassNumber"].")";
					    if($this->infoArr["postData"]["needphoto"]==1) {
					        $_rs_student_photo = $this->getAutocompleteUserImages($_rs);
					        $data[] = array(
					            "label" => $_rs["UserID"],
					            "name" => $_rs_student_photo.'<br/>'.$_rs_student_name."<br/>".$_rs_class_name_number,
					            "photo_class" => ($_rs_student_photo == '' ? '' : 'btn-photo')
					        );
					    }
					    else {
    						$data[] = array(
    						    "label" => $_rs["UserID"],
    						    "name" => $_rs_student_name.$_rs_class_name_number
    						);
					    }
					}
				}
				else
				{
					$sql = "SELECT 
								ClassName, ClassNumber, EnglishName, ChineseName, UserID 
							FROM 
								INTRANET_USER 
							WHERE 
								RecordType = 2 AND RecordStatus = 1 AND 
								CONCAT(LOWER(ClassName), ClassNumber) LIKE '%".$searchStr."%' 
							ORDER BY 
								ClassNumber";
					$rs = $ApiDBConnection->returnResultSet($sql);
					foreach($rs as $_rs){
						$student_name = Get_Lang_Selection($_rs["ChineseName"], $_rs["EnglishName"])."(".$_rs["ClassName"]."-".$_rs["ClassNumber"].")";
						$student_name = convert2unicode($student_name, 1, 1);
						$data[] = array(
											"label" => $_rs["UserID"],
											"name" => $student_name
										);
					}
				}
				$param["resultData"] = $data; 
			break;
		
		case "autocomplete-name":
				// Get Search String
				$searchStr = $this->infoArr["postData"]["srhtxt"];
				$searchStr = strtolower(trim($searchStr));
				if($this->isEJ()) {
					$searchStr = convert2unicode($searchStr, 1, 0);
				}
				$searchStr = $ApiDBConnection->Get_Safe_Sql_Like_Query($searchStr);
				
				// Search by Student Name 
				if(!$this->isEJ())
				{
					$ayid = Get_Current_Academic_Year_ID();
					$cond = " AND (LOWER(iu.EnglishName) LIKE '%".$searchStr."%' OR LOWER(iu.ChineseName) LIKE '%".$searchStr."%')";
					
					$sql= "	SELECT 
								yc.ClassTitleEN, yc.ClassTitleB5, ycu.ClassNumber, iu.EnglishName, iu.ChineseName, iu.UserID, iu.UserLogin
							FROM
								INTRANET_USER iu
							INNER JOIN
								YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
							INNER JOIN
								YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID and yc.AcademicYearID = '".$ayid."'
							WHERE
                                iu.RecordStatus='1'
								".$cond."
							ORDER BY	
								yc.ClassTitleEN, ycu.ClassNumber
							LIMIT 30 ";
					$rs = $ApiDBConnection->returnResultSet($sql);
					foreach($rs as $_rs)
					{
					    $_rs_student_name = Get_Lang_Selection($_rs["ChineseName"], $_rs["EnglishName"]);
					    $_rs_class_name_number = "(".Get_Lang_Selection($_rs["ClassTitleB5"], $_rs["ClassTitleEN"])."-".$_rs["ClassNumber"].")";
					    if($this->infoArr["postData"]["needphoto"]==1) {
					        $_rs_student_photo = $this->getAutocompleteUserImages($_rs);
					        $data[] = array(
					            "label" => $_rs["UserID"],
					            "name" => $_rs_student_photo.'<br/>'.$_rs_student_name."<br/>".$_rs_class_name_number,
					            "photo_class" => ($_rs_student_photo == '' ? '' : 'btn-photo')
					        );
					    }
					    else {
					        $data[] = array(
					            "label" => $_rs["UserID"],
					            "name" => $_rs_student_name.$_rs_class_name_number
					        );
					    }
					}
				}
				else
				{
					$sql = "SELECT 
								ClassName, ClassNumber, EnglishName, ChineseName, UserID 
							FROM 
								INTRANET_USER 
							WHERE 
								RecordType = 2 AND RecordStatus = 1 AND 
								(LOWER(EnglishName) LIKE '%".$searchStr."%' OR LOWER(ChineseName) LIKE '%".$searchStr."%') 
							ORDER BY 
								ClassNumber";
					$rs = $ApiDBConnection->returnResultSet($sql);
					foreach($rs as $_rs){
						$student_name = Get_Lang_Selection($_rs["ChineseName"], $_rs["EnglishName"])."(".$_rs["ClassName"]."-".$_rs["ClassNumber"].")";
						$student_name = convert2unicode($student_name, 1, 1);
						$data[] = array(
											"label" => $_rs["UserID"],
											"name" => $student_name
										);
					}
				}
				$param["resultData"] = $data; 
				break;
		
		case "autocomplete-teacher-name":
				// Get Search String
				$searchStr = $this->infoArr["postData"]["srhtxt"];
				$searchStr = strtolower(trim($searchStr));
				if($this->isEJ()) {
					$searchStr = convert2unicode($searchStr, 1, 0);
				}
				$searchStr = $ApiDBConnection->Get_Safe_Sql_Like_Query($searchStr);
				
				// Search by Teacher Name 
				$sql= "	SELECT 
							iu.UserID, iu.EnglishName, iu.ChineseName
						FROM
							INTRANET_USER iu
						WHERE
							RecordType = '1' AND RecordStatus = '1' AND (LOWER(iu.EnglishName) LIKE '%".$searchStr."%' OR LOWER(iu.ChineseName) LIKE '%".$searchStr."%')
 						ORDER BY
							EnglishName, ChineseName
						LIMIT 30";
				$rs = $ApiDBConnection->returnResultSet($sql);
				foreach($rs as $_rs){
					$student_name = Get_Lang_Selection($_rs["ChineseName"], $_rs["EnglishName"]);
					if($this->isEJ()) {
						$student_name = convert2unicode($student_name, 1, 1);
					}
					$data[] = array(
										"label" => $_rs["UserID"],
										"name" => $student_name
									);
				}
				$param["resultData"] = $data; 
				break;
		}
		sleep(1);
		$this->outputJSON($param);
	}
	
	private function getAutocompleteUserImages($data)
	{
	    global $intranet_root;
	    $user_login_path = $intranet_root."/file/user_photo/".$data["UserLogin"].".jpg";
	    $uid_path1 = $intranet_root."/file/photo/personal/". $data["UserID"].".jpg";
	    $uid_path2 = $intranet_root."/file/photo/personal/". $data["UserID"].".JPG";
	    
	    // Student Photo (UserLogin)
	    if(file_exists($user_login_path)) {
	        $_rs_photo= "<img src='/file/user_photo/".$data["UserLogin"].".jpg' style='width:85px;height:110px;' class='img-thumbnail'>";
	    }
	    // Personal Photo (UserID)
	    else if(file_exists($uid_path1)) {
	        $_rs_photo= "<img src='/file/photo/personal/p".$data["UserID"].".jpg' style='width:85px;height:110px;' class='img-thumbnail'>";
	    }
	    else if(file_exists($uid_path2)) {
	        $_rs_photo= "<img src='/file/photo/personal/p".$data["UserID"].".jpg' style='width:85px;height:110px;' class='img-thumbnail'>";
	    }
	    // Default Photo
	    else {
	        $_rs_photo= "<img src='/images/myaccount_personalinfo/samplephoto.gif' style='width:85px;height:110px;' class='img-thumbnail'>";
	    }
	    return $_rs_photo;
	}
	
	private $ApiDBConnection;
	private function getApiDBConnection()
	{
		global $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		if(isset($this->ApiDBConnection)){
			return $this->ApiDBConnection;
		}
		else {
			include_once $intranet_root."/includes/libdb.php";
			$PATH_WRT_ROOT = $intranet_root."/";
			
			intranet_opendb();
			
			$ApiDBConnection = new libdb();
			return $this->ApiDBConnection = $ApiDBConnection;
		}
	}
	
	private function closeApiDBConnection()
	{
		intranet_closedb();
	}
}