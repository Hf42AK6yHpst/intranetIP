<?php

/* class common extends libdb { */
class Controller {
	public $infoArr = array();
	public $jsonObj;
	public function __construct($info = array()) {
		
		$this->infoArr = $info;
		$this->infoArr["viewsPath"] = dirname(dirname(__FILE__)) . "/views/";
		$this->infoArr["assetsPath"] = dirname(dirname(__FILE__)) . "/assets/";
		
		// prepare form data for php
		if(isset($this->infoArr["postData"]["formData"])){
			$jsonobj = $this->getJsonObj();
			$tmpFormData = $jsonobj->decode(stripslashes($this->infoArr["postData"]["formData"]));
			$newFormDataArr = array();
			foreach((array)$tmpFormData as $_valInfoArr){
				$_name = str_replace('[]', '', $_valInfoArr['name'],$arrCount);
				$_value = $_valInfoArr['value'];
				if($arrCount > 0){
					$newFormDataArr[$_name][] =	$_value;
				}else{
					$newFormDataArr[$_name] =	$_value;
				}
			}
			$this->infoArr["postData"]["formDataJson"] = $this->infoArr["postData"]["formData"];
			$this->infoArr["postData"]["formData"] = $newFormDataArr;
		}
		
		//step data
		if(isset($this->infoArr["postData"]["stepData"])){
			$jsonobj = $this->getJsonObj();
			foreach ((array) $this->infoArr["postData"]["stepData"] as $_step => $_stepData){
				$tempStepData[$_step] = $jsonobj->decode(stripslashes($_stepData));
			}
			$newFormDataArr = array();
			foreach ((array)$tempStepData as $_step => $_stepData){
				$stepTemp =  substr($_step,strrpos($_step, "/")+1 );
				foreach((array)$_stepData as $_valInfoArr){
					$_name = str_replace('[]', '', $_valInfoArr['name'],$arrCount);
					$_value = $_valInfoArr['value'];
					if($arrCount > 0){
						$newStepData[$stepTemp][$_name][] =	$_value;
					}else{
						$newStepData[$stepTemp][$_name] =	$_value;
					}
				}
			}
			$this->infoArr["stepDataJson"] = $this->infoArr["postData"]["stepData"];
			$this->infoArr["stepData"] = $newStepData;
			unset($this->infoArr["postData"]["stepData"]);
		}
	}
	
	public function view($viewData = array(), $allowEcho = false) {
		$viewFile = $this->infoArr["viewsPath"] . implode("/", $this->infoArr["controllerInfo"]) . ".php";
		
		if (file_exists($viewFile)) {
			include($viewFile);
		}
		return $responseHTML;
	}
	
	public function str_replace_first($from, $to, $subject)
	{
		$from = '/'.preg_quote($from, '/').'/';
		return preg_replace($from, $to, $subject, 1);
	}
	
	public function getJsonObj(){
		if(isset($this->jsonObj)){
			return $this->jsonObj;
		}else{
			return $this->jsonObj = new JSON_obj();
		}
	}
	
	public function outputJson($param, $allowEcho = true) {
		
		$jsonobj = $this->getJsonObj();
		if ($allowEcho) {
			echo $jsonobj->encode($param);
		} else {
			return $jsonobj->encode($param);
		}
	}
}

?>