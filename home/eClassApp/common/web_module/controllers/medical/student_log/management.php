<?php
/*
 * 2018-05-02 Cameron
 * - set default item for Lev2 in edit_step2
 *
 * 2018-04-30 Cameron
 * - not need to apply convert2unicode to the whole output for ej because medical uses utf8
 *
 * 2018-03-01 Cameron
 * - add $this->infoArr["UIContent"]["isShowAddBodyPart"] in edit_record
 *
 * 2017-12-08 Cameron
 * - create this file
 */
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/cust/medical/medicalConfig.inc.php");
include_once ($PATH_WRT_ROOT . "includes/cust/common_function.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedical.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedical_app_api.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLog.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedicalShowInput.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogLev1.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogLev2.php");
include_once ($PATH_WRT_ROOT . "includes/libUploadFiles.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogDocument.php");

class Management extends Controller
{

    public function __construct($info = array())
    {
        parent::__construct($info);
    }

    public function __destruct()
    {
        if (isset($this->medicalAPI)) {
            $this->closeMedicalApiConnection();
        }
    }

    private function isEJ()
    {
        global $junior_mck;
        return isset($junior_mck);
    }

    public function _remap($method)
    {
        global $Lang, $intranet_session_language, $intranet_root, $PATH_WRT_ROOT;
        global $w2_cfg, $medical_cfg, $plugin, $sys_custom;
        
        $medical_api = $this->getMedicalApiConnection();
        
        $method = $this->str_replace_first("cus_", "", $method);
        $methodData = array(
            "controllerLink" => "medical/student_log/management",
            "pageLimit" => isset($_POST["cus"]["limit"]) ? $_POST["cus"]["limit"] : 10,
            "currPage" => isset($_POST["cus"]["page"]) ? $_POST["cus"]["page"] : 1
        );
        $this->infoArr["postData"]["limit"] = $methodData["pageLimit"];
        $this->infoArr["postData"]["page"] = $methodData["currPage"];
        
        $jsStatus = "success"; // success => print $method html, fail => return to orignal page (default success)
        $jsError = "";
        
        /**
         * ** Set up Access Web View required Data ***
         */
        switch ($method) {
            
            // Add Record - Step 1 (Student)
            case "edit":
                // Get Student Options when back from Step 2
                $selected_student = $this->infoArr["postData"]["formData"]["sld_student"];
                
                if (! empty($selected_student)) {
                    $this->infoArr["UIContent"]["StudentSelection"] = $medical_api->getTargetUserDropDownOption($selected_student[0]);
                }
                
                // Unset POST Data
                if (isset($_POST)) {
                    if (isset($_POST["sld_student"])) {
                        unset($_POST["sld_student"]);
                        unset($this->infoArr["postData"]["formData"]["sld_student"]);
                    }
                }
                break;
            
            // Add Record - Step 2 (Record Details)
            case "edit_step2":
                // Check Empty Student
                $selected_student = $this->infoArr["postData"]["formData"]["sld_student"];
                
                if (empty($selected_student)) {
                    // Set Error Message: No Students
                    $jsError = "noSelectedStudents";
                    $jsStatus = "fail";
                } else {
                    global $sys_custom;
                    
                    // Get Summary Table Content
                    $RecordSummaryAry = array();
                    foreach ((array) $selected_student as $thisStudentID) { // one student only
                        $thisStudentInfo = $medical_api->getStudentInfoByID($thisStudentID);
                        if (! empty($thisStudentInfo)) {
                            $thisStudentName = $thisStudentInfo['Name'];
                            $thisClassNameNum = $thisStudentInfo['ClassName'] . " - " . $thisStudentInfo['ClassNumber'];
                            $RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)";
                        }
                    }
                    $this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
                    
                    $valueArray = array();
                    
                    // Time Minute
                    $minList = range(0, 59);
                    foreach ($minList as $min) {
                        $valueArray['time_min'][$min] = str_pad($min, 2, "0", STR_PAD_LEFT);
                    }
                    
                    // Time Second
                    $secList = range(0, 59);
                    foreach ($secList as $sec) {
                        $valueArray['time_sec'][$sec] = str_pad($sec, 2, "0", STR_PAD_LEFT);
                    }
                    
                    $this->infoArr["UIContent"]["StudentLogTime"] = date("H:i");
                    
                    $objLogLev1 = new studentLogLev1();
                    $lev1Ary = $objLogLev1->getActiveStatus($orderCriteria = 'Lev1Code');
                    $firstLev1ID = count($lev1Ary) ? $lev1Ary[0]['Lev1ID'] : '';
                    
                    // get level1 items
                    $this->infoArr["UIContent"]["StudentLogLevel1Select"] = $medical_api->getStudentLogLev1($firstLev1ID);
                    
                    // get level2 items
                    $this->infoArr["UIContent"]["StudentLogLevel2Select"] = $medical_api->getStudentLogLev2('', $firstLev1ID); // 2018-05-02: void [show "Please Select" first], show first item
                    
                    $lasted_min = $medical_api->getSelectionBox($id = "event_lasted_min", $name = "event_lasted_min", $valueArray['time_min'], $selected_min = 0, $class = "");
                    $lasted_sec = $medical_api->getSelectionBox($id = "event_lasted_sec]", $name = "event_lasted_sec", $valueArray['time_sec'], $selected_sec = 0, $class = "");
                    $this->infoArr["UIContent"]["StudentLogLastedMinuteSelect"] = $lasted_min;
                    $this->infoArr["UIContent"]["StudentLogLastedSecondSelect"] = $lasted_sec;
                    
                    // Get Student Log Default Remarks
                    $this->infoArr["UIContent"]["StudentLogDefaultRemarks"] = $medical_api->getDefaultStudentLogRemarks();
                    
                    // Get PIC Preset Options
                    $thisUserName = $medical_api->getUserNameByID($_SESSION["UserID"]);
                    $this->infoArr["UIContent"]["PICDefultOption"] = " <option value=\"" . $_SESSION["UserID"] . "\" selected>" . $thisUserName . "</option> \n\r ";
                    
                    // Get Hidden Fields
                    $student_field = "";
                    foreach ((array) $selected_student as $thisStudentID) {
                        $student_field .= "<input type=\"hidden\" name=\"sld_student[]\" value=\"" . $thisStudentID . "\">\n";
                    }
                    $student_field .= "<input type=\"hidden\" id=\"NrItem\" name=\"NrItem\" value=\"1\">\n";
                    $this->infoArr["UIContent"]["HiddenInputField"] = $student_field;
                    $this->infoArr["PageData"]["HiddenPICSelect"] = "";
                }
                break;
            
            // Edit Record
            case "edit_record":
                
                // get pass in parameters
                $RecordID = $this->infoArr["postData"]["formData"]["RecordID"];
                
                if (! $RecordID) {
                    $jsError = "noRecordID";
                    $jsStatus = "fail";
                } else {
                    $studentLog = new StudentLog($RecordID);
                    if (! $studentLog->getUserID()) {
                        $jsError = "noRecordID";
                        $jsStatus = "fail";
                    } else {
                        $studentID = $studentLog->getUserID();
                        $recordTime = $studentLog->getRecordTime();
                        $behaviourOne = $studentLog->getBehaviourOne();
                        $behaviourTwo = $studentLog->getBehaviourTwo();
                        $duration = $studentLog->getDuration();
                        $remarks = $studentLog->getRemarks();
                        $modifiedBy = $studentLog->getModifyBy();
                        $modifiedOn = $studentLog->getDateModified();
                        $pic = $studentLog->getPIC();
                        
                        $convulationIDList = $medical_api->geConvulatonID();
                        $isShowAddBodyPart = (in_array($behaviourTwo, $convulationIDList) || $plugin['medical_module']['AlwaysShowBodyPart']) ? '' : 'display:none;';
                        $this->infoArr["UIContent"]["isShowAddBodyPart"] = $isShowAddBodyPart;
                        
                        // Get Summary Table Content
                        $RecordSummaryAry = array();
                        
                        $thisStudentInfo = $medical_api->getStudentInfoByID($studentID);
                        
                        if (! empty($thisStudentInfo)) {
                            $thisStudentName = $thisStudentInfo['Name'];
                            $thisClassNameNum = $thisStudentInfo['ClassName'] . " - " . $thisStudentInfo['ClassNumber'];
                            $RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)";
                        }
                        $this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
                        
                        $valueArray = array();
                        // Time Minute
                        $minList = range(0, 59);
                        foreach ($minList as $min) {
                            $valueArray['time_min'][$min] = str_pad($min, 2, "0", STR_PAD_LEFT);
                        }
                        
                        // Time Second
                        $secList = range(0, 59);
                        foreach ($secList as $sec) {
                            $valueArray['time_sec'][$sec] = str_pad($sec, 2, "0", STR_PAD_LEFT);
                        }
                        
                        $this->infoArr["UIContent"]["StudentLogTime"] = date("H:i", strtotime($recordTime));
                        $this->infoArr["UIContent"]["StudentLogDate"] = date("Y-m-d", strtotime($recordTime));
                        
                        // get level1 items
                        $this->infoArr["UIContent"]["StudentLogLevel1Select"] = $medical_api->getStudentLogLev1($behaviourOne);
                        
                        // get level2 items
                        $this->infoArr["UIContent"]["StudentLogLevel2Select"] = $medical_api->getStudentLogLev2($behaviourTwo, $behaviourOne); // show "Please Select" first
                                                                                                                                               
                        // get level3 items
                        $level3IDList = $medical_api->getLevel3ListByID($RecordID);
                        $level3Layout = '';
                        for ($i = 0, $iMax = count($level3IDList); $i < $iMax; $i ++) {
                            $j = $i + 1;
                            $level3Layout .= $medical_api->getBodySelectionLayout($j, $j, $prefix = 'n', $level3IDList[$i]['Level3ID'], $RecordID);
                        }
                        
                        $this->infoArr["UIContent"]["StudentLogLevel3Layout"] = $level3Layout;
                        
                        $timeLasted = explode(':', $duration);
                        $time_min = intval($timeLasted[1]) + intval($timeLasted[0] * 24);
                        $time_second = intval($timeLasted[2]);
                        
                        $lasted_min = $medical_api->getSelectionBox($id = "event_lasted_min", $name = "event_lasted_min", $valueArray['time_min'], $time_min, $class = "");
                        $lasted_sec = $medical_api->getSelectionBox($id = "event_lasted_sec]", $name = "event_lasted_sec", $valueArray['time_sec'], $time_second, $class = "");
                        $this->infoArr["UIContent"]["StudentLogLastedMinuteSelect"] = $lasted_min;
                        $this->infoArr["UIContent"]["StudentLogLastedSecondSelect"] = $lasted_sec;
                        
                        // get attachments
                        $attachmentDetailList = $medical_api->getAttachmentDetail($RecordID);
                        if (count($attachmentDetailList) == 0) {
                            $uploadedFilesHTML = '--';
                        } else {
                            $uploadedFilesHTML = $medical_api->getAttachmentHTML_App($RecordID, $attachmentDetailList, $deleteBtn = true);
                        }
                        $this->infoArr["UIContent"]["Attachment"] = $uploadedFilesHTML;
                        
                        $this->infoArr["UIContent"]["StudentLogRemarks"] = $remarks;
                        
                        // Get Student Log Default Remarks
                        $this->infoArr["UIContent"]["StudentLogDefaultRemarks"] = $medical_api->getDefaultStudentLogRemarks();
                        
                        // Get PIC Options
                        $picOptions = $medical_api->getPICDropDownOption($pic);
                        $this->infoArr["UIContent"]["PICOptions"] = $picOptions;
                        
                        // Get Hidden Fields
                        $hidden_field = "<input type=\"hidden\" name=\"sld_student[]\" value=\"" . $studentID . "\">\n";
                        $hidden_field .= "<input type=\"hidden\" id=\"RecordID\" name=\"RecordID\" value=\"" . $RecordID . "\">\n";
                        $hidden_field .= "<input type=\"hidden\" id=\"NrItem\" name=\"NrItem\" value=\"" . (count($level3IDList) ? count($level3IDList) : 1) . "\">\n";
                        $hidden_field .= "<input type=\"hidden\" id=\"AttachmentToDelete\" name=\"AttachmentToDelete[]\" value=\"\">\n";
                        $this->infoArr["UIContent"]["HiddenInputField"] = $hidden_field;
                        $this->infoArr["UIContent"]["RecordID"] = $RecordID;
                    }
                }
                break;
            
            // View Record
            case "view":
                // get pass in parameters
                $RecordID = $_GET["RecordID"];
                $userType = $_SESSION["UserType"];
                
                if (! $RecordID) {
                    // Set Error Message: No record identifier
                    $jsError = "noRecordID";
                    $jsStatus = "fail";
                } else {
                    $studentLog = new StudentLog($RecordID);
                    if (! $studentLog->getUserID()) {
                        $jsError = "noRecordID";
                        $jsStatus = "fail";
                    } else {
                        $studentID = $studentLog->getUserID();
                        $recordTime = $studentLog->getRecordTime();
                        $behaviourOne = $studentLog->getBehaviourOne();
                        $behaviourTwo = $studentLog->getBehaviourTwo();
                        $duration = $studentLog->getDuration();
                        $remarks = $studentLog->getRemarks();
                        $modifiedBy = $studentLog->getModifyBy();
                        $modifiedOn = $studentLog->getDateModified();
                        
                        // Get Summary Table Content
                        $RecordSummaryAry = array();
                        
                        $thisStudentInfo = $medical_api->getStudentInfoByID($studentID);
                        
                        if (! empty($thisStudentInfo)) {
                            $thisStudentName = $thisStudentInfo['Name'];
                            $thisClassNameNum = $thisStudentInfo['ClassName'] . " - " . $thisStudentInfo['ClassNumber'];
                            if ($userType == '1') {
                                $RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)";
                            } else {
                                $RecordSummaryAry[] = $thisStudentName;
                            }
                        }
                        $this->infoArr["UIContent"]["UserType"] = $userType;
                        $this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
                        $this->infoArr["UIContent"]["RecordTime"] = $recordTime;
                        $this->infoArr["UIContent"]["Remarks"] = $remarks;
                        $lastModifiedInfo = $medical_api->getPersonInfo($modifiedBy);
                        $lastUpdatedBy = $lastModifiedInfo['Name'] . (($lastModifiedInfo['UserType'] == USERTYPE_PARENT) ? " (" . $Lang['medical']['general']['parent'] . ")" : "");
                        $this->infoArr["UIContent"]["LastUpdatedBy"] = $lastUpdatedBy;
                        $this->infoArr["UIContent"]["LastUpdatedOn"] = $modifiedOn;
                        $this->infoArr["UIContent"]["ModifyByID"] = $modifiedBy;
                        $this->infoArr["UIContent"]["InputByID"] = $studentLog->getInputBy();
                        
                        // get present status
                        if (! libMedicalShowInput::attendanceDependence('studentLog')) {
                            $present = 1; // Always present if the school have not buy attendance module
                        } else {
                            $present = $medical_api->getAttendanceStatusOfUser($studentID, substr($recordTime, 0, 10));
                        }
                        
                        $show_attendance = ($present == 1) ? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present'] : $Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'];
                        $this->infoArr["UIContent"]["AttendanceStatus"] = $show_attendance;
                        
                        // get duration
                        $timeLasted = explode(':', $duration);
                        $time_min = intval($timeLasted[1]) + intval($timeLasted[0] * 24);
                        $time_second = intval($timeLasted[2]);
                        $time_min = str_pad($time_min, 2, '0', STR_PAD_LEFT);
                        $time_second = str_pad($time_second, 2, '0', STR_PAD_LEFT);
                        $this->infoArr["UIContent"]["Duration"] = "{$time_min} {$Lang['medical']['studentLog']['minute']} {$time_second} {$Lang['medical']['studentLog']['second']}";
                        
                        // get lev1 and lev2 name, construct title
                        $_lev1 = new studentLogLev1($behaviourOne);
                        $lev1Name = $_lev1->getLev1Name();
                        $_lev2 = new studentLogLev2($behaviourTwo);
                        $lev2Name = $_lev2->getLev2Name();
                        $this->infoArr["UIContent"]["Title"] = $lev1Name . ' - ' . $lev2Name;
                        
                        // get lev3 & lev4 (body part & syndrome)
                        $Lev3List = $medical_api->getLev3List();
                        foreach ((array) $Lev3List as $Lev3) {
                            $valueArray['lev3List'][$Lev3['Lev3ID']] = $Lev3['Lev3Name'];
                        }
                        $objLogLev3 = $medical_api->getLevel3ListByID($RecordID);
                        $lev3_Sel = '';
                        foreach ($objLogLev3 as $item) {
                            $Lev4List = $medical_api->getLev4List($item['Level3ID']);
                            $Lev4SelectedList = $medical_api->getLev4SelectedList($RecordID, $item['Level3ID']);
                            $lev3_Sel .= $valueArray['lev3List'][$item['Level3ID']];
                            $lev3_Sel .= " - <span class='lev4ListArea'>";
                            $lev4Str = '';
                            foreach ((array) $Lev4List as $Lev4) {
                                foreach ((array) $Lev4SelectedList as $Lev4Selected) {
                                    if ($Lev4['Lev4ID'] == $Lev4Selected['Level4ID']) {
                                        $lev4Str .= $Lev4['Lev4Name'] . ', ';
                                        break;
                                    }
                                }
                            }
                            $lev3_Sel .= trim($lev4Str, ', ');
                            $lev3_Sel .= "</span>";
                            $lev3_Sel .= "<br />";
                        }
                        $this->infoArr["UIContent"]["BodyParts"] = $lev3_Sel;
                        
                        // get attachments
                        $attachmentDetailList = $medical_api->getAttachmentDetail($RecordID);
                        if (count($attachmentDetailList) == 0) {
                            $uploadedFilesHTML = '--';
                        } else {
                            $uploadedFilesHTML = $medical_api->getAttachmentHTML_App($RecordID, $attachmentDetailList, $deleteBtn = false);
                        }
                        $this->infoArr["UIContent"]["Attachment"] = $uploadedFilesHTML;
                        
                        // display body and duration ?
                        $convulationIDList = $medical_api->geConvulatonID();
                        $isDisplayed = (in_array($behaviourTwo, $convulationIDList) || $plugin['medical_module']['AlwaysShowBodyPart']) ? '' : 'display:none;';
                        $this->infoArr["UIContent"]["IsDisplayBodyPart"] = $isDisplayed;
                        
                        // Get Hidden Fields
                        $hidden_field = "<input type=\"hidden\" id=\"RecordID\" name=\"RecordID\" value=\"" . $RecordID . "\">\n";
                        $this->infoArr["UIContent"]["HiddenInputField"] = $hidden_field;
                        $this->infoArr["UIContent"]["RecordID"] = $RecordID;
                    }
                }
                break;
        }
        
        // Get Web View
        $responseHTML = $this->view($methodData);
        
        // Set up defualt JSON return
        $param = array(
            "hash" => $this->infoArr["strHash"],
            "pageType" => $method,
            "respType" => "HTML",
            "content" => $responseHTML,
            "customJS" => "",
            "pData" => $this->infoArr["postData"],
            "status" => $jsStatus,
            "error" => $jsError
        );
        
        /**
         * ** Perform AJAX required Actions ***
         */
        switch ($method) {
            
            // change Level1, get Level2 Data
            case "event_level1_select":
                $data = array();
                
                $level1ID = $this->infoArr["postData"]["val"];
                if ($level1ID) {
                    $level2Data = $medical_api->getLev2List($level1ID);
                    foreach ((array) $level2Data as $item) {
                        $data[] = array(
                            "label" => $item["Lev2ID"],
                            "name" => $item["Lev2Name"]
                        );
                    }
                }
                $param["resultData"] = $data;
                
                break;
            
            // change Level3, get Level4 Data
            case "event_level3_select":
                $data = array();
                $level3ID = $this->infoArr["postData"]["selectedVal"];
                if ($level3ID) {
                    $level4Data = $medical_api->getLev4List($level3ID);
                    foreach ((array) $level4Data as $item) {
                        $data[] = array(
                            "label" => $item["Lev4ID"],
                            "name" => $item["Lev4Name"]
                        );
                    }
                }
                $param["resultData"] = $data;
                
                break;
            
            case "get_body_part":
                $itemNr = $this->infoArr["postData"]["itemNr"];
                $labelNr = $this->infoArr["postData"]["labelNr"];
                $data = $medical_api->getBodySelectionLayout($itemNr, $labelNr);
                $param["resultData"] = $data;
                
                break;
            
            case "get_uploaded_files":
                
                $maxFile = 6;
                $maxSize = 1024 * 1024 * $maxFile;
                $error = "";
                
                if (count($_FILES) > $maxFile) {
                    $error = "ExceedMaxNrFile";
                } else {
                    $totalSize = 0;
                    foreach ($_FILES as $idx => $fileDetails) {
                        if ($fileDetails['size'] > $maxSize) {
                            $error = "ExceedFileSize";
                            break;
                        }
                    }
                }
                
                // ///////////////////////////////////////
                // handle the file upload Data Structure//
                // ///////////////////////////////////////
                $uploadFileList = array();
                $studentLogDocumentID = array(); // for update documentID later
                $files = array(); // for return file name list
                
                if (count($_FILES) && empty($error)) {
                    foreach ($_FILES as $idx => $fileDetails) {
                        $uploadFileList[$idx]['name'] = $fileDetails['name'];
                        $uploadFileList[$idx]['type'] = $fileDetails['type'];
                        $uploadFileList[$idx]['tmp_name'] = $fileDetails['tmp_name'];
                        $uploadFileList[$idx]['error'] = $fileDetails['error'];
                        $uploadFileList[$idx]['size'] = $fileDetails['size'];
                    }
                    
                    $uploadFile_root = $medical_cfg['uploadFile_root'];
                    if (! file_exists($uploadFile_root)) {
                        mkdir($uploadFile_root, 0777);
                    }
                    $uploadResultAry = array(); // STORE THE UPLOAD DOCUMENT RESULT
                    foreach ((array) $uploadFileList as $idx => $_details) {
                        if (trim($_details['tmp_name']) == '') {
                            continue; // without upload item , skip to next one
                        }
                        $objUploadFiles = new libUploadFiles($uploadFile_root);
                        
                        $objUploadFiles->setName($_details['name']);
                        $objUploadFiles->setFileType($_details['type']);
                        $objUploadFiles->setTmp_Name($_details['tmp_name']);
                        $objUploadFiles->setError($_details['error']);
                        $objUploadFiles->setSize($_details['size']);
                        $uploadResultAry[] = $objUploadFiles->upload();
                    }
                    
                    $RecordID = $_POST['RecordID'] ? $_POST['RecordID'] : 0; // new: default 0, update later
                                                                             
                    // SAVE TO DB
                    foreach ($uploadResultAry as $idx => $_uploadResult) {
                        $_uploadFailed = 0; // RECORD FOR EACH _uploadInputName
                        
                        if (! $_uploadResult['uploadSuccess']) {
                            $_uploadFailed ++;
                            // upload failed , skip to save to DB
                            continue;
                        }
                        
                        $objDocument = new StudentLogDocument();
                        
                        $objDocument->setStudentLogID($RecordID);
                        $objDocument->setRenameFile($_uploadResult['renameFile']);
                        $objDocument->setOrgFileName($_uploadResult['name']);
                        $objDocument->setFileType($_uploadResult['type']);
                        $objDocument->setFileSize($_uploadResult['size']);
                        $objDocument->setFolderPath($_uploadResult['destFolder']);
                        $objDocument->setDateInput('now()');
                        $objDocument->setInputBy($_SESSION['UserID']);
                        $objDocument->save();
                        $studentLogDocumentID[] = $objDocument->getRecordID();
                        $files[] = $objDocument->getRenameFile();
                    }
                    $param["studentLogDocumentID"] = $studentLogDocumentID;
                    $param["files"] = $files;
                } else {
                    $param["formData"] = $_POST;
                }
                
                $param["error"] = $error;
                $param["status"] = empty($error) ? "success" : "fail";
                
                break;
            
            // Add Record - Step 3 - Save student_log record
            case "edit_step3":
                
                // POST Data (Add Record > "formData")
                $thisFormData = $this->infoArr["postData"]["formData"];
                
                $success = false;
                
                // Insert Student Log Record
                if (! empty($thisFormData)) {
                    $jsErrorMsg = $medical_api->addStudentLogRecord($thisFormData);
                    
                    if (empty($jsErrorMsg)) {
                        $success = true;
                    } else {
                        $param["error"] = $jsErrorMsg;
                    }
                } else {
                    $param["error"] = "noFormData";
                }
                
                $param["status"] = $success ? "success" : "fail";
                
                break;
            
            // Update student log record
            case "edit_update":
                
                $thisFormData = $this->infoArr["postData"]["formData"];
                $success = false;
                
                // Update Student Log Record
                if (! empty($thisFormData)) {
                    $retInfo = $medical_api->updateStudentLogRecord($thisFormData);
                    $jsErrorMsg = $retInfo["error"];
                    if (empty($jsErrorMsg)) {
                        $success = true;
                    } else {
                        $param["error"] = $jsErrorMsg;
                    }
                } else {
                    $param["error"] = "noFormData";
                }
                
                $param["status"] = $success ? "success" : "fail";
                $param["RecordID"] = $thisFormData['RecordID'];
                
                break;
            
            // Delete student log
            case "delete":
                $eventIDArray = $_POST["RecordID"];
                $objMedical = new libMedical();
                include ($intranet_root . "/home/eAdmin/StudentMgmt/medical/management/studentLogAddItemSave.php");
                
                if ($Msg == $Lang['General']['ReturnMessage']['UpdateSuccess']) {
                    $success = true;
                } else {
                    $param["error"] = $jsErrorMsg;
                }
                
                $param["status"] = $result ? "success" : "fail";
                
                break;
        }
        
        // Output JSON return
        sleep(1);
        $this->outputJSON($param);
    }

    private $medicalAPI;

    private function getMedicalApiConnection()
    {
        global $intranet_root, $PATH_WRT_ROOT, $intranet_session_language, $plugin;
        if (isset($this->medicalAPI)) {
            return $this->medicalAPI;
        } else {
            
            intranet_opendb();
            
            $medicalAPI = new libMedical_app_api();
            $medicalAPI->controllerModule = "student_log";
            $medicalAPI->setMedicalImagePath($this->infoArr["thisImagePath"]);
            
            $objMedical = new libMedical();
            if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'BOWEL_MANAGEMENT') || ! $plugin['medical_module']['student_log']) {
                $medicalAPI->AllowAccess = false;
            } else {
                $medicalAPI->AllowAccess = true;
            }
            
            return $this->medicalAPI = $medicalAPI;
        }
    }

    private function closeMedicalApiConnection()
    {
        intranet_closedb();
    }
}