<?php

class Management extends Controller
{
	public function __construct($info = array())
	{
		parent::__construct($info);
	}
	
	public function __destruct()
	{
		if(isset($this->disciplineAPI)) {
			$this->closeDisciplineApiConnection();
		}
	}
	
	private function isEJ()
	{
		global $junior_mck;
		return isset($junior_mck);
	}
	
	public function _remap($method)
	{
		global $Lang, $eDiscipline;
		
		$discipline_api = $this->getDisciplineApiConnection();
		$SESSION_ACCESS_RIGHT = $discipline_api->UserAccessRightList;
		
		$method = $this->str_replace_first("cus_", "", $method);
		$methodData = array (
			"controllerLink" => "ediscipline/award_punishment/management",
			"pageLimit" => isset($_POST["cus"]["limit"])? $_POST["cus"]["limit"] : 10,
			"currPage" => isset($_POST["cus"]["page"])? $_POST["cus"]["page"] : 1
		);
		$this->infoArr["postData"]["limit"] = $methodData["pageLimit"];
		$this->infoArr["postData"]["page"] = $methodData["currPage"]; 
		
		$jsStatus = "success"; 	// success => print $method html, fail => return to orignal page (default success)
		$jsError = "";
		
		/**** Set up Access Web View required Data ****/
		switch ($method) {
			
			# Merit Table List
			case "mylist":
				global $i_Discipline_Detention_All_Teachers;
				
				# Unset unwanted POST data
				if(isset($_POST) && isset($_POST["sld_student"])) {
					unset($_POST["sld_student"]);
					unset($this->infoArr["postData"]["formData"]["sld_student"]);
				}
				
				# School Year Selection
				$CurrentYearID = $this->isEJ()? getCurrentAcademicYear() : Get_Current_Academic_Year_ID();
				$selectYear = $this->infoArr["postData"]["formData"]["selectYear"];
				if($this->isEJ()) {
					$selectYear = convert2unicode($selectYear, 1, 0);
				}
				$selectYear = $selectYear == "" ? $CurrentYearID : $selectYear;
				$selectSchoolYear = $discipline_api->GetYearWebViewSelection($selectYear, $discipline_api->controllerModule);
				
				# Semester Selection
				$selectSemester = $this->infoArr["postData"]["formData"]["selectSemester"];
				if($this->isEJ()) {
					$selectSemester = convert2unicode($selectSemester, 1, 0);
				}
				if ($selectSemester == "") {
					$selectSemester = "WholeYear";
				}
				$selectYearTerm = $discipline_api->GetSemesterWebViewSelection($selectYear, $selectSemester, $discipline_api->controllerModule);
				
				# Class Selection
				$selectClass = $this->infoArr["postData"]["formData"]["selectClass"];
				if($this->isEJ()) {
					$selectClass = convert2unicode($selectClass, 1, 0);
				}
				$selectYearClass = $discipline_api->GetClassWebViewSelection($selectClass, $selectYear, $discipline_api->controllerModule);
				
				# PIC Selection
				$selectPIC = $this->infoArr["postData"]["formData"]["selectPIC"];
				$PICArray = $discipline_api->getPICInArray("DISCIPLINE_MERIT_RECORD", $selectYear, $selectSemester);
				if(sizeof($PICArray)==0) {
					$PICArray = array();
				}
				$selectPICTeacher = getSelectByArray($PICArray," id=\"selectPIC\" name=\"selectPIC\" ", $selectPIC, 0, 0, "".$i_Discipline_Detention_All_Teachers."");
				
				# Check Access Right - View Records
				$HasNewRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-New");
				
				$this->infoArr["UIContent"]["SchoolYearSelection"] = $selectSchoolYear;
				$this->infoArr["UIContent"]["SemesterSelection"] = $selectYearTerm;
				$this->infoArr["UIContent"]["ClassSelection"] = $selectYearClass;
				$this->infoArr["UIContent"]["PICSelection"] = $selectPICTeacher;
				$this->infoArr["PageData"]["AddAPAccessRight"] = $HasNewRight;
			break;
			
			# Record Details
			case "detail":
				$RecordID = $this->infoArr["postData"]["targetid"];
				if(!empty($RecordID))
				{
					// Get Merit Record Details
					$MeritRecord = $discipline_api->GetMeritRecord("", $RecordID);
					if(!empty($MeritRecord))
					{
						// Set Data for Display
						$MeritRecord["RecordReceived"] = $MeritRecord["MeritTypeImg"]." ".$MeritRecord["ReceivedMeritType"];
						$MeritRecord["RecordStatus"] = $MeritRecord["RecordStatusImg"]." ".$MeritRecord["RecordStatusName"]."&nbsp;&nbsp;&nbsp;".$MeritRecord["ReleaseStatusImg"]." ".$MeritRecord["ReleaseStatusName"];
						$MeritRecord["Remark"] = str_replace("\n", "<br/>", $MeritRecord["Remark"]);
						if(!$this->isEJ()) {
							$MeritRecord["RecordSubject"] = $discipline_api->getSubjectNameBySubjectID($MeritRecord["SubjectID"]);
							$MeritRecord["RecordSubject"] = nl2br($MeritRecord["RecordSubject"]);
						}
						
						// Get Merit Actions
						$thisMeritAction = $discipline_api->GetMeritAction($MeritRecord);
						$MeritRecord["RecordAction"] = $thisMeritAction["RecordAction"];
						$MeritRecord["RecordAction"] = str_replace("<br/>", ", ", $MeritRecord["RecordAction"]);
						$MeritRecord["RecordPICs"] = $thisMeritAction["RecordPICs"];
						$MeritRecord["RecordReference"] = $thisMeritAction["RecordReference"];
						
						// Get Merit Notice Content
						$NoticeID = $MeritRecord["NoticeID"];
						if(!empty($NoticeID)) {
							$RecordNoticeContent = $discipline_api->GetNoticeByNoticeID($NoticeID);
						}
						
						// Get Merit related Conducts
						if($MeritRecord["fromConductRecords"]) {
							$MeritRecord["RecordRelatedConduct"] = $discipline_api->getMeritRelatedConductTable($RecordID);
						}
						
						# Check Access Right - Edit Merit
						$isOwnRelatedRecords = $discipline_api->isMeritRecordPIC($RecordID) || $discipline_api->isMeritRecordOwn($RecordID);
						$HasEditRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || ($isOwnRelatedRecords && $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"));
						$this->infoArr["PageData"]["EditAPAccessRight"] = $HasEditRight;
						
						# Check Access Right - Delete Merit
						$HasDeleteRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll") || ($isOwnRelatedRecords && $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn"));
						
						# Not Allow Delete - (Upgraded from Conducts / Related to Case / Redeemed)
						$fromConductRecords = $MeritRecord["fromConductRecords"];
						$CaseID = $MeritRecord["CaseID"];
						$RedeemID = $MeritRecord["RedeemID"];
						if($HasDeleteRight && ($fromConductRecords > 0 || $CaseID > 0 || $RedeemID > 0)) {
							$HasDeleteRight = false;
						}
						$this->infoArr["PageData"]["DeleteAPAccessRight"] = $HasDeleteRight;
					}
				}
				
				// Store Merit Record Info
				if(!empty($MeritRecord)) {
					foreach($MeritRecord as $key => $data) {
						$MeritRecord[$key] = empty($data)? "---" : $data;
					}
					$this->infoArr["PageData"]["RecordInfo"] = $MeritRecord;
					unset($MeritRecord);
				}
				if(!empty($RecordNoticeContent)) {
					foreach($RecordNoticeContent as $key => $data) {
						$RecordNoticeContent[$key] = empty($data)? "&nbsp;" : $data;
					}
					$this->infoArr["PageData"]["NoticeInfo"] = $RecordNoticeContent;
					unset($RecordNoticeContent);
				}
			break;
			
			# Add Record - Step 1 (Student)
			case "edit":
				// Get Student Options when back from Step 2
				$selected_student = $this->infoArr["postData"]["formData"]["sld_student"];
				if(!empty($selected_student)) {
					$selected_student = array_unique((array)$selected_student);
					$this->infoArr["UIContent"]["StudentSelection"] = $discipline_api->getTargetUserDropDownOption($selected_student);
				}
				
				# Unset POST Data
				if(isset($_POST))
				{
					if(isset($_POST["targetid"])) {
						unset($_POST["targetid"]);
						unset($this->infoArr["postData"]["targetid"]);
					}
					if(isset($_POST["sld_student"])) {
						unset($_POST["sld_student"]);
						unset($this->infoArr["postData"]["formData"]["sld_student"]);
					}
				}
			break;
			
			# Add Record - Step 2 (Record Details)
			case "edit_step2":
				# Check Empty Student
				$selected_student = $this->infoArr["postData"]["formData"]["sld_student"];
				if(empty($selected_student))
				{
					// Set Error Message: No Students
					$jsError = "noSelectedStudents";
					$jsStatus = "fail";
				}
				else
				{
					global $sys_custom;
					
					$StudentCount = 0;
					$RecordSummaryAry = array();
					
					// Get Summary Table Content
					$selected_student = array_unique((array)$selected_student);
					$isSingleRecordOnly = count((array)$selected_student) == 1;
					foreach((array)$selected_student as $thisStudentID)
					{
						$thisStudentInfo = $discipline_api->getStudentNameByID($thisStudentID);
						$thisStudentInfo = $thisStudentInfo[0];
						if(!empty($thisStudentInfo))
						{
							$thisStudentName = $thisStudentInfo[1];
							$thisClassNameNum = $thisStudentInfo[2]." - ".$thisStudentInfo[3];
							if($isSingleRecordOnly) {
								$RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)";
							}
							else {
								++$StudentCount;
								$RecordSummaryAry[] = "$StudentCount. $thisStudentName ($thisClassNameNum)";
							}
						}
					}
					$this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
					
					// Get Semester Selection
					if($this->isEJ() && getSemesterMode()==1) {
						$this->InfoArr["UIContent"]["SemesterSelect"] = $discipline_api->GetSemesterWebViewSelection("", getCurrentSemester(), $discipline_api->controllerModule, true);
					}
					
					// Get Merit Type Options
					$MeritType = $discipline_api->returnValidMeritTypeFieldWithOrder(1);
					$MeritType[0] = array("-999", "--");
					ksort($MeritType);
					foreach ((array)$MeritType as $this_record_type) {
					     $this->infoArr["UIContent"]["MeritTypeOption"] .= "<option value=\"".$this_record_type[0]."\">".$this_record_type[1]."</option>\n";
					}
					
					// Get Demerit Type Options
					$DemeritType = $discipline_api->returnValidMeritTypeFieldWithOrder(-1);
					$DemeritType[0] = array("-999", "--");
					ksort($DemeritType);
					foreach ((array)$DemeritType as $this_record_type) {
					     $this->infoArr["UIContent"]["DemeritTypeOption"] .= "<option value=\"".$this_record_type[0]."\">".$this_record_type[1]."</option>\n";
					}
					
					// Get Merit Number Options
					$MeritInterval = $discipline_api->AP_Interval_Value ? $discipline_api->AP_Interval_Value : 1;
					for ($i=0; $i<=$discipline_api->AwardPunish_MAX; $i=$i+$MeritInterval) {
						$thisMeritNumber = my_round($i, $Decimal=2);
					    $this->infoArr["UIContent"]["MeritNumOption"] .= "<option value=\"".$thisMeritNumber."\">".$i."</option>\n";
					}
					
					// Get Conduct Mark Options
					if(!$discipline_api->Hidden_ConductMark) {
						$i = 0;
						$loopConductMark = true;
						$conductInterval = $sys_custom["eDiscipline"]["ConductMark1DecimalPlace"] ? 0.1 : 1;
						while($loopConductMark) {
							$thisConductMark = my_round($i, $Decimal=1);
							$this->infoArr["UIContent"]["ConductMarkOption"] .= "<option value='".$thisConductMark."'>".$i."</option>\n";
							$i = (float)(string)($i + $conductInterval);
							if(floatcmp($i ,">" ,$discipline_api->ConductMarkIncrement_MAX)) {
								$loopConductMark = false;
							}
						}
					}
					
					// Get Study Score Options
					if($discipline_api->UseSubScore) {
						for ($i=0; $i<=$discipline_api->SubScoreIncrement_MAX; $i++) {
							$this->infoArr["UIContent"]["StudyScoreOption"] .= "<option value='".$i."'>".$i."</option>\n";
						}
					}

					// Get Activity Score Options
					if($discipline_api->UseActScore) {
						for ($i=0; $i<=$discipline_api->ActScoreIncrement_MAX; $i++) {
							$this->infoArr["UIContent"]["ActivityScoreOption"] .= "<option value='".$i."'>".$i."</option>\n";
						}
					}
					
					// Get PIC Preset Options
					$thisUserName = $discipline_api->getUserNameByID($_SESSION["UserID"]);
					$thisUserName = $thisUserName[0];
					$this->infoArr["UIContent"]["PICDefultOption"] = " <option value=\"".$_SESSION["UserID"]."\" selected>".$thisUserName."</option> \n\r ";

					// [2019-1105-1700-25235] Get Subject Name Options
                    if ($discipline_api->use_subject)
                    {
                        $this->infoArr["UIContent"]["SubjectNameOption"] = $discipline_api->getSubjectNameDropDownOption();
                    }
					
					// Get Hidden Fields
					$student_field = "";
					foreach((array)$selected_student as $thisStudentID) {
						$student_field .= "<input type=\"hidden\" name=\"sld_student[]\" value=\"".$thisStudentID."\">\n";
					}
					$this->infoArr["UIContent"]["HiddenInputField"] = $student_field;
					
					# Hide PIC Search Field > System Setting - Only allow admin to select record's PIC
					$this->infoArr["PageData"]["HiddenPICSelect"] = $discipline_api->OnlyAdminSelectPIC && !$discipline_api->IS_ADMIN_USER();
				}
			break;
			
			# Add Record - Step 3 (Follow-ups) / Edit Record - Assign Notice
			case "edit_step3":
				// POST Data (Add Record > "formData" / Assign Notice > "targetid") 
				$thisFormData = $this->infoArr["postData"]["formData"];
				$thisRecordID = $this->infoArr["postData"]["targetid"];
				$IsNewMeritRecord = empty($thisRecordID);
				
				# Perform Form Data Checking > New Records
				if($IsNewMeritRecord && !empty($thisFormData)) {
					$jsErrorMsg = $discipline_api->insertMeritRecordDataChecking($thisFormData);
				}
				# Empty Data Checking
				else if($IsNewMeritRecord && empty($thisFormData)) {
					$jsErrorMsg = "noFormData";
				}
				
				# Data Checking : Success => Assign Notice Page
				if(empty($jsErrorMsg))
				{
					# Insert Merit Records
					if($IsNewMeritRecord) {
						$MeritIDAry = $discipline_api->insertMeritRecordByAPI($thisFormData);
					}
					# Get Merit Data for Processing
					else {
						$MeritIDAry = array($thisRecordID);
						$MeritRecord = $discipline_api->GetMeritRecord("", $thisRecordID);
						
						$thisFormData = array();
						$thisFormData["ap_category_select"] = $MeritRecord["CatID"];
					}
					
					// Get Page Navigation
					$this->infoArr["UIContent"]["PageNavigation"][0] = $IsNewMeritRecord? $eDiscipline['NewRecord'] : $Lang['eDiscipline']['App']['RecordDetails'];
					$this->infoArr["UIContent"]["PageNavigation"][1] = $IsNewMeritRecord? $Lang['General']['Steps']."3: ".$eDiscipline['SelectActions'] : $eDiscipline['SelectActions'];
					
					// Get Summary Table Content
					$MeritCount = 0;
					$RecordSummaryAry = array();
					$isSingleRecordOnly = count((array)$MeritIDAry) == 1;
					foreach((array)$MeritIDAry as $thisMeritID) {
						$thisMeritInfo = $discipline_api->GetMeritRecord("", $thisMeritID);
						if(!empty($thisMeritInfo)) {
							$thisStudentName = $thisMeritInfo["StudentName"];
							$thisClassNameNum = $thisMeritInfo["ClassNameNum"];
							$thisRecordType = $thisMeritInfo["ReceivedMeritType"];
							$thisItemName = $thisMeritInfo["ItemName"];
							$MeritTypeImg = $thisMeritInfo["MeritTypeImg"];
							if($isSingleRecordOnly) {
								$RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)<br/>$thisItemName $MeritTypeImg $thisRecordType";
							}
							else {
								++$MeritCount; 
								$RecordSummaryAry[] = "$MeritCount. $thisStudentName ($thisClassNameNum)<br/>$thisItemName $MeritTypeImg $thisRecordType";
							}
						}
					}
					$this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
					
					// Get Notice Template Options
					$this->infoArr["UIContent"]["NoticeOptions"] = $discipline_api->getSelectNoticeDropDown($thisFormData, true);
					
					// Get Hidden Fields
					$this->infoArr["UIContent"]["HiddenInputField"] = "";
					foreach((array)$MeritIDAry as $thisMeritID) {
						$this->infoArr["UIContent"]["HiddenInputField"] .= "<input type=\"hidden\" name=\"MeritID[]\" value=\"".$thisMeritID."\">\n";
					}
				}
				# Data Checking : Fail
				else {
					// Set Error Message
					$jsError = $jsErrorMsg;
					$jsStatus = "fail";
				}
			break;
			
			# Edit Record
			case "edit_record":
				global $i_Merit_Award;
				
				$RecordID = $this->infoArr["postData"]["targetid"];
				if(!empty($RecordID))
				{
					// Get Merit Info
					$MeritRecord = $discipline_api->GetMeritRecord("", $RecordID);
					$MeritSemester = $MeritRecord["Semester"];
					$MeritType = $MeritRecord["MeritType"];
					$MeritCatID = $MeritRecord["CatID"];
					$MeritItemID = $MeritRecord["ItemID"];
					$MeritProfileType = $MeritRecord["ProfileMeritType"];
					$MeritProfileCount = $MeritRecord["ProfileMeritCount"];
					$MeritCouductScore = $MeritRecord["ConductScoreChange"];
					$MeritStudyScore = $MeritRecord["SubScore1Change"];
					$MeritActivityScore = $MeritRecord["SubScore2Change"];
					$MeritPICs = $MeritRecord["PICs"];
					$MeritFromConducts = $MeritRecord["fromConductRecords"];
					//$MeritFromCase = $MeritRecord["CaseID"];
					$MeritWaived = $MeritRecord["RecordStatus"]==2;

					// [2019-1105-1700-25235]
                    $MeritSubjectName = $MeritRecord["Subject"];
                    if(!$this->isEJ()) {
                        $MeritSubjectID = $MeritRecord["SubjectID"];
                    }
					
					// Set Data for Display
					$MeritItemInfo = $MeritRecord["ItemName"];
					list($MeritCatName, $MeritItemName) = explode(" - ", $MeritItemInfo);
					$MeritRecord["CategoryName"] = $MeritCatName;
					$MeritRecord["Remark"] = $MeritRecord["Remark"]=="---"? "" : $MeritRecord["Remark"];
					$this->infoArr["PageData"]["MeritRecord"] = $MeritRecord;
					
					// Get Summary Table Content
					$thisStudentName = $MeritRecord["StudentName"];
					$thisClassNameNum = $MeritRecord["ClassNameNum"];
					$this->infoArr["UIContent"]["SummaryTable"] = "$thisStudentName ($thisClassNameNum)";
					
					// Get Semester Options
					$this->InfoArr["UIContent"]["SemesterSelect"] = "";
					if($this->isEJ() && getSemesterMode()==1) {
						$MeritSemester = $MeritSemester==""? getCurrentSemester() : $MeritSemester;
						$this->InfoArr["UIContent"]["SemesterSelect"] = $discipline_api->GetSemesterWebViewSelection("", $MeritSemester, $discipline_api->controllerModule, true);
					}
					
					// Get Merit Category Options
					$UserID = $_SESSION["UserID"];
					$MeritCategoryList = $discipline_api->returnMeritItemCategoryByType($MeritType);
					foreach((array)$MeritCategoryList as $thisMeritCategory) {
						list($thisCatID, $thisCatName) = $thisMeritCategory;
						$thisCatSelected = !empty($thisCatID) && $thisCatID==$MeritCatID? " selected" : "";
						$this->infoArr["UIContent"]["CategoryOption"] .= " <option value=\"".$thisCatID."\" ".$thisCatSelected.">".$thisCatName."</option> \n\r ";
					}
					// Load Disabled Category if cannot access Item
					if(empty($this->infoArr["UIContent"]["CategoryOption"])) {
						$this->infoArr["UIContent"]["CategoryOption"] .= " <option value=\"".$MeritCatID."\" selected>".$MeritCatName."</option> \n\r ";
					}
					
					// Get Merit Items Options
					$this->infoArr["UIContent"]["ItemOption"] = "";
					if($MeritCatID > 0) {
						$UserID = $_SESSION["UserID"];
						$MeritCatItems = $discipline_api->retrieveMeritItemswithCodeGroupByCat();
						$MeritCatItems = BuildMultiKeyAssoc((array)$MeritCatItems, array("CatID", "ItemID"), array("1", "2"));
						$MeritCatItems = $MeritCatItems[$MeritCatID];
						foreach((array)$MeritCatItems as $thisMeritItem) {
							list($thisItemCat, $thisItemID, $thisItemName) = $thisMeritItem;
							$thisItemSelected = !empty($thisItemID) && $thisItemID==$MeritItemID? " selected" : "";
							$this->infoArr["UIContent"]["ItemOption"] .= " <option value=\"".$thisItemID."\" ".$thisItemSelected.">".$thisItemName."</option> \n\r ";
						}
						// Load Disabled Item if cannot access Item
						if(empty($this->infoArr["UIContent"]["ItemOption"])) {
							$this->infoArr["UIContent"]["ItemOption"] .= " <option value=\"".$MeritItemID."\" selected>".$MeritItemName."</option> \n\r ";
						}
					}
					
					// Get Merit Type Options
					$MeritProfileTypeList = $discipline_api->returnValidMeritTypeFieldWithOrder($MeritType);
					$MeritProfileTypeList[0] = array("-999", "--");
					ksort($MeritProfileTypeList);
					foreach ((array)$MeritProfileTypeList as $thisProfileType) {
						list($thisProfileTypeVal, $thisProfileTypeName) = $thisProfileType;
						$thisTypeSelected = isset($thisProfileTypeVal) && $thisProfileTypeVal==$MeritProfileType? " selected" : "";
					    $this->infoArr["UIContent"]["ProfileTypeOption"] .= "<option value=\"".$thisProfileTypeVal."\" ".$thisTypeSelected.">".$thisProfileTypeName."</option>\n";
					}
					
					// Get Merit Number Options
					$MeritInterval = $discipline_api->AP_Interval_Value ? $discipline_api->AP_Interval_Value : 1;
					for ($i=0; $i<=$discipline_api->AwardPunish_MAX; $i=$i+$MeritInterval) {
						$thisMeritNumber = my_round($i, $Decimal=2);
						$thisCountSelected = !empty($thisMeritNumber) && $thisMeritNumber==$MeritProfileCount? " selected" : "";
					    $this->infoArr["UIContent"]["ProfileCountOption"] .= "<option value=\"".$thisMeritNumber."\" ".$thisCountSelected.">".$i."</option>\n";
					}
					
					// Get Conduct Mark Options
					if(!$discipline_api->Hidden_ConductMark) {
						$i = 0;
						$loopConductMark = true;
						$conductInterval = $sys_custom["eDiscipline"]["ConductMark1DecimalPlace"] ? 0.1 : 1;
						while($loopConductMark) {
							$thisConductMark = my_round($i, $Decimal=1);
							$thisConductScoreSelected = !empty($thisConductMark) && !empty($MeritCouductScore) && $thisConductMark==abs($MeritCouductScore)? " selected" : "";
							$this->infoArr["UIContent"]["ConductScoreOption"] .= "<option value='".$thisConductMark."' ".$thisConductScoreSelected.">".$i."</option>\n";
							$i = (float)(string)($i + $conductInterval);
							if(floatcmp($i ,">" ,$discipline_api->ConductMarkIncrement_MAX)) {
								$loopConductMark = false;
							}
						}
					}
					
					// Get Study Score Options
					if($discipline_api->UseSubScore) {
						for ($i=0; $i<=$discipline_api->SubScoreIncrement_MAX; $i++) {
							$thisStudyScoreSelected = !empty($MeritStudyScore) && !empty($i) && abs($MeritStudyScore)==$i? " selected" : "";
							$this->infoArr["UIContent"]["StudyScoreOption"] .= "<option value='".$i."' ".$thisStudyScoreSelected.">".$i."</option>\n";
						}
					}
					
					// Get Activity Score Options
					if($discipline_api->UseActScore) {
						for ($i=0; $i<=$discipline_api->ActScoreIncrement_MAX; $i++) {
							$thisActivityScoreSelected = !empty($MeritActivityScore) && !empty($i) && abs($MeritActivityScore)==$i? " selected" : "";
							$this->infoArr["UIContent"]["ActivityScoreOption"] .= "<option value='".$i."' ".$thisActivityScoreSelected.">".$i."</option>\n";
						}
					}

                    // [2019-1105-1700-25235] Get Subject Name Options
                    if ($discipline_api->use_subject)
                    {
                        $MeritSubject = $this->isEJ() ? $MeritSubjectName : $MeritSubjectID;
                        $this->infoArr["UIContent"]["SubjectNameOption"] = $discipline_api->getSubjectNameDropDownOption($MeritSubject);
                    }
					
					// Get PICs Options
					$this->infoArr["UIContent"]["PICOption"] = "";
					if(sizeof($MeritPICs) > 0) {
						$thisMeritPICs = explode(",", $MeritPICs);
						foreach((array)$thisMeritPICs as $thisPIC) {
							if($thisPIC > 0) {
								$thisPICName = $discipline_api->getUserNameByID($thisPIC);
								$thisPICName = $thisPICName[0];
								$this->infoArr["UIContent"]["PICOption"] .= " <option value=\"".$thisPIC."\" selected>".$thisPICName."</option> \n\r ";
							}
						}
					}
					
					// Get Record Status Options
					list($RecordStatusOption, $ReleaseStatusOption) = $discipline_api->getMeritStatusDropDown($MeritRecord);
					$this->infoArr["UIContent"]["RecordStatusOption"] = $RecordStatusOption;
					$this->infoArr["UIContent"]["ReleaseStatusOption"] = $ReleaseStatusOption;
					
					// Get Hidden Fields
					$this->infoArr["UIContent"]["HiddenInputField"] = "<input type=\"hidden\" name=\"MeritID[]\" value=\"".$RecordID."\">\n";
					
//					$AccessItem = 0;
//					if($discipline_api->IS_ADMIN_USER()) {
//						$AccessItem = 1;
//						$HasApprovalRight = true;
//						$CanApproveRecord = true;
//					}
//					else {
//						// Access Merit Items
//						$AccessCatItems = $discipline_api->Retrieve_User_Can_Access_AP_Category_Item($_SESSION["UserID"], "item");
//						if($AccessCatItems[$MeritCatID]) {
//							$AccessItem = in_array($MeritItemID, $AccessCatItems[$MeritCatID]);
//						}
//						
//						// Has Approval Right
//						$HasRightToRejectRecord = $discipline_api->RETURN_AP_RECORDID_CAN_BE_APPROVED($_SESSION["UserID"], "");
//						$HasApprovalRight = (sizeof($HasRightToRejectRecord)>0) ? true : false;
//						$CanApproveRecord = $discipline_api->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($RecordID);
//					}

					# Check if User can access Item
					if(!$this->isEJ())
					{
						$AccessItem = 0;
						if($discipline_api->IS_ADMIN_USER($UserID)) {
							$AccessItem = 1;
						}
						else {
							$AccessItemList = $discipline_api->Retrieve_User_Can_Access_AP_Category_Item($UserID, 'item');
							if($AccessItemList[$MeritCatID]) {
								$AccessItem = in_array($MeritItemID, $AccessItemList[$MeritCatID]);
							}
						}
					}
					else
					{
						$AccessItem = 1;
					}
					
					# Check if Edit Status Mode
					$isOwnRelatedRecords = $discipline_api->isMeritRecordPIC($RecordID) || $discipline_api->isMeritRecordOwn($RecordID);
					$HasEditRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || ($isOwnRelatedRecords && $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"));
					$HasApprovalRight = $this->isEJ()? $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") : $discipline_api->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($RecordID);
					$HasReleaseRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release");
					$HasWaiveRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive");
					$EditStatusOnly = !$HasEditRight && ($HasApprovalRight || $HasReleaseRight || $HasWaiveRight);
					
					# Disable Merit Details Fields > Edit Status Mode / Cannot access Item / Upgraded from Conduct Records / Related to Case Record / Wavied Merit Records
					$this->infoArr["PageData"]["DisableDetailsFields"] = ($EditStatusOnly || !$AccessItem || $MeritFromConducts || $MeritFromCase || $MeritWaived)? " disabled" : "";
					
					# Disable Event Date Fields > Edit Status Mode / Upgraded from Conduct Records / Related to Case Record / Wavied Merit Records
					$this->infoArr["PageData"]["DisableDateField"] = ($EditStatusOnly || $MeritFromConducts || $MeritFromCase || $MeritWaived)? " disabled" : "";
					
					# Disable PIC Search Field > System Setting - Only allow admin to select record's PIC
					$this->infoArr["PageData"]["HiddenPICSelect"] = $discipline_api->OnlyAdminSelectPIC && !$discipline_api->IS_ADMIN_USER($_SESSION["UserID"]);
					$this->infoArr["PageData"]["DisablePICField"] = $this->infoArr["PageData"]["HiddenPICSelect"]? " disabled" : "";
					
					# Disable PIC & Remarks Fields > Edit Status Mode
					$this->infoArr["PageData"]["HiddenPICSelect"] = $EditStatusOnly? true : $this->infoArr["PageData"]["HiddenPICSelect"];
					$this->infoArr["PageData"]["DisablePICField"] = $EditStatusOnly? " disabled" : $this->infoArr["PageData"]["DisablePICField"];
					$this->infoArr["PageData"]["DisableRemarkFields"] = $EditStatusOnly? " disabled" : "";
				}
				// Set Error Message
				else {
					$jsStatus = "fail";
				}
			break;
		}
		
		# Get Web View
		$responseHTML = $this->view($methodData);
		if($this->isEJ()) {
			$responseHTML = convert2unicode($responseHTML, 1, 1);
		}
		
		# Set up defualt JSON return
		$param = array(
			"hash" => $this->infoArr["strHash"],
			"pageType" => $method,
			"respType" => "HTML",
			"content" => $responseHTML,
			"customJS" => "",
			"pData" => $this->infoArr["postData"],
			"status" => $jsStatus,
			"error" => $jsError
		);
		
		/**** Perform AJAX required Actions ****/
		switch ($method) {
			
			# Merit Table Data
			case "mydata";
				# Check View Right
				$HasViewRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View");
				if(!$HasViewRight) {
					$this->infoArr["postData"]["ViewOwnClassOnly"] = true;
					$this->infoArr["PageData"]["ViewOwnClassOnly"] = true;
				}
				
				# Check if User have Approval Right
//				$HasApprovalRight = "";
//				if(!$discipline_api->IS_ADMIN_USER()) {
//					$HasRightToRejectRecord = $discipline_api->RETURN_AP_RECORDID_CAN_BE_APPROVED($_SESSION["UserID"], "");
//					$HasApprovalRight = (sizeof($HasRightToRejectRecord)>0) ? true : false;
//				}
				
				// Get filtered Merits
				$data = array();
				$MeritRecords = $discipline_api->GetMeritRecord($this->infoArr["postData"]);
				for($i=0; $i<count($MeritRecords); $i++) {
					$rowArr = array();
					
					// Get Merit Info
					$thisMeritInfo = $MeritRecords[$i];
					$rowArr[0] = $thisMeritInfo["ItemName"];
					$rowArr[1] = $thisMeritInfo["ClassNameNum"];
					$rowArr[2] = $thisMeritInfo["StudentName"];
					$rowArr[3] = $thisMeritInfo["RecordDate"];
					$rowArr[4] = $thisMeritInfo["MeritTypeImg"]." ".$thisMeritInfo["ReceivedMeritType"];
					$rowArr[8] = $thisMeritInfo["ModifiedDate"];
					$rowArr[9] = $thisMeritInfo["ReleaseStatusImg"]."&nbsp;&nbsp;&nbsp;".$thisMeritInfo["RecordStatusImg"];
					
					// Get Merit Actions
					$thisMeritAction = $discipline_api->GetMeritAction($thisMeritInfo);
					$rowArr[5] = $thisMeritAction["RecordReference"];
					$rowArr[6] = $thisMeritAction["RecordAction"];
					$rowArr[7] = $thisMeritAction["RecordPICs"];
					
					// Get Action Buttons
					$rowArr[10] = $discipline_api->GetMeritActionButtons($thisMeritInfo);
					
					# Convert to Unicode (EJ)
					if($this->isEJ()) {
						foreach ((array)$rowArr as $thisIndex => $thisValue) {
							$thisValue = convert2unicode($thisValue, 1, 1);
							$rowArr[$thisIndex] = $thisValue;
						}
					}
					$data[] = $rowArr;
				}
				$param = array("data" => $data);
			break;
			
			# Semester Data
			case "selectYear":
				global $i_Discipline_System_Award_Punishment_Whole_Year;
				
				// Get Semester List
				$this_yearid = $this->infoArr["postData"]["val"];
				if($this_yearid > 0) {
					$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$this_yearid."' ORDER BY TermStart";
					$semesterList = $discipline_api->returnArray($sql, 3);
				}
				
				$data = array();
				$data[] = array(
									"label" => "WholeYear",
									"name" => $i_Discipline_System_Award_Punishment_Whole_Year
								);
				foreach((array)$semesterList as $this_semester){
					$data[] = array(
										"label" => $this_semester["YearTermID"],
										"name" => Get_Lang_Selection($this_semester["YearTermNameB5"], $this_semester["YearTermNameEN"])
									);
				}
				
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$data as $thisIndex => $thisData) {
						$thisValue = $thisData["name"];
						$thisValue = convert2unicode($thisValue, 1, 1);
						$data[$thisIndex]["name"] = $thisValue;
					}
				}
				$param["resultData"] = $data;
			break;
			
			# Merit Category Data
			case "ap_type_select":
				$MeritType = $this->infoArr["postData"]["val"];
				if($MeritType == "1" || $MeritType == "-1") {
					$UserID = $_SESSION["UserID"];
					$CategoryList = $discipline_api->returnMeritItemCategoryByType($MeritType);
				}
				
				$data = array();
				foreach((array)$CategoryList as $thisCategory) {
					$data[] = array(
									"label" => $thisCategory["CatID"],
									"name"  => $thisCategory["CategoryName"]
								);
				}
			
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$data as $thisIndex => $thisData) {
						$thisValue = $thisData["name"];
						$thisValue = convert2unicode($thisValue, 1, 1);
						$data[$thisIndex]["name"] = $thisValue;
					}
				}
				$param["resultData"] = $data;
			break;
			
			# Merit Item Data
			case "ap_category_select":
				$MeritCategory = $this->infoArr["postData"]["val"];
				if($MeritCategory > 0) {
					$UserID = $_SESSION["UserID"];
					$CategoryItemList = $discipline_api->retrieveMeritItemswithCodeGroupByCat();
					$CategoryItemList = BuildMultiKeyAssoc((array)$CategoryItemList, array("CatID", "ItemID"), array("1", "2"));
					$CategoryItemList = $CategoryItemList[$MeritCategory];
				}
				
				$data = array();
				foreach((array)$CategoryItemList as $thisItem) {
					$data[] = array(
										"label" => $thisItem[1],
										"name"  => $thisItem[2]
									);
				}
				
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$data as $thisIndex => $thisData) {
						$thisValue = $thisData["name"];
						$thisValue = convert2unicode($thisValue, 1, 1);
						$data[$thisIndex]["name"] = $thisValue;
					}
				}
				$param["resultData"] = $data;
			break;
			
			# Merit Item Details
			case "ap_item_select";
				$conduct_item = $this->infoArr["postData"]["target_id"];
				if($conduct_item > 0) {
					$UserID = $_SESSION["UserID"];
					$item_details = $discipline_api->retrieveMeritItemswithCodeGroupByCat();
					$item_details = BuildMultiKeyAssoc((array)$item_details, array("ItemID"));
					$item_details = $item_details[$conduct_item];
					$item_details["NumOfMerit"] = my_round($item_details["NumOfMerit"], $Decimal=2);
					$item_details["ConductScore"] = my_round($item_details["ConductScore"], $Decimal=1);
				}
				
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$item_details as $thisKey => $thisValue) {
						$thisValue = convert2unicode($thisValue, 1, 1);
						$item_details[$thisKey] = $thisValue;
					}
				}
				$param["resultData"] = $item_details;
			break;
			
			# Notice Template Data
			case "notice_category":
				$NoticeCategory = $this->infoArr["postData"]["val"];
				if(!empty($NoticeCategory)) {
					$NoticeTemplates = $discipline_api->getDetentionENoticeByCategoryID("DISCIPLINE", $NoticeCategory);
				}
				
				$data = array();
				foreach((array)$NoticeTemplates as $thisTemplate) {
					$data[] = array(
										"label" => $thisTemplate[0],
										"name"  => $thisTemplate[1]
									);
				}
				
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$data as $thisIndex => $thisData) {
						$thisValue = $thisData["name"];
						$thisValue = convert2unicode($thisValue, 1, 1);
						$data[$thisIndex]["name"] = $thisValue;
					}
				}
				$param["resultData"] = $data;
			break;
			
			# Update Merit - Assign Notice
			case "edit_complete":
				# Empty Form Data Checking
				$thisFormData = $this->infoArr["postData"]["formData"];
				if(empty($thisFormData)) {
					$jsErrorMsg = "noFormData";
				}
				# Perform Form Data Checking > New Records
				else {
					$jsErrorMsg = $discipline_api->assignRecordNoticeDataChecking($thisFormData);
				}
				
				# Data Checking : Success
				if(empty($jsErrorMsg)) {
					$discipline_api->assignMeritNoticeByAPI($thisFormData);
					$success = true;
				}
				# Data Checking : Fail
				else {
					$jsError = $jsErrorMsg;
				}
				$param["status"] = $success? "success" : "fail";
				$param["error"] = $jsError;
			break;
			
			# Update Merit - Info + Record Status
			case "edit_record_update":
				# Empty Form Data Checking
				$thisFormData = $this->infoArr["postData"]["formData"];
				if(empty($thisFormData)) {
					$jsErrorMsg = "noFormData";
				}
				# Perform Form Data Checking
				else {
					# Check if Edit Status Mode
					$thisMeritRecordID = $thisFormData["MeritID"][0];
					$isOwnRelatedRecords = $discipline_api->isMeritRecordPIC($thisMeritRecordID) || $discipline_api->isMeritRecordOwn($thisMeritRecordID);
					$HasEditRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || ($isOwnRelatedRecords && $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"));
					$HasApprovalRight = $this->isEJ()? $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") : $discipline_api->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($thisMeritRecordID);
					$HasReleaseRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release");
					$HasWaiveRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive");
					$EditStatusOnly = !$HasEditRight && ($HasApprovalRight || $HasReleaseRight || $HasWaiveRight);
					
					# Allow edit Record Status only > No need to update Record Info
					if($EditStatusOnly) {
						// do nothing 
					}
					# Update Merit Record
					else {
						$jsErrorMsg = $discipline_api->updateMeritRecordByAPI($thisFormData);
					}
				}
				
				# Data Checking : Success > Change Merit Record Status
				if(empty($jsErrorMsg)) {
					# Update Merit Record Status
					$discipline_api->updateMeritRecordStatusByAPI($thisFormData);
					$success = true;
				}
				# Data Checking : Fail
				else {
					// Set Error Message
					$jsError = $jsErrorMsg;
				}
				$param["status"] = $success? "success" : "fail";
				$param["error"] = $jsError;
			break;
			
			# Delete Merit
			case "delete":
				$thisRecordID = $this->infoArr["postData"]["target_id"];
				if($thisRecordID > 0) {
					$success = $discipline_api->deleteMeritRecordByAPI($thisRecordID);
				}
				$param["status"] = $success? "success" : "fail";
			break;
		}
		
		# Output JSON return
		sleep(1);
		$this->outputJSON($param);
	}
	
	private $disciplineAPI;
	private function getDisciplineApiConnection()
	{
		global $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		if(isset($this->disciplineAPI)) {
			return $this->disciplineAPI;
		}
		else {
			include_once $intranet_root."/includes/libdisciplinev12.php";
			include_once $intranet_root."/includes/libdisciplinev12_app_api.php";
			$PATH_WRT_ROOT = $intranet_root."/";
			
			intranet_opendb();
			
			$disciplineAPI = new libdisciplinev12_app_api();
			$disciplineAPI->controllerModule = "award_punishment";
			$disciplineAPI->setDisciplineImagePath($this->infoArr["thisImagePath"]);
			
			// Get User Access Right
			$disciplineAPI->UserAccessRightList = $disciplineAPI->retrieveUserAccessRight($_SESSION['UserID']);
			session_register_intranet("SESSION_ACCESS_RIGHT", $disciplineAPI->UserAccessRightList);
			
			return $this->disciplineAPI = $disciplineAPI;
		}
	}
	
	private function closeDisciplineApiConnection()
	{
		intranet_closedb();
	}
}