<?php

class Management extends Controller
{
	public function __construct($info = array())
	{
		parent::__construct($info);
	}
	
	public function __destruct()
	{
		if(isset($this->disciplineAPI)) {
			$this->closeDisciplineApiConnection();
		}
	}
	
	private function isEJ()
	{
		global $junior_mck;
		return isset($junior_mck);
	}
	
	public function _remap($method)
	{
		global $Lang, $eDiscipline;
		
		$discipline_api = $this->getDisciplineApiConnection();
		$SESSION_ACCESS_RIGHT = $discipline_api->UserAccessRightList;
		
//		global $SESSION_ACCESS_RIGHT;
//		$this->infoArr["postData"]['Right'] = $SESSION_ACCESS_RIGHT;
//		debug_pr($_SESSION['SESSION_ACCESS_RIGHT']);
//		debug_pr($SESSION_ACCESS_RIGHT);
		
		$method = $this->str_replace_first("cus_", "", $method);
		$methodData = array (
			"controllerLink" => "ediscipline/goodconduct_misconduct/management",
			"pageLimit" => isset($_POST["cus"]["limit"])? $_POST["cus"]["limit"] : 10,
			"currPage" => isset($_POST["cus"]["page"])? $_POST["cus"]["page"] : 1
		);
		$this->infoArr["postData"]["limit"] = $methodData["pageLimit"];
		$this->infoArr["postData"]["page"] = $methodData["currPage"]; 
		
		$jsStatus = "success"; 	// success => print $method html, fail => return to orignal page (default success)
		$jsError = "";
		
		/**** Set up Access Web View required Data ****/
		switch ($method) {
			
			# Conduct Table List
			case "mylist":
				global $i_Discipline_Detention_All_Teachers;
				
				# Unset unwanted POST data
				if(isset($_POST) && isset($_POST["sld_student"])) {
					unset($_POST["sld_student"]);
					unset($this->infoArr["postData"]["formData"]["sld_student"]);
				}
				
				# School Year Selection
				$CurrentYearID = $this->isEJ()? getCurrentAcademicYear() : Get_Current_Academic_Year_ID();
				$selectYear = $this->infoArr["postData"]["formData"]["selectYear"];
				if($this->isEJ()) {
					$selectYear = convert2unicode($selectYear, 1, 0);
				}
				$selectYear = $selectYear == "" ? $CurrentYearID : $selectYear;
				$selectSchoolYear = $discipline_api->GetYearWebViewSelection($selectYear, $discipline_api->controllerModule);
				
				# Semester Selection
				$selectSemester = $this->infoArr["postData"]["formData"]["selectSemester"];
				if($this->isEJ()) {
					$selectSemester = convert2unicode($selectSemester, 1, 0);
				}
				if ($selectSemester == "") {
					$selectSemester = "WholeYear";
				}
				$selectYearTerm = $discipline_api->GetSemesterWebViewSelection($selectYear, $selectSemester, $discipline_api->controllerModule);
				
				# Class Selection
				$selectClass = $this->infoArr["postData"]["formData"]["selectClass"];
				if($this->isEJ()) {
					$selectClass = convert2unicode($selectClass, 1, 0);
				}
				$selectYearClass = $discipline_api->GetClassWebViewSelection($selectClass, $selectYear, $discipline_api->controllerModule);
				
				# PIC Selection
				$selectPIC = $this->infoArr["postData"]["formData"]["selectPIC"];
				$PICArray = $discipline_api->getPICInArray("DISCIPLINE_ACCU_RECORD", $selectYear, $selectSemester);
				if(sizeof($PICArray)==0) {
					$PICArray = array();
				}
				// Default PIC (Current Users)
				if(empty($selectPIC) && $discipline_api->PIC_SelectionDefaultOwn) {
					for($i=0; $i<sizeof($PICArray); $i++) {
						if($PICArray[$i][0] == $_SESSION["UserID"]) {
							$selectPIC = $_SESSION["UserID"];
							break;
						}
					}
				}
				$selectPICTeacher = getSelectByArray($PICArray," id=\"selectPIC\" name=\"selectPIC\" ", $selectPIC, 0, 0, "".$i_Discipline_Detention_All_Teachers."");
				
				# Check Access Right - View Records
				$HasNewRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New");
				
				$this->infoArr["UIContent"]["SchoolYearSelection"] = $selectSchoolYear;
				$this->infoArr["UIContent"]["SemesterSelection"] = $selectYearTerm;
				$this->infoArr["UIContent"]["ClassSelection"] = $selectYearClass;
				$this->infoArr["UIContent"]["PICSelection"] = $selectPICTeacher;
				$this->infoArr["PageData"]["AddAPAccessRight"] = $HasNewRight;
			break;
			
			# Record Details
			case "detail":
				$ConductID = $this->infoArr["postData"]["targetid"];
				if(!empty($ConductID))
				{
					// Get Conduct Record Details
					$ConductRecord = $discipline_api->GetConductRecord("", $ConductID);
					if(!empty($ConductRecord))
					{
						# Set Data for Display
						$ConductRecord["RecordStatus"] = $ConductRecord["RecordStatusImg"]." ".$ConductRecord["RecordStatusName"];
						$ConductRecord["Remark"] = str_replace("\n", "<br/>", $ConductRecord["Remark"]);
						
						// Get Conduct Actions
						$thisConductAction = $discipline_api->GetConductAction($ConductRecord);
						$ConductRecord["RecordAction"] = $thisConductAction["RecordAction"];
						$ConductRecord["RecordAction"] = str_replace("<br/>", ", ", $ConductRecord["RecordAction"]);
						$ConductRecord["RecordPICs"] = $thisConductAction["RecordPICs"];
						
						// Get Conduct Notice Content
						$NoticeID = $ConductRecord["NoticeID"];
						if(!empty($NoticeID)) {
							$RecordNoticeContent = $discipline_api->GetNoticeByNoticeID($NoticeID);
						}
						
						# Check Access Right - Edit Conduct
						$isOwnRelatedRecords = $discipline_api->isConductPIC($ConductID) || $discipline_api->isConductOwn($ConductID);
						$HasEditRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || ($isOwnRelatedRecords && $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn"));
						$this->infoArr["PageData"]["EditGMAccessRight"] = $HasEditRight;
						
						# Check Access Right - Delete Conduct
						$HasDeleteRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteAll") || ($isOwnRelatedRecords && $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteOwn"));
						
						# Not Allow Delete - (from Attendance / Waived)
						$CategoryID = $ConductRecord["CategoryID"];
						$RecordStatus = $ConductRecord["RecordStatus"];
						if($HasDeleteRight && ($CategoryID == 1 || $RecordStatus == 2)) {
							$HasDeleteRight = false;
						}
						$this->infoArr["PageData"]["DeleteGMAccessRight"] = $HasDeleteRight;
					}
				}
				
				// Store Conduct Info
				if(!empty($ConductRecord)) {
					foreach($ConductRecord as $key => $data) {
						$ConductRecord[$key] = empty($data)? "---" : $data;
					}
					$this->infoArr["PageData"]["RecordInfo"] = $ConductRecord;
					unset($ConductRecord);
				}
				if(!empty($RecordNoticeContent)) {
					foreach($RecordNoticeContent as $key => $data) {
						$RecordNoticeContent[$key] = empty($data)? "&nbsp;" : $data;
					}
					$this->infoArr["PageData"]["NoticeInfo"] = $RecordNoticeContent;
					unset($RecordNoticeContent);
				}
			break;
			
			# Add Record - Step 1 (Student)
			case "edit":
				// Get Student Options when back from Step 2
				$selected_student = $this->infoArr["postData"]["formData"]["sld_student"];
				if(!empty($selected_student)) {
					$selected_student = array_unique((array)$selected_student);
					$this->infoArr["UIContent"]["StudentSelection"] = $discipline_api->getTargetUserDropDownOption($selected_student);
				}
				
				# Unset POST Data
				if(isset($_POST))
				{
					if(isset($_POST["targetid"])) {
						unset($_POST["targetid"]);
						unset($this->infoArr["postData"]["targetid"]);
					}
					if(isset($_POST["sld_student"])) {
						unset($_POST["sld_student"]);
						unset($this->infoArr["postData"]["formData"]["sld_student"]);
					}
				}
			break;
			
			# Add Record - Step 2 (Record Details)
			case "edit_step2":
				# Check Empty Student
				$selected_student = $this->infoArr["postData"]["formData"]["sld_student"];
				if(empty($selected_student))
				{
					// Set Error Message: No Students
					$jsError = "noSelectedStudents";
					$jsStatus = "fail";
				}
				else
				{
					$StudentCount = 0;
					$RecordSummaryAry = array();
					
					// Get Summary Table Content
					$selected_student = array_unique((array)$selected_student);
					$isSingleRecordOnly = count((array)$selected_student) == 1;
					foreach((array)$selected_student as $thisStudentID)
					{
						$thisStudentInfo = $discipline_api->getStudentNameByID($thisStudentID);
						$thisStudentInfo = $thisStudentInfo[0];
						if(!empty($thisStudentInfo))
						{
							$thisStudentName = $thisStudentInfo[1];
							$thisClassNameNum = $thisStudentInfo[2]." - ".$thisStudentInfo[3];
							if($isSingleRecordOnly) {
								$RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)";
							}
							else {
								++$StudentCount;
								$RecordSummaryAry[] = "$StudentCount. $thisStudentName ($thisClassNameNum)";
							}
						}
					}
					$this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
					
					// Get Semester Selection
					if($this->isEJ() && getSemesterMode()==1) {
						$this->InfoArr["UIContent"]["SemesterSelect"] = $discipline_api->GetSemesterWebViewSelection("", getCurrentSemester(), $discipline_api->controllerModule, true);
					}
					
					// Get PIC Preset Options
					$thisUserName = $discipline_api->getUserNameByID($_SESSION["UserID"]);
					$thisUserName = $thisUserName[0];
					$this->infoArr["UIContent"]["PICDefultOption"] = " <option value=\"".$_SESSION["UserID"]."\" selected>".$thisUserName."</option> \n\r ";
					
					// Get Hidden Fields
					$student_field = "";
					foreach((array)$selected_student as $thisStudentID) {
						$student_field .= "<input type=\"hidden\" name=\"sld_student[]\" value=\"".$thisStudentID."\">\n";
					}
					$this->infoArr["UIContent"]["HiddenInputField"] = $student_field;
					
					# Hide PIC Search Field > System Setting - Only allow admin to select record's PIC
					# Hide PIC Search Field (EJ) > Not Support PIC Selection
					$this->infoArr["PageData"]["HiddenPICSelect"] = $this->isEJ() || ($discipline_api->OnlyAdminSelectPIC && !$discipline_api->IS_ADMIN_USER($_SESSION["UserID"]));
				}
			break;
			
			# Add Record - Step 3 (Follow-ups) / Edit Record - Assign Notice
			case "edit_step3":
				// POST Data (Add Record > "formData" / Assign Notice > "targetid") 
				$thisFormData = $this->infoArr["postData"]["formData"];
				$thisRecordID = $this->infoArr["postData"]["targetid"];
				$IsNewConductRecord = empty($thisRecordID);
				
				# Perform Form Data Checking > New Records
				if($IsNewConductRecord && !empty($thisFormData)) {
					$jsErrorMsg = $discipline_api->insertConductRecordDataChecking($thisFormData);
				}
				# Empty Data Checking
				else if($IsNewConductRecord && empty($thisFormData)) {
					$jsErrorMsg = "noFormData";
				}
				
				# Data Checking : Success => Assign Notice Page
				if(empty($jsErrorMsg))
				{
					# Insert Conduct Records
					if($IsNewConductRecord) {
						$ConductIDAry = $discipline_api->insertConductRecordByAPI($thisFormData);
					}
					# Get Conduct Data for Processing
					else {
						$ConductIDAry = array($thisRecordID);
						$ConductInfo = $discipline_api->GetConductRecord("", $thisRecordID);
						
						$thisFormData = array();
						$thisFormData["gm_category_select"] = $ConductInfo["CategoryID"];
					}
					
					// Get Page Navigation
					$this->infoArr["UIContent"]["PageNavigation"][0] = $IsNewConductRecord? $eDiscipline['NewRecord'] : $Lang['eDiscipline']['App']['RecordDetails'];
					$this->infoArr["UIContent"]["PageNavigation"][1] = $IsNewConductRecord? $Lang['General']['Steps']."3: ".$eDiscipline['SelectActions'] : $eDiscipline['SelectActions'];
					
					// Get Summary Table Content
					$ConductCount = 0;
					$RecordSummaryAry = array();
					$isSingleRecordOnly = count((array)$ConductIDAry) == 1;
					foreach((array)$ConductIDAry as $thisConductID) {
						$thisConductInfo = $discipline_api->GetConductRecord("", $thisConductID);
						if(!empty($thisConductInfo)) {
							$thisStudentName = $thisConductInfo["StudentName"];
							$thisClassNameNum = $thisConductInfo["ClassNameNum"];
							$thisRecordTypeImg = $thisConductInfo["RecordTypeImg"];
							$thisItemName = $thisConductInfo["ItemName"];
							if($isSingleRecordOnly) {
								$RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)<br/>$thisRecordTypeImg $thisItemName";
							}
							else {
								++$ConductCount;
								$RecordSummaryAry[] = "$ConductCount. $thisStudentName ($thisClassNameNum)<br/>$thisRecordTypeImg $thisItemName";
							}
						}
					}
					$this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
					
					// Get Notice Template Options
					$this->infoArr["UIContent"]["NoticeOptions"] = $discipline_api->getSelectNoticeDropDown($thisFormData);
					
					// Get Hidden Fields
					$this->infoArr["UIContent"]["HiddenInputField"] = "";
					foreach((array)$ConductIDAry as $thisConductID) {
						$this->infoArr["UIContent"]["HiddenInputField"] .= "<input type=\"hidden\" name=\"ConductID[]\" value=\"".$thisConductID."\">\n";
					}
				}
				# Data Checking : Fail
				else {
					// Set Error Message
					$jsError = $jsErrorMsg;
					$jsStatus = "fail";
				}
			break;
			
			# Edit Record
			case "edit_record":
				$ConductID = $this->infoArr["postData"]["targetid"];
				if(!empty($ConductID))
				{
					// Get Conduct Info
					$ConductRecord = $discipline_api->GetConductRecord("", $ConductID);
					$ConductSemester = $ConductRecord["Semester"];
					$ConductCategory = $ConductRecord["CategoryID"];
					$ConductItem = $ConductRecord["ItemID"];
					$ConductPICs = $ConductRecord["PICs"];
					
					// Set Data for Display
					$ConductCatItem = $ConductRecord["ItemName"];
					list($ConductCatName, $ConductItemName) = explode(" - ", $ConductCatItem);
					$ConductRecord["CategoryName"] = $ConductCatName;
					$ConductRecord["Remark"] = $ConductRecord["Remark"]=="---"? "" : $ConductRecord["Remark"];
					$this->infoArr["PageData"]["ConductRecord"] = $ConductRecord;
					
					// Get Summary Table Content
					$thisStudentName = $ConductRecord["StudentName"];
					$thisClassNameNum = $ConductRecord["ClassNameNum"];
					$this->infoArr["UIContent"]["SummaryTable"] = "$thisStudentName ($thisClassNameNum)";
					
					// Get Semester Options
					$this->InfoArr["UIContent"]["SemesterSelect"] = "";
					if($this->isEJ() && getSemesterMode()==1) {
						$this->InfoArr["UIContent"]["SemesterSelect"] = " <option value=\"\" disabled selected>".$ConductSemester."</option> \n\r ";
					}
					
					// Get Item Options (except Late Misconduct)
					$IsLateConduct = $ConductCategory == 1;
					$this->infoArr["UIContent"]["ConductItemOption"] = "";
					if($ConductCategory && !$IsLateConduct)
					{
						$UserID = $_SESSION["UserID"];
						$ConductCatItems = $discipline_api->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT();
						$ConductCatItems = BuildMultiKeyAssoc((array)$ConductCatItems, array("0", "1"), array("1", "2"));
						$ConductCatItems = $ConductCatItems[$ConductCategory];
						foreach((array)$ConductCatItems as $thisConductItem) {
							list($thisItemCat, $thisItemID, $thisItemName) = $thisConductItem;
							$thisItemSelected = !empty($thisItemID) && $thisItemID==$ConductItem? " selected" : "";
							$this->infoArr["UIContent"]["ConductItemOption"] .= " <option value=\"".$thisItemID."\" ".$thisItemSelected.">".$thisItemName."</option> \n\r ";
						}
						
						// Load Current Item if cannot access any Category Items
						if(empty($this->infoArr["UIContent"]["ConductItemOption"])) {
							$this->infoArr["UIContent"]["ConductItemOption"] .= " <option value=\"".$ConductItem."\" selected>".$ConductItemName."</option> \n\r ";
						}
					}
					
					// Get PIC Options
					$this->infoArr["UIContent"]["PICOption"] = "";
					if(sizeof($ConductPICs) > 0) {
						$thisConductPICs = explode(",", $ConductPICs);
						foreach((array)$thisConductPICs as $thisPIC) {
							$thisPICName = $discipline_api->getUserNameByID($thisPIC);
							$thisPICName = $thisPICName[0];
							$this->infoArr["UIContent"]["PICOption"] .= " <option value=\"".$thisPIC."\" selected>".$thisPICName."</option> \n\r ";
						}
					}
					
					// Get Record Status Options
					$this->infoArr["UIContent"]["RecordStatusOption"] = $discipline_api->getRecordStatusDropDown($ConductRecord["RecordStatus"]);
					
					// Get Hidden Fields
					$this->infoArr["UIContent"]["HiddenInputField"] = "<input type=\"hidden\" name=\"ConductID[]\" value=\"".$ConductID."\">\n";
					
					# Disable all Conduct Fields > Edit Status Mode
					$HasEditRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || (($discipline_api->isConductPIC($ConductID) || $discipline_api->isConductOwn($ConductID)) && $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn"));
					$HasApprovalRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval");
					$EditStatusOnly = !$HasEditRight && $HasApprovalRight;
					$this->infoArr["PageData"]["DisableAllFields"] = $EditStatusOnly? " disabled" : "";
					
					# Hide Item Field > Late Misconduct
					$this->infoArr["PageData"]["HiddenItemSelect"] = $IsLateConduct;
					
					# Hide PIC Search Field > System Setting - Only allow admin to select record's PIC
					# Hide PIC Search Field (EJ) > Not Support PIC Selection
					$this->infoArr["PageData"]["HiddenPICSelect"] = $this->isEJ() || ($discipline_api->OnlyAdminSelectPIC && !$discipline_api->IS_ADMIN_USER($_SESSION["UserID"]));
					$this->infoArr["PageData"]["DisablePICField"] = $this->infoArr["PageData"]["HiddenPICSelect"]? " disabled" : "";
				}
				// Set Error Message
				else {
					$jsStatus = "fail";
				}
			break;
		}
		
		# Get Web View
		$responseHTML = $this->view($methodData);
		if($this->isEJ()) {
			$responseHTML = convert2unicode($responseHTML, 1, 1);
		}
		
		# Set up defualt JSON return
		$param = array(
			"hash" => $this->infoArr["strHash"],
			"pageType" => $method,
			"respType" => "HTML",
			"content" => $responseHTML,
			"customJS" => "",
			"pData" => $this->infoArr["postData"],
			"status" => $jsStatus,
			"error" => $jsError
		);
		
		/**** Perform AJAX required Actions ****/
		switch ($method) {
			
			# Conduct Table Data
			case "mydata";
				# Check View Right
				$HasViewRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View");
				if(!$HasViewRight) {
					$this->infoArr["postData"]["ViewOwnClassOnly"] = true;
					$this->infoArr["PageData"]["ViewOwnClassOnly"] = true;
				}
				
				// Get filtered Conducts
				$data = array();
				$ConductRecords = $discipline_api->GetConductRecord($this->infoArr["postData"]);
				for($i=0; $i<count($ConductRecords); $i++) {
					$rowArr = array();
					
					// Get Conduct Info
					$thisConductInfo = $ConductRecords[$i];
					$rowArr[0] = $thisConductInfo["ItemName"];
					$rowArr[1] = $thisConductInfo["ClassNameNum"];
					$rowArr[2] = $thisConductInfo["StudentName"];
					$rowArr[3] = $thisConductInfo["RecordDate"];
					$rowArr[4] = $thisConductInfo["RecordTypeImg"]." ".$thisConductInfo["RecordTypeName"];
					$rowArr[7] = $thisConductInfo["ModifiedDate"];
					$rowArr[8] = $thisConductInfo["RecordStatusImg"];
					
					// Update Itme Name Display - Late Misconduct with Late Time Remark
					if($thisConductInfo["CategoryID"]==1 && $thisConductInfo["Remark"]!="" && $thisConductInfo["Remark"]!="---") {
						$rowArr[0] .= " (".$thisConductInfo["Remark"].")";
					}
					
					// Get Conduct Actions
					$thisConductAction = $discipline_api->GetConductAction($thisConductInfo);
					$rowArr[5] = $thisConductAction["RecordAction"];
					$rowArr[6] = $thisConductAction["RecordPICs"];
					
					// Get Action Buttons
					$rowArr[9] = $discipline_api->GetConductStatusButtons($thisConductInfo);
					
					# Convert to Unicode (EJ)
					if($this->isEJ()) {
						foreach ((array)$rowArr as $thisIndex => $thisValue) {
							$thisValue = convert2unicode($thisValue, 1, 1);
							$rowArr[$thisIndex] = $thisValue;
						}
					}
					$data[] = $rowArr;
				}
				$param = array("data" => $data);
			break;
			
			# Semester Data
			case "selectYear":
				global $i_Discipline_System_Award_Punishment_Whole_Year;
				
				// Get Semester List
				$this_yearid = $this->infoArr["postData"]["val"];
				if($this_yearid > 0) {
					$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$this_yearid."' ORDER BY TermStart";
					$semesterList = $discipline_api->returnArray($sql, 3);
				}
				
				// Get Semester Data
				$data = array();
				$data[] = array(
									"label" => "WholeYear",
									"name" => $i_Discipline_System_Award_Punishment_Whole_Year
								);
				foreach((array)$semesterList as $this_semester) {
					$data[] = array(
										"label" => $this_semester["YearTermID"],
										"name" => Get_Lang_Selection($this_semester["YearTermNameB5"], $this_semester["YearTermNameEN"])
									);
				}
				
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$data as $thisIndex => $thisData) {
						$thisValue = $thisData["name"];
						$thisValue = convert2unicode($thisValue, 1, 1);
						$data[$thisIndex]["name"] = $thisValue;
					}
				}
				$param["resultData"] = $data;
			break;
			
			# Conduct Category Data
			case "gm_type_select":
				$ConductType = $this->infoArr["postData"]["val"];
				if($ConductType == "1") {
					$UserID = $_SESSION["UserID"];
					$CategoryList = $discipline_api->RETRIEVE_CONDUCT_CATEGORY(1);
				}
				else if($ConductType == "-1") {
					$UserID = $_SESSION["UserID"];
					$CategoryList = $discipline_api->RETRIEVE_CONDUCT_CATEGORY(-1, 1);
				}
				
				$data = array();
				foreach((array)$CategoryList as $thisCategory) {
					$data[] = array(
										"label" => $thisCategory["CategoryID"],
										"name"  => $thisCategory["Name"]
									);
				}
			
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$data as $thisIndex => $thisData) {
						$thisValue = $thisData["name"];
						$thisValue = convert2unicode($thisValue, 1, 1);
						$data[$thisIndex]["name"] = $thisValue;
					}
				}
				$param["resultData"] = $data;
			break;
			
			# Conduct Item Data
			case "gm_category_select":
				$ConductCategory = $this->infoArr["postData"]["val"];
				if($ConductCategory > 0) {
					$UserID = $_SESSION["UserID"];
					$CategoryItemList = $discipline_api->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT();
					$CategoryItemList = BuildMultiKeyAssoc((array)$CategoryItemList, array("0", "1"), array("1", "2"));
					$CategoryItemList = $CategoryItemList[$ConductCategory];
				}
				
				$data = array();
				foreach((array)$CategoryItemList as $thisItem) {
					$data[] = array(
										"label" => $thisItem[1],
										"name"  => $thisItem[2]
									);
				}
				
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$data as $thisIndex => $thisData) {
						$thisValue = $thisData["name"];
						$thisValue = convert2unicode($thisValue, 1, 1);
						$data[$thisIndex]["name"] = $thisValue;
					}
				}
				$param["resultData"] = $data;
			break;
			
			# Notice Template Data
			case "notice_category":
				$NoticeCategory = $this->infoArr["postData"]["val"];
				if(!empty($NoticeCategory)) {
					$NoticeTemplates = $discipline_api->getDetentionENoticeByCategoryID("DISCIPLINE", $NoticeCategory);
				}
				
				$data = array();
				foreach((array)$NoticeTemplates as $thisTemplate) {
					$data[] = array(
										"label" => $thisTemplate[0],
										"name"  => $thisTemplate[1]
									);
				}
				
				# Convert to Unicode (EJ)
				if($this->isEJ()) {
					foreach ((array)$data as $thisIndex => $thisData) {
						$thisValue = $thisData["name"];
						$thisValue = convert2unicode($thisValue, 1, 1);
						$data[$thisIndex]["name"] = $thisValue;
					}
				}
				$param["resultData"] = $data;
			break;
			
			# Update Conduct - Assign Notice
			case "edit_complete":
				# Empty Form Data Checking
				$thisFormData = $this->infoArr["postData"]["formData"];
				if(empty($thisFormData)) {
					$jsErrorMsg = "noFormData";
				}
				# Perform Form Data Checking > New Records
				else {
					$jsErrorMsg = $discipline_api->assignRecordNoticeDataChecking($thisFormData);
				}
				
				# Data Checking : Success
				if(empty($jsErrorMsg)) {
					$discipline_api->assignRecordNoticeByAPI($thisFormData);
					$success = true;
				}
				# Data Checking : Fail
				else {
					$jsError = $jsErrorMsg;
				}
				$param["status"] = $success? "success" : "fail";
				$param["error"] = $jsError;
			break;
			
			# Update Conduct - Info + Record Status
			case "edit_record_update":
				# Empty Form Data Checking
				$thisFormData = $this->infoArr["postData"]["formData"];
				if(empty($thisFormData)) {
					$jsErrorMsg = "noFormData";
				}
				# Perform Form Data Checking
				else {
					# Check if User have Approval Right only
					$thisConductRecordID = $thisFormData["ConductID"][0];
					$HasEditRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || (($discipline_api->isConductPIC($thisConductRecordID) || $discipline_api->isConductOwn($thisConductRecordID)) && $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn"));
					$HasApprovalRight = $discipline_api->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval");
					$EditStatusOnly = !$HasEditRight && $HasApprovalRight;
					
					# Allow edit Record Status only > No need to update Record Info
					if($EditStatusOnly) {
						// do nothing 
					}
					# Update Conduct Record
					else {
						$jsErrorMsg = $discipline_api->updateConductRecordByAPI($thisFormData);
					}
				}
				
				# Data Checking : Success > Change Conduct Record Status
				if(empty($jsErrorMsg)) {
					$discipline_api->updateConductRecordStatusByAPI($thisFormData);
					$success = true;
				}
				# Data Checking : Fail
				else {
					$jsError = $jsErrorMsg;
				}
				$param["status"] = $success? "success" : "fail";
				$param["error"] = $jsError;
			break;
			
			# Delete Conduct
			case "delete":
				$thisRecordID = $this->infoArr["postData"]["target_id"];
				if($thisRecordID > 0) {
					$success = $discipline_api->deleteConductRecordByAPI($thisRecordID);
				}
				$param["status"] = $success? "success" : "fail";
			break;
		}
		
		# Output JSON return
		sleep(1);
		$this->outputJSON($param);
	}
	
	private $disciplineAPI;
	private $UserAccessRightList;
	private function getDisciplineApiConnection()
	{
		global $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		if(isset($this->disciplineAPI)) {
			return $this->disciplineAPI;
		}
		else {
			include_once $intranet_root."/includes/libdisciplinev12.php";
			include_once $intranet_root."/includes/libdisciplinev12_app_api.php";
			$PATH_WRT_ROOT = $intranet_root."/";
			
			intranet_opendb();
			
			$disciplineAPI = new libdisciplinev12_app_api();
			$disciplineAPI->controllerModule = "goodconduct_misconduct";
			$disciplineAPI->setDisciplineImagePath($this->infoArr["thisImagePath"]);
			
			// Get User Access Right
			$disciplineAPI->UserAccessRightList = $disciplineAPI->retrieveUserAccessRight($_SESSION['UserID']);
			session_register_intranet("SESSION_ACCESS_RIGHT", $disciplineAPI->UserAccessRightList);
			
			return $this->disciplineAPI = $disciplineAPI;
		}
	}
	
	private function closeDisciplineApiConnection()
	{
		intranet_closedb();
	}
}