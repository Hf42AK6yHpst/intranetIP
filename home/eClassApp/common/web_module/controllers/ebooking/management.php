<?php
/*
 * 	Using:     
 *
 *  2019-06-27 Cameron
 *      - check if pass in date is SkipSchoolDate in $sys_custom['eBooking']['EverydayTimetable'] in case new_step3a
 *      - add id=timeMode for timeMode selection list 
 *  2019-06-18 Cameron
 *      - assign TimeSlotID to BookingDetails for these method: new_step3aa, new_step3cc 
 *  2019-03-15 Cameron
 *      - apply convert2unicode to TextChi when retrieving cycle day
 *  2019-01-24 Cameron
 *      - set $emailResult for RequestSendingEmail to debug
 *  2018-12-11 Cameron
 *      - enable booking by item in getEbookingSelectorObject() > resourcesType case [P154856]
 *  2018-12-07 Isaac
 *      - Changed _remap(mydata) applied nl2br() to $_bookingRecordData['remarks']
 * 	2018-10-11  Isaac 
 * 		- added $sys_custom['eBooking']['displayRemarksDirectly'] and changed the booking remark field to textarea.
 */
class Management extends Controller {
	private function isEJ(){
		global $junior_mck;
		
		return isset($junior_mck);
	}
	public function __construct($info = array()) {
		parent::__construct($info);
	}
	
	public function __destruct(){
		if(isset($this->ebookingApi)){
			$this->closeEbookingApiConnection();
		}
	}
	
	public function _remap($method) {
	    global $Lang, $sys_custom;
		$ebooking_api = $this->getEbookingApiConnection();
		
		$method = $this->str_replace_first("cus_", "", $method);
		$methodData = array(
			"controllerLink" => "ebooking/management",
			"pageLimit" => isset($_POST["cus"]["limit"]) ? $_POST["cus"]["limit"] : 10,
			"currPage" => isset($_POST["cus"]["page"]) ? $_POST["cus"]["page"] : 1
		);
		$this->infoArr["postData"]["limit"] = $methodData["pageLimit"];
		$this->infoArr["postData"]["page"] = $methodData["currPage"];
		
		//step2 remapping handling
		$step2Cong['A'][1] = 'a';
		$step2Cong['A'][2] = 'c';
		$step2Cong['B'][1] = 'b';
		$step2Cong['B'][2] = 'd';
		if($method == 'new_step2'){
			$bookingMode = 	$this->infoArr["postData"]["formData"]["bookingMode"];
			$timeMode = $this->infoArr["postData"]["formData"]['timeMode'];
			if($bookingMode[0] && $bookingMode[1]){
				$this->infoArr["controllerInfo"][2] = $this->infoArr["controllerInfo"][2].$step2Cong[$bookingMode[0]][$bookingMode[1]];
				$method = $method.$step2Cong[$bookingMode[0]][$bookingMode[1]];
			}else{
				$this->infoArr["controllerInfo"][2] = $this->infoArr["controllerInfo"][2].'a';
				$method = $method.'a';
			}
			if($timeMode == 'timeslot'){
				$this->infoArr["controllerInfo"][2] = $this->infoArr["controllerInfo"][2].$step2Cong[$bookingMode[0]][$bookingMode[1]].'1';
				$method = $method.$step2Cong[$bookingMode[0]][$bookingMode[1]].'1';
			}
			unset($bookingMode);
			unset($timeMode);
		}
		
		$jsStatus = 'success'; //success => print $method html, fail return to orignal page (default success)
		$jsError = '';
		//send email handling
		if($method == 'requestSendingEmail'){
			$emailBookingIDArr = array_values((array)$this->infoArr['postData']['emailBookingIDArr']);
			$emailApprovedIDArr = array_values((array)$this->infoArr['postData']['emailApprovedIDArr']);

			$emailResult = $ebooking_api->RequestSendingEmail($emailBookingIDArr,$emailApprovedIDArr);
			return true;
		}
		
		$SelectorConfigArr = array();
		switch ($method){
			case 'new_step1':
				$SelectorConfigArr = array('bookingMode1','resourcesType','bookingMode2','timeMode');
				break;
			case 'new_step2aa1':
				$SelectorConfigArr = array('date','');
				break;
			case 'new_step2aa2':
				$SelectorConfigArr = array('timeHeader','','timeslot','');
				break;
			case 'new_step2a':
				$SelectorConfigArr = array('date','','startTime','endTime');
				break;
			case 'new_step2bb1':
			case 'new_step2b':
				$SelectorConfigArr = array('date','object');
				break;
			case 'new_step2cc1':
				$SelectorConfigArr = array('startDate','endDate','dayMode','weekCycleDays');
				break;
			case 'new_step2cc2':
				$SelectorConfigArr = array('timeHeader','','timeslot','');
				break;
			case 'new_step2c':
				$SelectorConfigArr = array('startDate','endDate','dayMode','weekCycleDays','startTime','endTime');
				break;
			case 'new_step2dd1':
				$SelectorConfigArr = array('object','','startDate','endDate','dayMode','weekCycleDays');
				break;
			case 'new_step2dd2':
				$SelectorConfigArr = array('timeslot','');
				break;
			case 'new_step2d':
				$SelectorConfigArr = array('object','','startDate','endDate','dayMode','weekCycleDays','startTime','endTime');
				break;
			case 'new_step3aa':
			case 'new_step3a':
				$SelectorConfigArr = array('timeHeader','','object','remarks');
				break;
			case 'new_step3bb':
				$SelectorConfigArr = array('timeHeader','','timeslot','');
				break;
			case 'new_step3b':
				$SelectorConfigArr = array('timeHeader','','startTime','endTime','remarks','');
				break;
			case 'new_step3cc':
			case 'new_step3c':
				$SelectorConfigArr = array('timeHeader','','object','remarks');
				break;
			case 'new_step3dd':
			case 'new_step3d':
				$SelectorConfigArr = array('timeHeader','','availableTime','','unAvailableTime','','remarks','');
				break;
			case 'edit':
				$SelectorConfigArr = array('timeHeader','','remarks','');
				break;
				
		}
		if(in_array('weekCycleDays',(array)$SelectorConfigArr) || in_array('timeHeader',(array)$SelectorConfigArr) || in_array('dayMode',(array)$SelectorConfigArr)){
			$sql = 'SELECT
					distinct CycleDay, TextChi, TextEng
				FROM
					INTRANET_CYCLE_DAYS
				WHERE
					PeriodID = (Select PeriodID From INTRANET_CYCLE_DAYS where RecordDate = CURDATE() )';
			$temp_arr = $ebooking_api->returnResultSet($sql);
			if ($this->isEJ()) {
			    foreach($temp_arr as $_index=>$_temp_arr) {
			        $_textChi = convert2unicode($_temp_arr['TextChi'], true);
			        $temp_arr[$_index]['TextChi'] = $_textChi;
			    }
			}
			$cycleDayArr = BuildMultiKeyAssoc($temp_arr, 'CycleDay');
			$this->infoArr['postData']['formData']['cycleDayArr'] = $cycleDayArr;
			unset($temp_arr);
			unset($cycleDayArr);
		}
		//data handling
// 		$formData = $this->infoArr['postData']['formData'];
// 		$stepData = $this->infoArr['stepData'][$method];
		$formData = $this->infoArr['stepData'][$method];

		if($formData['date']){
			$Date = str_replace('/','-',$formData['date']);
		}
		if($formData['startTime']){
			$StartTime = $formData['startTime'].":00";
		}
		if($formData['endTime']){
			$EndTime = $formData['endTime'].":00";
		}
		if($formData['startDate']){
			$StartDate = str_replace('/','-',$formData['startDate']);
		}
		if($formData['endDate']){
			$EndDate = str_replace('/','-',$formData['endDate']);
		}
	
		switch ($method) {
			case 'list':
				$eBookingSetting = $ebooking_api->ReturnEbookingSetting();
				$this->infoArr['postData']['formData']['eBookingSetting'] = $eBookingSetting;
				break;
			case 'edit':
				$bookingID = $this->infoArr["getData"]['bookingid'];
				$bookingRecord = $ebooking_api->ReturnUserBookingRecord($bookingID);
// 				Booking Details
				$BookingDetails[0]['Date'] = $bookingRecord[0]['date'];
				$BookingDetails[0]['StartTime'] = $bookingRecord[0]['startTime'];
				$BookingDetails[0]['EndTime'] = $bookingRecord[0]['endTime'];
				$BookingDetails[0]['RoomID'] = $bookingRecord[0]['locationID'];
				foreach ((array)$bookingRecord as $_bookingRecord){
					$BookingDetails[0]['ItemID'][] = $_bookingRecord['itemID'];
				}
				$this->infoArr["api"]['data']['BookingDetails'] = $BookingDetails;
				
				$this->infoArr['postData']['formData'] = $bookingRecord[0];
				$this->infoArr['postData']['formData']['roomID'] = $bookingRecord[0]['locationID'];
				$this->infoArr['postData']['formData']['bookingType'] = $bookingRecord[0]['locationID']? 'room':'item';
				
				
				break;
			case 'new_step2aa2':
			case 'new_step2cc2':
			case 'new_step2dd2':
				if($Date){
					$dateArr[] = $Date;
				}elseif($StartDate && $EndDate){
					$temp = $this->getBookingDetailByStartDateEndDate($StartDate,$EndDate);
					foreach ((array)$temp as $_temp){
						$dateArr[] = $_temp['Date'];
					}
				}
				
				$timeslotData = $ebooking_api->ReturnTimeslotData($dateArr);        // case new_step2aa2: It's array([TimetableID]=>[TimeSlotID]=>array([TimetableID] =>288,[TimeSlotID] => 1719,[TimeSlotName] => 1st, [StartTime] => 08:05:00,[EndTime] => 08:40:00)), no date info!

				$index = 0;
				foreach ((array)$timeslotData as $date => $timeslot){
				    $BookingDetails[$index]['Date'] = $date;       // 2019-06-17: case new_step2aa2: not date, it's TimeSlotID according to the above structure, why assign to BookingDetals?
					$index++;
				}
				
				$this->infoArr["api"]['data']['BookingDetails'] = $BookingDetails;
				$this->infoArr['postData']['formData']['timeslotData'] = $timeslotData;
				
				$this->infoArr['postData']['formData']['dateArr'] = $dateArr;

				//js popup checking handling
				if($timeslotData != false && count($timeslotData)==1){
					$jsStatus= 'success';
				}else{
					$jsStatus= 'fail';
					$jsError = 'noAvailPeriod';
				}
				break;
			case 'new_step2bb1':
			case 'new_step2dd1':
			case 'new_step2b':
			case 'new_step2d':
				//Get Available Object (Room/ Item)
				$availObject = $this->getAvailObject($BookingDetails,$formData['bookingType']);
				break;
			case 'new_step3aa':
			case 'new_step3cc':
			    
				$timeslotAssoArr = $ebooking_api->ReturnTimeslotDetail($formData['timeslot']);
				
				$this->infoArr['postData']['formData']['timeslotDetail'] = $timeslotAssoArr; //to timeheader
				
				$count = 0;
				foreach ((array)$formData['date'] as $_date){
					foreach ((array)$timeslotAssoArr as $_timeslotAssoArr){
						$BookingDetails[$count]['Date'] = $_date;
						$BookingDetails[$count]['StartTime'] = $_timeslotAssoArr['StartTime'];
						$BookingDetails[$count]['EndTime'] = $_timeslotAssoArr['EndTime'];
						$BookingDetails[$count]['BookingType'] = $formData['bookingType'];
						$BookingDetails[$count]['TimeSlotID'] = $_timeslotAssoArr['TimeSlotID'];
						$count++;
					}
				}
								
				$availObject = $this->getAvailObject($BookingDetails,$formData['bookingType']);

				$this->infoArr["api"]['data']['BookingDetails'] = $BookingDetails;
				if( !empty($availObject) ){
					$jsStatus= 'success';
				}else{
					$jsStatus= 'fail';
					if($formData['bookingType']=='item'){
						$jsError = 'noAvailItem';
					}else{
						$jsError = 'noAvailRoom';
					}
				}
				break;
			case 'new_step3a':
			    if ($sys_custom['eBooking']['EverydayTimetable']) {
			        $academicYearID = Get_Current_Academic_Year_ID();
			        $skipDateArr = $ebooking_api->getSkipSchoolDates($academicYearID, $Date, $Date, $formData['bookingType'], $arrFacility=array());
			        if (count($skipDateArr)) {
			            $date = $skipDateArr[0];   
			        }
			        else {
			            $date = '';
			        }
			    }
			    else {
			        $date = $Date;
			    }
			    
			    if ($date != '') {
    				$BookingDetails[0]['Date'] = $date;
    				$BookingDetails[0]['StartTime'] = $StartTime;
    				$BookingDetails[0]['EndTime'] = $EndTime;
    				$BookingDetails[0]['BookingType'] = $formData['bookingType'];
    				
    				$availObject = $this->getAvailObject($BookingDetails,$formData['bookingType']);
    				$this->infoArr["api"]['data']['BookingDetails'] = $BookingDetails;
    				//js popup checking handling 
    				if(!empty($availObject)){
    					$jsStatus= 'success';
    				}else{
    					$jsStatus= 'fail';
    					if($formData['bookingType']=='item'){
    						$jsError = 'noAvailItem';
    					}else{
    						$jsError = 'noAvailRoom';
    					}
    				}
			    }
			    else {
			        $jsStatus= 'fail';
		            $jsError = 'byTimeSlotOnly';
			    }
				break;
			case 'new_step3bb':
				if($formData['bookingType'] == 'item'){
					$ObjectIDArr = $formData['itemID'];
				}else{
					$ObjectIDArr[0] = $formData['roomID'];
				}
				$dateArr[0] = $Date;
				$timeslotData = $ebooking_api->ReturnTimeslotData($dateArr);
				foreach ((array)$timeslotData as $timetableID => $_timeslotData){
					$timetableID = $timetableID;
					foreach ((array)$_timeslotData as $timeslotID => $__timeslotData){
						$BookingDetails[$timeslotID]['Date'] = $Date;
						$BookingDetails[$timeslotID]['StartTime'] = $__timeslotData['StartTime'];
						$BookingDetails[$timeslotID]['EndTime'] = $__timeslotData['EndTime'];
						$BookingDetails[$timeslotID]['BookingType'] = $formData['bookingType'];
						if($formData['bookingType'] == 'item'){
							$BookingDetails[$timeslotID]['ItemID']= $formData['itemID'];
						}else{
							$BookingDetails[$timeslotID]['RoomID']= $formData['roomID'];
						}
					}
				}
				//Get Available Timeslot
				foreach ((array)$BookingDetails as $timeslotID=>$_BookingDetails){
					$temp[0] = $_BookingDetails;
					$availObject = $this->getAvailObject($temp,$formData['bookingType']);
					//get availObject ID arr
					unset($AvailObjectIDArr);
					if($formData['bookingType'] == 'item'){
						//item
						foreach ((array)$availObject as $_itemID => $_item){
							$AvailObjectIDArr[] = $_itemID;
						}
					}else{
						//room
						foreach((array)$availObject as $_roomID => $_roomID){
							$AvailObjectIDArr[] = $_roomID;
						}
					}
					foreach($ObjectIDArr as $objectID){
						if(in_array($objectID,(array)$AvailObjectIDArr)){
							//do nothing
						}else{
							unset($timeslotData[$timetableID][$timeslotID]);
							unset($BookingDetails[$timeslotID]);
						}
					}
				}
			
				$this->infoArr["api"]['data']['BookingDetails']= $BookingDetails;
				$this->infoArr['postData']['formData']['timeslotData'] = $timeslotData;
				if(!empty($BookingDetails)){
					$jsStatus= 'success';
				}else{
					$jsStatus= 'fail';
					$jsError = 'noAvailPeriod';
				}
				break;
			case 'new_step3b':
				if($this->infoArr['postData']['formData']['bookingType']=='item'){
					//item
					//get Available Time
					$availableTimeSlotArr = $ebooking_api->Get_Facility_Available_Booking_Period_Of_User('2', (array)$formData['itemID'], $_SESSION['UserID'], '', $formData['date'], $formData['date'],'');
					$ItemAvailablePeriodAssocAry = $ebooking_api->Union_Time_Range_Of_Date_Assoc($availableTimeSlotArr);
					$this->infoArr['postData']['formData']['AvailableTime'] = $ItemAvailablePeriodAssocAry;
				}else{
					//room
					//get Available Time
					$availableTimeSlotArr = $ebooking_api->Get_Facility_Available_Booking_Period_Of_User('1', $formData['roomID'], $_SESSION['UserID'], '', $formData['date'], $formData['date'],'');
					$RoomAvailablePeriodAssocAry = $ebooking_api->Union_Time_Range_Of_Date_Assoc($availableTimeSlotArr);
					$this->infoArr['postData']['formData']['AvailableTime'] = $RoomAvailablePeriodAssocAry;
				}
				//api data
				if($formData['bookingType']=='item'){
					$BookingDetails[0]['ItemID'] = $formData['itemID'];
				}else{
					$BookingDetails[0]['RoomID'] = $formData['roomID'];
				}
				$BookingDetails[0]['Date'] =  str_replace('/','-',$this->infoArr['postData']['formData']['date']);
				$BookingDetails[0]['BookingType'] = $formData['bookingType'];
				
				$this->infoArr["api"]['data']['BookingDetails'] = $BookingDetails;
				
				//js popup checking handleing
				if(!empty($RoomAvailablePeriodAssocAry)||!empty($ItemAvailablePeriodAssocAry)){
					$jsStatus= 'success';
				}else{
					$jsStatus= 'fail';
					$jsError = 'noAvailPeriod';
				}
				break;
			case 'new_step3c':
				//get all the selected date in the date range and patch as booking detail
				$BookingDetails = $this->getBookingDetailByStartDateEndDate($StartDate,$EndDate);

                if ($sys_custom['eBooking']['EverydayTimetable']) {
                    $newBookingDetails = array();
                    $academicYearID = Get_Current_Academic_Year_ID();
                    $skipDateArr = $ebooking_api->getSkipSchoolDates($academicYearID, $StartDate, $EndDate, $formData['bookingType'], $arrFacility=array());
                    if (count($skipDateArr)) {
                        foreach($BookingDetails as $_index=>$_bookingDetails) {
                            if (in_array($_bookingDetails['Date'],$skipDateArr)) {
                                $newBookingDetails[] = $_bookingDetails; 
                            }
                        }
                    }
                    if (count($newBookingDetails)) {
                        $availObject = $this->getAvailObject($newBookingDetails,$formData['bookingType']);
                        $this->infoArr["api"]['data']['BookingDetails'] = $newBookingDetails;
                    }
                    else {
                        $availObject = '';
                        $this->infoArr["api"]['data']['BookingDetails'] = '';
                        $jsError = 'byTimeSlotOnly2';
                    }
                }
                else {
    				$availObject = $this->getAvailObject($BookingDetails,$formData['bookingType']);
				    $this->infoArr["api"]['data']['BookingDetails'] = $BookingDetails;
                }
                
				if(!empty($availObject)){
					$jsStatus= 'success';
				}else{
					$jsStatus= 'fail';
					if ($jsError != 'byTimeSlotOnly2') {
    					if($formData['bookingType']=='item'){
    						$jsError = 'noAvailItem';
    					}else{
    						$jsError = 'noAvailRoom';
    					}
					}
				}
				break;
			case 'new_step3d':
				if($formData['bookingType']=='item'){
					$itemID = $formData['itemID'];
				}else{
					//get selected location detail
					$locationID = $formData['roomID'];
				}
				//get all the selected date in the date range and patch as booking detail
				
				$BookingDetails = $this->getBookingDetailByStartDateEndDate($StartDate,$EndDate);
				//Eliminate the location once one of the day is not available
			
				foreach ((array)$BookingDetails as $key=>$_BookingDetails){
					$temp = $ebooking_api->GetAvailableRoom($_BookingDetails);
					if($formData['bookingType']=='item'){
						//itemHandling
						$check = true;
						foreach ((array)$itemID as $_itemID){
							if(!$temp[$_BookingDetails['Date']] [$_itemID]['BookAvailable']){
								$check = false;
							}
						}
						if($check){
							$availableBookingDetails[$key] = $_BookingDetails;
						}else{
							$_BookingDetails['error'] = $temp[$_BookingDetails['Date']] [$_itemID]['ErrorCode'][0];
							$unAvailableBookingDetails[$key] = $_BookingDetails;
							unset($BookingDetails[$key]);
						}
					}else{
						//roomHandling
						if($temp[$_BookingDetails['Date']] [$locationID]['BookAvailable']){
							$availableBookingDetails[$key] = $_BookingDetails;
						}else{
							$_BookingDetails['error'] = $temp[$_BookingDetails['Date']] [$locationID]['ErrorCode'][0];
							$unAvailableBookingDetails[$key] = $_BookingDetails;
							unset($BookingDetails[$key]);
						}
					}
				}
				$this->infoArr['postData']['formData']['unAvailableBookingDetail'] = $unAvailableBookingDetails;
				$this->infoArr['postData']['formData']['AvailableBookingDetail'] = $availableBookingDetails;
				$this->infoArr["api"]['data']['BookingDetails'] = $BookingDetails;
				
				if(!empty($BookingDetails)){
					$jsStatus= 'success';
				}else{
					$jsStatus= 'fail';
					$jsError = 'noAvailPeriod';
				}
				break;
				
			case 'new_step3dd':
				$timeslotAssoArr = $ebooking_api->ReturnTimeslotDetail($formData['timeslot']);
				$this->infoArr['postData']['formData']['timeslotDetail'] = $timeslotAssoArr; //to timeheader
				
				$count = 0;
				foreach ((array)$formData['date'] as $_date){
					foreach ((array)$timeslotAssoArr as $_timeslotAssoArr){
						$BookingDetails[$count]['Date'] = $_date;
						$BookingDetails[$count]['StartTime'] = $_timeslotAssoArr['StartTime'];
						$BookingDetails[$count]['EndTime'] = $_timeslotAssoArr['EndTime'];
						$BookingDetails[$count]['BookingType'] = $formData['bookingType'];
						if($formData['bookingType']=='item'){
							$BookingDetails[$count]['ItemID']= $formData['itemID'];
						}else{
							//get selected location detail
							$BookingDetails[$count]['RoomID']= $formData['roomID'];
						}
						$count++;
					}
				}
				
				if($formData['bookingType']=='item'){
					$itemID = $formData['itemID'];
				}else{
					//get selected location detail
					$locationID = $formData['roomID'];
				}
				
				foreach ((array)$BookingDetails as $key=>$_BookingDetails){
					$temp = $ebooking_api->GetAvailableRoom($_BookingDetails);
					if($formData['bookingType']=='item'){
						//itemHandling
						$check = true;
						foreach ((array)$itemID as $_itemID){
							if(!$temp[$_BookingDetails['Date']] [$_itemID]['BookAvailable']){
								$check = false;
							}
						}
						if($check){
							$availableBookingDetails[$key] = $_BookingDetails;
						}else{
							$_BookingDetails['error'] = $temp[$_BookingDetails['Date']] [$_itemID]['ErrorCode'][0];
							$unAvailableBookingDetails[$key] = $_BookingDetails;
							unset($BookingDetails[$key]);
						}
					}else{
						//roomHandling
						if($temp[$_BookingDetails['Date']] [$locationID]['BookAvailable']){
							$availableBookingDetails[$key] = $_BookingDetails;
						}else{
							$_BookingDetails['error'] = $temp[$_BookingDetails['Date']] [$locationID]['ErrorCode'][0];
							$unAvailableBookingDetails[$key] = $_BookingDetails;
							unset($BookingDetails[$key]);
						}
					}
				}
				$this->infoArr['postData']['formData']['unAvailableBookingDetail'] = $unAvailableBookingDetails;
				$this->infoArr['postData']['formData']['AvailableBookingDetail'] = $availableBookingDetails;
				$this->infoArr["api"]['data']['BookingDetails'] = $BookingDetails;
				
				if(!empty($BookingDetails)){
					$jsStatus= 'success';
				}else{
					$jsStatus= 'fail';
					$jsError = 'noAvailPeriod';
				}
				break;
			
			case 'new_step4':
				$jsonobj = $this->getJsonObj();
				$BookingDetails = $jsonobj->decode($formData['BookingDetails']);
				
				//pack the data as eBooking Api Request
				switch($formData['SubmitFrom']){
					default:
						//do nth
						break;
					case 'edit':
						if($formData['BookingDetails'] && ($formData['roomID']||$formData['itemID'])){
							$BookingDetails = $jsonobj->decode($formData['BookingDetails']);
							foreach ((array)$BookingDetails as $indexOfBooking => $_BookingDetails){
								$apiRequest['BookingDetails'][$indexOfBooking] = $_BookingDetails;
								$apiRequest['BookingDetails'][$indexOfBooking]['RoomID'] = $formData['roomID'];
							}
							$apiRequest['Remarks']= $formData['remarks'];
							$apiRequest['BookingFrom'] = 'eBookingApp';
							$apiRequest['UserID'] = $_SESSION['UserID'];
						}
						$remarks = $formData['remarks'];
						$bookingID = $formData['bookingID'];
						$success = $ebooking_api ->RequestUpdateBookingDetails($bookingID, $remarks, '');
						if($success[1]){
							$Respond[0]['BookingID'] = $bookingID;
							$Respond[0]['Status'] = '1';
							$Respond[0]['Error'] = '';
							
						}else{
							$Respond[0]['BookingID'] = '';
							$Respond[0]['Status'] = '0';
							$Respond[0]['Error'] ='';
						}
						$this->infoArr["api"]['Request'] = $apiRequest;
						$this->infoArr["api"]['respond'] = $Respond;
						break;
					case 'step3a':
					case 'step3aa':
					case 'step3b':
					case 'step3bb':
					case 'step3c':
					case 'step3cc':
					case 'step3d':
					case 'step3dd':
						if($formData['BookingDetails']){
							$BookingDetails = $jsonobj->decode($formData['BookingDetails']);
							foreach ((array)$BookingDetails as $indexOfBooking => $_BookingDetails){
								$apiRequest['BookingDetails'][$indexOfBooking] = $_BookingDetails;
								if( $formData['roomID']){
									//room
									$apiRequest['BookingDetails'][$indexOfBooking]['RoomID'] = $formData['roomID'];
								}elseif($formData['itemID']){
									//item
									$apiRequest['BookingDetails'][$indexOfBooking]['ItemID'] = $formData['itemID'];
								}
								if($formData['startTime']){
									$apiRequest['BookingDetails'][$indexOfBooking]['StartTime'] = $StartTime;
								}
								if($formData['endTime']){
									$apiRequest['BookingDetails'][$indexOfBooking]['EndTime'] = $EndTime;
								}
							}
							$apiRequest['Remarks']= $formData['remarks'];
							$apiRequest['BookingFrom'] = 'eBookingApp';
							$apiRequest['UserID'] = $_SESSION['UserID'];
						}
						$this->infoArr["api"]['Request'] = $apiRequest;
						break;
				}
				//booking Request
				if($apiRequest && $formData['SubmitFrom'] != 'edit'){
					$Respond = $ebooking_api->RequestBooking($apiRequest);
					$this->infoArr["api"]['respond'] = $Respond;
				}
				//booking object details
				$objectDeatil = '';
				if($formData['roomID']){
					$allLocation = $ebooking_api->ReturnAllLocation();
					$objectDeatil[] = $allLocation[$formData['roomID']];
					unset($allLocation);
				}else{
					$allItem = $ebooking_api->ReturnAllItem();
					foreach ((array)$formData['itemID'] as $_itemID){
						$objectDeatil[] = $allItem[$_itemID];
					}
					unset($allItem);
				}
				$this->infoArr["api"]['objectDetail'] = $objectDeatil;
			break;
		}
		unset($formData);
		//generate form selector
		$this->getEbookingSelectorObject($SelectorConfigArr, $method);
		$responseHTML = $this->view($methodData);
		
		//selector handle end		
		$param = array(
			"hash" => $this->infoArr["strHash"],
			"pageType" => $method,
			"respType" => "HTML",
			"customJS" => "",
			"pData" => $this->infoArr["postData"],
			"content" => $responseHTML
		);
		
		switch ($method) {
			case "mydata":
				//function get data for /view/list
				$bookingRecordData = $ebooking_api->ReturnUserBookingRecord('');
				$dataarr = '';
				foreach ((array)$bookingRecordData as $_bookingRecordData){
				    $_bookingRecordData['remarks'] = nl2br($_bookingRecordData['remarks']);
					$dataarr[] = $_bookingRecordData;
				}
				$jsonobj = $this->getJsonObj();
				$ajaxData = $jsonobj->decode(stripslashes($this->infoArr["postData"]['ajaxData']));
				//date filtering
				if(	!empty($ajaxData)){
					foreach ((array)$ajaxData as $key =>$_val){
						$ajaxDataArr[$_val['name']] = $_val['value'];
					}
					foreach ((array)$dataarr as $key => $_val){
						if($ajaxDataArr['startDate'] && $ajaxDataArr['endDate']){
							if(!( strtotime($_val['date'])>= strtotime($ajaxDataArr['startDate']) && strtotime($_val['date'])<=strtotime($ajaxDataArr['endDate']) ) ){
								if($dataarr[$key]){
									unset($dataarr[$key]);
								}
							}
						}
					}
				}
				if($dataarr){
					$dataarr = array_values($dataarr);
				}
				$param = array( 'data' => $dataarr);
				break;
				//action Btn handling skip run the general logic below
			case 'actionBtn':
				switch($this->infoArr['postData']['action']){
					case 'delete':
						$jsStatus = $ebooking_api->RequestDeleteBooking($this->infoArr['postData']['bookingid']);
						if($jsStatus){
							$jsError = 'deleteSuccess';
						}else{
							$jsError = 'deleteFail';
						}
						break;
				}
				
				$param = array(
						"hash" => $this->infoArr["strHash"],
						"status" => $jsStatus,// success =>print the method page, fail=>showing error popup
						"error" => $jsError//decide the error lang
				);
				break;				
			default:
				$param = array(
						"hash" => $this->infoArr["strHash"],
						"pageType" => $method,
						"respType" => "HTML",
						"content" => $responseHTML,
						"status" => $jsStatus,// success =>print the method page, fail=>showing error popup
						"error" => $jsError//decide the error lang
				);
				break;
		}
		sleep(1);
		$this->outputJSON($param);
	}
	
	private $ebookingApi;
	private function getEbookingApiConnection(){
		global $intranet_root, $intranet_session_language, $PATH_WRT_ROOT;
		if(isset($this->ebookingApi)){
			return $this->ebookingApi;
		}else{
			include_once $intranet_root.'/includes/libebooking.php';
			include_once $intranet_root.'/includes/libebooking_api.php';
			$PATH_WRT_ROOT = $intranet_root.'/';
			intranet_opendb();
			$ebookingApi = new libebooking_api();
			return $this->ebookingApi = $ebookingApi;
		}
	}
	private function getBookingDetailByStartDateEndDate($StartDate, $EndDate){
		$ebooking_api = $this->getEbookingApiConnection();
		$formData = $this->infoArr['postData']['formData'];
		
		//overwrite stepData

		if($formData['SubmitFrom']=='new_step2cc2' || $formData['SubmitFrom']=='new_step2dd2'){
			$stepData = $this->infoArr['stepData'][$formData['SubmitFrom']];
			foreach((array)$stepData as $name => $value){
				$formData[$name] = $value;
			}
		}
		
		$count = 0;
		$date = $StartDate;
		
		while (strtotime($EndDate)>=strtotime($date)) {
			if($this->infoArr['postData']['formData']['itemID']){
				$BookingDetails[$count]['ItemID'] =$formData['itemID'];
			}
			if($formData['roomID']){
				$BookingDetails[$count]['RoomID'] = $formData['roomID'];
			}
			if($formData['daymode']=='weekday'){
				if(in_array(date('D',strtotime($date)), (array)$formData['day'])){
					$BookingDetails[$count]['Date']=$date;
					$BookingDetails[$count]['StartTime']=$formData['startTime'].":00";
					$BookingDetails[$count]['EndTime']=$formData['endTime'].":00";
					$BookingDetails[$count]['BookingType'] =$formData['bookingType'];
					$count++;
				}
			}elseif($formData['daymode']=='cycleday'){
				$sql = 'Select CycleDay FROM INTRANET_CYCLE_DAYS WHERE RecordDate = "'.$date.'"';
				$cycleDay = $ebooking_api->returnResultSet($sql);
				$cycleDay = $cycleDay[0]['CycleDay'];
				if(in_array($cycleDay, (array)$formData['day'])){
					$BookingDetails[$count]['Date']=$date;
					$BookingDetails[$count]['StartTime']=$formData['startTime'].":00";
					$BookingDetails[$count]['EndTime']=$formData['endTime'].":00";
					$BookingDetails[$count]['BookingType'] =$formData['bookingType'];
					$count++;
				}
			}
			$date = strtotime("+1 day $date");
			$date = date("Y-m-d",$date);
		}
		return $BookingDetails;
	}
	
	private function getAvailObject($BookingDetails,$FacilityType){
		$ebooking_api = $this->getEbookingApiConnection();
		if($FacilityType=='item'){
			//Initalize all the item can be booked
			$AllItem = $ebooking_api->ReturnAllItem();
// 			if(!empty($BookingDetails)){
				foreach((array)$AllItem as $itemID => $_item){
					$check[$itemID] = true;
				}
// 			}
			//Eliminate the item once one of the day is not available
			foreach ((array)$BookingDetails as $_BookingDetails){
				$temp = $ebooking_api->GetAvailableRoom($_BookingDetails);
				foreach ((array)$temp[$_BookingDetails['Date']] as $itemID=> $_temp){
					if(!$_temp['BookAvailable']){
						$check[$itemID] = false;
					}
				}
			}
			foreach ((array)$AllItem as $itemID => $_item){
				if($check[$itemID]){
					$availItem[$itemID] = $_item;
				}
			}
			unset($AllItem);
			$availObject = $availItem;
			$this->infoArr["api"]['data']['availItem'] = $availObject;
		}else{
			//room
			//Initalize all the location can be booked
			$Alllocation = $ebooking_api->ReturnAllLocation();
// 			if(!empty($BookingDetails)){
				foreach((array)$Alllocation as $locationID => $_location){
					$check[$locationID] = true;
				}
// 			}
			//Eliminate the location once one of the day is not available
			foreach ((array)$BookingDetails as $_BookingDetails){
				$temp = $ebooking_api->GetAvailableRoom($_BookingDetails);
				foreach ((array)$temp[$_BookingDetails['Date']] as $locationID => $_temp){
					if(!$_temp['BookAvailable']){
						$check[$locationID] = false;
					}
				}
			}
			//get available room info
			foreach((array)$Alllocation as $locationID => $_location){
				if($check[$locationID]){
					$availRoom[$locationID] = $_location;
				}
			}
			unset($Alllocation);
			$availObject = $availRoom;
			$this->infoArr["api"]['data']['availRoom'] = $availObject;
		}
		
		return $availObject;
	}
	
	private function getEbookingSelectorObject($SelectorConfigArr){
		//function to gen the selector displayed in the UI
	    global $Lang, $sys_custom;
		$ebooking_api = $this->getEbookingApiConnection();
		$eBookingSetting = $ebooking_api->ReturnEbookingSetting();
		//selector handle start
		unset($this->infoArr['formSelector']); // reset selector
		$count = '1';
		
		foreach ((array)$SelectorConfigArr as $_SelectorName){
			$selectorData  = $this->infoArr['postData']['formData'];
			
			//overwrite selectorData from the previous step 
			$stepData = $this->infoArr['stepData'][$selectorData['SubmitFrom']];
			foreach((array)$stepData as $name => $value){
				$selectorData[$name] = $value;
			}
			//default value handling start
			//date
			
			$date =  $selectorData['date']?  $selectorData['date']: date("Y/m/d");
			//start date && end date
			$startDate =  $selectorData['startDate']?  $selectorData['startDate']: date("Y/m/d");
			$date_temp = strtotime($startDate)+7*24*60*60; //default 7days after startday
			$endDate =  $selectorData['endDate']?  $selectorData['endDate']: date("Y/m/d",$date_temp);
			//starttime
			$time = round(strtotime(date('H:i'))/300)*300;
			$startTime = $selectorData['startTime']? date('H:i',strtotime($selectorData['startTime'])) : date('H:i',$time);
			//endtime
			$time = strtotime($startTime)+30*60; //default 30mins after starttime
			$endTime = $selectorData['endTime']? date('H:i',strtotime($selectorData['endTime'])) : date('H:i', $time);
			
			//daymode
			if($selectorData['daymode']){
				$daymode = $selectorData['daymode'];
			}else{
				$daymode = ($eBookingSetting['DefaultPeriodicBookingMethod']=='1')? 'weekday':'cycleday';
			}
			$roomID = $selectorData['roomID']?$selectorData['roomID']:'';
			
			$itemID =  $selectorData['itemID']?$selectorData['itemID']:'';
			
			$timeslotArr =  $selectorData['timeslot']?$selectorData['timeslot']:'';
			
			if($sys_custom['eBooking']['displayRemarksDirectly']){
			    $cust_remark = $Lang['eBooking']['PresetRemark'];
			}
			else{
			    $cust_remark= "";
			}

			$remarks =  $selectorData['remarks']?$selectorData['remarks']:$cust_remark;
			//default value handling end
			
			switch($_SelectorName){
				default:
					$formSelector['blank_'.$count] = '';
					$count++;
					break;
				case 'resourcesType':
					// Room OR Item
// 					if($selectorData['bookingType']=='item'){
// 						$itemSelected = 'selected';
// 						$roomSelected = '';
// 					}else{
// 						$roomSelected = 'selected';
// 						$itemSelected = '';
// 					}
					$formSelector['bookingType'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['bookingType'] .= '<select name="bookingType">';
					$formSelector['bookingType'] .= '<option value="" disabled>'.$Lang['eBooking']['App']['Selector']['PleaseSelect'].'</option>';
					$formSelector['bookingType'] .= '<option value="room" selected>'.$Lang['eBooking']['App']['FieldTitle']['Room'].'</option>';
 					$formSelector['bookingType'] .= '<option value="item" '.$itemSelected.'>'.$Lang['eBooking']['App']['FieldTitle']['Item'].'</option>';
					$formSelector['bookingType'] .= '</select>';
					$formSelector['bookingType'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['ResourcesType'].'</label>';
					$formSelector['bookingType'] .= '</div>';
					break;
				case 'bookingMode1':
					// selected Date OR selected room
// 					if($selectorData['bookingMode'][0]=='A'){
// 						$select1Room = '';
// 						$select1Date = 'selected';
// 					}elseif($selectorData['bookingMode'][0]=='B'){
// 						$select1Room = 'selected';
// 						$select1Date= '';
// 					}else{
// 						$select1Room = '';
// 						$select1Date= 'selected';
// 					}
					
// 					$formSelector['bookingMode1'] = '<div class="input-field col col-xs-12 col-sm-6">';
// 					$formSelector['bookingMode1'] .= '<select name="bookingMode[]">';
// 					$formSelector['bookingMode1'] .= '<option value="" disabled>'.$Lang['eBooking']['App']['Selector']['PleaseSelect'].'</option>';
// 					$formSelector['bookingMode1'] .= '<option value="B" '.$select1Room.'>'.$Lang['eBooking']['App']['FieldTitle']['AppointRoom'].'</option>';
// 					$formSelector['bookingMode1'] .= '<option value="A" '.$select1Date.'>'.$Lang['eBooking']['App']['FieldTitle']['AppointDate'].'</option>';
// 					$formSelector['bookingMode1'] .= '</select>';
// 					$formSelector['bookingMode1'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['AppointType'].'</label>';
// 					$formSelector['bookingMode1'] .= '</div>';
					$formSelector['bookingMode1'] = '<input type="hidden" name="bookingMode[]" value="A">';
					break;
				case 'bookingMode2':
					//option (period/ once)
					if($selectorData['bookingMode'][1]=='2'){
						$select2Repeat = 'selected';
						$select2Once = '';
					}elseif($eBookingSetting['DefaultBookingType']=='2' && !$selectorData['bookingMode'][1]){
						$select2Repeat = 'selected';
						$select2Once = '';
					}else{
						$select2Repeat = '';
						$select2Once = 'selected';
					}
					
					$formSelector['bookingMode2'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['bookingMode2'] .= '<select id="modeSelect" class="specialSelect" name="bookingMode[]">';
					$formSelector['bookingMode2'] .= '<option value="" disabled>'.$Lang['eBooking']['App']['Selector']['PleaseSelect'].'</option>';
					if($eBookingSetting['bookingTypeSingleEnabled'] || !isset($eBookingSetting['bookingTypeSingleEnabled'])){
						$formSelector['bookingMode2'] .= '<option value="1" rel="a" class="cuslinkA" '.$select2Once.'>'.$Lang['eBooking']['App']['FieldTitle']['OnceOnly'].'</option>';
					}
					if($eBookingSetting['bookingTypePeriodicEnabled'] || !isset($eBookingSetting['bookingTypePeriodicEnabled'])){
						$formSelector['bookingMode2'].= '<option value="2" rel="b" class="cuslinkA" '.$select2Repeat.'>'.$Lang['eBooking']['App']['FieldTitle']['Repeated'].'</option>';
					}
					$formSelector['bookingMode2'] .= '</select>';
					$formSelector['bookingMode2'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['BookingTypeRepeat'].'</label>';
					$formSelector['bookingMode2'] .= '</div>';
					break;
				case 'timeMode':
					//specific time / timeslot
					if($eBookingSetting['DefaultBookingMethod']=='2' || !isset($eBookingSetting['DefaultBookingMethod'])){
						$selectTimeModePeriod = 'selected';
						$selectTimeModeTimeslot = '';
					}else{
						$selectTimeModePeriod = '';
						$selectTimeModeTimeslot = 'selected';
					}
					$formSelector['timeMode'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['timeMode'] .= '<select id="timeMode" name="timeMode">';
					$formSelector['timeMode'] .= '<option value="" disabled>'.$Lang['eBooking']['App']['Selector']['PleaseSelect'].'</option>';
					if($eBookingSetting['SpecificPeriodEnabled'] || !isset($eBookingSetting['SpecificPeriodEnabled'])){
						$formSelector['timeMode'] .= '<option value="timeslot" '.$selectTimeModeTimeslot.'>'.$Lang['eBooking']['App']['FieldTitle']['AppointedTimeslot'].'</option>';
					}
					if($eBookingSetting['SpecificTimeEnabled'] || !isset($eBookingSetting['SpecificTimeEnabled'])){
						$formSelector['timeMode'] .= '<option value="specificTime" '.$selectTimeModePeriod.'>'.$Lang['eBooking']['App']['FieldTitle']['AppointedPeriod'].'</option>';
					}
					$formSelector['timeMode'] .= '</select>';
					$formSelector['timeMode'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['BookingMethod'].'</label>';
					$formSelector['timeMode'] .= '</div>';
					break;
				case 'startTime':
					//startTime
					$formSelector['startTime'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['startTime'] .= '<input type="text" name="startTime" id="startTime" class="req-fromtime timepicker form-control" value="'.$startTime.'">';
					$formSelector['startTime'] .= '<label for="startTime" class="active">'.$Lang['eBooking']['App']['FieldTitle']['StartTime'].'</label>';
					$formSelector['startTime'] .= '</div>';
					break;
				case 'endTime':
					//endTime
					$formSelector['endTime'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['endTime'] .= '<input type="text" name="endTime" id="endTime" class="req-totime timepicker form-control" value="'.$endTime.'">';
					$formSelector['endTime'] .= '<label for="endTime" class="active">'.$Lang['eBooking']['App']['FieldTitle']['EndTime'].'</label>';
					$formSelector['endTime'] .= '</div>';
					break;
				case 'timeslot':
					//timeslot data getting 
					$formSelector['timeslot'] = '<div class="input-field col col-xs-12 col-sm-6">';
						$formSelector['timeslot'] .= '<select multiple name="timeslot[]" id="timeslot" class="required">';
						$formSelector['timeslot'] .= '<option value="" disabled>'.$Lang['eBooking']['App']['Selector']['PleaseSelect'].'</option>';
						foreach ((array)$selectorData['timeslotData'] as $timeTableID => $_date_timeslot){
							foreach ((array)$_date_timeslot as $__date_timeslot){
								$selected = in_array($__date_timeslot['TimeSlotID'],(array)$timeslotArr)? 'selected':'';
								
								$formSelector['timeslot'] .= '<option value="'.$__date_timeslot['TimeSlotID'].'" '.$selected.'>';
									$formSelector['timeslot'] .= $__date_timeslot['TimeSlotName'].'('. date('H:i',strtotime($__date_timeslot['StartTime'])).'-'.date('H:i',strtotime($__date_timeslot['EndTime'])).')';
								$formSelector['timeslot'] .= '</option>'; 
							}
						}
						$formSelector['timeslot'] .= '</select>';
						$formSelector['timeslot'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['Timeslot'].'</label>';
					$formSelector['timeslot'] .= '</div>';
					break;
				case 'date':
					//date
					$formSelector['date'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['date'] .= '<input id="date" name="date" class="datepicker" value="'.$date.'">';
					$formSelector['date'] .= '<label for="date" class="active">'.$Lang['eBooking']['App']['FieldTitle']['BookingDate'].'</label>';
					$formSelector['date'] .= '</div>';
					break;
// 				case 'dateArr':
// 					//dataArr HiddenField
// 					$formSelector['dateArr'] = '';
// 					foreach ( (array)$selectorData['dateArr'] as $_date){
// 						$formSelector['dateArr'] .= '<input type="hidden" name="date[]" value="'.$_date.'">';
// 					}
// 					break;
				case 'startDate':
					$formSelector['startDate'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['startDate'] .= '<input id="startDate" name="startDate" class="datepicker req-fromdate" value="'.$startDate.'">';
					$formSelector['startDate'] .= '<label for="date" class="active">'.$Lang['eBooking']['App']['FieldTitle']['StartDate'].'</label>';
					$formSelector['startDate'] .= '</div>';
					break;
				case 'endDate':
					//endDate
					$formSelector['endDate'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['endDate'] .= '<input id="endDate" name="endDate" class="datepicker req-todate" value="'.$endDate.'">';
					$formSelector['endDate'] .= '<label for="date" class="active">'.$Lang['eBooking']['App']['FieldTitle']['EndDate'].'</label>';
					$formSelector['endDate'] .= '</div>';
					break;
				case 'dayMode':
					//weekDays OR cycleDays
					#week day
					$this->infoArr['postData']['formData']['day_weekday'] = '';
					foreach ((array)$Lang['eBooking']['App']['weekday'] as $_weekdayVaule => $_weekdayText){
						if(!empty($selectorData['day'])){
							$selected = (in_array($_weekdayVaule,$selectorData['day']))? 'selected':'';
						}else{
							//default all selected
							$selected = 'selected';
						}
						$this->infoArr['postData']['formData']['day_weekday'] .= '<option value="'.$_weekdayVaule.'" '.$selected.'>'.$_weekdayText.'</option>';
					}
					
					#cycle day
					$this->infoArr['postData']['formData']['day_cycleday'] = '';
					foreach((array)$selectorData['cycleDayArr'] as $_cycleDay => $_cycleDayStr){
						if(!empty($selectorData['day'])){
							$selected = (in_array($_cycleDay, $selectorData['day']))?'selected':'';
						}else{
							$selected = 'selected';
						}
						$this->infoArr['postData']['formData']['day_cycleday'] .= '<option value="'.$_cycleDay.'" '.$selected.'>'.str_replace('<--Num-->', Get_Lang_Selection($_cycleDayStr['TextChi'], $_cycleDayStr['TextEng']), $Lang['eBooking']['App']['FieldTitle']['CycleDay']).'</option>';
					}
					
					if($daymode=='weekday'){
						$weekdaySelect = 'selected';
						$cycledaySelect = '';
					}elseif($daymode=='cycleday'){
						$weekdaySelect = '';
						$cycledaySelect = 'selected';
					}
					$formSelector['dayMode'] = '<div class="input-field col col-xs-12 col-sm-6">';
					$formSelector['dayMode'] .= '<select id="daymode" name="daymode" class="select_ajaxload" readyonly>';
					
					//get the default value of eBookingSetting if not set
					if (! isset($eBookingSetting['WeekDayEnabled']) && ! isset($eBookingSetting['CycleEnabled'])) {
					    $weekDayEnabled = 1;
					    $cycleEnabled = 1;
					}else{
					    $weekDayEnabled = $eBookingSetting['WeekDayEnabled'];
					    $cycleEnabled = $eBookingSetting['CycleEnabled'];
					}
					
					if(!$weekDayEnabled && $cycleEnabled && empty($selectorData['cycleDayArr'])){
						$formSelector['dayMode'] .= '<option value="" disabled>'.'no cycle day'.'</option>';
					}else{
						$formSelector['dayMode'] .= '<option value="" disabled>'.$Lang['eBooking']['App']['Selector']['PleaseSelect'].'</option>';
						if($weekDayEnabled){
							$formSelector['dayMode'].= '<option value="weekday" '.$weekdaySelect.'>'.$Lang['eBooking']['App']['Selector']['weekday'].'</option>';
						}
						if(!empty($selectorData['cycleDayArr']) && $cycleEnabled){
							$formSelector['dayMode'] .= '<option value="cycleday" '.$cycledaySelect.'>'.$Lang['eBooking']['App']['Selector']['cycleday'].'</option>';
						}
					}
					$formSelector['dayMode'] .= '</select>';
					$formSelector['dayMode'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['RepeatFrequency'].'</label>';
					$formSelector['dayMode'] .= '</div>';
					break;
				case 'weekCycleDays':
					$formSelector['weekCycleDays'] = '<div class="input-field col col-xs-12 col-sm-6 weekDays" >';
					$formSelector['weekCycleDays'] .= '<select id="day" name="day[]" class="weekCycleDays" multiple readyonly>';
					$formSelector['weekCycleDays'] .= '</select>';
					$formSelector['weekCycleDays'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['RepeatRule'].'</label>';
					$formSelector['weekCycleDays'] .= '</div>';
					break;
				case 'object':
					$formSelector['object'] = '<div class="input-field col s12 row">';
					if($selectorData['bookingType']=='room'){
						##Available Room
						$AvailableRoom = $this->infoArr["api"]['data']['availRoom'];
						$formSelector['object'] .= '<select name="roomID" id="roomID">';
						if(!empty($AvailableRoom)){
							//get build asso arr
							$buildAsso = BuildMultiKeyAssoc($AvailableRoom, 'BuildingID', array('BuildingNameChi','BuildingNameEng'));
							//get level asso arr
							$levelAsso = BuildMultiKeyAssoc($AvailableRoom, array('BuildingID','LocationLevelID'), array('LevelNameChi','LevelNameEng'));
							//get location ass arr
							$availableRoomAsso = BuildMultiKeyAssoc($AvailableRoom, array('BuildingID','LocationLevelID','LocationID'));
							
							foreach((array)$buildAsso as $buildingID => $_building){
								$formSelector['object'].= '<optgroup label="'.Get_Lang_Selection($_building['BuildingNameChi'], $_building['BuildingNameEng']).'">';
								foreach((array)$levelAsso[$buildingID] as $levelID => $_level){
									$formSelector['object'].= '<optgroup label="'.'&nbsp;&nbsp;'.Get_Lang_Selection($_level['LevelNameChi'], $_level['LevelNameEng']).'">';
									foreach ((array)$availableRoomAsso[$buildingID][$levelID] as $_room){
										$selected = ($roomID==$_room['LocationID'])?'selected':'';
										$formSelector['object'].= '<option value="'.$_room['LocationID'].'" '.$selected.'>';
										$formSelector['object'].= Get_Lang_Selection($_room['NameChi'],$_room['NameEng']);
										$formSelector['object'].= '</option>';
									}
									$formSelector['object'].= '</optgroup>';
								}
								$formSelector['object'].= '</optgroup>';
							}
						}else{
							$formSelector['object'] .= '<option>'.$Lang['eBooking']['App']['Selector']['NoAvailableRoom'].'</option>';
						}
						$formSelector['object'] .= '</select>';
					}elseif($selectorData['bookingType']=='item'){
						$formSelector['object'] .= '<select multiple name="itemID[]" id="itemID" class="required">';
					
						//item
						$AvailableItem = $this->infoArr["api"]['data']['availItem'];
						if(!empty($AvailableItem)){
							//prepare for location mapping
							$catAssoArr = BuildMultiKeyAssoc($AvailableItem, 'CategoryID', array('catNameChi','catNameEng'));
							$cat2AssoArr = BuildMultiKeyAssoc($AvailableItem, array('CategoryID','cat2CategoryID'), array('cat2NameChi','cat2NameEng'));
							$itemAssoArr = BuildMultiKeyAssoc($AvailableItem, array('CategoryID','cat2CategoryID','ItemID'),array('ItemID','NameChi','NameEng'));
					
							$formSelector['object'] .= '<option value="" disabled>'.$Lang['eBooking']['App']['Selector']['PleaseSelect'].'</option>';
							foreach((array)$catAssoArr as $_catCategoryID => $_catAssoArr){
								$name = Get_Lang_Selection($_catAssoArr['catNameChi'], $_catAssoArr['catNameEng']);
								$formSelector['object'] .= '<optgroup label="'.$name.'">';
								foreach ((array)$cat2AssoArr[$_catCategoryID] as $_cat2CategoryID => $_cat2AssoArr){
									$name = Get_Lang_Selection($_cat2AssoArr['cat2NameChi'], $_cat2AssoArr['cat2NameEng']);
									$formSelector['object'] .= '<optgroup label="'.'&nbsp;'.$name.'">';
									foreach ((array)$itemAssoArr[$_catCategoryID][$_cat2CategoryID] as $_itemID => $_itemAssoArr){
										$name = Get_Lang_Selection($_itemAssoArr['NameChi'], $_itemAssoArr['NameEng']);
										$name = '&nbsp;'.'&nbsp;'.$name;
										$selected = in_array($_itemID,(array)$itemID)? 'selected': '';
										$formSelector['object'] .= '<option value="'.$_itemID.'" '.$selected.'>'.$name.'</option>';
									}
									$formSelector['object'] .= '</optgroup>';
								}
								$formSelector['object'] .= '</optgroup>';
							}
							unset($AvailableItem);
						}else{
							$formSelector['object'] .= '<option>'.$Lang['eBooking']['App']['Selector']['NoAvailableRoom'].'</option>';
						}
						$formSelector['object'] .= '</select>';
					}else{
						$formSelector['object'] .= '<select>';
						$formSelector['object'] .= '<option>'.$Lang['eBooking']['App']['Selector']['NoAvailableRoom'].'</option>';
						$formSelector['object'] .= '</select>';
					}
					$formSelector['object'] .= '<label>'.(($selectorData['bookingType']=='item')?$Lang['eBooking']['App']['FieldTitle']['Item']:$Lang['eBooking']['App']['FieldTitle']['Room']).'</label>';
					$formSelector['object'] .= '</div>';
					
					break;
					
				case 'timeHeader':
					$time_header_count = '0';
					
					if($this->infoArr['postData']['formData']['bookingType'] == 'item'){
						$itemID = $this->infoArr['postData']['formData']['itemID'];
						$AllItem = $ebooking_api->ReturnAllItem();
// 						$AllItem_itemID = BuildMultiKeyAssoc($AllItem, 'ItemID');
						foreach ((array)$itemID as $_itemID){
							$selectorData['ItemDetails'][] = $AllItem[$_itemID];
						}
					}else{
						//get selected location detail
						$locationID = $this->infoArr['postData']['formData']['roomID'];
						$Alllocation = $ebooking_api->ReturnAllLocation();
// 						$Alllocation_locationID = BuildMultiKeyAssoc($Alllocation, 'LocationID');
						$selectorData['LocationDetail'] = $Alllocation[$locationID];
					}
					
					$time_header = '';
					//room
					if($selectorData['LocationDetail']['NameChi'] && $selectorData['bookingType'] == 'room'){
						$time_header[$time_header_count] .= Get_Lang_Selection($selectorData['LocationDetail']['BuildingNameChi'], $selectorData['LocationDetail']['BuildingNameEng']).'>';
						$time_header[$time_header_count] .= Get_Lang_Selection($selectorData['LocationDetail']['LevelNameChi'], $selectorData['LocationDetail']['LevelNameEng']).'>';
						$time_header[$time_header_count] .= Get_Lang_Selection($selectorData['LocationDetail']['NameChi'], $selectorData['LocationDetail']['NameEng']);
						$time_header[$time_header_count] .= '&nbsp;'.'&nbsp;';
						$time_header_count++;
					}
					//item
					if($selectorData['bookingType'] == 'item' && !empty($selectorData['ItemID'])){
						$comma = '';
						foreach ((array)$selectorData['ItemID'] as $index=>$_itemID){
							$time_header[$time_header_count] .= $comma.Get_Lang_Selection($selectorData['ItemDetail'][$index]['NameChi'], $selectorData['ItemDetail'][$index]['NameEng']);
							$comma = ',';
						}
						$time_header[$time_header_count] .= '&nbsp;'.'&nbsp;';
						$time_header_count++;
					}
					//Date
					if($selectorData['date']){
						$time_header[$time_header_count] = implode(',',(array)$selectorData['date']).'&nbsp;'.'&nbsp;';
						$time_header_count++;
					}
					//startDate && endDate
					if($selectorData['startDate'] && $selectorData['endDate']){
						$time_header[$time_header_count] = $selectorData['startDate'];
						$time_header[$time_header_count] .= '-'.$selectorData['endDate'];
						$time_header_count++;
					}
					if($selectorData['day']){
						$day_display = '';
						$comma = '';
						foreach ((array)$selectorData['day'] as $_day){
							$day_display .= $comma.($Lang['eBooking']['App']['weekday'][$_day]?$Lang['eBooking']['App']['weekday'][$_day]:str_replace('<--Num-->', Get_Lang_Selection($selectorData['cycleDayArr'][$_day]['TextChi'], $selectorData['cycleDayArr'][$_day]['Eng']), $Lang['eBooking']['App']['FieldTitle']['CycleDay']) );
							$comma = ',';
						}
						$time_header[$time_header_count] = $day_display;
						$time_header_count++;
					}
					//start time && end time
					if($selectorData['startTime'] && $selectorData['endTime']){
						$time_header[$time_header_count] = $selectorData['startTime'];
						$time_header[$time_header_count] .= ' - '.$selectorData['endTime'];
						$time_header_count++;
					}
					
					$comma = '';
					if($selectorData['AvailableTime']){
						$time_header[$time_header_count] .= $Lang['eBooking']['App']['FieldTitle']['AvailableTimeSlot'];
						foreach ((array)$selectorData['AvailableTime'][str_replace('/','-',$selectorData['date'])] as $_timeslot){
							$time_header[$time_header_count] .= $comma.$_timeslot;
							$comma = ',';
						}
						$time_header_count++;
					}
					if($selectorData['timeslotDetail']){
						foreach ((array) $selectorData['timeslotDetail'] as $_timeslotDetail){
							$time_header[$time_header_count] .= $_timeslotDetail['TimeSlotName'].':'.'&nbsp;';
							$time_header[$time_header_count] .= date('H:i',strtotime($_timeslotDetail['StartTime'])).'-'. date('H:i',strtotime($_timeslotDetail['EndTime']));
							$time_header_count++;
						}
					}
					$formSelector['timeHeader'] = '<ul class="collection with-header">';
					$isFirst = true;
						foreach ((array)$time_header as $_time_header){
							$formSelector['timeHeader'] .= '<li class="collection-item">';
							if($isFirst){
								$formSelector['timeHeader'] .= '<h5>';
							}
							$formSelector['timeHeader'] .= $_time_header;
							if($isFirst){
								$formSelector['timeHeader'] .= '</h5>';
							}
							$formSelector['timeHeader'] .= '</li>';
							$isFirst = false;
						}
					$formSelector['timeHeader'] .= '</ul>';
					break;
				case 'remarks':
					$formSelector['remarks'] = '<ul class="collapsible" data-collapsible="accordion">';
					$formSelector['remarks'] .= '<li>';
					$formSelector['remarks'] .= '<div class="collapsible-header">'.$Lang['eBooking']['App']['FieldTitle']['More'].'</div>';
					$formSelector['remarks'] .= '<div class="collapsible-body">';
						//attach	
// 						$formSelector['remarks'] .= '<div class="file-field input-field">';
// 							$formSelector['remarks'] .= '<div class="btn btn-sm">';
// 								$formSelector['remarks'] .= '<span>'.$Lang['eBooking']['App']['FieldTitle']['Attach'].'</span>';
// 								$formSelector['remarks'] .= '<input type="file" multiple>';
// 							$formSelector['remarks'] .= '</div>';
// 							$formSelector['remarks'] .= '<div class="file-path-wrapper">';
// 								$formSelector['remarks'] .= '<input class="file-path validate" type="text" placeholder="請選擇檔案">';
// 							$formSelector['remarks'] .= '</div>';
// 						$formSelector['remarks'] .= '</div>'; 
						//remarks
						$formSelector['remarks'] .= '<div class="input-field" id="dropdown_opt">';
						$formSelector['remarks'] .= '<label for="remarks"'.($remarks?'class="active"':"").' >'.$Lang['eBooking']['App']['FieldTitle']['Remarks'].'</label>';
// 							$formSelector['remarks'] .= '<input type="text" id="remarks" name="remarks" rel="dropdown_opt" class="" value="'.$remarks.'">';
							$formSelector['remarks'] .= '<textarea rows="1" cols="50" id="remarks" name="remarks" rel="dropdown_opt" style="border:none; border-bottom-style:inset;">'.$remarks.'</textarea>';
						$formSelector['remarks'] .= '</div>';
					$formSelector['remarks'] .= '</div>';
					break;
				case 'availableTime':
					$formSelector['availableTime'] = '<div class="input-field col col-xs-12 col-sm-6">';
						$formSelector['availableTime'] .= '<select multiple id="availableTime" name="availableTime[]" class="required" readyonly >';
						if($selectorData['AvailableBookingDetail']){
							foreach ((array)$selectorData['AvailableBookingDetail'] as $key=>$_availableTimeArr){
								$formSelector['availableTime'] .= '<option value="'.$key.'" selected>'.$_availableTimeArr['Date'].'('.$_availableTimeArr['StartTime'].'-'.$_availableTimeArr['EndTime'].')'.'</option>';
							}
						}else{
							$formSelector['availableTime'] .= '<option disable>'.'no available time'.'</option>';
						}
						$formSelector['availableTime'] .= '</select>';
						$formSelector['availableTime'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['AvailableTimeSlot2'].'</label>';
					$formSelector['availableTime'] .= '</div>';
					break;
				case 'unAvailableTime':
					$formSelector['unAvailableTime'] = '<div class="input-field col col-xs-12 col-sm-6">';
						$formSelector['unAvailableTime'] .= '<select multiple id="unAvailableTime" readyonly>';
						foreach ((array)$selectorData['unAvailableBookingDetail'] as $key=>$_unAvailableTimeArr){
							$formSelector['unAvailableTime'] .= '<option value="'.$key.'" selected>'.$_unAvailableTimeArr['Date'].'('.$_unAvailableTimeArr['StartTime'].'-'.$_unAvailableTimeArr['EndTime'].')'.$Lang['eBooking']['App']['FieldTitle']['ErrorCode'][$_unAvailableTimeArr['error']].'</option>';
						}
						$formSelector['unAvailableTime'] .= '</select>';
						$formSelector['unAvailableTime'] .= '<label>'.$Lang['eBooking']['App']['FieldTitle']['NotAvailableTimeSlot2'].'</label>';
					$formSelector['unAvailableTime'] .= '</div>';
					break;
			}
		}
		$this->infoArr['formSelectorHTML']= '';
		$count = 0;
		foreach ((array)$formSelector as $selectorName => $_selector){
			if($count%2 == '0'){
				$this->infoArr['formSelectorHTML'].= '<div class="row">';
			}
			$this->infoArr['formSelectorHTML'].= $_selector;
			if($count%2 == '1'){
				$this->infoArr['formSelectorHTML'].= '</div>';
			}
			$count++;
		}
		return $this->infoArr['formSelectorHTML'];
	}
	
	private function closeEbookingApiConnection(){
		intranet_closedb();
	}
}