<?php
/*
 *  Using:
 *
 *  Purpose:    header file for eInventorywebview
 *
 *  Log
 *  
 *  2020-03-09 Cameron
 *      - handle character encoding problem in ej
 *      
 *  2019-12-16 Cameron
 *      - create this file
 */

if ($intranet_version == "2.0" || isset($junior_mck)) {
    $characterset = 'big5-hkscs';
}
else {
    $characterset = 'utf-8';
}
?>

<!doctype html>
<html>
<head>
    <meta charset="<?php echo $characterset;?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $Lang['Header']['Menu']['eInventory'];?></title>
    <script type="text/javascript" src="../web/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../web/js/moment.js"></script>

    <script type="text/javascript" src="../web/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../web/js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../web/bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../web/js/offcanvas.js"></script>
    <link rel="stylesheet" href="../web/css/offcanvas.css">
    <link rel="stylesheet" type="text/css" href="../web/css/eInventory.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>