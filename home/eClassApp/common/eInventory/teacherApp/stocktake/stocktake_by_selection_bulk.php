<?php
/*
 *  2020-04-10 Cameron
 *      - show admin group name and allow to input remark
 *      
 *  2020-03-09 Cameron
 *      - fix character encoding problem (use ascii colon instead of Chinese) in variance
 *
 *  2020-02-18 Cameron
 *      - always show funding source for bulk item
 *       
 *  2020-02-04 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$itemID = IntegerSafe($_POST['BulkItemID']);
$locationID = IntegerSafe($_POST['LocationID']);
$adminGroupID = IntegerSafe($_POST['AdminGroupID']);
$fundingSourceID = IntegerSafe($_POST['BulkFundingSourceID']);
//$showFundingSource = IntegerSafe($_POST['ShowFundingSource']);
$itemPhoto = '';

//debug_pr($_POST);
$itemDetail = $linventory->getBulkItemDetail($itemID, $locationID, $adminGroupID, $fundingSourceID);
//  debug_pr($itemDetail);
//  exit;
if (count($itemDetail) == 1) {
    $itemDetail = $itemDetail[0];
    $itemName = Get_Lang_Selection($itemDetail['NameChi'], $itemDetail['NameEng']);
    $itemName = $itemName ? $itemName : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemDescription = Get_Lang_Selection($itemDetail['DescriptionChi'], $itemDetail['DescriptionEng']);
    $itemDescription = $itemDescription ? $itemDescription : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemPhoto = $itemDetail['PhotoName'] ? $PATH_WRT_ROOT.$itemDetail['PhotoPath'].'/'.$itemDetail['PhotoName'] : '';
    $itemExpectedQty = $itemDetail['ExpectedQty'];
    $itemStockCheckQty= $itemDetail['StockCheckQty'];
    $itemVariance= $itemDetail['Variance'];
    if ($itemStockCheckQty == '') {
        $itemStockCheckQty = $itemExpectedQty;  // set to expected qty if not do any stocktake yet
        $itemVariance = 0;
    }
    
    $itemRemark= $itemDetail['Remark'];

    $adminGroupName = $linventory->returnGroupNameByGroupID($adminGroupID);
    
//     if ($showFundingSource) {
//         $fundingSourceName = $linventory->returnFundingSourceByFundingID($fundingSourceID);
//     }
    $displayVariance = $itemVariance > 0 ? '+'.$itemVariance : $itemVariance;
    
    $itemFundingSourceID = $itemDetail['FundingSourceID'];
    $itemFundingSourceName = $linventory->returnFundingSourceByFundingID($itemFundingSourceID);
}
else {
    echo "<script>
            window.alert('".$Lang['eInventory']['eClassApp']['Error']['MultipleItems']."!');
            window.location.href = \"?task=teacherApp.stocktake.stocktake&LocationID=\" + ".$locationID." + \"&AdminGroupID=\" + ".$adminGroupID.";
        </script>";
}

include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
$(document).ready(function(){
	
	adjustStocktakeDetailHeight();

    $(document).on('click', '#btnConfirm', function(e) {
		var expectedQty = parseInt(<?php echo $itemExpectedQty;?>);
		var stocktakeQty = parseInt($('#StocktakeQty').val());
		var qtyChange = stocktakeQty - expectedQty;
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=teacherApp.ajax',
            data : {
                'action': 'updateBulkItemStocktake',
                'ItemID': '<?php echo $itemID;?>',
                'LocationID': '<?php echo $locationID;?>',
                'AdminGroupID': '<?php echo $adminGroupID;?>',
                'FundingSourceID': '<?php echo $fundingSourceID;?>',
                'QtyChange': qtyChange,
                'StocktakeQty': stocktakeQty,
                'Remark': $('#inputRemarks').val()
            },
            success: backToStocktake,
            error: show_ajax_error
        });
    });

    $(document).on('click', '#divBtnMinus', function(e) {
		var expectedQty = parseInt(<?php echo $itemExpectedQty;?>);
		var stocktakeQty = parseInt($('#StocktakeQty').val());
		if (stocktakeQty > 0) {
			stocktakeQty-- 
			$('#StocktakeQty').val(stocktakeQty);
			var variance = stocktakeQty - expectedQty;
			if (variance > 0) {
				variance = '+' + variance;
			}
			$('#itemVariance').html(variance);
		}
    });

    $(document).on('click', '#divBtnPlus', function(e) {
		var expectedQty = parseInt(<?php echo $itemExpectedQty;?>);
		var stocktakeQty = parseInt($('#StocktakeQty').val());
		if (stocktakeQty >= 0) {
			stocktakeQty++; 
			$('#StocktakeQty').val(stocktakeQty);
			var variance = stocktakeQty - expectedQty;
			if (variance > 0) {
				variance = '+' + variance;
			}
			$('#itemVariance').html(variance);
		}
    });

    $(document).on('change', '#StocktakeQty', function(e) {
		var expectedQty = parseInt(<?php echo $itemExpectedQty;?>);
		var stocktakeQty = parseInt($('#StocktakeQty').val());
		if (stocktakeQty > 0) {
			var variance = stocktakeQty - expectedQty;
			if (variance > 0) {
				variance = '+' + variance;
			}
			$('#itemVariance').html(variance);
		}
    });
    
});


function backToStocktake(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		window.location.href = "?task=teacherApp.stocktake.stocktake&LocationID=" + "<?php echo $locationID;?>" + "&AdminGroupID=" + "<?php echo $adminGroupID;?>";
	}	
	else {
//		alert('Error on update missing');
	}
}

<?php echo $linventory->getJsAjaxError();?>

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body>
    <div id="wrapper">
        <!-- Header -->
        <nav id="header" class="inventory navbar navbar-fixed-top edit">
            <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.stocktake.stocktake_by_selection&LocationID=".$locationID."&AdminGroupID=".$adminGroupID."&FundingSourceID=".$fundingSourceID."&ShowFundingSource=".$showFundingSource;?>"><i class="fas fa-arrow-left"></i></a></div></div>
            <div class="headerTitle withFunction"><?php echo $Lang['eInventory']['eClassApp']['NotDoneStockTakeItem'];?></div>
            <div id="button">
            </div>
		</nav>
		<div id="content" class="stockingDetails bluk">
			<div class="row">
				<div class="subTitle map col-xs-12">
					<div class="title"><?php echo $itemDetail['FullLocation'];?></div>
				</div>
			</div>

			<div class="itemImg">
<?php if ($itemPhoto):?>			
				<img src="<?php echo $itemPhoto;?>" alt="<?php echo $itemName;?>" />
<?php endif;?>				
			</div>
			<span class="itemTitle"><?php echo $itemName;?></span>
			<ul class="itemInfo">
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Code;?></span>
					<span class="text"><?php echo $itemDetail['ItemCode'];?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Description;?></span>
					<span class="text"><?php echo $itemDescription;?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem['Caretaker'];?></span>
					<span class="text"><?php echo $adminGroupName;?></span>
				</li>				
<?php //if ($showFundingSource):?>    				
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Funding;?></span>
					<span class="text"><?php echo $itemFundingSourceName;?></span>
				</li>
<?php //endif;?>    				
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Barcode;?></span>
					<span class="text"><?php echo $itemDetail['Barcode'];?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Stocktake_ExpectedQty;?></span>
					<span class="text"><?php echo $itemExpectedQty;?></span>
				</li>
            </ul>
             
			<ul class="itemInfoList">
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Remark;?></span>
					<div id="chNameField" class="fieldInput ">
						<input type="text" class="form-control inputField" id="inputRemarks" placeholder="" value="<?php echo $itemRemark;?>">
					</div>
				</li>
			</ul>
 			
			<div id="setProcessArea" class="bluk">
				<div class="form-group">
					<div class="col-xs-6">
						<span class="label"><?php echo $i_InventorySystem_Stocktake_StocktakeQty;?></span>
					</div>
					<div class="col-xs-6">
						<span class="variance"><span class="label "><?php echo $i_InventorySystem_Stocktake_VarianceQty;?> : </span><span id="itemVariance"><?php echo $displayVariance;?></span></span>
					</div>
					<div class="stocktakeQuantity col-xs-12">
						<div class="input-group">
							<div class="input-group-btn" id="divBtnMinus">
								<button type="button" class="btn btn-primary"><i class="fas fa-minus"></i></button>
							</div>
							<input type="text" class="form-control" id="StocktakeQty" value="<?php echo $itemStockCheckQty;?>">
							<div class="input-group-btn" id="divBtnPlus">
								<button type="button" class="btn btn-primary"><i class="fas fa-plus"></i></button>
							</div>
						</div>
					</div>
				</div>
				<div class="submitBtn col-xs-12" id="btnConfirm">
					<a href="#" class="btn btn-primary second" role="button">
						<span><?php echo $Lang['eInventory']['eClassApp']['AddToStockTakeBtn'];?></span>
					</a>
				</div>
			</div>
            
		</div>            
	</div>
</body>
</html>

