<?php
/*
 *  2020-03-25 Cameron
 *      - also show AdminGroupName
 *      
 *  2020-02-18 Cameron
 *      - always show funding source for bulk item
 *       
 *  2020-01-20 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$itemID = IntegerSafe($_GET['ItemID']);
$locationID = IntegerSafe($_GET['LocationID']);
$adminGroupID = IntegerSafe($_GET['AdminGroupID']);
$fundingSourceID = IntegerSafe($_GET['FundingSourceID']);
$stocktakeAdminGroupID = IntegerSafe($_GET['StocktakeAdminGroupID']);       // the admin group that the user has selected to do stocktake
//$showFundingSource = IntegerSafe($_GET['ShowFundingSource']);
$itemPhoto = '';

$itemDetail = $linventory->getBulkItemDetail($itemID, $locationID, $adminGroupID, $fundingSourceID);

if (count($itemDetail) == 1) {
    $itemDetail = $itemDetail[0];
    $itemName = Get_Lang_Selection($itemDetail['NameChi'], $itemDetail['NameEng']);
    $itemName = $itemName ? $itemName : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemDescription = Get_Lang_Selection($itemDetail['DescriptionChi'], $itemDetail['DescriptionEng']);
    $itemDescription = $itemDescription ? $itemDescription : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemPhoto = $itemDetail['PhotoName'] ? $PATH_WRT_ROOT.$itemDetail['PhotoPath'].'/'.$itemDetail['PhotoName'] : '';
    $itemExpectedQty = $itemDetail['ExpectedQty'];
    $itemStockCheckQty= $itemDetail['StockCheckQty'];
    $itemVariance= $itemDetail['Variance'];
    $itemRemark= $itemDetail['Remark'];
    $adminGroupName = $linventory->returnGroupNameByGroupID($adminGroupID);
//    if ($showFundingSource) {
        $fundingSourceName = $linventory->returnFundingSourceByFundingID($fundingSourceID);
//    }
    $displayVariance = $itemVariance > 0 ? '+'.$itemVariance : $itemVariance;
}
else {
    echo "<script>
            window.alert('".$Lang['eInventory']['eClassApp']['Error']['MultipleItems']."!');
            window.location.href = \"?task=teacherApp.stocktake.stocktake&LocationID=\" + ".$locationID." + \"&AdminGroupID=\" + ".$adminGroupID.";
        </script>";
}



include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
$(document).ready(function(){
	
	adjustStocktakeDetailHeight();

    $(document).on('click', '#EditBtn', function(e) {
        window.location.href = "?task=teacherApp.stocktake.edit_bulk_item&ItemID=" + "<?php echo $itemID;?>" + "&LocationID=" + "<?php echo $locationID;?>" + "&AdminGroupID=" + "<?php echo $adminGroupID;?>"+ "&FundingSourceID=" + "<?php echo $fundingSourceID;?>" + "&StocktakeAdminGroupID=" + "<?php echo $stocktakeAdminGroupID;?>";
    });
	
});

<?php echo $linventory->getJsAjaxError();?>

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body>
    <div id="wrapper">
        <!-- Header -->
        <nav id="header" class="navbar navbar-fixed-top inventory">
            <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.stocktake.stocktake&LocationID=".$locationID."&AdminGroupID=".$stocktakeAdminGroupID;?>"><i class="fas fa-arrow-left"></i></a></div></div>
            <div class="headerTitle withFunction"><?php echo $Lang['eInventory']['eClassApp']['StockTakeItemInfo'];?></div>
            <div id="button">
<!--
  				<div class="headerIcon"><a class="delete" id="DeleteBtn"><i class="fas fa-trash"></i></a></div>
-->
				<div class="headerIcon"><a id="EditBtn"><span class="editItem"></span></a></div>
            </div>
		</nav>
		<div id="content" class="stockingDetails">
			<div class="row">
				<div class="subTitle map col-xs-12">
					<div class="title"><?php echo $itemDetail['FullLocation'];?></div>
				</div>
			</div>

			<div class="itemImg">
<?php if ($itemPhoto):?>			
				<img src="<?php echo $itemPhoto;?>" alt="<?php echo $itemName;?>" />
<?php endif;?>				
			</div>
			<span class="itemTitle"><?php echo $itemName;?></span>
			<ul class="itemInfo">
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Code;?></span>
					<span class="text"><?php echo $itemDetail['ItemCode'];?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Description;?></span>
					<span class="text"><?php echo $itemDescription;?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem['Caretaker'];?></span>
					<span class="text"><?php echo $adminGroupName;?></span>
				</li>
<?php //if ($showFundingSource):?>				
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Funding;?></span>
					<span class="text"><?php echo $fundingSourceName;?></span>
				</li>
<?php //endif;?>				
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Barcode;?></span>
					<span class="text"><?php echo $itemDetail['Barcode'];?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Stocktake_ExpectedQty;?></span>
					<span class="text"><?php echo $itemExpectedQty;?></span>
				</li>
				<li class="alert">
					<span class="label"><?php echo $i_InventorySystem_Stocktake_StocktakeQty;?></span>
					<span class="text"><?php echo $itemStockCheckQty;?></span>
				</li>
				<li class="alert">
					<span class="label"><?php echo $i_InventorySystem_Stocktake_VarianceQty;?></span>
					<span class="text"><?php echo $displayVariance;?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Remark;?></span>
					<span class="text"><?php echo $itemRemark;?></span>
				</li>
            </ul>
		</div>            
	</div>
</body>
</html>

