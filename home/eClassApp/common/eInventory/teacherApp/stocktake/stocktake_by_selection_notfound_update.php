<?php
/*
 *  2020-02-03 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$linventory = $indexVar['linventory'];

$locationID = IntegerSafe($_POST['LocationID']);
$adminGroupID = IntegerSafe($_POST['AdminGroupID']);
$itemIDTypeFunding = $_POST['ItemIDTypeFunding'];

$itemNotFoundResult = array();
$linventory->Start_Trans();
for ($i=0,$iMax=count($itemIDTypeFunding);$i<$iMax;$i++) {
    list($_itemID, $_itemType, $_fundingSourceID) = explode('-',$itemIDTypeFunding[$i]);
    $itemNotFoundResult[] = $linventory->updateSingleItemMissing($_itemID, $locationID, $adminGroupID, $remark='');
}
if (!in_array(false,$itemNotFoundResult)) {
    $linventory->Commit_Trans();
}
else {
    $linventory->RollBack_Trans();
}

header("location: ?task=teacherApp.stocktake.stocktake&LocationID=".$locationID."&AdminGroupID=".$adminGroupID);

?>

