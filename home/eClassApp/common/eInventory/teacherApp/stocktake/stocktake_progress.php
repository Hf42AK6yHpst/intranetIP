<?php
/*
 *  2020-03-23 Cameron
 *      - fix: should call adjustContentHeight() if $stocktakeProgress is not empty
 *      
 *  2020-03-17 Cameron
 *      - slideDown filter effect on page load
 *      
 *  2020-02-18 Cameron
 *      - show filter result if ShowFilterResult is set
 *      
 *  2019-12-16 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

# set cookies
$arrCookies[] = array("ck_FilterBy", "FilterBy");
$arrCookies[] = array("ck_AdminGroupID", "AdminGroupID");
$arrCookies[] = array("ck_TargetFloor", "TargetFloor");
$arrCookies[] = array("ck_TargetLocation", "TargetLocation");
if(isset($clearCoo) && $clearCoo == 1)
{
    clearCookies($arrCookies);
}
else {
    $FilterBy = $_COOKIE['ck_FilterBy'];
    $AdminGroupID = $_COOKIE['ck_AdminGroupID'];
    $TargetFloor = $_COOKIE['ck_TargetFloor'];
    $TargetLocation = $_COOKIE['ck_TargetLocation'];
}

$adminGroupIDAry = $linventory->getAdminGroupIDAry();
$adminGroupAry = $linventory->returnAdminGroup($adminGroupIDAry);
$stocktakeProgress = '';

if (count($adminGroupAry) ==1) {
    ## retrieve stocktake progress if there's only one eInventory admin group
    $stocktakeProgress = $linventory->getStocktakeProgressByAdminGroupUI($adminGroupAry[0]['AdminGroupID']);
}

$FilterBy = $FilterBy ? $FilterBy : 3;      // default by AdminGroup
$filterBySelection = $linventory->getStocktakeFilterByUI($FilterBy);        // select location or admin group to view progress
$adminGroupSelection = getSelectByArray($adminGroupAry, 'name="AdminGroupID" id="AdminGroupID" class="selectpicker"', $AdminGroupID, $all=0, $noFirst=1);

//$locationSelection = $linventory->getLocationSelectionUI($TargetLocation);
$buildingFloorSelection = $linventory->getBuildingFloorSelectionUI($TargetFloor);

$locationSelection = $linventory->getRoomSelectionUI($TargetFloor, $TargetLocation);

include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
$(document).ready(function(){

    $(document).on('change', '#filterBy', function(e) {
        if ($('#filterBy').val() == "1") {      // by location
        	$('#FloorSelectionSpan').css('display','');
            $('#AdminGroupSelectionSpan').css('display','none');
            var selectedFloor = $('#TargetFloor option:selected').val();
            getLocationByFloor(selectedFloor);
        }
        else {  // by Admin Group
            $('#LocationSelectionSpan').css('display','none');
        	$('#FloorSelectionSpan').css('display','none');
            $('#AdminGroupSelectionSpan').css('display','');
        }
    });

    $(document).on('change', '#TargetFloor', function(e) {
        var selectedFloor = $('#TargetFloor option:selected').val();
        getLocationByFloor(selectedFloor);
    });
    
    $(document).on('click', '#btnView', function(e) {
        var floorID, locationID, adminGroupID;
        if ($('#filterBy').val() == "1") {      // by location
        	adminGroupID = '';
            floorID = $('#TargetFloor').val();
            locationID = $('#TargetLocation').val();
        }
        else {  // by Admin Group
            adminGroupID = $('#AdminGroupID').val();
            floorID = '';
            locationID = '';
        }
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=teacherApp.ajax',
            data : {
                'action': 'getStocktakeProgress',
                'filterBy': $('#filterBy').val(),
                'adminGroupID': adminGroupID,
                'floorID': floorID,
                'locationID': locationID
            },
            success: showStocktakeProgress,
            error: show_ajax_error
        });
    });

    $(document).on('click', 'ul.locationList a.incomplete, ul.groupList a.incomplete', function(e) {
        var locationID = $(this).attr('data-LocationID');
        var adminGroupID = $(this).attr('data-AdminGroupID');
        window.location.href = "?task=teacherApp.stocktake.stocktake&LocationID=" + locationID + "&AdminGroupID=" + adminGroupID;
    });

    $(document).on('click', 'ul.locationList a.complete, ul.groupList a.complete', function(e) {
        var locationID = $(this).attr('data-LocationID');
        var adminGroupID = $(this).attr('data-AdminGroupID');
        window.location.href = "?task=teacherApp.stocktake.view_stocktake_done&LocationID=" + locationID + "&AdminGroupID=" + adminGroupID;
    });

<?php if ($_GET['ShowFilterResult']):?>
	$('#btnView').click();
<?php elseif ($stocktakeProgress !=''):?>
	adjustContentHeight();
<?php endif;?>

<?php if ($stocktakeProgress==''):?>
	$('div#filter').slideDown(500);
	$('div#filter').addClass('in').css('display','');
<?php endif;?>

    
});

function getLocationByFloor(floorID)
{
    $.ajax({
        dataType: "json",
        type: "POST",
        url: '?task=teacherApp.ajax',
        data : {
            'action': 'getLocationByFloor',
            'floorID': floorID
        },
        success: showLocationSelection,
        error: show_ajax_error
    });
}

function showLocationSelection(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success) {
		$('#LocationSelectionSpan').css('display','');
		$('#LocationSelectionSpan').html(ajaxReturn.LocationSelection);
		$('.selectpicker').selectpicker('refresh');
	}
}

function showStocktakeProgress(ajaxReturn)
{
    if (ajaxReturn != null && ajaxReturn.success) {
        $('#content').html(ajaxReturn.StocktakeProgress);
        $('.headerIcon .filter').click();
        adjustContentHeight();
    }
}

<?php echo $linventory->getJsAjaxError();?>

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body>
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top inventory">
        <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.index";?>"><i class="fas fa-arrow-left"></i></a></div></div>
        <div class="headerTitle withFunction"><?php echo $i_InventorySystem_Report_Stocktake_Progress;?></div>
        <div id="button">
            <div class="headerIcon"><a class="filter" role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter"> <span class="filter active"></span> </a></div>
        </div>
        <div class="collapse" id="filter">
            <ul class="fillerList stocktake">
                <li>
                    <?php echo $filterBySelection;?>
                </li>
                <li>
                    <span id="AdminGroupSelectionSpan" style="display: <?php echo ($FilterBy==3) ? '' : 'none'; ?>"><?php echo $adminGroupSelection;?></span>
                    <span id="FloorSelectionSpan" style="display: <?php echo ($FilterBy==1) ? '' : 'none'; ?>"><?php echo $buildingFloorSelection;?></span>
                </li>
                <li>
                	<span id="LocationSelectionSpan" style="display: <?php echo ($FilterBy==1) ? '' : 'none'; ?>"><?php echo $locationSelection;?></span>
                </li>
            </ul>
            <div class="submitBtn">
                <a href="#" class="btn btn-primary" role="button" id="btnView"><?php echo $Lang['Btn']['View'];?></a>
            </div>
        </div>
    </nav>
    <div id="content" class="stocktake">
        <?php echo $stocktakeProgress;?>
    </div>
</div>
</body>
</html>

