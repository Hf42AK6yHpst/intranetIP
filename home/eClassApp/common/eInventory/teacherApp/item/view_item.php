<?php
/*
 *  2020-04-09 Cameron
 *      - hide delete and edit button first as they are not available yet
 *      
 *  2020-03-06 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$itemID = IntegerSafe($_GET['ItemID']);
$itemPhoto = '';

$itemType = $linventory->returnItemType($itemID);
if ($itemType == ITEM_TYPE_SINGLE)
{
    $itemDetail = $linventory->getSingleItemMoreDetail($itemID);
}
else {
    $itemDetail = $linventory->getBulkItemMoreDetail($itemID);
}

if ($itemDetail != '') {
    $itemName = $itemDetail['ItemName'];
    $itemName = $itemName ? $itemName : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemDescription = $itemDetail['DescriptionName'];
    $itemDescription = $itemDescription ? $itemDescription : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemPhoto = $itemDetail['PhotoName'] ? $PATH_WRT_ROOT.$itemDetail['PhotoPath'].'/'.$itemDetail['PhotoName'] : '';
    $itemCategory = intranet_htmlspecialchars($itemDetail['CategoryName']) . ' > ' . intranet_htmlspecialchars($itemDetail['SubCategoryName']);
    if ($itemType == ITEM_TYPE_SINGLE) {
        $itemBrand = $itemDetail['Brand']? $itemDetail['Brand']: $Lang['eInventory']['eClassApp']['EmptySymbol'];
        $itemPurchaseDate = $itemDetail['PurchaseDate']? $itemDetail['PurchaseDate']: $Lang['eInventory']['eClassApp']['EmptySymbol'];
    }
}

include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
$(document).ready(function(){
	
    $(document).on('click', '#EditBtn', function(e) {
//        window.location.href = "?task=teacherApp.item.edit_single_item&ItemID=" + "<?php echo $itemID;?>" + "&LocationID=" + "<?php echo $locationID;?>" + "&AdminGroupID=" + "<?php echo $adminGroupID;?>";
    });
	
});

<?php echo $linventory->getJsAjaxError();?>

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body>
    <div id="wrapper">
        <!-- Header -->
        <nav id="header" class="navbar navbar-fixed-top ole">
            <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.item.index";?>"<i class="fas fa-arrow-left"></i></a></div></div>
            <div class="headerTitle withFunction withIcon"><?php echo $Lang['eInventory']['eClassApp']['ItemInfo'];?></div>
            <div id="button">
<!--   				<div class="headerIcon"><a class="delete" id="DeleteBtn"><i class="fas fa-trash"></i></a></div> -->
<!-- 				<div class="headerIcon"><a id="EditBtn"><span class="editItem"></span></a></div> -->
            </div>
		</nav>
		<div id="content" class="infoPage">
			<div class="itemImg">
<?php if ($itemPhoto):?>			
				<img src="<?php echo $itemPhoto;?>" alt="<?php echo $itemName;?>" />
<?php endif;?>				
			</div>
			<span class="itemTitle"><?php echo $itemName;?></span>
			<ul class="itemInfo">
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Code;?></span>
					<span class="text"><?php echo $itemDetail['ItemCode'];?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Description;?></span>
					<span class="text"><?php echo $itemDescription;?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Barcode;?></span>
					<span class="text"><?php echo $itemDetail['Barcode'];?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem['Category'];?></span>
					<span class="text"><?php echo $itemCategory;?></span>
				</li>
<?php if ($itemType == ITEM_TYPE_SINGLE):?>				
				<li>
					<span class="label"><?php echo $i_InventorySystem['Location'];?></span>
					<span class="text"><?php echo intranet_htmlspecialchars($itemDetail['FullLocation']);?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Group_Name;?></span>
					<span class="text"><?php echo intranet_htmlspecialchars($itemDetail['AdminGroupName']);?></span>
				</li>
<?php endif;?>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Ownership;?></span>
					<span class="text"><?php echo $itemDetail['OwnerShip'];?></span>
				</li>
				
<?php if (($itemType == ITEM_TYPE_SINGLE) && !$sys_custom['eInventoryCustForSMC']):?>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Brand_Name;?></span>
					<span class="text"><?php echo $itemBrand;?></span>
				</li>
<?php endif;?>

<?php if ($itemType == ITEM_TYPE_SINGLE):?>				
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Purchase_Date;?></span>
					<span class="text"><?php echo $itemPurchaseDate;?></span>
				</li>
<?php endif;?>				
            </ul>
		</div>            
	</div>
</body>
</html>

