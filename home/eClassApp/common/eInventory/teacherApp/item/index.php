<?php
/*
 *  2020-03-12 Cameron
 *      - fix: don't show NoRecrod when scroll to bottom
 *      
 *  2020-03-04 Cameron
 *      - add scroll to bottom to get more record function
 *  
 *  2020-02-10 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];
$llocation_ui = $indexVar['location_ui'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

# set cookies
$arrCookies[] = array("ck_eInvApp_TargetItemType", "TargetItemType");
$arrCookies[] = array("ck_eInvApp_TargetCategory", "TargetCategory");
$arrCookies[] = array("ck_eInvApp_TargetSubCategory", "TargetSubCategory");
$arrCookies[] = array("ck_eInvApp_TargetBuilding", "TargetBuilding");
$arrCookies[] = array("ck_eInvApp_TargetFloor", "TargetFloor");
$arrCookies[] = array("ck_eInvApp_TargetRoom", "TargetRoom");
$arrCookies[] = array("ck_eInvApp_TargetGroup", "TargetGroup");
$arrCookies[] = array("ck_eInvApp_TargetFunding", "TargetFunding");
$arrCookies[] = array("ck_eInvApp_TargetTag", "TargetTag");
$arrCookies[] = array("ck_eInvApp_DisplayZeroQty", "DisplayZeroQty");

if(isset($clearCoo) && $clearCoo == 1)
{
    clearCookies($arrCookies);
}
else {
   updateGetCookies($arrCookies);       // assign variable from cookies
}

$filterAry = array();       // default filter
$filterAry['ItemType'] = $TargetItemType;
$filterAry['Category'] = $TargetCategory;
$filterAry['SubCategory'] = $TargetSubCategory;
$filterAry['BuildingID'] = $TargetBuilding;
$filterAry['FloorID'] = $TargetFloor;
$filterAry['RoomID'] = $TargetRoom;
$filterAry['Tag'] = $TargetTag;
$filterAry['GroupID'] = $TargetGroup;
$filterAry['FundingSourceID'] = $TargetFunding;
$filterAry['DisplayZeroQty'] = $DisplayZeroQty;

## get filter selection list
$categoryAry = $linventory->getCategoryName();

if ($sys_custom['eInventory_DefaultFilterNoAll']) {
    $noFirst = 1;
    $firstTitleAllType = '';
    $firstTitleAllCategory = '';
    
    if ($TargetItemType== "") {
        $TargetItemType= 1;
    }    
    
    if ($targetCategory == "") {
        $TargetCategory = $categoryAry[0]['CategoryID'];
    }
}
else {
    $noFirst = 0;
    $firstTitleAllType = $Lang['eInventory']['FieldTitle']['AllType'];
    $firstTitleAllCategory = $Lang['eInventory']['FieldTitle']['AllCategory'];
}

# ItemType
$itemTypeSelection = getSelectByArray($i_InventorySystem_ItemType_Array, 'name="TargetItemType" id="TargetItemType" class="selectpicker"', $TargetItemType, 1, $noFirst, $firstTitleAllType);

# Category
$categorySelection = getSelectByArray($categoryAry, 'name="TargetCategory" id="TargetCategory" class="selectpicker"', $TargetCategory, 1, $noFirst, $firstTitleAllCategory);

# Building
$buildingSelection = $llocation_ui->Get_Building_Selection($TargetBuilding, "TargetBuilding", '', 0, 0, $Lang['SysMgr']['Location']['All']['Building'],$HasFloorOnly=1, "selectpicker");

# Tag
$tagAry = $linventory->getValidTag();
$tagSelection = getSelectByArray($tagAry, 'name="TargetTag" id="TargetTag" class="selectpicker"', $TargetTag, 1, 0, $Lang["eInventory"]["SelectionAllTags"]);

# Admin Group
$adminGroupIDAry = $linventory->getAdminGroupIDAry();
$adminGroupAry = $linventory->returnAdminGroup($adminGroupIDAry);
$adminGroupSelection = getSelectByArray($adminGroupAry, 'name="TargetGroup" id="TargetGroup" class="selectpicker"', $TargetGroup, $all=0, $noFirst=0, $Lang['eInventory']['FieldTitle']['AllResourceMgmtGroup']);

# Funding Source
$fundingSourceAry = $linventory->getFundingSoureInUse($DisplayZeroQty);
$fundingSourceSelection = getSelectByArray($fundingSourceAry, 'name="TargetFunding" id="TargetFunding" class="selectpicker"', $TargetFunding, 1, 0, $Lang['eInventory']['FieldTitle']['AllFundingSource']);


$offset = 0;                // starting index of record
$numberOfRecord = 20;       // number of record to retrieve each time


include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
var offset = <?php echo $offset;?>;
var numberOfRecord = <?php echo $numberOfRecord; ?>;
var isGetAllRecord = false;
var element = $("#content");
var lastHeight = $("#content").css('height');
var prevScrollTop = 0;

$(document).ready(function(){

    $(document).on('change', '#TargetCategory', function(e) {
        if ($(this).val() != '') {
            $.ajax({
            	dataType: "json",
                type: "POST",
                url: '?task=teacherApp.ajax',
                data : {
                    'action': 'getSubCategory',
                    'TargetCategory': $(this).val(),
                    'TargetSubCategory': '<?php echo $TargetSubCategory;?>'
                },
                success: showSubCategory,
                error: show_ajax_error
            });            
        }
        else {
    		$('#LiSubCategory').css('display','none');
    		$('#LiSubCategory').html('');
        }
    });

    $(document).on('change', '#TargetBuilding', function(e) {
        if ($(this).val() != '') {
            $.ajax({
            	dataType: "json",
                type: "POST",
                url: '?task=teacherApp.ajax',
                data : {
                    'action': 'getFloorByBuilding',
                    'TargetBuilding': $(this).val(),
                    'TargetFloor': '<?php echo $TargetFloor;?>'
                },
                success: showFloorByBuilding,
                error: show_ajax_error
            });            
        }
        else {
    		$('#LiFloor').css('display','none');
    		$('#LiFloor').html('');
    		$('#LiRoom').css('display','none');
    		$('#LiRoom').html('');
        }
    });
    
    $(document).on('change', '#TargetFloor', function(e) {
        if ($(this).val() != '') {
            $.ajax({
            	dataType: "json",
                type: "POST",
                url: '?task=teacherApp.ajax',
                data : {
                    'action': 'getRoomByFloor',
                    'TargetFloor': $(this).val(),
                    'TargetRoom': '<?php echo $TargetRoom;?>'
                },
                success: showRoomByFloor,
                error: show_ajax_error
            });            
        }
        else {
    		$('#LiRoom').css('display','none');
    		$('#LiRoom').html('');
        }
    });


    $(document).on('change', '.selectpicker', function(e) {
        if ($(this).val() != '' ) {
        	$('#button span.filter').addClass('active');
        }
        else {
            checkFilter();
        }
    });

    $(document).on('change', '#DisplayZeroQty', function(e) {
		checkFilter();
    });

    $(document).on('click', '#button a', function(e) {
        $(this).attr('aria-expanded', false); 
		checkFilter();
    });

    $(document).on('click', '#FilterBtn', function(e) {
    	prevScrollTop = 0;
    	$('#content').scrollTop(0);    	
    	offset = 0;
    	getFilteredResult(offset, numberOfRecord, false);
    });

    $('#content').on('scroll', function(e) {
 		var contentHeight = $(this).height();					// the height of the content that can be seen
 		var scrollHeight = $(this).prop('scrollHeight');		// the height of the content that need to scroll to view
 		var scrollTop = $(this).scrollTop();

// console.log('scrollHeight = ' + $('#content').prop('scrollHeight'));
// console.log('prevScrollTop = ' + prevScrollTop);
// console.log('scrollTop = ' + $('#content').scrollTop());
// console.log('appHeight = ' + $('#content').height());

		if ((scrollTop > 0) && (scrollTop - prevScrollTop >= 100) && (scrollHeight - scrollTop - 100 <= contentHeight)){
			
 	 		if (!isGetAllRecord) {
 	 			offset += numberOfRecord;
 				getFilteredResult(offset, numberOfRecord, true);
 	 		}
 		}
 		if (scrollTop - prevScrollTop >= 100) {
 			prevScrollTop = scrollTop;
 		}
	});

});

function getFilteredResult(_offset, _numberOfRecord, _append)
{
	var showNoRecord = (_append == true) ? 0 : 1;
	$.ajax({
    	dataType: "json",
        type: "POST",
        url: '?task=teacherApp.ajax',
        data : $('#form1').serialize() + '&action=getItemList&offset=' + _offset + '&numberOfRecord=' + _numberOfRecord + '&showNoRecord=' + showNoRecord,
        success: function(ajaxReturn){
            showFilterResult(ajaxReturn, _append);
        },
        error: show_ajax_error
    });  
}

function checkFilter()
{
	var selectNone = true;
	$('.selectpicker').each(function(){
		if ($(this).val() != '') {
			selectNone = false;
		}
	});
	if ($('#DisplayZeroQty').is(':checked')) {
		selectNone = false;
	}
	if (selectNone == false) {
		$('#button span.filter').addClass('active');
	}
	else {
		$('#button span.filter').removeClass('active');
	}	
}

function showSubCategory(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success) {
		$('#LiSubCategory').css('display','');
		$('#LiSubCategory').html(ajaxReturn.subCategorySelection);
		$('.selectpicker').selectpicker('refresh');
	}
}

function showFloorByBuilding(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success) {
		$('#LiRoom').css('display','none');
		$('#LiRoom').html('');
		$('#LiFloor').css('display','');
		$('#LiFloor').html(ajaxReturn.floorSelection);
		$('.selectpicker').selectpicker('refresh');
	}
}

function showRoomByFloor(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success) {
		$('#LiRoom').css('display','');
		$('#LiRoom').html(ajaxReturn.roomSelection);
		$('.selectpicker').selectpicker('refresh');
	}
}

function showFilterResult(ajaxReturn, _append)
{
	if (ajaxReturn != null && ajaxReturn.success) {
		if (_append == true) {
			if ($.trim(ajaxReturn.layout) == '~~~NoRecord~~~') {
				isGetAllRecord = true;
			}
			else {
				$('#inventoryList').append(ajaxReturn.layout);
			}
		}
		else {
			$('#inventoryList').html(ajaxReturn.layout);
		}
		$('#filter').removeClass('in');
	}
}

function setHeight(){
	var filterHeight = document.getElementById('header').offsetHeight;
	var mainHeight = window.innerHeight - filterHeight;
	document.getElementById('content').style.height = mainHeight+'px';
}

function checkForChanges(){
	setHeight();
    if (element.css('height') != lastHeight) {
        lastHeight = element.css('height'); 
        setHeight();
    }

    setTimeout(checkForChanges, 500);
}

<?php echo $linventory->getJsAjaxError();?>

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body onLoad="checkForChanges()">
<form id="form1" name="form1" method="post">
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top inventory">
        <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.index";?>"><i class="fas fa-arrow-left"></i></a></div></div>
        <div class="headerTitle withFunction withIcon"><?php echo $Lang['eInventory']['eClassApp']['ItemList'];?></div>
        <div id="button">
            <div class="headerIcon"><a class="filter" role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter"> <span class="filter"></span> </a></div>
        </div>
        <div class="collapse" id="filter">
            <ul class="fillerList">
                <li>
                    <?php echo $itemTypeSelection;?>
                </li>
                <li>
                	<?php echo $categorySelection;?>
                </li>
                <li id="LiSubCategory" style="display:none;">
                	
                </li>
                <li>
                	<?php echo $buildingSelection ?>
                </li>
                <li id="LiFloor" style="display:none;">
                	
                </li>
                <li id="LiRoom" style="display:none;">
                	
                </li>
                <li>
                	<?php echo $tagSelection;?>
                </li>
                <li>
                	<?php echo $adminGroupSelection;?>
                </li>
                <li>
                	<?php echo $fundingSourceSelection;?>
                </li>
            </ul>
            <div class="form-check form-check-inline">
				<label class="form-check-label">
					<input class="form-check-input" type="checkbox" name="DisplayZeroQty" id="DisplayZeroQty" value="1">
					<div class="text"><?php echo $Lang['eInventory']['DisplayZeroRecord'];?></div>
				</label>
			</div>
			<div class="submitBtn">
				<a class="btn btn-primary" role="button" id="FilterBtn"><?php echo $Lang['eInventory']['eClassApp']['Filter'];?></a>
			</div>
        </div>
    </nav>
    
    <div id="content" style="overflow-y: auto;">
		<ul class="inventoryList" id="inventoryList">
<!-- 			<div id="InventoryList"> -->
				<?php echo $linventory->getItemListUI($offset, $numberOfRecord, $showNoRecord=1, $filterAry);?>
<!-- 			</div> -->
		</ul>
	</div>
		
</div>
</form>
</body>
</html>

