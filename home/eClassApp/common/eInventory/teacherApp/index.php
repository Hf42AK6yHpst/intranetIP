<?php
/*
 *  Using:
 *
 *  Purpose: teacher app eInventory login portal
 *
 *  2020-03-09 Cameron
 *      - handle character encoding problem in ej
 *      
 *  2019-12-16 Cameron
 *      - create this file
 */

if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$today = date('Y-m-d');
$stockTakeStart = $linventory->getStocktakePeriodStart();
$stockTakeEnd = $linventory->getStocktakePeriodEnd();
$stockTakePeriod =  $stockTakeStart. ' ' . $Lang['General']['To'] . ' ' . $stockTakeEnd;

if (($stockTakeStart>$today) || ($stockTakeEnd < $today)) {
    header('location: ?task=teacherApp.item.index');         // go to item list directly if out of stock take period
}

if ($intranet_version == "2.0" || isset($junior_mck)) {
    $characterset = 'big5-hkscs';
}
else {
    $characterset = 'utf-8';
}

?>
<!doctype html>
<html>
<head>
    <meta charset="<?php echo $characterset;?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $Lang['Header']['Menu']['eInventory'];?></title>

    <script type="text/javascript" src="../web/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../web/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../web/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../web/css/eInventoryModules.css">
</head>

<body>
<div class="module_landing col-xs-12">
    <div class="image">
        <img src="../web/images/einventory/eInventory.png" alt="eInventory" />
    </div>
    <div class="menu">
        <ul>
            <a href="?task=teacherApp.item.index&clearCoo=1">
                <li class="inventory">
                    <span><?php echo $Lang['eInventory']['eClassApp']['ItemList'];?></span>
                </li>
            </a>
            <a href="?task=teacherApp.stocktake.stocktake_progress&clearCoo=1">
                <li class="stocktake alertMsg">
                    <span><?php echo $Lang['eInventory']['eClassApp']['StockTake']?></span>
                    <p class="alertMsg"><?php echo $stockTakePeriod;?></p>
                </li>
            </a>
        </ul>
    </div>
</div>
</body>
</html>