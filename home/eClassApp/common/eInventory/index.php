<?php
// using :
/*
 *  2020-03-10 Cameron
 *      - add argument $init_setting = true to ej new inventory_app_ui instance 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");

// App init
if (isset($parLang)) {
    $leClassApp_init = new libeClassApp_init();
    $intranet_session_language = $leClassApp_init->getPageLang($parLang);
    session_register_intranet("intranet_session_language", $intranet_session_language);
} else {
    $intranet_session_language = $_SESSION['intranet_session_language'];
}
if ($intranet_session_language == '') {
    $intranet_session_language = 'en';
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinventory_app_ui.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

if ($junior_mck) {
    $indexVar['linventory'] = new libinventory_app_ui(true);        // initialize setting
}
else {
    $indexVar['linventory'] = new libinventory_app_ui();
}
$indexVar['leClassApp'] = new libeClassApp();
$indexVar['location_ui'] = new liblocation_ui();

if (!$_SESSION['UserType']) {
    $user = new libuser($_SESSION['UserID']);
    $isTeacherStaff = $user->isTeacherStaff();

    if ($isTeacherStaff) {
        $userType = USERTYPE_STAFF;
        $_SESSION['UserType'] = $userType;
    }
    else {
        $userType = '';
    }
}
else {
    $userType = $_SESSION['UserType'];
}

if ($_SESSION['UserType'] != USERTYPE_STAFF) {
    No_Access_Right_Pop_Up();
    exit;
}

$task = '';
if ($_POST['task'] != '') {
    $task = $_POST['task'];
}
else if ($_GET['task'] != '') {
    $task = $_GET['task'];
}

if ($task == '') {
    switch ($userType) {
        case USERTYPE_STAFF:
            $task = 'teacherApp.index';
            break;
    }
}
// Prevent searching parent directories, like 'task=../../index'.
$task = str_replace("../", "", $task);
$task = str_replace('.', '/', $task);

$indexVar['linventory']->checkAppAccessRight();

### Include corresponding php files
$indexVar['taskScript'] = '';
$indexVar['taskScript'] .= $task.'.php';

if (file_exists($indexVar['taskScript'])){
    include_once($indexVar['taskScript']);
}
else {
    $indexVar['taskScript'] = $task.'/index.php';
    if (file_exists($indexVar['taskScript'])){
        include_once($indexVar['taskScript']);
    }else{
        No_Access_Right_Pop_Up();
    }
}

intranet_closedb();
?>