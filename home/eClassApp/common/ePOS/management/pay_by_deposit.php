<?php
/*
 *  Using:
 *
 *  Purpose: show pay by deposit page
 *
 *  2019-07-23 Cameron
 *      - create this file
 */

if (!$indexVar) {
    header('location: ../');
}

$libpos = $indexVar['libpos'];
$linterface = $indexVar['linterface'];

if (empty($intranet_root)) {
    echo $linterface->getErrorLayout($Lang['ePOS']['eClassApp']['error']['IncorrectPath'], $getBack=false);
    return;
}

if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
    $bodyClass="studentApp";
}
elseif ($_SESSION['UserType'] == USERTYPE_STAFF) {
    $bodyClass="teacherApp";
}
else {
    $bodyClass="";  // default parentApp
}

include($intranet_root."/home/eClassApp/common/ePOS/header.php");
?>
<script type="text/javascript">
    function show_ajax_error()
    {
        alert('<?php echo $Lang['General']['AjaxError'];?>');
    }

    jQuery(document).ready(function(){
        $(document).on('click', '.page-back-btn', function(e) {
            window.location.href = "?";
        });

        $(document).on('keyup', '#tbxTopSearch', function(e) {
            var keyword = $.trim($(this).val());
            if(e.keyCode == 13 && keyword.length>0)
            {
                window.location.href = "?task=&keyword=" + encodeURIComponent(keyword);
            }
        });

        $(document).on('click', 'div#btnPay', function(e) {
            window.location.href = "?task=management.pay_by_deposit_update&PaymentMethod="+$("#PaymentMethod").val()+"&Remark="+encodeURIComponent($("#Remark").val());
            $(this).attr('disabled', 'disabled');
        });

        $(document).on('click', '#orderDetalCloseBtn', function(e) {
            e.preventDefault();
            $('#orderDetails').hide();
            $("#payBeforeLayout").show();
        });

    });

    function showOrderDetails(ajaxReturn, obj)
    {
        if (ajaxReturn != null && ajaxReturn.success){
            $this = obj;
            var id = $this.parent().parent().attr('id');
            if (id != 'undefined' && id.substr(0,3) == 'blk') {
                var len = id.length;
                id = 'btn' + id.substr(3,len-3);
            }
            $('#' + id).click();    // toggle (hide) blkPending or blkHistory
            $('div#payBeforeLayout').fadeOut(250);
            $('div#orderDetails').html(ajaxReturn.orderDetails).fadeIn(250);
        }
    }

</script>

</head>

<body class="<?php echo $bodyClass;?>">
<div class="mainBody">

    <div id="payBeforeLayout">
        <?php echo $linterface->getBeforePayByDeposit($_SESSION['ePosStudentID']);?>
    </div>

    <div id="orderDetails" style="display: none;">

    </div>

</div>

<?php
    echo $linterface->getPowerByFooter();
    echo $linterface->getTopBarMenu($className='', $_SESSION['ePosStudentID'], $enableSelectStudent=false);
?>
</body>
</html>
