<?php
/*
 *  Using: 
 *
 *  Purpose: build submit page for Alipay
 *
 *  2020-08-03 Cameron
 *      - revised pay by Alipay method, pass parameter to app
 *
 *  2019-08-27 Cameron
 *      - create this file
 */

if (!$indexVar || !$intranet_root) {
    header('location: ../');
}

//include_once($intranet_root."/includes/libalipay.php");
include_once($intranet_root."/home/eClassApp/common/ePOS/epos_config.php");

$libpos = $indexVar['libpos'];
$linterface = $indexVar['linterface'];

if (empty($intranet_root)) {
    return;
}

if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
    $bodyClass="studentApp";
}
elseif ($_SESSION['UserType'] == USERTYPE_STAFF) {
    $bodyClass="teacherApp";
}
else {
    $bodyClass="";  // default parentApp
}

$error = array();
$displayError = '';
$transactionDetailAry = array();
$totalAmount = 0;
if (!$_SESSION['ePosStudentID']) {
    $error[]['ItemName'] = 'MissingUser';
    $displayError .= $Lang['ePOS']['eClassApp']['error']['UserNotFound'];
}
else {
    $pendingCart = $libpos->getPendingCart($_SESSION['ePosStudentID']);

    for ($i = 0, $iMax = count($pendingCart); $i < $iMax; $i++) {
        $_pendingCart = $pendingCart[$i];
        $_itemID = $_pendingCart['ItemID'];
        $_itemName = $_pendingCart['ItemName'];
        $_unitPrice = $_pendingCart['UnitPrice'];
        $_purchaseQty = $_pendingCart['PurchaseQty'];
        $_remainQty = $_pendingCart['RemainQty'];
        $_sequence = $_pendingCart['Sequence'];
        if ($_purchaseQty > $_remainQty) {
            $error[$_itemID] = array('ItemName' => $_itemName, 'PurchaseQty' => $_purchaseQty, 'RemainQty' => $_remainQty, 'Error' => -4);      // -4: OutOfStock
        }
        $transactionDetailAry[$i]['ItemID'] = $_itemID;
        $transactionDetailAry[$i]['Quantity'] = $_purchaseQty;
        $transactionDetailAry[$i]['UnitPrice'] = $_unitPrice;
        $transactionDetailAry[$i]['Sequence'] = $_sequence;
        $totalAmount += $_unitPrice * $_purchaseQty;
    }
    
    if (count($error)) {
        $displayError .= $Lang['ePOS']['eClassApp']['OutOfStockItem']."<br>";
        $i = 0;
        foreach((array) $error as $_error) {
            if ($i>0) {
                $displayError .= "<br>";
            }
            $displayError .= $_error['ItemName'];
            $i++;
        }
    }
}

if (count($error) == 0) {
    $transactionInfo = $libpos->addAlipayTransaction($totalAmount, $transactionDetailAry, $_SESSION['ePosStudentID']);

    if ($transactionInfo['OutTradeNo']) {
//        $alipay_config = ($sys_custom['ePOS']['env'] == 'dev') ? $alipay_dev_config : $alipay_prod_config;
        
//        $libalipay = new libalipay($alipay_config);
        
//        $parameter = array(
//            "service" => $alipay_config['service'],
//            "partner" => $alipay_config['partner'],
//            "notify_url" => $alipay_config['notify_url'],
//            "return_url" => $alipay_config['return_url'],
//
//            "payment_inst" => "ALIPAYHK",
//            "out_trade_no" => $transactionInfo['OutTradeNo'],
//            "subject" => $transactionInfo['OutTradeNo'],
//            "total_fee" => $totalAmount,
//            "body" => "",
//            "split_fund_info" => "",
//            "currency" => $alipay_config['currency'],
//            "product_code" => "NEW_WAP_OVERSEAS_SELLER",
//            "_input_charset" => trim(strtolower($alipay_config['input_charset']))
//        );
    }
    else {
        $displayError .= $Lang['ePOS']['eClassApp']['error']['NoOutTradeNo'];
    }
}

if ($transactionInfo['OutTradeNo']) {
//    header("location: ../../../../api/epos_pay_by_alipay.php?paymentID=".$transactionInfo['TempTransactionID']."&amount=".$totalAmount."&outTradeNo=".urlencode($transactionInfo['OutTradeNo']));
    header("location: ../../../../api/epos_pay_by_alipay.php?paymentID=".$transactionInfo['TempTransactionID']."&amount=".$totalAmount."&moduleName=ePOS&title=ePOSbyAlipay");
    exit;
}

include($intranet_root."/home/eClassApp/common/ePOS/header.php");
?>
<script type="text/javascript">
    function show_ajax_error()
    {
        alert('<?php echo $Lang['General']['AjaxError'];?>');
    }

    jQuery(document).ready(function(){
        $('#applForm').submit();
//        $(window.frames[0]).remove();
    });

</script>

</head>

<body class="<?php echo $bodyClass;?>">
<div class="mainBody">

    <div id="payBeforeLayout">
    <?php
        if ($transactionInfo['OutTradeNo']) {
//            echo $libalipay->buildRequestForm($parameter, "applForm", "applForm", "post", "payFrame");
//            echo $libalipay->buildRequestForm($parameter, "applForm", "applForm", "post", "");    -- disable on 2020-08-03, move to be handled by central server
        }
        else {
            echo $linterface->getErrorLayout($displayError);
        }
    ?>
    </div>

<?php if ($transactionInfo['OutTradeNo']): ?>
    <div id="div_processing">
        <table align="Center">
            <tr>
                <td align="center">
                    <h1 class="warning"><?php echo $Lang['ePOS']['TransactionStatus']['DoNotCloseWindow'];?></h1>
                    <br/>
                    <img id="spinner" src="<?php echo $PATH_WRT_ROOT;?>/images/<?php echo $LAYOUT_SKIN;?>/loadingAnimation.gif" width="208px" height="13px"/>
                </td>
            </tr>
        </table>
    </div>
<?php endif; ?>


</div>

<!-- 
<iframe id="payFrame" name="payFrame" src="" width="100%" height="100%" style="border:none;min-width:800px;min-height:600px;"></iframe>
 -->
<?php
echo $linterface->getPowerByFooter();
echo $linterface->getTopBarMenuWithoutFunction();
?>
</body>
</html>
