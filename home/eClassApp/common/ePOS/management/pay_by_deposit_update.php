<?php
/*
 *  Using:
 *
 *  Purpose: save pay by deposit record and remove pending cart
 *
 *  2019-11-12 Ray
 *      - Add PaymentMethod & Remark
 *
 *  2019-07-24 Cameron
 *      - create this file
 */

if (!$indexVar) {
    header('location: ../');
}

$libpos = $indexVar['libpos'];
$linterface = $indexVar['linterface'];

if (empty($intranet_root)) {
    echo $linterface->getErrorLayout($Lang['ePOS']['eClassApp']['error']['IncorrectPath'], $getBack=false);
    return;
}

$error = array();
$transactionDetailAry = array();
$totalAmount = 0;
$pendingCart = $libpos->getPendingCart($_SESSION['ePosStudentID']);

for($i=0,$iMax=count($pendingCart);$i<$iMax;$i++) {
    $_pendingCart = $pendingCart[$i];
    $_itemID = $_pendingCart['ItemID'];
    $_itemName = $_pendingCart['ItemName'];
    $_unitPrice = $_pendingCart['UnitPrice'];
    $_purchaseQty = $_pendingCart['PurchaseQty'];
    $_remainQty = $_pendingCart['RemainQty'];
    if ($_purchaseQty > $_remainQty) {
        $error[$_itemID] = array('ItemName' => $_itemName, 'PurchaseQty' => $_purchaseQty, 'RemainQty' => $_remainQty, 'Error' => -4);      // -4: OutOfStock
    }
    $transactionDetailAry[$i]['ItemID'] = $_itemID;
    $transactionDetailAry[$i]['Quantity'] = $_purchaseQty;
    $transactionDetailAry[$i]['UnitPrice'] = $_unitPrice;
    if($sys_custom['ePOS']['PaymentMethod']) {
		$transactionDetailAry[$i]['PaymentMethod'] = intval($PaymentMethod);
		$transactionDetailAry[$i]['Remark'] = $Remark;
	}
    $totalAmount += $_unitPrice * $_purchaseQty;
}

//$error[3791] = array('ItemName' => 'Manual Cookies', 'PurchaseQty' => 2, 'RemainQty' => 8, 'Error' => -4);
//$error[3792] = array('ItemName' => 'Star Cookies', 'PurchaseQty' => 2, 'RemainQty' => 8, 'Error' => -4);

$displayError = '';
if (count($error)) {
    $displayError .= $Lang['ePOS']['eClassApp']['OutOfStockItem']."<br>";
    $i = 0;
    foreach((array) $error as $_error) {
        if ($i>0) {
            $displayError .= "<br>";
        }
        $displayError .= $_error['ItemName'];
        $i++;
    }
}
else {
    $cardID = '';
    $siteName = '';
    $InvoiceMethod = 0;
    $returnTransactionLogID = true;
    $transactionLogID = $libpos->Process_POS_TRANSACTION($cardID, $totalAmount, $siteName, $transactionDetailAry, $InvoiceMethod, $_SESSION['ePosStudentID'], $returnTransactionLogID);

    if ($transactionLogID > 0) {
        $transactionDetail = $libpos->Get_POS_Transaction_Detail($transactionLogID);
        if (count($transactionDetail)) {
            $transactionDetail = $transactionDetail[0];
            $_transactionDetail = array();
            $_transactionDetail['TransactionLogID'] = $transactionLogID;
            $_transactionDetail['OrderNumber'] = $transactionDetail['REFCode'];
            $payFinishLayout = $linterface->getPayFinishLayout($_transactionDetail);
        }
        else {
            $displayError .= $Lang['ePOS']['eClassApp']['error']['LostTransaction'];
        }

        $autoCommitResult = $libpos->setAutocommit();   // must reset autocommit to 1, otherwise cannot commit the sql execution

        // remove pending cart
        $removeResult = $libpos->deletePendingCart($_SESSION['ePosStudentID']);
    }
    else {
        switch ($transactionLogID) {
            case 0:     // Not enough balance
                $displayError .= $Lang['ePOS']['eClassApp']['error']['Deflict'];
                break;
            case -1:    // user not found
                $displayError .= $Lang['ePOS']['eClassApp']['error']['UserNotFound'];
                break;
            case -4:    // Not enough Inventory
                $displayError .= $Lang['ePOS']['eClassApp']['error']['OutOfStock'];
                break;
            case -5:    // sql error
                $displayError .= $Lang['ePOS']['eClassApp']['error']['DatabaseError'];
                break;
            case -6:    // Not item detail
                $displayError .= $Lang['ePOS']['eClassApp']['error']['NoItemDetail'];
                break;
            default:    // unknown error
                $displayError .= $Lang['ePOS']['eClassApp']['error']['UnknownError'];
                break;
        }
    }
}

if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
    $bodyClass="studentApp";
}
elseif ($_SESSION['UserType'] == USERTYPE_STAFF) {
    $bodyClass="teacherApp";
}
else {
    $bodyClass="";  // default parentApp
}

include($PATH_WRT_ROOT."home/eClassApp/common/ePOS/header.php");
?>
<script type="text/javascript">
    function show_ajax_error()
    {
        alert('<?php echo $Lang['General']['AjaxError'];?>');
    }

    jQuery(document).ready(function(){
        $(document).on('click', 'div#btnHome', function(e) {
            window.location.href = "?";
        });

        $(document).on('click', 'div#btnOrder', function(e) {
//            window.location.href = "?task=order_details&TransactionLogID=" + $(this).attr('data-TransactionLogID');
            $.ajax({
                dataType: "json",
                type: "POST",
                url: '?task=management.ajax',
                data : {
                    'action': 'getOrderDetails',
                    'TransactionLogID': $(this).attr('data-TransactionLogID')
                },
                success: function(ajaxReturn) {
                    showOrderDetails(ajaxReturn, null);
                },
                error: show_ajax_error
            });
        });

        $(document).on('click', '#orderDetalCloseBtn', function(e) {
            e.preventDefault();
            $("#orderDetails").css('display','none');
            $("#paymentResult").fadeIn(300);
        });

        $(document).on('keyup', '#tbxTopSearch', function(e) {
            var keyword = $.trim($(this).val());
            if(e.keyCode == 13 && keyword.length>0)
            {
                window.location.href = "?task=&keyword=" + encodeURIComponent(keyword);
            }
        });
    });

    function showOrderDetails(ajaxReturn, obj)
    {
        if (ajaxReturn != null && ajaxReturn.success){
            if (obj != null) {
                $this = obj;
                var id = $this.parent().parent().attr('id');
                if (id != 'undefined' && id.substr(0, 3) == 'blk') {
                    var len = id.length;
                    id = 'btn' + id.substr(3, len - 3);
                }
                $('#' + id).click();    // toggle (hide) blkPending or blkHistory
            }
            $('div#paymentResult').fadeOut(250);
            $('div#orderDetails').html(ajaxReturn.orderDetails).fadeIn(250);
        }
    }
</script>

</head>

<body class="<?php echo $bodyClass;?>">
<div class="mainBody">
    <div id="paymentResult">
<?php
    if (empty($displayError)) {
        echo $payFinishLayout;
    }
    else {
        echo $linterface->getErrorLayout($displayError);
    }
?>
    </div>

    <div id="orderDetails" style="display: none;">

    </div>
</div>

<?php
    echo $linterface->getPowerByFooter();
    echo $linterface->getTopBarMenu($className='', $_SESSION['ePosStudentID'], $enableSelectStudent=false);
?>
</body>
</html>
