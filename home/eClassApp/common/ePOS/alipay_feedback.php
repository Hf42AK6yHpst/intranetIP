<?php

// Editing by :
/*
 *      
 *  2019-08-27 (Cameron)
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libalipay.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."home/eClassApp/common/ePOS/epos_config.php");

intranet_opendb();

$json = new JSON_obj();

$datafeed_data = $_GET;     // here must use GET method, don't use POST as number of parameters are different

$json_string = $json->encode($datafeed_data);
//$base64_string = base64_encode($json_string);
$log_path = $file_path . '/file/epos_alipay_datafeed_log';
$debug_file = $file_path . '/file/epos_alipay_datafeed_debug';

shell_exec('echo \'' . date("Y-m-d H:i:s") . ': ' . OsCommandSafe($json_string) . '\' >> \'' . $log_path . '\'');

if ($sys_custom['ePOS']['Alipay']['Debug']) {
    error_log("\n\n datafeed_data-->".print_r($datafeed_data,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, $debug_file);
}

$alipay_config = ($sys_custom['ePOS']['env'] == 'dev') ? $alipay_dev_config : $alipay_prod_config;
$libalipay = new libalipay($alipay_config);
$verify_result = $libalipay->verifyNotify($datafeed_data);
$transactionID = '';

if ($sys_custom['ePOS']['Alipay']['Debug']) {
    error_log("\n\n verify_result -->".print_r($verify_result,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, $debug_file);
}

if ($verify_result) {

    //out_trade_no
    $out_trade_no = $datafeed_data['out_trade_no'];

    //trade_no
    $trade_no = $datafeed_data['trade_no'];

    //trade_status
    $trade_status = $datafeed_data['trade_status'];

    $libpos = new libpos();

    if ($datafeed_data['trade_status'] == 'TRADE_FINISHED' || $datafeed_data['trade_status'] == 'TRADE_SUCCESS') {
        $status = 'success';
        $dbStatus = 1;
    } else {
        $status = 'failed';
        $dbStatus = -1;
    }
    $libpos->updateAlipayTransaction($out_trade_no, $dbStatus, $trade_no);

    if ($sys_custom['ePOS']['Alipay']['Debug']) {
        error_log("\n\n status-->".print_r($status,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, $debug_file);
    }
    
    if ($status == 'success') {
        $alipayTransaction = $libpos->getAlipayTransaction($out_trade_no);

        if ($sys_custom['ePOS']['Alipay']['Debug']) {
            error_log("\n\n alipayTransaction-->".print_r($alipayTransaction,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, $debug_file);
        }
        
        if (count($alipayTransaction)) {
            $tempTransactionID = $alipayTransaction['TempTransactionID'];
            $paymentTimeVal = $alipayTransaction['StatusUpdateOn'];
            $targetUserID = $alipayTransaction['StudentID'];
            $amount = $alipayTransaction['Amount'];

            $alipayTransactionItems = $libpos->getAlipayTransactionItems($tempTransactionID);
            
            if ($sys_custom['ePOS']['Alipay']['Debug']) {
                error_log("\n\n alipayTransactionItems-->".print_r($alipayTransactionItems,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, $debug_file);
            }
            
            $transactionDetailAry = array();
            for($i=0,$iMax=count($alipayTransactionItems);$i<$iMax;$i++) {
                $transactionDetailAry[$i]['ItemID'] = $alipayTransactionItems[$i]['ItemID'];
                $transactionDetailAry[$i]['Quantity'] = $alipayTransactionItems[$i]['PurchaseQty'];
                $transactionDetailAry[$i]['UnitPrice'] = $alipayTransactionItems[$i]['UnitPrice'];
            }

            $cardID = '';
            $siteName = $Lang['ePOS']['PaidByAlipay'];
            $InvoiceMethod = 0;
            $returnTransactionLogID = true;
            $isAlipay = true;
            $transactionLogID = $libpos->Process_POS_TRANSACTION($cardID, $amount, $siteName, $transactionDetailAry, $InvoiceMethod, $targetUserID, $returnTransactionLogID, $paymentTimeVal, $isAlipay);

            if ($sys_custom['ePOS']['Alipay']['Debug']) {
                error_log("\n\n transactionLogID-->".print_r($transactionLogID,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, $debug_file);
            }
            
            $autoCommitResult = $libpos->setAutocommit();   // must reset autocommit to 1, otherwise cannot commit the sql execution

            // remove pending cart
            $removeResult = $libpos->deletePendingCart($targetUserID);

            if ($sys_custom['ePOS']['Alipay']['Debug']) {
                error_log("\n\n removeResult -->".print_r($removeResult,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, $debug_file);
            }
        }
    }

    header("location: index.php?task=management.payment_complete&status=$dbStatus&TransactionLogID=$transactionLogID");
    
} else {
    $status = 'failed';
    $dbStatus = -1;
}


intranet_closedb();

?>
