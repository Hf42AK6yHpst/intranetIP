<?php
/*
 *  Using:
 *
 *  Purpose: student ePOS login portal
 *
 *  2020-08-20 Cameron
 *      - change to use $lpayment->isEWalletDirectPayEnabled && $sys_custom['ePayment']['MultiPaymentGateway'] to determine the payment method: direct pay or topup
 *
 *  2019-10-21 Cameron
 *      - add flag $sys_custom['ePOS']['force_paid_by_balance_only'] so that a site can force to use paid by balance method
 *
 *  2019-08-01 Cameron
 *      - create this file
 */

if (!$indexVar) {
    header('location: ../');
}

$libpos = $indexVar['libpos'];
$linterface = $indexVar['linterface'];
$lpayment = $indexVar['lpayment'];

$_SESSION['ePosStudentID'] = $_SESSION['UserID'];
if (!$_SESSION['ePosStudentID']) {
    No_Access_Right_Pop_Up();
    exit;
}

$pendingCart = $libpos->getPendingCart($_SESSION['ePosStudentID']);

// return from to search
if ($_GET['keyword']) {
    $keyword = rawurldecode($_GET['keyword']);
    $itemInCart = Get_Array_By_Key($pendingCart,'ItemID');
    $searchResult = $linterface->getSearchResult($keyword,$itemInCart);
    $searchResultCss = '';
}
else {
    $searchResult = '';
    $searchResultCss = 'none';
}


include($PATH_WRT_ROOT."home/eClassApp/common/ePOS/header.php");
?>
<script type="text/javascript" src="../web/js/eposPortal.js"></script>
<script type="text/javascript">
    function show_ajax_error()
    {
        alert('<?php echo $Lang['General']['AjaxError'];?>');
    }

    jQuery(document).ready(function(){
        $(document).on('click', 'div#btnCheckout', function() {
            if ($('#StudentID').val() == '') {
                window.alert('<?php echo $Lang['ePOS']['eClassApp']['error']['PleaseWaitForStudentLoad'];?>');
            }
            else {
                <?php if ($lpayment->isEWalletDirectPayEnabled() && $sys_custom['ePayment']['MultiPaymentGateway'] && !$sys_custom['ePOS']['force_paid_by_balance_only']): ?>
                window.location.href = "?task=management.pay_by_alipay";
                <?php else: ?>
                window.location.href = "?task=management.pay_by_deposit";
                <?php endif;?>
            }
        });

        <?php if ($_GET['keyword']):?>
        $("#pMain").hide();
        <?php endif;?>

    });

</script>

</head>

<body class="studentApp">
<div id="cHome" class="mainBody opacity0 transition">

    <article id="pMain">

        <div id="blkTabs">
            <nav id="tabScrollable" class="tabScrollable dragscroll">
                <div id="tabScrollableContents" class="tabScrollableContents">
                    <?php echo $linterface->getAllCategories();?>
                </div>
            </nav>
            <button id="tabScrollableLeftBtn" class="tabScrollable-button tabScrollable-button-left" type="button">
                <svg class="tabScrollable-button-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"/></svg>
            </button>
            <button id="tabScrollableRightBtn" class="tabScrollable-button tabScrollable-button-right" type="button">
                <svg class="tabScrollable-button-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"/></svg>
            </button>
        </div>

        <div id="blkPage" class="product-item-container">

        </div>
    </article><!--
    don't leave space including line feed here to suit the alignment
--><article id="pSearch" style="display: <?php echo $searchResultCss;?>">
        <?php echo $searchResult;?>
    </article><!--
    don't leave space including line feed here to suit the alignment
--><aside id="blkTray">
        <?php echo $linterface->getShoppingCart($pendingCart);?>
    </aside>
    <div id="orderDetails" style="display: none;">

    </div>

</div>

<?php
echo $linterface->getPowerByFooter();
echo $linterface->getTopBarMenu();
?>

<div class="dialog-container" style="display:none;">
    <?php
    echo $linterface->getConfirmRemoveItemDialog();
    echo $linterface->getConfirmCancelOrderDialog();
    echo $linterface->getZoomInItemPhotoDialog();
    ?>

</div>

</body>
</html>
