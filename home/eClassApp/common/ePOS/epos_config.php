<?php
$alipay_dev_config = array(
    "gateway"=>"https://openapi.alipaydev.com/gateway.do?", // sandbox test api gateway, need to change to above when turn to production
    "https_verify_url"=>"https://openapi.alipaydev.com/gateway.do?service=notify_verify&", // dev api
    "http_verify_url"=>"http://notify.alipay.com/trade/notify_query.do?",
    "partner"=>$sys_custom['ePOS']['Alipay']['DevMerchantID'], // Partner ID
    "key"=>$sys_custom['ePOS']['Alipay']['DevMD5Key'], // MD5 key
    "notify_url"=>$sys_custom['ePOS']['server_public_ip']."home/eClassApp/common/ePOS/alipay_feedback.php", // asynchronous notification
    "return_url"=>$sys_custom['ePOS']['server_public_ip']."home/eClassApp/common/ePOS/alipay_feedback.php", // synchronous notification
    "sign_type"=>"MD5",
    "input_charset"=>"utf-8",
    "cacert"=>"", // The path of ca certificate,used to check ssl of curl in verify_notify
    "transport"=>"https", // https or http
    "service"=>"create_forex_trade_wap",
    "currency" => "HKD"
);

$alipay_prod_config = array(
    "gateway"=>"https://mapi.alipay.com/gateway.do?", // production api
    "https_verify_url"=>"https://mapi.alipay.com/gateway.do?service=notify_verify&", // production api
    "http_verify_url"=>"http://notify.alipay.com/trade/notify_query.do?",
    
    "partner"=>$sys_custom['ePOS']['Alipay']['MerchantID'], // Partner ID
    "key"=>$sys_custom['ePOS']['Alipay']['MD5Key'], // MD5 key
    "notify_url"=>$sys_custom['ePOS']['server_public_ip']."home/eClassApp/common/ePOS/alipay_feedback.php", // asynchronous notification
    "return_url"=>$sys_custom['ePOS']['server_public_ip']."home/eClassApp/common/ePOS/alipay_feedback.php", // synchronous notification
    "sign_type"=>"MD5",
    "input_charset"=>"utf-8",
    "cacert"=>"", // The path of ca certificate,used to check ssl of curl in verify_notify
    "transport"=>"https", // https or http
    "service"=>"create_forex_trade_wap",
    "currency" => "HKD"
);

?>