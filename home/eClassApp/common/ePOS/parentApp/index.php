<?php
/*
 *  Using: 
 *  
 *  Purpose: parent ePOS login portal
 *
 *  2020-08-20 Cameron
 *      - change to use $lpayment->isEWalletDirectPayEnabled && $sys_custom['ePayment']['MultiPaymentGateway'] to determine the payment method: direct pay or topup
 *
 *  2020-06-08 Cameron
 *      -  show jsError and redirect to home if child is not related to the parent or child is not found
 *
 *  2020-04-02 Cameron
 *      - redirect to home page if $_SESSION['ePosStudentID'] is empty
 *      
 *  2020-02-11 Cameron
 *      - fix to avoid direct access this page by url 
 *      
 *  2019-10-21 Cameron
 *      - add flag $sys_custom['ePOS']['force_paid_by_balance_only'] so that a site can force to use paid by balance method
 *
 *  2019-07-11 Cameron
 *      - create this file
 */

if (!$indexVar) {
    header('location: ../');
}

$libpos = $indexVar['libpos'];
$linterface = $indexVar['linterface'];
$lpayment = $indexVar['lpayment'];

if ($_GET['studentId']) {
    $isChild = $libpos->isChild($_SESSION['UserID'], $_GET['studentId']);
    if ($isChild) {
        $_SESSION['ePosStudentID'] = $_GET['studentId'];            // get from api
        $_SESSION['fromApp'] = true;
    }
    else {
        $jsError = $linterface->invalidChildForTheParent($redirect='');
        echo $jsError;
        exit();
    }
}

$enableSelectStudent = false;
$children = array();
if (!$_SESSION['fromApp']) {
    $children = $libpos->getChildren($_SESSION['UserID']);
    $nrChildren = count($children);
    if ($nrChildren == 1) {
        $_SESSION['ePosStudentID'] = $children[0]['UserID'];
    }
    else if ($nrChildren > 1) {
        // pass $children to getTopBarMenu for selection
        $enableSelectStudent = true;
        $_SESSION['ePosStudentID'] = '';
    }
    else {
        $jsError = $linterface->invalidChildForTheParent();
        echo $jsError;
        exit();
    }
}

$pendingCart = $libpos->getPendingCart($_SESSION['ePosStudentID']);

// return from to search
if ($_GET['keyword']) {
    $keyword = rawurldecode($_GET['keyword']);
    $itemInCart = Get_Array_By_Key($pendingCart,'ItemID');
    $searchResult = $linterface->getSearchResult($keyword,$itemInCart);
    $searchResultCss = '';
}
else {
    $searchResult = '';
    $searchResultCss = 'none';
}


include($PATH_WRT_ROOT."home/eClassApp/common/ePOS/header.php");
?>
<script type="text/javascript" src="../web/js/eposPortal.js"></script>
<script type="text/javascript">
    var isLoading = true;
    var loadingImg = '<?php echo $linterface->Get_Ajax_Loading_Image(); ?>';

function show_ajax_error()
{
    alert('<?php echo $Lang['General']['AjaxError'];?>');
}

jQuery(document).ready(function(){
    $(document).on('click', 'div#btnCheckout', function() {
    if ($('#StudentID').val() == '') {
        window.alert('<?php echo $Lang['ePOS']['eClassApp']['error']['PleaseWaitForStudentLoad'];?>');
    }
    else {
        <?php if ($lpayment->isEWalletDirectPayEnabled() && $sys_custom['ePayment']['MultiPaymentGateway'] && !$sys_custom['ePOS']['force_paid_by_balance_only']): ?>
        window.location.href = "?task=management.pay_by_alipay";
        <?php else: ?>
        window.location.href = "?task=management.pay_by_deposit";
        <?php endif;?>
    }
    });

    <?php if ($_GET['keyword']):?>
        $("#pMain").hide();
    <?php endif;?>

    $(document).on('change', 'select#StudentID', function(e) {

        e.preventDefault();
        if ($(this).val() != '') {
            isLoading = true;
            $('#tabScrollableContents').html(loadingImg);
            $('#blkPage').fadeOut(150);
            $('#blkTray').fadeOut(150);

            $.ajax({
                dataType: "json",
                type: "POST",
                url: '?task=management.ajax',
                data : {
                    'action': 'getPosPageByStudent',
                    'StudentID': $('#StudentID').val()
                },
                success: refreshCurrentPage,
                error: show_ajax_error
            });
        }
        else {
            emptyCart();
        }
    });

});

function refreshCurrentPage(ajaxReturn)
{
    if (ajaxReturn != null && ajaxReturn.success){
        isLoading = false;
        $('#tabScrollableContents').html(ajaxReturn.tabScrollableContents);

        // show shopping cart before prodetails for highlighting product
        $('#blkTray').html(ajaxReturn.shoppingCart);
        $('#blkTray').fadeIn(150);

        // show first category items, must show shopping cart first
        $('a.tabScrollable-link:first').click();
        $('#blkPage').fadeIn(150);

        var maxHeight = window.innerHeight - 100;
        var height = 76.6 * parseInt(ajaxReturn.pickupNumber) + 47;
        if (height > maxHeight) {
            height = maxHeight;
        }
        $('#blkHistory').css({'height':height + 'px', 'min-height':'47px'});
        $('#blkHistory').html(ajaxReturn.pickupOrder);

        height = 76.6 * parseInt(ajaxReturn.unpickupNumber) + 47;
        if (height > maxHeight) {
            height = maxHeight;
        }
        $('#blkPending').css({'height':height + 'px', 'min-height':'47px'});
        $('#blkPending').html(ajaxReturn.unpickupOrder);
        $('#lblPending').html(ajaxReturn.unpickupNumber);
    }
}

</script>

</head>

<body>
<div id="cHome" class="mainBody opacity0 transition">

    <article id="pMain">

        <div id="blkTabs">
            <nav id="tabScrollable" class="tabScrollable dragscroll">
                <div id="tabScrollableContents" class="tabScrollableContents">
                    <?php echo $linterface->getAllCategories();?>
                </div>
            </nav>
            <button id="tabScrollableLeftBtn" class="tabScrollable-button tabScrollable-button-left" type="button">
                <svg class="tabScrollable-button-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"/></svg>
            </button>
            <button id="tabScrollableRightBtn" class="tabScrollable-button tabScrollable-button-right" type="button">
                <svg class="tabScrollable-button-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"/></svg>
            </button>
        </div>

        <div id="blkPage" class="product-item-container">

        </div>
    </article><!--
    don't leave space including line feed here to suit the alignment
--><article id="pSearch" style="display: <?php echo $searchResultCss;?>">
        <?php echo $searchResult;?>
    </article><!--
    don't leave space including line feed here to suit the alignment
--><aside id="blkTray">
        <?php echo $linterface->getShoppingCart($pendingCart);?>
    </aside>
    <div id="orderDetails" style="display: none;">

    </div>

</div>

<?php
    echo $linterface->getPowerByFooter();
    echo $linterface->getTopBarMenu($className='', $studentID='', $enableSelectStudent, $children);
?>

<div class="dialog-container" style="display:none;">
<?php
    echo $linterface->getConfirmRemoveItemDialog();
    echo $linterface->getConfirmCancelOrderDialog();
    echo $linterface->getZoomInItemPhotoDialog();
?>

</div>

</body>
</html>
