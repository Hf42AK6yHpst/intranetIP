<?php
################## Change Log [Start] ##############
#
#	Date:	2015-07-27	Omas
#			if section is undefined set as ''
#	Date:	2014-12-10	Omas
#			Create this file - for load template
#
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

$libAppTemplate = new libeClassApp_template();

$Module = $_POST['Module'];
$Section = $_POST['Section'];

if($Section=='undefined'){
	$Section = '';
}

# Init libeClassApp_template
$libAppTemplate->setModule($Module);
$libAppTemplate->setSection($Section);

intranet_auth();
intranet_opendb();

# Data Ready
$TemplateIDStr = $_POST['TemplateID'];

$result = $libAppTemplate->ajaxLoadTemplate($TemplateIDStr);

intranet_closedb();

echo $result;

?>
