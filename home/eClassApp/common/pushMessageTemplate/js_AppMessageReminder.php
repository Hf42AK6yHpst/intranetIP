<script ="javascript">

var AppMessageReminder = {
	
	targetAjaxScript: '',
	targetMessageTitleID: '',
	targetMessageContentID: '',
	MsgModule: '',
	MsgSection: '',
	
	showThickbox : function(){
		$('a#pushMessageButton').click();
	},
	
	initInsertAtTextarea : function(){
		jQuery.fn.extend({
			insertAtCaret: function(myValue){
			  return this.each(function(i) {
			    if (document.selection) {
			      //For browsers like Internet Explorer
			      this.focus();
			      var sel = document.selection.createRange();
			      sel.text = myValue;
			      this.focus();
			    }
			    else if (this.selectionStart || this.selectionStart == '0') {
			      //For browsers like Firefox and Webkit based
			      var startPos = this.selectionStart;
			      var endPos = this.selectionEnd;
			      var scrollTop = this.scrollTop;
			      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
			      this.focus();
			      this.selectionStart = startPos + myValue.length;
			      this.selectionEnd = startPos + myValue.length;
			      this.scrollTop = scrollTop;
			    } else {
			      this.value += myValue;
			      this.focus();
			    }
			  });
			}
		});
	},
		
	addAutoFillTag : function(){
		var message_field_name = AppMessageReminder.targetMessageContentID? AppMessageReminder.targetMessageContentID : 'MessageContent';
		$('#'+message_field_name).focus();
		var tagValue = '($' + $('#messageTag').val() + ')';
		$('#'+message_field_name).insertAtCaret(tagValue);
	},
	
	loadMessageTemplate : function(){
		var Module = $('input#Module').val();
		var Section = $('input#Section').val();
		if(AppMessageReminder.MsgModule){
			Module = AppMessageReminder.MsgModule;
		}
		if(AppMessageReminder.MsgSection){
			Section = AppMessageReminder.MsgSection;
		}
		
		var TemplateID = $('#templateSelect').val();
		$('#AjaxStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		$('#SendAppMessage').attr('disabled', true);
		$.post(
		"<?=$PATH_WRT_ROOT?>home/eClassApp/common/pushMessageTemplate/ajax_loadTemplate.php", 
		{ 
			Module : Module,
			Section : Section,
			TemplateID: TemplateID
		},
		function(ReturnData)
		{
			var returnResultStr = ReturnData;
			var returnResultAry = returnResultStr.split("<!--eClassAppMessageTemplate-->");
			var a = returnResultAry[0];
			var b = returnResultAry[1];
			if(AppMessageReminder.targetMessageTitleID){
				$('#' + AppMessageReminder.targetMessageTitleID).val(a);
			}else{
				$('#PushMessageTitle').val(a);
			}
			if(AppMessageReminder.targetMessageContentID){
				$('#' + AppMessageReminder.targetMessageContentID).val(b);
			}else{
				$('#MessageContent').val(b);				
			}
			$('#AjaxStatus').html('');
			$('#SendAppMessage').attr('disabled', false);
		}
	);
	},
	
	sendAppMessage : function(){
		var StudentList = $('input#StudentList').val();
		var DateStart = $('input#DateStart').val();
		var DateEnd = $('input#DateEnd').val();
		var PushMessageTitle = $('input#PushMessageTitle').val();
		var MessageContent = $('textarea#MessageContent').val();
		
		if (PushMessageTitle == ""){
			alert('Please fill in the Title.')
			return false;
		}
		if (MessageContent == ""){
			alert('Please fill in the Content')
			return false;
		}
		
		$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		js_Hide_ThickBox();
		$.post(
			AppMessageReminder.TargetAjaxScript, 
// 			{ 
// 				MessageContent: MessageContent,
// 				PushMessageTitle: PushMessageTitle,
// 				StudentList: StudentList,
// 				DateStart: DateStart,
// 				DateEnd: DateEnd		
// 			},
			$("#pushMessageForm").serialize(),
			function(ReturnData)
			{
				$('#PreviewStatus').html(ReturnData);
			}
		);
	}
}
</script>