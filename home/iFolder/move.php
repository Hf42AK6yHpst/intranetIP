<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}


if (!$lftp->connect($lu->UserLogin,$lu->UserPassword))
{
     header("Location: close.php");
     exit();
}
else
{
    # List of files
    $user_root = $lftp->pwd();

    // Collect request variable
    $current_dir = isset($_REQUEST['current_dir'])?$_REQUEST['current_dir']:$user_root;
    
    #$new_dir = isset($_REQUEST['new_dir'])?$_REQUEST['new_dir']:NULL;
    if ($current_dir != "")
    {
        $current_dir = stripslashes($current_dir);
    }

    $lftp->chdir($current_dir);
    
    $current_dir = $lftp->pwd();

    $dirs = $lftp->getDirectoryList($user_root);
    $folder_select = getSelectByValue($dirs,"name=targetDir","",0,1);

    $fileList = "";
    $hiddenFileList = "";
    for ($i=0; $i<sizeof($filename); $i++)
    {
         $name = cleanCrossSiteScriptingCode($filename[$i]);
         $decoded_name = urldecode($name);
         $name = stripslashes($name);
         $fileList .= "$decoded_name<br>\n";
         $hiddenFileList .= "<input type=hidden name=filename[] value='$name'>\n";
    }
}

include_once($PATH_WRT_ROOT."templates/fileheader.php");
?>
<form name=form1 method=post action='move_update.php'>
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="725" border="0" cellpadding="0" cellspacing="0" bgcolor="#FCDBA8">
        <tr>
          <td width="8"><img src="<?=$image_path?>/myaccount_filecabinet/board_top_left.gif" width="8" height="8"></td>
          <td width="709"><img src="<?=$image_path?>/spacer.gif" width="20" height="8"></td>
          <td width="8"><img src="<?=$image_path?>/myaccount_filecabinet/board_top_right.gif" width="8" height="8"></td>
        </tr>
        <tr>
          <td bgcolor="#FCDBA8">&nbsp;</td>
          <td align="center">  </td>
          <td bgcolor="#FCDBA8">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center">
            <table width="700" border="1" cellpadding="3" cellspacing="0" bordercolorlight="FCF7E5" bordercolordark="#DBD6C4" bgcolor="F6F6F6" class="body">
                <tr>
                  <td align="center"><p>&nbsp;</p><table width="450" border="0" cellpadding="0" cellspacing="0" class="body">
                      <tr>
                        <td width="170" align="left" valign="middle"><img src="<?=$image_path?>/myaccount_filecabinet/graphic_carbinet.gif" width="162" height="210"></td>
                        <td>

<table width=95% border=0 cellspacing=5 cellpadding=1>
<tr><td><?=$i_Files_Move?>:</td><td><?=$fileList?></td></tr>
<tr><td><?=$i_Files_To?>:</td><td><?=$folder_select?></td></tr>
</table>
                        </td>
                      </tr>
                    </table><p>&nbsp;</p></td>
                </tr>
                <tr>
                  <td align="right">
                    <input type=image src="<?=$image_path?>/btn_move_<?=$intranet_session_language?>.gif" border=0>
                    <a href="javascript:history.back();"><image border=0 src="<?=$image_cancel?>"></a>
                  </td>
                </tr>
            </table>
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><img src="<?=$image_path?>/myaccount_filecabinet/board_bottom_left.gif" width="8" height="8"></td>
          <td><img src="<?=$image_path?>/spacer.gif" width="20" height="8"></td>
          <td><img src="<?=$image_path?>/myaccount_filecabinet/board_bottom_right.gif" width="8" height="8"></td>
        </tr>
      </table></td>
  </tr>
</table>
<input type=hidden name=current_dir value="<?=$current_dir?>">
<?=$hiddenFileList?>
</form>
<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
$lftp->close();
intranet_closedb();
?>