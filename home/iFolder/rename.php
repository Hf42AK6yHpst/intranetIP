<?php


/*
 *
 * 2011-03-16 Yuen
 * 			trimmed the filename to prevent problem if the filename contains space at the end
 * 
 *  
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}

if (!$lftp->connect($lu->UserLogin,$lu->UserPassword))
{
     header("Location: close.php");
     exit();
}

$user_root = $lftp->pwd();
$current_dir = cleanHtmlJavascript($current_dir);
if ($current_dir != "")
{
    $current_dir = stripslashes($current_dir);
    $lftp->chdir($current_dir);
}
$current_dir = $lftp->pwd();
$old_file = (is_array($filename)? cleanCrossSiteScriptingCode($filename[0]) :cleanCrossSiteScriptingCode($filename));

if ($lftp->chdir($current_dir) && $old_file != "")
{
	$old_file = urldecode($old_file);
    //$old_file = stripslashes($old_file);
	$newName = trim(stripslashes(cleanCrossSiteScriptingCode($newName)));
    if ($lftp->rename($current_dir,$old_file,$newName))
        $msg = 7;
    else $msg = 8;
}
else
{
    $msg = 8;
}

$encoded_dir = urlencode($current_dir);
$lftp->close();
intranet_closedb();
header("Location: browse.php?current_dir=$encoded_dir&msg=$msg");
?>