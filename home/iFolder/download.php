<?php

/*
 *
 * 2011-03-01 Yuen
 * 			never call chmod() in this download process
 * 
 *  
 */
 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}

if (!$lftp->connect($lu->UserLogin,$lu->UserPassword))
{
     header("Location: close.php");
     exit();
}

$user_root = $lftp->pwd();

$current_dir = (isset($current_dir_e) && $current_dir_e!="") ? getDecryptedText($current_dir_e): stripslashes($current_dir);
if ($current_dir != "")
{
    //$current_dir = stripslashes($current_dir);
    $lftp->chdir($current_dir);
}
$current_dir = $lftp->pwd();

$filename  = (isset($filename_e ) && $filename_e !="") ? getDecryptedText($filename_e): stripslashes($filename);
if ($lftp->chdir($current_dir) && $filename != "")
{
	
    //$filename = stripslashes($filename);
 
    /*
    $permission = substr(sprintf('%o', @fileperms($current_dir."/".$filename)), -4);
	if($permission == 0)
		$lftp->chmod($current_dir."/".$filename,'','');
    */
    #echo $filename;
    #exit();
    $lftp->download($current_dir,$filename);
}
else
{
    header("Location: close.php");
}

$lftp->close();
intranet_closedb();
?>