<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}


if (!$lftp->connect($lu->UserLogin,$lu->UserPassword))
{
     header("Location: close.php");
     exit();
}

$user_root = $lftp->pwd();
if ($current_dir != "")
{
    $current_dir = stripslashes($current_dir);
    $lftp->chdir($current_dir);
}
$current_dir = $lftp->pwd();

$success = 0;
if ($lftp->chdir($current_dir))
{
    for ($i=0; $i<sizeof($filename); $i++)
    {
        $filename[$i] = stripslashes(urldecode(cleanCrossSiteScriptingCode($filename[$i])));
         if ($lftp->remove($current_dir,$filename[$i]))
             $success++;
    }
    $msg = 5;
}
else
{
    $msg = 6;
}

$encoded_dir = urlencode($current_dir);
$lftp->close();
intranet_closedb();
header("Location: browse.php?current_dir=$encoded_dir&msg=$msg&success=$success");
?>