<?php
## Using by : Jason
//error_reporting(ALL);
//ini_set('error_reporting', E_ALL);
/********************** Change Log ***********************/
#
#	Date	:	2016-08-21 (Jason)
#				Development of Google Single-Sign-On mechanism
#
/******************* End Of Change Log *******************/


########################################################################
### SSO ################################################################
########################################################################
include_once("includes/global.php");
include_once("includes/libdb.php");

intranet_opendb();


## Get Data
$srv	 = (isset($_GET['srv']) && $_GET['srv'] != '') ? $_GET['srv'] : '';
$gmail 	= (isset($_GET['gmail']) && $_GET['gmail'] != '') ? $_GET['gmail'] : '';


## Init 
$SP_name = '';


## Authentication
//debug_r($_SESSION);die();
require_once($ssoservice["Google"]["SimpleSAML_Root"].'/lib/_autoload.php');

$SP_name = Get_GoogleSSO_Service_Provider_Name($gmail);

if($SP_name != ''){
	$as = new SimpleSAML_Auth_Simple($SP_name);
	if(!$as->isAuthenticated()){
		$invalid_sso = true;
	}
} else {
	$invalid_sso = true;
}


## Error checking
if($invalid_sso){
	die('You are not allowed to access.');
}

## Main
# get user info
$attributes = $as->getAttributes();
$UserLogin = $attributes['uid'][0];
$UserEmail = $attributes['pri_email'][0];
//debug_r($attributes);die();

$tempData = explode("@", $UserEmail);
$UserLogin = $tempData[0];
$UserEmailSuffix = $tempData[1];

if($UserLogin != ''){
//	$session = SimpleSAML_Session::getInstance(); 
//	$session->setData('ABC', 'ABCId', 'Jason Testing'); 
//	$session_a = new SimpleSAML_Session();die();
//	$session = SimpleSAML_Session::getSessionFromRequest();
//	$session->cleanup();
	
	if($_SESSION['SignInWithGoogle']){
		
		$ToUrl = "https://mail.google.com/a/".$UserEmailSuffix;
		
	} else {
		# write session to distinguish the login is via Google SSO or not 
		$_SESSION['SignInWithGoogle'] = true;
		$_SESSION['SignInWithGoogle_UserEmail'] = $UserEmail;
		$_SESSION['SignInWithGoogle_UserEmailSuffix'] = $UserEmailSuffix;
		
		# grep userlogin for normal login as IP25 user
		$target_str .= "ssoUserLogin=".$UserLogin."&ssoAuthType=SSOLOGIN";
		$target_str .= ($srv != '') ? "&ssoSrvType=".$srv : ""; 
		$target_e = getEncryptedText($target_str);
	//	debug_r($_SESSION);
	/*
		switch($srv){
			case 'gmail':
				$ToUrl = "https://mail.google.com/a/".$UserEmailSuffix;
				break;
			default:
				$ToUrl = "login.php?target_e=".$target_e;
				break;
		}*/
		$ToUrl = "login.php?target_e=".$target_e;
	}
	
//	debug_r($ToUrl);

	header("Location: ".$ToUrl);
} else {
	# destroy corresponding session
	$_SESSION['SignInWithGoogle'] = null;
	$_SESSION['SignInWithGoogle_UserEmail'] = null;
	$_SESSION['SignInWithGoogle_UserEmailSuffix'] = null;
	unset($_SESSION['SignInWithGoogle']);
	unset($_SESSION['SignInWithGoogle_UserEmail']);
	unset($_SESSION['SignInWithGoogle_UserEmailSuffix']);
	
	echo "Invalid User";	
}

# get user credential of IP25
/*
include_once("includes/libuser.php");
$lu = new libuser();
$userInfo = $lu->returnUser("", $UserLogin);
//debug_r($userInfo);
if (count($userInfo) > 0) {
	if(trim($userInfo[0]['UserPassword']) == '') {
		if($intranet_authentication_method=="HASH"){
			include_once("includes/libauth.php");
			$lauth = new libauth();
			$hasedPw = $lauth->GetUserDecryptedPassword($userInfo[0]['UserID']);
			$lu->UserPassword = $hasedPw;
		}
	}
	
	$ToUrl = "login.php?UserLogin=".$UserLogin."&UserPassword=".$lu->UserPassword."&target_url=".urlencode($target_url);
	header("Location: ".$ToUrl);
	
} else {
	die('No UserID is found.');
}
*/

intranet_closedb();
die();
########################################################################
### SSO ################################################################
########################################################################

?>