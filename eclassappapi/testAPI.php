<?php

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."addon/check.php");

intranet_opendb();

$ldb = new libdb();
$linterface = new interface_html();

?>
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
function jsonPost() {
	var jsonString = $('#jsonContent').val();
	// var jsonObject = JSON.parse(jsonString);
	
	$.ajax({
		type: "POST",
		url: "index.php",
		data: jsonString,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data) {
			$('#result').val(JSON.stringify(data));
		},
		failture: function(errMsg) {
			alert(errMsg);
		}
	});
}

function goSubmit() {
	$('form#form1').submit();
}
</script>
<html>
	<body>
		<form id="form1" name="form1" method="post" action="testAPI_update.php" target="_blank">
			<table>
				<tr>
					<td>
						********** JSON Input **********<br>
						<textarea id="jsonContent" name="jsonContent" cols="50" rows="20"></textarea>
						<br>
						<input value="Submit by ajax" type="button" onclick="jsonPost();">
						<input value="Submit by form post" type="button" onclick="goSubmit();">
					</td>
				</tr>
			</table>
			<h3>Response: </h3>
			<div><textarea id="result" cols="50" rows="10"></textarea></div>	
			<br>
			<div id="api">
				<table border="1" width="500px">
				  <tr>
					<th>JSON API:</th>
				  </tr>
				  <tr>
					<td>
						<b>Login:</b><br>
						---------------------------------------------------------------------------------<br>
						{<br>
						"eClassRequest": {<br>
						"RequestID": "RequestID",<br>
						"RequestMethod": "GetSchoolNews",<br>
						"APIKey": "APIKey",<br>
						"Request": {<br>
						"CurrentUserLogin": "abcde",<br>
						"EndDate": "2015-01-01" <br>
						}<br>
						}<br>
						}
					</td>
				  </tr>
				</table>
			</div>
		</form>
	</body>
</html>