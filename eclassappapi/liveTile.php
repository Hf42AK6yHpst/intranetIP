<?php
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libmethod.php");
include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
include_once($PATH_WRT_ROOT."includes/libwebservice.php");
//include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libeclass_ws.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libAES.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/libeclass_ws.php');

intranet_opendb();

$AESKey = $eclassAppConfig['aesKey'];
$objDB = new libdb();
$objEClassWs = new libeclass_ws();

$studentID = IntegerSafe($_REQUEST["studentID"]);
$parentID = 0;
$lang = $_REQUEST['lang'];



###### Get Student Information START ######
$sql = "SELECT * FROM INTRANET_USER WHERE UserID='{$studentID}'";
$rs = $objDB->returnResultSet($sql);
if(count($rs)==0){
	exit;
}
$rs = $rs[0];
if(strpos($lang, 'en') === false){
	$studentName = ($rs['ChineseName'] == '')? $rs['EnglishName'] : $rs['ChineseName'];
}else{
	$studentName = ($rs['EnglishName'] == '')? $rs['ChineseName'] : $rs['EnglishName'];
}
$className = $rs['ClassName'];
$classNumber = $rs['ClassNumber'];
if($className == '' && $classNumber == ''){
	$classString = '';
}else{
	$classString = " - {$className} ({$classNumber})";
}
###### Get Student Information END ######


###### Get Content START ######

#### Get Announcement START ####
$rs = $objEClassWs->WS_GetAnnouncementForApp($TargetUserID=$studentID, $CurrentUserID=$parentID, $NewestDateTime='', $NoOfRecords='', $TargetGroup='S');
$announcement = $rs['Announcement'][1];
#### Get Announcement END ####

#### Get eNotice START ####
$rs = $objEClassWs->WS_GetNoticeForApp($TargetUserID=$studentID, $CurrentUserID=$parentID);
$noticeList = $rs['Notice'];
foreach($noticeList as $n){
	if($n['RecordStatus'] == 'N'){
		$notice = $n;
		break;
	}
}
#### Get eNotice END ####


#### Compare and get content START ####
if($announcement != null && $notice != null){
	$content = (strcmp($announcement['AnnouncementDate'], $notice['DateStart']))? $notice['Title'] : $announcement['Title'] ;
}else{
	if($announcement != null){
		$content = $announcement['Title'];
	}else if($notice != null){
		$content = $notice['Title'];
	}else{
		exit;
	}
}
#### Compare and get content END ####

$content = str_replace('"','&quot;',$content);
$content = str_replace('\'','&apos;',$content);
$content = str_replace('<','&lt;',$content);
$content = str_replace('>','&gt;',$content);
$content = str_replace('&','&amp;',$content);

###### Get Content END######


header("X-WNS-Tag: {$config_school_code}");
header("X-WNS-Group: {$rs['UserID']}");
?>
<?/**/?>
<tile>
  <visual version="2">
    <binding template="TileSquare150x150PeekImageAndText02" fallback="TileSquarePeekImageAndText02">
      <image id="1" src="Assets/eclass_app_icon_final_150.jpg" alt="eClass"/>
      <text id="1"><?=$studentName?><?=$classString?></text>
      <text id="2"><?=$content?></text>
    </binding>
    <binding template="TileWide310x150PeekImage01" fallback="TileWidePeekImage01">
      <image id="1" src="Assets/eclass_app_icon_final_WP.scale-240.png" alt="eClass"/>
      <text id="1"><?=$studentName?><?=$classString?></text>
      <text id="2"><?=$content?></text>
    </binding>  
  </visual>
</tile>
<?/** /?>
<tile>
  <visual version="2">
    <binding template="TileSquare150x150Text02" fallback="TileSquareText02">
      <text id="1"><?=$studentName?></text>
      <text id="2">Text Field 2b</text>
      <image id="1" src="Assets/eclass_app_icon_final_150.jpg" alt="eClass"/>
    </binding>
    <binding template="TileWide310x150Text09" fallback="TileWideText09">
      <text id="1"><?=$studentName?></text>
      <text id="2">Text Field 2b</text>
      <image id="1" src="Assets/eclass_app_icon_final_WP.scale-240.png" alt="eClass"/>
    </binding>  
  </visual>
</tile>
<?/**/?>