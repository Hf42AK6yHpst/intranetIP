<?php
# editing by anna

/********************
 * change log:
 * 2020-11-16 Ray: Add ApiKeyVersion
 * 2020-11-06 Ray: Add $SessionID checking
 * 2020-07-08 Ray: Add GetSchoolDayCount
 * 2020-06-09 Ray: Add GetSessionNumberForDate
 * 2018-10-12 Bill: Add function GetUserOfficialPhotoPath, GetUserPersonalPhotoPath
 * 2018-07-16 Carlos: Added request method GetPaymentItemMerchantInfo for getting ePayment item and merchant account info.
 * 2016-11-10 Henry HM: Add api CentennialLogin
 * 2016-08-09 Henry HM: Add api GetHKUFluAgreementRecord, NewHKUFluAgreementRecord, GetHKUFluSickLeaveRecord, NewHKUFluSickLeaveRecord
 * 2016-07-14 Ivan [ip.2.5.7.7.1]: do not add login record if login parent app using a student account
 * 2016-04-22 Roy: return student info data (image path, websams etc) for student app while login
 * 2016-03-10 Ivan: return module access right data for student app
 * 2015-07-09 Roy: add function GetNoticeUrlForApp to $skipSessionCheckingRequestAry
 * 2015-06-02 Ivan: support LDAP with $ldap_user_type_mode is turned on
 * 2014-11-07 Qiao: change get $result['ModuleAccessRight'] to add function to check whether current user has access right to teacher status
 ********************/
 
$NoLangOld20 = true;
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libmethod.php");
include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
include_once($PATH_WRT_ROOT."includes/libwebservice.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libeclass_ws.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libAES.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');


intranet_opendb();
$_SESSION['CurrentSchoolYearID'] = '';
$AESKey = $eclassAppConfig['aesKey'];

if ($sys_custom['eClassApp']['enableRequestLog']) {
	StartTimer('eclassappapiTimer');
	$startApiTime = date("Y-m-d H:i:s");
}

//$ldb = new libdb();
$jsonObj = new JSON_obj();
$libwebservice = new libwebservice();
$eclassApiAuth = new libeclassapiauth();
$method = new LibMethod();
$libaes = new libAES($AESKey);
$libauth = new libauth();
$libeclass_ws = new libeclass_ws();
$libeClassApp = new libeClassApp();
$successAry = array();


$requestJsonEncryptedString = '';
$requestJsonDecryptedString = '';
$responseJsonEncryptedString = '';
$responseJsonDecryptedString = '';



$requestJsonString = file_get_contents("php://input");
//error_log(date("Y-m-d H:i:s")."-->jsonData = '".$jsonData."';\n", 3, $eclassAppConfig['filePath']."requestLog/".date('Y-m-d').".log");

# sample request
//$requestJsonString = '{"eClassRequestEncrypted":"t+hD1mVfr6GFRr8LLVaz1KVPv5H3U3SLiHuXpI51PK672flYaZCemprgWp74PGqXHT7HONzPRpl3\nZ9usYjqwR3IjDSxPG\/nwxBX7aoX1vikkfTDtIiqhna7Pzv2u5w01i39oaKYOCL4Dcgr7\/JRIU\/UP\nMv5r1lrTn+6WeCkAH4cKMils4NkGINXQfl6tz3cigeTLnPmeGe3+9FOLaTw3QQ==\n"}';


if (($plugin['eClassApp'] || $plugin['eClassTeacherApp'] || $plugin['eClassStudentApp']) && $requestJsonString != '') {
	
	$requestJsonAry = $jsonObj->decode($requestJsonString);
	
	
	# AES Key transmission by RSA
	if ($requestJsonAry['PublicKey']) {
		
		$requestJsonDecryptedString = $requestJsonString;
		$publicKey = $requestJsonAry['PublicKey'];
		
		# sample key pairs for testing
		// $publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuN3fDHVczTBB+MGM+25BEy8UsOgGX5UV\nsHArTxYKcIHgPWU+lQDpZFz/liTwCdtPasOfN8veD2ZIf83/zjYn4cZUd5AqPAis4srEvqcmTbC2\ng1GTrHUrmUMAjpuQ5tkvwU2Xj6ZTZXgJSMP1ttQe1EPNtCtK1IUpDQxcO4PDLGNkiY2MJBeAo+hj\nxGuigrEQO1ndLDwFzetORxdEyBOc+EjMW9cjKXlnq0FWzvgYw62QqjvvOEWvMs3itCOKQZjjUWg+\nBuNUdx9oz+mrtxXhOBXXjyd0zh9bFbE0QtdvWjAn5I47fzZfX7m5MAAUXZrQ1TbgXQve2EGWdzAU\nNkelewIDAQAB";
		// $privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC43d8MdVzNMEH4wYz7bkETLxSw\n6AZflRWwcCtPFgpwgeA9ZT6VAOlkXP+WJPAJ209qw583y94PZkh/zf/ONifhxlR3kCo8CKziysS+\npyZNsLaDUZOsdSuZQwCOm5Dm2S/BTZePplNleAlIw/W21B7UQ820K0rUhSkNDFw7g8MsY2SJjYwk\nF4Cj6GPEa6KCsRA7Wd0sPAXN605HF0TIE5z4SMxb1yMpeWerQVbO+BjDrZCqO+84Ra8yzeK0I4pB\nmONRaD4G41R3H2jP6au3FeE4FdePJ3TOH1sVsTRC129aMCfkjjt/Nl9fubkwABRdmtDVNuBdC97Y\nQZZ3MBQ2R6V7AgMBAAECggEBAIKbFcM6g1bfuiwhqhXUfhssjwSw2BfdPxaJGL/mBXgY+bDGf6FL\npJhob5XHAOMl0wTNsgX8fSUKETxh8FDix+hlczWJX51MT8BmmvtLIe+PYlKCZr9XhHbh9FnMJGJD\nnM6HoAU38AFm/cAv2b/SOvmh8YlRcKuWbDswuVS+sTwAw19vqjhjh/jTUoFeTmZJ+iqXjiRICid/\n3uh8LcorW4iu4dvzuKSn8mUWy8f7u7/JFCf+tSje5Qz6y/10Gwb86VYmd/JM0nmKx9W8Rw3W0JRG\nnwm4J+0IRZA6BGzH4FItM9dfl1GGxzGkPkYudOZiiZYzz0ZSmbhUupmuzoD7NVECgYEA8d7NvIDd\nGKHlxbJZ2OZjFq75ps6Kb1MqioxG6EmeG8f1VYG9BPEaUEKo7EpSo4k6C1w8KJq3Ap9Nk33x1Y7g\nk3mKh3dcDC1xk9oXM+DeexQ1u0LsA+kTaxL3sPtY/RIE5SJ77GZvdVyjHNi+qxKCxjmmPBWPKtw2\n+7waih3Ns6MCgYEAw6qSdrxkZbeED7z+xkMf6OzGsc7mVoRAccPrdSViqGUcBYebBjdwdqElM+R5\nBy5WjIOe2G1EMstVk13kYpdlUyNgkX7JrSKtAByNbDKQKOwQcCkMwuOpzKIS6IGKqNTDle8K1d3P\nLRDgFXO7RsEacPIyg0JUR7AJaoKA1FL5pEkCgYATOWtO1Us9fvi9WuyrQbyUe/nZ2UbFjG2wL3Af\nXGKVwLHuRu6YKvW1dWbpyQqCdxDHAGDDtXBgY/sNgxAqsj2FWz64f7MwQJhrcQUxGWl1jOisZqhN\ns0PY2dwYFBJBoyICeFgzNP0c7e0FCPE2tbTxPnnJAsmrVW+FWLEfUzkbUwKBgArBCz+/Zv04Mufy\niUY4Vj6lFN6c8of6yNf33q8XmO6McBfVti2HSoUaokLR4d0FLPiYG1jl2IO6LT/mPzE0BPumzB3z\nC/6aE0wYRaWWudml79laSDtF7AU1OzjQNeDFoOJSuxO6FzLw5IAJVlezMVLX3PLd6GfwQvQ5q/7/\neH0RAoGBAOCUbQ7VbH4w/NA/QzIx7Fh5tC10Adyns1HburOTuvyufK3cUupFxzW3pbspnOQogP+a\ndqc5MG11GIjEONO66pTpkT8WfbXNyO5D/hmVIIOcEQGIu5MQuvoB+tw3Dra8V3wrx1pA+yKE6Sow\n4e2cZLebT/D6k2SCaWn1VCU7R9HI";
		
		$publicKey = base64_decode(($publicKey));

		# Key translation: DER encoded keys into PEM
		$pem = chunk_split(base64_encode($publicKey), 64, "\n");
		$publicKey = "-----BEGIN PUBLIC KEY-----\n".$pem."-----END PUBLIC KEY-----\n";
		
		# do encrypt
		if (openssl_public_encrypt($AESKey, $encrypted, $publicKey, OPENSSL_PKCS1_PADDING)) {
            $encryptedKey = base64_encode($encrypted);
		}
		
		# check if server installed mcrypt or not
		$mcryptStatus = function_exists('mcrypt_module_open');
		
		# return AESKey as response
		$returnJsonAry = array();
		$returnJsonAry['EncryptedAESKey'] = $encryptedKey;
		$returnJsonAry['McryptStatus'] = $mcryptStatus;
	}
	else {
		# decrypt request
		$requestJsonDecryptedString = $requestJsonString;
		if ($requestJsonAry["eClassRequestEncrypted"]) {
			$AESEnabled = true;
			$requestJsonEncryptedString = $requestJsonString;
			$encryptedRequest = $requestJsonAry["eClassRequestEncrypted"];
					
			$requestJsonDecryptedString = $libaes->decrypt($encryptedRequest);
			if ($requestJsonDecryptedString != '') {
				# decrypted request
				$requestJsonAry = $jsonObj->decode($requestJsonDecryptedString);
			}
		}
		
		# handle request
		if ($requestJsonAry['eClassRequest']) {
			// $SessionID <-- cannot use $sessionID since some functions already global $SessionID
			$SessionID = $requestJsonAry['eClassRequest']['SessionID'];
			//$requestID = $arr['eClassRequest']['RequestID'];
			$requestMethod = $requestJsonAry['eClassRequest']['RequestMethod'];
			$apiKey = $requestJsonAry['eClassRequest']["APIKey"];
			
			
			// commented as not used in eclas app now
	//		if ($apiKey) {
	//			// $verifyAPIKey = $eclassApiAuth->VerifyAPIKey($apiKey);	#commented for testing
	//			$verifyAPIKey = true;
	//			$arr['eClassRequest']['Request']["hasAPIKey"] = true;
	//		}
			$verifyAPIKey = true;
			$requestJsonAry['eClassRequest']['Request']["hasAPIKey"] = true;
			if ($apiKey == '') {
				$appType = $eclassAppConfig['appType']['Parent'];
			}
			else {
				$projectName = $eclassApiAuth->GetProjectByApiKey($apiKey);
				if ($projectName == $eclassAppConfig['appApiProjectName']['P']) {
					$appType = $eclassAppConfig['appType']['Parent'];
				}
				else if ($projectName == $eclassAppConfig['appApiProjectName']['T']) {
					$appType = $eclassAppConfig['appType']['Teacher'];
				}
				else if ($projectName == $eclassAppConfig['appApiProjectName']['S']) {
					$appType = $eclassAppConfig['appType']['Student'];
					
					$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
					$enableStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableStudentApp']);
					if (!$enableStudentApp) {
						$verifyAPIKey = false;
					}
				}
				else {
//					$verifyAPIKey = false;
//					$requestJsonAry['eClassRequest']['Request']["hasAPIKey"] = false;
					// parent app request may have wrong hardcoded API key
					$appType = $eclassAppConfig['appType']['Parent'];
				}
			}
			
			if ($verifyAPIKey) {
				# handle request method:
				if (trim($SessionID) != '' || $requestMethod=='SendPushMessageToUser' || $requestMethod=='UpdatePaymentCallback' || $requestMethod=='GetPaymentItemMerchantInfo' ||$requestMethod=='ForgetPasswordForDHL' || $requestMethod=='UpdateReprintCardRecordPaymentStatus' || $requestMethod == 'ForgetPasswordVersion' || $requestMethod == 'ForgetPasswordV2' || $requestMethod == 'ApiKeyVersion') {
					// request "SendPushMessageToUser" has no session id
					$skipSessionCheckingRequestAry = array();
					$skipSessionCheckingRequestAry[] = 'SaveUserDeviceInfo';
					$skipSessionCheckingRequestAry[] = 'GetSchoolBannerUrl';
					$skipSessionCheckingRequestAry[] = 'GetNoticeListForApp';
					$skipSessionCheckingRequestAry[] = 'GetAnnouncementListForApp';
					$skipSessionCheckingRequestAry[] = 'GetStudentAttendanceInfo';
					$skipSessionCheckingRequestAry[] = 'UpdatePushMessageReadStatus';
					$skipSessionCheckingRequestAry[] = 'DeleteAccountForApp';
					$skipSessionCheckingRequestAry[] = 'GetHomeworkListForApp';
					$skipSessionCheckingRequestAry[] = 'GetSchoolEventForApp';
					$skipSessionCheckingRequestAry[] = 'GetPaymentDataForApp';
					$skipSessionCheckingRequestAry[] = 'GetAttendanceDetailsForApp';
					$skipSessionCheckingRequestAry[] = 'GetStudentTakeLeaveRecords';
					$skipSessionCheckingRequestAry[] = 'GetStudentTakeLeaveSettings';
					$skipSessionCheckingRequestAry[] = 'SubmitApplyLeaveRecord';
					$skipSessionCheckingRequestAry[] = 'GetPushNotificationData';
					$skipSessionCheckingRequestAry[] = 'GetLatestPushNotificationData';
					$skipSessionCheckingRequestAry[] = 'GetLastPushNotificationId';
					$skipSessionCheckingRequestAry[] = 'GetLeaveRecordImages';
					$skipSessionCheckingRequestAry[] = 'GetLatestPushNotificationDataByTs';
					$skipSessionCheckingRequestAry[] = 'SaveDeviceAuthCode';
					$skipSessionCheckingRequestAry[] = 'ValidateDeviceAuthCode';
					$skipSessionCheckingRequestAry[] = 'UpdateScheduledPushMessageStatus';
					$skipSessionCheckingRequestAry[] = 'GetCurrentServerDateTime';
					$skipSessionCheckingRequestAry[] = 'GetStaffAttendanceTodayRecord';
					$skipSessionCheckingRequestAry[] = 'GetMultipleRequestResult';
					$skipSessionCheckingRequestAry[] = 'GeteCircularForApp';
					$skipSessionCheckingRequestAry[] = 'GetDeviceAuthCode';
					$skipSessionCheckingRequestAry[] = 'GeteNoticeSForApp';
					$skipSessionCheckingRequestAry[] = 'SendPushMessageToUser';
					$skipSessionCheckingRequestAry[] = 'GetDigitalChannelPhotoList';
					$skipSessionCheckingRequestAry[] = 'DigitalChannelPhotoLike';
					$skipSessionCheckingRequestAry[] = 'GetNoticeUrlForApp';
					$skipSessionCheckingRequestAry[] = 'UserChangePassword';					
					$skipSessionCheckingRequestAry[] = 'GetMedicalCaringForApp';
					$skipSessionCheckingRequestAry[] = 'GetSubjectListForApp';
					$skipSessionCheckingRequestAry[] = 'GetMedicalCaringForTeacherApp';
					$skipSessionCheckingRequestAry[] = 'GetAnnouncementListForApp';
					$skipSessionCheckingRequestAry[] = 'GetWeeklyDiaryForStudentApp';
					$skipSessionCheckingRequestAry[] = 'GetELibPlusLoanBookRecord';
					$skipSessionCheckingRequestAry[] = 'GetELibPlusReservedBookRecord';
					$skipSessionCheckingRequestAry[] = 'GetELibPlusOutstandingPenalty';
					$skipSessionCheckingRequestAry[] = 'SubmitWeeklyDiaryRecord';
					$skipSessionCheckingRequestAry[] = 'GetHKUFluAgreementRecord';
					$skipSessionCheckingRequestAry[] = 'NewHKUFluAgreementRecord';
					$skipSessionCheckingRequestAry[] = 'GetHKUFluSickLeaveRecord';
					$skipSessionCheckingRequestAry[] = 'NewHKUFluSickLeaveRecord';
					$skipSessionCheckingRequestAry[] = 'IsHKUFluRecordExists';
					$skipSessionCheckingRequestAry[] = 'UpdatePaymentCallback';
					$skipSessionCheckingRequestAry[] = 'ForgetPasswordForDHL';
					$skipSessionCheckingRequestAry[] = 'ClearLicenseCache';
					$skipSessionCheckingRequestAry[] = 'GetTargetableUsersForMessaging';
					$skipSessionCheckingRequestAry[] = 'GetPaymentItemMerchantInfo';
					$skipSessionCheckingRequestAry[] = 'GetUserOfficialPhotoPath';
					$skipSessionCheckingRequestAry[] = 'GetUserPersonalPhotoPath';
					$skipSessionCheckingRequestAry[] = 'SaveUserPhoto';
					$skipSessionCheckingRequestAry[] = 'UseOfficialPhotoAsPersonalPhoto';
					$skipSessionCheckingRequestAry[] = 'CancelOfficialPhotoAsPersonalPhoto';
					$skipSessionCheckingRequestAry[] = 'UpdateReprintCardRecordPaymentStatus';
					$skipSessionCheckingRequestAry[] = 'UpdateReprintCardOrderStatus';
					
					$skipSessionCheckingRequestAry[] = 'DigitalChannelUploadAlbumDetails';
					$skipSessionCheckingRequestAry[] = 'DigitalChannelUploadPhotoDetails';
					$skipSessionCheckingRequestAry[] = 'GetTargetGroupList';
					$skipSessionCheckingRequestAry[] = 'GetTargetGroupListForPIC';
					$skipSessionCheckingRequestAry[] = 'GetUserListInTargetGroup';
					$skipSessionCheckingRequestAry[] = 'DigitalChannelGetAlbumDetails';

					$skipSessionCheckingRequestAry[] = 'GetSessionNumberForDate';
					$skipSessionCheckingRequestAry[] = 'GetSchoolDayCount';

					$skipSessionCheckingRequestAry[] = 'ForgetPasswordVersion';
					$skipSessionCheckingRequestAry[] = 'ForgetPasswordV2';

					$skipSessionCheckingRequestAry[] = 'ApiKeyVersion';

					$sessionid_valid = true;
					$apiKey_data = $eclassApiAuth->GetDataByApiKey($apiKey);
					if(IntegerSafe($apiKey_data['Version']) >= 2) {
						$result = $libauth->CheckAppLoginSession(trim($SessionID));
						if($result == false) {
							$sessionid_valid = false;
						}
					}

					if($sessionid_valid) {
						if (in_array($requestMethod, $skipSessionCheckingRequestAry)) {
							// no need UserID for this request
							$requestID = $method->Generate_RequestID();
							$result = $method->callMethod($requestMethod, $requestJsonAry);
						} else {

							$UserID = $libauth->GetUserIDFromSessionKey(trim($SessionID));

							if ($UserID != '') {
								if ($requestMethod == 'Logout') {
									$requestJsonAry['eClassRequest']['Request']['UserID'] = $UserID;
								}
								$requestID = $method->Generate_RequestID();
								$result = $method->callMethod($requestMethod, $requestJsonAry);

								## 2011-08-04: log ws last request
								// $libwebservice->update_request_log_lastrequest($UserID, $xmlContent, $apikey = '');

							}else{
								$result = $method->getErrorArray('5');
							}
						}
					} else {
						$result = $method->getErrorArray('5');
					}

				} else if ($requestMethod == "Login" || $requestMethod == "LoginAndGetChildrenListForApp" || $requestMethod == "CentennialLogin") {
					// check license
					$isSchoolInAppLicense = $libeClassApp->isSchoolInLicense($appType);
					
					if ($isSchoolInAppLicense) {
						// in license => do login
						$requestID = $method->Generate_RequestID();
						$requestJsonAry['eClassRequest']['Request']['fromEClassApp'] = true;
						
						if ($appType==$eclassAppConfig['appType']['Student'] && isset($sys_custom['eClassApp']['sfocPublicStudentUserName']) && isset($sys_custom['eClassApp']['sfocPublicStudentPassword'])) {
							$requestJsonAry['eClassRequest']['Request']['UserName'] = $sys_custom['eClassApp']['sfocPublicStudentUserName'];
							$requestJsonAry['eClassRequest']['Request']['Password'] = $sys_custom['eClassApp']['sfocPublicStudentPassword'];
						}
						
						$curUserLogin = $requestJsonAry['eClassRequest']['Request']['UserName'];
						$isBlacklistedUser = $libeClassApp->isBlacklistedUser($curUserLogin);
						
						if (!$isBlacklistedUser) {
							if (((isset($UseLDAP) && $UseLDAP) || $intranet_authentication_method == 'LDAP') && $ldap_user_type_mode){
								# Different users using different LDAP dn string
								
								# Get User Type
								$sql = "SELECT UserID, RecordType FROM INTRANET_USER WHERE UserLogin = '".$curUserLogin."'";
								$temp = $libauth->returnArray($sql,2);
								
								list($t_id, $t_user_type) = $temp[0];
								
								if (!is_array($temp) || sizeof($temp)==0 || $t_user_type == "" || $t_user_type < 1 || $t_user_type > 4)
								{
									$Result = '401';
								}
								else{
									switch ($t_user_type)
									{
										case 1: $special_ldap_dn = $ldap_teacher_base_dn; break;
										case 2: $special_ldap_dn = $ldap_student_base_dn; break;
										case 3: $special_ldap_dn = $ldap_parent_base_dn; break;
										default: $special_ldap_dn = $ldap_teacher_base_dn;
									}
								}
							}
							
							if($requestMethod == "CentennialLogin"){
								$result = $method->callMethod('LoginCentennial', $requestJsonAry);
							}else{
								$result = $method->callMethod('Login', $requestJsonAry);
							}
							if (is_array($result)) {
								$curUserId = $result['UserID'];
								$targetUserLogin = $result['UserLogin'];
								$isTeacher = ($result['UserType']=='T')? true : false;
								$isStudent = ($result['UserType']=='S')? true : false;
								$isParent = ($result['UserType']=='P')? true : false;
								
								$matchedAppType = false;
								if ($isParent && $appType == $eclassAppConfig['appType']['Parent']) {
									$matchedAppType = true;
								}
								else if ($isTeacher && $appType == $eclassAppConfig['appType']['Teacher']) {
									$matchedAppType = true;
								}
								else if ($isStudent && $appType == $eclassAppConfig['appType']['Student']) {
									$matchedAppType = true;
								}
								
								
								// log last login time
								if ($matchedAppType) {
									if ($sys_custom['demo_site_account_mapping']) {
										$sql = "Select UserID From INTRANET_USER where UserLogin = '".$libeClassApp->Get_Safe_Sql_Query($curUserLogin)."'";
										$userInfoAry = $libeClassApp->returnResultSet($sql);
										$originallyUserId = $userInfoAry[0]['UserID'];
										
										if ($originallyUserId > 0) {
											$libwebservice->update_request_log_login_eClassApp($originallyUserId, $result['SessionID']);
										}
									}
									else {
										$libwebservice->update_request_log_login_eClassApp($curUserId, $result['SessionID']);
									}
								}
								
								// get children data
								if ($requestMethod == "LoginAndGetChildrenListForApp" || $requestMethod == "CentennialLogin") {
									$resultTmp = $result;
									$result = array();
									$result['LoginResult'] = $resultTmp;
									//$result['GetChildrenListForAppResult'] = $method->callMethod('GetChildrenListForApp', $requestJsonAry);
									
									if ($sys_custom['demo_site_account_mapping']) {
										// just map to "p" for demo accounts ending with "_p"
// 										$tmpUserLogin = 'p';
										if (strlen($curUserLogin)==strpos($curUserLogin, "_p")+2) {
											$tmpUserLogin = "p";
										}
										else {
											$tmpUserLogin = $targetUserLogin;
										}
									}
									else {
										$tmpUserLogin = $targetUserLogin;
									}
									$result['GetChildrenListForAppResult'] = $libeclass_ws->WS_GetChildrenListForApp($tmpUserLogin);
								}
								
								// 2016-04-22 Roy: return student image path
								if ($appType == $eclassAppConfig['appType']['Student'] && $isStudent) {
									$result['GetStudentInfo'] = $libeclass_ws->GetStudentInfo_eClassApp(array($curUserId));
									
									// 2016-04-25 Ivan: add flag to determine has enabled student app or not
									$enableStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableStudentApp']);
									$result['eClassStudentAppEnabled'] = ($enableStudentApp)? '1' : '0';
								}
								
								// 2016-12-30 Roy: Power lesson 2
								$enablePowerLesson2 = true;
								$result['PowerLesson2Enabled'] = $enablePowerLesson2 ?  '1' : '0';
								
								
								$schoolCustFlagAry = array();
								// get smartcard balance display or not
								$showSmartcardBalance = '1';
								if ($libeclass_ws->isKIS($curUserLogin) && $sys_custom['ePayment']['KIS_NoBalance']) {
									// KIS => not show balance
									$showSmartcardBalance = '0';
								}
								$result['SchoolCustFlags']['ShowSmartcardBalance'] = $showSmartcardBalance;
								$result['SchoolCustFlags']['flag2'] = '222';
								$result['SchoolCustFlags']['flag3'] = '333';
								
								
								$result['schoolCustFlagAry'] = $libeClassApp->getSchoolExtraFlag($curUserLogin, $curUserId);
								$result['userCustFlagAry'] = $libeClassApp->getUserExtraFlag($curUserLogin, $curUserId);
								
								if ($isTeacher || $isStudent) {
									//$moduleAccessRightAry = $libeClassApp->getAccessRightInfo($eclassAppConfig['appType']['Teacher']);
									//$result['ModuleAccessRight'] = $moduleAccessRightAry[0];	// 0 means no Form
									$result['ModuleAccessRight'] = $libeClassApp->getUserModuleAccessRight($curUserId, $appType);
									
									// 20160426 Ivan: quick fix to solve force close in student app if student has no class
									if ($isStudent && count($result['ModuleAccessRight'])==0) {
										//$result['ModuleAccessRight'] = null;
									    $result['ModuleAccessRight']['dummyModule']['RecordStatus'] = 0;
									}
								}
								
								$userAuthCode = $requestJsonAry['eClassRequest']['Request']['AuthCode'];
								if ($sys_custom['eClassApp']['authCode']) {
									if ($userAuthCode == '') {
										$isAuthCodeValid = false;
									}
									else {
										include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_authCode.php');
										$libeClassAppAuthCode = new libeClassApp_authCode();
										$authCodeAry = $libeClassAppAuthCode->getAuthCodeData($curUserId, $useStatus=1, $userAuthCode, $deviceId='', $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']);
										$isAuthCodeValid = (count($authCodeAry) > 0)? true : false;
									}
									
									$result['AuthCodeValid'] = $isAuthCodeValid? '1' : '0';
								}
							}
						}
						else {
							# Blacklist user
							if ($projectName == $eclassAppConfig['appApiProjectName']['S']) {
								$result['eClassStudentAppEnabled'] = '0';
							}
							else {
								$result = '410';
							}
						}
					}
					else {
						# Invalid license
						$result = '402';
					}
				}
				else {
					$result = $method->getErrorArray('5');
				}
				
			} else {
				# Invalid license
				if ($projectName == $eclassAppConfig['appApiProjectName']['S']) {
					$result['eClassStudentAppEnabled'] = '0';
				}
				else {
					$result = '410';
				}
			}
			
			if(!is_array($result)) {
				$result = $method->GetErrorArray($result);	
				$returnResult = 'N';
			}
			else{
				$returnResult = 'Y';
			}
			
			# generate return JSON content
			$returnJsonAry = array();
			$returnJsonAry['RequestID'] = $requestID;
			$returnJsonAry['ReturnResult'] = $returnResult;
			$returnJsonAry['Result'] = $result;
		}
	}
	
	$returnJsonString = $libeClassApp->standardizePushMessageText($jsonObj->encode($returnJsonAry));
	$responseJsonDecryptedString = $returnJsonString;
	
	if ($AESEnabled) {
		$encryptedReturnContent["eClassResponseEncrypted"] = $libaes->encrypt($returnJsonString);
		$returnJsonString = $jsonObj->encode($encryptedReturnContent);
		
		$responseJsonEncryptedString = $returnJsonString;
	}
	
	
	// log request
	if ($requestMethod == 'Login' || $requestMethod == 'LoginAndGetChildrenListForApp' || $requestMethod == 'CentennialLogin') {
		$requestJsonDecryptedString = '';	// because this request has user password
	}
	
	if ($sys_custom['eClassApp']['enableRequestLog']) {
		$runTime = StopTimer($precision=2, $NoNumFormat=false, 'eclassappapiTimer');
		$endApiTime = date("Y-m-d H:i:s");
		
		$successAry['logRequest'] = $libeClassApp->logRequest($requestJsonEncryptedString, $requestJsonDecryptedString, $responseJsonEncryptedString, $responseJsonDecryptedString, $startApiTime, $endApiTime, $runTime);
	}
	
	// For performance tunning info
//	if ($_SERVER["REMOTE_ADDR"]=='10.0.3.129') {
//		echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
//		debug_pr('$runTime = '.$runTime.'s');
//		debug_pr('convert_size(memory_get_usage()) = '.convert_size(memory_get_usage()));
//		debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//		debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
//		$libeClassApp->db_show_debug_log();
//		//$libeClassApp->db_show_debug_log_by_query_number(1);
//		die();
//	}	

	header('Content-Type: application/json; charset=utf-8');
	echo $returnJsonString;
	
}
else {
	$returnJsonAry = array();
	$returnJsonAry['ReturnResult'] = 'Y';
	$returnJsonAry['Result']['eClassAppEnabled'] = $plugin['eClassApp'] ?  '1' : '0';
	$returnJsonAry['Result']['eClassTeacherAppEnabled'] = $plugin['eClassTeacherApp'] ?  '1' : '0';
	
	$enableStudentApp = false;
	if ($plugin['eClassStudentApp']) {
		$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
		$enableStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableStudentApp']);
	}
	$returnJsonAry['Result']['eClassStudentAppEnabled'] = $enableStudentApp ?  '1' : '0';

	// 2016-12-30 Roy: Power lesson 2
	$enablePowerLesson2 = true;
	$returnJsonAry['Result']['powerLesson2Enabled'] = $enablePowerLesson2 ?  '1' : '0';
	
	header('Content-Type: application/json; charset=utf-8');
	$returnJsonString = $jsonObj->encode($returnJsonAry);
	
	echo $returnJsonString;
}

intranet_closedb();

?>