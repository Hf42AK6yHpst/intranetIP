<?php
include_once('../includes/global.php');
include_once($intranet_root.'/includes/json.php');

$jsonContent = trim(stripslashes($_POST['jsonContent']));

$jsonObj = new JSON_obj();
$successAry = array();
$headers = array('Content-Type: application/json');


session_write_close();
$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
curl_setopt($ch, CURLOPT_URL, curPageURL($withQueryString=0, $withPageSuffix=0).'/eclassappapi/');
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonContent);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
$responseJson = curl_exec($ch);
curl_close($ch);

//$responseJson = standardizeFormPostValue($responseJson);
$responseJsonAry = $jsonObj->decode($responseJson);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
		<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
		<script type="text/javascript">
		</script>
	</head>
	<body>
		<? debug_pr($responseJson); ?>
		<? debug_pr($responseJsonAry); ?>
	</body>
</html>