<?php

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."addon/check.php");

intranet_opendb();

$ldb = new libdb();
$linterface = new interface_html();

?>
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
function goSubmit() {
	$('form#form1').submit();
}
</script>
<html>
	<body>
		<form id="form1" name="form1" method="post" action="testCentralServerAPI_update.php">
			<table>
				<tr>
					<td>
						********** JSON Input **********<br>
						<textarea id="jsonContent" name="jsonContent" cols="50" rows="20">{"RequestMethod":"getSchoolListVersion"}</textarea>
						<br>
						<input value="submit" type="button" onclick="goSubmit();">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>