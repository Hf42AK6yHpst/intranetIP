<?php
$PATH_WRT_ROOT = "../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libmethod.php");

# This page can be access by Android Only
if(!$sys_custom['power_lesson_app_web_view_all_platform'] && isset($userBrowser) && $userBrowser->platform!='Andriod'){
	die();
}

intranet_opendb();

# initialize variable
$UserLogin 	  = isset($_REQUEST['UserLogin']) && $_REQUEST['UserLogin']? $_REQUEST['UserLogin'] : '';
$UserPassword = isset($_REQUEST['UserPassword']) && $_REQUEST['UserPassword']? $_REQUEST['UserPassword'] : '';

# initialize object
$method = new LibMethod();

# Check Login
$ParArr['eClassRequest']['Request']['UserName']  = $UserLogin;
$ParArr['eClassRequest']['Request']['Password']  = $UserPassword;
$ParArr['eClassRequest']['Request']['hasAPIKey'] = true;

$LoginResult = $method->callMethod("Login", $ParArr);

if(is_array($LoginResult)){
	# Check PowerLesson Connection
	$ValidateResult = $method->callMethod("AppValidate", array());
	
	if(is_array($ValidateResult)){
		$IsSuccess = true;
		$Result = "'".$ValidateResult['ExpiryDate']."'";
	}
	else{
		if($ValidateResult==802 || $ValidateResult==803){
			$IsSuccess = true;
			$Result = $ValidateResult;
		}
		else{
			$IsSuccess = false;
			$Result = $ValidateResult;
		}
		
		/* Comment out by thomas on 2012-10-25
		$IsSuccess = false;
		$Result = $ValidateResult;
		*/
	}
}
else{
	$IsSuccess = false;
	$Result = $LoginResult;
}

echo '{IsSuccess:'.($IsSuccess? 1:0).', Result:'.$Result.'}';
?>