<?
/*
Using By : 
Create Date: 25 Oct 2008
Modified Date: 28 Oct 2008
Description: Check account exist
*/
/********************************** Change Log ***********************************/
#
#	Date	: 20091211 (Ronald Yeung)
#	Details	: change the userlogin to lower case
#
/******************************* End OF Change Log *******************************/

include("includes/global.php");

$loginID = $_GET['loginID'];
$loginID = strtolower($loginID);

$errorMessage = "";

if (! checkIP($client_ip))
{
	if ($loginID == null) {
        	//print "0, Paratmeter cannot be empty";
		print "0";
	}
	else 
	{
		// Select Database
		$result = $db->Execute("SELECT count(userid) FROM ftpuser where userid = '$loginID'");

		if ($result === false) die("Error in retrieving data");
		if ( $result->fields[0] == 1)
			//print "1,Account Exist!";
			print "1";
		else
			//print "0,Account Not Exist!";
			print "0";
	}
}
else
{
	//print "0, Connection Refused!";
	print "0";
}


?>
