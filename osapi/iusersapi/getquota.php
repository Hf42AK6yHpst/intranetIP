<?
/*
Using By :
Create Date: 25 Oct 2008
Modified Date: 28 Oct 2008
Description: Get user quota
*/
/********************************** Change Log ***********************************/
#
#	Date	: 20091211 (Ronald Yeung)
#	Details	: change the userlogin to lower case
#
/******************************* End OF Change Log *******************************/


include("includes/global.php");

$loginID = $_GET['loginID'];
$loginID = strtolower($loginID);
$errorMessage = "";

if (! checkIP($client_ip))
{
	if ($loginID == null) {
        	//print "0, Paratmeter cannot be empty";
		print "0";
	}
	else 
	{
		// Select Database
		$result = $db->Execute("SELECT bytes_in_avail FROM ftpquotalimits where name = '$loginID'");

		if ($result === false) die("Error in retrieving data");
		//print "1," . $result->fields[0];
		$quota_avail = $result->fields[0] / 1024 / 1024;		// convert quota to Mb format
		print $quota_avail;
	}
}
else
{
	//print "0, Connection Refused!";
	print "0";
}


?>
