<?
/*
Using By :
Create Date: 25 Oct 2008
Modified Date: 28 Oct 2008
Description: Set user quota
*/

/********************************** Change Log ***********************************/
#
#	Date	: 20091211 (Ronald Yeung)
#	Details	: change the userlogin to lower case
#
/******************************* End OF Change Log *******************************/

include("includes/global.php");

$loginID = $_GET['loginID'];
$loginID = strtolower($loginID);
$quota = $_GET['quota'];
$errorMessage = "";

if (! checkIP($client_ip))
{
	if ($loginID == null || $quota == null) {
        	//$errorMessage = "0, Paratmeters cannot be empty";
		$errorMessage = "0";
	}
	else 
	{
		$quota = $quota * 1024 * 1024;		// change quota to Byte format
		// Set user quota
		$sql_quota = "UPDATE ftpquotalimits set bytes_in_avail = \"$quota\" where name = \"$loginID\"";
		if (!($db->Execute($sql_quota))) 
			//$errorMessage = "0, Error updating quota in ftpuser: " . $db->ErrorMsg();
			$errorMessage = "0";
	}
}
else
{
	//$errorMessage = "0, Connection Refused!";
	$errorMessage = "0";
}

if ( $errorMessage ==  "")
	//print "1,Quota update successfully";
	$errorMessage = "1";
else
	print $errorMessage;

?>
