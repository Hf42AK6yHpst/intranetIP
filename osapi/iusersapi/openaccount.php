<?
/*
Using By : 
Create Date: 25 Oct 2008
Modified Date: 28 Oct 2008
Description: Create user and assign quota 
*/

/********************************** Change Log ***********************************/
#	2014-07-04 (Carlos): decrypt password from $Pwd if set and store hashed password into database
#
#	Date	: 20091211 (Ronald Yeung)
#	Details	: change the userlogin to lower case
#
/******************************* End OF Change Log *******************************/

include("includes/global.php");

$loginID = $_GET['loginID'];
$loginID = strtolower($loginID);
$newPassword = $_GET['newPassword'];
$errorMessage = "";

if(isset($pwd) && $pwd != ""){
	$newPassword = decrypt(base64_decode($pwd));
}

//echo $loginID;
//echo $newPassword;

if (! checkIP($client_ip))
{
	if ($loginID == null || $newPassword == null) {
        	//$errorMessage = "0, Paratmeters cannot be empty";
		$errorMessage = "0";
	}
	else 
	{
		$hashed_pwd = doSHA256($newPassword);
		// Insert User record to DB
		$sql_user = "INSERT INTO ftpuser (id,userid,passwd,uid,gid,homedir,shell,count,accessed,modified)";
		$sql_user .= " VALUES ('', '$loginID', '$hashed_pwd', $uid, $gid, '$homedir/$loginID', '$shell', 0, '$timeStamp', '$timeStamp')";
		if (!($db->Execute($sql_user))) {
			//$errorMessage = "0, Error inserting in ftpuser: " . $db->ErrorMsg();
			$errorMessage = "0";
		} 
		else
		{
		// Insert Quota record to DB
		$sql_quota = "INSERT INTO ftpquotalimits (name, quota_type, per_session, limit_type, bytes_in_avail, bytes_out_avail, bytes_xfer_avail, files_in_avail, files_out_avail, files_xfer_avail)";
		$sql_quota .= "VALUES ('$loginID', '$quota_type', 'true', '$limit_type', $bytes_in_avail, $bytes_out_avail, $bytes_xfer_avail, $files_in_avail, $files_out_avail,$files_xfer_avail)";

		if (!($db->Execute($sql_quota))) 
			//$errorMessage = "0, Error inserting in ftpquotalimits: " . $db->ErrorMsg();
			$errorMessage = "0";
		} 
	}
}
else
{
	//$errorMessage = "0, Connection Refused!";
	$errorMessage = "0";
}

if ( $errorMessage ==  ""){
	//print "1,Account add successfully";
	print "1";

}else{
	print $errorMessage;
}

?>
