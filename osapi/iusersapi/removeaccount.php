<?
/*
Using By :
Create Date: 25 Oct 2008
Modified Date: 28 Oct 2008
Description: Delete user and quota record at Database
*/

/********************************** Change Log ***********************************/
#
#	Date	: 20091211 (Ronald Yeung)
#	Details	: change the userlogin to lower case
#
/******************************* End OF Change Log *******************************/

include("includes/global.php");

$loginID = $_GET['loginID'];
$loginID = strtolower($loginID);
$errorMessage = "";

if (! checkIP($client_ip))
{
	if ($loginID == null) {
        	//$errorMessage = "0, Paratmeter cannot be empty";
		$errorMessage = "0";
	}
	else 
	{
		// Delete User record 
		$sql_user = "DELETE FROM ftpuser where userid = \"$loginID\"";
		if (!($db->Execute($sql_user))) {
			//$errorMessage = "0, Error deleting in ftpuser: " . $db->ErrorMsg();
			$errorMessage = "0";
		} 
		else
		{
			// Delete Quota record
			$sql_quota = "DELETE FROM ftpquotalimits where name = \"$loginID\"";
			if (!($db->Execute($sql_quota))){ 
				//$errorMessage = "0, Error deleting in ftpquotalimits: " . $db->ErrorMsg();
				$errorMessage = "0";
			}
			else
			{
				$dir = escapeshellcmd("/home/iusers/".$loginID);
				shell_exec("sudo /bin/rm -r $dir");
			}
		}
		
	}
}
else
{
	//$errorMessage = "0, Connection Refused!";
	$errorMessage = "0";
}

if ( $errorMessage ==  "")
	//print "1,Account remove successfully";
	print "1";
else
	print $errorMessage;

?>