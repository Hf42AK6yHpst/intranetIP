<?php
// Editing by 
/*
 * @param $module : iMail or iFolder
 * @param $oldAccount : old email or account name
 * @param $newAccount : new email or account name
 * @param $secret : secret key
 * 
 * @return : "1" success, otherwise fail
 */
include_once("includes/global.php");
include_once("includes/liblinux.php");

$client_ip = $_SERVER['REMOTE_ADDR'];

if (checkSecretMatched($client_ip,$secret))
{
	$module = strtolower($_REQUEST['module']);
	$oldAccount = $_REQUEST['oldAccount'];
	$newAccount = $_REQUEST['newAccount'];
	
	$llinux = new liblinux($actype);
	$result = $llinux->changeAccountName($oldAccount, $newAccount, $module);
	/*
	if($module == "ifolder"){
		$result = shell_exec("/usr/bin/sudo asavapi/tools/changeIfolderAccountName.sh $oldAccount $newAccount");
	}else{
		$result = shell_exec("/usr/bin/sudo asavapi/tools/changeAccountName.sh $oldAccount $newAccount");
	}
	*/
	echo $result ? "1" : "0";
}else{
	echo "0";
}
?>