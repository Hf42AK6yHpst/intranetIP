<?php
/*
 * 2013-11-28 (Carlos): Created
 */
include_once("includes/global.php");
include_once("includes/liblinux.php");


$rule = $_REQUEST['Rule'];
$score = $_REQUEST['Score'];
$actype = $_REQUEST['actype'];
$type = $_REQUEST['Type'];

$llinux = new liblinux($actype);

$dataAry = array();
$data_count = count($rule);
for($i=0;$i<$data_count;$i++){
	$dataAry[] = array('rule' => $rule[$i],'score' => $score[$i]);
}

$result = $llinux->setAvasRules($dataAry,$type);

echo $result? "1":"0"; // 1: true , 0: false
?>
