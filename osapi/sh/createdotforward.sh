#!/bin/sh

# Param:
# target file path, file content, user

if [ $# -ne 3 ]; then
	echo 0
	exit 0
fi

target_file="$1/.forward"

echo $2 > $target_file
chown $3.$3 $target_file
