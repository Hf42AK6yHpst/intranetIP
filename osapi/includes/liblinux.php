<?php
// Editing by 
# Abstract class containing functions on Linux account management on local machine Write for FTP and EMail module
/******************************************* Change Log *****************************************************************
 * 2014-07-04 by Carlos: Added decrypt() and unpad() 
 * 2014-05-05 by Carlos: Added setSuspendUser() and setUnsuspendUser()
 * 2013-12-11 by Carlos: Added changeAccountName()
 * 2013-11-28 by Carlos: Added setAvasRules(), getSaString(), writeSaFile()
 * 2012-08-20 by Carlos: modified setMailForward(), for postfix type, do not call remove_mail_forward.pl here to allow set multiple forward emails
 * 2011-02-28 by Carlos: Merged iFolder api
 * 2011-02-25 by Carlos: Added validate_userlogin() which called in openLinuxAccount() and removeLinuxAccount() 
 * 						 to prevent passing in empty loginID.
 * 						 Escape shell command special chars in password at openLinuxAccount() and changeLinuxPassword().
 ************************************************************************************************************************/

if (!defined("LIBLINUX_DEFINED")) // Preprocessor directive 
{ 
define("LIBLINUX_DEFINED", true);

if
(!isset($usr_home_dir) || $usr_home_dir=="") {
     $usr_home_dir = "/home";
}


class liblinux {
        var $actype;
        function liblinux($act="")
        {
                $this->actype = $act;
				define("IUSERSAPI_PATH", "iusersapi");
				define("LIBLINUX_PATH", "asavapi/tools");
        }
        // Attempt to create an user account.
        // Return TRUE on succeed and FALSE on error.
        function openLinuxAccount($newLogin,$newPassword,$module="")
        {
        		global $usr_home_dir;
				global $api_path;
				
				if(!$this->validate_userlogin($newLogin)) return false;
        		$newPassword = escapeshellcmd($newPassword);
				
                if ($this->actype == 'postfix')
                {
                	if($module == "iMail"){
	                	// Check if user already exists
		                $newLogin = strtolower($newLogin);
	        	        if ($this->isLinuxAccountExist($newLogin, $module))
	                	{
			                return false;
	                	}
		                // Create User
						$newLogin = escapeshellcmd($newLogin);
	        	        $path = LIBLINUX_PATH . "/openLinuxAccount.pl";
	                	$result = shell_exec("/usr/bin/sudo $path $newLogin $newPassword");
	
	        	        $path2 = LIBLINUX_PATH . "/addMailfilter_single.sh";
	                	$result2 = shell_exec("/usr/bin/sudo $path2 $newLogin");
						return true;
                	}elseif($module == "iFolder"){
						$newLogin = strtolower($newLogin);
						if ($this->isLinuxAccountExist($newLogin, $module)){
								return false;
						}
						$newLogin = escapeshellcmd($newLogin);
						$iuserspath = $api_path."/".IUSERSAPI_PATH."/openaccount.php";
						if(version_compare( phpversion(), '5', '<' ))
						{
								// if php version is 5.0 before
				    			$result = shell_exec("php -q $iuserspath loginID=$newLogin newPassword=$newPassword");
						} else {
								// if php version is 5.0 or later
								$result = shell_exec("php-cgi -q $iuserspath loginID=$newLogin newPassword=$newPassword");
						}
						return $result;
					}
               }
               else
               {
               		global $usr_home_dir;
					# Fill code of create new account
					$newLogin = strtolower($newLogin);
	                // Check if user already exists
        	        if ($this->isLinuxAccountExist($newLogin, $module))
                	{
		                return false;
					}

                	// Create User
					$newLogin = escapeshellcmd($newLogin);
                	shell_exec("sudo /usr/sbin/useradd $newLogin -s /sbin/nologin -d $usr_home_dir/$newLogin");

                	// Change mode of home directory
                	shell_exec("sudo /bin/chmod 711 $usr_home_dir/$newLogin");

                	// Change Password
                	$exec = shell_exec("echo \"$newPassword\" | sudo /usr/bin/passwd --stdin $newLogin");
                	return (strpos($exec, "passwd: all authentication tokens updated successfully.") !== false);
                }
        }

        function removeLinuxAccount($login,$module)
        {
        		global $api_path;
        		
        		if(!$this->validate_userlogin($login)) return false;
				$login= strtolower($login);
				
                # Fill code of remove account
                if ($this->actype == 'postfix')
                {
                	if($module == "iMail")
                	{
		                $login = escapeshellcmd(strtolower($login));
	        	        $path = LIBLINUX_PATH . "/removeLinuxAccount.pl";
		                $result = shell_exec("/usr/bin/sudo $path $login");
						return $result;
                	}elseif($module == "iFolder")
					{
						if(strrpos($login,"@")){
								$new_login = substr($login,0,strpos($login,"@"));
						}else{
								$new_login = $login;
						}
						$new_login = escapeshellcmd($new_login);
						$iuserspath = $api_path."/".IUSERSAPI_PATH."/removeaccount.php";
						if(version_compare( phpversion(), '5', '<' ))
						{
								// if php version is 5.0 before
				       			$result = shell_exec("php -q $iuserspath loginID=$new_login");
						} else {
								// if php version is 5.0 or later
					       		$result = shell_exec("php-cgi -q $iuserspath loginID=$new_login");
						}
						return $result;
					}
                }
                else
                {
					$login = escapeshellcmd($login);
                    $response = shell_exec("sudo /usr/sbin/userdel -r $login");
                    if (strpos($response, "userdel: user $username does not exist") !== false)
                    {
                        return false;
                    }
                    else return true;
                }
        }
		
        // Attempt to change the password of the given $login.
        // Returns TRUE on succeed, FALSE on failed.
        function changeLinuxPassword($login,$newPassword, $module)
        {
        		global $api_path;
        		
        		$newPassword = escapeshellcmd($newPassword);
				$login = strtolower($login);
                # Fill code of changing password
                if ($this->actype == 'postfix')
                {
	                if($module == "iMail")
	                {
	                	$path = LIBLINUX_PATH . "/changeLinuxPassword.pl";
						$login = escapeshellcmd($login);
                        // Add by Gary (24 April 2009)
                        //$convertPassword = ereg_replace('(\$)', '\$', $newPassword);
                        $result = shell_exec("/usr/bin/sudo $path $login $newPassword");
                		return $result;
	                }elseif($module == "iFolder"){
						if(strrpos($login,"@")){
								$new_login = substr($login,0,strpos($login,"@"));
						}else{
								$new_login = $login;
						}
						$new_login = escapeshellcmd($new_login);
						$iuserspath =  $api_path."/".IUSERSAPI_PATH."/changepassword.php";
						if(version_compare( phpversion(), '5', '<' ))
						{
								// if php version is 5.0 before
								$result = shell_exec("php -q $iuserspath loginID=$new_login newPassword=$newPassword");
						} else {
								// if php version is 5.0 or later 
								$result = shell_exec("php-cgi -q $iuserspath loginID=$new_login newPassword=$newPassword");
						}
						return $result;
					}
                }
                else
                {
                    # return result
					$login = escapeshellcmd($login);
                    $exec = shell_exec("echo $newPassword | sudo /usr/bin/passwd --stdin $login");
                    return (strpos($exec, "passwd: all authentication tokens updated successfully.") !== false);
                }
        }

        // Return TRUE if the specified user exists, FALSE otherwise.
        function isAliasExist($login)
        {
                if ($this->actype == 'postfix')
                {
                        $path = LIBLINUX_PATH . "/isAliasExist.pl";
						$login = escapeshellcmd($login);
                        $result = shell_exec("/usr/bin/sudo $path $login");

                        if ($result) {
                                return true;    #Alias exist!
                        } else {
                                return false;   #Alias didn't exist!
                        }
                }
        }


        // Return TRUE if the specified user exists, FALSE otherwise.
        function isLinuxAccountExist($login, $module)
        {
        		global $api_path;
				$login = strtolower($login);
        	
                if ($this->actype == 'postfix')
                {
                	if($module == "iMail")
                	{
		                $path = LIBLINUX_PATH . "/isLinuxAccountExist.pl";
						$login = escapeshellcmd($login);
	        	        $result = shell_exec("/usr/bin/sudo $path $login");
	
		               	if ($result) {
	  	             		return true;	#Account exist!
		                } else {
		             		return false;	#Account didn't exist!
		               	}
	         		}elseif ($module == "iFolder")
					{
						if(strrpos($login,"@")){
								$new_login = substr($login,0,strpos($login,"@"));
						}else{
								$new_login = $login;
						}
						$new_login = escapeshellcmd($new_login);
						$iuserspath = $api_path."/".IUSERSAPI_PATH."/checkaccount.php";
						if(version_compare( phpversion(), '5', '<' ))
						{
								// if php version is 5.0 before
								$result = shell_exec("php -q $iuserspath loginID=$new_login");
						} else {
								// if php version is 5.0 or later
								$result = shell_exec("php-cgi -q $iuserspath loginID=$new_login");
						}
						if ($result) {
								return true;	#Account exist!
						} else {
								return false;	#Account didn't exist!
						}
					}
                }
                else
                {

                    $response = shell_exec("sudo /usr/bin/passwd ".escapeshellcmd($login));
                    if (strpos($response, "Changing password for user $login") !== false)
                    {
                        return true;
                    }
                    return false;
                }
        }


        function getTotalQuota($login, $module)
        {
        		global $api_path;
				$login = strtolower($login);
                
                if ($this->actype == 'postfix')
                {
                	if($module == "iMail")
                	{
	        	        $path = LIBLINUX_PATH . "/getTotalQuota.pl";
						$login = escapeshellcmd($login);
	                	$result = shell_exec("/usr/bin/sudo $path $login");
		                return $result;
                	}elseif($module == "iFolder")
					{
						if(strrpos($login,"@")){
								$new_login = substr($login,0,strpos($login,"@"));
						}else{
								$new_login = $login;
						}
						$new_login = escapeshellcmd($new_login);
						$iuserspath = $api_path."/".IUSERSAPI_PATH."/getquota.php";
						if(version_compare( phpversion(), '5', '<' ))
						{
								// if php version is 5.0 before
								$result = shell_exec("php -q $iuserspath loginID=$new_login");
						} else {
								// if php version is 5.0 or later
								$result = shell_exec("php-cgi -q $iuserspath loginID=$new_login");
						}
						return $result;
					}
                }
                else
                {
                    global $usr_home_dir;
					$login = escapeshellcmd($login);
                    $command = "sudo /usr/bin/quota $login";
                    $line = exec($command,$output);
                    $data = explode(":",$line);
                    $value = trim($data[0]);
                    if ($value == "none") return "";
                    $data = preg_split("/[\s\t\n\r]/",$line);
                    for ($i=0; $i<sizeof($data); $i++)
                    {
                         if (trim($data[$i])!="")
                             $array[] = trim($data[$i]);
                    }
                    $value = round($array[2]/1024);
                    return $value;
                }
        }


        function getQuotaTable($module="")
        {
        		global $api_path;
                global $system_reserved_account;
                
                 if ($this->actype == 'postfix')
                 {
                 		if($module == "iMail")
                 		{
	                        $domain = strtolower($domain);
							$path = LIBLINUX_PATH . "/getQuotaTable.pl";
	                		$result = shell_exec("/usr/bin/sudo $path");
							return $result;
                 		}elseif($module == "iFolder") {
							$iuserspath = $api_path."/".IUSERSAPI_PATH."/quotalist.php";
							if(version_compare( phpversion(), '5', '<' ))
							{
									// if php version is 5.0 before
									$result = shell_exec("php -q $iuserspath");
							} else {
									// if php version is 5.0 or later
									$result = shell_exec("php-cgi -q $iuserspath");
							}
							return $result;
						}
                 }
                 else
                 {
                     $command = "sudo /usr/sbin/repquota -a";
                     exec($command, $output);
                     $found = false;
                     $result = array();
                     for ($i=0; $i<sizeof($output); $i++)
                     {
                          $line = $output[$i];
                          if (!$found)
                          {
                               if (strpos($line,"---------------------")!==false)
                               {
                                   $found = true;
                               }
                          }
                          else
                          {
                              $data = preg_split("/[\s\t\n\r]/",$line);
                              $array = array();
                              for ($j=0; $j<sizeof($data); $j++)
                              {
                                   if (trim($data[$j])!="")
                                   {
                                       $array[] = trim($data[$j]);
                                   }
                              }
                              list($login,$delimiter,$used,$soft,$hard) = $array;
                              $used = round($used/1024);
                              $soft = round($soft/1024);
                              $hard = round($hard/1024);
                              if (!in_array($login,$system_reserved_account) && $login != "")
                              {
                                   $result[] = array($login,$delimiter,$used,$soft,$hard);
                              }
                          }
                     }
                 }
                 return $result;
        }


        function getUsedQuota($login, $module)
        {
        		global $api_path;		
				$login = strtolower($login);
				
                 if ($this->actype == 'postfix')
                 {
                 	if($module == "iMail")
                 	{
	                	$path = LIBLINUX_PATH . "/getUsedQuota.pl";
						$login = escapeshellcmd($login);
	        	        $result = shell_exec("/usr/bin/sudo $path $login");
		                return $result;
                 	}elseif($module == "iFolder"){
						if(strrpos($login,"@")){
							$new_login = substr($login,0,strpos($login,"@"));
						}else{
							$new_login = $login;
						}
						$new_login = escapeshellcmd($new_login);
						$iuserspath = $api_path."/".IUSERSAPI_PATH."/getusedquota.php";
						if(version_compare( phpversion(), '5', '<' ))
						{
							// if php version is 5.0 before
							$result = shell_exec("php -q $iuserspath loginID=$new_login");
						} else {
							// if php version is 5.0 or later
							$result = shell_exec("php-cgi -q $iuserspath loginID=$new_login");
						}
						return $result;
					}
                 }
                 else
                 {
                     $command = "sudo /usr/sbin/repquota -a | grep ".escapeshellcmd($login);
                     exec($command, $output);
                     for ($i=0; $i<sizeof($output); $i++)
                     {
                          $line = $output[$i];
                          $data = preg_split("/[\s\t\n\r]/",$line);
                          $array = array();
                          for ($j=0; $j<sizeof($data); $j++)
                          {
                               if (trim($data[$j])!="")
                               {
                                   $array[] = trim($data[$j]);
                               }
                          }
                          #list($login,$delimiter,$used,$soft,$hard) = $array;
			  list($userlogin,$delimiter,$used,$soft,$hard) = $array;
                          $used = round($used/1024);
                          $soft = round($soft/1024);
                          $hard = round($hard/1024);
                          if ($login==$userlogin) return $used;
                     }
                     return false;
                 }
        }

        function getUsedQuota1($login)
        {
                 global $usr_home_dir;
				 $login = escapeshellcmd($login);
                 $command = "sudo /usr/bin/du -sh $usr_home_dir/$login";
                 $line = exec($command);
                 $data = preg_split("/[\s\t\n\r]/", $line);
                 $storage = trim($data[0]);
                 $unit = strtolower(substr($storage,-1));
                 $value = substr($storage, 0,strlen($storage)-1);
                 if ($unit == "k")
                 {
                     $meg = round($value/1000);
                 }
                 if ($unit == "m")
                 {
                     $meg = $value;
                 }
                 return $meg;
        }


        function setTotalQuota($login, $quota, $module)
        {		
        		global $api_path;
				global $home_dir_partition;
						
				$login = strtolower($login);
				$quota = escapeshellcmd($quota);
				
                 if ($this->actype == 'postfix')
                 {
                 	if($module == "iMail")
                 	{
		                $login = strtolower($login);
						$login = escapeshellcmd($login);
	                	$path = LIBLINUX_PATH . "/setTotalQuota.pl";
	        	        $result = shell_exec("/usr/bin/sudo $path $login $quota");
		                return $result;
                 	}elseif($module == "iFolder"){
						if(strrpos($login,"@")){
								$new_login = substr($login,0,strpos($login,"@"));
						}else{
								$new_login = $login;
						}
						$new_login = escapeshellcmd($new_login);
						$iuserspath = $api_path."/".IUSERSAPI_PATH."/setquota.php";
						if(version_compare( phpversion(), '5', '<' ))
						{
								// if php version is 5.0 before
								$result = shell_exec("php -q $iuserspath loginID=$new_login quota=$quota");
						} else {
								// if php version is 5.0 or later
								$result = shell_exec("php-cgi -q $iuserspath loginID=$new_login quota=$quota");
						}
						return $result;
					}
                 }
                 else
                 {
                     global $usr_home_dir;
                     $hard_quota = $quota;
                     $command = "sudo /usr/local/bin/quotatool -u $login -bq $quota"."M -l \"$hard_quota Mb\" $home_dir_partition";
                     $output = array();
                     $line = exec($command, $output);
                     if($line == "")
					 {
						$command = "sudo /usr/bin/quotatool -u $login -bq $quota"."M -l \"$hard_quota Mb\" $home_dir_partition";
						$output = array();
						$line = exec($command, $output);
					 }
                     return true;
                 }
        }


        function setMailForward($login,$recipient,$type)
        {
                if ($this->actype == 'postfix')
                {
        	        // Check if user already exists
                	$login = strtolower($login);
                    //$path = LIBLINUX_PATH . "/removeMailForward.pl";
                    //shell_exec("/usr/bin/sudo $path $login");

        	        $path = LIBLINUX_PATH . "/setMailForward.pl";
                	$result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$login $recipient $type"));
	                return true;
	        	}else
	        	{
        			global $usr_home_dir;
     				global $api_path;
     												                 				
     				$login = strtolower($login);
	                # write file
	                $forward_filepath = "$usr_home_dir/$login";
	                $filecontent = "";
	                //if ($keepCopy)
	                if($type)
	                {
	                    $filecontent = "\\$login,\r\n";
	                }
	                $filecontent .= "$recipient\r\n";
	                #write_file_content($filecontent, $forward_filepath);
	                # Execute Shell script
	                $command = "sudo $api_path/sh/createdotforward.sh \"$forward_filepath\" \"$filecontent\" \"$login\"";
	                $output = array();
	                $line = exec($command, $output);
	                return true;
	        	}
		}


        function removeMailForward($login)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/removeMailForward.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $login");
                	return $result;
				}else
				{
						global $usr_home_dir;
		                global $api_path;
										                
		                $login = strtolower($login);
						$login = escapeshellcmd($login);
		                # write file
		                $forward_filepath = "$usr_home_dir/$login";
		                $command = "sudo $api_path/sh/removedotforward.sh \"$forward_filepath\"";
		                $output = array();
		                $line = exec($command, $output);
		                return true;
				}
        }


       	function getMailForward($login)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);
					$login = escapeshellcmd($login);
        	        $path = LIBLINUX_PATH . "/getMailForward.pl";
                	$result = shell_exec("/usr/bin/sudo $path $login");
	                return $result;
				}else
				{
						global $usr_home_dir;						
														
						$login = strtolower($login);
						$forward_filepath = "$usr_home_dir/$login";
						$content = get_file_content($forward_filepath."/.forward");
						return $content;
				}
        }


     	function addWhiteList($login)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);
	                $path = LIBLINUX_PATH . "/addWhiteList.pl";
        	        $result = shell_exec("/usr/bin/sudo $path \"$login\"");
                	return $result;
				}
        }


      	function readWhiteList()
        {
                if ($this->actype == 'postfix')
                {
	                $path = LIBLINUX_PATH . "/readWhiteList.pl";
        	        $result = shell_exec("/usr/bin/sudo $path");
                	return $result;
				}
        }


      	function removeWhiteListRecord($login)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/removeWhiteListRecord.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $login");
                	return $result;
				}
        }


      	function addBlackList($login)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);
	                $path = LIBLINUX_PATH . "/addBlackList.pl";
        	        $result = shell_exec("/usr/bin/sudo $path \"$login\"");
                	return $result;
				}
        }


      	function readBlackList()
        {
                if ($this->actype == 'postfix')
                {
	                $path = LIBLINUX_PATH . "/readBlackList.pl";
        	        $result = shell_exec("/usr/bin/sudo $path");
                	return $result;
				}
        }


      	function removeBlackListRecord($login)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/removeBlackListRecord.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $login");
                	return $result;
				}
        }


		function bypassSpamCheck($policy,$onOff)
        {
                if ($this->actype == 'postfix')
                {
	                $path = LIBLINUX_PATH . "/bypassSpamCheck.pl";
	                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$policy $onOff"));
                	return $result;
				}
        }


		function bypassVirusCheck($policy, $onOff)
        {
                if ($this->actype == 'postfix')
                {
	                $path = LIBLINUX_PATH . "/bypassVirusCheck.pl";
					$result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$policy $onOff"));
                	return $result;
				}
        }


        function addAlias($login,$recipient)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);

//                        if ($this->isAliasExist($login))
//                        {
//                                #echo "Cannot create alias : user alias already exists.";
//                                return "Alias Exist";
//                        }

                        $path = LIBLINUX_PATH . "/removeAlias.pl";
                        shell_exec("/usr/bin/sudo $path ".escapeshellcmd($login));

	                $path = LIBLINUX_PATH . "/addAlias.pl";
        	        $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$login $recipient"));

					return $result;
				}
        }


        function getAliasInfo($login)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/getAliasInfo.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $login");
                	return $result;
		}
        }


        function getAliasList()
        {
                if ($this->actype == 'postfix')
                {
	                $path = LIBLINUX_PATH . "/getAliasList.pl";
        	        $result = shell_exec("/usr/bin/sudo $path");
                	return $result;
		}
        }


        function removeAlias($login)
        {
                if ($this->actype == 'postfix')
                {
	                $login = strtolower($login);
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/removeAlias.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $login");
                	return $result;
		}
        }


       function addAutoreply($emailaddress,$text)
        {
                if ($this->actype == 'postfix')
                {
					$emailaddress = escapeshellcmd($emailaddress);
	                $path = LIBLINUX_PATH . "/addAutoreply.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $emailaddress \"$text\"");
                	return $result;
		}
        }

       function addAliasAutoreply($emailaddress,$aliasaddress,$text)
        {
                if ($this->actype == 'postfix')
                {
					$emailaddress = escapeshellcmd($emailaddress);
					$aliasaddress = escapeshellcmd($aliasaddress);
	                $path = LIBLINUX_PATH . "/addAliasAutoreply.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $emailaddress $aliasaddress \"$text\"");
                	return $result;
		}
        }

       function removeAliasAutoreply($login,$alias)
        {
                if ($this->actype == 'postfix')
                {
                        $path = LIBLINUX_PATH . "/removeAliasAutoreply.pl";
                        $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$login $alias"));
                        return $result;
                }
        }

       function readAutoreply($login)
        {
                if ($this->actype == 'postfix')
                {
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/readAutoreply.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $login");
                	return $result;
		}
        }

       function removeAutoreply($login)
        {
                if ($this->actype == 'postfix')
                {
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/removeAutoreply.pl";
        	        $result = shell_exec("/usr/bin/sudo $path $login");
                	return $result;
		}
        }


        // 7 May 2009 (Gary)
        function setGroupMailForward($login,$recipient,$type)
        {
                if ($this->actype == 'postfix')
                {
                        // Check if user already exists
                        $login = strtolower($login);
                        $path = LIBLINUX_PATH . "/removeGroupMailForward.pl";
                        //shell_exec("/usr/bin/sudo $path $login");

                        $path = LIBLINUX_PATH . "/setGroupMailForward.pl";
                        $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$login $recipient $type"));
                        return true;
                }
        }

        // 7 May 2009 (Gary)
        function removeGroupMailForward($login)
        {
                if ($this->actype == 'postfix')
                {
                        $login = strtolower($login);
						$login = escapeshellcmd($login);
                        $path = LIBLINUX_PATH . "/removeGroupMailForward.pl";
                        $result = shell_exec("/usr/bin/sudo $path $login");
                        return $result;
                }
        }

        // 6 Jan 2010 (Gary)
        function changeReturnPath($emailaddress)
        {
                if ($this->actype == 'postfix')
                {
                        $emailaddress = escapeshellcmd($emailaddress);
                        $path = LIBLINUX_PATH . "/changeReturnPath.pl";
                        $result = shell_exec("/usr/bin/sudo $path $emailaddress");
                        return $result;
                }
        }

        function removeReturnPath($emailaddress)
        {
                if ($this->actype == 'postfix')
                {
                        $emailaddress = escapeshellcmd($emailaddress);
                        $path = LIBLINUX_PATH . "/removeReturnPath.pl";
                        $result = shell_exec("/usr/bin/sudo $path $emailaddress");
                        return $result;
                }
        }


        function addBlockExternalEmail($login,$domain)
        {
        	if ($this->actype == 'postfix')
            {
            	$path = LIBLINUX_PATH . "/addBlockExternalEmail.pl";
                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$login $domain"));
                return $result;
	        }
		}

        function removeBlockExternalEmail($login)
		{
	        if ($this->actype == 'postfix')
	        {
				$login = escapeshellcmd($login);
	            $path = LIBLINUX_PATH . "/removeBlockExternalEmail.pl";
	            $result = shell_exec("/usr/bin/sudo $path $login");
	            return $result;
	        }
		}

        function searchEmail($keyword)
		{
			if ($this->actype == 'postfix')
            {
				$keyword = escapeshellcmd($keyword);
            	$path = LIBLINUX_PATH . "/searchEmail.pl";
                $result = shell_exec("/usr/bin/sudo $path $keyword");
                return $result;
            }
		}

		function deleteEmail($key, $file, $emailArray)
        {
        	if ($this->actype == 'postfix')
            {
            	$path = LIBLINUX_PATH . "/deleteEmail.pl";
                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$key $file $emailArray"));
                return $result;
            }
        }
		
        function addGroupBlockExternalEmail($loginArray,$domain)
		{
			if ($this->actype == 'postfix')
            {
                $separator = ",";
                $elements = explode($separator, $loginArray);
                $path = LIBLINUX_PATH . "/addBlockExternalEmail.pl";

                for ($i = 0; $i < count($elements); $i++) {
  	              $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$elements[$i] $domain"));
                }
                return $result;
            }
		}

		function removeGroupBlockExternalEmail($loginArray)
		{
			if ($this->actype == 'postfix')
            {
               $separator = ",";
               $elements = explode($separator, $loginArray);
               $path = LIBLINUX_PATH . "/removeBlockExternalEmail.pl";

               for ($i = 0; $i < count($elements); $i++) {
                   $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd($elements[$i]));
               }
               return $result;
            }
		}

        function changeDomainSpamScore($domain,$score)
        {
	        if ($this->actype == 'postfix')
            {
    	        $path = LIBLINUX_PATH . "/changeDomainSpamScore.sh";
                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$domain $score"));
            	return $result;
            }
        }

	// 20 Sept 2010 (Gary)
        function changeAllUserPolicy($policy)
        {
 	       	if ($this->actype == 'postfix')
        	{
            	$path = LIBLINUX_PATH . "/changeAllUserPolicy.pl";
                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd($policy));
                return $result;
            }
		}

		function changeCustomPolicyScore($policy,$score)
        {
        	if ($this->actype == 'postfix')
            {
            	$path = LIBLINUX_PATH . "/changeCustomPolicyScore.pl";
                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$policy $score"));
                return $result;
            }
		}
		
        function getSpamScore($policy)
        {
        	if ($this->actype == 'postfix')
            {
            	$path = LIBLINUX_PATH . "/getSpamScore.pl";
                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd($policy));
                return $result;
            }
		}

		function readSpamStatus($policy)
		{
        	if ($this->actype == 'postfix')
            {
                $path = LIBLINUX_PATH . "/readSpamStatus.pl";
                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd($policy));
                return $result;
            }
        }

        function readVirusStatus($policy)
        {
        	if ($this->actype == 'postfix')
            {
                $path = LIBLINUX_PATH . "/readVirusStatus.pl";
                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd($policy));
                return $result;
            }
        }


	# 20101118
        function getSingleQuota($login)
        {
			if ($this->actype == 'postfix')
	        {
            	$final_result = "";
                $separator = ",";
                $elements = explode($separator, $login);
                $path = LIBLINUX_PATH . "/getSingleQuota.pl";

                for ($i = 0; $i < count($elements); $i++) {
	                $result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd($elements[$i]));
                        $final_result .= $result . "<BR>";
                }
                return $final_result;
			}
		}
		
		### added on 20110208
		function setBatchQuota($login,$quota)
		{
			if($this->actype == 'postfix')
			{
				$module = "iMail";
				$update_result = array();
				$separator = ",";
				$logins = explode($separator, $login);
				$quotas = explode($separator, $quota);
				
				for($i=0;$i<sizeof($logins);$i++){
					//if setTotalQuota fail will output error
					if($this->setTotalQuota($logins[$i], $quotas[$i],$module)){
						$update_result[] = "0"; // fail
					}else{
						$update_result[] = "1"; // success
					}
				}
				return implode(",",$update_result);
			}
		}

		# verify the user login ID match the pattern userlogin[@domainname] to prevent passing in empty login ID
		function validate_userlogin($login)
		{
			if(preg_match('/^([^@])+(@([^@]+))?$/i',$login))
				return true;
			else
				return false;
		}
		
		 /**
		 * Generate header or body of  spamassassin rules.
		 * @param string $type there are two types :  header or body.
		 * @param string $charset the charset of the spamassassin rule file.
		 * @param string $count the counter to identify the rules name.
		 * @param int $score the  spamassassin rule score.
		 * @return string $data the string of sa rules.
		 */
		function getSaString($type,$charset,$count,$name,$score)
		{
			if($type=="header"){
				#subject
				$data  = $type." BL_SUBJECT_".$charset."_".$count."   Subject =~ /".$name."/\n";
				$data .= "describe BL_SUBJECT_".$charset."_".$count." \"Subject contains ".$name."\"\n";
				$data .= "score BL_SUBJECT_".$charset."_".$count."    ".$score."\n";
				$data .= "\n";
			}else{
				#Body
				$data  = $type." BL_BODY_".$charset."_".$count."   /".$name."/i\n";
				$data .= "describe BL_BODY_".$charset."_".$count." \"Message body contains ".$name."\"\n";
				$data .= "score BL_BODY_".$charset."_".$count."    ".$score."\n";
				$data .= "\n";
			}
			return $data;
		}
		
		 /**
		 * Write spamassassin rule to the *.cf files
		 * @param string $file the file to save spamassassin rule.
		 * @param array $row the array of spamassassin rule from database
		 * @param string $type there are two types :  header or body.
		 */
		function writeSaFile($file,$row,$type)
		{
			#counters
			$i=1; # UTF8
			$j=1; # GBK 
			$k=1; # BIG5
			
		//	echo "<pre>";
			
			$UTF8_FILE = $file."_".strtoupper($type)."_UTF8.cf";
			//if(file_exists($UTF8_FILE)){
			//	@unlink($UTF8_FILE);
			//}
			$OF_UTF8 = fopen($UTF8_FILE, 'w') or die("can't open file");
			
			$BIG5_FILE = $file."_".strtoupper($type)."_BIG5.cf";
			//if(file_exists($BIG5_FILE)){
			//	@unlink($BIG5_FILE);
			//}
			$OF_BIG5 = fopen($BIG5_FILE, 'w') or die("can't open file");
			
			$GBK_FILE = $file."_".strtoupper($type)."_GBK.cf";
			//if(file_exists($GBK_FILE)){
			//	@unlink($GBK_FILE);
			//}
			$OF_GBK = fopen($GBK_FILE, 'w') or die("can't open file");
			
			//CVarDumper::dump($row);
			
			foreach ($row as $v) {
				
				$word = $v['rule'];
				## WIRTE UTF8 FILE ###############
				if($type=="subject"){
					fwrite($OF_UTF8, $this->getSaString("header","UTF8",$i,$v['rule'],$v['score']));
				}else{
					fwrite($OF_UTF8, $this->getSaString("body","UTF8",$i,$v['rule'],$v['score']));
				}
				
				$i++;
			
				//CVarDumper::dump($v);
				
				# Important : Compare codding to find out GBK words
				$strGbk = iconv("UTF-8", "GBK//IGNORE", $word);
				$strGb2312 = iconv("UTF-8", "GB2312//IGNORE", $word);
			
				
				if (preg_match('/^[A-Za-z0-9]+$/', $word)){
					//echo "English do noting";
				}
				else if ($strGbk == $strGb2312) {
					if($type=="subject"){
						fwrite($OF_GBK, iconv("UTF-8", "GB2312//IGNORE",$this->getSaString("header","GBK",$j,$v['rule'],$v['score'])));
					}else{
						fwrite($OF_GBK, iconv("UTF-8", "GB2312//IGNORE",$this->getSaString("body","GBK",$j,$v['rule'],$v['score'])));
					}
					$j++;
				} else {
					//echo 'Traditional Chinese';
					//$word  = iconv("UTF-8", "BIG5//IGNORE", $word) ;
					if($type=="subject"){
						fwrite($OF_BIG5, iconv("UTF-8", "BIG5//IGNORE",$this->getSaString("header","BIG5",$k,$v['rule'],$v['score'])));
					}else{
						fwrite($OF_BIG5, iconv("UTF-8", "BIG5//IGNORE",$this->getSaString("body","BIG5",$k,$v['rule'],$v['score'])));
					}
					$k++;
				}
			}
			
			fclose($OF_UTF8);
			fclose($OF_BIG5);
			fclose($OF_GBK);
			
			//echo "</pre>";
		}
		
		/*
		 * @param 2D array $dataAry : e.g. $dataAry[0]=array('rule'=>'rule1','score'=>0)
		 * @param $type : 'subject' or 'body'
		 */
		function setAvasRules($dataAry,$type)
		{
			/*
			$fh = fopen("/tmp/debug.txt","w");
			if($fh){
				for($i=0;$i<count($dataAry);$i++){
					fwrite($fh,"&Rule[$i]=".$dataAry[$i]['rule']."&Score[$i]=".$dataAry[$i]['score']);
				}
				fclose($fh);
			}
			*/
			# Call function to wwite rules to files
			if(strtolower($type) == 'subject') {
				$this->writeSaFile("/tmp/BL_SA",$dataAry,"subject");
			}else{
				$this->writeSaFile("/tmp/BL_SA",$dataAry,"body");
			}
			##############
			# Others
			##############
			# Sync rules *.cf files to /etc/mail/spamassassin/ folder and restart service 
			shell_exec('sudo /usr/bin/rsync -rvlHpogDtS --progress /tmp/*.cf /etc/mail/spamassassin/');
			
			# Check SA rules valid or not
			# 0 : success
			# Others : fail
			$result = shell_exec('sudo /usr/bin/spamassassin --lint && echo $?');
			if($result==0){
				shell_exec("sudo /etc/init.d/amavisd restart;/etc/init.d/spamassassin restart");
			}
			
			# clean tmp files
			$remove_files = array("/tmp/BL_SA_SUBJECT_UTF8.cf",
									"/tmp/BL_SA_SUBJECT_GBK.cf",
									"/tmp/BL_SA_SUBJECT_BIG5.cf",
									"/tmp/BL_SA_BODY_UTF8.cf",
									"/tmp/BL_SA_BODY_GBK.cf",
									"/tmp/BL_SA_BODY_BIG5.cf");
			foreach($remove_files as $file_to_remove){
				if(file_exists($file_to_remove)){
					unlink($file_to_remove);
				}
			}
			
			return $result == 0;
		}
		
		function changeAccountName($oldAccount, $newAccount, $module)
		{
			global $usr_home_dir;
			global $api_path;
			
			if(strtolower($module) == "ifolder"){
				$path = LIBLINUX_PATH . "/changeIfolderAccountName.sh";
				$result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$oldAccount $newAccount"));
			}else{
				$path = LIBLINUX_PATH . "/changeAccountName.sh";
				$result = shell_exec("/usr/bin/sudo $path ".escapeshellcmd("$oldAccount $newAccount"));
			}
			
			return trim($result)=="1";
		}
		
		# suspend user by appending "__" string at the end of md5 hash password field
		# iFolder use alternative approach by generating new random password 
        function setSuspendUser($login,$module)
        {
        	global $usr_home_dir;
			global $api_path;
            if ($this->actype == 'postfix')
            {
            	if($module == 'iFolder'){
            		$iuserspath =  $api_path."/".IUSERSAPI_PATH."/setSuspendUser.php";
					$login = escapeshellcmd($login);
					if(version_compare( phpversion(), '5', '<' ))
					{
						// if php version is 5.0 before
						$result = shell_exec("php -q $iuserspath loginID=$login");
					} else {
						// if php version is 5.0 or later 
						$result = shell_exec("php-cgi -q $iuserspath loginID=$login");
					}
					return $result;
            	}else{
	                $login = strtolower($login);
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/setSuspendUser.pl";
	                $result = shell_exec("/usr/bin/sudo $path $login");
	                return $result;
            	}
            }
            else
            {
                return false;
            }
        }

        # suspend user by appending "__" string at the end of md5 hash password field
        # resume iFolder account by resetting to original password
        function setUnsuspendUser($login,$module,$password)
        {
        	global $usr_home_dir;
			global $api_path;
            if ($this->actype == 'postfix')
            {
            	if($module == 'iFolder'){
            		$iuserspath =  $api_path."/".IUSERSAPI_PATH."/setUnsuspendUser.php";
					$login = escapeshellcmd($login);
					$password = escapeshellcmd($password);
					if(version_compare( phpversion(), '5', '<' ))
					{
						// if php version is 5.0 before
						$result = shell_exec("php -q $iuserspath loginID=$login password=$password");
					} else {
						// if php version is 5.0 or later 
						$result = shell_exec("php-cgi -q $iuserspath loginID=$login password=$password");
					}
					return $result;
            	}else{
	                $login = strtolower($login);
					$login = escapeshellcmd($login);
	                $path = LIBLINUX_PATH . "/setUnsuspendUser.pl";
	                $result = shell_exec("/usr/bin/sudo $path $login");
	                return $result;
            	}
            }
            else
            {
                return false;
            }
        }
		
		// decrypt with aes-128 in ecb mode
		function decrypt($encrypted_text)
		{
			if($encrypted_text == "") return $encrypted_text;
			$key = "f2rhXjdpJQ7cS75Y";
			$iv = "ph6LW7zLFS5KBZgH";
			
			$command="echo $encrypted_text | openssl enc -d -aes-128-ecb -nosalt -a -A -K " . bin2hex ($key) . " -iv " . bin2hex ($iv);
			$plain_text=trim(shell_exec("$command"));
			
			return $this->unpad($plain_text);
		}
		
		// chop the tail of the number of pad size
		function unpad($text)
		{
			$len = strlen($text);
			$last_char = ord($text[$len-1]); // get the last character ascii value
			$chop = $last_char;
			
			if($chop > $len) return $text;
			
			for($i=$len-$last_char;$i<($len-1);$i++)
			{
				if(ord($text[$i]) != $last_char){
					$chop = 0;
					break;
				}
			}
			return substr($text,0,$len-$chop);
		}
		
	# End
	}
}        # End of directive
?>
