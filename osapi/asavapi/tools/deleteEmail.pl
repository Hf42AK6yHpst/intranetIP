#!/usr/bin/perl

#use strict; # Declare strict checking on variable names, etc.
#use warnings 'all'; 
use File::Copy;
use Archive::Tar; 

my $argc;   # Declare variable $argc. This represents
            # the number of commandline parameters entered.

$argc = @ARGV; # Save the number of commandline parameters.
if (@ARGV==0)
{
  # The number of commandline parameters is 0,
  # so print an Usage message.
  #
  usage();  # Call subroutine usage()
  exit();   # When usage() has completed execution,
            # exit the program.
}


sub usage
{
  print "Usage: perl deleteFile.pl [Key] [File] [Array]\n";
}

if ( $ARGV[0] eq "ghfjdksla") 
{
#	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
	my $backupPath = "/tmp";
#	my $backupLocation =  $backupPath . "/" . $ARGV[1] ;
	my $backupLocation =  $ARGV[1] ;

	#Parse the parameter and put to array
	my @values = split(',', $ARGV[2]);
	my $deleteFileList = $backupLocation . ".delete";
        my $deleteFileResult = $backupLocation . ".result";
	my $backupFile = $backupLocation . ".tar";
	my $tar = Archive::Tar->new(); 

	open (FILE, "$ARGV[1]");
	open (DFILE, ">$deleteFileList");

	while (<FILE>) {
	chomp;
	($id, $filepath, $subject) = split("###");
		foreach my $val (@values) 
		{
			if ($val == $id){
				print DFILE "$filepath\n";
			}

			# Add some files: 
			$tar->add_files( "$filepath" ); 
#			if ( $val != "" ){
#				$tar->add_files("$filepath"); 
#			}
		}
	 }
	 close (FILE);
	 close (DFILE);


	# Create Backup
#        $tar->write( '/tmp/ffffile.tar' );
	$tar->write( "$backupFile" ); 

	# Delete the files
	open (D_INFILE, "$deleteFileList");
	open (D_OUTFILE, ">$deleteFileResult");
	while (<D_INFILE>) {
		chomp;
		print "$_\n";
		if (unlink($_) == 0) {
   			print D_OUTFILE "File was not deleted.";
		} else {
			print D_OUTFILE "File deleted successfully.";
		}
	}
        close (D_INFILE);
        close (D_OUTFILE);
}else {
	print "Invalid key!!"
}
