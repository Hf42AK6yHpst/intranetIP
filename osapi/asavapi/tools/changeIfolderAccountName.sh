#!/bin/sh
#
# Change ifolder account name.
# @param string old ifolder account name in lower case
# @param string new ifolder account name in lower case
# @return string return "1" if change email address succesffully! Otherwise, it shows fail message!
# @author Bob
# @date 20130416
#
# USAGE : ./changeIfolderAccountName.sh old_user new_user
# Log   : /usr/local/sbin/changeIfolderAccountName_log.txt

CURRENT_PATH=`pwd`
OSAPI_PATH=`echo $CURRENT_PATH|sed "s/asavapi\/tools//"`
TIMESTAMP=`date +%Y%m%d_%H%S`
IFOLDER_PATH="/home/iusers"
IFOLDER_PASSWORD=`cat ${OSAPI_PATH}/iusersapi/config.php |grep password|cut -d\  -f3|cut -d\" -f2`

OLD_USER=$1
NEW_USER=$2

EXTMAIL_PASSWD="ext2009mailBL"

# check parameter number
if [ $# -ne 2 ];then
    echo "USAGE : ./changeIfolderAccountName.sh old_user new_user"
    exit 0
fi

# check domain
if [ ! -d "$IFOLDER_PATH" ];then
    echo "Fail,Domain not found!!"
    exit 0
fi

# check old user ifolder exist
if [ ! -d "$IFOLDER_PATH/$OLD_USER" ];then
    echo "Fail,Old user : ${OLD_USER} : folder not found!!"
    echo "Fail,Old user : ${OLD_USER} : folder not found!!"  >> /usr/local/sbin/changeIfolderAccountName_new_user_not_found.txt
    exit 0
fi

# check new user ifolder exist
if [ -d "$IFOLDER_PATH/$NEW_USER" ];then
    echo "Fail,New user : ${NEW_USER} : already exist!!"
    echo "Fail,New user : ${NEW_USER} : already exist!!" >> /usr/local/sbin/changeIfolderAccountName_new_user_exist.txt
    exit 0
fi

# rename user folder
/bin/mv $IFOLDER_PATH/$OLD_USER $IFOLDER_PATH/$NEW_USER
echo "$TIMESTAMP : Renamed $OLD_USER to $NEW_USER done!" >> /usr/local/sbin/changeIfolderAccountName_log.txt

#Update Mysql in iusers database
mysql -uroot -p${IFOLDER_PASSWORD} iusers<<EOFMYSQL

update ftpquotalimits set name ="$NEW_USER" where name="$OLD_USER";
update ftpquotatallies set name ="$NEW_USER" where name="$OLD_USER";
update ftpuser set userid ="$NEW_USER" where userid="$OLD_USER";

update ftpuser set homedir = replace(homedir,'$OLD_USER','$NEW_USER') where userid="$NEW_USER";

EOFMYSQL

echo "1"
