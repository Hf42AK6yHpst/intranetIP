#!/usr/bin/perl
# vim: set ci et ts=4 sw=4:
# reportusage.pl: a script to report storage usage of a specific domain or
#                 all domain(s), can send user a email about it.
#
#      Author: He zhiqiang <hzqbbc@hzqbbc.com>
# Last Update: Tue Feb 06 2007 23:19:00
#     Version: 0.2

use vars qw($Ext::Config::PF $DIR $base);

BEGIN {
    my $path = $0;
    if ($path =~ s/tools\/removeBlackListRecord\.pl$//) {
        if ($path !~ /^\//) {
            $DIR = "./$path";
        } else {
            $DIR = $path;
        }
    } else {
        $DIR = '../';
    }

    unshift @INC, $DIR .'libs';
    unshift @INC, $DIR .'../extmail/libs';
    $Ext::Config::PF= "$DIR/webman.cf";

    select((select(STDOUT), $| = 1)[0]);
};

use strict;
use POSIX qw(strftime);
use Ext::Config;
use Ext::Mgr;
use Ext::Utils qw(human_size lock unlock haslock);
use Ext::MailFilter;
#use Ext::App::Pref;
use Ext::App::Filter;
use Ext::BL_App;

die "Usage: $0 [email_address|domain] \n" unless $#ARGV == 0;

#die "Warning: you need to install extmail at the same top direcotry\n".
#    "         in order to call extmail modules.\n\n".
#    "Usage: $0 [domain|-all] mailbase [recipient]\n" unless $#ARGV == 2;

if (!$SYS_CFG) {
    Ext::Config::import;
}

# to check wheather another process is handling
# the same job?
open (my $fh, "< $0") or die "Error: $!\n";

if (haslock ($fh)) {
    warn "There is another process working, abort\n";
    exit 255;
} else {
    lock ($fh);
}

my $c = $SYS_CFG;

my $backend = $c->{SYS_BACKEND_TYPE};
my $removeRecord = $ARGV[0];
#my $listPath = $ARGV[1];
my $listPath = $c->{SYS_AMAVIS_PATH};

my $result = Ext::BL_App->remove_blacklist_record($removeRecord, $listPath);

# Fixed blacklist to 100 score
`perl -p -e "s/^@//" /usr/local/slockd/config/sender_blacklist > /usr/local/slockd/config/amavisd_sender_blacklist`;

# Fixed amavisd restart problem
#`/etc/init.d/amavisd reload`;
`/etc/init.d/amavisd restart`;

print $result;


1;
