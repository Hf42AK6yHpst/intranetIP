#!/bin/bash

LOGIN_ID=$1

DOMAIN=`echo $LOGIN_ID | cut -d"@" -f2`
USERNAME=`echo $LOGIN_ID | cut -d"@" -f1`
DOMAIN_PATH="/home/domains/"
MAILFILTER="$DOMAIN_PATH/$DOMAIN/$USERNAME/.mailfilter"

##### Create SPAM folder #####
mkdir -p $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Spam/{cur,new,tmp}
touch $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Spam/maildirfolder 
chown -R vuser.vgroup $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Spam
chmod -R 700 $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Spam

mkdir -p $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Junk/{cur,new,tmp}
touch $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Junk/maildirfolder
chown -R vuser.vgroup $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Junk
chmod -R 700 $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Junk


mkdir -p $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Sent/{cur,new,tmp}
touch $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Sent/maildirfolder 
chown -R vuser.vgroup $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Sent
chmod -R 700 $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Sent

mkdir -p $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Trash/{cur,new,tmp}
touch $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Trash/maildirfolder 
chown -R vuser.vgroup $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Trash
chmod -R 700 $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Trash

mkdir -p $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Drafts/{cur,new,tmp}
touch $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Drafts/maildirfolder 
chown -R vuser.vgroup $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Drafts
chmod -R 700 $DOMAIN_PATH/$DOMAIN/$USERNAME/Maildir/.Drafts

chown -R vuser.vgroup $MAILROOT/extmail-curcnt
chown -R vuser.vgroup $MAILROOT/maildirsize

echo $MAILFILTER
##### Check .mailfilter exist or not #####
if [ ! -f $MAILFILTER ]; then
	echo "#MFMAILDROP=2" > $MAILFILTER
	echo "#" >> $MAILFILTER
	echo "# DO NOT EDIT THIS FILE.  This is an automatically generated filter." >> $MAILFILTER
	echo "# Generated by ExtMail 1.1" >> $MAILFILTER
	echo "" >> $MAILFILTER
	echo "HIGHSCORE=2.3" >> $MAILFILTER
	echo "" >> $MAILFILTER
	echo "FROM='$LOGIN_ID'" >> $MAILFILTER
	echo "import SENDER" >> $MAILFILTER
	echo "if (\$SENDER eq \"\")" >> $MAILFILTER
	echo "{" >> $MAILFILTER
	echo " SENDER=\$FROM" >> $MAILFILTER
	echo "}" >> $MAILFILTER
	echo "" >> $MAILFILTER
	echo "# Count amount of * to determine where to deliver mail" >> $MAILFILTER
	echo "" >> $MAILFILTER
	echo "/^X-Spam-Score: (-?[0-9].*\.[0-9].*)/" >> $MAILFILTER
	echo "if(\$MATCH1 >= \${HIGHSCORE})" >> $MAILFILTER
	echo "{" >> $MAILFILTER
	echo "	to \"\$HOME/Maildir/.Junk/.\"" >> $MAILFILTER
	echo "}" >> $MAILFILTER
	echo "else" >> $MAILFILTER
	echo "{" >> $MAILFILTER
	echo "	# Tagged as spam but not exceed the treshhold value of HIGHSCORE" >> $MAILFILTER
	echo "	to \"\$HOME/Maildir/.\"" >> $MAILFILTER
	echo "}" >> $MAILFILTER

        #Change permisssion to the .mailfilter file
	chown vuser.vgroup $MAILFILTER
	chmod 700 $MAILFILTER
fi
