#!/usr/bin/perl
# vim: set ci et ts=4 sw=4:
# reportusage.pl: a script to report storage usage of a specific domain or
#                 all domain(s), can send user a email about it.
#
#      Author: He zhiqiang <hzqbbc@hzqbbc.com>
# Last Update: Tue Feb 06 2007 23:19:00
#     Version: 0.2

use vars qw($Ext::Config::PF $DIR $base);

BEGIN {
    my $path = $0;
    if ($path =~ s/tools\/openLinuxAccount\.pl$//) {
        if ($path !~ /^\//) {
            $DIR = "./$path";
        } else {
            $DIR = $path;
        }
    } else {
        $DIR = '../';
    }

    unshift @INC, $DIR .'libs';
    unshift @INC, $DIR .'../libs';
    $Ext::Config::PF= "$DIR/webman.cf";

    select((select(STDOUT), $| = 1)[0]);
};

#use strict;
use POSIX qw(strftime);
use Ext::Config;
use Ext::Mgr;
use Ext::Utils qw(human_size lock unlock haslock);
use File::Path;


die "Usage: $0 [email_address] [password]\n" unless $#ARGV == 1;

#die "Warning: you need to install extmail at the same top direcotry\n".
#    "         in order to call extmail modules.\n\n".
#    "Usage: $0 [domain|-all] mailbase [recipient]\n" unless $#ARGV == 2;

if (!$SYS_CFG) {
    Ext::Config::import;
}

# to check wheather another process is handling
# the same job?
open (my $fh, "< $0") or die "Error: $!\n";

if (haslock ($fh)) {
    warn "There is another process working, abort\n";
    exit 255;
} else {
    lock ($fh);
}

my $c = $SYS_CFG;
my $backend = $c->{SYS_BACKEND_TYPE};
my $mgr; # the backend object
my $SENDMAIL = '/usr/sbin/sendmail -t -oi';

    $mgr = Ext::Mgr->new(
        type => 'mysql',
        host => $c->{SYS_MYSQL_HOST},
        socket => $c->{SYS_MYSQL_SOCKET},
        dbname => $c->{SYS_MYSQL_DB},
        dbuser => $c->{SYS_MYSQL_USER},
        dbpw => $c->{SYS_MYSQL_PASS},
        table => $c->{SYS_MYSQL_TABLE},
        table_attr_username => $c->{SYS_MYSQL_ATTR_USERNAME},
        table_attr_passwd => $c->{SYS_MYSQL_ATTR_PASSWD},
        crypt_type => $c->{SYS_CRYPT_TYPE},
        psize => $c->{SYS_PSIZE} || 10,
    );

#my $opt_domain =  $c->{SYS_EMAIL_DOMAIN};
my @splitValues = split('@', $ARGV[0]);
my $opt_domain = $splitValues[1];

my $opt_passwd = $ARGV[1];
my $opt_username = $ARGV[0];
my @data = split(/@/, $opt_username);
my $opt_uid = @data[0];
my $opt_clearpw = "";
my $opt_name = "";
my $opt_mailhost = "";
my $opt_maildir = "$opt_domain/$opt_uid/Maildir/";
my $opt_homedir = "$opt_domain/$opt_uid";
my $opt_quota = $c->{SYS_USER_DEFAULT_QUOTA} * $c->{SYS_QUOTA_MULTIPLIER};
my $opt_netdiskquota = "0";
my $opt_uidnumber = $c->{SYS_DEFAULT_UID};
my $opt_gidnumber = $c->{SYS_DEFAULT_GID};;
my $opt_create = "2100-11-08 15:10:04";
my $opt_expire = "2100-11-08 15:10:04";
my $opt_active = "1";
my $opt_disablepwdchange = "0";
my $opt_disablesmtpd = "0";
my $opt_disablesmtp = "0";
my $opt_disablewebmail = "0";
my $opt_disablenetdisk = "0";
my $opt_disableimap = "0";
my $opt_disablepop3 = "0";
my $usermail_directory = "$c->{SYS_MAILDIR_BASE}/$opt_maildir";
my $tempusername = "$c->{SYS_MAILDIR_BASE}/$opt_domain/$opt_uid";

my $result = $mgr->add_user(mail => $opt_username , 
			uid => $opt_uid, 
			passwd => $opt_passwd,
			clearpw => $opt_clearpw,
			cn => $opt_name,
			mailhost => $opt_mailhost,
			maildir => $opt_maildir,
			homedir => $opt_homedir,
			quota => $opt_quota,
			netdiskquota => $opt_netdiskquota,
			domain => $opt_domain,
			uidnumber => $opt_uidnumber,
			gidnumber => $opt_gidnumber,
			create => $opt_create,
			expire => $opt_expire,
			active => $opt_active,
			disablepwdchange => $opt_disablepwdchange,
			disablesmtpd => $opt_disablesmtpd,
			disablesmtp => $opt_disablesmtp,
			disablewebmail => $opt_disablewebmail,
			disablenetdisk => $opt_disablenetdisk,
			disableimap => $opt_disableimap,
			disablepop3 => $opt_disablepop3,
	);

## Create user directory
my $curdir = '';
umask(0077);

foreach my $dir (split(/\//, $usermail_directory)) {
        $curdir .= "$dir/";
        print "dir=$curdir\n" if ($ARGV[1] && $ARGV[1] eq '--debug');
        `/bin/mkdir $curdir 2>/dev/null`;
}

`/bin/mkdir "$usermail_directory/new" 2>/dev/null`;
`/bin/mkdir "$usermail_directory/cur" 2>/dev/null`;
`/bin/mkdir "$usermail_directory/tmp" 2>/dev/null`;

#`/bin/chown -R vuser.vgroup $usermail_directory`;
#my $temphome = $c->{SYS_MAILDIR_BASE};
#print $tempusername;
#`/bin/chown -R vuser.vgroup $c->{SYS_MAILDIR_BASE} 2>/dev/null`;
`/bin/chown -R vuser.vgroup $tempusername 2>/dev/null`;
#my $rere=`/bin/chown -R vuser.vgroup $usermail_directory`;

print $result;

1;


