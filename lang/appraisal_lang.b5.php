<?php
// using:
/*
 * Remarks:
* 1) Arrange the lang variables in alphabetical order
* 2) Keep the same line break between en and b5 lang files (so that same lang variable is in the same line in different lang files)
* 3) Use single quote for the array keys and double quote for the lang value
*/

$Lang['Menu']['AccountMgmt']['Management'] = "管理";
$Lang['Menu']['AccountMgmt']['Reports'] = "報告";
$Lang['Menu']['AccountMgmt']['Settings'] = "設定";
$Lang['Header']['Menu']['eAppraisal'] = "考績管理系統";

// A
$Lang['Appraisal']['AllMember'] = "所有組員";
$Lang['Appraisal']['AllTeacher'] = "所有教職員";
$Lang['Appraisal']['AllForm'] = "所有評審表格";
$Lang['Appraisal']['AppraisalPeriod'] = "考績週期";
$Lang['Appraisal']['AppraisalRecords'] = "考績紀錄";
$Lang['Appraisal']['AppraisalRecordsSetting'] = "設定考績紀錄";
$Lang['Appraisal']['AppraisalRecordsEdit'] = "填寫紀錄";
$Lang['Appraisal']['AppraisalRecordsView'] = "檢視紀錄";
$Lang['Appraisal']['AppraisalRecordsNeedFill'] = "需填寫自評紀錄";
$Lang['Appraisal']['AppraisalRecordsNeedNotFill'] = "不需填寫自評紀錄";
$Lang['Appraisal']['AppraisalRecordsModify'] = "可要求退還修改";
$Lang['Appraisal']['AROtherStaffAppraisal'] = "其他職員的評審表格";
$Lang['Appraisal']['ARMyAppraisal'] = "我的評審表格";
$Lang['Appraisal']['ARCycleStart'] = "考績週期「%s」現在進行中，你需填寫下列評審表格：";
$Lang['Appraisal']['ARForm'] = "評審表格";
$Lang['Appraisal']['ARFormFrm'] = "由";
$Lang['Appraisal']['ARFormTo'] = "至";
$Lang['Appraisal']['ARFormEdit'] = "編輯";
$Lang['Appraisal']['ARFormView'] = "檢視";
$Lang['Appraisal']['ARFormViewMember'] = "檢視組員";
$Lang['Appraisal']['ARFormStfOwnerName'] = "遞屬職員";
$Lang['Appraisal']['ARFormStfGrp'] = "職員分組描述";
$Lang['Appraisal']['ARFormIdvArrRmk'] = "個別編配備註";
$Lang['Appraisal']['ARFormMyRole'] = "我的角色";
$Lang['Appraisal']['ARFormFormFillAndOrder'] = "填表安排及次序";
$Lang['Appraisal']['ARFormSubDate'] = "我已遞交於";
$Lang['Appraisal']['ARFormAction'] = "表格內容";
$Lang['Appraisal']['ARFormMale'] = "男";
$Lang['Appraisal']['ARFormFemale'] = "女";
$Lang['Appraisal']['ARFormTgtName'] = "姓名";
$Lang['Appraisal']['ARFormPersonalParticulars'] = "個人資料";
$Lang['Appraisal']['ARFormPersonalEnglishName'] = "姓名(英文)";
$Lang['Appraisal']['ARFormPersonalChineseName'] = "姓名(中文)";
$Lang['Appraisal']['ARFormPersonalGender'] = "性別";
$Lang['Appraisal']['ARFormPresentJobInformation'] = "現有工作";
$Lang['Appraisal']['ARFormJobGrade'] = "職級";
$Lang['Appraisal']['ARFormGradeEntranceDate'] = "出任此職級日期";
$Lang['Appraisal']['ARFormDateHireSchool'] = "進入本學校服務日期";
$Lang['Appraisal']['ARFormDateHireOrganization'] = "進入本團體服務日期";
$Lang['Appraisal']['ARFormSeniorityGovtSubSchool'] = "年資(官/津校)";
$Lang['Appraisal']['ARFormSeniorityPriSchool'] = "年資(私校)";
$Lang['Appraisal']['ARFormSeniorityOrganization'] = "服務本會學校總年資(包括本年度)";
$Lang['Appraisal']['ARFormQuaAcq'] = "在考績期間修讀中/已獲得學歷";
$Lang['Appraisal']['ARFormSeq'] = "#";
$Lang['Appraisal']['ARFormQuaAcqQuaDesc'] = "學歷簡介";
$Lang['Appraisal']['ARFormQuaAcqSts'] = "狀態";
$Lang['Appraisal']['ARFormQuaAcqStsVal']["0"] = "0";
$Lang['Appraisal']['ARFormQuaAcqStsVal']["1"] = "1";
$Lang['Appraisal']['ARFormQuaAcqStsDes']["0"] = "修讀中";
$Lang['Appraisal']['ARFormQuaAcqStsDes']["1"] = "資歷已取得";
$Lang['Appraisal']['ARFormQuaAcqCmpDat'] = "完成日期";
$Lang['Appraisal']['ARFormQuaAcqAccBody'] = "學歷機構";
$Lang['Appraisal']['ARFormQuaAcqPrgDtl'] = "修讀課程及其他資料";
$Lang['Appraisal']['ARFormOnMouseEdit'] = "按一下進行編輯";
$Lang['Appraisal']['ARFormEdit'] = "編輯";
$Lang['Appraisal']['ARFormDelete'] = "刪除";
$Lang['Appraisal']['ARFormAdd'] = "新增";
$Lang['Appraisal']['ARFormPubYear'] = "本年度之著作";
$Lang['Appraisal']['ARFormExtServ'] = "本年度參與之校外服務";
$Lang['Appraisal']['ARFormTotClsCnt'] = "每周課節總數";
$Lang['Appraisal']['ARFormTeachSubj'] = "教授科目";
$Lang['Appraisal']['ARFormCls'] = "班別";
$Lang['Appraisal']['ARFormClsLv'] = "年級";
$Lang['Appraisal']['ARFormClsPerCyc'] = "每週授課節數";
$Lang['Appraisal']['ARFormOthAcaDesc'] = "學科工作(輔導教學、科務工作)";
$Lang['Appraisal']['ARFormOthAdmDesc'] = "行政工作(班主任、I.T成員、訓導成員)";
$Lang['Appraisal']['ARFormOthExtCurDesc'] = "課外活動(社系、學會等)";
$Lang['Appraisal']['ARFormOthDutDesc'] = "其他：( 如教師福利、家長教師會、校董教師諮議會、校董會等)";
$Lang['Appraisal']['ARFormNoLesAsb'] = "缺席課堂數量";
$Lang['Appraisal']['ARFormNoSubLes'] = "代課課堂數量";
$Lang['Appraisal']['ARFormNoTimLat'] = "遲到次數";
$Lang['Appraisal']['ARFormLeaCat'] = "假期類別";
$Lang['Appraisal']['ARFormPayLea'] = "有薪假期";
$Lang['Appraisal']['ARFormNoPayLea'] = "無薪假期";
$Lang['Appraisal']['ARFormPayTot'] = "總假期";
$Lang['Appraisal']['ARFormSickLeave'] = "病假";
$Lang['Appraisal']['ARFormOfficialLeave'] = "公假";
$Lang['Appraisal']['ARFormStudyLeave'] = "進修假期";
$Lang['Appraisal']['ARFormMaternityLeave'] = "產假";
$Lang['Appraisal']['ARFormRemarks'] = "備註";
$Lang['Appraisal']['ARFormOptionalRemarks'] = "備註(如有)";
$Lang['Appraisal']['ARFormFiller'] = "填寫者";
$Lang['Appraisal']['ARFormFillerRole'] = "填寫者角色";
$Lang['Appraisal']['ARFormFillerSignature'] = "簽名(密碼)";
$Lang['Appraisal']['ARFormAppraiserSignature'] = "評核者簽署";
$Lang['Appraisal']['ARFormAppraiseeSignature'] = "被評核者簽署";
$Lang['Appraisal']['ARFormPrincipalSignature'] = "校長簽署";
$Lang['Appraisal']['ARFormInvalidSignature'] = "不正確簽名(密碼)";
$Lang['Appraisal']['ARFormSubmission'] = "遞交表格";
$Lang['Appraisal']['ARFormYearClass'] = "班別";
$Lang['Appraisal']['ARFormSubject'] = "科目";
$Lang['Appraisal']['ARFormSubjectMedium'] = "教學語言";
$Lang['Appraisal']['ARFormDate'] = "日期";
$Lang['Appraisal']['ARFormPrintDate'] = "列印日期";
$Lang['Appraisal']['ARFormOrganization'] = "主辦單位 / 機構";
$Lang['Appraisal']['ARFormCourseSeminarWorkshop'] = "課程 / 講座 /工作坊";
$Lang['Appraisal']['ARFormHours'] = "時數";
$Lang['Appraisal']['ARFormCertification'] = "獲授文憑 / 證書";
$Lang['Appraisal']['ARFormName'] = "姓名";
$Lang['Appraisal']['ARFormChiName'] = "(中文)";
$Lang['Appraisal']['ARFormEngName'] = "(英文)";
$Lang['Appraisal']['AppraisalPeriodSetting'] = "設定考績週期";
$Lang['Appraisal']['AppraisalSecAppRoleSetting']="適用評審表格、特別考績角色及遞交批次設定";
$Lang['Appraisal']['ARFormModification']="退還予當事人修改";
$Lang['Appraisal']['ARFormModDate']="修改日期範圍";
$Lang['Appraisal']['ARFormNature']="性質";
$Lang['Appraisal']['AppraisalPeriodCopy'] = "複製考績週期";

$Lang['Appraisal']['ARMyObsFrm'] = "我的教務紀錄";
$Lang['Appraisal']['AROthObsFrm'] = "對其他老師的教務紀錄";
$Lang['Appraisal']['ARObsCycleStart'] = "考績週期「%s」現在進行中，你需填寫下列教務評審表格：";
$Lang['Appraisal']['ARFormTaskObjective']="工作目標";
$Lang['Appraisal']['ARFormTaskStandard']="表現標準";
$Lang['Appraisal']['ARFormTaskAction']="計劃/行動";
$Lang['Appraisal']['ARFormTaskAssessment']="成效與反思";
$Lang['Appraisal']['ARFormTaskAchievement']="自我評估／工作成就";
$Lang['Appraisal']['ARFormTaskParts']=array();
$Lang['Appraisal']['ARFormTaskParts'][1] = "i. 教學方面";
$Lang['Appraisal']['ARFormTaskParts'][2] = "ii. 班級經營方面";
$Lang['Appraisal']['ARFormTaskParts'][3] = "iii. 行政方面";
$Lang['Appraisal']['ARFormTaskParts'][4] = "iv. 專業發展方面";
$Lang['Appraisal']['ARFormStdTrainComp']="校外活動／比賽名稱";
$Lang['Appraisal']['ARFormStdTrainNo']="訓練人數";
$Lang['Appraisal']['ARFormStdTrainDur']="訓練時數";
$Lang['Appraisal']['ARFormStdTrainResult']="成績";
$Lang['Appraisal']['ARFormAttendanceCoverDateSingle'] = "9月至評核日期";
$Lang['Appraisal']['ARFormAttendanceCoverDateTerm1'] = "日數／次數 \n(九至四月)";
$Lang['Appraisal']['ARFormAttendanceCoverDateTerm2'] = "日數／次數 \n(五至八月)";
$Lang['Appraisal']['ARFormAttendanceUnit'] = "單位";
$Lang['Appraisal']['ARFormAttendanceListTotalAfter'] = "於此部後計算請假總和";
$Lang['Appraisal']['ARFormAttendanceUnitList'][0] = "日";
$Lang['Appraisal']['ARFormAttendanceUnitList'][1] = "次";
$Lang['Appraisal']['ARFormAttendanceUnitList'][2] = "天";
$Lang['Appraisal']['ARFormAttendanceUnitList'][3] = "節";
$Lang['Appraisal']['ARFormAttendanceColumn'] = "欄目";
$Lang['Appraisal']['ARFormAttendanceColumnRelatedDate'] = "相關時間段";
$Lang['Appraisal']['ARFormAttendanceLeaveTotal'] = "請假總日數";

$Lang['Appraisal']['ARFormCPDHourCategory'] = "類別";
$Lang['Appraisal']['ARFormCPDHourCategoryHour'] = "進修時數";
$Lang['Appraisal']['ARFormCPDHourHour'] = "小時";
$Lang['Appraisal']['ARFormCPDHourSchoolYear'] = "年度";
$Lang['Appraisal']['ARFormCPDHourTermA'] = "時數(九月至四月)";
$Lang['Appraisal']['ARFormCPDHourTermB'] = "時數(五月至八月)";
$Lang['Appraisal']['ARFormCPDHourUndefined'] = "時數({{START_MONTH}}至{{END_MONTH}})";
$Lang['Appraisal']['ARFormCPDHourTotal'] = "總時數";
$Lang['Appraisal']['ARFormCPDFormCourseTitle'] = "課程/活動名稱";
$Lang['Appraisal']['ARFormCPDFormCourseNature'] = "課程/活動性質";
$Lang['Appraisal']['ARFormCPDFormCourseContent'] = "課程/活動內容";
$Lang['Appraisal']['ARFormCPDFormOrgnBody'] = "主辦機構";
$Lang['Appraisal']['ARFormCPDFormStartDate'] = "課程/活動開始日期";
$Lang['Appraisal']['ARFormCPDFormEndDate'] = "課程/活動結束日期";
$Lang['Appraisal']['ARFormCPDFormCPDMode'] = "持續專業發展模式";
$Lang['Appraisal']['ARFormCPDFormCPDDomain'] = "範疇";
$Lang['Appraisal']['ARFormCPDFormSubjectRelated'] = "科目";
$Lang['Appraisal']['ARFormCPDFormCPDHour'] = "持續專業發展時數";
$Lang['Appraisal']['ARFormCPDFormBasicLawHour'] = "與基本法相關的持續專業發展時數";
$Lang['Appraisal']['ARFormCPDMode'][0] = "有系統的學習";
$Lang['Appraisal']['ARFormCPDMode'][1] = "其他";
$Lang['Appraisal']['ARFormCPDDomain'][0] = "I. 教與學";
$Lang['Appraisal']['ARFormCPDDomain'][1] = "II. 學生發展";
$Lang['Appraisal']['ARFormCPDDomain'][2] = "III. 學校發展";
$Lang['Appraisal']['ARFormCPDDomain'][3] = "IV. 專業群體關係及服務";
$Lang['Appraisal']['ARFormCPDDomain'][4] = "V. 教師成長及發展";
$Lang['Appraisal']['ARFormCPDDomain'][5] = "VI. 其他";
$Lang['Appraisal']['ARFormCPDSubject'][0] = "1. 中文";
$Lang['Appraisal']['ARFormCPDSubject'][1] = "2. 英文";
$Lang['Appraisal']['ARFormCPDSubject'][2] = "3. 數學";
$Lang['Appraisal']['ARFormCPDSubject'][3] = "4. 常識";
$Lang['Appraisal']['ARFormCPDSubject'][4] = "5. 視藝";
$Lang['Appraisal']['ARFormCPDSubject'][5] = "6. 音樂";
$Lang['Appraisal']['ARFormCPDSubject'][6] = "7. 體育";
$Lang['Appraisal']['ARFormCPDSubject'][7] = "8. 宗教";
$Lang['Appraisal']['ARFormCPDSubject'][8] = "9. STEM";
$Lang['Appraisal']['ARFormCPDPastYearHead'] = "教師在一個三年周期內之持續專業進修總時數表：";
$Lang['Appraisal']['ARFormCPDPastYearRemark'] = "(根據教育局的要求，教師需於一個三年周期內完成150小時的專業培訓)";

$Lang['Appraisal']['ARFormExpSchoolName'] = "學校名稱";
$Lang['Appraisal']['ARFormExpDate'] = "日期";
$Lang['Appraisal']['ARFormExpPost'] = "職位";
$Lang['Appraisal']['ARFormTrainingUni'] = "院校";
$Lang['Appraisal']['ARFormTrainingDate'] = "日期";
$Lang['Appraisal']['ARFormTrainingAward'] = "所考獲之證書 /文憑 /學位";
$Lang['Appraisal']['ARFormTrainingSbj'] = "所修科目(主修 / 副修)";

// B
$Lang['Appraisal']['BtnTempSave'] = "暫存";
$Lang['Appraisal']['BtnSign'] = "簽署及遞交";
$Lang['Appraisal']['BtnPreview'] = "預覽表格";
$Lang['Appraisal']['BtnPreviewCover'] = "預覽封面";
$Lang['Appraisal']['BtnSave'] = "儲存";
$Lang['Appraisal']['BtnReOpen'] = "重開已提交表格";
$Lang['Appraisal']['BtnSaveNext'] = "儲存及下一步";
$Lang['Appraisal']['BtnSaveNew'] = "儲存及新增";
$Lang['Appraisal']['BtnReset'] = "重設";
$Lang['Appraisal']['BtnAddTemplate'] = "新增";
$Lang['Appraisal']['BtnChangeTemplateOrder'] = "編緝排序";
$Lang['Appraisal']['BtnChangeTemplateOrderInReport'] = "編緝報告顯示排序";
$Lang['Appraisal']['BtnAddParSec'] = "新增章節";
$Lang['Appraisal']['BtnAddSubSec'] = "新增內章節";
$Lang['Appraisal']['BtnAddQCat'] = "新增評審問題分組";
$Lang['Appraisal']['BtnAddLikert'] = "新增李克特量表刻度";
$Lang['Appraisal']['BtnAddMatrix'] = "新增矩陣輸入欄位";
$Lang['Appraisal']['BtnAddQues'] = "新增評審題目";
$Lang['Appraisal']['BtnAddAns'] = "新增選擇題答案";
$Lang['Appraisal']['BtnAddAccTemplate'] = "新增適用評審表格";
$Lang['Appraisal']['BtnAddStaffGrp'] = "新增職員分組";
$Lang['Appraisal']['BtnAddStaffIdv'] = "新增個別職員評核安排";
$Lang['Appraisal']['BtnImportLeave'] = "匯入請假資料";
$Lang['Appraisal']['BtnImportAbsLateSub'] = "匯入缺席、遲到及代課資料";
$Lang['Appraisal']['BtnImportSLLateSub'] = "匯入病假及遲到記錄";
$Lang['Appraisal']['BtnImportAttendance'] = "匯入考勤資料";
$Lang['Appraisal']['BtnImport'] = "匯入";
$Lang['Appraisal']['BtbAddBatch'] = "增加評審表格之章節";
$Lang['Appraisal']['BtbRlsAppForm'] = "發放考績表格";
$Lang['Appraisal']['BtbAddCycle'] = "新增考績週期";
$Lang['Appraisal']['BtbConRlsCycle'] = "發放後，將重組填表安排，部份已填寫的內容或會移除，確認進行？";
$Lang['Appraisal']['BtbDelItem'] = "資料將被永久移除， 而且無法恢復。 你是否礭定刪除項目？";
$Lang['Appraisal']['BtbSaveAsGrp'] = "另存新職員分組";
$Lang['Appraisal']['BtnModify'] = "退還修改";
$Lang['Appraisal']['BtnImportCPDFromTeacherPortfolio'] = "與教職員檔案系統同步";
$Lang['Appraisal']['BtnImportStaffAttendance'] = "與考勤管理紀錄同步";

$Lang['Appraisal']['BtnRptGen'] = "產生報告";
$Lang['Appraisal']['BtnRptCal'] = "顯示結果";
$Lang['Appraisal']['BtnAddAns'] = "新增教務評審表格";
$Lang['Appraisal']['BtbSaveAsObs'] = "另存新教務評審報表";
$Lang['Appraisal']['BtbRlsObsAppForm'] = "發放可用教務評審表格";
$Lang['Appraisal']['BtbConRlsCycle'] = "確認產生考績表格給教職員?";
$Lang['Appraisal']['BtbConRlsCycleObs'] = "確認發放教務評審表格?";
$Lang['Appraisal']['BtbConExportAppraisalList'] = "確認匯出年度考績結果?";
$Lang['Appraisal']['BtbAddObsFrm'] = "新增教務評審表格";
$Lang['Appraisal']['BtbSaveAsObs'] = "另存教務評審表格";
$Lang['Appraisal']['BtnReportRelease'] = "發放考績報告";
// C
$Lang['Appraisal']['Completed']="已完成";
$Lang['Appraisal']['Cycle']['AcademicYear']="學年";
$Lang['Appraisal']['Cycle']['Descr']="考績週期描述";
$Lang['Appraisal']['Cycle']['CycleStart']="考績週期開始日";
$Lang['Appraisal']['Cycle']['CycleClose']="考績週期完結日";
$Lang['Appraisal']['Cycle']['Status']="狀況";
$Lang['Appraisal']['CycleStatusDes']['NotYetStart']="未開始";
$Lang['Appraisal']['CycleStatusDes']['InProgress']="進行中";
$Lang['Appraisal']['CycleStatusDes']['Finish']="已完結";
$Lang['Appraisal']['Cycle']['DateTimeRls']="考績表格已發放於";
$Lang['Appraisal']['CycleHeader']['Steps1']="考績週期屬性及主要資料";
$Lang['Appraisal']['CycleHeader']['Steps2']="適用評審表格、特別考績角色及遞交批次設定";
$Lang['Appraisal']['CycleHeader']['Steps3']="職員分組及評核安排";
$Lang['Appraisal']['CycleHeader']['Steps4']="個別職員評核安排";

$Lang['Appraisal']['Cycle']['BasicSetting'] = "考績週期基本設定";
$Lang['Appraisal']['Cycle']['AcademicYearID'] = "學年";
$Lang['Appraisal']['Cycle']['AppPrd'] = "考績日期範圍";
$Lang['Appraisal']['Cycle']['DescrEng'] = "考績週期描述 (英文)";
$Lang['Appraisal']['Cycle']['DescrChi'] = "考績週期描述 (中文)";
$Lang['Appraisal']['Cycle']['CycleStart'] = "考績週期開始日";
$Lang['Appraisal']['Cycle']['CycleClose'] = "考績週期完結日";
$Lang['Appraisal']['Cycle']['DateCheck'] = "完結日不能早於開始日";
$Lang['Appraisal']['Cycle']['DateTimeRls'] = "考績表格已發放於";
$Lang['Appraisal']['Cycle']['RlsBy_UserID'] = "考績表格發放者";
$Lang['Appraisal']['Cycle']['AfterCycleStart'] = "職員會於此日或之後看見評審表格";
$Lang['Appraisal']['Cycle']['AfterCycleClose'] = "職員於此日之後不再看見評審表格";
$Lang['Appraisal']['Cycle']['Remark']="附有「%s」的項目必須填寫";
$Lang['Appraisal']['Cycle']['DateCheckOverPeriod'] = "考績週期不能重疊";

$Lang['Appraisal']['Cycle']['Guideline'] = "填寫指引文件";
$Lang['Appraisal']['Cycle']['GuideEng'] = "填寫指引文件標題 (英文)";
$Lang['Appraisal']['Cycle']['GuideChi'] = "填寫指引文件標題 (中文)";
$Lang['Appraisal']['Cycle']['GuideFile'] = "填寫指引文件檔案";

$Lang['Appraisal']['Cycle']['RlsInfo'] = "考績表格發放資料";
$Lang['Appraisal']['Cycle']['RlsObsInfo'] = "設定本週期可用教務評審表格";
$Lang['Appraisal']['Cycle']['ExistAcademic'] = "已有這學年的考績週期";
$Lang['Appraisal']['Cycle']['NotRls'] = "(未發放)";
$Lang['Appraisal']['Cycle']['CopyFromCurrentCycle']="從這考績週期複製";
$Lang['Appraisal']['Cycle']['CopyToAcademicYear']="複製為此學年的考績週期";

$Lang['Appraisal']['Cycle']['AccTemplate'] = "適用於此考績週期的評審表格";
$Lang['Appraisal']['Cycle']['ObsTemplate'] = "適用本週期的教務評審表格";
$Lang['Appraisal']['CycleTemplate']['InvolvedAppRoleID'] = "參與填表的特別考績角色";
$Lang['Appraisal']['CycleTemplate']['DisplayOrder'] = "(排序設定)";
$Lang['Appraisal']['CycleTemplate']['Delete'] = "(刪除)";
$Lang['Appraisal']['CycleTemplate']['NoAssSpecAppRole'] = "(當中%s角色未編配職員)";

$Lang['Appraisal']['CycleTemplate']['CurrTemplate'] = "目前考績週期";
$Lang['Appraisal']['CycleTemplate']['AcceptApprsialForm'] = "適用評審表格";
$Lang['Appraisal']['CycleTemplate']['ApprsialForm'] = "評審表格";
$Lang['Appraisal']['CycleTemplate']['ArrangeSpecialRole'] = "特別考績角色職員編配";
$Lang['Appraisal']['CycleTemplate']['RelatedApprsialForm'] = "相關評審表格";
$Lang['Appraisal']['CycleTemplate']['ForPreView'] = "供檢視";

$Lang['Appraisal']['CycleTemplate']['SpecialRole']="特別考績角色";
$Lang['Appraisal']['CycleTemplate']['SelectedStaff']="選擇職員";
$Lang['Appraisal']['CycleTemplate']['ArrangedStaff']="已編配職員";
$Lang['Appraisal']['CycleTemplate']['NewSpecialFormSetting']="新增評審表格特別設定";
$Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName']="評審表格特別設定名稱";
$Lang['Appraisal']['CycleTemplate']['SetupSpecialFormSettingUsers']="管理使用評審表格特別設定用戶";
$Lang['Appraisal']['CycleTemplate']['DuplicatedSettingUsers']="用戶不能在在同一評審表格內使用多於一個特別設定";

$Lang['Appraisal']['CycleTemplate']['SectionBatch']="各章節遞交批次";
$Lang['Appraisal']['CycleTemplate']['SelectedSection']="所選評審表格之章節";
$Lang['Appraisal']['CycleTemplate']['AppRole']="遞交此章節的考績角色";
$Lang['Appraisal']['CycleTemplate']['Deadline']="遞交日期範圍";
$Lang['Appraisal']['CycleTemplate']['NoTitleSection']="[無標題章節]";
$Lang['Appraisal']['CycleTemplate']['NoRecordKeep']="更改評番表格之章節/考績角色，這批次下的評審紀錄不作保留";

$Lang['Appraisal']['CycleTemplate']['StfGrpAndArr']="職員分組及評核安排";
$Lang['Appraisal']['CycleTemplate']['StfGrp']="職員分組";
$Lang['Appraisal']['CycleTemplate']['StfGrpCnt']="人數";
$Lang['Appraisal']['CycleTemplate']['StfGrpDelete']="(刪除)";
$Lang['Appraisal']['CycleTemplate']['NoGrpDescr']="[無分組描述]";

$Lang['Appraisal']['CycleTemplate']['StfGrpContent']="職員分組內容";
$Lang['Appraisal']['CycleTemplate']['StfGrpDescrEng']="職員分組描述 (英文)";
$Lang['Appraisal']['CycleTemplate']['StfGrpDescrChi']="職員分組描述 (中文)";
$Lang['Appraisal']['CycleTemplate']['StfGrpEditFormMode']="編配表格及評核模式";
$Lang['Appraisal']['CycleTemplate']['StfGrp']="編配表格及評核模式";
$Lang['Appraisal']['CycleTemplate']['PossibleTemplate']="適用於此考績週期的評審表格";
$Lang['Appraisal']['CycleTemplate']['IncludeTemplate']="適用於此職員分組?";
$Lang['Appraisal']['CycleTemplate']['ApprsialMode']="評核模式";
$Lang['Appraisal']['CycleTemplate']['SelfApprsialMode']="自評";
$Lang['Appraisal']['CycleTemplate']['UpToDownApprsialMode']="組長評組員";
$Lang['Appraisal']['CycleTemplate']['DownToUpApprsialMode']="組員評組長";
$Lang['Appraisal']['CycleTemplate']['GroupApprsialMode']="全組互評";
$Lang['Appraisal']['CycleTemplate']['AssignLeader']="編配組長";
$Lang['Appraisal']['CycleTemplate']['AssignMembers']="編配組員";

$Lang['Appraisal']['CycleTemplate']['StaffIdvAndArr']="個別職員評核安排";
$Lang['Appraisal']['CycleTemplate']['PossibleTemplateForm']="適用於是次安排的評審表格";
$Lang['Appraisal']['CycleTemplate']['AssignedAppraisee']='編配被評者';
$Lang['Appraisal']['CycleTemplate']['SelectedAppraiseeRmk']='請選擇被評者 (只限一人)';
$Lang['Appraisal']['CycleTemplate']['SelectedAppraisee']='已選擇的被評者';
$Lang['Appraisal']['CycleTemplate']['AssignedAppraiser']='編配評核者';
$Lang['Appraisal']['CycleTemplate']['SelectedAppraiserRmk']='請選擇評核者 (可多於一人)';
$Lang['Appraisal']['CycleTemplate']['SelectedAppraiser']='已選擇的評核者';
$Lang['Appraisal']['CycleTemplate']['StfIdvName']='被評者';
$Lang['Appraisal']['CycleTemplate']['StfIdvTemplate']='評審表格';
$Lang['Appraisal']['CycleTemplate']['StfIdvDelete']='(刪除)';

$Lang['Appraisal']['CycleTemplate']['IdvAssRmk']="個別編配備註";
$Lang['Appraisal']['CycleTemplate']['Rmk']="備註";

$Lang['Appraisal']['CycleTemplate']['ApprasialLeave']="考績週期內的請假、缺席、遲到、代課資料";
$Lang['Appraisal']['CycleTemplate']['Attendance']="考勤資料";
$Lang['Appraisal']['CycleTemplate']['Leave']="請假資料";
$Lang['Appraisal']['CycleTemplate']['DownloadLeave']="下載請假資料";
$Lang['Appraisal']['CycleTemplate']['DownloadAbsence']="下載缺席、遲到及代課資料";
$Lang['Appraisal']['CycleTemplate']['LastUploadLeaveCSV']="最後上載之CSV 檔案";
$Lang['Appraisal']['CycleTemplate']['DownloadAttendance']="範本檔案(連所選考績週期的病假和遲到數據)";
$Lang['Appraisal']['CycleTemplate']['InfoFile']="資料檔案";
$Lang['Appraisal']['CycleTemplate']['CSVFile']="(.csv檔案)";
$Lang['Appraisal']['CycleTemplate']['TemplateFile']="範本檔案";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow']="資料欄";
$Lang['Appraisal']['CycleTemplate']['Column']="欄位";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow1']="欄位 A: %s職員的內聯網帳號";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow2']="欄位 B: 請假種類描述  %s";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow3']="欄位 C: 有薪假日數  %s";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow4']="欄位 D: 無薪假日數  %s";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow5']="欄位 E: 請假日數-不分有薪假及無薪假%s，如沒有欄位 C及D的資料 ，直接填寫此欄";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow6']="欄位 F: 請假備註";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk1']="(例如事假、病假、產假，請劃一所有職員的請假種類描述)";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk2']="(以整日或半日計算，如 12.0、6.5)";
$Lang['Appraisal']['CycleTemplate']['DownloadFiles']="下載文件";
$Lang['Appraisal']['CycleLeave']['Step1']="選擇CSV檔案";
$Lang['Appraisal']['CycleLeave']['Step2']="確定";
$Lang['Appraisal']['CycleLeave']['Step3']="匯入結果";
$Lang['Appraisal']['CycleLeave']['Remark']="附有「%s」的項目必須填寫";
$Lang['Appraisal']['CycleTemplate']['AbsLateSub']="缺席、遲到及代課資料";
$Lang['Appraisal']['CycleTemplate']['LastUploadAbsLateSubCSV']="最後上載之缺席、遲到及代課資料 CSV 檔案";
$Lang['Appraisal']['CycleExportTemplate']="按此下載範例";
$Lang['Appraisal']['CycleExportLeaveEng']['Row']="Row";
$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID']="Teacher ID";
$Lang['Appraisal']['CycleExportLeaveEng']['LeaveType']="Leave Type";
$Lang['Appraisal']['CycleExportLeaveEng']['DaysWithPay']="Days With Pay";
$Lang['Appraisal']['CycleExportLeaveEng']['DaysWithoutPay']="Days Without Pay";
$Lang['Appraisal']['CycleExportLeaveEng']['Days']="Days";
$Lang['Appraisal']['CycleExportLeaveEng']['Rmk']="Remark";
$Lang['Appraisal']['CycleExportLeaveEng']['SysRmk']="系統備註";
$Lang['Appraisal']['CycleExportLeaveChi']['Row']="行";
$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID']="內聯網帳號";
$Lang['Appraisal']['CycleExportLeaveChi']['LeaveType']="假期分類";
$Lang['Appraisal']['CycleExportLeaveChi']['DaysWithPay']="有薪假日數";
$Lang['Appraisal']['CycleExportLeaveChi']['DaysWithoutPay']="無薪假日數";
$Lang['Appraisal']['CycleExportLeaveChi']['Days']="日數";
$Lang['Appraisal']['CycleExportLeaveChi']['Rmk']="備註";
$Lang['Appraisal']['CycleExportLeaveChi']['SysRmk']="系統備註";
$Lang['Appraisal']['CycleExportLeave']['Row']="行";
$Lang['Appraisal']['CycleExportLeave']['TeacherID']="內聯網帳號";
$Lang['Appraisal']['CycleExportLeave']['LeaveType']="假期分類";
$Lang['Appraisal']['CycleExportLeave']['DaysWithPay']="有薪假日數";
$Lang['Appraisal']['CycleExportLeave']['DaysWithoutPay']="無薪假日數";
$Lang['Appraisal']['CycleExportLeave']['Days']="日數";
$Lang['Appraisal']['CycleExportLeave']['Rmk']="備註";
$Lang['Appraisal']['CycleExportLeave']['SysRmk']="系統備註";
$Lang['Appraisal']['CycleExportLeaveError']['NoLoginUser']="沒有這內聯網帳號。";
$Lang['Appraisal']['CycleExportLeaveError']['NoLeaveType']="沒有這假期分類。";
$Lang['Appraisal']['CycleExportLeaveError']['NeedNum']="需要數字。";
$Lang['Appraisal']['CycleExportLeave']['SuccessfulRecord']="正確紀錄";
$Lang['Appraisal']['CycleExportLeave']['FailureRecord']="錯誤紀錄";
$Lang['Appraisal']['CycleExportLeave']['SuccessRecord']="%s 個資料匯入成功完成";

$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow']="資料欄";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow1']="欄位 A: %s職員的內聯網帳號";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow2']="欄位 B: 缺席節數  %s";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow3']="欄位 C: 代課節數  %s";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow4']="欄位 D: 遲到次數  %s";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRmk1']="(以整日或半日計算，如 12.0、6.5)";

$Lang['Appraisal']['CycleExportAbsLateSubEng']['Row']="Row";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['TeacherID']="Teacher ID";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumAbsence']="Absence Lessons";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumSubstitution']="Substituted Lessons";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumLateness']="No. of Lateness";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['SysRmk']="系統備註";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['Row']="行";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['TeacherID']="內聯網帳號";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumAbsence']="缺席節數";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumSubstitution']="代課節數";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumLateness']="遲到次數";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['SysRmk']="系統備註";

$Lang['Appraisal']['CycleExportAbsLateSub']['Row']="Row";
$Lang['Appraisal']['CycleExportAbsLateSub']['TeacherID']="內聯網帳號";
$Lang['Appraisal']['CycleExportAbsLateSub']['NumAbsence']="缺席節數";
$Lang['Appraisal']['CycleExportAbsLateSub']['NumSubstitution']="代課節數";
$Lang['Appraisal']['CycleExportAbsLateSub']['NumLateness']="遲到次數";
$Lang['Appraisal']['CycleExportAbsLateSub']['SysRmk']="系統備註";

$Lang['Appraisal']['CycleExportAbsLateSub']['SuccessfulRecord']="正確紀錄";
$Lang['Appraisal']['CycleExportAbsLateSub']['FailureRecord']="錯誤紀錄";

$Lang['Appraisal']['CycleExportAbsLateSubError']['NoLoginUser']="沒有這內聯網帳號。";
$Lang['Appraisal']['CycleExportAbsLateSubError']['NeedNum']="需要數字。";

$Lang['Appraisal']['CopyChi'] = "複製";
$Lang['Appraisal']['CopyEng'] = "Copy";
$Lang['Appraisal']['CurrentAcademicYearTilMar'] = "本學年(至三月份)";

// D
$Lang['Appraisal']['Drafted']="草擬中";
$Lang['Appraisal']['Deleted'] = "已離校";

// E
$Lang['Appraisal']['Error']['TemplateSettingEditRoleMissing'] = "所選評審表格並未完成設定。請於評審表格範本中設置可填寫此章節的考績角色。";
$Lang['Appraisal']['EarlyBird'] = "早鳥";
$Lang['Appraisal']['ExportAppraisalReports'] = "匯出年度考績結果";

// F

// G
$Lang['Appraisal']['GroupRecord'] = "分組紀錄";
$Lang['Appraisal']['GroupMember'] = "組員";
$Lang['Appraisal']['GroupRoleUnselected'] = "請選擇組長角色";
$Lang['Appraisal']['GroupRecordRead'] = "已閱";
$Lang['Appraisal']['GroupRecordUnread'] = "未閱";
$Lang['Appraisal']['GroupModifying'] = "退還修改中";
// H

// I
$Lang['Appraisal']['ImportInformation']="資料匯入";
$Lang['Appraisal']['IronMan'] = "鐵人";
// J


// K


// L
$Lang['Appraisal']['LatenessRecord'] = "遲到記錄";
$Lang['Appraisal']['LastAcademicYear'] = "上學年";
// LNT
$Lang['Appraisal']['LNT']['LearningAndTeaching'] = "學與教";
$Lang['Appraisal']['LNT']['Question'] = "問題";
$Lang['Appraisal']['LNT']['Answer'] = "答案";
$Lang['Appraisal']['LNT']['Score'] = "分數";
$Lang['Appraisal']['LNT']['Part'] = "分類";
$Lang['Appraisal']['LNT']['QuestionNumber'] = "問題編號";
$Lang['Appraisal']['LNT']['QuestionType'] = "種類";
$Lang['Appraisal']['LNT']['AnswerLowerLimit'] = "分數下限";
$Lang['Appraisal']['LNT']['AnswerUpperLimit'] = "分數上限";
$Lang['Appraisal']['LNT']['AllGeneralSubject'] = "所有一般學科";
$Lang['Appraisal']['LNT']['GeneralSubject'] = "一般學科";
$Lang['Appraisal']['LNT']['OtherSubject'] = "其他學科";
$Lang['Appraisal']['LNT']['AllSubject'] = "所有學科";

$Lang['Appraisal']['LNT']['ImportLnt'] = "匯入學與教資料";
$Lang['Appraisal']['LNT']['ImportType'] = "匯入種類";
$Lang['Appraisal']['LNT']['GenerateCsvTemplate'] = "製作CSV範本檔";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>學科<span class=\"tabletextremark\"> (WebSAMSCode)</span>";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>^</span>學科名稱 [參考用途]";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>分類";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "分類名稱";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>問題編號";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>題目";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>種類<span class=\"tabletextremark\"> (種類為  Scale 或 Text )</span>";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "分數下限";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "分數上限";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>*</span>職員的內聯網帳號";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>*</span>學科<span class=\"tabletextremark\"> (WebSAMSCode)</span>";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>^</span>學科名稱 [參考用途]";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>*</span>班別";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>*</span>答案";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoPart'] = "分類不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoQuestionNumber'] = "問題編號不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoQuestionTitle'] = "題目不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoQuestionType'] = "種類不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Question']['InvalidQuestionType'] = "種類無效";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoLowerLimit'] = "此種類的分數下限不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Question']['InvalidLowerLimit'] = "分數下限必須為整數";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoUpperLimit'] = "此種類的分數上限不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Question']['InvalidUpperLimit'] = "分數上限必須為整數";

$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyUser'] = "內聯網帳號不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptySubject'] = "學科不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyClass'] = "班別不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['NoSubject'] = "沒有此學科";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['WrongSubject'] = "學科錯誤";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['NoClass'] = "沒有此班別";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['NotIntAnswer'] = "答案應為整數";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyAnswer'] = "答案不能留空";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['AnswerOverMinMax'] = "答案不能超過上/下限";

$Lang['Appraisal']['LNT']['ImportMsg']['EmptyQuestion'] = "本學年尚未有題目";
$Lang['Appraisal']['LNT']['ImportMsg']['DeleteRelatedSubjectAnswer'] = "所有選擇年度之相關科目的答案將一併被刪除。你是否確定要刪除？";
$Lang['Appraisal']['LNT']['ImportMsg']['DeleteAnswer'] = "所有選擇年度、科目及班別之答案將一併被刪除。你是否確定要刪除？";


$Lang['Appraisal']['LNT']['Report']['ReportType'] = "報告類型";
$Lang['Appraisal']['LNT']['Report']['StatSummary'] = "考績統計";
$Lang['Appraisal']['LNT']['Report']['StatDetails'] = "統計詳細資料";
$Lang['Appraisal']['LNT']['Report']['Average'] = "平均分";
$Lang['Appraisal']['LNT']['Report']['SD'] = "標準差";
$Lang['Appraisal']['LNT']['Report']['Count'] = "總數";
$Lang['Appraisal']['LNT']['ReportMsg']['SelectTeacher'] = "請選擇老師";
$Lang['Appraisal']['LNT']['ReportMsg']['SelectReportType'] = "請選擇報告類型";

// M
$Lang['Appraisal']['Message']['ErrorDelete'] ="此數據在使用，不能刪除。";
$Lang['Appraisal']['Message']['ErrorInactive'] ="此評審表格正在使用，不能暫停。";
$Lang['Appraisal']['Message']['Released'] ="考績表格已發放";
$Lang['Appraisal']['Message']['ConfirmRemoveFormFromCycle'] = "確認從本考績週期移除此表格？";
$Lang['Appraisal']['Message']['ConfirmReOpenThisForm'] = "確認重新開放表格以供編輯？";
$Lang['Appraisal']['Message']['ConfirmReOpenThisFormCaution'] = "(注意：所有已提交表格也會轉為可編輯)";
$Lang['Appraisal']['Message']['SynchronizationCompleted'] = "同步已完成";
$Lang['Appraisal']['Message']['SelectAppraisalScheme'] = "請選擇考績週期";
$Lang['Appraisal']['Message']['SelectAcademicYear'] = "請選擇學年";
$Lang['Appraisal']['Message']['ConfirmReleaseReport'] = "您確定要向所有教師發布本考績週期報告嗎？";

$Lang['Appraisal']['Month'][1] = "一月";
$Lang['Appraisal']['Month'][2] = "二月";
$Lang['Appraisal']['Month'][3] = "三月";
$Lang['Appraisal']['Month'][4] = "四月";
$Lang['Appraisal']['Month'][5] = "五月";
$Lang['Appraisal']['Month'][6] = "六月";
$Lang['Appraisal']['Month'][7] = "七月";
$Lang['Appraisal']['Month'][8] = "八月";
$Lang['Appraisal']['Month'][9] = "九月";
$Lang['Appraisal']['Month'][10] = "十月";
$Lang['Appraisal']['Month'][11] = "十一月";
$Lang['Appraisal']['Month'][12] = "十二月";

$Lang['Appraisal']['ModificationList'] = "考績紀錄修正";
$Lang['Appraisal']['Modification']['modification']="修正紀錄";
$Lang['Appraisal']['Modification']['return']="退還紀錄";
$Lang['Appraisal']['Modification']['returnTo'] = "退還表格至";

// N
$Lang['Appraisal']['Message']['NoRecord']="沒有相關資料";
$Lang['Appraisal']['Message']['NoRecordBrkt']="[沒有設定相關資料]";
$Lang['Appraisal']['Message']['NoCPDRecord']="教職員檔案系統中沒有持續專業發展記錄";
$Lang['Appraisal']['Message']['NoNewCPDRecord']="教職員檔案系統沒有新的持續專業發展記錄記錄";
$Lang['Appraisal']['Message']['CPDImportComplete']="教職員檔案系統中的所有持續專業發展記錄已經同步到教師評估";
$Lang['Appraisal']['Message']['DeleteAcademicAffairs']="即將刪除此教務紀錄之所有章節。確認？";
$Lang['Appraisal']['Message']['CopyAcademicAffairs']="即將複製此教務紀錄。確認？";
$Lang['Appraisal']['Message']['AttendanceNoReason']="無選擇原因";
// O
$Lang['Appraisal']['Or']="或";
$Lang['Appraisal']['ObservationPeriod'] = "教務紀錄";
$Lang['Appraisal']['ObservationRecord'] = "教務紀錄";
$Lang['Appraisal']['ObservationRecordAdd'] = "新增教務紀錄";
$Lang['Appraisal']['ObsRcd']['AddAppraisee'] = "編輯被評者";
$Lang['Appraisal']['ObsRcd']['AddAppraiser'] = "編輯評核者";
$Lang['Appraisal']['ObsFormFiller'] = "評核者";
$Lang['Appraisal']['ObsFormInvalidSignature'] = "不正確簽名(密碼)";
$Lang['Appraisal']['ObsFormAtLeastOne'] = "至少一個考績者簽名";
$Lang['Appraisal']['ObsFormSubDate'] = "教務評審表已遞交於";
$Lang['Appraisal']['ObsFormSbjMissing'] = "請選擇科目";
$Lang['Appraisal']['ObsFormClassMissing'] = "請選擇班別";
$Lang['Appraisal']['ObsFormSigned'] = "由此評核者簽署";

// P
$Lang['Appraisal']['Pending']="待完成";
$Lang['Appraisal']['PageBreakWhenPrint'] = "列印時於此章節後分頁";
$Lang['Appraisal']['PreviewRemindSave'] = "下面顯示的格式更改尚未儲存。 請記得儲存。";

// Q


// R
$Lang['Appraisal']['ReportPersonalAppraisalResult'] = "個人年度考績結果";
$Lang['Appraisal']['ReportOutstandingAppraisalList'] = "未遞交考績紀錄列表";
$Lang['Appraisal']['ReportCalculationAppraisal'] = "計算考績紀錄平均分";
$Lang['Appraisal']['ReportSchoolResultlList'] = "教師整體考績評分表";
$Lang['Appraisal']['ReportPersonalAverageList'] = "個人考績統計報告";
$Lang['Appraisal']['ReportTaskAssignmentChecklist'] = "評估分配清單";
$Lang['Appraisal']['ReportExportProposedAllocation'] = "匯出職務安排建議";
$Lang['Appraisal']['ReportTeachingReview'] = "教師課業檢閱報告表";

$Lang['Appraisal']['ReportPrint'] = "列印";
$Lang['Appraisal']['ReportProcessing'] = "處理進行中...";

$Lang['Appraisal']['ReportOutstanding']['Title'] = "未遞交考績紀錄列表 (更新至 %s)";
$Lang['Appraisal']['ReportOutstanding']['NoFrmTitle'] = "[無表格標題]";
$Lang['Appraisal']['ReportOutstanding']['NoSecTitle'] = "[無章節標題]";
$Lang['Appraisal']['ReportOutstanding']['NoSecTitle'] = "[無章節標題]";
$Lang['Appraisal']['ReportOutstanding']['User'] = "被評者";
$Lang['Appraisal']['ReportOutstanding']['Template'] = "評審表格";
$Lang['Appraisal']['ReportOutstanding']['Filler'] = "現時填寫職員";
$Lang['Appraisal']['ReportOutstanding']['CurSec'] = "現時填寫章節";
$Lang['Appraisal']['ReportOutstanding']['SubDatFr'] = "遞交日期範圍由";
$Lang['Appraisal']['ReportOutstanding']['SubDatTo'] = "遞交日期範圍至";
$Lang['Appraisal']['ReportOutstanding']['RmnSubDat'] = "距離遞交限期尚餘日數";
$Lang['Appraisal']['ReportOutstanding']['RmnDat'] = "尚餘日數";
$Lang['Appraisal']['ReportOutstanding']['IDV'] = "個別職員評核";
$Lang['Appraisal']['ReportOutstanding']['GRP'] = "職員分組評核";
$Lang['Appraisal']['ReportOutstanding']['OBS'] = "教務評核";

$Lang['Appraisal']['ReportPersonResult']['SelectAppraisee']="請選擇被評者";
$Lang['Appraisal']['ReportPersonResult']['SelectedAppraisee']="已選擇的被評者";
$Lang['Appraisal']['ReportPersonResult']['LoginID']="以登入名稱搜尋";
$Lang['Appraisal']['ReportPersonResult']['InclObs']="包括教務記錄";
$Lang['Appraisal']['ReportPersonResult']['OutputDOC']="以Word文檔格式匯出";
$Lang['Appraisal']['ReportPersonResult']['DisplayName']="包括評核者名稱";
$Lang['Appraisal']['ReportPersonResult']['DisplayDate']="包括評核日期";
$Lang['Appraisal']['ReportPersonResult']['DisplayMode']="模式";
$Lang['Appraisal']['ReportPersonResult']['DisplayModeSeparate']="顯示所有評核回應";
$Lang['Appraisal']['ReportPersonResult']['DisplayModeGrouping']="整合顯示評核回應";
$Lang['Appraisal']['ReportPersonResult']['DisplayWithCover']="包括已設定封面";

$Lang['Appraisal']['Report']['InvalidFormat']="檔案格式無效。";
$Lang['Appraisal']['Report']['MandatorySelection']="需要選擇";
$Lang['Appraisal']['Report']['ReportBeforeCycleEnd']="[考績週期未完結版本]";
$Lang['Appraisal']['Report']['TeacherName']="老師姓名";
$Lang['Appraisal']['Report']['SumOfScore']="總評分";
$Lang['Appraisal']['Report']['AverageMark']="平均分";
$Lang['Appraisal']['Report']['ModeSelection']="選擇模式";
$Lang['Appraisal']['Report']['Total']="總數";
$Lang['Appraisal']['Report']['NotSubmitted']="未提交";
$Lang['Appraisal']['Report']['NoOfPeople']="人數";
$Lang['Appraisal']['Report']['ModeShowAll']="全體被評者";
$Lang['Appraisal']['Report']['ModeShowOne']="指定被評者";
$Lang['Appraisal']['Report']['ModeShowGeneral']="整體報告";
$Lang['Appraisal']['Report']['ModeShowIndividual']="個別評核詳情";
$Lang['Appraisal']['Report']['SpecialDisplaySection']="獨立計算評分內章節";
$Lang['Appraisal']['Report']['SpecialDisplaySectionRemarks']="此部分選擇的內章節將不會計算進評審表格的平均分內";

// S
$Lang['Appraisal']['SettingsSpecialRole'] = "特別考績角色";
$Lang['Appraisal']['SpecialRole'] = "設定特別考績角色";
$Lang['Appraisal']['SettingsTemplate'] = "評審表格範本";
$Lang['Appraisal']['SpecialRolesIsActive'] = "狀況";
$Lang['Appraisal']['SpecialRolesIsActiveDes']['0'] = "暫停";
$Lang['Appraisal']['SpecialRolesIsActiveDes']['1'] = "使用中";
$Lang['Appraisal']['SpecialRolesSysUse'] = "系統預設";
$Lang['Appraisal']['SpecialRolesSysUseDes']['0'] = "否";
$Lang['Appraisal']['SpecialRolesSysUseDes']['1'] = "是";
$Lang['Appraisal']['SpecialRolesDefRolDes']['1'] = "(指定考績者)";
$Lang['Appraisal']['SpecialRolesDefRolDes']['2'] = "(指定被評者)";

$Lang['Appraisal']['SpecialRolesEngDes'] = "考績角色名稱(英文)";
$Lang['Appraisal']['SpecialRolesChiDes'] = "考績角色名稱(中文)";
$Lang['Appraisal']['SpecialRolesDisplayOrder'] = "(排序設定)";
$Lang['Appraisal']['SpecialRoles']['AddRoles']="新增";
$Lang['Appraisal']['SpecialRoles']['ChangeOrder']="編緝排序";
$Lang['Appraisal']['SpecialRoles']['DeleteRoles']="刪除";
$Lang['Appraisal']['SpecialRoles']['Mandatory']="必須填寫";
$Lang['Appraisal']['SearchByLoginID']="以登入名稱搜尋";
$Lang['Appraisal']['SettingsBasicSettings'] = "基本設定";
$Lang['Appraisal']['SettingsAppraisalReportCoverSettings'] = "考績表封面設定";
$Lang['Appraisal']['SettingsExcludeTeachers'] = "不需被評核老師名單";
$Lang['Appraisal']['SettingsCPDRecords'] = "教師持續專業發展記錄同步設定";
$Lang['Appraisal']['SettingsCPDRecordsPeriod'] = "以%d年為單位顯示";
$Lang['Appraisal']['SettingsCPDRecordsStarting'] = "始於";
$Lang['Appraisal']['SettingsCPDDuration'] = "教師持續專業發展記錄時間段設定";
$Lang['Appraisal']['SettingsCPDDurationRemarks'] = "學年內教師持續專業發展記錄的時數統計時間段劃分";
$Lang['Appraisal']['SettingsCPDShowingTotalHours'] = "顯示教師持續專業發展記錄總時數";
$Lang['Appraisal']['SettingsAttendanceImport'] = "考勤記錄同步設定";
$Lang['Appraisal']['SettingsAttendanceReasons'] = "請假記錄類別設定";
$Lang['Appraisal']['SettingsColumnReasons'] = "考勤資料欄目設定";
$Lang['Appraisal']['SettingsColumnOrientation'] = "請選擇顯示模式";
$Lang['Appraisal']['SettingsReportDisplayPerson'] = "可檢視報告用戶名單";
$Lang['Appraisal']['SettingsReportDisplayPersonRemarks'] = "備註：<br>非考績管理系統管理員：可檢視自己的報告<br>考績管理系統管理員：可檢視自己及其他老師的報告";
$Lang['Appraisal']['SettingsReportDisplayTime'] = "個人報告發表時間";
$Lang['Appraisal']['SettingsReportDoubleCountPerson'] = "分數雙重加權用戶名單";
$Lang['Appraisal']['SettingsPrintMcDisplayMode'] = "個人年度考績結果選擇題顯示模式";
$Lang['Appraisal']['SettingsPrintMcDisplayModeAll'] = "顯示所有選項";
$Lang['Appraisal']['SettingsPrintMcDisplayModeChecked'] = "只顯示已選項目";
$Lang['Appraisal']['Settings']['Language'] = "語言";
$Lang['Appraisal']['Settings']['GroupName'] = "所屬團體名稱";
$Lang['Appraisal']['Settings']['SchoolEmblem'] = "校徽";
$Lang['Appraisal']['Settings']['SchoolYear'] = "該學年";
$Lang['Appraisal']['Settings']['ReportName'] = "報告名稱";
$Lang['Appraisal']['Settings']['TeacherName'] = "被評老師名稱";
$Lang['Appraisal']['Settings']['SchoolMotto'] = "校訓 (最多50個中文字)";
$Lang['Appraisal']['Settings']['SchoolMottoLimitation'] = "校訓過長 - 最多為50個中文字";
$Lang['Appraisal']['Settings']['AllAppRole'] = "所有考績角色";
$Lang['Appraisal']['Settings']['TraditionalChinese'] = "繁體中文";
$Lang['Appraisal']['Settings']['English'] = "英文";
$Lang['Appraisal']['Settings']['Display'] = "顯示";
$Lang['Appraisal']['Settings']['DisplayChineseName'] = "顯示中文名";
$Lang['Appraisal']['Settings']['DisplayEnglishName'] = "顯示英文名";
$Lang['Appraisal']['SickLeaveRecord'] = "病假記錄";
$Lang['Appraisal']['SickLeaveAndLateRecords'] = "病假及遲到記錄";
// T
$Lang['Appraisal']['TemplateSampleForm']="評審表格範本";
$Lang['Appraisal']['TemplateSample']["FormEngName"]="表格標題、編號、目的(英文)";
$Lang['Appraisal']['TemplateSample']["FormChiName"]="表格標題、編號、目的(中文)";
$Lang['Appraisal']['TemplateSample']["FormStatus"]="狀況";
$Lang['Appraisal']['TemplateSample']["ModifiedDate"]="最後更新於";
$Lang['Appraisal']['TemplateSample']["DisplayOrder"]="(排序設定)";
$Lang['Appraisal']['TemplateSampleIsActive']["0"]="已停用";
$Lang['Appraisal']['TemplateSampleIsActive']["1"]="已啟用";
$Lang['Appraisal']['TemplateSample']['CopyTemplate']="複製";
$Lang['Appraisal']['TemplateSample']['DeleteTemplate']="刪除";
$Lang['Appraisal']['TemplateSample']['PDFTitle']="設定預設評審表格";
$Lang['Appraisal']['TemplateSample']['Title']="設定自定義評審表格";
$Lang['Appraisal']['TemplateSample']['BasicInformation']="表格基本資料";
$Lang['Appraisal']['TemplateSample']['SelectedSection']="適用章節選項";
$Lang['Appraisal']['TemplateSample']['FrmTitle']="表格標題";
$Lang['Appraisal']['TemplateSample']['FrmTitleEng']="表格標題(英文)";
$Lang['Appraisal']['TemplateSample']['FrmTitleChi']="表格標題(中文)";
$Lang['Appraisal']['TemplateSample']['FrmCodEng']="表格編號(英文)";
$Lang['Appraisal']['TemplateSample']['FrmCodChi']="表格編號(中文)";
$Lang['Appraisal']['TemplateSample']['HdrRefEng']="表格參照(英文)";
$Lang['Appraisal']['TemplateSample']['HdrRefChi']="表格參照(中文)";
$Lang['Appraisal']['TemplateSample']['ObjEng']="表格目的(英文)";
$Lang['Appraisal']['TemplateSample']['ObjChi']="表格目的(中文)";
$Lang['Appraisal']['TemplateSample']['DescrEng']="表格描述(英文)";
$Lang['Appraisal']['TemplateSample']['DescrChi']="表格描述(中文)";
$Lang['Appraisal']['TemplateSample']['AppTgtEng']="評審對象稱謂(英文)";
$Lang['Appraisal']['TemplateSample']['AppTgtChi']="評審對象稱謂(中文)";
$Lang['Appraisal']['TemplateSample']['NeedClass']="須要填寫班別";
$Lang['Appraisal']['TemplateSample']['NeedSubj']="須要填寫科目";
$Lang['Appraisal']['TemplateSample']['NeedSubjLang']="須要填寫教學語言";
$Lang['Appraisal']['TemplateSample']['Mandatory']="必須填寫";
$Lang['Appraisal']['TemplateSample']['Number']="必須是數字";
$Lang['Appraisal']['TemplateSample']['DecRng03']="小數範圍必須0-3位";
$Lang['Appraisal']['TemplateSample']['DateOverlap']="時間重疊";
$Lang['Appraisal']['TemplateSample']['DateOutCycle']="時間不能超過週期範圍";
$Lang['Appraisal']['TemplateSample']['DateInvalidRange']="遞交完結日期不能早於遞交開始日期";
$Lang['Appraisal']['TemplateSample']['ModDateInvalidRange']="修改完結日期不能早於修改開始日期";
$Lang['Appraisal']['TemplateSample']['DateOutEdit']="時間不能超過遞交日期範圍";
$Lang['Appraisal']['TemplateSample']['Status']="狀況";
$Lang['Appraisal']['TemplateSampleStatusVal']['0']="暫停";
$Lang['Appraisal']['TemplateSampleStatusVal']['1']="啟用";
$Lang['Appraisal']['TemplateSample']['DataPreload']="預載去年輸入個人資料";
$Lang['Appraisal']['TemplateSampleDataPreloadVal']['0']="不預載";
$Lang['Appraisal']['TemplateSampleDataPreloadVal']['1']="預載";
$Lang['Appraisal']['TemplateSample']['Remark']="附有「%s」的項目必須填寫";
$Lang['Appraisal']['TemplateSampleDataSelection']['DescrEng']="章節名稱(英文)";
$Lang['Appraisal']['TemplateSampleDataSelection']['DescrChi']="章節名稱(中文)";
$Lang['Appraisal']['TemplateSampleDataSelection']['DisplayOrder']="(排序設定)";
$Lang['Appraisal']['TemplateSampleDataSelection']['Display']="於表格中顯示";
$Lang['Appraisal']['TemplateSample']['Steps1']="表格屬性";
$Lang['Appraisal']['TemplateSample']['Steps2']="章節";
$Lang['Appraisal']['TemplateSample']['Steps3']="內章節";
$Lang['Appraisal']['TemplateSample']['Steps4']="評審題目分組";
$Lang['Appraisal']['TemplateSample']['Steps5']="評審題目";
$Lang['Appraisal']['TemplateSample']['SDFBasicInformation']="表格基本資料";
$Lang['Appraisal']['TemplateSample']['SDFSecInclude']="包含章節";
$Lang['Appraisal']['TemplateSample']['SDFSecRoleAccess']="章節讀寫權限";
$Lang['Appraisal']['TemplateSampleSection']['CodDescrEng']="章節編號/標題(英文)";
$Lang['Appraisal']['TemplateSampleSection']['CodDescrChi']="章節編號/標題(中文)";
$Lang['Appraisal']['TemplateSampleSection']['DisplayOrder']="(排序設定)";
$Lang['Appraisal']['TemplateSampleSection']['Delete']="(刪除)";
$Lang['Appraisal']['TemplateSampleSection']['NoTitleAndCod']="[無編號及標題]";
$Lang['Appraisal']['TemplateSample']['CurrentTemplate']="目前表格";
$Lang['Appraisal']['TemplateSample']['SDFSecBasicInformation']="章節基本資料";
$Lang['Appraisal']['TemplateSample']['SDFSecCodEng']="章節編號 (英文)";
$Lang['Appraisal']['TemplateSample']['SDFSecCodChi']="章節編號 (中文)";
$Lang['Appraisal']['TemplateSample']['SDFSecTitleEng']="章節標題(英文)";
$Lang['Appraisal']['TemplateSample']['SDFSecTitleChi']="章節標題(中文)";
$Lang['Appraisal']['TemplateSample']['SDFSecDescrEng']="章節描述(英文)";
$Lang['Appraisal']['TemplateSample']['SDFSecDescrChi']="章節描述(中文)";
$Lang['Appraisal']['TemplateSample']['SDFSecDescrChi']="章節描述(中文)";
$Lang['Appraisal']['TemplateSample']['SDFSecAppRoleName']="考績角色";
$Lang['Appraisal']['TemplateSample']['SDFSecCanBrowse']="可閱讀此章節";
$Lang['Appraisal']['TemplateSample']['SDFSecCanFill']="可填寫章節";
$Lang['Appraisal']['TemplateSample']['SDFSubSecInclude']="包含內章節";
$Lang['Appraisal']['TemplateSampleSection']['SubSecCodDescrEng']="內章節編號/標題(英文)";
$Lang['Appraisal']['TemplateSampleSection']['SubSecCodDescrChi']="內章節編號/標題(中文)";
$Lang['Appraisal']['TemplateSampleSection']['SubSecDisplayOrder']="(排序設定)";
$Lang['Appraisal']['TemplateSampleSection']['SubSecDelete']="(刪除)";
$Lang['Appraisal']['TemplateSample']['CurrentParentSection']="章節";
$Lang['Appraisal']['TemplateSample']['SDFSubSecBasicInformation']="內章節基本資料";
$Lang['Appraisal']['TemplateSample']['SDFSubSecCodEng']="內章節編號 (英文)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecCodChi']="內章節編號 (中文)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecTitleEng']="內章節標題(英文)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecTitleChi']="內章節標題(中文)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecDescrEng']="內章節描述(英文)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecDescrChi']="內章節描述(中文)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecDescrChi']="內章節描述(中文)";
$Lang['Appraisal']['TemplateSample']['SDFQstCat']="包含評審題目分組";
$Lang['Appraisal']['TemplateSampleQCat']['SDFDescrEng']="評審題目分組編號/名稱(英文)";
$Lang['Appraisal']['TemplateSampleQCat']['SDFDescrChi']="評審題目分組編號 /名稱 (中文)";
$Lang['Appraisal']['TemplateSampleQCat']['SDFDisplayOrder']="(排序設定)";
$Lang['Appraisal']['TemplateSampleQCat']['SDFDelete']="(刪除)";
$Lang['Appraisal']['TemplateSampleQCat']['NoTitleAndCod']="[無編號及標題]";
$Lang['Appraisal']['TemplateSampleQCat']['ComfirmNoQuesLeave']="尚未建立評審題目。如要發佈此表格，輸入矩陣的資料將無法記錄。你確定離開嗎？";

$Lang['Appraisal']['TemplateSample']['CurrentSubSection']="內章節";
$Lang['Appraisal']['TemplateSample']['SDFQCatBasicInformation']="評審題目分組基本資料";
$Lang['Appraisal']['TemplateSample']['SDFQCatCodEng']="評審題目分組編號 (英文)";
$Lang['Appraisal']['TemplateSample']['SDFQCatCodChi']="評審題目分組編號 (中文)";
$Lang['Appraisal']['TemplateSample']['SDFQCatDescrEng']="評審題目分組名稱 (英文)";
$Lang['Appraisal']['TemplateSample']['SDFQCatDescrChi']="評審題目分組名稱 (中文)";
$Lang['Appraisal']['TemplateSample']['SDFQCatDisplayMode']="題目顯示模式";
$Lang['Appraisal']['TemplateSample']['SDFTextboxNote']="單位";
$Lang['Appraisal']['TemplateSample']['SDFAvgMark']="如有多位考績者, 列印「個人年度考績結果」時只會顯示每條題目的平均分?";
$Lang['Appraisal']['TemplateSample']['SDFAvgDec']="各題目平均分小數點位 (0-3位小數)";
$Lang['Appraisal']['TemplateSample']['SDFVerAvgMark']="同時顯示此分組內所有題目的匯總平均分?";
$Lang['Appraisal']['TemplateSample']['SDFVerAvgDec']="匯總平均分小數點位 (0-3位小數)";
$Lang['Appraisal']['TemplateSample']['SDFAvg']="(平均分 %d小數)";
$Lang['Appraisal']['TemplateSample']['SDFVerAvg']="(所有題目的匯總平均分 %d小數)";

$Lang['Appraisal']['TemplateSample']['0'] = "否";
$Lang['Appraisal']['TemplateSample']['1'] = "是";
$Lang['Appraisal']['TemplateSample']['IsObs']="教務評審表格?";

$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['0']="正常";
$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['1']="李克特量表";
$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['2']="矩陣";
$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['0']="(所有題目獨立設定屬性和答題模式)";
$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['1']="(所有題目共享同一組李克特量表刻度)";
$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['2']="(矩陣內的所有題目會有相同的欄目)";

$Lang['Appraisal']['TemplateSample']['SDFLikertInput']="李克特量表刻度";
$Lang['Appraisal']['TemplateSampleLikert']['SDFMarkCodEng']="刻度編號 (英文)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFMarkCodChi']="刻度編號 (中文)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFDescrEng']="刻度描述 (英文)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFDescrChi']="刻度描述(中文)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFScore']="對應分數";
$Lang['Appraisal']['TemplateSampleLikert']['SDFDisplayOrder']="(排序設定)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFDelete']="(刪除)";
$Lang['Appraisal']['TemplateSampleLikert']['NoTitleAndCod']="[無編號及標題]";

$Lang['Appraisal']['TemplateSample']['SDFMatrixInput']="矩陣輸入欄位";
$Lang['Appraisal']['TemplateMatrix']['SDFFldHdrEng']="輸入欄位名稱(英文)";
$Lang['Appraisal']['TemplateMatrix']['SDFFldHdrChi']="輸入欄位名稱(中文)";
$Lang['Appraisal']['TemplateMatrix']['SDFInputType']="欄位輸入模式";
$Lang['Appraisal']['TemplateMatrix']['SDFDisplayOrder']="(排序設定)";
$Lang['Appraisal']['TemplateMatrix']['SDFDelete']="(刪除)";
$Lang['Appraisal']['TemplateMatrix']['SDFInputText']="文字";
$Lang['Appraisal']['TemplateMatrix']['SDFInputMultipleText']="文字(多行)";
$Lang['Appraisal']['TemplateMatrix']['SDFInputNumeric']="數值";
$Lang['Appraisal']['TemplateMatrix']['SDFInputDate']="日期";
$Lang['Appraisal']['TemplateMatrix']['NoTitleAndCod']="[無編號及標題]";

$Lang['Appraisal']['TemplateSample']['SDFQuestion']="包含評審題目";
$Lang['Appraisal']['TemplateQuestion']['SDFCodDescrEng']="評審題目編號/標題 (英文)";
$Lang['Appraisal']['TemplateQuestion']['SDFCodDescrChi']="評審題目編號/標題 (中文)";
$Lang['Appraisal']['TemplateQuestion']['SDFDisplayOrder']="(排序設定)";
$Lang['Appraisal']['TemplateQuestion']['SDFDelete']="(刪除)";
$Lang['Appraisal']['TemplateQuestion']['NoTitleAndCod']="[無編號及標題]";

$Lang['Appraisal']['TemplateSample']['SDFAnswer']="選擇題答案";
$Lang['Appraisal']['TemplateAnswer']['SDFCodEng']="答案編號 (英文)";
$Lang['Appraisal']['TemplateAnswer']['SDFCodChi']="答案編號 (中文)";
$Lang['Appraisal']['TemplateAnswer']['SDFDescrEng']="答案描述 (英文)";
$Lang['Appraisal']['TemplateAnswer']['SDFDescrChi']="答案描述 (中文)";
$Lang['Appraisal']['TemplateAnswer']['SDFDisplayOrder']="(排序設定)";
$Lang['Appraisal']['TemplateAnswer']['SDFDelete']="(刪除)";
$Lang['Appraisal']['TemplateAnswer']['NoAns']="[無答案]";
$Lang['Appraisal']['TemplateAnswer']['NoAnsDescr']="[無答案描述]";

$Lang['Appraisal']['TemplateSample']['CurrentQCatSection']="評審題目分組";
$Lang['Appraisal']['TemplateSample']['SDFLikertInputInfo']="李克特量表刻度資料";
$Lang['Appraisal']['TemplateSample']['SDFMatrixInputInfo']="矩陣輸入欄位資料";

$Lang['Appraisal']['TemplateSample']['CurrentQuesSection']="評審題目";
$Lang['Appraisal']['TemplateQues']['SDFQuesBasicInformation']="評審題目基本資料";
$Lang['Appraisal']['TemplateQues']['SDFQCodEng']="評審題目編號(英文)";
$Lang['Appraisal']['TemplateQues']['SDFQCodChi']="評審題目編號(中文)";
$Lang['Appraisal']['TemplateQues']['SDFDescrEng']="評審題目描述(英文)";
$Lang['Appraisal']['TemplateQues']['SDFDescrChi']="評審題目描述(中文)";
$Lang['Appraisal']['TemplateQues']['SDFDescr']="評審題目描述";
$Lang['Appraisal']['TemplateQues']['SDFInputType']="答題模式";
$Lang['Appraisal']['TemplateQues']['SDFTxtBoxNote']="- 特註/計量單位 (於文字輸入欄後顯示)";
$Lang['Appraisal']['TemplateQues']['SDFHasRmk']="需輸入額外備註?";
$Lang['Appraisal']['TemplateQues']['SDFRmkLabelEng']="- 備註欄名稱(英文)";
$Lang['Appraisal']['TemplateQues']['SDFRmkLabelChi']="- 備註欄名稱(中文)";
$Lang['Appraisal']['TemplateQues']['SDFMandatory']="此題必須作答?";
$Lang['Appraisal']['TemplateQues']['SDFAvgMark']="如有多位考績者, 列印「個人年度考績結果」時只會顯示此題目的平均分?";
$Lang['Appraisal']['TemplateQues']['SDFAvgDec']="題目平均分小數點位 (0-3位小數)";

$Lang['Appraisal']['TemplateQues']['SDFInputTypeEmpty']="備註";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeMC']="選擇題";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeText']="短答";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeScore']="分數輸入";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeDate']="日期輸入";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeMultiText']="多行作答";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeMCM']="選擇題(多選)";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeUpload']="附件上載";

$Lang['Appraisal']['Template']['RecordID']="紀錄編號";
$Lang['Appraisal']['Template']['BatchID']="遞交批次";

$Lang['Appraisal']['TitleBar']['DownloadDocument']="填表指引";
$Lang['Appraisal']['TitleBar']['FormCompleted']="已完成表格";
$Lang['Appraisal']['TitleBar']['FormToBeEdited']="待編輯表格";

$Lang['Appraisal']['Template']['UsedNoDeletion']="評審表格範本已使用，不能刪除";

// U

// V

// W

// X

// Y

// Z

if(file_exists($intranet_root."/lang/cust/appraisal_lang.b5.php")){
	include_once($intranet_root."/lang/cust/appraisal_lang.b5.php");
}
?>