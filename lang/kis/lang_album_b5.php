<?
$kis_lang['albumlist']			= "相簿清單";
$kis_lang['photos']			= "相片";
$kis_lang['albums']			= "相簿";
$kis_lang['createalbum']		= "新增相簿";
$kis_lang['albumtitle']			= "相簿標題";
$kis_lang['description']		= "相簿描述";
$kis_lang['addphotos']			= "加入相片";
$kis_lang['photoname']			= "相片名稱";
$kis_lang['uploaddate']			= "上載日期";
$kis_lang['datetaken']			= "拍照日期";
$kis_lang['orderby']			= "排序";

?>