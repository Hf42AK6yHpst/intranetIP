<?
$kis_lang['present']			= "Present";
$kis_lang['late']			= "Late";
$kis_lang['earlyleave']			= "Early Leave";
$kis_lang['absent']			= "Absent";
$kis_lang['totalschooldays']		= "Total School Days";
$kis_lang['outing']			= "Outing";
$kis_lang['in']				= "In";
$kis_lang['out']			= "Out";
$kis_lang['carriedby']			= "Carried by";
$kis_lang['reason']			= "Reason";
$kis_lang['recordedby']			= "Recorded by";
$kis_lang['takeattendance']		= "Take Attendance";
$kis_lang['monthlyrecord']		= "Monthly Record";
$kis_lang['remainderrecord']		= "Remainder Record";
$kis_lang['past']			= "Past";
$kis_lang['coming']			= "Coming";
$kis_lang['selectclassname']		= "Select Class Name";
$kis_lang['format']			= "Format";
$kis_lang['hidedatewithoutdata']	= "Hide date without data";
$kis_lang['studentstatus']		= "Student status";
$kis_lang['displayinread']		= "Display in red when continuous absent for";
$kis_lang['showallcloumns']		= "Show all columns";
$kis_lang['web']			= "Web";
$kis_lang['excel']			= "Excel";
$kis_lang['web']			= "Web";
$kis_lang['active']			= "Active";
$kis_lang['suspended']			= "Suspended";
$kis_lang['todisable']			= "to disable";
$kis_lang['monthdetails']		= "Month Details";
$kis_lang['by']				= "by";

?>