<?
$kis_lang['albumlist']			= "Album List";
$kis_lang['photos']			= "Photo(s)";
$kis_lang['albums']			= "Album(s)";
$kis_lang['createalbum']		= "Create Album";
$kis_lang['albumtitle']			= "Album title";
$kis_lang['description']		= "Description";
$kis_lang['addphotos']			= "Add Photos";
$kis_lang['photoname']			= "Photo Name";
$kis_lang['uploaddate']			= "Upload Date";
$kis_lang['datetaken']			= "Date Taken";
$kis_lang['orderby']			= "Order by";
$kis_lang['dragphotoshere']		= "Drag photos here";
?>