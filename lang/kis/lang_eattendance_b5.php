<?
$kis_lang['present']			= "出席";
$kis_lang['late']			= "達到";
$kis_lang['earlyleave']			= "早退";
$kis_lang['absent']			= "缺席";
$kis_lang['totalschooldays']		= "總上課日";
$kis_lang['outing']			= "外出";
$kis_lang['in']				= "進入";
$kis_lang['out']			= "離開";
$kis_lang['carriedby']			= "接送人";
$kis_lang['reason']			= "原因";
$kis_lang['recordedby']			= "記錄人";
$kis_lang['takeattendance']		= "考勤登記";
$kis_lang['monthlyrecord']		= "每月記錄";
$kis_lang['remainderrecord']		= "剩餘記錄";
$kis_lang['past']			= "過去";
$kis_lang['coming']			= "將來";
$kis_lang['selectclassname']		= "選擇班別";
$kis_lang['format']			= "格式";
$kis_lang['hidedatewithoutdata']	= "隱藏空白記錄";
$kis_lang['studentstatus']		= "學生狀態";
$kis_lang['displayinred']		= "標註持續缺席的日子";
$kis_lang['showallcloumns']		= "顯示所有欄";
$kis_lang['web']			= "網頁";
$kis_lang['excel']			= "Excel";
$kis_lang['active']			= "有效";
$kis_lang['suspended']			= "暫停";
$kis_lang['todisable']			= "取消";
$kis_lang['monthdetails']		= "每日記錄";
$kis_lang['by']				= "記錄人";

?>