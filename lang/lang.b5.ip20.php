<?php
# using: 

// Please do NOT add/ update/ delete any of the entry in this file (By Kenneth Chung 20090813)
// if you wish to add a language, please add it in lang.b5.php

/********************** Change Log ***********************/
# Date	: 2015-06-16 [Jason]
#				replace split() by explode() in order to support php 5.4+
#
/******************* End Of Change Log *******************/

$i_title = "eClass 綜合平台";
$i_home_title  = "eClass 綜合平台";
$i_admin_title = "綜合平台行政管理中心";
$i_no_record_exists_msg = "暫時仍未有任何紀錄";
$i_no_record_exists_msg2 = "暫時仍未有任何紀錄";
$i_no_record_searched_msg = "找不到所需記錄";
$i_no_search_result_msg = "很抱歉，找不到和你的查詢相符的資料。請修訂你的查詢句並再嘗試。";
$i_alert_or = "或";
$i_alert_pleasefillin = "請填上 ";
$i_alert_pleaseselect = "請選擇 ";
$i_select_file = "選擇檔案";
$i_general_startdate = "開始日期";
$i_general_enddate = "結束日期";
$i_general_title = "題目";
$i_general_description = "說明";
$i_general_status = "狀況";
$i_general_clickheredownloadsample = "按此下載範例";
$i_general_sysadmin = "<B>系統管理員</B>";
$i_general_WholeSchool = "全校";
$i_general_TargetGroup = "目標小組";
$i_general_Poster = "發出人";
$i_general_BasicSettings = "基本設定";
$i_general_NotSet = "不設定";
$i_general_EachDisplay = "每頁顯示";
$i_general_PerPage = "項";
$i_general_DisplayOrder = "顯示次序";
$i_general_Teacher = "老師";
$i_general_Day = "日";
$i_general_Days = "日";
$i_general_Month = "月";
$i_general_Months = "月";
$i_general_Year = "年";
$i_general_Years = "年";
$i_general_Format = "格式";
$i_general_Format_web = "Web";
$i_general_Format_word = "MSWord";
$i_general_Type = "種類";
$i_general_SelectTarget = "選擇目標";
$i_general_SelectOption = "選項";
$i_general_ViewDetailRecords = "檢視詳細紀錄";
$i_general_MonthShortForm = array(
"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
);
$i_general_more = "更多";
$i_general_subject_teacher = "科目教師";
$i_general_ImportFailed = "以下的紀錄不能被匯入";
$i_general_targetParent = "的家長";
$i_general_select_parent = "選擇家長";
$i_general_targetGuardian = " 的監護人";
$i_general_select_guardian_from_student = "從學生名字選擇監護人";

$i_general_record_date = "紀錄日期";
$i_general_record_time = "紀錄時間";
$i_general_show_child = "顯示他的子女";
$i_general_BackToTop = "頁首";
$i_general_students_selected = "已選擇學生";
$i_general_selected_students = "已選擇學生";
$i_general_selected_teachers = "已選擇老師";
$i_general_search_again = "重新搜尋";
$i_general_view_past = "檢視昔日紀錄";
$i_general_english = "英文";
$i_general_chinese = "中文";
$i_general_yes = "是";
$i_general_yes2 = "有";
$i_general_no = "否";
$i_general_active = "可用";
$i_general_inactive = "停用";
$i_general_main_menu = "返回清單";
$i_general_logout = "登出";
$i_general_steps = "步驟";
$i_general_required_field = "附有「<span class='tabletextrequire'>*</span>」的項目必須填寫";
$i_general_required_field2 = "附有「<span class='tabletextrequire'>*</span>」的項目必須填寫";
$i_general_display_format = "顯示形式";

$i_general_alternative = "其他方式";
$i_general_expand_all = "展開全部";
$i_general_collapse_all = "摺疊全部";
$i_general_report_creation_time="報表建立時間";
$i_general_last_modified_by="最後修改人";
$i_general_no_privileges = "你沒有檢視特權。";
$i_general_icon_list = "圖示表";
## Added on 18-7-2007 by Andy
$i_general_all_classes = "全部班別";
$i_general_all_houses = "全部社";
$i_general_refresh = "重新整理";
$i_general_refreshed = "重新整理完成";

$i_general_choose_student = "選擇學生";
$i_general_from_class_group = "自 <b>班別</b> / <b>組別</b>";
$i_general_from_class = "自 <b>班別</b>";
$i_general_or = "或";
$i_general_search_by_inputformat = "輸入\"[班名] [學號] [姓名]\"<br>(e.g. 1a15 Chan) ";
$i_general_search_by_loginid = "以登入名稱搜尋";

$i_general_select_csv_file = "選擇CSV檔案";
$i_general_imported_result = "匯入結果";
$i_general_confirm_import_data = "確認要匯入紀錄";
$i_general_complete = "完成";

$i_general_enabled = "可用";
$i_general_disabled = "停用";
$i_general_orderby = "排序";
$i_general_target = "對象";
$i_general_class = "班別";
$i_general_name = "姓名";
$i_general_orderby_class = "班別, 班號";
$i_general_count = "次數";
$i_general_display_details = "顯示詳細紀錄";
$i_general_all = "全部";
$i_general_no_access_right = "你沒有權限觀看此頁。";
$i_general_highest = "最高";
$i_general_most = "最多";
$i_general_every = "每";
$i_general_first = "首";
$i_general_another = "再";
$i_general_instance_of = "次的";
$i_general_will_be_count_as = "將轉換為一次";
$i_general_receive = "獎懲數量";
$i_general_show = "顯示";
$i_general_loading = "載入中...";
$i_general_timeout_relogin = "逾時操作, 請重新登入系統。";

# 20090324 yatwoon
$i_general_level = "等級";
$i_general_lowest = "最低";

$i_general_na = "不適用";
$i_general_print_date = "列印日期";

$i_email_to = "致";
$i_email_subject = "主旨";
$i_email_message = "內容";
$i_email_sendemail = "傳送電郵";
$i_From = "由";
$i_To = "至";
$i_LastModified = "最後修改日期";
$i_status_approve = "批准";
$i_status_approved = "已批准";
$i_status_suspend = "暫停";
$i_status_suspended = "已暫停";
$i_status_pending = "未發佈";
$i_status_pendinguser = "未批准";
$i_status_graduate = "已離校";
$i_status_publish = "已發佈";
$i_status_published = "已發佈";
$i_status_checkin = "已登記";
$i_status_checkout = "已退回";
$i_status_cancel = "已取消";
$i_status_reserved = "預訂的";
$i_status_rejected = "已被拒絕";
$i_status_all = "全部";
$i_status_activated = "使用中";
$i_status_template = "範本";
$i_status_waiting = "等待批核";
$i_invalid_email = "電郵地址不符";
$i_invalid_date = "日期不符";
$i_gender_male = "男性";
$i_gender_female = "女性";
$i_title_mr = "先生";
$i_title_miss = "小姐";
$i_title_mrs = "太太";
$i_title_ms = "女士";
$i_title_dr = "博士";
$i_title_prof = "教授";
$i_export_msg1 = "分隔符號";
$i_export_msg2 = "匯出指定項目";
$i_import_msg1 = "1. 選擇匯入用戶的格式";
$i_import_msg2 = "2. 選擇需要匯入的檔案";
$i_import_msg3 = "按匯入鍵來上載及匯入檔案";
$i_import_msg4 = "甚麼是eClass .CSV檔案？";
$i_import_msg5 = "eClass .CSV 檔案是一個已被編排格式的 .CSV 檔案：";
$i_import_msg6 = "包含所有名單上的項目";
$i_import_msg7 = "例如：登入帳號、密碼、用戶電郵、英文名稱、中文名稱";
$i_import_msg8 = "可以在 Microsoft Excel 中編輯";
$i_import_msg9 = "下載 eClass.csv 檔案範本";
$i_import_msg10 = "3. 最少選擇一個身份組別項目";
$i_import_msg_note = "備註:<br>
<ul>
<li>檔案中的 'UserLogin' 須由 'a-z'(小楷), '0-9' 及 '_' 組成, 而且<b>必須</b>使用 'a-z' 為開始字元
<li>如果檔案中的 'UserLogin' 已經存在, 該帳戶所有資料會被更新(包括電郵及密碼)
<li>如欲不更新電郵及密碼, 可在檔案中留空該欄</ul>";
$i_import_msg_note2 = "備註二:<br>
<ul><li>為免遺失號碼前端的數字 \"0\"，請在每個 WebSAMS 註冊號碼前加上 \"#\" 號 (如: <b>#</b>0012345 )</ul>";

$i_import_invalid_format = "你上載之檔案格式並不符合. 資料順序應為: ";
$i_groupimport_msg = "選擇需要匯入的檔案";
$i_groupimport_type = "新增的組別種類";
$i_groupimport_fileformat = "檔案格式";
$i_groupimport_fileformat2 = "檔案中的第一欄須為內聯網帳號.<br>
第二欄則為該組別的名稱, 如該組別不存在, 系統將會自動新增.";
$i_groupimport_sample = "按此下載範本檔案 (CSV 格式)";

$i_import_big5 = "以Big-5格式匯入";
$i_import_gb = "以GB格式匯入";
$i_import_utf = "以UTF格式匯入";
$i_import_utf_type = "請上載UTF格式之檔案。";

$i_identity = "身份";
$i_identity_teachstaff = "教職員";
$i_identity_student = "學生";
$i_identity_parent = "家長";
$i_identity_alumni = "校友";
$i_identity_array = array("",$i_identity_teachstaff,$i_identity_student,$i_identity_parent,$i_identity_alumni);

$i_import_teacher_data = "匯入$i_identity_teachstaff"."資料";
$i_import_student_data = "匯入$i_identity_student"."資料";
$i_import_parent_data = "匯入$i_identity_parent"."資料";
$i_import_alumni_data = "匯入$i_identity_alumni"."資料";
$i_import_identity_array = array("",$i_import_teacher_data,$i_import_student_data,$i_import_parent_data,$i_import_alumni_data);

$i_teachingStaff = "教學職務員工";
$i_nonteachingStaff = "非教學職務員工";
$i_teachingDifference = "(教學職員會被自動加入 Teacher 小組; 非教學職員會加入 Admin Staff 小組)";

$i_ifapplicable = "如適用";
$i_notapplicable = "不適用";

$i_ClassLevel = "級別";
$i_ClassLevelName = "級別名稱";
$i_ClassName = "班別";
$i_GroupName = "小組";
$i_ClassNumber = "學號";
$i_Class_ImportInstruction = "<b><u>檔案說明:</u></b><br>
第一欄 : 班級名稱 (如 1A ).<br>
第二欄 : 級別 (如 F.1).<br>";
$i_Class_DownloadSample = "按此下載範例";

$i_SettingsSchoolName = "學校名稱";
$i_SettingsOrganization = "辦學團體";
$i_SettingsSchoolPhone = "學校電話";
$i_SettingsSchoolAddress = "學校地址";
$i_SettingsCurrentAcademicYear = "本年度";
$i_SettingsSemester = "學期";
$i_SettingsSchoolDayType = "日制";
$i_SettingsCurrentSemester = "現時";
$i_SettingsSemesterList = "學期";
$i_SettingsSchool_Class = "班級設定";
$i_SettingsSchool_ClassTeacher = "老師職務調配";
$i_SettingsSchool_Forms = "學校自訂表格";

### added by ronald 20081118 ###
$i_SettingSemesterMode = "學期更新模式";
$i_SettingSemesterManualUpdate = "用戶自行更新";
$i_SettingSemesterAutoUpdate = "系統自行更新";
$i_SettingSemesterSetting_Alert1 = "請填上開始日期";
$i_SettingSemesterSetting_Alert2 = "請填上結束日期";
$i_SettingSemesterSetting_Alert3 = "輸入的日期無效";
$i_SettingSemesterSetting_Alert4 = "輸入的日期排列次序無效，請重新輸入。";
######

$i_DayTypeWholeDay = "全日";
$i_DayTypeAM = "上午";
$i_DayTypePM = "下午";
$i_DayTypeSession = "節數";
$i_DayTypeArray = array(
"",
$i_DayTypeWholeDay,
$i_DayTypeAM,
$i_DayTypePM);

$i_wordtemplates_select = "選擇類型";
$i_wordtemplates_wordings = "預設字句 (以每行為一字句)";
$i_wordtemplates_attendance = "遲到/缺席/早退原因";
$i_wordtemplates_merit = "獎勵原因";
$i_wordtemplates_demerit = "懲罰原因";
$i_wordtemplates_activity = "活動名稱";
$i_wordtemplates_performance = "表現";
$i_wordtemplates_service = "服務名稱";
$i_wordtemplates_service_role = "服務職位";
$i_wordtemplates_activity_role = "活動職位";
$i_wordtemplates_award_name = "獎項名稱";
$i_wordtemplates_award_role = "獎項職位";

$i_wordtemplates_type_array = array(
$i_wordtemplates_attendance,
$i_wordtemplates_merit,
$i_wordtemplates_demerit
);

$i_wordtemplates_instruction = "你可以更改預設字句, 以每一行為一字句. <br>更改後請按更新儲存.";

$i_menu_setting = "內聯網設定";
$i_menu_intranet = "內聯網管理";
$i_menu_eclass = "網上教室管理";
$i_menu_network = "網絡系統管理";

# menu name
$i_adminmenu_group = "組別";
$i_adminmenu_user = "用戶";
$i_adminmenu_announcement = "宣佈";
$i_adminmenu_event = "事項";
$i_adminmenu_polling = "投票";
$i_adminmenu_timetable = "小組時間表";
$i_adminmenu_resource = "資源清單";
$i_adminmenu_booking = "預訂";
$i_adminmenu_staffdirectory = "職員名冊";
$i_adminmenu_filemgmt = "檔案管理";
$i_adminmenu_eclass = "網上教室";
$i_adminmenu_role = "職位";
$i_adminmenu_usermgmt = "用戶管理";
$i_adminmenu_infomgmt = "資訊管理";
$i_adminmenu_resourcemgmt = "資源管理";
$i_adminmenu_eclassmgmt = "網上教室管理";
$i_adminmenu_referencefiles = "參考檔案";
$i_adminmenu_motd = "流動資訊";
$i_adminmenu_sysmgmt = "系統管理";
$i_adminmenu_email_setting = "電子郵件設定";
//$i_adminmenu_admin_account = "管理人戶口";
$i_adminmenu_rbps = "資源預訂時期設定";

$i_admintitle_group = "組別";
$i_admintitle_user = "用戶";
$i_admintitle_announcement = "宣佈";
$i_admintitle_event = "事項";
$i_admintitle_polling = "投票";
$i_admintitle_timetable = "時間表";
$i_admintitle_resource = "資源";
$i_admintitle_tmpfiles = "暫存檔案";
$i_admintitle_booking = "預訂";
$i_admintitle_staffdirectory = "職員名冊";
$i_admintitle_filemgmt = "檔案管理";
$i_admintitle_eclass = "網上教室";
$i_admintitle_role = "職位";
$i_admintitle_usermgmt = "用戶管理";
$i_admintitle_infomgmt = "資料管理";
$i_admintitle_resourcemgmt = "資源管理";
$i_admintitle_eclassmgmt = "網上教室管理";
$i_admintitle_referencefiles = "參考檔案";
$i_admintitle_motd = "流動資訊";
$i_admintitle_sysmgmt = "系統管理";
$i_admintitle_email_setting = "電子郵件設定";
$i_admintitle_admin_account = "管理人戶口";
$i_admintitle_rbps = "資源預訂時期設定";
$i_admintitle_subjects = "科目";

$i_adminmenu_sa = "系統總管";
$i_adminmenu_sa_email = "電郵設定";
$i_adminmenu_sa_password = "更改密碼";
$i_adminmenu_sa_helper = "系統助手";
$i_adminmenu_sa_pw_leave_blank = "沿用舊密碼請留空";
$i_adminmenu_sc = "系統設定";
$i_adminmenu_sc_globalsetting = "總體設定";
$i_adminmenu_sc_resourcebooking = "資源預訂";
$i_adminmenu_sc_campuslink = "自訂連結";
$i_adminmenu_sc_basic_settings = "基本設定";
$i_adminmenu_sc_school_settings = "學校設定";
$i_adminmenu_sc_group_settings = "小組設定";
$i_adminmenu_sc_user_info_settings = "用戶資料設定";
$i_adminmenu_sc_clubs_enrollment_settings = "學會報名設定";
$i_adminmenu_sc_campustv = "校園電視台";
$i_adminmenu_sc_url = "聯網網址設定";
$i_adminmenu_sc_cycle = "循環日設定";
$i_adminmenu_sc_period = "上課節數設定";
$i_adminmenu_sc_language = "語言設定";
$i_adminmenu_sc_webmail = "網郵設定";
$i_adminmenu_sc_campusmail = "校園通告設定";
$i_adminmenu_sc_homework = "家課紀錄表";
$i_adminmenu_sc_campusquota = "校園信箱儲存量";
$i_adminmenu_sc_wordtemplates = "預設字句";
$i_adminmenu_fs = "功能設定";
$i_adminmenu_fs_homework = "家課紀錄表設定";
$i_adminmenu_fs_campusmail = "校園信箱設定";
$i_adminmenu_fs_resource_set = "資源預訂設定";
$i_adminmenu_sf = "系統檔案";
$i_adminmenu_plugin = "系統整合設定";
$i_adminmenu_plugin_sls_library = "SLS 圖書館系統";
$i_adminmenu_plugin_qb = "中央題目庫";
$i_adminmenu_am = "用戶管理";
$i_adminmenu_am_user = "用戶管理中心";
$i_adminmenu_am_usage = "登入紀錄";
//$i_adminmenu_am_group = "組別";
$i_adminmenu_am_role = "職位";
$i_adminmenu_adm = "行政管理";
$i_adminmenu_adm_academic_record = "學生檔案";
$i_adminmenu_adm_teaching = "教師職務設定";
$i_adminmenu_adm_adminjob = "行政職務設定";
$i_adminmenu_adm_enrollment_approval = "學會報名管理";
$i_adminmenu_adm_enrollment_group = "小組報名情況";
//$i_adminmenu_adm_enrollment_student = "學生報名情況";
$i_adminmenu_im = "日常資訊管理";
$i_adminmenu_im_motd = "提示訊息";
$i_adminmenu_im_announcement = "校園最新消息";
$i_adminmenu_im_event = "校曆表";
$i_adminmenu_im_timetable = "小組時間表";
$i_adminmenu_im_campusmail = "學校通告";
$i_adminmenu_im_polling = "投票";
$i_adminmenu_im_group_bulletin = "小組討論欄";
$i_adminmenu_im_group_files = "小組文件";
$i_adminmenu_im_group_links = "小組連結";
$i_adminmenu_gm = "小組管理";
$i_adminmenu_gm_group = "小組管理中心";
$i_adminmenu_gm_groupfunction = "小組功能管理";
$i_adminmenu_rm = "資源管理";
$i_adminmenu_rm_item = "資源清單";
$i_adminmenu_rm_record = "資源預訂紀錄";
$i_adminmenu_rm_record_periodic = "定期資源預訂紀錄";
$i_adminmenu_us = "使用統計";
$i_adminmenu_us_user = "用戶";
$i_adminmenu_us_group = "組別";
$i_adminmenu_basic_settings_misc = "其他設定";
$i_adminmenu_basic_settings_email = "用戶不能更改電郵地址.";
$i_adminmenu_basic_settings_title = "不顯示'$i_general_Teacher'字眼或稱謂 ($i_identity_teachstaff 及 $i_identity_parent)";
$i_adminmenu_basic_settings_home_tel = "用戶不能更改住宅電話.";
$i_adminmenu_basic_settings_badge = "校徽";
$i_adminmenu_basic_settings_badge_instruction = "圖像只能以'.JPG'、'.GIF' 或'.PNG'的格式上載，大小也規定為 120 X 60 pixel (闊 x 高)。";
$i_adminmenu_basic_settings_badge_use = "顯示校徽";

$i_adminmenu_user_info_settings_disable_1 = "教師不能更改:";
$i_adminmenu_user_info_settings_disable_2 = "學生不能更改:";
$i_adminmenu_user_info_settings_disable_3 = "家長不能更改:";

$i_adminmenu_user_info_settings_display_disable_1 = "老師不顯示";
$i_adminmenu_user_info_settings_display_disable_2 = "學生不顯示";
$i_adminmenu_user_info_settings_display_disable_3 = "家長不顯示";

$i_adminmenu_user_info_settings_display = "顯示";

$i_admintitle_sa = "系統總管";
$i_admintitle_sa_email = "電郵設定";
$i_admintitle_sa_password = "更改密碼";
$i_admintitle_sa_helper = "系統助手";
$i_admintitle_sc = "系統設定";
$i_admintitle_sc_globalsetting = "總體設定";
$i_admintitle_sc_resourcebooking = "資源預訂";
$i_admintitle_sc_campuslink = "自訂連結";
$i_admintitle_sc_url = "聯網網址設定";
$i_admintitle_sc_homework = "家課";
$i_admintitle_sc_subject = "科目清單";
$i_admintitle_sc_cycle = "循環日設定";
$i_admintitle_sc_period = "上課節數設定";
$i_admintitle_sc_language = "語言設定";
$i_admintitle_sc_webmail = "網郵設定";
$i_admintitle_sc_campusmail = "校園通告設定";
$i_admintitle_sc_campusquota = "校園信箱容量";
$i_admintitle_fs = "功能設定";
$i_admintitle_fs_homework = "家課紀錄表設定";
$i_admintitle_fs_homework_subject = "科目設定";
$i_admintitle_fs_official_subject = "增值模組科目設定";
$i_admintitle_fs_campusmail = "校園信箱設定";
$i_admintitle_fs_resource_set = "資源預訂設定";
$i_admintitle_sf = "系統檔案";
$i_admintitle_am = "用戶管理";
$i_admintitle_am_user = "基本用戶";
$i_admintitle_am_group = "組別";
$i_admintitle_am_role = "職位";
$i_admintitle_im = "日常資訊管理";
$i_admintitle_im_motd = "提示訊息";
$i_admintitle_im_announcement = "校園最新消息";
$i_admintitle_im_event = "校曆表";
$i_admintitle_im_timetable = "小組時間表";
$i_admintitle_im_campusmail = "學校通告";
$i_admintitle_im_campusmail_outbox = "通告紀錄";
$i_admintitle_im_campusmail_compose = "發通告";
$i_admintitle_im_campusmail_template = "通告草稿";
$i_admintitle_im_campusmail_trash = "垃圾箱";
$i_admintitle_im_polling = "投票";
$i_admintitle_im_group_bulletin = "小組討論欄";
$i_admintitle_im_group_files = "小組文件";
$i_admintitle_im_group_links = "小組連結";
$i_admintitle_rm = "資源管理";
$i_admintitle_rm_item = "資源清單";
$i_admintitle_rm_record = "資源預訂紀錄";
$i_admintitle_rm_per_record = "定期資源預訂紀錄";
$i_admintitle_us = "使用統計";
$i_admintitle_us_user = "用戶";
$i_admintitle_us_group = "組別";

//$i_admin_status_1 = "處理進行中...";
//$i_admin_status_2 = "請稍候!";

# List Value
$list_total = "總數";
$list_prev = "前一頁";
$list_next = "後一頁";
$list_page = "頁";
$list_sortby = "排序";
$list_desc = "遞減";
$list_asc = "遞增";

# Button Name
$button_add = "增加";
$button_attach = "附件";
$button_approve = "批准";
$button_archive = "存檔";
$button_archive_rb = "保存過去紀錄";
$button_archive1 = "整存";
$button_activate = "恢復使用";
$button_back = "返回";
$button_cancel = "取消";
$button_change = "更改";
$button_check_all = "全部選擇";
$button_checkin = "取用";
$button_checkout = "退還";
$button_clear = "清除";
$button_clear_all = "全部清除";
$button_close = "關閉視窗";
$button_continue = "繼續";
$button_copy = "複製";
$button_deactivate = "停用";
$button_edit = "編輯";
$button_email = "電郵";
$button_erase = "清除";
$button_export = "匯出";
$button_export_all = "全部匯出";
$button_export_pps = "匯出繳費聆之手續費";
$button_export_xml = "匯出 XML";
$button_find = "尋找";
$button_finish = "完成";
$button_image = "影像";
$button_import = "匯入";
$button_import_xml = "匯入 XML";
$button_move = "移動";
$button_moveto = "移至";
$button_new = "新增";
$button_newfolder = "新增資料夾";
$button_next = "下一步";
$button_next_page = "下一頁";
$button_pay = "繳款";
$button_pending = "未發佈";
$button_preview = "預覽";
$button_save_preview = "儲存 / 儲存及預覽";
$button_previous_page = "上一頁";
$button_quickadd = "特快新增";
$button_quicksearch = "尋找";
$button_delete = "刪除";
$button_remove = "刪除";
$button_remove_selected = "移除已選";
$button_remove_selected_student = "移除已選擇學生";
$button_skip = "略過";
$button_add_attachment = "附加文檔";
$button_remove_all = "全部刪除";
$button_rename = "重新命名";
$button_reject = "拒絕";
$button_unlock = "解鎖";
$button_lock = "封鎖";
$button_release = "開放";
$button_unrelease = "取消開放";
$button_waive = "豁免";
$button_unwaive = "取消「豁免」";
$button_reset = "重設";
$button_reserve = "預訂";
$button_restore = "復原";
$button_save = "儲存";
$button_save_as = "另存";
$button_save_as_new = "另存";
$button_save_draft = "儲存草稿";
$button_save_as_draft = "儲存草稿";
$button_save_continue = "儲存及繼續";
$button_submit_continue = "呈送及繼續";
$button_submit_preview = "呈送及預覽";
$button_search = "尋找";
$button_select = "選擇";
$button_select_situation = "選擇發放原因";
$button_select_template = "選擇模板";
$button_send = "傳送";
$button_submit = "呈送";
$button_suspend = "暫停";
$button_swap = "轉換";
$button_use_default = "使用預設值";
$button_undo = "還原至未繳";
$button_unzip = "解壓";
$button_update = "更新";
$button_upload = "上載";
$button_view = "檢視";
$button_emailpassword = "傳送密碼給使用者";
$button_select_all = "全選";
$button_updateperformance = "更新表現";
$button_print = "列印";
# 20090227 yat woon
$button_print_all = "列印所有資料";
$button_print_selected = "列印已選資料";

$button_quit = "離開";
$button_assignsubjectleader = "設定科長";
$button_confirm = "確認";
$button_emptytrash = "清理垃圾箱";
$button_set = "設定";
$button_more_file = "添加更多檔案";
$button_discard = "捨棄";
$button_hide = "隱藏";
$button_notify = "提醒";
$button_view_template = "檢視範本";
$button_view_statistics = "檢視統計";
$button_hide_statistics = "隱藏統計";
$button_print_statistics = "列印統計";


# General Confirmation Message
$i_con_gen_msg_add = "已增加紀錄";
$i_con_gen_msg_update = "已更新紀錄";
$i_con_gen_msg_delete = "已刪除紀錄";
$i_con_gen_msg_email = "已寄出電郵";
$i_con_gen_msg_email_remove = "已刪除電郵";
$i_con_gen_msg_email_restore = "已還原電郵";
$i_con_gen_msg_email_save = "已儲存電郵";
$i_con_gen_msg_password1 = "已更改密碼";
$i_con_gen_msg_password2 = "密碼不符";
$i_con_gen_msg_password3 = "新舊密碼不可相同";
$i_con_gen_msg_email1 = "已更改電郵地址";
$i_con_gen_msg_email2 = "不能更改電郵地址，原因：電郵地址已被另一用戶登記。";
$i_con_gen_msg_photo_delete = "已刪除相片";

# Confirmation Message
$i_con_msg_add = "<font color=green>已增加紀錄。</font>\n";
$i_con_msg_add_failed = "<font color=red>無法新增紀錄。</font>";
$i_con_msg_update = "<font color=green>已更新紀錄。</font>\n";
$i_con_msg_update_failed = "<font color=red>更新紀錄失敗。</font>\n";
$i_con_msg_delete = "<font color=green>已刪除紀錄。</font>\n";
$i_con_msg_delete_failed = "<font color=red>刪除紀錄失敗。</font>\n";
$i_con_msg_email = "<font color=green>已寄出電郵。</font>\n";
$i_con_msg_password1 = "<font color=green>已更改密碼。</font>";
$i_con_msg_password2 = "<font color=red>密碼不符。</font>";
$i_con_msg_password3 =" <font color=red>新舊密碼不可相同。</font>";
$i_con_msg_email1 = "<font color=green>已更改電郵地址。</font>";
$i_con_msg_email2 = "<font color=red>不能更改電郵地址，原因：電郵地址已被另一用戶登記。</font>";
$i_con_msg_archive = "<font color=green>已成功存檔。</font>";
$i_con_msg_import_success = "<font color=green>已成功匯入。</font>";
$i_con_msg_date_wrong = "<font color=red>日期輸入錯誤。</font>";
$i_con_msg_admin_modified = "<font color=green>已設定管理員。</font>";
$i_con_msg_suspend = "<font color=green>已更新紀錄。</font>\n";
$i_con_msg_cannot_edit_poll = "<font color=red>不能更改已開始的投票。</font>\n";
$i_con_msg_photo_delete = "<font color=green>已刪除相片。</font>";
$i_con_msg_import_failed = "<font color=red>檔案格式無效。</font>";
$i_con_msg_import_failed2 = "<font color=red>匯入檔案失敗。</font>";
$i_con_msg_import_header_failed = "<font color=red>標題格式不正確。</font>";
$i_con_msg_grade_generated = "<font color=green>操行等級計算完畢。</font>";
$i_con_msg_import_invalid_login="<font color=red>內聯網帳號不符。</font>";
$i_con_msg_user_add_failed = "<font color=red>新增用戶失敗。 (內聯網帳號及電子郵件不符規格或已存在)</font>";
$i_con_msg_date_startend_wrong_alert = "日期錯誤. 結束日期不能早於開始日期。";
$i_con_msg_date_startend_wrong = "<font color=red>$i_con_msg_date_startend_wrong_alert</font>";
$i_con_msg_date_start_wrong_alert = "日期錯誤. 開始日期必須是今天或以後。";
$i_con_msg_date_start_wrong = "<font color=red>$i_con_msg_date_start_wrong_alert</font>";
$i_con_msg_add_form_failed = "<font color=red>失敗. 表格名稱必須是唯一。</font>";
$i_con_msg_delete_part = "<font color=red>部份已刪除。 (只能刪除非預設及沒有小組的類別)</font>";
$i_con_msg_enrollment_next = "<font color=green>學生可以進行下一輪選擇 (你可能需要更改報名日期配合)。</font>";
$i_con_msg_enrollment_confirm = "<font color=green>已批核的報名名單已加進小組。</font>";
$i_con_msg_enrollment_lottery_completed = "<font color=green>已完成抽籤程序。</font>";
$i_con_msg_subject_leader_updated = "<font color=green>科長設定已更新。</font>";
$i_con_msg_sports_house_group_existed = "<font color=red>內聯網小組已被選用。</font>";
$i_con_msg_voted = "<font color=green>投票完成。</font>";
$i_con_msg_delete_following_failed = "<font color=red>以下紀錄未能被刪除：</font>\n";
$i_con_msg_copy = "<font color=green>複製完成。</font>";
$i_con_msg_copy_failed_dup_name = "<font color=red>複製失敗，原因：樣式名稱已被使用。</font>";
$i_con_msg_copy_failed = "<font color=red>複製失敗。</font>";
$i_con_msg_activate = "<font color=green>已啟用紀錄。</font>";
$i_con_msg_quota_exceeded = "<font color=red>超出限額。</font>";
$i_con_msg_notification_sent = "<font color=green>已傳送學生通知信。</font>";
$i_con_msg_wrong_header = "<font color=red>標題格式不正確</font>";
$i_con_msg_approve = "<font color=green>已批核紀錄。</font>\n";
$i_con_msg_reject = "<font color=green>紀錄已被拒絕。</font>\n";
$i_con_msg_release = "<font color=green>已開放紀錄。</font>\n";
$i_con_msg_unrelease = "<font color=green>紀錄已被取消「開放」狀態。</font>\n";
$i_con_msg_waive = "<font color=green>紀錄已被豁免。</font>\n";
$i_con_msg_unwaive = "<font color=green>紀錄已被取消「豁免」。</font>\n";
$i_con_msg_no_delete_acc_record = "<font color=green>部份累積紀錄刪除失敗。</font>\n";
$i_con_msg_no_waive_acc_record = "<font color=green>部份累積紀錄豁免失敗。</font>\n";
$i_con_msg_no_unwaive_acc_record = "<font color=green>累積紀錄取消豁免失敗。</font>\n";
$i_con_msg_no_student_select = "<font color=red>沒有選擇學生。</font>\n";
$i_con_msg_duplicate_students = "<font color=red>選擇學生重覆。</font>\n";
$i_con_msg_not_all_approve = "<font color=red>部份紀錄已批核。</font>\n";
$i_con_msg_no_record_approve = "<font color=red>沒有已批核的紀錄。</font>\n";
$i_con_msg_not_all_reject = "<font color=red>已拒絕部份紀錄。</font>\n";
$i_con_msg_not_all_release = "<font color=red>已開放部份紀錄。</font>\n";
$i_con_msg_no_record_release = "<font color=red>沒有開放紀錄。</font>\n";
$i_con_msg_not_all_unrelease = "<font color=red>已設定部份紀錄為未開放。</font>\n";
$i_con_msg_no_record_unrelease = "<font color=red>沒有未開放紀錄。</font>\n";
$i_con_msg_import_success_with_update = "<font color='green'>成功匯入新紀錄，並更新了現有紀錄。</font>";
$i_con_msg_sports_ranking_generated = "<font color=green>已更新紀錄及賽項排名。</font>";
$i_con_msg_import_no_record = "<font color=red>檔案沒有資料。</font>";
$i_con_msg_duplicate_item = "<font color=red>該類別名稱已經被使用過，請重新輸入。</font>";
$i_con_msg_duplicate_category = "<font color=red>該項目名稱已經被使用過，請重新輸入。</font>";
$i_con_msg_add_overlapped = "<font color=red>時段重覆，增加紀錄失敗。</font>\n";
$i_con_msg_update_overlapped = "<font color=red>時段重疊，更新紀錄失敗。</font>\n";
$i_con_msg_add_past = "<font color=red>時段已過去，增加紀錄失敗。</font>\n";
$i_con_msg_update_past = "<font color=red>時段已過去，更新紀錄失敗。</font>\n";
$i_con_msg_add_past_overlapped = "<font color=red>未能增加某些時段已過去或重疊的紀錄。</font>\n";
$i_con_msg_session_not_available = "<font color=red>沒有此時段</font>\n";
$i_con_msg_session_not_available2 = "<font color=red>沒有合適時段</font>\n";
$i_con_msg_session_past = "<font color=red>時段已過去</font>\n";
$i_con_msg_session_full = "<font color=red>時段額滿</font>\n";
$i_con_msg_session_assigned_before = "<font color=red>學生已編定到此時段</font>\n";
$i_con_msg_update_inapplicable_form = "<font color=red>級別不適用，更新紀錄失敗。</font>\n";
$i_con_msg_update_already_in_session = "<font color=red>學生已編定到此時段，更新紀錄失敗。</font>\n";
$i_con_msg_update_not_enough_session = "<font color=red>時段不足夠，更新紀錄失敗。</font>\n";
$i_con_msg_update_incompatible_form = "<font color=red>級別不適用，更新紀錄失敗。</font>\n";
$i_con_msg_update_incompatible_vacancy = "<font color=red>餘額不適用，更新紀錄失敗。</font>\n";
$i_con_msg_cancel = "<font color=green>留堂安排已取消。</font>\n";
$i_con_msg_cancel_failed = "<font color=red>取消留堂安排失敗。</font>\n";
$i_con_msg_cancel_past = "<font color=red>時段已過去，取消留堂安排失敗。</font>\n";


## added on 15 Jan 2009 by Ivan
$i_con_msg_redeem_success = "<font color=green>功過相抵完成</font>\n";
$i_con_msg_redeem_failed = "<font color=red>功過相抵失敗。</font>\n";
$i_con_msg_cancel_redeem_success = "<font color=green>取消功過相抵完成</font>\n";
$i_con_msg_cancel_redeem_failed = "<font color=red>取消功過相抵失敗</font>\n";

# for club enrolment (old version) 20080924 yat woon
$i_con_msg_enrollment_club_quota_exceeded = "<font color=red>學會超出限額。</font>";
$i_con_msg_enrollment_no_student = "<font color=red>沒有學生需要批核。</font>";
$i_con_msg_enrollment_some_already_exists_club = "<font color=red>部份學生已加入次序夠高的小組。</font>";
$i_con_msg_enrollment_club_reject_success = "<font color=green>學生已被拒絕。</font>";

# For class history promotion February 2009 By Sandy
$i_con_msg_record_not_found[0] = "<font color=red>Record not found</font>";
$i_con_msg_record_not_found[1] = "<font color=red>Duplicate/Problematic record not found. <BR />Record may have been corrected in another session</font>";
$i_con_msg_promo_not_deleted = "<font color=red>No records were deleted.</font>";


# Functions
$i_AlbumName = "相簿名稱";
$i_AlbumDescription = "內容";
$i_AlbumNumberOfItems = "項目數目";
$i_AlbumDateModified = "上次更新";
$i_AlbumDisplayOrder = "顯示次序";
$i_AlbumAccessType = "使用者類別";
$i_AlbumIntranet = "所有內聯網用戶";
$i_AlbumInternet = "所有互聯網用戶";
$i_AlbumSelectedGroupsUsers = "選擇組別及使用者";
$i_AlbumInternalGroup = "內部小組";
$i_AlbumUserGroup = "使用者";
$i_AlbumQuota = "儲存量 (MBytes)";
$i_AlbumPhotoFile = "檔案";
$i_AlbumPhotoDescription = "內容";
$i_AnnouncementTitle = "題目";
$i_AnnouncementDescription = "內容";
$i_AnnouncementDate = "開始日期";
$i_AnnouncementEndDate = "結束日期";
$i_AnnouncementRecordType = "種類";
$i_AnnouncementRecordStatus = "狀況";
$i_AnnouncementDateInput = "輸入日期";
$i_AnnouncementDateModified = "最近修改日期";
$i_AnnouncementAlert = "發送電子郵件給用戶";
$i_AnnouncementAttachment = "附件";
$i_AnnouncementCurrAttachment = "現有附件";
$i_AnnouncementNewAttachment = "更多附件";
$i_AnnouncementNoAttachment = "沒有附件";
$i_AnnouncementDelete = "刪除?";
$i_AnnouncementSchool = "學校宣佈";
$i_AnnouncementAlert_SMS = "傳送 SMS 短訊至用戶";
$i_AnnouncementAdminRight = "不能編輯或刪除由系統管理員建立的項目.";
$i_AnnouncementPublic = "顯示在首頁";
$i_AnnouncementOwner = "發佈人";
$i_AnnouncementByGroup = "負責小組";
$i_AnnouncementSystemAdmin = $i_general_sysadmin;
$i_AnnouncementNoAnnouncer = "Unknown (Old Post)";
$i_AnnouncementPublicInstruction = "如果此宣佈為公眾宣佈,請把組別一欄留空.";
$i_AnnouncementPublic = "公眾宣佈";
$i_AnnouncementGroup = "我的小組宣佈";
$i_AnnouncementDisplayDate = "顯示日期";
$i_AnnouncementWholeSchool = "全校";
$i_AnnouncementTargetGroup = "目標小組";
$i_AnnouncementReadCount = "已閱 ";
$i_AnnouncementUnreadCount = "未閱 ";
$i_AnnouncementReadList = "已閱名單";
$i_AnnouncementUnreadList = "未閱名單";
$i_AnnouncementViewReadStatus = "檢視觀看情況";
$i_AnnouncementViewReadList = "檢視已閱名單";
$i_AnnouncementViewUnreadList = "檢視未閱名單";
$i_All_MyGroup = "全部小組";

$i_BookingDateStart = "日期";
$i_BookingDateEnd = "時間";
$i_BookingRemark = "備註";
$i_BookingRecordType = "種類";
$i_BookingRecordStatus = "狀況";
$i_BookingDateInput = "輸入日期";
$i_BookingDateModified = "最近修改日期";
$i_BookingArterisk = "<font color=red>*</font>";
$i_BookingLegend = " - 該申請仍在處理中";
$i_BookingListAll = "檢視所有已預訂之紀錄";
$i_BookingMyRecords = "我的紀錄";
$i_BookingNew = "新增預訂";
$i_BookingSingle = "單次預訂";
$i_BookingPeriodic = "定期重複預訂";
$i_BookingAddStep = array("");
$i_BookingAddStep[1] = "先按入所需預訂項目的種類.";
$i_BookingAddStep[2] = "選擇預訂的頻率: 單次或定期重複預訂.";
$i_BookingAddStep[3] = "填寫其他相關的細節.";
$i_BookingAddRule = "如需要預訂，請按以下步驟:";
$i_BookingAddRule .= "<br>\n1. ".$i_BookingAddStep[1];
$i_BookingAddRule .= "<br>\n2. ".$i_BookingAddStep[2];
$i_BookingAddRule .= "<br>\n3. ".$i_BookingAddStep[3];
$i_BookingNewSingle = "新增單次預訂紀錄";
$i_BookingEditSingle = "編輯單次預訂紀錄";
$i_BookingNewPeriodic = "新增定期重複預訂紀錄";
$i_BookingEditPeriodic = "編輯定期重複預訂紀錄";
$i_BookingItem = "資源";
$i_BookingDate = "日期";
$i_BookingTimeSlots = "時段";
$i_BookingApplied = "申請時間";
$i_BookingRetrieveTimeSlots = "檢視時段";
$i_BookingStatus = "狀況";
$i_BookingStartDate = "開始日期";
$i_BookingEndDate = "結束日期";
$i_BookingPeriodicType = "週期";
$i_BookingNotes = "備忘";
$i_BookingReserved = "不可借用 - 已被預約";
$i_BookingAvailable = "可借用";
$i_BookingUnavailable = "不可借用";
$i_BookingAppliedByU = "你原來的申請.";
$i_BookingPending = "審批中";
$i_BookingSingleBooking = "單一預訂紀錄";
$i_BookingPeriodicBooking = "定期預訂紀錄";
$i_BookingPeriodicDates = "預訂的日期";
$i_BookingStatusArray = array(
"審批中",
"已取消",
"已取",
"已退還",
"已批准",
"已拒絕"
);
$i_BookingStatusImageArray = array(
"審批中",
"已取消",
"已取",
"已退還",
"已批准",
"已拒絕"
);
$i_BookingType_EveryDay = "每天";
$i_BookingType_EverySeparate = "每";
$i_BookingType_Days = "天";
$i_BookingType_Every = "逢";
$i_BookingType_Weekdays = "週日";
$i_BookingType_Cycleday = "循環日";
$i_BookingType_PleaseSelect = "請選擇";
$i_BookingPeriodicConfirm = "確定定期重複預訂紀錄申請";
$i_BookingPeriodic_confirmMsg1 = "預訂一覽 :";
$i_BookingPeriodic_confirmMsg2 = "您希望預訂的項目的狀態如下:";
$i_BookingPeriodic_confirmMsg3 = "你所申請的項目未必會全部獲批准. 確定提交申請?";
$i_BookingPeriodic_NoMatch = "沒有日期符合你所選的條件.";
$i_Booking_TooLate1 = "你需要在 ";
$i_Booking_TooLate2 = "  日前預訂此項目.";
$i_Booking_TodayOrLater = "你只能作今天或以後預訂.";
$i_Booking_EndDateWrong = "結束日期必須是開始日期或以後.";
$i_Booking_sepdayswrong = "日數必須為正整數";
$i_Booking_PlsWeekDays = "請選擇週日.";
$i_Booking_PlsCycleDays = "請選擇循環日.";
$i_BookingWrongPeriodicDateEdit = "開始日期必須為 ";
$i_BookingWrongPeriodicDateEdit2 = "或以後.";
$i_BookingWrongPeriodicEndDate = "結束日期必須為 ";
$i_BookingWrongPeriodicEndDate2 = "或以前.";
$i_Booking_indexTitle = "<img border=0 src=/images/index/resources_glass_b5.gif>";
$i_Booking_Period = "期間";
$i_Booking_filter = "只顯示";
$i_Booking_ViewSshool = "正常時段";
$i_Booking_ViewOther = "特別時段";
$i_Booking_Weekday = array (
"<img src=/images/resourcesbooking/week_sun_b5.gif>",
"<img src=/images/resourcesbooking/week_mon_b5.gif>",
"<img src=/images/resourcesbooking/week_tue_b5.gif>",
"<img src=/images/resourcesbooking/week_wed_b5.gif>",
"<img src=/images/resourcesbooking/week_thu_b5.gif>",
"<img src=/images/resourcesbooking/week_fri_b5.gif>",
"<img src=/images/resourcesbooking/week_sat_b5.gif>"
);
$i_Booking_Year = "年";
$i_Booking_BookingRecords = "預訂紀錄";
$i_Booking_Time = "時間";
$i_Booking_ReservedBy = "預訂者";
$i_Booking_ClearReject = "清除被拒借用項目紀錄";
$i_Booking_Signal = array("",
"<font color=green>已新增紀錄</font>",
"<font color=red>所選之時間已被預訂</font>",
"<font color=green>已更新紀錄</font>",
"<font color=red>你沒有權限預訂本項目</font>",
"<font color=green>已取消紀錄</font>",
"<font color=red>沒有日期符合你所選的條件</font>",
"<font color=red>日期有誤</font>",
"<font color=green>已清除紀錄</font>"
);
$i_Booking_ArchiveConfirm = "你確定要保存?";
$i_Booking_ViewArchive = "檢視已保存紀錄";
$i_Booking_ArchiveUser = "申請人";
$i_Booking_ArchiveLastAction = "最後更新時間";
$i_Booking_ArchiveBooking = "保存資源預訂紀錄";
$i_Booking_DateSelectOr = "或使用 ";
$i_BookingLegendFull = "所有時段已被預留";
$i_BookingLegendHalf = "部份時段可供借用";
$i_BookingLegendFree = "所有時段可供借用";
$i_BookingItemList = "/images/resourcesbooking/itemlist_b5.gif";
$i_BookingBanList = "限制用戶名單";
$i_BookingBanList_Instruction = "請輸入限制用戶的登入名稱. <br>每一行輸入一個登入名稱.<br>這些用戶不能再新增預訂紀錄, 但已申請的紀錄則不受影響.<br>";

$i_EmailWebsite = "內聯網網址";
$i_EmailWebmaster = "管理人電子郵件";

$i_AdminLogin = "管理人名稱";
$i_AdminPassword = "管理人密碼";
$i_AdminRights = "功能權限設定";

$i_RbpsTitle = "上課節數名稱";
$i_RbpsTimeStart = "開始時間";
$i_RbpsTimeEnd = "結束時間";

$i_EventTitle = "題目";
$i_EventDescription = "內容";
$i_EventDate = "事項日期";
$i_EventVenue = "地點";
$i_EventNature = "性質";
$i_EventRecordType = "種類";
$i_EventRecordStatus = "狀況";
$i_EventDateInput = "輸入日期";
$i_EventDateModified = "最近修改日期";
$i_EventTypeSchool = "學校";
$i_EventTypeAcademic = "教學";
$i_EventTypeHoliday = "假期";
$i_EventTypeGroup = "小組";
$i_EventSkipCycle = "不計算循環日";
$i_EventImport_msg = "選擇需要匯入的檔案";
$i_EventImport_type = "事項的種類";
$i_EventImport_fileformat = "檔案格式";
$i_EventImport_fileformat2 = "";
$i_EventImport_sample = "按此下載範本檔案 (CSV 格式)";
$i_EventImport_notes = "注意日期必須為 YYYY-MM-DD 或 YYYY/MM/DD 格式. 請在儲存CSV 檔前, 在 Microsoft Excel 中設定日期格式.";
$i_Events = "事項";
$i_EventTypeString = array (
"學校事項",
"教學事項",
"假期",
"小組事項"
);
$i_EventAdminRight = "您不能編輯或刪除由系統管理員建立的項目.";
$i_EventList = "<img src=$image_path/index/btn_eventlist.gif border=0>";
$i_EventShowAll = "<img border=0 src=$image_path/index/el_btn_allevents.gif width=76 height=22>";
$i_EventCloseImage = "<img border=0 src=$image_path/index/el_btn_close.gif width=14 height=14>";
$i_EventAll = "所有$i_Events";
$i_EventUpcoming = "即將來臨的$i_Events";
$i_EventPast = "昔日$i_Events";
$i_EventPoster = "發起人";
$i_EventPublicInstruction = "如果你欲把此$i_Events"."顯示給所有用戶, 請把小組一欄留空.";
$i_EventAcademicYear = "學年";

$i_RecordDate = "紀錄日期";

$i_GroupTitle = "小組名稱";
$i_GroupDescription = "小組簡介";
$i_GroupRecordType = "種類";
$i_GroupRecordStatus = "狀況";
$i_GroupDateInput = "輸入日期";
$i_GroupDateModified = "最近修改日期";
$i_GroupURL = "網址";
$i_GroupSetAdmin = "設定為管理員";
$i_GroupUnsetAdmin = "取消管理員";
$i_GroupAssignAdminRight = "設定小組管理員權限";
$i_GroupAssignAdminRight_SetAdminRightFor = "設定此用戶為小組管理員";
$i_GroupAssignAdminRight_AvailableAdminRights = "可選之權限";
$i_GroupAssignAdminRight_AllRights = "所有權限";
$i_GroupKey = "為本小組管理員.";
$i_GroupQuota = "儲存量 (MBytes)";
$i_GroupQuotaIsInt = "儲存量必須為正整數.";
$i_GroupStorageUsage1 = "使用中";
$i_GroupStorageUsage2 = "(KB)";
$i_GroupSettingsBasicInfo = "基本資料";
$i_GroupSettingsMemberList = "組員名單";
$i_GroupSettingsAnnouncement = "小組宣佈";
$i_GroupSettingsEvent = "小組活動";
$i_GroupSettingsSurvey = "小組問卷調查";

$i_GroupSettingTabs = array(
$i_GroupSettingsBasicInfo,
$i_GroupSettingsMemberList,
$i_GroupSettingsAnnouncement,
$i_GroupSettingsEvent);

$i_GroupAdminRightInternalAnnouncement = "內部宣佈";
$i_GroupAdminRightAllAnnouncement = "所有宣佈 (包括公開及內部)";
$i_GroupAdminRightInternalEvent = "小組內部事項";
$i_GroupAdminRightAllEvent = "所有事項 (包括公開及內部)";
$i_GroupAdminRightTimetable = "時間表";
$i_GroupAdminRightBulletin = "討論欄";
$i_GroupAdminRightLinks = "共享網址";
$i_GroupAdminRightFiles = "共享檔案";
$i_GroupAdminRightQB = "題目庫管理";
$i_GroupAdminRightInternalSurvey = "內部問卷調查";
$i_GroupAdminRightAllSurvey = "所有問卷調查 (包括公開及內部)";

$i_GroupPageDescription = "下列為全部與您相關的小組. 如需要與小組成員交換訊息及檔案等, 請按相關的功能鍵.";
$i_GroupMemberImport = "按$button_import"."以增添新成員";
$i_GroupMemberNewRole = "按$button_new$i_admintitle_role"."以增添新$i_admintitle_role.";
$i_GroupMemberFunction = "如需要轉換成員的$i_admintitle_role".", 請選擇新$i_admintitle_role,並按下$button_update$i_admintitle_role"."鍵";
$i_GroupSettingDescription = "這裡提供的資料將於學校組織版面展示.";
$i_GroupAnnounceRight = "公眾宣佈權限";
$i_GroupPublicToSchool = "可作公眾宣佈";
$i_GroupUseAllTools = "容許使用所有小組工具";
$i_GroupToolsAllowed = "小組工具使用權限";
$i_ClubToolsAllowed = "學會工具使用權限";
$i_GroupOnlyAcademicAllowed = "學術組別專用";
$i_GroupAdminRights = array(
"基本資料",
"管理會員名單",
"管理組內宣佈",
"管理所有宣佈 (包括對外)",
"管理組內活動",
"管理所有活動 (包括對外)",
"管理題目庫");

$i_GroupCategorySettings = "小組類別";
$i_GroupCategoryName = "類別名稱";
$i_GroupCategoryLastModified = "最近修改日期";
$i_GroupCategoryNewDefaultRole = "此類別的預設職位";

$i_Polling_Settings = "投票設定";
$i_PollingQuestion = "問題";
$i_PollingAnswerA = "選擇A";
$i_PollingAnswerB = "選擇B";
$i_PollingAnswerC = "選擇C";
$i_PollingAnswerD = "選擇D";
$i_PollingAnswerE = "選擇E";
$i_PollingAnswerF = "選擇F";
$i_PollingAnswerG = "選擇G";
$i_PollingAnswerH = "選擇H";
$i_PollingAnswerI = "選擇I";
$i_PollingAnswerJ = "選擇J";
$i_PollingReference = "相關網址";
$i_PollingDateStart = "開始";
$i_PollingDateEnd = "結束";
$i_PollingDateRelease = "結果公佈日期";
$i_PollingDateRelease_remark = "結果將會在投票後<b>及</b>結果公佈日期後公開";
$i_PollingRecordType = "種類";
$i_PollingRecordStatus = "狀況";
$i_PollingDateInput = "輸入日期";
$i_PollingDateModified = "最近修改日期";
$i_PollingWrontStart = "開始日期必須是今天或以後";
$i_PollingWrongEnd = "結束日期必須是開始日期或以後";
$i_PollingTopic = "最近題目";
$i_PollingPeriod = "投票期限";
$i_PollingResult = "投票結果";
$i_Polling_PastDescription = "請選擇您有興趣的題目觀看投票結果.";
$i_Polling_Vote = "投票";

$i_eNews = "校園最新消息";
$i_eNews_Settings = "校園最新消息設定";
$i_eNews_AddTo = "增加";
$i_eNews_DeleteTo = "刪除";
$i_eNews_Approval = "批核";
$i_eNews_Approval2 = "校園最新消息批核";

$i_ResourceCode = "資源編號";
$i_ResourceCategory = "資源種類";
$i_ResourceTitle = "項目";
$i_ResourceDescription = "內容";
$i_ResourceRemark = "備註";
$i_ResourceAttachment = "附件";
$i_ResourceRecordType = "種類";
$i_ResourceRecordStatus = "狀況";
$i_ResourceDateInput = "輸入日期";
$i_ResourceDateModified = "最近修改日期";
$i_ResourceDateStart = "開始日期";
$i_ResourceDateEnd = "結束日期";
$i_ResourcePeriodType = "每逢";
$i_ResourceDateStartWrong = "開始日期必須是今天或以後";
$i_ResourceDateEndWrong = "結束日期必須是開始日期或以後";
$i_ResourceDaysBefore = "提早預訂期限(日)";
$i_ResourceTimeSlot = "時間表";
$i_ResourceDaysBeforeWarning = "必須為正整數或 0.";
$i_ResourceImportFile = "請選擇檔案";
$i_ResourceImportNote = "備註";
$i_ResourceImportNote1 = "<ul><li>時間表名稱必須完全正確, 否則資料不能匯入
<li><a href=javascript:newWindow('tscheme.php',0)>檢視時間表內容</a>
<li><a href=itemsample.csv> 下載範本檔案 (CSV 格式)</a></ul>";
$i_ResourceViewScheme = "<a href=javascript:newWindow('tscheme.php',0)>檢視時間表內容</a>";
$i_ResourcePopScheme = "學校時間表";
$i_ResourceAutoApprove = "自動批准?";
$i_ResourceSelectCategory = "種類";
$i_ResourceSelectItem = "資源";
$i_ResourceSlotChangeAlert = "更改時間表或會令已申請之預訂紀錄有誤, 是否繼續?";
$i_ResourceSlotChangeAlert2 = "(預訂者將不會收到電郵通知)";
$i_ResourceResetBooking = "重設資源預訂紀錄";
$i_ResourceResetBooking2 = "取消所有此資源的預訂紀錄 (今天或以後的預訂紀錄會被刪除,過去的紀錄會被保存).<br> $i_ResourceSlotChangeAlert2";
$i_ResourceSelectAllCat = "全部種類";
$i_ResourceSelectAllSituations = "所有發放原因";
$i_ResourceSelectAllItem = "全部資源";
$i_ResourceSelectAllResourceCode = "全部資源編號";
$i_ResourceSelectAllUser = "全部皇請人";
$i_ResourceSelectAllStatus = "全部狀況";

$i_RoleTitle = "職位名稱";
$i_RoleDescription = "簡介";
$i_RoleRecordType = "種類";
$i_RoleRecordStatus = "狀況";
$i_RoleDateInput = "輸入日期";
$i_RoleDateModified = "最近修改日期";
$i_RoleDefault = "預設值";
$i_RoleNewPrompt = "請輸入新職位名稱";

$i_TimetableTitle = "題目";
$i_TimetableDescription = "內容";
$i_TimetableURL = "附件";
$i_TimetableRecordType = "種類";
$i_TimetableRecordStatus = "狀況";
$i_TimetableDateInput = "輸入日期";
$i_TimetableDateModified = "最近修改日期";
$i_Timetable_type0 = "使用學校時間表 <a href=\"/home/school/timetable/school_timetable.csv\">[範本 (CSV 格式)]</a>";
$i_Timetable_type1 = "使用自訂時間表 <a href=\"/home/school/timetable/self_timetable.csv\">[範本 (CSV 格式)]</a>";
$i_Timetable_IndexInstruction = "按編輯鍵, 然後填寫時間表的內容. 您可以自行定義時間表, 按匯入鍵上載文件.";

$i_UserLogin = "內聯網帳號";
$i_UserPassword = "密碼";
$i_UserEmail = "電子郵件";
$i_UserFirstName = "名字";
$i_UserLastName = "姓氏";
$i_UserChineseName = "中文名稱";
$i_UserEnglishName = "英文名稱";
$i_UserNickName = "別名";
$i_UserDisplayName = "顯示名稱";
$i_UserTitle = "稱謂";
$i_UserGender = "性別";
$i_UserDateOfBirth = "出生日期";
$i_UserHomeTelNo = "住宅電話";
$i_UserOfficeTelNo = "公司";
$i_UserMobileTelNo = "手提電話";
$i_UserMobileSMSNotes = "<br>(此流動電話號碼會用作接收SMS 短訊之用. 如果此用戶不會接收任何SMS 短訊, 請留空此欄.)";
$i_UserFaxNo = "傳真號碼";
$i_UserICQNo = "ICQ";
$i_UserURL = "個人網站網址";
$i_UserAddress = "地址";
$i_HKID = "身份証號碼";
$i_STRN = "學生編號(STRN)";
$i_UserCountry = "國家";
$i_UserInfo = "資料";
$i_UserRemark = "備註";
$i_UserClassNumber = "班號";
$i_UserClassName = "班別";
$i_UserClassLevel = "級別";
$i_UserLastUsed = "最後使用";
$i_UserLastModifiedPwd = "最後更改密碼";
$i_UserRecordType = "種類";
$i_UserRecordStatus = "狀況";
$i_UserDateInput = "輸入日期";
$i_UserDateModified = "最近修改日期";
$i_UserPhoto = "相片";
$i_UserPhotoDelete = "刪除現時相片";
$i_UserPhotoGuide = "相片只能以'.JPG'、'.GIF' 或'.PNG'的格式上載，相片大小也規定為100 X 130 pixel (闊 x 高)。";
$i_ClassNameNumber = "班別 (班號)";
$i_UserResetPassword = "重設密碼";
$i_UserNotMustChangePasswd = "(如果你不需要更改密碼, 請留空以上三項.)";
$i_UserName = "姓名";
$i_UserChineseName = "中文姓名";
$i_UserEnglishName = "英文姓名";
$i_UserOpenWebmail = "替新用戶開設網上電郵戶口";
$i_UserProfileDescription = "您的個人資料, 聯絡方法及其他留言將於您所有的 eClass 教室中出現. 所有內容都可以選擇是否填寫.";
$i_UserProfilePersonal = "個人資料";
$i_UserProfileContact = "聯絡資料";
$i_UserProfileMessage = "留言";
$i_UserProfileLogin = "登入密碼";
$i_UserProfileEmailUse = "用於接收系統郵件, 所以只可輸入一個有效的電子郵件，錯誤的輸入有可能導致你使用時出現問題！";
$i_UserProfilePwdChangeRule = "如果您不想更改您的密碼, 請勿填寫以下內容.";
$i_UserProfilePwdUse = "用作確認您的身份";
$i_UserStudentName = "學生姓名";
$i_UserProfile_Change_Notice="如需更改此項資料，請聯絡系統管理員。";

$i_UserImportGraduate = "$button_import"."離校名單";
$i_UserGraduate_ImportInstruction = "<b><u>檔案說明:</u></b><br>
第一欄 : 用戶的$i_UserLogin.<br>
";
$i_UserParentLink = "連結子女";
$i_UserParentLink_SelectClass = "選擇班別";
$i_UserParentLink_SelectStudent = "選擇子女";
$i_UserParentWithLink = "有連結";
$i_UserParentWithNoLink = "沒有連結";
$i_UserParent_LinkedChildren = "的子女名單";
$i_UserAccountOpenForNewOnly = "新用戶: 系統會自動開啟 Linux 系統戶口(用作FTP/Email). <br>現有用戶: 權限會被更新, 但基於系統保安理由, 不會再開啟Linux 戶口.";
$i_UserRemoveConfirm = "注意有關以下戶口會一併刪除";
$i_UserLinuxAccount = "Linux 戶口 (用作外來電郵及個人文件櫃)";
$i_UserLDAPAccount = "LDAP 戶口 (用作登入檢查)";
$i_UserRemoveConfirmAsk = "你確定要刪除剛才選擇的用戶?";
$i_UserDefaultExport = "匯出預設格式(修改資料後可再匯入至系統)";
$i_UserDefaultExportWarning = "如要匯出預設格式(修改資料後可再匯入至系統)，請選擇一個身份";
$i_UserDisplayTitle_English = "顯示稱謂 (英)";
$i_UserDisplayTitle_Chinese = "顯示稱謂 (中)";
$i_UserWarningiPortfolio = "<font color=red>請注意: 學生檔案及iPortfolio的資料將會在刪除學生戶口時被整存, 整存後的戶口資料將不能復元及更改</font>";
//$i_UserRemoveStop_PaymentBalanceNonZero = "此動作已停止, 因有部份用戶的戶口仍有結存. 請先作退款動作.";
$i_UserRemoveStop_PaymentBalanceNonZero = "以下用戶戶口仍有結餘或尚有未繳款項。操作終止。";

$i_FileSystemFiles = "檔案";
$i_FileSystemName = "名稱";
$i_FileSystemSize = "大小";
$i_FileSystemModified = "最近修改日期";
$i_FileSystemUp = "上移一層";

$i_eClassCourseID = "課程ID";
$i_eClassCourseCode = "課程編號";
$i_eClassCourseCodePrefix = "課程編號稱謂";
$i_eClassCourseCodePrefixGuide = "如課程編號\"C003\"的\"C\"";
$i_eClassCourseSettings = "課程設定";
$i_eClassCourseProposal = "根據現有班別及科目，組成以下可開設的教室";
$i_eClassCourseProposalNotes = "注意:<br> 系統會根據教師職務設定，自動將老師加入適當教室。請確定已完成教師職務設定。";
$i_eClassCourseConfirm = "你是否確定要加入已選課程?";
$i_eClassCourseProcessing = "正在開設教室: ";
$i_eClassCourseWait = "請等候 ... ";
$i_eClassCourseDone = "完成!";
$i_eClassCourseImportStudent = "加入學生";
$i_eClassLicenseExceed = "你沒有足夠教室許可證去開設已選教室。";
$i_eClassCourseNoneWarn = "請最少選擇一個課程。";
$i_eClassCourseProposalNone = "因為沒有班別或科目紀錄，所以不能建議任何教室。你應該先在[學校設定]輸入班別及科目。";
$i_eClassCourseName = "課程名稱";
$i_eClassCourseDesc = "課程概括";
$i_eClassIsGuest = "訪客進入";
$i_eClassNumUsers = "用戶數目";
$i_eClassMaxUser = "用戶限額";
$i_eClassMaxStorage = "容量限額 (MB)";
$i_eClassStorage = "容量 (MB)";
$i_eClassFileNo = "檔案數目";
$i_eClassFileSize = "檔案大小 (MB)";
$i_eClassFileSpace = "尚餘限額 (MB)";
$i_eClassHitsTotal = "總點擊次數";
$i_eClassHitsTeacher = "老師點擊次數";
$i_eClassHitsStudent = "學生點擊次數";
$i_eClassHitsAssistant = "助教點擊次數";
$i_eClassInputdate = "開設日期";
$i_eClassCreatorBy = "開設人";
$i_eClassLicenseDisplayN1 = "你的一般教室許可證有 ";
$i_eClassLicenseDisplayR1 = "你的閱讀室許可證有 ";
$i_eClassLicenseDisplay2 = " 個，尚有 ";
$i_eClassLicenseDisplay3 = " 個可供使用。";
$i_eClassLicenseDisplay4 = "你已使用的許可證為 ";
$i_eClassLicenseDisplay5 = " 個。";
$i_eClassLicenseMsg1 = "教室許可證";
$i_eClassLicenseMsg2 = "總數";
$i_eClassLicenseMsg3 = "已開設";
$i_eClassLicenseMsg4 = "尚餘";
$i_eClassLicenseFull = "你已使用所有教室許可證, 你不可再新增任何教室.";
$i_eClassUserQuotaMsg1 = "用戶限額為";
$i_eClassUserQuotaMsg2 = "，尚有";
$i_eClassUserQuotaMsg3 = "個空位。";
$i_eClassUserQuotaMsg4 = "這教室已使用的用戶數目為";
$i_eClassUserImport1 = "第一步：用戶分組";
$i_eClassUserImport2 = "第二步：選擇用戶";
$i_eClassStorageDisplay1 = "儲存限額為";
$i_eClassStorageDisplay2 = " MB ﹐尚有";
$i_eClassStorageDisplay3 = "MB 可供使用。";
$i_eClassStorageDisplay4 = "你已使用的儲存量為";
$i_eClassStorageDisplay5 = " MB.";
$i_eClassStorageDisplay6 = " 個檔案及 ";
$i_eClassStorageDisplay7 = "份文件。";
$i_eClassAdminFileRight = "開放所有檔案權限";
//$i_eClass_Admin_MgmtCenter = "eClass 管理中心";
$i_eClass_Admin_MgmtCenter = "特別室";
$i_eClass_Admin_Settings = "eClass 設定";
$i_eClass_Admin_Stats = "eClass 統計";
$i_eClass_Admin_AssessmentReport = "評估報告";
$i_eClass_Admin_AllAssessments = "全部評估";
$i_eClass_Admin_NoStudentsinAnyeClass = "沒有學生在任何網上教室中";
$i_eClass_Admin_OnlyAssessmentsPassedDeadline = "只包括到期評估";
$i_eClass_Admin_Shared_Files = "教師共享地帶";
$i_eClass_Admin_stats_login = "登入紀錄";
$i_eClass_Admin_stats_participation = "參與紀錄";
$i_eClass_Admin_stats_files = "文檔使用紀錄";
$i_eClass_Identity = "身份";
$i_eClass_ClassLink = "所屬班別";
$i_eClass_SubjectLink = "所屬科目";
# eclass homework list $i_eClass_ClassSubjectNote = "(如需連結家課紀錄表, 以上兩項必須填寫)";
$i_eClass_normal_course = "一般教室";
$i_eClass_reading_room = "閱讀室";
$i_eClass_elp = "eClass 學習資源室";
$i_eClass_elp_courseware = "課件";
$i_eClass_ssr = "網上評估室";
$i_eClass_ssr_type = "評估範本";
$i_eClass_course_type = "類別";
$i_eClass_group_teacher = "Teacher 老師";
$i_eClass_group_assistant = "Assistant 助教";
$i_eClass_group_student = "Student 學生";
$i_eClass_group_default = "Default Group 預設小組";
$i_eClass_set_teacher_rights = "設定老師權能";
$i_eClass_eclass_group = "設定eClass小組";
$i_eClass_teacher_rights_1 = "管理教室";
$i_eClass_teacher_rights_2 = "批核及批改讀書報告";
$i_eClass_teacher_rights_3 = "Student Profile Team";
$i_eClass_Tools = "內置工具";
$i_eClass_Tool_LicenseLeft = "尚餘許可證";
$i_eClass_Tool_EquationEditor = "方程式編輯器";
$i_eClass_Tool_EquationEditorUsing = "正使用方程式編輯器";
$i_eClass_management_enable = "讓老師使用eClass 管理";
$i_eClass_teacher_open_course = "讓老師在eClass目錄頁自行開eClass";
$i_eClass_email_function = "電郵功能";
$i_eClass_email_function_enable = "可使用eClass寄出電郵";
$i_eClass_identity_helper = "助手";
$i_eClass_quota_exceeded = "匯入失敗。用戶限額不足。請增加此網上教室的用戶限額。";
$i_eClass_no_user_selected = "請最少選擇一個用戶!";
$i_eClass_batch_new = "檔案新增";
$i_eClass_batch_import = "匯入班名單";
$i_eClass_ImportInstruction_batch_new = "
第一欄 : $i_eClassCourseCode<br>
第二欄 : $i_eClassCourseName<br>
第三欄 : $i_eClassMaxUser<br>
第四欄 : $i_eClassMaxStorage<br>
第五欄 : KLA 編號
";
$i_eClass_CategoryList = "Key Learning Areas Code Mapping";
$i_eClass_ImportInstruction_batch_member = "
第一欄 : $i_UserLogin<br>
第二欄 : $i_eClassCourseID<br>
第三欄 : 小組名稱
";

$i_eClass_planner_function = "教學計劃";
$i_eClass_planner_type_color = "設定事項分類顏色（留空當不使用）";
$i_eClass_planner_type = "類別";
$i_eClass_planner_color = "顏色";
$i_eClass_survey_deadline = "問卷限期";

$i_eClass_function_access = "使用功能";
$i_eClass_parent_access = "家長參與";
$i_eClass_portfolio_lang_c = "（中）";
$i_eClass_portfolio_lang_e = "（英）";

$i_eClass_Bulletin_BadWords_Instruction_top = "你可以輸入系統限制的字句. 在此出現的字句在討論欄將會被 *** 取代.";
$i_eClass_MyeClass_Content = "學習內容";
$i_eClass_MyeClass_Bulletin = "討論欄";
$i_eClass_MyeClass_Announcement = "宣佈";
$i_eClass_MyeClass_SharedLinks  = "共享網址";

$i_ssr_anonymous_warning = "「不記名」設定於問卷被作答後將不能取消，除非先清除所有已收回的問卷！";
$i_ssr_anonymous_guide = "若要取消「不記名」設定，請先清除所有已收回的問卷。";


$i_GroupRole = array("基本組別","行政","學術","班別","社級","課外活動","其他");
$i_GroupRoleOrganization = array("identity","admin","academic","class","house","eca","misc");

# $i_frontpage_separator = "<img src=/images/frontpage/triangleicon.gif border=0 align=absmiddle hspace=5>";
$i_frontpage_separator = "<img src=/images/arrow.gif border=0 align=absmiddle hspace=5>";
$i_frontpage_menu_schoolinfo = "<img src=/images/header/icon_schinfo.gif border=0>";
$i_frontpage_menu_schoolinfo_organization = "組織";
$i_frontpage_menu_schoolinfo_groupinfo = "小組資訊";
$i_frontpage_menu_schoolinfo_campus_gallery = "校園相簿"; #add by kelvin ho 2008-12-23
$i_frontpage_menu_schoolinfo_homework = "家課紀錄表";
$i_frontpage_menu_campusmail = "<img src=/images/header/icon_campusmail.gif border=0>";
$i_frontpage_menu_campusmail_inbox = "收件箱";
$i_frontpage_menu_campusmail_compose = "撰寫郵件";
$i_frontpage_menu_campusmail_title = "校園信箱";
$i_frontpage_menu_myinfo = "<img src=/images/header/icon_myinfo.gif border=0>";
$i_frontpage_menu_myinfo_profile = "更新個人資料";
$i_frontpage_menu_myinfo_email = "電郵設定";
$i_frontpage_menu_myinfo_password = "密碼設定";
$i_frontpage_menu_myinfo_bookmark = "我的書籤";
$i_frontpage_menu_myinfo_cabinet = "私人檔案櫃";
$i_frontpage_menu_classes = "<img src=/images/header/icon_classes.gif border=0>";
$i_frontpage_menu_resourcebooking = "<img src=/images/header/icon_resourcesbking.gif border=0>";
$i_frontpage_menu_resourcebooking_myrecords = "我的預訂紀錄";
$i_frontpage_menu_resourcebooking_reserved = "預訂紀錄";
$i_frontpage_menu_eclass = "<img src=/images/header/icon_eclass.gif border=0>";
$i_frontpage_menu_campustv_title = "校園電視";
$i_frontpage_menu_octopus = "校園通";
$i_frontpage_menu_library = "<img src=/images/header/icon_library.gif border=0>";
$i_frontpage_menu_library_title = "圖書館";
$i_frontpage_menu_logo = "<a href=/home/><img src=/images/frontpage/logo_big5.gif border=0 width=112 height=37></a>";
$i_frontpage_menu_home = "首頁";
$i_frontpage_menu_exit = "<img src=/images/header/icon_logout_b5.gif border=0>";
$i_frontpage_menu_eclass_mgt = "eClass 管理";

$i_frontpage_button_result = "<img src=/images/frontpage/button_result_big5.jpg border=0 width=51 height=22>";
$i_frontpage_button_vote = "<img src=/images/frontpage/button_vote_big5.jpg border=0 width=50 height=22>";
$i_frontpage_button_cancel = "<img src=/images/frontpage/button_cancel_big5.gif border=0 width=75 height=28>";
$i_frontpage_button_reset = "<img src=/images/frontpage/button_reset_big5.gif border=0 width=75 height=28>";
$i_frontpage_button_save = "<img src=/images/frontpage/button_save_big5.gif border=0 width=75 height=28>";
$i_frontpage_button_submit = "<img src=/images/frontpage/button_submit_big5.gif border=0 width=75 height=28>";
$i_frontpage_button_update = "<img src=/images/frontpage/button_update_big5.gif border=0 width=75 height=28>";
$i_frontpage_image_result = "/images/frontpage/button_result_big5.jpg";
$i_frontpage_image_vote = "/images/frontpage/button_vote_big5.jpg";
$i_frontpage_image_cancel = "/images/frontpage/button_cancel_big5.gif";
$i_frontpage_image_reset = "/images/frontpage/button_reset_big5.gif";
$i_frontpage_image_save = "/images/frontpage/button_save_big5.gif";
$i_frontpage_image_submit = "/images/frontpage/button_submit_big5.gif";
$i_frontpage_image_update = "/images/frontpage/button_update_big5.gif";
$i_frontpage_day = array("日","一","二","三","四","五","六");
$i_frontpage_classes = "教室";
$i_frontpage_eclass = "網上教室";
$i_frontpage_eclass_courses = "教室名稱";
$i_frontpage_eclass_lastlogin = "最後登入";
$i_frontpage_eclass_whatsnews = "新消息";
$i_frontpage_myinfo = "我的資訊";
$i_frontpage_myinfo_profile = "更新個人資料";
$i_frontpage_myinfo_email = "電郵設定";
$i_frontpage_myinfo_password = "密碼設定";
$i_frontpage_myinfo_password_old = "舊密碼";
$i_frontpage_myinfo_password_new = "新密碼";
$i_frontpage_myinfo_password_retype = "確認新密碼";
$i_frontpage_myinfo_password_mismatch = "密碼不符";
$i_frontpage_myinfo_bookmark = "我的書籤";
$i_frontpage_myinfo_cabinet = "私人檔案櫃";
$i_frontpage_myinfo_title_profile = "<img src=/images/myaccount_personalinfo/title_personalinfo_b5.gif>";
$i_frontpage_myinfo_title_email = "<img src=/images/frontpage/title_myinfo_email_big5.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_password = "<img src=/images/frontpage/title_myinfo_password_big5.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_bookmark = "<img src=/images/frontpage/title_myinfo_bookmark_big5.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_cabinet = "<img src=/images/frontpage/title_myinfo_cabinet_big5.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_rb = "資源預訂";
$i_frontpage_rb_msg = "資源預訂紀錄";
$i_frontpage_rb_title_add = "<img src=/images/frontpage/title_resourcebooking_add_big5.gif border=0 width=313 height=42 hspace=5 vspace=5>";
$i_frontpage_rb_title_edit = "<img src=/images/frontpage/title_resourcebooking_edit_big5.gif border=0 width=313 height=42 hspace=5 vspace=5>";
$i_frontpage_rb_title_form = "<img src=/images/frontpage/resourcebooking_form_title_big5.gif border=0 width=275 height=35>";
$i_frontpage_rb_title = "<img src=/images/resourcesbooking/title_b5.gif>";
$i_frontpage_rb_title_reserved = "<img src=/images/resourcesbooking/title_reservedrecords_b5.gif>";
$i_frontpage_rb_title_mybooking = "<img src=/images/frontpage/mybooking_title_big5.gif border=0 width=170 height=30>";
$i_frontpage_rb_title_mybookeditems = "<img src=/images/frontpage/mybookeditems_title_big5.gif border=0 width=414 height=50>";
$i_frontpage_rb_title_allbookeditems = "<img src=/images/frontpage/allbookeditems_title_big5.gif border=0 width=552 height=50>";
$i_frontpage_rb_title_allavailableitems = "<img src=/images/frontpage/allavailableitems_title_big5.gif border=0 width=552 height=50>";
$i_frontpage_rb_date = "預訂日期";
$i_frontpage_rb_period = "時期";
$i_frontpage_rb_note = "備忘錄";
$i_frontpage_rb_lastmodified = "最近修改日期";
$i_frontpage_home_bookings_title = "<img src=/images/frontpage/home_bookings_big5.gif border=0 width=163 height=37>";
$i_frontpage_home_eclass_title = "<img src=/images/frontpage/home_eclass_big5.gif border=0 width=163 height=37>";
$i_frontpage_home_mygroup_title = "<img src=/images/frontpage/home_mygroup_big5.gif border=0 width=163 height=37>";
$i_frontpage_home_schoolannouncement_title = "<img src=/images/index/announcement_glass_b5.gif>";
$i_frontpage_home_cal_legend = "<img src=/images/frontpage/cal_legend_big5.gif border=0 width=158 height=97 vspace=5>";
$i_frontpage_home_cal_legend_academicevents = "<img src=/images/frontpage/cal_legend_academicevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_schoolevents = "<img src=/images/frontpage/cal_legend_schoolevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_holidays = "<img src=/images/frontpage/cal_legend_holidays_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_groupevents = "<img src=/images/frontpage/cal_legend_groupevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_academicevents = "<img src=/images/frontpage/cal_white_legend_academicevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_schoolevents = "<img src=/images/frontpage/cal_white_legend_schoolevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_holidays = "<img src=/images/frontpage/cal_white_legend_holidays_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_groupevents = "<img src=/images/frontpage/cal_white_legend_groupevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_title_vote = "<img src=/images/index/pollingtab_b5.gif width=69 height=21>";
$i_frontpage_home_title_vote2 = "<img src=/images/index/pollingtab2_b5.gif width=203 height=6>";
$i_frontpage_home_title_pastvote = "<img border=0 src=/images/index/pollingicon.gif width=13 height=19>";
$i_frontpage_schoolinfo = "校園資訊";
$i_frontpage_schoolinfo_organization = "組織";
$i_frontpage_schoolinfo_groupinfo = "小組資訊";
$i_frontpage_schoolinfo_groupinfo_event = "事項";
$i_frontpage_schoolinfo_groupinfo_announcement = "宣佈";
$i_frontpage_schoolinfo_groupinfo_period = "日期範圍";
$i_frontpage_schoolinfo_groupinfo_infotype = "資訊類型";
$i_frontpage_schoolinfo_groupinfo_group = "小組";
$i_frontpage_schoolinfo_groupinfo_egroup = "小組";
$i_frontpage_schoolinfo_groupinfo_group_timetable = "時間表";
$i_frontpage_schoolinfo_groupinfo_group_chat = "交談室";
$i_frontpage_schoolinfo_groupinfo_group_bulletin = "討論欄";
$i_frontpage_schoolinfo_groupinfo_group_links = "共享網址";
$i_frontpage_schoolinfo_groupinfo_group_files = "共享檔案";
$i_frontpage_schoolinfo_groupinfo_group_settings = "小組管理";
$i_frontpage_schoolinfo_groupinfo_group_members = "組員名單";
$i_frontpage_schoolinfo_groupinfo_group_photoalbum = "相簿";
$i_frontpage_schoolinfo_groupinfo_thisweek = "這個星期";
$i_frontpage_schoolinfo_groupinfo_thismonth = "這個月";
$i_frontpage_schoolinfo_groupinfo_today = "今天";
$i_frontpage_schoolinfo_groupinfo_today_onward = "由今天起";
$i_frontpage_schoolinfo_popup_organization = "<img src=/images/frontpage/popup_schoolInfo_organization_big5.gif border=0 width=216 height=45>";
$i_frontpage_schoolinfo_title_organization = "<img src=/images/organization/title_b5.gif border=0>";
$i_frontpage_schoolinfo_title_group = "<img src=/images/frontpage/title_schoolInfo_group_big5.gif border=0 width=255 height=42 hspace=5 vspace=5>";
$i_frontpage_schoolinfo_title_homework = "<img src=/images/frontpage/title_schoolinfo_homework_big5.gif width=289 height=45 border=0 hspace=5 vspace=5>";
$i_frontpage_schoolinfo_organization_misc_t = "<img src=/images/frontpage/schoolInfo_category_misc_title_big5.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_misc_m = "<img src=/images/frontpage/schoolInfo_category_misc_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_misc_b = "<img src=/images/frontpage/schoolInfo_category_misc_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_eca_t = "<img src=/images/frontpage/schoolInfo_category_eca_title_big5.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_eca_m = "<img src=/images/frontpage/schoolInfo_category_eca_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_eca_b = "<img src=/images/frontpage/schoolInfo_category_eca_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_house_t = "<img src=/images/frontpage/schoolInfo_category_house_title_big5.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_house_m = "<img src=/images/frontpage/schoolInfo_category_house_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_house_b = "<img src=/images/frontpage/schoolInfo_category_house_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_class_t = "<img src=/images/frontpage/schoolInfo_category_class_title_big5.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_class_m = "<img src=/images/frontpage/schoolInfo_category_class_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_class_b = "<img src=/images/frontpage/schoolInfo_category_class_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_admin_t = "<img src=/images/frontpage/schoolInfo_category_admin_title_big5.gif border=0 width=383 height=50>";
$i_frontpage_schoolinfo_organization_admin_m = "<img src=/images/frontpage/schoolInfo_category_admin_bg_m.gif border=0 width=383 height=5>";
$i_frontpage_schoolinfo_organization_admin_b = "<img src=/images/frontpage/schoolInfo_category_admin_bg_b.gif border=0 width=383 height=18>";
$i_frontpage_schoolinfo_organization_academic_t = "<img src=/images/frontpage/schoolInfo_category_academic_title_big5.gif border=0 width=383 height=50>";
$i_frontpage_schoolinfo_organization_academic_m = "<img src=/images/frontpage/schoolInfo_category_academic_bg_m.gif border=0 width=383 height=5>";
$i_frontpage_schoolinfo_organization_academic_b = "<img src=/images/frontpage/schoolInfo_category_academic_bg_b.gif border=0 width=383 height=18>";
$i_frontpage_schoolinfo_homework = "家課紀錄表";
$i_frontpage_schoolinfo_popup_misc_t = "<img src=/images/frontpage/schoolinfo_popup_misc_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_eca_t = "<img src=/images/frontpage/schoolinfo_popup_eca_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_house_t = "<img src=/images/frontpage/schoolinfo_popup_house_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_class_t = "<img src=/images/frontpage/schoolinfo_popup_class_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_admin_t = "<img src=/images/frontpage/schoolinfo_popup_admin_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_academic_t = "<img src=/images/frontpage/schoolinfo_popup_academic_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_welcome = "<img src=/images/index/welcometxt_b5.gif>";
$i_frontpage_currentpoll = "現有投票";
$i_frontpage_pastpoll = "昔日投票";
$i_frontpage_my_eclass_t = "<img src=/images/frontpage/eclass_t_big5.gif border=0 width=770 height=45>";
$i_frontpage_dir_eclass_t = "<img src=/images/frontpage/eclass_directory_top_big5.gif border=0 width=172 height=27>";
$i_frontpage_eclass_eclass_dir = "eClass目錄";
$i_frontpage_eclass_eclass_my = "我的eClass";
$i_frontpage_campusmail_campusmail = "校園信箱";
$i_frontpage_campusmail_title_compose = "<img src=/images/campusmail/title_compose_b5.gif border=0>";
$i_frontpage_campusmail_title_inbox = "<img src=/images/campusmail/title_inbox_b5.gif border=0>";
$i_frontpage_campusmail_title_outbox = "<img src=/images/campusmail/title_outbox_b5.gif border=0>";
$i_frontpage_campusmail_title_draft = "<img src=/images/campusmail/title_draft_b5.gif border=0>";
$i_frontpage_campusmail_title_trash = "<img src=/images/campusmail/title_trash_b5.gif border=0>";
$i_frontpage_campusmail_icon_compose = "<img src=/images/frontpage/campusmail/campusmail_compose_big5.gif border=0>";
$i_frontpage_campusmail_icon_inbox = "<img src=/images/frontpage/campusmail/campusmail_inbox_big5.gif border=0>";
$i_frontpage_campusmail_icon_outbox = "<img src=/images/frontpage/campusmail/campusmail_outbox_big5.gif border=0>";
$i_frontpage_campusmail_icon_draft = "<img src=/images/frontpage/campusmail/campusmail_draft_big5.gif border=0>";
$i_frontpage_campusmail_icon_trash = "<img src=/images/frontpage/campusmail/campusmail_trash_big5.gif border=0>";
$i_frontpage_campusmail_compose = "撰寫郵件";
$i_frontpage_campusmail_inbox = "收件箱";
$i_frontpage_campusmail_outbox = "寄件箱";
$i_frontpage_campusmail_draft = "草稿箱";
$i_frontpage_campusmail_trash = "垃圾箱";
$i_frontpage_campusmail_recipients = "收件人";
$i_frontpage_campusmail_subject = "標題";
$i_frontpage_campusmail_message = "內容";
$i_frontpage_campusmail_attachment = "附件";
$i_frontpage_campusmail_important = "列為重要信件";
$i_frontpage_campusmail_notification = "要求回覆";
$i_frontpage_campusmail_size = "所佔空間 (Kb)";
$i_frontpage_campusmail_size2 = "所佔空間";
$i_frontpage_campusmail_read = "已閱讀";
$i_frontpage_campusmail_unread = "未閱讀";
$i_frontpage_campusmail_total = "總數";
$i_frontpage_campusmail_reply = "你的回覆";
$i_frontpage_campusmail_i = "I";
$i_frontpage_campusmail_a = "A";
$i_frontpage_campusmail_date = "日期";
$i_frontpage_campusmail_subject = "標題";
$i_frontpage_campusmail_sender = "寄件人";
$i_frontpage_campusmail_status = "狀況";
$i_frontpage_campusmail_folder = "收集箱";
$i_frontpage_campusmail_choose = "選擇收件人";
$i_frontpage_campusmail_remove = "刪除";
$i_frontpage_campusmail_attach = "加入附件";
$i_frontpage_campusmail_send = "傳送";
$i_frontpage_campusmail_save = "儲存草稿";
$i_frontpage_campusmail_reset = "重設";
$i_frontpage_campusmail_delete = "刪除";
$i_frontpage_campusmail_close = "關閉";
$i_frontpage_campusmail_add = "加入";
$i_frontpage_campusmail_expand = "擴張";
$i_frontpage_campusmail_retore = "恢復";
$i_frontpage_campusmail_icon_important = " <img src=/images/campusmail/btn_important.gif border=0>";
$i_frontpage_campusmail_icon_notification = "<img src=/images/frontpage/campusmail/icon_notification.gif border=0>";
$i_frontpage_campusmail_icon_attachment = "<img src=/images/frontpage/campusmail/icon_attachment.gif border=0>";
$i_frontpage_campusmail_icon_read = "<img src=/images/icon_envelope_read.gif border=0>";
$i_frontpage_campusmail_icon_unread = "<img src=/images/icon_envelope_unread.gif border=0>";
$i_frontpage_campusmail_icon_status_new = "<img src=/images/frontpage/campusmail/status_new_big5.gif border=0>";
$i_frontpage_campusmail_icon_status_read = "<img src=/images/frontpage/campusmail/status_read_big5.gif border=0>";
$i_frontpage_campusmail_icon_status_reply = "<img src=/images/frontpage/campusmail/status_replied_big5.gif border=0>";
$i_frontpage_campusmail_attachment_t = "<img src=/images/frontpage/campusmail/attachment_top_big5.gif width=450 height=40 border=0>";
$i_frontpage_campusmail_attachment_m = "<img src=/images/frontpage/campusmail/attachment_middle.gif width=450 height=16 border=0>";
$i_frontpage_campusmail_attachment_b = "<img src=/images/frontpage/campusmail/attachment_bottom.gif width=450 height=32 border=0>";
$i_frontpage_campusmail_namelist_t = "<img src=/images/frontpage/campusmail/namelist_top_big5.gif width=450 height=43 border=0>";
$i_frontpage_campusmail_namelist_m = "<img src=/images/frontpage/campusmail/namelist_middle.gif width=450 height=14 border=0>";
$i_frontpage_campusmail_namelist_b = "<img src=/images/frontpage/campusmail/namelist_bottom.gif width=450 height=21 border=0>";
$i_frontpage_campusmail_select_category = "選擇小組種類";
$i_frontpage_campusmail_select_group = "選擇小組";
$i_frontpage_campusmail_select_user = "選擇用戶";
$i_frontpage_campusmail_notification_instruction = "寄件人要求你回覆此訊息, 請輸入回覆信息或選取空格, 然後按傳送.";
$i_frontpage_campusmail_notification_instruction_message_only = "寄件人要求你回覆此訊息, 請輸入回覆信息, 然後按傳送.";
$i_frontpage_campusmail_alert_not_reply = "你是否決定不回覆此訊息?";
$i_frontpage_campusmail_new_mail = " 封新郵件";
$i_frontpage_survey = "問卷";
$i_frontpage_schedule = "日程表";
$i_frontpage_announcement = "宣佈";
$i_frontpage_notes = "學習內容";
$i_frontpage_links = "共享網址";
$i_frontpage_bulletin = "討論欄";
// Group Bulletin
$i_frontpage_bulletin_group = "組別";
$i_frontpage_bulletin_date = "時間";
$i_frontpage_bulletin_subject = "標題";
$i_frontpage_bulletin_author = "作者";
$i_frontpage_bulletin_message = "留言";
$i_frontpage_bulletin_no_record = "沒有留言於此討論欄";
$i_frontpage_bulletin_no_result = "對不起, 系統找不到您尋找的項目內容. 請嘗試修改您用的關鍵字後再試一次.";
$i_frontpage_bulletin_email_alert = "以電郵回覆作者";
$i_frontpage_bulletin_result = "結果";
$i_frontpage_bulletin_postnew = "新增標題";
$i_frontpage_bulletin_thread = "留言";
$i_frontpage_bulletin_replies = "回覆";
$i_frontpage_bulletin_reference = "回覆";
$i_frontpage_bulletin_postreply = "回覆";
$i_frontpage_bulletin_latest = "最新留言時間";
$i_frontpage_bulletin_markallread = "全部標示成已閱讀";
$i_frontpage_bulletin_alert_msg1 = "請填上文章標題";
$i_frontpage_bulletin_alert_msg2 = "請填上文章內容";
$i_frontpage_bulletin_alert_msg3 = "請填上重要字句";
$i_frontpage_bulletin_quote = "引用原文";
$i_frontpage_bulletin_instruction = "按新增標題鍵設立新的討論題目. 如需檢視及回覆其他作者的留言, 請鍵入相關的標題.";

// Group Link
$i_LinkGroup = "組別";
$i_LinkUserName = "作者";
$i_LinkUserEmail = "電子郵件";
$i_LinkCategory = "種類";
$i_LinkTitle = "題目";
$i_LinkURL = "超連結";
$i_LinkKeyword = "重要字句";
$i_LinkDescription = "描述";
$i_LinkVoteNum = "投票次數";
$i_LinkRating = "平均分數";
$i_LinkDateInput = "輸入日期";
$i_LinkDateModified = "最近修改日期";
$i_LinkSortBy = "排序方式";
$i_LinkNew = "新網址";
$i_LinkInstruction = "請按下CTRL鍵，用滑鼠選擇共享此連結的小組.";

// Group File
$i_FileGroup = "組別";
$i_FileUserName = "作者";
$i_FileUserEmail = "電子郵件";
$i_FileCategory = "種類";
$i_FileTitle = "檔案名稱";
$i_FileLocation = "檔案位置";
$i_FileKeyword = "重要字句";
$i_FileDescription = "描述";
$i_FileDateInput = "輸入日期";
$i_FileDateModified = "最近修改日期";
$i_FileSize = "檔案大小(KB)";
$i_FileNew = "新檔案";

// Campus Link
$i_CampusLinkTitle = "名稱";
$i_CampusLinkURL = "超連結";
$i_CampusLinkDescription = "校園連結會在校園資訊中顯示.";
$i_CampusLink_title = "校園連結";
$i_CampusLink_title_image = "<img src=/images/campus_links/title_b5.gif>";
// New CampusLink
$i_CampusLink_LinkName = "名稱";
$i_CampusLink_LinkAddress = "地址";
$i_CampusLink_Description = "敘述";
$i_CampusLink_DisplayOrder = "顯示次序";
$i_CampusLink_DisplayOrder_Desc = "1=最先, 10=最後";
$i_CampusLink_Identity = "身份";
$i_CampusLink_Identity_All = "全部";
$i_CampusLink_RecordType = "種類";
$i_CampusLink_RecordType_Normal = "一般";
$i_CampusLink_RecordType_Secure = "安全";

// Cycle
$i_CycleDay = "日子";
$i_CycleCycle = "循環";
$i_CycleOptions = "選項";
$i_CycleType = "顯示類別";
$i_CycleStart = "開始日期";
$i_CycleMsg = "日子循環功能會顯示於學校的行事曆中。";
$i_CycleSelect = "請選擇";
$i_CycleWeek = "日子";
$i_Cycle5Day = "五日";
$i_Cycle6Day = "六日";
$i_Cycle7Day = "七日";
$i_Cycle8Day = "八日";
$i_CycleSpecialArrangment = "特別安排";
$i_CycleSpecialForSatCounting = "以下日期, 星期六會計算循環日";
$i_CycleSpecialStart = "開始日期";
$i_CycleSpecialEnd = "結束日期";
$i_CycleSpecialNotes = "更新循環日設定後, 所有按循環日的定期資源預訂申請將會隨之更改, 請通知用戶更新其資源預訂申請。";

$i_DayType0 = array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
$i_DayType1 = array("循環日 1","循環日 2","循環日 3","循環日 4","循環日 5","循環日 6","循環日 7","循環日 8");
$i_DayType2 = array("循環日 A","循環日 B","循環日 C","循環日 D","循環日 E","循環日 F","循環日 G","循環日 H");
$i_DayType3 = array("循環日 I","循環日 II","循環日 III","循環日 IV","循環日 V","循環日 VI","循環日 VII","循環日 VIII");
$i_DayType4 = array("日","一","二","三","四","五","六");

$i_SimpleDayType0 = array ();
$i_SimpleDayType1 = array ("1","2","3","4","5","6","7","8");
$i_SimpleDayType2 = array ("A","B","C","D","E","F","G","H");
$i_SimpleDayType3 = array ("I","II","III","IV","V","VI","VII","VIII");

// Homework
$i_Homework_WholeSchool = "<font color=#FF6600>一般班別</font>";
$i_Homework_New = "新增";
$i_Homework_Import = "匯入";
$i_Homework_SearchTeacher = "按發出人名字搜尋";
$i_Homework_SearchSubject = "按科目搜尋";
$i_Homework_SearchTeaching = "按老師任教科目搜尋";
$i_Homework_class = "班別";
$i_Homework_subject = "科目";
$i_Homework_title = "標題";
$i_Homework_description = "內容";
$i_Homework_loading = "工作量";
$i_Homework_loading_csv_header = "需時";
$i_Homework_startdate = "開始日期";
$i_Homework_duedate = "限期";
$i_Homework_hours = "小時";
$i_Homework_morethan = "多於";
$i_Homework_lastModified = "最近修改時間";
$i_Homework_view = "檢視";
$i_Homework_unauthorized = "你沒有權限檢視這個紀渮";
$i_Homework_404 = "這紀錄並不存在";
$i_Homework_TeacherName = "教師";
$i_Homework_new_failed ="輸入失敗, 請檢查輸入的資料是否正確";
$i_Homework_new_succuess = "紀錄已新增";
$i_Homework_edit_failed = "請檢查輸入的資料是否正確.";
$i_Homework_new_startdate_wrong ="開始日期必須是今天或以後";
$i_Homework_new_duedate_wrong ="限期必須是開始日期或以後";
$i_Homework_new_title_missing = "課題必須輸入";
$i_Homework_no_subject_alert = "你確定要提交沒有科目的家課紀錄表?";
$i_Homework_new_subject_missing = "請選擇科目";
$i_Homework_new_homework_type_missing="請選擇家課類型";
$i_Homework_import_errors = "部份資料錯誤, 請檢查檔案再試";
$i_Homework_import_success = "已成功匯入";
$i_Homework_new_failed = "資料錯誤, 請檢查再試";
$i_Homework_new_success = "已新增紀錄";
$i_Homework_admin_startdate = "只可設定今天家課";
$i_Homework_admin_parent = "家長可檢視各班家課";
$i_Homework_admin_disable = "不使用家課紀錄表";
$i_Homework_admin_disable_search_subject = "不使用科目搜尋";
$i_Homework_admin_disable_search_teacher = "不使用發出人名字搜尋";
$i_Homework_admin_disable_search_taught = "不使用以教師任教職務搜尋";
$i_Homework_admin_nonteaching_access = "非教學職務員工可以新增家課 (所有科目及班別)";
$i_Homework_admin_restrict_access = "教學職務員工只可新增任教科目及班別";
$i_Homework_admin_enable_subjectleader = "容許科長輸入家課";
$i_Homework_admin_enable_export = "容許所有職員匯出家課";
$i_Homework_admin_clear_all_records = "清除所有家課紀錄";
$i_Homework_admin_clear_all_records_alert = "你確定要清除家課紀錄?";
$i_Homework_admin_past_day_allowed = "容許輸入舊日紀錄";
$i_Homework_admin_use_homework_type = "使用家課類型";
$i_Homework_admin_use_homework_handin = "使用家課點收功能";
$i_Homework_admin_use_teacher_collect_homework = "顯示班主任是否收回有關功課";
$i_Homework_list = "家課紀錄表";
$i_Homework_overdue_list="欠交家課清單";
$i_Homework_handin_list = "家課繳交清單";
$i_Homework_handin_current_status ="現在狀況";
$i_Homework_Handin_Required="須繳交";
$i_Homework_Collect_Required="班主任是否收回有關功課";
$i_Homework_Collected_By_Class_Teacher = "班主任收回功課";
$i_Homework_handin_records_will_be_deleted="這份家課的繳交清單將會被刪除";
$i_Homework_handin_continue="繼續";
$i_Homework_handin_not_handin="未繳交";
$i_Homework_handin_handin="已繳交";
$i_Homework_handin_no_need_to_handin="不需繳交";
$i_Homework_handin_change_status="更改狀況";
$i_Homework_handin_set_NotHandin_to_NoNeedHandin="把未繳交轉為不需繳交";
$i_Homework_handin_set_All_to_Handin="全部轉為已繳交";
$i_Homework_handin_set_All_to_NotHandin="全部轉為未繳交";


$i_Homework_detail = "詳情";
$i_Homework_import_sample = "按此下載範本檔案 (CSV 格式)";
$i_Homework_ToDoList = "功課清單";
$i_Homework_AllRecords = "所有紀錄";
$i_Homework_teacher_name = "教師";
$i_Homework_edit = "編輯";
$i_Homework_myrecords = "<font color=#FF6600>我的紀錄</font>";
$i_Homework_myrecords_icon = "<img src=/images/homeworklist/icon_myrecord.gif>";
$i_Homework_today = "<img src=/images/homework/b5/hw/icon_todayhw.gif border=0 width=29 height=15>";
$i_Homework_con_add = "<font color=green>已新增紀錄</font>";
$i_Homework_con_add_failed = "<font color=red>資料錯誤, 請檢查再試</font>";
$i_Homework_import_instruction = "請選擇檔案";
$i_Homework_import_points = "請注意:";
$i_Homework_import_instruction1 = "班名和科目名稱須要完全正確";
//$i_Homework_import_instruction2 = "需時必須為整數, 每半小時為一個單位. 例: 5 代表 2.5 小時. (0 為少於0.5 小時, 17 為多於8 小時)";
$i_Homework_import_instruction2 = "需時必須為整數, 每半小時為一個單位. 例: 2.5 代表 1.25 小時. (0 為少於0.25 小時, 17 為多於8 小時)";
$i_Homework_import_instruction3 = "日期的格式必須為 YYYY-MM-DD 或 YYYY/MM/DD. 如你使用 Excel 編輯, 請在日期的資料上更改儲存格格式.";
$i_Homework_import_instruction4 = "除負責老師及".$i_Homework_description."以外, 所有資料必須填寫.";
$i_Homework_import_instruction5 = "不能$button_import"."特別組別家課.";
$i_Homework_import_instruction6 = "家課類型名稱須要完全正確";
$i_Homework_import_reference = "請參考";
$i_Homework_import_sample = "範本檔案 (CSV 格式)";
$i_Homework_import_class = "各班名稱";
$i_Homework_import_subject = "各科目名稱";
$i_Homework_import_homeworktype = "各家課類型名稱";
$i_Homework_minimum = "--";
$i_Homework_WholeImage = "<img border=0 src=/images/btn_wholeschhomework_b5.gif>";
$i_Homework_WholeIcon = "<img src=/images/homeworklist/icon_wholeschhomework.gif>";
$i_Homework_MyRecordsImage = "<img border=0 src=/images/btn_myrecord_b5.gif>";
$i_Homework_PageTitle = "<img src=/images/homeworklist/title_b5.gif>";
$i_Homework_PleaseSelectType = "請選擇家課類別";
$i_Homework_ClassType = "班別家課";
$i_Homework_GroupType = "特別班別家課";
$i_Homework_Type = "家課類別";
$i_Homework_Group = "特別班別";
$i_Homework_IncludingClass = "包括班別";
$i_Homework_MultipleClassInstruction = "(可按下 CTRL 鍵，用滑鼠選擇多於一班)";
$i_Homework_Error_NoAccessRight = "此用戶沒有製作家課之權限";
$i_Homework_Error_HandIn = "請填上'yes'或'no'選擇是否需要家課點收";
$i_Homework_Error_Collect= "請填上'yes'或'no'選擇是否需要家課收集";
$i_Homework_Error_Empty = "部份欄位沒有填上";
$i_Homework_Error_NoSubject = "沒有此科目";
$i_Homework_Error_NoClass = "沒有此班別";
$i_Homework_Error_Date = "日期錯誤";
$i_Homework_Error_StartDate = "$i_Homework_startdate"."早於今天";
$i_Homework_Error_EndDate = "$i_Homework_duedate"."早於"."$i_Homework_startdate";
$i_Homework_Error_NoHomeworkType = "沒有此家課類型";
$i_Homework_SpecialGroup = "特別班別";
$i_Homework_NormalClass = "普通班別";
$i_Homework_WholeSchoolGroup = "<font color=#FF6600>$i_Homework_SpecialGroup</font>";
$i_Homework_ListView = "清單模式";
$i_Homework_MonthView = "月份模式";
$i_Homework_WeekView = "星期模式";
$i_Homework_ChildrenList = "子女名單";
$i_Homework_Only = "之家課";
$i_Homework_eClassAssignment = "此家課屬於課程: ";
$i_Homework_SubjectLeaderKey = "為此班科長";
$i_Homework_CreatedHomework = "所製作的家課";
$i_Homework_ByTeaching = "所任教科目的家課";
$i_Homework_Poster = "發出人";
$i_Homework_Export_Instruction = "請輸入日期範圍以搜尋需匯出的家課紀錄";
$i_Homework_HomeworkType = "家課類型";

$i_Subject_name = "科目名稱";
$i_SAMS_code = "代碼";
$i_SAMS_cmp_code = "分卷代碼";
$i_SAMS_description = "說明 (英/中)";
$i_SAMS_abbr_name = "名稱縮寫 (英/中)";
$i_SAMS_short_name = "簡稱 (英/中)";
$i_SAMS_CSV_file = "WEBSAMS CSV 檔";
$i_SAMS_download_subject_csv  = "下載科目 CSV 檔案範本";
$i_SAMS_download_cmp_subject_csv  = "下載科目分卷 CSV 檔案範本";
$i_SAMS_import_error_wrong_format = "錯誤的匯入格式";
$i_SAMS_import_update_no = "成功更新資料項數";
$i_SAMS_import_add_no = "成功加入資料項數";
$i_SAMS_import_fail_no = "不能加入資料項數";
$i_SAMS_import_error_row = "行數";
$i_SAMS_import_error_data = "錯誤項目";
$i_SAMS_import_error_reason = "錯誤原因";
$i_SAMS_import_error_detail= "檔案資料";
$i_SAMS_short_name_duplicate = "簡稱不可重複";
$i_SAMS_import_error_unknown = "不明";
$i_SAMS_notes_after = "之後";

$i_import_error = "部份資料有誤. 下列顯示的紀錄未能新增.";
$i_image_home = "<img src=/images/home.gif border=0>";

$i_Campusquota_basic_identity = array("教師","學生","職員","家長");
$i_Campusquota_identity = "身份";
$i_Campusquota_quota = "儲存量 (以MBytes 為單位)";
$i_Campusquota_noLimit = "無限制";
$i_Campusquota_max = "儲存量";
$i_Campusquota_used = "已用";
$i_Campusquota_left = "尚餘";
$i_Campusquota_uploadSuccess = "附件上載成功 ";
$i_Campusquota_noQuota = "儲存量不足";
$i_Campusquota_using1 = "你使用了 ";
$i_Campusquota_using2 = " 的儲存量.";
$i_Set_enabled = "允許 ";

$i_Tempfiles_display1 = "個檔案並佔用了 ";
$i_Tempfiles_display2 = " MBytes.";
$i_Tempfiles_confirm = "如欲移除這些檔案, 請按刪除.";

$i_Batch_Default = "<font color=red> *</font>";
$i_Batch_SetDefault = "設定為學校時間表";
$i_Batch_Description = "此時間表之說明";
$i_Batch_DefaultDescription = "* - 學校時間表";
$i_Batch_TitleNotNull = "時間表名稱必須輸入.";
$i_Batch_TitleDuplicated = "不能使用相同的時間表名稱.";
$i_Slot_TimeRange = "時間";
$i_Slot_Title = "時段名稱";
$i_Slot_ChangeArchiveRecords = "取消所有使用此時間表的預訂紀錄 (今天或以後的預訂紀錄會被刪除,過去的紀錄會被保存).<br> $i_ResourceSlotChangeAlert2";
$i_Slot_NotArchiveAlert = "你選擇不取消預訂紀錄, 可能會引起資料錯誤, 是否繼續?";

$i_schoolinfo_organization_iconleft_admin = "<img src=/images/organization/icon_admin.gif>";
$i_schoolinfo_organization_iconright_admin = "<img src=/images/organization/adminheading_b5.gif>";
$i_schoolinfo_organization_iconbottom_admin = "<img src=/images/organization/adminfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_admin_popup = "<img src=/images/organization/adminfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_academic = "<img src=/images/organization/icon_academic.gif>";
$i_schoolinfo_organization_iconright_academic = "<img src=/images/organization/academicheading_b5.gif>";
$i_schoolinfo_organization_iconbottom_academic = "<img src=/images/organization/academicfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_academic_popup = "<img src=/images/organization/academicfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_class = "<img src=/images/organization/icon_class.gif>";
$i_schoolinfo_organization_iconright_class = "<img src=/images/organization/classheading_b5.gif>";
$i_schoolinfo_organization_iconbottom_class = "<img src=/images/organization/classfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_class_popup = "<img src=/images/organization/classfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_house = "<img src=/images/organization/icon_house.gif>";
$i_schoolinfo_organization_iconright_house = "<img src=/images/organization/househeading_b5.gif>";
$i_schoolinfo_organization_iconbottom_house = "<img src=/images/organization/housefrbottom.gif>";
$i_schoolinfo_organization_iconbottom_house_popup = "<img src=/images/organization/housefrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_eca = "<img src=/images/organization/icon_eca.gif>";
$i_schoolinfo_organization_iconright_eca = "<img src=/images/organization/ecaheading_b5.gif>";
$i_schoolinfo_organization_iconbottom_eca = "<img src=/images/organization/ecafrbottom.gif>";
$i_schoolinfo_organization_iconbottom_eca_popup = "<img src=/images/organization/ecafrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_misc = "<img src=/images/organization/icon_misc.gif>";
$i_schoolinfo_organization_iconright_misc = "<img src=/images/organization/mischeading_b5.gif>";
$i_schoolinfo_organization_iconbottom_misc = "<img src=/images/organization/miscfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_misc_popup = "<img src=/images/organization/miscfrbottom_popup.gif>";

$image_back = "/images/btn_back_b5.gif";
$image_cancel = "/images/btn_cancel_b5.gif";
$image_chooserecipient = "/images/btn_chooserecipients_b5.gif";
$image_delete = "/images/btn_delete_b5.gif";
$image_next = "/images/btn_next.gif";
$image_ok = "/images/btn_ok_b5.gif";
$image_pre = "/images/btn_pre.gif";
$image_removeselected = "/images/btn_removeselected_b5.gif";
$image_reset = "/images/btn_reset_b5.gif";
$image_save = "/images/btn_save_b5.gif";
$image_send = "/images/btn_send_b5.gif";
$image_submit = "/images/btn_submit_b5.gif";
$image_update = "/images/btn_update_b5.gif";
$image_periodic_a = "btn_periodic_a_b5.gif";
$image_periodic_b = "btn_periodic_b_b5.gif";
$image_single_a = "btn_single_a_b5.gif";
$image_single_b = "btn_single_b_b5.gif";
$image_moreAttach = "/images/btn_moreattachment_b5.gif";
$image_add = "/images/btn_add_b5.gif";
$image_closewindow = "/images/btn_closewindow_b5.gif";
$image_expand = "/images/btn_expand_b5.gif";
$image_selectall = "/images/btn_selectall_b5.gif";
$image_iconlist = "/images/groupinfo/iconlist_b5.gif";
$image_nextweek = "<img border=0 src=/images/btn_nextweek_b5.gif width=131 height=24>";
$image_prevweek = "<img border=0 src=/images/btn_preweek_b5.gif width=124 height=24>";
$image_prevday = "<img border=0 src=/images/btn_preday_b5.gif width=124 height=24>";
$image_nextday = "<img border=0 src=/images/btn_nextday_b5.gif width=131 height=24>";
$image_import = "/images/btn_import_b5.gif";
$image_search = "/images/btn_search_b5.gif";
$image_include_quote = "/images/btn_includequote_b5.gif";
$image_edit = "/images/btn_edit_b5.gif";
$image_clear = "/images/btn_clear_b5.gif";
$image_restore = "/images/btn_restore_b5.gif";
$image_viewSlots = "/images/btn_viewslots_b5.gif";
$image_updaterole = "/images/btn_updaterole_b5.gif";
$image_delete_member = "/images/btn_deletemember_b5.gif";
$image_attachfiles = "/images/btn_attachfiles_b5.gif";
$image_authorize = "/images/btn_authorize.gif";
$image_forbid = "/images/btn_fornid.gif";
$image_reject = "/images/btn_reject.gif";
$image_go = "/images/btn_go.gif";
$image_cs = "btn_customersupport.gif";
$image_cs_blink = "btn_customersupport_blink.gif";
$image_cs_admin = "btn_customersupport_admin.gif";
$image_cs_admin_blink = "btn_customersupport_admin_blink.gif";

$i_grouphead_timetable = "<img src=/images/groupinfo/heading_timetable_b5.gif>";
$i_grouphead_chatroom = "<img src=/images/groupinfo/heading_chatroom_b5.gif>";
$i_grouphead_bulletin = "<img src=/images/groupinfo/heading_bulletin_b5.gif>";
$i_grouphead_sharedlinks = "<img src=/images/groupinfo/heading_sharedlinks_b5.gif>";
$i_grouphead_sharedfiles = "<img src=/images/groupinfo/heading_sharedfiles_b5.gif>";
$i_grouphead_groupsetting = "<img src=/images/groupinfo/heading_groupsetting_b5.gif>";
$i_grouphead_questionbank = "<img src=/images/groupinfo/heading_schoolbasedqb.gif>";

$i_campusmail_stamp = "<img border=0 src=/images/campusmail/stamp.gif>";
$i_campusmail_compose = "<img border=0 src=/images/campusmail/icon_compose_b5.gif>";
$i_campusmail_inbox = "<img border=0 src=/images/campusmail/icon_inbox_b5.gif>";
$i_campusmail_outbox = "<img border=0 src=/images/campusmail/icon_outbox_b5.gif>";
$i_campusmail_draft = "<img border=0 src=/images/campusmail/icon_draft_b5.gif>";
$i_campusmail_trash = "<img border=0 src=/images/campusmail/icon_trash_b5.gif>";
$i_campusmail_lettertext = "<img border=0 src=/images/campusmail/lettertext.gif>";
$i_campusmail_novaliduser = "你所選擇的收件人並沒有權限使用校園信箱";
$i_campusmail_policy = "要求收信人回覆的政策";
$i_campusmail_policy_original = "收件人必須回覆, 寄件人才能得知收件人已查閱該信件.";
$i_campusmail_policy_auto = "收件人只要打開信件, 寄件人便能得知收件人已查閱該信件.";
$i_campusmail_nondelivery = "以下用戶未能接收此訊息, 因為他們的郵箱已滿. 他們需要清理郵箱, 或你可以將沒有附件之訊息發送給他們.";
$i_campusmail_sent_successful = "訊息已傳送";
$i_campusmail_save_successful = "訊息已儲存";
$i_campusmail_processing = "處理中 ...";
$i_campusmail_processing_note = "如果收件人數多, 所需時間將會較長, 請耐心等候.";
$i_campusmail_accessright = "使用權";
$i_campusmail_ban = "禁止用戶發送郵件";
$i_campusmail_ban_instruction = "請輸入限制用戶的登入名稱. <br>每一行輸入一個登入名稱.<br>這些用戶不能發送郵件, 但能接收郵件及回應訊息.<br>";
$i_campusmail_alert_trashall = "你確定要把所有郵件放進垃圾箱?";
$i_campusmail_alert_emptytrash = "你確定要把垃圾箱的郵件永久刪除?";
$i_campusmail_alert_export = "只有訊息內容會被匯出(即不包括附件檔案). 你確定要繼續?";
$i_campusmail_forceRemoval = "強制移除";
$i_campusmail_forceRemoval_instruction1 = "請輸入開始及結束日期, 所有於此段期間的校園信箱郵件會被移除";
$i_campusmail_forceRemoval_Start = "開始日期";
$i_campusmail_forceRemoval_End = "結束日期";
$i_campusmail_next = "下一封";
$i_campusmail_prev = "上一封";

$i_campusmail_recipient_selection_control="收件者管理";

$i_campusmail_no_users_available="沒有收件人可供選擇";
$i_campusmail_usage_setting = "使用設定";



$i_campusmail_select_interface="選擇介面";
$i_campusmail_use_old_interface= "使用舊介面";
$i_campusmail_use_new_interface= "使用新介面";
$i_campusmail_staff_recipients_selection ="職員收件者";
$i_campusmail_student_recipients_selection="學生收件者";
$i_campusmail_parent_recipients_selection ="家長收件者";
$i_campusmail_blocked_groups="禁止選擇的小組類別";
$i_campusmail_blocked_usertypes="禁止選擇的用戶類別";
$i_campusmail_group_categories="小組類別";
$i_campusmail_usertypes ="用戶類別";
$i_campusmail_select_parents_from_students="從學生名字選擇家長";
$i_campusmail_teaching_staff="教學職務員工";
$i_campusmail_non_teaching_staff="非教學職務員工";

$i_campusmail_all_teacher_staff="所有教學及非教學職員";
$i_campusmail_no_teacher_staff="不可寄給職員";
$i_campusmail_teaching_sender_children_teacher="現正任教寄信者子女的教師";
$i_campusmail_teaching_sender_teacher="現正任教寄信者的教師";

$i_campusmail_all_students="所有學生";
$i_campusmail_only_students_in_same_class_level="只可寄給相同級別的學生";
$i_campusmail_only_students_in_same_class="只可寄給同班學生";
$i_campusmail_no_students="不可寄給學生";
$i_campusmail_only_students_in_taught_class="只可寄給任教班別的學生";
$i_campusmail_only_sender_own_children="只可寄給自己的子女";

$i_campusmail_all_parents="所有家長";
$i_campusmail_only_parents_with_children_in_taught_class="只可寄給任教班別的學生的家長";
$i_campusmail_only_parents_with_children_in_same_class_level="只可寄給相同級別的學生的家長";
$i_campusmail_only_parents_with_children_in_same_class="只可寄給同班學生的家長";
$i_campusmail_no_parents="不可寄給家長";
$i_campusmail_only_own_parents="只可寄給學生自己的家長";

$i_header_myaccount = "<img border=0 src=/images/header/icon_myaccount_b5.gif>";
$i_header_myaccount_teacher = "<img border=0 src=/images/header/icon_myaccount_t_b5.gif>";
$i_header_newmail = "<img border=0 src=/images/header/icon_newmail.gif>";
$i_header_newmail_icon_path = "/images/header/icon_newmail.gif";
$i_header_studentprofile_icon = "$image_path/header/icon_studentprofile_b5.gif";

$i_myac_title = "我的戶口";
$i_myac_myinfo = "個人資料";

$i_Webmail_title = "網上電郵";

$i_CampusTV_top = "<img src=/images/campus_tv/tv_top_b5.gif width=640 height=110>";
$i_CampusTV_Title = "標題";
$i_CampusTV_56k = "窄頻 (56K)";
$i_CampusTV_200k = "寬頻 (200K)";
$i_CampusTV_Select = "請選擇播放片段及頻寬";
$i_CampusTV_Failed = "如無法顯示片段畫面，";
$i_CampusTV_alt = "請按此開啟。";

$i_SMS_System_Message = "系統訊息";
$i_SMS_Balance_Lass_Than = "戶口結存少於";
$i_SMS_Notification = "短訊通知";
$i_SMS_NoChangeMobile = "(此流動電話號碼只用作接收 SMS 短訊之用, 如果你需要更改此項資料, 請聯絡系統管理員.)";
$i_SMS_Cautions = "請注意:";
$i_SMS_Limitation1 = "全英文短訊, 最多可輸入160字元.";
$i_SMS_Limitation2 = "如短訊包含BIG5編碼之繁體中文字, 則最多可輸入70字元(包括中英文字元及標點符號).";
$i_SMS_Limitation3 = "超出字數上限的短訊將被刪除過長部分.";
$i_SMS_Limitation4 = "部份手機或未能顯示中文標點符號(如\"，\"或\"。\"), 建議使用英文標點符號(如\",\"或\".\").";
$i_SMS_Limitation5 = "不能顯示之特殊字元: ~^`{}\|[]";
$i_SMS_Limitation6 = "Unicode編碼之字元可能不能顯示.";
$i_SMS_SMS = "短訊服務";
$i_SMS_Send = "傳送短訊";
$i_SMS_Send_Confirmation = "傳送短訊確認";
$i_SMS_Send_Broadcast = "傳送訊息至所有用戶";
$i_SMS_Send_SelectUser = "傳送短訊至內聯網用戶";
$i_SMS_Send_SelectParent = "傳送短訊至家長(以學生選擇)";
$i_SMS_Send_Raw = "傳送短訊(輸入電話號碼)";
$i_SMS_UsageReport = "使用報告";
$i_SMS_MessageTemplate = "訊息範本";
$i_SMS_MobileNumber = "流動電話號碼";
$i_SMS_Recipient = "接收人";
$i_SMS_Recipient2 = "接收人";
$i_SMS_RelatedUser = "相關用戶";
$i_SMS_SeparateRecipient = "電話號碼須以分號(;) 分隔";
$i_SMS_MessageContent = "短訊內容";
$i_SMS_SendDate = "傳送日期";
$i_SMS_SendTime = "傳送時間";
$i_SMS_Status = "狀態";
$i_SMS_TargetSendTime = "實際傳送時間";
$i_SMS_SubmissionTime = "請求傳送時間";
$i_SMS_NoteSendTime = "如即時傳送, 請留空傳送日期及時間";
$i_SMS_MessageSent = "<font color=green>短訊已傳送</font>";
$i_SMS_AlertSelectUserType = "請最少選擇一個身份";
$i_SMS_AlertMessageContent = "請輸入訊息內容";
$i_SMS_AlertRecipient = "請輸入接收人";
$i_SMS_TargetStudent = "目標學生";
$i_SMS_ClearRecords = "清除所有紀錄";
$i_SMS_RetrieveRecords = "由服務中心查詢紀錄";
$i_SMS_MessageSentSuccessfully = "已傳送的訊息 (包括未由服務中心更新的訊息)";
$i_SMS_MessageQuotaLeft = "尚餘訊息數目";
$i_SMS_con_NoUser = "<font color=red>所選用戶並沒有輸入流動電話號碼</font>";
$i_SMS_con_DataRetrieved = "<font color=green>已由服務中心取得資料</font>";
$i_SMS_NotExamined = "未更新";
$i_SMS_InTransit = "傳送中";
$i_SMS_Delivered = "已傳送";
$i_SMS_Expired = "已過期";
$i_SMS_Deleted = "已被刪除";
$i_SMS_Undeliverable = "不能傳送";
$i_SMS_Accepted = "已接納";
$i_SMS_Invalid = "不合規格";
$i_SMS_Pending = "等待中";
$i_SMS_InvalidPhone = "號碼錯誤";
$i_SMS_Status0 = $i_SMS_NotExamined;
$i_SMS_Status1 = $i_SMS_InTransit;
$i_SMS_Status2 = $i_SMS_Delivered;
$i_SMS_Status3 = $i_SMS_Expired;
$i_SMS_Status4 = $i_SMS_Deleted;
$i_SMS_Status5 = $i_SMS_Undeliverable;
$i_SMS_Status6 = $i_SMS_Accepted;
$i_SMS_Status7 = $i_SMS_Invalid;
$i_SMS_Status8 = $i_SMS_Pending;
$i_SMS_Status9 = $i_SMS_InvalidPhone;
$i_SMS_24Hr = "兩位數之二十四小時格式";
$i_SMS_InvalidTime = "時間格式錯誤";
$i_SMS_TemplateName = "範本名稱";
$i_SMS_TemplateType = "範本類別";
$i_SMS_TemplateContent = "內容";
$i_SMS_File_Send = "檔案輸入";
$i_SMS_ColData = "第一欄資料";
$i_SMS_Type_Mobile = "流動電話號碼";
$i_SMS_Type_Login = "$i_UserLogin";
$i_SMS_UploadFile = "選擇檔案";
$i_SMS_FileDescription = "<u><b>檔案說明:</b></u><br>
Column 1: 流動電話號碼<br>
Column 2: SMS 訊息.";
$i_SMS_FileConfirm = "請確認所上載的訊息正確, 然後按傳送.";
$i_SMS_SendTo_Student_Guardian ="傳送短訊至學生監護人";
$i_SMS_Notice_Send_To_Users ="將會傳送訊息至以下接收人:";
$i_SMS_Warning_Cannot_Send_To_Users="無法傳送訊息至以下接收人:";
$i_SMS_Error_No_Guardian ="沒有監護人";
$i_SMS_Error_NovalidPhone = "沒有合適的號碼";
$i_SMS_Notice_Show_Student_With_MobileNo_Only="不顯示沒有流動電話號碼的學生";
$i_SMS_Notice_Show_Student_With_Guardian_Only="不顯示沒有監護人的學生";

$i_SMS_SystemTemplate = "特別情況範本";
$i_SMS_NormalTemplate = "通用範本";
$i_SMS_Personalized_Msg_Tags = "自動插入項目";

/*
$i_SMS_Personalized_Tag['user_name_eng'] = "Target user's English name";
$i_SMS_Personalized_Tag['user_name_chi'] = "Target user's Chinese name";
$i_SMS_Personalized_Tag['user_class_name'] = "Target student's class name";
$i_SMS_Personalized_Tag['user_class_number'] = "Target student's class number";
$i_SMS_Personalized_Tag['user_login'] = "Target user's login name";
$i_SMS_Personalized_Tag['attend_card_time'] = "Arrival time or departure time when tapping card";
$i_SMS_Personalized_Tag['payment_balance'] = "Student's balance left";
$i_SMS_Personalized_Tag['payment_outstanding_amount'] = "Student's total outstanding amount";
*/


$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_ABSENCE'] 		= "缺席";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_LATE'] 		= "遲到";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_EARLYLEAVE'] 	= "早退";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_ARRIVAL'] 		= "已回校";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_LEAVE'] 		= "離開學校";
$i_SMS_Personalized_Msg_Template['STUDENT_MAKE_PAYMENT'] 		= "繳款";
$i_SMS_Personalized_Msg_Template['BALANCE_REMINDER'] 			= "戶口金額提示";

$i_SMS_Personalized_Template_Instruction="你可在自訂範本訊息中，加入如學生姓名或離校拍卡時間等資料。<BR>
<BR>
要加入該等資料，請從以下選單中，選取適用的項目，然後按\"增加\"，系統將以如(\$user_class_name)等的符號代表有關資料。<BR><BR>";
$i_SMS_Personalized_Template_Type="範本類型";
$i_SMS_Personalized_Template_SendCondition="訊息發出情況";


$i_SMS_no_student_select = "請最少選擇一個學生!";
$i_SMS_RecipientCount = "接收人數目";
$i_SMS_PIC = "PIC";
$i_SMS_Reply_Message="可回覆訊息";
$i_SMS_Reply_Message_View="檢視訊息";
$i_SMS_Reply_Message_View_Sent_Message="檢視已發送訊息";
$i_SMS_Reply_Message_View_Replied_Message="檢視已回覆訊息";
$i_SMS_Reply_Message_Message="訊息";
$i_SMS_Reply_Message_Replied_MessageCount="已回覆人數";
$i_SMS_Reply_Message_Replied_Message_DeliveryTime="發出時間";
$i_SMS_Reply_Message_Replied_Message="回覆訊息";
$i_SMS_Reply_Message_Replied_PhoneNumber="回覆號碼";
$i_SMS_Reply_Message_Replied_Time="回覆時間";
$i_SMS_Reply_Message_Status_Not_Yet_Replied="未回覆";
$i_SMS_Reply_Message_Status_Replied="已回覆";
$i_SMS_Reply_Message_Replied_Message_Stat="回覆訊息統計";
$i_SMS_Reply_Message_Retrieve="擷取回覆信息";
$i_SMS_Reply_Message_DateRange_Msg="顯示以下日子期間所有的紀錄";

$i_SMS_Select_DateRange="下載以下日期範圍內的$i_SMS_UsageReport";
$i_SMS_MultipleMessage="多項訊息";

$i_SMS['NoPluginWarning'] = "你可以透過eClass綜合平台發送即時短訊予學生或家長。請即聯絡銷售人員啟動有關服務!";
$i_SMS['jsWarning']['NoPluginWarning'] = "短訊服務尚未啟動, 請即聯絡銷售人員啟動有關服務!";


$i_iconAttachment = "<img src=/images/icon_attachment.gif border=0>";

$i_Alert_Reject = "你是否確定要拒絕?";
$i_Alert_Checkin = "你是否確定要取用?";
$i_Alert_Checkout = "你是否確定要退還?";

$i_label_SelectAll = "全選";

$i_Step_Name = "<img src=/images/step/step_b5.gif border=0>";

$i_organization_description = "此為本校的組織架構, 當中環節包含了本校不同的功能及活動組別. 要進入個別小組的網頁, 請按 $i_image_home (如有網頁). 要檢視小組成員名單, 請按該小組名稱.";

$i_SelectMultipleInstruction = "請按下CTRL鍵，用滑鼠選擇多於一個";
$i_SelectMemberInstruction = "請先選擇類別. 您可以於第 2 步選擇整個小組, 或於第 3 步選擇個別成員.";
$i_SelectMemberSteps = array (
"",
"選擇類別",
"選擇整個小組或檢視個別成員",
"選擇個別成員"
);
$i_SelectMemberNoGroupInstruction = "請先選擇新用戶身份, 然後選擇類別. 接著便可以檢視小組成員，及選擇個別用戶.";
$i_SelectMemberNoGroupSteps = array (
"",
"選擇新用戶身份",
"選擇類別",
"檢視個別成員",
"選擇個別成員"
);

$i_toggleBy = "只顯示";

$i_sls_library_mapping_file = "用戶連結檔案";
$i_sls_library_mapping_file_select = "選擇上載檔案";
$i_sls_library_mapping_file_instruction = "請注意, 使用中之連結檔案將會被取代. 如果只是新增用戶連結, 請先下載使用中之連結檔案再把新連結加到檔案中, 然後再進行上載.";
$i_sls_library_mapping_file_use = "此用戶連結檔案是作為連結 eClass 內聯網及 SLS 圖書館系統之用. 此連結檔案由另一個獨立程式產生. 如果你不懂得如何產生此檔案, 請與博文教育技術支援部聯絡.";
$i_sls_library_mapping_file_download = "下載現在使用的連結檔案.";
$i_sls_library_mapping_file_upload_success = "<font color=green>已成功上載.</font>";
$i_sls_library_search = "檢索";
$i_sls_library_info = "一般資料";
$i_sls_library_newadditions = "新書推介";
$i_sls_library_patron_top = "借書龍虎榜";
$i_sls_library_item_top = "熱門書籍龍虎榜";
$i_sls_library_patron_info = "Patron Information";
$i_sls_library_checkout_list = "借出書目";
$i_sls_library_hold_list = "預留書目";
$i_sls_library_outstanding_fine = "過期圖書罰款";
$i_sls_library_norecords = "&#26283;&#26178;&#20173;&#26410;&#26377;&#20219;&#20309;&#35352;&#37636;";          // 暫時仍未有任何紀錄
$i_sls_library_image_newadditions = "<img src='images/sls_info_btn_new_add.gif' border='0'>";
$i_sls_library_image_patron_top = "<img src='images/sls_info_btn_top_patron.gif' border='0'>";
$i_sls_library_image_item_top = "<img src='images/sls_info_btn_top_item.gif' border='0'>";
$i_sls_library_image_checkout_list = "<img src='images/sls_info_btn_checkout.gif' border='0'>";
$i_sls_library_image_hold_list = "<img src='images/sls_info_btn_hold.gif' border='0'>";
$i_sls_library_title_image_newadditions = "<img src='images/sls_info_table_new_add.gif' border='0'>";
$i_sls_library_title_image_patron_top = "<img src='images/sls_info_table_top_patron.gif' border='0'>";
$i_sls_library_title_image_item_top = "<img src='images/sls_info_table_top_item.gif' border='0'>";
$i_sls_library_title_image_checkout_list = "<img src='images/sls_info_table_checkout.gif' border='0'>";
$i_sls_library_title_image_hold_list = "<img src='images/sls_info_table_hold.gif' border='0'>";
$i_sls_library_book_title = "&#26360;&#30446;&#21517;&#31281;"; // 書目名稱
$i_sls_library_call_number = "&#32034;&#26360;&#34399;";        // 索書號
$i_sls_library_from = "&#30001;&#65306;";
$i_sls_library_to = "&#33267;&#65306;";
$i_sls_library_reader_name = "&#35712;&#32773;&#22995;&#21517;";              // 讀者姓名
$i_sls_library_reader_class = "&#29677;&#21029;&#21450;&#23416;&#34399;";     // 班別及學號
$i_sls_library_read_count = "&#20511;&#38321;&#25976;&#37327;";               // 借閱數量
$i_sls_library_borrow_count = "&#20511;&#20986;&#27425;&#25976;";             // 借出次數
$i_sls_library_return_date = "&#36996;&#26360;&#26085;&#26399;";              // 還書日期
$i_sls_library_current_status = "&#29694;&#26178;&#29376;&#24907;";           // 現時狀態

$i_motd_description = "此訊息將會在用戶登入後的首頁以流動形式顯示.";

$i_QB_AccessGroup = "使用中央題目庫之組別";
$i_QB_ModifySetting = "按下你欲更改設定 (程度, 種類及難易度) 的組別名稱.";
$i_QB_NoGroupUsing = "現時沒有設定任何可使用中央題目庫之組別.";
$i_QB_Category = "種類";
$i_QB_Level = "程度";
$i_QB_Difficulty = "難易度";
$i_QB_EngName = "英文名稱";
$i_QB_ChiName = "中文名稱";
$i_QB_DisplayOrder = "顯示次序";
$i_QB_admin_titlegif = "/images/admin/system_admin/sls_b5.gif";
$i_QB_NameCantEmpty = "你必須填上英文或中文名稱";
$i_QB_SelectFile = "選擇檔案:";
$i_QB_DownloadSettingSample = "按此處下載範本(CSV) 檔案.";
$i_QB_Pending = "待批";
$i_QB_Public = "已批核";
$i_QB_Rejected = "已拒絕";
$i_QB_Question = "問題";
$i_QB_Status = "狀況";
$i_QB_LangVer = "語言版本";
$i_QB_EngOnly = "只有英文版";
$i_QB_ChiOnly = "只有中文版";
$i_QB_BothLang = "中英文版";
$i_QB_LastModified = "最後修改日期";
$i_QB_QuestionCode = "問題編號";
$i_QB_DateSubmission = "遞交日期";
$i_QB_Owner = "擁有人";
$i_QB_EngVer = "英文版";
$i_QB_ChiVer = "中文版";
$i_QB_Answer = "答案";
$i_QB_MC = "多項選擇題";
$i_QB_ShortQ = "短題目";
$i_QB_FillIn = "填充";
$i_QB_Matching = "配對";
$i_QB_TandF = "是非題";
$i_QB_ImportNotes = "Description of Import";
$i_QB_QuestionType = "問題類別";
$i_QB_SelectFile = "選擇檔案";
$i_QB_MyQuestion = "我的題目";
$i_QB_SharingArea = "分享地帶";
$i_QB_FileList = "檔案清單";
$i_QB_DownloadEnglish = "下載英文版";
$i_QB_DownloadChinese = "下載中文版";
$i_QB_All = "全部";
$i_QB_ToBulletin = "[Bulletin]";
$i_QB_EditNotes = "編輯後請按儲存";
$i_QB_SubjectAdminLink = "問題庫";
$i_QB_LangSelect = "語言";
$i_QB_LangSelectEnglish = "英文";
$i_QB_LangSelectChinese = "中文";
$i_QB_LangSelectBoth = "雙語";
$i_QB_QuestionAdmin = "Admin";
$i_QB_Alert_Authorize = "Are you sure to authorize the checked questions?\\n(The questions will be shown in Sharing Area)";
$i_QB_Alert_Forbid = "Are you sure to forbid the checked questions?\\n(The questions will not be shown in Sharing Area)";
$i_QB_Alert_Reject = "Are you sure to reject the checked questions?";
$i_QB_Prompt_Reject = "請輸入拒絕接納之原因, 或取消拒絕.";
$i_QB_ViewOriginalEnglish = "檢視原檔案 [英文版]";
$i_QB_ViewOriginalChinese = "檢視原檔案 [中文版]";
$i_QB_MarkAllRead = ExportIcon()."標示全部為已閱 ";
$i_QB_MarkAllReadDownload = ExportIcon()."標示全部為已閱及下載";
$i_QB_ConfirmAllRead = "你確定要標示所有題目為已閱?";
$i_QB_ConfirmAllDownload = "你確定要標示所有題目為已閱及下載?";
$i_QB_ConfirmAddToBucket = "你確定要把已選之題目加進 pack?";
$i_QB_ConfirmAddAllToBucket = "你確定要把所有題目加進 pack?";
$i_QB_ConfirmClearBucket = "你確定要清除 pack 中所有題目?";
$i_QB_AddToBucket = "加進 pack";
$i_QB_AddAllToBucket = "所有題加進 pack";
$i_QB_ClearBucket = "清除 pack";
$i_QB_QuestionBucket1 = "已加進 pack 中的題目:";
$i_QB_QuestionBucket2 = " 條題目在pack 中, 你可以:";
$i_QB_DownloadAsWord = "下載 Microsoft Word 格式, 並以 zip 儲存.";
$i_QB_DownloadToEclass = "下載到以下其中一個eClass 課室:";
$i_QB_SameServer = "本伺服器";
$i_QB_AnswerOption = "答案選擇";
$i_QB_ChangeGroupReset = "轉換科目會清除已加進 pack 之題目. 是否確定繼續?";
$i_QB_Settings = "設定";
$i_QB_NoGroupUsing = "目前沒有小組使用題目庫";
$i_QB_AwardPts = "題目價值";
$i_QB_Pts = "分";
$i_QB_DownloadedCount = "已被下載次數";
$i_QB_Pts_Available = "現有點數";
$i_QB_Pts_InBucket = "在pack 的點數";
$i_QB_Pts_LeftAfterDownload = "下載後剩餘點數";
$i_QB_Pts_NotEnough = "你沒有足夠的點數下載題目";
$i_QB_SelectRemovalTransfer1 = "請選擇";
$i_QB_SelectRemovalTransfer2 = ", 系統會把原來題目轉至此.";
$i_QB_TypeRemovalDelete = "直接移除題目";
$i_QB_AllQuestions = "所有題目";
$i_QB_ViewAll = "檢視所有題目";

$i_CustomerSupport = "用戶支援";

$i_Profile_Student_Profile = "學生紀錄";
$i_Profile_settings = "學生檔案設定";
$i_Profile_settings_display = "顯示設定";
$i_Profile_settings_acl = "教師存取設定";
$i_Profile_settings_merit_type_select = "選擇使用的獎懲項目";
$i_Profile_settings_attend_stat_method = "考勤紀錄 - 缺席之統計方法設定";
$i_Profile_settings_select_fields_to_disable = "請選擇系統不使用的種類";
$i_Profile_settings_AttednanceReason = "考勤紀錄 - 可被班主任編輯";
$i_Profile_settings_AttednanceReasonNotEditable = "不可編輯";
$i_Profile_settings_AttednanceReasonEditable = "可編輯";
# added on 9 Sept
$i_Profile_settings_merit_type_edit = "編輯獎懲項目名稱";

$i_Profile_Attendance = "考勤紀錄";
$i_Profile_Merit = "獎懲紀錄";
$i_Profile_Service = "服務紀錄";
$i_Profile_Activity = "活動紀錄";
$i_Profile_Award = "得獎紀錄";
$i_Profile_Assessment = "學生評估";
$i_Profile_Files = "參考檔案";
$i_Profile_Summary = "總結";
$i_Profile_Summary_Has = "共 有";
$i_Profile_Summary_Conversion = "換算";
$i_Profile_Summary_Conversion_To = "進";
$i_Profile_AdminUserMgmt = "學生檔案";
$i_Profile_AdminLevel_Description_Normal = "只可以新增紀錄及更改自己輸入的紀錄";
$i_Profile_AdminLevel_Description_Full = "可以更改所有紀錄";
$i_Profile_AdminACL = "可存取紀錄";
$i_Profile_Admin_NotAllowed = "不允許 ";

$i_Profile_Attendance_Analysis = "考勤分析";
$i_Profile_Merit_Analysis = "獎懲分析";

$i_Profile_SelectUser = "請選擇一位學生";

$i_Profile_Absent = "缺席";
$i_Profile_Late = "遲到";
$i_Profile_EarlyLeave = "早退";
$i_Profile_Days = "日";

$i_Profile_From = "由";
$i_Profile_To = "到";
$i_Profile_Today = "今天";
$i_Profile_ThisWeek = "本星期";
$i_Profile_ThisMonth = "本月";
$i_Profile_ThisAcademicYear = "本年度";

$i_Profile_SelectStudent = "選擇學生";
$i_Profile_NotClassTeacher = "非班主任教師不能檢視學生資料.";
$i_Profile_NoFamilyRelation = "尚未建立子女連結.";
$i_Profile_OverallStudentView = "檢視現時學生紀錄";
$i_Profile_DataLeftStudent = "檢視已離校學生的資料";
$i_Profile_OverallStudentViewLeft = "檢視暫存已離校學生紀錄";
$i_Profile_DataRemovedStudent = "已被刪除學生的資料";
$i_Profile_SelectClass = "選擇班級:";

$i_Profile_Year = "年度";
$i_Profile_Semester = "學期";
$i_Profile_SelectSemester = "選擇日期";
$i_Profile_SelectReason = "選擇原因";
$i_Profile_ByReason = "以原因檢視";
$i_Profile_StudentRecord = "學生紀錄";
$i_Profile_DataLeftYear = "離校年度";
$i_Profile_ClassOfFinalYear = "離校班別";

$i_Profile_StudentNotFound = "找不到這名學生";

$i_Profile_StudentName = "學生名稱";

$i_Profile_ConfirmRemoveAllArchiveRecord = "你是否確定刪除所有學生的資料？";
$i_Profile_ConfirmRemoveLink = "按此刪除所有資料";
$i_Profile_SucceedRemoveMsg = "已成功刪除所有學生的資料";

$i_Profile_SearchMethod = "尋找方法";
$i_Profile_ClassHistory = "班級紀錄";

$i_Profile_Hide_Frontend = "不顯示在 eClass 校園網";
$i_Profile_Hide_PrintPage = "不顯示在列印格式";
$i_Profile_PersonInCharge = "負責人";

$i_Profile_RecordDate = "紀錄日期";
$i_Profile_SearchByDate = "搜尋選項";
$i_Profile_SearchByPeriod = "以時段搜尋";
$i_Profile_DateType = "日期類別";
$i_Profile_NotSearchByDate = "不以日期搜尋";
$i_Profile_ByRecordDate = "以紀錄日期";
$i_Profile_ByLastModified = "以最後修改日期";
$i_Profile_SearchText = "搜尋項目";

$i_Profile_AttendanceStatistic_Method1 = "$i_DayTypeWholeDay/$i_DayTypeAM/$i_DayTypePM 皆計算 1 次";
$i_Profile_AttendanceStatistic_Method2 = "$i_DayTypeWholeDay 計 1 次, $i_DayTypeAM/$i_DayTypePM 計 0.5 次";
$i_Profile_AttendanceStatistic_Description = "這個選項會影響所有計算的部份 (班別之";

$i_Profile_Import_NoMatch_Entry[2]="班別及班號錯誤(請用英文填寫)";
$i_Profile_Import_NoMatch_Entry[3]="內聯網帳號錯誤";
$i_Profile_Import_NoMatch_Entry[1]="部份欄不正確";
$i_Profile_Import_NoMatch_Entry[4]="部份欄位沒有填上";
$i_Profile_Import_NoMatch_Entry[5]="學校年份或學期不存在(請用英文填寫)";

$i_AttendanceTypeArray = array("",$i_Profile_Absent, $i_Profile_Late,$i_Profile_EarlyLeave);
$i_Attendance="考勤";
$i_Attendance_Others="其它";
$i_Attendance_Standard="標準";

$i_Attendance_Remark ="備註";
$i_Attendance_Num_Of_Day = "星期";
$i_Attendance_Date = "日期";
$i_Attendance_Type = "類別";
$i_Attendance_attendance_type="考勤類別";
$i_Attendance_Year = "年度";
$i_Attendance_AllYear = "全部年度";
$i_Attendance_Semester = "學期";
$i_Attendance_DayType = "時段";
$i_Attendance_Modified = "最後修改時間";
$i_Attendance_Reason = "原因";
$i_Attendance_ImportSelect = "選擇檔案";
$i_Attendance_DownloadSample = "按此下載範例";

$i_Attendance_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設).<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設).<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四欄 : 學生班別 (注意大小寫).<br>
第五欄 : 學生班號<br>
第六欄 : 類別 (A - 缺席, L - 遲到, E - 早退)<br>
第七欄 : 時段 (WD - 全日, AM - 上午, PM - 下午).<br>
第八欄 : 原因 (可選擇留空).
";

$i_Attendance_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設).<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設).<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四欄 : WebSAMS 註冊號碼 (帳號由 # 開始 如  #123456).<br>
第五欄 : 類別 (A - 缺席, L - 遲到, E - 早退)<br>
第六欄 : 時段 (WD - 全日, AM - 上午, PM - 下午).<br>
第七欄 : 原因 (可選擇留空).
";

$i_Attendance_DetailedAttendanceRecord = "詳細考勤紀錄";

/*----- Added On 18 DEC 2007 */
$i_StaffAttendance_Sort = "排序";
$i_StaffAttendance_Sort_Type = "排序方向";
$i_StaffAttendance_Sort_Name = "職員名稱";
$i_StaffAttendance_Sort_Status = "狀態";
$i_StaffAttendance_Sort_Time = "時間";
$i_StaffAttendance_Sort_Type_Asc = "向上";
$i_StaffAttendance_Sort_Type_Desc = "向下";
/*---------------------------*/

/*----- Added On 21 DEC 2007 */
$i_StaffAttendance_Field_Date_From = "由";
$i_StaffAttendance_Field_Date_To = "到";
/*---------------------------*/

/*----- Added On 24 DEC 2007 */
$i_StaffAttendance_Total_Day_Of_Abs = "總日數";
/*---------------------------*/

/*----- Added On 27 DEC 2007 */
$i_StaffAttendance_Report_Staff_Waived_Flag = "包括豁免記錄";
/*---------------------------*/

/*----- Added On 29 Feb 2008 */
$i_StaffAttendance_Report_Staff_Outgoing_Monthy = "職員出勤記錄 (每月)";
/*---------------------------*/

/*----- Added On 03 Mar 2008 */
$i_StaffAttendance_Report_Date_Range = "日期範圍";
$i_StaffAttendance_Report_Days_Abs = "日數";
/*---------------------------*/

/*----- Added On 14 Mar 2008 */
$i_StaffAttendance_ReasonType_Message = "以下原因類型將作為「教職員請假缺席統計」報告之欄位名稱，若<br><br>

																				 1.有關原因類型已停用，及<br>
																				 2.有關原因類型於缺席統計之統計日期範圍期間未曾選用<br><br>

																				 則該原因類型將不予顯示於該統計報告中。
																				 ";
/*---------------------------*/

/*----- Added On 27 Mar 2008 */
$i_StaffAttendance_SelfAttendance = "個人考勤記錄";
$i_StaffAttendance_Month_Selector_SelectMonth = "選擇月份";
/*---------------------------*/

$i_Activity_ViewStudent = "檢視學生紀錄";
$i_Activity_ArchiveRecord = "匯入本年度活動紀錄 (應只在學期或學年完結時執行)";
$i_Activity_Count = "數量";
$i_ActivityYear = "年度";
$i_ActivitySemester = "學期";
$i_ActivityName = "活動名稱";
$i_ActivityRole = "職位";
$i_ActivityOrganization = "舉辦機構";
$i_ActivityPerformance = "表現";
$i_ActivityLastModified = "最後更新時間";
$i_ActivityRemark = "備註";
$i_ActivityNoRecord = "暫時沒有任何課外活動紀錄.";
$i_ActivityArchive = "保存紀錄";
$i_ActivityArchiveDescription = "將學生活動資料從課外活動小組匯入至活動紀錄，匯入的資料包括活動名稱、職位和表現，並以下列年度及學期作為紀錄:";
$i_ActivityArchiveWrongData = "如以上資料有任何錯誤，請到內聯網設定 > 系統設定 >基本設定，更改年度及學期的資料.";
$i_ActivityArchiveProceed = "按此進行匯入程序";

$i_Activity_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設).<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設).<br>
第三欄 : 學生班別 (注意大小寫).<br>
第四欄 : 學生班號<br>
第五欄 : $i_ActivityName.<br>
第六欄 : $i_ActivityRole (可選擇留空).<br>
第七欄 : $i_ActivityPerformance (可選擇留空).<br>
第八欄 : $i_ActivityRemark (可選擇留空).<br>
第九欄 : $i_ActivityOrganization (可選擇留空).
";

$i_Activity_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設).<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設).<br>
第三欄 : WebSAMS 註冊號碼 (帳號由 # 開始 如  #123456).<br>
第四欄 : $i_ActivityName.<br>
第五欄 : $i_ActivityRole (可選擇留空).<br>
第六欄 : $i_ActivityPerformance (可選擇留空).<br>
第七欄 : $i_ActivityRemark (可選擇留空).<br>
第八欄 : $i_ActivityOrganization (可選擇留空).
";

$i_Merit_Date = "獎懲日期";
$i_Merit_Type = "類型";
$i_Merit_Qty = "數量";
$i_Merit_Reason = "原因";
$i_Merit_Remark = "備註";
$i_Merit_DateModified = "最後更新日期";
$i_Merit_Award = "獎勵";
$i_Merit_Punishment = "懲罰";
$i_Merit_NoAwardPunishment = "不適用";
$i_Merit_Merit = "優點";
$i_Merit_MinorCredit = "小功 ";
$i_Merit_MajorCredit = "大功 ";
$i_Merit_SuperCredit = "超功 ";
$i_Merit_UltraCredit = "極功 ";
$i_Merit_Warning = "警告";
$i_Merit_BlackMark = "缺點 ";
$i_Merit_MinorDemerit = "小過 ";
$i_Merit_MajorDemerit = "大過 ";
$i_Merit_SuperDemerit = "超過 ";
$i_Merit_UltraDemerit = "極過 ";
$i_Merit_MinorCredit_unicode = "小&#21151; ";
$i_Merit_MajorCredit_unicode = "大&#21151; ";
$i_Merit_SuperCredit_unicode = "超&#21151; ";
$i_Merit_UltraCredit_unicode = "極&#21151; ";

$i_Merit_TypeArray = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_TypeArrayWithWarning = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_Warning,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_Short_Merit = "優點";
$i_Merit_Short_MinorCredit = "小功 ";
$i_Merit_Short_MajorCredit = "大功 ";
$i_Merit_Short_SuperCredit = "超功 ";
$i_Merit_Short_UltraCredit = "極功 ";
$i_Merit_Short_BlackMark = "缺點";
$i_Merit_Short_MinorDemerit = "小過";
$i_Merit_Short_MajorDemerit = "大過";
$i_Merit_Short_SuperDemerit = "超過";
$i_Merit_Short_UltraDemerit = "極過";
$i_Merit_Short_TypeArray = array(
$i_Merit_Short_Merit,
$i_Merit_Short_MinorCredit,
$i_Merit_Short_MajorCredit,
$i_Merit_Short_SuperCredit,
$i_Merit_Short_UltraCredit,
$i_Merit_Short_BlackMark,
$i_Merit_Short_MinorDemerit,
$i_Merit_Short_MajorDemerit,
$i_Merit_Short_SuperDemerit,
$i_Merit_Short_UltraDemerit
);

$i_Merit_QtyMustBeInteger = "$i_Merit_Qty 必須為正整數";
$i_Merit_Unit = "個";
$i_Merit_CountByReason = "以原因分類";
$i_Merit_Count = "次數";
$i_Merit_DetailedMeritRecord = "詳細獎懲紀錄";

$i_Merit_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設)<span style=\"color:red;\">*請用英文填寫</span.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四欄 : 學生班別 (注意大小寫)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第五欄 : 學生班號<br>
第六欄 : 類別 (C1 - $i_Merit_Merit, C2 - $i_Merit_MinorCredit, C3 - $i_Merit_MajorCredit,
C4 - $i_Merit_SuperCredit, C5 - $i_Merit_UltraCredit,
D1 - $i_Merit_BlackMark, D2 - $i_Merit_MinorDemerit, D3 - $i_Merit_MajorDemerit, D4 - $i_Merit_SuperDemerit,
D5 - $i_Merit_UltraDemerit)<br>
第七欄 : $i_Merit_Qty.<br>
第八欄 : $i_Merit_Reason (可選擇留空).<br>
第九欄 : $i_Merit_Remark (可選擇留空).<br>
第十欄 : 負責人的登入帳號 (可選擇留空).
";

$i_Merit_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四欄 : WebSAMS 註冊號碼 (帳號由 # 開始 如  #123456).<br>
第五欄 : 類別 (C1 - $i_Merit_Merit, C2 - $i_Merit_MinorCredit, C3 - $i_Merit_MajorCredit,
C4 - $i_Merit_SuperCredit, C5 - $i_Merit_UltraCredit,
D1 - $i_Merit_BlackMark, D2 - $i_Merit_MinorDemerit, D3 - $i_Merit_MajorDemerit, D4 - $i_Merit_SuperDemerit,
D5 - $i_Merit_UltraDemerit)<br>
第六欄 : $i_Merit_Qty.<br>
第七欄 : $i_Merit_Reason (可選擇留空).<br>
第八欄 : $i_Merit_Remark (可選擇留空).<br>
第九欄 : 負責人的登入帳號 (可選擇留空).
";

$i_Service_Count = "數量";
$i_ServiceYear = "年度";
$i_ServiceSemester = "學期";
$i_ServiceDate = "日期";
$i_ServiceName = "服務名稱";
$i_ServiceRole = "職位";
$i_ServiceOrganization = "服務機構";
$i_ServicePerformance = "表現";
$i_ServiceLastModified = "最後更新時間";
$i_ServiceRemark = "備註";
$i_ServiceNoRecord = "暫時沒有任何服務紀錄.";
$i_ServiceDateCanBeIgnored = "(如此紀錄為一學期之服務,可留空此格)";

$i_Service_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設).<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設).<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : 學生班別 (注意大小寫).<br>
第五欄 : 學生班號<br>
第六欄 : $i_ServiceName<br>
第七欄 : $i_ServiceRole (可選擇留空).<br>
第八欄 : $i_ServicePerformance (可選擇留空).<br>
第九欄 : $i_ServiceRemark (可選擇留空).<br>
第十欄 : $i_ServiceOrganization (可選擇留空).
";

$i_Service_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設).<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設).<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : WebSAMS 註冊號碼 (帳號由 # 開始 如  #123456).<br>
第五欄 : $i_ServiceName<br>
第六欄 : $i_ServiceRole (可選擇留空).<br>
第七欄 : $i_ServicePerformance (可選擇留空).<br>
第八欄 : $i_ServiceRemark (可選擇留空).<br>
第九欄 : $i_ServiceOrganization (可選擇留空).
";

$i_Award_Count = "數量";
$i_AwardYear = "年度";
$i_AwardSemester = "學期";
$i_AwardDate = "日期";
$i_AwardName = "獎項";
$i_AwardRole = "職位";
$i_AwardOrganization = "頒發機構";
$i_AwardSubjectArea = "項目名稱";
$i_AwardPerformance = "表現";
$i_AwardLastModified = "最後更新時間";
$i_AwardRemark = "備註";
$i_AwardNoRecord = "暫時沒有任何獎項紀錄.";
$i_AwardDateCanBeIgnored = "(如此紀錄為一學期之服務,可留空此格)";

$i_Award_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設).<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設).<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : 學生班別 (注意大小寫).<br>
第五欄 : 學生班號<br>
第六欄 : $i_AwardName<br>
第七欄 : $i_AwardRemark (可選擇留空).<br>
第八欄 : $i_AwardOrganization (可選擇留空).<br>
第九欄 : $i_AwardSubjectArea (可選擇留空).
";

$i_Award_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本年度. 可在基本設定預設).<br>
第二欄 : 紀錄學期 (如留空即為本學期. 可在基本設定預設).<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : WebSAMS 註冊號碼 (帳號由 # 開始 如  #123456).<br>
第五欄 : $i_AwardName<br>
第六欄 : $i_AwardRemark (可選擇留空).<br>
第七欄 : $i_AwardOrganization (可選擇留空).<br>
第八欄 : $i_AwardSubjectArea (可選擇留空).
";

$i_MyAccount_StudentID = "學生證";
$i_MyAccount_SchoolRecord = "學校紀錄";
$i_MyAccount_Library = "圖書紀錄";
$i_MyAccount_Usage = "用戶登入紀錄";
$i_MyAccount_CampusMail = "校園信箱";
$i_MyAccount_WebMail = "網上電郵";
$i_MyAccount_FileCabinet = "個人文件櫃";

$i_Teaching_TeacherName = "教師姓名";
$i_Teaching_ClassTeacher = "班主任";
$i_Teaching_SubjectTeacher = "科目老師";
$i_Teaching_SubjectTaught = "任教科目數目";
$i_Teaching_NotClassTeacher = "不是班主任";
$i_Teaching_TeachingArrangement = "教職調配";
$i_Teaching_ClassSubjects = "任教科目/班別";
$i_Teaching_NotApplicable = "不適用";
$i_Teaching_Class = "班別";
$i_Teaching_Subject = "科目";
$i_Teaching_ImportInstruction = "<b><u>檔案說明:</u></b><br>
第一欄 : 教師內聯網帳號 (如 tmchan ).<br>
第二欄 : 班主任的班別 (如 1A. 如非班主任, 請留空此欄).<br>
第三欄 : 任教的班別 (如 1A)<br>
第四欄 : 任教的科目 (如 English, 與第三欄為一對) <br>
第五欄及以後的欄位 : 任教的班別及科目, 必須以一對出現<br>
班別及科目名稱必須完全正確 (分大小寫)<br><br>
<b>注意現時設定將會被取代.</b>";
$i_Teaching_ClassList = "班別列表";
$i_Teaching_SubjectList = "科目列表";
$i_Teaching_ImportFailed = "以下的紀錄不能被匯入";

$i_Form_Name = "表格名稱";
$i_Form_Description = "備註";
$i_Form_Type = "種類";
$i_Form_LastModified = "最後更新時間";
$i_Form_Templates = "範本";
$i_Form_Form = "表格";
$i_Form_Suspended = "停用";
$i_Form_Approved = "可用";
$i_Form_Assessment = "學生評估";
$i_Form_Activity = "活動報名";
$i_Form_ConstructForm = "製作表格";
$i_Form_AssessmentDone = "已完成評估表";
$i_Form_FormEditNotAllow = "此表格已被使用, 所以不能更改表格內容. 更改表格內容會引致資料錯誤. 你可以選擇更改表格屬性或停用此表格, 然後新增另一張表格代替(不能同名).";
$i_Form_FormNameMustBeUnique = "(必須是唯一)";
$i_Form_RemovalAlert = "刪除這些表格會同時刪除使用這些表格的資料, 你是否確定要刪除? <BR> (如果你希望保留資料, 可以停用這些表格.)";
$i_Form_answer_sheet="網上表格製作";
$i_Form_answersheet_template="範本";
$i_Form_answersheet_header="題目 / 標題";
$i_Form_answersheet_no="題數";
$i_Form_answersheet_type="填寫方式";
$i_Form_answersheet_maxno="問題數目已超越已選擇指數極限: 英文字母為 'a - z';  羅馬數字為 'i - x'!";
$i_Form_no_options_for = "不用為此填寫方式選擇數目!";
$i_Form_pls_specify_type = "請選定填寫方式!";
$i_Form_pls_fill_in = "請輸入內容!";
$i_Form_chg_title = "更改題目/標題:";
$i_Form_chg_template = "確定變換應用範本?";
$i_Form_answersheet_tf="是非項";
$i_Form_answersheet_mc="多項選擇";
$i_Form_answersheet_mo="可選多項";
$i_Form_answersheet_sq1="填寫(短)";
$i_Form_answersheet_sq2="填寫(長)";
$i_Form_answersheet_option="選項數目";
$i_Form_answersheet_not_applicable="不用填寫";
$i_Form_ShowHide = "顯示／隱藏";
$i_Form_ShowDetails = "顯示詳情";
$i_Form_HideDetails = "隱藏詳情";
$i_Form_TotalNo = "總共人數";

$i_Assessment_Year = "年份";
$i_Assessment_Semester = "學期";
$i_Assessment_Date = "評估日期";
$i_Assessment_By = "評估者";
$i_Assessment_LastModified = "最後更新日期";
$i_Assessment_SelectForm = "選擇表格";
$i_Assessment_PleaseSelectAForm = "請選擇一張表格";
$i_Assessment_FillForm = "填寫表格";
$i_Assessment_PleaseFillAForm = "請填上相關資料及按[顯示表格]顯示評估表";
$i_Assessment_Open = "顯示表格";
$i_Assessment_UserSelectAdmin = "如不選擇教職員, 評估者將會顯示為 $i_general_sysadmin.";
$i_Assessment_OriginalAssessBy = "原來評估者";

$i_ReferenceFiles_Qty = "檔案數目";
$i_ReferenceFiles_Title = "標題";
$i_ReferenceFiles_FileType = "檔案類型";
$i_ReferenceFiles_NewFile = "新增檔案";
$i_ReferenceFiles_SelectFile = "選擇檔案";
$i_ReferenceFiles_Description = "檔案說明";
$i_ReferenceFiles_FileName = "檔案名稱";

$i_PrinterFriendlyPage = "可列印格式";
$i_PrinterFriendly_StudentName = "學生姓名";

$i_UsageStartTime = "登入時間";
$i_UsageEndTime = "登出時間";
$i_UsageDuration = "逗留時間";
$i_UsageHost = "用戶地址";
$i_UsageToday = "今天";
$i_UsageLastWeek = "最近一星期";
$i_UsageLast2Week = "最近兩星期";
$i_UsageLastMonth = "最近一個月";
$i_UsageAll = "全部";
$i_Usage_Hour = "小時";
$i_Usage_Min = "分";
$i_Usage_Sec = "秒";
$i_Usage_RemoveRecords = "刪除紀錄";
$i_Usage_Remove1 = "刪除在 ";
$i_Usage_Remove2 = " 及以前的紀錄.";
$i_Usage_RemoveConfirm = "確定要刪除紀錄?";

$i_ClubsEnrollment = "學會報名";
$i_ClubsEnrollment_BasicSettings = "基本設定";
$i_ClubsEnrollment_StudentRequirement = "學生要求設定";
$i_ClubsEnrollment_QuotaSettings = "小組使用及限額設定";
$i_ClubsEnrollment_Mode = "模式";
$i_ClubsEnrollment_AppStart = "報名開始日期";
$i_ClubsEnrollment_AppEnd = "報名結束日期";
$i_ClubsEnrollment_GpStart = "小組批核開始日期";
$i_ClubsEnrollment_GpEnd = "小組批核結束日期";
$i_ClubsEnrollment_ApplicationDescription = "申請規則";
$i_ClubsEnrollment_DefaultMin = "預設學生最少須選";
$i_ClubsEnrollment_DefaultMax = "預設學生最多可選";
$i_ClubsEnrollment_TieBreakRule = "申請過多時的優先考慮次序";
$i_ClubsEnrollment_ClearRecord = "清除所有報名紀錄(用於新學年開始報名時)";
$i_ClubsEnrollment_Disable = "不使用";
$i_ClubsEnrollment_Simple = "簡單";
$i_ClubsEnrollment_Advanced = "進階";
$i_ClubsEnrollment_NoLimit = "無限制";
$i_ClubsEnrollment_Random = "隨機";
$i_ClubsEnrollment_AppTime = "先到先得";
$i_ClubsEnrollment_DefaultMaxMinWrong = "預設最少數目不能大於預設最多數目";
$i_ClubsEnrollment_EnrollMaxWrong = "最多可參加數目不能大於預設最多數目和不能小於預設最少數目";
$i_ClubsEnrollment_AppEndGpStart = "報名時段和小組批核時段不能重疊。";
$i_ClubsEnrollment_AppStartAppEnd = "報名時段不正確。";
$i_ClubsEnrollment_GpStartGpEnd = "小組批核時段不正確。";
$i_ClubsEnrollment_UseDefault = "使用預設值?";
$i_ClubsEnrollment_Min = "最少";
$i_ClubsEnrollment_Max = "最多";
$i_ClubsEnrollment_GroupName = "學會名稱";
$i_ClubsEnrollment_AllowEnrol = "使用報名系統?";
$i_ClubsEnrollment_Priority = "優先";
# added on 10 Sept 2008
$i_ClubsEnrollment_OnlineRegistration = "網上登記";
$i_ClubsEnrollment_ClubName = "學會名稱";

$i_ClubsEnrollment_Quota = "組員限額";
$i_ClubsEnrollment_ApplicationPeriod = "報名期";
$i_ClubsEnrollment_From = "由";
$i_ClubsEnrollment_To = "至";
$i_ClubsEnrollment_MinForStudent = "最少須要選擇學會數目";
$i_ClubsEnrollment_MaxForOwnWill = "你希望最多參加的學會數目";
$i_ClubsEnrollment_Priority = "次序";
$i_ClubsEnrollment_ClubsForSelection = "可選擇學會";
$i_ClubsEnrollment_Status = "狀況";
$i_ClubsEnrollment_StatusWaiting = "候批";
$i_ClubsEnrollment_StatusRejected = "已拒絕";
$i_ClubsEnrollment_StatusApproved = "已批核";
$i_ClubsEnrollment_Alert_NotEnough = "你並未選擇足夠的學會";
$i_ClubsEnrollment_Alert_Reject = "你確定要拒絕申請?";
$i_ClubsEnrollment_LastSubmissionTime = "最後提交日期";
$i_ClubsEnrollment_EnrollmentList = "報名名單";
$i_ClubsEnrollment_NotSubmitted = "尚未報名";
$i_ClubsEnrollment_GroupQuota1 = "此學會人數上限為";
$i_ClubsEnrollment_ActivityQuota1 = "此活動人數上限為";
$i_ClubsEnrollment_GroupQuotaNo = "此學會沒有人數上限";
$i_ClubsEnrollment_GroupApprovedCount = "已經批准加入的人數";
$i_ClubsEnrollment_SpaceLeft = "餘額";
$i_ClubsEnrollment_NoQuota = "無限額";
$i_ClubsEnrollment_NumOfStudent = "學生人數";
$i_ClubsEnrollment_NumOfReceived = "已提交報名表的學生";
$i_ClubsEnrollment_NumOfNotFulfilSchool = "未符合學校要求的學生";
$i_ClubsEnrollment_NumOfNotFulfilOwn = "符合學校要求, 但未符合個人要求的學生";
$i_ClubsEnrollment_NumOfSatisfied = "符合學校要求及個人要求的學生";
$i_ClubsEnrollment_NotHandinList = "欠交的學生名單";
$i_ClubsEnrollment_HeadingNotHandinList = "以下學生並未提交學會報名表";
$i_ClubsEnrollment_HeadingNotFulfilSchool = "以下學生並未符合學校要求";
$i_ClubsEnrollment_HeadingNotFulfilOwn = "以下學生已符合學校要求, 但並未符合學生個人意願";
$i_ClubsEnrollment_HeadingSatisfied = "以下學生已符合學校及個人要求";
$i_ClubsEnrollment_NameList = "[學生名單]";
$i_ClubsEnrollment_AllMemberList = "各組名單";
$i_ClubsEnrollment_NoMember = "未有已批核組員";
$i_ClubsEnrollment_CurrentSize = "會員人數";
$i_ClubsEnrollment_GoLottery = "進行抽籤";
$i_ClubsEnrollment_Proceed2NextRound = "進行下一輪報名程序";
$i_ClubsEnrollment_FinalConfirm = "確認整個報名程序完成";
$i_ClubsEnrollment_Confirm_Clear2Next = "你確定要進行下一輪? (請確定你已在設定中更改報名日期.)";
$i_ClubsEnrollment_Confirm_GoLottery = "你是否確認進行隨機抽簽？";
$i_ClubsEnrollment_Confirm_FinalConfirm = "你確定要完成整個報名程序?";
$i_ClubsEnrollment_Form_Updated = "你的表格已經更新及遞交";
$i_ClubsEnrollment_LotteryInProcess = "抽籤進行中，請等候片刻，並且不要關閉此視窗。";
$i_ClubsEnrollment_LotteryFinished = "抽籤已完成。請關閉此視窗。";
$i_ClubsEnrollment_Mail1 = "學校的學會報名程序已進入下一輪. 你的報名結果如下:";
$i_ClubsEnrollment_MailChoice = "選擇";
$i_ClubsEnrollment_Mail2 = "如你的部份申請未被批核, 你可選擇其他學會, 而報名期由 ";
$i_ClubsEnrollment_Mail3 = "至 ";
$i_ClubsEnrollment_Mail4 = "請到首頁按課外活動登記檢視結果及輸入此輪的選擇(如需要).";
$i_ClubsEnrollment_MailSubject = "學會報名進入下一輪";
$i_ClubsEnrollment_MailConfirm1 = "學會報名程序已完成. 你的報名結果如下:";
$i_ClubsEnrollment_MailConfirm2 = "你應該可在小組資訊中, 尋找你已加入的學會資訊.";
$i_ClubsEnrollment_MailConfirm3 = "如有問題, 請聯絡系統管理員或負責課外活動的導師.";
$i_ClubsEnrollment_MailConfirmSubject = "學會報名結果";
//$i_ClubsEnrollment_Warning_NotUse = "如果你選取任何級別為不使用, 則該級別已遞交之申請將會被取消";
$i_ClubsEnrollment_Warning_NotUse = "若把某級別設定為「不使用」，則該級別的申請人紀錄會被刪除。";

$i_Survey = "問卷調查";
$i_Survey_Poster = "製作人";
$i_Survey_Progress = "進行中";
$i_Survey_Finished = "已完成";
$i_Survey_PublicInstruction = "如果你期望所有用戶都能看到此問卷調查, 請把小組一欄留空.";
$i_Survey_PleaseFill = "請填寫以下調查問卷, 然後按提交. 問卷一經提交, 將不能更改.";
$i_Survey_Result = "你的問題已經被提交. 以下是你的答案紀錄. 你可以列印本頁, 或關閉本視窗.";
$i_Survey_Overall = "總括調查結果";
$i_Survey_Detail = "檢視詳細結果";
$i_Survey_perGroup = "檢視單一小組結果";
$i_Survey_perIdentity = "檢視單一身份結果(教職員,學生,家長)";
$i_Survey_perClass = "檢視單一班別結果";
$i_Survey_perUser = "檢視單一問卷結果";
$i_Survey_NoSurveyForCriteria = "此選擇並沒有已提交的問卷";
$i_Survey_RemoveConfirm = "刪除這些問卷會同時刪除問卷的答案及統計, 你是否確定要刪除?";
$i_Survey_NewSurvey = "份未填問卷調查";
$i_Survey_NoSurveyAvailable = "目前未有問卷調查";
$i_Survey_SurveyConstruction = "問卷製作";
$i_Survey_FillSurvey = "填寫問卷";
$i_Survey_FilledSurvey = "已填妥問卷";
$i_Survey_ClassList = "選擇班別";
$i_Survey_GroupList = "選擇組別";
$i_Survey_IdentityList = "選擇身份";
$i_Survey_UserList = "選擇用戶";
$i_Survey_PleaseSelectType = "請選擇其中一項";
$i_Survey_UnfillList = "未填問卷名單";
$i_Survey_AllFilled = "沒有未填用戶";
$i_Survey_Anonymous = "不記名";
$i_Survey_Anonymous_Description = "不能瀏覽單一問卷結果及名單";
$i_Survey_AllRequire2Fill = "所有題目必須回答";
$i_Survey_alert_PleaseFillAllAnswer = "必須回答此問卷的所有問題";

$i_Notice_ElectronicNotice2 = "電子通告";
$i_Notice_ElectronicNotice = "電子通告系統";
$i_Notice_ElectronicNoticeSettings = "電子通告設定";
$i_Notice_ElectronicNotice_History = "昔日通告";
$i_Notice_ElectronicNotice_Current = "現時通告";
$i_Notice_ElectronicNotice_All = "所有學校通告";
$i_Notice_Disable = "不使用電子通告";
$i_Notice_Setting_FullControlGroup = "進階管理小組 (可刪除通告及代家長更改回條).";
$i_Notice_Setting_NormalControlGroup = "一般管理小組 (可發通告及檢視回條).";
# 20090306 yat
$i_Notice_Setting_DisciplineGroup = "整批列印操行紀錄系統通告";

$i_Notice_Setting_DisableClassTeacher = "不讓班主任代家長更改回條.";
$i_Notice_Setting_AllHaveRight = "所有教職員可發通告.";
$i_Notice_Setting_DefaultNumDays = "預設簽署期限日數.";
$i_Notice_Setting_AdminGroupOnly = "必須為行政小組";
$i_Notice_Setting_ParentStudentCanViewAll = "讓所有家長及學生檢視所有通告.";
$i_Notice_New = "新增通告";
$i_Notice_Edit = "編輯通告";
$i_Notice_NoticeNumber = "通告編號";
$i_Notice_Title = "通告標題";
$i_Notice_Description = "通告內容";
$i_Notice_DateStart = "發出日期";
$i_Notice_DateEnd = "簽署限期";
$i_Notice_Issuer = "發出人";
$i_Notice_RecipientType = "適用對象";
$i_Notice_RecipientTypeAllStudents = "全校";
$i_Notice_RecipientTypeLevel = "部份級別";
$i_Notice_RecipientTypeClass = "部份班別";
$i_Notice_RecipientTypeIndividual = "相關同學";
$i_Notice_RecipientLevel = "級別";
$i_Notice_RecipientClass = "班別";
$i_Notice_RecipientIndividual = "個別學生/組別";
$i_Notice_ReplyContent = "設定回條內容";
$i_Notice_Attachment = "通告附件";
$i_Notice_FromTemplate = "載入範本";
$i_Notice_NotUseTemplate = "不使用範本";
$i_Notice_Type = "類別";
$i_Notice_StatusPublished = "已派發";
$i_Notice_StatusSuspended = "未派發";
$i_Notice_StatusTemplate = "範本";
$i_Notice_ViewOwnClass = "檢視班別";
$i_Notice_Signed = "已簽";
$i_Notice_Total = "總數";
$i_Notice_ViewResult = "檢視結果";
$i_Notice_ReplySlip = "回條";
$i_Notice_ReplySlip_Signed = "以下為已簽回之回條內容. 你可以列印此頁或關閉視窗.";
$i_Notice_StudentName = "學生姓名";
$i_Notice_OpenSign = "開啟";
$i_Notice_Sign = "簽署";
$i_Notice_SignStatus = "簽署狀況";
$i_Notice_Unsigned = "未簽";
$i_Notice_Signer = "簽署人";
$i_Notice_Editor = "代簽署人:";
$i_Notice_SignerNoColon = "簽署人";
$i_Notice_SignedAt = "簽署時間";
$i_Notice_At = "在";
$i_Notice_SignInstruction = "請填妥以上回條，再按簽署。";
$i_Notice_SignInstruction2 = "簽署後如有修改，請修改後再簽署。";
$i_Notice_NoRecord = "暫時沒有通告";
$i_Notice_MyNotice = "我發出的通告";
$i_Notice_AllNotice = "所有通告";
$i_Notice_RemovalWarning = "注意: 移除此通告會一併移除家長交回的回條. 你確定要繼續?";
$i_Notice_TemplateNotes = "(只有 <font color=green>$i_Notice_Title</font>, <font color=green>$i_Notice_Description</font>, 和 <font color=green>$i_Notice_ReplySlip</font> 會成為範本的內容.)";
$i_Notice_Alert_DateInvalid = "簽署日期必須是或遲於發出日期";
$i_Notice_ResultForEachClass = "各班情況";
$i_Notice_TableViewReply = "表列回條內容";
$i_Notice_ViewStat = "統計資料";
$i_Notice_NoReplyAtThisMoment = "暫時沒有已簽回條";
$i_Notice_NumberOfSigned = "已簽回人數";
$i_Notice_Option = "選項";
$i_Notice_NotForThisClass = "此通告並沒有派發給此班的任何學生";
$i_Notice_NoStudent = "此通告並沒有派發給任何學生.";
$i_Notice_AllStudents = "所有同學";
$i_Notice_Alert_Sign = "你所填寫的回條將被呈送. 確定簽署通告並遞交回條?";
$i_Notice_CurrentList = "現時通告清單";
$i_Notice_ListIncludingSuspend = "包括已派發及未派發";
$i_Notice_SendEmailToParent = "向家長發出電郵通知 (如類別為$i_Notice_StatusPublished)";

# 20090227 - eNotice with Discipline Notice enhancement
$eNotice['SchoolNotice'] = "學校通告";
$eNotice['DisciplineNotice'] = "操行紀錄系統通告";
$eNotice['Period_Start'] = "日期由";
$eNotice['Period_End'] = "至";

#20090611 eDisciplinev12 template setting
$eNotice['ReplySlipType'] = "回條類別";
$eNotice['QuestionBase'] = "問題模式";
$eNotice['ContentBase'] = "便條模式";

$i_LinuxAccountQuotaSetting = "iFolder 儲存量設定";
$i_LinuxAccount_SetDefaultQuota = "設定預設儲存量";
$i_LinuxAccount_SetUserQuota = "設定個別用戶儲存量";
$i_LinuxAccount_Quota_Description = "此儲存量包括: ";
$i_LinuxAccount_Webmail = "網上電郵";
$i_LinuxAccount_Campusmail = "外來電郵暫存 (當用戶檢視收件箱時, 郵件會被下載而不再佔儲存空間.)";
$i_LinuxAccount_PersonalFile = "個人文件櫃";
$i_LinuxAccount_Quota = "儲存量";
$i_LinuxAccount_NotExist = "戶口並不存在, 請再檢查一次. 大小寫須完全符合.";
$i_LinuxAccount_AccountName = "戶口名稱";
$i_LinuxAccount_Alert_NameMissing = "請輸入戶口名稱";
$i_LinuxAccount_Alert_QuotaMissing = "請輸入限額";
$i_LinuxAccount_DisplayQuota = "檢視用戶儲存量";
$i_LinuxAccount_SelectUserType = "選擇用戶類別";
$i_LinuxAccount_SelectGroup = "選擇組別";
$i_LinuxAccount_UsedQuota = "已用儲存量";
$i_LinuxAccount_CurrentQuota = "現時儲存量";
$i_LinuxAccount_SetGroupQuota = "設定全組/身分用戶的儲存量";
$i_LinuxAccount_IncreaseOnly = "只增加不減少";
$i_LinuxAccount_Reset = "重設所有此身份/組員用戶";
$i_LinuxAccount_UserSetNote = "要注意設定限額為 0 即等於沒有限制.";
$i_LinuxAccount_GroupSetNote = "要注意設定限額為 0 即等於沒有限制.<br>如用戶人數較多, 請耐心等候.";
$i_LinuxAccount_NoAccount = "沒有系統戶口";
$i_LinuxAccount_NoLimit = "沒有限制";
$i_LinuxAccount_Webmail_QuotaSetting = "電郵暫存設定";
$i_LinuxAccount_Folder_QuotaSetting = "iFolder 儲存量";
$i_LinuxAccount_PersonalFile_Description = "此設定會修改文件伺服器的儲存量, 請小心使用.";
$i_LinuxAccount_iMail_Description = "此儲存量只用於暫時存放外來的互聯網郵件";
$i_Linux_Description_ImportUser = "系統只會為新 eClass 用戶, 在檔案/郵件伺服器開啟戶口. 現有用戶需要在 <b>功能設定 &gt; $i_LinuxAccount_Folder_QuotaSetting</b> 設定";
$i_LinuxAccount_BatchProcess = "整批處理";
$i_LinuxAccount_NewCreation = "新增系統戶口";
$i_LinuxAccount_prompt_TypeRestricted = "此戶口所屬身份不能使用此項服務.";

$i_Files_ConnectionFailed_Remote = "不能連結檔案伺服器. 請確定密碼是否正確.";
$i_Files_ConnectionFailed_Local = "不能連結檔案伺服器. 請再更改一次密碼.";
$i_Files_ConnectionFailed = "不能連結檔案伺服器. 請再更改一次密碼. 如此錯誤仍然發生, 請聯絡系統管理員.";
$i_Files_CurrentDirectory = "現時位置";
$i_Files_Type = "種類";
$i_Files_Size = "大小";
$i_Files_Date = "最後更新日期";
$i_Files_Name = "檔案名稱";
$i_Files_NewFolder = "新增資料夾";
$i_Files_Upload = "上載";
$i_Files_Files = "檔案";
$i_Files_Move = "移動";
$i_Files_Delete = "移除";
$i_Files_Rename = "重新命名";
$i_Files_ClickToDownload = "按此下載檔案";
$i_Files_ThisIsPublic = "(此資料夾的檔案是能被其他人存取)";
$i_Files_UploadTarget = "上載檔案至 : ";
$i_Files_To = "至";
$i_Files_OpenAccount = "允許使用 iFolder";
$i_Files_LinkFTPAccount = "連結檔案伺服器及 iFolder";
$i_Files_LinkToAero = "允許使用個人文件櫃, 並連接至 AeroDrive.";
$i_Files_UseIFolder = "允許使用 iFolder";
$i_Files_Description_Unlink = "如果沒有選擇, 用戶將不能在 eClass 中使用 iFolder. 但檔案伺服器的戶口仍然保留.";
$i_Files_Description_Link = "如果選擇此項, 系統會在檔案伺服器新增此戶口.";
#$i_Files_AllowUserUseFTPAccount = "允許用戶使用個人文件櫃. (如作業系統(linux) 戶口已開啟)";
$i_Files_DefaultQuotaSameServer = "如果郵件伺服器及檔案伺服器是同一電腦, 請在 iMail 設定中更改. (此處資料會被略過)";
$i_Files_Linked = "已連結 iFolder";
$i_Files_LinkIfolder = "連結 iFolder";
$i_Files_BatchOption_OpenAccount = "新增檔案伺服器戶口及連結 iFolder. 儲存量為: ";
$i_Files_BatchOption_UnlinkAccount = "不連結 iFolder <font color=red>(檔案伺服器上的戶口<b><u>不會</u></b> 被刪除)</font>";
$i_Files_BatchOption_RemoveAccount = "刪除檔案伺服器上的戶口 <font color=red>(如郵件伺服器為同一電腦, 則 iMail 亦會受影響)</font>";
$i_Files_alert_RemoveAccount = "你確定要移除這個檔案伺服器的戶口? (如果郵件伺服器是同一電腦, 刪除會一併影響郵件接收)";
$i_Files_CheckQuota = "檢視儲存量";
$i_Files_msg_FailedToWrite = "檔案上載失敗. 可能之原因:<br>
1. 超出儲存量. 請移除部份檔案.<br>
2. 其中一些檔案名稱系統不能接受. 請更改部份檔案名稱 (至一般英文或UTF-8 中文字).<br>
如果問題仍然存在, 請聯絡系統管理員.<br>
";
$i_Files_ClickHereToBrowse = "按此返回檔案列表.";


$i_CampusTV_BasicSettings = "基本設定";
$i_CampusTV_LiveBroadcastManagement = "網上直播管理";
$i_CampusTV_ChannelManagement = "頻道管理";
$i_CampusTV_VideoManagement = "影片管理";
$i_CampusTV_BulletinManagement = "討論區管理";
$i_CampusTV_PollingManagement = "投票管理";
$i_CampusTV_AccessByPublic = "開放給公眾 (無須登入)";
$i_CampusTV_DisableBulletin = "不使用討論區";
$i_CampusTV_DisablePolling = "不使用投票";
$i_CampusTV_LiveBroadcast = "網上直播";
$i_CampusTV_LiveURL = "直播連結";
$i_CampusTV_ChannelName = "頻道名稱";
$i_CampusTV_ClipName = "影片名稱";
$i_CampusTV_Recommended = "推介";
$i_CampusTV_RecommendedStatus = "是";
$i_CampusTV_Type_Link = "連結形式";
$i_CampusTV_Type_File = "檔案形式";
$i_CampusTV_MovieURL = "影片連結";
$i_CampusTV_InputType = "輸入方式";
$i_CampusTV_SelectMovieFile = "選擇影片檔案";
$i_CampusTV_MovieProvider = "提供者";
$i_CampusTV_Play = "播放";
$i_CampusTV_Alert_Recommend = "你確定把已選的影片定為推介?";
$i_CampusTV_Alert_CancelRecommend = "你確定把已選的影片由推介中剔除?";
$i_CampusTV_LiveProgrammeList = "節目表";
$i_CampusTV_Time = "時段";
$i_CampusTV_Programme = "節目名稱";
$i_CampusTV_SelectClip = "選擇片段";
$i_CampusTV_SelectChannel = "選擇頻道";
$i_CampusTV_PollingName = "投票名稱";
$i_CampusTV_Reference = "參考";
$i_CampusTV_NumClips = "影片數目";
$i_CampusTV_PollingSelectClips = "選擇影片";
$i_CampusTV_PollingCandidate = "候選";
$i_CampusTV_ViewProgrammer = "收看節目";
$i_CampusTV_RecommendedList = "最新推介節目";
$i_CampusTV_MostChannel = "最受歡迎頻道";
$i_CampusTV_MostClip = "最受歡迎節目";
$i_CampusTV_Bulletin = "節目討論區";
$i_CampusTV_Upload = "節目上載";
$i_CampusTV_Polling = "節目投票區";
$i_CampusTV_UploadSuccessful =  "<font color=#FFFFFF>影片檔案已上載.</font>";
$i_CampusTV_NoPolling = "你現時並沒有投票未投.";
$i_CampusTV_NoVote = "並沒有用戶曾經投票";
$i_CampusTV_ChannelList = "頻道一覽表";
$i_CampusTV_PastPoll = "昔日投票";
$i_CampusTV_CurrentPoll = "今日投票";
$i_CampusTV_SecureNeeded = "需要登入 eClass IP 才能觀看";
$i_CampusTV_URL = "網址";
$i_CampusTV_Guide_Please_Select_Channel="請選擇頻道";
$i_CampusTV_Guide_Please_Select_Movie="請選擇影片";
$i_CampusTV_Alert_No_Movies_Available="沒有影片可供選擇";
$i_CampusTV_Alert_No_Channels_Available="沒有頻道可供選擇";
$i_CampusTV_Live_Portal_Config = "在 Portal 主頁的設定";
$i_CampusTV_Live_Portal_display = "顯示";
$i_CampusTV_Live_Portal_size = "尺寸";
$i_CampusTV_Live_Portal_pixel = "像素";
$i_CampusTV_Live_Portal_auto = "自動播放";
$i_CampusTV_Live_Portal_size_width_warn = "影片的寬度必需是1至320之間！";
$i_CampusTV_Live_Portal_size_height_warn = "影片的高度必需大過 0 ！";


$i_Mail_OpenWebmailAccount = "開設網上電郵戶口";
$i_Mail_LinkWebmail = "連結網上電郵";
$i_Mail_AllowSendReceiveExternalMail = "允許用戶收發外來電郵";
$i_Mail_AllowUserUseWebmail = "允許用戶使用網上電郵. (如此用戶已有電郵/FTP/Linux, 方為有效)";
$i_Mail_AllowSendReceiveExternalMailEdit = "允許用戶收發外來電郵. (如此用戶已有電郵/FTP/Linux, 方為有效)";

$i_Help_Show = "eClass 啟動訊息";
$i_Help_Description = "你可以在此設定啟動訊息，提示所有用戶有關開始使用eClass系統時的注意事項。請輸入啟動訊息的開始日期及結束日期。如任何一欄留空, 即表示該訊息不會在用戶登入時顯示出來。";
$i_Help_DateStart = "開始日期";
$i_Help_DateEnd = "結束日期";

$i_ec_file_assign = "移交文檔擁有權";
$i_ec_file_remove_teacher = "持有檔案的老師";
$i_ec_file_exist_teacher = "在此教室的老師";
$i_ec_file_not_exist_teacher = "不在此教室的老師";
$i_ec_file_target_teacher = "對象老師";
$i_ec_file_assign_to = "檔案移交給";
$i_ec_file_confirm = "檔案移交是不能逆轉的!\\n你是否確定要移交?";
$i_ec_file_confirm2 = "對象老師沒有在此教室任教，必須把這老師加入此教室才可移交文檔擁有權!\\n你是否確定要將這老師加進此教室?";
$i_ec_file_warning = "請最少選擇一位持有檔案的老師";
$i_ec_file_warning2 = "請選擇一位老師來接收文檔擁有權";
$i_ec_file_user_delete = "將被刪除的eClass用戶：";
$i_ec_file_msg_transfer = "刪除老師會使該老師擁有的文檔失去管理人，可移交擁有權給其他老師：";
$i_ec_file_no_transfer = "不移交";
$i_ec_file_user_delete_confirm = "你是否確定要刪除這些eClass用戶?";

$i_OrganizationPage_Settings = "組織頁面設定";
$i_OrganizationPage_NotDisplayMemberList = "不顯示用戶名單.";
$i_OrganizationPage_NotDisplayEmailAddress = "不顯示用戶電郵地址";
$i_OrganizationPage_HideInOrganization = "不顯示於組織頁面";
$i_OrganizationPage_DisplayOption = "在組織頁面的顯示";
$i_OrganizationPage_NotDisplay = "全部不顯示";
$i_OrganizationPage_Unchange = "不變";
$i_OrganizationPage_DisplayAll = "全部顯示";
$i_OrganizationPage_DefaultCat = "預設小組類別";

$i_SpecialRoom = "特別室";
$i_SpecialRoom_ReadingRoom = "閱讀室";
$i_SpecialRoom_ReadingRoom_Name = "閱讀室名稱";
$i_SpecialRoom_ReadingRoom_Description = "描述";
$i_SpecialRoom_ELP = "ELP";
$i_SpecialRoom_iPortfolio = "iPortfolio";

//$i_AdminJob_AdminCenter = "行政中心";

$i_AdminJob_StaffName = "職員姓名";
$i_AdminJob_AdminLevel = "管理權限";
$i_AdminJob_AdminLevel_Normal = "一般權限";
$i_AdminJob_AdminLevel_Full = "完全控制";
$i_AdminJob_AdminLevel_Detail_Normal = "一般權限 (可發出職員通告及觀看自己所發通告的結果)";
$i_AdminJob_AdminLevel_Detail_Full = "完全控制 (可發出職員通告及觀看所有通告的結果)";
//$i_AdminJob_Description_SetAdmin = "請選擇管理人員及選取管理權限.";
//$i_AdminJob_AccessClass = "班別";

$i_AdminJob_Announcement = "宣佈批核";

//$i_AdminJob_Announcement_SelectApproval = "需要批核, 批核者:";
$i_AdminJob_Announcement_NoEdit = "<font color=red>(此項宣佈已被批核. 因此不能作更改.)</font>";
$i_AdminJob_Announcement_NewEdit = "<font color=red>(此項宣佈已被拒絕. 你可以更改紀錄而系統會視為新增的紀錄.)</font>";
$i_AdminJob_Announcement_ApprovalUser = "批核者";
$i_AdminJob_Announcement_Remark = "備註";
$i_AdminJob_Announcement_HandleAnnouncement = "處理宣佈";
$i_AdminJob_Announcement_Handle_Reason = "原因: <br> (給發佈人)";
$i_AdminJob_Announcement_Handle_Remark = "私人<!-- 備註 -->:";
$i_AdminJob_Announcement_ApproveSubject = "宣佈已被批核";
$i_AdminJob_Announcement_RejectSubject = "宣佈已被拒絕";
$i_AdminJob_Announcement_ApproveMessage1 = "你所提交的宣佈申請 (";
$i_AdminJob_Announcement_ApproveMessage2 = ") 已得到批准並已發佈, 即已顯示在有關用戶的首頁宣佈欄內";
$i_AdminJob_Announcement_RejectMessage1 = "你所提交的宣佈申請 (";
$i_AdminJob_Announcement_RejectMessage2 = ") 已被拒絕. ";
//$i_AdminJob_Announcement_Reason = "原因為:";
$i_AdminJob_Announcement_AlertWaiting = "項等待紀錄";
$i_AdminJob_Announcement_Being = "已被";
$i_AdminJob_Announcement_At = "在";
$i_AdminJob_Announcement_Approved = "批准.";
$i_AdminJob_Announcement_Rejected = "拒絕.";
$i_AdminJob_Announcement_Handle = "處理";
$i_AdminJob_Announcement_Action_Approve = "批准";
$i_AdminJob_Announcement_Action_Waiting = "等待";
$i_AdminJob_Announcement_Action_Reject = "拒絕";
$i_AdminJob_Announcement_LastAction = "上次處理時間";
$i_AdminJob_Announcement_NeedApproval = "經批核";
$i_AdminJob_Announcement_NoApproval = "不經批核";
$i_AdminJob_Announcement_RemoveAdmin = "你確定要取消這個管理員?";

$i_CampusMail_Internal_Recipient_Group ="內在收件組別";
$i_CampusMail_External_Recipient= "外在收件人";
$i_CampusMail_External_Recipient_Group="外在收件組別";
$i_CampusMail_New_iMail = "iMail";
$i_CampusMail_New_FolderManagement = "管理郵件夾";
$i_CampusMail_New_Alert_RemoveFolder = "刪除此郵件夾會一併刪除內裏郵件. 你確定要繼續?";
$i_CampusMail_New_FolderName = "郵件夾名稱";
$i_CampusMail_New_Internal_Recipient_Group ="新增內在收件組別";
$i_CampusMail_New_Prompt_FolderExist = "此名稱的郵件夾已存在";
$i_CampusMail_New_ToFolder = "瀏覽郵件夾";
$i_CampusMail_New_AddressBook = "通訊錄";
$i_CampusMail_New_AddressBook_Name = "名稱";
$i_CampusMail_New_AddressBook_Type = "個人/小組";
$i_CampusMail_New_AddressBook_EmailAddress = "電郵地址";
$i_CampusMail_New_AddressBook_Category = "類別";
$i_CampusMail_New_AddressBook_Internal = "Int";
$i_CampusMail_New_AddressBook_External = "Ext";
$i_CampusMail_New_AddressBook_Prompt_Num = "你需要新增多少個紀錄?";
$i_CampusMail_New_AddressBook_Alert_NeedInteger = "請輸入一個正整數.";
$i_CampusMail_New_AddressBook_ByUser = "用戶";
$i_CampusMail_New_AddressBook_ByGroup = "小組";
$i_CampusMail_New_AddressBook_TypeSelect = "選擇";
$i_CampusMail_New_InternalRecipients = "內聯網收件人";
$i_CampusMail_New_ExternalRecipients = "外在收件人";
$i_CampusMail_New_Settings_PersonalSetting ="基本設定";
$i_CampusMail_New_Settings_Signature="署名欄";
$i_CampusMail_New_Settings_EmailForwarding="轉寄郵件";
$i_CampusMail_New_Settings_ReplyEmail ="回覆電郵地址(外在郵件)";
$i_CampusMail_New_Settings_DisplayName ="顯示名稱(外在郵件)";
$i_CampusMail_New_Settings_DaysInSpam ="濫發郵件保留天數";
$i_CampusMail_New_Settings_DaysInTrash ="垃圾箱郵件保留天數";
$i_CampusMail_New_Settings_PleaseFillInYourSignature ="請填上你的署名";
$i_CampusMail_New_Settings_PleaseFillInForwardEmail ="請輸入要轉寄的電郵地址:<font color=red>(每一行輸入一個電郵地址)</font>";
$i_CampusMail_New_Settings_PleaseFillInForwardKeepCopy="保留備份";
$i_CampusMail_New_Settings_Forever="永久保留";
$i_CampusMail_New_Settings_NoticeDefaultDisplayName ="<font color=red>(若不輸入，將會使用系統預設的使用者名稱。)</font>";
$i_CampusMail_New_Settings_NoticeDefaultReplyEmail  ="<font color=red>(若不輸入，將會使用系統預設的回郵地址。)</font>";
$i_CampusMail_New_Settings_DisableEmailForwarding="停用轉寄";
$i_CampusMail_New_Settings_POP3 = "POP3";
$i_CampusMail_New_Settings_DisableCheckEmail = "停止使用 iMail 下載互聯網郵件";
$i_CampusMail_New_Settings_AutoReply = "自動回覆";
$i_CampusMail_New_Settings_EnableAutoReply = "使用自動回覆";
$i_CampusMail_New_Settings_PleaseFillInAutoReply = "請輸入自動回覆之訊息 (只適用於互聯網郵件)";
$i_CampusMail_New_Settings_POP3_guideline = "檢視電郵軟件設定指引";

$i_CampusMail_New_Recipient_Status = "收件人狀況";
$i_CampusMail_Warning_External_Recipient_No_Semicolon ="收件人名稱不能含有分號 ( ; )";
$i_CampusMail_New_No_Subject = "(沒有標題)";
$i_CampusMail_New_No_Sendmail = "你沒有此權限";

$i_CampusMail_New_To = "To";
$i_CampusMail_New_CC = "CC";
$i_CampusMail_New_BCC = "BCC";
$i_CampusMail_New_MailFolder = "郵件夾";
$i_CampusMail_New_Settings = "偏好設定";
$i_CampusMail_New_Quota1 = "已使用";
$i_CampusMail_New_Quota2 = "的儲存空間";
$i_CampusMail_New_MailSource = "郵件來源";
$i_CampusMail_New_MailSource_Internal = "內聯網郵件";
$i_CampusMail_New_MailSource_External = "互聯網郵件";
$i_CampusMail_New_Icon_MailSource = "<img src=\"$image_path/frontpage/imail/icon_mailfrom.gif\" border=0>";
$i_CampusMail_New_Icon_IntMail = "<img src=\"$image_path/frontpage/imail/icon_intmail.gif\" alt=\"$i_CampusMail_New_MailSource_Internal\" border=0>";
$i_CampusMail_New_Icon_ExtMail = "<img src=\"$image_path/frontpage/imail/icon_extmail.gif\" alt=\"$i_CampusMail_New_MailSource_External\" border=0>";
$i_CampusMail_New_Icon_NewMail = "<img src=\"$image_path/frontpage/imail/icon_newmail.gif\" border=0>";
$i_CampusMail_New_Icon_ReadMail = "<img src=\"$image_path/frontpage/imail/icon_mailread_$intranet_session_language.gif\" border=0>";
$i_CampusMail_New_Icon_RepliedMail = "<img src=\"$image_path/frontpage/imail/icon_mailreplied_$intranet_session_language.gif\" border=0>";
$i_CampusMail_New_EmailAddressSeparationNote = "請以 ; 或 , 分隔每個電郵地址";
$i_CampusMail_New_NotificationInternalOnly = "只適用於 $i_CampusMail_New_InternalRecipients";
$i_CampusMail_New_ExternalSentSuccess = "$i_CampusMail_New_MailSource_External 傳送成功.";
$i_CampusMail_New_ExternalSentFailed = "$i_CampusMail_New_MailSource_External 傳送失敗.";
$i_CampusMail_New_MoveTo = "移至";
$i_CampusMail_New_alert_moveto = "你確定要移動已選的郵件?";
$i_CampusMail_New_alert_norecipients = "沒有任何收件人";
$i_CampusMail_New_NumOfMail = "郵件數量";
$i_CampusMail_New_NoFolder = "請按新增以設定新郵件夾.";
$i_CampusMail_New_MailAliasGroup = "自訂收件組別";
$i_CampusMail_New_AliasGroup = "組別名稱";
$i_CampusMail_New_AliasGroupRemark = "備註";
$i_CampusMail_New_NumberOfEntry = "數目";
$i_CampusMail_New_AddressBook_TargetGroup = "目標小組";
$i_CampusMail_New_SelectFromAlias = "由收件組別選取";
$i_CampusMail_New_Reply = "回覆";
$i_CampusMail_New_ReplyAll = "回覆全部";
$i_CampusMail_New_Forward = "轉寄";
$i_CampusMail_New_Wrote = "寫著...";
$i_CampusMail_New_iMail_Settings = "iMail 設定";
$i_CampusMail_New_QuotaSettings = "郵箱儲存量設定";
$i_CampusMail_New_ViewQuota_Identity = "以身份檢視";
$i_CampusMail_New_ViewQuota_Group = "以組別檢視";
$i_CampusMail_New_Quota = "儲存量";
$i_CampusMail_New_BatchUpdate = "整批更新";
$i_CampusMail_New_SingleEdit = "修改單一用戶儲存量";
$i_CampusMail_New_TargetUser = "欲修改的用戶";
$i_CampusMail_New_BatchUpdateTarget = "整批用戶更新";
$i_CampusMail_New_DefaultQuota = "預設儲存量 (以MBytes 為單位)";
$i_CampusMail_New_ExternalAllow = "允許接收/傳送外來互聯網郵件";
$i_CampusMail_New_alert_RemoveAccount = "你確定要移除這個郵件伺服器的戶口? (如果檔案伺服器是同一電腦, 刪除會一併影響)";
$i_CampusMail_New_BatchOption_OpenAccount = "連結 iMail, 允許接收/傳送互聯網郵件. 儲存量為: ";
$i_CampusMail_New_BatchOption_UnlinkAccount = "不連結 iMail <font color=red>(郵件伺服器上的戶口<b><u>不會</u></b> 被刪除)</font>";
$i_CampusMail_New_BatchOption_RemoveAccount = "刪除郵件伺服器上的戶口 <font color=red>(如檔案伺服器為同一電腦, 則 iFolder (如有)亦會受影響)</font>";
$i_CampusMail_New_QuotaNotEnough = "儲存量不足以傳送所有附件";
$i_CampusMail_New_ViewFormat_PlainText = "觀看純文字格式";
$i_CampusMail_New_ViewFormat_HTML = "觀看 HTML 格式";
$i_CampusMail_New_ViewFormat_MessageSource = "觀看郵件原文";
$i_CampusMail_New_PlsWait = "請等候 ...";
$i_CampusMail_New_ConnectingMailServer = "正在連接郵件伺服器 ... ";
$i_CampusMail_New_Receiving1 = "已接收";
$i_CampusMail_New_Receiving2 = "封郵件 (共";
$i_CampusMail_New_Receiving3 = "封)";
$i_CampusMail_New_WarningQuotaNotEnough = "請清理閣下的郵箱以接收餘下的電郵.";
$i_CampusMail_New_BackMailbox = "返回收件箱";
$i_CampusMail_New_CheckMail = "檢查郵件";
$i_CampusMail_New_MailServerNotReachable = "注意: 不能連接郵件伺服器. 請嘗試更改密碼. 如果此訊息仍然顯示, 請聯絡系統管理員.";
$i_CampusMail_New_MailSearch_Description = "請輸入搜尋條件";
$i_CampusMail_New_MailSearch_hasAttachment = "包含附件檔案";
$i_CampusMail_New_MailSearch_SearchFolder = "搜尋郵件夾";
$i_CampusMail_New_MailSearch_SearchResult = "搜尋結果";
$i_CampusMail_New_MailSearch_BackToResult = "返回搜尋結果";
$i_CampusMail_New_Confirm_remove_mail = "你確定要移除這封郵件?";
$i_CampusMail_New_BadWords_Settings = "不當字詞列表";
$i_CampusMail_New_BadWords_Instruction_top = "你可以輸入系統限制的字句. 在此出現的字句在郵件將會被 *** 取代.";
$i_CampusMail_New_BadWords_Instruction_bottom = "請每個字詞輸入一行";
$i_CampusMail_New_ViewMail = "檢視郵件";
$i_CampusMail_New_Inbox = "收件箱";
$i_CampusMail_New_Outbox = "寄件箱";
$i_CampusMail_New_Draft = "草稿箱";
$i_CampusMail_New_Trash = "垃圾箱";
$i_CampusMail_New_Action_Clean = "清理";
$i_CampusMail_New_FolderRename_NewName = "新郵件夾名稱";
$i_CampusMail_New_InternalAlias = "自訂內部組別";
$i_CampusMail_New_ExternalContact = "對外通訊錄";
$i_CampusMail_New_ListAlias = "名單";
$i_CampusMail_New_RemoveBySearch = "特別條件搜尋";
$i_CampusMail_New_RemoveBySearch_SearchPhrase = "搜尋字句";
$i_CampusMail_New_AddToAliasGroup = "加入收件組別";
$i_CampusMail_New_Mail_ExternalContact = "對外連絡人";
$i_CampusMail_New_Add_CC = "副本";
$i_CampusMail_New_Add_BCC = "密件副本";
$i_CampusMail_New_IntranetNameList = "內聯網名單";
$i_CampusMail_New_SearchRecipient = "搜尋";
$i_CampusMail_Encoding_Warning="此郵件之附件名稱含有一些系統無法辨別的字元，收件人可能無法看到正確之附件名稱。 請先儲存附件後再新增至郵件中。";
$i_CampusMail_New_FolderManager = "檔案管理";
$i_CampusMail_New_ComposeMail = "撰寫郵件";
$i_CampusMail_New_Mail_Search = "搜尋";
$i_CampusMail_New_Show_All_Recipient = "顯示詳細資料";
$i_CampusMail_New_Hide_All_Recipient = "隱藏詳細資料";
$i_CampusMail_OutOfQuota_Warning="由於你的收件箱儲存量不足，你需要清除收件箱以接收新的郵件。";
### added by Ronald on 20090211 ##
$i_CampusMail_ClickHereToDownloadAttachment = "或按此下載";
### added by Ronald on 20090401 ##
$i_CampusMail_Admin_DayInTrashSettings = "垃圾/雜件保留期限";
$i_CampusMail_Admin_DayInTrashSettings_Notice = "* 如用戶已設定保留日數，系統將會以該用戶的設定作準。";
### added by Ronald on 20090402 ##
$i_CampusMail_New_Settings_EmailRules = "郵件規則";
### added by Ronald on 20090417 ###
$i_CampusMail_Condition = "條件";
$i_CampusMail_Condition_From = "由";
$i_CampusMail_Condition_To = "致";
$i_CampusMail_Condition_Subject = "主旨";
$i_CampusMail_Condition_Message = "內容";
$i_CampusMail_Condition_HasAttachment = "包含附件";
$i_CampusMail_Action = "動作";
$i_CampusMail_Action_MarkAsRead = "標記成已讀取";
$i_CampusMail_Action_DeleteIt = "刪除";
$i_CampusMail_Action_MoveToFolder = "移至資料夾";
$i_CampusMail_Action_ForwardTo = "轉寄致";
$i_CampusMail_MailRule_DeleteActionJSWaring = "如選擇\"刪除\"，將不能同時選擇\"標記成已讀取\"或\"移至資料夾\"。";
$i_CampusMail_MailRule_ActionEmptyJSWaring = "請選擇最少一項動作。";

$i_Calendar_Admin_Setting = "iCalendar 設定";

$i_SmartCard = "智能咭";
$i_SmartCard_CardID = "智能咭 ID";
$i_SmartCard_DateRecorded = "紀錄日期";
$i_SmartCard_TimeRecorded = "紀錄時間";
$i_SmartCard_Site = "紀錄地點";
$i_SmartCard_StudentOutingDate = "活動/外出日期";
$i_SmartCard_StudentOutingOutTime = "活動/外出時間";
$i_SmartCard_StudentOutingBackTime = "結束時間";
$i_SmartCard_StudentOutingPIC = "負責老師";
$i_SmartCard_StudentOutingFromWhere = "出發地點";
$i_SmartCard_StudentOutingLocation = "活動/外出地點";
$i_SmartCard_StudentOutingReason = "活動/外出目的";
$i_SmartCard_StudentOutingNoOut = "不是由學校出發";
$i_SmartCard_StudentOutingNoBack = "未/沒有返回";
$i_SmartCard_ClassName = "班別";
$i_SmartCard_GroupName = "小組";
$i_SmartCard_Remark = "備註";
$i_SmartCard_DetentionDate = "留堂日期";
$i_SmartCard_DetentionArrivalTime = "到達時間";
$i_SmartCard_DetentionDepartureTime = "離開時間";
$i_SmartCard_DetentionReason = "留堂原因";
$i_SmartCard_DetentionLocation = "地點";
$i_SmartCard_DetentionNotArrived = "未到";
$i_SmartCard_DetentionNotLeft = "未離開";
$i_SmartCard_ReportMonth = "月份";
$i_SmartCard_Description_Terminal_IP_Settings = "
在 IP 位址的格上, 可以使用:
<ol><li>固定 IP 位址 (e.g. 192.168.0.101)</li>
<li>IP 位址一個範圍 (e.g. 192.168.0.[10-100])</li>
<li>CISCO Style 範圍(e.g. 192.168.0.0/24)</li>
<li>容許所有位址 (輸入 0.0.0.0)</li></ol>
<font color=red>連接讀咭器的電腦, 建議該電腦設定固定 IP 地址 (即不採用 DHCP), 以確保資料不會被其他電腦更改.</font><br>
本系統設有安全措施防範假冒的IP 地址 (Faked IP Address).
";
$i_SmartCard_Instruction_RemoveLog = "請輸入日期";
$i_SmartCard_DownloadOnly = "下載";
$i_SmartCard_RemoveOnly = "移除";
$i_SmartCard_DownloadRemove = "下載後移除";
$i_SmartCard_FromToIncluded = "資料將包括所填寫的兩個日期";
$i_SmartCard_DownloadLogFormat = "下載的檔案是班別及班號的格式 (可上載回伺服器)";
$i_SmartCard_SystemSettings = "系統設定";

$i_StudentAttendance_System = "學生智能咭考勤";
$i_StudentAttendance_SystemAdminUserSetting = "設定管理用戶";
$i_StudentAttendance_AllStudentsWithRecords = "所有出入的學生";
$i_StudentAttendance_Menu_ResponsibleAdmin = "可負責點名人員設定";
$i_StudentAttendance_Menu_DailyOperation = "每日工作";
$i_StudentAttendance_Menu_DataManagement = "出席資料管理";
$i_StudentAttendance_Menu_OtherFeatures = "其他功能";
$i_StudentAttendance_Menu_DataExport = "資料匯出";
$i_StudentAttendance_Menu_DataImport = "資料匯入";
$i_StudentAttendance_Menu_Report = "報告";
$i_StudentAttendance_Menu_OtherSettings = "其他設定";
$i_StudentAttendance_Menu_CustomFeatures = "客制功能";

$i_StudentAttendance_AttendanceMode = "點名模式";
$i_StudentAttendance_AttendanceMode_AM_Only = "只有上午";
$i_StudentAttendance_AttendanceMode_PM_Only = "只有下午";
$i_StudentAttendance_AttendanceMode_WD_Lunch = "全日, 需要計算午飯時間";
$i_StudentAttendance_AttendanceMode_WD_NoLunch = "全日, 不需計算午飯時間";

$i_StudentAttendance_Menu_Slot_School = "學校時間設定";
$i_StudentAttendance_Menu_Slot_Class = "班級特別設定";
$i_StudentAttendance_Menu_Slot_Group = "小組特別設定";

$i_StudentAttendance_NormalDays = "一般日子";
$i_StudentAttendance_Weekday_Specific = "週日特別設定";
$i_StudentAttendance_Cycleday_Specific = "循環日特別設定";
$i_StudentAttendance_NoSpecialSettings = "沒有特別設定";
$i_StudentAttendance_Warn_Please_Select_Weekday="請選擇週日";
$i_StudentAttendance_Warn_Please_Select_CycleDay="請選擇循環日";
$i_StudentAttendance_Warn_Please_Select_A_Day="請選擇日子";
$i_StudentAttendance_ShowALlGroup_Options = "所有小組";
$i_StudentAttendance_HiddenGroup_Options = "按時間表點名";
$i_StudentAttendance_Diplay_Mode = "小組點名模式";


### Added On 3 Sep 2007 ###
$i_StudentAttendance_NonSchoolDaySetting_Warning = "一般日子不能設定為非上課日子";
###########################

$i_StudentAttendance_SpecialDay = "特別日期設定";

$i_StudentAttendance_SetTime_AMStart = "上午上課時間";
$i_StudentAttendance_SetTime_LunchStart = "午飯時間";
$i_StudentAttendance_SetTime_PMStart = "下午上課時間";
$i_StudentAttendance_SetTime_SchoolEnd = "放學時間";

###############################
$i_StudentAttendance_NonSchoolDay="非上課日子";
$i_StudentAttendance_TodayOnward="由今天起";
$i_StudentAttendance_Past = "昔日";
$i_StudentAttendance_ViewPastRecords="檢視過往紀錄";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart="$i_StudentAttendance_SetTime_AMStart 不可大於 / 等於 $i_StudentAttendance_SetTime_LunchStart";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart="$i_StudentAttendance_SetTime_AMStart 不可大於 / 等於 $i_StudentAttendance_SetTime_PMStart";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_AMStart 不可大於 / 等於 $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart="$i_StudentAttendance_SetTime_LunchStart 不可大於 / 等於 $i_StudentAttendance_SetTime_PMStart";
$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_LunchStart 不可大於 / 等於 $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_PMStart 不可大於 / 等於 $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_Cannot_Modify_Past_Records="不能編輯舊紀錄";
$i_StudentAttendance_Previous_Week="上星期";
$i_StudentAttendance_Next_Week="下星期";

###############################
$i_StudentAttendance_WeekDay = "週日";
$i_StudentAttendance_CycleDay = "循環日";
$i_StudentAttendance_TimeSlot_SpecialDay="特別日子";
$i_StudentAttendance_Warn_Please_Select_WeekDay="請選擇週日";
$i_StudentAttendance_Warn_Please_Select_A_Day="請選擇日期";
$i_StudentAttendance_Warn_Invalid_Time_Format="時間格式錯誤";
$i_StudentAttendance_Warn_Invalid_Date_Format="日期格式錯誤";

$i_EditGroupHints = "請到「內聯網管理」，「小組管理」，「小組管理中心」新增小組。";
$i_StudentAttendance_ClassMode = "點名模式";
$i_StudentAttendance_GroupMode = "點名模式";
$i_StudentAttendance_ClassMode_UseSchoolTimetable = "使用學校時間表";
$i_StudentAttendance_ClassMode_UseClassTimetable = "使用班別指定時間表";
$i_StudentAttendance_GroupMode_UseSchoolTimetable = "無須按時間表點名";
$i_StudentAttendance_GroupMode_UseGroupTimetable = "須按時間表點名";
$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance = "無須點名";
$i_StudentAttendance_GroupMode_NoNeedToTakeAttendance = "無須點名";
$i_StudentAttendance_ClassMode_Edit = "編輯時間";

$i_StudentAttendance_CardLog = "拍咭紀錄";
$i_StudentAttendance_ViewTodayRecord = "檢視今天紀錄";
$i_StudentAttendance_ArchiveTodayRecord = "整存今天紀錄";
$i_StudentAttendance_Outing = "外出紀錄";
$i_StudentAttendance_Detention = "留堂紀錄";
$i_StudentAttendance_TimeSlotSettings = "時間設定";
$i_StudentAttendance_Report = "報告";
$i_StudentAttendance_WordTemplates = "預設字句";
$i_StudentAttendance_DataExport = "資料匯出 (離線電腦之用)";
$i_StudentAttendance_Reminder = "老師召見提示";
$i_StudentAttendance_DataImport = "資料匯入";
$i_StudentAttendance_ParentLetters = "家長信";
$i_StudentAttendance_By = "自";
$i_StudentAttendance_Slot = "時段";
$i_StudentAttendance_Type = "類型";
$i_StudentAttendance_Slot_Start = "開始時間";
$i_StudentAttendance_Slot_End = "結束時間";
$i_StudentAttendance_Slot_Boundary = "返校時間/放學時間";
$i_StudentAttendance_Slot_InUse  = "使用";
$i_StudentAttendance_Slot_Special = "特別日子";
$i_StudentAttendance_Slot_Special_Today1 = "今天為";
$i_StudentAttendance_Slot_Special_Today2 = " ,所以時間有所不同";
$i_StudentAttendance_Type_GoToSchool = "返校";
$i_StudentAttendance_Type_LeaveSchool = "離校";
$i_StudentAttendance_Slot_AM = "上午";
$i_StudentAttendance_Slot_PM = "下午";
$i_StudentAttendance_Slot_AfterSchool = "放學";
$i_StudentAttendance_Slot_SettingsDescription = "&nbsp; * 請輸入二十四小時制時間 (HH:mm) (e.g. 07:30, 16:30).";
$i_StudentAttendance_View_Date = "檢視日期";
$i_StudentAttendance_ToSchoolTime = "返校時間";
$i_StudentAttendance_LeaveSchoolTime = "放學時間";
$i_StudentAttendance_Time_Arrival = "到達時間";
$i_StudentAttendance_Time_Departure = "離開時間";
$i_StudentAttendance_Status = "狀態";
$i_StudentAttendance_MinLate = "遲到時間(分鐘)";
$i_StudentAttendance_MinEarly = "早退時間(分鐘)";
$i_StudentAttendance_Status_Present = "出席";
$i_StudentAttendance_Status_OnTime = "準時";
$i_StudentAttendance_Status_Late = "遲到";
$i_StudentAttendance_Status_Absent = "缺席";
$i_StudentAttendance_Status_PreAbsent = "無出席";
$i_StudentAttendance_Status_EarlyLeave = "早退";
$i_StudentAttendance_Status_Outing = "外出活動";
$i_StudentAttendance_Status_SL = "病假";
$i_StudentAttendance_Status_AR = "事假";
$i_StudentAttendance_Status_LE = "遲到及早退";
$i_StudentAttendance_Status_Truancy = "逃學";
$i_StudentAttendance_Status_Waived = "豁免";
$i_StudentAttendance_Archive_Date = "整存資料日期";
$i_StudentAttendance_con_RecordArchived = "資料已整存";
$i_StudentAttendance_Report_Daily = "檢視每天紀錄";
$i_StudentAttendance_Report_Student = "檢視學生每月紀錄";
$i_StudentAttendance_Report_Class = "全班列表";
$i_StudentAttendance_Report_SelectSlot = "檢視時段";
$i_StudentAttendance_Report_PlsSelectSlot = "請選擇最少一個時段";
$i_StudentAttendance_Report_PlsSelectLeaveType = '請選擇假別';
$i_StudentAttendance_Menu_DailyLessonOperation = '每日工作(課堂考勤)';
$i_StudentAttendance_Menu_ReportLesson = '報告(課堂考勤)';
$i_StudentAttendance_Report_NoRecord = "沒有紀錄";
$i_StudentAttendance_Report_Search="搜尋";
$i_StudentAttendance_Report_NoCardTab = "沒有拍卡報告";
$i_StudentAttendance_InSchool = "回校";
$i_StudentAttendance_LeaveSchool = "離校";
$i_StudentAttendance_Today_lastRecord = "今天最後拍咭時間";
$i_StudentAttendance_PresetWord_CardSite = "拍咭地點";
$i_StudentAttendance_PresetWord_OutingFromWhere = "[外出/活動] 出發地點";
$i_StudentAttendance_PresetWord_OutingLocation = "[外出/活動] 地點";
$i_StudentAttendance_PresetWord_OutingObjective = "[外出/活動] 目的";
$i_StudentAttendance_PresetWord_DetentionLocation = "[留堂] 地點";
$i_StudentAttendance_PresetWord_DetentionReason = "[留堂] 原因";

$i_StudentAttendance_NotDisplayOntime = "不顯示準時紀錄";
$i_StudentAttendance_ImportFormat_CardID = "時間, $i_SmartCard_Site, 智能咭 ID";
$i_StudentAttendance_ImportFormat_ClassNumber = "時間, $i_SmartCard_Site, 班別, 班號";
$i_StudentAttendance_ImportFormat_From_OfflineReader = "由離線拍咭機收集的紀錄";
$i_StudentAttendance_ImportTimeFormat = "請把時間一項的格式設定為 YYYY-MM-DD HH:mm:ss";
$i_StudentAttendance_Import_Instruction_OneDayOnly = "檔案內的所有紀錄需為同一天的紀錄";
$i_StudentAttendance_Import_Warning_OneDayOnly = "並非所有紀錄為同一天的紀錄, 請分開處理.";
$i_StudentAttendance_ImportConfirm = "如確定上例的紀錄正確, 請按匯入以完成匯入.";
$i_StudentAttendance_ImportCancel = "如資料有誤, 請按取消清除這些紀錄.";
$i_StudentAttendance_ArchiveWait = "此動作需時較長, 請耐心等候...";
$i_StudentAttendance_StudentInformation = "學生資料";
$i_StudentAttendance_ParentLetters_Late = "遲到家長信";
$i_StudentAttendance_Reminder_Date = "提示日期";
$i_StudentAttendance_Reminder_Teacher = "接見老師";
$i_StudentAttendance_Reminder_Reason = "原因";
$i_StudentAttendance_Reminder_Status_Past = "已過";
$i_StudentAttendance_Reminder_Status_Coming = "未到";
$i_StudentAttendance_Status_PastRecord = "已過的紀錄";
$i_StudentAttendance_Status_TodayAndComing = "今天及未到的紀錄";
$i_StudentAttendance_Reminder_ImportFileDescription = "
第一欄 : 班別 <br>
第二欄 : 班號 <br>
第三欄 : 提示日期 (如留空即為<b>明天</b>) <br>
第四欄 : 召見老師之內聯網帳號 <br>
第五欄 : 內容
";
$i_StudentAttendance_Reminder_StartDate = "提示開始日期";
$i_StudentAttendance_Reminder_FinishDate = "提示完結日期";
$i_StudentAttendance_Reminder_RepeatSelection[0] = "不重複";
$i_StudentAttendance_Reminder_RepeatSelection[1] = "每天";
$i_StudentAttendance_Reminder_RepeatSelection[2] = "週日";
$i_StudentAttendance_Reminder_RepeatSelection[3] = "循環日";
$i_StudentAttendance_Reminder_WeekdaySelection = "選擇週日";
$i_StudentAttendance_Reminder_CycledaySelection = "選擇循環日";
$i_StudentAttendance_Reminder_RepeatFrequency = "重複頻率";
$i_StudentAttendance_Reminder_StartDateEmptyWarning = "請輸入提示開始日期";
$i_StudentAttendance_Reminder_FinishDateEmptyWarning = "請輸入提示完結日期";
$i_StudentAttendance_Reminder_WrongDateWarning = "請輸入正確日期";
$i_StudentAttendance_Reminder_WeekdaySelectionWarning = "請選擇週日";
$i_StudentAttendance_Reminder_CycleDaySelectionWarning = "請選擇循環日";
$i_StudentAttendance_Reminder_TeacherSelectionWarning = "請選擇接見老師";
$i_StudentAttendance_Reminder_InputReasonWarning = "請輸入原因";
$i_StudentAttendance_Outing_ImportFileDescription = "
第一欄 : 班別 <br>
第二欄 : 班號 <br>
第三欄 : 負責老師姓名 <br>
第四欄 : 日期 (如留空即為<b>今天</b>)(YYYY-MM-DD) <br>
第五欄 : 外出時間 (HH:mm:ss) <br>
第六欄 : 結束時間 (HH:mm:ss) <br>
第七欄 : 原因
";
$i_StudentAttendance_Detention_ImportFileDescription = "
第一欄* : 班別 <br>
第二欄* : 班號 <br>
第三欄 : $i_SmartCard_DetentionDate (如留空即為<b>今天</b>)(YYYY-MM-DD) <br>
第四欄 : $i_SmartCard_DetentionArrivalTime (HH:mm:ss) <br>
第五欄 : $i_SmartCard_DetentionDepartureTime (HH:mm:ss) <br>
第六欄 : $i_SmartCard_DetentionLocation<br>
第七欄 : $i_SmartCard_DetentionReason<br>
第八欄 : $i_SmartCard_Remark<br>
<br>
* - 必須填上
";

$i_StudentAttendance_SimCard = "摸擬拍咭";
$i_StudentAttendance_Action_SpecialArchive = "特別點名時間";
$i_StudentAttendance_NeedTakeAttendance = "需要點名";
$i_StudentAttendance_NoCardRecord = "沒有拍咭紀錄";

$i_StudentAttendance_RemoveByDateRange = "以日期移除";
$i_StudentAttendance_ImportCardID = "匯入智能咭 ID";
$i_StudentAttendnace_ImportRawRecord = "匯入拍咭紀錄";
$i_StudentAttendance_OR_ExportStudentInfo = "匯出離線拍咭機使用的學生資料";

$i_StudentAttendance_LunchSettings = "中午出入設定";
$i_StudentAttendance_NonTargetSettings = "不使用智能咭點名的名單";
$i_StudentAttendance_BackSchoolTime = "上課時間";
$i_StudentAttendance_LunchStartTime = "午飯時間開始";
$i_StudentAttendance_LunchEndTime = "午飯時間結束";
$i_StudentAttendance_SchoolEndTime = "放學時間";

$i_StudentAttendance_Frontend_menu_TakeAttendance = "點名";
$i_StudentAttendance_Frontend_menu_CheckStatus = "檢查紀錄";
$i_StudentAttendance_Frontend_menu_Report = "報告";
$i_StudentAttendance_Frontend_menu_CheckMyRecord = "我的紀錄";

$i_StudentAttendance_SelectAnotherClass = "選擇另一班別";
$i_StudentAttendance_Field_Date = "日期";
$i_StudentAttendance_Field_Date_From = "由";
$i_StudentAttendance_Field_Date_To = "到";
$i_StudentAttendance_Field_ConfirmedBy = "已作確認的用戶";
$i_StudentAttendance_Field_LastConfirmedTime = "確認時間";
$i_StudentAttendance_LeftStatus_Type_InSchool = "返校情況";
$i_StudentAttendance_LeftStatus_Type_Lunch = "午飯情況";
$i_StudentAttendance_LeftStatus_Type_AfterSchool = "離校情況";
$i_StudentAttendance_NoLateStudents = "沒有學生遲到";
$i_StudentAttendance_NoAbsentStudents = "沒有學生缺席";
$i_StudentAttendance_NoEarlyStudents = "沒有學生早退";
$i_StudentAttendance_NoNeedTakeAttendance = "不使用智能咭點名";
$i_StudentAttendance_InSchool_BackAlready = "曾回校";
$i_StudentAttendance_InSchool_HaveBeenToSchool= "曾回校";
$i_StudentAttendance_InSchool_NotBackYet = "未回校";
$i_StudentAttendance_Lunch_NotOutYet = "未外出";
$i_StudentAttendance_Lunch_GoneOut = "已外出";
$i_StudentAttendance_Lunch_Back = "已回來";
$i_StudentAttendance_AfterSchool_Left = "已離校";
$i_StudentAttendance_AfterSchool_Stay = "未離校";
$i_StudentAttendance_ViewStudentList = "檢視學生名單";
$i_StudentAttendance_Menu_DataMgmt_ResetTime = "更改舊日上課時間以重設該日資料";
$i_StudentAttendance_Menu_DataMgmt_UndoPastProfileRecord = "還原已確認的缺席/遲到學生檔案紀錄";
$i_StudentAttendance_Menu_DataMgmt_DataClear = "清除過時資料";
$i_StudentAttendance_Menu_DataMgmt_BadLogs = "錯誤行為紀錄";
$i_StudentAttendance_Menu_OtherFeatures_Outing = "外出紀錄";
$i_StudentAttendance_Menu_OtherFeatures_Detention = "留堂紀錄";
$i_StudentAttendance_Menu_OtherFeatures_Reminder = "老師召見提示紀錄";
$i_StudentAttendance_Offline_Import_DataType = "資料種類";
$i_StudentAttendance_Offline_Import_DataType_InSchool = "回校上課";
$i_StudentAttendance_Offline_Import_DataType_LunchOut = "外出午膳";
$i_StudentAttendance_Offline_Import_DataType_LunchIn = "午膳後回校";
$i_StudentAttendance_Offline_Import_DataType_AfterSchool = "放學離校";
$i_StudentAttendance_BadLogs_Type_NotInLunchList = "無權的學生嘗試外出午膳";
$i_StudentAttendance_BadLogs_Type_GoLunchAgain = "嘗試再次外出午膳";
$i_StudentAttendance_BadLogs_Type_FakedCardAM = "有咭紀錄但缺席 (上午)";
$i_StudentAttendance_BadLogs_Type_FakedCardPM = "有咭紀錄但缺席 (下午)";
$i_StudentAttendance_BadLogs_Type_NoCardRecord = "忘記帶咭 / 拍咭";
$i_StudentAttendance_BadLogs_Type = $i_general_Type;
$i_StudentAttendance_UndoProfile_Warning = "以上的考勤紀錄(不論是由智能咭系統還是自行輸入的紀錄), 將會被刪除. 請按繼續還原.";
$i_StudentAttendance_ResetTime_Warning = "學生檔案遲到紀錄亦會同時被更新";
$i_StudentAttendance_DataRemoval_Warning = "所有拍咭紀錄 (包括時間, 每月紀錄) 會被移除, 而<u><b>學生檔案中的資料則會保留</b></u>.";
$i_StudentAttendance_Field_CardStation = "拍咭地點";
$i_StudentAttendance_Report_ClassMonth = "班別每月出席表";
$i_StudentAttendance_Report_ClassDaily = "班別每日出席表";
$i_StudentAttendance_Report_ClassBadRecords = "各班的錯誤行為的統計";
$i_StudentAttendance_Report_StudentBadRecords = "學生錯誤行為的統計";
$i_StudentAttendance_Field_InSchoolTime = "回校時間";
$i_StudentAttendance_Field_LunchOutTime = "外出午飯時間";
$i_StudentAttendance_Field_LunchBackTime = "返回時間";
$i_StudentAttendance_Field_LeaveTime = "離校時間";
$i_StudentAttendance_ReportHeader_NumStudents = "學生人數";
$i_StudentAttendance_ReportHeader_NumRecords = "紀錄數目";
$i_StudentAttendance_DisplayTop = "顯示最高的";
$i_StudentAttendance_Top = "最高";
$i_StudentAttendance_Top_student_suffix = "位的學生";
$i_StudentAttendance_Message_ThisClassNoNeedToTake = "這個班級無須點名";
$i_StudentAttendance_Message_NoStudentInClass = "本班沒有學生";
$i_StudentAttendance_New_InSchoolTime = "重設的上課時間";
$i_StudentAttendance_Field_NumLateOriginal = "原來的遲到人數";
$i_StudentAttendance_Field_NumLateNew = "更新後的遲到人數";
$i_StudentAttendance_CalendarLegend_NotConfirmed = "未確認";
$i_StudentAttendance_CalendarLegend_Confirmed = "已確認";
$i_StudentAttendance_Action_SetAbsentToPresent = "把".$i_StudentAttendance_Status_PreAbsent."轉為".$i_StudentAttendance_Status_Present."";
$i_StudentAttendance_Action_SetAbsentToOntime = "把".$i_StudentAttendance_Status_PreAbsent."轉為".$i_StudentAttendance_Status_OnTime."";

$i_StudentAttendance_Symbol_Present = "<span style='font-family:新細明體'>／</span>";
$i_StudentAttendance_Symbol_Absent = "<span style='font-family:新細明體'>○</span>";
$i_StudentAttendance_Symbol_Late = "<span lang=EN-US style='font-family:Symbol;mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:Symbol'><span style='mso-char-type:symbol;mso-symbol-font-family:Symbol'>&AElig;</span></span>";
$i_StudentAttendance_Symbol_Outing = "z";
$i_StudentAttendance_Symbol_EarlyLeave = "<span class=GramE><span lang=EN-US style='font-family:新細明體'>#</span></span>";
$i_StudentAttendance_Symbol_SL = "<span style='font-family:新細明體'>⊕</span>";
$i_StudentAttendance_Symbol_AR = "<span lang=EN-US style='font-family:Webdings;mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:Webdings'><span style='mso-char-type:symbol;mso-symbol-font-family:Webdings'>y</span></span>";
$i_StudentAttendance_Symbol_LE = "<span lang=EN-US style='font-family:\"Wingdings 2\";mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'><span style='mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'>U</span></span>";
$i_StudentAttendance_Symbol_Truancy = "<span lang=EN-US style='font-family:新細明體'>●</span>";
$i_StudentAttendance_Symbol_Waived = "<span class=GramE><span lang=EN-US style='font-family:新細明體'>*</span></span>";

$i_StudentAttendance_Stat_Present = "出席數";
$i_StudentAttendance_Stat_Absent = "缺席數";
$i_StudentAttendance_Stat_Late = "遲到數";
$i_StudentAttendance_Stat_EarlyLeave = "早退數";
$i_StudentAttendance_Stat_Truancy = "逃學數";
$i_StudentAttendance_Daily_Stat = "每日統計";
$i_StudentAttendance_Monthly_Level_Stat = "本月統計";
$i_StudentAttendance_Attendance_Number = "在席人數";
$i_StudentAttendance_Average_Present = "平均出席人數";
$i_StudentAttendance_Average_Absent = "平均缺席人數";
$i_StudentAttendance_Symbol_Reference = "考勤符號";
$i_StudentAttendance_FullReport_Days1 = "本月上課共:";
$i_StudentAttendance_FullReport_Days2  = "日";
$i_StudentAttendance_exactly_matched="須完全符合輸入字串";
$i_StudentAttendance_Export_Notice="備註：如使用離線模式(Offline Mode)而非離線拍卡器，請按\"學生資料\"匯出考勤紀錄。";
### Added By Ronald On 12 Apr 2007
$i_StudentAttendance_LeftStatus_Type_LeavingTime = "檢視學生離校時間";

### Added By Ronald On 13 Apr 2007
$i_StudentAttendance_Leave_Status_Early_Leave_AM = "早退 (上午)";
$i_StudentAttendance_Leave_Status_Early_Leave_PM = "早退 (下午)";
$i_StudentAttendance_Leave_Status_Normal = "正常";
### Added On 19 Apr 2007
$i_StudentAttendance_Early_Leave_Insert_Record = "新增早退記錄";

### Added On 20 Apr 2007
$i_StudentAttendance_Class_Select_Instruction = "請選擇班別";
$i_StudentAttendance_Student_Select_Instruction = "請選擇學生";
$i_StudentAttendance_Input_Correct_Time = "請輸入正確時間";
$i_StudentAttendance_Input_Time = "請輸入時間";
$i_StudentAttendance_Early_Leave_Warning = "<font color=red>請注意下列學生的離校記錄將會被更改:</font>";
$i_StudentAttendance_Menu_Slot_Session_Setting = "預定時間設定";
$i_StudentAttendance_Menu_Slot_Session_School = "學校時間設定";
$i_StudentAttendance_Menu_Slot_Session_Class = "班級特別設定";
$i_StudentAttendance_Menu_Slot_Session_Group = "小組特別設定";

$i_StudentAttendance_TimeSessionSettings = "時間設定";
$i_StudentAttendance_Menu_Slot_Session_Name = "名稱";
$i_StudentAttendance_Menu_Time_Mode = "時間表模式";
$i_StudentAttendance_Menu_Time_Input_Mode = "時間輸入";
$i_StudentAttendance_Menu_Time_Session_Mode = "預定時間";
$i_StudentAttendance_Menu_Time_Session_MorningTime = "上午上課時間";
$i_StudentAttendance_Menu_Time_Session_LunchStartTime = "午飯時間";
$i_StudentAttendance_Menu_Time_Session_LunchEndTime = "下午上課時間";
$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime = "放學時間";
$i_StudentAttendance_Menu_Time_Session_NonSchoolDay = "非上課日子";
$i_StudentAttendance_Menu_Time_Session_Using = "使用中";
$i_StudentAttendance_Menu_Time_Session_Activated = "使用中";
$i_StudentAttendance_Menu_Time_Session_School_Use = "使用";
$i_StudentAttendance_ViewToSchoolTime = "檢視返校時間";
$i_StudentAttendance_CalendarLegend_RecordWithData = "有記錄";
$i_StudentAttendance_TimeSessionSettings_SessionName_Warning = "請輸入名稱";
$i_StudentAttendance_TimeSessionSettings_WeekdaySelection_Warning = "請選擇週日";
$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning = "請選擇時間";
$i_StudentAttendance_TimeSessionSettings_CycledaySelection_Warning = "請選擇循環日";

$i_StudentAttendance_TimeSessionSettings_NowUsing1 = "正在使用";
$i_StudentAttendance_TimeSessionSettings_NowUsing2 = "的班別";
$i_StudentAttendance_TimeSessionSettings_AlreadyHaveSpecialDaySession = "已有特別日期設定的日子";
$i_StudentAttendance_TimeSession_GetSpecialdayInfo_Warning = "注意: 以上日子將不會被更改";
$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning = "* - 該時間設定已經停用";
$i_StudentAttendance_TimeSession_UsingAsNormalSetting = "正在使用為<span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_NormalDays</span>]</span>的班別";
$i_StudentAttendance_TimeSession_UsingAsWeeklySetting = "正在使用為<span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_Weekday_Specific</span>]</span>的班別";
$i_StudentAttendance_TimeSession_UsingAsCycleSetting = "正在使用為<span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_Cycleday_Specific</span>]</span>的班別";
$i_StudentAttendance_TimeSession_UsingAsSpecialSetting = "正在使用為<span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_SpecialDay</span>]</span>的班別";
$i_StudentAttendance_TimeSession_SessionNowUsing = "* - 正在使用中";
$i_StudentAttendance_TimeSession_OtherClassUseSameSession = "其他班別同時使用";
$i_StudentAttendance_TimeSession_TypeSelection = "選擇類別";
$i_StudentAttendance_TimeSession_ClassSelection = "選擇班別";
$i_StudentAttendance_TimeSession_TypeSelectionWarning = "請選擇類別";
$i_StudentAttendance_TimeSession_ClassSelectionWarning = "請選擇班別";
### Added By Ronald On 2 Aug 2007 ###
$i_StudentAttendance_StaffAndStudentInformation = "教職員及學生資料";
$i_StudentAttendance_Setting_DisallowStudentProfileInput="不允許在學生檔案中新增/刪除考勤紀錄";

### Added On 24 Aug 2007 ###
$i_StudentAttendance_Reminder_Content = "內容";
############################

### Added on 22 Oct 2008 ###
$i_StudentAttendance_ShortCut = "捷徑";
############################

### Added on 23 Jan 2009 ###
$i_StudentAttendance_default_attend_status="默認出席狀態";

$i_StudentAttendance_Warning_Data_Outdated="資料已過時，請重新呈送一次";

// added on 20090608 by kenneth chung
$Lang['StudentAttendance']['TakeAttendanceFor'] = "點名";
$Lang['StudentAttendance']['School'] = "回校";
$Lang['StudentAttendance']['Lesson'] = "上課";
$Lang['StudentAttendance']['Session'] = "課節";

// added on 20090710 by kenneth chung
$Lang['StudentAttendance']['RecordTime'] = "記錄時間";

$i_SmartCard_TerminalSettings = "智能咭終端機設定";
$i_SmartCard_Terminal_IPList = "終端機的 IP 位址";
$i_SmartCard_Terminal_IPInput = "請把終端機的 IP 地址每行一個輸入";
$i_SmartCard_Terminal_ExpiryTime = "終端機登記後可使用時間(分鐘)";
$i_SmartCard_Terminal_YourAddress = "你現時的 IP 地址";
$i_SmartCard_Terminal_IgnorePeriod = "不接受重複拍咭的時間";
$i_SmartCard_Terminal_IgnorePeriod_unit = "分鐘";

$i_SmartCard_Confirm_Update_Attend = "確定更新記錄";

$i_SmartCard_Responsible_Admin_Class = "班代表";

$i_SmartCard_Responsible_Admin_Settings_List = "檢視負責點名學生";
$i_SmartCard_Responsible_Admin_Settings_Manage = "管理負責點名學生";

$i_SmartCard_Settings_Lunch_List_Title = "檢視可外出午膳學生名單";
$i_SmartCard_Settings_Lunch_Misc_Title = "其他設定";

$i_SmartCard_Settings_Lunch_New_Type = "模式";
$i_SmartCard_Settings_Lunch_New_Type_Class = "新增整個班別";
$i_SmartCard_Settings_Lunch_New_Type_Student = "新增個別學生";
$i_SmartCard_Settings_NoStudentsAvailableForSelection = "沒有可選擇學生. 所有學生已經編入名單";
$i_SmartCard_Settings_Lunch_Instruction_Class = "請選擇班別. 該班別的所有學生將會編入可外出午膳名單.";
$i_SmartCard_Settings_Lunch_Instruction_Student = "請選擇要加入可外出午膳名單的學生.";
$i_SmartCard_Settings_Admin_Number_Students = "可負責點名的學生人數";


$i_SmartCard_Settings_Lunch_Misc_No_Record = "中午外出不需拍咭";
$i_SmartCard_Settings_Lunch_Misc_Out_Once = "中午只准外出一次";
$i_SmartCard_Settings_Lunch_Misc_All_Allow = "全部准許外出";

$i_SmartCard_DailyOperation_viewClassStatus_PrintOption = "$i_PrinterFriendlyPage 選項";
$i_SmartCard_DailyOperation_ViewClassStatus = "檢視各班別情況";
$i_SmartCard_DailyOperation_ViewGroupStatus = "檢視各小組情況";
$i_SmartCard_DailyOperation_ViewLateStatus = "檢視遲到名單";
$i_SmartCard_DailyOperation_ViewAbsenceStatus = "檢視缺席名單";
$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus = "檢視早退名單";
$i_SmartCard_DailyOperation_ViewLeftStudents = "檢視在校情況";
$i_SmartCard_DailyOperation_ImportOfflineRecords = "匯入離線紀錄";
$i_SmartCard_DailyOperation_ViewEntryLog = "檢視出入紀錄";
$i_SmartCard_DailyOperation_BlindConfirmation="核實所有班別";
$i_SmartCard_DailyOperation_Number_Of_Absent_Student="未確認的缺席學生人數";
$i_SmartCard_DailyOperation_Number_Of_Outing_Student="未確認的外出學生人數";
$i_SmartCard_DailyOperation_OrderBy_InSchoolTime="以拍咭時間排序";
$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber="以班別和班號排序";
$i_SmartCard_DailyOperation_Preset_Absence="學生請假紀錄";
$i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent="檢視指定學生";
$i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate="檢視指定日期";
$i_SmartCard_DailyOperation_Preset_Absence_Has_Leave_Record="有請假紀錄";

$i_SmartCard_DailyOperation_Check_Time = "檢視學生返學時間";
$i_SmartCard_DailyOperation_Check_Time_School_Time_Table="學校 / 班級 時間設定";
$i_SmartCard_DailyOperation_Check_Time_Group_Time_Table="小組時間設定";
$i_SmartCard_DailyOperation_Check_Time_Final_Time_Table="總結返學時間";

$i_SmartCard_DailyOperation_BlindConfirmation_Notes="<b><u>注意:</u></b>
<br>
<ul><li>呈送前請先核實當日出席資料準確無誤</li></ul>
";
$i_SmartCard_DailyOperation_BlindConfirmation_Notes1="注意";
$i_SmartCard_DailyOperation_BlindConfirmation_Notes2="呈送前請先核實當日出席資料準確無誤";

$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll = "顯示所有學生";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs = "顯示缺席學生";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate = "顯示遲到學生";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate = "顯示缺席或遲到學生";
### Added By Ronald On 16 Apr 2007
$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime = "以離開時間排序";

$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record = "記錄未被確認";
$i_SmartCard_Frontend_Take_Attendance_In_School_Time = "拍咭時間";
$i_SmartCard_Frontend_Take_Attendance_In_School_Station = "拍咭地點";
$i_SmartCard_Frontend_Take_Attendance_Status = "出席?";
$i_SmartCard_Frontend_Take_Attendance_PreStatus = "狀況";
$i_SmartCard_Frontend_Take_Attendance_Waived = "豁免";
$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record_Update = "記錄未被確認 ( 儲存途中,記錄曾被更新 )．請檢查記錄及再次呈送";


$i_StudentPromotion = "學生升班系統";
$i_StudentPromotion_Menu_Reset = "重設所有資料 (升班前的準備程序)";
$i_StudentPromotion_Menu_ViewEditList = "編配下學年班別 (升班程序 1)";
$i_StudentPromotion_Menu_EditClassNumber = "編排下學年班號 (升班程序 2)";
$i_StudentPromotion_Menu_NewClassEffective = "更新所有學生資料 (完成程序 1)";
$i_StudentPromotion_Menu_ArchiveLeftStudents = "整存所有離校生的資料 (完成程序 2)";
$i_StudentPromotion_StudentAssigned = "已編配至新班別的學生";
$i_StudentPromotion_Unset = "未被編配的學生 (將被視作離校生)";
$i_StudentPromotion_OldClass = "現時班別";
$i_StudentPromotion_OldClassNumber = "現時班號";
$i_StudentPromotion_NewClass = "新班別";
$i_StudentPromotion_NewClassNumber = "新班號";
$i_StudentPromotion_Type_ClassName = "$i_StudentPromotion_OldClass, $i_StudentPromotion_OldClassNumber, $i_StudentPromotion_NewClass, $i_StudentPromotion_NewClassNumber";
$i_StudentPromotion_Type_Login = "$i_UserLogin, $i_StudentPromotion_NewClass, $i_StudentPromotion_NewClassNumber";
$i_StudentPromotion_Prompt_PreparationDone = "<font color=green>資料已重設</font>";
$i_StudentPromotion_Prompt_NewClassEffectiveDone = "<font color=green>新班別資料已經更新</font>";
$i_StudentPromotion_Prompt_DataArchived = "<font color=green>資料已整存</font>";
$i_StudentPromotion_Summary = "總結";
$i_StudentPromotion_ConfirmFinalize = "請按呈送以完成整個程序";
$i_StudentPromotion_ClickContinueToProceed = "請按繼續以啟動此程序. 程序進行需時, 請耐心等候.";
$i_StudentPromotion_Sorting_Name = "英文名稱";
$i_StudentPromotion_Sorting_BoysName = "男生, 英文名稱";
$i_StudentPromotion_Sorting_GirlsName = "女生, 英文名稱";
$i_StudentPromotion_GenerateClassNumber = "自動排列班號";
$i_StudentPromotion_ClassSize = "本班總人數";
$i_StudentPromotion_Unset_List = "現時未分配新班別名單";
$i_StudentPromotion_RestoreLogin = "新$i_UserLogin";
$i_StudentPromotion_LoginDuplicated = "因$i_UserLogin"."已被其他使用者使用.";
$i_StudentPromotion_Description_ClassAssignment = "請選擇現時班別以編配該班學生至下學年的新班別.<br>或以檔案匯入方式編配新班別及新班號.";
$i_StudentPromotion_Notes_NewClassEffective = "<b><u>注意:</u></b><br>
<ul><li>此程序<font color=red><b><u>不能還原</u></b></font>, 請預先作資料備份</li>
<li>此程序啟動後:<br>
   <ul>
     <li>學生將更新至新班別及新班號.</li>
     <li><font color=red><u><b>所有未被編配新班別班號的學生，將被編為「已離校」</b></u></font></li>
     <li>學生將自動加入升班後的新班別小組; 舊班別小組將被存檔.</li>
     <li>已離校學生將不能登入系統.</li>
     <li>如有需要, 仍可於用戶管理中心將已離校學生重新編配至新班別 (特別升班)</li>
   </ul></li>
<li><u>剛完結之年度</u>為系統設定 &gt; 基本設定中的本年度. 請先進行此程序, 才更新本年度至新學年.</li>
</ul>
";
$i_StudentPromotion_NumLeftStudents = "狀態為已離校之學生人數";
$i_StudentPromotion_ArchivalYear = "這些學生的離校年份為";
$i_StudentPromotion_Notes_ArchiveLeftStudents = "<b><u>注意:</u></b><br>
<ul><li>此程序<font color=red><b><u>不能還原</font></u></b>, 請預先作資料備份.</li>
<li>此程序啟動後:<br>
    <ul>
      <li>離校生的學生紀錄可於 <u>學生檔案 &gt; 檢視已離校學生紀錄</u> 中查看.</li>
      <li>狀態為已離校的學生不能再重新編配至新班別. 這些學生戶口會從用戶管理中心刪除.</li></ul>
";
$i_StudentPromotion_Notes_SpecialPromote = "<b><u>注意:</u></b><br>
<ul><li>此功能只適用於特別情況.</li>
<li>此程序啟動後, 現時為已離校的學生, 不能再變回已批准.</li>
<li>請確定此學生已入學, 因為會影響此學生的班級紀錄.</li>
</ul>";
$i_StudentPromotion_Notes_MarkLeft = "<b><u>注意:</u></b><br>
<ul><li>此功能只適用於特別情況 (學期中途離校).</li>
<li>此程序啟動後, 現時為已離校的學生, 不能再變回已批准.</li>
<li>請確定此學生已退學, 因為會影響此學生的班級紀錄.</li>
</ul>";

$i_StudentPromotion_NoAutoAlumniAdd = "<font color=green>此程序啟動後, 將<b>不會</b>為已離校學生自動開啟本系統的校友戶口. 校友戶口需要自行開啟.</font>";
$i_StudentPromotion_AutoAlumni = "<font color=green>此程序啟動後, 將為已離校學生自動開啟校友戶口 (使用原來的登入帳號及密碼)</font>";
$i_StudentPromotion_AutoAlumni_NoGroupSet = "<font color=red>系統不能自動開啟校友戶口 (因為校友會的組別並沒有設置, 請聯絡博文教育技術支援人員.)</font>";
$i_StudentPromotion_AutoAlumni_NoGroupExist = "<font color=red>系統不能自動開啟校友戶口 (因為校友會的組別不存在/已被刪除, 請聯絡博文教育技術支援人員.)</font>";
$i_StudentPromotion_SpecialPromote = "特別升級";
$i_StudentPromotion_SetToLeft = "編為已離校";
$i_StudentPromotion_PromoteTo = "升班至";
$i_StudentPromotion_NewClassNumber = "新班號";

$i_StudentPromotion_Alert_Reset = "你確定要重設所有學生分班資料?";
$i_StudentPromotion_Alert_PromoteClass = "請選擇新班別";
$i_StudentPromotion_Alert_NoClassNumber = "你沒有輸入班號. 你確定要繼續?";
$i_StudentPromotion_Alert_RemoveRecord = "你確定從新班別中刪除這學生?";
$i_StudentPromotion_AcademicYear = "剛完結之年度";
$i_StudentPromotion_PlsChangeAfterPromotion = "請完成此程序後, 再更新$i_SettingsCurrentAcademicYear.";
$i_StudentPromotion_ImportFailed = "以下的紀錄不能被匯入";

// for iPortfolio Groups Promote
$i_StudentPromotion_Menu_iPortfolioGroup = "進行 iPortfolio 學生升班程序";
$i_StudentPromotion_iPortfolioGroup_Summary = "iPortfolio 學生升班報告";
$i_StudentPromotion_Alert_iPortfolioGroup = "你是否確定要進行 iPortfolio 學生升班程序？";
$i_StudentPromotion_iPortfolioGroup_UserNum = "升班學生數目";
$i_StudentPromotion_Current_AcademicYear = "新學年";
$i_StudentPromotion_Confirm_iPortfolioGroup = "iPortfolio 學生升班程序已完成。";
$i_StudentPromotion_iPortfolioRemind = "<b><u>注意:</u></b><br><ul><li>在 iPortfolio 中, 學生會按年度及所屬班別被分組。本程序會將學生加入到升班後所屬的年度及班別組別，而舊有的組別將一同保留。例如某同學在新學年前的所屬組別是 \"2005-2006 3A\"，在進行升班程序後，她將會被加入到 \"2006-2007 4A\"（但你仍然可從 \"2005-2006 3A\" 組別中找到該名學生）。</ul></li>";

//Class History Management 2009-01-15
$i_StudentPromotion_mgt['ACADEMICYEAR'] = "年度";
$i_StudentPromotion_mgt['NUMSTUDENT']	= "學生人數";
$i_StudentPromotion_mgt['CLASS']		= "班別";
$i_StudentPromotion_mgt['CLASSNUM']		= "班號";
$i_StudentPromotion_mgt['STUDENT']		= "學生";
$i_StudentPromotion_mgt['CLASSHISTORY']	= "班級紀錄";
$i_StudentPromotion_mgt['CONFIRMDELETE'][0]= "你是否確定要刪除這個學生的記錄？";
$i_StudentPromotion_mgt['CONFIRMDELETE'][1]= "你是否確定要刪除這班別所有學生的記錄？";
$i_StudentPromotion_mgt['CONFIRMDELETE'][2]= "你是否確定要刪除這年所有學生的記錄？";
$i_StudentPromotion_mgt['CONFIRMDELETE'][3]= "你是否確定要刪除全部學生的記錄？";
$i_StudentPromotion_mgt['BACKTOTOP']	= "返回頂";
$i_StudentPromotion_mgt['IMPORTDESC'][0]	= "User Login, 英文名稱, 年度, 班別, 班號";
$i_StudentPromotion_mgt['IMPORTDESC'][1]	= "WebSAMSRegNo, 英文名稱, 年度, 班別, 班號";
$i_StudentPromotion_mgt['YEARDUPLICATE']	= "年度重複. 請輸入其他年度.";
$i_StudentPromotion_mgt['EDITERROR']		= "請輸入有關資料";
$i_StudentPromotion_mgt['TOTAL'] = "總人數";
$i_StudentPromotion_mgt['PREVIOUS']	= "前一頁";
$i_StudentPromotion_mgt['NEXT']	= "後一頁";
$i_StudentPromotion_mgt['PAGE']	= "頁";
$i_StudentPromotion_mgt['DISPLAY']	= "每頁顯示";
$i_StudentPromotion_mgt['PERPAGE']	= "項";
$i_StudentPromotion_mgt['USER']	= "用戶";
$i_StudentPromotion_mgt['NAV']['YEAR']		= "班別紀錄管理";
$i_StudentPromotion_mgt['NAV']['CLASS']		= "班別名紀錄管理";
$i_StudentPromotion_mgt['NAV']['STUDENT']	= "學生紀錄管理";
$i_StudentPromotion_mgt['NAV']['DUPLICATE']	= "班別重複紀錄";
$i_StudentPromotion_mgt['NAV']['EDIT']		= "紀錄編輯";
$i_StudentPromotion_mgt['NAV']['PROBLEM']	= "管理問題紀錄";
$i_StudentPromotion_mgt['BTNDUPLICATE']		= "管理重複紀錄";
$i_StudentPromotion_mgt['BTNPROBLEMDATA']	= "管理問題紀錄";
$i_StudentPromotion_mgt['BTNSEARCH']		= "搜尋";
$i_StudentPromotion_mgt['NORECORD'] 		= "沒有記錄";
$i_StudentPromotion_mgt['UNCHECKTOREMOVE']	= "取消勾選以移除項目";
$i_StudentPromotion_mgt['DUPSEARCH']		= "搜尋";
$i_StudentPromotion_mgt['EDIT_NOTE']		= "* 重複或有問題之紀錄";
$i_StudentPromotion_mgt['RECORD']			= "紀錄";
$i_StudentPromotion_mgt['OCCURRENCE']		= "次數";
$i_StudentPromotion_mgt['DUPLICATE']		= "重複次數";


$i_WebSAMS_Notice_Export="如要匯出 WebSAMS 格式的考勤資料，請完成以下各項的設定：<br><Br>";
$i_WebSAMS_Notice_Export1="一 、 設定每個級別相對應的 WebSAMS 級別名稱<br><br>";
$i_WebSAMS_Notice_Export2="二 、 設定學校的基本資料 (如：WebSAMS 學校編號，現時 WebSAMS 學年等)<br><br>";
$i_WebSAMS_Notice_Export3="三 、 在原因代碼管理中輸入各類 WebSAMS 原因代碼<br><Br>";
$i_WebSAMS_Notice_AttendanceCode = "匯出 WebSAMS 格式的考勤資料用";
$i_WebSAMSRegNo_Format_Notice="(\"<b>#</b>\" 號是WebSAMS 註冊號碼格式的一部份)";
$i_WebSAMS_AttendanceCode_No_RegNo_Students="未有WebSAMS 註冊號碼的學生人數";
$i_WebSAMS_AttendanceCode_Export="匯出考勤紀錄";
$i_WebSAMS_Registration_No ="WebSAMS 註冊號碼";
$i_WebSAMS_ClassLevelName ="WebSAMS 級別名稱";
$i_WebSAMS_ClassLevel ="WebSAMS 級別";
$i_WebSAMS_Integration = "WebSAMS 整合";
$i_WebSAMS_Menu_Attendance = "考勤資料更新";
$i_WebSAMS_Menu_AttendanceCode = "匯出 WebSAMS 格式的考勤資料";
$i_WebSAMS_Attendance_StartDate = "資料開始日期";
$i_WebSAMS_Attendance_EndDate = "資料結束日期";
$i_WebSAMS_con_RecordSynchronized = "資料更新成&#21151;";
$i_WebSAMS_AttendanceCode_Basic = "基本資料";
$i_WebSAMS_AttendanceCode_ReasonCode = "原因代碼管理";
$i_WebSAMS_AttendanceCode_SchoolID = "WebSAMS 學校編號";
$i_WebSAMS_AttendanceCode_SchoolYear = "現時 WebSAMS 學年 (YYYY)";
$i_WebSAMS_AttendanceCode_SchoolLevel = "WebSAMS 學校級別";
$i_WebSAMS_AttendanceCode_SchoolSession = "WebSAMS 學校時段";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Kindergarten = "幼稚園";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Primary = "小學";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Secondary = "中學";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Others = "其他";

# Added On 13 Aug 2007
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_DependOnClassLevel = "以學生班級決定";
####################

$i_WebSAMS_AttendanceCode_Option_SchoolSession_AM = "上午";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_PM = "下午";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_WD = "全日";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_Evening = "夜校";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K1 = "K1 - Nursery Class";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K2 = "K2 - Lower Kindergarten";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K3 = "K3 - Upper Kindergarten";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_LP = "LP - Lower Preparatory";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P1 = "P1 - 小一";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P2 = "P2 - 小二";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P3 = "P3 - 小三";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P4 = "P4 - 小四";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P5 = "P5 - 小五";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P6 = "P6 - 小六";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_PP = "PP - Pre-Primary - Spec";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_PR = "PR - Primary - Spec";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S1 = "S1 - 中一";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S2 = "S2 - 中二";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S3 = "S3 - 中三";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S4 = "S4 - 中四";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S5 = "S5 - 中五";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S6 = "S6 - 中六";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S7 = "S7 - 中七";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_SJ = "SJ - 初中";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_SS = "SS - 高中";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_UP = "UP - Upper Preparatory";
$i_WebSAMS_Attendance_Reason_ReasonCode = "原因代碼";
$i_WebSAMS_Attendance_Reason_ReasonText = "原因名稱";
$i_WebSAMS_Attendance_Reason_ReasonType="原因類別";
$i_WebSAMS_Attendance_Reason_OtherReason="其他原因";
$i_WebSAMS_Attendance_Reason_OtherReason_Notice="當所輸入的原因沒有相應代碼時，系統將會以此代碼代替";
$i_WebSAMS_Attendance_Reason_Notice1="使用WEBSAMS考勤原因.";
$i_WebSAMS_ClassCode="WebSAMS 班別代碼";
$i_WebSAMS_Class_ImportInstruction="<b><u>檔案說明:</u></b><br>
第一欄 : 班級名稱 (如 1A ).<br>
第二欄 : 級別 (如 F.1 ).<br>
第三欄 : WebSAMS 班別代碼 (如 1B ).<br>
第四欄 : WebSAMS 級別 (如 S1 ).<br>";


$i_WebSAMS_ReasonCode_ImportInstruction="<table border=0 cellspacing=1 cellpadding=1><tr><td align=right><b><u>檔案說明:</u></b><br></td></tr>
<tr><td align=right>第一欄 : </td><td>原因代碼 (如 01 )</td></tr>
<tr><td align=right>第二欄 : </td><td>原因名稱 (如 Sick Leave )</td></tr>
<tr><td align=right>第三欄 : </td><td>原因類別 (如 1) :</td></tr>
<tr><td></td><td>1 - 缺席</td></tr>
<tr><td></td><td>2 - 遲到</td></tr>
<tr><td></td><td>3 - 早退</td></tr>
</table>";
$i_WebSAMS_Attendance_Export_Warning1="請先輸入以下基本資料 :<br><li>$i_WebSAMS_AttendanceCode_SchoolID</li><li>$i_WebSAMS_AttendanceCode_SchoolYear</li><li>$i_WebSAMS_AttendanceCode_SchoolLevel</li><li>$i_WebSAMS_AttendanceCode_SchoolSession</li>";
$i_WebSAMS_Attendance_Export_Warning2="以下級別沒有連結至WebSAMS級別:";
$i_WebSAMS_Attendance_Export_Warning3="搜尋的結果超出限額10000,請收窄日期範圍";
$i_WebSAMS_Attendance_Reason_CurrenlyMapTo="現正使用";

$i_Payment_UserType="用戶類別";
$i_Payment_All ="全部";
## Added on 18-7-2007
$i_Payment_All_Users ="全部用戶";
$i_Payment_System = "智能咭繳費系統";
$i_Payment_System_Admin_User_Setting = "設定管理用戶";
$i_Payment_Menu_Settings = "基本設定";
$i_Payment_Menu_DataImport = "戶口紀錄資料匯入";
$i_Payment_Menu_DataBrowsing = "各項紀錄瀏覽";
$i_Payment_Menu_StatisticsReport = "統計及報表";
$i_Payment_Menu_CustomReport = "客制報告";
$i_Payment_Menu_PrintPage = "可列印頁";
$i_Payment_Menu_Settings_TerminalIP = "智能咭終端機設定";
$i_Payment_Menu_Settings_TerminalAccount = "終端機登入戶口";
$i_Payment_Menu_Settings_PaymentCategory = "繳費類別設定";
$i_Payment_Menu_Settings_PaymentItem = "繳費項目設定";
$i_Payment_Menu_Settings_PaymentItem_SelectDateRange="顯示以下日子期間所有的繳費項目";
$i_Payment_Menu_Settings_PurchaseCategory = "貨品類別設定";
$i_Payment_Menu_Settings_PurchaseItem = "貨品項目設定";
$i_Payment_Menu_Settings_ClearTransaction = "清除所有交易紀錄";
$i_Payment_Menu_Settings_ResetAccount = "重設所有戶口金額";
$i_Payment_Menu_Settings_Letter = "繳費通知書設定";
$i_Payment_Menu_Settings_Letter_Header = "通知書頁首";
$i_Payment_Menu_Settings_Letter_Footer = "通知書頁尾";
$i_Payment_Menu_Settings_PPS_Setting = "繳費聆設定";
$i_Payment_Menu_Settings_AlipayHK_Handling_Fee = "AlipayHK 手續費";
$i_Payment_Menu_Settings_Letter_Disable_Signature = "不顯示家長簽名欄";


$i_Payment_Menu_Import_PPSLink = "學生 PPS 戶口連結";
$i_Payment_Menu_Import_PPSLog = "PPS 每日紀錄";
$i_Payment_Menu_Import_CashDeposit = "現金存入紀錄";
$i_Payment_Menu_Browse_TerminalLog = "終端機登入紀錄";
$i_Payment_Menu_Browse_CreditTransaction = "戶口增值紀錄";
$i_Payment_Menu_Browse_CreditTransaction_DateRange = "選擇時間範圍";
$i_Payment_Menu_Browse_StudentBalance = "檢視用戶的結存及收支紀錄";
$i_Payment_Menu_Browse_StudentBalance_DirectPay = "檢視用戶收支紀錄";
$i_Payment_TransactionType_Income = "收入";
$i_Payment_Menu_Browse_TransactionRecord = "詳細紀錄";
$i_Payment_Menu_Browse_PurchasingRecords = "單一銷售紀錄";
$i_Payment_Menu_Browse_MissedPPSBill = "不明的繳費聆紀錄";
$i_Payment_Menu_PrintPage_Outstanding = "追數表";
$i_Payment_Menu_PrintPage_AddValueReport = "增值報告";
$i_Payment_Menu_PrintPage_AddValueReport_Date= "日期";
$i_Payment_Menu_Report_SchoolAccount = "學校戶口報告";
$i_Payment_Menu_Report_PresetItem = "預設繳費項目報告";
$i_Payment_Menu_Report_TodayTransaction = "今日交易紀錄";

$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal = "款項(總數)";
$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount = "交易紀錄(項數)";

$i_Payment_Menu_PrintPage_Receipt = "收據";
$i_Payment_Menu_PrintPage_Receipt_PaymentItemList = "學生繳費收據";

$i_Payment_Import_Format_PPSLink2 = "<b><u>檔案說明:</u></b><br>
第一欄 : $i_UserLogin<br>
第二欄 : PPS 戶口號碼
";
$i_Payment_Import_Format_PPSLink = "<b><u>檔案說明:</u></b><br>
第一欄 : 班別<br>
第二欄 : 班號<br>
第三欄 : PPS 戶口號碼
";
$i_Payment_Import_Format_PPSLog = "請上載由 PPS 下載的紀錄檔案";
$i_Payment_Import_Format_PPSLog_Charge_Deduction = "繳費聆手續費($".LIBPAYMENT_PPS_CHARGE.", Counter Bill 則為 $".LIBPAYMENT_PPS_CHARGE_COUNTER_BILL.")將自動從所有交易項目中扣除。";
$i_Payment_Import_PPSLink_Result = "匯入結果";
$i_Payment_Import_PPSLink_Result_Summary_TotalRows = "總數";
$i_Payment_Import_PPSLink_Result_Summary_Successful = "已更新";
$i_Payment_Import_PPSLink_Result_Summary_NoMatch = "沒有這個學生";
$i_Payment_Import_PPSLink_Result_Summary_Occupied = "PPS 帳號已被使用";
$i_Payment_Import_PPSLink_Result_Summary_Missing = "沒有 PPS 帳號";
$i_Payment_Import_PPSLink_Result_Summary_ExistAlready = "學生已有 PPS 帳號";
$i_Payment_Import_PPSLink_Result_ErrorMsg_NotSuccessful = "未能匯入的紀錄";
$i_Payment_Import_PPSLink_Result_ErrorMsg_NoMatch = "沒有這個學生.";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Occupied = "此 PPS 帳號已被使用.";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Missing = "沒有 PPS 帳號";
$i_Payment_Import_PPSLink_Result_ErrorMsg_ExistAlready = "此學生已有 PPS 帳號. 如更改請先移除原有號碼.";
$i_Payment_Import_PPSLink_Result_GoToIndex = "返回學生列表";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason = "原因";
$i_Payment_Import_PPS_FileType = "檔案類型";
$i_Payment_Import_PPS_FileType_Normal = "IEPS檔案 (利用電話或互聯網)";
$i_Payment_Import_PPS_FileType_CounterBill = "Counter Bill 檔案 (例如: OK 便利店)";
$i_Payment_PPS_Charge = "繳費聆手續費";
$i_Payment_Import_PPS_ChargeSeparation = "手續費會分開作另一個交易紀錄顯示";

$i_Payment_Field_PPSAccountNo = "PPS 帳戶號碼";
$i_Payment_Field_CreditAmount = "增值金額";
$i_Payment_Field_CreditAmount_AfterDeduction = "扣除手續費後增值金額";
$i_Payment_Field_TransactionTime = "交易時間";
$i_Payment_Field_PostTime = "進誌時間";
$i_Payment_Field_Username = "用戶名稱";
$i_Payment_Field_PaymentAllowed = "繳費";
$i_Payment_Field_PurchaseAllowed = "售貨/增值";
$i_Payment_Field_LastLogin = "最後登入";
$i_Payment_Field_AccountCreation = "戶口開設時間";
$i_Payment_Field_RefCode = "參考編號";
$i_Payment_Field_Balance = "結存";
$i_Payment_Field_LastUpdated = "最後更新";
$i_Payment_Field_LastUpdatedBy = "最後更新的管理員";
$i_Payment_Field_AdminHelper = "經管理中心";
$i_Payment_Field_TerminalUser = "經終端機";
$i_Payment_Field_TransactionType = "交易類別";
$i_Payment_Field_TransactionDetail = "交易內容";
$i_Payment_Field_PaymentCategory = "繳費類別";
$i_Payment_Field_DisplayOrder = "排列次序";
$i_Payment_Field_PaymentItem = "繳費項目";
$i_Payment_Field_PaymentCategory_Code = "繳費類別編號";
$i_Payment_Warning_Enter_PaymentCategoryCode = "請輸入繳費類別編號";
$i_Payment_Warning_Duplicate_PaymentCategoryCode = "繳費類別編號重複";
$i_Payment_Field_PaymentItemCode = "繳費項目編號";
$i_Payment_Warning_Enter_PaymentItemCode = "請輸入繳費項目編號";
$i_Payment_Warning_Duplicate_PaymentItemCode = "繳費項目編號重複";
$i_Payment_Warning_Select_PaymentMethod = "請選擇付款方法";
$i_Payment_Field_PaymentDefaultAmount = "正常金額";
$i_Payment_Field_Amount = "金額";
$i_Payment_Field_Function = "功能";
$i_Payment_Field_ChangePassword = "更改密碼";
$i_Payment_Field_LoginTime = "登入時間";
$i_Payment_Field_LogoutTime = "登出時間";
$i_Payment_Field_IP = "終端機 IP 位址";
$i_Payment_Field_CurrentBalance = "現時結餘";
$i_Payment_Field_BalanceAfterPay = "繳款後結餘";
$i_Payment_Field_BalanceAfterUndo = "還原後結餘";
$i_Payment_Field_PaidTime = "繳費時間";
$i_Payment_Field_PaymentDeadline = "繳費最後限期";
$i_Payment_Field_BalanceAfterTransaction = "帳後結餘";
$i_Payment_Field_PayPriority = "付費優先次序";
$i_Payment_Field_NewDefaultAmount = "新$i_Payment_Field_PaymentDefaultAmount";
$i_Payment_Field_SinglePurchase_Unit = "收費單位";
$i_Payment_Field_SinglePurchase_TotalAmount = "共收金額";
$i_Payment_Field_SinglePurchase_Count = "交易次數";
$i_Payment_Field_AdminInCharge = "負責人";
$i_Payment_Field_RecordedAccountNo = "紀錄中的 $i_Payment_Field_PPSAccountNo";
$i_Payment_Field_MappedStudent = "已轉到學生";
$i_Payment_Field_PaidCount = "已繳費";
$i_Payment_Field_TotalPaidCount = "總人數";

$i_Payment_Empty_Will_Be_Genreated_By_System = "留空會由系統自動產生";

$i_Payment_Field_AllPaid = "已收妥";
$i_Payment_Field_NotAllPaid = "尚未收妥";

$i_Payment_Field_PaidCount = "已繳費人數";
$i_Payment_Field_TotalPaidCount = "總人數";
$i_Payment_Field_TransactionFileTime = "增值紀錄時間";

### Added On 3 Aug 2007 ###
$i_Payment_Menu_ManualCashDeposit = "手動現金存入紀錄";
$i_Payment_CashDepositDate = "現金存入日期";
$i_Payment_CashDepositTime = "現金存入時間";
###########################

### Added On 7 Aug 2007 ###
$i_Payment_Field_SetAllUse = "設定為全部使用";
$i_Payment_AmountWarning = "請輸入金額";
$i_Payment_RefCodeWarning = "請輸入參考編號";
$i_Payment_DepositDateWarning = "請輸入現金存入日期";
###########################

### Added On 8 Aug 2007 ###
$i_Payment_DetailTransactionLog = "詳細交易記錄";
$i_Payment_DepositWrongDateWarning = "請輸入正確現金存入日期";
$i_Payment_DepositAmountWarning = "請輸入正確金額";
###########################

### Sentence is too long for using linterface->GET_SYS_MSG()
$i_Payment_Import_ConfirmRemoval = "<table class='systemmsg' width='50%' border='0' cellpadding='3' cellspacing='0'><tr><td>系統發現有未經確認之紀錄. 這可能是由於上次的輸入並未確認成功或另一管理員正在輸入.<br />";
$i_Payment_Import_ConfirmRemoval2 = "你需要等待 (重新整理此頁) 或強行刪除那些未確認紀錄, 才可以匯入新的檔案.</td><tr></table>";

$i_Payment_Import_Confirm = "請按確認以繼續或按取消以清除.";
$i_Payment_AccountNoMatch = "帳戶號碼尚未登記<font color=red>*</font>";
$i_Payment_AccountNoMatch_Remark="系統會將此交易紀錄視為不明的繳費聆紀錄處理。<br>請前往 <b>$i_Payment_Menu_DataBrowsing</b> &gt; <b>$i_Payment_Menu_Browse_MissedPPSBill</b> 匹配此帳戶號碼予相關學生。";
$i_Payment_Import_Error="是次紀錄未能匯入，請檢查檔案中的資料正確無誤後再次上載";
$i_Payment_Import_NoMatch_Entry = "班別及班號錯誤";
$i_Payment_Import_NoMatch_Entry2 = "內聯網帳號錯誤";


$i_Payment_Import_InvalidAmount="金額無效";
$i_Payment_Import_DuplicatedStudent="資料重複";
$i_Payment_Import_PayAlready="此學生已繳費";

$i_Payment_Import_CashDeposit_Instruction = "
<span class=extraInfo>只適用於學生</span><Br><Br>
第一欄 : 班別<Br>
第二欄 : 班號<br>
第三欄 : 存入金額<br>
第四欄 : 存入之日期時間 (YYYY-MM-DD HH:mm:ss, 留空會被視為此匯入之時間)<br>
第五欄 : $i_Payment_Field_RefCode (留空會由系統自動產生)
";
$i_Payment_Import_CashDeposit_Instruction2 = "
<span class=extraInfo>適用於學生或老師</span><Br><br>
第一欄 : $i_UserLogin<br>
第二欄 : 存入金額<br>
第三欄 : 存入之日期時間 (YYYY-MM-DD HH:mm:ss, 留空會被視為此匯入之時間)<br>
第四欄 : $i_Payment_Field_RefCode (留空會由系統自動產生)
";

$i_Payment_Auth_PaymentRequired = "繳費終端機不需登入";
$i_Payment_Auth_PurchaseRequired = "貨品售賣/增值終端機不需登入";

$i_Payment_Credit_Method = "增值方法";
$i_Payment_Credit_TypePPS = "PPS";
$i_Payment_Credit_TypeCashDeposit = "現金";
$i_Payment_Credit_TypeAddvalueMachine = "增值機";
$i_Payment_Credit_TypeUnknown = "沒有紀錄";

$i_Payment_TransactionType_Credit = "增值";
$i_Payment_TransactionType_Payment = "繳費";
$i_Payment_TransactionType_Purchase = "銷售";
$i_Payment_TransactionType_TransferTo = "轉賬至";
$i_Payment_TransactionType_TransferFrom = "由轉賬";
$i_Payment_TransactionType_CancelPayment = "取消繳費";
$i_Payment_TransactionType_Refund = "退款";
$i_Payment_TransactionType_Debit = "支出";
$i_Payment_TransactionType_Other = "其他";

$i_Payment_TransactionDetailPPS = "PPS 增值 / PPS Credit";
$i_Payment_TransactionDetailCashDeposit = "現金增值 / Cash Deposit";
$i_Payment_ClassInvolved = "需要繳費的班別";
$i_Payment_NoClass = "如不是全班需要繳費, 請新增後才使用匯入";

$i_Payment_PaymentStatus_NotStarted="未開始";
$i_Payment_PaymentStatus_Paid = "已繳交";
$i_Payment_PaymentStatus_Unpaid = "未繳交";
$i_Payment_Payment_PaidCount = "已繳交的學生";
$i_Payment_Payment_UnpaidCount = "未繳交的學生";
/*
$i_Payment_Payment_StudentImportFileDescription1 = "
第一欄 : 班別 <br>
第二欄 : 班號 <br>
第三欄 : 需繳付金額 <br>";
$i_Payment_Payment_StudentImportFileDescription2 = "
第一欄 : 內聯網帳號 <br>
第二欄 : 需繳付金額 <br>";
*/
$i_Payment_Payment_StudentImportFileDescription1 = "班別,班號,需繳付金額";
$i_Payment_Payment_StudentImportFileDescription2 = "內聯網帳號,需繳付金額";

$i_Payment_Payment_StudentImportNotice = "
請注意:<br>
- 已繳交之學生不會被更改 <br>
- 如金額空白, 則表示此學生無需繳交此項費用
";
$i_Payment_con_PaidNoEdit = "<font color=red>已繳費的紀錄不能更改</font>";
$i_Payment_con_PaymentProcessed = "<font color=green>繳費已成功處理.</font>";
$i_Payment_con_PaymentUndo = "<font color=green>款項已還原.</font>";
$i_Payment_con_PaymentProcessFailed = "<font color=red>款項未能成功繳交.</font>";
$i_Payment_con_RefundFailed = "<font color=red>不能退款或結存為零</font>";
$i_Payment_con_RecordMapped = "<font color=red>這項紀錄已經被處理</font>";
$i_Payment_con_FileImportedAlready = "<font color=red>這個檔案已被匯入過一次</font>";

$i_Payment_alert_selectOneFunction = "你必須選擇最少一項功能";
$i_Payment_alert_forcepay = "你確定要替已選之學生進行繳款程序?";
$i_Payment_alert_undopay = "你確定要把已選學生之繳款紀錄還原?";
$i_Payment_alert_refund = "你確定要把已選學生退款? (此程序不能還原)";

// add by kenneth chung on 20090112
$i_Payment_alert_forcepayall = "你確定要替學生進行繳款程序?";

$i_Payment_action_batchpay = "繳款";
$i_Payment_action_undo = "還原至未繳";
$i_Payment_action_proceed_payment = "進行繳款";
$i_Payment_action_proceed_undo = "進行還原";
$i_Payment_action_cancel_payment = "取消";
$i_Payment_action_refund = "退款";
$i_Payment_action_handle = "處理";

$i_Payment_AccountBalance = "戶口結存及紀錄";
$i_Payment_PaymentRecord = "繳費紀錄";
$i_Payment_ValueAddedRecord = "增值紀錄";
$i_Payment_Transfer = "轉賬至另一子女";
$i_Payment_SelectChildToTransfer = "轉賬至";
$i_Payment_TransferAmount = "轉賬金額";
$i_Payment_ItemSummary = "概覽";
$i_Payment_ItemPaid = "已繳金額";
$i_Payment_ItemUnpaid = "未繳金額";
$i_Payment_ItemTotal = "總金額";
$i_Payment_RefundAmount = '已退款金額';
# Added on 17-7-2007
$i_Payment_Item_Select_Class_Level = "全級";
$i_Payment_Students = "學生";
$i_Payment_Refunded = "已退款";
$i_Payment_ChangeDefaultAmount = "更改$i_Payment_Field_PaymentDefaultAmount";

### Added on 6 Sep 2007
$i_Payment_UpdateRecordConfirm = "你確定要更新記錄?";

$i_Payment_Note_Priority = "數值最細為優先";

$i_Payment_Title_Receipt_Payment = "繳費收據";
$i_Payment_Title_Overall_SinglePurchasing = "單一銷售紀錄";
$i_Payment_Title_SinglePurchasing_Detail = "單一銷售紀錄";

$i_Payment_SearchResult = "搜尋結果";
$i_Payment_IncreaseSensitivity = "縮窄搜尋 (較少結果會被顯示)";
$i_Payment_DecreaseSensitivity = "擴大搜尋 (更多結果會被顯示)";
$i_Payment_ManualSelectStudent = "自行選擇";
$i_Payment_ShowAll = "顯示全部";
$i_Payment_ShowUnlink = "未處理";
$i_Payment_ShowHandled = "已處理";
$i_Payment_Exclude = "不顯示無需追收的班別 <font color=red>(不適用於用繳費通知書)</font>";
$i_Payment_IncludeNonOverDue = "包括所有未到期的繳費項目";

$i_Payment_TotalAmount = "總金額";
$i_Payment_TotalCountTransaction = "交易次數";

// Add on 20-2-2009
$i_Payment_Receipt_Person_In_Charge = "負責人";

$i_Payment_Receipt_Principal = "校長";
$i_Payment_Receipt_SchoolChop = "校印";
$i_Payment_Receipt_Date = "日期";
$i_Payment_Receipt_ThisIsComputerReceipt = "此乃電腦收據";
$i_Payment_Receipt_Remark = "備註";
$i_Payment_Receipt_UpTo = "截至";
$i_Payment_Receipt_AccountBalance = "智能卡戶口結餘";
$i_Payment_Receipt_Official_Receipt = "正式收據";
$i_Payment_Receipt_Computer_GeneratedReceipt = "電腦收據";
$i_Payment_Receipt_ReceivedFrom = "茲收到";
$i_Payment_Receipt_PaidAmount = "交來 HKD";
$i_Payment_Receipt_ForItems = ", 所繳費用以支付下列項目:";
$i_Payment_Receipt_Payment_Category = "收費類別";
$i_Payment_Receipt_Payment_Item = "收費項目";
$i_Payment_Receipt_Payment_Description = "說明";
$i_Payment_Receipt_Payment_Amount = "金額";
$i_Payment_Receipt_Payment_Total = "總額";
$i_Payment_Receipt_Payment_ReceiptCode = "收據編號";
$i_Payment_Receipt_Payment_StaffInCharge_Name = "負責職員姓名";
$i_Payment_Receipt_Payment_StaffInCharge_Signature = "負責職員簽署";
$i_Payment_Receipt_Payment_Date = "日期";
$i_Payment_PrintPage_Outstanding_Total = "總結欠金額";
$i_Payment_PrintPage_Outstanding_DueDate = "到期日";
$i_Payment_PrintPage_PaymentLetter = "繳費通知書";
$i_Payment_PrintPage_PaymentLetter_ParentSign = "家長簽署";
$i_Payment_PrintPage_PaymentLetter_Date = "日期";
$i_Payment_PrintPage_PaymentLetter_UnpaidItem = "未繳費項目";
$i_Payment_PrintPage_Browse_UserIdentityWarning = "請選擇身份";
$i_Payment_PrintPage_Browse_UserStatusWarning = "請選擇狀況";
$i_Payment_PrintPage_StudentOnly = "學生專用";

$i_Payment_PrintPage_Outstanding_AccountBalance = "戶口結餘";
$i_Payment_PrintPage_Outstanding_LeftAmount = "戶口結存不足金額";
$i_Payment_Note_StudentRemoved = "<font color=red>*</font> - 表示該用戶已被移除.";
$i_Payment_Note_PPSCostExport = "日期計算 PPS 每日紀錄內的交易時間";

### added on 11 Oct 2007
$i_Payment_PhotocopierQuotaNotValid_Warning = "請輸入有效配額";
$i_Payment_PhotocopierPackageQtyNotValid_Warning = "請輸入有效數量";
$i_Payment_PhotocopierQuotaSelectPackage_Warning = "請選擇影印套票";
$i_Payment_PhotocopierQuotaPackageName_Warning = "請輸入影印套票名稱";
$i_Payment_PhotocopierQuotaPriceNotValid_Warning = "請輸入有效金額";

$i_Payment_SchoolAccount_Detailed_Transaction="進支詳情";
$i_Payment_SchoolAccount_Deposit="存入";
$i_Payment_SchoolAccount_Expenses="支出";
$i_Payment_SchoolAccount_PresetItem="預設繳費項目";
$i_Payment_SchoolAccount_Total="總數";
$i_Payment_SchoolAccount_SinglePurchase="單一銷售";
$i_Payment_SchoolAccount_TotalIncome="總收入";
$i_Payment_SchoolAccount_TotalExpense="總支出";
$i_Payment_SchoolAccount_NetIncomeExpense="現金淨流入(流出)";
$i_Payment_SchoolAccount_Other="其它";
$i_Payment_PresetPaymentItem_PaidCount ="已繳交";
$i_Payment_PresetPaymentItem_UnpaidCount ="未繳交";
$i_Payment_PresetPaymentItem_PaidAmount ="已繳金額";
$i_Payment_PresetPaymentItem_UnpaidAmount ="未繳金額";
$i_Payment_PresetPaymentItem_ClassStat ="班別統計";
$i_Payment_PresetPaymentItem_Personal ="個別學生紀錄";
$i_Payment_PresetPaymentItem_Progress ="進行中";
$i_Payment_PresetPaymentItem_PaymentPeriod="收款日期";
$i_Payment_PaymentStatus_Ended="已結束";
$i_Payment_PresetPaymentItem_PaidStudentCount="已繳人數";
$i_Payment_PresetPaymentItem_UnpaidStudentCount="未繳人數";
$i_Payment_Menu_StatisticsReport_CreateTime="報表建立時間";
$i_Payment_Warning_Enter_PaymentItemName="請輸入項目名稱";
$i_Payment_Warning_Invalid_DisplayOrder ="排列次序無效";
$i_Payment_Warning_Invalid_PayPriority ="付費優先次序無效";
$i_Payment_Warning_Enter_NewDefaultAmount="請輸入新正常金額";
$i_Payment_Warning_Enter_DefaultAmount="請輸入正常金額";
$i_Payment_Warning_Item_Has_Paid_Student = "已有學生就所選繳費項目進行繳款, 請還原有關繳款後再行刪除。";
$i_Payment_Warning_Item_No_Has_Paid_Student = "未有學生就所選繳費項目進行繳款, 你可以放心刪除。";
$i_Payment_Class_Payment_Report="班別繳費報告";
$i_Payment_Class_Payment_Report_Warning="此日期範圍內的繳費項目多於256個，請嘗試收窄日期範圍。";
$i_Payment_PresetPaymentItem_NoNeedToPay ="--";
$i_Payment_Menu_Settings_Letter_Header2 = "通知書頁首二";
$i_Payment_Warning_InvalidAmount ="輸入金額無效";
$i_Payment_Total_Balance ="總結存";
$i_Payment_Total_Users ="總用戶";

### Added On 30 Apr 2007
$i_Payment_Menu_PhotoCopierQuotaSetting = "影印配額設定";
$i_Payment_Menu_PhotoCopier_Package_Setting = "影印套票設定";
$i_Payment_Menu_PhotoCopier_Quota_Management = "影印配額管理";
$i_Payment_Menu_PhotoCopier_Quota_Record = "影印配額記錄";
$i_Payment_Menu_PhotoCopier_Quota_Details = "影印使用記錄";
$i_Payment_Menu_PhotoCopier_Quota_Purchase = "購買影印套票";
$i_Payment_Menu_PhotoCopier_View_Quota_Record = "檢視影印配額記錄";
$i_Payment_Menu_PhotoCopier_TitleName = "影印套票名稱";
### Added On 2 May 2007
$i_Payment_Field_Quota = "配額";
### Added On 25 Apr 2007
$i_SmartCard_Payment_alert_RecordAddSuccessful = "<font color=green>已新增紀錄</font>";
$i_SmartCard_Payment_Student_Select_Instruction = "請選擇學生";
$i_SmartCard_Payment_Input_Amount_Instruction = "請輸入金額";
$i_SmartCard_Payment_Photocopier_Quota_Purchase_Quantity = "數量";
$i_SmartCard_Payment_Input_PaymentItem_Instruction = "請輸入繳費項目";
$i_SmartCard_Payment_Input_Quota_Instruction = "請輸入配額";
### Added On 3 May 2007
$i_SmartCard_Payment_PhotoCopier_TotalQuota = "總配額";
$i_SmartCard_Payment_PhotoCopier_UsedQuota = "已使用配額";
$i_SmartCard_Payment_PhotoCopier_Class_Select_Instruction = "請選擇班別";
$i_SmartCard_Payment_PhotoCopier_LastestTransactionTime = "最後交易時間";
### Added On 4 May 2007
$i_SmartCard_Payment_PhotoCopier_SelectUser = "選擇用戶";
### Added On 7 May 2007
$i_SmartCard_Payment_PhotoCopier_ResetQuota = "你是否確定要重設所選學生的配額?";
### Added On 8 May 2007
$i_SmartCard_Payment_PhotoCopier_ClassLevel_Select = "選擇級別";
$i_SmartCard_Payment_PhotoCopier_Class_Select = "選擇班別";
### Added On 9 May 2007
$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction = "請輸入總配額";
$i_SmartCard_Payment_PhotoCopier_ClassLevel_Select_Instruction = "請選擇級別";
$i_SmartCard_Payment_PhotoCopier_Class_Select_Instruction = "請選擇班別";
$i_SmartCard_Payment_PhotoCopier_SelectUser_Instruction = "請選擇用戶";
$i_SmartCard_Payment_photoCopier_UserType = "用戶類別";
$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid = "輸入總配額無效";

$i_SmartCard_Payment_PhotoCopier_ChangeQuota = "新增 / 修改配額";
$i_SmartCard_Payment_PhotoCopier_FinalQuota = "最後配額";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType = "修改類別";

$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Reset_By_Admin = "管理員重設";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Add_By_Admin = "管理員新增";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Purchasing = "購買";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Copied_Deduction = "影印扣除";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Quota_Purchase_Quantity = "數量";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type = "類型";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_bw = "黑白 A4";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_bw = "黑白 非A4";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_color = "彩色 A4";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_color = "彩色 非A4";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_No_Record= "沒有紀錄";

$i_Payment_Menu_Settings_PaymentItem_Import_Action_New="新增學生";
$i_Payment_Menu_Settings_PaymentItem_Import_Action_Update="更改金額";
$i_Payment_Menu_Settings_PaymentItem_Import_Action_NoChange="不更改(已繳交)";
$i_Payment_Menu_Settings_PaymentItem_Import_Action="動作";
$i_Payment_Menu_Settings_PaymentItem_ArchivedRecord="已整存紀錄";
$i_Payment_Menu_Settings_PaymentItem_ActiveRecord="現行紀錄";
$i_Payment_Menu_Settings_PaymentItem_Archive_Warning="你是否確定要整存項目?";
$i_Payment_Menu_Settings_PaymentItem_UndoArchive_Warning="你是否確定要將項目還原至未整存?";
$i_Payment_Menu_Settings_PaymentItem_Archive_Warning2="以下項目已整存，不能再進行繳款 :";

### Addded On 23 Aug 2007 ###
$i_Payment_Teacher_PaymentRecord = "檢視教師個人繳費紀錄";
#############################

$i_Payment_Subsidy_Option_No="沒有資助";
$i_Payment_Subsidy_Current_Amount="現時金額";
$i_Payment_Subsidy_New_Amount="更新金額";
$i_Payment_Subsidy_Current_Subsidy_Amount="現時資助金額";
$i_Payment_Subsidy_New_Subsidy="更新資助金額";
$i_Payment_Subsidy_Amount="資助金額";
$i_Payment_Subsidy_Unit="資助來源";
$i_Payment_Subsidy_UnitName=$i_Payment_Subsidy_Unit;
$i_Payment_Subsidy_UnitCode = '資助來源代碼';
$i_Payment_Subsidy_UnitCode_Duplicate = '重複資助來源代碼';
$i_Payment_Import_SubsidyNotFound = "資助來源無效";
$i_Payment_Import_InvalidSubSidyAmount = "資助金額無效";
$i_Payment_Import_SourceOfSubsidyNotEnoughMoney = "資助來源沒有足夠的資助額";
$i_Payment_Subsidy_Edit_Confirm="你是否確定要呈送?";
$i_Payment_Subsidy_Total_Subsidy_Amount ="總資助金額";
$i_Payment_Subsidy_Total_Subsidy_Amount_Detail = "各項資助來源的名稱及總金額";
$i_Payment_Subsidy_Total_Amount ="總額";
$i_Payment_Subsidy_Setting="資助來源設定";
$i_Payment_Subsidy_Warning_Enter_UnitName="請輸入".$i_Payment_Subsidy_UnitName;
$i_Payment_Subsidy_Used_Amount="已使用";
$i_Payment_Subsidy_Left_Amount="餘額";
$i_Payment_Subsidy_Search_Amount="以下學生合共使用資助額";
$i_Payment_Subsidy_Unit_Admin ="最後更新";
$i_Payment_Subsidy_Unit_UpdateTime="更新時間";
$i_Payment_StudentStatus_Removed="已移除的學生";
$i_Payment_StudentStatus_NonRemoved="未移除的學生";
$i_Payment_Subsidy_Warning_Select_SubsidyUnit="請選擇".$i_Payment_Subsidy_Unit;
$i_Payment_Subsidy_Warning_Enter_New_SubsidyAmount=$i_Payment_Subsidy_New_Subsidy."必須大於0";
$i_Payment_Subsidy_Warning_NotEnough_Amount=$i_Payment_Subsidy_Unit.$i_Payment_Subsidy_Left_Amount."不足";

$i_Payment_PPS_Export_Choices_1=$i_status_suspended.", ".$i_status_approved.", ".$i_status_pendinguser."的學生";
$i_Payment_PPS_Export_Choices_2=$i_status_graduate."的學生";
$i_Payment_PPS_Export_Choices_3=$i_Payment_StudentStatus_Removed." <span class='extraInfo'>(以 </span><b>*</b><span class='extraInfo'> 標記)</span>";
$i_Payment_PPS_Export_Select_Student="學生類別";
$i_Payment_PPS_Export_Warning="請選擇".$i_Payment_PPS_Export_Select_Student;
$i_Payment_Note_StudentRemoved2 = "* - 表示該用戶已被移除.";
$i_Payment_Note_StudentLeft = "# - 表示該用戶已離校.";
$i_Payment_OutstandingAmount="未繳金額";
$i_Payment_OutstandingAmountLeft="尚欠";
$i_Payment_PaymentItem_Pay_Warning="系統將不處理戶口結餘不足以繳付當前款項之學生.";
$i_Payment_Refund_Warning="系統將不處理戶口結餘為 $0 之用戶";
$i_Payment_Subsidy_Warning_TotalAmount_MustGreaterThan_ConsumedAmount=$i_Payment_Subsidy_Total_Amount."不能少於".$i_Payment_Subsidy_Used_Amount.$i_Payment_Field_Amount;
$i_Payment_Class_Payment_Report_Display_Mode="顯示模式";
$i_Payment_Class_Payment_Report_Display_Mode_1="於列顯示學生, 於欄顯示繳費項目";
$i_Payment_Class_Payment_Report_Display_Mode_2="於列顯示繳費項目, 於欄顯示學生";
$i_Payment_PresetPaymentItem_ClassStat_Subsidy_Plus_Paid="總金額(已付+資助)";
$i_Payment_Class_Payment_Report_NoNeedToPay="不需繳交";
$i_Payment_Class_Item_Type="紀錄類型";
$i_Payment_Report_Print_Date="編印日期";
$i_Payment_Class_Payment_Report_Notice="每頁最多可顯示45位學生之紀錄";
$i_Payment_Class_Payment_Report_No_of_Items="每版顯示項目";
$i_Payment_Search_By_TransactionTime="以".$i_Payment_Field_TransactionTime;
$i_Payment_Search_By_PostTime="以".$i_Payment_Field_PostTime;
$i_Payment_Field_Total_Chargeable_Amount="總應繳金額";
$i_Payment_Field_Chargeable_Amount="應繳金額";
$i_Payment_Field_Current_Chargeable_Amount="現時應繳金額";
$i_Payment_Field_New_Chargeable_Amount="更新應繳金額";
$i_Payment_NoTransactions_Processed="未有處理任何增值紀錄。";

### added on 5 jun 2008 ###
$i_Payment_ViewStudentAccount_InvalidStartDate = "開始日期無效。";
$i_Payment_ViewStudentAccount_InvalidEndDate = "結束日期無效。";
$i_Payment_ViewStudentAccount_InvalidDateRange = "日期範圍無效。";
###########################

### Added on 20080813 ###
$i_Payment_Menu_Report_Export_StudentBalance = "匯出用戶結存及收支紀錄";
$i_Payment_Menu_Report_StudentBalance = "用戶的結存及收支紀錄";
$i_Payment_Menu_Report_Export_StudentBalance_Alert1 = "請選擇級別";
$i_Payment_Menu_Report_Export_StudentBalance_Alert2 = "請選擇班別";
$i_Payment_Menu_Report_Export_StudentBalance_Alert3 = "請選擇學生";
$i_Payment_Menu_Report_Export_StudentBalance_MultiSelect = "可按住CTRL鍵，點擊多個項目。";
$i_Payment_Menu_Browse_StudentBalance_Status_Left_Temp = "已離校 (暫存)";
$i_Payment_Menu_Browse_StudentBalance_Status_Left_Archived = "已離校 (整存)";
#########################


// added on 20090407
$Lang['Payment']['CancelDeposit'] = "取消紀錄";
$Lang['Payment']['CSVImportDeposit'] = "CSV 匯入";
$Lang['Payment']['ManualDeposit'] = "手動輸入";

// added on 20090415
$Lang['Payment']['CancelDepositDescription'] = "取消現金存入紀錄";
$Lang['Payment']['CashDeposit'] = "現金存入";
$Lang['Payment']['CancelDepositWarning'] = "你確定要取消選取的紀錄? (此程序不能還原)";
$Lang['Payment']['DonateBalanceWarning'] = "你確定要把已選取之紀錄捐給學校? (此程序不能還原)";
$Lang['Payment']['DonateBalance'] = "捐款給學校";
$Lang['Payment']['DonateBalanceDescription'] = "結餘捐款給學校";
$Lang['Payment']['Donated'] = "已捐款";
$Lang['Payment']['DonateFailed'] = "<font color=red>不能捐款或結存為零</font>";
$Lang['Payment']['DepositCancelSuccess'] = "<font color=green>紀錄已取消</font>\n";
$Lang['Payment']['CancelDepositInvalidWarning'] = "某些記錄的結餘未能支付取消動作";

// added on 20090708
$Lang['Payment']['CancelDepositReport'] = "取消增值記錄報告";
$Lang['Payment']['DonationReport'] = "捐款報告";
$Lang['Payment']['Keyword'] = "關鍵字";

// added on 20090803
$Lang['Payment']['ArchiveUserLegend'] = "<font color=red>*</font> - 已整存用戶";

$i_StaffAttendance_InputTime_Warning1="進入時間不可大過下班時間";
$i_StaffAttendance_InputTime_Warning2="這個職員於所選日期不需要拍咭";
$i_StaffAttendance_InputTime_Warning3="系統已有進入時間紀錄";
$i_StaffAttendance_InputTime_Warning4="離開時間不可小於上班時間";
$i_StaffAttendance_InputTime_Warning5="系統已有離開時間紀錄";
$i_StaffAttendance_InputTime_Confirmation="請確保所輸入之時間沒有錯誤. 確定要新增時間?";


$i_StaffAttendance_ManualTimeAdjustment ="手動更改時間";
$i_StaffAttendance_Outing_warning="以下職員在該日已有假期紀錄";
$i_StaffAttendance_Holiday_warning="以下職員在該日已有外出紀錄";
$i_StaffAttendance_msg_time1 = "放工時間不可早或等於返工時間";
$i_StaffAttendance_import_invalid_entries="匯入資料錯誤";
$i_StaffAttendance_msg_password1 = "密碼已更改";
$i_StaffAttendance_msg_invalid_number="輸入數字無效";
$i_StaffAttendance_System = "eClass 職員智能咭考勤系統";
$i_StaffAttendance_SystemSettings = "系統設定";
$i_StaffAttendance_SystemSettings_Terminal = "終端機設定";
$i_StaffAttendance_SystemSettings_ChangePassword = "更改密碼";
$i_StaffAttendance_StaffSettings = "職員資料";
$i_StaffAttendance_StaffSettings_Group = "分組設定";
$i_StaffAttendance_StaffSettings_Staff = "職員資料";
$i_StaffAttendance_DataManagement = "資料管理";
$i_StaffAttendance_DataManagement_RawLog = "拍咭紀錄";
$i_StaffAttendance_DataManagement_ViewArchive = "檢視/整存拍咭紀錄";
$i_StaffAttendance_DataManagement_Outing = "外出紀錄";
$i_StaffAttendance_DataManagement_Leave = "假期紀錄";
$i_StaffAttendance_Report = "報表";
$i_StaffAttendance_Report_Day = "每日";
$i_StaffAttendance_Report_Week = "每星期";
$i_StaffAttendance_Report_Staff = "以職員檢視";
$i_StaffAttendance_Report_Summary = "概覽";
$i_StaffAttendance_Report_Day_Range_Abs = "教職員缺席統計表";
$i_StaffAttendance_Report_Day_Range_Abs_By_Name = "教職員請假缺席統計";
$i_StaffAttendance_PageTitle_Day = "職員考勤紀錄詳情";
$i_StaffAttendance_Report_Staff_Waived = "撤回";
$i_StaffAttendance_Report_Date = "以日期檢視";
$i_StaffAttendance_Report_Date_Title = "日期";
$i_StaffAttendance_Report_Number_Of_Date = "日數";

$i_StaffAttendance_Report_Monthly = "每月";

$i_StaffAttendance_LogOut="登出";
$i_StaffAttendance_Find="尋找";

$i_StaffAttendance_Greeting = "請從左邊清單選擇";

$i_StaffAttendance_Group = "上班時間組別";
$i_StaffAttendance_Group_ID = "組別號碼";
$i_StaffAttendance_Group_Title = "組別名稱";
$i_StaffAttendance_Group_Start = "返工時間";
$i_StaffAttendance_Group_End = "放工時間";
$i_StaffAttendance_Group_Special = "特別日子";
$i_StaffAttendance_Group_Special_DutyTime = "特別日子上班時間";

$i_StaffAttendance_Group_SpecialDayType = "特別日子";
$i_StaffAttendance_Group_WorkingHrs = "工作時間";
$i_StaffAttendance_NoNeedMark = "不用點名";
$i_StaffAttendance_NeedMark = "需要點名";

$i_StaffAttendance_Group_OnDuty = "需上班";
$i_StaffAttendance_Group_InTime = "進入時間";
$i_StaffAttendance_Group_InStatus = "狀況(進入)";
$i_StaffAttendance_Group_InWaived = "撤回進入紀錄";
$i_StaffAttendance_Group_OutTime = "離開時間";
$i_StaffAttendance_Group_OutStatus = "狀況(離開)";
$i_StaffAttendance_Group_OutWaived = "撤回離開紀錄";
$i_StaffAttendance_Status = "狀況";
$i_StaffAttendance_Status_in = "進入";
$i_StaffAttendance_Status_out = "離開";

$i_StaffAttendance_Result_Waived = "撤回";
$i_StaffAttendance_Result_NotWaived = "未撤回";

$i_StaffAttendance_Change_Status = "更改狀況";
$i_StaffAttendance_Status_Normal = "正常";
$i_StaffAttendance_Status_Absent = "缺席";
$i_StaffAttendance_Status_Late = "遲到";
$i_StaffAttendance_Status_Holiday = "放假";
$i_StaffAttendance_Status_Outgoing = "外出";
$i_StaffAttendance_Status_EarlyLeave = "早退";

$i_StaffAttendance_Mode = "顯示模式";
$i_StaffAttendance_Mode_List = "列出名單";
$i_StaffAttendance_Mode_Group = "分組顯示";

$i_StaffAttendance_StaffName = "職員姓名";
$i_StaffAttendance_Field_RecordDate = "紀錄日期";
$i_StaffAttendance_Field_ArrivalTime = "抵達時間";
$i_StaffAttendance_Field_DepartureTime = "離開時間";
$i_StaffAttendance_Field_Signature = "簽名";
$i_StaffAttendance_Field_InSite = "進入站";
$i_StaffAttendance_Field_OutSite = "離開站";
$i_StaffAttendance_Status_NoRecord = "沒有紀錄";
$i_StaffAttendance_Field_ArrivalAndDepartureTime = "抵達及離開時間";

$i_StaffAttendance_Sim_BackSchool = "模擬拍咭 (返校)";
$i_StaffAttendance_Sim_LeaveSchool = "模擬拍咭 (離校)";

$i_StaffAttendance_Outing_Date = "外出日期";
$i_StaffAttendance_Outing_OutTime = "出發時間";
$i_StaffAttendance_Outing_BackTime = "返回時間";
$i_StaffAttendance_Outing_FromWhere = "出發地點";
$i_StaffAttendance_Outing_Location = "目的地";
$i_StaffAttendance_Outing_Objective = "原因";
$i_StaffAttendance_Outing_Remark = "備註";
$i_StaffAttendance_Outing_ImportFormat = "
<b><u>檔案說明 : </u></b><br>
第一欄 : $i_UserLogin<br>
第二欄 : $i_StaffAttendance_Outing_Date <br>
第三欄 : $i_StaffAttendance_Outing_OutTime<br>
第四欄 : $i_StaffAttendance_Outing_BackTime<br>
第五欄 : $i_StaffAttendance_Outing_FromWhere<br>
第六欄 : $i_StaffAttendance_Outing_Location<br>
第七欄 : $i_StaffAttendance_Outing_Objective<br>
第八欄 : $i_StaffAttendance_Sim_BackSchool<br>
第九欄 : $i_StaffAttendance_Sim_LeaveSchool<br>
";

$i_StaffAttendance_DayShort="日";
$i_StaffAttendance_MonthShort="月";
$i_StaffAttendance_YearShort="年";

$i_StaffAttendance_Slot_AM = "上午";
$i_StaffAttendance_Slot_PM = "下午";
$i_StaffAttendance_Slot_WD = "整日";

$i_StaffAttendance_Leave_Date = "放假日期";
$i_StaffAttendance_Leave_Deduct_Salary = "扣薪";
$i_StaffAttendnace_Leave_TimeSlot = "時節";
$i_StaffAttendance_Leave_Type = "類別";
$i_StaffAttendance_Leave_Reason = "原因";
$i_StaffAttendance_Leave_Remark = "備註";
$i_StaffAttendance_LeaveType_AL = "年假";
$i_StaffAttendance_LeaveType_SL = "病假";
$i_StaffAttendance_LeaveType_CL = "補假";
$i_StaffAttendance_LeaveType_OL = "其他";
$i_StaffAttendance_LeaveType_AL_Short = "AL";
$i_StaffAttendance_LeaveType_SL_Short = "SL";
$i_StaffAttendance_LeaveType_CL_Short = "CL";
$i_StaffAttendance_LeaveType_OL_Short = "OL";
$i_StaffAttendance_Leave_ImportFormat = "
<b><u>檔案說明 : </u></b><br>
第一欄 : $i_UserLogin<br>
第二欄 : $i_StaffAttendance_Leave_Date <br>
第三欄 : $i_StaffAttendnace_Leave_TimeSlot (AM, PM or WD)<br>
第四欄 : $i_StaffAttendance_Leave_Type (
$i_StaffAttendance_LeaveType_AL_Short - $i_StaffAttendance_LeaveType_AL
,$i_StaffAttendance_LeaveType_SL_Short - $i_StaffAttendance_LeaveType_SL
,$i_StaffAttendance_LeaveType_CL_Short - $i_StaffAttendance_LeaveType_CL
,$i_StaffAttendance_LeaveType_OL_Short - $i_StaffAttendance_LeaveType_OL)
<br>
第五欄 : $i_StaffAttendance_Leave_Reason<br>
第六欄 : $i_StaffAttendance_Sim_BackSchool<br>
第七欄 : $i_StaffAttendance_Sim_LeaveSchool<br>
";
$i_StaffAttendance_Result_ArchivedSummary = "已整存概覽";
$i_StaffAttendance_Result_Total = "總數";
$i_StaffAttendance_Result_Ontime = "準時";
$i_StaffAttendance_Result_Absent = "缺席";
$i_StaffAttendance_Result_Late = "遲到";
$i_StaffAttendance_Result_EarlyLeave = "早退";

#########################
$i_StaffAttendance_Report_Staff_Duty = "當值";
$i_StaffAttendance_Report_Staff_Time="時間";
$i_StaffAttendance_Report_Staff_WaivedOnly="已撤回";
$i_StaffAttendance_Report_Staff_ValidOnly ="有效的";
$i_StaffAttendance_Report_Staff_Off = "休息/不需上班";

#########################

$i_StaffAttendance_ReportTitle_Weekly = "每星期出席資料表";
$i_StaffAttendance_ReportTitle_Daily = "每天出席資料表";
$i_StaffAttendance_ReportTitle_StaffMonthly = "職員每月報告";
$i_StaffAttendance_ReportTitle_Summary = "$i_StaffAttendance_Report_Summary";
$i_StaffAttendance_Report_Week = "一星期";
$i_StaffAttendance_Report_Month = "一個月";
$i_StaffAttendance_Report_Year = "一年";
$i_StaffAttendance_Report_SelfInput = "自行輸入日期";

$i_StaffAttendance_ImportFormat_CardID = "時間, $i_SmartCard_Site, 智能咭 ID";
$i_StaffAttendance_ImportFormat_UserLogin = "時間, $i_SmartCard_Site, $i_UserLogin";
$i_StaffAttendance_ImportFormat_OfflineReader = "由離線拍咭機的資料檔";
$i_StaffAttendance_ImportTimeFormat = "請把時間一項的格式設定為 YYYY-MM-DD HH:mm:ss";
$i_StaffAttendance_ImportConfirm = "如確定上例的紀錄正確, 請按匯入以完成匯入.";
$i_StaffAttendance_ImportCancel = "如資料有誤, 請按取消清除這些紀錄.";
$i_StaffAttendance_ImportCardFormat = "
<b><u>檔案說明 : </u></b><br>
第一欄 : $i_UserLogin<br>
第二欄 : $i_SmartCard_CardID <br>
第三欄 : $i_StaffAttendance_Group_ID <br>
";

$i_StaffAttendance_SelectAnother = "選擇另一位職員";
#### New version
$i_StaffAttendance_SystemSettings_OtherSettings = "選項設定";

$i_StaffAttendance_DataManagement_DailyStatus = "每日狀況";
$i_StaffAttendance_Group_InStation = "進入地點";
$i_StaffAttendance_Group_OutStation = "離開地點";

$i_StaffAttendance_DataManagement_LateList = "每日遲到名單";
$i_StaffAttendance_DataManagement_AbsentList = "每日缺席名單";
$i_StaffAttendance_DataManagement_EarlyLeaveList = "每日早退名單";
$i_StaffAttendance_DataManagement_SpecialWorkingPreset = "特別日工作設定";
$i_StaffAttendance_DataManagement_HolidayPreset = "假期設定";
$i_StaffAttendance_DataManagement_OutgoingPreset = "外出工作紀錄";
$i_StaffAttendance_DataManagement_ReasonTypeSettings = "原因類型";
$i_StaffAttendance_DataManagmenet_IntermediateRecord = "工作中出入紀錄";
$i_StaffAttendance_DataManagmenet_OTRecord="超時工作紀錄";

$i_StaffAttendance_WordTemplate_Reason = "預設原因";

$i_StaffAttendance_GroupID = "組別號碼";

##########################################
$i_StaffAttendance_Big5="繁";
$i_StaffAttendance_GB="簡";
$i_StaffAttendance_IntermediateRecord_SelectView = "選擇模式";
$i_StaffAttendance_IntermediateRecord_Date ="日期";
$i_StaffAttendance_IntermediateRecord_Staff ="職員";
$i_StaffAttendance_IntermediateRecord_Warn_Please_Select_Staff="請選擇一位職員";
$i_StaffAttendance_IntermediateRecord_Warn_Please_Select_Day="請選擇一個日期";
$i_StaffAttendance_IntermediateRecord_StaffName="職員名稱";
$i_StaffAttendance_IntermediateRecord_RecordTime="紀錄時間";
$i_StaffAttendance_OTRecord_WorkingRecord="工作紀錄";
$i_StaffAttendance_OTRecord_Redeem="扣除";
$i_StaffAttendance_OTRecord_ConfirmRedeem="確定扣除?";
$i_StaffAttendance_OTRecord_RedeemRecord="扣除紀錄";
$i_StaffAttendance_OTRecord_Outstanding="未扣除";
$i_StaffAttendance_OTRecord_RecordDate="記錄日期";
$i_StaffAttendance_OTRecord_StartTime="開始時間";
$i_StaffAttendance_OTRecord_EndTime="結束時間";
$i_StaffAttendance_OTRecord_OTmins="超時工作分鐘";
$i_StaffAttendance_OTRecord_RedeemDate="扣除日期";
$i_StaffAttendance_OTRecord_MinsRedeemed="扣除的分鐘";
$i_StaffAttendance_OTRecord_Remark="備註";
$i_StaffAttendance_OTRecord_OutstandingOTmins="未扣除的分鐘";
$i_StaffAttendance_OTRecord_DatePeriod="日期";
$i_StaffAttendance_OTRecord_DateFrom="由";
$i_StaffAttendance_OTRecord_DateTo="至";
$i_StaffAttendance_OTRecord_Warn_Please_Select_Range="請選擇日期範圍";
$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format="日期格式錯誤";
$i_StaffAttendance_OTRecord_TotalOTmins="總超時分鐘";
$i_StaffAttendance_OTRecord_TotalMinsRedeemed="總扣除分鐘";
$i_StaffAttendance_OTRecord_Warn_RedeemedMins_Out_Of_Range="扣除的分鐘超出了範圍";
$i_StaffAttendance_Special_Working_ViewALL="檢視全部";
$i_StaffAttendance_Special_Working_ViewByDate="檢視指定日期";
$i_StaffAttendance_Report_Roster="職員更表";

$i_StaffAttendance_OT_Mins_Ignored ="一般日子不當作超時工作的分鐘";

###########################################
$i_StaffAttendance_ReasonTypeName = "原因類型名稱";
$i_StaffAttendance_ReasonType = "原因類型";
$i_StaffAttendance_ReasonText="原因";
$i_StaffAttendance_Outgoing_Type = "外出類別";
$i_StaffAttendance_Warning_Select_OutgoingType="請選擇外出類別";

$i_StaffAttendance_OutgoingType1 = "不計算遲到及早退";
$i_StaffAttendance_OutgoingType2 = "不計算遲到";
$i_StaffAttendance_OutgoingType3 = "不計算早退";

$i_StaffAttendance_OutgoingType[1] = "不計算遲到及早退";
$i_StaffAttendance_OutgoingType[2] = "不計算遲到";
$i_StaffAttendance_OutgoingType[3] = "不計算早退";

$i_StaffAttendance_WordTemplate_Reason = "預設原因";
$i_StaffAttendance_GroupID = "組別號碼";

$i_StaffAttendance_Month = "月份";
$i_StaffAttendance_StaffType ="類別";

$i_staffAttendance_Date = "日期";
$i_staffAttendance_ShowBy = "顯示類別";
$i_staffAttendance_ExportFileType = "報表格式";
$i_staffAttendance_DetailEntryLogRecord = "完整出入記錄";
$i_staffAttendance_DailyEntryLogRecord = "每日出席記錄表";
$i_staffAttendance_GroupSelection = "選擇組別";
$i_staffAttendance_SelectAllGroup = "所有組別";
$i_staffAttendance_SelectNotInGroup = "不屬任何組別";
$i_staffAttendance_PleaseSpecify = "請註明";
$i_staffAttendance_OtherReason = "其他原因";
$i_staffAttendance_GroupSelectionWarning = "請選擇組別";

### Added On 16 Aug 2007 ###
$i_staffAttendance_SpecialWorking_ImportFormat = "$i_UserLogin, $i_StaffAttendance_Field_RecordDate, $i_StaffAttendance_Group_OnDuty, $i_StaffAttendance_Group_Start, $i_StaffAttendance_Group_End";
$i_staffAttendance_Holiday_ImportFormat = "$i_UserLogin, $i_StaffAttendance_Field_RecordDate, $i_StaffAttendance_ReasonTypeName, $i_StaffAttendance_ReasonText";
$i_staffAttendance_Outgoing_ImportFormat = "$i_UserLogin, $i_StaffAttendance_ReasonTypeName, $i_StaffAttendance_ReasonText, $i_StaffAttendance_Field_RecordDate, *$i_StaffAttendance_Outgoing_Type";
############################

### Added On 22 Aug 2007 ###
$i_staffAttendance_DateFormat_Warning = "請輸入正確日期";
$i_staffAttendance_SelectType_Warning = "請選擇類別";
$i_staffAttendance_TimeFormat_Warning = "請輸入正確時間";
$i_staffAttendance_ImportDutyRecordDateFormat = "請把紀錄日期一項的格式設定為 YYYY-MM-DD";
$i_staffAttendance_ImportDutyFormat = "請把需上班一項的格式設定為 Y/N";
$i_staffAttendance_IMportDutyStartFormat = "請把返工時間一項的格式設定為 HH:mm:ss";
$i_staffAttendance_ImportDutyEndFormat = "請把放工時間一項的格式設定為 HH:mm:ss";
$i_staffAttendance_OutgoingTypeFormat = "外出類別: <br><ol><li>不計算遲到及早退</ul><li>不計算遲到</ul><li>不計算早退</ul></ol>";
############################

### Added On 23 Aug 2007 ###
$i_staffAttendance_InvalidImportFormatWarning = "資料錯誤, 請檢查檔案再試";
$i_staffAttendance_Import_Row = "行數";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[1] = "內聯網帳號 錯誤";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[2] = $i_StaffAttendance_Group_OnDuty." 錯誤";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[3] = $i_general_record_date."/".$i_StaffAttendance_Group_Start."/".$i_StaffAttendance_Group_End." 錯誤";
$i_staffAttendance_Holiday_Import_ErrorMsg[1] = "內聯網帳號 錯誤";
$i_staffAttendance_Holiday_Import_ErrorMsg[2] = $i_general_record_date." 錯誤";
$i_staffAttendance_Holiday_Import_ErrorMsg[3] = $i_StaffAttendance_ReasonTypeName." 錯誤";
$i_staffAttendance_Holiday_Import_ErrorMsg[4] = $i_StaffAttendance_Holiday_warning;
$i_staffAttendance_Outgoing_Import_ErrorMsg[1] = "內聯網帳號 錯誤";
$i_staffAttendance_Outgoing_Import_ErrorMsg[2] = $i_StaffAttendance_ReasonTypeName." 錯誤";
$i_staffAttendance_Outgoing_Import_ErrorMsg[3] = $i_general_record_date." 錯誤";
$i_staffAttendance_Outgoing_Import_ErrorMsg[4] = $i_StaffAttendance_Outgoing_Type." 錯誤";
$i_staffAttendance_Outgoing_Import_ErrorMsg[5] = $i_StaffAttendance_Outing_warning;
############################

### added on 17 Jun 2008 ###
$i_StaffAttendnace_ShowReportMethod = "檢視模式";
$i_StaffAttendnace_ShowReportByHoldPage = "整頁顯示";
$i_StaffAttendnace_ShowReportByseparatePage = "分頁顯示";
###

### added on 29 Dec 2008 ###
$i_staffAttendance_all_group = "所有組別";
$i_staffAttendance_no_group = "不屬任何組別";
###

$i_Circular_System = "職員通告系統";
$i_Circular_Circular = "職員通告";
$i_Circular_Settings = "職員通告設定";
$i_Circular_Admin = "職員通告管理人員";
$i_Circular_Settings_Disable = "不使用$i_Circular_System.";
$i_Circular_Settings_AllowHelpSigning = "容許管理人員替收件人更改回條.";
$i_Circular_Settings_AllowLateSign = "容許遲交.";
$i_Circular_Settings_AllowResign = "容許收件者更改已簽回的回條.";
$i_Circular_Settings_DefaultDays = "預設簽署期限日數.";
$i_Circular_Setting_TeacherCanViewAll = "讓所有老師檢視所有通告.";
$i_Circular_AdminLevel = "管理權限";
$i_Circular_AdminLevel_Normal = "一般權限";
$i_Circular_AdminLevel_Full = "完全控制";
$i_Circular_AdminLevel_Detail_Normal = "一般權限 (可發出職員通告及觀看自己所發通告的結果)";
$i_Circular_AdminLevel_Detail_Full = "完全控制 (可發出職員通告及觀看所有通告的結果)";
$i_Circular_Description_SetAdmin = "請選擇管理人員及選取管理權限.";
$i_Circular_Management = "職員通告管理";
$i_Circular_MyIssueCircular = "我發出的職員通告";
$i_Circular_AllCircular = "所有職員通告";
$i_Circular_MyCircular = "我的職員通告";
$i_Circular_DateStart = "開始日期";
$i_Circular_DateEnd = "結束日期";
$i_Circular_CircularNumber = "通告編號";
$i_Circular_Title = "標題";
$i_Circular_Description = "內容";
$i_Circular_Issuer = "發出人";
$i_Circular_RecipientType = "適用對象類型";
$i_Circular_RecipientTypeAllStaff = "所有員工";
$i_Circular_RecipientTypeAllTeaching = "所有教學職務員工";
$i_Circular_RecipientTypeAllNonTeaching = "所有非教學職務員工";
$i_Circular_RecipientTypeIndividual = "個別有關員工/小組";
$i_Circular_Signed = "已簽回";
$i_Circular_Total = "總數";
$i_Circular_ViewResult = "查看結果";
$i_Circular_NoRecord = "暫時沒有通告";
$i_Circular_NotUseTemplate = "不使用範本";
$i_Circular_FromTemplate = "載入範本";
$i_Circular_RecipientIndividual = "個別員工";
$i_Circular_CurrentList = "現時通告清單";
$i_Circular_Attachment = "附加檔案";
$i_Circular_SetReplySlipContent = "設定回條內容";
$i_Circular_ReplySlip = "回條";
$i_Circular_Type = "類別";
$i_Circular_TemplateNotes = "(只有 <font color=green>$i_Circular_Title</font>, <font color=green>$i_Circular_Description</font>, 和 <font color=green>$i_Circular_ReplySlip</font> 會成為範本的內容.)";
$i_Circular_ListIncludingSuspend = "包括已派發及未派發";
$i_Circular_CurrentCircular = "現時通告";
$i_Circular_PastCircular = "通告紀錄";
$i_Circular_Alert_Sign = "你所填寫的回條將被呈送. 確定簽署通告並遞交回條?";
# 20090326 yatwoon eService>eCircular
$i_Circular_Alert_Sign_AddStar = "你所填寫的回條將被呈送及加上星號標記. 確定簽署通告並遞交回條?";
$i_Circular_Alert_UnStar = "確定取消這職員通告的星號標記?";
$i_Circular_Alert_AddStar = "確定加上這職員通告的星號標記?";

$i_Circular_Sign = "簽署";
$i_Circular_SignStatus = "簽署狀況";
$i_Circular_Unsigned = "未簽";
$i_Circular_Signer = "簽署人:";
$i_Circular_Editor = "代簽署人:";
$i_Circular_SignerNoColon = "簽署人";
$i_Circular_SignedAt = "簽署時間";
$i_Circular_Recipient = "收件人";
$i_Circular_SignInstruction = "請填妥以上回條, 再按簽署.";
$i_Circular_SignInstruction2 = "你可以更改以上回條的內容. 請於更改後按簽署.";
$i_Circular_Sign = "簽署";
$i_Circular_Option = "選項";
$i_Circular_NoTarget = "此通告並沒有派發給任何職員.";
$i_Circular_ViewResult = "檢視結果";
$i_Circular_TableViewReply = "表列回條內容";
$i_Circular_NoReplyAtThisMoment = "暫時沒有已簽回條";
$i_Circular_ViewStat = "統計資料";
$i_Circular_NumberOfSigned = "已簽回人數";
$i_Circular_RemovalWarning = "注意: 移除此通告會一併移除已交回的回條. 你確定要繼續?";
$i_Circular_Alert_DateInvalid = "簽署日期必須是或遲於發出日期";
$i_Circular_NotExpired = "未過期";
$i_Circular_Expired = "已過期";
$i_Circular_AllSigned = "全部已簽回";
$i_Circular_NotAllSigned = "未全部簽回";
############ New wordings for eClass IP ###############
$i_Circular_Select_Template = "選擇範本";
$i_Circular_Select_Recipient_Type = "選擇適用對象類型";
$i_Circular_Add_Circular = "增加通告";
$i_Circular_New = "新增通告";
$i_Circular_Edit = "編輯通告";

# 20090326 yatwoon eCircular Stared function
//職員通告
$eCircular["StarredCirculars"] = "有星號標記";
$eCircular["SignClose"] = "簽署及關閉視窗";
$eCircular["SignAddStar"] = "簽署及加上星號標記";
$eCircular["RemoveStar"] = "移除星號標記";
$eCircular["AddStar"] = "加上星號標記";
#######################################################

$i_CycleNew_Menu_DefinePeriods = "定義日期範圍";
$i_CycleNew_Menu_GeneratePreview = "產生預覽";
$i_CycleNew_Menu_CheckPreview = "檢查預覽";
$i_CycleNew_Menu_MakePreviewToProduction = "把預覽轉至使用";
$i_CycleNew_Alert_GeneratePreview = "你是否確定要把現時預覽清除, 再產生新預覽?";
$i_CycleNew_Alert_RemoveConfirm = "你確定要移除這個紀錄?";
$i_CycleNew_Alert_RemoveAllConfirm = "你確定要移除所有紀錄?";
$i_CycleNew_Alert_MakeToProduction = "你是否確定要把現時預覽轉至使用?";
$i_CycleNew_Prompt_DateRangeOverlapped = "各日期不能重疊.";
$i_CycleNew_Prompt_PreviewGenerated = "已產生預覽. 請檢查預覽後才轉至使用.";
$i_CycleNew_Prompt_ProductionMade = "已轉成使用.";
$i_CycleNew_Field_PeriodStart = "開始日期";
$i_CycleNew_Field_PeriodEnd = "結束日期";
$i_CycleNew_Field_PeriodType = "類型";
$i_CycleNew_Field_CycleType = "循環日類型";
$i_CycleNew_Field_PeriodDays = "循環日數";
$i_CycleNew_Field_SaturdayCounted = "計算星期六";
$i_CycleNew_Field_FirstDay = "初始日";
$i_CycleNew_Field_Date = "日期";
$i_CycleNew_Field_TxtChi = "中文顯示";
$i_CycleNew_Field_TxtEng = "英文顯示";
$i_CycleNew_field_TxtShort = "日曆顯示";
$i_CycleNew_PeriodType_NoCycle = "不計算循環日";
$i_CycleNew_PeriodType_Generation = "一般計法";
$i_CycleNew_PeriodType_FileImport = "檔案匯入";
$i_CycleNew_CycleType_Numeric = "數字 (1, 2, 3 ...)";
$i_CycleNew_CycleType_Alphabetic = "字母 (A, B, C ...)";
$i_CycleNew_CycleType_Roman = "羅馬數字 (I, II, III ...)";
$i_CycleNew_Description_FileImport = "<u><b>檔案說明:</b></u><br>
第一欄: 日期 (YYYY-MM-DD)<br>
第二欄: 中文介面的顯示 (如: 循環日 A)<br>
第三欄: 英文介面的顯示 (如: Day A)<br>
第四欄: 日曆介面的顯示及定期資源預訂等功能使用 (如: A)<br>
";
$i_CycleNew_Warning_FileImport = "在此日期內的匯入紀錄會被清除.";
$i_CycleNew_View_ImportRecords = "檢視上次匯入的紀錄";
$i_CycleNew_Export_FileImport = "下載可匯入格式";
$i_CycleNew_Prefix_Chi = "循環日 ";
$i_CycleNew_Prefix_Eng = "Day ";
$i_CycleNew_NumOfMonth = "顯示月份數目";

$i_Discipline_System = "操行紀錄系統";
$i_Discipline_System_Approval = "批核";
$i_Discipline_System_PunishCounselling = "留堂及操分調整";
$i_Discipline_System_Report = "報告";
$i_Discipline_System_Stat = "統計";
$i_Discipline_System_SchoolRules = "校規";
$i_Discipline_System_Settings = "基本設定";
$i_Discipline_System_Accumulative = "累積違規";
$i_Discipline_System_Settings_SchoolRule = "設定校規";
$i_Discipline_System_Settings_Merit = "獎勵項目及類別";
$i_Discipline_System_Settings_Punishment = "違規項目及類別";
$i_Discipline_System_Settings_MildPunishment = "輕微違規紀錄";
$i_Discipline_System_Settings_MeritType = "優點類型及批核方式";
$i_Discipline_System_Settings_DemeritType = "缺點類型及批核方式";
$i_Discipline_System_Settings_UpgradeRule = "累積升級規則";
$i_Discipline_System_Settings_SpecialPunishment = "特別處罰項目";
$i_Discipline_System_Settings_Conduct = "操行分使用規則";
$i_Discipline_System_Settings_Subscore1 =  "勤學分使用規則";
$i_Discipline_System_Settings_Calculation = "操行分計算";
$i_Discipline_System_Settings_Calculation_Conduct_Semester = "學期操行等級";
$i_Discipline_System_Settings_Calculation_Conduct_Annual = "全年操行分及等級";
$i_Discipline_System_Approval = "操行紀錄批核";

$i_Discipline_System_RuleCode = "校規編號";
$i_Discipline_System_RuleName = "校規";
$i_Discipline_System_UpgradeRule = "升級條件";
$i_Discipline_System_NeedApproval = "需要批核數目";
$i_Discipline_System_ApprovalLevel = "批核的級別";
$i_Discipline_System_WaivedBy_Prefix = "由";
$i_Discipline_System_WaivedBy_Subfix = "豁免";
$i_Discipline_System_UnwaivedBy_Prefix = "由";
$i_Discipline_System_UnwaivedBy_Subfix = "取消豁免";
$i_Discipline_System_Admin_Level1 = "訓導老師";
$i_Discipline_System_Admin_Level2 = "系統管理員";
$i_Discipline_System_Category_Merit = "獎勵類型";
$i_Discipline_System_Category_Punish = "違規類型";
$i_Discipline_System_CategoryName = "類別名稱";
$i_Discipline_System_Item_Merit = "獎勵項目";
$i_Discipline_System_Item_Punish = "違規項目";
$i_Discipline_System_ItemName = "項目名稱";
$i_Discipline_System_ItemCode = "項目編號";
$i_Discipline_System_Item_Code_Name = "項目編號/名稱";
$i_Discipline_System_Item_Code_Item_Name = "項目編號/項目名稱";
$i_Discipline_System_ConductScore = "操行分";
$i_Discipline_System_NumOfMerit = "優點 數目/類型";
$i_Discipline_System_NumOfPunish = "缺點 數目/類型";
$i_Discipline_System_SpecialPunishment = "特別處罰";
$i_Discipline_System_DetentionMinutes = "留堂時間 (分鐘)";
$i_Discipline_System_InputAward = "輸入獎勵紀錄";
$i_Discipline_System_InputPunishment = "輸入違規紀錄";
$i_Discipline_System_Add_Merit = "記功 ";
$i_Discipline_System_No_Of_Award_Punishment = "獎懲數量";
$i_Discipline_System_Quantity = "數量";
$i_Discipline_System_Add_Demerit = "記過";
$i_Discipline_System_Add_Merit_Demerit = "獎勵/罰則";
$i_Discipline_System_Event_Merit = "獎勵事項";
$i_Discipline_System_Event_Demerit = "違規事項";
$i_Discipline_System_ContactReq_Parent = "需會見家長";
$i_Discipline_System_ContactReq_Social = "需會見社工";
$i_Discipline_System_Contact_Date = "會面日期";
$i_Discipline_System_Contact_Time = "會面時間";
$i_Discipline_System_Contact_Venue = "地點";
$i_Discipline_System_Contact_PIC = "負責人";
$i_Discipline_System_Illegal_Character = "包含系統不接受的符號: /";



$i_Discipline_System_Description_Approval = "獎勵/懲罰相等或大於此數目將需要批核";
$i_Discipline_System_Message_SaveRequired = "由於其他設定已變更, 請再存儲一次";
$i_Discipline_System_general_increment = "增加";
$i_Discipline_System_general_decrement = "扣減";
$i_Discipline_System_general_date_merit = "事件日期"; # "獎勵日期";
$i_Discipline_System_general_date_demerit = "事件日期"; # "懲處日期";
$i_Discipline_System_general_record = "紀錄項目";
$i_Discipline_System_general_remark = "備註";
$i_Discipline_System_default = "通用時段";
$i_Discipline_System_customize = "類別專用時段";
$i_Discipline_System_period_setting = "使用時段";
$i_Discipline_System_new_period_setting = "新增晉段";
$i_Discipline_System_Field_Times = "超過次數";
$i_Discipline_Ssytem_Category_Item_Not_Selected = "尚未選擇紀錄類別或項目.";


$i_Discipline_System_Control_LevelSetting = "設定用戶分級名稱";
$i_Discipline_System_Control_UserACL = "指定用戶的使用級別";
$i_Discipline_System_Control_ItemLevel = "可用功能級別設定";
$i_Discipline_System_Control_AttendanceSettings = "與學生智能咭考勤的連結";
$i_Discipline_System_Control_Attend_YearSemester = "設定累計學年/學期";
$i_Discipline_System_Control_Attend_CalculationRule = "計算規則";

$i_Discipline_System_Control_Warning_LevelSetting = "第一步：設定用戶級別名稱";
$i_Discipline_System_Control_Warning_ItemLevel = "第二步：設定可用功能級別";
$i_Discipline_System_Control_Warning_UserACL = "第三步：設定指定用戶的使用級別";


$i_Discipline_System_Field_Type = "類別";
$i_Discipline_System_Field_AdminLevel = "級別";
$i_Discipline_System_Field_Personal = "個人";

$i_Discipline_System_NumOfLevel = "級別數目";
$i_Discipline_System_alert_NumLevelLess = "你選擇的數目少於現時的級別數目, 較高的級別將被移除. 是否確定要更改?";
$i_Discipline_System_alert_NeedToSave = "請注意: 新設定尚未儲存!";
$i_Discipline_System_alert_ChangeAdminLevel = "你是否確定要更改已選擇用戶的權限?";
$i_Discipline_System_alert_RecordAddSuccessful = "已新增紀錄";
$i_Discipline_System_alert_RecordAddFail = "新增紀錄失敗";
$i_Discipline_System_alert_RecordModifySuccessful = "已更新紀錄";
$i_Discipline_System_alert_RecordModifyFail = "杆新紀錄失敗";
$i_Discipline_System_alert_RecordWaitForApproval = "已新增紀錄, 並等待批核.";
$i_Discipline_System_alert_PleaseSelectStudent = "請選擇學生";
$i_Discipline_System_alert_PleaseSelectClass = "請選擇班別";
$i_Discipline_System_alert_PleaseSelectClassLevel = "請選擇級別";
$i_Discipline_System_alert_PleaseSelectDemeritRecord = "請選擇違規紀錄";
$i_Discipline_System_alert_PleaseSelectDetentionRecord = "請選擇原因";
$i_Discipline_System_alert_PleaseSelectEvent = "請選擇項目";
$i_Discipline_System_alert_PleaseSelectMeritRecord = "請選擇獎勵紀錄";
$i_Discipline_System_alert_PleaseInsertNewConductScore = "請填寫新操行分";
$i_Discipline_System_alert_PleaseInsertNewSubscore1 = "請填寫新勤學分";
$i_Discipline_System_alert_remove_warning_level = "你是否確定要移除此紀錄?";
$i_Discipline_System_alert_grading_scheme_already_exists = "<font color=red>此操行等級規則已存在</font>";
$i_Discipline_System_alert_warning_level_already_exists = "<font color=red>此提示已存在</font>";
$i_Discipline_System_alert_warning_level_not_exists = "<font color=red>此提示並不存在</font>";
$i_Discipline_System_alert_reset_conduct = "你是否確定要重設所有學生的操行分?";
$i_Discipline_System_alert_ApprovedSome = "<font color=red>部份紀錄已批核, 但仍有部份你沒有足夠權限批核</font>";
$i_Discipline_System_alert_Approved = "<font color=green>紀錄已批核</font>";
$i_Discipline_System_alert_calculate = "你是否確定要進行計算?";
$i_Discipline_System_alert_SchoolYear = "請填寫年度";
$i_Discipline_System_alert_remove_category = "此類型所包含的項目將同時被刪除, 你是否確定要刪除項目？";
$i_Discipline_System_alert_remove_record = "所有相關紀錄將一同被刪除。你是否確定要刪除？";
$i_Discipline_System_normal_teacher = "一般老師/職員";
$i_Discipline_System_alert_require_group_title = "請輸入小組名稱";
$i_Discipline_System_alert_exist_group_title = "已有該小組名稱";
$i_Discipline_System_alert_Award_Punishment_Missing = "你選擇了不對所選學生進行獎勵或懲罰。你是否確定要繼續?";

$i_Discipline_System_msg_GoBackPersonalRecord = "回到個人紀錄";
$i_Discipline_System_msg_AddAnotherRecord = "新增另一項紀錄";
$i_Discipline_System_msg_ModifyAnotherRecord = "更新更多紀錄";
$i_Discipline_System_msg_ModifyOtherRecord = "更新其他紀錄";
$i_Discipline_System_msg_GoBackApproval = "返回批核目錄";
$i_Discipline_System_field_discipline_record = "操行紀錄";
$i_Discipline_System_Report_Type_Personal = "個人報告";
$i_Discipline_System_Report_Type_AwardPunish = "獎勵/違規紀錄";
$i_Discipline_System_Report_Type_Class = "班別摘要";
$i_Discipline_System_Report_Type_PunishCounselling = "處罰及輔導報告";
$i_Discipline_System_Report_Type_Assessment = "評估項目";
$i_Discipline_System_Report_Type_Detail = "詳細報告";
$i_Discipline_System_Report_Personal_PersonalRecord = "個人紀錄";
$i_Discipline_System_Report_Personal_ConductAdjustment = "操行紀錄調整";
$i_Discipline_System_Report_AwardPunish_ClassLevel = "級別紀錄";
$i_Discipline_System_Report_AwardPunish_ClassRecord = "班別紀錄";
$i_Discipline_System_Report_AwardPunish_TodayRecord = "每天獎勵/違規紀錄";
$i_Discipline_System_Report_PunishCounselling_ParentMeeting = "家長會議";
$i_Discipline_System_Report_PunishCounselling_SocialMeeting = "輔導工作";
$i_Discipline_System_Report_PunishCounselling_PunishReport = "處罰報告";
$i_Discipline_System_Report_PunishCounselling_DetentionReport = "留堂報告";
$i_Discipline_System_Report_Assessment_Assessment = "評估項目";
$i_Discipline_System_Report_Detail_Student = "班別學生詳細報告";
$i_Discipline_System_Report_Detail_CountReport = "違規次數報告";
$i_Discipline_System_Report_Class_Total = "全班合計";
$i_Discipline_System_Field_Total = "總數";
$i_Discipline_System_Stat_Type_General = "班別操行統計";
$i_Discipline_System_Stat_Type_Award = "獎勵統計";
$i_Discipline_System_Stat_Type_Detention = "留堂統計";
$i_Discipline_System_Stat_Type_Discipline = "違規統計";
$i_Discipline_System_Stat_Type_Punishment = "處罰統計";
$i_Discipline_System_Stat_Type_Others = "其他統計";
$i_Discipline_System_Stat_General_ClassLevel = "級別統計";
$i_Discipline_System_Stat_General_Class = "班別統計";
$i_Discipline_System_Stat_General_Detail = "學生個別操行統計";
$i_Discipline_System_Stat_Award_ClassLevel = "級別統計";
$i_Discipline_System_Stat_Award_Class = "班別統計";
$i_Discipline_System_Stat_Detention_Reason_ClassLevel = "級別 - 原因統計";
$i_Discipline_System_Stat_Detention_Reason_Class = "班別 - 原因統計";
$i_Discipline_System_Stat_Detention_Subject_ClassLevel = "級別 - 科目統計";
$i_Discipline_System_Stat_Detention_Subject_Class = "班別科目統計";
$i_Discipline_System_Stat_Discipline_ClassLevel = "級別統計";
$i_Discipline_System_Stat_Discipline_Class = "班別統計";
$i_Discipline_System_Stat_Punishment_ClassLevel = "級別統計";
$i_Discipline_System_Stat_Punishment_Class = "班別統計";
$i_Discipline_System_Stat_Others_Top_Punish_Student = "違規最多的學生";
$i_Discipline_System_Stat_Others_Top_Punish_Class = "違規最多的班別";
$i_Discipline_System_Stat_Others_Top_Award_Student = "獎勵最多的學生";
$i_Discipline_System_Stat_Others_Top_Award_Class = "獎勵最多的班別";
$i_Discipline_System_PunishCouncelling_Type_PunishmentFollowup = "跟進今天的處罰事宜";
$i_Discipline_System_PunishCouncelling_Type_DetentionFollowup = "跟進今天的留堂事宜";
$i_Discipline_System_PunishCouncelling_Type_MeetingFollowup = "跟進今天的會面事宜";
$i_Discipline_System_PunishCouncelling_Type_ExportNotice = "匯出通告";
$i_Discipline_System_PunishCouncelling_Punishment_NotExecuted = "未執行處罰";
$i_Discipline_System_PunishCouncelling_Detention_TodayDetentionRecord = "今天須留堂紀錄";
$i_Discipline_System_PunishCouncelling_Detention_Attendance = "留堂出席點名";
$i_Discipline_System_PunishCouncqlling_Detention_Outstanding_Minutes = "未留堂時間";
$i_Discipline_System_PunishCouncelling_Detention_This_Minutes = "本次留堂時間";
$i_Discipline_System_PunishCouncelling_Detention_Past_Minutes = "已留堂時間";

$i_Discipline_System_Detention_Calentar_Instruction_Msg = "行事曆顯示提供本月留堂課節的概欖。你可在此為尚未編排留堂的學生編排留堂課節。";
$i_Discipline_System_Student_Detention_Calentar_Instruction_Msg = "行事曆顯示你本月的留堂安排情況。";
$i_Discipline_System_Detention_List_Instruction_Msg = "留堂課節列表顯示留堂課節之詳細資訊。你可在此為尚未編排留堂的學生編排留堂時段。";
$i_Discipline_System_Detention_Student_Instruction_Msg = "學生列表顯示學生之留堂安排情況。你可在此編排學生之留堂時段和地點。";


$i_Discipline_System_PunishCouncelling_Detention_Attendance_Manual = "留堂出席點名(人手)";
$i_Discipline_System_PunishCouncelling_Detention_New = "新增留堂紀錄";
$i_Discipline_System_PunishCouncelling_Meeting_Parent = "今天的家長會議";
$i_Discipline_System_PunishCouncelling_Meeting_Social = "今天的輔導工作";
$i_Discipline_System_PunishCouncelling_ExportNotice_Punishment = "尚未列印違規通告";
$i_Discipline_System_PunishCouncelling_ExportNotice_Award = "尚未列印獎勵通告";
$i_Discipline_System_PunishCouncelling_ExportNotice_Meeting = "尚未列印會面通告";
$i_Discipline_System_PunishCouncelling_ExportNotice_Detention = "尚未列印留堂通告";
$i_Discipline_System_PunishCouncelling_Conduct = "更改操行分";
$i_Discipline_System_PunishCouncelling_Conduct_ManualAdjustment = "手動調整操行分";

$i_Discipline_System_PunishCouncelling_Detention_SetMinutes = "設定為出席全部時間";
$i_Discipline_System_PunishCouncelling_Detention_SetAllMinutes = "設定為所有學生出席全部時間";

$i_Discipline_System_Count_Option = "遲到計算選項";
$i_Discipline_System_Count_Semester = "只計算同一學年及同一學期";
$i_Discipline_System_Count_Year = "只計算同一學年";
$i_Discipline_System_Count_All = "計算所有紀錄";
$i_Discipline_System_LateUpgrade_Rule_AddNew = "增加下一項";
$i_Discipline_System_LateUpgrade_Accumulated = "累計遲到次數";
$i_Discipline_System_LateUpgrade_Next = "若再遲到次數";
$i_Discipline_System_LateUpgrade_Detention_Minutes = "留堂(分鐘)";

$i_Discipline_System_Report_DateSelected = "已選擇日期";
$i_Discipline_System_Report_thisYear = "本年度";
$i_Discipline_System_Report_thisSem = "本學期";
$i_Discipline_System_Report_DetailType_Demerit = "違規紀錄";
$i_Discipline_System_Report_DetailType_Merit = "獎勵紀錄";
$i_Discipline_System_Report_DetailType_Merit_Demerit = "獎勵/違規紀錄";
$i_Discipline_System_Report_DetailType_SpecialPunishment = "特別處罰紀錄";
$i_Discipline_System_Report_DetailType_Detention = "留堂出席紀錄";
$i_Discipline_System_Report_DetailType_Punishment_Detention = "留堂處罰紀錄";
$i_Discipline_System_Report_DetailType_Meeting = "會面紀錄";
$i_Discipline_System_Report_ClassReport_Grade = "操行等級";
$i_Discipline_System_general_SortField = "排列次序";

$i_Discipline_System_Approval_Demerit_Waiting = "等待批核的違規紀錄";
$i_Discipline_System_Approval_Merit_Waiting = "等待批核的獎勵紀錄";
$i_Discipline_System_Approval_Amount = "數量";
$i_Discipline_System_Approval_View = "檢視";
$i_Discipline_System_Waiting_Approval_Event = "等待批核事項";


$i_Discipline_System_Conduct_Initial_Score = "底分";
$i_Discipline_System_Conduct_Warning_Point = "提示分數";
$i_Discipline_System_Conduct_List_ConductScore_Dropped = "以下學生的操行分已跌至警告水平";
$i_Discipline_System_Conduct_DroppedPassWarningPoint = "跌至警告水平";
$i_Discipline_System_Conduct_UpdatedScore = "更暘後操行分";
$i_Discipline_System_Conduct_ShowNotice = "顯示家長信";
$i_Discipline_System_Conduct_Print_Remark = "列印時連同備註";
$i_Discipline_System_Conduct_ResetAllStudents = "重設所有學生的操行分(本學期)";
$i_Discipline_System_Conduct_ManualAdjustment = "手動設定";
$i_Discipline_System_Conduct_CurrentScore = "現時操行分";
$i_Discipline_System_Subscore1_CurrentScore = "現時勤學分";
$i_Discipline_System_Conduct_ScoreDifference = "已加/減操行分";
$i_Discipline_System_Conduct_UpdatedScore = "已更新操行分";
$i_Discipline_System_Subscore1_ScoreDifference = "已加/減勤學分";
$i_Discipline_System_Conduct_AdjustTo = "調整至";
$i_Discipline_System_Conduct_ConductGradeRule = "操行等級規則";
$i_Discipline_System_Conduct_ConductGradeRule_MinScore = "操行分下限";
$i_Discipline_System_Conduct_ConductGradeRule_CalculatedGrade = "對應等級";
$i_Discipline_System_Calculation_ScoreRatio = "計算比例";

$i_Discipline_System_Subscore1 = "勤學分";
$i_Discipline_System_Subscore1_ResetAllStudents = "重設所有學生的勤學分 (本學期)";
$i_Discipline_System_alert_reset_subscore1 = "你是否確定要重設所有學生的勤學分?";
$i_Discipline_System_Settings_Calculation_SubScore1_Annual = "全年勤學分";
$i_Discipline_System_Subscore1_UpdatedScore = "更新後的勤學分";
$i_Discipline_System_Subscore1_List_SubScore_Dropped = "以下學生的勤學分已跌至警告水平";
$i_Discipline_System_PunishCouncelling_Subscore1_ManualAdjustment = "手動調整勤學分";

# added on 31 Jan 2008
$i_Discipline_System_Notice_RecordType = "種類";
$i_Discipline_System_Notice_RecordType1 = "家長信";
$i_Discipline_System_Notice_RecordType2 = "警告信";
#################

# added on 4 Feb 2008
$i_Discipline_System_Notice_Date = "日期";
$i_Discipline_System_Notice_Pending = "未發佈家長信";
$i_Discipline_System_Notice_Show = "顯示";
$i_Discipline_System_Notice_Remove = "移除";
$i_Discipline_System_Notice_Warning_Please_Select = "請選擇最少一項紀錄";
$i_Discipline_System_Notice_Alert_Remove_Notice = "請列印已選取家長信內容，此家長信紀錄將會被移除";
####################

$i_Discipline_System_Notice_AttachItem = "附加扣分事項";
$i_Discipline_System_Notice_Following = "以下是";
$i_Discipline_System_Notice_Demerit_Receord = "的違規紀錄";
$i_Discipline_System_Option_SameEvent = "同一事件";
$i_Discipline_System_Option_DifferentEvent = "不同事件";

$i_Discipline_System_Select_Student = "選擇學生";
$i_Discipline_System_Add_Record = "為學生增加紀錄";
$i_Discipline_System_Over = "超過";
$i_Discipline_System_Times = "次";
$i_Discipline_System_Conduct_Change_Log ="操行分更改紀錄";
$i_Discipline_System_SubScore1_Change_Log ="勤學分更改紀錄";

$i_Discipline_System_FromScore="分數(由)";
$i_Discipline_System_ToScore ="分數(至)";

$i_Discipline_System_New_MeritDemerit="新增紀錄";
$i_Discipline_System_Edit_MeritDemerit="更新紀錄";
$i_Discipline_System_Remove_MeritDemerit="移除紀錄";
$i_Discipline_System_Record_Already_Removed="紀錄已被移除";
$i_Discipline_System_Manual_Adjustment="手動調整";
$i_Discipline_System_System_Revised="系統修正";
$i_Discipline_System_Previous_Student="上一個學生";
$i_Discipline_System_Next_Student="下一個學生";
$i_Discipline_System_Print_WarningLetter="列印警告信";
$i_Discipline_System_WaiveStatus ="豁免狀況";
$i_Discipline_System_WaiveStatus_Waived ="豁免";
$i_Discipline_System_WaiveStatus_Waived_Prefix = "其中";
$i_Discipline_System_WaiveStatus_Waived_Subfix = "個紀錄已豁免";
$i_Discipline_System_WaiveStatus_NoWaived="沒有參加計劃";
$i_Discipline_System_WaiveStatus_InProgress="進行中";
$i_Discipline_System_WaiveStatus_Success="完成";
$i_Discipline_System_WaiveStatus_Fail="失敗";
$i_Discipline_System_WaiveStatus_ChangeStatus_Warning="重新挑戰豁免計劃，上一次的失敗紀錄將被覆蓋.繼續?";
$i_Discipline_System_RecordNotYet_Arppoved ="無法顯示家長信.紀錄尚待批核中.";
$i_Discipline_System_Waive_NoUpdate_Warning="你已選擇豁免此紀錄，已更改的資料(除備註及豁免日期外)將不會被儲存";
$i_Discipline_System_Set_Status_Warning="請選擇$i_Discipline_System_WaiveStatus";
$i_Discipline_System_Show_WaiveStatus="顯示豁免紀錄";
$i_Discipline_System_No_WaiveStatus="隱藏豁免紀錄";
$i_Discipline_System_WaiveStartDate="豁免開始日期";
$i_Discipline_System_WaiveEndDate="豁免結束日期";
$i_Discipline_System_Waive_LostDateInfo_Warning="選擇".$i_Discipline_System_WaiveStatus_NoWaived."將失去全部舊有的豁免日期資料";
$i_Discipline_System_Waive_LostEndDateInfo_Warning="選擇".$i_Discipline_System_WaiveStatus_InProgress."將失去舊有的豁免結束日期資料";
$i_Discipline_System_Please_Select_Record = "請選擇要顯示的紀錄。";

# added on 17 Jan 2008 #
$i_Discipline_System_Frontend_menu_eDiscipline = "操行紀錄";

# added on 29 Jan 2008 #
$i_Discipline_System_Conduct_KeepNotice = "儲存家長信";
########################

$iDiscipline['By'] = "由";
$iDiscipline['Period_Start'] = "由";
$iDiscipline['Period_End'] = "至";
$iDiscipline['Period_SchoolYear'] = "對應".$i_Profile_Year;
$iDiscipline['Period_Semester'] = "對應".$i_Profile_Semester;
$iDiscipline['Accumulative_Category'] = "類別";
$iDiscipline['Accumulative_Category_EditItem'] = "編輯項目";
$iDiscipline['Accumulative_Category_EditPeriod'] = "編輯時段設定";
$iDiscipline['Accumulative_Category_Name'] = "類別名稱";
$iDiscipline['Accumulative_DefaultCat_Late'] = "遲到";
$iDiscipline['Accumulative_DefaultCat_Homework'] = "欠交功課";
$iDiscipline['Accumulative_DefaultCat_Uniform'] = "校服儀容違規";
$iDiscipline['Accumulative_Category_Item_Name'] = "項目名稱";
$iDiscipline['Accumulative_Category_Homework_Intranet_Homework_List']="Intranet 科目";
$iDiscipline['Accumulative_Period_Overlapped_Warning']="各時段不能重疊";
$iDiscipline['Accumulative_Period_InUse_Warning']="無法刪除紀錄。所選之時段中，某些時段已被使用。";
$iDiscipline['Accumulative_Homework_ChangeList_Warning']="新增".$i_Homework_subject."將停止使用".$iDiscipline['Accumulative_Category_Homework_Intranet_Homework_List'];
$iDiscipline['Accumulative_Demerit_Times']="累積違規次數";
# added on 01 Sept 2008
$iDiscipline['Accumulative_Merit_Times']="累積獎勵次數";

$iDiscipline['Accumulative_Punishment']="懲罰";
$iDiscipline['Period']="時段";
$iDiscipline['Accumulative_Category_Period']="計算".$iDiscipline['Period'];
$iDiscipline['Accumulative_Category_Select_Period']="選擇時段";
$iDiscipline['Accumulative_Category_Warning_Select_Period']="請選擇時段";
$iDiscipline['Accumulative_Category_Warning_Invalid_Accumulated_Demerit']=$iDiscipline['Accumulative_Demerit_Times']."無效";
$iDiscipline['Accumulative_Select_Category']="選擇類別";
$iDiscipline['Accumulative_Please_Select_Category']="請選擇".$iDiscipline['Accumulative_Category'];
$iDiscipline['SetAll']="設定給全部";
$iDiscipline['GeneralSettings']="一般設定";
$iDiscipline['Accumulative_Punishment_Warning_Select_Category']="請選擇".$iDiscipline['Accumulative_Category'];
$iDiscipline['Accumulative_Punishment_Warning_Select_Item']="請選擇".$iDiscipline['Accumulative_Category_Item'];
$iDiscipline['Accumulative_Punishment_Warning_Select_Subject']="請選擇".$i_Homework_subject;
$iDiscipline['Warning_Enter_Integer'] ="分數必須為整數";
$iDiscipline['Accumulative_Punishment_Report']="累積違規紀錄";
# added on 3 Sept 2008
$iDiscipline['Accumulative_Reward_Report']="累積獎勵紀錄";

$iDiscipline['Accumulative_Punishment_No_Category_Period_Setting']="no linked period";
$iDiscipline['Accumulative_Period_In_Use_Warning']= "該時段已有違規紀錄，新設定將只對往後新增之紀錄有效。";
$iDiscipline['Accumulative_Category_EditPeriod_UseRule'] ="編輯時段設定 (使用升級條件)";
$iDiscipline['Accumulative_Category_NextNumDemerit']="再違規次數";
$iDiscipline['Outdated_Page_Waring']="此版面已停用, 請前往 學校行政管理工具>訓導管理>設定 進行有關設定。";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme']="計算方法";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_Details']="計算方法細節";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1']="固定方式";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_2']="累加方式";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_Undefined']="未設定";
$iDiscipline['Accumulative_Category_Period_Select_Calculation_Scheme']="選擇".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme'];
$iDiscipline['Accumulative_Category_Selected_Period']="已選擇時段";
$iDiscipline['Accumulative_Category_Period_Set_Details']="設定計算細節";
$iDiscipline['Accumulative_Category_Period_Set_Details_Warning']="必需填寫至少一種處罰方式.";
$iDiscipline['Accumulative_Category_Period_Set_Details_Warning_2']="必需填寫至少一種處罰方式.";
$iDiscipline['Management_Child_AccumulativePunishmentAlert']="累積違規通知";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_NoNeed_Alert']="不需提示";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_NA']="不適用";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_AlertCount']="累積違規次數到達以下數目便需提示";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_DateRange_Part1']="檢視自";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Notice']="通知書";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendEmailCampusmail']="傳送電子郵件/校園電郵";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendEmail']="傳送電子郵件給";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendCampusmail']="傳送「校園電郵」給";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Show_TeacherNotice']="班主任通知書";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Show_ParentNotice']="顯示家長通知書";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Select_Recipient']="請選擇收件人";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Mail_Sent']="郵件已傳送";
$iDiscipline['Reports_Child_AccumulativePunishmentReport']="累積違規報告";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_PrintAll']="列印所有班別";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Total']="共";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Item']="次";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_BlackMarkCount']="缺點次數";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Sum']="總計";
$i_Discipline_System_Merit = "記功 ";
$i_Discipline_System_Demerit = "記過";
$iDiscipline['Reports_Child_AccumulativePunishmentClassReport']="班級累積違規(".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1'].")總表";
$iDiscipline['Reports_Child_AccumulativePunishmentCategoryReport']="每天班級累積違規報告";
$iDiscipline['AccumulativePunishment_Preset_Category']="附有「<span class='tabletextrequire'>*</span>」的項目為系統預設類別，不可刪除。";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning1']="注意：更改預設類別之顯示名稱並不影響其功能。系統仍會將此類別下的違規事項視為「遲到」處理。";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning2']="注意：更改預設類別之顯示名稱並不影響其功能。系統仍會將此類別下的違規事項視為「欠交功課」處理。";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning3']="注意：更改預設類別之顯示名稱並不影響其功能。系統仍會將此類別下的違規事項視為「校服儀容違規」處理。";
$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning']="要增加操行紀錄到任何日期，該日期必須位於一\"累計時段\"內，且該時段之\"累計轉換方式\"必須已設定妥當。詳情請參閱用戶手冊。";
$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning2']="要增加操行紀錄到任何日期，該日期必須位於一\"累計時段\"內，且該時段之\"累計轉換方式\"必須已設定妥當。詳情請參閱用戶手冊。";
$iDiscipline['AccumulativePunishment_Preset_Category_Delete_Warning']="所選項目中包含不能被刪除之類別.";
$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning']="你所輸入的事項日期尚未設定進行累績違規計算，或事項日期所屬時段尚未設定計算方法細節。請前往 設定>累成積違規設定 進行設定。";
$iDiscipline['AccumulativePunishment_Period_FromDate_MustSmallerThan_ToDate']="時段之 開始日期 須早過 結束日期";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_DateRange_Part2']="起之紀錄";
$iDiscipline['AccumulativePunishment_Setting_EditCategoryProperty']="編輯類別屬性";
$iDiscipline['AccumulativePunishment_Setting_DeleteCategory']="刪除類別";
$iDiscipline['AccumulativePunishment_Import']="累積違規項目匯入";
$iDiscipline['AccumulativePunishment_Parent']="所屬違規類別/項目";
$iDiscipline['AccumulativePunishment_Report_Remark']="X/Y: 現時違規次數 / 引致處罰的違規次數。";
$iDiscipline['AccumulativePunishment_Import_FileDescription1']="$i_UserClassName, $i_ClassNumber, $i_RecordDate, ".$iDiscipline['Accumulative_Category'].", ".$iDiscipline['Accumulative_Category_Item_Name'].", ".$i_Discipline_System_general_remark;
$iDiscipline['AccumulativePunishment_Import_FileDescription2']="$i_UserLogin, $i_RecordDate, ".$iDiscipline['Accumulative_Category'].", ".$iDiscipline['Accumulative_Category_Item_Name'].", ".$i_Discipline_System_general_remark;
$iDiscipline['AccumulativePunishment_Import_Failed_Reason']="錯誤";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason1']="沒有對應的學生";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason2']="日期格式錯誤";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason3']="沒有對應的".$iDiscipline['Accumulative_Category']." / ".$iDiscipline['Accumulative_Category_Item'];
$iDiscipline['AccumulativePunishment_Import_Failed_Reason4']="沒有累積違規設定";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason5']="不可匯入遲到紀錄";
$iDiscipline['AccumulativePunishment_Input_Confirm']="你是否確定要呈送?";
$iDiscipline['DuplicatdItemCode'] = "項目編號重複";
$iDiscipline['item_code_not_found'] = "項目編號無效";
$iDiscipline['invalid_date'] = "事項日期無效";
$iDiscipline['student_not_found'] = "找不到學生資料";
$iDiscipline['MeritType'] = "獎勵項目";
$iDiscipline['Award_Punishment_Type'] = "獎懲類型";
$iDiscipline['DemeritType'] = "違規項目";
$iDiscipline['Award_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore, $i_Discipline_System_Subscore1, $i_Discipline_System_Add_Merit, ". $iDiscipline['MeritType'].", $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withSubject'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Subscore1 ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_Subject_name, $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withoutSubject'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Subscore1 ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_UserRemark";

# added on 01 Sept 2008
$iDiscipline['AccumulativeReward_Import']="累積獎勵項目匯入";

# added on 04 Sept 2008
$iDiscipline['Reports_Child_AccumulativeRewardReport']="累積獎勵報告";
$iDiscipline['Reports_Child_AccumulativeRewardClassReport']="班級累積獎勵 (".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1'].")總表";
$iDiscipline['Reports_Child_AccumulativeRewardCategoryReport']="每天班級累積獎勵報告";
$iDiscipline['Reports_Child_AccumulativeRewardReport_RewardCount']="優點次數";
$iDiscipline['AccumulativeReward_Report_Remark']="X/Y: 現時獎勵次數 / 引致優點的獎勵次數。";

# add on 08 Sept 2008
$i_Discipline_System_Conduct_Marks_Reset = "重設學生操行分";




# added by Kelvin Ho on 1 Dec 2008
$i_Discipline_System_Discipline_Conduct_JS_alert = "請輸入分數";
###########################################
## cases (added by YatWoon 20080211)
###########################################
$iDiscipline['CaseReport'] = "個案報告";
$iDiscipline['CreateReport'] = "建立報告";
$iDiscipline['SelectCaseReport'] = "選擇報告類型";
$iDiscipline['SelectedCaseReport'] = "已選擇報告類型";
$iDiscipline['ReportInfo'] = "報告設定";
$iDiscipline['CaseChoice'] = array(
									array(1, "Violation of Mobile Phone Regulations"),
									array(2, "Violation of School Regulations"),
									array(3, "Case Record (form1)"),
									array(4, "Case Record (form4)"),
									array(5, "Warning Letter"),
									array(6, "Improper School Uniform"),
);

$iDiscipline['Venue'] = "地點";
$iDiscipline['VenueChoice'] = array(
									array(1, "Classroom ([#Room#])"),
									array(2, "Corridor"),
									array(3, "Hall"),
									array(4, "New Annex"),
									array(5, "Tuck Shop"),
									array(6, "Playground"),
);

$iDiscipline['Others'] = "其他";
$iDiscipline['StudentInvolved'] = "牽涉學生";
$iDiscipline['TearchersInvolved'] = "牽涉老師";
$iDiscipline['Description'] = "詳情";
$iDiscipline['CourseOfAction'] = "處罰/跟進行動";
$iDiscipline['CourseOfActionChoice'] = array(
									array(1, "Detention"),
									array(2, "Compensation"),
									array(3, "Suspension"),
									array(4, "Verbal Warning"),
									array(5, "Record in 'Black Book'"),
									array(6, "School Services"),
									array(7, "Social Services"),
									array(8, "Inform Parents"),
									array(9, "Refer to Counseling Committee / Social Worker"),
);
$iDiscipline['UniformAction'] = array(
									array(1, "See [#ActionTeacherName#] on [#ActionDate#] at [#ActionTime#] [#ActionAPM#]"),
									array(2, "Send student home to change"),
									array(3, "Detention on [#ActionDate#]"),
);
$iDiscipline['Remarks'] = "備註";
$iDiscipline['ReferenceNo'] = "參考編號";
$iDiscipline['Reason_s'] = "選項";
$iDiscipline['MobileReasons'] = array(
									array(1, "The phone was switched on during school hours;"),
									array(2, "He brought the mobile phone without prior application;"),
									array(3, "The phone rang during the lesson or during school hours;"),
									array(4, "The mobile phone had no \"Approved Label\" issued by the Discipline Committee;"),
);
$iDiscipline['NoReasonSelected'] = "請最少選擇一個選項。";
$iDiscipline['NoProblemSelected'] = "請最少選擇一個選項。";
$iDiscipline['DatesTimes'] = "日期, 時間";
$iDiscipline['Details'] = "詳情";
$iDiscipline['Punishment'] = "處罰";
$iDiscipline['RecordInBlackBook'] = "加入黑名單";
//$iDiscipline['DetailsOffence'] = "Details of offence";
$iDiscipline['DetailsDescription'] = "詳情";
$iDiscipline['RecordDate'] = "紀錄日期";
$iDiscipline['RecordTime'] = "紀錄時間";
$iDiscipline['UniformProblem'] = array(
									array(1, "Belt"),
									array(2, "School Badge / Tie"),
									array(3, "Shoes / Socks"),
									array(4, "Coloured Singlet / Vest"),
									array(5, "Ring / Accessories"),
									array(6, "Sweater"),
									array(7, "Hair Style"),
									array(8, "School Blazer"),
									array(9, "Trousers"),
									array(10, "Jacket"),
									array(11, "Shirt"),
);
$iDiscipline['UniformProblems'] = "選項";
$iDiscipline['fromStaffList'] = "自 <b>教職員名單</b>";
$iDiscipline['CaseHandledby'] = "負責人";
$iDiscipline['FollowUp'] = "跟進";
$iDiscipline['PleaseSelectOption'] = "請最少選擇一個選項。";
$iDiscipline['PreviewReport'] = "預覽報告";
$iDiscipline['RemoveRecord'] = "你確定要刪除此紀錄？";

### added on 20080828 ###
$i_Discipline_System_Settings_Email_Notification = "電郵通知設定";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Teachers = "電郵通知 (教師)";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Parents = "電郵通知 (家長)";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Students = "電郵通知 (學生)";
$i_Discipline_System_Settings_Email_Notification_Email_ClassTeacher = "通知班主任每當學生有新違規 / 獎勵紀錄";
$i_Discipline_System_Settings_Email_Notification_Email_PIC = "通知負責教師每當有關記錄已被批核 / 拒絕 / 修改";
$i_Discipline_System_Settings_Email_Notification_Email_Parents = "發送家長信給有關家長";
$i_Discipline_System_Settings_Email_Notification_Email_Student_Record = "通知學生每當有新違規 / 獎勵紀錄";
$i_Discipline_System_Settings_Email_Notification_Email_Student_Detention = "通知學生餘下未留堂時間";
$i_Discipline_System_Record_Status_Updated = "已更新";

### added on 20080918 ###
$i_Discipline_System_ParentLetter_Title = "違規紀錄通知";
$i_Discipline_System_ParentLetter_Content1 = "敬啟者︰";
$i_Discipline_System_ParentLetter_Content2 = "貴子弟";
$i_Discipline_System_ParentLetter_Content3 = "於";
$i_Discipline_System_ParentLetter_Content4 = "違反校規";
$i_Discipline_System_ParentLetter_Content5 = "現給予懲罰";
$i_Discipline_System_ParentLetter_Content6 = "希　台端督促　貴子弟遵守校規，用心向學。";
$i_Discipline_System_ParentLetter_Content7 = "此致";
$i_Discipline_System_ParentLetter_Content8 = "貴家長";

$eDiscipline['RankingReport'] = "排名報告";
$eDiscipline['AwardPunishmentRanking'] = "獎勵及懲罰排名";
$eDiscipline['GoodconductMisconductRanking'] = "良好及違規行為排名";
$eDiscipline['StudentReport'] = "學生報告";
$eDiscipline['ClassSummary'] = "班別摘要";
$eDiscipline['MonthlyReport'] = "每月違規報告";
$eDiscipline['UniformRecords'] = "Uniform Records";
$eDiscipline['Top10'] = $eDiscipline['RankingReport'];
$eDiscipline['MisconductReport'] = "違規行為報告";
$eDiscipline['GoodConductMisconductReport'] = "良好及違規行為報告";
$eDiscipline['AwardPunishmentReport'] = "獎勵及懲罰報告";
$eDiscipline['Conduct_Grade_Report'] = "操行評級報告";

# added by marcus 3/7
$eDiscipline['Viewing'] = "訓導紀錄";
$eDiscipline['ClassRank'] = "全班排名";
$eDiscipline['ClassLevelRank'] = "全級排名";
$eDiscipline['AwardRanking'] = "獎勵排名";
$eDiscipline['GoodconductRanking'] = "良好行為排名";
$eDiscipline['Class'] = "班";
$eDiscipline['Form'] = "級";
$eDiscipline['Detention_Reason'] = "留堂原因";
$i_Discipline_List_View = "列表";
$i_Discipline_System_Student_Detention_List_Instruction_Msg= "列表顯示你的留堂安排情況";
$i_Discipline_All_Arrangement = "所有留堂安排";

$eDiscipline['no_ConductScore_MeritNum_selected'] = "沒有選擇獎懲內容及操行分, 繼續嗎?";

$eDiscipline['ReportGeneratedDate'] = "報告產生日期 : ";

###########################################

$i_eSports = "運動會系統";
$i_Sports_Select_Menu = "請從左方功能清單選出所需服務。";
$i_Sports_System = "運動會系統";
$i_Sports_menu_Settings = "設定";
$i_Sports_menu_General_Settings = "基本設定";
$i_Sports_menu_Annual_Settings = "年度事項設定";
$i_Sports_menu_Arrangement = "比賽安排";
$i_Sports_menu_Item_Settings = "項目及賽事設定";
$i_Sports_menu_Report = "瀏覽報告";
$i_Sports_menu_Participation = "參賽紀錄";
$i_Sports_menu_Settings_House = "社際設定";
$i_Sports_menu_Settings_Lane = "線道數量";
$i_Sports_menu_Settings_Score = "得分設定";
$i_Sports_menu_Settings_LaneSet = "線道設定";
$i_Sports_menu_Settings_AgeGroup = "年齡分組";
$i_Sports_menu_Settings_GroupNumber = "分組編號";
$i_Sports_menu_Settings_Participant = "運動員號碼";
$i_Sports_menu_Settings_ParticipantFormat = "運動員號碼格式設定";
$i_Sports_menu_Settings_ParticipantGenerate = "產生運動員號碼";
$i_Sports_menu_Settings_ParticipantRecord = "參賽紀錄歷";
$i_Sports_menu_Settings_Enrolment = "報名設定";
$i_Sports_menu_Settings_TrackFieldName = "項目";
$i_Sports_menu_Settings_CommonTrackFieldEvent = "一般田徑比賽";
$i_Sports_menu_Settings_TrackFieldEvent = "賽事";
$i_Sports_menu_Settings_EnrolmentLimit = "報名限額";
$i_Sports_menu_Settings_Detail = "比賽詳情";
$AllGroups = "全部組別";

$i_Sports_export_reuslt = "匯出賽事成績";

#########################################################
$i_Sports_menu_Settings_YearEndClearing = "刪除年度紀錄";
#########################################################


$i_Sports_menu_Arrangement_EnrolmentUpdate = "報名更新";
$i_Sports_menu_Arrangement_Schedule = "賽程自動安排";
$i_Sports_menu_Arrangement_Relay = "接力報名名單";
$i_Sports_menu_Report_WholeSchool = "全校統計";
$i_Sports_menu_Report_Class = "班別統計";
$i_Sports_menu_Report_House = "社際統計";
$i_Sports_menu_Report_Event = "賽項統計";
$i_Sports_menu_Report_EventRanking = "賽項排名";
$i_Sports_menu_Report_HouseGroupScore = "社際及組別分數";
$i_Sports_menu_Report_GroupChampion = "分組個人成績";
$i_Sports_menu_Report_ClassEnrolment = "班別參賽表";
$i_Sports_menu_Report_HouseEnrolment = "社際參賽表";
$i_Sports_menu_Report_ExportRecord = "匯出比賽檔案";
$i_Sports_menu_Report_ExportRaceArrangement = "匯出比賽安排";
$i_Sports_menu_Report_ExportRaceResult = "列印比賽成績紙";
$i_Sports_menu_Report_ExportAthleticNumber = "列印號碼紙";
$i_Sports_menu_Report_RaceRecord = "比賽紀錄";
$i_Sports_menu_Report_RaceTopRecord = "歷年比賽最佳紀錄";
$i_Sports_menu_Report_RaceResult = "比賽成績";
$i_Sports_menu_Participation_TrackField = "田徑成績";
$i_Sports_menu_Participation_Relay = "接力紀錄";

#added by marcus 20090713
$i_Sports_menu_Report_Best_Athlete_By_Group = "各組別最佳運動員";
$i_Sports_menu_Report_Best_Athlete_By_Class = "各班最佳運動員";

$i_Sports_field_Total_Score = "總分";
$i_Sports_field_Score = "分數";
$i_Sports_field_NumberOfLane = "線道數目";
$i_Sports_field_ScoreStandard = "分數標準";
$i_Sports_field_Rank_1 = "第一名";
$i_Sports_field_Rank_2 = "第二名";
$i_Sports_field_Rank_3 = "第三名";
$i_Sports_field_Rank_4 = "第四名";
$i_Sports_field_Rank_5 = "第五名";
$i_Sports_field_Rank_6 = "第六名";
$i_Sports_field_Rank_7 = "第七名";
$i_Sports_field_Rank_8 = "第八名";
$i_Sports_field_Lane = "線道";
$i_Sports_field_Rank = "名次";
$i_Sports_field_Grade = "級別";
$i_Sports_field_GradeName = "級別名稱";
$i_Sports_field_DOBUpLimit = "最早出生日期";
$i_Sports_field_DOBLowLimit = "最遲出生日期";
$i_Sports_field_GradeCode = "分組編號";
$i_Sports_field_Enrolment_Date_start = "開始報名日期";
$i_Sports_field_Enrolment_Date_end = "截止報名日期";
$i_Sports_field_Event_Name = "項目";
$i_Sports_field_Event_Type = "比賽類別";
$i_Sports_field_Group = "組別";
$i_Sports_field_Heat = "場次";
$i_Sports_field_Remark = "備註";
$i_Sports_field_Gender = "姓別";
$i_Sports_field_Result = "成績";
$i_Sports_Class = "班別";
$i_Sports_Time = "時間";
$i_Sports_Detail = "詳細資料";
$i_Sports_The = "第";
$i_Sports_TheLine1 = "第";
$i_Sports_TheLine2 = "線";
$i_Sports_TheGroup1 = "第";
$i_Sports_TheGroup2 = "組";
$i_Sports_Line = "線";
$i_Sports_Order = "次序";
$i_Sports_Pos = "位";
$i_Sports_Group = "組";
$i_Sports_Rank = "名";
$i_Sports_From = "從";
$i_Sports_To = "到";
$i_Sports_All = "所有項目";
$i_Sports_All_Grade = "所有級別";
$i_Sports_Color_Palette = "調色板";
$i_Sports_Color_Preview = "預覽顏色";
$i_Sports_Metres = "米";

$i_Sports_Enrolment_Warn_MaxEvent = "你最多可參與";
$i_Sports_Enrolment_MaxEvent = "最多參與項目";
$i_Sports_Enrolment_Detail = "報名詳情";
$i_Sports_Warn_Please_Select = "請選擇或輸入其中一項";
$i_Sports_Warn_OpenGroup_Error = "公開組項目不能同時設定為非公開組項目, 請重選組別.";
$i_Sports_Warn_Select_Group = "請選擇組別";
$i_Sports_Warn_Please_Fill_EventName = "請輸入比賽名稱";
$i_Sports_Record_Holder = "紀錄保持者";
$i_Sports_New_Record = "新紀錄";
$i_Sports_New_Record_Delete = "新紀錄已刪除";
$i_Sports_Record = "紀錄";
$i_Sports_Record_Year = "紀錄年份";
$i_Sports_House = "社";
$i_Sports_Standard_Record = "標準紀錄";
$i_Sports_First_Round = "初賽";
$i_Sports_Second_Round = "複賽";
$i_Sports_Final_Round = "決賽";
$i_Sports_Person_Per_Group = "每組人數";
$i_Sports_Num_Of_Group = "組別數目";
$i_Sports_Random_Order = "隨機排列";
$i_Sports_Race_Day = "比賽日期";
$i_Sports_Item = "項目";
$i_Sports_Event_Jump = "跳高";
$i_Sports_Enroled_Student_Count = "報名人數";
$i_Sports_Enrolled = "已報名";
$i_Sports_Arranged_Student_Count = "已安排人數";
$i_Sports_Participation_Count = "出席人數";
$i_Sports_Explain = "註解";
$i_Sports_Item_Without_Setting_Meaning = "表示此項目尚未設定";
$i_Sports_Not_Complete_Events_Meaning = "表示此項目尚未完結";
$i_Sports_Auto_Lane_Arrange_Reminder = "<font color=blue>*按呈送進行賽道自動安排</font>";

$i_Sports_Re_Auto_Arrange_Lane = "重新進行賽道自動安排";
$i_Sports_Arrange_Lane = "賽道安排";
$i_Sprots_Proceed_Arrange = "請按確定繼續";
$i_Sports_Stu_Num_Exceed_Lane_Num ="每組參與人數將超越賽道數目，請更改項目設定";


############################################################################
$i_Sports_Warn_Please_Select_Rank=" 請選擇同分者的名次 ";
$i_Sports_Warn_Rank_Assigned_To_Others=" 已分配給其他運動員";
$i_Sports_YearEndClearing_Msg1 = "這將會刪除舊年度紀錄";
$i_Sports_YearEndClearing_Msg2 = "<font color=red>紀錄已刪除</font>";
$i_Sports_YearEndClearing_Msg3 = "這將會:<li>刪除報名紀錄<li>更新年齡組別年份<li>更新比賽紀錄";

$i_Sports_YearEndClearing_Clear ="刪除";
$i_Sports_YearEndClearing_Continue="繼續";
############################################################################

$i_Sports_RecordBroken = "破紀錄";
$i_Sports_Qualified = "達到標準";

####################################
$i_Sports_Foul = "違規";
$i_Sports_NotAttend="缺席";
####################################

$i_Sports_Day_Enrolment = "運動會報名";
$i_Sports_Enrolment = "報名";
$i_Sports_Present = "出席";
$i_Sports_Absent = "缺席";
$i_Sports_Other = "其他";
#############################################
$i_Sports_Tie_Breaker= "選擇同分者名次";
$i_Sports_Tie_Breaker_Rank = "名次";
#####################

$i_Sports_Count = "人次";
$i_Sports_Decrement = "扣分";
$i_Sports_total = "總數";
$i_Sports_already_set = "已設定";
$i_Sports_Unsuitable= "不適用";
$i_Sports_Event_Set_OnlineEnrol = "網上報名?";
$i_Sports_Event_Set_RestrictQuota = "計算報名限額?";
$i_Sports_Event_Set_CountHouseScore = "計算社分數";
$i_Sports_Event_Set_CountClassScore = "計算班分數";
$i_Sports_Event_Set_CountIndividualScore = "計算個人分數";
$i_Sports_Event_Set_ScoreStandard = "分數標準";
$i_Sports_Event_Open_Group_Event = "公開組賽事";
$i_Sports_Event_UnrestrictQuota_Event = "不計算報名限額賽事";
$i_Sports_Event_Open = "公開組";
$i_Sports_Event_Boys_Open = "男子公開組";
$i_Sports_Event_Girls_Open = "女子公開組";
$i_Sports_Event_Mixed_Open = "混合公開組";
$i_Sports_Event_Boys = "男子組";
$i_Sports_Event_Girls = "女子組";
$i_Sports_Event_Mixed = "男女混合";
$i_Sports_Event_All = "所有";

$i_Sports_Participant = "運動員";
$i_Sports_Participant_Number = "運動員號碼";
$i_Sports_Participant_Number_Type = "類別";
$i_Sports_Participant_Number_1stRow = "第一行";
$i_Sports_Participant_Number_2ndRow = "第二行";
$i_Sports_Participant_Number_3rdRow = "第三行";
$i_Sports_Participant_Number_AutoNumLength = "順序號碼長度";
$i_Sports_Participant_Number_Generation_cond_HouseOrder = "社次序";
$i_Sports_Participant_Number_Generate = "產生運動員號碼";
$i_Sports_Participant_Number_alert_generate = "你確定要產生運動員號碼?";
$i_Sports_Participant_Number_Summary_NumberOfStudents = "學生數目";
$i_Sports_Participant_Number_Summary_NoClass  = "沒有班別";
$i_Sports_Participant_Number_Summary_NoClassNum = "沒有班號";
$i_Sports_Participant_Number_Summary_NoHouse = "沒有社";
$i_Sports_Participant_Number_Summary_MultipleHouse = "多於一個社";
$i_Sports_Participant_Number_Summary_NoGender = "沒有性別";
$i_Sports_Participant_Number_Summary_NoDOB = "沒有出生日期";
$i_Sports_Participant_Number_Summary_NoAthleticNumber = "沒有運動員號碼";
$i_Sports_AdminConsole_AccountAllowedToUse = "可以設定使用運動會系統的用戶";
$i_Sports_AdminConsole_UserTypes = "用戶類別";
$i_Sports_AdminConsole_UserType['ADMIN'] = "管理員";
$i_Sports_AdminConsole_UserType['HELPER'] = "工作人員";
$i_Sports_AdminConsole_Alert_ChangeAdminLevel = "你是否確定要更改已選擇用戶的權限?";

$i_Sports_Report_Total_Participant_Count = "參賽人數";
$i_Sports_Report_Total_Student_Count = "學生總數";
$i_Sports_Report_Enrolment_Count = "報名人次";
$i_Sports_Report_Average_Enrolment_Count = "平均報名人次";
$i_Sports_Report_Item_Name = "項目名稱";
$i_Sports_Arrange_Change_Postion = "對調位置";
$i_Sports_Record_Generate_Second_Round = "產生複賽參賽名單";
$i_Sports_Record_Generate_Final_Round = "產生決賽參賽名單";
$i_Sports_Record_Alert_Duplicate_RankNumber = "名次重複, 請重新選擇";
$i_Sports_Record_Msg_Generate_Success = "<font color=red>出賽名單已成功產生</font>";
$i_Sports_Record_Msg_Generate_Unsuccess = "<font color=red>沒有足夠參賽紀錄, 未能成功產生下一輪賽事的出賽名單</font>";
$i_Sports_Setting_Alert_FinalRound_Req = "若此項目需設定複賽, 必須同時設定決賽.";
$i_Sports_No_DOB = "出生日期尚未設定";

$i_Sports_Participation_Score = "參賽分數";
$i_Sports_Event_Detail = "賽事項目資料";
$i_Sports_Invalid_Data = "資料不正確";
$i_Sports_Edit_House_Alert = "更改社設定可能影響其他管理工具, 繼續?";

####### Add for lunch box
$i_SmartCard_Lunchbox_System = "飯盒領取管理系統";
$i_SmartCard_Lunchbox_Menu_Settings  = "系統設定";
$i_SmartCard_Lunchbox_Menu_DataInput = "資料輸入";
$i_SmartCard_Lunchbox_Menu_StatusChecking = "檢視每日狀況";
$i_SmartCard_Lunchbox_Menu_Report = "報告";
$i_SmartCard_Lunchbox_Settings_Terminal = "終端機設定";
$i_SmartCard_Lunchbox_Settings_Calendar = "每月飯盒分發日期設定";
$i_SmartCard_Lunchbox_Settings_DataClearance = "舊資料清除 (以提升效能)";
$i_SmartCard_Lunchbox_DataInput_Day = "每日名單";
$i_SmartCard_Lunchbox_DataInput_Month = "每月名單";
$i_SmartCard_Lunchbox_Sync_From_Attendance = "由今天的考勤紀錄匯入名單";
$i_SmartCard_Lunchbox_Total_LunchBox = "名單總數";
$i_SmartCard_Lunchbox_Total_Present = "出席學生總數(包括遲到)";
$i_SmartCard_Lunchbox_Sync_From_Attendance_Confirm = "你是否確定由今天的考勤紀錄匯入名單?";
$i_SmartCard_Lunchbox_Report_Generation = "報表產生";
$i_SmartCard_Lunchbox_Report_View = "報表檢視";
$i_SmartCard_Lunchbox_Warning_CannotEditPrevious = "不能更改以往的紀錄";
$i_SmartCard_Lunchbox_Warning_AlreadyHasLunchTicket = "此月份已有學生飯盒紀錄. 更改月份紀錄並<font color=red><b>不會</b></font>自動更改學生飯盒紀錄.";
$i_SmartCard_Lunchbox_Warning_Save = "你確定要儲存?";
$i_SmartCard_Lunchbox_Warning_Change = "你確定要轉變?";
$i_SmartCard_Lunchbox_Warning_MonthlyCalendarNotExist = "此月份並未設定. 因此不能新增任何學生紀錄.";
$i_SmartCard_Lunchbox_DataField_NumTickets = "已買飯日數";
$i_SmartCard_Lunchbox_DateField_Date = "日期";
$i_SmartCard_Lunchbox_Action_AddStudent = "更改名單";
$i_SmartCard_Lunchbox_Action_SetTo = "轉為";
$i_SmartCard_Lunchbox_Status_NotTaken = "未取";
$i_SmartCard_Lunchbox_Status_Taken = "已取";
$i_SmartCard_Lunchbox_Status_NoTicket = "沒有買飯";
$i_SmartCard_Lunchbox_Report_Daily = "每日報告";
$i_SmartCard_Lunchbox_Report_Month = "每月報告";
$i_SmartCard_Lunchbox_Report_BadAction = "錯誤行為";
$i_SmartCard_Lunchbox_Report_Type_Summary = "總結";
$i_SmartCard_Lunchbox_Report_Type_UntakenList = "未取飯盒名單";
$i_SmartCard_Lunchbox_Report_Type_TakenList = "已取飯盒名單";
$i_SmartCard_Lunchbox_Report_Type_StudentList = "所有學生名單";
$i_SmartCard_Lunchbox_Report_Type_DayBreakdown = "每日分析";
$i_SmartCard_Lunchbox_Report_Type_TopUntaken = "最多次未取飯盒名單";
$i_SmartCard_Lunchbox_Report_Type_BadAction_NoTicket = "沒有買飯, 但嘗試取飯";
$i_SmartCard_Lunchbox_Report_Type_BadAction_TakeAgain = "嘗試再次取飯";
$i_SmartCard_Lunchbox_Report_Param_Type = "報告類型";
$i_SmartCard_Lunchbox_Report_Param_SplitClass = "分隔班別";
$i_SmartCard_Lunchbox_Report_Param_SelectClass = "選擇班別";
$i_SmartCard_Lunchbox_Report_Lang_AllClasses = "所有班別";
$i_SmartCard_Lunchbox_Report_Field_Untaken_Times = "未取次數";
$i_SmartCard_Lunchbox_Report_Field_Untaken_Days = "未取日期";
$i_SmartCard_Lunchbox_Report_Words_NoUntaken = "沒有學生未取飯盒";
$i_SmartCard_Lunchbox_Report_Words_NoTaken = "沒有學生已取飯盒";
$i_SmartCard_Lunchbox_Report_Words_NoTicket = "沒有學生已買飯盒";
$i_SmartCard_Lunchbox_Import_Format_Login = "以內聯網帳號";
$i_SmartCard_Lunchbox_Import_Format_ClassNumber = "以班別班號";
$i_SmartCard_Lunchbox_Import_From_LastMonth = "從上月匯入";
$i_SmartCard_Lunchbox_MonthTarget = "從何月份匯入";
$i_SmartCard_Lunchbox_TargetMonthNumStudents = "上述月份已買飯盒的學生";
$i_SmartCard_Lunchbox_NewMonth_Days = "新月份飯盒日數";
$i_SmartCard_Lunchbox_Import_From_LastMonth_Description = "";


# yat 20090625
$i_eSwimmingGala = "水運會系統";
$i_swimming_gala = "水運會";
$i_Swimming_Gala_Enrolment = "水運會報名";


$i_House = "社";
$i_House_name = "社名";
$i_House_GroupLink = "內聯網小組";
$i_House_ColorCode = "社顏色";
$i_House_HouseCode = "社代號";


$i_Community_Communities = "群組";
$i_Community_MyCommunities = "我的群組";
$i_Community_CommunityDirectory = "群組組織";
$i_Community_CommunityFunction_Settings = "設定";
$i_Community_Community_GoTo = "轉至";
$i_Community_EventAll = "所有";
$i_Community_EventUpcoming = "即將來臨";
$i_Community_EventPast = "昔日";


$i_ServiceMgmt_System = "時數計劃管理"; #"服務時數管理系統";
$i_ServiceMgmt_System_Settings = $i_ServiceMgmt_System."設定";
$i_ServiceMgmt_System_Admin = "系統使用者及使用權設定";
$i_ServiceMgmt_System_Description_SetAdmin = "請選擇可以使用本系統的用戶及權限.";
$i_ServiceMgmt_System_AccessLevel = "權限";
$i_ServiceMgmt_System_AccessLevel_Normal = "一般用戶";
$i_ServiceMgmt_System_AccessLevel_High = "管理員";
$i_ServiceMgmt_System_AccessLevel_Detail_Normal = "$i_ServiceMgmt_System_AccessLevel_Normal (允許資料輸入及檢視報告)";
$i_ServiceMgmt_System_AccessLevel_Detail_High = "$i_ServiceMgmt_System_AccessLevel_High (允許更改設定及學生要求)";

$i_ServiceMgmt_System_Menu_Report = "服務時數報表";
$i_ServiceMgmt_System_Menu_ClassReport = "服務時數一覽表";
$i_ServiceMgmt_System_Menu_InputNew = "新增活動工作時數";
$i_ServiceMgmt_System_Menu_PresetActivity_Attend = "輸入預設活動出席情況";
$i_ServiceMgmt_System_Menu_PresetActivity_Report = "預設活動報表";
$i_ServiceMgmt_System_Menu_PresetActivity_Input = "預設活動設定";
$i_ServiceMgmt_System_Menu_Settings_Type = "活動類型設定";
$i_ServiceMgmt_System_Menu_Settings_Student = "學生要求設定";

$i_ServiceMgmt_System_NotSet = "未設定";
$i_ServiceMgmt_System_Field_HoursRequired = "工作時數";
$i_ServiceMgmt_System_Field_ActivityCategory = "活動類型";
$i_ServiceMgmt_System_TargetClass = "欲更改要求的班別";
$i_ServiceMgmt_System_Warning_Numeric = "請輸入數字";

$i_ServiceMgmt_System_Hours_Type1_Name = "校內";
$i_ServiceMgmt_System_Hours_Type2_Name = "校外";
$i_ServiceMgmt_System_Hours_Type3_Name = "其他";

$i_ServiceMgmt_System_ActivityCategory = "活動類型";

$i_PresetActivity_ActivityName = "活動名稱";
$i_PresetActivity_ActivityCode = "活動編號";
$i_PresetActivity_ActivityCategory = "活動類型";
$i_PresetActivity_ActivityNature = "性質";
$i_PresetActivity_ActivityDate = "活動日期";
$i_PresetActivity_ActivityTime = "活動時間";
$i_PresetActivity_LevelName = "摘星計劃種類";
$i_PresetActivity_PIC = "負責人";
$i_PresetActivity_StudentSelect = "已選學生";
$i_PresetActivity_HoursReq = "所需時數";
$i_PresetActivity_HoursComplete = "完成時數";
$i_PresetActivity_HoursLeft = "未完成時數";
$i_PresetActivity_DateStart = "開始日期";
$i_PresetActivity_DateEnd = "完成日期";

$i_ServiceMgmt_System_Class_Settings = "班級設定";

$i_Extra_SocialNumber = "社署編號";
$i_Extra_Manage_SocialNumber = "管理$i_Extra_SocialNumber";


################################################################################################
$iDiscipline['Management'] = "管理";
$iDiscipline['Management_Child_AwardInput'] = "輸入獎勵紀錄";
$iDiscipline['Management_Child_AwardImport'] = "匯入獎勵紀錄";
$iDiscipline['Management_Child_AwardApproval'] = "批核獎勵紀錄";
$iDiscipline['Management_Child_PunishmentInput'] = "輸入違規紀錄";
$iDiscipline['Management_Child_PunishmentImport'] = "匯入違規紀錄";
$iDiscipline['Management_Child_PunishmentApproval'] = "批核違規紀錄";
$iDiscipline['Management_Child_PunishmentCounselling'] = "留堂及操行分紀錄";
$iDiscipline['Management_Child_AccumulativePunishment'] = "輸入累積違規紀錄";
# added on 01 Sept 2008
$iDiscipline['Management_Child_AccumulativeReward'] = "輸入累積獎勵紀錄";
# added on 31 Jan 2008
$iDiscipline['Management_Child_PendingNoticeRecord'] = "未發佈家長信紀錄";
# added on 8 Sept 2008
$iDiscipline['Management_Child_Personal'] = "個人紀錄";
# added on 9 Sept 2008
$iDiscipline['Detention_Attendance_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber,$i_Discipline_System_PunishCouncelling_Detention_This_Minutes";
#####################

$iDiscipline['Reports'] = "報告";
$iDiscipline['Reports_Child_PersonalReport'] = $i_Discipline_System_Report_Type_Personal;
$iDiscipline['Reports_Child_AwardPunishmentRecord'] = "獎勵/違規報告";
$iDiscipline['Reports_Child_DetailReport'] = $i_Discipline_System_Report_Type_Detail;
$iDiscipline['Reports_Child_ClassBasedReport'] = $i_Discipline_System_Report_Type_Class;

$iDiscipline['Statistics'] = "統計";
$iDiscipline['Statistics_Child_GeneralStats'] = $i_Discipline_System_Stat_Type_General;
$iDiscipline['Statistics_Child_DisciplineStats'] = $i_Discipline_System_Stat_Type_Discipline;
$iDiscipline['Statistics_Child_AwardStats'] = $i_Discipline_System_Stat_Type_Award;
$iDiscipline['Statistics_Child_DetentionStats'] = $i_Discipline_System_Stat_Type_Detention;
$iDiscipline['Statistics_Child_OtherStats'] = $i_Discipline_System_Stat_Type_Others;

$iDiscipline['Settings'] = "設定";
$iDiscipline['Settings_Child_Settings'] = $i_Discipline_System_Settings;
$iDiscipline['Settings_Child_Calculation'] = $i_Discipline_System_Settings_Calculation;
$iDiscipline['Settings_Child_Accumulative'] = "累積違規";
$iDiscipline['Settings_Child_Accumulative_Reward'] = "累積獎勵";

$button_addtoview = "加入檢視";
$iDiscipline['select_students'] = "選擇學生";
$iDiscipline['class'] = "班別";
$iDiscipline['students'] = "學生";
$iDiscipline['warning_format'] = "學生";

$iDiscipline['compare_different_periods'] = "不同時期的比較";
$iDiscipline['Accumulative_Setting_Period'] = "設定計算時段";
$iDiscipline['Accumulative_NO_Setting_Period'] = "今天尚未設定時段";
$iDiscipline['Accumulative_Setting_Category'] = "類別及項目";

##################################################################################################################
# eDiscipline v1.2 [REVISED - START]

$eDiscipline['Overview'] = "概覽";
$eDiscipline['Management'] = "管理";
$eDiscipline['Award_and_Punishment'] = "獎勵及懲罰";
$eDiscipline['Good_Conduct_and_Misconduct'] = "良好及違規行為";
$eDiscipline['Case_Record'] = "個案紀錄";
$eDiscipline['Conduct_Mark'] = "操行分";
$eDiscipline['Conduct_Grade'] = "操行評級";
$eDiscipline['Detention'] = "留堂";
$eDiscipline['Statistics'] = "統計";
$eDiscipline['Reports'] = "報告";
$eDiscipline['Settings'] = "設定";
$eDiscipline['eNoticeTemplate'] = "電子通告模板";
$eDiscipline['Access_Right'] = "權限";
$eDiscipline['General_Settings'] = "系統屬性";
$iDiscipline['teacher'] = "老師";
$iDiscipline['parent'] = "家長";
$i_general_name = "姓名";
$i_general_class = "班別";
$i_general_are_you_sure = "確定?";
$i_general_please_select = "請選擇";
$i_Discipline_System_CategoryList = "類別列表";
$i_Discipline_System_NewCategory = "新增類別";
$i_Discipline_System_Not_Yet_Assigned = "未被編排";

# Award & Punishment
$i_Discipline_Last_Updated = "最近更新";
$i_Discipline_Last_Updated_Str = "最近由 %NAME% 於 %DATE% 更新";
$i_Discipline_System_Conduct_School_Year = "年度";
$i_Discipline_System_Conduct_Semester = "學期";
$eDiscipline["BasicInfo"] = "基本資訊";
$eDiscipline["RecordList"] = "紀錄列表";
$eDiscipline["RecordDetails"] = "紀錄明細";
$eDiscipline["Record"] = "紀錄";
$eDiscipline["AwardPunishmentQty"] = "獎懲數量";
$eDiscipline["Type"] = "類型";
$eDiscipline["Award_Punishment_Type"] = "獎勵/懲罰項目";
$eDiscipline["Award_Punishment_Type2"] = "獎罰類別　";
$eDiscipline["Award_Punishment"] = "獎勵/懲罰內容";
$eDiscipline["EventDate"] = $i_RecordDate;
$i_Discipline_System_Award_Punishment_Reference = "參考資訊";
$eDiscipline["Actions"] = "跟進行動";
$iDiscipline['Accumulative_Category_Item'] = "項目";
$eDiscipline["PreviewNotice"] = "預覽通告";
$eDiscipline["Status"] = "狀況";
$eDiscipline["ApprovedBy"] = "由";
$eDiscipline["ApprovedBy2"] = "於";
$eDiscipline["ApprovedBy3"] = "批核";
$eDiscipline["ReleasedBy"] = "由";
$eDiscipline["ReleasedBy2"] = "於";
$eDiscipline["ReleasedBy3"] = "開放";
$eDiscipline["RejectedBy"] = "由";
$eDiscipline["RejectedBy2"] = "於";
$eDiscipline["RejectedBy3"] = "拒絕";
$eDiscipline["WaivedBy"] = "由";
$eDiscipline["WaivedBy2"] = "於";
$eDiscipline["WaivedBy3"] = "豁免";
$eDiscipline["EditRecordInfo"] = "編輯此紀錄";
$i_Discipline_System_Discipline_Case_Record_Case = "個案";
$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete = "你是否確定要刪除?";
$eDiscipline["SelectStudents"] = "選擇學生";
$eDiscipline["AddRecordToStudents"] = "新增紀錄";
$eDiscipline["SelectActions"] = "選擇跟進行動";
$eDiscipline["FinishNotification"] = "完成並發送通知";
$eDiscipline["DisciplineDetails"] = "個案資訊";
$eDiscipline["DisciplineDetails2"] = "個案明細";
$eDiscipline["RecordItem"] = "原因";
$eDiscipline["Award_Punishment_RecordItem"] = "獎懲項目";
$eDiscipline["SelectRecordCategory"] = "選擇良好/違規行為類別";
$eDiscipline["SelectRecordCategory2"] = "選擇獎懲類別";
$eDiscipline["SelectRecordCategory3"] = "選擇獎懲類別";
$eDiscipline["SelectRecordItem"] = "選擇獎懲項目";
$eDiscipline["SelectRecordItem2"] = "選擇獎懲項目";
$eDiscipline["MeritDemeritContent"] = "獎懲內容";
$eDiscipline["Action"] = "跟進行動";
$eDiscipline["SendNoticeWhenReleased"] = "在開放予學生時，發送電子通告";
$eDiscipline["GlobalSettingsToAllStudent"] = "通用設定(套用至以下所有學生)";
$i_Discipline_Apply_To_All = "套用至所有紀錄";
$i_Discipline_Detention_Times = "留堂次數";
$i_Discipline_Session = "時段";
$i_Discipline_Session2 = "留堂課節";
$i_Discipline_Auto_Assign_Session = "自動編排時段";
$i_Discipline_Template = "模板";
$i_Discipline_Usage_Template = "原因/模板";
$i_Discipline_Additional_Info = "附加資訊";
$eDiscipline["SelectTemplate"] = "選擇模板";
$i_Discipline_PIC = "負責人";
$eDiscipline["Notification"] = "通知";
$eDiscipline["EmailTo"] = "發送電郵給";
$eDiscipline["ApproveRelease"] = "批准並開放";
$i_Discipline_System_Access_Right_Release = "開放";
$eDiscipline["SendNotice"] = "發放通告";
$i_Discipline_Not_Yet = "尚未點名";
$i_Discipline_Arranged_Session = "已編訂時段";
$i_Discipline_Attendance = "點名";
$eDiscipline["WaiveRecord"] = "豁免記錄";
$eDiscipline["NewLeafRecord"] = "更新計劃記錄";
$eDiscipline["NewLeaf_Rehabilitation_Record"] = "「更新計劃」建議豁免記錄";
$eDiscipline["SelectedRecord"] = "已選取紀錄";
$eDiscipline["WaiveReason"] = "豁免原因";
$eDiscipline["HISTORY"] = "經過";
$i_Discipline_System_Award_Punishment_Submenu_Category_Item = "類別及項目";
$i_Discipline_System_Award_Punishment_Submenu_Approval = "批核";
$i_Discipline_System_Award_Punishment_Submenu_Promotion = "轉換";
$i_Discipline_System_Award_Punishment_Move_Up = "上移";
$i_Discipline_System_Award_Punishment_Move_Down = "下移";
$i_Discipline_System_Award_Punishment_Move_Top = "移至最高";
$i_Discipline_System_Award_Punishment_Move_Bottom = "移至最低";
$i_Discipline_System_Award_Punishment_No_of_Item = "項目數目";
$i_Discipline_System_Award_Punishment_Award_Category = "獎勵類別";
$i_Discipline_System_Award_Punishment_Punish_Category = "懲罰類別";
$i_Discipline_System_Award_Punishment_Setting_Category = "使用類別獨立設定";
$i_Discipline_System_Award_Punishment_Setting_Global = "使用通用設定";
$i_Discipline_System_Award_Punishment_Common_Setting = "通用設定";
$i_Discipline_System_Award_Punishment_Conversion_Method_List = "轉換方式列表";
$i_Discipline_System_Award_Punishment_Conversion_Method = "轉換方式";
$i_Discipline_System_Award_Punishment_Equivalent_Point_Msg = "*對應分數顯示要獲得獎勵所需的Good Point";
$i_Discipline_System_Award_Punishment_Search_Alert = "請輸入內容";
$i_Discipline_System_Award_Punishment_Equivalent_Point = "對應分數 *";
$i_Discipline_System_Award_Punishment_All_School_Year = "所有年度";
$i_Discipline_System_Award_Punishment_Whole_Year = "全年";
$i_Discipline_System_Award_Punishment_All_Classes = "所有班別";
$i_Discipline_System_Award_Punishment_Warning = "你可以從\"選擇狀況\"下拉選單，選擇檢視特定狀況的紀錄。";
$i_Discipline_System_Award_Punishment_All_Records = "所有紀錄";
$i_Discipline_System_Award_Punishment_Awards = "獎勵紀錄";
$i_Discipline_System_Award_Punishment_Punishments = "懲罰紀錄";
$i_Discipline_System_Award_Punishment_Change_Status = "更改狀況";
$i_Discipline_System_Award_Punishment_Select_Status = "選擇狀況";
$i_Discipline_System_Award_Punishment_Reference = "參考資訊";
$i_Discipline_System_Award_Punishment_Pending = "等待批核";
$i_Discipline_System_Award_Punishment_Wait_For_Waive = "等待豁免";
$i_Discipline_System_Award_Punishment_Wait_For_Release = "等待開放予學生";
$i_Discipline_System_Award_Punishment_Approved = "已批核";
$i_Discipline_System_Award_Punishment_Rejected = "已拒絕";
$i_Discipline_System_Award_Punishment_Released = "已開放";
$i_Discipline_System_Award_Punishment_UnReleased = "未開放";
$i_Discipline_System_Award_Punishment_ProcessedBy = "已由";
$i_Discipline_System_Award_Punishment_Processed = "處理完畢。";
$i_Discipline_System_Award_Punishment_Change_To_Processed = "轉換狀況為'已處理'";
$i_Discipline_System_Award_Punishment_Change_To_Processing = "轉換狀況為'處理中'";
$i_Discipline_System_Award_Punishment_Change_To_Release = "轉換狀況為'開放'";
$i_Discipline_System_Award_Punishment_Change_To_Unrelease = "轉換狀況為'未開放'";
$i_Discipline_System_Award_Punishment_Waived = "已豁免";
$i_Discipline_System_Award_Punishment_Locked = "已鎖定";
$i_Discipline_System_Award_Punishment_Redeemed = "功過相抵";
$i_Discipline_System_Award_Punishment_Approve = "批核";
$i_Discipline_System_Award_Punishment_Reject = "拒絕";
$i_Discipline_System_Award_Punishment_Release = "開放";
$i_Discipline_System_Award_Punishment_UnRelease = "取消開放";
$i_Discipline_System_Award_Punishment_Waive = "豁免";
$i_Discipline_System_Award_Punishment_Redeem = "功過相抵";
$i_Discipline_System_Award_Punishment_Detention = "留堂";
$i_Discipline_System_Award_Punishment_Send_Notice = "發放通告";
$i_Discipline_System_Award_Punishment_Convert_To = "轉換成";
$i_Discipline_System_Award_Punishment_Event_Date = "發生日期";

$i_Discipline_System_GoodConduct_Misconduct_Waive_Date_Count = "逾豁免期日數";
$i_Discipline_System_GoodConduct_Misconduct_Every = "每";
$i_Discipline_System_GoodConduct_Misconduct_Instance_Of = "次的";
$i_Discipline_System_GoodConduct_Misconduct_Will_Be_Count_As = "計算為";
$i_Discipline_System_GoodConduct_Misconduct_Category_To_Subcategory = "類別下的";
$i_Discipline_System_GoodConduct_Misconduct_Wait_For_NewLeaf = "等待透過「更新計劃」進行豁免";

$i_Discipline_System_Insert_From_PPC = "從電子手帳輸入";

$i_Discipline_GoodConduct = "良好行為";
$i_Discipline_Misconduct = "違規行為";
$i_Discipline_Conduct_Grade = "操行等級";
$iDiscipline['Confirmation'] = "確定";
$iDiscipline['ConductCategoryItem'] = "行為類別/項目";
$iDiscipline['SelectBehaviourItem'] = "-- 選擇行為項目 --";
$eDiscipline["CompareShow"] = "比較/顯示";


# Case Record
$i_Discipline_System_Discipline_Case_Record_Case_List = "個案列表 ";
$i_Discipline_System_Discipline_Case_Record_Case_Details = "個案明細";
$i_Discipline_System_Access_Right_Case_Record = "個案紀錄";
$i_Discipline_System_Case_Record_Case_Info = "個案資訊";
$i_Discipline_System_Case_Record_Student_Involved = "相關學生";
$i_Discipline_System_Discipline_Case_Record_Case_Number = "個案編號";
$i_Discipline_System_Discipline_Case_Record_Case_Title = "個案名稱";
$i_Discipline_System_Discipline_Case_Record_Category = "類別";
$i_Discipline_System_Discipline_Case_Record_Case_School_Year = "年度";
$i_Discipline_System_Discipline_Case_Record_Case_Semester = "學期";
$i_Discipline_System_Discipline_Case_Record_Case_Event_Date = "發生日期";
$i_Discipline_System_Discipline_Case_Record_Case_Location = "地點";
$i_Discipline_System_Discipline_Case_Record_Case_PIC = "負責人";
$i_Discipline_System_Discipline_Case_Record_Attachment = "附件";
$i_Discipline_System_Discipline_Case_Record_Processing = "處理中";
$i_Discipline_System_Discipline_Case_Record_Processed = "已處理";
$i_Discipline_System_Discipline_Case_Record_Case_Number_JS_alert = "請輸入個案編號";
$i_Discipline_System_Discipline_Case_Record_Case_Number_Exists_JS_alert = "個案編號已存在";
$i_Discipline_System_Discipline_Case_Record_Case_Title_JS_alert = "請輸入個案名稱";
$i_Discipline_System_Discipline_Case_Record_Category_JS_alert = "請輸入類別";
$i_Discipline_System_Discipline_Case_Record_Year_JS_alert = "請輸入年度";
$i_Discipline_System_Discipline_Case_Record_Event_Date_JS_alert = "請輸入記錄日期";
$i_Discipline_System_Discipline_Case_Record_Location_JS_alert = "請輸入地點";
$i_Discipline_System_Discipline_Case_Record_PIC_JS_alert = "請輸入負責人";
$i_Discipline_System_Discipline_Case_Record_PIC = "負責人";
$i_Discipline_System_Case_Record_New_Student = "新增學生";

$i_Discipline_System_Ranking_Report_Msg = "排名報告顯示獲獎勵或懲罰最多的級別或班別。請設定報告時段、對象、及排名方式。";
$i_Discipline_System_Ranking_Goodconduct_Misconduct_Report_Msg = "排名報告顯示良好或違規行為最多的級別、班別或學生。請設定報告時段、對象、及排名方式。";
$i_Discipline_System_Case_Record_Delete_Msg = "刪除個案紀錄時，所有相關學生的獎懲紀錄將會被一併刪除。你是否確定要刪除所選的個案紀錄?";

$i_Discipline_All_Classes = "所有班別";
$i_Discipline_System_Award_Punishment_Approved = "已批核";
$i_Discipline_System_Award_Punishment_Rejected = "已拒絕";
$i_Discipline_System_Award_Punishment_Waived = "已豁免";
$i_Discipline_System_Award_Punishment_Released = "已開放";
$i_Discipline_System_Discipline_Case_Record_CaseNumber = "個案編號";
$i_Discipline_System_Discipline_Case_Record_days_ago = "日前";
$i_Discipline_System_Discipline_Case_Record_New_Case = "新增個案";
$eDiscipline["CaseIsFinished"] = "<font color=red>個案已完成</font>";
$eDiscipline["RecordIsWavied"] = "<font color=red>紀錄已被豁免</font>";
$eDiscipline["RecordIsRejected"] = "<font color=red>紀錄已被拒絕</font>";
$eDiscipline["RecordisGoodConduct"] = "<font color=red>紀錄是".$i_Discipline_GoodConduct.".</font>";
$eDiscipline["RecordNotPassWaiveDate"] = "<font color=red>紀錄未過豁免期</font>";
$eDiscipline["RecordHappenAgain"] = "<font color=red>豁免期內重犯</font>";
$eDiscipline["RecordNotEnoughNewLeaf"] = "<font color=red>未有足夠改過遷善的限額.</font>";
$eDiscipline["RecordNoNewLeafSetting"] = "<font color=red>未有改過遷善的設定</font>";
$eDiscipline["RecordIsPending"] = "<font color=red>紀錄正等待批核</font>";
$eDiscipline["RecordIsCase"] = "<font color=red>紀錄屬於個案紀錄。</font>";
$eDiscipline["RecordIsRedeemed"] = "<font color=red>紀錄已功過相抵。</font>";
$eDiscipline["RecordIsAcc"] = "<font color=red>紀錄由良好/違規行為紀錄組成。</font>";
$eDiscipline["noDeleteRight"] = "<font color=red>沒有刪除權限。</font>";
$eDiscipline["RejectRecord"] = "拒絕紀錄";
$eDiscipline["ApproveRecord"] = "批核紀錄";
$eDiscipline["RecordIsApproved"] = "<font color=red>紀錄已被批核</font>";

# Good Conduct & Misconduct
$eDiscipline_Settings_GoodConduct = "良好行為";
$eDiscipline_Settings_Misconduct = "違規行為";
$eDiscipline["SelectConductCategory"] = "選擇行為類別";
$eDiscipline["SelectConductItem"] = "選擇行為";
$eDiscipline["RecordCategoryItem"] = "獎懲類別/項目";
$eDiscipline["Category_Item"] = "類別/項目";
$eDiscipline["Category_Item2"] = "類別/項目";

# 20090401 yatwoon
$eDiscipline["MissingSemesterPromptMsg_WithRight"] = "系統管理員之前曾更改學期更新模式至\"用戶自行更新\"，導致系統無跟依據日期資料判斷各累計時段所屬之學期。你需要重新為所有累計時段指定所屬學期。在此之前，用戶將無法存取 管理>良好及違規 頁面。";
$eDiscipline["MissingSemesterPromptMsg_WithoutRight"] = "由於學期更新模式已由\"系統自行更新\"更改為\"用戶自行更新\"，系統無法跟據日期資料判斷各累計時段所屬之學期。你需要重新為所有累計時段指定所屬學期。在此之前，你將無法存取 管理>良好及違規 頁面。你可以聯絡你的系統管理員，了解學期更新模式的變更。";

# 20090513 Henry
$eDiscipline["PPC_Input_Award_Record"] = "輸入獎勵紀錄";
$eDiscipline["PPC_Input_Punishment_Record"] = "輸入懲罰紀錄";
$eDiscipline["PPC_Input_Goodconduct_Record"] = "輸入良好行為紀錄";
$eDiscipline["PPC_Input_Misconduct_Record"] = "輸入違規行為紀錄";


# Conduct Mark Management
$i_Discipline_System_Conduct_Mark_Current_Conduct_Mark = "現時操行分";
$i_Discipline_System_Conduct_Mark_Pre_Adjustment = "調整前";
$i_Discipline_System_Conduct_Mark_Acc_Adjustment = "累積調整";
$i_Discipline_System_Conduct_Mark_Current = "現時操行分";
$i_Discipline_System_Discipline_Conduct_Mark_Adjustment = "調整";
$i_Discipline_System_Discipline_Conduct_Mark_Adjusted_Conduct_Mark = "調整後的操行分";
$i_Discipline_System_Discipline_Conduct_Last_Updated = "最近更新";
$i_Discipline_System_Discipline_Conduct_Mark_Generate_Conduct_Grade = "計算操行評級";
$i_Discipline_System_Access_Right_Adjust = "調整";
$i_Discipline_Reason = "原因";
$i_Discipline_Reason2 = "獎懲內容";
$i_Discipline_System_Discipline_Conduct_This_Adjustment = "本次調整";
$i_Discipline_System_Discipline_Conduct_Last_Generated = "上一次計算";
$i_Discipline_System_Discipline_Conduct_Last_Adjustment = "最近一次調整";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction1 = "使用指引";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction2 = "請選擇要計算的年度及學期。";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction3 = "一旦進行計算，所有先前的計算結果將被覆寫。";
$i_Discipline_System_Discipline_Conduct_Mark_Weighting = "操行分權重";
$i_Discipline_System_Discipline_Conduct_Mark_Weight = "比重";
$i_Discipline_System_Discipline_Conduct_Mark_Weight_Total = "總共";
$i_Discipline_System_Discipline_Conduct_Mark_Weight_Semester = "學期";

# Conduct Mark Setting
$i_Discipline_System_Conduct_Warning_Rules = "警告規則";
$i_Discipline_System_Conduct_Base_Mark = "操行分底分";
$i_Discipline_System_Conduct_Grading_Scheme = "評級準則";
$i_Discipline_System_Conduct_Semester_Ratio = "學期計算比例";
$i_Discipline_System_Conduct_alert_min_conduct_mark = "請輸入操行分下限";
$i_Discipline_System_Conduct_Ratio2 = "比例";
$i_Discipline_System_Conduct_Warning_Reminder_Point = "操行分底分";
$i_Discipline_System_Conduct_Instruction_Msg = "使用指引";
$i_Discipline_System_Conduct_Conduct_Balance_Auto_Update = "不論結餘, 所有操行分數將會統一更新成所輸入數目。你是否確定要繼續?";
$i_Discipline_System_Conduct_Instruction_Recalculate_Msg = "更改評級準則後，請前往<b>管理 > 操行分</b>，重新計算操行評級。";
//$i_Discipline_System_Conduct_Instruction = "";
$i_Discipline_System_Conduct_Mark_Tips = "* 當操行分到達該警戒分數時,系統將發出警告.";
$i_Discipline_System_Conduct_Total = "總共";
$i_Discipline_System_Discipline_Conduct_Mark_Grade_JS_alert = "對應等級含有不允許的字元";
$i_Discipline_System_Discipline_Conduct_Mark_Score_Integer_JS_alert = "請輸入數目";

$i_Discipline_System_Conduct_Instruction = "使用指引";
$i_Discipline_System_Conduct_Warning_Rules_List = "警告規則列表";
$i_Discipline_System_Conduct_Warning_Reminder_Point = "警戒分數";
$i_Discipline_System_Conduct_School_Year = "年度";
$i_Discipline_System_Conduct_Semester = "學期";
$i_Discipline_System_Conduct_Ratio = "比重";

$i_Discipline_System_Reports_Instruction_Msg = "個人報告顯示所選學生之獎懲紀錄及良好/違規行為紀錄。請設定報告時段與及要顯示的訓導紀錄類型。";
$i_Discipline_System_Reports_Report_Option = "- 選項 -";
$i_Discipline_System_Reports_First_Section = "- 獎懲紀錄 -";
$i_Discipline_System_Reports_Second_Section = "- 良好/違規行為紀錄 -";
$i_Discipline_System_Reports_Award_Punishment = "獎勵 / 懲罰";
$i_Discipline_System_Reports_All_Awards = "所有獎勵";
$i_Discipline_System_Reports_All_Punishment = "所有懲罰";
$i_Discipline_System_Reports_Include_Waived_Record = "包含豁免記錄";
$i_Discipline_System_Reports_Report_Period = "報告時段";
$i_Discipline_System_Reports_Conduct_Behavior = "操行 / 行為";
$i_Discipline_System_Reports_All_Good_Conducts = "所有良好行為";
$i_Discipline_System_Reports_All_Misconduct = "所有違規行為";
$i_Discipline_System_Reports_Discipline_report = "操行報告";
$i_Discipline_System_Reports_Show_Reports_Option = "顯示報告選項";
$i_Discipline_System_Reports_Misconduct_Behavior = "違規項目";
$i_Discipline_System_Reports_Invalid_Date_Compare = "起始日期需早於結束日期";
$iDiscipline['RankingTarget'] = "對象";
$iDiscipline['RecordType'] = "紀錄種類";
$iDiscipline['RecordType2'] = "紀錄類型";
$iDiscipline['Awarded'] = "獎勵";
$iDiscipline['Punished'] = "懲罰";
$iDiscipline['Rank'] = "名次";
$iDiscipline['RankingRange'] = "範圍";
$iDiscipline['ReceiveType'] = "類型";
$iDiscipline['byCategory'] = "行為類別";
$iDiscipline['byItem'] = "行為項目";
$iDiscipline['HorizontalDisplay'] = "並排顯示";
$iDiscipline['StackDisplay'] = "層疊顯示";

# Conduct Grade
$iDiscipline['ConductGradeAssessment'] = "操行評級評估";
$iDiscipline['ConductGradeMeeting'] = "操行評級會議";
$iDiscipline['CompleteRatio'] = "完成比例";
$iDiscipline['Assessment'] = "評級";
$iDiscipline['Percentage'] = "百分比";
$iDiscipline['TotalAward'] = "獎勵次數";
$iDiscipline['TotalPunishment'] = "獎勵總次數";
$iDiscipline['ViewConductGradeOf1stSemester'] = "檢視上學期操行資料";
$iDiscipline['ConductGradeOverview'] = "操行等級概覽";
$iDiscipline['ConductGradeFinal'] = "綜合操行等級";
$iDiscipline['Grade'] = "操行評級";
$iDiscipline['Grade Range'] = "相差";
$iDiscipline['TeacherInvolved'] = "參與評級老師";
$iDiscipline['ReAssessmentOfConductGrade'] = "調整操行等級";
$iDiscipline['OriginalResult'] = "原有結果";
$iDiscipline['IntegratedConductGrade'] = "綜合操行評級";
$iDiscipline['Overall'] = "綜合";
$iDiscipline['Revised'] = "已調整　";
$iDiscipline['WaitingForReview'] = "等待調整";
$iDiscipline['Class_Report'] = "班別報告";
$iDiscipline['Teacher_Report'] = "老師報告";
$iDiscipline['NoAssessmentIsNeeded'] = "沒有班別需要評級。";
$iDiscipline['PrintedBy'] = "列印老師";
$iDiscipline['NeedToReassessInConductMeeting'] = "若第一欄\"註\"顯示「<font color='red'>*</font>」即表示該生某些項目之評級須於操行會議作討論。";
$iDiscipline['ClassNo'] = "學號";
$iDiscipline['ChiName'] = "中文姓名";
$iDiscipline['Grade2'] = "等級";
$iDiscipline['Remark'] = "註";
$iDiscipline['IntegratedConduct'] = "綜合操行";
$iDiscipline['TableTopText1'] = "各項目之等級(";
$iDiscipline['TableTopText2'] = "給予學生之等級) / 等級範圍 / 參與評級老師之百分比";

# conduct report
$eDiscipline['Student_Conduct_Report'] = "學生操行報告";
$eDiscipline['Item'] = "事項";
$eDiscipline['PICTeacher'] = "負責老師";
$eDiscipline['ConductReportPeriod'] = " 期數";
$eDiscipline['AccumulateHomeworkNotSubmitted'] = "累積欠交功課";
$BaptistWingLungSecondarySchool = "浸信會永隆中學<br>BAPTIST WING LUNG SECONDARY SCHOOL";
$eDiscipline['Remark1ForWaiveRecord'] = "註一：\"R\"已抵消之記錄";
$eDiscipline['Remark2ForPrefectReport'] = "註二：由領袖生報告之記錄已交訓導老師查證並核對。";
$eDiscipline['PrincipalSignature'] = "校長簽署";
$eDiscipline['DisciplineGroupSignature'] = "訓導組簽署";
$eDiscipline['GuardianSignature'] = "家長/監護人簽署";
$eDiscipline['Date'] = "簽發日期";


# Modified by henry on 30 Dec 2008
$eDiscipline['NewLeaf_Notes'] = "注意：「更新計劃」記錄豁免功能不具追溯性。因此，如於記錄被豁免後始追加更新時段內的違規行為記錄，系統並不會取消已豁免記錄的\"豁免\"狀態。此時，請自行取消豁免有關記錄。";

# eNotice Template Setting
$i_Discipline_System_Discipline_Template_Name = "範本名稱";
$i_Discipline_System_Discipline_Category = "類別";
$i_Discipline_System_Discipline_Situation = "發放原因";
$i_Discipline_System_Discipline_Reply_Slip = "附有回條";
$i_Discipline_System_Discipline_Status = "狀況";
$i_Discipline_System_Discipline_Reason_For_Issue = "發放原因";
$i_Discipline_System_Discipline_Template_All_Status = "所有狀況";
$i_Discipline_System_Discipline_Template_Published = "使用中";
$i_Discipline_System_Discipline_Template_Draft = "暫不使用";
$i_Discipline_System_Discipline_Template_Title = "模板名稱";
$i_Discipline_System_Discipline_Template_Title_JS_warning= "請輸入範本名稱";
$i_Discipline_System_Discipline_Template_Subject_JS_warning= "請輸入範本標題";$i_Discipline_System_Discipline_Template_Auto_Fillin = "自動填充項目";
$i_Discipline_System_Discipline_Template_Topic_Title = "題目 / 標題";

# Access Right Setting
$i_Discipline_System_Discipline_Group_Access_Rights = "小組權限";
$i_Discipline_System_Discipline_Members_Rights = "訓導成員";
$i_Discipline_System_Teacher_Rights = "老師";
$i_Discipline_System_Student_Rights = "家長 / 學生";
$i_Discipline_System_Student_Right_Navigation2 = "學生權限";
$i_Discipline_System_Access_Right_Award_Punish = "獎勵<br>與懲罰";
$i_Discipline_System_Access_Right_Case_Record = "個案紀錄";
$i_Discipline_System_Access_Right_Case = "個案";
$i_Discipline_System_Access_Right_Good_Conduct_Misconduct = "良好行為<br>及違規行為";
$i_Discipline_System_Access_Right_Conduct_Mark = "操行分";
$i_Discipline_System_Access_Right_Detention = "留堂";
$i_Discipline_System_Access_Right_Detention_Management = "留堂 -<br>安排";
$i_Discipline_System_Access_Right_Detention_Session_Arrangement = "留堂 -<br>時段管理";
$i_Discipline_System_Access_Right_Top10 = "排名報告";
$i_Discipline_System_Access_Right_Email = "電子通告模板";
$i_Discipline_System_Access_Right_Letter = "權限";
$i_Discipline_System_Access_Right_eNotice_Template = "電子通告模板";
$i_Discipline_System_Access_Right_View = "檢視";
$i_Discipline_System_Access_Right_New = "新增";
$i_Discipline_System_Access_Right_Edit = "編輯";
$i_Discipline_System_Access_Right_Own = "自己";
$i_Discipline_System_Access_Right_All = "全部";
$i_Discipline_System_Access_Right_Delete = "刪除";
$i_Discipline_System_Access_Right_Waive = "豁免";
$i_Discipline_System_Access_Right_NewLeaf = "更新計劃";
$i_Discipline_System_Access_Right_Release = "開放";
$i_Discipline_System_Access_Right_Approval = "批核";
$i_Discipline_System_Access_Right_Finish = "完成";
$i_Discipline_System_Access_Right_Lock = "鎖定";
$i_Discipline_System_Access_Right_Adjust = "調整操行分";
$i_Discipline_System_Access_Right_Take_Attendance = "點名";
$i_Discipline_System_Access_Right_Rearrange_Student = "重新編排時段";
$i_Discipline_System_Access_Right_Access = "存取";
$i_Discipline_System_Access_Right_Member_Name = "成員姓名";
$i_Discipline_System_Access_Right_Added_Date = "加入日期";
$button_Add_Members_Now = "立即新增組員";
$button_Back_To_Group_List = "返回小組列表";
$i_Discipline_System_Teacher_Right_Navigation2 = "老師權限";
$i_Discipline_System_Teacher_Right_Management = "- 管理 -";
$i_Discipline_System_Teacher_Right_Statistics = "- 統計 -";
$i_Discipline_System_Teacher_Right_Reports = "- 報告 -";
$i_Discipline_System_Teacher_Right_Settings = "- 設定 -";
$i_Discipline_System_Group_Right_Navigation_Group_List = "小組列表";
$i_Discipline_System_Group_Right_Navigation_New_Group = "新增小組";
$i_Discipline_System_Group_Right_Navigation_New_Member = "新增";
$i_Discipline_System_Group_Right_Navigation_Choose_Member = "選擇成員";
$i_Discipline_System_Group_Right_Navigation_Selected_Member = "選擇學生";
$i_Discipline_System_Group_Right_Navigation_Selected_Member2 = "已選擇成員";
$i_Discipline_System_Group_Right_Navigation_Group_Info = "小組資訊";
$i_Discipline_System_Group_Right_Navigation_Group_Title = "小組名稱";
$i_Discipline_System_Group_Right_Navigation_Group_Description = "描述";
$i_Discipline_System_Group_Right_Navigation_Group_Access_Right = "小組權限";
$i_Discipline_System_Group_Right_Group_Name = "小組名稱";
$i_Discipline_System_Group_Right_Description = "描述";
$i_Discipline_System_Group_Right_No_Of_Members = "組員數目";

### Settings - Good Conduct & Misconduct
$eDiscipline['GoodConduct_and_Misconduct'] = "良好及違規行為";
$eDiscipline['Setting_GoodConduct'] = "良好行為";
$eDiscipline['Setting_Misconduct'] = "違規行為";
$eDiscipline['Setting_Cancel_PPC'] = "取消電子手帳狀況";
$eDiscipline['Setting_NewLeaf'] = "更新計劃";
$eDiscipline['Waive_By_New_Leaf_Scheme'] = "豁免(更新計劃)";
$eDiscipline['Setting_NewLeaf_Remark'] = "因更新計劃而被豁免.";
$eDiscipline['Setting_DefaultPeriodSettings'] = "累計時段";
$eDiscipline['Setting_CategorySettings'] = "類別及項目";
$eDiscipline['Setting_HeldEach'] = "Held each";
$eDiscipline['Setting_Rehabilitation_Period'] = "時段";
$eDiscipline['Setting_Days'] = "日";
$eDiscipline['Setting_WaiveFirst'] = "豁免數量上限";
$eDiscipline['Setting_Category'] = "類別";
$eDiscipline['Setting_Period'] = "累計時段";
$eDiscipline['Setting_Status'] = "狀況";
$eDiscipline['Setting_Status_All'] = "所有狀況";
$eDiscipline['Setting_Status_Using'] = "使用中";
$eDiscipline['Setting_Status_NonUsing'] = "非使用中";
$eDiscipline['Setting_CategoryList'] = "類別列表";
$eDiscipline['Setting_Status_Published'] = "使用中";
$eDiscipline['Setting_Status_Drafted'] = "暫不使用";
$eDiscipline['Setting_ItemName'] = "項目名稱";
$eDiscipline['Setting_Reorder'] = "重新排序";
$eDiscipline['Setting_NAV_CategoryList'] = "類別列表";
$eDiscipline['Setting_NAV_NewCategoryItem'] = "新增項目";
$eDiscipline['Setting_NAV_EditCategoryItem'] = "修改項目";
$eDiscipline['Setting_NAV_NewCategory'] = "新增類別";
$eDiscipline['Setting_NAV_EditCategory'] = "編輯類別";
$eDiscipline['Setting_NAV_NewPeriod'] = "新增時段";
$eDiscipline['Setting_NAV_EditPeriod'] = "編輯時段";
$eDiscipline['Setting_CategoryName'] = "類別名稱";
$eDiscipline['Setting_CalculationSchema'] = "計算方式";
$eDiscipline['Setting_CalculationSchema_Static'] = "固定遞增方式";
$eDiscipline['Setting_CalculationSchema_Static_Abb'] = "固定";
$eDiscipline['Setting_CalculationSchema_Floating'] = "浮動遞增方式";
$eDiscipline['Setting_CalculationSchema_Floating_Abb'] = "浮動";
$eDiscipline['Setting_CalculationSchema_Undefined'] = "未設定";
$eDiscipline['Setting_AccumulatedTimes'] = "累積次數";
$eDiscipline['Setting_DetentionTimes'] = "留堂次數";
$eDiscipline['Setting_SendNotice'] = "發出提示";
$eDiscipline['Setting_NewPeriod_Step1'] = "設定時段";
$eDiscipline['Setting_NewPeriod_Step2'] = "選擇累計轉換方式";
$eDiscipline['Setting_NewPeriod_Step3'] = "設定累計步驟";
$eDiscipline['Setting_CalculationDetails'] = "- 首個步驟 -";
$eDiscipline['Setting_FirstCalculationSchema']  = "- 首輪計算設定 -";
$eDiscipline['Setting_FollowUpAction'] = "- 跟進行動 -";
$eDiscipline['Setting_Reminder'] = "- 提示 -";
$eDiscipline['Setting_Condition'] = "條件";
$eDiscipline['Setting_Duplicate'] = "複製";
$eDiscipline['Setting_DefaultFollowUpAction'] = "預設跟進行動";
$eDiscipline['Setting_Detention'] = "留堂";
$eDiscipline['Setting_Semester'] = "對應年度";
$eDiscipline['Setting_BasicInformation'] = "基本資訊";
$eDiscipline['Setting_NumOfItem'] = "項目數量";
$eDiscipline['Setting_ConductMark_Increase']  = "操行分 (增加)";
$eDiscipline['Setting_ConductMark_Decrease']  = "操行分 (減少)";
$eDiscipline['Setting_Warning_PleaseSelectAtLeaseOnePunlishment'] = "請最少選擇一項懲罰";
$eDiscipline['Setting_CalculationInfo'] = "- 計算設定 -";
$eDiscipline['Content'] = "內容";
$eDiscipline['Subject'] = "標題";
$eDiscipline['MeritDemerit'] = "獎懲";

$i_Discipline['Warning_Integer_Input'] = "必須為正整數或 0。";
$i_Discipline['Warning_Input_WaiveFirst'] = "當時段內之Waive First有數值時, 請同時設定Waive Day的數值。";
$i_Discipline['Warning_Need_Setting'] = "請先設定".$eDiscipline['Setting_Period'];
$i_Discipline['Waive_Day_Header_Detail'] = "幾多日之內冇再犯可Waive* (days)";
$i_Discipline['Waive_First_Header_Detail']  = "Waive first* (times)";

### added on 20090113 by Ivan ###
$eDiscipline["Redeem"]["Step1"] = "選擇要抵銷的懲罰";
$eDiscipline["Redeem"]["Step2"] = "確認紀錄";
$eDiscipline["RedeemRecord"] = "功過相抵紀錄";
$eDiscipline["Redemption"] = "功過相抵";
$eDiscipline["jsWarning"]["MeritOnly"] = "要進行功過相抵，請選擇一個獎勵紀錄。";
$eDiscipline["jsWarning"]["OneStudentOnly"] = "只可選擇同一位學生之紀錄。";
$eDiscipline["jsWarning"]["ApprovedRecordOnly"] = "只有已批核的獎勵紀錄可進行功過相抵。";
$eDiscipline["SelectPunishmentToRedeem"] = "請選擇需要功過相抵的懲罰。";
$eDiscipline["AwardRedemption"] = "用作抵銷懲罰的獎勵";
$eDiscipline["PunishmentRedemption"] = "要抵銷的懲罰";
$eDiscipline["CorrespondingWaivedRecords"] = "- 相關豁免記錄 -";
$eDiscipline["CancelRedemption"] = "取消功過相抵";
$eDiscipline["Processing"] = "處理中...";
$eDiscipline["CancelRedemptionSuccess"] = "取消功過相抵完成";
$eDiscipline["CancelRedemptionFailed"] = "取消功過相抵失敗";
### added on 2 Mar 2009 by Ivan ###
$eDiscipline["Websams_Transition"] = "資料傳送(至網上校管系統)";
$eDiscipline["Websams_Transition_Step1"] = "上載網上校管系統CSV檔";
$eDiscipline["Websams_Transition_Step2"] = "選擇要傳送之紀錄類型";
$eDiscipline["Websams_Transition_Step3"] = "傳送紀錄";
$eDiscipline["Websams_Transition_Step1_url_example"] = "例子: http://websams.xxx.edu.hk/";
$eDiscipline["File"] = "檔案";
$eDiscipline["WebSAMS_URL"] = "網上校管系統網址";
### added on 3 Mar 2009 by Ivan ###
$eDiscipline["Websams_Transition_CSV_File_Remarks"] = "從網上校管系統匯出包含STUID及REGNO之CSV檔";
$eDiscipline["jsWarning"]["SelectFileFirst"] = "尚未選擇CSV檔。";
$eDiscipline["Websams_Transition_Step1_Instruction1"] = "你只能傳送紀錄到網上校管系統一次。進行傳送前，請確認所有獎懲紀錄均已經更新並正確無誤。";
$eDiscipline["Websams_Transition_Step1_Instruction2"] = "要匯出所需CSV檔, 請使用此查詢語句:";
$eDiscipline["Websams_Transition_Step1_Instruction3"] = "<i>SELECT STUID, REGNO FROM TB_STU_STUDENT</i>";
$eDiscipline["Websams_Transition_Step1_Instruction4"] = "要傳送獎懲紀錄到網上校管系統，請先登入網上校管系統並匯出含有STUID及REGNO的CSV檔，然後進入本頁。傳送過程必須在登入及匯出CSV檔後的五分鐘內開始。";
$eDiscipline["WebSAMS_Warning_CsvNoData"] = "CSV檔案沒有任何資料";
$eDiscipline["WebSAMS_Warning_wrongExtension"] = "<font color=\"red\">檔案不是CSV格式</font>";
$eDiscipline["WebSAMS_Warning_uploadFailed"] = "<font color=\"red\">上載失敗</font>";
$eDiscipline["WebSAMS_Warning_wrongHeader"] = "<font color=\"red\">標題錯誤</font>";
$eDiscipline["Instruction"] = "使用指引";
$eDiscipline["NumOfWebSAMSRegNoInCSV"] = "已上傳REGNO數量";
$eDiscipline["ReportCardPrintIndicate"] = "於網上校管系統成績表顯示所傳送之獎懲紀錄";
$eDiscipline["Caution"] = "注意";
$eDiscipline["Websams_Transition_Step2_Instruction1"] = "系統只會傳送 <i>已經批核</i> 的獎懲紀錄。";
$eDiscipline["TargetSchoolYear"] = "目標學年";
$eDiscipline["CurrentSchoolYear"] = "現時學年";
$eDiscipline["RecordsInDiscipline"] = "紀錄概覽";
$eDiscipline["RecordType"] = "紀錄類型";
$eDiscipline["TransferredRecords"] = "已傳送紀錄數量";
$eDiscipline["NotYetTransferredRecords"] = "未傳送紀錄數量";
$eDiscipline["LastTransferringDate"] = "最近傳送";
$eDiscipline["Websams_Transition_Step3_Instruction1"] = "資料傳送期間，請勿瀏覽其他頁面，或關閉任何瀏覽器視窗或彈出式視窗。";
$eDiscipline["Websams_Transition_Step3_Instruction2"] = "按\"開始傳送\"按鈕，傳送紀錄往網上校管系統。";
$eDiscipline["DataTransferComplete"] = "紀錄傳送完成。";
$eDiscipline["RecordsTransferred1"] = "已傳送";
$eDiscipline["RecordsTransferred2"] = "紀錄";
$eDiscipline["jsWarning"]["InvalidURL"] = "網址不正確。";
$eDiscipline["jsWarning"]["NothingSelected"] = "你尚未選擇要傳送之紀錄類型。";
$eDiscipline["jsWarning"]["AllAwardsTransferrred"] = "已傳送所有獎勵紀錄。";
$eDiscipline["jsWarning"]["AllPunishmentsTransferrred"] = "已傳送所有懲罰紀錄。";
$eDiscipline["Transferring"] = "轉送中...";
$eDiscipline["CheckConnection"] = "檢查連線";
$eDiscipline["Connecting"] = "連線中";
$eDiscipline["Successful"] = "成功連線";
$eDiscipline["Failed"] = "連線失敗";
$eDiscipline["StartDataTransfer"] = "開始傳送";
$eDiscipline["Websams_Transition_Step3_PopUpWarning"] = "<b>注意：</b><br />
資料傳送時，<b><font color=\"red\">請勿</font></b>關閉此視窗。<br />
完成傳送後，此視窗會自動關閉。";
$eDiscipline["Websams_Transition_Step1_Instruction5"] = "請注意，系統目前只支援傳送紀錄至WebSAMS1.5.1版本。";
$eDiscipline["Re-generate_CM_Warning"] = "由於評級準則已被修改，請前往<b>管理 > 操行分</b>，重新計算操行評級。";
# added on 30-07-2009 by Ivan - to WebSAMS
$eDiscipline["StudentMappingCSVFile"] = "學生編碼CSV檔案";
$eDiscipline["TeacherMappingCSVFile"] = "老師編碼CSV檔案";
$eDiscipline["Websams_Transition_Teacher_CSV_File_Remarks"] = "包含WebSAMS及eClass的老師編碼之CSV檔";
$eDiscipline["Download_Teacher_CSV"] = "按此下載eClass的老師編碼";


# without SubScore
//$iDiscipline['Award_Import_FileDescription_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_SettingsSemester, $i_Discipline_System_ItemCode, ".$eDiscipline['Conduct_Mark'].", $i_Discipline_System_Discipline_Case_Record_PIC, $i_email_subject, $i_Discipline_System_No_Of_Award_Punishment, ".$iDiscipline['RecordType'].", ".$iDiscipline['Award_Punishment_Type'].", $i_UserRemark";
/*$iDiscipline['Award_Import_FileDescription_withoutSubScore'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td>
<td>Class Number</td><td>Record Date</td><td>Semester</td><td>Item Code</td><td>Conduct Score</td><td>PIC</td><td>Subject</td><td>Add Merit Record</td><td>Category</td><td>Merit Type</td><td>Remark</tr>
<tr><td>1A</td><td>12</td><td>2009-03-20</td><td>Fall semester</td><td>LATE002F</td><td>4</td><td>Teacher01</td><td>English</td><td>1</td><td>Punishment</td><td>-3</td><td>Remark</td></tr>
</table>";	*/
$iDiscipline['Demerit_Import_FileDescription_withSubject_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_Subject_name, $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withoutSubject_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_UserRemark";

$iDiscipline['Award_Import_Remarks'] = "如果你使用Excel編輯CSV檔，請緊記更改所有儲存格成文字格式。選取有關儲存格，然後按滑鼠右鍵並選擇<b>儲存格格式</b>。在<b>儲存格格式</b>對話框之<b>數值</b>分頁，選擇<b>文字</b>。";
$iDiscipline['ImportOtherRecords'] = "匯入其他紀錄";
$iDiscipline['DemeritCount'] = "違規次數";

$iDiscipline['TotalTime'] = "總次數";
$iDiscipline['PastRecord'] = "過往紀綠";
$iDiscipline['TotalRecord'] = "紀綠總數";


### added on 9 Feb 2009 by Kelvin ###
$eDiscipline["jsWarning"]["EnableEnotice"] = "此功能已被系統管理員停用.";

### added on 11 Feb 2009 by Kelvin ###

$eDiscipline["EnoticeDisabledMsg"] = "電子通告並未啟用 或 你沒有使用電子通告的權限。";

$eDiscipline["Every"] = "每";
$eDiscipline["InstanceOf"] = "次";
$eDiscipline["WillBeCounted"] = "將轉換為一次";

$eDiscipline["SelectCategory"] = "-- 選擇一分類 --";
$eDiscipline["SelectItem"] = "-- 選擇一項目 --";

$eDiscipline['RecordWillChangeToWaiting_alert'] = "紀錄將會自動轉為「等待批准」狀態。繼續?";

$eDiscipline['SettingName'] = "系統屬性";
$eDiscipline['Value'] = "設定值";
$eDiscipline['SemesterRatio_MAX'] = "學期計算比例最大值";
$eDiscipline['ConductMarkIncrement_MAX'] = "操行分調整上限";
$eDiscipline['AwardPunish_MAX'] = "優缺功過數量上限";
$eDiscipline['AccumulativeTimes_MAX'] = "累積良好及違規行為紀錄數量上限";
$eDiscipline['ApprovalLevel_MAX'] = "需要批核的獎懲內容數量上限";
$eDiscipline['UseSubject'] = "需要指定獎勵及懲罰紀錄之相關科目";
$eDiscipline['AP_Conversion_MAX'] = "獎勵及懲罰轉換數量上限";
$eDiscipline['Activate_NewLeaf'] = "啟動「更新計劃」功能";
$eDiscipline['Detention_Sat'] = "留堂時段包括星期六";
$eDiscipline['Hidden_ConductMark'] = "隱藏操行分功能";

# eDiscipline v1.2 [REVISED - END]
##################################################################################################################



# eDiscipline v1.2
$eDiscipline["UnReleasedBy"] = "未開放設定者";
$i_Discipline_Date = "日期";
$i_Discipline_Time = "時間";
$i_Discipline_Applicable_Form = "適用級別";
$i_Discipline_Form = "級別";
$i_Discipline_Location = "地點";
$i_Discipline_Duty_Teacher = "當值老師";
$i_Discipline_Vacancy = "餘額";
$i_Discipline_Assigned = "已編定人數";
$i_Discipline_Attended = "已出席";
$i_Discipline_StartEnd_Time_Alert = "時間不正確。開始時間必須早於結束時間。";
$i_Discipline_Invalid_Vacancy = "餘額不正確";
$i_Discipline_Date_Selected = "已選擇日期";
$i_Discipline_Assigned_Session_Cannot_Be_Deleted = "不能刪除已分配時段。";
$i_Discipline_Delete_Session_Alert = "你是否確定刪除此時段？";
$i_Discipline_Delete = "刪除";
$i_Discipline_All_Forms = "所有級別";
$i_Discipline_Select = "選擇";
$i_Discipline_Remove = "移除已選用戶";
$i_Discipline_Select_Duty_Teachers = "選擇當值老師";
$i_Discipline_All_Date = "所有日期";
$i_Discipline_All_Week = "所有週次";
$i_Discipline_This_Month = "當月";
$i_Discipline_This_Month2 = "本月";
$i_Discipline_This_Week = "當週";
$i_Discipline_This_Week2 = "本週";
$i_Discipline_Next_Week = "下星期";
$i_Discipline_All_Status = "所有狀況";
$i_Discipline_All_Classroom = "所有課室";
$i_Discipline_Available = "未滿";
$i_Discipline_Full = "已滿";
$i_Discipline_Finished = "留堂完畢";
$i_Discipline_All_Time = "所有時間";
$i_Discipline_All_Duty = "所有當值老師";
$i_Discipline_Weekday = "平日";
$i_Discipline_In_The_Past_Alert = " is in the past";
$i_Discipline_Records = "紀錄";
$i_Discipline_Session_List = "時段列表";
$i_Discipline_New_Sessions = "新增時段";
$i_Discipline_Edit_Session = "編輯時段";
$i_Discipline_Detention_List = "留堂紀錄列表";
$i_Discipline_Detention_All_Status = "所有狀況";
$i_Discipline_Detention_All_Time_Slot = "所有時間";
$i_Discipline_Detention_All_Teachers = "所有當值老師";
$i_Discipline_New_Student_Records = "新增學生";
$i_Discipline_Select_Student = "選擇學生";
$i_Discipline_Add_Record = "增加紀錄";
$i_Discipline_Enotice_Setting = "設定電子通告";
$i_Discipline_Previous = "Previous";
$i_Discipline_Next = "Next";
$i_Discipline_Remark = "備註";
$i_Discipline_Calendar = "行事曆";
$i_Discipline_List = "詳細紀錄";
$i_Discipline_New_Session = "新增時段";
$i_Discipline_Student_Record = "學生紀錄";
$i_Discipline_Take_Attendance = "點名";
$i_Discipline_Assign_To_Here = "分配於此";
$i_Discipline_Assign_To_Here_Alert = "你是否確定要分配於此？";
$i_Discipline_Unassign_Student_List = "未編排學生列表";
$i_Discipline_Unassign_Student = "未編排學生";
$i_Discipline_Student = "學生";
$i_Discipline_Times_Remain = "剩餘次數";
$i_Discipline_Times = "次數";
$i_Discipline_View_Student_Detention_Arrangement = "檢視留堂安排";
$i_Discipline_Vacancy_Is_Not_Enough = "餘額不足。";
$i_Discipline_Auto_Arrange = "自動編排";
$i_Discipline_Arrange = "手動編排";
$i_Discipline_Arrange_Alert = "你是否確定要自行為所選學生分配留堂時間？";
$i_Discipline_Auto_Arrange_Alert = "你是否確定要自動為所選學生分配留堂時間？";
$i_Discipline_Detention_Details = "留堂明細";
$i_Discipline_Detention_Info = "留堂資料";
$i_Discipline_Students_Attending = "出席學生數目";
$i_Discipline_Attending_Student_List = "出席學生清單";
$i_Discipline_Student_List = "學生列表";
$i_Discipline_Cancel_Arrangement = "取消安排";
$i_Discipline_Class = "班別";
$i_Discipline_Detention_Reason = "留堂原因";
$i_Discipline_Request_Date = "懲罰日期";
$i_Discipline_Status = "狀況";
$i_Discipline_Cancel_Arrange_Alert = "你是否確定取消已選擇的安排？";
$i_Discipline_Display_Photos = "顯示相片";
$i_Discipline_Hide_Photos = "隱藏相片";
$i_Discipline_Set_Absent_To_Present = "設定全部學生為\"出席\"";
$i_Discipline_Take_Attendance_Alert = "你是否確定更新紀錄？";
$i_Discipline_Present = "出席";
$i_Discipline_Absent = "缺席";
$i_Discipline_Details = "詳情";
$i_Discipline_Take = "立即點名";
$i_Discipline_Waiting_For_Arrangement = "等候編排時段";
$i_Discipline_Arranged = "已編排";
$i_Discipline_All_Students = "所有學生";
$i_Discipline_Not_Assigned = "未編定";
$i_Discipline_Detented = "已留堂";
$i_Discipline_Send_Notice_Via_ENotice = "發放電子通告";
$i_Discipline_Select_Category = "選擇類別";
$i_Discipline_Session_Arranged_Before = "時段已被分配。";
$i_Discipline_Session_Not_Arranged_Before = "時段未被分配。";
$i_Discipline_Event_Type = "事項類別";
$i_Discipline_No_of_Records = "紀錄數目";
$i_Discipline_No_of_Records2 = "紀錄數量";
$i_Discipline_Pending_Event = "待辦事項";
$i_Discipline_Released_To_Student = "已開放予學生";
$i_Discipline_No_Action = "沒有跟進行動";
$i_Discipline_Attendance_Sheet = "點名紙";
$i_Discipline_Session_Info = "時段資訊";
$i_Discipline_Statistics_Options = "選項";
$i_Discipline_Show_Statistics_Option = "顯示統計選項";
$i_Discipline_Hide_Statistics_Option = "隱藏統計選項";
$i_Discipline_School_Year = "年度";
$i_Discipline_Semester = "學期";
$i_Discipline_By = "By";
$i_Discipline_Target = "對象";
$i_Discipline_Press_Ctrl_Key = "可按Ctrl鍵選擇多項";
$i_Discipline_No_Of_Detentions = "總留堂次數";
$i_Discipline_Reasons = "特定留堂原因數目";
$i_Discipline_Detention_Reasons = "特定留堂原因紀錄數量";
$i_Discipline_Subjects = "科目";
$i_Discipline_Subject = "科目";
$i_Discipline_Generate = "輸出統計";
$i_Discipline_Generate_Update_Statistic = "產生/更新統計";
$i_Discipline_Generate_Update_Report = "產生/更新報告";
$i_Discipline_Put_Into_Unassign_List = "加到未編排列表";
$i_Discipline_Quantity = "數量";
$i_Discipline_No_Of_Merits_Demerits = "獎懲紀錄總數";
$i_Discipline_Award_Punishment_Titles = "特定類型紀錄數量";
$i_Discipline_Show_Only = "統計範圍";
$i_Discipline_Top = "最高";
$i_Discipline_No_Of_GoodConductsMisconducts = "良好及違規行為總數";
$i_Discipline_GoodConductMisconduct_Titles = "特定類型紀錄數量";
$i_Discipline_Award_Punishment_Miss_Titles = "你尚未選擇任何獎懲類型/項目。";

$eDiscipline["DisciplineAdmin"] = "操行管理員";
$eDiscipline["DisciplineAdmin2"] = "模組管理員";
$i_Discipline_System_Discipline_Case_Record_Finished = "已完成";
$i_Discipline_System_Case_Record_Notes = "Notes";
$i_Discipline_System_Award_Punishment_Pending = "等待批核";
$i_Discipline_System_Award_Punishment_Wait_For_Waive = "等待豁免";
$i_Discipline_System_Award_Punishment_Wait_For_Release = "等待開放予學生";
$i_Discipline_System_Award_Punishment_Locked = "已鎖定";
$eDisicpline["GoodConduct_Misconduct_View"] = "檢視";

$eDiscipline["EditDetention"] = "留堂編輯";
$eDiscipline["LockedBy"] = "Locked By";
$eDiscipline['Detention_Settings'] = "留堂設定";
$eDiscipline['Detention_Statistics'] = "留堂統計";
$eDiscipline['Award_and_Punishment_Statistics'] = "獎勵及懲罰統計";
$eDiscipline['Good_Conduct_and_Misconduct_Statistics'] = "良好及違規行為統計";
$eDiscipline['Conduct_Mark_Statistics'] = "操行分統計";

$eDiscipline['Personal_Report_Goodconduct_Record']="良好行為";
$eDiscipline['Personal_Report_Misconduct_Record']="違規行為";
$eDiscipline['Personal_Report_Good_Misconduct_Record']="良好/違規行為";
$eDiscipline["Personal_Report_AwardPunishmentRecord"] = "獎勵/懲罰紀錄";


$eDiscipline['WarningLetter'] = "警告";
$eDiscipline['DetentionNoticeLetter'] = "留堂";
$eDiscipline['AwardNoticeLetter'] = "獎勵";
$eDiscipline['PunishmentNoticeLetter'] = "懲罰";
$eDiscipline['ConductMarkNoticeLetter'] = "操行分";
$eDiscipline['GoodConductNoticeLetter'] = "良好行為";
$eDiscipline['MisConductNoticeLetter'] = "違規行為";


$eDiscipline['AdjustmentDate'] = "調整日期";

$eDiscipline_System_Admin_User_Setting = "設定管理用戶";
$eDiscipline["AdminUser"] = "管理用戶";
$eDiscipline["NewRecord"] = "新增紀錄";
$eDiscipline["EditRecord"] = "編輯紀錄";
$eDiscipline["AwardRecord"] = "獎勵紀錄";
$eDiscipline["PunishmentRecord"] = "懲罰紀錄";
$eDiscipline["SelectSemester"] = "選擇學期";
$eDiscipline["AwardPunishmentRecord"] = "獎勵/懲罰紀錄";
$eDiscipline["AwardPunishmentRecord2"] = "獎懲紀錄";
$eDiscipline["TeacherStaffList"] = "教學及非教學職員名單";
$eDiscipline["SelectTeacherStaff"] = "請選擇教學及非教學職員";
$eDiscipline["RecordRejectAlert"] = "如拒絕此紀錄，相關的留堂紀錄將會被移除，及相關通告將被暫停。\\n你確定要拒絕此紀錄？";
$eDiscipline["RecordRemoveAlert"] = "如刪除此紀錄，相關的留堂紀錄及通告將會被刪除。\\n你確定要刪除此紀錄？";
$eDiscipline["EditActions"] = "編輯跟進行動";
$eDiscipline["AddDetention"] = "新增留堂紀錄";
$eDiscipline["NoSemesterSettings"] = "沒有學期設定。\\n請聯絡系統管理員跟進相關學期資料設定。";
$eDiscipline["CorrespondingConductRecords"] = "相對操行紀錄";
$eDiscipline["DisciplineName"] = "操行";	# for eNotice usage
$eDiscipline["WaivedReason"] = "豁免原因";
$eDiscipline["SemesterSettingError"] = "學期設定錯誤";
$eDiscipline["AddStudents"] = "新增學生";
$eDiscipline["FinishCaseConfirm"] = "你是否確定要此個案已處理完畢？";
$eDiscipline["ReleaseCaseConfirm"] = "所有相關的獎勵及懲罰紀錄將自動開放。\\n\\n你是否確定開放此個案？";
$eDiscipline["UnReleaseCaseConfirm"] = "所有相關的獎勵及懲罰紀錄將自動設定為「未開放」。\\n\\n你是否確定不開放此個案？";

### Settings - Good Conduct & Misconduct (Need to translate)
$eDiscipline['Setting_NAV_PeriodList'] = "時段清單";
$eDiscipline['Setting_NewPeriod_Step4'] = "計算方案總結";
$eDiscipline['Setting_FutherCalculationInfo'] = "- 額外步驟 -";
$eDiscipline['Setting_DetentionSession'] = "時段";
$eDiscipline['Setting_StudyMark_Increase']  = "勤學分 (增加)";
$eDiscipline['Setting_StudyMark_Decrease']  = "勤學分 (減少)";
$eDiscipline['Setting_Period_Default'] = "預設時段";
$eDiscipline['Setting_Period_Default_Setting'] = "使用預設累計時段";
$eDiscipline['Setting_Period_Use_Default'] = "使用預設時段";
$eDiscipline['Setting_Period_Use_Default_Periods'] = "預設累計時段使用中";
$eDiscipline['Setting_Period_Specify'] = "指定時段";
$eDiscipline['Setting_Period_Specify_Setting'] = "使用類別累計時段";
$eDiscipline['Setting_Period_Use_Specify'] = "使用類別時段";
$eDiscipline['Setting_Period_Use_Specify_Periods'] = "類別累計時段使用中";
$eDiscipline['Accumulative_Period_InUse_Warning'] = "不能刪除紀錄。有已選擇的紀錄時段已在使用中。";
$eDiscipline['Accumulative_Period_InUse_Warning_Update'] = "不能更新紀錄。有已選擇的紀錄時段已在使用中。";
$eDiscipline['Setting_Warning_InputCategoryName'] = "請輸入類別名稱。";
$eDiscipline['Setting_CalculationScheme'] = "計算方案";
$eDiscipline['Setting_ConversionScheme'] = "累計轉換方式";
$eDiscipline['Setting_AddMeritRecord'] = "增加優點紀錄";
$eDiscipline['Setting_AddDemeritRecord'] = "增加缺點紀錄";
$eDiscipline['Setting_DefaultPeriodOverlapWarning'] = "時段不能重疊.";
$eDiscipline['Setting_JSWarning_PeriodStartDateEmpty'] = "請輸入開始日期。";
$eDiscipline['Setting_JSWarning_PeriodEndDateEmpty'] = "請輸入結束日期。";
$eDiscipline['Setting_JSWarning_InvalidStartDate'] = "開始日期不正確。";
$eDiscipline['Setting_JSWarning_InvalidEndDate'] = "結束日期不正確。";
$eDiscipline['Setting_JSWarning_StartDateLargerThanEndDate'] = "開始日期不能遲於結束日期。";
$eDiscipline['Setting_JSWarning_RemoveCategory'] = "不能刪除此類別。";
$eDiscipline['Setting_JSWarning_CategoryItemNameEmpty'] = "請輸入項目名稱。";
$eDiscipline['Setting_JSWarning_CategoryItemCodeEmpty'] = "請輸入項目編號。";
$eDiscipline['AwardPunishmentRecord'] = "獎懲內容";

$iPowerVoice['bit_rate'] = "位元率 (影響音質)";
$iPowerVoice['bit_rate_unit'] = "kbps";
$iPowerVoice['sampling_rate'] = "採樣率 (影響清晰度)";
$iPowerVoice['sampling_rate_unit'] = "Hz";
$iPowerVoice['sound_length'] = "最大錄音長度";
$iPowerVoice['sound_length_unit'] = "mins";
$iPowerVoice['sound_unlimited'] = "沒有限制";
$iPowerVoice['power_voice_setting'] = "PowerVoice 設定";
$iPowerVoice['sound_recording'] = "錄音";
$iPowerVoice['powervoice'] = "PowerVoice";

$i_frontpage_campusmail_icon_important_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_important_title.gif' align='absmiddle' border='0'  >";
$i_frontpage_campusmail_icon_important2_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_important.gif' align='absmiddle' border='0' alt='{$i_frontpage_campusmail_important}' >";
$i_frontpage_campusmail_icon_notification_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_notification_title.gif' align='absmiddle' border='0' >";
$i_frontpage_campusmail_icon_notification2_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_notification.gif' align='absmiddle' border='0' alt='{$i_frontpage_campusmail_notification}' >";
$i_frontpage_campusmail_icon_status_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_unread_mail_title.gif' align='absmiddle' border='0' >";
$i_CampusMail_New_Icon_ReadMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_open_mail.gif' align='absmiddle'  border='0' alt='{$i_frontpage_campusmail_read}' >";
$i_CampusMail_New_Icon_RepliedMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_reply_mail.gif' align='absmiddle'  border='0' alt='{$i_CampusMail_New_Reply}' >";
$i_CampusMail_New_Icon_NewMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_unread_mail.gif' align='absmiddle'  border='0' alt='{$i_frontpage_campusmail_unread}' >";
$i_frontpage_campusmail_icon_attachment_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_attachment.gif' border='0' align='absmiddle' alt='{$i_frontpage_campusmail_attachment}' >";
$i_frontpage_campusmail_icon_restore_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash_restore.gif' border='0' align='absmiddle' alt='{$button_restore}' >";
$i_frontpage_campusmail_icon_trash_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif' border='0' align='absmiddle' alt='{$button_remove}' >";
$i_frontpage_campusmail_icon_empty_trash_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash_clear.gif' border='0' align='absmiddle' alt='{$button_emptytrash}' >";

$i_frontpage_campusmail_ago['days'] = " 天以前";
$i_frontpage_campusmail_ago['hours'] = " 小時以前";
$i_frontpage_campusmail_ago['minutes'] = " 分鐘以前";
$i_frontpage_campusmail_ago['seconds'] = " 秒以前";

$i_frontpage_campusmail_after['days'] = " 小時以後";
$i_frontpage_campusmail_after['hours'] = " 天以後";
$i_frontpage_campusmail_after['minutes'] = "  分鐘以後";
$i_frontpage_campusmail_after['seconds'] = " 秒以後";

$ip20_lang_eng = "ENG";
$ip20_lang_b5 = "繁";
$ip20_lang_gb = "&#x7B80;";
$ip20_powerby = "<span class=\"footertext\">Powered by</span> <a href=\"http://www.eclass.com.hk\" target=\"_blank\"><img src=\"$image_path/2007a/logo_eclass_footer.gif\" border=\"0\" align=\"absmiddle\"></a>";
$ip20_welcome = "歡迎";
$ip20_event_today = "今日事項";
$ip20_event_month = "本月事項";
$ip20_esurvey = "問卷";
$ip20_enotice = "通告";
$ip20_reminder = "提醒";
$ip20_what_is_new = "校園最新消息";
$ip20_loading = "載入中";
$ip20_public = "公眾";
$ip20_my_group = "我的小組";


################################################################################################
$i_ReportCard = "成績表";
$i_ReportCard_System = "成績表系統";
$i_ReportCard_System_Setting = "成績表系統設定";
$i_ReportCard_System_Admin_User_Setting = "設定管理用戶";
$i_ReportCard_All_Year = "全部年度";
$eReportCard["AdminUser"] = "管理用戶";
$eReportCard["TeachingAppointmentSettings"] = "教師職務設定";
$eReportCard['AssignSubjectClassWarning'] = "請選擇班別！";
$eReportCard['Code'] = "科目代碼";
$eReportCard['SubjectName'] = "科目名稱";
$i_ReportCard_Teaching_ImportInstruction = "<b><u>檔案說明:</u></b><br>
第一欄 : 教師內聯網帳號 (如 tmchan )。<br>
第二欄 : 任教的科目代碼 (如 ENG)。 <br>
第三欄 : 任教的班別 (如 1A;1B, 班別名稱以';'分隔)。<br>
班別及科目代碼必須完全正確 (分大小寫)<br><br>
<b>注意現時設定將會被取代。</b>";


#####
$i_StudentGuardian['MenuInfo'] = "監護人資料";
$i_StudentGuardian_all_students="所有學生";
$i_StudentGuardian_MainContent="主監護人";
$i_StudentGuardian_SMSPhone="SMS 號碼";
$i_StudentGuardian_Phone="聯絡號碼";
$i_StudentGuardian_EMPhone="緊急聯絡號碼";
$i_StudentGuardian_GuardianName="監護人名稱";
$i_StudentGuardian_SMS="SMS";
$i_StudentGuardian_warning_enter_chinese_english_name="請輸入監護人的英文或中文姓名";
$i_StudentGuardian_warning_enter_phone_emphone ="請輸入$i_StudentGuardian_Phone 或 $i_StudentGuardian_EMPhone";
$i_StudentGuardian_warning_Delete_Main_Guardian="你選擇了刪除主監護人，請設定主監護人。";
$i_StudentGuardian_warning_Delete_SMS="你選擇了刪除SMS聯絡號碼，請選擇其他號碼。";
$i_StudentGuardian_Additional_Searching_Criteria="附加搜尋條件";
$i_StudentGuardian_warning_PleaseSelectClass="請選擇最少一個班別";
$i_StudentGuardian_Back_To_Search_Result="返回搜尋結果";
$ec_guardian['01'] = "父親";
$ec_guardian['02'] = "母親";
$ec_guardian['03'] = "祖父";
$ec_guardian['04'] = "祖母";
$ec_guardian['05'] = "兄/弟";
$ec_guardian['06'] = "姊/妹";
$ec_guardian['07'] = "親戚";
$ec_guardian['08'] = "其他";
$ec_iPortfolio['relation'] = "關係";
$ec_iPortfolio['guardian_import_remind'] = "注意: <Br> - 第一欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - *第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定為SMS的接收者 (1 - 是, 0 - 否) <br> * (01 - 父親, 02 - 母親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";

$ec_iPortfolio['guardian_import_remind_sms'] = "注意: <Br> - 第一欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - * 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定為SMS的接收者 (1 - 是, 0 - 否) <br> - 第九欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否) <br> * (01 - 父親, 02 - 母親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";

$ec_iPortfolio['guardian_import_remind_studentregistry'] = "注意: <Br> - 第一欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否) <Br> - 第九欄為監護人職業 <Br> - 第十欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> 第十一欄為地址區碼<Br> - 第十二欄為住址街道 <Br> - 第十三欄為住址其他資料，如大廈，層數及單位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父親, 02 - 親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> (O - 其他, M - 澳門, T - &#27705;仔, C - 路環) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";



$ec_iPortfolio['guardian_import_remind_sms_studentregistry'] = "注意: <Br> - 第一欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定為SMS的接收者 (1 - 是, 0 - 否) <br> - 第九欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否)  <Br> - 第十欄為監護人職業 <Br> - 第十一欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> 第十二欄為地址區碼<Br> - 第十三欄為住址街道 <Br> - 第十四欄為住址其他資料，如大廈，層數及單位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父親, 02 - 母親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> (O - 其他, M - 澳門, T - &#27705;仔, C - 路環) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";


$ec_yyc['yyc_export_usage'] = "匯出給offline system";

$i_StudentGuardian_Searching_Area = "搜尋範圍";


### new smartcard
$StatusRecord = "考勤紀錄";
$MonthlyRecord = "每月考勤紀錄";
$DisplayAllPhoto = "顯示所有相片";
$HiddenAllPhoto = "隱藏所有相片";
$OverallRecord = "紀錄一覽";

### PPC
$i_PPC_SmartCard_Take_Attendance_In_School_TimeLocation="時間 / 地點";
$i_PPC_SmartCard_Take_Attendance_Show_LateAbsentOuting="只顯示遲到 / $i_StudentAttendance_Status_PreAbsent";
$i_PPC_SmartCard_Take_Attendnace_Show_AllStatus ="顯示所有狀況";
$i_ppc_home_title ="Mobile eClass";
$i_ppc_menu_home ="首頁";
### eFilling
$eFilling = "eFilling";

### iAccount
$iAccount = "iAccount";
$iAccount_Account = "戶口";
$AllRecords = "全部紀錄";

###############################################################################################################
##### eEnrollment Begin #######################################################################################
###############################################################################################################

$eEnrollmentMenu['act_attendance_mgt'] = "活動出席管理";
$eEnrollmentMenu['act_date_time'] = "活動日期";
$eEnrollmentMenu['act_event_further_mgt'] = "活動進階管理";
$eEnrollmentMenu['act_event_status_stat'] = "活動報名情況一覽";
$eEnrollmentMenu['act_event_status_stat_overall'] = "學會報名情況統計";
$eEnrollmentMenu['act_event_status_average_attendance'] = "平均出席總括";
$eEnrollmentMenu['act_status_stat'] = "學會報名情況一覽";
$eEnrollmentMenu['add_act_event'] = "新增活動";
$eEnrollmentMenu['attendance_mgt'] = "學生出席管理及統計";
$eEnrollmentMenu['attendance_mgt_stu_attend'] = "學生出席　";
$eEnrollmentMenu['attendance_mgt_stu_stat'] = "學生出席統計";
$eEnrollmentMenu['cat_setting'] = "類別";
$eEnrollmentMenu['cat_title'] = "類別名稱";
$eEnrollmentMenu['class_lvl_setting'] = "級別設定";
$eEnrollmentMenu['club_attendance_mgt'] = "學會出席管理";
$eEnrollmentMenu['club_enroll'] = "學會報名";
$eEnrollmentMenu['club_enroll_status'] = "學會報名情況";
$eEnrollmentMenu['data_batch_process'] = "資料同步處理";
$eEnrollmentMenu['enroll_title'] = "我的報名概況";
$eEnrollmentMenu['enrollment_sys_setting'] = "設定";
$eEnrollmentMenu['enrollment_basic_setting'] = "報名基本設定";
$eEnrollmentMenu['event_enroll'] = "活動報名";
$eEnrollmentMenu['gro_setting'] = "管理學會";
$eEnrollmentMenu['head_count'] = "人數";
$eEnrollmentMenu['mgt_act_event'] = "管理";
$eEnrollmentMenu['mgt_cat'] = "管理類別";
$eEnrollmentMenu['mgt_event'] = "管理活動";
$eEnrollmentMenu['percentage'] = "百分比";
$eEnrollmentMenu['title'] = "課外活動系統";
$eEnrollmentMenu['user_permission_mgt'] = "使用者權限管理";
$eEnrollmentMenu['user_set'] = "使用者身份設定";
## added on 13 Aug 2008
$eEnrollmentMenu['reports'] = "報告";
$eEnrollmentMenu['overall_perform_report'] = "表現總括報告";
## added on 01/09/2008 Yat Woon
$eEnrollmentMenu['transition'] = "傳送";
$eEnrollmentMenu['transfer_to_ole'] = "傳送至其他學習經歷";
## added on 5 Sept 2008
$eEnrollmentMenu['club'] = "學會";
$eEnrollmentMenu['activity'] = "活動";
$eEnrollmentMenu['club_mc'] = '餘暇活動';
$eEnrollment['tab']['management'] = "管理";
$eEnrollment['tab']['process'] = "進度";
$eEnrollment['tab']['attendance'] = "出席率";
## added on 10 Sept 2008
$eEnrollment['tab']['record'] = "紀錄";
## added on 11 Sept 2008
$eEnrollmentMenu['overall_perform'] = "表現總括";
$eEnrollmentMenu['class_level_access'] = "班級權限";
$eEnrollmentMenu['attendance_taking'] = "更新出席率";
## added on 02 Dec 2008
$eEnrollmentMenu['data_handling_to_SP'] = "資料處理(轉移至學生檔案)";
$eEnrollmentMenu['data_handling_to_OLE'] = "資料處理(轉移至其他學習經歷)";
$eEnrollmentMenu['data_handling_to_WebSAMS'] = "資料傳送(至網上校管系統)";
## added on 23 Mar 2009
$eEnrollmentMenu['enrolment_summary'] = "報名總括";
$eEnrollment['enrolment_summary_report'] = "報名總括報告";
$eEnrollmentMenu['enrolment_status'] = "報名狀態";
$eEnrollment['enrolment_status_report'] = "報名狀態報告";

$eEnrollment['absent'] = "不出席";
$eEnrollment['act_attendence'] = "活動出席率概要";
$eEnrollment['act_most_update_avg_attendance'] = "最新平均出席總括";
$eEnrollment['act_name'] = "活動名稱";
$eEnrollment['accept_apply'] = "接受申請";
$eEnrollment['activity'] = "活動";
$eEnrollment['add_student_conflict_warning'] = "<font color=\"#ff0000\">以下學生因為時間衝突問題而未能成為活動成員。</font>";
$eEnrollment['add_payment']['step_1'] = "繳費細則";
$eEnrollment['add_payment']['step_2'] = "津貼";
$eEnrollment['all'] = "全部";
$eEnrollment['app_period'] = "報名時段";
$eEnrollment['app_stage'] = "報名階段";
$eEnrollment['app_stu_list'] = "報名學生名單";
$eEnrollment['approve_period'] = "小組批核時段";
$eEnrollment['approve_stage'] = "小組批核階段";
$eEnrollment['approved'] = "已批核";
$eEnrollment['approved_stu_list'] = "參加學生名單";
$eEnrollment['attendence'] = "出席率";
$eEnrollment['amount_paid_by_student'] = "學生支付金額";
$eEnrollment['amount_paid_by_school'] = "校方津貼餘額";
$eEnrollment['back_to_event_index'] = "回到管理活動";
$eEnrollment['back_to_group_index'] = "回到管理學會";
$eEnrollment['backup_activity_btn'] = "把活動資料加入至學生檔案";
$eEnrollment['backup_activity_confirm'] = "你確定要把資料加入學生檔案？";
$eEnrollment['belong_to_group'] = "所屬學會";
$eEnrollment['club_name'] = "學會名稱";
$eEnrollment['club_quota'] = "會員名額";
$eEnrollment['club_setting'] = "活動設定";
$eEnrollment['conflict_warning'] = "<font color=\"#ff0000\">時間衝突</font>";
$eEnrollment['debit_directly'] = "直接繳費";
$eEnrollment['debit_later'] = "拍卡收費";
$eEnrollment['debit_method'] = "繳費方式";
$eEnrollment['decline_apply'] = "拒絕申請";
$eEnrollment['default_enroll_max_club'] = "學生可參加學會數目上限";
$eEnrollment['default_max_club'] = "學生可申請學會數目上限";
$eEnrollment['default_min_club'] = "學生可申請學會數目下限";
$eEnrollment['denied'] = "已拒絕";
$eEnrollment['del_warning'] = "<font color=\"#ff0000\">如果你儲存此頁，所有已遞交之申請將會被取消。</font>";
$eEnrollment['DoNoUseEnrolment'] = "不使用 eEnrolment";
$eEnrollment['js_del_warning'] = "如果你早前已輸入此步驟內的資料，而現在修改並儲存，所有已遞交之申請將被刪除。確定儲存？";
$eEnrollment['draw'] = "抽籤";
$eEnrollment['draw_and_end'] = "抽籤並完成報名程序";
$eEnrollment['event_member'] = "活動參加者";
$eEnrollment['GuardianInformationRemarks'] = "主要監護人";
# added on 10 Sept 2008
$eEnrollment['member'] = "會員";

$eEnrollment['enroll_persontype'] = "申請者對象";
$eEnrollment['female'] = "女";
$eEnrollment['groupmates_list'] = "會員名單";
$eEnrollment['lucky_draw'] = "進行抽籤";
$eEnrollment['mail_enroll_event_mail1'] = "活動報名程序已完成。";
$eEnrollment['mail_enroll_event_mail2'] = "你應該可在活動資訊中，尋找你已成功申請的活動資訊。";
$eEnrollment['mail_enroll_event_mail3'] = "如有問題，請聯絡系統管理員或負責課外活動的導師。";
$eEnrollment['mail_enroll_event_mail_fail'] = "你<STRONG>未能</STRONG>成功申請此活動。";
$eEnrollment['mail_enroll_event_mail_success'] = "你已<STRONG>成功</STRONG>申請此活動。";
$eEnrollment['mail_enroll_event_subject'] = "活動報名結果";
$eEnrollment['mail_enroll_club_mail1'] = "學會報名程序已完成。";
$eEnrollment['mail_enroll_club_mail2'] = "你可在學會資訊中，尋找已成功申請的學會資訊。";
$eEnrollment['mail_enroll_club_mail3'] = "如有問題，請聯絡系統管理員或負責課外活動的導師。";
$eEnrollment['mail_enroll_club_mail_fail'] = "你<STRONG>未能</STRONG>成功申請此學會。";
$eEnrollment['mail_enroll_club_mail_success'] = "你已<STRONG>成功</STRONG>申請此學會。";
$eEnrollment['mail_enroll_club_subject'] = "學會報名結果";
$eEnrollment['male'] = "男";
$eEnrollment['manage_app_stu_list'] = "已報名學生";
$eEnrollment['manage_apply_namelist'] = "已報名學生";
$eEnrollment['manage_groupmates_list'] = "組員";
$eEnrollment['manage_namelist'] = "管理名單";
$eEnrollment['meeting'] = "聚會";
$eEnrollment['next_app_stage'] = "進行下一輪報名程序";
$eEnrollment['no_limit'] = "無限制";
$eEnrollment['no_name'] = "無名稱";
$eEnrollment['no_record'] = "沒有紀錄";
$eEnrollment['parent'] = "家長";
$eEnrollment['payment'] = "繳費";
$eEnrollment['PaymentAmount'] = "暫定費用 HK($)";
$eEnrollment['payment_item'] = "繳費項目";
$eEnrollment['position'] = "排位";
$eEnrollment['present'] = "出席";
$eEnrollment['process_end'] = "程序完成";
$eEnrollment['quota_left'] = "餘額";
$eEnrollment['set_admin'] = "設定為管理員";
$eEnrollment['status'] = "狀態";
$eEnrollment['stu_club_rule'] = "預設學生只能選 <font color=\"#ff0000\"><!--MinClub--></font> 至 <font color=\"#ff0000\"><!--MaxChoice--></font> 個活動，學生最多可參加 <font color=\"#ff0000\"><!--MaxClub--></font> 個活動";
$eEnrollment['studnet_attendence'] = "學生出席率";
$eEnrollment['student_name'] = "學生姓名";
$eEnrollment['student'] = "學生";
$eEnrollment['system_user_list'] = "能使用系統之教職員名單";
$eEnrollment['system_user_name'] = "教職員姓名";
$eEnrollment['teachers_comment'] = "老師評語";
$eEnrollment['to'] = "至";
$eEnrollment['too_many_apply_process'] = "報名人數過多時的安排";
$eEnrollment['unset_admin'] = "取消管理員";
$eEnrollment['up_to'] = "截至";
$eEnrollment['use_system'] = "能夠使用此系統";
$eEnrollment['waiting'] = "待批";
$eEnrollment['whole_stage_end'] = "傳送通知予成功申請者";
$eEnrollment['year'] = "年度";

$eEnrollment['replySlip']['replySlip'] = "回條";
$eEnrollment['replySlip']['Preview'] = "預覽";
$eEnrollment['replySlip']['Edit'] = "編輯";
$eEnrollment['replySlip']['SaveAsTemplate'] = "另存為範本";
$eEnrollment['replySlip']['SignedPerson'] = "簽署人";
$eEnrollment['replySlip']['Instruction'] = "指示";
$eEnrollment['replySlip']['SignedDate'] = "簽署日期";
$eEnrollment['replySlip']['Title'] = "回條標題";
$eEnrollment['replySlip']['Signed_Total'] = "已簽/總人數";
$eEnrollment['replySlip']['BuildDate'] = "建立日期";
$eEnrollment['replySlip']['Instruction'] = '指示'; 
$eEnrollment['replySlip']['SignedInstruction'] = '此回條已於##DATE##由##PERSON##簽署'; 
$eEnrollment['replySlip']['NotSign'] = "未簽"; 
$eEnrollment['replySlip']['ClassName'] = "班別"; 
$eEnrollment['replySlip']['ClassNumber'] = "班號"; 
$eEnrollment['replySlip']['StudentName'] = "學生姓名"; 
$eEnrollment['replySlip']['Question'] = "問題"; 
$eEnrollment['replySlip']['SignedBy'] = "簽署人"; 
$eEnrollment['replySlip']['DateSigned'] = "簽署時間"; 
$eEnrollment['replySlip']['ReplySlipDetail'] = "回條詳情";
$eEnrollment['replySlip']['ReplySlipNotSet'] = "此項目並未有設立回條";
$eEnrollment['replySlip']['MustFillWarning'] = "所有打星的問題必須回答";

$eEnrollment['add_club_activity']['step_1'] = "設定資料";
$eEnrollment['add_club_activity']['step_1b'] = "委任職員及助手";
$eEnrollment['add_club_activity']['step_2'] = "設定活動日期及時間";

$eEnrollment['add_activity']['act_age'] = "對象年齡範圍";
$eEnrollment['add_activity']['act_assistant'] = "活動點名助手";
$eEnrollment['add_activity']['act_category'] = "活動類型";
$eEnrollment['add_activity']['act_content'] = "活動內容";
$eEnrollment['add_activity']['act_date'] = "活動日期";
$eEnrollment['add_activity']['act_gender'] = "對象性別";
$eEnrollment['add_activity']['act_name'] = "活動名稱";
$eEnrollment['add_activity']['act_pic'] = "活動負責人";
$eEnrollment['add_activity']['act_quota'] = "名額";
$eEnrollment['add_activity']['act_target'] = "對象級別";
$eEnrollment['add_activity']['act_time'] = "活動時段";
$eEnrollment['add_activity']['app_method'] = "報名方式";
$eEnrollment['add_activity']['app_period'] = "報名時段";
$eEnrollment['add_activity']['attachment'] = "附件";
$eEnrollment['add_activity']['age'] = "歲";
$eEnrollment['add_activity']['apply_role_type'] = "委任角色";
$eEnrollment['add_activity']['apply_ppl_type'] = "報名權限";
$eEnrollment['add_activity']['more_attachment'] = "更多附件";
$eEnrollment['add_activity']['parent_enroll'] = "由家長報名";
$eEnrollment['add_activity']['pickup_ppl'] = "選擇參加者";
$eEnrollment['add_activity']['select'] = "選擇";
$eEnrollment['add_activity']['set_select_period'] = "設定已選擇時段";
$eEnrollment['add_activity']['set_all'] = "設定所有時段";
$eEnrollment['add_activity']['step_1'] = "定義活動";
$eEnrollment['add_activity']['step_1b'] = "委任職員及助手";
$eEnrollment['add_activity']['step_2'] = "設定活動日期及時間";
$eEnrollment['add_activity']['step_3'] = "設定報名權限";
$eEnrollment['add_activity']['student_enroll'] = "由學生報名";

$eEnrollment['front']['app_period'] = "報名時段：<!--AppStart--> 至 <!--AppEnd-->";
$eEnrollment['front']['applied'] = "已申請";
$eEnrollment['front']['approve_period'] = "小組批核時段：<!--AppStart--> 至 <!--AppEnd-->";
$eEnrollment['front']['enroll'] = "報名參加";
$eEnrollment['front']['enrolled'] = "成功參加";
$eEnrollment['front']['event_time_conflict'] = "<font color=\"#ff0000\">若你選取之活動有時間衝突，你只能參加其中一項。</font>";
$eEnrollment['front']['information'] = "資料";
$eEnrollment['front']['quota_warning'] = "<font color=\"#ff0000\">滿額</font>";
$eEnrollment['front']['time_conflict_warning'] = "<font color=\"#ff0000\">若你選取之學會其聚會有時間衝突，你只能參加其中一個。</font>";
$eEnrollment['front']['wish_enroll_min'] = "你最少需要參加 <!--NoOfClub--> 個學會";
$eEnrollment['front']['wish_enroll_min_event'] = "你最少需要參加 <!--NoOfActivity--> 個活動";
$eEnrollment['front']['wish_enroll_front'] = "，希望最多參加&nbsp;";
$eEnrollment['front']['wish_enroll_end'] = "&nbsp;個活動。";
$eEnrollment['front']['wish_enroll_end_event'] = "&nbsp;個學會。";
$eEnrollment['approve_quota_exceeded'] = "人數超出限額，有學生未能加入此活動。";

$eEnrollment['js_confirm_del'] = "你是否確認刪除所選項目？";
$eEnrollment['js_confirm_del_single'] = "你是否確認刪除所選項目？";
$eEnrollment['js_payment_alert'] = "每項活動只能繳費一次，繳費實行之後不能修改，確定繳費？";
$eEnrollment['js_prepayment_alert'] = "每項活動只能進行繳費一次，並請先確定資料無誤。你是否確認進行繳費？";
$eEnrollment['js_sel_pic'] = "請選擇負責人。";
$eEnrollment['js_sel_student'] = "請選擇參與學生。";
$eEnrollment['js_quota_exists'] = "人數超出限額，你選擇了<!-- NoOfSelectedStudent-->位學生，請選擇 <!-- NoOfStudent--> 位學生。";
$eEnrollment['js_sel_date'] = "請選擇活動日期。";
$eEnrollment['js_time_invalid'] = "時間範圍不適當。";
$eEnrollment['js_confirm_cancel'] = "你確定要取消申請？";
$eEnrollment['js_fee_positive'] = "暫定費用只能輸入正數數字";
## added on 26 Aug 2008
//add_member.php
$eEnrollment['js_enter_role'] = "請輸入學生角色";
## added on 03 Sept 2008
//add_member.php
$eEnrollment['js_zero_means_no_limit'] = "設定限額為 0 即等於沒有限制";
# added on 24 Sept 2008
//active_member.php
$eEnrollment['js_update_active_member_per'] = "系統會因應新的出席率計算學生是否活躍會員，要繼續？";
## added on 29 Sept 2008
//payment.php
$eEnrollment['js_amount_cannot_be_zero'] = "項目收費必須多於零";
## added on 02 Oct 2008
$eEnrollment['js_enrol_one_only'] = "學會之間有時間衝突，你只能申請其中一個學會。";
## added on 09 Oct 2008
$eEnrollment['js_percentage_alert'] = "輸入之百分比必須為 0.00-100.00% ";
$eEnrollment['js_hours_alert'] = "輸入之時數必須是正數。";
## added on 11 Oct 2008
$eEnrollment['js_target_group_alert'] = "請至少選擇一組活動對象。";
$eEnrollment['js_no_active_member_club'] = "以上學會皆沒有活躍會員";
$eEnrollment['js_no_active_member_activity'] = "以上活動皆沒有活躍會員";
$eEnrollment['js_close_compose_email_window'] = "如關閉此視窗，你將失去所有已修改的內容。你是否確定要關閉此視窗？";


$eEnrollment['month'][0] = "十二月";
$eEnrollment['month'][1] = "一月";
$eEnrollment['month'][2] = "二月";
$eEnrollment['month'][3] = "三月";
$eEnrollment['month'][4] = "四月";
$eEnrollment['month'][5] = "五月";
$eEnrollment['month'][6] = "六月";
$eEnrollment['month'][7] = "七月";
$eEnrollment['month'][8] = "八月";
$eEnrollment['month'][9] = "九月";
$eEnrollment['month'][10] = "十月";
$eEnrollment['month'][11] = "十一月";
$eEnrollment['month'][12] = "十二月";
$eEnrollment['month'][13] = "一月";

$eEnrollment['pay']['amount'] = "項目收費";
$eEnrollment['pay']['item_name'] = "項目名稱";

$eEnrollment['select']['helper'] = "活動點名助手";
$eEnrollment['select']['pic'] = "活動負責人";

$eEnrollment['setting']['club_lower_limit'] = "能申請之學會數目上限";
$eEnrollment['setting']['club_upper_limit'] = "能申請之學會數目下限";

$eEnrollment['user_role']['normal_user'] = "Normal User 一般使用者";
$eEnrollment['user_role']['enrollment_master'] = "Enrollment Master 學會報名主任";
$eEnrollment['user_role']['enrollment_admin'] = "Enrollment Admin 學會報名管理員";

$eEnrollment['weekday']['sun'] = "日";
$eEnrollment['weekday']['mon'] = "一";
$eEnrollment['weekday']['tue'] = "二";
$eEnrollment['weekday']['wed'] = "三";
$eEnrollment['weekday']['thu'] = "四";
$eEnrollment['weekday']['fri'] = "五";
$eEnrollment['weekday']['sat'] = "六";

$eEnrollment['skip'] = "跳過此步驟";

### added on 19 Jun 2008
$eEnrollment['payment_success_list'] = "已成功處理以下繳費項目：";
$eEnrollment['payment_fail_list'] = "未能處理以下繳費項目：";
$eEnrollment['payment_status_success'] = "繳費成功。";
$eEnrollment['payment_status_fail'] = "繳費失敗，戶口結餘不足。";
$eEnrollment['payment_summary'] = "繳費總結";

$eEnrollment['attendance']['present'] = "出席";
$eEnrollment['attendance']['absent'] = "缺席";

$eEnrollment['role'] = "角色";
$eEnrollment['class_name'] = "班別";
$eEnrollment['class_number'] = "學號";
$eEnrollment['student_name'] = "姓名";
$eEnrollment['performance'] = "表現";
$eEnrollment['comment'] = "意見";
$eEnrollment['member_perform_comment_list'] = "組員表現及老師評語";
$eEnrollment['student_perform_comment_list'] = "學生表現及老師評語";

### added on 04 Aug 2008
$eEnrollment['club'] = "學會";
$eEnrollment['selection_mode'] = "選擇模式";

$eEnrollment['assigning_member'] = "加入會員";
$eEnrollment['assigning_student'] = "加入參加者";
$eEnrollment['assigning_enrolment'] = "加入報名學生";

### added on 05 Aug 2008
// import.php
$eEnrolment['DownloadCSVFile'] = "下載CSV範本";
$eEnrolment['AlertSelectFile'] = "請選擇檔案";
$eEnrolment['attendance_title'] = "出席資料";

### added on 07 Aug 2008
// import_result.php
$eEnrollment['invalid_date'] = "事項日期時間無效";
$eEnrollment['student_not_found'] = "找不到學生資料";
$eEnrollment['student_not_member'] = "學生不是會員";
$eEnrollment['record_existed'] = "已有該記錄";
$eEnrollment['empty_row'] = "沒有輸入資料";
$eEnrollment['student_not_event_student'] = "學生沒有參加此活動";
$eEnrollment['club_not_found'] = "沒有此學會";
$eEnrollment['activity_not_found'] = "沒有此活動";
$eEnrollment['clubs_with_same_name'] = "學會名稱重覆";
$eEnrollment['activity_with_same_name'] = "活動名稱重覆";
$eEnrollment['invalid_active_member_status'] = "活躍狀況不正確";
$eEnrollment['member_added'] = "已加入會員資料";
$eEnrollment['member_updated'] = "已更新會員資料";
$eEnrollment['participant_added'] = "已加入參加者資料";
$eEnrollment['participant_updated'] = "已更新參加者資料";

$eEnrollment['import_status'] = "匯入狀況";

$eEnrollment['back_to_enrolment'] = "回到報名情況";
$eEnrollment['back_to_attendance'] = "回到出席管理";

//member_index and event_member_index
$eEnrollment['import_performance_comment'] = "匯入表現資料及老師評語";

// import.php
$eEnrollment['Attendance_Import_FileDescription'] = "\"班別\",\"學號\",\"活動日期(YYYY-MM-DD)\",\"活動開始時間(HH:MM)\"";
$eEnrollment['Performance_Import_FileDescription'] = "\"班別\",\"學號\",\"表現\",\"老師評語\"";
$eEnrollment['Member_Import_FileDescription'] = "\"班別\",\"學號\",\"角色\",\"表現\",\"老師評語\",\"活躍/非活躍\"";
$eEnrollment['All_Member_Import_FileDescription'] = "\"學會名稱\", \"班別\",\"學號\",\"角色\",\"表現\",\"老師評語\",\"活躍/非活躍\"";
$eEnrollment['All_Participant_Import_FileDescription'] = "\"活動名稱\", \"學號\",\"學號\",\"角色\",\"表現\",\"老師評語\",\"活躍/非活躍\"";
$eEnrollment['activity_start_time'] = "活動開始時間";
$eEnrollment['activity_title'] = "活動名稱";
$eEnrollment['performance_title'] = "表現及老師評語";
$eEnrollment['csv_header']['club_name'] = "學會名稱";
$eEnrollment['csv_header']['activity_title'] = "活動名稱";

### added on 11 Aug 2008
//add_member.php
$eEnrollment['add_student_result'] = "加入學生結果";
$eEnrollment['select_student'] = "選擇學生";

//add_member_update.php
$eEnrollment['student_is_member'] = "學生已經是會員";
$eEnrollment['student_is_joined_event'] = "學生已參加此活動";
$eEnrollment['crash_group'] = "與其他學會發生時間衝突";
$eEnrollment['crash_activity'] = "與其他活動發生時間衝突";
$eEnrollment['crash_group_activity'] = "與其他學會及活動發生時間衝突";

//add_member_result.php
$eEnrollment['back_to_member_list'] = "回到會員名單";
$eEnrollment['back_to_student_list'] = "回到學生名單";
$eEnrollment['back_to_applicant_list'] = "回到報名名單";
$eEnrollment['add_others_student'] = "加入其他學生";
$eEnrollment['back_to_club_management'] = "回到學會管理";
$eEnrollment['back_to_activity_management'] = "回到活動管理";

### added on 12 Aug 2008
//get_sample_csv.php
$eEnrollment['sample_csv']['ClassName'] = "班別";
$eEnrollment['sample_csv']['ClassNumber'] = "學號";

//basic.php
$eEnrollment['disable_status'] = "繳費後不允許更改申請者狀態";
$eEnrollment['active_member_per'] = "活躍組員之最低出席率要求";
$eEnrollment['active_member_per_short'] = "成為活躍會員的最低出席率要求";

$eEnrollment['active_member'] = "活躍會員";
$eEnrollment['in_active_member'] = "非活躍會員";

## added on 01/09/2008 Yat Woon
$eEnrollment['Record_Type'] = "紀錄類別";
$eEnrollment['All_Records'] = "所有紀錄";
$eEnrollment['Club_Records'] = "學會紀錄";
$eEnrollment['Activity_Records'] = "活動紀錄";
$eEnrollment['Transfer'] = "傳送";
$eEnrollment['Not_Transfer'] = "不傳送";
$eEnrollment['Date'] = " 日期";
$eEnrollment['Period'] = "時段";
$eEnrollment['No_of_Student'] = "學生人數";
$eEnrollment['Component_of_OLE '] = "其他學習經歷種類";
$eEnrollment['transfer_finish']['step1'] = "選擇紀錄類別";
$eEnrollment['transfer_finish']['step2'] = "設定細則";
$eEnrollment['transfer_finish']['step3'] = "覆寫紀錄";
$eEnrollment['transfer_finish']['step4'] = "完成";
$eEnrollment['duplicate_ole_records']  = "如學會或活動之其他學習經歷重覆，取替其紀錄？";
$eEnrollment['duplicate_ole_participant_record']  = "如參與學生之紀錄重覆，取替其紀錄？";
$eEnrollment['overwrite']  = "取替";
$eEnrollment['skip2']  = "略過";
$eEnrollment['Already_Transfered']  = "已傳送";
$eEnrollment['Transfer_Another_OLE']  = "傳送其他紀錄";
$eEnrollment['transfer_student_setting']  = "紀錄傳送對象";
$eEnrollment['all_member']  = "全部會員";
$eEnrollment['active_member_only']  = "只限活躍會員";
$eEnrollment['OLE_Category'] = "紀錄類別";

## added on 13 Aug 2008
//basic.php
$eEnrollment['active_member_alert'] = "活躍學生之最低出席率必須為 0.00-100.00%";

//overall_performance_report.php
$eEnrollment['total_performance'] = "整體表現";

## added on 14 Aug 2008
//overall_performance_report.php
$eEnrollment['total_club_performance'] = "學會表現總分";
$eEnrollment['total_act_performance'] = "活動表現總分";

## added on 21 Aug 2008
$eEnrollment['notification_letter'] = "通知信";

## added on 25 Aug 2008
//overall_performance_report.php
$eEnrollment['print_class_enrolment_result'] = "列印整班課外活動報名結果";
$eEnrollment['print_club_enrolment_result'] = "列印整個學會報名結果";

//add_member_result.php
$eEnrollment['club_quota_exceeded'] = "超出學會限額";
$eEnrollment['student_quota_exceeded'] = "超出學生可加入學會之限額";
$eEnrollment['act_quota_exceeded'] = "超出活動限額";
$eEnrollment['rejected'] = "已拒絕";

//basic.php
$eEnrollment['one_club_when_time_crash'] = "如出現時間衝突，學生只允許參加其中一個學會";
$eEnrollment['hide_club_if_quota_full'] = "如報名人數達到該學會之限額，所有學生將不能報名參加該學會";

## added on 26 Aug 2008
//add_member_result.php
$eEnrollment['time_crash'] = "時間衝突";
$eEnrollment['club_or_act_title'] = "學會/活動及其時間";

## added on 28 Aug 2008
//payment_summary.php
$eEnrollment['back_to_club_enrolment'] = "回到學會報名情況一覽";
$eEnrollment['back_to_act_enrolment'] = "回到活動報名情況一覽";

//search box initial text
$eEnrollment['enter_name'] = "輸入姓名";
$eEnrollment['enter_student_name'] = "輸入學生姓名";

## added on 10 Sept 2008
$eEnrollment['enable_online_registration'] = "啟用網上登記";
$eEnrollment['disable_online_registration'] = "停用網上登記";
$eEnrollment['current_title'] = "現有名稱";
$eEnrollment['new_title'] = "新名稱";

## added on 11 Sept 2008
$eEnrollment['waiting_list'] = "候批";
$eEnrollment['number_of_applicants'] = "報名人數";
$eEnrollment['process'] = "處理";
$eEnrollment['participant'] = "參加人數";
$eEnrollment['participant2'] = "參加者";

## added on 12 Sept 2008
$eEnrollment['processed'] = "已處理";

## added on 17 Sept 2008
$eEnrollment['quota_exceeded'] = "超出限額";
$eEnrollment['default_school_quota_exceeded'] = "超出學校預設限額";
//basic_event.php
$eEnrollment['default_max_act'] = "學生可申請活動數目上限";
$eEnrollment['default_min_act'] = "學生可申請活動數目下限";
$eEnrollment['default_enroll_max_act'] = "學生可參加活動數目上限";
$eEnrollment['one_act_when_time_crash'] = "如出現時間衝突，學生只允許參加其中一個活動";

## added on 26 Sept 2008
$eEnrollment['Edit_Club_Info'] = "編輯學會資料";
$eEnrollment['crash_with'] = "時間衝突：";

## added on 29 Sept 2008
$eEnrollment['app_stage_event'] = "以下設定有效期為";
$eEnrollment['event_and'] = "至";

## added on 02 Oct 2008
$eEnrollment['warn_no_more_enrolment'] = "<font color=\"red\">你參予的學會數目已超出學校限額，你不能申請其他學會。</font>";

## added on 09 Oct 2008
$eEnrollment['total_hours_of_student'] = "傳送實際參與時數";
$eEnrollment['total_hours_if_above_percentage'] = "如出席率等於或高於 ";
$eEnrollment['total_hours_if_above_percentage2'] = "，傳送整個活動的所有時數";
$eEnrollment['manual_input_hours'] = "自訂傳送時數值 - ";
$eEnrollment['unit_hour'] = "小時";
$eEnrollment['hours_setting'] = "時數傳送設定";
$eEnrollment['transfer_if_zero_hour'] = "允許傳送時數為0小時的紀錄";
$eEnrollment['num_transferred_record'] = "已傳送紀錄數量";

## added on 10 Oct 2008
$eEnrollment['overall_performance_instruction'] = "表現總括報告只適用於表現<font color=\"red\"><strong>分數</strong></font>。您仍可於表現處輸入文字，但所有文字將不會顯示在表現總括報告中。";

## added on 20 Oct 2008
$eEnrollment['OLE_Components'] = "其他學習經歷種類";

## added on 21 Oct 2008
$eEnrollment['club_act_in_OLE'] = "學會/活動於其他學習經歷紀錄";
$eEnrollment['student_OLE_records'] = "學生於其他學習經歷紀錄";
$eEnrollment['replace_all'] = "更新全部";
$eEnrollment['replace'] = "更新";

## added on 22 Oct 2008
$eEnrollment['global_settings'] = "整體設定";
$eEnrollment['Record_exists'] = "紀錄己存在";	// for OLE program record
$eEnrollment['record_exists'] = "個紀錄己存在";	// for OLE student record
$eEnrollment['record_in_OLE'] = "其他學習經歷紀錄數量";

## added on 3 Nov 2008
$eEnrollment['alert_all_enrolment_records_deleted_club'] = "警告：除學會資料外，所有紀錄將被刪除。";
$eEnrollment['alert_all_enrolment_records_deleted_activity'] = "警告：除活動資料外，所有紀錄將被刪除。";

## added on 14 Nov 2008
$eEnrollment['all_categories'] = "全部類別";

## added on 2 Dec 2008
$eEnrollment['enrolment_settings'] = "課外活動設定";
$eEnrollment['schedule_settings']['instruction'] = "請於日曆內選擇學會聚會/活動的舉行日期。當日期被選取後，系統便會自動顯示相應時槽以供設定。";
$eEnrollment['schedule_settings']['scheduling_dates'] = "設定活動日期";
$eEnrollment['schedule_settings']['scheduling_times'] = "設定活動時間";
$eEnrollment['date_chosen'] = "已選擇日期";
$eEnrollment['button']['add&assign'] = "新增及委任";
$eEnrollment['button']['active_member'] = "檢視及編輯".$eEnrollment['active_member'];
$eEnrollment['button']['notification_letter'] = "列印".$eEnrollment['notification_letter'];
$eEnrollment['button']['update_role'] = $button_update.$eEnrollment['role'];
$eEnrollment['select_role'] = "- 選擇角色 -";
$eEnrollment['add_member_and_assign_role'] = "新增會員及委任角色";
$eEnrollment['add_participant_and_assign_role'] = "新增參加者及委任角色";
$eEnrollment['add_member'] = "新增會員";
$eEnrollment['add_participant'] = "新增參加者";
$eEnrollment['class_no'] = "學號";
$eEnrollment['time_conflict'] = "時間衝突";
$eEnrollment['lookup_for_clashes'] = "檢查衝突";
$eEnrollment['succeeded'] = "成功";
$eEnrollment['failed'] = "失敗";
$eEnrollment['active'] = "活躍";
$eEnrollment['inactive'] = "非活躍";
$eEnrollment['category'] = "類別";
$eEnrollment['view_statistics'] = "檢視統計";
$eEnrollment['take_attendance'] = "記錄出席情況";
$eEnrollment['unhandled'] = "候批";
$eEnrollment['drawing'] = "抽籤";
$eEnrollment['new_activity'] = "新增活動";
$eEnrollment['edit_activity'] = "編輯活動";
$eEnrollment['participant_quota'] = "參加名額";
$eEnrollment['participant_list'] = "參加者名單";
$eEnrollment['enter_role'] = "輸入角色";
$eEnrollment['attendance_rate'] = "出席率";
$eEnrollment['member_selection_and_drawing'] = "申請批核及抽籤";
$eEnrollment['participant_selection_and_drawing'] = "Participant Selection & Drawing";
$eEnrollment['random_drawing'] = "隨機抽籤";
$eEnrollment['last_modified'] = "上一次更新";
$eEnrollment['confirm'] = "確定";
$eEnrollment['proceed_to_step2'] = "下一步";

$eEnrollment['next_round_instruction'] = "過渡至下一輪報名程序前，請按此按鈕：";
$eEnrollment['button']['next_round'] = "清除未批核申請";
$eEnrollment['drawing_instruction'] = "要啟動系統的<font color='red'><!--DrawingMethod--></font>派位，請按此按鈕：";
$eEnrollment['button']['perform_drawing'] = "進行抽簽";
$eEnrollment['student_enrolment_rule'] = "學生可選擇<font color='red'><!--MinNumApp--></font>至<font color='red'><!--MaxNumApp--></font>個學會並最多可參加<font color='red'><!--MaxEnrol--></font>學會";
$eEnrollment['enrolment_period'] = "本輪報名程序時段：";
$eEnrollment['student_enrolment_rule_activity'] = "學生可選擇<font color='red'><!--MinNumApp--></font>至<font color='red'><!--MaxNumApp--></font>個活動並最多可參加<font color='red'><!--MaxEnrol--></font>學會";
$eEnrollment['enrolment_period_activity'] = "報名時段：";

$eEnrollment['archive_instruction'] = "你可把課外活動小組的資料輸出至學生檔案，這些資料包括<b>活動名稱，角色及表現</b>，並根據所屬年度及學期輸出";
$eEnrollment['archive_footer1'] = "如要更改上述年度及學期資料，請前往綜合平台行政管理中心的內聯網設定>系統設定>基本設定或聯絡系統管理員。";
$eEnrollment['archive_footer2'] = "你是否確認把課外活動資料加入到學生檔案？";

$eEnrollment['attendence_record'] = "出席紀錄";
$eEnrollment['import_member'] = "匯入會員";
$eEnrollment['import_participant'] = "匯入參加者";

$eEnrollment['Specify'] = "指定";
$eEnrollment['generate_or_update'] = "產生 / 更新報告";

$eEnrollment['send_club_enrolment_notification'] = "傳送學會報名通知";
$eEnrollment['notify_by_email'] = "電郵提醒";
$eEnrollment['default_student_notification_title'] = "Reminder: Club Enrolment (deadline: <--deadline-->)";
$eEnrollment['default_student_notification_content'] = "Please be reminded to register clubs in eEnrolment by <--deadline-->!";
$eEnrollment['default_class_teacher_notification_title'] = "Reminder: Student Club Enrolment (deadline: <--deadline-->)";
$eEnrollment['default_class_teacher_notification_content'] = "Some of your students have not registerred any clubs. They are listed below. Please kindly remind them to register clubs in eEnrolment by <--deadline-->! \n";
$eEnrollment['to_student'] = "致學生";
$eEnrollment['to_class_teacher'] = "致班主任";
$eEnrollment['no_class_teacher_warning'] = "<--ClassList-->班沒有班主任";
$eEnrollment['class'] = "班";
$eEnrollment['and'] = "及";
$eEnrollment['system_append_student_list_atuomatically'] = "系統會於電郵內容結尾自動加上該班的學生列表。";

$eEnrollment['success'] = "完成";
$eEnrollment['ImportOtherRecords'] = "匯入其他紀錄";

$eEnrollment['Content'] = "內容";

$eEnrollment['Target'] = "對象";
$eEnrollment['AverageAttendance'] = "平均出席";

###############################################################################################################
##### eEnrollment End #########################################################################################
###############################################################################################################



## IP20 Menu language
$ip20TopMenu['iTools'] = "個人通訊管理工具";
$ip20TopMenu['iMail'] = "我的電郵";
$ip20TopMenu['iFolder'] = "我的文件夾";
$ip20TopMenu['iCalendar'] = "我的行事曆";
$ip20TopMenu['iSmartCard'] = "我的智能卡紀錄";
$ip20TopMenu['iPortfolio'] = "學習檔案";
$ip20TopMenu['iAccount'] = "我的戶口";

$ip20TopMenu['eAdmin'] = "學校行政管理工具";
$ip20TopMenu['eAttendance'] = "考勤管理";
$ip20TopMenu['eAttendanceLesson'] = '考勤管理(課堂考勤)';
$ip20TopMenu['eDiscipline'] = "訓導管理";
$ip20TopMenu['eDisciplinev12'] = "訓導管理";
$ip20TopMenu['eReportCard'] = "成績表管理";
$ip20TopMenu['eMarking'] = "網上試卷批改";
$ip20TopMenu['eHomework'] = "網上家課表";
$ip20TopMenu['eEnrollment'] = "課外活動管理";
$ip20TopMenu['eSports'] = "運動會管理";
$ip20TopMenu['SwimmingGala'] = "水運會管理";
$ip20TopMenu['eOffice'] = "網上校務處";
$ip20TopMenu['ePayment'] = "收費管理";
$ip20TopMenu['eBooking'] = "預訂管理";
$ip20TopMenu['eInventory'] = "資產管理行政系統";
$ip20TopMenu['eSurvey'] = "問卷管理";
$ip20TopMenu['eTimeTabling'] = "編課管理";

$ip20TopMenu['eLearning'] = "學與教管理工具";
$ip20TopMenu['eClass'] = "網上教室";
$ip20TopMenu['eLC'] = "網上課件中心";
$ip20TopMenu['eMeeting'] = "網上視像會議";
$ip20TopMenu['LSLP'] = "通識學習平台";
$ip20TopMenu['eLibrary'] = "電子圖書";
if (isset($plugin['eLib_trial']) && $plugin['eLib_trial'])
{
	$ip20TopMenu['eLibrary'] .= " (試用版)";
}
$ip20TopMenu['eTV'] = "網上電視台";

$ip20TopMenu['eCommunity'] = "社群管理工具";
$ip20TopMenu['eGroups'] = "校內社群";
$ip20TopMenu['eParents'] = "家長會";
$ip20TopMenu['eAlumni'] = "校友會";

$ip20TopMenu['eService'] = "資訊服務";
$ip20TopMenu['SLSLibrary'] = "SLS圖書館系統";

$eOffice = $ip20TopMenu['eOffice'];

$i_SMS_Personalized_Tag['user_name_eng'] = $i_UserEnglishName;
$i_SMS_Personalized_Tag['user_name_chi'] = $i_UserChineseName;
$i_SMS_Personalized_Tag['user_class_name'] = $i_ClassName;
$i_SMS_Personalized_Tag['user_class_number'] = $i_ClassNumber;
$i_SMS_Personalized_Tag['user_login'] = $i_UserLogin;
$i_SMS_Personalized_Tag['attend_card_time'] = "進入/離開之拍咭時間";
$i_SMS_Personalized_Tag['payment_balance'] = $i_Payment_PrintPage_Outstanding_AccountBalance;
$i_SMS_Personalized_Tag['payment_outstanding_amount'] = $i_Payment_PrintPage_Outstanding_LeftAmount;

$i_SMS_SentRecords = "檢視已發送訊息";
$i_SMS_MessageCount = "短訊數目";


$i_license_sys_msg = "你的學生用戶許可證有<!--license_total-->個，尚有<!--license_left-->個可供使用。你已使用的許可證為<!--license_used-->個。";
$i_license_sys_msg_none = "沒有學生用戶許可證可用!";

###### iMail Spam Control ######
$i_spam['FolderSpam'] =  "雜件箱";
$i_spam['Setting'] = "過濾電郵防護服務設定";
$i_spam['WhiteList'] = "白名單";
$i_spam['BlackList'] = "黑名單";
$i_spam['BypassSetting'] = "使用設定";
$i_spam['SpamControl'] = "垃圾郵件控制";
$i_spam['VirusScan'] = "掃毒程式";
$i_spam['Option_Activated'] = "使用";
$i_spam['Option_Bypass'] = "繞過";
$i_spam['EmailAddress'] = "電郵地址";
$i_spam['AlertRemove'] = "你確定要移除這項紀錄?";
# added by Ronald on 20080807 #
$i_spam['SpamControlLevel'] = "垃圾郵件控制程度";
$i_spam['SpamControlLevel_DefaultLevel'] = "使用預設程度";
$i_spam['SpamControlLevel_CustomLevel'] = "使用自設程度";
$i_spam['js_Alert_SpamControlLevelEmpty'] = "請輸入垃圾郵件控制程度";
$i_spam['js_Alert_SpamControlLevelNotVaild'] = "請輸入有效的垃圾郵件控制程度";
# added by Ronald on 20080812 #
$i_spam['CustoSpamControlLevel_Description'] = "* - 數值越少代表垃圾郵件控制程度越高，反之亦然。範圍：0 - 6.0";
$i_Campusmail_MailQuotaSettting_SelectGroupWarning = "請選擇組別";

####################################
### Student Leave School Option ####
### Added On 29 Aug 2007 ###
$i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption = "學生離校選項";
$i_StudentAttendance_LeaveOption_Weekday = array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
$i_StudentAttendance_LeaveOption_Selection = array("無","家長接送","校車","自行離開");
$i_StudentAttendance_LeaveOption_ImportFileDescription = "
第一欄 : 班別 <br>
第二欄 : 班號 <br>
第三欄 : * <b>星期一</b> 離校選項 <br>
第四欄 : * <b>星期二</b> 離校選項 <br>
第五欄 : * <b>星期三</b> 離校選項 <br>
第六欄 : * <b>星期四</b> 離校選項 <br>
第七欄 : * <b>星期五</b> 離校選項 <br>
第八欄 : * <b>星期六</b> 離校選項 <br>
<br>
*&nbsp;0 - 無選項 <br>
&nbsp;&nbsp;&nbsp;1 - 家長接送 <br>
&nbsp;&nbsp;&nbsp;2 - 校車 <br>
&nbsp;&nbsp;&nbsp;3 - 自行離開
";
############################

### Added On 31 Aug 2007 ###
$i_studentAttendance_LeaveOption_ClassSelection_Warning = "請選擇班別";

$i_studentAttendance_LeaveOption_Import_ErrorMsg[1] = "<b>星期一</b> 離校選項 錯誤";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[2] = "<b>星期二</b> 離校選項 錯誤";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[3] = "<b>星期三</b> 離校選項 錯誤";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[4] = "<b>星期四</b> 離校選項 錯誤";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[5] = "<b>星期五</b> 離校選項 錯誤";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[6] = "<b>星期六</b> 離校選項 錯誤";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[7] = "班別/學號 錯誤";
############################

### Added On 1 Sep 2007 ###
$i_studentAttendance_ImportFailed_Reason = "錯誤原因";
###########################

### Added On 3 Sep 2007 ###
$attendst_leave_option = array(
array(0,"無"),
array(1,"家長接送"),
array(2,"校車"),
array(3,"自行離開"),
);

$i_studentAttendance_ConfirmMsg = "<font color=red>請確認以下記錄正確, 然後按呈送儲存</font>";
### End Of Student Leave School Option ###
##########################################


### Added On 4 Sep 2007 ###
$i_Staff_Patrol = "教師巡查記錄系統";


# eInventory words
$i_InventorySystem['eInventory'] = "資產管理行政系統";
$i_InventorySystem['InventorySystemSetting'] = "資產管理行政系統設定";
$i_InventorySystem['AdminUserSetting'] = "設定管理用戶";
$i_InventorySystem['AdminUser'] = "管理用戶";
$i_InventorySystem['Inventory'] = "物品清單";
$i_InventorySystem['Category'] = "類別";
$i_InventorySystem['SubCategory'] = "子類別";
$i_InventorySystem['Location'] = "位置";
$i_InventorySystem['Caretaker'] = "物品管理小組";
$i_InventorySystem['FundingSource'] = "資金來源";
$i_InventorySystem['WriteOffReason'] = "報銷原因";
$i_InventorySystem['StockTake'] = "盤點";
$i_InventorySystem['Report'] = "報表";
$i_InventorySystem['Settings'] = "設定";
$i_InventorySystem['HidePictures'] = "隱藏圖片";
$i_InventorySystem['Others'] = "其他";
$i_InventorySystem['PageManagement'] = "管理";

### Added On 28 Sep 2007 ###
$i_InventorySystem_Select_Menu = "請從左方功能清單選出所需服務。";
$i_InventorySystem_Category_Code = "類別編號";
$i_InventorySystem_Category_Name = "類別名稱";
$i_InventorySystem_Category_ChineseName = "中文名稱";
$i_InventorySystem_Category_EnglishName = "英文名稱";
$i_InventorySystem_Category_DisplayOrder = "顯示次序";
$i_InventorySystem_Category_Photo = "相片";
$i_InventorySystem_Category2_License = "許可證";
$i_InventorySystem_Category2_Warranty = "保用證";
$i_InventorySystem_Category2_Barcode = "條碼";

### Added on 13 Nov 2007 ###
$i_InventorySystem_ReportType_Array = array(
array(1,"物品資料"),
array(2,"數量改變"),
array(3,"保用證到期日"),
array(4,"現時數量"));
$i_InventorySystem_ReportType = "類型";
$i_InventorySystem_Report_Warranty = "保用證";
$i_InventorySystem_Report_QuantityLevel = "現時數量";
$i_InventorySystem_Report_ItemName = "名稱";
$i_InventorySystem_Report_CurrentQuantity = "現時數量";
$i_InventorySystem_Report_InitialQuantity = "開始數量";
$i_InventorySystem_Report_QtyDifference = "數量差別";
$i_InventorySystem_Report_WarrantyExpiryDate = "保用證到期日";
$i_InventorySystem_Report_WarrantyExpied = "已過期";
$i_InventorySystem_Report_WarrantyNotExpied = "未過期";
$i_InventorySystem_Report_QtyMoreThan = "多於 $eInventory_StockAlertLevel";
$i_InventorySystem_Report_QtyLessEqualTo = "少於/等於 $eInventory_StockAlertLevel";
$i_InventorySystem_Report_ItemDetails = "物品詳細資料";

$i_InventorySystem_Action_Purchase = "購買";
$i_InventorySystem_Action_StockCheck = "盤點";
$i_InventorySystem_Action_UpdateStatus = "更新狀況";
$i_InventorySystem_Action_ChangeLocation = "更改位置";
$i_InventorySystem_Action_ChangeLocationQty = "更改數量";
$i_InventorySystem_Action_MoveBackToOriginalLocation = "搬回原位";
$i_InventorySystem_Action_Stocktake_Original_Location = "盤點 - 在原位找到";
$i_InventorySystem_Action_Stocktake_Other_Location = "盤點 - 在其他位置找到";
$i_InventorySystem_Action_Stocktake_Not_Found = "盤點 - 找不到";
$i_InventorySystem_Action_Surplus_Ignore = "忽略多出數量";
$i_InventorySystem_Action_Edit_Item = "修改物品資料";
$i_InventorySystem_Action_WriteOff = "報銷";

$i_InventorySystem_ItemStatus_Normal = "正常";
$i_InventorySystem_ItemStatus_Repair = "維修中";
$i_InventorySystem_ItemStatus_WriteOff = "報銷";
$i_InventorySystem_ItemStatus_Damaged = "損壞";

$i_InventorySystem_Action_Array = array(
array(1,$i_InventorySystem_Action_Purchase),
array(2,$i_InventorySystem_Action_Stocktake_Original_Location),
array(3,$i_InventorySystem_Action_Stocktake_Other_Location),
array(4,$i_InventorySystem_Action_Stocktake_Not_Found),
array(5,$i_InventorySystem_Action_WriteOff),
array(6,$i_InventorySystem_Action_UpdateStatus),
array(7,$i_InventorySystem_Action_ChangeLocation),
array(8,$i_InventorySystem_Action_ChangeLocationQty),
array(9,$i_InventorySystem_Action_MoveBackToOriginalLocation),
array(10,$i_InventorySystem_Action_Surplus_Ignore)
);

$i_InventorySystem_ItemType_Single = "單一物品";
$i_InventorySystem_ItemType_Bulk = "大量物品";

$i_InventorySystem_ItemType_Array = array(
array(1,$i_InventorySystem_ItemType_Single),
array(2,$i_InventorySystem_ItemType_Bulk));

$i_InventorySystem_Assigned = "被編配";
$i_InventorySystem_Avanced_Search = "進階搜尋";
$i_InventorySystem_Simple_Search = "簡單搜尋";
$i_InventorySystem_Found = "找到";
$i_InventorySystem_Group_Member = "管理成員";
$i_InventorySystem_Import_Member_From = "由內聯網小組匯入名單";
$i_InventorySystem_Loading = "處理中";
$i_InventorySystem_Location_Level = "位置";
$i_InventorySystem_Location = "子位置";

$i_InventorySystem_Keyword = "關鍵字";
$i_InventorySystem_Optional = "可不填";
$i_InventorySystem_Item_NotFound = "物品位置錯誤";
$i_InventorySystem_Item_NotFound_Display = "檢視位置錯誤的物品";
$i_InventorySystem_Item_Name = "物品名稱";
$i_InventorySystem_Item_Qty = "數量";
$i_InventorySystem_Item_Code = "物品編號";
$i_InventorySystem_Item_Barcode = "條碼";
$i_InventorySystem_Item_Description = "詳細資料";
$i_InventorySystem_Item_Ownership = "擁有權";
$i_InventorySystem_Item_Price = "購買金額 (單據總額)";
$i_InventorySystem_Item_Remark = "備註";
$i_InventorySystem_Item_Detail = "詳細資料";
$i_InventorySystem_Item_History = "紀錄";
$i_InventorySystem_Item_Invoice = "發票";
$i_InventorySystem_Item_Supplier_Name = "供應商名稱";
$i_InventorySystem_Item_Supplier_Contact = "供應商聯絡";
$i_InventorySystem_Item_Supplier_Description = "供應商資料";
$i_InventorySystem_Item_Purchase_Date = "購入日期";
$i_InventorySystem_Item_Purchase_Date2 = "購入日期";
$i_InventorySystem_Item_Quot_Num = "報價單編號";
$i_InventorySystem_Item_Tender_Num = "標書編號";
$i_InventorySystem_Item_Invoice_Num = "發票編號";
$i_InventorySystem_Item_Voucher_Num = "收據編號";
$i_InventorySystem_Item_Attachment = "附件";
$i_InventorySystem_Item_Brand_Name = "牌子";
$i_InventorySystem_Item_Warrany_Expiry = "保用日期";
$i_InventorySystem_Item_Record_Date = "紀錄日期";
$i_InventorySystem_Item_License_Type = "許可證";
$i_InventorySystem_Item_Action = "行動";
$i_InventorySystem_Item_Past_Status = "改動前狀態";
$i_InventorySystem_Item_New_Status = "改動後狀態";
$i_InventorySystem_Item_Remark = "備註";
$i_InventorySystem_Item_PIC = "負責人";
$i_InventorySystem_Item_AssignTo = "編配至";
$i_InventorySystem_Item_Stock_Qty = $i_InventorySystem['StockCheck'].$i_InventorySystem_Item_Qty;
$i_InventorySystem_Item_ChineseName= "物品名稱 (中文)";
$i_InventorySystem_Item_EnglishName= "物品名稱 (英文)";
$i_InventorySystem_Item_ChineseDescription = "詳細資料 (中文)";
$i_InventorySystem_Item_EnglishDescription = "詳細資料 (英文)";
$i_InventorySystem_Item_Photo = "相片";
$i_InventorySystem_Item_Location = "位置";
$i_InventorySystem_Item_Funding = "資金來源";
$i_InventorySystem_Item_License = "許可證";
$i_InventorySystem_Item_WarrantyExpiryDate = "保用證到期日";
$i_InventorySystem_Item_Serial_Num = "序號";
$i_InventorySystem_Item_BulkItemAdmin = "誤差總管";

$i_InventorySystem_Ownership_School = "學校";
$i_InventorySystem_Ownership_Government = "政府";
$i_InventorySystem_Ownership_Donor = "辦學團體";

$i_InventorySystem_Location_Level_Name = "位置名稱";
$i_InventorySystem_Location_Name = "子位置名稱";
$i_InventorySystem_Group_Name = "物品管理組別名稱";
$i_InventorySystem_Group_MemberName = "組員名稱";

$i_InventorySystem_ItemStatus_Array = array(
array(1,$i_InventorySystem_ItemStatus_Normal),
array(2,$i_InventorySystem_ItemStatus_Damaged),
array(3,$i_InventorySystem_ItemStatus_Repair)
);

$i_InventorySystem_ItemDetails = "物品詳細資料";
$i_InventorySystem_PurchaseDetails = "購買資料";

# added on 30 Nov 2007

$i_InventorySystem_Item_CategoryCode = "類別編號";
$i_InventorySystem_Item_CategoryName = "類別名稱";
$i_InventorySystem_Item_Category2Code = "子類別編號";
$i_InventorySystem_Item_LocationCode = "位置編號";
$i_InventorySystem_Item_Location2Code = "子位置編號";
$i_InventorySystem_Item_GroupCode = "管理組別編號";
$i_InventorySystem_Item_FundingSourceCode = "資金來源編號";
$i_InventorySystem_Index = "編號";

$i_InventorySystem_Item_New = "新物品";
$i_InventorySystem_Item_Existing = "現存物品";

# added on 5 Dec 2007 #
$i_InventorySystem_ImportInstruction_Title = "CSV 檔案使用方法";
$i_InventorySystem_ImportInstruction_Download = "按此下載";
$i_InventorySystem_ImportInstruction_InstallFontType = "及安裝條碼字型 - IDAutomationHC39M (true type)";
$i_InventorySystem_ImportInstruction_ReportTemplate = "報表樣式(MS Word 格式)";
$i_InventorySystem_ImportInstruction_ImportCSVFile = "設定報表中的資料來源為 CSV 檔案";
$i_InventorySystem_ImportInstruction_PrintReport = "列印報表";
$i_InventorySystem_Category_CustomPhoto = "自訂";

# addded on 6 Dec 2007 #
$i_InventorySystem_Setting_StockCheckPeriod = "盤點日期";
$i_InventorySystem_Setting_StockCheckStart = "開始日期";
$i_InventorySystem_Setting_StockCheckEnd = "完結日期";
$i_InventorySystem_Setting_WarrantyExpiryWarning = "保用證到期提示";
$i_InventorySystem_Setting_WarrantyExpiryWarningDayPeriod = "到期前日數";
$i_InventorySystem_Report_WarrantyExpiry = "到期保用證";
$i_InventorySystem_Report_ItemNotStockCheck = "未點算物品";

# added on 10 Dec 2007 #
$i_InventorySystem_StockAssigned = "數量(已編配)";
$i_InventorySystem_StockFound = "數量(已找到)";
$i_InventorySystem_Input_Category_Code_Warning = "請輸入編號";
$i_InventorySystem_Input_Category_Item_ChineseName_Warning = "請輸入中文名稱";
$i_InventorySystem_Input_Category_Item_EnglishName_Warning = "請輸入英文名稱";

# added on 11 Dec 2007 #
$i_InventorySystem_Import_CheckItemCode = "按此查詢".$i_InventorySystem_Item_Code;
$i_InventorySystem_Import_CheckCategory = "按此查詢".$i_InventorySystem_Item_CategoryCode;
$i_InventorySystem_Import_CheckCategory2 = "按此查詢".$i_InventorySystem_Item_Category2Code;
$i_InventorySystem_Import_CheckGroup = "按此查詢".$i_InventorySystem_Item_GroupCode;
$i_InventorySystem_Import_CheckLocation = "按此查詢".$i_InventorySystem_Item_LocationCode;
$i_InventorySystem_Import_CheckLocation2 = "按此查詢".$i_InventorySystem_Item_Location2Code;
$i_InventorySystem_Import_CheckFunding = "按此查詢".$i_InventorySystem_Item_FundingSourceCode;
$i_InventorySystem_Import_CorrectSingleItemCodeReminder = " * 以上之物品編號已在使用，請使用新的物品編號進行匯入。";
$i_InventorySystem_Import_CorrectBulkItemCodeReminder = " * 以上之物品編號已在使用，匯入已有的物品編號，將更改所對應物品之數量。";
$i_InventorySystem_Import_CorrectFundingIndexReminder = " * 請小心核對csv匯入檔之資金來源編號是否如上相同。";
$i_InventorySystem_Import_CorrectGroupIndexReminder = " * 請小心核對csv匯入檔之管理組別編號是否如上相同。";
$i_InventorySystem_Import_CorrecrCategory2IndexReminder = " * 請小心核對csv匯入檔之子類別編號是否如上相同。";
$i_InventorySystem_Import_CorrectLocationIndexReminder = " * 請小心核對csv匯入檔之子位置編號是否如上相同。";
$i_InventorySystem_Import_CorrectLocation2IndexReminder = " * 匯入檔案時 請使用正確的".$i_InventorySystem_Item_Location2Code;
$i_InventorySystem_StockCheck_QtyDifferent = "相差數量";
$i_InventorySystem_StockCheck_ValidQuantityWarning = "請輸入有效數量";
$i_InventorySystem_StockCheck_EmptyQuantityWarning = "請輸入數量";
$i_InventorySystem_StockCheck_Found = "已找到";
$i_InventorySystem_StockCheck_LastStatus = "最後狀況";
$i_InventorySystem_StockCheck_NewStatus = "最新狀況";
$i_InventorySystem_Import_CorrectCategoryCodeWarning = " * 以上之類別編號已在使用，請使用新的類別編號進行匯入。";
$i_InventorySystem_Import_CorrecrCategory2CodeWarning = " * 以上之子類別編號已在使用，請使用新的子類別編號進行匯入。";
$i_InventorySystem_Import_CorrectLocationCodeWarning = " * 以上之位置編號已在使用，請使用新的位置編號進行匯入。";
$i_InventorySystem_Import_CorrectLocation2CodeWarning = " * 以上之子位置編號已在使用，請使用新的子位置編號進行匯入。";
$i_InventorySystem_Import_CorrectFundingCodeWarnging = " * 以上之資金來源編號已在使用，請使用新的資金來源編號進行匯入。";

# added on 12 Dec 2007 #
$i_InventorySystem_Item_Registration = "新增物品";
$i_InventorySystem_Input_Item_Code_Warning = "請輸入物品編號";
$i_InventorySystem_Input_Item_Code_Length_Warning = "物品編號不能多於10個字元";
$i_InventorySystem_Input_Item_GroupSelection_Warning = "請編配組別";
$i_InventorySystem_Input_Item_LocationSelection_Warning = "請選擇位置";
$i_InventorySystem_Input_Item_FundingSelection_Warning = "請選擇資金來源";
$i_InventorySystem_Input_Item_PurchaseDate_Warning = "請輸入正確購入日期";
$i_InventorySystem_Input_Item_WarrantyExpiryDate_Warning = "請輸入正確保用證到期日";
$i_InventorySystem_Input_Item_Barcode_Warning = "請輸入或產生條碼";

# added on 14 Dec 2007 #
$i_InventorySystem_StockCheck_Step1 = "選擇盤點地點";
$i_InventorySystem_StockCheck_Step2 = "開始盤點";
$i_InventorySystem_StockCheck_Step3 = "確認盤點結果";
$i_InventorySystem_Setting_WarrantyGiveWarning1 = "到期";
$i_InventorySystem_Setting_WarrantyGiveWarning2 = "日前提示";
$i_InventorySystem['BasicSettings'] = "基本設定";
$i_InventorySystem['FullList'] = "所有物品";
$i_InventorySystem['View'] = "檢視";
$i_InventorySystem_Quantity_StockChecked = "數量(已點算)";
$i_InventorySystem_PhotoGuide = "相片只能以'.JPG'、'.GIF' 或'.PNG'的格式上載，相片大小也規定為100 X 100 pixel (闊 x 高)。";
$i_InventorySystem_PurchasePriceGuide = "代表整張單據售價總數。";
$i_InventorySystem_ImportCaretaker_Warning = "系統只會匯入所選組別內的教職員";

# added on 27 Dec 2007 #
$i_InventorySystem_Setting_BarcodeMaxLength1 = "條碼長度";
$i_InventorySystem_Setting_BarcodeMaxLength2 = "字元";
$i_InventorySystem_Setting_BarcodeFormat = "條碼格式";
$i_InventorySystem_Setting_BarcodeFormat_NumOnly = "只使用數字，特定字元('-','$','|','.','/')及空白鍵";
$i_InventorySystem_Setting_BarcodeFormat_BothNumAndChar = "使用數字(0-9)，大楷字母(A-Z)特定字元('-','$','|','.','/')及空白鍵";
$i_InventorySystem_ItemWarrantyExpiryWarning = "以下物品的保用證即將到期";

# added on 28 Dec 2007 #
$i_InventorySystem_WriteOffItem = "報銷物品";
$i_InventorySystem_WriteOffQty = "數量 (報銷)";

# added on 2 Jan 2008 #
$i_InventorySystem_Input_Item_PurchasePrice_Warning = "請輸入有效購入價錢";

# added on 3 Jan 2008 #
$i_InventorySystem_Input_Item_Generate_Item_Code = "產生物品編號";
$i_InventorySystem_Input_Item_Generate_Item_Barcode = "產生條碼";

# added on 4 Jan 2008 #
$i_InventorySystem_Report_ItemFinishStockCheck = "物品點算";

# added on 8 Jan 2008 #
$i_InventorySystem_Search_Result = "搜尋結果";
$i_InventorySystem_ViewItem_All_Location_Level = "所有位置";
$i_InventorySystem_ViewItem_All_Resource_Management_Group = "所有管理小組";
$i_InventorySystem_ViewItem_All_Finding_Source = "所有資金來源";
$i_InventorySystem_Item_Update_Status = "更新狀況";

# added on 9 Jan 2008 #
$i_InventorySystem_Location_Code = "子位置編號";
$i_InventorySystem_Location_Level_Code = "位置編號";
$i_InventorySystem_Caretaker_Code = "小組編號";
$i_InventorySystem_Funding_Code = "資金編號";

# added on 10 Jan 2008 #
$i_InventorySystem_RequestWriteOff = "申請報銷";

# added on 10 Jan 2008 #
$i_InventorySystem_WriteOffReason_Name = "報銷原因";

# added on 15 Jan 2008 #
$i_InventorySystem_StockTake_SelectItemStatus_Warning = "請選擇物品狀況";

# added on 16 Jan 2008 #
$i_InventorySystem_ItemStatus_Lost = "遺失";
$i_InventorySystem_ItemStatus_Surplus = "過剩";
$i_InventorySystem_Btn_Generate = "產生";
$i_InventorySystem_Unit_Price = "單位價格";
$i_InventorySystem_Group_MemberPosition = "職位";
$i_InventorySystem_Group_MemberPosition_Leader = "組長";
$i_InventorySystem_Group_MemberPosition_Member = "組員";
$i_InventorySystem_Group_Member_LeaderRemark = "組長";
$i_InventorySystemItemStatus = "物品狀況";
$i_InventorySystemItemStatusWriteOff = "已報銷";
$i_InventorySystemItemStatusNonWriteOff = "未報銷";
$i_InventorySystemItemStatusAll = "全部";
$i_InventorySystemItemMaintainInfo = "維修資料";
$i_InventorySystem_Approval = "資產管理批核";
$i_InventorySystem_WriteOffItemApproval = "物品報銷批核";

$i_InventorySystem_Error = "錯誤";


# added on 17 Jan 2008 #
$i_InventorySystem_Input_Item_ExistingItemCode = "現有物品編號";

# added on 18 Jan 2008 #
$i_InventorySystem_Input_Item_Barcode_Exist_Warning = "剛輸入的條碼已被使用。請輸入另一個。";
$i_InventorySystem_Input_Category_Item_Name_Exist_Warning = "剛輸入的名稱已被使用。請輸入另一個。";
$i_InventorySystem_Input_Category_Code_Exist_Warning = "剛輸入的類別編號已被使用。請輸入另一個。";
$i_InventorySystem_Last_Stock_Take_Time = "最後盤點時間";
$i_InventorySystem_Write_Off_RequestTime = "申請日期";
$i_InventorySystem_Write_Off_RequestQty = "申請數量";
$i_InventorySystem_Write_Off_Reason = "報銷原因";

# added on 21 Jan 2008 #
$i_InventorySystem_Export_Select_Column = "請選擇所需的物品資料：";
$i_InventorySystem_Item_TagCode = "條碼";

# added on 22 Jan 2008 #
$i_InventorySystem_Report_ItemWrittenOff = "已報銷物品";

# added on 31 Jan 2008 #
$i_InventorySystem_StocktakeVarianceHandling = "盤點誤差處理";

# added on 4 Feb 2008 #
$i_InventorySystem_Resource_Management_GroupLeader = "組長";
$i_InventorySystem_SetAsGroupLeader = "設定為組長";
$i_InventorySystem_SetAsGroupMember = "設定為組員";
$i_InventorySystem_Report_FixedAssetsRegister = "固定資產登記冊";
$i_InventorySystem_Report_ItemStatus = "物品狀況";
$i_InventorySystem_Input_Category_Code_RegExp_Warning = "請輸入有效的類別編號";
$i_InventorySystem_Input_SubCategory_Code_RegExp_Warning = "請輸入有效的類別編號";
$i_InventorySystem_Input_SubCategory_Item_Name_Exist_Warning = "剛輸入的名稱已被使用。請輸入另一個。";
$i_InventorySystem_Input_SubCategory_Code_Exist_Warning = "剛輸入的類別編號已被使用。請輸入另一個。";
$i_InventorySystem_Input_LocationLevel_Code_RegExp_Warning = "請輸入有效的位置編號";
$i_InventorySystem_Input_LocationLevel_Name_Exist_Warning = "剛輸入的名稱已被使用。請輸入另一個。";
$i_InventorySystem_Input_LocationLevel_Code_Exist_Warning = "剛輸入的位置編號已被使用。請輸入另一個。";
$i_InventorySystem_Input_Location_Code_RegExp_Warning = "請輸入有效的子位置編號";
$i_InventorySystem_Input_Location_Name_Exist_Warning = "剛輸入的名稱已被使用。請輸入另一個。";
$i_InventorySystem_Input_Location_Code_Exist_Warning = "剛輸入的子位置編號已被使用。請輸入另一個。";
$i_InventorySystem_Input_Group_Code_RegExp_Warning = "請輸入有效的小組編號";
$i_InventorySystem_Input_Group_Name_Exist_Warning = "剛輸入的名稱已被使用。請輸入另一個。";
$i_InventorySystem_Input_Group_Code_Exist_Warning = "剛輸入的小組編號已被使用。請輸入另一個。";
$i_InventorySystem_Input_Funding_Code_RegExp_Warning = "請輸入有效的資金來源編號";
$i_InventorySystem_Input_Funding_Name_Exist_Warning = "剛輸入的名稱已被使用。請輸入另一個。";
$i_InventorySystem_Input_Funding_Code_Exist_Warning = "剛輸入的資金來源編號已被使用。請輸入另一個。";

# added on 6 Feb 2008 #
$i_InventorySystem_EditItem_NoEditRightWarning = "您不能編輯該物品";
$i_InventorySystem_Report_Col_Item = "物品";
$i_InventorySystem_Report_Col_Cost = "成本";
$i_InventorySystem_Report_Col_Description = "詳情";
$i_InventorySystem_Report_Col_Purchase_Date = "購買日期";
$i_InventorySystem_Report_Col_Govt_Fund = "政府經費";
$i_InventorySystem_Report_Col_Sch_Fund = "學校經費";
$i_InventorySystem_Report_Col_Sch_Unit_Price = "單位價格";
$i_InventorySystem_Report_Col_Sch_Purchase_Price = "價值";
$i_InventorySystem_Report_Col_Quantity = "數量";
$i_InventorySystem_Report_Col_Location = "放置地點";
$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off = "報銷日期及理由";
$i_InventorySystem_Report_Col_Signature = "校監/校長簽署批准日期";
$i_InventorySystem_Report_Col_Remarks = "備註(發票編號及公司名稱)";

# added on 11 Feb 2008 #
$i_InventorySystem_Input_Item_ItemPrice_Warning = "請輸入有效單位價格";

# added on 12 Feb 2008 #
$i_InventorySystem_StockTakeResult = "盤點結果";

# added on 13 Feb 2008 #
$i_InventorySystem_FileFormat = "檔案類型";
$i_InventorySystem_ViewItem_All_Category = "所有類別";
$i_InventorySystem_Setting_BarcodeSetting = "條碼格式";
$i_InventorySystem_Input_ItemCode_Exist_Warning = "剛輸入的物品編號已被使用。請輸入另一個。";

# added on 14 Feb 2008 #
$i_InventorySystem_MissingQty = "遺失數量";
$i_InventorySystem_SurplusQty = "多出數量";
$i_InventorySystem_TotalMissingQty = "總遺失數量";
$i_InventorySystem_TotalSurplusQty = "總多出數量";
$i_InventorySystem_RecordDate = "記錄日期";
$i_InventorySystem_RequestPerson = "申請人";
$i_InventorySystem_ApprovePerson = "批准人";

# added on 15 Feb 2008 #
$i_InventorySystem_ApproveDate = "批准日期";
$i_InventorySystem_Write_Off_Attachment = "報銷附件";
$i_InventorySystem_Item_UpdateStatus = "更新狀況";
$i_InventorySystem_Item_UpdateLocation = "更改位置";
$i_InventorySystem_Item_RequestWriteOff = "申請報銷";

# added on 19 Feb 2008 #
$i_InventorySystem_Search_Purchase_Price_Range = "總購入金額範圍";
$i_InventorySystem_Search_Unit_Price_Range = "單位價格範圍";
$i_InventorySystem_From = "由";
$i_InventorySystem_To = "至";
$i_InventorySystem_BarcodeLabels = "條碼標籤";
$i_InventorySystem_Report_Stocktake = "盤點";
$i_InventorySystem_NumOfItemAdd = "數量";
$i_InventorySystem_AddNewItem_Step1 = "輸入基本物品資料";
$i_InventorySystem_AddNewItem_Step2 = "輸入詳細物品資料";
$i_InventorySystem_Report_Stocktake_Progress = "盤點進度";
$i_InventorySystem_Stocktake_ProgressFinished = "已完成";
$i_InventorySystem_Stocktake_ProgressNotFinished = "未完成";

# added on 20 Feb 2008 #
$i_InventorySystem_ViewItem_All_Category = "所有類別";
$i_InventorySystem_DisplayAllImage = "顯示圖片";
$i_InventorySystem_Setting_StockCheckDateRange = "選擇日期範圍";
$i_InventorySystem_Funding_Type = "資金類別";
$i_InventorySystem_Funding_Type_School = "學校";
$i_InventorySystem_Funding_Type_Government = "政府";
$i_InventorySystem_Funding_Type_Array = array(
array(1,$i_InventorySystem_Funding_Type_School),
array(2,$i_InventorySystem_Funding_Type_Government));
$i_InventorySystem_ItemType_All = "所有種類";
$i_InventorySystem_HideAllImage = "隱藏圖片";
$i_InventorySystem_Stocktake_List = "盤點清單";
$i_InventorySystem_Stocktake_ReadBarcode = "輸入或掃瞄條碼";
$i_InventorySystem_Stocktake_LastStocktakeBy= "最後盤點職員";
$i_InventorySystem_Status = "狀況";
$i_InventorySystem_Stocktake_TotalQty = "總數";
$i_InventorySystem_Stocktake_FoundQty = "找到";
$i_InventorySystem_Stocktake_UnlistedQty = "不明";
$i_InventorySystem_Stocktake_ExpectedQty = "預期數量";
$i_InventorySystem_Stocktake_StocktakeQty = "盤點數量";
$i_InventorySystem_Stocktake_NormalQty = "正常";
$i_InventorySystem_Stocktake_RequestWriteOffQty = "申請報銷";
$i_InventorySystem_Stocktake_VarianceQty = "誤差";
$i_InventorySystem_Stocktake_MissingRemark = "未能找到";
$i_InventorySystem_Stocktake_UnlistedItem = "不明物品";
$i_InventorySystem_Stocktake_ExpectedLocation = "預期位置";
$i_InventorySystem_Stocktake_Dislocated_Item = "位置錯誤";
$i_InventorySystem_Stocktake_ItemNotExist = "沒有物品記錄";
$i_InventorySystem_Stocktake_ReasonAndAttachment = "原因及附件";
$i_InventorySystem_VarianceHandling_ChangeLocation = "更改位置";
$i_InventorySystem_VarianceHandling_Qty = "數量";
$i_InventorySystem_Report_StocktakeProgress = "盤點進度";

$i_InventorySystem_CategoryImport_Format1_Row1 = "第一欄 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_CategoryImport_Format1_Row2 = "第二欄 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_CategoryImport_Format1_Row3 = "第三欄 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_CategoryImport_Format1_Row4 = "第四欄 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_Category2Import_Format1_Row1 = "第一欄 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_Category2Import_Format1_Row2 = "第二欄  : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_Category2Import_Format1_Row3 = "第三欄 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_Category2Import_Format1_Row4 = "第四欄 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_Category2Import_Format1_Row8 = "第八欄 : $i_InventorySystem_Category_DisplayOrder";
$i_InventorySystem_Category2Import_Format1_Row5 = "第五欄 : $i_InventorySystem_Category2_License (1 - 適用, 0 - 不適用)";
$i_InventorySystem_Category2Import_Format1_Row6 = "第六欄 : $i_InventorySystem_Category2_Warranty (1 - 適用, 0 - 不適用)";

$i_InventorySystem_LocationImport_Format1_Row1 = "第一欄 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_LocationImport_Format1_Row2 = "第二欄 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_LocationImport_Format1_Row3 = "第三欄 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_LocationImport_Format1_Row4 = "第四欄 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_Location2Import_Format1_Row1 = "第一欄 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_Location2Import_Format1_Row2 = "第二欄 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_Location2Import_Format1_Row3 = "第三欄 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_Location2Import_Format1_Row4 = "第四欄 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_Location2Import_Format1_Row5 = "第五欄 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_FundingImport_Format1_Row1 = "第一欄 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_FundingImport_Format1_Row2 = "第二欄 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_FundingImport_Format1_Row3 = "第三欄 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_FundingImport_Format1_Row4 = "第四欄 : $i_InventorySystem_Funding_Type (e.g School, Government)";
$i_InventorySystem_FundingImport_Format1_Row5 = "第五欄 : $i_InventorySystem_Category_DisplayOrder";


$i_InventorySystem_CategoryImportError[1] = $i_InventorySystem_Item_CategoryCode."已存在";
$i_InventorySystem_CategoryImportError[2] = "沒有填上".$i_InventorySystem_Item_CategoryCode;
$i_InventorySystem_CategoryImportError[3] = "沒有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_CategoryImportError[4] = "沒有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_CategoryImportError[5] = "沒有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_CategoryImportError[19] = $i_InventorySystem_Item_CategoryCode."已存在";
$i_InventorySystem_CategoryImportError[20] = $i_InventorySystem_Item_CategoryCode."長度必須在5個字以內";

$i_InventorySystem_Category2ImportError[1] = $i_InventorySystem_Item_CategoryCode."無效";
$i_InventorySystem_Category2ImportError[2] = "沒有填上".$i_InventorySystem_Item_CategoryCode;
$i_InventorySystem_Category2ImportError[3] = "沒有填上".$i_InventorySystem_Item_Category2Code;
$i_InventorySystem_Category2ImportError[4] = "沒有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_Category2ImportError[5] = "沒有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_Category2ImportError[6] = "沒有填上".$i_InventorySystem_Category2_License;
$i_InventorySystem_Category2ImportError[7] = "沒有填上".$i_InventorySystem_Category2_Warranty;
$i_InventorySystem_Category2ImportError[8] = "沒有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_Category2ImportError[9] =  $i_InventorySystem_Item_Category2Code."已存在";
$i_InventorySystem_Category2ImportError[19] = $i_InventorySystem_Item_Category2Code."已存在";
$i_InventorySystem_Category2ImportError[22] = $i_InventorySystem_Item_Category2Code."長度必須在5個字以內";

$i_InventorySystem_LocationLevelImportError[1] = $i_InventorySystem_Item_LocationCode."已存在";
$i_InventorySystem_LocationLevelImportError[2] = "沒有填上".$i_InventorySystem_Item_LocationCode;
$i_InventorySystem_LocationLevelImportError[3] = "沒有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_LocationLevelImportError[4] = "沒有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_LocationLevelImportError[5] = "沒有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_LocationLevelImportError[19] = $i_InventorySystem_Item_LocationCode."已存在";

$i_InventorySystem_LocationImportError[1] = $i_InventorySystem_Item_LocationCode."無效";
$i_InventorySystem_LocationImportError[2] = $i_InventorySystem_Item_Location2Code."已存在";
$i_InventorySystem_LocationImportError[3] = "沒有填上".$i_InventorySystem_Item_LocationCode;
$i_InventorySystem_LocationImportError[4] = "沒有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_LocationImportError[5] = "沒有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_LocationImportError[6] = "沒有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_LocationImportError[19] = $i_InventorySystem_Item_LocationCode."已存在";
$i_InventorySystem_LocationImportError[9] = $i_InventorySystem_Item_Location2Code."已存在";

$i_InventorySystem_FundingImportError[1] = $i_InventorySystem_Item_FundingSourceCode."已存在";
$i_InventorySystem_FundingImportError[2] = "沒有填上".$i_InventorySystem_Item_FundingSourceCode;
$i_InventorySystem_FundingImportError[3] = "沒有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_FundingImportError[4] = "沒有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_FundingImportError[5] = "沒有填上".$i_InventorySystem_Funding_Type;
$i_InventorySystem_FundingImportError[6] = "沒有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_FundingImportError[7] = $i_InventorySystem_Funding_Type."無效";
$i_InventorySystem_FundingImportError[8] = $i_InventorySystem_Category_DisplayOrder."無效";
$i_InventorySystem_FundingImportError[19] = $i_InventorySystem_Funding_Type."已存在";

# Added on 4 Mar 2008 #
$i_InventorySystem_VarianceHandling_Action[1] = "搬回原來位置";
$i_InventorySystem_VarianceHandling_Action[2] = "重新設定位置";
$i_InventorySystem_VarianceHandling_Action[3] = "要求報銷";
$i_InventorySystem_VarianceHandling_Action[4] = "新增物品";
$i_InventorySystem_VarianceHandling_Action[5] = "忽略";

# Added on 6 Mar 2008 #
$i_InventorySystem_Report_Stocktake_groupby = "顯示次序";

# Added on 7 Mar 2008 #
$i_InventorySystem['item_type'] = "類型";

# added on 10 Mar 2008 #
$i_InventorySystem_WriteOff_Reason_LostItem = "找不到"; #"盤點時遺失";


$i_InventorySystem_VarianceHandling_AddAsNewItemQty = "新增物品數量";
$i_InventorySystem_VarianceHandling_IgnoreQty = "忽略數量";

# added on 12 Mar 2008 #
$i_InventorySystem['jsLocationCheckBox'] = "請選擇位置";
$i_InventorySystem['jsGroupCheckBox'] = "請選擇組別";
$i_InventorySystem['jsLocationGroupCheckBox'] = "請選擇位置/組別";

# added on 13 Mar 2008 #
$i_InventorySystem['VarianceHandlingNotice'] = "誤差處理通知";
$i_InventorySystem_VarianceHandlingNotice_CurrentLocation = "現時位置";
$i_InventorySystem_VarianceHandlingNotice_NewLocation = "新位置";
$i_InventorySystem_VarianceHandlingNotice_Sender = "發送者";
$i_InventorySystem_VarianceHandlingNotice_Handler = "處理者";
$i_InventorySystem_VarianceHandlingNotice_SendDate = "發送日期";

# added on 14 Mar 2008 #
$i_InventorySystem_BackToFullList = "返回物品清單";

# added on 17 Mar 2008 #
$i_InventorySystem_SubCategory_Code = "子類別編號";
$i_InventorySystem_SubCategory_Name = "子類別名稱";
$i_InventorySystem_Setting_Location_ChineseName = "位置名稱 (中文)";
$i_InventorySystem_Setting_Location_EnglishName = "位置名稱 (英文)";
$i_InventorySystem_Setting_SubLocation_ChineseName = "子位置名稱 (中文)";
$i_InventorySystem_Setting_SubLocation_EnglishName = "子位置名稱 (英文)";
$i_InventorySystem_Setting_NewLocation = "新增位置";
$i_InventorySystem_Setting_NewSubLocation  = "新增子位置";
$i_InventorySystem_Setting_EditLocation = "編輯位置";
$i_InventorySystem_Setting_EditSubLocation  = "編輯子位置";
$i_InventorySystem_Setting_NewManagementGroup = "新增物品管理小組";
$i_InventorySystem_Setting_EditManagementGroup = "編輯物品管理小組";
$i_InventorySystem_Setting_ManagementGroup_ChineseName = "小組名稱 (中文)";
$i_InventorySystem_Setting_ManagementGroup_EnglishName = "小組名稱 (英文)";
$i_InventorySystem_Setting_ManagementGroup_NewMember = "新增組員";
$i_InventorySystem_Setting_ManagementGroup_SelectUser = "選擇用戶";
$i_InventorySystem_Setting_Funding_Name = "資金名稱";
$i_InventorySystem_Setting_NewFunding = "新增資金來源";
$i_InventorySystem_Setting_EditFunding = "編輯資金來源";
$i_InventorySystem_Setting_Funding_ChineseName = "資金名稱 (中文)";
$i_InventorySystem_Setting_Funding_EnglishName = "資金名稱 (英文)";
$i_InventorySystem_Setting_NewWriteOffReason = "新增報銷原因";
$i_InventorySystem_Setting_EditWriteOffReason = "編輯報銷原因";
$i_InventorySystem_Setting_WriteOffReason_ChineseName = "原因 (中文)";
$i_InventorySystem_Setting_WriteOffReason_EnglishName = "原因 (英文)";
$i_InventorySystem_Search_Warrany_Expiry_Between = "保用日期範圍";
$i_InventorySystem_Search_And = "至";
$i_InventorySystem_Search_And2 = "及";
$i_InventorySystem_Search_Purchase_Date_Between = "購買日期範圍";

# added on 20 Mar 2008 #
$i_InventorySystem_NewItem_PurchasePrice_Help = "<p>若交易涉及折扣，「總購入金額」未必等同「單位價格」乘以「數量」。</p><p>「總購入金額」一般為發票上的總金額，亦是你為整項交易所繳付的金額。</p><p>此金額將會列於「固定資產登記冊」的「總購入金額」欄上。</p>";
$i_InventorySystem_NewItem_UnitPrice_Help = "「單位價格」是此物品的原價，並會乘以「數量」以計算「固定資產登記冊」上「成本」欄的金額。";
$i_InventorySystem_MultiSelect_Help = "可按住CTRL鍵，點擊多個項目。";

# added on 28 Mar 2008 #
$i_InventorySystem_VarianceHandling_New_Quantity = "新數量";
$i_InventorySystem_VarianceHandling_Original_Quantity = "原有數量";

# added on 8 Apr 2008 #
$i_InventorySystem_Send_Reminder = "發送提示";

# added on 15 Apr 2008 #
$i_InventorySystem_ImportItem_Format1_Title = "<span class=extraInfo>匯入$i_InventorySystem_ItemType_Single</span><Br><Br>";
$i_InventorySystem_ImportItem_Format1_Row1 = "第一欄 : $i_InventorySystem_Item_Code";
$i_InventorySystem_ImportItem_Format1_Row2 = "第二欄 : $i_InventorySystem_Item_Barcode";
$i_InventorySystem_ImportItem_Format1_Row3 = "第三欄 : $i_InventorySystem_Item_ChineseName";
$i_InventorySystem_ImportItem_Format1_Row4 = "第四欄 : $i_InventorySystem_Item_EnglishName";
$i_InventorySystem_ImportItem_Format1_Row5 = "第五欄 : $i_InventorySystem_Item_ChineseDescription";
$i_InventorySystem_ImportItem_Format1_Row6 = "第六欄 : $i_InventorySystem_Item_EnglishDescription";
$i_InventorySystem_ImportItem_Format1_Row7 = "第七欄 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_ImportItem_Format1_Row8 = "第八欄 : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_ImportItem_Format1_Row9 = "第九欄 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_ImportItem_Format1_Row10 = "第十欄 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_ImportItem_Format1_Row11 = "第十一欄 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_ImportItem_Format1_Row12 = "第十二欄 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_ImportItem_Format1_Row13 = "第十三欄 : $i_InventorySystem_Item_Ownership (1 - 學校, 2 - 政府, 3 - 辦學團體)";
$i_InventorySystem_ImportItem_Format1_Row14 = "第十四欄 : $i_InventorySystem_Item_Warrany_Expiry (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format1_Row15 = "第十五欄 : $i_InventorySystem_Item_License";
$i_InventorySystem_ImportItem_Format1_Row16 = "第十六欄 : $i_InventorySystem_Item_Serial_Num";
$i_InventorySystem_ImportItem_Format1_Row17 = "第十七欄 : $i_InventorySystem_Item_Brand_Name";
$i_InventorySystem_ImportItem_Format1_Row18 = "第十八欄 : $i_InventorySystem_Item_Supplier_Name";
$i_InventorySystem_ImportItem_Format1_Row19 = "第十九欄 : $i_InventorySystem_Item_Supplier_Contact";
$i_InventorySystem_ImportItem_Format1_Row20 = "第二十欄 : $i_InventorySystem_Item_Supplier_Description";
$i_InventorySystem_ImportItem_Format1_Row21 = "第二十一欄 : $i_InventorySystem_Item_Quot_Num";
$i_InventorySystem_ImportItem_Format1_Row22 = "第二十二欄 : $i_InventorySystem_Item_Tender_Num";
$i_InventorySystem_ImportItem_Format1_Row23 = "第二十三欄 : $i_InventorySystem_Item_Invoice_Num";
$i_InventorySystem_ImportItem_Format1_Row24 = "第二十四欄 : $i_InventorySystem_Item_Purchase_Date (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format1_Row25 = "第二十五欄 : $i_InventorySystem_Item_Price";
$i_InventorySystem_ImportItem_Format1_Row26 = "第二十六欄 : $i_InventorySystem_Unit_Price";
$i_InventorySystem_ImportItem_Format1_Row27 = "第二十七欄 : $i_InventorySystemItemMaintainInfo";
$i_InventorySystem_ImportItem_Format1_Row28 = "第二十八欄 : $i_InventorySystem_Item_Remark";

$i_InventorySystem_ImportItem_Format2_Title = "<span class=extraInfo>匯入$i_InventorySystem_ItemType_Bulk</span><Br><Br>";
$i_InventorySystem_ImportItem_Format2_Row1 = "第一欄 : $i_InventorySystem_Item_Code";
$i_InventorySystem_ImportItem_Format2_Row2 = "第二欄 : $i_InventorySystem_Item_ChineseName";
$i_InventorySystem_ImportItem_Format2_Row3 = "第三欄 : $i_InventorySystem_Item_EnglishName";
$i_InventorySystem_ImportItem_Format2_Row4 = "第四欄 : $i_InventorySystem_Item_ChineseDescription";
$i_InventorySystem_ImportItem_Format2_Row5 = "第五欄 : $i_InventorySystem_Item_EnglishDescription";
$i_InventorySystem_ImportItem_Format2_Row6 = "第六欄 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_ImportItem_Format2_Row7 = "第七欄 : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_ImportItem_Format2_Row8 = "第八欄 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_ImportItem_Format2_Row9 = "第九欄 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_ImportItem_Format2_Row10 = "第十欄 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_ImportItem_Format2_Row11 = "第十一欄 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_ImportItem_Format2_Row12 = "第十二欄 : $i_InventorySystem_Item_Ownership (1 - 學校, 2 - 政府, 3 - 辦學團體)";
$i_InventorySystem_ImportItem_Format2_Row13 = "第十三欄 : $i_InventorySystem_Item_BulkItemAdmin";
$i_InventorySystem_ImportItem_Format2_Row14 = "第十四欄 : $i_InventorySystem_Item_Qty";
$i_InventorySystem_ImportItem_Format2_Row15 = "第十五欄 : $i_InventorySystem_Item_Supplier_Name";
$i_InventorySystem_ImportItem_Format2_Row16 = "第十六欄 : $i_InventorySystem_Item_Supplier_Contact";
$i_InventorySystem_ImportItem_Format2_Row17 = "第十七欄 : $i_InventorySystem_Item_Supplier_Description";
$i_InventorySystem_ImportItem_Format2_Row18 = "第十八欄 : $i_InventorySystem_Item_Quot_Num";
$i_InventorySystem_ImportItem_Format2_Row19 = "第十九欄 : $i_InventorySystem_Item_Tender_Num";
$i_InventorySystem_ImportItem_Format2_Row20 = "第二十欄 : $i_InventorySystem_Item_Invoice_Num";
$i_InventorySystem_ImportItem_Format2_Row21 = "第二十一欄 : $i_InventorySystem_Item_Purchase_Date (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format2_Row22 = "第二十二欄 : $i_InventorySystem_Item_Price";
$i_InventorySystem_ImportItem_Format2_Row23 = "第二十三欄 : $i_InventorySystem_Unit_Price";
$i_InventorySystem_ImportItem_Format2_Row24 = "第二十四欄 : $i_InventorySystemItemMaintainInfo";
$i_InventorySystem_ImportItem_Format2_Row25 = "第二十五欄 : $i_InventorySystem_Item_Remark";

# added on 16 Apr 2008 #
$i_InventorySystem_ImportItem_RowWithRecord = "有記錄行數";
$i_InventorySystem_ImportItem_TotalRow = "總行數";
$i_InventorySystem_ImportItem_EmptyRowWarning = "CSV檔案不能有空白行";
$i_InventorySystem_ImportItem_EmptyRowRecord = "空白行數";

# added on 17 Apr 2008 #
$i_InventorySystem_UpdateSingleItemStatusWarning = "請選擇狀況";
$i_InventorySystem_UpdateBulkItemStatusWarning_Normal = "請輸入有效的正常數量";
$i_InventorySystem_UpdateBulkItemStatusWarning_Damaged = "請輸入有效的損壞數量";
$i_InventorySystem_UpdateBulkItemStatusWarning_Reparing = "請輸入有效的維修數量";
$i_InventorySystem_UpdateItemRequestWriteOffQty_Warning = "請輸入有效的報銷數量";
$i_InventorySystem_UpdateItemRequestWriteOffReason_Warning = "請選擇報銷原因";

# added on 18 Apr 2008 #
$i_InventorySystem_GroupImport_Format1_Row1 = "第一欄 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_GroupImport_Format1_Row2 = "第二欄 : $i_InventorySystem_Setting_ManagementGroup_ChineseName";
$i_InventorySystem_GroupImport_Format1_Row3 = "第三欄 : $i_InventorySystem_Setting_ManagementGroup_EnglishName";
$i_InventorySystem_GroupImport_Format1_Row4 = "第四欄 : $i_InventorySystem_Category_DisplayOrder";


# added on 21 Apr 2008 #
$i_InventorySystem_Import_CorrectLocationCodeWarning2 = " * 請小心核對csv匯入檔之位置編號是否如上相同。";
$i_InventorySystem_Import_CorrectCategoryCodeWarning2 = " * 請小心核對csv匯入檔之類別編號是否如上相同。";
$i_InventorySystem_Import_CorrectGroupCodeWarning = " * 以上之管理組別編號已在使用，請使用新的管理組別編號進行匯入。";

# added on 24 Apr 2008 #
$i_InventorySystem_Category2_Serial = "序號";
$i_InventorySystem_Category2Import_Format1_Row7 = "第七欄 : $i_InventorySystem_Category2_Serial (1 - 適用, 0 - 不適用)";
$i_InventorySystem_Category2ImportError[10] = "沒有填上".$i_InventorySystem_Category2_Serial;

# added on 28 Apr 2008 #
$i_InventorySystem_Export_Search_Result_Column = "請選擇所需的資料：";

# added on 2 May 2008 #
$i_InventorySystem_Settings_BulkItemAdmin = "誤差總管";
$i_InventorySystem_GroupMemberImport_Format1_Row1 = "第一欄 : 管理組別編號";
$i_InventorySystem_GroupMemberImport_Format1_Row2 = "第二欄 : 內聯網帳號";
$i_InventorySystem_GroupMemberImport_Format1_Row3 = "第三欄 : 身分 (1 - 組長, 2 - 組員)";
$i_InventorySystem_Import_CorrectGroupCodeWarning2 = " * 請小心核對csv匯入檔之管理組別編號是否如上相同。";
$i_InventorySystem_GroupMemberImportError[1] = $i_InventorySystem_Item_GroupCode."無效";
$i_InventorySystem_GroupMemberImportError[2] = "沒有填上".$i_InventorySystem_Item_GroupCode;
$i_InventorySystem_GroupMemberImportError[3] = "沒有填上".$i_UserLogin;
$i_InventorySystem_GroupMemberImportError[4] = $i_UserLogin."無效";
$i_InventorySystem_GroupMemberImportError[5] = "沒有填上".$i_identity;
$i_InventorySystem_GroupMemberImportError[6] = $i_identity."無效";
$i_InventorySystem_GroupMemberImportError[7] = $i_UserLogin."已存在";

# added on 5 May 2008 #
$i_InvertorySystem_SelectOriginalLocation_Warning = "請選擇原有位置";
$i_InvertorySystem_SelectOriginalGroup_Warning = "請選擇原有管理組別";
$i_InvertorySystem_SelectNewLocation_Warning = "請選擇新位置";
$i_InvertorySystem_SelectNewGroup_Warning = "請選擇新管理組別";
$i_InventorySystem_Notice = "資產管理通告";
$i_InventorySystem_Input_ItemName_Warning = "請輸入物品名稱";

# added on 7 May 2008 #
$i_InventorySystem_VarianceHandle_ReAssignLocationQuantity = "重新分配各位置的數量";
$i_InventorySystem_VarianceHandle_TotalAvaliableQuantity = "可分配數量";
$i_InventorySystem_VarianceHandle_TotalAssignedQuantity = "已分配數量";
$i_InventorySystem_VarianceHandle_TotalRemainingQuantity = "餘下數量";
$i_InventorySystem_VarianceHandle_TotalIgnoreQuantity = "忽略數量";
$i_InventorySystem_VarianceHandle_TotalWriteOffQuantity = "報銷數量";
$i_InventorySystem_VarianceHandle_InputWriteOffQuantity = "請輸入各位置的報銷數量";
$i_InventorySystem_VarianceHandle_InputWriteOffQty_Warning = "輸入的報銷數量無效";
$i_InventorySystem_VarianceHandle_InputReAssignQty_Warning = "重新分配各的位置數量無效";
$i_InventorySystem_VarianceHandle_InputReAssignQty_Warning = "重新分配各位置的數量與可分配數量不符";
$i_InventorySystem_VarianceHandle_QuantityRemaining_Warning = "餘下數量";
$i_InventorySystem_VarianceHandle_SelectHandlerWarning = "請選擇處理者";
$i_InventorySystem_VarianceHandle_SelectActionWarning = "請選擇動作";
$i_InventorySystem_VarianceHandle_InputReAssignQtyWarning = "請輸入各位置的數量";
$i_InventorySystem_VarianceHandle_ButtonCalculate = "計算";
$i_InventorySystem_VarianceHandle_ClickCalculateWarning = "請先按$i_InventorySystem_VarianceHandle_ButtonCalculate"."以便核對數量";
$i_InventorySystem_NewItem_BulkItemAdmin_Warning = "請選擇誤差總管";
$i_InventorySystem_UpdateItemLocation_OriginalLocation = "原有位置";
$i_InventorySystem_UpdateItemLocation_OriginalGroup = "原有管理組別";
$i_InventorySystem_UpdateItemLocation_OriginalQuantity = "原有數量";
$i_InventorySystem_UpdateItemLocation_NewLocation = "新位置";
$i_InventorySystem_UpdateItemLocation_NewGroup = "新管理組別";
$i_InventorySystem_UpdateItemLocation_NewQuantity = "新數量";
$i_InventorySystem_SettingOthers_StocktakePeriod_Warning = "盤點日期無效";
$i_InventorySystem_SettingOthers_ReminderDay_Warning = "請輸入保用證到期提示日數";
$i_InventorySystem_SettingOthers_BarcodeLength_Warning = "請輸入條碼長度";
$i_InventorySystem_Setting_Catgeory_DeleteFail = "刪除紀錄失敗，該類別已被使用。";
$i_InventorySystem_Setting_SubCatgeory_DeleteFail = "刪除紀錄失敗，該子類別已被使用。";
$i_InventorySystem_Setting_Location_DeleteFail = "刪除紀錄失敗，該位置已被使用。";
$i_InventorySystem_Setting_SubLocation_DeleteFail = "刪除紀錄失敗，該子位置已被使用。";
$i_InventorySystem_Setting_ResourceManagementGroup_DeleteFail = "刪除紀錄失敗，該管理小組已被使用。";
$i_InventorySystem_Setting_FundingSource_DeleteFail = "刪除紀錄失敗，該資金來源已被使用。";
$i_InventorySystem_ItemFullList_DeleteItemFail = "刪除紀錄失敗，該物品仍在使用中。";
$i_InventorySystem_ItemFullList_Cannot_New_Item = "無法新增物品。請前往<b>設定</b>，設置好<b>物品類別</b>、<b>位置/子位置</b>、<b>物品管理小組</b>及<b>資金來源 </b>後，重新新增物品。";

### added on 14 May 2008 ###
$i_InventorySystem_UpdateBulkItemStatus_Warning = "你所輸入的物品數量大於/小於表列數量。確定繼續?";
$i_InventorySystem_Import_CheckVarianceManager = "按此查詢".$i_InventorySystem_Item_BulkItemAdmin;

### added on 15 May 2008 ###
$i_InventorySystem_VarianceHandling_CurrentLocation = "當前位置";
$i_InventorySystem_Report_Stocktake_NotDone = "還未進行盤點";
$i_InventorySystem_FullList_DeleteItemAlert = "該物品記錄將被永久移除，你將再不能存取該物品之相關資訊。如欲於物品清單上移除該物品，但保留物品之過往記錄，請註銷物品。";
$i_InventorySystem_NewItem_PurchaseType_Help = "如選擇\"現存物品\"，新增之物品必須與之後所選擇的現存物品屬於同一個物品管理小組及資金來源。如要新增之物品和已有物品名稱相同，但屬於不同的物品管理小組及資金來源，你仍應選擇\"新物品\"。";
$i_InventorySystem_NewItem_SelectLocationFisrt_MSG = "先選擇位置";
$i_InventorySystem_LabelPerPage = "每頁列印數量";
$i_InventorySystem_Report_PCS = "件";
$i_InventorySystem_Stocktake_NotIncludeWriteOffQty = "不包括申請報銷數量";

# added on 29 May 2008 #
$i_InventorySystem_SetAsResourceBookingItem = "設定為資源預訂頂目";
$i_InventorySystem_Warning_SetBulkItemAsResourceItem = "大量物品不能用於資源預訂";
$i_InventorySystem_ResourceItemsSetting = "資源頂目設定";

# added on 2 Jun 2008 #
$i_InventorySystem_RequestWriteOff_ResourceItemStatusSelection = "資源項目設定";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus1 = "刪除資源項目";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus2 = "將項目狀態轉為審批中";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus3 = "保留資源項目";
$i_InventorySystem_RequestWriteOff_ResourceItemConditionWarning = "請選擇資源頂目設定";
$i_InventorySystem_UpdateStatus_ResourceItemStatus = "資源狀況";

# added on 12 Jan 2009
$i_InventorySystem_Setting_ItemCodeFormat_Title = "物品編號格式";
$i_InventorySystem_Setting_ItemCodeFormat_Year = "購買年份";

# added on 22 Jan 2009
$i_InventorySystem_Setting_ItemCodeFormatSetting_Warning = "請先完成物品編號格式設定。";
$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup = "請選擇物品管理小組。";
$i_InventorySystem_JSWarning_NewItem_SelectLocation = "請選擇位置。";
$i_InventorySystem_JSWarning_NewItem_SelectSubLocation = "請選擇位置。";
$i_InventorySystem_JSWarning_NewItem_SelectFunding = "請選擇資金來源。";
$i_InventorySystem_JSWarning_NewItem_InvalidItemBarcode = "條碼無效，請重新輸入。";
$i_InventorySystem_JSWarning_NewItem_ItemCodeEmpty = "請輸入物品編號。";

# added on 4 Feb 2009
$i_InventorySystem_NewItem_ItemBarcodeRemark = "請參考 \"設定> 其他 > 條碼格式\" 中的條碼格式。";

# added on 20090213
$i_InventorySystem_ExportItemBarcode = "匯出條碼";

# added on 20090324
$i_InventorySystem_ImportFileFormatWarning = "請確保匯入的csv檔案為最新格式";
$i_InventorySystem_Setting_GroupLeaderRight = "組長權限";
$i_InventorySystem_Setting_GroupLeader_NotAllowAddItem = "不允許組長新增物品";
$i_InventorySystem_Setting_GroupLeader_AllowAddItem = "允許組長新增物品";
# added on 20090325
$i_InventorySystem_Setting_Photo_Use = "使用";
$i_InventorySystem_Setting_Photo_DoNotUse = "不使用";
# added on 20090331
$i_InventorySystem_Setting_Barcode_ChangedWarning = "注意：條碼格式已被更改。";

# added on 20090409
$i_InventorySystem_ItemType_Single_ExportTitle_ExportTitle = "Single Items";
$i_InventorySystem_ItemType_Bulk_ExportTitle = "Bulk Items";
$i_InventorySystem_Item_Code_ExportTitle = "Item Code";
$i_InventorySystem_Item_Barcode_ExportTitle = "Barcode";
$i_InventorySystem_Item_ChineseName_ExportTitle = "Item Chinese Name";
$i_InventorySystem_Item_EnglishName_ExportTitle = "Item English Name";
$i_InventorySystem_Item_ChineseDescription_ExportTitle = "Chinese Description";
$i_InventorySystem_Item_EnglishDescription_ExportTitle = "English Description";
$i_InventorySystem_Item_CategoryCode_ExportTitle = "Category Code";
$i_InventorySystem_Item_Category2Code_ExportTitle = "Sub-category Code";
$i_InventorySystem_Item_GroupCode_ExportTitle = "Group Code";
$i_InventorySystem_Item_LocationCode_ExportTitle = "Location Code";
$i_InventorySystem_Item_Location2Code_ExportTitle = "Sub-location Code";
$i_InventorySystem_Item_FundingSourceCode_ExportTitle = "Funding Source Code";
$i_InventorySystem_Item_Ownership_ExportTitle = "Ownership";
$i_InventorySystem_Item_Warrany_Expiry_ExportTitle = "Warranty Expiry Date";
$i_InventorySystem_Item_License_ExportTitle = "License";
$i_InventorySystem_Item_Serial_Num_ExportTitle = "Serial No";
$i_InventorySystem_Item_Brand_Name_ExportTitle = "Brand";
$i_InventorySystem_Item_Supplier_Name_ExportTitle = "Supplier";
$i_InventorySystem_Item_Supplier_Contact_ExportTitle = "Supplier Contact";
$i_InventorySystem_Item_Supplier_Description_ExportTitle = "Supplier Description";
$i_InventorySystem_Item_Quot_Num_ExportTitle = "Quotation No";
$i_InventorySystem_Item_Tender_Num_ExportTitle = "Tender No";
$i_InventorySystem_Item_Invoice_Num_ExportTitle = "Invoice No";
$i_InventorySystem_Item_Purchase_Date_ExportTitle = "Purchase Date";
$i_InventorySystem_Item_Price_ExportTitle = "Total Purchase Amount";
$i_InventorySystem_Unit_Price_ExportTitle = "Unit Price";
$i_InventorySystemItemMaintainInfo_ExportTitle = "Maintanence Details";
$i_InventorySystem_Item_Remark_ExportTitle = "Remarks";
$i_InventorySystem_Item_BulkItemAdmin_ExportTitle = "Variance Manager";
$i_InventorySystem_Item_Qty_ExportTitle = "Quantity";
$i_InventorySystem_Item_CategoryName_ExportTitle = "Category";
$i_InventorySystem_Item_Category2Name_ExportTitle = "Sub-category";
$i_InventorySystem_Item_GroupName_ExportTitle = "Resource Management Group";
$i_InventorySystem_Item_LocationName_ExportTitle = "Location";
$i_InventorySystem_Item_Location2Name_ExportTitle = "Sub-location";
$i_InventorySystem_Item_FundingSourceName_ExportTitle = "Funding Source";

## added on 20090416
$i_InventorySystem_FixedAssetsRegister_Warning = "當使用「位置」作為顯示次序時, 系統會依據個別位置中大量物品的數目，計算物品的單位價格、價值及成本，該等計算結果*可能*與實際數值有所偏差。如需準確了解單位價格、價值及成本, 請選用\"類型\"或\"物品管理小組\"作為顯示次序。";
$i_InventorySystem_ExportItemDetails1 = "匯出 (編號格式)";
$i_InventorySystem_ExportItemDetails2 = "匯出 (名稱格式)";
$i_InventorySystem_ExportWarning = "如沒有選擇任何物品, 系統將會匯出全部物品資料。確定繼續？";
$i_InventorySystem_CategorySetting_PriceCeiling = "價格上限";
$i_InventorySystem_CategorySetting_PriceCeiling_ApplyToBulk = "此設定適用於大量物品";
$i_InventorySystem_CategorySetting_PriceCeiling_JSWarning1 = "請輸入有效的價格上限";

## added on 20090527
$i_InventorySystem_DeleteItemPhoto = "刪除現時物品相片";

## added on 20090812
$i_InventorySystem_StocktakeType_AllItem = "顯示所有物品";
$i_InventorySystem_StocktakeType_StocktakeNotDone = "只顯示盤點未完成的";

### End Of eInventory Words ###

$i_StudentRegistry['System'] = "學籍紀錄系統";
$i_StudentRegistry_ModifiedSince = "上次更新日期";
$i_StudentRegistry_AuthCodeMain = "教青局學生編號";
$i_StudentRegistry_AuthCodeCheck = "校驗位";
$i_StudentRegistry_PlaceOfBirth = "出生地點";
$i_StudentRegistry_ID_Type = "身份證明文件種類";
$i_StudentRegistry_ID_No = "身份證明文件編號";
$i_StudentRegistry_ID_IssuePlace = "身份證明文件發出地點";
$i_StudentRegistry_ID_IssueDate = "身份證明文件本次發出日期";
$i_StudentRegistry_ID_ValidDate = "身份證明文件有效日期";
$i_StudentRegistry_SP_Type = "逗留許可";
$i_StudentRegistry_SP_No = "逗留許可編號";
$i_StudentRegistry_SP_IssueDate = "逗留許可本次發出日期";
$i_StudentRegistry_SP_ValidDate = "逗留許可有效日期";
$i_StudentRegistry_Province = "籍貫";
$i_StudentRegistry_ResidentialArea_Night = "夜間住宿地區";
$i_StudentRegistry_ResidentialArea = "居住地區";
$i_StudentRegistry_ResidentialRoad = "住址街名";
$i_StudentRegistry_ResidentialAddress = "住址門牌、大廈、層數、座";

$i_StudentRegistry_ExportPurpose = "匯出用途";
$i_StudentRegistry_ExportFormat_XML_1 = "用於批量查詢學生編號";
$i_StudentRegistry_ExportFormat_XML_2 = "用於學生資料升級(方案二)";
$i_StudentRegistry_ExportFormat_XML_3 = "用於學生資料升級(方案三/五)";

$i_StudentRegistry_EnglishName_Alert = "英名姓必需為大寫。現正被自動轉換。請儲存此變更。";

$i_UserAddress_Area = "居住地區";
$i_UserAddress_Road = "住址街名";
$i_UserAddress_Address = "住址門牌、大廈、層數、座";

$i_StudentGuardian_EmergencyContact = "緊急聯絡人";
$i_StudentGuardian_LiveTogether = "同住";
$i_StudentGuardian_Occupation = "職業";

$i_StudentRegistry_AreaCode = array();//see cust. template
$i_StudentRegistry_PlaceOfBirth_Code = array();//see cust. template
$i_StudentRegistry_ID_Type_Code = array();//see cust. template
$i_StudentRegistry_ID_IssuePlace_Code = array();//see cust. template
$i_StudentRegistry_Country_Code = array();//see cust. template
$i_StudentRegistry_ResidentialAreaNight_Code = array();//see cust. template


$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE1 = "EE1 - 特殊教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE2 = "EE2 - 特殊教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE3 = "EE3 - 特殊教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EEP = "EEP - 特殊教育";

$i_WebSAMS_AttendanceCode_Option_ClassLevel_1 = "1 - 葡文小學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_2 = "2 - 葡文小學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_3 = "3 - 葡文小學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_4 = "4 - 葡文小學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_5 = "5 - 葡文小學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_6 = "6 - 葡文小學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EP6 = "EP6 - 英文中學預備班";

$i_WebSAMS_AttendanceCode_Option_ClassLevel_F1 = "F1 - 英文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F2 = "F2 - 英文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F3 = "F3 - 英文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F4 = "F4 - 英文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F5 = "F5 - 英文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F6 = "F6 - 英文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_7 = "7 - 葡文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_8 = "8 - 葡文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_9 = "9 - 葡文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_10 = "10 - 葡文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_11 = "11 - 葡文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_12 = "12 - 葡文中學";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERP = "ERP - 小學回歸教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERG = "ERG - 初中回歸教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERC = "ERC - 高中回歸教育";

$ec_iPortfolio['guardian_import_remind_sms'] = "注意: <Br> - 第一欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - * 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定為SMS的接收者 (1 - 是, 0 - 否) <br> - 第九欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否) <br> * (01 - 父親, 02 - 母親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";

$ec_iPortfolio['guardian_import_remind_studentregistry'] = "注意: <Br> - 第一欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否) <Br> - 第九欄為監護人職業 <Br> - 第十欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> 第十一欄為地址區碼<Br> - 第十二欄為住址街道 <Br> - 第十三欄為住址其他資料，如大廈，層數及單位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父親, 02 - 親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> (O - 其他, M - 澳門, T - &#27705;仔, C - 路環) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";

$ec_iPortfolio['guardian_import_remind_sms_studentregistry'] = "注意: <Br> - 第一欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定為SMS的接收者 (1 - 是, 0 - 否) <br> - 第九欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否)  <Br> - 第十欄為監護人職業 <Br> - 第十一欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> 第十二欄為地址區碼<Br> - 第十三欄為住址街道 <Br> - 第十四欄為住址其他資料，如大廈，層數及單位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父親, 02 - 母親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> (O - 其他, M - 澳門, T - &#27705;仔, C - 路環) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";

$i_StudentRegistry_Import_XML_Format = "XML 匯入格式:<br />
&lt;?xml version=&quot;1.0&quot; encoding=&quot;ISO-8859-1&quot;?&gt;<br />
&lt;!DOCTYPE SRA SYSTEM &quot;http://app.dsej.gov.mo/prog/edu/sra/sra3.dtd&quot;&gt;<br />

&lt;SRA&gt;<br />
&lt;STUDENT&gt;<br />
&lt;STUD_ID&gt;12345&lt;/STUD_ID&gt;<br />
&lt;CODE&gt;9400999-6&lt;/CODE&gt;<br />
&lt;NAME_C&gt;&#37758;&#22283;&#20161;&lt;/NAME_C&gt;<br />
&hellip;<br />
&lt;/STUDENT&gt;<br />
&lt;STUDENT&gt;<br />
&lt;STUD_ID&gt;12346&lt;/STUD_ID&gt;<br />
&lt;CODE&gt;9400998-X&lt;/CODE&gt;<br />
&lt;NAME_C&gt;&#37758;&#22283;&#26862;&lt;/NAME_C&gt;<br />
&hellip;<br />
&lt;/STUDENT&gt;<br />
&hellip;<br />
&lt;/SRA&gt;";

$i_StudentRegistry_Import_Invalid_Record = "檔案含有無效紀錄";
$i_StudentRegistry_Import_No_Record = "檔案未包含任何紀錄";
$i_StudentRegistry_Import_ValidInvalid_Record = "檔案含有有效紀錄及錯誤紀錄。 如繼續匯入，只有有效紀錄會被匯入";
$i_StudentRegistry_Import_Valid_Record = "檔案含有有效紀錄";

$i_StudentRegistry_Export_Invalid_Record = "匯出的資料包含不完整的紀錄。";
$i_StudentRegistry_Export_Valid_Record = "匯出的資料包括 <--NoOfValid--> 個完整紀錄。";

$i_general_clickheredownloaddtd = '按此下載 DTD 檔';
$i_general_invalid = '資料無效';
$i_general_incomplete = '資料不完整';

$i_StudentRegistry_Import_Invalid_Reason = "此問題可能由於：<br>1. <span style='color:red;'>-</span> 沒有資料<br>2. <span style='color:red;'>[已輸入資料]</span> 錯誤資料。";
$i_StudentRegistry_Export_Invalid_Reason = "此問題可能由於：<br><span style='color:red;'>-</span> 沒有資料";


##########################################
# eCommunity
##########################################
$eComm['eCommunity'] = "eCommunity 網上社群";
$eComm['MyGroups'] = "我的社群";
$eComm['OtherGroups'] = "其他社群";
$eComm['MyPage'] = "我的主頁";
$eComm['Settings'] = "設定";
$eComm['LatestShare'] = "最新分享";
$eComm['Photo'] = "相片";
$eComm['Video'] = "視訊";
$eComm['Website'] = "網站";
$eComm['File'] = "檔案";
$eComm['P'] = $eComm['Photo'];
$eComm['V'] = $eComm['Video'];
$eComm['W'] = $eComm['Website'];
$eComm['F'] = $eComm['File'];
$eComm['Add_'] = "新增";
$eComm['New_'] = "新增";
$eComm['Edit_'] = "編輯";

$eComm['View'] = "檢視";
$eComm['Add'] = "新增";

$eComm_Field_Topic = "主題";
$eComm_Field_Createdby = "發表者";
$eComm_Field_ReplyNo = "回覆數目";
$eComm_Field_LastUpdated = "最近更新";
$eComm['Forum'] = "論壇";
$eComm['Title'] = "標題";
$eComm['Content'] = "內容";
$eComm['Group'] = "社群";
$eComm['YourReply'] = "你的回覆";
$eComm['Search'] = "搜尋";
$eComm['Close'] = "關閉";
$eComm['Attachment'] = "附件";

$eComm['Share'] = "資源分享";
$eComm['By'] = "自";
$eComm['Comment'] = "意見";
$eComm['Album'] = "相簿";
$eComm['Folder'] = "資料夾";
$eComm['Category'] = "類別";
$eComm['NewAlbum'] = "新增" . $eComm['Album'];
$eComm['NewFolder'] = "新增" . $eComm['Folder'];
$eComm['NewCateogry'] = "新增" . $eComm['Cateogry'];
$eComm['EditAlbum'] = "編輯" . $eComm['Album'];
$eComm['EditFolder'] = "編輯" . $eComm['Folder'];
$eComm['EditCateogry'] = "編輯" . $eComm['Cateogry'];
$eComm['LastUpload'] = "最新上載";
$eComm['Status'] = "狀態";
$eComm['Public'] = "公開";
$eComm['Private'] = "非公開";
$eComm['Top'] = "最高";
$eComm['jsDeleteRecord'] = "你是否確定要刪除記錄？";
$eComm['jsDeleteFolder'] = "你是否確定要刪除記錄？所有相關檔案及意見將一併刪除。";
$eComm['CreatedBy'] = "上載者";
$eComm['CreatedDate'] = "分享日期";
$eComm['WriteYourcomment'] = "發表意見";
$eComm['Yourcomment'] = "你的意見";
$eComm['NoRecord'] = "尚未有紀錄";


$eComm['Steps'] = "步驟";
$eComm['select_file'] = "選擇/上載檔案";
$eComm['select_photo'] = "選擇/上載相片";
$eComm['input_website'] = "輸入網址 ";
$eComm['select_video'] = "選擇你的影片 ";
$eComm['input_detail_info'] = "輸入詳細資料";
$eComm['add_file_finished'] = "檔案上載完畢";
$eComm['add_photo_finished'] = "相片上載完畢";
$eComm['add_website_finished'] = "已加入網頁";
$eComm['add_video_finished'] = "已加入影片";
$eComm['add_to_folder'] = "加入到資料夾";
$eComm['add_to_album'] = "加入到相簿";
$eComm['add_to_category'] = "加入到類別";
$eComm['move_to_folder'] = "移進資料夾";
$eComm['move_to_album'] = "移進相簿";
$eComm['move_to_category'] = "移至類別";
$eComm['Upload'] = "上載";
$eComm['URL'] = "網址";
$eComm['js_please_select_file'] = "你必須選擇最少一個檔案。";
$eComm['js_please_select_photo'] = "你必須選擇最少一張相片。";
$eComm['js_please_input_url'] = "你必須選擇最少一個網址。";
$eComm['info_str']['P'] = "相片上載完畢。你可以按一下圖示，填寫詳細資料。";
$eComm['info_str']['F'] = "檔案上載完畢。你可以按一下圖示，填寫詳細資料。";
$eComm['info_str']['W'] = "已加入網頁。你可以按一下圖示，填寫詳細資料。";
$eComm['info_str']['V'] = "影片上載完畢。你可以按一下圖示，填寫詳細資料。";
$eComm['Description'] = "描述";
$eComm['remove_this_file'] = "移除此檔案";
$eComm['remove_this_video'] = "移除此影片";
$eComm['set_private'] = "設定為非公開";
$eComm['set_all_to_folder'] = "將所有檔案加入到資料夾";
$eComm['set_all_to_album'] = "將所有相片加入到相簿";
$eComm['set_all_to_category'] = "將所有網頁加入到類別";
$eComm['set_all_to_video_album'] = "將所有影片加入到資料夾";
$eComm['select_all'] = "全選";

$eComm['Member'] = "組員";
$eComm['NewTopic'] = "新主題";
$eComm['NewMember'] = "新增組員";
$eComm['Storage'] = "儲存量";
$eComm['NoFolder']['P'] = "尚未有相簿，請聯絡你的系統管理員建立相簿，然後再行上載。";
$eComm['NoFolder']['F'] = "尚未有資料夾，請聯絡你的系統管理員建立資料夾，然後再行上載。";
$eComm['NoFolder']['W'] = "尚未有類別，請聯絡你的系統管理員建立資料夾，然後再行上載。";
$eComm['NoFolder']['V'] = "尚未有影片資料夾，請聯絡你的系統管理員建立資料夾，然後再行上載。";
$eComm['Performance'] = "表現";
$eComm['MemberList'] = "組員名單";
$eComm['Warning'] = "警告";
$eComm['no_space'] = "沒有足夠儲存空間";
$eComm['no_file'] = "沒有上載任何檔案";
$eComm['no_website'] = "沒有加入任何網頁";
$eComm['chatroom'] = "聊天室";
$eComm['GroupSettings'] = "小組設定";
$eComm['MemberSettings'] = "成員設定";
$eComm['AnnouncementSettings'] = "宣佈設定";
$eComm['SharingSettings'] = "分享設定";
$eComm['ForumSettings'] = "討論區設定";
$eComm['GroupLogo'] = "群組圖示";
$eComm['GroupLogo_PhotoGuide'] = "群組圖示只接受'.JPG'、'.GIF'或'.PNG'等格式，而且尺寸必須為225 x 170像素(闊x高)。";
$eComm['GroupLogoDelete'] = "刪除現有群組圖示";
$eComm['PublicStatus'] = "Public Status";
$eComm['Management'] = "管理";
$eComm['IndexAnnounce'] = "Announcement# at index page";
$eComm['FolderType'] = "資料夾種類";
$eComm['FileType'] = "檔案類型";
$eComm['FileName'] = "檔案名稱";
$eComm['NoFolderSelected'] = "沒有選擇資料夾";
$eComm['WaitingRejectList'] = "等待批核 / 不獲批核列表";
$eComm['WaitingApproval'] = "等待批核";
$eComm['Rejected'] = "不獲批核";
$eComm['ApprovalStatus'] = "批核狀況";
$eComm['DisplayOnTop'] = "置頂";
$eComm['GrantAdmin'] = "授予管理權限";
$eComm['LatestNumber'] = "最新紀錄編號";
$eComm['NeedApproval'] = "需要批核";
$eComm['SetAsCoverImage'] = "設為封面圖像";
$eComm['DefaultViewModeIndexPage'] = "首頁預設檢視模式";
$eComm['ViewMode_List'] = "列表";
$eComm['ViewMode_4t'] = "4 圖示";
$eComm['ViewMode_2t'] = "2 圖示";
$eComm['ViewMode_1t'] = "1 圖示";
$eComm['AllowedImageTypes'] = "接受相片檔案格式";
$eComm['AllowedFileTypes'] = "接受檔案格式";
$eComm['AllowedVideoTypes'] = "接受影片檔案格式";
$eComm['AllowedTypesRemarks'] = "\"ALL\" 代表接受任何格式，若你希望接受多種格式，請用 / 將檔名分隔 e.g. jpg/bmp/tif";
$eComm['file_type_not_allowed'] = "檔案上載失敗，不接受此檔案格式";
$eComm['CalendarSettings'] = "月曆";
$eComm['NoCalendar'] = "暫時未有月曆，現在加入?";
$eComm['CreateCalendar'] = "設定月曆";
$eComm['DisplayAtGroupIndex'] = "在群組首頁顯示?";
$eComm['AddTo'] = "<< 增加";
$eComm['DeleteTo'] = "刪除 >>";
$eComm['Message'] = "訊息";
$eComm['TopicSettings'] = "相關主題設定";

# HKU Medicine research
$i_MedicalReasonTitle = "健康分類";
$i_MedicalReasonName[] = array(0,"未能定義");
$i_MedicalReasonName[] = array(1,"疑似流感/呼吸道疾病");
$i_MedicalReasonName[] = array(2,"腸胃科疾病");
$i_MedicalReasonName[] = array(3,"手足口病");
$i_MedicalReasonName[] = array(99,"其他疾病");
$i_MedicalReasonName[] = array(999,"非因疾缺席");
$i_MedicalReason_Export = "匯出健康報表";
$i_MedicalDataTransferSuccess = "資料已傳送。";
$i_MedicalDataTransferFailed = "資料傳送失敗。";
$i_MedicalReason_Export2="匯出健康報表";
$i_MedicalExportError ="無法匯出報告，因為WebSAMS級別名稱尚未設定妥當。";

# eLibrary
$i_eLibrary_System = "電子圖書";
$i_eLibrary_System_Admin_User_Setting = "設定管理用戶";
$i_eLibrary_System_Current_Batch = "現有批次";
$i_eLibrary_System_Update_Batch = "批次更新";
$i_eLibrary_System_Updated_Batch = "已更新批次";
$i_eLibrary_System_Reset_Fail = "更新批次失敗";
$i_eLibrary_System_Updated_Records = "已更新記錄";
$eLib["AdminUser"] = "管理用戶";

$eLib['ManageBook']["Title"] = "管理書籍";
$eLib['ManageBook']["ConvertBook"] = "轉換圖書";
$eLib['ManageBook']["ContentManage"] = "管理內容";
$eLib['ManageBook']["ContentView"] = "檢視內容";
$eLib['ManageBook']["ImportBook"] = "匯入圖書";
$eLib['ManageBook']["ImportCSV"] = "匯入CSV檔案";
$eLib['ManageBook']["ImportXML"] = "匯入XML檔案";
$eLib['ManageBook']["ImportZIP"] = "匯入ZIP檔案";
$eLib['ManageBook']["ImportZIPDescribe"] = "
				ZIP檔案需備下檔案<br />
				content.text - text檔,  將轉換成XML檔案 <br />
				 \"image\" 文件夾 - 包含相關圖片案. <br />
				control.text - 控制轉換過程. <br />
													";

$eLib['Book']["Author"] = "作者";
$eLib['Book']["SeriesEditor"] = "系列編輯";
$eLib['Book']["Category"] = "類型";
$eLib['Book']["Subcategory"] = "子類型";
$eLib['Book']["DateModified"] = "最近修改日期";
$eLib['Book']["Description"] = "簡介";
$eLib['Book']["Language"] = "語言";
$eLib['Book']["Level"] = "等級";
$eLib['Book']["AdultContent"] = "敏感內容";
$eLib['Book']["Title"] = "書名";
$eLib['Book']["Publisher"] = "出版社";
$eLib['Book']["TableOfContent"] = "目錄";
$eLib['Book']["Copyright"] = "版權";

$eLib["SourceFrom"] = "資料來源";
$eLib['Source']["green"] = "Green Apple";
$eLib['Source']["cup"] = "Cambridge University Press";

$eLib['EditBook']["IsChapterStart"] = "章節開始?";
$eLib['EditBook']["ChapterID"] = "章節索引";

# HTML editor
$ec_html_editor['bold'] = "粗體";
$ec_html_editor['italic'] = "斜體";
$ec_html_editor['underline'] = "加底線";
$ec_html_editor['strikethr'] = "刪除線";
$ec_html_editor['subscript'] = "下標";
$ec_html_editor['superscript'] = "上標";
$ec_html_editor['justifyleft'] = "靠左對齊";
$ec_html_editor['justifyright'] = "靠右對齊";
$ec_html_editor['justifycent'] = "置中";
$ec_html_editor['justifyfull'] = "左右對齊";
$ec_html_editor['orderlist'] = "編號";
$ec_html_editor['bulletlist'] = "項目符號";
$ec_html_editor['decindent'] = "減少縮排";
$ec_html_editor['incindent'] = "增加縮排";
$ec_html_editor['fontcolor'] = "字型色彩";
$ec_html_editor['bgcolor'] = "醒目提示";
$ec_html_editor['horrule'] = "插入分隔線";
$ec_html_editor['weblink'] = "插入或編輯超連結";
$ec_html_editor['insertimage'] = "插入或編輯圖片";
$ec_html_editor['table'] = "表格功能";
$ec_html_editor['htmlsource'] = "瀏覽綱頁原始碼";
$ec_html_editor['topicsent'] = "中心句子";
$ec_html_editor['wordcount'] = "字數總計";
$ec_html_editor['highlight'] = "強調";
$ec_html_editor['missing'] = "錯漏";
$ec_html_editor['comment'] = "提議";
$ec_html_editor['correct'] = "正確";
$ec_html_editor['pcorrect'] = "部分正確";
$ec_html_editor['incorrect'] = "不正確";
$ec_html_editor['appreciate'] = "讚賞";
$ec_html_editor['markcode'] = $ec_wordtemplates['marking_code'];
$ec_html_editor['errsummy'] = "錯誤概括";
$ec_html_editor['markscheme'] = $ec_wordtemplates['marking_scheme'];
$ec_html_editor['sticker'] = "貼紙";
$ec_html_editor['undo'] = "復原";
$ec_html_editor['redo'] = "取消復原";
$ec_html_editor['hvmode'] = "直書/橫書";
$ec_html_editor['mgcomment'] = "註解";
$ec_html_editor['linespace'] = "設定行距";
$ec_html_editor['copy'] = "複製";
$ec_html_editor['cut'] = "剪下";
$ec_html_editor['paste'] = "貼上";
$ec_html_editor['font'] = "字型";
$ec_html_editor['font_size'] = "字型大小";
$ec_html_editor['clear_text_style'] = "消除字型格式";
$ec_html_editor['createanchor'] = "建立或編輯錨點";
$ec_html_editor['anchorname'] = "錨點名稱";
$ec_html_editor['anchor'] = "錨點";
$ec_html_editor['selectcolor'] = "選擇顏色";
$ec_html_editor['targetwindow'] = "目標視窗";
$ec_html_editor['power_voice'] = "Power Voice";

$ec_html_editor['headingNor'] = "預設標題";
$ec_html_editor['heading1'] = "標題一";
$ec_html_editor['heading2'] = "標題二";
$ec_html_editor['heading3'] = "標題三";
$ec_html_editor['heading4'] = "標題四";
$ec_html_editor['heading5'] = "標題五";
$ec_html_editor['heading6'] = "標題六";
$ec_html_editor['headingAdd'] = "位址";

$ec_html_editor['inserttable'] = "插入表格...";
$ec_html_editor['edittable'] = "編輯表格格式...";
$ec_html_editor['editcell'] = "編輯儲存格格式...";
$ec_html_editor['insertcoll'] = "插入左方欄";
$ec_html_editor['insertcolr'] = "插入右方欄";
$ec_html_editor['insertrowa'] = "插入上方列";
$ec_html_editor['insertrowb'] = "插入下方列";
$ec_html_editor['increasecols'] = "增加欄合併";
$ec_html_editor['decreasecols'] = "減少欄合併";
$ec_html_editor['increaserows'] = "增加列合併";
$ec_html_editor['decreaserows'] = "減少列合併";
$ec_html_editor['deleterow'] = "刪除列";
$ec_html_editor['deletecol'] = "刪除欄";

$ec_html_editor['image_url'] = "圖片 URL";
$ec_html_editor['alternate_text'] = "提示文字";
$ec_html_editor['layout'] = "外觀";
$ec_html_editor['alignment'] = "對齊方式";
$ec_html_editor['border_thickness'] = "框線寬度";
$ec_html_editor['spacing'] = "間隔";
$ec_html_editor['horizontal'] = "水平間距";
$ec_html_editor['vertical'] = "垂直間距";

$ec_html_editor['al_none'] = "無";
$ec_html_editor['al_left'] = "向左對齊";
$ec_html_editor['al_center'] = "置中";
$ec_html_editor['al_right'] = "向右對齊";
$ec_html_editor['al_texttop'] = "向文字頂部對齊";
$ec_html_editor['al_absmiddle'] = "絕對置中";
$ec_html_editor['al_baseline'] = "向文字底部對齊";
$ec_html_editor['al_absbottom'] = "絕對置下";
$ec_html_editor['al_bottom'] = "向下對齊";
$ec_html_editor['al_middle'] = "向中對齊";
$ec_html_editor['al_top'] = "向上對齊";

$ec_html_editor['tb_size'] = "大小";
$ec_html_editor['tb_row'] = "列數";
$ec_html_editor['tb_col'] = "欄數";
$ec_html_editor['tb_height'] = "高度";
$ec_html_editor['tb_width'] = "寬度";
$ec_html_editor['tb_pixel'] = "像素";
$ec_html_editor['tb_percent'] = "百分比";
$ec_html_editor['tb_cellspacing'] = "儲存格間距";
$ec_html_editor['tb_cellpadding'] = "儲存格填塞";
$ec_html_editor['tb_color'] = "色彩";
$ec_html_editor['tb_bgcolor'] = "網底色彩";
$ec_html_editor['tb_bordercolor'] = "框線色彩";
$ec_html_editor['tb_wrap'] = "換行";
$ec_html_editor['tb_nowrap'] = "換行";
$ec_html_editor['tb_nowrap_yes'] = "不換行";
$ec_html_editor['tb_nowrap_no'] = "自動換行";
$ec_html_editor['tb_hor_alignment'] = "水平對齊";
$ec_html_editor['tb_ver_alignment'] = "垂直對齊";

$ec_html_editor['warn_image_url'] = "必須填上圖片URL";
$ec_html_editor['warn_hor_space'] = "水平間距必須是 0 至 999";
$ec_html_editor['warn_border_thick'] = "框線寬度必須是 0 至 999";
$ec_html_editor['warn_ver_space'] = "垂直間距必須是 0 至 999";
$ec_html_editor['warn_specify_a'] = "必須輸入";
$ec_html_editor['warn_specify_b'] = "的數值";
$ec_html_editor['warn_specify_num_a'] = "";
$ec_html_editor['warn_specify_num_b'] = "的數值必須是 0 至 999";
$ec_html_editor['warn_overwrite'] = "你是否確定要取替已選的內容？";

$ec_html_editor['total_word'] = "字數";
$ec_html_editor['total_symbol'] = "標點符號";

$ec_html_editor['save_content'] = "儲存內容";
$button_ok = "確定";

$file_kb = "KB";
$file_mb = "MB";

###############################################################################
# iPortfolio 2.5
###############################################################################
$ec_iPortfolio['above_mean'] = "高於全級總平均分";
$ec_iPortfolio['academic_performance'] = "校內高中學科成績";
$ec_iPortfolio['account_quota_free'] = "剩餘可使用的戶口額";
$ec_iPortfolio['achievement'] = "獎項 / 証書文憑 / 成就 (如有)";
$ec_iPortfolio['action_type'] = "動作";
$ec_iPortfolio['activate'] = "啟用";
$ec_iPortfolio['activation_quota_afterward'] = "啟用後將剩餘戶口額";
$ec_iPortfolio['activation_result_activated'] = "此戶口在之前已被啟用";
$ec_iPortfolio['activation_result_no_regno'] = "欠缺學生編號";
$ec_iPortfolio['activation_student_reactivate'] = "再啟用的學生數目";
$ec_iPortfolio['activation_student_activate'] = "啟用的學生數目";
$ec_iPortfolio['activation_total_fail'] = "啟用失敗的學生戶口數目";
$ec_iPortfolio['activation_total_success'] = "啟用成功的學生戶口數目";
$ec_iPortfolio['activity'] = "活動紀錄";
$ec_iPortfolio['activity_name'] = "活動名稱";
$ec_iPortfolio['address'] = "地址";
$ec_iPortfolio['admission_date'] = "入學日期";
$ec_iPortfolio['adv_analyze'] = "搜尋式報告";
$ec_iPortfolio['all_class'] = "全部班級";
$ec_iPortfolio['all_classlevel'] = "全部年級";
$ec_iPortfolio['all_ele'] = "全部其他學習經歷種類";
$ec_iPortfolio['all_school_year'] = "全部";
$ec_iPortfolio['all_statistic'] = "全級統計";
$ec_iPortfolio['amount'] = "數量";
$ec_iPortfolio['analysis_report'] = "分析報告";
$ec_iPortfolio['assessment_report'] = "學業報告";
$ec_iPortfolio['attendance'] = "考勤紀錄";
$ec_iPortfolio['attendance_detail'] = "考勤紀錄";
$ec_iPortfolio['available_portfolio_acc'] = "剩餘可使用的戶口額";

$ec_iPortfolio['award'] = "獎項";
$ec_iPortfolio['award'] = "得獎紀錄";

$ec_iPortfolio['award_achievement'] = "獎項及成就";
$ec_iPortfolio['award_date'] = "得獎日期";
$ec_iPortfolio['award_name'] = "獎項";
$ec_iPortfolio['based_on'] = "按";
$ec_iPortfolio['basic_info'] = "基本資料";
$ec_iPortfolio['below_mean'] = "低於全級總平均分";
$ec_iPortfolio['blue'] = "藍";
$ec_iPortfolio['bold'] = "粗體";
$ec_iPortfolio['by_semester'] = '學期';
$ec_iPortfolio['by_year'] = '年度';
$ec_iPortfolio['category'] = "分類";
$ec_iPortfolio['choose_group'] = "請選擇以下小組";

$ec_iPortfolio['class'] = "班別";
$ec_iPortfolio['class'] = "班級";

$ec_iPortfolio['class_and_number'] = "班別 - 學號";
$ec_iPortfolio['class_list'] = "班別名單";
$ec_iPortfolio['class_number'] = "班別 -<br>學號";
$ec_iPortfolio['class_perform_stat'] = "學期/年度表現統計";
$ec_iPortfolio['class_photo_number'] = "相片數目/<span class='guide'>*</span>學生總數";
$ec_iPortfolio['class_review'] = "班別內互評";
$ec_iPortfolio['color'] = "顏色";
$ec_iPortfolio['comments'] = "評語/意見";
$ec_iPortfolio['comment_chi'] = "中文評語";
$ec_iPortfolio['comment_eng'] = "英文評語";
$ec_iPortfolio['composite_performance_report'] = "綜合表現報告";
$ec_iPortfolio['conduct_grade'] = "操行";
$ec_iPortfolio['copied_portfolio_reomved'] = "早前預備了的 XXX 班同學的學習檔案已被刪除。";
$ec_iPortfolio['copied_portfolio_reomved_fail'] = "早前預備了的 XXX 班同學的學習檔案未能被刪除。";
$ec_iPortfolio['copied_template_title'] = "複製模板名稱";
$ec_iPortfolio['copy_web_portfolio_files'] = "預備學習檔案";
$ec_iPortfolio['csv_report_config'] = "學生學習紀錄 CSV 檔案上載";
$ec_iPortfolio['date'] = "日期";
$ec_iPortfolio['date_issue'] = "發出日期";
$ec_iPortfolio['deactive'] = "暫停啟用";
$ec_iPortfolio['delete'] = "刪除";
$ec_iPortfolio['detail'] = "詳細資料";
$ec_iPortfolio['details'] = "詳情";
$ec_iPortfolio['disable_review'] = "否";
$ec_iPortfolio['display'] = "顯示";
$ec_iPortfolio['display_by_grade'] = "以等級顯示";
$ec_iPortfolio['display_by_rank'] = "以級名次顯示";
$ec_iPortfolio['display_by_score'] = "以分數顯示";
$ec_iPortfolio['display_by_stand_score'] = "以標準分數顯示";
$ec_iPortfolio['display_class_distribution'] = "顯示班別統計";
$ec_iPortfolio['display_class_stat'] = "顯示班別統計";
$ec_iPortfolio['draft'] = "草稿";
$ec_iPortfolio['edit'] = "編輯";
$ec_iPortfolio['edit_content'] = "編輯內容";
$ec_iPortfolio['edit_portfolios'] = "編輯";
$ec_iPortfolio['edit_template_alert'] = "注意：此頁面正在使用模板 \'XXX\'，對此頁面所作的任何改動，將反映到其他採用相同模板的頁面。如只想修改此頁面, 請建立一個新模板。";
$ec_iPortfolio['edit_this_page'] = "編輯此頁";
$ec_iPortfolio['ele'] = "其他學習經歷種類";
$ec_iPortfolio['em_phone'] = "緊急聯絡電話";
$ec_iPortfolio['enable_review'] = "是";
$ec_iPortfolio['end_report'] = "完";
$ec_iPortfolio['enter_student_name'] = "輸入學生姓名";
$ec_iPortfolio['enter_student_name_or_userlogin'] = "輸入學生姓名或者內聯網帳號";
$ec_iPortfolio['failed_uploaded_photo_count'] = "未能上載的相片數目";
$ec_iPortfolio['finished_cd_burning_preparation'] = "XXX 班同學的學習檔案已預備至以下文件夾：";

$ec_iPortfolio['form'] = "班別";
$ec_iPortfolio['form'] = "年級";

$ec_iPortfolio['friends_review'] = "朋友互評";
$ec_iPortfolio['full_mark'] = "滿分";
$ec_iPortfolio['full_report'] = "整體報告";
$ec_iPortfolio['full_report_config'] = "整體報告設定";
$ec_iPortfolio['generate_score_list'] = "製作分數表";
$ec_iPortfolio['get_prepared_portfolio'] = "學生的學習檔案放置在以學生編號命名的文件夾內，老師可根據同學的學生編號去燒錄 CD-ROM。";
$ec_iPortfolio['grade'] = "等級";
$ec_iPortfolio['green'] = "綠";
$ec_iPortfolio['green_frame'] = " (以 <span style=\"color:#3d7001; font-weight:bold\">綠色相框</span> 標示)";
$ec_iPortfolio['group_by'] = "請選擇";
$ec_iPortfolio['growth_description'] = "簡介";
$ec_iPortfolio['growth_group'] = "參予小組";
$ec_iPortfolio['growth_phase_period'] = "時段";
$ec_iPortfolio['growth_status'] = "狀況";
$ec_iPortfolio['growth_title'] = "名稱";
$ec_iPortfolio['guardian_info'] = "監護人資料";
$ec_iPortfolio['heading']['learning_portfolio'] = "學習檔案";
$ec_iPortfolio['heading']['lp_updated'] = "學習檔案更新數目";
$ec_iPortfolio['heading']['no'] = "編號";
$ec_iPortfolio['heading']['no_lp_active_stu'] = "已啟用學習檔案的學生數目";
$ec_iPortfolio['heading']['no_stu'] = "學生數目";
$ec_iPortfolio['heading']['photo'] = "照片";
$ec_iPortfolio['heading']['sb_scheme_updated'] = "校本計劃更新數目";
$ec_iPortfolio['heading']['school_record_updated'] = "學校紀錄更新數目";
$ec_iPortfolio['heading']['student_info'] = "個人資料";
$ec_iPortfolio['heading']['student_name'] = "學生名稱";
$ec_iPortfolio['heading']['student_record'] = "學校紀錄";
$ec_iPortfolio['heading']['student_regno'] = "學生編號";
$ec_iPortfolio['highest_score'] = "最高分";
$ec_iPortfolio['highlight_result'] = "強調方式";
$ec_iPortfolio['house'] = "社級";
$ec_iPortfolio['hours'] = "時數";
$ec_iPortfolio['hours_above'] = "小時以上";
$ec_iPortfolio['hours_below'] = "小時以下";
$ec_iPortfolio['hours_range'] = "時數範圍";
$ec_iPortfolio['hours_to'] = "至";
$ec_iPortfolio['import_activation'] = "啟用 iPortfolio 戶口";
$ec_iPortfolio['import_regno'] = "匯入學生編號";
$ec_iPortfolio['iportfolio_folder_size'] = "檔案儲存容量";
$ec_iPortfolio['iPortfolio_published'][1] = "<font color=green>你 的 iPortfolio 已 成 功 <u>出 版</u> 了 ！</font>";
$ec_iPortfolio['iPortfolio_published'][0] = "<font color=green>你 的 iPortfolio 已 成 功 <u>收 起</u> 了 ！</font>";
$ec_iPortfolio['italic'] = "斜體";
$ec_iPortfolio['last_update'] = "更新日期";
$ec_iPortfolio['last_update_time'] = "最後更新時間";
$ec_iPortfolio['learning_portfolio_content'] = "學習檔案內容管理";
$ec_iPortfolio['level_review'] = "級別內互評";
$ec_iPortfolio['list'] = "清單";
$ec_iPortfolio['list_award'] = "校內頒發的主要獎項及成就";
$ec_iPortfolio['lowest_score'] = "最低分";
$ec_iPortfolio['main_guardian'] = "主監護人";
$ec_iPortfolio['mandatory_field_description']= "附有<span class='tabletextrequire'>*</span>的項目必須填寫。";
$ec_iPortfolio['mark_performance'] = "校內表現";
$ec_iPortfolio['master_score_list'] = "班別科目表現總覽";
$ec_iPortfolio['mean'] = "平均分";
$ec_iPortfolio['merit'] = "獎懲紀錄";
$ec_iPortfolio['merit_detail'] = "獎懲紀錄";
$ec_iPortfolio['mtype'] = "種類";
$ec_iPortfolio['new_comments'] = " 個新評語/意見";
$ec_iPortfolio['new_portfolios'] = "新增";
$ec_iPortfolio['no'] = "否";
$ec_iPortfolio['notes_guide'] = "指引";
$ec_iPortfolio['notes_method'] = "輸入方式";
$ec_iPortfolio['notes_method_eClass'] = "eClass 匯入";
$ec_iPortfolio['notes_method_form'] = "表格式";
$ec_iPortfolio['notes_method_html'] = "網頁式";
$ec_iPortfolio['notes_method_weblog'] = "網上日誌";
$ec_iPortfolio['notes_relocate_DL'] = "移出一層至&quot;XXX&quot;之後";
$ec_iPortfolio['notes_relocate_Down'] = "下移至&quot;XXX&quot;之後";
$ec_iPortfolio['notes_relocate_DR'] = "移入一層至&quot;XXX&quot;之內";
$ec_iPortfolio['notes_relocate_UL'] = "移出一層至&quot;XXX&quot;之前";
$ec_iPortfolio['notes_relocate_Up'] = "上移至&quot;XXX&quot;之前";
$ec_iPortfolio['notes_relocate_UR'] = "移入一層至&quot;XXX&quot;之內";
$ec_iPortfolio['notes_template'] = "預設模板";
$ec_iPortfolio['no_student'] = "沒有學生";
$ec_iPortfolio['number'] = "學號";
$ec_iPortfolio['number_using_template'] = "使用此範本的學習檔案數目";
$ec_iPortfolio['ole'] = "其他學習經歷";
$ec_iPortfolio['ole_report_stat'] = "其他學習經歷報告";
$ec_iPortfolio['ole_role'] = "參與角色";
$ec_iPortfolio['optional'] = "(可選擇填寫)";
$ec_iPortfolio['overall'] = "總成績";
$ec_iPortfolio['overall_comment'] = "總評語";
$ec_iPortfolio['overall_grade'] = "總等級";
$ec_iPortfolio['overall_perform_stat'] = "科目表現統計";
$ec_iPortfolio['overall_rank'] = "總級名次";
$ec_iPortfolio['overall_result'] = "總成績";
$ec_iPortfolio['overall_score'] = "總平均分";
$ec_iPortfolio['overall_stand_score'] = "總標準分數";
$ec_iPortfolio['overall_summary'] = "總結";
$ec_iPortfolio['peer_review_setting'] = "同輩互評設定";
$ec_iPortfolio['peer_review_type'] = "同輩互評類型";
$ec_iPortfolio['performance'] = "表現";
$ec_iPortfolio['period'] = "時段";
$ec_iPortfolio['phone'] = "電話號碼";
$ec_iPortfolio['photo_num_guide'] = "* 指已啟動iPortfolio戶口的學生。";
$ec_iPortfolio['photo_removed_msg'] = "相片已成功被刪除";
$ec_iPortfolio['pic'] = "負責人";
$ec_iPortfolio['place_of_birth'] = "出生地點";
$ec_iPortfolio['portfolios'] = "學習檔案內容管理";
$ec_iPortfolio['portfolios_mgt'] = "學習檔案";
$ec_iPortfolio['portfolio_status'] = "共有 <!--TotalStudentNo--> 個學生，已啟用 <!--PortfolioNo--> 個學習檔案<!--GreenFrame-->";
$ec_iPortfolio['prepare_other_class_cd_burning'] = "預備其他班的 CD-ROM 燒錄";
$ec_iPortfolio['programme'] = "活動名稱";
$ec_iPortfolio['proj_ext_act_prog'] = "研習作品 / 活動項目";
$ec_iPortfolio['publish_date'] = "發佈日期";
$ec_iPortfolio["rank"] = "級名次";
$ec_iPortfolio['reason'] = "原因";
$ec_iPortfolio['record'] = "紀錄";
$ec_iPortfolio['red'] = "紅";
$ec_iPortfolio['remark'] = "備註";
$ec_iPortfolio['remove_copied_files'] = "刪除早前預備了的學習檔案";
$ec_iPortfolio['report_title_class'] = "Class (班別)";
$ec_iPortfolio['report_title_classno'] = "Class No. (學號)";
$ec_iPortfolio['report_title_name'] = "Name (姓名)";
$ec_iPortfolio['report_title_regno'] = "Reg. No. (編號)";
$ec_iPortfolio['report_title_year'] = "Year (學年)";
$ec_iPortfolio['report_type'] = "報告類型";
$ec_iPortfolio['review_author'] = "發表：";
$ec_iPortfolio['review_type'] = "互評";
$ec_iPortfolio['role'] = "職位";
$ec_iPortfolio['role_participate'] = "角色";
$ec_iPortfolio['SAMS_CSV_file'] = "WEBSAMS CSV 檔";
$ec_iPortfolio['SAMS_last_update'] = "最後更新日期";
$ec_iPortfolio['SAMS_subject'] = "科目";
$ec_iPortfolio['school_code'] = "學校編號";
$ec_iPortfolio['school_phone'] = "學校電話";
$ec_iPortfolio['school_remark'] = "學校備註";
$ec_iPortfolio['school_review'] = "全校互評";
$ec_iPortfolio['school_year'] = "學年";
$ec_iPortfolio['school_yr'] = "全部年份";
$ec_iPortfolio["score"] = "分數";
$ec_iPortfolio['select_group'] = "已選擇了以下小組";
$ec_iPortfolio['self_account'] = "個人自述";
$ec_iPortfolio['semester'] = "學期";
$ec_iPortfolio['service_name'] = "服務名稱";
$ec_iPortfolio['show_by'] = "顯示";
$ec_iPortfolio['size_unit'] = '(MB)';
$ec_iPortfolio['slp_config'] = "學生學習概覽設定";
$ec_iPortfolio['standard_deviation'] = "標準差";
$ec_iPortfolio["stand_score"] = "標準分數";
$ec_iPortfolio['statistic'] = "個人時數統計";
$ec_iPortfolio['student_account'] = "學生戶口";
$ec_iPortfolio['student_learning_profile'] = "學生學習概覽";
$ec_iPortfolio['student_particulars'] = "學生資料";
$ec_iPortfolio['student_perform_stat'] = "個人表現統計";
$ec_iPortfolio['student_photo'] = "學生相片";
$ec_iPortfolio['student_photo_no'] = "沒有相片";
$ec_iPortfolio['student_report_print'] = "學生報告列印";
$ec_iPortfolio['student_self_account'] = "學生自述";
$ec_iPortfolio['student_self_account_desc'] = "學生可於此欄分享其高中學習生活及個人發展之得著或提供額外資料，好讓其他人仕(例如各大專院校及未來僱主等) 作參考之用。為提供
更全面的內容，學生可考慮提及於高中前所獲得的重要成就。";
$ec_iPortfolio['stype'] = "類別";
$ec_iPortfolio['subject_chart'] = "學科統計圖表";
$ec_iPortfolio['success_uploaded_photo_count'] = "成功上載的相片數目";
$ec_iPortfolio['suspend_result_inactive'] = "此戶口尚未啟用";
$ec_iPortfolio['suspend_result_suspended'] = "此戶口在之前已被停用";
$ec_iPortfolio['suspend_total_fail'] = "失敗暫停戶口的學生數目";
$ec_iPortfolio['suspend_total_success'] = "成功暫停戶口的學生數目";
$ec_iPortfolio['teacher_comment'] = "老師評語";
$ec_iPortfolio['templates'] = "模板管理";
$ec_iPortfolio['templates_mgt'] = "範本";
$ec_iPortfolio['template_title'] = "模板名稱";
$ec_iPortfolio['term'] = "學期";
$ec_iPortfolio['thumbnail'] = "縮圖";
$ec_iPortfolio['title'] = "項目";
$ec_iPortfolio['title_academic_report'] = "學業報告";
$ec_iPortfolio['title_activity'] = "活動紀錄";
$ec_iPortfolio['title_attendance'] = "考勤紀錄";
$ec_iPortfolio['title_award'] = "得獎紀錄";
$ec_iPortfolio['title_award_punishment'] = "Awards & Punishment 獎懲資料";
$ec_iPortfolio['title_eca'] = "Extra-Curricular Activities 課外活動";
$ec_iPortfolio['title_form_teacher_chi'] = "班主任";
$ec_iPortfolio['title_form_teacher_eng'] = "FORM TEACHER";
$ec_iPortfolio['title_guardian_chi'] = "家長/監護人";
$ec_iPortfolio['title_guardian_eng'] = "PARENT/GUARDIAN";
$ec_iPortfolio['title_ole'] = "Other Learning Experiences 其他學習經歷";
$ec_iPortfolio['title_principal_chi'] = "校長";
$ec_iPortfolio['title_principal_eng'] = "PRINCIPAL";
$ec_iPortfolio['title_merit'] = "獎懲紀錄";
$ec_iPortfolio['title_service'] = "Services in School 校內服務";
$ec_iPortfolio['title_teacher_comments'] = "老師評語";
$ec_iPortfolio['total'] = "總數";
$ec_iPortfolio['total_absent_count'] = "總缺席次數";
$ec_iPortfolio['total_earlyleave_count'] = "總早退次數";
$ec_iPortfolio['total_hours'] = "總時數";
$ec_iPortfolio['total_late_count'] = "總遲到次數";
$ec_iPortfolio['total_record'] = "共有 <!--NoRecord--> 項記錄";
$ec_iPortfolio['total_records'] = "記錄數目";
$ec_iPortfolio['transcript_config'] = "學業報告設定";
$ec_iPortfolio['type'] = "建立";
$ec_iPortfolio['type_category'] = "文件夾";
$ec_iPortfolio['type_document'] = "文件";
$ec_iPortfolio['underline'] = "加底線";
$ec_iPortfolio['unknown_photo'] = "不明相片";
$ec_iPortfolio['update_msg'] = "已更新紀錄";
$ec_iPortfolio['upload_cert'] = "獎項/証明";
$ec_iPortfolio['upload_photo'] = "上載相片";
$ec_iPortfolio['view_analysis'] = "檢視";
$ec_iPortfolio['view_comment'] = "檢視評語/意見";
$ec_iPortfolio['view_my_record'] = "檢視我的紀錄";
$ec_iPortfolio['view_stat'] = "檢視統計";
$ec_iPortfolio['web_view'] = "學習檔案";
$ec_iPortfolio['whole_school'] = "全級";
$ec_iPortfolio['whole_year'] = "全年";
$ec_iPortfolio['within_word_limit'] = "(以 <!--EngLimit--> 字內之英文 或 <!--ChiLimit--> 字內之中文撰寫本部分)";
$ec_iPortfolio['with_comment'] = "包括留言";
$ec_iPortfolio['year'] = "年度";
$ec_iPortfolio['year_period'] = "時期";
$ec_iPortfolio['yes'] = "是";
$ec_iPortfolio['your_comment'] = "你的意見";

$ec_iPortfolio_guide['group_growth'] = "如沒有選擇小組，則會發放予全部學生。";
$ec_iPortfolio_guide['manage_student_portfolio_msg'] = "你將會以學生身份管理學生的學習檔案！";

$ec_iPortfolio_Report['form'] = "年級";
$ec_iPortfolio_Report['subject'] = "科目";
$ec_iPortfolio_Report['term'] = "學期";
$ec_iPortfolio_Report['year'] = "年度";

$ec_guide['fail_reason'] = "失敗的原因";
$ec_guide['import_back'] = "再匯入";
$ec_guide['import_error_activated_regno'] = "此學生戶口已啟用，不能更改 WebSAMS RegNo";
$ec_guide['import_error_detail'] = "檔案資料";
$ec_guide['import_error_duplicate_classnum'] = "此學生的班別及學號出現重複";
$ec_guide['import_error_duplicate_regno'] = "WebSAMS RegNo 已存在";
$ec_guide['import_error_incorrect_regno'] = "匯入的RegNo 編號應加上#";
$ec_guide['import_error_incorrect_regno_2'] = "匯入的RegNo 編號應為數字";
$ec_guide['import_error_no_user'] = "沒有此用戶";
$ec_guide['import_error_reason'] = "錯誤原因";
$ec_guide['import_error_row'] = "行數";
$ec_guide['import_error_unknown'] = "不明";
$ec_guide['import_error_wrong_format'] = "錯誤的匯入格式";
$ec_guide['import_update_no'] = "成功更新資料項數";
$ec_guide['learning_portfolio_time_start'] = "如果設定了開始時間，這個學習檔案將會於開始時間後出現給學生使用。";
$ec_guide['learning_portfolio_time_end'] = "如果設定了終結時間，學生將不能於終結時間後更改這個學習檔案的內容。";

$ec_student_word['name_chinese'] = "中文姓名";
$ec_student_word['name_english'] = "英文姓名";
$ec_student_word['registration_no'] = "學生編號";

# Warning message
$ec_iPortfolio['suspend_account_confirm'] = "你是否確定要暫停所選用戶？";
$ec_iPortfolio['suspend_template_confirm'] = "你是否確定要停用所選的模板？";
$ec_iPortfolio['active_template_confirm'] = "你是否確定要恢復所選的模板？";
$ec_iPortfolio['delete_attachment_confirm'] = "你是否確定要刪除所選的附件？";
$ec_iPortfolio['delete_template_confirm'] = "你是否確定要刪除所選的模板？";
$ec_iPortfolio['photo_remove_confirm'] = "你是否確定要刪除已上載的相片？";
$ec_iPortfolio['student_photo_upload_remind'] = "上載前請先以有大小寫之分的學生編號命名相片並壓縮相片至'.ZIP'檔案格式！<br />相片只能以'.JPG'的格式上載，相片大小也規定為100 X 130 像素 (闊 x 高)。";
$ec_iPortfolio['student_photo_upload_warning'] = "相片必須要以'.ZIP'檔案格式上載。";
$ec_iPortfolio['used_template_cant_delete'] = "*注意: 使用中的範本不能被刪除";
$ec_warning['comment_content'] = "請先填上你的意見。";
$ec_warning['comment_submit_confirm'] = "你是否確定要提交意見？";
$ec_warning['growth_title'] = "請輸入計劃名稱。";
$ec_warning['notes_title'] = "請輸入題目。";
$ec_warning['please_select_class'] = "請選擇班別！";
$ec_warning['please_select_subject'] = "請選擇科目！";
$ec_warning['please_enter_pos_integer'] = "請輸入正數數字";
$ec_warning['start_end_year'] = "終結年份不能早於開始年份。";
$ec_warning['start_end_hours'] = "最多時數不能少於最少時數。";
$ec_warning['start_end_scheme_start_time'] = "開始時間不能早於學習檔案開始時間。";
$ec_warning['start_end_scheme_end_time'] = "終結時間不能遲於學習檔案終結時間。";

# for semester sort name
$ec_iPortfolio['semester_name_array_1'] = array(1, '一', '上', 'first', '1st Semester');
$ec_iPortfolio['semester_name_array_2'] = array(2, '二', '中', 'second', '2nd Semester');
$ec_iPortfolio['semester_name_array_3'] = array(3, '三', '下', 'third', '3rd Semester');

//$ec_iPortfolio['cutomized_report_remark_chi'] = "有關報告內之詳情，可參閱學生綜合表現紀錄小冊子";
//$ec_iPortfolio['cutomized_report_remark_eng'] = "For details, please refer to the handbook of Student Portfolio";

#for Kei Wai Report
$ec_iPortfolio['keiwai_part_A'] = "【甲部】";
$ec_iPortfolio['keiwai_part_B'] = "【乙部】";
$ec_iPortfolio['keiwai_part_C'] = "【丙部】";
$ec_iPortfolio['keiwai_part_D'] = "【丁部】";
$ec_iPortfolio['keiwai_part_E'] = "【戊部】";
$ec_iPortfolio['keiwai_part_F'] = "【己部】";
$ec_iPortfolio['keiwai_part_G'] = "【庚部】";
$ec_iPortfolio['keiwai_personal_info'] = "個人資料";
$ec_iPortfolio['keiwai_education'] = "教育程度";
$ec_iPortfolio['keiwai_academic_results'] = "學業成績";
$ec_iPortfolio['keiwai_professional_profile'] = "考獲資格";
$ec_iPortfolio['keiwai_extra_data_manage'] = "基慧補充資料管理";
$ec_iPortfolio['keiwai_report'] ="學生個人成就紀錄表";
$ec_iPortfolio['keiwai_report_2'] ="學生個人成就證明書";
$ec_iPortfolio['keiwai_HKID'] = "香港身份證號碼";
$ec_iPortfolio['keiwai_religion'] = "宗教信仰";
$ec_iPortfolio['pob'] = "出生地點";
$ec_iPortfolio['nationality'] = "國籍";
$ec_iPortfolio['telno'] = "聯絡電話";
$ec_iPortfolio['keiwai_subject_area'] = "項目名稱";
$ec_iPortfolio['keiwai_organization_a'] = "頒發機構";
$ec_iPortfolio['keiwai_organization_b'] = "舉辦機構";
$ec_iPortfolio['keiwai_organization_c'] = "服務機構";
$ec_iPortfolio['service'] = "服務紀錄";
$ec_iPortfolio['female'] = "女";
$ec_iPortfolio['male'] = "男";
$ec_iPortfolio['institute'] = "學校名稱";
$ec_iPortfolio['doc'] = "修讀年級";
$ec_iPortfolio['keiwai_period'] = "年份";
$ec_iPortfolio['keiwai_classlevel'] = "年級";
$ec_iPortfolio['keiwai_position_c'] = "全班名次";
$ec_iPortfolio['keiwai_totalnumber_c'] = "全班人數";
$ec_iPortfolio['keiwai_position_l'] = "全級名次";
$ec_iPortfolio['keiwai_totalnumber_l'] = "全級人數";
$ec_iPortfolio['keiwai_certificate'] = "考獲證書";
$ec_iPortfolio['keiwai_qualification'] = "項目成績";
$ec_iPortfolio['keiwai_organization'] = "簽發機構";

$assignments_alert_msg6 = "HTML 預覽";
$assignments_alert_msg9 = "日期不正確";
$assignments_alert_msg13 = " 檔案儲存容量必須是正數";
$button_activate_iPortfolio = "啟用";
$button_apply = "應用";
$button_new_template = "新增模板";
$button_edit_template = "編輯模板";
$button_organize = "整理";
$button_private = "保密";
$button_public = "公開";
$button_publish_iPortfolio = "發佈";
$button_save_and_publish_iPortfolio = "儲存及發佈";
$con_msg_add = "已增加紀錄";
$con_msg_del = "已刪除紀錄";
$con_msg_save = "已儲存紀錄";
$con_msg_update = "已更新紀錄";
$EndTime = "終結時間";
$i_general_others = "其他";
$i_general_settings = "設定";
$i_list_all = "全部";
$i_status_shared = "已分享";
$i_status_usable = "可使用";
$list_display = "每頁顯示";
$list_item_no = "項";
$msg_check_at_least_one = "請最少選擇一個選項";
$msg_check_at_least_one_item = "請最少選擇一個項目";
$msg_check_at_most_one = "請選擇其中一項";
$MyInfo = "我的個人資料";
$namelist_class_number = "學號";
$notes_after = "之後";
$notes_position = "位置";
$notes_title = "題目";
$notes_under = "之下";
$no_record_msg = "暫時仍未有任何紀錄。";
$profiles_to = "至";
$profile_dob = "出生日期";
$profile_gender = "性別";
$profile_modified = "最近修改日期";
$profile_nationality = "國籍";
$profile_pob = "出生地點";
$qbank_status = "狀況";
$qbank_public = "公開";
$quizbank_desc = "簡介";
$range_all = "全部";
$StartTime = "開始時間";
$usertype_s = "學生";
$valueTitle = "分數";

$ec_iPortfolio['not_display'] = "不顯示";
$ec_iPortfolio['not_display_student_photo'] = "學生相片";
$ec_iPortfolio['no_student_details'] = "學生詳細資料";
$ec_iPortfolio['no_school_name'] = "校名於頁首";
$ec_iPortfolio['other_learning_record'] = "其他學習紀錄";
$ec_iPortfolio['show_component_sub'] = "顯示科目分卷";
$ec_iPortfolio['preview'] = "預覽";
$ec_words['school_badge'] = "校徽";
$ec_words['school_address'] = "學校地址";
$ec_iPortfolio['transcript_description'] = "學生成績報告單簡介";
$ec_iPortfolio['transcript_CSV_file'] = "CSV 補充資料";
$ec_iPortfolio['reading_record'] = "閱讀紀錄";
$ec_iPortfolio['ole_record'] = "其他學習經歷紀錄";
$ec_iPortfolio['file'] = "檔案";
$import_csv['download'] = "下載 CSV 檔案範本";
$ec_iPortfolio['customized_file_upload_remind'] = "注意：上載之檔案必須以年份命名。(例：2006.csv)";

$w_alert['start_end_time2'] = "終結日期不能早於開始日期。";
$wording['contents_notice1'] = "<font color=#AAAAAA>灰色</font>代表保密。";


	$ec_iPortfolio['eClass_update_assessment'] = "從 eClass 更新學業資料";
	$ec_iPortfolio['eClass_update'] = "從 eClass 更新";
	$ec_iPortfolio['eClass_update_merit'] = "從 eClass 更新獎懲紀錄";
	$ec_iPortfolio['eClass_update_activity'] = "從 eClass 更新活動紀錄";
	$ec_iPortfolio['eClass_update_attendance'] = "從 eClass 更新考勤紀錄";
	$ec_iPortfolio['eClass_update_award'] = "從 eClass 更新得獎紀錄";
	$ec_iPortfolio['eClass_update_comment'] = "從 eClass 更新老師評語";
	$ec_iPortfolio['update_more_assessment'] = "再更新學業紀錄";
	$ec_iPortfolio['update_more_award'] = "再更新得獎紀錄";
	$ec_iPortfolio['update_more_activity'] = "再更新活動紀錄";
	$ec_iPortfolio['update_more_attendance'] = "再更新考勤紀錄";
	$ec_iPortfolio['update_more_merit'] = "再更新獎懲紀錄";
	$ec_iPortfolio['update_more_comment'] = "再更新老師評語";
	$ec_iPortfolio['last_update'] = "更新日期";
	$ec_iPortfolio['SAMS_last_update'] = "最後更新日期";
	$ec_iPortfolio['activation_no_quota'] = "很抱歉，沒有足夠的戶口額！";
	$ec_iPortfolio['SAMS_code'] = "代碼";
	$ec_iPortfolio['SAMS_cmp_code'] = "分卷代碼";
	$ec_iPortfolio['SAMS_cmp_code'] = "分卷代碼";
	$ec_iPortfolio['SAMS_description'] = "說明 (英/中)";
	$ec_iPortfolio['SAMS_abbr_name'] = "名稱縮寫 (英/中)";
	$ec_iPortfolio['SAMS_short_name'] = "簡稱 (英/中)";
	$ec_iPortfolio['SAMS_display_order'] = "顯示次序";
	$ec_iPortfolio['SAMS_subject'] = "科目";
	$ec_iPortfolio['SAMS_short_name_duplicate'] = "簡稱不可重複";
	$ec_iPortfolio['WebSAMSRegNo'] = "WebSAMS RegNo";

	$ec_iPortfolio['record_submitted'] = "已申報獎項";
	$ec_iPortfolio['record_approved'] = "已批准獎項";
	$ec_iPortfolio['house'] = "社級";
	$ec_iPortfolio['pending'] = "審批中";
	$ec_iPortfolio['approved'] = "已批准";
	$ec_iPortfolio['rejected'] = "已拒絕";
	$ec_iPortfolio['approve'] = "批准";
	$ec_iPortfolio['reject'] = "拒絕";
	$ec_iPortfolio['status'] = "狀態";
	$ec_iPortfolio['source'] = "來源";
	$ec_iPortfolio['student_apply'] = "學生申報";
	$ec_iPortfolio['teacher_submit_record'] = "老師提交紀錄";
	$ec_iPortfolio['school_record'] = "學校紀錄";
	$ec_iPortfolio['process_date'] = "審批日期";
	$ec_iPortfolio['submit_external_award_record'] = "提交校外得獎紀錄";
	$ec_iPortfolio['total'] = "總數";
	$ec_iPortfolio['grade'] = "等級";
	$ec_iPortfolio['score'] = "分數";
	$ec_iPortfolio['stand_score'] = "標準分數";
	$ec_iPortfolio['rank'] = "級名次";
	$ec_iPortfolio['conduct_grade'] = "操行";
	$ec_iPortfolio['comment_chi'] = "中文評語";
	$ec_iPortfolio['comment_eng'] = "英文評語";
	$ec_iPortfolio['detail'] = "詳細資料";

	$ec_iPortfolio['all_status'] = "全部狀況";
	$ec_iPortfolio['all_category'] = "全部類別";
	$ec_iPortfolio['title'] = "名稱";
	$ec_iPortfolio['category'] = "類別";
	$ec_iPortfolio['category_code'] = "類別代號";
	$ec_iPortfolio['achievement'] = "獎項 / 証書文憑 / 成就";
	$ec_iPortfolio['organization'] = "合辦機構";
	$ec_iPortfolio['details'] = "詳情";
	$ec_iPortfolio['approved_by'] = "審批老師";
	$ec_iPortfolio['attachment'] = "附件 / 証明";
	$ec_iPortfolio['attachment_only'] = "附件";
	$ec_iPortfolio['startdate'] = "開始日期";
	$ec_iPortfolio['enddate']= "結束日期";
	$ec_iPortfolio['or'] = "或";
	$ec_iPortfolio['add_enddate'] = "加入結束日期";
	$ec_iPortfolio['unit_hour'] = "小時";
	$ec_iPortfolio['school_remarks'] = "學校備註";

	$ec_warning['date'] = "請輸入日期。";
	$w_alert['Invalid_Date'] = "日子錯誤";
	$w_alert['Invalid_Time'] = "時間錯誤";
	$ec_warning['title'] = "請輸入名稱。";
	$ec_iPortfolio['upload_error'] = "請輸入正確的檔案路徑";


	$ec_iPortfolio_ele_array = array("[ID]"=>"智育發展",
									"[MCE]"=>"德育及公民教育",
									"[CS]"=>"社會服務",
									"[PD]"=>"體育發展",
									"[AD]"=>"藝術發展",
									"[CE]"=>"與工作有關經驗",
									"[OTHERS]"=>"其他");
	$ec_iPortfolio['type_competition'] = "比賽";
	$ec_iPortfolio['type_activity'] = "活動";
	$ec_iPortfolio['type_course'] = "課程";
	$ec_iPortfolio['type_service'] = "服務";
	$ec_iPortfolio['type_award'] = "獎項";
	$ec_iPortfolio['type_others'] = "其他";
	$ec_iPortfolio_category_array = array("1"=>$ec_iPortfolio['type_competition'],
										"2"=>$ec_iPortfolio['type_activity'],
										"3"=>$ec_iPortfolio['type_course'],
										"4"=>$ec_iPortfolio['type_service'],
										"5"=>$ec_iPortfolio['type_award'],
										"6"=>$ec_iPortfolio['type_others']);
	$ec_iPortfolio['waiting_approval']= "等待批核";
	$ec_iPortfolio['program']= "活動";
	$ec_iPortfolio['program_list']= "活動一覽";
	$ec_warning['olr_from_teacher'] = "此乃老師呈交之紀錄，無需審批！";
	$ec_iPortfolio['student_list']= "學生名單";
	$ec_iPortfolio['assign_more_student']= "加入更多學生";
	$ec_iPortfolio['no_record_found'] = "此檔案不存在";
	$ec_iPortfolio['no_privilege_to_read_file'] = "你沒有權限觀看此檔案";
	$ec_iPortfolio['new_ole_activity'] = "新增其他學習紀錄活動";
	$ec_iPortfolio['assign_student_now'] = "加入學生";
	$ec_iPortfolio['comment'] = "評語";
	$ec_iPortfolio['no_of_records'] = "紀錄數目";


$ip_lang['class_history_management'] = "班級紀錄管理";

$iDiscipline['Conduct_Category'] = "行為類別";
$iDiscipline['Conduct_Item'] = "行為項目";
//$iDiscipline['Good_Conduct_Import_FileDescription'] = "$i_Discipline_System_Discipline_Case_Record_Case_Semester, $i_UserClassName, $i_ClassNumber, $i_Discipline_System_Discipline_Category , ".$iDiscipline['Conduct_Category'].", ".$iDiscipline['Conduct_Item'].", $i_Discipline_System_Discipline_Case_Record_PIC, ".$iDiscipline['RecordDate'].",$i_UserRemark";
$iDiscipline['Good_Conduct_Import_FileDescription'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Record Date</td><td>Semester</td><td>Item Code</td><td>PIC</td><td>Remark</tr>
<tr><td>1A</td><td>11</td><td>2009-03-20</td><td>Fall Semester</td><td>Item01</td><td>Teacher01</td><td>Remark</td></tr></table>";
//$iDiscipline['Conduct_Mark_Import_FileDescription'] = "$i_general_Year, $i_SettingsSemester, $i_UserClassName, $i_ClassNumber, $i_Discipline_System_Discipline_Case_Record_PIC, $i_Discipline_System_Discipline_Conduct_Mark_Adjustment,$i_Attendance_Reason";
$iDiscipline['Conduct_Mark_Import_FileDescription'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Year</td><td>Semester</td><td>Adjustment</td><td>PIC</td><td>Reason</td></tr>
<tr><td>1A</td><td>12</td><td>2008-2009</td><td>Fall semester</td><td>20</td><td>Teacher01</td><td>Helping Teacher</td></tr>
</table>";
//$iDiscipline['Detention_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_Attendance_Reason, $i_UserRemark";
$iDiscipline['Detention_Import_FileDescription'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Reason</td><td>Remark</td></tr>
<tr><td>1A</td><td>12</td><td>Late for school</td><td>Remark</td></tr>
</table>";
$iDiscipline['Good_Conduct_Missing_Category'] = "沒有Category";
$iDiscipline['Good_Conduct_Wrong_Category'] = "Wrong Category";
$iDiscipline['Good_Conduct_Missing_Conduct_Category'] = "沒有行為類別";
$iDiscipline['Good_Conduct_Missing_Conduct_Item'] = "沒有行為項目";
$iDiscipline['Good_Conduct_Missing_Conduct_Score'] = "沒有操分";
$iDiscipline['Good_Conduct_No_Conduct_Item'] = "沒有此行為項目";
$iDiscipline['Good_Conduct_No_Conduct_Category'] = "沒有此行為類別";
$iDiscipline['Good_Conduct_No_Staff'] = "沒有此職員";
$iDiscipline['Good_Conduct_No_Student'] = "沒有此學生";
$iDiscipline['Award_Punishment_Wrong_Merit_Type'] = "沒有此優點類型";
$iDiscipline['Award_Punishment_ConductScore_Numeric'] = "操分必須為數字";
$iDiscipline['Award_Punishment_No_Subject'] = "沒有此科目";
$iDiscipline['Award_Punishment_No_Merit_Num'] = "沒有獎懲數目";
$iDiscipline['Award_Punishment_Missing_Merit_Num'] = "沒有優點類型";
$iDiscipline['Award_Punishment_Merit_Numeric'] = "獎懲必須為數字";
$iDiscipline['Award_Punishment_Merit_Negative'] = "獎懲必須為整數";
$iDiscipline['Award_Punishment_Wrong_MeritType'] = "沒有此類別";
$iDiscipline['Award_Punishment_No_ItemCode'] = "沒有此項目編號";
$iDiscipline['Award_Punishment_Missin_ItemCode'] = "沒有項目編號";
$iDiscipline['Award_Punishment_Missing_Year'] = "沒有年份";
$iDiscipline['Award_Punishment_No_Year'] = "沒有此年份";
$iDiscipline['Award_Punishment_Missing_Semester'] = "沒有學期";
$iDiscipline['Award_Punishment_RecordDate_In_Future'] = "紀錄日期必須是今天或以後";
$iDiscipline['Award_Punishment_RecordDate_Error'] = "紀錄日期必須是日期";
$iDiscipline['Award_Punishment_Missing_RecordDate'] = "沒有紀錄日期";
$iDiscipline['Good_Conduct_Cat_Item_Mismatch'] = "行為類別及行為項目不符";
$iDiscipline['Good_Conduct_No_Category'] = "沒有此類別";
$iDiscipline['Good_Conduct_No_Period_Setting'] = "未設定累計時段";
$iDiscipline['Good_Conduct_Category_Mismatch'] = "行為類別及行為項目並不符類別";
$iDiscipline['Detention_Missing_Reason'] = "沒有留堂原因";
$iDiscipline['Assign_Duty_Teacher'] = "請最少選擇一名負責人。";

$iDiscipline['Form_Templates'] = "模板";
$iDiscipline['Form_answersheet_option'] = '選項數目';
$iDiscipline['Form_answersheet_header'] = '題目 / 標題';
$iDiscipline['Form_answersheet_content'] = '內容';
$iDiscipline['Form_answersheet_type_selection'] = "-- 選擇格式 --";
$iDiscipline['Form_answersheet_selection'] = "-- 選擇一數字 --";
$i_Discipline_System_Discipline_Conduct_Mark_No_Ratio_Setting = '此年度沒有此重設定';
//$iDiscipline['Case_Import_FileDescription'] = "$i_Discipline_System_Discipline_Case_Record_Case_Number,$i_Discipline_System_Discipline_Case_Record_Case_Title,$i_Discipline_System_Discipline_Case_Record_Case_Event_Date,$i_Discipline_System_Discipline_Case_Record_Case_Semester,$i_Discipline_System_Discipline_Case_Record_Case_Location,$i_Discipline_System_Discipline_Case_Record_Case_PIC";
$iDiscipline['Example'] = "例子";
$iDiscipline['Case_Import_FileDescription'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Case Number</td><td>Case Name</td><td>Event Date</td><td>Semester</td><td>Location</td><td>PIC</td><td>Remark</td></tr>
<tr><td>001</td><td>Case 1</td><td>2009-03-20</td><td>Fall Semester</td><td>Classroom</td><td>Teacher01</td><td>Remark</td></tr>
</table>";
$iDiscipline['Case_Missing_CaseNumber'] = "沒有個案編號";
$iDiscipline['Case_Missing_CaseName'] = "沒有個案名稱";
$iDiscipline['Case_Missing_Category'] = "沒有類別";
$iDiscipline['Case_Missing_EventDate'] = "沒有發生日期";
$iDiscipline['Case_Missing_Location'] = "沒有地點";
$iDiscipline['Case_Missing_PIC'] = "沒有負責人";
//$iDiscipline['Case_Student_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_general_record_date,$i_SettingsSemester,$i_Discipline_System_ItemCode,$i_Discipline_System_ConductScore,$i_Discipline_System_Discipline_Case_Record_PIC, ".$eDiscipline['Subject'].",$i_Discipline_System_Add_Merit,$i_ResourceCategory,".$iDiscipline['MeritType'].",$i_UserRemark";
/*$iDiscipline['Case_Student_Import_FileDescription'] = "Example:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Record Date</td><td>Semester</td><td>Item Code</td><td>Conduct Score</td><td>PIC</td><td>Subject</td><td>Add Merit Record</td><td>Category</td><td>Merit Type</td><td>Remark</td></tr>
<tr><td>1A</td><td>12</td><td>2009-03-20</td><td>Fall Semester</td><td>LATE002F</td><td>3</td><td>Teacher01</td><td>English</td><td>1</td><td>Punishment</td><td>-2</td><td>Remark</td></tr>
</table>";*/
$iDiscipline['Case_Student_Import_No_Semester'] = "沒有此學期";
$iDiscipline['Case_Student_Import_Conduct_Score_OutRange'] = "操行分超出了範圍";
$iDiscipline['Case_Student_Import_Quantity_OutRange'] = "數量超出了範圍";
$iDiscipline['Case_Student_Import_Conduct_Score_Negative'] = "操行分應少於0";
$iDiscipline['Case_Student_Import_Conduct_Score_Positive'] = "操行分應大於0";
$eDiscipline['Detention_Arrangement'] = "安排";
$eDiscipline['Session_Management'] = "時段管理";
$eDiscipline['Detention_Session_Default_String'] =  "$i_Discipline_Date | $i_Discipline_Time | $i_Discipline_Location | $i_Discipline_Vacancy | $i_Discipline_PIC";
$iDiscipline['Category_Item_Code_Warning'] = "項目編號已存在或曾被使用/刪除";
$iDiscipline['Reord_Import_Successfully'] = "紀錄已成功匯入";
$iDiscipline['Invalid_Date_Format'] = "日期格式錯誤";
$iDiscipline['Import_Instruct_Main'] = "要進行匯入，請：";
//$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1'] = "Press ";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2'] = "按此";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3'] = "下載CSV資料檔模板。";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1'] = "根據以下範例，於CSV資料檔模板中輸入紀錄。";
$iDiscipline['Import_Instruct_Note'] = "註:";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_1'] = "你需於CSV資料檔中以項目編號形式，指定每個紀錄所屬之良好及違規行為項目。你可以";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2'] = "按此";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2_1'] = "檢視良好及違規行為項目名稱及 對應之項目編號。";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_2_1'] = "請使用YYYY-MM-DD格式，於\"Record Date\"欄中輸入紀錄日期。";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_c_1'] = "儲存CSV資料檔。輸入CSV資料檔的路徑，或按\"瀏覽\"，從你的電腦中選取資料檔，然後按<strong>繼續</strong>。";

$iDiscipline['Award_Punishment_Import_Instruct_Note_1_1'] = "你需於CSV資料檔中以項目編號形式，指定每個紀錄所屬之獎懲項目。你可以";
$iDiscipline['Award_Punishment_Import_Instruct_Note_1_2'] = "按此";
$iDiscipline['Award_Punishment_Import_Instruct_Note_1_2_1'] = "檢視獎懲項目名稱及 對應之項目編號。";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_1'] = "<b>只適用於從1.0版升級之用戶：</b>你需於CSV資料檔中以科目編號形式，指定每個紀錄所屬之科目。你可以";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_2'] = "按此";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_3'] = "檢視科目名稱及對應之科目編號。如不適用，此欄可留空。 ";
$iDiscipline['Award_Punishment_Import_Instruct_Note_3_1'] = "請參閱以下代號，於\"Type\"欄及\"Quantity\"欄填寫紀錄的獎懲內容。";
$iDiscipline['Award_Punishment_Import_Instruct_Note_3_2'] = "檢視代號";
$iDiscipline['Award_Punishment_Import_Instruct_Note_3_3'] = "如記三個缺點，請於\"Quantity\"欄及\"Type\"欄分別輸入\"3\"及\"{代表缺點的代號}\"。 ";
$iDiscipline['Award_Punishment_Import_Instruct_Note_4_1'] = "請使用YYYY-MM-DD格式，於\"Record Date\"欄中輸入紀錄日期。";
$iDiscipline['Award_Punishment_Import_Instruct_c_1'] = "儲存CSV資料檔。輸入CSV資料檔的路徑，或按\"瀏覽\"，從你的電腦中選取資料檔，然後按<strong>繼續</strong>。";

$iDiscipline['Case_Record_Import_Instruct_Note_1_1'] = "請使用YYYY-MM-DD格式，於\"Record Date\"欄中輸入紀錄日期。";
$iDiscipline['Case_Record_Import_Instruct_c_1'] = "儲存CSV資料檔。輸入CSV資料檔的路徑，或按\"瀏覽\"，從你的電腦中選取資料檔，然後按<strong>繼續</strong>。";
$iDiscipline['Import_Source_File'] = "CSV資料檔";
$iDiscipline['Import_Source_File_Note'] = "你可於\"資料檔案\"欄輸入CSV資料檔的路徑，或按\"瀏覽\"，從你的電腦中選取資料檔。";

###############################################################################
# scrabble fun corner
###############################################################################
$word_sfc['title'] = "拼字樂";
$word_sfc['description'] = "「Scrabble拼字樂」讓你透過不同的有趣小遊戲，輕輕鬆鬆地掌握到Scrabble拼字遊戲的規則和技巧。";

# 20090403 yatwoon (semester related)
$semester_change_warning = "警告：切勿於學年結束前修改\"學期更新模式\"，否則將破壞系統中所儲存紀錄的完整性，引起資料損失或其他嚴重後果。";

# 20090627 yat woon - eRC reports
$i_alert_PleaseSelectClassForm = "請選擇班別或級別";

###############################################################################

###############################################################################
# LSLP
###############################################################################
$i_LSLP['LSLP'] = "通識學習平台";
$i_LSLP['admin_user_setting'] = "設定管理用戶";
$i_LSLP['user_license_setting'] = "許可證設定";
$i_LSLP['license_display_1'] = "你的通識學習平台許可證有";
$i_LSLP['license_display_2'] = " 個，尚有 ";
$i_LSLP['license_display_3'] = " 個可供使用。";
$i_LSLP['license_display_4'] = "你已使用的許可證為 ";
$i_LSLP['license_display_5'] = " 個。";
$i_LSLP['used_up_all_license_msg'] = "你已使用所有許可證, 你不可再新增任何教室.";
$i_LSLP['license_period_expired'] = "許可證已過期";
$i_LSLP['license_exceed_msg1'] = "你只有";
$i_LSLP['license_exceed_msg2'] = "個許可證可供使用。";
$i_LSLP['license_exceed_msg3'] = "請返回";
$i_LSLP['school_license'] = "學校許可證";
$i_LSLP['student_license'] = "學生許可證";
$i_LSLP['license_type'] = "許可證類型";
$i_LSLP['license_period'] = "許可證有效期";
###############################################################################

if (is_file("$intranet_root/templates/lang.$intranet_session_language.customized.php"))
{
  include_once("$intranet_root/templates/lang.$intranet_session_language.customized.php");
}

# if setting of merit/demerit title are set
# load the setting
$merit_demerit_customize_file = "$intranet_root/file/merit.b5.customized.txt";
if(file_exists($merit_demerit_customize_file) && is_file($merit_demerit_customize_file) && filesize($merit_demerit_customize_file)!=0){
	if ($file_content_merit_demeirt_wordings=="")
		$file_content_merit_demeirt_wordings = get_file_content($merit_demerit_customize_file);
	$lines = explode("\n",$file_content_merit_demeirt_wordings);

	$i_Merit_Merit = $lines[0];
	$i_Merit_MinorCredit = $lines[1];
	$i_Merit_MajorCredit = $lines[2];
	$i_Merit_SuperCredit = $lines[3];
	$i_Merit_UltraCredit = $lines[4];

	$i_Merit_BlackMark = $lines[5];
	$i_Merit_MinorDemerit = $lines[6];
	$i_Merit_MajorDemerit = $lines[7];
	$i_Merit_SuperDemerit = $lines[8];
	$i_Merit_UltraDemerit = $lines[9];
}

$i_Merit_TypeArray = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_TypeArrayWithWarning = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_Warning,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_studentAttendance_HostelAttendance_InputReason = '請輸入原因';
?>