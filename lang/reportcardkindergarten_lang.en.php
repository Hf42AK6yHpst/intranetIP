<?php
$Lang['eReportCardKG']['Title'] = "(eRC)";

$Lang['eReportCardKG']['General']['NoRecord'] = 'No Record';
$Lang['eReportCardKG']['General']['PleaseConfirmChanges'] = 'Please caution updated settings might affect other functions of eReportCard (Kindergarten).';
$Lang['eReportCardKG']['General']['PleaseConfirmScoreUpdate'] = 'Existing input data will be overwritten.';

$Lang['eReportCardKG']['Management']['LearningZone']['Title'] = "Learning Zone";

$Lang['eReportCardKG']['Management']['TopicTimeTable']['Title'] = "Learning TimeTable";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['StartDate'] = "Start Date";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['EndDate'] = "End Date";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseEnterTopic'] = "Please Enter Topic";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseEnterYear'] = "Please Enter Year";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['NoLearningTool'] = "No Learning Tool";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['NumOfZones'] = "Number of zones";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseConfirmChanges'] = 'Zone Settings are updated. Please click the "Submit" button to confirm the changes.';

$Lang['eReportCardKG']['Management']['ToolScore']['Title'] = "Input Score Revision (Learning Tool)";
$Lang['eReportCardKG']['Management']['ToolScore']['ToolList'] = "Learning Tool List";
$Lang['eReportCardKG']['Management']['ToolScore']['InputScore'] = "Input Score";
$Lang['eReportCardKG']['Management']['ToolScore']['AllTimeTable'] = "All TimeTable";
$Lang['eReportCardKG']['Management']['ToolScore']['TimeTable'] = "TimeTable";
$Lang['eReportCardKG']['Management']['ToolScore']['TitleName'] = "Topic";
$Lang['eReportCardKG']['Management']['ToolScore']['ClassName'] = "Class";
$Lang['eReportCardKG']['Management']['ToolScore']['TeachingTools'] = "Teaching Tool";
$Lang['eReportCardKG']['Management']['ToolScore']['ClassNumber'] = "Class No";
$Lang['eReportCardKG']['Management']['ToolScore']['StudentName'] = "Student";
$Lang['eReportCardKG']['Management']['ToolScore']['AddNewInputScoreRow'] = "Add Input Score Rows";
$Lang['eReportCardKG']['Management']['ToolScore']['MaxAddNewInputScoreRow'] = "Added row number cannot greater than 20";
$Lang['eReportCardKG']['Management']['ToolScore']['InputMarkInvalid'] = "Invalid input mark.";
$Lang['eReportCardKG']['Management']['ToolScore']['InputMarkCannotBeNegative'] = "The input mark cannot be negative.";
$Lang['eReportCardKG']['Management']['ToolScore']['InputMarkCannotBeLargerThanFullMark'] = "The input mark cannot be larger than the full mark.";

$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Title'] = "Input Score Revision (Subjects' Ability Targets)";
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputScore'] = "Input Score";
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Form'] = 'Form';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Subject'] = 'Subject';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['TermTimeTable'] = 'Term / Learning TimeTable';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Term'] = 'Term';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['TimeTable'] = 'Learning TimeTable';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputMarkInvalid'] = "Invalid input mark.";
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputMarkCannotBeNegative'] = "The input mark cannot be negative.";
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputMarkCannotBeLargerThanFullMark'] = "The input mark cannot be larger than the full mark.";

$Lang['eReportCardKG']['Management']['LanguageBehavior']['Title'] = "Language Proficiency and Behavioural Indicators";
$Lang['eReportCardKG']['Management']['LanguageBehavior']['InputScore'] = "Input Score";

$Lang['eReportCardKG']['Management']['OtherInfo']['Title'] = "Other Info";
$Lang['eReportCardKG']['Management']['OtherInfoArr']['summary'] = "Student Info";
$Lang['eReportCardKG']['Management']['OtherInfoArr']['award'] = "Award";
$Lang['eReportCardKG']['Management']['OtherInfoWarningArr']['InputGrade'] = "Input Grade";
$Lang['eReportCardKG']['Management']['OtherInfoWarningArr']['InputNumber'] = "Input Number";

$Lang['eReportCardKG']['Management']['GenerateReports']['Title'] = "Print Report Card";

$Lang['eReportCardKG']['Management']['ExportPerformance']['Title'] = "Export Student Performance";
$Lang['eReportCardKG']['Management']['ExportYearlyPerformance']['Title'] = "Export Student Yearly Performance";

$Lang['eReportCardKG']['Management']['DataTransition']['Title'] = "Data Handling";
$Lang['eReportCardKG']['Management']['DataTransition']['Transition'] = "Transition";
$Lang['eReportCardKG']['Management']['DataTransition']['ActiveAcademicYear'] = "Active Academic Year";
$Lang['eReportCardKG']['Management']['DataTransition']['CurrentAcademicYear'] = "Current Academic Year";
$Lang['eReportCardKG']['Management']['DataTransition']['NewAcademicYear'] = "New academic year";
$Lang['eReportCardKG']['Management']['DataTransition']['OtherAcademicYears'] = "Other academic years";
$Lang['eReportCardKG']['Management']['DataTransition']['DataTransitionWarning1'] = "Please proceed this action before:<br />- start using eReportCard for a new academic year, or,<br />- restore eReportCard to use the data of previous academic years";
$Lang['eReportCardKG']['Management']['DataTransition']['DataTransitionWarning2'] = "The data from the current active academic year will be preserved.";
$Lang['eReportCardKG']['Management']['DataTransition']['ConfirmCreateNewYearDatabase'] = "The template data of the new academic year will be copied from school year <!--academicYearName-->. Are you sure you want to create new academic year data for the system?";
$Lang['eReportCardKG']['Management']['DataTransition']['ConfirmTransition'] = "Proceed to do selected data transition?";

$Lang['eReportCardKG']['Management']['Device']['Title'] = "Tablet Interface";
$Lang['eReportCardKG']['Management']['Device']['LearningZone'] = "Learning Zone";
$Lang['eReportCardKG']['Management']['Device']['InputScore'] = "Input Lesson Score";
$Lang['eReportCardKG']['Management']['Device']['Class'] = "班別";
$Lang['eReportCardKG']['Management']['Device']['Topic'] = "主題";
$Lang['eReportCardKG']['Management']['Device']['NoClasses'] = "無所屬班別";
$Lang['eReportCardKG']['Management']['Device']['NoTopics'] = "無可以選擇主題";
$Lang['eReportCardKG']['Management']['Device']['NotSelectAnyClasses'] = "無選擇任何班別";
$Lang['eReportCardKG']['Management']['Device']['NotSelectAnyTopics'] = "無選擇任何主題";
$Lang['eReportCardKG']['Management']['Device']['CompleteInputScore'] = "已評分";
$Lang['eReportCardKG']['Management']['Device']['IncompleteInputScore'] = "未評分";
$Lang['eReportCardKG']['Management']['Device']['AllEntryStatus'] = "全部";
$Lang['eReportCardKG']['Management']['Device']['InZoneStatus'] = "已進入";
$Lang['eReportCardKG']['Management']['Device']['CompleteStatus'] = "已完成";
$Lang['eReportCardKG']['Management']['Device']['IncompleteStatus'] = "未完成";
$Lang['eReportCardKG']['Management']['Device']['NoQuota'] = "學習區已滿";
$Lang['eReportCardKG']['Management']['Device']['PleaseLeaveFirst1'] = "你仍未離開學習區[";
$Lang['eReportCardKG']['Management']['Device']['PleaseLeaveFirst2'] = "]";
$Lang['eReportCardKG']['Management']['Device']['PleaseSelectTool'] = "請選擇教具";

$Lang['eReportCardKG']['Setting']['MacauAbilityIndex']['Title'] = "The Requirements of Basic Academic Attainments";
$Lang['eReportCardKG']['Setting']['MacauAbilityIndex']['Code'] = "Code";
$Lang['eReportCardKG']['Setting']['MacauAbilityIndex']['Name'] = "The Requirements of Basic Academic Attainments";

$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Title'] = "Ability Targets";
$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Code'] = "Code";
$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Name'] = "Ability Targets";

$Lang['eReportCardKG']['Management']['AbilityRemarks']['Title'] = 'Ability Target Grading Remark (Form-based & Term-based)';
$Lang['eReportCardKG']['Management']['AbilityRemarks']['Reset'] = 'Reset Remark';
$Lang['eReportCardKG']['Management']['AbilityRemarks']['ConfirmResetRemark'] = 'Confirm to reset remark?';
$Lang['eReportCardKG']['Management']['AbilityRemarks']['ChangeAbilityResetAll'] = 'All related remarks will be reset as well. Are you sure you want to change to the selected Ability Targets?';

$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Title'] = 'Ability Target Grading Remark';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Grading'] = 'Grading';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['UpperLimit'] = 'Grading Range';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Comment'] = 'Ability Target Grading Remark (Original)';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['OriComment'] = 'Ability Target Grading Remark (Modified)';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['NewComment'] = 'Ability Target Grading Remark (Form-based & Term-based)';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Term'] = 'Academic Term';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Year'] = 'Academic Year';

$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['Title'] = "Ability Target Mapping";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['MacauCode'] = "Macau Code";
//$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['TaiwanCode'] = "Taiwan Codes";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['TaiwanCode'] = "Ability Targets' Codes";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllMacauCat'] = "All Macau Codes";
//$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanCat'] = "All Taiwan Codes";
//$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanLevel'] = "All Taiwan Class Levels";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanCat'] = "All Ability Targets' Codes";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanLevel'] = "All Ability Targets' Class Levels";

$Lang['eReportCardKG']['Setting']['FormTopic']['Title'] = "Form Topics";
$Lang['eReportCardKG']['Setting']['LearningZone']['Title'] = "Learning Zone";
$Lang['eReportCardKG']['Setting']['TeachingTool']['Title'] = "Teaching Tools";
$Lang['eReportCardKG']['Setting']['TeachingToolCategory']['Title'] = "Teaching Tools' Category";
$Lang['eReportCardKG']['Setting']['LanguageBehavior']['Title'] = "Language Proficiency and Behavioural Indicators";
$Lang['eReportCardKG']['Setting']['LanguageBehaviorItem']['Title'] = "Language Proficiency and Behavioural Indicators' Items";
$Lang['eReportCardKG']['Setting']['LanguageBehavior']['Category1'] = "Language Proficiency";
$Lang['eReportCardKG']['Setting']['LanguageBehavior']['Category2'] = "Behavioural Indicators";

$Lang['eReportCardKG']['Setting']['Code'] = "Code";
$Lang['eReportCardKG']['Setting']['Type'] = "Type";
$Lang['eReportCardKG']['Setting']['Name'] = "Name";
$Lang['eReportCardKG']['Setting']['NameB5'] = "Name (Chinese)";
$Lang['eReportCardKG']['Setting']['NameEN'] = "Name (English)";
$Lang['eReportCardKG']['Setting']['ApplyForm'] = "Apply Form";
$Lang['eReportCardKG']['Setting']['ZoneTopic'] = "Related Topic";
$Lang['eReportCardKG']['Setting']['ZoneQuota'] = "Quota";
$Lang['eReportCardKG']['Setting']['PhotoRemarks'] = "Photos with size 300px x 300px (W x H) were recommanded";
$Lang['eReportCardKG']['Setting']['PleaseInputContent'] = "Please input content";
$Lang['eReportCardKG']['Setting']['PleaseInputNum'] = "Please input number";
$Lang['eReportCardKG']['Setting']['PleaseSelectForm'] = "Please select form level";
$Lang['eReportCardKG']['Setting']['PleaseOneOption'] = "Please select one option";
$Lang['eReportCardKG']['Setting']['Photo'] = "Photo";
$Lang['eReportCardKG']['Setting']['Picture'] = "Zone Picture";
$Lang['eReportCardKG']['Setting']['AbilityIndex'] = "Ability Index";
$Lang['eReportCardKG']['Setting']['Year'] = "Year";
$Lang['eReportCardKG']['Setting']['Remarks'] = "Remarks";
$Lang['eReportCardKG']['Setting']['ToolCategory'] = "Teaching Tools' Category";
$Lang['eReportCardKG']['Setting']['Chapter'] = "Chapter";

$Lang['eReportCardKG']['Setting']['InputWarning'] = "Please Input ";
$Lang['eReportCardKG']['Setting']['DuplicateWarning'] = " duplicated";
$Lang['eReportCardKG']['Setting']['EditWarning']['NoMoreThan1'] = "Edit item cannot more than one";
$Lang['eReportCardKG']['Setting']['EditWarning']['PleaseSelectEdit'] = "Please Select Edit Item";
$Lang['eReportCardKG']['Setting']['DeleteWarning']['PleaseSelectDelete'] = "Please Select Delete Item";

$Lang['eReportCardKG']['Setting']['SubjectMapping']['Title'] = 'Subjects\' Ability Targets';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['Form'] = 'Year';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['Subject'] = 'Subject';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['TermTimeTable'] = 'Term / Learning TimeTable';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['Term'] = 'Term';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['TimeTable'] = 'Learning TimeTable';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TermInUse'] = 'Term is already applied to this form and subject';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TermUsed'] = 'This term is already used';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TermEmpty'] = 'Please Input Term';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TopicInUse'] = 'TimeTable is already applied to this form and subject';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TopicUsed'] = 'TimeTable is already applied to this form and subject';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TopicEmpty'] = 'Please Input TimeTable';

$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['ModuleTitle'] = 'Ability Targets\' Category Selection (Form-based & Term-based)';
$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['Title'] = 'Ability Target Grading Remark Selection';
$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['Selections'] = 'Selections';
$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['None'] = 'None of Selected';

$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Title'] = 'Schedule';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Term'] = 'Term';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['StartDate'] = 'Start Date';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['StartTime'] = 'Start Time';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['EndDate'] = 'End Date';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['EndTime'] = 'End Time';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ModifiedDate'] = 'Last Modified Date';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['InputScorePeriod'] = "Submission";
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ViewReportPeriod'] = "Print Report";

$Lang['eReportCardKG']['Management']['AwardGeneration']['Title'] = "Select / Generate Student Award";
$Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'] = "Student Award";
$Lang['eReportCardKG']['Management']['AwardGeneration']['SelectAward'] = "Select Award";
$Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAward'] = "Generate Award";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReceivedAwardCount'] = "No. of Awarded Students";
$Lang['eReportCardKG']['Management']['AwardGeneration']['LastGenerate'] = "Last Generate";
$Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAll'] = "Generate Award for all classes";

$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoAward'] = "Please select an award.";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoClass'] = "Please select a class.";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoStudentInClass'] = "There is no Student in this Class";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoAwardedStudentSettings'] = "There is no Student getting this Award";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['AtLeastOneStudent'] = "Please select at least one student.";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['DeleteAward'] = "Are you sure you want to delete the award of this student?";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['ReGenerateAward'] = "The generated awards will be overwritten if you generate the awards now. Are you sure you want to generate the awards now?";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardGenerateSuccess'] = "1|=|Award Generated Successfully.";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardGenerateFailed'] = "0|=|Award Generation Failed.";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteSuccess'] = "1|=|Award Deleted Successfully.";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteFailed'] = "0|=|Award Deletion Failed.";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddSuccess'] = "1|=|Award Added Successfully.";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddFailed'] = "0|=|Award Addition Failed.";

$Lang['eReportCardKG']['Setting']['AwardsList']['Title'] = "Student Awards";
$Lang['eReportCardKG']['Setting']['AwardsList']['AwardType'] = "Award Type";
$Lang['eReportCardKG']['Setting']['AwardsList']['AwardTypeArr']['Generate'] = "Generated by System";
$Lang['eReportCardKG']['Setting']['AwardsList']['AwardTypeArr']['ManualInput'] = "Manual Input";
$Lang['eReportCardKG']['Setting']['AwardsList']['AwardTypeArr']['G'] = "Award Generated by System";
$Lang['eReportCardKG']['Setting']['AwardsList']['AwardTypeArr']['I'] = "Award Selected by Teacher";

# Settings > Admin Group
$Lang['eReportCardKG']['Setting']['AdminGroup']['MenuTitle'] = 'Admin Group';
$Lang['eReportCardKG']['Setting']['AdminGroup']['Title'] = 'Admin Group';
$Lang['eReportCardKG']['Setting']['AdminGroup']['NumOfMember'] = "No. of Members";
$Lang['eReportCardKG']['Setting']['AdminGroup']['NewAdminGroupStep'][1] = "Info & Access Right";
$Lang['eReportCardKG']['Setting']['AdminGroup']['NewAdminGroupStep'][2] = "Select Member(s)";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupCode'] = "Admin Group Code";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupName'] = "Admin Group Name";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupList'] = "Admin Group List";
$Lang['eReportCardKG']['Setting']['AdminGroup']['MemberList'] = "Member List";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AddMember'] = "Add Member";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupInfo'] = "Admin Group Info";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AccessRight'] = "Access Right";

$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['AddSuccess'] = "1|=|Admin Group Added.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['AddFailed'] = "0|=|Admin Group Addition Failed.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['EditSuccess'] = "1|=|Admin Group Edited.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['EditFailed'] = "0|=|Admin Group Edition Failed.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['DeleteSuccess'] = "1|=|Admin Group(s) Deleted.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['DeleteFailed'] = "0|=|Admin Group(s) Deletion Failed.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['AddMemberSuccess'] = "1|=|Admin Group Member Added.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['AddMemberFailed'] = "0|=|Admin Group Member Addition Failed.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['DeleteMemberSuccess'] = "1|=|Admin Group Member Deleted.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['DeleteMemberFailed'] = "0|=|Admin Group Member Deletion Failed.";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ModifiedDate'] = 'Last Modified Date';
$Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['SelectionContainsMember'] = "The highlighted options contain member(s) of this Admin Group already";
$Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['Blank']['Code'] = "Code cannot be blank";
$Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['Blank']['Name'] = "Name cannot be blank";
$Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['InUse']['Code'] = "Code is in use already";

$Lang['eReportCardKG']['Management']['ArchiveReport']['Title'] = "Report Card Archive";
$Lang['eReportCardKG']['Management']['ArchiveReport']['LastArchived'] = "Last Archive";
$Lang['eReportCardKG']['Management']['ArchiveReport']['ConfirmArchiveReport'] = 'Existing Report Card Archive will be overwritten. Are you sure you want to continue?';

$Lang['eReportCardKG']['Management']['PrintArchiveReport']['Title'] = "Print Archived Report";
$Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchTerm'] = "No matched term";
$Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchClass'] = "No matched class";
$Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchStudent'] = "No matched student";
?>