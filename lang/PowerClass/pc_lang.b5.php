<?php
if ($sys_custom['PowerClass']) {
	// Editing by 
	$Lang['Header']['HomeTitle'] = "Power Class @ eClass";
	
	$Lang["PageTitle"] = "Power Class @ eClass";
	$Lang['Header']['Menu']['SystemSetting'] = "學校基本設定";
	$Lang['Header']['Menu']['PC_App'] = "手機應用程式";
	$Lang['Header']['Menu']['PowerLesson2'] = "互動課堂";
	$Lang['Header']['Menu']['StemXPl2'] = "STEM@eClass";
	$Lang['Header']['Menu']['PowerClassReport'] = "分析";
	$Lang['Header']['Menu']['iCalendar'] = "我的行事曆";
	$Lang['Header']['Menu']['eBooking'] = "電子資源預訂系統";
	$Lang['Header']['Menu']['eAttednance'] = "考勤管理";
	$Lang['Header']['Menu']['eAttendanceLesson'] = "考勤管理(課堂考勤)";
	$Lang['Header']['Menu']['ePayment'] = "智能咭繳費系統";
	$Lang['Header']['Menu']['ChangePassword'] = "更改密碼";
	$Lang['Header']['Menu']['iAccount'] = "我的戶口";
	
	$Lang['Footer']['PowerBy'] = "Powered by ";
	
	$Lang['PowerClass'] = array();
	$Lang['PowerClass']['Chinese'] = "繁體";
	$Lang['PowerClass']['ChineseSimplified'] = "简体";
	$Lang['PowerClass']['English'] = "ENG";
	$Lang["PowerClass"]["Settings"] = "設定";
	$Lang["PowerClass"]["Add"] = "新增";
	$Lang["PowerClass"]["Import"] = "匯入";
	$Lang["PowerClass"]["SendTo"] = "發給";
	$Lang["PowerClass"]["Parent"] = "家長";
	$Lang["PowerClass"]["Student"] = "學生";
	$Lang["PowerClass"]["Teacher"] = "教師";
	$Lang["PowerClass"]["Staff"] = "教職員";
	$Lang["PowerClass"]["Payment"] = "繳費";
	
	$Lang['PowerClass']["Login"]["Login"] = "登入";
	$Lang['PowerClass']["Login"]["LoginTitle"] = "Employee Mobile App Platform";
	$Lang['PowerClass']["Login"]["LoginID"] = "登入名稱";
	$Lang['PowerClass']["Login"]["LoginID_alert"] = "請輸入登入名稱";
	$Lang['PowerClass']["Login"]["LoginID_err"] = "登入名稱/密碼無效";
	$Lang['PowerClass']["Login"]["Password"] = "密碼";
	$Lang['PowerClass']["Login"]["Password_alert"] = "請輸入密碼";
	$Lang['PowerClass']["Login"]["Password_err"]= "密碼錯誤";
	$Lang['PowerClass']['Login']['ForgotPassword'] = "忘記密碼?";
	
	$Lang['PowerClass']["Login"]["ForgotPasswordMsg"] = '1. 請輸入登入名稱，重啟密碼程序將寄送至閣下備用電郵。\n2. 如無法取得密碼，請聯絡所屬機構查詢。';
	$Lang['PowerClass']['Warnings']['DuplicatedName'] = "重複名稱。";
	$Lang['PowerClass']['Warnings']['DuplicatedCode'] = "重複代碼。";

	
	$Lang["PowerClass"]["cht_Today"] = "今天";
	$Lang["PowerClass"]["cht_Yesterday"] = "昨天";
	$Lang["PowerClass"]["cht_Week"] = "星期";
	$Lang["PowerClass"]["cht_Month"] = "月份";
	$Lang["PowerClass"]["cht_loginrecord"] = "用戶登入紀錄";
	$Lang["PowerClass"]["cht_AccessLog"] = "用戶登入紀錄";
	
	$Lang["PoserClass"]["BtnBack"] = "返回";
	
	$Lang["PowerClass"]["School"] = "學校";
	$Lang["PowerClass"]["SchoolYearAndTerm"] = "學年  / 學期";
	$Lang["PowerClass"]["Class"] = "班別";
	$Lang["PowerClass"]["Subject"] = "學科";
	$Lang["PowerClass"]["Group"] = "小組";
	$Lang["PowerClass"]["Role"] = "身份角色";
	$Lang["PowerClass"]["HolidayAndEvents"] = "假期 / 事項";
	$Lang["PowerClass"]["SchoolCalendar"] = "校曆表";
	
	$Lang["PowerClass"]["Account"] = "用戶管理";
	$Lang["PowerClass"]["eClass"] = "網上教室";
	$Lang["PowerClass"]["eClassroom"] = "eClassroom";
	
	$Lang["PowerClass"]["SchoolNews"] = "校園最新消息";
	$Lang["PowerClass"]["SchoolNews_BasicSettings"] = "基本設定";
	$Lang["PowerClass"]["eNotice"] = "電子通告系統";
	$Lang["PowerClass"]["eNotice_BasicSettings"] = "基本設定";
	$Lang["PowerClass"]["eNotice_PushMessageTemplate"] = "推送訊息範本 (App)";
    $Lang["PowerClass"]["eCircular_BasicSettings"] = "基本設定 (職員通告)";
    $Lang["PowerClass"]["eCircular_AdminSettings"] = "管理權限設定 (職員通告)";
	$Lang["PowerClass"]["eHomework"] = "網上家課表";
	$Lang["PowerClass"]["eHomework_BasicSettings"] = "基本設定";
	$Lang["PowerClass"]["eHomework_ViewerGroup"] = "檢視人員組別";
	$Lang["PowerClass"]["MessageCenter"] = "通訊中心";
	$Lang["PowerClass"]["MessageCenter_MessageTemplate"] = "訊息範本";
	$Lang["PowerClass"]["ParentApp"] = "家長 App";
	$Lang["PowerClass"]["ParentApp_FunctionAccessRight"] = "功能權限";
	$Lang["PowerClass"]["ParentApp_SchoolImage"] = "學校圖像";
	$Lang["PowerClass"]["ParentApp_LoginStatus"] = "登入狀況";
	$Lang["PowerClass"]["ParentApp_Blacklist"] = "黑名單";
	$Lang["PowerClass"]["ParentApp_PushMessageRight"] = "推送訊息權限";
	$Lang["PowerClass"]["StudentApp"] = "學生 App";
	$Lang["PowerClass"]["StudentApp_FunctionAccessRight"] = "功能權限";
	$Lang["PowerClass"]["StudentApp_LoginStatus"] = "登入狀況";
	$Lang["PowerClass"]["StudentApp_SchoolImage"] = "學校圖像";
	$Lang["PowerClass"]["StudentApp_Account"] = "用戶";
	$Lang["PowerClass"]["StudentApp_OfficalPhoto"] = "學生相片";
	$Lang["PowerClass"]["TeacherApp"] = "教職員  App";
	$Lang["PowerClass"]["TeacherApp_FunctionAccessRight"] = "功能權限";
	$Lang["PowerClass"]["TeacherApp_SchoolImage"] = "學校圖像";
	$Lang["PowerClass"]["TeacherApp_LoginStatus"] = "登入狀況";
	$Lang["PowerClass"]["TeacherApp_Blacklist"] = "黑名單";
	$Lang["PowerClass"]["TeacherApp_PushMessageRight"] = "推送訊息權限";
	$Lang["PowerClass"]["TeacherApp_StaffList"] = "教職員目錄";
	$Lang["PowerClass"]["TeacherApp_StudentList"] = "學生目錄";

	
	$Lang["PowerClass"]["NumberOfAccess"] = "訪問次數";

	$Lang["PowerClass"]["App_GroupMessage"] = "小組訊息";
	$Lang["PowerClass"]["App_SchoolInfo"] = "學校資訊";
	$Lang["PowerClass"]["SchoolSecurity"] = "系統保安";
	
	$Lang['General']['SystemProperties'] = "系統特性";
	$Lang['eBooking']['Settings']['FieldTitle']['GeneralPermission'] = "預訂特性";
	$Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'] = "批核小組";
	$Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'] = "跟進小組";
	$Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule'] = "用戶權限";
	$Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod'] = "預訂時段";
	$Lang['eBooking']['Settings']['FieldTitle']['Category'] = "資源類別";
	
	$Lang['PowerClass']['StudentAttendance_TimeSlotSettings'] = "時間設定";
	$Lang['PowerClass']['TakeAttendance'] = "點名";
	$Lang['PowerClass']['TakeAttendance_PresetAbsence_New'] = "學生請假紀錄 - 新增";
	$Lang['PowerClass']['TakeAttendance_PresetAbsence_BrowseByStudent'] = "學生請假紀錄 - 檢視指定學生";
	$Lang['PowerClass']['TakeAttendance_PresetAbsence_BrowseByDate'] = "學生請假紀錄 - 檢視指定日期";
	
	$Lang['PowerClass']['GoogleApps_More'] = '更多';
	$Lang["PowerClass"]["TeacherNotice"] = "教師  (職員通告)";
	
	$Lang['PowerClass']['eInventory']['Category'] = "類別";
	$Lang['PowerClass']['eInventory']['Caretaker'] = "物品管理小組";
	$Lang['PowerClass']['eInventory']['FundingSource'] = "資金來源";
	$Lang['PowerClass']['eInventory']['WriteOffReason'] = "報銷原因";
	$Lang['PowerClass']['eInventory']['General_Settings'] = "系統屬性";
	
	$Lang['PowerClass']['ePayment']['Settings_TerminalIP'] = "智能咭終端機設定";
	$Lang['PowerClass']['ePayment']['Settings_TerminalAccount'] = "終端機登入戶口";
	$Lang['PowerClass']['ePayment']['Settings_PaymentCategory'] = "繳費類別設定";
	$Lang['PowerClass']['ePayment']['Settings_Letter'] = "繳費通知書設定";
	$Lang['PowerClass']['ePayment']['PPS_Setting'] = "繳費靈設定";
	
	$Lang['PowerClass']['GoWith']['Parent'] = "家長用戶請使用 APP來繼續操作";
	$Lang['PowerClass']['GoWith']['Student'] = "學生用戶請使用 APP來繼續操作";
}
if($sys_custom['Project_Use_Sub']){
	if(file_exists($intranet_root.'/lang/' . $sys_custom['Project_Sub_Label']. '/' . basename(__FILE__))){
		include($intranet_root.'/lang/' . $sys_custom['Project_Sub_Label']. '/' . basename(__FILE__));
	}
}
?>