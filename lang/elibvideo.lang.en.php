<?php

	$Lang['Header']['en'] = "ENG";
	$Lang['Header']['b5'] = "繁";
	
	$Lang['Header']['Categories'] = "Categories";
	$Lang['Header']['Location'] = "Location";
	$Lang['Header']['MyFavourite'] = "My Favourite";
	$Lang['Header']['AdvancedSearch'] = "Advanced Search";
	
	$Lang['Common']['NoRecordsFound'] = "No Records Found";
	$Lang['Common']['Views'] = "";
	$Lang['Common']['ViewsEnd'] = " view(s)";	
	$Lang['Common']['Records'] = "Record(s)";
	$Lang['Common']['TermAndConditions'] = "Terms &amp; Condition";	
	
	
	$Lang['FrontPage']['SortBy'] = "Sort by";
	$Lang['FrontPage']['LatestUploaded'] = "Newest";
	$Lang['FrontPage']['MostViewed'] = "Most Viewed";
	$Lang['FrontPage']['All'] = "All";
	$Lang['FrontPage']['Total'] = "Total";
	$Lang['FrontPage']['Page'] = "Page";
	$Lang['FrontPage']['PageEnd'] = "";
	$Lang['FrontPage']['ViewBy'] = "View By:";	
	$Lang['FrontPage']['Display'] = "Display";
	$Lang['FrontPage']['DisplayEnd'] = "/ Page";
	
	
	$Lang['VideoPage']['Previous'] = "Previous";
	$Lang['VideoPage']['Next'] = "Next";
	$Lang['VideoPage']['Summary'] = "Summary";
	$Lang['VideoPage']['Script'] = "Script";	
	$Lang['VideoPage']['Tag'] = "Tag";
	$Lang['VideoPage']['AddToFavourite'] = "Add To Favourite";
	$Lang['VideoPage']['RemoveBookmark'] = "Remove Bookmark";
	
	$Lang['SearchPage']['AdvancedSearch'] = "Advanced Search";
	$Lang['SearchPage']['Keywords'] = "Keywords";
	$Lang['SearchPage']['Category'] = "Category";
	$Lang['SearchPage']['Location'] = "Location";
	$Lang['SearchPage']['Select'] = "Select";
	$Lang['SearchPage']['Search'] = "Search";
	$Lang['SearchPage']['Reset'] = "Reset";
	$Lang['SearchPage']['Found'] = "Found";
	$Lang['SearchPage']['Total'] = "Total";
	$Lang['SearchPage']['Page'] = "Page";
	$Lang['FavouritePage']['PageEnd'] = "";
	
	$Lang['FavouritePage']['RemoveAll'] = "Remove All";
	$Lang['FavouritePage']['Total'] = "Total";
	$Lang['FavouritePage']['Page'] = "Page";
	$Lang['FavouritePage']['PageEnd'] = "";
	$Lang['FavouritePage']['RemovefromFavourite'] = "Remove from Favourite";
	
	$Lang['Message']['BrowserChecking'] = "Your browser is not FULLY supported by eLibrary video platform. Google Chrome or Internet Explorer 10 are recommended.";
?>