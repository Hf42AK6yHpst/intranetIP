<?php 
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/

// left menu
$Lang['eSchoolBus']['ManagementArr']['MenuTitle'] = 'Management';
$Lang['eSchoolBus']['ManagementArr']['AttendanceArr']['MenuTitle'] = 'Attendance';
$Lang['eSchoolBus']['ManagementArr']['SpecialArrangementArr']['MenuTitle'] = 'Special Arragement';
$Lang['eSchoolBus']['ReportArr']['MenuTitle'] = 'Reports';
$Lang['eSchoolBus']['ReportArr']['ClassListArr']['MenuTitle'] = 'List by Class';
$Lang['eSchoolBus']['ReportArr']['RouteArrangementArr']['MenuTitle'] = 'Route Arrangement';
$Lang['eSchoolBus']['ReportArr']['StudentListArr']['MenuTitle'] = 'Student List';
$Lang['eSchoolBus']['ReportArr']['AsaContactArr']['MenuTitle'] = 'ASA Contact';
$Lang['eSchoolBus']['ReportArr']['AsaRouteArr']['MenuTitle'] = 'ASA Route';
$Lang['eSchoolBus']['ReportArr']['PickUpArr']['MenuTitle'] = 'Pick-up Report';
$Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['MenuTitle'] = 'List by Bus (specific date)';
$Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['NotTakeBus'] = 'List by Bus (Not taking at specific date)';
$Lang['eSchoolBus']['ReportArr']['StudentListByClassArr']['MenuTitle'] = 'List by Class (specific date)';
$Lang['eSchoolBus']['SettingsArr']['MenuTitle'] = 'Settings';
$Lang['eSchoolBus']['SettingsArr']['BusStopsArr']['MenuTitle'] = 'Bus Stops';
$Lang['eSchoolBus']['SettingsArr']['RouteArr']['MenuTitle'] = 'Route';
$Lang['eSchoolBus']['SettingsArr']['VehicleArr']['MenuTitle'] = 'Vehicle';
$Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'] = 'Students';
$Lang['eSchoolBus']['SettingsArr']['CutoffTimeArr']['MenuTitle'] = 'Apply Leave';
$Lang['eSchoolBus']['ViewArr']['MenuTitle'] = 'eSchoolBus';
$Lang['eSchoolBus']['ViewArr']['Student']['MenuTitle'] = 'School bus schedule';


$Lang['eSchoolBus']['ActivityArr']['ASA'] = 'ASA';
$Lang['eSchoolBus']['ActivityArr']['FootballClass'] = 'Football class';
$Lang['eSchoolBus']['ActivityArr']['SwimmingClass'] = 'Swimming class';
$Lang['eSchoolBus']['ActivityArr']['Orchestra'] = 'Orchestra';
$Lang['eSchoolBus']['ActivityArr']['MUN'] = 'MUN';
$Lang['eSchoolBus']['ActivityArr']['Volleyball'] = 'Volleyball';

$Lang['eSchoolBus']['AttendanceStatusArr']['OnTime']='On time';
$Lang['eSchoolBus']['AttendanceStatusArr']['Late']='Late';
$Lang['eSchoolBus']['AttendanceStatusArr']['Absent']='Absent';
$Lang['eSchoolBus']['AttendanceStatusArr']['Activity']='Leave for activity';
$Lang['eSchoolBus']['AttendanceStatusArr']['Parent']='Pick by parents';

//General
$Lang['eSchoolBus']['General']['From'] = 'From';
$Lang['eSchoolBus']['General']['To'] = 'To';

//Management
$Lang['eSchoolBus']['Management']['SpecialArrangement']['SpecialArrangementByDates'] = 'Special arrangement by dates';
$Lang['eSchoolBus']['Management']['SpecialArrangement']['FollowPreviousArrangementForNextRecord'] = 'Will follow the previous arrangement to add the next record.';
$Lang['eSchoolBus']['Management']['SpecialArrangement']['UnableToSetSpecialArrangements'] = 'Unable to set special arrangements because this student does not have normal arrangements.';
$Lang['eSchoolBus']['Management']['Attendance']['NoStudents'] = 'No related students';
$Lang['eSchoolBus']['Management']['Attendance']['SpecialArrangement'] = 'This student has special arrangement';
$Lang['eSchoolBus']['Management']['Attendance']['SpecialArrangement2'] = 'Special Arrangement';
$Lang['eSchoolBus']['Management']['Attendance']['HaveASA'] = 'Take ASA Route';

$Lang['eSchoolBus']['Management']['Attendance']['Date'] = 'Date';
$Lang['eSchoolBus']['Management']['Attendance']['Route'] = 'Route';
$Lang['eSchoolBus']['Management']['Attendance']['AmPm'] = 'AM / PM';
$Lang['eSchoolBus']['Management']['Attendance']['TimeAndStopName'] = 'Time / Bus Stops';
$Lang['eSchoolBus']['Management']['Attendance']['StudentName'] = 'Student Name';
$Lang['eSchoolBus']['Management']['Attendance']['Status'] = 'Status';
$Lang['eSchoolBus']['Management']['Attendance']['Remarks'] = 'Remarks';
$Lang['eSchoolBus']['Management']['Attendance']['ContactNumber'] = 'Contact Number';

//Settings
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameEng'] = 'Bus stop name (Eng)';
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameChi'] = 'Bus stop name (Chi)';
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopName'] = 'Bus stop';

$Lang['eSchoolBus']['Settings']['Vehicle']['CarNumber'] = 'Car number';
$Lang['eSchoolBus']['Settings']['Vehicle']['CarPlateNumber'] = 'Car plate number';
$Lang['eSchoolBus']['Settings']['Vehicle']['Driver'] = 'Driver';
$Lang['eSchoolBus']['Settings']['Vehicle']['DriverName'] = 'Name';
$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'] = 'Phone number';
$Lang['eSchoolBus']['Settings']['Vehicle']['NumberOfSeat'] = 'No. of seat';
$Lang['eSchoolBus']['Settings']['Vehicle']['Status'] = 'Status';
$Lang['eSchoolBus']['Settings']['Vehicle']['Active'] = 'Active';
$Lang['eSchoolBus']['Settings']['Vehicle']['Inactive'] = 'Inactive';

$Lang['eSchoolBus']['Settings']['Route']['RouteName']='Route Number';
$Lang['eSchoolBus']['Settings']['Route']['Remarks']='Remarks';
$Lang['eSchoolBus']['Settings']['Route']['Type']='Route Type';
$Lang['eSchoolBus']['Settings']['Route']['NormalRoute']='Normal Route';
$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute']='ASA Route';
$Lang['eSchoolBus']['Settings']['Route']['BusStop']='Bus Stop';
$Lang['eSchoolBus']['Settings']['Route']['AmTime']='To school';
$Lang['eSchoolBus']['Settings']['Route']['Am']='To school';
$Lang['eSchoolBus']['Settings']['Route']['PmTime']='From school';
$Lang['eSchoolBus']['Settings']['Route']['Pm']='From school';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][1]='Monday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][2]='Tuesday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][3]='Wednesday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][4]='Thursday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][5]='Friday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['MON']='Monday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['TUE']='Tuesday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['WED']='Wednesday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['THU']='Thursday';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['FRI']='Friday';
$Lang['eSchoolBus']['Settings']['Route']['Dates']='Date';
$Lang['eSchoolBus']['Settings']['Route']['StartDate']='Start Date';
$Lang['eSchoolBus']['Settings']['Route']['EndDates']='End Date';
$Lang['eSchoolBus']['Settings']['Route']['IsActive']='Status';
$Lang['eSchoolBus']['Settings']['Route']['Status']['Active']='Active';
$Lang['eSchoolBus']['Settings']['Route']['Status']['Inactive']='Inactive';
$Lang['eSchoolBus']['Settings']['Route']['NoOfStops']='No. of stops';
$Lang['eSchoolBus']['Settings']['Route']['TimeScheduled']='Time Scheduled';
$Lang['eSchoolBus']['Settings']['Route']['ValidDates']='Valid dates';
$Lang['eSchoolBus']['Settings']['Route']['VehicleNum']='Vehicle num';
$Lang['eSchoolBus']['Settings']['Route']['LastModified']='Last modified';
$Lang['eSchoolBus']['Settings']['Route']['Teacher'] = 'Teacher';
$Lang['eSchoolBus']['Settings']['Route']['ArriveOrLeaveTime']='Arrival/Departure time';
$Lang['eSchoolBus']['Settings']['Route']['LeaveTime']='Departure time';
$Lang['eSchoolBus']['Settings']['Route']['AddTeacher']='Add Teacher';
$Lang['eSchoolBus']['Settings']['Route']['SelectTeacher']='Select Teacher';
$Lang['eSchoolBus']['Settings']['Route']['CtrlMultiSelectMessage'] = "Use CTRL to select multiple items";

$Lang['eSchoolBus']['Settings']['Student']['AM'] = 'To school';
$Lang['eSchoolBus']['Settings']['Student']['PM'] = 'From school';
$Lang['eSchoolBus']['Settings']['Student']['BusStop'] = 'Bus stop';
$Lang['eSchoolBus']['Settings']['Student']['AMBus'] = 'To school';
$Lang['eSchoolBus']['Settings']['Student']['PMBus'] = 'From school';
$Lang['eSchoolBus']['Settings']['Student']['ByParent'] = 'Pick by parent';
$Lang['eSchoolBus']['Settings']['Student']['NotTake'] = 'Not take';
$Lang['eSchoolBus']['Settings']['Student']['Activity'] = 'Activity';
$Lang['eSchoolBus']['Settings']['Student']['SelectActivity'] = 'Select activity';
$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'] = 'Select route';
$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'] = 'Select bus stop';
$Lang['eSchoolBus']['Settings']['Student']['LeaveBlank'] = 'Leave blank';
$Lang['eSchoolBus']['Settings']['Student']['RowNumber'] = 'Row no.';
$Lang['eSchoolBus']['Settings']['Student']['Errors'] = 'Errors';
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'] = array();
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Login ID', 'Student login id (please lookup at student account management).','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('AM Normal Route','Normal route for To school (leave blank if does not take bus).','get_route_N_AM');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('AM Normal Route Bus Stop','Normal route bus stop for To school.','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('PM Normal Route','Normal route for From school (leave blank if does not take bus).','get_route_N_PM');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('PM Normal Route Bus Stop','Normal route bus stop for From school.','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route Activity','ASA activity on Monday (leave blank if no activity).','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route','ASA route on Monday.','get_route_S_MON');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route Bus Stop','ASA route bus stop on Monday.','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route Activity','ASA activity on Tueday (leave blank if no activity).','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route','ASA route on Tueday.','get_route_S_TUE');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route Bus Stop','ASA route bus stop on Tueday.','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route Activity','ASA activity on Wednesday (leave blank if no activity).','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route','ASA route on Wednesday.','get_route_S_WED');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route Bus Stop','ASA route bus stop on Wednesday.','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route Activity','ASA activity on Thursday (leave blank if no activity).','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route','ASA route on Thursday.','get_route_S_THU');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route Bus Stop','ASA route bus stop on Thursday.','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route Activity','ASA activity on Friday (leave blank if no activity).','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route','ASA route on Friday.','get_route_S_FRI');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route Bus Stop','ASA route bus stop on Friday.','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Start Date','Effective start date (with format yyyy-mm-dd)','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('End Date','Effective end date (with format yyyy-mm-dd)','');

$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound'] = '<!--TARGET--> cannot be found.';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['RouteIsNotSet'] = '<!--TARGET--> route is not set.';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['BusStopIsNotSet'] = '<!--TARGET--> bus stop is not set.';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidStartDate'] = 'Invalid start date.';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidEndDate'] = 'Invalid end date.';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['ExceedBusSeat'] = 'The bus is full in the selected route:';

$Lang['eSchoolBus']['Settings']['ApplyLeave']['ApplyLeaveCutOffTiming'] = 'Set cut-off time';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['GotoSchool'] = 'To school';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['LeaveSchool'] = 'From school';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['DaysBeforeApplyLeave'] = 'Number of days required before applying a leave';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup'] = "Class Group";

//eService
$Lang['eSchoolBus']['eService']['View']['Student'] = 'Bus Schedule for this month';
$Lang['eSchoolBus']['eService']['StudentView']['Time'] = 'Time';
$Lang['eSchoolBus']['eService']['StudentView']['Class'] = 'Class';
$Lang['eSchoolBus']['eService']['StudentView']['Date'] = 'Date';
$Lang['eSchoolBus']['eService']['StudentView']['Year'] = 'Year';
$Lang['eSchoolBus']['eService']['StudentView']['Month'] = 'Month';

//Warning
$Lang['eSchoolBus']['Managment']['Attendance']['Warning']['WeekendNotAvailable'] = 'Weekend is not available';
$Lang['eSchoolBus']['Managment']['Attendance']['Warning']['SundayNotAvailable'] = 'Sunday is not available';

$Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseAtLeastOne']='Please choose at least one bus stop';
$Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseOnlyOne']='Please choose a bus stop';

$Lang['eSchoolBus']['Settings']['Vehicle']['Warning']['DuplicatedCarNum'] = 'Same car number already exists.';
$Lang['eSchoolBus']['Settings']['Vehicle']['Warning']['DuplicatedCarPlateNum'] = 'Same car plate number is being used by another car.';

$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'] = 'The bus is full in the selected route:';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRoute'] = 'Please select route.';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'] = 'Please select bus stop.';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop'] = 'Please select route and bus stop.';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['StudentHasBeenSet'] = 'The student has already setup bus schedule in this academic year, please modify the record instead.';

$Lang['eSchoolBus']['Settings']['Warning']['ConfirmDelete']='Confirm to delete?';
$Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty']='This field cannot be empty';
$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'] = 'Please input this data.';
$Lang['eSchoolBus']['Settings']['Warning']['DuplicatedDate'] = 'Duplicated date.';
$Lang['eSchoolBus']['Settings']['Warning']['Duplicate'] = 'Item Name is duplicated';

//Report
$Lang['eSchoolBus']['Report']['ClassStudentList']['StudentNameList'] = 'Student name list  學生名單';
$Lang['eSchoolBus']['Report']['ClassStudentList']['RegularDays'] = 'Regular days';
$Lang['eSchoolBus']['Report']['ClassStudentList']['People'] = '';
$Lang['eSchoolBus']['Report']['ClassStudentList']['RequestSelectForms'] = 'Please select form(s).';

$Lang['eSchoolBus']['Report']['RouteArrangement']['Period'] = 'Period';
$Lang['eSchoolBus']['Report']['RouteArrangement']['RouteType'] = 'Route type';
$Lang['eSchoolBus']['Report']['RouteArrangement']['ASAWeekday'] = 'ASA';
$Lang['eSchoolBus']['Report']['RouteArrangement']['SelectAll'] = 'Select All';
$Lang['eSchoolBus']['Report']['RouteArrangement']['NumberOfStudent'] = 'Number of students';
$Lang['eSchoolBus']['Report']['RouteArrangement']['Time'] = 'Time';

$Lang['eSchoolBus']['Report']['StudentList']['CarNum'] = 'Route';
$Lang['eSchoolBus']['Report']['StudentList']['BusStop'] = 'Bus stop';
$Lang['eSchoolBus']['Report']['StudentList']['NumberOfStudent'] = 'No. of Students';
$Lang['eSchoolBus']['Report']['StudentList']['StudentName'] = 'Students';

$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Bus'] = 'Bus';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['RequestSelectBus'] = 'Please select bus(es).';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Teacher'] = 'Bus teacher';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusRoute'] = 'Bus route';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusNumber'] = 'Bus No.';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Name'] = 'Name';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['FormClass'] = 'Form class';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['PickupAddress'] = 'Pickup address';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['ContactPhone'] = 'Contact phone number';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['EmailAddress'] = 'Email address';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['MON'] = 'Monday';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['TUE'] = 'Tuesday';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['WED'] = 'Wednesday';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['THU'] = 'Thursday';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['FRI'] = 'Firday';

$Lang['eSchoolBus']['Report']['Pickup']['SchoolBusList'] = 'BIBA School Bus List 海嘉学校校车接送表';
$Lang['eSchoolBus']['Report']['Pickup']['GeneralSchoolBusList'] = 'School Bus List 校車接送表';
$Lang['eSchoolBus']['Report']['Pickup']['BusNumber'] = 'Bus No. / 班車號';
$Lang['eSchoolBus']['Report']['Pickup']['Route'] = 'Route / 路綫';
$Lang['eSchoolBus']['Report']['Pickup']['BusDriver'] = 'Bus Driver / 校車司機';
$Lang['eSchoolBus']['Report']['Pickup']['BusTeacher'] = 'Bus Teacher / 校車老師';
$Lang['eSchoolBus']['Report']['Pickup']['Date'] = '日期 / Date';
$Lang['eSchoolBus']['Report']['Pickup']['Year'] = '年';
$Lang['eSchoolBus']['Report']['Pickup']['Month'] = '月';
$Lang['eSchoolBus']['Report']['Pickup']['Day'] = '日';
$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['en'] = 'No.';
$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['ch'] = '序';
$Lang['eSchoolBus']['Report']['Pickup']['Name']['en'] = 'Name';
$Lang['eSchoolBus']['Report']['Pickup']['Name']['ch'] = '姓名';
$Lang['eSchoolBus']['Report']['Pickup']['Class']['en'] = 'Class';
$Lang['eSchoolBus']['Report']['Pickup']['Class']['ch'] = '班級';
$Lang['eSchoolBus']['Report']['Pickup']['SignOutSignature']['en'] = 'Sign-out Signature';
$Lang['eSchoolBus']['Report']['Pickup']['SignOutSignature']['ch'] = '接送者簽字';
$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['en'] = 'Sign-out Signature';
$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['ch'] = '接送教師簽字';
$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['en'] = 'Pick-up Person';
$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['ch'] = '接送者簽字';
$Lang['eSchoolBus']['Report']['Pickup']['Remark']['en'] = 'Remark';
$Lang['eSchoolBus']['Report']['Pickup']['Remark']['ch'] = '備註';
$Lang['eSchoolBus']['Report']['Pickup']['ArrivedAtHome']['en'] = 'Arrived at home';
$Lang['eSchoolBus']['Report']['Pickup']['ArrivedAtHome']['ch'] = '到家時間';

$Lang['eSchoolBus']['Report']['Attendance']['TotalAbsent'] = 'Total absent';
$Lang['eSchoolBus']['Report']['Attendance']['TotalOnBus'] = 'Total on bus';
$Lang['eSchoolBus']['Report']['Bus'] = 'Bus';
$Lang['eSchoolBus']['Report']['ByClass']['All'] = 'All';
$Lang['eSchoolBus']['Report']['ByClass']['HasRoute'] = 'Has Route';
$Lang['eSchoolBus']['Report']['ByClass']['HasRouteOrNot'] = 'Has route / no route';
$Lang['eSchoolBus']['Report']['ByClass']['NoRoute'] = 'No Route';
$Lang['eSchoolBus']['Report']['ByClass']['NotTake'] = 'Not take';
$Lang['eSchoolBus']['Report']['ByClass']['RequestSelectClass'] = 'Please select class(es)';
$Lang['eSchoolBus']['Report']['ByClass']['Take'] = 'Take';
$Lang['eSchoolBus']['Report']['ByClass']['TakeOption'] = 'Take/Not take';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Name'] = 'Name';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['StudentNumber'] = 'Student no';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Route'] = 'Route';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Class'] = 'Class';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Campus'] = 'Campus';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['TimeSlot'] = 'To/From school';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['TakeOrNot'] = 'Take or not';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Remark'] = 'Teacher\'s Remark';
$Lang['eSchoolBus']['Report']['Campus'] = 'Campus';
$Lang['eSchoolBus']['Report']['IsHolidayWarning'] = 'Please re-select date as it is holiday';
$Lang['eSchoolBus']['Report']['SelectDate'] = 'Please select date';

# App
$Lang['eSchoolBus']['App']['ApplyLeave']['AllStatus'] = 'All Status';
$Lang['eSchoolBus']['App']['ApplyLeave']['Apply'] = 'Leave application for taking school bus';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplicaton'] = 'Are you sure to cancel this application?';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelAllApplyLeave'] = 'Cancel above leave application';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplyLeave'] = 'Cancel leave application';
$Lang['eSchoolBus']['App']['ApplyLeave']['ConfirmSubmit'] = 'Are you sure to submit this application?';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Absent'] = 'Absent';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Cancelled'] = 'Cancelled';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Confirmed'] = 'Accepted';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Deleted'] = 'Deleted';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Rejected'] = 'Rejected';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Waiting'] = 'Waiting Approval';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddSuccess'] = 'Record added';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddUnSuccess'] = 'Fail to add record';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['ApproveSuccess'] = 'Record has been accepted';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['ApproveUnsuccess'] = 'Fail to accept record';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelSuccess'] = 'Record cancelled';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelUnsuccess'] = 'Fail to cancel record';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['InvalidTimeSlot'] = 'Invalid time slot';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingApplicationID'] = 'ApplicationID is missing';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingStudentID'] = 'StudentID is missing';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['NotAllowedApplyLeave'] = 'Not allow applying leave';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['RejectSuccess'] = 'Record rejected';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['RejectUnsuccess'] = 'Fail to reject record';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateSuccess'] = 'Record Updated';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateUnsuccess'] = 'Fail to update record';
$Lang['eSchoolBus']['App']['ApplyLeave']['Reason'] = 'Reason';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Absent'] = 'Absent(auto created)';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Approved'] = 'Accepted';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Cancelled'] = 'Cancelled';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Deleted'] = 'Deleted(auto created)';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Pending'] = 'Waiting Approval';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Rejected'] = 'Rejected';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlot'] = 'Time Slot';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'] = 'To School';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'] = 'From School';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'] = 'Full Date';
$Lang['eSchoolBus']['App']['ApplyLeave']['Title']= 'Apply Leave';
$Lang['eSchoolBus']['App']['Date'] = 'Date';
$Lang['eSchoolBus']['App']['Error']['Ajax'] = 'There is error when get data from database, please retry!';
$Lang['eSchoolBus']['App']['Month'][1] = 'JAN';
$Lang['eSchoolBus']['App']['Month'][2] = 'FEB';
$Lang['eSchoolBus']['App']['Month'][3] = 'MAR';
$Lang['eSchoolBus']['App']['Month'][4] = 'APR';
$Lang['eSchoolBus']['App']['Month'][5] = 'MAY';
$Lang['eSchoolBus']['App']['Month'][6] = 'JUN';
$Lang['eSchoolBus']['App']['Month'][7] = 'JUL';
$Lang['eSchoolBus']['App']['Month'][8] = 'AUG';
$Lang['eSchoolBus']['App']['Month'][9] = 'SEP';
$Lang['eSchoolBus']['App']['Month'][10] = 'OCT';
$Lang['eSchoolBus']['App']['Month'][11] = 'NOV';
$Lang['eSchoolBus']['App']['Month'][12] = 'DEC';
$Lang['eSchoolBus']['App']['NewRecord'] = 'New Record';
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['ApproveApplicationContent'] = "校方已接納閣下於[ApplyDate]提交貴子弟[StudentChineseName]的乘車請假申請。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been accepted.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['ApproveApplicationTitle'] = "接納乘車請假申請通知
Leave Application (not taking school bus) Accepted Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToTeacher']['ApplyLeaveContent'] = "現收到家長於[ApplyDate]提交學生[StudentChineseName]的乘車請假申請。
Please note that the leave application of not taking school bus for [StudentEnglishName] submitted by parent on [ApplyDate] has been received.";
$Lang['eSchoolBus']['App']['PushMessage']['ToTeacher']['ApplyLeaveTitle'] = "乘車請假申請通知
Leave Application (not taking school bus) Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['CancelApplicationContent'] = "校方已取消閣下於[ApplyDate]提交貴子弟[StudentChineseName]的乘車請假申請。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been cancelled.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['CancelApplicationTitle'] = "取消乘車請假申請通知
Leave Application (not taking school bus) Cancelled Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['RejectApplicationContent'] = "校方未能接納閣下於[ApplyDate]提交貴子弟[StudentChineseName]的乘車請假申請。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been rejected.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['RejectApplicationTitle'] = "乘車請假申請不獲接納通知
Leave Application (not taking school bus) Rejected Alert";
$Lang['eSchoolBus']['App']['RequiredField']= "<span class='alertText'> * </span> Mandatory field(s)";
$Lang['eSchoolBus']['App']['Schedule']['Absent'] = 'Absent(auto created)';
$Lang['eSchoolBus']['App']['Schedule']['AppliedLeave'] = 'Applied Leave';
$Lang['eSchoolBus']['App']['Schedule']['NA'] = 'N.A.';
$Lang['eSchoolBus']['App']['Schedule']['Pending'] = 'Waiting Approval';
$Lang['eSchoolBus']['App']['Schedule']['Rejected'] = 'Application Rejected';
$Lang['eSchoolBus']['App']['Tab']['ApplyLeave'] = 'Apply Leave';
$Lang['eSchoolBus']['App']['Tab']['Schedule'] = 'Schedule';
$Lang['eSchoolBus']['App']['Warning']['AllowFutureDatesOnly'] = 'Leave date should be on or after today';
$Lang['eSchoolBus']['App']['Warning']['DuplicateApplication'] = 'Not allow duplicate application';
$Lang['eSchoolBus']['App']['Warning']['InputEndDate'] = 'Please input end date';
$Lang['eSchoolBus']['App']['Warning']['InputLeaveReason'] = 'Please input leave reason';
$Lang['eSchoolBus']['App']['Warning']['InputStartDate'] = 'Please input start date';
$Lang['eSchoolBus']['App']['Warning']['PassedCutoffDate'] = 'Leave application (not taking school bus) must be applied before %s day(s), please contact school.';
$Lang['eSchoolBus']['App']['Warning']['PassedCutoffTime'] = 'Leave application (not taking school bus) must be applied before %s, please contact school.';
$Lang['eSchoolBus']['App']['Warning']['SelectStudent'] = 'Please select student';
$Lang['eSchoolBus']['App']['Warning']['StartDateIsLaterThanEndDate'] = 'Start date cannot be latter than end date';
$Lang['eSchoolBus']['App']['Warning']['StartDateTimeSlotIsLaterThanEndDateTimeSlot'] = 'Start date time slot cannot be latter than that of end date';

$Lang['eSchoolBus']['TeacherApp']['AllStudent'] = 'All Students';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['Add'] = 'Add new leave application';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmApproval'] = 'Are you sure to approve this application?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmCancel'] = 'Are you sure to cancel this application?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmReject'] = 'Are you sure to reject this application?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmSubmit'] = 'Are you sure to submit this record?';
$Lang['eSchoolBus']['TeacherApp']['Remark']='Teacher\'s Remark';
$Lang['eSchoolBus']['TeacherApp']['Tab']['Handled'] = 'Handled';
$Lang['eSchoolBus']['TeacherApp']['Tab']['Pending'] = 'Pending';
?>