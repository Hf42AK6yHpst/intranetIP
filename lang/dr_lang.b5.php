<?php
// using: 
/*
 * Remarks:
 * 1) Arrange the lang variables in alphabetical order
 * 2) Keep the same line break between en and b5 lang files (so that same lang variable is in the same line in different lang files)
 * 3) Use single quote for the array keys and double quote for the lang value
 * 
 * Modification Log:
 * 
 * Date:	2019-11-15 (Sam)
 * 			Added labels for Delete Existing Routing
 */
 
// A
$Lang['DocRouting']['Action'] = "行動";
$Lang['DocRouting']['Actions']['ATTACHMENT'] = "允許加入附件";
$Lang['DocRouting']['Actions']['COMMENT'] = "允許留下意見或評論"; 
$Lang['DocRouting']['Actions']['ONLINEFORM'] = "提供網上回條(若設置此項，則用戶必須填寫網上回條)";
$Lang['DocRouting']['Actions']['SIGN'] = "要求簽署及確認 ";
$Lang['DocRouting']['AddAdHocRoute'] = "新增特設傳遞";
$Lang['DocRouting']['AddMoreFollowUp'] = "新增下一個傳遞";
$Lang['DocRouting']['ArchiveRoutings'] = "文件傳遞存檔";
$Lang['DocRouting']['AddMoreFile'] = "新增更多附件";
$Lang['DocRouting']['AllowAdHocRoute'] = "允許參與者新建傳遞";
$Lang['DocRouting']['Active'] = "已發佈";
$Lang['DocRouting']['Attachment'] = "附件";
$Lang['DocRouting']['AddAttachment'] = "新增附件";
$Lang['DocRouting']['AddVoice'] = "新增錄音";
$Lang['DocRouting']['AddAdHocRouting'] = "新增特別傳遞";
$Lang['DocRouting']['ActivateDocument'] = "重新開放文件";
$Lang['DocRouting']['ArchiveFilesToDigitalArchive'] = "傳送文件到 [ ".$Lang['Header']['Menu']['DigitalArchive']." ]";
$Lang['DocRouting']['Advanced'] = "進階";
$Lang['DocRouting']['AttachmentRemark'] = ' * 注意：若有人已跟進了（簽了），此附件將不能被修改。';
$Lang['DocRouting']['ActionAndTime'] = "行動 / 時間";
$Lang['DocRouting']['Attached'] = "上載附件";
$Lang['DocRouting']['ArchiveDocument'] = "整存文件";
$Lang['DocRouting']['AuthenticationSetting'] = "首次進入時需要進行登入認證";
$Lang['DocRouting']['AllowAccessIP'] = "指定可存取電腦 IP";

// B
$Lang['DocRouting']['Button']['Back'] = "返回";
$Lang['DocRouting']['Button']['CopyExistingRounting'] = "複製現有傳遞";
$Lang['DocRouting']['Button']['CopyFromExistingRounting'] = "複製自現有傳遞";
$Lang['DocRouting']['Button']['DeleteExistingRounting'] = "刪除現有傳遞";

// C
$Lang['DocRouting']['CompletedRoutings'] = "已完成文件傳遞";
$Lang['DocRouting']['CurrentRoutings'] = "現有文件傳遞 ";
$Lang['DocRouting']['CompleteRouting'] = "完成文件";
$Lang['DocRouting']['Creator'] = "建立者";
$Lang['DocRouting']['CopyStatus']['Yes'] = "是";
$Lang['DocRouting']['CopyStatus']['No'] = "否";
$Lang['DocRouting']['CheckingRoutingStatus'] = "正在檢查傳遞狀況...";
$Lang['DocRouting']['ChooseFile'] = "選擇文件";
$Lang['DocRouting']['CommentAndDocument'] = "意見或評論 / 文件";
$Lang['DocRouting']['Commented'] = "發表意見或評論";
$Lang['DocRouting']['CommentedAndAttached'] = "發表意見或評論 及 上載附件";
$Lang['DocRouting']['CanViewAllCommentsAndAttachments'] = "可以檢視所有評論及附件";

// D
$Lang['DocRouting']['Documents'] = "文件";
$Lang['DocRouting']['DocumentNA'] = "N/A";
$Lang['DocRouting']['DocumentPhysical'] = "來自實質地點";
$Lang['DocRouting']['DocumentUpload'] = "來自檔案上傳";
$Lang['DocRouting']['DocumentDA'] = "來自電子文件";
$Lang['DocRouting']['DraftRoutings'] = "文件傳遞草稿 ";
$Lang['DocRouting']['Draft'] = "草稿";
$Lang['DocRouting']['DownloadAll'] = "下載全部附件";
$Lang['DocRouting']['DraftCommentRemark'] = "草稿最後儲存於 <!--DATETIME-->。";
$Lang['DocRouting']['DeleteDocRoutings'] = "刪除傳遞";
$Lang['DocRouting']['Deleted'] = "已刪除";
$Lang['DocRouting']['DeletedRoutings'] = "已刪除的文件傳遞";
$Lang['DocRouting']['DeletedBy'] = "<!--USERNAME--> 刪除於 <!--DATETIME-->";

// E
$Lang['DocRouting']['EmailNotification'] = "電郵提示";
$Lang['DocRouting']['EmailNotification_newFeedbackInRoute']['EmailTitle'] = "[".$Lang['Header']['Menu']['DocRouting']."] \"<!--docTitle-->\" 有新回應";
$Lang['DocRouting']['EmailNotification_newFeedbackInRoute']['EmailContentAry'][] = "<!--userName--> 於 \"<!--docTitle-->\" 傳遞 \"<!--routeNum-->\" 有新回應。";
$Lang['DocRouting']['EmailNotification_newFeedbackInRoute']['EmailContentAry'][] = "閣下可前往 \"".$Lang['Header']['Menu']['eAdmin']." > ".$Lang['Header']['Menu']['ResourcesManagement']." > ".$Lang['Header']['Menu']['DocRouting']."\" 檢視最新回應。";
$Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailTitle'] = "[".$Lang['Header']['Menu']['DocRouting']."] 新傳遞 \"<!--docTitle-->\"";
$Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailContentAry'][] = "新傳遞 \"<!--docTitle-->\" 需要閣下跟進。";
$Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailContentAry'][] = "閣下可前往 \"".$Lang['Header']['Menu']['eAdmin']." > ".$Lang['Header']['Menu']['ResourcesManagement']." > ".$Lang['Header']['Menu']['DocRouting']."\" 檢視最新傳遞。";
$Lang['DocRouting']['EmailTarget'] = "傳送目標";
$Lang['DocRouting']['ExportCSV'] = "匯出成 CSV";
$Lang['DocRouting']['EditRouting'] = "編輯傳遞";
$Lang['DocRouting']['ExistingRoutings'] = "現有傳遞";
$Lang['DocRouting']['EndDate'] = "結束日期範圍";
$Lang['DocRouting']['EditRoutingPeriodAndSendEmail'] = "編輯傳遞時段及電郵傳送";
$Lang['DocRouting']['EmailContent'] = "電郵內容";
$Lang['DocRouting']['Email'] = "電郵";
$Lang['DocRouting']['EmailNotification_RouteSigned']['EmailTitle'] = "簽署文件傳遞確認通知 [<!--DocTitle-->]";
$Lang['DocRouting']['EmailNotification_RouteSigned']['EmailContent'] = "已收到閣下於 <!--SignedTime--> 簽署的文件傳遞 「<!--DocTitle-->」。";
$Lang['DocRouting']['EmailSendArchivedDocument']['EmailTitle'] = $Lang['Header']['Menu']['DocRouting']." - 已完成的文件 [<!--DocTitle-->]";
$Lang['DocRouting']['EmailSendArchivedDocument']['EmailContent'] = "文件 [<!--DocTitle-->] 已完成，具體內容請查看附件。";
$Lang['DocRouting']['EmailSendArchivedDocument']['EmailRemark'] = "備註: 由於文件太大，文件被分拆成<!--NUMBER_OF_FILE-->部分並分別發出電郵。你可以使用任何解壓縮軟件還原文件。";

// F
$Lang['DocRouting']['Files'] = "檔案";
$Lang['DocRouting']['File'] = "個檔案";
$Lang['DocRouting']['FollowedRoutings'] = "已跟進的文件傳遞";

// G


// H


// I
$Lang['DocRouting']['InProgress'] = "進行中";
$Lang['DocRouting']['Instruction'] = "指示";

// J


// K


// L
$Lang['DocRouting']['LockRoutingWhenGivingFeedback'] = "不允許多人同時編輯意見或評論";
$Lang['DocRouting']['LockFor'] = "最多封鎖";
$Lang['DocRouting']['LastSentTime'] = "最後發出時間";
$Lang['DocRouting']['LastFollowedTime'] = "最後跟進時間";

// M
$Lang['DocRouting']['Management']['BeforeSigningRemind'] ="在簽署文件前, 你可以";
//$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'] = "請檢閱以下文件，並在指定日期或之前完成寄件者的指示。紅色行為你所需跟進的項目。";
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'] = "請檢閱以下文件，並在指定日期或之前完成寄件者的指示。<br />";
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'].= '<span style="background-color:#F7D8D8;padding:2px;line-height:18px;">紅色─需要你回應的項目</span><br />';
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'].= '<span style="background-color:#FFDBC1;padding:2px;line-height:18px;">橙色─已到期的項目</span><br />';
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'].= '<span style="background-color:#FFFBCE;padding:2px;line-height:18px;">黃色─等待同事回應未到期的項目</span><br />';
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'].= '<span style="background-color:#E9F6E0;padding:2px;line-height:18px;">綠色─你已簽署或回應的項目</span>';
$Lang['DocRouting']['Management']['FillInReplySlip'] = "請填寫以下回條";
$Lang['DocRouting']['Management']['Publish'] = "發佈";
$Lang['DocRouting']['Management']['PublishDate'] = "建立日期:";
$Lang['DocRouting']['Management']['DraftDate'] = "草稿日期:";
//$Lang['DocRouting']['Management']['Status']['CreatedByYourself'] = "由你所建立";
//$Lang['DocRouting']['Management']['Status']['FollowedByYouNow'] = "現時由你跟進";
//$Lang['DocRouting']['Management']['Status']['RoutedByPeriod'] = "按時段傳遞";
//$Lang['DocRouting']['Management']['Status']['StarRemark'] = "已被標記";

$Lang['DocRouting']['Management']['WriteYourComment']="寫下意見或評論";
$Lang['DocRouting']['Management']['EditYourComment']="編輯意見或評論";


$Lang['DocRouting']['Management']['FilterStatus']['AllStarStatus'] ="所有星號狀況";
$Lang['DocRouting']['Management']['FilterStatus']['StarRemarked'] ="已被星號標記";
$Lang['DocRouting']['Management']['FilterStatus']['NotStarRemarked'] ="沒有被星號標記";
$Lang['DocRouting']['Management']['FilterStatus']['AllCreateStatus'] ="所有建立狀況";
$Lang['DocRouting']['Management']['FilterStatus']['CreatedByYou'] ="由你所建立";
$Lang['DocRouting']['Management']['FilterStatus']['NotCreatedByYou'] ="非由你所建立";
$Lang['DocRouting']['Management']['FilterStatus']['AllFollowUpStatus'] ="所有跟進狀況";
//$Lang['DocRouting']['Management']['FilterStatus']['FollowedByYouNow'] ="現時由你所跟進";
$Lang['DocRouting']['Management']['FilterStatus']['FollowedByYouNow'] ="你要跟進的項目〈包括自己建立及他人建立並需要回應〉";
$Lang['DocRouting']['Management']['FilterStatus']['NotFollowedByYouNow'] ="現時非由你所跟進";
$Lang['DocRouting']['Management']['FilterStatus']['AllExpireStatus'] = "所有過期狀況";
$Lang['DocRouting']['Management']['FilterStatus']['ExpiredButNotYetFinished'] = "已過期但未完成";

$Lang['DocRouting']['Minutes'] = "分鐘";

// N
$Lang['DocRouting']['NewDocRouting'] = "新傳遞";
$Lang['DocRouting']['NewDocRoutings'] = "新傳遞";
$Lang['DocRouting']['NewDocRoutingStep1'] = "填寫文件";
$Lang['DocRouting']['NewDocRoutingStep2'] = "設定傳遞";
$Lang['DocRouting']['NewFeedbackReceivedInOwnRouting'] = "於自己建立的傳遞中有新回應";
$Lang['DocRouting']['NewRouteReceived'] = "有新傳遞到達用戶";
$Lang['DocRouting']['NotApplicableToFirstRoute'] = "不適用於第一傳遞";
$Lang['DocRouting']['Note'] = "註釋";
$Lang['DocRouting']['NotViewedYet'] = "尚未查看";
$Lang['DocRouting']['NotYetSigned'] = "尚未簽署";
$Lang['DocRouting']['NextRouting'] = "下一個文件傳遞";

// O
//$Lang['DocRouting']['OptionalAction'] = "Optional Actions";
$Lang['DocRouting']['ONLINEFORM']['ReadByOthers'] = "其他使用者觀看:";
$Lang['DocRouting']['ONLINEFORM']['ReleaseResultSettings'] = "檢測回條統計數字:";
$Lang['DocRouting']['ONLINEFORM']['ReleaseAnytime'] = "隨時";
$Lang['DocRouting']['ONLINEFORM']['ReleaseNever'] = "永不";
$Lang['DocRouting']['ONLINEFORM']['ReleaseResultAfterSubmission'] = "遞交回條後";
$Lang['DocRouting']['ONLINEFORM']['ReleaseResultAfterRouteCompeleted'] = "傳遞終結後";
$Lang['DocRouting']['ONLINEFORM']['UserNameDisplaySettings'] = "用戶名稱顯示設定:";
$Lang['DocRouting']['ONLINEFORM']['DisplayUserName'] = "顯示用戶名稱";
$Lang['DocRouting']['ONLINEFORM']['HideUserName'] = "隱藏用戶名稱";
$Lang['DocRouting']['ONLINEFORM']['ReplySlipType'] = "回條類型";
$Lang['DocRouting']['ONLINEFORM']['ReplySlipTypeList'] = "條列";
$Lang['DocRouting']['ONLINEFORM']['ReplySlipTypeTable'] = "表格";
$Lang['DocRouting']['ONLINEFORM']['SubmitAfterDeadline'] = "生效時段後允許遞交:";
$Lang['DocRouting']['ONLINEFORM']['Yes'] = '是';
$Lang['DocRouting']['ONLINEFORM']['No'] = '否';

// P
//$Lang['DocRouting']['PassRouting'] = "Pass Routings";
$Lang['DocRouting']['Period'] = "基於時段 (在下方設置)";
$Lang['DocRouting']['Personal'] = "個人";
$Lang['DocRouting']['PhysicalLocation'] = "實質地點";
$Lang['DocRouting']['Public'] = "公眾";
$Lang['DocRouting']['Password'] = "密碼";
$Lang['DocRouting']['PreviousRouting'] = "上一個文件傳遞";

# Preset Doc Instruction
$Lang['DocRouting']['PresetDocInstruction'] = "預設文件指示";
$Lang['DocRouting']['PresetDocInstructionContent'] = "內容";
$Lang['DocRouting']['PresetInstruction'] = "預設指示";
$Lang['DocRouting']['PresetNotes'][] = array("1", "I would like to have your comments or recommendations");
$Lang['DocRouting']['PresetNotes'][] =  array("2", "Please make arrangement or return the reply slip");
$Lang['DocRouting']['PresetNotes'][] =  array("3", "Please discuss with me (on or before: yyyy-mm-dd )");
$Lang['DocRouting']['PresetNotes'][] =  array("4", "Please encourage teachers or students to participate");
$Lang['DocRouting']['PresetNotes'][] =  array("5", "For your action (on or before: yyyy-mm-dd )");
$Lang['DocRouting']['PresetNotes'][] =  array("6", "For your filing");
$Lang['DocRouting']['PresetNotesSelection'] = "--- 從預設選項選取  ---";
$Lang['DocRouting']['PresetRoutingInstruction'] = "預設傳遞指示";
$Lang['DocRouting']['PresetType'] = "預設類別";
$Lang['DocRouting']['PreviewRouteDone'] = "於前一個傳遞的終結";
$Lang['DocRouting']['StartRouteNow'] = "立即開始傳遞";
$Lang['DocRouting']['PreviewReplySlip'] = "預覽回條";
$Lang['DocRouting']['PreviewCurrentReplySlip'] = "預覽現時回條";

// Q


// R
$Lang['DocRouting']['ReadOnlyUser'] = "只可檢視用戶";
$Lang['DocRouting']['Route'] = "傳遞";
$Lang['DocRouting']['Routing'] = "傳遞";
$Lang['DocRouting']['RoutingDetail'] = "文件傳遞詳細資料";
$Lang['DocRouting']['ReturnMessage']['EmailSentUnsuccess'] = "0|=|發送電郵失敗。"; 
$Lang['DocRouting']['ReturnMessage']['EmailSentSuccess'] = "1|=|發送電郵成功。";
$Lang['DocRouting']['ReplySlipStatistics'] = "回條結果統計";
$Lang['DocRouting']['RequestNumber'] = "Request Number";

// S
$Lang['DocRouting']['SaveAsDraft'] = "儲存為草稿";
$Lang['DocRouting']['Selected'] = "位";
$Lang['DocRouting']['SelectUser'] = "選擇用戶";
$Lang['DocRouting']['SignAndConfirm'] = "簽署/確認";
$Lang['DocRouting']['StepAccess'] = "存取權限開放給";
$Lang['DocRouting']['StepAccessAll'] = "這文件內的所有教職員";
$Lang['DocRouting']['StepAccessRelated'] = "只有這傳遞內的教職員";
$Lang['DocRouting']['StepAccessRelatedAndMore'] = "這傳遞內的教職員以及以下選取的用戶";
$Lang['DocRouting']['Status'] = "狀況";
$Lang['DocRouting']['StatusDisplay']['Waiting'] = "等待處理";
$Lang['DocRouting']['StatusDisplay']['Processing'] = "進行中";
$Lang['DocRouting']['StatusDisplay']['Completed'] = "已完成";
$Lang['DocRouting']['StatusDisplay']['NotYetStarted'] = "尚未開始";
$Lang['DocRouting']['StatusDisplay']['ViewOnly'] = "只能閱讀";

$Lang['DocRouting']['SendReminder'] = "發送提示電郵";
$Lang['DocRouting']['SubmitComment'] = "呈送意見";
$Lang['DocRouting']['SaveAsDraft'] = "儲存草稿";
$Lang['DocRouting']['StartDate'] = "開始日期範圍";
$Lang['DocRouting']['StarredFile'] = "星號標記的文件";
$Lang['DocRouting']['SendEmail'] = "傳送電郵";
$Lang['DocRouting']['SendSignEmailInstruction'] = "你可以透過電郵發出邀請，要求用戶使用發出的超連結簽署文件傳遞。";
$Lang['DocRouting']['SendSignEmailRemark'] = '備註: 一旦郵件被發送，用戶可以不受日期限制隨時簽署文件傳遞。';
$Lang['DocRouting']['SendSignEmailTitle'] = "從 <!--SCHOOL_NAME--> 發出標題為 <!--TITLE--> 的文件傳遞";
$Lang['DocRouting']['SendSignEmailContent'] = '<p>你已經被邀請跟進/簽署一個由 <!--SCHOOL_NAME--> 發出的文件傳遞。</p><p>請按以下的超連結開始處理: </p><p><a href="<!--LINK-->" target="_blank"><!--LINK--></a></p><p>謝謝您的關注。</p>';
$Lang['DcoRouting']['SignedOn'] = "已簽署於<!--TIME-->";
$Lang['DcoRouting']['SignRoutings'] = "簽署傳遞文件";
$Lang['DocRouting']['ReturnMessage']['SignSuccess'] = "1|=|文件傳遞已成功簽署。";
$Lang['DocRouting']['ReturnMessage']['SignUnsuccess'] = "0|=|文件傳遞未能成功簽署。";
$Lang['DocRouting']['SignConfirmMessage'] = "你確定要簽署此傳遞文件?";
$Lang['DocRouting']['SignedAllRoutingMessage'] = "你已經簽署了你所收到的電郵裡所有相關的傳遞文件。謝謝你的參與。";
$Lang['DocRouting']['SignedConfirmed'] = "已簽署/已確認";
$Lang['DocRouting']['SendArchivedDocumentEmailInstruction'] = "你可以透過電郵發送已完成的文件給用戶。";
$Lang['DocRouting']['Security'] = "安全";

// T
$Lang['DocRouting']['Tag']['label'] = "標籤:";
$Lang['DocRouting']['Tags'] = "標籤";
$Lang['DocRouting']['TagsInputRemarks'] = "請用逗號 (,) 作分隔，最多<!--maxNumOfTags-->個標籤。";


# Target Type 
$Lang['DocRouting']['TargetType']['ForAllUsers'] = "所有使用者";
$Lang['DocRouting']['TargetType']['ForMySelf'] = "只有自己";
$Lang['DocRouting']['TargetType']['TargetType'] = "目標用戶";
$Lang['DocRouting']['TargetType']['Title'] = "標題";

$Lang['DocRouting']['Title'] = "標題";
$Lang['DocRouting']['To'] = "給";


// U
$Lang['DocRouting']['Users'] = "用戶";
$Lang['DocRouting']['UploadAttachments'] = "上載附件";
$Lang['DocRouting']['UploadAttachment'] = "上載附件";
$Lang['DocRouting']['UserName'] = "用戶名稱";

// V
$Lang['DocRouting']['Visible'] = "生效";
$Lang['DocRouting']['ViewBy'] = "可檢視用戶";
$Lang['DocRouting']['ViewReplySlipStatistics'] = "檢測回條統計數字";
$Lang['DocRouting']['Version'] = "版本";

// W
$Lang['DocRouting']['WarningMsg']['AreYouSureWantToDeleteThisFile'] = "你確定要移除此附件?";
$Lang['DocRouting']['WarningMsg']['AreYouSureWantToDeleteThisRouting'] = "你確定要刪除此文件傳遞?";
$Lang['DocRouting']['WarningMsg']['AreYouSureWantToSign'] = "你確定要簽署此文件?";
$Lang['DocRouting']['WarningMsg']['MoreThanAllowedTags'] = "超出標籤數目上限，請移除部份標籤。";
$Lang['DocRouting']['WarningMsg']['PleaseFillInBelowReplySlip'] = "請填寫以下回條。";
$Lang['DocRouting']['WarningMsg']['PleaseFillInComment'] = "請填寫意見。";
$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange'] = "請填上合理的日期範圍。";
$Lang['DocRouting']['WarningMsg']['PleaseSelectAllowedActions'] = "請選擇可進行的動作。";
$Lang['DocRouting']['WarningMsg']['PleaseSelectEffectiveTypeByPeriod'] = "請選擇第一個文件傳遞的生效時段。";
$Lang['DocRouting']['WarningMsg']['PleaseSelectUsers'] = "請選擇用戶。";
$Lang['DocRouting']['WarningMsg']['PleaseSetStartDateAfterDateTime'] = "請設定開始日期為<!--DATETIME-->之後。";
$Lang['DocRouting']['WarningMsg']['PleaseUploadReplySlipFile'] = "請上載回條文件。";
$Lang['DocRouting']['WarningMsg']['PleaseUploadAtLeastOneFile'] = "請上載至少一個文件。";
$Lang['DocRouting']['WarningMsg']['ReplySlipCsvNotCorrect'] = "檔案內容不正確，請按「".$Lang['DocRouting']['PreviewReplySlip']."」檢視詳情。";
$Lang['DocRouting']['WarningMsg']['ReplySlipHasAnswerAlready'] = "因有用戶已遞交回條，所以現時不能更改回條內容。";
$Lang['DocRouting']['WarningMsg']['ThereShouldBeAtLeastOneRouting'] = '設定至少需要一個文件傳遞。\n請先新增文件傳遞再移除此文件傳遞。';
$Lang['DocRouting']['WarningMsg']['AnotherPersonGivingFeedback'] = "其他人正在給予回覆，請於<!--TIMEOUT-->分鐘之後再試。";
$Lang['DocRouting']['WarningMsg']['RequestInputPositiveInteger'] = "請輸入正整數。";
$Lang['DocRouting']['WarningMsg']['RouteLockTiemout'] = "此文件傳遞已超時，請取消操作後重新嘗試。";
$Lang['DocRouting']['WarningMsg']['SignIgnoreDraftedComment'] = "你尚有草稿未提交，你確定要簽署此文件?";
$Lang['DocRouting']['WarningMsg']['UserNameWillBeDisplayInReplySlipResult']= "用戶名稱將會被顯示於回條結果。";
$Lang['DocRouting']['WarningMsg']['UserNameWillNotBeDisplayInReplySlipResult']= "用戶名稱將不會被顯示於回條結果。";
$Lang['DocRouting']['WarningMsg']['PleaseSelectEmailTarget'] = "請選擇傳送目標。";
$Lang['DocRouting']['WarningMsg']['PleaseEnterEmailContent'] = "請輸入電郵內容。";
$Lang['DocRouting']['WarningMsg']['PleaseSelectEmailType'] = "請選擇電郵類別。";
$Lang['DocRouting']['WarningMsg']['PleaseSelectRoutes'] = "請選擇文件傳遞。";
$Lang['DocRouting']['WarningMsg']['RequestEnterPasswordToSign'] = "請輸入你的eClass帳戶密碼進入並簽署文件。";
$Lang['DocRouting']['WarningMsg']['InvalidToken'] = "文件碼不正確。";
$Lang['DocRouting']['WarningMsg']['UnauthorizedAccess'] = "你沒有權限進行操作。";
$Lang['DocRouting']['WarningMsg']['ConfirmDeleteFeedback'] = "你確定要移除此回應?";

// X


// Y


// Z



// Print Instruction Customization
$Lang['DocRouting']['PrintInstruction']['PageTitle'] = "列印傳遞指示";
$Lang['DocRouting']['PrintInstruction']['SchoolName'] = "樂善堂楊葛小琳中學";
$Lang['DocRouting']['PrintInstruction']['Memo'] = "備忘錄";
// Print Instruction Customization
$Lang['DocRouting']['PrintInstruction']['FromThePrincipal'] = "由校長";
$Lang['DocRouting']['PrintInstruction']['Date'] = "日期";
$Lang['DocRouting']['PrintInstruction']['TO'] = "予";
$Lang['DocRouting']['PrintInstruction']['Signature'] = "簽署";
$Lang['DocRouting']['PrintInstruction']['CommentsAndRemarks'] = "已簽署老師的意見或評論 (如有需要，請使用後頁)";

?>
