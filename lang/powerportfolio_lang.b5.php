<?php
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/
$Lang['PowerPortfolio']['Title'] = "幼兒學習歷程檔案";

$Lang['PowerPortfolio']['General']['NoRecord'] = '沒有資料';
$Lang['PowerPortfolio']['General']['PleaseConfirmChanges'] = '修改內容後或會影響系統其他功能，繼續前請先再作確認。';
$Lang['PowerPortfolio']['General']['PleaseConfirmScoreUpdate'] = '呈送後原有輸入資料會被覆蓋。';
$Lang['PowerPortfolio']['General']['Level'] = "<!--level_num-->層";
$Lang['PowerPortfolio']['General']['Max'] = "最高";
$Lang['PowerPortfolio']['General']['MaxAverage'] = "最高平均分";
$Lang['PowerPortfolio']['General']['Min'] = "最低";
$Lang['PowerPortfolio']['General']['GradingDetails'] = "等級說明";
$Lang['PowerPortfolio']['General']['LastModifiedDate'] = "最後編輯日期";

$Lang['PowerPortfolio']['Management']['DataTransition']['Title'] = "資料處理";
$Lang['PowerPortfolio']['Management']['DataTransition']['Transition'] = "轉變";
$Lang['PowerPortfolio']['Management']['DataTransition']['ActiveAcademicYear'] = "系統使用中的學年";
$Lang['PowerPortfolio']['Management']['DataTransition']['CurrentAcademicYear'] = "本學年";
$Lang['PowerPortfolio']['Management']['DataTransition']['NewAcademicYear'] = "新學年";
$Lang['PowerPortfolio']['Management']['DataTransition']['OtherAcademicYears'] = "其他學年";
$Lang['PowerPortfolio']['Management']['DataTransition']['DataTransitionWarning1'] = "請在以下兩種情況發生前，執行此功能:<br />- 於學年始時，重新啟用 Power Portfolio 系統<br />- 還原以往某學年 Power Portfolio 系統的資料";
$Lang['PowerPortfolio']['Management']['DataTransition']['DataTransitionWarning2'] = "- 系統內現有資料將會被整存。";
$Lang['PowerPortfolio']['Management']['DataTransition']['ConfirmCreateNewYearDatabase'] = "新學年資料會複製自學年<!--academicYearName-->，你是否確定轉變系統至新學年？";
$Lang['PowerPortfolio']['Management']['DataTransition']['ConfirmTransition'] = "確定進行所選擇的資料轉變?";

$Lang['PowerPortfolio']['Management']['InputScore']['Title'] = "持續性評估紀錄";
$Lang['PowerPortfolio']['Management']['InputScore']['InputStatus'] = "已輸入";
$Lang['PowerPortfolio']['Management']['InputScore']['Topic'] = "主題";
$Lang['PowerPortfolio']['Management']['InputScore']['Rubric'] = "評量指標";
$Lang['PowerPortfolio']['Management']['InputScore']['GradeDescription'] = "等級說明";
$Lang['PowerPortfolio']['Management']['InputScore']['ClassAndClassNo'] = "班別學號";
$Lang['PowerPortfolio']['Management']['InputScore']['StudentName'] = "學生名稱";
$Lang['PowerPortfolio']['Management']['InputScore']['FormatWarningMsg'] = "資料格式不正確";
$Lang['PowerPortfolio']['Management']['InputScore']['AccordingClass'] = "按班別";
$Lang['PowerPortfolio']['Management']['InputScore']['AccordingStudent'] = "按學生";
$Lang['PowerPortfolio']['Management']['InputScore']['Student'] = "學生";
$Lang['PowerPortfolio']['Management']['InputScore']['Score'] = "評分";
$Lang['PowerPortfolio']['Management']['InputScore']['N.A.'] = "不適用";

$Lang['PowerPortfolio']['Management']['Activity']['Title'] = '活動紀錄';
$Lang['PowerPortfolio']['Management']['Activity']['InputStatus'] = "已輸入";
$Lang['PowerPortfolio']['Management']['Activity']['AccordingClass'] = "按班別";
$Lang['PowerPortfolio']['Management']['Activity']['AccordingStudent'] = "按學生";
$Lang['PowerPortfolio']['Management']['Activity']['ActivityPhoto'] = "活動照片 (最大5Mb)";
$Lang['PowerPortfolio']['Management']['Activity']['Description'] = "描述 (最多100字)";
$Lang['PowerPortfolio']['Management']['Activity']['Student'] = "學生";

$Lang['PowerPortfolio']['Reprots']['CAReport']['Title'] = "持續性評估報告";
$Lang['PowerPortfolio']['Reprots']['CAReport']['Report'] = "報告";
$Lang['PowerPortfolio']['Reprots']['CAReport']['ReleaseDate'] = "發出日期";
$Lang['PowerPortfolio']['Reprots']['CAReport']['Topic'] = "主題";

$Lang['PowerPortfolio']['Settings']['Rubrics']['Title'] = "評量指標設定";
$Lang['PowerPortfolio']['Settings']['Rubrics']['Name'] = "名稱";
$Lang['PowerPortfolio']['Settings']['Rubrics']['TargetForm'] = "應用級別";
$Lang['PowerPortfolio']['Settings']['Rubrics']['Levels'] = "評量指標層數";
$Lang['PowerPortfolio']['Settings']['Rubrics']['GradingNum'] = "評量等級數目";
$Lang['PowerPortfolio']['Settings']['Rubrics']['GradingDetails'] = "評量等級說明";
$Lang['PowerPortfolio']['Settings']['Rubrics']['TermResultGradingNum'] = "學期總結評估等級數目";
$Lang['PowerPortfolio']['Settings']['Rubrics']['TermResultGrades'] = "學期總結評估等級";
$Lang['PowerPortfolio']['Settings']['Rubrics']['SubTitle'] = "評量指標";
$Lang['PowerPortfolio']['Settings']['Rubrics']['AddItem'] = '新增項目';
$Lang['PowerPortfolio']['Settings']['Rubrics']['AddSubCat'] = '新增子分類';
$Lang['PowerPortfolio']['Settings']['Rubrics']['AddCat'] = '新增分類';
$Lang['PowerPortfolio']['Settings']['Rubrics']['PleaseInputName'] = '請輸入名稱';
$Lang['PowerPortfolio']['Settings']['Rubrics']['ImportRubricIndex'] = '匯入評量指標';
$Lang['PowerPortfolio']['Settings']['Rubrics']['Category'] = '分類';
$Lang['PowerPortfolio']['Settings']['Rubrics']['SubCategory'] = '子分類';
$Lang['PowerPortfolio']['Settings']['Rubrics']['Item'] = '項目';
$Lang['PowerPortfolio']['Settings']['Rubrics']['RubricIndexHint'] = '若要使用「匯入評量指標」功能，請勿修改或刪除任何評量指標';
$Lang['PowerPortfolio']['Settings']['Rubrics']['ImportReference']['Title'] = '按此查詢可匯入的評量指標設定';
$Lang['PowerPortfolio']['Settings']['Rubrics']['ImportReference']['SubCatRemark'] = '<span class="tabletextremark">(若評量指標層數為2層，請留空)</span>';

$Lang['PowerPortfolio']['Settings']['Topic']['Title'] = "主題設定";
$Lang['PowerPortfolio']['Settings']['Topic']['Name'] = "主題名稱";
$Lang['PowerPortfolio']['Settings']['Topic']['TargetForm'] = "應用級別";
$Lang['PowerPortfolio']['Settings']['Topic']['Rubrics'] = "評量指標設定";
$Lang['PowerPortfolio']['Settings']['Topic']['AssessmentPeriod'] = "評估時段";
$Lang['PowerPortfolio']['Settings']['Topic']['InputPeriod'] = "入分時段";
$Lang['PowerPortfolio']['Settings']['Topic']['Selected'] = "已選";

$Lang['PowerPortfolio']['Settings']['Activity']['Title'] = '活動紀錄設定';
$Lang['PowerPortfolio']['Settings']['Activity']['Name'] = '活動名稱';
$Lang['PowerPortfolio']['Settings']['Activity']['ReportName'] = '報告名稱';
$Lang['PowerPortfolio']['Settings']['Activity']['ApplyTopic'] = '使用主題';
$Lang['PowerPortfolio']['Settings']['Activity']['ActivityTopic'] = '主題';
$Lang['PowerPortfolio']['Settings']['Activity']['ActivityTitle'] = '題目';
$Lang['PowerPortfolio']['Settings']['Activity']['TargetForm'] = '應用級別';
$Lang['PowerPortfolio']['Settings']['Activity']['Date'] = '活動日期';
$Lang['PowerPortfolio']['Settings']['Activity']['Duration'] = '活動時間 (分鐘)';
$Lang['PowerPortfolio']['Settings']['Activity']['Location'] = '活動地點';
$Lang['PowerPortfolio']['Settings']['Activity']['Type'] = '內容';
$Lang['PowerPortfolio']['Settings']['Activity']['ObserveArea'] = '觀察範疇';
$Lang['PowerPortfolio']['Settings']['Activity']['TypeArr']['Photo'] = '相片';
$Lang['PowerPortfolio']['Settings']['Activity']['TypeArr']['Text'] = '文字描述';
$Lang['PowerPortfolio']['Settings']['Activity']['TypeArr']['PhotoText'] = '相片及文字描述';

$Lang['PowerPortfolio']['Settings']['Zone']['Title'] = "學習區";
$Lang['PowerPortfolio']['Settings']['Zone']['Name'] = "學習區名稱";
$Lang['PowerPortfolio']['Settings']['Zone']['Topic'] = "主題";
$Lang['PowerPortfolio']['Settings']['Zone']['TopicCode'] = "主題代號";
$Lang['PowerPortfolio']['Settings']['Zone']['Quota'] = "容納人數";
$Lang['PowerPortfolio']['Settings']['Zone']['Image'] = "學習區圖像";
$Lang['PowerPortfolio']['Settings']['Zone']['ImageCode'] = "學習區圖像代號";
$Lang['PowerPortfolio']['Settings']['Zone']['ImportReference']['Topic'] = '按此查詢主題';
$Lang['PowerPortfolio']['Settings']['Zone']['ImportReference']['Image'] = '按此查詢學習區圖像';

$Lang['PowerPortfolio']['Settings']['Tool']['Title'] = "教具庫";
$Lang['PowerPortfolio']['Settings']['Tool']['Name'] = "教具名稱";
$Lang['PowerPortfolio']['Settings']['Tool']['ApplyForm'] = '應用級別';
$Lang['PowerPortfolio']['Settings']['Tool']['Topic'] = '主題';
$Lang['PowerPortfolio']['Settings']['Tool']['Zone'] = '學習區';
$Lang['PowerPortfolio']['Settings']['Tool']['Rubrics'] = '評量指標';
$Lang['PowerPortfolio']['Settings']['Tool']['Photo'] = "相片";
$Lang['PowerPortfolio']['Settings']['Tool']['AddRubricIndex'] = "新增評量指標";

$Lang['PowerPortfolio']['Settings']['Comment']['Title'] = "評語庫";
$Lang['PowerPortfolio']['Settings']['Comment']['Name'] = "評語";
$Lang['PowerPortfolio']['Settings']['Comment']['Category'] = "評語分類";
$Lang['PowerPortfolio']['Settings']['Comment']['CategoryName'] = "分類名稱";
$Lang['PowerPortfolio']['Settings']['Comment']['Content'] = "評語內容";
$Lang['PowerPortfolio']['Settings']['Comment']['Count'] = "評語數量";
$Lang['PowerPortfolio']['Settings']['Comment']['ImportReference']['CategoryName'] = '按此查詢已存在之分類名稱';

$Lang['PowerPortfolio']['Settings']['Report']['Title'] = "報告設定";
$Lang['PowerPortfolio']['Settings']['Report']['Name'] = "報告簡稱";
$Lang['PowerPortfolio']['Settings']['Report']['TargetForm'] = "應用級別";
$Lang['PowerPortfolio']['Settings']['Report']['Period'] = "評估時段";
$Lang['PowerPortfolio']['Settings']['Report']['Cover'] = "封面";
$Lang['PowerPortfolio']['Settings']['Report']['CoverIncludeInfo'] = "封面包括性別、身高及體重";
$Lang['PowerPortfolio']['Settings']['Report']['CoverTitle'] = "封面題目";
$Lang['PowerPortfolio']['Settings']['Report']['CoverTitleDefault'] = "幼兒評量紀錄";
$Lang['PowerPortfolio']['Settings']['Report']['CoverSubTitle'] = "封面副題";
$Lang['PowerPortfolio']['Settings']['Report']['CAReport'] = "持續性評估報告";
$Lang['PowerPortfolio']['Settings']['Report']['CAReportTitle'] = "持續性評估報告名稱";
$Lang['PowerPortfolio']['Settings']['Report']['CAReportTitleDefault'] = "持續性評估紀錄";
$Lang['PowerPortfolio']['Settings']['Report']['CAReportSelectTopic'] = "選擇主題";
$Lang['PowerPortfolio']['Settings']['Report']['CAReportTopicCount'] = "主題數目";
$Lang['PowerPortfolio']['Settings']['Report']['ActivityRecords'] = "活動紀錄";
$Lang['PowerPortfolio']['Settings']['Report']['ActivitySelect'] = "選擇活動";
$Lang['PowerPortfolio']['Settings']['Report']['ActivityCount'] = "活動數目";
$Lang['PowerPortfolio']['Settings']['Report']['TermAssessment'] = "學期總結評估";
$Lang['PowerPortfolio']['Settings']['Report']['TermAssessmentIncluded'] = "包括學期總結評估";
$Lang['PowerPortfolio']['Settings']['Report']['TermAssessmentTitle'] = "學期總結評估報告名稱";
$Lang['PowerPortfolio']['Settings']['Report']['TermComment'] = "學期總結評語";
$Lang['PowerPortfolio']['Settings']['Report']['TermCommentIncluded'] = "包括學期總結評語";
$Lang['PowerPortfolio']['Settings']['Report']['TermCommentTitle'] = "學期總結評語報告名稱";
$Lang['PowerPortfolio']['Settings']['Report']['PageLastPage'] = "報告尾頁";
$Lang['PowerPortfolio']['Settings']['Report']['PageLastPageIncludeInfo'] = "包括出席情況";

$Lang['PowerPortfolio']['Setting']['Code'] = "編號";
$Lang['PowerPortfolio']['Setting']['Type'] = "類型";
$Lang['PowerPortfolio']['Setting']['Name'] = "名稱";
$Lang['PowerPortfolio']['Setting']['NameB5'] = "中文名稱";
$Lang['PowerPortfolio']['Setting']['NameEN'] = "英文名稱";
$Lang['PowerPortfolio']['Setting']['ApplyForm'] = "應用級別";
$Lang['PowerPortfolio']['Setting']['ZoneTopic'] = "所屬主題";
$Lang['PowerPortfolio']['Setting']['ZoneQuota'] = "容納人數";
$Lang['PowerPortfolio']['Setting']['PhotoRemarks'] = "建議解像度: 300px * 300px (闊 * 高)";
$Lang['PowerPortfolio']['Setting']['PleaseInputContent'] = "請輸入內容";
$Lang['PowerPortfolio']['Setting']['PleaseInputNum'] = "請輸入數字";
$Lang['PowerPortfolio']['Setting']['PleaseSelectForm'] = "請選擇一個級別";
$Lang['PowerPortfolio']['Setting']['PleaseOneOption'] = "請選擇一個選項";
$Lang['PowerPortfolio']['Setting']['Photo'] = "相片";
$Lang['PowerPortfolio']['Setting']['Picture'] = "學習區圖象";
$Lang['PowerPortfolio']['Setting']['AbilityIndex'] = "能力指標";
$Lang['PowerPortfolio']['Setting']['Year'] = "應用級別";
$Lang['PowerPortfolio']['Setting']['Remarks'] = "備註";
$Lang['PowerPortfolio']['Setting']['ToolCategory'] = "教具類別";
$Lang['PowerPortfolio']['Setting']['Chapter'] = "單元";
$Lang['PowerPortfolio']['Setting']['ActionType'] = "行動類型";


$Lang['PowerPortfolio']['Setting']['InputWarning'] = "請輸入";
$Lang['PowerPortfolio']['Setting']['DuplicateWarning'] = "重複";
$Lang['PowerPortfolio']['Setting']['EditWarning']['NoMoreThan1'] = "編緝數目不能大於一";
$Lang['PowerPortfolio']['Setting']['EditWarning']['PleaseSelectEdit'] = "請選擇編緝項目";
$Lang['PowerPortfolio']['Setting']['EditWarning']['CommentTooLong'] = "評語超出字元上限。（最多<!--max-->字）";
$Lang['PowerPortfolio']['Setting']['DeleteWarning']['PleaseSelectDelete'] = "請選擇刪除項目";
$Lang['PowerPortfolio']['Setting']['ImportWarning']['CSVFormat'] = '檔案格式錯誤。上載的檔案必須是.csv的檔案。';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['EmptyContent'] = '檔案內容不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['ErrorCount'] = '錯誤總數';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['RecordCount'] = '紀錄總數';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['InsertCount'] = '成功新增紀錄';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['RubricSettingEdited'] = '此設定之評量指標已被修改過';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['ItemNotFound']['Topic'] = '找不到主題';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['ItemNotFound']['RubricSetting'] = '找不到評量指標設定';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Name'] = '名稱不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Item'] = '項目不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Topic'] = '主題不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Quota'] = '容納人數不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Category'] = '分類不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['CommentCategory'] = '評語分類不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Comment'] = '評語內容不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['SubCategory'] = '子分類不能為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldInvalid']['SubCategory'] = '子分類應留為空白';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['ZonePicture'] = "圖片編號必須為";
$Lang['PowerPortfolio']['Setting']['ImportWarning']['WrongInputPeriod'] = "入分時段不能早於評估時段開始";
$Lang['PowerPortfolio']['Setting']['ImportWarning']['WrongInputPeriod2'] = "入分時段不能早於評估時段結束";
/*
$Lang['PowerPortfolio']['Management']['LearningZone']['Title'] = "學習區";

$Lang['PowerPortfolio']['Management']['TopicTimeTable']['Title'] = "主題時間表";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['StartDate'] = "開始時間";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['EndDate'] = "結束時間";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['PleaseEnterTopic'] = "請選擇主題庫";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['PleaseEnterYear'] = "請選擇應用級別";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['NoLearningTool'] = "沒有教具";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['NumOfZones'] = "學習區數目";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['PleaseConfirmChanges'] = '學習區設定已經更改，如要儲存已更改的設定，請按"呈送"。';

$Lang['PowerPortfolio']['Management']['ToolScore']['Title'] = "教具分數修改";
$Lang['PowerPortfolio']['Management']['ToolScore']['ToolList'] = "教具列表";
$Lang['PowerPortfolio']['Management']['ToolScore']['InputScore'] = "輸入分數";
$Lang['PowerPortfolio']['Management']['ToolScore']['AllTimeTable'] = "所有時間表";
$Lang['PowerPortfolio']['Management']['ToolScore']['TimeTable'] = "時間表";
$Lang['PowerPortfolio']['Management']['ToolScore']['TitleName'] = "主題";
$Lang['PowerPortfolio']['Management']['ToolScore']['ClassName'] = "班別";
$Lang['PowerPortfolio']['Management']['ToolScore']['TeachingTools'] = "教具";
$Lang['PowerPortfolio']['Management']['ToolScore']['ClassNumber'] = "學號";
$Lang['PowerPortfolio']['Management']['ToolScore']['StudentName'] = "學生";
$Lang['PowerPortfolio']['Management']['ToolScore']['AddNewInputScoreRow'] = "增加輸入行數";
$Lang['PowerPortfolio']['Management']['ToolScore']['MaxAddNewInputScoreRow'] = "每次行數增加不可以多於20";
$Lang['PowerPortfolio']['Management']['ToolScore']['InputMarkInvalid'] = "輸入分數無效";
$Lang['PowerPortfolio']['Management']['ToolScore']['InputMarkCannotBeNegative'] = "所需入分數不可為負數。";
$Lang['PowerPortfolio']['Management']['ToolScore']['InputMarkCannotBeLargerThanFullMark'] = "所輸入的分數不可高於滿分。";

$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['Title'] = "科目能力指標分數修改";
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['InputScore'] = "輸入分數";
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['Form'] = '級別';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['Subject'] = '科目';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['TermTimeTable'] = '學期  / 主題時間表';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['Term'] = '學期';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['TimeTable'] = '主題時間表';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['InputMarkInvalid'] = "輸入分數無效";
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['InputMarkCannotBeNegative'] = "所需入分數不可為負數。";
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['InputMarkCannotBeLargerThanFullMark'] = "所輸入的分數不可高於滿分。";

$Lang['PowerPortfolio']['Management']['LanguageBehavior']['Title'] = "語文能力及行為指標分數修改";
$Lang['PowerPortfolio']['Management']['LanguageBehavior']['InputScore'] = "輸入分數";

$Lang['PowerPortfolio']['Management']['OtherInfo']['Title'] = "其他資料";
$Lang['PowerPortfolio']['Management']['OtherInfoArr']['summary'] = "學生資料";
$Lang['PowerPortfolio']['Management']['OtherInfoArr']['award'] = "獎項";

$Lang['PowerPortfolio']['Management']['OtherInfoWarningArr']['InputGrade'] = "輸入等級";
$Lang['PowerPortfolio']['Management']['OtherInfoWarningArr']['InputNumber'] = "輸入數字";

$Lang['PowerPortfolio']['Management']['GenerateReports']['Title'] = "列印學生成績表";

$Lang['PowerPortfolio']['Management']['ExportPerformance']['Title'] = "匯出學生表現";
$Lang['PowerPortfolio']['Management']['ExportYearlyPerformance']['Title'] = "匯出學生全年表現";

$Lang['PowerPortfolio']['Management']['ArchiveReport']['Title'] = "成績表存檔";
$Lang['PowerPortfolio']['Management']['ArchiveReport']['LastArchived'] = "上一次存檔";
$Lang['PowerPortfolio']['Management']['ArchiveReport']['ConfirmArchiveReport'] = '確定要覆蓋原有成績表存檔？';

$Lang['PowerPortfolio']['Management']['PrintArchiveReport']['Title'] = "列印成績表存檔";
$Lang['PowerPortfolio']['Management']['PrintArchiveReport']['WarningArr']['NoMatchTerm'] = "沒有對應的學期";
$Lang['PowerPortfolio']['Management']['PrintArchiveReport']['WarningArr']['NoMatchClass'] = "沒有對應的班別";
$Lang['PowerPortfolio']['Management']['PrintArchiveReport']['WarningArr']['NoMatchStudent'] = "沒有對應的學生";

$Lang['PowerPortfolio']['Management']['Device']['Title'] = "上課平板介面";
$Lang['PowerPortfolio']['Management']['Device']['LearningZone'] = "學習區";
$Lang['PowerPortfolio']['Management']['Device']['InputScore'] = "課堂入分";
$Lang['PowerPortfolio']['Management']['Device']['Class'] = "班別";
$Lang['PowerPortfolio']['Management']['Device']['Topic'] = "主題";
$Lang['PowerPortfolio']['Management']['Device']['NoClasses'] = "無所屬班別";
$Lang['PowerPortfolio']['Management']['Device']['NoTopics'] = "無可以選擇主題";
$Lang['PowerPortfolio']['Management']['Device']['NotSelectAnyClasses'] = "無選擇任何班別";
$Lang['PowerPortfolio']['Management']['Device']['NotSelectAnyTopics'] = "無選擇任何主題";
$Lang['PowerPortfolio']['Management']['Device']['CompleteInputScore'] = "已評分";
$Lang['PowerPortfolio']['Management']['Device']['IncompleteInputScore'] = "未評分";
$Lang['PowerPortfolio']['Management']['Device']['AllEntryStatus'] = "全部";
$Lang['PowerPortfolio']['Management']['Device']['InZoneStatus'] = "已進入";
$Lang['PowerPortfolio']['Management']['Device']['CompleteStatus'] = "已完成";
$Lang['PowerPortfolio']['Management']['Device']['IncompleteStatus'] = "未完成";
$Lang['PowerPortfolio']['Management']['Device']['NoQuota'] = "學習區已滿";
$Lang['PowerPortfolio']['Management']['Device']['PleaseLeaveFirst1'] = "你仍未離開學習區[";
$Lang['PowerPortfolio']['Management']['Device']['PleaseLeaveFirst2'] = "]";
$Lang['PowerPortfolio']['Management']['Device']['PleaseSelectTool'] = "請選擇教具";

//$Lang['PowerPortfolio']['Setting']['TaiwanAbilityIndex']['Title'] = "台灣能力指標";
$Lang['PowerPortfolio']['Setting']['TaiwanAbilityIndex']['Title'] = "能力指標";
$Lang['PowerPortfolio']['Setting']['TaiwanAbilityIndex']['Code'] = "編碼";
$Lang['PowerPortfolio']['Setting']['TaiwanAbilityIndex']['Name'] = "能力指標";

$Lang['PowerPortfolio']['Management']['AbilityRemarks']['Title'] = '能力指標評語 (級別及學期設定)';
$Lang['PowerPortfolio']['Management']['AbilityRemarks']['Reset'] = '重設評語';
$Lang['PowerPortfolio']['Management']['AbilityRemarks']['ConfirmResetRemark'] = '你確定要重設評語？';
$Lang['PowerPortfolio']['Management']['AbilityRemarks']['ChangeAbilityResetAll'] = '如更改能力指標，所有相關評語設定將被同時重設。你是否確定更改至已選的能力指標？';

$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Title'] = '能力指標評語';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Grading'] = '評級';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['UpperLimit'] = '評級範圍';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Comment'] = '能力指標評語 (原始)';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['OriComment'] = '能力指標評語 (自定)';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['NewComment'] = '能力指標評語 (級別及學期設定)';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Term'] = '學期';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Year'] = '學年';

$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['Title'] = "能力指標配對";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['MacauCode'] = "澳門編碼";
//$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['TaiwanCode'] = "台灣編碼";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['TaiwanCode'] = "能力指標編碼";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllMacauCat'] = "所有澳門編碼類別";
//$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllTaiwanCat'] = "所有台灣編碼類別";
//$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllTaiwanLevel'] = "所有台灣級別";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllTaiwanCat'] = "所有能力指標編碼類別";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllTaiwanLevel'] = "所有能力指標級別";

$Lang['PowerPortfolio']['Setting']['FormTopic']['Title'] = "主題庫";
$Lang['PowerPortfolio']['Setting']['LearningZone']['Title'] = "學習區庫";
$Lang['PowerPortfolio']['Setting']['TeachingTool']['Title'] = "教具庫";
$Lang['PowerPortfolio']['Setting']['TeachingToolCategory']['Title'] = "教具類別";
$Lang['PowerPortfolio']['Setting']['LanguageBehavior']['Title'] = "語文能力及行為指標";
$Lang['PowerPortfolio']['Setting']['LanguageBehaviorItem']['Title'] = "語文能力及行為指標評估項目";
$Lang['PowerPortfolio']['Setting']['LanguageBehavior']['Category1'] = "語文能力";
$Lang['PowerPortfolio']['Setting']['LanguageBehavior']['Category2'] = "行為指標";

$Lang['PowerPortfolio']['Setting']['SubjectMapping']['Title'] = '科目能力指標配對';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['Form'] = '級別';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['Subject'] = '科目';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['TermTimeTable'] = '學期  / 主題時間表';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['Term'] = '學期';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['TimeTable'] = '主題時間表';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TermInUse'] = '此級別科目已設定使用學期';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TermUsed'] = '此學期已被使用';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TermEmpty'] = '請輸入學期';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TopicInUse'] = '此級別科目已設定使用主題時間表';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TopicUsed'] = '此主題時間表已被使用';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TopicEmpty'] = '請輸入主題時間表';

$Lang['PowerPortfolio']['Management']['AbilityRemarksSelections']['ModuleTitle'] = '能力指標分類配對 (級別及學期設定)';
$Lang['PowerPortfolio']['Management']['AbilityRemarksSelections']['Title'] = '能力指標分類';
$Lang['PowerPortfolio']['Management']['AbilityRemarksSelections']['Selections'] = '分類選項';
$Lang['PowerPortfolio']['Management']['AbilityRemarksSelections']['None'] = '無分類';

$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['Title'] = '管理時間表';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['Term'] = '學期';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['StartDate'] = '開始日期';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['StartTime'] = '開始時間';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['EndDate'] = '結束日期';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['EndTime'] = '結束時間';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['ModifiedDate'] = '最後更新日期';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['InputScorePeriod'] = "老師呈交分數";
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['ViewReportPeriod'] = "家長列印成績表";

$Lang['PowerPortfolio']['Management']['AwardGeneration']['Title'] = "選擇/製作學生獎項";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['StudentAward'] = "學生獎項";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['SelectAward'] = "選擇獎項";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['GenerateAward'] = "製作獎項";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReceivedAwardCount'] = "得獎學生數目";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['LastGenerate'] = "上一次製作";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['GenerateAll'] = "製作所有班別獎項";

$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['NoAward'] = "請選擇獎項。";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['NoClass'] = "請選擇班別。";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['NoStudentInClass'] = "此班別沒有學生";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['NoAwardedStudentSettings'] = "沒有學生獲得此獎項";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['AtLeastOneStudent'] = "請選擇至少一個學生。";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['DeleteAward'] = "你是否確定要刪除此學生的此獎項？";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['ReGenerateAward'] = "如製作獎項，所有已製作的獎項將被刪除。你是否確定要製作獎項？";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardGenerateSuccess'] = "1|=|製作獎項成功。";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardGenerateFailed'] = "0|=|製作獎項失敗。";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteSuccess'] = "1|=|刪除獎項成功。";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteFailed'] = "0|=|刪除獎項失敗。";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddSuccess'] = "1|=|新增獎項成功。";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddFailed'] = "0|=|新增項失敗。";


$Lang['PowerPortfolio']['Setting']['AwardsList']['Title'] = "學生獎項庫";
$Lang['PowerPortfolio']['Setting']['AwardsList']['AwardType'] = "獎項種類";
$Lang['PowerPortfolio']['Setting']['AwardsList']['AwardTypeArr']['G'] = "系統製作獎項";
$Lang['PowerPortfolio']['Setting']['AwardsList']['AwardTypeArr']['I'] = "老師選擇獎項";
# Settings > Admin Group
$Lang['PowerPortfolio']['Setting']['AdminGroup']['MenuTitle'] = '行政小組';
$Lang['PowerPortfolio']['Setting']['AdminGroup']['Title'] = '行政小組';
$Lang['PowerPortfolio']['Setting']['AdminGroup']['NumOfMember'] = "組員數目";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['NewAdminGroupStep'][1] = "資料及權限";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['NewAdminGroupStep'][2] = "選擇成員";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AdminGroupCode'] = "行政小組代號";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AdminGroupName'] = "行政小組名稱";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AdminGroupList'] = "行政小組列表";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['MemberList'] = "組員列表";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AddMember'] = "加入組員";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AdminGroupInfo'] = "行政小組資料";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AccessRight'] = "權限";

$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['AddSuccess'] = "1|=|新增行政小組成功。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['AddFailed'] = "0|=|新增行政小組失敗。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['EditSuccess'] = "1|=|編輯行政小組成功。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['EditFailed'] = "0|=|編輯行政小組失敗。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['DeleteSuccess'] = "1|=|刪除行政小組成功。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['DeleteFailed'] = "0|=|刪除行政小組失敗。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['AddMemberSuccess'] = "1|=|已加入行政小組組員。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['AddMemberFailed'] = "0|=|行政小組組員加入失敗。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['DeleteMemberSuccess'] = "1|=|已刪除行政小組組員。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['DeleteMemberFailed'] = "0|=|行政小組組員刪除失敗。";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ModifiedDate'] = '最後更新日期';

$Lang['PowerPortfolio']['Setting']['AdminGroup']['Warning']['SelectionContainsMember'] = "已選擇的用戶中包含此行政小組的成員";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['WarningArr']['Blank']['Code'] = "不能沒有代號";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['WarningArr']['Blank']['Name'] = "不能沒有名稱";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['WarningArr']['InUse']['Code'] = "此代號已被使用";
*/
?>