<?php

	$Lang['Header']['en'] = "ENG";
	$Lang['Header']['b5'] = "繁";
	
	$Lang['Header']['Categories'] = "類別";
	$Lang['Header']['Location'] = "拍攝地";
	$Lang['Header']['MyFavourite'] = "我的最愛";
	$Lang['Header']['AdvancedSearch'] = "進階搜尋";
	
	$Lang['Common']['NoRecordsFound'] = "沒有記錄";	
	$Lang['Common']['Views'] = "瀏覽(";
	$Lang['Common']['ViewsEnd'] = ")";
	$Lang['Common']['Records'] = "影片";
	$Lang['Common']['TermAndConditions'] = "條款及細則";	
	
	$Lang['FrontPage']['SortBy'] = "分類";
	$Lang['FrontPage']['LatestUploaded'] = "最新";
	$Lang['FrontPage']['MostViewed'] = "最多瀏覽";
	$Lang['FrontPage']['All'] = "所有影片";
	$Lang['FrontPage']['Total'] = "總數";
	$Lang['FrontPage']['Page'] = "第";
	$Lang['FrontPage']['PageEnd'] = "頁";
	$Lang['FrontPage']['ViewBy'] = "顯示:";
	$Lang['FrontPage']['Display'] = "每頁顯示";
	$Lang['FrontPage']['DisplayEnd'] = " 項";
	
	
	$Lang['VideoPage']['Previous'] = "上一段影片";
	$Lang['VideoPage']['Next'] = "下一段影片";
	$Lang['VideoPage']['Summary'] = "簡介";
	$Lang['VideoPage']['Script'] = "文字稿";
	$Lang['VideoPage']['Tag'] = "標籤";
	$Lang['VideoPage']['AddToFavourite'] = "加入我的最愛";
	$Lang['VideoPage']['RemoveBookmark'] = "從我的最愛移除";
	
	$Lang['SearchPage']['AdvancedSearch'] = "進階搜尋";
	$Lang['SearchPage']['Keywords'] = "關鍵字";
	$Lang['SearchPage']['Category'] = "類別";
	$Lang['SearchPage']['Location'] = "拍攝地";
	$Lang['SearchPage']['Select'] = "選擇";
	$Lang['SearchPage']['Search'] = "搜尋";
	$Lang['SearchPage']['Reset'] = "重設";
	$Lang['SearchPage']['Found'] = "個記錄";
	$Lang['SearchPage']['Total'] = "總數";
	$Lang['SearchPage']['Page'] = "第";
	$Lang['SearchPage']['PageEnd'] = "頁";
	
	$Lang['FavouritePage']['RemoveAll'] = "全部移除";
	$Lang['FavouritePage']['Total'] = "總數";
	$Lang['FavouritePage']['Page'] = "第";
	$Lang['FavouritePage']['PageEnd'] = "頁";
	$Lang['FavouritePage']['RemovefromFavourite'] = "從我的最愛移除";
	
	$Lang['Message']['BrowserChecking'] = "eLibrary影片平台不完全支援你所使用的瀏覽器版本。建議你使用Google Chrome 或 Internet Explorer 10進行瀏覽。";
?>