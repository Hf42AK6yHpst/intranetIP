<?php
/** [Modification Log] Modifying By: Siuwan**/
// a
$Lang['W2']['addMoreQuestion'] = 'Add More';
$Lang['W2']['addMore'] = 'Add more';
$Lang['W2']['allGrades'] = 'All Grades';
$Lang['W2']['allWriting'] = 'All Writings';
$Lang['W2']['alreadyCommented'] = 'Teacher Commented';
//$Lang['W2']['alreadyCommented'] = 'Already Commented';###
$Lang['W2']['approvalIsNeeded'] = 'Approval is needed';
$Lang['W2']['advanced'] = 'Advanced';
$Lang['W2']['addComment'] = 'Add Comment';
$Lang['W2']['answer'] = 'Answer';
$Lang['W2']['article'] = 'Article';
$Lang['W2']['attachment'] = 'Attachment';
$Lang['W2']['audience'] = 'Audience';

// b
$Lang['W2']['basic'] = 'Basic';
$Lang['W2']['backToPublish'] = 'Back to Publish Page';
$Lang['W2']['brainstorming'] = 'Brainstorming';
$Lang['W2']['buildUpTheMindMap'] = 'Build up the mind map';
$Lang['W2']['btn']['go'] = 'Go';

// c
$Lang['W2']['category'] = 'Category';
$Lang['W2']['check'] = 'Check';
$Lang['W2']['checkPoint'] = 'Guiding Question';
$Lang['W2']['className'] = 'Class';
$Lang['W2']['ClassNo'] = "Class Number";
$Lang['W2']['clickHere'] = 'Click Here';
$Lang['W2']['comments'] = 'Comment(s)';
$Lang['W2']['comment'] = 'Comment';
$Lang['W2']['completed'] = 'Completed';
$Lang['W2']['completedDate'] = 'Completed Date';
$Lang['W2']['completedWriting'] = 'Completed Writing';
$Lang['W2']['conceptMap'] = 'Concept Map';
$Lang['W2']['content'] = 'Content';
$Lang['W2']['contentInput'] = 'Content Input';
$Lang['W2']['contentName'] = 'Content Name';
$Lang['W2']['contentPro'] = 'Content Provider';
$Lang['W2']['copy'] = 'Copy';
$Lang['W2']['createdBy'] = 'Created By';
$Lang['W2']['creator'] = 'Created By';
$Lang['W2']['createDate'] = 'Created Date';
// d
$Lang['W2']['datePublished'] = 'Date Published';
$Lang['W2']['deleteConfirmed'] = 'Are you sure to delete this template?';
$Lang['W2']['details'] = 'Details';
$Lang['W2']['draft'] = 'Draft';
$Lang['W2']['data1Title'] = 'Data 1 Title';
$Lang['W2']['data2Title'] = 'Data 2 Title';

// e
$Lang['W2']['engMyWriting'] = 'My Writing(s)';
$Lang['W2']['eachStudentCanMark'] = 'Each student can mark';
$Lang['W2']['eachStudentMarksEveryone'] = 'Each student marks everyone';
$Lang['W2']['evaluated'] = 'Evaluated';
$Lang['W2']['exampleSentence'] = 'Example Sentence';
$Lang['W2']['exerciseName'] = 'Exercise Name';
$Lang['W2']['expectedCompletionDate'] = 'Expected Completion Date';
$Lang['W2']['exportAll'] = 'Export All';
$Lang['W2']['exportCSV'] = 'Export CSV';
$Lang['W2']['errMsg'] = 'Please fill in all the fields.';
$Lang['W2']['editTemplate']='Edit Template';
$Lang['W2']['enterText']='Enter text';
// f
$Lang['W2']['fullmark'] = 'Full';
// g
$Lang['W2']['generate'] = 'Generate';
$Lang['W2']['group(s)'] = 'group(s)';
$Lang['W2']['groupNow'] = 'Group now';
$Lang['W2']['grammar']= 'Grammar';
$Lang['W2']['giveSticker'] = 'Give Sticker(s)';
$Lang['W2']['grade'] = 'Grade';
$Lang['W2']['grammarAnalysis'] = 'Grammar Analysis';
$Lang['W2']['grammarAnalysisForEngine'] = 'Grammar<br />Analysis';
$Lang['W2']['genre'] = 'Genre';

// h
$Lang['W2']['home'] = 'Home';
$Lang['W2']['highlightedWordsPhrases'] = 'Highlighted Words/Phrases';
$Lang['W2']['highlightedWordsPhrasesWithBreak'] = 'Highlighted<br/>Words/Phrases';
$Lang['W2']['highestMark'] = 'Highest Mark';
// i
$Lang['W2']['imageFile'] = 'Image';
$Lang['W2']['incomplete'] = 'Incomplete';
$Lang['W2']['incompleteWriting'] = 'Incomplete Writing';
$Lang['W2']['introduction'] = 'Introduction';
$Lang['W2']['instruction'] = 'Instruction';
$Lang['W2']['issueAnalysis'] = 'Issue Analysis';


// j
$Lang['W2']['jsWarningAry']['deleteVocab'] = 'Are you sure you want to delete this vocab?';
$Lang['W2']['jsWarningAry']['deleteVocabGroup'] = 'All the related vocab(s) of this group will be deleted also. Are you sure you want to delete this vocab group?';
$Lang['W2']['jsWarningAry']['fullMarkMustBePositive'] = 'Full mark must be positive number.';
$Lang['W2']['jsWarningAry']['passMarkMustBePositive'] = 'Pass mark must be positive number.';
$Lang['W2']['jsWarningAry']['weighingFormat'] = 'Weight should be in Integer';
$Lang['W2']['jsWarningAry']['alertTotalWeight'] = 'Total weight should be equal to 100';
$Lang['W2']['jsWarningAry']['deleteAttachment'] = 'Are you sure you want to delete this attachment?';
$Lang['W2']['jsWarningAry']['deleteImage'] = 'Are you sure you want to delete this image?';
$Lang['W2']['jsWarningAry']['attachmentNameCannotBeBlank'] = 'Name of attachment cannot be blank.';
$Lang['W2']['jsWarningAry']['topicCannotBeBlank'] = 'Topic cannot be blank';
$Lang['W2']['jsWarningAry']['pleaseSelectReferenceType'] = 'Please select reference type.';
$Lang['W2']['jsWarningAry']['wrongImageFormat'] = 'Wrong image format.';
$Lang['W2']['jsWarningAry']['contentNotSaved'] = 'The data in this page has not been saved. Data will be lost once you go to another page. Do you want to continue?';
$Lang['W2']['jsWarningAry']['atLeast5RelatedScienceTerms'] = 'Please state at least 5 Related Science Terms.';
// k
$Lang['W2']['keywords'] = 'Keywords';
// l
$Lang['W2']['lastUpdateDate'] = 'Last Updated';
$Lang['W2']['lastPublished'] = 'Last Published';
$Lang['W2']['level'] = 'Level';
$Lang['W2']['lowestmark'] = 'Lowest';
$Lang['W2']['learningTips'] = 'Learning Tips';
$Lang['W2']['LSThemeAry'] = array();
$Lang['W2']['LSThemeAry'][] = 'Urban renewal';
$Lang['W2']['LSThemeAry'][] = 'Land use';
$Lang['W2']['LSThemeAry'][] = 'Wall effect';
$Lang['W2']['LSThemeAry'][] = 'Hong Kong\'s economy';
$Lang['W2']['LSThemeAry'][] = 'Migrant worker';
$Lang['W2']['LSThemeAry'][] = 'China\'s soft power';
$Lang['W2']['LSThemeAry'][] = 'The pollution problem in Hong Kong';
$Lang['W2']['LSThemeAry'][] = 'Global warming';
$Lang['W2']['LSThemeAry'][] = 'Globalization: Economic aspect';
$Lang['W2']['LSThemeAry'][] = 'Globalization: Cultural aspect';
$Lang['W2']['LSThemeAry'][] = 'Globalization: Governmental aspect';
$Lang['W2']['LSThemeAry'][] = 'Globalization: Non-governmental aspect';
$Lang['W2']['ls_engMyWriting'] = 'My Writing(s)';
// m
$Lang['W2']['management'] = 'Management';
$Lang['W2']['markingMethod'] = 'Marking method';
$Lang['W2']['markNow'] = 'Mark now';
$Lang['W2']['myKeywords'] = 'My Keywords';
$Lang['W2']['myWriting'] = 'My Writing(s)';
$Lang['W2']['myDraft'] = 'My Draft';
$Lang['W2']['markUsing'] = 'Mark Using';
$Lang['W2']['mark'] = 'Mark';
$Lang['W2']['marks'] = 'Marks';
$Lang['W2']['marker'] = 'Marker';
$Lang['W2']['meaningUsage'] = 'Meaning/Usage';
$Lang['W2']['mores'] = 'More...';
$Lang['W2']['moreVocabulary'] = 'More Vocabulary';
$Lang['W2']['moreVocabularyForEngine'] = 'More<br />Vocabulary';
$Lang['W2']['myApproach'] = 'My Approach';
$Lang['W2']['myConcept'] = 'My Concept';
$Lang['W2']['myMindMap'] = 'My Mind Map';
$Lang['W2']['myOutline'] = 'My Outline';

// n
$Lang['W2']['na'] = 'N/A';
$Lang['W2']['na_without_slash'] = 'NA';
$Lang['W2']['needCheckComment'] = 'Already Commented';
$Lang['W2']['needComment'] = 'Need Comment';
$Lang['W2']['needProvideComment'] = 'Pending for Comment';
$Lang['W2']['needToRedo'] = 'Need to redo';
$Lang['W2']['noDefaultConceptMap'] = 'No default concept map';
$Lang['W2']['newTemplate'] = 'New Template';
$Lang['W2']['name'] = 'Name';
$Lang['W2']['newParagraph'] = 'New Paragraph';
$Lang['W2']['newSection'] = 'New Section';
$Lang['W2']['note'] = 'Note';
$Lang['W2']['NumberOfStudents'] = 'No. of Student(s)';

// o
$Lang['W2']['optional'] = ' (Optional)';
$Lang['W2']['outline'] = 'Outline';
// p

$Lang['W2']['purpose'] = 'Purpose';
$Lang['W2']['PurposeAry'] = array();
$Lang['W2']['PurposeAry'][] = 'Summarize the key points of a science issue';
$Lang['W2']['PurposeAry'][] = 'Compare and contrast different aspects of a science issue';
$Lang['W2']['PurposeAry'][] = 'Report the findings and results of a science experiment, and observation';
$Lang['W2']['PurposeAry'][] = 'Seek funding to support advanced research';
$Lang['W2']['peer'] = 'Peer';
$Lang['W2']['peerComment'] = 'Peer\'s Comment';
$Lang['W2']['peerCommented'] = 'Peer Commented';
$Lang['W2']['peerMarking'] = 'Peer Marking';
$Lang['W2']['peerMarkingSettings'] = 'Peer marking settings';
$Lang['W2']['pointsOfContention'] = 'Points of Contention';
$Lang['W2']['powerConcept'] = 'PowerConcept';
$Lang['W2']['publish'] = 'Publish';
$Lang['W2']['published'] = 'Published';
$Lang['W2']['publishAll'] = 'Publish to All Schools';
$Lang['W2']['publishPart'] = 'Publish to School';
$Lang['W2']['passmark'] = 'Pass';
$Lang['W2']['pleaseSelect'] = '-- Please Select --';
$Lang['W2']['pressEnterToSeparateVocab'] = 'Please press Enter to separate each vocabulary.';
$Lang['W2']['powerBoard'] = 'PowerBoard';
$Lang['W2']['paperTemplate'] = 'Paper Template';
$Lang['W2']['paragraph'] = 'Paragraph';
$Lang['W2']['projAck'] = 'Project Acknowledgement';
$Lang['W2']['partSch'] = 'Participating Schools';
$Lang['W2']['projPart'] = 'Project Partners';
// q
$Lang['W2']['question'] = 'Question';
// r
$Lang['W2']['referenceMaterials'] = 'Reference Materials';
$Lang['W2']['relatedInformation'] = 'Related Information';
$Lang['W2']['relatedTermsAndInformation'] = 'Related Terms and Information';
$Lang['W2']['relatedScienceTerms'] = 'Related Science Terms<br />(State at least 5)';
$Lang['W2']['report'] = 'Report';
$Lang['W2']['returnMsgArr']['peerCommentUpdateSuccess'] = '1|=|Peer Comment Update Success';
$Lang['W2']['returnMsgArr']['peerCommentUpdateFailed'] = '0|=|Peer Comment Update Failed';
$Lang['W2']['returnMsgArr']['teacherCommentUpdateSuccess'] = '1|=|Teacher Comment Update Success';
$Lang['W2']['returnMsgArr']['teacherCommentUpdateFailed'] = '0|=|Teacher Comment Update Failed';
$Lang['W2']['returnMsgArr']['vocabGroupDeleteSuccess'] = '1|=|Vocab Group Delete Success';
$Lang['W2']['returnMsgArr']['vocabGroupDeleteFailed'] = '0|=|Vocab Group Delete Failed';
$Lang['W2']['returnMsgArr']['vocabGroupUpdateSuccess'] = '1|=|Vocab Group Update Success';
$Lang['W2']['returnMsgArr']['vocabGroupUpdateFailed'] = '0|=|Vocab Group Update Failed';
$Lang['W2']['returnMsgArr']['vocabUpdateSuccess'] = '1|=|Vocab Update Success';
$Lang['W2']['returnMsgArr']['vocabUpdateFailed'] = '0|=|Vocab Update Failed';
$Lang['W2']['returnMsgArr']['grammarUpdateSuccess'] = '1|=|Grammar Analysis Update Success';
$Lang['W2']['returnMsgArr']['grammarUpdateFailed'] = '0|=|Grammar Analysis Update Failed';
$Lang['W2']['returnMsgArr']['teacherAttachmentUpdateSuccess'] = '1|=|Teacher Attachment Update Success';
$Lang['W2']['returnMsgArr']['teacherAttachmentUpdateFailed'] = '0|=|Teacher Attachment Update Failed';
$Lang['W2']['returnMsgArr']['updateSuccess'] = '1|=|Record updated.';
$Lang['W2']['returnMsgArr']['updateFailed'] = '0|=|Record update failed.';
$Lang['W2']['regenerate'] = 'Regenerate';
$Lang['W2']['republish'] = 'Re-publish';
$Lang['W2']['resource'] = 'Resource';
$Lang['W2']['refWebsite'] = 'Reference Website';
$Lang['W2']['reference'] = 'Reference';
$Lang['W2']['revisedDraft'] = 'Revised Draft';
$Lang['W2']['RedoSent'] = 'Redo Request Sent';
$Lang['W2']['readMore'] = 'read more...';

// s
$Lang['W2']['saveAndNextStep'] = 'Save and Next Step';
$Lang['W2']['saveAsDraft'] = 'Save As Draft';
$Lang['W2']['sciHighlightInstruction'] = 'Please create a new group and new word in useful points/information section.';
$Lang['W2']['SciThemeAry'] = array();
$Lang['W2']['SciThemeAry'][] = 'Environmental';
$Lang['W2']['SciThemeAry'][] = 'Others';
$Lang['W2']['sciMyWriting'] = 'My Writing';
$Lang['W2']['section'] = 'Section';
$Lang['W2']['setMarker'] = 'Set Marker';
$Lang['W2']['settingTheScopeOfTheAnswer'] = 'Setting the scope of the answer';
$Lang['W2']['shareDate'] = 'Sharing Date';
$Lang['W2']['shareThisWriting'] = 'Share this writing';
$Lang['W2']['shareThisWritingHeader'] = 'share writing(s)';
$Lang['W2']['shareWritingShared'] = 'Writing shared';
$Lang['W2']['shareWritingUnshare'] = 'Unshare writing';
$Lang['W2']['shareRubrics'] = 'Share Rubrics';
$Lang['W2']['shareWriting'] = 'Share Writing';
$Lang['W2']['shortQuestions'] = 'Short Questions';
$Lang['W2']['showDefaultConceptMap'] = 'Show default concept map';
$Lang['W2']['showPeersNameToMarker'] = 'Show peer\'s name to marker';
$Lang['W2']['source'] = 'Source';
$Lang['W2']['steps'] = 'Steps';
$Lang['W2']['stepApprovalSetting'] = 'Steps approval';
$Lang['W2']['stepApprovalSettingNoApproval'] = 'No approval is required';
$Lang['W2']['stepApprovalSettingRequireApproval'] = 'Step(s) needs approval';
$Lang['W2']['student(s)'] = 'student(s)';
$Lang['W2']['studentName'] = 'Student Name';
$Lang['W2']['studentsAreDiviedInto'] = 'Students are divided into';
$Lang['W2']['submissionDate'] = 'Submission Date';
$Lang['W2']['submissionTime'] = 'Submission Time';
$Lang['W2']['submitted'] = 'Submitted';
$Lang['W2']['sampleWriting']='Sample Writing';
$Lang['W2']['step1_eng']='Step 1<br />(Examining the Qs)';
$Lang['W2']['step2_eng']='Step 2<br />(Brainstorming)';
$Lang['W2']['step3_eng']='Step 3<br />(Sample Writing)';
$Lang['W2']['step4_eng']='Step 4<br />(Drafting)';
$Lang['W2']['step5_eng']='Step 5<br />(Writing)';
$Lang['W2']['step1_ls_eng'] = 'Step 1<br />(Issue of Enquiry)';
$Lang['W2']['step2_ls_eng'] = 'Step 2<br />(Learning related terms<br /> and information)';
$Lang['W2']['step3_ls_eng'] = 'Step 3<br />(Brainstorming the scope<br /> of the answer)';
$Lang['W2']['step4_ls_eng'] = 'Step 4<br />(Drafting the answer)';
$Lang['W2']['step5_ls_eng'] = 'Step 5<br />(Answering the question)';

$Lang['W2']['step1_sci'] = 'Step 1<br />(Examining <br />the Question)';
$Lang['W2']['step2_sci'] = 'Step 2<br />(Extracting the<br />Relevant Information)';
$Lang['W2']['step3_sci'] = 'Step 3<br />(Organizing the<br />Relevant Information)';
$Lang['W2']['step4_sci'] = 'Step 4<br />(Related Type of<br />Essay and Vocabulary)';
$Lang['W2']['step5_sci'] = 'Step 5<br />(Drafting)';
$Lang['W2']['step6_sci'] = 'Step 6<br />(Writing)';

$Lang['W2']['step1Title']['eng'] = 'Examining the Question';
$Lang['W2']['step2Title']['eng'] = 'Brainstorming';
$Lang['W2']['step3Title']['eng'] = 'Related Text type and Vocabulary';
$Lang['W2']['step4Title']['eng'] = 'Drafting';
$Lang['W2']['step5Title']['eng'] = 'Writing';

$Lang['W2']['step1Title']['ls_eng'] = 'Issue of Enquiry';
$Lang['W2']['step2Title']['ls_eng'] = 'Learning related terms and information';
$Lang['W2']['step3Title']['ls_eng'] = 'Brainstorming the scope of the answer';
$Lang['W2']['step4Title']['ls_eng'] = 'Drafting the answer';
$Lang['W2']['step5Title']['ls_eng'] = 'Answering the question';
$Lang['W2']['step1Title']['sci'] = 'Examining the Question';
$Lang['W2']['step2Title']['sci'] = 'Extracting the Relevant Information';
$Lang['W2']['step3Title']['sci'] = 'Organizing the Relevant Information';
$Lang['W2']['step4Title']['sci'] = 'Related Type of Essay and Vocabulary';
$Lang['W2']['step5Title']['sci'] = 'Drafting';
$Lang['W2']['step6Title']['sci'] = 'Writing';
$Lang['W2']['step']='Step';
$Lang['W2']['status'] = 'Status';

$Lang['W2']['subjectFileCode']['chi'] = 'Chi';
$Lang['W2']['subjectFileCode']['eng'] = 'Eng';
$Lang['W2']['subjectFileCode']['sci'] = 'Sci';
$Lang['W2']['subjectFileCode']['ls'] = 'LS(Chi)';
$Lang['W2']['subjectFileCode']['ls_eng'] = 'LS(Eng)';

$Lang['W2']['suggestedByTeacher'] = 'Suggested By Teacher';
$Lang['W2']['selectSticker'] = 'Select Sticker';
$Lang['W2']['stickers'] = 'Sticker(s)';
$Lang['W2']['seeMore'] = 'See more..';
$Lang['W2']['scoreOutOfRange'] = 'The score is out of range';
$Lang['W2']['structure'] = 'Structure';
$Lang['W2']['systemDev'] = 'System Developer';

// t
$Lang['W2']['target'] = 'Target';
$Lang['W2']['teacherCommented'] = 'Teacher Commented';
$Lang['W2']['teacherCommentSoon'] = 'Teacher Comment Soon';
$Lang['W2']['teachersComment'] = 'Teacher\'s Comment';
$Lang['W2']['termsAndInformation'] = 'Terms and Information';
$Lang['W2']['theme'] = 'Theme';
$Lang['W2']['toBeEvaluated'] = 'To be Evaluated';
$Lang['W2']['toMark'] = 'To Mark';
$Lang['W2']['totalSubmit'] = 'Total No. of Attempt(s)';
$Lang['W2']['totalAttempt'] = 'Total No. of Attempt(s)';
$Lang['W2']['totalStudents'] = 'Total Students';
$Lang['W2']['topic'] = 'Topic';
$Lang['W2']['topicAnalysis'] = 'Analysis';
$Lang['W2']['topicAndTheme_eng'] = 'Topic & Theme';
$Lang['W2']['teacher'] = 'Teacher';
$Lang['W2']['teacherAttachment'] = 'Teacher\'s Attachment';
$Lang['W2']['teacherAttachmentWithBreak'] = 'Teacher\'s<br /> Attachment';
$Lang['W2']['textType'] = 'Text type';
$Lang['W2']['title'] = 'Title';

$Lang['W2']['template']['Wallpaper'] = 'Paper';
$Lang['W2']['template']['Clipart'] = 'Clipart';
$Lang['W2']['template']['Paper'] = 'Paper';
$Lang['W2']['template']['Email'] = 'Email';
$Lang['W2']['template']['Panda'] = 'Panda';

$Lang['W2']['template']['Squirrel'] = 'Squirrel';
$Lang['W2']['template']['Sketchbook'] = 'Sketchbook';
$Lang['W2']['template']['Robot'] = 'Robot';
$Lang['W2']['template']['Film'] = 'Film';
$Lang['W2']['template']['Leaflet'] = 'Leaflet';

$Lang['W2']['template']['Postbox'] = 'Postbox';
$Lang['W2']['template']['News'] = 'News';
$Lang['W2']['template']['Letter'] = 'Letter';
$Lang['W2']['template']['Friends'] = 'Friends';
$Lang['W2']['template']['Cooking'] = 'Cooking';

$Lang['W2']['template']['ChineseWriting'] = 'Chinese Writing';
$Lang['W2']['template']['ChineseTravel'] = 'Chinese Travel';
$Lang['W2']['template']['Travel'] = 'Travel';
$Lang['W2']['template']['City'] = 'City';
$Lang['W2']['template']['Coast'] = 'Coast';

// u
$Lang['W2']['uploadFileName'] = 'Name of attachment';
$Lang['W2']['uploadFileSource'] = 'Source';
$Lang['W2']['uploadFileCaption'] = 'My attachment (s)';
$Lang['W2']['uploadFileMore'] = 'Add';
//$Lang['W2']['uploadFileMore'] = 'Add more';##
$Lang['W2']['unselectalert'] = 'Please select writing scheme.';
$Lang['W2']['usefulExpressions'] = 'Useful expressions';

// v
$Lang['W2']['viewSource'] = 'View Source';
$Lang['W2']['vocabularyPool'] = 'My vocabulary pool';
$Lang['W2']['vocabulary']= 'Vocabulary';
$Lang['W2']['vocabSuggestedByTeacher']= 'Vocabulary Suggested By Teacher';
$Lang['W2']['vocabularies']= 'Vocabularies';
$Lang['W2']['versionAry'] = array();

// w
$Lang['W2']['waitingForApproval'] = 'Waiting for approval';
$Lang['W2']['waitingTeacherComment'] = 'Pending for Comment';
$Lang['W2']['warningMsgArr']['deleteWriting'] = 'All the submitted exercise also will be removed. Are you sure you want to delete?';
$Lang['W2']['warningMsgArr']['modifyPeerMarkingWarning'] = 'Modifying grouping will cause existing peer-marking results to be erased. Peer-marking has to be carried out again.';
$Lang['W2']['warningMsgArr']['mustUpdatePeerMarkingSettings'] = 'The peer marking settings must be updated since you have modified the student list.';
$Lang['W2']['warningMsgArr']['selectSchemeCode'] = 'Please select at least one subject';
$Lang['W2']['warningMsgArr']['selectStudentBeforeApplyingPeerMarking'] = 'Please select at least two students before applying Peer marking settings';
$Lang['W2']['warningMsgArr']['selectCat'] = 'Please select a category';
$Lang['W2']['warningMsgArr']['selectLevel'] = 'Please select a level';
$Lang['W2']['warningMsgArr']['selectPurpose'] = 'Please select a purpose';
$Lang['W2']['warningMsgArr']['selectTextType'] = 'Please select a text type';
$Lang['W2']['website'] = 'Website';
$Lang['W2']['markingWeight'] = 'Weight';
$Lang['W2']['WebSAMSCode'] = 'WebSAMS Code';
$Lang['W2']['wordsPhrases'] = 'Words/Phrases';
$Lang['W2']['writingEnd'] = '\'s Writing';
$Lang['W2']['writingSample'] = 'Writing Sample';

$Lang['W2']['confirmGSGameScoreSave'] = 'Are you sure to confirm the marks?';//20140529-w2gsgame
// x

// y
$Lang['W2']['yourComment'] = 'Your Comment';

// z

?>