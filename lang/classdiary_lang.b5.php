<?php
// editing by 
// UTF8 萬國碼

$Lang['ClassDiary']['ClassDiary'] = "課堂日誌";
$Lang['ClassDiary']['Management'] = "管理";
$Lang['ClassDiary']['Report'] = "報告";
$Lang['ClassDiary']['Statistics'] = "統計";
$Lang['ClassDiary']['ExportMailMerge'] = "匯出數據";

$Lang['ClassDiary']['RecordDate'] = "紀錄日期";
$Lang['ClassDiary']['Class'] = "班別";
$Lang['ClassDiary']['Creator'] = "建立者";
$Lang['ClassDiary']['LastModifiedBy'] = "最後修改者";
$Lang['ClassDiary']['LastModifiedDate'] = "最後修改日期";
$Lang['ClassDiary']['ViewRecordDetail'] = "檢視詳細紀錄";

$Lang['ClassDiary']['Cycle'] = "循環日";
$Lang['ClassDiary']['Date'] = "日期";
$Lang['ClassDiary']['Subject'] = "學科";
$Lang['ClassDiary']['MediumOfInstruction'] = "教學語言";
$Lang['ClassDiary']['Assignment'] = "功課";
$Lang['ClassDiary']['DateOfSubmission'] = "提交日期";
$Lang['ClassDiary']['ForgotToBringBooks'] = "忘記攜帶書本或者所需文具的學生";
$Lang['ClassDiary']['StudentsExcuseLessons'] = "允許缺席課堂的學生";
$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'] = "教學語言不適合的學生";
$Lang['ClassDiary']['Others'] = "其他";
$Lang['ClassDiary']['Absentee'] = "缺席學生";
$Lang['ClassDiary']['AM'] = "(上午)";
$Lang['ClassDiary']['PM'] = "(下午)";
$Lang['ClassDiary']['Late'] = "遲到學生";
$Lang['ClassDiary']['EarlyLeave'] = "早退學生";
$Lang['ClassDiary']['TeachersSignature'] = "教師簽署";
$Lang['ClassDiary']['Remarks'] = "備註";
$Lang['ClassDiary']['SignatureOfClassMonitor'] = "班督導簽署";
$Lang['ClassDiary']['SignatureOfClassTeacher'] = "班主任簽署";
$Lang['ClassDiary']['RemarksDescription'][] = "班督導必須保管好此日誌，和提示課目老師記下不恰當行為的學生並且簽署。";
$Lang['ClassDiary']['RemarksDescription'][] = "班主任必須交回此日誌到校務處。";
$Lang['ClassDiary']['RemarksDescription'][] = "學生事務委員會的負責老師會跟進此日誌紀錄。";
$Lang['ClassDiary']['RemarksDescription'][] = "記下學生編號，不要寫學生名字。";
$Lang['ClassDiary']['MediumOfInstructionMapping']['P'] = "普通話";
$Lang['ClassDiary']['MediumOfInstructionMapping']['E'] = "英語";
$Lang['ClassDiary']['MediumOfInstructionMapping']['C'] = "廣東話";
$Lang['ClassDiary']['MediumOfInstructionMapping']['B'] = "雙語";
$Lang['ClassDiary']['SelectStudents'] = "選取學生";
$Lang['ClassDiary']['Loading'] = "讀取中...";
$Lang['ClassDiary']['Lesson'] = "堂數";
$Lang['ClassDiary']['Lesson_Session'] = "節數";
$Lang['ClassDiary']['RecordType'] = "紀錄類別";
$Lang['ClassDiary']['CountTimesOfRecordType'] = "計算以下紀錄類別的次數";
$Lang['ClassDiary']['Times'] = "次數";
$Lang['ClassDiary']['ClassNumber'] = "學號";
$Lang['ClassDiary']['StudentName'] = "學生名稱";
$Lang['ClassDiary']['ShowDates'] = "顯示日期";
$Lang['ClassDiary']['Children'] = "子女";
$Lang['ClassDiary']['Edit_Class_Attendance'] = "編輯出席紀錄";
$Lang['ClassDiary']['Discard_Edit_Class_Attendance'] = "捨棄編輯出席紀錄";

$Lang['ClassDiary']['WarningMsg']['SelectClass'] = "請選擇班別。";
$Lang['ClassDiary']['WarningMsg']['InvalidDate'] = "請輸入正確的日期。";
$Lang['ClassDiary']['WarningMsg']['SelectSubject'] = "請選擇科目。";
$Lang['ClassDiary']['WarningMsg']['InputAssignment'] = "請輸入功課。";
$Lang['ClassDiary']['WarningMsg']['DuplicatedClassDiary'] = "班別 <!--CLASSNAME--> 於 <!--DATE--> 已經有日誌紀錄。";
$Lang['ClassDiary']['WarningMsg']['ConfirmDeleteClassDiary'] = "你確定要移除此班別日誌?";
$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneClass'] = "請選擇至少一個班別。";
$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneRecordType'] = "請選擇至少一種紀錄類別。";
$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneMediumOfInstruction'] = "請選擇至少一種教學語言。";
$Lang['ClassDiary']['WarningMsg']['FirstMonth'] = "若選擇了的日期橫跨兩個月，系統則會以第一個月份作為匯出數據的月份。";


$Lang['ClassDiary']['Month'][1] = "一";
$Lang['ClassDiary']['Month'][2] = "二";
$Lang['ClassDiary']['Month'][3] = "三";
$Lang['ClassDiary']['Month'][4] = "四";
$Lang['ClassDiary']['Month'][5] = "五";
$Lang['ClassDiary']['Month'][6] = "六";
$Lang['ClassDiary']['Month'][7] = "七";
$Lang['ClassDiary']['Month'][8] = "八";
$Lang['ClassDiary']['Month'][9] = "九";
$Lang['ClassDiary']['Month'][10] = "十";
$Lang['ClassDiary']['Month'][11] = "十一";
$Lang['ClassDiary']['Month'][12] = "十二";

?>