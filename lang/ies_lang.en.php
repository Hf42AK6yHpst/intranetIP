<?php
/** [Modification Log] Modifying By: connie
 * *******************************************
 * *******************************************
 */
//debug_R($_SESSION);
/*if($_SESSION['UserType'] == 2){
	include_once ("ies_lang.b5.php");
}
else{*/
$ies_lang['IES_TITLE']			= "LSIES";

$ies_lang['Group']				= "Group";
$ies_lang['Management']			= "Management";
$ies_lang['Settings']			= "Settings";
$ies_lang['StudentLicense']     = "License Authorization";
$ies_lang['StudentProgress']	= "Student Progress";

$Lang['_symbol']["colon"] = ":";
$Lang['_symbol']["questionMark"] = "?";
$Lang['_symbol']["dot"] = ".";
$Lang['_symbol']["exclamationMark"] = "!";
$Lang['_symbol']["comma"] = ",";

//en/b5/gb
$Lang['IES']['SchemeLang']["en"] = "ENG";
$Lang['IES']['SchemeLang']["b5"] = "中文";


// Should be compatible with IP25
// Please insert in alphabetical order of key

$Lang['IES']['Add'] = "Add";
$Lang['IES']['AddFromDefaultQuestion'] = "Add Default Question";
$Lang['IES']['AddNewQuestion'] = "Add New Question";
$Lang['IES']['AddRow'] = "Add Row";
$Lang['IES']['AlertMsg']["add"] = "Record Added.";
$Lang['IES']['AlertMsg']["delete"] = "Record Deleted.";
$Lang['IES']['AlertMsg']["delete_failed"] = "Record Delete Failed.";
$Lang['IES']['AlertMsg']["update"] = "Record Updated.";
$Lang['IES']['AlertMsg']["update_failed"] = "Update Failed";
$Lang['IES']['AllQuestionType'] = "All question type";
$Lang['IES']['AllScheme'] = "All Schemes";
$Lang['IES']['AllowToNextStep'] = "Allow to next step";
$Lang['IES']['AllowedToNextStep'] = "Allowed to next step";
$Lang['IES']['Answer'] = "Answer";
$Lang['IES']['AnswerNotSumbitted'] = "Answered (Not Submitted)";
$Lang['IES']['Approve'] = "Approved";
$Lang['IES']['Approved'] = "Approved";
$Lang['IES']['AreYouSureYouWouldLikeToDeleteThisFile'] = "Are you sure you would like to delete this file";
$Lang['IES']['AskedBy'] = "Posted by";
$Lang['IES']['AskedByStudent'] = "Asked by student";
$Lang['IES']['AskedByTeacher'] = "Asked by teaher";
$Lang['IES']['AssessTeacher']    = "Assessing Teacher";
$Lang['IES']['AssignTo'] = "Show at";
$Lang['IES']['AssignToAllScheme'] = "Show at all schemes";
$Lang['IES']['AssignToParticularStudent'] = "Show at student only";
$Lang['IES']['AssignToSomeScheme'] = "Show at more schemes";
$Lang['IES']['Attachment'] = "Attachment(s)";


$Lang['IES']['Back'] = "Back";
$Lang['IES']['BackToHomepage'] = "Back To Homepage";
$Lang['IES']['BasicSettings'] = "Basic Settings";
$Lang['IES']['Button']['SaveAndSetStep']    = "Save and set steps";

$Lang['IES']['CalSchemeMark'] = "Calculate Scheme Mark";
$Lang['IES']['Cancel'] = "Cancel";
$Lang['IES']['ChooseQueType'] = "Please choose question type";
$Lang['IES']['ChooseQuestion'] = "Choose question";
$Lang['IES']['ChooseOtherScheme'] = "Choose another Scheme";
$Lang['IES']['Class'] = "Class";
$Lang['IES']['ClassNumber'] = "Class number";
$Lang['IES']['ClickHere'] = "Click Here";
$Lang['IES']['Close'] = "Close";
$Lang['IES']['CombinationName'] = "Combination Name";
$Lang['IES']['CombinationAnalysis'] = "Combination Analysis";
$Lang['IES']['CombinationElement1'] = "Combination Element 1";
$Lang['IES']['CombinationElement2'] = "Combination Element 2";
$Lang['IES']['Content'] = "Content";            // Same key, diff content
$Lang['IES']['Copy1'] = "";
$Lang['IES']['CopyTaskAnswer'] = "Copy My Work";
$Lang['IES']['Create'] = "Create";
$Lang['IES']['CreateNew'] = "Create New";
$Lang['IES']['CreateNewQuestionnaire'] = "Create New Questionnaire";
$Lang['IES']['CreateNewObserve'] = "Create New Record Form";
$Lang['IES']['CreationDate'] = "Creation Date";
$Lang['IES']['CreatedBy'] = "Created by";
$Lang['IES']['Criteria'] = "Assessment Items";
$Lang['IES']['CriteriaDesc'] = "Criteria and Descriptors";
$Lang['IES']['CurrentRecords'] = "Current Records";
$Lang['IES']['CurrentSurveyAnswerNum'] = "There are #NUM# set(s) of answered questionnaire.";
$Lang['IES']['CommentCategory'] = "Comment Category";
$Lang['IES']['Chinese'] = "Chinese";
$Lang['IES']['CommentBy'] = "by";
$Lang['IES']['CopyFailed'] = "Copy Failed!";

$Lang['IES']['Date'] = "Date";
$Lang['IES']['DateLastUpdate'] = "Date of last update";
$Lang['IES']['DateSubmitted'] = "Date Submitted";
$Lang['IES']['DateSubmitted2'] = "Date Submitted";
$Lang['IES']['DefaultSurveyQuestion'] = "Default Survey Questions";
$Lang['IES']['DefaultLoadSurveyQuestion'] = "Load Default Question";
$Lang['IES']['DelSurveyAnswer'] = "All answered questionnaire must be deleted before editing.";
$Lang['IES']['Delete'] = "Delete";
$Lang['IES']['DeleteWarning']    = "Do you want to delete this?";
$Lang['IES']['Description']    = "Description";
$Lang['IES']['DetailSetupTitle']    = "Tool Tip Text";
$Lang['IES']['Difficulties'] = 'Difficulties';
$Lang['IES']['Done']    = "Done";
$Lang['IES']['DuplicateOptions'] = "Duplicate options are not allowed";
$Lang['IES']['DuplicateQuestions'] = "Duplicate questions are not allowed";
$Lang['IES']['Download'] = "Download";
$Lang['IES']['DiscoveryName'] = "Discovery Name";
$Lang['IES']['DiscoveryDetails'] = "Discovery Details";
$Lang['IES']['DeleteStudentWarning'] = "If any student is deleted from the old scheme, his/her records may be deleted as well.";

$Lang['IES']['Edit'] = "Edit";
$Lang['IES']['Enter'] = "Enter";
$Lang['IES']['Enable'] = "Enable";
$Lang['IES']['ExplorationPlan'] = "Exploration Plan";
$Lang['IES']['English'] = "English";

$Lang['IES']['Disable'] = "Disabled";

$Lang['IES']['ExistingScheme']= "Existing Scheme";
$Lang['IES']['Email']['To'] = "To";
$Lang['IES']['Email']['Subject'] = "Subject";
$Lang['IES']['Email']['Message'] = "Message";
$Lang['IES']['Email']['IntranetRecipient(s)'] = "Intranet recipient(s)";
$Lang['IES']['Email']['ExternalRecipient(s)'] = "External recipient(s)";
$Lang['IES']['Email']['AddEmail(s)Here'] = "Add email(s) here";
$Lang['IES']['Email']['QuestionnaireLink_1'] = "The link of the questionnaire";
$Lang['IES']['Email']['QuestionnaireLink_2'] = "Click here to insert a weblink for your questionnaire.";
$Lang['IES']['Email']['string1'] = "You do not have the right to send email. You can select recipients or their multiple email addresses directly with ';' to separate addresses.";
$Lang['IES']['Email']['string2'] = "Note: If you click on 'Save', the system will only save the email message but not the recipients and their email addresses. <br/>If you do not wish to send the email, please do not enter recipients. ";
$Lang['IES']['Email']['Gamma']['SelectFromInternalRecipient'] = "Select from internal recipient";
$Lang['IES']['Email']['Gamma']['SelectFromExternalRecipient'] = "Select from external recipient";
$Lang['IES']['Email']['Gamma']['SelectFromExternalRecipientGroup'] = "Select from external recipient group";
$Lang['IES']['Email']['Gamma']['SelectFromInternalRecipientGroup'] = "Select from internal recipient group";
$Lang['IES']['Email']['IMail']['i_frontpage_campusmail_choose'] = "Choose Recipient(s)";
$Lang['IES']['Email']['IMail']['i_CampusMail_New_SelectFromAlias'] = "Select From Alias Group";
$Lang['IES']['Email']['IMail']['i_frontpage_campusmail_remove'] = "Remove";
$Lang['IES']['Email']['IMail']['FieldTitle']['ChooseFromExternalRecipient'] = "Choose Recipient(s) From External Recipient";
$Lang['IES']['Email']['IMail']['FieldTitle']['ChooseFromInternalRecipient'] = "Choose Recipient(s) From Internal Recipient";
$Lang['IES']['Email']['ReceivedDate'] = "Received Date";


$Lang['IES']['FAQ']    = "FAQ";
$Lang['IES']['FAQDesc']    = $Lang['IES']['FAQ'];
$Lang['IES']['FAQDesc_org'] = "Frequently asked questions and their answers are listed here.";
$Lang['IES']['File'] = "File";
$Lang['IES']['Finished'] = "Completed";
$Lang['IES']['FinishedSurvey'] = "List of completed questionnaires";
$Lang['IES']['FinishedObserve'] = "List of completed record forms";
$Lang['IES']['FinishedSurveyType2'] = "List of completed report";
$Lang['IES']['FolderDesc']    = "My Files";
$Lang['IES']['FolderDesc_org'] = "Save your working files related to your study here.";
$Lang['IES']['From'] = "From";
$Lang['IES']['FillInTheQuestionnaire'] = 'Fill in the questionnaire';
$Lang['IES']['FillInTheRespondent'] = 'Please fill in the respondent’s name!';

$Lang['IES']['GiveMark'] = "Marking";

$Lang['IES']['HKDSE'] = "Hong Kong Diploma of Secondary Education";
$Lang['IES']['HandInCommented'] = "Teacher Commented";
$Lang['IES']['HandInCommentedMarked'] = "Teacher Commented / Marked";
$Lang['IES']['HandInIn'] = "Student New Submitted";
$Lang['IES']['HandInMarked'] = "Teacher Marked";
$Lang['IES']['HandInSubmit'] = "Student Submitted";
$Lang['IES']['HandinAnswer'] = "Submitted Answer";
$Lang['IES']['Hide']	= "Hide";
$Lang['IES']['HoldCtrlButton'] = "Hold CTRL &amp; click a list item to select multiple items";

$Lang['IES']['IESTitle'] = "Independent Enquiry Study";
$Lang['IES']['Instructor'] = "Instructor";
$Lang['IES']['Interview'] = "In-depth interview";
$Lang['IES']['IESReport'] = "Independent Enquiry Study Report";
$Lang['IES']['IsSubmitQuestionnaire'] = "Are you going to submit this questionnaire?";
$Lang['IES']['IncorrectRespondent'] = 'The name of Respondent is incorrect! Please re-enter!';
$Lang['IES']['InputSchemeTitle'] = 'Please input the Scheme Title!';

$Lang['IES']['LastModifiedDate'] = "Last Modified Date";
$Lang['IES']['LastStep'] = "Last Step";
$Lang['IES']['LastSubmissionDate']    = "Last Submission Date";
$Lang['IES']['Level']    = "Level";
$Lang['IES']['LiberalStudies'] = "Liberal Studies";
$Lang['IES']['Loading'] = "Loading";

$Lang['IES']['MainWork'] = 'Main work';
$Lang['IES']['Mark'] = "Marks";
$Lang['IES']['MarkingSettings'] = "Marking Settings";
$Lang['IES']['MarkingSettingsInstruction'] = "Changing any settings in the page will cause students' existing marks to be affected immediately.";
$Lang['IES']['MaxScore'] = "Max. Marks";
$Lang['IES']['MaxScoreRestrictedByRubric'] = "Max. marks is restricted by rubrics";
$Lang['IES']['MemberTitle']    = "Members";
$Lang['IES']['MoreScheme'] = "More scheme";
$Lang['IES']['MoveDown'] = "Move Up";
$Lang['IES']['MoveUp'] = "Move Down";
$Lang['IES']['Manage'] = "Manage";

$Lang['IES']['NAinPreview']    = "Not Available in Preview Mode.";
$Lang['IES']['NeedTeacherApproval']    = "Student needs teacher's approval before compiling this part";
$Lang['IES']['NewComment'] = "Comment";
$Lang['IES']['NewDocSection'] = "New Document Section";
$Lang['IES']['NewRubric'] = "Rubric";
$Lang['IES']['NewSchemeNotes']['PurchasedSeparately'] = "Purchased separately";
$Lang['IES']['NewSchemeNotes']['SchemeLanguageCanOnlyBeSetOnce'] = "Scheme language can only be set once.";
$Lang['IES']['NewSchemeNotes']['Stage1Functionalities'] = "For the moment, only Stage 1 and 2 functionalities are usable for English Schemes.";
$Lang['IES']['NextStep'] = "Next Step";
$Lang['IES']['NotApplicable']    = "Not Applicable";
$Lang['IES']['NoRecordAtThisMoment']    = "No record at this moment";
$Lang['IES']['NotSent']    = "Not Sent";
$Lang['IES']['NotYetAnswered']    = "Not yet answered";
$Lang['IES']['NotYetReplied'] = "Not yet replied";
$Lang['IES']['NoteDesc']    = "Reflection Log";
$Lang['IES']['NoteDesc_org'] = "Note down your ideas and thoughts in the course of your study so as to help yourself write up your learning reflection upon the completion of each stage.";
$Lang['IES']['NumberOfOptions'] = "Number of Options";
$Lang['IES']['NumberOfQuestions'] = "Number of Question(s)";
$Lang['IES']['NumberOfCategories'] = "# of Categories";
$Lang['IES']['NoRightToSendEmail'] = "You do not have the right to send email.";

$Lang['IES']['Observe'] = "Field observation";
$Lang['IES']['OldScheme'] = "Old Scheme";

$Lang['IES']['PastRecords'] = "Past Records";
$Lang['IES']['PleaseFillInCombinationName'] = "Please fill in combination name";
$Lang['IES']['PleaseFillInTheWorkPlanSchedule'] = "Please fill in the work plan schedule:";
$Lang['IES']['PleaseRedo'] = "Please Redo";
$Lang['IES']['Progress'] = "Progress";
$Lang['IES']['PlsSelectStudent'] = "Please select the student(s)!";

$Lang['IES']['Question'] = "Question";
$Lang['IES']['QuestionDate'] = "Creation Date";
$Lang['IES']['QuestionFormat'] = "Format";
$Lang['IES']['QuestionTitle'] = "Question / Title";
$Lang['IES']['Questionnaire'] = "Questionnaire";
$Lang['IES']['QuestionnaireDescription'] = "Description";
$Lang['IES']['Questionnaire_ObserveRemark1'] = 'Objective of the field observation';
$Lang['IES']['Questionnaire_ObserveRemark2'] = 'Location of the field observation';
$Lang['IES']['Questionnaire_ObserveRemark3'] = 'Mode of the field observation';
$Lang['IES']['Questionnaire_ObserveRemark4'] = 'Research object';
$Lang['IES']['Questionnaire_ObserveTitle'] = 'Title';

$Lang['IES']['Questionnaire_InterviewRemark1'] = 'Location of the interview';
$Lang['IES']['Questionnaire_InterviewRemark2'] = 'Date of the interview';
$Lang['IES']['Questionnaire_InterviewRemark3'] = 'Duration of the interview';
$Lang['IES']['Questionnaire_InterviewRemark4'] = 'Mode of the interview';
$Lang['IES']['Questionnaire_InterviewTitle'] = 'Title';

$Lang['IES']['QuestionnaireEnddate'] = "End Date";
$Lang['IES']['QuestionnaireQuestion'] = "Question";
$Lang['IES']['QuestionnaireQuestionType2'] = "Content";
$Lang['IES']['QuestionnaireTitle'] = "Questionnaire Title";

$Lang['IES']['Redo'] = "Redo";          // Same key, diff content
$Lang['IES']['RedoSent'] = "Redo Request Sent";
$Lang['IES']['Remark'] = "Remark";
$Lang['IES']['Replied'] = "Replied";
$Lang['IES']['Reset'] = "Reset";


$Lang['IES']['Save'] = "Save";
$Lang['IES']['SaveAndNextStep'] = "Save and Proceed";
$Lang['IES']['Scheme']    = "Scheme";
$Lang['IES']['SchemeLangCaption']    = "Scheme Lang";
$Lang['IES']['SchemeTitle']    = "Scheme Title";
$Lang['IES']['SchemeTitleDuplicate'] = "Scheme Title Duplicated!";
$Lang['IES']['Schemes']    = "scheme(s)";
$Lang['IES']['SchoolName'] = "School Name";
$Lang['IES']['SchoolWork'] = "Result";
$Lang['IES']['ScoreOutRange'] = 'Score out of range';
$Lang['IES']['ScoreRange'] = "Score Range";
$Lang['IES']['SearchScheme'] = "Search Scheme";
$Lang['IES']['SectionTitle']    = "Section Title";
$Lang['IES']['SelectedScheme'] = "Selected Scheme";
$Lang['IES']['Sent']    = "Sent";
$Lang['IES']['Select']    = "Select";
$Lang['IES']['ShowAllResponse'] = "Show All Responses";
$Lang['IES']['ShowLatestResponse'] = "Show Latest Response";
$Lang['IES']['ShowRecord'] = "Show Record";
$Lang['IES']['HideRecord'] = "Hide Record";
$Lang['IES']['Solution'] = 'Solution';
$Lang['IES']['Stage']    = "Stage";
$Lang['IES']['StageAndCriteria']    = "Stage and Criteria";
$Lang['IES']['StageDesc_seq1']= 	"<br />The Project Proposal takes up 25% of the total weighting of the IES Project, in which 'Task' and 'Process' will have an equal share.<br /><br />The 'Task' for writing up a project proposal at Stage 1 includes:<ul><li>Setting the enquiry topic and objectives</li><li>Collecting background information</li><li>Constructing focus questions</li><li>Formulating a work plan</li></ul><br /><br />As for the 'Process', the following aspects will be taken into account for assessment:<ul><li>Independent thinking skills</li><li>Communication skills</li><li>Effort</li></ul>";
$Lang['IES']['StageDesc_seq2']= "<br />In the IES Project, the record of the Data Collection process takes up 25% of the total weighting , in which both ‘Task’ and ‘Process’ will be assessed.<br /><br />The ‘Task’ for Stage 2 includes:<ul><li>elaborating and revising the enquiry scope and topic</li><li>stating the tool(s) and method(s) for data collection </li><li>justifying the rationale for choosing such tool(s) and method(s)</li><li>providing progress records or research diaries for the data collection process</li><li>explaining the usefulness and relevancy of the data collected for the topic for enquiry</li></ul><br /><br />As for the ‘Process’, the following aspects will be taken into account for assessment:<ul><li>Independent thinking skills</li><li>Communication skills</li><li>Effort</li></ul><br />‘Task’ and ‘Process’ will have an equal share.<br /><br />";
$Lang['IES']['StageDesc_seq3']= "<br />On completion of Stage 2, you should have finished data collection and gathered useful facts and figures for writing up the enquiry report. At Stage 3, you have to integrate the information of the previous two stages, elaborate and write up an enquiry report within 1500 to 4000 words. The contents will cover enquiry objectives, focus questions, enquiry method, research outcomes, analysis and conclusion. The weighting of Stage 3 is 50% of the total score of IES in which ‘Task’ and ‘Process’ will have an equal share.<br /><br />The ‘Task’ for Stage 3 includes:<ul><li>drawing meaningful inferences according to the data collected at Stage 2</li><li>describing a strict research framework and stating clearly how data was obtained</li><li>applying multi-perspective thinking to analyse the enquiry issue in depth</li><li>bringing up reasonable arguments and possible solutions</li><li>summarizing self-reflection on the enquiry</li></ul><br /><br />As for ‘Process’, the following aspects will be considered for assessment:<ul><li>Independent thinking skills</li><li>Communication skills</li><li>Effort</li></ul><br />‘Task’ and ‘Process’ will have an equal share.<br /><br />";
$Lang['IES']['StageDesc_seqDefault'] = "獨立專題探究內容";
$Lang['IES']['StageSubmitStatus']    = "Stage Submission Status";
$Lang['IES']['Status'] = "Status";
$Lang['IES']['StatusSubmitted'] = "Submit Status";
$Lang['IES']['Step'] = "Step";
$Lang['IES']['StepNum'] = "Number of steps";
$Lang['IES']['StepSettingTitle']    = "Step Setting";
$Lang['IES']['StepSettings']    = "Step Settings";
$Lang['IES']['Steps']    = "Workflow";
$Lang['IES']['Student']    = "Student";
$Lang['IES']['StudentAnswer']    = "Student's Answer";
$Lang['IES']['StudentInputData'] = "View Steps";
$Lang['IES']['StudentInputData2'] = "Student Input Data";
$Lang['IES']['StudentInputData3'] = 'View Tasks';
$Lang['IES']['StudentName'] = "Name of student";
$Lang['IES']['StudentNumber']    = "Number of Students";
$Lang['IES']['StudentSubmitData'] = "Student Input";
$Lang['IES']['SubmissionDeadline'] = "Submission Deadline";      // Duplicated: $Lang['IES']['SubmitDeadLine']
$Lang['IES']['Submit'] = "Submit";
$Lang['IES']['SubmitAndMore'] = "Submit & Add More";
$Lang['IES']['SubmitDate'] = "Submit Date";
$Lang['IES']['SubmitDeadLine'] = "Submission Deadline";          // Duplicated: $Lang['IES']['SubmissionDeadline']
$Lang['IES']['SubmitReport'] = "Submit Report";
$Lang['IES']['SubmitReportDetail'] = "整理好你的課業後，可在此上載所有檔案（如文字檔、報表、簡報、聲檔等），供老師評分。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;請注意，呈交課業後，除非老師發回重做，否則不能修改。";
$Lang['IES']['SubmitRequest'] = "Submit";
$Lang['IES']['Submitted'] = "Submitted";
$Lang['IES']['Submitting'] = "Submitting";
$Lang['IES']['SubmittingFilesDeleted'] = "Student has deleted the previously uploaded file(s)";
$Lang['IES']['SubmittingNoMarking'] = "Student has not submitted the homework (not yet pressed \"submit\").  You cannot give mark at this moment.";
$Lang['IES']['Survey'] = "Survey";
$Lang['IES']['SurveyCreated'] = "Created";
$Lang['IES']['Send'] = "Send";
$Lang['IES']['Submission'] = "Submission";
$Lang['IES']['SelectExistScheme'] ="Please select an Existing Scheme!";

$Lang['IES']['Task'] = "Task";
$Lang['IES']['TaskAnswer'] = "My Work";
$Lang['IES']['TeacherComment'] = "Teacher's Comment";
$Lang['IES']['TeacherRead'] = "Teacher Read";
$Lang['IES']['TeachingTeacher']    = "Teaching Teacher";
$Lang['IES']['TheInformationYouHaveEntered'] = "Your previous entry/entries";
$Lang['IES']['TextDesc']    = "My Source";
$Lang['IES']['TheSubmissionPeriodIsOvered'] = "The submission period is over";//"The Submission Period is Over";
$Lang['IES']['TidyUp'] = "";
$Lang['IES']['AddFigureThickboxLink'] = "How to include graphs in the report?";
$Lang['IES']['TidyUpDetail'] = "You may like to edit the draft task you have just exported before submission.";
$Lang['IES']['Topic'] = "Topic for Enquiry";
$Lang['IES']['TotalMark'] = "Marks awarded";
$Lang['IES']['TaskAnswerNotAvailable'] = "Answer display is not available for this task.";
$Lang['IES']['TransferStudent'] = "Are you sure you want to transfer the students?";

$Lang['IES']['UploadFiles'] = "Upload Files";
$Lang['IES']['UseThisFunction']    = "Use This Function";

$Lang['IES']['Warning']['AtLeastOneDescriptor']    = "Please leave at least one descriptor.";
$Lang['IES']['Warning']['FillInSchemeName']    = "Please fill in scheme name";
$Lang['IES']['Warning']['FillInSectionTitle']    = "Please fill in section title";
$Lang['IES']['Warning']['MaxScoreNumeric']    = "Max. Score must be a number";
$Lang['IES']['Warning']['WeightNumeric']    = "Weight must be a number";
$Lang['IES']['Warning']['DeleteCategory']    = "Please note that all comments under this category will also be deleted!";
$Lang['IES']['Warning']['CompareEqualQuestion']    = "Cannot compare two same questions. Please select again!";
$Lang['IES']['Weight'] = "Weight";
$Lang['IES']['WordDocSettings'] = "Word Document Export Settings";
//$Lang['IES']['WorkItem'] = "Task items";
//$Lang['IES']['WorkPurpose'] = "Task objectives";
$Lang['IES']['WorksheetDesc']    = "Worksheet";
$Lang['IES']['WorksheetDesc_org'] = "Assignments to or from teachers.";
$Lang['IES']['WantToUseThisLangContactUs'] = "Want to use this lang? Contact us...";
$Lang['IES']['stepSetup']    = "Settings";




//$Lang['IES']['Redo'] = "重做";              // Same key, diff content
//$Lang['IES']['content'] = "目錄";             // Same key, diff content

# Worksheet
$Lang['IES']['EndDate'] = "End Date";
$Lang['IES']['NeedComment'] = "Pending for Comment";
$Lang['IES']['StartDate'] = "Start Date";
$Lang['IES']['StartEndDate'] = "Start & End Date";
$Lang['IES']['StudentFile'] = "Student File(s)";
$Lang['IEs']['TeacherComment'] = "Teacher Comment";
$Lang['IES']['Title'] = "Title";
$Lang['IES']['UploadedAmount'] = "Uploaded Amount";
$Lang['IES']['UploadFile'] = "Upload File";
$Lang['IES']['Worksheet'] = $Lang['IES']['WorksheetDesc'];
$Lang['IES']['WorksheetAmount'] = "Worksheet Amount";
$Lang['IES']['WorksheetFile'] = "Description File";
$Lang['IES']['WorksheetTitle'] = "Worksheet Title";
$Lang['IES']['WsUploadFile'] = "Uploaded File(s)";

# CommentBank
$Lang['IES']['CommentBank'] = "Comment Bank";
$Lang['IES']['Comment'] = "Comment";
$Lang['IES']['AddComment'] = "New Comment";
$Lang['IES']['AddCommentCategory'] = "New Comment Category";
$Lang['IES']['CommentWarning'] = "Comment blanked";
$Lang['IES']['CommentCategoryWarning'] = "Comment Category blanked";
$Lang['IES']['Save2'] = "Save";
$Lang['IES']['Cancel2'] = "Cancel";
$Lang['IES']['Delete2']  = "Delete";
$Lang['IES']['Student2'] = "Student";
$Lang['IES']['Reviews'] = "Teacher Comment";

# Start: tempararily set set as chinese
$Lang['IES']['Date'] = "Date";
$Lang['IES']['WorkItem'] = "Task items";
$Lang['IES']['Content'] = "Content";
$Lang['IES']['WorkPurpose'] = "Task objectives";
$Lang['IES']['Step'] = "Step";
$Lang['IES']['Student'] = "Student";
$Lang['IES']['PleaseFillInTheWorkPlanSchedule'] = "Please fill in the work plan schedule";
//$Lang['IES']['AddRow'] = "增加一行";
$Lang['IES']['LastModifiedDate'] = "Last modified date";
$Lang['IES']['PastRecords'] = "Past records";
$Lang['IES']['CurrentRecords'] = "Latest Record";
$Lang['IES']['File'] = "File";
$Lang['IES']['Status'] = "Status";
$Lang['IES']['ExplorationPlan'] = "Project Proposal";
$Lang['IES']['SubmissionDeadline'] = "Submission Deadline";
$Lang['IES']['BackToHomepage'] = "Back to Last Page";
$Lang['IES']['Submitting'] = "Submitting";
$Lang['IES']['Submitted'] = "Submitted";
$Lang['IES']['Approved'] = "已批改";
$Lang['IES']['TeacherRead'] = "Teacher Read";
$Lang['IES']['Redo'] = "Redo";
$Lang['IES']['AreYouSureYouWouldLikeToDeleteThisFile'] = "Are you sure you would like to delete this file";
//$Lang['IES']['Delete'] = "刪除";
$Lang['IES']['Submit'] = "Submit";
$Lang['IES']['PleaseRedo'] = "Please Redo";
$Lang['IES']['UploadFiles'] = "Upload Files";
$Lang['IES']['Loading'] = "Loading";
$Lang['IES']['Save'] = "Save";
$Lang['IES']['Reset'] = "Reset";

$Lang['IES']['LastStep'] = "Last Step";
$Lang['IES']['NextStep'] = "Next Step";
$Lang['IES']['SaveAndNextStep'] = "Save and Next Step";
$Lang['IES']['Cancel'] = "Cancel";
$Lang['IES']['TextDesc_org'] = "Enter the source, contents and citations, if any, for future reference.";

$Lang['IES']['SubmitReportDetail'] = "When you have tidied up the 'Task', upload all files (such as Word documents, Excel tables, PowerPoint slides and sound files) for assessment.<br \>Please note that you cannot revise the task after submission unless teachers ask you to re-do it. ";


$Lang['IES']['TeacherMark'] = "Mark";
$Lang['IES']['UploadSettings'] = "Upload Settings";
$Lang['IES']['UploadFailed'] = "Upload Failed";
$Lang['IES']['ExceedMaxSize'] = "May Due To Reached The Maximum File Size For This Module";

#Rubric
$Lang['IES']['Rubric'] = "Rubric";
$Lang['IES']['RubricSetting'] = "Rubric";
$Lang['IES']['RubricSetupTitle'] = $Lang['IES']['RubricSetting']." Settings";

#Start: bibi<============================== This is very special, some requires user can see both chinese and eng, no need to set to EN
$Lang['IES']['language'] = "Language";
$Lang['IES']['newbibi'] = "Add Entry";
$Lang['IES']['bibilang'] = "Language Format";

/********old bibi *************/
$Lang['IES']['language'] = "Language";
$Lang['IES']['true'] = "Yes";
$Lang['IES']['false'] = "No";
$Lang['IES']['displaytobibi'] = "Display in reference information";

$Lang['IES']['chibook']['fieldname'] = "書籍";
$Lang['IES']['chinew']['fieldname'] = "期刊、雜誌或報紙";
$Lang['IES']['chinet']['fieldname'] = "網上資源";
$Lang['IES']['manual']['fieldname'] = "其他";
$Lang['IES']['engbook']['fieldname'] = "English Books, Reports";
$Lang['IES']['engnew']['fieldname'] = "English Journals, Magazines, newspapers in print format";
$Lang['IES']['engnet']['fieldname'] = "English Online resources";
/********old bibi END*************/


/********new bibi *************/
$Lang['IES']['oldData']="Old Data<br/>(The old data cannot be updated. Please delete the old one and input new data if necessary.)";

$Lang['IES']['eng']['Yes']='Yes';
$Lang['IES']['eng']['No']='No';
$Lang['IES']['chi']['Yes']='是';
$Lang['IES']['chi']['No']='否';

$Lang['IES']['language_new'] = "Text Language";
$Lang['IES']['ForSelfRef'] = "For self-reference"; //true
$Lang['IES']['ForDispInBibi'] = "For display in Bibliography";//false
$Lang['IES']['displaytobibi_new'] = "Use of this Entry";

$Lang['IES']['oldDataChi']['fieldname'] = "舊有資料";
$Lang['IES']['chibook_new']['fieldname'] = "書籍";
$Lang['IES']['chireport_new']['fieldname'] = "報告";
$Lang['IES']['chijournals_new']['fieldname'] = "期刊";
$Lang['IES']['chimagazine_new']['fieldname'] = "雜誌";
$Lang['IES']['chinew_new']['fieldname'] = "報紙";
$Lang['IES']['chinet_new']['fieldname'] = "網上資源";
$Lang['IES']['chiother_new']['fieldname'] = "其他";

$Lang['IES']['oldDataEng']['fieldname'] = "Old Data";
$Lang['IES']['engbook_new']['fieldname'] = "Books";
$Lang['IES']['engreport_new']['fieldname'] = "Reports";
$Lang['IES']['engjournals_new']['fieldname'] = "Journals";
$Lang['IES']['engmagazine_new']['fieldname'] = "Magazines";
$Lang['IES']['engnew_new']['fieldname'] = "Newspapers";
$Lang['IES']['engnet_new']['fieldname'] = "Online Resources";
$Lang['IES']['engother_new']['fieldname'] = "Others";
/********new bibi END*************/



/********************
 * old bibi details *
 ********************/

$Lang['IES']['chibook']['fieldname'] = "書籍";
$Lang['IES']['chinew']['fieldname'] = "期刊、雜誌或報紙";
$Lang['IES']['chinet']['fieldname'] = "網上資源";
$Lang['IES']['manual']['fieldname'] = "其他";
$Lang['IES']['engbook']['fieldname'] = "English Books, Reports";
$Lang['IES']['engnew']['fieldname'] = "English Journals, Magazines, newspapers in print format";
$Lang['IES']['engnet']['fieldname'] = "English Online resources";


$Lang['IES']['chibook']['author'] = "作者姓名";
$Lang['IES']['chibook']['translator'] = "譯者姓名";
$Lang['IES']['chibook']['editor'] = "編者姓名";
$Lang['IES']['chibook']['year'] = "年份";
$Lang['IES']['chibook']['bkname'] = "書名";
$Lang['IES']['chibook']['articlename'] = "文章名稱";
$Lang['IES']['chibook']['edition'] = "版數(如再版)";
$Lang['IES']['chibook']['location'] = "出版地點";
$Lang['IES']['chibook']['pub'] = "出版社";
$Lang['IES']['chibook']['remark'] = "備註";

$Lang['IES']['chinew']['author'] = "作者姓名";
$Lang['IES']['chinew']['date'] = "日期";
$Lang['IES']['chinew']['bkname'] = "刊物名稱";
$Lang['IES']['chinew']['articlename'] = "文章名稱";
$Lang['IES']['chinew']['fnum'] = "期數";
$Lang['IES']['chinew']['pnum'] = "頁數";
$Lang['IES']['chinew']['remark'] = "備註";

$Lang['IES']['chinet']['author'] = "作者姓名";
$Lang['IES']['chinet']['year'] = "年份";
$Lang['IES']['chinet']['bkname'] = "刊物名稱";
$Lang['IES']['chinet']['articlename'] = "文章名稱";
$Lang['IES']['chinet']['date'] = "檢索日期";
$Lang['IES']['chinet']['website'] = "網址";
$Lang['IES']['chinet']['remark'] = "備註";

$Lang['IES']['manual']['manual'] = '自行輸入';

$Lang['IES']['engbook']['author'] = 'Author Name';
$Lang['IES']['engbook']['year'] = 'Year of Publication';
$Lang['IES']['engbook']['articlename'] = 'Title of Chapter/Article';
$Lang['IES']['engbook']['editor'] = 'Editor Name';
$Lang['IES']['engbook']['title'] = 'Title of Work';
$Lang['IES']['engbook']['edition'] = 'Edition';
$Lang['IES']['engbook']['pnum'] = 'Page number';
$Lang['IES']['engbook']['location'] = 'Location';
$Lang['IES']['engbook']['pub'] = 'Publisher';
$Lang['IES']['engbook']['remark'] = "Remark";

$Lang['IES']['engnew']['author'] = 'Author Name';
$Lang['IES']['engnew']['year'] = 'Year of Publication';
$Lang['IES']['engnew']['articlename'] = 'Title of Article';
$Lang['IES']['engnew']['journal'] = 'Title of Journal';
$Lang['IES']['engnew']['vnum'] = 'Volume Number';
$Lang['IES']['engnew']['inum'] = 'Issue Number';
$Lang['IES']['engnew']['pnum'] = 'Page number';
$Lang['IES']['engnew']['remark'] = "Remark";

$Lang['IES']['engnet']['author'] = 'Author Name';
$Lang['IES']['engnet']['date'] = 'Date';
$Lang['IES']['engnet']['title'] = 'Title of Work';
$Lang['IES']['engnet']['bdate'] = 'Last browsed date (month dd, yyyy)';
$Lang['IES']['engnet']['website'] = 'Web address';
$Lang['IES']['engnet']['remark'] = "Remark";

/***********old bibi details END *************/


/********************
 * new bibi details *
 ********************/
 
$Lang['IES']['chibook_new']['authorChi'] = "作者姓名";
$Lang['IES']['chibook_new']['authorEng'] = "Author Name";
$Lang['IES']['chibook_new']['translator'] = "譯者姓名";
$Lang['IES']['chibook_new']['editor'] = "編者姓名";
$Lang['IES']['chibook_new']['year'] = "出版年份";
$Lang['IES']['chibook_new']['bkname'] = "書名";
$Lang['IES']['chibook_new']['articlename'] = "文章名稱";
$Lang['IES']['chibook_new']['edition'] = "版數(如再版)";
$Lang['IES']['chibook_new']['pnum'] = "頁數";
$Lang['IES']['chibook_new']['location'] = "出版地點";
$Lang['IES']['chibook_new']['pub'] = "出版社";
$Lang['IES']['chibook_new']['remark'] = "引文和筆記";
$Lang['IES']['chibook_new']['IsEditedBook'] = "這是否以主編當作者的書籍？〈全書的章節由多位作者編寫，並有主編整理。〉";


$Lang['IES']['chireport_new']['author'] = "作者姓名";
$Lang['IES']['chireport_new']['organization'] = "機構名稱";
$Lang['IES']['chireport_new']['year'] = "出版年份";
$Lang['IES']['chireport_new']['month'] = "出版月份";
$Lang['IES']['chireport_new']['day'] = "出版日期";
$Lang['IES']['chireport_new']['title'] = "報告名稱";
$Lang['IES']['chireport_new']['remark'] = "引文和筆記";

$Lang['IES']['chijournals_new']['author'] = "作者姓名";
$Lang['IES']['chijournals_new']['year'] = "出版年份";
$Lang['IES']['chijournals_new']['bkname'] = "刊物名稱";
$Lang['IES']['chijournals_new']['articlename'] = "文章名稱";
$Lang['IES']['chijournals_new']['gnum'] = "卷數";
$Lang['IES']['chijournals_new']['fnum'] = "期數";
$Lang['IES']['chijournals_new']['pnum'] = "頁數";
$Lang['IES']['chijournals_new']['remark'] = "引文和筆記";

$Lang['IES']['chimagazine_new']['author'] = "作者姓名";
$Lang['IES']['chimagazine_new']['year'] = "出版年份";
$Lang['IES']['chimagazine_new']['month'] = "出版月份";
$Lang['IES']['chimagazine_new']['day'] = "出版日期";
$Lang['IES']['chimagazine_new']['bkname'] = "刊物名稱";
$Lang['IES']['chimagazine_new']['articlename'] = "文章名稱";
$Lang['IES']['chimagazine_new']['fnum'] = "期數";
$Lang['IES']['chimagazine_new']['pnum'] = "頁數";
$Lang['IES']['chimagazine_new']['remark'] = "引文和筆記";

$Lang['IES']['chinew_new']['author'] = "作者姓名";
$Lang['IES']['chinew_new']['date'] = "出版日期";
$Lang['IES']['chinew_new']['bkname'] = "報章名稱";
$Lang['IES']['chinew_new']['articlename'] = "文章名稱";
$Lang['IES']['chinew_new']['pnum'] = "頁數";
$Lang['IES']['chinew_new']['remark'] = "引文和筆記";

$Lang['IES']['chinet_new']['author'] = "作者姓名";
$Lang['IES']['chinet_new']['organization'] = "機構名稱";
$Lang['IES']['chinet_new']['year'] = "年份";
$Lang['IES']['chinet_new']['articlename'] = "網頁名稱";
$Lang['IES']['chinet_new']['date'] = "瀏覽日期";
$Lang['IES']['chinet_new']['website'] = "網址";
$Lang['IES']['chinet_new']['remark'] = "引文和筆記";

$Lang['IES']['chiother_new']['other'] = '自行輸入';

$Lang['IES']['manual']['manual'] = '自行輸入';

$Lang['IES']['engbook_new']['bookAuthor'] = 'Book Author';
$Lang['IES']['engbook_new']['bookTitle'] = 'Book Title';
$Lang['IES']['engbook_new']['chapterAuthor'] = 'Chapter Author';
$Lang['IES']['engbook_new']['chapterTitle'] = 'Chapter Title';
$Lang['IES']['engbook_new']['bookEditor'] = 'Book Editor';
$Lang['IES']['engbook_new']['year'] = 'Year of Publication';
$Lang['IES']['engbook_new']['editor'] = 'Editor Name';
$Lang['IES']['engbook_new']['titleWork'] = 'Title of Work';
$Lang['IES']['engbook_new']['edition'] = 'Edition';
$Lang['IES']['engbook_new']['pnum'] = 'Page';
$Lang['IES']['engbook_new']['location'] = 'Place of Publication';
$Lang['IES']['engbook_new']['pub'] = 'Publisher';
$Lang['IES']['engbook_new']['remark'] = "Citations and Notes";
$Lang['IES']['engbook_new']['IsEditedBook'] = "Is this an edited book?(i.e. with a collections of chapters which are written by different authors and collated by an editor or editors)";


$Lang['IES']['engreport_new']['author'] = 'Author Name';
$Lang['IES']['engreport_new']['organization'] = 'Organization Name';
$Lang['IES']['engreport_new']['year'] = 'Year of Publication';
$Lang['IES']['engreport_new']['title'] = 'Title of Report';
$Lang['IES']['engreport_new']['location'] = 'Place of Publication';
$Lang['IES']['engreport_new']['pub'] = 'Publisher';
$Lang['IES']['engreport_new']['remark'] = "Citations and Notes";

$Lang['IES']['engjournals_new']['author'] = 'Author Name';
$Lang['IES']['engjournals_new']['year'] = 'Year of Publication';
$Lang['IES']['engjournals_new']['articlename'] = 'Title of Article';
$Lang['IES']['engjournals_new']['journal'] = 'Title of Journal';
$Lang['IES']['engjournals_new']['vnum'] = 'Volume Number';
$Lang['IES']['engjournals_new']['inum'] = 'Issue Number';
$Lang['IES']['engjournals_new']['pnum'] = 'Page number';
$Lang['IES']['engjournals_new']['remark'] = "Citations and Notes";

$Lang['IES']['engmagazine_new']['author'] = 'Author Name';
$Lang['IES']['engmagazine_new']['year'] = 'Year of Publication';
$Lang['IES']['engmagazine_new']['month'] = 'Month of Publication';
$Lang['IES']['engmagazine_new']['day'] = 'Day of Publication';
$Lang['IES']['engmagazine_new']['articlename'] = 'Title of Article';
$Lang['IES']['engmagazine_new']['magazine'] = 'Title of Magazine';
$Lang['IES']['engmagazine_new']['vnum'] = 'Volume Number';
$Lang['IES']['engmagazine_new']['inum'] = 'Issue Number';
$Lang['IES']['engmagazine_new']['pnum'] = 'Page number';
$Lang['IES']['engmagazine_new']['remark'] = "Citations and Notes";

$Lang['IES']['engnew_new']['author'] = 'Author Name';
$Lang['IES']['engnew_new']['date'] = 'Date of Publication';
$Lang['IES']['engnew_new']['articlename'] = 'Title of Article';
$Lang['IES']['engnew_new']['newspaper'] = 'Title of Newspaper';
$Lang['IES']['engnew_new']['pnum'] = 'Page';
$Lang['IES']['engnew_new']['remark'] = "Citations and Notes";

$Lang['IES']['engnet_new']['author'] = 'Author Name';
$Lang['IES']['engnet_new']['organization'] = 'Organization Name';
$Lang['IES']['engnet_new']['year'] = 'Year of Publication';
$Lang['IES']['engnet_new']['month'] = 'Month of Publication';
$Lang['IES']['engnet_new']['day'] = 'Day of Publication';
$Lang['IES']['engnet_new']['title'] = 'Title of Work';
$Lang['IES']['engnet_new']['bdate'] = 'Date of retrieval';
$Lang['IES']['engnet_new']['website'] = 'Web Address';
$Lang['IES']['engnet_new']['remark'] = "Citations and Notes";

$Lang['IES']['engother_new']['other'] = 'Others';

/***********new bibi details END *************/

$Lang['IES']['remark'] = "Citations and Notes";
$Lang['IES']['type'] = "Type";
$Lang['IES']['bibi'] = "Bibliography";
#End: bibi<============================== This is very special, it requires user can see both chinese and eng, no need to set to EN


#Survey
$Lang['IES']['NoCombination'] = "No Combination";
$Lang['IES']['GreyCantMap'] = "Question grey in color cannot do mapping";
$Lang['IES']['ConfirmChangeMapping'] = "Please be awared that all unsaved mapping will lost, are you sure to change?";
$Lang['IES']['questionaireresult'] = "Questionnaire Results";
$Lang['IES']['manage_answered_questionaire'] = "Manage questionnaires collected";
$Lang['IES']['manage_answered_observation'] = "Manage record forms collected";
$Lang['IES']['manage_answered_questionaireType2'] = "Management The Completed Report";
$Lang['IES']['answered_questionaire_discovery_list'] = "Questionnaire Discovery List";
$Lang['IES']['answered_questionaire_discovery'] = "Questionnaire Discovery";
$Lang['IES']['question_is_valid'] = "Validity";
$Lang['IES']['is_valid'] = "Valid";
$Lang['IES']['is_invalid'] = "Invalid";
$Lang['IES']['inputdate'] = "Input Date";
$Lang['IES']['inputdateInterview'] = "Input Date";
$Lang['IES']['inputdateObserve'] = "Input Date";
$Lang['IES']['inputdateSurvey'] = "Input Date";
$Lang['IES']['Respondent'] = "Respondent";
$Lang['IES']['EmailList'] = "Email list";
$Lang['IES']['EmailInvalid'] = "There are invalid e-mail addresses in the e-mail addresses fields.";
$Lang['IES']['InterviewDate'] = "Date Of Interview";
$Lang['IES']['Overall'] = "View results by question";
$Lang['IES']['Grouping'] = "Choosing and comparing results";
$Lang['IES']['GroupingList'] = "Cross comparison";
$Lang['IES']['newgrouping'] = "Start the comparison";
$Lang['IES']['PairupGroupItem'] = "Title";
$Lang['IES']['PairUpWith'] = "Choose a question for comparison";
$Lang['IES']['TableBelowShowResultOfQuestion'] = "Table below shows cross-analysis of questions chosen";
$Lang['IES']['YouCanInputOtherPairUpGroupHere'] = '1|=|You can new a Pair-up Group here.';
$Lang['IES']['InsertTemplateWord'] = 'Insert template text';
$Lang['IES']['MC'] = 'Multiple-choice Question(Choose One)';
$Lang['IES']['MultiMC'] = 'Multiple-choice Question(Choose Multiple)';
$Lang['IES']['FillShort'] = 'Short Question';
$Lang['IES']['FillLong'] = 'Long Question';
$Lang['IES']['LikertScale'] = 'Likert-type Question';
$Lang['IES']['New'] = 'New';
$Lang['IES']['Copy'] = 'Copy';
$Lang['IES']['Export'] = 'Export';
$Lang['IES']['Export_Question'] = 'Export questions';
$Lang['IES']['Export_QuestionObserve'] = 'Export record forms';
$Lang['IES']['Export_Result'] = 'Export result';
$Lang['IES']['Export_Analysis'] = 'Export analysis and key questions';
$Lang['IES']['Export_Discovery'] = 'Export discovery';
$Lang['IES']['IS_VALID_TO_DO'] = 'You are not invited for this survey, or your invitation has been cancelled.';
$Lang['IES']['IsFocusQuestion'] = "This is a key question.";
$Lang['IES']['GroupingInstruction'] = "Read each question carefully. Choose several key questions, or comparisons between any two that can address the focus questions for writing up the research findings and outcomes at Stage 3.";
$Lang['IES']['GroupingInstruction2'] = 'Choose two questions for comparison each time, and write up findings and outcomes in Stage 3.';
$Lang['IES']['ClickToAnswerQuestionnaire'] = 'Click To Answer Questionnaire';


//////20101103//////
$Lang['IES']['FirstStageTeacherComment'] = 'Teacher\'s comment in 1st stage';
$Lang['IES']['DateOfSubmission'] ="Date of Submission";
$Lang['IES']['NotReleased'] ="Not Released";	// 未開放
$Lang['IES']['Introduction'] ="Introduction";//簡介
$Lang['IES']['View'] ="View";	//檢視
$Lang['IES']['View2'] ="View";	
$Lang['IES']['FollowDefaultStepToModify'] ="Follow the default steps to modify";
$Lang['IES']['TeachersFeedback'] ="Teacher's Feedback";	//老師回饋
$Lang['IES']['PleaseGuideMeToWrite'] ="Guide me";	//請引導我撰寫
$Lang['IES']['InstantEdit'] ="Instant Edit";	//即時撰寫
$Lang['IES']['ManageInformation'] = 'Manage data';	//管理資料
$Lang['IES']['ChooseOtherScheme'] = 'Choose another '.$Lang['IES']['Scheme'];
$Lang['IES']['Folder'] = $Lang['IES']['FolderDesc'];
$Lang['IES']['ReflectNote'] = $Lang['IES']['NoteDesc'];
//$Lang['IES']['FAQ'] = 'FAQ';
$Lang['IES']['MyFolder'] = 'My Files';
$Lang['IES']['Search'] = 'Search';
$Lang['IES']['UploadedFiles'] = 'Teacher Uploaded File(s)';
$Lang['IES']['TemporarilyNoContent'] = 'No Content';
$Lang['IES']['Modify'] = 'Modify';
$Lang['IES']['IWouldLikeToAsk'] = 'I would like to ask';
$Lang['IES']['PleaseSelectFiles'] = 'Please select files';
$Lang['IES']['BibliographyRecords'] = $Lang['IES']['TextDesc'];
$Lang['IES']['PleaseFillInRequiredInformation'] = 'Please fill in required information';
////////////



$Lang['IES']['SurveyTable']['string1'] = 'To start inviting your research objects to fill in the questionnaire, click on \'Send email\'.';
$Lang['IES']['SurveyTable']['string2'] = 'To view the results of the questionnaires replied by email or input the data of the questionnaires collected from the other means, click on \'Manage data\'.';
$Lang['IES']['SurveyTable']['string3'] = 'Send email';
$Lang['IES']['SurveyTable']['string4'] = $Lang['IES']['ManageInformation'];
$Lang['IES']['SurveyTable']['string5'] = 'State the date, time and venue clearly when making appointments with interviewees. Better tell them the objectives of the interview and the focus of the questions in advance. If the interview will be recorded, their consent is required. To note down the important points during the interview, you are advised to export and print the interview record form you have created by clicking on the above link before the interview. ';
$Lang['IES']['SurveyTable']['string6'] = 'Points to note:';
$Lang['IES']['SurveyTable']['string7'] = 'Before the interview, check if the recording devices function normally. ';
$Lang['IES']['SurveyTable']['string8'] = 'In the interview, take the initiative to communicate with the interviewee so that useful information will be provided under your guidance. ';
$Lang['IES']['SurveyTable']['string9'] = 'Discover some follow-up questions and clarify the interviewee\'s viewpoints.';
$Lang['IES']['SurveyTable']['string10'] = 'Be patient when the interviewee is sharing their thoughts. Don\'t push them for answers.';
$Lang['IES']['SurveyTable']['string11'] = 'If possible, you should achieve the expected objectives.';
$Lang['IES']['SurveyTable']['string12'] = 'After the field observation, tidy up the observation report with the aid of your notes. Click on \'Manage data\' to type up the complete record.';
$Lang['IES']['SurveyTable']['string13'] = 'Questionnaire created';
$Lang['IES']['SurveyTable']['string14'] = $Lang['IES']['Interview'];
$Lang['IES']['SurveyTable']['string15'] = $Lang['IES']['Observe'];
$Lang['IES']['SurveyTable']['string16'] = 'A key to collect data useful to your enquiry study is to learn more about the steps for designing a questionnaire and important notes as soon as possible.';
$Lang['IES']['SurveyTable']['string17'] = 'The design of a questionnaire';
$Lang['IES']['SurveyTable']['string18'] = 'How to ask the right questions?';
$Lang['IES']['SurveyTable']['string19'] = 'First of all, decide on the interviewees and the interviewing method like face-to-face interview, telephone interview or interviewees sending the feedback in text by email. Next, design interview questions.';
$Lang['IES']['SurveyTable']['string20'] = 'Interviewees and the scope of the enquiry should be interrelated. It would be best if they were the key persons who can provide reliable information. Refer to the exemplar below:';
$Lang['IES']['SurveyTable']['string21'] = 'How to select interviewees?';
$Lang['IES']['SurveyTable']['string22'] = 'Refer to the following exemplar to learn more about the course of an interview.';
$Lang['IES']['SurveyTable']['string23'] = 'The course of an interview';
$Lang['IES']['SurveyTable']['string24'] = 'The objective of conducting field observation is to obtain useful information on the topic for enquiry through observations. What information do you have to collect? Who or what phenomena should be observed? What are the characteristics of the research objects or phenomena? What is the rundown of the observation? Do you need approval beforehand? What equipment do you need to prepare? What difficulties will you encounter in the process? What are the limitations of field observation, i.e. what type of information cannot be obtained through field observation? ';
$Lang['IES']['SurveyTable']['string25'] = 'Click on ‘Create’ to fill in the details for the above items in the field observation record form.';
$Lang['IES']['SurveyTable']['string26'] = 'Cancel Selection';





$Lang['IES']['Questionnaire_viewQue']['string1'] = "Sample questions:";
$Lang['IES']['Questionnaire_viewQue']['string2'] = "Multiple choices (single answer/multiple answers):";
$Lang['IES']['Questionnaire_viewQue']['string3'] = "Which of the following is / are your favourite activity / activities on the social networking site?(A) Play games (B) Connect friends (C) Share photos (D) Update your blog (E) Make new friends (F) Share videos";
$Lang['IES']['Questionnaire_viewQue']['string4'] = "Long question / short question (Select long question if a lengthy answer is required): ";
$Lang['IES']['Questionnaire_viewQue']['string5'] = "What do you think are the advantages of running small-class teaching in primary schools?";
$Lang['IES']['Questionnaire_viewQue']['string6'] = "Likert type question: ";
$Lang['IES']['Questionnaire_viewQue']['string7'] = "Do you agree that the prohibition of smoking at open-air bus-stops should be legislated by the government?";
$Lang['IES']['Questionnaire_viewQue']['string8'] = "Strongly agree";
$Lang['IES']['Questionnaire_viewQue']['string9'] = "Agree";
$Lang['IES']['Questionnaire_viewQue']['string10'] = "Neutral";
$Lang['IES']['Questionnaire_viewQue']['string11'] = "Disagree";
$Lang['IES']['Questionnaire_viewQue']['string12'] = "Strongly disagree";


$Lang['IES']['Questionnaire_questionGroupingList']['string1'] = "Below are the cross comparisons you have selected to address the focus questions, and to write up findings and outcomes.";
$Lang['IES']['Questionnaire_questionGroupingList']['string1_w_CommentEdit'] = "These are the key questions you have selected to address the focus questions, and to write up findings and outcomes.Click each title to write up research findings and outcomes.";

$Lang['IES']['Questionnaire_management']['string1'] = "When the research objects have completed the questionnaires through the system, the results of their questionnaires will be automatically delivered to the system. If you find the responses of the questionnaires abnormal when viewing the results, you should invalidate the corresponding questionnaires in order not to interfere with the accuracy of the research results and the analysis. <br/><br/>To ensure the data collected is complete, click on 'New' to enter the responses for each questionnaire manually, if you have not sent your questionnaires by email. ";
$Lang['IES']['Questionnaire_management']['string2'] = "To transcribe the interview content, click on 'Add'. Remember to indicate the tone of both parties clearly to prove your argument. ";
$Lang['IES']['Questionnaire_management']['string3'] = "After the field observation, click on 'Add' to type up the complete observation record.";

$Lang['IES']['Questionnaire_Display']['string1'] = "To back up the records of the data collection process, click 'Export result' to export the record list which can be appended to the enquiry report.";

$Lang['IES']['Coursework_index']['string2'] = "Welcome to the world of Independent Enquiry Study!";
$Lang['IES']['Coursework_index']['string3'] = "Do you know what Independent Enquiry Study (IES) is?";
$Lang['IES']['Coursework_index']['string4'] = "Congratulations if you know it well, as 'well begun is half done'. But if you are not too sure, no worries! Just follow the instructions, and you can get the IES project done. ";
$Lang['IES']['Coursework_index']['string5'] = "We hope that in the course of doing the IES, you can relate different knowledge and learning experience, develop advanced thinking and communication abilities and become an independent learner.";
$Lang['IES']['Coursework_index']['string6'] = "The Independent Enquiry Study is divided into 3 stages:";

$Lang['IES']['Coursework__general']['string1'] = "Your submission here will be displayed in the task page.\\nPlease note that your previous answer in the task page will be replaced by the new submission.";
$Lang['IES']['SchemeMoveStudent'] = 'Transfer Students to this Scheme';
$Lang['IES']['StageExportDoc']['TableOfContent'] = "Table of Contents";
$Lang['IES']['StageExportDoc']['Page'] = "Page";

$Lang['IES']['StageExportDoc']['string1_stage1'] = "Project Proposal Form";
$Lang['IES']['StageExportDoc']['string1_stage2'] = "Data Collection Process Record Form";
$Lang['IES']['StageExportDoc']['string1_stage3'] = "Independent Enquiry Study (IES) Report";

$Lang['IES']['StageExportDoc']['string2'] = "Important Note";

$Lang['IES']['StageExportDoc']['string3'] = "Each student should hand in a project proposal to his/her IES supervising teacher before the deadline according to the Assessment Plan set out by the school. This Project Proposal Form, which serves as a comprehensive guide to the writing of a project proposal, can be completed as the task of Stage 1. Students might, in accordance with their supervising teacher’s consent and the Assessment Plan, choose to fill in the Form part by part and receive comments in a gradual manner and/or submit the fully completed Form to their supervising teacher, bearing in mind that they might have to do this more than once. Candidates are reminded that no re-submission will be allowed after the supervising teacher has completed the assessment of the task and marks have been given to the task.";
$Lang['IES']['StageExportDoc']['string3_stage2'] = "Each student should hand in a data collection record to his/her IES supervising teacher before the deadline according to the Assessment Plan set out by the school. This Data Collection Form, which serves as a comprehensive guide to the record-keeping of data collection, can be completed as the task of Stage II. Students might, in accordance with their supervising teacher's consent and the Assessment Plan, choose to fill in the Form part by part and receive comments in a gradual manner and/or submit the fully completed Form to their supervising teacher, bearing in mind that they might have to do this more than once. Candidates are reminded that no re-submission will be allowed after the supervising teacher has completed the assessment of the task and marks have been given to the task.";

$Lang['IES']['StageExportDoc']['string4'] = "1st time / Re-Submision (___ time) / Last Submission";
$Lang['IES']['StageExportDoc']['string5'] = "Student Declaration";
$Lang['IES']['StageExportDoc']['string6'] = "I certify that:";
$Lang['IES']['StageExportDoc']['string7'] = "the task submitted/completed for this stage is my own work;";
$Lang['IES']['StageExportDoc']['string8'] = "it does not include materials copied directly, in part or in whole, from any sources without proper acknowledgement; and";
$Lang['IES']['StageExportDoc']['string9'] = "I have not submitted this piece of work for assessment in any other course in the HKDSE Examination.";
$Lang['IES']['StageExportDoc']['string10'] = "Student’s signature:";
$Lang['IES']['StageExportDoc']['string11'] = "Date:";

$Lang['IES']['StageExportDoc']['string12'] = "Important Note";
$Lang['IES']['StageExportDoc']['string13'] = "Each student should hand in an independent enquiry report to his/her IES supervising teacher before the deadline according to the Assessment Plan set out by the school. Candidates are reminded that no re-submission will be allowed after the supervising teacher has completed the assessment of the task and marks have been given to the task.";
$Lang['IES']['StageExportDoc']['string14'] = "Topic for Enquiry";
$Lang['IES']['StageExportDoc']['string15'] = "第";
$Lang['IES']['StageExportDoc']['string16'] = "Chapter";  
$Lang['IES']['StageExportDoc']['string17'] = "附錄";
$Lang['IES']['StageExportDoc']['string18'] = "目錄";
$Lang['IES']['StageExportDoc']['string19'] = "頁數";
$Lang['IES']['StageExportDoc']['string20'] = "香港中學文憑考試";
$Lang['IES']['StageExportDoc']['string21'] = "Independent Enquiry Study (IES) Report";
$Lang['IES']['StageExportDoc']['string22'] = "學校名稱";
$Lang['IES']['StageExportDoc']['string23'] = "學生姓名";
$Lang['IES']['StageExportDoc']['string24'] = "指導老師";
$Lang['IES']['StageExportDoc']['string25'] = "班別";
$Lang['IES']['StageExportDoc']['string26'] = "繳交日期";

$Lang['IES']['StageExportDoc']['string27'] = "Note*	In addition to research diaries, raw data and an extract listing the essence of raw data (such as a complete listing of the records of data resultant from a survey or from content analysis; together with some basic summary statistics or a summary table summarising opinions and views collected from focused interviews, discussions, observations or article reviews) should be submitted for checking.";

$Lang['IES']['WordDocSettingsInstruction'] = "Below is the order different parts will appear in the final report. When students choose to export their reports, the system will rearrange the parts according to this order. You can re-order the parts using drag and drop.<br/><br/>Note: As students CANNOT know of this order from their interface, you have to tell them about your settings. Otherwise, transitional phrases like \"According to the <b>above</b> analysis...\" may become appearing before the text where the analysis occurs.";
$Lang['IES']['ResetDoc']['SelectMode'] = "Please select a reset mode :";
$Lang['IES']['ResetDoc']['DefaultMode'] = "Reset to the official EDB format.";
$Lang['IES']['ResetDoc']['DeleteMode'] = "Remove all parts for a fresh start.";
$Lang['IES']['ViewStudentFile'] = "View Student\'s [".$Lang['IES']['MyFolder']."] Files";



include_once($PATH_WRT_ROOT."lang/ies_lang_fai.en.php");
####################### This should be placed at the bottom
//}
?>