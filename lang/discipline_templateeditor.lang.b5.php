<?php
# using: 

# this file is for the template editor in Discipline 1.2 only!
$i_Discipline_System_Template_Variable['default_value'] = "不適用";
$i_Discipline_System_Template_Variable['student_name'] = "學生姓名";
$i_Discipline_System_Template_Variable['StudentNameChi'] = "學生姓名(中文)";
$i_Discipline_System_Template_Variable['StudentNameEng'] = "學生姓名(英文)";
$i_Discipline_System_Template_Variable['class_name'] = "班別";
$i_Discipline_System_Template_Variable['ClassNameChi'] = "班別(中文)";
$i_Discipline_System_Template_Variable['ClassNameEng'] = "班別(英文)";
$i_Discipline_System_Template_Variable['class_number'] = "學號";
$i_Discipline_System_Template_Variable['issue_date'] = "發出日期";
// Added [2015-0106-1649-27066]
$i_Discipline_System_Template_Variable['IssueDateChi'] = "發出日期(中文)";
$i_Discipline_System_Template_Variable['additional_info'] = "附加資訊";
$i_Discipline_System_Template_Variable['school_year'] = "年度";
$i_Discipline_System_Template_Variable['semester'] = "學期";
$i_Discipline_System_Template_Variable['detention_reason'] = "留堂原因";
$i_Discipline_System_Template_Variable['session_details'] = "留堂時段";
$i_Discipline_System_Template_Variable['event_date'] = "項目日期";
$i_Discipline_System_Template_Variable['merit_record'] = "獎勵紀錄";
$i_Discipline_System_Template_Variable['conduct_mark_increment'] = "操行分 (增加)";
$i_Discipline_System_Template_Variable['PIC'] = "負責人";
$i_Discipline_System_Template_Variable['award_reason'] = "獎勵原因";
$i_Discipline_System_Template_Variable['remark'] = "備註";
$i_Discipline_System_Template_Variable['event_date'] = "事項日期";
$i_Discipline_System_Template_Variable['demerit_record'] = "懲罰紀錄";
$i_Discipline_System_Template_Variable['content_type'] = "紀錄類別";
$i_Discipline_System_Template_Variable['warning_content'] = "違規警告紙 (紀錄)";
$i_Discipline_System_Template_Variable['demerit_content'] = "記缺點通知信 (紀錄)";
// Added: 2015-01-19
$i_Discipline_System_Template_Variable['demerit_recordChi'] = "懲罰紀錄(中文)";
$i_Discipline_System_Template_Variable['demerit_recordEng'] = "懲罰紀錄(英文)";
$i_Discipline_System_Template_Variable['conduct_mark (Decrement)'] = "操行分 (減少)";
$i_Discipline_System_Template_Variable['punishment_reason'] = "懲罰原因";
$i_Discipline_System_Template_Variable['current_conduct_mark'] = "現時操行分";
$i_Discipline_System_Template_Variable['warning_reminderpoint'] = "扣分提示";
$i_Discipline_System_Template_Variable['subscore_warning_reminderpoint'] = "勤學分扣分提示";
// Added: 2017-11-13
$i_Discipline_System_Template_Variable['IssueDateEng'] = "發出日期(英文)";

$i_Discipline_System_Template_Variable['study_score_increment'] = "勤學分 (增加)";
$i_Discipline_System_Template_Variable['attitude_score_increment'] = "態度分 (增加)";
$i_Discipline_System_Template_Variable['activity_score_increment'] = "活動分 (增加)";
$i_Discipline_System_Template_Variable['study_mark (Decrement)'] = "勤學分 (減少)";
$i_Discipline_System_Template_Variable['attitude_mark (Decrement)'] = "態度分 (減少)";
$i_Discipline_System_Template_Variable['activity_mark (Decrement)'] = "活動分 (減少)";
$i_Discipline_System_Template_Variable['current_study_score'] = "現時勤學分";
$i_Discipline_System_Template_Variable['current_attitude_score'] = "現時態度分";
$i_Discipline_System_Template_Variable['current_activity_score'] = "現時活動分";
$i_Discipline_System_Template_Variable['attitude_score_warning_reminderpoint'] = "態度分扣分提示";
$i_Discipline_System_Template_Variable['actscore_warning_reminderpoint'] = "活動分扣分提示";

$i_Discipline_System_Template_Variable['goodconduct_reason'] = "良好行為原因";
$i_Discipline_System_Template_Variable['misconduct_reason'] = "違規行為原因";

$i_Discipline_System_Template_Variable['AccuNumber'] = "累積良好/違規次數";

$i_Discipline_System_Template_Variable['PeriodDescription'] = "學段";
$i_Discipline_System_Template_Variable['GoodConductAwardRecord'] = "優良表現紀錄表";
$i_Discipline_System_Template_Variable['AmountOfMerit'] = "優點數量";
$i_Discipline_System_Template_Variable['AmountOfMinorMerit'] = "小功數量";
$i_Discipline_System_Template_Variable['AmountOfMajorMerit'] = "大功數量";
$i_Discipline_System_Template_Variable['AmountOfGoodConduct'] = "良好行為數量";

$i_Discipline_System_Template_Variable['ParentName'] ="家長姓名"; 
?>
