<?php
# Editing by ivan
$eRC_Rubrics_ConfigArr['MaxLevel'] = (!isset($eRC_Rubrics_ConfigArr['MaxLevel']))? 0 : $eRC_Rubrics_ConfigArr['MaxLevel'];

$Lang['eRC_Rubrics']['GeneralArr']['Report'] = "報告";
$Lang['eRC_Rubrics']['GeneralArr']['SchoolYear'] = "學年";
$Lang['eRC_Rubrics']['GeneralArr']['SchoolYear2Ch'] = "年度";
$Lang['eRC_Rubrics']['GeneralArr']['Form'] = "級別";
$Lang['eRC_Rubrics']['GeneralArr']['ClassName'] = "班別";
$Lang['eRC_Rubrics']['GeneralArr']['ClassNumber'] = "班號";
$Lang['eRC_Rubrics']['GeneralArr']['Gender'] = "性別";
$Lang['eRC_Rubrics']['GeneralArr']['Class'] = "班別";
$Lang['eRC_Rubrics']['GeneralArr']['Subject'] = "學科";
$Lang['eRC_Rubrics']['GeneralArr']['SubjectGroup'] = "科組";
$Lang['eRC_Rubrics']['GeneralArr']['Code'] = "代號";
$Lang['eRC_Rubrics']['GeneralArr']['Name'] = "名稱";
$Lang['eRC_Rubrics']['GeneralArr']['ApplicableForm'] = "適用級別";
$Lang['eRC_Rubrics']['GeneralArr']['NameEn'] = "名稱 (英文)";
$Lang['eRC_Rubrics']['GeneralArr']['NameCh'] = "名稱 (中文)";
$Lang['eRC_Rubrics']['GeneralArr']['EnglishName'] = "名稱 (英文)";
$Lang['eRC_Rubrics']['GeneralArr']['ChineseName'] = "名稱 (中文)";
$Lang['eRC_Rubrics']['GeneralArr']['Remarks'] = "備註";
$Lang['eRC_Rubrics']['GeneralArr']['AllClass'] = "所有班別";
$Lang['eRC_Rubrics']['GeneralArr']['AllForm'] = "所有級別";
$Lang['eRC_Rubrics']['GeneralArr']['AllReport'] = "所有成績表";
$Lang['eRC_Rubrics']['GeneralArr']['DownloadCSVFile'] = "下載CSV檔案範本";
$Lang['eRC_Rubrics']['GeneralArr']['GenerateCSVFile'] = "製作CSV範本檔";
$Lang['eRC_Rubrics']['GeneralArr']['LastModifiedDate'] = "最後修改日期";
$Lang['eRC_Rubrics']['GeneralArr']['Mark'] = "分數";
$Lang['eRC_Rubrics']['GeneralArr']['WholeForm'] = "全級";
$Lang['eRC_Rubrics']['GeneralArr']['NotAssessed'] = "未評級";
$Lang['eRC_Rubrics']['GeneralArr']['ActiveAcademicYear'] = "系統使用中的學年";
$Lang['eRC_Rubrics']['GeneralArr']['CurrentAcademicYear'] = "本學年";
$Lang['eRC_Rubrics']['GeneralArr']['NotClassified'] = "沒有分類的學生";
$Lang['eRC_Rubrics']['GeneralArr']['SelectAll'] = "全選";

$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['Report'] = "-- 選擇報告 --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllReport'] = "-- 所有成績表 --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['Rubrics'] = "-- 選擇評量指標 --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllClass'] = "-- 所有班別 --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllForm'] = "-- 所有級別 --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllSubjectGroup'] = "-- 所有科組 --";

$Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr']['Desc'] = "名稱";
$Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr']['Abbr'] = "縮寫";
$Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr']['ShortForm'] = "簡寫";

$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongWebSamsRegNo'] = "沒有此用戶";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongUserLogin'] = "沒有此用戶";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongClassNameNumber'] = "沒有此用戶";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyClassNumber'] = "班號空白";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyClassName'] = "班別空白";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyStudentData'] = "學生資料空白";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongDataFormat'] = "資料格式錯誤";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongCsvHeader'] = "CSV檔格式錯誤";

$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Code'] = "不能沒有代號";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name'] = "不能沒有名稱";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code'] = "此代號已被使用";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoTopicForSubject'] = "此學科未有任何課程紀錄，請前往 \"設定 > 課程資料庫\" 更新有關設定。";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoThisTopicForSubject'] = "此學生未有學習此科目的有關範疇，請前往 \"設定 > 學生學科細目\" 更新有關設定。";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoModuleInTerm'] = "此學期未有任何單元紀錄，請前往 \"設定 > 單元\" 更新有關設定。";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoRubricsForSubject'] = "此學科未有任何評量指標紀錄，請前往 \"設定 > 學科評量指標\" 更新有關設定。";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoLearningCategory'] = "未有任何學習領域設定，請前往 \"學校基本設定 > 學科\" 更新有關設定。";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoSubject'] = "未有任何學科設定，請前往 \"學校基本設定 > 學科\" 更新有關設定。";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoStudent'] = "此科組未有任何學生。";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter'] = "代號開首必須為英文字母";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage'] = "請選擇級別階段";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form'] = "請選擇級別";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class'] = "請選擇班別";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Student'] = "請選擇學生";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['StudentInfo'] = "請選擇學生資料";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject'] = "請選擇學科";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['3Subject'] = "請選擇最少三個學科";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['SubjectGroup'] = "請選擇科組";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Term'] = "請選擇學期";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'] = "請選擇單元";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['TopicLevel'] = "請選擇課程資料";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['LearningDetails'] = "請選擇學習細目";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Date']['EndDateCannotEarlierThanStartDate'] = "結束日期不能早於開始日期。";

$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['AlertSelectFile'] = "請選擇要匯入的檔案";
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['FirefoxNotAllowPaste'] = '瀏覽器不允許使用貼上功能。請於網址欄輸人 "about:config"，然後將 "signed.applets.codebase_principal_support" 設置為 "true"。';
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] = '';
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] .= "成績表系統使用中的學年: |||--eRC Active Year--|||";
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] .= '\n';
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] .= "本學年: |||--Current Academic Year--|||";
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] .= '\n\n';
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearTeacher'] = "成績表系統現在不是使用本學年的資料，如要檢視或處理本學年資料，請與成績表系統管理員聯絡。";
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearAdmin'] = "成績表系統現在不是使用本學年的資料，如要檢視或處理本學年資料，請到 \\\"管理 > 資料處理 > 資料轉移\\\" 將成績表系統轉移至本學年。";


### Left Menu [Start]
$Lang['eRC_Rubrics']['ManagementArr']['MenuTitle'] = "管理";
$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['MenuTitle'] = "時間表";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle'] = "分紙";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'] = "班主任評語";
$Lang['eRC_Rubrics']['ManagementArr']['BehaviorAndAttitudeArr']['MenuTitle'] = "行為表現";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle'] = "課外活動及服務";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['MenuTitle'] = "資料處理";

$Lang['eRC_Rubrics']['ReportsArr']['MenuTitle'] = "報告";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MenuTitle'] = "成績表製作";
$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['MenuTitle'] = "已教授項目數量(科目)";
$Lang['eRC_Rubrics']['ReportsArr']['TopicTaughtReportArr']['MenuTitle'] = "已教授項目數量(範疇)";
$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['MenuTitle'] = "各班自行完成的平均分數統計";
$Lang['eRC_Rubrics']['ReportsArr']['FormAverageRubricsCountArr']['MenuTitle'] = "各級自行完成的平均分數統計";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MenuTitle'] = "大分紙";
$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['MenuTitle'] = "範疇比較報告";

$Lang['eRC_Rubrics']['StatisticsArr']['MenuTitle'] = "統計資料";
$Lang['eRC_Rubrics']['StatisticsArr']['RubricsReportArr']['MenuTitle'] = "評量指標報告";
$Lang['eRC_Rubrics']['StatisticsArr']['RadarDiagramArr']['MenuTitle'] = "評估雷達圖";
$Lang['eRC_Rubrics']['StatisticsArr']['ClassSubjectPerformanceArr']['MenuTitle'] = "全班學生於各科的表現";
$Lang['eRC_Rubrics']['StatisticsArr']['SubjectClassPerformanceArr']['MenuTitle'] = "個別科目於各階段的表現";
$Lang['eRC_Rubrics']['StatisticsArr']['ClassTopicPerformanceArr']['MenuTitle'] = "全班學生於各範疇的表現";
$Lang['eRC_Rubrics']['StatisticsArr']['TopicClassPerformanceArr']['MenuTitle'] = "各學生於個別範疇的表現";


$Lang['eRC_Rubrics']['SettingsArr']['MenuTitle'] = "設定";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'] = "單元";
$Lang['eRC_Rubrics']['SettingsArr']['ReportCardTemplateArr']['MenuTitle'] = "成績表範本";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['MenuTitle'] = "課程資料庫";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['MenuTitle'] = "學生學科細目";
$Lang['eRC_Rubrics']['SettingsArr']['BehaviorAndAttitudeArr']['MenuTitle'] = "行為表現";
$Lang['eRC_Rubrics']['SettingsArr']['SubjectRubricsArr']['MenuTitle'] = "學科評量指標";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleSubjectArr']['MenuTitle'] = "單元學科";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle'] = "行政小組";
### Left Menu [End]


### Management > Schedule
$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['MarksheetSubmission'] = "呈交分數";
$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['ApplySchedule'] = "使用期限";
$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['ToolTipArr']['EditModuleSchedule'] = "編輯單元時間表";

### Management > Marksheet
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['Title'] = "學習範疇";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][2]['Title'] = "學習重點";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][3]['Title'] = "學習細目";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['TitleEn'] = "Learning Topic";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][2]['TitleEn'] = "Learning Objective";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][3]['TitleEn'] = "Learning Details";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['TitleCh'] = "學習範疇";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][2]['TitleCh'] = "學習重點";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][3]['TitleCh'] = "學習細目";

for ($i=1; $i<=$eRC_Rubrics_ConfigArr['MaxLevel']; $i++)
{
	$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['Select'] = "選擇".$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['Title'];
	$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['All'] = "全部".$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['Title'];	
}	

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['SelectLevel'] = "選擇等級";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['CopyAndPasteInstruction'] = "為可便用戶入分，用戶可將Excel檔案內的等級複製及貼上到分紙。請先於Excel檔案預備學生於該學科的等級，然後選擇所有等級及按「複製」，再到分紙處點擊要開始複製的選擇單，最後按「Ctrl + V」便可將等級貼上到分紙處。";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ChooseFromCommentBank'] = "由評語庫選擇";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['SubjectGroupCode'] = "Subject Group Code";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['SubjectGroupName'] = "Subject Group Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['StudentUserLogin'] = "Student User Login";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['StudentName'] = "Student Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['TopicCode'] = "Learning Topic Code";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['TopicName'] = "Learning Topic Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['ObjectiveCode'] = "Learning Objective Code";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['ObjectiveName'] = "Learning Objective Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['DetailsCode'] = "Learning Details Code";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['DetailsName'] = "Learning Details Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['Level'] = "Rubrics level";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['Description'] = "Rubrics description";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportDataInstruction'] = "若學生在匯入的檔案中，原來的評量指標級別及敘述將會被取代。";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportDataRemarks'] = "<u>匯入說明:</u><br>
1. 請先製作CSV範本檔，下載後再作修改。<br/>
2. 請在相關欄目輸入評量指標級別。";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['BackToMarksheetRevision'] = "回到分紙";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportOtherMarksheetData'] = "匯入其他資料";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ReturnMsgArr']['ImportSuccess'] = "匯入資料成功";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ReturnMsgArr']['ImportFail'] = "匯入資料失敗";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['jsConfirmMsg']['ChangesWillBeDiscard'] = "剛才所作的更改將會被取消，繼續？";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningTopicWarning'] = "沒有此學習範疇";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningObjectiveWarning'] = "沒有此學習重點";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningDetailsWarning'] = "沒有此學習細目";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoStudentWarning'] = "沒有此學生";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoSubjectGroupWarning'] = "沒有此科組";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotStudyingSubject'] = "學生沒有學習此學科";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotStudyLearningDetails'] = "學生沒有學習此學科細目";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotInSubjectGroup'] = "學生不在此科組內";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['InvalidDetailsLevel'] = "輸入評量指標級別錯誤";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['InvalidInputWarning'] = "輸入格式錯誤";

### Management > Other Information
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['MenuTitle'] = "其他資料";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['ClassName']['En'] = "Class Name";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['ClassNumber']['En'] = "Class Number";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['UserLogin']['En'] = "User Login";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['WebSamsRegNo']['En'] = "WebSams Reg. No.";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['StudentName']['En'] = "Student Name".$Lang['General']['ImportArr']['Reference']['En'];
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['ClassName']['Ch'] = "(班別)";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['ClassNumber']['Ch'] = "(班號)";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['UserLogin']['Ch'] = "(內聯網帳號)";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['WebSamsRegNo']['Ch'] = "(WebSAMS 註冊號碼)";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['StudentName']['Ch'] = "(學生姓名)".$Lang['General']['ImportArr']['Reference']['Ch'];

$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['ClassName'] = array("F1A","","");
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['ClassNumber'] = array("1","","");
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['UserLogin'] = array("","student1login","");
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['WebSamsRegNo'] = array("","","#123456");
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['StudentName'] = array("Mary Chan","Peter Wong","Tom Li");

$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportDataInstruction'] = "如需要更改已匯入的資料，請先匯出現有的CSV檔案並進行修改，然後才匯入到系統。否則，是次匯入的班別/級別的現有的資料將會被覆蓋。";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportOtherInfoSuccess'] = "其他資料匯入成功";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportOtherInfoFail'] = "其他資料匯入失敗"; 

$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['jsConfirmMsg']['OriginalInformationReplace'] = "此班別/級別的資料已被匯入到所選的報告，原有的資料將會被取代，繼續？";

### Management > Class Teacher Comment
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'] = "學生";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'] = "評語";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['AddToStudentComment'] = "加入至學生評語";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SelectedComment'] = "已選擇評語";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseReportCard'] = "選擇成績表";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'] = "選擇學生";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['OnlyStudentInTheSelectedFormWillBeShown'] = "系統只會顯示已選擇的級別內的學生。";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['LastModifiedDate'] = "最後更新日期";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['LastModifiedBy'] = "最後更新者";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Confirmed'] = "完成";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NotCompleted'] = "未完成";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChangeAllTo'] = "全部更改至";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SubmissionStartDate'] = "Submission Start Date";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SubmissionEndDate'] = "Submission End Date";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Class'] = "班別";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Form'] = "級別";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Class'] = "班別";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Name'] = "名稱";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['TeacherComment'] = "老師評語";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Report'] = "報告";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ClassNo'] = "班號";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Code'] = "代號";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ClassTeacherComment'] = "班老師評語";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudentRemarks'] = "只會列出所選級別的學生。";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['AddCommentSuccess'] ="評語加入成功";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['AddCommentFail'] = "評語加入失敗";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Language'] = "語言";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SelectComment'] = "選擇評語";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['File'] = "檔案";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ExportFormData'] = "匯出全級評語";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ExportClassData'] = "匯出全班評語";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentSuccess'] = "評語匯入成功";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentFail'] = "評語匯入失敗";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportDataInstruction'] = "若學生在匯入的檔案中，該原來的評語將會被取代。";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NumOfStudent'] = "學生總數";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NumOfComment'] = "已輸入評語數目";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr']['AddCommentSuccess'] = "1|=|已成功加入評語。";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr']['AddCommentFailed'] = "0|=|加入評語失敗。";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedCommentWillBeLostIfSearch'] = "如現在搜尋評語，已選擇的評語選項將會取消。你是否確定要搜尋評語？";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment'] = "請選擇至少一則評語。";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedStudentWillBeRemovedIfChangedForm'] = "如現在更改級別，所有已選擇的用戶將被移除。你是否確定要更改級別？";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment'] = "請選擇最少一個評語。";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedCommentWillBeLostIfSearch'] = "所選的評語將會被取消，繼續？";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsConfirmArr']['SelectedStudentWillBeRemoved'] = "所選的學生將會被取消，繼續？";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['ClassName']['En'] = "Class Name";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['ClassNumber']['En'] = "Class Number";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['UserLogin']['En'] = "User Login";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['WebSamsRegNo']['En'] = "WebSams Reg. No.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['StudentName']['En'] = "Student Name".$Lang['General']['ImportArr']['Reference']['En'];
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['Comment']['En'] = "Comment".$Lang['General']['ImportArr']['Optional']['En'];
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['ClassName']['Ch'] = "(班別)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['ClassNumber']['Ch'] = "(班號)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['UserLogin']['Ch'] = "(內聯網帳號)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['WebSamsRegNo']['Ch'] = "(WebSAMS 註冊號碼)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['StudentName']['Ch'] = "(學生姓名)".$Lang['General']['ImportArr']['Reference']['Ch'];
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['Comment']['Ch'] = "(評語)".$Lang['General']['ImportArr']['Optional']['Ch'];

### Management > Extra-Curricular Activities and Services
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatCode'] = "Category Code";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatNameEn'] = "Category Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['SubCatCode'] = "Subcategory Code";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['SubCatNameEn'] = "Subcategory Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatItemCode'] = "Item Code";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatItemNameEn'] = "Item Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['Class'] = "Class Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['StudentNameEn'] = "Student Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['StudentUserLogin'] = "Student UserLogin";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN']['ClassName'] = "Class Name";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN']['ClassNumber'] = "Class Number";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN']['UserLogin'] = "User Login";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN']['StudentName'] = "Student Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5']['ClassName'] = "(班別)";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5']['ClassNumber'] = "(班號)";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5']['UserLogin'] = "(內聯網帳號)";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5']['StudentName'] = "(學生姓名) [參考用途]";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportDataInstruction'] = "若學生在匯入的檔案中，原來的課外活動及服務將會被取代。";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportDataRemarks'] = "<u>匯入說明:</u><br>
1. 請先製作CSV範本檔，下載後再作修改。<br/>
2. 如需要為學生選擇課外活動及服務，請在相關欄目輸入「Y」。";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['BackToECAService'] = "回到課外活動及服務";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportOtherECAService'] = "匯入其他資料";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ReturnMsgArr']['ImportSuccess'] = "匯入資料成功";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ReturnMsgArr']['ImportFail'] = "匯入資料失敗";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['jsConfirmMsg']['ChangesWillBeDiscard'] = "剛才所作的更改將會被取消，繼續？";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoECACategoryWarning'] = "沒有此類別";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoECASubCategoryWarning'] = "沒有此子類別";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoECAItemWarning'] = "沒有此項目";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoStudentWarning'] = "沒有此學生";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['InvalidInputWarning'] = "輸入格式錯誤";

### Management > Data Handling
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['TransitTo'] = "轉換至";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['NextYearTermNotComplete'] = "下學年的學期設定尚未完成。";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['NewAcademicYear'] = "新學年";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['OtherAcademicYears'] = "其他學年";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr'][] = "請在以下兩種情況發生前，執行此功能:";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr'][] = "- 於學年開始時，重新啟用eReportCard成績表系統";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr'][] = "- 還原以往某學年eReportCard成績表系統的資料";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr'][] = "系統內現有資料將會被整存。";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['jsWarningArr']['ConfirmTransition'] = "確定進行所選擇的資料轉變?";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['UpdateArr']['Success'] = "1|=|資料轉換成功。";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['UpdateArr'] ['Failed']= "0|=|資料轉換失敗。";


### Reports > ReportCard Generation
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportCardDisplay'] = "成績表顯示";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Title'] = "標題";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Row'] = "行";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['AddMore'] = "增加";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Signature'] = "簽署";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['DataDisplay'] = "顯示資料";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['HeaderData'] = "頁首資料";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['NumOfHeaderDataEachRow'] = "頁首資料每行數目";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectName'] = "學科名稱";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectGroupName'] = "科組名稱";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['DateRangeDisplay'] = "<!--StartYear--> 年 <!--StartMonth--> 月 <!--StartDate--> 日 至 <!--EndYear--> 年 <!--EndMonth--> 月 <!--EndDate--> 日";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CurriculumData'] = "課程資料";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CurriculumWhichStudentNeedNotStudy'] = "學生不用學習的課程資料";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['LastTopicLevelIsAlwaysDisplay'] = "「<!--TopicLevelName-->」為必須顯示資料";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportCardTemplateSettings'] = "成績表範本預設";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsName'] = "預設名稱";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SchoolName'] = "學校名稱";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CoverPage'] = "封面";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ShowCoverPage'] = "顯示封面";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['StudentPhotoFrame'] = "學生相本框架";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarksheetRemarks'] = "分紙備註";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ApplicableToModuleReportOnly'] = "只適用於「單元成績表」。";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportType'] = "報告類型";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SelectFrom'] = "選擇類別";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['GeneralReport'] = "普通報告";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['TherapyReport'] = "治療報告";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['LearningReport'] = "學習報告";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['WarningArr']['ModuleReportMustSelectLastTopicLevel'] = "「單元成績表」必須選擇「<!--TopicLevelName-->」。";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['WarningArr']['ModuleMarkConsolidatedReportMustSelectLastTopicLevel'] = "顯示單元分數的「總結成績表」必須選擇「<!--TopicLevelName-->」。";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['jsWarningArr']['DeleteSettings'] = "你是否確定要刪除此預設？";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['Save'] = "儲存設定";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['SaveAs'] = "另存設定";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['Add'] = "增加設定";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarkDisplayOptionArr']['ModuleBased'] = "單元分數";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarkDisplayOptionArr']['Consolidated_%'] = "已整合百分比";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarkDisplayOptionArr']['Consolidated_Fraction'] = "已整合分數比例";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReturnMsgArr']['SaveSettingsSuccess'] = "1|=|儲存設定成功。";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReturnMsgArr']['SaveSettingsFailed'] = "0|=|儲存設定失敗。"; 

$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['TaughtItem'] = "已教授項目";
$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['TopicItem'] = "教授項目";

$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['Getting'] = "得";
$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['AverageNumber'] = "平均數";
$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['level'] = "分";

$Lang['eRC_Rubrics']['ReportsArr']['ClassRubricsCount']['ReportTitle'] = "自行完成的平均分數";

# Grand Marksheet
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowOption'] = "顯示選項";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['HideOption'] = "隱藏選項";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayFormat'] = "顯示格式";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['StudentDisplayOption'] = "學生顯示選項";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MarksheetDisplayOption'] = "分紙顯示選項";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowFormSummary'] = "在同一個表格內顯示整個級別的班別";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowClassSummary'] = "在不同的表格顯示不同的班別";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayNameInOneLine'] = "姓名顯示於同一行";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DataDisplay'] = "顯示資料";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['LearningDetailDisplay'] = "學習細目";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowLearningTopicTitle'] = "學習範疇標題";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowLearningObjectiveTitle'] = "顯示學習重點標題";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayAll'] = "顯示全部";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['Custom'] = "自選";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['HTML'] = "HTML";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['CSV'] = "CSV";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ViewFormat'] = "檢視格式";

$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Report1'] = "報告一";
$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Report2'] = "報告二";
$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Module'] = "單元";

### Statistics
$Lang['eRC_Rubrics']['StatisticArr']['RubricsReportArr']['Performance'] = "於各科之表現";

$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr']['SubjectSettings'] = "學科";
$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr']['StudentSettings'] = "學生";
$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr']['ModuleSettings'] = "單元";
$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr']['ReportSettings'] = "成績表顯示";

$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['ApplyPageBreak'] = "每頁只列印一個學生";


### Settings > Curriculum Pool
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['Title'] = "學習範疇";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][2]['Title'] = "學習重點";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][3]['Title'] = "學習細目";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['TitleEn'] = "Learning Topic";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][2]['TitleEn'] = "Learning Objective";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][3]['TitleEn'] = "Learning Details";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['TitleCh'] = "學習範疇";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][2]['TitleCh'] = "學習重點";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][3]['TitleCh'] = "學習細目";
for ($i=1; $i<=$eRC_Rubrics_ConfigArr['MaxLevel']; $i++)
{
	$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Settings'] = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Title']."設定";
	$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Add'] = "新增".$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Title'];
	$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['All'] = "全部".$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Title'];
	
	if ($i==1)
		$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Remarks'] = "學習目標";
	else
		$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Remarks'] = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Title']."備註";
}

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['En'][] = "Subject Code";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['En'][] = "Subject Name (Eng)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['En'][] = "Subject Name (Chi)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['Ch'][] = "科目代號";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['Ch'][] = "科目名稱 (英文)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['Ch'][] = "科目代號 (中文)";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CodeEn'] = "Code";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CodeCh'] = "代號";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Eng)En'] = "Name (Eng)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Chi)En'] = "Name (Chi)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Eng)Ch'] = "名稱 (英文)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Chi)Ch'] = "名稱 (中文)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['RemarksEn'] = "Remarks";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['RemarksCh'] = "備註";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ApplicableFormEn'] = "Applicable Form";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ApplicableFormCh'] = "適用級別";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['AllSubjects'] = "所有學科";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ThisSubject'] = "此學科";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['BackToCurriculumPool'] = "回到課程資料庫";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportOtherCurriculum'] = "匯入其他課程資料";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportCurriculumSuccess'] = "已成功匯入課程資料。";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportCurriculumFailed'] = "匯入課程資料失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CurriculumnFormMapping'] = "課程班級對應設定";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicNotBelongsToThisSubject'] = "此<!--TopicLevel-->不屬於此學科";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['WrongLinkageOfExistingTopic'] = "此<!--TopicLevel-->已聯繫於其他<!--PreviousTopicLevel-->";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicNotBelongsToThisLevel'] = "<!--TopicCode-->不是<!--TopicLevel-->";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicDuplicatedIfAddTopic'] = "如匯入此<!--TopicLevel-->，課程資料編號將會被重複使用";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['NoSuchForm'] = "沒有此級別";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['NoApplicableForm'] = "未有輸入適用級別";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['FormNotLinkedToAnyStage'] = "級別不屬於任何階段";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['jsWarningArr']['Delete'] = "屬於此項目的課程記錄將會同時被刪除，你是否確定要刪除此項目？";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['WarningArr']['ApplicableForm'] = "請選擇適用級別。";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ToolTipArr']['EditApplicableForm'] = "編輯適用級別";


### Settings > Student Learning Details
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['RecordOverwriteIfSaveForSG'] = "如儲存此頁，系統將會重寫所有此科組學生的學科細目設定。";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['BackgroundInfoInstructionArr'][0] = "白色底色指<b>所有</b>此科組的學生需要於該單元學習該細目。";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['BackgroundInfoInstructionArr'][1] = "淺灰色底色指<b>部份</b>此科組的學生需要於該單元學習該細目。";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['BackgroundInfoInstructionArr'][2] = "深灰色底色指<b>沒有</b>此科組的學生需要於該單元學習該細目。";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['jsWarningArr']['RecordOverwriteIfSaveForSG'] = "如儲存此頁，系統將會重寫所有此科組學生的學科細目設定。你是否確定要儲存設定？";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['AllModule'] = "全部單元";

###  Settings > Module
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Warning'] = "警告";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EditModuleWarning'] = "更改及刪除單元可能會影響現存的分數。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Term'] = "學期";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['WholeYear'] = "全年";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Module'] = "單元";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Period'] = "時段";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['StartDate'] = "開始日期";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EndDate'] = "結束日期";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ModuleCode'] = "單元代號";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Name'] = "名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ModuleName'] = "單元名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ChineseName'] = "中文名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EnglishName'] = "英文名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ApplyDate'] = "使用日期";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ThickBoxTitleArr']['AddModule'] = "新增單元";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ThickBoxTitleArr']['EditModule'] = "修改單元";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['ModuleCodeNotAvailible'] = "單元代號已被使用。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['DateRangeOverlap'] = "日期範圍與其他單元重疊。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EndDateLaterThanStartDate'] = "結束日期不能早於開始日期。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['StartDateOutOfTerm'] = "開始日期在學期之外。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EndDateOutOfTerm'] = "結束日期在學期之外。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleNameEn'] = "請輸入英文單元名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleNameCh'] = "請輸入中文單元名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleCode'] = "請輸入單元代號。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['StartDate'] = "請輸入開始日期。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['EndDate'] = "請輸入結束日期。";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsConfirmArr']['RemoveModule'] = "你確定要刪除此單元？";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['InsertSuccess'] = "1|=|新增單元成功。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['InsertFail'] = "0|=|新增單元失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['DeleteSuccess'] = "1|=|刪除單元成功"; 
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['DeleteFail'] = "0|=|刪除單元失敗";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['UpdateSuccess'] = "1|=|修改單元成功"; 
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['UpdateFail'] = "0|=|修改單元失敗";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['AddModule'] = "新增單元";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['EditModule'] = "修改單元";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['DeleteModule'] = "刪除單元";

###  Settings > Module Subject
$Lang['eRC_Rubrics']['SettingsArr']['ModuleSubjectArr']['RemarksArr'][] = "系統會因應以下的設定，以決定於其他版面的學科選單內的顯示學科。";

###  Settings > Subject Rubrics
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['MenuTitle'] = "學科評量指標";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['LearningCategory'] = "學習領域";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Subject'] = "學科";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsScheme'] = "評量指標";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['NoLearningCategory'] = "未分類學科";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsLevelDescription'] = "評量指標級別及敘述";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsCode'] = "評量指標代號";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsName'] = "評量指標名稱";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ChineseName'] = "中文名稱";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['EnglishName'] = "英文名稱";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Lowest'] = "(最低)";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Highest'] = "(最高)";
//$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['SelectRubrics'] = "選擇評量指標";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ThickBoxTitleArr']['EditRubrics'] = "修改評量指標";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ThickBoxTitleArr']['AddRubrics'] = "新增評量指標";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['RubricsCodeNotAvailible'] = "評量指標代號已被使用。";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsEn'] = "請輸入英文評量指標名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsCh'] = "請輸入中文評量指標名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsCode'] = "請輸入評量指標代號。";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsLevelDesc'] = "請輸入級別及敘述。";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsScheme'] = "請選擇所有學科的評量指標。"; 
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['RubricsItemUsed'] = "此評量指標級別已被使用。";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['SelectRubricsToDelete'] = "請選擇想刪除的評量指標。";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['EditRubricsWithUsedItem'] = "此評量指標已被使用，更改及刪除可能會影響現存的分數，繼續？";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['DeleteRubrics'] = "你確定要刪除此評量指標？";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['DeleteUsedRubrics'] = "此評量指標已被使用，更改及刪除可能會影響現存的分數，繼續？";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['InsertSuccess'] = "1|=|新增評量指標成功。";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['InsertFail'] = "0|=|新增評量指標失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['UpdateSuccess'] = "1|=|修改評量指標成功"; 
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['UpdateFail'] = "0|=|修改評量指標失敗";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['DeleteSuccess'] = "1|=|刪除評量指標成功"; 
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['DeleteFail'] = "0|=|刪除評量指標失敗";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['SaveSuccess'] = "1|=|儲存評量指標成功"; 
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['SaveFail'] = "0|=|儲存評量指標失敗";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['EditRubrics'] = "修改評量指標";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['DeleteRubrics'] = "刪除評量指標";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['MoveRubricsLevel'] = "移動級別";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['AddRubricsLevel'] = "新增級別";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['DeleteRubricsLevel'] = "刪除級別";

###  Settings > Template settings
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportCardTemplate'] = "成績表範本";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ChineseName'] = "中文名稱";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['EnglishName'] = "英文名稱";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportType'] = "成績表類型";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportTitle'] = "成績表標題";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ModuleReport'] = "單元成績表";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['LastModifiedDate'] = "最後修改日期";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ConsolidatedReport'] = "總結成績表";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['SelectAll'] = "選擇全部";

$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['EmptyArr']['ReportTitleEn'] = "請輸入中文名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['EmptyArr']['ReportTitleCh'] = "請輸入英文名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['SelectModule'] = "最少選擇一個單元。";

###  Settings > ECA settings
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ThickBoxTitleArr']['AddEditCategory'] = "新增/修改類別";
//$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ThickBoxTitleArr']['EditModule'] = "Edit Module";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['MenuTitle'] = "課外活動及服務";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddCategory'] = "新增類別";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['EditCategory'] = "修改類別";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteCategory'] = "刪除類別";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddSubCategory'] = "新增子類別";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['EditSubCategory'] = "修改子類別";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteSubCategory'] = "刪除子類別";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddItem'] = "新增項目";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['EditItem'] = "修改項目";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteItem'] = "刪除項目";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Category'] = "類別";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Subcategory'] = "子類別";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Item'] = "項目";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Code'] = "代號";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Name'] = "名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['NumOfItem'] = "項目數量";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['CategoryName'] = "類別名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['SubCategoryName'] = "子類別名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ItemName'] = "項目名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ChineseName'] = "中文名稱";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['EnglishName'] = "英文名稱";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryCode'] = "類別代號空白或重複。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryNameEn'] = "英文名稱空白。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryNameCh'] = "中文名稱空白。";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryCode'] = "請輸入類別代號。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryNameEn'] = "請輸入英文名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryNameCh'] = "請輸入中文名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['ECACodeNotAvailible'] = "類別代號已被使用";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemCode'] = "請輸入項目代號。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemNameEn'] = "請輸入英文名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemNameCh'] = "請輸入中文名稱。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['ItemCodeNotAvailible'] = "項目代號已被使用";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertCategorySuccess'] = "1|=|新增類別成功。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertCategoryFail'] = "0|=|新增類別失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateCategorySuccess'] = "1|=|修改類別成功。"; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateCategoryFail'] = "0|=|修改類別失敗";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['ECACodeNotAvailible'] = "0|=|類別代號已被使用";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteCategorySuccess'] = "1|=|刪除類別成功。"; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteCategoryFail'] = "0|=|刪除類別失敗";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertSubCategorySuccess'] = "1|=|新增子類別成功。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertSubCategoryFail'] = "0|=|新增子類別失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateSubCategorySuccess'] = "1|=|修改子類別成功。"; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateSubCategoryFail'] = "0|=|修改子類別失敗";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['SubCodeNotAvailible'] = "0|=|子類別代號已被使用";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteSubCategorySuccess'] = "1|=|刪除子類別成功。"; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteSubCategoryFail'] = "0|=|刪除子類別失敗";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertItemSuccess'] = "1|=|新增項目成功。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertItemFail'] = "0|=|新增項目失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateItemSuccess'] = "1|=|修改項目成功。"; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateItemFail'] = "0|=|修改項目失敗";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['SubCodeNotAvailible'] = "0|=|子項目代號已被使用。";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteItemSuccess'] = "1|=|刪除項目成功。"; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteItemFail'] = "0|=|刪除項目失敗";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteCategoryWithSubCategory'] = "此類別及其子類別和項目將會被刪除，繼續？";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteCategory'] = "你確定要刪除此類別？";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteSubCategoryWithItem'] = "此子類別及其項目將會被刪除，繼續？";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteSubCategory'] = "你確定要刪除此子類別？";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteItem'] = "你確定要刪除此項目？";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['SelectOption']['AllCategory'] = "- 所有類別 -";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['SelectOption']['AllSubCategory'] = "- 所有子類別 -";

### Settings > Comment Bank 
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['MenuTitle'] = "評語庫";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ClassTeacherComment']  = "班老師評語";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['SubjectDetailsComment']  = "學科細目評語";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Topic']  = "題目";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['WarningTopic']  = "請選擇題目";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Code']  = "代號";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Comment']  = "評語";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Subject']  = "學科";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Category']  = "類別";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['New'] = "新增";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Edit'] = "修改";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportCommentSuccess'] = "個評語已被匯入";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportCommentFail'] = "評語匯入失敗";

$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentCode'] = "代號空白";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentEn'] = "英文評語空白";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentCh'] = "中文評語空白";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['CommentCodeNotAvailible'] = "評語代號已被使用";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['WrongCodeFormat'] = "評語代號開首必須為英文字母";

$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCode']['En'] = "Comment Code";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentEn']['En'] = "English Comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCh']['En'] = "Chinese Comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectCode']['En'] = "Subject Code ".$Lang['General']['ImportArr']['Reference']['En'];
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectName']['En'] = "Subject Name ".$Lang['General']['ImportArr']['Reference']['En'];
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCode']['Ch'] = "(評語代號)";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentEn']['Ch'] = "(英文評語)";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCh']['Ch'] = "(中文評語)";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectCode']['Ch'] = "(學科代號) ".$Lang['General']['ImportArr']['Reference']['Ch'];
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectName']['Ch'] = "(學科名稱) ".$Lang['General']['ImportArr']['Reference']['Ch'];
for ($i=1; $i<=$eRC_Rubrics_ConfigArr['MaxLevel']; $i++) {
	$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['En'] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['TitleEn']." Code";
	$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicName'][$i]['En'] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['TitleEn']." Name ".$Lang['General']['ImportArr']['Reference']['En'];	
	
	$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['Ch'] = "(".$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['TitleCh']."代號)";
	$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicName'][$i]['Ch'] = "(".$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['TitleCh']."名稱) ".$Lang['General']['ImportArr']['Reference']['En'];
	
	if ($i != $eRC_Rubrics_ConfigArr['MaxLevel']) {
		$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['En'] .= " ".$Lang['General']['ImportArr']['Reference']['En'];
		$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['Ch'] .= " ".$Lang['General']['ImportArr']['Reference']['Ch'];
	}
}

$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentCode'] = "代號空白";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentEn'] = "英文評語空白";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentCh'] = "中文評語空白";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['WrongCodeFormat'] = "評語代號開首必須為英文字母";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['TopicCode'] = "<!--TopicLevelName-->代號空白";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['WrongTopicCode'] = "沒有此<!--TopicLevelName-->代號";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['CommentEnExceededWordLimit'] = "英文評語不能多於 <!--MaxWordCount--> 字";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['CommentChExceededWordLimit'] = "中文評語不能多於 <!--MaxWordCount--> 字";

$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportDataInstruction'] = "如果匯入的評語代號已被使用，該代號原來的評語將會被取代。";


# Settings > Admin Group
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NumOfMember'] = "組員數目";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NewAdminGroupStepArr'][1] = "資料及權限";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NewAdminGroupStepArr'][2] = "選擇成員";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupCode'] = "行政小組代號";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupName'] = "行政小組名稱";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupList'] = "行政小組列表";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MemberList'] = "組員列表";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AddMember'] = "加入組員";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupInfo'] = "行政小組資料";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AccessRight'] = "權限";

$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['AddSuccess'] = "1|=|新增行政小組成功。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['AddFailed'] = "0|=|新增行政小組失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['EditSuccess'] = "1|=|編輯行政小組成功。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['EditFailed'] = "0|=|編輯行政小組失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['DeleteSuccess'] = "1|=|刪除行政小組成功。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['DeleteFailed'] = "0|=|刪除行政小組失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['AddMemberSuccess'] = "1|=|已加入行政小組組員。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['AddMemberFailed'] = "0|=|行政小組組員加入失敗。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['DeleteMemberSuccess'] = "1|=|已刪除行政小組組員。";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['DeleteMemberFailed'] = "0|=|行政小組組員刪除失敗。";

$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['WarningArr']['SelectionContainsMember'] = "已選擇的用戶中包含此行政小組的成員";


### Wordings which can be customized in the future
$Lang['eRC_Rubrics']['Customized']['General']['Subject']['En'] = "Subject";
$Lang['eRC_Rubrics']['Customized']['General']['Subject']['Ch'] = "科目";
$Lang['eRC_Rubrics']['Customized']['General']['Result']['En'] = "Result";
$Lang['eRC_Rubrics']['Customized']['General']['Result']['Ch'] = "成績";
$Lang['eRC_Rubrics']['Customized']['General']['ClassTeacherComment']['En'] = "Comment";
$Lang['eRC_Rubrics']['Customized']['General']['ClassTeacherComment']['Ch'] = "評語";
$Lang['eRC_Rubrics']['Customized']['General']['TopicDetailsRemarks']['En'] = "Remarks";
$Lang['eRC_Rubrics']['Customized']['General']['TopicDetailsRemarks']['Ch'] = "細目註釋";
$Lang['eRC_Rubrics']['Customized']['General']['Remarks']['En'] = "Remarks";
$Lang['eRC_Rubrics']['Customized']['General']['Remarks']['Ch'] = "備註";
$Lang['eRC_Rubrics']['Customized']['General']['ThisTerm']['En'] = "This Term";
$Lang['eRC_Rubrics']['Customized']['General']['ThisTerm']['Ch'] = "本學期";
$Lang['eRC_Rubrics']['Customized']['General']['Day']['En'] = "Day";
$Lang['eRC_Rubrics']['Customized']['General']['Day']['Ch'] = "天";
$Lang['eRC_Rubrics']['Customized']['General']['StudentPerformance']['En'] = "Student Performance";
$Lang['eRC_Rubrics']['Customized']['General']['StudentPerformance']['Ch'] = "學生表現";
$Lang['eRC_Rubrics']['Customized']['General']['LearningPerformance']['En'] = "Learning Performance";
$Lang['eRC_Rubrics']['Customized']['General']['LearningPerformance']['Ch'] = "學習表現";
$Lang['eRC_Rubrics']['Customized']['General']['StudentPerformanceRatio']['En'] = "Student Performance Ratio";
$Lang['eRC_Rubrics']['Customized']['General']['StudentPerformanceRatio']['Ch'] = "學生表現比例";

$Lang['eRC_Rubrics']['Customized']['TopicLevel'][1]['En'] = "Learning Topic";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][1]['Ch'] = "學習範疇";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][2]['En'] = "Learning Objective";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][2]['Ch'] = "學習重點";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][3]['En'] = "Learning Details";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][3]['Ch'] = "學習細目";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][1]['En'] = "Therapy Topic";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][1]['Ch'] = "治療範疇";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][2]['En'] = "Therapy Objective";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][2]['Ch'] = "治療重點";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][3]['En'] = "Therapy Details";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][3]['Ch'] = "治療內容";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][1]['En'] = "Learning Topic";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][1]['Ch'] = "學習範疇";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][2]['En'] = "Learning Objective";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][2]['Ch'] = "學習重點";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][3]['En'] = "Learning Details";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][3]['Ch'] = "學習內容";

$Lang['eRC_Rubrics']['Customized']['Signature']['Principal']['En'] = "Principal Signature";
$Lang['eRC_Rubrics']['Customized']['Signature']['Principal']['Ch'] = "校長簽署";
$Lang['eRC_Rubrics']['Customized']['Signature']['ClassTeacher']['En'] = "Class Teacher Signature";
$Lang['eRC_Rubrics']['Customized']['Signature']['ClassTeacher']['Ch'] = "班主任簽署";
$Lang['eRC_Rubrics']['Customized']['Signature']['LinguisticsTeacher']['En'] = "Linguistics Teacher in charge";
$Lang['eRC_Rubrics']['Customized']['Signature']['LinguisticsTeacher']['Ch'] = "負責言語治療師";
$Lang['eRC_Rubrics']['Customized']['Signature']['TeacherInCharge']['En'] = "Teacher in charge";
$Lang['eRC_Rubrics']['Customized']['Signature']['TeacherInCharge']['Ch'] = "負責教師";
$Lang['eRC_Rubrics']['Customized']['Signature']['Parent']['En'] = "Parent Signature";
$Lang['eRC_Rubrics']['Customized']['Signature']['Parent']['Ch'] = "家長簽署";
$Lang['eRC_Rubrics']['Customized']['Signature']['Date']['En'] = "Date of Signature";
$Lang['eRC_Rubrics']['Customized']['Signature']['Date']['Ch'] = "簽署日期";

$Lang['eRC_Rubrics']['Customized']['HeaderData']['StudentName']['En'] = "Student Name";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['StudentName']['Ch'] = "學生姓名";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ClassName']['En'] = "Class";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ClassName']['Ch'] = "班別";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['FormName']['En'] = "Form";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['FormName']['Ch'] = "班級";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ModuleName']['En'] = "Module Name";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ModuleName']['Ch'] = "單元名稱";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ModuleDateRange']['En'] = "Date";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ModuleDateRange']['Ch'] = "日期";

$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['summary']['En'] = "Summary Info";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['summary']['Ch'] = "總結資料";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['award']['En'] = "Awards Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['award']['Ch'] = "本學期獎項紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['merit']['En'] = "Merits & Demerits Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['merit']['Ch'] = "獎懲紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['eca']['En'] = "ECA Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['eca']['Ch'] = "課外活動紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['remark']['En'] = "Remark";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['remark']['Ch'] = "備註";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['interschool']['En'] = "Inter-school Competitions Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['interschool']['Ch'] = "聯校比賽紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['schoolservice']['En'] = "School Service Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['schoolservice']['Ch'] = "學校服務紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['attendance']['En'] = "Attendance Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['attendance']['Ch'] = "考勤紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['demerit']['En'] = "Punishment Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['demerit']['Ch'] = "懲罰紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['OLE']['En'] = "OLE Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['OLE']['Ch'] = "其他學習經歷紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['post']['En'] = "Post Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['post']['Ch'] = "職責";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['others']['En'] = "Others Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['others']['Ch'] = "其他紀錄";

$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Total Number of School Days']['En'] = "Present";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Total Number of School Days']['Ch'] = "出席";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Days Absent']['En'] = "Days Absent";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Days Absent']['Ch'] = "缺席";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Time Late']['En'] = "Times Late";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Time Late']['Ch'] = "遲到";

$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Addresss'] = "校址"; 
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Phone'][0] = "電話: 26700800(校務處/教員室)";
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Phone'][1] = "26569697(社工室/醫療室)";
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Phone'][2] = "26700824(校長室)";
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['WebSite'] = "網址";
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Email'] = "電郵";

$Lang['eRC_Rubrics']['Template']['RemarksArr']['A'] = "【A】— 做到全部/ 自行完成";
$Lang['eRC_Rubrics']['Template']['RemarksArr']['B'] = "【B】— 做到大部份/ 在口頭提示下完成";
$Lang['eRC_Rubrics']['Template']['RemarksArr']['C'] = "【C】— 做到一半/ 口頭加動作下提示完全";
$Lang['eRC_Rubrics']['Template']['RemarksArr']['D'] = "【D】— 做到少許/ 教師協助下完成";
?>