<?php 

#### eBooking App [START]
$Lang['eBooking']['App']['DataTable']['DataNotFound'] = '沒有紀錄';
$Lang['eBooking']['App']['DataTable']['PageInfo'] = '第 _PAGE_ / _PAGES_ 頁';
$Lang['eBooking']['App']['DataTable']['EmptyPageInfo'] = '沒有紀錄';
$Lang['eBooking']['App']['DataTable']['RecordPerPage'] = '每頁顯示  _MENU_ 項';
$Lang['eBooking']['App']['DataTable']['PageInfoFilter'] = '(已搜尋 _MAX_ 紀錄)';
$Lang['eBooking']['App']['DataTable']['Search'] = '搜尋: ';
$Lang['eBooking']['App']['DataTable']['Next'] = '下一頁';
$Lang['eBooking']['App']['DataTable']['Previous'] = '上一頁';
$Lang['eBooking']['App']['DataTable']['Loading'] = '載入中...';
$Lang['eBooking']['App']['DataTable']['Processing'] = '處理中';
$Lang['eBooking']['App']['FieldTitle']['eBooking'] = '電子資源預訂';
$Lang['eBooking']['App']['FieldTitle']['MyBooking'] = '我的預訂';
$Lang['eBooking']['App']['FieldTitle']['NewBooking'] = '新增預訂';
$Lang['eBooking']['App']['FieldTitle']['EditBooking'] = "編輯預訂";
$Lang['eBooking']['App']['FieldTitle']['Step'] = '步驟 <--Step Num-->: ';
$Lang['eBooking']['App']['FieldTitle']['SelectBookingMethod'] = '選擇預訂方法'; //step1a
$Lang['eBooking']['App']['FieldTitle']['SelectBookingTime'] = '選擇預訂時間'; //step2a
$Lang['eBooking']['App']['FieldTitle']['SelectBookingPeriod'] = '選擇預訂時段'; //step2c
$Lang['eBooking']['App']['FieldTitle']['SelectBookingDateNRoom'] = '選擇預訂日期及房間'; //step 2b
$Lang['eBooking']['App']['FieldTitle']['SelectBookingDateNTime'] = '選擇預訂日期及時間(重覆性)'; //step2d
$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingRoom'] = '確認預訂房間'; //step3a 3c
$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingItem'] = '確認預訂物品'; //step3a 3c
$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingTime'] = '確認預訂時間'; //step3b
$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingPeriod'] = '確認預訂時段'; //step 3d
$Lang['eBooking']['App']['FieldTitle']['Finish'] = '完成';//step 4
$Lang['eBooking']['App']['FieldTitle']['BookingDate'] = '預訂日期';
$Lang['eBooking']['App']['FieldTitle']['StartDate'] = '開始日期';
$Lang['eBooking']['App']['FieldTitle']['EndDate'] = '結束日期';
$Lang['eBooking']['App']['FieldTitle']['StartTime'] = '開始時間';
$Lang['eBooking']['App']['FieldTitle']['EndTime'] = '結束時間';
$Lang['eBooking']['App']['FieldTitle']['RepeatFrequency'] = '重覆頻率';
$Lang['eBooking']['App']['FieldTitle']['RepeatRule'] = '重覆規則';
$Lang['eBooking']['App']['FieldTitle']['SelectRoomOrItem'] = '選擇房間/ 物品';
$Lang['eBooking']['App']['FieldTitle']['AvailableTimeSlot'] = '可供預訂時段為: ';
$Lang['eBooking']['App']['FieldTitle']['NumOfAvailableTimeSlot'] = '有<--Num-->個可預定時段';
$Lang['eBooking']['App']['FieldTitle']['AvailableTimeSlot2'] = '可預定時段';
$Lang['eBooking']['App']['FieldTitle']['NumOfNotAvailableTimeSlot'] = '有<--Num-->個時段無法預訂';
$Lang['eBooking']['App']['FieldTitle']['NotAvailableTimeSlot2'] = '無法預定時段';

//list
$Lang['eBooking']['App']['FieldTitle']['BookingTime'] = '預訂時間';
$Lang['eBooking']['App']['FieldTitle']['BookedBy'] = '預訂人';
$Lang['eBooking']['App']['FieldTitle']['Status'] = '預訂狀態';
$Lang['eBooking']['App']['FieldTitle']['Action'] = '預訂動作';

$Lang['eBooking']['App']['FieldTitle']['Room'] = '房間';
$Lang['eBooking']['App']['FieldTitle']['Item'] = '物品';
$Lang['eBooking']['App']['FieldTitle']['AppointType'] = '預訂指定房間/物品';
$Lang['eBooking']['App']['FieldTitle']['AppointDate'] = '否';
$Lang['eBooking']['App']['FieldTitle']['AppointRoom'] = '是';
$Lang['eBooking']['App']['FieldTitle']['OnceOnly'] = '一次性';
$Lang['eBooking']['App']['FieldTitle']['Repeated'] = '重複性';
$Lang['eBooking']['App']['FieldTitle']['AppointedPeriod'] = '指定時間';
$Lang['eBooking']['App']['FieldTitle']['AppointedTimeslot'] = '指定課節';
$Lang['eBooking']['App']['FieldTitle']['Timeslot'] = '課節';
$Lang['eBooking']['App']['FieldTitle']['ResourcesType'] = '資源類別';
$Lang['eBooking']['App']['FieldTitle']['BookingTypeRepeat'] = '預訂種類';
$Lang['eBooking']['App']['FieldTitle']['BookingMethod'] = '預訂方法';
$Lang['eBooking']['App']['FieldTitle']['CycleDay'] = '循環日<--Num-->';
$Lang['eBooking']['App']['FieldTitle']['EditRemark'] = '修改備註';

$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['RejectReason'] = '拒絕預訂原因';
$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['101'] = '非可預訂時段';
$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['201'] = '所選房間已被預訂';
$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['301'] = '沒有權限預訂';
$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['999'] = '不明原因';

$Lang['eBooking']['App']['FieldTitle']['DeleteConfirm'] = '確定刪除';
$Lang['eBooking']['App']['FieldTitle']['DeleteConfirm2'] = '你是否確定刪除此紀錄?';
$Lang['eBooking']['App']['FieldTitle']['Delete'] = '刪除';
$Lang['eBooking']['App']['FieldTitle']['Close'] = '取消';

$Lang['eBooking']['App']['FieldTitle']['More'] = '更多 +';
$Lang['eBooking']['App']['FieldTitle']['Attach'] = '附件';
$Lang['eBooking']['App']['FieldTitle']['Remarks'] = '備註';
$Lang['eBooking']['App']['FieldTitle']['Every'] = '逢 ';

$Lang['eBooking']['App']['Selector']['PleaseSelect'] = '請選擇';
$Lang['eBooking']['App']['Selector']['weekday'] = '每星期';
$Lang['eBooking']['App']['Selector']['cycleday'] = '每循環日';
$Lang['eBooking']['App']['Selector']['NoAvailableRoom'] = '沒有可預定房間';

$Lang['eBooking']['App']['weekday'] = array(
		'Mon' => '星期一',
		'Tue' => '星期二',
		'Wed' => '星期三',
		'Thu' => '星期四',
		'Fri' => '星期五',
		'Sat' => '星期六'
);

$Lang['eBooking']['App']['cycleday'] = array(
		'1' => '循環日一',
		'2' => '循環日二',
		'3' => '循環日三',
		'4' => '循環日四',
		'5' => '循環日五',
		'6' => '循環日六',
		
);

$Lang['eBooking']['App']['Button']['Continue'] = '繼續';
$Lang['eBooking']['App']['Button']['Cancel'] = '上一步';
$Lang['eBooking']['App']['Button']['RealCancel'] = '取消';
$Lang['eBooking']['App']['Button']['PrevStep'] = '上一步';
$Lang['eBooking']['App']['Button']['Back'] = '返回';

$Lang['eBooking']['App']['Result']['Success'] = '申請成功';
$Lang['eBooking']['App']['Result']['Fail'] = '申請失敗';
#### eBooking APP [END]
?>