<?php 
// using:   
/*
 * ############### 這個模组使用 UTF-8 ###############
 * Remarks:
 * 1) Arrange the lang variables in alphabetical order
 * 2) Keep the same line break between en and b5 lang files (so that same lang variable is in the same line in different lang files)
 * 3) Use single quote for the array keys and double quote for the lang value
 */

// D
$Lang['DigitalChannels']['DigitalChannelsName'] = "Digital Channels";

// C
$Lang['DigitalChannels']['Category']['Title'] = "Categories";

// F
$Lang['DigitalChannels']['Favorites']['Title'] = "Favorites";

// G
$Lang['DigitalChannels']['General']['Select'] = "Select";
$Lang['DigitalChannels']['General']['Edit'] = "Edit";
$Lang['DigitalChannels']['General']['Remove'] = "Remove";
$Lang['DigitalChannels']['General']['Submit'] = "Submit";
$Lang['DigitalChannels']['General']['Cancel'] = "Cancel";
$Lang['DigitalChannels']['General']['ViewBy'] = "View by:";
$Lang['DigitalChannels']['General']['Or'] = "or";
$Lang['DigitalChannels']['General']['To'] = "to";
$Lang['DigitalChannels']['General']['Back'] = "Back";

$Lang['DigitalChannels']['General']['Total'] = "Total";
$Lang['DigitalChannels']['General']['Category'] = "Categories";
$Lang['DigitalChannels']['General']['CategoriesInTotal'] = "%s category(ies) in total";
$Lang['DigitalChannels']['General']['Album'] = "Album(s)";
$Lang['DigitalChannels']['General']['AlbumsInTotal'] = "%s album(s) in total";
$Lang['DigitalChannels']['General']['Photo'] = "Photo(s)";
$Lang['DigitalChannels']['General']['PhotosInTotal'] = "%s photo(s) in total";
$Lang['DigitalChannels']['General']['more'] = "more";
$Lang['DigitalChannels']['General']['MostHitPhoto'] = "Most Hit Photo";
$Lang['DigitalChannels']['General']['MostHitVideo'] = "Most Hit Video";
$Lang['DigitalChannels']['General']['LastestPhoto'] = "Latest Photo / Video";
$Lang['DigitalChannels']['General']['LastestAlbum'] = "Latest Album";
$Lang['DigitalChannels']['General']['Recommend'] = "Highlight";
$Lang['DigitalChannels']['General']['MostPopular'] = "Most Popular";
$Lang['DigitalChannels']['General']['JustNow'] = "just now";
$Lang['DigitalChannels']['General']['SecondAgo'] = "second ago";
$Lang['DigitalChannels']['General']['MinuteAgo'] = "minute ago";
$Lang['DigitalChannels']['General']['HourAgo'] = "hour ago";
$Lang['DigitalChannels']['General']['DayAgo'] = "day ago";
$Lang['DigitalChannels']['General']['WeekAgo'] = "week ago";
$Lang['DigitalChannels']['General']['MonthAgo'] = "month ago";
$Lang['DigitalChannels']['General']['YearAgo'] = "year ago";
$Lang['DigitalChannels']['General']['SecondsAgo'] = "seconds ago";
$Lang['DigitalChannels']['General']['MinutesAgo'] = "minutes ago";
$Lang['DigitalChannels']['General']['HoursAgo'] = "hours ago";
$Lang['DigitalChannels']['General']['DaysAgo'] = "days ago";
$Lang['DigitalChannels']['General']['WeeksAgo'] = "weeks ago";
$Lang['DigitalChannels']['General']['MonthsAgo'] = "months ago";
$Lang['DigitalChannels']['General']['YearsAgo'] = "years ago";
$Lang['DigitalChannels']['General']['Search'] = "Search";
$Lang['DigitalChannels']['General']['AdvanceSearch'] = "Advanced";
$Lang['DigitalChannels']['General']['SearchResult'] = "Search Result";
$Lang['DigitalChannels']['General']['All'] = "All";
$Lang['DigitalChannels']['General']['Video'] = "Video";
$Lang['DigitalChannels']['General']['Photo'] = "Photo";
$Lang['DigitalChannels']['General']['Type'] = "Type";
$Lang['DigitalChannels']['General']['FileType'] = "File Type";
$Lang['DigitalChannels']['General']['PhotoVideo'] = "Photo / Video";
$Lang['DigitalChannels']['General']['Album2'] = "Album";
$Lang['DigitalChannels']['General']['AlbumTitle'] = "Album Title";
$Lang['DigitalChannels']['General']['AlbumDescription'] = "Album Description";
$Lang['DigitalChannels']['General']['PhotoDescription'] = "Photo Description";
$Lang['DigitalChannels']['General']['Embed'] = "Embed";
$Lang['DigitalChannels']['General']['New'] = "New";
$Lang['DigitalChannels']['General']['Add'] = "Add";
$Lang['DigitalChannels']['General']['QuickAdd'] = "Quick Add";

//M
$Lang['DigitalChannels']['Msg']['AreYouSureTo'] = "Are you sure to ";
$Lang['DigitalChannels']['Msg']['AllPhotosWillBeRemoved']	= "All photos will be removed";
$Lang['DigitalChannels']['Msg']['StopUploadingPhotos']	= "stop uploading photo";
$Lang['DigitalChannels']['Msg']['BrowserReminder']	= "Please use Firefox, Google Chrome, Internet Explorer 10 or higher browser on this module";
$Lang['DigitalChannels']['Msg']['RemoveAlbum'] = "Remove the album?";
$Lang['DigitalChannels']['Msg']['CannotRemoveAlbum'] = "This category which contains album(s) cannot be deleted";
$Lang['DigitalChannels']['Msg']['enterComment'] = "Please enter a comment.";
$Lang['DigitalChannels']['Msg']['RemoveComment'] = "Remove the comment?";

$Lang['DigitalChannels']['General']['NoRecord']	= "No Record";
$Lang['DigitalChannels']['General']['DaysAgo']	= "day(s) ago";
// S
$Lang['DigitalChannels']['Settings']['Title'] = "Admin Settings";
$Lang['DigitalChannels']['Settings']['BasicSettings'] = "Basic Settings";
$Lang['DigitalChannels']['Settings']['Category'] = "Category";
$Lang['DigitalChannels']['Settings']['KeepOriginalPhoto'] = "Keep original photo";
$Lang['DigitalChannels']['Settings']['AllowDownloadOriginalPhoto']  = "Allow user download original photo";
$Lang['DigitalChannels']['Settings']['AllowUserToComment']  = "Allow user to comment";
$Lang['DigitalChannels']['Settings']['CategoryCode'] = "Category Code";
$Lang['DigitalChannels']['Settings']['DescriptionEn'] = "English Description";
$Lang['DigitalChannels']['Settings']['DescriptionChi'] = "Chinese Description";
$Lang['DigitalChannels']['Settings']['Msg']['DuplicatedCategoryCode'] = 'Duplicated Category Code';
$Lang['DigitalChannels']['Settings']['PleaseFillInEnglish'] = "Please use English to fill in";
$Lang['DigitalChannels']['Settings']['Helper'] = "helper";
$Lang['DigitalChannels']['Settings']['UserType'] = "User identity";
$Lang['DigitalChannels']['Settings']['SupportStaff'] = "Non-teaching Staff";
$Lang['DigitalChannels']['Settings']['TeachingStaff'] = "Teacher";
$Lang['DigitalChannels']['Settings']['SelectClass'] = "Select class";
$Lang['DigitalChannels']['Settings']['SelectedUser'] = "Selected user(s)";
$Lang['DigitalChannels']['Settings']['CtrlMultiSelectMessage'] = "Use CTRL to select multiple items";
$Lang['DigitalChannels']['Settings']['Or'] = ' or ';
$Lang['DigitalChannels']['Settings']['SearchUser'] = "Search user";

// O
$Lang['DigitalChannels']['Organize']['Title'] = "Organize";
$Lang['DigitalChannels']['Organize']['ChiDescriptionHere'] = "輸入描述";
$Lang['DigitalChannels']['Organize']['DescriptionHere'] = "Description here";
$Lang['DigitalChannels']['Organize']['EngDescriptionHere'] = "Description here";
$Lang['DigitalChannels']['Organize']['ChiEventTitleHere'] = "輸入賽事";
$Lang['DigitalChannels']['Organize']['EngEventTitleHere'] = "Event title";
$Lang['DigitalChannels']['Organize']['EventTitleHere'] = "Event title";
$Lang['DigitalChannels']['Organize']['SetAsCoverPhoto']		= "Set as Cover Photo";
$Lang['DigitalChannels']['Organize']['RemoveAlbum']		= "Remove Album";
$Lang['DigitalChannels']['Organize']['UntitledAlbum'] = "Untitled Album";
$Lang['DigitalChannels']['Organize']['NewAlbum']		= "New Album";
$Lang['DigitalChannels']['Organize']['RemoveFile']		= "Remove File";

$Lang['DigitalChannels']['Organize']['Category'] = "Category";
$Lang['DigitalChannels']['Organize']['AlbumCode'] = "Website Code";     // for SFOC
$Lang['DigitalChannels']['Organize']['AlbumChiTitle'] = "相簿標題";
$Lang['DigitalChannels']['Organize']['AlbumEngTitle'] = "Album title";
$Lang['DigitalChannels']['Organize']['AlbumTitle'] = "Album title";
$Lang['DigitalChannels']['Organize']['AlbumTitleEng'] = "Album Title (Eng)";
$Lang['DigitalChannels']['Organize']['AlbumTitleChi'] = "Album Title (Chi)";
$Lang['DigitalChannels']['Organize']['ChiDescription'] = "相簿描述";
$Lang['DigitalChannels']['Organize']['Description'] = "Description";
$Lang['DigitalChannels']['Organize']['DescriptionEng'] = "Album Description (Eng)";
$Lang['DigitalChannels']['Organize']['DescriptionChi'] = "Album Description (Chi)";
$Lang['DigitalChannels']['Organize']['EngDescription'] = "Description";
$Lang['DigitalChannels']['Organize']['PIC'] = "Person in charge";
$Lang['DigitalChannels']['Organize']['EventTitle'] = "Event title";
$Lang['DigitalChannels']['Organize']['EventTitleEng'] = "Event Title (Eng)";
$Lang['DigitalChannels']['Organize']['EventTitleChi'] = "Event Title (Chi)";
$Lang['DigitalChannels']['Organize']['Date'] = "Date";
$Lang['DigitalChannels']['Organize']['EventDate2'] = "Event Date";
$Lang['DigitalChannels']['Organize']['EventDate'] = "Date";
$Lang['DigitalChannels']['Organize']['DateTaken'] = "Date Taken";
$Lang['DigitalChannels']['Organize']['DragPhoto'] = "Drag Resources  here";
$Lang['DigitalChannels']['Organize']['SelectPhotoFrom'] = "Select from your computer";
$Lang['DigitalChannels']['Organize']['TargetGroup'] = "Target group(s)";
$Lang['DigitalChannels']['Organize']['Period'] = "Period";
$Lang['DigitalChannels']['Organize']['StartDate'] = "Start Date";
$Lang['DigitalChannels']['Organize']['EndDate'] = "End Date";
$Lang['DigitalChannels']['Organize']['Private'] = "Private";
$Lang['DigitalChannels']['Organize']['AllUsers'] = "All users";
$Lang['DigitalChannels']['Organize']['Groups'] = "Group(s)";
$Lang['DigitalChannels']['Organize']['Users'] = "Specific User(s)";
$Lang['DigitalChannels']['Organize']['UnsupportedFormat'] = "Unsupported Format";
$Lang['DigitalChannels']['Organize']['FileSizeTooLarge'] = "File size ecxceed";

$Lang['DigitalChannels']['Organize']['Album'] = "Album";
$Lang['DigitalChannels']['Organize']['Category'] = "Category";
$Lang['DigitalChannels']['Organize']['AlbumNum'] = "No. of Album(s)";
$Lang['DigitalChannels']['Organize']['CreateDate'] = "Create Date";
$Lang['DigitalChannels']['Organize']['View'] = "views";
$Lang['DigitalChannels']['Organize']['Enjoy'] = "favorite";
$Lang['DigitalChannels']['Organize']['Comment'] = "comment";
$Lang['DigitalChannels']['Organize']['AddComment'] = "Add a comment";
$Lang['DigitalChannels']['Organize']['Edited'] = "Edited";
$Lang['DigitalChannels']['Organize']['EnjoyThis'] = "favorite this.";
$Lang['DigitalChannels']['Organize']['PreviousComments'] = "View %s previous comments";
$Lang['DigitalChannels']['Organize']['EnjoyThisPhoto'] = "Favorite this photo";
$Lang['DigitalChannels']['Organize']['RemoveEnjoyThisPhoto'] = "Unfavorite this photo";
$Lang['DigitalChannels']['Organize']['DownloadThisPhoto'] = "Download this photo";

//R
$Lang['DigitalChannels']['Remarks']['SupportUploadFormat'] = "Support photos in JPG/PNG/GIF format and videos in MP4/3GP/WMV/MOV/RM/FLV format";
$Lang['DigitalChannels']['Remarks']['MaxFileSize'] = "Maximum file size is ";
$Lang['DigitalChannels']['Remarks']['PicSelection'] = "Apart from the Administrator and creator of this album, PIC can manage this album.";
$Lang['DigitalChannels']['Remarks']['FailToUpload'] = "Fail to Upload";
$Lang['DigitalChannels']['Remarks']['MediaConvert'] = "Converting media format。。。";
$Lang['DigitalChannels']['Remarks']['DiscardQuickAdd'] = "Are you sure to discard this upload?";
$Lang['DigitalChannels']['Remarks']['DiscardUploadMedia'] = "You might lost the media. Continue?";
$Lang['DigitalChannels']['Remarks']['DiscardThisAlbum'] = "Remove this album?";
$Lang['DigitalChannels']['Remarks']['RecAlbumDateRange'] = "The Highlight cannot be shown if the date range is not set";
$Lang['DigitalChannels']['Remarks']['CategoryPicSelection'] = "Helper can manage album of the category.";
$Lang['DigitalChannels']['Recommend']['Title'] = "Highlight Management";
$Lang['DigitalChannels']['Recommend']['NewReommendation'] = "New Highlight";
$Lang['DigitalChannels']['Recommend']['Reommendation'] = "Highlight";
$Lang['DigitalChannels']['Recommend']['ReommendationInTotal'] = "%s highlight(s) in total";
$Lang['DigitalChannels']['Recommend']['RemoveRecommendation'] = "Remove Highlight";
$Lang['DigitalChannels']['Recommend']['RecommendTitle'] = "Title";
$Lang['DigitalChannels']['Recommend']['RecommendDescription'] = "Description";
$Lang['DigitalChannels']['Recommend']['NumberOfSelect'] = "Number of select";
$Lang['DigitalChannels']['Recommend']['SortBy'] = "Sort By";
$Lang['DigitalChannels']['Recommend']['LastModified'] = "Last Modified";
$Lang['DigitalChannels']['Recommend']['DateUploaded'] = "Date Uploaded";
$Lang['DigitalChannels']['Recommend']['just']  = "just";
$Lang['DigitalChannels']['Recommend']['a_few_seconds_ago']  = "a few seconds ago";
$Lang['DigitalChannels']['Recommend']['a_minute_ago']  = "a minute ago";
$Lang['DigitalChannels']['Recommend']['minutes_ago']  = "minutes ago";
$Lang['DigitalChannels']['Recommend']['1_hour_ago']  = "1 hour ago";
$Lang['DigitalChannels']['Recommend']['hours_ago']  = "hours ago";
$Lang['DigitalChannels']['Recommend']['day_ago']  = "day(s) ago";
$Lang['DigitalChannels']['Recommend']['year_ago']  = "year(s) ago";
$Lang['DigitalChannels']['Recommend']['UntitledRecAlbum'] = "Untitled Recommedation";

// Others, for UTF-8 wording overwrite (in EJ)
$button_cancel = "Cancel";
$button_reset = "Reset";
$button_submit = "Submit";
$i_general_collapse_all = "Collapse All";
$i_general_expand_all = "Expand All";
$i_junior_leftmenu_hide = "H<br/>I<br/>D<br/>E";

$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderSuccess'] = "1|=|Category Re-ordered Successfully.";
$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderFailed'] = "0|=|Category Re-order Failed.";

$Lang['General']['Loading'] = 'Loading...';
$Lang['General']['Yes'] = 'Yes';
$Lang['General']['No'] = 'No';
$Lang['General']['RequiredField'] = "<span class='tabletextrequire'>*</span> Mandatory field(s)";
$Lang['General']['Settings'] = "Settings";
$Lang['General']['NoRecordAtThisMoment'] = "There is no record at this moment.";

$Lang['General']['ReturnMessage']['UpdateSuccess'] = '1|=|Record Updated.';
$Lang['General']['ReturnMessage']['UpdateUnsuccess'] = '0|=|Record Update Failed.';
$Lang['General']['ReturnMessage']['DeleteSuccess'] = '1|=|Record Deleted.';
$Lang['General']['ReturnMessage']['DeleteUnsuccess'] = '0|=|Record Delete Failed.';

$Lang['Btn']['Delete'] = "Delete";
$Lang['Btn']['Edit'] = 'Edit';
$Lang['Btn']['New'] = 'New';
$Lang['Btn']['Submit'] = 'Submit';
$Lang['Btn']['Cancel'] = 'Cancel';
$Lang['Btn']['AddAll'] = 'Add All';
$Lang['Btn']['AddSelected'] = 'Add Selected';
$Lang['Btn']['RemoveSelected'] = 'Remove Selected';
$Lang['Btn']['RemoveAll'] = 'Remove All';

$Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear'] = "-- All School Year --";
?>