<?php
// OLE 
$iPort["internal_record"] = "CIP";
$iPort["internal_record_submision_period"] = "CIP (Submission Period)";
$iPort["external_record"] = "Awards & Achievements";
$iPort["external_recordSetPerios"] = "Awards & Achievements (Submission Period)";
$iPort["OLESetSLPRecord"] = "CIP record selection period for SLP report display (for both '".$iPort["internal_record"]."' and '".$iPort["external_record"]."') ";
$ec_iPortfolio['all_ele'] = 'ALL CIP Components';


# Holistic Report
$ec_iPortfolio['fitness'] = "Fitness";
$ec_iPortfolio['fitness_item_result'] = "Item / Result";
$ec_iPortfolio['FitnessItem']['PushUp'] = "Push Up";
$ec_iPortfolio['FitnessItem']['SitUp'] = "Sit Up";
$ec_iPortfolio['FitnessItem']['Sprint'] = "Sprint";
$ec_iPortfolio['FitnessItem']['Agility'] = "Agility";
$ec_iPortfolio['FitnessItem']['MultiStageFitnessRun'] = "Multi-Stage Fitness Run";

$ec_iPortfolio['fitness_report']['show_or_not'] = "Show Fitness?";
$ec_iPortfolio['fitness_report']['with'] = "With Fitness Result";
$ec_iPortfolio['fitness_report']['without'] = "Without Fitness Result";
?>