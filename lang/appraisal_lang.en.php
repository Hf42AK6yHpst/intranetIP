<?php
// using:
/*
 * Remarks:
* 1) Arrange the lang variables in alphabetical order
* 2) Keep the same line break between en and b5 lang files (so that same lang variable is in the same line in different lang files)
* 3) Use single quote for the array keys and double quote for the lang value
*/

$Lang['Menu']['AccountMgmt']['Management'] = "Management";
$Lang['Menu']['AccountMgmt']['Reports'] = "Reports";
$Lang['Menu']['AccountMgmt']['Settings'] = "Settings";
$Lang['Header']['Menu']['eAppraisal'] = "Teacher Appraisal";

// A
$Lang['Appraisal']['AllMember'] = "All Members";
$Lang['Appraisal']['AllTeacher'] = "All Teachers";
$Lang['Appraisal']['AllForm'] = "All Appraisal Forms";
$Lang['Appraisal']['AppraisalPeriod'] = "Appraisal Scheme";
$Lang['Appraisal']['AppraisalRecords'] = "Fill-in / View";
$Lang['Appraisal']['AppraisalRecordsSetting'] = "View/Edit";
$Lang['Appraisal']['AppraisalRecordsEdit'] = "Fill-in";
$Lang['Appraisal']['AppraisalRecordsView'] = "View";
$Lang['Appraisal']['AppraisalRecordsNeedFill'] = "Need Fill-in";
$Lang['Appraisal']['AppraisalRecordsNeedNotFill'] = "Need not to Fill-in";
$Lang['Appraisal']['AppraisalRecordsModify'] = "Able to request modification";
$Lang['Appraisal']['AROtherStaffAppraisal'] = "Evaluation Forms of Other Colleagues";
$Lang['Appraisal']['ARMyAppraisal'] = "My Appraisal Forms";
$Lang['Appraisal']['ARCycleStart'] = "\"%s\" is in progress, please fill-in and submit the forms listed below:";
$Lang['Appraisal']['ARForm'] = "Appraisal Form";
$Lang['Appraisal']['ARFormFrm'] = "from";
$Lang['Appraisal']['ARFormTo'] = "to";
$Lang['Appraisal']['ARFormEdit'] = "Fill-in/Edit";
$Lang['Appraisal']['ARFormView'] = "View";
$Lang['Appraisal']['ARFormViewMember'] = "View Members";
$Lang['Appraisal']['ARFormStfOwnerName'] = "Appraisee";
$Lang['Appraisal']['ARFormStfGrp'] = "Grouping";
$Lang['Appraisal']['ARFormIdvArrRmk'] = "Remarks";
$Lang['Appraisal']['ARFormMyRole'] = "My Role";
$Lang['Appraisal']['ARFormFormFillAndOrder'] = "Period";
$Lang['Appraisal']['ARFormSubDate'] = "Submitted";
$Lang['Appraisal']['ARFormAction'] = "Evaluation";
$Lang['Appraisal']['ARFormMale'] = "Male";
$Lang['Appraisal']['ARFormFemale'] = "Female";
$Lang['Appraisal']['ARFormTgtName'] = "Name";
$Lang['Appraisal']['ARFormPersonalParticulars'] = "Personal Particulars";
$Lang['Appraisal']['ARFormPersonalEnglishName'] = "Teacher (Eng.)";
$Lang['Appraisal']['ARFormPersonalChineseName'] = "Teacher (Chi.)";
$Lang['Appraisal']['ARFormPersonalGender'] = "Gender";
$Lang['Appraisal']['ARFormPresentJobInformation'] = "Current Job Information";
$Lang['Appraisal']['ARFormJobGrade'] = "Position/Rank";
$Lang['Appraisal']['ARFormGradeEntranceDate'] = "Entry Date for this Position";
$Lang['Appraisal']['ARFormDateHireSchool'] = "Entry Date to this School";
$Lang['Appraisal']['ARFormDateHireOrganization'] = "Entry Date to this School Organization";
$Lang['Appraisal']['ARFormSeniorityGovtSubSchool'] = "Seniority (Gov./Aided)";
$Lang['Appraisal']['ARFormSeniorityPriSchool'] = "Seniority (Private)";
$Lang['Appraisal']['ARFormSeniorityOrganization'] = "Seniority in this School Organization(including this year)";
$Lang['Appraisal']['ARFormQuaAcq'] = "Qualification Acquired / Study-in-Progress during Appraising Period";
$Lang['Appraisal']['ARFormSeq'] = "#";
$Lang['Appraisal']['ARFormQuaAcqQuaDesc'] = "Qualification";
$Lang['Appraisal']['ARFormQuaAcqSts'] = "Status";
$Lang['Appraisal']['ARFormQuaAcqStsVal']["0"] = "0";
$Lang['Appraisal']['ARFormQuaAcqStsVal']["1"] = "1";
$Lang['Appraisal']['ARFormQuaAcqStsDes']["0"] = "Study-in-Progress";
$Lang['Appraisal']['ARFormQuaAcqStsDes']["1"] = "Acquired";
$Lang['Appraisal']['ARFormQuaAcqCmpDat'] = "Completion Date";
$Lang['Appraisal']['ARFormQuaAcqAccBody'] = "Education Institution";
$Lang['Appraisal']['ARFormQuaAcqPrgDtl'] = "Course or Related Information";
$Lang['Appraisal']['ARFormOnMouseEdit'] = "Edit";
$Lang['Appraisal']['ARFormEdit'] = "Edit";
$Lang['Appraisal']['ARFormDelete'] = "Delete";
$Lang['Appraisal']['ARFormAdd'] = "New";
$Lang['Appraisal']['ARFormPubYear'] = "Publishing in this Year";
$Lang['Appraisal']['ARFormExtServ'] = "Service Outside School";
$Lang['Appraisal']['ARFormTotClsCnt'] = "Lessons per Week/Cycle";
$Lang['Appraisal']['ARFormTeachSubj'] = "Subjects Taught";
$Lang['Appraisal']['ARFormCls'] = "Class";
$Lang['Appraisal']['ARFormClsLv'] = "Class Level";
$Lang['Appraisal']['ARFormClsPerCyc'] = "Lessons per Week/Cycle";
$Lang['Appraisal']['ARFormOthAcaDesc'] = "Academic Duty(Remedial Teaching, Subject Affairs)";
$Lang['Appraisal']['ARFormOthAdmDesc'] = "Administrative Duty(Class Teacher, IT Committee, Discipline Committee)";
$Lang['Appraisal']['ARFormOthExtCurDesc'] = "ECA(Club, Activity)";
$Lang['Appraisal']['ARFormOthDutDesc'] = "Others( e.g. PTA, IMC ... etc)";
$Lang['Appraisal']['ARFormNoLesAsb'] = "Lessons Absent";
$Lang['Appraisal']['ARFormNoSubLes'] = "Lessons Substituted";
$Lang['Appraisal']['ARFormNoTimLat'] = "Late Total";
$Lang['Appraisal']['ARFormLeaCat'] = "Leave Type";
$Lang['Appraisal']['ARFormPayLea'] = "Pay Leave";
$Lang['Appraisal']['ARFormNoPayLea'] = "No-pay Leave";
$Lang['Appraisal']['ARFormPayTot'] = "Total Leave";
$Lang['Appraisal']['ARFormSickLeave'] = "Sick Leave";
$Lang['Appraisal']['ARFormOfficialLeave'] = "Annual Leave";
$Lang['Appraisal']['ARFormStudyLeave'] = "Study Leave";
$Lang['Appraisal']['ARFormMaternityLeave'] = "Maternity Leave";
$Lang['Appraisal']['ARFormRemarks'] = "Remarks";
$Lang['Appraisal']['ARFormOptionalRemarks'] = "Remarks (if applicable)";
$Lang['Appraisal']['ARFormFiller'] = "User";
$Lang['Appraisal']['ARFormFillerRole'] = "Role";
$Lang['Appraisal']['ARFormFillerSignature'] = "Password (as signature)";
$Lang['Appraisal']['ARFormAppraiserSignature'] = "Signature of Appraiser";
$Lang['Appraisal']['ARFormAppraiseeSignature'] = "Signature of Appraisee";
$Lang['Appraisal']['ARFormPrincipalSignature'] = "Signature of Principal";
$Lang['Appraisal']['ARFormInvalidSignature'] = "Incorrect password! ";
$Lang['Appraisal']['ARFormSubmission'] = "Form Submission";
$Lang['Appraisal']['ARFormYearClass'] = "Class";
$Lang['Appraisal']['ARFormSubject'] = "Subject";
$Lang['Appraisal']['ARFormSubjectMedium'] = "Medium of Instruction";
$Lang['Appraisal']['ARFormDate'] = "Date";
$Lang['Appraisal']['ARFormPrintDate'] = "Print Date";
$Lang['Appraisal']['ARFormOrganization'] = "Organization";
$Lang['Appraisal']['ARFormCourseSeminarWorkshop'] = "Course / Seminar / Workshop";
$Lang['Appraisal']['ARFormHours'] = "hours";
$Lang['Appraisal']['ARFormCertification'] = "Awarded Diploma/Certificate";
$Lang['Appraisal']['ARFormName'] = "Teacher";
$Lang['Appraisal']['ARFormChiName'] = " (Chi.)";
$Lang['Appraisal']['ARFormEngName'] = " (Eng.)";
$Lang['Appraisal']['AppraisalPeriodSetting'] = "Edit";
$Lang['Appraisal']['AppraisalSecAppRoleSetting']="Appraisal Form";
$Lang['Appraisal']['ARFormModification']="Return to Modify";
$Lang['Appraisal']['ARFormModDate']="Period of Modification";
$Lang['Appraisal']['ARFormNature']="Nature";
$Lang['Appraisal']['AppraisalPeriodCopy'] = "Copy";

$Lang['Appraisal']['ARMyObsFrm'] = "My Lesson Records";
$Lang['Appraisal']['AROthObsFrm'] = "Lesson Records to Other Teachers";
$Lang['Appraisal']['ARObsCycleStart'] = "In this appraisal scheme '%s', please fill in academic affairs records:";
$Lang['Appraisal']['ARFormTaskObjective']="Duty Objectives";
$Lang['Appraisal']['ARFormTaskStandard']="Performing Standards";
$Lang['Appraisal']['ARFormTaskAction']="Planning/Activities";
$Lang['Appraisal']['ARFormTaskAssessment']="Effectiveness and reflection";
$Lang['Appraisal']['ARFormTaskAchievement']="Self-Assessment/Achievements";
$Lang['Appraisal']['ARFormTaskParts']=array();
$Lang['Appraisal']['ARFormTaskParts'][1] = "i. Teaching";
$Lang['Appraisal']['ARFormTaskParts'][2] = "ii. Class Management";
$Lang['Appraisal']['ARFormTaskParts'][3] = "iii. Administration";
$Lang['Appraisal']['ARFormTaskParts'][4] = "iv. Professional Development";
$Lang['Appraisal']['ARFormStdTrainComp']="Off-campus activities / competition name";
$Lang['Appraisal']['ARFormStdTrainNo']="No. of Students";
$Lang['Appraisal']['ARFormStdTrainDur']="Hours of Training";
$Lang['Appraisal']['ARFormStdTrainResult']="Result";
$Lang['Appraisal']['ARFormAttendanceCoverDateSingle'] = "Sep to Appraisal Date";
$Lang['Appraisal']['ARFormAttendanceCoverDateTerm1'] = "Days/Times \n(Sep - Apr)";
$Lang['Appraisal']['ARFormAttendanceCoverDateTerm2'] = "Days/Times \n(May - Aug)";
$Lang['Appraisal']['ARFormAttendanceUnit'] = "Unit";
$Lang['Appraisal']['ARFormAttendanceListTotalAfter'] = "Display Total Unit of Leave after this section";
$Lang['Appraisal']['ARFormAttendanceUnitList'][0] = "Day(s)";
$Lang['Appraisal']['ARFormAttendanceUnitList'][1] = "Time(s)";
$Lang['Appraisal']['ARFormAttendanceUnitList'][2] = "Day(s)";
$Lang['Appraisal']['ARFormAttendanceUnitList'][3] = "Section(s)";
$Lang['Appraisal']['ARFormAttendanceColumn'] = "Column";
$Lang['Appraisal']['ARFormAttendanceColumnRelatedDate'] = "Related Time Period";
$Lang['Appraisal']['ARFormAttendanceLeaveTotal'] = "Total Leave";

$Lang['Appraisal']['ARFormCPDHourCategory'] = "Category";
$Lang['Appraisal']['ARFormCPDHourCategoryHour'] = "CPD hours";
$Lang['Appraisal']['ARFormCPDHourHour'] = "hour(s)";
$Lang['Appraisal']['ARFormCPDHourSchoolYear'] = "";
$Lang['Appraisal']['ARFormCPDHourTermA'] = "Hours(Sep - Apr)";
$Lang['Appraisal']['ARFormCPDHourTermB'] = "Hours(May - Aug)";
$Lang['Appraisal']['ARFormCPDHourUndefined'] = "Hours({{START_MONTH}} - {{END_MONTH}})";
$Lang['Appraisal']['ARFormCPDHourTotal'] = "Total Hours";
$Lang['Appraisal']['ARFormCPDFormCourseTitle'] = "Course/Activity Title";
$Lang['Appraisal']['ARFormCPDFormCourseNature'] = "Nature";
$Lang['Appraisal']['ARFormCPDFormCourseContent'] = "Content";
$Lang['Appraisal']['ARFormCPDFormOrgnBody'] = "Organization Body";
$Lang['Appraisal']['ARFormCPDFormStartDate'] = "Start Date";
$Lang['Appraisal']['ARFormCPDFormEndDate'] = "End Date";
$Lang['Appraisal']['ARFormCPDFormCPDMode'] = "CPD Mode";
$Lang['Appraisal']['ARFormCPDFormCPDDomain'] = "CPD Domain(s)";
$Lang['Appraisal']['ARFormCPDFormSubjectRelated'] = "Subject(s)";
$Lang['Appraisal']['ARFormCPDFormCPDHour'] = "CPD Hours";
$Lang['Appraisal']['ARFormCPDFormBasicLawHour'] = "Related to Basic Law";
$Lang['Appraisal']['ARFormCPDMode'][0] = "Structured Learning";
$Lang['Appraisal']['ARFormCPDMode'][1] = "Others";
$Lang['Appraisal']['ARFormCPDDomain'][0] = "I. Teaching & Learning";
$Lang['Appraisal']['ARFormCPDDomain'][1] = "II. Student Development";
$Lang['Appraisal']['ARFormCPDDomain'][2] = "III. School Development";
$Lang['Appraisal']['ARFormCPDDomain'][3] = "IV. Professional Relationships & Services";
$Lang['Appraisal']['ARFormCPDDomain'][4] = "V. Personal Growth & Development";
$Lang['Appraisal']['ARFormCPDDomain'][5] = "VI. Others";
$Lang['Appraisal']['ARFormCPDSubject'][0] = "1. Chinese";
$Lang['Appraisal']['ARFormCPDSubject'][1] = "2. English";
$Lang['Appraisal']['ARFormCPDSubject'][2] = "3. Maths";
$Lang['Appraisal']['ARFormCPDSubject'][3] = "4. General Eduction";
$Lang['Appraisal']['ARFormCPDSubject'][4] = "5. Visual Arts";
$Lang['Appraisal']['ARFormCPDSubject'][5] = "6. Music";
$Lang['Appraisal']['ARFormCPDSubject'][6] = "7. Physical Eduction";
$Lang['Appraisal']['ARFormCPDSubject'][7] = "8. Religion";
$Lang['Appraisal']['ARFormCPDSubject'][8] = "9. STEM";
$Lang['Appraisal']['ARFormCPDPastYearHead'] = "Total CPD house in this 3-year Cycle:";
$Lang['Appraisal']['ARFormCPDPastYearRemark'] = "((According to the EDB requirement, 150 hours are required in a 3-year cycle)";

$Lang['Appraisal']['ARFormExpSchoolName'] = "School Name";
$Lang['Appraisal']['ARFormExpDate'] = "Date";
$Lang['Appraisal']['ARFormExpPost'] = "Post";
$Lang['Appraisal']['ARFormTrainingUni'] = "Institution";
$Lang['Appraisal']['ARFormTrainingDate'] = "Date";
$Lang['Appraisal']['ARFormTrainingAward'] = "Certificate / Diploma / Degree awarded";
$Lang['Appraisal']['ARFormTrainingSbj'] = "Subjects (please specify major / minor)";

// B
$Lang['Appraisal']['BtnTempSave'] = "Save as Draft";
$Lang['Appraisal']['BtnSign'] = "Sign and Submit";
$Lang['Appraisal']['BtnPreview'] = "Preview";
$Lang['Appraisal']['BtnPreviewCover'] = "Preview Cover";
$Lang['Appraisal']['BtnSave'] = "Save";
$Lang['Appraisal']['BtnReOpen'] = "Re-Open Submitted Form";
$Lang['Appraisal']['BtnSaveNext'] = "Save and Continue";
$Lang['Appraisal']['BtnSaveNew'] = "Save and Add New";
$Lang['Appraisal']['BtnReset'] = "Reset";
$Lang['Appraisal']['BtnAddTemplate'] = "New";
$Lang['Appraisal']['BtnChangeTemplateOrder'] = "Edit Display Order";
$Lang['Appraisal']['BtnChangeTemplateOrderInReport'] = "Edit Display Order For Final Report";
$Lang['Appraisal']['BtnAddParSec'] = "New";
$Lang['Appraisal']['BtnAddSubSec'] = "New";
$Lang['Appraisal']['BtnAddQCat'] = "New";
$Lang['Appraisal']['BtnAddLikert'] = "New";
$Lang['Appraisal']['BtnAddMatrix'] = "New";
$Lang['Appraisal']['BtnAddQues'] = "New";
$Lang['Appraisal']['BtnAddAns'] = "New";
$Lang['Appraisal']['BtnAddAccTemplate'] = "Select/Add";
$Lang['Appraisal']['BtnAddStaffGrp'] = "New";
$Lang['Appraisal']['BtnAddStaffIdv'] = "New";
$Lang['Appraisal']['BtnImportLeave'] = "Import Leave Records";
$Lang['Appraisal']['BtnImportAbsLateSub'] = "Import Absent, Lateness & Lessons Substitute Records";
$Lang['Appraisal']['BtnImportSLLateSub'] = "Import Sickness & Lateness Records";
$Lang['Appraisal']['BtnImportAttendance'] = "Import Attendance Records";
$Lang['Appraisal']['BtnImport'] = "Import";
$Lang['Appraisal']['BtbAddBatch'] = "Select/Add";
$Lang['Appraisal']['BtbRlsAppForm'] = "Generate Forms To Corresponding Teachers";
$Lang['Appraisal']['BtbAddCycle'] = "New";
$Lang['Appraisal']['BtbConRlsCycle'] = "Generate forms to corresponding staff, confirm proceed?";
$Lang['Appraisal']['BtbConRlsCycleObs'] = "Public Academic Affairs Form(s) to staff, confirm proceed?";
$Lang['Appraisal']['BtbConExportAppraisalList'] = "Export Appraisal Reports, confirm proceed?";
$Lang['Appraisal']['BtbDelItem'] = "Are you sure you want to delete?";
$Lang['Appraisal']['BtbSaveAsGrp']= "Save as New Grouping";
$Lang['Appraisal']['BtnModify'] = "Return to Modify";
$Lang['Appraisal']['BtnImportCPDFromTeacherPortfolio'] = "Synchronize with Teacher Porfolio";
$Lang['Appraisal']['BtnImportStaffAttendance'] = "Synchronize with eAttendance";

$Lang['Appraisal']['BtnRptGen'] = "Submit";
$Lang['Appraisal']['BtnRptCal'] = "Calculate Result";
$Lang['Appraisal']['BtnAddAns'] = "New";
$Lang['Appraisal']['BtbSaveAsObs'] = "Save as New";
$Lang['Appraisal']['BtbRlsObsAppForm'] = "Publish Academic Affairs Form(s)";
$Lang['Appraisal']['BtbAddObsFrm'] = "New Academic Affairs Result";
$Lang['Appraisal']['BtbSaveAsObs'] = "Save as New";
$Lang['Appraisal']['BtnReportRelease'] = "Release reports";
// C
$Lang['Appraisal']['Completed']="Completed";
$Lang['Appraisal']['Cycle']['AcademicYear']="Academic Year";
$Lang['Appraisal']['Cycle']['Descr']="Description";
$Lang['Appraisal']['Cycle']['CycleStart']="Start Date";
$Lang['Appraisal']['Cycle']['CycleClose']="Due Date";
$Lang['Appraisal']['Cycle']['DateCheck'] = "End date must not be ealier than start date!";
$Lang['Appraisal']['Cycle']['Status']="Status";
$Lang['Appraisal']['CycleStatusDes']['NotYetStart']="Pending";
$Lang['Appraisal']['CycleStatusDes']['InProgress']="In Progress";
$Lang['Appraisal']['CycleStatusDes']['Finish']="Complete";
$Lang['Appraisal']['Cycle']['DateTimeRls']="Last Form(s) Generated";
$Lang['Appraisal']['Cycle']['DateCheckOverPeriod'] = "Period cannot be overlapped";
/*
$Lang['Appraisal']['CycleHeader']['Steps1']="考績週期屬性及主要資料";
$Lang['Appraisal']['CycleHeader']['Steps2']="適用評審表格、特別考績角色及遞交批次設定";
$Lang['Appraisal']['CycleHeader']['Steps3']="Grouping for Evaluation";
$Lang['Appraisal']['CycleHeader']['Steps4']="Individual Evaluation (One to One)";
*/

$Lang['Appraisal']['Cycle']['BasicSetting'] = "Appraisal Scheme Information";
$Lang['Appraisal']['Cycle']['AcademicYearID'] = "Academic Year";
$Lang['Appraisal']['Cycle']['AppPrd'] = "Scheme Period";
$Lang['Appraisal']['Cycle']['DescrEng'] = "Appraisal Title (Eng.)";
$Lang['Appraisal']['Cycle']['DescrChi'] = "Appraisal Title (Chi.)";
$Lang['Appraisal']['Cycle']['RlsBy_UserID'] = "Generated by";
$Lang['Appraisal']['Cycle']['AfterCycleStart'] = "Teachers can access their appraisal forms starting on this date.";
$Lang['Appraisal']['Cycle']['AfterCycleClose'] = "Teachers cannot access their appraisal forms starting on this date.";
$Lang['Appraisal']['Cycle']['Remark']="%s Mandatory field(s)";

$Lang['Appraisal']['Cycle']['Guideline'] = "Scheme Reference";
$Lang['Appraisal']['Cycle']['GuideEng'] = "Reference Title (Eng.)";
$Lang['Appraisal']['Cycle']['GuideChi'] = "Reference Title (Chi.)";
$Lang['Appraisal']['Cycle']['GuideFile'] = "Reference File";

$Lang['Appraisal']['Cycle']['RlsInfo'] = "Forms Generation Details";
$Lang['Appraisal']['Cycle']['RlsObsInfo'] = "Academic Affairs Form(s) to be Used by Appraisers";
$Lang['Appraisal']['Cycle']['ExistAcademic'] = "Appraisal scheme for the same school year already exists!";
$Lang['Appraisal']['Cycle']['NotRls'] = "(Forms not yet generated)";
$Lang['Appraisal']['Cycle']['CopyFromCurrentCycle']="Copy This Appraisal Scheme";
$Lang['Appraisal']['Cycle']['CopyToAcademicYear']="Copy for this Academic Year";

$Lang['Appraisal']['Cycle']['AccTemplate'] = "Forms to be Used in this Scheme";
$Lang['Appraisal']['Cycle']['ObsTemplate'] = "Academic Affairs Form(s) to be Used by Appraisers";
$Lang['Appraisal']['CycleTemplate']['InvolvedAppRoleID'] = "Special Role Involved";
$Lang['Appraisal']['CycleTemplate']['DisplayOrder'] = "(Display Order)";
$Lang['Appraisal']['CycleTemplate']['Delete'] = "(Delete)";
$Lang['Appraisal']['CycleTemplate']['NoAssSpecAppRole'] = "(%s roles have no user assigned)";

$Lang['Appraisal']['CycleTemplate']['CurrTemplate'] = "Appraisal Scheme";
$Lang['Appraisal']['CycleTemplate']['AcceptApprsialForm'] = "Appraisal Form";
$Lang['Appraisal']['CycleTemplate']['ApprsialForm'] = "Select From Template";
$Lang['Appraisal']['CycleTemplate']['ArrangeSpecialRole'] = "Special Role";
$Lang['Appraisal']['CycleTemplate']['RelatedApprsialForm'] = "Related Appraisal Form";
$Lang['Appraisal']['CycleTemplate']['ForPreView'] = "For Viewing";

$Lang['Appraisal']['CycleTemplate']['SpecialRole']="Special Role";
$Lang['Appraisal']['CycleTemplate']['SelectedStaff']="Choose";
$Lang['Appraisal']['CycleTemplate']['ArrangedStaff']="Selected";
$Lang['Appraisal']['CycleTemplate']['NewSpecialFormSetting']="New Special Form Settings";
$Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName']="Special Form Setting Name";
$Lang['Appraisal']['CycleTemplate']['SetupSpecialFormSettingUsers']="Manage Special Form Setting Users";
$Lang['Appraisal']['CycleTemplate']['DuplicatedSettingUsers']="Users cannot be in more than one special setting in the same form";

$Lang['Appraisal']['CycleTemplate']['SectionBatch']="Sequence of Access Right Section(s)";
$Lang['Appraisal']['CycleTemplate']['SelectedSection']="Access Right Section";
$Lang['Appraisal']['CycleTemplate']['AppRole']="Fill in by";
$Lang['Appraisal']['CycleTemplate']['Deadline']="Period";
$Lang['Appraisal']['CycleTemplate']['NoTitleSection']="[untitled]";
$Lang['Appraisal']['CycleTemplate']['NoRecordKeep']="Change in form templates and role will affect the fill-in records!";

$Lang['Appraisal']['CycleTemplate']['StfGrpAndArr']="Grouping (Appraiser and Appraisee Matching)";
$Lang['Appraisal']['CycleTemplate']['StfGrp']="Grouping";
$Lang['Appraisal']['CycleTemplate']['StfGrpCnt']="Number of Members";
$Lang['Appraisal']['CycleTemplate']['StfGrpDelete']="(Delete)";
$Lang['Appraisal']['CycleTemplate']['NoGrpDescr']="[untitled]";

$Lang['Appraisal']['CycleTemplate']['StfGrpContent']="Grouping (Appraiser and Appraisee Matching)";
$Lang['Appraisal']['CycleTemplate']['StfGrpDescrEng']="Group Title (Eng.)";
$Lang['Appraisal']['CycleTemplate']['StfGrpDescrChi']="Group Title  (Chi.)";
$Lang['Appraisal']['CycleTemplate']['StfGrpEditFormMode']="Grouping";
$Lang['Appraisal']['CycleTemplate']['StfGrp']="Grouping";
$Lang['Appraisal']['CycleTemplate']['PossibleTemplate']="Forms to be Used in this Scheme";
$Lang['Appraisal']['CycleTemplate']['IncludeTemplate']="Involved Form(s)";
$Lang['Appraisal']['CycleTemplate']['ApprsialMode']="Evaluation Mode";
$Lang['Appraisal']['CycleTemplate']['SelfApprsialMode']="Self-evaluation";
$Lang['Appraisal']['CycleTemplate']['UpToDownApprsialMode']="Leader evaluates members";
$Lang['Appraisal']['CycleTemplate']['DownToUpApprsialMode']="Members evaluate leader";
$Lang['Appraisal']['CycleTemplate']['GroupApprsialMode']="Peer-review";
$Lang['Appraisal']['CycleTemplate']['AssignLeader']="Choose Leader";
$Lang['Appraisal']['CycleTemplate']['AssignMembers']="Choose Members";

$Lang['Appraisal']['CycleTemplate']['StaffIdvAndArr']="Individual Evaluation (One to One)";
$Lang['Appraisal']['CycleTemplate']['PossibleTemplateForm']="Appraisal Form";
$Lang['Appraisal']['CycleTemplate']['AssignedAppraisee']='Appraisee';
$Lang['Appraisal']['CycleTemplate']['SelectedAppraiseeRmk']='Teacher';
$Lang['Appraisal']['CycleTemplate']['SelectedAppraisee']='Selected Appraisee(s)';
$Lang['Appraisal']['CycleTemplate']['AssignedAppraiser']='Appraiser';
$Lang['Appraisal']['CycleTemplate']['SelectedAppraiserRmk']='Choose Appraiser(s)';
$Lang['Appraisal']['CycleTemplate']['SelectedAppraiser']='Selected Appraiser(s)';
$Lang['Appraisal']['CycleTemplate']['StfIdvName']='Appraisee';
$Lang['Appraisal']['CycleTemplate']['StfIdvTemplate']='Appraisal Form';
$Lang['Appraisal']['CycleTemplate']['StfIdvDelete']='(Delete)';

$Lang['Appraisal']['CycleTemplate']['IdvAssRmk']="Remarks";
$Lang['Appraisal']['CycleTemplate']['Rmk']="Remarks";

$Lang['Appraisal']['CycleTemplate']['ApprasialLeave']="Attendance Records";
$Lang['Appraisal']['CycleTemplate']['Attendance']="Attendance Records";
$Lang['Appraisal']['CycleTemplate']['Leave']="Leave Records";
$Lang['Appraisal']['CycleTemplate']['DownloadLeave']="Download";
$Lang['Appraisal']['CycleTemplate']['DownloadAbsence']="Download";
$Lang['Appraisal']['CycleTemplate']['LastUploadLeaveCSV']="Last CSV uploaded";
$Lang['Appraisal']['CycleTemplate']['DownloadAttendance']="Sample CSV (with Sick Leave & Lateness Data of the chosen Appraisal Scheme)";
$Lang['Appraisal']['CycleTemplate']['InfoFile']="Data File";
$Lang['Appraisal']['CycleTemplate']['CSVFile']="(.csv)";
$Lang['Appraisal']['CycleTemplate']['TemplateFile']="Sample CSV";
$Lang['Appraisal']['CycleTemplate']['Column']="Column";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow']="CSV Columns";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow1']="Column A: %s LoginID";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow2']="Column B: Leave Type  %s";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow3']="Column C: Pay Leave %s";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow4']="Column D: No-pay Leave %s";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow5']="Column E: Number of days %s";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow6']="Column F: Leave Remarks";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk1']="(e.g. annual leave, sick leave, maternity leave ... etc)";
$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk2']="(e.g. 12.0, 6.5 ... etc)";
$Lang['Appraisal']['CycleTemplate']['DownloadFiles']="Download";
$Lang['Appraisal']['CycleLeave']['Step1']="Upload";
$Lang['Appraisal']['CycleLeave']['Step2']="Confirmation";
$Lang['Appraisal']['CycleLeave']['Step3']="Result";
$Lang['Appraisal']['CycleLeave']['Remark']="%s Mandatory field(s)";
$Lang['Appraisal']['CycleTemplate']['AbsLateSub']="Absent, Lateness & Lessons Substitute Records";
$Lang['Appraisal']['CycleTemplate']['LastUploadAbsLateSubCSV']="Last CSV uploaded";
$Lang['Appraisal']['CycleExportTemplate']="Download CSV sample";
$Lang['Appraisal']['CycleExportLeaveEng']['Row']="Row";
$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID']="Teacher ID";
$Lang['Appraisal']['CycleExportLeaveEng']['LeaveType']="Leave Type";
$Lang['Appraisal']['CycleExportLeaveEng']['DaysWithPay']="Days With Pay";
$Lang['Appraisal']['CycleExportLeaveEng']['DaysWithoutPay']="Days Without Pay";
$Lang['Appraisal']['CycleExportLeaveEng']['Days']="Days";
$Lang['Appraisal']['CycleExportLeaveEng']['Rmk']="Remark";
$Lang['Appraisal']['CycleExportLeaveEng']['SysRmk']="Admin Remarks";
$Lang['Appraisal']['CycleExportLeaveChi']['Row']="Row ";
$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID']="LoginID";
$Lang['Appraisal']['CycleExportLeaveChi']['LeaveType']="Leave Type";
$Lang['Appraisal']['CycleExportLeaveChi']['DaysWithPay']="Pay Leave";
$Lang['Appraisal']['CycleExportLeaveChi']['DaysWithoutPay']="No-pay Leave";
$Lang['Appraisal']['CycleExportLeaveChi']['Days']="Number of Days";
$Lang['Appraisal']['CycleExportLeaveChi']['Rmk']="Remarks";
$Lang['Appraisal']['CycleExportLeaveChi']['SysRmk']="Admin Remarks";
$Lang['Appraisal']['CycleExportLeave']['Row']="行";
$Lang['Appraisal']['CycleExportLeave']['TeacherID']="LoginID";
$Lang['Appraisal']['CycleExportLeave']['LeaveType']="Leave Type";
$Lang['Appraisal']['CycleExportLeave']['DaysWithPay']="Pay Leave";
$Lang['Appraisal']['CycleExportLeave']['DaysWithoutPay']="No-pay Leave";
$Lang['Appraisal']['CycleExportLeave']['Days']="Number of days";
$Lang['Appraisal']['CycleExportLeave']['Rmk']="Remarks";
$Lang['Appraisal']['CycleExportLeave']['SysRmk']="Admin Remarks";
$Lang['Appraisal']['CycleExportLeaveError']['NoLoginUser']="No such LoginID。";
$Lang['Appraisal']['CycleExportLeaveError']['NoLeaveType']="No such leave type";
$Lang['Appraisal']['CycleExportLeaveError']['NeedNum']="only number is accepted";
$Lang['Appraisal']['CycleExportLeave']['SuccessfulRecord']="Valid";
$Lang['Appraisal']['CycleExportLeave']['FailureRecord']="Invalid";
$Lang['Appraisal']['CycleExportLeave']['SuccessRecord']="%s records have been imported successfully";

$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow']="CSV Columns";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow1']="Column A: %s LoginID";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow2']="Column B: Total number of lessons abent  %s";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow3']="Column C: Total number of lessons substituted %s";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRow4']="Column D: Total number of late %s";
$Lang['Appraisal']['CycleTemplate']['AbsLateSubInfoRmk1']="(e.g. 12, 6 ... etc)";

$Lang['Appraisal']['CycleExportAbsLateSubEng']['Row']="Row";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['TeacherID']="Teacher ID";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumAbsence']="Absence Lessons";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumSubstitution']="Substituted Lessons";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumLateness']="No. of Lateness";
$Lang['Appraisal']['CycleExportAbsLateSubEng']['SysRmk']="Admin Remarks";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['Row']="Row";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['TeacherID']="LoginID";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumAbsence']="Number of Lessons Absent";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumSubstitution']="Number of Lessons Substituted";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumLateness']="Number of Lateness";
$Lang['Appraisal']['CycleExportAbsLateSubChi']['SysRmk']="System Remarks";

$Lang['Appraisal']['CycleExportAbsLateSub']['Row']="Row";
$Lang['Appraisal']['CycleExportAbsLateSub']['TeacherID']="LoginID";
$Lang['Appraisal']['CycleExportAbsLateSub']['NumAbsence']="Absence Lessons";
$Lang['Appraisal']['CycleExportAbsLateSub']['NumSubstitution']="Substituted Lessons";
$Lang['Appraisal']['CycleExportAbsLateSub']['NumLateness']="No. of Late";
$Lang['Appraisal']['CycleExportAbsLateSub']['SysRmk']="Admin Remarks";

$Lang['Appraisal']['CycleExportAbsLateSub']['SuccessfulRecord']="Valid";
$Lang['Appraisal']['CycleExportAbsLateSub']['FailureRecord']="Invalid";

$Lang['Appraisal']['CycleExportAbsLateSubError']['NoLoginUser']="No such LoginID";
$Lang['Appraisal']['CycleExportAbsLateSubError']['NeedNum']="Only number is accepted";

$Lang['Appraisal']['CopyChi'] = "複製";
$Lang['Appraisal']['CopyEng'] = "Copy";
$Lang['Appraisal']['CurrentAcademicYearTilMar'] = "Current academic year (till Mar)";

// D
$Lang['Appraisal']['Drafted']="Drafted";
$Lang['Appraisal']['Deleted'] = "Left";

// E
$Lang['Appraisal']['Error']['TemplateSettingEditRoleMissing'] = "This template is incomplete, you have to set the role first in this template's setting";
$Lang['Appraisal']['EarlyBird'] = "Early Bird";
$Lang['Appraisal']['ExportAppraisalReports'] = "Export Appraisal Reports";

// F

// G
$Lang['Appraisal']['GroupRecord'] = "Group Records";
$Lang['Appraisal']['GroupMember'] = "Member";
$Lang['Appraisal']['GroupRoleUnselected'] = "Please select leaders role";
$Lang['Appraisal']['GroupRecordRead'] = "Read";
$Lang['Appraisal']['GroupRecordUnread'] = "Unread";
$Lang['Appraisal']['GroupModifying'] = "Returned for modification";

// H

// I
$Lang['Appraisal']['ImportInformation']="Data Import";
$Lang['Appraisal']['IronMan'] = "Iron Man/Lady";
// J


// K


// L
$Lang['Appraisal']['LatenessRecord'] = "Lateness Record";
$Lang['Appraisal']['LastAcademicYear'] = "Last academic year";
// LNT
$Lang['Appraisal']['LNT']['LearningAndTeaching'] = "Learning and Teaching";
$Lang['Appraisal']['LNT']['Question'] = "Question";
$Lang['Appraisal']['LNT']['Answer'] = "Answer";
$Lang['Appraisal']['LNT']['Score'] = "Score";
$Lang['Appraisal']['LNT']['Part'] = "Part";
$Lang['Appraisal']['LNT']['QuestionNumber'] = "Question Number";
$Lang['Appraisal']['LNT']['QuestionType'] = "Question Type";
$Lang['Appraisal']['LNT']['AnswerLowerLimit'] = "Lower Limit";
$Lang['Appraisal']['LNT']['AnswerUpperLimit'] = "Upper Limit";
$Lang['Appraisal']['LNT']['AllGeneralSubject'] = "All General Subject";
$Lang['Appraisal']['LNT']['GeneralSubject'] = "General Subject";
$Lang['Appraisal']['LNT']['OtherSubject'] = "Other Subject";
$Lang['Appraisal']['LNT']['AllSubject'] = "All Subject";

$Lang['Appraisal']['LNT']['ImportLnt'] = "Import Learning and Teaching data";
$Lang['Appraisal']['LNT']['ImportType'] = "Import Type";
$Lang['Appraisal']['LNT']['GenerateCsvTemplate'] = "Generate CSV file template";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>Subject<span class=\"tabletextremark\"> (WebSAMSCode)</span>";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>^</span>Subject Name [Ref]";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>Part";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "Part Name";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>Question Number";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>Title";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "<span class='tabletextrequire'>*</span>Type<span class=\"tabletextremark\"> (Value is Scale or Text )</span>";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "Lower Limit";
$Lang['Appraisal']['LNT']['ImportRawQuestion'][] = "Upper Limit";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>*</span>LoginID";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>*</span>Subject<span class=\"tabletextremark\"> (WebSAMSCode)</span>";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>^</span>Subject Name [Ref]";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>*</span>Class";
$Lang['Appraisal']['LNT']['ImportRawAnswer'][] = "<span class='tabletextrequire'>*</span>Answer";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoPart'] = "LoginID cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoQuestionNumber'] = "Question Number cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoQuestionTitle'] = "Title cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoQuestionType'] = "Type cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Question']['InvalidQuestionType'] = "Invalid Type";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoLowerLimit'] = "The Lower Limit of this type cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Question']['InvalidLowerLimit'] = "Lower Limit must be positive integer";
$Lang['Appraisal']['LNT']['ImportError']['Question']['NoUpperLimit'] = "The Upper Limit of this type cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Question']['InvalidUpperLimit'] = "Upper Limit must be positive integer";

$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyUser'] = "User Login cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptySubject'] = "Subject cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyClass'] = "Class cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['NoSubject'] = "No such subject";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['WrongSubject'] = "Wrong subject";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['NoClass'] = "No such class";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['NotIntAnswer'] = "Answer should be a valid integer";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyAnswer'] = "Answer cannot be empty";
$Lang['Appraisal']['LNT']['ImportError']['Answer']['AnswerOverMinMax'] = "Answer must be between lower and upper limit";

$Lang['Appraisal']['LNT']['ImportMsg']['EmptyQuestion'] = "There is no question in selected year";
$Lang['Appraisal']['LNT']['ImportMsg']['DeleteRelatedSubjectAnswer'] = "All related subjects\' answer of selected year will be deleted. Are you sure you want to delete?";
$Lang['Appraisal']['LNT']['ImportMsg']['DeleteAnswer'] = "All answer of selected year, subject and class will be deleted. Are you sure you want to delete?";

$Lang['Appraisal']['LNT']['Report']['ReportType'] = "Report Type";
$Lang['Appraisal']['LNT']['Report']['StatSummary'] = "Statistics Summary";
$Lang['Appraisal']['LNT']['Report']['StatDetails'] = "Statistics Details";
$Lang['Appraisal']['LNT']['Report']['Average'] = "Average";
$Lang['Appraisal']['LNT']['Report']['SD'] = "SD";
$Lang['Appraisal']['LNT']['Report']['Count'] = "Count";
$Lang['Appraisal']['LNT']['ReportMsg']['SelectTeacher'] = "Please select Teacher";
$Lang['Appraisal']['LNT']['ReportMsg']['SelectReportType'] = "Please select Report Type";

// M
$Lang['Appraisal']['Message']['ErrorDelete'] = "Deletion is not allowed as this is in used.";
$Lang['Appraisal']['Message']['ErrorInactive'] ="This Appraisal Form is being used. Cannot be set inactive.";
$Lang['Appraisal']['Message']['Released'] ="Form(s) Generated";
$Lang['Appraisal']['Message']['ConfirmRemoveFormFromCycle'] = "Are you sure you want to remove this form from this Appraisal Period?";
$Lang['Appraisal']['Message']['ConfirmReOpenThisForm'] = "Comfirm reopening this form for edit？";
$Lang['Appraisal']['Message']['ConfirmReOpenThisFormCaution'] = "(Caution：All active submitted form of this template would be editable)";
$Lang['Appraisal']['Message']['SynchronizationCompleted'] = "Synchornization Completed";
$Lang['Appraisal']['Message']['SelectAppraisalScheme'] = "Please select Appraisal Scheme";
$Lang['Appraisal']['Message']['SelectAcademicYear'] = "Please select Academic Year";
$Lang['Appraisal']['Message']['ConfirmReleaseReport'] = "Are you sure you want to release reports of the cycle to all teachers ?";

$Lang['Appraisal']['Month'][1] = "Jan";
$Lang['Appraisal']['Month'][2] = "Feb";
$Lang['Appraisal']['Month'][3] = "Mar";
$Lang['Appraisal']['Month'][4] = "Apr";
$Lang['Appraisal']['Month'][5] = "May";
$Lang['Appraisal']['Month'][6] = "Jun";
$Lang['Appraisal']['Month'][7] = "Jul";
$Lang['Appraisal']['Month'][8] = "Aug";
$Lang['Appraisal']['Month'][9] = "Sep";
$Lang['Appraisal']['Month'][10] = "Oct";
$Lang['Appraisal']['Month'][11] = "Nov";
$Lang['Appraisal']['Month'][12] = "Dec";

$Lang['Appraisal']['ModificationList'] = "Corrections to-do";
$Lang['Appraisal']['Modification']['modification']="Modification Log";
$Lang['Appraisal']['Modification']['return']="Return Log";
$Lang['Appraisal']['Modification']['returnTo']="Returned to";

// N
$Lang['Appraisal']['Message']['NoRecord']="No record";
$Lang['Appraisal']['Message']['NoRecordBrkt']="[No special role is available in Special Role Settings]";
$Lang['Appraisal']['Message']['NoCPDRecord']="There are no CPD records in Teacher Portfolio.";
$Lang['Appraisal']['Message']['NoNewCPDRecord']="No new CPD records are available.";
$Lang['Appraisal']['Message']['CPDImportComplete']="All CPD records from Teacher Portfolio are synced to Teacher Appraisal.";
$Lang['Appraisal']['Message']['DeleteAcademicAffairs']="This action will delete ALL sections of this apprisal form. Are you sure to proceed ?";
$Lang['Appraisal']['Message']['CopyAcademicAffairs']="This apprisal form is going to be copied. Are you sure to proceed ?";
$Lang['Appraisal']['Message']['AttendanceNoReason']="No reason";
// O
$Lang['Appraisal']['Or']="or";
$Lang['Appraisal']['ObservationPeriod'] = "Academic Affairs";
$Lang['Appraisal']['ObservationRecord'] = "Academic Affairs";
$Lang['Appraisal']['ObservationRecordAdd'] = "Add Academic Affairs Result";
$Lang['Appraisal']['ObsRcd']['AddAppraisee'] = "Select Appraisee";
$Lang['Appraisal']['ObsRcd']['AddAppraiser'] = "Select Appraiser(s)";
$Lang['Appraisal']['ObsFormFiller'] = "Appraiser";
$Lang['Appraisal']['ObsFormInvalidSignature'] = "Incorrect password";
$Lang['Appraisal']['ObsFormAtLeastOne'] = "Required one appraiser to sign to submit";
$Lang['Appraisal']['ObsFormSbjMissing'] = "Please select the subject";
$Lang['Appraisal']['ObsFormClassMissing'] = "Please select a class";
$Lang['Appraisal']['ObsFormSigned'] = "Signed by this appraiser";
// P
$Lang['Appraisal']['Pending']="Pending";
$Lang['Appraisal']['PageBreakWhenPrint'] = "Add Page Break after this section when printing";
$Lang['Appraisal']['PreviewRemindSave'] = "Changes in format shown below has not been saved yet. Please remember to save.";

// Q


// R
$Lang['Appraisal']['ReportPersonalAppraisalResult'] = "Individual Report";
$Lang['Appraisal']['ReportOutstandingAppraisalList'] = "Not Submitted List";
$Lang['Appraisal']['ReportCalculationAppraisal'] = "Calculate average mark";
$Lang['Appraisal']['ReportSchoolResultlList'] = "Teacher Appraisal Overall Marksheet";
$Lang['Appraisal']['ReportPersonalAverageList'] = "Personal Statistics Report";
$Lang['Appraisal']['ReportTaskAssignmentChecklist'] = "Task Assignment Checklist";
$Lang['Appraisal']['ReportExportProposedAllocation'] = "Export Proposed School Allocation";
$Lang['Appraisal']['ReportTeachingReview'] = "Teaching Review Report";

$Lang['Appraisal']['ReportPrint'] = "Print";
$Lang['Appraisal']['ReportProcessing'] = "Processing ...";

$Lang['Appraisal']['ReportOutstanding']['Title'] = "Not Submitted List (last modified: %s)";
$Lang['Appraisal']['ReportOutstanding']['NoFrmTitle'] = "[untitled]";
$Lang['Appraisal']['ReportOutstanding']['NoSecTitle'] = "[untitled]";
$Lang['Appraisal']['ReportOutstanding']['NoSecTitle'] = "[untitled]";
$Lang['Appraisal']['ReportOutstanding']['User'] = "Appraisee";
$Lang['Appraisal']['ReportOutstanding']['Template'] = "Appraisal Form";
$Lang['Appraisal']['ReportOutstanding']['Filler'] = "Being filled by";
$Lang['Appraisal']['ReportOutstanding']['CurSec'] = "Responsible";
$Lang['Appraisal']['ReportOutstanding']['SubDatFr'] = "From";
$Lang['Appraisal']['ReportOutstanding']['SubDatTo'] = "To";
$Lang['Appraisal']['ReportOutstanding']['RmnSubDat'] = "Day(s) Left";
$Lang['Appraisal']['ReportOutstanding']['RmnDat'] = "Day(s) Left";
$Lang['Appraisal']['ReportOutstanding']['IDV'] = "Individual Assessment";
$Lang['Appraisal']['ReportOutstanding']['GRP'] = "Group Assessment";
$Lang['Appraisal']['ReportOutstanding']['OBS'] = "Academic Affairs";

$Lang['Appraisal']['ReportPersonResult']['SelectAppraisee']="Appraisee";
$Lang['Appraisal']['ReportPersonResult']['SelectedAppraisee']="Selected Appraisee";
$Lang['Appraisal']['ReportPersonResult']['LoginID']="Search by LoginID";
$Lang['Appraisal']['ReportPersonResult']['InclObs']="include academic affairs record";
$Lang['Appraisal']['ReportPersonResult']['OutputDOC']="Export as Word document";
$Lang['Appraisal']['ReportPersonResult']['DisplayName']="include Appraiser Name";
$Lang['Appraisal']['ReportPersonResult']['DisplayDate']="include Submission Date by Appraiser";
$Lang['Appraisal']['ReportPersonResult']['DisplayMode']="Display Mode";
$Lang['Appraisal']['ReportPersonResult']['DisplayModeSeparate']="Display all appraisal responses";
$Lang['Appraisal']['ReportPersonResult']['DisplayModeGrouping']="Integrated display on responses";
$Lang['Appraisal']['ReportPersonResult']['DisplayWithCover']="Print with cover";

$Lang['Appraisal']['Report']['InvalidFormat']="Invalid format";
$Lang['Appraisal']['Report']['MandatorySelection']="Please choose";
$Lang['Appraisal']['Report']['ReportBeforeCycleEnd']="[Version before End of Appraisal Period]";
$Lang['Appraisal']['Report']['TeacherName']="Name of Teacher";
$Lang['Appraisal']['Report']['SumOfScore']="Total Score";
$Lang['Appraisal']['Report']['AverageMark']="Average";
$Lang['Appraisal']['Report']['ModeSelection']="Mode Selection";
$Lang['Appraisal']['Report']['Total']="Total";
$Lang['Appraisal']['Report']['NotSubmitted']="Not Submitted";
$Lang['Appraisal']['Report']['NoOfPeople']="No. of People";
$Lang['Appraisal']['Report']['ModeShowAll']="All Appraisees";
$Lang['Appraisal']['Report']['ModeShowOne']="Selected Appraisee";
$Lang['Appraisal']['Report']['ModeShowGeneral']="Overall";
$Lang['Appraisal']['Report']['ModeShowIndividual']="Individual Details";
$Lang['Appraisal']['Report']['SpecialDisplaySection']="Independently Calculated Sections";
$Lang['Appraisal']['Report']['SpecialDisplaySectionRemarks']="Sections selected here will be not included in the average score calculation of the related form";

// S
$Lang['Appraisal']['SettingsSpecialRole'] = "Special Role";
$Lang['Appraisal']['SpecialRole'] = "Special Role";
$Lang['Appraisal']['SettingsTemplate'] = "Form Template";
$Lang['Appraisal']['SpecialRolesIsActive'] = "Status";
$Lang['Appraisal']['SpecialRolesIsActiveDes']['0'] = "Inactive";
$Lang['Appraisal']['SpecialRolesIsActiveDes']['1'] = "Active";
$Lang['Appraisal']['SpecialRolesSysUse'] = "Default by System";
$Lang['Appraisal']['SpecialRolesSysUseDes']['0'] = "No";
$Lang['Appraisal']['SpecialRolesSysUseDes']['1'] = "Yes";
$Lang['Appraisal']['SpecialRolesDefRolDes']['1'] = "(Appraiser)";
$Lang['Appraisal']['SpecialRolesDefRolDes']['2'] = "(Appraisee)";

$Lang['Appraisal']['SpecialRolesEngDes'] = "Role Title (Eng.)";
$Lang['Appraisal']['SpecialRolesChiDes'] = "Role Title (Chi.)";
$Lang['Appraisal']['SpecialRolesDisplayOrder'] = "(Display Order)";
$Lang['Appraisal']['SpecialRoles']['AddRoles']="New";
$Lang['Appraisal']['SpecialRoles']['ChangeOrder']="Edit Display Order";
$Lang['Appraisal']['SpecialRoles']['DeleteRoles']="Delete";
$Lang['Appraisal']['SpecialRoles']['Mandatory']="%s Mandatory field(s)";
$Lang['Appraisal']['SearchByLoginID']="Search by LoginID";
$Lang['Appraisal']['SettingsBasicSettings'] = "Basic Settings";
$Lang['Appraisal']['SettingsAppraisalReportCoverSettings'] = "Appraisal Report Cover Settings";
$Lang['Appraisal']['SettingsExcludeTeachers'] = "Teachers Exempt From being assessed";
$Lang['Appraisal']['SettingsCPDRecords'] = "CPD Synchronization Settings";
$Lang['Appraisal']['SettingsCPDRecordsPeriod'] = "Retrieve records in %d years";
$Lang['Appraisal']['SettingsCPDRecordsStarting'] = "Starting from";
$Lang['Appraisal']['SettingsCPDDuration'] = "CPD Time Period Settings";
$Lang['Appraisal']['SettingsCPDDurationRemarks'] = "Time period of the CPD record during the school year.";
$Lang['Appraisal']['SettingsCPDShowingTotalHours'] = "Display Total Hours in CPD";
$Lang['Appraisal']['SettingsAttendanceImport'] = "Attendance Record Synchronization Setting";
$Lang['Appraisal']['SettingsAttendanceReasons'] = "Leaves Records Type Setting";
$Lang['Appraisal']['SettingsColumnReasons'] = "Attendance Information Section";
$Lang['Appraisal']['SettingsColumnOrientation'] = "Please select display mode";
$Lang['Appraisal']['SettingsReportDisplayPerson'] = "User(s) to be able to view report";
$Lang['Appraisal']['SettingsReportDisplayPersonRemarks'] = "Remarks：<br>Non-Teacher Appraisal Admin：can view user-owned report<br>Teacher Appraisal Admin： can view all users report";
$Lang['Appraisal']['SettingsReportDisplayTime'] = "Public release time of personal report";
$Lang['Appraisal']['SettingsReportDoubleCountPerson'] = "User(s) given score to be counted in double weighting";
$Lang['Appraisal']['SettingsPrintMcDisplayMode'] = "Individual Report MC Question Display Mode";
$Lang['Appraisal']['SettingsPrintMcDisplayModeAll'] = "Display all options";
$Lang['Appraisal']['SettingsPrintMcDisplayModeChecked'] = "Display selected option(s)";
$Lang['Appraisal']['Settings']['Language'] = "Language";
$Lang['Appraisal']['Settings']['GroupName'] = "Group Name";
$Lang['Appraisal']['Settings']['SchoolEmblem'] = "School Emblem";
$Lang['Appraisal']['Settings']['SchoolYear'] = "School Year";
$Lang['Appraisal']['Settings']['ReportName'] = "Report Name";
$Lang['Appraisal']['Settings']['TeacherName'] = "Teacher Name";
$Lang['Appraisal']['Settings']['SchoolMotto'] = "School Motto";
$Lang['Appraisal']['Settings']['SchoolMottoLimitation'] = "School motto must be under fifty words.";
$Lang['Appraisal']['Settings']['AllAppRole'] = "All App Role";
$Lang['Appraisal']['Settings']['TraditionalChinese'] = "Chinese";
$Lang['Appraisal']['Settings']['English'] = "English";
$Lang['Appraisal']['Settings']['Display'] = "Display";
$Lang['Appraisal']['Settings']['DisplayChineseName'] = "Display Chinese Name";
$Lang['Appraisal']['Settings']['DisplayEnglishName'] = "Display English Name";
$Lang['Appraisal']['SickLeaveRecord'] = "Sick Leave Record";
$Lang['Appraisal']['SickLeaveAndLateRecords'] = "Sick Leave & Lateness Records";

// T
$Lang['Appraisal']['TemplateSampleForm']="Appraisal Form Template";
$Lang['Appraisal']['TemplateSample']["FormEngName"]="Tile/Code/Aims (Eng.)";
$Lang['Appraisal']['TemplateSample']["FormChiName"]="Tile/Code/Aims (Chi.)";
$Lang['Appraisal']['TemplateSample']["FormStatus"]="Status";
$Lang['Appraisal']['TemplateSample']["ModifiedDate"]="Last Modified";
$Lang['Appraisal']['TemplateSample']["DisplayOrder"]="(Display Order)";
$Lang['Appraisal']['TemplateSampleIsActive']["0"]="Inactive";
$Lang['Appraisal']['TemplateSampleIsActive']["1"]="Active";
$Lang['Appraisal']['TemplateSample']['CopyTemplate']="Copy";
$Lang['Appraisal']['TemplateSample']['DeleteTemplate']="Delete";
$Lang['Appraisal']['TemplateSample']['PDFTitle']="Appraisal Form";
$Lang['Appraisal']['TemplateSample']['Title']="School Defined Form";
$Lang['Appraisal']['TemplateSample']['BasicInformation']="Basic Information";
$Lang['Appraisal']['TemplateSample']['SelectedSection']="Selected Section";
$Lang['Appraisal']['TemplateSample']['FrmTitle']="Form Title";
$Lang['Appraisal']['TemplateSample']['FrmTitleEng']="Form Title (Eng.)";
$Lang['Appraisal']['TemplateSample']['FrmTitleChi']="Form Title (Chi.)";
$Lang['Appraisal']['TemplateSample']['FrmCodEng']="Form Number (Eng.)";
$Lang['Appraisal']['TemplateSample']['FrmCodChi']="Form Number (Chi.)";
$Lang['Appraisal']['TemplateSample']['HdrRefEng']="Reference Code (Eng.)";
$Lang['Appraisal']['TemplateSample']['HdrRefChi']="Reference Code (Chi.)";
$Lang['Appraisal']['TemplateSample']['ObjEng']="Objectives (Eng.)";
$Lang['Appraisal']['TemplateSample']['ObjChi']="Objectives (Chi.)";
$Lang['Appraisal']['TemplateSample']['DescrEng']="Description (Eng.)";
$Lang['Appraisal']['TemplateSample']['DescrChi']="Description (Chi.)";
$Lang['Appraisal']['TemplateSample']['AppTgtEng']="Appraisee Title (Eng.)";
$Lang['Appraisal']['TemplateSample']['AppTgtChi']="Appraisee Title (Chi.)";
$Lang['Appraisal']['TemplateSample']['NeedClass']="User has to select corresponding class?";
$Lang['Appraisal']['TemplateSample']['NeedSubj']="User has to select corresponding subject?";
$Lang['Appraisal']['TemplateSample']['NeedSubjLang']="User has to select corresponding medium of instruction?";
$Lang['Appraisal']['TemplateSample']['Mandatory']="%s Mandatory field(s)";
$Lang['Appraisal']['TemplateSample']['Number']="Only number is accepted.";
$Lang['Appraisal']['TemplateSample']['DecRng03']="Value must be from 0 to 3.";
$Lang['Appraisal']['TemplateSample']['DateOverlap']="Periods overlap";
$Lang['Appraisal']['TemplateSample']['DateOutCycle']="Date range must fall into the scheme's period";
$Lang['Appraisal']['TemplateSample']['DateInvalidRange']="Due date must not be earlier than start date";
$Lang['Appraisal']['TemplateSample']['ModDateInvalidRange']="Due date must not be earlier than start date";
$Lang['Appraisal']['TemplateSample']['DateOutEdit']="Must not outside the submission period!";
$Lang['Appraisal']['TemplateSample']['Status']="Status";
$Lang['Appraisal']['TemplateSampleStatusVal']['0']="Inactive";
$Lang['Appraisal']['TemplateSampleStatusVal']['1']="Active";
$Lang['Appraisal']['TemplateSample']['DataPreload']="Pre-loaded personal information";
$Lang['Appraisal']['TemplateSampleDataPreloadVal']['0']="No";
$Lang['Appraisal']['TemplateSampleDataPreloadVal']['1']="Yes";
$Lang['Appraisal']['TemplateSample']['Remark']="%s Mandatory field(s)";
$Lang['Appraisal']['TemplateSampleDataSelection']['DescrEng']="Chapter Title (Eng.)";
$Lang['Appraisal']['TemplateSampleDataSelection']['DescrChi']="Chapter Title (Chi.)";
$Lang['Appraisal']['TemplateSampleDataSelection']['DisplayOrder']="(Display Order)";
$Lang['Appraisal']['TemplateSampleDataSelection']['Display']="Display in Form";
$Lang['Appraisal']['TemplateSample']['Steps1']="Form Details";
$Lang['Appraisal']['TemplateSample']['Steps2']="Section";
$Lang['Appraisal']['TemplateSample']['Steps3']="Section";
$Lang['Appraisal']['TemplateSample']['Steps4']="Group Question";
$Lang['Appraisal']['TemplateSample']['Steps5']="Question/Criteria";
$Lang['Appraisal']['TemplateSample']['SDFBasicInformation']="Basic Information";
$Lang['Appraisal']['TemplateSample']['SDFSecInclude']= "Division by Access Right";
$Lang['Appraisal']['TemplateSample']['SDFSecRoleAccess']="Role and Access Rights";
$Lang['Appraisal']['TemplateSampleSection']['CodDescrEng']="Access Right Section (Eng.)";
$Lang['Appraisal']['TemplateSampleSection']['CodDescrChi']="Access Right Section (Chi.)";
$Lang['Appraisal']['TemplateSampleSection']['DisplayOrder']="(Display Order)";
$Lang['Appraisal']['TemplateSampleSection']['Delete']="(Delete)";
$Lang['Appraisal']['TemplateSampleSection']['NoTitleAndCod']="[untitled]";
$Lang['Appraisal']['TemplateSample']['CurrentTemplate']="Template";
$Lang['Appraisal']['TemplateSample']['SDFSecBasicInformation']="Information";
$Lang['Appraisal']['TemplateSample']['SDFSecCodEng']="Division Number (Eng.)";
$Lang['Appraisal']['TemplateSample']['SDFSecCodChi']="Division Number (Chi.)";
$Lang['Appraisal']['TemplateSample']['SDFSecTitleEng']="Division Title (Eng.)";
$Lang['Appraisal']['TemplateSample']['SDFSecTitleChi']="Division Title (Chi.)";
$Lang['Appraisal']['TemplateSample']['SDFSecDescrEng']="Division Description (Eng.)";
$Lang['Appraisal']['TemplateSample']['SDFSecDescrChi']="Division Description (Chi.)";
$Lang['Appraisal']['TemplateSample']['SDFSecAppRoleName']="Role";
$Lang['Appraisal']['TemplateSample']['SDFSecCanBrowse']="Read";
$Lang['Appraisal']['TemplateSample']['SDFSecCanFill']="Fill-in";
$Lang['Appraisal']['TemplateSample']['SDFSubSecInclude']="Sections";
$Lang['Appraisal']['TemplateSampleSection']['SubSecCodDescrEng']="Section Number/Title (Eng.)";
$Lang['Appraisal']['TemplateSampleSection']['SubSecCodDescrChi']="Section Number/Title (Chi.)";
$Lang['Appraisal']['TemplateSampleSection']['SubSecDisplayOrder']="(Display Order)";
$Lang['Appraisal']['TemplateSampleSection']['SubSecDelete']="(Delete)";
$Lang['Appraisal']['TemplateSample']['CurrentParentSection']="Access Right Section";
$Lang['Appraisal']['TemplateSample']['SDFSubSecBasicInformation']="Information";
$Lang['Appraisal']['TemplateSample']['SDFSubSecCodEng']="Form Section Number  (Eng.)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecCodChi']="Form Section Number  (Chi.)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecTitleEng']="Form Section Title (Eng.)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecTitleChi']="Form Section Title (Chi.)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecDescrEng']="Form Section Description (Eng.)";
$Lang['Appraisal']['TemplateSample']['SDFSubSecDescrChi']="Form Section Description (Chi.)";
$Lang['Appraisal']['TemplateSample']['SDFQstCat']="Assessment Section";
$Lang['Appraisal']['TemplateSampleQCat']['SDFDescrEng']="Assessment Section Number/Title (Eng.)";
$Lang['Appraisal']['TemplateSampleQCat']['SDFDescrChi']="Assessment Section Number/Title  (Chi.)";
$Lang['Appraisal']['TemplateSampleQCat']['SDFDisplayOrder']="(Display Order)";
$Lang['Appraisal']['TemplateSampleQCat']['SDFDelete']="(Delete)";
$Lang['Appraisal']['TemplateSampleQCat']['NoTitleAndCod']="[untitled]";
$Lang['Appraisal']['TemplateSampleQCat']['ComfirmNoQuesLeave']="Questions has not been set. If this form has released, data inputted in this matrix would not be saved. Are you sure you want to leave?";

$Lang['Appraisal']['TemplateSample']['CurrentSubSection']="Form Section";
$Lang['Appraisal']['TemplateSample']['SDFQCatBasicInformation']="Information";
$Lang['Appraisal']['TemplateSample']['SDFQCatCodEng']="Assessment Section Number (Eng.)";
$Lang['Appraisal']['TemplateSample']['SDFQCatCodChi']="Assessment Section Number (Chi.)";
$Lang['Appraisal']['TemplateSample']['SDFQCatDescrEng']="Assessment Section Tile (Eng.)";
$Lang['Appraisal']['TemplateSample']['SDFQCatDescrChi']="Assessment Section Tile (Chi.)";
$Lang['Appraisal']['TemplateSample']['SDFQCatDisplayMode']="Question Type";
$Lang['Appraisal']['TemplateSample']['SDFTextboxNote']="Unit";
$Lang['Appraisal']['TemplateSample']['SDFAvgMark']="Calculate average mark per assessment criteria/question?";
$Lang['Appraisal']['TemplateSample']['SDFAvgDec']="Decimal Places of Roundup (0-3)";
$Lang['Appraisal']['TemplateSample']['SDFVerAvgMark']="Calculate and display average mark for assessment section?";
$Lang['Appraisal']['TemplateSample']['SDFVerAvgDec']="Decimal Places of Roundup (0-3)";
$Lang['Appraisal']['TemplateSample']['SDFAvg']="Average mark rounded to {0} decimal place(s)";
$Lang['Appraisal']['TemplateSample']['SDFVerAvg']="(Assessment section's average mark rounded to %d decimal places)";

$Lang['Appraisal']['TemplateSample']['0'] = "No";
$Lang['Appraisal']['TemplateSample']['1'] = "Yes";
$Lang['Appraisal']['TemplateSample']['IsObs']="Academic Affairs Use?";


$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['0']="MC/Open-end/Mark";
$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['1']="Likert Scale";
$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['2']="Table Format Question";
$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['0']="(allow questions of different types)";
$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['1']="(various criteria with the same scale)";
$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['2']="(self-defined columns and to be filled cell by cell)";

$Lang['Appraisal']['TemplateSample']['SDFLikertInput']="Scale";
$Lang['Appraisal']['TemplateSampleLikert']['SDFMarkCodEng']="Scale Number (Eng.)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFMarkCodChi']="Scale Number (Chi.)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFDescrEng']="Scale Title (Eng.)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFDescrChi']="Scale Title (Chi.)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFScore']="Equivalent Mark";
$Lang['Appraisal']['TemplateSampleLikert']['SDFDisplayOrder']="(Display Order)";
$Lang['Appraisal']['TemplateSampleLikert']['SDFDelete']="(Delete)";
$Lang['Appraisal']['TemplateSampleLikert']['NoTitleAndCod']="[untitled]";

$Lang['Appraisal']['TemplateSample']['SDFMatrixInput']="Table Columns";
$Lang['Appraisal']['TemplateMatrix']['SDFFldHdrEng']="Column Title (Eng.)";
$Lang['Appraisal']['TemplateMatrix']['SDFFldHdrChi']="Column Title (Chi.)";
$Lang['Appraisal']['TemplateMatrix']['SDFInputType']="Format";
$Lang['Appraisal']['TemplateMatrix']['SDFDisplayOrder']="(Display Order)";
$Lang['Appraisal']['TemplateMatrix']['SDFDelete']="(Delete)";
$Lang['Appraisal']['TemplateMatrix']['SDFInputText']="Text";
$Lang['Appraisal']['TemplateMatrix']['SDFInputMultipleText']="Text (Multiple Line)";
$Lang['Appraisal']['TemplateMatrix']['SDFInputNumeric']="Value";
$Lang['Appraisal']['TemplateMatrix']['SDFInputDate']="Date";
$Lang['Appraisal']['TemplateMatrix']['NoTitleAndCod']="[untitled]";

$Lang['Appraisal']['TemplateSample']['SDFQuestion']="Question/Criteria";
$Lang['Appraisal']['TemplateQuestion']['SDFCodDescrEng']="Number/Title (Eng.)";
$Lang['Appraisal']['TemplateQuestion']['SDFCodDescrChi']="Number/Title (Chi.)";
$Lang['Appraisal']['TemplateQuestion']['SDFDisplayOrder']="(Display Order)";
$Lang['Appraisal']['TemplateQuestion']['SDFDelete']="(Delete)";
$Lang['Appraisal']['TemplateQuestion']['NoTitleAndCod']="[untitled]";

$Lang['Appraisal']['TemplateSample']['SDFAnswer']="Choice";
$Lang['Appraisal']['TemplateAnswer']['SDFCodEng']="Choice Number (Eng.)";
$Lang['Appraisal']['TemplateAnswer']['SDFCodChi']="Choice Number (Chi.)";
$Lang['Appraisal']['TemplateAnswer']['SDFDescrEng']="Choice Title (Eng.)";
$Lang['Appraisal']['TemplateAnswer']['SDFDescrChi']="Choice Title (Chi.)";
$Lang['Appraisal']['TemplateAnswer']['SDFDisplayOrder']="(Display Order)";
$Lang['Appraisal']['TemplateAnswer']['SDFDelete']="(Delete)";
$Lang['Appraisal']['TemplateAnswer']['NoAns']="[untitled]";
$Lang['Appraisal']['TemplateAnswer']['NoAnsDescr']="[untitled]";

$Lang['Appraisal']['TemplateSample']['CurrentQCatSection']="Assessment Section";
$Lang['Appraisal']['TemplateSample']['SDFLikertInputInfo']="Information";
$Lang['Appraisal']['TemplateSample']['SDFMatrixInputInfo']="Cell Column";

$Lang['Appraisal']['TemplateSample']['CurrentQuesSection']="Assessment Question";
$Lang['Appraisal']['TemplateQues']['SDFQuesBasicInformation']="Question Details";
$Lang['Appraisal']['TemplateQues']['SDFQCodEng']="Question Number (Eng.)";
$Lang['Appraisal']['TemplateQues']['SDFQCodChi']="Question Number (Chi.)";
$Lang['Appraisal']['TemplateQues']['SDFDescrEng']="Question Description (Eng.)";
$Lang['Appraisal']['TemplateQues']['SDFDescrChi']="Question Description (Chi.)";
$Lang['Appraisal']['TemplateQues']['SDFDescr']="Question Description";
$Lang['Appraisal']['TemplateQues']['SDFInputType']="Answer Type";
$Lang['Appraisal']['TemplateQues']['SDFTxtBoxNote']="- Unit if necessary (will be displayed after the input box)";
$Lang['Appraisal']['TemplateQues']['SDFHasRmk']="Allow user to input remarks?";
$Lang['Appraisal']['TemplateQues']['SDFRmkLabelEng']="- Remark Title (Eng.)";
$Lang['Appraisal']['TemplateQues']['SDFRmkLabelChi']="- Remark Title (Chi.)";
$Lang['Appraisal']['TemplateQues']['SDFMandatory']="Compulsory Field?";
$Lang['Appraisal']['TemplateQues']['SDFAvgMark']="Calculate average mark per assessment criteria/question?";
$Lang['Appraisal']['TemplateQues']['SDFAvgDec']="Decimal Places of Roundup (0-3)";

$Lang['Appraisal']['TemplateQues']['SDFInputTypeEmpty']="Remarks";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeMC']="MC";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeText']="Short Question";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeScore']="Mark";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeDate']="Date";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeMultiText']="Multiple Line Textbox";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeMCM']="MC (multiple answers)";
$Lang['Appraisal']['TemplateQues']['SDFInputTypeUpload']="Upload Document";

$Lang['Appraisal']['Template']['RecordID']="Record Number";
$Lang['Appraisal']['Template']['BatchID']="Submission Batch Number";

$Lang['Appraisal']['TitleBar']['DownloadDocument']="Instruction";
$Lang['Appraisal']['TitleBar']['FormCompleted']="Completed";
$Lang['Appraisal']['TitleBar']['FormToBeEdited']="To be Edited";

$Lang['Appraisal']['Template']['UsedNoDeletion']="The template has been used, cannot be deleted";
// U

// V

// W

// X

// Y

// Z

if(file_exists($intranet_root."/lang/cust/appraisal_lang.en.php")){
	include_once($intranet_root."/lang/cust/appraisal_lang.en.php");
}
?>