<?php 
// Editing by  

// left menu
$Lang['eSchoolBus']['ManagementArr']['MenuTitle'] = '管理';
$Lang['eSchoolBus']['ManagementArr']['AttendanceArr']['MenuTitle'] = '点名';
$Lang['eSchoolBus']['ManagementArr']['SpecialArrangementArr']['MenuTitle'] = '更改当天安排';
$Lang['eSchoolBus']['ReportArr']['MenuTitle'] = '报告';
$Lang['eSchoolBus']['ReportArr']['ClassListArr']['MenuTitle'] = '各班乘车名单';
$Lang['eSchoolBus']['ReportArr']['RouteArrangementArr']['MenuTitle'] = '校车綫路安排';
$Lang['eSchoolBus']['ReportArr']['StudentListArr']['MenuTitle'] = '乘坐校车学生名单';
$Lang['eSchoolBus']['ReportArr']['AsaContactArr']['MenuTitle'] = 'ASA校车学生联系表';
$Lang['eSchoolBus']['ReportArr']['AsaRouteArr']['MenuTitle'] = 'ASA校车路綫';
$Lang['eSchoolBus']['ReportArr']['PickUpArr']['MenuTitle'] = '校车接送表';
$Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['MenuTitle'] = '指定日期各车乘车名单';
$Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['NotTakeBus'] = '指定日期各车不乘车名单';
$Lang['eSchoolBus']['ReportArr']['StudentListByClassArr']['MenuTitle'] = '指定日期各班乘车名单';
$Lang['eSchoolBus']['SettingsArr']['MenuTitle'] = '设定';
$Lang['eSchoolBus']['SettingsArr']['BusStopsArr']['MenuTitle'] = '地点';
$Lang['eSchoolBus']['SettingsArr']['RouteArr']['MenuTitle'] = '路綫';
$Lang['eSchoolBus']['SettingsArr']['VehicleArr']['MenuTitle'] = '校车';
$Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'] = '乘车学生';
$Lang['eSchoolBus']['SettingsArr']['CutoffTimeArr']['MenuTitle'] = '乘车请假';
$Lang['eSchoolBus']['ViewArr']['MenuTitle'] = '校车管理';
$Lang['eSchoolBus']['ViewArr']['Student']['MenuTitle'] = '检示学生乘车安排';


$Lang['eSchoolBus']['ActivityArr']['ASA'] = 'ASA';
$Lang['eSchoolBus']['ActivityArr']['FootballClass'] = '足球课';
$Lang['eSchoolBus']['ActivityArr']['SwimmingClass'] = '游泳课';
$Lang['eSchoolBus']['ActivityArr']['Orchestra'] = '乐队';
$Lang['eSchoolBus']['ActivityArr']['MUN'] = 'MUN';
$Lang['eSchoolBus']['ActivityArr']['Volleyball'] = '排球';

$Lang['eSchoolBus']['AttendanceStatusArr']['OnTime']='准时';
$Lang['eSchoolBus']['AttendanceStatusArr']['Late']='迟到';
$Lang['eSchoolBus']['AttendanceStatusArr']['Absent']='缺席';
$Lang['eSchoolBus']['AttendanceStatusArr']['Activity']='外出活动';
$Lang['eSchoolBus']['AttendanceStatusArr']['Parent']='家长自接';

//General 
$Lang['eSchoolBus']['General']['From'] = '由';
$Lang['eSchoolBus']['General']['To'] = '至';

//Management
$Lang['eSchoolBus']['Management']['SpecialArrangement']['SpecialArrangementByDates'] = '指定特别日子安排';
$Lang['eSchoolBus']['Management']['SpecialArrangement']['FollowPreviousArrangementForNextRecord'] = '会以对上一个安排作新加。';
$Lang['eSchoolBus']['Management']['SpecialArrangement']['UnableToSetSpecialArrangements'] = '由於未设定平常乘车安排，不能为该学生设定特别乘车安排。';
$Lang['eSchoolBus']['Management']['Attendance']['NoStudents'] = '本站没有相关学生';
$Lang['eSchoolBus']['Management']['Attendance']['SpecialArrangement'] = '行程已被更改';
$Lang['eSchoolBus']['Management']['Attendance']['SpecialArrangement2'] = '特别安排';
$Lang['eSchoolBus']['Management']['Attendance']['HaveASA'] = '本日乘坐ASA路线';

$Lang['eSchoolBus']['Management']['Attendance']['Date'] = '日期';
$Lang['eSchoolBus']['Management']['Attendance']['Route'] = '路綫';
$Lang['eSchoolBus']['Management']['Attendance']['AmPm'] = '上午/下午';
$Lang['eSchoolBus']['Management']['Attendance']['TimeAndStopName'] = '时间 / 站名';
$Lang['eSchoolBus']['Management']['Attendance']['StudentName'] = '学生姓名';
$Lang['eSchoolBus']['Management']['Attendance']['Status'] = '状态';
$Lang['eSchoolBus']['Management']['Attendance']['Remarks'] = '备注';
$Lang['eSchoolBus']['Management']['Attendance']['ContactNumber'] = '联系电话';

//Settings
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameEng'] = '地点 (英文)';
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameChi'] = '地点 (中文)';
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopName'] = '地点';

$Lang['eSchoolBus']['Settings']['Vehicle']['CarNumber'] = '车号';
$Lang['eSchoolBus']['Settings']['Vehicle']['CarPlateNumber'] = '车牌号码';
$Lang['eSchoolBus']['Settings']['Vehicle']['Driver'] = '校车司机';
$Lang['eSchoolBus']['Settings']['Vehicle']['DriverName'] = '姓名';
$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'] = '电话号码';
$Lang['eSchoolBus']['Settings']['Vehicle']['NumberOfSeat'] = '座位数量';
$Lang['eSchoolBus']['Settings']['Vehicle']['Status'] = '状态';
$Lang['eSchoolBus']['Settings']['Vehicle']['Active'] = '使用中';
$Lang['eSchoolBus']['Settings']['Vehicle']['Inactive'] = '已停用';

$Lang['eSchoolBus']['Settings']['Route']['RouteName']='路綫编号';
$Lang['eSchoolBus']['Settings']['Route']['Remarks']='备注';
$Lang['eSchoolBus']['Settings']['Route']['Type']='路綫分类';
$Lang['eSchoolBus']['Settings']['Route']['NormalRoute']='平常路綫';
$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute']='ASA路綫';
$Lang['eSchoolBus']['Settings']['Route']['BusStop']='地点';
$Lang['eSchoolBus']['Settings']['Route']['AmTime']='早上时间';
$Lang['eSchoolBus']['Settings']['Route']['Am']='早上';
$Lang['eSchoolBus']['Settings']['Route']['PmTime']='下午时间';
$Lang['eSchoolBus']['Settings']['Route']['Pm']='下午';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][1]='星期一';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][2]='星期二';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][3]='星期三';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][4]='星期四';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][5]='星期五';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['MON']='星期一';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['TUE']='星期二';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['WED']='星期三';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['THU']='星期四';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['FRI']='星期五';
$Lang['eSchoolBus']['Settings']['Route']['Dates']='日期';
$Lang['eSchoolBus']['Settings']['Route']['StartDate']='开始日期';
$Lang['eSchoolBus']['Settings']['Route']['EndDates']='完结日期';
$Lang['eSchoolBus']['Settings']['Route']['IsActive']='状态';
$Lang['eSchoolBus']['Settings']['Route']['Status']['Active']='使用中';
$Lang['eSchoolBus']['Settings']['Route']['Status']['Inactive']='已停用';
$Lang['eSchoolBus']['Settings']['Route']['NoOfStops']='站点数';
$Lang['eSchoolBus']['Settings']['Route']['TimeScheduled']='适用时段';
$Lang['eSchoolBus']['Settings']['Route']['ValidDates']='有效时间范围';
$Lang['eSchoolBus']['Settings']['Route']['VehicleNum']='使用车号';
$Lang['eSchoolBus']['Settings']['Route']['LastModified']='最後修改';
$Lang['eSchoolBus']['Settings']['Route']['Teacher'] = '负责老师';
$Lang['eSchoolBus']['Settings']['Route']['ArriveOrLeaveTime']='到校/离校时间';
$Lang['eSchoolBus']['Settings']['Route']['LeaveTime']='离校时间';
$Lang['eSchoolBus']['Settings']['Route']['AddTeacher']='增加负责老师';
$Lang['eSchoolBus']['Settings']['Route']['SelectTeacher']='选择负责老师';
$Lang['eSchoolBus']['Settings']['Route']['CtrlMultiSelectMessage'] = "按CTRL选择更多";

$Lang['eSchoolBus']['Settings']['Student']['AM'] = '上午';
$Lang['eSchoolBus']['Settings']['Student']['PM'] = '下午';
$Lang['eSchoolBus']['Settings']['Student']['BusStop'] = '车站';
$Lang['eSchoolBus']['Settings']['Student']['AMBus'] = '上午校车';
$Lang['eSchoolBus']['Settings']['Student']['PMBus'] = '下午校车';
$Lang['eSchoolBus']['Settings']['Student']['ByParent'] = '家长自接';
$Lang['eSchoolBus']['Settings']['Student']['NotTake'] = '不乘坐';
$Lang['eSchoolBus']['Settings']['Student']['Activity'] = '活动';
$Lang['eSchoolBus']['Settings']['Student']['SelectActivity'] = '选择活动';
$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'] = '选择路綫';
$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'] = '选择车站';
$Lang['eSchoolBus']['Settings']['Student']['LeaveBlank'] = '请留空';
$Lang['eSchoolBus']['Settings']['Student']['RowNumber'] = '行数';
$Lang['eSchoolBus']['Settings']['Student']['Errors'] = '错误';
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'] = array();
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Login ID','学生登入编号 (请於学生用户管理查找)。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('AM Normal Route','上午平常路綫 (如不需要乘坐请留空此栏)。','get_route_N_AM');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('AM Normal Route Bus Stop','上午平常路綫车站 。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('PM Normal Route','下午平常路綫 (如不需要乘坐请留空此栏)。','get_route_N_PM');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('PM Normal Route Bus Stop','下午平常路綫车站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route Activity','星期一的ASA活动 (如无活动请留空此栏)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route','星期一的ASA路綫。','get_route_S_MON');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route Bus Stop','星期一的ASA路綫车站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route Activity','星期二的ASA活动 (如无活动请留空此栏)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route','星期二的ASA路綫。','get_route_S_TUE');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route Bus Stop','星期二的ASA路綫车站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route Activity','星期三的ASA活动 (如无活动请留空此栏)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route','星期三的ASA路綫。','get_route_S_WED');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route Bus Stop','星期三的ASA路綫车站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route Activity','星期四的ASA活动 (如无活动请留空此栏)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route','星期四的ASA路綫。','get_route_S_THU');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route Bus Stop','星期四的ASA路綫车站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route Activity','星期五的ASA活动 (如无活动请留空此栏)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route','星期五的ASA路綫。','get_route_S_FRI');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route Bus Stop','星期五的ASA路綫车站 。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Start Date','开始日期 (日期格式为年月日 yyyy-mm-dd)','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('End Date','完结日期 (日期格式为年月日 yyyy-mm-dd)','');

$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound'] = '找不到 <!--TARGET-->。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['RouteIsNotSet'] = '未设定 <!--TARGET--> 的路綫。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['BusStopIsNotSet'] = '未设定 <!--TARGET--> 的车站。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidStartDate'] = '开始日期不正确。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidEndDate'] = '完结日期不正确。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['ExceedBusSeat'] = '该路线的乘车已经满载:';

$Lang['eSchoolBus']['Settings']['ApplyLeave']['ApplyLeaveCutOffTiming'] = '设订截止时间';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['GotoSchool'] = '上学';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['LeaveSchool'] = '放学';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['DaysBeforeApplyLeave'] = '需要提前多少天申请请假纪录';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup'] = "班别组别";

//eService
$Lang['eSchoolBus']['eService']['View']['Student'] = '本月乘车安排';
$Lang['eSchoolBus']['eService']['StudentView']['Time'] = '时间';
$Lang['eSchoolBus']['eService']['StudentView']['Class'] = '班别';
$Lang['eSchoolBus']['eService']['StudentView']['Date'] = '日期';
$Lang['eSchoolBus']['eService']['StudentView']['Year'] = '年';
$Lang['eSchoolBus']['eService']['StudentView']['Month'] = '月';

//Warning
$Lang['eSchoolBus']['Managment']['Attendance']['Warning']['WeekendNotAvailable'] = '周末不适用';

$Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseAtLeastOne']='请选择至少一个地点';
$Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseOnlyOne']='请选择一个地点';

$Lang['eSchoolBus']['Settings']['Vehicle']['Warning']['DuplicatedCarNum'] = '相同的车号已经存在。';
$Lang['eSchoolBus']['Settings']['Vehicle']['Warning']['DuplicatedCarPlateNum'] = '相同的车牌号码已经被其他车辆使用了。';

$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'] = '该路线的乘车已经满载:';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRoute'] = '请选择路綫。';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'] = '请选择车站。';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop'] = '请选择路綫及车站。';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['StudentHasBeenSet'] = '这位学生於本学年已经设定了行车表，请修改该纪录。';

$Lang['eSchoolBus']['Settings']['Warning']['ConfirmDelete']='确定删除?';
$Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty']='项目不能留空';
$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'] = '请输入此数据。';
$Lang['eSchoolBus']['Settings']['Warning']['DuplicatedDate'] = '日期重覆了。';
$Lang['eSchoolBus']['Settings']['Warning']['Duplicate'] = '项目名称已被使用';

//Report
$Lang['eSchoolBus']['Report']['ClassStudentList']['StudentNameList'] = 'Student name list  学生名单';
$Lang['eSchoolBus']['Report']['ClassStudentList']['RegularDays'] = '平常日子';
$Lang['eSchoolBus']['Report']['ClassStudentList']['People'] = '人';
$Lang['eSchoolBus']['Report']['ClassStudentList']['RequestSelectForms'] = '请选择年级。';

$Lang['eSchoolBus']['Report']['RouteArrangement']['Period'] = '时段';
$Lang['eSchoolBus']['Report']['RouteArrangement']['RouteType'] = '路线类型';
$Lang['eSchoolBus']['Report']['RouteArrangement']['ASAWeekday'] = 'ASA 日子';
$Lang['eSchoolBus']['Report']['RouteArrangement']['SelectAll'] = '全选';
$Lang['eSchoolBus']['Report']['RouteArrangement']['NumberOfStudent'] = '人数';
$Lang['eSchoolBus']['Report']['RouteArrangement']['Time'] = '时间';

$Lang['eSchoolBus']['Report']['StudentList']['CarNum'] = '车号';
$Lang['eSchoolBus']['Report']['StudentList']['BusStop'] = '站名';
$Lang['eSchoolBus']['Report']['StudentList']['NumberOfStudent'] = '人数';
$Lang['eSchoolBus']['Report']['StudentList']['StudentName'] = '学生姓名';

$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Bus'] = '校巴';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['RequestSelectBus'] = '请选择校巴。';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Teacher'] = '校车老师';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusRoute'] = '校车路綫';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusNumber'] = '原车号';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Name'] = '姓名';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['FormClass'] = '班级';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['PickupAddress'] = '接送地址';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['ContactPhone'] = '联系电话';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['EmailAddress'] = '邮箱';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['MON'] = '周一';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['TUE'] = '周二';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['WED'] = '周三';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['THU'] = '周四';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['FRI'] = '周五';

$Lang['eSchoolBus']['Report']['Pickup']['SchoolBusList'] = 'BIBA School Bus List 海嘉学校校车接送表';
$Lang['eSchoolBus']['Report']['Pickup']['GeneralSchoolBusList'] = 'School Bus List 校车接送表';
$Lang['eSchoolBus']['Report']['Pickup']['BusNumber'] = 'Bus No. / 班车号';
$Lang['eSchoolBus']['Report']['Pickup']['Route'] = 'Route / 路綫';
$Lang['eSchoolBus']['Report']['Pickup']['BusDriver'] = 'Bus Driver / 校车司机';
$Lang['eSchoolBus']['Report']['Pickup']['BusTeacher'] = 'Bus Teacher / 校车老师';
$Lang['eSchoolBus']['Report']['Pickup']['Date'] = '日期 / Date';
$Lang['eSchoolBus']['Report']['Pickup']['Year'] = '年';
$Lang['eSchoolBus']['Report']['Pickup']['Month'] = '月';
$Lang['eSchoolBus']['Report']['Pickup']['Day'] = '日';
$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['en'] = 'No.';
$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['ch'] = '序';
$Lang['eSchoolBus']['Report']['Pickup']['Name']['en'] = 'Name';
$Lang['eSchoolBus']['Report']['Pickup']['Name']['ch'] = '姓名';
$Lang['eSchoolBus']['Report']['Pickup']['Class']['en'] = 'Class';
$Lang['eSchoolBus']['Report']['Pickup']['Class']['ch'] = '班级';
$Lang['eSchoolBus']['Report']['Pickup']['SignOutSignature']['en'] = 'Sign-out Signature';
$Lang['eSchoolBus']['Report']['Pickup']['SignOutSignature']['ch'] = '接送者签字';
$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['en'] = 'Sign-out Signature';
$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['ch'] = '接送教师签字';
$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['en'] = 'Pick-up Person';
$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['ch'] = '接送者签字';
$Lang['eSchoolBus']['Report']['Pickup']['Remark']['en'] = 'Remark';
$Lang['eSchoolBus']['Report']['Pickup']['Remark']['ch'] = '备注';
$Lang['eSchoolBus']['Report']['Pickup']['ArrivedAtHome']['en'] = 'Arrived at home';
$Lang['eSchoolBus']['Report']['Pickup']['ArrivedAtHome']['ch'] = '到家时间';

$Lang['eSchoolBus']['Report']['Attendance']['TotalAbsent'] = '缺席总数';
$Lang['eSchoolBus']['Report']['Attendance']['TotalOnBus'] = '乘车总数';
$Lang['eSchoolBus']['Report']['Bus'] = '校车';
$Lang['eSchoolBus']['Report']['ByClass']['All'] = '全部';
$Lang['eSchoolBus']['Report']['ByClass']['HasRoute'] = '有路线';
$Lang['eSchoolBus']['Report']['ByClass']['HasRouteOrNot'] = '有路线/没有路线';
$Lang['eSchoolBus']['Report']['ByClass']['NoRoute'] = '没有路線';
$Lang['eSchoolBus']['Report']['ByClass']['NotTake'] = '不乘车';
$Lang['eSchoolBus']['Report']['ByClass']['RequestSelectClass'] = '请选择班别。';
$Lang['eSchoolBus']['Report']['ByClass']['Take'] = '乘车';
$Lang['eSchoolBus']['Report']['ByClass']['TakeOption'] = '乘车/不乘车';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Name'] = '姓名';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['StudentNumber'] = '学号';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Route'] = '路线';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Class'] = '班别';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Campus'] = '位置';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['TimeSlot'] = '上学/放学';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['TakeOrNot'] = '乘车/不乘车';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Remark'] = '老师备注';
$Lang['eSchoolBus']['Report']['Campus'] = '位置';
$Lang['eSchoolBus']['Report']['IsHolidayWarning'] = '所选日期为假期,请重选';
$Lang['eSchoolBus']['Report']['SelectDate'] = '请选择日期';

# App
$Lang['eSchoolBus']['App']['ApplyLeave']['AllStatus'] = '全部状态';
$Lang['eSchoolBus']['App']['ApplyLeave']['Apply'] = '乘车请假申请';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplicaton'] = '是否确定取消此申请?';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelAllApplyLeave'] = '取消以上日子乘车请假申请';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplyLeave'] = '取消乘车请假申请';
$Lang['eSchoolBus']['App']['ApplyLeave']['ConfirmSubmit'] = '是否确定提交此申请?';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Absent'] = '缺席';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Cancelled'] = '已取消';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Confirmed'] = '已接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Deleted'] = '删除';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Rejected'] = '不获接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Waiting'] = '待接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddSuccess'] = '纪录已经新增';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddUnSuccess'] = '纪录新增失败';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['ApproveSuccess'] = '纪录已经接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['ApproveUnsuccess'] = '纪录未能接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelSuccess'] = '纪录已经取消';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelUnsuccess'] = '取消纪录失败';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['InvalidTimeSlot'] = '无效的时段';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingApplicationID'] = '遗失申请编号';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingStudentID'] = '遗失学生编号';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['NotAllowedApplyLeave'] = '不允许申请乘车请假';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['RejectSuccess'] = '纪录已经更新为不获接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['RejectUnsuccess'] = '纪录未能更新为不获接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateSuccess'] = '纪录已经更新';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateUnsuccess'] = '更新纪录失败';
$Lang['eSchoolBus']['App']['ApplyLeave']['Reason'] = '原因';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Absent'] = '缺席(自动产生)';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Approved'] = '已接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Cancelled'] = '已取消';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Deleted'] = '删除(自动产生)';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Pending'] = '待接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Rejected'] = '不获接纳';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlot'] = '时段';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'] = '上学';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'] = '放学';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'] = '全天';
$Lang['eSchoolBus']['App']['ApplyLeave']['Title']= '乘车请假';
$Lang['eSchoolBus']['App']['Date'] = '日期';
$Lang['eSchoolBus']['App']['Error']['Ajax'] = '存取资料时遇到错误,请重试!';
$Lang['eSchoolBus']['App']['Month'][1] = '一月';
$Lang['eSchoolBus']['App']['Month'][2] = '二月';
$Lang['eSchoolBus']['App']['Month'][3] = '三月';
$Lang['eSchoolBus']['App']['Month'][4] = '四月';
$Lang['eSchoolBus']['App']['Month'][5] = '五月';
$Lang['eSchoolBus']['App']['Month'][6] = '六月';
$Lang['eSchoolBus']['App']['Month'][7] = '七月';
$Lang['eSchoolBus']['App']['Month'][8] = '八月';
$Lang['eSchoolBus']['App']['Month'][9] = '九月';
$Lang['eSchoolBus']['App']['Month'][10] = '十月';
$Lang['eSchoolBus']['App']['Month'][11] = '十一月';
$Lang['eSchoolBus']['App']['Month'][12] = '十二月';
$Lang['eSchoolBus']['App']['NewRecord'] = '新增纪录';
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['ApproveApplicationContent'] = "校方已接纳阁下于[ApplyDate]提交贵子弟[StudentChineseName]的乘车请假申请。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been accepted.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['ApproveApplicationTitle'] = "接纳乘车请假申请通知
Leave Application (not taking school bus) Accepted Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToTeacher']['ApplyLeaveContent'] = "现收到家长于[ApplyDate]提交学生[StudentChineseName]的乘车请假申请。
Please note that the leave application of not taking school bus for [StudentEnglishName] submitted by parent on [ApplyDate] has been received.";
$Lang['eSchoolBus']['App']['PushMessage']['ToTeacher']['ApplyLeaveTitle'] = "乘车请假申请通知
Leave Application (not taking school bus) Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['CancelApplicationContent'] = "校方已取消阁下于[ApplyDate]提交贵子弟[StudentChineseName]的乘车请假申请。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been cancelled.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['CancelApplicationTitle'] = "取消乘车请假申请通知
Leave Application (not taking school bus) Cancelled Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['RejectApplicationContent'] = "校方未能接纳阁下于[ApplyDate]提交贵子弟[StudentChineseName]的乘车请假申请。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been rejected.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['RejectApplicationTitle'] = "乘车请假申请不获接纳通知
Leave Application (not taking school bus) Rejected Alert";
$Lang['eSchoolBus']['App']['RequiredField'] = "附有<span class='alertText'> * </span>的项目必须填写";
$Lang['eSchoolBus']['App']['Schedule']['Absent'] = '缺席(自动产生)';
$Lang['eSchoolBus']['App']['Schedule']['AppliedLeave'] = '已请假';
$Lang['eSchoolBus']['App']['Schedule']['NA'] = '不适用';
$Lang['eSchoolBus']['App']['Schedule']['Pending'] = '请假待接纳';
$Lang['eSchoolBus']['App']['Schedule']['Rejected'] = '请假不获接纳';
$Lang['eSchoolBus']['App']['Tab']['ApplyLeave'] = '乘车请假';
$Lang['eSchoolBus']['App']['Tab']['Schedule'] = '乘车安排';
$Lang['eSchoolBus']['App']['Warning']['AllowFutureDatesOnly'] = '只可以申请今天或以后的日期';
$Lang['eSchoolBus']['App']['Warning']['DuplicateApplication'] = '不允许重复申请';
$Lang['eSchoolBus']['App']['Warning']['InputEndDate'] = '请输入结束日期';
$Lang['eSchoolBus']['App']['Warning']['InputLeaveReason'] = '请输入请假的原因';
$Lang['eSchoolBus']['App']['Warning']['InputStartDate'] = '请输入开始日期';
$Lang['eSchoolBus']['App']['Warning']['PassedCutoffDate'] = '乘车请假必须在%s日前提交申请,请与学校联络。';
$Lang['eSchoolBus']['App']['Warning']['PassedCutoffTime'] = '乘车请假必须%s前提交申请,,请与学校联络。';
$Lang['eSchoolBus']['App']['Warning']['SelectStudent'] = '请选择学生';
$Lang['eSchoolBus']['App']['Warning']['StartDateIsLaterThanEndDate'] = '结束日期不能比开始日期早';
$Lang['eSchoolBus']['App']['Warning']['StartDateTimeSlotIsLaterThanEndDateTimeSlot'] = '结束日期时段不能比开始日期时段早';

$Lang['eSchoolBus']['TeacherApp']['AllStudent'] = '全部学生';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['Add'] = '新增乘车请假';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmApproval'] = '是否确定批准此申请?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmCancel'] = '是否确定取消此申请?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmReject'] = '是否确定拒绝此申请?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmSubmit'] = '是否确定新增此请假?';
$Lang['eSchoolBus']['TeacherApp']['Remark']='老师备注';
$Lang['eSchoolBus']['TeacherApp']['Tab']['Handled'] = '已处理';
$Lang['eSchoolBus']['TeacherApp']['Tab']['Pending'] = '待处理';

if (is_file("$intranet_root/lang/cust/eSchoolBus_lang.$intranet_session_language.customized.php"))
{
    include("$intranet_root/lang/cust/eSchoolBus_lang.$intranet_session_language.customized.php");
}
?>