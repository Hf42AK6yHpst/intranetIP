<?php 
// using:   
/*
 * ############### 這個模组使用 UTF-8 ###############
 * Remarks:
 * 1) Arrange the lang variables in alphabetical order
 * 2) Keep the same line break between en and b5 lang files (so that same lang variable is in the same line in different lang files)
 * 3) Use single quote for the array keys and double quote for the lang value
 */

//D
$Lang['DigitalChannels']['DigitalChannelsName'] = "數碼頻道";

// C
$Lang['DigitalChannels']['Category']['Title'] = "分類";

// F
$Lang['DigitalChannels']['Favorites']['Title'] = "我的最愛";

// G
$Lang['DigitalChannels']['General']['Select'] = "選擇";
$Lang['DigitalChannels']['General']['Edit'] = "編輯";
$Lang['DigitalChannels']['General']['Remove'] = "移除";
$Lang['DigitalChannels']['General']['Submit'] = "呈送";
$Lang['DigitalChannels']['General']['Cancel'] = "取消";
$Lang['DigitalChannels']['General']['ViewBy'] = "檢視:";
$Lang['DigitalChannels']['General']['Or'] = "或";
$Lang['DigitalChannels']['General']['To'] = "至";
$Lang['DigitalChannels']['General']['Back'] = "返回";

$Lang['DigitalChannels']['General']['Total'] = "共有";
$Lang['DigitalChannels']['General']['Category'] = "分類";
$Lang['DigitalChannels']['General']['CategoriesInTotal'] = "共有 %s 分類";
$Lang['DigitalChannels']['General']['Album'] = "相簿";
$Lang['DigitalChannels']['General']['AlbumsInTotal'] = "共有 %s 相簿";
$Lang['DigitalChannels']['General']['Photo'] = "圖片";
$Lang['DigitalChannels']['General']['PhotosInTotal'] = "共有 %s 相片";

$Lang['DigitalChannels']['General']['NoRecord']	= "沒有記錄";
$Lang['DigitalChannels']['General']['DaysAgo']	= "日前";
$Lang['DigitalChannels']['General']['more'] = "更多";
$Lang['DigitalChannels']['General']['MostHitPhoto'] = "點擊最多的相片";
$Lang['DigitalChannels']['General']['MostHitVideo'] = "點擊最多的影片";
$Lang['DigitalChannels']['General']['LastestPhoto'] = "最新相片 / 影片";
$Lang['DigitalChannels']['General']['LastestAlbum'] = "最新相簿";
$Lang['DigitalChannels']['General']['Recommend'] = "推介";
$Lang['DigitalChannels']['General']['MostPopular'] = "最受歡迎";
$Lang['DigitalChannels']['General']['JustNow'] = "現在";
$Lang['DigitalChannels']['General']['SecondAgo'] = "秒前";
$Lang['DigitalChannels']['General']['MinuteAgo'] = "分鐘前";
$Lang['DigitalChannels']['General']['HourAgo'] = "小時前";
$Lang['DigitalChannels']['General']['DayAgo'] = "日前";
$Lang['DigitalChannels']['General']['WeekAgo'] = "星期前";
$Lang['DigitalChannels']['General']['MonthAgo'] = "個月前";
$Lang['DigitalChannels']['General']['YearAgo'] = "年前";
$Lang['DigitalChannels']['General']['SecondsAgo'] = "秒前";
$Lang['DigitalChannels']['General']['MinutesAgo'] = "分鐘前";
$Lang['DigitalChannels']['General']['HoursAgo'] = "小時前";
$Lang['DigitalChannels']['General']['DaysAgo'] = "日前";
$Lang['DigitalChannels']['General']['WeeksAgo'] = "星期前";
$Lang['DigitalChannels']['General']['MonthsAgo'] = "個月前";
$Lang['DigitalChannels']['General']['YearsAgo'] = "年前";
$Lang['DigitalChannels']['General']['Search'] = "搜尋";
$Lang['DigitalChannels']['General']['AdvanceSearch'] = "進階搜尋";
$Lang['DigitalChannels']['General']['SearchResult'] = "搜尋結果";
$Lang['DigitalChannels']['General']['All'] = "全部";
$Lang['DigitalChannels']['General']['Video'] = "影片";
$Lang['DigitalChannels']['General']['Photo'] = "相片";
$Lang['DigitalChannels']['General']['Type'] = "類型";
$Lang['DigitalChannels']['General']['FileType'] = "媒體類型";
$Lang['DigitalChannels']['General']['PhotoVideo'] = "相片 / 影片";
$Lang['DigitalChannels']['General']['Album2'] = "相簿";
$Lang['DigitalChannels']['General']['AlbumTitle'] = "相簿標題";
$Lang['DigitalChannels']['General']['AlbumDescription'] = "相簿描述";
$Lang['DigitalChannels']['General']['PhotoDescription'] = "相片描述";
$Lang['DigitalChannels']['General']['Embed'] = "嵌入";
$Lang['DigitalChannels']['General']['New'] = "新增";
$Lang['DigitalChannels']['General']['Add'] = "加入";
$Lang['DigitalChannels']['General']['QuickAdd'] = "快速新增";

//M
$Lang['DigitalChannels']['Msg']['AreYouSureTo'] = "您確認要";
$Lang['DigitalChannels']['Msg']['AllPhotosWillBeRemoved'] = "所有相片將會被移除";
$Lang['DigitalChannels']['Msg']['StopUploadingPhotos']	= "停止上載相片";
$Lang['DigitalChannels']['Msg']['BrowserReminder']	= "請使用 Firefox, Google Chrome, Internet Explorer 10 或以上瀏覽器瀏覽此模组";
$Lang['DigitalChannels']['Msg']['RemoveAlbum'] = "移除相簿?";
$Lang['DigitalChannels']['Msg']['CannotRemoveAlbum'] = "分類內已有相簿，不能被移除";
$Lang['DigitalChannels']['Msg']['enterComment'] = "請輸入留言。";
$Lang['DigitalChannels']['Msg']['RemoveComment'] = "移除留言?";

// S
$Lang['DigitalChannels']['Settings']['Title'] = "管理設定";
$Lang['DigitalChannels']['Settings']['BasicSettings'] = "基本設定";
$Lang['DigitalChannels']['Settings']['Category'] = "分類";
$Lang['DigitalChannels']['Settings']['KeepOriginalPhoto'] = "保留原始檔案";
$Lang['DigitalChannels']['Settings']['AllowDownloadOriginalPhoto']  = "允許使用者下載原始檔案";
$Lang['DigitalChannels']['Settings']['AllowUserToComment']  = "允許使用者留言";
$Lang['DigitalChannels']['Settings']['CategoryCode'] = "分類代碼";
$Lang['DigitalChannels']['Settings']['DescriptionEn'] = "英文分類名稱";
$Lang['DigitalChannels']['Settings']['DescriptionChi'] = "中文分類名稱";
$Lang['DigitalChannels']['Settings']['Msg']['DuplicatedCategoryCode'] = '重複代碼';
$Lang['DigitalChannels']['Settings']['PleaseFillInEnglish'] = "請用英文字母填寫";
$Lang['DigitalChannels']['Settings']['Helper'] = "助手";
$Lang['DigitalChannels']['Settings']['UserType'] = "用戶種類";
$Lang['DigitalChannels']['Settings']['SupportStaff'] = "非教學職務員工";
$Lang['DigitalChannels']['Settings']['TeachingStaff'] = "教師";
$Lang['DigitalChannels']['Settings']['SelectClass'] = "選擇班別";
$Lang['DigitalChannels']['Settings']['SelectedUser'] = "已選擇用戶";
$Lang['DigitalChannels']['Settings']['CtrlMultiSelectMessage'] = "按CTRL選擇更多";
$Lang['DigitalChannels']['Settings']['Or'] = '或';
$Lang['DigitalChannels']['Settings']['SearchUser'] = "搜尋用戶";

// O
$Lang['DigitalChannels']['Organize']['Title'] = "媒體管理";
$Lang['DigitalChannels']['Organize']['ChiDescriptionHere'] = "輸入描述";
$Lang['DigitalChannels']['Organize']['DescriptionHere'] = "輸入描述";
$Lang['DigitalChannels']['Organize']['EngDescriptionHere'] = "Description here";
$Lang['DigitalChannels']['Organize']['ChiEventTitleHere'] = "輸入賽事";
$Lang['DigitalChannels']['Organize']['EngEventTitleHere'] = "Event title";
$Lang['DigitalChannels']['Organize']['EventTitleHere'] = "輸入賽事";
$Lang['DigitalChannels']['Organize']['SetAsCoverPhoto']	= "設定成封面相片";
$Lang['DigitalChannels']['Organize']['RemoveAlbum'] = "移除相簿";
$Lang['DigitalChannels']['Organize']['UntitledAlbum'] = "未命名相簿";
$Lang['DigitalChannels']['Organize']['NewAlbum'] = "新增相簿";
$Lang['DigitalChannels']['Organize']['RemoveFile'] = "移除檔案";

$Lang['DigitalChannels']['Organize']['Category'] = "分類";
$Lang['DigitalChannels']['Organize']['AlbumCode'] = "網站識別編號";       // for SFOC
$Lang['DigitalChannels']['Organize']['AlbumChiTitle'] = "相簿標題";
$Lang['DigitalChannels']['Organize']['AlbumEngTitle'] = "Album title";
$Lang['DigitalChannels']['Organize']['AlbumTitle'] = "相簿標題";
$Lang['DigitalChannels']['Organize']['AlbumTitleEng'] = "相簿標題(英文)";
$Lang['DigitalChannels']['Organize']['AlbumTitleChi'] = "相簿標題(中文)";
$Lang['DigitalChannels']['Organize']['ChiDescription'] = "相簿描述";
$Lang['DigitalChannels']['Organize']['Description'] = "相簿描述";
$Lang['DigitalChannels']['Organize']['DescriptionEng'] = "相簿描述(英文)";
$Lang['DigitalChannels']['Organize']['DescriptionChi'] = "相簿描述(中文)";
$Lang['DigitalChannels']['Organize']['EngDescription'] = "Description";
$Lang['DigitalChannels']['Organize']['PIC'] = "負責人";
$Lang['DigitalChannels']['Organize']['EventTitle'] = "賽事標題";
$Lang['DigitalChannels']['Organize']['EventTitleEng'] = "賽事標題 (英文)";
$Lang['DigitalChannels']['Organize']['EventTitleChi'] = "賽事標題 (中文)";
$Lang['DigitalChannels']['Organize']['Date'] = "活動日期";
$Lang['DigitalChannels']['Organize']['EventDate2'] = "活動日期";
$Lang['DigitalChannels']['Organize']['EventDate'] = "賽事日期";
$Lang['DigitalChannels']['Organize']['DateTaken'] = "拍攝日期";
$Lang['DigitalChannels']['Organize']['StartDate'] = "開始日期";
$Lang['DigitalChannels']['Organize']['EndDate'] = "完結日期";
$Lang['DigitalChannels']['Organize']['DragPhoto'] = "將媒體拖放到此";
$Lang['DigitalChannels']['Organize']['SelectPhotoFrom'] = "從電腦選取";
$Lang['DigitalChannels']['Organize']['TargetGroup'] = "目標小組";
$Lang['DigitalChannels']['Organize']['Period'] = "時段";
$Lang['DigitalChannels']['Organize']['Private'] = "非公開";
$Lang['DigitalChannels']['Organize']['AllUsers'] = "所有用戶";
$Lang['DigitalChannels']['Organize']['Groups'] = "小組";
$Lang['DigitalChannels']['Organize']['Users'] = "指定用戶";
$Lang['DigitalChannels']['Organize']['UnsupportedFormat'] = "不支援此格式";
$Lang['DigitalChannels']['Organize']['FileSizeTooLarge'] = "檔案太大";

$Lang['DigitalChannels']['Organize']['Album'] = "相簿";
$Lang['DigitalChannels']['Organize']['Category'] = "分類";
$Lang['DigitalChannels']['Organize']['AlbumNum'] = "相簿數量";
$Lang['DigitalChannels']['Organize']['CreateDate'] = "建立時間";
$Lang['DigitalChannels']['Organize']['View'] = "觀看次數";
$Lang['DigitalChannels']['Organize']['Enjoy'] = "喜愛";
$Lang['DigitalChannels']['Organize']['Comment'] = "留言";
$Lang['DigitalChannels']['Organize']['AddComment'] = "加入一個留言";
$Lang['DigitalChannels']['Organize']['Edited'] = "已修改";
$Lang['DigitalChannels']['Organize']['EnjoyThis'] = "已喜愛";
$Lang['DigitalChannels']['Organize']['PreviousComments'] = "顯示 %s 個先前的留言";
$Lang['DigitalChannels']['Organize']['EnjoyThisPhoto'] = "喜愛此相片";
$Lang['DigitalChannels']['Organize']['RemoveEnjoyThisPhoto'] = "取消喜愛此相片";
$Lang['DigitalChannels']['Organize']['DownloadThisPhoto'] = "下載此相片";

//R
$Lang['DigitalChannels']['Remarks']['SupportUploadFormat'] = "支援JPG/PNG/GIF相片及MP4/3GP/WMV/MOV/RM/FLV影片";
$Lang['DigitalChannels']['Remarks']['MaxFileSize'] = "上傳檔案大小上限為";
$Lang['DigitalChannels']['Remarks']['PicSelection'] = "除ADMIN及相簿建立者外，負責人亦有管理相簿之權限。";
$Lang['DigitalChannels']['Remarks']['FailToUpload'] = "上傳失敗";
$Lang['DigitalChannels']['Remarks']['MediaConvert'] = "媒體格式轉換中。。。";
$Lang['DigitalChannels']['Remarks']['DiscardQuickAdd'] = "放棄該次上載?";
$Lang['DigitalChannels']['Remarks']['DiscardUploadMedia'] = "媒體或未能成功上傳，繼續?";
$Lang['DigitalChannels']['Remarks']['DiscardThisAlbum'] = "放棄此相簿?";
$Lang['DigitalChannels']['Remarks']['RecAlbumDateRange'] = "如不輸入時段，精華將不會顯示";
$Lang['DigitalChannels']['Remarks']['CategoryPicSelection'] = "助手有管理分類內相簿之權限。";
$Lang['DigitalChannels']['Recommend']['Title'] = "精華管理";
$Lang['DigitalChannels']['Recommend']['NewReommendation'] = "新增精華";
$Lang['DigitalChannels']['Recommend']['Reommendation'] = "精華";
$Lang['DigitalChannels']['Recommend']['ReommendationInTotal'] = "共有 %s 精華";
$Lang['DigitalChannels']['Recommend']['RemoveRecommendation'] = "移除精華";
$Lang['DigitalChannels']['Recommend']['RecommendTitle'] = "標題";
$Lang['DigitalChannels']['Recommend']['RecommendDescription'] = "描述";
$Lang['DigitalChannels']['Recommend']['NumberOfSelect'] = "已選擇的數量";
$Lang['DigitalChannels']['Recommend']['SortBy'] = "排序";
$Lang['DigitalChannels']['Recommend']['LastModified'] = "最近更新";
$Lang['DigitalChannels']['Recommend']['DateUploaded'] = "上傳日期";
$Lang['DigitalChannels']['Recommend']['just']  = "剛剛";
$Lang['DigitalChannels']['Recommend']['a_few_seconds_ago']  = "幾秒鐘前";
$Lang['DigitalChannels']['Recommend']['a_minute_ago']  = "一分鐘前";
$Lang['DigitalChannels']['Recommend']['minutes_ago']  = "分鐘前";
$Lang['DigitalChannels']['Recommend']['1_hour_ago']  = "1小時前";
$Lang['DigitalChannels']['Recommend']['hours_ago']  = "小時前";
$Lang['DigitalChannels']['Recommend']['day_ago']  = "天前";
$Lang['DigitalChannels']['Recommend']['year_ago']  = "年前";
$Lang['DigitalChannels']['Recommend']['UntitledRecAlbum'] = "未命名精華";

// Others, for UTF-8 wording overwrite (in EJ)
$button_cancel = "取消";
$button_reset = "重設";
$button_submit = "呈送";
$i_general_collapse_all = "摺疊全部";
$i_general_expand_all = "展開全部";
$i_junior_leftmenu_hide="隱<br/>藏";

$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderSuccess'] = "1|=|排列類別成功";
$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderFailed'] = "0|=|排列類別失敗";

$Lang['General']['Loading'] = '載入中...';
$Lang['General']['Yes'] = '是';
$Lang['General']['No'] = '否';
$Lang['General']['RequiredField'] = "附有「<span class='tabletextrequire'>*</span>」的項目必須填寫";
$Lang['General']['Settings'] = "設定";
$Lang['General']['NoRecordAtThisMoment'] = "暫時仍未有任何紀錄";

$Lang['General']['ReturnMessage']['UpdateSuccess'] = '1|=|紀錄已經更新.';
$Lang['General']['ReturnMessage']['UpdateUnsuccess'] = '0|=|更新紀錄失敗.';
$Lang['General']['ReturnMessage']['DeleteSuccess'] = '1|=|紀錄已經刪除';
$Lang['General']['ReturnMessage']['DeleteUnsuccess'] = '0|=|刪除紀錄失敗';

$Lang['Btn']['Delete'] = "刪除";
$Lang['Btn']['Edit'] = '編輯';
$Lang['Btn']['New'] = '新增';
$Lang['Btn']['Submit'] = '呈送';
$Lang['Btn']['Cancel'] = '取消';
$Lang['Btn']['AddAll'] = '新增所有';
$Lang['Btn']['AddSelected'] = '新增已選取的';
$Lang['Btn']['RemoveSelected'] = '移除已選項目';
$Lang['Btn']['RemoveAll'] = '移除所有';

$Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear'] = "-- 所有學年 --";
?>