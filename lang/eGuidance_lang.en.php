<?
/*
* using by : 
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/

$Lang['eGuidance']['AddAttachment'] = "Add Attachment";

$Lang['eGuidance']['advanced_class']['AddLesson'] = "New Lesson";
$Lang['eGuidance']['advanced_class']['EditLesson'] = "Edit Lesson";
$Lang['eGuidance']['advanced_class']['LessonList'] = "Lesson List";
$Lang['eGuidance']['advanced_class']['LessonManagement'] = "Lesson Management";
$Lang['eGuidance']['advanced_class']['Remind']['ClickDateToAddNew'] = "Please click the date to add new lesson";
$Lang['eGuidance']['advanced_class']['TeacherComment'] = "Teacher's Comment and Result";
$Lang['eGuidance']['advanced_class']['Warning']['DeleteStudentWithLesson'] = "The corresponding comment and result of the deleted student will be deleted at the same time if you saved this page. Are you sure to update?";
$Lang['eGuidance']['advanced_class']['Warning']['InputTeacherComment'] = "Please input at least one teacher's comment and result";

$Lang['eGuidance']['AllStudent'] = "All Student";
$Lang['eGuidance']['AllClass'] = "All Class";
$Lang['eGuidance']['AlreadyNotifyReferral'] = "Has notified colleague for case referral";
$Lang['eGuidance']['AlreadyNotifyUpdate'] = "Has notified colleague for record update";
$Lang['eGuidance']['Attachment'] = "Attachment";
$Lang['eGuidance']['Class'] = "Class";
$Lang['eGuidance']['Confidential'] = "Confidential";

$Lang['eGuidance']['contact']['AllContactCategory'] = "All Category";
$Lang['eGuidance']['contact']['ContactCategory'] = "Category";
$Lang['eGuidance']['contact']['Details'] = "Contact Details & Result";
$Lang['eGuidance']['contact']['Warning']['InputDetails'] = "Please input contact details & result";
$Lang['eGuidance']['contact']['Warning']['SelectCategory'] = "Please select category";

$Lang['eGuidance']['ctmeeting']['Comments'] = "Comments";
$Lang['eGuidance']['ctmeeting']['Export']['WebSAMSRegNo'] = "Ad No";
$Lang['eGuidance']['ctmeeting']['Export']['ClassName'] = "Class %s";
$Lang['eGuidance']['ctmeeting']['Export']['ClassNo'] = "No";
$Lang['eGuidance']['ctmeeting']['Export']['StudentName'] = "Name";
$Lang['eGuidance']['ctmeeting']['Export']['Gender'] = "Sex";
$Lang['eGuidance']['ctmeeting']['Export']['Followup'] = "%s Follow-up";
$Lang['eGuidance']['ctmeeting']['Followup'] = "Follow-up Advice";
$Lang['eGuidance']['ctmeeting']['Import']['StudentName'] = "Student Name";
$Lang['eGuidance']['ctmeeting']['Import']['WebSAMSRegNo'] = "WebSAMSRegNo";
$Lang['eGuidance']['ctmeeting']['ImportColumns'] = array (
"<font color = red>*</font>WebSAMSRegNo (The leading \"#\" is part of the format of WebSAMSRegNo)",
"Student Name",
"<font color = red>@</font>First meeting comments",
"<font color = red>^</font>First meeting follow-up advice",
"<font color = red>@</font>Second meeting comments",
"<font color = red>^</font>Second meeting follow-up advice"
);
$Lang['eGuidance']['ctmeeting']['ImportError']['DuplicateRecord'] = "Student record already exist";
$Lang['eGuidance']['ctmeeting']['ImportError']['StudentNotFound'] = "Student not found";
$Lang['eGuidance']['ctmeeting']['ImportError']['StudentNotRegisterInTheYear'] = "This student hasn't registered in the academic year";
$Lang['eGuidance']['ctmeeting']['ImportError']['WebSAMSRegNoNotFound'] = "WebSAMSRegNo is not set for this student";
$Lang['eGuidance']['ctmeeting']['ImportRemarks'] = array (
"<font color = red>*</font>Mandatory field(s)",
"<font color = red>@</font>Please fill in column title with meeting date (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)",
"<font color = red>^</font>Please fill in column title with meeting date followed by 'Follow-up' (YYYY-MM-DD Follow-up or D/M/YYYY Follow-up or DD/MM/YYYY Follow-up)",
"Please add meeting comments and follow-up advice next to the second meeting follow-up advice if there're more than two meetings"
);
$Lang['eGuidance']['ctmeeting']['ImportReturnMsg']['DateNotExist'] = "0|=|Meeting Date is out of scope in all academic years";
$Lang['eGuidance']['ctmeeting']['ImportTitle'] = "Class Teacher Meeting Import Record";
$Lang['eGuidance']['ctmeeting']['MeetingDate'] = "Meeting Date";
$Lang['eGuidance']['ctmeeting']['StudentName'] = "Student Name";
$Lang['eGuidance']['ctmeeting']['StudentRegNo'] = "Student Registry No";

$Lang['eGuidance']['Date'] = "Date";
$Lang['eGuidance']['error']['ajax'] = "Error on AJAX call!";
$Lang['eGuidance']['FollowupAdvice'] = "Follow-up Advice";

$Lang['eGuidance']['guidance']['AllContactStage'] = "All Contact Stage";
$Lang['eGuidance']['guidance']['CaseType'] = "Case Type";
$Lang['eGuidance']['guidance']['ContactContent'] = "Content";
$Lang['eGuidance']['guidance']['ContactDate'] = "Contact Date";
$Lang['eGuidance']['guidance']['ContactFinding'] = "Findings";
$Lang['eGuidance']['guidance']['ContactPlace'] = "Place";
$Lang['eGuidance']['guidance']['ContactPurpose'] = "Purpose";
$Lang['eGuidance']['guidance']['ContactStage'] = "Contact Stage";
$Lang['eGuidance']['guidance']['ContactTime'] = "Time";
$Lang['eGuidance']['guidance']['Result'] = "Result";
$Lang['eGuidance']['guidance']['Warning']['SelectCaseType'] = "Please select case type";
$Lang['eGuidance']['guidance']['Warning']['InputCaseTypeName'] = "Please input case type name";

$Lang['eGuidance']['Import']['InvalidRecord'] = "No. of invalid record(s)";
$Lang['eGuidance']['Import']['Steps'][0] = "Select CSV file";
$Lang['eGuidance']['Import']['Steps'][1] = "Data Verification";
$Lang['eGuidance']['Import']['Steps'][2] = "Imported Result";
$Lang['eGuidance']['Import']['Successful'] = " record import successful";
$Lang['eGuidance']['Import']['TotalRecord'] = "Total row(s)";
$Lang['eGuidance']['Import']['ValidRecord'] = "No. of valid record(s)";
$Lang['eGuidance']['Item'] = "Item";

// parent
$Lang['eGuidance']['menu']['CaseFollow'] = "Case Follow";

// admin
/* !!! Keep these menu in physical order as it use in loop of access right settings: start */
$Lang['eGuidance']['menu']['main']['Management'] = "Management";
$Lang['eGuidance']['menu']['main']['Reports'] = "Reports";
$Lang['eGuidance']['menu']['main']['Settings'] = "Settings";
$Lang['eGuidance']['menu']['Management']['Contact'] = "Meeting/Contact";
$Lang['eGuidance']['menu']['Management']['AdvancedClass'] = "Advanced Class";
$Lang['eGuidance']['menu']['Management']['Therapy'] = "Therapy";
$Lang['eGuidance']['menu']['Management']['Guidance'] = "Guidance";
$Lang['eGuidance']['menu']['Management']['SEN'] = "SEN";
$Lang['eGuidance']['menu']['Management']['Suspend'] = "Suspend Study";
$Lang['eGuidance']['menu']['Management']['Transfer'] = "Case Referral";
$Lang['eGuidance']['menu']['Management']['Personal'] = "Student Personal Record";
$Lang['eGuidance']['menu']['Management']['SelfImprove'] = "Self Improve Scheme";
$Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'] = "Class Teacher Meeting";
$Lang['eGuidance']['menu']['Reports']['StudentReport'] = "Student Report";
$Lang['eGuidance']['menu']['Reports']['TeacherReport'] = "Teacher Report";
$Lang['eGuidance']['menu']['Reports']['ClassteacherReport'] = "Class Teacher Report";
$Lang['eGuidance']['menu']['Reports']['StudentProblemReport'] = "Student Problem Report";
$Lang['eGuidance']['menu']['Settings']['PermissionSetting'] = "Permission Settings";
$Lang['eGuidance']['menu']['Settings']['FineTuneSetting'] = "SEN Settings";
$Lang['eGuidance']['menu']['Settings']['GeneralSetting'] = "System Settings";
$Lang['eGuidance']['menu']['Settings']['IPAccessRightSetting'] = "Access Right Computer IP";
/* !!! Keep the above menu in physical order as it use in loop of access right settings: end */

$Lang['eGuidance']['name'] = "eGuidance";

$Lang['eGuidance']['NotifyItem']['Contact'] = "meeting/contact record";
$Lang['eGuidance']['NotifyItem']['CTMeeting'] = "Class teacher meeting record";
$Lang['eGuidance']['NotifyItem']['Guidance'] = "guidance record";
$Lang['eGuidance']['NotifyItem']['Personal'] = "personal record";
$Lang['eGuidance']['NotifyItem']['SelfImprove'] = "Self improve scheme";
$Lang['eGuidance']['NotifyItem']['SENCase'] = "SEN case";
$Lang['eGuidance']['NotifyItem']['Suspend'] = "suspending study record";
$Lang['eGuidance']['NotifyReferral'] = "Notify colleague for case referral";
$Lang['eGuidance']['NotifyReferralContent'] = "The case of [name] of [class] has been referred to guidance group on [yy]-[mm]-[dd], please login eClass eGuidance module for details.(eAdmin > Student Management > eGuidance > Management > Case Referral)";
$Lang['eGuidance']['NotifySubject']['Contact'] = "Meeting/contact record update notification";
$Lang['eGuidance']['NotifySubject']['CTMeeting'] = "Class teacher meeting record update notification";
$Lang['eGuidance']['NotifySubject']['Guidance'] = "Guidance record update notification";
$Lang['eGuidance']['NotifySubject']['Personal'] = "Personal record update notification";
$Lang['eGuidance']['NotifySubject']['Referral'] = "Case referral notification";
$Lang['eGuidance']['NotifySubject']['SelfImprove'] = "Self improve scheme update notification";
$Lang['eGuidance']['NotifySubject']['SENCase'] = "SEN case update notification";
$Lang['eGuidance']['NotifySubject']['Suspend'] = "Suspending study notification";
$Lang['eGuidance']['NotifyUpdate'] = "Notify colleague for record update";
$Lang['eGuidance']['NotifyUpdateContent'] = "The [notifyItem] of [name] of [class] has been updated on [yy]-[mm]-[dd], please login eClass eGuidance module for details.(eAdmin > Student Management > eGuidance > Management > [module])";

$Lang['eGuidance']['personal']['BasicInfo'] = "Basic Information";
$Lang['eGuidance']['personal']['BornInHongKong'] = "Born in Hong Kong";
$Lang['eGuidance']['personal']['BornInOther'] = "From (Country)";
$Lang['eGuidance']['personal']['BrotherAndSister'] = "Brother and Sister";
$Lang['eGuidance']['personal']['ContactTel'] = "Contact Tel";
$Lang['eGuidance']['personal']['ElderBrother'] = "Elder Brother";
$Lang['eGuidance']['personal']['ElderSister'] = "Elder Sister";
$Lang['eGuidance']['personal']['FamilyStatus'] = "Family Status";
$Lang['eGuidance']['personal']['Father'] = "Father";
$Lang['eGuidance']['personal']['FatherWorkingStatus'] = "Working status of father";
$Lang['eGuidance']['personal']['FollowupStatus'] = "Follow-up Status";
$Lang['eGuidance']['personal']['Gender'] = "Student Gender";
$Lang['eGuidance']['personal']['GenderMale'] = "Male";
$Lang['eGuidance']['personal']['GenderFemale'] = "Female";
$Lang['eGuidance']['personal']['Guidance'] = "Guidance Group";
$Lang['eGuidance']['personal']['GuidanceTeacher'] = "Guidance Teacher";
$Lang['eGuidance']['personal']['MainProblem'] = "Main Problem <br>(can select multiple)";
$Lang['eGuidance']['personal']['Mother'] = "Mother";
$Lang['eGuidance']['personal']['MotherWorkingStatus'] = "Working status of mother";
$Lang['eGuidance']['personal']['Occupatiion'] = "Occupation";
$Lang['eGuidance']['personal']['ParentStatus'] = "Parent Status";
$Lang['eGuidance']['personal']['People'] = "Person";
$Lang['eGuidance']['personal']['PersonalRecord'] = "Personal Record";
$Lang['eGuidance']['personal']['PersonLivedWithStudent'] = "Person lived with student <br>(can select multiple)";
$Lang['eGuidance']['personal']['PlaceOfBirth'] = "Place of birth";
$Lang['eGuidance']['personal']['ReferralFromAndReason'] = "Referral From and Reason";
$Lang['eGuidance']['personal']['ReferralPerson'] = "Referral Person";
$Lang['eGuidance']['personal']['ReferredToReason'] = "Reason";
$Lang['eGuidance']['personal']['ReferredToSocialWorker'] = "Has referred to social worker";
$Lang['eGuidance']['personal']['Student'] = "Student";
$Lang['eGuidance']['personal']['Total'] = "Total";
$Lang['eGuidance']['personal']['ViewPersonalRecord'] = "View Personal Record";
$Lang['eGuidance']['personal']['YearInHongKong'] = "Year of stayed in Hong Kong";
$Lang['eGuidance']['personal']['YoungerBrother'] = "Younger Brother";
$Lang['eGuidance']['personal']['YoungerSister'] = "Younger Sister";

$Lang['eGuidance']['PleaseSelectFiles'] = "Please select file(s)";
$Lang['eGuidance']['PushMessage']['SendToStaff'] = "Send to staff";
$Lang['eGuidance']['PushMessage']['ViewStatus'] = "View push message result";
$Lang['eGuidance']['Remark'] = "Remark";
$Lang['eGuidance']['RemoveAttachment'] = "Remove Attachment";

$Lang['eGuidance']['report']['basicInformation'] = "Basic Information";
$Lang['eGuidance']['report']['eGuidanceReport'] = "eGuidance Report";
$Lang['eGuidance']['report']['GenerateReport'] = "Generate Report";
$Lang['eGuidance']['report']['NotToShowStudentWithoutRecord'] = "Not to show student without record";
$Lang['eGuidance']['report']['NotShowSectionWithoutRecord'] = "Not to show section without record";
$Lang['eGuidance']['report']['PrintTime'] = "Print time";
$Lang['eGuidance']['report']['ReportOption'] = "Report Option";
$Lang['eGuidance']['report']['ReportType'] = "Report Type";
$Lang['eGuidance']['report']['SENCase']['Add'] = "加";
$Lang['eGuidance']['report']['SENCase']['ClassTeacher'] = "班主任";
$Lang['eGuidance']['report']['SENCase']['IsNeedAdjustmentNextYear'] = "來年是否需要以上的調適安排(請在適當的";
$Lang['eGuidance']['report']['SENCase']['Need'] = "需要";
$Lang['eGuidance']['report']['SENCase']['NotNeed'] = "不需要";
$Lang['eGuidance']['report']['SENCase']['Organizer'] = "統籌主任";
$Lang['eGuidance']['report']['SENCase']['ParentSignature'] = "家長簽署";
$Lang['eGuidance']['report']['SENCase']['SchoolSocialWorker'] = "學校社工";
$Lang['eGuidance']['report']['SENCase']['Signature'] = "負責人員簽署";
$Lang['eGuidance']['report']['SENCase']['SignatureDate'] = "日期";
$Lang['eGuidance']['report']['SENCase']['SubjectChi'] = "中文科";
$Lang['eGuidance']['report']['SENCase']['SubjectEng'] = "英文科";
$Lang['eGuidance']['report']['SENCase']['SubjectGes'] = "常識科";
$Lang['eGuidance']['report']['SENCase']['SubjectMat'] = "數學科";
$Lang['eGuidance']['report']['SENCase']['SubjectTeacherChi'] = "中文科任";
$Lang['eGuidance']['report']['SENCase']['SubjectTeacherEng'] = "英文科任";
$Lang['eGuidance']['report']['SENCase']['SubjectTeacherGes'] = "常識科任";
$Lang['eGuidance']['report']['SENCase']['SubjectTeacherMat'] = "數學科任";
$Lang['eGuidance']['report']['SENCase']['TeacherSuggestion'] = "老師建議(請於下學期末填寫)";
$Lang['eGuidance']['report']['SENStudentAnnualReport'] = "Support Services Annual Report for SEN Students";
$Lang['eGuidance']['report']['StudentPivateInfo'] = "Student Private Info";
$Lang['eGuidance']['report']['StudentPivateInfoDetails'] = "STRN, Gender, Date of Birth";

$Lang['eGuidance']['ReturnMessage']['AddAndNotifySuccess'] = "1|=|Record added and notified colleague";
$Lang['eGuidance']['ReturnMessage']['AddSuccessNotifyFail'] = "0|=|Record added but fail to notify colleague";
$Lang['eGuidance']['ReturnMessage']['UpdateAndNotifySuccess'] = "1|=|Record updated and notified colleague";
$Lang['eGuidance']['ReturnMessage']['UpdateSuccessNotifyFail'] = "0|=|Record updated but fail to notify colleague";
$Lang['eGuidance']['SelectColleague'] = "Select Colleague";
$Lang['eGuidance']['SelectDate'] = "Select Date";

$Lang['eGuidance']['selfimprove']['AllResult'] = "All Result";
$Lang['eGuidance']['selfimprove']['Result'] = "Result";
$Lang['eGuidance']['selfimproveResult']['Delay'] = "Delay";
$Lang['eGuidance']['selfimproveResult']['Successful'] = "Successful";
$Lang['eGuidance']['selfimproveResult']['Unsuccessful'] = "Unsuccessful";

$Lang['eGuidance']['sen']['AllCase'] = "All Case";
$Lang['eGuidance']['sen']['Confirm'] = "Confirmed";
$Lang['eGuidance']['sen']['NotConfirm'] = "Not Confirm";
$Lang['eGuidance']['sen']['Intention'] = "Intention";
$Lang['eGuidance']['sen']['Quit'] = "Quit";
$Lang['eGuidance']['sen']['RD']['Chi'] = "Chinese";
$Lang['eGuidance']['sen']['RD']['Eng'] = "English";
$Lang['eGuidance']['sen']['SENStudent'] = "SEN Student";
$Lang['eGuidance']['sen']['SENType'] = "SEN Type";
$Lang['eGuidance']['sen']['Tab']['Case'] = "Case";
$Lang['eGuidance']['sen']['Tab']['Service'] = "Service";

$Lang['eGuidance']['sen_case']['AddAdjustmentItem'] = "Add new adjustment item";
$Lang['eGuidance']['sen_case']['AddSupportStudyItem'] = "Add new support service on study";
$Lang['eGuidance']['sen_case']['AddSupportOtherItem'] = "Add new support service on emotion, social and self-management";
$Lang['eGuidance']['sen_case']['BasicInfo'] = "Basic Info";
$Lang['eGuidance']['sen_case']['ConfirmDate'] = "Confirm Date";
$Lang['eGuidance']['sen_case']['Filter']['LogicAnd'] = "Match all choices";
$Lang['eGuidance']['sen_case']['Filter']['LogicOr'] = "Match any choice";
$Lang['eGuidance']['sen_case']['FilterSelection'] = "Select Filter";
$Lang['eGuidance']['sen_case']['FineTuneArrangement'] = "Adjustment Arrangement";
$Lang['eGuidance']['sen_case']['FineTuneSupportOther'] = "Support service (emotional, social, self-care)";
$Lang['eGuidance']['sen_case']['FineTuneSupportStudy'] = "Support service (learning)";
$Lang['eGuidance']['sen_case']['NumberOfReport'] = "Number of Report(s)";
$Lang['eGuidance']['sen_case']['Remark'] = "Extra Info";
$Lang['eGuidance']['sen_case']['SelectSENType'] = "Select SEN Type";
$Lang['eGuidance']['sen_case']['SENTypeConfirmDate'] = "Confirmed Date";
$Lang['eGuidance']['sen_case']['Tier']['Tier1'] = "Tier 1";
$Lang['eGuidance']['sen_case']['Tier']['Tier2'] = "Tier 2";
$Lang['eGuidance']['sen_case']['Tier']['Tier3'] = "Tier 3";
$Lang['eGuidance']['sen_case']['Tier']['TierName'] = "Support Mode";
$Lang['eGuidance']['sen_case']['Warning']['DuplicateRecord'] = "Cannot overwrite record as SEN case exist for the selected student";
$Lang['eGuidance']['sen_case']['Warning']['SelectCaseType'] = "Please select case type";
$Lang['eGuidance']['sen_case']['Warning']['SelectIsSEN'] = "Please choose if the student confirm to SEN student or not";

$Lang['eGuidance']['sen_service']['ActivityList'] = "Activity List";
$Lang['eGuidance']['sen_service']['ActivityManagement'] = "Activity Management";
$Lang['eGuidance']['sen_service']['AddActivity'] = "Add Activity";
$Lang['eGuidance']['sen_service']['AllServiceType'] = "All Service Type";
$Lang['eGuidance']['sen_service']['EditActivity'] = "Edit Activity";
$Lang['eGuidance']['sen_service']['ExtraInfo'] = "Extra Info";
$Lang['eGuidance']['sen_service']['SenCase'] = "SEN Case";
$Lang['eGuidance']['sen_service']['SenService'] = "SEN Service";
$Lang['eGuidance']['sen_service']['ServiceDate'] = "Service Date";
$Lang['eGuidance']['sen_service']['ServiceManagement'] = "Service Management";
$Lang['eGuidance']['sen_service']['ServiceType'] = "Service Type";
$Lang['eGuidance']['sen_service']['Remind']['ClickDateToAddNew'] = "Please click the date to add new activity";
$Lang['eGuidance']['sen_service']['Warning']['CannotDeleteSENService'] = "Cannot delete SEN Service as there's activity!";
$Lang['eGuidance']['sen_service']['Warning']['DeleteStudentWithActivity'] = "The corresponding content and result of the deleted student will be deleted at the same time if you saved this page. Are you sure to update?";
$Lang['eGuidance']['sen_service']['Warning']['InputServiceTypeName'] = "Please input service type name";
$Lang['eGuidance']['sen_service']['Warning']['InputExtraInfo'] = "Please input at least one extra info";
$Lang['eGuidance']['sen_service']['Warning']['SelectServiceType'] = "Please select service type";

$Lang['eGuidance']['settings']['FineTune']['AdjustItemChineseName'] = "Adjustment Item (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['AdjustItemEnglishName'] = "Adjustment Item (English)";
$Lang['eGuidance']['settings']['FineTune']['AdjustType'] = "Adjustment Type";
$Lang['eGuidance']['settings']['FineTune']['AdjustTypeChineseName'] = "Adjustment Type (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['AdjustTypeEnglishName'] = "Adjustment Type (English)";
$Lang['eGuidance']['settings']['FineTune']['AllServiceScope'] = "All Service Scope";
$Lang['eGuidance']['settings']['FineTune']['CaseSubTypeChineseName'] = "Case Subtype (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['CaseSubTypeEnglishName'] = "Case Subtype (English)";
$Lang['eGuidance']['settings']['FineTune']['CaseTypeChineseName'] = "Case Type (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['CaseTypeCode'] = "Code";
$Lang['eGuidance']['settings']['FineTune']['CaseTypeEnglishName'] = "Case Type (English)";
$Lang['eGuidance']['settings']['FineTune']['NumberOfSubType'] = "Number of sub-type";
$Lang['eGuidance']['settings']['FineTune']['ServiceScope'] = "Support Service Scope";
$Lang['eGuidance']['settings']['FineTune']['ServiceType'] = "Support Service Type";
$Lang['eGuidance']['settings']['FineTune']['ServiceTypeChineseName'] = "Support Service Type (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['ServiceTypeEnglishName'] = "Support Service Type (English)";
$Lang['eGuidance']['settings']['FineTune']['Support']['AllSupportType'] = "All Support Type";
$Lang['eGuidance']['settings']['FineTune']['Support']['ChineseName'] = "Support Service (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['Support']['EnglishName'] = "Support Service (English)";
$Lang['eGuidance']['settings']['FineTune']['Support']['Service'] = "Support Service";
$Lang['eGuidance']['settings']['FineTune']['Support']['Type'] = "Support Type";
$Lang['eGuidance']['settings']['FineTune']['SupportType']['Study'] = "Learning";
$Lang['eGuidance']['settings']['FineTune']['SupportType']['Other'] = "Emotion,Social,Self-Care";
$Lang['eGuidance']['settings']['FineTune']['Tab']['Arrangement'] = "Adjustment Arrangement";
$Lang['eGuidance']['settings']['FineTune']['Tab']['CaseType'] = "Case Type";
$Lang['eGuidance']['settings']['FineTune']['Tab']['Support'] = "Support Service";
$Lang['eGuidance']['settings']['FineTune']['Tier']['Tier'] = "Tier";
$Lang['eGuidance']['settings']['FineTune']['Tier']['Tier1'] = "Tier 1";
$Lang['eGuidance']['settings']['FineTune']['Tier']['Tier2'] = "Tier 2";
$Lang['eGuidance']['settings']['FineTune']['Tier']['Tier3'] = "Tier 3";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteAdjustItem'] = "The selected adjustment item(s) cannot delete because it(they) has(have) been used in SEN case";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteAdjustType'] = "The selected adjustment type cannot delete because the corresponding items have been used in SEN case";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteSenCaseSubType'] = "The selected case(s) sub-type cannot delete because it(they) has(have) been used in SEN case";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteSenCaseType'] = "The selected case(s) type cannot delete because it(they) has(have) been used in SEN case";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteServiceType'] = "The selected support service type cannot delete because the corresponding items have been used in SEN case";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteSupportService'] = "The selected support service(s) cannot delete because it(they) has(have) been used in SEN case";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateAdjustItem'] = "Adjustment Item cannot be duplicate";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateAdjustType'] = "Adjustment Type cannot be duplicate";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateCaseSubType'] = "The name and code of SEN case sub-type cannot be duplicate";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateCaseType'] = "The name and code of SEN case type cannot be duplicate";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateServiceType'] = "Service Type cannot be duplicate";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateSupport'] = "Support service cannot be duplicate";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustItemChineseName'] = "Please input adjustment item (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustItemEnglishName'] = "Please input adjustment item (English)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustTypeChineseName'] = "Please input adjustment type (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustTypeEnglishName'] = "Please input adjustment type (English)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeChineseName'] = "Please input SEN case sub-type (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeCode'] = "Please input SEN case sub-type code";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeEnglishName'] = "Please input SEN case sub-type (English)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseTypeChineseName'] = "Please input SEN case type (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseTypeCode'] = "Please input SEN case type code";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseTypeEnglishName'] = "Please input SEN case type (English)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputServiceTypeChineseName'] = "Please input service type (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputServiceTypeEnglishName'] = "Please input service type (English)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputSupportChineseName'] = "Please input support service (Chinese)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputSupportEnglishName'] = "Please input support service (English)";
$Lang['eGuidance']['settings']['FineTune']['Warning']['PleaseSelectServiceScope'] = "Please select support service scope";
$Lang['eGuidance']['settings']['FineTune']['WithText'] = "Is there text input?";
$Lang['eGuidance']['settings']['MoveToReorder'] = "Move to arrange the display order";
$Lang['eGuidance']['settings']['Permission']['GeneralAccessRight'] = "General Access Rights";
$Lang['eGuidance']['settings']['Permission']['GroupDescription'] = "Description";
$Lang['eGuidance']['settings']['Permission']['GroupInfo'] = "Group Info";
$Lang['eGuidance']['settings']['Permission']['GroupList'] = "Group List";
$Lang['eGuidance']['settings']['Permission']['GroupPermission'] = "Group Access Rights";
$Lang['eGuidance']['settings']['Permission']['GroupTitle'] = "Group Name";
$Lang['eGuidance']['settings']['Permission']['InputDate'] = "Date Added";
$Lang['eGuidance']['settings']['Permission']['Management'] = "Management";
$Lang['eGuidance']['settings']['Permission']['MemberList'] = "User List";
$Lang['eGuidance']['settings']['Permission']['MemberName'] = "Member Name";
$Lang['eGuidance']['settings']['Permission']['NoRights'] = "No access rights";
$Lang['eGuidance']['settings']['Permission']['NumOfMember'] = "No. of Members";
$Lang['eGuidance']['settings']['Permission']['SelectTeacher'] = "Please select teacher/staff";
$Lang['eGuidance']['settings']['Permission']['View'] = "View";
$Lang['eGuidance']['settings']['Reorder'] = "Arrange display order";
$Lang['eGuidance']['settings']['SeqNo'] = "Display Order";
$Lang['eGuidance']['settings']['System']['AllowClassTeacherViewSEN'] = "Allow class teacher to view related student's SEN info";
$Lang['eGuidance']['settings']['System']['AllowSubjectTeacherViewSEN'] = "Allow subject teacher to view related student's SEN info";
$Lang['eGuidance']['settings']['System']['PrintSenCasePICinStudentReport'] = "Print teacher in charge in student report for SEN case";
$Lang['eGuidance']['settings']['System']['General'] = "General";
$Lang['eGuidance']['settings']['Warning']['DuplicateGroupName'] = "Group name cannot be duplicate";
$Lang['eGuidance']['settings']['Warning']['InputGroupName'] = "Please input group name";
$Lang['eGuidance']['settings']['Warning']["Symbols"] = "!\"#$%&\'*,/:;<=>?@^`|~";
$Lang['eGuidance']['settings']['Warning']["SymbolsExclude"] = "Please exclude these symbols in code field ".$Lang['eGuidance']['settings']['Warning']["Symbols"];

$Lang['eGuidance']['StartDate'] = "Start Date";
$Lang['eGuidance']['StudentList'] = "Student List";
$Lang['eGuidance']['StudentName'] = "Student Name";

$Lang['eGuidance']['student_problem']['AdditionalMaterial'] = "Additional Material";
$Lang['eGuidance']['student_problem']['StudentRegNo'] = "Student Registry No";
$Lang['eGuidance']['student_problem']['StudentName'] = "Student Name";
$Lang['eGuidance']['student_problem']['Type'] = "Type";
$Lang['eGuidance']['student_problem']['Warning']['PleaseSelectAtLeastOne'] = "Please select at least one filter";

$Lang['eGuidance']['SubmitAndNotifyReferral'] = "Submit and Notify colleague for case referral";
$Lang['eGuidance']['SubmitAndNotifyUpdate'] = "Submit and Notify colleague for record update";

$Lang['eGuidance']['suspend']['AllSuspendType'] = "All Study Suspend Type";
$Lang['eGuidance']['suspend']['InSchool'] = "Suspend study inside school";
$Lang['eGuidance']['suspend']['OutSchool'] = "Suspend study outside school";
$Lang['eGuidance']['suspend']['Result'] = "Result";
$Lang['eGuidance']['suspend']['SuspendType'] = "Study Suspend Type";
$Lang['eGuidance']['suspend']['Warning']['SelectSuspendType'] = "Please select study suspend type";

$Lang['eGuidance']['TeacherName'] = "Teacher in charge";

$Lang['eGuidance']['therapy']['ActivityList'] = "Activity List";
$Lang['eGuidance']['therapy']['ActivityManagement'] = "Activity Management";
$Lang['eGuidance']['therapy']['ActivityName'] = "Activity Name";
$Lang['eGuidance']['therapy']['AddActivity'] = "Add Activity";
$Lang['eGuidance']['therapy']['ContentAndResult'] = "Content and Result";
$Lang['eGuidance']['therapy']['EditActivity'] = "Edit Activity";
$Lang['eGuidance']['therapy']['GroupName'] = "Group Name";
$Lang['eGuidance']['therapy']['Remind']['ClickDateToAddNew'] = "Please click the date to add new activity";
$Lang['eGuidance']['therapy']['Warning']['DeleteStudentWithActivity'] = "The corresponding content and result of the deleted student will be deleted at the same time if you saved this page. Are you sure to update?";
$Lang['eGuidance']['therapy']['Warning']['InputAtLeastOne'] = "Please input activity name, remark or at least one content and result";
$Lang['eGuidance']['therapy']['Warning']['InputGroupName'] = "Please input group name";

$Lang['eGuidance']['transfer']['IsClassTeacherKnow'] = "Is class teacher know the event?";
$Lang['eGuidance']['transfer']['KnowDate'] = "Date to know";
$Lang['eGuidance']['transfer']['Remark'] = "Other Information";
$Lang['eGuidance']['transfer']['TransferDate'] = "Referral Date";
$Lang['eGuidance']['transfer']['TransferFrom'] = "Referral From";
$Lang['eGuidance']['transfer']['TransferReason'] = "Referral Reason";
$Lang['eGuidance']['transfer']['TransferTo'] = "Referral To";
$Lang['eGuidance']['transfer']['Warning']['InputTransferReason'] = "Please input referral reason";
$Lang['eGuidance']['transfer']['Warning']['SelectTransferReason'] = "Please select referral reason";
$Lang['eGuidance']['transfer']['Warning']['TransferFromClassmateButSelf'] = "Referral from classmate, but select self";

$Lang['eGuidance']['Warning']['AreYouSureYouWouldLikeToDeleteThisFile'] = "Are you sure you would like to delete this file?";
$Lang['eGuidance']['Warning']['IllegalFileType'] = "Illegal file type";
$Lang['eGuidance']['Warning']['InputInteger'] = "Please input integer";
$Lang['eGuidance']['Warning']['InputNumber'] = "Please input number";
$Lang['eGuidance']['Warning']['InvalidDateEarly'] = "Selected date should not be earlier than start date";
$Lang['eGuidance']['Warning']['MeetingDate'] = "Meeting date should be within the academic year: %s ~ %s";
$Lang['eGuidance']['Warning']['SelectAcademicYear'] = "Please select academic year";
$Lang['eGuidance']['Warning']['SelectAuthorizedUser'] = "Please select authorized user";
$Lang['eGuidance']['Warning']['SelectClass'] = "Please select class";
$Lang['eGuidance']['Warning']['SelectConfidentialViewer'] = "Please select viewer for confidential info";
$Lang['eGuidance']['Warning']['SelectNotifier'] = "Please select colleague";
$Lang['eGuidance']['Warning']['SelectReportType'] = "Please select report type";
$Lang['eGuidance']['Warning']['SelectStudent'] = "Please select student";
$Lang['eGuidance']['Warning']['SelectTeacher'] = "Please select teacher in charge";

if (is_file("$intranet_root/lang/cust/eGuidance_lang.$intranet_session_language.customized.php"))
{
	include("$intranet_root/lang/cust/eGuidance_lang.$intranet_session_language.customized.php");
}
