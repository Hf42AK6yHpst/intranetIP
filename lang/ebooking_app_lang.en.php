<?php 

#### eBooking App [START]
$Lang['eBooking']['App']['DataTable']['DataNotFound'] = 'Data Not Found';
$Lang['eBooking']['App']['DataTable']['PageInfo'] = 'Showing page _PAGE_ of _PAGES_';
$Lang['eBooking']['App']['DataTable']['EmptyPageInfo'] = 'No records available';
$Lang['eBooking']['App']['DataTable']['RecordPerPage'] = 'Display _MENU_ records per page';
$Lang['eBooking']['App']['DataTable']['PageInfoFilter'] = '(filtered from _MAX_ total records)';
$Lang['eBooking']['App']['DataTable']['Search'] = 'Search: ';
$Lang['eBooking']['App']['DataTable']['Next'] = 'Next';
$Lang['eBooking']['App']['DataTable']['Previous'] = 'Previous';
$Lang['eBooking']['App']['DataTable']['Loading'] = 'Loading...';
$Lang['eBooking']['App']['DataTable']['Processing'] = 'Processing';
$Lang['eBooking']['App']['FieldTitle']['eBooking'] = 'eBooking';
$Lang['eBooking']['App']['FieldTitle']['MyBooking'] = 'My Booking Record';
$Lang['eBooking']['App']['FieldTitle']['NewBooking'] = "New Booking";
$Lang['eBooking']['App']['FieldTitle']['EditBooking'] = "Edit Booking";
$Lang['eBooking']['App']['FieldTitle']['Step'] = 'Step <--Step Num-->: ';
$Lang['eBooking']['App']['FieldTitle']['SelectBookingMethod'] = 'Select Booking Method'; //step1a
$Lang['eBooking']['App']['FieldTitle']['SelectBookingTime'] = 'Select Booking Time'; //step 2a
$Lang['eBooking']['App']['FieldTitle']['SelectBookingPeriod'] = 'Select Booking Period'; //step2c
$Lang['eBooking']['App']['FieldTitle']['SelectBookingDateNRoom'] = 'Select Booking Date & Room'; //step 2b
$Lang['eBooking']['App']['FieldTitle']['SelectBookingDateNTime'] = 'Select Booking Date & Time(Repeated)'; //step2d
$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingRoom'] = 'Comfirm Booking Room'; //step3a 3c
$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingItem'] = 'Comfirm Booking Item'; //step3a 3c
$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingTime'] = 'Comfirm Booking Time'; //step3b
$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingPeriod'] = 'Comfirm Booking Period'; //step 3d
$Lang['eBooking']['App']['FieldTitle']['Finish'] = 'Finish';//step 4
$Lang['eBooking']['App']['FieldTitle']['BookingDate'] = 'Booking Date';
$Lang['eBooking']['App']['FieldTitle']['StartDate'] = 'Start Date';
$Lang['eBooking']['App']['FieldTitle']['EndDate'] = 'End Date';
$Lang['eBooking']['App']['FieldTitle']['StartTime'] = 'Start Time';
$Lang['eBooking']['App']['FieldTitle']['EndTime'] = 'End Time';
$Lang['eBooking']['App']['FieldTitle']['RepeatFrequency'] = 'Repeat Frequency';
$Lang['eBooking']['App']['FieldTitle']['RepeatRule'] = 'Repeat Rule';
$Lang['eBooking']['App']['FieldTitle']['SelectRoomOrItem'] = 'Select Room/ Item';
$Lang['eBooking']['App']['FieldTitle']['AvailableTimeSlot'] = 'Available Timeslot: ';
$Lang['eBooking']['App']['FieldTitle']['AvailableTimeSlot2'] = 'Available Timeslot';
$Lang['eBooking']['App']['FieldTitle']['NumOfAvailableTimeSlot'] = '<--Num--> Timeslot Can Be Booked';
$Lang['eBooking']['App']['FieldTitle']['NotAvailableTimeSlot2'] = 'Not Available Timeslot';
$Lang['eBooking']['App']['FieldTitle']['NumOfNotAvailableTimeSlot'] = '<--Num--> Timeslot Can Be NOT Booked';

//list
$Lang['eBooking']['App']['FieldTitle']['BookingTime'] = 'Booking Time';
$Lang['eBooking']['App']['FieldTitle']['BookedBy'] = 'Booked By';
$Lang['eBooking']['App']['FieldTitle']['Status'] = 'Booking Status';
$Lang['eBooking']['App']['FieldTitle']['Action'] = 'Action';

$Lang['eBooking']['App']['FieldTitle']['Room'] = 'Room';
$Lang['eBooking']['App']['FieldTitle']['Item'] = 'Item';
$Lang['eBooking']['App']['FieldTitle']['AppointType'] = 'Reserve Specific Room/Item';
$Lang['eBooking']['App']['FieldTitle']['AppointDate'] = 'No';
$Lang['eBooking']['App']['FieldTitle']['AppointRoom'] = 'Yes';
$Lang['eBooking']['App']['FieldTitle']['OnceOnly'] = 'Once Only';
$Lang['eBooking']['App']['FieldTitle']['Repeated'] = 'Repeated';
$Lang['eBooking']['App']['FieldTitle']['AppointedPeriod'] = 'Appointed Period';
$Lang['eBooking']['App']['FieldTitle']['AppointedTimeslot'] = 'Appointed Timeslot';
$Lang['eBooking']['App']['FieldTitle']['Timeslot'] = 'Timeslot';
$Lang['eBooking']['App']['FieldTitle']['ResourcesType'] = 'Resources Type';
$Lang['eBooking']['App']['FieldTitle']['BookingTypeRepeat'] = 'Booking Type';
$Lang['eBooking']['App']['FieldTitle']['BookingMethod'] = 'Booking Method';
$Lang['eBooking']['App']['FieldTitle']['CycleDay'] = 'Cycle Day <--Num-->';
$Lang['eBooking']['App']['FieldTitle']['EditRemark'] = 'Edit Remark';

$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['RejectReason'] = 'Reject Booking Reason';
$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['101'] = 'Not Available Timeslot';
$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['201'] = 'This room is booked';
$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['301'] = 'No right no book';
$Lang['eBooking']['App']['FieldTitle']['ErrorCode']['999'] = 'Unknown';

$Lang['eBooking']['App']['FieldTitle']['DeleteConfirm'] = 'Delete Confirm';
$Lang['eBooking']['App']['FieldTitle']['DeleteConfirm2'] = 'Are you sure to delete this record?';
$Lang['eBooking']['App']['FieldTitle']['Delete'] = 'Delete';
$Lang['eBooking']['App']['FieldTitle']['Close'] = 'Close';

$Lang['eBooking']['App']['FieldTitle']['More'] = 'More+';
$Lang['eBooking']['App']['FieldTitle']['Attach'] = 'Attachment';
$Lang['eBooking']['App']['FieldTitle']['Remarks'] = 'Remarks';
$Lang['eBooking']['App']['FieldTitle']['Every'] = 'Every ';

$Lang['eBooking']['App']['Selector']['PleaseSelect'] = 'Please Select';
$Lang['eBooking']['App']['Selector']['weekday'] = 'Every Weekday';
$Lang['eBooking']['App']['Selector']['cycleday'] = 'Every Cycleday';
$Lang['eBooking']['App']['Selector']['NoAvailableRoom'] = 'No Available Room';

$Lang['eBooking']['App']['weekday'] = array(
		'Mon' => 'Monday',
		'Tue' => 'Tuesday',
		'Wed' => 'Wednesday',
		'Thu' => 'Thursday',
		'Fri' => 'Friday',
		'Sat' => 'Saturday'
);

$Lang['eBooking']['App']['cycleday'] = array(
		'1' => 'Day 1',
		'2' => 'Day 2',
		'3' => 'Day 3',
		'4' => 'Day 4',
		'5' => 'Day 5',
		'6' => 'Day 6',
		
);

$Lang['eBooking']['App']['Button']['Continue'] = 'Continue';
$Lang['eBooking']['App']['Button']['Cancel'] = 'Cancel';
$Lang['eBooking']['App']['Button']['RealCancel'] = 'Cancel';
$Lang['eBooking']['App']['Button']['PrevStep'] = 'Back';
$Lang['eBooking']['App']['Button']['Back'] = 'Back';

$Lang['eBooking']['App']['Result']['Success'] = 'Applied Success';
$Lang['eBooking']['App']['Result']['Fail'] = 'Applied Fail';

#### eBooking App [END]
?>