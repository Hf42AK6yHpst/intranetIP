<?php

$Lang['MsschPrint']['module'] = 'Printing Records';
$Lang['MsschPrint']['menu']['management'] = 'Management';
	$Lang['MsschPrint']['menu']['print'] = 'Printing Data Import';
	$Lang['MsschPrint']['menu']['deposit'] = 'Cash Deposit';
$Lang['MsschPrint']['menu']['report'] = 'Reports';
	$Lang['MsschPrint']['menu']['printingGroupSummary'] = 'Group Summary';
	$Lang['MsschPrint']['menu']['printingClassSummary'] = 'Class Summary';
	$Lang['MsschPrint']['menu']['printingUserSummary'] = 'Individual User Summary';
$Lang['MsschPrint']['menu']['setting'] = 'Settings';
	$Lang['MsschPrint']['menu']['group'] = 'Group Settings';
    $Lang['MsschPrint']['menu']['printSetting'] = 'Printing Settings';

//////////////////// Management START ////////////////////

//////// Print START ////////
$Lang['MsschPrint']['management']['print']['tableHeader']['date'] = 'Date';
$Lang['MsschPrint']['management']['print']['tableHeader']['groupName'] = 'Group Name';
$Lang['MsschPrint']['management']['print']['tableHeader']['mfpCode'] = 'MFP Code';
$Lang['MsschPrint']['management']['print']['tableHeader']['totalAmount'] = 'Total Amount';
$Lang['MsschPrint']['management']['print']['tableHeader']['modifiedBy'] = 'Admin In Charge';
$Lang['MsschPrint']['management']['print']['tableHeader']['dateModified'] = 'Date Input';
$Lang['MsschPrint']['management']['print']['tableHeader']['colorMode'] = 'Color';
$Lang['MsschPrint']['management']['print']['tableHeader']['pages'] = 'Pages';
$Lang['MsschPrint']['management']['print']['tableHeader']['sheet'] = 'Sheet';
$Lang['MsschPrint']['management']['print']['tableHeader']['paperSize'] = 'Paper Size';

$Lang['MsschPrint']['management']['print']['importRecord'] = 'Printing Data';
$Lang['MsschPrint']['management']['print']['importMonth'] = 'Month';
$Lang['MsschPrint']['management']['print']['importSkippedRecord'] = 'Skipped Record';
$Lang['MsschPrint']['management']['print']['importRemarks']['mfpCode'] = 'MFP Code';
$Lang['MsschPrint']['management']['print']['importRemarks']['amount'] = 'Amount';
$Lang['MsschPrint']['management']['print']['importRemarks2']['lastSyncTime'] = 'lastSyncTime';
$Lang['MsschPrint']['management']['print']['importRemarks2']['code01'] = 'code01';
$Lang['MsschPrint']['management']['print']['importRemarks2']['code02'] = 'code02';
$Lang['MsschPrint']['management']['print']['importRemarks2']['code03'] = 'code03';
$Lang['MsschPrint']['management']['print']['importRemarks2']['code04'] = 'code04';
$Lang['MsschPrint']['management']['print']['importRemarks2']['jobType'] = 'jobType';
$Lang['MsschPrint']['management']['print']['importRemarks2']['jobTypeDetail'] = 'jobTypeDetail';
$Lang['MsschPrint']['management']['print']['importRemarks2']['status'] = 'status';
$Lang['MsschPrint']['management']['print']['importRemarks2']['colorMode'] = 'colorMode';
$Lang['MsschPrint']['management']['print']['importRemarks2']['pages'] = 'pages';
$Lang['MsschPrint']['management']['print']['importRemarks2']['sheet'] = 'sheet';
$Lang['MsschPrint']['management']['print']['importRemarks2']['paperSize'] = 'paperSize';
$Lang['MsschPrint']['management']['print']['importRemarks2']['statusReason'] = 'statusReason';


$Lang['MsschPrint']['management']['print']['importError']['dateFormatError'] = 'Invalid Date';
$Lang['MsschPrint']['management']['print']['importError']['dateFuture'] = 'Input Future Date';
$Lang['MsschPrint']['management']['print']['importError']['mfpNotFound'] = 'Invalid MFP Code';
$Lang['MsschPrint']['management']['print']['importError']['amountFormatError'] = 'Invalid Amount';
$Lang['MsschPrint']['management']['print']['importError']['groupNoMember'] = 'This group has no user';
$Lang['MsschPrint']['management']['print']['importError']['colorModeWrong'] = 'Invalid Color Mode';
$Lang['MsschPrint']['management']['print']['importError']['pagesWrong'] = 'Invalid Pages';
$Lang['MsschPrint']['management']['print']['importError']['sheetWrong'] = 'Invalid Sheet';
$Lang['MsschPrint']['management']['print']['importError']['paperSizeWrong'] = 'Invalid Paper Size';
$Lang['MsschPrint']['management']['print']['importError']['missingCostSetting'] = 'Please set the printing cost first.';
$Lang['MsschPrint']['management']['print']['ReImportRemarks'] = 'Re-import is needed if changed the printing cost settings or students\' exit date.';
//////// Print END ////////

//////// Deposit START ////////
$Lang['MsschPrint']['management']['deposit']['tableHeader']['title'] = 'Title';
$Lang['MsschPrint']['management']['deposit']['tableHeader']['date'] = 'Date';
$Lang['MsschPrint']['management']['deposit']['tableHeader']['amount'] = 'Amount';
$Lang['MsschPrint']['management']['deposit']['tableHeader']['userCount'] = 'Number of User';
$Lang['MsschPrint']['management']['deposit']['tableHeader']['pic'] = 'Admin In Charge';

$Lang['MsschPrint']['management']['deposit']['newDeposit'] = 'Deposit';
	$Lang['MsschPrint']['management']['deposit']['new']['amount'] = 'Amount';
	$Lang['MsschPrint']['management']['deposit']['new']['user'] = 'User';
$Lang['MsschPrint']['management']['deposit']['newErr']['titleEmpty'] = 'Please fill in the title';
$Lang['MsschPrint']['management']['deposit']['newErr']['amountEmpty'] = 'Please fill in the amount';
$Lang['MsschPrint']['management']['deposit']['newErr']['amountFormat'] = 'Please input correct amount';
$Lang['MsschPrint']['management']['deposit']['newErr']['userEmpty'] = 'Please select at least one user';
//////// Deposit END ////////

//////////////////// Management END ////////////////////


//////////////////// Report START ////////////////////
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['academicYear'] = $Lang['General']['AcademicYear'];
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['month'] = 'Month';
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['form'] = 'Form';
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['class'] = 'Class';
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['user'] = 'User';
$Lang['MsschPrint']['report']['printingSummary']['searchErr']['noUser'] = 'Please select user';
$Lang['MsschPrint']['report']['printingSummary']['searchErr']['wrongTimePeriod'] = 'End time period cannot be earlier then the start time';
$Lang['MsschPrint']['report']['printingSummary']['searchErr']['emptyForm'] = 'Please select form';

$Lang['MsschPrint']['report']['printingSummary']['result']['year'] = $Lang['General']['AcademicYear'];
$Lang['MsschPrint']['report']['printingSummary']['result']['userName'] = 'User Name';
$Lang['MsschPrint']['report']['printingSummary']['result']['totalDeposit'] = 'Total Deposit';
$Lang['MsschPrint']['report']['printingSummary']['result']['totalPrinting'] = 'Total Printing Usage';
$Lang['MsschPrint']['report']['printingSummary']['result']['Balance'] = 'Balance';

$Lang['MsschPrint']['report']['printingSummary']['result']['depositRecord'] = 'Deposit Record';
$Lang['MsschPrint']['report']['printingSummary']['result']['dateInput'] = 'Date';
$Lang['MsschPrint']['report']['printingSummary']['result']['amount'] = 'Amount';
$Lang['MsschPrint']['report']['printingSummary']['result']['inputBy'] = 'Admin In Charge';
$Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] = 'Total Amount';

$Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord'] = 'Printing Record';
$Lang['MsschPrint']['report']['printingSummary']['result']['month'] = 'Month';

$Lang['MsschPrint']['report']['printingSummary']['result']['class'] = 'Class';
$Lang['MsschPrint']['report']['printingSummary']['result']['allClass'] = 'All Classes';
$Lang['MsschPrint']['report']['printingSummary']['result']['form'] = 'Form';

$Lang['MsschPrint']['report']['printingSummary']['result']['accumulateBalance'] = 'Accumulate Balance';
$Lang['MsschPrint']['report']['printingSummary']['result']['amount'] = 'Amount';
$Lang['MsschPrint']['report']['printingSummary']['result']['currentBalance'] = 'Current Balance';

$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['title'] = 'Export for ePayment';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['startMonth'] = 'Start Month';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['endMonth'] = 'End Month';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['importHint'] = 'Please import to ePayment with "User Login" format';

$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][0] = 'User Login';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][1] = 'Student Name [Ref]';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][2] = 'Amount';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][3] = 'Remark';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][4] = 'Payment Method';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][0] = '(內聯網帳號)';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][1] = '(學生姓名)[參考用途]';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][2] = '(需繳付金額)';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][3] = '(備註)';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][4] = '(付款方法)';
//////////////////// Report END ////////////////////


//////////////////// Setting START ////////////////////

//////// Group START ////////
$Lang['MsschPrint']['setting']['group']['tableHeader']['mfpCode'] = 'MFP Code';
$Lang['MsschPrint']['setting']['group']['tableHeader']['name'] = 'Name';
$Lang['MsschPrint']['setting']['group']['tableHeader']['countMember'] = 'No. of Members';

$Lang['MsschPrint']['setting']['group']['addFromSubjectGroup'] = 'Add from subject group';
$Lang['MsschPrint']['setting']['group']['newGroup'] = 'New Group';
$Lang['MsschPrint']['setting']['group']['editGroup'] = 'Edit Group';
	$Lang['MsschPrint']['setting']['group']['edit']['mfpCode'] = 'MFP Code';
	$Lang['MsschPrint']['setting']['group']['edit']['name'] = 'Group Name';
	$Lang['MsschPrint']['setting']['group']['edit']['pic'] = 'PIC';
	$Lang['MsschPrint']['setting']['group']['edit']['member'] = 'Group Member';
$Lang['MsschPrint']['setting']['group']['editErr']['mfpCodeEmpty'] = 'Please fill in the MFP Code';
$Lang['MsschPrint']['setting']['group']['editErr']['mfpCodeDuplicate'] = 'MFP Code has been used';
$Lang['MsschPrint']['setting']['group']['editErr']['groupNameEmpty'] = 'Please fill in the Group Name';
$Lang['MsschPrint']['setting']['group']['editErr']['removeSelf'] = 'You can\'t remove yourself from PIC list';

$Lang['MsschPrint']['setting']['group']['delete']['printingAmount'] = 'Amount';
$Lang['MsschPrint']['setting']['group']['deleteHint']['groupNoContainsData'] = 'The item(s) selected has/have not been paid by any user. You may safely delete the item(s).';
$Lang['MsschPrint']['setting']['group']['deleteHint']['groupContainsData'] = 'The item(s) selected has/have been paid by user(s). Please undo the payment(s) concerned before delete.';
//////// Group END ////////

//////// Printing START ////////
$Lang['MsschPrint']['setting']['print']['ColorBwPrice'] = 'Black/White price (per pages)';
$Lang['MsschPrint']['setting']['print']['ColorFcPrice'] = 'Color price (per pages)';
$Lang['MsschPrint']['setting']['print']['A4Price'] = 'A4 price (per sheets)';
$Lang['MsschPrint']['setting']['print']['A3Price'] = 'A3 price (per sheets)';
$Lang['MsschPrint']['setting']['print']['OtherPrice'] = 'OTHER price (per sheets)';
$Lang['MsschPrint']['setting']['print']['OtherPriceRemarks'] = 'OTHER price only count with colour price, formula: Colour price X no. of sheets X 2<br />
E.g. Use OTHER printing with Black/White for two sheets (four pages) = Black/White price X 4 pages X 2';
$Lang['MsschPrint']['setting']['print']['ReImportRemarks'] = 'Re-import is needed if changed the printing cost settings for that month.';
//////// Printing END ////////

//////////////////// Setting END ////////////////////


//////////////////// General START ////////////////////
$Lang['MsschPrint']['general']['ImportData'] = 'Import Data';
$Lang['MsschPrint']['general']['import']['dataChecking'] = 'Data Checking';
$Lang['MsschPrint']['general']['import']['reason'] = 'Reason';
?>