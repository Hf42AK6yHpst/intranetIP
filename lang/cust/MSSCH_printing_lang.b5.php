<?php

$Lang['MsschPrint']['module'] = '影印紀錄';
$Lang['MsschPrint']['menu']['management'] = '管理';
	$Lang['MsschPrint']['menu']['print'] = '列印數據匯入';
	$Lang['MsschPrint']['menu']['deposit'] = '現金存入';
$Lang['MsschPrint']['menu']['report'] = '報告';
	$Lang['MsschPrint']['menu']['printingGroupSummary'] = '小組列印概覽';
	$Lang['MsschPrint']['menu']['printingClassSummary'] = '班別列印概覽';
	$Lang['MsschPrint']['menu']['printingUserSummary'] = '個人列印概覽';
$Lang['MsschPrint']['menu']['setting'] = '設定';
	$Lang['MsschPrint']['menu']['group'] = '小組設定';
	$Lang['MsschPrint']['menu']['printSetting'] = '列印設定';

//////////////////// Management START ////////////////////

//////// Print START ////////
$Lang['MsschPrint']['management']['print']['tableHeader']['date'] = '日期';
$Lang['MsschPrint']['management']['print']['tableHeader']['groupName'] = '小組名稱';
$Lang['MsschPrint']['management']['print']['tableHeader']['mfpCode'] = 'MFP 編號';
$Lang['MsschPrint']['management']['print']['tableHeader']['totalAmount'] = '金額';
$Lang['MsschPrint']['management']['print']['tableHeader']['modifiedBy'] = '負責人';
$Lang['MsschPrint']['management']['print']['tableHeader']['dateModified'] = '輸入日期';
$Lang['MsschPrint']['management']['print']['tableHeader']['colorMode'] = '顏色';
$Lang['MsschPrint']['management']['print']['tableHeader']['pages'] = '頁數';
$Lang['MsschPrint']['management']['print']['tableHeader']['sheet'] = '紙張數';
$Lang['MsschPrint']['management']['print']['tableHeader']['paperSize'] = '紙張尺寸';

$Lang['MsschPrint']['management']['print']['importRecord'] = '列印記錄數據';
$Lang['MsschPrint']['management']['print']['importMonth'] = '月份';
$Lang['MsschPrint']['management']['print']['importSkippedRecord'] = '跳過紀錄';
$Lang['MsschPrint']['management']['print']['importRemarks']['mfpCode'] = 'MFP 編號';
$Lang['MsschPrint']['management']['print']['importRemarks']['amount'] = '金額';
$Lang['MsschPrint']['management']['print']['importRemarks2']['lastSyncTime'] = 'lastSyncTime';
$Lang['MsschPrint']['management']['print']['importRemarks2']['code01'] = 'code01';
$Lang['MsschPrint']['management']['print']['importRemarks2']['code02'] = 'code02';
$Lang['MsschPrint']['management']['print']['importRemarks2']['code03'] = 'code03';
$Lang['MsschPrint']['management']['print']['importRemarks2']['code04'] = 'code04';
$Lang['MsschPrint']['management']['print']['importRemarks2']['jobType'] = 'jobType';
$Lang['MsschPrint']['management']['print']['importRemarks2']['jobTypeDetail'] = 'jobTypeDetail';
$Lang['MsschPrint']['management']['print']['importRemarks2']['status'] = 'status';
$Lang['MsschPrint']['management']['print']['importRemarks2']['colorMode'] = 'colorMode';
$Lang['MsschPrint']['management']['print']['importRemarks2']['pages'] = 'pages';
$Lang['MsschPrint']['management']['print']['importRemarks2']['sheet'] = 'sheet';
$Lang['MsschPrint']['management']['print']['importRemarks2']['paperSize'] = 'paperSize';
$Lang['MsschPrint']['management']['print']['importRemarks2']['statusReason'] = 'statusReason';

$Lang['MsschPrint']['management']['print']['importError']['dateFormatError'] = '日期錯誤';
$Lang['MsschPrint']['management']['print']['importError']['dateFuture'] = '輸入未來記錄';
$Lang['MsschPrint']['management']['print']['importError']['mfpNotFound'] = 'MFP 編號錯誤';
$Lang['MsschPrint']['management']['print']['importError']['amountFormatError'] = '金額錯誤';
$Lang['MsschPrint']['management']['print']['importError']['groupNoMember'] = '該小組沒有用戶';
$Lang['MsschPrint']['management']['print']['importError']['colorModeWrong'] = '顏色錯誤';
$Lang['MsschPrint']['management']['print']['importError']['pagesWrong'] = '頁數錯誤';
$Lang['MsschPrint']['management']['print']['importError']['sheetWrong'] = '紙張數錯誤';
$Lang['MsschPrint']['management']['print']['importError']['paperSizeWrong'] = '紙張尺寸錯誤';
$Lang['MsschPrint']['management']['print']['importError']['missingCostSetting'] = '請先設定列印價錢';
$Lang['MsschPrint']['management']['print']['ReImportRemarks'] = '如更改列印價錢或學生離校日期則需要重新匯入該月份的數據';
//////// Print END ////////

//////// Deposit START ////////
$Lang['MsschPrint']['management']['deposit']['tableHeader']['title'] = '標題';
$Lang['MsschPrint']['management']['deposit']['tableHeader']['date'] = '日期';
$Lang['MsschPrint']['management']['deposit']['tableHeader']['amount'] = '金額';
$Lang['MsschPrint']['management']['deposit']['tableHeader']['userCount'] = '用戶數量';
$Lang['MsschPrint']['management']['deposit']['tableHeader']['pic'] = '負責人';

$Lang['MsschPrint']['management']['deposit']['newDeposit'] = '存入現金';
	$Lang['MsschPrint']['management']['deposit']['new']['amount'] = '金額';
	$Lang['MsschPrint']['management']['deposit']['new']['user'] = '用戶';
$Lang['MsschPrint']['management']['deposit']['newErr']['titleEmpty'] = '請填上 標題';
$Lang['MsschPrint']['management']['deposit']['newErr']['amountEmpty'] = '請填上 金額';
$Lang['MsschPrint']['management']['deposit']['newErr']['amountFormat'] = '請輸入正確金額';
$Lang['MsschPrint']['management']['deposit']['newErr']['userEmpty'] = '請選擇最少一名用戶';
//////// Deposit END ////////

//////////////////// Management END ////////////////////


//////////////////// Report START ////////////////////
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['academicYear'] = $Lang['General']['AcademicYear'];
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['month'] = '月份';
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['form'] = '級別';
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['class'] = '班別';
$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['user'] = '用戶';
$Lang['MsschPrint']['report']['printingSummary']['searchErr']['noUser'] = '請選擇用戶';
$Lang['MsschPrint']['report']['printingSummary']['searchErr']['wrongTimePeriod'] = '結束時段不能早於開始時段';
$Lang['MsschPrint']['report']['printingSummary']['searchErr']['emptyForm'] = '請選擇級別';

$Lang['MsschPrint']['report']['printingSummary']['result']['year'] = $Lang['General']['AcademicYear'];
$Lang['MsschPrint']['report']['printingSummary']['result']['userName'] = '用戶';
$Lang['MsschPrint']['report']['printingSummary']['result']['totalDeposit'] = '存款金額';
$Lang['MsschPrint']['report']['printingSummary']['result']['totalPrinting'] = '列印金額';
$Lang['MsschPrint']['report']['printingSummary']['result']['Balance'] = '結餘';

$Lang['MsschPrint']['report']['printingSummary']['result']['depositRecord'] = '存款紀錄';
$Lang['MsschPrint']['report']['printingSummary']['result']['dateInput'] = '日期';
$Lang['MsschPrint']['report']['printingSummary']['result']['amount'] = '金額';
$Lang['MsschPrint']['report']['printingSummary']['result']['inputBy'] = '負責人';
$Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] = '總金額';

$Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord'] = '列印紀錄';
$Lang['MsschPrint']['report']['printingSummary']['result']['month'] = '月份';

$Lang['MsschPrint']['report']['printingSummary']['result']['class'] = '班別';
$Lang['MsschPrint']['report']['printingSummary']['result']['allClass'] = '全部班別';
$Lang['MsschPrint']['report']['printingSummary']['result']['form'] = '級別';

$Lang['MsschPrint']['report']['printingSummary']['result']['accumulateBalance'] = '累計結餘';
$Lang['MsschPrint']['report']['printingSummary']['result']['amount'] = '金額';
$Lang['MsschPrint']['report']['printingSummary']['result']['currentBalance'] = '當前結餘';

$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['title'] = '匯出至智能咭繳費系統';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['startMonth'] = '開始月份';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['endMonth'] = '結束月份';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['importHint'] = '請使用 "內聯網帳號" 格式匯入至 智能咭繳費系統';

$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][0] = 'User Login';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][1] = 'Student Name [Ref]';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][2] = 'Amount';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][3] = 'Remark';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'][4] = 'Payment Method';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][0] = '(內聯網帳號)';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][1] = '(學生姓名)[參考用途]';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][2] = '(需繳付金額)';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][3] = '(備註)';
$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'][4] = '(付款方法)';
//////////////////// Report END ////////////////////


//////////////////// Setting START ////////////////////

//////// Group START ////////
$Lang['MsschPrint']['setting']['group']['tableHeader']['mfpCode'] = 'MFP 編號';
$Lang['MsschPrint']['setting']['group']['tableHeader']['name'] = '名稱';
$Lang['MsschPrint']['setting']['group']['tableHeader']['countMember'] = '組員數量';

$Lang['MsschPrint']['setting']['group']['addFromSubjectGroup'] = '從科組新增';
$Lang['MsschPrint']['setting']['group']['newGroup'] = '新增小組';
$Lang['MsschPrint']['setting']['group']['editGroup'] = '編輯小組';
	$Lang['MsschPrint']['setting']['group']['edit']['mfpCode'] = 'MFP 編號';
	$Lang['MsschPrint']['setting']['group']['edit']['name'] = '小組名稱';
	$Lang['MsschPrint']['setting']['group']['edit']['pic'] = '負責人';
	$Lang['MsschPrint']['setting']['group']['edit']['member'] = '小組成員';
$Lang['MsschPrint']['setting']['group']['editErr']['mfpCodeEmpty'] = '請填上 MFP 編號';
$Lang['MsschPrint']['setting']['group']['editErr']['mfpCodeDuplicate'] = 'MFP 編號已被使用';
$Lang['MsschPrint']['setting']['group']['editErr']['groupNameEmpty'] = '請填上 小組名稱';
$Lang['MsschPrint']['setting']['group']['editErr']['removeSelf'] = '你不能從負責人列表中移除自己';

$Lang['MsschPrint']['setting']['group']['delete']['printingAmount'] = '金額';
$Lang['MsschPrint']['setting']['group']['deleteHint']['groupNoContainsData'] = '未有學生就所選繳費項目進行繳款, 你可以放心刪除。';
$Lang['MsschPrint']['setting']['group']['deleteHint']['groupContainsData'] = '已有用戶就所選繳費項目進行繳款, 請還原有關繳款後再行刪除。';
//////// Group END ////////

//////// Printing START ////////
$Lang['MsschPrint']['setting']['print']['ColorBwPrice'] = '每頁 黑白 打印價錢';
$Lang['MsschPrint']['setting']['print']['ColorFcPrice'] = '每頁 彩色 打印價錢';
$Lang['MsschPrint']['setting']['print']['A4Price'] = '每張 A4 紙價錢';
$Lang['MsschPrint']['setting']['print']['A3Price'] = '每張 A3 紙價錢';
$Lang['MsschPrint']['setting']['print']['OtherPrice'] = '每頁 OTHER 紙價錢';
$Lang['MsschPrint']['setting']['print']['OtherPriceRemarks'] = 'OTHER 紙只計算顏色價錢，計算公式為： 顏色價錢 x 打印頁數 x 2<br/>
例： 使用 OTHER 紙黑白雙面打印兩張紙(四頁) = 每頁黑白打印價錢 x 4頁 x 2';
$Lang['MsschPrint']['setting']['print']['ReImportRemarks'] = '如更改列印價錢則需要重新匯入該月份的數據';
//////// Printing END ////////

//////////////////// Setting END ////////////////////


//////////////////// General START ////////////////////
$Lang['MsschPrint']['general']['ImportData'] = '匯入數據';
$Lang['MsschPrint']['general']['import']['dataChecking'] = '數據檢查';
$Lang['MsschPrint']['general']['import']['reason'] = '錯誤備註';
?>