<?php
//updating by : 

$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Title']['b5'] = "訓導組個人奬懲紀錄";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SchoolName']['b5'] = "佛教筏可紀念中學";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['StudentName']['b5'] = "學生姓名";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['ClassNameNumber']['b5'] = "班別";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Date']['b5'] = "日期";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['RecordDetails']['b5'] = "個案內容";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['APRecord']['b5'] = "奬懲方法";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Times']['b5'] = "次";

$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Warning']['b5'] = "警告";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['BlackMark']['b5'] = "缺點";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MinorDemerit']['b5'] = "小過";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MajorDemerit']['b5'] = "大過 ";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SuperDemerit']['b5'] = "超過 ";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['UltraDemerit']['b5'] = "極過 ";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Merit']['b5'] = "優點";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MinorCredit']['b5'] = "小功";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MajorCredit']['b5'] = "大功";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SuperCredit']['b5'] = "超功";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['UltraCredit']['b5'] = "極功";

$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Title']['en'] = "Individual Discipline Record";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SchoolName']['en'] = "Buddhist Fat Ho Memorial College";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['StudentName']['en'] = "Name";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['ClassNameNumber']['en'] = "Class";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Date']['en'] = "Date";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['RecordDetails']['en'] = "Case Details";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['APRecord']['en'] = "Awards/ Punishment";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Times']['en'] = "Time(s)";

$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Warning']['en'] = "Warning";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['BlackMark']['en'] = "Black Mark";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MinorDemerit']['en'] = "Minor Demerit";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MajorDemerit']['en'] = "Major Demerit ";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SuperDemerit']['en'] = "Super Demerit ";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['UltraDemerit']['en'] = "Ultra Demerit";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['Merit']['en'] = "Merit";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MinorCredit']['en'] = "Minor Credit";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['MajorCredit']['en'] = "Major Credit";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['SuperCredit']['en'] = "Super Credit";
$Lang['eDiscipline']['IndividualDisciplineRecord_cust']['UltraCredit']['en'] = "Ultra Credit";

?>