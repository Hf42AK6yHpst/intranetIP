<?php
// Using:anna

// ### Menu START ####
$Lang['SDAS']['menu']['academic'] = 'Academic';
$Lang['SDAS']['menu']['AcademicProgress'] = 'Academic Progress';
$Lang['SDAS']['menu']['ClassSubjectComparison'] = 'Class Subject Comparison';
$Lang['SDAS']['menu']['ClassPerformance'] = 'Class Performance';
$Lang['SDAS']['menu']['DseAnalysis'] = 'DSE Analysis';
$Lang['SDAS']['menu']['DsePrediction'] = 'DSE Prediction';
$Lang['SDAS']['menu']['DsePredictionEvaluation'] = 'DSE Prediction Evaluation';
$Lang['SDAS']['menu']['DsePredictionDegreeDiploma'] = 'Degree & Diploma Prediction';
$Lang['SDAS']['menu']['PassingRateStats'] = 'Passing Rate Stats';
$Lang['SDAS']['menu']['StudentPerformanceTracking'] = 'Student Performance Tracking';
$Lang['SDAS']['menu']['SubjectImprovementStats'] = 'Subject Improvement Stats';
$Lang['SDAS']['menu']['SubjectStats'] = 'Subject Stats';
$Lang['SDAS']['menu']['TeachingImprovementStats'] = 'Teaching Improvement Stats';
$Lang['SDAS']['menu']['SubjectClassDistribution'] = 'Class Subject Performance Distribution';
$Lang['SDAS']['menu']['FormStudentPerformance'] = "Form Student Performance";
$Lang['SDAS']['menu']['StudentCrossYearPerformance'] = "Student Cross Year Performance";
$Lang['SDAS']['menu']['FormPassingRate'] = 'Form Passing Rate';
$Lang['SDAS']['menu']['LearnAndTeach'] = 'L&T';
$Lang['SDAS']['menu']['ValueAddedData'] = 'Value-added Data Report';
$Lang['SDAS']['menu']['ReportComparsion'] = 'Report Comparison';
$Lang['SDAS']['menu']['customReport'] = 'School-based Report';
$Lang['SDAS']['menu']['ClassPromotionAssessment'] = 'Class Promotion Assessment';
$Lang['SDAS']['menu']['management'] = 'Management';
$Lang['SDAS']['menu']['examMgmt'] = 'Data Import';
$Lang['SDAS']['menu']['CEES'] = 'Submitting to CEES';
$Lang['SDAS']['menu']['settings'] = 'Settings';
$Lang['SDAS']['menu']['accessRight'] = 'Access Rights';
$Lang['SDAS']['menu']['percentile'] = 'Percentile Rank';
$Lang['SDAS']['menu']['SchBaseReportConfig'] = "School-based Report";
$Lang['SDAS']['menu']['MonitoringGroupSetting'] = "Monitoring Group Setting";
$Lang['SDAS']['menu']['PreS1Analysis'] = "Pre-S1 Analysis";

$Lang['SDAS']['toCEES']['RecordType'] = 'Record Type';
$Lang['SDAS']['toCEES']['NotSubmit'] = 'Not submitted yet';
$Lang['SDAS']['toCEES']['NeedResubmit'] = 'Need to re-submit';
$Lang['SDAS']['toCEES']['Resubmit'] = 'Re-submit';
$Lang['SDAS']['toCEES']['Endorsed'] = 'Confirmed by Principal';
$Lang['SDAS']['toCEES']['VerifiedByCEES'] = 'Certified by Education Division';
$Lang['SDAS']['toCEES']['PleaseEndorseDSEfirst'] = 'Please endorse DSE results first';
$Lang['SDAS']['toCEES']['confirmAppealNoData'] = 'Confirm no appeal record';
$Lang['SDAS']['toCEES']['PleaseConfirmAppealNoData'] = 'Please confirm there is no appeal record';
$Lang['SDAS']['toCEES']['PleaseSyncOnceMore'] = 'Data do not synced with group-level.<br> Please submit once more before endorse.';
$Lang['SDAS']['toCEES']['Duration'] = 'Submission Period';
$Lang['SDAS']['toCEES']['DataFile'] = 'Data File';
$Lang['SDAS']['toCEES']['Status'] = 'Status';
$Lang['SDAS']['toCEES']['LastUpdated'] = 'Last Submission';
$Lang['SDAS']['toCEES']['SchoolYear'] = 'School Year';
$Lang['SDAS']['toCEES']['PrincipalConfirm'] = 'DSE Result Confirmation';
$Lang['SDAS']['toCEES']['Confirm'] = 'Confirm to submit ?';
$Lang['SDAS']['toCEES']['Confirmed'] = 'Confirmed';
$Lang['SDAS']['toCEES']['NoNeedConfirm'] = 'No record need to confirm';
$Lang['SDAS']['toCEES']['LastConfirm'] = 'Last Confirmation';
$Lang['SDAS']['toCEES']['principalInstruction'] = 'Principal please kindly check the DSE reports:<br>
<br>
1. <a href="index.php?t=academic.dse_stats.dse_distribution">DSE Distribution</a><br>
2. <a href="index.php?t=academic.dse_stats.attendance">DSE Attendance</a><br>
3. <a href="index.php?t=academic.dse_stats.best_five_score">Best 5 Grade Point</a><br>
4. <a href="index.php?t=academic.dse_stats.university">Stats on Meeting University Requirement</a><br>
5. <a href="index.php?t=academic.dse_stats.best_student">Best Student</a><br>
<br>
If the reports are okay to report to TWGHs Education Division, please confirm here.<br>
If there is any question or problem found, please discuss with your colleague. Thanks.';
$Lang['SDAS']['CEES']['MonthlyReport'] = 'Monthly Report';
$Lang['SDAS']['CEES']['MonthlyReportNotification']['Title'] = '每月報表呈交提示';
$Lang['SDAS']['CEES']['MonthlyReportNotification']['Content'] = '請呈交上月之每月報表至中央平台。<br> 你可登入__WEBSITE__，然後前往「中央電子教育系統 (學校) > 管理 > 每月報表」處理。<br>';

$Lang['SDAS']['DSE']['exportAll'] = 'Export all record (format 2)';
$Lang['SDAS']['DSE']['ImportError']['grade'] = 'Wrong grade or level';
// # DSE analysis START ####
$Lang['SDAS']['DSEstat']['ClassName'] = 'Class Name';
$Lang['SDAS']['DSEstat']['ClassNumber'] = 'Class Number';
$Lang['SDAS']['DSEstat']['DSELevel']['Title'] = 'DSE Level';
$Lang['SDAS']['DSEstat']['DSELevel']['X'] = 'Abs';
$Lang['SDAS']['DSEstat']['DSELevel']['U'] = 'Lv U';
$Lang['SDAS']['DSEstat']['DSELevel']['1'] = 'Lv 1';
$Lang['SDAS']['DSEstat']['DSELevel']['2'] = 'Lv 2';
$Lang['SDAS']['DSEstat']['DSELevel']['3'] = 'Lv 3';
$Lang['SDAS']['DSEstat']['DSELevel']['4'] = 'Lv 4';
$Lang['SDAS']['DSEstat']['DSELevel']['5'] = 'Lv 5';
$Lang['SDAS']['DSEstat']['DSELevel']['5*'] = 'Lv 5*';
$Lang['SDAS']['DSEstat']['DSELevel']['5**'] = 'Lv 5**';
$Lang['SDAS']['DSEstat']['orAbove'] = " or above";
$Lang['SDAS']['DSEstat']['NumApplied'] = "Num of applied";
$Lang['SDAS']['DSEstat']['NumSat'] = "Num of sat";
$Lang['SDAS']['DSEstat']['form']['4'] = 'F.4';
$Lang['SDAS']['DSEstat']['form']['5'] = 'F.5';
$Lang['SDAS']['DSEstat']['form']['6'] = 'F.6';
$Lang['SDAS']['DSEstat']['Mock'] = 'Mock result';
$Lang['SDAS']['DSEstat']['Int'] = 'Assessment result';
$Lang['SDAS']['DSEstat']['Alert']['NoData'] = 'No data for analysis';
$Lang['SDAS']['DSEstat']['barChartTitle'] = 'DSE Distribution';
$Lang['SDAS']['DSEstat']['box1'] = 'DSE  vs Mock Result';
$Lang['SDAS']['DSEstat']['box2'] = 'DSE  vs "FormName" Assessment Result';
$Lang['SDAS']['DSEstat']['boxplot_remarks'] = '*Figure above are rounding up to nearest integer';
$Lang['SDAS']['DSEstat']['box_y'] = 'Score in Exam';
$Lang['SDAS']['DSEstat']['bar_y'] = 'Number of Student';
$Lang['SDAS']['DSEstat']['percent'] = 'Percentage (%)';
$Lang['SDAS']['DSEstat']['number'] = 'Number';
$Lang['SDAS']['DSEstat']['number2'] = 'Number';
$Lang['SDAS']['DSEstat']['Totalnumber'] = 'Total';
$Lang['SDAS']['DSETopStudent']['Top5']['Title'][0] = 'Student scoring <!--score--> points or above in 4 Core Subjects plus 2 Electives Subjects';
$Lang['SDAS']['DSETopStudent']['Top5']['Title'][1] = 'No student scoring <!--score--> points or above in 4 Core Subjects plus 2 Electives Subjects';
$Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][0] = 'Students obtaining <!--score--> points or above in the best <!--subject--> subjects';
$Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][1] = 'No student meeting <!--score--> points in <!--subject--> subjects';
$Lang['SDAS']['DSETopStudent']['Table'] = 'Table';
$Lang['SDAS']['DSEUniversity']['Title'] = 'Stats on Meeting University Requirement';
$Lang['SDAS']['DSEBestFive']['Title'] = 'Best 5 Grade Point';
$Lang['SDAS']['TitleNumber'] = '(Number)';
$Lang['SDAS']['TitleNumber2'] = '(Number)';
$Lang['SDAS']['TitlePercent'] = '(Percentage)';
$Lang['SDAS']['ColumnNumber'] = 'No. of students';
$Lang['SDAS']['ColumnPercent'] = 'Percentage(%)';
$Lang['SDAS']['ColumnNumberShort2'] = 'No.';
$Lang['SDAS']['ColumnNumberShort'] = 'No.';
$Lang['SDAS']['ColumnPercentShort'] = '%';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['55'] = 'Core subjects at 3322, plus level 5 in two electives or above';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['5'] = 'Core subjects at 3322, plus level 5 in one electives or above';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['44'] = 'Core subjects at 3322, plus level 4 in two electives or above';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['4'] = 'Core subjects at 3322, plus level 4 in one electives or above';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['33'] = 'Core subjects at 3322, plus level 3 in two electives or above';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['3'] = 'Core subjects at 3322, plus level 3 in one electives or above';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['coreCount'] = 'Core subjects at 3322 or above';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['22'] = "Levels gained 3(Chi)-3(Eng)-2(Maths)-2(LS), plus level 2 in two elective subjects or above";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['2'] = "Levels gained 3(Chi)-3(Eng)-2(Maths)-2(LS), plus level 2 in one elective subject or above";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['core22222'] = "Levels gained level 2 or above in five subjects,<br>including Chinese Language, English Language, Mathematics & LS";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['CE22222'] = "Levels gained level 2 or above in five subjects,<br>including Chinese Language and English Language";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['22222'] = 'Students obtaining level 2 or above in any five subjects';
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['totalNum'] = 'Total';
$Lang['SDAS']['DseDropEnterSatReport']['column'][0] = 'No. Sat (Mock)';
$Lang['SDAS']['DseDropEnterSatReport']['column'][1] = 'No. Applied (DSE)';
$Lang['SDAS']['DseDropEnterSatReport']['column'][2] = 'No. Sat (DSE)';
$Lang['SDAS']['DsePerformanceTrend']['Title'] = 'Performance Trend';
$Lang['SDAS']['DseDistribution']['Title'] = 'DSE Distribution';
$Lang['SDAS']['DseDistributionSubjectGroup']['Title'] = 'DSE Distribution (Subject Group)';
$Lang['SDAS']['DseBestStudent']['Title'] = 'Best Student';
$Lang['SDAS']['DseAttendance']['Title'] = 'DSE Attendance';
$Lang['SDAS']['DseVsMock']['Title'] = 'DSE vs mock';
$Lang['SDAS']['DseVsGpa']['Title'] = 'DSE vs GPA';
$Lang['SDAS']['DisplayBy'] = 'Display By';
$Lang['SDAS']['SubjectNameDisplay'] = "Subject Name Display";
$Lang['SDAS']['SubjectNameShortForm'] = "Short Form";
$Lang['SDAS']['SubjectNameFullName'] = "Full Name";
$Lang['SDAS']['DisplayItem'] = "Display Item";
$Lang['SDAS']['TotalScore'] = 'Total';
// # DSE analysis END ####

// # DSE prediction START ####
$Lang['SDAS']['DSEprediction']['DseLevel'][1] = 'Level 1';
$Lang['SDAS']['DSEprediction']['DseLevel'][2] = 'Level 2';
$Lang['SDAS']['DSEprediction']['DseLevel'][3] = 'Level 3';
$Lang['SDAS']['DSEprediction']['DseLevel'][4] = 'Level 4';
$Lang['SDAS']['DSEprediction']['DseLevel'][5] = 'Level 5';
$Lang['SDAS']['DSEprediction']['DseLevel'][6] = 'Level 5*';
$Lang['SDAS']['DSEprediction']['DseLevel'][7] = 'Level 5**';
$Lang['SDAS']['DSEprediction']['DseLevelOrAbove'] = 'or above';
$Lang['SDAS']['DSEprediction']['FilterType']['UniversityRequire'] = 'University entry requirement';
$Lang['SDAS']['DSEprediction']['FilterType']['BestFive'] = 'Total score of best five subjects';
$Lang['SDAS']['DSEprediction']['FilterType']['Subject'] = 'Individual subject';
$Lang['SDAS']['DSEprediction']['PredictMethod'] = 'Predict Method';
$Lang['SDAS']['DSEprediction']['Method']['Percentile'] = 'Percentile';
$Lang['SDAS']['DSEprediction']['Method']['ConfidenceInterval'] = 'Confidence Interval (95%)';
$Lang['SDAS']['DSEprediction']['Display'] = 'Display';
$Lang['SDAS']['DSEprediction']['Filter'] = 'Filter By';
$Lang['SDAS']['DSEprediction']['Meet'] = 'If meet';
$Lang['SDAS']['DSEprediction']['NotMeet'] = 'If cannot meet';
$Lang['SDAS']['DSEprediction']['compulsory'] = 'Compulsory subject requirement';
$Lang['SDAS']['DSEprediction']['elective'] = 'Elective subject requirement';
$Lang['SDAS']['DSEprediction']['remarks'] = 'Please use comma(,) to separate the grade';
$Lang['SDAS']['DSEprediction']['SummaryTable'] = 'Summary of Predicted Results';
$Lang['SDAS']['DSEprediction']['IndividualStudent'] = 'Individual Student';
$Lang['SDAS']['DSEprediction']['PercentileTable'] = 'Level and Percentile Mapping';
$Lang['SDAS']['DSEprediction']['ConfidenceIntervalTable'] = 'Level and Confidence Interval Mapping';
$Lang['SDAS']['DSEprediction']['BestFiveTotal'] = 'Total Score From Best 5';
$Lang['SDAS']['DSEprediction']['FirstLine'] = 'First line is predicted DSE result';
$Lang['SDAS']['DSEprediction']['SecondLine'] = 'Second line is <!--type-->';
$Lang['SDAS']['DSEprediction']['ThirdLine'] = 'Third line is percentile in the <!--exam-->';
$Lang['SDAS']['DSEprediction']['percentileRemark'] = 'percentile in the <!--exam-->';
$Lang['SDAS']['DSEprediction']['scoreRemark'] = 'score of student in the <!--exam-->';
$Lang['SDAS']['DSEprediction']['assessment'] = 'assessment';
$Lang['SDAS']['DSEprediction']['mock'] = 'mock exam';
$Lang['SDAS']['DSEprediction']['remarks2'] = 'Level and Percentile Mapping is generated by <!--Year--> student\'s performance in DSE';
$Lang['SDAS']['DSEprediction']['remarksConfidenceInterval'] = 'Level and Confidence Interval Mapping is generated by <!--Year--> student\'s performance in DSE';
$Lang['SDAS']['DSEprediction']['to'] = ' to ';
$Lang['SDAS']['DSEprediction']['customWeighting'] = 'Custom Weighting';
$Lang['SDAS']['DSEprediction']['refYear'] = 'Reference Years';
$Lang['SDAS']['DSEprediction']['saveEvaluationReport'] = 'Save to evaluation report';
$Lang['SDAS']['DSEprediction']['saveEvaluationReportSuccess'] = 'Data saved successfully.';
$Lang['SDAS']['DSEprediction']['saveEvaluationReportUnsuccess'] = 'Data saved unsuccessful.';
$Lang['SDAS']['DSEprediction']['ConfirmOverwrite'] = "System contains data, confirm update data?";
$Lang['SDAS']['DSEprediction']['SelectRefYearWarning'] = "Please select reference year";
// # DSE prediction END ####

// # DSE prediction evaluation START ####
$Lang['SDAS']['DSEpredictionEvaluation']['EvaluationReport'] = 'Evaluation Report';
$Lang['SDAS']['DSEpredictionEvaluation']['EvaluationReport'] = 'Evaluation Report';
$Lang['SDAS']['DSEpredictionEvaluation']['AllSubject'] = 'All Subject';
$Lang['SDAS']['DSEpredictionEvaluation']['PredictGrade'] = 'Predict Grade';
$Lang['SDAS']['DSEpredictionEvaluation']['DseResult'] = 'DSE Result';
$Lang['SDAS']['DSEpredictionEvaluation']['DsePredictDiff'] = 'DSE Predict different';
$Lang['SDAS']['DSEpredictionEvaluation']['DsePredictDiffAbs'] = 'DSE Predict absolute different';
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfPass'] = 'No. of Pass';
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfPassSubject'] = 'No. of Pass Subject';
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfCandidate'] = 'No. of Candidate';
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfSubject'] = 'No. of Subject';
$Lang['SDAS']['DSEpredictionEvaluation']['PassPercent'] = 'Pass Rate';
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfLevel'] = 'No. of <!--Level-->';
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfLevelStudent'] = 'No. of <!--Level-->';
$Lang['SDAS']['DSEpredictionEvaluation']['ME'] = 'Mean Error';
$Lang['SDAS']['DSEpredictionEvaluation']['MAE'] = 'Mean absolute error';
// # DSE prediction evaluation END ####

$Lang['SDAS']['Exam']['HKAT'] = 'Attainment Test';
$Lang['SDAS']['Exam']['HKDSE'] = 'HKDSE';
$Lang['SDAS']['Exam']['HKDSE_APPEAL'] = 'HKDSE(APPEAL)';
$Lang['SDAS']['Exam']['Mock'] = 'Mock Exam';
$Lang['SDAS']['Exam']['FinalExam'] = 'Final Exam';
$Lang['SDAS']['Exam']['SchoolExam'] = 'School Assessment Record';
$Lang['SDAS']['Exam']['OtherExam'] = 'Other Exam Record';
$Lang['SDAS']['Exam']['archivedStudent'] = 'Archived student';
$Lang['SDAS']['Exam']['ImportOtherExam'] = 'Import Other Exam Record';
$Lang['SDAS']['Exam']['ExportOtherExam'] = 'Export Other Exam Record';
$Lang['SDAS']['Exam']['EraseOtherExam'] = 'Erase Other Exam Record';
$Lang['SDAS']['Exam']['NoRecordToErase'] = 'No record can erase';
$Lang['SDAS']['Exam']['ContinueToErase'] = 'Continue to erase';
$Lang['SDAS']['Exam']['ImportFileFormat']['Title'] = 'File format';
$Lang['SDAS']['Exam']['ImportFileFormat']['Default'] = 'eClass format';
$Lang['SDAS']['Exam']['ImportFileFormat']['WebSAMS'] = 'WebSAMS format';
$Lang['SDAS']['Exam']['ImportFileFormat']['HKEAA'] = 'HKEAA format';
$Lang['SDAS']['Exam']['Template'] = 'Template';
$Lang['SDAS']['Exam']['PleaseSelectType'] = 'Please select record type.';
$Lang['SDAS']['Exam']['FormatRemarks']['HKEAA'] = 'Remark: Using HKEAA format require student\'s STRN or HKID';
$Lang['SDAS']['Exam']['FormatRemarks']['Regno'] = 'Remark: To ensure the number is complete, # has to be added to RegNo (e.g. "#0025136").';
$Lang['SDAS']['Exam']['Import']['InvalidRecords'] = 'Invalid Record';
$Lang['SDAS']['Exam']['Import']['MoreThanOneTerm'] = 'More than one assessment result is found in the file';
$Lang['SDAS']['Exam']['Import']['Error'] = 'Error !!';
$Lang['SDAS']['Exam']['Import']['MisMatchTerm'] = 'The file provide <b>T<!--termNoFromFile--></b>. But you chose <b><!--displayChoseTerm--></b>, should it be <b><!--displaySuggestionTermName--></b>?';
$Lang['SDAS']['Exam']['Import']['ConfirmToContinue'] = 'My choice is correct, please proceed.';
$Lang['SDAS']['Exam']['Import']['NoDseRecord'] = 'Import terminated as target academic year do not found any DSE result.';
$Lang['SDAS']['Exam']['Import']['NoRecordToUpdate'] = 'No record need to update.';
$Lang['SDAS']['Exam']['Import']['AlreadyAppealNotAllowToImport'] = 'DSE(Appeal) record is found, you are not allowed to modify DSE results.';
$Lang['SDAS']['Exam']['Import']['EndorsedNotAllowToImport'] = 'Import terminated as HKDSE results are endorsed and submitted to CEES';
$Lang['SDAS']['Exam']['Import']['EndorsedNotAllowToImport_Appeal'] = 'Import terminated as HKDSE(Appeal) results are endorsed and submitted to CEES';
$Lang['SDAS']['Exam']['Import']['NotAllowRemoveAsEndorse'] = 'Not allow to remove as the results are endorsed and submmitted to CEES';
// ### Menu END ####

// ### Normal START ####
$Lang['SDAS']['Warning']['PleaseSelectSubject'] = 'Please Select Subject';
$Lang['SDAS']['Warning']['PleaseSelectForm'] = 'Please Select Form';
$Lang['SDAS']['Warning']['PleaseSelectAcademicYear'] = 'Please Select Academic Year';
// ### Normal END ####

// ### Class Performance START ####
$Lang['SDAS']['ClassPerformance']['IndividualYear'] = 'Individual Year';
$Lang['SDAS']['ClassPerformance']['CrossYear'] = 'Cross-Year';
$Lang['SDAS']['ClassPerformance']['CrossTerm'] = "According to Term";
// ### Class Performance END ####

// ### Student Performance Tracking START ####
$Lang['SDAS']['StudentPerformanceTracking']['Marksheet'] = 'Marksheet';
$Lang['SDAS']['StudentPerformanceTracking']['Percentile'] = 'Percentile';
$Lang['SDAS']['StudentPerformanceTracking']['FormPercentile'] = 'Form position percentile';
// ### Student Performance Tracking END ####

// ### Teaching Improvement Stats START ####
$Lang['SDAS']['TeachingImprovementStats']['SelectDiffCompare'] = 'Please select different compare items.';
// ### Teaching Improvement Stats END ####

// ### AcademicProgress START ####
$Lang['SDAS']['AcademicProgress']['Selection'] = 'Selection';
$Lang['SDAS']['AcademicProgress']['Sort'] = 'Sort By';
$Lang['SDAS']['AcademicProgress']['Order'] = 'Order By';
$Lang['SDAS']['AcademicProgress']['Ordering']['Class'] = 'By Class & Class Number';
$Lang['SDAS']['AcademicProgress']['Ordering']['ScoreDiff'] = 'By Score Difference';
$Lang['SDAS']['AcademicProgress']['Ordering']['ZScoreDiff'] = 'By Standard Score Difference';
$Lang['SDAS']['AcademicProgress']['Ordering']['Asc'] = 'Ascending Order';
$Lang['SDAS']['AcademicProgress']['Ordering']['Dec'] = 'Decending Order';
$Lang['SDAS']['AcademicProgress']['Color'] = 'Mark Difference Highlight';
$Lang['SDAS']['AcademicProgress']['highlight']['red'] = ' or lower will be highlighted in red';
$Lang['SDAS']['AcademicProgress']['highlight']['blue'] = ' or higher will be highlighted in blue';
$Lang['SDAS']['AcademicProgress']['HideShow'] = 'Click class(es) to show/hide';
$Lang['SDAS']['AcademicProgress']['PleaseEnterInt'] = 'Please Enter Positive Integer';
$Lang['SDAS']['AcademicProgress']['PleaseSelectSubject'] = 'Please Select Subject';
$Lang['SDAS']['AcademicProgress']['PleaseSelect'] = '--Please Select--';
$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] = 'Monitoring Group';
// ### AcademicProgress END ####

// ### Subject Stat START ####
$Lang['SDAS']['SubjectStat']['SingleYear'] = 'Single Year';
$Lang['SDAS']['SubjectStat']['MultipleYear'] = 'Multiple Year';
// ### Subject Stat END ####

// ### JUPAS START ####
$Lang['SDAS']['JUPASRecord'] = 'JUPAS Offer Record';

$Lang['SDAS']['ImportJUPASRecord'] = 'Import JUPAS Offer Record';
$Lang['SDAS']['ExportJUPASRecord'] = 'Export JUPAS Offer Record';
$Lang['SDAS']['Error']['institution'] = 'Wrong Institution';
$Lang['SDAS']['Error']['funding'] = 'Wrong Funding';
$Lang['SDAS']['Error']['round'] = 'Wrong Round';
$Lang['SDAS']['Error']['status'] = 'Wrong Status';
$Lang['SDAS']['Error']['noSubject'] = 'No corresponding subject';
$Lang['SDAS']['Error']['JUPASempty'] = 'Application number and JUPAS code cannot be empty';
$Lang['SDAS']['Error']['JUPASinvalid'] = 'JUPAS programme code is not valid';
// ### JUPAS END ####


#### Exit to START ###########
$Lang['SDAS']['ExitTo']['LeftYear'] = "Left Year";
$Lang['SDAS']['ExitToRecord'] = 'Exit To Record';
$Lang['SDAS']['ExitTo']['FinalChoice'] = "Final Choice"; 
$Lang['SDAS']['ExitTo']['JUPAS'] = "JUPAS";
$Lang['SDAS']['ExitTo']['OtherInstitution'] = "Other Institution";
$Lang['SDAS']['ExitTo']['Employment'] = "Employment";

#### Exit to END ###########



$Lang['SDAS']['StackedPercentageColumn'] = 'Stacked Percentage Column Diagram';
$Lang['SDAS']['StudentDistributionChart'] = 'Student Distribution Chart';
$Lang['SDAS']['SplineDiagram'] = 'Spline Diagram';
$Lang['SDAS']['Reverse'] = 'Reverse';
$Lang['SDAS']['WholeForm'] = 'Whole Form';
$Lang['SDAS']['PositionInForm'] = 'Position In Form';
$Lang['SDAS']['PositionInFormDiff'] = 'Position Difference In Form';
$Lang['SDAS']['Mode'] = 'Mode';
$Lang['SDAS']['NoTeachingClass'] = 'No Teaching Class';
$Lang['SDAS']['Option'] = 'Option';
$Lang['SDAS']['ConvertTo100'] = 'Convert To 100';
$Lang['SDAS']['XAsixOrdering'] = 'XAsix Ordering';
$Lang['SDAS']['Descending'] = 'Descending';
$Lang['SDAS']['Ascending'] = 'Ascending';
$Lang['SDAS']['SEN'] = "SEN";
$Lang['SDAS']['totalStudents'] = "Total Students";
$Lang['SDAS']['averageValue'] = "Average";
$Lang['SDAS']['averageScore'] = "Average Score";
$Lang['SDAS']['passPercent'] = "Pass Percentage";
$Lang['SDAS']['StudentName'] = 'Student Name';
$Lang['SDAS']['Class'] = 'Class';
$Lang['SDAS']['ClassNumber'] = 'Class Number';
$Lang['SDAS']['Subject'] = "Subject";
$Lang['SDAS']['SubjectGroup'] = "Subject Group";
$Lang['SDAS']['Amount'] = "Amount";
$Lang['SDAS']['Score'] = "Score";
$Lang['SDAS']['SchoolYear'] = "Academic Year";
$Lang['SDAS']['Term'] = "Term";
$Lang['SDAS']['JupasAnalysis'] = "JUPAS Analysis";
$Lang['SDAS']['JupasOfferStat'] = "JUPAS Offer Stats";
$Lang['SDAS']['JupasVsDse'] = "JUPAS vs DSE Results ";
$Lang['SDAS']['JupasVsDseAvg'] = "JUPAS vs DSE Average Score";
$Lang['SDAS']['Jupas']['Result'] = "JUPAS Result"; 
$Lang['SDAS']['Jupas']['Institution'] = 'Institution';
$Lang['SDAS']['Jupas']['University'] = 'University';
$Lang['SDAS']['Jupas']['ProgrammeName'] = 'Programme Name';
$Lang['SDAS']['Jupas']['NumOffer'] = 'No. Of Offer';
$Lang['SDAS']['Jupas']['Band'] = 'Band';
$Lang['SDAS']['Jupas']['OfferRound'] = 'Offer round';
$Lang['SDAS']['Jupas']['JUPASNo'] = 'JUPAS Application No.';
$Lang['SDAS']['Jupas']['Degree'] = 'Bachelor\'s Degree Programme';
$Lang['SDAS']['Jupas']['SubDegree'] = 'Sub-Degree Programme';
$Lang['SDAS']['Jupas']['NoOffer'] = 'No Offer';
$Lang['SDAS']['Jupas']['No'] = 'No.';
$Lang['SDAS']['Jupas']['%'] = '%';
$Lang['SDAS']['Jupas']['InstitutionName']['BU'] = 'BU';
$Lang['SDAS']['Jupas']['InstitutionName']['CITY'] = 'City U';
$Lang['SDAS']['Jupas']['InstitutionName']['CU'] = 'CU';
$Lang['SDAS']['Jupas']['InstitutionName']['HKU'] = 'HKU';
$Lang['SDAS']['Jupas']['InstitutionName']['IED'] = 'EdU';
$Lang['SDAS']['Jupas']['InstitutionName']['LU'] = 'Lingnan U';
$Lang['SDAS']['Jupas']['InstitutionName']['OU'] = 'Open U';
$Lang['SDAS']['Jupas']['InstitutionName']['PU'] = 'Poly U';
$Lang['SDAS']['Jupas']['InstitutionName']['UST'] = 'UST';
$Lang['SDAS']['Jupas']['InstitutionName']['SSSDP'] = 'SSSDP';
$Lang['SDAS']['Jupas']['RoundName']['SR'] = 'Subsequent Round';
$Lang['SDAS']['Jupas']['RoundName']['MR'] = 'Main Round';
$Lang['SDAS']['Jupas']['RoundName']['CR'] = 'Clearing Round';
$Lang['SDAS']['Jupas']['JupasVsDseAvg']['Min'] = 'Min';
$Lang['SDAS']['Jupas']['JupasVsDseAvg']['Avg'] = 'Avg';
$Lang['SDAS']['Jupas']['JupasVsDseAvg']['Max'] = 'Max';
$Lang['SDAS']['Jupas']['AllInstitution'] = 'All Institution';
$Lang['SDAS']['Jupas']['All'] = 'Total Number of Application';

// Lang Related to Form_student_Performance
$Lang['SDAS']['Form_student_Performance']['PecentRankingInFormComparsion'] = 'Percentile Ranking Differences In Form';
$Lang['SDAS']['Form_student_Performance']['Compare'] = 'Compare';
$Lang['SDAS']['Form_student_Performance']['Remarks'] = '<span class="red">(&nbsp;)</span> represents ranking decreasing';

// Lang Related to Student_CrossYear_Result
$Lang['SDAS']['Student_CrossYear_Result']['PreS1'] = 'Attainment Test';
$Lang['SDAS']['Student_CrossYear_Result']['MOCK_EXAM'] = 'Mock Exam';
$Lang['SDAS']['Student_CrossYear_Result']['DSE'] = 'HKDSE';
$Lang['SDAS']['Student_CrossYear_Result']['overall_score_diff'] = "Overall Average Score Diff.";

$Lang['SDAS']['DeletedUserLegend'] = '<font style="color:red;">*</font> Representing that the person has been deleted or left already.';

$Lang['SDAS']['Promotion']['ClassPromotionAssessment'] = 'Class Promotion Assessment';
$Lang['SDAS']['Promotion']['ClassPromotionAssessmentConfig'] = 'Class Promotion Assessment Configuration';
$Lang['SDAS']['Promotion']['JuniorAndSeniorFormConfigration'] = 'Junior And Senior Form Configration';
$Lang['SDAS']['Promotion']['Criteria'] = 'Criteria';
$Lang['SDAS']['Promotion']['JuniorFormPromotionCriteria'] = 'Junior form promotion criteria';
$Lang['SDAS']['Promotion']['SeniorFormPromotionCriteria'] = 'Senior form promotion criteria';
$Lang['SDAS']['Promotion']['JuniorForm'] = 'Junior Form';
$Lang['SDAS']['Promotion']['SeniorForm'] = 'Senior Form';
$Lang['SDAS']['Promotion']['Repeat'] = 'Repeat';
$Lang['SDAS']['Promotion']['TryPromote'] = 'Promotion (on discretion) ';
$Lang['SDAS']['Promotion']['Rule'] = 'Rule';
$Lang['SDAS']['Promotion']['Step1'] = 'Step 1';
$Lang['SDAS']['Promotion']['Step2'] = 'Step 2';
$Lang['SDAS']['Promotion']['Mode'] = 'Mode';
$Lang['SDAS']['Promotion']['SimpleScoreMode'] = 'Simple Mode';
$Lang['SDAS']['Promotion']['WeightedAverage'] = 'Weighted Score';
$Lang['SDAS']['Promotion']['CoreAndElectiveSubjectMode'] = 'Core and Elective Subject Mode';
$Lang['SDAS']['Promotion']['CoreSubject'] = 'Core Subject(s)';
$Lang['SDAS']['Promotion']['ElectiveSubject'] = 'Elective Subject(s)';
$Lang['SDAS']['Promotion']['SubjectsThatIsNotCoreAreElectives'] = 'Subjects that is not core are electives';
$Lang['SDAS']['Promotion']['WeightedAverageCEML'] = 'Weighted Average (CEML)';
$Lang['SDAS']['Promotion']['Weighting'] = 'Weighting';
$Lang['SDAS']['Promotion']['Chinese'] = 'Chinese';
$Lang['SDAS']['Promotion']['English'] = 'English';
$Lang['SDAS']['Promotion']['Maths'] = 'Maths';
$Lang['SDAS']['Promotion']['LS'] = 'Liberal Studies';
$Lang['SDAS']['Promotion']['WarningEmpty'] = 'Cannot be empty';
$Lang['SDAS']['Promotion']['FormOverLap'] = 'Form cannot be overlap';
$Lang['SDAS']['Promotion']['Result']['Repeat'] = 'Repeat';
$Lang['SDAS']['Promotion']['Result']['TryPromote'] = 'Promotion (on discretion)';
$Lang['SDAS']['Promotion']['Result']['Pass'] = 'Pass';
$Lang['SDAS']['Promotion']['Stats'] = 'Statistics';
$Lang['SDAS']['Promotion']['SubjectPerformance'] = 'Overall Subject Performance';
$Lang['SDAS']['Promotion']['SubjectStatsType']['average'] = 'Average';
$Lang['SDAS']['Promotion']['SubjectStatsType']['min'] = 'Lowest';
$Lang['SDAS']['Promotion']['SubjectStatsType']['percentile5'] = 'Bottom 5%';
$Lang['SDAS']['Promotion']['SubjectStatsType']['percentile10'] = 'Bottom 10%';
$Lang['SDAS']['Promotion']['SubjectStatsType']['percentile60'] = 'Top 40%';
$Lang['SDAS']['NoRecord']['A'] = 'No record at this moment';
$Lang['SDAS']['FullMarkSetting']['RecalculateSDMEAN'] = 'Recalculate SD Mean/ Passing Rate';
$Lang['SDAS']['FullMarkSetting']['FullMarkNoSet'] = 'Have not set the subject full mark correctly. Please go to: Setting > Subject Full Mark, set the subject full mark correctly.';
$Lang['SDAS']['Import']['HeaderFormatError'] = 'Wrong format';

$Lang['SDAS']['Form_Passing_Rate']['YearTerm']['YearTerm'] = 'Year Term';
$Lang['SDAS']['Form_Passing_Rate']['Form'] = 'Form';
$Lang['SDAS']['Form_Passing_Rate']['AcademicYear'] = 'Academic Year';
$Lang['SDAS']['Form_Passing_Rate']['GroupBy'] = 'Display Mode';
$Lang['SDAS']['Form_Passing_Rate']['Subject'] = 'Subject';
$Lang['SDAS']['Form_Passing_Rate']['Class'] = 'Class';
$Lang['SDAS']['Form_Passing_Rate']['WholeForm'] = 'Whole Form';
$Lang['SDAS']['Form_Passing_Rate']['WholeYear'] = 'Whole Year';
$Lang['SDAS']['Form_Passing_Rate']['Subject_Class'] = 'Subject\ Class';
$Lang['SDAS']['Form_Passing_Rate']['AcademicYear_Form'] = 'Academic Year\Form';
$Lang['SDAS']['Form_Passing_Rate']['OverallResult'] = 'Overall Result';
$Lang['SDAS']['Form_Passing_Rate']['Warning']['PleaseSelectForm'] = 'Please Select Form!';
$Lang['SDAS']['Form_Passing_Rate']['Warning']['PleaseSelectAcademicYear'] = 'Please Select Academic Year';
$Lang['SDAS']['Form_Passing_Rate']['YearTerm']['YearTermNo'] = 'Term##';
$Lang['SDAS']['Form_Passing_Rate']['Remark']['1'] = 'Remark: Number in Quotes Represents Number of Pass';

// ### Learn and Teach START ####
$Lang['SDAS']['LearnAndTeach']['TeacherReport'] = 'Teacher Report';
$Lang['SDAS']['LearnAndTeach']['SubjectReport'] = 'Subject Report';
$Lang['SDAS']['LearnAndTeach']['ScoreRemark'] = 'Completely agree(5), Agree(4), Neutral(3)，Disagree(2)，Strongly disagree(1)';
$Lang['SDAS']['LearnAndTeach']['TotalAverage'] = 'Total average';
$Lang['SDAS']['LearnAndTeach']['OverallTeacherMin'] = 'Min of all teacher';
$Lang['SDAS']['LearnAndTeach']['OverallTeacherAvg'] = 'Average of all teacher';
$Lang['SDAS']['LearnAndTeach']['OverallTeacherMax'] = 'Max of all teacher';
$Lang['SDAS']['LearnAndTeach']['OverallTeacherSD'] = 'SD of all teacher';
$Lang['SDAS']['LearnAndTeach']['OverallSubjectMin'] = 'Min of this subject';
$Lang['SDAS']['LearnAndTeach']['OverallSubjectAvg'] = 'Average of this subject';
$Lang['SDAS']['LearnAndTeach']['OverallSubjectMax'] = 'Max of this subject';
$Lang['SDAS']['LearnAndTeach']['OverallSubjectSD'] = 'SD of this subject';
$Lang['SDAS']['LearnAndTeach']['ListByClass'] = 'By Classes';
$Lang['SDAS']['LearnAndTeach']['ListByStdAns'] = 'By Number of replies';
// ### Learn and Teach END ####
$Lang['SDAS']['class_subject_performance_summary']['Subject'] = 'Subject';
$Lang['SDAS']['class_subject_performance_summary']['Score'] = 'Score';
$Lang['SDAS']['class_subject_performance_summary']['FullMark'] = 'Full Marks';
$Lang['SDAS']['class_subject_performance_summary']['ScoreStatistic'] = 'Score Statistic';
$Lang['SDAS']['class_subject_performance_summary']['convertTo100'] = 'Convert To Percentage Score';
$Lang['SDAS']['class_subject_performance_summary']['convertTo100_Remarks'] = 'Subject Full Mark must be set, if full mark is not 100.';
$Lang['SDAS']['class_subject_performance_summary']['DataTable'] = "Data Table";
$Lang['SDAS']['class_subject_performance_summary']['DataChart'] = "Data Chart";
$Lang['SDAS']['class_subject_performance_summary']['DataTableInfo'] = "Data Table Info";
$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['HighChartMode'] = 'HighChart Display Mode';
$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['column'] = 'Column';
$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['box'] = 'Box';
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Min'] = 'Minimun';
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Q1'] = 'Lower Quartile';
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Median'] = 'Median';
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Q3'] = 'Upper Quartile';
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Max'] = 'Maximun';

$Lang['SDAS']['MonitoringGroupSetting']['MonitoringGroupSetting'] = 'Monitoring Group Setting';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameCH'] = 'Group Name(Chinese)';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameEN'] = 'Group Name(English)';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['DateModified'] = 'Date Modified';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['PIC'] = 'Person In Charge';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['AcademicYear'] = 'Academic Year';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['Member'] = 'Member';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['ChineseName'] = 'Chinese Name';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['EnglishName'] = 'English Name';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['ClassName'] = 'Class Name';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['ClassNumber'] = 'Class Number';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['Copy'] = 'Copy';
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['CopyMember'] = 'Copy Member';
$Lang['SDAS']['MonitoringGroupSetting']['NoResult'] = 'No Result';
$Lang['SDAS']['MonitoringGroupSetting']['NoMember'] = 'No Member';
$Lang['SDAS']['MonitoringGroupSetting']['UpdateSuccess'] = '1|=|Record Updated.';
$Lang['SDAS']['MonitoringGroupSetting']['UpdateUnsuccess'] = '0|=|Record Update Failed.';
$Lang['SDAS']['MonitoringGroupSetting']['EditGroup'] = 'Edit Group';
$Lang['SDAS']['MonitoringGroupSetting']['NewGroup'] = 'New Group';
$Lang['SDAS']['MonitoringGroupSetting']['AddMember'] = 'Add Member';
$Lang['SDAS']['MonitoringGroupSetting']['AllYear'] = '--All Year--';
$Lang['SDAS']['MonitoringGroupSetting']['PleaseSelect'] = '--Please Select-';
$Lang['SDAS']['MonitoringGroupSetting']['GroupList'] = 'Group List';
$Lang['SDAS']['MonitoringGroupSetting']['CopyGroupToOtherYear'] = 'Copy Group To Other Year';
$Lang['SDAS']['MonitoringGroupSetting']['DeleteConfirm'] = 'Are you sure to delete?';
$Lang['SDAS']['MonitoringGroupSetting']['PleaseSelectToDelete'] = 'Please Select Item To Delete';
$Lang['SDAS']['MonitoringGroupSetting']['PleaseFillInGroupNameCH'] = 'Please Fill In Chinese Group Name';
$Lang['SDAS']['MonitoringGroupSetting']['PleaseFillInGroupNameEN'] = 'Please Fill In English Group Name';
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['HaveSameGroup'] = 'There are same groups in target year';
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopySuccess'] = 'copied successfully';
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopySuccessWRecord'] = 'successfully copied <--Count--> record(s)';
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopyFail'] = 'copy fail';
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['Yes'] = 'Yes';
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['No'] = 'No';

$Lang['SDAS']['FormStudentPerformance']['Remark']['PercentileLabel'] = 'Label for percentile rank';
$Lang['SDAS']['FormStudentPerformance']['Remark']['RankingLabel'] = 'Label for position differences';
$Lang['SDAS']['FormStudentPerformance']['Remark']['LightBlue'] = '<span style="background-color:#D2F6F8;">&nbsp;&nbsp;&nbsp;&nbsp;</span> represents percentile ranking decrease for 30 or above';
$Lang['SDAS']['FormStudentPerformance']['Remark']['LightYellow'] = '<span style="background-color:#FEEE80;">&nbsp;&nbsp;&nbsp;&nbsp;</span> represents percentile ranking increase for 20 or above';
$Lang['SDAS']['FormStudentPerformance']['Remark']['Yellow'] = '<span style="background-color:yellow;">&nbsp;&nbsp;&nbsp;&nbsp;</span> represents first percentile ranking group';
$Lang['SDAS']['FormStudentPerformance']['Remark']['Orange'] = '<span style="background-color:orange;">&nbsp;&nbsp;&nbsp;&nbsp;</span> represents last percentile ranking group';

// ### Value-added Data Report START ####
$Lang['SDAS']['ValueAddedData']['Tab']['ClassValueAdded'] = 'Class Value-added';
$Lang['SDAS']['ValueAddedData']['Tab']['TeacherValueAdded'] = 'Teacher Value-added';
$Lang['SDAS']['ValueAddedData']['ValueAdded'] = 'Value-added';
// ### Value-added Data Report END ####

// ### DSE vs GPA Report START ####
$Lang['SDAS']['DseVsGpa']['graphTitle'] = "DSE vs GPA";
$Lang['SDAS']['DseVsGpa']['gpaResult'] = "GPA";
$Lang['SDAS']['DseVsGpa']['highChanceJupasOffer'] = "High chance of JUPAS offer";
$Lang['SDAS']['DseVsGpa']['dseBest5'] = "DSE Best 5 / 4C+2X";
$Lang['SDAS']['DseVsGpa']['jupasBaseLine'] = "JUPAS OFFER BASELINE";
$Lang['SDAS']['DseVsGpa']['universityBaseLink'] = "HKU/UST/CUHK BASELINE";
$Lang['SDAS']['DseVsGpa']['unexpectedResult'] = "Unexpected result";
$Lang['SDAS']['DseVsGpa']['slipped'] = "Slipped";
$Lang['SDAS']['DseVsGpa']['studentResult'] = "Students' results";
$Lang['SDAS']['DseVsGpa']['schoolResult'] = "School result";
$Lang['SDAS']['DseVsGpa']['dseResult'] = "HKDSE result";
// ### DSE vs GPA Report END ####

// ### Predict Degree-Diploma START ####
$Lang['SDAS']['PredictDegreeDiploma']['DseScoreAfterAdj'] = "DSE score after adj.";
$Lang['SDAS']['PredictDegreeDiploma']['MeetRequirement'] = "Meet Requirement";
$Lang['SDAS']['PredictDegreeDiploma']['ChiFocus'] = "Chi Focus";
$Lang['SDAS']['PredictDegreeDiploma']['EngFocus'] = "Eng Focus";
$Lang['SDAS']['PredictDegreeDiploma']['MathFocus'] = "Math Focus";
$Lang['SDAS']['PredictDegreeDiploma']['LsFocus'] = "LS Focus";
$Lang['SDAS']['PredictDegreeDiploma']['ChiEngFail'] = "Chi & Eng Fail";
$Lang['SDAS']['PredictDegreeDiploma']['ElectiveFail'] = "Elective Fail";
$Lang['SDAS']['PredictDegreeDiploma']['Degree'] = "Degree";
$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] = "Higher Diploma";

$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Chi'] = "Chi";
$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Eng'] = "Eng";
$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Math'] = "Math";
$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Ls'] = "LS";
$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Elective'] = "Ele";
// #### Predict Degree-Diploma END ####


##### pre s1 analysis report ###
$Lang['SDAS']['PreS1Analysis']['SubjectAverage'] = "Subject Average";


// ### Report Comparsion START ####
$Lang['SDAS']['ReportComparison']['Assessment'] = "Assessment";
$Lang['SDAS']['ReportComparison']['AssessmentAry']['CA'] = "CA";
$Lang['SDAS']['ReportComparison']['AssessmentAry']['Exam'] = "Exam Grade";
$Lang['SDAS']['ReportComparison']['AssessmentAry']['YearGrade'] = "Year Grade";
$Lang['SDAS']['ReportComparison']['Compare'] = "Compare";
$Lang['SDAS']['ReportComparison']['Percentile'] = 'Percentile';
$Lang['SDAS']['ReportComparison']['Term1'] = "Term 1";
$Lang['SDAS']['ReportComparison']['Term2'] = "Term 2";

$Lang['SDAS']['ReportComparison']['TableHeader']['Class'] = 'Class';
$Lang['SDAS']['ReportComparison']['TableHeader']['ClassNo'] = 'Class No.';
$Lang['SDAS']['ReportComparison']['TableHeader']['EnglishName'] = 'English Name';
$Lang['SDAS']['ReportComparison']['TableHeader']['ChineseName'] = 'Chinses Name';
$Lang['SDAS']['ReportComparison']['TableHeader']['NickName'] = 'Nickname';
$Lang['SDAS']['ReportComparison']['TableHeader']['Subject'] = 'Subjects';
$Lang['SDAS']['ReportComparison']['TableHeader']['DataInYear'] = '<!--DataType--> of <!--AssessmentType--> in <!--Year-->';
$Lang['SDAS']['ReportComparison']['TableHeader']['ChangeInYear'] = 'Change in <!--DataType--> of <!--AssessmentType-->';

$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectSubject'] = "Please select subject(s).";
$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectSubjectGroup'] = "Please select subject group(s).";
$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelect2Years'] = "Please select at least 2 school year(s).";
$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectAssessment'] = "Please select assessment(s).";
$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectCompareTarget'] = "Please select compare target(s).";
// #### Report Comparsion END ####

$Lang['SDAS']['Settings']['AccessRight']['PIC'] = 'Principal/PIC (access to all reports)';
$Lang['SDAS']['Settings']['AccessRight']['SubjectPanel'] = 'Subject Panel';
$Lang['SDAS']['Settings']['AccessRight']['MonthlyReport'] = 'Monthly Report PIC';
$Lang['SDAS']['Settings']['AccessRight']['MonthlyReportSection'] = "Monthly Report Section PIC";

$Lang['SDAS']['Settings']['AccessGroup']['Title'] = "Access Group";
$Lang['SDAS']['Settings']['AccessGroup']['Name_ch'] = "Group Name (Chinese)";
$Lang['SDAS']['Settings']['AccessGroup']['Name_en'] = "Group Name (English)";
$Lang['SDAS']['Settings']['AccessGroup']['Description'] = "Description";
$Lang['SDAS']['Settings']['AccessGroup']['MemberCount'] = "Member count";
$Lang['SDAS']['Settings']['AccessGroup']['Member'] = "Group Member";

### Class Subject Comparison START ###

$Lang['SDAS']['ClassSubjectComparsion']['ClassWarning'] = "Please select at least 1 class";
$Lang['SDAS']['ClassSubjectComparsion']['SubjectWarning'] = "Please select at least 1 subject";

### Class Subject Comparison END ###

### SSPA START ###
// $Lang['SDAS']['SSPA']['Title'] = "Secondary School Places Allocation";
// $Lang['SDAS']['SSPA']['ParticipateAllocation'] = "Participate Allocation";
// $Lang['SDAS']['SSPA']['PlacesEMI'] = "Allocates to EMI";
// $Lang['SDAS']['SSPA']['PlacesFirstChoice'] = "Allocates to 1st Choice";
// $Lang['SDAS']['SSPA']['PlacesFirsttoThirdChoice'] = "Allocates to 1st-3rd Choice";
// $Lang['SDAS']['SSPA']['Net'] = "Net";
// $Lang['SDAS']['SSPA']['Territory'] = "Territory";
// $Lang['SDAS']['SSPA']['TotalCount'] = "Total Count";
// $Lang['SDAS']['SSPA']['Count'] = "Count";

### SSPA END ###