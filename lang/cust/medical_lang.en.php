<?php
//updating by :         
//please place the value in alphea order
$Lang['medical']['General']['All'] = 'ALL';

$Lang['medical']['menu']['message'] = 'Medical Notification';
$Lang['medical']['menu']['writeMessage'] = 'Write Message';
$Lang['medical']['menu']['readMessage'] = 'Receive Message';
$Lang['medical']['menu']['sendMessage'] = 'Send Message';

$Lang['medical']['menu']['management'] = 'Management';
$Lang['medical']['menu']['meal'] = 'Meal Record';
$Lang['medical']['menu']['bowel'] = 'Bowel Movement';
$Lang['medical']['menu']['studentLog'] = 'Student Log';
$Lang['medical']['menu']['studentSleep'] = 'Sleep Status';
$Lang['medical']['menu']['problemRecord'] = 'Wrong Record';
$Lang['medical']['menu']['input'] = 'Input';
$Lang['medical']['menu']['event'] = 'Behaviour and Event';
$Lang['medical']['menu']['case'] = 'Case Follow-up';
$Lang['medical']['menu']['award'] = 'Awarding Scheme';
$Lang['medical']['menu']['revisit'] = 'Revisit';
$Lang['medical']['menu']['staffEvent'] = 'Staff Medical Record';
$Lang['medical']['menu']['handover'] = 'Administration Handover';

$Lang['medical']['menu']['settings'] = 'Settings';
$Lang['medical']['menu']['settings_accessRight'] = 'Access Rights';
$Lang['medical']['menu']['settings_meal'] = 'Meal Settings';
$Lang['medical']['menu']['settings_studentlog'] = 'Student Log Settings';
$Lang['medical']['menu']['settings_studentsleep'] = 'Sleep Status Settings';
$Lang['medical']['menu']['settings_bowel'] = 'Bowel Movement Settings';
$Lang['medical']['menu']['settings_defaultRemarks'] = 'Default Remarks Settings';
$Lang['medical']['menu']['settings_noticeRemarks'] = 'Medical Notification Default Settings';
$Lang['medical']['menu']['settings_groupFilter'] = 'Group Filter Settings';
$Lang['medical']['menu']['settings_event_type'] = 'Event Type Settings';
$Lang['medical']['menu']['settings_staff_event_type'] = 'Staff Event Type Settings';
$Lang['medical']['menu']['settings_system_setting'] = 'System Settings';
$Lang['medical']['menu']['settings_allowed_ip'] = 'Allowed access IP';
$Lang['medical']['menu']['settings_handover'] = 'Administration Handover Settings';

$Lang['medical']['menu']['report'] = 'Reports';
$Lang['medical']['menu']['report_meal'] = 'Meal Reports';
$Lang['medical']['menu']['report_bowel'] = 'Bowel Reports';
$Lang['medical']['menu']['report_convulsion'] = 'Student Log Reports';
$Lang['medical']['menu']['report_sleep']= 'Sleep Condition Reports';
$Lang['medical']['menu']['report_student_event'] = 'Behaviour and Event Reports';
$Lang['medical']['menu']['report_staff_event'] = 'Staff Medical Record Reports';
$Lang['medical']['menu']['report_revisit'] = "Student Medical Record Reports";
$Lang['medical']['menu']['report_handover'] = "Administration Handover Reports";

$Lang['medical']['module'] = 'Medical Caring System';

// message option
$Lang['medical']['message']['sender'] = 'Sender';
$Lang['medical']['message']['receiver'] = 'Receiver';
$Lang['medical']['message']['sendContent'] = 'Message';
$Lang['medical']['message']['content'] = 'Content';
$Lang['medical']['message']['date'] = 'Date';
$Lang['medical']['message']['defaultHint'] = ' -- Default Hint -- ';
$Lang['medical']['message']['AlertMessage']['FillReceiver'] = 'Please select receiver.';
$Lang['medical']['message']['AlertMessage']['FillMessage'] = 'Please fill in message.';
$Lang['medical']['message']['ReturnMessage']['Delete']['Success'] = '1|=|Mail deleted。';
$Lang['medical']['message']['ReturnMessage']['Delete']['Failed'] = '0|=|Mail delete fail。';

$Lang['medical']['message']['setting']['title'] = 'Title';
$Lang['medical']['message']['setting']['message'] = 'Message';
$Lang['medical']['message']['setting']['recordStatus'] = 'RecordStatus';
$Lang['medical']['message']['setting']['StatusOption']['InUse'] = 'In use';
$Lang['medical']['message']['setting']['StatusOption']['Suspend'] = 'Suspend';

$Lang['medical']['message_setting']['ReturnMessage']['Add']['Success'] = '1|=|Message Setting Added.';
$Lang['medical']['message_setting']['ReturnMessage']['Add']['Failed'] = '0|=|Message Setting Addition Failed.';
$Lang['medical']['message_setting']['ReturnMessage']['Edit']['Success'] = '1|=|Message Setting Updated.';
$Lang['medical']['message_setting']['ReturnMessage']['Edit']['Failed'] = '0|=|Message Setting Update Failed.';
$Lang['medical']['message_setting']['ReturnMessage']['Delete']['Success'] = '1|=|Message Setting Deleted.';
$Lang['medical']['message_setting']['ReturnMessage']['Delete']['Failed'] = '0|=|Message Setting Deletion Failed.';
$Lang['medical']['message_setting']['ReturnMessage']['Activate']['Success'] = '1|=|Message Setting activated.';
$Lang['medical']['message_setting']['ReturnMessage']['Activate']['Failed'] = '0|=|Message Setting Activation Failed.';
$Lang['medical']['message_setting']['ReturnMessage']['Suspend']['Success'] = '1|=|Message Setting Suspended.';
$Lang['medical']['message_setting']['ReturnMessage']['Suspend']['Failed'] = '0|=|Message Setting Suspension Failed.';

$Lang['medical']['message']['thickbox']['showOnlyCanReceiveUser'] = '<font color="red">*</font>It only shows the users that can receive message.';


// meal option
$Lang['medical']['meal']['searchMenu']['date'] = 'Date'; 
//$Lang['medical']['meal']['searchMenu']['timePeriod'] = 'Time Period'; 
$Lang['medical']['meal']['searchMenu']['form'] = 'Class Name'; 
$Lang['medical']['meal']['search']['ClassAll'] = 'All Classes'; 
$Lang['medical']['meal']['searchMenu']['group'] = 'Group';
$Lang['medical']['meal']['searchMenu']['sleep'] = 'Stay/ Not Stay';
$Lang['medical']['meal']['search']['sleepOption'][] = $Lang['medical']['General']['All'];
$Lang['medical']['meal']['search']['sleepOption'][] = 'Stay';
$Lang['medical']['meal']['search']['sleepOption'][] = 'Not Stay';
$Lang['medical']['meal']['searchMenu']['gender'] = 'Gender';
$Lang['medical']['meal']['searchMenu']['timePeriod'] = 'Time Slot'; 
$Lang['medical']['meal']['search']['timePeriodOption'][] = 'Breakfast'; 
$Lang['medical']['meal']['search']['timePeriodOption'][] = 'Lunch'; 
$Lang['medical']['meal']['search']['timePeriodOption'][] = 'Dinner';



$Lang['medical']['meal']['tableHeader']['Number'] = '#';
$Lang['medical']['meal']['tableHeader']['Add'] = '&nbsp;';
$Lang['medical']['meal']['tableHeader']['ClassName'] = 'Class Name';
$Lang['medical']['meal']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['meal']['tableHeader']['StayOrNot'] = 'Stay/ Not Stay';
$Lang['medical']['meal']['tableHeader']['AttendanceStatus'] = 'Attendance Status';
$Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Present'] = 'Present';
$Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Absent'] = 'Absent';
$Lang['medical']['meal']['tableHeader']['EatingCondition'] = 'Eating Condition';
$Lang['medical']['meal']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['meal']['tableHeader']['LastPersonConfirmed'] = 'Last Person Updated';
$Lang['medical']['meal']['tableHeader']['LastUpdated'] = 'Last Modified By';

$Lang['medical']['meal']['AttendanceStatusExplain'] = 'Remarks：<br />
	1) Breakfast：You can insert records, if the student was back to school yesterday afternoon.<br />
	2) Lunch：You can insert records, if the student was back to school this morning.<br />
	3) Dinner：You can insert records, if the student was back to school this afternoon.<br />
	<font color="red">**</font>) Student attendance has been modified to absent';

$Lang['medical']['meal']['tableContent'][] = 'Edit Remarks';

$Lang['medical']['meal']['button']['save'] = 'Save';
$Lang['medical']['meal']['button']['reset'] = 'Reset';
$Lang['medical']['meal']['button']['cancel'] = 'Cancel';
$Lang['medical']['meal']['button']['search'] = 'View';

// meal report
$Lang['medical']['report_meal']['report1'] = 'Meal report';
$Lang['medical']['report_meal']['report2'] = 'Meal records';

$Lang['medical']['report_meal']['searchMenu']['date'] = 'Date';
$Lang['medical']['report_meal']['searchMenu']['form'] = 'Class Name';
$Lang['medical']['report_meal']['searchMenu']['sleep'] = 'Stay/ Not Stay';
$Lang['medical']['report_meal']['search']['sleepOption'][] = $Lang['medical']['General']['All'];
$Lang['medical']['report_meal']['search']['sleepOption'][] = 'Stay';
$Lang['medical']['report_meal']['search']['sleepOption'][] = 'Not Stay';


$Lang['medical']['report_meal']['sleepOption']['StayIn'] = 'Stay';
$Lang['medical']['report_meal']['sleepOption']['StayOut'] = 'Not Stay';


$Lang['medical']['report_meal']['searchMenu']['gender'] = 'Gender';
$Lang['medical']['report_meal']['search']['genderOption']['all'] = $Lang['medical']['General']['All'];
$Lang['medical']['report_meal']['search']['genderOption']['M'] = 'Male';
$Lang['medical']['report_meal']['search']['genderOption']['F'] = 'Female';
$Lang['medical']['report_meal']['searchMenu']['timePeriod'] = 'Time Slot';
$Lang['medical']['report_meal']['search']['timePeriodOption'][] = $Lang['medical']['General']['All'];
$Lang['medical']['report_meal']['search']['timePeriodOption'][] = 'Breakfast'; 
$Lang['medical']['report_meal']['search']['timePeriodOption'][] = 'Lunch'; 
$Lang['medical']['report_meal']['search']['timePeriodOption'][] = 'Dinner';
$Lang['medical']['report_meal']['searchMenu']['studentName'] = 'Student Name';
$Lang['medical']['report_meal']['searchMenu']['item'] = 'Item';
$Lang['medical']['report_meal']['search']['itemCheckbox']['all'] = $Lang['medical']['General']['All'];
$Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'] = 'Remarks';
$Lang['medical']['report_meal']['searchMenu']['reportType'] = 'Report Type';
$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealReport'] = 'Meal Report';
$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealStatistics'] = 'Meal Statistics';
$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealReport2'] = 'Meal Records'; 

$Lang['medical']['report_meal']['tableHeader']['name'] = 'Name';
$Lang['medical']['report_meal']['tableHeader']['totalDayOfPresent'] = 'Total Attendance';
$Lang['medical']['report_meal']['tableHeader']['remarks'] = 'Remarks';
$Lang['medical']['report_meal']['tableSubHeader']['noOfMeal'] = 'Meal(s)';
$Lang['medical']['report_meal']['tableSubHeader']['ratio'] = 'Ratio';
$Lang['medical']['report_meal']['remarks'] = 'The attendance record includes those meal days ONLY.';

$Lang['medical']['report_meal2']['tableHeader']['class'] = 'Class';
$Lang['medical']['report_meal2']['tableHeader']['name'] = 'Name';
$Lang['medical']['report_meal2']['tableHeader']['sleep'] = 'Stay/ Not Stay';
$Lang['medical']['report_meal2']['tableHeader']['timePeroid'] = 'Time Slot';

$Lang['medical']['report_meal']['btn']['print'] = 'Print';
$Lang['medical']['report_meal']['btn']['export'] = 'Export';

$Lang['medical']['report_meal']['searchMonthLimited'] = 'The search date cannot be more then three month';

// access right
$Lang['medical']['accessRightView']['userType'] = 'User Type';
$Lang['medical']['accessRightView']['noOfUsers'] = 'No. of Users';

$Lang['medical']['accessRight']['selectTitle']['setUserGroup'] = 'User Group';
$Lang['medical']['accessRight']['selectTitle']['setUsers']='User Name List';

$Lang['medical']['accessRight']['header']['option']='- Option -';

$Lang['medical']['accessRight']['selectTitle']['MEAL']='Meal Daily Log Access Right';
$Lang['medical']['accessRight']['selectTitle']['BOWEL']='Bowel Daily Log Access Right';
$Lang['medical']['accessRight']['selectTitle']['STUDENTLOG']='Student Daily Log Access Right';
$Lang['medical']['accessRight']['selectTitle']['SLEEP']=' Sleeping Condition Access Right';
$Lang['medical']['accessRight']['selectTitle']['NOTICE']=' Medical Notification Access Right';
$Lang['medical']['accessRight']['selectTitle']['EVENT']='Behaviour and Event Access Right';
$Lang['medical']['accessRight']['selectTitle']['CASE']='Case Follow-up Access Right';
$Lang['medical']['accessRight']['selectTitle']['AWARDSCHEME']='Awarding Scheme Access Right';
$Lang['medical']['accessRight']['selectTitle']['DEFAULTREMARK']='Default Remarks Access Right';
$Lang['medical']['accessRight']['selectTitle']['REVISIT']='Revisit Access Right';
$Lang['medical']['accessRight']['selectTitle']['STAFFEVENT']='Staff Medical Record Access Right';
$Lang['medical']['accessRight']['selectTitle']['HANDOVER']='Administration Handover Access Right';

$Lang['medical']['accessRight']['Option']['DeleteSelfRecord'] = 'Delete record created by myself only';
$Lang['medical']['accessRight']['Option']['Management'] = 'Management';
$Lang['medical']['accessRight']['Option']['Report'] = 'Reports';
$Lang['medical']['accessRight']['Option']['Settings'] = 'Settings';
$Lang['medical']['accessRight']['Option']['NursingHint'] = 'Medical Notification';
$Lang['medical']['accessRight']['Option']['Send'] = 'Write';
$Lang['medical']['accessRight']['Option']['Receive'] = 'Receive';
$Lang['medical']['accessRight']['Option']['View'] = 'View';
$Lang['medical']['accessRight']['Option']['Add'] = 'Add';
$Lang['medical']['accessRight']['Option']['Edit'] = 'Edit';
$Lang['medical']['accessRight']['Option']['Delete'] = 'Delete';
$Lang['medical']['accessRight']['Option']['Self'] = 'Self';
$Lang['medical']['accessRight']['Option']['All'] = 'All';
$Lang['medical']['accessRight']['Option']['ViewAll'] = 'View All Records';
$Lang['medical']['accessRight']['Option']['Print'] = 'Print';
$Lang['medical']['accessRight']['Option']['Export'] = 'Export';

// meal setting
$Lang['medical']['meal_setting']['button']['new_category'] = 'New Category';

$Lang['medical']['meal_setting']['tableHeader']['Category'] = 'Category';
$Lang['medical']['meal_setting']['tableHeader']['Code'] = 'Code';
$Lang['medical']['meal_setting']['tableHeader']['Status'] = 'Status';
$Lang['medical']['meal_setting']['tableHeader']['Default'] = 'Default';

$Lang['medical']['meal_setting']['tableSorting'] = 'Note: Records are sort by Code';

$Lang['medical']['meal_setting']['Name'] = 'Name';
$Lang['medical']['meal_setting']['Code'] = 'Code';
$Lang['medical']['meal_setting']['Status'] = 'Status';
$Lang['medical']['meal_setting']['Default'] = 'Default';
$Lang['medical']['meal_setting']['StatusOption']['InUse'] = 'In use';
$Lang['medical']['meal_setting']['StatusOption']['Suspend'] = 'Suspend';

$Lang['medical']['meal_setting']['AlertMessage']['FillText'] = 'Please fill the field.';
$Lang['medical']['meal_setting']['AlertMessage']['SelectOption'] = 'Please select the options';
$Lang['medical']['meal_setting']['AlertMessage']['Activate'] = 'Are you sure to activate selected items?';

$Lang['medical']['meal_setting']['RemindMessage']['DefaultHasSet'] = 'Default value have been set.';

$Lang['medical']['meal_setting']['ReturnMessage']['Add']['Success'] = '1|=|Meal Setting Added.';
$Lang['medical']['meal_setting']['ReturnMessage']['Add']['Failed'] = '0|=|Meal Setting Addition Failed.';
$Lang['medical']['meal_setting']['ReturnMessage']['Edit']['Success'] = '1|=|Meal Setting Updated.';
$Lang['medical']['meal_setting']['ReturnMessage']['Edit']['Failed'] = '0|=|Meal Setting Update Failed.';
$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['Success'] = '1|=|Meal Setting Deleted.';
$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['Failed'] = '0|=|Meal Setting Deletion Failed.';
$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['FailedWithReason'] = '0|=|Meal Setting Deletion Failed because it is used.';
$Lang['medical']['meal_setting']['ReturnMessage']['Activate']['Success'] = '1|=|Meal Setting activated.';
$Lang['medical']['meal_setting']['ReturnMessage']['Activate']['Failed'] = '0|=|Meal Setting Activation Failed.';
$Lang['medical']['meal_setting']['ReturnMessage']['Suspend']['Success'] = '1|=|Meal Setting Suspended.';
$Lang['medical']['meal_setting']['ReturnMessage']['Suspend']['Failed'] = '0|=|Meal Setting Suspension Failed.';

$Lang['medical']['general_setting']['GeneralSetting'] = 'General Setting';

// bowel setting
$Lang['medical']['report_studentBowel']['report1'] = 'Line Graph Report';
$Lang['medical']['report_studentBowel']['report1_thickBox']['title'] = 'Self Bowel Report';
$Lang['medical']['report_studentBowel']['report2'] = 'Bowel Report';
$Lang['medical']['report_studentBowel']['reportOption'] = 'No. of Students';
$Lang['medical']['report_studentBowel']['option']['single'] = 'Single';
$Lang['medical']['report_studentBowel']['option']['group'] = 'Group';

$Lang['medical']['report_studentBowel']['searchMenu']['timePeriodHint'] = 'Note: Time slot interval must be set more than <!--Time--> minutes in order to reflect a line in the graph.';
$Lang['medical']['report_studentBowel']['timePeriodWrong'] = 'At least select 1 hour for time period.';

$Lang['medical']['bowel_setting']['BarCode'] = 'Barcode';
$Lang['medical']['bowel']['BowelStatusBarCode'] = 'Bowel Condition Barcode';
$Lang['medical']['bowel']['duplicateRecord'] = 'Record cannot duplicate';
$Lang['medical']['bowel']['enter'] = 'Enter';
$Lang['medical']['bowel']['InvalidBarCode'] = 'Cannot find this student';
$Lang['medical']['bowel']['StudentBarCode'] = 'Student Barcode';
$Lang['medical']['bowel']['tab']['Bowel'] = 'Bowel Movement';
$Lang['medical']['bowel']['tab']['Barcode'] = 'Input by barcode';

$Lang['medical']['bowel']['tableHeader']['Number'] = '#';
$Lang['medical']['bowel']['tableHeader']['ClassName'] = 'Class';
$Lang['medical']['bowel']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['bowel']['tableHeader']['StayOrNot'] = 'Stay or not';
$Lang['medical']['bowel']['tableHeader']['AttendanceStatus'] = 'Attendance Status';
$Lang['medical']['bowel']['tableHeaderOption']['AttendanceOption']['Present'] = 'Present';
$Lang['medical']['bowel']['tableHeaderOption']['AttendanceOption']['Absent'] = 'Absent';
$Lang['medical']['bowel']['tableHeader']['BowelCondition'] = 'Bowel Condition';
$Lang['medical']['bowel']['tableHeader']['BowelTime'] = 'Bowel Time';
$Lang['medical']['bowel']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['bowel']['tableHeader']['LastPersonConfirmed'] = 'Last Updated By';
$Lang['medical']['bowel']['tableHeader']['LastUpdated'] = 'Last Updated';
$Lang['medical']['bowel']['tableHeader']['del'] = '';
$Lang['medical']['bowel']['tableHeader']['Add'] = '';
$Lang['medical']['bowel']['tableHeader']['Detail'] = 'Detail';
$Lang['medical']['bowel']['AttendanceAbsentAlert'] = 'You can insert records, if the student was back to school.';
$Lang['medical']['bowel']['AttendanceInputExplain'] = '*) You can insert records, if the student was back to school.';
$Lang['medical']['bowel']['AttendanceStatusExplain'] = 'Remarks：<br />
*) If students attended in the morning or afteroon, the attendance status will be displayed as "Present"';
$Lang['medical']['bowel']['bowelStatusBarcodeNotFound'] = 'Bowel condition barcode not found';
$Lang['medical']['bowel']['studentBarcodeNotFound'] = 'Student barcode not found';
$Lang['medical']['bowel']['warning']['SelectBowelStatus'] = 'Please select bowel condition';
$Lang['medical']['bowel']['warning']['InputBowelStatus'] = 'Please input bowel condtion barcode';
$Lang['medical']['bowel']['warning']['InputStudentBarcode'] = 'Please input student barcode';
$Lang['medical']['bowel_setting']['AlertMessage']['Activate'] = 'Are you sure to activate?';
$Lang['medical']['bowel_setting']['RemindMessage']['DefaultHasSet'] = 'Default value have been set.';

$Lang['medical']['bowel_setting']['ReturnMessage']['Add']['Success'] = '1|=|Bowel Setting Added.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Add']['Failed'] = '0|=|Bowel Setting Addition Failed.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Edit']['Success'] = '1|=|Bowel Setting Updated.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Edit']['Failed'] = '0|=|Bowel Setting Update Failed.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Delete']['Success'] = '1|=|Bowel Setting Deleted.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Delete']['Failed'] = '0|=|Bowel Setting Deletion Failed.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Delete']['FailedWithReason'] = '0|=|Bowel Setting Deletion Failed because it is used.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Activate']['Success'] = '1|=|Bowel Setting activated.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Activate']['Failed'] = '0|=|Bowel Setting Activation Failed.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Suspend']['Success'] = '1|=|Bowel Setting Suspended.';
$Lang['medical']['bowel_setting']['ReturnMessage']['Suspend']['Failed'] = '0|=|Bowel Setting Suspension Failed.';

$Lang['medical']['bowel']['importRemarks']['barCode'] = 'Student Bar-code / Bowel Bar-code';
$Lang['medical']['bowel']['importRemarks']['nullColumn'] = '(Can leave blank)';
$Lang['medical']['bowel']['importRemarks']['recordDateTime'] = 'Record Date Time (YY/MM/DD HH:MM)';

$Lang['medical']['bowel']['import']['invalidBarCode'] = 'Invalid Student ID';
$Lang['medical']['bowel']['import']['invalidAttendance'] = 'Student had not back school';
$Lang['medical']['bowel']['import']['invalidRecordBarCode'] = 'Invalid Bowel Barcode';
$Lang['medical']['bowel']['import']['invalidDateTime'] = 'Invalid Date/Time';
$Lang['medical']['bowel']['import']['duplicateRecord'] = 'Duplicate Record in same file';
$Lang['medical']['bowel']['import']['duplicateRecordBelow'] = 'These record(s) are duplicate record, and will not import to database!';

$Lang['medical']['studentBowel']['importRemarks']['StudentBarcode'] = 'Student Barcode';
$Lang['medical']['studentBowel']['importRemarks']['BowelBarcode'] = 'Bowel Barcode';
$Lang['medical']['studentBowel']['importRemarks']['DateTime'] = 'Record Date Time (YYYY-MM-DD HH:mm)';
$Lang['medical']['studentBowel']['importRemarks']['RecordDate'] = 'Record Date (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)';
$Lang['medical']['studentBowel']['importRemarks']['RecordTime'] = 'Record Time (HH:MM)';
$Lang['medical']['studentBowel']['importTableHeader']['RecordDate'] = 'Record Date';
$Lang['medical']['studentBowel']['importTableHeader']['RecordTime'] = 'Record Time';
$Lang['medical']['studentBowel']['importError']['barcodeNotExist'] = 'Student Barcode not exist';
$Lang['medical']['studentBowel']['importError']['bowelBarcodeNotExist'] = 'Invalid Bowel Barcode';
$Lang['medical']['studentBowel']['importError']['duplicateRecord'] = 'Duplicate record to existing database';

$Lang['medical']['general']['BowelData'] = 'Bowel Data';

$Lang['medical']['bowel']['importRemarksTable']['studentID'] = 'Student Barcode';
$Lang['medical']['bowel']['importRemarksTable']['studentName'] = 'Student Name';
$Lang['medical']['bowel']['importRemarksTable']['class'] = 'Class';
$Lang['medical']['bowel']['importRemarksTable']['classNo'] = 'Class Number';
$Lang['medical']['bowel']['importRemarksTable']['bowelName'] = 'Bowel Type';
$Lang['medical']['bowel']['importRemarksTable']['bowelDateTime'] = 'System Date/Time';
$Lang['medical']['bowel']['importRemarksTable']['bowelDateTimeOriginal'] = 'Bowel Date/Time';

$Lang['medical']['report_bowel']['tableHeader']['timeVerusPeople'] = 'Time Vs People';
$Lang['medical']['report_bowel']['noStudentSelected'] = 'Please select at least one student.';
$Lang['medical']['report_bowel']['noReasonSelected'] = 'Please select at least one item.';
$Lang['medical']['report_bowel']['wrongDatePeriod'] = 'End date cannot be earlier then the start date';
$Lang['medical']['report_bowel']['wrongTimePeriod'] = 'End time period cannot be earlier then the start time';
## General Settings
$Lang['medical']['general']['button']['new_category'] = 'New Category';
$Lang['medical']['general']['tableHeader']['Category'] = 'Category';
$Lang['medical']['general']['tableHeader']['Default'] = 'Default';
$Lang['medical']['general']['tableHeader']['Subcategory'] = 'Subcategory';

$Lang['medical']['general']['tableSorting'] = 'Note: Records are sort by Code';

$Lang['medical']['general']['Default'] = 'Default';
$Lang['medical']['general']['StatusOption']['InUse'] = 'In use';
$Lang['medical']['general']['StatusOption']['Suspend'] = 'Suspend';

$Lang['medical']['general']['AlertMessage']['FillText'] = 'Please fill the field.';
$Lang['medical']['general']['AlertMessage']['SelectOption'] = 'Please select the options';
$Lang['medical']['general']['AlertMessage']['Activate'] = 'Are you sure to activate selected items?';
$Lang['medical']['general']['AlertMessage']['MaxAttachmentError'] ='Please Check the File Size.(Size: 6MB)';
$Lang['medical']['general']['AlertMessage']['noAccessRight'] = 'You do not have access right to this part.';
$Lang['medical']['general']['AlertMessage']['NoLevel4SelectedError'] ='Please Check At Least One Body Part(s).';
$Lang['medical']['general']['Hint']['MaxAttachment'] = 'An event can have 6 attachments ONLY.';

$Lang['medical']['general']['RemindMessage']['DefaultHasSet'] = 'Other record has set as default value';

$Lang['medical']['general']['ReturnMessage']['Delete']['FailedWithReason'] = '0|=|Deletion Failed because it is used.';
$Lang['medical']['general']['ReturnMessage']['Activate']['Success'] = '1|=|Item activated.';
$Lang['medical']['general']['ReturnMessage']['Activate']['Failed'] = '0|=|Activation Failed.';
$Lang['medical']['general']['ReturnMessage']['Suspend']['Success'] = '1|=|Item Suspended.';
$Lang['medical']['general']['ReturnMessage']['Suspend']['Failed'] = '0|=|Suspension Failed.';


// studentlog 
$Lang['medical']['studentLog']['button']['saveAll'] = 'Save all';
$Lang['medical']['studentLog']['button']['back'] = 'Back';
$Lang['medical']['studentlog_setting']['tableHeader']['ItemQty'] = 'Item Quantity';
$Lang['medical']['studentlog_setting']['convulsionRemark'] = 'Convulsant Parts and Syndrome';
$Lang['medical']['studentlog_setting']['convulsion']['syndrome'] = 'Syndrome';

$Lang['medical']['studentLog']['className'] = 'Class Name';
$Lang['medical']['studentLog']['classNumber'] = 'Class Number';
$Lang['medical']['studentLog']['eventDate'] = 'Date of Event';
$Lang['medical']['studentLog']['time'] = 'Time';
$Lang['medical']['studentLog']['studentLog'] = 'Log Type';
$Lang['medical']['studentLog']['case'] = 'Item';
$Lang['medical']['studentLog']['bodyParts'] = 'Body Part(s)';
$Lang['medical']['studentLog']['bodyParts1'] = 'Body Parts 1 Code';
$Lang['medical']['studentLog']['bodyParts2'] = 'Body Parts 2 Code';
$Lang['medical']['studentLog']['pic'] = 'PIC';

$Lang['medical']['studentLog']['button']['importBtn'] = 'Import Data';
$Lang['medical']['studentLog']['eventInfo'] = '- Event Information -';

$Lang['medical']['studentLog']['behaviour'] = 'Behaviour';
$Lang['medical']['studentLog']['behaviour1'] = 'Log Type Code';
$Lang['medical']['studentLog']['behaviour2'] = 'Item Code';
$Lang['medical']['studentLog']['timelasted'] = 'Duration';

$Lang['medical']['studentLog']['addBodyParts'] = 'Add Body Part';
$Lang['medical']['studentLog']['addEvent'] = 'Add Event';
$Lang['medical']['studentLog']['addAttachment'] = 'Add Attachment';
$Lang['medical']['studentLog']['attachmentRemarks'] = '( The max. size of each attachment is 6MB. At Most 6 Attachment per Event)';

$Lang['medical']['studentLog']['deleteEvent'] = 'Delete Event';
$Lang['medical']['studentLog']['LinkRelevantEvent'] = 'Link relevant behaviour and event';
$Lang['medical']['studentLog']['newEventDesc']='New Event';
$Lang['medical']['studentLog']['oldEventDesc']='Old Event';
$Lang['medical']['studentLog']['RelevantEvent'] = 'Relevant behaviour and event';
$Lang['medical']['studentLog']['RemoveStudentLog'] = 'Remove relevant student log';

$Lang['medical']['studentLog']['importRemarks']['className'] = 'Class Name';
$Lang['medical']['studentLog']['importRemarks']['classNum'] = 'Class Number';
$Lang['medical']['studentLog']['importRemarks']['date'] = 'Date (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)';
$Lang['medical']['studentLog']['importRemarks']['time'] = 'Time (HH:MM)';
$Lang['medical']['studentLog']['importRemarks']['behaviour1'] = 'Log Type Code';
$Lang['medical']['studentLog']['importRemarks']['behaviour2'] = 'Item Code';
$Lang['medical']['studentLog']['importRemarks']['parts'] = 'Body Parts';
$Lang['medical']['studentLog']['importRemarks']['duration'] = 'Duration (MM:SS)';
$Lang['medical']['studentLog']['importRemarks']['remarks'] = 'Remarks';
$Lang['medical']['studentLog']['importRemarks']['pic'] = 'PIC';

$Lang['medical']['studentLog']['import']['invalidAttendance'] = 'Student had not back school at night';
$Lang['medical']['studentLog']['importTableHeader']['date'] = 'Date';
$Lang['medical']['studentLog']['importTableHeader']['time'] = 'Time';
$Lang['medical']['studentLog']['importTableHeader']['duration'] = 'Duration';

$Lang['medical']['studentLog']['import']['dataChecking'] = 'Data Checking';
$Lang['medical']['studentLog']['import']['reason'] = 'Reason';
$Lang['medical']['studentLog']['import']['invalidClassName'] = 'Invalid Class Name';
$Lang['medical']['studentLog']['import']['invalidClassNum'] = 'Invalid Class Num';
$Lang['medical']['studentLog']['import']['invalidDate'] = 'Invalid Date';
$Lang['medical']['studentLog']['import']['invalidTime'] = 'Invalid Time';
$Lang['medical']['studentLog']['import']['emptyBehaviour1'] = 'Empty Log Type Code';
$Lang['medical']['studentLog']['import']['invalidBehaviour1'] = 'Invalid Log Type Code';
$Lang['medical']['studentLog']['import']['emptyBehaviour2'] = 'Empty Item Code';
$Lang['medical']['studentLog']['import']['invalidBehaviour2'] = 'Invalid Item Code';
$Lang['medical']['studentLog']['import']['emptyParts'] = 'Empty Body Parts';
$Lang['medical']['studentLog']['import']['invalidParts'] = 'Invalid Body Parts';
$Lang['medical']['studentLog']['import']['invalidDuration'] = 'Invalid Duration';
$Lang['medical']['studentLog']['import']['emptyPIC'] = 'Empty PIC';
$Lang['medical']['studentLog']['import']['invalidPIC'] = 'Invalid PIC';

$Lang['medical']['studentLog']['hour'] = 'h';
$Lang['medical']['studentLog']['minute'] = 'm';
$Lang['medical']['studentLog']['second'] = 's';

$Lang['medical']['studentLog']['report_convulsion']['reportName'] = 'Student Convulsion Records';
$Lang['medical']['studentLog']['report_convulsion']['reportName2'] = 'Student Log';

$Lang['medical']['studentLog']['addItemNoLev2'] = 'No record Item';

$Lang['medical']['studentLog']['AttendanceStatusExplain'] = 'Remarks：<br />
*) If students attended in the morning or afteroon, the attendance status will be displayed as "Present"';

$Lang['medical']['studentLog']['AttendanceInputExplain'] = '*) You can insert records, if the student was back to school.';

$Lang['medical']['studentLog_general']['deleteEvent'] = 'Are you sure to delete the Event?';
$Lang['medical']['studentLog']['email']['subject'] = 'Student Log change notification';
$Lang['medical']['studentLog']['email2pic'] = 'Email to person-in-charge';
$Lang['medical']['studentLog']['recordDuplicate'] = 'Record duplicate';
// student sleep
$Lang['medical']['sleep']['searchMenu']['date'] = 'Date'; 
$Lang['medical']['sleep']['searchMenu']['form'] = 'Class Name'; 
$Lang['medical']['sleep']['search']['ClassAll'] = 'All Classes'; 
$Lang['medical']['sleep']['searchMenu']['group'] = 'Group'; 
$Lang['medical']['sleep']['searchMenu']['sleep'] = 'Stay/ Not Stay';
$Lang['medical']['sleep']['search']['sleepOption'][] = $Lang['medical']['General']['All'];
$Lang['medical']['sleep']['search']['sleepOption'][] = 'Stay';
$Lang['medical']['sleep']['search']['sleepOption'][] = 'Not Stay';
$Lang['medical']['sleep']['searchMenu']['gender'] = 'Gender'; 
$Lang['medical']['sleep']['search']['genderOption'][] = 'All'; 
$Lang['medical']['sleep']['search']['genderOption'][] = 'Male'; 
$Lang['medical']['sleep']['search']['genderOption'][] = 'Female'; 
$Lang['medical']['sleep']['toNextDay'] = '&nbsp;To&nbsp;';

$Lang['medical']['sleep']['tableHeader']['Number'] = '#';
$Lang['medical']['sleep']['tableHeader']['NewItem'] = '';
$Lang['medical']['sleep']['tableHeader']['ClassName'] = 'Class';
$Lang['medical']['sleep']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['sleep']['tableHeader']['StayOrNot'] = 'Stay/ Not Stay';
$Lang['medical']['sleep']['tableHeader']['AttendanceStatus'] = 'Attendance Status';
$Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Present'] = 'Present';
$Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent'] = 'Absent';
$Lang['medical']['sleep']['tableHeader']['SleepCondition'] = 'Sleep Condition';
$Lang['medical']['sleep']['tableHeader']['Reason'] = 'Reason';
$Lang['medical']['sleep']['tableHeader']['Frequency'] = 'Frequency';
$Lang['medical']['sleep']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['sleep']['tableHeader']['inputBy'] = 'Input By';
$Lang['medical']['sleep']['tableHeader']['LastPersonConfirmed'] = 'Last Person Updated';
$Lang['medical']['sleep']['tableHeader']['LastUpdated'] = 'Last Modified By';
$Lang['medical']['sleep']['tableHeader']['DelItem'] = '';

$Lang['medical']['sleep']['AttendanceStatusExplain'] = 'Remarks：<br />
*) You can insert records, if the student was back to school this afternoon.<br />
<font color="red">**</font>) Student attendance has been modified to absent';

$Lang['medical']['sleep_setting']['tableHeader']['DefaultLev2'] = 'Default Item';

$Lang['medical']['report_sleep']['tableHeader']['stayForDays'] = 'Total Number of Stay within that period';
$Lang['medical']['report_sleep']['tableHeader']['normal'] = 'Normal Sleeping Days';
$Lang['medical']['report_sleep']['tableHeader']['abnormal'] = 'Abnormal Sleeping Days';
$Lang['medical']['report_sleep']['tableHeader']['sleepWell']  = 'Sleep Well';
$Lang['medical']['report_sleep']['tableHeader']['healthProblem'] = 'Health Problem';
$Lang['medical']['report_sleep']['tableHeader']['emotionProblem'] = 'Emotion Problem';
$Lang['medical']['report_sleep']['tableHeader']['otherProblem'] = 'Other Problem';
$Lang['medical']['report_sleep']['tableContent']['frequency'] = 'time(s)';

$Lang['medical']['report_sleep']['reminder1']= '註:睡眠報告不會計算補充字樣的項目';
$Lang['medical']['report_sleep']['reminder2']= '註:睡眠統計只會計算包含整晚安睡字樣的項目為正常睡眠，包含健康問題、情緒行為問題、其他問題的項目為不正常睡眠';
$Lang['medical']['report_sleep']['reminder3']= '註:睡眠統計只會計算包含整晚安睡字樣的項目為正常睡眠，包含健康問題、情緒行為問題、其他問題的項目為不正常睡眠';

$Lang['medical']['studentsleep_setting']['tableHeader']['ItemQty'] = 'Item Quantity';

$Lang['medical']['studentSleep']['importRemarks']['className'] = 'Class Name';
$Lang['medical']['studentSleep']['importRemarks']['classNum'] = 'Class Num';
$Lang['medical']['studentSleep']['importRemarks']['date'] = 'Date (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)';
$Lang['medical']['studentSleep']['importRemarks']['SleepCondition'] = 'Sleep Condition';
$Lang['medical']['studentSleep']['importRemarks']['reason'] = 'Reason Code';
$Lang['medical']['studentSleep']['importRemarks']['frequency'] = 'Frequency';
$Lang['medical']['studentSleep']['importRemarks']['remarks'] = 'Remarks';

$Lang['medical']['studentSleep']['import']['invalidStatus'] = 'Invalid Status Code';
$Lang['medical']['studentSleep']['import']['invalidReason'] = 'Invalid Reason';
$Lang['medical']['studentSleep']['import']['invalidFrequency'] = 'Invalid Frequence';

$Lang['medical']['studentLog']['tableHeader']['Item'] = 'Item Name';
// report
$Lang['medical']['report_general']['searchMenu']['date'] = 'Date';
$Lang['medical']['report_general']['searchMenu']['form'] = 'Class Name';
$Lang['medical']['report_general']['searchMenu']['sleep'] = 'Stay/ Not Stay';
$Lang['medical']['report_general']['searchMenu']['gender'] = 'Gender';
$Lang['medical']['report_general']['searchMenu']['studentName'] = 'Student Name';
$Lang['medical']['report_general']['searchMenu']['item'] = 'Item';

$Lang['medical']['report_general']['search']['itemCheckbox']['remarks'] = 'Remarks';

$Lang['medical']['report_general']['search']['genderOption']['all'] = $Lang['medical']['General']['All'];
$Lang['medical']['report_general']['search']['genderOption']['M'] = 'Male';
$Lang['medical']['report_general']['search']['genderOption']['F'] = 'Female';

$Lang['medical']['report_general']['deleteEvent'] = 'Are you sure to delete the Event(s)?';
$Lang['medical']['report_general']['deleteFile'] = 'Are you sure to delete the file(s)?';
$Lang['medical']['report_general']['attachmentTitle'] = 'List of Attachment';

$Lang['medical']['report_studentlog']['tableHeader']['date'] = 'Date';
$Lang['medical']['report_studentlog']['tableHeader']['time'] = 'Time';
$Lang['medical']['report_studentlog']['tableHeader']['duration'] = 'Duration';
$Lang['medical']['report_studentlog']['tableHeader']['remarks'] = 'Remarks';
$Lang['medical']['report_studentlog']['subTotal'] = 'Sub Total';
$Lang['medical']['report_studentlog']['grandTotal'] = 'Grand Total';

$Lang['medical']['report_studentlog2']['tableHeader']['type'] = 'Type';

$Lang['medical']['report_general']['tableHeader']['studentName'] = 'Student Name';
$Lang['medical']['report_general']['tableHeader']['time'] = 'Time';
$Lang['medical']['report_general']['tableHeader']['item'] = 'Item';
$Lang['medical']['report_general']['tableHeader']['remarks'] = 'Remarks';
$Lang['medical']['report_general']['tableHeader']['lastUpdatedBy'] = 'Last Modified By';
$Lang['medical']['report_general']['tableHeader']['lastUpdatedOn'] = 'Last Modified';

$Lang['medical']['report_general']['PeriodStatus'][1] = 'Morning';
$Lang['medical']['report_general']['PeriodStatus'][2] = 'Noon';
$Lang['medical']['report_general']['PeriodStatus'][3] = 'Night';

$Lang['medical']['ReturnMessage']['ImportPartiallySuccess'] = '1|=|Record Partially Import successfully';

$Lang['medical']['report_studentLog']['reportTitle'] = '學生抽搐紀錄表';
$Lang['medical']['report_studentLog']['address'][] = '香港新界將軍澳安達臣道三○一號';
$Lang['medical']['report_studentLog']['address'][] = '電話: (852) 2703 1722　　傳真: (852) 2703 6320';
$Lang['medical']['report_studentLog']['address'][] = '電郵: info@sunnyside.edu.hk　網址: www.sunnyside.edu.hk';
$Lang['medical']['report_studentLog']['printPerson'] = '紀錄表列印';
$Lang['medical']['report_studentLog']['printDate'] = '列印日期';
$Lang['medical']['report_studentLog']['printStudent'] = '學生姓名';
$Lang['medical']['report_studentLog']['printRecordDate'] = '日期';

$Lang['medical']['report_studentSleep']['report1'] = 'Sleep Report';
$Lang['medical']['report_studentSleep']['report2'] = 'Sleep Statistics';
$Lang['medical']['report_studentSleep']['report3'] = 'Sleep Record';

$Lang['medical']['studentSleep']['tableHeader']['Reason'] = 'Reason Name';

##### Problem Record START #####
$Lang['medical']['problemRecord']['searchMenu']['date'] = 'Date';
$Lang['medical']['problemRecord']['searchMenu']['type'] = 'Type';


$Lang['medical']['problemRecord']['sleep']['tableHeader']['Number'] = '#';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['ClassName'] = 'Class Name';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['Date'] = 'Date';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['SleepCondition'] = 'Sleep Condition';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['Reason'] = 'Reason';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['Frequency'] = 'Frequency';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['LastPersonConfirmed'] = 'Last Person Updated';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['LastUpdated'] = 'Last Modified By';

$Lang['medical']['problemRecord']['meal']['tableHeader']['Number'] = '#';
$Lang['medical']['problemRecord']['meal']['tableHeader']['ClassName'] = 'Class Name';
$Lang['medical']['problemRecord']['meal']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['problemRecord']['meal']['tableHeader']['Date'] = 'Date';
$Lang['medical']['problemRecord']['meal']['tableHeader']['TimePeriod'] = 'Time Period';
$Lang['medical']['problemRecord']['meal']['tableHeader']['EatingCondition'] = 'Eating Condition';
$Lang['medical']['problemRecord']['meal']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['problemRecord']['meal']['tableHeader']['LastPersonConfirmed'] = 'Last Person Updated';
$Lang['medical']['problemRecord']['meal']['tableHeader']['LastUpdated'] = 'Last Modified By';

$Lang['medical']['problemRecord']['bowel']['tableHeader']['Number'] = '#';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['ClassName'] = 'Class Name';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['Date'] = 'Date';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['BowelCondition'] = 'Bowel Condition';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['Time'] = 'Bowel Time';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['LastPersonConfirmed'] = 'Last Person Updated';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['LastUpdated'] = 'Last Modified By';

$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Number'] = '#';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['ClassName'] = 'Class Name';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Date'] = 'Date';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Time'] = 'Time';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['StudentLogCondition'] = 'Log Type';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Item'] = 'Item';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['LastPersonConfirmed'] = 'Last Person Updated';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['LastUpdated'] = 'Last Modified By';

$Lang['medical']['general']['dateModified'] = '	Last Modified By';
$Lang['medical']['general']['deleteCategoryContainsItem'] = 'Category contains items';
$Lang['medical']['general']['duplicateBarCode'] = 'Bar code cannot duplicate.';
$Lang['medical']['general']['duplicateCategory'] = $Lang['medical']['general']['tableHeader']['Category'] . ' cannot duplicate.';
$Lang['medical']['general']['duplicateCode'] = $Lang['General']['Code'] . ' cannot duplicate.';
$Lang['medical']['general']['edit'] = 'Edit Record';
$Lang['medical']['general']['form'] = 'Class Name';
$Lang['medical']['general']['ImportData'] = 'Import Data';
$Lang['medical']['general']['inputBy'] = 'Input By';
$Lang['medical']['general']['lastModifiedBy'] = 'Last Person Updated';
$Lang['medical']['general']['new'] = 'Add New Record';
$Lang['medical']['general']['parent'] = 'Parent';
$Lang['medical']['general']['pic'] = 'PIC';
$Lang['medical']['general']['report']['total'] = 'Total';
$Lang['medical']['general']['sleepOption']['StayIn'] = 'Stay';
$Lang['medical']['general']['sleepOption']['StayOut'] = 'Not Stay';
$Lang['medical']['general']['sleepType'] = 'Stay/ Not Stay';
$Lang['medical']['general']['student'] = 'Student';
$Lang['medical']['general']['StudentLogData'] = 'Student Log Data';
$Lang['medical']['general']['StudentSleepData'] = 'Student Sleep Data';
$Lang['medical']['general']['update'] = 'Update';

$Lang['medical']['bowel']['parent']['month'] = 'Month';
$Lang['medical']['bowel']['parent']['bowelInfo'] = 'Defecation Info';
$Lang['medical']['bowel']['parent']['bowelDate'] = 'Defecation Date';
$Lang['medical']['bowel']['parent']['new'] = 'New Defecation Record';
$Lang['medical']['bowel']['parent']['duplicateRecord'] = 'Record cannot duplicate';

$Lang['medical']['bowel']['parent']['tableHeader']['Number'] = '#';
$Lang['medical']['bowel']['parent']['tableHeader']['Date'] = 'Date';
$Lang['medical']['bowel']['parent']['tableHeader']['AttendanceStatus'] = 'Attendance';
$Lang['medical']['bowel']['parent']['tableHeader']['BowelTime'] = 'Time of Defecation';
$Lang['medical']['bowel']['parent']['tableHeader']['BowelCondition'] = 'Defecation';
$Lang['medical']['bowel']['parent']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['bowel']['parent']['tableHeader']['LastUpdatedBy'] = 'Last Person Updated';
$Lang['medical']['bowel']['parent']['tableHeader']['LastUpdatedOn'] = 'Last Modified By';

$Lang['medical']['bowel']['parent']['AttendanceOption']['Present'] = 'Present';
$Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'] = 'Absent(Stay at Home)';
$Lang['medical']['bowel']['parent']['AttendanceOption']['Absent'] = 'Absent';


$Lang['medical']['studentLog']['parent']['month'] = 'Month';
$Lang['medical']['studentLog']['parent']['bowelInfo'] = 'Student Log Info';
$Lang['medical']['studentLog']['parent']['bowelDate'] = 'Student Log Date';

$Lang['medical']['studentLog']['parent']['tableHeader']['Number'] = '#';
$Lang['medical']['studentLog']['parent']['tableHeader']['Date'] = 'Date';
$Lang['medical']['studentLog']['parent']['tableHeader']['AttendanceStatus'] = 'Attendance';
$Lang['medical']['studentLog']['parent']['tableHeader']['Time'] = 'Time';
$Lang['medical']['studentLog']['parent']['tableHeader']['LogCondition'] = 'Log Type';
$Lang['medical']['studentLog']['parent']['tableHeader']['Item'] = 'Item';
$Lang['medical']['studentLog']['parent']['tableHeader']['Remarks'] = 'Remarks';
$Lang['medical']['studentLog']['parent']['tableHeader']['LastUpdatedBy'] = 'Last Person Updated';
$Lang['medical']['studentLog']['parent']['tableHeader']['LastUpdatedOn'] = 'Last Modified By';

$Lang['medical']['general']['allStudent'] = ' - All Student - ';
$Lang['medical']['general']['datFileFormat'] = '(.dat File)';
$Lang['medical']['general']['displayMethod']['title'] = 'Display method';
$Lang['medical']['general']['displayMethod']['orderBy']['desc'] = 'By date descending';
$Lang['medical']['general']['displayMethod']['orderBy']['asc'] = 'By date ascending';
$Lang['medical']['general']['displayMethod']['displayEmptyDate'] = 'Display no record day';
$Lang['medical']['general']['displayMethod']['displayEmptyStudent'] = 'Display no record student';
$Lang['medical']['general']['duplicateRecordBelow'] = 'Duplicate record';
$Lang['medical']['general']['error']['ajax'] = "Error on AJAX call!";
$Lang['medical']['general']['group'] = 'Group';
$Lang['medical']['general']['ImportDATArr']['ImportStepArr']['SelectDATFile'] = "Select DAT file";
$Lang['medical']['general']['ImportDATArr']['ImportStepArr']['DATConfirmation'] = "Confirmation";
$Lang['medical']['general']['ImportDATArr']['ImportStepArr']['ImportResult'] = "Imported Result";
$Lang['medical']['general']['importHint']['clickToCheck'] = 'Click here to check ';
$Lang['medical']['general']['linkRelevantStaffEvent'] = 'Link Relevant Staff Event';
$Lang['medical']['general']['linkRelevantStudentLog'] = 'Link Relevant Student Log';
$Lang['medical']['general']['relevantStaffEvent'] = 'Relevant Staff Medical Record';
$Lang['medical']['general']['relevantStudentLog'] = 'Relevant Student Log';
$Lang['medical']['general']['ReturnMessage']['NotAllowDeletingNonSelfRecord'] = "0|=|Not allow deleting other people\'s record";
$Lang['medical']['general']['SaveAllConfirm'] = 'Are you sure to save all edited record?';
$Lang['medical']['general']['viewPic'] = 'View PIC';
$Lang['medical']['general']['Warning']['NotAllowToDeleteNonSelfRecord'] = 'Not allow to delete other people\'s record';
$Lang['medical']['general']['WarnMsg']['PleaseSelectDAT'] = "Please select DAT(.dat) file";
$Lang['medical']['general']['Warning']['StartDateGtEndDate'] = 'Start Date cannot be greater than End Date';

$Lang['medical']['default_remarks_setting']['module'] = 'Module';
$Lang['medical']['default_remarks_setting']['defaultRemarks'] = 'Default Remarks';
$Lang['medical']['default_remarks_setting']['defaultRemarksHint'] = 'One line for a default remarks';
$Lang['medical']['default_remarks_setting']['ReturnMessage']['Update']['Success'] = '1|=|Default Remarks update success.';
$Lang['medical']['default_remarks_setting']['ReturnMessage']['Update']['Failed'] = '0|=|Default Remarks update fail.';

$Lang['medical']['group_filter_setting']['category'] = 'Category';
$Lang['medical']['group_filter_setting']['deselectedCategory'] = 'Group Category';
$Lang['medical']['group_filter_setting']['selectedCategory'] = 'Selected Category';
$Lang['medical']['group_filter_setting']['hint'] = 'Remarks: <br />
*) Categories have been renamed.';
$Lang['medical']['group_filter_setting']['ReturnMessage']['Update']['Success'] = '1|=|Group Filter update success.';
$Lang['medical']['group_filter_setting']['ReturnMessage']['Update']['Failed'] = '0|=|Group Filter update fail.';

$Lang['medical']['App']['ChooseStudent'] = 'Choose Student';
$Lang['medical']['App']['CopyDefaultRemarks'] = 'Copy default remarks';
$Lang['medical']['App']['Misc'] = 'Misc';
$Lang['medical']['App']['NoRecordFound'] = "No record found";
$Lang['medical']['App']['PleaseSearch'] = 'Please search';
$Lang['medical']['App']['PleaseSearchStudent'] = 'Please search students.';
$Lang['medical']['App']['PleaseSelect'] = "Please Select";
$Lang['medical']['App']['PleaseSelectSyndrome'] = 'Please select syndrome';
$Lang['medical']['App']['Remark'] = 'Remarks';
$Lang['medical']['App']['SelectedPICs'] = 'Selected PICs';
$Lang['medical']['App']['SearchPICsUsingName'] = 'Search by User Name';
$Lang['medical']['App']['SearchStudentUsingClass'] = 'Search by Class & Class No. (E.G. 1A15)';
$Lang['medical']['App']['SearchStudentUsingName'] = 'Search by Student Name';
$Lang['medical']['App']['SelectedStudent'] = 'Selected Student(s)';

$Lang['medical']['award']['AddSuggestion'] = 'Add Suggestion';
$Lang['medical']['award']['AllGroupPIC'] = ' - All Group PIC - ';
$Lang['medical']['award']['AllStatus'] = ' - All Status - ';
$Lang['medical']['award']['ClassOrientedSuggestion'] = 'Class Oriented Suggestion';
$Lang['medical']['award']['EditPerformance'] = 'Edit Performance';
$Lang['medical']['award']['Group'] = 'Target Oriented Group';
$Lang['medical']['award']['GroupPIC'] = 'Group PIC';
$Lang['medical']['award']['performance']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['award']['Performance'] = 'Performance in Target Oriented Group';
$Lang['medical']['award']['RemoveTarget'] = 'Remove Target';
$Lang['medical']['award']['RemoveTargetSuggestion'] = 'Remove Class Oriented Suggestion';
$Lang['medical']['award']['Scheme'] = 'Scheme';
$Lang['medical']['award']['SchemeName'] = 'Name';
$Lang['medical']['award']['Score'] = 'Score';
$Lang['medical']['award']['ShowAllDate'] = 'Show all dates';
$Lang['medical']['award']['ShowRecentDate'] = 'Show recent dates only';
$Lang['medical']['award']['status']['Completed'] = 'Completed';
$Lang['medical']['award']['status']['Processing'] = 'Processing';
$Lang['medical']['award']['tab']['Scheme'] = 'Awarding Scheme';
$Lang['medical']['award']['tab']['Performance'] = 'Student Performance';
$Lang['medical']['award']['Target'] = 'Target';
$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedDelete'] = 'This change will delete present performance record, are you sure you would like to change the date range?';
$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedFillBack'] = 'This change need to fill back record in earlier dates, are you sure you would like to change the date range?';
$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedFillBackDelete'] = 'This change will delete present performance record and need to fill back record in earlier dates, are you sure you would like to change the date range?';
$Lang['medical']['award']['Warning']['ConfirmDeleteGroup'] = 'Are you sure you would like to delete this Target Oriented Group?';
$Lang['medical']['award']['Warning']['ConfirmDeleteSchemeTarget'] = 'Are you sure you would like to delete this Class Oriented Suggestion?';
$Lang['medical']['award']['Warning']['ConfirmDeleteStudentTarget'] = 'Are you sure you would like to delete this target and score (if any)?';
$Lang['medical']['award']['Warning']['InputInteger'] = 'Please input integer between 0 and 100';
$Lang['medical']['award']['Warning']['InputSuggestion'] = 'Please input target';
$Lang['medical']['award']['Warning']['PerformanceRecordExist'] = 'Do not allow removing the group as there is performance record.';
$Lang['medical']['award']['Warning']['SelectGroupPIC'] = 'Please select group PIC';
$Lang['medical']['award']['Warning']['SelectGroup'] = 'Please select Target Oriented Group';
$Lang['medical']['case']['AddMeetingMinute'] = 'Add Meeting Minutes';
$Lang['medical']['case']['AllReporter'] = ' - All Reporter - ';
$Lang['medical']['case']['AllStatus'] = ' - All Status - ';
$Lang['medical']['case']['AllPIC'] = ' - All Case Follow-up PIC - ';
$Lang['medical']['case']['ClosedRemark'] = 'Closed Case Remark';
$Lang['medical']['case']['Detail'] = 'Case Details';
$Lang['medical']['case']['EditMinutes'] = 'Edit Meeting Minutes';
$Lang['medical']['case']['LinkRelevantEvent'] = 'Link Relevant Events';
$Lang['medical']['case']['meeting']['Date'] = 'Meeting Date';
$Lang['medical']['case']['meeting']['Record'] = 'Record';
$Lang['medical']['case']['Minutes'] = 'Meeting Minutes';
$Lang['medical']['case']['NewMinutes'] = 'New Meeting Minutes';
$Lang['medical']['case']['RelevantEvent'] = 'Relevant Events';
$Lang['medical']['case']['RemoveEvent'] = 'Remvoe Relevant Event';
$Lang['medical']['case']['RemoveMinutes'] = 'Remove Meeting Minutes';
$Lang['medical']['case']['SelectEvent'] = 'Select Relevant Event';
$Lang['medical']['case']['status']['Closed'] = 'Closed';
$Lang['medical']['case']['status']['Processing'] = 'Processing';
$Lang['medical']['case']['StudentName'] = 'Student Name';
$Lang['medical']['case']['tableHeader']['CaseNo'] = 'Case No';
$Lang['medical']['case']['tableHeader']['CasePIC'] = 'Case PIC';
$Lang['medical']['case']['tableHeader']['StartDate'] = 'Start Date';
$Lang['medical']['case']['tableHeader']['Status'] = 'Status';
$Lang['medical']['case']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['case']['tableHeader']['Summary'] = 'Case Summary';
$Lang['medical']['case']['ViewEvent'] = 'View Relevant Event';
$Lang['medical']['case']['Warning']['ConfirmDeleteCaseEvent'] = 'Are you sure you would like to delete this relevant event?';
$Lang['medical']['case']['Warning']['ConfirmDeleteMeetingMinutes'] = 'Are you sure you would like to delete this meeting minutes?';
$Lang['medical']['case']['Warning']['DuplicateEvent'] = 'At least one event is duplicate to current selection, please try again.';
$Lang['medical']['case']['Warning']['InputMeetingDate'] = 'Please input meeting date';
$Lang['medical']['case']['Warning']['InvalidDateEarly'] = 'Selected date should not be earlier than start date.';
$Lang['medical']['case']['Warning']['SelectCasePIC'] = 'Please select case PIC';
$Lang['medical']['case']['Warning']['SelectStudent'] = 'Please select student';
$Lang['medical']['event']['AffectedPerson'] = 'Target / Affected Person';
$Lang['medical']['event']['AllAffectedPerson'] = ' - All affected person - ';
$Lang['medical']['event']['AllCategory'] = ' - All Category - ';
$Lang['medical']['event']['AllClass'] = ' - All Class - ';
$Lang['medical']['event']['AllGroup'] = ' - All Group - ';
$Lang['medical']['event']['AllStudent'] = ' - All Student - ';
$Lang['medical']['event']['AllTeacher'] = ' - All Teacher - ';
$Lang['medical']['event']['Attachment'] = 'Attachment';
$Lang['medical']['event']['CaseNo'] = 'Case No';
$Lang['medical']['event']['Cause'] = 'Cause';
$Lang['medical']['event']['EditFollowupAction'] = 'Edit Follow-up Action';
$Lang['medical']['event']['EndTime'] = 'End Time';
$Lang['medical']['event']['EventDate'] = 'Date';
$Lang['medical']['event']['EventType'] = 'Event Type';
$Lang['medical']['event']['FollowupAction'] = 'Follow-up Action';
$Lang['medical']['event']['FollowupActionInfo'] = 'Follow-up by %s on %s';
$Lang['medical']['event']['Handling'] = 'Handling';
$Lang['medical']['event']['HideFollowupAction'] = 'Hide Follow-up Action';
$Lang['medical']['event']['NewCase'] = 'New Case';
$Lang['medical']['event']['NewFollowup'] = 'New Follow-up';
$Lang['medical']['event']['NonAccountHolder'] = 'Non-Account Holder, please use comma to separate';
$Lang['medical']['event']['Other'] = 'Other';
$Lang['medical']['event']['Place'] = 'Place';
$Lang['medical']['event']['RelevantCase'] = 'Relevant Case';
$Lang['medical']['event']['RelevantCaseNo'] = 'Not Require';
$Lang['medical']['event']['RelevantCaseYes'] = 'Require';
$Lang['medical']['event']['RemoveCase'] = 'Remvoe Relevant Case';
$Lang['medical']['event']['RemoveStaffEvent'] = 'Remove Relevant Staff Event linkage';
$Lang['medical']['event']['ReturnMessage']['EventTypeNotSetup'] = "0|=|Event type has not setup";
$Lang['medical']['event']['SelectCase'] = 'Select Case';
$Lang['medical']['event']['SelectFile'] = 'Select File(s)';
$Lang['medical']['event']['SelectStaffEvent'] = 'Select relevant staff event';
$Lang['medical']['event']['SelectStudentLog'] = 'Select relevant student log';
$Lang['medical']['event']['ShowFollowupAction'] = 'Show Follow-up Action';
$Lang['medical']['event']['StartTime'] = 'Start Time';
$Lang['medical']['event']['StudentResponse'] = 'Student Response';
$Lang['medical']['event']['Summary'] = 'Summary';
$Lang['medical']['event']['tableHeader']['AffectedPerson'] = 'Affected Person';
$Lang['medical']['event']['tableHeader']['ClassName'] = 'Class Name';
$Lang['medical']['event']['tableHeader']['EndTime'] = 'End Time';
$Lang['medical']['event']['tableHeader']['EventDate'] = 'Date';
$Lang['medical']['event']['tableHeader']['EventType'] = 'Event Type';
$Lang['medical']['event']['tableHeader']['Place'] = 'Place';
$Lang['medical']['event']['tableHeader']['ReportPerson'] = 'Reporter';
$Lang['medical']['event']['tableHeader']['StartTime'] = 'Start Time';
$Lang['medical']['event']['tableHeader']['StayOrNot'] = 'Stay/ Not Stay';
$Lang['medical']['event']['tableHeader']['StudentName'] = 'Student Name';
$Lang['medical']['event']['ViewCase'] = 'View Case';
$Lang['medical']['event']['ViewRelevantStaffEvent'] = 'View Relevant Staff Event';
$Lang['medical']['event']['ViewStaffEvent'] = 'View Staff Event';
$Lang['medical']['event']['ViewStudentLog'] = 'View Student Log';
$Lang['medical']['event']['Warning']['ConfirmDeleteEventCase'] = 'Are you sure you would like to delete this link to relevant case?';
$Lang['medical']['event']['Warning']['ConfirmDeleteStaffEvent'] = 'Are you sure you would like to delete this link to relevant staff event?';
$Lang['medical']['event']['Warning']['ConfirmDeleteStudentLLog'] = 'Are you sure you would like to delete this link to relevant student log?';
$Lang['medical']['event']['Warning']['InputFollowup'] = 'Please input at least one follow-up action';
$Lang['medical']['event']['Warning']['InputSummary'] = 'Please input summary';
$Lang['medical']['event']['Warning']['SelectCase'] = 'Please select relevant case';
$Lang['medical']['event']['Warning']['SelectEventType'] = 'Please select event type';
$Lang['medical']['event']['Warning']['SelectStudent'] = 'Please select student';
$Lang['medical']['event']['Warning']['StartTimeGtEndTime'] = 'Start Time cannot be later than End Time';
$Lang['medical']['filter']['FilterDateRange'] = 'Filter Date Range';
$Lang['medical']['revisit']['AddDrug'] = 'Add drug';
$Lang['medical']['revisit']['AllDivision'] = ' - All Division - ';
$Lang['medical']['revisit']['AllGroup'] = ' - All Group - ';
$Lang['medical']['revisit']['AllHospital'] = ' - All Hospital - ';
$Lang['medical']['revisit']['CuringStatus'] = 'Curing Status';
$Lang['medical']['revisit']['Division'] = 'Division';
$Lang['medical']['revisit']['Dosage'] = 'Dosage';
$Lang['medical']['revisit']['DosageUnit']['mg'] = 'mg';
$Lang['medical']['revisit']['DosageUnit']['ml'] = 'ml';
$Lang['medical']['revisit']['DosageUnit']['prn'] = 'prn';
$Lang['medical']['revisit']['Drug'] = 'Drug';
$Lang['medical']['revisit']['Hospital'] = 'Hospital';
$Lang['medical']['revisit']['LastChangedDrug'] = 'Last drug changed';
$Lang['medical']['revisit']['IsChangedDrug'] = 'Is changed drug';
$Lang['medical']['revisit']['RemoveDrug'] = 'Remove drug';
$Lang['medical']['revisit']['RevisitDate'] = 'Next revisit date';
$Lang['medical']['revisit']['RevisitDate2'] = 'Revisit Date';
$Lang['medical']['revisit']['ShowLastChangedDrug'] = 'Show last drug changed';
$Lang['medical']['revisit']['StudentName'] = 'Student Name';
$Lang['medical']['revisit']['ThisRevisitInfo'] = 'This revisit info';
$Lang['medical']['revisit']['Unit'] = 'Unit';
$Lang['medical']['revisit']['Warning']['InputDosage'] = 'Please input number';
$Lang['medical']['revisit']['Warning']['InputDrugName'] = 'Please input drug name';
$Lang['medical']['revisit']['Warning']['SelectUnit'] = 'Please select dosage unit';
$Lang['medical']['revisit']['Warning']['CuringStatusEmpty'] = 'Curing Status cannot be empty';
$Lang['medical']['revisit']['CannotEarlier'] = "cannot be earlier than";
$Lang['medical']['revisit']['Diagnosis'] = "Diagnosis";
$Lang['medical']['revisit']['VisitDate'] = "Visit Date";
$Lang['medical']['revisit']['AccidentInjure'] = "Accident / Injure";
$Lang['medical']['revisit']['DischargedAdmittion'] = "Discharged / Admittion";
$Lang['medical']['revisit']['unstatedDate'] = "To be Determined";
$Lang['medical']['revisit']['VisitHospital'] = "Visit Hospital / Clinic";
$Lang['medical']['revisit']['StatusNormal'] = "Normal";
$Lang['medical']['revisit']['SelectAtLeastOneItem'] = "Please select at least one item";
$Lang['medical']['staffEvent']['AllSubcategory'] = ' - All Subcategory - ';
$Lang['medical']['staffEvent']['DaysOfInjuryLeave'] = 'Days of work injury leave';
$Lang['medical']['staffEvent']['DaysOfSickLeave'] = 'Days of sick leave';
$Lang['medical']['staffEvent']['EndTime'] = 'End Time';
$Lang['medical']['staffEvent']['EventDate'] = 'Date';
$Lang['medical']['staffEvent']['EventType'] = 'Event Type';
$Lang['medical']['staffEvent']['HasReportedInjury'] = 'Has reported work injury';
$Lang['medical']['staffEvent']['LinkRelevantEvent'] = 'Link relevant behaviour and event';
$Lang['medical']['staffEvent']['Place'] = 'Place';
$Lang['medical']['staffEvent']['RelevantEvent'] = 'Relevant behaviour and event';
$Lang['medical']['staffEvent']['RemoveEvent'] = 'Remvoe Relevant Event';
$Lang['medical']['staffEvent']['remark'] = 'Remark';
$Lang['medical']['staffEvent']['ReturnMessage']['EventTypeNotSetup'] = "0|=|Staff event type has not setup";
$Lang['medical']['staffEvent']['SelectEvent'] = 'Select Relevant Event';
$Lang['medical']['staffEvent']['setting']['subcategoryQty'] = 'Sub-category Quantity';
$Lang['medical']['staffEvent']['StartTime'] = 'Start Time';
$Lang['medical']['staffEvent']['StudentName'] = 'Student Name';
$Lang['medical']['staffEvent']['tableHeader']['EndTime'] = 'End Time';
$Lang['medical']['staffEvent']['tableHeader']['EventDate'] = 'Date';
$Lang['medical']['staffEvent']['tableHeader']['EventType'] = 'Event Type';
$Lang['medical']['staffEvent']['tableHeader']['Place'] = 'Place';
$Lang['medical']['staffEvent']['tableHeader']['ReportPerson'] = 'Reporter';
$Lang['medical']['staffEvent']['tableHeader']['StartTime'] = 'Start Time';
$Lang['medical']['staffEvent']['tableHeader']['StaffName'] = 'Staff Name';
$Lang['medical']['staffEvent']['ViewEvent'] = 'View Relevant Event';
$Lang['medical']['staffEvent']['Warning']['ConfirmDeleteStudentEventRelStaff'] = 'Are you sure you would like to delete this link to relevant event?';
$Lang['medical']['staffEvent']['warning']['duplicateStudentLog'] = 'At least one student log is duplicate to current selection, please try again.';
$Lang['medical']['staffEvent']['Warning']['PleaseInputNumeric'] = 'Please input numeric';
$Lang['medical']['staffEvent']['Warning']['SelectEventType'] = 'Please select event type';
$Lang['medical']['staffEvent']['Warning']['SelectStaff'] = 'Please select staff';
$Lang['medical']['upload']['jsWaitAllFilesUploaded'] = "Please wait until all files are completed uploading.";
$Lang['medical']['upload']['OrDragAndDropFilesHere'] = "Or drag and drop files here.";
$Lang['medical']['reportStaffEvent']['noStaffSelected'] = 'Please select at least one staff';
$Lang['medical']['reportStaffEvent']['noStaffTypeSelected'] = 'Please select at least one staff type';
$Lang['medical']['reportStaffEvent']['StaffType'] = 'Staff Type';
$Lang['medical']['reportStaffEvent']['TotalLeave'] = 'Total';

$Lang['medical']['handover']['record'] = 'Administration Handover Records';
$Lang['medical']['handover']['item'] = 'Administration Item';
$Lang['medical']['handover']['studentName'] = 'Student Name';
$Lang['medical']['handover']['pic'] = 'PICs';
$Lang['medical']['handover']['itemName'] = 'Item Name';
$Lang['medical']['handover']['itemCode'] = 'Code';
$Lang['medical']['handover']['itemStatus'] = 'Status';
$Lang['medical']['handover']['attachment'] = 'Attachment';
$Lang['medical']['handover']['attachmentCount'] = 'Attachment Count';
$Lang['medical']['handover']['nameInUse'] = 'The code is using by another item already';
$Lang['medical']['handover']['noPICSelected'] = 'Please select at least one PIC';

if (is_file("$intranet_root/lang/cust/medical_lang.$intranet_session_language.customized.php"))
{
	include("$intranet_root/lang/cust/medical_lang.$intranet_session_language.customized.php");
}

?>