<?php
// editing by RITA
//include_once('readinggarden_lang.b5.marcus.php');
//include_once('readinggarden_lang.b5.carlos.php');

$Lang['ReadingGarden']['ReadingGarden'] = "閱讀計劃";

$Lang['ReadingGarden']['Class'] = "班別";
$Lang['ReadingGarden']['Form'] = "級別";
$Lang['ReadingGarden']['BookSource'] = "圖書來源";
$Lang['ReadingGarden']['CategoryCode'] = "類別代號";
$Lang['ReadingGarden']['Book'] = "圖書";
$Lang['ReadingGarden']['Attachment'] = "附件";
$Lang['ReadingGarden']['AnswerSheet'] = " 工作紙";
$Lang['ReadingGarden']['Name'] = "名字";

$Lang['ReadingGarden']['StudentRecord'] = "學生記錄";
$Lang['ReadingGarden']['MyRecord'] = "我的記錄";
$Lang['ReadingGarden']['State'] = "等級";
$Lang['ReadingGarden']['AssignedBook'] = "指定閱讀";
$Lang['ReadingGarden']['MyReadingBook'] = "我的閱讀書";
$Lang['ReadingGarden']['More'] = "更多...";
$Lang['ReadingGarden']['Target'] = "對象";
$Lang['ReadingGarden']['ReadingStatus'] = "閱讀狀態";
$Lang['ReadingGarden']['BookReport'] = "閱讀報告";
$Lang['ReadingGarden']['Unread'] = "未閱讀";
$Lang['ReadingGarden']['Read'] = "已閱讀";
$Lang['ReadingGarden']['SubmitBookReport'] = "提交報告書";
$Lang['ReadingGarden']['BookReportSubmitting'] = "提交報告中...";
$Lang['ReadingGarden']['Like'] = "讚";
$Lang['ReadingGarden']['Unlike'] = "收回讚";
$Lang['ReadingGarden']['likes'] = "說讚";
$Lang['ReadingGarden']['You'] = "你";
$Lang['ReadingGarden']['AlsoLikeThis'] = "說讚。";
$Lang['ReadingGarden']['Comment'] = "評論";
$Lang['ReadingGarden']['Settings'] = "設定";
$Lang['ReadingGarden']['Home'] = "首頁";
$Lang['ReadingGarden']['AccomplishRate'] = "完成率";
$Lang['ReadingGarden']['BracketChinese'] = "（中文）";
$Lang['ReadingGarden']['BracketEnglish'] = "（英文）";
$Lang['ReadingGarden']['BracketOthers'] = "(廣泛閱讀)";
$Lang['ReadingGarden']['Report'] = "報告";
$Lang['ReadingGarden']['Period'] = "時段";
$Lang['ReadingGarden']['To'] = "至";
$Lang['ReadingGarden']['BookInfo'] = "圖書資料";
$Lang['ReadingGarden']['ReadingStatusAndReport'] = "閱讀現狀及閱讀報告";
$Lang['ReadingGarden']['ExistingBook'] = "現存圖書";
$Lang['ReadingGarden']['ExternalBook'] = "外來圖書";
$Lang['ReadingGarden']['BookSearchResult'] = "搜尋結果";
$Lang['ReadingGarden']['BookReportRequired'] = "需要提交閱讀報告";
$Lang['ReadingGarden']['AddOtherReadingRecord'] = "提交更多閱讀記錄";
$Lang['ReadingGarden']['BackToMyReadingRecord'] = "返回我的記錄";
$Lang['ReadingGarden']['DisplayAllCover'] = "顯示所有封面";
$Lang['ReadingGarden']['HideAllCover'] = "隱藏所有封面";

$Lang['ReadingGarden']['ClassExport'] = "全級匯出";
$Lang['ReadingGarden']['SchoolExport'] = "全校匯出";
$Lang['ReadingGarden']['Title'] = "標題";
$Lang['ReadingGarden']['Message'] = "內容";
$Lang['ReadingGarden']['EndDate'] = "結束日期";
$Lang['ReadingGarden']['DaysAgo'] = "天前";
$Lang['ReadingGarden']['PostedBy'] = "發表";
$Lang['ReadingGarden']['ResultPublishDate'] = "結果公佈日期";
$Lang['ReadingGarden']['StudentState'] = "學生等級";
$Lang['ReadingGarden']['MyLike'] = "我的讚";
$Lang['ReadingGarden']['Score'] = "分數";
$Lang['ReadingGarden']['Progress'] = "完成率";
$Lang['ReadingGarden']['Recommend'] = "推介";
$Lang['ReadingGarden']['Grade'] = "等級";
$Lang['ReadingGarden']['Mark'] = "積分";
$Lang['ReadingGarden']['TeacherComment'] = "評語";
$Lang['ReadingGarden']['MarkReport'] = "批改報告";
$Lang['ReadingGarden']['BackToMainPage'] = "返回主頁";
$Lang['ReadingGarden']['NoCurrentReadingTarget'] = "沒有進行中的閱讀目標。";
$Lang['ReadingGarden']['InCurrentTarget'] = "計算在進行中的閱讀目標中";
$Lang['ReadingGarden']['Teacher'] = "教師";
$Lang['ReadingGarden']['Student'] = "學生";
$Lang['ReadingGarden']['ConfirmArr']['DeleteTeacherReportComment'] = "你確定要刪除此評論？";
$Lang['ReadingGarden']['ConfirmArr']['DeleteStudentReportComment'] = "學生的等級分數將被扣除罰分，你確定要刪除此評論？";
$Lang['ReadingGarden']['RecommendBookReport'] = "推介閱讀報告";

$Lang['ReadingGarden']['ReadingStatusArr'][2] = "未閱讀";
$Lang['ReadingGarden']['ReadingStatusArr'][1] = "已閱讀";

$Lang['ReadingGarden']['ChineseReadingTarget'] = $Lang['ReadingGarden']['Book']." ".$Lang['ReadingGarden']['BracketChinese'];
$Lang['ReadingGarden']['ChineseReportTarget'] = $Lang['ReadingGarden']['Report']." ".$Lang['ReadingGarden']['BracketChinese'];
$Lang['ReadingGarden']['EnglishReadingTarget'] = $Lang['ReadingGarden']['Book']." ".$Lang['ReadingGarden']['BracketEnglish'];
$Lang['ReadingGarden']['EnglishReportTarget'] = $Lang['ReadingGarden']['Report']." ".$Lang['ReadingGarden']['BracketEnglish'];
$Lang['ReadingGarden']['OtherReadingTarget'] = $Lang['ReadingGarden']['Book']." ".$Lang['ReadingGarden']['BracketOthers'];

$Lang['ReadingGarden']['UnclassifiedCategory'] = "未分類";
$Lang['ReadingGarden']['AssignedClass']= "已指定的班別";
$Lang['ReadingGarden']['UnassignClass']= "刪除指定班別";
$Lang['ReadingGarden']['DeleteReadingAssignment']= "刪除此書目";
$Lang['ReadingGarden']['AssignReadingToClass']= "指定多個書目";
$Lang['ReadingGarden']['SelectClass'] = "選擇班別";
$Lang['ReadingGarden']['SelectReadingAssignment'] = "選擇指定閱讀";
$Lang['ReadingGarden']['SelectedClass'] = "已選班別";
$Lang['ReadingGarden']['AvailableReadingAssignement'] = "可供選擇的指定閱讀";
$Lang['ReadingGarden']['AssignToClass'] = "適用班別";
$Lang['ReadingGarden']['Confirmation'] = "你是否確定要";
$Lang['ReadingGarden']['AssignReadingRemarks'] = "<span class='tabletextrequire'>*</span>已標示的指定閱讀將不會被指定到該班別。";
$Lang['ReadingGarden']['EachBookAssignOnce'] = "每本書只能指定予同一個班別一次。";

$Lang['ReadingGarden']['AwardTypeName'][1] = $Lang['ReadingGarden']['Individual'];
$Lang['ReadingGarden']['AwardTypeName'][2] = $Lang['ReadingGarden']['Group'];
$Lang['ReadingGarden']['AwardWinnerNotPublish'] = "得獎名單尚未公佈";
$Lang['ReadingGarden']['Announcement'] = "宣佈";
$Lang['ReadingGarden']['All'] = "全部";
$Lang['ReadingGarden']['GenerateWinner'] = "產生得獎名單";
$Lang['ReadingGarden']['AssignWinner'] = "加進得獎名單";
$Lang['ReadingGarden']['UnassignWinner'] = "從得獎名單移除";
$Lang['ReadingGarden']['WinnerList'] = "得獎名單";

$Lang['ReadingGarden']['BookRead'] = "完成冊數";
$Lang['ReadingGarden']['ReportSubmit'] = "完成報告數量";
$Lang['ReadingGarden']['IsWinner'] = "得獎名單";

$Lang['ReadingGarden']['InWinnerList'] = "已被加到得獎名單內";
$Lang['ReadingGarden']['NotInWinnerList'] = "尚未加到得獎名單內";

$Lang['ReadingGarden']['NoClassesMeetTheRequirement'] = "沒有班別符合獎勵計劃要求";
$Lang['ReadingGarden']['NoStudentsMeetTheRequirement'] = "沒有學生符合獎勵計劃要求";

$Lang['ReadingGarden']['BookSourceInput'][1] = "管理員輸入";
$Lang['ReadingGarden']['BookSourceInput'][2] = "網上圖書館 匯入";
$Lang['ReadingGarden']['BookSourceInput'][3] = "學生輸入";

$Lang['ReadingGarden']['ViewBook'] = "看書";
$Lang['ReadingGarden']['ViewState'] = "等級要求";
$Lang['ReadingGarden']['StateRequirement'] = "等級要求";
$Lang['ReadingGarden']['StateList'] = "等級列表";
$Lang['ReadingGarden']['ScoringRule'] = "計分規則";
$Lang['ReadingGarden']['FileUpload'] = "上載檔案";
$Lang['ReadingGarden']['OnlineWriting'] = "網上寫作";
$Lang['ReadingGarden']['WordLimit'] = "字數限制";
$Lang['ReadingGarden']['WordCount'] = "字數";
$Lang['ReadingGarden']['Unlimit'] = "不限";

# General Wordings
$Lang['ReadingGarden']['CreateDate'] = "建立日期";
$Lang['ReadingGarden']['LastModifiedDate'] = "最後修改日期";
$Lang['ReadingGarden']['CreatedBy'] = "建立者";
$Lang['ReadingGarden']['LastModifiedBy'] = "最後修改者";
$Lang['ReadingGarden']['Group'] = "小組";
$Lang['ReadingGarden']['Individual'] = "個人";
$Lang['ReadingGarden']['Inter-Class'] = "班際";
$Lang['ReadingGarden']['SortBy'] = "排序以";

//Left Menu Title
$Lang['ReadingGarden']['ManagementArr']['MenuTitle'] = "管理";
$Lang['ReadingGarden']['AnnouncementMgmt']['MenuTitle'] = "宣佈";
$Lang['ReadingGarden']['ForumMgmt']['MenuTitle'] = "討論區";
$Lang['ReadingGarden']['ReportsArr']['MenuTitle'] = "報告";
$Lang['ReadingGarden']['StatisticArr']['MenuTitle'] = "統計";
$Lang['ReadingGarden']['SettingsArr']['MenuTitle'] = "設定";

//Return Message
$Lang['ReadingGarden']['ReturnMessage']['SettingsSaveSuccess'] = "1|=|設定已儲存";
$Lang['ReadingGarden']['ReturnMessage']['SettingsSaveFail'] = "0|=|設定儲存失敗";
$Lang['ReadingGarden']['ReturnMessage']['AssignToClassSuccess']= "1|=|指定閱讀成功。";
$Lang['ReadingGarden']['ReturnMessage']['AssignToClassFail']= "0|=|未能指定閱讀。";
$Lang['ReadingGarden']['ReturnMessage']['UnassignClassSuccess']= "1|=|已成功取消指定閱讀。";
$Lang['ReadingGarden']['ReturnMessage']['UnassignClassFail']= "0|=|未能取消指定閱讀。";

//Warning Message
$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'] = "請輸入數字。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBookSource'] = "請選擇圖書來源。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputCategoryName'] = "請輸入類別名稱。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputCategoryCode'] = "請輸入類別代號。";
$Lang['ReadingGarden']['WarningMsg']['CategoryCodeExist'] = "類別代號已被使用。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputAssignReadingName'] = "請輸入指定閱讀的名稱。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBook']= "請選擇圖書。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputStateName'] = "請輸入等級名稱。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputScoreRequired'] = "請輸入分數範圍。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputAnInteger'] = "請輸入整數。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectAnImage']= "請選擇等級圖像。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectReadingStatus'] = "請選擇閱讀狀態。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputAuthor'] = "請輸入一位作者。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputPublisher'] = "請輸入一個出版商。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBookCover'] = "請選擇書的封面。";
$Lang['ReadingGarden']['WarningMsg']['PleaseComposeAsnwerSheet'] = "請編制工作紙。";
$Lang['ReadingGarden']['WarningMsg']['ScoreRequiredExist'] = "分數範圍已存在。";
$Lang['ReadingGarden']['WarningMsg']['AssignmentAssigned'] = "這個指定閱讀已被指定到此班別。";
$Lang['ReadingGarden']['WarningMsg']['BookAssigned'] = "這本書已被指定到此班別。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectClass']= "請選擇班別。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectForm']= "請選擇級別。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectCategory']= "請選擇分類。";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectReadingAssignment']= "請選擇一個或更多的指定閱讀。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputMessage'] = "請輸入內容。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputAnnouncementTitle'] = "請輸入宣佈的標題。";
$Lang['ReadingGarden']['WarningMsg']['InvalidPublishDate'] = "結果公佈之日期不得早於結束日期。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputWriting'] = "請輸入內容。";
$Lang['ReadingGarden']['WarningMsg']['WordCountExceedLimit'] = "字數在限制範圍以外。";

//Select Title
$Lang['ReadingGarden']['SelectTitleArr']['AllCategory'] = "-- 所有分類 --";
$Lang['ReadingGarden']['SelectTitleArr']['SelectAssignReading'] = " -- 選擇指定閱讀 --";
$Lang['ReadingGarden']['SelectTitleArr']['NewAssignReading'] = " -- 新增指定閱讀 --";
$Lang['ReadingGarden']['SelectTitleArr']['Status'] = "選擇狀態";
$Lang['ReadingGarden']['SelectTitleArr']['BookSource'] = "圖書來源";
$Lang['ReadingGarden']['SelectTitleArr']['AllAssignReading'] = " -- 所有指定圖書 --";
$Lang['ReadingGarden']['SelectTitleArr']['AllReadingStatus'] = " -- 所有閱讀狀態 -- ";

//Confirm Message
$Lang['ReadingGarden']['ConfirmArr']['DeleteCategory'] = "你確定要刪除此類別？";
$Lang['ReadingGarden']['ConfirmArr']['DeleteCategoryWithBook'] = "此類別的圖書將被移至\"未分類\"，你確定要刪除此類別？";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromClassWithReadingRecord'] = "所有與此指定閱讀相關的閱讀記錄將從這班刪除，繼續？";
$Lang['ReadingGarden']['ConfirmArr']['AssignedReadingOfAllClassWillBeUpdate'] = "其他班級的指定閱讀也將會被修改，繼續？";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromClass']= "你是否確定要刪除全部班別？此動作只會移除班別，但不會移除書目。";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromAllClass']= "你確定要從所有班別刪除這個指定閱讀？";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReading']= "你確定要刪除這指定閱讀？";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromClass']= "你是否確定要刪除全部班別？此動作只會移除班別，但不會移除書目。";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromAllClass']= "你確定要從所有班別刪除這個指定？";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReading']= "你確定要刪除這指定閱讀？";

$Lang['ReadingGarden']['ConfirmArr']['UnassgnWinner'] = "你確定從得獎名單移除已選的學生？";
$Lang['ReadingGarden']['ConfirmArr']['WinnerListWasGenerated'] = "已產生得獎名單將被移除，繼續？";


$Lang['ReadingGarden']['Indication']['Normal'] = "一般";
$Lang['ReadingGarden']['Indication']['InProgress'] = "進行中";

//ScoringSettings
$Lang['ReadingGarden']['ScoringSettings']['MenuTitle'] = "計分規則";
$Lang['ReadingGarden']['ScoringSettings']['For'] = "";
$Lang['ReadingGarden']['ScoringSettings']['Every'] = "每";
$Lang['ReadingGarden']['ScoringSettings']['Words'] = "字";
$Lang['ReadingGarden']['ScoringSettings']['Times'] = "次";
$Lang['ReadingGarden']['ScoringSettings']['Time'] = "次";
$Lang['ReadingGarden']['ScoringSettings']['MoreThan'] = "超過";
$Lang['ReadingGarden']['ScoringSettings']['MinsWithin24Hours'] = "分鐘 (24小時內)";
$Lang['ReadingGarden']['ScoringSettings']['ScoringRule'] = "計分規則";
$Lang['ReadingGarden']['ScoringSettings']['PressLikeButton'] = "獲得「讚」";
$Lang['ReadingGarden']['ScoringSettings']['WriteComment'] = "寫評語";
$Lang['ReadingGarden']['ScoringSettings']['StartAThreadInForum'] = "在論壇開新話題";
$Lang['ReadingGarden']['ScoringSettings']['FollowAThreadInForum'] = "在話題留言";
$Lang['ReadingGarden']['ScoringSettings']['AddReadingRecord'] = "新增閱讀記錄";
$Lang['ReadingGarden']['ScoringSettings']['WriteBookReport'] = "呈交閱讀報告";
$Lang['ReadingGarden']['ScoringSettings']['TeacherRecommendReport'] = "閱讀報告被教師推介";
$Lang['ReadingGarden']['ScoringSettings']['CommentMoreThan'] = "評語多於";
$Lang['ReadingGarden']['ScoringSettings']['CommentLength'] = "評語長度";
$Lang['ReadingGarden']['ScoringSettings']['Login'] = "登入";

$Lang['ReadingGarden']['ScoringSettings']['SuccessiveDaysWithoutLogin'] = "連續幾天沒有登入";
$Lang['ReadingGarden']['ScoringSettings']['SuccessiveDays'] = "相連日";
$Lang['ReadingGarden']['ScoringSettings']['CommentDeletedByAdmin'] = "評論被管理員刪除";
$Lang['ReadingGarden']['ScoringSettings']['ForumThreadDeletedByAdmin'] = "論壇主題被管理員刪除";
$Lang['ReadingGarden']['ScoringSettings']['ForumPostDeletedByAdmin'] = "論壇留言被管理員刪除";

$Lang['ReadingGarden']['ScoringSettings']['InputZeroToDisableRule'] = "請輸入'0'或留空以停用該計分規則";

//BookSourceSettings
$Lang['ReadingGarden']['BookSourceSettings']['MenuTitle'] = "圖書來源";
$Lang['ReadingGarden']['BookSourceSettings']['BookSource'][1] = "只限推介圖書";
$Lang['ReadingGarden']['BookSourceSettings']['BookSource'][2] = "所有「圖書列表」中的圖書";
$Lang['ReadingGarden']['BookSourceSettings']['BookSource'][3] = "任何圖書，包括學生自行輸入的書目";
$Lang['ReadingGarden']['BookSourceSettings']['PossibleBookSource'] = "閱讀報告可用的圖書來源";

//Book Report Setting
$Lang['ReadingGarden']['BookReportSettings']['MenuTitle'] = "閱讀報告設定";

//Book Category
$Lang['ReadingGarden']['BookCategorySettings']['MenuTitle'] = "圖書分類設定";
$Lang['ReadingGarden']['BookCategorySettings']['Language'] = "語言分頖";
$Lang['ReadingGarden']['BookCategorySettings']['Chinese'] = "中文";
$Lang['ReadingGarden']['BookCategorySettings']['English'] = "英文";
$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][1] =  "中文";
$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][2] =  "英文";
$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][3] =  "其他";
$Lang['ReadingGarden']['BookCategorySettings']['CategoryCode'] = "類別代號";
$Lang['ReadingGarden']['BookCategorySettings']['CategoryName'] = "類別名稱";
$Lang['ReadingGarden']['BookCategorySettings']['AddCategory'] = "新增類別";
$Lang['ReadingGarden']['BookCategorySettings']['MoveCategory'] = "移動類別";
$Lang['ReadingGarden']['BookCategorySettings']['EditCategory'] = "修改類別";
$Lang['ReadingGarden']['BookCategorySettings']['DeleteCategory'] = "刪除類別";
$Lang['ReadingGarden']['BookCategorySettings']['NumOfBooks'] = "圖書數量";
$Lang['ReadingGarden']['BookCategorySettings']['Category'] = "類別";
$Lang['ReadingGarden']['BookCategorySettings']['Uncategorized'] = "未分類";

// RecommendBookSettings
$Lang['ReadingGarden']['RecommendBookSettings']['MenuTitle'] = "推介圖書";
$Lang['ReadingGarden']['RecommendBookSettings']['AddRecommendBook'] = "新增推介圖書";
$Lang['ReadingGarden']['RecommendBookSettings']['SelectBook'] = "選擇圖書";
$Lang['ReadingGarden']['RecommendBookSettings']['BookSelected'] = "已選擇的圖書";
$Lang['ReadingGarden']['RecommendBookSettings']['From'] = "從";
$Lang['ReadingGarden']['RecommendBookSettings']['EnterDetail'] = "輸入 \"[書名] [作者] [發布] \"";
$Lang['ReadingGarden']['RecommendBookSettings']['EnterCallNumber'] = "輸入 [電話號碼]";

// AssignReadingSettings
$Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'] = "指定書目";
$Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading'] = "指定單一書目";
$Lang['ReadingGarden']['AssignReadingSettings']['EditAssignReading'] = "編輯書目";
$Lang['ReadingGarden']['AssignReadingSettings']['AssignReadingName'] = "指定書目名稱";
$Lang['ReadingGarden']['AssignReadingSettings']['AssignReading'] = "指定書目";
$Lang['ReadingGarden']['AssignReadingSettings']['ManageAssignment'] = "管理指定書目";
$Lang['ReadingGarden']['AssignReadingSettings']['NumberOfReadingRecord'] = "提交閱讀記錄";
$Lang['ReadingGarden']['AssignReadingSettings']['Assignment'] = "指定書目";
$Lang['ReadingGarden']['AssignReadingSettings']['MoveReadingAssignment'] = "移動指定書目";
$Lang['ReadingGarden']['AssignReadingSettings']['UseAnswerSheet'] = "使用";
$Lang['ReadingGarden']['AssignReadingSettings']['UseBookReport'] = "使用";
$Lang['ReadingGarden']['AssignReadingSettings']['FileUpload'] = "上載檔案";
$Lang['ReadingGarden']['AssignReadingSettings']['OnlineWriting'] = "網上寫作";
$Lang['ReadingGarden']['AssignReadingSettings']['WordLimit'] = "字數限制";

// StateSettings
$Lang['ReadingGarden']['StateSettings']['MenuTitle'] = "學生等級";
$Lang['ReadingGarden']['StateSettings']['StateName'] = "等級名稱";
$Lang['ReadingGarden']['StateSettings']['Description'] = "描述";
$Lang['ReadingGarden']['StateSettings']['ScoreRequired'] = "分數要求";// 20110328
$Lang['ReadingGarden']['StateSettings']['ScoreRange'] = "分數範圍";// 20110328
$Lang['ReadingGarden']['StateSettings']['Logo'] = "等級圖像";
$Lang['ReadingGarden']['StateSettings']['New'] = "新增";
$Lang['ReadingGarden']['StateSettings']['NewState'] = "新增等級";
$Lang['ReadingGarden']['StateSettings']['EditState'] = "編輯等級";
$Lang['ReadingGarden']['StateSettings']['InputZeroForDefaultState'] = "輸入'0'為預設等級。";

# Book
$Lang['ReadingGarden']['BookSettings']['MenuTitle'] = "圖書列表";
$Lang['ReadingGarden']['BookSettings']['AllBooks'] = "所有圖書";


# Books Wordings
$Lang['ReadingGarden']['CallNumber'] = "索書號";
$Lang['ReadingGarden']['BookName'] = "書名";
$Lang['ReadingGarden']['Author'] = "作者";
$Lang['ReadingGarden']['Publisher'] = "出版社";
$Lang['ReadingGarden']['Description'] = "描述";
$Lang['ReadingGarden']['CoverImage'] = "封面";
$Lang['ReadingGarden']['CategoryName'] = "分類名稱";
$Lang['ReadingGarden']['Language'] = "語言";
$Lang['ReadingGarden']['AddBook'] = "新增圖書";
$Lang['ReadingGarden']['EditBook'] = "編輯圖書";
$Lang['ReadingGarden']['English'] = "英文";
$Lang['ReadingGarden']['Chinese'] = "中文";
$Lang['ReadingGarden']['Either'] = "不拘";
$Lang['ReadingGarden']['Category'] = "分類";
$Lang['ReadingGarden']['SubCategory'] = "子類別";
$Lang['ReadingGarden']['Source'] = "來源";
$Lang['ReadingGarden']['BookReport'] = "閱讀報告";
$Lang['ReadingGarden']['AssignToForm'] = "分派到級別";
$Lang['ReadingGarden']['ImportFromELIBRARY'] = "從".$Lang['Header']['Menu']['eLibrary']."匯入";
$Lang['ReadingGarden']['FromELIBRARY'] = "匯入圖書". " (從".$Lang['Header']['Menu']['eLibrary'].")";
$Lang['ReadingGarden']['ViewBookDetail'] = "檢視圖書詳細資料";

# Answer Sheet Template Wordings
$Lang['ReadingGarden']['AnswerSheetTemplateSettings']['MenuTitle'] = "工作紙範本";
$Lang['ReadingGarden']['AnswerSheet'] = "工作紙";
$Lang['ReadingGarden']['AnswerSheetTemplate'] = "工作紙範本";
$Lang['ReadingGarden']['TemplateName'] = "範本名稱";
$Lang['ReadingGarden']['NewAnswerSheetTemplate'] = "新增範本";

# Award Scheme Wordings
$Lang['ReadingGarden']['AwardScheme'] = "獎勵計劃";
$Lang['ReadingGarden']['Award'] = "獎勵";
$Lang['ReadingGarden']['AwardName'] = "獎勵名稱";
$Lang['ReadingGarden']['Type'] = "類別";
$Lang['ReadingGarden']['TargetDescription'] = "對象(顯示於學生介面，如「中一至中三」)";
$Lang['ReadingGarden']['TargetClass'] = "對象班別";
$Lang['ReadingGarden']['MinBookRequired'] = "需完成冊數";
$Lang['ReadingGarden']['MinBookRequiredInSpecificCategory'] = "各種類的最少閱書量";
$Lang['ReadingGarden']['MinReportRequired'] = "需完成報告數量";
$Lang['ReadingGarden']['Period'] = "期限";
$Lang['ReadingGarden']['Winner'] = "獲獎者";
$Lang['ReadingGarden']['Requirement'] = "得獎條件";
$Lang['ReadingGarden']['AddAward'] = "新增獎勵";
$Lang['ReadingGarden']['AwardTypeName'][1] = $Lang['ReadingGarden']['Individual'];
$Lang['ReadingGarden']['AwardTypeName'][2] = $Lang['ReadingGarden']['Group'];
$Lang['ReadingGarden']['Level'] = "等級";
$Lang['ReadingGarden']['AwardLevel'] = "獎勵等級";
$Lang['ReadingGarden']['LevelName'] = "等級名稱";
$Lang['ReadingGarden']['Detail'] = "詳細資料";

# Reading Target Wordings
$Lang['ReadingGarden']['ReadingTarget'] = "階段目標";
$Lang['ReadingGarden']['NewReadingTarget'] = "新增階段目標";
$Lang['ReadingGarden']['EditReadingTarget'] = "編輯階段目標";
$Lang['ReadingGarden']['ReadingTargetName'] = "閱讀階段";
$Lang['ReadingGarden']['NumberOfReadingRequired'] = "需完成冊數";
$Lang['ReadingGarden']['NumberOfBookReportRequired'] = "需完成報告數量";

# Recommend Book List Wordings
$Lang['ReadingGarden']['RecommendBookList'] = "推介書目";
$Lang['ReadingGarden']['RecommendBook'] = "推介圖書";
$Lang['ReadingGarden']['RecommendBookReport'] = "推介閱讀報告";
$Lang['ReadingGarden']['Import_Book_Column'] = array("書名","索書號","ISBN","作者","出版社","語言","分類","描述","分派到級別","工作紙");

$Lang['SysMgr']['ReadingGarden']['Button']['ImportBook'] = "匯入圖書";
$Lang['ReadingGarden']['Import_Error']['BookTitleIsMissing']= "書名不能留空";
$Lang['ReadingGarden']['Import_Error']['InvalidISBN'] ="ISBN不正確";
$Lang['ReadingGarden']['Import_Error']['InvalidCallNumber'] = "索書號不正確";
$Lang['ReadingGarden']['Import_Error']['InvalidLanguage'] ="語言不正確";
$Lang['ReadingGarden']['Import_Error']['LanguageIsMissing'] = "語言不能留空";
$Lang['ReadingGarden']['Import_Error']['InvalidCategory'] ="類別不正確";
$Lang['ReadingGarden']['Import_Error']['CategoryIsMissing'] = "類別不能留空";
$Lang['ReadingGarden']['Import_Error']['InvalidForm'] ="級別不正確";
$Lang['ReadingGarden']['Import_Error']['InvalidAnswerSheet'] = "工作紙不正確";
$Lang['SysMgr']['ReadingGarden']['ImportBook']['CategoryRemarks'] = "請根據「語言」填寫相應的「類別」，如果沒有「類別」，請填上\"Uncategorized\"。";

# Reading Record Wordings
$Lang['ReadingGarden']['MyRecord'] = "我的記錄";
$Lang['ReadingGarden']['ReadingRecord'] = "閱讀記錄";
$Lang['ReadingGarden']['AddReadingRecord'] = "新增閱讀記錄";
$Lang['ReadingGarden']['EditReadingRecord'] = "編輯閱讀記錄";
$Lang['ReadingGarden']['DeleteReadingRecord'] = "刪除閱讀記錄";
$Lang['ReadingGarden']['StartDate'] = "開始日期";
$Lang['ReadingGarden']['FinishedDate'] = "結束日期";
$Lang['ReadingGarden']['ReadingHours'] = "閱讀時數";
$Lang['ReadingGarden']['Hour(s)'] = "小時";
$Lang['ReadingGarden']['Progress'] = "進度";
$Lang['ReadingGarden']['Readed'] = "已閱讀";

# Book Report Wordings
$Lang['ReadingGarden']['HandInBookReport'] = "上載檔案";
$Lang['ReadingGarden']['DoAnswerSheet'] = "填寫工作紙";
$Lang['ReadingGarden']['EditAnswerSheet'] = "編輯工作紙";
$Lang['ReadingGarden']['DeleteAnswerSheet'] = "刪除工作紙";
$Lang['ReadingGarden']['ViewAnswerSheet'] = "檢視工作紙";
$Lang['ReadingGarden']['BookReport'] = "閱讀報告";
$Lang['ReadingGarden']['ViewBookReport'] = "檢視閱讀報告";
$Lang['ReadingGarden']['DeleteBookReport'] = "刪除閱讀報告";
$Lang['ReadingGarden']['Grade'] = "評分";
$Lang['ReadingGarden']['RecommendThisBookReport'] = "推介此閱讀報告";
$Lang['ReadingGarden']['Recommended'] = "被推介";
$Lang['ReadingGarden']['MarkingArea'] = "評核區";
$Lang['ReadingGarden']['Mark(s)'] = "分";
$Lang['ReadingGarden']['Answer'] = "答案";
$Lang['ReadingGarden']['EditOnlineWriting'] = "編輯網上寫作";
$Lang['ReadingGarden']['DeleteOnlineWriting'] = "刪除網上寫作";
$Lang['ReadingGarden']['PublicLink'] = "公開分享";

# Forum Wordings
$Lang['ReadingGarden']['Forum'] = "討論區";
$Lang['ReadingGarden']['ForumName'] = "討論區名稱";
$Lang['ReadingGarden']['MoveForum'] = "移動討論區";
$Lang['ReadingGarden']['StudentCreateTopicRequireApprove'] = "學生新建主題需要批准";
$Lang['ReadingGarden']['Topic'] = "主題";
$Lang['ReadingGarden']['NewTopic'] = "新增主題";
$Lang['ReadingGarden']['Post'] = "帖子";
$Lang['ReadingGarden']['NewPost'] = "新增回應";
$Lang['ReadingGarden']['Reply'] = "回應";
//$Lang['ReadingGarden']['Quote'] = "引用";
$Lang['ReadingGarden']['Topic(s)'] = "主題";
$Lang['ReadingGarden']['Post(s)'] = "回帖";
$Lang['ReadingGarden']['LastTopicOn'] = "最後發表的主題於";
$Lang['ReadingGarden']['LastPostOn'] = "最後發表的回應於";
$Lang['ReadingGarden']['Approved'] = "已批核";
$Lang['ReadingGarden']['WaitingForApproval'] = "等待批核";
$Lang['ReadingGarden']['Rejected'] = "被拒";
$Lang['ReadingGarden']['Approve'] = "批准";
$Lang['ReadingGarden']['Reject'] = "拒絕";
$Lang['ReadingGarden']['MarkAsOnTop'] = "置頂";
$Lang['ReadingGarden']['RemoveOnTop'] = "移除置頂";
$Lang['ReadingGarden']['NumberOfReply'] = "回應數目";
$Lang['ReadingGarden']['LastReplyBy'] = "最後回應者";
$Lang['ReadingGarden']['LastPostTime'] = "最後回應時間";
$Lang['ReadingGarden']['Content'] = "內容";
$Lang['ReadingGarden']['PostSubject'] = "帖子主題";
$Lang['ReadingGarden']['ThereIsNoReplyAtTheMoment'] = "暫時仍未有任何回帖。";
$Lang['ReadingGarden']['Reply:'] = "回應: ";
$Lang['ReadingGarden']['PostedOn'] = "發表於";
$Lang['ReadingGarden']['GoToTop'] = "回到最上";
$Lang['ReadingGarden']['PreviousTopic'] = "上一個主題";
$Lang['ReadingGarden']['NextTopic'] = "下一個主題";

# Comment bank
$Lang['ReadingGarden']['CommentBank'] = "評語庫";
$Lang['ReadingGarden']['AddComment'] = "加入評語";

# Report Sticker
$Lang['ReadingGarden']['Sticker'] = "貼紙";
$Lang['ReadingGarden']['UploadStickerImage'] = "上傳貼紙圖片";
$Lang['ReadingGarden']['UploadNewStickerImage'] = "上傳新的貼紙圖片";
$Lang['ReadingGarden']['CurrentStickerImage'] = "目前的貼紙圖片";
$Lang['ReadingGarden']['DescribeStickerDimension'] = "最佳維度為  寬:120px x 長:120px";
$Lang['ReadingGarden']['GiveSticker'] = "給予貼紙";
$Lang['ReadingGarden']['ClickToRemoveSticker'] = "按下移除貼紙";

# Warning Msg
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBook'] = "你是否確定要要刪除此圖書?";
$Lang['ReadingGarden']['WarningMsg']['BooksHaveImportedFromeLibrary'] = "圖書可能已從 ".$Lang['Header']['Menu']['eLibrary']." 匯入過。";
$Lang['ReadingGarden']['WarningMsg']['InvalidCoverImage'] = "上傳的封面檔案並非圖像檔案。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputBookCallNumber'] = "請輸入索書號。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputBookName'] = "請輸入書名。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputISBN'] = "請輸入ISBN。";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteCoverImage'] = "你是否確定要要刪除此封面?";
$Lang['ReadingGarden']['WarningMsg']['AnswerSheetTemplateNameExist'] = "同名的工作紙已經存在。";
$Lang['ReadingGarden']['WarningMsg']['BlankAnswerSheetTemplateName'] = "工作紙名稱不能留空。";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBookAnswerSheet'] = "你是否確定要要刪除此圖書的工作紙?";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteAwardScheme'] = "你是否確定要要刪除此獎勵?";
$Lang['ReadingGarden']['WarningMsg']['RequestInputAwardName'] = "請輸入獎勵名稱。";
$Lang['ReadingGarden']['WarningMsg']['AwardNameExist'] = "同名的獎勵已經存在。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'] = "請輸入一個正整數。";
$Lang['ReadingGarden']['WarningMsg']['InvalidDateFormat'] = "日期格式不正確。";
$Lang['ReadingGarden']['WarningMsg']['InvalidDateRange'] = "日期範圍不正確。";
$Lang['ReadingGarden']['WarningMsg']['ClassAlreadyAdded'] = "班別已經加入。.";
$Lang['ReadingGarden']['WarningMsg']['CategoryAlreadyAdded'] = "類別已經加入。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputReadingTargetName'] = "請輸入閱讀階段名稱。";
$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneClasses'] = "請加入至少一個班別。";
$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastTwoClasses'] = "請加入至少兩個班別。";
$Lang['ReadingGarden']['WarningMsg']['ReadingTargetNameExist'] = "同名的閱讀階段已經存在。";
$Lang['ReadingGarden']['WarningMsg']['HaveOverlappingPeriods'] = "的期限與其他閱讀階段重疊了。";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteReadingTarget'] = "你是否確定要要刪除此閱讀階段?";
$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumber'] = "請輸入大於0的數字。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumberWithin100'] = "請輸入大於0並於100或以內的數字。";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteReadingRecord'] = "已上傳的閱讀報告與工作紙將會同時刪除。你是否確定要刪除此閱讀記錄?";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBookReport'] = "你是否確定要要刪除此閱讀報告?";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteAnswerSheet'] = "你是否確定要要刪除此工作紙?";
$Lang['ReadingGarden']['WarningMsg']['RequestInputForumName'] = "請輸入討論區名稱。";
$Lang['ReadingGarden']['WarningMsg']['ForumNameExist'] = "討論區名稱已存在。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputTopic'] = "請輸入主題。";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputContent'] = "請輸入內容。";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteTopic']  ="你是否確定要要刪除所選主題?";
$Lang['ReadingGarden']['WarningMsg']['NewTopicNeedApprove'] = "新建的主題需要獲得管理員或老師的批准後才會顯示。";
//$Lang['ReadingGarden']['WarningMsg']['NewPostNeedApprove'] = "新建的回帖需要獲得管理員或老師的批准才能看到。";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeletePost'] = "你是否確定要要刪除此帖子?";
$Lang['ReadingGarden']['WarningMsg']['RequestInputStartDateEarlierThanEndDate'] = "請輸入早於結束日期的開始日期。";
$Lang['ReadingGarden']['WarningMsg']['StartDateGreaterThanToday'] = "開始日期不可在今日之後。";
$Lang['ReadingGarden']['WarningMsg']['EndDateGreaterThanToday'] = "結束日期不可以在今日之後。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputLevelName'] = "請輸入等級名稱。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputComment'] = "請輸入評語。";
$Lang['ReadingGarden']['WarningMsg']['RequestInputStickerImage'] = "請上傳貼紙圖片。";
$Lang['ReadingGarden']['WarningMsg']['RequestCompleteAnswerSheetAllAnswerScore'] = "請完成輸入所有答案與分數。";
$Lang['ReadingGarden']['WarningMsg']['InvalidRangeOfWordLimit'] = "不正確的字數限制範圍。";
$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneAwardLevel'] = "請加入最少一個獎勵等級。";

#Return Msg
$Lang['ReadingGarden']['ReturnMsg']['AddBookSuccess'] = "1|=|已成功新增圖書。";
$Lang['ReadingGarden']['ReturnMsg']['AddBookFail'] = "0|=|新增圖書失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteBookSuccess'] = "1|=|已成功刪除圖書。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteBookFail'] = "0|=|刪除圖書失敗。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateBookSuccess'] = "1|=|已成功更新圖書。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateBookFail'] = "0|=|更新圖書失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteCoverImageSuccess'] = "1|=|已成功刪除圖書封面。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteCoverImageFail'] = "0|=|刪除圖書封面失敗。";
$Lang['ReadingGarden']['ReturnMsg']['BookImportFromeLibrarySuccess'] = "1|=|已成功匯入圖書。";
$Lang['ReadingGarden']['ReturnMsg']['BookImportFromeLibraryFail'] = "0|=|匯入圖書失敗。";
$Lang['ReadingGarden']['ReturnMsg']['RemoveBookAnswerSheetSuccess'] = "1|=|已成功移除圖書的答題紙。";
$Lang['ReadingGarden']['ReturnMsg']['RemoveBookAnswerSheetFail'] = "1|=|移除圖書答題紙失敗。";

$Lang['ReadingGarden']['ReturnMsg']['AddAnswerSheetTemplateSuccess'] = "1|=|已成功新增答題紙範本。";
$Lang['ReadingGarden']['ReturnMsg']['AddAnswerSheetTemplateFail'] = "0|=|新增答題紙範本失敗。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateAnswerSheetTemplateSuccess'] = "1|=|已成功更新答題紙範本。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateAnswerSheetTemplateFail'] = "0|=|更新答題紙範本失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetTemplateSuccess'] = "1|=|已成功刪除答題紙範本。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetTemplateFail'] = "0|=|刪除答題紙範本失敗。";

$Lang['ReadingGarden']['ReturnMsg']['AddAwardSchemeSuccess'] = "1|=|已成功新增獎勵。";
$Lang['ReadingGarden']['ReturnMsg']['AddAwardSchemeFail'] = "0|=|新增獎勵失敗。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateAwardSchemeSuccess'] = "1|=|已成功更新獎勵。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateAwardSchemeFail'] = "0|=|更新獎勵失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAwardSchemeSuccess'] = "1|=|已成功刪除獎勵。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAwardSchemeFail'] = "0|=|刪除獎勵失敗。";

$Lang['ReadingGarden']['ReturnMsg']['AddReadingTargetSuccess'] = "1|=|已成功新增階段目標。";
$Lang['ReadingGarden']['ReturnMsg']['AddReadingTargetFail'] = "0|=|新增階段目標失敗。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingTargetSuccess'] = "1|=|已成功更新階段目標。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingTargetFail'] = "0|=|更新階段目標失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingTargetSuccess'] = "1|=|已成功刪除階段目標。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingTargetFail'] = "0|=|刪除階段目標失敗。";

$Lang['ReadingGarden']['ReturnMsg']['AddReadingRecordSuccess'] = "1|=|已成功新增閱讀記錄。";
$Lang['ReadingGarden']['ReturnMsg']['AddReadingRecordFail'] = "0|=|新增閱讀記錄失敗。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingRecordSuccess'] = "1|=|已成功更新閱讀記錄。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingRecordFail'] = "0|=|更新閱讀記錄失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingRecordSuccess'] = "1|=|已成功刪除閱讀記錄。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingRecordFail'] = "0|=|刪除閱讀記錄失敗。";

$Lang['ReadingGarden']['ReturnMsg']['AddBookReportSuccess'] = "1|=|已成功新增閱讀報告。";
$Lang['ReadingGarden']['ReturnMsg']['AddBookReportFail'] = "0|=|新增閱讀報告失敗。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateBookReportSuccess'] = "1|=|已成功更新閱讀報告。";
$Lang['ReadingGarden']['ReturnMsg']['UpdateBookReportFail'] = "0|=|更新閱讀報告失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteBookReportSuccess'] = "1|=|已成功刪除閱讀報告。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteBookReportFail'] = "0|=|刪除閱讀報告失敗。";

$Lang['ReadingGarden']['ReturnMsg']['SubmitAnswerSheetSuccess'] = "1|=|已成功呈交工作紙。";
$Lang['ReadingGarden']['ReturnMsg']['SubmitAnswerSheetFail'] = "0|=|呈交工作紙失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetSuccess'] = "1|=|已成功刪除工作紙。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetFail'] = "0|=|刪除工作紙失敗。";

$Lang['ReadingGarden']['ReturnMsg']['SubmitOnlineWritingSuccess'] = "1|=|已成功呈交網上閱讀。";
$Lang['ReadingGarden']['ReturnMsg']['SubmitOnlineWritingFail'] = "0|=|呈交網上閱讀失敗。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteOnlineWritingSuccess'] = "1|=|已成功刪除網上閱讀。";
$Lang['ReadingGarden']['ReturnMsg']['DeleteOnlineWritingFail'] = "0|=|刪除網上閱讀失敗。";

$Lang['ReadingGarden']['ReturnMsg']['AssignSuccess'] = "1|=|已成功將已選的學生增加到得獎名單";
$Lang['ReadingGarden']['ReturnMsg']['AssignFail'] = "0|=|未能將已選的學生從得獎名單移除";
$Lang['ReadingGarden']['ReturnMsg']['UnassignSuccess'] = "1|=|已成功從得獎名單移除所選學生";
$Lang['ReadingGarden']['ReturnMsg']['UnassignFail'] = "0|=|未能從得獎名單移除所選學生";

//20110328
$Lang['ReadingGarden']['VieweBook'] = "閱讀電子圖書";

// Statistic
$Lang['ReadingGarden']['DisplayItem'] = "顯示項目";
$Lang['ReadingGarden']['Row'] = "行";
$Lang['ReadingGarden']['Column'] = "欄";
$Lang['ReadingGarden']['Month'] = "月份";
$Lang['ReadingGarden']['ISBN'] = "ISBN";
$Lang['ReadingGarden']['DisplayBookReport'] = "顯示閱讀報告完成數量";
$Lang['ReadingGarden']['DisplayNoOfMarkedReport'] = "顯示已批改閱讀報告數量";
$Lang['ReadingGarden']['SeparateByLang'] = "按語言分開顯示";
$Lang['ReadingGarden']['Ch'] = "中";
$Lang['ReadingGarden']['En'] = "英";
$Lang['ReadingGarden']['Other'] = "其他";
$Lang['ReadingGarden']['MarkedReport'] = "已批改";
$Lang['ReadingGarden']['Total'] = "所有";

$Lang['ReadingGarden']['AllowAttachment'] = "允許上載附件";
$Lang['ReadingGarden']['ExtraSetting'] = "附加設定";
$Lang['ReadingGarden']['RequireStudentToSelectAwardScheme'] = "要求學生選擇獎勵計劃";
$Lang['ReadingGarden']['Enable'] = "啟用";
$Lang['ReadingGarden']['Disable'] = "停用";
$Lang['ReadingGarden']['ReportSettings'] = "報告選項";

$Lang['ReadingGarden']['AdditionalAward'] = "附加獎項 (非排他性)";
$Lang['ReadingGarden']['DescendingOrder'] = "降序排列";
$Lang['ReadingGarden']['PrizeName'] = "獎項名稱";

$Lang['ReadingGarden']['AllAwardLevel'] = "所有獎項";
$Lang['ReadingGarden']['ApplicableCategory'] = "適用分類";
$Lang['ReadingGarden']['ConfirmArr']['YouCanOnlyAnswerOneAnswerSheet'] = "每份閱讀紀錄只可以提交一份工作紙，你曾作答過的另一份工作紙答案將被刪除，繼續？";
$Lang['ReadingGarden']['ConfirmArr']['TheModificationOfTheAnswerSheetWillBeDeleted'] = "此前對這工作紙上所作出的更改將被刪除，繼續？";

$Lang['ReadingGarden']['FileUploadDesc'] = "上傳檔案指引";
$Lang['ReadingGarden']['OnlineWritingDesc'] = "網上寫作指引";
$Lang['ReadingGarden']['ActiveStatus'][1] = "使用";
$Lang['ReadingGarden']['ActiveStatus'][0] = "停用";

// Popular Reading Award Scheme
 
$Lang['ReadingGarden']['PopularAwardScheme'] = "普及閱讀獎勵計劃";
$Lang['ReadingGarden']['PopularAwardSchemeMgmt']['MenuTitle'] = "普及閱讀獎勵計劃"; //menu name//
$Lang['ReadingGarden']['PopularAwardSchemeSettings']['MenuTitle'] = "普及閱讀獎勵計劃"; //menu name//
$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardSetting'] = "獎章";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['Award'] = "獎章";  
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoringItem'] = "計分項目";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'] = "分類";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryCode'] = "分類編號";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryNameEn'] = "分類名稱 (Eng)";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryNameCh'] = "分類名稱 (中文)";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryName'] = "分類名稱";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['Item'] = "計分項目"; 
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDesc'] = "項目描述";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemCode'] = "項目編號";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDescEn'] = "項目描述 (Eng)";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDescCh'] = "項目描述 (中文)";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['Score'] = "分數";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['SubmitByStudent'] = "學生可自行輸入";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['AddItem'] = "新增項目";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['DeleteItem'] = "刪除項目";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['MoveItem'] = "移動項目"; 
$Lang['ReadingGarden']['PopularAwardSchemeArr']['EditItem'] = "修改項目";  
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategorySetting'] = "分類設定";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['DeleteCategory'] = "刪除分類";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord'] = "新增普及閱讀獎勵計劃紀錄";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['ViewStudentScore'] = "查看學生分數";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['NewAward'] = "新增獎章";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardName'] = "獎章名稱";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardRequirement'] = "附加條件";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoreRequired'] = "分數要求";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryIncluded'] = "包括分類";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['Times'] = "次數";

# Import Award
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ImportAwardRecord'] = "匯入獎章紀錄";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'] = array("班別名稱","學號","用戶登入名稱","項目編號","次數");

$Lang['ReadingGarden']['Import_Error']['ItemCodeIsMissing'] = "項目編號不能留空";
$Lang['ReadingGarden']['Import_Error']['TimesIsMissing'] = "次數不能留空";
$Lang['ReadingGarden']['Import_Error']['InvalidItemCode'] = "項目編號不正覺";
$Lang['ReadingGarden']['Import_Error']['InvalidTimes'] = "次數不正覺";

$Lang['SysMgr']['ReadingGarden']['ImportAwardRecord']['UserInfoRemarks'] = "請填寫「" . $Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][0] ."」及「" . $Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][1] . "」或只填寫「" . $Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][2] ."」";


$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsConfirmArr']['DeleteCategory'] = "你確定要刪除這此分類？";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsConfirmArr']['DeleteItem'] = "你確定要刪除這此項目？";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryCode'] = "分類編號空白";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryName'] = "分類名稱空白";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CodeUnavailable'] = "編號已被使用";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputItemCode'] = "請輸入項目編號。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['ItemCodeExist'] = "項目編號已被使用。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputItemName'] = "請輸入項目描述。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputScore'] = "請輸入分數。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputNumber'] = "請輸入項目數字。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseSelectItem'] = "請選擇項目。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseSelectCategory'] = "請選擇分類。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseSelectAtLeastOneCategory'] = "請選擇最少一個分類。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['DuplicatedRequirement'] = "重複選擇分類組合。";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputAwardName'] = "請輸入獎章名稱。";

// Reading Award Scheme
$Lang['ReadingGarden']['ReadingAwardScheme'] = "閱讀獎勵計劃";

$Lang['ReadingGarden']['ReturnMessage']['AddCategorySuccess'] = "1|=|已成功新增分類。";
$Lang['ReadingGarden']['ReturnMessage']['AddCategoryFail'] = "0|=|新增分類失敗。";
$Lang['ReadingGarden']['ReturnMessage']['UpdateCategorySuccess'] = "1|=|已成功修改分類。";
$Lang['ReadingGarden']['ReturnMessage']['UpdateCategoryFail'] = "0|=|修改分類失敗。";
$Lang['ReadingGarden']['ReturnMessage']['DeleteCategorySuccess'] = "1|=|已成功刪除分類。";
$Lang['ReadingGarden']['ReturnMessage']['DeleteCategoryFail'] = "0|=|刪除分類失敗。";
$Lang['ReadingGarden']['ReturnMessage']['UpdateCategoryOrderSuccess'] = "1|=|已成功重新排列分類。";
$Lang['ReadingGarden']['ReturnMessage']['UpdateCategoryOrderFail'] = "0|=|重新排列分類失敗。";

$Lang['ReadingGarden']['ReturnMessage']['AddItemSuccess'] = "1|=|已成功新增項目。";
$Lang['ReadingGarden']['ReturnMessage']['AddItemFail'] = "0|=|新增項目失敗。";
$Lang['ReadingGarden']['ReturnMessage']['UpdateItemSuccess'] = "1|=|已成功修改項目。";
$Lang['ReadingGarden']['ReturnMessage']['UpdateItemFail'] = "0|=|修改項目失敗";
$Lang['ReadingGarden']['ReturnMessage']['DeleteItemSuccess'] = "1|=|已成功刪除項目。";
$Lang['ReadingGarden']['ReturnMessage']['DeleteItemFail'] = "0|=|刪除項目失敗。";
$Lang['ReadingGarden']['ReturnMessage']['UpdateItemOrderSuccess'] = "1|=|已成功重新排列項目。";
$Lang['ReadingGarden']['ReturnMessage']['UpdateItemOrderFail'] = "0|=|重新排列項目失敗。";

$Lang['ReadingGarden']['ReturnMessage']['AddPopularAwardSchemeRecordSuccess'] = "1|=|已成功新增普及閱讀獎勵計劃紀錄。";
$Lang['ReadingGarden']['ReturnMessage']['AddPopularAwardSchemeRecordFail'] = "0|=|新增普及閱讀獎勵計劃紀錄失敗。";

$Lang['ReadingGarden']['ReturnMessage']['AddAwardSuccess'] = "1|=|已成功新增獎章。";
$Lang['ReadingGarden']['ReturnMessage']['AddAwardFail'] = "0|=|新增獎章失敗。";
$Lang['ReadingGarden']['ReturnMessage']['EditAwardSuccess'] = "1|=|已成功修改獎章。";
$Lang['ReadingGarden']['ReturnMessage']['EditAwardFail'] = "0|=|修改獎章失敗。";
$Lang['ReadingGarden']['ReturnMessage']['DeleteAwardSuccess'] = "1|=|已成功刪除獎章。";
$Lang['ReadingGarden']['ReturnMessage']['DeleteAwardFail'] = "0|=|刪除獎章失敗。";


//if (!$NoLangWordings && is_file("$intranet_root/templates/lang_reading_garden.$intranet_session_language.customized.php"))
//{
//  include("$intranet_root/templates/lang_reading_garden.$intranet_session_language.customized.php");
//}



?>