<?php
// editing by 
// UTF8 萬國碼

$Lang['ClassDiary']['ClassDiary'] = "Class Diary";
$Lang['ClassDiary']['Management'] = "Management";
$Lang['ClassDiary']['Report'] = "Report";
$Lang['ClassDiary']['Statistics'] = "Statistics";
$Lang['ClassDiary']['ExportMailMerge'] = "Data Export";

$Lang['ClassDiary']['RecordDate'] = "Record date";
$Lang['ClassDiary']['Class'] = "Class";
$Lang['ClassDiary']['Creator'] = "Creator";
$Lang['ClassDiary']['LastModifiedBy'] = "Last modified by";
$Lang['ClassDiary']['LastModifiedDate'] = "Last modified date";
$Lang['ClassDiary']['ViewRecordDetail'] = "View record detail";

$Lang['ClassDiary']['Cycle'] = "Cycle";
$Lang['ClassDiary']['Date'] = "Date";
$Lang['ClassDiary']['Subject'] = "Subject";
$Lang['ClassDiary']['MediumOfInstruction'] = "Medium of Instruction";
$Lang['ClassDiary']['Assignment'] = "Assignment";
$Lang['ClassDiary']['DateOfSubmission'] = "Date of Submission";
$Lang['ClassDiary']['ForgotToBringBooks'] = "Forgot to bring books or stationery required";
$Lang['ClassDiary']['StudentsExcuseLessons'] = "Students permited to be excused between lessons";
$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'] = "Students not adopting an appropriate medium of instruction";
$Lang['ClassDiary']['Others'] = "Others";
$Lang['ClassDiary']['Absentee'] = "Absentee";
$Lang['ClassDiary']['AM'] = "(a.m.)";
$Lang['ClassDiary']['PM'] = "(p.m.)";
$Lang['ClassDiary']['Late'] = "Late";
$Lang['ClassDiary']['EarlyLeave'] = "Early Leave";
$Lang['ClassDiary']['TeachersSignature'] = "Teacher's Signature";
$Lang['ClassDiary']['Remarks'] = "Remarks";
$Lang['ClassDiary']['SignatureOfClassMonitor'] = "Signature of Class Monitor";
$Lang['ClassDiary']['SignatureOfClassTeacher'] = "Signature of Class Teacher";
$Lang['ClassDiary']['RemarksDescription'][] = "Class monitor must keep this diary well and remind the subject teacher to sign and note down the misbehaved students.";
$Lang['ClassDiary']['RemarksDescription'][] = "Class teacher must return this diary to the office.";
$Lang['ClassDiary']['RemarksDescription'][] = "Teachers of Student Affairs Committee will follow up the cases recorded on this sheet.";
$Lang['ClassDiary']['RemarksDescription'][] = "Write the class number instead of student's name.";
$Lang['ClassDiary']['MediumOfInstructionMapping']['P'] = "Putonghua";
$Lang['ClassDiary']['MediumOfInstructionMapping']['E'] = "English";
$Lang['ClassDiary']['MediumOfInstructionMapping']['C'] = "Cantonese";
$Lang['ClassDiary']['MediumOfInstructionMapping']['B'] = "Bilingual";
$Lang['ClassDiary']['SelectStudents'] = "Select students";
$Lang['ClassDiary']['Loading'] = "Loading...";
$Lang['ClassDiary']['Lesson'] = "Lesson";
$Lang['ClassDiary']['Lesson_Session'] = "Lesson";
$Lang['ClassDiary']['RecordType'] = "Record type";
$Lang['ClassDiary']['CountTimesOfRecordType'] = "Count number of times of record type";
$Lang['ClassDiary']['Times'] = "Times";
$Lang['ClassDiary']['ClassNumber'] = "Class number";
$Lang['ClassDiary']['StudentName'] = "Student name";
$Lang['ClassDiary']['ShowDates'] = "Show dates";
$Lang['ClassDiary']['Children'] = "Children";
$Lang['ClassDiary']['Edit_Class_Attendance'] = "Edit Attendance Records";
$Lang['ClassDiary']['Discard_Edit_Class_Attendance'] = "Discard Attendance Edit";

$Lang['ClassDiary']['WarningMsg']['SelectClass'] = "Please select a class.";
$Lang['ClassDiary']['WarningMsg']['InvalidDate'] = "Pleae input a valid date.";
$Lang['ClassDiary']['WarningMsg']['SelectSubject'] = "Please select a subject.";
$Lang['ClassDiary']['WarningMsg']['InputAssignment'] = "Please input assignment.";
$Lang['ClassDiary']['WarningMsg']['DuplicatedClassDiary'] = "Class <!--CLASSNAME--> already has diary record on <!--DATE-->.";
$Lang['ClassDiary']['WarningMsg']['ConfirmDeleteClassDiary'] = "Are you sure want to delete this class diary?";
$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneClass'] = "Please select at least one class.";
$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneRecordType'] = "Please select at least one record type.";
$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneMediumOfInstruction'] = "Please select at least one medium of instruction.";
$Lang['ClassDiary']['WarningMsg']['FirstMonth'] = "You may select across months but the first month will be displayed on the CSV.";



$Lang['ClassDiary']['Month'][1] = "一";
$Lang['ClassDiary']['Month'][2] = "二";
$Lang['ClassDiary']['Month'][3] = "三";
$Lang['ClassDiary']['Month'][4] = "四";
$Lang['ClassDiary']['Month'][5] = "五";
$Lang['ClassDiary']['Month'][6] = "六";
$Lang['ClassDiary']['Month'][7] = "七";
$Lang['ClassDiary']['Month'][8] = "八";
$Lang['ClassDiary']['Month'][9] = "九";
$Lang['ClassDiary']['Month'][10] = "十";
$Lang['ClassDiary']['Month'][11] = "十一";
$Lang['ClassDiary']['Month'][12] = "十二";
?>