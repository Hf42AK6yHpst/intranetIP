<?php
/** [Modification Log] Modifying By:  **/
// a
$Lang['W2']['addMoreQuestion'] = '加入問題';
$Lang['W2']['addMore'] = '加入更多';
$Lang['W2']['allWriting'] = '全部練習';
$Lang['W2']['alreadyCommented'] = '老師已評核';
$Lang['W2']['approvalIsNeeded'] = '需要批核';
$Lang['W2']['advanced'] = 'Advanced';
$Lang['W2']['addComment'] = '加入評語';
$Lang['W2']['allGrades'] = '所有年級';
$Lang['W2']['answer'] = '答案';
$Lang['W2']['attachment'] = '附件';

// b
$Lang['W2']['basic'] = 'Basic';
$Lang['W2']['brainstorming'] = 'Brainstorming';
$Lang['W2']['btn']['go'] = '前往';
// c
$Lang['W2']['category'] = '類型';
$Lang['W2']['check'] = '檢察';
$Lang['W2']['checkPoint'] = '引導問題';
$Lang['W2']['chiMyWriting'] = '我的寫作';
$Lang['W2']['className'] = '班別';
$Lang['W2']['ClassNo'] = "班號";
$Lang['W2']['clickHere'] = '按此';
$Lang['W2']['completed'] = '已完成';
$Lang['W2']['completedDate'] = '完成日期';
$Lang['W2']['completedWriting'] = '已完成寫作';
$Lang['W2']['conceptMap'] = '腦圖';
$Lang['W2']['copy'] = '複製';
$Lang['W2']['comments'] = '評語';
$Lang['W2']['comment'] = '評核';
$Lang['W2']['conceptMap'] = '腦圖';
$Lang['W2']['conceptMapInstruction'] = '你可運用以下腦圖幫助思考。你可以更改腦圖內的資料，以及增加或減少腦圖的支線。<br /><br />';
$Lang['W2']['content'] = '內容';
$Lang['W2']['contentInput'] = '內容設定';
$Lang['W2']['contentName'] = '主題';
$Lang['W2']['contentPro'] = '內容供應';
$Lang['W2']['createDate'] = '製作日期';
$Lang['W2']['createdBy'] = '作者';
$Lang['W2']['creator'] = '設定者';

// d
$Lang['W2']['datePublished'] = '發佈日期';
$Lang['W2']['deleteConfirmed'] = '是否確定刪除此題目?';
$Lang['W2']['details'] = '內容';
$Lang['W2']['draft'] = '第 <-version-> 稿';
$Lang['W2']['data1Title'] = '資料一標題';
$Lang['W2']['data2Title'] = '資料二標題';
// e
$Lang['W2']['eachStudentCanMark'] = '每人評改';
$Lang['W2']['eachStudentMarksEveryone'] = '每人評改所有同學';
$Lang['W2']['evaluated'] = '已評核';
$Lang['W2']['exampleSentence'] = '例句';
$Lang['W2']['exerciseName'] = '練習名稱';
$Lang['W2']['expectedCompletionDate'] = '預計完成日期';
$Lang['W2']['exportAll'] = '匯出全部';
$Lang['W2']['exportCSV'] = '匯出CSV檔案';
$Lang['W2']['enterText']='輸入文字';
// f
$Lang['W2']['fullmark'] = '滿分';
// g
$Lang['W2']['group(s)'] = '組';
$Lang['W2']['groupNow'] = '進行分組';
$Lang['W2']['grammar'] = '文法';
$Lang['W2']['giveSticker'] = '贈送貼紙';
$Lang['W2']['grade'] = '年級';
$Lang['W2']['grammarAnalysis'] = '修辭技巧說明';
$Lang['W2']['grammarAnalysisForEngine'] = '修辭技巧說明';

// h
$Lang['W2']['home'] = '首頁';
$Lang['W2']['highestMark'] = '最高分數';

// i
$Lang['W2']['incomplete'] = '未完成';
$Lang['W2']['incompleteWriting'] = '未完成寫作';
$Lang['W2']['introduction'] = '簡介';
$Lang['W2']['instruction'] = '指示';
$Lang['W2']['imageCaption'] = '圖像說明';
$Lang['W2']['imageFile'] = '圖像檔案';
$Lang['W2']['issueAnalysis'] = '議題分析';
// j
$Lang['W2']['jsWarningAry']['deleteVocab'] = '你是否確定要刪除此生字？';
$Lang['W2']['jsWarningAry']['deleteVocabGroup'] = '所有此組別的生字將會一拼刪除。你是否確定要刪除此生字組別？';
$Lang['W2']['jsWarningAry']['fullMarkMustBePositive'] = '滿分必須為正數。';
$Lang['W2']['jsWarningAry']['passMarkMustBePositive'] = '及格分數必須為正數。';
$Lang['W2']['jsWarningAry']['lowestMarkMustBePositive'] = '最低分數必須為正數。';
$Lang['W2']['jsWarningAry']['weighingFormat'] = '比重一定是整數';
$Lang['W2']['jsWarningAry']['alertTotalWeight'] = '比重總和必須等於100';
$Lang['W2']['jsWarningAry']['deleteAttachment'] = '你是否確定要刪除此附件？';
$Lang['W2']['jsWarningAry']['deleteImage'] = '你是否確定要刪除此圖片？';
$Lang['W2']['jsWarningAry']['attachmentNameCannotBeBlank'] = '檔案名稱不能留空。';
$Lang['W2']['jsWarningAry']['topicCannotBeBlank'] = '標題不能留空。';
$Lang['W2']['jsWarningAry']['pleaseSelectReferenceType'] = '請選擇參考類型。';
$Lang['W2']['jsWarningAry']['wrongImageFormat'] = '圖像格式不正確。';
$Lang['W2']['jsWarningAry']['contentNotSaved'] = '如要離開此頁面，所有已更改的資料將不被儲存。你是否確定要離開此頁面？';
// k
$Lang['W2']['keywordMeaning'] = '關鍵字的含意';
$Lang['W2']['keywords'] = '關鍵字';
// l
$Lang['W2']['lastUpdateDate'] = '上次更新';
$Lang['W2']['level'] = 'Level';
$Lang['W2']['lowestmark'] = '最低分數';
$Lang['W2']['learningTips'] = '學習提示';
$Lang['W2']['LSThemeAry'] = array();
$Lang['W2']['LSThemeAry'][] = '市區重建';
$Lang['W2']['LSThemeAry'][] = '土地利用';
$Lang['W2']['LSThemeAry'][] = '屏風效應';
$Lang['W2']['LSThemeAry'][] = '香港經濟';
$Lang['W2']['LSThemeAry'][] = '農民工';
$Lang['W2']['LSThemeAry'][] = '中國軟實力';
$Lang['W2']['LSThemeAry'][] = '香港污染問題';
$Lang['W2']['LSThemeAry'][] = '全球暖化';
$Lang['W2']['LSThemeAry'][] = '全球化：經濟範疇';
$Lang['W2']['LSThemeAry'][] = '全球化：文化範疇';
$Lang['W2']['LSThemeAry'][] = '全球化：政府範疇';
$Lang['W2']['LSThemeAry'][] = '全球化：非政府範疇';
$Lang['W2']['lsMyWriting'] = '我的寫作';
// m
$Lang['W2']['management'] = '管理';
$Lang['W2']['markingMethod'] = '評核方法';
$Lang['W2']['markNow'] = '現在評核';
$Lang['W2']['myWriting'] = '我的練習';
$Lang['W2']['myDraft'] = 'My Draft';
$Lang['W2']['markUsing'] = '評改方式';
$Lang['W2']['mark'] = '分數';
$Lang['W2']['marks'] = '分數';
$Lang['W2']['marker'] = '評核者';
//$Lang['W2']['marker'] = '評改者'; ###
$Lang['W2']['moreVocabulary'] = '參考詞語';
$Lang['W2']['moreVocabularyForEngine'] = '參考詞語';
$Lang['W2']['myApproach'] = '我的手法';
$Lang['W2']['myConcept'] = '我的構思';
$Lang['W2']['myMindMap'] = '我的腦圖';
$Lang['W2']['myOutline'] = '我的大綱';


// n
$Lang['W2']['na'] = 'N/A';
$Lang['W2']['na_without_slash'] = 'NA';
$Lang['W2']['name'] = '名稱';
$Lang['W2']['needCheckComment'] = '已給予評語';
$Lang['W2']['needComment'] = '需要評核';
//$Lang['W2']['needComment'] = '需要給評語'; ###
$Lang['W2']['needProvideComment'] = '等待老師評語中';
$Lang['W2']['needToRedo'] = '需要重做';
$Lang['W2']['noDefaultConceptMap'] = '沒有預設腦圖';
$Lang['W2']['newTemplate'] = '新增範本';
$Lang['W2']['nameOfAttachment'] = '附件名稱';
$Lang['W2']['newParagraph'] = '新增段落';
$Lang['W2']['newSection'] = '新增分段';
$Lang['W2']['NumberOfStudents'] = '學生數目';

// o
$Lang['W2']['outline'] = '內容大要';
// p
$Lang['W2']['paperTemplate'] = '背景設計';
$Lang['W2']['powerConcept'] = 'PowerConcept';
$Lang['W2']['peer'] = '同儕';
//$Lang['W2']['peer'] = '互評'; ###
$Lang['W2']['peerComment'] = '同學評語';
$Lang['W2']['peerCommented'] = '同學已評核';
$Lang['W2']['peerMarking'] = '互評';
$Lang['W2']['peerMarkingSettings'] = '互評設定';
$Lang['W2']['pointsOfContention'] = '爭議點';
$Lang['W2']['powerConcept'] = 'PowerConcept';
$Lang['W2']['publish'] = '發佈';
$Lang['W2']['published'] = '已發佈';
$Lang['W2']['publishAll'] = '發佈予全部學校';
$Lang['W2']['publishPart'] = '發佈予個別學校';
$Lang['W2']['passmark'] = '合格分數';
$Lang['W2']['pleaseSelect'] = '- 請選擇 -';
$Lang['W2']['powerBoard'] = 'PowerBoard';
$Lang['W2']['paragraph'] = '段落';
$Lang['W2']['projAck'] = '鳴謝';
$Lang['W2']['partSch'] = '參與學校';
$Lang['W2']['projPart'] = '計劃協作夥伴';
// q
$Lang['W2']['question'] = '問題';

// r
$Lang['W2']['relatedInformation'] = '參考資料';
$Lang['W2']['relatedTermsAndInformation'] = '相關詞彙及資料';
$Lang['W2']['report'] = '報告';
$Lang['W2']['returnMsgArr']['peerCommentUpdateSuccess'] = '1|=|更新同學評語成功';
$Lang['W2']['returnMsgArr']['peerCommentUpdateFailed'] = '0|=|更新同學評語失敗';
$Lang['W2']['returnMsgArr']['teacherCommentUpdateSuccess'] = '1|=|更新老師評語成功';
$Lang['W2']['returnMsgArr']['teacherCommentUpdateFailed'] = '0|=|更新老師評語失敗';
$Lang['W2']['returnMsgArr']['vocabGroupDeleteSuccess'] = '1|=|刪除生字組別成功';
$Lang['W2']['returnMsgArr']['vocabGroupDeleteFailed'] = '0|=|刪除生字組別失敗';
$Lang['W2']['returnMsgArr']['vocabGroupUpdateSuccess'] = '1|=|更新生字組別成功';
$Lang['W2']['returnMsgArr']['vocabGroupUpdateFailed'] = '0|=|更新生字組別失敗';
$Lang['W2']['returnMsgArr']['vocabUpdateSuccess'] = '1|=|更新生字成功';
$Lang['W2']['returnMsgArr']['vocabUpdateFailed'] = '0|=|更新生字失敗';
$Lang['W2']['returnMsgArr']['grammarUpdateSuccess'] = '1|=|1|=|更新文法分析成功';
$Lang['W2']['returnMsgArr']['grammarUpdateFailed'] = '0|=|更新文法分析失敗';
$Lang['W2']['returnMsgArr']['updateSuccess'] = '1|=|更新成功';
$Lang['W2']['returnMsgArr']['updateFailed'] = '0|=|更新失敗';
$Lang['W2']['reference'] = '參考資料';
$Lang['W2']['referenceTitle'] = '參考標題';
$Lang['W2']['referenceType'] = '參考類型';
$Lang['W2']['refWebsite'] = '參考網頁';
$Lang['W2']['revisedDraft'] = '第 <-version-> 稿';
$Lang['W2']['RedoSent'] = '已發還重做';
$Lang['W2']['resource'] = '資源';
$Lang['W2']['readMore'] = '更多...';
// s
$Lang['W2']['sampleWriting'] = '參考文章';
$Lang['W2']['sampleWritingApproach'] = '參考寫作手法';
$Lang['W2']['saveAndNextStep'] = '儲存及下一步';
$Lang['W2']['saveAsDraft'] = '儲存草稿';
$Lang['W2']['section'] = '分段';
$Lang['W2']['setMarker'] = '設定評核者';
$Lang['W2']['settingTheScopeOfTheAnswer'] = '思考答題方向';
$Lang['W2']['shareDate'] = '分享日期';
$Lang['W2']['shareThisWriting'] = '分享這文章';
$Lang['W2']['shareThisWritingHeader'] = '文章分享';
$Lang['W2']['shareWritingShared'] = '文章已分享';
$Lang['W2']['shareWritingUnshare'] = '取消分享文章';
$Lang['W2']['shareRubrics'] = '共享評量指標';
$Lang['W2']['shareWriting'] = '文章分享';
$Lang['W2']['showDefaultConceptMap'] = '顯示預設腦圖';
$Lang['W2']['showPeersNameToMarker'] = '顯示被評改學生姓名';
$Lang['W2']['steps'] = '步驟';
$Lang['W2']['stepApprovalSetting'] = '步驟批核設定';
$Lang['W2']['stepApprovalSettingNoApproval'] = '無需批核';
$Lang['W2']['stepApprovalSettingRequireApproval'] = '需要批核';
$Lang['W2']['student(s)'] = '名同學';
$Lang['W2']['studentName'] = '學生名稱';
$Lang['W2']['studentsAreDiviedInto'] = '學生分為';
$Lang['W2']['submissionDate'] = '呈交日期';
//$Lang['W2']['submissionDate'] = '遞交日期'; ###
$Lang['W2']['submissionTime'] = '呈交時間';
//$Lang['W2']['submissionTime'] = '遞交時間'; ###
$Lang['W2']['submitted'] = '已呈交';
$Lang['W2']['step'] = '步驟';
$Lang['W2']['step1_eng'] = 'Examining the Qs';
$Lang['W2']['step2_eng'] = 'Brainstorming';
$Lang['W2']['step3_eng'] = 'Sample Writing';
$Lang['W2']['step4_eng'] = 'Drafting';
$Lang['W2']['step5_eng'] = 'Writing';
$Lang['W2']['step1_chi'] = '步驟一<br />〈審題立意〉';
$Lang['W2']['step2_chi'] = '步驟二<br />〈內容構思〉';
$Lang['W2']['step3_chi'] = '步驟三<br />〈擬寫大綱〉';
$Lang['W2']['step4_chi'] = '步驟四<br />〈確定寫作手法和字詞〉';
$Lang['W2']['step5_chi'] = '步驟五<br />〈撰寫文章〉';
$Lang['W2']['step1_ls'] = '步驟一<br />〈探究議題〉';
$Lang['W2']['step2_ls'] = '步驟二<br />〈相關詞彙及資料〉';
$Lang['W2']['step3_ls'] = '步驟三<br />〈思考答題方向〉';
$Lang['W2']['step4_ls'] = '步驟 四<br />〈草擬答案〉';
$Lang['W2']['step5_ls'] = '步驟 五<br />〈作答問題〉';
$Lang['W2']['step1Title']['chi'] = '審題立意';
$Lang['W2']['step2Title']['chi'] = '內容構思';
$Lang['W2']['step3Title']['chi'] = '擬寫大綱';
$Lang['W2']['step4Title']['chi'] = '確定寫作手法和字詞';
$Lang['W2']['step5Title']['chi'] = '撰寫文章';
$Lang['W2']['step1Title']['ls'] = '探究議題';
$Lang['W2']['step2Title']['ls'] = '相關詞彙及資料';
$Lang['W2']['step3Title']['ls'] = '思考答題方向';
$Lang['W2']['step4Title']['ls'] = '草擬答案';
$Lang['W2']['step5Title']['ls'] = '作答問題';

$Lang['W2']['shortQuestions'] = '短答題';
$Lang['W2']['step2ReferenceTypeAry']['normal'] = '一般資料';
$Lang['W2']['step2ReferenceTypeAry']['compare'] = '正反比較';
$Lang['W2']['status'] = '狀態';

$Lang['W2']['subjectFileCode']['chi'] = 'Chi';
$Lang['W2']['subjectFileCode']['eng'] = 'Eng';
$Lang['W2']['subjectFileCode']['sci'] = 'Sci';
$Lang['W2']['subjectFileCode']['ls'] = 'LS(Chi)';
$Lang['W2']['subjectFileCode']['ls_eng'] = 'LS(Eng)';

$Lang['W2']['selectSticker'] = '選擇貼紙';
$Lang['W2']['source'] = '資料';
$Lang['W2']['stickers'] = '貼紙';
$Lang['W2']['seeMore'] = '顯示更多';
$Lang['W2']['scoreOutOfRange'] = '分數超出範圍';
$Lang['W2']['structure'] = '結構';
$Lang['W2']['systemDev'] = '系統開發';

$Lang['W2']['standard'] = '標準';

// t
$Lang['W2']['target'] = '對象';
$Lang['W2']['teacherCommented'] = '老師已評核';
$Lang['W2']['teacherCommentSoon'] = '老師即將給評語';
$Lang['W2']['teachersComment'] = '老師評語';
$Lang['W2']['termsAndInformation'] = '詞彙及資料';
$Lang['W2']['theme'] = '類型';
$Lang['W2']['toBeEvaluated'] = '未評核';
$Lang['W2']['toMark'] = '需批改';
$Lang['W2']['totalSubmit'] = '呈交次數';
$Lang['W2']['totalAttempt'] = '嘗試總數';
$Lang['W2']['totalStudents'] = '學生總數';
$Lang['W2']['topic'] = '題目';
$Lang['W2']['topicName'] = '題目';
$Lang['W2']['topicKeyword'] = '題目關鍵字';
$Lang['W2']['textType'] = '體裁';
$Lang['W2']['textTypeAry'] = array('1'=>'遊記','2'=>'記敍文','3'=>'描寫文','4'=>'抒情文','5'=>'說明文','6'=>'議論文'); 
$Lang['W2']['textTypeChoiceAry'] = array('1'=>'記敍','2'=>'描寫','3'=>'抒情','4'=>'說明','5'=>'議論'); 
$Lang['W2']['teacher'] = '老師';
$Lang['W2']['teacherAttachment'] = '老師附件';
$Lang['W2']['teacherAttachmentWithBreak'] = '老師附件';
$Lang['W2']['topicAndTheme_chi'] = '主題';
$Lang['W2']['topicAnalysis'] = '題目分析';
$Lang['W2']['title'] = '標題';

$Lang['W2']['template']['Wallpaper'] = '樣章設計';
$Lang['W2']['template']['Clipart'] = '美工圖案';
$Lang['W2']['template']['Paper'] = '紙張';
$Lang['W2']['template']['Email'] = '電郵';
$Lang['W2']['template']['Panda'] = '大熊貓';

$Lang['W2']['template']['Squirrel'] = '松鼠';
$Lang['W2']['template']['Sketchbook'] = '畫簿';
$Lang['W2']['template']['Robot'] = '機械人';
$Lang['W2']['template']['Film'] = '電影';
$Lang['W2']['template']['Leaflet'] = '單張';

$Lang['W2']['template']['Postbox'] = '郵箱';
$Lang['W2']['template']['News'] = '新聞';
$Lang['W2']['template']['Letter'] = '信件';
$Lang['W2']['template']['Friends'] = '朋友';
$Lang['W2']['template']['Cooking'] = '烹飪';

$Lang['W2']['template']['ChineseWriting'] = '寫作';
$Lang['W2']['template']['ChineseTravel'] = '中國旅遊';
$Lang['W2']['template']['Travel'] = '旅遊';
$Lang['W2']['template']['City'] = '城市';
$Lang['W2']['template']['Coast'] = '海岸';

$Lang['W2']['template']['EssaySample'] = "
<div class=\"essay\">
	<center><i>- 中文寫作範文 -</i></center><br />
	<div><center>人見人愛的小女孩</center></div><br />
	<div>
		<div>
　　「姨姨，早安。你看我今天漂不漂亮？你猜猜我今天要到哪裏去？」一把稚嫩清脆的童音自電梯大堂響起。「早安，子瑤。你活像一位小公主呢！今天你要去哪兒？」保安員姨姨問道。「媽媽今天帶我到主題樂園玩！我要和米妮老鼠拍照，回來時給你看吧！」子瑤邊轉圈邊回答說。
<br /><br />
　　那穿着紅底白點花裙子的小女孩名叫子瑤，是我的鄰居，今年六歲。子瑤擁有一張標緻的臉容，圓圓的臉龐上，貼着一雙水靈靈的眼睛，扁塌的鼻子，薄薄的嘴脣，配上一對小酒渦，笑的時候教人連心也融化掉。她齒如齊貝，唯獨缺了一隻門牙，使她說話時有點口齒不清，更添可愛。子瑤總是梳着兩條孖辮，瀏海貼服地排在額前，據說是她媽媽每天悉心為她梳理的招牌髮型。
<br /><br />
　　別看子瑤的外表斯文，就以為她是個文靜的小女孩，其實她表情豐富，經常弄出很多古怪的神情，逗得附近的鄰居忍俊不禁。一次，她從遠處看見我，便扯着大嗓子叫道：「姐姐，你看，我的圖畫被老師讚賞呢！」原來她也想得到我的讚賞呢！她說話時眉飛色舞，手舞足蹈般，讓人覺得她是位天生的表演者。
<br /><br />
　　子瑤可謂是我們這座大廈的開心果，希望她能永遠保持着那純真的個性。
		</div>
  	</div>
</div>
";

// u
$Lang['W2']['uploadFileName'] = '檔案名稱';
$Lang['W2']['uploadFileSource'] = '來源';
$Lang['W2']['uploadFileCaption'] = '上載檔案';
$Lang['W2']['uploadFileMore'] = '加入';
$Lang['W2']['unselectalert'] = '請選擇寫作方案';

// v
$Lang['W2']['viewSource'] = '查看資料';
$Lang['W2']['vocabularyPool'] = '我的字庫';
$Lang['W2']['vocabulary'] = '詞語';
// w
$Lang['W2']['waitingForApproval'] = '等候批核';
$Lang['W2']['waitingTeacherComment'] = '等待老師評語中';
$Lang['W2']['warningMsgArr']['deleteWriting'] = '所有已呈交的練習將被同時刪除。你是否確定你要刪除此練習？';
$Lang['W2']['warningMsgArr']['modifyPeerMarkingWarning'] = '更改分組將令現存的互評成績失效，你亦需要重新進行互評。';
$Lang['W2']['warningMsgArr']['mustUpdatePeerMarkingSettings'] = '因學生名單已更改，你必須更新有關的互評設定。';
$Lang['W2']['warningMsgArr']['selectSchemeCode'] = '請選擇至少一項題目。';
$Lang['W2']['warningMsgArr']['selectStudentBeforeApplyingPeerMarking'] = '請於設定互評前選擇最少兩位學生至本練習。';
$Lang['W2']['warningMsgArr']['selectCat'] = '請選擇體裁。';
$Lang['W2']['warningMsgArr']['selectAtLeastOneCat'] = '請選擇至少一個體裁。';
$Lang['W2']['markingWeight'] = '比重';
$Lang['W2']['WebSAMSCode'] = 'WebSAMS 代號';
$Lang['W2']['website'] = '網址';
$Lang['W2']['word'] = '字';
$Lang['W2']['wordCount'] = '字數';
$Lang['W2']['writingApproach'] = '寫作手法';
$Lang['W2']['writingEnd'] = '的文章';

$Lang['W2']['confirmGSGameScoreSave'] = '如已確認分數後有所更改，更改的分數將不會於太空環保基地使用。確認?';//20140529-w2gsgame

// x

// y
$Lang['W2']['yourComment'] = '你的評語';

// z
?>