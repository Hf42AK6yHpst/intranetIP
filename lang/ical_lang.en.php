<?php
# Editing by 

######### ESF customization start #########
$i_Calendar_Today = "Today";
$i_Calendar_NextSevenDays = "Next 7 Days";
$i_Calendar_ThisWeek = "This Week";

$i_Calendar_Edit = "Edit";
$i_Calendar_Delete = "Delete";
$i_Calendar_firstDay = "st";
$i_Calendar_secondDay = "nd";
$i_Calendar_thirdDay = "rd";
$i_Calendar_otherDay = "th";
########## ESF customization end ##########

$i_Calendar_Undo = "Undo";
$i_Calendar_Apply = "Apply";
$i_Calendar_Am = "am";
$i_Calendar_Pm = "pm";
$i_Calendar_At = "starts at";
$i_Calendar_RemoveAll = "Remove All";
$i_Calendar_UndoAll = "Undo All";
$i_Calendar_Loading = "Loading...";
$i_Calendar_Synchronizing = "Synchronizing... from Google to TG";
//$i_Calendar_InvalidSearch_NoKeyword = "Invalid search - Please enter keywords";
$i_Calendar_InvalidSearch_NoKeyword = "Unable to search. Please enter a search keyword.";
$i_Calendar_NoCalSelect = "Please select at least one calendar.";

$i_Calendar_Subject = "Subject";
$i_Calendar_StartDate = "Start Date";
$i_Calendar_StartTime = "Start Time";
$i_Calendar_EndDate = "End Date";
$i_Calendar_EndTime = "End Time";
$i_Calendar_AllDayEvent = "All Day Event";

//$i_Calendar_Conflict_Warning = "Your suggested time of <strong>%s</strong> conflicts with the following existing event entries:";
//$i_Calendar_Conflict_Warning2 = "The time of this event (<strong>%s</strong>) conflicts with the following existing event entries:";
$i_Calendar_Conflict_Warning = "The selected time of (<strong>%s</strong>) conflicts with the following events:";
$i_Calendar_Conflict_Warning2 = "The time of this event (<strong>%s</strong>) conflicts with the following events:";
$i_Calendar_Conflict_Ignore = "Proceed to Save";

$i_Calendar_ImportCalendar = "Import Event";
$i_Calendar_ImportCalendar_ChooseCalendar = "Choose Calendar";
//$i_Calendar_ImportCalendar_IncorrectValue = "<span class='tabletextrequire'>*</span>&nbsp; Incorrect field(s).";
$i_Calendar_ImportCalendar_IncorrectValue = "<font color='red'>*</font>&nbsp; Fields have not been set up correctly.";
$i_Calendar_ImportCalendar_Instruction1 = "
Column 1 : Subject<br />
Column 2 : Start Date (YYYY-MM-DD)<br />
Column 3 : Start Time (HH:mm)<br />
Column 4 : End Date (YYYY-MM-DD)<br />
Column 5 : End Time (HH:mm)<br />
Column 6 : All Day Event<br />
Column 7 : Description<br />
Column 8 : Location<br />
";

$i_Calendar_ExportCalendar = "Export Event";
$i_Calendar_ExportCalendar_Range = "Date Range";
$i_Calendar_ExportCalendar_Onward = "All Events from Today";
$i_Calendar_ExportCalendar_All = "All Events";
$i_Calendar_ExportCalendar_Select = "Select Period";

$i_Calendar_Settings_PreferredView = "Preferred View";
$i_Calendar_Settings_TimeFormat = "Time Format";
$i_Calendar_Settings_12hr = "12 hour";
$i_Calendar_Settings_24hr = "24 hour";
$i_Calendar_Settings_WeekStartOn = "Week Starts On";
$i_Calendar_Settings_WorkingHours = "Working Hours";
$i_Calendar_Settings_DisableRepeat = "Disable Repeating Events";
//$i_Calendar_Settings_DisableGuest = "Disable Guest";
$i_Calendar_Settings_DisableGuest = "Disable Participant(s)";

$i_Calendar_AccessRight = "Access Rights";
$i_Calendar_ForceRemoval = "Force Removal";
$i_Calendar_New_RemoveBySearch = "Removal by Search";
$i_Calendar_ForceRemoval_Instruction = "Please input the start date and end date for the event you want to remove";
$i_Calendar_SearchRemoval_Type = "Search Type";
$i_Calendar_SearchRemoval_TypeOption1 = "Event";
$i_Calendar_SearchRemoval_TypeOption2 = "Calendar";
$i_Calendar_SearchRemoval_Phase = "Search Phrase";

$iCalendar_Main_Title = "iCalendar";

$iCalendar_NewEvent_Header = "Create New Event";
$iCalendar_EditEvent_Header = "Edit Event";

$iCalendar_ToolLink_ChooseDate = "Choose Date";
$iCalendar_ToolLink_QuickAddEvent = "Quick Add Event";
$iCalendar_ToolLink_Agenda = "Agenda";
$iCalendar_ToolLink_Preference = "Preference";

$iCalendar_Preference_View = "Default Calendar View";

$iCalendar_SchoolEvent_RecordType = array("School","Academic","Holiday","Group");
$iCalendar_SchoolEvent_Color = array("4F9313","0D98B6","FF0000","FF8401");
$iCalendar_SchoolEvent_Venue = "Venue";
$iCalendar_SchoolEvent_Nature = "Nature";
$iCalendar_SchoolEvent_PostedBy = "Posted By";
$iCalendar_SchoolEvent_Type = "Type";

$iCalendar_Agenda_Status = "Status";
$iCalendar_Agenda_YourResponse = "Your Response";
$iCalendar_Agenda_NoEvent = "No events were found in this period.";

$iCalendar_QuickAdd_Required = "Required.";
$iCalendar_QuickAdd_InvalidDate = "Invalid format.";

$iCalendar_NewEvent_EventDate = "Event Date";
$iCalendar_NewEvent_EventTime = "Event Time";

$iCalendar_NewEvent_HeaderOption = "Options";
$iCalendar_NewEvent_HeaderShare = "Share";
$iCalendar_NewEvent_Title = "Title";
$iCalendar_NewEvent_DateTime = "Date/Time";
$iCalendar_NewEvent_StartTime = "Start:";
$iCalendar_NewEvent_EndTime = "End:";
$iCalendar_NewEvent_Duration = "Duration:";
$iCalendar_NewEvent_DurationMin = "min.";
$iCalendar_NewEvent_DurationMins = "mins.";
$iCalendar_NewEvent_DurationHr = "hr.";
$iCalendar_NewEvent_DurationHrs = "hrs.";
$iCalendar_NewEvent_DurationDay = "day";
$iCalendar_NewEvent_DurationDays = "days";
$iCalendar_NewEvent_DurationWeek = "week";
$iCalendar_NewEvent_DurationWeeks = "weeks";
$iCalendar_NewEvent_DurationMonth = "month";
$iCalendar_NewEvent_DurationMonths = "months";
$iCalendar_NewEvent_DurationYear = "year";
$iCalendar_NewEvent_DurationYears = "years";
$iCalendar_NewEvent_Repeats = "Repeats";
$iCalendar_NewEvent_Repeats_Not = "Does not repeat";
$iCalendar_NewEvent_Repeats_Every = "Repeat Every: ";
$iCalendar_NewEvent_Repeats_On = "Repeat On: ";
$iCalendar_NewEvent_Repeats_By = "Repeat By: ";
$iCalendar_NewEvent_Repeats_Daily = "Daily";
$iCalendar_NewEvent_Repeats_DailyMsg1 = "Every ";
$iCalendar_NewEvent_Repeats_DailyMsg2 = ", until ";
$iCalendar_NewEvent_Repeats_EveryWeekday = "Every weekday (Mon - Fri)";
$iCalendar_NewEvent_Repeats_EveryWeekday2 = "Weekly on weekdays";
$iCalendar_NewEvent_Repeats_EveryMonWedFri = "Every Mon., Wed. and Fri.";
$iCalendar_NewEvent_Repeats_EveryMonWedFri2 = "Weekly on Monday, Wednesday, Friday";
$iCalendar_NewEvent_Repeats_EveryTuesThur = "Every Tues. and Thur.";
$iCalendar_NewEvent_Repeats_EveryTuesThur2 = "Weekly on Tuesday, Thursday";
$iCalendar_NewEvent_Repeats_Weekly = "Weekly";
$iCalendar_NewEvent_Repeats_Weekly2 = "Weekly on ";
$iCalendar_NewEvent_Repeats_Weekly3 = " weeks on ";
$iCalendar_NewEvent_Repeats_CheckWeekday = "day";
$iCalendar_NewEvent_Repeats_WeekdayArray = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
$iCalendar_NewEvent_Repeats_WeekdayArray2 = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
$iCalendar_NewEvent_Repeats_Count = array("the first", "the second", "the third", "the fourth", "the fifth", "the sixth");
$iCalendar_NewEvent_Repeats_Monthly = "Monthly";
$iCalendar_NewEvent_Repeats_Monthly2 = "Every month on ";
$iCalendar_NewEvent_Repeats_Monthly2b = "day of ";
$iCalendar_NewEvent_Repeats_Monthly_general = "%D% day of every %N% month(s)";

$iCalendar_NewEvent_Repeats_Monthly3 = "day of the month";
$iCalendar_NewEvent_Repeats_Monthly4 = "day of the week";
$iCalendar_NewEvent_Repeats_Monthly5 = "months on ";
$iCalendar_NewEvent_Repeats_MonthArray = array("January","February","March","April","May","June","July","August","September","October","November","December");
$iCalendar_NewEvent_Repeats_Yearly = "Yearly";
$iCalendar_NewEvent_Repeats_Yearly2 = "Annually on ";
$iCalendar_NewEvent_Repeats_Yearly3 = " years on ";
$iCalendar_NewEvent_Repeats_Range = "Range:";
$iCalendar_NewEvent_Repeats_RangeStart = "Starts:";
$iCalendar_NewEvent_Repeats_RangeEnd = "Ends:";
$iCalendar_NewEvent_Repeats_RangeNever = "Never";
$iCalendar_NewEvent_Repeats_RangeUntil = "Until";
$iCalendar_NewEvent_Repeats_SaveTitle = "Edit recurring event";
$iCalendar_NewEvent_Repeats_SaveMsg = "Do you want to change only this event, or all events in the series, or this and all future events in the series?";
$iCalendar_NewEvent_MultipleRepeat_Overlap = "Yes, create overlapping events.";
$iCalendar_NewEvent_MultipleRepeat_One = "No, just one.";
$iCalendar_NewEvent_Repeats_SaveThisOnly = "Only this event";
$iCalendar_NewEvent_Repeats_SaveAll = "All events in the series";
$iCalendar_NewEvent_Repeats_SaveFollow = "All following";
$iCalendar_NewEvent_IsImportant = "Important";
$iCalendar_NewEvent_IsAllDay = "All day event";
$iCalendar_NewEvent_Calendar = "Calendar";
$iCalendar_NewEvent_Description = "Description";
$iCalendar_NewEvent_Personal_Note = "Personal Note";
$iCalendar_NewEvent_Location = "Location";
$iCalendar_NewEvent_Link = "Link:";
$iCalendar_NewEvent_Reminder = "Reminder";
$iCalendar_NewEvent_Reminder_Popup = "Pop-up";
$iCalendar_NewEvent_Reminder_Email = "Email";
$iCalendar_NewEvent_Reminder_NotSet = "No reminders set";
$iCalendar_NewEvent_Reminder_Remove = "Remove";
$iCalendar_NewEvent_Reminder_Add = "Add a reminder";
$iCalendar_NewEvent_Access = "Access";
$iCalendar_NewEvent_AccessDefault = "Default";
$iCalendar_NewEvent_AccessPublic = "Public";
$iCalendar_NewEvent_AccessPrivate = "Private";
//$iCalendar_NewEvent_Share = "Add guest(s)";
$iCalendar_NewEvent_Share = "Add participant(s)";
$iCalendar_NewEvent_ShareResponse = "Are you coming?";
//$iCalendar_NewEvent_ShareCount = "Guest count";
$iCalendar_NewEvent_ShareCount = "Participant count";
$iCalendar_NewEvent_ShareCountMsgX = "You haven't responsed yet";
$iCalendar_NewEvent_ShareCountMsg1 = "hasn't responded";
$iCalendar_NewEvent_ShareCountMsg2 = "maybe";
$iCalendar_NewEvent_ShareCountMsg3 = "yes";
$iCalendar_NewEvent_ShareCountMsg4 = "no";
//$iCalendar_NewEvent_SelectGuest = "Select guest(s)";
$iCalendar_NewEvent_SelectGuest = "Select participant(s)";

$iCalendar_Calendar_MyCalendar = "Personal";
$iCalendar_Calendar_OtherCalendar = "Other Calendars";
$iCalendar_Calendar_SchoolCalendar = "School Calendar";
$iCalendar_Calendar_InvolveEvent = "Involve Events";
$iCalendar_Calendar_otherInvitation = "Other Invitaion";      
$iCalendar_Calendar_ManageCalendar = "Manage calendars";
$iCalendar_ManageCalenadar_syncDes = "(Note for Google Calendar: copy the link shown in <span style='text-decoration:underline'>Private Address</span> instead of Calendar Address if you have not already published your Google Calendar.)";
$iCalendar_Calendar_NoCalendar = "No calendars found.";

$iCalendar_EditEvent_CreatedBy = "Created By";
$iCalendar_DeleteEvent_ConfirmTitle1 = "Delete Recurring Event";
$iCalendar_DeleteEvent_ConfirmTitle2 = "Delete Event";
$iCalendar_DeleteEvent_ConfirmMsg1 = "Would you like to delete only this event, all events in the series, or this and all future events in the series?";
$iCalendar_DeleteEvent_ConfirmMsg2 = "Are you sure you want to delete \"#%EventTitle%#\"?";
$iCalendar_DeleteEvent_ConfirmMsg2_js = "Are you sure you want to delete #%EventTitle%#?";

$iCalendar_CheckForm_NoRepeatEnd = "Please enter the end date of the recurring event.";
$iCalendar_CheckForm_NoDate = "Please enter the event date.";
//$iCalendar_CheckForm_NoTitle = "Please enter the event title";
//$iCalendar_CheckForm_NoCalName = "Please enter the calendar name";
//$iCalendar_CheckForm_DateWrong = "Date format is wrong, correct format is YYYY-MM-DD.";
//$iCalendar_CheckForm_UrlWrong = "Please enter a valid web link address.";
$iCalendar_CheckForm_NoTitle = "Please enter an event title.";
$iCalendar_CheckForm_NoCalName = "Please enter a calendar name.";
$iCalendar_CheckForm_DateWrong = "Please enter using this date format: YYYY-MM-DD.";
$iCalendar_CheckForm_UrlWrong = "Please enter a valid URL address.";
$iCalendar_CheckForm_NoCalColor = "Please select a display color.";

$iCalendar_NewCalendar_Header = "Create New Calendar";
$iCalendar_NewCalendar_Name = "Calendar Name";
$iCalendar_NewCalendar_Type = "Calendar Type";
$iCalendar_NewCalendar_Description = "Description";
$iCalendar_NewCalendar_PeopleShare = "Sharing with";
$iCalendar_NewCalendar_ShareWithAll = "Share with everyone";
$iCalendar_NewCalendar_GroupShare = "Sharing with (groups)";
$iCalendar_NewCalendar_AddPeopleShare = "Add new people";
$iCalendar_NewCalendar_ShareTablePerson = "Person";
$iCalendar_NewCalendar_ShareTablePermission = "Permission";
$iCalendar_NewCalendar_ShareTableDelete = "Delete";
$iCalendar_NewCalendar_SharePermissionFull = "Full";
//$iCalendar_NewCalendar_SharePermissionEdit = "Read and Edit";
$iCalendar_NewCalendar_SharePermissionEdit = "Read and Create";
$iCalendar_NewCalendar_SharePermissionRead = "Read Only";
$iCalendar_NewEvent_ShareSelectViewer = "Select viewer(s)";
$iCalendar_NewEvent_ShareColor = "Display Color";
$iCalendar_NewEvent_SyncUrl = "Sync Calendar from:";
$iCalendar_NewEvent_NeedHelp = "Need Help?";

$iCalendar_AddPublicCalendar_Header = "Add Public Calendar";
$iCalendar_SearchPublicCalendar = "Search by keyword(s)";

$iCalendar_EditCalendar_Header = "Edit Calendar";
$iCalendar_EditCalendar_Owner = "Calendar Owner";
$iCalendar_EditCalendar_Group = "Group";

$iCalendar_ManageCalendar_Sharing = "Sharing";
$iCalendar_ManageCalendar_SharedMsg1 = "Shared";
$iCalendar_ManageCalendar_SharedMsg2 = "Not shared";
$iCalendar_ManageCalendar_Setting = "Calendar Settings";
$iCalendar_ManageCalendar_ConfirmTitle1 = "Delete Calendar";
$iCalendar_ManageCalendar_ConfirmMsg1 = "Are you sure you want to permanently delete the <strong>#%calName%#</strong> calendar?";
$iCalendar_ManageCalendar_ConfirmMsg2 = "Are you sure you want to remove the <strong>#%calName%#</strong> calendar from view?";

$iCalendar_CalendarList_Title = "Calendar List";

$iCalendar_ChooseViewer_ParentSuffix = "'s Parent";
$iCalendar_ChooseViewer_Course = "Course";
$iCalendar_ChooseViewer_Group = "Group";

$iCalendar_Import_Error1 = "Cannot connect to server, import failed.";
$iCalendar_Import_Error2 = "iCal file format incorrect, import failed.";
$iCalendar_Import_Success = "Calendar imported successfully.";

# for ESF 
$iCalendar_Calendar_InvolveEvent = "Involved Events";
$iCalendar_NewCalendar_ShareWithAll = "Everyone can subscribe?";
$iCalendar_NewCalendar_DefaultEventAccess = "Default Event Access";
$iCalendar_EditCalendar_NumOfSubscribers = "Number of Subscribers";
$iCalendar_NewCalendar_PermissionSetting = "Permission Settings";

$iCalendar_NewEvent_AccessRemark = "applies to calendar subscribers only";
$iCalendar_NewEvent_ShareCountMsgA = "Acknowledged";
//$iCalendar_NewEvent_InviteGuests = "Invite Guests";
$iCalendar_NewEvent_InviteGuests = "Invite Participant(s)";
//$iCalendar_NewEvent_Guests = "Guests";
$iCalendar_NewEvent_Guests = "Participant(s)";
$iCalendar_NewEvent_Attach = "Attach";
$iCalendar_NewEvent_Attachment = "Attachment";
$iCalendar_NewEvent_Alert = "Alert";
$iCalendar_NewEvent_AddAlert = "Add Alert";
//$iCalendar_NewEvent_EventType_Compulsory = "Compulsory as \"Notification\"";
$iCalendar_NewEvent_EventType_Compulsory = "Compulsory";
//$iCalendar_NewEvent_EventType_Optional = "Optional as \"Invitation\"";
$iCalendar_NewEvent_EventType_Optional = "Invitation for participation";

$iCalendar_Calendar_OtherCalendar_School = "School";
$iCalendar_Calendar_OtherCalendar = "Optional";
$iCalendar_Calendar_SystemCalendar = "System Calendar";
$iCalendar_Calendar_Subscribe = "Subscribe";
$iCalendar_Calendar_Unsubscribe = "Unsubscribe";
$iCalendar_Calendar_CalendarFound = "Calendar found.";
$iCalendar_Calendar_NoCalendarFound = "No Calendar is found.";

$iCalendar_NewCalendar_ShareTo = "Share to";
$iCalendar_NewCalendar_Name = "Name";
$iCalendar_SchoolCalendar = "School Calendar";
$iCalendar_SharedCalendar = "Shared Calendar";
$iCalendar_PublicCalendar = "Public Calendar";
$iCalendar_AddAllUsersAsReader = "Add all users as Readers";
$iCalendar_SetSchoolCalender_Remark = "Other than the DBA user, all normal users cannot remove, edit this school calendar and add, edit, delete events in this school calendar";
$iCalendar_SetShareToAll_Remark = "DBAs will be granted the Editor rights.";
$iCalendar_BackTo_Calendar = "Back to Calendar";

$iCalendar_NewCalendar_Header = "New Calendar";
$iCalendar_ImportCalendarEvent_SelectFile = "Select File";
$iCalendar_ImportCalendarEvent_Format = "Format";
$iCalendar_ImportCalendarEvent_DownloadSample = "Click here to download sample";
$i_Calendar_ImportCalendar_Instruction1 = "
Column 1 : Subject<br />
Column 2 : Start Date (YYYY-MM-DD or MM/DD/YYYY)<br />
Column 3 : Start Time (HH:mm)<br />
Column 4 : End Date (YYYY-MM-DD or MM/DD/YYYY)<br />
Column 5 : End Time (HH:mm)<br />
Column 6 : All Day Event (TRUE or FALSE)<br />
Column 7 : Location<br />
Column 8 : Description<br />
Column 9 : Link<br />
";
//Column 9 : Private<br />

$i_Calendar_CompulsoryEvent_Remark = "This event will be a compulsory event.";
$i_Calendar_OptionalEvent_Remark = "Your permission level does not permit creation of compulsory events.";

//$icalender_deleteCalendar_existGuest_remark = "Since the calender (<strong>#%calName%#</strong>) is shared to someone, this calendar is only remove from your view but not the view of shared guests. All other shared guests can still view this calendar unless you remove all the guests from sharing.";
$icalender_deleteCalendar_existGuest_remark = "Since the calender (<strong>#%calName%#</strong>) is shared to someone, this calendar is only remove from your view but not the view of shared participants. All other shared guests can still view this calendar unless you remove all the participants from sharing.";

## added after 2009-2-20 
$iCalendar_Calendar_Staff = "Staff";
$iCalendar_Calendar_Student = "Student";
$iCalendar_Calendar_PersonalCalendar = "Personal Calendar";
$iCalendar_Calendar_FoundationCalendar = "Foundation Calendar";
$iCalendar_Calendar_OtherCalendar_Foundation = "Foundation";
$iCalendar_SetFoundationCalender_Remark = "All users will not be granted to remove, edit this school calendar and add, edit, delete events in this school calendar except the Foundation SGA owner.";


## added afer 2009-3-6
$iCalendar_OtherCalType_ViewDetails = "View Detail";
$iCalendar_OtherCalType_EnrolNow = "Enrol Now";
$iCalendar_ImportEvent_Link = "Link";
$iCalendar_ImportEvent_InvalidRecords = "Record(s) will not be imported";
$iCalendar_ImportEvent_ValidRecords = "Record(s) will be imported";

$iCalendar_meeting_Check_Availability = "Check Availabilily";
$iCalendar_meeting_Check = "Check";
//$iCalendar_meeting_Reminder = "Please select at least one guest and specify an event date/time when checking for availability.";
$iCalendar_meeting_Reminder = "Please select at least one participant and specify an event date/time when checking for availability.";
$iCalendar_Agenda_NoEventsForToday = "No events for today.";
$iCalendar_ActiveEvent_Event = "Event";
$iCalendar_Agenda_NoEventsForThisMonment = "No records found at this moment";
$iCalendar_Calendar_Today = "Today";
$iCalendar_addUser = "Add user";
$iCalendar_Search = "Search";
$iCalendar_Name = "Calendar Name";
$iCalendar_Yes = "Yes";
$iCalendar_No = "No";
$iCalendar_New_Event = "New Event";
$iCalendar_Print= "Print";
$iCalendar_invitation = "Invitation";
$iCalendar_notification = "Notification";
$iCalendar_NoActiveEvent = "No Active Event at this moment.";
$iCalendar_OK  = "OK";
$iCalendar_more = "more";
$iCalendar_new = "New";
$iCalendar_add= "Add";
$iCalendar_close= "Close";
$iCalendar_OverlapWarning = "The event is a %numDay%-day recurring event. Are you sure you want to create a series of %numDay%-day recurring events?";
$iCalendar_PopupAlert= "Popup Alert";
$iCalendar_EventType = "Event Type";
//$iCalendar_AddToGuest = "Add to guest's calendar";
$iCalendar_AddToGuest = "Add to participant's calendar";
$iCalendar_OfficeHR = "Office Hour";
$iCalendar_ExteranlEvent_StartDate = "Start Date";
$iCalendar_ExteranlEvent_DueDate = "Due Date";
$iCalendar_group = "Group";
$iCalendar_course = "Course";
$iCalendar_systemAdmin = "System Admin";
$iCalendar_selectedTime = "Specified Time";
$iCalendar_available = 'Available';
$iCalendar_unavailable = 'Not Available';
$iCalendar_searchTab = 'Search Result';
$iCalendar_eventSearch = 'Event Search';
$iCalendar_eventSearch_noRecordFound = 'No records found.';
$iCalendar_eventSearch_searchingFor ='Search Result For %%value%%';
$iCalendar_evtRequest = 'Event request';

### ESF en.php
$Lang['btn_export'] = "Export to csv";
$Lang['btn_export_ical'] = "Export to ical";
$Lang['syncURL'] = "Sync";
$Lang['btn_edit'] = "Edit";
$Lang['btn_delete'] = "Delete";
$Lang['btn_remove'] = "Remove";
$Lang['btn_restore'] = "Restore";
$Lang['btn_submit'] = "Submit";
$Lang['btn_cancel'] = "Cancel";
$Lang['btn_save'] = "Save";
$Lang['general']['SetToAll_SelectionBox'] = "--- Set to All ---";
$Lang['ReturnMsg']['CalendarCreateSuccess'] = "1|=|Calendar created successfully.";
$Lang['ReturnMsg']['CalendarCreateUnSuccess'] = "0|=|Calendar cannot be created.";
$Lang['ReturnMsg']['CalendarEditSuccess'] = "1|=|Calendar edited successfully.";
$Lang['ReturnMsg']['CalendarEditUnSuccess'] = "0|=|Calendar cannot be edited.";
$Lang['ReturnMsg']['CalendarUpdateSuccess'] = "1|=|Calendar updated successfully.";
$Lang['ReturnMsg']['CalendarUpdateUnSuccess'] = "0|=|Calendar cannot be updated.";
$Lang['ReturnMsg']['PersonalCalendarDeleteSuccess'] = "1|=|Personal calendar deleted successfully.";
$Lang['ReturnMsg']['PersonalCalendarDeleteUnSuccess'] = "0|=|Personal Calendar cannot be deleted.";
$Lang['ReturnMsg']['OptionalCalendarRemoveSuccess'] = "1|=|Optional Calendar removed successfully.";
$Lang['ReturnMsg']['OptionalCalendarRemoveUnSuccess'] = "1|=|Optional Calendar cannot be removed.";
$Lang['ReturnMsg']['EventImportSuccess'] = "1|=|Event(s) imported successfully.";
$Lang['ReturnMsg']['EventImportUnSuccess'] = "0|=|Event(s) cannot be imported.";
$Lang['ReturnMsg']['FormatOfImportedFileNotValid'] = "0|=|Format of the imported file is invalid.";
$Lang['ReturnMsg']['TypeOfImportedFileNotSupport'] = "0|=|Type of the imported file is not supported.";
$Lang['ReturnMsg']['EventDeleteSuccess'] = "1|=|Event(s) deleted successfully.";
$Lang['ReturnMsg']['EventDeleteUnSuccess'] = "0|=|Event(s) cannot be deleted.";
$Lang['ReturnMsg']['EventCreateSuccess'] = "1|=|Event(s) created successfully.";
$Lang['ReturnMsg']['EventCreateUnSuccess'] = "0|=|Event(s) cannot be created.";
$Lang['ReturnMsg']['EventUpdateSuccess'] = "1|=|Event(s) updated successfully.";
$Lang['ReturnMsg']['EventUpdateUnSuccess'] = "0|=|Event(s) cannot be updated.";
$Lang['ReturnMsg']['SettingUpdatedSuccess'] = "1|=|School term settings updated successfully.";
$Lang['ReturnMsg']['SettingUpdatedUnSuccess'] = "0|=|School term settings cannot be updated.";
$Lang['ReturnMsg']['UserPreferenceUpdateSuccess'] = "1|=|Preference updated successfully.";
$Lang['ReturnMsg']['UserPreferenceUpdateUnSuccess'] = "0|=|Preference cannot be updated.";
$Lang['btn_submitAndSend'] = "Submit & Send Email";
$Lang['btn_select'] = "Select";
$Lang['btn_reset'] = "Reset";

#newly added 
$iCalendar_submitNsend_title = "Invitaion to New Event";
$iCalendar_submitNsend_msg= "You are invited to join %EventTitle%. Please login to intranet to accept/deny the invitation. ";
$iCalendar_submitNsend_msgShort= "You are invited to join %EventTitle%.";
$iCalendar_seachType_all = "Search All";
$iCalendar_seachType_page = "Search This Page";
$iCalendar_seach_Keyword = "Keyword";
$i_Calendar_Edit = "Edit";
$i_Calendar_modifiedDate = "Modified Date"; 
$iCalendar_viewMode_simple = "Simple View";
$iCalendar_viewMode_full = "Full View";
$iCalendar_User_Preference = "User Preference";

$iCalendar_UserPref_DefaultMonthView = 'Default Month View';
$iCalendar_UserPref_DefaultCalendar = 'Default Calendar (New event)';

$i_Calendar_ImportEvent_InvalidRecords = 'Invalid Records';
$i_Calendar_ImportEvent_ValidRecords = 'Valid Records';
$i_Calendar_NoRecords_Remark = 'No records';
$i_Calendar_eBookingEvent_TimeClashWarning = 'The chosen location in the selected date and time has been booked already. Your booking record will be changed to "Pending" status.';
$i_Calendar_eBookingEvent_cancelBookingFirstWarning = 'The event has be associated with booking record(s) already. Please cancel the booking before changing the date and time of the event.';
$i_Calendar_eBookingEvent_clearBookingWarning = 'Are you sure you want to clear the booking now?'; 
$i_Calendar_eBookingEvent_titleDescRemarks = 'The title and the description of the event will be displayed as Remarks of the eBooking records.';

$Lang['iCalendar']['SkipHoliday'] = "Skip holidays";
$Lang['iCalendar']['WarningArr']['AllDaysInDateRangeAreHolidays'] = "All days in selected date range are holidays, please select another day range or uncheck [".$Lang['iCalendar']['SkipHoliday']."] option.";

$Lang['iCalendar']['RemoveSharedCalendarWarning'] = "The calendar (<strong>#%calName%#</strong>) is shared with others. Do you want to remove and keep sharers view right, or remove the calendar completely?";
$Lang['iCalendar']['RemoveAndKeepSharersViewRight'] = "Remove and keep sharers view right";
$Lang['iCalendar']['RemoveCalendarCompletely'] = "Remove completely";

$Lang['iCalendar']['TheLast'] = "the last";

$Lang['iCalendar']['CheckAvailability'] = "Check availability";
$Lang['iCalendar']['CheckFor'] = "Check for";
$Lang['iCalendar']['AvailableOrNot'] = "Available or not";
$Lang['iCalendar']['EventTimeline'] = "Whole day event timeline";
$Lang['iCalendar']['IncludingEventType'] = "Including event types";
$Lang['iCalendar']['Available'] = "Available";
$Lang['iCalendar']['Unavailable'] = "Unavailable";
$Lang['iCalendar']['RequestSelectGuest'] = "Please select at least one participant.";
$Lang['iCalendar']['ParticipationOption'] = "Options";
$Lang['iCalendar']['PersonalEvent'] = "Personal Event";
$Lang['iCalendar']['NParticipantsInvolved'] = " (<!--N--> participants involved)";
$Lang['iCalendar']['SubmitAndSendPushMessage'] = "Submit & Send Push Message";
$Lang['iCalendar']['CompulsoryEventPushNotificationTitle'] = "Calendar event (<!--DATE-->) from <!--NAME-->";
$Lang['iCalendar']['CompulsoryEventPushNotificationContentTimeStatement'] = " which will start at <!--TIME-->";
$Lang['iCalendar']['CompulsoryEventPushNotificationContentVenueStatement'] = " (venue: <!--VENUE-->)";
$Lang['iCalendar']['CompulsoryEventPushNotificationContent'] = "You have been added to an event titled \"<!--TITLE-->\"<!--TIME_STATEMENT--><!--VENUE_STATEMENT--> on <!--DATE-->. ";
$Lang['iCalendar']['InvitationEventPushNotificationTitle'] = "Calendar event (<!--DATE-->) from <!--NAME-->";
$Lang['iCalendar']['InvitationEventPushNotificationContentTimeStatement'] = " which will start at <!--TIME-->";
$Lang['iCalendar']['InvitationEventPushNotificationContentVenueStatement'] = " (venue: <!--VENUE-->)";
$Lang['iCalendar']['InvitationEventPushNotificationContent'] = "You are invited to join an event titled \"<!--TITLE-->\"<!--TIME_STATEMENT--><!--VENUE_STATEMENT--> on <!--DATE-->. Please response using iCalendar. ";

$Lang['iCalendar']['TheRoomIsNotAvailableOnTheFollowingDates'] = "The room is NOT available on the following date(s):";
$Lang['iCalendar']['TheRoomIsAvailableOnTheFollowingDates'] = "The room is available on the following date(s):";

?>