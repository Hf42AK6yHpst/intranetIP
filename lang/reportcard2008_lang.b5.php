<?php
# Editing by anna
# Date: 20200407 Philips
#	added $eReportCard['Reports_StudentYearlyResultReport']
#	added $eReportCard['DataExport_To_WebSAMS']
#
# Date: 20190628 Philips
#   added $eReportCard['GradeSettings']
#
# Date: 20180620 Philips
#   added $eReportCard['SubjectType']
#   added $eReportCard['SubjectCredit']
#   added $eReportCard['SubjectLowerLimit']
#   added $eReportCard['NotSet']
#   added $eReportCard['SubjectCore']
#   added $eReportCard['SubjectElection']
#   added $eReportCard['SubjectUnknown']
#
#
// 2016-10-03 Add $eReportCard['Reports_GradingSchemeSammary']

$eReportCard['ReportCard'] = "成績表";

// System message after form submit, must have the prefix "$i_con_msg_"
$i_con_msg_wrong_row = "<font color=red>於%s行的資料無效。</font>\n";
$i_con_msg_wrong_csv_header = "<font color=red>CSV檔案標頭錯誤。</font>\n";
$i_con_msg_report_generate = "<font color=green>成績表製作完成。</font>\n";
$i_con_msg_report_generate_failed = "<font color=red>找不到成績資料，成績成製作尚未完成。</font>\n";
$i_con_msg_promotion_generate = "<font color=green>升級狀態製作完成。</font>\n";
$i_con_msg_promotion_generate_failed = "<font color=red>升級狀態製作尚未完成。</font>\n";
$i_con_msg_delete_comment_cat_success = "<font color=green>評語類別已被刪除。</font>\n";
$i_con_msg_delete_comment_cat_failed = "<font color=red>評語類別尚未刪除。</font>\n";
$i_con_msg_num_records_updated = "項記錄已更新。";
$i_con_msg_report_archive = "<font color=green>成績表存檔成功。</font>\n";
$i_con_msg_report_archive_failed = "<font color=red>成績表存檔失敗。</font>\n";


// Menu
$eReportCard['Management'] = "管理";
$eReportCard['Management_Schedule'] = "時間表";
$eReportCard['Management_Progress'] = "進度";
$eReportCard['Management_MarksheetRevision'] = "分紙修改";
$eReportCard['Management_MarksheetSubmission'] = "呈交分紙";
$eReportCard['Management_MarksheetVerification'] = "核對分紙";
$eReportCard['Management_ClassTeacherComment'] = "班主任評語";
$eReportCard['Management_ConductMark'] = "操行分"; // for li sing tai hang use only
$eReportCard['Management_OtherInfo'] = "其他資料";
$eReportCard['Management_OtherStudentInfo'] = "其他學生資料"; // for SIS use only
$eReportCard['Management_DataHandling'] = "資料處理";
$eReportCard['Management_ReportArchive'] = "成績表存檔";
$eReportCard['Management_Promotion'] = "升級狀況";
$eReportCard['Management_AcademicProgress'] = "成績進步";
$eReportCard['Management_ConductGradeCalculation'] = "學生操行等級";
$eReportCard['Management_ManualInputReExamStatus'] = "學生補考結果";

$eReportCard['NoComment'] = "沒有評語";

$eReportCard['Reports'] = "報告";
$eReportCard['Reports_GenerateReport'] = "製作及列印成績表";
$eReportCard['Reports_SubMarksheetReport'] = "附屬分紙報告";
$eReportCard['Reports_AssessmentReport'] = "評估報告";
$eReportCard['Reports_GrandMarksheet'] = "大分紙";
$eReportCard['Reports_OtherReports'] = "其他報告";
$eReportCard['Reports_ClassHonourReport'] = "等級榮譽報告";
$eReportCard['Reports_ScholarshipReport'] = "榮譽學生獎學金";
$eReportCard['Reports_ConductAwardReport'] = "操行獎報告";
$eReportCard['Reports_SubjectPrizeReport'] = "學科獎報告";
$eReportCard['Reports_GPATop3Rank'] = "排名報告";
$eReportCard['Reports_ExaminationSummary'] = "考試成績概覽";
$eReportCard['Reports_StudentResultSummary'] = "學生成績概覽";
$eReportCard['Reports_SubjectSummarySheet'] = "科目成績概覽";
$eReportCard['Reports_GradeDistribution'] = "成績分佈";
$eReportCard['Reports_MarkGradeConversion'] = "分數等級轉換表";
$eReportCard['Reports_MarkPositionConversion'] = "分數名次轉換表";
$eReportCard['Reports_RawMarkDistribution'] = "分數分佈";
$eReportCard['Reports_RemarksReport'] = "評語報告";
$eReportCard['Reports_SubjectPercentagePassReport'] = "科目及格比率報告";
$eReportCard['Reports_MasterReport'] = "綜合報告";
$eReportCard['Reports_TrialPromotion'] = "試升報告";
$eReportCard['Reports_HKUGAGradeList'] = "報告概覽";
$eReportCard['Reports_AwardList'] = "獎項得獎名單";
$eReportCard['Reports_SubjectAcademicResult'] = "科目學業成績";
$eReportCard['Reports_FinalComment'] = "學年結語";
$eReportCard['Reports_ReExamList'] = "補考名單";
$eReportCard['Reports_Transcript'] = "成績單";
$eReportCard['Reports_SubjectFullMarkComponentRatioSettings'] = "各科滿分及分卷比重設定";
$eReportCard['Reports_ClassSubjectComponentResultAnalysis'] = "各科各班分卷成績分析";
$eReportCard['Reports_TeacherSubjectResultAnalysis'] = "各老師任教科目成績分析";
$eReportCard['Reports_ClassSubjectCAResultAnalysis'] = "各班各科平時分分析";
$eReportCard['Reports_TermClassResultAnalysis'] = "各班各科學期成績分析";
$eReportCard['Reports_FormSubjectCAResultAnalysis'] = "各級各科平時分分析";
$eReportCard['Reports_FormSubjectResultAnalysis'] = "各科全級成績分析";
$eReportCard['Reports_ClassSubjectResultAnalysis'] = "各科各班成績分析";
$eReportCard['Reports_PrintArchivedReport'] = "列印成績表存檔";
$eReportCard['Reports_MarkAnalysis'] = "分數分析";
$eReportCard['Reports_GradingReport'] = "等級報告";
$eReportCard['Reports_YearEndReport'] = "年終報告";
$eReportCard['Reports_ClassAllocation'] = "編班報告";
$eReportCard['Reports_SubjectStat'] = "各科成績統計表";
$eReportCard['Reports_MarkupExamSubject'] = "補考成績登記表";
$eReportCard['Reports_AwardPrediction'] = "獎項名單估算表";
$eReportCard['Reports_GradingSchemeSummary'] = "等級計劃概覽";
$eReportCard['Reports_FormTeacherCommentReport'] = "班級老師評語";
$eReportCard['Reports_ReportCardSummary'] = "成績表概覽";
$eReportCard['Reports_AwardCertificate'] = "獎項證書";
$eReportCard['Reports_AwardScoreSummary'] = "獎項分數概覽";
$eReportCard['Reports_Testimonial'] = "推薦書";
$eReportCard['Statistics'] = "統計";

$eReportCard['Settings'] = "設定";
$eReportCard['Settings_BasicSettings'] = "基本設定";
$eReportCard['Settings_CommentBank'] = "評語庫";
$eReportCard['Settings_Conduct'] = "操行";
$eReportCard['Settings_SubjectsAndForm'] = "科目及級別";
$eReportCard['Settings_ReportCardTemplates'] = "成績表範本";
$eReportCard['Settings_ExcludeStudents'] = "不排名次學生";
$eReportCard['Settings_CurriculumExpectation'] = "課程概要";
$eReportCard['Settings_TeacherExtraInfo'] = "教師額外資料";
$eReportCard['Settings_GradingScheme'] = "等級計劃"; 
$eReportCard['Settings_SubjectTopics'] = "科目範疇";
$eReportCard['Settings_ImportAwards'] = "匯入無須製作獎項";
$eReportCard['Settings_AllowAccessIP'] = "指定可存取電腦 IP";
$eReportCard['Settings_ViewGroupUser'] = "檢視小組人員";
$eReportCard['Settings_ChangeClubActivityName'] = "更改課外活動名稱";
$eReportCard['Settings_SubjectStream'] = "學生分組設定";
$eReportCard['Settings_PromotionStatusRemarks'] = "升留班報告顯示";
$eReportCard['Settings_SubjectGroupDescription'] = "科組概要";

// Tabs
#$eReportCard['HighlightSettings'] = "樣式";
$eReportCard['StyleSettings'] = "樣式";
$eReportCard['MarkStorageAndDisplay'] = "分數儲存及顯示";
$eReportCard['AbsentAndExemptSettings'] = "缺席及免修";
$eReportCard['CalculationSettings'] = "計算";
$eReportCard['AccessSettings'] = "存取權限";
$eReportCard['PostTableHeader'] = "報告職位表格欄位";

$eReportCard['SubjectSettings'] = '科目';
$eReportCard['FormSettings'] = '級別';
//$eReportCard['CreditSettings'] = '學分';

// Calculation settings
$eReportCard['Term'] = "學期";
$eReportCard['WholeYear'] = "全年";
$eReportCard['SchemeTitle'] = "等級計劃 ";

$eReportCard['Absent'] = "缺席";
$eReportCard['AbsentExcludeWeight'] = "扣除缺席評估項目的比重";
$eReportCard['AbsentExcludeFullMark'] = "扣除缺席評估項目的滿分";
$eReportCard['AbsentTreatAsZeroMark'] = "把缺席評估項目的原始分數當作0分";

$eReportCard['Exempt'] = "免修";
$eReportCard['ExemptExcludeWeight'] = "扣除免修評估項目的比重";
$eReportCard['ExemptExcludeFullMark'] = "扣除免修評估項目的滿分";
$eReportCard['ExemptTreatAsZeroMark'] = "把免修評估項目的本分當作0分";

$eReportCard['CalculationMethod'] = "計算方法";
$eReportCard['ConsolidateMark'] = "為科目紀錄結算出比重分數(總分除外)";
$eReportCard['AdjustFullMark'] = "套用比重前調整每個科目的滿分至100";

$eReportCard['CalculationOrder'] = "計算次序";
$eReportCard['UseTermMarkFromAll'] = "使用從所有成分計算出來的學期科目分數";
$eReportCard['UseTermAvgMarkFromAll'] = "使用從所有科目計算出來的學期平均分數";
$eReportCard['TermRightDown'] = "先計算學期內科目的平均分";
$eReportCard['TermDownRight'] = "先計算評估項目的平均分";
$eReportCard['YearRightDown'] = "先計算全年內科目的平均分";
$eReportCard['YearDownRight'] = "先計算學期平均分";
$eReportCard['TermPreviewNotApplicable'] = "預覽功能不適用於「先計算評估項目的平均分」的成績表範本";
$eReportCard['YearPreviewNotApplicable'] = "預覽功能不適用於「先計算學期平均分」的成績表範本";

// Highlight
$eReportCard['Style'] = "樣式";
$eReportCard['Format'] = "格式";
$eReportCard['SpecialSymbol'] = "特別符號";
$eReportCard['Sample'] = "實例";
$eReportCard['Mark'] = "分數";
$eReportCard['TotalMark'] = "總分數";

// Mark Storage & Display 
$eReportCard['AssessmentMark'] = "評估項目的分數";
$eReportCard['SubjectOverallMark'] = "科目總分";
$eReportCard['OverallAverage'] = "整體平均分";
$eReportCard['OverallTotal'] = "整體總分";
$eReportCard['Integer'] = "整數";
$eReportCard['OneDecimalPlace'] = "1個小數位";
$eReportCard['TwoDecimalPlaces'] = "2個小數位";

// Access Settings
$eReportCard['AllowClassTeacherUploadCSV'] = "允許班主任管理".$eReportCard['Management_OtherInfo'];
$eReportCard['AllowTeacherAccessGrandMS'] = "允許老師使用".$eReportCard['Reports_GrandMarksheet'];
$eReportCard['AllowTeacherAccessMasterReport'] = "允許老師使用".$eReportCard['Reports_MasterReport'];
$eReportCard['EnableVerificationPeriod'] = "啟用家長及學生核對時段";
$eReportCard['AuthenticationSetting'] = "首次進入時需要進行登入認證";

// Comment Bank
$eReportCard['Subject'] = "科目";
$eReportCard['Class'] = "班別";
$eReportCard['Code'] = "編號";
$eReportCard['CommentContent'] = "評語內容";
$eReportCard['CommentContentChi'] = "評語內容 (中文)";
$eReportCard['CommentContentEng'] = "評語內容 (英文)";
$eReportCard['Category'] = "類別";
$eReportCard['Type'] = "類型";
$eReportCard['AllTeachers'] = "所有老師";
$eReportCard['ClassTeacher'] = "班主任";
$eReportCard['SubjectTeacher'] = "科目老師";
$eReportCard['AllCategories'] = "所有類別";
$eReportCard['AlertEnterCommentCode'] = "請輸入編號";
$eReportCard['AlertDuplicateCommentCode'] = "編號已經存在，請輸入一個新的編號";
$eReportCard['AlertEnterCommentCategory'] = "請輸入類別";
$eReportCard['AlertEnterCommentContent'] = "請輸入評語內容";
$eReportCard['AlertSelectFile'] = "請先選擇檔案";
$eReportCard['DeleteSelectedCategory'] = "刪除已選擇類別";
$eReportCard['AlertDeleteSelectedCategory'] = "刪除評語類別同時亦會移除所有相關內容，確認刪除?";
$eReportCard['SelectComment'] = "選擇評語";
$eReportCard['CommentList'] = "評語列表";
$eReportCard['Language'] = "語言";

// Grading Differentiation
$eReportCard['Distinction'] = '優異';
$eReportCard['Pass'] = '合格';
$eReportCard['Fail'] = '不合格';

$eReportCard['Removing'] = '移除中';
$eReportCard['Saving'] = '儲存中';

$eReportCard['Grade'] = '等級';
$eReportCard['SelectScheme'] = '選擇計劃';
$eReportCard['GradePoint'] = "等級點數";
$eReportCard['GradingRange'] = '等級範圍';
$eReportCard['GradingScheme'] = '等級計劃';
$eReportCard['GradingTitle'] = "等級標題";
$eReportCard['GradingType'] = "等級類型";
$eReportCard['HonorBased'] = "榮譽";
$eReportCard['UpperLimit'] = "上限";
$eReportCard['LowerLimit'] = "下限";
$eReportCard['InputScaleType'] = '輸入比例';
$eReportCard['PassFailBased'] = "合格/不合格";
$eReportCard['ResultDisplayType'] = '顯示結果';
$eReportCard['SchemesFullMark'] = "滿分";
$eReportCard['StudentsFromTop'] = "學生<br>從最頂";
$eReportCard['StudentsFromTop_nowrap'] = "學生從最頂";
$eReportCard['Mark'] = '分數';
$eReportCard['MarkRange'] = '分數範圍';
$eReportCard['Next'] = '下一個';
$eReportCard['SchemesPassingMark'] = '合格分數';
$eReportCard['PercentageRangeForAllStudents'] = '全體學生的百分比範圍';
$eReportCard['PercentageRangeForPassedStudents'] = '合格學生的百分比範圍';

$eReportCard['AddMore'] = "加入更多";
$eReportCard['CopyFrom'] = "複製自";
$eReportCard['Sift'] = '篩選';
$eReportCard['ClickToSift'] = '按圖篩選';
$eReportCard['ClickToUnsift'] = '顯示全部';
$eReportCard['jsAlertNoSubjectSelect'] = "班級中沒有選擇科目";
$eReportCard['MoveUp'] = '向上';
$eReportCard['MoveDown'] = '向下';
$eReportCard['NextForm'] = '下一個班級';
$eReportCard['PreviousForm'] = '上一個班級';

$eReportCard['jsAlertNoSubjectAssigned'] = '沒有班級設定存在可供複製';

// added on 24 Apr 08
$eReportCard['CopyClassLevelSettingsResult'] = '班別設定複製結果';
$eReportCard['GradingSchemeExist'] = '等級計劃指定完成';
$eReportCard['SubmitMarksheetConfirm'] = "本頁結果將被儲存，你是否確認繼續?";

$eReportCard['RemoveSubMarksheetColumn'] = "本評估項目的數據將一起被刪除，你是否確要刪除此評估項目?";

// ReportCard Template
$eReportCard['InputBasicInformation'] = "輸入基本資料";
$eReportCard['InputSubjectWeight'] = "輸入科目比重";
$eReportCard['InputOverallSubjectWeight'] = "輸入整體科目比重";
$eReportCard['InputAssessmentRatio'] = "輸入評估項目的比率";
$eReportCard['AssessmentRatio'] = "評估項目的比率";
$eReportCard['Assessment'] = "評估項目";
$eReportCard['Ratio'] = "比率";
$eReportCard['InputTermsWeight'] = "輸入學期比重";
$eReportCard['InputTermsRatio'] = "輸入學期比率";
$eReportCard['InputAssessmentWeight'] = "輸入評估比重";
$eReportCard['InputReportDisplayInformation'] = "輸入成績表顯示資料";
$eReportCard['Finishandpreview'] = "完成及預覽";
$eReportCard['ReportTitle'] = "成績表標題";
$eReportCard['SelectTerm'] = "選擇學期";
$eReportCard['TermReport'] = "學期成績表";
$eReportCard['WholeYearReport'] = "結算成績表";
$eReportCard['Main'] = "主要";
$eReportCard['Extra'] = "額外";
$eReportCard['jsHasMainTermReportAlready'] = "此班級已有設定主要學期成績表。";
$eReportCard['jsHasSameReportAlready'] = "此班級已有同類型成績表。";

$eReportCard['ShowAllAssessments'] = "顯示所有評估項目";
$eReportCard['ShowTermTotalOnly'] = "只顯示學期總分";
$eReportCard['ReportType'] = "成績表類型";
$eReportCard['SettingUpdateReminder'] = "設定已更新，請重新製作。";
$eReportCard['NewReport'] = "新增成績表";
$eReportCard['SkipStep'] = "確定跳過此步?";
$eReportCard['RatioBetweenAssesments'] = "評估項目之間的比例";
$eReportCard['Use'] = "採用";
$eReportCard['MoveLeft'] = "移到左邊";
$eReportCard['MoveRight'] = "移到右邊";
$eReportCard['NoTermSelected'] = "未有選擇學期";
$eReportCard['NoAssesment'] = "未有評估項目。";
$eReportCard['AddTerm'] = "加入學期";
$eReportCard['AddAssesment'] = "加入評估項目";
$eReportCard['AtBegin'] = "移到最左";
$eReportCard['NoSubjectSettings'] = "沒有科目設定";
$eReportCard['ColumnTitle'] = "評估項目標題";
$eReportCard['MissingDefaultWeight'] = "找不到預設比重。";
$eReportCard['DefaultWeightNumeric'] = "預設比重必需是數值。";
$eReportCard['MissingWeight'] = "找不到比重。";
$eReportCard['WeightNumeric'] = "比重必需是數值。";
$eReportCard['SubjectTotleNot100'] = "科目總比重不是100%，確定繼續嗎?";
$eReportCard['InsertAfter'] = "插入．．．後面";
$eReportCard['ShowPosition'] = "顯示名次";
$eReportCard['DonotShowPosition'] = "不顯示名次";
$eReportCard['ShowPositionInClass'] = "顯示全班名次";
$eReportCard['ShowPositioninClassLevel'] = "顯示全級名次";
$eReportCard['AssignToAll'] = "套用全部";
$eReportCard['OverallSubjectWeight'] = "總科目比重";
$eReportCard['MissingData'] = "資料不足";
$eReportCard['RatioNumeric'] = "比例必需為數值";
$eReportCard['RatioSumNot100'] = "比例總和不等於100%，確定繼續嗎?";
$eReportCard['LineHeight'] = "行高";
$eReportCard['Header'] = "頁首";
$eReportCard['WithDefaul Header'] = "使用預設頁首";
$eReportCard['NoHeader'] = "不加入頁首";
$eReportCard['NumberOfEmptyLine'] = "空行的數目";
$eReportCard['Footer'] = "頁尾";
$eReportCard['DisplayStudentInformation'] = "顯示學生資料";
$eReportCard['DisplayColumn'] = "顯示行列";
$eReportCard['ShowSubjectFullMark'] = "顯示科目的滿分";
$eReportCard['ShowSubjectOverall'] = "顯示科目的總分";
$eReportCard['DisplayOverallResult'] = "顯示總成績";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowClassNumOfStudent'] = "顯示班人數";
$eReportCard['ShowFormPosition'] = "顯示級名次";
$eReportCard['ShowFormNumOfStudent'] = "顯示級人數";
# added on 01 Jan 2008
$eReportCard['MaximumPosition'] = "顯示名次的最大值為";
$eReportCard['MaximumPositions'] = "* 若最大值設定為0，系統將顯示所有名次。";
$eReportCard['GreaterThanGrandAverage'] = "高於平均分";
$eReportCard['Mark(s)'] = "分";
$eReportCard['AttendanceDays'] = "出席日數";
$eReportCard['SpecialPercentage'] = "全級首%學生獲得等級 A*";
$eReportCard['PrintOption'] = "科目評語列印";
$eReportCard['PrintOption_1'] = "列印所有科目";
$eReportCard['PrintOption_2'] = "只列印不及格科目";
$eReportCard['PrincipalName'] = "校長姓名";


$eReportCard['CommentFrom'] = "評語來自";
$eReportCard['ClassTeache'] = "班主任";
$eReportCard['SubjectTeacher'] = "科目教師";
$eReportCard['Success'] = "成功 ";
$eReportCard['SuccessWithMissing'] = "成功 - 但找不到部分科目";
$eReportCard['TemplateExists'] = "範本已存在。";
$eReportCard['Copy'] = "複製";
$eReportCard['CopyTo'] = "複製至";
$eReportCard['CopyFrom'] = "複製自";
$eReportCard['CopyResult'] = "成績表複製結果";
$eReportCard['SubmissionEndDateNotReached'] = "呈交結束期限未到。";
$eReportCard['VerificationEndDateNotReached'] = "核對結束期限未到。";
$eReportCard['ClassCommentNotCompleted'] = "班別評語還未完成。";
$eReportCard['MSNotCompleted'] = "分紙還未完成。";
$eReportCard['TemplatePreview'] = "預覽範本";
$eReportCard['Preview'] = "預覽";
$eReportCard['Generate'] = "製作";
$eReportCard['Adjust'] = "調整";
$eReportCard['Generation'] = "版本";
$eReportCard['ManualAdjustment'] = "手動調整";
$eReportCard['LastGenerate'] = "上一次製作";
$eReportCard['LastArchive'] = "上一次存檔";
$eReportCard['LastAdjust'] = "上一次調整";
$eReportCard['AdjustmentList'] = "調整列表";
$eReportCard['LastPrint'] = "上一次列印";
$eReportCard['MarkModifiedOn'] = "分數更新於";
$eReportCard['ReportCardGeneratedOn'] = "成績表製作於";
$eReportCard['LastGenerationDate'] = "最後製作日期";
$eReportCard['LastTransferDate'] = "最後傳送日期";
$eReportCard['LastExportDate'] = "最後匯出日期";
$eReportCard['SchoolYear'] = "學年";
$eReportCard['Semester'] = "學期";
$eReportCard['Transfer'] = "傳送";
$eReportCard['AllTypes'] = "全部類型";
$eReportCard['ViewReportCard'] = "檢視成績表";

// Schedule
$eReportCard['AllSemesters'] = "所有學期";
$eReportCard['AllReportCards'] = "所有成績表";
$eReportCard['AllForms'] = "所有級別";
$eReportCard['ReportTitle'] = "成績表標題";
$eReportCard['Submission'] = "呈交";
$eReportCard['Verification'] = "家長及學生核對";
$eReportCard['PublishReport'] = "公佈";
$eReportCard['IssueDate'] = "發出日期";
$eReportCard['Start'] = "開始";
$eReportCard['End'] = "結束";
$eReportCard['NoSchedule'] = "沒有時間表";
$eReportCard['SubmissionPeriod'] = "呈交時段";
$eReportCard['VerificationPeriod'] = "家長及學生核對時段";
$eReportCard['PublishReportPeriod'] = "報告公佈時段";
$eReportCard['AlertComparePeriod'] = "開始日期必需為結束日期之前。";
$eReportCard['AlertNoSubStartDate'] = "呈交時段的開始日期空白或有錯誤，請再輸入。";
$eReportCard['AlertNoIssueDate'] = "發出日期空白或有錯誤，請再輸入。";
$eReportCard['AlertCompareSubToVer'] = "核對時段必需為呈交校時段之後！";

// Other Info
########### Need to be added if more categories are required ###########
$eReportCard['SummaryInfoUpload'] = "總結資料";
$eReportCard['AwardRecordUpload'] = "獎項紀錄";
$eReportCard['MeritRecordUpload'] = "獎懲紀錄";
$eReportCard['ECARecordUpload'] = "課外活動紀錄";
$eReportCard['RemarkUpload'] = "備註";
$eReportCard['InterSchoolCompetitionUpload'] = "聯校比賽紀錄";
$eReportCard['SchoolServiceUpload'] = "學校服務紀錄";
$eReportCard['AttendanceUpload'] = "考勤紀錄";
$eReportCard['DemeritUpload'] = "懲罰紀錄";
$eReportCard['OLEUpload'] = "其他學習經歷紀錄";
$eReportCard['Post'] = "職責";
$eReportCard['OthersUpload'] = "其他紀錄";
// $eReportCard['PELesson'] = "體藝課: ";
// $eReportCard['ServiceItem'] = "服務項目: ";
// $eReportCard['Award'] = "獎項/成就: ";
// $eReportCard['Remarks'] = "備註: ";
// $eReportCard['Appearance'] = "儀容";
// $eReportCard['Mood'] = "情緒";
// $eReportCard['Courtesy'] = "禮貌";
// $eReportCard['Gregarious'] = "合群";
// $eReportCard['Discipline'] = "紀律";
// $eReportCard['Honesty'] = "誠實";
// $eReportCard['Service'] = "服務";
// $eReportCard['AppearanceOverall'] = "整體表現";
// $eReportCard['AppearanceOverallYear'] = "全學年整體表現";


$eReportCard['OtherInfoArr']['summary'] = "總結資料";
$eReportCard['OtherInfoArr']['award'] = "獎項紀錄";
$eReportCard['OtherInfoArr']['merit'] = "獎懲紀錄";
$eReportCard['OtherInfoArr']['eca'] = "課外活動紀錄";
$eReportCard['OtherInfoArr']['remark'] = "備註";
$eReportCard['OtherInfoArr']['interschool'] = "聯校比賽紀錄";
$eReportCard['OtherInfoArr']['schoolservice'] = "學校服務紀錄";
$eReportCard['OtherInfoArr']['attendance'] = "考勤紀錄";
$eReportCard['OtherInfoArr']['dailyPerformance'] = "日常表現紀錄";
$eReportCard['OtherInfoArr']['demerit'] = "懲罰紀錄";
$eReportCard['OtherInfoArr']['OLE'] = "其他學習經歷紀錄";
$eReportCard['OtherInfoArr']['assessment'] = "評估項目";
$eReportCard['OtherInfoArr']['post'] = "職責";
$eReportCard['OtherInfoArr']['subjectweight'] = "科目比重";
$eReportCard['OtherInfoArr']['others'] = "其他紀錄";
$eReportCard['OtherInfoArr']['additional_comments'] = "附加評語";
$eReportCard['OtherInfoArr']['position'] = "班名次";
$eReportCard['OtherInfoArr']['skills'] = "技能";
$eReportCard['OtherInfoArr']['themeForInvestigation'] = "主題調查";
$eReportCard['OtherInfoArr']['TFIPerformance'] = "主題調查表現";
$eReportCard['OtherInfoArr']['foreignStudent'] = "外國學生";
$eReportCard['OtherInfoArr']['promotion'] = "升留紀錄";
$eReportCard['OtherInfoArr']['reading_academy'] = "校本閱讀計劃";
$eReportCard['OtherInfoArr']['english_as_a_lang'] = "跨科英語學習能力";
$eReportCard['OtherInfoArr']['otherinfor'] = "其他資料";
$eReportCard['OtherInfoArr']['behaviour'] = "行為表現";
$eReportCard['OtherInfoArr']['transcript'] = "成績單";

$eReportCard['ManagementArr']['OtherInfoArr']['ImportDataInstruction'] = "如需要更改已匯入的資料，請先匯出現有的CSV檔案並進行修改，然後才匯入到系統。否則，是次匯入的班別/級別的現有的資料將會被覆蓋。";

########################################################################
$eReportCard['File'] = "檔案";
$eReportCard['AllSchoolYears'] = "整個學年";
$eReportCard['AllTerms'] = "所有學期";
$eReportCard['AllClasses'] = "所有班別";
$eReportCard['FileRemoveConfirm'] = "確定刪除檔案?";
$eReportCard['FileRemoveAllConfirm'] = "確定刪除所有檔案?";
$eReportCard['NoClass'] = "找不到班別";
$eReportCard['NoSemester'] = "找不到學期";
$eReportCard['NoForm'] = "找不到級別";
$eReportCard['Form'] = "級別";
$eReportCard['FormName'] = "級別";
$eReportCard['AlertExistFile'] = "檔案已存在於同一學期的同一班別中。\\n以新的檔案覆蓋?";

// Marksheet Revision & Edit
$eReportCard['Marksheet'] = "分紙";
$eReportCard['TeacherComment'] = "老師評語";
$eReportCard['Feedback'] = "回應";
$eReportCard['LastModifiedDate'] = "最後更新日期";
$eReportCard['LastModifiedBy'] = "最後更新者";
$eReportCard['Confirmed'] = "完成";
$eReportCard['Total'] = "總分";
$eReportCard['RawMarks'] = "原始分數";
$eReportCard['WeightedMarks'] = "比重分數";
$eReportCard['ConvertedGrade'] = "對應等級";
$eReportCard['InputMarks'] = "輸入分數";
$eReportCard['PreviewMarks'] = "預覽分數";
$eReportCard['PreviewGrades'] = "預覽等級";
$eReportCard['Import'] = "匯入";
$eReportCard['Export'] = "匯出";
//$eReportCard['MarkRemind'] = ": \"abs\" 代表缺席; \"/\" 代表沒有登記或免修。";
$eReportCard['MarkRemindSet1'] = "備註: \"+\" 代表缺席(0分); \"-\" 代表缺席(不作考慮); \"*\" 代表退修; \"/\" 代表免修; \"N.A.\" 代表不作評估。";
$eReportCard['MarkRemindSet1_WithEstimate'] = "備註: \"+\" 代表缺席(0分); \"-\" 代表缺席(不作考慮); \"*\" 代表退修; \"/\" 代表免修; \"N.A.\" 代表不作評估; \"#\" 代表估計分數。";
$eReportCard['MarkRemindSet2'] = "備註: \"abs\" 代表缺席; \"*\" 代表退修; \"/\" 代表免修; \"N.A.\" 代表不作評估。";
$eReportCard['MarkRemindSet_SubMS'] = "備註: \"+\" 代表缺席(0分); \"/\" 代表免修。";
$eReportCard['SubMS'] = "附屬分紙";
$eReportCard['SubMarksheet'] = "附屬分紙";
$eReportCard['Student'] = "學生";
$eReportCard['StudentNo_short'] = "學號";
$eReportCard['TermSubjectMark'] = "學期科目分數";
$eReportCard['TermSubjectMarkEn'] = "Term Subject Mark";
$eReportCard['TermSubjectMarkCh'] = "學期科目分數";
$eReportCard['OverallSubjectMark'] = "科目總分";
$eReportCard['OverallSubjectMarkEn'] = "Overall Subject Mark";
$eReportCard['OverallSubjectMarkCh'] = "科目總分";
$eReportCard['CmpSubjectMarksheet'] = "科目分卷分紙";
$eReportCard['CmpSubjects'] = "科目分卷";

$eReportCard['RecordUpdatedSuccessfully'] = "成功更新之紀錄";
$eReportCard['NoRecordIsUpdated'] = "沒有更新紀錄。";
$eReportCard['RecordShowingMSContainEmptyMark'] = "包含空白分數之分紙紀錄";
$eReportCard['CurrentStatus'] = "現時狀況";
$eReportCard['Reason'] = "原因";
$eReportCard['Status'] = "狀況";

$eReportCard['TeachersComment'] = "老師評語";
$eReportCard['Period'] = "時段";

$eReportCard['NotCompleted'] = "未完成";

$eReportCard['ImportMarks'] = "匯入分數";
$eReportCard['ExportMarks'] = "匯出分數";
$eReportCard['DownloadCSV'] = "下載CSV檔案";
$eReportCard['DownloadCSVFile'] = "下載CSV檔案範本";
$eReportCard['CSVMarksheetTemplateReminder'] = "在處理的過程裏，CSV檔案範本內各標題為N/A行列中所有輸入的分數，都會被忽略。";
$eReportCard['Absent'] = "缺席";
$eReportCard['AbsentZeorMark'] = "+ : 缺席 (0分)";
$eReportCard['AbsentNotConsidered'] = "- : 缺席 (不獲考慮)";
$eReportCard['Dropped'] = "* :退修";
$eReportCard['Exempted'] = "/ : 免修";
$eReportCard['NotAssessed'] = "N.A.：不作評估";

$eReportCard['RemarkAbsent'] = "缺席";
$eReportCard['RemarkAbsentZeorMark'] = "+ : 缺席 (0分)";
$eReportCard['RemarkAbsentNotConsidered'] = "- : 缺席 (不獲考慮)";
$eReportCard['RemarkDropped'] = "* :退修";
$eReportCard['RemarkExempted'] = "/ : 免修";
$eReportCard['RemarkNotAssessed'] = "N.A.：不作評估";

$eReportCard['ClearPosition'] = "清除名次";
$eReportCard['jsWarningClearPosition'] = '你是否確定清除本成績表科目內的所有已調整名次?';

$eReportCard['jsFillGradingSchemeTitle'] = '請需入等級計劃標題。';
$eReportCard['jsConfirmDeleteGradingScheme'] = '刪除計劃同時影響其他使用該計劃的科目，是否確認刪除此計劃。';
$eReportCard['jsCheckDisplayResult'] = '當輸入的比例為等級，%1$s所顯示結果的類型變成無效。';
$eReportCard['jsSthMustBePositive'] = '%1$s必需為正數值';
$eReportCard['jsSthMustBeNumericalNumber'] = '%1$s必需為數值。';
$eReportCard['jsSthCannotBeLargerThanFullMark'] = '%1$s不可大於等級計劃內的滿分值。';
$eReportCard['jsInvalidSth'] = '%1$s無效';
$eReportCard['jsCheckTotalUpperLimit'] = '上限總和必需等於100%。';
$eReportCard['jsCheckOverallSubjectWeightedMark'] = '科目的整體比重分數不可大於滿分值。';
# added on 15 Dec 2008
$eReportCard['jsSthCannotBeLessThanPassingMark'] = '%1$s不可少於等級計劃內的合格分數。';
# added on 16 Dec 2008
$eReportCard['jsLowerLimitCannotBeTheSame'] = '分數下限不可重複。';


# added on 29 Apr 2008
$eReportCard['jsAlertToCompleteMarksheet'] = '是否確定完成分紙?';
$eReportCard['jsAlertToIncompleteMarksheet'] = '分紙的狀態將改為未完成，確定繼續?';
$eReportCard['jsInputMarkInvalid'] = '輸入分數無效';
$eReportCard['jsInputMarkCannotBeNegative'] = '所需入分數不可為負數。';
$eReportCard['jsInputMarkCannotBeLargerThanFullMark'] = '所輸入的分數不可高於滿分。';
$eReportCard['jsConfirmMarksheetToIncomplete'] = '分紙已完成，確定進行更新?如選擇「是」，分紙的狀態便會改為 \"未完成\"。';
$eReportCard['jsConfirmCmpSubjectMarksheetToIncomplete'] = '部分分卷己完成，確定更新此分級，如選擇「是」，分紙的狀態將會改為 \"未完成\"。';
$eReportCard['jsAlertDuplicateGradingTitle'] = '等級標題已經存在，請輸入一個新的標題。';
# added on 6 Nov 2008
$eReportCard['jsCheckTotalUpperLimitDistinction&Pass'] = '優異和合格的學生上限總和必需等於100%。';
$eReportCard['jsCheckTotalUpperLimitFail'] = '不合格的學生上限總和必需等於100%。';
# added on 25 Nov 2008
$eReportCard['jsMarkEditedFromSubMS'] = '本頁分數已因應附屬分紙而作出修改，如要儲存已修改的分數，請按"儲存"。';
$eReportCard['jsFirefoxNotAllowPaste'] = '瀏覽器不允許使用貼上功能。請於網址欄輸人 "about:config"，然後將 "signed.applets.codebase_principal_support" 設置為 "true"。';
$eReportCard['jsFillAllNA'] = '你是否確定將所有未輸入的分數更改為 "N.A"?';
# added on 1 Dec 2009
$eReportCard['jsAlertToRemoveSelectedSubject'] = '如你取消這選項，系統將自動移除選項內已選擇的學科。你是否確定取消這選項？';
$eReportCard['jsAlertGreaterMaxNumber'] = '最多不合格科目數目不能大於已選的科目數目';

$eReportCard['jsSelectGradingScheme'] = '請選擇等級計劃予%1$s。';
$eReportCard['jsSelectInputScale'] = '請選擇輸入比例予%1$s。';
$eReportCard['jsSelectDisplayScale'] = '請選擇顯示結果予%1$s。';


# added on 30 Apr 2008
// Marksheet Confirm Complete Status
$eReportCard['MarksheetNotCompleted'] = '分紙未完成。';
$eReportCard['MarksheetOfTermReportNotCompleted'] = '學期成績表分紙未完成。';
$eReportCard['MarksheetOfCmpSubjectNotCompleted'] = '科目分卷分紙未完成。';

// SubMS
$eReportCard['TransferOverallMarkToMS'] = "傳送總分到分紙";

######################

// Class Teacher's Comment
$eReportCard['ChangeAllTo'] = "全部更改至";
$eReportCard['ClassTeacherCommentTemp']['View'] = "[檢視]";
$eReportCard['ClassTeacherCommentTemp']['ViewAll'] = "[檢視全部]";
$eReportCard['ClassTeacherCommentTemp']['Hide'] = "[隱藏]";
$eReportCard['ClassTeacherCommentTemp']['HideAll'] = "[隱藏全部]";

// Progress
$eReportCard['NotSet'] = "Not set";
$eReportCard['SendNotification'] = "送出通告";
$eReportCard['NotStartedYet'] = "未開始";
$eReportCard['InProgress'] = "處理中";
$eReportCard['Feeback'] = "回應";
$eReportCard['SetToComplete'] = "設定為已完成";
$eReportCard['SetToNotComplete'] = "設定為未完成";
$eReportCard['NoSubjectGroup'] = "沒有科組設定";

// Common 
$eReportCard['SettingsNotCompleted'] = "未完成";
$eReportCard['SettingsCompleted'] = "完成";
$eReportCard['SettingsProgress'] = "設定進度";
$eReportCard['ExportStudentInfo'] = "匯出學生資料";
$eReportCard['ExportStudentRecords'] = "匯出學生紀錄";

// Report Template
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassIndexNo'] = "Class Index No.";
$eReportCard['Template']['StudentInfo']['StudentAdmNo'] = "Student Adm. No.";
$eReportCard['Template']['StudentInfo']['UserLogin'] = "內聯網帳號";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['ClassName'] = "班別";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "班號";
$eReportCard['Template']['StudentInfo']['StudentNo'] = "學號";
$eReportCard['Template']['StudentInfo']['ClassTeacher'] = "班主任";
$eReportCard['Template']['StudentInfo']['AcademicYear'] = "學年";
$eReportCard['Template']['StudentInfo']['DateOfIssue'] = "發出日期";
$eReportCard['Template']['StudentInfo']['DateOfBirth'] = "出生日期";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
//$eReportCard['Template']['StudentInfo']['SubjectStream']['A']['Ch'] = "文組人數";
//$eReportCard['Template']['StudentInfo']['SubjectStream']['A']['En'] = "Enrolled (Art&Com)";
//$eReportCard['Template']['StudentInfo']['SubjectStream']['S']['Ch'] = "理組人數";
//$eReportCard['Template']['StudentInfo']['SubjectStream']['S']['En'] = "Enrolled (Science)";
$eReportCard['Template']['StudentInfo']['SubjectStream']['A']['Ch'] = "文商組人數";
$eReportCard['Template']['StudentInfo']['SubjectStream']['A']['En'] = "Art & Com. Enrolled";
$eReportCard['Template']['StudentInfo']['SubjectStream']['S']['Ch'] = "理組人數";
$eReportCard['Template']['StudentInfo']['SubjectStream']['S']['En'] = "Science Enrolled";
$eReportCard['Template']['SubjectEng'] = "Subject";
$eReportCard['Template']['SubjectChn'] = "科目";
$eReportCard['Template']['SubjectTeacherComment'] = "科目老師評語";
$eReportCard['Template']['Ranking'] = "Ranking";
$eReportCard['Template']['ClassTeacher'] = "班主任";
$eReportCard['Template']['Principal'] = "校長";
$eReportCard['Template']['ParentGuardian'] = "家長 / 監護人";
$eReportCard['Template']['SchoolChop'] = "校印";
$eReportCard['Template']['TeacherSignature'] = "班主任簽署";
$eReportCard['Template']['PrincipalSignature'] = "校長簽署";
$eReportCard['Template']['ParentSignature'] = "家長簽署";
$eReportCard['Template']['SubjectOverall'] = "科目總分";
$eReportCard['Template']['FirstCombined'] = "第一次合併";
$eReportCard['Template']['SecondCombined'] = "第二次合併";
$eReportCard['Template']['OverallCombined'] = "總結";
$eReportCard['Template']['GrandTotal'] = "Grand Total";
$eReportCard['Template']['GrandTotal2'] = "總分";
$eReportCard['Template']['AverageMark'] = "Average Mark";
$eReportCard['Template']['AverageMark2'] = "平均分";
$eReportCard['Template']['Position'] = "Position";
$eReportCard['Template']['Position2'] = "名次";
$eReportCard['Template']['NoOfStudent'] = "No. of students";
$eReportCard['Template']['NoOfStudent2'] = "學生數目";
$eReportCard['Template']['Grade'] = "等級";
$eReportCard['Template']['Mark'] = "分數";
$eReportCard['Template']['s'] = "備註";
$eReportCard['Template']['AchievementBand'] = "成就等級";
$eReportCard['Template']['AchievementGrade'] = "成就等級";
$eReportCard['Template']['1SemestralAssessment'] = "<上學期評估>";
$eReportCard['Template']['2SemestralAssessment'] = "<下學期評估>";
$eReportCard['Template']['Overall'] = "< --- --- --- 整體 --- --- --- >";
$eReportCard['Template']['Attendance'] = "考勤";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['Total'] = "總分";
$eReportCard['Template']['Percentage'] = "百分比";
$eReportCard['Template']['Passed'] = "合格";
$eReportCard['Template']['Failed'] = "不合格";
$eReportCard['Template']['Promoted'] = "升級";
$eReportCard['Template']['Retained'] = "留級";
$eReportCard['Template']['Advanced'] = "進階";
$eReportCard['Template']['1Sem'] = "上學期";
$eReportCard['Template']['2Sem'] = "下學期";
$eReportCard['Template']['Assessment'] = "評估項目";
$eReportCard['Template']['CCA'] = "聯課活動";
$eReportCard['Template']['NAPFA'] = "國家體適能獎勵計劃";
$eReportCard['Template']['SSPA_Footer'] = 
"注意:<br />
(一) 本表績分須呈教統局升中派位組，請特別留意該績分是否與試卷相同。<br />
(二) 本績分表只填寫考試分數，不包括常分在內。<br />
(三) 合計後如出現小數位，則將小數位四捨五入，音樂及視覺藝術為五分之差。
";
$eReportCard['Template']['SSPA_Signature'] = "教師署名：______________________";
$eReportCard['Template']['ClassPosition'] = "全班名次";
$eReportCard['Template']['FormPosition'] = "全級名次";
$eReportCard['Template']['StreamPosition'] = "班級分組名次";
$eReportCard['Template']['SubjGroupPosition'] = "科目組別名次";
$eReportCard['Template']['GrandMarksheet'] = "大分紙";
$eReportCard['Template']['GrandAverage'] = "總平均分";
$eReportCard['Template']['ActualAverage'] = "實得平均分";
$eReportCard['Template']['TotalUnitFailed'] = "不合格單位";
$eReportCard['Template']['GrandAverageGrade'] = "總平均分等級";
$eReportCard['Template']['GrandAverageEn'] = "Grand Average";
$eReportCard['Template']['GrandAverageCh'] = "總平均分";
$eReportCard['Template']['GrandTotalEn'] = "Grand Total";
$eReportCard['Template']['GrandTotalCh'] = "總分";
$eReportCard['Template']['GPAEn'] = "GPA";
$eReportCard['Template']['GPACh'] = "總等級分數平均分";
$eReportCard['Template']['GrandStandardScore'] = "總標準分";
$eReportCard['Template']['GrandStandardScoreEn'] = "Grand S.D. Score";
$eReportCard['Template']['GrandStandardScoreCh'] = "總標準分";
$eReportCard['Template']['Annual'] = "全年";
$eReportCard['ForOverallColumnOnly'] = "只適用於總結成績";

// Other Student Info	# added on 28 Apr 2008
$eReportCard['OtherStudentInfo'] = '其他學生資料';
$eReportCard['ChangeAllTo'] = "更改全部至";
$eReportCard['OtherStudentInfo_Attendance'] = array(1=>'Regular', 2=>'Irregular');
$eReportCard['OtherStudentInfo_Conduct'] = array(4=>'優', 3=>'良', 2=>'常', 1=>'可');
$eReportCard['OtherStudentInfo_NAPFA'] = array(1=>'無', 2=>'金', 3=>'銀', 4=>'銅', 5=>'免修');
$eReportCard['OtherStudentInfoValue_CIPHours'] = 'CIP時間';
$eReportCard['OtherStudentInfoValue_Attendance'] = '考勤';
$eReportCard['OtherStudentInfoValue_Conduct'] = '操行';
$eReportCard['OtherStudentInfoValue_NAPFA'] = 'NAPFA';
$eReportCard['OtherStudentInfoValue_CCAs'] = 'CCA';
$eReportCard['jsCIPHourCannotBeNegative'] = 'CIP時間時不可是負數值';
$eReportCard['jsCIPHourInvalid'] = 'CIP時間無效';

// Geneate Report
$eReportCard['jsConfirmToGenerateReport'] = '如你製作成績表，已調整的分數將被刪除，你是否確定製作成績表？';
$eReportCard['ReportPrinting'] = "列印成績表";
$eReportCard['ReportPreview&Printing'] = "預覽或列印整張成績表";
$eReportCard['Promotion'] = "升/留班";
$eReportCard['PromotionEdit'] = "編輯升/留班";
$eReportCard['NewClassLevel'] = "新的級別";
$eReportCard['NewClassLevelAlert'] = "新級別名稱長度至少為兩個字元";

// Grand Marksheet
$eReportCard['SelectCriteria'] = "選擇統計範圍";
$eReportCard['AllSubjects'] = "所有科目";
$eReportCard['GrandMarksheetType'] = "大分紙類型";
$eReportCard['NoReportAvailable'] = "找不到成績表";
$eReportCard['NoTermAvailable'] = "找不到學期";

//$eReportCard['GrandMarksheetTypeOption'] = array("班別總結報告", "全級名次", "學生進度");
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結報告", "全級名次");
$eReportCard['GrandMarksheetTypeAlert1'] = "學生進度不能單以一個學期的成績計算。";
# added on 15 Oct 2008
$eReportCard['ShowSummaryInfo'] = "顯示總結資料";
$eReportCard['ShowSubject'] = "顯示科目"; # added on 3 Dec 2009
$eReportCard['ShowSubjectComponent'] = "顯示科目分卷";
$eReportCard['ShowGrandTotal'] = "顯示總分";
$eReportCard['ShowGrandAverage'] = "顯示總平均分";
$eReportCard['ShowActualAverage'] = "顯示實得平均分";
$eReportCard['ShowTotalUnitFailed'] = "顯示不合格單位";
$eReportCard['StudentNameDisplay'] = "顯示學生姓名";
$eReportCard['ShowGender'] = "顯示學生性別"; 
$eReportCard['ShowUserLogin'] = "顯示內聯網帳號"; 
$eReportCard['ShowGPA'] = "顯示GPA";
$eReportCard['RankingDisplay'] = "顯示名次";
# added on 16 Oct
$eReportCard['For_SSPA'] = "用作中學學位分配呈分";
# added on 19 Dec 2008
$eReportCard['ViewFormat'] = "檢視格式";
$eReportCard['HTML'] = "HTML";
$eReportCard['CSV'] = "CSV";
$eReportCard['PDF'] = "PDF";
$eReportCard['EnglishName'] = "英文名";
$eReportCard['ChineseName'] = "中文名";
$eReportCard['ClassRanking'] = "全班名次";
$eReportCard['OverallRanking'] = "全級名次";
# added on 22 Dec 2008
$eReportCard['Template']['Average'] = "平均數";
$eReportCard['Template']['SD'] = "標準偏差";
$eReportCard['Template']['Variance'] = "Variance";
$eReportCard['Template']['HighestMark'] = "最高分數";
$eReportCard['Template']['LowestMark'] = "最低分數";
$eReportCard['Template']['PassingRate'] = "及格率";
# added on 24 Dec 2008
$eReportCard['ShowStatistics'] = "顯示統計資料";
$eReportCard['Formula'] = "公式";

$eReportCard['Template']['PassingNumber'] = "及格人數";
$eReportCard['Template']['SubjectWeight'] = "科目比重";
$eReportCard['Template']['GrandAvgPassNumber'] = "平均分及格人數(Pass)";
$eReportCard['Template']['GrandAvgFailNumber'] = "平均分不及格人數(Fail)";
$eReportCard['Template']['ActualAvgPassNumber'] = "實得平均分及格人數(Pass)";
$eReportCard['Template']['ActualAvgFailNumber'] = "實得平均分不及格人數(Fail)";
$eReportCard['SubjectDisplay'] = "顯示科目"; 
$eReportCard['Abbr'] = "名稱縮寫 ";
$eReportCard['ShortName'] = "簡稱";
$eReportCard['Desc'] = "說明";
$eReportCard['TotalNum'] = "總人數";
$eReportCard['GrandMarkSheet']['Gender']["M"] = "男生";
$eReportCard['GrandMarkSheet']['Gender']["F"] = "女生";
$eReportCard['WholeForm'] = "全級";
$eReportCard['StatNotShown'] = "如果成績以等級顯示，將不會顯示統計資料";
$eReportCard['NumOfPassSubject'] = "及格科目總數";
$eReportCard['NumOfFailSubject'] = "不及格科目總數";

// Data Handling
$eReportCard['DataTransition'] = "資料轉移";
$eReportCard['DataDeletion'] = "資料刪除";
$eReportCard['DataTransfer_To_iPortfolio'] = "資料傳送(至 iPortfolio)";
$eReportCard['DataExport_To_WebSAMS'] = "資料匯出 (至 WebSAMS)";
$eReportCard['DataTransfer_To_WebSAMS'] = "資料傳送(至 WebSAMS)";
$eReportCard['DataTransitionWarning1'] = "請在以下兩種情況發生前，執行此功能:<br />- 於學年始時，重新啟用eReportCard成績表系統<br />- 還原以往某學年eReportCard成績表系統的資料";
$eReportCard['DataTransitionWarning2'] = "- 系統內現有資料將會被整存。";
$eReportCard['ActiveAcademicYear'] = "系統使用中的學年";
$eReportCard['CurrentAcademicYear'] = "本學年";
$eReportCard['Transition'] = "轉變";
$eReportCard['NewAcademicYear'] = "新學年";
$eReportCard['OtherAcademicYears'] = "其他學年";
$eReportCard['ConfirmCreateNewYearDatabase'] = "新學年資料會複製自學年<!--academicYearName-->，你是否確定轉變系統至新學年？";
$eReportCard['ConfirmTransition'] = "確定進行所選擇的資料轉變?";
$eReportCard['DataDeletionWarning'] = "這個程序將會移除所選擇成績表的資料(包括積分和老師評語)";
$eReportCard['ConfirmDeletion'] = "確定移除所選擇成績表的資料?";
$eReportCard['NextYearTermNotComplete'] = "下學年的學期設定尚未完成。";
$eReportCard['Generated&ArchivedReport'] = "已製作及已存檔成紀錄";
$eReportCard['DeleteMarksheetRemarks'] = "包括分紙及附屬分紙的分數";
$eReportCard['DeleteReportRemarks'] = "包括已製作、已調整及已存檔的分數";
$eReportCard['TargetDeletionRecord'] = "刪除紀錄類型";
$eReportCard['Target'] = "目標";
$eReportCard['jsAtLeastOneDeletionRecord'] = "請選擇最少一項要刪除的紀錄類型。";
$eReportCard['ManagementArr']['DataHandlingArr']['TransferData'] = "傳送資料";
$eReportCard['ManagementArr']['DataHandlingArr']['AcademicResult'] = "學業成績";
$eReportCard['ManagementArr']['DataHandlingArr']['MockExamResult'] = "模擬試成績";
$eReportCard['ManagementArr']['DataHandlingArr']['ClassTeacherComment'] = "班主任評語";
$eReportCard['ManagementArr']['DataHandlingArr']['ForMainTermReportAndConsolidatedReportOnly'] = "只適用於主要學期成績表及結算成績表";
$eReportCard['ManagementArr']['DataHandlingArr']['Conduct'] = "操行";
$eReportCard['ManagementArr']['DataHandlingArr']['SubjectFullMark'] = "學科滿分";
$eReportCard['ManagementArr']['DataHandlingArr']['WarningArr']['SelectData'] = "請選擇要傳送的資料";
$eReportCard['ManagementArr']['DataHandlingArr']['WarningArr']['DuplicatedSelection'] = "請避免選擇重複評估項目連結";
$eReportCard['ManagementArr']['DataHandlingArr']['MockExamScoreFrom'] = "由 <!--reportColumn--> 取得";


$button_promotion = "升/留班";
$button_finish_preview = "完成及預覽";

$eReportCard['DisplayOverallResult'] = "顯示總成績";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowFormPosition'] = "顯示級名次";

# added on 09 Dec 2008
$eReportCard['ManualAdjustMarkInstruction'] = "此頁是選擇性填寫的，如你沒有填上學生的科目名次，系統將因應學生的分數而決定其名次。";

# added on 5 Jan 2008
$eReportCard['SubmissionStartDate'] = "開始呈交日期";
$eReportCard['SubmissionEndDate'] = "結束呈交日期";
$eReportCard['VerificationStartDate'] = "開始核對日期";
$eReportCard['VerificationEndDate'] = "結束核對日期";
$eReportCard['VerificationPeriodIsNotNow'] = "現在不是分數核對時段。";

# added on 3 Feb 2009
$eReportCard['FillAllEmptyMarksWithNA'] = "將所有空白分數設定為「不作評估」";
$eReportCard['TotalRecords'] = "總數";

# added on 27 Apr 2009
$eReportCard['Warning'] = "警告";
$eReportCard['SubjectConductMarkWarning'] = "當你呈送操行分後，在成績表中的操行分顯示，將會由學生操行分的「最終等級」取代其「對應等級」。";

// Data Handling (to iPortfolio)
$eReportCard['jsConfirmToTransferToiPortfolio'] = '你是否確定傳送資料到iPortfolio?';
$eReportCard['CurrentAcadermicYear'] = '本學年';

# 20090627 yatwoon
$eReportCard['NoFormSetting'] = '尚未有年級設定';
$eReportCard['orAbove'] = '或以上';
$eReportCard['AllPass'] = '全部科目必須合格';

// Honour Report
$eReportCard['GPA'] = 'GPA';
$eReportCard['Honour'][1] = '一級榮譽';
$eReportCard['Honour'][2] = '二級榮譽 (第一部份)';
$eReportCard['Honour'][3] = '二級榮譽 (第二部份)';
$eReportCard['AllHonour'] = '榮譽';


$eReportCard['AwardName'] = '獎項名稱';
$eReportCard['StudentNameEn'] = '學生姓名(英文)';
$eReportCard['StudentNameCh'] = '學生姓名(中文)';
$eReportCard['Criteria'] = '準則';
$eReportCard['Template']['GPA'] = 'GPA';
$eReportCard['warnGPANotDesc'] = "請重新選擇學分，每一級榮譽的學分必須高於其以下的榮譽";
$eReportCard['LastYearSchool'] = '去年就讀學校';


// Conduct Report
$eReportCard['Excluded'] = "例外";
$eReportCard['GradeImprovement'] = "進步級數";
$eReportCard['orabove'] = "或以上";
$eReportCard['SelectDiffTerm'] = "請選擇不同的學年";
$eReportCard['Reports_ConductProgressReport'] = "操行進步獎報告";
$eReportCard['OneTermOnly'] = "只有一個學期時不能使用操行進步獎報告";
$eReportCard['warnSelectTerm'] = "請選擇最少一個學期";
$eReportCard['warnSelectConduct'] = "請選擇最少一個操行分";
$eReportCard['warnTermOrder'] = "請順序選擇學期";
$eReportCard['warnExcludedAllConduct'] = "不能設定所有操行分為例外操行分";
$eReportCard['maxRanking'] = "最高排名";

// Scholarship Report
$eReportCard['TargetHonour'] = "等級榮譽";
$eReportCard['AllHonour'] = "所有等級榮譽";
$eReportCard['warnSelectOneTerm'] = "請選擇一個學期";
$eReportCard['warnSelectTwoTerm'] = "請選擇最少兩個學期";
$eReportCard['warnSelectHonour'] ="請選擇最少一個";
$eReportCard['ForScholarship'] = "獎學金用途";

// Grading Scheme
$eReportCard['AdditionalCriteria'] = "附加條件";

// Feedback
$eReportCard['AllStatus'] = "-- 所有狀況 --";
$eReportCard['Closed'] = "已處理";
$eReportCard['NoFeedback'] = "沒有回應";
$eReportCard['EditMarksheet'] = "檢視分紙";

// Manual Adjustment
$eReportCard['MarkAdjustSuccess'] = "1|=|儲存成功";
$eReportCard['MarkAdjustFail'] = "0|=|儲存失敗";

// Archive Report
$eReportCard['jsConfirmToArchiveReport'] = '此成績表的舊有存檔將被重寫，你是否確定你要存檔？';

// Not current academic year warning
$eReportCard['jsNotCurrentAcademicYearInfo'] = '';
$eReportCard['jsNotCurrentAcademicYearInfo'] .= "成績表系統使用中的學年: |||--eRC Active Year--|||";
$eReportCard['jsNotCurrentAcademicYearInfo'] .= '\n';
$eReportCard['jsNotCurrentAcademicYearInfo'] .= "本學年: |||--Current Academic Year--|||";
$eReportCard['jsNotCurrentAcademicYearInfo'] .= '\n\n';
$eReportCard['jsNotCurrentAcademicYearTeacher'] = "成績表系統現在不是使用本學年的資料，如要檢視或處理本學年資料，請與成績表系統管理員聯絡。";
$eReportCard['jsNotCurrentAcademicYearAdmin'] = "成績表系統現在不是使用本學年的資料，如要檢視或處理本學年資料，請到 \\\"管理 > 資料處理 > 資料轉移\\\" 將成績表系統轉移至本學年。";

$eReportCard['jsWarningSelectClass'] = "請選擇班別。";

// Sub-Marksheet Report
$eReportCard['Display'] = "顯示";
$eReportCard['SubMarksheetColumn'] = "附屬分紙";
$eReportCard['NoSubMarksheet'] = "沒有附屬分紙"; 
$eReportCard['jsSelectSubjectWarning'] = "請選擇科目。"; 
$eReportCard['jsSelectClassWarning'] = "請選擇班別。"; 
$eReportCard['jsSelectSubjectTeacherWarning'] = "請選擇科目教師。"; 
$eReportCard['ApplyToAll'] = "全部使用";

// Assessment Report
$eReportCard['DataSource'] = '資料來源';
$eReportCard['DataSourceChoice']['Report'] = '報告';
$eReportCard['DataSourceChoice']['Marksheet'] = '分紙';

// Promotion Settings
$eReportCard['Settings_PromotionCriteria'] = "升級準則";
$eReportCard['PromotionCriteriaSettings'] = "準則設定";
$eReportCard['PromotionInstruction'] = "說明";
$eReportCard['PromotionInstructionContent'] = "學生若符合所有準則，成績表將顯示學生為「升級」，否則，系統將評定學生為試升。";
$eReportCard['PassGrandAverage'] = "全年總平均分合格";
$eReportCard['MaximumFailSubject1'] = "以下科目最多不合格";
$eReportCard['MaximumFailSubject2'] = "科";
$eReportCard['SelectedSubject'] = "已選擇科目";
$eReportCard['MaximumNotSubmitAssignment1'] = "欠交功課最多";
$eReportCard['MaximumNotSubmitAssignment2'] = "次";
$eReportCard['MaximumNotSubmitAssignment3'] = "次數";
$eReportCard['Value'] = "設定值";
$eReportCard['NoPromotionSettings'] = "此級別尚未設定升級準則";

// Examination Summary
$eReportCard['ExaminationSummary']['Subj'] = "科目";
$eReportCard['ExaminationSummary']['Rank'] = "排名";
$eReportCard['ExaminationSummary']['Year'] = "年度";
$eReportCard['ExaminationSummary']['Grade'] = "等級";
$eReportCard['ExaminationSummary']['Mark'] = "分數";
$eReportCard['ExaminationSummary']['Conduct'] = "操行";
$eReportCard['ExaminationSummary']['times'] = "次";
$eReportCard['ExaminationSummary']['days'] = "日";
$eReportCard['ExaminationSummary']['AverageMark'] = "平均分";
$eReportCard['ExaminationSummary']['Absent'] = "缺席";
$eReportCard['ExaminationSummary']['Late'] = "遲到";
$eReportCard['ExaminationSummary']['Position'] = "名次";
$eReportCard['ExaminationSummary']['Cd1'] = "Diligence";
$eReportCard['ExaminationSummary']['Cd2'] = "Discipline";
$eReportCard['ExaminationSummary']['Cd3'] = "Manner";
$eReportCard['ExaminationSummary']['Cd4'] = "Sociability";
$eReportCard['ExaminationSummary']['YearResult'] = "全年成績";
$eReportCard['ExaminationSummary']['Exam'] = "考試";
$eReportCard['ExaminationSummary']['FormTest'] = "測驗";

// Subject Summary Sheet
$eReportCard['ClassNo'] = "班號";
$eReportCard['SubjectSummarySheet']['Percentage'] = "%";
$eReportCard['SubjectSummarySheet']['MaxMark'] = "最高分";
$eReportCard['SubjectSummarySheet']['MinMark'] = "最低分";
$eReportCard['SubjectSummarySheet']['SpecifiedPassingPercentage'] = "指定合格百分比";
$eReportCard['SubjectSummarySheet']['%_OfFullMark'] = "% 的滿分";
$eReportCard['PassRate'] = "合格率";
$eReportCard['SubjectGroup'] = "科目組別";
$eReportCard['ViewBy'] = "檢視類別";

// Grade Distribution
$eReportCard["GradeDistribution"]["total_pass"] = "合格人數";
$eReportCard["GradeDistribution"]["TotalInForm"] = "全級人數";
$eReportCard["GradeDistribution"]["NumOfStudent"] = "人數";
$eReportCard["GradeDistribution"]["Percentage"] = "百分率";
//$eReportCard["Class"] = "Class";

# Lang Display of Form Setting 
$eReportCard['LangDisplay'] = "顯示語言";
$eReportCard['LangDisplayChoice']['ch'] = "中文";
$eReportCard['LangDisplayChoice']['en'] = "英文";
$eReportCard['LangDisplayChoice']['both'] = "中英對照";

# Display SubjectGroup
$eReportCard['DisplaySubjectGroup'] = "顯示科目組別";

# Subject Ranking Display Settings
$eReportCard['AboveAverageMark'] = "高於平均分";
$eReportCard['AboveMark'] = "高於分數";
$eReportCard['NoSubjectSettingInThisForm'] = "此班級未有設定任何學科。";
$eReportCard['PositionRange'] = "名次範圍";
$eReportCard['Top'] = "首";
$eReportCard['Rank'] = "名";
$eReportCard['Range'] = "範圍";
$eReportCard['GrandPosition'] = "總名次";
$eReportCard['PositionRangeDisplays'] = "* 如選擇「名次範圍」及將上限設定為0，系統將顯示所有名次。";

// Settings > Personal Characteristics
$eReportCard['PersonalCharacteristics'] = "個人表現";
$eReportCard['PersonalCharacteristicsPool'] = "個人表現庫";
$eReportCard['PersonalCharacteristicsFormSettings'] = "個人表現班級設定";
$eReportCard['Scale'] = "等級";
$eReportCard['NewPersonalCharacteristics'] = "新增個人表現";
$eReportCard['EditPersonalCharacteristics'] = "編輯個人表現";
$eReportCard['Title_EN'] = "名稱 (英文)";
$eReportCard['Title_CH'] = "名稱 (中文)";
$eReportCard['NoPersonalCharacteristicsSetting'] = "未有個人表現設定。";
$eReportCard['ApplyOtherSubjects'] = "套用到此班級的其他科目";
$eReportCard['Excellent_en'] = "Excellent";
$eReportCard['Good_en'] = "Good";
$eReportCard['Satisfactory_en'] = "Satisfactory";
$eReportCard['NeedsImprovement_en'] = "Needs Improvement";
$eReportCard['NotApplicable_en'] = "Not Applicable";
$eReportCard['Excellent_b5'] = "優";
$eReportCard['Good_b5'] = "良";
$eReportCard['Satisfactory_b5'] = "尚可";
$eReportCard['NeedsImprovement_b5'] = "有待改進";
$eReportCard['NotApplicable_b5'] = "不適用";
$eReportCard['DisplayOrder'] = "顯示次序";
$eReportCard['PersonalChar']['ExportTitleArr']['En'][] = "Class";
$eReportCard['PersonalChar']['ExportTitleArr']['En'][] = "Class No.";
$eReportCard['PersonalChar']['ExportTitleArr']['En'][] = "Student Name";
$eReportCard['PersonalChar']['ExportTitleArr']['Ch'][] = "班別";
$eReportCard['PersonalChar']['ExportTitleArr']['Ch'][] = "學號";
$eReportCard['PersonalChar']['ExportTitleArr']['Ch'][] = "學生姓名";
$eReportCard['PersonalChar']['ExportTitle']['Comment']['En'] = "Comment";
$eReportCard['PersonalChar']['ExportTitle']['Comment']['Ch'] = "評語";

$eReportCard['Button']['back_to_PersonalCharacteristics'] = "Back to ".$eReportCard['PersonalCharacteristics'];
$eReportCard['Represents'] = "代表";

// Settings > Course Description
$eReportCard['ApplyOtherTerm'] = "套用到其他學期";

// Settings > Report template setting
$eReportCard['BlockMarksheet'] = "鎖定分紙";

$eReportCard['ShowSubjectPosition'] = "顯示科目名次";

// Settings > Viewer Group
$eReportCard["UserAccessPages"] = "可存取頁面";
$eReportCard["UserAddedDate"] = "加入日期";

$eReportCard["List"] = "列表";
$eReportCard["AllPromotionStatus"] = "- 所有狀況 -";
$eReportCard["PromotionStatus"][1] = "升級";
$eReportCard["PromotionStatus"][2] = "試級";
$eReportCard["PromotionStatus"][3] = "其他";
$eReportCard["Promoted"] = "升級";
$eReportCard["PromotedonTrial"] = "試級";
$eReportCard["Retained"] = "留級";
$eReportCard["EmptyPromotion"] = "(空白)";
$eReportCard["PromotionStatusInstruction"] = "如果已從CSV上載升級狀況紀錄，最後狀況會使用CSV的升級狀況，否則會使用系統製作狀況。";
$eReportCard["GenerationStatus"] = "系統製作狀況";
$eReportCard["FinalStatus"] = "最後狀況";
$eReportCard["LastPromotionGeneration"] = "上一次製作升級狀況日期";
$eReportCard["ConductGeneratedOn"] = "上一次操行分製作日期";
$eReportCard["ReportGeneratedOn"] = "上一次成績表製作日期";
$eReportCard["PromotionCriteriaUpdatedOn"] = "上一次更新升級準則日期";
$eReportCard["PromotionStatusReport"] = "升級狀況報告";

$eReportCard["ConductNotGenerated"] = "未有在訓導管理系統製作操行分。";
$eReportCard["ReportNotGenerated"] = "未有製作成績表。";

$eReportCard['DeletedStudentLegend'] = '<font style="color:red;">*</font> 表示該學生已離校或已被刪除。';

$eReportCard['GrandStandardScore'] = "總標準分";

$eReportCard['AssessmentRatioRemarks'] = "請以小數輸入，例如0.2";
$eReportCard['AssessmentRatioRemarks2'] = "請以百分比輸入，例如20";

$eReportCard['TermStartDate'] = "學期開始日期";
$eReportCard['TermEndDate'] = "學期完結日期";

$eReportCard['jsWarnRatioNotANumber'] = "比率一定是數字";
$eReportCard['jsWarnFullMarkNotANumber'] = "滿分一定是數字";
$eReportCard['jsWarnPassingMarkNotANumber'] = "合格分數一定是數字";
$eReportCard['jsWarnPassingMarkLargerThanFullMark'] = "合格分數不能大於滿分";

$eReportCard['Mean'] = "平均分";
$eReportCard['StandardDeviation'] = "標準偏差";
$eReportCard['PassingPercentage'] = "合格百分比";
$eReportCard['PassingMark'] = "合格分數";
$eReportCard['No.Sat'] = "應考學生";
$eReportCard['PercentagePass'] = "合格率";
$eReportCard['MaxScore'] = "最低分數";
$eReportCard['MinScore'] = "最高分數";
$eReportCard['JobTitle'] = "職位";

$eReportCard['GeneralArr']['GrandAverage'] = "總平均分";
$eReportCard['GeneralArr']['Ranking'] = "排名";

# Management > Academic Progress
$eReportCard['ManagementArr']['AcademicProgressArr']['GenerationOptions'] = "製作選項";
$eReportCard['ManagementArr']['AcademicProgressArr']['DetermineBy'] = "取決於";
$eReportCard['ManagementArr']['AcademicProgressArr']['FromReportCard'] = "由成績表";
$eReportCard['ManagementArr']['AcademicProgressArr']['ToReportCard'] = "至成績表";
$eReportCard['ManagementArr']['AcademicProgressArr']['FromMark'] = "由…分數";
$eReportCard['ManagementArr']['AcademicProgressArr']['ToMark'] = "至…分數";
$eReportCard['ManagementArr']['AcademicProgressArr']['MarkDifference'] = "進步分數";
$eReportCard['ManagementArr']['AcademicProgressArr']['ClassPosition'] = "班名次";
$eReportCard['ManagementArr']['AcademicProgressArr']['FormPosition'] = "級名次";
$eReportCard['ManagementArr']['AcademicProgressArr']['ProgressPrize'] = "進步獎";
$eReportCard['ManagementArr']['AcademicProgressArr']['TopClassPosition'] = "班名次上限";
$eReportCard['ManagementArr']['AcademicProgressArr']['TopFormPosition'] = "級名次上限";
$eReportCard['ManagementArr']['AcademicProgressArr']['Reports'] = "此報告不能顯示入等級的科目。";
$eReportCard['ManagementArr']['AcademicProgressArr']['SetPrize'] = "給予獎項";
$eReportCard['ManagementArr']['AcademicProgressArr']['CancelPrize'] = "取消獎項";
$eReportCard['ManagementArr']['AcademicProgressArr']['ProgressPrizeStatus'] = "所有獎項狀況";
$eReportCard['ManagementArr']['AcademicProgressArr']['WithPrize'] = "有獎";
$eReportCard['ManagementArr']['AcademicProgressArr']['WithoutPrize'] = "沒有獎";
$eReportCard['ManagementArr']['AcademicProgressArr']['jsWarningArr']['ConfirmGenerate'] = "如你製作進步紀錄至此成績表，已製作的進步紀錄將被重寫。你是否確定製作進步紀錄至此成績表？";
$eReportCard['ManagementArr']['AcademicProgressArr']['jsWarningArr']['SameReportSelected'] = "不能比較相同成績表，請選擇其他成績表。";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr']['GenerateSuccess'] = "1|=|成績進步紀錄製作成功。";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr']['GenerateFailed'] = "0|=|成績進步紀錄製作失敗。";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr']['ProgressPrizeUpdateSuccess'] = "1|=|進步獎已更新。";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr']['ProgressPrizeUpdateFailed'] = "0|=|進步獎更新失敗。";

# Remarks Report
$eReportCard['RemarksReport']['ReportTitle'] = "評語報告";
$eReportCard['RemarksReport']['No.'] = "學號";
$eReportCard['RemarksReport']['Name'] = "姓名";
$eReportCard['RemarksReport']['Remarks'] = "評語";
$eReportCard['RemarksReport']['RemarksFontSize'] = "評語字體大小";

# Subject Pass Percentage Report
$eReportCard['PassPercentageReport']['Subject'] = "科目";
$eReportCard['PassPercentageReport']['NumOfStudent'] = "學生數目";
$eReportCard['PassPercentageReport']['AdjustedPassingScore'] = "已調整及格分數";
$eReportCard['PassPercentageReport']['%PassAdjusted'] = "及格比率 (已調整)";
$eReportCard['PassPercentageReport']['%PassRaw'] = "及格比率 (未調整)";
$eReportCard['PassPercentageReport']['Max'] = "最高分數";
$eReportCard['PassPercentageReport']['Min'] = "最低分數";
$eReportCard['PassPercentageReport']['Mean'] = "平均分數";
$eReportCard['PassPercentageReport']['StandardDeviation'] = "標準偏差";

# Class Teacher Comment
$eReportCard['ClassTeacherComment'] = "班主任評語";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ImportMode'] = "匯入模式";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ImportNew'] = "匯入評語";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['AppendExisting'] = "附加至現有評語";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'] = "學生";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'] = "評語";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['GenerateReport'] = "已製作的報告";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['AdditionalComment'] = "附加評語";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ManualAdjustComment'] = "手動調整評語";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['AddToStudentComment'] = "加入至學生評語";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['SelectedComment'] = "已選擇評語";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ChooseReportCard'] = "選擇成績表";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'] = "選擇學生";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ClassTeacherCommentSubmissionPeriod'] = "班主任評語呈交時段";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['OnlyStudentInTheSelectedFormWillBeShown'] = "系統只會顯示已選擇的級別內的學生。";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr']['AddCommentSuccess'] = "1|=|已成功加入評語。";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr']['AddCommentFailed'] = "0|=|加入評語失敗。";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedCommentWillBeLostIfSearch'] = "如現在搜尋評語，已選擇的評語選項將會取消。你是否確定要搜尋評語？";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment'] = "請選擇至少一則評語。";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedStudentWillBeRemovedIfChangedForm'] = "如現在更改級別，所有已選擇的用戶將被移除。你是否確定要更改級別？";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['ClassName'] = "Class Name";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['ClassName'] = "Class Name";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['ClassNumber'] = "Class Number";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['ClassNumber'] = "Class Number";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['WebSAMSRegNo'] = "WebSAMSRegNumber";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['WebSAMSRegNo'] = "WebSAMSRegNumber";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['StudentName'] = "Student Name";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['StudentName'] = "Student Name";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['CommentContent'] = "Comment Content";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['CommentContent'] = "Comment Content";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['AdditionalComment'] = "Additional Comment";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['AdditionalComment'] = "Additional Comment";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['Conduct'] = "Conduct";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['Conduct'] = "Conduct";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportStudentRemarks'] = "你可使用「Class Name 及 Class Number」<b>或</b>「WebSAMSRegNumber」以作學生配對。";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentSuccess'] = "匯入班主任評語成功。";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentFailed'] = "匯入班主任評語失敗。";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportOtherComments'] = "匯入其他評語";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['GoBackToCommentView'] = "回到檢視班主任評語";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['InvalidConduct'] = "操行分錯誤";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['NotAccessibleStudent'] = "沒有修改此評語的權限";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CommentExceedCharacterLimit'] = "評語超過字數限制";

# Other Info
$eReportCard['ManagementArr']['OtherInfoArr']['WarningMsg'] = "如已上載級別csv檔案，系統將不會使用該級別的班別csv檔案。";
$eReportCard['ManagementArr']['OtherInfoArr']['ExportPastYearData'] = "匯出所有學年資料";

# Master Report
$eReportCard['Overall'] = "總分";
$eReportCard['ReportColumn'] = "評估項目";
$eReportCard['StandardScore'] = "標準分";
$eReportCard['RawStandardScore'] = "原始標準分";
$eReportCard['MSScore'] = "分紙分數";
$eReportCard['FormPosition'] = "全級名次";
$eReportCard['ClassPosition'] = "全班名次";
$eReportCard['StreamPosition'] = "班級分組名次";
$eReportCard['Gender'] = "性別";
$eReportCard['ClassNumber'] = "班號";
$eReportCard['GrandTotalGrade'] = "總分等級";
$eReportCard['GrandAverageGrade'] = "總平均分等級";
$eReportCard['GrandStandardScoreGrade'] = "總標準分等級";
$eReportCard['GPAGrade'] = "GPA等級";
$eReportCard['GrandGrade'] = "總等級";
$eReportCard['NumOfStudent'] = "學生人數";
$eReportCard['NumOfStudentStudyingInClass'] = "全班修讀人數";
$eReportCard['NumOfStudentStudyingInForm'] = "全級修讀人數";
$eReportCard['GrandTotal'] = "總分";
$eReportCard['GrandAverage'] = "平均分";
$eReportCard['Average'] = "平均數";
$eReportCard['SD'] = "標準偏差";
$eReportCard['HighestMark'] = "最高分數";
$eReportCard['LowestMark'] = "最低分數";
$eReportCard['PassingRate'] = "及格率";
$eReportCard['PassingNumber'] = "及格人數";
$eReportCard['FailingRate'] = "不及格率";
$eReportCard['FailingNumber'] = "不及格人數";
$eReportCard['SubjectWeight'] = "科目比重";
$eReportCard['GrandAvgPassNumber'] = "平均分及格人數(Pass)";
$eReportCard['GrandAvgFailNumber'] = "平均分不及格人數(Fail)";
$eReportCard['ClassAndClassNo'] = "班別及班號";
$eReportCard['LowerQuartile'] = "下四分位數 (Q1)";
$eReportCard['Median'] = "中位數 (Q2)";
$eReportCard['UpperQuartile'] = "上四分位數 (Q3)";

$eReportCard['MasterReport']['ReportOption'] = "報告選項";
$eReportCard['MasterReport']['StudentDisplayOption'] = "學生顯示選項";
$eReportCard['MasterReport']['SubjectDisplayOption'] = "科目顯示選項";
$eReportCard['MasterReport']['GrandMarkDisplayOption'] = "總成績顯示選項";
$eReportCard['MasterReport']['StatisticDisplayOption'] = "統計資料顯示選項";
$eReportCard['MasterReport']['SortingOption'] = "排序選項";
$eReportCard['MasterReport']['SortBy'] = "排序方式";
$eReportCard['MasterReport']['OtherInfoDisplayOption'] = "其他資料顯示選項";
$eReportCard['MasterReport']['ShowOption'] = "顯示選項";
$eReportCard['MasterReport']['HideOption'] = "隱藏選項";
$eReportCard['MasterReport']['DisplayFormat'] = "顯示格式";
$eReportCard['MasterReport']['ClassDisplayArr']['ShowFormSummary'] = "在同一個表格內顯示整個級別的班別";
$eReportCard['MasterReport']['ClassDisplayArr']['ShowClassSummary'] = "在不同的表格顯示不同的班別";
$eReportCard['MasterReport']['SubjectGroupDisplayArr']['ShowFormSummary'] = "在同一個表格內顯示所有科組";
$eReportCard['MasterReport']['SubjectGroupDisplayArr']['ShowClassSummary'] = "在不同的表格顯示不同的科組";
$eReportCard['MasterReport']['SubjectGroupDisplayArr']['ShowSubjectGroupSubjectOnly'] = "在不同的表格顯示不同的科組，並只顯示該科組的科目";
$eReportCard['MasterReport']['ShowSubjectGroupSubjectOnly'] = "顯示科組的科目";
$eReportCard['MasterReport']['DataDisplay'] = "顯示資料";
$eReportCard['MasterReport']['ColumnDataDisplay'] = "評估項目顯示資料";
$eReportCard['MasterReport']['SubjectDataDisplay'] = "科目顯示資料";
$eReportCard['MasterReport']['DisplayNameInOneLine'] = "姓名顯示於同一行";
$eReportCard['MasterReport']['DisplayStyle'] = "報告格式";
$eReportCard['MasterReport']['Border'] = "框線";
$eReportCard['MasterReport']['Space'] = "空白";
$eReportCard['MasterReport']['SubjectComponentSeparator'] = "子科目分格";
$eReportCard['MasterReport']['SubjectSeparator'] = "科目分格";
$eReportCard['MasterReport']['BorderBetweenSubjectData'] = "科目資料用框線隔開";
$eReportCard['MasterReport']['FontSize'] = "文字大小";
$eReportCard['MasterReport']['StudentBottomLine'] = "水平分隔線";
$eReportCard['MasterReport']['Every'] = "每";
$eReportCard['MasterReport']['Row'] = "行";
$eReportCard['MasterReport']['SubjectComponentFontSize'] = "子科目文字大小";
$eReportCard['MasterReport']['ReportStyle1']= "格式一";
$eReportCard['MasterReport']['ReportStyle2']= "格式二";
$eReportCard['MasterReport']['ReportStyle3']= "格式三";
$eReportCard['MasterReport']['ReportStyle4']= "格式四";
$eReportCard['MasterReport']['DisplayColumnLabel']= "顯示評估項目標籤";
$eReportCard['MasterReport']['DisplaySubjectDataLabel']= "顯示科目分數標籤";
$eReportCard['MasterReport']['ColumnDisplayOrder']= "欄位排列";
$eReportCard['MasterReport']['ReportPreset']= "報告預設";
$eReportCard['MasterReport']['Preset']= "預設";
$eReportCard['MasterReport']['DefaultPresetName'] = "new preset";
$eReportCard['MasterReport']['DisplaySubjectWithAllNA'] = '顯示全班學生未選修科目';
$eReportCard['MasterReport']['EmptySymbol'] = "不作評估(NA)資料顯示";
$eReportCard['MasterReport']['EmptySubjectDataForNA'] = '以空白顯示"N.A."的科目';
$eReportCard['MasterReport']['FormatWithStyle'] = "顯示樣式";
$eReportCard['MasterReport']['ShowClassTeacher'] = "顯示班主任";
$eReportCard['MasterReport']['RegistrationNo'] = "註冊號碼";
$eReportCard['MasterReport']['ApplyFailStyleToScore']= "套用不及格樣式";
$eReportCard['MasterReport']['ApplyFailStyleToScoreRemarks']= "如套用此選項，系統將無視「顯示樣式」選項。";

$eReportCard['MasterReport']['jsWarningArr']['SelectStudentdisplayInfo'] = "請最少選擇一項學生顯示資料。";
$eReportCard['MasterReport']['jsWarningArr']['SelectReportColumn'] = "請最少選擇一項評估項目。";
$eReportCard['MasterReport']['jsWarningArr']['SelectSubjectData'] = "請最少選擇一項科目顯示資料。";
$eReportCard['MasterReport']['jsConfirmArr']['DeletePreset'] = "確定刪除預設?";
$eReportCard['MasterReport']['jsConfirmArr']['ModifyPreset'] = "確定修改預設?";
$eReportCard['MasterReport']['jsConfirmArr']['UseDefaultPresetName'] = "確定預設名稱為".$eReportCard['MasterReport']['DefaultPresetName']."?";

$eReportCard['MasterReport']['LastestTerm'] = "最後學期";
$eReportCard['MasterReport']['WholeYear'] = "全年";
$eReportCard['MasterReport']['ReportTitle'] = "報告標題";

// Student Info
$eReportCard['MasterReport']['DataShortName']['UserLogin'] = "UserLogin";
$eReportCard['MasterReport']['DataShortName']['ClassName'] = "Class";
$eReportCard['MasterReport']['DataShortName']['ClassNumber'] = "No.";
$eReportCard['MasterReport']['DataShortName']['ChineseName'] = "Chinese Name";
$eReportCard['MasterReport']['DataShortName']['EnglishName'] = "English Name";
$eReportCard['MasterReport']['DataShortName']['Gender'] = "Gender";
$eReportCard['MasterReport']['DataShortName']['RegNo'] = "RegNo";

// Subject Marks
$eReportCard['MasterReport']['DataShortName']['Mark'] = "M";
$eReportCard['MasterReport']['DataShortName']['RawMark'] = "RM";
$eReportCard['MasterReport']['DataShortName']['MSScore'] = "MSS";
$eReportCard['MasterReport']['DataShortName']['SDScore'] = "SD";
$eReportCard['MasterReport']['DataShortName']['RawSDScore'] = "RSD";
$eReportCard['MasterReport']['DataShortName']['Grade'] = "G";
$eReportCard['MasterReport']['DataShortName']['OrderMeritForm'] = "OMF";
$eReportCard['MasterReport']['DataShortName']['OrderMeritClass'] = "OMC";
$eReportCard['MasterReport']['DataShortName']['OrderMeritSubjectGroup'] = "OMSG";
$eReportCard['MasterReport']['DataShortName']['OrderMeritStream'] = "OMST";
$eReportCard['MasterReport']['DataShortName']['SubjectTeacherComment'] = "科目老師評語";

// Grand Marks
$eReportCard['MasterReport']['DataShortName']['GrandTotal'] = "GT";
$eReportCard['MasterReport']['DataShortName']['GrandGrade'] = "GG";
$eReportCard['MasterReport']['DataShortName']['GrandAverage'] = "GA";
$eReportCard['MasterReport']['DataShortName']['ActualAverage'] = "AA";
$eReportCard['MasterReport']['DataShortName']['GrandSDScore'] = "GSD";
$eReportCard['MasterReport']['DataShortName']['GPA'] = "GPA";
$eReportCard['MasterReport']['DataShortName']['OrderMeritForm'] = "OMF";
$eReportCard['MasterReport']['DataShortName']['OrderMeritClass'] = "OMC";
$eReportCard['MasterReport']['DataShortName']['NoOfPassSubject'] = "Pass";
$eReportCard['MasterReport']['DataShortName']['NoOfFailSubject'] = "Fail";
$eReportCard['MasterReport']['DataShortName']['Promotion'] = "Prom't";
$eReportCard['MasterReport']['DataShortName']['ClassTeacherComment'] = "班主任評語";

// Statistic
$eReportCard['MasterReport']['DataShortName']['Mean'] = "平均數";
$eReportCard['MasterReport']['DataShortName']['SD'] = "標準偏差";
$eReportCard['MasterReport']['DataShortName']['HighestMark'] = "最高分數";
$eReportCard['MasterReport']['DataShortName']['LowestMark'] = "最低分數";
$eReportCard['MasterReport']['DataShortName']['PassingRate'] = "及格率";
$eReportCard['MasterReport']['DataShortName']['PassingNumber'] = "及格人數";
$eReportCard['MasterReport']['DataShortName']['FailingRate'] = "不及格率";
$eReportCard['MasterReport']['DataShortName']['FailingNumber'] = "不及格人數";
$eReportCard['MasterReport']['DataShortName']['SubjectWeight'] = "科目比重";
$eReportCard['MasterReport']['DataShortName']['NumOfStudent'] = "學生人數";
$eReportCard['MasterReport']['DataShortName']['NumOfStudentStudyingInClass'] = "全班修讀人數";
$eReportCard['MasterReport']['DataShortName']['NumOfStudentStudyingInForm'] = "全級修讀人數";
$eReportCard['MasterReport']['DataShortName']['LowerQuartile'] = "下四分位數 (Q1)";
$eReportCard['MasterReport']['DataShortName']['Median'] = "中位數 (Q2)";
$eReportCard['MasterReport']['DataShortName']['UpperQuartile'] = "上四分位數 (Q3)";

// Other Info
$eReportCard['MasterReport']['OtherInfo'][''] = "";
$eReportCard['MasterReport']['OtherInfo']['Days Absent'] = "DABS";
$eReportCard['MasterReport']['OtherInfo']['Time Late'] = "LATE";
$eReportCard['MasterReport']['OtherInfo']['Conduct'] = "COND";
$eReportCard['MasterReport']['OtherInfo']['Minor Demerit'] = "Minor Demerit";
$eReportCard['MasterReport']['OtherInfo']['Major Demerit'] = "Major Demerit";
$eReportCard['MasterReport']['OtherInfo']['ECA'] = "ECA";
$eReportCard['MasterReport']['OtherInfo']['Minor Credit'] = "Minor Credit";
$eReportCard['MasterReport']['OtherInfo']['Major Credit'] = "Major Credit";
$eReportCard['MasterReport']['OtherInfo']['Minor Fault'] = "Minor Fault";
$eReportCard['MasterReport']['OtherInfo']['Major Fault'] = "Major Fault";
$eReportCard['MasterReport']['OtherInfo']['Merits'] = "Merits";
$eReportCard['MasterReport']['OtherInfo']['Demerits'] = "Demerits";

$eReportCard['MasterReport']['PromotionArr'][1] = "";
$eReportCard['MasterReport']['PromotionArr'][0] = "R";
$eReportCard['MasterReport']['PromotionArr'][2] = "C";
$eReportCard['MasterReport']['PromotionArr'][3] = "";

$eReportCard['SelectFrom'] = "選擇類別";

// Preset Report Name - BIBA Cust
$eReportCard['Template']['ReportName']['ESProgressReport'] = "學期進度評語報告";
$eReportCard['Template']['ReportName']['MSProgressReport'] = "學期進度報告";
$eReportCard['Template']['ReportName']['SemesterReport'] = "學期成績報告";
$eReportCard['Template']['ReportName']['FinalReport'] = "學期成績終期報告";

// Master Report Uccke Cust
$eReportCard['MasterReport']['Marks'] = "分數";
$eReportCard['MasterReport']['WithPassingMarkEqualsTo'] = "合格分數等於";
$eReportCard['MasterReport']['PercentageOfCh'] = "總分的";
$eReportCard['MasterReport']['PercentageOfEn'] = "";

$eReportCard['MasterReport']['NoWrap'] = "避免自動換行";
$eReportCard['MasterReport']['ExcludeComponentSubject'] = "不包括學科分卷";
// Master Report Sao Jose (Macau) Cust
$eReportCard['MasterReport']['SubjectUnit'] = "單位";
$eReportCard['MasterReport']['LeftStudent'] = "退學";
$eReportCard['MasterReport']['PrintDate'] = "列印日期";
$eReportCard['MasterReport']['Signature'] = "簽名";
$eReportCard['MasterReport']['SignatureDate'] = "日期";
$eReportCard['MasterReport']['DataShortName']['FailSubjectUnit'] = "單位";

// Other Info Import
$eReportCard['ImportOtherInfoSuccess'] = "其他資料匯入成功。";
$eReportCard['ImportOtherInfoFail'] = "其他資料匯入失敗。";
$eReportCard['ImportWarningArr']['WrongWebSamsRegNo '] = "沒有此學生";
$eReportCard['ImportWarningArr']['WrongUserLogin'] = "沒有此學生";
$eReportCard['ImportWarningArr']['WrongClassNameNumber'] = "沒有此學生";
$eReportCard['ImportWarningArr']['EmptyClassNumber'] = "班號空白";
$eReportCard['ImportWarningArr']['EmptyClassName'] = "班別空白";
$eReportCard['ImportWarningArr']['EmptyStudentData'] = "學生資料空白";
$eReportCard['ImportWarningArr']['WrongDataFormat'] = "資料格式錯誤";
$eReportCard['ImportWarningArr']['InvilidMark'] = "分數輸入錯誤";
$eReportCard['ImportWarningArr']['MarkGreaterThanFullMark'] = "分數大於滿分";
$eReportCard['ImportWarningArr']['EmptyMark'] = "分數空白";
$eReportCard['ImportWarningArr']['InputGrade'] = "輸入等級";
$eReportCard['ImportWarningArr']['InputNumber'] = "輸入數字";



// Subject Prize Report
$eReportCard['ReportArr']['SubjectPrize']['jsWarningArr']['SubjectGroupRankingMustSelectATerm'] = "如要檢視科組科目獎，你必須選擇其中一個學期。";
$eReportCard['ReportArr']['SubjectPrize']['NumOfPrize'] = "獎項數目";

############################################################

##### For Re-exam module #####
$eReportCard['ReExam'] = "補考";
$eReportCard['ReExamStudentList'] = "補考學生列表";
$eReportCard['ReExamScore'] = "補考分數";
$eReportCard['UploadReExamResult'] = "上載補考分數";
$eReportCard['LastUploadedDate'] = "最後上載日期";
$eReportCard['LastUploadedBy'] = "最後上載者";

$eReportCard['Download'] = "下載";

$eReportCard['Template']['GraduationExam'] = array();
$eReportCard['Template']['GraduationExam'][0] = "全科合格，准予參加畢業試。";
$eReportCard['Template']['GraduationExam'][1] = "符合學校規定，准予參加畢業試。";
$eReportCard['Template']['GraduationExam'][2] = "成績不合格，應予留級。";

$eReportCard['Template']['Graduation'] = array();
$eReportCard['Template']['Graduation'][0] = "全科合格，准予畢業。";
$eReportCard['Template']['Graduation'][1] = "成績不合格，不得畢業。";
$eReportCard['Template']['Graduation'][2] = "<!--PassSubjects-->補考合格，准予畢業。";
$eReportCard['Template']['Graduation'][3][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['Graduation'][3][1] = "<!--FailedSubjects-->補考不合格，不得畢業。";

$eReportCard['Template']['Promotion'] = array();
$eReportCard['Template']['Promotion'][0] = "全科合格，准予升級。";
$eReportCard['Template']['Promotion'][1] = "成績不合格，應予留級。";
$eReportCard['Template']['Promotion'][2] = "<!--PassSubjects-->補考合格，准予升級。";
$eReportCard['Template']['Promotion'][3][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['Promotion'][3][1] = "<!--FailedSubjects-->補考不合格，應予留級。";
$eReportCard['Template']['Promotion'][4][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['Promotion'][4][1] = "<!--FailedSubjects-->補考不合格，准予帶科升級。";

$eReportCard['Template']['SubjectSeparator'] = "、";

$eReportCard['Template']['NoReExamRequired'] = "沒有需要補考的成績表範本。";
##### End of Re-exam module #####

$eReportCard['jsConfirmArr']['GradingSchemeWillBeCovered'] = "等級計劃將被複製到目標級別的所有成績表，原來的等級計劃將被覆蓋，繼續？";
$eReportCard['jsConfirmArr']['ReportGradingSchemeWillBeCovered'] = "等級計劃將被複製到目標成績表，該成績表原來的等級計劃將被覆蓋，繼續？";
$eReportCard['jsConfirmArr']['FormGradingSchemeWillBeCovered'] = "等級計劃將被複製到此級別的所有成績表，原來的等級計劃將被覆蓋，繼續？";
$eReportCard['GradingSchemeNotSet'] = "等級計劃尚未設定";

$eReportCard['AllSubjectGroup'] = "所有科組";

$eReportCard['ImportHeader']['ClassName']['En'] = "Class Name";
$eReportCard['ImportHeader']['ClassNumber']['En'] = "Class Number";
$eReportCard['ImportHeader']['UserLogin']['En'] = "User Login";
$eReportCard['ImportHeader']['WebSamsRegNo']['En'] = "WebSams Reg. No.";
$eReportCard['ImportHeader']['StudentName']['En'] = "Student Name".$Lang['General']['ImportArr']['Reference']['En'];
$eReportCard['ImportHeader']['Subject']['En'] = "Subject";
$eReportCard['ImportHeader']['SubjectComponentCode']['En'] = "Subject Component Code";
$eReportCard['ImportHeader']['SubjectGroup']['En'] = "Subject Group";
$eReportCard['ImportHeader']['SubjectGroupCode']['En'] = "Subject Group Code";
$eReportCard['ImportHeader']['Feedback']['En'] = "Feedback";
$eReportCard['ImportHeader']['LastModified']['En'] = "Last Modified";
$eReportCard['ImportHeader']['Status']['En'] = "Status";
$eReportCard['ImportHeader']['SubjectGroup']['En'] = "Subject Group";
$eReportCard['ImportHeader']['ClassName']['Ch'] = "(班別)";
$eReportCard['ImportHeader']['ClassNumber']['Ch'] = "(班號)";
$eReportCard['ImportHeader']['UserLogin']['Ch'] = "(內聯網帳號)";
$eReportCard['ImportHeader']['WebSamsRegNo']['Ch'] = "(WebSAMS 註冊號碼)";
$eReportCard['ImportHeader']['StudentName']['Ch'] = "(學生姓名)".$Lang['General']['ImportArr']['Reference']['Ch'];
$eReportCard['ImportHeader']['Subject']['Ch'] = "(科目)";
$eReportCard['ImportHeader']['SubjectGroup']['Ch'] = "(科組)";
$eReportCard['ImportHeader']['Feedback']['Ch'] = "(回應)";
$eReportCard['ImportHeader']['LastModified']['Ch'] = "(最後修改日期)";
$eReportCard['ImportHeader']['Status']['Ch'] = "(狀況)";

$eReportCard['ExportHeader']['ClassName']['En'] = "Class Name";
$eReportCard['ExportHeader']['ClassNumber']['En'] = "Class Number";
$eReportCard['ExportHeader']['UserLogin']['En'] = "User Login";
$eReportCard['ExportHeader']['WebSamsRegNo']['En'] = "WebSams Reg. No.";
$eReportCard['ExportHeader']['StudentName']['En'] = "Student Name";
$eReportCard['ExportHeader']['Subject']['En'] = "Subject";
$eReportCard['ExportHeader']['SubjectGroup']['En'] = "Subject Group";
$eReportCard['ExportHeader']['Feedback']['En'] = "Feedback";
$eReportCard['ExportHeader']['LastModified']['En'] = "Last Modified";
$eReportCard['ExportHeader']['Status']['En'] = "Status";
$eReportCard['ExportHeader']['SubjectGroup']['En'] = "Subject Group";
$eReportCard['ExportHeader']['TeacherComment']['En']="Teacher Comment";
$eReportCard['ExportHeader']['ClassName']['Ch'] = "(班別)";
$eReportCard['ExportHeader']['ClassNumber']['Ch'] = "(班號)";
$eReportCard['ExportHeader']['UserLogin']['Ch'] = "(內聯網帳號)";
$eReportCard['ExportHeader']['WebSamsRegNo']['Ch'] = "(WebSAMS 註冊號碼)";
$eReportCard['ExportHeader']['StudentName']['Ch'] = "(學生姓名)";
$eReportCard['ExportHeader']['Subject']['Ch'] = "(科目)";
$eReportCard['ExportHeader']['SubjectGroup']['Ch'] = "(科組)";
$eReportCard['ExportHeader']['Feedback']['Ch'] = "(回應)";
$eReportCard['ExportHeader']['LastModified']['Ch'] = "(最後修改日期)";
$eReportCard['ExportHeader']['Status']['Ch'] = "(狀況)";
$eReportCard['ExportHeader']['TeacherComment']['Ch']="(老師評語)";

$eReportCard['FailToImportMarksheetScore'] = "分數匯入失敗";

$eReportCard['ClearAllNA'] = "清除所有 N.A.";

$eReportCard['Simple'] = "簡單";
$eReportCard['Normal'] = "正常";
$eReportCard['ReportView'] = "報告格式";

$eReportCard['ForeignStd'] = "外國學生";
$eReportCard['NormalStd'] = "一般學生";
$eReportCard['AcademicReport'] = "成績表";

$eReportCard['ExtraInfoLabel'] = "努力";
$eReportCard['Template']['SelectEffort'] = "選擇努力";
$eReportCard['Template']['WrongEffortValue'] = "努力數值不正確";

$eReportCard['DisplayColumnNum'] = "顯示欄位數量";

# Marksheet Feedback
$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['FeedbackInProgress'] = "分紙回應仍在處理中";
$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['ExportFeedback'] = "匯出分紙回應";

$eReportCard['ManagementArr']['SchdeuleArr']['ApplySpecificSubmissionPeriod'] = "使用特定呈交時段";
$eReportCard['ManagementArr']['SchdeuleArr']['SpecificSubmissionPeriod'] = "特定呈交時段";
$eReportCard['ManagementArr']['SchdeuleArr']['AvailableTeacher'] = "可選老師";
$eReportCard['ManagementArr']['SchdeuleArr']['SelectedTeacher'] = "已選老師";
$eReportCard['ManagementArr']['SchdeuleArr']['WarningArr']['PleaseSelectTeacher'] = "請選擇老師";
$eReportCard['ManagementArr']['SchdeuleArr']['WarningArr']['PleaseInputDate'] = "請輸入日期";

### Award Generation
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardNotYetGeneratedAfterReportGeneration'] = "成績表製作後未有製作獎項。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardGeneration'] = "製作獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GenerateAward'] = "製作獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratingAward'] = "製作獎項中";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardSettings'] = "獎項設定";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardCriteria'] = "獎項條件";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardName'] = "獎項名稱";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardCode'] = "獎項編號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardForm'] = "獎項相關級別";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] = "獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Student'] = "學生";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['NoAwardSettingsForThisReportAndForm'] = "此報告及級別沒有獎項設定。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['SubjectPrizeSuffix'] = "(科目)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankRangeDisplay'] = "全 <!--RankFieldSelection--> 第 <!--FromRankSelection--> 名至第 <!--ToRankSelection--> 名";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['PersonalCharDisplay'] = "素質各方面至少獲得 <!--PersonalCharCodeSelection-->";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['LinkedAwardSettingsDisplay'] = "<!--AwardSelection--> 第 <!--FromRankSelection--> 名至第 <!--ToRankSelection--> 名";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ExcludeStudentFromOriginalAward'] = "學生不會獲得原有獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DetermineBy'] = "取決於";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['MinimumImprovement'] = "最少進步";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Rank(s)'] = "名";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Mark(s)'] = "分";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Quota'] = "名額";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['FromReport'] = "由成績表";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ToReport'] = "至成績表";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['QuotaRemarks'] = "如沒有名額限制，請選擇「0」。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['MustBePass'] = "必須合格";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DoNotIncludeAdjustedMarkRemarks'] = "製作獎項時系統<b>不會</b>考慮手動調整的分數。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['BackToReportGeneration'] = "返回成績表製作";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['StudentBasedList'] = "學生為本列表 (包含已手動更新的獎項資料)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardBasedList'] = "獎項為本列表 (只顯示系統製作獎項資料)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'] = "檢視獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['StudentName'] = "學生姓名";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['SavingAwardsData'] = "儲存獎項資料中";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ClassView'] = "班別檢視";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardView'] = "獎項檢視";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['NoOfAwardedStudent'] = "得獎學生數目";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportStudentAwardSuccess'] = "匯入學生獎項資料成功。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportStudentAwardFailed'] = "匯入學生獎項資料失敗。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportOtherAwards'] = "匯入其他獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GoBackToViewAwards'] = "回到檢視學生獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DeleteAward'] = "刪除獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AddAward'] = "新增獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AddStudent'] = "新增學生";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardRanking'] = "獎項排名";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratedFromReport'] = "由「<!--reportName-->」產生";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ManualInput'] = "手動輸入";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['PreviousCriteriaRelationArr']['AND'] = "及";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['PreviousCriteriaRelationArr']['OR'] = "或";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['Form'] = "級";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['Class'] = "班";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['SubjectGroup'] = "科組";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandAverage'] = "總平均分";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandTotal'] = "總分";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandGPA'] = "總GPA";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandSDScore'] = "總標準分";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['Mark'] = "分數";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['SDScore'] = "標準分";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritClass'] = "班名次";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritForm'] = "級名次";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritSubjectGroup'] = "科組名次";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['FromRankCannotLargerThenToRank'] = "起始名次不能大於結束名次";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['MinImprovement_Mark'] = "「最少進步分數」必須為正數";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['MinImprovement_Rank'] = "「最少進步分數」必須為正整數";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['GenerateAward'] = "如製作獎項，所有已製作的獎項將被刪除。你是否確定要製作獎項？";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoClassSettings'] = "未有班別設定";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoStudentInClass'] = "此班別沒有學生";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardSettings'] = "未有獎項設定";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardedStudentSettings'] = "沒有學生獲得此獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['DeleteAward'] = "你是否確定要刪除此學生的此獎項？";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardGenerateSuccess'] = "1|=|製作獎項成功。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardGenerateFailed'] = "0|=|製作獎項失敗。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardSaveSuccess'] = "1|=|儲存獎項成功。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardSaveFailed'] = "0|=|儲存獎項失敗。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardDeleteSuccess'] = "1|=|刪除獎項成功。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardDeleteFailed'] = "0|=|刪除獎項失敗。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardAddSuccess'] = "1|=|新增獎項成功。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardAddFailed'] = "0|=|新增項失敗。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardReorderSuccess'] = "1|=|更新獎項排名成功。";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardReorderFailed'] = "0|=|更新獎項排名失敗。";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['ClassName'] = "Class Name";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['ClassName'] = "班別";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['ClassNumber'] = "Class Number";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['ClassNumber'] = "班號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['UserLogin'] = "User Login";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['UserLogin'] = "內聯網帳號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['WebSAMSRegNo'] = "WebSAMSRegNo";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['WebSAMSRegNo'] = "WebSAMS 註冊號碼";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['StudentName'] = "Student Name";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['StudentName'] = "學生姓名";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['Award'] = "Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['Award'] = "獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardCode'] = "Award Code";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['AwardCode'] = "獎項代號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['Subject'] = "Subject";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['Subject'] = "學科";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['SubjectWebSAMSCode'] = "Subject WebSAMS Code";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['SubjectWebSAMSCode'] = "學科 WebSAMS 代號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardName'] = "Award Name";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardCode'] = "Award Code";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardForm'] = "Award related Form";

$Lang['eReportCard']['OverallResult'] = "總成績";

$Lang['eReportCard']['WarningArr']['Select']['Class'] = "請選擇班別。";
$Lang['eReportCard']['WarningArr']['Select']['SubjectGroup'] = "請選擇科組。";
$Lang['eReportCard']['WarningArr']['Select']['Subject'] = "請選擇科目。";
$Lang['eReportCard']['WarningArr']['Select']['Award'] = "請選擇獎項。";
$Lang['eReportCard']['WarningArr']['Select']['AtLeastOneStudent'] = "請選擇至少一個學生。";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['SignatureFileTypeWarning'] = "檔案必須為圖片 (.gif, .jpg, .jpeg, .png, .bmp)";
$Lang['eReportCard']['WarningArr']['Select']['Form'] = "請選擇級別。";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['FailSubject'] = "學科成績不及格";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline'] = "懲罰記錄達<!--Type-->";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline1'] = "懲罰記錄達勸告或缺點";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline2'] = "懲罰記錄達小過";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline3'] = "懲罰記錄達大過";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['RejectAwardWhenFailConduct'] = "因學生操行爲乙以下，未能為學生新增獎項";

$Lang['eReportCard']['ImportWarningArr']['empty_row'] = "沒有輸入資料";
$Lang['eReportCard']['ImportWarningArr']['student_not_found'] = "沒有此學生資料";
$Lang['eReportCard']['ImportWarningArr']['option_no_right'] = "沒有權限匯入此選項";
$Lang['eReportCard']['ImportWarningArr']['code_not_found'] = "沒有此編碼";
$Lang['eReportCard']['ImportWarningArr']['comment_too_long'] = "評語超過字數限制";
$Lang['eReportCard']['ImportWarningArr']['award_not_found'] = "沒有此獎項";
$Lang['eReportCard']['ImportWarningArr']['subject_not_found'] = "沒有此學科";
$Lang['eReportCard']['ImportWarningArr']['form_subject_not_found'] = "此級別沒有修讀此學科";
$Lang['eReportCard']['ImportWarningArr']['student_has_award_already'] = "此學生已有此獎項";
$Lang['eReportCard']['ImportWarningArr']['empty_sg_code'] = "沒有輸入科組代號";
$Lang['eReportCard']['ImportWarningArr']['sg_code_not_found'] = "沒有此科組";
$Lang['eReportCard']['ImportWarningArr']['comp_code_not_found'] = "沒有此學科分卷";
$Lang['eReportCard']['ImportWarningArr']['student_not_in_sg'] = "學生不屬於此科組";
$Lang['eReportCard']['ImportWarningArr']['not_teaching_student'] = "閣下沒有任教有關此學生的科目";
$Lang['eReportCard']['ImportWarningArr']['empty_award_name'] = "沒有輸入獎項名稱";
$Lang['eReportCard']['ImportWarningArr']['duplicate_award_name'] = "獎項名稱已被使用";
$Lang['eReportCard']['ImportWarningArr']['award_form_not_found'] = "沒有此級別";

$eReportCard['HKUGAGradeList']['Student'] = "Student";
$eReportCard['HKUGAGradeList']['Achievement'] = "Achievement";
$eReportCard['HKUGAGradeList']['Attitude'] = "Attitude";
$eReportCard['HKUGAGradeList']['EG'] = "EG";
$eReportCard['HKUGAGradeList']['CG'] = "CG";
$eReportCard['HKUGAGradeList']['YG'] = "YG";
$eReportCard['HKUGAGradeList']['Term'] = "Term";
$eReportCard['HKUGAGradeList']['Subject'] = "Subject";
$eReportCard['HKUGAGradeList']['Comments'] = "Comments";
$eReportCard['HKUGAGradeList']['SubjectTeacher'] = "Subject Teacher";
$eReportCard['HKUGAGradeList']['HeadOfDepartment'] = "Head of Dept";
$eReportCard['HKUGAGradeList']['Date'] = "Date";

$eReportCard['HKUGAGradeList']['AchievementArr'][] = "Responses to Stimuli and Coordination In Humans (I)";
$eReportCard['HKUGAGradeList']['AchievementArr'][] = "Responses to Stimuli and Coordination In Humans (II)";
$eReportCard['HKUGAGradeList']['AchievementArr'][] = "Cellular Energetics (II): Photosynthesis & Respiration";

$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Preparation for lessons";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Attitude to learning";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Quality of assignments / presentations";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Presentation of work";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Independent learning";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Participation in class";

$eReportCard['HKUGAGradeList']['GradeArr']['EG'] = "Examination";
$eReportCard['HKUGAGradeList']['GradeArr']['YG'] = "Year Grade";
$eReportCard['HKUGAGradeList']['GradeArr']['CG'] = "Continuous Assessment";

$eReportCard['FormSubjectReportAnalysis']['DataTypeArr']['Title'] = "分析類別";
$eReportCard['FormSubjectReportAnalysis']['DataTypeArr']['Term'] = "學期";
$eReportCard['FormSubjectReportAnalysis']['DataTypeArr']['Assessment'] = "考績";
$eReportCard['FormSubjectReportAnalysis']['DataTypeArr']['CA'] = "平時分考績";

### Marksheet Revision
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['PreviewReport'] = "預覽報告";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['NoOfEstimatedScore'] = "評估分數數量";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SynchronizeFromEnrol'] = "同步自課外活動管理系統";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SynchronizeFromEnrolWarning'] = "如你現在將分數同步自課外活動管理系統，現有的分數將被重寫。你是否確定要將分數同步自課外活動管理系統？";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SaveBeforeLeaveWarning'] = "你已更改分紙分數，你是否想於離開此頁面前儲存已更改的分數？";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['ChangeLostInputWarning'] = "更改<!--inputfield-->將導致更改的分數紀錄被移除。你是否確定要繼續？";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['UpdateAfterChangeStudent'] = "更改的分數紀錄將被更新。你是否確定要繼續？";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['NoExistingSubjectTopics'] = "暫時未有科目範疇。";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['GoToMarksheet'] = "前往分紙";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['PrintTime'] = "列印時間";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['Signature'] = "簽署";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['ExportDetails'] = "匯出分紙輸入資料";

### Schedule
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['AddTeacher'] = "加入老師";

### General
$Lang['eReportCard']['GeneralArr']['ImportStudentRemarks'] = "你可使用「班別及班號」<b>或</b>「內聯網帳號」<b>或</b>「WebSAMS 註冊號碼」以作學生配對。";
$Lang['eReportCard']['GeneralArr']['WarningArr']['HaveScaleDefaultAlready'] = "已有預設等級";

## Personal Characteristic Schedule
$eReportCard['PCSubmissionPeriod'] = "個人表現呈交時段";

$eReportCard['CommentTooLong'] = "其中評語於行數%s過長並已被截短。";
$eReportCard['CommentTooLongInFile'] = "其中評語於csv檔裡行數%s過長並已被截短。";
$eReportCard['ConductNotFound'] = "在操行庫中沒有行數%s中的操行。";
$eReportCard['CourseDescription']['Overall'] = "整體";
$eReportCard['GradingSchemeArr']['Description'] =  "描述";
$eReportCard['Comment'] = "評語";
$eReportCard['AddComment'] = "新增評語";
$eReportCard['CommentRelatedGrade'] = "對應等級";
$eReportCard['CommentViewPreviousTerm'] = "檢視以前學期評語";

# Trial Promotion
$eReportCard['TrialPromotion']['ReportTitle'] = "試升名單";
$eReportCard['TrialPromotion']['Report'] = '報告';
$eReportCard['TrialPromotion']['Conduct']= "操行";
$eReportCard['TrialPromotion']['BelowOneSubject'] = "低於'1'的科目";
$eReportCard['TrialPromotion']['LateAbsTruantTotal'] = "缺席, 遲到, 曠課次數";
$eReportCard['TrialPromotion']['Truant'] = "曠課";
$eReportCard['TrialPromotion']['DisapprovedLeave'] = "不獲批核缺課";
$eReportCard['TrialPromotion']['EarlyLeave'] = "早退";
$eReportCard['TrialPromotion']['Late'] = "遲到";
$eReportCard['TrialPromotion']['SickLeave'] = "病假";
$eReportCard['TrialPromotion']['LeaveWithReason'] = "事假";


$eReportCard['TrialPromotion']['trialCSVheader']['En']['Class'] = "Class";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['ClassNo'] = "ClassNo";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['Student'] = "Student";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['BelowOneSubject'] = "Below '1' Subject";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['Conduct'] = "Conduct";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['LateAbsTruantTotal'] = "Total number of being late, absent and truant";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['Truant'] = "Truant";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['DisapprovedLeave'] = "Disapproved Leave";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['EarlyLeave'] = "Early Leave";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['Late'] = "Late";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['SickLeave'] = "Sick Leave";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['LeaveWithReason'] = "Leave with Reason";


$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Class'] = "班別";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['ClassNo'] = "班號";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Student'] = "學生";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['BelowOneSubject'] = "低於'1'的科目";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Conduct'] = "操行";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['LateAbsTruantTotal'] = "缺席, 遲到, 曠課次數";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Truant'] = "曠課";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['DisapprovedLeave'] = "不獲批核缺課";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['EarlyLeave'] = "早退";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Late'] = "遲到";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['SickLeave'] = "病假";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['LeaveWithReason'] = "事假";


### Subject Academic Result
$eReportCard['SubjectAcademicResult']['ReportOption'] = "報告選項";
$eReportCard['SubjectAcademicResult']['StudentDisplayOption'] = "學生顯示選項";
$eReportCard['SubjectAcademicResult']['SubjectDisplayOption'] = "科目顯示選項";
$eReportCard['SubjectAcademicResult']['ShowOption'] = "顯示選項";
$eReportCard['SubjectAcademicResult']['HideOption'] = "隱藏選項";
$eReportCard['SubjectAcademicResult']['StudentReport'] = "學生報告";
$eReportCard['SubjectAcademicResult']['Overall'] = "總分";

$eReportCard['SubjectAcademicResult']['En']['SchoolName'] = "FANLING KAU YAN COLLEGE";
$eReportCard['SubjectAcademicResult']['En']['Subject'] = "Subject";
$eReportCard['SubjectAcademicResult']['En']['SimpleStat'] = "Simple Statistics";
$eReportCard['SubjectAcademicResult']['En']['ForAmendOnly'] = "For amendment only";
$eReportCard['SubjectAcademicResult']['En']['Name'] = "Name";
$eReportCard['SubjectAcademicResult']['En']['ReceivedDate'] = "Received date";
$eReportCard['SubjectAcademicResult']['En']['Signature'] = "Signature";
$eReportCard['SubjectAcademicResult']['En']['SDO'] = "SDO";
$eReportCard['SubjectAcademicResult']['En']['Office'] = "Office";
$eReportCard['SubjectAcademicResult']['En']['SubjectTeacher'] = "Subject Teacher";
$eReportCard['SubjectAcademicResult']['En']['Checker'] = "Checker";
$eReportCard['SubjectAcademicResult']['En']['SubjCoordinator'] = "Subject Co-ordinator";
$eReportCard['SubjectAcademicResult']['En']['Date'] = "Date";
$eReportCard['SubjectAcademicResult']['En']['GradeScale'] = "Grade Scale";
$eReportCard['SubjectAcademicResult']['En']['Abs'] = "Abs";

$eReportCard['SubjectAcademicResult']['En']['Tot'] = "Tot";
$eReportCard['SubjectAcademicResult']['En']['Mean'] = "Mean";
$eReportCard['SubjectAcademicResult']['En']['SD'] = "SD";
$eReportCard['SubjectAcademicResult']['En']['Max'] = "Max";
$eReportCard['SubjectAcademicResult']['En']['Min'] = "Min";

$eReportCard['SubjectAcademicResult']['En']['Frequency'] = "Frequency";

$eReportCard['SubjectAcademicResult']['En']['Entry'] = "Entry";
$eReportCard['SubjectAcademicResult']['En']['LA'] = "LA";
$eReportCard['SubjectAcademicResult']['En']['LearningAtt'] = "Learning Attitude";
$eReportCard['SubjectAcademicResult']['En']['AvgGradeArr']['Word']="Avg. Grade";
$eReportCard['SubjectAcademicResult']['En']['AvgGradeArr']['Number']="1*,1-6,6*";
$eReportCard['SubjectAcademicResult']['En']['GradePtArr']['Word']="Grade Pt";
$eReportCard['SubjectAcademicResult']['En']['GradePtArr']['Number']="3.2,3-0";
$eReportCard['SubjectAcademicResult']['En']['MarksArr']['Word']="Marks";
$eReportCard['SubjectAcademicResult']['En']['MarksArr']['Number']="100-0";
$eReportCard['SubjectAcademicResult']['En']['GradeArr']['Word']="Grade";
$eReportCard['SubjectAcademicResult']['En']['GradeArr']['Number']="A1-U";
$eReportCard['SubjectAcademicResult']['En']['ReasonArr']['Word']="Reason";
$eReportCard['SubjectAcademicResult']['En']['ReasonArr']['Number']="(For 3.2/1/0)";

### Personal Characteristics
$Lang['eReportCard']['PersonalCharArr']['NoOfManagementUser'] = "管理用戶數目";
$Lang['eReportCard']['PersonalCharArr']['DefaultScaleSetAlready'] = "已有預設等級";
$Lang['eReportCard']['PersonalCharArr']['NewScaleStepArr'][1] = "資料";
$Lang['eReportCard']['PersonalCharArr']['NewScaleStepArr'][2] = "選擇成員";
$Lang['eReportCard']['PersonalCharArr']['ReturnMsgArr']['ScaleReorderSuccess'] = "1|=|排列等級成功";
$Lang['eReportCard']['PersonalCharArr']['ReturnMsgArr']['ScaleReorderFailed'] = "0|=|排列等級失敗";
$Lang['eReportCard']['PersonalCharArr']['MemberList'] = "管理用戶列表";
$Lang['eReportCard']['PersonalCharArr']['AddMember'] = "加入管理用戶";
$Lang['eReportCard']['PersonalCharArr']['ManagementUser'] = "管理用戶";
$Lang['eReportCard']['PersonalCharArr']['WarningArr']['SelectionContainsMember'] = "已選擇的用戶中包含管理用戶";
$Lang['eReportCard']['PersonalCharArr']['WarningArr']['NoUserSelectionMeansAllCanEdit'][] = "如不選擇任何管理用戶，所有用戶皆能選擇此等級。";
$Lang['eReportCard']['PersonalCharArr']['WarningArr']['NoUserSelectionMeansAllCanEdit'][] = "如選擇了管理用戶，則只有所選用戶能選擇此等級，如成績表管理員未有加入管理等級，則成績表管理員也不能選擇此等級。";
$Lang['eReportCard']['PersonalCharArr']['WarningArr']['ScaleLinkedToDataAlready'] = "已輸入的學生資料包含將被刪除的等級，如刪除等級，學生資料將受影響，而該等級及有關學生資料將不能回復。你是否確定你要刪除等級？";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['AddScaleSuccess'] = "1|=|新增等級成功。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['AddScaleFailed'] = "0|=|新增等級失敗。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['EditScaleSuccess'] = "1|=|編輯等級成功。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['EditScaleFailed'] = "0|=|編輯等級失敗。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['DeleteScaleSuccess'] = "1|=|刪除等級成功。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['DeleteScaleFailed'] = "0|=|刪除等級失敗。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['AddScaleMemberSuccess'] = "1|=|已加入等級管理組員。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['AddScaleMemberFailed'] = "0|=|等級管理組員加入失敗。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['DeleteScaleMemberSuccess'] = "1|=|已刪除等級管理組員。";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['DeleteScaleMemberFailed'] = "0|=|等級管理組員刪除失敗。";

$Lang['eReportCard']['PersonalCharArr']['ExportFormData'] = "匯出全級資料";
$Lang['eReportCard']['PersonalCharArr']['ExportClassData'] = "匯出全班資料";
$Lang['eReportCard']['PersonalCharArr']['Comment'] = "評語";

// Marksheet Verification
$Lang['eReportCard']['AdditionalInfo']['Attendance'] = "考勤紀錄";
$Lang['eReportCard']['AdditionalInfo']['Absent'] = "缺席";
$Lang['eReportCard']['AdditionalInfo']['Late'] = "遲到";
$Lang['eReportCard']['AdditionalInfo']['Days'] = "日數";
$Lang['eReportCard']['AdditionalInfo']['Period'] = "節數";
$Lang['eReportCard']['AdditionalInfo']['MeritsDemerits'] = "獎懲紀錄";
$Lang['eReportCard']['AdditionalInfo']['Merits'] = "優點";
$Lang['eReportCard']['AdditionalInfo']['Demerits'] = "缺點";
$Lang['eReportCard']['AdditionalInfo']['MinorMerit'] = "小功";
$Lang['eReportCard']['AdditionalInfo']['MajorMerit'] = "大功";
$Lang['eReportCard']['AdditionalInfo']['MinorDemerit'] = "小過";
$Lang['eReportCard']['AdditionalInfo']['MajorDemerit'] = "小過";
$Lang['eReportCard']['VerificationStatus'] = "核對分紙狀態";
$Lang['eReportCard']['AllVerificationStatus'] = "所有核對分紙狀態";
$Lang['eReportCard']['VerificationStatusComplete'] = "已完成";
$Lang['eReportCard']['VerificationStatusIncomplete'] = "未完成";
$Lang['eReportCard']['CompletedVerification'] = "設定核對分紙狀態為「已完成」";
$Lang['eReportCard']['IncompletedVerification'] = "設定核對分紙狀態為「未完成」";

$eReportCard['PromoteRetainQuit']['PromotionAndRetention'] = "帶科升級及留級";

// Print Archived Report
$eReportCard['SelectStudentMethod'] = "選擇學生方式";
$eReportCard['UserLogin'] = "內聯網帳號";
$eReportCard['ClassHistory'] = "班級紀錄";
$eReportCard['SelectStudent'] = "選擇學生";
$eReportCard['SelectReport'] = "選擇報告";
$eReportCard['AcademicYear'] = "學年";
$eReportCard['StudentNotFound'] = "找不到此學生";
$eReportCard['WarningArr']['NoArchivedReport'] = "找不到成績表存檔。";
$eReportCard['jsWarningArr']['PleaseInputUserLogin'] = "請輸入內聯網帳號。";
$eReportCard['jsWarningArr']['PleaseSelectStudent'] = "請選擇學生。";
$eReportCard['jsWarningArr']['PleaseSelectReport'] = "請選擇報告。";
$eReportCard['jsWarningArr']['PleaseSelectAcademicYear'] = "請選擇學年。";
$eReportCard['jsWarningArr']['PleaseSelectSemester'] = "請選擇學期。";
$eReportCard['jsWarningArr']['InvalidUserLogin'] = "內聯網帳號不正確。";


// Conduct 
$eReportCard['Conduct'] = "操行";
$eReportCard['jsWarningArr']['PleaseInputConduct'] = "請輸入操行。";
$eReportCard['jsWarningArr']['ConductExist'] = "操行已經存在。";

// Foreign Student
$eReportCard['foreignStudentArr']['Instruction'] = "說明";
$eReportCard['foreignStudentArr']['Remark'] = "是否外國學生 : Y = 外國學生, N = 本地學生";

// Manual Adjustment
$eReportCard['ManualAdjustmentArr']['UpdateAssessmentPosition'] = "修改評估項目的名次";
$eReportCard['Result'] = "成績";

// Comment Max Length Settings
$Lang['eReportCard']['CommentArr']['MaxNumberOfChar'] = "字元數目上限";

// Final Comment Report
$Lang['eReportCard']['ReportArr']['FinalCommentArr']['FinalComment'] = "學年結語";

// Re-exam List
$Lang['eReportCard']['ReportArr']['ReExamListArr']['ReExamSubject'] = "補考科目";

// Promotion Summary Report
$Lang['eReportCard']['ReportArr']['PromotionArr']['ReportName'] = "升留班會議資料";

// Wong Kam Fai Secondary Transcript
$Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentSource'] = "選擇學生方式";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['FormClass'] = "級別班別";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['UserLogin'] = "內聯網帳號";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['ClassHistory'] = "過往班級紀錄";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfIssue'] = "派發日期";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfGraduation'] = "畢業日期";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfLeaving'] = "離校日期";

$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['DeleteSignature'] = "刪除簽名";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['DeleteSchoolChop'] = "刪除校印";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['BackToTeacherExtraInfo'] = "回到教師額外資料";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['SignatureFileTypeWarning'] = "簽名檔案必須為圖片 (.gif, .jpg, .jpeg, .png, .bmp)";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['SchoolChopFileTypeWarning'] = "校印檔案必須為圖片 (.gif, .jpg, .jpeg, .png, .bmp)";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['ImagesUploadedSuccessfully'] = "<!--NumOfImage--> 相片成功上載。";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadedSignature'] = "已上載簽名";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['NotYetUploadedSignature'] = "未上載簽名";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadedSchoolChop'] = "已上載校印";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['NotYetUploadedSchoolChop'] = "未上載校印";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "相片的檔案名稱要與教職員的登入編號一樣。";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "建議使用細於 1MB 的相片。";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "只接受 .gif, .jpg, .jpeg, .png, 或 .bmp 檔的相片。";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "可以以壓縮檔案(.zip)形式上載多個檔案。";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "壓縮檔案內不能存在文件夾。";


$Lang['eReportCard']['GeneralArr']['ToDigitalArchive'] = "至電子文件";
$Lang['eReportCard']['GeneralArr']['ArchiveOption'] = "存檔選項";

$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "成績表範本設計已有所改動，請於存檔前到「".$eReportCard['Reports']." > ".$eReportCard['Reports_GenerateReport']."」確認成績表範本。";
$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "<br />";
//$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "以下是曾作出的改動：";
//$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "<!--changesLog-->";
//$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "<br />";
$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "已存檔的紀錄將被永久移除，而且無法恢復。如確定要進行成績表存檔，請按<font style=\"font-size:large;\">存檔</font>。";

$Lang['eReportCard']['GradingSchemeArr']['removeSchemeWarning'][] = "所選擇等級計劃正被使用中，請於完成移除後到「".$eReportCard['Settings']." > ".$eReportCard['Settings_SubjectsAndForm']."」更新級別設定。";
$Lang['eReportCard']['GradingSchemeArr']['removeSchemeWarning'][] = "這個程序將會永久移除所選擇等級計劃，而且無法恢復。 如確定要進行移除，請按<font style=\"font-size:large;\">呈送</font>。";

$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['CoverPage'] = "報告封面";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['ReportLogo'] = "報告圖像";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['ReportHeader'] = "頂部圖像";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['GradeDesc_DSE'] = "等級描述 (DSE)";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['GradeDesc_IB'] = "等級描述 (IB)";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['DeleteImage'] = "刪除圖像";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['DeleteImageFailed'] = "未能刪除圖像";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['NotInSubmissionPeriod'] = "現在不是呈交時段";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CommentTooLong'] = "評語超出字元上限。";

$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheet'] = "分紙複製";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheetWarning'] = "所有於已選擇的成績表及評估項目內的分數將被重寫，而且無法恢復。如確定要複製分紙，請按呈送。";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheetConfirmWarning'] = "所有於已選擇的成績表及評估項目內的分數將被重寫，而且無法恢復。你是否確定要複製分紙？";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['SameSelectionWarning'] = "「由成績表」和「至成績表」的選項不能一樣";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['FromReport'] = "由成績表";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['ToReport'] = "至成績表";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['AllReportColumn'] = "所有評估項目";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopySuccess'] = "1|=|分紙複雜成功";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyFailed'] = "0|=|分紙複雜失敗";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['ReportColumnNotMatched'] = "0|=|已選擇的成績表評估項目數量不一致";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['ImportAllSubject'] = "匯入全部科目";

$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['AllFormAllSubject'] = '全部級別及全部學科';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ThisFormAllSubject'] = '此級別的全部學科';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ThisFormThisSubject'] = '此級別的此學科';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['FormNameEn'] = 'Form Name';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectCodeEn'] = 'Subject Code';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectNameEn'] = 'Subject Name';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentCodeEn'] = 'Subject Component Code';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentNameEn'] = 'Subject Component Name';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicCodeEn'] = 'Subject Topic Code';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameEnEn'] = 'Subject Topic Name (Eng)';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameChEn'] = 'Subject Topic Name (Chi)';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicTermEn'] = 'Subject Topic Term';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['FormNameCh'] = '級別名稱';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectCodeCh'] = '學科代號';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectNameCh'] = '學科名稱';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentCodeCh'] = '學科分卷代號';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentNameCh'] = '學科分卷名稱';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicCodeCh'] = '學科範疇代號';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameEnCh'] = '學科範疇名稱 (英文)';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameChCh'] = '學科範疇名稱 (中文)';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicTermCh'] = '學科範疇學期';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoForm'] = '級別名稱不能留空';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['FormNotExisting'] = '級別名稱不存在';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectCode'] = '學科代號不能留空';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectNotExisting'] = '學科代號不存在';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectNotExisting'] = '學科分卷代號不存在';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicCode'] = '學科範疇代號不能留空';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicName'] = '學科範疇名稱 (中文) 及 (英文) 不能同時留空';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicNameEn'] = '學科範疇名稱 (英文) 不能留空';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicNameCh'] = '學科範疇名稱 (中文) 不能留空';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicTerm'] = '學科範疇學期不能留空';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectTopicTermNotExisting'] = '學科範疇學期不存在';

$Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['MainSubject'] = '主科';
$Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['Subject'] = '學科';
$Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['TechnicalSubject'] = '術科';

$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['PercentageOfStudent'] = "學生百分比";
$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['MarkGreaterThanOrEqualToFullMarkPercentage'] = "合格分數等於總分的<!--textbox-->%";
$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['Test'] = "測驗";
$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['DailyMark'] = "平時分";
$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['Exam'] = "考試";

$Lang['eReportCard']['ReportArr']['TransferResultToAwardStudentList'] = '傳送結果至學生得獎名單';

$Lang['eReportCard']['ReportArr']['GradingReportArr']['NumOfAcademicYear'] = '學年數量';
$Lang['eReportCard']['ReportArr']['GradingReportArr']['ShowResultWithTerm'] = '顯示本年度 <!--curTermSel--> 與過往 <!--pastTermSel--> 資料';

$Lang['eReportCard']['ReportArr']['AwardPredictionArr']['Predict'] = "完成處理第二段學校成績 (作估算用途)";
$Lang['eReportCard']['ReportArr']['AwardPredictionArr']['Confirm'] = "完成處理第三段學校成績 (作確定用途)";

$Lang['eReportCard']['Test'] = "測驗";
$Lang['eReportCard']['Exam'] = "考試";

$eReportCard['Security'] = "保安";

// Subject Type
// $eReportCard['SubjectType'] = '學科分類';
// $eReportCard['SubjectCredit'] = '學科佔分';
// $eReportCard['SubjectLowerLimit'] = '學分門檻';
// $eReportCard['NotSet'] = '未設定';
// $eReportCard['SubjectCore'] = '主修/必修科目';
// $eReportCard['SubjectElection'] = '選修科目';
// $eReportCard['SubjectUnknown'] = '其他科目';

// Subject Type
$eReportCard['SubjectType']['Type'] = '科目類別';
$eReportCard['SubjectType']['SubjectUnit'] = '學科單位';
$eReportCard['SubjectType']['SubjectDisplayLimit'] = '最低顯示分數';
$eReportCard['SubjectType']['NoSetting'] = '未設定';
$eReportCard['SubjectType']['NotApplicable'] = "不合適";
$eReportCard['SubjectType']['SubjectNormal'] = '基本/普通學科';
$eReportCard['SubjectType']['SubjectElective'] = '選修學科';
$eReportCard['SubjectType']['SubjectCompetitive'] = '競賽學科';

$eReportCard['GradeSettings'] = "表現評價";

// Subject Stream
$eReportCard['SubjectStream']['Selection'] = '文商/理組';
$eReportCard['SubjectStream']['Art'] = '文商組';
$eReprotCard['SubjectStream']['Science'] = '理組';
$eReportCard['SubjectStream']['ReturnMsg']['UpdateSuccess'] = "1|=|更新設定成功";
$eReportCard['SubjectStream']['ReturnMsg']['UpdateFailed'] = "0|=|更新設定失敗";

$eReportCard['Reports_StudentYearlyResultReport'] = '匯出學生全年表現成績';

$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ConductScore'] = '操行分';
$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ConductGrade'] = '操行等級';
$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ModifyGrade'] = '調整操行等級';
$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['WarningMsgArr']['GenerateConduct'] = "如製作操行等級，所有已製作的操行等級將被刪除。你是否確定要製作操行等級？";
$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['WarningMsgArr']['NotGenerated'] = "未有製作操行等級";

$Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['Promotion'] = '升班顯示內容';
$Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['ReExamPassed'] = '補考合格顯示內容';
$Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['ReExamFailed'] = '補考不合格顯示內容';


?>