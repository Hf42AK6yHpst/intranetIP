<?php
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/

// common words
$iPort['iPortfolio'] = "iPortfolio";
$iPort["submitted"] = "Submitted";
$iPort["content"] = "Content";
$iPort["scheme_list"] = "Scheme List";
$iPort["new_phase"] = "New Phase";
$iPort["new_phase_for"] = "New Phase for";
$iPort["edit_phase"] = "Edit Phase";
$iPort["new_scheme"] = "New Scheme";
$iPort["edit_scheme"] = "Edit Scheme";
$iPort["start"] = "Start";
$iPort["end"] = "End";
$iPort["title"] = "Title";
$iPort["description"] = "Description";
$iPort["participating_group"] = "Participating Group";
$iPort["choose_group"] = "Choose Group";
$iPort["selected_groups"] = "Selected Group(s)";
$iPort["status"] = "Status";
$iPort["activate"] = "Activate";
$iPort["deactivate"] = "Deactivate";
$iPort["participants"] = "Participants";
$iPort["subject"] = "Subject";
$iPort["contents"] = "Contents";
$iPort["answer_display_mode"] = "Answer Display Mode";
$iPort["display_answer_only"] = "Display answer only";
$iPort["display_answers_and_choices"] = "Display answers and choices";
$iPort["phase_title"] = "Phase Title";
$iPort["related_phase"] = "Related Phase";
$iPort["choose_phase"] = "Choose Phase(s)";
$iPort["selected_phase"] = "Selected Phase(s)";
$iPort["create_form"] = "Create Form";
$iPort["form_display_v"] = "Question Format";
$iPort["form_display_h"] = "Brief Format";
$iPort["informatics"] = "Informatics";
$iPort["form"] = "Form";
$iPort['enter_scheme_name'] = "Enter Scheme Name";
$iPort["all_school_years"] = "All School Years";
$iPort["last_updated"] = "Last Updated";
$iPort["phase"] = "Phase";
$iPort["finished"] = "Finished";
$iPort["period"] = "Period";
$iPort["ele"] = "Components of OLE";
$iPort["student_submission_period"] = "Submission Period";
$iPort["student_period_settings"] = "Period Settings";
$iPort["record_approval"] = "Record Approval";
$iPort["new"] = "New";
$iPort["options_select"] = "Please select";
$iPort["start_time"] = "Start Time";
$iPort["end_time"] = "End Time";
$iPort["display"]= "Display";
$iPort["additionalInformation"] = "Additional Information";
$iPort["order"] = "Order";
$iPort["report_section_component"] = "Report Section / Component";
$iPort["selected_subject"] = "Selected Subject(s)";
$iPort["choose_subject"] = "Choose Subject(s)";
$iPort["submitted_list"] = "Submitted List";
$iPort["students_submitted"] = "Students submitted";
$iPort["of"] = "of";
$iPort['student_name'] = "Student Name";
$iPort['class'] = "Class";
$iPort['class_no'] = "Class No.";
$iPort['submitted_date'] = "Submitted Date";
$iPort["upload"] = "Upload";
$iPort['OLE_Upload_File'] = "Upload File";
$iPort["upload_file"] = "Upload File";
$iPort["activity_title_template"] = "Preset Programme Name";
$iPort["default_ele"] = "Default OLE Component";
$iPort["activity_role_template"] = "Preset Participation Role";
$iPort["activity_awards_template"] = "Preset Awards / Certifications / Achievements";
$iPort["category"] = "Programme Category";
$iPort["tag"] = "Programme Additional Category";
$iPort["print_result"] = "Print Result";
$iPort["export_result"] = "Export Result";
$iPort["upload_date"] = "Upload Date";
$iPort["file_list"] = "File List";
$iPort["upload_files"] = "Upload Files";
$iPort["related_to"] = "Related to";
$iPort["allow_to_submit"] = "Allow to submit";
$iPort["internal_record"] = "Other Learning Experiences";
$iPort["internal_record_submision_period"] = "Other Learning Experiences (Submission Period)";
$iPort["external_record"] = "Performance / Awards and Key Participation Outside School";
$iPort["external_recordSetPerios"] = "Performance / Awards and Key Participation Outside School (Submission Period)";
$iPort["OLESetSLPRecord"] = "OLE record selection period for SLP report display (for both '".$iPort["internal_record"]."' and '".$iPort["external_record"]."') ";
$iPort['submission_type'] = "Submission Type";
$iPort["select_default_sa"] = "Select default Self Account";
$iPort["select_default_sa_teacher"] = "Set as default Self Account";
$iPort["selected_as_defaultSelfAccount"] = "<font color=\"blue\">Default Self Account</font>";
$iPort['to'] = "to";
$iPort['period_start_end'] = "The submission period is from";
$iPort['period_start'] = "The submission period is started from";
$iPort['period_end'] = "The submission period is end of";
$iPort['SLPperiod_start_end'] = "The setting of period of SLP report is from";
$iPort['SLPperiod_start'] = "The setting of period of SLP report is started from";
$iPort['SLPperiod_end'] = "The setting of period of SLP report is end of";
$iPort['no_of_program'] = "No. of Programs";
$iPort['sub_total'] = "Sub Total";
$iPort["last_submission"] = "Last Submission";
$iPort["lp_template"] = "Template";
$iPort["lp_skin"] = "Site Skin";
$iPort["report_section"] = "Report Section";
$iPort["component"] = "Component";
$iPort["share_to"] = "Share to";
$iPort["stat_by"] = "<!--base-->";
$iPort['attachments'] = "Attachment(s)";
$iPort['loading'] = "Loading";
$iPort['MergeProgramme'] = "Merge Programme";
$iPort['ApprovalNeeded'] = "Approval Needed";
$iPort['NoApprovalNeeded'] = "No Approval Needed";
$iPort['RecordApproval_ClassTeacher'] = "Class teachers have approval right";
$iPort["Item"] = "Item";
$iPort["Award"] = "Award";
$iPort["Post"] = "Post";
$iPort["Export"] = "Export";
$iPort["Selected"] = "Selected";
$iPort["All"] = "All";
$iPort["Type"] = "Type";
$iPort["ReportPeriod"] = "Report Period";
$iPort["CurrentStudent"] = "Current Student";
$iPort["Class"] = "Class";
$iPort["Subject"] = "Subject";
$iPort["SubjectGroup"] = "Subject Group";
$iPort["compareScore"] = "Compare score between";
$iPort["zScoreImprovementPercent"] = "Standard Score Improved Percentage";
$iPort["scoreImprovementPercent"] = "Score Improved Percentage";
$iPort["FormClassSubjectGroup"] = "Form / Class / Subject Group";
$iPort["Teacher"] = "Teacher";
$iPort["Personal_character_record_submision_period"] = "Class Teacher Submit Period";

$iPort['filter']['LAA']='LAA';
$iPort['filter']['SLD']='SLD';
$iPort['filter']['ID']='ID';
$iPort['filter']['VI']='VI';
$iPort['filter']['HI']='HI';
$iPort['filter']['PD']='PD';
$iPort['filter']['ASD']='ASD';
$iPort['filter']['ADHD']='ADHD';
$iPort['filter']['SLI']='SLI';
$iPort['filter']['GF']='GF';

$iPort['Gender'] = "Gender";
$iPort['GenderM'] = "Male";
$iPort['GenderF'] = "Female";
$iPort['NonChinese'] = "Non Chinese Student";
$iPort['ComeFromChina'] = "Come From China";
$iPort['CrossBoundary'] = "Cross Boundary";
$iPort['NotLimit'] = "Not limit";
$iPort['AdvancedSearch'] = 'Advanced Search';

$iPort["ClassComparison"] = "Class Comparison";
$iPort["SubjectComparison"] = "Subject Comparison";
$iPort["AvgerageMarks"] = "Avgerage Marks";

$iPort["AvgerageMarkOf"] = "Avgerage Mark of <!-- Name -->";
$iPort["MEAN"] = "Avgerage Marks";
$iPort["SD"] = "S.D.";
$iPort["HighestMark"] = "Highest Marks";
$iPort["LowestMark"] = "Lowest Marks";

// menu
$iPort['menu']['my_information'] = "My Information";
$iPort['menu']['school_records'] = "School Record";
$iPort['menu']['personal_attributes'] = "Personal Attributes";
$iPort['menu']['academic_points'] = "Outstanding Academic Performance";
$iPort['menu']['olr'] = "OLR";
$iPort['menu']['ole'] = "Other Learning Experiences";   // change from "Student Learning Profile" 20110418
$iPort['menu']['slp'] = "Student Learning Profile";
$iPort['menu']['self_account'] = "Self-Account";
$iPort['menu']['teacher_comment'] = "Teacher Comment";
$iPort['menu']['audit_student_report'] = "Student Audit Report";
$iPort['menu']['audit_student'] = "Student Audit";
$iPort['menu']['learning_portfolio'] = "Learning Portfolio";
$iPort['menu']['school_based_scheme'] = (!function_exists("iportfolio_getSBSTitle") || !isset($ck_course_id) || iportfolio_getSBSTitle($ck_course_id) == "") ? "School Based Scheme" : iportfolio_getSBSTitle($ck_course_id);
$iPort['menu']['peer_learning_portfolio'] = "Peer Learning Portfolio";
$iPort['menu']['student_account'] = "Student Account";
$iPort['menu']['student_report_printing'] = "Student Report";
$iPort['menu']['dynReport'] = "Dynamic Report";
$iPort['menu']['alumniReport'] = "Alumni Report";
$iPort['menu']['olr_report'] = "OLR Report";
$iPort['menu']['ole_report'] = "OLE Report";
$iPort['menu']['assessment_statistic_report'] = "Assessment Statistic Report";
$iPort['menu']['prepare_cd_rom_burning'] = "Disk Preparation";
$iPort['menu']['download_cd_rom_burning'] = "Download Disk";
$iPort['menu']['reports'] = "Reports";
$iPort['menu']['management'] = "Management";
$iPort['menu']['data_handling'] = "Data Handling";
$iPort['menu']['settings'] = "Settings";
$iPort['menu']['addon_module'] = "Add-on Module";
$iPort['menu']['update_info_eclass_websams'] = "Update info (eClass/WEBSAMS)";
$iPort['menu']['data_export'] = "Data Export";
$iPort['menu']['eReportCard'] = "Report Card";
$iPort['menu']['Import']['Programme'] = "Programme";
$iPort['menu']['Import']['ProgrammeWithStudent'] = "Programme with student record";
$iPort['menu']['assessment_report'] = "Assessment Report";
$iPort['menu']['third_party'] = "Third-party report";
// table display
$iPort['table']['records'] = "Records";

// button
$iPort['btn']['search'] = "Search";
$iPort["btn"]["submit"] = "Submit";
$iPort["btn"]["cancel"] = "Cancel";
$iPort["btn"]["edit"] = "Edit";
$iPort["btn"]["delete"] = "Delete";
$iPort["btn"]["activate"] = "Activate";
$iPort["btn"]["deactivate"] = "Deactivate";
$iPort["btn"]["copy"] = "Copy";
$iPort["btn"]["reset"] = "Reset";
$iPort["btn"]["save"] = "Save";
$iPort["btn"]["create_phase_now"] = "Create Phase Now";
$iPort["btn"]["back_to_scheme_list"] = "Back to Scheme List";
$iPort["btn"]["submit_and_create_phase"] = "Submit &amp; Create New Phase";
$iPort["btn"]["submit_and_finish"] = "Submit &amp; Finish";
$iPort["btn"]["print"] = "Print";
$iPort["btn"]["redo"] = "Redo";
$iPort["btn"]["import_student_info"] = "Import Student Info";
$iPort["btn"]["import_another"] = "Import Another File";
$iPort["btn"]["ole_finish"] = "Finish";

// alert message
$iPort["msg"]["group_no_selected"] = "If no group is selected, the scheme will be released to ALL students.";
$iPort["msg"]["fields_with_asterisk_are_required"] = $i_general_required_field;
$iPort["msg"]["record_add"] = "Record Added";
$iPort["msg"]["record_update"] = "Record Updated";
$iPort["msg"]["record_delete"] = "Record Deleted";
$iPort["msg"]["confirm_delete"] = "Are you sure to delete?";
$iPort["msg"]["confirm_activate"] = "Are you sure to activate?";
$iPort["msg"]["confirm_deactivate"] = "Are you sure to deactivate?";
$iPort["msg"]["confirm_make_change"] = "The following changes will be made:";
$iPort["msg"]["start_time"] = "If start-time is set, students can only submit their OLE records after this time.";
$iPort["msg"]["end_time"] = "If end-time is set, students can only submit their OLE records before this time.";
$iPort["msg"]["SetSLPRecord_start_time"] = "If start-time is set, students can only set record for SLP report after this time.";
$iPort["msg"]["SetSLPRecord_end_time"] = "If end-time is set, students can only set record for SLP report before this time.";
$iPort["msg"]["start_time_self_account"] = "If start-time is set, students can only submit their self accounts after this time.";
$iPort["msg"]["end_time_self_account"] = "If end-time is set, students can only submit their self accounts before this time.";
$iPort["msg"]["start_time_teacher_comment"] = "If start-time is set, teachers can only submit their comments after this time.";
$iPort["msg"]["end_time_teacher_comment"] = "If end-time is set, teachers can only submit their comments before this time.";
$iPort["msg"]["warning_eng_title"] = "Please enter the Title(Eng)";
$iPort["msg"]["warning_chi_title"] = "Please enter the Title(Chi)";
$iPort["msg"]["select_submit_type"] = "Please select at least one submit type";
$iPort["msg"]["batchEditWarning"] = "You can only edit items in the same title.";
$iPort["msg"]["selectOneItem"] = "Please select at least one option.";
$iPort["msg"]["InputOneItem"] = "Please input at least one option.";

$iPort["alert"]["this_phase_is_filled_by"] = "This phase is filled by";
$iPort["alert"]["filled_in"] = ". ";
$iPort["alert"]["no_right_to_do"] = " You do not have right to fill in or view the answer(s) during processing.";
$iPort["alert"]["only_preview"] = " Since this phase is processing, you can preview it only.";


$iPort["report_col"]["total_student"] = "Students";
$iPort["report_col"]["total_attempt"] = "Attempts";
$iPort["report_col"]["total_pass"] = "Pass";
$iPort["report_col"]["passing_rate"] = "Passing Rate";
$iPort["report_col"]["full_mark"] = "Full Mark";
$iPort["report_col"]["average"] = "Avg";
$iPort["report_col"]["highest_mark"] = "Highest";
$iPort["report_col"]["lowest_mark"] = "Lowest";
$iPort["report_col"]["SD"] = "S.D.";
$iPort["report_col"]["term"] = "Term";

$iPort["report_col"]["form_class_group"] = "Form/Class/Group";
$iPort["report_col"]["type"] = "Type";
$iPort["report_col"]["type_form"] = "Form";
$iPort["report_col"]["type_class"] = "Class";
$iPort["report_col"]["type_group"] = "Split Group";
$iPort["report_col"]["subject"] = "Subject";
$iPort["report_col"]["teacher"] = "Teacher";
$iPort["report_col"]["compare"] = "Compare";
$iPort["report_col"]["attempted_both"] = "Total Attempts for Both";
$iPort["report_col"]["improved_by_ss"] = "No of Student with Improvements on Standard Score";
$iPort["report_col"]["improved_by_percentage"] = "Percentage";


// User Type
$iPort['usertype_t'] = "Teacher";
$iPort['usertype_s'] = "Student";
$iPort['usertype_a'] = "Assistant";
$iPort['usertype_g'] = "Guest";
$iPort['usertype_p'] = "Parent";
$iPort['usertype_ct'] = "Class Teacher";
$iPort['usertype_st'] = "Subject Teacher";
$iPort['usertype_admin'] = "Administrator";

$iPort["ole_report"]["programmes_with_description"] = "Programmes (with description)";
$iPort["ole_report"]["selected_records"] = " - Selected Records";
$iPort["ole_report"]["school_year"] = "School Year";
$iPort["ole_report"]["role_of_participation"] = "Role of Participation";
$iPort["ole_report"]["partner_organizations_if_any"] = "Partner Organizations (if any)";
$iPort["ole_report"]["essential_learning_experiences"] = "Components of OLE";
$iPort["ole_report"]["achievements_if_any"] = "Awards / Certifications / Achievements* (if any)";
$iPort["ole_report"]["other_learning_experiences"] = "Other Learning Experiences";
$iPort["ole_report"]["table_description"] = "Apart from core and elective subjects, the learning programmes that the student participated during the senior secondary education. It includes Moral and Civic Education, Aesthetic Development, Physical Development, Community Service and Career-related Experiences. 'Other Learning Experiences' could be gained through programmes organized by the school or co-organized by the school with outside organizations.";
$iPort["ole_report"]["required_remark"] = "Evidence of awards/ certifications/ achievements listed is available for submission when required";

$iPort["external_ole_report"]["performance"] = "Performance / Awards and Key Participation Outside School";
$iPort["external_ole_report"]["table_description"] = "For learning programmes not organized by the school during the senior secondary education, student could provide the information to the school. It is not necessary for the school to validate information below. Student should hold full responsibility to provide evidence to relevant people when requested.";
$iPort["external_ole_report"]["organization"] = "Organization";

///////////////////////// Copy From KIS ///////////////////////////////////////
#Assessment Report
$iPort['Assessment'] = array();
$iPort['Assessment']['NewAssessment'] = "New Assessment";
$iPort['Assessment']['Assessment'] = "Assessment";
$iPort['Assessment']['EditAssessment'] = "Edit Assessment";
$iPort['Assessment']['Title'] = "Title";
$iPort['Assessment']['ReleaseDate'] = "Release Date";
$iPort['Assessment']['AllClasses'] = "All Classes";
$iPort['Assessment']['Target'] = "Target";
$iPort['Assessment']['ReleaseStatus'][1] = "Released";
$iPort['Assessment']['ReleaseStatus'][2] = "Not Released";
$iPort['Assessment']['UploadStatus'][1] = "Uploaded";
$iPort['Assessment']['UploadStatus'][2] = "Not Uploaded";
$iPort['Assessment']['Search'] = "Search....";
$iPort['Assessment']['NoOfUploads'] = "No.of Uploads / No. of Students";
$iPort['Assessment']['AssessmentList'] = "Assessment List";
$iPort['Assessment']['AssessmentFile'] = "Assessment File";
$iPort['Assessment']['AssessmentTitle'] = "Assessment Title";
$iPort['Assessment']['UploadedDate'] = "Uploaded Date";
$iPort['Assessment']['EditBy'][0] = "by ";
$iPort['Assessment']['EditBy'][1] = "";
$iPort['Assessment']['Upload'] = "Upload";
$iPort['Assessment']['LastUpdated'] = "Last Updated";
$iPort['Assessment']['Error']['ReleaseDate'] = "Please fill in release date";

///////////////////////// Below Copy from eclass 30 ///////////////////////////

$ec_form_word['fill_in_method'] = "Fill-in method";
$ec_form_word['growth_scheme_form'] = "Create forms";
$ec_form_word['confirm_to_template'] = "Confirm the changed template?";
$ec_form_word['answersheet_tf'] = "True and False";
$ec_form_word['answersheet_mc'] = "Multiple Choices";
$ec_form_word['answersheet_mo'] = "More than one option allowed";
$ec_form_word['answersheet_sq1'] = "Fill-in (Short)";
$ec_form_word['answersheet_sq2'] = "Fill-in (Long)";
$ec_form_word['answersheet_not_applicable'] = "Not applicable";
$ec_form_word['answersheet_template'] = "Templates";
$ec_form_word['move_up_item'] = "Move item up";
$ec_form_word['move_down_item'] = "Move item down";
$ec_form_word['delete_item'] = "Delete item";
$ec_form_word['rename_item'] = "Rename item/description";
$ec_form_word['change_total'] = "Change question total no.";
$ec_form_word['rename_item_partA'] = "Rename item/description, from '";
$ec_form_word['rename_item_partB'] = "'Change to be:";
$ec_form_word['change_total_partA'] = "Change question total number, from '";
$ec_form_word['change_total_partB'] = "'Change to be";
$ec_form_word['question_title']="Topic/Title";
$ec_form_word['no_options_for'] = "No need to select number of options!";
$ec_form_word['fill_in_type'] = "Please select a method!";
$ec_form_word['fill_in_content'] = "Please fill in content!";
$ec_form_word['change_heading'] = "change topic/title:";
$ec_form_word['no_need_input']="Not applicable";
$ec_form_word['total_submission'] = "Total handins";
$ec_form_word['answersheet_old'] = "Question Excluded";
$ec_form_word['answersheet_new'] = "Question Included";
$ec_form_word['uncheck_option'] = "Un-check All Options";
$ec_form_word['fill_by_teacher'] = "Completed by teachers";
$ec_form_word['answersheet_ls'] = "Likert Scale";
$ec_form_word['answersheet_tl'] = "Table-like Input Format";
$ec_form_word['answersheet_no_ques'] = "# questions";
$ec_form_word['answersheet_no_select_ques'] = "No need to select number of questions";
$ec_form_word['question_scale'] = "[Ques./Scal.]";
$ec_form_word['question_question'] = "[Ques./Ques.]";


// Student - Course Work Assignments
$automarking="Automark";
$answer_sheet="Answer Sheet";
$answersheet_header="Section Header";
$answersheet_no="Number of questions";
$answersheet_order="Order Type";
$answersheet_type="Question Type";
$answersheet_mark="Mark for each question";
$answer_doit="Do it now!";
$answer_modifyit="Modify your work!";
$answer_attached="Attached answer";
$answer_sum="Sum";
$answersheet_tf="T & F";
$answersheet_mc="Multiple Choices";
$answersheet_mo="Multiple Options";
$answersheet_sq="Short Answer";
$answersheet_lq="Long Answer";
$answersheet_likert="Likert Score";
$answersheet_option="# options";
$answersheet_maxno="Number of question exceeds the limit: 'a - z' for alphabet; 'i - x' for Roman index!";
$legend_desc1="swap with the item above";
$legend_desc2="swap with the item below";
$legend_desc3="delete";
$legend_desc4="change the section header";
$legend_desc5="change the number of question";
$last_submitted = "Last Submitted";
$last_modified = "Last Modified";
$assignment_remark = "Remark";
$assignment_done="Done";
$assignment_save="Save";
$assignment_status_msg="Graded already and you are not allowed to modify your work.";
$work_list_not_submit = "Student List of No Submission";
$work_all = "All work";
$assignments_subject = "Subject";
$assignments_to = "To";
$assignments_deadline = "Deadline";
$assignments_title = "Title";
$assignments_status = "Status";
$assignments_handin = "Submitted";
$assignments_handin_history = "Handin History";
$assignments_submission = "Submission";
$assignments_instruction = "Instruction";
$assignments_attachment = "Attachment";
$assignments_publish = "Public";
$assignments_pending = "Private";
$assignments_email = "Send e-mail to all users.";
$assignments_alert = "Email Alert";
$assignments_sbj_redo = "Assignment Redo";
$assignments_msg = "There is no assignment at the moment.";
$assignments_msg2 = "No files found.";
$assignments_msg3 = "There is no handin at the moment.";
$assignments_files = "file(s)";
$assignments_name = "Name";
$assignments_size = "Size";
$assignments_campusmail = "Send Campusmail to all users.";
$assignment_overall_stat = "Overall Statistics";
$assignment_public_overall_stat = "Public the overall statistics";
$assignment_not_public_msg = "Not Public Yet";
$assignment_public_mark = "Public Grade/Mark";
$assignment_public_exercise = "Public Exercise";
$assignment_submitted_exercise = "Submitted Exercise";
$assignment_public_guide1= "This statistic is not suitable for assignment which marked by grade";
$assignment_view_result = "View Result";
$assignment_stat_sd = "Standard Deviation";
$assignment_stat_mean = "Average Score";
$assignment_stat_lowest = "Lowest Score";
$assignment_stat_highest = "Highest Score";
$assignments_lastmodi = "Last Modified";
$assignments_datesubmit = "Date Submitted";
$assignments_student = "Student";
$assignments_file = "Assignment";
$assignments_answer = "Teacher Answer";
$assignments_grade = "Grade/Mark";
$assignments_late = "Late Submission";
$assignments_msg4 = "Fill in the grade to mark the assignment. If you want to attach the answer, click BROWSE to upload the file.";
$assignments_msg5 = "Students in the recipient box below have not submitted the assignment. You may send an email alert to remind them.";
$assignments_msg6 = "The answersheet has been updated. Close this window and continue editng the assignment.";
$assignments_msg7 = array("create and monitor student assignment", "create answer sheets for your uploaded files and have them auto-marked");
$assignments_modelanswer = "Model Answer";
$assignments_permissions = "Permissions";
$assignments_comment = "Comments";
$assignments_alert_msg1 = "Please fill in the subject.";
$assignments_alert_msg2 = "Please fill in the message.";
$assignments_alert_msg3 = "Send e-mail?";
$assignments_alert_msg4 = "Please fill in the grade.";
$assignments_alert_msg5 = "preview is for HTML documents only.";
$assignments_alert_msg6 = "HTML Preview";
$assignments_alert_msg7 = "Please fill in the title.";
$assignments_alert_msg8 = "Please fill in the deadline.";
$assignments_alert_msg9 = "Invalid Date.";
$assignments_alert_msg10 = "Please fill in the instruction.";
$assignments_alert_msg11 = "Click here to save the handed-in asssignment!";
$assignments_alert_msg12 = "Record Time Limit must be between  1 and";
$assignments_alert_msg13 = "Storage Quota must be positive value";
$Assignments_submitted = "The answer that you have submitted";
$assignments_message = "Message";
$gradedAnswer_msg1='Do it now!';
$gradedAnswer_msg2='Check the graded paper!';
$grade_not = "Unmark";

// project info
$project_info_basic = "Basic Info";
$project_info_group = "Group Info";
$project_info_phase = "Phase Info";
$project_phase_no = "Total Number of Project Phases";
$project_phase_name = "Phase Name";
$project_interim_date = "Interim Date";
$project_report = "Report";
$project_media = "Medium";
$project_ratio = "Percentage";
$project_steps = "STEPS";
$project_none = "Not Applicable";
$project_file = "File Upload";
$project_general = "General Info";
$project_progress = "Progress";
$project_reject = "Reject";
$project_my = "My";
$project_our = "Our";
$project_by = "by";
$project_submit_by = "Submitted By";
$project_finished = "Finished";
$project_std_msg1 = "This page shows the general information of your project. Click <b>Our Progress</b> for viewing the details and submitting reports if applicable.";
$project_std_msg2 = "You should do this project by folllowing the phases one by one. Make sure you have submitted your report (if required) for each phase before the interim date and tick the checkbox in \"<b>Finished</b>\" column to report your progress.";

// warning
$ec_warning['reset_template'] = "Are you sure that you don't want to keep the current contents but revert to the last version?";
$ec_warning['publish_content'] = "Are you sure that you want to publish the selected contents for your iPortfolio?";
$ec_warning['hide_content'] = "Are you sure that you want to remove your learning portfolio?";
$ec_warning['comment_content'] = "Please enter your comments.";
$ec_warning['comment_author'] = "Please enter your name.";
$ec_warning['comment_submit_confirm'] = "Are you sure that you want to submit your comments?";
$ec_warning['notes_title'] = "Please enter the title.";
$ec_warning['release_template'] = "Are you sure that you want to publish the selected document format for students to create their iPortfolio?\\n�eAttention�fOnce students start using the published document format, it cannot be cancelled!";
$ec_warning['growth_title'] = "Please enter the scheme title.";
$ec_warning['growth_phase_title'] = "Please enter the phase title.";
$ec_warning['form_handin'] = "Are you sure that you want to submit the current info?";
$ec_warning['growth_phase_viewed'] = "Are you sure that you have read the contents above?";
$ec_warning['form_no_fill_in'] = "Review in progress, you cannot write in here!";
$ec_warning['no_permission'] = "Sorry, you don't have the access right.";
$ec_warning['no_view_now'] = "Sorry, not yet in review phase.";
$ec_warning['weblog_contents'] = "Please enter contents.";
$ec_warning['weblog_title'] = "Please enter title.";
$ec_warning['weblog_entry_remove'] = "Are you sure that you want to delete this record?";
$ec_warning['portfolio_activate'] = "Are you sure that you want to activate the account(s) above?";
$ec_warning['award_title'] = "Please enter the award title.";
$ec_warning['date'] = "Please enter the date.";
$ec_warning['title'] = "Please enter the title.";
$ec_warning['chiTitle'] = "Please enter the Chinese title.";
$ec_warning['engTitle'] = "Please enter title.";
$ec_warning['eclass_data_syn'] = "Are you sure that you want to update inform from eClass?";
$ec_warning['eclass_data_no_record'] = "Sorry, no record for update!";
$ec_warning['order_repeat'] = "The order can not be repeat. Please select again.";
$ec_warning['order_required'] = "The order of each displayed item should not be null!";
$ec_warning['copy_portfolio'] = "Are you sure you want to copy the Learning Portfolio files of class XXX";
$ec_warning['copy_portfolio_SP'] = "Are you sure you want to copy the Learning Portfolio files of XXX";
$ec_warning['remove_copied_portfolio'] = "Are you sure you want to remove the copied Learning Portfolio files of class XXX?";
$ec_warning['coyp_template'] = "Are you sure you want to copy template(s)?";
$ec_warning['select_student'] = "Please select student(s).";
$ec_warning['select_class'] = "Please select class(es).";
$ec_warning['olr_from_teacher'] = "This record is submmited by teacher. You do not need to approve it!";
$ec_warning['info_remove'] = "Are you sure that you want to delete this information?";
$ec_warning['overwrite_file'] = "The newly uploaded file will overwrite the old file.";
$ec_warning['remove_default'] = "Default records cannot be deleted!";
$ec_warning['edit_default'] = "Default records cannot be edited!";
$ec_warning['over_limit'] = "Number of records selected exceeds the limit.";
$ec_warning['category'] = "Please choose a category";
$ec_warning['convert_year_instruction'] = "eClass IP 2.5 uses YYYY-YYYY as the standard school year format. You can convert school year information of legacy records into this new format.<br /><br /><font color=\"#FF0000\">Please make sure you have selected the right school year (Bad Example: Matching \"08-09\" records to the \"2009-2010\" school year). This conversion is NOT reversible.</font>";
$ec_warning['reportPrinting_instruction'] = "You can print records from previous school years. However, only the <b>CURRENT</b> class name and class number can be displayed on the report. To print reports for previous school years, please select a past school year with the <b>CURRENT</b> class name and class number of the students.";

$ec_warning['reportPrinting_instruction_for_student'] = "You can print records from previous school years. However, only the <b>CURRENT</b> class name and class number can be displayed on the report. To print reports for previous school years, please select a past school year.";
$ec_warning['reportPrinting_instruction_for_student_ywgs'] = "You can preview your OLE report here. It's suggested to use IE's print-preivew function to check the page-breaks. If necessary, please add page-break to your self-account manually.";

$ec_warning['OeaReportPrinting_instruction_for_student'] = "You can print / export for those approved OEA Record(s).";
$ec_warning['reportExport_instruction'] = "You can export records from previous school years. However, only the <b>CURRENT</b> class and class number can be displayed on the record.";
$ec_warning['oleMgmt_instruction'] = "Below are records submitted for your approval. Check appropriate records, then click \"Approve\" or \"Reject\". You can also edit, merge, and delete records not marked with \"#\". Records with \"#\" marks were created by other teachers. You can only approve them.";
$ec_warning['lpForcePublish_instruction'] = "<strong>You can select appropriate records, and choose one of the following publish method:</strong><ul><li>Previously published content - For situation where students have made changes to previously published contents yet forgotten to re-publish them.</li><li>All content - For situation where students have forgotten to publish completed contents or have failed to complete their work on time that you need to force-publish their work for assessment.</li><ul>";

$ec_warning['DefaultSA_by_ModifiedDate'] = "Since the selected Self Account was deleted, the Self Account with latest modified date is automatically selected.";

$ec_iPortfolio['pending'] = "Pending for Approval";
$ec_iPortfolio['please_fill_start_end_date'] = "You must fill in the start time and the end time";
$ec_iPortfolio['erase_info'] = "Erase Information";
$ec_iPortfolio['remove_info'] = "Delete Information";
$ec_iPortfolio['remove_merit_info'] = "Delete Merit Record";
$ec_iPortfolio['remove_activity_info'] = "Delete Activity Record";
$ec_iPortfolio['remove_assessment_info'] = "Delete Assessment Record";
$ec_iPortfolio['remove_award_info'] = "Delete Award Record";
$ec_iPortfolio['remove_conduct_info'] = "Delete Teacher Comment Record";
$ec_iPortfolio['remove_attendance_info'] = "Delete Attendance Record";
$ec_iPortfolio['remove_more_merit'] = "Delete more merit records";
$ec_iPortfolio['remove_more_activity'] = "Delete more activity records";
$ec_iPortfolio['remove_more_award'] = "Delete more award records";
$ec_iPortfolio['remove_more_attendance'] = "Delete more attendance records";
$ec_iPortfolio['remove_more_conduct'] = "Delete more teacher comment records";
$ec_iPortfolio['remove_more_assessment'] = "Delete more assessment records";
$ec_iPortfolio['number_of_record'] = "Number of record";
$ec_iPortfolio['number_of_removed_record'] = "No. of items successfully deleted";
$ec_iPortfolio['iportfolio_folder_size'] = "Storage Quota";
$ec_iPortfolio['size_unit'] = '(MB)';
$ec_iPortfolio['no_this_record'] = "This student has not yet published his/her portfolio.";
$ec_iPortfolio['lastModifiedDate'] = "Last Modified Date";
$ec_iPortfolio['setThisDefaultSA'] = "Do you select this Self-Account?";
$ec_iPortfolio['Selected'] = "selected";
$ec_iPortfolio['Export_Format'] = "Export Format";
$ec_iPortfolio['Export_Custom_Field'] = "Custom Fields";
$ec_iPortfolio['Export_WebSAMS_Format'] = "WebSAMS Format";
$ec_iPortfolio['Export_Custom_Format'] = "Custom Format";

$ec_iPortfolio['NoOfFirstPage'] = "The Number of First Page";
$ec_iPortfolio['NoOfOtherPage'] = "The Number of Other Page";
$ec_iPortfolio['NoOfRecordInt'] = "The Number of OLE Records Per Page";
$ec_iPortfolio['NoOfRecordExt'] = "The Number of Performance / Awards and Key Participation Outside School Record Per Page";

$ec_iPortfolio['PassMark'] = "Pass Mark";
$ec_iPortfolio['ole_record'] = "Other Learning Experience Record";
$ec_iPortfolio['reading_records'] = "Reading Records";
$ec_iPortfolio['title_reading_records'] = "Reading Records";

// Student Report Printing
$Lang['StdRepPrint']['PrintingLanguage'] = "Printing Language";
$Lang['StdRepPrint']['English'] = "English";
$Lang['StdRepPrint']['Chinese'] = "Chinese";
$Lang['StdRepPrint']['Bilingual'] = "Bilingual";

// alert
$w_alert['Invalid_Date'] = "Invalid Date";
$w_alert['Invalid_Time'] = "Invalid Time";
$w_alert['Invalid_Lang'] = "Please choose language";
$w_alert['fill_in_info'] = "Please fill in corresponding information.";
$w_alert['start_end_time'] = "Start time must not be greater than end time.";
$w_alert['start_end_time2'] = "Start time must not be greater than end time.";
$w_alert['deadline_interim'] = "Interim Date must not be greater than deadline.";
$w_alert['choose_option'] = "Please choose an option.";
$w_alert['sure_to_cancel'] = "Are you sure you want to cancel?";
$w_alert['sure_to_reset'] = "Are you sure you want to reset?";
$w_alert['exam_release_ed'] = "Please fill in the deadline if you want to set marked papers to be released after deadline.";
$w_alert['logout_confirm'] = "Are you sure you want to logout?";
$w_alert['exit_confirm'] = "Are you sure you want to exit?";

$ec_iPortfolio['record_approval_setting'] = "Record Approval Setting";
$ec_iPortfolio['record_approval_setting_auto'] = "No approval needed";
$ec_iPortfolio['record_approval_setting_selective'] = "Allow student to select approval teacher";
$ec_iPortfolio['record_approval_setting_default'] = "Only class teachers and administrators have approval right";
$ec_iPortfolio['record_approval_setting_editable'] = "Allow students to edit";

$ec_iPortfolio['student_info'] = "Student Info";
$ec_iPortfolio['academic_result'] = "Academic Results";
$ec_iPortfolio['admission_date'] = "Admission Date";
$ec_iPortfolio['merit'] = "Merit/Demerit Record";
$ec_iPortfolio['full_report'] = "Full Report";
$ec_iPortfolio['full_reportTitle2'] = "Student Full Report Result";
$ec_iPortfolio['assessment_report'] = "Academic Results";
$ec_iPortfolio['transcript'] = "Transcript";
$ec_iPortfolio['display_stat_report'] = "Display Statistical Report";
$ec_iPortfolio['activity'] = "Activity Record";
$ec_iPortfolio['award'] = "Award Record";
$ec_iPortfolio['award_file'] = "Award File";
$ec_iPortfolio['teacher_comment'] = "Teacher Comment";
$ec_iPortfolio['attendance'] = "Attendance Record";
$ec_iPortfolio['print_report'] = "Print Report";
$ec_iPortfolio['print_full_report'] = "Print Full Report";
$ec_iPortfolio['print_transcript'] = "Print Transcript";
$ec_iPortfolio['overall_file'] = "General Portfolio";
$ec_iPortfolio['guardian_info'] = "Guardian Info";
$ec_iPortfolio['brother_sister_info'] = "Brothers and sisters Info";
$ec_iPortfolio['main_guardian'] = "Main Guardian";
$ec_iPortfolio['school_chop'] = "School Chop";

$ec_iPortfolio['admin_accessRight'] = "Principal/PIC (with full rights)";
$ec_iPortfolio['subjectPanel_accessRight'] = "Subject Panel";
$ec_iPortfolio['monthlyReport_accessRight'] = "Monthly Report PIC";

$ec_iPortfolio['management_subject'] = "Subject Setting";
$ec_iPortfolio['management_student'] = "Student Management";
$ec_iPortfolio['data_import_update'] = "Update import";
$ec_iPortfolio['data_import'] = "Update info <font size=-1> (eClass/WEBSAMS)</font>";
$ec_iPortfolio['SAMS_import_regno'] = "RegNo. Import";
$ec_iPortfolio['SAMS_import_form'] = "CSV format";
$ec_iPortfolio['import_activation'] = "Activate iPortfolio account";
$ec_iPortfolio['account_quota_free'] = "Available user account remaining";
$ec_iPortfolio['activation_student_total'] = "Current no. of selected students";
$ec_iPortfolio['activation_quota_afterward'] = "User account remaining after activation";
$ec_iPortfolio['activation_yes'] = "Activated";
$ec_iPortfolio['activation_total_success'] = "No. of student accounts successfully activated";
$ec_iPortfolio['activation_total_fail'] = "No. of student accounts failed to be activated";
$ec_iPortfolio['activation_result_no_regno'] = "RegNo. missing";
$ec_iPortfolio['activation_result_activated'] = "This user account has been activated before this.";
$ec_iPortfolio['activation_quota_confirm'] = "Please confirm the number of account to be activated now. Remember, once the account has been activated, it can't be reverted.";
$ec_iPortfolio['portfolio_account'] = "iPortfolio account";
$ec_iPortfolio['SAMS_import'] = "Import WebSAMS CSV";
$ec_iPortfolio['SAMS_import_subject'] = "Subject import";
$ec_iPortfolio['SAMS_import_anp'] = "Merit/Demerit Record import";
$ec_iPortfolio['SAMS_import_assessment'] = "WebSAMS Academic Record import";
$ec_iPortfolio['SAMS_import_activity'] = "Activity Record import";
$ec_iPortfolio['SAMS_import_comment'] = "WebSAMS Teacher Comment import";
$ec_iPortfolio['SAMS_import_student_data'] = "WebSAMS Student Info import";
$ec_iPortfolio['SAMS_import_guardian_data'] = "WebSAMS Guardian Info import";
$ec_iPortfolio['generate_csv_template'] = "Generate CSV file template";
$ec_iPortfolio['please_select_subject'] = "Please select subject(s).";
$ec_iPortfolio['show_remark'] = "Show Remarks";

$ec_iPortfolio['student_result_list'] = "Student Result List";
$ec_iPortfolio['class_highest_score_student'] = "Highest Score Student(s) in Class";
$ec_iPortfolio['class_lowest_score_student'] = "Lowest Score Student(s) in Class";
$ec_iPortfolio['level_highest_score_student'] = "Highest Score Student(s) in Class Level";
$ec_iPortfolio['level_lowest_score_student'] = "Lowest Score Student(s) in Class Level";
$ec_iPortfolio['by_year'] = "By school year";
$ec_iPortfolio['by_semester'] = "By semester";
$ec_iPortfolio['show_by'] = "Show by";
$ec_iPortfolio['SAMS_CSV_file'] = "WEBSAMS CSV file";
$ec_iPortfolio['SAMS_import_intro'] = "You may import info. using CSV files in WebSAMS format <br /> or press Update to synchronize data with eClass Student Profile.";

$ec_iPortfolio['SAMS_year_remind'] = "Please fill in the same way as in eClass. (The format should either be YYYY-yyyy or YYYY. E.g. \"2006-007\" or \"2006\".)";
$ec_iPortfolio['SAMS_regno_import_remind'] = "Attention: To ensure the number is complete, # has to be added to RegNo (e.g. \"#0025136\").";
$ec_iPortfolio['guardian_import_remind'] = " Also, the new imported guardian records will overwrite the existing records.";
$ec_iPortfolio['template_deletion_remind'] = "Attention: Template which is being used cannot be deleted.";
$ec_iPortfolio['student_photo_upload_remind'] = "Attention: The photos should be named with case-sensitive Student RegNo and zipped before uploading! Photo must be in '.JPG' format and the Size should be fixed to 100 x 130 pixel (W x H).";
$ec_iPortfolio['student_photo_upload_warning'] = "Failed to upload photo! The photo(s) must be zipped before uploading.";
$ec_iPortfolio['no_searched_result'] = "Your search yielded no results.";
$ec_iPortfolio['search_by_condition'] = "Search by conditions below";
$ec_iPortfolio['admission_year'] = "Admission Year";
$ec_iPortfolio['ole_comment'] = "Teacher's Comment";
$ec_iPortfolio['ole_comment_teacher'] = "Name of Teacher Advisor:";
$ec_iPortfolio['ole_advice'] = "Advisor's Comment";
$ec_iPortfolio['ole_override_comment'] = "This comment is owned by <!--TeacherName-->. If you update this comment, the ownership will transfer to you.";
$ec_iPortfolio['ole_include'] = "Including OLE";
$ec_iPortfolio['ole_set_pool_record'] = "Set records for SLP Report";
$ec_iPortfolio['ole_pkms_set_pool_record'] = "Set records for Pui Kiu SLP Report";
$ec_iPortfolio['ole_pkms_rec_start_from'] = "The setting of period of Pui Kiu SLP report is from";
$ec_iPortfolio['ole_no_rec_add_to_pool'] = "You can add <!--NoRec--> records";

$ec_iPortfolio['scoreHighlightRemarks'] = "Failed scores will be shown in RED";
$ec_iPortfolio['OMFHighlightRemarks'] = "Decreasing OMF will be shown in RED";
$ec_iPortfolio['OMFHighlightRemarks_Improve'] = "Increasing OMF will be shown in GREEN";
$ec_iPortfolio['chart_type'] = "Chart";
$ec_iPortfolio['barchart'] = "bar chart";
$ec_iPortfolio['linechart'] = "line chart";

$ec_guide['import_update_no'] = "No. of items successfully updated";
$ec_guide['import_add_no'] = "No. of items successfully added";
$ec_guide['import_fail_no'] = "No. of items failed to be added";
$ec_guide['import_error_row'] = "Row number";
$ec_guide['import_error_reason'] = "Reason for the Error";
$ec_guide['import_error_data'] = "Error Record";
$ec_guide['import_error_detail'] = "File info";
$ec_guide['import_error_duplicate_regno'] = "WebSAMS RegNo already existing";
$ec_guide['import_error_activated_regno'] = "This student account has already been activated, the WebSAMS RegNo cannot be modified.";
$ec_guide['import_error_incorrect_regno'] = "# has to be added to RegNo";
$ec_guide['import_error_incorrect_class'] = "This Class cannot be found in the system";
$ec_guide['import_error_unknown'] = "Unknown";
$ec_guide['import_error_no_user'] = "No such user";
$ec_guide['import_error_wrong_format'] = "Wrong import format";
$ec_guide['import_error_wrong_import_data'] = "Wrong file data";
$ec_guide['import_error_empty_import_data'] = "File data not found";
$ec_guide['import_error_duplicate_classnum'] = "The class name and class number of this student are duplicated.";
$ec_guide['import_error_date_format'] = "Date format should be with (yyyy-mm-dd) or not a valid date (eg 2009-11-31)";
$ec_guide['result'] = "Result";
$ec_guide['import_back'] = "Re-import";
$ec_guide['import_programid_not_found'] = "Program ID not found";
$ec_guide['import_dateformat_not_correct'] = "Date format should be (yyyy-mm-dd)";
$ec_guide['import_academic_year_not_found'] = "Academic Year not found";
$ec_guide['import_type_term_not_found'] = "Term  not found";
$ec_guide['import_type_year_term_not_found'] = "Academic Year / Term  not found";
$ec_guide['import_empty_title'] = "Empty title";
$ec_guide['import_header_content_column_not_match'] = "The content column do not match with the header column";
$ec_guide['import_ole_category_code_not_found'] = "The OLE Category Code not found";
$ec_guide['import_ole_category_not_found'] = "The OLE Category Code not found";
$ec_guide['import_ole_subcategory_not_found'] = "The OLE Subcategory Code not found";
$ec_guide['import_empty_category_title'] = "Empty OLE Category ";
$ec_guide['import_empty_program_title'] = "Empty Preset Programme";
$ec_guide['import_empty_ELE'] = "Empty Components of OLE";
$ec_guide['import_ELE_not_found'] = "Components of OLE not found";
$ec_guide['import_ole_insideoutside_not_found']="The OLE inside/outside school not found";
$ec_guide['import_ole_isSAS_not_found']="The OLE Belong to SAS not found";
$ec_guide['import_ole_ele_not_match']="At least one of the OLE Component does not match";
$ec_guide['import_ole_compulsory_field_invalid']="At least one of the Submission Compulsory Fields does not match";
$ec_guide['import_ole_user_should_not_exist'] = "Default Approver should be empty if 'Allow Auto Approval' is marked as 'Yes'";
$ec_guide['fail_reason'] = "Reason for the Failure";
$ec_guide['above_level_mean'] = "Above mean of the class level";
$ec_guide['below_level_mean'] = "Below mean of the class level";
$ec_guide['import_ole_can_join'] = "Allow students to join is not correctly inserted";
$ec_guide['import_ole_join_start_date_not_found'] = "The join period start date not found";
$ec_guide['import_ole_applicable_form_not_found'] = "The applicable form not found";
$ec_guide['import_ole_auto_approval_not_found'] = "The auto approval not found";
$ec_guide['import_ole_applicable_form_invalid'] = "Applicable form do not match with existing form";
$ec_guide['import_ole_max_hour_negative'] = "The maximum hour is found as negative";
$ec_guide['import_ole_default_hour_negative'] = "The default hour is found as negative";
$ec_guide['import_ole_complusory_fields_invalid'] = "Compulusory Fields do not match";
$ec_guide['import_ole_user_not_found'] = "The userLogin in default approver is not found";
$ec_guide['import_ole_user_not_teacher'] = "The userLogin in default approver is not a teacher / staff";
$ec_guide['import_ole_join_date_not_coexist'] = "Join Period start and end dates are not co-exist";
$ec_guide['import_ole_student_exist'] = "The student is already exist in this OLE";
$ec_guide['import_ole_record_not_exist'] = "Record not found";
$ec_guide['import_ole_program_title_already_exist'] = "Activity Name is already exist";

$ec_words['preset_no_record'] = "no record available";
$ec_words['photo_no'] = "No photo available";
$ec_iPortfolio['yearform'] = "Year Form";

$ec_iPortfolio['age'] = "Age";
$ec_iPortfolio['primarySchool'] = "Primary School";

  $ec_iPortfolio['existing_format'] = "Existing Format(s)";
  $ec_iPortfolio['convert_to'] = "To be Converted to";

	$ec_iPortfolio['eClass_update_assessment'] = "Update Academic Record from eClass";
	$ec_iPortfolio['eClass_update'] = "Synchronize with Student Profile";
	$ec_iPortfolio['eClass_update_merit'] = "Update Merit/Demerit Record from eClass";
	$ec_iPortfolio['eClass_update_activity'] = "Update Activity Record from eClass";
	$ec_iPortfolio['eClass_update_attendance'] = "Update Attendance Record from eClass";
	$ec_iPortfolio['eClass_update_award'] = "Update Award record from eClass";
	$ec_iPortfolio['eClass_update_comment'] = "Update Teacher Comment from eClass";
	$ec_iPortfolio['update_more_assessment'] = "Update academic record again";
	$ec_iPortfolio['update_more_award'] = "Update award record again";
	$ec_iPortfolio['update_more_activity'] = "Update activity record again";
	$ec_iPortfolio['update_more_attendance'] = "Update attendance record again";
	$ec_iPortfolio['update_more_merit'] = "Update merit/demerit record again";
	$ec_iPortfolio['update_more_comment'] = "Update teacher comment again";
	$ec_iPortfolio['last_update'] = "Date of last update";
	$ec_iPortfolio['SAMS_last_update'] = "Last Update";
	$ec_iPortfolio['SAMS_last_record_change'] = "Last Record Change";

	$ec_iPortfolio['Activated_Liense_Summary'] = "Activated License Summary";
	$ec_iPortfolio['Activated_Liense_Summary_current'] = "License used on Current Student:";
 	$ec_iPortfolio['Activated_Liense_Summary_without_class'] = "License used on Student without Class:";
 	$ec_iPortfolio['Activated_Liense_Summary_alumni'] = "License used on Alumni:";
 	$ec_iPortfolio['Activated_Liense_Summary_total_amount'] = "Total";
 	$ec_iPortfolio['Activated_Liense_Summary_inactive'] = "Inactive (Suspended or Left)";

	$ec_iPortfolio['Record_Approval_Note'] = "Note: This may not cause change in record if there are no new/modified record since the last synchronization.";
	$ec_iPortfolio['activation_no_quota'] = "Sorry, the account quota is insufficient!";
	$ec_iPortfolio['SAMS_code'] = "Code";
	$ec_iPortfolio['SAMS_cmp_code'] = "Component Code";
	$ec_iPortfolio['SAMS_description'] = "Description (Eng/Chi)";
	$ec_iPortfolio['SAMS_abbr_name'] = "Abbreviation (Eng/Chi)";
	$ec_iPortfolio['SAMS_short_name'] = "Short Form (Eng/Chi)";
	$ec_iPortfolio['SAMS_display_order'] = "Display order";
	$ec_iPortfolio['SAMS_subject'] = "Subject";
	$ec_iPortfolio['SAMS_short_name_duplicate'] = "Same short form is not allowed.";
	$ec_iPortfolio['WebSAMSRegNo'] = "WebSAMSRegNo";

	$ec_iPortfolio['admin_content_builder'] = "Create content";
	$ec_iPortfolio['template_selected'] = "Selected template:";
	$ec_iPortfolio['class_list'] = "Class Directory";
	$ec_iPortfolio['year_list'] = "Year Directory";
	$ec_iPortfolio['student_list'] = "Student List";
	$ec_iPortfolio['student'] = "Student";
	$ec_iPortfolio['class'] = "Class";
	$ec_iPortfolio['ClassName'] = "Class Name";
	$ec_iPortfolio['number'] = "Class No.";
	$ec_iPortfolio['ClassNumber'] = "Class Number";
	$ec_iPortfolio['LoginName'] = "Login Name";
	$ec_iPortfolio['ProgramRemarks'] = "Program Remarks";
	$ec_iPortfolio['ComponentCode'] = "Component Code";
	$ec_iPortfolio['identity'] = "Identity";
	$ec_iPortfolio['pob'] = "Place of Birth";
	$ec_iPortfolio['nationality'] = "Nationality";
	$ec_iPortfolio['address'] = "Address";
	$ec_iPortfolio['telno'] = "Tel. No.";
	$ec_iPortfolio['institute'] = "Name of Educational Institution";
	$ec_iPortfolio['doc'] = "Date of Completion";
	$ec_iPortfolio['class_number'] = "Class No.";
	$ec_iPortfolio['class_and_number'] = "Class No.";
	$ec_iPortfolio['mtype'] = "Type";
	$ec_iPortfolio['stype'] = "Type";
	$ec_iPortfolio['period'] = "Period";
	$ec_iPortfolio['amount'] = "Quantity";
	$ec_iPortfolio['reason'] = "Reason";
	$ec_iPortfolio['approval_date'] = "Approval Time";
	$ec_iPortfolio['student_report_print'] = "Student Report";
	$ec_iPortfolio['pic'] = "Person-in-charge";
	$ec_iPortfolio['role'] = "Position";
	$ec_iPortfolio['date'] = "Date";
	$ec_iPortfolio['remark'] = "Remarks";
	$ec_iPortfolio['performance'] = "Performance";
	$ec_iPortfolio['activity_name'] = "Activity Name";
	$ec_iPortfolio['iPortfolio_type'] = "iPortfolio Name";
	$ec_iPortfolio['contents_steps_menu'] = "iPortfolio Steps";
	$ec_iPortfolio['eca'] = "Extra-Curricular Activities";
	$ec_iPortfolio['column'] = "Column";

	$ec_iPortfolio['service'] = "Service Record";
	$ec_iPortfolio['service_name'] = "Service Name";
	$ec_iPortfolio['eClass_update_service'] = "Update Service Record from eClass";
	$ec_iPortfolio['update_more_service'] = "Update service record again";
	$ec_iPortfolio['remove_service_info'] = "Delete Service Record";
	$ec_iPortfolio['remove_more_service'] = "Delete more service records";
	$ec_iPortfolio['export_service_record'] = "Service Record Export";
	$ec_iPortfolio['upload_error'] = "Please Enter Valid File Path";
	$ec_iPortfolio['make_zip_download'] = "Create ZIP file and download";

	# For data export
	$ec_iPortfolio['data_export'] = "Data Export";
	$ec_iPortfolio['export_merit_record'] = "Merit Record Export";
	$ec_iPortfolio['export_assessment_record'] = "Assessment Record Export";
	$ec_iPortfolio['export_activity_record'] = "Activity Record Export";
	$ec_iPortfolio['export_award_record'] = "Award Record Export";
	$ec_iPortfolio['export_comment_record'] = "Teacher Comment Record Export";
	$ec_iPortfolio['export_attendance_record'] = "Attendance Record Export";
	$ec_iPortfolio['subject_result'] = "Subject Result";
	$ec_iPortfolio['student_overall_result'] = "Student Overall Result";
	$ec_iPortfolio['report_type'] = "Report Type";
	$ec_iPortfolio['record_type'] = "Record Type";
	$ec_iPortfolio['SLPOrder'] = 'Mark records students want to show in SLP';
	$ec_iPortfolio['SLPOrder_csv'] = 'SLP Display Order';

	$ec_iPortfolio['export']['CheckAll'] = "Check All";
	$ec_iPortfolio['export']['UnCheckAll'] = "Uncheck All";
	$ec_iPortfolio['export']['SelectFields'] = "Select Fields";
	$ec_iPortfolio['export']['WebSAMSRegNo'] = "WebSAMSRegNo";
	$ec_iPortfolio['export']['Programme'] = "Programme";
	$ec_iPortfolio['export']['ProgrammeDescription'] = "Programme Description";
	$ec_iPortfolio['export']['SchoolYearFrom'] = "School Year From";
	$ec_iPortfolio['export']['SchoolYearTo'] = "School Year To";
	$ec_iPortfolio['export']['ShowSemester'] = "Semester";
	$ec_iPortfolio['export']['Role'] = "Role Of Participation";
	$ec_iPortfolio['export']['Organization'] = "Partner Organization";
	$ec_iPortfolio['export']['Awards'] = "Awards / Certifications/ Achievements (including grades/marks, if any)";
	$ec_iPortfolio['export']['Student'] = "Student Name";
	$ec_iPortfolio['export']['ClassName'] = "Class Name";
	$ec_iPortfolio['export']['ClassNumber'] = "Class Number";
	$ec_iPortfolio['export']['Period'] = "Date/Period";
	$ec_iPortfolio['export']['Category'] = "Category";
	$ec_iPortfolio['export']['Hours'] = "Hours";
	$ec_iPortfolio['export']['Details'] = "Details";
	$ec_iPortfolio['export']['ApprovedBy'] = "Approved By";
	$ec_iPortfolio['export']['Status'] = "Status";
	$ec_iPortfolio['export']['ApprovedDate'] = "Approved Date";
	$ec_iPortfolio['export']['Remarks'] = "School Remarks";
	$ec_iPortfolio['export']['ELE'] = "Component of OLE";
	$ec_iPortfolio['export']['Comment'] = "Comment";

$ec_iPortfolio['exportWebSAMS']['WebSAMSRegNo'] = "學生註冊編號
Registration Number";
$ec_iPortfolio['exportWebSAMS']['ProgrammeNameEng'] = "活動項目(英文)
Programme Name (Eng)";
$ec_iPortfolio['exportWebSAMS']['ProgrammeNameChi'] = "活動項目(中文)
Programme Name (Chi)";
$ec_iPortfolio['exportWebSAMS']['ProgrammeDescriptionEng'] = "活動項目簡介(英文)
Programme Description (Eng)";
$ec_iPortfolio['exportWebSAMS']['ProgrammeDescriptionChi'] = "活動項目簡介(中文)
Programme Description (Chi)";
$ec_iPortfolio['exportWebSAMS']['SchoolYearFrom'] = "由學年
School Year From";
$ec_iPortfolio['exportWebSAMS']['SchoolYearTo'] = "至學年
School Year To";
$ec_iPortfolio['exportWebSAMS']['RoleEng'] = "參與角色(英文)
Role of Participation (Eng)";
$ec_iPortfolio['exportWebSAMS']['RoleChi'] = "參與角色(中文)
Role of Participation (Chi)";
$ec_iPortfolio['exportWebSAMS']['PartnerOrganizationEng'] = "合辦機構(英文)
Partner Organization (Eng)";
$ec_iPortfolio['exportWebSAMS']['PartnerOrganizationChi'] = "合辦機構(中文)
Partner Organization (Chi)";
$ec_iPortfolio['exportWebSAMS']['OLEEng'] = "其他學習經歷的主要種類(英文)
Major Components of Other Learning Experiences (Eng)";
$ec_iPortfolio['exportWebSAMS']['OLEChi'] = "其他學習經歷的主要種類(中文)
Major Components of Other Learning Experiences (Chi)";
$ec_iPortfolio['exportWebSAMS']['AwardsEng'] = "獎項 / 證書文憑 / 成就 (如有)(英文)
Awards / Certifications / Achievements (if any) (Eng)";
$ec_iPortfolio['exportWebSAMS']['AwardsChi'] = "獎項 / 證書文憑 / 成就 (如有)(中文)
Awards / Certifications / Achievements (if any) (Chi)";
$ec_iPortfolio['exportWebSAMS']['Validation'] = "校驗 Validation";


$ec_iPortfolio['highlight_result'] = "Grade Highlight";
$ec_iPortfolio['overall_rank'] = "Overall Rank";


$ec_iPortfolio['DGS_Custom']['Student'] = "Student";
$ec_iPortfolio['DGS_Custom']['CurrentStudent'] = "Current Student";
$ec_iPortfolio['DGS_Custom']['AllStudent'] = "All Student";
$ec_iPortfolio['export_DGS']['EnglishName'] = "English Name";
$ec_iPortfolio['export_DGS']['ChineseName'] = "chinese Name";
$ec_iPortfolio['export_DGS']['DOB'] = "DOB";
$ec_iPortfolio['export_DGS']['AdmissionDate'] = "Admission Date";
$ec_iPortfolio['export_DGS']['HKID'] = "HKID";
$ec_iPortfolio['export_DGS']['WebSAMSRegNo'] = "WebSAMSRegNo";
$ec_iPortfolio['export_DGS']['AwardName'] = "Award Name";
$ec_iPortfolio['export_DGS']['SchoolYear'] = "School Year";
$ec_iPortfolio['export_DGS']['ProgrammeName'] = "Programme Name";
$ec_iPortfolio['export_DGS']['AcademicYear'] = "Academic Year";
$ec_iPortfolio['export_DGS']['Role'] = "Role";
$ec_iPortfolio['export_DGS']['ComponentofOLE'] = "Component of OLE";
$ec_iPortfolio['export_DGS']['Achievement'] = "Achievement";
$ec_iPortfolio['export_DGS']['Category'] = "Category";
$ec_iPortfolio['export_DGS']['Organization'] = "Organization";
$ec_iPortfolio['export_DGS']['ClassName'] = "Class Name";
$ec_iPortfolio['export_DGS']['ClassNo'] = "Class No";

$ec_iPortfolio['activity_role_template_instruction'] = "You can edit the preset activity roles. Each line is one item. Please click update to save the changes.";
$ec_iPortfolio['activity_awards_template_instruction'] = "You can edit the preset activity awards. Each line is one item. Please click update to save the changes.";
$ec_iPortfolio['enter_title_name'] = "Enter Title Name";

	$ec_iPortfolio['lang_chi'] = "Chi";
	$ec_iPortfolio['lang_eng'] = "Eng";
	$ec_iPortfolio['total_hours'] = "Total Hours";
	$ec_iPortfolio['olr_display_record'] = "Display Record";
	$ec_iPortfolio['olr_display_stat'] = "Display Statistic";
	$ec_iPortfolio['olr_reference_guide'] = "Please follow these codes to fill in cateory and component of OLE:";
	$ec_iPortfolio['ele'] = "Component of OLE";
	$ec_iPortfolio['default_ele'] = "Default Component of OLE";
	$ec_iPortfolio['ele_setting'] = "Component of OLE Setting";
	$ec_iPortfolio['ele_code'] = "OLE Component Code";
	$ec_iPortfolio['category_setting'] = "Category Setting";
	$ec_iPortfolio['default'] = "Default";

  $ec_iPortfolio['all_status'] = "All Status";
	$ec_iPortfolio['all_category'] = "All Categories";
	$ec_iPortfolio['all_tag'] = "All Additional Category";
	$ec_iPortfolio['tag'] = "Additional Category";
	$ec_iPortfolio['title'] = "Title";
	$ec_iPortfolio['chinese'] = "Chinese";
	$ec_iPortfolio['english'] = "English";
	$ec_iPortfolio['chart_report'] = "Hour Statistics";
	$ec_iPortfolio['student_year_accum_chart'] = "Yearly Accumulated Hours";
	$ec_iPortfolio['class_stat_chart'] = "Class Statistics";
	$ec_iPortfolio['cat_stat_chart'] = "Category Statistics";
	$ec_iPortfolio['with_slp'] = "With Student Learning Profile report";
	$ec_iPortfolio['with_freport'] = "With Full report";
	$ec_iPortfolio['with_sbs'] = "With School-based Scheme";
	$ec_iPortfolio['with_attachment'] = "With Award/Certificate attachement";
	$ec_iPortfolio['growth_title'] = "Title";
	$ec_iPortfolio['growth_description'] = "Description";
	$ec_iPortfolio['growth_group'] = "Participating Group";
	$ec_iPortfolio['growth_status'] = "Status";
	$ec_iPortfolio['growth_phase'] = "Phase";
	$ec_iPortfolio['growth_scheme_title'] = "Scheme Title";
	$ec_iPortfolio['growth_phase_title'] = "Phase Title";
	$ec_iPortfolio['growth_phase_period'] = "Period";
	$ec_iPortfolio['growth_target_person'] = "Participants";
	$ec_iPortfolio['growth_new_phase'] = "New Phase";
	$ec_iPortfolio['growth_edit_phase'] = "Editing Phase";
	$ec_iPortfolio['growth_relevant_phase'] = "Related Phase";
	$ec_iPortfolio['growth_online_form'] = "Form";
	$ec_iPortfolio['growth_contents'] = "Contents";
	$ec_iPortfolio['growth_info'] = "Informatics";
	$ec_iPortfolio['growth_create'] = "Create";
	$ec_iPortfolio['form_display_v'] = "Question Format";
	$ec_iPortfolio['form_display_h'] = "Brief Format";
	$ec_iPortfolio['form_fill'] = "Fill in the form";
	$ec_iPortfolio['form_not_answered'] = "Not answered";
	$ec_iPortfolio['weblog_date'] = "Date";
	$ec_iPortfolio['weblog_title'] = "Title";
	$ec_iPortfolio['weblog_contents'] = "Contents";
	$ec_iPortfolio['weblog_config'] = "Set display format";
	$ec_iPortfolio['weblog_config_way'] = "List formats";
	$ec_iPortfolio['weblog_config_all'] = "All";
	$ec_iPortfolio['weblog_config_list_year'] = "By year";
	$ec_iPortfolio['weblog_config_list_month'] = "By year and month";
	$ec_iPortfolio['weblog_config_order'] = "Order by time";
	$ec_iPortfolio['weblog_config_order_A'] = "In ascending order";
	$ec_iPortfolio['weblog_config_order_D'] = "In decending order";
	$ec_iPortfolio['weblog_config_layout'] = "Page Layout";
	$ec_iPortfolio['no_record_exist'] = "No such record";
	$ec_iPortfolio['web_structure_changed'] = "There is change(s) in this Learning Portfolio!";
	$ec_iPortfolio['edit_template_alert'] = "Warning: This page is using template \'XXX\'. Any modification made on this page will be mirrored in other pages sharing the same template. To modify just this page, assign it a new template.";

	$ec_iPortfolio['growth_answer_display_mode'] = "Answer Display Mode";
	$ec_iPortfolio['growth_display_answer_only'] = "Display answer only";
	$ec_iPortfolio['growth_display_answer_format'] = "Display answers and choices";
	$ec_iPortfolio['print_result'] = "Print Result";

	$ec_iPortfolio['upload_photo_fail'] = "Record Updated failed! Please check the file type of School Header Image.";

	$ec_iPortfolio['female'] = "Female";
	$ec_iPortfolio['male'] = "Male";
	$ec_iPortfolio['not_display_student_photo'] = "Do not display student photo";
	$ec_iPortfolio['phase_in_processing_state'] = "Attention: This phase is in processing state. The answer current shown may not be the final answer.";
	$ec_iPortfolio['growth_not_submitted'] = "Not Submitted";

	$ec_iPortfolio_guide['alert_student'] = "You may send an email alert to remind student(s).";
	$ec_iPortfolio_guide['alert_class_teacher'] = "You may send an email alert to remind class teacher(s).";
	$ec_iPortfolio_guide['alert_subject_teacher'] = "You may send an email alert to subject teacher(s).";
	$ec_iPortfolio_guide['alert_parent'] = "You may send an email alert to remind parent(s).";
	$ec_iPortfolio['distinct_form_desc'] = "Brief Format: Questions are displayed parallel to answers.<br />Question Format: Questions are displayed above answers.";

	$ec_iPortfolio['no_student_info'] = "Do not display student information";
	$ec_iPortfolio['no_student_details'] = "Do not display student details";
	$ec_iPortfolio['no_school_name'] = "Do not display school name in the first page";
	$ec_iPortfolio['no_related_phase'] = "Do not display related phase";

	$ec_iPortfolio['people'] = "people";
	$ec_iPortfolio['updated'] = "updated";
	$ec_iPortfolio['revise_result'] = "Revise Result";

	$ec_guardian['01'] = "Father";
	$ec_guardian['02'] = "Mother";
	$ec_guardian['03'] = "Grandfather";
	$ec_guardian['04'] = "Grandmother";
	$ec_guardian['05'] = "Brother";
	$ec_guardian['06'] = "Sister";
	$ec_guardian['07'] = "Relative";
	$ec_guardian['08'] = "Other";

	$ec_iPortfolio['relation'] = "Relationship";
	$ec_iPortfolio['phone'] = "Phone No.";
	$ec_iPortfolio['em_phone'] = "Emergency Phone No.";

	$ec_iPortfolio['school_profile_mgt'] = "School Records Management";
	$ec_iPortfolio['web_teacher_mgt'] = "Learning Portfolio Management";
	$ec_iPortfolio['growth_mgt'] = "School-based Scheme Management";
	$ec_iPortfolio['web_view'] = "Learning Portfolio";

	$ec_iPortfolio['growth_view'] = "School-based Scheme";
	$ec_iPortfolio['school_profile'] = "School Records";
	$ec_iPortfolio['my_learning_sharing'] = "My Learning Portfolio";
	$ec_iPortfolio['my_learning_sharing_mgt'] = "My Learning Portfolio Management";

	$ec_iPortfolio['all_programmes'] = "All Programmes";
	$ec_iPortfolio['my_programmes'] = "My Programmes";
	$ec_iPortfolio['all_students'] = "All Students";
	$ec_iPortfolio['my_students'] = "My Students";

	$ec_iPortfolio['growth_management'] = "Scheme Management";
	$ec_iPortfolio['growth_work_on'] = "Scheme Participation";
	$ec_iPortfolio['scheme'] = "Scheme";

	$ec_iPortfolio['login_teacher'] = "Teacher Login";
	$ec_iPortfolio['login_student'] = "Student Login";
	$ec_iPortfolio['login_parent'] = "Parent Login";

	$ec_iPortfolio['sys_manage'] = "Go to Management";
	$ec_iPortfolio['sys_view'] = "Back to View";

	$ec_iPortfolio['greeting'] = "Welcome to Student General Portfolio";
	$ec_iPortfolio['class_directory'] = "iPortfolio Class Directory";
	$ec_iPortfolio['class_directory_intro'] = "To search for your student portfolio, you may start with Class Directory for the follow-through steps, or use the search tool for direct search.";
	$ec_iPortfolio['growth_student_name'] = "Student Name";

	$ec_iPortfolio['growth_work'] = "You may fill in here";
	$ec_iPortfolio['student_photo'] = "Student Photo";
	$ec_iPortfolio['student_photo_no'] = "No photo";
	$ec_iPortfolio['preferred_approver'] = "Preferred Approver";


	$ec_iPortfolio['category'] = "Category";
	$ec_iPortfolio['sub_category'] = "Subcategory";
	$ec_iPortfolio['NoOfSubcategory'] = "View Programme Sub-category";
	$ec_iPortfolio['NewCategory'] = "No. of Subcategory";
	$ec_iPortfolio['NewSubcategory'] = "New Subcategory";
	$ec_iPortfolio['PresetProgrammeName'] = "Preset Programme Name";
	$ec_iPortfolio['NewPresetProgrammeName'] = "Add Preset Programme Name";
	$ec_iPortfolio['NoOfPresetProgrammeName'] = "No. of Preset Programme Name";
	$ec_iPortfolio['AllPresetProgrammeName'] = "View Preset Programme";
	$ec_iPortfolio['Categorize'] = "Categorize";
	$ec_iPortfolio['Active'] = "Active";
	$ec_iPortfolio['Suspend'] = "Suspend";
# Language for Student Learning Profile
$ec_iPortfolio['SLP']['EditableFields']			= "Editable Fields";
$ec_iPortfolio['SLP']['AllowStudentsToJoin']	= "Allow Participation Reporting";
$ec_iPortfolio['SLP']['Yes']					= "Yes";
$ec_iPortfolio['SLP']['No']						= "No";
$ec_iPortfolio['SLP']['JoinPeriod']				= "Join Period";
$ec_iPortfolio['SLP']['PeriodStart']			= "Period Start";
$ec_iPortfolio['SLP']['PeriodEnd']				= "Period End";
$ec_iPortfolio['SLP']['InvalidPeriodStart']		= "Invalid Format of Period Start";
$ec_iPortfolio['SLP']['InvalidPeriodEnd']		= "Invalid Format of Period End";
$ec_iPortfolio['SLP']['PeriodStart_LT_PeriodEnd']	= "Period Start is later than Period End";
$ec_iPortfolio['SLP']['From']					= "From";
$ec_iPortfolio['SLP']['to']						= "to";
$ec_iPortfolio['SLP']['now']					= "now";
$ec_iPortfolio['SLP']['forever']				= "forever";
$ec_iPortfolio['SLP']['button_Join']			= "Join";
$ec_iPortfolio['SLP']['Title']					= "Title";
$ec_iPortfolio['SLP']['SchoolYear']				= "School Year";
$ec_iPortfolio['SLP']['Category']				= "Category";
$ec_iPortfolio['SLP']['FillTogether']			= "Period Start and Period End must be filled in together";
$ec_iPortfolio['SLP']['Status']					= "Status";
$ec_iPortfolio['SLP']['ApprovedDate']			= "Approved Date";
$ec_iPortfolio['SLP']['CreatedBy']				= "Created By";
$ec_iPortfolio['SLP']['AutoApprove']			= "Allow Auto Approval";
$ec_iPortfolio['SLP']['warning_Hour']			= "Invalid Hour Format";
$ec_iPortfolio['SLP']['TeacherCreatedProgram']	= "Teacher created programme";
$ec_iPortfolio['SLP']['StudentCreatedProgram']	= "Student created programme";
$ec_iPortfolio['SLP']['JoinableYear']			= "Applicable Form(s)";
$ec_iPortfolio['SLP']['ChooseAtLeastOneYear']	= "Please select at least one form";
$ec_iPortfolio['SLP']['ReportOtherActivities']	= "New Record";
$ec_iPortfolio['SLP']['SchoolPresetActivities']	= "School Preset Activities";
$ec_iPortfolio['SLP']['OleJoinNotes1']			= "Note: Please press an appropriate activity title from <b>School Preset Activities</b> below to report your participation.";
$ec_iPortfolio['SLP']['OleJoinNotes2']			= "Press <b>New Record</b> if you cannot find the activity you want to report.";

$ec_iPortfolio['SLP']['OleJoinNotes']['WithinSubmissionPeriod'] = "Note: Please select an appropriate activity title from <b>School Preset Activities</b> below to report your participation.  Otherwise, press <b>New</b> to submit new record.";
$ec_iPortfolio['SLP']['OleJoinNotes']['WithoutSubmissionPeriod'] = "Note: The submission period for <b>Other Activities</b> has expired, user cannot add Other Activities.  However, you may select an appropriate activity title from <b>School Preset Activities</b> below to report your participation.";

$ec_iPortfolio['SLP']['OLE_approval_mail']['title'] = "OLE record approval request from [StudentName] [[DateNow]] ";
$ec_iPortfolio['SLP']['OLE_approval_mail']['body'] = "<p>[StudentName] submitted an OLE titled \"[Title]\" on [DateNow].</p>" .
					"<p>You can <a href=\"<!--schoolUrl-->/home/portfolio/teacher/management/oleIndex.php?clearCoo=1\" target=\"OLE_approval\">click here</a> to access the approval page directly (user login is required).</p>";

$ec_iPortfolio['SLP']['ExportAsPdf']			= "Export as PDF";
$ec_iPortfolio['SLP']['ExportAsWordDocument']			= "Export as Word Document";
$ec_iPortfolio['SLP']['Name'] = "Name";
$ec_iPortfolio['SLP']['MoveTo'] = "Move to";
$ec_iPortfolio['SLP']['SubCategory'] = "Sub-Category";
$ec_iPortfolio['NoPrintToLimit'] = "Do not print any record if student does not specify.";
$ec_iPortfolio['get_prepared_portfolio_ST'] = "Click the button to download the file";

$ec_iPortfolio['all_subcategory'] = "All Subcategory";
$ec_iPortfolio['graduation_year'] = "Graduation Year";
$ec_iPortfolio['please_select_graduation_year'] = "Please Select Graduation Year";
$ec_iPortfolio['view_peer_profolio'] = "View peer's learning portfolios";
$ec_iPortfolio['View'] = "View ";
$ec_iPortfolio['learning_portfolio'] = "'s learning portfolio";

$ec_iPortfolio['NoRecordsUpdated'] = "No Records Updated";
$ec_iPortfolio['DoesNotHaveDefaultSelfAccount'] = "does no have default self account";
$ec_iPortfolio['pkms_SLP_config'] = "Pui Kiu Student Learning Profile";
### CCYM customarized report ###
$ec_iPortfolio['ccym_ole_report'] = "OLE Report";
$ec_iPortfolio['import_regno'] = "Import WebSAMSRegNo";
$ec_iPortfolio['heading']['student_regno'] = "WebSAMSRegNo";




#Start from 20100323 , all new iportfolio Lang should with $Lang (IP25 convention)
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][0] = 'ClassName';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][1] = 'ClassNumber';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][2] = 'WebSAMSRegNo';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][3] = 'Title';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][4] = 'SelfAccount';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][0] = '班別';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][1] = '班號';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][2] = 'WebSAMS學生註冊編號';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][3] = '題目';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][4] = '學生自述';
$Lang['iPortfolio']['noAccessRightRedirectMsg'] = 'Access rights to iPortfolio not assigned by administrator.\n Press \'Confirm\' to return to homepage.';
$Lang['iPortfolio']['redirecting'] = 'Redirecting...';
$Lang['iPortfolio']['standAloneWithoutAccount']	= 'Your iPortfolio account has not been activated. Please contact your system administrator to activate. ';
$Lang['iPortfolio']['wholeYearIncludeNullSem']	= 'Whole Year (Including records not belonging to any term)';

$Lang['iPortfolio']['PleaseInputPositiveNumber'] = "Please input positive number";
$Lang['iPortfolio']['alertYearExceed'] = "You have input a year too far away from now. Please try again.";
$Lang['iPortfolio']['alertExceedWordLimit'] = "You have entered too much. The maximum number of words allowed is <!--WordLimit-->.";
$Lang['iPortfolio']['alertLeavePage'] = "Your data is being processed. Please stay in this page and do NOT close the browser";
$Lang['iPortfolio']['allowed'] = "Allow";
$Lang['iPortfolio']['allRecord'] = "All Records";
$Lang['iPortfolio']['all_content'] = "All content";
$Lang['iPortfolio']['advanceSetting'] = "Advance Settings";
$Lang['iPortfolio']['approver'] = "Approver";
$Lang['iPortfolio']['InputBy'] = "Creator";
$Lang['iPortfolio']['approvaldate'] = "Approval Date";
$Lang['iPortfolio']['auditby'] = "Audited By";
$Lang['iPortfolio']['auditdate'] = "Audit Date";

$Lang['iPortfolio']['cd_burning_setting_forparent'] = "Allow parents to burn CD-ROM";
$Lang['iPortfolio']['cd_burning_setting_forstudent'] = "Allow students to burn CD-ROM";
$Lang['iPortfolio']['commonAC'] = "Common Ability / Main Criteria";
$Lang['iPortfolio']['custReport'] = "Customized Report";

$Lang['iPortfolio']['default'] = "Default";

$Lang['iPortfolio']['Group'] = "Group";
$Lang['iPortfolio']['GroupSettingRemark'] = "Permission is for VIEW records only.";
$Lang['iPortfolio']['GroupBasic'] = "Basic Information";
$Lang['iPortfolio']['GroupID'] = "Group ID";
$Lang['iPortfolio']['GroupList'] = "Group List";
$Lang['iPortfolio']['GroupList_instruction'] = "Groups and their teacher members can be set in Settings > Group > Management Group";
$Lang['iPortfolio']['GroupMemberName'] = "Name of Member";
$Lang['iPortfolio']['GroupName'] = "Group Name";
$Lang['iPortfolio']['GroupRight'] = "Rights Settings";
$Lang['iPortfolio']['GroupSetting'] = "Group Settings";

$Lang['iPortfolio']['LastPublishDate'] = "Last Publish Date";
$Lang['iPortfolio']['LPnotOpened'] = "This learning portfolio has not been opened and worked on yet.";
$Lang['iPortfolio']['LPnotPublished'] = "This learning portfolio has not been published yet.";
$Lang['iPortfolio']['LPnoWordDoc'] = "Word document does not exists.  Please publish again.";
$Lang['iPortfolio']['LPSetting'] = "Learning Portfolio Settings";
$Lang['iPortfolio']['LP_Rights']['learning_portfolio_content'] = "Can access <strong>Management > Learning Portfolio > Portfolios tab</strong>";
$Lang['iPortfolio']['LP_Rights']['manage_templates'] = "Can access <strong>Management > Learning Portfolio > Template Management tab</strong>";

$Lang['iPortfolio']['mgmtGroup'] = "Management Group";

$Lang['iPortfolio']['NumGroupMember'] = "Number of Group Members";

$Lang['iPortfolio']['onlyPortfolioUser'] = "Only users who are granted the rights to use iPortfolio are shown below.";
$Lang['iPortfolio']['orderby'] = "Order by";
$Lang['iPortfolio']['orderbyName'] = "Name";
$Lang['iPortfolio']['orderbyClass'] = "Class";
$Lang['iPortfolio']['orderbyLastPublishedDate'] = "Last Published Date";

$Lang['iPortfolio']['preferred_approver'] = "Preferred Approver";
$Lang['iPortfolio']['default_approver'] = "Default Approver";
$Lang['iPortfolio']['prev_published_content'] = "Previously published content";
$Lang['iPortfolio']['print_date_issue'] = "Display date of issue";
$Lang['iPortfolio']['print_slp_lang'] = "Header Language";
$Lang['iPortfolio']['print_slp_lang_en'] = "English";
$Lang['iPortfolio']['print_slp_lang_b5'] = "Chinese";
$Lang['iPortfolio']['prohibit'] = "Prohibit";
$Lang['iPortfolio']['publish_student_complete'] = "Selected students' iPortfolio are completed.";
$Lang['iPortfolio']['publish_student_content'] = "Are you sure that you want to publish selected students' iPortfolio?";

$Lang['iPortfolio']['resetButton'] = "Reset Button";
$Lang['iPortfolio']['disable_facebook'] = "Facebook Like & Share Function";
$Lang['iPortfolio']['restrictToComponent'] = "Records belonging to selected component(s) only:";


$Lang['iPortfolio']['page_break_ywgs'] = "When necessary, you can insert \"[PAGE-BREAK]\" to break your contents into 2 pages for printing.";
$Lang['iPortfolio']['selectedRecord'] = "Selected Records";
$Lang['iPortfolio']['selfAccountWordCount'][0] = "You have written:";
$Lang['iPortfolio']['selfAccountWordCount'][1] = "words (Chinese characters not counted)";
$Lang['iPortfolio']['selfAccountIsEmpty'] = "Please input content";
$Lang['iPortfolio']['Settings_BurningCD'] = "CD burning";
$Lang['iPortfolio']['Settings_Grouping'] = "Group Management";
$Lang['iPortfolio']['SLPRecord'] = "SLP Records";
$Lang['iPortfolio']['SpaceBottom'] = "lower Spacing Adjustment";
$Lang['iPortfolio']['SpaceTop'] = "Top Spacing Adjustment";
$Lang['iPortfolio']['spacingHeight'] = "Spacing Adjustment";
$Lang['iPortfolio']['SP_Rights']['assessment_stat_report'] = "Can view the Assessment Static Report";
$Lang['iPortfolio']['SP_Rights']['data_import'] = "Can access <strong>Management > Student Learning Profile > School Record tab</strong> to perform data interchange with WebSAMS";
$Lang['iPortfolio']['SP_Rights']['export_slp_data'] = "Can export data from <strong>Management > Student Learning Profile</strong>";
$Lang['iPortfolio']['SP_Rights']['management_student'] = "Can access <strong>Management > Student Account</strong>";
$Lang['iPortfolio']['SP_Rights']['student_ole_manage'] = "Can view and approve all/selected types of Other Learning Experiences records";
$Lang['iPortfolio']['SP_Rights']['student_report_printing'] = "Can print the Student Report";
$Lang['iPortfolio']['SP_Rights']['student_report_setting'] = "Can access <strong>Settings > Reports</strong>";
$Lang['iPortfolio']['SP_Rights']['self_added_record_delete'] = "In <strong>Management > Student Learning Profile > Other Learning Experiences</strong> OR <strong>Performance / Awards and Key Participation Outside School > Programme</strong>, batch manage self-added records only";
$Lang['iPortfolio']['studentGroup'] = "Student Group";
$Lang['iPortfolio']['StudentViewDisplay'] = "Display in student view";
$Lang['iPortfolio']['SubmissionSetting'] = "Submission Period Settings";
$Lang['iPortfolio']['OverallResult'] = "Overall Result";
$Lang['iPortfolio']['MarkDifference'] = "Mark Difference";
$Lang['iPortfolio']['MarkDifference(%)'] = "Mark Difference (%)";
$Lang['iPortfolio']['ZScoreDifference'] = "Standard Score Difference";
$Lang['iPortfolio']['ZScoreDifference(%)'] = "Standard Score Difference (%)";
$Lang['iPortfolio']['ExpectedMark'] = "Expected Score";
$Lang['iPortfolio']['ExpectedMarkDifference'] = "Expected Score Difference";
$Lang['iPortfolio']['RegressionLineEquation'] = "Regression Line equation: b0 + b1(S1x)";
$Lang['iPortfolio']['AcademicProgress'] = "Academic Progress";
$Lang['iPortfolio']['AcademicProgressByStd'] = "Class Marksheets";
$Lang['iPortfolio']['AssessmentResultOfStd'] = "Student Marksheets";
$Lang['iPortfolio']['SubjectStats'] = "Subject Statistics";
$Lang['iPortfolio']['TeacherStats'] = "Passing Stats by Teacher";
$Lang['iPortfolio']['ImprovementStatsBySbj'] = "Subject Improvements";
$Lang['iPortfolio']['ImprovementStatsByTeacher'] = "Improvement Stats by Teacher";
$Lang['iPortfolio']['InternalValueAdded'] = "Internal Value Added Index";
$Lang['iPortfolio']['DseStats'] = "DSE Analysis";
$Lang['iPortfolio']['teacherGroup'] = "Teachers";
$Lang['iPortfolio']['FunctionAccessRight'] = "Function Access Right";

$Lang['iPortfolio']['ChangeClassCompare'] = "Change Class Student Compare";

$Lang['iPortfolio']['TeacherComment'] = "Teacher Comment";
$Lang['iPortfolio']['TeacherTutor'] = "Teacher";
$Lang['iPortfolio']['NotActivatedPortfolioStudent'] = "Representing that the student has not activated iPortfolio";

$Lang['iPortfolio']['SubjectFullMark'] = "Subject Full Mark";
$Lang['iPortfolio']['SubjectFullMarkInputTips'] = "You may copy Full Marks from a Excel file and paste them to the below textboxes.";
$Lang['iPortfolio']['SubjectFullMarkInputWarning'] = "SLP Report will show the Grade of the Full Mark if the Grade is input as the Full Mark of a Subject.";
$Lang['iPortfolio']['LoadAllFullMarkWarning'] = "Viewing full mark of all Forms may require some times (depending on the number of Subjects and Forms). Are you sure you want to view the Full Mark of all Forms?";
$Lang['iPortfolio']['Subject_En'] = "Subject";
$Lang['iPortfolio']['Subject_Ch'] = "學科";
$Lang['iPortfolio']['Mark_En'] = "Mark";
$Lang['iPortfolio']['Mark_Ch'] = "分數";
$Lang['iPortfolio']['FullMark_En'] = "Full_Mark";
$Lang['iPortfolio']['FullMark_Ch'] = "滿分";
$Lang['iPortfolio']['PassMark_En'] = "Pass_Mark";
$Lang['iPortfolio']['PassMark_Ch'] = "合格分";
$Lang['iPortfolio']['Grade_En'] = "Grade";
$Lang['iPortfolio']['Grade_Ch'] = "等級";
$Lang['iPortfolio']['GeneralSettings'] = "General Settings";
$Lang['iPortfolio']['AdvancedSettings'] = "Advanced Settings";
$Lang['iPortfolio']['Display'] = "Display";
$Lang['iPortfolio']['Filter'] = "Filter";
$Lang['iPortfolio']['DataColumn'] = "Data Column";
$Lang['iPortfolio']['DisplayCriteria'] = "Display Criteria";
$Lang['iPortfolio']['AcademicResult'] = "Academic Result";
$Lang['iPortfolio']['MarkOnly'] = "Show mark only.";
$Lang['iPortfolio']['GradeOnly'] = "Show grade only.";
$Lang['iPortfolio']['MarkThenGrade'] = "Show mark if available. Otherwise, grade will be shown.";
$Lang['iPortfolio']['GradeThenMark'] = "Show grade if available. Otherwise, mark will be shown.";
$Lang['iPortfolio']['AcademicResult_Mark'] = "Academic Result (".$Lang['iPortfolio']['MarkOnly'].")";
$Lang['iPortfolio']['AcademicResult_Grade'] = "Academic Result (".$Lang['iPortfolio']['GradeOnly'].")";
$Lang['iPortfolio']['AcademicResult_MarkThenGrade'] = "Academic Result (".$Lang['iPortfolio']['MarkThenGrade'].")";
$Lang['iPortfolio']['AcademicResult_GradeThenMark'] = "Academic Result (".$Lang['iPortfolio']['GradeThenMark'].")";
$Lang['iPortfolio']['DisplayOptions'] = "Display Options";
$Lang['iPortfolio']['DisplayFullMark'] = "Display Full Mark";
$Lang['iPortfolio']['DisplayComponentSubject'] = "Display Component Subject(s)";
$Lang['iPortfolio']['DisplaySelfAccount'] = "Display Self-Account";
$Lang['iPortfolio']['DisplayReportHeader'] = "Display School Logo and School Name";
$Lang['iPortfolio']['DisplayReportFooter'] = "Display Footer";
$Lang['iPortfolio']['LowerForm'] = "Lower Forms";
$Lang['iPortfolio']['HigherForm'] = "Higher Forms";
$Lang['iPortfolio']['CannotSameYear'] = "Academic year cannot be the same";

$Lang['iPortfolio']['oleByCreator'] = "By Creator";
$Lang['iPortfolio']['oleOwn'] = "Programme I created";
$Lang['iPortfolio']['oleSourceByApprovalRequest'] = "By Source of Approval Request";
$Lang['iPortfolio']['oleSourceByIndividualStudent'] = "From individual student";
$Lang['iPortfolio']['oleSourceFromMyClass'] = "From my class";
$Lang['iPortfolio']['oleSourceFromMyGroup'] = "From my OLE group";

$Lang['iPortfolio']['OLEArr']['ActivateStudentWarning'] = "The activation of the student accounts will reduce the iPortfolio quota accordingly and the quota cannot be recovered. If confirm, press Submit.";

$Lang['iPortfolio']['DynamicReport']['EmptyDataSymbol'] = "Empty Data Symbol";
$Lang['iPortfolio']['DynamicReport']['IssueDate'] = "Date of Issue";
$Lang['iPortfolio']['DynamicReport']['IssueDateGuide'] = "This date and its format will appear on the report as is.";
$Lang['iPortfolio']['DynamicReport']['ShowNoDataTable'] = " Show Table with no data";
$Lang['iPortfolio']['DynamicReport']['NoDataTableContent'] = "Table Content";
$Lang['iPortfolio']['DynamicReport']['DataContentDisplay'] = "Data Content Display";
$Lang['iPortfolio']['DynamicReport']['NoCriteriaSettingsRemarks'] = "The selected data source(s) have no criteria settings.";
$Lang['iPortfolio']['DynamicReport']['PrintingDirection'] = "Printing Direction";
$Lang['iPortfolio']['DynamicReport']['Portrait'] = "Portrait";
$Lang['iPortfolio']['DynamicReport']['Landscape'] = "Landscape";
$Lang['iPortfolio']['DynamicReport']['OrderByStudentSelectedOrdering'] = "Print in the order of SLP Records chosen by students";
$Lang['iPortfolio']['DynamicReport']['OrderByReportSetting'] = "Print in the order of SLP Records as report setting";
$Lang['iPortfolio']['DynamicReport']['DisplayFullMark'] = "Display Full Mark";
$Lang['iPortfolio']['DynamicReport']['PrintHtml'] = "Print (HTML)";
$Lang['iPortfolio']['DynamicReport']['PrintPdf'] = "Print (PDF)";
$Lang['iPortfolio']['DynamicReport']['WarningArr']['EditDefaultTemplate'] = "System will create a new custom template when you save this default template. The original content of the default template will not be affected.";

$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['MustHaveText'] = "Text must has value";
$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['NoCriteria'] = "No Criteria";
$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['TextContains'] = "Text contains";
$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['TextDoesNotContain'] = "Text does not contain";
$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['TextIsExactly'] = "Text is exactly";

$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['ShowStatistics'] = "Show Statistics";
$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatisticsData'] = "Statistics Data";
$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['Blank'] = "Blank";
$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatOptionAry']['sum'] = "Sum";
$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatOptionAry']['text'] = "Text";

$Lang['iPortfolio']['DynamicReport']['ReleaseToStudent'] = "Release to student";
$Lang['iPortfolio']['DynamicReport']['ReleaseStatus'] = "Release status";
$Lang['iPortfolio']['DynamicReport']['Release'] = "Release";
$Lang['iPortfolio']['DynamicReport']['Unrelease'] = "Unrelease";
$Lang['iPortfolio']['DynamicReport']['Released'] = "Released";
$Lang['iPortfolio']['DynamicReport']['RegenerateReport'] = "Regenerate Report";
$Lang['iPortfolio']['DynamicReport']['IssueDate'] = "IssueDate";

$Lang['iPortfolio']['StudentReport']['PrintingMode'] = "Printing Mode";
$Lang['iPortfolio']['StudentReport']['PrintingMode_All'] = "Export a single file with all student reports";
$Lang['iPortfolio']['StudentReport']['PrintingMode_Zip'] = "Export a zipped file with separated student report files";
$Lang['iPortfolio']['other_reports'] = "Other Reports";
$Lang['iPortfolio']['OtherReports']['Title'] = "Report Title";
$Lang['iPortfolio']['OtherReports']['CreationDate'] = "Creation Date";
$Lang['iPortfolio']['OtherReports']['LastModifiedDate'] = "Last Modified Date";
$Lang['iPortfolio']['OtherReports']['NumberOfItems'] = "Number of items";
$Lang['iPortfolio']['OtherReports']['ReportInformation'] = "Report Information";
$Lang['iPortfolio']['OtherReports']['Description'] = "Description";
$Lang['iPortfolio']['OtherReports']['TitleNotes'] = "will appear in report's filename";
$Lang['iPortfolio']['OtherReports']['AlertInputTitle'] = "Please type the report title.";
$Lang['iPortfolio']['OtherReports']['ReportFile'] = "Report File";
$Lang['iPortfolio']['OtherReports']['Class'] = "Class";
$Lang['iPortfolio']['OtherReports']['Student'] = "Student";
$Lang['iPortfolio']['OtherReports']['PleaseSelectClass'] = "Please select class.";
$Lang['iPortfolio']['OtherReports']['PleaseSelectStudent'] = "Please select student";
$Lang['iPortfolio']['OtherReports']['PleaseSelectFile'] = "Please select file";
$Lang['iPortfolio']['OtherReports']['ZeroFileSize'] = "File cannot be empty";
$Lang['iPortfolio']['OtherReports']['FileSizeExceed'] = "File size cannot be larger than 100MB";
$Lang['iPortfolio']['OtherReports']['InvalidFileType'] = "File format must be in one of following: pdf, zip";
$Lang['iPortfolio']['OtherReports']['NonEmptyItems'] = '0|=|There\'s item in this report, please remove it first if you insist deleting the record.';
$Lang['iPortfolio']['Instruction_OtherReport'] = "For any student report generated outside iPortfolio, you can upload here.";

$ec_iPortfolio['heading']['report'] = "Reports";
$ec_iPortfolio['SLP']['MaximumHours'] = "Maximum Hours";
$ec_iPortfolio['SLP']['DefaultHours'] = "Default Hours";
$ec_iPortfolio['SLP']['PleaseEnterANumber'] = "Please Enter a Number";
$ec_iPortfolio['SLP']['ExceedingMaxumumHours'] = "Exceeding Maximum Hours";
$ec_iPortfolio['SLP']['CompulsoryFields'] = "Compulsory Fields";
$ec_iPortfolio['SLP']['DefaultApprover'] = "<span class=\"tabletextrequire\">@</span>Default Approver";
$ec_iPortfolio['SLP']['PleaseFillIn'] = "Please Fill in";
$ec_iPortfolio['Merge'] = "Merge";
$ec_iPortfolio['SLP']['ProgramMergeStatement1'] = "{{{ REPLACE }}} selected in previous page will be merge to this newly created program when submit!";
$ec_iPortfolio['SLP']['ProgramMergeStatement2'] = "All selected {{{ REPLACE }}} will be deleted after merge!";
$ec_iPortfolio['SLP']['InputByTeacher'] = "Input By Teacher";
$ec_iPortfolio['SLP']['ImportByTeacher'] = "Import By Teacher";
$ec_iPortfolio['SLP']['InputByStudent'] = "Input By Student";
$ec_iPortfolio['SLP']['EnrollmentTransfer'] = "Enrollment Transfer";
$ec_iPortfolio['SLP']['ComeFrom'] = "Come From";
$ec_iPortfolio['SLP']['PleaseSelect'] = "Please select ";

$ec_iPortfolio['export_slp_data'] = "Student Learning Profile Data Export";
$ec_iPortfolio['student_ole_manage'] = "Other Learning Record Management";
$ec_iPortfolio['student_report_setting'] = "Student Report Setting";
$ec_iPortfolio['student_report_printing'] = "Student Report Printing";
$ec_iPortfolio['assessment_stat_report'] = "Assessment Statistic Report";
$ec_iPortfolio['manage_templates'] = "Templates Management";
$ec_iPortfolio['prepare_cd_burning'] = "Prepare CD-ROM burning";
$ec_iPortfolio['learning_sharing'] = "Learning Portfolio";
$ec_iPortfolio['growth_scheme'] = "Growth & Development Scheme";
$ec_iPortfolio['view_alumni'] = "View Alumni";
$ec_iPortfolio['SchoolHeaderImage'] = "School Header Image";
$ec_iPortfolio['SchoolHeaderImageUpload'] = "Upload School Header Image";
$ec_iPortfolio['ShowSchoolHeaderImage'] = "Show School Header Image";
$ec_iPortfolio['SchoolHeaderImageSizeSuggest'] = "Suggestion of School Header Image Size";
$ec_iPortfolio['SchoolHeaderImageSizeActual'] = "Actual Size of School Header Image";

$ec_iPortfolio['Width'] = "Width";
$ec_iPortfolio['Height'] = "Height";
$ec_student_word['registration_no'] = "WebSAMSRegNo";

$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0] = "Date format must be YYYY-MM-DD";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][1] = "Leave it blank if the records are for current year";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][2] = "Leave it blank if the records are for current term";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][3] = "The number starts with '#' e.g: #123456";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][4] = "For those have more than one components, please sepearate by ';'. e.g. 1,4,6 Please fill as 1;4;6.";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][5] = "Click here to check Category Code";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][6] = "Click here to check Components of OLE Code";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][7] = "Please provide \"ClassName ClassNumber\" Or \"UserLogin\" Or \"WebSAMSRegNo\" for identifying student.";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][8] = "Click here to check Subcategory Code";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][9] = "Click here to check Inside/Outside Code";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][10] = "Click here to check Belong to SAS Code";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][11] = "Click here to check Allow Participation Reporting Code";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][12] = "Click here to check Applicable Form(s)";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][13] = "For those have more than forms, please sepearate by ';'. e.g. S.1,S.4,S.5 Please fill as S1,S4,S5.";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][14] = "Click here to check Allow Auto Approval Code";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][15] = "Click here to check Compulsory Fields Code";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][16] = "Please provide Approver's UserLogin";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][17] = " Mandatory field(s) if Column 'Allow Participation Reporting' is marked as 'Yes'";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][18] = "'Y' for yes; 'N' for no";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][19] = " The end date is required if the start date has been entered";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][20] = "'Y' for yes; 'N' for no";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][21] = "For those have more than fields, please sepearate by ';'. e.g. Hours, Role of Participation, Attachment / Evidence Please fill as 1;2;4.";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][22] = " Mandatory field(s) if Column 'Allow Auto Approval' is marked as 'No'";

$Lang['iPortfolio']['IMPORT']['ClassNameClassNumberInvalid'] = "Class Name and Class Number does not match";
$Lang['iPortfolio']['IMPORT']['ELECodeDuplicate'] = "OLE Component Code Duplicate";
$Lang['iPortfolio']['IMPORT']['ELECodeError'] = "OLE Component Code Error";
$Lang['iPortfolio']['IMPORT']['StudentLoginIDNotFind'] = "Student Login not Exist";
$Lang['iPortfolio']['IMPORT']['TeacherLoginIDNotFind'] = "Teacher Login not Exist";
$Lang['iPortfolio']['IMPORT']['WebSAMSNotFind'] = "Incorrect WebSAMS Registration Number";
$Lang['iPortfolio']['IMPORT']['CodeGuide'] = "Code Reference";

$Lang['iPortfolio']['OEA']['OEA_Name'] = "Other Experiences and Achievements";
$Lang['iPortfolio']['OEA']['OEA_Name_PreMap_Student'] = "Other Experiences and Achievements (Pre Map)";
$Lang['iPortfolio']['OEA']['OEA_ShortName'] = "OEA";
$Lang['iPortfolio']['OEA']['AdditionalInformation'] = "Additional Information";
$Lang['iPortfolio']['OEA']['PersonalAndGeneralAbility'] = "Personal & General Ability";
$Lang['iPortfolio']['OEA']['AcademicPerformance'] = "Academic Performance";
$Lang['iPortfolio']['OEA']['SupplementaryInformation'] = "Supplementary Information From Principal";
$Lang['iPortfolio']['OEA']['Convert'] = "Convert";
$Lang['iPortfolio']['OEA']['PreviousRecord'] = "Previous Record";
$Lang['iPortfolio']['OEA']['Program'] = "Program";
$Lang['iPortfolio']['OEA']['NextRecord'] = "Next Record";
$Lang['iPortfolio']['OEA']['ReSelectRecord'] = "Re-select Record";
$Lang['iPortfolio']['OEA']['SLP'] = "SLP";
$Lang['iPortfolio']['OEA']['SLPProgramName'] = "SLP Name";
$Lang['iPortfolio']['OEA']['OEAProgramName'] = "OEA Name";
$Lang['iPortfolio']['OEA']['NoOfUser']="The number of Students";
$Lang['iPortfolio']['OEA']['SortedBy']="Sorted By";
$Lang['iPortfolio']['OEA']['download_start']="Starting ...";
$Lang['iPortfolio']['OEA']['download_notes1']="Preparing OEA suggestions for the mapping template with ";
$Lang['iPortfolio']['OEA']['download_notes2']=" OLE records.";
$Lang['iPortfolio']['OEA']['download_time_needed_1']="About ";
$Lang['iPortfolio']['OEA']['download_time_needed_2']=" minute(s) is required for the remaining process. Please wait ...";

$Lang['iPortfolio']['SelfAccountArr']['exportArr']['RegNo'] = "Registration Number";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['StdName'] = "Student Name";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['ClassName'] = "Class Name";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['ClassNo'] = "Class Number";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['SelfAccPrimary'] = "Self-Account (Primary)";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['SelfAcc'] = "Self-Account";
$Lang['iPortfolio']['SelfAccount']['exportArr']['Export'] = "Export";
$Lang['iPortfolio']['SelfAccount']['exportArr']['Selected'] = "Selected";
$Lang['iPortfolio']['SelfAccount']['exportArr']['All'] = "All";
$Lang['iPortfolio']['SelfAccount']['exportArr']['Type'] = "Type";
$Lang['iPortfolio']['SelfAccount']['exportArr']['PlsSelectClass'] = "Please select a class";

$Lang['iPortfolio']['OEA']['SettingSubmitType'] = 'Item(s) submitted by students';
$Lang['iPortfolio']['OEA']['SettingForceWithRoleMapping'] = 'The role setting by student must be based on the system setting';
$Lang['iPortfolio']['OEA']['SettingNotAllowSubmit'] = 'Item(s) preset as not for submission';
$Lang['iPortfolio']['OEA']['SettingOEASrc'] = 'Item(s) selected from';
$Lang['iPortfolio']['OEA']['SettingPartSrc'] = 'Item(s) selected from';

$Lang['iPortfolio']['OEA']['jsWarningArr']['SLP_OEA_Mapping_Changed'] = "The change in the mapping will be applied to ALL linked OEA records for this SLP. Are you sure you want to change the mapping?";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Description_Greater_Than_The_Max'] = "The description exceeds <!--MaxWordCount--> characters. Please reduce the number of characters in the description.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Input_Title'] = "Please input Additional Info Title.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Input_Content'] = "Please input Additional Info Content.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Greater_Than_The_Max'] = "The Content exceeds <!--MaxWordCount--> characters. Please reduce the number of characters in the Content.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Greater_Than_The_Max_Confirm'] = "The Content exceeds <!--MaxWordCount--> words. Are you sure you want to save the Content?";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_CompleteRatingFirst'] = "Please click \"edit\" to complete the Additional Information for the student first.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Ability_SelectRating'] = "Please select a Rating for the Attribute.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Ability_CompleteRatingFirst'] = "Please click \"edit\" to complete the Attribute Rating for the student first.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Academic_SelectPercentile'] = "Please select a Percentile for the Subject.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Academic_SelectOverallRating'] = "Please select a Overall Rating for the Subject.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_Input_Title'] = "Please input Supplementary Info Title.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_Input_Content'] = "Please input Supplementary Info Content.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_CompleteRatingFirst'] = "Please click \"edit\" to complete the Supplementary Information for the student first.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['OLE_SelectProgramNature'] = "Please select a Program Nature.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['OLE_SelectMappingStatus'] = "Please select a Mapping Status.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Description'] = "Please input a Description.";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Sup_Info_Eng_Only'] = "According to the instruction of JUPAS, you can only input \\\"".$Lang['iPortfolio']['OEA']['SupplementaryInformation']."\\\" in English . Do you still want to continue?";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AppliedOeaAlready'] = "You have applied this OEA already. Are you sure you want to apply this OEA?";
$Lang['iPortfolio']['OEA']['jsWarningArr']['NotAllowToApplyOthers'] = "Student is not allowed to apply Category \"Others\". Please choose another OEA program.";

$Lang['iPortfolio']['OEA']['WarningArr']['NoSubjectSettings'] = "There are no Subject Settings. Please contact system administrator to complete the settings.";
$Lang['iPortfolio']['OEA']['WarningArr']['NoSubjectOrAcademicSettings'] = "There are no Subject Settings or no Academic Result for this student. Please contact system administrator to complete the settings or input the academic result data.";
$Lang['iPortfolio']['OEA']['WarningArr']['NoAcademicClassSettings'] = "There are no Classes applicable to the Academic Performance. Please contact system administrator to complete the settings.";
$Lang['iPortfolio']['OEA']['WarningArr']['NotAllPercentileMapped'] = "There are Percentile(s) not mapped to the Overall Rating. Please contact system administrator to complete the settings.";
$Lang['iPortfolio']['OEA']['WarningArr']['OverrideStudentOeaRecords'] = "You have selected to override the Student's OEA records. Student's OEA records will be changed if the OLE is mapped to a different OEA now. These OEA records will be permanently changed and cannot be recovered.";
$Lang['iPortfolio']['OEA']['WarningArr']['OverrideStudentOeaRecordsImport'] = "Press <font style=\"font-size:large\">Continue</font> to import.";
$Lang['iPortfolio']['OEA']['WarningArr']['NoApplicableFormSettings'] = "JUPAS function is not yet opened to any form. Please check form(s) who are to use the JUPAS function.";

$Lang['iPortfolio']['OEA']['JUPAS_Trial'] = "JUPAS (Trial)";
$Lang['iPortfolio']['OEA']['JUPAS'] = "JUPAS";
$Lang['iPortfolio']['OEA']['Title'] = "Activity Name";
$Lang['iPortfolio']['OEA']['OLE_Title'] = "Title (OLE)";
$Lang['iPortfolio']['OEA']['OeaCode'] = "OEA Code";
$Lang['iPortfolio']['OEA']['OeaName'] = "OEA Name";
$Lang['iPortfolio']['OEA']['OeaDescription'] = "OEA Description";
$Lang['iPortfolio']['OEA']['MappedOEAItem'] = "Mapped OEA Item";
$Lang['iPortfolio']['OEA']['Category'] = "Category";
$Lang['iPortfolio']['OEA']['Role'] = "Participation Mode";
$Lang['iPortfolio']['OEA']['Achievement'] = "Achievement";
$Lang['iPortfolio']['OEA']['Description'] = "Description";
$Lang['iPortfolio']['OEA']['RoleType'] = array("L"=>"Leader", "C"=>"Committee Member", "M"=>"Member / Participant");
$Lang['iPortfolio']['OEA']['AchievementType'] = array("G"=>"Champion / Gold Medal", "B"=>"1st Runner-up / 2nd Runner-up / Silver / Bronze Medal", "O"=>"Other Awards", "N"=>"No Award");
$Lang['iPortfolio']['OEA']['ParticipationNatureType'] = array("C"=>"By competition","N"=>"By nomination","P"=>"By participation");
$Lang['iPortfolio']['OEA']['OLE'] = "Other Learning Experience (OLE)";
$Lang['iPortfolio']['OEA']['OLE_Name2'] = "Other Learning Experience";
$Lang['iPortfolio']['OEA']['OEA_Name2'] = "Other Experiences and Achievements (OEA)";
$Lang['iPortfolio']['OEA']['Add_CountText_1'] = "Item(s) selected";
$Lang['iPortfolio']['OEA']['Add_CountText_2'] = "Item(s) saved";
$Lang['iPortfolio']['OEA']['ItemSaveSuccessfully'] = "1|=|Item saved successfully.";
$Lang['iPortfolio']['OEA']['ItemSaveUnsuccessfully'] = "0|=|Item saved unsuccessfully.";
$Lang['iPortfolio']['OEA']['AddiInfoSaveSuccessfully'] = "1|=|Additional Info saved successfully.";
$Lang['iPortfolio']['OEA']['AddiInfoSaveUnsuccessfully'] = "0|=|Additional Info saving failed.";
$Lang['iPortfolio']['OEA']['AbilitySaveSuccessfully'] = "1|=|Ability saved successfully.";
$Lang['iPortfolio']['OEA']['AbilitySaveUnsuccessfully'] = "0|=|Ability saving failed.";
$Lang['iPortfolio']['OEA']['AcademicSaveSuccessfully'] = "1|=|Academic saved successfully.";
$Lang['iPortfolio']['OEA']['AcademicSaveUnsuccessfully'] = "0|=|Academic saving failed.";
$Lang['iPortfolio']['OEA']['SuppInfoSaveSuccessfully'] = "1|=|Supplementary Info saved successfully.";
$Lang['iPortfolio']['OEA']['SuppInfoSaveUnsuccessfully'] = "0|=|Supplementary Info saving failed.";
$Lang['iPortfolio']['OEA']['SettingsSaveSuccessfully'] = "1|=|Settings saved successfully.";
$Lang['iPortfolio']['OEA']['SettingsSaveUnsuccessfully'] = "0|=|Settings saving failed.";
$Lang['iPortfolio']['OEA']['OeaNumExceedLimit'] = "0|=|Exceeded the limit of number of OEA";
$Lang['iPortfolio']['OEA']['SubmissionPeriodHasPast'] = "0|=|The submission period is past";
$Lang['iPortfolio']['OEA']['OEA_List'] = "OEA List";
$Lang['iPortfolio']['OEA']['OEA_CreateFromOLE'] = "Create from OLE record";
$Lang['iPortfolio']['OEA']['Edit'] = "Edit";
$Lang['iPortfolio']['OEA']['SaveAndNext'] = "Save & Next";
$Lang['iPortfolio']['OEA']['SaveAndFinish'] = "Save & Finish";
$Lang['iPortfolio']['OEA']['AllSettingsAreDisabled'] = "All OEA-related functions have been disabled. Please contact system administrator to enable the ralated functions.";
$Lang['iPortfolio']['OEA']['ViewAndApproveStudentJupasData'] = "View & Approve Student Jupas Info";
$Lang['iPortfolio']['OEA']['SuggestedItems'] = "Use System-suggested title";
$Lang['iPortfolio']['OEA']['SuggestedItemsCheck'] = "Check suggestions by system (based on OLE title)";
$Lang['iPortfolio']['OEA']['MatchItemCaption'] = "Similar title found:";
$Lang['iPortfolio']['OEA']['selectMapMethod'] = 'Choose one:';
$Lang['iPortfolio']['OEA']['MoreOptions'] = "More matching options";
$Lang['iPortfolio']['OEA']['AdvanceSearch'] = "Find an appropriate title manually";
$Lang['iPortfolio']['OEA']['Keyword'] = "Keyword";
$Lang['iPortfolio']['OEA']['ByCategory'] = "By ".$ec_iPortfolio['category'];
$Lang['iPortfolio']['OEA']['ByItem'] = "By ".$iPort["Item"];
$Lang['iPortfolio']['OEA']['ProgramCode'] = "Program Code";
$Lang['iPortfolio']['OEA']['ByKeyword'] = "By ".$Lang['iPortfolio']['OEA']['Keyword']." / ".$Lang['iPortfolio']['OEA']['ProgramCode'];
$Lang['iPortfolio']['OEA']['ItemSelected'] = "Item selected";
$Lang['iPortfolio']['OEA']['Approved'] = "Approved";
$Lang['iPortfolio']['OEA']['NotApproved'] = "Not Approved";
$Lang['iPortfolio']['OEA']['LastApproveOrRejectRemark'] = 'Last approved / rejected : <!--DaysAgo--> by <!--LastModifiedBy-->';

$Lang['iPortfolio']['OEA']['OeaItemApproveSuccessfully'] = "1|=|OEA Item approved successfully.";
$Lang['iPortfolio']['OEA']['OeaItemApproveUnsuccessfully'] = "0|=|OEA Item approval failed.";
$Lang['iPortfolio']['OEA']['OeaItemRejectSuccessfully'] = "1|=|OEA Item rejected successfully.";
$Lang['iPortfolio']['OEA']['OeaItemRejectUnsuccessfully'] = "0|=|OEA Item rejection failed.";
$Lang['iPortfolio']['OEA']['AddiInfoApproveSuccessfully'] = "1|=|Additional Info approved successfully.";
$Lang['iPortfolio']['OEA']['AddiInfoApproveUnsuccessfully'] = "0|=|Additional Info approval failed.";
$Lang['iPortfolio']['OEA']['AddiInfoRejectSuccessfully'] = "1|=|Additional Info rejected successfully.";
$Lang['iPortfolio']['OEA']['AddiInfoRejectUnsuccessfully'] = "0|=|Additional Info rejection failed.";
$Lang['iPortfolio']['OEA']['AbilityApproveSuccessfully'] = "1|=|Ability approved successfully.";
$Lang['iPortfolio']['OEA']['AbilityApproveUnsuccessfully'] = "0|=|Ability approval failed.";
$Lang['iPortfolio']['OEA']['AbilityRejectSuccessfully'] = "1|=|Ability rejected successfully.";
$Lang['iPortfolio']['OEA']['AbilityRejectUnsuccessfully'] = "0|=|Ability rejection failed.";
$Lang['iPortfolio']['OEA']['AcademicApproveSuccessfully'] = "1|=|Academic approved successfully.";
$Lang['iPortfolio']['OEA']['AcademicApproveUnsuccessfully'] = "0|=|Academic approval failed.";
$Lang['iPortfolio']['OEA']['AcademicRejectSuccessfully'] = "1|=|Academic rejected successfully.";
$Lang['iPortfolio']['OEA']['AcademicRejectUnsuccessfully'] = "0|=|Academic rejection failed.";
$Lang['iPortfolio']['OEA']['SuppInfoApproveSuccessfully'] = "1|=|Supplementary Info approved successfully.";
$Lang['iPortfolio']['OEA']['SuppInfoApproveUnsuccessfully'] = "0|=|Supplementary Info approval failed.";
$Lang['iPortfolio']['OEA']['SuppInfoRejectSuccessfully'] = "1|=|Supplementary Info rejected successfully.";
$Lang['iPortfolio']['OEA']['SuppInfoRejectUnsuccessfully'] = "0|=|Supplementary Info rejection failed.";
$Lang['iPortfolio']['OEA']['NoOfResults'] = "No. of result(s)";
$Lang['iPortfolio']['OEA']['Date'] = "Year of Participation";
$Lang['iPortfolio']['OEA']['Participation'] = "Participation";
$Lang['iPortfolio']['OEA']['OLE_Int'] = "Inside school";
$Lang['iPortfolio']['OEA']['OLE_Ext'] = "Outside school";
$Lang['iPortfolio']['OEA']['Part1'] = "Part I";
$Lang['iPortfolio']['OEA']['Part2'] = "Part II";
$Lang['iPortfolio']['OEA']['OEA_Int'] = "Attended as School activity";
$Lang['iPortfolio']['OEA']['OEA_Ext'] = "Attended as Non-School activity";
$Lang['iPortfolio']['OEA']['OEA_AwardBearing'] = "Award Bearing";
$Lang['iPortfolio']['OEA']['OEA_NotAwardBearing'] = "Not Award Bearing";
$Lang['iPortfolio']['OEA']['StartYear'] = "Start Year";
$Lang['iPortfolio']['OEA']['EndYear'] = "End Year";
$Lang['iPortfolio']['OEA']['ApprovalStatus'] = "Approval Status";
$Lang['iPortfolio']['OEA']['OnlyExportApprovedOEARemarks'] = "Only <font color='red'><b>APPROVED</b></font> OEA records will be included in the JUPAS csv.";
$Lang['iPortfolio']['OEA']['PleaseSelectOEAItem'] = "Please select at least one OEA item.";
$Lang['iPortfolio']['OEA']['RecordStatus_Pending'] = "Pending";
$Lang['iPortfolio']['OEA']['RecordStatus_Approved'] = "Approved";
$Lang['iPortfolio']['OEA']['RecordStatus_Rejected'] = "Rejected";
$Lang['iPortfolio']['OEA']['Remark'] = "Remark";
$Lang['iPortfolio']['OEA']['PreviousStudent'] = "Previous Student";
$Lang['iPortfolio']['OEA']['NextStudent'] = "Next Student";
$Lang['iPortfolio']['OEA']['NewFromOLE_Step1'] = "Select OLE item(s)";
$Lang['iPortfolio']['OEA']['NewFromOLE_Step2'] = "Input OEA information";
$Lang['iPortfolio']['OEA']['SubmissionPeriod'] = "Submission Period";
$Lang['iPortfolio']['OEA']['Content'] = "Content";
$Lang['iPortfolio']['OEA']['AddiInfoEditInstruction'] = "Please describe in approximately 300 words and not more than 2,000 characters (in either English or Chinese) as interest or experience that has been particularly meaningful to you, or has affected your personal growth and life goals.";
$Lang['iPortfolio']['OEA']['SuppInfoEditInstruction'] = "Please provide information on event(s) that may adversely affect your student's application (such as sickness, accident, family problem). Please note that the JUPAS Office / Institutions may ask the applicant to produce documentation as proof and support if considered appropriate.";
$Lang['iPortfolio']['OEA']['AllStudents'] = "All Students";
$Lang['iPortfolio']['OEA']['WaitingForApprovalStudents'] = "Waiting for Approval Students";
$Lang['iPortfolio']['OEA']['NoOfApproved'] = "No. of Approved";
$Lang['iPortfolio']['OEA']['TotalNoOfRecords'] = "Total No. of Records";
$Lang['iPortfolio']['OEA']['Checking'] = array(
											"OEA_Title"=>"OEA Title",
											"OEA_StartDate"=>"OEA Start Date",
											"OEA_EndDate"=>"OEA End Date",
											"OEA_Role"=>"OEA Role",
											"OEA_AwardBearing"=>"OEA AwardBearing",
											"OEA_ParticipationNature"=>"OEA Participation Nature",
											"OEA_Achievement"=>"OEA Achievement",
											"OEA_Participation"=>"OEA Participation"
												);
$Lang['iPortfolio']['OEA']['Mapping_No_OEA_Record_Is_Mapped'] = "OEA title has not been provided";
$Lang['iPortfolio']['OEA']['NoInfo'] = "No information";
$Lang['iPortfolio']['OEA']['Mapping_Others'] = "Others";
$Lang['iPortfolio']['OEA']['Mapping_Fail_To_Map_From_Above'] = "If fail to map from above";
$Lang['iPortfolio']['OEA']['Mapping_MappedItemName'] = "Item name mapped";
$Lang['iPortfolio']['OEA']['NoOfItemsSuggested'] = "No. of item(s) suggested";
$Lang['iPortfolio']['OEA']['SaveAndApprove'] = "Save & Approve";
$Lang['iPortfolio']['OEA']['OLEProgramMappingFile'] = "OLE Program Mapping";
$Lang['iPortfolio']['OEA']['OEAProgramInfoFile'] = "OEA Program Information";
$Lang['iPortfolio']['OEA']['AllOLEProgram'] = "All OLE Programs";
$Lang['iPortfolio']['OEA']['UnmappedOLEProgram'] = "Unmapped OLE Programs";
$Lang['iPortfolio']['OEA']['ProgramNature'] = "Program Nature";
$Lang['iPortfolio']['OEA']['AllProgramNature'] = "All Program Nature";
$Lang['iPortfolio']['OEA']['MappingStatus'] = "Mapping Status";
$Lang['iPortfolio']['OEA']['AllMappingStatus'] = "All Mapping Status";
$Lang['iPortfolio']['OEA']['ClickHereToDownloadMappingFile'] = "Click here to download mapping file";
$Lang['iPortfolio']['OEA']['ClickHereToDownloadOEAInfo'] = "Click here to download JUPAS OEA information";
$Lang['iPortfolio']['OEA']['Internal'] = "Internal";
$Lang['iPortfolio']['OEA']['External'] = "External";
$Lang['iPortfolio']['OEA']['Mapped'] = "Mapped";
$Lang['iPortfolio']['OEA']['NotMapped'] = "Not Mapped";
$Lang['iPortfolio']['OEA']['BackToOLEItemMappingList'] = "Back to OLE Item Mapping List";
$Lang['iPortfolio']['OEA']['ImportOtherMapping'] = "Import Other Mapping";
$Lang['iPortfolio']['OEA']['ImportMappingSuccess'] = "Mapping Import Success";
$Lang['iPortfolio']['OEA']['ImportMappingFailed'] = "Mapping Import Failed";
$Lang['iPortfolio']['OEA']['ImportOtherAutoMapTitleMsg'] = "If 「00000」is input as the code of OEA, it will match and be named as that in OLE automatically.";
$Lang['iPortfolio']['OEA']['ImportOtherWarningMsg'] = "When 'Others' (Code: 00000) is chosen, the student MUST fill in activity description. If this activity is reported as 'Part II - ".$Lang['iPortfolio']['OEA']['OEA_Ext']."', the student has to login JUPAS system and upload proof document as well.";
$Lang['iPortfolio']['OEA']['WordCount'] = "Word Count";
$Lang['iPortfolio']['OEA']['Word(s)'] = "word(s)";
$Lang['iPortfolio']['OEA']['CharacterCount'] = "Character Count";
$Lang['iPortfolio']['OEA']['SchoolCode'] = "School Code";
$Lang['iPortfolio']['OEA']['ApplicableForms'] = "Applicable Form(s)";
$Lang['iPortfolio']['OEA']['SchoolCodeEmpty'] = "School Code cannot be empty.";
$Lang['iPortfolio']['OEA']['ApplicableFormsEmpty'] = "Applicable Form(s) cannot be empty.";
$Lang['iPortfolio']['OEA']['ApplicationNumber'] = "Application Number";
$Lang['iPortfolio']['OEA']['OLEMappingRemarks'] = "System will set the OEA as Award Bearing automatically if the \"OEA Award Bearing\" is blank.";
$Lang['iPortfolio']['OEA']['ModulesInUse'] = "Modules in-use";
$Lang['iPortfolio']['OEA']['RepresentMappedFromOLE'] = "Representing the OEA is mapped from OLE";
$Lang['iPortfolio']['OEA']['SLPOrder'] = "SLP Order";
$Lang['iPortfolio']['OEA']['TargetForm'] = "Target Form";
$Lang['iPortfolio']['OEA']['TargetClass'] = "Target Class(es)";
$Lang['iPortfolio']['OEA']['TeacherMapping'] = "Teacher's Mapping";
$Lang['iPortfolio']['OEA']['SaveAndApprove'] = "Save and Approve";
$Lang['iPortfolio']['OEA']['DetailsFromStudent'] = "Details from student";
$Lang['iPortfolio']['OEA']['DetailsForOle'] = "Details for OLE";
$Lang['iPortfolio']['OEA']['ChineseCharacterRemarks'] = "One Chinese charater equals three English characters";
$Lang['iPortfolio']['OEA']['JupasWordCountRemarks'] = "If there is any inconsistency between iPortfolio and JUPAS, the word count of JUPAS shall prevail";

$Lang['iPortfolio']['OEA']['ModuleTitleArr']['OeaItem'] = $Lang['iPortfolio']['OEA']['OEA_ShortName'];
$Lang['iPortfolio']['OEA']['ModuleTitleArr']['AddiInfo'] = $Lang['iPortfolio']['OEA']['AdditionalInformation'];
$Lang['iPortfolio']['OEA']['ModuleTitleArr']['Ability'] = $Lang['iPortfolio']['OEA']['PersonalAndGeneralAbility'];
$Lang['iPortfolio']['OEA']['ModuleTitleArr']['Academic'] = $Lang['iPortfolio']['OEA']['AcademicPerformance'];
$Lang['iPortfolio']['OEA']['ModuleTitleArr']['SuppInfo'] = $Lang['iPortfolio']['OEA']['SupplementaryInformation'];

$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['AbilityToCommunicate'] = "Ability to communicate";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['AbilityToWorkWithOthers'] = "Ability to work with others";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['AnalyticalPower'] = "Analytical power";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Conduct'] = "Conduct";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Creativity'] = "Creativity";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['IndependenceOfMind'] = "Independence of mind";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Industriousness'] = "Industriousness";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Initiative'] = "Initiative";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Leadership'] = "Leadership";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Maturity'] = "Maturity";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Perseverance'] = "Perseverance";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['SenseOfResponsibility'] = "Sense of responsibility";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['OverallEvaluation'] = "Overall Evaluation";

$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['Excellent'] = "Excellent";
$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['Good'] = "Good";
$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['Average'] = "Average";
$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['BelowAverage'] = "Below Average";
$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['UnableToJudge'] = "Unable to Judge";

$Lang['iPortfolio']['OEA']['AbilityArr']['Instruction'] = "From your knowledge of the applicant and, where appropriate, in comparison with other S6 candidates in your school, please rate the applicant on the following attributes by ticking the appropriate boxes against such attributes below.";
$Lang['iPortfolio']['OEA']['New_From_OLE'] = "New from OLE";
$Lang['iPortfolio']['OEA']['New_Record'] = "New record";

$Lang['iPortfolio']['OEA']['oeaSetting'] = "OEA Settings";
$Lang['iPortfolio']['OEA']['itemMap'] = "OLE Item Mapping";
$Lang['iPortfolio']['OEA']['roleMap'] = "OLE Role Mapping";
$Lang['iPortfolio']['OEA']['appNoImport'] = "Import Student Details";
$Lang['iPortfolio']['OEA']['Self_Mapped'] = "Self-mapped";
$Lang['iPortfolio']['OEA']['MappedByTeacher'] = "Mapped by teacher";

$Lang['iPortfolio']['OEA']['academicSettings'] = "Academic Result";
$Lang['iPortfolio']['OEA']['AllowManualAdjustment'] = "Allow Manual Adjustment";
$Lang['iPortfolio']['OEA']['StudentList'] = "Student List";

$Lang['iPortfolio']['OEA']['AcademicArr']['Basic'] = "Basic";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileAndOverallRating'] = "Percentile & Overall Rating";
$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASApplicationForm'] = "JUPAS Application Form";
$Lang['iPortfolio']['OEA']['AcademicArr']['AcademicResultSource'] = "Academic Result Source";
$Lang['iPortfolio']['OEA']['AcademicArr']['DataSource'] = "Data Source";
$Lang['iPortfolio']['OEA']['AcademicArr']['SchoolYear'] = "School Year";
$Lang['iPortfolio']['OEA']['AcademicArr']['SchoolTerm'] = "School Term";
$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'] = "Percentile";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating'] = "Overall Rating";
$Lang['iPortfolio']['OEA']['AcademicArr']['MappingCriteria'] = "Mapping Criteria";
$Lang['iPortfolio']['OEA']['AcademicArr']['Score'] = "Score";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileLowerLimit(%)'] = "Percentile Lower Limit (%)";
$Lang['iPortfolio']['OEA']['AcademicArr']['Mark'] = "Mark";
$Lang['iPortfolio']['OEA']['AcademicArr']['Grade'] = "Grade";
$Lang['iPortfolio']['OEA']['AcademicArr']['GradeRemarks'] = "e.g. \"A\" or \"A, A-, B+, B\"";
$Lang['iPortfolio']['OEA']['AcademicArr']['PositionInForm'] = "Position in Form";
$Lang['iPortfolio']['OEA']['AcademicArr']['GenerateBasedOnSettings'] = "Generate based on Settings";
$Lang['iPortfolio']['OEA']['AcademicArr']['ApplyToJUPAS'] = "Apply to JUPAS";
$Lang['iPortfolio']['OEA']['AcademicArr']['NotApplyToJUPAS'] = "Not Apply to JUPAS";
$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCode'] = "JUPAS Subject Code";
$Lang['iPortfolio']['OEA']['AcademicArr']['NoSubjectApplyToJUPAS'] = "There are no Subjects to be applied to JUPAS.";
$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCodeEmpty'] = "JUPAS Subject Code cannot be empty.";
$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCodeDuplicated'] = "JUPAS Subject Code cannot be duplicated.";
$Lang['iPortfolio']['OEA']['AcademicArr']['ExportForJUPAS'] = "Export (for JUPAS)";
$Lang['iPortfolio']['OEA']['AcademicArr']['AcademicForFormSixOnly'] = "\"Academic Performance\" is for Form 6 students only. If you are viewing Form 6 students but failed to view the academic result, please contact system administrator or set the WebSAMS Code of the Form to \"S6\".";
$Lang['iPortfolio']['OEA']['AcademicArr']['AutoMappingForPercentileAndOverallRating'] = "Auto-mapping for Percentile and Overall Rating";
$Lang['iPortfolio']['OEA']['AcademicArr']['MappingSettings'] = "Mapping Settings";
$Lang['iPortfolio']['OEA']['AcademicArr']['GenerateAcademicPerformance'] = "Generate Academic Performance";
$Lang['iPortfolio']['OEA']['AcademicArr']['GenerateWarning'] = "The generation of the student's academic performance will reset ALL current academic performance data for the following Classes and the current academic performance data cannot be recovered. If confirm, press Generate.";
$Lang['iPortfolio']['OEA']['AcademicArr']['GenerateFormula'] = "System uses this formula to calculate the percentile (the result will be rounded up to integer to detemine the percentile) : (Student's Subject Form Ranking / Total No. of Form Student studied that Subject) x 100%";

$Lang['iPortfolio']['OEA']['AcademicArr']['ReturnMsgArr']['GenerateSuccess'] = "1|=|Academic Performance Generated Successfully.";
$Lang['iPortfolio']['OEA']['AcademicArr']['ReturnMsgArr']['GenerateFailed'] = "0|=|Academic Performance Generation Failed.";

$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][1] = "Top 10%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][2] = "11%-25%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][3] = "26%-50%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][4] = "51%-75%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][5] = "Bottom 25%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][6] = "Unable to Judge";


$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['A'] = "Excellent";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['B'] = "Very Good";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['C'] = "Good";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['D'] = "Average";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['E'] = "Below Average";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['U'] = "Unable to Judge";

$Lang['iPortfolio']['OEA']['roleMapArr']['RoleSelected']="Role Selected";
$Lang['iPortfolio']['OEA']['roleMapArr']['NoOfRole']="No. of role(s)";


$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramID'] = "OLE Program ID";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_AcademicYear'] = "OLE Academic Year";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_NumOfStudentJoined'] = "OLE No. of Students Joined";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramName'] = "OLE Program Name";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramNature'] = "OLE Program Nature";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_ProgramCode'] = "OEA Program Code";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_Suggestions'] = "OEA Suggestion (Program Code : Name) [Ref]";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_ProgramName'] = "OEA Program Name";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_AwardBearing'] = "OEA Award Bearing (Y / N)";

$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramID'] = "OLE 活動編號";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_AcademicYear'] = "OLE 活動年份";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_NumOfStudentJoined'] = "OLE 參加學生數目";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramName'] = "OLE 活動名稱";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramNature'] = "OLE 活動性質";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_ProgramCode'] = "OEA 活動代碼";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_Suggestions'] = "建議 OEA (活動代碼 : 名稱) [參考用途] ";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_ProgramName'] = "OEA 活動名稱";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_AwardBearing'] = "OEA 包含獎項 (Y / N)";

$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['AwardDontMatch'] = "OEA Award does not match";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLECodeDuplicated'] = "OLE ProgramID is duplicated in the selected file";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLEProgramIDNotExist'] = "OLE ProgramID does not exist";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLEProgramIDNotMatchedWithTitle'] = "OLE ProgramID is not matched with the Title";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OEACodeNotExist'] = "OEA Code does not exist";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardBearing'] = "Award Bearing should be \"Y\" or \"N\"";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardType'] = "Award Type should be \"G\" , \"B\",\"O\" or \"N\"";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardNature'] = "Award Nature should be \"C\" , \"N\" or \"D\"";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongParticipationMode'] = "Participation Mode should be \"L\" , \"C\" or \"M\"";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongOeaYear'] = "OEA Year should be YYYY";

$Lang['iPortfolio']['OLE']['NewStudent'] = "Assign Student";
$Lang['iPortfolio']['OLE']['UploadButton']="Upload attachments";
$Lang['iPortfolio']['OLE']['UploadAttachment']['UploadButton'] = "Upload";
$Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][] = "Select zip file";
$Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][] = "Confirm upload data";
$Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][] = "Uploaded result";
$Lang['iPortfolio']['OLE']['UploadAttachment']['File'] = "File";
$Lang['iPortfolio']['OLE']['UploadAttachment']['FileSample'] = "Sample File";
$Lang['iPortfolio']['OLE']['UploadAttachment']['DownloadSingleFileSample'] = "Sample of one attachment for each students";
$Lang['iPortfolio']['OLE']['UploadAttachment']['DownloadMultipleFileSample'] = "Sample of multiple attachments for each students";
$Lang['iPortfolio']['OLE']['UploadAttachment']['Remarks'] = "Remarks";
$Lang['iPortfolio']['OLE']['UploadAttachment']['Error'] = "Error Reason";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "Archiving in a zip file (.zip)";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "The zip file can include folder(s)";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "Sub-folders are not allowed in folder";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "Chinese name is not allowed in folders and files";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "Please use student class name and number (e.g. 1A_01),  or WebSAMS RegNo as the file/folder name";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['PleaseSelectZIP'] = "Please select ZIP(.zip) file";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['UnmappingName'] = "Cannot find Student";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['RepeatStudent'] = "Files already mapped to this student";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['FolderInside']  = "Folder should not have any subfolders";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['EmptyFolder'] = "Empty folder";
$Lang['iPortfolio']['OLE']['UploadAttachment']['UploadNumber'] = "File Attempted";
$Lang['iPortfolio']['OLE']['UploadAttachment']['UploadUncessfulNumber'] = "Upload Fail";


// Temp Lang
$Lang['iPortfolio']['OEA']['AcademicArr']['Overall'] = "Overall";
$Lang['iPortfolio']['OEA']['AcademicArr']['Term1'] = "Term 1";
$Lang['iPortfolio']['OEA']['AcademicArr']['Term2'] = "Term 2";
// End Temp Lang

$Lang['iPortfolio']['OEA']['GeneralSetting'] = "General Settings";
$Lang['iPortfolio']['OEA']['MaxNoOfOeaAllow'] = "Max number of record(s) allowed for each student";
$Lang['iPortfolio']['OEA']['MaxNoOfOeaAllow_StudentView'] = "Max number of OEA record(s) allowed";
$Lang['iPortfolio']['OEA']['SubmissionPeriodSetting'] = "Submission Period Setting";
$Lang['iPortfolio']['OEA']['SubmissionSettings'] = "Submission Settings";
$Lang['iPortfolio']['OEA']['AllowStudentNewOEA_NotFromOLE'] = "Allow students to create new OEA records";
$Lang['iPortfolio']['OEA']['AllowStudentSubmitOthersOea'] = "Allow students to apply Category \"Others\"";
$Lang['iPortfolio']['OEA']['ApprovalSetting'] = "Approval Settings";
$Lang['iPortfolio']['OEA']['AutoApproveOnOeaRecord'] = "Auto approve on OEA record";
$Lang['iPortfolio']['OEA']['NeedApproval'] = "Record needs approval";
$Lang['iPortfolio']['OEA']['Part1NeedApproval'] = "\"Part I - Attended as School activity\" record needs approval";
$Lang['iPortfolio']['OEA']['Part2NeedApproval'] = "\"Part II - Attended as non-school activity\" record needs approval";
$Lang['iPortfolio']['OEA']['Part2NeedApprovalRemarks'] = "\"Part II - Attended as Non-School activity\" record will be auto approved.";
$Lang['iPortfolio']['OEA']['jsWarning']['InvalidStartDate'] = "Invalid start date";
$Lang['iPortfolio']['OEA']['jsWarning']['InvalidEndDate'] = "Invalid end date";
$Lang['iPortfolio']['OEA']['jsWarning']['StartDateShouldBeEarlierThanEndDate'] = "Start date should be earlier than end date";
$Lang['iPortfolio']['OEA']['jsWarning']['StartTimeShouldBeEarlierThanEndTime'] = "Start time should be earlier than end time";
$Lang['iPortfolio']['OEA']['jsWarning']['StartToFillInDetails'] = "Now, you can proceed supplying additional information for OEA submission.";
$Lang['iPortfolio']['OEA']['jsWarning']['AllowSubmitedItem'] = "Please select at one '".$Lang['iPortfolio']['OEA']['SettingSubmitType']."'";
$Lang['iPortfolio']['OEA']['jsWarning']['AllowSubmitedOEAItem'] = "Please select at one item in '".$Lang['iPortfolio']['OEA']['AllowStudentNewOEA_NotFromOLE']."'";
$Lang['iPortfolio']['OEA']['jsWarning']['DeleteMapping'] = "Are you sure you want to delete this mapping?";
$Lang['iPortfolio']['OEA']['jsWarning']['ChooseOeaProgram'] = "Please choose a OEA Program.";
$Lang['iPortfolio']['OEA']['QuotaLeft'] = "Quota left";
$Lang['iPortfolio']['OEA']['NoOfRecordSelected'] = "No. of record(s) selected";
$Lang['iPortfolio']['OEA']['OeaQuotaIsNotEnough'] = "OEA quota is not enough";
$Lang['iPortfolio']['OEA']['OLE_Selection_Instruction'] = "These OLE records have been approved by your teacher. You can choose from those records to be declared as JUPAS OEA.";
$Lang['iPortfolio']['OEA']['NoSuggestedItemReturn'] = "Cannot find any matching item.";
$Lang['iPortfolio']['OEA']['UseOleName'] = "Use OLE title";
$Lang['iPortfolio']['OEA']['RecordSaved_SureWantToQuit'] = " record(s) saved. Are you sure you want to quit?";
$Lang['iPortfolio']['OEA']['SelfInput'] = "Self-input";
$Lang['iPortfolio']['OEA']['DisplaySelectedOeaItem'] = "Display selected OEA Item(s)";
$Lang['iPortfolio']['OEA']['ReferenceToSelfAccount'] = 'Write with reference to '.$iPort['menu']['self_account'];
$Lang['iPortfolio']['OEA']['PreMapping'] = 'Map to OEA';
$Lang['iPortfolio']['OEA']['PreMappingNotes'] = "for JUPAS";
$Lang['iPortfolio']['OEA']['PreMappingRemove'] = "remove OEA mapping";
$Lang['iPortfolio']['OEA']['MissingOLETitle'] = "Please input OLE title.";
$Lang['iPortfolio']['OEA']['defaultEmptyTextAreaMsg'] = "input message.";
$Lang['iPortfolio']['OEA']['PercentileEmptyMsg'] = "Reason has to be provided when percentile is set to \"Unable to Judge\"";
$Lang['iPortfolio']['OEA']['OverallRatingEmptyMsg'] = "Reason has to be provided when overall rating is set to \"Unable to Judge\"";
$Lang['iPortfolio']['OEA']['PercentileExceedMsg'] = "Remarks of percentile has exceeded the limit.";
$Lang['iPortfolio']['OEA']['OverallRatingExceedMsg'] = "Remarks of overall rating has exceeded the limit.";
$Lang['iPortfolio']['OEA']['Remarks'] = "Remarks";

$Lang['iPortfolio']['OEA']['DownloadMappingInstructions'] =
"<br /><b>Please read the following instructions carefully, and then you can download the mapping template file.</b><br />
<ul><li>Column F of the mapping file (which you attempt to download) contains OEA codes which are likely to match each OLE programme title.</li><br />
<li>Suggestions by the system are for teachers' reference. It may help teachers for the mappings but may not be always accurate.</li><br />
<li>To map with OEA codes, review each programme/row:</li>
<ul><li>If one of the suggested codes in column F matches the OLE programme, copy that code to column G and fill in column I.</li>
	<li>If none of the suggested codes matches, search the official JUPAS OEA activity list for a proper code and fill in columns G and I.</li>
</ul><br />
<li>Delete the entire row if you do not need to map a particular OLE programme.</li><br />
<li><font color = \"red\">It takes time to generate the mapping template file. Please be patient.</font></li>
</ul>";


$Lang['iPortfolio']['OEA']['Disclaimer'] = "Disclaimer";
$Lang['iPortfolio']['OEA']['OLE_OLE_Mapping_Warning'] = "The last updated date of the OEA list in the system is 2011-09-09. Please login the JUPAS admission system, then download the OEA list to complete the matching.";
//$Lang['iPortfolio']['OEA']['Disclaimer_TeacherView'] = "In view of the possible changes of JUPAS data format and other requirements and that such changes, if any, are beyond the knowing and control of eClass, we do not guarantee data input at this trial period can be usable in the real run. We recommend schools to test using a small set of data.";
$Lang['iPortfolio']['OEA']['Disclaimer_TeacherView'] = "JUPAS may update the OEA list or file format which is uncontrollable by our Company.";
//$Lang['iPortfolio']['OEA']['Disclaimer_StudentView'] = "In view of the possible changes of JUPAS data format and other requirements and that such changes, if any, are beyond the knowing and control of eClass, we do not guarantee data input at this trial period can be usable in the real run. You may have to re-submit information if such a situation appears.";
$Lang['iPortfolio']['OEA']['Disclaimer_StudentView'] = "JUPAS may update the OEA list or file format which is uncontrollable by our Company.";
$Lang['iPortfolio']['OEA']['discussForumCaption'] = "JUPAS Forum";
$Lang['iPortfolio']['OEA']['discussForumMessage'] = "If teachers have any latest news or opinions and would like to share, please <a href =\"index.php?task=linkForum\" target=\"_blank\" class=\"tablelink\"> [click here] </a>to enter our forum to share and discuss with other teachers using eClass.";
$Lang['iPortfolio']['OEA']['OverrideStudentMappedRecords'] = "Override students mapped OEA records (override the OEA Code and Activity Name only)";
$Lang['iPortfolio']['OEA']['NotApplicableToDelete'] = "not applicable to delete";

$Lang['iPortfolio']['OEA']['ReportArr']['DisplayInOeaFormat'] = "Display in OEA Format";
$Lang['iPortfolio']['OEA']['ReportArr']['DisplayInOleFormat'] = "Display in OLE Format";
$Lang['iPortfolio']['OEA']['ReportArr']['StudentOeaReport'] = "Student OEA Report";
$Lang['iPortfolio']['OEA']['ReportArr']['StudentOea'] = "Student OEA";
$Lang['iPortfolio']['OEA']['ReportArr']['IncludeOleData'] = "Include OLE data";
$Lang['iPortfolio']['OEA']['ReportArr']['CustReportTitleArr']['LaSalle_StudentMappedOeaOleInfoReport'] = $Lang['iPortfolio']['OEA']['ReportArr']['DisplayInOleFormat'];

$Lang['iPortfolio']['OEA']['FAQCaption'] = "FAQ";
$Lang['iPortfolio']['OEA']['FAQMessage'] = "If there is any enquiry, please see <a href = \"http://support.broadlearning.com/doc/help/faq/\" target=\"_blank\">FAQ <img src=\"/images/2009a/btn_FAQ.png\" border=\"0\"></a>first.";
$Lang['iPortfolio']['OEA']['DownloadTemplateInstruction'] = "Instruction of Mapping Template Download";
$Lang['iPortfolio']['OEA']['DownloadTemplateInstructionConfirm'] = "I understand and want to download it.";
$Lang['iPortfolio']['OEA']['ViewPreMap'] = "View OEA Student Pre-MapItem";

$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'] = "will not be used.";
$Lang['iPortfolio']['JUPAS']['and'] = "and";
$Lang['iPortfolio']['JUPAS']['RemarksOEA'] = "According to JUPAS policy, student has to fill in OEA and Additional Information in JUPAS system directly.  Thus OEA and Additional Information exported from eClass may not be imported into JUPAS system.<br/>Moreover, OEA program category/code may be updated in JUPAS system, thus DO NOT ask student to perform OEA mapping in eClass.";
$Lang['iPortfolio']['JUPAS']['RemarksAbility'] = "Personal & General Ability of each student will be exported on one row respectively.";
$Lang['iPortfolio']['JUPAS']['RemarksAcademic'] = "Pencentile and Overall Rating will be exported separately.";

$Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_mms']['OLEReport'] = "EXTRA-CURRICULAR ACTIVITIES AND AWARDS REPORT";
$Lang['iPortfolio']['Cust_cactm']['OLEReport'] = "Record of Other Learning Experiences (Report by school)";
$Lang['iPortfolio']['Cust_cactm']['OLEReport2'] = "Record of Other Learning Experiences (Report by student)";
$Lang['iPortfolio']['Cust_ghslp']['StudentLearningProfile'] = "Student Learning Profile";
$Lang['iPortfolio']['Cust_hktlc']['StudentLearningProfileReport'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_kc']['StudentLearningProfileReport'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_stmc']['StudentLearningProfileReport'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_stcec']['StudentLearningProfileReport'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_htc']['StudentLearningProfileReport'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_lskc']['StudentLearningProfileReport'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_yttmc']['StudentLearningProfileReport'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_toss']['StudentLearningProfileReport'] = "Student Learning Profile Report";
$Lang['iPortfolio']['Cust_ucc']['SLP_Report_SchoolName'] = '&#28377;基書院<br />UNITED CHRISTIAN COLLEGE';
$Lang['iPortfolio']['Cust_ucc']['SLP_Report_ReportName'] = '學生學習概覽<br />STUDENT LEARNING PROFILE';
$Lang['iPortfolio']['Cust_ucc']['SLP_Report_Signature'] = 'Signature';

$Lang['iPortfolio']['Cust_uccke']['SLP_Report_SchoolName'] = '&#28377;基書院(東九龍)<br />United Christian College (Kowloon East) ';
$Lang['iPortfolio']['Cust_uccke']['SLP_Report_ReportName'] = '學生學習概覽<br />STUDENT LEARNING PROFILE';
$Lang['iPortfolio']['Cust_sptss']['report1'] = '學生功過記錄明細';
$Lang['iPortfolio']['Cust_sptss']['report2'] = '學生生命素質涵養及功過記錄明細';
$Lang['iPortfolio']['Cust_sptss']['report3'] = '學生全人發展記錄表';
//$Lang['iPortfolio']['Cust_sptss']['report4'] = '歷年生命素養涵養記錄';
$Lang['iPortfolio']['Cust_sptss']['report4'] = '歷年生命素質涵養記錄';
$Lang['iPortfolio']['Cust_sptss']['report5'] = '歷年全人發展記錄表';
$Lang['iPortfolio']['Cust_sptss']['studentCharacter'] = '學生生命素質涵養';


$Lang['iPortfolio']['Cust_nlp']['OLECurrentYear'] = 'Student Profile Report (Current Year)';
$Lang['iPortfolio']['Cust_nlp']['OLEWholeYear'] = 'Student Profile Report (Whole Year)';

$Lang['iPortfolio']['SRP']['StudentReadingProfile'] = "Student Reading Profile";
$Lang['iPortfolio']['SRP']['DateOfIzzue'] = "Date of Izzue";
$Lang['iPortfolio']['SRP']['StudentParticulars'] = "Student Particulars";
$Lang['iPortfolio']['SRP']['StudentName'] = "Student Name";
$Lang['iPortfolio']['SRP']['DateOfBirth'] = "DOB";
$Lang['iPortfolio']['SRP']['SchoolName'] = "Name of Educational Institution";
$Lang['iPortfolio']['SRP']['AdmissionDate'] = "Admission Date";
$Lang['iPortfolio']['SRP']['SchoolAddress'] = "School Address";
$Lang['iPortfolio']['SRP']['SchoolPhone'] = "School Phone";
$Lang['iPortfolio']['SRP']['HKID'] = "HKID";
$Lang['iPortfolio']['SRP']['Gender'] = "Gender";
$Lang['iPortfolio']['SRP']['SchoolCode'] = "School Code";
$Lang['iPortfolio']['SRP']['StudentID'] = "Student ID";

$Lang['iPortfolio']['SRP']['Item'] = "Item";
$Lang['iPortfolio']['SRP']['Title'] = "Title";
$Lang['iPortfolio']['SRP']['Language'] = "Language";
$Lang['iPortfolio']['SRP']['Author'] = "Author";
$Lang['iPortfolio']['SRP']['Publisher'] = "Publisher";
$Lang['iPortfolio']['SRP']['Categories'] = "Categories";
$Lang['iPortfolio']['SRP']['PeriodOfReading'] = "Period of Reading";
$Lang['iPortfolio']['SRP']['StartingDate'] = "Starting date";
$Lang['iPortfolio']['SRP']['FinishingDate'] = "Finishing date";
$Lang['iPortfolio']['SRP']['ReadingHours'] = "Reading Hours";
$Lang['iPortfolio']['SRP']['BookReport'] = "Book Report";
$Lang['iPortfolio']['SRP']['YN'] = "(Y/N)";

$Lang['iPortfolio']['SRP']['LanguageType'][1] = "Chinese";
$Lang['iPortfolio']['SRP']['LanguageType'][2] = "English";
$Lang['iPortfolio']['SRP']['Uncategorized'] = "Uncategorized";

$Lang['iPortfolio']['SmallGrade'] = "Subdivided grade";
$Lang['iPortfolio']['SmallGradeDetails'] = "Divided by 9+ 9 8 7 6 5 4 3 2 1 1-";
$Lang['iPortfolio']['Avg'] = 'Average';
$Lang['iPortfolio']["Score"] = "Mark";
$Lang['iPortfolio']["LevelAvageScore"] = "Avg. Score of whole level";
$Lang['iPortfolio']["LevelAvageSD"] = "SD of whole level.";
$Lang['iPortfolio']["StandardScore"] = "Z-score";
$Lang['iPortfolio']["NewStandardScore"] = "New Z-score";
$Lang['iPortfolio']["StandardScoreDiff"] = "Z-score Diff.";
$Lang['iPortfolio']["MarkDifference%"] = "Mark Diff.";
$Lang['iPortfolio']["StandardScoreDifference"] = "Z-score Diff.";
$Lang['iPortfolio']["MarkDifferenceHTML"] = "Mark<br />Diff.";
$Lang['iPortfolio']["StandardScoreDifferenceHTML"] = "Z-score<br />Diff.";
$Lang['iPortfolio']["FormPosition"] = "OMF";
$Lang['iPortfolio']["FormPositionDifference"] = "OMF Diff.";
$Lang['iPortfolio']["increaseRank"] = "Increase Ranking";
$Lang['iPortfolio']["oldClass"] = "Old Class";

$Lang['iPortfolio']['OLE']['SAS'] = "Belong to SAS";
$Lang['iPortfolio']['OLE']['SAS_ALL'] = "ALL (Belong to SAS or Not)";
$Lang['iPortfolio']['OLE']['SAS_NOT'] = "Not belong to SAS";
$Lang['iPortfolio']['OLE']['SAS_with_link'] = "Belong to <a href=\"http://www2.hkugac.edu.hk/en_us/ela/sas.html\" target=\"SASwindow\"><font color=\"blue\">SAS</font></a>";
$Lang['iPortfolio']['OLE']['In_Outside_School'] = "Inside / Outside School";
$Lang['iPortfolio']['OLE']['Inside_School'] = "Inside school";
$Lang['iPortfolio']['OLE']['Outside_School'] = "Outside school";
$Lang['iPortfolio']['OLE']['DisplayIneRC'] = "Display In eRC";
$Lang['iPortfolio']['OLE']['HideIneRC'] = "Hide In eRC";

$Lang['iPortfolio']['SLP']['ImportRemarks'] = "Import template has been updated on 2011-06-29. Please ensure you have used the latest template.";

# Dynamic Report
$Lang['iPortfolio']['SLP']['DynamicReport']['GeneratedBy'] = "Generated By";
$Lang['iPortfolio']['SLP']['DynamicReport']['GeneratedDate'] = "Generated Date";
$Lang['iPortfolio']['SLP']['DynamicReport']['Report'] = "Report";

$Lang['iPortfolio']['VolunteerArr']['Volunteer'] = "Volunteer";
$Lang['iPortfolio']['VolunteerArr']['VolunteerEvent'] = "Volunteer Event";
$Lang['iPortfolio']['VolunteerArr']['EventCode'] = "Event Code";
$Lang['iPortfolio']['VolunteerArr']['EventName'] = "Event Name";
$Lang['iPortfolio']['VolunteerArr']['PartnerOrganization'] = "Partner Organization";
$Lang['iPortfolio']['VolunteerArr']['EventRegistration'] = "Event Registration";
$Lang['iPortfolio']['VolunteerArr']['AppliedEvent'] = "Applied Event";
$Lang['iPortfolio']['VolunteerArr']['Applied'] = "Applied";
$Lang['iPortfolio']['VolunteerArr']['FillInDetails'] = "Fill in details";
$Lang['iPortfolio']['VolunteerArr']['Hours'] = "Hours";
$Lang['iPortfolio']['VolunteerArr']['HourUnit'] = "Hour(s)";
$Lang['iPortfolio']['VolunteerArr']['Role'] = "Role";
$Lang['iPortfolio']['VolunteerArr']['Awards/Certifications/Achievements'] = "Awards / Certifications / Achievements";
$Lang['iPortfolio']['VolunteerArr']['TeacherPIC'] = "Teacher PIC";
$Lang['iPortfolio']['VolunteerArr']['Status'] = "Status";
$Lang['iPortfolio']['VolunteerArr']['AllStatus'] = "All Status";
$Lang['iPortfolio']['VolunteerArr']['AllProgram'] = "All Programs";
$Lang['iPortfolio']['VolunteerArr']['MyPICProgram'] = "My PIC Programs";

$Lang['iPortfolio']['VolunteerArr']['jsWarningArr']['SelectEvent'] = "Please select an Event.";

$Lang['iPortfolio']['OLE']['Information'] = "OLE Information";
$Lang['iPortfolio']['OLE']['OleTitle'] = "OLE Title";
$Lang['iPortfolio']['OLE']['OleNature'] = "OLE Type";
$Lang['iPortfolio']['OLE']['OleAcademicYear'] = "OLE School Year";
$Lang['iPortfolio']['OLE']['OleTerm'] = "OLE School Term";
$Lang['iPortfolio']['OLE']['OleStartDate'] = "OLE Start Date";
$Lang['iPortfolio']['OLE']['OleEndDate'] = "OLE End Date";
$Lang['iPortfolio']['OLE']['OleRole'] = "OLE Role";
$Lang['iPortfolio']['OLE']['OleCategory'] = "OLE Category";
$Lang['iPortfolio']['OLE']['OleComponentOfOle'] = "OLE Component of OLE";
$Lang['iPortfolio']['OLE']['OleAwards/Certifications/Achievements'] = "OLE Awards / Certifications / Achievements";
$Lang['iPortfolio']['OLE']['MergeProgramMessageBoxTitle'] = 'Selected Program';
$Lang['iPortfolio']['OLE']['TeacherInCharge'] = 'Teacher-in-charge';
$Lang['iPortfolio']['OLE']['ProgramCreator'] = 'Program Creator';
$Lang['iPortfolio']['OLE']['AddCertificationFile'] = 'Add certification file';
$Lang['iPortfolio']['OLE']['NewOLE'] = 'New OLE';
$Lang['iPortfolio']['OLE']['mandatory'] = '<span class="alertText"> * </span>Mandatory field(s)';
$Lang['iPortfolio']['OLE']['JoinEndDate'] = 'Join end date';
$Lang['iPortfolio']['OLE']['SchoolPresetActivities'] = 'School Preset Activities';
$Lang['iPortfolio']['OLE']['Records'] = 'Record(s)';
$Lang['iPortfolio']['OLE']['PendingApprovals'] = 'Pending approval(s)';
$Lang['iPortfolio']['OLE']['Approvals'] = 'Approval(s)';

$Lang['iPortfolio']['OLE_Outside']['Information'] = "Information of Performance / Awards and Key Participation Outside School";
$Lang['iPortfolio']['OLE_OEA']['Description'] = "To be input by student if necessary.";
$Lang['iPortfolio']['OLE_OEA']['Nature'] = "Based on OEA basic settings.";
$Lang['iPortfolio']['OLE_OEA']['Role'] = "Student roles will be mapped according to role mapping settings if set.";
$Lang['iPortfolio']['OLE_OEA']['Date'] = "System will choose for student according to start and end date.";

$Lang['iPortfolio']['SLP_Setting']['CompulsoryRemarks'] = 'This setting only apply to the page for student submission';
$Lang['iPortfolio']['SLP_REPORT']['AllowForStudent'] = 'Allow student to print \''.$iPort['menu']['slp'] .'\' report';
$Lang['iPortfolio']['SLP_REPORT']['AllowForStudentToPrint'] = 'Allow student to print:';
$Lang['iPortfolio']['SLP_REPORT']['AllowForStudentToPrintYesNo'] = 'Yes';
$Lang['iPortfolio']['SLP_REPORT']['SchoolHeaderImagePrint'] = 'School Header Image Print Setting';
$Lang['iPortfolio']['SLP_REPORT']['SchoolHeaderImageDetail'] = 'School Header Image Detail';

$Lang['iPortfolio']['plms_SLP_REPORT']['NumberOfOLE'] = 'Number of OLE records';
$Lang['iPortfolio']['plms_SLP_REPORT']['SelectOLERecord'] = 'Period for student to select records';

$Lang['iPortfolio']['Student']['Report']['PlsSelectAcademicYear'] = 'Please select a School Year.';
$Lang['iPortfolio']['OLE']['ViewModeReason'] = "Record cannot be edited due to one of the following reasons:<br>1. Submission period expired, or <br>2. Record submitted by teacher, or<br>3. Record approved by teacher already.";

$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_valid'] = 'rows of valid data can be saved to system now.';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_invalid'] = 'invalid data is found';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_result_success'] = 'record(s) has/have been imported to system successfully.';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_result_failed'] = 'Failed to import!';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_removal'] = 'Are you sure you want to remove all the records of semester [TERM] of [YEAR]?';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Import_format'] = '
Column 1 : Student Name
Column 2 : *RegNo
Column 3 : *Year
Column 4 : *Term
Column 5 : *Subject Code
Column 6 : Score';

$Lang['iPortfolio']['twghczm_pesonalcharacter'] = "Personal Character And Remarks";
$Lang['iPortfolio']['twghczm_csv_upload'] = "Please upload the data of personal character for students every year. ";
$Lang['iPortfolio']['twghwfns_pesonalability'] = "Personal and General Abilities";
$Lang['iPortfolio']['twghwfns_csv_upload'] = "Please upload the data of personal and general abilities for students every year. ";
$Lang['iPortfolio']['fywss_csv_upload'] = "Please upload the data of personal attributes for all students every year. ";
$Lang['iPortfolio']['academic_performance_csv_upload'] = "Please upload the data of outstanding academic performance for students every year in order to generate SLP (Paulinian Award Scheme).";

$Lang['iPortfolio']['cat_name_eng'] = "Category Name (Eng)";
$Lang['iPortfolio']['sub_cat_name_eng'] = "SubCategory Name (Eng)";
$Lang['iPortfolio']['program_name'] = "Programme Name (Eng)";
$Lang['iPortfolio']['old_compo'] = "OLE Component(s)";

$Lang['iPortfolio']['studentGroupRemark_KIS'] = "To update the Group and Group Members for current year, please access \"Admin Console > Intranet > Account Mgmt > Student Promotion System > Promote iPortfolio Students to New Classes\".";

# Ng Wah SAS Cust
$Lang['iPortfolio']['NgWah_SAS'] = "Student Award Scheme";
$Lang['iPortfolio']['NgWah_SAS_Report'] = "Student Award Scheme Report";
$Lang['iPortfolio']['NgWah_SAS_Settings']['Category'] = "SAS Category";
$Lang['iPortfolio']['NgWah_SAS_Settings']['Credit_pt'] = "SAS Credit Point";
$Lang['iPortfolio']['NgWah_SAS_Settings']['Remarks'] = "<span class='tabletextrequire'>*</span> To apply the SAS Credit Point in Programme to students, please empty the Column of SAS Credit Point.";
$Lang['iPortfolio']['NgWah_SAS_Category']['Post'] = "Elected/Appointed Post";
$Lang['iPortfolio']['NgWah_SAS_Category']['Achievement'] = "Achievement";
$Lang['iPortfolio']['NgWah_SAS_Category']['Internal_Performance'] = "Outstanding Performances (Inside School)";
$Lang['iPortfolio']['NgWah_SAS_Category']['External_Performance'] = "Outstanding Performances (Outside School)";
$Lang['iPortfolio']['NgWah_SAS_Category']['Service'] = "Services";
$Lang['iPortfolio']['NgWah_SAS_Category']['Others'] = "Other Learning Activities";
$Lang['iPortfolio']['SAS_Settings']['Category_Credit_Point'] = "Category & Max. Credit Point";
$Lang['iPortfolio']['SAS_Settings']['Category'] = "Category";
$Lang['iPortfolio']['SAS_Settings']['Credit_pt'] = "Credit Point";
$Lang['iPortfolio']['SAS_Settings']['Min_Credit_pt'] = "Minimum Credit Point";
$Lang['iPortfolio']['SAS_Settings']['Max_Credit_pt'] = "Maximum Credit Point";
$Lang['iPortfolio']['SAS_Settings']['Post'] = "Elected/Appointed Post";
$Lang['iPortfolio']['SAS_Settings']['Achievement'] = "Achievement";
$Lang['iPortfolio']['SAS_Settings']['Internal_Performance'] = "Outstanding Performances (Inside School)";
$Lang['iPortfolio']['SAS_Settings']['External_Performance'] = "Outstanding Performances (Outside School)";
$Lang['iPortfolio']['SAS_Settings']['Service'] = "Services";
$Lang['iPortfolio']['SAS_Settings']['Award'] = "Award";
$Lang['iPortfolio']['SAS_Settings']['Award_Ch'] = "Award (Chinese)";
$Lang['iPortfolio']['SAS_Settings']['Award_En'] = "Award (English)";
$Lang['iPortfolio']['SAS_Settings']['Gold'] = "Gold Award";
$Lang['iPortfolio']['SAS_Settings']['Silver'] = "Silver Award";
$Lang['iPortfolio']['SAS_Settings']['Bronze'] = "Bronze Award";
$Lang['iPortfolio']['SAS_Settings']['Release_Student'] = "Release to Students";
$Lang['iPortfolio']['SAS_Settings']['JS_MinCreditLargerThanMaxCredit'] = "Minimum Credit Point cannot be larger than the Maximum Credit Point.";
$Lang['iPortfolio']['SAS_Settings']['JS_CreditOutOfRange'] = "The range of credit point cannot be overlapped.";
$Lang['iPortfolio']['DownloadDisk']['Instruction'] = 'For classes or students are disabled or missing, please go "Disk Preparation" prepare the files.';

$Lang['iPortfolio']['Percentile']['Setting'] = 'Percentile Setting';
$Lang['iPortfolio']['Percentile']['PercentileRank'] = 'Percentile Rank';
$Lang['iPortfolio']['Percentile']['FirstPercentGroup'] = 'First Group';
$Lang['iPortfolio']['Percentile']['NextPercentGroup'] = 'Next Group';
$Lang['iPortfolio']['Percentile']['alert']['PleaseInputPercentile'] = 'Please input percentile';
$Lang['iPortfolio']['Percentile']['alert']['PercentileSumNotEqual100'] = 'The sum of the percentile must be 100%';
$Lang['iPortfolio']['Percentile']['alert']['PleaseInputTitle'] = 'Please input title';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['SchoolTerm'] = 'STA Duration';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ClassLevel'] = 'Class Level';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ClassName'] = 'Class Name';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['WebSAMSRegNo'] = 'Registration Number';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StudentNameEng'] = 'Student Name (English)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StudentNameChi'] = 'Student Name (Chinese)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Type'] = 'Type';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Activity'] = 'Activity (Chinese)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Role'] = 'Post (Chinese)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Performance'] = 'Performance (Chinese)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Successful'] = ' record import successful';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ANPType'] = 'ANP Type';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ANPDate'] = 'ANP Date';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Description'] = 'Description';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['CodeDescription'] = 'CodeDescription';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL1'] = 'LVL1';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL2'] = 'LVL2';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL3'] = 'LVL3';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL4'] = 'LVL4';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StaffCode'] = 'Staff Code';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['TeacherNameEng'] = 'Teacher Name (English)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['TeacherNameChi'] = 'Teacher Name (Chinese)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['AcademicGrade'] = 'Academic Grade';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ConductGrade'] = 'Conduct Grade';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['CommentChi'] = 'Comment (Chinese)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['CommentEng'] = 'Comment (English)';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['AcademicYearNotFound'] = 'School Year not found';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['AcademicYearTermNotFound'] = 'STA Duration not found';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['StudentNotFound'] = 'Student not found';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongType'] = 'Empty type or wrong';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyActivity'] = 'Empty activity';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyPerformance'] = 'Empty performance';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['TeacherNotFound'] = 'Teacher not found';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongANPType'] = 'Empty ANP type or wrong';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongANPDate'] = 'Empty ANP date or wrong format';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongLVL'] = 'Empty LVL or not a number';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyDescription'] = 'Empty description or code description';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['ANPDateOutofRange'] = 'ANP date is out of the academic term';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyComment'] = 'Empty academic grade or conduct grade or comment';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WebSAMSRegNoNotFound'] = 'WebSAMSRegNo is not set for this student';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportColumns']['ANP'] = array(
"<font color = red>@</font>WebSAMSRegNo",
"<font color = red>*</font>School Year (Year format must be yyyy or yyyy-YYYY or yy/YY)",
"School Session (Period)",
"Class Level",
"<font color = red>@</font>Class",
"<font color = red>@</font>Class No",
"Student English Name",
"Student Chinese Name",
"<font color = red>*</font>ANP Type (Either A or P. A - Award, P - Punishment)",
"<font color = red>*</font>ANP Date (Date format must be yyyy-mm-dd or dd/mm/yyyy or d/m/yyyy. If you use Excel to edit, please change the date format in Format Cells)",
"<font color = red>#^</font>Description",
"Event Code",
"Code Description in English",
"<font color = red>#^</font>Code Description in Chinese",
"<font color = red>^</font>LVL1 (Number of Merit / Demerit depends on ANP Type)",
"<font color = red>^</font>LVL2 (Number of Minor Merit / Minor Demerit depends on ANP Type)",
"<font color = red>^</font>LVL3 (Number of Major Merit / Major Demerit depends on ANP Type)",
"<font color = red>^</font>LVL4 (Number of Super Merit / Super Demerit depends on ANP Type)",
"LVL5 - Not Used",
"Conduct Mark - Not Used",
"<font color = red>+^</font>Staff Code",
"<font color = red>+</font>Teacher English Name",
"<font color = red>+</font>Teacher Chinese Name",
"Print Indicator",
"Action Taken - Not Used",
"Action Taken Date From - Not Used",
"Action Taken Date To - Not Used",
"Class Detention Indicator",
"Report Card Print Seq",
"Award Category - Not Used",
"Award From - Not Used",
"SLP Remarks - Not Used",
"SLP Read Indicator",
"Antecedence - Not Used",
"Addendum - Not Used",
"Remarks - Not Used",
"Other Language - Not Used",
"Other Antecedence - Not Used",
"Other Addendum - Not Used",
"Other Description - Not Used"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportRemarks']['ANP'] = array(
"<font color = red>*</font>Mandatory field(s)",
"<font color = red>@</font>must input either WebSAMSRegNo or (Class + Class No)",
"<font color = red>+</font>must input at least one of these fields: Staff Code, Teacher English Name, Teacher Chinese Name",
"<font color = red>#</font>must input either Description or Code Description in Chinese",
"<font color = red>^</font>fields can be updated if record exists"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportColumns']['Activity'] = array(
"<font color = red>*</font>School Year (Year format must be yyyy or yyyy-YYYY or yy/YY)",
"<font color = red>*</font>STA Duration (Term Number) (1 - Term 1, 2 - Term 2)",
"Class Level",
"<font color = red>@</font>Class Name",
"<font color = red>@</font>Class Number",
"<font color = red>@</font>WebSAMSRegNo",
"Student Name (English)",
"Student Name (Chinese)",
"<font color = red>*</font>Type (Either E or S. E - Activity, S - Service)",
"<font color = red>*</font>Activity (Chinese)",
"<font color = red>^</font>Post (Chinese)",
"<font color = red>^</font>Performance (Chinese)"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportRemarks']['Activity'] = array(
"<font color = red>*</font>Mandatory field(s)",
"<font color = red>@</font>must input either WebSAMSRegNo or (Class Name + Class Number)",
"<font color = red>^</font>fields can be updated if record exists"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportColumns']['Comment'] = array(
"<font color = red>@</font>WebSAMSRegNo",
"<font color = red>*</font>School Year (Year format must be yyyy or yyyy-YYYY or yy/YY)",
"<font color = red>*</font>Term (0 - The whole academic year, 1 - Term 1, 2 - Term 2)",
"<font color = red>@</font>Class",
"<font color = red>@</font>Class No",
"Student Name (Chinese)",
"<font color = red>+^</font>Academic Result",
"<font color = red>+^</font>Conduct Mark",
"<font color = red>+^</font>Comment (Chinese)",
"<font color = red>+^</font>Comment (English)"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportRemarks']['Comment'] = array(
"<font color = red>*</font>Mandatory field(s)",
"<font color = red>@</font>must input either WebSAMSRegNo or (Class + Class No)",
"<font color = red>+</font>must input at least one of these fields: Academic Result, Conduct Mark, Comment (Chinese), Comment (English)",
"<font color = red>^</font>fields can be updated if record exists"
);
$Lang['iPortfolio']['StudentAccount']['TeacherComment']['Academic'] = 'Academic';
// Learing Portfolio 2

$langpf_lp2['admin']['top']['new'] = "New";
$langpf_lp2['admin']['top']['public'] = "Public";
$langpf_lp2['admin']['top']['private'] = "Private";
$langpf_lp2['admin']['top']['delete'] = "Delete";
$langpf_lp2['admin']['top']['pleaseselectportfolio'] = 'Please select portfolio(s)';
$langpf_lp2['admin']['top']['portfoliosselected'] = 'portfolio(s) selected';
$langpf_lp2['admin']['top']['areyousureto'] = 'Are you sure to';
$langpf_lp2['admin']['top']['managetemplates'] = "Manage Templates";
$langpf_lp2['admin']['top']['group'] = "Group";
$langpf_lp2['admin']['top']['portfolio'] = 'Portfolio';
$langpf_lp2['admin']['top']['all'] = "All";

$langpf_lp2['admin']['navi']['records'] = "Records";
$langpf_lp2['admin']['navi']['total'] = "Total";
$langpf_lp2['admin']['navi']['search'] = "Search";
$langpf_lp2['admin']['navi']['of'] = 'of';
$langpf_lp2['admin']['navi']['sortby'] = 'Sort by';
$langpf_lp2['admin']['navi']['in'] = 'in';
$langpf_lp2['admin']['navi']['order'] = 'order';
$langpf_lp2['admin']['navi']['order_choice']['modified'] = "Last Modified";
$langpf_lp2['admin']['navi']['order_choice']['title'] = "Title";
$langpf_lp2['admin']['navi']['order_choice']['deadline'] = "Deadline";
$langpf_lp2['admin']['navi']['order_choice']['student_count'] = "No. of students";
$langpf_lp2['admin']['navi']['order_choice']['status'] = $langpf_lp2['admin']['top']['public'].'/'.$langpf_lp2['admin']['top']['private'];
$langpf_lp2['admin']['navi']['ascending'] = "Ascending";
$langpf_lp2['admin']['navi']['descending'] = "Descending";
$langpf_lp2['admin']['navi']['showpublishedonly'] = 'Show Published Only';
$langpf_lp2['admin']['navi']['page'] = "Page";
$langpf_lp2['admin']['navi']['display'] = "Display";
$langpf_lp2['admin']['navi']['all'] = "All";
$langpf_lp2['admin']['navi']['perpage'] = "per page";
$langpf_lp2['admin']['navi']['back'] = "Back" ;
$langpf_lp2['admin']['navi']['studentprogress'] = "Student Progress";
$langpf_lp2['admin']['navi']['studentProgressSortFields']['className|asc'] = "Class name - Class number (Ascending)";
$langpf_lp2['admin']['navi']['studentProgressSortFields']['className|desc'] = "Class name - Class number (Descending)";
$langpf_lp2['admin']['navi']['studentProgressSortFields']['notes_published|asc'] = "Publish time (Ascending)";
$langpf_lp2['admin']['navi']['studentProgressSortFields']['notes_published|desc'] = "Publish time (Descending)";
$langpf_lp2['admin']['navi']['studentProgressPublishStatus']['all'] = "Show all portfolios";
$langpf_lp2['admin']['navi']['studentProgressPublishStatus']['published'] = "Show published only";
$langpf_lp2['admin']['navi']['studentProgressPublishStatus']['notPublished'] = "Show draft only";

$langpf_lp2['admin']['item']['edit'] = "Edit";
$langpf_lp2['admin']['item']['settings'] = "Settings";
$langpf_lp2['admin']['item']['publish'] = "Publish";
$langpf_lp2['admin']['item']['export'] = 'Export';
$langpf_lp2['admin']['item']['clone'] = 'Clone';
$langpf_lp2['admin']['item']['starttime'] = "Start Time";
$langpf_lp2['admin']['item']['deadline'] = "Deadline";
$langpf_lp2['admin']['item']['lastmodified'] = "Last modified";
$langpf_lp2['admin']['item']['published'] = "Published";
$langpf_lp2['admin']['item']['drafted'] = 'Drafted';
$langpf_lp2['admin']['item']["lessthan1minago"] = 'less than 1 minute ago';
$langpf_lp2['admin']['item']["minsago"] = 'minutes ago';
$langpf_lp2['admin']['item']["hoursago"] = 'hours ago';
$langpf_lp2['admin']['item']['today'] = "Today";
$langpf_lp2['admin']['item']['yesterday'] = "Yesterday";
$langpf_lp2['admin']['item']['daysago'] = 'days ago';
$langpf_lp2['admin']['item']['dayago'] = 'day ago';
$langpf_lp2['admin']['item']['private'] = "Private";
$langpf_lp2['admin']['item']['viewprogress'] = 'View Progress';
$langpf_lp2['admin']['item']['total'] = 'Total';
$langpf_lp2['admin']['item']['students'] = "Students";
$langpf_lp2['admin']['item']['share'] = 'Share';
$langpf_lp2['admin']['item']['clonefrom'] = 'Clone from';
$langpf_lp2['admin']['item']['newportfolio'] = 'New Portfolio';
$langpf_lp2['admin']['item']['pleaseenaterperiod'] = 'Please enter period';
$langpf_lp2['admin']['item']['areyousuretoforcepublish'] = 'Are you sure to force publish?';

$langpf_lp2['admin']['export']['student'] = "Student Name";
$langpf_lp2['admin']['export']['class'] = "Class";
$langpf_lp2['admin']['print']['class'] = "Class";
$langpf_lp2['admin']['print']['selectclass'] = 'Select Class';
$langpf_lp2['admin']['print']['student'] = "Student";
$langpf_lp2['admin']['print']['selectall'] = "Select All";
$langpf_lp2['admin']['print']['page'] = "Page";
$langpf_lp2['admin']['print']['continue'] = "Continue";

$langpf_lp2['edit']['tool']['settings'] = "Settings";
$langpf_lp2['edit']['tool']['publish'] = 'Publish';
$langpf_lp2['edit']['tool']['recyclebin'] = 'Recycle Bin';
$langpf_lp2['edit']['tool']['new'] = "New";
$langpf_lp2['edit']['tool']['newweblog'] = 'New Weblog';
$langpf_lp2['edit']['tool']['edit'] = "Edit";
$langpf_lp2['edit']['tool']['delete'] = "Delete";
$langpf_lp2['edit']['tool']['reset'] = "Resume to teacher's template";
$langpf_lp2['edit']['tool']['reorder'] = 'Reorder';
$langpf_lp2['edit']['tool']['dragtoreorder'] = 'Drag to reorder';
$langpf_lp2['edit']['tool']['share'] = 'Share';
$langpf_lp2['edit']['tool']['print'] = 'Print';

$langpf_lp2['edit']['themesettings']['theme'] = 'Theme';
$langpf_lp2['edit']['themesettings']['banner'] = 'Banner';
$langpf_lp2['edit']['themesettings']['enablebanner'] = 'Enable Banner';
$langpf_lp2['edit']['themesettings']['allowstudentstocustomizebanner'] = 'Allow students to customize banner';
$langpf_lp2['edit']['themesettings']['uploadedbanner'] = 'Uploaded Banner';
$langpf_lp2['edit']['themesettings']['bestsize'] = 'Best Size';
$langpf_lp2['edit']['themesettings']['fileformat'] = 'File Format';
$langpf_lp2['edit']['themesettings']['rootsections'] = 'Root Sections';
$langpf_lp2['edit']['themesettings']['allowstudentstocustomizerootsections'] = 'Allow students to customize root sections';

$langpf_lp2['edit']['recyclebin']['recoveredcontent'] = 'Recovered Content';
$langpf_lp2['edit']['recyclebin']['removedcontent'] = 'Removed Content';
$langpf_lp2['edit']['recyclebin']['select'] = '-- Select --';
$langpf_lp2['edit']['recyclebin']['deletedat'] = 'Deleted at';
$langpf_lp2['edit']['recyclebin']['close'] = 'Close';

$langpf_lp2['edit']['weblog']['newweblog'] = 'New Weblog';
$langpf_lp2['edit']['weblog']['weblogtitle'] = 'Weblog Title';
$langpf_lp2['edit']['weblog']['weblogcontent'] = 'Weblog Content';

$langpf_lp2['edit']['section']['main'] = 'Main';
$langpf_lp2['edit']['section']['main_alt'] = '主頁';

$langpf_lp2['edit']['editform']['title'] = "Title";
$langpf_lp2['edit']['editform']['date'] = "Date";
$langpf_lp2['edit']['editform']['instruction'] = "Instruction";
$langpf_lp2['edit']['editform']['content'] = "Content";
$langpf_lp2['edit']['editform']['pleaseentertitle'] = 'Please enter title';
$langpf_lp2['edit']['editform']['pleaseentervaliddate'] = 'Please enter valid date';
$langpf_lp2['edit']['editform']['loadtemplate'] = 'Load template';
$langpf_lp2['edit']['editform']['newtemplate'] = 'New template';
$langpf_lp2['edit']['editform']['pleaseenternewtemplatetitle'] = 'Please enter title of new template';
$langpf_lp2['edit']['editform']['areyousuretosavethecontentof'] = 'Are you sure to save the content to';
$langpf_lp2['edit']['editform']['templatenamealreadyexists'] = 'Template name already exists, please try again';
$langpf_lp2['edit']['editform']['load'] = 'Load';
$langpf_lp2['edit']['editform']['save'] = 'Save';
$langpf_lp2['edit']['editform']['weblog'] = 'Web Log';
$langpf_lp2['edit']['editform']['custom'] = 'Web Page';
$langpf_lp2['edit']['editform']['blank'] = 'Nil';
$langpf_lp2['edit']['editform']['allowstudentsto'] = 'Allow Students to';
$langpf_lp2['edit']['editform']['from'] = 'From';
$langpf_lp2['edit']['editform']['to'] = 'to';
$langpf_lp2['edit']['editform']['hidden'] = '(hidden)';
$langpf_lp2['edit']['editform']['editcontent'] = 'Edit content';
$langpf_lp2['edit']['editform']['editcontentandcustomizesections'] = 'Edit Content and customize sections';
$langpf_lp2['edit']['editform']['showthissectionto'] = 'Show this section to';
$langpf_lp2['edit']['editform']['fileupload'] = 'File Upload';
$langpf_lp2['edit']['editform']['addfile'] = 'Add File';
$langpf_lp2['edit']['editform']['pleaseselectpdffile'] = 'Please select PDF(.pdf) file.';

$langpf_lp2['edit']['message']['warning'] = 'Warning';
$langpf_lp2['edit']['message']['notice'] = 'Notice';
$langpf_lp2['edit']['message']['portfolioschemeupdated'] = "Portfolio Scheme Updated";
$langpf_lp2['edit']['message']['draftexist'] = 'Some students have started working on the portfolio, so performing any updates will reset all student works!';
$langpf_lp2['edit']['message']['asksync'] = "Teacher has modified the portfolio scheme.<br/>You may update with your portfoilo now, but the updated sections will be reset! ";
$langpf_lp2['edit']['message']['newsectionscreated'] = 'The teacher has created some new sections in the portfolio, would you like to import them?';
$langpf_lp2['edit']['message']['sectionupdated'] = '"%1$s" has been %2$sd by the teacher, would you like to %2$s it now';
$langpf_lp2['edit']['message']['update'] = "update";
$langpf_lp2['edit']['message']['updatenow'] = "Update Now!";
$langpf_lp2['edit']['message']['updatelater'] = "Update Later";
$langpf_lp2['edit']['message']['areyousure'] = "Are you sure?";
$langpf_lp2['edit']['message']['areyousuretodelete'] = "Are you sure to delete";
$langpf_lp2['edit']['message']['draftnoteditable'] = 'You cannot edit the portfolio now because it is out of editable period.';
$langpf_lp2['edit']['message']['discardchangesandclose'] = "Discard changes and close?";
$langpf_lp2['edit']['message']['remembertopublishupdates'] = "Remember to publish your updates!";
$langpf_lp2['edit']['message']['dropbannerhere'] = "Drop Banner file/URL here";
$langpf_lp2['edit']['message']['isnotavaildbannerformat'] = "is not a vaild banner format!";
$langpf_lp2['edit']['message']['doesnotsupportuploadmethod'] = "Your browser does not support this upload method. Please upload your banner in \"Settings\"";

$langpf_lp2['common']['share']['shareurl'] = 'Public URL';
$langpf_lp2['common']['share']['facebook'] = 'Facebook';
$langpf_lp2['common']['share']['sharetofacebook'] = 'Share to Facebook';
$langpf_lp2['common']['share']['sharetopeer'] = 'Share to Peer';
$langpf_lp2['common']['share']['findstudents'] = 'Find Students';
$langpf_lp2['common']['share']['allclasses'] = 'All Classes';
$langpf_lp2['common']['share']['studentnameorloginid'] = 'Students name or Login ID';
$langpf_lp2['common']['share']['search'] = "Search";
$langpf_lp2['common']['share']['searchresult'] = 'Search Result';
$langpf_lp2['common']['share']['add'] = 'Add';
$langpf_lp2['common']['share']['remove'] = 'Remove';
$langpf_lp2['common']['share']['sharedfriends'] = 'Shared Friends';
$langpf_lp2['common']['share']['my_friend_list'] = 'My Friend List';
$langpf_lp2['common']['share']['searchByInputFormat'] = 'Search by input format<br/>"[Class Name] [Class Number] [Name]"<br/>(e.g. 1a15 Chan)';
$langpf_lp2['common']['publish']['useindividualsettings'] = '(Use individual settings)';
$langpf_lp2['common']['publish']['sectionsettings'] = 'Section Settings';
$langpf_lp2['common']['publish']['setallsectionsvisibleto'] = 'Set <strong>All</strong> Sections visible to';
$langpf_lp2['common']['publish']['showfacebooklikebutton'] = 'Show Facebook Like button';
$langpf_lp2['common']['publish']['publishsuccess'] = 'Portfolio published successfully.';

$langpf_lp2['common']['form']['myselfonly'] = 'Myself Only';
$langpf_lp2['common']['form']['usersinsamegroup'] = 'Users in same group';
$langpf_lp2['common']['form']['allintranetusers'] = 'All Intranet users';
$langpf_lp2['common']['form']['public'] = "Public";
$langpf_lp2['common']['form']['publish'] = 'Publish';
$langpf_lp2['common']['form']['apply'] = 'Apply';
$langpf_lp2['common']['form']['cancel'] = 'Cancel';
$langpf_lp2['common']['form']['close'] = 'Close';
$langpf_lp2['common']['form']['yes'] = "Yes";
$langpf_lp2['common']['form']['no'] = "No";
$langpf_lp2['common']['error']['noavailable'] = "No Learning Portfolio available.";

#For SIS CUST
	$ec_iPortfolio['role'] = "Role";
	$ec_iPortfolio['reportcard_CIP'] = "Community Involvement Programme (CIP)";
	$ec_iPortfolio['reportcard_CCA_Inv'] = "CCA Membership";
	$ec_iPortfolio['reportcard_CCA_Ach'] = "Awards & Achievements";
	$ec_iPortfolio['reportcard_Fitness'] = "National Physical Fitness Award (NAPFA) - for P4 and above";
	$ec_iPortfolio['reportcard_header'] = "Record of Co-Curricular Achievements";
	$ec_iPortfolio['reportcard_academicyr'] = "Academic Year";

	$ec_iPortfolio['reportcard_Signature'] = array("Teacher's Signature","Principal's Signature","Parent's Signature");
	$ec_iPortfolio['total'] = "Total";
	$ec_iPortfolio['title_activity'] = "Activity";
	$ec_iPortfolio['hours'] = "Hours";
	$ec_iPortfolio['AllowStudentsToJoin'] = "Allow Students To Join";

$Lang['iPortfolio']['SPTSS']['SPC']['Student']='Student';
$Lang['iPortfolio']['SPTSS']['SPC']['Integrity']='Integrity';
$Lang['iPortfolio']['SPTSS']['SPC']['Courtesy']='Courtesy';
$Lang['iPortfolio']['SPTSS']['SPC']['Right From Wrong']='Right From Wrong';
$Lang['iPortfolio']['SPTSS']['SPC']['Relationship']='Relationship';
$Lang['iPortfolio']['SPTSS']['SPC']['Learning Attitude']='Learning Attitude';
$Lang['iPortfolio']['SPTSS']['SPC']['Autonomy']='Autonomy';

$Lang['iPortfolio']['Promotion'] = "Promotion";

$Lang['iPortfolio']['JinYing']['JinYingScheme'] = "JinYing Scheme";
$Lang['iPortfolio']['JinYing']['SelectItem'] = "- Select Item -";
$Lang['iPortfolio']['JinYing']['ajaxError'] = "AJAX Error!";
$Lang['iPortfolio']['JinYing']['ActivityItems'] = "Items";
$Lang['iPortfolio']['JinYing']['EmptySymbol'] = "-";
$Lang['iPortfolio']['JinYing']['TargetAttained'] = "Target attained";
$Lang['iPortfolio']['JinYing']['TargetHighlyAttained'] = "Target highly attained";
$Lang['iPortfolio']['JinYing']['TargetNotAttained'] = "Target not attained. Greater effort needed.";
$Lang['iPortfolio']['JinYing']['Scope']['BSQ'] = "Bettering self-quality";
$Lang['iPortfolio']['JinYing']['Scope']['EPW'] = "Enhancing personal well-being";
$Lang['iPortfolio']['JinYing']['Scope']['OFS'] = "Offering services";
$Lang['iPortfolio']['JinYing']['Scope']['BTP'] = "Building talents";
$Lang['iPortfolio']['JinYing']['ScopeName'] = "Scope";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['HW'] = "Homework";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['PC'] = "Punctuality";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['AD'] = "Attendance";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['AI'] = "Attire";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['RC'] = "Extensive Reading (Chinese)";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['RE'] = "Extensive Reading (English)";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['EC'] = "Overseas Excursion#";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['TA'] = "Enhancement (Bettering self-quality)#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['PY'] = "Physique#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['PJ'] = "Physical agility (jogging)#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['PS'] = "Physical agility (swimming)#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['PW'] = "Physical well-being";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['CA'] = "Christian faith building";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['TA'] = "Enhancement (Enhancing personal well-being)#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['OC'] = "Overall comment";
$Lang['iPortfolio']['JinYing']['Item']['OFS']['SI'] = "Services in school#";
$Lang['iPortfolio']['JinYing']['Item']['OFS']['SO'] = "Services outside school#";
$Lang['iPortfolio']['JinYing']['Item']['BTP']['EA'] = "Extracurricular activities#";
$Lang['iPortfolio']['JinYing']['Item']['BTP']['IS'] = "Competitions in school#";
$Lang['iPortfolio']['JinYing']['Item']['BTP']['IC'] = "Participation in external competitions#";
$Lang['iPortfolio']['JinYing']['Item']['BTP']['TP'] = "Talent performances#";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-HW']['BSQ-HW-P-01'] = "1. 交齊所有家課";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-HW']['BSQ-HW-P-02'] = "2. 欠交功課次數少於五次";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-HW']['BSQ-HW-N-01'] = "3. 欠交功課次數五次或以上";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-PC']['BSQ-PC-P-01'] = "1.從未遲到";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-PC']['BSQ-PC-P-02'] = "2.遲到次數少於三次";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-PC']['BSQ-PC-N-01'] = "3.遲到次數三次或以上";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-AD']['BSQ-AD-P-01'] = "1.出席所有上課天";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-AD']['BSQ-AD-N-01'] = "2.曾經請假";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-AI']['BSQ-AI-P-01'] = "1.符合規定";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-AI']['BSQ-AI-N-01'] = "2.違規";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-RC']['BSQ-RC-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-RC']['BSQ-RC-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-RE']['BSQ-RE-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-RE']['BSQ-RE-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-EC']['BSQ-EC-P-01'] = "a.達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-EC']['BSQ-EC-P-02'] = "b.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-EC']['BSQ-EC-N-01'] = "c.未達標，仍須努力";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-TA']['BSQ-TA-P-01'] = "a.達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-TA']['BSQ-TA-P-02'] = "b.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-TA']['BSQ-TA-N-01'] = "c.未達標，仍須努力";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PY']['EPW-PY-P-01'] = "1.合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PY']['EPW-PY-N-01'] = "2.不合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PJ']['EPW-PJ-P-01'] = "1.合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PJ']['EPW-PJ-N-01'] = "2.不合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PS']['EPW-PS-P-01'] = "1.合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PS']['EPW-PS-N-01'] = "2.不合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PW']['EPW-PW-P-01'] = "1.成績B+或以上";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PW']['EPW-PW-P-02'] = "2.成績B至E";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PW']['EPW-PW-N-01'] = "3.成績F或以下";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-CA']['EPW-CA-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-CA']['EPW-CA-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-TA']['EPW-TA-P-01'] = "a.達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-TA']['EPW-TA-P-02'] = "b.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-TA']['EPW-TA-N-01'] = "c.未達標，仍須努力";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-OC']['EPW-OC-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-OC']['EPW-OC-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeName']['OFS-SI']['OFS-SI-P-01'] = "a.表現良好，獲記功（填H欄）";
$Lang['iPortfolio']['JinYing']['CodeName']['OFS-SI']['OFS-SI-P-02'] = "b.不獲記功";
$Lang['iPortfolio']['JinYing']['CodeName']['OFS-SO']['OFS-SO-P-01'] = "校外服務時數";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-EA']['BTP-EA-P-01'] = "a.出席不少於70%而表現積極";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-EA']['BTP-EA-P-02'] = "b.出席不少於70%而表現一般";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-EA']['BTP-EA-N-01'] = "c.出席率不足70%或表現欠佳";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IS']['BTP-IS-P-01'] = "1.曾參加並獲獎";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IS']['BTP-IS-P-02'] = "2.曾參加但未有獲獎";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IS']['BTP-IS-N-01'] = "3.未曾參加";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-01'] = "a.全年出席練習不少於70%而表現積極";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-02'] = "b.全年出席練習不少於70%而表現滿意";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-N-01'] = "c.全年出席練習不足70%或表現欠佳";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-03'] = "d.獲獎項及推薦記功 (填K L及M欄)";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-04'] = "e.無需賽前練習。無獲獎";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-TP']['BTP-TP-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-TP']['BTP-TP-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-HW-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-HW-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-HW-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-PC-P-01'] = "No tardy record";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-PC-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-PC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-AD-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-AD-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-AI-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-AI-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-RC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-RC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-RE-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-RE-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-EC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-EC-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-EC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-TA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-TA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-TA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PY-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PY-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PJ-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PJ-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PS-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PS-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PW-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PW-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PW-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-CA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-CA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-TA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-TA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-TA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-OC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-OC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['OFS-SI-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['OFS-SI-P-02'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['OFS-SO-P-01'] = "%s hours of voluntary work";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-EA-P-01'] = "Attendance rate is 70% or more. Active";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-EA-P-02'] = "Attendance rate is 70% or more. Fair performance.";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-EA-N-01'] = "Attendance rate is less than 70%. Unsatisfactory";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IS-P-01'] = "Have participated in competitions in school";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IS-P-02'] = "Have participated in competitions in school";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IS-N-01'] = "No record";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-P-01'] = "Participation. Attendance rate for practice is 70% or more.";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-P-02'] = "Participation. Attendance rate for practice is 70% or more.";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-P-03'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-P-04'] = "Participation";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-N-01'] = "Participation. Attendance rate for practice is less than 70%.";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-TP-P-01'] = "Have participated in talent performances";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-TP-N-01'] = "No record";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-HW-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-HW-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-HW-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-PC-P-01'] = "Excellent";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-PC-P-02'] = "Good";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-PC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-AD-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-AD-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-AI-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-AI-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-RC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-RC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-RE-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-RE-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-EC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-EC-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-EC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-TA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-TA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-TA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PY-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PY-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PJ-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PJ-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PS-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PS-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PW-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PW-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PW-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-CA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-CA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-TA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-TA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-TA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-OC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-OC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['OFS-SI-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['OFS-SI-P-02'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['OFS-SO-P-01'] = "%s hours of voluntary work";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-EA-P-01'] = "Attendance rate is 70% or more. Active";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-EA-P-02'] = "Attendance rate is 70% or more. Fair performance.";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-EA-N-01'] = "Participation";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IS-P-01'] = "Have participated in competitions in school";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IS-P-02'] = "Have participated in competitions in school";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IS-N-01'] = "No record";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-P-01'] = "Participation. Attendance rate for practice is 70% or more.";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-P-02'] = "Participation. Attendance rate for practice is 70% or more.";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-P-03'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-P-04'] = "Participation";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-N-01'] = "Participation";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-TP-P-01'] = "Have participated in talent performances";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-TP-N-01'] = "No record";
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-HW-P-01'] = "1 good point";
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-HW-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-HW-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-PC-P-01'] = "1 good point";
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-PC-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-PC-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-AD-P-01'] = "1 good point";
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-AD-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-AI-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-AI-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-RC-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-RC-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-RE-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-RE-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-EC-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-EC-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-EC-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-TA-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-TA-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-TA-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PY-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PY-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PJ-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PJ-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PS-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PS-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PW-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PW-P-02'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PW-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-CA-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-CA-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-TA-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-TA-P-02'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-TA-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-OC-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-OC-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['OFS-SI-P-01'] = "%s %s";
$Lang['iPortfolio']['JinYing']['CodeAward']['OFS-SI-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['OFS-SO-P-01'] = "%s good point";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-EA-P-01'] = "Target attained with good performance";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-EA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-EA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IS-P-01'] = "Target attained with good performance";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IS-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IS-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-01'] = "Target attained with good performance";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-03'] = "%s %s";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-04'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-TP-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-TP-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['Award']['Merit_1'] = "1 good point";
$Lang['iPortfolio']['JinYing']['Award']['Merit_2'] = "2 good points";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_1'] = "1 minor merit";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_1_Merit_1'] = "1 minor merit and 1 good point";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_1_Merit_2'] = "1 minor merit and 2 good points";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_2'] = "2 minor merits";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_2_Merit_1'] = "2 minor merits and 1 good point";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_2_Merit_2'] = "2 minor merits and 2 good points";
$Lang['iPortfolio']['JinYing']['Award']['MajorM_1'] = "1 major merit";
$Lang['iPortfolio']['JinYing']['Export']['Error']['InputError'] = "Input error";
$Lang['iPortfolio']['JinYing']['Export']['Error']['InputInteger4ServiceHours'] = "Please input integer between 1 and 300";
$Lang['iPortfolio']['JinYing']['Export']['Error']['NotInList'] = "Input value is wrong, please select from list";
$Lang['iPortfolio']['JinYing']['Export']['Error']['ReadOnlyTitle'] = "Readonly Item";
$Lang['iPortfolio']['JinYing']['Export']['Error']['ReadOnlyContent'] = "Cannot change read only item";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-HW'] = "班主任";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-PC'] = "訓導組";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-AD'] = "訓導組";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-AI'] = "訓導組";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-RC'] = "中文科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-RE'] = "英文科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-EC'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-TA'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-PY'] = "體育科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-PJ'] = "體育科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-PS'] = "體育科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-PW'] = "體育科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-CA'] = "宗教組";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-TA'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-OC'] = "班主任";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['OFS-SI'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['OFS-SO'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BTP-EA'] = "會員制學會負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BTP-IS'] = "班主任";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BTP-IC'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BTP-TP'] = "班主任";
$Lang['iPortfolio']['JinYing']['Export']['PressCtrlKey'] = "Press Ctrl key to select multiple items";
$Lang['iPortfolio']['JinYing']['Export']['WarningSelectItem'] = "Please select item";
$Lang['iPortfolio']['JinYing']['Export']['WarningSelectClassName'] = "Please select class name";
$Lang['iPortfolio']['JinYing']['Export']['WarningSelectTerm'] = "Please select academic term";
$Lang['iPortfolio']['JinYing']['Import']['ActionSubType'] = "Action Subtype";
$Lang['iPortfolio']['JinYing']['Import']['ActionType'] = "Action Type";
$Lang['iPortfolio']['JinYing']['Import']['AddNew'] = "Insert";
$Lang['iPortfolio']['JinYing']['Import']['Delete'] = "Delete";
$Lang['iPortfolio']['JinYing']['Import']['DeleteSuccessful'] = "Delete JinYing Scheme successful";
$Lang['iPortfolio']['JinYing']['Import']['FileFormat'] = "Must be Excel 2007 format (.xlsx)";
$Lang['iPortfolio']['JinYing']['Import']['Replace'] = "Replace";
$Lang['iPortfolio']['JinYing']['Import']['Successful'] = "JinYing Scheme import successful";
$Lang['iPortfolio']['JinYing']['Import']['Update'] = "Update";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['ClassName'] ="Class Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['ClassNumber'] ="Class Number";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['WebSAMS'] ="WebSAMSRegNo";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['StudentName'] ="Student Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['ActivityNameEng'] ="Club Name (English)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['ActivityNameChi'] ="Club Name (Chinese)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['Performance'] ="Performance";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ClassName'] ="Class Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ClassNumber'] ="Class Number";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['WebSAMS'] ="WebSAMSRegNo";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['StudentName'] ="Student Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ActivityDate'] ="Activity Date";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ActivityNameEng'] ="Activity Name (English)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ActivityNameChi'] ="Activity Name (Chinese)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['OrganizationNameEng'] ="Organization Name (English)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['OrganizationNameChi'] ="Organization Name (Chinese)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['Performance'] ="Performance";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['AwardNameEng'] ="Name of Award (English)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['AwardNameChi'] ="Name of Award (Chinese)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['RecommendMerit'] ="Recommended Merit Number";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['ClassName'] ="Class Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['ClassNumber'] ="Class Number";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['WebSAMS'] ="WebSAMSRegNo";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['StudentName'] ="Student Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['ActivityNameEng'] ="Service Role (English)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['ActivityNameChi'] ="Service Role (Chinese)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['Performance'] ="Performance";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['RecommendMerit'] ="Recommended Merit Number";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['ClassName'] ="Class Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['ClassNumber'] ="Class Number";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['WebSAMS'] ="WebSAMSRegNo";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['StudentName'] ="Student Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['ServiceHours'] ="Service Hours";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['ClassName'] ="Class Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['ClassNumber'] ="Class Number";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['WebSAMS'] ="WebSAMSRegNo";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['StudentName'] ="Student Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['Performance'] ="Performance";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ClassName'] ="Class Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ClassNumber'] ="Class Number";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['WebSAMS'] ="WebSAMSRegNo";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['StudentName'] ="Student Name";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ActivityDate'] ="Activity Date";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ActivityNameEng'] ="Activity Name (English)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ActivityNameChi'] ="Activity Name (Chinese)";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['Performance'] ="Performance";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['WITH-ACTIVITY-DATE']['AwardInReport'] ="報告顯示獎項/成就";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['WITH-ACTIVITY-DATE']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['PHYSICAL']['PerformanceInReport'] ="報告顯示表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['PHYSICAL']['AwardInReport'] ="報告顯示獎項/成就";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['PHYSICAL']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SI']['PerformanceInReport'] ="報告顯示表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SI']['AwardInReport'] ="記功數目";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SI']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SO']['AwardInReport'] ="報告顯示獎項/成就 (獲記功數目)";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SO']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-EA']['PerformanceInReport'] ="報告顯示表現(評語)";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-EA']['AwardInReport'] ="報告顯示獎項/成就";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-EA']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-IC']['PerformanceInReport'] ="報告顯示表現(評語)";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-IC']['AwardInReport'] ="記功數目";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-IC']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['PerformanceInReport'] ="報告顯示表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['AwardInReport'] ="報告顯示獎項/成就";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['Semester'] ="學期";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD-WO-AWARD']['PerformanceInReport'] ="報告顯示表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD-WO-AWARD']['Semester'] ="學期";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD-WO-AWARD']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportRemarks']['FillInEnglish'] = "Please fill in English Name";
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'] = "<span class=tabletextrequire>*</span>Mandatory field(s)";
$Lang['iPortfolio']['JinYing']['ImportRemarks']['ACTIVITY'] = array(
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'],
"<span class=tabletextrequire>^</span>At least fill in one: Activity Name (English) or Activity Name (Chinese)"
);
$Lang['iPortfolio']['JinYing']['ImportRemarks']['BTP-EA'] = array(
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'],
"<span class=tabletextrequire>^</span>At least fill in one: Club Name (English) or Club Name (Chinese)"
);
$Lang['iPortfolio']['JinYing']['ImportRemarks']['OFS-SI'] = array(
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'],
"<span class=tabletextrequire>^</span>At least fill in one: Service Role (English) or Service Role (Chinese)"
);
$Lang['iPortfolio']['JinYing']['ImportRemarks']['STANDARD'] = array($Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory']);
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Update'] = "Mapped by RecordID";
$Lang['iPortfolio']['JinYing']['ImportStepArr']['SelectItem'] = "Select Import Item";
$Lang['iPortfolio']['JinYing']['ImportStepArr']['Confirmation'] = "Data Verification";
$Lang['iPortfolio']['JinYing']['ImportStepArr']['ImportResult'] = "Imported Result";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptyActivity'] = "Empty activity";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptyClubName'] = "Empty club name";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'] = "Empty performance";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptySemester'] = "Empty academic term";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptyService'] = "Empty service role";
$Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'] = "Performance is invalid";
$Lang['iPortfolio']['JinYing']['ImportError']['LoadingFile'] = "Error loading file \"";
$Lang['iPortfolio']['JinYing']['ImportError']['NotNumeric'] = "Not numeric";
$Lang['iPortfolio']['JinYing']['ImportError']['PerformanceConflictAward'] = "Do not input award and number of merits if not available";
$Lang['iPortfolio']['JinYing']['ImportError']['RecordDuplicate'] = "Record duplicate";
$Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'] = "Duplicate to existing data";
$Lang['iPortfolio']['JinYing']['ImportError']['RecordIDNotFound'] = "Record not found";
$Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'] = "Student not found";
$Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'] = "Student name does not match with WebSAMS";
$Lang['iPortfolio']['JinYing']['ImportWarning']['NonCurrentYear'] = "You select non-current academic year, do you want to continue?";
$Lang['iPortfolio']['JinYing']['Medal']['AllMedal'] = "All Award";
$Lang['iPortfolio']['JinYing']['Medal']['AssignBronze'] = "Assign Bronze Award";
$Lang['iPortfolio']['JinYing']['Medal']['AssignGold'] = "Assign Gold Award";
$Lang['iPortfolio']['JinYing']['Medal']['AssignSilver'] = "Assign Silver Award";
$Lang['iPortfolio']['JinYing']['Medal']['Award'] = "Award";
$Lang['iPortfolio']['JinYing']['Medal']['Awarded'] = "Awarded Medal";
$Lang['iPortfolio']['JinYing']['Medal']['AwardStatus'] = "Award Status";
$Lang['iPortfolio']['JinYing']['Medal']['Bronze'] = "Bronze Award";
$Lang['iPortfolio']['JinYing']['Medal']['CancelAward'] = "Cancel Award";
$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignBronze'] = "Are you sure to assign Bronze Award";
$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignGold'] = "Are you sure to assign Gold Award";
$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignSilver'] = "Are you sure to assign Silver Award";
$Lang['iPortfolio']['JinYing']['Medal']['ConfirmCancelAward'] = "Are you sure to cancel award";
$Lang['iPortfolio']['JinYing']['Medal']['Form'] = "Year Form";
$Lang['iPortfolio']['JinYing']['Medal']['Gold'] = "Gold Award";
$Lang['iPortfolio']['JinYing']['Medal']['GoodPoint'] = "Good Point";
$Lang['iPortfolio']['JinYing']['Medal']['HideOption'] = "Hide options";
$Lang['iPortfolio']['JinYing']['Medal']['Medal'] = "Medal";
$Lang['iPortfolio']['JinYing']['Medal']['NoMedal'] = "No Award";
$Lang['iPortfolio']['JinYing']['Medal']['NotAwarded'] = "Not Awarded";
$Lang['iPortfolio']['JinYing']['Medal']['OrAbove'] = " or above";
$Lang['iPortfolio']['JinYing']['Medal']['Point'] = "";
$Lang['iPortfolio']['JinYing']['Medal']['PresetCriteria'] = "Preset criteria for awarded medal";
$Lang['iPortfolio']['JinYing']['Medal']['ScopeRequirement'] = "Scope Requirement";
$Lang['iPortfolio']['JinYing']['Medal']['ShowOption'] = "Display options";
$Lang['iPortfolio']['JinYing']['Medal']['Silver'] = "Silver Award";
$Lang['iPortfolio']['JinYing']['Medal']['TargetMet'] = "Target met";
$Lang['iPortfolio']['JinYing']['Medal']['TargetMetWiGood'] = "Target met with good performance";
$Lang['iPortfolio']['JinYing']['Medal']['TargetMetScopeNumber'] = "Number of target attained scope";
$Lang['iPortfolio']['JinYing']['Medal']['Times'] = "";
$Lang['iPortfolio']['JinYing']['MedalError']['EmptyClassLevel'] = "Please select class level";
$Lang['iPortfolio']['JinYing']['RecommendAward']['Merit_1'] = "a.優點1個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['Merit_2'] = "b.優點2個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_1'] = "c.小功1個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_1_Merit_1'] = "d.小功1個及優點1個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_1_Merit_2'] = "e.小功1個及優點2個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_2'] = "f.小功2個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_2_Merit_1'] = "g.小功2個及優點1個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_2_Merit_2'] = "h.小功2個及優點2個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MajorM_1'] = "i.大功1個";
$Lang['iPortfolio']['JinYing']['Remark'] = "# Item independent of academic year term";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Gold'] = "The above student obtains Gold Award in the Student Profile Scheme.";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Silver'] = "The above student obtains Silver Award in the Student Profile Scheme.";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Bronze'] = "The above student obtains Bronze Award in the Student Profile Scheme.";
$Lang['iPortfolio']['JinYing']['Report']['Award']['None'] = "The above student obtains no award in the Student Profile Scheme.";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Excellent'] = "Excellent performance";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Merit'] = "good point";
$Lang['iPortfolio']['JinYing']['Report']['Award']['MinorM'] = "minor merit";
$Lang['iPortfolio']['JinYing']['Report']['Award']['MajorM'] = "major merit";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-HW'] = "Homework non-submission record (less than 5 times per school term)";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-PC'] = "Tardy record (less than 3 times per school term)";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-AD'] = "Full attendance";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-AI'] = "Fully observe school regulations";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-RC'] = "Read at least 2 books and finish reading reports";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-RE'] = "Read at least 2 books and finish reading reports";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-EC'] = "Participate in related activities organized by the school voluntarily and perform satisfactorily";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-TA'] = "Participate in related training schemes organized by the school and perform satisfactorily";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-PY'] = "Pass in body fat test";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-PJ'] = "Possess good physical agility";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-PS'] = "Possess good physical agility";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-PW'] = "Possess good overall physical agility";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-CA'] = "Participate in in-school Christian activities voluntarily (not less than 2 events per school term)";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-TA'] = "Participate in related training schemes organized by the school and perform satisfactorily";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-OC'] = "Be worthy of admiration";
$Lang['iPortfolio']['JinYing']['Report']['ClassName'] = "Class Name";
$Lang['iPortfolio']['JinYing']['Report']['ClassNumber'] = "Class No.";
$Lang['iPortfolio']['JinYing']['Report']['ClassTeacher'] = "Class Teacher";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Award'] = "Award";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['BasicRequirements'] = "Basic Requirements";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Item'] = "Items";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Performance'] = "Performance";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['PerformanceAward'] = "Performance/Award";
$Lang['iPortfolio']['JinYing']['Report']['DateOfIssue'] = "Date of Issue";
$Lang['iPortfolio']['JinYing']['Report']['Details'] = "Student’s performance under the Student Profile Scheme is detailed as below.";
$Lang['iPortfolio']['JinYing']['Report']['DetailsRemark'] = "(For items marked with asterisk*, good point(s) are awarded for excellent performance.)";
$Lang['iPortfolio']['JinYing']['Report']['Index'][0] = "A";
$Lang['iPortfolio']['JinYing']['Report']['Index'][1] = "B";
$Lang['iPortfolio']['JinYing']['Report']['Index'][2] = "C";
$Lang['iPortfolio']['JinYing']['Report']['Index'][3] = "D";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-HW'] = "Homework *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-PC'] = "Punctuality *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-AD'] = "Attendance *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-AI'] = "Attire";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-RC'] = "Extensive Reading (Chinese)";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-RE'] = "Extensive Reading (English)";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-EC'] = "Overseas Excursion";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-TA'] = "Enhancement";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['EPW-PY'] = "Physique";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['EPW-PJ'] = "Physical agility (jogging)";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['EPW-PS'] = "Physical agility (swimming)";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['EPW-PW'] = "Physical well-being";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['EPW-CA'] = "Christian faith building";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['EPW-OC'] = "Overall comment";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['EPW-TA'] = "Enhancement";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['OFS-SI'] = "Services in school *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['OFS-SO'] = "Services outside school *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-EA'] = "Extracurricular Activities";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-IS'] = "Competitions in school";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-IC'] = "Competitions outside school *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-TP'] = "Talent performances";
$Lang['iPortfolio']['JinYing']['Report']['JYR'] = "Student Profile Report";
$Lang['iPortfolio']['JinYing']['Report']['NoRecord'] = "No record";
$Lang['iPortfolio']['JinYing']['Report']['NotApplicable'] = "Not applicable";
$Lang['iPortfolio']['JinYing']['Report']['Organizer'] = "Organizer";
$Lang['iPortfolio']['JinYing']['Report']['Page']['PageNumSuffix'] = "Page ";
$Lang['iPortfolio']['JinYing']['Report']['Page']['TotPageNumPrefix'] = " of ";
$Lang['iPortfolio']['JinYing']['Report']['Page']['TotPageNumSuffix'] = " pages";
$Lang['iPortfolio']['JinYing']['Report']['Parent'] = "Parent/Guardian";
$Lang['iPortfolio']['JinYing']['Report']['Principal'] = "Principal";
$Lang['iPortfolio']['JinYing']['Report']['RegisterNumber'] = "Reg. No.";
$Lang['iPortfolio']['JinYing']['Report']['Scope']['BSQ'] = "(%s) Bettering self-quality";
$Lang['iPortfolio']['JinYing']['Report']['Scope']['EPW'] = "(%s) Enhancing personal well-being";
$Lang['iPortfolio']['JinYing']['Report']['Scope']['OFS'] = "(%s) Offering services";
$Lang['iPortfolio']['JinYing']['Report']['Scope']['BTP'] = "(%s) Building talents";
$Lang['iPortfolio']['JinYing']['Report']['SLP'] = "Student Learning Profile";
$Lang['iPortfolio']['JinYing']['Report']['StudentName'] = "Name";
$Lang['iPortfolio']['JinYing']['Report']['Title']['JYR'] = "Student Profile Report %s/%s";
$Lang['iPortfolio']['JinYing']['Report']['Title']['SLP'] = "Student Learning Profile %s/%s";
$Lang['iPortfolio']['JinYing']['ReturnMessage']['MissingItem'] = "0|=|Item Code cannot be empty";
$Lang['iPortfolio']['JinYing']['StatisticalAnalysis'] = "Statistical Analysis";
$Lang['iPortfolio']['JinYing']['StatisticsByClass'] = "Student Profile Report Statistical Analysis by Class %s/%s";
$Lang['iPortfolio']['JinYing']['StatisticsByForm'] = "Student Profile Report Statistical Analysis by Form %s/%s";
$Lang['iPortfolio']['JinYing']['StatisticsByItem'] = "Student Profile Report Statistical Analysis by Item %s/%s (%s)";
$Lang['iPortfolio']['JinYing']['StatisticsByItemAll'] = "Student Profile Report Statistical Analysis by Item %s/%s";
$Lang['iPortfolio']['JinYing']['StatisticsByScope'] = "Student Profile Report Statistical Analysis by Scope %s/%s (%s)";
$Lang['iPortfolio']['JinYing']['StatisticsByMethod'] = "Display by method";
$Lang['iPortfolio']['JinYing']['Statistics']['ByClass'] = "By Year Class";
$Lang['iPortfolio']['JinYing']['Statistics']['ByForm'] = "By Year Form";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['AllItems'] = "All items (Export only)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] = "Award";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['MeritExcludeTargetMet'] = "Award(Exclude Target Met)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] = "No import data";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoMeritWithParticipation'] = "No Award (Participatiion)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] = "No Participation";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEA'] = "No participation in extracurricular activities";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEC'] = "No participation in external competitions";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Participation'] = "Participation (No matter award or not)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['ParticipationWithoutPrize'] = "Participation (No Award)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithMerit'] = "Awarded with merit";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithoutMerit'] = "Awarded without merit";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMet'] = "Target Met";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] = "Target Met(exclude target met with good performance)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeMerit'] = "Target Met(exclude target met with award)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] = "Target met with good performance(exclude target met only)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] = "Target not met";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipation'] = "Target not met(with participation)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEA'] = "Target not met(with participation in extracurricular activities)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEC'] = "Target not met(with participation in external competitions)";
$Lang['iPortfolio']['JinYing']['Statistics']['Class'] = "Class";
$Lang['iPortfolio']['JinYing']['Statistics']['Count'] = "Number";
$Lang['iPortfolio']['JinYing']['Statistics']['Form'] = "Form";
$Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'] = "High Secondary";
$Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'] = "Junior Secondary";
$Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Gold'] = "Gold Award";
$Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Silver'] = "Silver Award";
$Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Bronze']= "Bronze Award";
$Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['None']= "No Award";
$Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] = "Percentage";
$Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByReceive'] = "Percentage of award by class level";
$Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByScope'] = "Percentage of award by scope";
$Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByItem'] = "Percentage of award by item";
$Lang['iPortfolio']['JinYing']['Statistics']['Total'] = "Total";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningGoldLessSilverGoodPoint'] = "Gold award good point requirement should not be less than that of silver";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningGoldLessSilverTargetMet'] = "Gold award target met requirement should not be less than that of silver";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningSelectItem'] = "Please select item";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningSelectScope'] = "Please select scope";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningSilverLessBronzeGoodPoint'] = "Silver award good point requirement should not be less than that of bronze";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningSilverLessBronzeTargetMet'] = "Silver award target met requirement should not be less than that of bronze";
$Lang['iPortfolio']['JinYing']['Statistics']['WholeYear'] = "Whole Year";

$Lang['iPortfolio']['LivingHomeopathy']['PersonalProfile'] = "Personal Profile";


$Lang['iPortfolio']['ChiuChunKG']['SBS']['printVersion'] = "Print Version";
$Lang['iPortfolio']['ChiuChunKG']['SBS']['reportTitle'] = "Report Title";
$Lang['iPortfolio']['ChiuChunKG']['SBS']['someFieldNotAns'] = "Some question does not answered, continue submit?";
$Lang['iPortfolio']['ChiuChunKG']['SBS']['rowCount'] = "Rows per page";

$Lang['iPortfolio']['TWGHCZM']['StudentLearningProfile'] = 'Testimonial';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['0'] = 'General Academic Ability';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['1'] = 'Diligence';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['2'] = 'Initiative';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['3'] = 'Courtesy';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['4'] = 'Cooperation';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['5'] = 'Concern for others';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['6'] = 'Leadership';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['7'] = 'Punctuality';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['8'] = 'Attendance';
$Lang['iPortfolio']['twghczm']['MaxECASize'] = 'Max ECA Records';
$Lang['iPortfolio']['twghczm']['MaxPrizeSize'] = 'Max Special Achievement Records';

$Lang['iPortfolio']['DBSTranscript']['Title'] = 'Transcript';
$Lang['iPortfolio']['DBSTranscript']['ReportType'] = 'Report Type';
$Lang['iPortfolio']['DBSTranscript']['Transcript'] = 'Transcript';
$Lang['iPortfolio']['DBSTranscript']['PredictedGrade'] = 'Predicted Grades';
$Lang['iPortfolio']['DBSTranscript']['DisplayForm'] = 'Display Forms';
$Lang['iPortfolio']['DBSTranscript']['Curriculums'] = 'Curriculum';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Title'] = 'Curriculums Setting';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Code'] = 'Code';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameEn'] = 'Name (Eng)';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameCh'] = 'Name (Chi)';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['SubjectLevel'] = 'Subject Level';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Status'] = 'Status';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Title'] = 'Disable Transcript Printing';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['IssuedBy'] = 'Issued By';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Status'] = 'Status';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Locked'] = 'Locked';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Unlocked'] = 'Un-locked';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Title'] = 'Grading Scale for Pre-IB Subject';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['AllSubject'] = 'All subjects except Maths and Chinese';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['IBGrade'] = 'IB Grade';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Grade'] = 'Grade';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Math'] = 'IB G10 \'Pre-IB Mathematics\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathStudies'] = 'Progress to G11 \'Mathematical Studies\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathSL'] = 'Progress to G11 \'Mathematics SL\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathHL'] = 'Progress to G11 \'Mathematics HL\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Chinese'] = 'IB G10 \'Pre-IB Chinese\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseLit'] = 'Progress to G11 \'Chinese Literature\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseLangLit'] = 'Progress to G11 \'Chinese Language and Literature\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseB'] = 'Progress to G11 \'Chinese B\'';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Title'] = 'Image Management';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['BackCover'] = 'Back Cover';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Header'] = 'Header';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SchoolSeal'] = 'School Seal';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Signature'] = 'Signature';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Description'] = 'Description';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Image'] = 'Image';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SignatureName'] = 'Name';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SignatureDetails'] = 'Details';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Title'] = 'Predicted Grade';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Adjustment'] = 'Adjustment';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['MinGrade'] = 'Min. Grade';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['MaxGrade'] = 'Max. Grade';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ImportGrade'] = 'Import Grade';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['OriginalGrade'] = 'Original Grade';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment1'] = '1st Amendment';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment2'] = '2nd Amendment';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['FinalGrade'] = 'Final Adjustment Grade';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['NoChange'] = 'No Change';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentPeriodSetting'] = 'Adjustment Period Setting';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentStartDate'] = 'Adjustment Start Date';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentEndDate'] = 'Adjustment End Date';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ValidData'] = 'rows of valid data can be saved to system now.';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['InvalidData'] = 'No valid data can be saved';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ImportedData'] = 'rows of data is saved to system.';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['InvalidImport'] = 'Invalid data';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['Title'] = 'Print Report';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['TranscriptLeftSignature'] = 'Signature of transcript (left)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['TranscriptRightSignature'] = 'Signature of transcript (right)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['PredictedGradeLeftSignature'] = 'Signature of predicted grade report (left)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['PredictedGradeRightSignature'] = 'Signature of predicted grade report (right)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SelectStudent'] = 'Select Student(s)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SearchByStudentNameFormat'] = "Search by input format<br>\"[Name]\" (e.g. Chan)";
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SearchByStudentName'] = 'Search by student name';
$Lang['iPortfolio']['DBSTranscript']['StudentCurriculumsMapping']['Title'] = 'Student Curriculums Mapping';
$Lang['iPortfolio']['DBSTranscript']['SelectReportType'] = 'Select Report Type';
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['InputWarning'] = "Please Input ";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['SelectWarning'] = "Please Select ";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['DuplicateWarning'] = " duplicated";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PeriodCannotOverlap'] = "2 adjustment periods cannot be overlapped.";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRange'] = "Please enter valid form range.";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeIB'] = "(G9 - G12)";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeDBS'] = "(G7 - G12)";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeOthers'] = "(G7 - G13)";
$Lang['iPortfolio']['DBSTranscript']['RemarkArr']['GradeSaveAsDraft'] = "<span class=\"tabletextrequire\">^</span> Amendment is saved as draft.";

$Lang['iPortfolio']['bps']['MaxRecordsSize'] = 'Max Records';
###########################################################
if ($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile'] && is_file("$intranet_root/lang/iportfolio_custom/stpaulcoeduprischool_slp_lang.php"))
{
  include("$intranet_root/lang/iportfolio_custom/stpaulcoeduprischool_slp_lang.php");
}

if ($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'])
{
	include("$intranet_root/lang/iportfolio_custom/stpaulPAS_slp_lang_en.php");
}
if ($sys_custom['IPF_HIDE_COLUMN']['SIS'])
{
  include("$intranet_root/lang/iportfolio_custom/sis_en.php");
}
if (!$NoLangWordings && is_file("$intranet_root/templates/lang.$intranet_session_language.customized.php"))
{
  include("$intranet_root/templates/lang.$intranet_session_language.customized.php");
}
if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")||($sys_custom['project']['NCS'])){
	include($intranet_root."/ncs/lang.ncs.en.php");
}
####################### This should be placed at the bottom
?>
