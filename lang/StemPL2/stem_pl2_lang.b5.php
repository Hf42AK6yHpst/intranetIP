<?php
if ($sys_custom['StemPL2']) {
	$Lang['StemPL2']['Or'] = '或';
	$Lang['StemPL2']['Username'] = '使用者名稱';
	$Lang['StemPL2']['Password'] = '密碼';
	$Lang['StemPL2']['Login'] = '登入';
	$Lang['StemPL2']['Pin'] = 'PIN';
	$Lang['StemPL2']['PleaseEnterValidUsernameOrPassword'] = '請輸入正確的使用者名稱或密碼。';
	$Lang['StemPL2']['PleaseEnterValidPin'] = '請輸入正確的 PIN 碼。';
}
?>