<?php
# modifying : 

$Lang["DigitalArchive"]["DigitalArchiveName"] = "電子文件";
$Lang["DigitalArchive"]["SubjectReference"] = "學科電子資源";
$Lang["DigitalArchive"]["SubjectReferenceDoc"] = "參考文件";
$Lang["DigitalArchive"]["SchoolAdminDoc"] = "行政文件";
$Lang["DigitalArchive"]["GroupCategory"] = "小組類別";
$Lang["DigitalArchive"]["AdvanceSearch"] = "進階搜尋";
$Lang["DigitalArchive"]["Search"] = "搜尋";
$Lang["DigitalArchive"]["SearchFor"] = "搜尋";
$Lang["DigitalArchive"]["SearchResult"] = "搜尋結果";
$Lang["DigitalArchive"]["FileType"] = "檔案類別";
$Lang["DigitalArchive"]["Level"] = "班級";
$Lang["DigitalArchive"]["Subject"] = "科目";
$Lang["DigitalArchive"]["Subjects"] = "科目";
$Lang["DigitalArchive"]["Language"] = "語言";
$Lang["DigitalArchive"]["Document"] = "文件";
$Lang["DigitalArchive"]["Weblink"] = "網址";
$Lang["DigitalArchive"]["Richtext"] = "文字";
$Lang["DigitalArchive"]["Media"] = "多媒體";
$Lang["DigitalArchive"]["Lang_CH"] = "中文";
$Lang["DigitalArchive"]["Lang_EN"] = "英文";
$Lang["DigitalArchive"]["New"] = "新增";
$Lang["DigitalArchive"]["Lang_Others"] = "其他";
$Lang["DigitalArchive"]["List"] = "列表";
$Lang["DigitalArchive"]["NewFile"] = "新增檔案";
$Lang["DigitalArchive"]["Title"] = "題目";
$Lang["DigitalArchive"]["DisplayName"] = "顯示名稱";
$Lang["DigitalArchive"]["Description"] = "描述";
$Lang["DigitalArchive"]["Source"] = "來源";
$Lang["DigitalArchive"]["Tag"] = "標籤";
$Lang["DigitalArchive"]["CommonUseTag"] = "常用標籤";
$Lang["DigitalArchive"]["File"] = "檔案";
$Lang["DigitalArchive"]["Files"] = " 個檔案";
$Lang["DigitalArchive"]["Url"] = "網址";
$Lang["DigitalArchive"]["Max10Words"] = "最多10個標籤";
$Lang["DigitalArchive"]["Like"] = "讚";
$Lang["DigitalArchive"]["Unlike"] = "收回讚";
$Lang["DigitalArchive"]["UnlikeAll"] = "收回所有讚";
$Lang["DigitalArchive"]["UploadDate"] = "上載日期";
$Lang["DigitalArchive"]["FileName"] = "檔案名稱";
$Lang["DigitalArchive"]["Newest"] = "最新";
$Lang["DigitalArchive"]["AllForms"] = "所有級別";
$Lang["DigitalArchive"]["More"] = "更多";
$Lang["DigitalArchive"]["DaysAgo"] = "日前";
$Lang["DigitalArchive"]["People"] = "個人";
$Lang["DigitalArchive"]["ChooseLike"]  = "說讚。";
$Lang["DigitalArchive"]["AndSign"]  = " & ";
$Lang["DigitalArchive"]["You"]  = "你";
$Lang["DigitalArchive"]["MyLike"]  = "我說的讚";
$Lang["DigitalArchive"]["Subject"] = "科目";
$Lang["DigitalArchive"]["Favourite"] = "最受歡迎";
$Lang["DigitalArchive"]["AccessRightGroup"] = "權限小組";
$Lang["DigitalArchive"]["AccessRight"] = "存取權限";
$Lang["DigitalArchive"]["UserList"] = "成員列名單";
$Lang["DigitalArchive"]["GroupList"] = "小組列表";
$Lang["DigitalArchive"]["NewFolder"] = "新增文件夾";
$Lang["DigitalArchive"]["MyFavourite"] = "我的最愛";
$Lang["DigitalArchive"]["Directory"] = "文件目錄";
$Lang["DigitalArchive"]["Folder"] = "文件夾";
$Lang["DigitalArchive"]["FolderName"] = "文件夾名稱";
$Lang["DigitalArchive"]["CurrentFolder"] = "此文件夾";
$Lang["DigitalArchive"]["SubFolders1"] = "含 ";
$Lang["DigitalArchive"]["SubFolders2"] = " 個子文件夾";
$Lang["DigitalArchive"]["MoveTo"] = "移至";
$Lang["DigitalArchive"]["MoveFile"] = "移動檔案";
$Lang["DigitalArchive"]["ExtractFile"] = "檔案解壓";
$Lang["DigitalArchive"]["Action"] = "行動";
$Lang["DigitalArchive"]["FileInvolved"] = "相關檔案";
$Lang["DigitalArchive"]["SourceFolder"] = "檔案原位置";
$Lang["DigitalArchive"]["TargetFolder"] = "檔案目的地";
$Lang["DigitalArchive"]["IfFileExistInTargetFolder"] = "如檔案在目的地已存在";
$Lang["DigitalArchive"]["Rename"] = "重新命名";
$Lang["DigitalArchive"]["AllFolders"] = "所有文件夾";
$Lang["DigitalArchive"]["CreatedBy"] = "建立者";
$Lang["DigitalArchive"]["CreatorName"] = "建立者姓名";
$Lang["DigitalArchive"]["Size"] = "大小";
$Lang["DigitalArchive"]["Version"] = "版本";
$Lang['DigitalArchive']['Root'] = "最上層";
$Lang['DigitalArchive']['SystemAdmin'] = "系統管理";
$Lang['DigitalArchive']['Admin'] = "管理員";
$Lang['DigitalArchive']['GroupCode'] = "組別編號";
$Lang['DigitalArchive']['GroupMembers'] = "組別成員";
$Lang['DigitalArchive']['Member'] = "成員";
$Lang['DigitalArchive']['SetAsAdmin'] = "設定為管理員";
$Lang['DigitalArchive']['SetAsMember'] = "設定為成員";
$Lang['DigitalArchive']['MyGroups'] = "我的小組";
$Lang['DigitalArchive']['AccessRightSettings'] = "權限管理";
$Lang['DigitalArchive']['AdminSettings'] = "管理設定";+
$Lang['DigitalArchive']['Advanced'] = "進階";
$Lang['DigitalArchive']['MyGroup'] = "我的小組";
$Lang['DigitalArchive']['Latest'] = "最新文件";
$Lang['DigitalArchive']['Records'] = "個";
$Lang['DigitalArchive']['SortBy'] = "排序";
$Lang['DigitalArchive']['LastViewed'] = "上次瀏覽";
$Lang['DigitalArchive']['GroupCategoryCode'] = "類別編號";
$Lang['DigitalArchive']['GroupCategoryName'] = "類別名稱";
$Lang['DigitalArchive']['DisplayOrder'] = "顯示次序";
$Lang['DigitalArchive']['NewGroupCategory'] = "新增類別";
$Lang['DigitalArchive']['GroupTotal'] = "小組數目";
$Lang['DigitalArchive']['GroupName'] = "小組名稱";
$Lang['DigitalArchive']['NoOfMembers'] = "成員人數";
$Lang['DigitalArchive']['NewGroup'] = "新增組別";
$Lang['DigitalArchive']['OwnFiles'] = "自己的檔案";
$Lang['DigitalArchive']['OthersFiles'] = "其他成員的檔案";
$Lang['DigitalArchive']['FileIsDeleted_1'] = "檔案已於 ";
$Lang['DigitalArchive']['FileIsDeleted_2'] = " 被";
$Lang['DigitalArchive']['FileIsDeleted_3'] = "刪除";
$Lang['DigitalArchive']['OverwriteFile'] = "取代";
$Lang['DigitalArchive']['RenameFile'] = "重新命名";
$Lang['DigitalArchive']['CancelAll'] = "取消所有";
$Lang['DigitalArchive']['ExcludeDuplicateRecord'] = "不包括重複的檔案";
$Lang['DigitalArchive']['NoOfDoc'] = "檔案總數";
$Lang['DigitalArchive']['NoOfNewDoc'] = "新增檔案數量";
$Lang['DigitalArchive']['MemberList'] = "成員名單";
$Lang['DigitalArchive']['ChangeGroupCategorySuccess'] = "1|=|成功更改組別類別";
$Lang['DigitalArchive']['ChangeGroupCategoryUnsuccess'] = "0|=|更改組別類別失敗";
$Lang['DigitalArchive']['ChangeGroupCodeSuccess'] = "1|=|成功更改組別編號";
$Lang['DigitalArchive']['ChangeGroupCodeUnsuccess'] = "0|=|更改組別編號失敗";
$Lang['DigitalArchive']['CopyAccessRightGroupSuccess'] = "1|=|從小組中匯入成小組成功";
$Lang['DigitalArchive']['CopyAccessRightGroupUnsuccess'] = "0|=|從小組中匯入成小組失敗";
$Lang['DigitalArchive']['GroupNameDuplicateWarning'] = "* 小組名稱重複, 太長或空白";
$Lang['DigitalArchive']['InitializeFromGroup'] = "由小組作起始設定";
$Lang['DigitalArchive']['GroupCodeDuplicateWarning'] = "組別編號重複或空白";
$Lang['DigitalArchive']['Location'] = "位置";
$Lang['DigitalArchive']['ModifiedDate'] = "更新日期";
$Lang['DigitalArchive']['ASC'] = "順序";
$Lang['DigitalArchive']['DESC'] = "倒序";
$Lang['DigitalArchive']['NoFileInThisFolder'] = "文件夾內沒有檔案";
$Lang['DigitalArchive']['MoveLeft'] = "左移";
$Lang['DigitalArchive']['MoveRight'] = "右移";
$Lang['DigitalArchive']['Keywords'] = "關鍵字";
$Lang['DigitalArchive']['Unclassified'] = "未分類之小組";
$Lang['DigitalArchive']['LastUpload'] = "最後上載於";
$Lang['DigitalArchive']['AllCategories'] = "所有類別";
$Lang['DigitalArchive']['NoFileInside'] = "未有文件檔案";
$Lang['DigitalArchive']['NoOfFolders'] = "文件夾數目";
$Lang['DigitalArchive']['NoOfFiles'] = "文件數目";
$Lang['DigitalArchive']['SpaceUsed'] = "佔用空間";
$Lang['DigitalArchive']['ReportUsage'] = "使用報告";
$Lang['DigitalArchive']['TotalAmount'] = "總計";
$Lang['DigitalArchive']['SelectFiles'] = "選取文件";
$Lang['DigitalArchive']['SelectFile'] = "選擇文件";
$Lang['DigitalArchive']['OrDragAndDropFilesHere'] = "或者在此拖放文件。";
$Lang['DigitalArchive']['MaxFileSize'] = "單一檔案上限";
$Lang['DigitalArchive']['FileFormatSettings'] = "類型限制";
$Lang['DigitalArchive']['FileFormat'] = "檔案類型";
$Lang['DigitalArchive']['ListofFileFormats'] = "檔案格式列表";
$Lang['DigitalArchive']['ListofFileFormatsRemark'] = "(每行只可輸入一個檔案副檔名。)";
$Lang['DigitalArchive']['MaximumFileSizeRemark'] = "(請輸入正數限制檔案大小，\"0\"代表不限制。)";
$Lang['DigitalArchive']['NewFileFormatSettings'] = "增加檔案類型";
$Lang['DigitalArchive']['UploadPolicy'] = "檔案上載規範 ";
$Lang['DigitalArchive']['UploadNotice'] = "上載提示";
$Lang['DigitalArchive']['UploadNoticeRemark'] = "(每行輸入一項指引。)";
$Lang['DigitalArchive']['UploadInstruction'] = "上載提示";
$Lang['DigitalArchive']['CollapseAll'] = "全部收起";
$Lang['DigitalArchive']['ExpandAll'] = "全部打開";
$Lang['DigitalArchive']['FolderTree'] = "資料夾樹狀目錄";
$Lang['DigitalArchive']['Icon'] = "圖示";
$Lang['DigitalArchive']['FileInfo'] = "檔案資料";
$Lang['DigitalArchive']['QuickViewFolders'] = "快速預覽資料夾";
$Lang['DigitalArchive']['OpenFolder'] = "開啟資料夾";
$Lang['DigitalArchive']['NoSubfolder'] = "這群組內沒有資料夾。";
$Lang['DigitalArchive']['Rating'] = "評分";
$Lang['DigitalArchive']['ReviewNow'] = "即時評分";
$Lang['DigitalArchive']['MyReview'] = "我已評分";
$Lang['DigitalArchive']['View'] = "瀏覽";
$Lang['DigitalArchive']['PeopleLikeThis'] = "人說讚。";
$Lang['DigitalArchive']['PeopleViewedThis'] = "人已瀏覽。";
$Lang['DigitalArchive']['Average'] = "平均";
$Lang['DigitalArchive']['ViewStatistics'] = "檢視統計";
$Lang['DigitalArchive']['TotalReviews'] = "總人數";
$Lang['DigitalArchive']['NoOfReview'] = "人數";
$Lang['DigitalArchive']['AverageRating'] = "平均評分";
$Lang['DigitalArchive']['CampusTV'] = "校園電視";
$Lang['DigitalArchive']['Channels'] = "頻道";
$Lang['DigitalArchive']['Clips'] = "節目";
$Lang['DigitalArchive']['SelectProgram']="選擇節目";
$Lang['DigitalArchive']['ForlderTag']="使用文件夾作為標籤";
$Lang['DigitalArchive']['SearchHelp']="<ol><li>當上傳檔案時，系統會自動把當前位置前的文件夾作標籤；</li><li>若想上傳的新檔案沿用現有檔案的標籤，則先選取了該檔案，然後才按「上傳檔案」；</li><li>除了使用界面上載多個檔案外，亦可以使用 ZIP 檔（並可用 CSV 定義各檔的標籤）上載，然後解壓出來。可參考範例 (<a class='tablelink' href='db_upload_by_zip_sample.zip' target='_self'>按此下載範例</a>)；</li></ol>";
$Lang['DigitalArchive']['AllMatches']="全部符合";
$Lang['DigitalArchive']['PartialMatches']="部分符合";
$Lang['DigitalArchive']['TagInput']="(請用[Enter]鍵新增分隔。)";
$Lang['DigitalArchive']['AccessSetting'] = '訪問設定';
$Lang['DigitalArchive']['AccessSettingDescription'] = '首次進入時需要進行登入認證';
$Lang['DigitalArchive']['Authentication'] = '登入認證';
$Lang['DigitalArchive']['AuthenticationInstruction'] = '請輸入用戶密碼進行登入認證。';
$Lang['DigitalArchive']['AllowIPAddresses'] = '限制允許存取的IP地址';
$Lang['DigitalArchive']['InvalidIPAddress'] = 'IP地址不正確';
$Lang['DigitalArchive']['AllowIPAddressesRemark'] = '你可以輸入允許存取位於此文件夾內的文件和子文件夾的IP地址，以限制不合法的存取行為。<br />';
$Lang['DigitalArchive']['AllowIPAddressesRemark'].= '你可以輸入以下格式的IP地址: ';
$Lang['DigitalArchive']['AllowIPAddressesRemark'].= '<ol><li>固定 IP 位址 (e.g. 192.168.0.101)</li><li>IP 位址一個範圍 (e.g. 192.168.0.[10-100])</li><li>CISCO Style 範圍(e.g. 192.168.0.0/24)</li><li>容許所有位址 (輸入 0.0.0.0)</li></ol>';
$Lang['DigitalArchive']['AllowIPAddressesRemark'].= '留空表示不限制IP地址。<br />';
$Lang['DigitalArchive']['AllowIPAddressesRemark'].= '請每行輸入一個IP地址。<br />';
$Lang['DigitalArchive']['AllowIPAddressesRemark'].= '你現在的IP地址是 <!--IP-->';
$Lang['DigitalArchive']['ForbiddenToDownload'] = '該文件不允許於你所在的位置下載。';
$Lang['DigitalArchive']['RedirectBackInstruction'] = '此頁面會自動返回上一頁，如未能自動返回，請按 <a href="<!--LINK-->">這裡</a> 立即返回上一頁。';
$Lang['DigitalArchive']['NoAvailableGroup'] = "沒有可使用的小組。";

# Javascript Message
$Lang['DigitalArchive']['jsDeleteGroup'] = "你是否確定要刪除小組？";
$Lang['DigitalArchive']['jsDeleteAlertMsg'] = "你是否確定要刪除已選的項目？";
$Lang['DigitalArchive']['jsMoveAlertMsg'] = "你是否確定移動已選項目？";
$Lang['DigitalArchive']['jsDisplayNameCannotWithSpecialCharacters'] = "顯示名稱不可含有以下字元  :<>\\\/\|\*\?\"";
$Lang['DigitalArchive']['jsFolderNameCannotWithSpecialCharacters'] = "文件夾名稱不可含有以下字元  :<>\\\/\|\*\?\"";
$Lang['DigitalArchive']['jsFileNameCannotWithSpecialCharacters'] = "檔案名稱不可含有以下字元  :<>\\\/\|\*\?\"";
$Lang['DigitalArchive']['jsDuplicatedTitleOrFileName'] = "顯示名稱重複了，你確定要取代現有檔案？";
$Lang['DigitalArchive']['jsFileNameInvalid'] = "檔案名稱格式不正確";
$Lang['DigitalArchive']['jsCancelAllLikeRecord'] = "你是否確定要取消所有的「讚」？";
$Lang['DigitalArchive']['jsIsDuplicated'] = "已經存在";
$Lang['DigitalArchive']['jsCannotCopyToCurrentFolder'] = "檔案不能移至原有的文件夾";
$Lang['DigitalArchive']['jsNoAccessRightToManage'] = "你沒有權限管理部份文件";
$Lang['DigitalArchive']['jsCopyFromGroupMsg'] = "你是否確定要由小組中複製成權限小組？";
$Lang['DigitalArchive']['jsUpdateTag'] = "你是否確定套用新標籤至相關文件中？";
$Lang['DigitalArchive']['FileNameWarning'] = "檔案名稱不可超過127個字元";
$Lang['DigitalArchive']['FolderNameWarning'] = "資料夾名稱不可超過127個字元";
$Lang['DigitalArchive']['jsDuplicatedOthersFileName'] = "顯示名稱與其他用戶的文件重複了，你沒有管理權限取代其他用戶的文件。";
$Lang['DigitalArchive']['NoGroupCode'] = "請輸入類別編號";
$Lang['DigitalArchive']['NoGroupName'] = "請輸入類別名稱";
$Lang['DigitalArchive']['NoDisplayOrder'] = "請輸顯示次序";
$Lang['DigitalArchive']['jsDeleteCategory'] = "你是否確定要刪除小組類別？";
$Lang['DigitalArchive']['GroupCodeAlreadyExists'] = "類別編號已經存在";
$Lang['DigitalArchive']['PleaseFillIn'] = "請輸入";
$Lang['DigitalArchive']['jsSelectFiles'] = "請選取文件。";
$Lang['DigitalArchive']['jsWaitAllFilesUploaded'] = "請等待直至所有文件完成上傳。";
$Lang['DigitalArchive']['jsContentWarning'] = "內容不可超過127個字元";
$Lang['DigitalArchive']['jsFileSizeExceedLimit'] = "[<!--FILENAME-->] 文件大小不能超過<!--SIZE-->MB。";
$Lang['DigitalArchive']['jsFileTypeNotAllowed'] = "[<!--FILENAME-->] 不允許上傳<!--EXT-->類型的文件。";
$Lang['DigitalArchive']['jsZipFileContainRestrictedFileType'] = "[<!--FILENAME-->] 壓縮檔含有被禁止的文件類型: <!--EXT-->。";
$Lang['DigitalArchive']['jsDuplicatedOtherUserFile'] = "[<!--FILENAME-->] 與其他用戶的文件重複了，你沒有管理權限取代其他用戶的文件。";
$Lang['DigitalArchive']['jsFileFormatWarning'] = "副檔名只可包含英文字母及數字。";
$Lang['DigitalArchive']['HaveNotAssignedToSubjectGroup'] = "尚未加入任何科組";
$Lang['DigitalArchive']['HaveNotAssignedToForm'] = "尚未編排到任何班級";
$Lang['DigitalArchive']['PleaseSelectAtLeastOneProgram'] = "請至少選擇一個節目。";
$Lang['DigitalArchive']['jsMaxFileSizeWarning'] = "每個文件大小不能超過 <!--SIZE-->MB。";

?>