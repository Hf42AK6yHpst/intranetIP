<?php
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/


// common words
//$iPort['iPortfolio'] = "iPortfolio 電子學習檔案";
$iPort['iPortfolio'] = "iPortfolio";
$iPort["submitted"] = "已呈交";
$iPort["content"] = "內容";
$iPort["scheme_list"] = "計劃列表";
$iPort["new_phase"] = "新增階段";
$iPort["new_phase_for"] = "新增階段 ";
$iPort["edit_phase"] = "編輯階段";
$iPort["new_scheme"] = "新增計劃";
$iPort["edit_scheme"] = "編輯計劃";
$iPort["start"] = "開始";
$iPort["end"] = "結束";
$iPort["title"] = "標題";
$iPort["description"] = "簡介";
$iPort["participating_group"] = "參與小組";
$iPort["choose_group"] = "選擇小組";
$iPort["selected_groups"] = "已選擇小組";
$iPort["status"] = "狀況";
$iPort["activate"] = "啟用";
$iPort["deactivate"] = "停用";
$iPort["participants"] = "參加者";
$iPort["subject"] = "Subject";
$iPort["contents"] = "內容";
$iPort["answer_display_mode"] = "答案顯示模式";
$iPort["display_answer_only"] = "只顯示答案";
$iPort["display_answers_and_choices"] = "顯示問題及答案";
$iPort["phase_title"] = "階段名稱";
$iPort["related_phase"] = "相關階段";
$iPort["choose_phase"] = "選擇階段";
$iPort["selected_phase"] = "已選擇階段";
$iPort["create_form"] = "建立表格";
$iPort["form_display_v"] = "問題格式";
$iPort["form_display_h"] = "簡潔格式";
$iPort["informatics"] = "資料性";
$iPort["form"] = "表格";
$iPort['enter_scheme_name'] = "輸入計劃名稱";
$iPort["all_school_years"] = "全部年度";
$iPort["last_updated"] = "最近更新";
$iPort["phase"] = "階段";
$iPort["finished"] = "已完成";
$iPort["period"] = "時段";
$iPort["ele"] = "其他學習經歷範疇";
$iPort["student_submission_period"] = "提交時段";
$iPort["student_period_settings"] = "時段設定";
$iPort["record_approval"] = "紀錄審核";
$iPort["new"] = "新增";
$iPort["options_select"] = "請選擇";
$iPort["start_time"] = "開始時間";
$iPort["end_time"] = "結束時間";
$iPort["display"]= "顯示";
$iPort["additionalInformation"] = "附加資料";
$iPort["order"] = "次序";
$iPort["report_section_component"] = "Report Section / Component";
$iPort["selected_subject"] = "已選擇科目";
$iPort["choose_subject"] = "未選擇科目";
$iPort["submitted_list"] = "已提交列表";
$iPort["students_submitted"] = "學生已提交";
$iPort["of"] = "/";
$iPort['student_name'] = "學生姓名";
$iPort['class'] = "班別";
$iPort['class_no'] = "學號";
$iPort['submitted_date'] = "提交日期";
$iPort["upload"] = "上載";
$iPort['OLE_Upload_File'] = "上載檔案";
$iPort["upload_file"] = "上載檔案";
$iPort["activity_title_template"] = "預設學習項目名稱";
$iPort["default_ele"] = "預設其他學習經歷範疇";
$iPort["activity_role_template"] = "預設參與角色";
$iPort["activity_awards_template"] = "預設 獎項 / 証書文憑 / 成就";
$iPort["category"] = "學習項目類別";
$iPort["tag"] = "項目增補類別";
$iPort["print_result"] = "列印結果";
$iPort["export_result"] = "匯出結果";
$iPort["upload_date"] = "上載日期";
$iPort["file_list"] = "檔案管理";
$iPort["upload_files"] = "上載檔案";
$iPort["related_to"] = "相關於";
$iPort["allow_to_submit"] = "允許提交";
$iPort["internal_record"] = "其他學習經歷";
$iPort["internal_record_submision_period"] = "其他學習經歷 (提交時段)";
$iPort["external_record"] = "校外表現/獎項及重要參與";
$iPort["external_recordSetPerios"] = "校外表現/獎項及重要參與 (提交時段)";
$iPort["OLESetSLPRecord"] = "設定SLP報告紀錄時段 (包括 '".$iPort["internal_record"]."' 及 '".$iPort["external_record"]."')";
$iPort['submission_type'] = "提交類型";
$iPort["select_default_sa"] = "更改預設學生自述";
$iPort["select_default_sa_teacher"] = "設定為預設學生自述";
$iPort["selected_as_defaultSelfAccount"] = "<font color=\"blue\">此為預設學生自述</font>";
$iPort['to'] = "至";
$iPort['period_start_end'] = "提交時段由";
$iPort['period_start'] = "提交時段開始時間:";
$iPort['period_end'] = "提交時段結束時間:";
$iPort['SLPperiod_start_end'] = "設定報告用時段由";
$iPort['SLPperiod_start'] = "設定報告用時段開始時間:";
$iPort['SLPperiod_end'] = "設定報告用時段結束時間:";
$iPort['no_of_program'] = "項目總數";
$iPort['sub_total'] = "總數";
$iPort["last_submission"] = "最後提交";
$iPort["lp_template"] = "檔案樣式";
$iPort["lp_skin"] = "網站樣式";
$iPort["report_section"] = "報告章節";
$iPort["component"] = "元件";
$iPort["share_to"] = "分享給";
$iPort["pts"] = "分";
$iPort["stat_by"] = "按<!--base-->統計";
$iPort['attachments'] = "附件";
$iPort['loading'] = "運作中";
$iPort['MergeProgramme'] = "合併項目";
$iPort['ApprovalNeeded'] = "需要批核";
$iPort['NoApprovalNeeded'] = "不用批核";
$iPort['RecordApproval_ClassTeacher'] = "班主任擁有批核權";
$iPort["Item"] = "項目";
$iPort["Award"] = "獎項";
$iPort["Post"] = "職位";
$iPort["Export"] = "匯出";
$iPort["Selected"] = "已選擇";
$iPort["All"] = "全部";
$iPort["Type"] = "類別";
$iPort["ReportPeriod"] = "報告時間";
$iPort["CurrentStudent"] = "現時學生";
$iPort["Class"] = "班別";
$iPort["Subject"] = "科目";
$iPort["SubjectGroup"] = "科組";
$iPort["compareScore"] = "成績比對";
$iPort["zScoreImprovementPercent"] = "標準分有進步百分比";
$iPort["scoreImprovementPercent"] = "分數增減百分比";
$iPort["FormClassSubjectGroup"] = "級別 / 班別 / 科組";
$iPort["Teacher"] = "任教老師";
$iPort["Personal_character_record_submision_period"] = "班主任提交時段";

#### AdminConsole INTRANET_SEN_ITEM
$iPort['filter']['LAA']='成績稍遜';
$iPort['filter']['SLD']='特殊學習困難';
$iPort['filter']['ID']='智障';
$iPort['filter']['VI']='視障';
$iPort['filter']['HI']='聽障';
$iPort['filter']['PD']='肢體傷殘';
$iPort['filter']['ASD']='自閉症';
$iPort['filter']['ADHD']='注意力不足/過度活躍症';
$iPort['filter']['SLI']='言語障礙';
$iPort['filter']['GF']='資優';

$iPort['Gender'] = "性別";
$iPort['GenderM'] = "男";
$iPort['GenderF'] = "女";
$iPort['NonChinese'] = "非華語學生";
$iPort['ComeFromChina'] = "內地新來港學生";
$iPort['CrossBoundary'] = "跨境學生";
$iPort['NotLimit'] = "不限";
$iPort['AdvancedSearch'] = '進階搜尋';

$iPort["ClassComparison"] = "班別比較";
$iPort["SubjectComparison"] = "科目比較";
$iPort["AvgerageMarks"] = "平均分";

$iPort["AvgerageMarkOf"] = "<!-- Name --> 平均分";
$iPort["MEAN"] = "平均分";
$iPort["SD"] = "標準差";
$iPort["HighestMark"] = "最高分";
$iPort["LowestMark"] = "最低分";
$ec_iPortfolio['AllowStudentsToJoin'] = "容許學生參加";



// menu
$iPort['menu']['my_information'] = "我的資料";
$iPort['menu']['school_records'] = "學校紀錄";
$iPort['menu']['personal_attributes'] = "個人特質";
$iPort['menu']['academic_points'] = "卓越學業表現";
$iPort['menu']['olr'] = "OLR";
$iPort['menu']['ole'] = "其他學習經歷";  // <-- change form "學生學習概覽" 20110418
$iPort['menu']['slp'] = "學生學習概覽";
$iPort['menu']['self_account'] = "學生自述";
$iPort['menu']['teacher_comment'] = "老師評語";
$iPort['menu']['audit_student_report'] = "Student Audit Report";
$iPort['menu']['audit_student'] = "Student Audit";
$iPort['menu']['learning_portfolio'] = "學習檔案";
$iPort['menu']['school_based_scheme'] = (!function_exists("iportfolio_getSBSTitle") || !isset($ck_course_id) || iportfolio_getSBSTitle($ck_course_id) == "") ? "校本計劃" : iportfolio_getSBSTitle($ck_course_id);
$iPort['menu']['peer_learning_portfolio'] = "朋輩的學習檔案";
$iPort['menu']['student_account'] = "個人帳戶";
$iPort['menu']['student_report_printing'] = "個人報告";
$iPort['menu']['dynReport'] = "自訂報告";
$iPort['menu']['alumniReport'] = "校友報告";
$iPort['menu']['olr_report'] = "其他學習經歷報告";
$iPort['menu']['ole_report'] = "其他學習經歷報告";
$iPort['menu']['assessment_statistic_report'] = "評估統計報告";
$iPort['menu']['prepare_cd_rom_burning'] = "預備光碟";
$iPort['menu']['download_cd_rom_burning'] = "下載光碟";
$iPort['menu']['reports'] = "報告";
$iPort['menu']['management'] = "管理";
$iPort['menu']['data_handling'] = "資料處理";
$iPort['menu']['settings'] = "設定";
$iPort['menu']['addon_module'] = "增值模組";
$iPort['menu']['update_info_eclass_websams'] = "資料更新 (eClass/WEBSAMS)";
$iPort['menu']['data_export'] = "資料匯出";
$iPort['menu']['eReportCard'] = "成績表";
$iPort['menu']['Import']['Programme'] = "項目";
$iPort['menu']['Import']['ProgrammeWithStudent'] = "項目及學生紀錄";
$iPort['menu']['assessment_report'] = "評估報告";
$iPort['menu']['third_party'] = "第三方報告";
// table display
$iPort['table']['records'] = "紀錄";

// button
$iPort['btn']['search'] = "搜尋";
$iPort["btn"]["submit"] = "呈交";
$iPort["btn"]["cancel"] = "取消";
$iPort["btn"]["edit"] = "編輯";
$iPort["btn"]["delete"] = "刪除";
$iPort["btn"]["activate"] = "啟用";
$iPort["btn"]["deactivate"] = "停用";
$iPort["btn"]["copy"] = "Copy";
$iPort["btn"]["reset"] = "重設";
$iPort["btn"]["save"] = "儲存";
$iPort["btn"]["create_phase_now"] = "立即建立階段";
$iPort["btn"]["back_to_scheme_list"] = "返回計劃列表";
$iPort["btn"]["submit_and_create_phase"] = "呈送後另新增";
$iPort["btn"]["submit_and_finish"] = "呈送";
$iPort["btn"]["print"] = "列印";
$iPort["btn"]["redo"] = "發還重做";
$iPort["btn"]["import_student_info"] = "匯入學生資料";
$iPort["btn"]["import_another"] = "匯入其他檔案";
$iPort["btn"]["ole_finish"] = "完成";

// alert message
$iPort["msg"]["group_no_selected"] = "如不選擇小組，則計劃將公開予全部學生。";
$iPort["msg"]["fields_with_asterisk_are_required"] = $i_general_required_field;
$iPort["msg"]["record_add"] = "已增加紀錄";
$iPort["msg"]["record_update"] = "已更新紀錄";
$iPort["msg"]["record_delete"] = "已刪除紀錄";
$iPort["msg"]["confirm_delete"] = "你是否確認刪除?";
$iPort["msg"]["confirm_activate"] = "你是否確定啟用?";
$iPort["msg"]["confirm_deactivate"] = "你是否確定暫停使用?";
$iPort["msg"]["confirm_make_change"] = "系統將進行以下變更：";
$iPort["msg"]["start_time"] = "如設定開始時間，學生將只能於該段時間後提交其他學習經歷紀錄。";
$iPort["msg"]["end_time"] = "如設定結束時間，學生將只能於該段時間前提交其他學習經歷紀錄。";
$iPort["msg"]["SetSLPRecord_start_time"] = "如設定開始時間，學生將只能於該段時間後設定SLP報告紀錄。";
$iPort["msg"]["SetSLPRecord_end_time"] = "如設定結束時間，學生將只能於該段時間前設定SLP報告紀錄。";
$iPort["msg"]["start_time_self_account"] = "如設定開始時間，學生將只能於該段時間後提交學生自述。";
$iPort["msg"]["end_time_self_account"] = "如設定結束時間，學生將只能於該段時間前提交學生自述。";
$iPort["msg"]["start_time_teacher_comment"] = "如設定開始時間，老師將只能於該段時間後提交導師評語。";
$iPort["msg"]["end_time_teacher_comment"] = "如設定結束時間，老師將只能於該段時間前提交導師評語。";
$iPort["msg"]["warning_eng_title"] = "請輸入英文標題";
$iPort["msg"]["warning_chi_title"] = "請輸入中文標題";
$iPort["msg"]["select_submit_type"] = "請選擇至少一種提交類別";
$iPort["msg"]["batchEditWarning"] = "你只能夠批次編輯相同標題的項目。";
$iPort["msg"]["selectOneItem"] = "請最少勾選一個選項。";
$iPort["msg"]["InputOneItem"] = "請最少輸入一個選項。";
$iPort["alert"]["this_phase_is_filled_by"] = "這階段是由";
$iPort["alert"]["filled_in"] = "填寫, ";
$iPort["alert"]["no_right_to_do"] = " 你沒有權限填寫或覲看已填寫內容";
$iPort["alert"]["only_preview"] = " 由於此階段正在進行中, 你只可以預覽內容";

$iPort["report_col"]["total_student"] = "學生人數";
$iPort["report_col"]["total_attempt"] = "應考人數";
$iPort["report_col"]["total_pass"] = "合格人數";
$iPort["report_col"]["passing_rate"] = "合格率";
$iPort["report_col"]["full_mark"] = "滿分";
$iPort["report_col"]["average"] = "平均分";
$iPort["report_col"]["highest_mark"] = "最高分";
$iPort["report_col"]["lowest_mark"] = "最低分";
$iPort["report_col"]["SD"] = "標準差";
$iPort["report_col"]["term"] = "學期";

$iPort["report_col"]["form_class_group"] = "級/班/組";
$iPort["report_col"]["type"] = "類別";
$iPort["report_col"]["type_form"] = "級";
$iPort["report_col"]["type_class"] = "班";
$iPort["report_col"]["type_group"] = "組";
$iPort["report_col"]["subject"] = "科目";
$iPort["report_col"]["teacher"] = "任教老師";
$iPort["report_col"]["compare"] = "比較";
$iPort["report_col"]["attempted_both"] = "兩次皆有應考人數";
$iPort["report_col"]["improved_by_ss"] = "標準分有進步人數";
$iPort["report_col"]["improved_by_percentage"] = "百分比";


// User Type
$iPort['usertype_t'] = "教師";
$iPort['usertype_s'] = "學生";
$iPort['usertype_a'] = "教學助理";
$iPort['usertype_g'] = "訪客";
$iPort['usertype_p'] = "家長";
$iPort['usertype_ct'] = "班主任";
$iPort['usertype_st'] = "科目老師";
$iPort['usertype_admin'] = "系統管理員";

$iPort["ole_report"]["programmes_with_description"] = "活動項目(及簡介)";
$iPort["ole_report"]["selected_records"] = " - 經選擇後的項目";
$iPort["ole_report"]["school_year"] = "學年";
$iPort["ole_report"]["role_of_participation"] = "參與角色";
$iPort["ole_report"]["partner_organizations_if_any"] = "合辦機構 (如有)";
$iPort["ole_report"]["essential_learning_experiences"] = "其他學習經歷範疇";
$iPort["ole_report"]["achievements_if_any"] = "獎項 / 証書文憑 / 成就* (如有)";
$iPort["ole_report"]["other_learning_experiences"] = "其他學習經歷";
$iPort["ole_report"]["table_description"] = "除核心及選修科目外，學生在高中階段，所參與由學校舉辦或與校外機構合辦的學習活動，當中包括德育及公民教育、藝術發展、體育發展、社會服務及與工作有關的經驗。學校已確認有關資料。";
$iPort["ole_report"]["required_remark"] = "有需要時可提供 獎項 / 証書文憑 / 成就 作証明";

$iPort["external_ole_report"]["performance"] = "校外表現 / 獎項及重要參與";
$iPort["external_ole_report"]["table_description"] = "學生可向學校提供一些在高中階段曾參與過而並非由學校舉辦的學習活動資料。學校不須確認學生的參與資料。在有需要時，學生須負全責向相關人仕提供適當証明。";
$iPort["external_ole_report"]["organization"] = "合辦機構";

///////////////////////// Copy From KIS ///////////////////////////////////////
#Assessment Report
$iPort['Assessment'] = array();
$iPort['Assessment']['NewAssessment'] = "新增評估";
$iPort['Assessment']['Assessment'] = "評估";
$iPort['Assessment']['EditAssessment'] = "編輯評估";
$iPort['Assessment']['Title'] = "標題";
$iPort['Assessment']['ReleaseDate'] = "開放日期";
$iPort['Assessment']['AllClasses'] = "所有班別";
$iPort['Assessment']['Target'] = "對象";
$iPort['Assessment']['ReleaseStatus'][1] = "已開放";
$iPort['Assessment']['ReleaseStatus'][2] = "未開放";
$iPort['Assessment']['UploadStatus'][1] = "已上載";
$iPort['Assessment']['UploadStatus'][2] = "未上載";
$iPort['Assessment']['Search'] = "搜尋....";
$iPort['Assessment']['NoOfUploads'] = "上載總數 / 學生總數";
$iPort['Assessment']['AssessmentList'] = "評估列表";
$iPort['Assessment']['AssessmentFile'] = "評估檔案";
$iPort['Assessment']['AssessmentTitle'] = "評估標題";
$iPort['Assessment']['UploadedDate'] = "上載日期";
$iPort['Assessment']['EditBy'][0] = "由";
$iPort['Assessment']['EditBy'][1] = "上載";
$iPort['Assessment']['Upload'] = "上載";
$iPort['Assessment']['LastUpdated'] = "最後更新時間";
$iPort['Assessment']['Error']['ReleaseDate'] = "請輸入開放日期";

///////////////////////// Below Copy from eclass 30 ///////////////////////////

$ec_form_word['fill_in_method'] = "填寫方式";
$ec_form_word['growth_scheme_form'] = "建立表格";
$ec_form_word['confirm_to_template'] = "Confirm the changed template?";
$ec_form_word['answersheet_tf'] = "是非題";
$ec_form_word['answersheet_mc'] = "多項選擇題";
$ec_form_word['answersheet_mo'] = "允許多於一個選項";
$ec_form_word['answersheet_sq1'] = "Fill-in (Short)";
$ec_form_word['answersheet_sq2'] = "Fill-in (Long)";
$ec_form_word['answersheet_not_applicable'] = "不適用";
$ec_form_word['answersheet_template'] = "模板";
$ec_form_word['move_up_item'] = "上移";
$ec_form_word['move_down_item'] = "下移";
$ec_form_word['delete_item'] = "刪除項目";
$ec_form_word['rename_item'] = "Rename item/description";
$ec_form_word['change_total'] = "Change question total no.";
$ec_form_word['rename_item_partA'] = "Rename item/description, from '";
$ec_form_word['rename_item_partB'] = "'Change to be:";
$ec_form_word['change_total_partA'] = "Change question total number, from '";
$ec_form_word['change_total_partB'] = "'Change to be";
$ec_form_word['question_title']="Topic/Title";
$ec_form_word['no_options_for'] = "無需選擇選項數目!";
$ec_form_word['fill_in_type'] = "Please select a method!";
$ec_form_word['fill_in_content'] = "請輸入內容!";
$ec_form_word['change_heading'] = "change topic/title:";
$ec_form_word['no_need_input']="不適用";
$ec_form_word['total_submission'] = "已呈交";
$ec_form_word['answersheet_old'] = "Question Excluded";
$ec_form_word['answersheet_new'] = "Question Included";
$ec_form_word['uncheck_option'] = "反選全部項目";
$ec_form_word['fill_by_teacher'] = "由教師填寫";
$ec_form_word['answersheet_ls'] = "Likert Scale";
$ec_form_word['answersheet_tl'] = "Table-like Input Format";
$ec_form_word['answersheet_no_ques'] = "# questions";
$ec_form_word['answersheet_no_select_ques'] = "No need to select number of questions";
$ec_form_word['question_scale'] = "[Ques./Scal.]";
$ec_form_word['question_question'] = "[Ques./Ques.]";

// Student - Course Work Assignments
$automarking="自動批改";
$answer_sheet="答題紙";
$answersheet_header="Section Header";
$answersheet_no="問題數目";
$answersheet_order="Order Type";
$answersheet_type="問題類型";
$answersheet_mark="每題得分";
$answer_doit="開始!";
$answer_modifyit="Modify your work!";
$answer_attached="Attached answer";
$answer_sum="Sum";
$answersheet_tf="是非題";
$answersheet_mc="多項選擇題";
$answersheet_mo="Multiple Options";
$answersheet_sq="短問題";
$answersheet_lq="長問題";
$answersheet_likert="Likert Score";
$answersheet_option="# options";
$answersheet_maxno="Number of question exceeds the limit: 'a - z' for alphabet; 'i - x' for Roman index!";
$legend_desc1="與上一項目對調位置";
$legend_desc2="與下一項目對調位置";
$legend_desc3="刪除";
$legend_desc4="change the section header";
$legend_desc5="更改問題數目";
$last_submitted = "最近呈交";
$last_modified = "最近更新";
$assignment_remark = "備註";
$assignment_done="完成";
$assignment_save="儲存";
$assignment_status_msg="批改完畢，不可再修改。";
$work_list_not_submit = "尚未呈交學生";
$work_all = "All work";
$assignments_subject = "Subject";
$assignments_to = "To";
$assignments_deadline = "提交期限";
$assignments_title = "Title";
$assignments_status = "狀態";
$assignments_handin = "已呈交";
$assignments_handin_history = "呈交紀錄";
$assignments_submission = "呈交";
$assignments_instruction = "指示";
$assignments_attachment = "附件";
$assignments_publish = "公開";
$assignments_pending = "加密";
$assignments_email = "傳送電郵予全部用戶";
$assignments_alert = "電郵提示";
$assignments_sbj_redo = "重做";
$assignments_msg = "尚未有習作";
$assignments_msg2 = "找不到檔案";
$assignments_msg3 = "尚未有已呈交的習作。";
$assignments_files = "檔案";
$assignments_name = "習作名稱";
$assignments_size = "Size";
$assignments_campusmail = "傳送內部電郵予全部用戶。";
$assignment_overall_stat = "整體分析";
$assignment_public_overall_stat = "公開整體分析";
$assignment_not_public_msg = "尚未公開";
$assignment_public_mark = "公開評級/分數";
$assignment_public_exercise = "公開練習";
$assignment_submitted_exercise = "呈交練習";
$assignment_public_guide1= "此分析不適用於以實行評級制的習作";
$assignment_view_result = "檢視成績";
$assignment_stat_sd = "Standard Deviation";
$assignment_stat_mean = "Average Score";
$assignment_stat_lowest = "Lowest Score";
$assignment_stat_highest = "Highest Score";
$assignments_lastmodi = "最近修改";
$assignments_datesubmit = "呈交日期";
$assignments_student = "學生";
$assignments_file = "習作";
$assignments_answer = "Teacher Answer";
$assignments_grade = "評級/評分";
$assignments_late = "遲交";
$assignments_msg4 = "請輸入評級。你可按瀏覽上載答案。";
$assignments_msg5 = "以下學生尚未呈交習作，你可以發送電郵通知。";
$assignments_msg6 = "已更新答題紙。請關閉此視窗繼續編輯你的習作。";
$assignments_msg7 = array("新增及監察學生習作", "為已上傳檔案建立答案紙，然後進行自動批改");
$assignments_modelanswer = "標準答案";
$assignments_permissions = "權限";
$assignments_comment = "意見";
$assignments_alert_msg1 = "請輸入主旨。";
$assignments_alert_msg2 = "請輸入訊息內容";
$assignments_alert_msg3 = "發送電郵?";
$assignments_alert_msg4 = "請輸入評級。";
$assignments_alert_msg5 = "你只可預覽HTML文件。";
$assignments_alert_msg6 = "HTML預覽";
$assignments_alert_msg7 = "請輸入標題。";
$assignments_alert_msg8 = "請輸入呈交期限。";
$assignments_alert_msg9 = "無效日期。";
$assignments_alert_msg10 = "請輸入指示。";
$assignments_alert_msg11 = "按此儲存已呈交習作!";
$assignments_alert_msg12 = "Record Time Limit must be between 1 and";
$assignments_alert_msg13 = "儲存配額必須為正數";
$Assignments_submitted = "你所呈交的答案";
$assignments_message = "訊息";
$gradedAnswer_msg1='開始!';
$gradedAnswer_msg2='Check the graded paper!';
$grade_not = "Unmark";

// project info
$project_info_basic = "基本設定";
$project_info_group = "小組設定";
$project_info_phase = "階段設定";
$project_phase_no = "階段數目";
$project_phase_name = "階段名稱";
$project_interim_date = "階段提交期限";
$project_report = "報告形式";
$project_media = "Medium";
$project_ratio = "百分比";
$project_steps = "STEPS";
$project_none = "不適用";
$project_file = "檔案上載";
$project_general = "一般資訊";
$project_progress = "進度";
$project_reject = "拒絕";
$project_my = "我的";
$project_our = "我們的";
$project_by = "屬於";
$project_submit_by = "呈交者";
$project_finished = "已完成";
$project_std_msg1 = "此頁顯示你專題研習的一般資訊。如適用，按 <b>我的進度</b> for viewing the details and submitting reports if applicable.";
$project_std_msg2 = "你需要按階段完成專題習作。請確定你在階段提交期限以前呈交所需之報告，然後勾選 \"<b>已完成</b>\" 欄報告進度。";

// warning
$ec_warning['reset_template'] = "你是否確定要還原到上一版本並刪除現有內容？Are you sure that you don't want to keep the current contents but revert to the last version?";
$ec_warning['publish_content'] = "你是否確定發佈所選內容?";
$ec_warning['hide_content'] = "你是否確定移除學習檔案?";
$ec_warning['comment_content'] = "請輸入你的意見.";
$ec_warning['comment_author'] = "請輸入你的姓名.";
$ec_warning['comment_submit_confirm'] = "你是否確定要提交意見?";
$ec_warning['notes_title'] = "請輸入標題.";
$ec_warning['release_template'] = "你是否確定公開所選模板予學生建立學習檔案?\\n°e注意°f當學生開始使用已公開之模板後, 你將不能再更改有關模板!";
$ec_warning['growth_title'] = "請輸入計劃名稱。";
$ec_warning['growth_phase_title'] = "請輸入階段名稱。";
$ec_warning['form_handin'] = "Are you sure that you want to submit the current info?";
$ec_warning['growth_phase_viewed'] = "你是否已閱讀以上內容？";
$ec_warning['form_no_fill_in'] = "審閱中，你不能在此書寫任何內容!";
$ec_warning['no_permission'] = "你沒有存取權限。";
$ec_warning['no_view_now'] = "還沒有進入審閱階段。";
$ec_warning['weblog_contents'] = "請輸入內容。";
$ec_warning['weblog_title'] = "請輸入標題。";
$ec_warning['weblog_entry_remove'] = "你是否確定刪除紀錄?";
$ec_warning['portfolio_activate'] = "你是否確定啟動以上戶口？";
$ec_warning['award_title'] = "請輸入獎項名稱。";
$ec_warning['date'] = "請輸入日期。";
$ec_warning['title'] = "請輸入標題。";
$ec_warning['eclass_data_syn'] = "你是否確定從eClass更新資料？";
$ec_warning['eclass_data_no_record'] = "沒有可以更新的紀錄!";
$ec_warning['order_repeat'] = "次序不能重複，請重新選擇。";
$ec_warning['order_required'] = "你必須填寫次序!";
$ec_warning['copy_portfolio'] = "你是否確定複製XXX班之學習檔案文件？";
$ec_warning['copy_portfolio_SP'] = "你是否確定複製XXX同學的學習檔案文件？";
$ec_warning['remove_copied_portfolio'] = "你是否確定刪除XXX班之學習檔案文件?";
$ec_warning['coyp_template'] = "你是否確定複製模板？";
$ec_warning['select_student'] = "請選擇學生";
$ec_warning['select_class'] = "請選擇班別";
$ec_warning['olr_from_teacher'] = "此紀錄由老師提交，請批核!";
$ec_warning['info_remove'] = "你是否確定刪除此項資料？";
$ec_warning['overwrite_file'] = "新檔案將會覆蓋舊檔案。";
$ec_warning['remove_default'] = "不可刪除預設紀錄!";
$ec_warning['edit_default'] = "不可編輯預設紀錄!";
$ec_warning['over_limit'] = "所選擇的紀錄數目超過上限。";
$ec_warning['category'] = "請選擇一個分類";
$ec_warning['convert_year_instruction'] = "eClass IP 2.5校園綜合平台統一使用YYYY-YYYY作為學年顯示格式。你可轉換舊有紀錄的年度資料到新格式。<br /><br /><font color=\"#FF0000\">由於此項操作無法取消，請確保你已經選擇正確的學年(錯誤例：轉換 \"08-09\" 學年的紀錄到 \"2009-2010\" 學年)。</font>";
$ec_warning['reportPrinting_instruction'] = "你可列印過往學年的紀錄，但報告只會顯示學生 <b>現時的</b> 班別及學號。如要列印過往紀錄，請選擇舊學年，並選擇學生 <b>於本學年</b> 的班別及學號。";
$ec_warning['reportPrinting_instruction_for_student'] = "你可列印過往學年的紀錄，但報告只會顯示你 <b>現時的</b> 班別及學號。如要列印過往紀錄，請選擇該學年。";

$ec_warning['reportPrinting_instruction_for_student_ywgs'] = "你可預覽你的 OLE 報告。建議你使用 IE 的列印預覽功能，以檢查分頁情況。若有需要，你可編輯學生自述並插入分頁。";

$ec_warning['reportExport_instruction'] = "你可匯出過往學年的紀錄，但紀錄只會顯示學生 <b>現時的</b> 班別及學號。";
$ec_warning['oleMgmt_instruction'] = "本頁顯示所有你需要批核的紀錄。勾選紀錄後，按“批核”或“拒絕”。此外，你可以編輯、合併、及刪除紀 錄，但只限於沒有#號的紀錄。標有#號的紀錄由其他老師建立，你只可以對其進行批核。";

$ec_warning['OeaReportPrinting_instruction_for_student'] = "你可列印 / 匯出已批核的OEA紀錄.";

$ec_warning['lpForcePublish_instruction'] = "<strong>你可以勾選學習檔案，然後選擇以下其中一種發佈模式：</strong><ul><li>發佈曾經發佈過的內容 - 適用於學生對已經發佈的內容進行更新，但更新後忘記再行發佈的情況。</li><li>發佈全部內容 - 適用於學生忘記發佈學習檔案或逾期未能完成，老師需要強制發佈的情況。</li></ul>";

$ec_warning['DefaultSA_by_ModifiedDate'] = "由於所選擇的學生自述已被刪除，最新更新的學生自述會自動被選擇。";

$ec_iPortfolio['please_fill_start_end_date'] = "你必須輸入開始及結束時間";
$ec_iPortfolio['erase_info'] = "清除資料";
$ec_iPortfolio['remove_info'] = "刪除資料";
$ec_iPortfolio['remove_merit_info'] = "刪除優點紀錄";
$ec_iPortfolio['remove_activity_info'] = "刪除活動紀錄";
$ec_iPortfolio['remove_assessment_info'] = "刪除評核紀錄";
$ec_iPortfolio['remove_award_info'] = "刪除獎項紀錄";
$ec_iPortfolio['remove_conduct_info'] = "刪除老師意見紀錄";
$ec_iPortfolio['remove_attendance_info'] = "刪除考勤紀錄";
$ec_iPortfolio['remove_more_merit'] = "刪除更多優點紀錄";
$ec_iPortfolio['remove_more_activity'] = "刪除更多活動紀錄";
$ec_iPortfolio['remove_more_award'] = "刪除更多獎項紀錄";
$ec_iPortfolio['remove_more_attendance'] = "刪除更多考勤紀錄";
$ec_iPortfolio['remove_more_conduct'] = "刪除更多老師意見紀錄";
$ec_iPortfolio['remove_more_assessment'] = "刪除更多評核紀錄";
$ec_iPortfolio['number_of_record'] = "紀錄數目";
$ec_iPortfolio['number_of_removed_record'] = "成功刪除紀錄數目";
$ec_iPortfolio['iportfolio_folder_size'] = "儲存配額";
$ec_iPortfolio['size_unit'] = '(MB)';
$ec_iPortfolio['no_this_record'] = "此學生尚未公開學習檔案。";
$ec_iPortfolio['lastModifiedDate'] = "上次修改日期";
$ec_iPortfolio['setThisDefaultSA'] = "是否選擇此學生自述?";
$ec_iPortfolio['Selected'] = "已選擇";
$ec_iPortfolio['Export_Format'] = "匯出格式";
$ec_iPortfolio['Export_Custom_Field'] = "自選紀錄";
$ec_iPortfolio['Export_WebSAMS_Format'] = "WebSAMS格式";
$ec_iPortfolio['Export_Custom_Format'] = "訂製格式";

$ec_iPortfolio['NoOfFirstPage'] = "第一頁數目";
$ec_iPortfolio['NoOfOtherPage'] = "其他頁數目";
$ec_iPortfolio['NoOfRecordInt'] = "每頁其他學習經歷數目";
$ec_iPortfolio['NoOfRecordExt'] = "每頁校外的表現/獎項數目";

$ec_iPortfolio['PassMark'] = "合格分數";

// Student Report Printing
$Lang['StdRepPrint']['PrintingLanguage'] = "列印語言";
$Lang['StdRepPrint']['English'] = "英文";
$Lang['StdRepPrint']['Chinese'] = "中文";
$Lang['StdRepPrint']['Bilingual'] = "中英混合";

// alert
$w_alert['Invalid_Date'] = "無效日期";
$w_alert['Invalid_Time'] = "無效時間";
$w_alert['fill_in_info'] = "請輸入有關資料。";
$w_alert['start_end_time'] = "開始時間不能後於結束時間。";
$w_alert['start_end_time2'] = "開始時間不能後於結束時間。";
$w_alert['deadline_interim'] = "階段提交期限不能後於專題研習之提交期限";
$w_alert['choose_option'] = "請選一項。";
$w_alert['sure_to_cancel'] = "你是否確定要刪除?";
$w_alert['sure_to_reset'] = "你是否確定要重設?";
$w_alert['exam_release_ed'] = "如你想於提交期限後公開已批改之試圈，請輸入提交期限。";
$w_alert['logout_confirm'] = "你是否確定要登出?";
$w_alert['exit_confirm'] = "你是否確定要離開?";

$ec_iPortfolio['record_approval_setting'] = "紀錄批核設定";
$ec_iPortfolio['record_approval_setting_auto'] = "不用批核";
$ec_iPortfolio['record_approval_setting_selective'] = "允許學生選擇批核老師";
$ec_iPortfolio['record_approval_setting_default'] = "只有班主任及系統管理員擁有批核權";
$ec_iPortfolio['record_approval_setting_editable'] = "准許學生修改";

###############################################################################
# iPortfolio 2.5
###############################################################################
$ec_iPortfolio['export']['CheckAll'] = "全選";
$ec_iPortfolio['export']['UnCheckAll'] = "全消";
$ec_iPortfolio['export']['SelectFields'] = "紀錄顯示";
$ec_iPortfolio['export']['WebSAMSRegNo'] = "WebSAMS 學生註冊編號";
$ec_iPortfolio['export']['Programme'] = "活動項目";
$ec_iPortfolio['export']['ProgrammeDescription'] = "活動項目簡介";
$ec_iPortfolio['export']['SchoolYearFrom'] = "由學年";
$ec_iPortfolio['export']['SchoolYearTo'] = "至學年";
$ec_iPortfolio['export']['ShowSemester'] = "學期";
$ec_iPortfolio['export']['Role'] = "參與角色";
$ec_iPortfolio['export']['Organization'] = "主辦機構";
$ec_iPortfolio['export']['Awards'] = "獎項 / 證書文憑 / 成就 (包括等級/分數, 如有)";
$ec_iPortfolio['export']['Student'] = "學生姓名";
$ec_iPortfolio['export']['ClassName'] = "班別名稱";
$ec_iPortfolio['export']['ClassNumber'] = "班號";
$ec_iPortfolio['export']['Period'] = "日期/時段";
$ec_iPortfolio['export']['Category'] = "類別";
$ec_iPortfolio['export']['Hours'] = "時數";
$ec_iPortfolio['export']['Details'] = "詳情";
$ec_iPortfolio['export']['ApprovedBy'] = "批核者";
$ec_iPortfolio['export']['Status'] = "狀況";
$ec_iPortfolio['export']['ApprovedDate'] = "已批核日期";
$ec_iPortfolio['export']['Remarks'] = "學校備註";
$ec_iPortfolio['export']['ELE'] = "其他學習經歷範疇";
$ec_iPortfolio['export']['Comment'] = "評語";

$ec_iPortfolio['exportWebSAMS']['WebSAMSRegNo'] = "學生註冊編號
Registration Number";
$ec_iPortfolio['exportWebSAMS']['ProgrammeNameEng'] = "活動項目(英文)
Programme Name (Eng)";
$ec_iPortfolio['exportWebSAMS']['ProgrammeNameChi'] = "活動項目(中文)
Programme Name (Chi)";
$ec_iPortfolio['exportWebSAMS']['ProgrammeDescriptionEng'] = "活動項目簡介(英文)
Programme Description (Eng)";
$ec_iPortfolio['exportWebSAMS']['ProgrammeDescriptionChi'] = "活動項目簡介(中文)
Programme Description (Chi)";
$ec_iPortfolio['exportWebSAMS']['SchoolYearFrom'] = "由學年
School Year From";
$ec_iPortfolio['exportWebSAMS']['SchoolYearTo'] = "至學年
School Year To";
$ec_iPortfolio['exportWebSAMS']['RoleEng'] = "參與角色(英文)
Role of Participation (Eng)";
$ec_iPortfolio['exportWebSAMS']['RoleChi'] = "參與角色(中文)
Role of Participation (Chi)";
$ec_iPortfolio['exportWebSAMS']['PartnerOrganizationEng'] = "合辦機構(英文)
Partner Organization (Eng)";
$ec_iPortfolio['exportWebSAMS']['PartnerOrganizationChi'] = "合辦機構(中文)
Partner Organization (Chi)";
$ec_iPortfolio['exportWebSAMS']['OLEEng'] = "其他學習經歷的主要種類(英文)
Major Components of Other Learning Experiences (Eng)";
$ec_iPortfolio['exportWebSAMS']['OLEChi'] = "其他學習經歷的主要種類(中文)
Major Components of Other Learning Experiences (Chi)";
$ec_iPortfolio['exportWebSAMS']['AwardsEng'] = "獎項 / 證書文憑 / 成就 (如有)(英文)
Awards / Certifications / Achievements (if any) (Eng)";
$ec_iPortfolio['exportWebSAMS']['AwardsChi'] = "獎項 / 證書文憑 / 成就 (如有)(中文)
Awards / Certifications / Achievements (if any) (Chi)";
$ec_iPortfolio['exportWebSAMS']['Validation'] = "校驗 Validation";

$ec_iPortfolio['DGS_Custom']['Student'] = "學生";
$ec_iPortfolio['DGS_Custom']['CurrentStudent'] = "本屆學生";
$ec_iPortfolio['DGS_Custom']['AllStudent'] = "所有學生";
$ec_iPortfolio['export_DGS']['EnglishName'] = "英文姓名";
$ec_iPortfolio['export_DGS']['ChineseName'] = "中文姓名";
$ec_iPortfolio['export_DGS']['DOB'] = "出生日期";
$ec_iPortfolio['export_DGS']['AdmissionDate'] = "入學日期";
$ec_iPortfolio['export_DGS']['HKID'] = "HKID";
$ec_iPortfolio['export_DGS']['WebSAMSRegNo'] = "WebSAMSRegNo";
$ec_iPortfolio['export_DGS']['AwardName'] = "獎項名稱";
$ec_iPortfolio['export_DGS']['SchoolYear'] = "學年";
$ec_iPortfolio['export_DGS']['ProgrammeName'] = "項目名稱";
$ec_iPortfolio['export_DGS']['AcademicYear'] = "學年";
$ec_iPortfolio['export_DGS']['Role'] = "角色";
$ec_iPortfolio['export_DGS']['ComponentofOLE'] = "其他學習經歷範疇";
$ec_iPortfolio['export_DGS']['Achievement'] = "成果";
$ec_iPortfolio['export_DGS']['Category'] = "類別";
$ec_iPortfolio['export_DGS']['Organization'] = "合作機構";
$ec_iPortfolio['export_DGS']['ClassName'] = "班別名稱";
$ec_iPortfolio['export_DGS']['ClassNo'] = "學號";

$ec_iPortfolio['address'] = "地址";
$ec_iPortfolio['admission_date'] = "入學日期";
$ec_iPortfolio['award'] = "獎項";
$ec_iPortfolio['basic_info'] = "基本資料";
$ec_iPortfolio['class'] = "班別";
$ec_iPortfolio['ClassName'] = "班別";
$ec_iPortfolio['ClassNumber'] = "學號";
$ec_iPortfolio['LoginName'] = "登入名稱";
$ec_iPortfolio['ProgramRemarks'] = "課程備註";
$ec_iPortfolio['ComponentCode'] = "其他學習經歷範疇代碼";
$ec_iPortfolio['identity'] = "身分";
$ec_iPortfolio['class_list'] = "班別列表";
$ec_iPortfolio['year_list'] = "年度列表";
$ec_iPortfolio['display_by_grade'] = "依評級";
$ec_iPortfolio['display_by_rank'] = "依級別名次";
$ec_iPortfolio['display_by_score'] = "依分數";
$ec_iPortfolio['display_by_score_grade'] = "依分數及評級";
$ec_iPortfolio['display_by_stand_score'] = "依標準分";
$ec_iPortfolio['em_phone'] = "緊急聯絡號碼";
$ec_iPortfolio['enter_student_name'] = "輸入學生姓名";
$ec_iPortfolio['guardian_info'] = "監護人資料";
$ec_iPortfolio['brother_sister_info'] = "兄弟姊妹資料";
$ec_iPortfolio['house'] = "社";
$ec_iPortfolio['import_activation'] = "啟動iPortfolio戶口";
$ec_iPortfolio['list'] = "列表";
$ec_iPortfolio['main_guardian'] = "主要監護人";
$ec_iPortfolio['school_chop'] = "校印";


$ec_iPortfolio['SAMS_import_form'] = "CSV 格式";

$ec_iPortfolio['SAMS_import'] = "匯入 WebSAMS CSV 檔";
$ec_iPortfolio['SAMS_import_subject'] = "匯入科目";
$ec_iPortfolio['SAMS_import_anp'] = "匯入獎懲紀錄";
$ec_iPortfolio['SAMS_import_assessment'] = "匯入 WebSAMS 學業報告";
$ec_iPortfolio['SAMS_import_activity'] = "匯入活動紀錄";
$ec_iPortfolio['SAMS_import_comment'] = "匯入 WebSAMS 老師評語";
$ec_iPortfolio['SAMS_import_student_data'] = "匯入 WebSAMS 學生資料";

$ec_iPortfolio['merit'] = "獎懲紀錄";
$ec_iPortfolio['number'] = "班號";
$ec_iPortfolio['phone'] = "電話號碼";
$ec_iPortfolio['record'] = "紀錄";
$ec_iPortfolio['semester'] = "學期";
$ec_iPortfolio['self_account'] = "學生自述";
$ec_iPortfolio['student_photo_no'] = "沒有照片";
$ec_iPortfolio['student_account'] = "個人帳戶";
$ec_iPortfolio['thumbnail'] = "縮圖";
$ec_iPortfolio['total'] = "共";
$ec_iPortfolio['upload_photo'] = "上載照片";
$ec_iPortfolio['whole_year'] = "全年";
$ec_iPortfolio['year'] = "學年";
$ec_iPortfolio['graduation_year'] = "畢業學年";
$ec_iPortfolio['please_select_graduation_year'] = "請選擇畢業學年";
$ec_iPortfolio['ole'] = "其他學習經歷";
$ec_iPortfolio['overall_result'] = "整體成績";
$ec_iPortfolio['overall_score'] = "整體平均分";
$ec_iPortfolio['overall_rank'] = "級別整體名次";
$ec_iPortfolio['overall_score_grade'] = "整體平均分及評級";
$ec_iPortfolio['overall_grade'] = "整體評級";
$ec_iPortfolio['overall_stand_score'] = "級別整體標準分";
$ec_iPortfolio['overall_comment'] = "整體意見";
$ec_iPortfolio['overall_summary'] = "整體總結";
$ec_iPortfolio['subject_chart'] = "科目圖表";
$ec_iPortfolio['by_semester'] = "學期";

$ec_iPortfolio['SAMS_import_intro'] = "你可以使用 WebSAMS 格式的 CSV 檔匯入以下資料，<br />或按 \"更新\" 與 eClass 學生檔案進行資料同步。";

$ec_iPortfolio['SAMS_year_remind'] = "請依 eClass 的方式填寫 (格式須為 YYYY-yyyy 或 YYYY。 例如 \"2006-2007\" or \"2006\"。)";
$ec_iPortfolio['SAMS_regno_import_remind'] = "*注意: RegNo 編號應加上#（如 \"#0025136\")，以確保編號數字的完整";
$ec_iPortfolio['guardian_import_remind'] = " 另外，新匯入的監護人紀錄會覆蓋現有紀錄";
$ec_iPortfolio['template_deletion_remind'] = "*注意: 使用中的樣板不能被刪除";
$ec_iPortfolio['student_photo_upload_remind'] = "上載前請先以有大小寫之分的學生編號命名相片並壓縮相片至'.ZIP'檔案格式！<br />相片只能以'.JPG'的格式上載，相片大小也規定為100 X 130 像素 (闊 x 高)。";
$ec_iPortfolio['student_photo_upload_warning'] = "相片必須要以'.ZIP'檔案格式上載。";
$ec_iPortfolio['no_searched_result'] = "找不到和查詢相符的資料";
$ec_iPortfolio['search_by_condition'] = "用以下條件搜尋";
$ec_iPortfolio['admission_year'] = "學年";
$ec_iPortfolio['ole_comment'] = "教師評語";
$ec_iPortfolio['ole_comment_teacher'] = "教師姓名：";
$ec_iPortfolio['ole_advice'] = "導師評語";
$ec_iPortfolio['ole_override_comment'] = "此評語的發佈人是 <!--TeacherName-->。如你更新此評語，將會改變此評語的發佈人。";
$ec_iPortfolio['ole_set_pool_record'] = "設定報告用紀錄";
$ec_iPortfolio['ole_pkms_set_pool_record'] = "設定培僑中學報告用紀錄";
$ec_iPortfolio['ole_pkms_rec_start_from'] = "設定培僑中學報告用時段由";
$ec_iPortfolio['ole_no_rec_add_to_pool'] = "你可以加入 <!--NoRec--> 項紀錄";

$ec_iPortfolio['scoreHighlightRemarks'] = "不合格的分數會以紅色顯示";
$ec_iPortfolio['OMFHighlightRemarks'] = "退步的排名會以紅色顯示";
$ec_iPortfolio['OMFHighlightRemarks_Improve'] = "進步的排名會以綠色顯示";
$ec_iPortfolio['chart_type'] = "圖形";
$ec_iPortfolio['barchart'] = "棒形圖";
$ec_iPortfolio['linechart'] = "折線圖";

$ec_guide['import_update_no'] = "成功更新資料項數";
$ec_guide['import_add_no'] = "成功加入資料項數";
$ec_guide['import_fail_no'] = "不能加入資料項數";
$ec_guide['import_error_row'] = "行數";
$ec_guide['import_error_data'] = "錯誤項目";
$ec_guide['import_error_reason'] = "錯誤原因";
$ec_guide['import_error_detail'] = "檔案資料";
$ec_guide['import_error_duplicate_regno'] = "WebSAMS RegNo 已存在";
$ec_guide['import_error_activated_regno'] = "此學生戶口已啟用，不能更改 WebSAMS RegNo";
$ec_guide['import_error_incorrect_regno'] = "匯入的RegNo 編號應加上#";
$ec_guide['import_error_incorrect_class'] = "系統內沒有此班別資料";
$ec_guide['import_error_unknown'] = "不明";
$ec_guide['import_error_no_user'] = "沒有此用戶";
$ec_guide['import_error_wrong_format'] = "錯誤的匯入格式";
$ec_guide['import_error_empty_import_data'] = "找不到匯入資料";
$ec_guide['import_ole_join_date_not_coexist'] = "報名時段開始結束日期不同時存在";
$ec_guide['import_ole_program_title_already_exist'] = "活動名稱已存在";

$ec_guide['import_error_duplicate_classnum'] = "此學生的班別及學號出現重複";
$ec_guide['import_error_date_format'] = "日期格式錯誤 (yyyy-mm-dd) 或 日期錯誤(如 2009-11-31)";
$ec_guide['result'] = "結果";
$ec_guide['import_back'] = "再匯入";
$ec_guide['import_programid_not_found'] = "找不到課程編號";
$ec_guide['import_dateformat_not_correct'] = "日期格式應該是 (yyyy-mm-dd)";
$ec_guide['import_academic_year_not_found'] = "找不到學年";
$ec_guide['import_type_term_not_found'] = "找不到學期";
$ec_guide['import_type_year_term_not_found'] = "找不到 學年/學期";
$ec_guide['import_empty_title'] = "標題空白";
$ec_guide['import_header_content_column_not_match'] = "內容欄數與標題欄數不相乎";
$ec_guide['import_ole_category_not_found'] = "找不到OLE類別編號";
$ec_guide['import_ole_subcategory_not_found'] = "找不到OLE子類別編號";
$ec_guide['import_empty_category_title'] = "OLE類別空白";
$ec_guide['import_empty_program_title'] = "預設學習項目空白";
$ec_guide['import_empty_ELE'] = "OLE範疇空白";
$ec_guide['import_ELE_not_found'] = "找不到OLE範疇";
$ec_guide['import_ole_insideoutside_not_found']="找不到OLE校內/校外";
$ec_guide['import_ole_isSAS_not_found']="找不到OLE是否屬於 SAS";
$ec_guide['import_ole_ele_not_match']="至少一個其他學習經歷範疇不相乎";
$ec_guide['import_ole_compulsory_field_invalid']="至少一個學生申報時必要填寫欄位不相乎";
$ec_guide['fail_reason'] = "失敗的原因";
$ec_guide['above_level_mean'] = "高於全級總平均分";
$ec_guide['below_level_mean'] = "低於全級總平均分";
$ec_guide['import_ole_can_join'] = "允許學生申報填寫不正確";
$ec_guide['import_ole_join_start_date_not_found'] = "報名時段開始日期空白";
$ec_guide['import_ole_applicable_form_not_found'] = "適用班級空白";
$ec_guide['import_ole_auto_approval_not_found'] = "允許自動認可空白";
$ec_guide['import_ole_applicable_form_invalid'] = "適用班級不對應";
$ec_guide['import_ole_max_hour_negative'] = "最大時數為負數";
$ec_guide['import_ole_default_hour_negative'] = "預設時數為負數";
$ec_guide['import_ole_complusory_fields_invalid'] = "必要欄位不對應";
$ec_guide['import_ole_user_not_found'] = "找不到批核者帳號";
$ec_guide['import_ole_user_not_teacher'] = "批核者帳號並非教職員";
$ec_guide['import_ole_student_exist'] = "學生已屬有關OLE";
$ec_guide['import_ole_record_not_exist'] = "找不到紀錄";
$ec_guide['import_ole_user_should_not_exist'] = "如允許自動認可，批核者應為空白 ";

$ec_words['preset_no_record'] = "暫時沒有可選項目";
$ec_words['photo_no'] = "沒有提供相片";

$ec_iPortfolio['show_by'] = "依";
$ec_iPortfolio["score"] = "分數";
 $ec_iPortfolio["stand_score"] = "標準分";
 $ec_iPortfolio["rank"] = "級別名次";
 $ec_iPortfolio['term'] = "學期";
 $ec_iPortfolio['activity_name'] = "活動名稱";
 $ec_iPortfolio['role'] = "職位";
 $ec_iPortfolio['performance'] = "表現";
 $ec_iPortfolio['SAMS_last_update'] = "最近更新";
 $ec_iPortfolio['activity'] = "活動紀錄";
 $ec_iPortfolio['portfolio_status'] = "<!--PortfolioNo--> / <!--TotalStudentNo--> iPortfolio 戶口已經啟動。<!--GreenFrame-->";
 $ec_iPortfolio['Activated_Liense_Summary'] = "已啟動的戶口摘要";
 $ec_iPortfolio['Activated_Liense_Summary_current'] = "已啟動的現時學生人數:";
 $ec_iPortfolio['Activated_Liense_Summary_without_class'] = "已啟動的無所屬班別學生人數:";
 $ec_iPortfolio['Activated_Liense_Summary_alumni'] = "已啟動的校友人數:";
 $ec_iPortfolio['Activated_Liense_Summary_total_amount'] = "總數";
 $ec_iPortfolio['Activated_Liense_Summary_inactive'] = "非啟用 (暫停或已離校)";
 $ec_iPortfolio['record'] = "紀錄";
 $ec_iPortfolio['report_type'] = "報告類型";
 $ec_iPortfolio['record_type'] = "紀錄類型";
 $ec_iPortfolio['SLPOrder']  = '顯示學生想用於SLP的紀錄';
 $ec_iPortfolio['SLPOrder_csv'] = 'SLP顯示次序';
 $ec_iPortfolio['year'] = "學年";
 $ec_iPortfolio['year_period'] = "時段";
 $ec_iPortfolio['full_report'] = "綜合報告";
 $ec_iPortfolio['full_reportTitle2'] = "學生綜合報告表";
 $ec_iPortfolio['assessment_report'] = "學業報告";
 $ec_iPortfolio['transcript'] = "成績報告";
 $ec_iPortfolio['composite_performance_report'] = "綜合評核報告";
$ec_iPortfolio['view_my_record'] = "檢視我的紀錄";
$ec_iPortfolio['programme'] = "項目";
$ec_iPortfolio['role_participate'] = "參與角色";

$ec_iPortfolio['admin_accessRight'] = "校長／負責人（能檢視全部分析）";
$ec_iPortfolio['subjectPanel_accessRight'] = "科主任";
$ec_iPortfolio['monthlyReport_accessRight'] = "每月報表負責人";

$ec_iPortfolio['all_programmes'] = "全部項目";
$ec_iPortfolio['my_programmes'] = "我的項目";
$ec_iPortfolio['all_students'] = "全部學生";
$ec_iPortfolio['my_students'] = "我的學生";


$ec_iPortfolio['heading']['learning_portfolio'] = "學習檔案";
$ec_iPortfolio['heading']['no'] = "No";
$ec_iPortfolio['heading']['photo'] = "相片";
$ec_iPortfolio['heading']['student_info'] = "個人資料";
$ec_iPortfolio['heading']['student_name'] = "學生姓名";
$ec_iPortfolio['heading']['student_record'] = "學校紀錄";
$ec_iPortfolio['heading']['student_regno'] = "WebSAMS 學生註冊編號";
$ec_iPortfolio['heading']['school_record_updated'] = "已更新學校紀錄";
$ec_iPortfolio['heading']['sb_scheme_updated'] = "已更新校本計劃";
$ec_iPortfolio['heading']['lp_updated'] = "已更新學習檔案";
$ec_iPortfolio['heading']['no_lp_active_stu'] = "已啟動iPortfolio之學生總數";
$ec_iPortfolio['heading']['no_stu'] = "學生總數";
$ec_iPortfolio['heading']['report'] = "報告";
$ec_iPortfolio['student_photo'] = "學生相片";
$ec_iPortfolio['service_name'] = "服務名稱";
$ec_iPortfolio['title'] = "標題";
$ec_iPortfolio['category'] = "類別";
$ec_iPortfolio['sub_category'] = "子類別";
$ec_iPortfolio['NoOfSubcategory'] = "檢視學習項目子類別";
$ec_iPortfolio['NewCategory'] = "類別";
$ec_iPortfolio['NewSubcategory'] = "新增子類別";
$ec_iPortfolio['PresetProgrammeName'] = "預設項目名稱";
$ec_iPortfolio['NewPresetProgrammeName'] = "新增預設學習項目名稱";
$ec_iPortfolio['NoOfPresetProgrammeName'] = "預設項目名稱數量";
$ec_iPortfolio['AllPresetProgrammeName'] = "檢視預設學習項目";
$ec_iPortfolio['Categorize'] = "分類";
$ec_iPortfolio['Active'] = "啟用";
$ec_iPortfolio['Suspend'] = "停用";
$ec_iPortfolio['ele'] = "其他學習經歷範疇";
$ec_iPortfolio['ole_role'] = $ec_iPortfolio['role_participate'];
$ec_iPortfolio['total_hours'] = "總時數";
$ec_iPortfolio['achievement'] = "獎項 / 證書 / 成果 (如適用)";
$ec_iPortfolio['details'] = "詳情";
$ec_iPortfolio['school_remark'] = "學校備註";
$ec_iPortfolio['class_and_number'] = "班號";
$ec_iPortfolio['comments'] = "意見";
$ec_iPortfolio['new_comments'] = "個新評語";
$ec_iPortfolio['last_update_time'] = "最近更新";
$ec_iPortfolio['action_type'] = "行動類型";
$ec_iPortfolio['total_records'] = "紀錄總數";
$ec_iPortfolio['eca'] = "課外活動";
$ec_iPortfolio['column'] = "欄位";

$range_all = "全部";
$list_item_no = "/頁";
$list_display = "顯示";
$ec_iPortfolio['date'] = "日期";
$ec_iPortfolio['publish_date'] = "發佈日期";
$ec_iPortfolio['award_name'] = "獎項";
$ec_iPortfolio['award_date'] = "獲獎日期";
$ec_iPortfolio['upload_cert'] = "獎項/證明";
$ec_iPortfolio['remark'] = "備註";
$ec_iPortfolio['SAMS_last_update'] = "最近更新";
$ec_iPortfolio['award'] = "獎項紀錄";
$ec_iPortfolio['reading_records'] = "閱讀紀錄";
$ec_iPortfolio['teacher_comment'] = "老師評語";
$ec_iPortfolio['class_number'] = "班號";
$ec_iPortfolio['conduct_grade'] = "操行";
$ec_iPortfolio['comment_chi'] = "中文評語";
$ec_iPortfolio['comment_eng'] = "英文評語";
$ec_iPortfolio['title_merit'] = "獎懲評語";
$ec_iPortfolio['title_academic_report'] = "成績報告";
$ec_iPortfolio['title_activity'] = "活動";
$ec_iPortfolio['title_award'] = "獎項";
$ec_iPortfolio['title_reading_records'] = "閱讀紀錄";
$ec_iPortfolio['title_teacher_comments'] = "老師評語";
$ec_iPortfolio['title_attendance'] = "考勤";
$ec_iPortfolio['total_late_count'] = "總遲到次數";
$ec_iPortfolio['total_absent_count'] = "總缺席次數";
$ec_iPortfolio['total_earlyleave_count'] = "總早退次數";
$ec_iPortfolio['stype'] = "類型";
$ec_iPortfolio['period'] = "時段";
$ec_iPortfolio['reason'] = "原因";
$ec_iPortfolio['detail'] = "詳情";
$ec_iPortfolio['approval_date'] = "批核時間";
$ec_iPortfolio['student_report_print'] = "個人報告";
$ec_iPortfolio['class'] = "班別";
$ec_iPortfolio_Report['subject'] = "科目";
$ec_iPortfolio_Report['year'] = "學年";
$ec_iPortfolio_Report['term'] = "學期";
$ec_iPortfolio['report_title_year'] = "Year (學年)";
$ec_iPortfolio['report_title_class'] = "Class (班級)";
$ec_iPortfolio['report_title_classno'] = "Class No. (班號)";
$ec_iPortfolio['report_title_regno'] = "Reg. No. (學生編號)";
$ec_iPortfolio['report_title_name'] = "Name (姓名)";
$ec_iPortfolio['title_form_teacher_eng'] = "FORM TEACHER";
$ec_iPortfolio['title_form_teacher_chi'] = "班主任";
$ec_iPortfolio['title_principal_chi'] = "校長";
$ec_iPortfolio['title_principal_eng'] = "PRINCIPAL";
$ec_iPortfolio['title_guardian_chi'] = "家長/監護人";
$ec_iPortfolio['title_guardian_eng'] = "PARENT/GUARDIAN";
//$ec_iPortfolio['cutomized_report_remark_chi'] = "有關報告之詳情，請參閱學生綜合紀錄表現小冊子";
//$ec_iPortfolio['cutomized_report_remark_eng'] = "For details, please refer to the handbook of Student Portfolio";
$ec_iPortfolio['title_eca'] = "Extra-Curricular Activities 課外活動";
$ec_iPortfolio['title_service'] = "Services in School 校內服務";
$ec_iPortfolio['title_award_punishment'] = "獎懲";
$ec_iPortfolio['title_ole'] = "Other Learning Experiences 其他學習經歷";
$ec_iPortfolio['pic'] = "負責人";
$ec_iPortfolio['mtype'] = "類型";
$ec_iPortfolio['student_info'] = "個人資料";
$ec_iPortfolio['academic_result'] = "學業報告";

# For data export
$ec_iPortfolio['data_export'] = "資料匯出";
$ec_iPortfolio['export_slp_data'] = "學生學習概覽資料匯出";
$ec_iPortfolio['export_merit_record'] = "獎懲紀錄匯出";
$ec_iPortfolio['export_assessment_record'] = "學業成績匯出";
$ec_iPortfolio['export_activity_record'] = "活動紀錄匯出";
$ec_iPortfolio['export_award_record'] = "得獎紀錄匯出";
$ec_iPortfolio['export_comment_record'] = "老師評語紀錄匯出";
$ec_iPortfolio['export_attendance_record'] = "考勤紀錄匯出";
$ec_iPortfolio['subject_result'] = "科目成績";
$ec_iPortfolio['student_overall_result'] = "學生總成績";
$ec_iPortfolio['report_type'] = "報告類型";

$ec_iPortfolio['attendance'] = "考勤紀錄";
$ec_iPortfolio['disable_review'] = "不使用";
$ec_iPortfolio['enable_review'] = "使用";
$ec_iPortfolio['review_type'] = "朋輩互評";
$ec_iPortfolio['peer_review_type'] = "朋輩互評類型";
$ec_iPortfolio['school_review'] = "全校朋輩互評";
$ec_iPortfolio['level_review'] = "全級朋輩互評";
$ec_iPortfolio['class_review'] = "全班朋輩互評";
$ec_iPortfolio['friends_review'] = "同時允許學生邀請校內指定同學評核自己的學習檔案";
$ec_iPortfolio['yes'] = "是";
$ec_iPortfolio['no'] = "否";
$ec_iPortfolio['update_msg'] = "已更新紀錄";
$ec_iPortfolio['peer_review_setting'] = "朋輩互評設定";
$ec_iPortfolio['attendance_detail'] = "考勤詳情";
$ec_iPortfolio['merit_detail'] = "獎懲詳情";
$ec_iPortfolio['generate_score_list'] = "產生分數列表";
$ec_iPortfolio['display'] = "顯示";
$ec_iPortfolio['highlight_result'] = "等級高亮";
$ec_iPortfolio['grade'] = "評級";
$ec_iPortfolio['bold'] = "粗體";
$ec_iPortfolio['italic'] = "斜體";
$ec_iPortfolio['underline'] = "下劃線";
$ec_iPortfolio['color'] = "顏色";
$profiles_to = "至";
$ec_iPortfolio['red'] = "紅";
$ec_iPortfolio['green'] = "綠";
$ec_iPortfolio['blue'] = "藍";
$ec_iPortfolio['growth_title'] = "標題";
$ec_iPortfolio['growth_phase_period'] = "時段";
$ec_iPortfolio['growth_description'] = "簡介";
$ec_iPortfolio['growth_status'] = "狀態";
$profile_modified = "已修改";
$ec_iPortfolio['web_view'] = "學習檔案";
$ec_iPortfolio['activate'] = "啟用";
$ec_iPortfolio['deactive'] = "停用";
$ec_iPortfolio['delete'] = "刪除";
$ec_iPortfolio['edit'] = "編輯";
$ec_iPortfolio['edit_content'] = "編輯內容";
$ec_iPortfolio['your_comment'] = "你的意見";
$StartTime = "開始時間";
$EndTime = "結束時間";
$ec_iPortfolio['iportfolio_folder_size'] = "儲存配額";
$ec_iPortfolio['size_unit'] = '(MB)';
$ec_iPortfolio['growth_group'] = "參與小組";
$ec_iPortfolio['edit_this_page'] = "編輯此頁";
$ec_iPortfolio['view_comment'] = "檢視評語";
$ec_warning['growth_title'] = "請輸入計劃名稱";
$assignments_alert_msg13 = "儲存配額必須為正數";
$assignments_alert_msg9 = "無效日期。";
$w_alert['start_end_time2'] = "開始時間不能後於結束時間。";
$ec_iPortfolio['new_portfolios'] = "新增檔案";
$con_msg_add = "已新增紀錄。";
$con_msg_update = "已更新紀錄。";
$con_msg_del = "已刪除紀錄。";
$con_msg_save = "已儲存紀錄。";
$qbank_status = "狀態";
$button_public = "公開";
$button_private = "保密";
$ec_iPortfolio['edit_portfolios'] = "編輯檔案";
$ec_iPortfolio['choose_group'] = "選擇小組";
$ec_iPortfolio['select_group'] = "已選擇小組";
$qbank_public = "公開";
$ec_iPortfolio['class'] = "班別";
$ec_iPortfolio_Report['form'] = "級別";
$ec_iPortfolio['SAMS_subject'] = "科目";
$ec_iPortfolio['based_on'] = "統計基準";
$ec_iPortfolio['view_stat'] = "檢視統計";
$ec_iPortfolio['student_perform_stat'] = "個人表現統計";
$ec_iPortfolio['mean'] = "平均數";
$ec_warning['please_select_class'] = "請選擇班別!";
$ec_warning['please_select_subject'] = "請選擇科目!";
$ec_iPortfolio['standard_deviation'] = "標準差";
$ec_iPortfolio['highest_score'] = "最高分";
$ec_iPortfolio['lowest_score'] = "最低分";
$ec_iPortfolio['number_using_template'] = "正在使用此模板的學習檔案數目";
$ec_iPortfolio['portfolios_mgt'] = "檔案管理";
$ec_iPortfolio['templates_mgt'] = "模板管理";
$ec_iPortfolio['form'] = "級別";
$ec_iPortfolio['whole_school'] = "全校";
$ec_iPortfolio['school_yr'] = "全部年度";
$ec_iPortfolio['display_class_stat'] = "顯示班級統計";
$ec_iPortfolio['display_class_distribution'] = "顯示班級統計";
$ec_iPortfolio['all_statistic'] = "全級統計";
$ec_iPortfolio['term'] = "學期";
$ec_iPortfolio['class'] = "班別";
$ec_iPortfolio['group_by'] = "分組方式";
$ec_iPortfolio['school_year'] = "學年";
$ec_iPortfolio['form'] = "級別";
$ec_iPortfolio['above_mean'] = "高於班際平均數";
$ec_iPortfolio['below_mean'] = "低於班際平均數";
$ec_iPortfolio['all_classlevel'] = "全部級別";
$ec_iPortfolio['all_class'] = "全部班別";
$ec_iPortfolio['all_ele'] = "全部其他學習經歷範疇";
$ec_iPortfolio['learning_portfolio_content'] = "學習檔案管理";
$ec_iPortfolio['ole_include'] = "包含 OLE";
$button_organize = "組織";
$wording['contents_notice1'] = "以 <font color=#AAAAAA>灰色</font> 顯示的為保密內容。";
$notes_title = "Page title";
$ec_iPortfolio['notes_guide'] = "指示";
$ec_iPortfolio['notes_template'] = "默認模板";
$button_new_template = "新模板";
$ec_guide['learning_portfolio_time_start'] = "如設定開始時間，學生將只能於該段時間後修改學習檔案內容。";
$ec_guide['learning_portfolio_time_end'] = "如設定結束時間，學生將只能於該段時間前修改學習檔案內容。";
$ec_iPortfolio['edit_template_alert'] = "警告: 此頁正在使用模板 \'XXX\'。 所有使用此模板的頁面將一並更新。若只想修改此頁面，請指定一個新的模板。";
$ec_iPortfolio['type'] = "建立";
$notes_position = "頁面位置";
$ec_iPortfolio['type_category'] = "文件夾";
$ec_iPortfolio['type_document'] = "文件";
$ec_warning['notes_title'] = "請輸入標題。";
$ec_iPortfolio['notes_method'] = "內容輸入格式";
$ec_iPortfolio['notes_method_html'] = "HTML格式";
$ec_iPortfolio['notes_method_form'] = "表格格式";
$ec_iPortfolio['notes_method_weblog'] = "網誌";
$ec_iPortfolio['notes_method_eClass'] = "從eClass匯入";
$quizbank_desc = "簡介";
$assignments_alert_msg6 = "HTML預覽";
$ec_iPortfolio['notes_relocate_Up'] = "上移到 &quot;XXX&quot;之前";
$ec_iPortfolio['notes_relocate_Down'] = "下移到 &quot;XXX&quot;之後";
$ec_iPortfolio['notes_relocate_DL'] = "上移一層到 &quot;XXX&quot;之後";
$ec_iPortfolio['notes_relocate_DR'] = "下移一層到 &quot;XXX&quot;之內";
$ec_iPortfolio['notes_relocate_UL'] = "上移一層到 &quot;XXX&quot;之前";
$ec_iPortfolio['notes_relocate_UR'] = "下移一層到 &quot;XXX&quot;之內";
$usertype_s = "學生";
$ec_iPortfolio['hours_range'] = "總時數範圍";
$ec_iPortfolio['hours_above'] = "小時以上";
$ec_iPortfolio['hours_below'] = "小時以下";
$ec_iPortfolio['hours_to'] = "至";
$ec_iPortfolio['analysis_report'] = "分析報告";
$ec_iPortfolio['overall'] = "整體";
$valueTitle = "分";
$ec_iPortfolio['all_school_year'] = "全部學年";
$ec_iPortfolio['no_student'] = "沒有學生";
$ec_iPortfolio['ole_report_stat'] = "其他學習經歷報告";
$ec_iPortfolio['template_title'] = "模板名稱";
$ec_iPortfolio['copied_template_title'] = "已複製模板名稱";
$ec_iPortfolio['full_report_config'] = "綜合報告";
$ec_iPortfolio['transcript_config'] = "成績報告";
$ec_iPortfolio['csv_report_config'] = "匯入學習紀錄";
$ec_iPortfolio['pkms_SLP_config'] = "培僑中學學生學習概覽設定";
$ec_iPortfolio['not_display_student_photo'] = "學生相片";
$ec_iPortfolio['no_student_details'] = "學生詳細資料";
$ec_iPortfolio['no_school_name'] = "學校名稱 (首頁)";
$ec_iPortfolio['other_learning_record'] = "其他學習紀錄";
$ec_iPortfolio['show_component_sub'] = "顯示科目分卷";
$ec_iPortfolio['show_remark'] = "顯示備註";
$ec_iPortfolio['copy_web_portfolio_files'] = "整合學習檔案文件";
$ec_iPortfolio['remove_copied_files'] = "移除已整合之學習檔案文件";
$ec_iPortfolio['with_comment'] = "包括評語";
$ec_iPortfolio['with_slp'] = "包括學生學習概覽報告";
$ec_iPortfolio['with_freport'] = "包括綜合報告";
$ec_iPortfolio['with_sbs'] = "包括校本計劃";
$ec_iPortfolio['with_attachment'] = "包括獎項/證書附件";
$ec_iPortfolio['review_author'] = "Post:";
$ec_iPortfolio['finished_cd_burning_preparation'] = "XXX班學生的學習檔案文件已整合到以下資料夾:";
$ec_iPortfolio['get_prepared_portfolio'] = "學習檔案文件已存放到學生編號資料夾中，老師可將資料夾燒錄到光碟中。";
$ec_iPortfolio['prepare_other_class_cd_burning'] = "預備其他班別的光碟燒錄";
$ec_iPortfolio['copied_portfolio_reomved'] = "XXX班學生的學習檔案文件已經移除";
$ec_iPortfolio['copied_portfolio_reomved_fail'] = "XXX班學生的學習檔案文件未能移除";
$ec_iPortfolio['preview'] = "預覽";
$ec_words['school_badge'] = "校徽";
$ec_words['school_address'] = "學校地址";
$ec_iPortfolio['transcript_description'] = "學生成績表描述";
$ec_iPortfolio['transcript_CSV_file'] = "CSV補充資料";
$ec_iPortfolio['not_display'] = "不顯示";
$ec_iPortfolio['reading_record'] = "Reading Records";
$ec_iPortfolio['ole_record'] = "其他學習經歷紀錄";
$ec_iPortfolio['file'] = "檔案";
$ec_iPortfolio['iPortfolio_published'][1] = "<font color=green>你的學習檔案已經成功 <u> 發佈 </u>!</font>";
$ec_iPortfolio['iPortfolio_published'][0] = "<font color=green>你的學習檔案已經成功 <u> 移除 </u>!</font>";
$ec_iPortfolio['available_portfolio_acc'] = "剩餘iPortfolio戶口數目 (以全校計算)";
$ec_iPortfolio['import_regno'] = "匯入WebSAMS 學生註冊編號";
$ec_iPortfolio['activated_summary'] = "已啟動的戶口摘要";
$ec_iPortfolio['account_quota_free'] = "剩餘用戶戶口數目";
$ec_iPortfolio['activation_student_total'] = "已選擇學生總數";
$ec_iPortfolio['activation_quota_afterward'] = "啟動後之剩餘用戶戶口數目";
$ec_iPortfolio['activation_total_success'] = "成功啟動之學生戶口數目";
$ec_iPortfolio['activation_total_fail'] = "未能啟動之學生戶口數目";
$namelist_class_number = "班號";
$ec_iPortfolio['activation_result_no_regno'] = "找不到登記號碼";
$ec_iPortfolio['activation_result_activated'] = "此用戶戶口已經啟動。";
$ec_iPortfolio['draft'] = "草稿";
$ec_iPortfolio_guide['manage_student_portfolio_msg'] = "You will manage Learning iPortfolio by changing to Student Role!";
$ec_iPortfolio['suspend_total_success'] = "已成功暫停戶口之學生數目";
$ec_iPortfolio['suspend_total_fail'] = "未能暫停戶口之學生數目";
$ec_iPortfolio['suspend_result_suspended'] = "此戶口已被暫停";
$ec_iPortfolio['suspend_result_inactive'] = "此戶口尚未啟動";

$ec_iPortfolio['generate_csv_template'] = "製作CSV範本檔";
$ec_iPortfolio['portfolios'] = "學習檔案管理";
$ec_iPortfolio['yearform'] = "年級";

#for Kei Wai Report
$ec_iPortfolio['keiwai_personal_info'] = "個人資料";
$ec_iPortfolio['keiwai_education'] = "學歷";
$ec_iPortfolio['keiwai_academic_results'] = "學業成績";
$ec_iPortfolio['keiwai_professional_profile'] = "履歷";
$ec_iPortfolio['keiwai_extra_data_manage'] ="基華補充資料管理系統";
$ec_iPortfolio['keiwai_report'] ="學生Achievement Form";
$ec_iPortfolio['keiwai_report_2'] ="Letter of Accomplishment";
$ec_iPortfolio['keiwai_HKID'] = "身份證號碼";
$ec_iPortfolio['keiwai_religion'] = "宗教信仰";
$ec_iPortfolio['pob'] = "出生地點";
$ec_iPortfolio['nationality'] = "國籍";
$ec_iPortfolio['telno'] = "電話";
$ec_iPortfolio['keiwai_subject_area'] = "Subject Area";
$ec_iPortfolio['keiwai_organization_a'] = "頒授機構";
$ec_iPortfolio['keiwai_organization_b'] = "機構";
$ec_iPortfolio['keiwai_organization_c'] = "機構";
$ec_iPortfolio['service'] = "服務紀錄";
$ec_iPortfolio['eClass_update_service'] = "從eClass學生檔案更新服務紀錄";
$ec_iPortfolio['update_more_service'] = "再次更新服務紀錄";
$ec_iPortfolio['remove_service_info'] = "刪除服務紀錄";
$ec_iPortfolio['remove_more_service'] = "刪除其他服務紀錄";
$ec_iPortfolio['export_service_record'] = "匯出服務紀錄";
$ec_iPortfolio['export_comment_record'] = "匯出老師評語";

$ec_iPortfolio['female'] = "女";
$ec_iPortfolio['male'] = "男";
$ec_iPortfolio['institute'] = "院校名稱";
$ec_iPortfolio['doc'] = "完成日期";
$ec_iPortfolio['keiwai_period'] = "時段";
$ec_iPortfolio['keiwai_classlevel'] = "班別";
$ec_iPortfolio['keiwai_position_c'] = "全班名次";
$ec_iPortfolio['keiwai_totalnumber_c'] = "全班人數";
$ec_iPortfolio['keiwai_position_l'] = "全級名次";
$ec_iPortfolio['keiwai_totalnumber_l'] = "全級人數";
$ec_iPortfolio['keiwai_certificate'] = "獲取Certificate";
$ec_iPortfolio['keiwai_qualification'] = "獲取資格 (包括榮譽，Classification，如適用)";
$ec_iPortfolio['keiwai_organization'] = "頒授機構";

# Warning message
$ec_warning['comment_content'] = "請輸入評語。";
$ec_warning['comment_submit_confirm'] = "你是否確定呈送評語？";
$ec_iPortfolio['suspend_account_confirm'] = "你是否確定暫定啟用所選戶口？";
$ec_iPortfolio['suspend_template_confirm'] = "你是否確定暫停使用所選模板？";
$ec_iPortfolio['active_template_confirm'] = "你是否確定重新啟用所選模板？";
$ec_iPortfolio['delete_template_confirm'] = "你是否確定刪除所選模板？";
$ec_iPortfolio['used_template_cant_delete'] = "*注意：模板被使用中，無法刪除。";
$ec_warning['start_end_year'] = "結束年份不可早於開始年份";
$ec_warning['start_end_hours'] = "小時上限不能小於小時下限";
$ec_warning['please_enter_pos_integer'] = "請輸入正整數值";
$ec_iPortfolio['delete_attachment_confirm'] = "你是否確定刪除所選附件";

# for semester sort name
$ec_iPortfolio['semester_name_array_1'] = array(1, '一', '上', 'first', '1st Semester');
$ec_iPortfolio['semester_name_array_2'] = array(2, '二', '中', 'second', '2nd Semester');
$ec_iPortfolio['semester_name_array_3'] = array(3, '三', '下', 'third', '3rd Semester');

$ec_student_word['registration_no'] = "WebSAMS學生註冊編號";
$ec_student_word['name_english'] = "英文姓名";
$ec_student_word['name_chinese'] = "中文姓名";

$MyInfo = "我的資料";
$no_record_msg = "未有紀錄。";

$button_activate_iPortfolio = "啟用";
$button_publish_iPortfolio = "發佈";
$button_apply = "套用";
$button_save_and_publish_iPortfolio = "儲存並發佈";
$i_general_others = "其他";
$i_status_shared = "已分享";
$i_list_all = "顯示全部";
$i_general_settings = "設定";

$msg_check_at_least_one = "請選擇至少一個項目。";
$msg_check_at_most_one = "只可選擇其中一項。";

$profile_gender = "性別";
$profile_dob = "出生日期";
$profile_nationality = "國籍";
$profile_pob = "出生地點";

$ec_iPortfolio['age'] = "年齡";
$ec_iPortfolio['primarySchool'] = "小學";

  $ec_iPortfolio['existing_format'] = "現存格式";
  $ec_iPortfolio['convert_to'] = "轉換到";

	$ec_iPortfolio['eClass_update_assessment'] = "從eClass更新學業成績";
	$ec_iPortfolio['eClass_update'] = "與eClass學生檔案同步化紀錄";
	$ec_iPortfolio['eClass_update_merit'] = "從eClass更新獎懲紀錄";
	$ec_iPortfolio['eClass_update_activity'] = "從eClass更新活動紀錄";
	$ec_iPortfolio['eClass_update_attendance'] = "從eClass更新考勤紀錄";
	$ec_iPortfolio['eClass_update_award'] = "從eClass更新獎項紀錄";
	$ec_iPortfolio['eClass_update_comment'] = "從eClass更新教師評語";
	$ec_iPortfolio['update_more_assessment'] = "再次更新學業成績";
	$ec_iPortfolio['update_more_award'] = "再次更新獎項紀錄";
	$ec_iPortfolio['update_more_activity'] = "再次更新活動紀錄";
	$ec_iPortfolio['update_more_attendance'] = "再次更新考勤紀錄";
	$ec_iPortfolio['update_more_merit'] = "再次更新獎懲紀錄";
	$ec_iPortfolio['update_more_comment'] = "再次更新教師評語";
	$ec_iPortfolio['SAMS_last_update'] = "最近更新";
	$ec_iPortfolio['SAMS_last_record_change'] = "最近紀錄變更時間";
	$ec_iPortfolio['Record_Approval_Note'] = "註: 如上一次同步化後並未有新增或變更的紀錄，\"最近紀錄變更時間\"將維持不變。";
	$ec_iPortfolio['activation_no_quota'] = "戶口配額不足！";
	$ec_iPortfolio['SAMS_code'] = "編號";
	$ec_iPortfolio['SAMS_cmp_code'] = "Component Code";
	$ec_iPortfolio['SAMS_description'] = "簡介(英/中)";
	$ec_iPortfolio['SAMS_abbr_name'] = "縮寫(英/中)";
	$ec_iPortfolio['SAMS_short_name'] = "簡寫(英/中)";
	$ec_iPortfolio['SAMS_display_order'] = "顯示次序";
	$ec_iPortfolio['SAMS_subject'] = "科目";
	$ec_iPortfolio['SAMS_short_name_duplicate'] = "簡寫不可重複。";
	$ec_iPortfolio['WebSAMSRegNo'] = "WebSAMS 學生註冊編號";

	$ec_iPortfolio['record_submitted'] = "已呈送獎項紀錄";
	$ec_iPortfolio['record_approved'] = "已批核獎項紀錄";
	$ec_iPortfolio['house'] = "社";
	$ec_iPortfolio['pending'] = "等候批核";
	$ec_iPortfolio['approved'] = "已批核";
	$ec_iPortfolio['rejected'] = "已拒絕";
	$ec_iPortfolio['accept'] = "接受";
	$ec_iPortfolio['approve'] = "批核";
	$ec_iPortfolio['reject'] = "拒絕";
	$ec_iPortfolio['status'] = "狀況";
	$ec_iPortfolio['source'] = "Source";
	$ec_iPortfolio['student_apply'] = "學生申請";
	$ec_iPortfolio['teacher_submit_record'] = "教師呈送紀錄";
	$ec_iPortfolio['school_record'] = "學校紀錄";
	//$ec_iPortfolio['process_date'] = "已批核日期";
	$ec_iPortfolio['process_date'] = "處理日期";
	$ec_iPortfolio['submit_external_award_record'] = "呈送External獎項紀錄";
	$ec_iPortfolio['total'] = "總分";
	$ec_iPortfolio['grade'] = "等級";
	$ec_iPortfolio['score'] = "分數";
	$ec_iPortfolio['stand_score'] = "標準分數";
	$ec_iPortfolio['rank'] = "全級名次";
	$ec_iPortfolio['conduct_grade'] = "操行";
	$ec_iPortfolio['comment_chi'] = "中文評語";
	$ec_iPortfolio['comment_eng'] = "英文評語";

	$ec_iPortfolio['all_status'] = "全部狀態";
	$ec_iPortfolio['all_category'] = "全部類別";
	$ec_iPortfolio['all_tag'] = "全部增補類別";
	$ec_iPortfolio['tag'] = "增補類別";
	$ec_iPortfolio['title'] = "標題";
	$ec_iPortfolio['chinese'] = "中文";
	$ec_iPortfolio['english'] = "英文";
	$ec_iPortfolio['category'] = "類別";
	$ec_iPortfolio['category_code'] = "類別編號";
	$ec_iPortfolio['achievement'] = "獎項 / 證書 / 成果";
	$ec_iPortfolio['organization'] = "合作機構";
	$ec_iPortfolio['details'] = "詳情";
	$ec_iPortfolio['approved_by'] = "批核者";
	$ec_iPortfolio['attachment'] = "附件 / 證明";
	$ec_iPortfolio['startdate'] = "開始日期";
	$ec_iPortfolio['enddate']= "結束日期";
	$ec_iPortfolio['or'] = "或";
	$ec_iPortfolio['add_enddate'] = "新增結束日期";
	$ec_iPortfolio['unit_hour'] = "小時";
	$ec_iPortfolio['school_remarks'] = "學校備註";
	$import_csv['download'] = "下載範例 ";
	$ec_iPortfolio['customized_file_upload_remind'] = "注意：上載檔案需以對應年份命名。 (例如：2006.csv ) ";
	$ec_iPortfolio['make_zip_download'] = "製作 ZIP 檔及下載";

	$ec_iPortfolio['enter_title_name'] = "輸入標題";

	$ec_warning['date'] = "請輸入日期";
	$w_alert['Invalid_Date'] = "無效日期";
	$w_alert['Invalid_Time'] = "時間無效";
	$w_alert['Invalid_Lang'] = "請選擇語言";
	$ec_warning['title'] = "請輸入名稱";
	$ec_warning['chiTitle'] = "請輸入中文名稱";
	$ec_warning['engTitle'] = "請輸入標題";
	$ec_iPortfolio['upload_error'] = "請輸入有效檔案路徑";

	$ec_iPortfolio_ele_array = array("[ID]"=>"智育發展",
									"[MCE]"=>"德育及公民教育",
									"[CS]"=>"社區服務",
									"[PD]"=>"體育發展",
									"[AD]"=>"美育發展",
									"[CE]"=>"職業相關經驗",
									"[OTHERS]"=>"其他");
	$ec_iPortfolio['type_competition'] = "比賽";
	$ec_iPortfolio['type_activity'] = "活動";
	$ec_iPortfolio['type_course'] = "課程";
	$ec_iPortfolio['type_service'] = "服務";
	$ec_iPortfolio['type_award'] = "獎項";
	$ec_iPortfolio['type_others'] = "其他";
	$ec_iPortfolio_category_array = array("1"=>$ec_iPortfolio['type_competition'],
										"2"=>$ec_iPortfolio['type_activity'],
										"3"=>$ec_iPortfolio['type_course'],
										"4"=>$ec_iPortfolio['type_service'],
										"5"=>$ec_iPortfolio['type_award'],
										"6"=>$ec_iPortfolio['type_others']);
	$ec_iPortfolio['waiting_approval']= "等候批核";
	$ec_iPortfolio['program']= "項目";
	$ec_iPortfolio['program_list']= "項目列表";
	$ec_warning['olr_from_teacher'] = "由教師提交的紀錄，毋需批核！";
	$ec_iPortfolio['student_list']= "學生列表";
	$ec_iPortfolio['assign_more_student']= "指定學生";
	$ec_iPortfolio['no_record_found'] = "沒有紀錄。";
	$ec_iPortfolio['no_privilege_to_read_file'] = "你並無檢視該檔案的權限。";
	$ec_iPortfolio['new_ole_activity'] = "新增其他學習經驗活動";
	$ec_iPortfolio['assign_student_now'] = "立即指定學生";
	$ec_iPortfolio['comment'] = "評語";

	$ec_iPortfolio['lang_chi'] = "中文";
	$ec_iPortfolio['lang_eng'] = "英文";
	$ec_iPortfolio['total_hours'] = "總時數";
	$ec_iPortfolio['olr_display_record'] = "顯示紀錄";
	$ec_iPortfolio['olr_display_stat'] = "顯示統計";
	$ec_iPortfolio['olr_reference_guide'] = "請參照以下的代號填寫種類及其他學習經歷範疇：";
	$ec_iPortfolio['default_ele'] = "預設其他學習經歷範疇";
	$ec_iPortfolio['ele_setting'] = "其他學習經歷範疇設定";
	$ec_iPortfolio['ele_code'] = "其他學習經歷範疇代號";
	$ec_iPortfolio['category_setting'] = "種類設定";
	$ec_iPortfolio['default'] = "預設";
	$ec_iPortfolio['olr_display_analysis'] = "達標分析";
	$ec_iPortfolio['olr_stas_remark'] = "注意：部份紀錄可能包含幾個其他學習經歷其他學習經歷範疇能少於其他學習經歷範疇的時數總和。";
	$ec_iPortfolio['hours_or_above'] = "小時或以上";
	$ec_iPortfolio['hours_or_below'] = "小時或以下";
	$ec_iPortfolio['hours_range'] = "時數範圍";
//	$ec_iPortfolio['view_analysis'] = "檢視分析";
	$ec_iPortfolio['view_analysis'] = "檢視";
	$ec_iPortfolio['chart_report'] = "時數統計圖";
	$ec_iPortfolio['student_year_accum_chart'] = "學生年度累積";
	$ec_iPortfolio['class_stat_chart'] = "班別統計";
	$ec_iPortfolio['cat_stat_chart'] = "活動種類統計圖";

	$ec_iPortfolio['growth_title'] = "名稱";
	$ec_iPortfolio['growth_description'] = "簡介";
	$ec_iPortfolio['growth_group'] = "參予小組";
	$ec_iPortfolio['growth_status'] = "狀況";
	$ec_iPortfolio['growth_phase'] = "階段";
	$ec_iPortfolio['growth_scheme_title'] = "計劃名稱";
	$ec_iPortfolio['growth_phase_title'] = "階段名稱";
	$ec_iPortfolio['growth_phase_period'] = "時段";
	$ec_iPortfolio['growth_target_person'] = "參予者";
	$ec_iPortfolio['growth_new_phase'] = "新增階段";
	$ec_iPortfolio['growth_edit_phase'] = "編輯階段";
	$ec_iPortfolio['growth_relevant_phase'] = "相關階段";
	$ec_iPortfolio['growth_online_form'] = "表格";
	$ec_iPortfolio['growth_contents'] = "內容";
	$ec_iPortfolio['growth_info'] = "資料性";
	$ec_iPortfolio['growth_create'] = "製作";
	$ec_iPortfolio['form_display_v'] = "問題形式";
	$ec_iPortfolio['form_display_h'] = "簡潔形式";
	$ec_iPortfolio['form_fill'] = "填表";
	$ec_iPortfolio['form_not_answered'] = "沒作答";
	$ec_iPortfolio['weblog_date'] = "日期";
	$ec_iPortfolio['weblog_title'] = "標題";
	$ec_iPortfolio['weblog_contents'] = "內容";
	$ec_iPortfolio['weblog_config'] = "設定顯示形式";
	$ec_iPortfolio['weblog_config_way'] = "列出形式";
	$ec_iPortfolio['weblog_config_all'] = "全部";
	$ec_iPortfolio['weblog_config_list_year'] = "按年份";
	$ec_iPortfolio['weblog_config_list_month'] = "按年月";
	$ec_iPortfolio['weblog_config_order'] = "時間排序";
	$ec_iPortfolio['weblog_config_order_A'] = "順序";
	$ec_iPortfolio['weblog_config_order_D'] = "倒序";
	$ec_iPortfolio['weblog_config_layout'] = "版面編排";
	$ec_iPortfolio['no_record_exist'] = "暫時沒有紀錄";
	$ec_iPortfolio['web_structure_changed'] = "此學習檔案已被更改！";
	$ec_iPortfolio['edit_template_alert'] = "注意：此頁面正在使用模板 \'XXX\'，對此頁面所作的任何改動，將反映到其他採用相同模板的頁面。如只想修改此頁面, 請建立一個新模板。";

	$ec_iPortfolio['growth_answer_display_mode'] = "答案顯示形式";
	$ec_iPortfolio['growth_display_answer_only'] = "只顯示答案";
	$ec_iPortfolio['growth_display_answer_format'] = "顯示答案及全部選擇";
	$ec_iPortfolio['print_result'] = "列印結果";

	$ec_iPortfolio['upload_photo_fail'] = "更新紀錄失敗! 請檢查學校圖像的檔案類型。";

	$ec_iPortfolio['female'] = "女";
	$ec_iPortfolio['male'] = "男";
	$ec_iPortfolio['not_display_student_photo'] = "不顯示學生相片";
	$ec_iPortfolio['phase_in_processing_state'] = "注意：此階段仍然在進行中，現在顯示的答案不等於最後呈交之答案。";
	$ec_iPortfolio['growth_not_submitted'] = "未呈交";

	$ec_iPortfolio_guide['alert_student'] = "你可以電郵提示學生呈交。";
	$ec_iPortfolio_guide['alert_class_teacher'] = "你可以電郵提示班主任呈交。";
	$ec_iPortfolio_guide['alert_subject_teacher'] = "你可以電郵提示科目老師呈交。";
	$ec_iPortfolio_guide['alert_parent'] = "你可以電郵提示家長呈交。";
	$ec_iPortfolio['distinct_form_desc'] = "簡潔格式: 評估問題和答案分左右顯示。<br />問題形式: 評估問題和答案分上下顯示。";

	$ec_iPortfolio['no_student_info'] = "不顯示學生資料";
	$ec_iPortfolio['no_student_details'] = "不顯示學生詳細資料";
	$ec_iPortfolio['no_school_name'] = "不顯示校名於頁首";
	$ec_iPortfolio['no_related_phase'] = "不顯示相關階段";

	$ec_iPortfolio['people'] = "人";
	$ec_iPortfolio['updated'] = "個更新";
	$ec_iPortfolio['revise_result'] = "修改結果";

	$ec_guardian['01'] = "父親";
	$ec_guardian['02'] = "母親";
	$ec_guardian['03'] = "祖父";
	$ec_guardian['04'] = "祖母";
	$ec_guardian['05'] = "兄/弟";
	$ec_guardian['06'] = "姊/妹";
	$ec_guardian['07'] = "親戚";
	$ec_guardian['08'] = "其他";

	$ec_iPortfolio['relation'] = "關係";
	$ec_iPortfolio['phone'] = "電話號碼";
	$ec_iPortfolio['em_phone'] = "緊急聯絡電話";

	$ec_iPortfolio['school_profile_mgt'] = "學校檔案管理";
	$ec_iPortfolio['web_teacher_mgt'] = "學習檔案管理";
	$ec_iPortfolio['growth_mgt'] = "校本計劃管理";
	$ec_iPortfolio['web_view'] = "學習檔案";

	$ec_iPortfolio['growth_view'] = "校本計劃";
	$ec_iPortfolio['school_profile'] = "學校紀錄";
	$ec_iPortfolio['my_learning_sharing'] = "我的學習檔案";
	$ec_iPortfolio['my_learning_sharing_mgt'] = "我的學習檔案管理";

	$ec_iPortfolio['growth_management'] = "管理計劃";
	$ec_iPortfolio['growth_work_on'] = "參予計劃";
	$ec_iPortfolio['scheme'] = "計劃";

	$ec_iPortfolio['login_teacher'] = "老師登入";
	$ec_iPortfolio['login_student'] = "學生登入";
	$ec_iPortfolio['login_parent'] = "家長登入";

	$ec_iPortfolio['sys_manage'] = "進入管理";
	$ec_iPortfolio['sys_view'] = "返回檢視";

	$ec_iPortfolio['greeting'] = "歡迎使用學生綜合檔案";
	$ec_iPortfolio['class_directory'] = "iPortfolio 班別目錄";
	$ec_iPortfolio['class_directory_intro'] = "您可從班別目錄，以漸進式選擇要找尋的學生檔案；或使用搜尋工具，準確地搜尋你需要的學生檔案。";
	$ec_iPortfolio['growth_student_name'] = "學生姓名";


	$ec_iPortfolio['growth_work'] = "可填寫";
	$ec_iPortfolio['student_photo'] = "學生相片";
	$ec_iPortfolio['student_photo_no'] = "沒有相片";
	$ec_iPortfolio['preferred_approver'] = "要求的批核者 ";

	### CCYM customarized report ###
	$ec_iPortfolio['ccym_ole_report'] = "其他學習經歷 報告";

# Language for Student Learning Profile
$ec_iPortfolio['SLP']['EditableFields']			= "可更改欄位";
$ec_iPortfolio['SLP']['AllowStudentsToJoin']	= "允許學生申報";
$ec_iPortfolio['SLP']['Yes']					= "是";
$ec_iPortfolio['SLP']['No']						= "否";
$ec_iPortfolio['SLP']['JoinPeriod']				= "報名時段";
$ec_iPortfolio['SLP']['PeriodStart']			= "開始";
$ec_iPortfolio['SLP']['PeriodEnd']				= "結束";
$ec_iPortfolio['SLP']['InvalidPeriodStart']		= "開始時段格式錯誤";
$ec_iPortfolio['SLP']['InvalidPeriodEnd']		= "結束時段格式錯誤";
$ec_iPortfolio['SLP']['PeriodStart_LT_PeriodEnd']	= "開始時段大於結束時段";
$ec_iPortfolio['SLP']['From']					= "從";
$ec_iPortfolio['SLP']['to']						= "到";
$ec_iPortfolio['SLP']['now']					= "現在";
$ec_iPortfolio['SLP']['forever']				= "永遠";
$ec_iPortfolio['SLP']['button_Join']			= "加入";
$ec_iPortfolio['SLP']['Title']					= "標題";
$ec_iPortfolio['SLP']['SchoolYear']				= "學校年份";
$ec_iPortfolio['SLP']['Category']				= "分類";
$ec_iPortfolio['SLP']['FillTogether']			= "開始時段與結束時段必需一同填入";
$ec_iPortfolio['SLP']['Status']					= "狀態";
$ec_iPortfolio['SLP']['ApprovedDate']			= "批獲日期";
$ec_iPortfolio['SLP']['CreatedBy']				= "創建者";
$ec_iPortfolio['SLP']['AutoApprove']			= "允許自動認可";
$ec_iPortfolio['SLP']['warning_Hour']			= "時數格式錯誤";
$ec_iPortfolio['SLP']['TeacherCreatedProgram']	= "老師建立的活動";
$ec_iPortfolio['SLP']['StudentCreatedProgram']	= "學生建立的活動";
$ec_iPortfolio['SLP']['JoinableYear']			= "適用班級";
$ec_iPortfolio['SLP']['ChooseAtLeastOneYear']	= "請選擇最少一個級別";
$ec_iPortfolio['SLP']['ReportOtherActivities']	= "申報紀錄 ";
$ec_iPortfolio['SLP']['SchoolPresetActivities']	= "學校預設活動(按標題進行申報)";
$ec_iPortfolio['SLP']['OleJoinNotes1']			= "註：若要申報的活動已顯示於<b>學校預設活動</b>內，請按活動標題進行申報。";
$ec_iPortfolio['SLP']['OleJoinNotes2']			= "否則，請按<b>自行申報紀錄</b>進行申報。";

$ec_iPortfolio['SLP']['OleJoinNotes']['WithinSubmissionPeriod'] = "註：若要申報的活動已顯示於<b>學校預設活動</b>內，請按活動標題進行申報。 否則，請按<b>新增</b>自行申報紀錄。";
$ec_iPortfolio['SLP']['OleJoinNotes']['WithoutSubmissionPeriod'] = "註：自行<b>申報紀錄</b>的時限已過，用戶不能新增其他活動。但若要申報的活動已顯示於<b>學校預設活動</b>內，請按活動標題進行申報。";

$ec_iPortfolio['SLP']['OLE_approval_mail']['title'] = "來自  [StudentName] 的 OLE 紀錄審批要求 [[DateNow]] ";
$ec_iPortfolio['SLP']['OLE_approval_mail']['body'] = "<p>[StudentName] 於 [DateNow] 申報了一個新的 OLE 紀錄 : \"[Title]\"。</p>" .
					"<p>你可以 <a href=\"<!--schoolUrl-->/home/portfolio/teacher/management/oleIndex.php?clearCoo=1\" target=\"OLE_approval\">按這裡</a> 直接進入審批版面 (需要帳戶登入)。</p>";


$ec_iPortfolio['SLP']['ExportAsPdf']			= "匯出為PDF 檔";
$ec_iPortfolio['SLP']['ExportAsWordDocument']			= "匯出為Word 檔";
$ec_iPortfolio['SLP']['Name'] = "名稱";
$ec_iPortfolio['SLP']['MoveTo'] = "移到";
$ec_iPortfolio['SLP']['SubCategory'] = "子分類";
$ec_iPortfolio['NoPrintToLimit'] = "如學生沒有指定要列印的紀錄，則不列印任何紀錄。";
$ec_iPortfolio['get_prepared_portfolio_ST'] = "請下載檔案";

$ec_iPortfolio['all_subcategory'] = "所有子分類";
$ec_iPortfolio['view_peer_profolio'] = "檢視同學學習檔案";
$ec_iPortfolio['View'] = "檢視";
$ec_iPortfolio['learning_portfolio'] = "的學習檔案";

$ec_iPortfolio['NoRecordsUpdated'] = "沒有紀錄更新";
$ec_iPortfolio['DoesNotHaveDefaultSelfAccount'] = "沒有預設學生自述";


#Start from 20100323 , all new iportfolio Lang should with $Lang (IP25 convention)
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][0] = 'ClassName';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][1] = 'ClassNumber';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][2] = 'WebSAMSRegNo';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][3] = 'Title';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'][4] = 'SelfAccount';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][0] = '班別';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][1] = '班號';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][2] = 'WebSAMS學生註冊編號';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][3] = '題目';
$Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'][4] = '學生自述';
$Lang['iPortfolio']['noAccessRightRedirectMsg'] = '系統管理員未有設定你的 iPortfolio 使用權限。\n請按確定返回主頁。';
$Lang['iPortfolio']['redirecting'] = '返回中...';
$Lang['iPortfolio']['standAloneWithoutAccount']	= '你的iPortfolio帳戶尚未啟動。請聯絡你的系統管理員進行啟動。';
$Lang['iPortfolio']['wholeYearIncludeNullSem']	= '全年(包括沒有指定學期的紀錄)';

$Lang['iPortfolio']['PleaseInputPositiveNumber'] = "請輸入正數。";
$Lang['iPortfolio']['alertYearExceed'] = "你輸入的年份過大，請從新輸入。";
$Lang['iPortfolio']['alertExceedWordLimit'] = "你輸入的字數過多。你最多可輸入 <!--WordLimit--> 字(不計算中文字符)";
$Lang['iPortfolio']['alertLeavePage'] = "資料處理進行中，請勿關閉瀏覽器或瀏覽其他頁面。";
$Lang['iPortfolio']['allowed'] = "允許";
$Lang['iPortfolio']['allRecord'] = "所有紀錄";
$Lang['iPortfolio']['all_content'] = "所有內容";
$Lang['iPortfolio']['advanceSetting'] = "進階設定";
$Lang['iPortfolio']['approver'] = "批核者";
$Lang['iPortfolio']['InputBy'] = "創建者";
$Lang['iPortfolio']['approvaldate'] = "批核日期";
$Lang['iPortfolio']['auditby'] = "審核者";
$Lang['iPortfolio']['auditdate'] = "審核日期";

$Lang['iPortfolio']['cd_burning_setting_forparent'] = "允許家長自行燒錄CD-ROM";
$Lang['iPortfolio']['cd_burning_setting_forstudent'] = "允許學生自行燒錄CD-ROM";
$Lang['iPortfolio']['commonAC'] = "共通能力 / 主要範疇";
$Lang['iPortfolio']['custReport'] = "訂製報告";

$Lang['iPortfolio']['default'] = "預設";

$Lang['iPortfolio']['Group'] = "小組";
$Lang['iPortfolio']['GroupSettingRemark'] = "此頁面的設定為檢視權限。";
$Lang['iPortfolio']['GroupBasic'] = "基本資料";
$Lang['iPortfolio']['GroupID'] = "小組 ID";
$Lang['iPortfolio']['GroupList'] = "小組名單";
$Lang['iPortfolio']['GroupList_instruction'] = "小組及老師成員可在 設定 > 小組 > 管理小組 設定";
$Lang['iPortfolio']['GroupMemberName'] = "成員名稱";
$Lang['iPortfolio']['GroupName'] = "小組名稱";
$Lang['iPortfolio']['GroupRight'] = "權限設定";
$Lang['iPortfolio']['GroupSetting'] = "小組設定";

$Lang['iPortfolio']['LastPublishDate'] = "最後發佈日期";
$Lang['iPortfolio']['LPnotOpened'] = "未有檢視及編輯過的學習檔案";
$Lang['iPortfolio']['LPnotPublished'] = "沒有發佈過內容的學習檔案";
$Lang['iPortfolio']['LPnoWordDoc'] = "Word 文件不存在，請再次發佈";
$Lang['iPortfolio']['LPSetting'] = "學習檔案設定";
$Lang['iPortfolio']['LP_Rights']['learning_portfolio_content'] = "可存取 <strong>管理 > 學習檔案 > 學習檔案管理</strong> 分頁";
$Lang['iPortfolio']['LP_Rights']['manage_templates'] = "可存取 <strong>管理 > 學習檔案 > 模板管理</strong> 分頁";

$Lang['iPortfolio']['mgmtGroup'] = "管理小組";

$Lang['iPortfolio']['NumGroupMember'] = "成員數目";

$Lang['iPortfolio']['onlyPortfolioUser'] = "以下只顯示擁有iPortfolio使用權的用戶。";
$Lang['iPortfolio']['orderby'] = "排序";
$Lang['iPortfolio']['orderbyName'] = "依姓名";
$Lang['iPortfolio']['orderbyClass'] = "依班別";
$Lang['iPortfolio']['orderbyLastPublishedDate'] = "依最近發佈日期";

$Lang['iPortfolio']['preferred_approver'] = "要求的批核者";
$Lang['iPortfolio']['default_approver'] = "要求的批核者";
$Lang['iPortfolio']['prev_published_content'] = "曾經發佈過的內容";
$Lang['iPortfolio']['print_date_issue'] = "顯示發出日期";
$Lang['iPortfolio']['print_slp_lang'] = "標題語言";
$Lang['iPortfolio']['print_slp_lang_en'] = "英文";
$Lang['iPortfolio']['print_slp_lang_b5'] = "中文";
$Lang['iPortfolio']['prohibit'] = "禁止";
$Lang['iPortfolio']['publish_student_complete'] = "所選學生的內容已發佈";
$Lang['iPortfolio']['publish_student_content'] = "你是否確定發佈所選學生的內容?";

$Lang['iPortfolio']['resetButton'] = "重設按鈕";
$Lang['iPortfolio']['disable_facebook'] = "Facebook 讚好及分享功能";
$Lang['iPortfolio']['restrictToComponent'] = "只限所選範疇的紀錄：";

$Lang['iPortfolio']['page_break_ywgs'] = "如有需要，可以在自述中輸入 \"[PAGE-BREAK]\" 來分開兩頁，以便列印。";
$Lang['iPortfolio']['selectedRecord'] = "已選擇紀錄";
$Lang['iPortfolio']['selfAccountWordCount'][0] = "已輸入:";
$Lang['iPortfolio']['selfAccountWordCount'][1] = "個英文字。";
$Lang['iPortfolio']['selfAccountIsEmpty'] = "請輸入內容";
$Lang['iPortfolio']['Settings_BurningCD'] = "光碟燒錄";
$Lang['iPortfolio']['Settings_Grouping'] = "小組管理";
$Lang['iPortfolio']['SLPRecord'] = "學生學習概覽紀錄";
$Lang['iPortfolio']['SpaceBottom'] = "下間隔調整";
$Lang['iPortfolio']['SpaceTop'] = "上間隔調整";
$Lang['iPortfolio']['spacingHeight'] = "間隔調整";
$Lang['iPortfolio']['SP_Rights']['assessment_stat_report'] = "可檢視「評估統計報告」";
$Lang['iPortfolio']['SP_Rights']['data_import'] = "可存取 <strong>管理 > 學生學習概覽 > 學校紀錄</strong> 分頁";
$Lang['iPortfolio']['SP_Rights']['export_slp_data'] = "可由 <strong>管理 > 學生學習概覽</strong> 匯出紀錄";
$Lang['iPortfolio']['SP_Rights']['management_student'] = "可存取 <strong>管理 > 個人帳戶</strong> 頁面";
$Lang['iPortfolio']['SP_Rights']['student_ole_manage'] = "可檢視及批核全部或所選其他學習經歷紀錄";
$Lang['iPortfolio']['SP_Rights']['student_report_printing'] = "可列印「學生報告」";
$Lang['iPortfolio']['SP_Rights']['student_report_setting'] = "可存取 <strong>設定 > 報告</strong> 頁面";
$Lang['iPortfolio']['SP_Rights']['self_added_record_delete'] = "在  <strong>管理 > 學生學習概覽 > 其他學習經歷</strong> 或 <strong>校外表現/獎項及重要參與 > 項目</strong> 頁面, 只可以批量管理自己增加的紀錄";
$Lang['iPortfolio']['studentGroup'] = "學生小組";
$Lang['iPortfolio']['StudentViewDisplay'] = "學生介面顯示";
$Lang['iPortfolio']['SubmissionSetting'] = "提交時段設定";
$Lang['iPortfolio']['OverallResult'] = "全年總成績";
$Lang['iPortfolio']['MarkDifference'] = "分數差別";
$Lang['iPortfolio']['MarkDifference(%)'] = "分數差別(%)";
$Lang['iPortfolio']['ZScoreDifference'] = "標準分數差別";
$Lang['iPortfolio']['ZScoreDifference(%)'] = "標準分數差別(%)";
$Lang['iPortfolio']['ExpectedMark'] = "預期分數";
$Lang['iPortfolio']['ExpectedMarkDifference'] = "預期分數差別";
$Lang['iPortfolio']['RegressionLineEquation'] = "Regression Line 方程式：b0 + b1(S1x)";
$Lang['iPortfolio']['AcademicProgress'] = "成績進度";
$Lang['iPortfolio']['AcademicProgressByStd'] = "班別歷年成績";
$Lang['iPortfolio']['AssessmentResultOfStd'] = "個別學生成績";
$Lang['iPortfolio']['SubjectStats'] = "學科統計";
$Lang['iPortfolio']['TeacherStats'] = "任教合格統計";
$Lang['iPortfolio']['ImprovementStatsBySbj'] = "學科進步統計";
$Lang['iPortfolio']['ImprovementStatsByTeacher'] = "任教進步統計";
$Lang['iPortfolio']['InternalValueAdded'] = "校內增值指標";
$Lang['iPortfolio']['DseStats'] = "文憑試成績分析";
$Lang['iPortfolio']['teacherGroup'] = "教師";
$Lang['iPortfolio']['FunctionAccessRight'] = "功能權限設定";

$Lang['iPortfolio']['ChangeClassCompare'] = "調班後成績比較";

$Lang['iPortfolio']['TeacherComment'] = "導師評語";
$Lang['iPortfolio']['TeacherTutor'] = "導師";
$Lang['iPortfolio']['NotActivatedPortfolioStudent'] = "表示該用戶沒有啟動電子學習檔案。";

$Lang['iPortfolio']['SubjectFullMark'] = "學科滿分";
$Lang['iPortfolio']['SubjectFullMarkInputTips'] = "你可先從Excel檔案複製滿分資料，然後貼上在下面的文字盒。";
$Lang['iPortfolio']['SubjectFullMarkInputWarning'] = "如輸入學科滿分等級，SLP報告將顯示該學科的滿分等級而非滿分分數。";
$Lang['iPortfolio']['LoadAllFullMarkWarning'] = "檢視所有科目總分可能需時 (視乎學科及班級的數目而定)，你是否確定要檢視所有科目總分？";
$Lang['iPortfolio']['Subject_En'] = "Subject";
$Lang['iPortfolio']['Subject_Ch'] = "學科";
$Lang['iPortfolio']['Mark_En'] = "Mark";
$Lang['iPortfolio']['Mark_Ch'] = "分數";
$Lang['iPortfolio']['FullMark_En'] = "Full_Mark";
$Lang['iPortfolio']['FullMark_Ch'] = "滿分";
$Lang['iPortfolio']['PassMark_En'] = "Pass_Mark";
$Lang['iPortfolio']['PassMark_Ch'] = "合格分";
$Lang['iPortfolio']['Grade_En'] = "Grade";
$Lang['iPortfolio']['Grade_Ch'] = "等級";
$Lang['iPortfolio']['GeneralSettings'] = "一般設定";
$Lang['iPortfolio']['AdvancedSettings'] = "進階設定";
$Lang['iPortfolio']['Display'] = "顯示";
$Lang['iPortfolio']['Filter'] = "篩選";
$Lang['iPortfolio']['DataColumn'] = "資料欄";
$Lang['iPortfolio']['DisplayCriteria'] = "顯示條件";
$Lang['iPortfolio']['AcademicResult'] = "學業成績";
$Lang['iPortfolio']['MarkOnly'] = "只顯示分數";
$Lang['iPortfolio']['GradeOnly'] = "只顯示等級";
$Lang['iPortfolio']['MarkThenGrade'] = "先顯示分數，如沒有分數則顯示等級";
$Lang['iPortfolio']['GradeThenMark'] = "先顯示等級，如沒有等級則顯示分數";
$Lang['iPortfolio']['AcademicResult_Mark'] = "學業成績 (".$Lang['iPortfolio']['MarkOnly'].")";
$Lang['iPortfolio']['AcademicResult_Grade'] = "學業成績 (".$Lang['iPortfolio']['GradeOnly'].")";
$Lang['iPortfolio']['AcademicResult_MarkThenGrade'] = "學業成績 (".$Lang['iPortfolio']['MarkThenGrade'].")";
$Lang['iPortfolio']['AcademicResult_GradeThenMark'] = "學業成績 (".$Lang['iPortfolio']['GradeThenMark'].")";
$Lang['iPortfolio']['DisplayOptions'] = "顯示選項";
$Lang['iPortfolio']['DisplayFullMark'] = "顯示滿分";
$Lang['iPortfolio']['DisplayComponentSubject'] = "顯示科目分卷";
$Lang['iPortfolio']['DisplaySelfAccount'] = "顯示學生自述";
$Lang['iPortfolio']['DisplayReportHeader'] = "顯示校徽和校名";
$Lang['iPortfolio']['DisplayReportFooter'] = "顯示頁尾";
$Lang['iPortfolio']['LowerForm'] = "初中";
$Lang['iPortfolio']['HigherForm'] = "高中";
$Lang['iPortfolio']['CannotSameYear'] = "學年不能相同";

$Lang['iPortfolio']['oleByCreator'] = "依建立者檢視";
$Lang['iPortfolio']['oleOwn'] = "我建立的學習項目";
$Lang['iPortfolio']['oleSourceByApprovalRequest'] = "依要求審批者檢視";
$Lang['iPortfolio']['oleSourceByIndividualStudent'] = "個別學生的要求";
$Lang['iPortfolio']['oleSourceFromMyClass'] = "任教班別的要求";
$Lang['iPortfolio']['oleSourceFromMyGroup'] = "所屬OLE小組的要求";

$Lang['iPortfolio']['OLEArr']['ActivateStudentWarning'] = "啟動學生戶口後會減少剩餘戶口的數目，而剩餘戶口的數目將不能還原。如確定，請按呈送。";

$Lang['iPortfolio']['DynamicReport']['EmptyDataSymbol'] = "空白資料符號";
$Lang['iPortfolio']['DynamicReport']['IssueDate'] = "發出日期";
$Lang['iPortfolio']['DynamicReport']['IssueDateGuide'] = "所輸入的日期及其格式將直接顯示於報告上";
$Lang['iPortfolio']['DynamicReport']['ShowNoDataTable'] = "顯示沒有資料表格";
$Lang['iPortfolio']['DynamicReport']['NoDataTableContent'] = "表格內容";
$Lang['iPortfolio']['DynamicReport']['DataContentDisplay'] = "資料內容顯示";
$Lang['iPortfolio']['DynamicReport']['NoCriteriaSettingsRemarks'] = "已選擇的資料選項並沒有篩選設定。";
$Lang['iPortfolio']['DynamicReport']['PrintingDirection'] = "列印方向";
$Lang['iPortfolio']['DynamicReport']['Portrait'] = "直向";
$Lang['iPortfolio']['DynamicReport']['Landscape'] = "橫向";
$Lang['iPortfolio']['DynamicReport']['OrderByStudentSelectedOrdering'] = "以學生所選的\"學生學習概覽紀錄\"次序列印";
$Lang['iPortfolio']['DynamicReport']['OrderByReportSetting'] = "根據自訂報告模板之次序列印";
$Lang['iPortfolio']['DynamicReport']['DisplayFullMark'] = "顯示滿分";
$Lang['iPortfolio']['DynamicReport']['PrintHtml'] = "列印 (HTML)";
$Lang['iPortfolio']['DynamicReport']['PrintPdf'] = "列印 (PDF)";
$Lang['iPortfolio']['DynamicReport']['WarningArr']['EditDefaultTemplate'] = "如你儲存此模板，系統將自動新增一項自訂報告模板記錄。原有的預設報告模板將不受影響。";



$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['MustHaveText'] = "必須有文字";
$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['NoCriteria'] = "沒有條件";
$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['TextContains'] = "文字包含";
$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['TextDoesNotContain'] = "文字不包含";
$Lang['iPortfolio']['DynamicReport']['FilterCriteriaArr']['TextIsExactly'] = "文字等於";

$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['ShowStatistics'] = "顯示統計資料";
$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatisticsData'] = "統計資料";
$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['Blank'] = "空白";
$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatOptionAry']['sum'] = "總數";
$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['StatOptionAry']['text'] = "其他文字";

$Lang['iPortfolio']['DynamicReport']['ReleaseToStudent'] = "開放給學生";
$Lang['iPortfolio']['DynamicReport']['ReleaseStatus'] = "開放狀況";
$Lang['iPortfolio']['DynamicReport']['Release'] = "開放";
$Lang['iPortfolio']['DynamicReport']['Unrelease'] = "不開放";
$Lang['iPortfolio']['DynamicReport']['Released'] = "已開放";
$Lang['iPortfolio']['DynamicReport']['RegenerateReport'] = "重製報告";
$Lang['iPortfolio']['DynamicReport']['IssueDate'] = "發出日期";

$Lang['iPortfolio']['StudentReport']['PrintingMode'] = "列印模式";
$Lang['iPortfolio']['StudentReport']['PrintingMode_All'] = "匯出儲存所有學生報告的單一PDF檔案";
$Lang['iPortfolio']['StudentReport']['PrintingMode_Zip'] = "匯出一個獨立儲存所有學生報告的壓縮(zipped)資料夾";
$Lang['iPortfolio']['other_reports'] = "其他報告";
$Lang['iPortfolio']['OtherReports']['Title'] = "報告標題";
$Lang['iPortfolio']['OtherReports']['CreationDate'] = "呈交日期";
$Lang['iPortfolio']['OtherReports']['LastModifiedDate'] = "上次修改日期";
$Lang['iPortfolio']['OtherReports']['NumberOfItems'] = "項目數量";
$Lang['iPortfolio']['OtherReports']['ReportInformation'] = "報告詳情";
$Lang['iPortfolio']['OtherReports']['Description'] = "簡介";
$Lang['iPortfolio']['OtherReports']['TitleNotes'] = "會用作報告的檔案名稱";
$Lang['iPortfolio']['OtherReports']['AlertInputTitle'] = "請輸入報告標題。";
$Lang['iPortfolio']['OtherReports']['ReportFile'] = "檔案";
$Lang['iPortfolio']['OtherReports']['Class'] = "班別";
$Lang['iPortfolio']['OtherReports']['Student'] = "學生";
$Lang['iPortfolio']['OtherReports']['PleaseSelectss'] = "請選擇班別";
$Lang['iPortfolio']['OtherReports']['PleaseSelectStudent'] = "請選擇學生";
$Lang['iPortfolio']['OtherReports']['PleaseSelectFile'] = "請選擇檔案";
$Lang['iPortfolio']['OtherReports']['ZeroFileSize'] = "檔案不能為空白";
$Lang['iPortfolio']['OtherReports']['FileSizeExceed'] = "檔案不能大於100MB";
$Lang['iPortfolio']['OtherReports']['InvalidFileType'] = "檔案類型必須為 pdf 或 zip";
$Lang['iPortfolio']['OtherReports']['NonEmptyItems'] = '0|=|由於報告中包含項目,如你仍需要刪除,請先移除它';
$Lang['iPortfolio']['Instruction_OtherReport'] = "可在此上載非經 iPortfolio 產生的學生履歷";

$ec_iPortfolio['SLP']['MaximumHours'] = "學生申報時最大時數";
$ec_iPortfolio['SLP']['DefaultHours'] = "學生申報時預設時數";
$ec_iPortfolio['SLP']['PleaseEnterANumber'] = "請輸入數字";
$ec_iPortfolio['SLP']['ExceedingMaxumumHours'] = "超出時限最大值";
$ec_iPortfolio['SLP']['CompulsoryFields'] = "學生申報時必要填寫欄位";
$ec_iPortfolio['SLP']['DefaultApprover'] = "<span class=\"tabletextrequire\">@</span>要求的批核者 ";
$ec_iPortfolio['SLP']['PleaseFillIn'] = "請填上";
$ec_iPortfolio['Merge'] = "合併";
$ec_iPortfolio['SLP']['ProgramMergeStatement1'] = "在前一頁選定的{{{ REPLACE }}}將會合拼至以下新增的{{{ REPLACE }}}！";
$ec_iPortfolio['SLP']['ProgramMergeStatement2'] = "所有選定的{{{ REPLACE }}}將在合併後被刪除！";
$ec_iPortfolio['SLP']['InputByTeacher'] = "老師輸入";
$ec_iPortfolio['SLP']['ImportByTeacher'] = "老師滙入";
$ec_iPortfolio['SLP']['InputByStudent'] = "學生輸入";
$ec_iPortfolio['SLP']['EnrollmentTransfer'] = "從課外活動轉移";
$ec_iPortfolio['SLP']['ComeFrom'] = "來自";
$ec_iPortfolio['SLP']['PleaseSelect'] = "請選擇";

$ec_iPortfolio['management_student'] = "學生管理";
$ec_iPortfolio['data_import'] = "資料更新<font size=-1> (eClass/WEBSAMS)</font>";
$ec_iPortfolio['student_ole_manage'] = "其他學習紀錄管理";
$ec_iPortfolio['student_report_setting'] = "學生報告設定";
$ec_iPortfolio['student_report_printing'] = "學生報告列印";
$ec_iPortfolio['assessment_stat_report'] = "學業統計報告";
$ec_iPortfolio['manage_templates'] = "樣式管理";
$ec_iPortfolio['prepare_cd_burning'] = "預備 CD-ROM 燒錄";
$ec_iPortfolio['learning_sharing'] = "學習檔案";
$ec_iPortfolio['growth_scheme'] = "成長計劃";
$ec_iPortfolio['view_alumni'] = "檢視校友";
$ec_iPortfolio['SchoolHeaderImage'] = "學校圖像";
$ec_iPortfolio['SchoolHeaderImageUpload'] = "學校圖像上載";
$ec_iPortfolio['ShowSchoolHeaderImage'] = "顯示學校圖像";
$ec_iPortfolio['SchoolHeaderImageSizeSuggest'] = "學校圖像大小建議";
$ec_iPortfolio['SchoolHeaderImageSizeActual'] = "實際學校圖像大小";
$ec_iPortfolio['last_update']='更新日期';
$ec_iPortfolio['student']='學生';

$ec_iPortfolio['Width'] = "寬度";
$ec_iPortfolio['Height'] = "高度";

$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0] = "日期格式必須為 YYYY-MM-DD";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][1] = "如留空即為本年度";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][2] = "如留空即為本學期";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][3] = "號碼由 「#」 開始。例: #123456";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][4] = "請輸入範疇代號。如多於一個範疇請以「;」分隔代號，例：1,4,6 請填上1;4;6";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][5] = "按此查詢類別代號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][6] = "按此查詢其他學習經歷範疇代號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][7] = "請使用「班別及學號」或 「內聯網帳號」 或「WebSAMS學生註冊編號」以識別學生";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][8] = "按此查詢子類別代號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][9] = "按此查詢校內 / 校外代號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][10] = "按此查詢屬於 SAS代號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][11] = "按此查詢允許學生申報代號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][12] = "按此查詢適用班級";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][13] = "請輸入適用班級。如多於一個班級請以「;」分隔代號，例：S.1,S.4,S.5 請填上S1,S4,S5";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][14] = "按此查詢允許自動認可代號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][15] = "按此查詢必要欄位代號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][16] = "請輸入批核者的內聯網帳號";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][17] = "如「允許學生申報 」為【要】, 該些項目必須填寫";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][18] = "'Y' 代表要; 'N' 代表不需";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][19] = "如輸入開始日期，必須輸入結束日期";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][20] = "'Y' 代表允許; 'N' 代表不允許";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][21] = "請輸入欄位代號。如多於一個必要欄位，請以「;」分隔代號，例：時數 / 分數, 參與角色, 獎項 / 證書 / 成果 請填上1;2;4";
$Lang['iPortfolio']['IMPORT']['OLE']['Description'][22] = "如「允許自動認可 」為【不允許】, 該項目必須填寫";

$Lang['iPortfolio']['IMPORT']['ClassNameClassNumberInvalid'] = "班別及班號不配合";
$Lang['iPortfolio']['IMPORT']['ELECodeDuplicate'] = "其他學習經歷範疇代號重複";
$Lang['iPortfolio']['IMPORT']['ELECodeError'] = "其他學習經歷範疇代號不存在";
$Lang['iPortfolio']['IMPORT']['StudentLoginIDNotFind'] = "學生登入編號不存在";
$Lang['iPortfolio']['IMPORT']['TeacherLoginIDNotFind'] = "老師登入編號不存在";
$Lang['iPortfolio']['IMPORT']['WebSAMSNotFind'] = "WebSAMS 註冊號碼錯誤";
$Lang['iPortfolio']['IMPORT']['CodeGuide'] = "參照代號";

$Lang['iPortfolio']['OEA']['OEA_Name'] = "比賽/活動經驗及成就";
$Lang['iPortfolio']['OEA']['OEA_Name_PreMap_Student'] = $Lang['iPortfolio']['OEA']['OEA_Name'].' (預先輸入)';
$Lang['iPortfolio']['OEA']['OEA_ShortName'] = "OEA";
$Lang['iPortfolio']['OEA']['AdditionalInformation'] = "附加資料";
$Lang['iPortfolio']['OEA']['PersonalAndGeneralAbility'] = "個人及普遍能力";
$Lang['iPortfolio']['OEA']['AcademicPerformance'] = "學業成績表現";
$Lang['iPortfolio']['OEA']['SupplementaryInformation'] = "校長補充資料";
$Lang['iPortfolio']['OEA']['Convert'] = "轉換";
$Lang['iPortfolio']['OEA']['PreviousRecord'] = "前一資料";
$Lang['iPortfolio']['OEA']['Program'] = "活動";
$Lang['iPortfolio']['OEA']['NextRecord'] = "後一資料";
$Lang['iPortfolio']['OEA']['ReSelectRecord'] = "重新選擇資料";
$Lang['iPortfolio']['OEA']['SLP'] = "學生學習概覽";
$Lang['iPortfolio']['OEA']['SLPProgramName'] = "學生學習概覽名稱";
$Lang['iPortfolio']['OEA']['OEAProgramName'] = "OEA名稱";
$Lang['iPortfolio']['OEA']['NoOfUser']="學生數目";
$Lang['iPortfolio']['OEA']['SortedBy']="排序";
$Lang['iPortfolio']['OEA']['download_start']="開始中 ...";
$Lang['iPortfolio']['OEA']['download_notes1']="現在預備配對檔案 - 系統正為";
$Lang['iPortfolio']['OEA']['download_notes2']=" OLE 項目建議 OEA 配對。 ";
$Lang['iPortfolio']['OEA']['download_time_needed_1']="大概還需 ";
$Lang['iPortfolio']['OEA']['download_time_needed_2']=" 分鐘，請稍候 ...";

$Lang['iPortfolio']['SelfAccountArr']['exportArr']['RegNo'] = "Registration Number";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['StdName'] = "Student Name";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['ClassName'] = "Class Name";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['ClassNo'] = "Class Number";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['SelfAccPrimary'] = "Self-Account (Primary)";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['SelfAcc'] = "Self-Account";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['Export'] = "匯出";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['Selected'] = "已選擇";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['All'] = "全部";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['Type'] = "類別";
$Lang['iPortfolio']['SelfAccountArr']['exportArr']['PlsSelectClass'] = "請選擇班別";

$Lang['iPortfolio']['OEA']['SettingSubmitType'] = '同學申報選項';
$Lang['iPortfolio']['OEA']['SettingForceWithRoleMapping'] = '學生需要根據系統設定來設定角色';
$Lang['iPortfolio']['OEA']['SettingNotAllowSubmit'] = '現設為不可申報';
$Lang['iPortfolio']['OEA']['SettingOEASrc'] = '可選取項目';
$Lang['iPortfolio']['OEA']['SettingPartSrc'] = '可申報項目';

$Lang['iPortfolio']['OEA']['jsWarningArr']['SLP_OEA_Mapping_Changed'] = "如你改變此SLP的對應OEA，所有已連結此SLP的OEA將會更新至現選的OEA。你是否確定要更新此SLP的對應OEA？";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Description_Greater_Than_The_Max'] = "描述超出<!--MaxWordCount-->字元，請減少描述字數。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Input_Title'] = "請輸入附加資料標題。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Input_Content'] = "請輸入附加資料內容。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Greater_Than_The_Max'] = "內容超出<!--MaxWordCount-->字元，請減少內容字元數目。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Greater_Than_The_Max_Confirm'] = "內容超出<!--MaxWordCount-->字，你是否確定要儲存內容？";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_CompleteRatingFirst'] = "請先按「編輯」以完成附加資料內容。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Ability_SelectRating'] = "請選擇個人能力等級。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Ability_CompleteRatingFirst'] = "請先按「編輯」以完成學生的個人能力資料。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Academic_SelectPercentile'] = "請選擇百一分段值。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Academic_SelectOverallRating'] = "請選擇整體等級。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_Input_Title'] = "請輸入補充資料標題。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_Input_Content'] = "請輸入補充資料內容。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_CompleteRatingFirst'] = "請先按「編輯」以完成補充資料內容。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['OLE_SelectProgramNature'] = "請選擇活動性質。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['OLE_SelectMappingStatus'] = "請選擇配對狀況。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Description'] = "請輸入描述。";
$Lang['iPortfolio']['OEA']['jsWarningArr']['Sup_Info_Eng_Only'] = "根據JUPAS要求，只能以英文輸入 \\\"".$Lang['iPortfolio']['OEA']['SupplementaryInformation']."\\\"。是否仍要繼續？";
$Lang['iPortfolio']['OEA']['jsWarningArr']['AppliedOeaAlready'] = "你已申報此OEA項目，你是否要繼續申報此OEA項目？";
$Lang['iPortfolio']['OEA']['jsWarningArr']['NotAllowToApplyOthers'] = "學校不准許學生申報類別「其他」，請重新選擇OEA項目。";

$Lang['iPortfolio']['OEA']['WarningArr']['NoSubjectSettings'] = "系統未有學科設定，請先聯絡系統管理員設定有關資料。";
$Lang['iPortfolio']['OEA']['WarningArr']['NoSubjectOrAcademicSettings'] = "系統未有學科設定成學生未有分數資料，請先聯絡系統管理員設定及輸入有關資料。";
$Lang['iPortfolio']['OEA']['WarningArr']['NoAcademicClassSettings'] = "系統未有班別適用於製作學科成績表現，請先聯絡系統管理員設定有關資料。";
$Lang['iPortfolio']['OEA']['WarningArr']['NotAllPercentileMapped'] = "仍有百一分段值未配對整體等級，請先聯絡系統管理員設定有關資料。";
$Lang['iPortfolio']['OEA']['WarningArr']['OverrideStudentOeaRecords'] = "你已選擇重寫學生已選擇的OEA記錄，如你將 OLE 配對至不同的 OEA，系統將自動更新學生已配對的 OEA，此資料更新將無法恢復。";
$Lang['iPortfolio']['OEA']['WarningArr']['OverrideStudentOeaRecordsImport'] = "如確定要匯入，請按<font style=\"font-size:large\">繼續</font>。";
$Lang['iPortfolio']['OEA']['WarningArr']['NoApplicableFormSettings'] = "現時尚未有級別能使用JUPAS功能。請勾選需要使用JUPAS功能的級別。";

$Lang['iPortfolio']['OEA']['JUPAS_Trial'] = "大學聯招 (測試)";
$Lang['iPortfolio']['OEA']['JUPAS'] = "大學聯招";
$Lang['iPortfolio']['OEA']['Title'] = "標題";
$Lang['iPortfolio']['OEA']['OLE_Title'] = "標題 (OLE)";
$Lang['iPortfolio']['OEA']['OeaCode'] = "OEA 編號";
$Lang['iPortfolio']['OEA']['OeaName'] = "OEA 名稱";
$Lang['iPortfolio']['OEA']['OeaDescription'] = "OEA 描述";
$Lang['iPortfolio']['OEA']['MappedOEAItem'] = "已配對之OEA項目";
$Lang['iPortfolio']['OEA']['Category'] = "分類";
$Lang['iPortfolio']['OEA']['Role'] = "角色";
$Lang['iPortfolio']['OEA']['Achievement'] = "成就";
$Lang['iPortfolio']['OEA']['Description'] = "描述";
$Lang['iPortfolio']['OEA']['RoleType'] = array("L"=>"Leader", "C"=>"Committee Member", "M"=>"Member / Participant");
$Lang['iPortfolio']['OEA']['AchievementType'] = array("G"=>"Champion / Gold Medal", "B"=>"1st Runner-up / 2nd Runner-up / Silver / Bronze Medal", "O"=>"Other Awards", "N"=>"No Award");
$Lang['iPortfolio']['OEA']['ParticipationNatureType'] = array("C"=>"By competition","N"=>"By nomination","P"=>"By participation");
$Lang['iPortfolio']['OEA']['OLE'] = "其他學習經歷範疇 (OLE)";
$Lang['iPortfolio']['OEA']['OLE_Name2'] = "其他學習經歷";
$Lang['iPortfolio']['OEA']['OEA_Name2'] = "其他經歷成就(OEA)";
$Lang['iPortfolio']['OEA']['Add_CountText_1'] = "已選項目數量";
$Lang['iPortfolio']['OEA']['Add_CountText_2'] = "已儲存數量";
$Lang['iPortfolio']['OEA']['ItemSaveSuccessfully'] = "1|=|項目儲存成功";
$Lang['iPortfolio']['OEA']['ItemSaveUnsuccessfully'] = "0|=|項目儲存失敗";
$Lang['iPortfolio']['OEA']['AddiInfoSaveSuccessfully'] = "1|=|附加資料儲存成功";
$Lang['iPortfolio']['OEA']['AddiInfoSaveUnsuccessfully'] = "0|=|附加資料儲存失敗";
$Lang['iPortfolio']['OEA']['AbilitySaveSuccessfully'] = "1|=|個人能力儲存成功";
$Lang['iPortfolio']['OEA']['AbilitySaveUnsuccessfully'] = "0|=|個人能力儲存失敗";
$Lang['iPortfolio']['OEA']['AcademicSaveSuccessfully'] = "1|=|學業成績儲存成功";
$Lang['iPortfolio']['OEA']['AcademicSaveUnsuccessfully'] = "0|=|學業成績儲存失敗";
$Lang['iPortfolio']['OEA']['SuppInfoSaveSuccessfully'] = "1|=|補充資料儲存成功";
$Lang['iPortfolio']['OEA']['SuppInfoSaveUnsuccessfully'] = "0|=|補充資料儲存失敗";
$Lang['iPortfolio']['OEA']['SettingsSaveSuccessfully'] = "1|=|設定儲存成功";
$Lang['iPortfolio']['OEA']['SettingsSaveUnsuccessfully'] = "0|=|設定儲存失敗";
$Lang['iPortfolio']['OEA']['OeaNumExceedLimit'] = "0|=|已超過學生可申報OEA數目的上限";
$Lang['iPortfolio']['OEA']['SubmissionPeriodHasPast'] = "0|=|提交時段已過";
$Lang['iPortfolio']['OEA']['OEA_List'] = "OEA 列表";
$Lang['iPortfolio']['OEA']['OEA_CreateFromOLE'] = "新增自 OLE";
$Lang['iPortfolio']['OEA']['Edit'] = "編輯";
$Lang['iPortfolio']['OEA']['SaveAndNext'] = "儲存並繼續";
$Lang['iPortfolio']['OEA']['SaveAndFinish'] = "儲存並完成";
$Lang['iPortfolio']['OEA']['AllSettingsAreDisabled'] = "系統已停用所有有關OEA之功能，請聯絡系統管理員以開啟有關功能。";
$Lang['iPortfolio']['OEA']['ViewAndApproveStudentJupasData'] = "檢視及批核學生大學聯招資料";
$Lang['iPortfolio']['OEA']['SuggestedItems'] = "使用系統配對的標題";
$Lang['iPortfolio']['OEA']['SuggestedItemsCheck'] = "系統按OLE標題建議配對";
$Lang['iPortfolio']['OEA']['MatchItemCaption'] = "找到的類近標題:";
$Lang['iPortfolio']['OEA']['selectMapMethod'] = '請點選以下其中一項:';
$Lang['iPortfolio']['OEA']['MoreOptions'] = "更多配對選項";
$Lang['iPortfolio']['OEA']['AdvanceSearch'] = "手動配對";
$Lang['iPortfolio']['OEA']['Keyword'] = "關鍵字";
$Lang['iPortfolio']['OEA']['ByCategory'] = $ec_iPortfolio['category'];
$Lang['iPortfolio']['OEA']['ByItem'] = $iPort["Item"];
$Lang['iPortfolio']['OEA']['ProgramCode'] = "活動代碼";
$Lang['iPortfolio']['OEA']['ByKeyword'] = $Lang['iPortfolio']['OEA']['Keyword']." / ".$Lang['iPortfolio']['OEA']['ProgramCode'];
$Lang['iPortfolio']['OEA']['ItemSelected'] = "已選項目";
$Lang['iPortfolio']['OEA']['Approved'] = "已批核";
$Lang['iPortfolio']['OEA']['NotApproved'] = "未批核";
$Lang['iPortfolio']['OEA']['LastStatusUpdateRemark'] = "最近更新狀況：由<!--LastModifiedBy-->於<!--DaysAgo-->";

$Lang['iPortfolio']['OEA']['OeaItemApproveSuccessfully'] = "1|=|OEA項目批核成功";
$Lang['iPortfolio']['OEA']['OeaItemApproveUnsuccessfully'] = "0|=|OEA項目批核失敗";
$Lang['iPortfolio']['OEA']['OeaItemSaveSuccessfully'] = "1|=|OEA項目儲存成功";
$Lang['iPortfolio']['OEA']['OeaItemSaveUnsuccessfully'] = "0|=|OEA項目儲存失敗";
$Lang['iPortfolio']['OEA']['OeaItemRejectSuccessfully'] = "1|=|OEA項目拒絕成功";
$Lang['iPortfolio']['OEA']['OeaItemRejectUnsuccessfully'] = "0|=|OEA項目拒絕失敗";
$Lang['iPortfolio']['OEA']['AddiInfoApproveSuccessfully'] = "1|=|附加資料批核成功";
$Lang['iPortfolio']['OEA']['AddiInfoApproveUnsuccessfully'] = "0|=|附加資料批核失敗";
$Lang['iPortfolio']['OEA']['AddiInfoRejectSuccessfully'] = "1|=|附加資料拒絕成功";
$Lang['iPortfolio']['OEA']['AddiInfoRejectUnsuccessfully'] = "0|=|附加資料拒絕失敗";
$Lang['iPortfolio']['OEA']['AbilityApproveSuccessfully'] = "1|=|個人能力批核成功";
$Lang['iPortfolio']['OEA']['AbilityApproveUnsuccessfully'] = "0|=|個人能力批核失敗";
$Lang['iPortfolio']['OEA']['AbilityRejectSuccessfully'] = "1|=|個人能力拒絕成功";
$Lang['iPortfolio']['OEA']['AbilityRejectUnsuccessfully'] = "0|=|個人能力拒絕失敗";
$Lang['iPortfolio']['OEA']['AcademicApproveSuccessfully'] = "1|=|學業成績批核成功";
$Lang['iPortfolio']['OEA']['AcademicApproveUnsuccessfully'] = "0|=|學業成績批核失敗";
$Lang['iPortfolio']['OEA']['AcademicRejectSuccessfully'] = "1|=|學業成績拒絕成功";
$Lang['iPortfolio']['OEA']['AcademicRejectUnsuccessfully'] = "0|=|學業成績拒絕失敗";
$Lang['iPortfolio']['OEA']['SuppInfoApproveSuccessfully'] = "1|=|補充資料批核成功";
$Lang['iPortfolio']['OEA']['SuppInfoApproveUnsuccessfully'] = "0|=|補充資料批核失敗";
$Lang['iPortfolio']['OEA']['SuppInfoRejectSuccessfully'] = "1|=|補充資料拒絕成功";
$Lang['iPortfolio']['OEA']['SuppInfoRejectUnsuccessfully'] = "0|=|補充資料拒絕失敗";
$Lang['iPortfolio']['OEA']['NoOfResults'] = "結果數量";
$Lang['iPortfolio']['OEA']['Date'] = "日期";
$Lang['iPortfolio']['OEA']['Participation'] = "性質";
$Lang['iPortfolio']['OEA']['OLE_Int'] = "校內";
$Lang['iPortfolio']['OEA']['OLE_Ext'] = "校外";
$Lang['iPortfolio']['OEA']['Part1'] = "部份(一)";
$Lang['iPortfolio']['OEA']['Part2'] = "部份(二)";
$Lang['iPortfolio']['OEA']['OEA_Int'] = "校內";
$Lang['iPortfolio']['OEA']['OEA_Ext'] = "校外";
$Lang['iPortfolio']['OEA']['OEA_AwardBearing'] = "有獎項";
$Lang['iPortfolio']['OEA']['OEA_NotAwardBearing'] = "沒有獎項";
$Lang['iPortfolio']['OEA']['StartYear'] = "開始年份";
$Lang['iPortfolio']['OEA']['EndYear'] = "結束年份";
$Lang['iPortfolio']['OEA']['ApprovalStatus'] = "批核狀況";
$Lang['iPortfolio']['OEA']['OnlyExportApprovedOEARemarks'] = "JUPAS csv 檔案內只會包括<font color='red'><b>已批核</b></font>的 OEA 資料。";
$Lang['iPortfolio']['OEA']['PleaseSelectOEAItem'] = "請最少選擇一個 OEA 項目。";
$Lang['iPortfolio']['OEA']['RecordStatus_Pending'] = "等待批核";
$Lang['iPortfolio']['OEA']['RecordStatus_Approved'] = "已批核";
$Lang['iPortfolio']['OEA']['RecordStatus_Rejected'] = "已拒絕";
$Lang['iPortfolio']['OEA']['Remark'] = "備註";
$Lang['iPortfolio']['OEA']['PreviousStudent'] = "前一學生";
$Lang['iPortfolio']['OEA']['NextStudent'] = "下一學生";
$Lang['iPortfolio']['OEA']['NewFromOLE_Step1'] = "選擇 OLE 項目";
$Lang['iPortfolio']['OEA']['NewFromOLE_Step2'] = "補充 OEA 資料";
$Lang['iPortfolio']['OEA']['SubmissionPeriod'] = "遞交期限";
$Lang['iPortfolio']['OEA']['Content'] = "內容";
$Lang['iPortfolio']['OEA']['AddiInfoEditInstruction'] = "請用約三百個中英文字(最多二千個字符)，簡述一項對你別具意義、或影響你成長及人生目標的興趣或經驗。";
$Lang['iPortfolio']['OEA']['SuppInfoEditInstruction'] = "請提供可能會影響學生申請的有關資料（如疾病，意外事故，家庭問題）。請注意，大學聯招或有關學院機構可能要求申請人出示有關證明文件。";
$Lang['iPortfolio']['OEA']['AllStudents'] = "所有學生";
$Lang['iPortfolio']['OEA']['WaitingForApprovalStudents'] = "等候批核學生";
$Lang['iPortfolio']['OEA']['NoOfApproved'] = "已批核數目";
$Lang['iPortfolio']['OEA']['TotalNoOfRecords'] = "紀錄總數";
$Lang['iPortfolio']['OEA']['Checking'] = array(
											"OEA_Title"=>"OEA 標題",
											"OEA_StartDate"=>"OEA 開始日期",
											"OEA_EndDate"=>"OEA 結束日期",
											"OEA_Role"=>"OEA 角色",
											"OEA_AwardBearing"=>"OEA 成就獎項",
											"OEA_ParticipationNature"=>"OEA 參與性質",
											"OEA_Achievement"=>"OEA 成績",
											"OEA_Participation"=>"OEA 性質"
												);
$Lang['iPortfolio']['OEA']['Mapping_No_OEA_Record_Is_Mapped'] = "尚未配對 OEA 標題";
$Lang['iPortfolio']['OEA']['NoInfo'] = "未有資料";
$Lang['iPortfolio']['OEA']['Mapping_Others'] = "其他";
$Lang['iPortfolio']['OEA']['Mapping_Fail_To_Map_From_Above'] = "如未能從以上選項作配對";
$Lang['iPortfolio']['OEA']['Mapping_MappedItemName'] = "已配對項目名稱";
$Lang['iPortfolio']['OEA']['NoOfItemsSuggested'] = "建議項目數量";
$Lang['iPortfolio']['OEA']['SaveAndApprove'] = "儲存及批核";
$Lang['iPortfolio']['OEA']['OLEProgramMappingFile'] = "OLE 活動配對";
$Lang['iPortfolio']['OEA']['OEAProgramInfoFile'] = "OEA 活動資料";
$Lang['iPortfolio']['OEA']['AllOLEProgram'] = "全部 OLE 活動";
$Lang['iPortfolio']['OEA']['UnmappedOLEProgram'] = "未配對的 OLE 活動";
$Lang['iPortfolio']['OEA']['ProgramNature'] = "活動性質";
$Lang['iPortfolio']['OEA']['AllProgramNature'] = "全部活動性質";
$Lang['iPortfolio']['OEA']['MappingStatus'] = "配對狀況";
$Lang['iPortfolio']['OEA']['AllMappingStatus'] = "全部配對狀況";
$Lang['iPortfolio']['OEA']['ClickHereToDownloadMappingFile'] = "按此下載配對檔案";
$Lang['iPortfolio']['OEA']['ClickHereToDownloadOEAInfo'] = "按此下載 JUPAS OEA 資料";
$Lang['iPortfolio']['OEA']['Internal'] = "校內";
$Lang['iPortfolio']['OEA']['External'] = "校外";
$Lang['iPortfolio']['OEA']['Mapped'] = "已配對";
$Lang['iPortfolio']['OEA']['NotMapped'] = "未配對";
$Lang['iPortfolio']['OEA']['BackToOLEItemMappingList'] = "回到OLE活動配對列表";
$Lang['iPortfolio']['OEA']['ImportOtherMapping'] = "匯入其他配對";
$Lang['iPortfolio']['OEA']['ImportMappingSuccess'] = "匯入配對成功";
$Lang['iPortfolio']['OEA']['ImportMappingFailed'] = "匯入配對失敗";
$Lang['iPortfolio']['OEA']['ImportOtherAutoMapTitleMsg'] = "如果輸入「00000」作為OEA活動編碼，系統將會自動把它配對為OLE名稱。";
$Lang['iPortfolio']['OEA']['ImportOtherWarningMsg'] = "如選取「其他」(活動編碼：00000)，請務必填寫活動描述。如作為「第二部份 - 校外活動」申報，學生必須登入JUPAS系統，上載相關活動証明文件。";
$Lang['iPortfolio']['OEA']['WordCount'] = "字數統計";
$Lang['iPortfolio']['OEA']['Word(s)'] = "字";
$Lang['iPortfolio']['OEA']['CharacterCount'] = "字元統計";
$Lang['iPortfolio']['OEA']['SchoolCode'] = "學校編碼";
$Lang['iPortfolio']['OEA']['ApplicableForms'] = "申報級別";
$Lang['iPortfolio']['OEA']['SchoolCodeEmpty'] = "學校編碼不能留空。";
$Lang['iPortfolio']['OEA']['ApplicableFormsEmpty'] = "申報級別不能留空。";
$Lang['iPortfolio']['OEA']['ApplicationNumber'] = "申請編號";
$Lang['iPortfolio']['OEA']['OLEMappingRemarks'] = "如留空\"OEA 包含獎項\"欄位，系統將自動設定該 OEA 為包含獎項。";
$Lang['iPortfolio']['OEA']['ModulesInUse'] = "現正使用模組";
$Lang['iPortfolio']['OEA']['RepresentMappedFromOLE'] = "代表該 OEA 是配對自 OLE 紀錄";
$Lang['iPortfolio']['OEA']['SLPOrder'] = "SLP 次序";
$Lang['iPortfolio']['OEA']['TargetForm'] = "目標級別";
$Lang['iPortfolio']['OEA']['TargetClass'] = "目標班別";
$Lang['iPortfolio']['OEA']['TeacherMapping'] = "老師配對";
$Lang['iPortfolio']['OEA']['SaveAndApprove'] = "儲存及批核";
$Lang['iPortfolio']['OEA']['DetailsFromStudent'] = "學生描述";
$Lang['iPortfolio']['OEA']['DetailsForOle'] = "OLE描述";
$Lang['iPortfolio']['OEA']['ChineseCharacterRemarks'] = "每個中文字佔三字元單位";
$Lang['iPortfolio']['OEA']['JupasWordCountRemarks'] = "如字數統計與JUPAS有不符，請以JUPAS的統計為準。";

$Lang['iPortfolio']['OEA']['ModuleTitleArr']['OeaItem'] = $Lang['iPortfolio']['OEA']['OEA_ShortName'];
$Lang['iPortfolio']['OEA']['ModuleTitleArr']['AddiInfo'] = $Lang['iPortfolio']['OEA']['AdditionalInformation'];
$Lang['iPortfolio']['OEA']['ModuleTitleArr']['Ability'] = $Lang['iPortfolio']['OEA']['PersonalAndGeneralAbility'];
$Lang['iPortfolio']['OEA']['ModuleTitleArr']['Academic'] = $Lang['iPortfolio']['OEA']['AcademicPerformance'];
$Lang['iPortfolio']['OEA']['ModuleTitleArr']['SuppInfo'] = $Lang['iPortfolio']['OEA']['SupplementaryInformation'];

$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['AbilityToCommunicate'] = "溝通能力";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['AbilityToWorkWithOthers'] = "合作能力";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['AnalyticalPower'] = "分析能力";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Conduct'] = "操行";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Creativity'] = "創意";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['IndependenceOfMind'] = "獨立思維";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Industriousness'] = "勤奮";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Initiative'] = "主動性";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Leadership'] = "領導才能";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Maturity'] = "成熟程度";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['Perseverance'] = "毅力";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['SenseOfResponsibility'] = "責任感";
$Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr']['OverallEvaluation'] = "整體評分";

$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['Excellent'] = "優秀";
$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['Good'] = "良好";
$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['Average'] = "普通";
$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['BelowAverage'] = "低於平均";
$Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr']['UnableToJudge'] = "未能判斷";

$Lang['iPortfolio']['OEA']['AbilityArr']['Instruction'] = "從你對此學生的認識，或在與其他中六學生比較之下，評價此學生於下列屬性的表現。";
$Lang['iPortfolio']['OEA']['New_From_OLE'] = "新增自OLE紀錄";
$Lang['iPortfolio']['OEA']['New_Record'] = "新增紀錄";

$Lang['iPortfolio']['OEA']['oeaSetting'] = " OEA 設定";
$Lang['iPortfolio']['OEA']['itemMap'] = "OLE 活動配對";
$Lang['iPortfolio']['OEA']['roleMap'] = "OLE 角色配對";
$Lang['iPortfolio']['OEA']['appNoImport'] = "匯入同學資料";
$Lang['iPortfolio']['OEA']['Self_Mapped'] = "自行配對";
$Lang['iPortfolio']['OEA']['MappedByTeacher'] = "老師已作配對";

$Lang['iPortfolio']['OEA']['academicSettings'] = "學業成績";
$Lang['iPortfolio']['OEA']['AllowManualAdjustment'] = "允許手動調整";
$Lang['iPortfolio']['OEA']['StudentList'] = "學生列表";

$Lang['iPortfolio']['OEA']['AcademicArr']['Basic'] = "基本";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileAndOverallRating'] = "百一分段值及整體等級";
$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASApplicationForm'] = "大學聯招級別";
$Lang['iPortfolio']['OEA']['AcademicArr']['AcademicResultSource'] = "學業成績來源";
$Lang['iPortfolio']['OEA']['AcademicArr']['DataSource'] = "資料來源";
$Lang['iPortfolio']['OEA']['AcademicArr']['SchoolYear'] = "學年";
$Lang['iPortfolio']['OEA']['AcademicArr']['SchoolTerm'] = "學期";
$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'] = "百一分段值";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating'] = "整體等級";
$Lang['iPortfolio']['OEA']['AcademicArr']['MappingCriteria'] = "配對條件";
$Lang['iPortfolio']['OEA']['AcademicArr']['Score'] = "分數";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileLowerLimit(%)'] = "百一分段值下限 (%)";
$Lang['iPortfolio']['OEA']['AcademicArr']['Mark'] = "分數";
$Lang['iPortfolio']['OEA']['AcademicArr']['Grade'] = "等級";
$Lang['iPortfolio']['OEA']['AcademicArr']['GradeRemarks'] = "例子：\"A\" 或 \"A, A-, B+, B\"";
$Lang['iPortfolio']['OEA']['AcademicArr']['PositionInForm'] = "級名次";
$Lang['iPortfolio']['OEA']['AcademicArr']['GenerateBasedOnSettings'] = "依設定產生";
$Lang['iPortfolio']['OEA']['AcademicArr']['ApplyToJUPAS'] = "應用至JUPAS";
$Lang['iPortfolio']['OEA']['AcademicArr']['NotApplyToJUPAS'] = "不應用至JUPAS";
$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCode'] = "JUPAS 學科編碼";
$Lang['iPortfolio']['OEA']['AcademicArr']['NoSubjectApplyToJUPAS'] = "未有學科應用至JUPAS。";
$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCodeEmpty'] = "JUPAS 學科編碼不能留空。";
$Lang['iPortfolio']['OEA']['AcademicArr']['JUPASSubjectCodeDuplicated'] = "JUPAS 學科編碼不能重複。";
$Lang['iPortfolio']['OEA']['AcademicArr']['ExportForJUPAS'] = "匯出 (用於JUPAS)";
$Lang['iPortfolio']['OEA']['AcademicArr']['AcademicForFormSixOnly'] = "「學業成績表現」只限中六學生使用。如你現正處理中六學生但看不到成績，請聯絡系統管理員或到「學校基本設定」設定此級別的 WebSAMS 代號為 \"S6\"。";
$Lang['iPortfolio']['OEA']['AcademicArr']['AutoMappingForPercentileAndOverallRating'] = "自動配對百一分段值及整體等級";
$Lang['iPortfolio']['OEA']['AcademicArr']['MappingSettings'] = "配對設定";
$Lang['iPortfolio']['OEA']['AcademicArr']['GenerateAcademicPerformance'] = "產生學業成績表現";
$Lang['iPortfolio']['OEA']['AcademicArr']['GenerateWarning'] = "產生學業成績表現後，系統將重新儲存以下班別的學業成績表現資料，而現有的學業成績表現資料將不能還原。如確定，請按產生。";
$Lang['iPortfolio']['OEA']['AcademicArr']['GenerateFormula'] = "系統以此方程式計算百一分段值 (此數值將四捨五入至整數以決定學生的百一分段值)： (學生科目級名次 / 該級該科目學生總人數) x 100%";

$Lang['iPortfolio']['OEA']['AcademicArr']['ReturnMsgArr']['GenerateSuccess'] = "1|=|產生學業成績表現成功。";
$Lang['iPortfolio']['OEA']['AcademicArr']['ReturnMsgArr']['GenerateFailed'] = "0|=|產生學業成績表現失敗。";

$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][1] = "首10%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][2] = "11%-25%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][3] = "26%-50%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][4] = "51%-75%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][5] = "尾25%";
$Lang['iPortfolio']['OEA']['AcademicArr']['PercentileArr'][6] = "不予評級";

$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['A'] = "優秀";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['B'] = "良好";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['C'] = "好";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['D'] = "普通";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['E'] = "低於平均";
$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRatingArr']['U'] = "不予評級";

$Lang['iPortfolio']['OEA']['roleMapArr']['RoleSelected']="已選擇角色";
$Lang['iPortfolio']['OEA']['roleMapArr']['NoOfRole']="角色數目";

$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramID'] = "OLE Program ID";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_AcademicYear'] = "OLE Academic Year";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_NumOfStudentJoined'] = "OLE No. of Students Joined";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramName'] = "OLE Program Name";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramNature'] = "OLE Program Nature";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_ProgramCode'] = "OEA Program Code";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_Suggestions'] = "OEA Suggestion (Program Code : Name) [Ref]";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_ProgramName'] = "OEA Program Name";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_AwardBearing'] = "OEA Award Bearing (Y / N)";

$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramID'] = "OLE 活動編號";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_AcademicYear'] = "OLE 活動年份";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_NumOfStudentJoined'] = "OLE 參加學生數目";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramName'] = "OLE 活動名稱";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramNature'] = "OLE 活動性質";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_ProgramCode'] = "OEA 活動代碼";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_Suggestions'] = "建議 OEA (活動代碼 : 名稱) [參考用途] ";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_ProgramName'] = "OEA 活動名稱";
$Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_AwardBearing'] = "OEA 包含獎項 (Y / N)";

$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['AwardDontMatch'] = "OEA獎項輸入不正確";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLECodeDuplicated'] = "OLE 活動編號於此檔案中已被重複使用";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLEProgramIDNotExist'] = "OLE 活動編號不存在";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLEProgramIDNotMatchedWithTitle'] = "OLE 活動編號與名稱不配合";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OEACodeNotExist'] = "OEA 活動代碼不存在";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardBearing'] = "包含獎項必須輸入「Y」或「N」";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardType'] = "獎項種類必須輸入 「G」 ,「B」,「O」或「N」";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardNature'] = "獎項類別必須輸入 「C」 ,「N」或「D」";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongParticipationMode'] = "參與方式必須輸入 「L」 ,「C」或「M」";
$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongOeaYear'] = "OEA 年份必須輸入 YYYY";

$Lang['iPortfolio']['OLE']['NewStudent'] = "新增學生";
$Lang['iPortfolio']['OLE']['UploadButton']="批量上載附件";
$Lang['iPortfolio']['OLE']['UploadAttachment']['UploadButton'] = "上載";
$Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][] = "選擇檔案";
$Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][] = "確認上載記錄";
$Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][] = "上載結果";
$Lang['iPortfolio']['OLE']['UploadAttachment']['File'] = "檔案";
$Lang['iPortfolio']['OLE']['UploadAttachment']['FileSample'] = "範本檔案";
$Lang['iPortfolio']['OLE']['UploadAttachment']['DownloadSingleFileSample'] = "每個學生一個文件範例";
$Lang['iPortfolio']['OLE']['UploadAttachment']['DownloadMultipleFileSample']= "每個學生多個文件範例";
$Lang['iPortfolio']['OLE']['UploadAttachment']['Remarks'] = "備註";
$Lang['iPortfolio']['OLE']['UploadAttachment']['Error']="失敗原因";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "以壓縮檔案(.zip)形式上載多個檔案";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "壓縮檔案內可以存在文件夾";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "文件夾內不可再有文件夾";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "文件夾或文檔名稱不能使用中文";
$Lang['iPortfolio']['OLE']['UploadAttachment']['RemarksArr'][] = "文件夾或文件名稱要與學生的班別學號(例如: 1A_01) 或 WebSAMS學生註冊編號一致 ";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['PleaseSelectZIP'] ="請選擇壓縮檔(.zip)";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['UnmappingName'] = "未找到配對學生";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['RepeatStudent'] = "已有檔案配對此學生";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['FolderInside'] = "文件夾內不可以有文件夾";
$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['EmptyFolder'] = "文件夾不可以為空";
$Lang['iPortfolio']['OLE']['UploadAttachment']['UploadNumber'] = "上傳文件數量";
$Lang['iPortfolio']['OLE']['UploadAttachment']['UploadUncessfulNumber'] = "上傳失敗";


// Temp Lang
$Lang['iPortfolio']['OEA']['AcademicArr']['Overall'] = "全年總結";
$Lang['iPortfolio']['OEA']['AcademicArr']['Term1'] = "學期一";
$Lang['iPortfolio']['OEA']['AcademicArr']['Term2'] = "學期二";
// End Temp Lang

$Lang['iPortfolio']['OEA']['GeneralSetting'] = "一般設定";
$Lang['iPortfolio']['OEA']['MaxNoOfOeaAllow'] = "每名學生可提交的紀錄上限";
$Lang['iPortfolio']['OEA']['MaxNoOfOeaAllow_StudentView'] = "可提交的 OEA 紀錄上限";
$Lang['iPortfolio']['OEA']['SubmissionPeriodSetting'] = "提交時段設定";
$Lang['iPortfolio']['OEA']['SubmissionSettings'] = "提交設定";
$Lang['iPortfolio']['OEA']['AllowStudentNewOEA_NotFromOLE'] = "准許學生新增全新 OEA 記錄";
$Lang['iPortfolio']['OEA']['AllowStudentSubmitOthersOea'] = "准許學生申報 類別「其他」";
$Lang['iPortfolio']['OEA']['ApprovalSetting'] = "批核設定";
$Lang['iPortfolio']['OEA']['AutoApproveOnOeaRecord'] = "自動批核 OEA 紀錄";
$Lang['iPortfolio']['OEA']['NeedApproval'] = "紀錄需經批核";
$Lang['iPortfolio']['OEA']['Part1NeedApproval'] = "「部份(一) - 校內」紀錄需經批核";
$Lang['iPortfolio']['OEA']['Part2NeedApproval'] = "「部份(二) - 校外」紀錄需經批核";
$Lang['iPortfolio']['OEA']['Part2NeedApprovalRemarks'] = "「部份(二) - 校外」紀錄將自動批核";
$Lang['iPortfolio']['OEA']['jsWarning']['InvalidStartDate'] = "開始日期不正確";
$Lang['iPortfolio']['OEA']['jsWarning']['InvalidEndDate'] = "結束日期不正確";
$Lang['iPortfolio']['OEA']['jsWarning']['StartDateShouldBeEarlierThanEndDate'] = "開始日期必須早於結束日期";
$Lang['iPortfolio']['OEA']['jsWarning']['StartTimeShouldBeEarlierThanEndTime'] = "開始時間必須早於結束時間";
$Lang['iPortfolio']['OEA']['jsWarning']['StartToFillInDetails'] = "現在，你可以提供額外資料供OEA申報之用。";
$Lang['iPortfolio']['OEA']['jsWarning']['AllowSubmitedItem'] = " 請選擇最少一項 '".$Lang['iPortfolio']['OEA']['SettingSubmitType']."'";
$Lang['iPortfolio']['OEA']['jsWarning']['AllowSubmitedOEAItem'] = " 請於 '".$Lang['iPortfolio']['OEA']['AllowStudentNewOEA_NotFromOLE']."' 選擇最少一項";
$Lang['iPortfolio']['OEA']['jsWarning']['DeleteMapping'] = "你是否確定要刪除此配對?";
$Lang['iPortfolio']['OEA']['jsWarning']['ChooseOeaProgram'] = "請選擇OEA項目。";
$Lang['iPortfolio']['OEA']['QuotaLeft'] = "尚餘名額";
$Lang['iPortfolio']['OEA']['NoOfRecordSelected'] = "已選紀錄";
$Lang['iPortfolio']['OEA']['OeaQuotaIsNotEnough'] = "OEA 名額不足";
$Lang['iPortfolio']['OEA']['OLE_Selection_Instruction'] = "老師已經批核以下的OLE紀錄。你可以從中挑選要申報為JUPAS OEA的紀錄。";
$Lang['iPortfolio']['OEA']['NoSuggestedItemReturn'] = "系統找不到建議配對項目";
$Lang['iPortfolio']['OEA']['UseOleName'] = "使用OLE名稱";
$Lang['iPortfolio']['OEA']['RecordSaved_SureWantToQuit'] = "個紀錄已儲存，你是否確定要離開？";
$Lang['iPortfolio']['OEA']['SelfInput'] = "自行輸入";
$Lang['iPortfolio']['OEA']['DisplaySelectedOeaItem'] = "顯示已選的OEA項目";
$Lang['iPortfolio']['OEA']['ReferenceToSelfAccount'] = '參考'.$iPort['menu']['self_account'].'填寫';
$Lang['iPortfolio']['OEA']['PreMapping'] = '預先配對OEA';
$Lang['iPortfolio']['OEA']['PreMappingNotes'] = "申報 JUPAS 時用";
$Lang['iPortfolio']['OEA']['PreMappingRemove'] = "刪除 OEA 配對";
$Lang['iPortfolio']['OEA']['MissingOLETitle'] = "請先輸入 OLE 標題！";
$Lang['iPortfolio']['OEA']['defaultEmptyTextAreaMsg'] = "input message.";
$Lang['iPortfolio']['OEA']['PercentileEmptyMsg'] = "將百一分段值設定為\"不予評級\"須給予原因";
$Lang['iPortfolio']['OEA']['OverallRatingEmptyMsg'] = "將整體等級設定為\"不予評級\"須給予原因";
$Lang['iPortfolio']['OEA']['PercentileExceedMsg'] = "百一分段值的備註的長度超過了上限";
$Lang['iPortfolio']['OEA']['OverallRatingExceedMsg'] = "整體等級的備註的長度超過了上限";
$Lang['iPortfolio']['OEA']['Remarks'] = "備註";

$Lang['iPortfolio']['OEA']['DownloadMappingInstructions'] =
"<br /><b>請仔細閱讀以下使用需知，明白後便可下載配對檔案。</b><br />
<ul><li>配對檔案 F 欄顯示由系統建議的配對（可能適用於各 OLE 項目的 OEA 活動代碼）。</li><br />
<li>系統建議的配對作參考用途，以方便老師作配對，但有時不一定最準確。</li><br />
<li>配對時，請逐一檢查各項目： </li><br />
<ul><li>如從 F 欄的建議列發現有適用於該 OLE 項目的代碼，可複製有關代碼到 G 欄，並填寫 I 欄。</li><br />
	<li>如自動配對代碼中，並沒有適用的代碼，請參閱 JUPAS 官方 OEA 活動列表，找出適用的代碼，然後輸入到 G 欄，並填寫I 欄。</li><br />
	</ul>
<li>如果某些項目不需配對，請刪除整行。 </li><br />
<li><font color=\"red\">製作配對檔案需時，請耐心等候。</font></li>
</ul>";


$Lang['iPortfolio']['OEA']['Disclaimer'] = "免責聲明";
$Lang['iPortfolio']['OEA']['OLE_OLE_Mapping_Warning'] = "系統內的OEA 活動列表最後更新日期為 2011-09-09。你需要登入JUPAS網上申請系統，下載OEA活動列表來完成配對工作。";
//$Lang['iPortfolio']['OEA']['Disclaimer_TeacherView'] = "鑒於JUPAS方面可能更改資料格式及申報要求，而該等變更並不屬於本公司可掌握範圍之內。因此，我們無法保證於測試期間所輸入的資料能夠於系統正式運作時使用。我們建議學校只輸入少量資料作測試。";
$Lang['iPortfolio']['OEA']['Disclaimer_TeacherView'] = "JUPAS可能會更新OEA活動列表或資料格式，該變更並不屬於本公司可掌握的範圍之內。";
//$Lang['iPortfolio']['OEA']['Disclaimer_StudentView'] = "鑒於JUPAS方面可能更改資料格式及申報要求，而該等變更並不屬於本公司可掌握範圍之內。因此，我們無法保證於測試期間所輸入的資料能夠於系統正式運作時使用。如遇有關情況，你可能需要再次申報紀錄。";
$Lang['iPortfolio']['OEA']['Disclaimer_StudentView'] = "JUPAS可能會更新OEA活動列表或資料格式，該變更並不屬於本公司可掌握的範圍之內。";
$Lang['iPortfolio']['OEA']['discussForumCaption'] = "JUPAS 討論區";
$Lang['iPortfolio']['OEA']['discussForumMessage'] = "如果老師對於JUPAS的申報有任何最新消息或意見，可<a href =\"index.php?task=linkForum\" target=\"_blank\" class=\"tablelink\"> [按此] </a>進入中央討論區，與使用eClass的其他老師一起分享及討論。";
$Lang['iPortfolio']['OEA']['OverrideStudentMappedRecords'] = "重寫學生已選擇的OEA記錄 (只重寫活動編號及名稱)";
$Lang['iPortfolio']['OEA']['NotApplicableToDelete'] = "不適用於刪除";

$Lang['iPortfolio']['OEA']['ReportArr']['DisplayInOeaFormat'] = "以OEA形式顯示";
$Lang['iPortfolio']['OEA']['ReportArr']['DisplayInOleFormat'] = "以OLE形式顯示";
$Lang['iPortfolio']['OEA']['ReportArr']['StudentOeaReport'] = "學生OEA報告";
$Lang['iPortfolio']['OEA']['ReportArr']['StudentOea'] = "學生OEA";
$Lang['iPortfolio']['OEA']['ReportArr']['IncludeOleData'] = "包括OLE資料";
$Lang['iPortfolio']['OEA']['ReportArr']['CustReportTitleArr']['LaSalle_StudentMappedOeaOleInfoReport'] = $Lang['iPortfolio']['OEA']['ReportArr']['DisplayInOleFormat'];

$Lang['iPortfolio']['OEA']['FAQCaption'] = "常見問題";
$Lang['iPortfolio']['OEA']['FAQMessage'] = "如果有任何查詢，請先參閱<a href = \"http://support.broadlearning.com/doc/help/faq/\" target=\"_blank\">FAQ<img src=\"/images/2009a/btn_FAQ.png\" border=\"0\"></a>。";
$Lang['iPortfolio']['OEA']['DownloadTemplateInstruction'] = '配對檔案下載須知';
$Lang['iPortfolio']['OEA']['DownloadTemplateInstructionConfirm'] = "我已了解，現需下載檔案。";
$Lang['iPortfolio']['OEA']['ViewPreMap'] = "檢視預先輸入學生OEA項目";

$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'] = "將不會使用.";
$Lang['iPortfolio']['JUPAS']['and'] = "和";
$Lang['iPortfolio']['JUPAS']['RemarksOEA'] = "根據大學聯招政策，學生必須直接在大學聯招系統中填寫OEA和附加資料。因此，老師不能由eClass匯出OEA和附加資料再匯入到大學聯招系統中。<br/>再者，大學聯招系統內OEA的活動種類/代碼或已更新，故請勿讓學生於eClass中進行OEA配對。";
$Lang['iPortfolio']['JUPAS']['RemarksAbility'] = "學生的個人及普遍能力將於同一行列出。";
$Lang['iPortfolio']['JUPAS']['RemarksAcademic'] = "百一分段值和整體等級將分別匯出。";

$Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']  = "學生學習概覽";
$Lang['iPortfolio']['Cust_mms']['OLEReport'] = "EXTRA-CURRICULAR ACTIVITIES AND AWARDS REPORT";
$Lang['iPortfolio']['Cust_cactm']['OLEReport'] = "其他學習經歷記錄 (學校申報)";
$Lang['iPortfolio']['Cust_cactm']['OLEReport2'] = "其他學習經歷記錄 (學生申報)";
$Lang['iPortfolio']['Cust_ghslp']['StudentLearningProfile'] = "學生學習概覽";
$Lang['iPortfolio']['Cust_hktlc']['StudentLearningProfileReport'] = "學生學習概覽報告";
$Lang['iPortfolio']['Cust_kc']['StudentLearningProfileReport'] = "學生學習概覽";
$Lang['iPortfolio']['Cust_stmc']['StudentLearningProfileReport'] = "學生學習概覽";
$Lang['iPortfolio']['Cust_stcec']['StudentLearningProfileReport'] = "學生學習概覽";
$Lang['iPortfolio']['Cust_htc']['StudentLearningProfileReport'] = "學生學習概覽";
$Lang['iPortfolio']['Cust_lskc']['StudentLearningProfileReport'] = "學生學習概覽";
$Lang['iPortfolio']['Cust_yttmc']['StudentLearningProfileReport'] = "學生學習概覽";
$Lang['iPortfolio']['Cust_toss']['StudentLearningProfileReport'] = "學生學習概覽";
$Lang['iPortfolio']['Cust_ucc']['SLP_Report_SchoolName'] = '&#28377;基書院<br />UNITED CHRISTIAN COLLEGE';
$Lang['iPortfolio']['Cust_ucc']['SLP_Report_ReportName'] = '學生學習概覽<br />STUDENT LEARNING PROFILE';
$Lang['iPortfolio']['Cust_ucc']['SLP_Report_Signature'] = 'Signature';

$Lang['iPortfolio']['Cust_uccke']['SLP_Report_SchoolName'] = '&#28377;基書院(東九龍)<br />United Christian College (Kowloon East) ';
$Lang['iPortfolio']['Cust_uccke']['SLP_Report_ReportName'] = '學生學習概覽<br />STUDENT LEARNING PROFILE';
$Lang['iPortfolio']['Cust_sptss']['report1'] = '學生功過記錄明細';
$Lang['iPortfolio']['Cust_sptss']['report2'] = '學生生命素質涵養及功過記錄明細';
$Lang['iPortfolio']['Cust_sptss']['report3'] = '學生全人發展記錄表';
//$Lang['iPortfolio']['Cust_sptss']['report4'] = '歷年生命素養涵養記錄';
$Lang['iPortfolio']['Cust_sptss']['report4'] = '歷年生命素質涵養記錄';
$Lang['iPortfolio']['Cust_sptss']['report5'] = '歷年全人發展記錄表';
$Lang['iPortfolio']['Cust_sptss']['studentCharacter'] = '學生生命素質涵養';

$Lang['iPortfolio']['Cust_nlp']['OLECurrentYear'] = '學生學習概覽（本學年）';
$Lang['iPortfolio']['Cust_nlp']['OLEWholeYear'] = '學生學習概覽（全部學年）';

$Lang['iPortfolio']['SRP']['StudentReadingProfile'] = "學生閱讀概覽";
$Lang['iPortfolio']['SRP']['DateOfIzzue'] = "發出日期";
$Lang['iPortfolio']['SRP']['StudentParticulars'] = "學生資料";
$Lang['iPortfolio']['SRP']['StudentName'] = "學生姓名";
$Lang['iPortfolio']['SRP']['DateOfBirth'] = "出生日期";
$Lang['iPortfolio']['SRP']['SchoolName'] = "學校名稱";
$Lang['iPortfolio']['SRP']['AdmissionDate'] = "入學日期";
$Lang['iPortfolio']['SRP']['SchoolAddress'] = "學校地址";
$Lang['iPortfolio']['SRP']['SchoolPhone'] = "學校電話";
$Lang['iPortfolio']['SRP']['HKID'] = "身份証號碼";
$Lang['iPortfolio']['SRP']['Gender'] = "性別";
$Lang['iPortfolio']['SRP']['SchoolCode'] = "學校編號";
$Lang['iPortfolio']['SRP']['StudentID'] = "學生編號";

$Lang['iPortfolio']['SRP']['Item'] = "項目";
$Lang['iPortfolio']['SRP']['Title'] = "書名";
$Lang['iPortfolio']['SRP']['Language'] = "中文/英文/其他";
$Lang['iPortfolio']['SRP']['Author'] = "作者/譯者";
$Lang['iPortfolio']['SRP']['Publisher'] = "出版社";
$Lang['iPortfolio']['SRP']['Categories'] = "範疇";
$Lang['iPortfolio']['SRP']['PeriodOfReading'] = "閱書時期";
$Lang['iPortfolio']['SRP']['StartingDate'] = "開始日期";
$Lang['iPortfolio']['SRP']['FinishingDate'] = "完成日期";
$Lang['iPortfolio']['SRP']['ReadingHours'] = "閱讀時數";
$Lang['iPortfolio']['SRP']['BookReport'] = "閱讀報告";
$Lang['iPortfolio']['SRP']['YN'] = "(Y/N)";

$Lang['iPortfolio']['SRP']['LanguageType'][1] = "中文";
$Lang['iPortfolio']['SRP']['LanguageType'][2] = "英文";
$Lang['iPortfolio']['SRP']['Uncategorized'] = "未分類";

$Lang['iPortfolio']['SmallGrade'] = "細分等級";
$Lang['iPortfolio']['SmallGradeDetails'] = "分為 9+ 9 8 7 6 5 4 3 2 1 1-";
$Lang['iPortfolio']['Avg'] = '平均';
$Lang['iPortfolio']["Score"] = "分數";
$Lang['iPortfolio']["LevelAvageScore"] = "全級平均分";
$Lang['iPortfolio']["LevelAvageSD"] = "全級標準差";
$Lang['iPortfolio']["StandardScore"] = "標準分";
$Lang['iPortfolio']["NewStandardScore"] = "新標準分";
$Lang['iPortfolio']["StandardScoreDiff"] = "標準分之差";
$Lang['iPortfolio']["MarkDifference%"] = "分數增減%";
$Lang['iPortfolio']["StandardScoreDifference"] = "標準分增減";
$Lang['iPortfolio']["MarkDifferenceHTML"] = "分數<br />增減%";
$Lang['iPortfolio']["StandardScoreDifferenceHTML"] = "標準分<br />增減";
$Lang['iPortfolio']["FormPosition"] = "排名";
$Lang['iPortfolio']["FormPositionDifference"] = "排名增減";
$Lang['iPortfolio']["increaseRank"] = "增值名次";
$Lang['iPortfolio']["oldClass"] = "前班別";


$Lang['iPortfolio']['OLE']['SAS'] = "屬於 SAS";
$Lang['iPortfolio']['OLE']['SAS_ALL'] = "全部 (屬於或不屬 SAS)";
$Lang['iPortfolio']['OLE']['SAS_NOT'] = "不屬於 SAS";
$Lang['iPortfolio']['OLE']['SAS_with_link'] = "屬於 <a href=\"http://www2.hkugac.edu.hk/en_us/ela/sas.html\" target=\"SASwindow\"><font color=\"blue\">SAS</font></a>";
$Lang['iPortfolio']['OLE']['In_Outside_School'] = "校內 / 校外";
$Lang['iPortfolio']['OLE']['Inside_School'] = "校內";
$Lang['iPortfolio']['OLE']['Outside_School'] = "校外";
$Lang['iPortfolio']['OLE']['DisplayIneRC'] = "顯示於eRC";
$Lang['iPortfolio']['OLE']['HideIneRC'] = "不顯示於eRC";

$Lang['iPortfolio']['SLP']['ImportRemarks'] = "匯入模板已經於 2011-06-29 更新。請確認你使用最新的模板匯入紀錄。";

# Dynamic Report
$Lang['iPortfolio']['SLP']['DynamicReport']['GeneratedBy'] = "產生自";
$Lang['iPortfolio']['SLP']['DynamicReport']['GeneratedDate'] = "產生日期";
$Lang['iPortfolio']['SLP']['DynamicReport']['Report'] = "報告";

$Lang['iPortfolio']['VolunteerArr']['Volunteer'] = "義工";
$Lang['iPortfolio']['VolunteerArr']['VolunteerEvent'] = "義工活動";
$Lang['iPortfolio']['VolunteerArr']['EventCode'] = "活動代號";
$Lang['iPortfolio']['VolunteerArr']['EventName'] = "活動名稱";
$Lang['iPortfolio']['VolunteerArr']['PartnerOrganization'] = "合作機構";
$Lang['iPortfolio']['VolunteerArr']['EventRegistration'] = "活動申報";
$Lang['iPortfolio']['VolunteerArr']['AppliedEvent'] = "已申報活動";
$Lang['iPortfolio']['VolunteerArr']['Applied'] = "已申報";
$Lang['iPortfolio']['VolunteerArr']['FillInDetails'] = "填寫資料";
$Lang['iPortfolio']['VolunteerArr']['Hours'] = "時數";
$Lang['iPortfolio']['VolunteerArr']['HourUnit'] = "小時";
$Lang['iPortfolio']['VolunteerArr']['Role'] = "參與角色";
$Lang['iPortfolio']['VolunteerArr']['Awards/Certifications/Achievements'] = "獎項 / 證書 / 成果";
$Lang['iPortfolio']['VolunteerArr']['TeacherPIC'] = "負責老師";
$Lang['iPortfolio']['VolunteerArr']['Status'] = "狀況";
$Lang['iPortfolio']['VolunteerArr']['AllStatus'] = "全部狀況";
$Lang['iPortfolio']['VolunteerArr']['AllProgram'] = "全部狀況活動";
$Lang['iPortfolio']['VolunteerArr']['MyPICProgram'] = "負責活動";

$Lang['iPortfolio']['VolunteerArr']['jsWarningArr']['SelectEvent'] = "請選擇活動。";

$Lang['iPortfolio']['OLE']['Information'] = "其他學習經歷資料";
$Lang['iPortfolio']['OLE']['OleTitle'] = "OLE 標題";
$Lang['iPortfolio']['OLE']['OleNature'] = "OLE 類型";
$Lang['iPortfolio']['OLE']['OleAcademicYear'] = "OLE 學年";
$Lang['iPortfolio']['OLE']['OleTerm'] = "OLE 學期";
$Lang['iPortfolio']['OLE']['OleStartDate'] = "OLE 開始日期";
$Lang['iPortfolio']['OLE']['OleEndDate'] = "OLE 結束日期";
$Lang['iPortfolio']['OLE']['OleRole'] = "OLE 參與角色";
$Lang['iPortfolio']['OLE']['OleCategory'] = "OLE 類別";
$Lang['iPortfolio']['OLE']['OleComponentOfOle'] = "OLE 其他學習經歷範疇";
$Lang['iPortfolio']['OLE']['OleAwards/Certifications/Achievements'] = "OLE 獎項 / 證書 / 成果";
$Lang['iPortfolio']['OLE']['MergeProgramMessageBoxTitle'] = '已選擇之項目';
$Lang['iPortfolio']['OLE']['TeacherInCharge'] = '負責老師';
$Lang['iPortfolio']['OLE']['ProgramCreator'] = '活動創建者';
$Lang['iPortfolio']['OLE']['AddCertificationFile'] = '添加證明檔案';
$Lang['iPortfolio']['OLE']['NewOLE'] = '新增其他學習經歷';
$Lang['iPortfolio']['OLE']['mandatory'] = '附有<span class="alertText"> * </span>的項目必須填寫。';
$Lang['iPortfolio']['OLE']['JoinEndDate'] = '申報結束日期';
$Lang['iPortfolio']['OLE']['SchoolPresetActivities'] = '學校預設活動';
$Lang['iPortfolio']['OLE']['Records'] = '項紀錄';
$Lang['iPortfolio']['OLE']['PendingApprovals'] = '項等待批核';
$Lang['iPortfolio']['OLE']['Approvals'] = '項批核紀錄';


$Lang['iPortfolio']['OLE_Outside']['Information'] = "校外表現/獎項及重要參與資料";
$Lang['iPortfolio']['OLE_OEA']['Description'] = "申報時，如有需要，學生自己輸入。";
$Lang['iPortfolio']['OLE_OEA']['Nature'] = "會按照 OEA 的基本設定。";
$Lang['iPortfolio']['OLE_OEA']['Role'] = "會按照 OEA 的設定中的 OLE 角色配對。";
$Lang['iPortfolio']['OLE_OEA']['Date'] = "系統會按 OLE 開始及結束日期來為學生選出相應日期。";

$Lang['iPortfolio']['SLP_Setting']['CompulsoryRemarks'] = '此設定只套用到學生申報的版面。';
$Lang['iPortfolio']['SLP_REPORT']['AllowForStudent'] = '學生自行列印 "'.$iPort['menu']['slp'] .'" 報告設定';
$Lang['iPortfolio']['SLP_REPORT']['AllowForStudentToPrint'] = '准許同學自行列印';
$Lang['iPortfolio']['SLP_REPORT']['AllowForStudentToPrintYesNo'] = '准許';
$Lang['iPortfolio']['SLP_REPORT']['SchoolHeaderImagePrint'] = '學校圖像列印設定';
$Lang['iPortfolio']['SLP_REPORT']['SchoolHeaderImageDetail'] = '學校圖像資料';

$Lang['iPortfolio']['plms_SLP_REPORT']['NumberOfOLE'] = '其他學習經歷報告記錄數量';
$Lang['iPortfolio']['plms_SLP_REPORT']['SelectOLERecord'] = '學生選擇記錄時期';

$Lang['iPortfolio']['Student']['Report']['PlsSelectAcademicYear'] = '請選擇一個學年。';
$Lang['iPortfolio']['OLE']['ViewModeReason'] = "由於以下原因，不能更改紀錄:<br>1. 提交時段已過，或<br>2. 紀錄由老師新增，或<br>3. 老師已經批核紀錄。";

$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_valid'] = '項合規格的資料 - 請確定匯入';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_invalid'] = '項不合規格的資料 - 請修正再作匯入';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_result_success'] = '項資料已成功匯入系統。';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_result_failed'] = '匯入失敗!';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_removal'] = '你是否確認要刪除[YEAR]第[TERM]學期的所有紀錄?';
$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Import_format'] = '
欄位 1 : 學生姓名
欄位 2 : *WebSAMS 註冊號碼
欄位 3 : *學年
欄位 4 : *學期
欄位 5 : *科目縮寫
欄位 6 : 得分';

$Lang['iPortfolio']['twghczm_pesonalcharacter'] = "個人特質及備註";
$Lang['iPortfolio']['twghczm_csv_upload'] = "每年必須上載個人特質資料，以製作年度學生學習概覽。";
$Lang['iPortfolio']['twghwfns_pesonalability'] = "個人與一般能力";
$Lang['iPortfolio']['twghwfns_csv_upload'] = "每年必須上載學生的個人與一般能力資料，以製作年度學生學習概覽。";
$Lang['iPortfolio']['fywss_csv_upload'] = "每年必須上載學生的個人特質資料，以製作年度學生學習概覽。";
$Lang['iPortfolio']['academic_performance_csv_upload'] = "每年必須上載學生的科目表現數據，以製作年度學生學習概覽 (Paulinian Award Scheme)。";

$Lang['iPortfolio']['cat_name_eng'] = "OLE類別名稱 (英文)";
$Lang['iPortfolio']['sub_cat_name_eng'] = "OLE子類別名稱 (英文)";
$Lang['iPortfolio']['program_name'] = "預設學習項目名稱 (英文)";
$Lang['iPortfolio']['old_compo'] = "OLE範疇";

$Lang['iPortfolio']['studentGroupRemark_KIS'] = "如要更新現年度的學生小組及成員，請到\"綜合平台行政管理中心 > 內聯網管理 > 用戶管理 > 學生升班系統 > 進行iPortfolio學生升班程序\" 便可。";

# Ng Wah SAS Cust
$Lang['iPortfolio']['NgWah_SAS'] = "學生獎勵計劃";
$Lang['iPortfolio']['NgWah_SAS_Report'] = "學生課外活動及獎勵紀錄表";
$Lang['iPortfolio']['NgWah_SAS_Settings']['Category'] = "SAS 分類";
$Lang['iPortfolio']['NgWah_SAS_Settings']['Credit_pt'] = "SAS 積點";
$Lang['iPortfolio']['NgWah_SAS_Settings']['Remarks'] = "<span class='tabletextrequire'>*</span> 如使用項目設定內的SAS積點, 請將學生SAS積點欄位空置";
$Lang['iPortfolio']['NgWah_SAS_Category']['Post'] = "認可職銜";
$Lang['iPortfolio']['NgWah_SAS_Category']['Achievement'] = "獎項成就";
$Lang['iPortfolio']['NgWah_SAS_Category']['Internal_Performance'] = "校內傑出表現";
$Lang['iPortfolio']['NgWah_SAS_Category']['External_Performance'] = "校外傑出表現";
$Lang['iPortfolio']['NgWah_SAS_Category']['Service'] = "服務";
$Lang['iPortfolio']['NgWah_SAS_Category']['Others'] = "其他學習活動";
$Lang['iPortfolio']['SAS_Settings']['Category_Credit_Point'] = "分類及最高積點";
$Lang['iPortfolio']['SAS_Settings']['Category'] = "分類";
$Lang['iPortfolio']['SAS_Settings']['Credit_pt'] = "積點";
$Lang['iPortfolio']['SAS_Settings']['Min_Credit_pt'] = "最低積點";
$Lang['iPortfolio']['SAS_Settings']['Max_Credit_pt'] = "最高積點";
$Lang['iPortfolio']['SAS_Settings']['Post'] = "認可職銜";
$Lang['iPortfolio']['SAS_Settings']['Achievement'] = "獎項成就";
$Lang['iPortfolio']['SAS_Settings']['Internal_Performance'] = "校內傑出表現";
$Lang['iPortfolio']['SAS_Settings']['External_Performance'] = "校外傑出表現";
$Lang['iPortfolio']['SAS_Settings']['Service'] = "服務";
$Lang['iPortfolio']['SAS_Settings']['Award'] = "獲取獎項";
$Lang['iPortfolio']['SAS_Settings']['Award_Ch'] = "獲取獎項 (中文)";
$Lang['iPortfolio']['SAS_Settings']['Award_En'] = "獲取獎項 (英文)";
$Lang['iPortfolio']['SAS_Settings']['Gold'] = "金獎";
$Lang['iPortfolio']['SAS_Settings']['Silver'] = "銀獎";
$Lang['iPortfolio']['SAS_Settings']['Bronze'] = "銅獎";
$Lang['iPortfolio']['SAS_Settings']['Release_Student'] = "發佈給學生";
$Lang['iPortfolio']['SAS_Settings']['JS_MinCreditLargerThanMaxCredit'] = "最低積點不能大於最高積點";
$Lang['iPortfolio']['SAS_Settings']['JS_CreditOutOfRange'] = "積點範圍不能重複";
$Lang['iPortfolio']['DownloadDisk']['Instruction'] = '如無法選擇班別或學生，請先到「預備光碟」預備檔案。';

$Lang['iPortfolio']['Percentile']['Setting'] = '百分位數設定';
$Lang['iPortfolio']['Percentile']['PercentileRank'] = '百分位數';
$Lang['iPortfolio']['Percentile']['FirstPercentGroup'] = '第一組';
$Lang['iPortfolio']['Percentile']['NextPercentGroup'] = '下一組';
$Lang['iPortfolio']['Percentile']['alert']['PleaseInputPercentile'] = '請輸入百一分段值 (級名次)';
$Lang['iPortfolio']['Percentile']['alert']['PercentileSumNotEqual100'] = '百一分段值 (級名次)的總和必需為100%';
$Lang['iPortfolio']['Percentile']['alert']['PleaseInputTitle'] = '請輸入整體等級';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['SchoolTerm'] = '學期';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ClassLevel'] = '級別';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ClassName'] = '班別';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['WebSAMSRegNo'] = 'WebSAMS 註冊號碼';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StudentNameEng'] = '學生英文姓名';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StudentNameChi'] = '學生中文姓名';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Type'] = '類型';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Activity'] = '活動名稱';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Role'] = '角色';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Performance'] = '表現';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Successful'] = '個資料匯入成功';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ANPType'] = '獎懲類型';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ANPDate'] = '獎懲日期';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['Description'] = '非WebSAMS事件描述';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['CodeDescription'] = 'WebSAMS事件描述';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL1'] = '缺點/優點數目';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL2'] = '小過/小功數目';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL3'] = '大過/大功數目';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['LVL4'] = '特過/特功數目';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['StaffCode'] = '教師編號';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['TeacherNameEng'] = '教師姓名-英';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['TeacherNameChi'] = '教師姓名-中';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['AcademicGrade'] = '評分';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['ConductGrade'] = '操行';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['CommentChi'] = '評語-中';
$Lang['iPortfolio']['SLP']['SchoolRecord']['Import']['CommentEng'] = '評語-英';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['AcademicYearNotFound'] = '找不到該學年';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['AcademicYearTermNotFound'] = '找不到該學期';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['StudentNotFound'] = '找不到該學生';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongType'] = '沒有填上類型或錯誤';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyActivity'] = '沒有填上活動';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyPerformance'] = '沒有填上表現';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['TeacherNotFound'] = '找不到該老師';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongANPType'] = '沒有填上獎懲類型或錯誤';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongANPDate'] = '沒有填上獎懲時間或格式錯誤';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WrongLVL'] = '沒有填上獎懲數目或錯誤';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyDescription'] = '沒有填上事件描述';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['ANPDateOutofRange'] = '獎懲時間不在學期期限之內';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['EmptyComment'] = '沒有填上評分或評語';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportError']['WebSAMSRegNoNotFound'] = '沒有設定 WebSAMS 學生註冊編號';
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportColumns']['ANP'] = array(
"<font color = red>@</font>WebSAMS 學生註冊編號",
"<font color = red>*</font>學年 (學年的格式必須為 yyyy 或 yyyy-YYYY 或 yy/YY)",
"時段",
"級別",
"<font color = red>@</font>班別",
"<font color = red>@</font>學號",
"學生英文姓名",
"學生中文姓名",
"<font color = red>*</font>獎懲類型 (必須選擇 A 或 P。 A - 獎勵, P - 懲罰)",
"<font color = red>*</font>獎懲日期 (日期的格式必須為 yyyy-mm-dd 或 dd/mm/yyyy 或 d/m/yyyy. 如你使用 Excel 編輯, 請在日期的資料上更改儲存格格式)",
"<font color = red>#^</font>非WebSAMS事件描述",
"WebSAMS事件代碼",
"WebSAMS事件描述(英文)",
"<font color = red>#^</font>WebSAMS事件描述(中文)",
"<font color = red>^</font>缺點或優點數目 (視符獎懲類型而定)",
"<font color = red>^</font>小過或小功數目 (視符獎懲類型而定)",
"<font color = red>^</font>大過或大功數目 (視符獎懲類型而定)",
"<font color = red>^</font>特過或特功數目 (視符獎懲類型而定)",
"極過或極功數目 - 沒有使用",
"操行分 - 沒有使用",
"<font color = red>+^</font>負責教師編號",
"<font color = red>+</font>負責教師英文姓名",
"<font color = red>+</font>負責教師中文姓名",
"成績表可讀取示標",
"跟進事項 - 沒有使用",
"跟進事項開始日期 - 沒有使用",
"跟進事項結束日期 - 沒有使用",
"留堂示標",
"成績表列印次序",
"獎勵類型 - 沒有使用",
"獎勵來源 - 沒有使用",
"學生學習概覽備註 - 沒有使用",
"學生學習概覽讀取示標",
"學生以前概況 - 沒有使用",
"補充資料 - 沒有使用",
"備註 - 沒有使用",
"其他語言 - 沒有使用",
"其他前況 - 沒有使用",
"其他補充資料 - 沒有使用",
"其他資料 - 沒有使用"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportRemarks']['ANP'] = array(
"附有「<font color = red>*</font>」的項目必須填寫",
"<font color = red>@</font>必須填寫 學生註冊編號 或 (班別和學號)",
"<font color = red>+</font>必須填寫 負責教師編號 或 負責教師英文姓名 或 負責教師中文姓名",
"<font color = red>#</font>必須填寫 非WebSAMS事件描述 或 WebSAMS事件描述(中文)",
"如紀錄已存在,附有「<font color = red>^</font>」的項目允許更新"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportColumns']['Activity'] = array(
"<font color = red>*</font>學年 (學年的格式必須為 yyyy 或 yyyy-YYYY 或 yy/YY)",
"<font color = red>*</font>學期 (1 - 第一學期, 2 - 第二學期)",
"級別",
"<font color = red>@</font>班別",
"<font color = red>@</font>學號",
"<font color = red>@</font>WebSAMS 學生註冊編號",
"學生英文姓名",
"學生中文姓名",
"<font color = red>*</font>類型 (必須選擇 E 或 S。 E - 活動, S - 服務)",
"<font color = red>*</font>活動名稱 (中文)",
"<font color = red>^</font>職位 (中文)",
"<font color = red>^</font>表現 (中文)"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportRemarks']['Activity'] = array(
"附有「<font color = red>*</font>」的項目必須填寫",
"<font color = red>@</font>必須填寫 學生註冊編號 或 (班別和學號)",
"如紀錄已存在,附有「<font color = red>^</font>」的項目允許更新"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportColumns']['Comment'] = array(
"<font color = red>@</font>WebSAMS 學生註冊編號",
"<font color = red>*</font>學年 (學年的格式必須為 yyyy 或 yyyy-YYYY 或 yy/YY)",
"<font color = red>*</font>學期 (0 - 全年, 1 - 第一學期, 2 - 第二學期)",
"<font color = red>@</font>班別",
"<font color = red>@</font>學號",
"學生中文姓名",
"<font color = red>+^</font>學術",
"<font color = red>+^</font>操行",
"<font color = red>+^</font>老師評語 (中文)",
"<font color = red>+^</font>老師評語 (英文)"
);
$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportRemarks']['Comment'] = array(
"附有「<font color = red>*</font>」的項目必須填寫",
"<font color = red>@</font>必須填寫 學生註冊編號 或 (班別和學號)",
"<font color = red>+</font>必須填寫至少一項: 學術, 操行, 老師評語 (中文) 及 老師評語 (英文)",
"如紀錄已存在,附有「<font color = red>^</font>」的項目允許更新"
);
$Lang['iPortfolio']['StudentAccount']['TeacherComment']['Academic'] = '學業';

// Learning Portfolio 2
$langpf_lp2['admin']['top']['new'] = "新增";
$langpf_lp2['admin']['top']['public'] = "公開";
$langpf_lp2['admin']['top']['private'] = "保密";
$langpf_lp2['admin']['top']['delete'] = "刪除";
$langpf_lp2['admin']['top']['pleaseselectportfolio'] = '請選擇檔案';
$langpf_lp2['admin']['top']['portfoliosselected'] = '項已選檔案';
$langpf_lp2['admin']['top']['areyousureto'] = '確認要將其';
$langpf_lp2['admin']['top']['managetemplates'] = "模板管理";
$langpf_lp2['admin']['top']['group'] = "組別";
$langpf_lp2['admin']['top']['portfolio'] = '學習檔案';
$langpf_lp2['admin']['top']['all'] = "全部";

$langpf_lp2['admin']['navi']['records'] = "記錄";
$langpf_lp2['admin']['navi']['total'] = "共";
$langpf_lp2['admin']['navi']['search'] = "搜尋";
$langpf_lp2['admin']['navi']['of'] = '之';
$langpf_lp2['admin']['navi']['sortby'] = '以';
$langpf_lp2['admin']['navi']['in'] = '作';
$langpf_lp2['admin']['navi']['order'] = '排序';
$langpf_lp2['admin']['navi']['order_choice']['modified'] = "最後修改時間";
$langpf_lp2['admin']['navi']['order_choice']['title'] = "標題";
$langpf_lp2['admin']['navi']['order_choice']['deadline'] = "提交限期";
$langpf_lp2['admin']['navi']['order_choice']['student_count'] = "學生數目";
$langpf_lp2['admin']['navi']['order_choice']['status'] = $langpf_lp2['admin']['top']['public'].'/'.$langpf_lp2['admin']['top']['private'];
$langpf_lp2['admin']['navi']['ascending'] = "遞增";
$langpf_lp2['admin']['navi']['descending'] = "遞減";
$langpf_lp2['admin']['navi']['showpublishedonly'] = '只顯示已發佈檔案';
$langpf_lp2['admin']['navi']['page'] = "頁";
$langpf_lp2['admin']['navi']['display'] = "顯示";
$langpf_lp2['admin']['navi']['all'] = "全部";
$langpf_lp2['admin']['navi']['perpage'] = "每頁";
$langpf_lp2['admin']['navi']['back'] = "返回" ;
$langpf_lp2['admin']['navi']['studentprogress'] = '學生進度';
$langpf_lp2['admin']['navi']['studentProgressSortFields']['className|asc'] = "班別 - 班號 (遞增)";
$langpf_lp2['admin']['navi']['studentProgressSortFields']['className|desc'] = "班別 - 班號 (遞減)";
$langpf_lp2['admin']['navi']['studentProgressSortFields']['notes_published|asc'] = "發佈日期 (遞增)";
$langpf_lp2['admin']['navi']['studentProgressSortFields']['notes_published|desc'] = "發佈日期 (遞減)";
$langpf_lp2['admin']['navi']['publishStatus'] = "發佈狀態";
$langpf_lp2['admin']['navi']['studentProgressPublishStatus']['all'] = "顯示全部檔案";
$langpf_lp2['admin']['navi']['studentProgressPublishStatus']['published'] = "只顯示已發佈檔案";
$langpf_lp2['admin']['navi']['studentProgressPublishStatus']['notPublished'] = "只顯示未發佈檔案";

$langpf_lp2['admin']['item']['edit'] = "編緝";
$langpf_lp2['admin']['item']['settings'] = "設定";
$langpf_lp2['admin']['item']['publish'] = "發佈";
$langpf_lp2['admin']['item']['export'] = '匯出';
$langpf_lp2['admin']['item']['clone'] = '複製';
$langpf_lp2['admin']['item']['starttime'] = "開始時間";
$langpf_lp2['admin']['item']['deadline'] = "提交限期";
$langpf_lp2['admin']['item']['lastmodified'] = "最後修改時間";
$langpf_lp2['admin']['item']['published'] = "已發佈";
$langpf_lp2['admin']['item']['drafted'] = '已起草';
$langpf_lp2['admin']['item']["lessthan1minago"] = '少於 1 分鐘前';
$langpf_lp2['admin']['item']["minsago"] = '分鐘前';
$langpf_lp2['admin']['item']["hoursago"] = '小時前';
$langpf_lp2['admin']['item']['today'] = "今日";
$langpf_lp2['admin']['item']['yesterday'] = "昨日";
$langpf_lp2['admin']['item']['daysago'] = '天前';
$langpf_lp2['admin']['item']['dayago'] = '天前';
$langpf_lp2['admin']['item']['private'] = "保密";
$langpf_lp2['admin']['item']['viewprogress'] = '檢視進度';
$langpf_lp2['admin']['item']['total'] = '共有';
$langpf_lp2['admin']['item']['students'] = "學生";
$langpf_lp2['admin']['item']['share'] = '分享';
$langpf_lp2['admin']['item']['clonefrom'] = '複製副本 - ';
$langpf_lp2['admin']['item']['newportfolio'] = '新建檔案';
$langpf_lp2['admin']['item']['pleaseenaterperiod'] = '請輸入時段';
$langpf_lp2['admin']['item']['areyousuretoforcepublish'] = '確認要強制發佈嗎?';

$langpf_lp2['admin']['export']['student'] = "學生名稱";
$langpf_lp2['admin']['export']['class'] = "班別";
$langpf_lp2['admin']['print']['class'] = "班別";
$langpf_lp2['admin']['print']['selectclass'] = '選擇班級';
$langpf_lp2['admin']['print']['student'] = "學生";
$langpf_lp2['admin']['print']['selectall'] = "全選";
$langpf_lp2['admin']['print']['page'] = "頁";
$langpf_lp2['admin']['print']['continue'] = "繼續";

$langpf_lp2['edit']['tool']['settings'] = "設定";
$langpf_lp2['edit']['tool']['publish'] = '發佈';
$langpf_lp2['edit']['tool']['recyclebin'] = '回收箱';
$langpf_lp2['edit']['tool']['new'] = "新增";
$langpf_lp2['edit']['tool']['newweblog'] = '新增網誌';
$langpf_lp2['edit']['tool']['edit'] = "編緝";
$langpf_lp2['edit']['tool']['delete'] = "刪除";
$langpf_lp2['edit']['tool']['reset'] = "還原至老師所作的模版";
$langpf_lp2['edit']['tool']['reorder'] = '排序';
$langpf_lp2['edit']['tool']['dragtoreorder'] = '拖曳以排序';
$langpf_lp2['edit']['tool']['share'] = '分享';
$langpf_lp2['edit']['tool']['print'] = '列印';

$langpf_lp2['edit']['themesettings']['theme'] = '主題';
$langpf_lp2['edit']['themesettings']['banner'] = '橫額';
$langpf_lp2['edit']['themesettings']['enablebanner'] = '顯示橫額';
$langpf_lp2['edit']['themesettings']['allowstudentstocustomizebanner'] = '容許學生自訂橫額';
$langpf_lp2['edit']['themesettings']['uploadedbanner'] = '已上載的橫額';
$langpf_lp2['edit']['themesettings']['bestsize'] = '最佳大小';
$langpf_lp2['edit']['themesettings']['fileformat'] = '圖片格式';
$langpf_lp2['edit']['themesettings']['rootsections'] = '頂級分頁';
$langpf_lp2['edit']['themesettings']['allowstudentstocustomizerootsections'] = '容許學生自訂頂級分頁';

$langpf_lp2['edit']['recyclebin']['recoveredcontent'] = '已回復內容';
$langpf_lp2['edit']['recyclebin']['removedcontent'] = '已刪除內容';
$langpf_lp2['edit']['recyclebin']['select'] = '--請選擇--';
$langpf_lp2['edit']['recyclebin']['deletedat'] = '刪除時間:';
$langpf_lp2['edit']['recyclebin']['close'] = '關閉';

$langpf_lp2['edit']['weblog']['newweblog'] = '新增網誌';
$langpf_lp2['edit']['weblog']['weblogtitle'] = '網誌標題';
$langpf_lp2['edit']['weblog']['weblogcontent'] = '網誌內容';

$langpf_lp2['edit']['section']['main'] = '主頁';
$langpf_lp2['edit']['section']['main_alt'] = 'Main';

$langpf_lp2['edit']['editform']['title'] = "標題";
$langpf_lp2['edit']['editform']['date'] = "日期";
$langpf_lp2['edit']['editform']['instruction'] = "指示";
$langpf_lp2['edit']['editform']['content'] = "內容";
$langpf_lp2['edit']['editform']['pleaseentertitle'] = '請輸入標題';
$langpf_lp2['edit']['editform']['pleaseentervaliddate'] = '請輸入有效日期';
$langpf_lp2['edit']['editform']['loadtemplate'] = '載入模板';
$langpf_lp2['edit']['editform']['newtemplate'] = '新建模板';
$langpf_lp2['edit']['editform']['pleaseenternewtemplatetitle'] = '請輸入新建模板的名稱';
$langpf_lp2['edit']['editform']['areyousuretosavethecontentof'] = '確認要儲存內容至';
$langpf_lp2['edit']['editform']['templatenamealreadyexists'] = '已有相同名稱的模板, 請重新輸入';
$langpf_lp2['edit']['editform']['load'] = '載入';
$langpf_lp2['edit']['editform']['save'] = '儲存';
$langpf_lp2['edit']['editform']['weblog'] = '網誌';
$langpf_lp2['edit']['editform']['custom'] = '網頁';
$langpf_lp2['edit']['editform']['blank'] = '沒有';
$langpf_lp2['edit']['editform']['allowstudentsto'] = '學生權限';
$langpf_lp2['edit']['editform']['from'] = '由';
$langpf_lp2['edit']['editform']['to'] = '至';
$langpf_lp2['edit']['editform']['hidden'] = '(隱藏)';
$langpf_lp2['edit']['editform']['editcontent'] = '編緝內容';
$langpf_lp2['edit']['editform']['editcontentandcustomizesections'] = '編緝內容並自訂分頁';
$langpf_lp2['edit']['editform']['showthissectionto'] = '將本分頁公開給';
$langpf_lp2['edit']['editform']['fileupload'] = '檔案上載';
$langpf_lp2['edit']['editform']['addfile'] = '加入檔案';
$langpf_lp2['edit']['editform']['pleaseselectpdffile'] = '請選擇PDF檔(.pdf)';

$langpf_lp2['edit']['message']['warning'] = '警告';
$langpf_lp2['edit']['message']['notice'] = '告示';
$langpf_lp2['edit']['message']['portfolioschemeupdated'] = "學習檔案格式已更新";
$langpf_lp2['edit']['message']['draftexist'] = '更新檔案格式將會影響到已開始的學生!';
$langpf_lp2['edit']['message']['asksync'] = "老師已更新學習檔案格式<br/>你可以馬上執行同步, 但會重設已更新部份的內容!";
$langpf_lp2['edit']['message']['newsectionscreated'] = '老師新增了學習檔案的分頁</br>要馬上匯入嗎';
$langpf_lp2['edit']['message']['sectionupdated'] = '"%1$s" 已被老師%2$s了, 你要馬上執行%2$s嗎';
$langpf_lp2['edit']['message']['update'] = "更新";
$langpf_lp2['edit']['message']['updatenow'] = "立即更新";
$langpf_lp2['edit']['message']['updatelater'] = "暫不更新";
$langpf_lp2['edit']['message']['areyousure'] = "確認?";
$langpf_lp2['edit']['message']['areyousuretodelete'] = "確認要刪除";
$langpf_lp2['edit']['message']['draftnoteditable'] = '本檔案已超出可修改的期限';
$langpf_lp2['edit']['message']['discardchangesandclose'] = "關閉並放棄已更改內容?";
$langpf_lp2['edit']['message']['remembertopublishupdates'] = "請緊記發佈已更改的內容!";
$langpf_lp2['edit']['message']['dropbannerhere'] = "在此放下橫額檔案/網址";
$langpf_lp2['edit']['message']['isnotavaildbannerformat'] = "並不是有效的橫額格式!";
$langpf_lp2['edit']['message']['doesnotsupportuploadmethod'] = "你的瀏覽器不支援此上載橫額的方法, 請在\"設定\"內上載";

$langpf_lp2['common']['share']['shareurl'] = '公開網址';
$langpf_lp2['common']['share']['facebook'] = 'Facebook';
$langpf_lp2['common']['share']['sharetofacebook'] = '分享至 Facebook';
$langpf_lp2['common']['share']['sharetopeer'] = '向朋友分享';
$langpf_lp2['common']['share']['findstudents'] = '搜尋學生';
$langpf_lp2['common']['share']['allclasses'] = '所有班級';
$langpf_lp2['common']['share']['studentnameorloginid'] = '學生名稱或登入帳號';
$langpf_lp2['common']['share']['search'] = "搜尋";
$langpf_lp2['common']['share']['searchresult'] = '搜尋結果';
$langpf_lp2['common']['share']['add'] = '增加';
$langpf_lp2['common']['share']['remove'] = '移除';
$langpf_lp2['common']['share']['sharedfriends'] = '已分享的朋友';
$langpf_lp2['common']['share']['my_friend_list'] = '我的朋友名單';
$langpf_lp2['common']['share']['searchByInputFormat'] = '輸入"[班名] [學號] [姓名]"<br/>(e.g. 1a15 Chan)';
$langpf_lp2['common']['publish']['useindividualsettings'] = '(使用個別設定)';
$langpf_lp2['common']['publish']['sectionsettings'] = '分頁設定';
$langpf_lp2['common']['publish']['setallsectionsvisibleto'] = '將 <strong>所有</strong> 分頁公開給';
$langpf_lp2['common']['publish']['showfacebooklikebutton'] = '顯示Facebook讚好按鈕';
$langpf_lp2['common']['publish']['publishsuccess'] = '檔案發佈成功';


$langpf_lp2['common']['form']['myselfonly'] = '自己';
$langpf_lp2['common']['form']['usersinsamegroup'] = '同組別的使用者';
$langpf_lp2['common']['form']['allintranetusers'] = '所有內聯網使用者';
$langpf_lp2['common']['form']['public'] = '所有人';
$langpf_lp2['common']['form']['publish'] = '發佈';
$langpf_lp2['common']['form']['apply'] = '套用';
$langpf_lp2['common']['form']['cancel'] = '取消';
$langpf_lp2['common']['form']['close'] = '關閉';
$langpf_lp2['common']['form']['yes'] = "是";
$langpf_lp2['common']['form']['no'] = "否";
$langpf_lp2['common']['error']['noavailable'] = "無可用的學習檔案";

$Lang['iPortfolio']['SPTSS']['SPC']['Student']='學生';
$Lang['iPortfolio']['SPTSS']['SPC']['Integrity']='誠信';
$Lang['iPortfolio']['SPTSS']['SPC']['Courtesy']='禮貌';
$Lang['iPortfolio']['SPTSS']['SPC']['Right From Wrong']='明辨是非';
$Lang['iPortfolio']['SPTSS']['SPC']['Relationship']='人際關係';
$Lang['iPortfolio']['SPTSS']['SPC']['Learning Attitude']='學習態度';
$Lang['iPortfolio']['SPTSS']['SPC']['Autonomy']='自治';

$Lang['iPortfolio']['Promotion'] = "升留紀錄";

$Lang['iPortfolio']['JinYing']['JinYingScheme'] = "剪影計劃";
$Lang['iPortfolio']['JinYing']['SelectItem'] = "- 選擇項目 -";
$Lang['iPortfolio']['JinYing']['ajaxError'] = "AJAX 錯誤!";
$Lang['iPortfolio']['JinYing']['ActivityItems'] = "活動項目";
$Lang['iPortfolio']['JinYing']['EmptySymbol'] = "-";
$Lang['iPortfolio']['JinYing']['TargetAttained'] = "達標";
$Lang['iPortfolio']['JinYing']['TargetHighlyAttained'] = "達標並表現良好";
$Lang['iPortfolio']['JinYing']['TargetNotAttained'] = "未達標，仍須努力";
$Lang['iPortfolio']['JinYing']['Scope']['BSQ'] = "提升個人素質";
$Lang['iPortfolio']['JinYing']['Scope']['EPW'] = "身心靈健康";
$Lang['iPortfolio']['JinYing']['Scope']['OFS'] = "服務精神";
$Lang['iPortfolio']['JinYing']['Scope']['BTP'] = "發揮個人才華";
$Lang['iPortfolio']['JinYing']['ScopeName'] = "範疇";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['HW'] = "交齊家課";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['PC'] = "守時";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['AD'] = "勤到";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['AI'] = "校服儀容整潔";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['RC'] = "閱讀圖書(中文)";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['RE'] = "閱讀圖書(英文)";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['EC'] = "參加港外考察活動#";
$Lang['iPortfolio']['JinYing']['Item']['BSQ']['TA'] = "參加培訓課程(提素質)#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['PY'] = "具理想體質#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['PJ'] = "具良好體能(跑步)#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['PS'] = "具良好體能(游泳)#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['PW'] = "具良好整體體能";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['CA'] = "參加基督教活動";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['TA'] = "參加培訓課程(增健康)#";
$Lang['iPortfolio']['JinYing']['Item']['EPW']['OC'] = "值得欣賞之處";
$Lang['iPortfolio']['JinYing']['Item']['OFS']['SI'] = "參與校內服務#";
$Lang['iPortfolio']['JinYing']['Item']['OFS']['SO'] = "參與校外服務#";
$Lang['iPortfolio']['JinYing']['Item']['BTP']['EA'] = "積極參與課外活動#";
$Lang['iPortfolio']['JinYing']['Item']['BTP']['IS'] = "積極參與校內比賽#";
$Lang['iPortfolio']['JinYing']['Item']['BTP']['IC'] = "積極參與校外比賽#";
$Lang['iPortfolio']['JinYing']['Item']['BTP']['TP'] = "樂於參與才藝表演#";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-HW']['BSQ-HW-P-01'] = "1. 交齊所有家課";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-HW']['BSQ-HW-P-02'] = "2. 欠交功課次數少於五次";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-HW']['BSQ-HW-N-01'] = "3. 欠交功課次數五次或以上";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-PC']['BSQ-PC-P-01'] = "1.從未遲到";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-PC']['BSQ-PC-P-02'] = "2.遲到次數少於三次";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-PC']['BSQ-PC-N-01'] = "3.遲到次數三次或以上";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-AD']['BSQ-AD-P-01'] = "1.出席所有上課天";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-AD']['BSQ-AD-N-01'] = "2.曾經請假";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-AI']['BSQ-AI-P-01'] = "1.符合規定";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-AI']['BSQ-AI-N-01'] = "2.違規";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-RC']['BSQ-RC-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-RC']['BSQ-RC-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-RE']['BSQ-RE-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-RE']['BSQ-RE-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-EC']['BSQ-EC-P-01'] = "a.達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-EC']['BSQ-EC-P-02'] = "b.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-EC']['BSQ-EC-N-01'] = "c.未達標，仍須努力";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-TA']['BSQ-TA-P-01'] = "a.達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-TA']['BSQ-TA-P-02'] = "b.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BSQ-TA']['BSQ-TA-N-01'] = "c.未達標，仍須努力";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PY']['EPW-PY-P-01'] = "1.合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PY']['EPW-PY-N-01'] = "2.不合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PJ']['EPW-PJ-P-01'] = "1.合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PJ']['EPW-PJ-N-01'] = "2.不合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PS']['EPW-PS-P-01'] = "1.合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PS']['EPW-PS-N-01'] = "2.不合格";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PW']['EPW-PW-P-01'] = "1.成績B+或以上";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PW']['EPW-PW-P-02'] = "2.成績B至E";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-PW']['EPW-PW-N-01'] = "3.成績F或以下";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-CA']['EPW-CA-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-CA']['EPW-CA-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-TA']['EPW-TA-P-01'] = "a.達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-TA']['EPW-TA-P-02'] = "b.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-TA']['EPW-TA-N-01'] = "c.未達標，仍須努力";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-OC']['EPW-OC-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['EPW-OC']['EPW-OC-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeName']['OFS-SI']['OFS-SI-P-01'] = "a.表現良好，獲記功（填H欄）";
$Lang['iPortfolio']['JinYing']['CodeName']['OFS-SI']['OFS-SI-P-02'] = "b.不獲記功";
$Lang['iPortfolio']['JinYing']['CodeName']['OFS-SO']['OFS-SO-P-01'] = "校外服務時數";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-EA']['BTP-EA-P-01'] = "a.出席不少於70%而表現積極";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-EA']['BTP-EA-P-02'] = "b.出席不少於70%而表現一般";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-EA']['BTP-EA-N-01'] = "c.出席率不足70%或表現欠佳";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IS']['BTP-IS-P-01'] = "1.曾參加並獲獎";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IS']['BTP-IS-P-02'] = "2.曾參加但未有獲獎";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IS']['BTP-IS-N-01'] = "3.未曾參加";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-01'] = "a.全年出席練習不少於70%而表現積極";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-02'] = "b.全年出席練習不少於70%而表現滿意";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-N-01'] = "c.全年出席練習不足70%或表現欠佳";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-03'] = "d.獲獎項及推薦記功 (填K L及M欄)";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-IC']['BTP-IC-P-04'] = "e.無需賽前練習。無獲獎";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-TP']['BTP-TP-P-01'] = "1.達標";
$Lang['iPortfolio']['JinYing']['CodeName']['BTP-TP']['BTP-TP-N-01'] = "2.未達標";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-HW-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-HW-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-HW-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-PC-P-01'] = "達標，無遲到紀錄";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-PC-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-PC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-AD-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-AD-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-AI-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-AI-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-RC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-RC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-RE-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-RE-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-EC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-EC-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-EC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-TA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-TA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BSQ-TA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PY-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PY-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PJ-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PJ-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PS-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PS-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PW-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PW-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-PW-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-CA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-CA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-TA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-TA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-TA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-OC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['EPW-OC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['OFS-SI-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['OFS-SI-P-02'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['OFS-SO-P-01'] = "參與校外義工服務 %s 小時";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-EA-P-01'] = "全年出席率不少於70%而表現積極";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-EA-P-02'] = "全年出席率不少於70%而表現一般";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-EA-N-01'] = "全年出席率不足70%或表現欠佳";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IS-P-01'] = "曾參與校內比賽";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IS-P-02'] = "曾參與校內比賽";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IS-N-01'] = "無紀錄";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-P-01'] = "參與。全年練習出席率不少於70%而表現積極";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-P-02'] = "參與。全年練習出席率不少於70%而表現一般";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-P-03'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-P-04'] = "參與";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-IC-N-01'] = "參與。全年練習出席率不足70%或表現欠佳";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-TP-P-01'] = "曾參與才藝表演";
$Lang['iPortfolio']['JinYing']['CodeDesc']['JYR']['BTP-TP-N-01'] = "無紀錄";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-HW-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-HW-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-HW-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-PC-P-01'] = "優異";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-PC-P-02'] = "良好";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-PC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-AD-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-AD-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-AI-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-AI-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-RC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-RC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-RE-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-RE-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-EC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-EC-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-EC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-TA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-TA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BSQ-TA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PY-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PY-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PJ-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PJ-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PS-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PS-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PW-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PW-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-PW-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-CA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-CA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-TA-P-01'] = $Lang['iPortfolio']['JinYing']['TargetHighlyAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-TA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-TA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-OC-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['EPW-OC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['OFS-SI-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['OFS-SI-P-02'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['OFS-SO-P-01'] = "參與校外義工服務 %s 小時";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-EA-P-01'] = "全年出席率不少於70%而表現積極";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-EA-P-02'] = "全年出席率不少於70%而表現一般";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-EA-N-01'] = "參與";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IS-P-01'] = "曾參與校內比賽";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IS-P-02'] = "曾參與校內比賽";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IS-N-01'] = "無紀錄";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-P-01'] = "參與。全年練習出席率不少於70%而表現積極";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-P-02'] = "參與。全年練習出席率不少於70%而表現一般";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-P-03'] = "";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-P-04'] = "參與";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-IC-N-01'] = "參與";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-TP-P-01'] = "曾參與才藝表演";
$Lang['iPortfolio']['JinYing']['CodeDesc']['SLP']['BTP-TP-N-01'] = "無紀錄";
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-HW-P-01'] = "優點1個";
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-HW-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-HW-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-PC-P-01'] = "優點1個";
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-PC-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-PC-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-AD-P-01'] = "優點1個";
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-AD-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-AI-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-AI-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-RC-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-RC-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-RE-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-RE-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-EC-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-EC-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-EC-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-TA-P-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-TA-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BSQ-TA-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PY-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PY-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PJ-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PJ-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PS-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PS-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PW-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PW-P-02'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-PW-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-CA-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-CA-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-TA-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-TA-P-02'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-TA-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-OC-P-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['EPW-OC-N-01'] = "";
$Lang['iPortfolio']['JinYing']['CodeAward']['OFS-SI-P-01'] = "獲%s%s個";
$Lang['iPortfolio']['JinYing']['CodeAward']['OFS-SI-P-02'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['OFS-SO-P-01'] = "優點%s個";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-EA-P-01'] = "達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-EA-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-EA-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IS-P-01'] = "達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IS-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IS-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-01'] = "達標並表現良好";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-02'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-03'] = "獲%s%s個";
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-04'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-N-01'] = $Lang['iPortfolio']['JinYing']['TargetNotAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-TP-P-01'] = $Lang['iPortfolio']['JinYing']['TargetAttained'];
$Lang['iPortfolio']['JinYing']['CodeAward']['BTP-TP-N-01'] = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
$Lang['iPortfolio']['JinYing']['Award']['Merit_1'] = "優點1個";
$Lang['iPortfolio']['JinYing']['Award']['Merit_2'] = "優點2個";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_1'] = "小功1個";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_1_Merit_1'] = "小功1個及優點1個";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_1_Merit_2'] = "小功1個及優點2個";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_2'] = "小功2個";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_2_Merit_1'] = "小功2個及優點1個";
$Lang['iPortfolio']['JinYing']['Award']['MinorM_2_Merit_2'] = "小功2個及優點2個";
$Lang['iPortfolio']['JinYing']['Award']['MajorM_1'] = "大功1個";
$Lang['iPortfolio']['JinYing']['Export']['Error']['InputError'] = "輸入錯誤";
$Lang['iPortfolio']['JinYing']['Export']['Error']['InputInteger4ServiceHours'] = "請輸入1~300之間的正整數";
$Lang['iPortfolio']['JinYing']['Export']['Error']['NotInList'] = "輸入的值不正確,請從清單中挑選";
$Lang['iPortfolio']['JinYing']['Export']['Error']['ReadOnlyTitle'] = "唯讀項目";
$Lang['iPortfolio']['JinYing']['Export']['Error']['ReadOnlyContent'] = "不能修改唯讀項目";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-HW'] = "班主任";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-PC'] = "訓導組";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-AD'] = "訓導組";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-AI'] = "訓導組";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-RC'] = "中文科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-RE'] = "英文科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-EC'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BSQ-TA'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-PY'] = "體育科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-PJ'] = "體育科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-PS'] = "體育科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-PW'] = "體育科";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-CA'] = "宗教組";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-TA'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['EPW-OC'] = "班主任";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['OFS-SI'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['OFS-SO'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BTP-EA'] = "會員制學會負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BTP-IS'] = "班主任";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BTP-IC'] = "活動負責老師";
$Lang['iPortfolio']['JinYing']['Export']['PIC']['BTP-TP'] = "班主任";
$Lang['iPortfolio']['JinYing']['Export']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
$Lang['iPortfolio']['JinYing']['Export']['WarningSelectItem'] = "請選擇活動項目";
$Lang['iPortfolio']['JinYing']['Export']['WarningSelectClassName'] = "請選擇班別";
$Lang['iPortfolio']['JinYing']['Export']['WarningSelectTerm'] = "請選擇學期";
$Lang['iPortfolio']['JinYing']['Import']['ActionSubType'] = "行動子類別";
$Lang['iPortfolio']['JinYing']['Import']['ActionType'] = "行動類別";
$Lang['iPortfolio']['JinYing']['Import']['AddNew'] = "新增";
$Lang['iPortfolio']['JinYing']['Import']['Delete'] = "刪除";
$Lang['iPortfolio']['JinYing']['Import']['DeleteSuccessful'] = "刪除剪影計劃成功";
$Lang['iPortfolio']['JinYing']['Import']['FileFormat'] = "必需為 Excel 2007 (.xlsx) 的格式";
$Lang['iPortfolio']['JinYing']['Import']['Replace'] = "取代";
$Lang['iPortfolio']['JinYing']['Import']['Successful'] = "剪影計劃匯入成功";
$Lang['iPortfolio']['JinYing']['Import']['Update'] = "更新";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['ClassName'] ="班別";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['ClassNumber'] ="班號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['WebSAMS'] ="WebSAMS 學生註冊編號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['StudentName'] ="姓名";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['ActivityNameEng'] ="會員制學會英文名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['ActivityNameChi'] ="會員制學會中文名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-EA']['Performance'] ="表現";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ClassName'] ="班別";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ClassNumber'] ="班號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['WebSAMS'] ="WebSAMS 學生註冊編號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['StudentName'] ="姓名";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ActivityDate'] ="活動日期";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ActivityNameEng'] ="英文活動名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['ActivityNameChi'] ="中文活動名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['OrganizationNameEng'] ="主辦機構英文名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['OrganizationNameChi'] ="主辦機構中文名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['Performance'] ="表現";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['AwardNameEng'] ="獎項英文名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['AwardNameChi'] ="獎項中文名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['BTP-IC']['RecommendMerit'] ="推薦記功數目";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['ClassName'] ="班別";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['ClassNumber'] ="班號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['WebSAMS'] ="WebSAMS 學生註冊編號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['StudentName'] ="姓名";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['ActivityNameEng'] ="服務崗位英文名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['ActivityNameChi'] ="服務崗位中文名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['Performance'] ="表現";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SI']['RecommendMerit'] ="推薦記功數目";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['ClassName'] ="班別";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['ClassNumber'] ="班號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['WebSAMS'] ="WebSAMS 學生註冊編號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['StudentName'] ="姓名";
$Lang['iPortfolio']['JinYing']['ImportColumns']['OFS-SO']['ServiceHours'] ="服務時數";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['ClassName'] ="班別";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['ClassNumber'] ="班號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['WebSAMS'] ="WebSAMS 學生註冊編號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['StudentName'] ="姓名";
$Lang['iPortfolio']['JinYing']['ImportColumns']['STANDARD']['Performance'] ="表現";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ClassName'] ="班別";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ClassNumber'] ="班號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['WebSAMS'] ="WebSAMS 學生註冊編號";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['StudentName'] ="姓名";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ActivityDate'] ="活動日期";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ActivityNameEng'] ="英文活動名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ActivityNameChi'] ="中文活動名稱";
$Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['Performance'] ="表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['WITH-ACTIVITY-DATE']['AwardInReport'] ="報告顯示獎項/成就";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['WITH-ACTIVITY-DATE']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['PHYSICAL']['PerformanceInReport'] ="報告顯示表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['PHYSICAL']['AwardInReport'] ="報告顯示獎項/成就";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['PHYSICAL']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SI']['PerformanceInReport'] ="報告顯示表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SI']['AwardInReport'] ="記功數目";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SI']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SO']['AwardInReport'] ="報告顯示獎項/成就 (獲記功數目)";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['OFS-SO']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-EA']['PerformanceInReport'] ="報告顯示表現(評語)";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-EA']['AwardInReport'] ="報告顯示獎項/成就";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-EA']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-IC']['PerformanceInReport'] ="報告顯示表現(評語)";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-IC']['AwardInReport'] ="記功數目";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['BTP-IC']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['PerformanceInReport'] ="報告顯示表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['AwardInReport'] ="報告顯示獎項/成就";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['Semester'] ="學期";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD-WO-AWARD']['PerformanceInReport'] ="報告顯示表現";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD-WO-AWARD']['Semester'] ="學期";
$Lang['iPortfolio']['JinYing']['ImportColumns4Update']['STANDARD-WO-AWARD']['RecordID'] ="RecordID";
$Lang['iPortfolio']['JinYing']['ImportRemarks']['FillInEnglish'] = "請填英文名稱";
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'] = "附有「<span class=tabletextrequire>*</span>」的項目必須填寫";
$Lang['iPortfolio']['JinYing']['ImportRemarks']['ACTIVITY'] = array(
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'],
"<span class=tabletextrequire>^</span>必須填寫至少一項: 英文活動名稱 或 中文活動名稱"
);
$Lang['iPortfolio']['JinYing']['ImportRemarks']['BTP-EA'] = array(
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'],
"<span class=tabletextrequire>^</span>必須填寫至少一項: 會員制學會英文名稱 或 會員制學會中文名稱"
);
$Lang['iPortfolio']['JinYing']['ImportRemarks']['OFS-SI'] = array(
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'],
"<span class=tabletextrequire>^</span>必須填寫至少一項: 服務崗位英文名稱 或 服務崗位中文名稱"
);
$Lang['iPortfolio']['JinYing']['ImportRemarks']['STANDARD'] = array($Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory']);
$Lang['iPortfolio']['JinYing']['ImportRemarks']['Update'] = "按照 RecordID";
$Lang['iPortfolio']['JinYing']['ImportStepArr']['SelectItem'] = "選擇匯入項目";
$Lang['iPortfolio']['JinYing']['ImportStepArr']['Confirmation'] = "資料驗證";
$Lang['iPortfolio']['JinYing']['ImportStepArr']['ImportResult'] = "匯入結果";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptyActivity'] = "沒有填上活動";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptyClubName'] = "沒有填上學會名稱";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptyPerformance'] = "沒有填上表現";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptySemester'] = "沒有揀選學期";
$Lang['iPortfolio']['JinYing']['ImportError']['EmptyService'] = "沒有填上服務崗位";
$Lang['iPortfolio']['JinYing']['ImportError']['InvalidPerformanceCode'] = "表現描述無效";
$Lang['iPortfolio']['JinYing']['ImportError']['LoadingFile'] = "讀取檔案時發生錯誤\"";
$Lang['iPortfolio']['JinYing']['ImportError']['NotNumeric'] = "非數字";
$Lang['iPortfolio']['JinYing']['ImportError']['PerformanceConflictAward'] = "沒有獲獎項及推薦記功,請勿填上獎項名稱及推薦記功數目";
$Lang['iPortfolio']['JinYing']['ImportError']['RecordDuplicate'] = "資料重複";
$Lang['iPortfolio']['JinYing']['ImportError']['RecordExist'] = "資料已經存在";
$Lang['iPortfolio']['JinYing']['ImportError']['RecordIDNotFound'] = "找不到該項紀錄";
$Lang['iPortfolio']['JinYing']['ImportError']['StudentNotFound'] = "找不到該學生";
$Lang['iPortfolio']['JinYing']['ImportError']['StudentNameNotMatch'] = "學生姓名與WebSAMS紀錄不符";
$Lang['iPortfolio']['JinYing']['ImportWarning']['NonCurrentYear'] = "閣下所選的學年並非本學年,是否繼續?";
$Lang['iPortfolio']['JinYing']['Medal']['AllMedal'] = "獲章級";
$Lang['iPortfolio']['JinYing']['Medal']['AssignBronze'] = "給予銅章";
$Lang['iPortfolio']['JinYing']['Medal']['AssignGold'] = "給予金章";
$Lang['iPortfolio']['JinYing']['Medal']['AssignSilver'] = "給予銀章";
$Lang['iPortfolio']['JinYing']['Medal']['Award'] = "獲章";
$Lang['iPortfolio']['JinYing']['Medal']['Awarded'] = "已獲章";
$Lang['iPortfolio']['JinYing']['Medal']['AwardStatus'] = "獲章狀態";
$Lang['iPortfolio']['JinYing']['Medal']['Bronze'] = "銅章";
$Lang['iPortfolio']['JinYing']['Medal']['CancelAward'] = "取消獲章";
$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignBronze'] = "是否確定給予銅章";
$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignGold'] = "是否確定給予金章";
$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignSilver'] = "是否確定給予銀章";
$Lang['iPortfolio']['JinYing']['Medal']['ConfirmCancelAward'] = "是否確定取消獲章";
$Lang['iPortfolio']['JinYing']['Medal']['Form'] = "年級";
$Lang['iPortfolio']['JinYing']['Medal']['Gold'] = "金章";
$Lang['iPortfolio']['JinYing']['Medal']['GoodPoint'] = "優點";
$Lang['iPortfolio']['JinYing']['Medal']['HideOption'] = "隱藏選項";
$Lang['iPortfolio']['JinYing']['Medal']['Medal'] = "獎章";
$Lang['iPortfolio']['JinYing']['Medal']['NoMedal'] = "無獎章";
$Lang['iPortfolio']['JinYing']['Medal']['NotAwarded'] = "未獲章";
$Lang['iPortfolio']['JinYing']['Medal']['OrAbove'] = "或以上";
$Lang['iPortfolio']['JinYing']['Medal']['Point'] = "個";
$Lang['iPortfolio']['JinYing']['Medal']['PresetCriteria'] = "預設的獲章條件";
$Lang['iPortfolio']['JinYing']['Medal']['ScopeRequirement'] = "各範疇的達標要求";
$Lang['iPortfolio']['JinYing']['Medal']['ShowOption'] = "顯示選項";
$Lang['iPortfolio']['JinYing']['Medal']['Silver'] = "銀章";
$Lang['iPortfolio']['JinYing']['Medal']['TargetMet'] = "達標";
$Lang['iPortfolio']['JinYing']['Medal']['TargetMetWiGood'] = "達標並表現良好";
$Lang['iPortfolio']['JinYing']['Medal']['TargetMetScopeNumber'] = "達標範疇數目";
$Lang['iPortfolio']['JinYing']['Medal']['Times'] = "次";
$Lang['iPortfolio']['JinYing']['MedalError']['EmptyClassLevel'] = "請選擇年級";
$Lang['iPortfolio']['JinYing']['RecommendAward']['Merit_1'] = "a.優點1個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['Merit_2'] = "b.優點2個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_1'] = "c.小功1個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_1_Merit_1'] = "d.小功1個及優點1個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_1_Merit_2'] = "e.小功1個及優點2個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_2'] = "f.小功2個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_2_Merit_1'] = "g.小功2個及優點1個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MinorM_2_Merit_2'] = "h.小功2個及優點2個";
$Lang['iPortfolio']['JinYing']['RecommendAward']['MajorM_1'] = "i.大功1個";
$Lang['iPortfolio']['JinYing']['Remark'] = "# 表示不分學期的項目";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Gold'] = "該生在學生剪影計劃中獲金章。";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Silver'] = "該生在學生剪影計劃中獲銀章。";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Bronze'] = "該生在學生剪影計劃中獲銅章。";
$Lang['iPortfolio']['JinYing']['Report']['Award']['None'] = "該生在學生剪影計劃中未獲奬章。";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Excellent'] = "優異表現";
$Lang['iPortfolio']['JinYing']['Report']['Award']['Merit'] = "優點";
$Lang['iPortfolio']['JinYing']['Report']['Award']['MinorM'] = "小功";
$Lang['iPortfolio']['JinYing']['Report']['Award']['MajorM'] = "大功";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-HW'] = "每學期欠交功課次數少於五次";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-PC'] = "每學期遲到次數少於三次";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-AD'] = "出席所有上課天";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-AI'] = "從未校服髮飾違規";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-RC'] = "閱讀中文圖書至少兩本，並完成閱書報告";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-RE'] = "閱讀英文圖書至少兩本，並完成閱書報告";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-EC'] = "參加經校方安排之相關活動並表現良好";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-TA'] = "參加經校方安排之相關活動並表現良好";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-PY'] = "校內體脂測量合格";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-PJ'] = "校內跑步考核合格";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-PS'] = "校內游泳考核合格";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-PW'] = "體育科評核表現良好";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-CA'] = "每學期參與校內基督教活動至少2項";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-TA'] = "參加經校方安排之相關培訓課程並表現良好";
$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-OC'] = "老師評定具值得欣賞之處";
$Lang['iPortfolio']['JinYing']['Report']['ClassName'] = "班別";
$Lang['iPortfolio']['JinYing']['Report']['ClassNumber'] = "班號";
$Lang['iPortfolio']['JinYing']['Report']['ClassTeacher'] = "班主任";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Award'] = "獎勵";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['BasicRequirements'] = "基本要求";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Item'] = "項目";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Performance'] = "表現";
$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['PerformanceAward'] = "表現/獎勵";
$Lang['iPortfolio']['JinYing']['Report']['DateOfIssue'] = "派發日期";
$Lang['iPortfolio']['JinYing']['Report']['Details'] = "學生在學生剪影計劃內各項目的表現詳述如下。";
$Lang['iPortfolio']['JinYing']['Report']['DetailsRemark'] = "（在具*之項目表現優異可獲記功獎勵。）";
$Lang['iPortfolio']['JinYing']['Report']['Index'][0] = "甲";
$Lang['iPortfolio']['JinYing']['Report']['Index'][1] = "乙";
$Lang['iPortfolio']['JinYing']['Report']['Index'][2] = "丙";
$Lang['iPortfolio']['JinYing']['Report']['Index'][3] = "丁";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-HW'] = "交齊家課 *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-PC'] = "守時 *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-AD'] = "勤到 *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-AI'] = "校服儀容整潔";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-RC'] = "閱讀圖書（中文）";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['BSQ-RE'] = "閱讀圖書（英文）";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-EC'] = "參加港外考察活動";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-TA'] = "參加培訓課程";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['EPW-PY'] = "具理想體質";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['EPW-PJ'] = "具良好體能（跑步）";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['EPW-PS'] = "具良好體能（游泳）";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['EPW-PW'] = "具良好整體體能";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['EPW-CA'] = "參加基督教活動";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm']['EPW-OC'] = "值得欣賞之處";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['EPW-TA'] = "參加培訓課程";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['OFS-SI'] = "參與校內服務 *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['OFS-SO'] = "參與校外服務 *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-EA'] = "積極參與課外活動";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-IS'] = "積極參與校內比賽";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-IC'] = "積極參與校外比賽 *";
$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-TP'] = "樂於參與才藝表演";
$Lang['iPortfolio']['JinYing']['Report']['JYR'] = "學生剪影報告";
$Lang['iPortfolio']['JinYing']['Report']['NoRecord'] = "無紀錄";
$Lang['iPortfolio']['JinYing']['Report']['NotApplicable'] = "不適用";
$Lang['iPortfolio']['JinYing']['Report']['Organizer'] = "主辦機構";
$Lang['iPortfolio']['JinYing']['Report']['Page']['PageNumSuffix'] = "第 ";
$Lang['iPortfolio']['JinYing']['Report']['Page']['TotPageNumPrefix'] = " 頁共 ";
$Lang['iPortfolio']['JinYing']['Report']['Page']['TotPageNumSuffix'] = " 頁";
$Lang['iPortfolio']['JinYing']['Report']['Parent'] = "家長/監護人";
$Lang['iPortfolio']['JinYing']['Report']['Principal'] = "校長";
$Lang['iPortfolio']['JinYing']['Report']['RegisterNumber'] = "註冊編號";
$Lang['iPortfolio']['JinYing']['Report']['Scope']['BSQ'] = "(%s) 提升個人素質";
$Lang['iPortfolio']['JinYing']['Report']['Scope']['EPW'] = "(%s) 身心靈健康";
$Lang['iPortfolio']['JinYing']['Report']['Scope']['OFS'] = "(%s) 服務精神";
$Lang['iPortfolio']['JinYing']['Report']['Scope']['BTP'] = "(%s) 發揮個人才華";
$Lang['iPortfolio']['JinYing']['Report']['SLP'] = "學生學習概覽";
$Lang['iPortfolio']['JinYing']['Report']['StudentName'] = "學生姓名";
$Lang['iPortfolio']['JinYing']['Report']['Title']['JYR'] = "%s至%s年度學生剪影報告";
$Lang['iPortfolio']['JinYing']['Report']['Title']['SLP'] = "%s至%s年度學生學習概覽";
$Lang['iPortfolio']['JinYing']['ReturnMessage']['MissingItem'] = "0|=|項目號碼不能留空";
$Lang['iPortfolio']['JinYing']['StatisticalAnalysis'] = "數據分析";
$Lang['iPortfolio']['JinYing']['StatisticsByClass'] = "%s至%s年度學生剪影計劃班別數據";
$Lang['iPortfolio']['JinYing']['StatisticsByForm'] = "%s至%s年度學生剪影計劃年級數據";
$Lang['iPortfolio']['JinYing']['StatisticsByItem'] = "%s至%s年度學生剪影計劃項目數據(%s)";
$Lang['iPortfolio']['JinYing']['StatisticsByItemAll'] = "%s至%s年度學生剪影計劃項目數據";
$Lang['iPortfolio']['JinYing']['StatisticsByScope'] = "%s至%s年度學生剪影計劃範疇數據(%s)";
$Lang['iPortfolio']['JinYing']['StatisticsByMethod'] = "顯示類型";
$Lang['iPortfolio']['JinYing']['Statistics']['ByClass'] = "班別";
$Lang['iPortfolio']['JinYing']['Statistics']['ByForm'] = "年級";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['AllItems'] = "所有項目 (只限匯出)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] = "獲記功";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['MeritExcludeTargetMet'] = "獲記功(不包括「達標」)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] = "沒有匯入資料";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoMeritWithParticipation'] = "不獲記功 (曾參與本項目活動)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] = "無參加本項目活動";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEA'] = "沒有參加課外活動";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEC'] = "沒有參加校外比賽";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Participation'] = "曾參與本項目活動 (不論記功與否)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['ParticipationWithoutPrize'] = "參與(無需賽前練習。無獲獎。)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithMerit'] = "獲獎項，並獲記功";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithoutMerit'] = "獲獎項，但不獲記功";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMet'] = "達標";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] = "達標(不包括「達標並表現良好」)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeMerit'] = "達標(不包括「獲記功」)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] = "達標並表現良好(不包括「達標」)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] = "未達標";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipation'] = "未達標(有參加本項目活動)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEA'] = "未達標(有參加課外活動)";
$Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEC'] = "未達標(有參加校外比賽)";
$Lang['iPortfolio']['JinYing']['Statistics']['Class'] = "班別";
$Lang['iPortfolio']['JinYing']['Statistics']['Count'] = "人數";
$Lang['iPortfolio']['JinYing']['Statistics']['Form'] = "年級";
$Lang['iPortfolio']['JinYing']['Statistics']['HighSecondary'] = "高中";
$Lang['iPortfolio']['JinYing']['Statistics']['JuniorSecondary'] = "初中";
$Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Gold'] = "達金章級";
$Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Silver'] = "達銀章級";
$Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Bronze']= "達銅章級";
$Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['None']= "不達獎章級";
$Lang['iPortfolio']['JinYing']['Statistics']['Percentage'] = "百分比";
$Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByReceive'] = "各級獲獎章人數百分比";
$Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByScope'] = "各範疇達獎章要求人數百分比";
$Lang['iPortfolio']['JinYing']['Statistics']['Tab']['ByItem'] = "各項目達要求人數百分比";
$Lang['iPortfolio']['JinYing']['Statistics']['Total'] = "總人數";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningGoldLessSilverGoodPoint'] = "金章的優點要求不能低於銀章";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningGoldLessSilverTargetMet'] = "金章的達標要求不能低於銀章";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningSelectItem'] = "請選擇項目";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningSelectScope'] = "請選擇範疇";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningSilverLessBronzeGoodPoint'] = "銀章的優點要求不能低於銅章";
$Lang['iPortfolio']['JinYing']['Statistics']['WarningSilverLessBronzeTargetMet'] = "銀章的達標要求不能低於銅章";
$Lang['iPortfolio']['JinYing']['Statistics']['WholeYear'] = "全年";

$Lang['iPortfolio']['LivingHomeopathy']['PersonalProfile'] = "個人履歷表";

$Lang['iPortfolio']['ChiuChunKG']['SBS']['printVersion'] = "列印版本";
$Lang['iPortfolio']['ChiuChunKG']['SBS']['reportTitle'] = "報告標題";
$Lang['iPortfolio']['ChiuChunKG']['SBS']['someFieldNotAns'] = "部份答案並未填寫，是否確定傳送？";
$Lang['iPortfolio']['ChiuChunKG']['SBS']['rowCount'] = "每頁行數";

$Lang['iPortfolio']['TWGHCZM']['StudentLearningProfile'] = '學生推薦書';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['0'] = 'General Academic Ability';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['1'] = 'Diligence';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['2'] = 'Initiative';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['3'] = 'Courtesy';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['4'] = 'Cooperation';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['5'] = 'Concern for others';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['6'] = 'Leadership';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['7'] = 'Punctuality';
$Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type']['8'] = 'Attendance';
$Lang['iPortfolio']['twghczm']['MaxECASize'] = '最大課外活動顯示紀錄';
$Lang['iPortfolio']['twghczm']['MaxPrizeSize'] = '最大成就顯示紀錄';

$Lang['iPortfolio']['DBSTranscript']['Title'] = '成績報告';
$Lang['iPortfolio']['DBSTranscript']['ReportType'] = '報告類型';
$Lang['iPortfolio']['DBSTranscript']['Transcript'] = '成績報告';
$Lang['iPortfolio']['DBSTranscript']['PredictedGrade'] = '預測等級';
$Lang['iPortfolio']['DBSTranscript']['DisplayForm'] = '顯示級別';
$Lang['iPortfolio']['DBSTranscript']['Curriculums'] = '課程';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Title'] = '課程設定';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Code'] = '代碼';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameEn'] = '名稱 (英文)';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['NameCh'] = '名稱 (中文)';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['SubjectLevel'] = '科目課程程度';
$Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Status'] = '狀況';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Title'] = '停用學生成績報告列印';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['IssuedBy'] = '發出老師';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Status'] = '狀況';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Locked'] = '停用';
$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Unlocked'] = '未停用';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Title'] = 'Pre-IB 科目等級計劃';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['AllSubject'] = '所有科目 (中文及數學科除外)';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['IBGrade'] = 'IB 等級';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Grade'] = '等級';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Math'] = 'IB G10 \'Pre-IB 數學科\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathStudies'] = '轉換自  G11 \'Mathematical Studies\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathSL'] = '轉換自  G11 \'Mathematics SL\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathHL'] = '轉換自  G11 \'Mathematics HL\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Chinese'] = 'IB G10 \'Pre-IB 中文科\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseLit'] = '轉換自 G11 \'Chinese Literature\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseLangLit'] = '轉換自 G11 \'Chinese Language and Literature\'';
$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseB'] = '轉換自 G11 \'Chinese B\'';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Title'] = '圖像管理';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['BackCover'] = '背頁';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Header'] = '頂部';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SchoolSeal'] = '校印';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Signature'] = '簽署欄';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Description'] = '簡介';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Image'] = '圖像';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SignatureName'] = '名稱';
$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SignatureDetails'] = '詳情';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Title'] = '預計等級';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Adjustment'] = '調整';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['MinGrade'] = '最低等級';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['MaxGrade'] = '最高等級';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ImportGrade'] = '匯入等級';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['OriginalGrade'] = '原始等級';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment1'] = '第一次修訂';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment2'] = '第二次修訂';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['FinalGrade'] = '最終調整等級';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['NoChange'] = '維持不變';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentPeriodSetting'] = '調整時段設定';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentStartDate'] = '調整時段開始時間';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentEndDate'] = '調整時段結束時間';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ValidData'] = '項合規格的資料 - 請確定匯入';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['InvalidData'] = '沒有合規格的資料';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['ImportedData'] = '項合規格的資料 - 已成功匯入';
$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['InvalidImport'] = '不合規格的資料';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['Title'] = '報告列印';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['TranscriptLeftSignature'] = '成績報告簽署欄 (左)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['TranscriptRightSignature'] = '成績報告簽署欄 (右)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['PredictedGradeLeftSignature'] = '預測等級報告簽署欄 (左)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['PredictedGradeRightSignature'] = '預測等級報告簽署欄 (右)';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SelectStudent'] = '選擇學生';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SearchByStudentName'] = '以姓名搜尋負責人';
$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SearchByStudentNameFormat'] = "輸入\"[姓名]\" (e.g. 陳) ";
$Lang['iPortfolio']['DBSTranscript']['StudentCurriculumsMapping']['Title'] = '學生課程配對';
$Lang['iPortfolio']['DBSTranscript']['SelectReportType'] = '報告類型';
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['InputWarning'] = "請輸入";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['SelectWarning'] = "請選擇";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['DuplicateWarning'] = "重複";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PeriodCannotOverlap'] = "兩個調整時段設定不能重疊。";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRange'] = "請輸入正確級別範圍。";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeIB'] = "(G9 - G12)";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeDBS'] = "(G7 - G12)";
$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeOthers'] = "(G7 - G13)";
$Lang['iPortfolio']['DBSTranscript']['RemarkArr']['GradeSaveAsDraft'] = "<span class=\"tabletextrequire\">^</span> 該調整等級被儲存為草稿";

$Lang['iPortfolio']['bps']['MaxRecordsSize'] = '記錄最大顯示數量';
###########################################################
if ($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile'] && is_file("$intranet_root/lang/iportfolio_custom/stpaulcoeduprischool_slp_lang.php"))
{
  include("$intranet_root/lang/iportfolio_custom/stpaulcoeduprischool_slp_lang.php");
}


if ($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'])
{
	include("$intranet_root/lang/iportfolio_custom/stpaulPAS_slp_lang_b5.php");
}
if (!$NoLangWordings && is_file("$intranet_root/templates/lang.$intranet_session_language.customized.php"))
{
  include("$intranet_root/templates/lang.$intranet_session_language.customized.php");
}
if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")||($sys_custom['project']['NCS'])){
	include($intranet_root."/ncs/lang.ncs.b5.php");
}
####################### This should be placed at the bottom
?>
