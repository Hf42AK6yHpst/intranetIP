<?php
# Editing by ivan
$eRC_Rubrics_ConfigArr['MaxLevel'] = (!isset($eRC_Rubrics_ConfigArr['MaxLevel']))? 0 : $eRC_Rubrics_ConfigArr['MaxLevel'];

$Lang['eRC_Rubrics']['GeneralArr']['Report'] = "Report";
$Lang['eRC_Rubrics']['GeneralArr']['SchoolYear'] = "School Year"; 
$Lang['eRC_Rubrics']['GeneralArr']['SchoolYear2Ch'] = "年度";
$Lang['eRC_Rubrics']['GeneralArr']['Form'] = "Form";
$Lang['eRC_Rubrics']['GeneralArr']['ClassName'] = "Class Name";
$Lang['eRC_Rubrics']['GeneralArr']['ClassNumber'] = "Class Number";
$Lang['eRC_Rubrics']['GeneralArr']['Gender'] = "Gender";
$Lang['eRC_Rubrics']['GeneralArr']['Class'] = "Class";
$Lang['eRC_Rubrics']['GeneralArr']['Subject'] = "Subject";
$Lang['eRC_Rubrics']['GeneralArr']['SubjectGroup'] = "Subject Group";
$Lang['eRC_Rubrics']['GeneralArr']['Code'] = "Code";
$Lang['eRC_Rubrics']['GeneralArr']['Name'] = "Name";
$Lang['eRC_Rubrics']['GeneralArr']['ApplicableForm'] = "Applicable Form";
$Lang['eRC_Rubrics']['GeneralArr']['NameEn'] = "Name (English)";
$Lang['eRC_Rubrics']['GeneralArr']['NameCh'] = "Name (Chinese)";
$Lang['eRC_Rubrics']['GeneralArr']['EnglishName'] = "Name (English)";
$Lang['eRC_Rubrics']['GeneralArr']['ChineseName'] = "Name (Chinese)";
$Lang['eRC_Rubrics']['GeneralArr']['Remarks'] = "Remarks";
$Lang['eRC_Rubrics']['GeneralArr']['AllClass'] = 'All Classes';
$Lang['eRC_Rubrics']['GeneralArr']['AllForm'] = 'All Forms';
$Lang['eRC_Rubrics']['GeneralArr']['AllReport'] = 'All Reports';
$Lang['eRC_Rubrics']['GeneralArr']['DownloadCSVFile'] = "Download CSV file template";
$Lang['eRC_Rubrics']['GeneralArr']['GenerateCSVFile'] = "Generate CSV file template";
$Lang['eRC_Rubrics']['GeneralArr']['LastModifiedDate'] = "Last Modified Date";
$Lang['eRC_Rubrics']['GeneralArr']['Mark'] = "Mark";
$Lang['eRC_Rubrics']['GeneralArr']['WholeForm'] = "Whole form";
$Lang['eRC_Rubrics']['GeneralArr']['NotAssessed'] = "N.A.";
$Lang['eRC_Rubrics']['GeneralArr']['ActiveAcademicYear'] = "Active Academic Year";
$Lang['eRC_Rubrics']['GeneralArr']['CurrentAcademicYear'] = "Current Academic Year";
$Lang['eRC_Rubrics']['GeneralArr']['NotClassified'] = "Students not classified";
$Lang['eRC_Rubrics']['GeneralArr']['SelectAll'] = "Select All";

$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['Report'] = "-- Select Report --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllReport'] = "-- All Report --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['Rubrics'] = "-- Select Rubrics --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllClass'] = "-- All Classes --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllForm'] = "-- All Forms --";
$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllSubjectGroup'] = "-- All Subject Groups --";

$Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr']['Desc'] = "Description";
$Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr']['Abbr'] = "Abbreviation";
$Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr']['ShortForm'] = "Short Form";

$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongWebSamsRegNo'] = "No such user.";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongUserLogin'] = "No such user.";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongClassNameNumber'] = "No such user.";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyClassNumber'] = "Empty class number.";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyClassName'] = "Empty class name.";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyStudentData'] = "Empty student data.";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongDataFormat'] = "Wrong Format";
$Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongCsvHeader'] = "Wrong csv header";

$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Code'] = "Code cannot be blank";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name'] = "Name cannot be blank";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code'] = "Code is in use already";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoTopicForSubject'] = "There is no Topic settings for this Subject. Please go to \"Settings > Curriculum Pool\" to update the settings.";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoThisTopicForSubject'] = "This Student does not study any Topics for this Subject. Please go to \"Settings > Student Learning Details\" to update the settings.";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoModuleInTerm'] = "There is no Module settings for this Term. Please go to \"Settings > Module\" to update the settings.";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoRubricsForSubject'] = "There is no Rubrics settings for this Subject. Please go to \"Settings > Subject Rubrics\" to update the settings.";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoLearningCategory'] = "There is no Learning Category settings. Please go to \"School Settings > Subject\" to update the settings.";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoSubject'] = "There is no Subject settings. Please go to \"School Settings > Subject\" to update the settings.";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoStudent'] = "There is no Student in this Subject Group.";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter'] = "Code must begin with letter.";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage'] = "Please select a Form Stage";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form'] = "Please select a Form";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class'] = "Please select a Class";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Student'] = "Please select a Student";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['StudentInfo'] = "Please select a Student information";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject'] = "Please select a Subject";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['SubjectGroup'] = "Please select a Subject Group";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Term'] = "Please select a Term";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['3Subject'] = "Please select at least 3 Subjects";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'] = "Please select a Module";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['TopicLevel'] = "Please select a Topic Level";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['LearningDetails'] = "Please select a Learning Details";
$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Date']['EndDateCannotEarlierThanStartDate'] = "The End Date cannot be earlier than the Start Date.";

$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['AlertSelectFile'] = "Please select a file first.";
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['FirefoxNotAllowPaste'] = 'The copy and paste function is disabled by the browser. Please enter "about:config" in the URL field and set "signed.applets.codebase_principal_support" as "true".';
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] = '';
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] .= "eReportCard Active Year: |||--eRC Active Year--|||";
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] .= '\n';
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] .= "Current Academic Year: |||--Current Academic Year--|||";
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'] .= '\n\n';
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearTeacher'] = "The eReportCard system is not using the data of the current academic year. Please contact eReportCard Administrator if you want to view and process the data of the current academic year.";
$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearAdmin'] = "The eReportCard system is not using the data of the current academic year. Please switch the eReportCard to the current academic year by going to \\\"Management > Data Handling > Data Transition\\\" if you want to view and process the data of the current academic year.";


### Left Menu [Start]
$Lang['eRC_Rubrics']['ManagementArr']['MenuTitle'] = "Management";
$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['MenuTitle'] = "Schedule";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle'] = "Marksheet";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'] = "Class Teacher Comment";
$Lang['eRC_Rubrics']['ManagementArr']['BehaviorAndAttitudeArr']['MenuTitle'] = "Behavior & Attitude";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle'] = "Extra-Curricular Activities and Services";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['MenuTitle'] = "Data Handling";

$Lang['eRC_Rubrics']['ReportsArr']['MenuTitle'] = "Reports";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MenuTitle'] = "Report Card Generation";
$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['MenuTitle'] = "No. of Subject Taught";
$Lang['eRC_Rubrics']['ReportsArr']['TopicTaughtReportArr']['MenuTitle'] = "No. of Topic Taught";
$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['MenuTitle'] = "Class Average Rubrics Count";
$Lang['eRC_Rubrics']['ReportsArr']['FormAverageRubricsCountArr']['MenuTitle'] = "Form Average Rubrics Count";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MenuTitle'] = "Grand Marksheet";
$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['MenuTitle'] = "Topic Comparison Report";

$Lang['eRC_Rubrics']['StatisticsArr']['MenuTitle'] = "Statistics";
$Lang['eRC_Rubrics']['StatisticsArr']['RubricsReportArr']['MenuTitle'] = "Rubrics Report";
$Lang['eRC_Rubrics']['StatisticsArr']['RadarDiagramArr']['MenuTitle'] = "Radar Diagram";
$Lang['eRC_Rubrics']['StatisticsArr']['ClassSubjectPerformanceArr']['MenuTitle'] = "Class Subject Performance";
$Lang['eRC_Rubrics']['StatisticsArr']['SubjectClassPerformanceArr']['MenuTitle'] = "Subject Class Performance";
$Lang['eRC_Rubrics']['StatisticsArr']['ClassTopicPerformanceArr']['MenuTitle'] = "Class Topic Performance";
$Lang['eRC_Rubrics']['StatisticsArr']['TopicClassPerformanceArr']['MenuTitle'] = "Topic Class Performance";


$Lang['eRC_Rubrics']['SettingsArr']['MenuTitle'] = "Settings";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'] = "Module";
$Lang['eRC_Rubrics']['SettingsArr']['ReportCardTemplateArr']['MenuTitle'] = "Report Card Template";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['MenuTitle'] = "Curriculum Pool";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['MenuTitle'] = "Student Learning Details";
$Lang['eRC_Rubrics']['SettingsArr']['BehaviorAndAttitudeArr']['MenuTitle'] = "Behavior & Attitude";
$Lang['eRC_Rubrics']['SettingsArr']['SubjectRubricsArr']['MenuTitle'] = "Subject Rubrics";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleSubjectArr']['MenuTitle'] = "Module Subject";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle'] = "Admin Group";
### Left Menu [End]


### Management > Schedule
$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['MarksheetSubmission'] = "Marksheet Submission";
$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['ApplySchedule'] = "Apply Schedule";
$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['ToolTipArr']['EditModuleSchedule'] = "Edit Module Schedule";

### Management > Marksheet
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['Title'] = "Learning Topic";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][2]['Title'] = "Learning Objective";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][3]['Title'] = "Learning Details";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['TitleEn'] = "Learning Topic";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][2]['TitleEn'] = "Learning Objective";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][3]['TitleEn'] = "Learning Details";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['TitleCh'] = "學習範疇";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][2]['TitleCh'] = "學習重點";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][3]['TitleCh'] = "學習細目";

for ($i=1; $i<=$eRC_Rubrics_ConfigArr['MaxLevel']; $i++)
{
	$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['Select'] = "Select ".$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['Title'];
	$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['All'] = "All ".$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['Title'];	
}

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['SelectLevel'] = "Select Level";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['CopyAndPasteInstruction'] = "For easier mark input, you may copy and paste the level from an excel file to this page. Firstly, copy the level from an excel file. Secondly, click the selection box which you may want to start paste with. Lastly, press 'Ctrl + V' to paste the level into the marksheet.";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ChooseFromCommentBank'] = "Choose From Comment Bank";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['SubjectGroupCode'] = "Subject Group Code";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['SubjectGroupName'] = "Subject Group Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['StudentUserLogin'] = "Student User Login";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['StudentName'] = "Student Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['TopicCode'] = "Learning Topic Code";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['TopicName'] = "Learning Topic Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['ObjectiveCode'] = "Learning Objective Code";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['ObjectiveName'] = "Learning Objective Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['DetailsCode'] = "Learning Details Code";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['DetailsName'] = "Learning Details Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['Level'] = "Rubrics level";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['Description'] = "Rubrics description";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportDataInstruction'] = "Original marksheet rubrics level and description of students in CSV will be replaced.";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportDataRemarks'] = "<u>Description of import:</u><br>
1. Please generate CSV file template first. Then, please edit the CSV template you downloaded.<br/>
2. Input marksheet rubrics level and description.";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['BackToMarksheetRevision'] = "Back to Marksheet";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportOtherMarksheetData'] = "Import Other Data";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ReturnMsgArr']['ImportOtherInfoSuccess'] = "Data imported successfully.";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ReturnMsgArr']['ImportOtherInfoFail'] = "Failed to import data.";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['jsConfirmMsg']['ChangesWillBeDiscard'] = "The changes you just made will be discarded, proceed?";

$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningTopicWarning'] = "Learning Topic Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningObjectiveWarning'] = "Learning Objective Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningDetailsWarning'] = "Learning Details Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoStudentWarning'] = "Student Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoSubjectGroupWarning'] = "Subject Group Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotStudyingSubject'] = "Student not studying this subject.";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotStudyLearningDetails'] = "Student not studying this learning details";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotInSubjectGroup'] = "Student not in this subject group";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['InvalidDetailsLevel'] = "Invalid rubrics level input";
$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['InvalidInputWarning'] = "Invalid input format";

### Management > Other Information
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['MenuTitle'] = "Other Information";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['ClassName']['En'] = "Class Name";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['ClassNumber']['En'] = "Class Number";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['UserLogin']['En'] = "User Login";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['WebSamsRegNo']['En'] = "WebSams Reg. No.";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['StudentName']['En'] = "Student Name".$Lang['General']['ImportArr']['Reference']['En'];
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['ClassName']['Ch'] = "(班別)";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['ClassNumber']['Ch'] = "(班號)";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['UserLogin']['Ch'] = "(內聯網帳號)";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['WebSamsRegNo']['Ch'] = "(WebSAMS 註冊號碼)";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader']['StudentName']['Ch'] = "(學生姓名)".$Lang['General']['ImportArr']['Reference']['Ch'];

$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['ClassName'] = array("F1A","","");
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['ClassNumber'] = array("1","","");
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['UserLogin'] = array("","student1login","");
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['WebSamsRegNo'] = array("","","#123456");
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample']['StudentName'] = array("Mary Chan","Peter Wong","Tom Li");

$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportDataInstruction'] = "If you want to update existing information, please export and update the original csv file before uploading to the system. Otherwise, the original information of selected class or form will be replaced.";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportOtherInfoSuccess'] = "Other info imported successfully.";
$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportOtherInfoFail'] = "Failed to import other info."; 

$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['jsConfirmMsg']['OriginalInformationReplace'] = "Other information of selected Class/Form was uploaded to selected report before. Original information will be replaced, proceed?";

### Management > Class Teacher Comment
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'] = "Student";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'] = "Comment";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['AddToStudentComment'] = "Add to Student Comment";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SelectedComment'] = "Selected Comment(s)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseReportCard'] = "Choose Report Card";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'] = "Choose Student";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['OnlyStudentInTheSelectedFormWillBeShown'] = "Only the student in the selected form will be shown for selection.";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['LastModifiedDate'] = "Last Modified Date";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['LastModifiedBy'] = "Last Modified By";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Confirmed'] = "Completed";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NotCompleted'] = "Not Completed";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChangeAllTo'] = "Change All to";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SubmissionStartDate'] = "Submission Start Date";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SubmissionEndDate'] = "Submission End Date";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Class'] = "Class";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Form'] = "Form";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Class'] = "Class";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Name'] = "Name";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['TeacherComment'] = "Teacher Comment";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Report'] = "Report";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ClassNo'] = "Class no.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Code'] = "Code";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ClassTeacherComment'] = "Class Teacher's Comment";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudentRemarks'] = "Only the student in the selected form will be shown for selection.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['AddCommentSuccess'] = "Added comment successfully.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['AddCommentFail'] =  "Failed to add comment .";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Language'] = "Language";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SelectComment'] = "Select Comment";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['File'] = "File";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ExportFormData'] = "Export Form Data";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ExportClassData'] = "Export Class Data";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentSuccess'] = "Comments imported successfully.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentFail'] = "Failed to import comments";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportDataInstruction'] = "If students exist in the import file, original comments of the students will be replaced.";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NumOfStudent'] = "No. of Students";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NumOfComment'] = "No. of Comments";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr']['AddCommentSuccess'] = "1|=|Add Comment Success.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr']['AddCommentFailed'] = "0|=|Add Comment Failed.";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedCommentWillBeLostIfSearch'] = "All selected comment option will be lost if you search comment. Are you sure you want to search the comment now?";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment'] = "Please select at least one comment.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedStudentWillBeRemovedIfChangedForm'] = "The selected student(s) will be removed if you change the Form now. Are you sure you want to change the Form?";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment'] = "Please select at least one comment.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedCommentWillBeLostIfSearch'] = "Selected comment will be lost, proceed?";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsConfirmArr']['SelectedStudentWillBeRemoved'] = "Selected student wil be removed, proceed?";

$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['ClassName']['En'] = "Class Name";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['ClassNumber']['En'] = "Class Number";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['UserLogin']['En'] = "User Login";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['WebSamsRegNo']['En'] = "WebSams Reg. No.";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['StudentName']['En'] = "Student Name".$Lang['General']['ImportArr']['Reference']['En'];
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['Comment']['En'] = "Comment".$Lang['General']['ImportArr']['Optional']['En'];
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['ClassName']['Ch'] = "(班別)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['ClassNumber']['Ch'] = "(班號)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['UserLogin']['Ch'] = "(內聯網帳號)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['WebSamsRegNo']['Ch'] = "(WebSAMS 註冊號碼)";
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['StudentName']['Ch'] = "(學生姓名)".$Lang['General']['ImportArr']['Reference']['Ch'];
$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']['Comment']['Ch'] = "(評語)".$Lang['General']['ImportArr']['Optional']['Ch'];

### Management > Extra-Curricular Activities and Services
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatCode'] = "Category Code";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatNameEn'] = "Category Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['SubCatCode'] = "Subcategory Code";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['SubCatNameEn'] = "Subcategory Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatItemCode'] = "Item Code";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatItemNameEn'] = "Item Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['Class'] = "Class Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['StudentNameEn'] = "Student Name (Eng) [Ref]";
//$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['StudentUserLogin'] = "Student UserLogin";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN']['ClassName'] = "Class Name";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN']['ClassNumber'] = "Class Number";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN']['UserLogin'] = "User Login";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN']['StudentName'] = "Student Name [Ref]";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5']['ClassName'] = "(班別)";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5']['ClassNumber'] = "(班號)";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5']['UserLogin'] = "(內聯網帳號)";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5']['StudentName'] = "(學生姓名) [參考用途]";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportDataInstruction'] = "Original extra-curricular activities and services of students in CSV will be replaced.";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportDataRemarks'] = "<u>Description of import:</u><br>
1. Please generate CSV template first. Then, please edit the CSV template you downloaded.<br/>
2. Input 'Y' if select extra-curricular activities and services for students.";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['BackToECAService'] = "Back to Extra-Curricular Activities and Services";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportOtherECAService'] = "Import Other Data";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ReturnMsgArr']['ImportSuccess'] = "Data imported successfully.";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ReturnMsgArr']['ImportFail'] = "Failed to import data.";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['jsConfirmMsg']['ChangesWillBeDiscard'] = "The changes you just made will be discarded, proceed?";

$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoECACategoryWarning'] = "Category Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoECASubCategoryWarning'] = "Subcategory Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoECAItemWarning'] = "Item Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoStudentWarning'] = "Student Not Exist";
$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['InvalidInputWarning'] = "Invalid input format";

### Management > Data Handling
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['TransitTo'] = "Transit to";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['NextYearTermNotComplete'] =  "The Term settings of the next Academic Year is incomplete.";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['NewAcademicYear'] = "New Academic Year";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['OtherAcademicYears'] = "Other Academic Years";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr'][] = "Please proceed this action before:";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr'][] = "- start using eReportCard for a new academic year, or,";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr'][] = "- restore eReportCard to use the data of previous academic years";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr'][] = "The data from the current active academic year will be preserved.";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['jsWarningArr']['ConfirmTransition'] = "Proceed to do selected data transition?";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['UpdateArr']['Success'] = "1|=|Data Transited.";
$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['UpdateArr'] ['Failed']= "0|=|Data Transition Failed.";


### Reports > ReportCard Generation
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportCardDisplay'] = "Report Card Display";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Title'] = "Title";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Row'] = "Row";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['AddMore'] = "Add More";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Signature'] = "Signature";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['DataDisplay'] = "Data Display";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['HeaderData'] = "Header Data";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['NumOfHeaderDataEachRow'] = "No. of Header Data on each Row";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectName'] = "Subject Name";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectGroupName'] = "Subject Group Name";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['DateRangeDisplay'] = "From <!--StartYear-->-<!--StartMonth-->-<!--StartDate--> to <!--EndYear-->-<!--EndMonth-->-<!--EndDate-->";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CurriculumData'] = "Topic Level";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CurriculumWhichStudentNeedNotStudy'] = "Topic which Student has not studied";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['LastTopicLevelIsAlwaysDisplay'] = "\"<!--TopicLevelName-->\" will always be displayed.";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportCardTemplateSettings'] = "Report Card Settings Preset";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsName'] = "Settings Name";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SchoolName'] = "School Name";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CoverPage'] = "Cover Page";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ShowCoverPage'] = "Show Cover Page";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['StudentPhotoFrame'] = "Student Photo Frame";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarksheetRemarks'] = "Marksheet Remarks";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ApplicableToModuleReportOnly'] = "Applicable to Module Report only.";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportType'] = "Report Type";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SelectFrom'] = "Select From";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['GeneralReport'] = "General Report";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['TherapyReport'] = "Therapy Report";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['LearningReport'] = "Learning Report";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['WarningArr']['ModuleReportMustSelectLastTopicLevel'] = "\"<!--TopicLevelName-->\" must be selected for Moudule Report.";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['WarningArr']['ModuleMarkConsolidatedReportMustSelectLastTopicLevel'] = "\"<!--TopicLevelName-->\" must be selected for Consolidated Report which displays Module-based Mark.";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['jsWarningArr']['DeleteSettings'] = "Are you sure you want to delete this Settings?";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['Save'] = "Save Settings";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['SaveAs'] = "Save Settings As";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['Add'] = "Add Settings";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarkDisplayOptionArr']['ModuleBased'] = "Module-based Mark";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarkDisplayOptionArr']['Consolidated_%'] = "Consolidated Percentage";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarkDisplayOptionArr']['Consolidated_Fraction'] = "Consolidated Fraction";

$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReturnMsgArr']['SaveSettingsSuccess'] = "1|=|Settings Saved.";
$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReturnMsgArr']['SaveSettingsFailed'] = "0|=|Settings Saving Failed."; 

$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['TaughtItem'] = "Detail Taught";
$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['TopicItem'] = "Learning Detail";

$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['Getting'] = "getting";
$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['AverageNumber'] = "Average number of ";
$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['level'] = "level";

$Lang['eRC_Rubrics']['ReportsArr']['ClassRubricsCount']['ReportTitle'] = "Self-completed Average";

# GrandMarksheet
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowOption'] = "Show Options";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['HideOption'] = "Hide Options";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayFormat'] = "Display Format";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['StudentDisplayOption'] = "Student Display Option";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MarksheetDisplayOption'] = "Marksheet Display Option";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowFormSummary'] = "Show in one table for the classes in the same form";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowClassSummary'] = "Show in separate pages for each class";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayNameInOneLine'] = "Display Name in one line";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DataDisplay'] = "Data Display";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['LearningDetailDisplay'] = "Learning Detail Display";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowLearningTopicTitle'] = "Learning Topic Title";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowLearningObjectiveTitle'] = "Learning Objective Title";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayAll'] = "Display All";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['Custom'] = "Custom";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['HTML'] = "HTML";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['CSV'] = "CSV";
$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ViewFormat'] = "View Format";

$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Report1'] = "Report 1";
$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Report2'] = "Report 2";
$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Module'] = "Module";

### Statistics
$Lang['eRC_Rubrics']['StatisticArr']['RubricsReportArr']['Performance'] = "於各科之表現";

$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr']['SubjectSettings'] = "Subject";
$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr']['StudentSettings'] = "Student";
$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr']['ModuleSettings'] = "Module";
$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr']['ReportSettings'] = "Report Card Display";

$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['ApplyPageBreak'] = "Apply page-break for each student";


### Settings > Curriculum Pool
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['Title'] = "Learning Topic";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][2]['Title'] = "Learning Objective";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][3]['Title'] = "Learning Details";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['TitleEn'] = "Learning Topic";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][2]['TitleEn'] = "Learning Objective";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][3]['TitleEn'] = "Learning Details";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['TitleCh'] = "學習範疇";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][2]['TitleCh'] = "學習重點";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][3]['TitleCh'] = "學習細目";
for ($i=1; $i<=$eRC_Rubrics_ConfigArr['MaxLevel']; $i++)
{
	$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Settings'] = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Title']." Settings";
	$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Add'] = "Add ".$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Title'];
	$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['All'] = "All ".$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Title'];
	
	if ($i==1)
		$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Remarks'] = "Learning Target";
	else
		$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Remarks'] = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['Title']." Remarks";
}

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['En'][] = "Subject Code";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['En'][] = "Subject Name (Eng)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['En'][] = "Subject Name (Chi)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['Ch'][] = "科目代號";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['Ch'][] = "科目名稱 (英文)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn']['Ch'][] = "科目代號 (中文)";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CodeEn'] = "Code";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CodeCh'] = "代號";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Eng)En'] = "Name (Eng)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Chi)En'] = "Name (Chi)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Eng)Ch'] = "名稱 (英文)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Chi)Ch'] = "名稱 (中文)";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['RemarksEn'] = "Remarks";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['RemarksCh'] = "備註";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ApplicableFormEn'] = "Applicable Form";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ApplicableFormCh'] = "適用級別";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['AllSubjects'] = "All Subjects";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ThisSubject'] = "This Subject";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['BackToCurriculumPool'] = "Back to Curriculum Pool";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportOtherCurriculum'] = "Import Other Curriculum";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportCurriculumSuccess'] = "Curriculum imported successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportCurriculumFailed'] = "Curriculum import failed.";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CurriculumnFormMapping'] = "Curriculumn Form Mapping";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicNotBelongsToThisSubject'] = "This <!--TopicLevel--> is not belongs to this Subject";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['WrongLinkageOfExistingTopic'] = "This <!--TopicLevel--> has linked to another <!--PreviousTopicLevel--> already";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicNotBelongsToThisLevel'] = "<!--TopicCode--> is not a <!--TopicLevel-->";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicDuplicatedIfAddTopic'] = "Code will be duplicated if adding this <!--TopicLevel-->";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['NoSuchForm'] = "The Form does not exist";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['NoApplicableForm'] = "No Applicable Form";
$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['FormNotLinkedToAnyStage'] = "The Form does not linked to any Stages";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['jsWarningArr']['Delete'] = "All curriculums belong to this item will be deleted at the same time. Are you sure you want to deleted this item?";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['WarningArr']['ApplicableForm'] = "Please select at least one Applicable Form.";

$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ToolTipArr']['EditApplicableForm'] = "Edit Applicable Form";


### Settings > Student Learning Details
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['RecordOverwriteIfSaveForSG'] = "All records of the Student in this Subject Group will be overwritten if the Subject Group Settings is saved.";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['BackgroundInfoInstructionArr'][0] = "White background represents <b>ALL</b> students in the Subject Group are studying that Topic in that Module.";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['BackgroundInfoInstructionArr'][1] = "Lite Grey background represents <b>SOME</b> of the students in the Subject Group are studying that Topic in that Module.";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['BackgroundInfoInstructionArr'][2] = "Dark Grey background represents <b>NONE</b> of the students in the Subject Group are studying that Topic in that Module.";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['jsWarningArr']['RecordOverwriteIfSaveForSG'] = "All records of the Student in this Subject Group will be overwritten if the Subject Group Settings is saved. Are you sure you want to save the settings?";
$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['AllModule'] = "All Modules";


### Setting > Module
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Warning'] = "Warning";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EditModuleWarning'] = "Editing or deleting module may affect existing marks.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Term'] = "Term";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['WholeYear'] = "Whole Year";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Module'] = "Module";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Period'] = "Period";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['StartDate'] = "Start";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EndDate'] = "End";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ModuleCode'] = "Code";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Name'] = "Name";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ModuleName'] = "Module Name";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ChineseName'] = "Chinese Name";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EnglishName'] = "English Name";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ApplyDate'] = "Apply Date";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ThickBoxTitleArr']['AddModule'] = "Add Module";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ThickBoxTitleArr']['EditModule'] = "Edit Module";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['ModuleCodeNotAvailible'] = "Module code is not availible.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['DateRangeOverlap'] = "Date range overlapped with another module.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EndDateLaterThanStartDate'] = "End Date cannot be earlier than the start date.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['StartDateOutOfTerm'] = "Start date is out of term.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EndDateOutOfTerm'] = "End date is out of term.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleNameEn'] = "Please input english module name.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleNameCh'] = "Please input chinese module name.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleCode'] = "Please input module code.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['StartDate'] = "Please input start date.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['EndDate'] = "Please input end date.";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsConfirmArr']['RemoveModule'] = "Are you sure you want to remove this module?";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['InsertSuccess'] = "1|=|Inserted module successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['InsertFail'] = "0|=|Failed to insert module.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['UpdateSuccess'] = "1|=|Updated module successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['UpdateFail'] = "0|=|Failed to update module.";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['DeleteSuccess'] = "1|=|Deleted module successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['DeleteFail'] = "0|=|Failed to delete module.";

$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['AddModule'] = "Add Module";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['EditModule'] = "Edit Module";
$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['DeleteModule'] = "Delete Module";

###  Settings > Module Subject
$Lang['eRC_Rubrics']['SettingsArr']['ModuleSubjectArr']['RemarksArr'][] = "The Subject display of the Subject Selection in other pages will be determined by the below settings.";

###  Settings > Subject Rubrics
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['MenuTitle'] = "Subject Rubrics";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['LearningCategory'] = "Learning Category";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Subject'] = "Subject";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsScheme'] = "Rubrics Scheme";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['NoLearningCategory'] = "No Learning Category";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsLevelDescription'] = "Rubrics level and description";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsCode'] = "Rubrics Code";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsName'] = "Rubrics Name";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ChineseName'] = "Chinese Name";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['EnglishName'] = "English Name";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Lowest'] = "(Lowest)";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Highest'] = "(Highest)";
//$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['SelectRubrics'] = "Select Rubrics";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ThickBoxTitleArr']['EditRubrics'] = "Edit Rubrics";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ThickBoxTitleArr']['AddRubrics'] = "Add Rubrics";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['RubricsCodeNotAvailible'] = "Rubrics code is not availible.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsEn'] = "Please input english rubrics name.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsCh'] = "Please input chinese rubrics name.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsCode'] = "Please input rubrics code.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsLevelDesc'] = "Please input rubrics level and description.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsScheme'] = "Please select rubrics for all subjects."; 
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['RubricsItemUsed'] = "This rubrics item has been used in marksheet.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['SelectRubricsToDelete'] = "Please select a rubrics to delete.";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['EditRubricsWithUsedItem'] = "Some rubrics items were used in marksheet. Editing this rubrics may affect existing marks, proceed?";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['DeleteRubrics'] = "Are you sure you want to delete this rubrics?";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['DeleteUsedRubrics'] = "This rubrics was used in marksheet. Editing this rubrics may affect existing marks, proceed?";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['InsertSuccess'] = "1|=|Inserted rubrics items successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['InsertFail'] = "0|=|Failed to insert rubrics items.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['UpdateSuccess'] = "1|=|Updated rubrics items successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['UpdateFail'] = "0|=|Failed to update rubrics items.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['DeleteSuccess'] = "1|=|Deleted rubrics successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['DeleteFail'] = "0|=|Failed to delete rubrics.";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['SaveSuccess'] = "1|=|Rubrics saved successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['SaveFail'] = "0|=|Failed to save rubrics.";

$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['EditRubrics'] = "Edit Rubrics";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['DeleteRubrics'] = "Delete Rubrics";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['MoveRubricsLevel'] = "Move Rubrics Level";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['AddRubricsLevel'] = "Add Rubrics Level";
$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['DeleteRubricsLevel'] = "Delete Rubrics Level";

###  Settings > Template settings
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportCardTemplate'] = "Report Card Template";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ChineseName'] = "Chinese Name";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['EnglishName'] = "English Name";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportType'] = "Report Type";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportTitle'] = "Report Title";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ModuleReport'] = "Module Report";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['LastModifiedDate'] = "Last Modified Date";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ConsolidatedReport'] = "Consolidated Report";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['SelectAll'] = "Select All";

$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['EmptyArr']['ReportTitleEn'] = "Please input english rubrics name.";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['EmptyArr']['ReportTitleCh'] = "Please input chinese rubrics name.";
$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['SelectModule'] = "Please select at least one module.";

###  Settings > ECA settings
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ThickBoxTitleArr']['AddEditCategory'] = "Add/Edit Category";
//$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ThickBoxTitleArr']['EditModule'] = "Edit Module";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['MenuTitle'] = "Extra-Curricular Activities and Services";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddCategory'] = "Add Category";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['EditCategory'] = "Edit Category";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteCategory'] = "Delete Category";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddSubCategory'] = "Add Subcategory";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['EditSubCategory'] = "Edit Subcategory";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteSubCategory'] = "Delete Subcategory";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddItem'] = "Add Item";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['EditItem'] = "Edit Item";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteItem'] = "Delete Item";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Category'] = "Category";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Subcategory'] = "Subcategory";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Item'] = "Item";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Code'] = "Code";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Name'] = "Name";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['NumOfItem'] = "No. of items";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['CategoryName'] = "Category Name";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['SubCategoryName'] = "Subcategory Name";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ItemName'] = "Item Name";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ChineseName'] = "Chinese Name";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['EnglishName'] = "English Name";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryCode'] = "Empty or duplicate category code.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryNameEn'] = "Empty english name.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryNameCh'] = "Empty chinese name.";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryCode'] = "Please input category code.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryNameEn'] = "Please input english name.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryNameCh'] = "Please input chinese name.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['ECACodeNotAvailible'] = "Category code is not availible.";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemCode'] = "Please input item code.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemNameEn'] = "Please input english name.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemNameCh'] = "Please input chinese name.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['ItemCodeNotAvailible'] = "Item code is not availible.";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertCategorySuccess'] = "1|=|Inserted eca category successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertCategoryFail'] = "0|=|Failed to insert eca category.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateCategorySuccess'] = "1|=|Updated eca category successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateCategoryFail'] = "0|=|Failed to update eca category.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['ECACodeNotAvailible'] = "0|=|ECA Code is not availible.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteCategorySuccess'] = "1|=|Deleted eca category successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteCategoryFail'] = "0|=|Failed to delete eca category.";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertSubCategorySuccess'] = "1|=|Inserted subcategory successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertSubCategoryFail'] = "0|=|Failed to insert subcategory.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateSubCategorySuccess'] = "1|=|Updated subcategory successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateSubCategoryFail'] = "0|=|Failed to update subcategory.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['SubCodeNotAvailible'] = "0|=|Subcategory code is not availible.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteSubCategorySuccess'] = "1|=|Deleted subcategory successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteSubCategoryFail'] = "0|=|Failed to delete subcategory.";
 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertItemSuccess'] = "1|=|Inserted item successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertItemFail'] = "0|=|Failed to insert item.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateItemSuccess'] = "1|=|Updated item successfully."; 
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateItemFail'] = "0|=|Failed to update item.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['SubCodeNotAvailible'] = "0|=|Item code is not available.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteItemSuccess'] = "1|=|Deleted item successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteItemFail'] = "0|=|Failed to delete item.";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteCategoryWithSubCategory'] = "This cateogry and all subcategories and subitems will be deleted, proceed?";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteCategory'] = "Are you sure you want to delete this cateogry?";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteSubCategoryWithItem'] = "This subcateogry and all subitems will be deleted, proceed?";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteSubCategory'] = "Are you sure you want to delete this subcateogry?";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteItem'] = "Are you sure you want to delete this subcateogry?";

$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['SelectOption']['AllCategory'] = "- All Category -";
$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['SelectOption']['AllSubCategory'] = "- All Subcategory -";

# Setting > Comment Bank 
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['MenuTitle'] = "Comment Bank";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ClassTeacherComment']  = "Class Teacher's Comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['SubjectDetailsComment']  = "Subject Details Comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Topic']  = "Topic";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['WarningTopic']  = "Please select Topic.";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Code']  = "Code";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Comment']  = "Comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Subject']  = "Subject";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Category']  = "Category";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['New'] = "New";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Edit'] = "Edit";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportCommentSuccess'] = "comment(s) were imported successfully.";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportCommentFail'] = "Failed to import comments";

$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentCode'] = "Empty comment code";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentEn'] = "Empty english comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentCh'] = "Empty chinese comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['CommentCodeNotAvailible'] = "This comment code is not available.";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['WrongCodeFormat'] = "Comment code must be start with letter.";

$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCode']['En'] = "Comment Code";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentEn']['En'] = "English Comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCh']['En'] = "Chinese Comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectCode']['En'] = "Subject Code ".$Lang['General']['ImportArr']['Reference']['En'];
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectName']['En'] = "Subject Name ".$Lang['General']['ImportArr']['Reference']['En'];
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCode']['Ch'] = "(評語代號)";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentEn']['Ch'] = "(英文評語)";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCh']['Ch'] = "(中文評語)";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectCode']['Ch'] = "(學科代號) ".$Lang['General']['ImportArr']['Reference']['Ch'];
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectName']['Ch'] = "(學科名稱) ".$Lang['General']['ImportArr']['Reference']['Ch'];
for ($i=1; $i<=$eRC_Rubrics_ConfigArr['MaxLevel']; $i++) {
	$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['En'] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['TitleEn']." Code";
	$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicName'][$i]['En'] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['TitleEn']." Name ".$Lang['General']['ImportArr']['Reference']['En'];	
	
	$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['Ch'] = "(".$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['TitleCh']."代號)";
	$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicName'][$i]['Ch'] = "(".$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$i]['TitleCh']."名稱) ".$Lang['General']['ImportArr']['Reference']['En'];
	
	if ($i != $eRC_Rubrics_ConfigArr['MaxLevel']) {
		$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['En'] .= " ".$Lang['General']['ImportArr']['Reference']['En'];
		$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['Ch'] .= " ".$Lang['General']['ImportArr']['Reference']['Ch'];
	}
}

$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentCode'] = "Empty comment code";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentEn'] = "Empty english comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentCh'] = "Empty chinese comment";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['WrongCodeFormat'] = "Comment code must be start with letter.";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['TopicCode'] = "Empty <!--TopicLevelName--> code";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['WrongTopicCode'] = "No such <!--TopicLevelName--> code";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['CommentEnExceededWordLimit'] = "English Comment exceeded <!--MaxWordCount--> words";
$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['CommentChExceededWordLimit'] = "Chinese Comment exceeded <!--MaxWordCount--> words";

$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportDataInstruction'] = "If comment code exists in the system, original comment of the comment code will be replaced.";


# Settings > Admin Group
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NumOfMember'] = "No. of Members";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NewAdminGroupStepArr'][1] = "Info & Access Right";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NewAdminGroupStepArr'][2] = "Select Member(s)";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupCode'] = "Admin Group Code";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupName'] = "Admin Group Name";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupList'] = "Admin Group List";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MemberList'] = "Member List";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AddMember'] = "Add Member";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupInfo'] = "Admin Group Info";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AccessRight'] = "Access Right";

$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['AddSuccess'] = "1|=|Admin Group Added.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['AddFailed'] = "0|=|Admin Group Addition Failed.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['EditSuccess'] = "1|=|Admin Group Edited.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['EditFailed'] = "0|=|Admin Group Edition Failed.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['DeleteSuccess'] = "1|=|Admin Group(s) Deleted.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['DeleteFailed'] = "0|=|Admin Group(s) Deletion Failed.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['AddMemberSuccess'] = "1|=|Admin Group Member Added.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['AddMemberFailed'] = "0|=|Admin Group Member Addition Failed.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['DeleteMemberSuccess'] = "1|=|Admin Group Member Deleted.";
$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage']['DeleteMemberFailed'] = "0|=|Admin Group Member Deletion Failed.";

$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['WarningArr']['SelectionContainsMember'] = "The highlighted options contain member(s) of this Admin Group already";


### Wordings which can be customized in the future
$Lang['eRC_Rubrics']['Customized']['General']['Subject']['En'] = "Subject";
$Lang['eRC_Rubrics']['Customized']['General']['Subject']['Ch'] = "科目";
$Lang['eRC_Rubrics']['Customized']['General']['Result']['En'] = "Result";
$Lang['eRC_Rubrics']['Customized']['General']['Result']['Ch'] = "成績";
$Lang['eRC_Rubrics']['Customized']['General']['ClassTeacherComment']['En'] = "Comment";
$Lang['eRC_Rubrics']['Customized']['General']['ClassTeacherComment']['Ch'] = "評語";
$Lang['eRC_Rubrics']['Customized']['General']['TopicDetailsRemarks']['En'] = "Remarks";
$Lang['eRC_Rubrics']['Customized']['General']['TopicDetailsRemarks']['Ch'] = "細目註釋";
$Lang['eRC_Rubrics']['Customized']['General']['Remarks']['En'] = "Remarks";
$Lang['eRC_Rubrics']['Customized']['General']['Remarks']['Ch'] = "備註";
$Lang['eRC_Rubrics']['Customized']['General']['ThisTerm']['En'] = "This Term";
$Lang['eRC_Rubrics']['Customized']['General']['ThisTerm']['Ch'] = "本學期";
$Lang['eRC_Rubrics']['Customized']['General']['Day']['En'] = "Day";
$Lang['eRC_Rubrics']['Customized']['General']['Day']['Ch'] = "天";
$Lang['eRC_Rubrics']['Customized']['General']['StudentPerformance']['En'] = "Student Performance";
$Lang['eRC_Rubrics']['Customized']['General']['StudentPerformance']['Ch'] = "學生表現";
$Lang['eRC_Rubrics']['Customized']['General']['LearningPerformance']['En'] = "Learning Performance";
$Lang['eRC_Rubrics']['Customized']['General']['LearningPerformance']['Ch'] = "學習表現";
$Lang['eRC_Rubrics']['Customized']['General']['LearningContent']['En'] = "Learning Content";
$Lang['eRC_Rubrics']['Customized']['General']['LearningContent']['Ch'] = "學習內容";
$Lang['eRC_Rubrics']['Customized']['General']['StudentPerformanceRatio']['En'] = "Student Performance Ratio";
$Lang['eRC_Rubrics']['Customized']['General']['StudentPerformanceRatio']['Ch'] = "學生表現比例";

$Lang['eRC_Rubrics']['Customized']['TopicLevel'][1]['En'] = "Learning Topic";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][1]['Ch'] = "學習範疇";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][2]['En'] = "Learning Objective";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][2]['Ch'] = "學習重點";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][3]['En'] = "Learning Details";
$Lang['eRC_Rubrics']['Customized']['TopicLevel'][3]['Ch'] = "學習細目";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][1]['En'] = "Therapy Topic";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][1]['Ch'] = "治療範疇";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][2]['En'] = "Therapy Objective";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][2]['Ch'] = "治療重點";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][3]['En'] = "Therapy Details";
$Lang['eRC_Rubrics']['Customized']['TherapyTopicLevel'][3]['Ch'] = "治療內容";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][1]['En'] = "Learning Topic";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][1]['Ch'] = "學習範疇";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][2]['En'] = "Learning Objective";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][2]['Ch'] = "學習重點";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][3]['En'] = "Learning Details";
$Lang['eRC_Rubrics']['Customized']['LearningReportLevel'][3]['Ch'] = "學習內容";

$Lang['eRC_Rubrics']['Customized']['Signature']['Principal']['En'] = "Principal Signature";
$Lang['eRC_Rubrics']['Customized']['Signature']['Principal']['Ch'] = "校長簽署";
$Lang['eRC_Rubrics']['Customized']['Signature']['ClassTeacher']['En'] = "Class Teacher Signature";
$Lang['eRC_Rubrics']['Customized']['Signature']['ClassTeacher']['Ch'] = "班主任簽署";
$Lang['eRC_Rubrics']['Customized']['Signature']['LinguisticsTeacher']['En'] = "Linguistics Teacher in charge";
$Lang['eRC_Rubrics']['Customized']['Signature']['LinguisticsTeacher']['Ch'] = "負責言語治療師";
$Lang['eRC_Rubrics']['Customized']['Signature']['TeacherInCharge']['En'] = "Teacher in charge";
$Lang['eRC_Rubrics']['Customized']['Signature']['TeacherInCharge']['Ch'] = "負責教師";
$Lang['eRC_Rubrics']['Customized']['Signature']['Parent']['En'] = "Parent Signature";
$Lang['eRC_Rubrics']['Customized']['Signature']['Parent']['Ch'] = "家長簽署";
$Lang['eRC_Rubrics']['Customized']['Signature']['Date']['En'] = "Date of Signature";
$Lang['eRC_Rubrics']['Customized']['Signature']['Date']['Ch'] = "簽署日期";

$Lang['eRC_Rubrics']['Customized']['HeaderData']['StudentName']['En'] = "Student Name";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['StudentName']['Ch'] = "學生姓名";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ClassName']['En'] = "Class";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ClassName']['Ch'] = "班別";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['FormName']['En'] = "Form";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['FormName']['Ch'] = "班級";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ModuleName']['En'] = "Module Name";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ModuleName']['Ch'] = "單元名稱";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ModuleDateRange']['En'] = "Date";
$Lang['eRC_Rubrics']['Customized']['HeaderData']['ModuleDateRange']['Ch'] = "日期";

$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['summary']['En'] = "Summary Info";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['summary']['Ch'] = "總結資料";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['award']['En'] = "Awards Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['award']['Ch'] = "本學期獎項紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['merit']['En'] = "Merits & Demerits Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['merit']['Ch'] = "獎懲紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['eca']['En'] = "ECA Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['eca']['Ch'] = "課外活動紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['remark']['En'] = "Remark";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['remark']['Ch'] = "備註";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['interschool']['En'] = "Inter-school Competitions Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['interschool']['Ch'] = "聯校比賽紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['schoolservice']['En'] = "School Service Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['schoolservice']['Ch'] = "學校服務紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['attendance']['En'] = "Attendance Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['attendance']['Ch'] = "考勤紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['demerit']['En'] = "Punishment Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['demerit']['Ch'] = "懲罰紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['OLE']['En'] = "OLE Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['OLE']['Ch'] = "其他學習經歷紀錄";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['post']['En'] = "Post Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['post']['Ch'] = "職責";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['others']['En'] = "Others Record";
$Lang['eRC_Rubrics']['Customized']['OtherInfoCategory']['others']['Ch'] = "其他紀錄";

$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Total Number of School Days']['En'] = "Present";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Total Number of School Days']['Ch'] = "出席";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Days Absent']['En'] = "Days Absent";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Days Absent']['Ch'] = "缺席";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Time Late']['En'] = "Times Late";
$Lang['eRC_Rubrics']['Customized']['OtherInfoItem']['Time Late']['Ch'] = "遲到";

$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Addresss'] = "校址"; 
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Phone'][0] = "電話: 26700800(校務處/教員室)";
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Phone'][1] = "26569697(社工室/醫療室)";
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Phone'][2] = "26700824(校長室)";
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['WebSite'] = "網址";
$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Email'] = "電郵";

$Lang['eRC_Rubrics']['Template']['RemarksArr']['A'] = "【A】— 做到全部/ 自行完成";
$Lang['eRC_Rubrics']['Template']['RemarksArr']['B'] = "【B】— 做到大部份/ 在口頭提示下完成";
$Lang['eRC_Rubrics']['Template']['RemarksArr']['C'] = "【C】— 做到一半/ 口頭加動作下提示完全";
$Lang['eRC_Rubrics']['Template']['RemarksArr']['D'] = "【D】— 做到少許/ 教師協助下完成";
?>