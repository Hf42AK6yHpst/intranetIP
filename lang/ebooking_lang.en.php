<?php
// using : 

/********************** Change Log ***********************/
# Date:  2019-09-18[Tommy]
#               added $Lang['eBooking']['Settings']['SystemProperty']['BookingApproveNotification']
/******************* End Of Change Log *******************/

$Lang['eBooking']['BookingPeriod']['FieldTitle']['UsingDefaultPeriod'] = "Using default booking period";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['UsingFacilityPeriod'][1] = "Using specific room booking period";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['UsingFacilityPeriod'][2] = "Using specific item booking period";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['EditFacilityPeriod'][1] = "Edit room booking period";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['EditFacilityPeriod'][2] = "Edit item booking period";
$Lang['eBooking']['BookingPeriod']['Btn']['CopyFrom'] = "Copy booking period from others";
$Lang['eBooking']['BookingPeriod']['Btn']['CopyTo'] = "Copy booking Period to others";
$Lang['eBooking']['BookingPeriod']['Btn']['ClearFacilityPeriodSetting'][1] = "Remove booking period setting of this room";
$Lang['eBooking']['BookingPeriod']['Btn']['ClearFacilityPeriodSetting'][2] = "Remove booking period setting of this item";
$Lang['eBooking']['BookingPeriod']['ConfirmMessage']['RemoveFacilityBookingPeriod'][1] = "All the booking period settings of this room will be removed, this room will use the default period settings. Proceed?";
$Lang['eBooking']['BookingPeriod']['ConfirmMessage']['RemoveFacilityBookingPeriod'][2] = "All the booking period settings of this item will be removed, this room will use the default period settings. Proceed?";
$Lang['eBooking']['BookingPeriod']['Weekday'][0] = "Sun";
$Lang['eBooking']['BookingPeriod']['Weekday'][1] = "Mon";
$Lang['eBooking']['BookingPeriod']['Weekday'][2] = "Tue";
$Lang['eBooking']['BookingPeriod']['Weekday'][3] = "Wed";
$Lang['eBooking']['BookingPeriod']['Weekday'][4] = "Thu";
$Lang['eBooking']['BookingPeriod']['Weekday'][5] = "Fri";
$Lang['eBooking']['BookingPeriod']['Weekday'][6] = "Sat";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][0] = "Sun";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][1] = "Mon";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][2] = "Tue";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][3] = "Wed";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][4] = "Thu";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][5] = "Fri";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][6] = "Sat";

$Lang['eBooking']['BookingPeriod']['FieldTitle']['Dates'] = "Date(s)";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['Options'] = "Options";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['Confirmation'] = "Confirmation";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['CopyResult'] = "Copy Result";
$Lang['eBooking']['BookingPeriod']['RecordIndication']['GeneralTimetable'] = "General Timetable";
$Lang['eBooking']['BookingPeriod']['RecordIndication']['SpecialTimetable'] = "Special Timetable";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['TimetableCovering'] = "Timetable Covering";

$Lang['eBooking']['General']['FieldTitle']['Facility'][1] = "Room";
$Lang['eBooking']['General']['FieldTitle']['Facility'][2] = "Item";
$Lang['eBooking']['General']['FieldTitle']['RoomOrItem'] = "Room/Item";
$Lang['eBooking']['General']['FieldTitle']['Category'] = 'Category';
$Lang['eBooking']['General']['FieldTitle']['Category2'] = 'Sub-Cateogry';
//$Lang['eBooking']['General']['FieldTitle']['Item']['Item'] = 'Item';
$Lang['eBooking']['General']['FieldTitle']['PeriodType'] = "Period Type";
$Lang['eBooking']['General']['FieldTitle']['Overwrite'] = "Overwrite";
$Lang['eBooking']['General']['FieldTitle']['ApplyDoorAccess'] = "Apply Door Access";

$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['WarningMessage'] = "The specific booking period settings of selected rooms / items will be overwritten.";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['ShowOption'] = "Show Options";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['HideOption'] = "Hide Options";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['jsWarningMessage']['PleaseSelectFacility'] = "Please select at least one room / item.";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['Yes'] = "Yes";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['BookingPeriodCopySuccess'] = "%s booking period(s) were copied successfully.";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['BookingPeriodCopyFail'] = "Failed to copy %s booking period(s).";


$Lang['eBooking']['Btn']['PrintAvailableBookingPeriod'] = "Print available booking period";
$Lang['eBooking']['Btn']['ViewAvailableBookingPeriod'] = "View available booking period";

$Lang['eBooking']['eService']['ReturnMsg']['EditBookingSuccess'] = "1|=|Booking Updated";
$Lang['eBooking']['eService']['ReturnMsg']['EditBookingFailed'] = "0|=|Failed to update Booking";

$Lang['eBooking']['ImportItemFromInventory'] = "Import > items from eInventory";
$Lang['eBooking']['ImportItem']= "Import > items";
$Lang['eBooking']['jsConfirmMsg']['ImportItemFromInventory'] = "Are you sure to import new item from eInventory?";

$Lang['eBooking']['Settings']['GeneralPermission']['ImportFromIventorySuccess'] = "%s new items have been imported from eInventory successfully.";
$Lang['eBooking']['Settings']['GeneralPermission']['ImportFromIventoryFail'] = "Failed to import new items from eInventory.";
$Lang['eBooking']['Settings']['GeneralPermission']['ImportSummary'] = "Import Summary";
$Lang['eBooking']['Settings']['GeneralPermission']['ImportResult'] = "Import Result";
$Lang['eBooking']['Settings']['GeneralPermission']['AllInventoryItemsHaveBeenImported'] = "All inventory items have been imported to eBooking.";
$Lang['eBooking']['Settings']['GeneralPermission']['NumberOfItemWillBeImported'] = "%s new items will be imported from eInventory.";

$Lang['eBooking']['Settings']['FieldTitle']['DoorAccess'] = "Door Access";
$Lang['eBooking']['Settings']['SystemProperty']['MenuTitle'] = "System Property";
$Lang['eBooking']['Settings']['SystemProperty']['AllowBooking'] = "Allow general users booking";
$Lang['eBooking']['Settings']['SystemProperty']['Allow'] = "Allow";
$Lang['eBooking']['Settings']['SystemProperty']['Suspend'] = "Suspend";
$Lang['eBooking']['Settings']['SystemProperty']['SuspendUntil'] = "Suspend until %s";
$Lang['eBooking']['Settings']['SystemProperty']['EnableBookingMethod'] = "Enabled Booking Method";
$Lang['eBooking']['Settings']['SystemProperty']['DefaultBookingMethod'] = "Default Booking Method";


$Lang['eBooking']['Settings']['SystemProperty']['EnableBookingType'] = "Enabled Booking Type";
$Lang['eBooking']['Settings']['SystemProperty']['DefaultBookingType'] = "Default Booking Type";


$Lang['eBooking']['jsWarningArr']['PleaseSelectBookingMethod'] = "Please select at least one booking method.";
$Lang['eBooking']['jsWarningArr']['PleaseSelectBookingType'] = "Please select at least one booking type.";
$Lang['eBooking']['jsWarningArr']['PleaseSelectPeriodicBookingMethod']="Please select at least one periodic booking method.";
$Lang['eBooking']['jsWarningArr']['InvalidSearchPeriod'] = "Invalid search period.";
$Lang['eBooking']['jsWarningArr']['PleaseInputRemarks'] = "Please input remarks.";





$Lang['eBooking']['BookingSuspendByAdmin'] = "The booking function is temporarily suspended by administrator.";

$Lang['eBooking']['General']['SpecialTimetable'] = "Special Timetable";
$Lang['eBooking']['General']['DaySuspension'] = "Day Suspension";
$Lang['eBooking']['General']['NewDaySuspension'] = "New Day Suspension";
$Lang['eBooking']['General']['EditDaySuspension'] = "Edit Day Suspension";
$Lang['eBooking']['General']['NormalDay'] = "Regular Booking";
$Lang['eBooking']['General']['SpecialDay'] = "Special Booking";
$Lang['eBooking']['General']['NonBookingDay'] = "Suspension";
$Lang['eBooking']['General']['SetForSpecialTimetable'] = "Set for special timetable";

$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Created']['Success'] = "1|=|Available booking period created successfully.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Created']['Fail'] = "0|=|Failed to create available booking period.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Edit']['Success'] = "1|=|Available booking period edited successfully.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Edit']['Fail'] = "0|=|Failed to edit available booking period.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Deleted']['Success'] = "1|=|Available booking period deleted successfully.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Deleted']['Fail'] = "0|=|Failed to delete available booking period.";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['BookingPeriod']['Assign'] = "Only ONE Booking Period can be assigned to any day. There is already a Booking Period occupying your selected Period, either fully or partially. Do you want to assign new Booking Period to the overlapping day(s)?";

$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Created']['Success'] = "1|=|Suspension period created successfully.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Created']['Fail'] = "0|=|Failed to create suspension period.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Edit']['Success'] = "1|=|Suspension period edited successfully.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Edit']['Fail'] = "0|=|Failed to edit suspension period";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Deleted']['Success'] = "1|=|Suspension period deleted successfully.";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Deleted']['Fail'] = "0|=|Failed to delete suspension period.";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DaySuspension']['Assign'] = "All or part of your selected date have been suspended already. Do you want to assign new suspension period to the overlapping day(s)?";

$Lang['eBooking']['BookingPeriod']['FieldTitle']['ExplainDefaultPeriod'] = "The default booking period settings apply to all other rooms/items except those using specific room/item booking period.";

$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Reason'] = "Reject Reason";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RejectBooking'] = "Reject Booking";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['EditRejectReason'] = "Edit Reject Reason";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Waiting'] = "Waiting For Approval";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookingRemark'] = "Booking Remark";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['CheckOutRemark'] = "Check Out Remark";

$Lang['eBooking']['Button']['FieldTitle']['Borrow'] = "Borrow";
$Lang['eBooking']['Button']['FieldTitle']['Return'] = "Return";
$Lang['eBooking']['Button']['FieldTitle']['Search'] = "Search";

$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['History'] = "History";
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutBorrow'] = "Check Out / Return";
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['EditRemark'] = "Edit Remarks";
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutReturnTime'] = "Check Out / Return Time";


$Lang['eBooking']['CheckInOutStatusArr'][0] = "Waiting";
$Lang['eBooking']['CheckInOutStatusArr'][1] = "Checked-in/Borrowed";
$Lang['eBooking']['CheckInOutStatusArr'][2] = "Checked-out/Returned";
$Lang['eBooking']['SelectStatus'] = "Select Status";

$Lang['eBooking']['Management']['FieldTitle']['BookingType'] = "Booking Type";
$Lang['eBooking']['Management']['FieldTitle']['Single'] = "Single";
$Lang['eBooking']['Management']['FieldTitle']['Repetitive'] = "Periodic";

$Lang['eBooking']['WeekDay'] = "Week day";

$Lang['eBooking']['eService']['FieldTitle']['WeekDay']="Week Day";
$Lang['eBooking']['eService']['FieldTitle']['Cycle']="Cycle";
$Lang['eBooking']['eService']['FieldTitle']['SpecificDateRange']="Specific Date Range";

$Lang['eBooking']['jsWarningMsg']['OutOfDateRange'] = "Selected date is out of available booking date range. \\nAvailable date range: ";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectWeekDate'] = "Please select week days.";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectCycleDate'] = "Please select cycle days.";

$Lang['eBooking']['NoCycleSetting'] = 'No cycle day set';

$Lang['eBooking']['NoLimit'] = "No Limit";
$Lang['eBooking']['CreateiCalEvent'] = "Create a new event in \"iCalendar\"?";
$Lang['eBooking']['OccupiedBy'] = "Occupied By";
$Lang['eBooking']['ChangeUser'] = "Change User";

$Lang['eBooking']['IfItemRejected'] = "If any one of the items is not available, please select";
$Lang['eBooking']['CancelRelatedBooking'] = "cancel the room booking on that date.";
$Lang['eBooking']['KeepRelatedBooking'] = "remain the room booking on that date.";
$Lang['eBooking']['ItemNotAvailableInSomeDay'] = "This item is not available in some or all of the dates selected.";
$Lang['eBooking']['Detail'] = "Detail";

$Lang['eBooking']['NAReasonArr']['OutOfAvailablePeriod'] = "Out of available period";
$Lang['eBooking']['NAReasonArr']['Booked'] = "Booked";
$Lang['eBooking']['NAReasonArr']['Available'] = "Available";

$Lang['eBooking']['NoValidDate'] = "No valid dates, please select again.";

$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['CustomGroup'] = "Custom group";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['TargetUser'] = "Target user";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['PleaseSelectTargetUser'] = "Please select at lease one user.";

$Lang['eBooking']['Lesson'] = "Lesson";

$Lang['eBooking']['InstructionArr']['DisplayGeneralTimetableOnly'] = "The following table displays the general timetable only. If you would like to book facilities according to the sessions of special timetable, please kindly press \"Back\" and then select \"Single\". ";

$Lang['eBooking']['PrintCustomReport'] = "Print custom report";
$Lang['eBooking']['ExportCustomReport'] = "Export custom report";

$Lang['eBooking']['General']['Export']['FieldTitle']['BookedBy'] = "Booked By";
$Lang['eBooking']['General']['Export']['FieldTitle']['Date'] = "Date";
$Lang['eBooking']['General']['Export']['FieldTitle']['Time'] = "Time";
$Lang['eBooking']['General']['Export']['FieldTitle']['Day'] = "Day";
$Lang['eBooking']['General']['Export']['FieldTitle']['Venue'] = "Venue";
$Lang['eBooking']['General']['Export']['FieldTitle']['ActivityName'] = "Activity Name";
$Lang['eBooking']['General']['Export']['FieldTitle']['PIC'] = "PIC";
$Lang['eBooking']['General']['Export']['FieldTitle']['Attandance'] = "Attandance";
$Lang['eBooking']['General']['Export']['FieldTitle']['Details'] = "Details";
$Lang['eBooking']['General']['ViewDetails'] = "View Details";

$Lang['eBooking']['Settings']['SystemProperty']['RoomNameDisplay'] = "Room name display";
$Lang['eBooking']['Settings']['SystemProperty']['Building'] = "Building";
$Lang['eBooking']['Settings']['SystemProperty']['Floor'] = "Floor";
$Lang['eBooking']['Settings']['SystemProperty']['Room'] = "Room";
$Lang['eBooking']['Settings']['SystemProperty']['BookingNotification'] = "Booking Result Email Notification";
$Lang['eBooking']['Settings']['SystemProperty']['DefaultApplyDoorAccess'] = "Default Apply Door Access";
$Lang['eBooking']['Settings']['SystemProperty']['BookingFormUrl'] = "Booking Form URL";
$Lang['eBooking']['Settings']['SystemProperty']['BookingForm'] = "Booking Form";
$Lang['eBooking']['Settings']['SystemProperty']['DownloadBookingForm'] = "Download Booking Form";
$Lang['eBooking']['Settings']['SystemProperty']['EventInICalendar'] = "Default iCalendar Event Setting";
$Lang['eBooking']['Settings']['SystemProperty']['AllowBookingDamagedItem'] = "Allow to book damaged item";
$Lang['eBooking']['Settings']['SystemProperty']['AllowBookingRepairingItem'] = "Allow to book repairing item";
$Lang['eBooking']['Settings']['SystemProperty']['ReceiveEmailNotificationFromeInventory'] = "Receive email notification from eInventory (for admin only)";
$Lang['eBooking']['Settings']['SystemProperty']['ReceiveCancelBookingEmailNotification'] = " Receive cancel booking email notification (for admin only)";
$Lang['eBooking']['Settings']['SystemProperty']['EnablePeriodicBookingMethod']="Enabled Periodic Booking Method";
$Lang['eBooking']['Settings']['SystemProperty']['DefaultPeriodicBookingMethod']="Default Periodic Booking Method";
$Lang['eBooking']['Settings']['SystemProperty']['ApprovalNotification'] = "Receive Email Notification (for approval group)";
$Lang['eBooking']['Settings']['SystemProperty']['BookingApproveNotification'] = "Booking Approve Email Notification (for follow-up group)";

$Lang['eBooking']['Settings']['BookingRequest']['JSConfirm']['TimeOverLapped']="The time of the selected requests are overlapped, would you still like to approve the bookings?";
$Lang['eBooking']['Settings']['BookingRequest']['JSConfirm']['AlreadyBooked']="The selected item(s) / room(s) are booked, would you still like to approve the bookings?";


$Lang['eBooking']['Instruction']['WarnDeleteRule'] = "Removal of user booking rules may affect existing setting in Booking Properties.";
$Lang['eBooking']['Instruction']['PleaseDownloadTheRoomBookingForm'] = "Please download the room booking form";

$Lang['eBooking']['Event'] = "Event";
$Lang['eBooking']['SupportEnterOrScanBarCode'] = "support enter or scan barcode";
$Lang['eBooking']['Damaged'] = "Damaged";
$Lang['eBooking']['Repairing'] = "Repairing";
$Lang['eBooking']['WrittenOff'] = "Written-off";
$Lang['eBooking']['RoomIsDeleted'] = "The room is deleted";
$Lang['eBooking']['ItemIsDeleted'] = "The item is deleted";

$Lang['eBooking']['eService']['DoorAccessappliedRemind'] = "This room has applied door access setting, please remember to bring your smart card.";
$Lang['eBooking']['eService']['CancelBookingNotification'] = "Cancel Booking Notification";
$Lang['eBooking']['eService']['PleaseNoteThatBelowBookingIsBeingCancelled'] = "Please note that below booking is being cancelled";

$Lang['eBooking']['Period'] = "Period";
$Lang['eBooking']['BookingUser'] = "Booking User";
$Lang['eBooking']['BookingStatus'] = "Booking Status";
$Lang['eBooking']['ReserveBookingAdminOverwriteWarning'] = "The following bookings will be overwritten";
$Lang['eBooking']['ReserveBookingAdminOverwriteConfirm'] = "Are you sure you want to overwrite those bookings?";
$Lang['eBooking']['MakePendingBookingAlready'] = "You have pending booking record in this timeslot already.";
$Lang['eBooking']['PendingBooking'] = "Pending Booking";
$Lang['eBooking']['PeriodicTimetableBookingRemarks'] = "For \"Periodic Booking\" with \"Specific Period\", date range must be within the same timezone.";

$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['emailTitle'] = "eBooking - Follow-up Work";
$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['dearSir/Madam'] = "Dear Sir / Madam,";
$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['PleaseFollowItem'] = "Please follow-up the booking below";
$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['WillBeUsedBy'] = "Will be used by";
$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['BookedBy'] = "Booked by";
$Lang['eBooking']['eMailContentAry']['ManageWorkAry']['PleaseManageItem'] = "Please approve the booking below";
$Lang['eBooking']['eMailContentAry']['ManageWorkAry']['Remarks'] = "You can approve the booking record through 'eAdmin> Resources Management> eBooking> Management> Booking application'.";
$Lang['eBooking']['eMailContentAry']['reservedBy'] = "reserved by <--Name-->";

$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Resource Category ID";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Subcategory ID";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Code";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Name (English)";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Name (Chinese)";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Description (English)";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Description (Chinese)";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Location ID";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Allow Booking";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Booking Day Beforehand";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Approval Group ID";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "Follow-up Group ID";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "User permision rule ID";

$Lang['eBooking']['Settings']['Items']['Import']['Update']['DataColumn'] = array_merge(array("Existing Code"),$Lang['eBooking']['Settings']['Items']['Import']['DataColumn']);
$Lang['eBooking']['Settings']['Items']['Import']['error']['missing']="is missing";
$Lang['eBooking']['Settings']['Items']['Import']['error']['notFound']="is not found";
$Lang['eBooking']['Settings']['Items']['Import']['error']['notMatch']="do not match";
$Lang['eBooking']['Settings']['Items']['Import']['error']['isExist']="is already exist";
$Lang['eBooking']['Settings']['Items']['Import']['error']['invalid']="is invalid";
$Lang['eBooking']['Settings']['Items']['Import']['error']['unknown']="has error due to unknown reason";

?>