<?php
if ($sys_custom['LivingHomeopathy']) {
	// Editing by 
	$Lang['Header']['HomeTitle'] = "Living Homeopathy Platform @ eClass";
	
	$Lang["PageTitle"] = "Living Homeopathy Platform @ eClass";
	$Lang['Header']['Menu']['SchoolCalendar'] = "校曆表";
	$Lang['SysMgr']['SchoolCalendar']['ModuleTitle'] = "校曆表";
	$Lang['Header']['Menu']['SchoolSettings'] = "管理";
	$Lang['Header']['Menu']['StaffAccount'] = "用戶管理";
	$Lang['Header']['Menu']['eEnrolment'] = "課程";
	
	$Lang['Header']['Menu']['schoolNews'] = "校園最新消息";
	$Lang['SysMgr']['SchoolNews']['SchoolNews'] = "校園最新消息";
	
	$Lang['Footer']['PowerBy'] = "Powered by ";
	
	$Lang['LivingHomeopathy'] = array();
	$Lang['LivingHomeopathy']['Organization'] = "組織";
	$Lang['LivingHomeopathy']['Company'] = "公司";
	$Lang['LivingHomeopathy']['Division'] = "分區";
	$Lang['LivingHomeopathy']['Department'] = "部門";
	$Lang['LivingHomeopathy']['Chinese'] = "繁";
	$Lang['LivingHomeopathy']['English'] = "English";
	$Lang['LivingHomeopathy']['Code'] = "代碼";
	$Lang['LivingHomeopathy']['Description'] = "描述";
	$Lang['LivingHomeopathy']['CompanyName'] = "公司名稱";
	$Lang['LivingHomeopathy']['DivisionName'] = "分區名稱";
	$Lang['LivingHomeopathy']['DepartmentName'] = "部門名稱";
	$Lang['LivingHomeopathy']['RelatedCompany'] = "所屬公司";
	$Lang['LivingHomeopathy']['RelatedDivision'] = "所屬分區";
	$Lang['LivingHomeopathy']['DepartmentStaff'] = "部門職員";
	$Lang['LivingHomeopathy']['NoOfStaff'] = "員工數目";
	$Lang['LivingHomeopathy']['StaffName'] = "員工名稱";
	$Lang['LivingHomeopathy']['ViewStaffList'] = "查看員工列表";
	
	$Lang['LivingHomeopathy']['JoinDate'] = "入職日期";
	$Lang['LivingHomeopathy']['TerminatedDate'] = "離職日期";
	$Lang['LivingHomeopathy']['EffectiveDate'] = "生效日期";
	
	$Lang['LivingHomeopathy']["Login"]["Login"] = "登入";
	$Lang['LivingHomeopathy']["Login"]["LoginTitle"] = "Employee Mobile App Platform";
	$Lang['LivingHomeopathy']["Login"]["LoginID"] = "登入名稱";
	$Lang['LivingHomeopathy']["Login"]["LoginID_alert"] = "請輸入登人名秼";
	$Lang['LivingHomeopathy']["Login"]["LoginID_err"] = "登入名稱/密碼無效";
	$Lang['LivingHomeopathy']["Login"]["Password"] = "密碼";
	$Lang['LivingHomeopathy']["Login"]["Password_alert"] = "請輸入密碼";
	$Lang['LivingHomeopathy']["Login"]["Password_err"]= "密碼錯誤";
	$Lang['LivingHomeopathy']['Login']['ForgotPassword'] = "忘記密碼?";
	
	$Lang['LivingHomeopathy']["Login"]["ForgotPasswordMsg"] = '1. 請輸入登入名稱，重啟密碼程序將寄送至閣下備用電郵。\n2. 如無法取得密碼，請聯絡所屬機構查詢。';
	$Lang['LivingHomeopathy']['Warnings']['DuplicatedName'] = "重複名稱。";
	$Lang['LivingHomeopathy']['Warnings']['DuplicatedCode'] = "重複代碼。";
	$Lang['LivingHomeopathy']['Warnings']['norecord'] = "暫時仍未有任何紀錄";
	
	$Lang['Portal']['Tag']['eHomework'] = "網上家課表";
	$Lang['Portal']['AlleHomework'] = "網上家課表";
	$Lang['Portal']["DueOn"] = "限期";
	$Lang['Portal']["ToMark"] = "已標記";
	$Lang['Portal']['Tag']['eNotice'] = "電子通告系統";
	$Lang['Portal']['AlleNotice'] = "電子通告系統";
	$Lang['Portal']['AllSchoolNews'] = "校園最新消息";
	$Lang['Portal']['AllCourses'] = "課程";
}
?>