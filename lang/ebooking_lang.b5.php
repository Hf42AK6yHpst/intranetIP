<?php
// Using: 

/********************** Change Log ***********************/
# Date:  2019-09-18[Tommy]
#               added $Lang['eBooking']['Settings']['SystemProperty']['BookingApproveNotification']
/******************* End Of Change Log *******************/

$Lang['eBooking']['BookingPeriod']['FieldTitle']['UsingDefaultPeriod'] = "預設預訂時段使用中";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['UsingFacilityPeriod'][1] = "房間特定預訂時段使用中";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['UsingFacilityPeriod'][2] = "物品特定預訂時段使用中";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['EditFacilityPeriod'][1] = "設定房間預訂時段";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['EditFacilityPeriod'][2] = "設定物品預訂時段";
$Lang['eBooking']['BookingPeriod']['Btn']['CopyFrom'] = "從其他設施複製預訂時段";
$Lang['eBooking']['BookingPeriod']['Btn']['CopyTo'] = "複製預訂時段至其他設施";
$Lang['eBooking']['BookingPeriod']['Btn']['ClearFacilityPeriodSetting'][1] = "清除此房間的預訂時段設定";
$Lang['eBooking']['BookingPeriod']['Btn']['ClearFacilityPeriodSetting'][2] = "清除此物品的預訂時段設定";
$Lang['eBooking']['BookingPeriod']['ConfirmMessage']['RemoveFacilityBookingPeriod'][1] = "所有此房間的預訂時段設定將被清除，改為使用預設預訂時段，繼續？";
$Lang['eBooking']['BookingPeriod']['ConfirmMessage']['RemoveFacilityBookingPeriod'][2] = "所有此物品的預訂時段設定將被清除，改為使用預設預訂時段，繼續？";
$Lang['eBooking']['BookingPeriod']['Weekday'][0] = "星期日";
$Lang['eBooking']['BookingPeriod']['Weekday'][1] = "星期一";
$Lang['eBooking']['BookingPeriod']['Weekday'][2] = "星期二";
$Lang['eBooking']['BookingPeriod']['Weekday'][3] = "星期三";
$Lang['eBooking']['BookingPeriod']['Weekday'][4] = "星期四";
$Lang['eBooking']['BookingPeriod']['Weekday'][5] = "星期五";
$Lang['eBooking']['BookingPeriod']['Weekday'][6] = "星期六";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][0] = "日";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][1] = "一";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][2] = "二";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][3] = "三";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][4] = "四";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][5] = "五";
$Lang['eBooking']['BookingPeriod']['ShortWeekday'][6] = "六";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['Dates'] = "日期";

$Lang['eBooking']['BookingPeriod']['FieldTitle']['Options'] = "選項";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['Confirmation'] = "確認";
$Lang['eBooking']['BookingPeriod']['FieldTitle']['CopyResult'] = "複製結果";
$Lang['eBooking']['BookingPeriod']['RecordIndication']['GeneralTimetable'] = "一般時間表";
$Lang['eBooking']['BookingPeriod']['RecordIndication']['SpecialTimetable'] = "特別時間表";

$Lang['eBooking']['BookingPeriod']['FieldTitle']['TimetableCovering'] = "時間表覆蓋的日期";

$Lang['eBooking']['General']['FieldTitle']['Facility'][1] = "房間";
$Lang['eBooking']['General']['FieldTitle']['Facility'][2] = "物品";
$Lang['eBooking']['General']['FieldTitle']['RoomOrItem'] = "房間/物品";
$Lang['eBooking']['General']['FieldTitle']['Category'] = "類別";
$Lang['eBooking']['General']['FieldTitle']['Category2'] = "子類別";
//$Lang['eBooking']['General']['FieldTitle']['Item'] = "物品";
$Lang['eBooking']['General']['FieldTitle']['PeriodType'] = "預訂時段類別";
$Lang['eBooking']['General']['FieldTitle']['Overwrite'] = "覆寫";
$Lang['eBooking']['General']['FieldTitle']['ApplyDoorAccess'] = "使用門禁設施";

$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['WarningMessage'] = "所選的房間/物品的特定預訂時段設定將會被覆蓋。";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['ShowOption'] = "顯示選項";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['HideOption'] = "隱藏選項";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['jsWarningMessage']['PleaseSelectFacility'] = "請選擇至少一個房間/物品。";

$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['Yes'] = "是";

$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['BookingPeriodCopySuccess'] = "%s 個預訂時段設定複製成功。";
$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['BookingPeriodCopyFail'] = "%s 個預訂時段設定複製失敗。";

$Lang['eBooking']['Btn']['PrintAvailableBookingPeriod'] = "列印可用的預訂時段";
$Lang['eBooking']['Btn']['ViewAvailableBookingPeriod'] = "檢視可用的預訂時段";

$Lang['eBooking']['eService']['ReturnMsg']['EditBookingSuccess'] = "1|=|修改預訂成功";
$Lang['eBooking']['eService']['ReturnMsg']['EditBookingFailed'] = "0|=|修改預訂失敗";

$Lang['eBooking']['ImportItemFromInventory'] = "資產管理行政系統匯入新的物品";
$Lang['eBooking']['ImportItem']= "物品";
$Lang['eBooking']['jsConfirmMsg']['ImportItemFromInventory'] = "確定要從資產管理行政系統匯入新的物品？";

$Lang['eBooking']['Settings']['GeneralPermission']['ImportFromIventorySuccess'] = "成功從資產管理行政系統匯入%s件新物品。";
$Lang['eBooking']['Settings']['GeneralPermission']['ImportFromIventoryFail'] = "從資產管理行政系統匯入新的物品失敗。";
$Lang['eBooking']['Settings']['GeneralPermission']['ImportSummary'] = "匯入概覽";
$Lang['eBooking']['Settings']['GeneralPermission']['ImportResult'] = "匯入結果";
$Lang['eBooking']['Settings']['GeneralPermission']['AllInventoryItemsHaveBeenImported'] = "所有資產管理行政系統的物品已被匯入到電子資源預訂系統。";
$Lang['eBooking']['Settings']['GeneralPermission']['NumberOfItemWillBeImported'] = "將從資產管理行政系統匯入%s件新物品。";

$Lang['eBooking']['Settings']['FieldTitle']['DoorAccess'] = "門禁設定";
$Lang['eBooking']['Settings']['SystemProperty']['MenuTitle'] = "系統特性";
$Lang['eBooking']['Settings']['SystemProperty']['AllowBooking'] = "允許一般使用者進行預訂";
$Lang['eBooking']['Settings']['SystemProperty']['Allow'] = "允許";
$Lang['eBooking']['Settings']['SystemProperty']['Suspend'] = "暫停";
$Lang['eBooking']['Settings']['SystemProperty']['SuspendUntil'] = "暫停直至%s為止";

$Lang['eBooking']['Settings']['SystemProperty']['EnableBookingType'] = "啟用的預訂種類";
$Lang['eBooking']['Settings']['SystemProperty']['DefaultBookingType'] = "預設預訂種類";

$Lang['eBooking']['Settings']['SystemProperty']['EnableBookingMethod'] = "啟用的預訂方法";
$Lang['eBooking']['Settings']['SystemProperty']['DefaultBookingMethod'] = "預設預訂方法";
$Lang['eBooking']['Settings']['SystemProperty']['EnablePeriodicBookingMethod']="啟用的重複性預定類型";
$Lang['eBooking']['Settings']['SystemProperty']['DefaultPeriodicBookingMethod']="預設重複性預定類型";

$Lang['eBooking']['jsWarningArr']['PleaseSelectBookingType'] = "請選擇最少一個預訂種類。";
$Lang['eBooking']['jsWarningArr']['PleaseSelectBookingMethod'] = "請選擇最少一個預訂方法。";
$Lang['eBooking']['jsWarningArr']['PleaseSelectPeriodicBookingMethod']="請選擇最少一個重複性預定類型";	
$Lang['eBooking']['jsWarningArr']['InvalidSearchPeriod'] = "查詢時段不正確";
$Lang['eBooking']['jsWarningArr']['PleaseInputRemarks'] = "請填寫備註";


$Lang['eBooking']['BookingSuspendByAdmin'] = "預訂功能被系統管理員暫停。";

$Lang['eBooking']['General']['SpecialTimetable'] = "特別時間表";
$Lang['eBooking']['General']['DaySuspension'] = "暫停使用日期";
$Lang['eBooking']['General']['NewDaySuspension'] = "新增暫停使用日期";
$Lang['eBooking']['General']['EditDaySuspension'] = "修改暫停使用日期";
$Lang['eBooking']['General']['NormalDay'] = "常規預訂時間";
$Lang['eBooking']['General']['SpecialDay'] = "特別預訂時間";
$Lang['eBooking']['General']['NonBookingDay'] = "暫停預訂";
$Lang['eBooking']['General']['SetForSpecialTimetable'] = "設定特別時間表預訂時間";


$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Created']['Success'] = "1|=|新增可預訂時期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Created']['Fail'] = "0|=|新增可預訂時期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Edit']['Success'] = "1|=|修改可預訂時期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Edit']['Fail'] = "0|=|修改可預訂時期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Deleted']['Success'] = "1|=|刪除可預訂時期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Deleted']['Fail'] = "0|=|刪除可預訂時期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['BookingPeriod']['Assign'] = "系統只允許你指派一個預訂時間到任何一日。所選時段部份或全部日子，已獲指派預定時段。你是否確定要指派新的預定時段？";

$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Created']['Success'] = "1|=|新增暫停使用日期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Created']['Fail'] = "0|=|新增暫停使用日期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Edit']['Success'] = "1|=|修改暫停使用日期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Edit']['Fail'] = "0|=|修改暫停使用日期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Deleted']['Success'] = "1|=|刪除暫停使用日期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Deleted']['Fail'] = "0|=|刪除暫停使用日期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DaySuspension']['Assign'] = "所選日期的部份或全部日子，已暫停使用。你是否確定要指派新的暫停使用日期？";

$Lang['eBooking']['BookingPeriod']['FieldTitle']['ExplainDefaultPeriod'] = "預設時段適用於所有房間/物品，但不包括正在使用特定預訂時段的房間/物品。";

$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Reason'] = "拒絕原因";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RejectBooking'] = "拒絕預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['EditRejectReason'] = "修改拒絕原因";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Waiting'] = "等候批核";

$Lang['eBooking']['Button']['FieldTitle']['Borrow'] = "借出";
$Lang['eBooking']['Button']['FieldTitle']['Return'] = "歸還";
$Lang['eBooking']['Button']['FieldTitle']['Search'] = "查詢";
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['History'] = "退還 / 歸還紀錄";
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutBorrow'] = "退還 / 歸還";
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['EditRemark'] = "修改備註";
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutReturnTime'] = "退還/歸還時間";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookingRemark'] = "預訂備註";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['CheckOutRemark'] = "退還備註";

$Lang['eBooking']['CheckInOutStatusArr'][0] = "等待";
$Lang['eBooking']['CheckInOutStatusArr'][1] = "已取用/已借出";
$Lang['eBooking']['CheckInOutStatusArr'][2] = "已退還/已歸還";

$Lang['eBooking']['SelectStatus'] = "請選擇狀態";

$Lang['eBooking']['Management']['FieldTitle']['BookingType'] = "預訂種類";
$Lang['eBooking']['Management']['FieldTitle']['Single'] = "一次性";
$Lang['eBooking']['Management']['FieldTitle']['Repetitive'] = "重複性";

$Lang['eBooking']['WeekDay'] = "週日";

$Lang['eBooking']['eService']['FieldTitle']['WeekDay']="週日";
$Lang['eBooking']['eService']['FieldTitle']['Cycle']="循環日";
$Lang['eBooking']['eService']['FieldTitle']['SpecificDateRange']="指定日期範圍";


$Lang['eBooking']['jsWarningMsg']['OutOfDateRange'] = "所選日期不在可預訂日期範圍內。\\n可預訂日期範圍:";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectWeekDate'] = "請選擇週日。";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectCycleDate'] = "請選擇循環日。";

$Lang['eBooking']['NoCycleSetting'] = "並未設定循環日";

$Lang['eBooking']['NoLimit'] = "無限制";
$Lang['eBooking']['CreateiCalEvent'] = "在行事曆建立新的事件?";
$Lang['eBooking']['OccupiedBy'] = "使用者";
$Lang['eBooking']['ChangeUser'] = "更改使用者";

$Lang['eBooking']['IfItemRejected'] = "如任何一件物品未能預訂 ，請選擇";
$Lang['eBooking']['CancelRelatedBooking'] = "取消當日的房間預訂";
$Lang['eBooking']['KeepRelatedBooking'] = "仍保留當日的房間預訂";
$Lang['eBooking']['ItemNotAvailableInSomeDay'] = "這件物品於部份或全部日期不可預訂。";
$Lang['eBooking']['Detail'] = "詳細資料";

$Lang['eBooking']['NAReasonArr']['OutOfAvailablePeriod'] = "不在可預訂時段內";
$Lang['eBooking']['NAReasonArr']['Booked'] = "已被預訂";
$Lang['eBooking']['NAReasonArr']['Available'] = "可用";

$Lang['eBooking']['NoValidDate'] = "沒有有效的日期，請重新選擇。";

// User booking Rule
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['CustomGroup'] = "自訂用戶";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['TargetUser'] = "自訂用戶";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['PleaseSelectTargetUser'] = "請選擇最少一個用戶";

$Lang['eBooking']['Lesson'] = "課節";

$Lang['eBooking']['InstructionArr']['DisplayGeneralTimetableOnly'] = "以下為普通時間表的課節，若需要預約特別時間表的課節，請按＂返回＂並選擇＂一次性＂，再繼續預訂。";

$Lang['eBooking']['PrintCustomReport'] = "列印自訂報告";
$Lang['eBooking']['ExportCustomReport'] = "匯出自訂報告";

$Lang['eBooking']['General']['Export']['FieldTitle']['BookedBy'] = "預訂者";
$Lang['eBooking']['General']['Export']['FieldTitle']['Date'] = "日期";
$Lang['eBooking']['General']['Export']['FieldTitle']['Time'] = "時間";
$Lang['eBooking']['General']['Export']['FieldTitle']['Day'] = "日";
$Lang['eBooking']['General']['Export']['FieldTitle']['Venue'] = "場地";
$Lang['eBooking']['General']['Export']['FieldTitle']['ActivityName'] = "活動名稱";
$Lang['eBooking']['General']['Export']['FieldTitle']['PIC'] = "負責人";
$Lang['eBooking']['General']['Export']['FieldTitle']['Attandance'] = "參加人數";
$Lang['eBooking']['General']['Export']['FieldTitle']['Details'] = "詳情";
$Lang['eBooking']['General']['ViewDetails'] = "檢視內容";

$Lang['eBooking']['Settings']['SystemProperty']['RoomNameDisplay'] = "房間顯示格式";
$Lang['eBooking']['Settings']['SystemProperty']['Building'] = "大樓";
$Lang['eBooking']['Settings']['SystemProperty']['Floor'] = "樓層";
$Lang['eBooking']['Settings']['SystemProperty']['Room'] = "房間";
$Lang['eBooking']['Settings']['SystemProperty']['BookingNotification'] = "預訂結果電郵通知";
$Lang['eBooking']['Settings']['SystemProperty']['DefaultApplyDoorAccess'] = "預設使用門禁設施";
$Lang['eBooking']['Settings']['SystemProperty']['BookingFormUrl'] = "預訂表格連結";
$Lang['eBooking']['Settings']['SystemProperty']['BookingForm'] = "預訂表格";
$Lang['eBooking']['Settings']['SystemProperty']['DownloadBookingForm'] = "下載預訂表格";
$Lang['eBooking']['Settings']['SystemProperty']['EventInICalendar'] = "預訂行事曆事件設定";
$Lang['eBooking']['Settings']['SystemProperty']['AllowBookingDamagedItem'] = "允許預訂已損壞物品";
$Lang['eBooking']['Settings']['SystemProperty']['AllowBookingRepairingItem'] = "允許預訂維修中物品";
$Lang['eBooking']['Settings']['SystemProperty']['ReceiveEmailNotificationFromeInventory'] = "接收由資產管理行政系統系統發出的電郵通知 (只限管理員)";
$Lang['eBooking']['Settings']['SystemProperty']['ReceiveCancelBookingEmailNotification'] = "接收取消預訂的電郵通知 (只限管理員)";
$Lang['eBooking']['Settings']['SystemProperty']['ApprovalNotification'] = "接收預訂的電郵通知 (只限批核小組)";
$Lang['eBooking']['Settings']['SystemProperty']['BookingApproveNotification'] = "接收批核成功的電郵通知 (只限跟進小組)";

$Lang['eBooking']['Settings']['BookingRequest']['JSConfirm']['TimeOverLapped']="閣下所選的的預訂中出現時間重疊 , 仍需批核有關預訂?";
$Lang['eBooking']['Settings']['BookingRequest']['JSConfirm']['AlreadyBooked']="閣下所選的的預訂中有物品/房間已被預訂, 仍需批核有關預訂?";

$Lang['eBooking']['Instruction']['WarnDeleteRule'] = "刪除預訂規則可能會影響預訂特性中設定。";
$Lang['eBooking']['Instruction']['PleaseDownloadTheRoomBookingForm'] = "請下載預訂表格";

$Lang['eBooking']['Event'] = "事件";
$Lang['eBooking']['SupportEnterOrScanBarCode'] = "支援輸入或掃瞄條碼";
$Lang['eBooking']['Damaged'] = "已損壞";
$Lang['eBooking']['Repairing'] = "維修中";
$Lang['eBooking']['WrittenOff'] = "已報銷";
$Lang['eBooking']['RoomIsDeleted'] = "房間已被刪除";
$Lang['eBooking']['ItemIsDeleted'] = "物品已被刪除";

$Lang['eBooking']['eService']['DoorAccessappliedRemind'] = "此房間已啟用門禁設定，請緊記攜帶你的智能卡。";
$Lang['eBooking']['eService']['CancelBookingNotification'] = "取消預訂通知";
$Lang['eBooking']['eService']['PleaseNoteThatBelowBookingIsBeingCancelled'] = "請注意以下預訂已被取消";

$Lang['eBooking']['Period'] = "時段";
$Lang['eBooking']['BookingUser'] = "預訂用戶";
$Lang['eBooking']['BookingStatus'] = "預訂狀況";
$Lang['eBooking']['ReserveBookingAdminOverwriteWarning'] = "以下的預訂將會被覆寫";
$Lang['eBooking']['ReserveBookingAdminOverwriteConfirm'] = "您是否確定要覆寫上述預訂?";
$Lang['eBooking']['MakePendingBookingAlready'] = "此時段已有屬於你的待批預訂紀錄";
$Lang['eBooking']['PendingBooking'] = "待批預訂";
$Lang['eBooking']['PeriodicTimetableBookingRemarks'] = "以「指定時段」作「重複預訂」，所選日期必須在同一時區內。";

$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['emailTitle'] = "資源預訂跟進通知";
$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['dearSir/Madam'] = "";
$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['PleaseFollowItem'] = "請跟進以下預訂";
$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['WillBeUsedBy'] = "使用者";
$Lang['eBooking']['eMailContentAry']['followUpWorkAry']['BookedBy'] = "預訂者";
$Lang['eBooking']['eMailContentAry']['ManageWorkAry']['PleaseManageItem'] = "請批核以下預訂";
$Lang['eBooking']['eMailContentAry']['ManageWorkAry']['Remarks'] = "閣下可前往「學校行政管理工具 > 資源管理 > 電子資源預訂系統 > 管理 > 預訂申請」批核預訂申請紀錄。";
$Lang['eBooking']['eMailContentAry']['reservedBy'] = "已被<--Name-->預訂";

$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "資源類別編號";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "子類別編號";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "代號";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "名稱 (英文)";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "名稱 (中文)";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "描述 (英文)";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "描述 (中文)";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "位置編號";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "允許預訂";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "需於多少日前預訂";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "批核小組";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "跟進小組";
$Lang['eBooking']['Settings']['Items']['Import']['DataColumn'][] = "用戶權限規則編號";

$Lang['eBooking']['Settings']['Items']['Import']['Update']['DataColumn'] = array_merge(array("現有代號"),$Lang['eBooking']['Settings']['Items']['Import']['DataColumn']);
$Lang['eBooking']['Settings']['Items']['Import']['error']['missing']="為空白";
$Lang['eBooking']['Settings']['Items']['Import']['error']['notFound']="不存在";
$Lang['eBooking']['Settings']['Items']['Import']['error']['notMatch']="不符合";
$Lang['eBooking']['Settings']['Items']['Import']['error']['isExist']="已存在";
$Lang['eBooking']['Settings']['Items']['Import']['error']['invalid']="為無效輸入";
$Lang['eBooking']['Settings']['Items']['Import']['error']['unknown']="輸入失敗，原因不明";
?>