<?php
## Using By :  yat
/* 
 * ********** Change Log [START] **********
 * 
 * Date	:	2011-01-19	[YatWoon]
 *			add send_password_title()
 *
 * Date	:	2010-12-29 [YatWoon]
 *			add ecircular_notify_mail_subject(), ecircular_notify_mail_body() for eCircular email notification
 * Date	:	2010-10-13 [Ronald]
 * 				modified enotice_notify_mail_body(), change the wording base on UCCKE #278
 * 
 * ********** Change Log [END] ********** 
 */

include_once("$intranet_root/includes/libfilesystem.php");
$li = new libfilesystem();
$website = $li->file_read($intranet_root."/file/email_website.txt");
$website = ($website=="") ? "http://".$HTTP_SERVER_VARS["HTTP_HOST"] : $website;
$webmaster = trim($li->file_read($intranet_root."/file/email_webmaster.txt"));
$webmaster = ($webmaster=="") ? $HTTP_SERVER_VARS["SERVER_ADMIN"] : $webmaster;

$title_array = array (
"Mr.",
"Miss.",
"Mrs.",
"Ms.",
"Dr.",
"Prof."
);

if (is_file("$intranet_root/lang/email.customerized.php"))
{
    include_once("$intranet_root/lang/email.customerized.php");
}
else
{

# Start of writing customerized email message php

function email_footer(){
        global $website, $webmaster;
        $x  = "<br>\r\n<br>\r\n------------------------------------------------------<br>\r\n";
        $x .= "This email was sent to you by eClass System.<br>\r\n";
        $x .= "For any enquiry, please send mail to $webmaster<br>\r\n";
        $x .= "For the latest information, please go to $website<br>\r\n";
        return $x;
}

function forget_title(){
        return "eClass Intranet: Request for password";
}

function forget_body($UserEmail, $UserLogin, $UserPassword, $FirstName, $LastName){
         global $website;
        $x  = "Dear $FirstName $LastName,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Welcome to join eClass Intranet, you may login at $website<br>\r\n<br>\r\n";
        $x .= "Your account information is:<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Email: $UserEmail<br>\r\n";
        $x .= "Login ID: $UserLogin<br>\r\n";
        $x .= "Password: $UserPassword<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}

function forget_ack_title()
{
         return "eClass Intranet: Forget Password Acknowledgement";
}

function forget_ack_body($UserLogin, $UserEmail)
{
         $x = "An user has used forget password to retrieve his/her password<br>\r\n";
         $x .= "Login: $UserLogin<br>\r\n";
         $x .= "Email: $UserEmail<br>\r\n";
         $x .= email_footer();
         return $x;
}
function registration_title(){
        return "eClass Intranet: New registration";
}

function send_password_title(){
        return "eClass Intranet account information";
}

function registration_body($UserEmail, $UserLogin, $UserPassword, $FirstName, $LastName){
         global $website;
        $x  = "Dear $FirstName $LastName,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Welcome to join eClass Intranet, you may login at $website<br>\r\n<br>\r\n";
        $x .= "Your account information is:<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Email: $UserEmail<br>\r\n";
        $x .= "Login ID: $UserLogin<br>\r\n";
        $x .= "Password: $UserPassword<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}
function registration_body_new($UserEmail, $UserLogin, $UserPassword, $EnglishName, $ChineseName){
         $cName = ($ChineseName == ""? "":"($ChineseName)");
         global $website;
        $x  = "Dear $EnglishName $cName,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Welcome to join eClass Intranet, you may login at $website<br>\r\n<br>\r\n";
        $x .= "Your account information is:<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Email: $UserEmail<br>\r\n";
        $x .= "Login ID: $UserLogin<br>\r\n";
        $x .= "Password: $UserPassword<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}

function booking_title(){
        return "eClass Intranet: Resource booking cancellation";
}

function periodic_booking_cancel_title(){
         return "eClass Intranet: Periodic Resource Booking Rejected";
}

function periodic_booking_approve_title(){
         return "eClass Intranet: Periodic Resource Booking Approved";
}

function booking_body($DateStart, $DateEnd, $Remark, $ResourceCategory, $ResourceCode, $Title, $FirstName, $LastName){
        $x  = "Dear $FirstName $LastName,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Your resource booking has been cancelled by the administrator.<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Resource: $ResourceCategory: $ResourceCode $Title<br>\r\n";
        $x .= "Booking: $DateStart - $DateEnd<br>\r\n";
        $x .= "Remark: $Remark<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}

function booking_cancel_body ($bdate, $slot, $Remark, $ResourceCategory, $ResourceCode, $Title, $EnglishName, $ChineseName)
{
		$cName = ($ChineseName == ""? "":"($ChineseName)");
        $x  = "Dear $EnglishName $cName,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Your resource booking has been cancelled by the administrator.<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Resource: $ResourceCategory: $ResourceCode $Title<br>\r\n";
        $x .= "Booking Date: $bdate<br>\r\n";
        $x .= "Period: $slot<br>\r\n";
        $x .= "Remark: $Remark<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}

function periodic_booking_cancel_body($ResourceCategory, $ResourceCode, $Title, $start,$end,$slot_str,$type_str, $dates_str, $EnglishName, $ChineseName, $Remark)
{
        $cName = ($ChineseName == ""? "":"($ChineseName)");
        $x  = "Dear $EnglishName $cName,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Your resource booking has been rejected by the administrator.<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Resource: $ResourceCategory: $ResourceCode $Title<br>\r\n";
        $x .= "Start Date: $start<br>\r\n";
        $x .= "End Date: $end<br>\r\n";
        $x .= "Type: $type_str<br>\r\n";
        $x .= "Period: $slot_str<br>\r\n";
        $x .= "Exact Dates:<br>\r\n";
        $x .= "$dates_str<br>\r\n";
        $x .= "Remark: $Remark<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}

function periodic_booking_approve_body($ResourceCategory, $ResourceCode, $Title, $start,$end,$slot_str,$type_str, $dates_str, $EnglishName, $ChineseName,$Remark)
{
		$cName = ($ChineseName == ""? "":"($ChineseName)");
        $x  = "Dear $EnglishName $cName,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Your resource booking has been approved by the administrator.<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Resource: $ResourceCategory: $ResourceCode $Title<br>\r\n";
        $x .= "Start Date: $start<br>\r\n";
        $x .= "End Date: $end<br>\r\n";
        $x .= "Type: $type_str<br>\r\n";
        $x .= "Period: $slot_str<br>\r\n";
        $x .= "Exact Dates:<br>\r\n";
        $x .= "$dates_str<br>\r\n";
        $x .= "Remark: $Remark<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}

function booking_approve_title(){
        return "eClass Intranet: Resource booking approved";
}

function booking_approve_body($bdate, $slot, $Remark, $ResourceCategory, $ResourceCode, $Title, $EnglishName, $ChineseName){
		
		$cName = ($ChineseName == ""? "":"($ChineseName)");
        $x  = "Dear $EnglishName $cName,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Your resource booking has been approved by the administrator.<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Resource: $ResourceCategory: $ResourceCode $Title<br>\r\n";
        $x .= "Booking Date: $bdate<br>\r\n";
        $x .= "Period: $slot<br>\r\n";
        $x .= "Remark: $Remark<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}

function announcement_title($date){
         $x = "Announcement ($date)";
         return $x;
}
function announcement_title_20($date, $type)
{
         $x = "New $type Announcement ($date)";
         return $x;
}
function announcement_body($title, $type)
{
         global $website;
         $x = "A new $type Announcement (\"$title\") has been issued.  Please login here $website to view the announcement.<br>\r\n";
         return $x;
}

function school_announcement_body($date, $title, $poster="")
{
         global $website;
         if ($poster != "")
         {
             $byWho = " by $poster";
         }
         $x = "A new School Announcement (\"$title\") has been issued$byWho on $date. Please login here $website to view the announcement.<br>\r\n";
         return $x;
}

function group_announcement_body($date, $title, $Groups)
{
         global $website;
         $list = implode(",",$Groups);
         $x = "A new Group Announcement (\"$title\") has been issued for $list on $date. Please login here $website to view the announcement.<br>\r\n";
         return $x;
}

function reset_password_title()
{
         return "eClass Intranet: Your Password Has Been Reset";
}

function reset_password_body($title, $name)
{
         global $title_array;
         $s_title = $title_array[$title];
         $x .= "Dear $s_title $name,

         Your password in eClass Intranet has been reset. Please contact the administrator if you do not know the new password.
         ";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,<br>\r\n";
        $x .= "eClass User Support<br>\r\n";
        return $x;


}

function password_remind_title()
{
         return "eClass Intranet: Password Reminder";
}

function password_remind_body($UserEmail,$UserLogin,$UserPassword, $title,$name)
{
         global $title_array,$website;
         $s_title = $title_array[$title];
         $x .= "Dear $s_title $name,<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Welcome to join eClass Intranet, you may login at $website<br>\r\n<br>\r\n";
        $x .= "Your account information is:<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Email: $UserEmail<br>\r\n";
        $x .= "Login ID: $UserLogin<br>\r\n";
        $x .= "Password: $UserPassword<br>\r\n";
        $x .= "<br>\r\n";
        $x .= "Have a nice day!<br>\r\n";
        $x .= "Sincerely,";
        $x .= "eClass User Support<br>\r\n";
        $x .= email_footer();
        return $x;
}

function qb_approve_mail_title()
{
         return "Approval for sharing of your provided questions";
}

function qb_approve_mail_body($subject, $qcodelist, $pts)
{
         $pts += 0;
         $x = "Your questions provided to $subject (Question Code: $qcodelist) have been approved for sharing. ";
         if ($pts != 0)
         $x .= "You have awarded for $pts points for your contribution.<br>\r\n";
         $x .= "Thanks a lot.<br>\r\n";
         return $x;
}
function qb_reject_mail_title()
{
         return "Rejection for sharing of your provided questions";
}

function qb_reject_mail_body($subject, $qcodelist, $reason)
{
         $pts += 0;
         $x = "Your questions provided to $subject (Question Code: $qcodelist) have been reject for sharing.<br>\r\n";
         $x .= "Reason: $reason<br>\r\n";
         if ($pts != 0)
         $x .= "You have awarded for $pts points for your contribution.<br>\r\n";
         $x .= "Thanks a lot.<br>\r\n";
         return $x;
}

function enotice_notify_mail_subject($title)
{
         $x = "[eClass] New electronic Notice - $title";
         return $x;
}

function enotice_notify_mail_body($startdate, $enddate, $title)
{
         global $website;
         
         $Today = date("Y-m-d");
         if ($startdate>$Today)
         {
	         $Term = " will be ";
         }
         else
         {
	         $Term = " has been ";
         }
         
         //$x = "A new electronic notice ($title) has been issued on $startdate.\nPlease kindly sign the notice before $enddate.\nYou may login here $website to view the notice.\n";
         $x = "A new electronic notice ($title) ".$Term." issued on $startdate.<br>\r\n Please kindly sign the notice on or before $enddate.<br>\r\n You may login $website , then go to eService > eNotice to view the notice.<br>\r\n";
         
         return $x;
}

function ecircular_notify_mail_subject($title)
{
         $x = "[eClass] New electronic Circular - $title";
         return $x;
}

function ecircular_notify_mail_body($startdate, $enddate, $title)
{
         global $website;
         
         $Today = date("Y-m-d");
         if ($startdate>$Today)
         {
	         $Term = " will be ";
         }
         else
         {
	         $Term = " has been ";
         }
         
         //$x = "A new electronic notice ($title) has been issued on $startdate.\nPlease kindly sign the notice before $enddate.\nYou may login here $website to view the notice.\n";
         $x = "A new electronic circular ($title) ".$Term." issued on $startdate.<br>\r\n Please kindly sign the circular on or before $enddate.<br>\r\n You may login $website , then go to eService > eCircular to view the circular.<br>\r\n";
         
         return $x;
}

# End of email.php

}

?>
