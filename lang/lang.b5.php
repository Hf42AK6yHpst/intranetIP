<?php
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/
/* ------------------ ATTENTION !!!!!!!!!!!!!!!!!!!! ----------------------------*/
/* please DO NOT use CRIMSON Editor to write on this file and it will override some special character
 you may refer to variable $Lang['StudentAttendance']['AttendLateSymbol'] */
/* for string that start with "1|=|" or "0|=|",
 that means the variable is for return message from server to client side,
"1" stand for success, "0" stand for Fail*/

/********************** Change Log ***********************/
#
# Date : 2020-07-17 [Leon]
#               added $Lang['eInventory']['OrderByPurchaseDate']
#               added $Lang['eInventory']['Ascending']
#               added $Lang['eInventory']['Decending']
#               added $Lang['eInventory']['GenerationTime']
#
# Date : 2020-06-12 [Tommy]
#               added $Lang['eEnrolment']['ShowForm']
#               added $Lang['eEnrolment']['ShowRoundForm']
#               added $Lang['eEnrolment']['ShowAllForm']
#
# Date : 2020-06-04 [Tommy]
#               added $Lang['iCalendar']['null_created_by']
#
# Date : 2020-05-28 [Tommy]
#               added $Lang['SysMgr']['SchoolNews']['onlyShowOwnClass']
#
# Date : 2020-05-25 [Tommy]
#               added $Lang['eEnrolment']['SignUpWarnMsg']
#               added $Lang['eEnrolment']['SignUpTargetForm']
#
# Date : 2020-05-04 [Tommy]
#               added 	$Lang['eEnrolment']['Process']['TargetForm']
#               added   $Lang['eEnrolment']['Process']['StudentChoose']
#               added   $Lang['eEnrolment']['Process']['StudentJoin']
#
# Date : 2020-04-03 [Tommy]
#               added $Lang['Group']['jsWarningMsgArr']['editSingleGroup']
#               added $Lang['Group']['jsWarningMsgArr']['diffGroupSelected']
#
# Date : 2020-04-03 [Ray]
#               added $Lang['SubsidyIdentity']['ClassName']
#               added $Lang['SubsidyIdentity']['ClassNumber']
#               added $Lang['SubsidyIdentity']['StudentName']
#               added $Lang['SubsidyIdentity']['EffectivePeriod']
#
# Date : 2020-04-01 [Tommy]
#               added $Lang['Btn']['Bulk_Edit']
#
# Date : 2020-03-18 [Tommy]
#                added $Lang['eEnrolment']['MaxClub']
#                added $Lang['eEnrolment']['ExportClubApplicantList']
#                added $Lang['eEnrolment']['ExportStudentMaxWanted']
#
# Date : 2020-03-17 [Tommy]
#               added $Lang['ePOS']['TransactionTimes']
#               added $Lang['ePOS']['LastTransactionDate']
#
# Date : 2020-03-12 [Philips]
#				added $Lang['StudentRegistry']['Settings']['Custom']['Section']
#				added $Lang['StudentRegistry']['Settings']['CustomColNotSelected']
#				added $Lang['StudentRegistry']['Settings']['CustomColSectionReminder']
#
# Date : 2020-03-12 [Tommy]
#               added $Lang['eEnrolment']['OverallMark']
#
# Date : 2020-03-10 [Tommy]
#               added $Lang['eEnrolment']['SemesterMark']
#
# Date : 2020-02-26 [Tommy]
#               added $Lang['eEnrolment']['Btn']['ExportMemberPerformance']
#
# Date : 2020-02-12 [Tommy]
#               added $Lang['eInventory']['ItemCode_PLKCHC']
#               added $Lang['eInventory']['ExpectedLocation_PLKCHC']
#               added $Lang['eInventory']['CurrentLocation_PLKCHC']
#               added $Lang['eInventory']['Original_Quantity_PLKCHC']
#               added $Lang['eInventory']['VarianceQty_PLKCHC']
#               added $Lang['eInventory']['TotalPrice_PLKCHC']
#               added $Lang['eInventory']['PurchaseDate_PLKCHC']
#               added $Lang['eInventory']['Item_Price_PLKCHC']
#               added $Lang['eInventory']['Item_Qty_PLKCHC']
#               added $Lang['eInventory']['Now_TotalPrice_PLKCHC']
#               added $Lang['eInventory']['Caretaker_PLKCHC']
#               added $Lang['eInventory']['RequestDate_PLKCHC']
#               added $Lang['eInventory']['WriteOffStatus_PLKCHC']
#               added $Lang['eInventory']['ItemType_PLKCHC']
#               added $Lang['eInventory']['StockCheckPeriod_PLKCHC']
#               added $Lang['eInventory']['Item_Price2_PLKCHC']
#
# Date : 2020-02-05 [Philips]
#				added $Lang['eSports']['Sports']['Export_TrackFieldResult_MKSS']
#
# Date : 2020-02-04 [Tommy]
#               added $Lang['eInventory']['TotalFunds']
#
# Date : 2020-01-31 [Philips]
#				added $Lang['eSports']['Sports']['Print_FinalResultReport_MKSS']
#				added $Lang['eSports']['Sports']['Print_Top3EventResult_MKSS']
#
# Date : 2020-01-31 [Tommy]
#               added $Lang['eInventory']['TotalWriteOffPrice']
#
# Date : 2020-01-30 [Philips]
#				added $Lang['eSports']['Sports']['EnrolMinEvent']
#				added $Lang['eSports']['Sports']['Warn_EnrolMinEvent']
#
# Date : 2020-01-20 [Tommy]
#               added $Lang['SysMgr']['FormClassMapping']['ImportTeacherWarning']
#               added $Lang['SysMgr']['FormClassMapping']['ImportTeacherWarning2']
#
# Date : 2020-01-17 [Tommy]
#               added $Lang['eEnrolment']['Refresh_Performance']['AdjustedReason']
#
# Date : 2020-01-14 [Tommy]
#               added $Lang['eEnrolment']['Refresh_Performance']['Name']
#               added $Lang['eEnrolment']['Refresh_Performance']['Export']
#               added $Lang['eEnrolment']['Refresh_Performance']['Role']
#               added $Lang['eEnrolment']['Refresh_Performance']['extraInfo']
#
# Date : 2020-01-13 [Tommy]
#               added $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['CancelledByBookedUser']
#
# Date : 2020-01-07 [Tommy]
#               added $Lang['eEnrolment']['heungto_PerformanceMark']
#
# Date : 2019-12-27 [Philips]
#				added $Lang['ePayment']['IssueDate']
#
# Date : 2019-12-13 [Philips]
#				added $Lang['eHomework']['ExportHandinHistory']
#				added $Lang['Homework']['HandinHistoryExportColumn']['EN']
#				added $Lang['Homework']['HandinHistoryExportColumn']['CH']
#				added $Lang['eHomework']['NormalExport']
#
# Date : 2019-12-11 [Philips]
#				added $Lang['eHomework']['ExportPeriodWithin90Days']
#
# Date : 2019-11-11 [Philips]
#				added $Lang['eNotice']['AccessGroupCanSeeAllNotice']
#
# Date : 2019-11-01 [Tommy]
#               added $eEnrollment['TotalStudent']
#
# Date : 2019-10-31[Philips]
#				added $Lang['eSports']['IndividualEventRanking']
#
# Date : 2019-10-11[Philips]
#               added $Lang['eSports']['Lane']
#               added $Lang['eSports']['Order']
#
# Date : 2019-10-03[Tommy]
#               added $i_InventorySystem_Item_Barcode_Num
#
# Date: 2019-09-30[Philips]
#				added $Lang['eNotice']['DefaultUserNotifyUsingEmail']
#
# Date:	2019-09-23[Philips]
#				added $Lang['eSports']['Report']['AverageScore']
#				added $Lang['eSports']['Settings']['EventGroupSchedule']
#				added $Lang['eSports']['Settings']['EventRound']
#				added $Lang['eSports']['Settings']['EventScheduleCode']
#				added $Lang['eSports']['Settings']['EventTime']
#				added $Lang['eSports']['Settings']['EventGroupScheduleReset']
#				added $Lang['eSports']['Participation']['AutoStatusReminder']
#				added $Lang['MessageCenter']['ImageAttachementReminder']
#
# Date:  2019-09-10[Tommy]
#               added $Lang['SysMgr']['SchoolNews']['DisplayOnTop']
#
# Date:  2019-07-26[Tommy]
#               added $i_InventorySystem_Item_FundingSource2Code
#               added $Lang['eInventory']['System']['ItemCode']['format']['N/A']
#               added $Lang['eInventory']['System']['NumberDigits']
#               added $Lang['eInventory']['System']['Separator']
#               added $Lang['eInventory']['System']['Separtor']['NotUsing']
#
#  Date : 2019-07-04[Ray]
#               added $Lang['StudentAttendance']['EmailNotificationJobSendToAdmin']
#               added $Lang['StudentAttendance']['EmailNotificationJobSendToTeacher']
#               added $Lang['StudentAttendance']['AllAdminUsers']
#               added $Lang['StudentAttendance']['OnlySelectedAdminUsers']
#               added $Lang['StudentAttendance']['AllTeacherUsers']
#               added $Lang['StudentAttendance']['OnlySelectedTeacherUsers']
#               added $Lang['StudentAttendance']['EmailNotificationJobCurrentAcademicYear']
#               added $Lang['StudentAttendance']['EmailNotificationJobSpecificDateRange']
#               added $Lang['StudentAttendance']['SelectSendToAdminClassTeacherErrorMessage']
#
# Date: 2019-06-28[Tommy]
#               added $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['WrittenOff']
#
# Date: 2019-06-25[Philips]
#               added $Lang['StudentRegistry']['NoSpecialCharacter']
#
# Date: 2019-06-24[Tommy]
#               added $Lang['eInventory']['FundingSource']['jsFundingSourceCheckBox']
#
# Date : 2019-06-20[Ray]
#               added $Lang['ePayment']['AdminTransferMoneyForStudents']
#
# Date : 2019-06-19[Tommy]
#               added $Lang['eInventory']['ItemNewQuantityGreaterThan0']
#
# Date : 2019-05-21[Philips]
#               added $Lang['StudentAttendance']['SeriousLate']
#               added $Lang['StudentAttendance']['NumberOfSeriousLate']
#
# Date : 2019-04-24[Philips]
#               added $Lang['StudentRegistry']['Settings']['CustomCol']
#               added $Lang['StudentRegistry']['Settings']['Custom']['DisplayText_ch']
#               added $Lang['StudentRegistry']['Settings']['Custom']['DisplayText_en']
#               added $Lang['StudentRegistry']['Settings']['Custom']['Code']
#
# Date : 2019-01-29[Isaac]
#               modified $Lang['eInventory']['ExportInvoice'], fixed wording not showing generally
#
# Date : 2018-12-31 [Isaac]
#              added  $Lang['AccountMgmt']['ErrorMsg']['MissedDOB']
#
# Date : 2018-12-28 [Isaac]
#               modified $Lang['AccountMgmt']['ArchivedStudentImportFields'] where added '*' when $sys_custom['AccountMgmt']['StudentDOBRequired'] is on
#
# Date : 2018-12-19 [Isaac]
#               added $Lang['Header']['Menu']['ePCMApproval']
#
# Date : 2018-11-12 [Philips]
#               modified $Lang['EmailNotification']['SchoolNews']['Subject'] by adding __TITLE
#
# Date : 2018-11-05 [Rox]
#               added $Lang['eClass']['Management']['select_subject']
#                     $Lang['eClass']['Management']['teacher_only']
#                     $Lang['eClass']['Management']['student_only']
#
# Date : 2018-10-10 [Philips]
#               added $Lang['iAccount']['StudentPerformance_STSummary']
#
# Date : 2018-09-10 [Philips]
#               added $Lang['eNotice']['NotifyPICByEmail']
#               added $Lang['eNotice']['NotifyClassTeachersByEmail']
#
# Date : 2018-08-30 [Philips]
#               added $Lang['SystemSetting']['ReportLog']['Title']
#               added $Lang['SystemSetting']['ReportLog']['Column']['Rtype']
#               added $Lang['SystemSetting']['ReportLog']['Column']['Action']
#               added $Lang['SystemSetting']['ReportLog']['Rtype']['login']
#               added $Lang['SystemSetting']['ReportLog']['Rtype']['account']
#               added $Lang['SystemSetting']['ReportLog']['Rtype']['report']
#               added $Lang['SystemSetting']['ReportLog']['Action']['export']
#               added $Lang['SystemSetting']['ReportLog']['Action']['html']
#               added $Lang['SystemSetting']['ReportLog']['Action']['pdf']
#               added $Lang['SystemSetting']['AccountLog']['PageItem']
#               added $Lang['SystemSetting']['AccountLog']['PageMax']
#               added $Lang['SystemSetting']['AccountLog']['CurrPage']
#               added $Lang['SystemSetting']['AccountLog']['PageRecord']
#
# Date : 2018-08-15 [Vito]
#               added $Lang['SystemSetting']['AccountLog']['LogBylogin']
#               added $Lang['SystemSetting']['AccountLog']['Userlogin']
#               added $Lang['SystemSetting']['AccountLog']['LogBy']
#
# Date : 2018-08-10 [Philips]
#               added $Lang['eSports']['Sports']['ClassRelay']['EnrolLimitAlert']
#
# Date : 2018-08-06 [Philips]
#               added $Lang['SystemSetting']['LoginRecords']['Title']
#               added $Lang['SystemSetting']['AccountLog']['Title']
#               added $Lang['SystemSetting']['AccountLog']['Keyword']
#               added $Lang['SystemSetting']['AccountLog']['showID']
#               added $Lang['SystemSetting']['AccountLog']['logType']
#               added $Lang['SystemSetting']['AccountLog']['DateRange']
#               added $Lang['SystemSetting']['AccountLog']['From']
#               added $Lang['SystemSetting']['AccountLog']['To']
#               added $Lang['SystemSetting']['AccountLog']['LogBy']
#               added $Lang['SystemSetting']['AccountLog']['User']
#               added $Lang['SystemSetting']['AccountLog']['Action']
#               added $Lang['SystemSetting']['AccountLog']['Date']
#               added $Lang['SystemSetting']['AccountLog']['Time']
#               added $Lang['SystemSetting']['AccountLog']['IPAddress']
#               added $Lang['SystemSetting']['AccountLog']['Create']
#               added $Lang['SystemSetting']['AccountLog']['Update']
#               added $Lang['SystemSetting']['AccountLog']['Read']
#               added $Lang['SystemSetting']['AccountLog']['Delete']
#               added $Lang['SystemSetting']['AccountLog']['FilterOption']
#               added $Lang['SystemSetting']['AccountLog']['All']
#
# Date : 2018-07-19 [Philips]
#               added $Lang['Sports']['Report']['AllArrangement']
#               added $Lang['Sports']['Report']['GameArrangement']
#               added $Lang['Sports']['Report']['Classes']
#               added $Lang['Sports']['Report']['ClassTotal']
#               added $Lang['Sports']['Report']['AgeGroupTotal']
#               added $Lang['Sports']['Report']['Boy']
#               added $Lang['Sports']['Report']['Girl']
#               added $Lang['Sports']['Report']['Events']
#
# Date : 2018-06-27 [Philips]
#               added $Lang['SFOC']['Settings']['Icon']
#
# Date : 2018-06-26 [Philips]
#               added $MedalListMenu['sport']
#               added $Lang['SFOC']['Settings']['SportChi']
#               added $Lang['SFOC']['Settings']['SportEng']
#               added $Lang['SFOC']['Settings']['SportIntroChi']
#               added $Lang['SFOC']['Settings']['SportIntroEng']
#
# Date : 2018-06-08 [Philips]
#               added $Lang['eEnrolment']['course_tempMenu'] = 'Course Template';
#               added $Lang['eEnrolment']['course_temp']['template'] = 'Template';
#               added $Lang['eEnrolment']['course_temp']['sim'] = 'Similar Item(s)';
#               added $Lang['eEnrolment']['course_temp']['noSim'] = 'No Similar Item';
#               added $Lang['eEnrolment']['course_temp']['group']['Title'] = 'Course Title';
#               added $Lang['eEnrolment']['course_temp']['group']['eventTotal'] = 'Total of Event(s)';
#               added $Lang['eEnrolment']['course_temp']['generate'] = 'Generate';
#               added $Lang['eEnrolment']['course_temp']['totalDay'] = 'Total of Day(s)';
#               added $Lang['eEnrolment']['course_temp']['event']['Title'] = 'Event Title';
#               added $Lang['eEnrolment']['course_temp']['event']['Description'] = 'Event Description';
#               added $Lang['eEnrolment']['course_temp']['period']['rangeError'] = 'Time Range Error';
#               added $Lang['eEnrolment']['course_temp']['period']['label'] = 'Period';
#               added $Lang['eEnrolment']['course_temp']['period']['day'] = 'Day(s)';
#               added $Lang['eEnrolment']['course_temp']['period']['duration'] = 'During time';
#               added $Lang['eEnrolment']['course_temp']['result']['clubTotal'] = 'Club(s) Generated';
#               added $Lang['eEnrolment']['course_temp']['result']['eventTotal'] = 'Event(s) Generated';
#
# Date : 2018-05-25 [Philips]
#               added $Lang['eEnrolment']['course_tempMenu']
#
# Date : 2018-05-23 [Philips]
#               added $Lang['AppNotifyMessage']['AllSenders']
#               added $Lang['AppNotifyMessage']['FromSystem']
#               added $Lang['AppNotifyMessage']['FromTeacher']
#
# Date : 2018-05-21 [Philips]
#               added $Lang['eNotice']['NotifyPICByPushMessage']
#               added $Lang['eNotice']['NotifyClassTeachersByPushMessage']
#
# Date : 2018-05-15 [Isaac]
#               added $Lang['AccountMgmt']['LastEditUserName']
#               added $Lang['AccountMgmt']['LastEditUserDate']
#               added $Lang['AccountMgmt']['CreatedAndModifiedRecords']
#               added $Lang['AccountMgmt']['ArchivedRecords']
#               edited $Lang['AccountMgmt']['CreateUserID']
#               edited $Lang['AccountMgmt']['CreateUserName']
#               edited $Lang['AccountMgmt']['CreateUserDate']
#
# Date : 2018-05-07 [Isaac]
#               added $Lang['AppNotifyMessage']['WarningFollowingUsersCannotBeFoundOrAdded']
#
# Date  : 2018-04-26 [Isaac]
#               added $Lang['CommonChoose']['SearchParentByInputFormat']
#               added $Lang['General']['searchAndInsertUser']
#               added $Lang['General']['msgSearchAndInsertInfo']
#
# Date	: 2018-04-20 [Ronald]
#               added $Lang['AppNotifyMessage']['AttachImage']
#
# Date	: 2018-04-16 [Danny]
#				added $Lang['StudentMgmt']['ExportForAlumni']
#				added $Lang['StudentMgmt']['MayNotImportToAlumniDirectly']
#				added $Lang['StudentMgmt']['ExportSpecificYear']
#				added $Lang['StudentMgmt']['ExportAll']
#				added $Lang['StudentMgmt']['EmptyYearRecords']
#
# Date : 2018-04-09 [Philips]
#               edited $Lang['eEnrolment']['createUserName']
#               edited $Lang['eEnrolment']['createUserID']
#               edited $Lang['eEnrolment']['createUserDate']
#
# Date  : 2018-03-20 [Isaac]
#               added $Lang['eInventory']['FieldTitle']['DownloadofflineStocktakeSingleItemTemplate']
#               added $Lang['eInventory']['FieldTitle']['DownloadofflineStocktakeBulkItemTemplate']
#               added $Lang['eInventory']['FieldTitle']['OfflineStocktakeFormatArray']
#               added $Lang['eInventory']['FieldTitle']['ForSingleItem']
#               added $Lang['eInventory']['FieldTitle']['ForBulkItem']
#               added $Lang['eInventory']['FieldTitle']['OfflineStocktakeFormatArray']['Single']
#               added $Lang['eInventory']['FieldTitle']['OfflineStocktakeFormatArray']['Bulk']
#
# Date  ; 2018-03-19 [Isaac]
#               aded $Lang['eInventory']['ExportInvoice']
#
# Date	: 2018-02-22 [Isaac]
#               added $Lang['AppNotifyMessage']['searchAndInsertStudentAndStaff']
#
# Date	: 2018-02-07 [Isaac]
#               added $Lang['eDiscipline']['PrintNewPagePerClass']
#
# Date	: 2018-01-23 [Isaac]
#               added $Lang['AppNotifyMessage']['MessageToNotSignedParents']
#
# Date	: 2018-01-16 [Anna]
#				added $Lang['eDiscipline']['PushMessage']['DetentionAbsent']
#
# Date  : 2017-11-28 [Isaac]
#               changed $Lang['AccountMgmt']['StudentWithPhotoList'] to equal to "下載相片"
#               added $Lang['iAccount']['StudentOfficialPhoto']
# Date  : 2017-11-20 [Simon]
#				added $Lang['General']['StudentPhotos']
#
#Date  ： 2017-11-16 [Isaac]
#
#Date  ： 2017-10-17 [Siuwan]
#               added $Lang['SysMgr']['RoleManagement']['PowerLesson2']
#
# Date  ： 2017-10-13 [Simon]
#               added $Lang['eEnrolment']['InformedToClassTeacher']
#               added $Lang['eEnrolment']['InformedToPIC']
#
# Date  ： 2017-09-07 [Anna]
#               added $sys_custom['eEnrolment']['UnitRolePoint']
#
# Date	: 2017-08-18 [Anna]
#				added $Lang['eEnrolment']['ClearDrawingInstruction']
#
# Date	: 2017-07-06 [Anna]
#				added $Lang['Security']
#
# Date	: 2017-06-26 [Anna]
#				added $Lang['ePayment']['AmountRemarks']
#
# Date	: 2017-06-14 [Anna]
#				added $Lang['SysMgr']['CycleDay']['RemarkSpecialTimetable']
#
# Date	: 2017-06-06[Anna]
#				added $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicator']
#				      $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicatorByAll']
#				      $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicatorBySingle']
#
# Date	: 2017-06-05[Anna]
#				added $Lang['General']['JS_warning']['InputRole'],$Lang['General']['JS_warning']['RoleIsInUse']
#
# Date	: 2017-02-15[Anna]
#				added $Lang['eSurvey']['Target']
#
# Date	: 2016-12-14[Frankie Leung]
#				added $Lang['SysMgr']['RoleManagement']['TeacherPortfolio']
#
# Date	: 2016-11-21[HenryHM]
#				added $Lang['Gamma']['UserGmail']
#
# Date	: 2016-10-13[Pun]
#				added $Lang['PowerLesson2']['PowerLesson2']
#
# Date	: 2016-09-28[Villa]
#				added $Lang['eBooking']['eService']['WarningArr']['SelectedDatesInDifferentTimetable']
#
# Date	: 2016-09-23[Villa]
#				added $Lang['eClassApp']['DefaultUserNotifyUsingApp'];
#
# Date 	: 2016-09-21[Villa]
#				added $Lang['eClassApps']['PushMessage']
#				added $Lang['eClassApps']['SendToNew']
#
# Date  : 2016-09-20 [Villa]
#				added $Lang['AppNotifyMessage']['MessageToStaff']
#				added $Lang['AppNotifyMessage']['eCircular']['Alert']['App']
# 				added $Lang['AppNotifyMessage']['MessagingMethodToStaff']
#
# Date  : 2016-09-15 [Villa]
#				added $Lang['StudentAttendance']['AbsentProveReport']['DateOfAbsentLateEarlyLeave']
#
# Date  : 2016-08-15 [Henry HM]
#               added $Lang['HKUSPH'][...]
#
# Date	: 2016-06-30 [Cara]
#				added $Lang['RepairSystem']['MappingSettings']
#					  $Lang['RepairSystem']['MapToInventory']
#					  $Lang['RepairSystem']['NoItemIsMapped']
#					  $Lang['RepairSystem']['SelectMapMethod']
#					  $Lang['RepairSystem']['SuggestedItemsCheck']
#					  $Lang['RepairSystem']['AdvanceSearch']
#					  $Lang['RepairSystem']['SystemUser']
#					  $Lang['RepairSystem']['NonSystemUser']
#					  $Lang['RepairSystem']['FollowUpRequest']
#					  $Lang['RepairSystem']['Mapped']
#					  $Lang['RepairSystem']['NoResults']
#
# Date  : 2016-06-22 [Henry HM]
#               added $Lang['eEnrolment']['IncludeInReports']
#
# Date	: 2016-05-31 [Henry HM]
#				added $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit']
#
# Date	: 2016-03-02 [Paul]
#				added $Lang['PowerLessonAppWeb']['ManageLessonPlan']
#
# Date	: 2015-12-29 [Paul]
#				added a set of Markbook Lang from eclass40 lang file to prevent including both intranet lang file and eclass lang file
#
# Date	: 2015-12-7 [Kenneth]
#				added $Lang['eEnrolment']['Warning']['SelectCategory']
#
# Date	: 2015-10-20 [Paul]
#				added a set of lang $Lang['eClass']['LfAssessmentReport']
# Date	: 2015-10-16 [Kenneth Yau]
#				edit $Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']
#				edit $Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents']
# Date	: 2015-10-06 [Paul]
#				added $Lang['eClass']['default_max_size']
# Date	: 2015-08-21 [Siuwan] [ip.2.5.6.9.1]
#				added $Lang['General']['PowerFlip']
# Date	: 2015-06-16 [Jason]
#				replace split() by explode() in order to support php 5.4+
#
# Date	: 2010-11-02 [Yuen]
#				include customized lang once after loading this default lang file
#
/******************* End Of Change Log *******************/
$Lang['eClassApps']['PushMessage'] = '推送訊息';
$Lang['eClassApps']['SendToNew'] = '只傳送至新加入的對象';

$Lang['Header']['HomeTitle'] = "eClass IP";
$Lang['Header']['eClassCommunity'] = "eClass 社群網";
$Lang['Header']['OnlineHelp'] = "說明";
$Lang['Header']['OnlineDocs'] = "文件下載";
$Lang['Header']['FAQ'] = "常見問題";
$Lang['Header']['VideoTutorial'] = "產品教學示範";
$Lang['Header']['ReprintCardSystem'] = "智能卡訂購系統";
$Lang['Header']['iPortbuy'] = "iPortfolio 戶口訂購系統";
$Lang['Header']['ChangeLang'] = "Change Interface Language";
$Lang['Header']['Logout'] = "登出";

$Lang['Header']['ENG'] = "ENG";
$Lang['Header']['B5'] = "繁";

$Lang['Header']['Admin']['eClassUpdate'] = "eClass 更新";
$Lang['Header']['Admin']['eClassBackup'] = "eClass 備份";

$Lang['Header']['Menu']['Home'] = "首頁";
$Lang['Header']['Menu']['eService'] = "資訊服務";
$Lang['Header']['Menu']['eLearning'] = "學與教管理工具";
$Lang['Header']['Menu']['eAdmin'] = "學校行政管理工具";
$Lang['Header']['Menu']['SchoolSettings'] = "學校基本設定";

$Lang['Header']['Menu']['GeneralManagement'] = "一般事項管理";
$Lang['Header']['Menu']['StaffManagement'] = "職員管理";
$Lang['Header']['Menu']['StudentManagement'] = "學生管理";
$Lang['Header']['Menu']['ResourcesManagement'] = "資源管理";

$Lang['Header']['Menu']['eCircular'] = "職員通告";
$Lang['Header']['Menu']['eHomework'] = "網上家課表";
$Lang['Header']['Menu']['eReportCard'] = "成績表系統";
$Lang['Header']['Menu']['eReportCard_Rubrics'] = "成績表系統 (評量指標)";
$Lang['Header']['Menu']['eReportCardKindergarten'] = "成績表系統 (幼稚園)";
$Lang['Header']['Menu']['eBooking'] = "電子資源預訂系統";
$Lang['Header']['Menu']['eBookingApproval'] = "電子資源預訂批核";
$Lang['Header']['Menu']['CampusTV'] = "校園電視台";
$Lang['Header']['Menu']['DigitalArchive'] = "電子文件";
$Lang['Header']['Menu']['SubjectReference'] = "學科電子資源";
$Lang['Header']['Menu']['Organization'] = "組織";
$Lang['Header']['Menu']['eClass'] = "網上教室";
$Lang['Header']['Menu']['eLC'] = "網上課件中心";
$Lang['Header']['Menu']['ELP'] = "eClass 網上課件套";
$Lang['Header']['Menu']['SSR'] = "網上評估室";
$Lang['Header']['Menu']['ReadingRoom'] = "閱讀室";
$Lang['Header']['Menu']['SFC'] = "Scrabble Fun Corner";
$Lang['Header']['Menu']['DRC'] = "網上圖書館";
$Lang['Header']['Menu']['IES'] = "通識獨立專題探究";
$Lang['Header']['Menu']['SBA'] = "SBA_LS";
$Lang['Header']['Menu']['ReadingScheme'] = "閱讀計劃";
$Lang['Header']['Menu']['eLibrary'] = "電子圖書";
$Lang['Header']['Menu']['eLibraryPlus'] = ($plugin['eLib_Lite']?"電子圖書館":"eLibrary <i>plus</i> 綜合圖書館");
$Lang['Header']['Menu']['eLibraryPlus4PowerClass'] = ($plugin['eLib_Lite']?"電子圖書館":"綜合圖書館");
$Lang['Header']['Menu']['eAttednance'] = "考勤管理";
$Lang['Header']['Menu']['eDiscipline'] = "訓導管理";
$Lang['Header']['Menu']['eEnrolment'] = "課外活動管理";
$Lang['Header']['Menu']['eEnrolmentLite'] = "課外活動管理 (Lite version)";
$Lang['Header']['Menu']['eSports'] = "運動會管理";
$Lang['Header']['Menu']['SportDay'] = "運動會";
$Lang['Header']['Menu']['SwimmingGala'] = "水運會";
$Lang['Header']['Menu']['eNotice'] = "電子通告系統";
$Lang['Header']['Menu']['eInventory'] = "資產管理行政系統";
$Lang['Header']['Menu']['ePCM'] = "採購系統";
$Lang['Header']['Menu']['ePCMApproval'] = "採購系統申請批核";
$Lang['Header']['Menu']['ePolling'] = "投票";
$Lang['Header']['Menu']['ePayment'] = "智能咭繳費系統";
$Lang['Header']['Menu']['ePOS'] = "ePOS";
$Lang['Header']['Menu']['ResourcesBooking'] = "資源預訂";
$Lang['Header']['Menu']['schoolNews'] = "校園最新消息";
$Lang['Header']['Menu']['eSurvey'] = "問卷調查";
$Lang['Header']['Menu']['Writing'] = "寫作";
$Lang['Header']['Menu']['easyWriting'] = "寫作";
$Lang['Header']['Menu']['eClassApp'] = "eClass App";
$Lang['Header']['Menu']['msschPrinting'] = "影印紀錄";
$Lang['Header']['Menu']['SLRS'] = "代課編排系統";
$Lang['Header']['Menu']['eAppraisal'] = "考績管理系統";
$Lang['Header']['Menu']['TeacherPortfolio'] = "教職員檔案系統";
$Lang['Header']['Menu']['eForm'] = "電子表單";

$Lang['Header']['Menu']['iMail'] = "我的電郵";
$Lang['Header']['Menu']['iMailGamma'] = "iMail plus";
$Lang['Header']['Menu']['iCalendar'] = "我的行事曆";
$Lang['Header']['Menu']['eCommunity'] = "社群管理工具";
$Lang['Header']['Menu']['iAccount'] = "我的戶口";
$Lang['Header']['Menu']['iFolder'] = "我的文件夾";
$Lang['Header']['Menu']['iPortfolio'] = "學習檔案";
$Lang['Header']['Menu']['StudentDataAnalysisSystem']['general'] = "學生數據分析";
$Lang['Header']['Menu']['StudentDataAnalysisSystem']['catholic'] = "學生數據分析";
$Lang['Header']['Menu']['StudentDataAnalysisSystem']['tungwah'] = "中央電子教育系統 (學校)";
$Lang['Header']['Menu']['CampusLinks'] = "校園連結";
$Lang['Header']['Menu']['ismartcard'] = "我的智能卡紀錄";
$Lang['Header']['Menu']['RSS'] = "RSS";
$Lang['Header']['Menu']['MessageCenter'] = "通訊中心";
$Lang['Header']['Menu']['MassMailing'] = "合併電郵";
$Lang['Header']['Menu']['ParentAppsNotify'] = "家長通知 (eClass App)";
$Lang['Header']['Menu']['TeacherAppsNotify'] = "教職員通知 (Teacher App)";
$Lang['Header']['Menu']['StudentAppsNotify'] = "學生通知 (Student App)";
$Lang['Header']['Menu']['SMS'] = "短訊服務";
$Lang['Header']['Menu']['PushNotification'] = "推送訊息 (Apps)";
$Lang['Header']['Menu']['LibraryMgmtSystem'] = ($plugin['eLib_Lite']?"電子圖書館管理":"eLibrary <i>plus</i> 綜合圖書館管理");
$Lang['Header']['Menu']['ClassDiary'] = "課堂日誌";
$Lang['Header']['Menu']['DocRouting'] = "文件傳遞";
$Lang['Header']['Menu']['Medical'] = '護理系統';
$Lang['Header']['Menu']['AwardScheme'] = '獎勵計劃';
$Lang['Header']['Menu']['MDM'] = 'MDM';
$Lang['Header']['Menu']['eSchoolBus']='校車管理';
$Lang['Header']['Menu']['eGuidance']='輔導管理';
$Lang['Header']['Menu']['SharingArea'] = "共享地帶";
$Lang['Header']['Menu']['Forum'] = "討論區";
# School Settings
$Lang['Header']['Menu']['Site'] = "校園";
$Lang['Header']['Menu']['Role'] = "身份角色";
$Lang['Header']['Menu']['Subject'] = "學科";
$Lang['Header']['Menu']['Subjects'] = "學科";
$Lang['Header']['Menu']['Class'] = "班別";
$Lang['Header']['Menu']['Group'] = "小組";
$Lang['Header']['Menu']['CycleDay'] = "循環日";
$Lang['Header']['Menu']['Timetable'] = "時間表";
$Lang['Header']['Menu']['Period'] = "時段";
$Lang['Header']['Menu']['SchoolCalendar'] = "校曆表";
$Lang['Header']['Menu']['DigitalChannels'] = "數碼頻道";
$Lang['Header']['Menu']['PersonalProfile'] = "個人資料";
$Lang['Header']['Menu']['ChangePassword'] = "更改密碼";
$Lang['Header']['Menu']['OrganizationInfo'] = "學校資料";

$Lang['Portal']['CampusTV']['Live'] = "直播";
$Lang['Portal']['BrowserChecking']['BrowserTooNew'] = "eClass校園綜合平台不完全支援你所使用的瀏覽器版本。建議你使用Internet Explorer 7進行瀏覽。";
$Lang['Portal']['Timetable']['TodayTimetable'] = "時間表";
$Lang['Portal']['Timetable']['ViewWholeTimetable'] = '檢視完整時間表';

$Lang['Identity']['TeachingStaff'] = "教學職務員工";
$Lang['Identity']['NonTeachingStaff'] = "非教學職務員工";
$Lang['Identity']['SupplyTeacher'] = "外來代課老師";
$Lang['Identity']['Student'] = "學生";
$Lang['Identity']['Parent'] = "家長";
$Lang['Identity']['Guardian'] = "監護人";
$Lang['Identity']['Alumni'] = "校友";
$Lang['Identity']['Staff'] = "教職員";


$Lang['Btn']['Submit'] = '呈送';
$Lang['Btn']['Reset'] = '重置';
$Lang['Btn']['Cancel'] = '取消';
$Lang['Btn']['AddAll'] = '新增所有';
$Lang['Btn']['AddSelected'] = '新增已選取的';
$Lang['Btn']['RemoveSelected'] = '移除已選項目';
$Lang['Btn']['Remove'] = '移除';
$Lang['Btn']['RemovePhoto'] = '移除相片';
$Lang['Btn']['RemoveAll'] = '移除所有';
$Lang['Btn']['Edit'] = '編輯';
$Lang['Btn']['EditThisGroup'] = '編輯此科組';
$Lang['Btn']['Bulk_Edit'] = '批量編輯';
$Lang['Btn']['Done'] = '完成';
$Lang['Btn']['Clear'] = '清除';
$Lang['Btn']['Save'] = '儲存';
$Lang['Btn']['SaveAs'] = "另存";
$Lang['Btn']['Add'] = '增加';
$Lang['Btn']['Select'] = "選擇";
$Lang['Btn']['NotSet'] = "不設定";
$Lang['Btn']['All'] = "全部";
$Lang['Btn']['Submit&AddMore'] = "呈送後繼續新增";
$Lang['Btn']['AddMore'] = "新增";
$Lang['Btn']['Move'] = "移動";
$Lang['Btn']['Delete'] = "刪除";
$Lang['Btn']['New'] = '新增';
$Lang['Btn']['Import'] = '匯入';
$Lang['Btn']['Export'] = '匯出';
$Lang['Btn']['ImportStudent'] = '匯入學生';
$Lang['Btn']['SelectAll'] = '全選';
$Lang['Btn']['Show'] = '顯示';
$Lang['Btn']['Hide'] = '隱藏';
$Lang['Btn']['Generate'] = '產生';
$Lang['Btn']['Close'] = '關閉';
$Lang['Btn']['Clone'] = '複製';
$Lang['Btn']['View'] = '檢視';
$Lang['Btn']['Apply'] = '套用';
$Lang['Btn']['ApplyToAll'] = '全部套用';
$Lang['Btn']['Back'] = '返回';
$Lang['Btn']['Enable'] = '啟用';
$Lang['Btn']['Disable'] = '停用';
$Lang['Btn']['Search'] = '尋找';
$Lang['Btn']['Print'] = '列印';
$Lang['Btn']['Continue'] = '繼續';
$Lang['Btn']['Confirm'] = "確定";
$Lang['Btn']['ShowOption'] = "顯示選項";
$Lang['Btn']['HideOption'] = "隱藏選項";
$Lang['Btn']['ShowSection'] = "顯示此部分";
$Lang['Btn']['HideSection'] = "隱藏此部分";
$Lang['Btn']['Build'] = "建立";
$Lang['Btn']['Load'] = "載入";
$Lang['Btn']['DeleteAll'] = "刪除全部";
$Lang['Btn']['Expand'] = "擴張";
$Lang['Btn']['Copy'] = "複製";
$Lang['Btn']['Preview'] = "預覽";
$Lang['Btn']['Next'] = "下一步";
$Lang['Btn']['Approve'] = "批核";
$Lang['Btn']['Reject'] = "拒絕";
$Lang['Btn']['AssignToStudent'] = "指派到學生";
$Lang['Btn']['UnassignToStudent'] = "取消指派到學生";
$Lang['Btn']['ToTop'] = "置頂";
$Lang['Btn']['Undo'] = "還原";
$Lang['Btn']['Set'] = "指定";
$Lang['Btn']['AttachFiles'] = "加入附件";
//$Lang['Btn']['RemoveSelected'] = "刪除";
$Lang['Btn']['WaitingForApproval'] = "等候批核";
$Lang['Btn']['Redo'] = "重做";
$Lang['Btn']['Send'] = "傳送";
$Lang['Btn']['Sending'] = "傳送中";
$Lang['Btn']['AddTo'] = "<< 增加";
$Lang['Btn']['DeleteTo'] = "刪除 >>";
$Lang['Btn']['PrintAll'] = "全部列印";
$Lang['Btn']['Upload'] = "上載";
$Lang['Btn']['Student'] = "學生";
$Lang['Btn']['Comment'] = "評語";
$Lang['Btn']['PrintEnvelope'] = "列印信封";
$Lang['Btn']['Setting'] = "設定";
$Lang['Btn']['ExportPrioity'] = "匯出優先次序";
$Lang['Btn']['AddAllWithArrow'] = '<< 增加全部';
$Lang['Btn']['AddWithArrow'] = '< 增加';
$Lang['Btn']['DeleteAllWithArrow'] = '刪除全部 >>';
$Lang['Btn']['DeleteWithArrow'] = '刪除 >';

$Lang['General']['Loading'] = '載入中...';
$Lang['General']['Yes'] = '是';
$Lang['General']['No'] = '否';
$Lang['General']['Yes2'] = '有';
$Lang['General']['No2'] = '沒有	';
$Lang['General']['Allow'] = '允許';
$Lang['General']['NotAllow'] = '不允許';
$Lang['General']['More'] = '更多';
$Lang['General']['SystemGenerated'] = '系統管理員';
$Lang['General']['eClassUserCode'] = 'eClass 帳戶碼';
$Lang['General']['ReturnMessage']['AddSuccess'] = '1|=|紀錄已經新增';
$Lang['General']['ReturnMessage']['UpdateSuccess'] = '1|=|紀錄已經更新';
$Lang['General']['ReturnMessage']['UpdatePartiallySuccess'] = "1|=|部份紀錄更新成功";
$Lang['General']['ReturnMessage']['DeleteSuccess'] = '1|=|紀錄已經刪除';
$Lang['General']['ReturnMessage']['AddUnsuccess'] = '0|=|新增紀錄失敗';
$Lang['General']['ReturnMessage']['UpdateUnsuccess'] = '0|=|更新紀錄失敗';
$Lang['General']['ReturnMessage']['DeleteUnsuccess'] = '0|=|刪除紀錄失敗';
$Lang['General']['ReturnMessage']['DeletePartiallySuccess'] = '0|=|刪除部份紀錄成功';
$Lang['General']['ReturnMessage']['EmailUsed'] = '0|=|電郵已被其他匯入中的使用者使用';
$Lang['General']['ReturnMessage']['InvalidPassword'] = '0|=|密碼不符';
$Lang['General']['ReturnMessage']['WrongCSVHeader'] = '0|=|標題格式不正確';
$Lang['General']['ReturnMessage']['WrongFileFormat'] = '0|=|檔案格式錯誤。上載的檔案必須是.csv或.txt的檔案。';
$Lang['General']['ReturnMessage']['WrongCSVHeader'] = '0|=|標題格式不正確';
$Lang['General']['ReturnMessage']['WrongExcelHeader'] = '0|=|標題格式不正確';
$Lang['General']['ReturnMessage']['CSVFileNoData'] = '0|=|沒有資料於檔案內';
$Lang['General']['ReturnMessage']['AccessDenied'] = '0|=|訪問被拒絕';
$Lang['General']['ReturnMessage']['RecordDuplicated'] = '0|=|紀錄重複';
$Lang['General']['ReturnMessage']['ImageUploadFail'] =  '0|=|未能上載圖片';
$Lang['General']['ReturnMessage']['ImageUploadSuccess'] = '1|=|上載圖片成功';
$Lang['General']['ReturnMessage']['CSVUploadFail'] =  '0|=|未能上載 CSV 檔';
$Lang['General']['ReturnMessage']['EmailSent'] = "1|=|已寄出電郵";
$Lang['General']['ReturnMessage']['ImportSuccess'] = '1|=|匯入成功';
$Lang['General']['ReturnMessage']['ImportUnsuccess'] = '0|=|匯入失敗';
$Lang['General']['ReturnMessage']['ImportUnsuccess_IncorrectHeaderFormat'] = '0|=|標題格式不正確。';
$Lang['General']['ReturnMessage']['ImportUnsuccess_NoRecord'] = '0|=|檔案沒有資料。';
$Lang['General']['ReturnMessage']['FileNotExist'] = "0|=|檔案不存在";
$Lang['General']['ReturnMessage']['FileMoveSuccess'] = "1|=|檔案移動成功。";
$Lang['General']['ReturnMessage']['FileMoveUnsuccess'] = "0|=|檔案移動失敗。";
$Lang['General']['ReturnMessage']['DuplicateFolderName'] = "0|=|文件夾名稱相同。";
$Lang['General']['ReturnMessage']['UnzipSuccess'] = "1|=|檔案解壓成功。";
$Lang['General']['ReturnMessage']['UnzipUnsuccess'] = "0|=|檔案解壓失敗。";
$Lang['General']['ReturnMessage']['ArchivedSuccess'] = "1|=|紀錄存檔成功";
$Lang['General']['ReturnMessage']['ArchivedUnsuccess'] = "0|=|紀錄存檔失敗";
$Lang['General']['ReturnMessage']['ArchivedPartiallySuccess'] = "0|=|紀錄部份存檔成功";
$Lang['General']['ReturnMessage']['CopyAccessRightGroupSuccess'] = "1|=|成功從小組中匯入成小組";
$Lang['General']['ReturnMessage']['CopyAccessRightGroupUnsuccess'] = "0|=|從小組中匯入成小組失敗";
$Lang['General']['ReturnMessage']['RecordSaveSuccess'] = "1|=|儲存紀錄成功。";
$Lang['General']['ReturnMessage']['RecordSaveUnSuccess'] = "0|=|儲存紀錄失敗。";
$Lang['General']['ReturnMessage']['FileDeleteSuccess'] = "1|=|檔案已經刪除。";
$Lang['General']['ReturnMessage']['FileDeleteUnsuccess'] = "0|=|檔案刪除失敗。";
$Lang['General']['ReturnMessage']['RecordApproveSuccess'] = "1|=|批核紀錄成功。";
$Lang['General']['ReturnMessage']['RecordApproveUnSuccess'] = "0|=|批核紀錄失敗。";
$Lang['General']['ReturnMessage']['RecordRejectSuccess'] = "1|=|拒絕紀錄成功。";
$Lang['General']['ReturnMessage']['RecordRejectUnSuccess'] = "0|=|拒絕紀錄失敗。";
$Lang['General']['ReturnMessage']['SettingSaveSuccess'] = "1|=|設定儲存成功 。";
$Lang['General']['ReturnMessage']['SettingSaveFail'] = "0|=|設定儲存失敗。";
$Lang['General']['ReturnMessage']['Copied'] = "1|=|紀錄已經複製。";
$Lang['General']['ReturnMessage']['SMSsubmitted'] = "1|=|已送出短訊（請留意狀況）。";
$Lang['General']['ReturnMessage']['SMSsubmittedfailed'] = "0|=|短訊送出失敗。";
$Lang['General']['ReturnMessage']['NoAccessRightForThisItem'] = "0|=|你沒有權限修改這紀錄";
$Lang['General']['ReturnMessage']['FileSizeExceedLimit'] = "0|=|文件大小超出上限。";
$Lang['General']['ReturnMessage']['FileTypeIsNotSupported'] = "0|=|不支援此類文件。";
$Lang['General']['ReturnMessage']['FileTypeIsBanned'] = "0|=|文件類型不被允許上傳。";
$Lang['General']['ReturnMessage']['ZipFileContainsRestrictedFileType'] = "0|=|壓縮檔含有被禁止的文件類型。";

$Lang['General']['ReturnMessage']['NotiPadRichtextEditor'] = "由於 iPad 不支援富文本編輯器，本功能不可在這平台上運作。";

$Lang['General']['AjaxError'] = "存取資料時遇到錯誤,請重試!";
$Lang['General']['NoRecordFound'] = "找不到所需紀錄";
$Lang['General']['NoRecordAtThisMoment'] = "暫時仍未有任何紀錄";
$Lang['General']['Remark'] = '備註';
$Lang['General']['TotalRecord'] = '紀錄總數';
$Lang['General']['SuccessfulRecord'] = '正確紀錄';
$Lang['General']['Search'] = '搜尋';
$Lang['General']['UploadFiles'] = '上傳檔案';
$Lang['General']['RemainingQuota'] = '尚餘容量';
$Lang['General']['FailureRecord'] = '錯誤紀錄';
$Lang['General']['SourceFile'] = '資料檔案';
$Lang['General']['CSVFileFormat'] = '(.csv 或 .txt 檔案)';
$Lang['General']['MARC21FileFormat'] = '(.txt 檔案)';
$Lang['General']['ImportMARC21'] = 'MARC21資料檔';
$Lang['General']['CSVSample'] = '範本檔案';
$Lang['General']['ClickHereToDownloadSample'] = '按此下載範例';
$Lang['General']['ClickToEdit'] = '按一下進行編輯';
$Lang['General']['Record'] = '紀錄';
$Lang['General']['Date'] = '日期';
$Lang['General']['Time'] = '時間';
$Lang['General']['warnSelectcsvFile'] = '請選擇匯入的檔案';
$Lang['General']['NoError'] = "沒有錯誤";
$Lang['General']['Error'] = "錯誤";
$Lang['General']['warnNoRecordInserted'] = "資料未被儲存，請更正錯誤後再試";
$Lang['General']['Enabled'] = '已啟用';
$Lang['General']['Disabled'] = '已停用';
$Lang['General']['LastModifiedBy'] = "最後修改用戶";
$Lang['General']['LastModified'] = "最後修改日期";
$Lang['General']['LastModified2'] = "最後修改";
$Lang['General']['LastUpdatedBy'] = "最後更新用戶";
$Lang['General']['LastUpdatedTime'] = "最後更新時間";
$Lang['General']['InputBy'] = "輸入用戶";
$Lang['General']['DateInput'] = "輸入日期";
$Lang['General']['Name'] = "名稱";
$Lang['General']['ChineseName'] = "中文名";
$Lang['General']['EnglishName'] = "英文名";
$Lang['General']['ChineseName2'] = "中文名稱";
$Lang['General']['EnglishName2'] = "英文名稱";
$Lang['General']['Sex'] = "性別";
$Lang['General']['InvalidDateFormat'] = "日期格式不符";
$Lang['General']['RequiredField'] = "附有「<span class='tabletextrequire'>*</span>」的項目必須填寫";
$Lang['General']['ReferenceField'] = "附有「<span class='tabletextrequire'>^</span>」的項目為參考用途";
$Lang['General']['InactiveStudentField'] = "附有「<span class='tabletextrequire'>^</span>」的用戶為已停用的用戶或沒有分班的學生";
$Lang['General']['ArchiveStudentField'] = "<span class='tabletextrequire'>*</span> 表示該用戶已被刪除或已離校。";
$Lang['General']['LeftStudentField'] = "<span class='tabletextrequire'>#</span> 表示該用戶已離校。";
$Lang['General']['GroupAdminField'] = "附有「<span class='tabletextrequire'>*</span>」的學生為小組管理員";
$Lang['General']['Default'] = "預設";
$Lang['General']['SetAsDefault'] = "設為預設選項";
$Lang['General']['Custom'] = "自訂";
$Lang['General']['Help'] = "說明";
$Lang['General']['Today'] = '今天';
$Lang['General']['DaysAgo'] = '日前';
$Lang['General']['LastModifiedInfoRemark'] = '最近更新：由<!--LastModifiedBy-->於<!--DaysAgo-->';
$Lang['General']['Warning'] = '警告';
$Lang['General']['Note'] = '注意';
$Lang['General']['Caution'] = '注意';
$Lang['General']['English'] = '英文';
$Lang['General']['Chinese'] = '中文';
$Lang['General']['All'] = '全部';
$Lang['General']['To'] = '至';
$Lang['General']['And'] = '及';
$Lang['General']['Or'] = '或';
$Lang['General']['EmptySymbol'] = '--';
$Lang['General']['ImportContentSeparator'] = '###';
$Lang['General']['Show'] = '顯示';
$Lang['General']['Hide'] = '隱藏';
$Lang['General']['Percentage'] = '百分比';
$Lang['General']['Total'] = '總數';
$Lang['General']['DeleteAllWarning'] = '以下<!--ItemName-->將被永久移除，而且無法恢復。如確定要刪除，請按<font style="font-size:large"><!--ButtonName--></font>。';
$Lang['General']['Status'] = '狀態';
$Lang['General']['Status2'] = '狀況';
$Lang['General']['NotApplicable'] = '不適用';
$Lang['General']['SelectChildren'] = '選擇兒女';
$Lang['General']['Others'] = "其他";
$Lang['General']['ChooseUser'] = "選擇用戶";
$Lang['General']['SelectedUser'] = "已選擇用戶";
$Lang['General']['FromClassOrGroup'] = "由 <b>班別</b> / <b>小組</b>";
$Lang['General']['FromGroup'] = "由 <b>小組</b>";
$Lang['General']['SearchByLoginID'] = "以登入名稱搜尋";
$Lang['General']['Important'] = "重要事項";
$Lang['General']['ClassNumber'] = "學號";
$Lang['General']['Approved'] = "已批核";
$Lang['General']['NotApproved'] = "未批核";
$Lang['General']['Rejected'] = "已拒絕";
$Lang['General']['Pending'] = "等候批核";
$Lang['General']['Paid'] = "已繳交";
$Lang['General']['NotPaid'] = "未繳交";
$Lang['General']['NoNeedPaid'] = "不需繳交";
$Lang['General']['Mark'] = "分數";
$Lang['General']['Grade'] = "等級";
$Lang['General']['Tips'] = "提示";
$Lang['General']['FileNames'] = "檔案名稱";
$Lang['General']['Display'] = "每頁顯示";
$Lang['General']['Expand'] = "展開所有";
$Lang['General']['PleaseSelect'] = "請選擇";
$Lang['General']['PleaseFillIn'] = "請輸入";
$Lang['General']['Start'] = "開始";
$Lang['General']['End'] = "結束";
$Lang['General']['StartDate'] = "開始日期";
$Lang['General']['EndDate'] = "結束日期";
$Lang['General']['SortByField'] = "以<!--FieldName-->排序";
$Lang['General']['FirstPage'] = "首頁";
$Lang['General']['LastPage'] = "尾頁";
$Lang['General']['ChooseMember'] = "選擇成員";
$Lang['General']['Public'] = "公開";
$Lang['General']['Private'] = "隱藏";
$Lang['General']['Teacher'] = "老師";
$Lang['General']['Sequence'] = "次序";
$Lang['General']['Reason'] = "原因";
$Lang['General']['Details'] = "詳細內容";
$Lang['General']['Submitting...'] = "呈送中...";
$Lang['General']['Sending...'] = "傳送中...";
$Lang['General']['Saving...'] = "儲存中...";
$Lang['General']['Generating...'] = "製作中...";
$Lang['General']['YesNo'] = '(Y - 是; N - 否)';
$Lang['General']['On'] = " 於 ";
$Lang['General']['Order'] = "次序";

$Lang['Import']['Method']['Title'] = "匯入方式";
$Lang['Import']['Method']['UserLogin'] = "根據用戶名稱匯入";
$Lang['Import']['Method']['STRN'] = "根據STRN匯入";

$Lang['General']['DayType4'][0] = "日";
$Lang['General']['DayType4'][1] = "一";
$Lang['General']['DayType4'][2] = "二";
$Lang['General']['DayType4'][3] = "三";
$Lang['General']['DayType4'][4] = "四";
$Lang['General']['DayType4'][5] = "五";
$Lang['General']['DayType4'][6] = "六";
$Lang['General']['DayType4'][7] = "日";

$Lang['General']['month'][0] = "十二月";
$Lang['General']['month'][1] = "一月";
$Lang['General']['month'][2] = "二月";
$Lang['General']['month'][3] = "三月";
$Lang['General']['month'][4] = "四月";
$Lang['General']['month'][5] = "五月";
$Lang['General']['month'][6] = "六月";
$Lang['General']['month'][7] = "七月";
$Lang['General']['month'][8] = "八月";
$Lang['General']['month'][9] = "九月";
$Lang['General']['month'][10] = "十月";
$Lang['General']['month'][11] = "十一月";
$Lang['General']['month'][12] = "十二月";
$Lang['General']['month'][13] = "一月";
$Lang['General']['Month'] = "月";

$Lang['General']['SchoolYear'] = "學年";
$Lang['General']['Term'] = "學期";
$Lang['General']['TermName'] = "學期名稱";
$Lang['General']['YearTermID'] = "學期代號";
$Lang['General']['Steps'] = "步驟";
$Lang['General']['SubjectPanel'] = "科主任";

$Lang['General']['FormFieldLabel']['Ch'] = "(中文)";
$Lang['General']['FormFieldLabel']['En'] = "(ENG)";

$Lang['General']['ImportOtherRecords'] = "匯入其他紀錄";
$Lang['General']['ImportArr']['Reference']['En'] = "[Ref]";
$Lang['General']['ImportArr']['Reference']['Ch'] = "[參考用途]";
$Lang['General']['ImportArr']['Optional']['En'] = "[Optional]";
$Lang['General']['ImportArr']['Optional']['Ch'] = "[非必須的]";
$Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'] = "選擇CSV檔案";
$Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'] = "資料驗證";
$Lang['General']['ImportArr']['ImportStepArr']['ImportResult'] = "匯入結果";
$Lang['General']['ImportArr']['DataColumn'] = "資料欄";
$Lang['General']['ImportArr']['Column'] = "欄位";
$Lang['General']['ImportArr']['Row'] = "行#";
$Lang['General']['ImportArr']['ImportSymbolArr']['Required'] = "*";
$Lang['General']['ImportArr']['ImportSymbolArr']['Reference'] = "^";
$Lang['General']['ImportArr']['ImportSymbolArr']['Other'] = "#";
$Lang['General']['ImportArr']['RecordsImportedSuccessfully'] = "個資料匯入成功";
$Lang['General']['ImportArr']['FirstRowWarn'] = "請注意不要刪除匯入檔的首行。";
$Lang['General']['ImportArr']['SecondRowWarn'] = "請注意不要刪除匯入檔的首兩行。";
$Lang['General']['ImportArr']['RecordsValidated'] = "已核對 <!--NumOfRecords--> 項紀錄";
$Lang['General']['ImportArr']['RecordsProcessed'] = "已處理 <!--NumOfRecords--> 項紀錄";
$Lang['General']['ImportArr']['ClassNameClassNum_WebSAMS'] = '請提供「班別及學號」或「WebSAMS學生註冊編號」以識別學生';
$Lang['SysMgr']['Timetable']['TeacherName'] = "老師姓名";
$Lang['SysMgr']['Timetable']['TeacherLoginID'] = "老師內聯網帳號";

$Lang['General']['ImportWarningArr']['WrongWebSamsRegNo'] = "沒有此用戶";
$Lang['General']['ImportWarningArr']['WrongUserLogin'] = "沒有此用戶";
$Lang['General']['ImportWarningArr']['WrongClassNameNumber'] = "沒有此用戶";
$Lang['General']['ImportWarningArr']['EmptyClassNumber'] = "班號空白";
$Lang['General']['ImportWarningArr']['EmptyClassName'] = "班別空白";
$Lang['General']['ImportWarningArr']['EmptyStudentData'] = "學生資料空白";


$Lang['General']['PageNumber'] = "頁數";
$Lang['General']['Type'] = "類型";
$Lang['General']['UploadDate'] = "上傳日期";
$Lang['General']['Used'] = "已用";
$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile'] = "你確定要刪除此檔案？";
$Lang['General']['__Warning']['AreYouSureYouWantToDeleteSelectedRecord'] = "你確定要刪除已選紀錄？";
$Lang['General']['PleaseSelectFiles'] = "請選擇檔案";
$Lang['General']['File'] = "檔案";
$Lang['General']['UploadMoreFiles'] = "上載更多檔案";

$Lang['General']['StudentInternalRemark'] = "學生備註";
$Lang['General']['Record_s'] = "紀錄";
$Lang['General']['Instruction'] = "使用指引";
$Lang['General']['Completed'] = "已完成";
$Lang['General']['WholeYear'] = "全年";
$Lang['General']['Annual'] = "全年";
$Lang['General']['Overall'] = "總分";
$Lang['General']['From'] = "由";
$Lang['General']['To'] = "至";
//$Lang['General']['Record'] = "記錄"; # commented by Henry Chow, since duplicated
$Lang['General']['NA'] = "不設定";
$Lang['General']['Code'] = "代號";
$Lang['General']['InputCode'] = "輸入代號";

$Lang['General']['UserAccountNotExists'] = "此用戶不存在";
$Lang['General']['Settings'] = "設定";
$Lang['General']['ImportColumn'] = "欄位";
$Lang['General']['Beta'] = "試用版";
$Lang['General']['Female'] = "女";
$Lang['General']['Male'] = "男";
$Lang['General']['UserLogin'] = "內聯網帳號";
$Lang['General']['SystemProperties']  = "系統特性";
$Lang['General']['AccessDenied'] = "訪問被拒絕";

# genearl javascript warning message
$Lang['General']['JS_warning']['InputPositiveInteger'] = "請輸入正整數。";
$Lang['General']['JS_warning']['SelectAtLeastOneRecord'] = "請選擇至少一項資料。";
$Lang['General']['JS_warning']['InputEnglishName'] = "請輸入英文名稱。";
$Lang['General']['JS_warning']['InputChineseName'] = "請輸入中文名稱。";
$Lang['General']['JS_warning']['InputPositiveValue'] = "請輸入正數值。";
$Lang['General']['JS_warning']['SelectAForm'] = "請選擇級別。";
$Lang['General']['JS_warning']['InputCode'] = "請輸入代號。";
$Lang['General']['JS_warning']['CodeIsInUse'] = "此代號已被使用。";
$Lang['General']['JS_warning']['CodeMustBeginWithLetter'] = "代號開首必須為英文字母。";
$Lang['General']['JS_warning']['CodeCannotStartWithZero'] = "代號開首不能是「0」。";
$Lang['General']['JS_warning']['InputName'] = "請輸入名稱。";
$Lang['General']['JS_warning']['InputTitle'] = "請輸入標題。";
$Lang['General']['JS_warning']['SelectDate'] = "請選擇日子。";
$Lang['General']['JS_warning']['CannotBeBlank'] = "此欄不能留空。";
$Lang['General']['JS_warning']['MustBePositiveNumber'] = "此欄必須為正數。";
$Lang['General']['JS_warning']['ConfirmDelete'] = '是否確定要移除？';
$Lang['General']['JS_warning']['InvalidDateFormat'] = "日期格式錯誤";
$Lang['General']['JS_warning']['InvalidDateRange'] = "日期範圍無效。";
$Lang['General']['JS_warning']['SelectStudent'] = "請選擇學生。";
$Lang['General']['JS_warning']['SelectAtLeastOneStudent'] = "請選擇至少一位學生。";
$Lang['General']['JS_warning']['SelectAtLeastOneTeacher'] = "請選擇至少一位老師。";
$Lang['General']['JS_warning']['InvalidChineseCommercialCode'] = "中文商用電碼無效";
$Lang['General']['JS_warning']['InvalidHomeTel'] = "住宅電話號碼無效";
$Lang['General']['JS_warning']['InvalidMobile'] = "手提電話號碼無效";
$Lang['General']['JS_warning']['SelectClasses'] = "請選擇班別。";
$Lang['General']['JS_warning']['SelectClass'] = "請選擇班別。";
$Lang['General']['JS_warning']['FormIsChanged'] = "表格已被改動，你是否需要在返回之前先儲存表格內容？";
$Lang['General']['JS_warning']['SelectColumn'] = "請選擇顯示欄位。";
$Lang['General']['JS_warning']['SelectSchoolYear'] = "請選擇學年。";
$Lang['General']['JS_warning']['InputRole']="請輸入角色";
$Lang['General']['JS_warning']['RoleIsInUse'] = "角色已被使用";
$Lang['General']['JS_warning']['InputGradeWeight'] ="請輸入自訂成績比重";
$Lang['General']['JS_warning']['InputYearWeight'] ="請輸入參考學年比重";

$Lang['General']['InputEitherOneField'] = "附有「<span class='tabletextrequire'>&</span>」的項目只須輸入其中一項資料";
$Lang['General']['EnterBothOrEitherOne'] = "可只輸入其中一項, 或所有項目";

$Lang['General']['EmptyTableMsg']['NoClass'] = "未有班別資料，請到「學校基本設定」或聯絡系統管理員完成有關設定。";
$Lang['General']['EmptyTableMsg']['NoSubject'] = "未有學科資料，請到「學校基本設定」或聯絡系統管理員完成有關設定。";

$Lang['General']['RecordDate'] = "紀錄日期";
$Lang['General']['CreatedBy'] = "紀錄建立者";
$Lang['General']['AcademicYear'] = "學年";
$Lang['General']['ViewFormat'] = "檢視格式";
$Lang['General']['HTML'] = "HTML";
$Lang['General']['CSV'] = "CSV";
$Lang['General']['Semester'] = "學期";
$Lang['General']['DataFormat'] = "資料格式";
$Lang['General']['Symblo'] = "符號";
$Lang['General']['Symbol'] = "符號";

$Lang['General']['WarningArr']['InputDate'] = "請輸入日期";
$Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'] = "結束日期不能早於開始日期";
$Lang['General']['WarningArr']['EndPeriodCannotEarlierThanStartPeriod'] = "結束時段不能早於開始時段";
$Lang['General']['WarningArr']['PleaseSelectPeriod'] = "請選擇時段";
$Lang['General']['WarningArr']['PleaseSelectAtLeastOneForm'] = "請最少選擇一個級別";
$Lang['General']['WarningArr']['PleaseSelectAtLeastOneTeacher'] = "請最少選擇一位老師";
$Lang['General']['WarningArr']['PleaseSelectAtLeastOneSubjectGroup'] = "請最少選擇一個科組";
$Lang['General']['WarningArr']['PleaseInputUserLogin'] = "請輸入帳戶名稱";
$Lang['General']['WarningArr']['UserLoginInUse'] = "此帳戶名稱已被使用";
$Lang['General']['WarningArr']['PleaseInputPassword'] = "請輸入密碼";
$Lang['General']['PrintList'] = "列印列表";
$Lang['General']['RefNo'] = "參考編號";
$Lang['General']['DeletedBy'] = "刪除者";
$Lang['General']['DeletedDate'] = "刪除日期";
$Lang['General']['CustomizedSettings'] = "自定設定";
$Lang['General']['_Invalid'] = "無效。";

$Lang['General']['Parent'] = "家長";
$Lang['General']['Children'] = "子女";
$Lang['General']['NoClass'] = "沒有班別";
$Lang['General']['InvalidData'] = "資料無效";
$Lang['General']['ParentUsingEClassApp'] = "使用eClass App的家長";
$Lang['General']['UserType'] = "用戶類型";
$Lang['General']['MemberType'] = "組員類型";
$Lang['General']['Admin'] = "管理員";
$Lang['General']['Member'] = "一般成員";
$Lang['General']['Member2'] = "成員";
$Lang['General']['FlippedChannels'] = '翻轉頻道';
$Lang['General']['StudentWithClass'] = '已編班學生';
$Lang['General']['StudentWithoutClass'] = '未編班學生';
$Lang['General']['OtherField'] = "附有「<span class='tabletextrequire'>#</span>」的項目，請使用「班別及學號」或「內聯網帳號」或「WebSAMS學生註冊編號」以識別用戶";
$Lang['General']['Misc'] = "其他資料";
$Lang['General']['LeftStudents'] = "已離校的學生";
$Lang['General']['SystemAdmin'] = "系統管理員";
$Lang['General']['StudentNameEng'] = '學生姓名 (英)';
$Lang['General']['StudentNameChi'] = '學生姓名 (中)';

$Lang['General']['Email']='電子郵件';
$Lang['General']['PushMessage']='推播通知';

$Lang['General']['AM'] = '上午';
$Lang['General']['PM'] = '下午';
# add $Lang['General']['StudentName'],$Lang['General']['UserLogin'],$Lang['General']['Photo'] for [E131184]
$Lang['General']['StudentName'] = '學生姓名';
$Lang['General']['UserLogin'] = '登入編號';
$Lang['General']['Photo'] = '相片';
$Lang['General']['StudentPhotos'] = '學生相片';
$Lang['General']['searchAndInsertUser']='搜尋及新增用戶';
$Lang['General']['msgSearchAndInsertInfo'] ='輸入一行一個搜尋記錄  <br>"[內聯網帳號]"';

# Popup select member page (/home/common_choose/)
$Lang['CommonChoose']['IdentitySelectOptionLabel'] = '---------- 身份 ----------';
$Lang['CommonChoose']['GroupCategorySelectOptionLabel'] = '-------- 小組類別 --------';
$Lang['CommonChoose']['InternalRecipientGroupSelectOptionLabel'] = '-------- 內聯網收件組別 --------';
$Lang['CommonChoose']['SelectTarget'] = '選擇對象';
$Lang['CommonChoose']['PageTitle']['SelectMembers'] = '選擇成員';
$Lang['CommonChoose']['PageTitle']['SelectAudience'] = '選擇適用對象';
$Lang['CommonChoose']['PageTitle']['SelectRecipients'] = '選擇接收人';
$Lang['CommonChoose']['SelectTargetField']['TeacherStaff'] = "請選擇教學及非教學職員";
$Lang['CommonChoose']['SelectTargetField']['StudentTeacherStaff'] = "請選擇學生、教學及非教學職員";
$Lang['CommonChoose']['SearchByInputFormat'] = "輸入 [班名][學號] 或 [學生姓名]";
$Lang['CommonChoose']['SearchParentByInputFormat'] = '以家長名稱搜尋';
$Lang['CommonChoose']['SearchByLoginID'] = "以登入名稱搜尋";
$Lang['CommonChoose']['FilterAppParent'] = "隱藏使用eClass App的家長";

$Lang['Personal']['OfficialPhoto'] = '學生相片';
$Lang['Personal']['DeleteOfficialPhoto'] = '刪除現時學生相片';
$Lang['Personal']['PersonalPhoto'] = '個人相片';
$Lang['Personal']['DeletePersonalPhoto'] = '刪除現時個人相片';

$Lang['SysMgr']['FormClassMapping']['Import'] = '匯入';
$Lang['SysMgr']['FormClassMapping']['Export'] = '匯出';
$Lang['SysMgr']['FormClassMapping']['AddClass'] = '新增班別';
$Lang['SysMgr']['FormClassMapping']['NewForm'] = '新增級別';
$Lang['SysMgr']['FormClassMapping']['EditForm'] = '新增／編輯級別';
$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'] = '(按下CTRL鍵並點選項目以選擇多項。)';
$Lang['SysMgr']['FormClassMapping']['StudentClassNumber'] = '學號';
$Lang['SysMgr']['FormClassMapping']['TargetClassNumber'] = '目標學號';
$Lang['SysMgr']['FormClassMapping']['ClassNumFollowName'] = '根據學生英文名字字母序從新編排';
$Lang['SysMgr']['FormClassMapping']['ClassNumFollowTime'] = '新增學號到現存學號之後';
$Lang['SysMgr']['FormClassMapping']['ClassNumSelectWarning'] = '請選擇學號計算方法';
$Lang['SysMgr']['FormClassMapping']['NewClass'] = '新增班別';
$Lang['SysMgr']['FormClassMapping']['ClassTitleEN'] = '班別名稱(英文)';
$Lang['SysMgr']['FormClassMapping']['TargetClassTitleEN'] = '目標班別名稱(英文)';
$Lang['SysMgr']['FormClassMapping']['ClassTitleB5'] = '班別名稱(中文)';
$Lang['SysMgr']['FormClassMapping']['ClassTeacher'] = '班主任';
$Lang['SysMgr']['FormClassMapping']['AddTeacher'] = '- 選擇老師 -';
$Lang['SysMgr']['FormClassMapping']['AddFromFormClass'] = '- 由級別 / 班別新增 -';
$Lang['SysMgr']['FormClassMapping']['AddFromClass'] = '- 由班別新增 -';
$Lang['SysMgr']['FormClassMapping']['ClassStudent'] = '學生';
$Lang['SysMgr']['FormClassMapping']['StudentSelected'] = '已選學生';
$Lang['SysMgr']['FormClassMapping']['SelectSchoolYear'] = '- 選擇學年 -';
$Lang['SysMgr']['FormClassMapping']['SelectSchoolYearTerm'] = '- 選擇學期 -';
$Lang['SysMgr']['FormClassMapping']['AllTerm'] = '全部學期';
$Lang['SysMgr']['FormClassMapping']['Form'] = '級別';
$Lang['SysMgr']['FormClassMapping']['FormSetting'] = '級別設定';
$Lang['SysMgr']['FormClassMapping']['Class'] = '班別';
$Lang['SysMgr']['FormClassMapping']['NumberOfStudent'] = '學生數量';
$Lang['SysMgr']['FormClassMapping']['SubjectTaken'] = '修讀科目數量';
$Lang['SysMgr']['FormClassMapping']['NameOfClassEng'] = 'Name of class (English)';
$Lang['SysMgr']['FormClassMapping']['NameOfClassChi'] = 'Name of class (Chinese)';
$Lang['SysMgr']['FormClassMapping']['NameOfClass'] = 'Name of class';
$Lang['SysMgr']['FormClassMapping']['Delete'] = '移取';
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCode'] = 'WebSAMS 代號';
$Lang['SysMgr']['FormClassMapping']['ClickToEdit'] = $Lang['General']['ClickToEdit'];
$Lang['SysMgr']['FormClassMapping']['Move'] = '移動';
$Lang['SysMgr']['FormClassMapping']['ClassPageTitle'] = '班別';
$Lang['SysMgr']['FormClassMapping']['ClassPageSubTitle'] = '班別列表';
$Lang['SysMgr']['FormClassMapping']['Or'] = '或';
$Lang['SysMgr']['FormClassMapping']['SearchStudent'] = '自動搜尋結果新增(輸入學生姓名或登入名稱或電郵地址進行搜尋)';
$Lang['SysMgr']['FormClassMapping']['ClassInfo'] = '班別資料';
$Lang['SysMgr']['FormClassMapping']['Print'] = '列印';
$Lang['SysMgr']['FormClassMapping']['BasicClassInfo'] = '- 基本資料 -';
$Lang['SysMgr']['FormClassMapping']['RemoveThisClass'] = '移除此班別';
$Lang['SysMgr']['FormClassMapping']['EditThisClass'] = '編輯此班別';
$Lang['SysMgr']['FormClassMapping']['EditClass'] = '更新班別';
$Lang['SysMgr']['FormClassMapping']['ClassTitle'] = '班別名稱';
$Lang['SysMgr']['FormClassMapping']['ClassNo'] = '學號';
$Lang['SysMgr']['FormClassMapping']['StudentName'] = '姓名';
$Lang['SysMgr']['FormClassMapping']['ClassNumberNotYetSet'] = '未編排';
$Lang['SysMgr']['FormClassMapping']['DeleteClassWarning'] = '移除班別將一併移除相關之學生、班主任、及科目修讀情況資料。此操作不可回復，你是否確定要繼續？';
$Lang['SysMgr']['FormClassMapping']['DeleteFormWarning'] = '是否確定要移除此級別？';
$Lang['SysMgr']['FormClassMapping']['SelectYearTerm'] = '- 選擇學期 -';
$Lang['SysMgr']['FormClassMapping']['Title'] = '名稱';
$Lang['SysMgr']['FormClassMapping']['SelectPreviousYearClass'] = '- 自去年班別新增 -';
$Lang['SysMgr']['FormClassMapping']['SelectStudentWithoutClass'] = '- 自沒有分班學生 -';
$Lang['SysMgr']['FormClassMapping']['SubjectTakenPageTitle'] = '詳細科目修讀情況';
$Lang['SysMgr']['FormClassMapping']['SubjectsApplied'] = '正修讀科目';
$Lang['SysMgr']['FormClassMapping']['Total'] = '總數';
$Lang['SysMgr']['FormClassMapping']['ReferenceInfo'] = '- 參考資料 -';
$Lang['SysMgr']['FormClassMapping']['ViewDetails'] = '檢視詳細資料';
$Lang['SysMgr']['FormClassMapping']['RelatedSubjects'] = '科目修讀情況';
$Lang['SysMgr']['FormClassMapping']['Subject'] = '科目';
$Lang['SysMgr']['FormClassMapping']['NoOfStudentsTaken'] = '選修學生數量';
$Lang['SysMgr']['FormClassMapping']['ClassDeleteSuccess'] = '1|=|班別移除成功.';
$Lang['SysMgr']['FormClassMapping']['ClassDeleteUnsuccess'] = '0|=|班別移除失敗.';
$Lang['SysMgr']['FormClassMapping']['ClassCreateSuccess'] = '1|=|班別新增成功.';
$Lang['SysMgr']['FormClassMapping']['ClassCreateUnsuccess'] = '0|=|班別新增失敗.';
$Lang['SysMgr']['FormClassMapping']['ClassUpdateSuccess'] = '1|=|班別更新成功.';
$Lang['SysMgr']['FormClassMapping']['ClassUpdateUnsuccess'] = '0|=|班別更新失敗.';
$Lang['SysMgr']['FormClassMapping']['AllClass'] = '所有班別';
$Lang['SysMgr']['FormClassMapping']['FormCreateSuccess'] = '1|=|級別新增成功.';
$Lang['SysMgr']['FormClassMapping']['FormCreateUnsuccess'] = '0|=|級別新增失敗.';
$Lang['SysMgr']['FormClassMapping']['FormDeleteSuccess'] = '1|=|級別移除成功.';
$Lang['SysMgr']['FormClassMapping']['FormDeleteUnsuccess'] = '0|=|級別移除失敗.';
$Lang['SysMgr']['FormClassMapping']['FormNameWarning'] = '*級別名稱重複或空白.';
$Lang['SysMgr']['FormClassMapping']['ClassTitleWarning'] = '*請填上班別名稱.';
$Lang['SysMgr']['FormClassMapping']['FormEditSuccess'] = '1|=|已儲存變更。';
$Lang['SysMgr']['FormClassMapping']['FormEditUnsuccess'] = '0|=|級別更改失敗.';
$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] = '<font style="color:red;">*</font> 表示該用戶已被刪除或已離校。';
$Lang['SysMgr']['FormClassMapping']['ClassTitleDuplicationWarning'] = '<font style="color:red;">*</font> 在同一學年下，班別名稱不可重複.';
$Lang['SysMgr']['FormClassMapping']['ClassTitleQuoteWarning'] = '*班別名稱不可包含 " 或 \\\'.';
$Lang['SysMgr']['FormClassMapping']['FormWebsamsCodeInvalid'] = "級別 WebSAMS 代號無效";
$Lang['SysMgr']['FormClassMapping']['MissingData'] = "資料不足";
$Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'] = "用戶不存在";
$Lang['SysMgr']['FormClassMapping']['StudentNotExist'] = "沒有此學生";
$Lang['SysMgr']['FormClassMapping']['NotActiveAccountWarning'] = "不是在用用戶";
$Lang['SysMgr']['FormClassMapping']['StudentNotInSelectedForm'] = "學生不屬於已選的級別";
$Lang['SysMgr']['FormClassMapping']['UserIsNotAStudent'] = "此用戶不是學生用戶";
$Lang['SysMgr']['FormClassMapping']['ClassNumberIntegerOnlyWarning'] = "學號只可包含數字";
$Lang['SysMgr']['FormClassMapping']['ClassNameQuoteWarning'] = "班別名稱不可包含 \" 或 '";
$Lang['SysMgr']['FormClassMapping']['ClassNotExistWarning'] = "班別不存在";
$Lang['SysMgr']['FormClassMapping']['ClassNotInSelectedForm'] = "班別不屬於已選的級別";
#Siuwan 20130823 Target Class Info Warning
$Lang['SysMgr']['FormClassMapping']['TargetClassNumberIntegerOnlyWarning'] = "目標學號只可包含數字";
$Lang['SysMgr']['FormClassMapping']['TargetClassNameQuoteWarning'] = "目標班別名稱不可包含 \" 或 '";
$Lang['SysMgr']['FormClassMapping']['TargetClassNotExistWarning'] = "目標班別不存在";
$Lang['SysMgr']['FormClassMapping']['TargetClassNotInSelectedForm'] = "目標班別不屬於已選的級別";

$Lang['SysMgr']['FormClassMapping']['FormNotExistWarning'] = "級別不存在";
$Lang['SysMgr']['FormClassMapping']['ImportFormClass'] = "匯入級別及班別";
$Lang['SysMgr']['FormClassMapping']['ImportClassStudent'] = "匯入班學生";
$Lang['SysMgr']['FormClassMapping']['ImportClassTeacher'] = "匯入班主任";
$Lang['SysMgr']['FormClassMapping']['SchoolYear'] = "學年";
$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'] = "個資料匯入成功";
$Lang['SysMgr']['FormClassMapping']['StudentLoginID'] = "學生登入編號";
$Lang['SysMgr']['FormClassMapping']['CheckingClassAndClassNumberConflict'] = "正在檢查班別及學號是否有衝突";
$Lang['SysMgr']['FormClassMapping']['ClassProcessed'] = "已處理<!--NumOfRecords-->個班別資料";
$Lang['SysMgr']['FormClassMapping']['ImportClassStudentSuccess'] = "已成功匯入班學生。";
$Lang['SysMgr']['FormClassMapping']['ImportClassStudentFailed'] = "匯入班學生失敗。";
$Lang['SysMgr']['FormClassMapping']['ImportClassStudentWarning'] = "系統會根據最後匯入的資料更新學生的班別及班號，但更新資料必須為同一級別。";
$Lang['SysMgr']['FormClassMapping']['ImportClassStudentForOneFormOnly'] = "每次只可匯入同一級別的學生。";
$Lang['SysMgr']['FormClassMapping']['ImportClassStudentFieldRemark'] = "請輸入「學生內聯網帳號」(欄位 1) 或「班別(英文) 及 班號」(欄位 3 及 4) 以作識別學生。";
$Lang['SysMgr']['FormClassMapping']['ImportTeacherWarning'] = "匯入的老師會覆蓋現有同名的「老師內聯網帳號」的相關班主任紀錄。";
$Lang['SysMgr']['FormClassMapping']['ImportTeacherWarning2'] = "如老師為多班的班主任，請在csv / txt內建立新一列加入現存的老師資料及班別(英文)。";

# used in admin console
$Lang['ErrorMsg']['InvalidRegNo'] = "<font color=red>RegNo 編號無效。</font>";
$Lang['AdminConsole']['jsMovedToFrontEndUiAlertMsg'] = "行政管理中心已經不再支援加入及匯入用戶。請前往「學校行政管理工具 > 用戶管理」，進行有關操作。";
$Lang['AdminConsole']['SMSAlertMsg'] = "行政管理中心已經不再支援短訊服務功能。請前往「學校行政管理工具 > 一般事項管理 > 通訊中心」，進行有關操作。";


if($plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EE1'] = "EE1 - 特殊敎育";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EE2'] = "EE2 - 特殊敎育";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EE3'] = "EE3 - 特殊敎育";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EEP'] = "EEP - 特殊敎育";
}
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['K1'] = "K1 - 幼兒班";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['K2'] = "K2 - 幼稚園低班";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['K3'] = "K3 - 幼稚園高班";

if(!$plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['LP'] = "LP - 預科低班";
}

$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P1'] = "P1 - 小一";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P2'] = "P2 - 小二";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P3'] = "P3 - 小三";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P4'] = "P4 - 小四";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P5'] = "P5 - 小五";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P6'] = "P6 - 小六";

if($plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['1'] = "1 - 葡語小學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['2'] = "2 - 葡語小學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['3'] = "3 - 葡語小學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['4'] = "4 - 葡語小學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['5'] = "5 - 葡語小學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['6'] = "6 - 葡語小學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EP6'] = "EP6 - 英文中學";
}
if(!$plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PP'] = "PP - 小學預備班 - 特殊敎育";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PR'] = "PR - 小學 - 特殊敎育";
}

$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S1'] = "S1 - 中一";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S2'] = "S2 - 中二 ";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S3'] = "S3 - 中三";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S4'] = "S4 - 中四";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S5'] = "S5 - 中五";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S6'] = "S6 - 中六";
if(!$plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S7'] = "S7 - 中七";
}

if(!$plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SJ'] = "SJ - 初中";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SS'] = "SS - 高中";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['UP'] = "UP - 預科高班";
}

if($plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F1'] = "F1 - 英文中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F2'] = "F2 - 英文中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F3'] = "F3 - 英文中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F4'] = "F4 - 英文中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F5'] = "F5 - 英文中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F6'] = "F6 - 英文中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['7'] = "7 - 葡語中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['8'] = "8 - 葡語中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['9'] = "9 - 葡語中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['10'] = "10 - 葡語中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['11'] = "11 - 葡語中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['12'] = "12 - 葡語中學";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['ERP'] = "ERP - 小學回歸教育";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['ERG'] = "ERG - 初中回歸教育";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['ERC'] = "ERC - 高中回歸教育";
}

if($sys_custom['Form']['ExtraWEBSAMSCodeForSchoolSection']) {
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PC1'] = "PC1 - 小一 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PC2'] = "PC2 - 小二 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PC3'] = "PC3 - 小三 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PC4'] = "PC4 - 小四 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PC5'] = "PC5 - 小五 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PC6'] = "PC6 - 小六 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PE1'] = "PE1 - 小一 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PE2'] = "PE2 - 小二 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PE3'] = "PE3 - 小三 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PE4'] = "PE4 - 小四 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PE5'] = "PE5 - 小五 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PE6'] = "PE6 - 小六 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SC1'] = "SC1 - 中一 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SC2'] = "SC2 - 中二 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SC3'] = "SC3 - 中三 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SC4'] = "SC4 - 中四 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SC5'] = "SC5 - 中五 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SC6'] = "SC6 - 中六 (中文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SE1'] = "SE1 - 中一 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SE2'] = "SE2 - 中二 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SE3'] = "SE3 - 中三 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SE4'] = "SE4 - 中四 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SE5'] = "SE5 - 中五 (英文部)";
    $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SE6'] = "SE6 - 中六 (英文部)";
}

if($sys_custom['iPf']['CurriculumsSettings']) {
    $Lang['SysMgr']['FormClassMapping']['Curriculum'] = "課程";
    $Lang['SysMgr']['FormClassMapping']['Select']['Curriculum'] = "選擇課程";
}

if($sys_custom['iPf']['DBSTranscript']) {
    $Lang['SysMgr']['FormClassMapping']['StudentSubjectLevel'] = "學生科目課程程度";
}

$Lang['SysMgr']['FormClassMapping']['Select']['Form'] = '選擇級別';
$Lang['SysMgr']['FormClassMapping']['Select']['Class'] = '選擇班別';
$Lang['SysMgr']['FormClassMapping']['Select']['Term'] = '選擇學期';
$Lang['SysMgr']['FormClassMapping']['Select']['TeachingStaff'] = '選擇教師';
$Lang['SysMgr']['FormClassMapping']['Select']['NonTeachingStaff'] = '選擇非教務職員';

$Lang['SysMgr']['FormClassMapping']['All']['Class'] = '全部班別';
$Lang['SysMgr']['FormClassMapping']['All']['TeachingStaff'] = '全部教師';
$Lang['SysMgr']['FormClassMapping']['All']['NonTeachingStaff'] = '全部非教務職員';
$Lang['SysMgr']['FormClassMapping']['All']['Form'] = '全部級別';
$Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'] = '全部學年';


// subject class mapping
$Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'] = '學科';
$Lang['SysMgr']['SubjectClassMapping']['Subject'] = '學科';
$Lang['SysMgr']['SubjectClassMapping']['Group'] = '科組';
$Lang['SysMgr']['SubjectClassMapping']['ViewGroup'] = '檢視科組';
$Lang['SysMgr']['SubjectClassMapping']['Subject&ComponentTitle'] = '學科及學科分卷';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'] = '科組';
$Lang['SysMgr']['SubjectClassMapping']['BatchCreate'] = '新增班別成科組';
$Lang['SysMgr']['SubjectClassMapping']['FromClass'] = '每班建立一個科組';
$Lang['SysMgr']['SubjectClassMapping']['ByImport'] = '透過CSV匯入';
$Lang['SysMgr']['SubjectClassMapping']['Import'] = '匯入';
$Lang['SysMgr']['SubjectClassMapping']['Export'] = '匯出';
$Lang['SysMgr']['SubjectClassMapping']['CollapseAll'] = 'Collapse All';
$Lang['SysMgr']['SubjectClassMapping']['ExpandAll'] = 'Expand All';
$Lang['SysMgr']['SubjectClassMapping']['Order'] = '排序';
$Lang['SysMgr']['SubjectClassMapping']['SubjectWEBSAMSCode'] = '代號';
$Lang['SysMgr']['SubjectClassMapping']['SubjectDescription'] = '敘述 (英/中)';
$Lang['SysMgr']['SubjectClassMapping']['AddClass'] = '新增學科組別';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'] = '科組';
$Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroup'] = '新增科組';
$Lang['SysMgr']['SubjectClassMapping']['ClassCode'] = '代號';
$Lang['SysMgr']['SubjectClassMapping']['ClassTitleEN'] = '名稱(英)';
$Lang['SysMgr']['SubjectClassMapping']['ClassTitleB5'] = '名稱(中)';
$Lang['SysMgr']['SubjectClassMapping']['ClassTitle'] = '科組標題';
$Lang['SysMgr']['SubjectClassMapping']['ClassName'] = '名稱';
$Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'] = '老師';
$Lang['SysMgr']['SubjectClassMapping']['GroupTeacher'] = '科組老師';
$Lang['SysMgr']['SubjectClassMapping']['ClassStudent'] = '學生';
$Lang['SysMgr']['SubjectClassMapping']['ClassStudentNumber'] = '學生數目';
$Lang['SysMgr']['SubjectClassMapping']['SelectClass'] = '- 選擇班別 -';
$Lang['SysMgr']['SubjectClassMapping']['SelectForm'] = '- 選擇級別聯繫 -';
$Lang['SysMgr']['SubjectClassMapping']['InvalidSelectedStudWarning'] = '*以上學生應要移除，因他們所屬級別不在級別聯繫之上.';
$Lang['SysMgr']['SubjectClassMapping']['InvalidYearIDWarning'] = '*請至少選擇一個級別.';
$Lang['SysMgr']['SubjectClassMapping']['ClassTitleWarning'] = '*請輸入科組標題.';
$Lang['SysMgr']['SubjectClassMapping']['ClassCodeWarning'] = '*科組代號無效.';
$Lang['SysMgr']['SubjectClassMapping']['ClassCreateSuccess'] = '1|=|新增科組成功.';
$Lang['SysMgr']['SubjectClassMapping']['ClassCreateUnsuccess'] = '0|=|新增科組失敗.';
$Lang['SysMgr']['SubjectClassMapping']['DeleteClass'] = '移除科組';
$Lang['SysMgr']['SubjectClassMapping']['EditClass'] = '編輯科組';
$Lang['SysMgr']['SubjectClassMapping']['DeleteClassWarning'] = '移除科組會失去所有組別<->學生關聯，此動作不能返回。是否確定要繼續進行？';
$Lang['SysMgr']['SubjectClassMapping']['EditSubjectGroup'] = '編輯科組';
$Lang['SysMgr']['SubjectClassMapping']['ClassEditSuccess'] = '1|=|科組更新成功.';
$Lang['SysMgr']['SubjectClassMapping']['ClassEditUnsuccess'] = '0|=|科組更新失敗.';
$Lang['SysMgr']['SubjectClassMapping']['ShowAllGroup'] = '顯示所有組別';
$Lang['SysMgr']['SubjectClassMapping']['HideAllGroup'] = '隱藏所有組別';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupList'] = '科組清單';
$Lang['SysMgr']['SubjectClassMapping']['RemoveThisGroup'] = '移除此組別';
$Lang['SysMgr']['SubjectClassMapping']['Print'] = '列印';
$Lang['SysMgr']['SubjectClassMapping']['ClassDeleteSuccess'] = '1|=|科組移除成功.';
$Lang['SysMgr']['SubjectClassMapping']['ClassDeleteUnsuccess'] = '0|=|科組移除失敗.';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['ClassDeleteSuccess'] = '1|=|科組移除成功.';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['ClassDeleteUnsuccess'] = '0|=|科組移除失敗.';
$Lang['SysMgr']['SubjectClassMapping']['Instruction'] = '使用指引';
$Lang['SysMgr']['SubjectClassMapping']['Instruction1'] = '請選擇學科及班別。系統將為每個所選的班別建立所選科目的科組。';
$Lang['SysMgr']['SubjectClassMapping']['BatchCreateNote'] = '系統將會略過該班別，如班別是位於"級別與科目"關係以外';
$Lang['SysMgr']['SubjectClassMapping']['Note'] = '注意';
$Lang['SysMgr']['SubjectClassMapping']['SelectSubjects'] = '選擇學科';
$Lang['SysMgr']['SubjectClassMapping']['SelectClasses'] = '選擇班別';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning1'] = '以下的學科-班別組合未能新增，因為班別上的學生已參加了同一學科的其他組別.';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning2'] = '有問題的班別:';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning3'] = '可行的班別:';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning4'] = '您是否確定要繼續進行此動作？';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning_without_Subject'] = '以下的學科-班別組合未能新增，因為班別是位於級別與科目關係以外.';
$Lang['SysMgr']['SubjectClassMapping']['FormAssociatedTo'] = '適用於';
$Lang['SysMgr']['SubjectClassMapping']['NumberOfStudentInForm'] = '級別中學生數目';
$Lang['SysMgr']['SubjectClassMapping']['FilterFormList'] = '過濾此列表';
$Lang['SysMgr']['SubjectClassMapping']['CloseFilter'] = '關閉過濾';
$Lang['SysMgr']['SubjectClassMapping']['ClearFilter'] = '清除選項';
$Lang['SysMgr']['SubjectClassMapping']['HideNonRelatedSubject'] = '隱藏空白學科';
$Lang['SysMgr']['SubjectClassMapping']['ViewSubjectGroupWithStudentFrom'] = '只檢視含下列級別學生的科組：';
$Lang['SysMgr']['SubjectClassMapping']['LinkedClass'] = '已聯繫班別';

$Lang['SysMgr']['SubjectClassMapping']['LearningCategory'] = '學習領域';
$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'] = 'WebSAMS 代號';
$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCodeComponent'] = 'WebSAMS 代號（學科分卷）';
$Lang['SysMgr']['SubjectClassMapping']['Title'] = '名稱';
$Lang['SysMgr']['SubjectClassMapping']['TitleEn'] = '名稱 (英文)';
$Lang['SysMgr']['SubjectClassMapping']['TitleCh'] = '名稱 (中文)';
$Lang['SysMgr']['SubjectClassMapping']['ComponentName'] = '學科分卷名稱';
$Lang['SysMgr']['SubjectClassMapping']['Abbreviation'] = '縮寫';
$Lang['SysMgr']['SubjectClassMapping']['AbbreviationEn'] = '縮寫 (英文)';
$Lang['SysMgr']['SubjectClassMapping']['AbbreviationCh'] = '縮寫 (中文)';
$Lang['SysMgr']['SubjectClassMapping']['ShortForm'] = '簡寫';
$Lang['SysMgr']['SubjectClassMapping']['ShortFormEn'] = '簡寫 (英文)';
$Lang['SysMgr']['SubjectClassMapping']['ShortFormCh'] = '簡寫 (中文)';
$Lang['SysMgr']['SubjectClassMapping']['NoLearningCategory'] = '未分類學科';
$Lang['SysMgr']['SubjectClassMapping']['ParentSubject'] = '主學科';
$Lang['SysMgr']['SubjectClassMapping']['ComponentSubject'] = '學科分卷';
$Lang['SysMgr']['SubjectClassMapping']['eClassLicenseLeftMessage'] = '你的一般教室許可證有 <!--TotalQuota--> 個，尚有 <!--QuotaLeft--> 個可供使用。你已使用的許可證為 <!--QuotaUsed--> 個。';
$Lang['SysMgr']['SubjectClassMapping']['NotEnoughLicenseMessage'] = '你沒有足夠的許可證建立eClass教室。';
$Lang['SysMgr']['SubjectClassMapping']['AddingStudentsToSubjectGroup'] = '正在加入學生到科組';
$Lang['SysMgr']['SubjectClassMapping']['AddingTeachersToSubjectGroup'] = '正在加入老師到科組';
$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'] = '製作錯誤資料備註中';

$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'][0] = $Lang['SysMgr']['SubjectClassMapping']['ParentSubject']." ".$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'];
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'][1] = "學科分卷"." ".$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'];
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'][2] = "學科分卷名稱 (英文)";
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'][3] = "學科分卷名稱 (中文)";
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'][4] = "學科分卷".$Lang['SysMgr']['SubjectClassMapping']['AbbreviationEn'];
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'][5] = "學科分卷".$Lang['SysMgr']['SubjectClassMapping']['AbbreviationCh'];
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'][6] = "學科分卷".$Lang['SysMgr']['SubjectClassMapping']['ShortFormEn'];
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'][7] = "學科分卷".$Lang['SysMgr']['SubjectClassMapping']['ShortFormCh'];

$Lang['SysMgr']['SubjectClassMapping']['Settings']['LearningCategory'] = '學習領域設定';

$Lang['SysMgr']['SubjectClassMapping']['Add']['LearningCategory'] = '新增學習領域';
$Lang['SysMgr']['SubjectClassMapping']['Add']['Subject'] = '新增學科';
$Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectInLearningCategory'] = '於此學習領域新增學科';
$Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectComponent'] = '新增子學科';

$Lang['SysMgr']['SubjectClassMapping']['Edit']['LearningCategory'] = '編輯學習領域';
$Lang['SysMgr']['SubjectClassMapping']['Edit']['Subject'] = '編輯學科';
$Lang['SysMgr']['SubjectClassMapping']['Edit']['SubjectComponent'] = '編輯子學科';

$Lang['SysMgr']['SubjectClassMapping']['Delete']['LearningCategory'] = '刪除學習領域';
$Lang['SysMgr']['SubjectClassMapping']['Delete']['Subject'] = '刪除學科';
$Lang['SysMgr']['SubjectClassMapping']['Delete']['SubjectComponent'] = '刪除子學科';
$Lang['SysMgr']['SubjectClassMapping']['Delete']['SubjectGroupOfThisSubject'] = '刪除此學科的科組';

$Lang['SysMgr']['SubjectClassMapping']['Reorder']['LearningCategory'] = '排列學習領域';
$Lang['SysMgr']['SubjectClassMapping']['Reorder']['Subject'] = '排列學科';
$Lang['SysMgr']['SubjectClassMapping']['Reorder']['SubjectComponent'] = '排列子學科';

$Lang['SysMgr']['SubjectClassMapping']['Setting']['LearningCategory'] = '學習領域設定';

$Lang['SysMgr']['SubjectClassMapping']['View']['SubjectComponent'] = '檢視學科分卷';
$Lang['SysMgr']['SubjectClassMapping']['View']['B
ToAllSubjectsView'] = '回到全部學科';

$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterWebSAMSCode'] = '請輸入WebSAMS代碼。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterTitleEn'] = '請輸入英文名稱。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterTitleCh'] = '請輸入中文名稱。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterAbbrEn'] = '請輸入英文縮寫。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterAbbrCh'] = '請輸入中文縮寫。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterShortFormEn'] = '請輸入英文簡寫。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterShortFormCh'] = '請輸入中文簡寫。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteLearningCategory'] = '你是否確定刪除此學習領域？';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteSubject'] = '你是否確定刪除此學科？';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteSubjectComponent'] = '你是否確定刪除此子學科？';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseSelectLearningCategory'] = '請選擇學習領域。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['DeleteSingleSubjectGroup'] = '刪除科組後，系統不會自動刪除相關的eClass課室。請自行到 "學與教管理工具 > 網上教室 > eClass 管理" 手動進行刪除。如果你不肯定哪些課室連結到此科組，請先行檢查清楚，然後才移除科組。你是否確定要刪除此科組？';

$Lang['SysMgr']['SubjectClassMapping']['Warning']['Delete']['MultipleSubjectGroup'] = '刪除科組後，系統不會自動刪除相關的eClass課室。請自行到 <b>學與教管理工具 > 網上教室 > eClass 管理</b> 手動進行刪除。如果你不肯定哪些課室連結到此等科組，請先行檢查清楚，然後才移除科組。';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['NoSelected']['LearningCategory'] = '* 沒有選擇學習領域';

$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['WebSAMSCode'] = '* WebSAMS代碼空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['Title'] = '* 名稱空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['TitleEn'] = '* 英文名稱空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['TitleCh'] = '* 中文名稱空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['AbbrEn'] = '* 英文縮寫空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['AbbrCh'] = '* 中文縮寫空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['ShortFormEn'] = '* 英文簡寫空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['ShortFormCh'] = '* 中文簡寫空白';

$Lang['SysMgr']['SubjectClassMapping']['Warning']['DuplicatedCode']['Subject'] = '* 已有其他學科使用此代號';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['DuplicatedCode']['SubjectComponent'] = '* 已有其他學科分卷使用此代號';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['DuplicatedTitle']['LearningCategory'] = '* 已有學習領域使用此名稱';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['MustBeInteger']['WebSAMSCode'] = '* WebSAMS代碼必須是數字';

$Lang['SysMgr']['SubjectClassMapping']['Select']['LearningCategory'] = '選擇學習領域';
$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject'] = '選擇學科';
$Lang['SysMgr']['SubjectClassMapping']['Select']['SubjectComponent'] = '選擇學科分卷';
$Lang['SysMgr']['SubjectClassMapping']['Select']['SubjectGroup'] = '選擇科組';

$Lang['SysMgr']['SubjectClassMapping']['All']['Subject'] = '全部學科';
$Lang['SysMgr']['SubjectClassMapping']['All']['SubjectGroup'] = '全部科組';

$Lang['SysMgr']['SubjectClassMapping']['Button']['Add&Finish'] = $Lang['Btn']['Submit'];
$Lang['SysMgr']['SubjectClassMapping']['Button']['Add&AddMoreSubject'] = $Lang['Btn']['Submit&AddMore'];

$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['LearningCategory'] = '1|=|學習領域新增成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['Subject'] = '1|=|學科新增成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['SubjectComponent'] = '1|=|子學科新增成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['LearningCategory'] = '0|=|學習領域新增失敗。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['Subject'] = '0|=|學科新增失敗。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['SubjectComponent'] = '0|=|子學科新增失敗。';

$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['LearningCategory'] = '1|=|學習領域編輯成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['Subject'] = '1|=|學科編輯成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['SubjectComponent'] = '1|=|子學科編輯成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['LearningCategory'] = '0|=|學習領域編輯失敗。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['Subject'] = '0|=|學科編輯失敗。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['SubjectComponent'] = '0|=|子學科編輯失敗。';

$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['LearningCategory'] = '1|=|學習領域刪除成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['Subject'] = '1|=|學科刪除成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['SubjectComponent'] = '1|=|子學科刪除成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['LearningCategory'] = '0|=|學習領域刪除失敗。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['Subject'] = '0|=|學科刪除失敗。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['SubjectComponent'] = '0|=|子學科刪除失敗。';

$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'] = array("學科代號  <a href='javascript:js_Show_Detail_Layer(1)' class='tablelink'>[按此查詢代碼]</a>");
if($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent']){
	$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'][] = "學科分卷代號";
}
$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'][] = "學科分組代號 <a href='javascript:js_Show_Detail_Layer(2)' class='tablelink'>[按此查詢代碼]</a>";
$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'][] = "名稱(英)";
$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'][] = "名稱(中)";
$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'][] = "適用班級";
$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'][] = "建立網上教室";
$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField'][] = "老師內聯網帳號";

$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectName'] = "學科名稱";
$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectCode'] = "學科代號";
$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectGroupName'] = "學科分組名稱";
$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectGroupCode'] = "學科分組代號";

// Timetable
$Lang['SysMgr']['Timetable']['ModuleTitle'] = '時間表';
$Lang['SysMgr']['Timetable']['Timetable'] = '時間表';

$Lang['SysMgr']['Timetable']['Period'] = '時段';
$Lang['SysMgr']['Timetable']['TimeSession'] = '時段';
$Lang['SysMgr']['Timetable']['Cycle'] = '循環';
$Lang['SysMgr']['Timetable']['CycleDay'] = '循環日';
// 20100326 Ivan: requested by Clement to change back to English
//$Lang['SysMgr']['Timetable']['Day'] = '日';
$Lang['SysMgr']['Timetable']['Day'] = 'Day';
$Lang['SysMgr']['Timetable']['Identity'] = '身份';
$Lang['SysMgr']['Timetable']['AcademicYear'] = '學年';
$Lang['SysMgr']['Timetable']['Term'] = '學期';
$Lang['SysMgr']['Timetable']['Target'] = '目標';
$Lang['SysMgr']['Timetable']['User'] = '用戶';
$Lang['SysMgr']['Timetable']['Title'] = '標題';
$Lang['SysMgr']['Timetable']['Name'] = '名稱';
$Lang['SysMgr']['Timetable']['StartTime'] = '開始時間';
$Lang['SysMgr']['Timetable']['EndTime'] = '結束時間';
$Lang['SysMgr']['Timetable']['TimeSlot'] = '課節';
$Lang['SysMgr']['Timetable']['Time'] = '時間';
$Lang['SysMgr']['Timetable']['DataFiltering'] = '過濾此時間表';
$Lang['SysMgr']['Timetable']['DisplayOption'] = '檢視選項';
$Lang['SysMgr']['Timetable']['SubjectTitle'] = '學科名稱';
$Lang['SysMgr']['Timetable']['NumOfStudent'] = '學生數量';
$Lang['SysMgr']['Timetable']['Copy'] = '複製';
$Lang['SysMgr']['Timetable']['PrintDataInOneTimetable'] = '於同一時間表列印所有資料';
$Lang['SysMgr']['Timetable']['FirstPmLesson'] = '首節下午課';
$Lang['SysMgr']['Timetable']['Mode']['View'] = '檢視模式';
$Lang['SysMgr']['Timetable']['Mode']['Edit'] = '編排模式';
$Lang['SysMgr']['Timetable']['SetTimetable'] = '設定時間表';

$Lang['SysMgr']['Timetable']['MangeTimetable'] = '管理時間表';
$Lang['SysMgr']['Timetable']['RecordsValidated'] = '已核對<!--NumOfRecords-->項紀錄';
$Lang['SysMgr']['Timetable']['RecordsProcessed'] = '已處理<!--NumOfRecords-->項紀錄';
$Lang['SysMgr']['Timetable']['NumOfContinuousTimeSlot'] = '連續節數';
$Lang['SysMgr']['Timetable']['TeacherFreeLesson'] = '老師空堂';
$Lang['SysMgr']['Timetable']['FreeRoom'] = '可用的課室';

$Lang['SysMgr']['Timetable']['Tag']['Management'] = '管理';
$Lang['SysMgr']['Timetable']['Tag']['View'] = '檢視/列印';
$Lang['SysMgr']['Timetable']['Tag']['EverydayTimetable'] = '編排每日時間表';

$Lang['SysMgr']['Timetable']['Add']['RoomAllocation'] = '新增課堂';
$Lang['SysMgr']['Timetable']['Add']['Timetable'] = '新增時間表';
$Lang['SysMgr']['Timetable']['Add']['CycleDay'] = '新增循環日';
$Lang['SysMgr']['Timetable']['Add']['TimeSlot'] = '新增時段';

$Lang['SysMgr']['Timetable']['Delete']['RoomAllocation'] = '刪除課堂';
$Lang['SysMgr']['Timetable']['Delete']['RoomAllocationOfThisTimeSlotAndDay'] = '刪除此日此時段的課堂';
$Lang['SysMgr']['Timetable']['Delete']['Timetable'] = '刪除時間表';
$Lang['SysMgr']['Timetable']['Delete']['CycleDay'] = '刪除循環日';
$Lang['SysMgr']['Timetable']['Delete']['TimeSlot'] = '刪除時段';

$Lang['SysMgr']['Timetable']['Edit']['RoomAllocation'] = '編輯課堂';
$Lang['SysMgr']['Timetable']['Edit']['Timetable'] = '編輯時間表';
$Lang['SysMgr']['Timetable']['Edit']['TimeSlot'] = '編輯時段';

$Lang['SysMgr']['Timetable']['Select']['Period'] = '選擇時段';
$Lang['SysMgr']['Timetable']['Select']['Cycle'] = '選擇循環';
$Lang['SysMgr']['Timetable']['Select']['CycleDays'] = '選擇循環日數';
$Lang['SysMgr']['Timetable']['Select']['ViewMode'] = '選擇檢視模式';
$Lang['SysMgr']['Timetable']['Select']['Identity'] = '選擇身份';
$Lang['SysMgr']['Timetable']['Select']['Timetable'] = '選擇時間表';
$Lang['SysMgr']['Timetable']['Setting']['AssignEverydayTimetable'] = '編排時間表';

$Lang['SysMgr']['Timetable']['Button']['Add&AddMoreRoomAllocation'] = $Lang['Btn']['Submit&AddMore'];
$Lang['SysMgr']['Timetable']['Button']['ClearFiltering'] = '不過濾資料';
$Lang['SysMgr']['Timetable']['Button']['ImportLesson'] = '匯入課堂';
$Lang['SysMgr']['Timetable']['Button']['ExportLesson'] = '匯出課堂';
$Lang['SysMgr']['Timetable']['Button']['IncreaseFontSize'] = '增大字體';
$Lang['SysMgr']['Timetable']['Button']['DecreaseFontSize'] = '縮少字體';

$Lang['SysMgr']['Timetable']['ViewMode'] = '檢視/列印對象';
$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Class'] = '班別';
$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['SubjectGroup'] = '科組';
$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Personal'] = '個人';
$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Room'] = '房間';

$Lang['SysMgr']['Timetable']['Warning']['ChangeEverydayTimetable'] = '請注意!直接點選日期編排/更改時間表。所有資源預訂紀錄的時間會被自動更改。';
$Lang['SysMgr']['Timetable']['Warning']['Blank']['Title'] = '* 請輸入名稱。';
$Lang['SysMgr']['Timetable']['Warning']['NoCycleSetUpYet'] = '未有任何循環設定。';
$Lang['SysMgr']['Timetable']['Warning']['NoPeriodSetUpYet'] = '未有任何時段設定。';
$Lang['SysMgr']['Timetable']['Warning']['TimeSlotOverlapped'] = '* 時段重疊。';
$Lang['SysMgr']['Timetable']['Warning']['TitleDuplicated'] = '* 標題重複。';
$Lang['SysMgr']['Timetable']['Warning']['OnlyCopiedSubjectGroupCanBeCopied'] = '* 如選擇的學期與時間表的學期不相同，系統只會複製透過「複製」來建立的科組到此時間表。';
$Lang['SysMgr']['Timetable']['Warning']['ChangesWillApplyToAllRelatedLesson'] = '此課堂是連續課堂，所作出的改動將套用至所有相關課堂。';
$Lang['SysMgr']['Timetable']['Warning']['Import']['NoTimeSlot'] = '時間表沒有此時段。';
$Lang['SysMgr']['Timetable']['Warning']['Import']['NoDay'] = '時間表沒有此Day。';
$Lang['SysMgr']['Timetable']['Warning']['Import']['NoSubjectGroupFound'] = '此代號不屬於任何科組。';
$Lang['SysMgr']['Timetable']['Warning']['Import']['NoLocationFound'] = '此代號不屬於任何位置。';
$Lang['SysMgr']['Timetable']['Warning']['Import']['LocationOccupiedAlready'] = '已有其他課堂使用此房間。';
$Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectGroupAllocatedAlready'] = '此科組已有課堂於此時段。';
$Lang['SysMgr']['Timetable']['Warning']['Import']['StudentConflict'] = '此科組的學生已參予此時段的其他科組。';

$Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectIsNotFound'] = '學科不存在';
$Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectIsCompNotFound'] = '學科分卷不存在';

$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear'] = '請選擇學年。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Term'] = '請選擇學期。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Period'] = '請選擇時段。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Cycle'] = '請選擇循環。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Semester'] = '請選擇學期。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Building'] = '請選擇大樓。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Floor'] = '請選擇樓層。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Room'] = '請選擇房間。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Timetable'] = '請選擇時間表。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['ViewMode'] = '請選擇檢視模式。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['SubjectGroup'] = '請選擇科組。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Subject'] = '請選擇學科。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['TimeSlot'] = '請選擇時段。';

$Lang['SysMgr']['Timetable']['jsWarning']['PleaseEnter']['Title'] = '請輸入標題。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseEnter']['Location'] = '請輸入地點。';

$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['Timetable'] = '如你刪除此時間表，所有相關時段及課堂紀錄將被同時刪除。你是否確定刪除此時間表？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['TimetableUsed'] = '此時間表已經用在以此日期為第一日的時區或特別時間表,請先從他們那裡刪除才可繼續。';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['RoomAllocation'] = '你是否確定刪除此課堂？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['DeleteAllLessonsOfDayAndTimeSlot'] = '如此日子及時段內有連續課堂，相關課堂將會同時刪除。你是否確定刪除此日子及時段內的課堂？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['DeleteAllLessonsOfDay'] = '如此日子內有連續課堂，相關課堂將會同時刪除。你是否確定刪除此日子內的課堂？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['DeleteAllLessonsOfTimeSlot'] = '如此時段內有連續課堂，相關課堂將會同時刪除。你是否確定刪除此時段內的課堂？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['ContinuousLesson'] = '此課堂是連續課堂，刪除此課堂將同時刪除相關的課堂。你是否確定刪除此課堂？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['CycleDays'] = '所有本日之課堂資料將一併被刪除。你是否確定要刪除？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['TimeSlot'] = '如你刪除此時段，所有相關課堂紀錄將被同時刪除。你是否確定刪除此時段？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['TimeSlotWithBooking'] = '如你刪除此時段，所有相關課堂紀錄,房間預訂紀錄,物品預訂紀錄及行事曆將被同時刪除。你是否確定刪除此時段？';

$Lang['SysMgr']['Timetable']['jsWarning']['Copy']['Timetable'] = '如你複製時間表，已於本時間表設定的時段及課堂紀錄將被刪除。你是否確定複製時間表？';

$Lang['SysMgr']['Timetable']['jsWarning']['RoomUsedAlready'] = '* 此房間已分配給其他科組。';
$Lang['SysMgr']['Timetable']['jsWarning']['SubjectGroupAllocatedAlready'] = '* 此科組已於此時段獲分配到其他房間。';
$Lang['SysMgr']['Timetable']['jsWarning']['SubjectGroupUserConflict'] = '* 此科組的學生已參予此時段的科組。';
$Lang['SysMgr']['Timetable']['jsWarning']['StartTimeMustBeEarlierThenEndTime'] = '* 結束時間不能早於開始時間。';
$Lang['SysMgr']['Timetable']['jsWarning']['TimeSlotOverlapped'] = '* 時段不可重疊。';

$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['Timetable'] = '1|=|時間表新增成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['CycleDay'] = '1|=|循環日新增成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['TimeSlot'] = '1|=|時段新增成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['RoomAllocation'] = '11|=|課堂新增成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['Timetable'] = '0|=|時間表新增失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['CycleDay'] = '0|=|循環日新增失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['TimeSlot'] = '0|=|時段新增失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['RoomAllocation'] = '0|=|課堂新增失敗。';

$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['Sync']='1|=|同步成功';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['Sync']='0|=|同步失敗';

$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['Timetable'] = '1|=|時間表編輯成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['CycleDay'] = '1|=|循環日編輯成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['TimeSlot'] = '1|=|時段編輯成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['RoomAllocation'] = '1|=|課堂編輯成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['Timetable'] = '0|=|時間表編輯失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['CycleDay'] = '0|=|循環日編輯失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['TimeSlot'] = '0|=|時段編輯失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['RoomAllocation'] = '0|=|課堂編輯失敗。';

$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['Timetable'] = '1|=|時間表刪除成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['CycleDay'] = '1|=|循環日刪除成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['TimeSlot'] = '1|=|時段刪除成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['RoomAllocation'] = '1|=|課堂刪除成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['Timetable'] = '0|=|時間表刪除失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['CycleDay'] = '0|=|循環日刪除失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['TimeSlot'] = '0|=|時段刪除失敗。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['RoomAllocation'] = '0|=|課堂刪除失敗。';

$Lang['SysMgr']['Timetable']['ReturnMessage']['Import']['WrongFileFormat'] = '0|=|檔案格式錯誤。上載的檔案必須是.csv或.txt的檔案。';

$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Day";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Time Slot";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Time Slot Name";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Time Slot Time Range";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Subject Group Code";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Subject Group Name";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Building Code";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Building Name";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Location Code";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Location Name";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Sub-location Code";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Sub-location Name";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['En'][] = "Others Site";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "日";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "時段";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "時段名稱";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "時段時間";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "科組代號";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "科組名稱";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "大樓代號";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "大樓名稱";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "位置代號";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "位置名稱";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "子位置代號";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "子位置名稱";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column']['Ch'][] = "其他場所";

$Lang['SysMgr']['Timetable']['ImportLesson']['LocationRemarks'] = "如課堂地點以代號輸入，系統將不會處理該「其他場所」的資料。";


// System Security
$Lang['SysMgr']['Security']['Title'] = '系統保安';
$Lang['SysMgr']['Security']['PasswordPolicies'] = '用戶密碼措施';
$Lang['SysMgr']['Security']['AccountLockoutPolicy'] = '登入密碼錯誤鎖定措施';
$Lang['SysMgr']['Security']['AllowAccessIP'] = '指定可存取行政管理中心電腦 IP';
$Lang['SysMgr']['Security']['Note'] = '為加強系統保安，請設定用戶密碼措施及登入密碼錯誤鎖定措施！';
$Lang['SysMgr']['Security']['enable_password_change'] = "允許更改密碼";
$Lang['SysMgr']['Security']['enable_password_policy'] = "強制使用字母及數字混合密碼";
$Lang['SysMgr']['Security']['enable_periodical_change'] = "強制定期改變密碼";

$Lang['SysMgr']['Security']['remark_complex_password'] = "強制用戶使用至少由六個英文字母及數字混合組成的密碼，以加強戶口的安全性！";
$Lang['SysMgr']['Security']['remark_periodic_change'] = "可設定最多 999 天，而 0 代表不使用。";

$Lang['SysMgr']['Security']['Allow2Change'] = "允許";
$Lang['SysMgr']['Security']['Disallow2Change'] = "不允許";
$Lang['SysMgr']['Security']['RequireComplexPassword'] = "使用";
$Lang['SysMgr']['Security']['NotRequireComplexPassword'] = "不使用";
$Lang['SysMgr']['Security']['NotRequirePeriodicalChange'] = "不使用";

$Lang['SysMgr']['Security']['NoAttemptLimit'] = "不限";

$Lang['SysMgr']['Security']['unit_day'] = "日";
$Lang['SysMgr']['Security']['unit_character'] = "個字元";

// Role Management
$Lang['SysMgr']['RoleManagement']['IdentityRole'] = '身份 / 角色概覽';
$Lang['SysMgr']['RoleManagement']['ModuleRole'] = 'Module Role';
$Lang['SysMgr']['RoleManagement']['ModuleTitle'] = '角色';
$Lang['SysMgr']['RoleManagement']['New'] = '新增';
$Lang['SysMgr']['RoleManagement']['Student'] = '學生';
$Lang['SysMgr']['RoleManagement']['Parent'] = '家長';
$Lang['SysMgr']['RoleManagement']['Alumni'] = '校友';
$Lang['SysMgr']['RoleManagement']['RemoveRole'] = '移除角色';
$Lang['SysMgr']['RoleManagement']['NewRole'] = '新增角色';
$Lang['SysMgr']['RoleManagement']['Identity'] = '身份';
$Lang['SysMgr']['RoleManagement']['SelectIdentity'] = '- 選擇身份 -';
$Lang['SysMgr']['RoleManagement']['RoleTitle'] = '身份名稱';
$Lang['SysMgr']['RoleManagement']['SubmitAndEdit'] = '遞交 &amp; 修改設定';
$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning'] = '*身份名稱重複或空白';
$Lang['SysMgr']['RoleManagement']['IdentitySelectWarning'] = '*Please select an identity';
$Lang['SysMgr']['RoleManagement']['RoleCreatedSuccess'] = '1|=|身份新增成功.';
$Lang['SysMgr']['RoleManagement']['RoleCreatedUnsuccess'] = '0|=|身份新增失敗.';
$Lang['SysMgr']['RoleManagement']['RoleList'] = '身份清單';
$Lang['SysMgr']['RoleManagement']['AccessRightAndTarget'] = 'Access Right & Targeting';
$Lang['SysMgr']['RoleManagement']['UserList'] = '成員名單';
$Lang['SysMgr']['RoleManagement']['AccessRight'] = '此角色可管理以下所勾選的模組：';
$Lang['SysMgr']['RoleManagement']['Targeting'] = '寄件對象';
$Lang['SysMgr']['RoleManagement']['Name'] = '姓名';
$Lang['SysMgr']['RoleManagement']['UserType'] = '身份';
$Lang['SysMgr']['RoleManagement']['Delete'] = '移除';
$Lang['SysMgr']['RoleManagement']['RemoveThisRole'] = '移除此身份';
$Lang['SysMgr']['RoleManagement']['AddUser'] = '新增用戶';
$Lang['SysMgr']['RoleManagement']['Users'] = '用戶';
$Lang['SysMgr']['RoleManagement']['RoleRenamedSuccess'] = '1|=|身份重新命名成功.';
$Lang['SysMgr']['RoleManagement']['RoleRenamedUnsuccess'] = '0|=|身份重新命名失敗.';
$Lang['SysMgr']['RoleManagement']['AddRoleMemberWarning'] = '*Please select at least one user to add.';
$Lang['SysMgr']['RoleManagement']['AddRoleMemberUnsuccess'] = '0|=|成員新增失敗.';
$Lang['SysMgr']['RoleManagement']['AddRoleMemberSuccess'] = '1|=|成員新增成功.';
$Lang['SysMgr']['RoleManagement']['RoleRemoveSuccess'] = '1|=|成員移除成功.';
$Lang['SysMgr']['RoleManagement']['RoleRemoveUnsuccess'] = '0|=|成員移除失敗.';
$Lang['SysMgr']['RoleManagement']['RoleDeleteWarning'] = '您是否確定要移除此身份?';
$Lang['SysMgr']['RoleManagement']['RoleMemberDeleteWarning'] = '您是否確定要移除已選擇之身份成員?';
$Lang['SysMgr']['RoleManagement']['RoleMemberRemoveSuccess'] = '1|=|身份成員移除成功.';
$Lang['SysMgr']['RoleManagement']['RoleMemberRemoveUnsuccess'] = '0|=|身份成員移除失敗.';
$Lang['SysMgr']['RoleManagement']['RoleSavedSuccess'] = '1|=|身分儲存成功.';
$Lang['SysMgr']['RoleManagement']['RoleSavedUnsuccess'] = '0|=|身分儲存失敗.';
$Lang['SysMgr']['RoleManagement']['RoleName'] = '角色';
$Lang['SysMgr']['RoleManagement']['TopManagement'] = '模組管理權限';
$Lang['SysMgr']['RoleManagement']['RoleUpdateWarning'] = '設定於再次登入時才生效';
$Lang['SysMgr']['RoleManagement']['SearchUser'] = '搜尋用戶';
$Lang['SysMgr']['RoleManagement']['SelectedUser'] = '已選擇用戶';
$Lang['SysMgr']['RoleManagement']['SelectAll'] = '-- 選擇全部 --';
$Lang['SysMgr']['RoleManagement']['TeachingStaff'] = '教師';
$Lang['SysMgr']['RoleManagement']['SupportStaff'] = '非教學職員';
$Lang['SysMgr']['RoleManagement']['AllTeachingStaff'] = '所有教師';
$Lang['SysMgr']['RoleManagement']['AllSupportStaff'] = '所有非教學職員';
$Lang['SysMgr']['RoleManagement']['AllStudent'] = '所有學生';
$Lang['SysMgr']['RoleManagement']['MyChildren'] = '我的子女';
$Lang['SysMgr']['RoleManagement']['MyChild'] = '我的子女';
$Lang['SysMgr']['RoleManagement']['AllParent'] = '所有家長';
$Lang['SysMgr']['RoleManagement']['AllAlumni'] = '所有校友';
$Lang['SysMgr']['RoleManagement']['MyGroupAlumni'] = '我的同一小組校友';
$Lang['SysMgr']['RoleManagement']['MyParent'] = '我的父母';
$Lang['SysMgr']['RoleManagement']['MySubjectGroupStaff'] = '我的科組';
$Lang['SysMgr']['RoleManagement']['MyClassStaff'] = '我的班別';
$Lang['SysMgr']['RoleManagement']['MyFormStudent'] = '我的級別';
$Lang['SysMgr']['RoleManagement']['MySubjectGroupStudent'] = '我的科組';
$Lang['SysMgr']['RoleManagement']['MyClassStudent'] = '我的班別';
$Lang['SysMgr']['RoleManagement']['MyFormParent'] = '我的級別';
$Lang['SysMgr']['RoleManagement']['MySubjectGroupParent'] = '我的科組';
$Lang['SysMgr']['RoleManagement']['MyClassParent'] = '我的班別';
$Lang['SysMgr']['RoleManagement']['ToAllUser'] = '可發送至所有用戶：';
$Lang['SysMgr']['RoleManagement']['ToAll'] = '所有...';
$Lang['SysMgr']['RoleManagement']['BelongToSameForm'] = '同一級別的...';
$Lang['SysMgr']['RoleManagement']['BelongToSameFormClass'] = '同一班別的...';
$Lang['SysMgr']['RoleManagement']['BelongToSameSubject'] = '同一科目的...';
$Lang['SysMgr']['RoleManagement']['BelongToSameSubjectGroup'] = '同一科組的...';
$Lang['SysMgr']['RoleManagement']['BelongToSameGroup'] = '同一小組的...';
$Lang['SysMgr']['RoleManagement']['CanSendTo'] = "可發送至：";
$Lang['SysMgr']['RoleManagement']['UncheckForMoreOptions'] = "取消勾選以顯示更多選項。";
$Lang['SysMgr']['RoleManagement']['Class'] = "班別";
$Lang['SysMgr']['RoleManagement']['ClassNumber'] = "學號";

$Lang['SysMgr']['RoleManagement']['eDisciplineAdmin'] = '訓導管理';
$Lang['SysMgr']['RoleManagement']['eCircularAdmin'] = "職員通告";
$Lang['SysMgr']['RoleManagement']['eNoticeAdmin'] = "電子通告系統";
$Lang['SysMgr']['RoleManagement']['eReportCardAdmin'] = "成績表管理";
$Lang['SysMgr']['RoleManagement']['eReportCard_Rubrics_Admin'] = "成績表 (評量指標) 管理";
$Lang['SysMgr']['RoleManagement']['eReportCardKindergarten_Admin'] = "成績表 (幼稚園) 管理";
$Lang['SysMgr']['RoleManagement']['eAttendanceAdmin'] = "教職員考勤系統管理";
$Lang['SysMgr']['RoleManagement']['eEnrolmentAdmin'] = "課外活動系統管理";
$Lang['SysMgr']['RoleManagement']['eSportsAdmin'] = "運動會管理";
$Lang['SysMgr']['RoleManagement']['eInventoryAdmin'] = "資產管理行政系統";
$Lang['SysMgr']['RoleManagement']['eBookingAdmin'] = "電子資源預訂管理系統";
$Lang['SysMgr']['RoleManagement']['eClassAdmin'] = "eClass 管理";
$Lang['SysMgr']['RoleManagement']['ePayment'] = "收費管理";
$Lang['SysMgr']['RoleManagement']['ePOS'] = "ePOS 管理";
$Lang['SysMgr']['RoleManagement']['ePolling'] = "投票管理";
$Lang['SysMgr']['RoleManagement']['ePCM'] = "採購系統管理";
$Lang['SysMgr']['RoleManagement']['eHomework'] = "家課管理";
$Lang['SysMgr']['RoleManagement']['campusLink'] = "校園連結管理";
$Lang['SysMgr']['RoleManagement']['schoolNews'] = "校園最新消息管理";
$Lang['SysMgr']['RoleManagement']['ResourcesBooking'] = "資源預訂";
$Lang['SysMgr']['RoleManagement']['StudentAttendance'] = "學生考勤系統管理";
$Lang['SysMgr']['RoleManagement']['iCalendar'] = "行事曆管理";
$Lang['SysMgr']['RoleManagement']['eSurveyAdmin'] = "問卷調查";
$Lang['SysMgr']['RoleManagement']['iPortfolioAdmin'] = "iPortfolio 管理";
$Lang['SysMgr']['RoleManagement']['sdasAdmin']['general'] = $Lang['Header']['Menu']['StudentDataAnalysisSystem']['general'];
$Lang['SysMgr']['RoleManagement']['sdasAdmin']['catholic'] = $Lang['Header']['Menu']['StudentDataAnalysisSystem']['catholic'];
$Lang['SysMgr']['RoleManagement']['sdasAdmin']['tungwah'] = $Lang['Header']['Menu']['StudentDataAnalysisSystem']['tungwah'];
$Lang['SysMgr']['RoleManagement']['eClassAppAdmin'] = "eClass Parent App 管理";
$Lang['SysMgr']['RoleManagement']['eClassTeacherAppAdmin'] = "eClass Teacher App 管理";
$Lang['SysMgr']['RoleManagement']['eClassStudentAppAdmin'] = "eClass Student App 管理";
$Lang['SysMgr']['RoleManagement']['SLRSAdmin'] = "代課編排系統";
$Lang['SysMgr']['RoleManagement']['eAppraisalAdmin'] = "考績管理系統";
$Lang['SysMgr']['RoleManagement']['eGuidanceAdmin'] = "輔導管理";

$Lang['SysMgr']['RoleManagement']['eService'] = "資訊服務";
$Lang['SysMgr']['RoleManagement']['eLearning'] = "學與教管理工具";
$Lang['SysMgr']['RoleManagement']['eAdmin'] = "學校行政管理工具";
$Lang['SysMgr']['RoleManagement']['other'] = "其他";
$Lang['SysMgr']['RoleManagement']['SchoolSettings'] = $Lang['Header']['Menu']['SchoolSettings'];
$Lang['SysMgr']['RoleManagement']['RepairSystem'] = "報修系統";
$Lang['SysMgr']['RoleManagement']['AccountMgmt'] = "用戶管理";
$Lang['SysMgr']['RoleManagement']['IES_Admin'] = "通識獨立專題探究管理";
$Lang['SysMgr']['RoleManagement']['SBA_Admin'] = "SBA_LS 管理";
$Lang['SysMgr']['RoleManagement']['PrintRoleDetail'] = "列印身份/角色詳細資料";
$Lang['SysMgr']['RoleManagement']['ExportRoleDetail'] = "匯出身份/角色詳細資料";
$Lang['SysMgr']['RoleManagement']['RoleDetail'] = "身份/角色詳細資料";
$Lang['SysMgr']['RoleManagement']['ReadingScheme'] = "閱讀計劃";
$Lang['SysMgr']['RoleManagement']['DigitalArchive'] = "電子文件管理";
$Lang['SysMgr']['RoleManagement']['SubjecteResources'] = "學科電子資源管理";
$Lang['SysMgr']['RoleManagement']['iTextbook'] = "電子課本";
$Lang['SysMgr']['RoleManagement']['EmailMerge'] = "合併電郵管理";
$Lang['SysMgr']['RoleManagement']['SMS'] = "短訊管理";
$Lang['SysMgr']['RoleManagement']['ParentAppNotify'] = "家長通知 (eClass App) 管理";
$Lang['SysMgr']['RoleManagement']['PushMessageCenter'] = "推送通知 (eClass App) 管理";
$Lang['SysMgr']['RoleManagement']['TeacherAppNotify'] = "教職員通知 (Teacher App) 管理";
$Lang['SysMgr']['RoleManagement']['StudentAppNotify'] = "學生通知 (Student App) 管理";
$Lang['SysMgr']['RoleManagement']['ClassDiary'] = "課堂日誌管理";
$Lang['SysMgr']['RoleManagement']['UpdateStudentPwdPopUp'] = "更改學生密碼";
$Lang['SysMgr']['RoleManagement']['DocRouting'] = "文件傳遞管理";
$Lang['SysMgr']['RoleManagement']['ePostAdmin'] = "ePost管理";
$Lang['SysMgr']['RoleManagement']['eAdmission'] = "入學管理";
$Lang['SysMgr']['RoleManagement']['PhotoAlbum'] = "電子相簿管理";
$Lang['SysMgr']['RoleManagement']['DigitalChannels'] = "數碼頻道管理";
$Lang['SysMgr']['RoleManagement']['FlippedChannels'] = "翻轉頻道管理";
$Lang['SysMgr']['RoleManagement']['TeacherPortfolio'] = "教職員檔案系統管理";
$Lang['SysMgr']['RoleManagement']['eForm'] = "電子表單管理";
$Lang['SysMgr']['RoleManagement']['PowerLesson2'] = "PowerLesson2管理";
$Lang['SysMgr']['RoleManagement']['PowerPortfolio'] = "幼兒學習歷程檔案";


// Location

$Lang['SysMgr']['Location']['All']['Floor'] = "所有位置";
$Lang['SysMgr']['Location']['Location'] = '校園';
$Lang['SysMgr']['Location']['LocationList'] = '所有位置';
$Lang['SysMgr']['Location']['Building'] = '大樓';
$Lang['SysMgr']['Location']['Floor'] = '位置';
$Lang['SysMgr']['Location']['Room'] = '子位置';
$Lang['SysMgr']['Location']['Code'] = '代號';
$Lang['SysMgr']['Location']['Barcode'] = '條碼';
$Lang['SysMgr']['Location']['Title'] = '名稱';
$Lang['SysMgr']['Location']['TitleEn'] = '英文名稱';
$Lang['SysMgr']['Location']['TitleCh'] = '中文名稱';
$Lang['SysMgr']['Location']['Description'] = '描述';
$Lang['SysMgr']['Location']['Capacity'] = '容量';
$Lang['SysMgr']['Location']['ClickToEdit'] = $Lang['General']['ClickToEdit'];
$Lang['SysMgr']['Location']['OthersLocation'] = '其他場所';
$Lang['SysMgr']['Location']['Room&OthersLocation'] = '房間及其他場所';



$Lang['SysMgr']['Location']['Toolbar']['NewLocation'] = '新增場所';
$Lang['SysMgr']['Location']['Toolbar']['Import'] = '匯入';
$Lang['SysMgr']['Location']['Toolbar']['Export'] = '匯出';

$Lang['SysMgr']['Location']['Setting']['Building'] = '編輯大樓';
$Lang['SysMgr']['Location']['Setting']['Floor'] = '編輯位置';
$Lang['SysMgr']['Location']['Setting']['Room'] = '子位置設定';

$Lang['SysMgr']['Location']['Add']['Building'] = '新增大樓';
$Lang['SysMgr']['Location']['Add']['Floor'] = '新增位置';
$Lang['SysMgr']['Location']['Add']['Room'] = '新增子位置';
$Lang['SysMgr']['Location']['Add']['RoomWithinFloor'] = '在此新增子位置';

$Lang['SysMgr']['Location']['Edit']['Room'] = '編輯';

$Lang['SysMgr']['Location']['Delete']['Building'] = '刪除';
$Lang['SysMgr']['Location']['Delete']['Floor'] = '刪除';
$Lang['SysMgr']['Location']['Delete']['Room'] = '刪除';

$Lang['SysMgr']['Location']['Reorder']['Building'] = '移動';
$Lang['SysMgr']['Location']['Reorder']['Floor'] = '移動';
$Lang['SysMgr']['Location']['Reorder']['Room'] = '移動';

$Lang['SysMgr']['Location']['Select']['Building'] = '選擇大樓';
$Lang['SysMgr']['Location']['Select']['Floor'] = '選擇位置';
$Lang['SysMgr']['Location']['Select']['Room'] = '選擇子位置';

$Lang['SysMgr']['Location']['All']['Building'] = '全部大樓';
$Lang['SysMgr']['Location']['All']['Room'] = '全部子位置';

$Lang['SysMgr']['Location']['Button']['Add&Finish'] = $Lang['Btn']['Submit'];
$Lang['SysMgr']['Location']['Button']['Add&AddMoreLocation'] = $Lang['Btn']['Submit&AddMore'];

$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterCode'] = '請輸入代號。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterTitleEn'] = '請輸入英文名稱。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterTitleCh'] = '請輸入中文名稱。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterDescription'] = '請輸入內容。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseSelectBuilding'] = '請選擇大樓。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseSelectFloor'] = '請選擇位置。';
$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteBuilding'] = '你是否確定刪除此大樓？';
$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteFloor'] = '你是否確定刪除此位置？';
$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteRoom'] = '你是否確定刪除此子位置？';


$Lang['SysMgr']['Location']['Warning']['BarcodeNotValid'] = "* 條碼格式不正確。";
$Lang['SysMgr']['Location']['Warning']['Blank']['Barcode'] = "* 請輸入條碼。";
$Lang['SysMgr']['Location']['Warning']['Blank']['Code'] = '* 請輸入代號。';
$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn'] = '* 請輸入英文名稱。';
$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh'] = '* 請輸入中文名稱。';
$Lang['SysMgr']['Location']['Warning']['NoSelected']['Building'] = '* 沒有選擇大樓';
$Lang['SysMgr']['Location']['Warning']['NoSelected']['Floor'] = '* 沒有選擇位置';
$Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Building'] = '* 已有其他大樓使用此代號';
$Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Floor'] = '* 已有其他位置使用此代號';
$Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Room'] = '* 已有其他子位置使用此代號';
$Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Index'] = '顯示為紅色的代號為重複使用之代號，重複代號將會影響相關匯入結果，請立即更改重複的代號。';
$Lang['SysMgr']['Location']['Warning']['DuplicatedBarcode']['Building'] = '* 已有其他大樓使用此條碼';
$Lang['SysMgr']['Location']['Warning']['DuplicatedBarcode']['Floor'] = '* 已有其他位置使用此條碼';
$Lang['SysMgr']['Location']['Warning']['DuplicatedBarcode']['Room'] = '* 已有其他子位置使用此條碼';
$Lang['SysMgr']['Location']['Warning']['ReloadTimetable']['ClearFilteringWarning'] = '如時間表已設定大量課堂，顯示整個時間表可能需時數分鐘。你是否確定要顯示整個時間表？';
$Lang['SysMgr']['Location']['Warning']['Incorrect']['Capacity']='* 請輸入正確容量';

$Lang['SysMgr']['Location']['Remark']['RedWordMeansDuplicatedCode'] = '* 紅色顯示為已重複使用之編號';

$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Building'] = '1|=|大樓新增成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Floor'] = '1|=|位置新增成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Room'] = '1|=|子位置新增成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Building'] = '0|=|大樓新增失敗。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Floor'] = '0|=|位置新增失敗。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Room'] = '0|=|子位置新增失敗。';

$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Building'] = '1|=|大樓編輯成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Floor'] = '1|=|位置編輯成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Room'] = '1|=|子位置編輯成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Building'] = '0|=|大樓編輯失敗。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Floor'] = '0|=|位置編輯失敗。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Room'] = '0|=|子位置編輯失敗。';

$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Building'] = '1|=|大樓刪除成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Floor'] = '1|=|位置刪除成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Room'] = '1|=|子位置刪除成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Building'] = '0|=|大樓刪除失敗。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Floor'] = '0|=|位置刪除失敗。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Room'] = '0|=|子位置刪除失敗。';

$Lang['SysMgr']['Location']['FieldTitle']['TotalNumOfSubLocation'] = '子位置總數';

$Lang['SysMgr']['Location']['ImportColumn'][] = "<font color='red'>*</font>位置代號";
$Lang['SysMgr']['Location']['ImportColumn'][] = "<font color='red'>*</font>位置條碼";
$Lang['SysMgr']['Location']['ImportColumn'][] = "<font color='red'>*</font>位置名稱(英文)";
$Lang['SysMgr']['Location']['ImportColumn'][] = "<font color='red'>*</font>位置名稱(中文)";
$Lang['SysMgr']['Location']['ImportColumn'][] = "<font color='red'>*</font>子位置代號";
$Lang['SysMgr']['Location']['ImportColumn'][] = "<font color='red'>*</font>子位置條碼";
$Lang['SysMgr']['Location']['ImportColumn'][] = "<font color='red'>*</font>子位置名稱(英文)";
$Lang['SysMgr']['Location']['ImportColumn'][] = "<font color='red'>*</font>子位置名稱(中文)";
$Lang['SysMgr']['Location']['ImportColumn'][] = "子位置描述";
$Lang['SysMgr']['Location']['ImportColumn'][] = "子位置容量";
$Lang['SysMgr']['Location']['AdditionalImportColumn']['CourseCategory'] = "子位置適合的課程類別(用##分隔)";
$Lang['SysMgr']['Location']['LocationBarCodeDuplicate'] = "位置條碼重複";
$Lang['SysMgr']['Location']['CannotFindRelatedLocationBarcodeFromDB'] = "找不到相對應的位置條碼";
$Lang['SysMgr']['Location']['SubLocationBarCodeDuplicate'] = "子位置條碼重複";
$Lang['SysMgr']['Location']['CannotFindRelatedLocationBarcodeFromDB'] = "找不到相對應的子位置條碼";
$Lang['SysMgr']['Location']['SubLocationCodeDuplicate'] = "子位置代號重複";
$Lang['SysMgr']['Location']['CannotFindRelatedSubLocationCodeFromDB'] = "找不到相對應的子位置代號";
$Lang['SysMgr']['Location']['LocationCodeLengthError'] = "位置代號不能超過十位數字";
$Lang['SysMgr']['Location']['LocationBarCodeLengthError'] = "位置條碼不能超過十位數字";
$Lang['SysMgr']['Location']['LocationBarCodeInvalidFormat'] = "位置條碼格式不正確";
$Lang['SysMgr']['Location']['SubLocationCodeLengthError'] = "子位置代號不能超過十位數字";
$Lang['SysMgr']['Location']['SubLocationBarCodeLengthError'] = "子位置條碼不能超過十位數字";
$Lang['SysMgr']['Location']['SubLocationBarCodeInvalidFormat'] = "子位置條碼格式不正確";
$Lang['SysMgr']['Location']['SubLocationCapacityInvalidFormat'] = "子位置容量格式不正確";
$Lang['SysMgr']['Location']['CannotFindCourseCategory'] = "找不到課程類別";

$Lang['SysMgr']['Location']['CardReaderArr']['CardReader'] = "門監系統";
$Lang['SysMgr']['Location']['CardReader']['ReaderInfo']= "讀卡器資料";
$Lang['SysMgr']['Location']['CardReader']['Location'] = "位置";
$Lang['SysMgr']['Location']['CardReader']['Code'] = "讀卡器編號";
$Lang['SysMgr']['Location']['CardReader']['ReaderName'] = "讀卡器名稱";
$Lang['SysMgr']['Location']['CardReader']['Remarks'] = "備註";
$Lang['SysMgr']['Location']['CardReader']['Status'] = "狀況";
$Lang['SysMgr']['Location']['CardReader']['EmptyArr']['ReaderNameEng'] = '請輸入讀卡器英文名稱。';
$Lang['SysMgr']['Location']['CardReader']['EmptyArr']['ReaderNameChi'] = '請輸入讀卡器中文名稱。';
$Lang['SysMgr']['Location']['CardReader']['EmptyArr']['ReaderID'] = '請輸入讀卡器編號。';
$Lang['SysMgr']['Location']['CardReader']['EmptyArr']['LocationID'] = '請選擇位置。';
$Lang['SysMgr']['Location']['CardReader']['EmptyArr']['ReaderIDExist'] = '讀卡器編號不能與現有讀卡器編號相同';
$Lang['SysMgr']['Location']['CardReader']['MoreThanTwo']['LocationID'] = '同一位置不能被使用兩次或以上';
$Lang['SysMgr']['Location']['CardReader']['Confirm']['Enable'] = '你是否確認要啟用所選項目?';
$Lang['SysMgr']['Location']['CardReader']['Confirm']['Disable'] = '你是否確認要停用所選項目?';

$Lang['SysMgr']['CycleDay']['CycleDayTitle'] = "循環日";
$Lang['SysMgr']['CycleDay']['NewPeriod'] = "新增時區";
$Lang['SysMgr']['CycleDay']['NewTimeZoneHere'] = "在此新增時區";
$Lang['SysMgr']['CycleDay']['EditPeriod'] = "編輯時區";
$Lang['SysMgr']['CycleDay']['EditThisTimeZone'] = "編輯此時區";
$Lang['SysMgr']['CycleDay']['DeletePeriod'] = "刪除時區";
$Lang['SysMgr']['CycleDay']['PeriodTitle'] = "日期範圍";
$Lang['SysMgr']['CycleDay']['PeriodStart']['FieldTitle'] = "開始日期";
$Lang['SysMgr']['CycleDay']['PeriodEnd']['FieldTitle'] = "結束日期";
$Lang['SysMgr']['CycleDay']['PeriodType']['FieldTitle'] = "類型";
$Lang['SysMgr']['CycleDay']['CycleType']['FieldTitle'] = "循環日編號";
$Lang['SysMgr']['CycleDay']['DaysOfACycle']['FieldTitle'] = "循環日數";
$Lang['SysMgr']['CycleDay']['FirstDay']['FieldTitle'] = "起始日";
$Lang['SysMgr']['CycleDay']['SaturdayCounted']['FieldTitle'] = "計算星期六";
$Lang['SysMgr']['CycleDay']['SkipOnWeekday']['FieldTitle'] = "不計算(星期)";
$Lang['SysMgr']['CycleDay']['BackgroundColor']['FieldTitle'] = "顯示顏色";
$Lang['SysMgr']['CycleDay']['SaturdayCounted']['Yes'] = "是";
$Lang['SysMgr']['CycleDay']['SaturdayCounted']['No'] = "否";
$Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle'] = "週日";
$Lang['SysMgr']['CycleDay']['PeriodType']['HaveCycle'] = "循環日";
$Lang['SysMgr']['CycleDay']['PeriodType']['Generation'] = "一般計法";
$Lang['SysMgr']['CycleDay']['PeriodType']['FileImport'] = "檔案匯入";
$Lang['SysMgr']['CycleDay']['CycleType']['Numeric'] = "數字 (1, 2, 3 ...)";
$Lang['SysMgr']['CycleDay']['CycleType']['Alphabets'] = "字母 (A, B, C ...)";
$Lang['SysMgr']['CycleDay']['CycleType']['Roman'] = "羅馬數字 (I, II, III ...)";
$Lang['SysMgr']['CycleDay']['FileImport']['Description'] = "<u><b>檔案說明:</b></u><br>
第一欄: 日期 (YYYY-MM-DD)<br>
第二欄: 中文介面的顯示 (如: 循環日 A)<br>
第三欄: 英文介面的顯示 (如: Day A)<br>
第四欄: 日曆介面的顯示及定期資源預訂等功能使用 (如: A)<br>
";
$Lang['SysMgr']['CycleDay']['FileImport']['Warning'] = "在此日期內的匯入紀錄會被清除。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodStartEmpty'] = "請輸入開始日期。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndEmpty'] = "請輸入結束日期。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodStartInvalid'] = "開始日期不正確。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndInvalid'] = "結束日期不正確。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodOverlapped'] = "時段不能重疊。";
$Lang['SysMgr']['CycleDay']['JSWarning']['ConfirmDelete'] = "你是否確定刪除已選的時區？";
$Lang['SysMgr']['CycleDay']['JSWarning']['ConfirmDeleteWithSpecialTimetable'] = "如刪除此時區，所有此時區中的特別時間設定將被同時刪除。你是否確定刪除已選的時區？";
$Lang['SysMgr']['CycleDay']['JSWarning']['StartPeriodLargerThenEndPeriod'] = "結束日期不能早於開始日期。";
$Lang['SysMgr']['CycleDay']['JSWarning']['SelectAtLeaseOnePeriod'] = '請選擇最少一個時段。';
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodIsNotInTheSelectedSchoolYear'] = '時段並不是在選擇學年的範圍內。';
$Lang['SysMgr']['CycleDay']['CycleName'] = "名稱";
$Lang['SysMgr']['CycleDay']['CycleShortForm'] = "簡稱";
$Lang['SysMgr']['CycleDay']['GenerateProduction']['ReturnSuccess'] = '1|=|已成功轉至使用。';
$Lang['SysMgr']['CycleDay']['GenerateProduction']['ReturnFail'] = '0|=|未能成功轉至使用。';
$Lang['SysMgr']['CycleDay']['GeneratePreview']['ReturnSuccess'] = '1|=|已成功產生預覽。';
$Lang['SysMgr']['CycleDay']['GeneratePreview']['ReturnFail'] = '0|=|未能成功產生預覽。';
$Lang['SysMgr']['CycleDay']['BackToProductionView'] = "返回使用中模式。";
$Lang['SysMgr']['CycleDay']['PeriodManagement'] = "時段管理模式";
$Lang['SysMgr']['CycleDay']['ViewBy']['Title'] = "檢視";
$Lang['SysMgr']['CycleDay']['ViewBy']['ViewByCalendar'] = "日曆";
$Lang['SysMgr']['CycleDay']['ViewBy']['ViewByList'] = "列表";
$Lang['SysMgr']['CycleDay']['DefinePeriod'] = "定義日期範圍";
$Lang['SysMgr']['CycleDay']['GenerateAndCheckPreview'] = "產生及檢視預覽";
$Lang['SysMgr']['CycleDay']['MakeCurrentPreviewToProduction'] = "把預覽轉至使用";
$Lang['SysMgr']['CycleDay']['Publish'] = "發佈";
$Lang['SysMgr']['CycleDay']['Preview'] = "預覽";
$Lang['SysMgr']['CycleDay']['ManagementMode'] = "管理模式";
$Lang['SysMgr']['CycleDay']['LastPublishDate'] = "最後發佈日期";
$Lang['SysMgr']['CycleDay']['LastModifiedDate'] = "最後修改日期";
$Lang['SysMgr']['CycleDay']['SettingTitle'] = "時區";
$Lang['SysMgr']['CycleDay']['SpecialTimetableSettings'] = "特別時間表設定";
$Lang['SysMgr']['CycleDay']['AddSpecialTimetable'] = "新增特別時間表";
$Lang['SysMgr']['CycleDay']['SpecialTimetable'] = "特別時間表";
$Lang['SysMgr']['CycleDay']['DisplayColour'] = "顯示顏色";
$Lang['SysMgr']['CycleDay']['RepeatType'] = "重複類型";
$Lang['SysMgr']['CycleDay']['RepeatEvery'] = "每逢";
$Lang['SysMgr']['CycleDay']['SelectedDates'] = "選擇日期";
$Lang['SysMgr']['CycleDay']['NoDateMessage'] = "沒有日期符合已選擇的條件";
$Lang['SysMgr']['CycleDay']['TheDateHasSpecialTimetableSettingsAlready'] = "代表該日子已設有特別時間表";
$Lang['SysMgr']['CycleDay']['AddNewSettings'] = "加入新設定";
$Lang['SysMgr']['CycleDay']['EditThisSettings'] = "編輯此設定";
$Lang['SysMgr']['CycleDay']['FirstDayOfTimezone'] = "代表此日子為該時區的第一日。";
$Lang['SysMgr']['CycleDay']['FirstDayOfSpecialTimetableSettings'] = "代表此日子為該特別時間表設定的第一日。";
$Lang['SysMgr']['CycleDay']['BothFirstDayOfTimezoneAndSpecialTimetableSettings'] = "代表此日子同時為該時區及該特別時間表設定的第一日。";
$Lang['SysMgr']['CycleDay']['RemarkSpecialTimetable'] = "(若使用週日設定，時間表之日數設定需為7日。若使用循環日設定，其日數必須等於循環日數。)";

$Lang['SysMgr']['CycleDay']['ReturnMsg']['SpecialTimetableSettingsSavedSuccess'] = "1|=|已儲存特別時間表設定。";
$Lang['SysMgr']['CycleDay']['ReturnMsg']['SpecialTimetableSettingsSavedFailed'] = "0|=|儲存特別時間表設定失敗。";
$Lang['SysMgr']['CycleDay']['ReturnMsg']['SpecialTimetableSettingsDeleteSuccess'] = "1|=|已刪除特別時間表設定。";
$Lang['SysMgr']['CycleDay']['ReturnMsg']['SpecialTimetableSettingsDeleteFailed'] = "0|=|刪除特別時間表設定失敗。";

$Lang['SysMgr']['CycleDay']['jsWarningArr']['SelectDate'] = "請選擇最少一個日子。";
$Lang['SysMgr']['CycleDay']['jsWarningArr']['SelectTimetable'] = "請選擇時間表。";
$Lang['SysMgr']['CycleDay']['jsWarningArr']['DeleteSpecialTimetableSettings'] = "如你選擇刪除此特別時間表設定，所有此設定的已選日期將被刪除，而這些資料將無法復原。你是否確定要刪除此特別時間表設定？";
$Lang['SysMgr']['CycleDay']['jsWarningArr']['AddEventHoliday'] = "你是否確定要增加此事項 / 假期？";
$Lang['SysMgr']['CycleDay']['jsWarningArr']['UpdateEventHoliday'] = "你是否確定要編輯此事項 / 假期？";

$Lang['SysMgr']['CycleDay']['WarningArr']['EditCycleDayTimezoneWithSpecialTimetable'] = "因此時區已有特別時間表設定，所以現時未能轉換「".$Lang['SysMgr']['CycleDay']['PeriodType']['FieldTitle']."」、「".$Lang['SysMgr']['CycleDay']['DaysOfACycle']['FieldTitle']."」、「".$Lang['SysMgr']['CycleDay']['FirstDay']['FieldTitle']."」及「".$Lang['SysMgr']['CycleDay']['SkipOnWeekday']['FieldTitle']."」之設定。 如需更改有關設定，請先刪除此時區內的特別時間表設定。";
$Lang['SysMgr']['CycleDay']['WarningArr']['EditTimezoneDayRangeWithSpecialTimetable'] = "如時區日期範圍有變，而某些日子將不在新的日期範圍內，系統將自動刪除該日子的特別時間表設定。";
$Lang['SysMgr']['CycleDay']['WarningArr']['NewHolidayOrEventWithSpecialTimetable'] = "此假期 / 事項的日期範圍內有特別時間表設定，如你設定「不適用於上課日」為「是」，系統將因應有關時區內的特別時間表之重複日設定重新製作一次特別時間表日期。";
$Lang['SysMgr']['CycleDay']['WarningArr']['EditHolidayOrEventWithSpecialTimetable'] = "此假期 / 事項的日期範圍內有特別時間表設定，如你更改「不適用於上課日」的設定，系統將因應有關時區內的特別時間表之重複日設定重新製作一次特別時間表日期。";
$Lang['SysMgr']['CycleDay']['WarningArr']['ImportHolidayOrEventWithSpecialTimetable'] = "此假期 / 事項的日期範圍內有特別時間表設定，如你設定「不適用於上課日」為「Y」，系統將因應有關時區內的特別時間表之重複日設定重新製作一次特別時間表日期。";
$Lang['SysMgr']['CycleDay']['WarningArr']['DeleteHolidayOrEventWithSpecialTimetable'] = "此假期 / 事項的日期範圍內有特別時間表設定，如你刪除此假期 / 事項，系統將因應有關時區內的特別時間表之重複日設定重新製作一次特別時間表日期。";

$Lang['SysMgr']['GroupMessageBatchCreate']['BatchCheckClassWarning1'] = '可新增/修改{number}個班別小組：';
$Lang['SysMgr']['GroupMessageBatchCreate']['BatchCheckClassWarning2'] = '{number}個班別小組未能新增：';
$Lang['SysMgr']['GroupMessageBatchCreate']['BatchCheckClassWarning3'] = '小組數目或組員數目限額已滿';

$Lang['SysMgr']['SchoolCalendar']['ModuleTitle'] = "校曆表";
$Lang['SysMgr']['SchoolCalendar']['SettingTitle'] = "假期 / 事項";
$Lang['SysMgr']['SchoolCalendar']['NewHolidaysOrEvents'] = "新增假期 / 事項";
$Lang['SysMgr']['SchoolCalendar']['EventType'][0] = "學校事項";
$Lang['SysMgr']['SchoolCalendar']['EventType'][1] = "教學事項";
$Lang['SysMgr']['SchoolCalendar']['EventType'][2] = "小組事項";
$Lang['SysMgr']['SchoolCalendar']['EventType'][3] = "公眾假期";
$Lang['SysMgr']['SchoolCalendar']['EventType'][4] = "學校假期";
$Lang['SysMgr']['SchoolCalendar']['SelectType'] = "- 選擇種類 -";
$Lang['SysMgr']['SchoolCalendar']['EventStatus']['Publish'] = "已發佈";
$Lang['SysMgr']['SchoolCalendar']['EventStatus']['Pending'] = "未發佈";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDate'] = "日期";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventStartDate'] = "開始日期";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventEndDate'] = "結束日期";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventType'] = "種類";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitle'] = "標題";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitleEng'] = "標題（英文）";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenue'] = "地點";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenueEng'] = "地點（英文）";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNature'] = "假期 / 事項性質";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNatureEng'] = "假期 / 事項性質 （英文）";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescription'] = "描述";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescriptionEng'] = "描述 (英文)";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipCycleDay'] = "不適用於上課日";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipSAT'] = "不適用於星期六";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipSUN'] = "不適用於星期日";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventStatus'] = "狀況";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes'] = "是";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No'] = "否";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SchoolHolidayEvent'] = "學校假期 / 事項";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['None'] = "沒有";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['School'] = "有";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['Applicable'] = "適用";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['NotApplicable'] = "不適用";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SelectTimeTable'] = "-- 選擇時間表格式 --";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['TimeTable'] = "添附此時間表";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ApplyToRelatedEvents'] = "應用於所有有關的假期 / 事項";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['To'] = "至";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['NotCountInCycleDay'] = "不計算循環日";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CountInCycleDay'] = "計算循環日";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AcademicYearIsNotSet'] = "0|=|請先完成學年設定。";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CurrentDateNotInTerm'] = "0|=|請先檢查學年設定是否正確。";
$Lang['SysMgr']['SchoolCalendar']['Event']['Edit'] = "編輯";
$Lang['SysMgr']['SchoolCalendar']['Event']['EditHolidayEvent'] = "編輯假期 / 事項";
$Lang['SysMgr']['SchoolCalendar']['ToolTip']['DeleteRelatedHolidaysEvents'] = "刪除相關假期 / 事項";
$Lang['SysMgr']['SchoolCalendar']['ToolTip']['Expand'] = "顯示所有";
$Lang['SysMgr']['SchoolCalendar']['ToolTip']['Hide'] = "顯示部分";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['ConfirmDelete'] = "你是否確定要刪除此假期 / 事項？";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['ConfirmDeleteRelatedEvents'] = "你是否確定要刪除所有相關的假期 / 事項？";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectEventType'] = "請選擇種類。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InputEventTitle'] = "請輸入標題。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectTimeTable'] = "請選擇時間表格式。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['DateRangeNotInSameTerm'] = "日期範圍並不是在同一學期內。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InputLocationName'] = "請輸入地點。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectLocation'] = "請選擇地點。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EventEndDateIsEmpty'] = "請輸入結束日期。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InvalidEventEndDate'] = "請輸入有效的結束日期。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EndDateLaterThanStartDate'] = "結束日期不能早於開始日期。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EventCannotOverTheSchoolYear'] = "假期 / 事項必須設定於同一學年內。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectTheGroup'] = "請選擇小組。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectClassGroup'] = "請選擇班別小組。";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Group'] = "小組";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ImportHolidayOrEvent'] = "匯入假期 / 事項";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ExportHolidayOrEvent'] = "匯出假期 / 事項";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['SelectImportFile'] = "選擇 CSV 或 TXT 檔案";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ConfirmImportRecord'] = "確認匯入資料";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ImportedResult'] = "匯入結果";
$Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Submit'] = "呈送";
$Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Cancel'] = "取消";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['DataColumn'] = "資料欄";

/*
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][1] = "<font color=red>*</font> 開始日期 (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][2] = "<font color=red>*</font> 結束日期 (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][3] = "<font color=red>*</font> 種類 (SE - 學校事項，AE - 教學事項，PH - 公眾假期，SH - 學校假期，GE - 小組事項)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][4] = "<font color=red>*</font> 標題";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][5] = "<font color=red>^</font> 地點";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][6] = "假期 / 事項性質";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][7] = "描述";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][8] = "<font color=red>*</font> 不適用於上課日";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][9] = "<font color=red>**</font> 小組編號";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][10] = "<font color=red>*</font> 不適用於星期六";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][11] = "<font color=red>*</font> 不適用於星期日";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][12] = " 班別小組編號";*/

$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][1] = "<font color=red>*</font> 開始日期 (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][2] = "<font color=red>*</font> 結束日期 (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][3] = "<font color=red>*</font> 種類 (SE - 學校事項，AE - 教學事項，PH - 公眾假期，SH - 學校假期，GE - 小組事項)";
// $Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][4] = "<font color=red>*</font> 運動種類";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][4] = "協會";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][5] = "協會（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][6] = "<font color=red>*</font> 標題";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][7] = "<font color=red>*</font> 標題（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][8] = "<font color=red>^</font> 地點";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][9] = "<font color=red>^</font> 地點（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][10] = "假期 / 事項性質";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][11] = "假期 / 事項性質（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][12] = "描述";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][13] = "描述（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][14] = "<font color=red>*</font> 不適用於上課日";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][15] = "<font color=red>**</font> 小組編號";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][16] = "<font color=red>*</font> 不適用於星期六";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][17] = "<font color=red>*</font> 不適用於星期日";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][18] = "電郵";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][19] = "聯絡電話";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][20] = "網站";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][21] = " 班別小組編號";

$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][1] = "<font color=red>*</font> 開始日期 (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][2] = "<font color=red>*</font> 結束日期 (YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][3] = "<font color=red>*</font> 種類 (SE - 學校事項，AE - 教學事項，PH - 公眾假期，SH - 學校假期，GE - 小組事項)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][4] = "<font color=red>*</font> 標題";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][5] = "<font color=red>*</font> 標題（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][6] = "<font color=red>^</font> 地點";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][7] = "<font color=red>^</font> 地點（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][8] = "假期 / 事項性質";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][9] = "假期 / 事項性質（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][10] = "描述";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][11] = "描述（英文）";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][12] = "<font color=red>*</font> 不適用於上課日";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][13] = "<font color=red>**</font> 小組編號";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][14] = "<font color=red>*</font> 不適用於星期六";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][15] = "<font color=red>*</font> 不適用於星期日";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][16] = " 班別小組編號";


$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['Remark'] = "備註";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['DataMissing'] = "資料不足。";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['StartDateInvalid'] = "開始日期無效。";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['EndDateInvalid'] = "結束日期無效。";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['TypeInvalid'] = "種類無效。";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['TypeNotWithGroupID'] = "種類無效 (不是小組事項)";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSchoolDayInvalid'] = "不適用於上課日無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSATInvalid'] = "不適用於星期六無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSUNInvalid'] = "不適用於星期日無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['VenueInvalid'] = "地點無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['ClassGroupCodeInvalid'] = "班別小組編號無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SportTypeInvalid'] = "運動種類編號無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['Result']['Success'] = "匯入成功";
$Lang['SysMgr']['SchoolCalendar']['Import']['Result']['Fail'] = "匯入失敗";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['RequiredField'] = "附有「<span class='tabletextrequire'>*</span>」的項目必須填寫";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['VenueFormatExplanation'] = "<span class='tabletextrequire'>^</span> 如是校內場所, 請使用 \"大樓代號>位置代號>子位置代號\" 為資料格式。如是校外地方, 請使用 \"Others>場所名稱\" 為資料格式。";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['GroupIDFormatExplanation'] = "<span class='tabletextrequire'>**</span> 小組編號欄位只適用於GE - 小組事項，其他種類請空置該欄位。請使用\",\"分隔每個小組編號。";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['StartDate'] = "Start Date";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['EndDate'] = "End Date";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Type'] = "Type";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Title'] = "Title";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['TitleEng'] = "TitleEng";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Venue'] = "Venue";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['VenueEng'] = "VenueEng";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Nature'] = "Nature";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['NatureEng'] = "NatureEng";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['Description'] = "Description";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['DescriptionEng'] = "DescriptionEng";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSchoolDay'] = "Skip School Day";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['GroupID'] = "Group ID";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSAT'] = "Skip Saturday";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['SkipSUN'] = "Skip Sunday";
$Lang['SysMgr']['SchoolCalendar']['Export']['ColumnTitle']['ClassGroupCode'] = "Class Group Code";

$Lang['SysMgr']['SchoolCalendar']['Print']['Print'] = "列印";
$Lang['SysMgr']['SchoolCalendar']['Print']['Cancel'] = "取消";
$Lang['SysMgr']['SchoolCalendar']['Print']['PrintOptions'] = "列印選項";
$Lang['SysMgr']['SchoolCalendar']['Print']['PrintingColor'] = "列印色彩";
$Lang['SysMgr']['SchoolCalendar']['Print']['Black/White'] = "黑白";
$Lang['SysMgr']['SchoolCalendar']['Print']['Color'] = "彩色";
$Lang['SysMgr']['SchoolCalendar']['Print']['PrintMonthsWithoutEvents'] = "列印事件";
$Lang['SysMgr']['SchoolCalendar']['Print']['Yes'] = "是";
$Lang['SysMgr']['SchoolCalendar']['Print']['No'] = "否";
$Lang['SysMgr']['SchoolCalendar']['Print']['MaximumCalendarsPrintedPerPage'] = "每頁最多月份";
$Lang['SysMgr']['SchoolCalendar']['Print']['EventTitle'] = "事件標題";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['InvalidDateRange'] = "日期範圍無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['NotInCurrentSchoolYear'] = "該記錄不屬於本學年";
$Lang['SysMgr']['SchoolCalendar']['Import']['ReturnFail']['IncorrectFileExtention'] = "0|=|資料檔案無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ReturnFail']['InvalidFileFormat'] = "0|=|檔案格式無效";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenueCode'] = "地點編號";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['GroupID'] = "小組編號";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['GroupCategory'] = "類別";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['GroupIDError'] = "小組編號無效";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckSiteCode'] = "< 按此檢視場所編號 >";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckGroupCode'] = "< 按此檢視小組編號 >";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckClassGroupCode'] = "< 按此檢視班別小組編號 >";
$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['WarningMsg']['DeleteAllEventsHolidays'] = "以下所有事件及假期將被永久移除，而且無法恢復。如確定要刪除，請按<font style='font-size:large'>刪除全部</font>。";
$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['Summary'] = "總結";
$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfSchoolEvents'] = "學校事項總數";
$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfAcademicEvents'] = "教學事項總數";
$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfGroupEvents'] = "小組事項總數";
$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfPublicHolidays'] = "公眾假期總數";
$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfSchoolHolidays'] = "學校假期總數";
$Lang['SysMgr']['SchoolCalendar']['JSJSWarning']['DeleteAllEventsHolidays'] = "是否確定刪除所有假期 / 事項？";
$Lang['SysMgr']['SchoolCalendar']['ImportHolidaysEvent']['Remarks']['GroupID'] = '按此查詢小組編號';
$Lang['SysMgr']['SchoolCalendar']['ImportHolidaysEvent']['Remarks']['SiteCode'] = '按此查詢地點編號';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SportsType'] = '運動類型';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Association'] = '協會';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AssociationEng'] = '協會（英文）';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SFOCEventTitle'] = '項目名稱';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Time'] = '時間';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Email'] = '電郵';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ContactNo'] = '聯繫電話';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Website'] = '網站';
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ToBeDeterminedEvent'] = "待辦事項";
$Lang['SysMgr']['SchoolCalendar']['NormalSchoolDay'] = '正常上課日子';

$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'] = "學年 / 學期";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYear'] = "學年";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term'] = "學期";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearName'] = "名稱";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearNameChi'] = "中文名稱";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearNameEng'] = "英文名稱";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermName'] = "名稱";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermNameChi'] = "學期名稱 (中文)";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermNameEng'] = "學期名稱 (英文)";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermStartDate'] = "開始日期";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermEndDate'] = "結束日期";
$Lang['SysMgr']['AcademicYear']['ToolTip']['AcademicYearAddEdit'] = "新增 / 編輯學年";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear'] = "-- 所有學年 --";
$Lang['SysMgr']['AcademicYear']['ToolTip']['NewAcademicYear'] = "新增學年";
$Lang['SysMgr']['AcademicYear']['ToolTip']['MoveToArrangeDisplayOrder'] = "移動滑鼠以排列顯示次序";
$Lang['SysMgr']['AcademicYear']['ToolTip']['DeleteAcademicYear'] = "刪除學年";
$Lang['SysMgr']['AcademicYear']['ToolTip']['AddTerm'] = "新增學期";
$Lang['SysMgr']['AcademicYear']['ToolTip']['EditTerm'] = "編輯學期";
$Lang['SysMgr']['AcademicYear']['ToolTip']['DeleteTerm'] = "刪除學期";
$Lang['SysMgr']['AcademicYear']['JSWarning']['TermOverlapped'] = "學期不能重疊。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['AcademicYearOverlapped'] = "學年不能重複。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseTermNameIsEmpty'] = "請輸入中文名稱。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishTermNameIsEmpty'] = "請輸入英文名稱。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseAcademicYearNameIsEmpty'] = "請輸入中文名稱。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishAcademicYearNameIsEmpty'] = "請輸入英文名稱。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['StartDateIsEmpty'] = "請輸入開始日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EndDateIsEmpty'] = "請輸入結束日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['InvalidStartDate'] = "請輸入有效的開始日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['InvalidEndDate'] = "請輸入有效的結束日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EndDateLaterThanStartDate'] = "結束日期不能早於開始日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseTermNameIsDuplicate'] = "中文名稱不能重複。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishTermNameIsDuplicate'] = "英文名稱不能重複。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['InputTodayOrFutureOnly'] = '請輸入今天或將來的日期。';
$Lang['SysMgr']['AcademicYear']['Warning']['TermCannotEditOrDelete'] = "如要刪除學期，請先將其包含的資料刪除。";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['CurrentSchoolYear'] = "本學年";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings_NewStr'] = "學年及學期之間不可留有時間空隙。否則，學期範圍外的日子，系統將不能如常操作。<br>如上學期於1月31日結束，下學期必須於2月1日開始";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings']['Title'] = "重要學年/學期注意事項";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings']['Content'] = "輸入學期開始及結束日期時，請務必遵守以下兩項原則：<br><br>
1. 學期之間不可留有空間。例如：上學期於1月31日結束，下學期必須於2月1日開始。<br>
2. 上學期的開始日期，與下學期的結束日期(如果貴校有三個學期，則首學期的開始日期與最後一個學期的結束日期)，必須要包含一整年。例如：上學期自9月1日開始，下學期則在下一年8月31日結束。<br><br>
否則，不在學期範圍內的日子，系統功能將無法正常使用。因此，各學期的開始和結束日期，不應理解為學校學期的真正開始/結束日期，而是「可以使用系統」的日期範圍。";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantNote_TermSetting'] = "由於在未有包含在任何學期的日子，你將無法正常使用系統功能，請注意：<br><br>
1. 正確填寫開始日期：請緊貼上一學期的結束日期。如上一學期結束日期為1月31日，此學期的開始日期應為2月1日。<br><br>
2. 如此學期為學年內的最後一個學期：則此學期的結束日期，必須與首學期的開始日期，相隔一整年。例如：首學期由9月1日開始，則此學期應在下一年8月31日結束。";
$Lang['SysMgr']['AcademicYear']['Warning']['TermDateChecking'] = "由於在未有包含在任何學期的日子，你將無法正常使用系統功能。因此，請確保學期的開始日期緊貼上一學期的結束日期。如上一學期結束日期為1月31日，此學期的開始日期應為2月1日。";

$Lang['SysMgr']['Periods']['FieldTitle']['PeriodsName'] = "時段名稱";
$Lang['SysMgr']['Periods']['FieldTitle']['Status'] = "使用狀況";
$Lang['SysMgr']['Periods']['FieldTitle']['PeriodRange'] = "日期範圍";
$Lang['SysMgr']['Periods']['FieldTitle']['PeriodSetting'] = "時段設定";
$Lang['SysMgr']['OrganizationInfo']['ConfirmDeleteBackgroundImage'] = '是否確定刪除現時首頁背景圖案';
$Lang['SysMgr']['OrganizationInfo']['ConfirmDeleteLogo'] = '是否確定刪除現時校徽';
//$Lang['SysMgr']['OrganizationInfo']['DeleteBackgroundImage'] = '刪除現時首頁背景圖案';
$Lang['SysMgr']['OrganizationInfo']['DeleteBackgroundImage'] = '刪除現時登入背景圖案';
//$Lang['SysMgr']['OrganizationInfo']['DeleteLogo'] = '刪除現時校徽';
$Lang['SysMgr']['OrganizationInfo']['DeleteLogo'] = '刪除現時學校校徽';
$Lang['SysMgr']['OrganizationInfo']['FileSizeExceedLimit'] = "檔案大小超出上限。";
$Lang['SysMgr']['OrganizationInfo']['InvalidFile'] = '檔案不正確。';
//$Lang['SysMgr']['OrganizationInfo']['BackgroundImage'] = "首頁背景圖案";
$Lang['SysMgr']['OrganizationInfo']['BackgroundImage'] = "登入背景圖案";
$Lang['SysMgr']['OrganizationInfo']['BackgroundImageInstruction'] = "建議圖像大小: 最少 W1220px * H1080px, 格式: PNG / JPG";
//$Lang['SysMgr']['OrganizationInfo']['Logo'] = "校徽";
$Lang['SysMgr']['OrganizationInfo']['Logo'] = "學校校徽";
$Lang['SysMgr']['OrganizationInfo']['LogoInstruction'] = "圖像只能以'.JPG'、'.JPEG'、'.GIF' 或'.PNG'的格式上載。大小不超過2M";
$Lang['SysMgr']['OrganizationInfo']['LogoInstruction2'] = "建議圖像大小: 不超過 W120px * H80px, 格式: PNG / JPG / GIF";
$Lang['SysMgr']['OrganizationInfo']['PleaseInputSchoolName'] = '請輸入學校名稱';


## Inventory
if($plugin['Inventory'])
{
	$Lang['eInventory']['Ownership']['School'] = "學校";
	$Lang['eInventory']['Ownership']['Government'] = "政府";
	$Lang['eInventory']['Ownership']['SponsoringBody'] = "辦學團體";

	$Lang['eInventory']['FieldTitle']['ExportBuildingCode'] = "Building Code";
	$Lang['eInventory']['FieldTitle']['ExportBuildingTitle'] = "Building";
	$Lang['eInventory']['FieldTitle']['ExportFloorCode'] = "Location Code";
	$Lang['eInventory']['FieldTitle']['ExportFloorTitle'] = "Location";
	$Lang['eInventory']['FieldTitle']['ExportRoomCode'] = "Sub-location Code";
	$Lang['eInventory']['FieldTitle']['ExportRoomTitle'] = "Sub-location";
	$Lang['eInventory']['FieldTitle']['UpdateItemLocation'] = "更改位置";
	$Lang['eInventory']['FieldTitle']['UpdateItemStatus'] = "更新狀況";
	$Lang['eInventory']['FieldTitle']['ItemRequestWriteOff'] = "申請報銷";
	$Lang['eInventory']['FieldTitle']['BulkItemRequestWriteOff'] = "大量申請報銷";

	$Lang['eInventory']['FieldTitle']['Import']['Single'][1]['FieldTitle'] = "第一欄 : 物品編號  <span class='tabletextremark'>(如未輸入, 系統會自動產生物品編號)</span>";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][1]['AjaxLinkTitle'] = "按此查詢物品編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][1]['AjaxLink'] = "ItemCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][2]['FieldTitle'] = "<span class='tabletextrequire'>@</span>第二欄 : 條碼  <span class='tabletextremark'>(如未輸入, 系統會自動產生條碼)</span>";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][3]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第三欄 : 物品名稱 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][4]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第四欄 : 物品名稱 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][5]['FieldTitle'] = "第五欄 : 詳細資料 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][6]['FieldTitle'] = "第六欄 : 詳細資料 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][7]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第七欄 : 類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][7]['AjaxLinkTitle'] = "按此查詢類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][7]['AjaxLink'] = "CategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][8]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第八欄 : 子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][8]['AjaxLinkTitle'] = "按此查詢子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][8]['AjaxLink'] = "SubCategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][9]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第九欄 : 管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][9]['AjaxLinkTitle'] = "按此查詢管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][9]['AjaxLink'] = "GroupCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][10]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十欄 : 大樓編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][10]['AjaxLinkTitle'] = "按此查詢大樓編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][10]['AjaxLink'] = "BuildingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][11]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十一欄 : 位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][11]['AjaxLinkTitle'] = "按此查詢位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][11]['AjaxLink'] = "LocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][12]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十二欄 : 子位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][12]['AjaxLinkTitle'] = "按此查詢子位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][12]['AjaxLink'] = "SubLocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][13]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十三欄 : 資金來源編號 (1)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][13]['AjaxLinkTitle'] = "按此查詢資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][13]['AjaxLink'] = "FundingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][14]['FieldTitle'] = "第十四欄 : 單位價格 (1)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][15]['FieldTitle'] = "第十五欄 : 資金來源編號 (2)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][15]['AjaxLinkTitle'] = "按此查詢資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][15]['AjaxLink'] = "FundingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][16]['FieldTitle'] = "第十六欄 : 單位價格2";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][17]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十七欄 : 擁有權 (1 - 學校, 2 - 政府, 3 - ". $Lang['eInventory']['Ownership']['SponsoringBody'] .")";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][18]['FieldTitle'] = "第十八欄 : 保用日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][19]['FieldTitle'] = "第十九欄 : 許可證";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][20]['FieldTitle'] = "第二十欄 : 序號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][21]['FieldTitle'] = "第二十一欄 : 牌子";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][22]['FieldTitle'] = "第二十二欄 : 供應商名稱";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][23]['FieldTitle'] = "第二十三欄 : 供應商聯絡";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][24]['FieldTitle'] = "第二十四欄 : 供應商資料";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][25]['FieldTitle'] = "第二十五欄 : 報價單編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][26]['FieldTitle'] = "第二十六欄 : 標書編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][27]['FieldTitle'] = "第二十七欄 : 發票編號";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][28]['FieldTitle'] =  ($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired'] ? "<span class='tabletextrequire'>*</span>" : "") . "第二十八欄 : 購入日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][29]['FieldTitle'] = "第二十九欄 : 購買金額 (單據總額)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][30]['FieldTitle'] = "第三十欄 : 維修資料";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][31]['FieldTitle'] = "第三十一欄 : 備註";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][32]['FieldTitle'] = "第三十二欄 : 數量";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][33]['FieldTitle'] = "第三十三欄 : 盤點設定 ('Y' 代表要; 'N' 代表不需; 留空代表跟系統設定 )";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][34]['FieldTitle'] = "第三十四欄 : 標籤 (請用逗號 {,} 作分隔, 最多 10 個標籤)";

	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][1]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第一欄 : 物品編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][1]['AjaxLinkTitle'] = "按此查詢物品編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][1]['AjaxLink'] = "ItemCode";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][2]['FieldTitle'] = "第二欄 : 條碼";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][3]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第三欄 : 物品名稱 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][4]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第四欄 : 物品名稱 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][5]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第五欄 : 詳細資料 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][6]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第六欄 : 詳細資料 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][7]['FieldTitle'] = "第七欄 : 類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][7]['AjaxLinkTitle'] = "按此查詢類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][7]['AjaxLink'] = "CategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][8]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第八欄 : 子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][8]['AjaxLinkTitle'] = "按此查詢子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][8]['AjaxLink'] = "SubCategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][9]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第九欄 : 管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][9]['AjaxLinkTitle'] = "按此查詢管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][9]['AjaxLink'] = "GroupCode";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][10]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十欄 : 大樓編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][10]['AjaxLinkTitle'] = "按此查詢大樓編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][10]['AjaxLink'] = "BuildingCode";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][11]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十一欄 : 位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][11]['AjaxLinkTitle'] = "按此查詢位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][11]['AjaxLink'] = "LocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][12]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十二欄 : 子位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][12]['AjaxLinkTitle'] = "按此查詢子位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][12]['AjaxLink'] = "SubLocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][13]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十三欄 : 資金來源編號 (1)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][13]['AjaxLinkTitle'] = "按此查詢資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][13]['AjaxLink'] = "FundingCode";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][14]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十四欄 : 單位價格 (1)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][15]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十五欄 : 資金來源編號 (2)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][15]['AjaxLinkTitle'] = "按此查詢資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][15]['AjaxLink'] = "FundingCode";

	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][16]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十六欄 : 單位價格2";

	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][17]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十七欄 : 擁有權 (1 - 學校, 2 - 政府, 3 - ". $Lang['eInventory']['Ownership']['SponsoringBody'] .")";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][18]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十八欄 : 保用日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][19]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十九欄 : 許可證";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][20]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十欄 : 序號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][21]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十一欄 : 牌子";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][22]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十二欄 : 供應商名稱";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][23]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十三欄 : 供應商聯絡";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][24]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十四欄 : 供應商資料";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][25]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十五欄 : 報價單編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][26]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十六欄 : 標書編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][27]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十七欄 : 發票編號";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][28]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十八欄 : 購入日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][29]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十九欄 : 購買金額 (單據總額)";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][30]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第三十欄 : 維修資料";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][31]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第三十一欄 : 備註";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][32]['FieldTitle'] = "第三十二欄 : 數量";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][33]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第三十三欄 : 盤點設定 ('Y' 代表要; 'N' 代表不需; 留空代表跟系統設定 )";
	$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][34]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第三十四欄 : 標籤 (請用逗號 {,} 作分隔, 最多 10 個標籤)";

	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][1]['FieldTitle'] = "第一欄 : 物品編號  <span class='tabletextremark'>(如未輸入, 系統會自動產生物品編號)</span>";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][1]['AjaxLinkTitle'] = "按此查詢物品編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][1]['AjaxLink'] = "ItemCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][2]['FieldTitle'] = "<span class='tabletextrequire'>@</span>第二欄 : 條碼  <span class='tabletextremark'>(如未輸入, 系統會自動產生條碼)</span>";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][3]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第三欄 : 物品名稱 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][4]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第四欄 : 物品名稱 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][5]['FieldTitle'] = "第五欄 : 詳細資料 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][6]['FieldTitle'] = "第六欄 : 詳細資料 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][7]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第七欄 : 類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][7]['AjaxLinkTitle'] = "按此查詢類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][7]['AjaxLink'] = "CategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][8]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第八欄 : 子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][8]['AjaxLinkTitle'] = "按此查詢子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][8]['AjaxLink'] = "SubCategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][9]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第九欄 : 管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][9]['AjaxLinkTitle'] = "按此查詢管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][9]['AjaxLink'] = "GroupCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][10]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十欄 : 大樓編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][10]['AjaxLinkTitle'] = "按此查詢大樓編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][10]['AjaxLink'] = "BuildingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][11]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十一欄 : 位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][11]['AjaxLinkTitle'] = "按此查詢位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][11]['AjaxLink'] = "LocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][12]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十二欄 : 子位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][12]['AjaxLinkTitle'] = "按此查詢子位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][12]['AjaxLink'] = "SubLocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][13]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十三欄 : 資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][13]['AjaxLinkTitle'] = "按此查詢資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][13]['AjaxLink'] = "FundingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][14]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十四欄 : 擁有權 (1 - 學校, 2 - 政府, 3 - ". $Lang['eInventory']['Ownership']['SponsoringBody'] .")";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][15]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十五欄 : 誤差總管";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][15]['AjaxLinkTitle'] = "按此查詢誤差總管";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][15]['AjaxLink'] = "VarianceManager";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][16]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十六欄 : 數量";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][17]['FieldTitle'] = "第十七欄 : 供應商名稱";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][18]['FieldTitle'] = "第十八欄 : 供應商聯絡";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][19]['FieldTitle'] = "第十九欄 : 供應商資料";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][20]['FieldTitle'] = "第二十欄 : 報價單編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][21]['FieldTitle'] = "第二十一欄 : 標書編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][22]['FieldTitle'] = "第二十二欄 : 發票編號";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][23]['FieldTitle'] = ($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired'] ? "<span class='tabletextrequire'>*</span>" : "") . "第二十三欄 : 購入日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][24]['FieldTitle'] = "第二十四欄 : 購買金額 (單據總額)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][25]['FieldTitle'] = "第二十五欄 : 單位價格";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][26]['FieldTitle'] = "第二十六欄 : 維修資料";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][27]['FieldTitle'] = "第二十七欄 : 備註";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][28]['FieldTitle'] = "第二十八欄 : 盤點設定 ('Y' 代表要; 'N' 代表不需; 留空代表跟系統設定 )";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][29]['FieldTitle'] = "第二十九欄 : 標籤 (請用逗號 {,} 作分隔, 最多 10 個標籤)";

	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][1]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第一欄 : 物品編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][1]['AjaxLinkTitle'] = "按此查詢物品編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][1]['AjaxLink'] = "ItemCode";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][2]['FieldTitle'] = "第二欄 : 條碼";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][3]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第三欄 : 物品名稱 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][4]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第四欄 : 物品名稱 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][5]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第五欄 : 詳細資料 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][6]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第六欄 : 詳細資料 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][7]['FieldTitle'] = "第七欄 : 類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][7]['AjaxLinkTitle'] = "按此查詢類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][7]['AjaxLink'] = "CategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][8]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第八欄 : 子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][8]['AjaxLinkTitle'] = "按此查詢子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][8]['AjaxLink'] = "SubCategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][9]['FieldTitle'] = "第九欄 : 管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][9]['AjaxLinkTitle'] = "按此查詢管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][9]['AjaxLink'] = "GroupCode";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][10]['FieldTitle'] = "第十欄 : 大樓編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][10]['AjaxLinkTitle'] = "按此查詢大樓編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][10]['AjaxLink'] = "BuildingCode";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][11]['FieldTitle'] = "第十一欄 : 位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][11]['AjaxLinkTitle'] = "按此查詢位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][11]['AjaxLink'] = "LocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][12]['FieldTitle'] = "第十二欄 : 子位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][12]['AjaxLinkTitle'] = "按此查詢子位置編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][12]['AjaxLink'] = "SubLocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][13]['FieldTitle'] = "第十三欄 : 資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][13]['AjaxLinkTitle'] = "按此查詢資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][13]['AjaxLink'] = "FundingCode";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][14]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十四欄 : 擁有權 (1 - 學校, 2 - 政府, 3 - ". $Lang['eInventory']['Ownership']['SponsoringBody'] .")";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][15]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第十五欄 : 誤差總管";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][15]['AjaxLinkTitle'] = "按此查詢誤差總管";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][15]['AjaxLink'] = "VarianceManager";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][16]['FieldTitle'] = "第十六欄 : 數量";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][17]['FieldTitle'] = "第十七欄 : 供應商名稱";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][18]['FieldTitle'] = "第十八欄 : 供應商聯絡";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][19]['FieldTitle'] = "第十九欄 : 供應商資料";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][20]['FieldTitle'] = "第二十欄 : 報價單編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][21]['FieldTitle'] = "第二十一欄 : 標書編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][22]['FieldTitle'] = "第二十二欄 : 發票編號";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][23]['FieldTitle'] = "第二十三欄 : 購入日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][24]['FieldTitle'] = "第二十四欄 : 購買金額 (單據總額)";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][25]['FieldTitle'] = "第二十五欄 : 單位價格";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][26]['FieldTitle'] = "第二十六欄 : 維修資料";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][27]['FieldTitle'] = "第二十七欄 : 備註";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][28]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十八欄 : 盤點設定 ('Y' 代表要; 'N' 代表不需; 留空代表跟系統設定 )";
	$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][29]['FieldTitle'] = "<span class='tabletextrequire'>#</span>第二十九欄 : 標籤 (請用逗號 {,} 作分隔, 最多 10 個標籤)";




	$Lang['eInventory']['FieldTitle']['Import']['Category'][0]['FieldTitle'] = "第一欄 : <span class='tabletextrequire'>*</span>類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['Category'][1]['FieldTitle'] = "第二欄 : <span class='tabletextrequire'>*</span>中文名稱";
	$Lang['eInventory']['FieldTitle']['Import']['Category'][2]['FieldTitle'] = "第三欄 : <span class='tabletextrequire'>*</span>英文名稱";
	$Lang['eInventory']['FieldTitle']['Import']['Category'][3]['FieldTitle'] = "第四欄 : <span class='tabletextrequire'>*</span>顯示次序";

	$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][0]['FieldTitle'] = "第一欄 : <span class='tabletextrequire'>*</span>類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][1]['FieldTitle'] = "第二欄 : <span class='tabletextrequire'>*</span>子類別編號";
	$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][2]['FieldTitle'] = "第三欄 : <span class='tabletextrequire'>*</span>中文名稱";
	$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][3]['FieldTitle'] = "第四欄 : <span class='tabletextrequire'>*</span>英文名稱";
	$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][4]['FieldTitle'] = "第五欄 : <span class='tabletextrequire'>*</span>許可證 (1 - 適用, 0 - 不適用) <span class='tabletextremark'>(只適用於單一物品)</span>";
	$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][5]['FieldTitle'] = "第六欄 : <span class='tabletextrequire'>*</span>保用證 (1 - 適用, 0 - 不適用) <span class='tabletextremark'>(只適用於單一物品)</span>";
	$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][6]['FieldTitle'] = "第七欄 : <span class='tabletextrequire'>*</span>序號 (1 - 適用, 0 - 不適用) <span class='tabletextremark'>(只適用於單一物品)</span>";
	$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][7]['FieldTitle'] = "第八欄 : 顯示次序";

	$Lang['eInventory']['FieldTitle']['Import']['ResourceMgmtGroup'][0]['FieldTitle'] = "第一欄 : <span class='tabletextrequire'>*</span>管理組別編號";
	$Lang['eInventory']['FieldTitle']['Import']['ResourceMgmtGroup'][1]['FieldTitle'] = "第二欄 : <span class='tabletextrequire'>*</span>小組名稱 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['ResourceMgmtGroup'][2]['FieldTitle'] = "第三欄 : <span class='tabletextrequire'>*</span>小組名稱 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['ResourceMgmtGroup'][3]['FieldTitle'] = "第四欄 : <span class='tabletextrequire'>*</span>顯示次序";

	$Lang['eInventory']['FieldTitle']['Import']['FundingSource'][0]['FieldTitle'] = "第一欄 : <span class='tabletextrequire'>*</span>資金來源編號";
	$Lang['eInventory']['FieldTitle']['Import']['FundingSource'][1]['FieldTitle'] = "第二欄 : <span class='tabletextrequire'>*</span>中文名稱";
	$Lang['eInventory']['FieldTitle']['Import']['FundingSource'][2]['FieldTitle'] = "第三欄 : <span class='tabletextrequire'>*</span>英文名稱";
	$Lang['eInventory']['FieldTitle']['Import']['FundingSource'][3]['FieldTitle'] = "第四欄 : <span class='tabletextrequire'>*</span>資金類別 (e.g School, Government, Sponsoring body)";
	$Lang['eInventory']['FieldTitle']['Import']['FundingSource'][4]['FieldTitle'] = "第五欄 : <span class='tabletextrequire'>*</span>顯示次序";

	$Lang['eInventory']['FieldTitle']['BuildingCode'] = "大樓編號";
	$Lang['eInventory']['FieldTitle']['BuildingName'] = "大樓名稱";
	$Lang['eInventory']['FieldTitle']['FloorCode'] = "位置編號";
	$Lang['eInventory']['FieldTitle']['FloorName'] = "位置名稱";
	$Lang['eInventory']['FieldTitle']['FloorNameEn'] = "位置名稱(英文)";
	$Lang['eInventory']['FieldTitle']['FloorNameChi'] = "位置名稱(中文)";
	$Lang['eInventory']['FieldTitle']['RoomCode'] = "子位置編號";
	$Lang['eInventory']['FieldTitle']['RoomName'] = "子位置名稱";
	$Lang['eInventory']['FieldTitle']['RoomNameEn'] = "子位置名稱(英文)";
	$Lang['eInventory']['FieldTitle']['RoomNameChi'] = "子位置名稱(中文)";
	$Lang['eInventory']['FieldTitle']['Import']['CorrecrBuildingIndexReminder'] = " * 請小心核對csv匯入檔之大樓編號是否如上相同。";
	$Lang['eInventory']['FieldTitle']['Import']['CorrecrFloorIndexReminder'] = " * 請小心核對csv匯入檔之位置編號是否如上相同。";
	$Lang['eInventory']['FieldTitle']['Import']['CorrecrRoomIndexReminder'] = " * 請小心核對csv匯入檔之子位置編號是否如上相同。";
	$Lang['eInventory']['FieldTitle']['AllResourceMgmtGroup'] = " - 所有管理小組 - ";
	$Lang['eInventory']['FieldTitle']['AllFundingSource'] = " - 所有資金來源 - ";
	$Lang['eInventory']['FieldTitle']['AllCategory'] = " - 所有類別 - ";
	$Lang['eInventory']['FieldTitle']['AllType'] = " - 所有種類 - ";
	$Lang['eInventory']['FieldTitle']['StocktakeType']['AllItem'] = "顯示所有物品";
	$Lang['eInventory']['FieldTitle']['StocktakeType']['NotDone'] = "只顯示未完成盤點的";
	$Lang['eInventory']['FieldTitle']['DeletedStaff'] = "已離職職員";

	$Lang['eInventory']['ExportFieldTitle']['FloorCode']['EN'] = "Location Code";
	$Lang['eInventory']['ExportFieldTitle']['Barcode']['EN'] = "Location Barcode";
	$Lang['eInventory']['ExportFieldTitle']['FloorNameEn']['EN'] = "Location Name(En)";
	$Lang['eInventory']['ExportFieldTitle']['FloorNameChi']['EN'] = "Location Name(Chi)";
	$Lang['eInventory']['ExportFieldTitle']['RoomCode']['EN'] = "Sub-location Code";
	$Lang['eInventory']['ExportFieldTitle']['RoomBarcode']['EN'] = "Sub-location Barcode";
	$Lang['eInventory']['ExportFieldTitle']['RoomNameEn']['EN'] = "Sub-location Name(En)";
	$Lang['eInventory']['ExportFieldTitle']['RoomNameChi']['EN'] = "Sub-location Name(Chi)";
	$Lang['eInventory']['ExportFieldTitle']['RoomDescription']['EN'] = "Sub-location Description";
	$Lang['eInventory']['ExportFieldTitle']['FloorCode']['B5'] = "位置編號";
	$Lang['eInventory']['ExportFieldTitle']['Barcode']['B5'] = "位置條碼";
	$Lang['eInventory']['ExportFieldTitle']['FloorNameEn']['B5'] = "位置名稱(英文)";
	$Lang['eInventory']['ExportFieldTitle']['FloorNameChi']['B5'] = "位置名稱(中文)";
	$Lang['eInventory']['ExportFieldTitle']['RoomCode']['B5'] = "子位置編號";
	$Lang['eInventory']['ExportFieldTitle']['RoomBarcode']['B5'] = "子位置條碼";
	$Lang['eInventory']['ExportFieldTitle']['RoomNameEn']['B5'] = "子位置名稱(英文)";
	$Lang['eInventory']['ExportFieldTitle']['RoomNameChi']['B5'] = "子位置名稱(中文)";
	$Lang['eInventory']['ExportFieldTitle']['RoomDescription']['B5'] = "子位置描述";

	$Lang['eInventory']['FieldTitle']['Export']['BuildingCode'] = "Building Code";		// For inport use
	$Lang['eInventory']['FieldTitle']['Export']['Building'] = "Building";				// For inport use

	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Title'] = "離線模式";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Online']['Title'] = "在線模式";

	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step1'] = "選擇 CSV 或 TXT 檔案";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step2'] = "確認匯入資料";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step3'] = "匯入結果";

	$Lang['eInventory']['FieldTitle']['Report']['ShowFundingName'] = "顯示資金名稱";

	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['RowNum'] = "行#";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Location'] = "位置";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Category'] = "類別";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ItemName'] = "物品名稱";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Code'] = "條碼";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Quantity'] = "數量";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['StocktakeDate'] = "日期";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['StocktakeTime'] = "時間";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Remark'] = "備註";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Import'] = "匯入";

	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemShouldLocation'] = '該物品應該存放於';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['LocationNotFound'] = '位置不存在';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemNotFound'] = '未能在本系統找到相關物品，請刪除本行記錄並重新匯入';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidQty'] = '數量無效';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidDate'] = '日期無效';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidDateFormat'] = '日期格式無效';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidTime'] = '時間無效';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['IncorrectFileExtention'] = "0|=|資料檔案無效";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidFileFormat'] = "0|=|檔案格式無效";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['WrittenOff'] = '物品已報銷';

	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ImportMessage']['ReturnImportFail'] = "0|=|離線盤點記錄匯入失敗";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ImportMessage']['ReturnImportSuccess'] = "1|=|離線盤點記錄匯入成功";

	$Lang['eInventory']['FieldTitle']['DownloadOfflineReaderApplication'] = "下載條碼閱讀器設定程式";
	$Lang['eInventory']['FieldTitle']['DownloadofflineStocktakeSingleItemTemplate'] = "下載離線盤點種類單一物品範本(.txt)";
	$Lang['eInventory']['FieldTitle']['DownloadofflineStocktakeBulkItemTemplate'] = "下載離線盤點大量種類物品範本(.txt)";

	$Lang['eInventory']['FieldTitle']['OfflineStocktakeFormatArray'] = array();
	$Lang['eInventory']['FieldTitle']['ForSingleItem'] = "單一物品類別";
	$Lang['eInventory']['FieldTitle']['ForBulkItem'] = "大量物品種類別";
	$Lang['eInventory']['FieldTitle']['OfflineStocktakeFormatArray']['Single'] = array( "欄位 A : 位置條碼", "欄位 B : 條碼", "欄位 C : 盤點日期", "欄位 D : 盤點時間");
	$Lang['eInventory']['FieldTitle']['OfflineStocktakeFormatArray']['Bulk'] = array("欄位 A : 位置條碼", "欄位 B : 物品管理小組條碼", "欄位 C : 資金來源條碼", "欄位 D : 條碼",
	                                                                                 "欄位 E : 數量", "欄位 F : 盤點日期", "欄位 G : 盤點時間");

	$Lang['eInventory']['FieldTitle']['LocationBarcode'] = "位置條碼";
	$Lang['eInventory']['FieldTitle']['SubLocationBarcode'] = "子位置條碼";

	$Lang['eInventory']['FieldTitle']['EditInvoice'] = "編輯發票";
	$Lang['eInventory']['FieldTitle']['BarcodeWidth'] = "條碼闊度";
	$Lang['eInventory']['JSWarning']['InvalidBarcodeWidth'] = "條碼闊度無效";
	$i_InventorySystem_Item_Barcode_Num = "條碼(連條碼號碼)";

	$Lang['eInventory']['FieldTitle']['NoOfSubCategory'] = "子類別數量";

	$i_InventorySystem_Category2ImportError[20] = "此類別不是由電子資源預訂系統加入";
	$i_InventorySystem_Category2ImportError[21] = "此類別不是由資產管理行政系統加入";

	$Lang['eInventory']['WarningArr']['DataChangeAffect_eBooking'] = "資產管理行政系統及電子資源預訂系統使用相同的類別設定，如你更新類別設定，更新的設定將會同時影響資產管理行政系統及電子資源預訂系統。";
	$Lang['eInventory']['ReturnMessage']['DeleteUnsuccessNotEnoughQty'] = "報銷失敗 - 數量不足";

	$Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'] = "校長簽署";
	$Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate'] = "日期";

	$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'] = "總值";

	$Lang['eInventory']['UploadImages'] = "上載圖片";
	$Lang['eInventory']['UploadPhoto']['StepArr'][] = "上載相片";
	$Lang['eInventory']['UploadPhoto']['StepArr'][] = "上載完成";
	$Lang['eInventory']['Remarks'] = "備註";
	$Lang['eInventory']['File'] = "檔案";
	$Lang['eInventory']['RemarksList'][] = "相片只能以'.JPG'、'.GIF' 或'.PNG'的格式上載，相片大小也規定為100 X 100 pixel (闊 x 高)。";
	$Lang['eInventory']['RemarksList'][] = "可以以壓縮檔案(.zip)形式上載多個檔案";
	$Lang['eInventory']['RemarksList'][] = "壓縮檔案內不能存在文件夾";
	$Lang['eInventory']['RemarksList'][] = "相片的檔案名稱要與物品編號一樣。";
	$Lang['eInventory']['PhotoAttempt'] = "上載相片數";
	$Lang['eInventory']['UploadSuccess'] = "上載成功";
	$Lang['eInventory']['UploadFail'] = "上載失敗";
	$Lang['eInventory']['UploadMorePhoto'] = "上載更多相片";
	$Lang['eInventory']['WarnMsg']['PleaseSelectIMAGEorZIP'] = "請選擇JPEG檔(.jpg)，GIF檔(.gif)，PNG檔(.png)或壓縮檔(.zip)。";
	$Lang['eInventory']['BatchImage']['FilenameRemark'] = "如物品編號中含有\"/\"號，命名相片時請以\"+\"代替";

	$Lang['eInventory']['select_more_fields'] = "選擇顯示其他資料";
	$Lang['eInventory']['total_single_item_cost'] = "總值";

	$Lang['eInventory']['OrderByPurchaseDate'] = "按購買日期";
	$Lang['eInventory']['Ascending']="順序";
	$Lang['eInventory']['Decending']="倒序";

	$Lang['eInventory']['UpdateStatusErrorMsg'] = "你所輸入的物品數量大於/小於表列數量。請重新輸入。";
	$Lang['eInventory']['DisplayZeroRecord'] = "顯示數量為0的紀錄";

	$Lang['eInventory']['BasicInfo'] = "基本資料";
	$Lang['eInventory']['Details'] = "詳細資料";
	$Lang['eInventory']['ForSingleItemOnly'] = "只適用於單一物品";
	$Lang['eInventory']['NewQtyCannotGreaterthenOriQty'] = "新數量不可以多於原有數量";
	$Lang['eInventory']['NewQtyCannotZero'] = "新數量不可以等於 0 ";

	$Lang['eInventory']['Action']['ITEM_ACTION_REMOVE_LOCATION_ITEM'] = "刪除位置物品";

	$Lang['eInventory']['NoRightForEditThisItem'] = "你沒有權限修改這紀錄.";
	$Lang['eInventory']['SystemProperties'] = "系統屬性";
	$Lang['eInventory']['DoNotAllowGroupLeaderWriteOffItem'] = "不容許組長批核報銷物品";

	$Lang['eInventory']['CurrentLocation'] = "現時位置";
	$Lang['eInventory']['StocktakeLocation'] = "盤點位置";
	$Lang['eInventory']['DeleteLog'] = "資料刪除紀錄";
	$Lang['eInventory']['DeletedDate'] = "刪除日期";
	$Lang['eInventory']['DeletedBy'] = "刪除者";
	$Lang['eInventory']['RecordInfo'] = "紀錄資料";
	$Lang['eInventory']['Others'] = "其他";
	$Lang['eInventory']['SingleItem'] = "單一物品";
	$Lang['eInventory']['BulkItem'] = "大量物品";
	$Lang['eInventory']['SortBy'] = "排序";
	$Lang['eInventory']['JSWarning']['NewLocationSameAsOriginalLocation'] = "新位置不能與原有位置相同";

	$Lang['eInventory']['DisplayWrittenOffItem'] = "顯示已報銷物品";
	$Lang['eInventory']['DisplayNotReqStocktakingItem'] = "顯示不需盤點物品";
	$Lang['eInventory']['FundingType']['School'] = "學校";
	$Lang['eInventory']['FundingType']['Government'] = "政府";
	$Lang['eInventory']['FundingType']['SponsoringBody'] = "辦學團體";
	$Lang['eInventory']['FundingSource']['jsFundingSourceCheckBox'] = "請選擇資金來源";

	$Lang['eInventory']['FundingTypeAry'] = array(
			array(1,$Lang['eInventory']['FundingType']['School']),
			array(2,$Lang['eInventory']['FundingType']['Government']),
			array(3,$Lang['eInventory']['FundingType']['SponsoringBody']));

	$Lang['eInventory']['BarcodeMaxLength1'] = "條碼長度";
	$Lang['eInventory']['BarcodeMaxLength2'] = "字元";
	$Lang['eInventory']['InvalidUnitPrice'] = "單位價格無效";
	$Lang['eInventory']['InvalidPurchasePrice'] = "購買金額無效";
	$Lang['eInventory']['DuplicateFundingSource'] = "重複資金來源";
	$Lang['eInventory']['WarrantyExpiryWarningRemark'] = "(\"0\"代表不提示。)";

	##Generate Report
	$Lang['eInventory']['GenerationTime']="報表建立時間";

	$i_InventorySystem_Report_Col_Signature = "校監/校長簽署";
	$i_InventorySystem_Report_Col_Remarks = "備註";
	$i_InventorySystem_Report_Filter_Expired = "已到期";
	$i_InventorySystem_Report_Filter_Expire_in_15 = "15日內到期";
	$i_InventorySystem_Report_Filter_Expire_in_30 = "30日內到期";
	$i_InventorySystem_Report_Filter_Expire_in_50 = "50日內到期";
	$Lang['eInventory']['DateType'] = "日期類別";
	$Lang['eInventory']['WritOffWaitingForApproval'] = "這物品已被申請報銷並等待批核中";
	$Lang['eInventory']['CancelRequest'] = "取消申請";
	$Lang['eInventory']['JSWarning']['SureCancelRequest'] = "確定取消這物品報銷申請?";
	$Lang['eInventory']['NewQtyNotMatchStocktakeQty'] = "新數量與盤點數量不相符";

	$i_InventorySystem_VarianceHandling_Action[1] = "搬回原來位置";
	$i_InventorySystem_VarianceHandling_Action[2] = "重新設定位置";
	$i_InventorySystem_VarianceHandling_Action[3] = "要求報銷";
	$i_InventorySystem_VarianceHandling_Action[4] = "新增物品";
	$i_InventorySystem_VarianceHandling_Action[5] = "忽略";
	$i_InventorySystem_VarianceHandling_Action[6] = "在原來位置尋回物品";
	$i_InventorySystem_VarianceHandling_Action[7] = "報銷物品 (不需經過批核程序, 報銷原因: 盤點 - 找不到)";
	$i_InventorySystem_VarianceHandling_Action[8] = "已處理";
	$Lang['eInventory']['SearchByCodeDesc'] = "請輸入項目編號或項目名稱搜尋(e.g YWYW2_0001 或  打印機)";

	$i_InventorySystem['ByCategory'] = "顯示某類別的物品";
	$i_InventorySystem['ByLocation'] = "顯示某位置的物品";
	$i_InventorySystem['ByCaretaker'] = "顯示某物品管理小組的物品";
	$i_InventorySystem['BySearch'] = "顯示個別物品";

	$Lang['eInventory']['ItemLocation'] = "位置";
	$Lang['eInventory']['Category'] = "類別";
	$Lang['eInventory']['Caretaker'] = "管理小組";
	$Lang['eInventory']['ParticularItem'] = "物品編號";
	$Lang['eInventory']['AllLocation'] = "所有位置";
	$Lang['eInventory']['AllSubLocation'] = "所有子位置";
	$Lang['eInventory']['ParticularLocation'] = "指定位置";
	$Lang['eInventory']['AllCategory'] = "所有類別";
	$Lang['eInventory']['ParticularCategory'] = "指定類別";
	$Lang['eInventory']['AllCaretaker'] = "所有小組";
	$Lang['eInventory']['ParticularCaretaker'] = "指定小組";
	$Lang['eInventory']['WarningArr']['PurchaseEndDateCannotEarlierThanStartDate'] = "購買結束日期不能早於開始日期";
	$Lang['eInventory']['WarningArr']['WarrantyEndDateCannotEarlierThanStartDate'] = "保用結束日期不能早於開始日期";

	$Lang['eInventory']['InUse'] = "使用中";
	$Lang['eInventory']['NotInUse'] = "已停用";
	$Lang['eInventory']['Status'] = "狀況";
	$Lang['eInventory']['SetInUse'] = "設定為 \"使用中\"";
	$Lang['eInventory']['SetNotInUse'] = "設定為 \"已停用\"";
	$Lang['eInventory']['UpdateStatusConfirm'] = '是否確定要更新狀況？';
	$i_InventorySystem_NewItem_PurchasePrice_Help = "<p>若交易涉及折扣，「總購入金額」未必等同「單位價格」乘以「數量」。</p><p>「總購入金額」一般為發票上的總金額，亦是你為整項交易所繳付的金額。</p>";

	$i_InventorySystem['StockTakeScope'] = "盤點物品";
	$i_InventorySystem['StockTakePriceSingle'] = "盤點金額達[=PriceSingle=]之物品";
	$i_InventorySystem['StockTakePriceBulk'] = "盤點總值達[=PriceBulk=]之物品";
	$i_InventorySystem['StockTakeOptionPrice'] = "系統決定（根據系統設定之盤點金額）";
	$i_InventorySystem['StockTakeOptionCapital'] = "必需盤點（資本資產）";
	$i_InventorySystem['StockTakeOptionNo'] = "不需盤點（非資本資產）";

	$i_InventorySystem['StockTakePriceWarning'] = "金額值必需大過或者等於 0 ！";

	$Lang['eInventory']['OtherFilters'] = "其他選項";
	$Lang['eInventory']['CountStocktakeOnly'] = "按盤點";
	$Lang['eInventory']['CountByPrice'] = "按價格";
	$Lang['eInventory']['RemarkBoundary'] = "留空代表不限上限或下限";
	$Lang['eInventory']['BarcodeFormat2'] = "條碼格式";

	$Lang['eInventory']['DisplaySignature'] = "簽名";
	$Lang['eInventory']['DisplayColumns'] = "資料欄";
	$Lang['eInventory']['DisplayMisc'] = "其他";
	//$Lang['eInventory']['DisplaySignatureYes'] = "顯示";
	$Lang['eInventory']['SignatureSubmittedBy'] = "填表人";
	$Lang['eInventory']['SignatureCheckedBy'] = "核對人";
	$Lang['eInventory']['SignatureAudit'] = "審計師";

	$Lang['eInventory']['JSWarning']['WriteOffSingleItemsOnly'] = "此選項只適用於單一物品, 將不會顯示已選取的大量物品。";
	$Lang['eInventory']['WarningArr']['AtLeastChooseOneWriteOffSingleItems'] = "請選擇至少一個單一物品。";
	$Lang['eInventory']['WarningArr']['NoRightWriteOffSomeItems'] = "你沒有權限報銷部分物品。";
	$Lang['eInventory']['WarningArr']['NoRightWriteOffSelectedItems'] = "你沒有權限報銷所選物品。";
	$Lang['eInventory']['WarningArr']['WriteOffSingleItemsOnly']="已套用通用設定";
	$Lang['eInventory']['WarningArr']['AlreadyAppliedWriteOff']="此物品已經申請了報銷,不能更改位置";
	$Lang['eInventory']['GlobalSettingsToAllSingleItems'] = "通用設定(套用至以下所有單一物品)";
	$Lang['eInventory']['WarningArr']['CannotIgnoreAllItems']="不能忽略所有物品。";
	$Lang['eInventory']['NoOfSingleItemsWritOffWaitingForApproval'] = "件單一物品已被申請報銷並等待批核中。";
	$Lang['eInventory']['IgnoreThisItem'] ="忽略這物品";


	$Lang['Inventoty']['Settigs']['SetAsHelper'] = "設定為助手";
	$Lang['Inventoty']['Settigs']['Helper'] = "助手";
	$Lang['Inventoty']['Settigs']['ImportMemberDescriptionRow3'] = "第三欄 : 身分 (1 - 組長, 2 - 助手, 3 - 組員)";

	$i_InventorySystem_Item_Quantity_ExportTitle = "Quantity";

	$Lang['eInventory']['ItemStatus']['Borrowed'] = "已借出";
	$Lang['eInventory']['BookingRecords'] = "預訂紀錄";
	$Lang['eInventory']['BookingRecordsNo'] = "項紀錄";
	$Lang['eInventory']['UpdateItemStatusEmailToeBookingAdminRemark'] = "電子資源預訂系統管理員將會收到有關物品更改狀況的電郵通知";
	$Lang['eInventory']['UpdateItemStatusEmailToeBookingAdmin']['Subject'] = "[eInventory] Item [ITEM_NAME_EN] status updated";
	$Lang['eInventory']['UpdateItemStatusEmailToeBookingAdmin']['Content']['B5'] = "以下物品狀況更新了, 如有必要, 請通知已預訂該物品的預訂者。\n\n物品名稱: [ITEM_NAME_B5]\n位置: [ITEM_LOCATION_B5]\n物品管理小組: [ITEM_GROUP_B5]\n資金來源: [ITEM_FUNDING_B5]\n最新狀況: [ITEM_STATUS_B5]\n備註: [ITEM_REMARK]";
	$Lang['eInventory']['UpdateItemStatusEmailToeBookingAdmin']['Content']['EN'] = "The status of the following item has been updated, kindly inform the person who reserved this resource if necessary.\n\nItem: [ITEM_NAME_EN]\nLocation: [ITEM_LOCATION_EN]\nResource Management Group: [ITEM_GROUP_EN]\nFunding Source: [ITEM_FUNDING_EN]\nNew status: [ITEM_STATUS_EN]\nRemarks: [ITEM_REMARK]";
	$Lang['eInventory']['ItemStatus']['Normal']['EN'] = "Normal";
	$Lang['eInventory']['ItemStatus']['Normal']['B5'] = "正常";
	$Lang['eInventory']['ItemStatus']['Repairing']['EN'] = "Repairing";
	$Lang['eInventory']['ItemStatus']['Repairing']['B5'] = "維修中";
	$Lang['eInventory']['ItemStatus']['Damaged']['EN'] = "Damaged";
	$Lang['eInventory']['ItemStatus']['Damaged']['B5'] = "損壞";
	$Lang['eInventory']['ItemStatus']['WriteOff']['EN'] = "Write-off";
	$Lang['eInventory']['ItemStatus']['WriteOff']['B5'] = "已報銷";
	$Lang['eInventory']['ItemStatus']['Deleted']['EN'] = "Deleted";
	$Lang['eInventory']['ItemStatus']['Deleted']['B5'] = "已刪除";

	$Lang['eInventory']['ItemPhoto'] = "物品相片";
	$Lang['eInventory']['QLabelFormat'] = "QLabel/GoLabel 格式";
	$Lang['eInventory']['ExtraInformation'] = "其他資料";
	$Lang['eInventory']['Avg'] = "平均";

	$Lang['eInventory']['FieldTitle']['ResourceMgmtGroupBarcode'] = "物品管理小組條碼";
	$Lang['eInventory']['FieldTitle']['FundingSourceBarcode'] = "資金來源條碼";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidResourceMgmtGroup'] = "物品管理小組無效";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemShouldManagedBy'] = "物品應屬於";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidFundingSource'] = "資金來源無效";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemShouldFundedBy'] = "物品資金來源是";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ResourceMgmtGroup'] = "物品管理小組";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['FundingSource'] = "資金來源";

	$Lang['eInventory']['ExistingQty'] = "現存物品數量";
	$Lang['eInventory']['WriteOffQty'] = "已報銷物品數量";
	$Lang['eInventory']['DeletedInvoice'] = "已刪除的發票";
	$Lang['eInventory']['QuantityMismatchWithInvoice'] = "以下物品總數量(包括報銷)與其他資料中有效發票的物品總數量不符。";
	$Lang['eInventory']['Report']['StocktakeList'] = "固定資產盤點表";
	$Lang['eInventory']['Report']['StocktakePeriod'] = "盤點日期";
	$Lang['eInventory']['ItemStatus_Damaged'] = "損耗";
	$Lang['eInventory']['WaitingForApproval'] = "等待批核";
	$Lang['eInventory']['Handled'] = "已處理";
	$Lang['eInventory']['ApprovedBy'] = "批核者";
	$Lang['eInventory']['ApprovedDate'] = "批核日期";
	$Lang['eInventory']['ApprovedStatus'] = "批核狀態";
	$Lang['eInventory']['WriteOffRequestRestore'] = "復原報銷批核狀態";


	$Lang['eInventory']['NotEnoughQty'] = "數量不足";

	## customization wordings for SKH IP eInventory [Start]
	$Lang['eInventory']['GenearlDescription'] = "一般資料";
	$Lang['eInventory']['ResourceMgtGpName'] = "物品管理小組名稱";
	$Lang['eInventory']['SourceFundingCatCode'] = "資金來源類別";
	$Lang['eInventory']['ItemDetails'] = "物品詳情";
	$Lang['eInventory']['CurrentQty'] = "現存數量";
	$Lang['eInventory']['FollowupRemarks'] = "跟進 / 備註";
	$Lang['eInventory']['SotcktakeDate'] = "盤點日期";
	$Lang['eInventory']['SotcktakePICSignature'] = "盤點人簽署 (姓名)";
	$Lang['eInventory']['SotcktakeApproverSignature'] = "核對人(盤點統籌人員)簽署 (姓名)";
	$Lang['eInventory']['PrincipalSignature'] = "校長簽署 (姓名)";
	$Lang['eInventory']['SKH_StocktakeListRemark'] = "如發現物品沒有記錄在盤點表上，請於上表處填上有關資料。";
	## customization wordings for SKH IP eInventory [End]

	## customization wordings for PLKCHC IP eInventory [Start]
	$Lang['eInventory']['ItemCode_PLKCHC'] = "條碼";
	$Lang['eInventory']['ExpectedLocation_PLKCHC'] = "登記位置";
	$Lang['eInventory']['CurrentLocation_PLKCHC'] = "盤點位置";
	$Lang['eInventory']['Original_Quantity_PLKCHC'] = "預期數量";
	$Lang['eInventory']['VarianceQty_PLKCHC'] = "差異";
	$Lang['eInventory']['TotalPrice_PLKCHC'] = "總價格";
	$Lang['eInventory']['PurchaseDate_PLKCHC'] = "購置日期";
	$Lang['eInventory']['Item_Price_PLKCHC'] = "購買金額";
	$Lang['eInventory']['Item_Qty_PLKCHC'] = "現時數量";
	$Lang['eInventory']['Now_TotalPrice_PLKCHC'] = "現時總值";
	$Lang['eInventory']['Caretaker_PLKCHC'] = "管理小組";
	$Lang['eInventory']['RequestDate_PLKCHC'] = "申請日期";
	$Lang['eInventory']['WriteOffStatus_PLKCHC'] = "物品批核狀態";
	$Lang['eInventory']['ItemType_PLKCHC'] = "物品種類";
	$Lang['eInventory']['StockCheckPeriod_PLKCHC'] = "最後盤點時間";
	$Lang['eInventory']['Item_Price2_PLKCHC'] = "購置金額";
	## customization wordings for PLKCHC IP eInventory [End]

	$i_InventorySystem['Format'] = "格式";
	$i_InventorySystem['Default'] = "預設";
	$i_InventorySystem['Custom'] = "自定";
	$i_InventorySystem['BarcodeTemplates'] = "標籤模板";
	$i_InventorySystem['ManagementBarcodeTemplates'] = "管理標籤模板";
	$i_InventorySystem['CustomBarcodeFormats'] = "自定標籤格式";
	$i_InventorySystem['StartPrintPosition']= "起始位置";
	$i_InventorySystem['RecordNotFound']="物品沒有找到。";

	$Lang["eInventory"]["label"]["name"] = "標籤名稱";
	$Lang["eInventory"]["label"]["paper-size-width"] = "紙張闊度";
	$Lang["eInventory"]["label"]["paper-size-height"] = "紙張高度";
	$Lang["eInventory"]["label"]["NX"] = "一列標籤數量";
	$Lang["eInventory"]["label"]["NY"] = "一欄標籤數量";
	$Lang["eInventory"]["label"]["metric"] = "量度單位";
	$Lang["eInventory"]["label"]["metric_inch"] = "英寸";
	$Lang["eInventory"]["label"]["metric_mm"] = "毫米";
	$Lang["eInventory"]["label"]["unit"]["in"] = "英寸";
	$Lang["eInventory"]["label"]["unit"]["mm"] = "毫米";
	$Lang["eInventory"]["label"]["piece"] = "張";
	$Lang["eInventory"]["label"]["SpaceX"] = "標籤之間相距闊度";
	$Lang["eInventory"]["label"]["SpaceY"] = "標籤之間相距高度";
	$Lang["eInventory"]["label"]["width"] = "標籤寬度";
	$Lang["eInventory"]["label"]["height"] = "標籤高度";
	$Lang["eInventory"]["label"]["font_size"] = "字體大小";
	$Lang["eInventory"]["label"]["printing_margin_h"] = "標籤邊緣和文字之間相距闊度";
	$Lang["eInventory"]["label"]["printing_margin_v"] = "標籤邊緣和文字之間相距高度";
	$Lang["eInventory"]["label"]["max_barcode_width"] = "條碼闊度";
	$Lang["eInventory"]["label"]["max_barcode_height"] = "條碼高度";
	$Lang["eInventory"]["label"]["lMargin"] = "紙張和標籤左邊相距空間";
	$Lang["eInventory"]["label"]["tMargin"] = "紙張和標籤上方相距空間";
	$Lang["eInventory"]["label"]["input_msg"] = "請輸入數字e.g.(10.75)";
	$Lang["eInventory"]["label"]["lineHeight"] = "行距";
	$Lang["eInventory"]["label"]["info_order"] = "列印資料及次序";
	$Lang["eInventory"]["label"]["BookTitle"] = "書名 ";
	$Lang["eInventory"]["label"]["LocationCode"] = "位置";
	$Lang["eInventory"]["label"]["CallNumCallNum2"] = "索書號";
	$Lang["eInventory"]["label"]["BarCode"] = "條碼";
	$Lang["eInventory"]["label"]["BookCode"] = "登錄號碼";
	$Lang["eInventory"]["label"]["print_school_name"] = "學校名稱";
	$Lang["eInventory"]["label"]["msg"]["LabelAtLeastOne"] = "請選擇至少一個列印資料！";
	$Lang["eInventory"]["label"]["msg"]["no_template"] = "請先於 [管理標籤模板] 裡建立標籤格式!";
	$Lang['eInventory']['DeleteRecordsInDateRange'] = "刪除所選日期範圍內的所有紀錄";
	$Lang['eInventory']['ConfirmDeleteRecordsInDateRange'] = "確定要刪除所選日期範圍內的所有紀錄?";
	$Lang['eInventory']['StocktakeConductedDuringPeriod'] = "盤點期間才可進行盤點";
	$Lang['eInventory']['StocktakeVarianceHandlingConductedAfterPeriod'] = "盤點期過後，才可進行盤點誤差處理";
	$Lang['eInventory']['Confirm']['Submit'] = '你是否確定要呈交?';
	$Lang['eInventory']['HandleNotUndergoneStocktakeSingle'] = "處理未完成盤點的單一物品";
	$Lang['eInventory']['HandleNotUndergoneStocktakeBulk'] = "處理未完成盤點的大量物品";
	$Lang['eInventory']['SetStocktakeQuantityToZero'] = "設定盤點數量為 0";
	$Lang['eInventory']['SetStocktakeQuantityToExpected'] = "設定盤點數量為預期數量";
	$Lang['eInventory']['SingleItemNewQuantityNotMore100'] = "單一物品每次新增數量不能多於100";
	$Lang['eInventory']['ItemNewQuantityGreaterThan0'] = "數量不可少於1";
	$Lang['eInventory']['RequestWriteoffRemarks'] = "要求報銷的物品須於物品報銷批核進行處理。";
	$Lang['eInventory']['NewQtyNotMatchOriginalQty'] = "新數量與原有數量不相符";
	$Lang['eInventory']['TotalQtyMustMatchOriginalQty'] = "正常、損壞及維修中的數量總和必需等於原有數量。";
	$Lang['eInventory']['FixedAssetsStocktakeLocation'] = "盤點固定資產地點";
	$Lang['eInventory']['DataCorrect'] = "資料完全正確";
	$Lang['eInventory']['DataInCorrect'] = "資料不符";
	$Lang['eInventory']['NoSuchItem'] = "沒有此項物品";
	$Lang['eInventory']['Variation'] = "其他差異資料";
	$Lang['eInventory']['StocktakeFollowupRemark'] = "盤點統籌人員跟進/備註";
	$Lang['eInventory']['StocktakeDateTime'] = "盤點日期及時間";
	$Lang['eInventory']['ReportPrincipalSignature'] = "校長簽署批核日期";
	$Lang['eInventory']['TotalCost'] = "總計";
	$Lang['eInventory']['PurchasedQty'] = "購買數量";
	$Lang['eInventory']['SotcktakePICSignatureForGeneral'] = "盤點人簽署";
	$Lang['eInventory']['SotcktakeApproverSignatureForGeneral'] = "核對人(盤點統籌人員)簽署";
	$Lang['eInventory']['PrincipalSignatureForGeneral'] = "校長簽署";
	$Lang['eInventory']['StocktakeListRemark'] = "若發現現存物品沒有在盤點表上列出，<br/>請於物品上暫時貼上有顏色的記示貼，<br/>並於本頁/另表填寫物品有關資料。";
	$Lang['eInventory']['TotalAssets'] = "總資產";
	$Lang['eInventory']['TotalFunds'] = "總資金";
	$Lang['eInventory']['ShowHideWriteoffRecord'] = "顯示/隱藏註銷紀錄";
	$Lang['eInventory']['DisplayEarliestPurchaseDateOnly'] = "只顯示最早購買日期";
	$Lang['eInventory']['MoveToOriginalLocationByBatch'] = "批量搬回原來位置";
	$Lang['eInventory']['TotalWriteOffPrice'] = "報銷金額";
	# eInventory ImportWriteoff [Start]
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][1]['FieldTitle'] = "第一欄 : 物品編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][1]['AjaxLinkTitle'] = "按此查詢物品編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][1]['AjaxLink'] = "ItemCode";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][2]['FieldTitle'] = "第二欄 : 物品名稱 (中文)";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][3]['FieldTitle'] = "第三欄 : 物品名稱 (英文)";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][4]['FieldTitle'] = "第四欄 : 報銷原因";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][4]['AjaxLinkTitle'] = "按此查詢報銷原因";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][4]['AjaxLink'] = "WriteOffReason";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][5]['FieldTitle'] = "第五欄 : 申請日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][6]['FieldTitle'] = "第六欄 : 批核日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][7]['FieldTitle'] = "第七欄 : 備註";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][1]['FieldTitle'] = "第一欄 : 物品編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][1]['AjaxLinkTitle'] = "按此查詢物品編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][1]['AjaxLink'] = "ItemCode";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][2]['FieldTitle'] = "第二欄 : 物品名稱 (中文)";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][3]['FieldTitle'] = "第三欄 : 物品名稱 (英文)";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][4]['FieldTitle'] = "第四欄 : 位置編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][4]['AjaxLinkTitle'] = "按此查詢位置編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][4]['AjaxLink'] = "LocationCode";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][5]['FieldTitle'] = "第五欄 : 子位置編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][5]['AjaxLinkTitle'] = "按此查詢子位置編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][5]['AjaxLink'] = "SubLocationCode";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][6]['FieldTitle'] = "第六欄 : 資金來源編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][6]['AjaxLinkTitle'] = "按此查詢資金來源編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][6]['AjaxLink'] = "FundingCode";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][7]['FieldTitle'] = "第七欄 : 管理組別編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][7]['AjaxLinkTitle'] = "按此查詢管理組別編號";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][7]['AjaxLink'] = "GroupCode";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][8]['FieldTitle'] = "第八欄 : 數量";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][9]['FieldTitle'] = "第九欄 : 報銷原因";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][9]['AjaxLinkTitle'] = "按此查詢報銷原因";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][9]['AjaxLink'] = "WriteOffReason";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][10]['FieldTitle'] = "第十欄 : 申請日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][11]['FieldTitle'] = "第十一欄 : 批核日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)";
	$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][12]['FieldTitle'] = "第十二欄 : 備註";
	$Lang['eInventory']['ImportWriteoffError'][1] = "物品不存在";
	$Lang['eInventory']['ImportWriteoffError'][2] = "沒有填上報銷原因";
	$Lang['eInventory']['ImportWriteoffError'][3] = "沒有填上申請日期或格式錯誤";
	$Lang['eInventory']['ImportWriteoffError'][4] = "沒有填上批核日期或格式錯誤";
	$Lang['eInventory']['ImportWriteoffError'][5] = "物品已報銷或正等待批核";
	$Lang['eInventory']['ImportWriteoffError'][6] = "匯入的檔案有重複物品";
	$Lang['eInventory']['ImportWriteoffError'][7] = "報銷原因不存在";
	$Lang['eInventory']['ImportWriteoffError'][8] = "報銷數量多於庫存量";
	$Lang['eInventory']['ImportWriteoffError'][9] = "庫存量不足";
	$Lang['eInventory']['ImportWriteoffError'][10] = "沒有填上報銷數量或填上了非數字";
	$Lang['eInventory']['ImportWriteoffError'][11] = "沒有填上資金來源編號或錯誤";
	$Lang['eInventory']['ImportWriteoffError'][12] = "沒有填上管理組別編號或錯誤";
	$Lang['eInventory']['ImportWriteoffError'][13] = "沒有填上子位置編號或錯誤";
	$Lang['eInventory']['ImportWriteoffError'][14] = "沒有填上位置編號或錯誤";
	$Lang['eInventory']['ImportWriteoffItem'] = "匯入報銷物品";
	$Lang['eInventory']['ImportStep'][2] = "資料驗證";
	$Lang['eInventory']['ImportWriteoffItemFail'] = "匯入報銷物品失敗!";
	# eInventory ImportWriteoff [End]
	$Lang['eInventory']['ReturnMessage']['MissingBasicSettings'] = '0|=|請先設定基本項目,例如位置,物品管理組別,類別,資金來源等';
	$Lang['eInventory']['ItemAttachment'] = '物品附件';
	$Lang['eInventory']['ItemName'] = "項目名稱";
	# eInventory System [Start]
	$i_InventorySystem_Item_FundingSource2Code = "資金來源編號(2)";
	$Lang['eInventory']['System']['ItemCode']['format']['N/A'] = "不適用";
	$Lang['eInventory']['System']['NumberDigits'] = "物品編號序列號數字數目";
	$Lang['eInventory']['System']['Separator'] = "物品編號分格符號";
	$Lang['eInventory']['System']['Separtor']['NotUsing'] = "不使用";
}

if($sys_custom['Invoice2Inventory'] && $plugin['Inventory'])
{
	$Lang['Header']['Menu']['InvoiceMgmtSystem'] = "帳單管理系統";
	$Lang['SysMgr']['RoleManagement']['InvoiceMgmtSystem'] = "帳單管理系統管理";
	$Lang['Invoice']['Invoice'] = "帳單";
	$Lang['Invoice']['Reports'] = "報告";
	$Lang['Invoice']['BudgetReport'] = "預算報告";
	$Lang['Invoice']['YearReport'] = "年度報告";
	$Lang['Invoice']['GroupReport'] = "小組報告";
	$Lang['Invoice']['DeletionLog'] = "資料刪除紀錄";
	$Lang['Invoice']['DeletedDate'] = "刪除日期";
	$Lang['Invoice']['DeletedBy'] = "刪除者";
	$Lang['Invoice']['RecordType'] = "紀錄類別";
	$Lang['Invoice']['RecordInfo'] = "紀錄資料";

	$Lang['Invoice']['ResourceMgmtGroup'] = "物品管理小組";
	$Lang['Invoice']['GroupBudget'] = "小組預算";
	$Lang['Invoice']['Category'] = "帳單細項類別";
	$Lang['Invoice']['TotalBudget'] = "總預算";
	$Lang['Invoice']['Budget'] = "預算";
	$Lang['Invoice']['Balance'] = "結算";
	$Lang['Invoice']['AccumulatedExpense'] = "累計支出";
	$Lang['Invoice']['MgmtGroup'] = $i_InventorySystem['Caretaker'];
	$Lang['Invoice']['NoItemCategory'] = "沒有任何物品類別";
	$Lang['Invoice']['InvoiceDate'] = "帳單日期";
	$Lang['Invoice']['InvoiceCompany'] = "公司";
	$Lang['Invoice']['InvoiceNo'] = "帳單編號";
	$Lang['Invoice']['InvoiceNo_Remark'] = "(如留空, 系統會自動產生 YYYYNNNNN 形式的帳單編號)";
	$Lang['Invoice']['Description'] = "摘要/用途";
	$Lang['Invoice']['DiscountAmount'] = "折扣金額";
	$Lang['Invoice']['TotalAmount'] = "總額";
	$Lang['Invoice']['AccountDate'] = "交會計日期";
	$Lang['Invoice']['PIC'] = "經手人";
	$Lang['Invoice']['AllPic'] = "所有經手人";
	$Lang['Invoice']['FundingSource'] = "資金來源";
	$Lang['Invoice']['CategoryName'] = "細項類別名稱";
	$Lang['Invoice']['CategoryTitle'] = "類別名稱";
	$Lang['Invoice']['Chi'] = "中文";
	$Lang['Invoice']['Eng'] = "英文";
	$Lang['Invoice']['CreateInvoiceOnly'] = "只新增帳單資料";
	$Lang['Invoice']['CreateInvoiceWithItems'] = "新增帳單資料及細項";
	$Lang['Invoice']['CategoryList'] = "細項類別清單";
	$Lang['Invoice']['NewCategory'] = "新增細項類別";
	$Lang['Invoice']['EditCategory'] = "更新細項類別";
	$Lang['Invoice']['IsAsset'] = "是否屬資產項目?";
	$Lang['Invoice']['IsAssetRemark'] = "(需要於下一步輸入資產管理行政系統的資料)";
	$Lang['Invoice']['ItemPrice'] = "物品價格";
	$Lang['Invoice']['InvalidItemPrice'] = "物品價格無效";
	$Lang['Invoice']['NewItemNextBtn1'] = "下一步 (輸入資產管理行政系統資料)";
	$Lang['Invoice']['Error_Duplicate_CategoryName'] = "0|=|細項類別名稱重複";
	$Lang['Invoice']['NewItemNextBtn2'] = "儲存及輸入其他物品";
	$Lang['Invoice']['NewItemNextBtn3'] = "完成";
	$Lang['Invoice']['Balance'] = "結存";
	$Lang['Invoice']['NoInvoiceItem'] = "暫時沒有帳單細項";
	$Lang['Invoice']['ItemName'] = "項目名稱";
	$Lang['Invoice']['Quantity'] = "數量";
	$Lang['Invoice']['TotalPrice'] = "總額";
	$Lang['Invoice']['IsAsset'] = "資產";
	$Lang['Invoice']['SubTotalAmount'] = "金額";
	$Lang['Invoice']['SubTotalAmount2'] = "細項金額";
	$Lang['Invoice']['ViewerGroup'] = "檢視小組";
	$Lang['Invoice']['Group'] = "小組";
	$Lang['Invoice']['Groups'] = "小組";

	$Lang['Invoice']['EditItem'] = "編輯物品";
	$Lang['Invoice']['InvoiceBudgetSummary'] = "帳單預算總覽";
	$Lang['Invoice']['EnterItemInfo'] = "輸入物品資料";
	$Lang['Invoice']['EnterInventoryItemInfo1'] = "輸入資產管理行政系統基本物品資料 <br>(只適用於資產物品)";
	$Lang['Invoice']['EnterInventoryItemInfo2'] = "輸入資產管理行政系統詳細物品資料 <br>(只適用於資產物品)";

	$Lang['Invoice']['InvoiceDateError1'] = "帳單日期必須是今天或以前";
	$Lang['Invoice']['PrintPaymentVoucher'] = "列印支出憑單";

	$Lang['Invoice']['JS_warning']['PIC_1only'] = "不能多於1位經手人。";

	$Lang['Invoice']['ExportInvoice']['EN'] = array("Year Name", "Invoice Date", "Company", "Invoice No.", "Description of use", "Discount Amount", "Total Amount", "Funding Source", "Account Date", "PIC", "Remarks");
	$Lang['Invoice']['ExportInvoice']['B5'] = array("學年", "帳單日期", "公司", "帳單編號", "摘要/用途", "折扣金額", "總額", "資金來源", "交會計日期", "經手人", "備註");

	$Lang['Invoice']['ExportIItem']['EN'] = array("Item Name", "Sub-Total Amount", "Quantity", "Resource Management Group", "Invoice Category", "Is Asset");
	$Lang['Invoice']['ExportIItem']['B5'] = array("物品名稱", "物品價格", "數量", "物品管理小組", "帳單細項類別", "資產");

}
$Lang['eInventory']['ExportInvoice'] = "匯出發票資料";

## iMail Words
$Lang['iMail']['FieldTitle']['Choose'] = "選擇";
$Lang['iMail']['FieldTitle']['ByIdentity'] = "身份";
$Lang['iMail']['FieldTitle']['ByGroup'] = "小組";
$Lang['iMail']['FieldTitle']['Teacher'] = "教師";
$Lang['iMail']['FieldTitle']['NonTeachingStaff'] = "非教學職務員工";
$Lang['iMail']['FieldTitle']['Student'] = "學生";
$Lang['iMail']['FieldTitle']['Parent'] = "家長";
$Lang['iMail']['FieldTitle']['Alumni'] = "校友";

$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff'] = "所有教師";
$Lang['iMail']['FieldTitle']['ToFormTeachingStaff'] = "同一班級的教師";
$Lang['iMail']['FieldTitle']['ToClassTeachingStaff'] = "同一班別的教師";
$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff'] = "同一學科的教師";
$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff'] = "同一科組的教師";

$Lang['iMail']['FieldTitle']['ToIndividualsStudent'] = "所有學生";
$Lang['iMail']['FieldTitle']['ToFormStudent'] = "同一班級的學生";
$Lang['iMail']['FieldTitle']['ToClassStudent'] = "同一班別的學生";
$Lang['iMail']['FieldTitle']['ToSubjectStudent'] = "同一學科的學生";
$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent'] = "同一科組的學生";

$Lang['iMail']['FieldTitle']['ToIndividualsParents'] = "所有家長";
$Lang['iMail']['FieldTitle']['ToFormParents'] = "同一班級的家長";
$Lang['iMail']['FieldTitle']['ToClassParents'] = "同一班別的家長";
$Lang['iMail']['FieldTitle']['ToSubjectParents'] = "同一學科的家長";
$Lang['iMail']['FieldTitle']['ToSubjectGroupParents'] = "同一學科組別的家長";
$Lang['iMail']['FieldTitle']['TargetParent'] = "的家長";
$Lang['iMail']['FieldTitle']['Group'] = "小組";
$Lang['iMail']['FieldTitle']['MyChildren'] = "我的子女";
$Lang['iMail']['FieldTitle']['MyParent'] = "我的父母";
$Lang['iMail']['FieldTitle']['Category'] = "類別";
$Lang['iMail']['FieldTitle']['SubCategory'] = "子類別";
$Lang['iMail']['FieldTitle']['NonDelivery'] = "以下用戶未能接收此訊息，因為他們的郵箱已滿。但你仍可以將沒有附件之訊息發送給他們。";
$Lang['iMail']['FieldTitle']['EmailAddressSeparationNote'] = "請以 ; 或 , 分隔每個電郵地址";
$Lang['iMail']['ErrorMsg']['MailCannotSendOutSuccessfully'] = "郵件未能成功發送";
$Lang['iMail']['FieldTitle']['ChooseFromExternalRecipient'] = "選擇外在收件人";
$Lang['iMail']['FieldTitle']['ChooseFromInternalRecipient'] = "選擇內聯網收件人";
$Lang['iMail']['FieldTitle']['PersonLeaveSchool'] = "已離校職員 / 學生";
$Lang['iMail']['FieldTitle']['TeacherAndStaff'] = "教師 / 非教學職務員工";
$Lang['iMail']['JSWarning']['ExternalRecipientNoTriangleBrackets1'] = "收件人名稱不能含有( < )";
$Lang['iMail']['JSWarning']['ExternalRecipientNoTriangleBrackets2'] = "收件人名稱不能含有( > )";
$Lang['iMail']['JSWarning']['ExternalRecipientNoComma'] = "收件人名稱不能含有( , )";
$Lang['iMail']['FieldTitle']['Remark']['CommaCannotSeprateEmailInAutoComplete'] = "(在自動完成中\",\"不能用作分隔每個電郵地址)";
$Lang['iMail']['FieldTitle']['Reminder']['CannotReadMailContent_Title'] = "無法讀取郵件內容？";
$Lang['iMail']['FieldTitle']['Reminder']['CannotReadMailContent_Body'] = "由Google Mail發出的郵件如並非以UTF-8編碼，將無法顯示。請參閱<a class='tablelink' target='_blank' href='http://mail.google.com/support/bin/answer.py?hl=en-GB&ctx=mail&answer=22841'>此文件</a>，瞭解設定方法。";

$Lang['iMail']['FieldTitle']['Year'] = "年份";
$Lang['iMail']['FieldTitle']['NumberOfEmails'] = "郵件數量";
$Lang['iMail']['FieldTitle']['Archive'] = "封存";
$Lang['iMail']['FieldTitle']['ArchiveInboxMsg'] = "你的收件夾已經有超過<!--NUMBER-->封電郵，並且將影響系統效能。你是否想將今年之前收到的郵件封存？封存後，你可前往 <b>檔案管理 > 年份</b> 存取這些電郵。";
$Lang['iMail']['JSWarning']['RequestAtLeastOneYearToArchive'] = "請選擇至少一年來進行封存。";

$Lang['iMail']['RemindMsg']['MaxAttachmentSize'] = "最大附件大小為<!--SIZE-->MB";
$Lang['iMail']['FieldTitle']['SubjectKeywordKeyPhrase'] = "主旨關鍵字/關鍵句";
$Lang['iMail']['RemindMsg']['OnlyForwardInternetMails'] = "只可以轉發互聯網郵件，內聯網郵件則不可。";
$Lang['Email']['SystemAdmin'] = "系統管理員";
$Lang['ResourceBooking']['ImportNote1'] = "<ul><li>時間表名稱必須完全正確, 否則資料不能匯入
<li><a href=javascript:newWindow('tscheme.php',0)>檢視時間表內容</a>
<li><a href='/templates/get_sample_csv.php?file=itemsample_unicode.csv'> 下載範本檔案 (CSV 格式)</a></ul>";

###  eCircular
$Lang['Circular']['Settings'] = "設定";
$Lang['Circular']['BasicSettings'] = "基本設定";
$Lang['Circular']['AdminSettings'] = "管理權限設定";
$Lang['Circular']['DisableCircular'] = "不使用". $Lang['Header']['Menu']['eCircular'];
$Lang['Circular']['AllowHelpSigning'] = "容許管理人員替收件人更改回條";
$Lang['Circular']['AllowLateSign'] = "容許遲交";
$Lang['Circular']['AllowResign'] = "容許收件者於限期前更改已簽回的回條";
$Lang['Circular']['DefaultDays'] = "預設簽署期限日數";
$Lang['Circular']['TeacherCanViewAll'] = "讓所有老師檢視所有通告";
$Lang['Circular']['AdminLevel_Normal'] = "一般權限";
$Lang['Circular']['AdminLevel_Normal_Detail'] = $Lang['Circular']['AdminLevel_Normal']." (可發出職員通告及觀看自己所發通告的結果)";
$Lang['Circular']['AdminLevel_Full'] = "完全控制";
$Lang['Circular']['AdminLevel_Full_Detail'] = $Lang['Circular']['AdminLevel_Full'] ." (可發出職員通告及觀看所有通告的結果)";
$Lang['Circular']['StaffName'] = "職員/老師姓名";
$Lang['Circular']['AdminLevel'] = "管理權限";
$Lang['Circular']['PleaseSelectStaff'] = "請選擇職員/老師";
$Lang['Circular']['SearchStaffName'] = "輸入職員/老師姓名";
$Lang['Circular']['FromGroup'] = "自 <b>小組</b>";
$Lang['Circular']['SettingsUpdateSuccess'] = "<font color=green>設定成功</font>";

$Lang['eCircular']['ReplySlipForm'] = "回條";
$Lang['eCircular']['ReplySlip'] = "回條";
$Lang['eCircular']['Preview'] = "預覽";
$Lang['eCircular']['SetReplySlipContent'] = "設定回條內容";
$Lang['eCircular']['ReplySlipQuestions'] = "回條問題";
$Lang['eCircular']['ViewCircular'] = "檢視相關通告";
$Lang['eCircular']['NotifyStaffByEmail'] = "以電郵通知教職員";
$Lang['eCircular']['NotifyStaffByPushMessage'] = "以Teacher App推送訊息通知教職員";
$Lang['eCircular']['DisplayQuestionNumber'] = "顯示問題編號";
$Lang['eCircular']['NotifyEmailRemark'] = '備註：如勾選此功能，按  "呈送" 後會即時發出電郵。';
$Lang['Circular']['MaxNumberReplySlipOption'] = "回條選項數目上限";
$Lang['Circular']['UpdateStatusWarning'] = "若修改已發佈的職員通告內容,所有已填寫的回條內容,將會消失。";
$Lang['Circular']['UnsignedCircularList'] = "未簽通告名單";
$Lang['Circular']['AllUnsignedStaff'] = "所有未簽通告教職員";
$Lang['Circular']['AllIssuerStaff'] = "所有發出人";
$Lang['Circular']['UnsignedCircularStaffName'] = "未簽通告教職員";
$Lang['Circular']['UnsignedCircularTitle'] = "未簽通告標題";
$Lang['Circular']['NoticeDeleted'] = '通告已被刪除，故未能檢視此通告。';
### ePolling
$Lang['Polling']['ToBeReleasedOn'] = '可檢閱日期 :';
$Lang['Polling']['InvalidDueDate'] ="限期必須是今天或以後";
$Lang['Polling']['InvalidReleaseDate'] ="結果公佈日期必須是開始日期或以後";
$Lang['Polling']['ViewPoll'] ="檢視投票";
$Lang['Polling']['Choice'] ="選擇 ";

$Lang['Polling']['Anonymous'] ="不記名 ";
$Lang['Polling']['Anonymous_Description'] ="不能瀏覽投票名單";
$Lang['Polling']['WholeSchool'] ="全校";
$Lang['Polling']['WarnSelectGroup'] = "請選擇組別";
$Lang['Polling']['EditRemarks'] = "如刪除對象組別，該組別已填寫的問卷內容會一併被刪除。";

$Lang['Polling']['Target'] = "對象";
$Lang['Polling']['Answer'] = "答案";

/* ------------------- Start of ePayment ------------------- */
if ($plugin['payment']) {
	$Lang['ePayment']['ManualCashInputSuccess'] = '1|=|手動輸入成功';
	$Lang['ePayment']['ManualCashInputUnsuccess'] = '0|=|手動輸入失敗';
	$Lang['ePayment']['DateType'] = '時間類別';
	$Lang['ePayment']['CancelDepositSuccess'] = '1|=|取消紀錄成功';
	$Lang['ePayment']['CancelDepositUnsuccess'] = '0|=|取消紀錄失敗';
	$Lang['ePayment']['PaymentItemCreateSuccess'] = '1|=|新增繳費項目成功';
	$Lang['ePayment']['PaymentItemCreateUnsuccess'] = '0|=|新增繳費項目失敗';
	$Lang['ePayment']['DataImportSuccess'] = '1|=|資料匯入成功';
	$Lang['ePayment']['DataImportUnsuccess'] = '0|=|資料匯入失敗';
	$Lang['ePayment']['PaymentStudentItemAddedSuccess'] = '1|=|同戶新增成功';
	$Lang['ePayment']['PaymentStudentItemAddedUnsuccess'] = '0|=|用戶新增失敗';
	$Lang['ePayment']['PaymentStudentItemUpdatedSuccess'] = '1|=|用戶更新成功';
	$Lang['ePayment']['PaymentStudentItemUpdatedUnsuccess'] = '0|=|用戶更新失敗';
	$Lang['ePayment']['PaymentItemDeletedSuccess'] = '1|=|繳費項目移除成功';
	$Lang['ePayment']['PaymentItemDeletedUnsuccess'] = '0|=|繳費項目移除失敗';
	$Lang['ePayment']['PaymentItemEditSuccess'] = '1|=|繳費項目更新成功';
	$Lang['ePayment']['PaymentItemEditUnsuccess'] = '0|=|繳費項目更新失敗';
	$Lang['ePayment']['PayProcessSuccess'] = '1|=|繳費成功';
	$Lang['ePayment']['PayProcessUnsuccess'] = '0|=|繳費失敗';
	$Lang['ePayment']['PayUndoSuccess'] = '1|=|還原成功';
	$Lang['ePayment']['PayUndoUnsuccess'] = '0|=|還原失敗';
	$Lang['ePayment']['ArchiveSuccess'] = "1|=|紀錄整存成功";
	$Lang['ePayment']['ArchiveUnsuccess'] = "0|=|紀錄整存失敗";
	$Lang['ePayment']['ArchiveRestoreSuccess'] = "1|=|紀錄回復成功";
	$Lang['ePayment']['ArchiveRestoreUnsuccess'] = "0|=|紀錄回復失敗";
	$Lang['ePayment']['CreateSubsidySourceSuccess'] = "1|=|資助來源新增成功";
	$Lang['ePayment']['CreateSubsidySourceUnsuccess'] = "0|=|資助來源新增失敗";
	$Lang['ePayment']['UpdateSubsidySourceSuccess'] = "1|=|資助來源更新成功";
	$Lang['ePayment']['UpdateSubsidySourceUnsuccess'] = "0|=|資助來源更新失敗";
	$Lang['ePayment']['RefundSuccess'] = "1|=|退款成功";
	$Lang['ePayment']['RefundFail'] = "0|=|退款失敗";
	$Lang['ePayment']['DonateSuccess'] = "1|=|捐款成功";
	$Lang['ePayment']['DonateFail'] = "0|=|捐款失敗";
	$Lang['ePayment']['PPSRemapSuccess'] = "1|=|繳費靈增值成功";
	$Lang['ePayment']['PPSRemapFail'] = "0|=|繳費靈增值失敗";
	$Lang['ePayment']['UnknownRecordSuccess'] = "1|=|不明繳費靈紀錄移除成功";
	$Lang['ePayment']['UnknownRecordFail'] = "0|=|不明繳費靈紀錄移除失敗";
	$Lang['ePayment']['PhotocopyPackageCreateSuccess'] = "1|=|影印套票新增成功";
	$Lang['ePayment']['PhotocopyPackageCreateFail'] = "0|=|影印套票新增失敗";
	$Lang['ePayment']['PhotocopyPackageEditSuccess'] = "1|=|影印套票更新成功";
	$Lang['ePayment']['PhotocopyPackageEditFail'] = "0|=|影印套票更新失敗";
	$Lang['ePayment']['PhotocopyPackageDeleteSuccess'] = "1|=|影印套票移除成功";
	$Lang['ePayment']['PhotocopyPackageDeleteFail'] = "0|=|影印套票移除失敗";
	$Lang['ePayment']['PrintQuotaEditSuccess'] = "1|=|配額更新成功";
	$Lang['ePayment']['PrintQuotaEditFail'] = "0|=|配額更新失敗";
	$Lang['ePayment']['PrintQuotaNewSuccess'] = "1|=|配額新增成功";
	$Lang['ePayment']['PrintQuotaNewFail'] = "0|=|配額新增失敗";
	$Lang['ePayment']['PrintQuotaResetSuccess'] = "1|=|配額重設成功";
	$Lang['ePayment']['PrintQuotaResetFail'] = "0|=|配額重設失敗";
	$Lang['ePayment']['PayAll'] = "全部繳款";
	$Lang['ePayment']['PPSChargeWarning'] = "繳費靈手續費($";
	$Lang['ePayment']['PPSChargeWarning1'] = ", Counter Bill 則為 $";
	$Lang['ePayment']['PPSChargeWarning2'] = ")將自動從所有交易項目中扣除。";
	$Lang['ePayment']['CashDepositImportSuccess'] = "1|=|匯入現金存入紀錄成功";
	$Lang['ePayment']['PaymentItemStudentDeleteSuccess'] = "1|=|用戶移除成功";
	$Lang['ePayment']['PaymentItemStudentDeleteFail'] = "0|=|用戶移除失敗";
	$Lang['ePayment']['IncludeNotStarted'] = "包括所有未開始的繳費項目";
	$Lang['ePayment']['IEPS'] = "PPS - IEPS";
	$Lang['ePayment']['CounterBill'] = "PPS - CounterBill";

	$Lang['ePayment']['POSTransactionReport'] = 'POS 交易紀錄';
	$Lang['ePayment']['GrandTotal'] = '總數';
	$Lang['ePayment']['InvoiceNumber'] = '發單編號';
	$Lang['ePayment']['NoInvoiceNumber'] = '沒有發票編號';
	$Lang['ePayment']['ItemName'] = '項目名稱';
	$Lang['ePayment']['Quantity'] = '數量';
	$Lang['ePayment']['UnitPrice'] = '單價';
	$Lang['ePayment']['RefundReport'] = "項目退款報告";
	$Lang['ePayment']['SelectFormat'] = "選擇格式";
	$Lang['ePayment']['Format_1'] = "格式 1";
	$Lang['ePayment']['Format_2'] = "格式 2";
	$Lang['Payment']['SignatureText'] = '簽名顯示';
	$Lang['Payment']['StartingNextReceiptNumber'] = '起始收據號碼';
	$Lang['Payment']['ReceiptNumber'] = '收據號碼';
	$Lang['Payment']['StartingNextReceiptNumberWarning'] = '起始收據號碼必須大於或等於 ';
	$Lang['Payment']['StartingNextReceiptNumberNumberWarning'] = '請輸入收據號碼為數字';
	$Lang['ePayment']['POSItemReport'] = "POS 項目統計";
	$Lang['ePayment']['POSStudentReport'] = "POS 學生統計";
	$Lang['ePayment']['PossibleDuplicatePPSWarning'] = "在系統中找到繳費靈號碼/交易時間/總數相同的紀錄. 此紀錄可能是重複輸入.";
	$Lang['ePayment']['PossibleDuplicatePPSJSWarning'] = "在系統中找到繳費靈號碼/交易時間/總數相同的紀錄. 某此紀錄可能是重複輸入.\\n 確定繼續要輸入?";
	$Lang['ePayment']['ExcludeParentSignature'] = "不顯示家長簽署<br /><font color='red'>(只適用於繳費通知書)</font>";
	$Lang['ePayment']['ViewNotice'] = "檢視相關通告";
	$Lang['ePayment']['PaymentItemGenerateFromNotice'] = "產生自".$Lang['Header']['Menu']['eNotice']."的繳費項目";
	$Lang['ePayment']['SystemProperties'] = "系統屬性";
	$Lang['ePayment']['SettineValue'] = "設定值";
	$Lang['ePayment']['PaymentNoticeFeature'] = "繳費通告功能";
	$Lang['ePayment']['SettingApplySuccess'] = "1|=|設定更新成功";
	$Lang['ePayment']['AllowStudentChooseReceipt'] = "允許學生選擇是否需要收據";
	$Lang['ePayment']['ReceiptSettings'] = "收據設定";
	$Lang['ePayment']['NeedReceipt'] = "是否需要收據";
	$Lang['ePayment']['StudentPaymentItemSettings'] = "學生繳費項目";
	$Lang['ePayment']['NeedToPrintReceipt'] = "需要打印收據嗎？";
	$Lang['ePayment']['OfficialReceipt'] = "正式收據";
	$Lang['ePayment']['AddValueAndPayTotalError'] = "入賬及應繳金額錯誤";
	//$Lang['ePayment']['Select'] = " -- 選擇 --";

	# wording of options in editing Payment Notice
	$Lang['ePayment']['Options'] = "選項";
	$Lang['ePayment']['NoOfOptions'] = "選項數量";
	$Lang['ePayment']['WhetherToPay'] = "選擇是否繳費";
	$Lang['ePayment']['OnePaymentFromOneCategory'] = "繳付單一款項(單一繳費類別)";
	$Lang['ePayment']['OnePaymentFromFewCategories'] = "繳付單一款項(多個繳費類別)";
	$Lang['ePayment']['SeveralPaymentFromFewCategories'] = "繳付多筆款項(多個繳費類別)";
	$Lang['ePayment']['SeveralPaymentFromFewCategoriesWithInputQuantity'] = "繳付多筆款項(可輸入數量)(多個繳費類別)";
	$Lang['ePayment']['MustPayItem'] = "必須繳費";
	$Lang['ePayment']['DisplayOrderMissingWarning'] = "請輸入排列次序";

	$Lang['ePayment']['SendNotice'] = "傳送電子通告";
	$Lang['ePayment']['NotEnoughBalance'] = "戶口結存不足";

	$Lang['ePayment']['Notification'] = "提示";
	$Lang['ePayment']['SMS'] = "短訊";
	$Lang['ePayment']['Email'] = "電子郵件";
	$Lang['ePayment']['EmailContent'] = "電子郵件內容";
	$Lang['ePayment']['EmailSubject'] = "電子郵件主旨";
	$Lang['ePayment']['EmailAttachment'] = "電子郵件附件";
	$Lang['ePayment']['By'] = "使用";
	$Lang['ePayment']['CcTo'] = "CC給";
	$Lang['ePayment']['ClassTeacher'] = "班主任";
	$Lang['ePayment']['EmailSentToRecipient'] = "將會傳送電郵至以下接收人";
	$Lang['ePayment']['EmailCcToAdmin'] = "將會傳送電郵副本至以下".$Lang['SysMgr']['RoleManagement']['ePayment'];
	$Lang['ePayment']['EmailCcToClassTeacher'] = "將會傳送電郵副本至以下班主任";
	$Lang['ePayment']['WarningMsg']['RequestInputEmailSubject'] = "請輸入電子郵件主旨。";
	$Lang['ePayment']['ReturnMsg']['EmailSentSuccess'] = "1|=|成功發送郵件。";
	$Lang['ePayment']['ReturnMsg']['EmailSentFail'] = "0|=|發送郵件失敗。";

	$Lang['ePayment']['AutoDetectFileType'] = "系統能自動判別匯入檔案類型(IEPS或Counter Bill)。";
	$Lang['ePayment']['PostTime'] = "進誌時間";
	$Lang['ePayment']['UnpaidItem'] = "未繳費項目";
	$Lang['ePayment']['Amount'] = "金額";

	$Lang['ePayment']['ProceedToDelete'] = "進行刪除";
	$Lang['ePayment']['PaymentItemStudentsToDelete'] = "以下的用戶將會從此繳費項目中刪除";
	$Lang['ePayment']['PaymentItemStudentsNotToDelete'] = "以下的用戶將不會從此繳費項目中刪除";

	$Lang['ePayment']['BalanceReport'] = "結存報告";
	$Lang['ePayment']['Identity'] = "身份";
	$Lang['ePayment']['YearForm'] = "年級";
	$Lang['ePayment']['Staff'] = "教職員";
	$Lang['ePayment']['Student'] = "學生";
	$Lang['ePayment']['Class'] = "班別";
	$Lang['ePayment']['IdentityOrYearForm'] = "身份或者年級";
	$Lang['ePayment']['WarningMsg']['PleaseSelectIdentityOrYearForm'] = "請選擇身份或者年級。";
	$Lang['ePayment']['WarningMsg']['PleaseInputValidDateRange'] = "請輸入正確的日期範圍。";
	$Lang['ePayment']['OpenningBalance'] = "期初結餘";
	$Lang['ePayment']['ClosingBalance'] = "期末結餘";

	$Lang['ePayment']['PayAllItems'] = "繳付全部繳費項目";
	$Lang['ePayment']['NoUnpaidItems'] = "無未繳付的繳費項目";
	$Lang['ePayment']['CurrentAccountBalance'] = "現時戶口結餘";
	$Lang['ePayment']['PPSCharge'] = "繳費靈手續費";
	$Lang['ePayment']['PPSAddValue'] = "繳費靈增值";
	$Lang['ePayment']['PPSIEPS'] = "IEPS";
	$Lang['ePayment']['PPSCounterBill'] = "Counter Bill";
	$Lang['ePayment']['TotalPaidAmount'] = "總支付";
	$Lang['ePayment']['AddValue'] = "增值";
	$Lang['ePayment']['TotalAddValue'] = "總增值";

	$Lang['ePayment']['DefaultSearch'] = "預設搜尋 (適當的搜尋結果會被顯示)";
	$Lang['ePayment']['CsvDataSet'] = "CSV (數據集)";
	$Lang['ePayment']['ResetType'] = "重設類型";
	$Lang['ePayment']['ResetQuota'] = "重設配額";
	$Lang['ePayment']['DailyPaymentReport'] = "每日繳費報告";
	$Lang['ePayment']['ServiceProductSales'] = "服務/產品銷售排行表";
	$Lang['ePayment']['SalesDetail'] = "收費明細表";
	$Lang['ePayment']['School'] = "學校";
	$Lang['ePayment']['PaymentItem'] = "繳費項目";
	$Lang['ePayment']['SelectMode'] = "選擇模式";

	$Lang['ePayment']['WarningMsg']['SourceOfSubsidyDuplicated'] = "*請不要重複選取資助來源。";
	$Lang['ePayment']['WarningMsg']['SourceOfSubsidyNotEnoughMoney'] = "*資助來源沒有足夠的資助額。";
	$Lang['ePayment']['WarningMsg']['PleaseRemoveThisSourceOfSubsidy'] = "*請移除此資助來源。";
	$Lang['ePayment']['ReceiptTitle'] = "收據標題";

	$Lang['ePayment']['eLibPlusOverduePayment'] = $Lang['Header']['Menu']['eLibraryPlus']."逾期繳款";
	$Lang['ePayment']['LibraryPayment'] = "圖書館繳費 / Library Payment";

	#Siuwan 20130902 Override lang.en.ip20.php
	$i_Payment_Import_Format_PPSLink = "<b><u>檔案說明:</u></b><br>
	第一欄 : 班別<br>
	第二欄 : 班號<br>
	第三欄 : <font color='red'>^</font>學生姓名<br>
	第四欄 : PPS 戶口號碼
	";
	$i_Payment_Import_Format_PPSLink2 = "<b><u>檔案說明:</u></b><br>
	第一欄 : $i_UserLogin<br>
	第二欄 : PPS 戶口號碼<br>
	第三欄 : <font color='red'>^</font>用戶姓名<br>
	";
	$i_Payment_Payment_StudentImportFileDescription1 = "班別,班號, <font color='red'>^</font>學生姓名, 需繳付金額, 備註";
	if($sys_custom['ePayment']['PaymentMethod']){
		$i_Payment_Payment_StudentImportFileDescription1 .= ", 付款方法";
	}
	$i_Payment_Payment_StudentImportFileDescription1 .= ", 資助來源代碼<a href=\"javascript:showMenu2('img_3','ToolMenu2');\"><img id='img_3' src=\"$image_path/$LAYOUT_SKIN/inventory/icon_help.gif\" border=\"0\"></a>, 資助金額";
	$i_Payment_Payment_StudentImportFileDescription2 = "內聯網帳號, <font color='red'>^</font>用戶姓名, 需繳付金額, 備註";
	if($sys_custom['ePayment']['PaymentMethod']){
		$i_Payment_Payment_StudentImportFileDescription2 .= ", 付款方法";
	}
	$i_Payment_Payment_StudentImportFileDescription2 .= ", 資助來源代碼<a href=\"javascript:showMenu2('img_4','ToolMenu2');\"><img id='img_4' src=\"$image_path/$LAYOUT_SKIN/inventory/icon_help.gif\" border=\"0\"></a>, 資助金額";
	$Lang['ePayment']['NumberArray'] = array('一','二','三','四','五','六','七','八','九','十');
	$Lang['ePayment']['NumberArrayIndex'] = -1;
	$i_Payment_Import_CashDeposit_Instruction = "
	<span class=extraInfo>只適用於學生</span><Br><Br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 班別<Br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 班號<br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : <font color='red'>^</font>學生姓名<br>";
	if($sys_custom['ePayment']['CreditMethodAutoPay']){
		$i_Payment_Import_CashDeposit_Instruction .= "第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 增值方法 (1表示使用現金，2表示自動轉帳)<br>";
	}else if($sys_custom['ePayment']['CashDepositMethod']){
		$i_Payment_Import_CashDeposit_Instruction .= "第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 增值方法 (1表示使用現金，2表示銀行轉帳，3表示支票存款)<br>";
	}
	$i_Payment_Import_CashDeposit_Instruction .= "第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 存入金額<br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 存入之日期時間 (YYYY-MM-DD HH:mm:ss 或 DD/MM/YYYY hh:mm:ss, 留空會被視為此匯入之時間)<br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : $i_Payment_Field_RefCode (留空會由系統自動產生)
	";
	if($sys_custom['ePayment']['CashDepositAccount']){
		$i_Payment_Import_CashDeposit_Instruction .= "<br>第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 戶口";
		$i_Payment_Import_CashDeposit_Instruction .= "<br>第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 收據編號";
	}
	if($sys_custom['ePayment']['CashDepositRemark']){
		$i_Payment_Import_CashDeposit_Instruction .= "<br>第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 備註";
	}

	$Lang['ePayment']['NumberArrayIndex'] = -1;
	$i_Payment_Import_CashDeposit_Instruction2 = "
	<span class=extraInfo>適用於學生或老師</span><Br><br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : $i_UserLogin<br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : <font color='red'>^</font>學生姓名<br>";
	if($sys_custom['ePayment']['CreditMethodAutoPay']){
		$i_Payment_Import_CashDeposit_Instruction2 .= "第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 增值方法 (1表示使用現金，2表示自動轉帳)<br>";
	}else if($sys_custom['ePayment']['CashDepositMethod']){
		$i_Payment_Import_CashDeposit_Instruction2 .= "第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 增值方法 (1表示使用現金，2表示銀行轉帳，3表示支票存款)<br>";
	}
	$i_Payment_Import_CashDeposit_Instruction2 .= "第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 存入金額<br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 存入之日期時間 (YYYY-MM-DD HH:mm:ss 或 DD/MM/YYYY hh:mm:ss, 留空會被視為此匯入之時間)<br>
	第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : $i_Payment_Field_RefCode (留空會由系統自動產生)
	";
	if($sys_custom['ePayment']['CashDepositAccount']){
		$i_Payment_Import_CashDeposit_Instruction2 .= "<br>第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 戶口";
		$i_Payment_Import_CashDeposit_Instruction2 .= "<br>第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 收據編號";
	}
	if($sys_custom['ePayment']['CashDepositRemark']){
		$i_Payment_Import_CashDeposit_Instruction2 .= "<br>第".$Lang['ePayment']['NumberArray'][++$Lang['ePayment']['NumberArrayIndex']]."欄 : 備註";
	}

	$Lang['ePayment']['StudentsHaveUnsettledPayments'] = "以下學生仍有繳費項目未繳付。<br />請到 [繳費項目設定] 完成繳費程序。";
	$Lang['ePayment']['ConfirmForceStudentToPay'] = "你確定要替該學生進行繳款程序?";
	$Lang['ePayment']['NotEnoughBalanceToPay'] = "該學生沒有足夠的結餘進行繳費。";
	$Lang['ePayment']['SourceOfSubsidyChangeLog'] = "資助來源修改紀錄";
	$Lang['ePayment']['PaymentMethod'] = "付款方法";
	$Lang['ePayment']['PaymentRemark'] = "付款備註";
	$Lang['ePayment']['NA'] = '不適用';
	$Lang['ePayment']['DeleteRecordsInDateRange'] = "刪除所選日期範圍內的所有紀錄";
	$Lang['ePayment']['ConfirmDeleteRecordsInDateRange'] = "確定要刪除所選日期範圍內的所有紀錄?";
	$Lang['ePayment']['RecordInfo'] = "紀錄資料";

	$Lang['ePayment']['Chargeable_Amount'] = "繳交金額";
	$Lang['ePayment']['Total_Chargeable_Amount'] = "應繳總金額";

	$Lang['ePayment']['FinalQuota'] = "最後配額";
	$Lang['ePayment']['QuotaLeft'] = "餘額";

	$Lang['ePayment']['IncludeLibraryOverdueFine'] = "包括 ".$Lang['Header']['Menu']['eLibraryPlus']." 罰款";

	$Lang['ePayment']['eLibPlusOverduePayment'] = "eLibrary <i>plus</i> 綜合圖書館逾期繳款";
	$Lang['ePayment']['LibraryPayment'] = "圖書館繳費 / Library Payment";
	$Lang['ePayment']['LibraryOutstanding'] = "未繳";
	$Lang['ePayment']['LibraryPaymentSettled'] = "已繳交";
	$Lang['ePayment']['LibraryName'] = "讀者";
	$Lang['ePayment']['LibraryBookTitle'] = "書名";
	$Lang['ePayment']['LibraryPenaltyType'] = "罰款類型";
	$Lang['ePayment']['LibraryFine'] = "罰款";
	$Lang['ePayment']['LibraryPaymentReceived'] = "已收罰款";
	$Lang['ePayment']['LibraryFineDate'] = "罰款日期（還書日期）";
	$Lang['ePayment']['LibraryLost'] = "遺失";
	$Lang['ePayment']['LibraryOverdue'] = "已逾期";
	$Lang['ePayment']['LibraryOverdueDay'] = "天";
	$Lang['ePayment']['LibraryOverdueFine'] = "逾期罰款";
	$Lang['ePayment']['LibraryLostBook'] = "遺失罰款";
	$Lang['ePayment']['LibraryWaived'] = "豁免";
	$Lang['ePayment']['LibraryPaymentStatus'] = "繳款狀態";
	$Lang['ePayment']['LibraryPaymentMethod'] = "收費形式";
	$Lang['ePayment']['LibraryByePayment'] = "經ePayment收費管理系統";
	$Lang['ePayment']['LibraryByeLibraryPlus'] = "經eLibrary <i>plus</i> 綜合圖書館";
	$Lang['ePayment']['LibraryOverdueDays'] = "逾期(天)";
	$Lang['ePayment']['LibraryPaid'] = "已繳交";
	$Lang['ePayment']['LibraryPaidOn'] = "繳交時間";
	$Lang['ePayment']['LibraryRequestSelectOutstandingRecords'] = "請選擇至少一個未繳款的紀錄去做繳款。";
	$Lang['ePayment']['LibraryRequestSelectSettledRecords'] = "請選擇至少一個已繳款的紀錄去做還原繳款。";

	$Lang['ePayment']['ApplyToAllRecords'] = "套用至所有紀錄";
	$Lang['ePayment']['PrintReceipt'] = "列印收據";
	$Lang['ePayment']['PleaseSelectAtLeastOnePaidStudent'] = "請至少選擇一位已繳款的用戶。";
	$Lang['ePayment']['PaymentStatus'] = "繳費狀況";

	$Lang['ePayment']['PaymentItemType'] = "繳費項目類型";
	$Lang['ePayment']['AllPaymentItems'] = "所有繳費項目";
	$Lang['ePayment']['OverduePaymentItems'] = "已到期的繳費項目";
	$Lang['ePayment']['NonOverduePaymentItems'] = "未到期的繳費項目";
	$Lang['ePayment']['NonStartedPaymentItems'] = "未開始的繳費項目";
	$Lang['ePayment']['StartedPaymentItems'] = "已開始的繳費項目";
	$Lang['ePayment']['SelectPaymentItems'] = "選擇繳費項目";
	$Lang['ePayment']['PaymentItemChangeLog'] = "繳費項目修改紀錄";
	$Lang['ePayment']['RecordType'] = "紀錄類型";
	$Lang['ePayment']['PaymentItemChangeLogType'] = array();
	$Lang['ePayment']['PaymentItemChangeLogType'][1] = "編輯";
	$Lang['ePayment']['PaymentItemChangeLogType'][2] = "刪除";
	$Lang['ePayment']['PaymentItemChangeLogType'][3] = "存檔";
	$Lang['ePayment']['PaymentItemChangeLogType'][4] = "復原存檔";
	$Lang['ePayment']['PaymentItemChangeLogType'][5] = "全部繳款";
	$Lang['ePayment']['PaymentItemChangeLogType'][6] = "新增學生";
	$Lang['ePayment']['PaymentItemChangeLogType'][7] = "編輯學生";
	$Lang['ePayment']['PaymentItemChangeLogType'][8] = "刪除學生";
	$Lang['ePayment']['PaymentItemChangeLogType'][9] = "繳款";
	$Lang['ePayment']['PaymentItemChangeLogType'][10] = "還原至未繳";

	$Lang['ePayment']['ThermalPaperReceipt'] = "熱感紙收據";
	$Lang['ePayment']['PrintThermalPaperReceipt'] = "列印熱感紙收據";
	$Lang['ePayment']['RemarkReceiptDisclaimer'] = "*繳費後因事退學，所繳費用恕不發還。";
	$Lang['ePayment']['BalanceAfter'] = "付款後結餘";

	$Lang['ePayment']['SortBy'] = "排序以";
	$Lang['ePayment']['Ascendingly'] = "使用遞增次序";
	$Lang['ePayment']['Descendingly'] = "使用遞減次序";
	$Lang['ePayment']['ReceiptPaidAmount'] = "交來 ";
	$Lang['ePayment']['SystemAdmin'] = "系統管理員";
	$Lang['ePayment']['HideCancelledTransactions'] = "不顯示取消的交易";
	$Lang['ePayment']['Invalid'] = "數據不正確";
	$Lang['ePayment']['CopyPaymentItemsSuccess'] = "1|=|成功複製繳費項目。";
	$Lang['ePayment']['CopyPaymentItemsUnsuccess'] = "0|=|複製繳費項目失敗。";
	$Lang['ePayment']['PPSFileImportLog'] = "PPS文件匯入紀錄";
	$Lang['ePayment']['ReturnMsgPPSImportNotAllSuccess'] = "部分紀錄未能成功匯入，請前往[".$i_Payment_Menu_Browse_MissedPPSBill."]查看並進行處理。";
	$Lang['ePayment']['TotalPOSAmount'] = "ePOS 總數";
	$Lang['ePayment']['DisplayCancelledTransactions'] = "顯示取消的交易";
	$Lang['ePayment']['AutoPay'] = "自動轉帳";
	$Lang['ePayment']['TransactionDetailAutoPay'] = "自動轉帳 / Auto-pay";
	$Lang['ePayment']['ShowOutstandingPaymentAlertMessageForParent'] = "於家長用戶顯示待繳款項的提示信息";
	$Lang['ePayment']['users'] = "用戶";
	$i_Payment_Payment_PaidCount = "已繳交的用戶";
	$i_Payment_Payment_UnpaidCount = "未繳交的用戶";
	$i_Payment_Payment_StudentImportNotice = "請注意:<br>- 已繳交之用戶不會被更改 <br>- 如金額空白, 則表示此用戶無需繳交此項費用<br>- 只限一個資助來源 ";
	$i_Payment_alert_forcepay = "你確定要替已選之用戶進行繳款程序?";
	$i_Payment_alert_undopay = "你確定要把已選用戶之繳款紀錄還原?";
	$i_Payment_alert_refund = "你確定要把已選用戶退款? (此程序不能還原)";
	$i_Payment_alert_forcepayall = "你確定要替用戶進行繳款程序?";
	$i_Payment_Menu_Settings_PaymentItem_Import_Action_New="新增用戶";
	$i_Payment_Menu_Import_PPSLink = "PPS 戶口連結";
	$i_Payment_Field_MappedStudent = "已轉到用戶";
	$i_Payment_Field_CancelDepositTime = '取消增值記錄時間';
	$Lang['ePayment']['AllBalances'] = "所有結存";
	$Lang['ePayment']['NegativeBalancesOnly'] = "負結存";
	$Lang['ePayment']['ElectronicBilling'] = "電子繳費單";
	$Lang['ePayment']['StudentNumber'] = "學生證號碼";
	$Lang['ePayment']['DatetimeByUsername'] = "被 <!--USERNAME--> 更新於 <!--DATETIME-->";
	$Lang['ePayment']['NegativeBalanceRecords'] = "負結餘的記錄";
	$Lang['ePayment']['GeneratedRecords'] = "已產生的記錄";
	$Lang['ePayment']['PaymentReceiptList'] = "繳費收據列表";
	$Lang['ePayment']['PaymentReceiptSearchHint'] = "可以搜索收據號碼、班別或學生姓名。";
	$Lang['ePayment']['DuplicatedRecord'] = "重覆的記錄。";
	$Lang['ePayment']['BankTransfer'] = "銀行轉帳";
	$Lang['ePayment']['ChequeDeposit'] = "支票存款";
	$Lang['ePayment']['TransactionDetailBankTransfer'] = "銀行轉帳  / Bank transfer";
	$Lang['ePayment']['TransactionDetailChequeDeposit'] = "支票存款  / Cheque deposit";
	$Lang['ePayment']['Account'] = "戶口";
	$Lang['ePayment']['ReceiptNo'] = "收據編號";
	$Lang['ePayment']['CashDepositApproval'] = "現金存入批核";
	$Lang['ePayment']['ApprovalUsers'] = "批核用戶";
	$Lang['ePayment']['CashDepositApprovalRemark'] = "現金存入記錄需要等待管理員批核後才能生效。";
	$Lang['ePayment']['ExcludeDate'] = "不顯示日期";
	$Lang['ePayment']['TNGPaymentRecords'] = "TNG";
	$Lang['ePayment']['OrderNo'] = "帳單號碼";
	$Lang['ePayment']['TNGRefNo'] = "TNG 繳費號碼";
	$Lang['ePayment']['TransactionTime'] = "交易時間";
	$Lang['ePayment']['ChargeStatus'] = "收費狀況";
	$Lang['ePayment']['PaidBy'] = "付款人";
	$Lang['ePayment']['Success'] = "成功";
	$Lang['ePayment']['Fail'] = "失敗";
	$Lang['ePayment']['Pending'] = "待定";
	$Lang['ePayment']['Void'] = "取消交易";
	$Lang['ePayment']['Voided'] = "已取消";
	$Lang['ePayment']['Refund'] = "退款";
	$Lang['ePayment']['Refunded'] = "已退款";
	$Lang['ePayment']['ToBeRefunded'] = "待退款";
	$Lang['ePayment']['ExportRefundedRecords'] = "匯出退款紀錄";
	$Lang['ePayment']['ImportRefundedRecords'] = "匯入退款紀錄";
	$Lang['ePayment']['ImportAlipayTransactionRecords'] = "匯入 AlipayHK 交易紀錄";
	$Lang['ePayment']['ImportTNGTransactionRecords'] = "匯入 TNG 交易紀錄";
	$Lang['ePayment']['ImportTNGTransactionRecordsInstruction'] = "請上載 TNG 交易紀錄的csv文件:";
	$Lang['ePayment']['ImportAlipayTransactionRecordsInstruction'] = "請上載  AlipayHK 交易紀錄的csv文件:";
	$Lang['ePayment']['InvalidTNGTransactionRecordAndItemNotFound'] = "無效的交易記錄。 找不到付款項目。";
	$Lang['ePayment']['TNGRecordWasProcessed'] = "此交易記錄已被".$Lang['Header']['Menu']['ePayment']."處理。";
	$Lang['ePayment']['ProceedToNextStepToAddTNGRecord'] = "繼續下一步將交易記錄添加到".$Lang['Header']['Menu']['ePayment']."。";
	$Lang['ePayment']['WithinDateRange'] = "只限日期範圍內";
	$Lang['ePayment']['CanExceedDateRange'] = "可超過日期範圍";
	$Lang['ePayment']['PaymentTime'] = "付款時間";
	$Lang['ePayment']['StudentPaymentMethodSettings'] = "學生付款方法設定";
	$Lang['ePayment']['NoPaymentMethod'] = "無付款方法";
	$Lang['ePayment']['SetSelectedStudentsPaymentMethodAs'] = "設定所選學生的付款方法為";
	$Lang['ePayment']['SetAllStudentsPaymentMethodAs'] = "設定所有學生的付款方法為";
	$Lang['ePayment']['PaymentItemStatusReport'] = "繳費項目狀況報告";
	$Lang['ePayment']['PaidPaymentItems'] = "已付款繳費項目";
	$Lang['ePayment']['UnpaidPaymentItems'] = "未付款繳費項目";
	$Lang['ePayment']['AmountRemarks'] = "備註：自動填充項目內的金額為戶口結存不足金額";
	$Lang['ePayment']['OutstandingAmount'] = "未付款金額";
	$Lang['ePayment']['InadequateAmount'] = "不足金額";
	$Lang['ePayment']['SendReminder'] = "發送提示訊息";
	$Lang['ePayment']['SendNotificationAfterPaid'] = "成功付款後發送通知訊息";
	$Lang['ePayment']['GroupBy'] = "分組方式";
	$Lang['ePayment']['PrintDate'] = "列印日期";
	$Lang['ePayment']['PaymentDate'] = "付款日期";
	$Lang['ePayment']['TNGPaidCount'] = "TNG繳交人數";
	$Lang['ePayment']['TNGAmountPaid'] = "TNG繳交金額";
	$Lang['ePayment']['ManualPaidCount'] = "手動繳交人數";
	$Lang['ePayment']['ManualAmountPaid'] = "手動繳交金額";
	$Lang['ePayment']['TNGVoidTodayTransactionRemark'] = "系統只允許取消今天的交易。";
	$Lang['ePayment']['SendPaymentNotification'] = '發送已付款通知訊息';
	$Lang['ePayment']['SendPaymentNotificationTooltipDescription'] = '發送eClass App推送訊息以通知家長繳費項目已付款';
	$Lang['ePayment']['SendReminderNotificationTooltipDescription'] = '發送eClass App推送訊息以提示家長繳付繳費項目';
	$Lang['ePayment']['SendPaymentNotificationSelectionWarning'] = '請只選取已付款的學生。';
	$Lang['ePayment']['SendReminderNotificationSelectionWarning'] = '請只選取未付款的學生。';
	$Lang['ePayment']['SearchWithTime'] = '搜索時間';
	$Lang['ePayment']['MerchantAccount'] = '商戶';
	$Lang['ePayment']['ServiceProviderAndSchoolAccount'] = '服務供應商及收款帳戶';
	$Lang['ePayment']['ServiceProvider'] = '服務供應商';
	$Lang['ePayment']['MerchantAccountName'] = '帳戶名稱';
	$Lang['ePayment']['MerchantUID'] = '商戶UID';
	$Lang['ePayment']['Alipay'] = 'AlipayHK';
	$Lang['ePayment']['TNG'] = 'TNG';
	$Lang['ePayment']['FPS'] = '轉數快';
	$Lang['ePayment']['TapAndGo'] = 'Tap & Go 拍住賞';
	$Lang['ePayment']['VisaMaster'] = 'VisaMaster';
	$Lang['ePayment']['WeChat'] = 'WeChat Pay';
	$Lang['ePayment']['AlipayCN'] = '支付寶';
	$Lang['ePayment']['PaymentGateway'] = '服務供應商';
	$Lang['ePayment']['DirectPay'] = 'Direct pay';
	$Lang['ePayment']['MustSignNoticeInApp'] = '請於 eClass Parent App 內簽署此通告。';
	$Lang['ePayment']['UserLogin'] = "學生編號";
	$Lang['ePayment']['AlipayTopUp'] = "AlipayHK 增值 / AlipayHK Top-up";
	$Lang['ePayment']['TNGTopUp'] = "TNG 增值 / TNG Top-up";
	$Lang['ePayment']['FPSTopUp'] = '轉數快 增值 / FPS Top-up';
	$Lang['ePayment']['TapAndGoTopUp'] = 'Tap & Go 拍住賞 增值 / Tap & Go Top-up';
	$Lang['ePayment']['VisaMasterTopUp'] = 'VisaMaster 增值 / VisaMaster Top-up';
	$Lang['ePayment']['WeChatTopUp'] = 'WeChat Pay 增值 / WeChat Pay Top-up';
	$Lang['ePayment']['AlipayCNTopUp'] = '支付寶 增值 / 支付寶 Top-up';
	$Lang['ePayment']['CurrentRemainQuota'] = '現時餘下豁免交易額: {COUNT}個';
	$Lang['ePayment']['ManageQuota'] = '管理每月豁免交易額';
	$Lang['ePayment']['EwalletRefund'] = '電子錢包退款';
	$Lang['ePayment']['PaymentItemRefund30Days'] = '系統只允許為過去30天內的交易作退款。';
	$Lang['ePayment']['PaymentItemRefundToday'] = '系統只允許為今天的交易作退款。';
	$Lang['ePayment']['ConfirmAccountEnoughBalance'] = '請確保戶口內有足夠餘額進行退款';
	$Lang['ePayment']['SetupMerchant'] = '未有設定商戶，是否確定呈送?';
	$Lang['ePayment']['PaymentProcessFail'] = '未能成功繳費，請與學校聯絡';
	$Lang['ePayment']['PaymentCopyWarning'] = '- 完成複製後，請設定商戶<br/>- 完成複製後，請編輯個別學生的應繳金額';

	$Lang['ePayment']['PaymentFailOrCanceled_0'] = '出現異常狀態 (api 錯誤)。';
	$Lang['ePayment']['PaymentFailOrCanceled_1'] = '應該已完成項目繳付，但出現異常狀態，請聯絡學校檢查付款紀錄。';
	$Lang['ePayment']['PaymentFailOrCanceled_2'] = '付款過程已被封鎖, 請10分鐘後再試。\r\n\r\n原因:\r\n1. 付款過程曾被取消\r\n2. 其他用戶付款進行中';
	$Lang['ePayment']['PaymentFailOrCanceled_3'] = '付款過程已被封鎖, 請10分鐘後再試。\r\n\r\n原因:\r\n1. 付款過程曾被取消\r\n2. 其他用戶付款進行中';
	$Lang['ePayment']['PaymentFailOrCanceled_other'] = '繳款未成功';
	
	$Lang['ePayment']['SelectAllStudents'] = "選取所有學生";
	$Lang['ePayment']['UnselectAllStudents'] = "取消選取所有學生";
	$Lang['ePayment']['SubsidyIdentity'] = "學生資助身份";
	$Lang['ePayment']['SubsidyIdentitySettings'] = "學生資助身份設定";
	$Lang['ePayment']['SubsidyIdentityName'] = "學生資助身份名稱";
	$Lang['ePayment']['SubsidyIdentityNameUsedByAnotherRecord'] = "學生資助身份名稱已被其他紀錄所使用。";
	$Lang['ePayment']['SubsidyIdentityCode'] = "代碼";
	$Lang['ePayment']['SubsidyIdentityCodeUsedByAnotherRecord'] = "代碼已被其他紀錄所使用。";
	$Lang['ePayment']['PaymentNoticeCanPayAtSchool'] = "啟用「到校繳費」選項";
	$Lang['ePayment']['EnableTopUpInApp'] = "啟用 eClass Parent App「增值」功能";
    $Lang['ePayment']['AdminTransferMoneyForStudents'] = "替學生轉賬";
    $Lang['ePayment']['SubsidyIdentityExportClassNameClassNumber'] = "匯出學生班別名稱及班號";
    $Lang['ePayment']['SubsidyIdentityExportLoginId'] = "匯出學生內聯網帳號";
    $i_SubsidyIdentity_Import_Format_SILink = "<b><u>學生班別名稱及班號：</u></b><br>
第一欄：<font color='red'>*</font>學生資助身份代碼  <a href=\"javascript:showMenu2('img_3','ToolMenu2');\"><img id='img_3' src=\"$image_path/$LAYOUT_SKIN/inventory/icon_help.gif\" border=\"0\"></a><br>
第二欄：<font color='red'>*</font>班別<br>
第三欄：<font color='red'>*</font>班號<br>
第四欄：<font color='red'>^</font>學生姓名<br>
第五欄：有效時段開始日 <span class=\"tabletextremark\">(YYYY-MM-DD)</span><br>
第六欄：有效時段結束日 <span class=\"tabletextremark\">(YYYY-MM-DD)</span>
";
    $i_SubsidyIdentity_Import_Format_SILink2 = "<b><u>學生內聯網帳號：</u></b><br>
第一欄：<font color='red'>*</font>學生資助身份代碼 <a href=\"javascript:showMenu2('img_4','ToolMenu2');\"><img id='img_4' src=\"$image_path/$LAYOUT_SKIN/inventory/icon_help.gif\" border=\"0\"></a><br>
第二欄：<font color='red'>*</font>內聯網帳號<br>
第三欄：<font color='red'>^</font>學生姓名<br>
第四欄：有效時段開始日 <span class=\"tabletextremark\">(YYYY-MM-DD)</span><br>
第五欄：有效時段結束日 <span class=\"tabletextremark\">(YYYY-MM-DD)</span>
";

    $Lang['ePayment']['SubsidyIdentityImportHeader_1']['EN'] = array("Subsidy Identity Code","Class Name","Class Number","Student Name","Effective Start Date","Effective End Date");
    $Lang['ePayment']['SubsidyIdentityImportHeader_1']['B5'] = array("學生資助身份代碼","班別","班號","學生姓名","有效時段開始日","有效時段結束日");
    $Lang['ePayment']['SubsidyIdentityImportHeader_2']['EN'] = array("Subsidy Identity Code","Login ID","Student Name","Effective Start Date","Effective End Date");
    $Lang['ePayment']['SubsidyIdentityImportHeader_2']['B5'] = array("學生資助身份代碼","內聯網帳號","學生姓名","有效時段開始日","有效時段結束日");
    $Lang['ePayment']['StudentSubsidyIdentityReport'] = '學生資助身份報告';

    $Lang['ePayment']['StudentSubsidyReport'] = '學生資助使用報告';
    $Lang['ePayment']['StudentSubsidyNoticeName'] = '通告名稱';
    $Lang['ePayment']['StudentSubsidyOriginalAmount'] = '應繳金額';
    $Lang['ePayment']['StudentSubsidyAmount'] = '實收金額';
    $Lang['ePayment']['StudentSubsidySubsidizedAmount'] = '資助金額';

    $Lang['ePayment']['DataImportFail'] = "0|=|資料匯入失敗";
    $Lang['ePayment']['SubsidyIdentityCode'] = "學生資助身份代碼";
    $Lang['ePayment']['SubsidyIdentityCodeNotFound'] = "找不到學生資助身份代碼";
    $Lang['ePayment']['StudentNotFound'] = "找不到該學生";
    $Lang['ePayment']['StudentWillRemoveExistingIdentity'] = "該學生已擁有其他學生資助身份，系統會移除現有的學生資助身份";
    $Lang['ePayment']['ImportMoreThanOneStudentFound'] = "找到多於一位學生";
    $Lang['ePayment']['ImportMoreThanOneSubsidyIdentityCodeFound'] = "找到多於一個學生資助身份代碼";
    $Lang['ePayment']['PresetSubsidyIdentitySetOnSameFile'] = "系統中找到相同資助身份代碼, 學生, 學生的紀錄";
    $Lang['ePayment']['SubsidyIdentityEffectivePeriod'] = "有效時期";
    $Lang['ePayment']['SubsidyIdentityEffectiveStartDate'] = "有效日期開始日";
	$Lang['ePayment']['SubsidyIdentityEffectiveEndDate'] = "有效日期結束日";
	$Lang['ePayment']['SubsidyIdentityEffectivePeriodRemark'] = "留空開始日期和結束日期表示沒有限期。";
	$Lang['ePayment']['StudentsHaveExistingSubsidyIdentityConfirmMsg'] = "以下學生已分配了學生資助身份，現有的學生資助身份將會被移除， 確定要繼續?";
    $Lang['General']['WarningRecord'] = '警告紀錄';
    $Lang['General']['Warning'] = '警告';
    $Lang['ePayment']['PaymentSendReminderPushMessage'] = "發送提示訊息";
    $Lang['ePayment']['PaymentStartDate'] = "繳費開始日期";
    $Lang['ePayment']['PaymentEndDate'] = "繳費結束日期";
    $Lang['ePayment']['DefaultPushMessageTitle'] = '最新繳費項目提示 [($Payment_StartDate)] Latest payment item alert [($Payment_StartDate)]';
    $Lang['ePayment']['DefaultPushMessageContent'] = '請於 ($Payment_EndDate) 或之前，為貴 子弟繳付款項「($Payment_Item)」(費用為 $($Payment_Amount))。'."\r\n\r\n".'Please be reminded to settle payment item "($Payment_Item)" (cost $($Payment_Amount)) by ($Payment_EndDate).';
    $Lang['ePayment']['IncomeExpensesReport'] = "收支報告";
    $Lang['ePayment']['PaymentItemTotalAmount'] = "繳費項目總數";
    $Lang['ePayment']['LeaveSchoolRefund'] = "離校退款";
	$Lang['ePayment']['LeaveSchoolRefundReport'] = "離校退款報告";
	$Lang['ePayment']['RefundTime'] = "退款時間";
	$Lang['ePayment']['TopUp'] = "增值";
	$Lang['ePayment']['AccountRevenue'] = "入賬";
	$Lang['ePayment']['PaymentItemRefundReport'] = "繳費項目退款報告";
	$Lang['ePayment']['ViewUsersTransactionRecords'] = "檢視用戶收支紀錄";
	$Lang['ePayment']['HandlingFee'] = "進誌手續費";
	$Lang['ePayment']['AdministrativeFee'] = '行政費';
	$Lang['ePayment']['FPSMonthlyWaiveCount'] = '每月豁免交易額(如適用)';
	$Lang['ePayment']['FPSWaiveCountName'] = '豁免交易額';
	$Lang['ePayment']['AlipayHandlingFee'] = "AlipayHK 手續費 / AlipayHK Handling Fee";
	$Lang['ePayment']['PaymentItemRefunded'] = "繳費項目已退款。";
	$Lang['ePayment']['PayAtSchoolDisplayWordAtParentApp'] = "「到校繳費」於eClass Parent App的顯示字眼";
	$Lang['ePayment']['AutoFillItemsPaymentItemStartDateEndDateAreNotApplicableToNotificationMessage'] = "請注意[".$Lang['ePayment']['PaymentStartDate']."]與[".$Lang['ePayment']['PaymentEndDate']."]的自動填充項目皆不適用於此通知訊息。";
	$Lang['ePayment']['TotalAmount'] = "總額";
	$Lang['ePayment']['PaymentDetail'] = "繳費詳情";
	$Lang['ePayment']['SettlementTime'] = "結算時間";
	$Lang['ePayment']['SettlementStatus'] = "結算狀況";
	$Lang['ePayment']['IssueDate'] = "發出日期";
	$Lang['ePayment']['StudentSubsidyReportValidDate'] = "有效日期";
	$Lang['ePayment']['DoublePaidPaymentReport'] = '重覆繳費紀錄報告';
}
/* ------------------- End of ePayment -------------------- */

/* ------------------- Start of Student Attendance ------------------- */
if ($plugin['attendancestudent'] || $plugin['attendancelesson']) {
	$Lang['StudentAttendance']['SettingApplySuccess'] = "1|=|設定更新成功";
	$Lang['StudentAttendance']['SettingApplyFail'] = "0|=|設定更新失敗";
	$Lang['StudentAttendance']['HelperListUpdateSuccess'] = "1|=|學生名單更新成功";
	$Lang['StudentAttendance']['HelperListUpdateFail'] = "0|=|學生名單更新失敗";
	$Lang['StudentAttendance']['DailyStatusConfirmSuccess'] = "1|=|每日紀錄確定成功";
	$Lang['StudentAttendance']['DailyStatusConfirmFail'] = "0|=|每日紀錄確定失敗";
	$Lang['StudentAttendance']['LateListConfirmSuccess'] = "1|=|每日遲到紀錄確定成功";
	$Lang['StudentAttendance']['LateListConfirmFail'] = "0|=|每日遲到紀錄確定失敗";
	$Lang['StudentAttendance']['AbsenceListConfirmSuccess'] = "1|=|每日缺席紀錄確定成功";
	$Lang['StudentAttendance']['AbsenceListConfirmFail'] = "0|=|每日缺席紀錄確定失敗";
	$Lang['StudentAttendance']['EarlyLeaveCreateSuccess'] = "1|=|早退紀錄新增成功";
	$Lang['StudentAttendance']['EarlyLeaveCreateFail'] = "0|=|早退紀錄新增失敗";
	$Lang['StudentAttendance']['EarlyLeaveListConfirmSuccess'] = "1|=|早退紀錄確定成功";
	$Lang['StudentAttendance']['EarlyLeaveListConfirmFail'] = "0|=|早退紀錄確定失敗";
	$Lang['StudentAttendance']['OfflineRecordImportSuccess'] = "1|=|離線資料匯入成功";
	$Lang['StudentAttendance']['OfflineRecordImportFail'] = "0|=|離線資料匯入失敗";
	$Lang['StudentAttendance']['EntryReasonUpdatedSuccess'] = "1|=|出入原因更新成功";
	$Lang['StudentAttendance']['EntryReasonUpdatedFail'] = "0|=|出入原因更新失敗";
	$Lang['StudentAttendance']['PresetAbsenceUpdateSuccess'] = "1|=|學生請假紀錄更新成功";
	$Lang['StudentAttendance']['PresetAbsenceUpdateFail'] = "0|=|學生請假紀錄更新失敗";
	$Lang['StudentAttendance']['PresetAbsenceDeleteSuccess'] = "1|=|學生請假紀錄移除成功";
	$Lang['StudentAttendance']['PresetAbsenceDeleteFail'] = "0|=|學生請假紀錄移除失敗";
	$Lang['StudentAttendance']['PresetAbsenceCreateSuccess'] = "1|=|學生請假紀錄新增成功";
	$Lang['StudentAttendance']['PresetAbsenceCreateFail'] = "0|=|學生請假紀錄新增失敗";
	$Lang['StudentAttendance']['MonthlyDataDeleteSuccess'] = "1|=|清除過時資料成功";
	$Lang['StudentAttendance']['MonthlyDataDeleteFail'] = "0|=|清除過時資料失敗";
	$Lang['StudentAttendance']['OutingRecordCreateSuccess'] = "1|=|外出紀錄新增成功";
	$Lang['StudentAttendance']['OutingRecordCreateFail'] = "0|=|外出紀錄新增失敗";
	$Lang['StudentAttendance']['OutingRecordUpdateSuccess'] = "1|=|外出紀錄更新成功";
	$Lang['StudentAttendance']['OutingRecordUpdateFail'] = "0|=|外出紀錄更新失敗";
	$Lang['StudentAttendance']['OutingRecordDeleteSuccess'] = "1|=|外出紀錄移除成功";
	$Lang['StudentAttendance']['OutingRecordDeleteFail'] = "0|=|外出紀錄移除失敗";
	$Lang['StudentAttendance']['DetentionRecordCreateSuccess'] = "1|=|留堂紀錄新增成功";
	$Lang['StudentAttendance']['DetentionRecordCreateFail'] = "0|=|留堂紀錄新增失敗";
	$Lang['StudentAttendance']['DetentionRecordUpdateSuccess'] = "1|=|留堂紀錄更新成功";
	$Lang['StudentAttendance']['DetentionRecordUpdateFail'] = "0|=|留堂紀錄更新失敗";
	$Lang['StudentAttendance']['DetentionRecordDeleteSuccess'] = "1|=|留堂紀錄移除成功";
	$Lang['StudentAttendance']['DetentionRecordDeleteFail'] = "0|=|留堂紀錄移除失敗";
	$Lang['StudentAttendance']['ReminderRecordCreateSuccess'] = "1|=|老師召見提示新增成功";
	$Lang['StudentAttendance']['ReminderRecordCreateFail'] = "0|=|老師召見提示新增失敗";
	$Lang['StudentAttendance']['ReminderRecordUpdateSuccess'] = "1|=|老師召見提示更新成功";
	$Lang['StudentAttendance']['ReminderRecordUpdateFail'] = "0|=|老師召見提示更新失敗";
	$Lang['StudentAttendance']['ReminderRecordDeleteSuccess'] = "1|=|老師召見提示移除成功";
	$Lang['StudentAttendance']['ReminderRecordDeleteFail'] = "0|=|老師召見提示移除失敗";
	$Lang['StudentAttendance']['LeaveOptionSaveSuccess'] = "1|=|學生離校選項更新成功";
	$Lang['StudentAttendance']['LeaveOptionSaveFail'] = "0|=|學生離校選項更新失敗";
	$Lang['StudentAttendance']['CardIDUpdateSuccess'] = "1|=|智能咭 ID 更新成功 ";
	$Lang['StudentAttendance']['CardIDUpdateFail'] = "0|=|智能咭 ID 更新失敗";
	$Lang['StudentAttendance']['ProfileAttendCount'] = "考勤紀錄 - 缺席之統計方法設定";
	$Lang['StudentAttendance']['SMSSentSuccessfully'] = "1|=|短訊發送成功 ";
	$Lang['StudentAttendance']['DataOutOfDate'] = "0|=|在你進行編輯期間，此紀錄已被其他登入用戶、或智能卡使用者更改。你的編輯已被取消。";
	$Lang['StudentAttendance']['AttendLateSymbol'] = "Ø";
	$Lang['StudentAttendance']['TeacherIAccountStudentProfileEdit'] = '允許班主任在"我的戶口"修改學生檔案紀錄原因';
	$Lang['StudentAttendance']['ImportOldTerminalOfflineRecord'] = '從終端機離線紀錄匯入';
	$Lang['StudentAttendance']['PeriodType'] = '日期範圍類別';
	$Lang['StudentAttendance']['CurrentPeriodDay'] = '日期';
	$Lang['StudentAttendance']['AttendanceType'] = '考勤類型';

	$Lang['StudentAttendance']['ImportAttendenceData'] = '匯入考勤資料';
	$Lang['StudentAttendance']['ImportAttendDataFormatDesc'] = '第一欄 : Class<br />
																第二欄 : Class No.<br />
																第三欄 : Date (YYYY-MM-DD)<br />
																第四欄 : AMInStatus (1 = 缺席, 2 = 遲到)<br />
																第五欄 : AMInTime (HH:MM:SS)(遲到才需要填)<br />
																第六欄 : AMInWaive (1 = 豁免, 0 = 不豁免)<br />
																第七欄 : AMInReason<br />
																第八欄 : AMOutStatus (3 = 早退)<br />
																第九欄 : AMOutWaive (1 = 豁免, 0 = 不豁免)<br />
																第十欄 : AMOutReason<br />
																第十一欄 : PMInStatus (1 = 缺席, 2 = 遲到)<br />
																第十二欄 : PMInTime (HH:MM:SS)(遲到才需要填)<br />
																第十三欄 : PMInWaive (1 = 豁免, 0 = 不豁免)<br />
																第十四欄 : PMInReason<br />
																第十五欄 : PMOutStatus (3 = 早退)<br />
																第十六欄 : PMOutWaive (1 = 豁免, 0 = 不豁免)<br />
																第十七欄 : PMOutReason<br />';
	$Lang['StudentAttendance']['StudentName'] = "學生姓名";
	$Lang['StudentAttendance']['Class'] = "班別";
	$Lang['StudentAttendance']['ClassNumber'] = "學號";
	$Lang['StudentAttendance']['AMInStatus'] = "上午進入狀況";
	$Lang['StudentAttendance']['AMInTime'] = "上午進入時間";
	$Lang['StudentAttendance']['AMInWaive'] = "上午進入豁免";
	$Lang['StudentAttendance']['AMInReason'] = "上午進入原因";
	$Lang['StudentAttendance']['AMOutStatus'] = "上午離開狀況";
	$Lang['StudentAttendance']['AMOutWaive'] = "上午離開豁免";
	$Lang['StudentAttendance']['AMOutReason'] = "上午離開原因";
	$Lang['StudentAttendance']['PMInStatus'] = "下午進入狀況";
	$Lang['StudentAttendance']['PMInTime'] = "下午進入時間";
	$Lang['StudentAttendance']['PMInWaive'] = "下午進入豁免";
	$Lang['StudentAttendance']['PMInReason'] = "下午進入原因";
	$Lang['StudentAttendance']['PMOutStatus'] = "下午離開狀況";
	$Lang['StudentAttendance']['PMOutWaive'] = "下午離開豁免";
	$Lang['StudentAttendance']['PMOutReason'] = "下午離開原因";
	$Lang['StudentAttendance']['Present'] = "準時";
	$Lang['StudentAttendance']['Absent'] = "缺席";
	$Lang['StudentAttendance']['Late'] = "遲到";
	$Lang['StudentAttendance']['EarlyLeave'] = "早退";
	$Lang['StudentAttendance']['Outing'] = "外出活動";
	$Lang['StudentAttendance']['Waived'] = "豁免";
	$Lang['StudentAttendance']['NotWaived'] = "不豁免";
	$Lang['StudentAttendance']['DataDistribution'] = "數據分佈";
	$Lang['StudentAttendance']['PMNotFollowAM'] = "默認下午狀況不跟上午狀況";
	$Lang['StudentAttendance']['DailyAbsentAnalysisReport'] = "每日缺席分析報告";
	$Lang['StudentAttendance']['AbsentSessionType'] = "缺席類型";
	$Lang['StudentAttendance']['BothAMPMAbsent'] = "上午和下午皆缺席";
	$Lang['StudentAttendance']['OnlyAMAbsent'] = "只是上午缺席";
	$Lang['StudentAttendance']['OnlyPMAbsent'] = "只是下午缺席";
	$Lang['StudentAttendance']['EntryLeaveDate'] = "學生入學/離校日期設定";
	$Lang['StudentAttendance']['EntryDate'] = "入學";
	$Lang['StudentAttendance']['LeaveDate'] = "離校";
	$Lang['StudentAttendance']['EntryLeavePeriodOverlapWarning'] = "*入學/離校期重疊";
	$Lang['StudentAttendance']['EntryLeavePeriodNullWarning'] = "*請最少輸入開始日期或結束日期";
	$Lang['StudentAttendance']['EntryLeavePeriodEndDateLesserThenStartDateWarning'] = "*結束日期必須在開始日期之後";
	$Lang['StudentAttendance']['EntryPeriodSaveSuccess'] = "1|=|入學/離校期儲存成功 ";
	$Lang['StudentAttendance']['EntryPeriodSaveFail'] = "0|=|入學/離校期儲存失敗 ";
	$Lang['StudentAttendance']['EntryPeriodRemoveSuccess'] = "1|=|入學/離校期移除成功 ";
	$Lang['StudentAttendance']['EntryPeriodRemoveFail'] = "0|=|入學/離校期移除失敗 ";
	$Lang['StudentAttendance']['EnableEntryLeavePeriodSetting'] = "開啓".$Lang['StudentAttendance']['EntryLeaveDate'];
	$Lang['StudentAttendance']['SelectCSVFile'] = "選擇CSV檔案";
	$Lang['StudentAttendance']['CSVConfirmation'] = "確定";
	$Lang['StudentAttendance']['ImportResult'] = "匯入結果";
	$Lang['StudentAttendance']['EntryPeriodImportDesc'] = "第一欄 : 學生登入名稱<br />第二欄 : 入學日期 (YYYY-MM-DD)<br />第三欄 : 離校日期 (YYYY-MM-DD)";
	$Lang['StudentAttendance']['DataImportFail'] = "0|=|資料匯入失敗";
	$Lang['StudentAttendance']['UserLogin'] = "用戶登入名稱";
	$Lang['StudentAttendance']['InvalidDateFormat'] = '日期格式錯誤';
	$Lang['StudentAttendance']['ClassTeacherTakeOwnClassOnlySetting'] = '在我的智能卡紀錄中教職員只可為有關的班別點名';
	$Lang['StudentAttendance']['ISmartCardDisableTakePastRecord'] = '智能卡紀錄 - 不能為已過去日子點名';
	$Lang['StudentAttendance']['HasHandinParentLetter'] = "已交家長信";
	$Lang['StudentAttendance']['Notes'] = "管理員備註";
	$Lang['StudentAttendance']['SlotTimeFormatWarning'] = "*時間格式不符";
	$Lang['StudentAttendance']['SummaryStatistic'] = "簡要紀錄";
	$Lang['StudentAttendance']['DaysOfAbsent'] = "缺席日數";
	$Lang['StudentAttendance']['DaysOfLate'] = "遲到日數";
	$Lang['StudentAttendance']['DaysOfEarlyLeave'] = "早退日數";
	$Lang['StudentAttendance']['LateSessions'] = "遲到堂數";
	$Lang['StudentAttendance']['LateSessionsWarning'] = "請於遲到堂數輸入正整數";
	$Lang['StudentAttendance']['LateAbsentSessions'] = "遲到/缺席堂數";
	$Lang['StudentAttendance']['LateAbsentSessionsWarning'] = "請於遲到/缺席堂數輸入正整數";
	$Lang['StudentAttendance']['ImportRFID'] = "匯入無線射頻辨識ID";
	$Lang['StudentAttendance']['RFID'] = "無線射頻辨識ID";
	$Lang['StudentAttendance']['SymbolPresent'] = "<span >/</span>";
	$Lang['StudentAttendance']['SymbolAbsent'] = "<span >O</span>";
	$Lang['StudentAttendance']['SymbolLate'] = "<span lang=EN-US style='font-family:Symbol;mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:Symbol'><span style='mso-char-type:symbol;mso-symbol-font-family:Symbol'>&AElig;</span></span>";
	$Lang['StudentAttendance']['SymbolOuting'] = "z";
	$Lang['StudentAttendance']['SymbolEarlyLeave'] = "<span class=GramE><span lang=EN-US style='font-family:新細明體'>#</span></span>";
	$Lang['StudentAttendance']['SymbolSL'] = "<span style='font-family:新細明體'>⊕</span>";
	$Lang['StudentAttendance']['SymbolAR'] = "<span lang=EN-US style='font-family:Webdings;mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:Webdings'><span style='mso-char-type:symbol;mso-symbol-font-family:Webdings'>y</span></span>";
	$Lang['StudentAttendance']['SymbolLE'] = "<span lang=EN-US style='font-family:\"Wingdings 2\";mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'><span style='mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'>U</span></span>";
	$Lang['StudentAttendance']['SymbolTruancy'] = "<span lang=EN-US style='font-family:新細明體'>●</span>";
	$Lang['StudentAttendance']['SymbolWaived'] = "<span class=GramE><span lang=EN-US style='font-family:新細明體'>*</span></span>";
	$Lang['StudentAttendance']['NotationSymbols'] = "考勤符號";
	$Lang['StudentAttendance']['MonthlyStatistic'] = "本月統計";
	$Lang['StudentAttendance']['DailyStatistic'] = "每日統計";
	$Lang['StudentAttendance']['NumberOfAttendants'] = "在席人數";
	$Lang['StudentAttendance']['AveragePresentNumber'] = "平均出席人數";
	$Lang['StudentAttendance']['AverageAbsentNumber'] = "平均缺席人數";
	$Lang['StudentAttendance']['NumberOfPresent'] = "出席節數";
	$Lang['StudentAttendance']['NumberOfAbsent'] = "缺席節數";
	$Lang['StudentAttendance']['NumberOfLate'] = "遲到節數";
	$Lang['StudentAttendance']['NumberOfEarlyLeave'] = "早退節數";
	$Lang['StudentAttendance']['NumberOfSchoolDays'] = "本月上課共";
	$Lang['StudentAttendance']['Day'] = "日";
	$Lang['StudentAttendance']['MonthShortForm'] = array("一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月");
	$Lang['StudentAttendance']['ClassName'] = "班別";
	$Lang['StudentAttendance']['Gender'] = "性別";
	$Lang['StudentAttendance']['Others'] = "其它";
	$Lang['StudentAttendance']['Weekday'] = "星期";
	$Lang['StudentAttendance']['AM'] = "上午";
	$Lang['StudentAttendance']['PM'] = "下午";
	$Lang['StudentAttendance']['Confirmed'] = "紀錄已確認";
	$Lang['StudentAttendance']['DefaultAbsentReason'] = "默認缺席原因 (如沒其他設定)";
	$Lang['StudentAttendance']['DefaultLateReason'] = "默認遲到原因 (如沒其他設定)";
	$Lang['StudentAttendance']['DefaultEarlyReason'] = "默認早退原因 (如沒其他設定)";
	$Lang['StudentAttendance']['DefaultOutingReason'] = "默認外出活動原因 (如沒其他設定)";
	$Lang['StudentAttendance']['ViewWebsamsReasonCode'] = "檢視WebSAMS 原因代碼";
	$Lang['StudentAttendance']['OutingDate'] = "活動/外出日期";
	$Lang['StudentAttendance']['OutingStartTime'] = "活動/外出時間";
	$Lang['StudentAttendance']['OutingEndTime'] = "結束時間";
	$Lang['StudentAttendance']['OutingPIC'] = "負責老師";
	$Lang['StudentAttendance']['OutingFrom'] = "出發地點";
	$Lang['StudentAttendance']['OutingTo'] = "活動/外出地點";
	$Lang['StudentAttendance']['OutingReason'] = "活動/外出目的";
	$Lang['StudentAttendance']['OutingNotStartFromSchool'] = "不是由學校出發";
	$Lang['StudentAttendance']['OutingNotYetBack'] = "未/沒有返回";
	$Lang['StudentAttendance']['OutingRemark'] = "備註";
	$Lang['StudentAttendance']['DayType'] = "時段";
	$Lang['StudentAttendance']['DayTypeWD'] = "全日";
	$Lang['StudentAttendance']['DayTypeAM'] = "上午";
	$Lang['StudentAttendance']['DayTypePM'] = "下午";
	$Lang['StudentAttendance']['SelectSessionWarning'] = "請選擇欲設定的時段";
	$Lang['StudentAttendance']['InvalidTimeWarning'] = "請輸入有效的時間";
	$Lang['StudentAttendance']['ImpotOutingDescription'] = "
			第一欄 : 班別 <br>
			第二欄 : 班號 <br>
			第三欄 : 日期 (如留空即為<b>今天</b>)(YYYY-MM-DD) <br>
			第四欄 : 時段 (AM/PM) <br>
			第五欄 : 負責老師姓名 <br>
			第六欄 : 參考外出時間 (HH:mm:ss) <br>
			第七欄 : 參考結束時間 (HH:mm:ss) <br>
			第八欄 : 出發地點 <br>
			第九欄 : 活動/外出地點 <br>
			第十欄 : 原因 <br>
			第十一欄 : 備註
			";
	$Lang['StudentAttendance']['EmptyReasonWarning'] = "請輸入原因";
	$Lang['StudentAttendance']['StudentNotFound'] = "找不到該學生";
	$Lang['StudentAttendance']['ImportMoreThanOneStudentFound'] = "找到多於一位學生";
	$Lang['StudentAttendance']['PresetOutingPICNotFound'] = "找不到負責人";
	$Lang['StudentAttendance']['PresetOutingSetPreviously'] = "系統中找到相同日期, 時段, 學生的紀錄";
	$Lang['StudentAttendance']['PresetOutingSetOnSameFile'] = "在同一檔中找到相同日期, 時段, 學生的紀錄";
	$Lang['StudentAttendance']['PresetOutMUSTFileMissed'] = "請最少輸入班名/班號及時段";
	$Lang['StudentAttendance']['PresetOutTimeSlotMissMatch'] = "時段必須為AM或PM";
	$Lang['StudentAttendance']['ApplyToTheseClassAlso'] = "也套用到這些班別上(選擇性)";
	$Lang['StudentAttendance']['ApplyToTheseGroupAlso'] = "也套用到這些組別上(選擇性)";
	$Lang['StudentAttendance']['FilterNoDataDate'] = "隱藏没有資料的日期";
	$Lang['StudentAttendance']['LunchOut'] = "中午外出";
	$Lang['StudentAttendance']['LastConfirmPerson'] = "最後確認人 ";
	$Lang['StudentAttendance']['Outgoing'] = "外出活動 ";
	$Lang['StudentAttendance']['UnConfirmed'] = "未確認 ";
	$Lang['StudentAttendance']['OfficalLeave'] = "公假/例假";
	$Lang['StudentAttendance']['RequestLeave'] = "請假";
	$Lang['StudentAttendance']['PlayTruant'] = "曠課";
    $Lang['StudentAttendance']['PlayTruantShow'] = "曠課";
    $Lang['StudentAttendance']['SessionsStat'] = "課節統計";
    $Lang['StudentAttendance']['NonSchoolDayReason'] = '非上課日原因';
	$Lang['StudentAttendance']['iSmartCardRemark'] = "老師備註";
	$Lang['StudentAttendance']['eNoticeTemplate'] = "電子通告模板";
	$Lang['StudentAttendance']['CumulativeProfile'] = "累積缺席/遲到";
	$Lang['StudentAttendance']['eNoticeTemplateActive'] = "使用中";
	$Lang['StudentAttendance']['eNoticeTemplateInActive'] = "非使用中";
	$Lang['StudentAttendance']['eNoticeTemplateName'] = "模板名稱 ";
	$Lang['StudentAttendance']['eNoticeTemplateCategory'] = "種類 ";
	$Lang['StudentAttendance']['eNoticeTemplateStatus'] = "狀況";
	$Lang['StudentAttendance']['eNoticeTemplateReplySlip'] = "附有回條";
	$Lang['StudentAttendance']['eNoticeTemplatePreview'] = "預覽";
	$Lang['StudentAttendance']['eNoticeTemplateSubject'] = "標題";
	$Lang['StudentAttendance']['eNoticeTemplateContent'] = "內容";
	$Lang['StudentAttendance']['RecordDate'] = "日期";
	$Lang['StudentAttendance']['AdditionInfo'] = "附加資料";
	$Lang['StudentAttendance']['TimeSlot'] = "時段";
	$Lang['StudentAttendance']['Reason'] = "原因";
	$Lang['StudentAttendance']['FromDate'] = "開始日期";
	$Lang['StudentAttendance']['ToDate'] = "結束日期";
	$Lang['StudentAttendance']['TotalCount'] = "總數";
	$Lang['StudentAttendance']['Insert'] = "加入";
	$Lang['StudentAttendance']['eNotice']['TemplateNameWarning'] = "請輸入".$Lang['StudentAttendance']['eNoticeTemplateName'];
	$Lang['StudentAttendance']['eNotice']['SubjectWarning'] = "請輸入".$Lang['StudentAttendance']['eNoticeTemplateSubject'];
	$Lang['StudentAttendance']['eNotice']['PleaseSelectCategory'] = "請選擇".$Lang['StudentAttendance']['eNoticeTemplateCategory'];
	$Lang['StudentAttendance']['eNotice']['TemplateSaveSuccess'] = "1|=|".$Lang['StudentAttendance']['eNoticeTemplate']."儲存成功 ";
	$Lang['StudentAttendance']['eNotice']['TemplateSaveFail'] = "0|=|".$Lang['StudentAttendance']['eNoticeTemplate']."儲存失敗 ";
	$Lang['StudentAttendance']['eNotice']['TemplateDeleteSuccess'] = "1|=|".$Lang['StudentAttendance']['eNoticeTemplate']."移除成功 ";
	$Lang['StudentAttendance']['eNotice']['TemplateDeleteFail'] = "0|=|".$Lang['StudentAttendance']['eNoticeTemplate']."移除失敗 ";
	$Lang['StudentAttendance']['SendNotice'] = "傳送電子通告";
	$Lang['StudentAttendance']['CheckSendNotice'] = "請選擇欲傳送電子通告的同學";
	$Lang['StudentAttendance']['ShowIfCountMoreThan'] = "顯示當紀綠多於";
	$Lang['StudentAttendance']['CumulativeProfileNotice'] = "累積缺席/遲到者發函";
	$Lang['StudentAttendance']['CountMoreThanWarning'] = "此欄必須為數字及不能留空";
	$Lang['StudentAttendance']['eNotice']['NoticeSentSuccess'] = "1|=|電子通告傳送成功 ";
	$Lang['StudentAttendance']['eNotice']['PrintNotice'] = "列印電子通告";
	$Lang['StudentAttendance']['ViewWEBSAMSReasonRemark'] = "如需更改，請前往行政綜合平台 > 內聯網設定 > WebSAMS 整合 > 原因代碼管理";
	$Lang['StudentAttendance']['NoCardTappingReport'] = "沒有拍卡報告";
	$Lang['StudentAttendance']['RFIDDuplicateWarning'] = "無線射頻辨識ID已被其他使用者使用";
	$Lang['StudentAttendance']['Attend'] = "出席";
	$Lang['StudentAttendance']['HostelAttend'] = "出席";
	$Lang['StudentAttendance']['HostelAbsent'] = "缺席";
	$Lang['StudentAttendance']['HostelNormalLeave'] = "正常離開";
	$Lang['StudentAttendance']['HostelEarlyLeave'] = "早退";
	$Lang['StudentAttendance']['NumberOfOnTime'] = "準時數";
	$Lang['StudentAttendance']['Detail'] = "詳細";
	$Lang['StudentAttendance']['Summary'] = "摘要";
	$Lang['StudentAttendance']['SelectAbsentSessionTypeWarning'] = "請選擇至少一項缺席節數類別。";
	$Lang['StudentAttendance']['NumberOfRequestLeave'] = "請假節數";
	$Lang['StudentAttendance']['NumberOfPlayTruant'] = "曠課節數";
	$Lang['StudentAttendance']['NumberOfOfficialLeave'] = "公假節數";
	$Lang['StudentAttendance']['PersonalSelection'] = "選擇個別學生";
	$Lang['StudentAttendance']['NoMissConductReport'] = "勤學報告";
	$Lang['StudentAttendance']['IncludeWaive'] = "包括豁免記錄";
	$i_StudentAttendance_Menu_OtherFeatures_AttendanceList_LunchboxCancel = "退飯情況";
	$i_SmartCard_Frontend_Take_Attendance_CancelLunch = "退飯";
	$i_StudentAttendance_Menu_OtherFeatures_AttendanceList_LunchboxCancel_Count = "退飯人數";
	//	$Lang['StudentAttendance']['PresetAbsenceImportDesc'] = '第一欄 : 班別<br />
	//																第二欄 : 班號<br />
	//																第三欄 : 日期 (YYYY-MM-DD)<br />
	//																第四欄 : 時段 (2 = 上午, 3 = 下午) <br/>
	//																第五欄 : 原因 (請參考 其他功能 -> 檢視WebSAMS 原因代碼) <br/>
	//																第六欄 : 豁免 (0 = 不豁免, 1 = 豁免) <br/>
	//																第七欄 : 備註 <br/>
	//																';
	$Lang['StudentAttendance']['PresetAbsenceImportDesc'] = array('班別', '班號', '日期 ', '時段', '原因', '豁免', '已呈交證明文件', '備註');
	$Lang['StudentAttendance']['PresetAbsenceImportRemarks'][2] = '(YYYY-MM-DD)';
	$Lang['StudentAttendance']['PresetAbsenceImportRemarks'][3] = '(2 = 上午, 3 = 下午)';
	$Lang['StudentAttendance']['PresetAbsenceImportRemarks'][4] = '(請參考 其他功能 -> 檢視WebSAMS 原因代碼)';
	$Lang['StudentAttendance']['PresetAbsenceImportRemarks'][5] = '(0 = 不豁免, 1 = 豁免)';
	$Lang['StudentAttendance']['PresetAbsenceImportRemarks'][6] = '(0 = 未呈交, 1 = 已呈交)';
	
	$Lang['StudentAttendance']['PresetAbsenceImportDescTW'] = array('班別', '班號', '類別', '開始日期', '結束日期','節數(由)','節數(至)','假別代號','原因', '備註');
	$Lang['StudentAttendance']['PresetAbsenceImportRemarksTW'][2] = '(0 = 全日, 1 = 上午, 2 = 下午, 3 = 節數)';
	$Lang['StudentAttendance']['PresetAbsenceImportRemarksTW'][3] = '(YYYY-MM-DD)';
	$Lang['StudentAttendance']['PresetAbsenceImportRemarksTW'][4] = '(YYYY-MM-DD)';
	$Lang['StudentAttendance']['PresetAbsenceImportRemarksTW'][8] = '(請參考 其他功能 -> 檢視WebSAMS 原因代碼)';
	$Lang['StudentAttendance']['InvalidAttendanceType'] = '不正確類別';
	$Lang['StudentAttendance']['InvalidSession'] = '不正確節數';
	$Lang['StudentAttendance']['InvalidLeaveType'] = '不正確假別';
	
	$Lang['StudentAttendance']['InvalidTimeSlot'] = '不正確時段 (2 = 上午, 3 = 下午)';
	$Lang['StudentAttendance']['DuplicateImportPresetAbsence'] = '在相同檔案中找到重複的紀錄';
	$Lang['StudentAttendance']['OutingReasonManage'] = $Lang['StudentAttendance']['Outgoing'].'原因管理';
	$Lang['StudentAttendance']['OutingReasonDuplicateOrBlank'] = '*原因不能留空或重複';
	$Lang['StudentAttendance']['OutingReasonDeleteConfirm'] = '您是否確定要移除此原因?';
	$Lang['StudentAttendance']['ReasonManagement'] = '原因管理';
	$Lang['StudentAttendance']['WaiveCodeNotValid'] = '不正確的豁免代號 (0 = 不豁免, 1 = 豁免)';
	$Lang['StudentAttendance']['HandinProveCodeNotValid'] = '不正確的呈交證明文件代號 (0 = 未呈交, 1 = 已呈交)';
	$Lang['StudentAttendance']['ClassSexRatioReport'] = '學生出席人數登記表';
	$Lang['StudentAttendance']['PleaseSelectAtLeastOnClass'] = '*請最少選擇一個班別';
	$Lang['StudentAttendance']['NumberOfStudent'] = '原有人數';
	$Lang['StudentAttendance']['Male'] = '男';
	$Lang['StudentAttendance']['Female'] = '女';
	$Lang['StudentAttendance']['NumberOfStudentInPreset'] = '未確認的準時學生人數';
	$Lang['StudentAttendance']['NumberOfStudentInLate'] = '未確認的遲到學生人數';
	$Lang['StudentAttendance']['MinsToIgnoreLateTapCardRecord'] = '如遲到超過此時間，不記錄拍卡時間 [ ] 分鐘';
	$Lang['StudentAttendance']['MinsToIgnoreLateTapCardRecordRemark'] = '<b>[這是什麼？]</b><br>如果不希望系統計算於時段後期回校的學生為「遲到」，你可以輸入一個大於零的時間，並且設定「預設出席狀況」 為「無出席」。例如，當上午時段是由9:00開始到12:00結束，而你輸入了120分鐘，並且已經設定「預 設出席狀況」 為「無出席」，則11:00後拍卡回校的學生，其出席狀態將視為「缺席」。<br><br>如無需使用此功能，請輸入「0」。';
	$Lang['StudentAttendance']['MinsToIgnoreLateTapCardRecordNote'] = '(如不使用請輸入0)';
	$Lang['StudentAttendance']['MinsToTreatAsPMIn'] = '下午時段開始前此分鐘的拍卡計算為"午後回校"(即準時)';
	$Lang['StudentAttendance']['ClassMemberAsInAcademicYear'] = '顯示學生該學年的班別資料';
	$Lang['StudentAttendance']['AbsenceSessionReport']['ShowAllStudent'] = "包括沒有任何缺席 (遲到、請假、曠課、公假)紀錄的學生";
	$Lang['StudentAttendance']['DataOutdatedWarning'] = "0|=|資料已過時，請重新呈送一次。";
	$Lang['StudentAttendance']['Warning']['NoNeedToTakeAttendanceOnSelectedDate'] = "所選的日子無須點名。";
	$Lang['StudentAttendance']['RemovePastData'] = "清除舊紀錄";
	$Lang['StudentAttendance']['DataExist'] = "有紀錄存在";
	$Lang['StudentAttendance']['Warning']['PleaseSelectAtLeastOneDay'] = "請至少選擇一日。";
	$Lang['StudentAttendance']['Warning']['RemoveWholeDayDataWarning'] = "請注意刪除的紀錄將不可能被還原，你真的要刪除已選日子的所有紀錄?";
	$Lang['StudentAttendance']['Warning']['RemoveWholeDayDataRemark'] = "所有拍咭紀錄 (包括時間, 每月紀錄, 學生檔案中的資料) 都會被移除。";
	$Lang['StudentAttendance']['iSmartCardOnlyDisplaySchoolDayAttendanceRecord'] = "智能卡紀錄 - 學生/家長 只顯示上學日子的考勤紀錄";
	$Lang['StudentAttendance']['StudentStatus'] = "學生狀況";
	$Lang['StudentAttendance']['Warning']['NoValidRecordsInImportFile']= "沒有可用的匯入紀錄。請核對清楚匯入紀錄是否正確。(例如: 智能咭 ID, 時間格式)";
	$Lang['StudentAttendance']['OnlySynceDisLateRecordWhenConfirm'] = "確認遲到記錄時才同步訓導管理遲到記錄";
	$Lang['StudentAttendance']['Warning']['SystemSettingsStudentEntryLeaveDate'] = "(如啟用此選項，請務必加入於[系統設定]的[學生入學/離校日期設定])";
	$Lang['StudentAttendance']['NormalLeave'] = "正常離開";
	$Lang['StudentAttendance']['UserDateRange'] = "使用日期範圍";
	$Lang['StudentAttendance']['RemoveAllEntryLeaveDates'] = "移除所有入學/離校日期";
	$Lang['StudentAttendance']['Warning']['ConfirmRemoveAllEntryLeaveDates'] = "你確定要移除所有入學/離校日期?";
	$Lang['StudentAttendance']['JustCanEditRecordWithinNDaysBefore'] = "在「我的智能咭紀錄」中，老師可編輯過去 [ ] 日的考勤紀錄";
	$Lang['StudentAttendance']['Unlimited'] = "無限制";
	$Lang['StudentAttendance']['Within'] = "";
	$Lang['StudentAttendance']['days'] = "日（包括今天）";

	$Lang['StudentAttendance']['ExportForOfflineReaderUse'] = "匯出離線拍咭機使用的學生及教職員資料";
	$Lang['StudentAttendance']['ExportForOfflineReaderUse_950e'] = "匯出離線拍咭機使用的學生及教職員資料 (950e 格式)";

	$Lang['StudentAttendance']['FluSurvRemark'] = "學校的eClass考勤管理系統將自動把已確認的考勤紀錄，傳送至香港大學公共衛生學院作缺席率監測之用";
	$Lang['StudentAttendance']['DisplayInRedForContinuousAbsent'] = "以紅色字顯示當連續缺席 <!--NUMBER--> 日";
	$Lang['StudentAttendance']['ZeroToDisable'] = "0 表示不生效";
	$Lang['StudentAttendance']['Warning']['SelectPeriodSession'] = "請選擇時段。";

	$Lang['StudentAttendance']['ViewIndividualSubjectGroupList'] = "檢視各科組情況";
	$Lang['StudentAttendance']['PleaseSelectAtLeastOneStatus'] = "請最少選擇一個狀況。";
	$Lang['StudentAttendance']['PleaseSelectAtLeastOneStudent'] = "請最少選擇一位學生。";
	$Lang['StudentAttendance']['PleaseSelectOneStudent'] = "請選擇一位學生。";

	$Lang['StudentAttendance']['OfflineImportWarning']['NonSchoolDay'] = "*當日為非上課日。";
	$Lang['StudentAttendance']['OfflineImportWarning']['InTimeLaterThanLeaveTime'] = "*回校上課時間遲過放學時間 <!--LEAVE_TIME-->。";
	$Lang['StudentAttendance']['OfflineImportWarning']['TimeEarlierThanSchoolStartTime'] = "*時間早過回校上課時間 <!--START_TIME-->。";
	$Lang['StudentAttendance']['OfflineImportWarning']['IgnoreWarningContinue'] = "你確定要忽略警告並提交?";

	$Lang['StudentAttendance']['SessionSettings'] = "堂數設定";
	$Lang['StudentAttendance']['DefaultNumAbsent'] = "預設課堂數目(缺席)";
	$Lang['StudentAttendance']['DefaultNumLate'] = "預設課堂數目(遲到)";
	$Lang['StudentAttendance']['DefaultNumRequestedLeave'] = "預設課堂數目(請假)";
	$Lang['StudentAttendance']['DefaultNumAbsenteesism'] = "預設課堂數目(曠課)";
	$Lang['StudentAttendance']['DefaultNumPublicOfficialHoliday'] = "預設課堂數目(公假/例假)";

	$Lang['StudentAttendance']['Checking'] = "檢查中...";
	$Lang['StudentAttendance']['Warning']['ConfirmOverwriteDuplicatedOutingRecords'] = "部分外出紀錄會取代現有的外出紀錄，你確定要繼續?";

	$Lang['StudentAttendance']['StudentTapCardPushMessage']['Arrive'] = "<!--StudentNameCh-->已於<!--DateTime-->到校。
<!--StudentNameEn--> has reached school at <!--DateTime-->.";
	$Lang['StudentAttendance']['StudentTapCardPushMessage']['Leave'] = "<!--StudentNameCh-->已於<!--DateTime-->離校。
<!--StudentNameEn--> has left school at <!--DateTime-->.";
	$Lang['StudentAttendance']['StudentAttendance']['Arrive'] = "學生到校
Student Reaches School";
	$Lang['StudentAttendance']['StudentAttendance']['Leave'] = "學生離校
Student Leaves School";
	$Lang['StudentAttendance']['StudentTapCardStatusAry']['Arrive'] = "到校";
	$Lang['StudentAttendance']['StudentTapCardStatusAry']['Leave'] = "離校";

	$Lang['StudentAttendance']['ImportTapCardRecords'] = "匯入拍卡紀錄";
	$Lang['StudentAttendance']['Button']['SynchronizeHolidays'] = "同步假期";

	$Lang['StudentAttendance']['ProveDocument'] = "已呈交證明文件";
	$Lang['StudentAttendance']['Processed'] = "已跟進";
	$Lang['StudentAttendance']['ProcessedBySchool'] = "校方已跟進";
	$Lang['StudentAttendance']['AbsentReasonManage'] = "缺席 老師備註管理";
	$Lang['StudentAttendance']['AbsentReasonManageInstruction'] = "你可以修改預設老師備註，每一行代表一個老師備註.";
	$Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'] = "請假申請 (App)";
	$Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeaveList'] = "請假申請列表";
	$Lang['StudentAttendance']['ApplyLeaveAry']['DocumentRequired'] = "必須呈交證明文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['SubmissionDeadline'] = "證明文件呈交期限";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NumberOfDayToSubmitDocument'] = "家長需於申請請假後 <!--dayNum--> 天或以內呈交證明文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['DocumentRemarks'] = "證明文件備註";
	$Lang['StudentAttendance']['ApplyLeaveAry']['ApplicationTime'] = "申請時間";
	$Lang['StudentAttendance']['ApplyLeaveAry']['AllApplicationTimeRecord'] = "所有申請時間紀錄";
	$Lang['StudentAttendance']['ApplyLeaveAry']['PastApplicationTimeRecord'] = "過往申請紀錄";
	$Lang['StudentAttendance']['ApplyLeaveAry']['TodayApplicationTimeRecord'] = "今天申請紀錄";
	$Lang['StudentAttendance']['ApplyLeaveAry']['Document'] = "證明文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['DocumentApprovalStatus'] = "證明文件批核狀況";
	$Lang['StudentAttendance']['ApplyLeaveAry']['RecordAcknowledgeStatus'] = "確認狀況";
	$Lang['StudentAttendance']['ApplyLeaveAry']['Acknowledge'] = "確認";
	$Lang['StudentAttendance']['ApplyLeaveAry']['Reject'] = "拒絕";
	$Lang['StudentAttendance']['ApplyLeaveAry']['Cancel'] = "取消";
	$Lang['StudentAttendance']['ApplyLeaveAry']['AcknowledgedLeave'] = "已確認";
	$Lang['StudentAttendance']['ApplyLeaveAry']['PendingAcknowledgeLeave'] = "待確認";
	$Lang['StudentAttendance']['ApplyLeaveAry']['RejectedLeave'] = "已拒絕";
	$Lang['StudentAttendance']['ApplyLeaveAry']['CancelledLeave'] = "已取消";
	$Lang['StudentAttendance']['ApplyLeaveAry']['AllAcknowledgeStatus'] = "所有確認狀況";
	$Lang['StudentAttendance']['ApplyLeaveAry']['ApproveDocument'] = "批准文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['RejectDocument'] = "要求重交文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['AllDocumentStatus'] = "所有證明文件批核狀況";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NoDocument'] = "沒有證明文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['PendingDocument'] = "待批核證明文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['ApprovedDocument'] = "已批核證明文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['RejectedDocument'] = "待重交證明文件";
	$Lang['StudentAttendance']['ApplyLeaveAry']['WD'] = "全日";
	$Lang['StudentAttendance']['ApplyLeaveAry']['AM'] = "上午";
	$Lang['StudentAttendance']['ApplyLeaveAry']['PM'] = "下午";
	$Lang['StudentAttendance']['ApplyLeaveAry']['Duration'] = "日數";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['ReceiveApplicationTitle'] = "請假申請確認通知
Leave Application Alert";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['ReceiveApplicationContent'] = "已收到閣下於 <!--datetime--> 提交    貴子弟 <!--studentNameChWithClass--> 由 <!--startDateTime--> 至 <!--endDateTime--> 的請假申請。
Please note that the leave application for your child <!--studentNameEnWithClass--> from <!--startDateTime--> to <!--endDateTime--> submitted at <!--datetime--> has been received.";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['AcceptApplicationTitle'] = "接納請假申請通知
Leave Application Accepted Alert";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['AcceptApplicationContent'] = "校方已接納閣下於 <!--datetime-->  提交      貴子弟 <!--studentNameChWithClass--> 由 <!--startDateTime--> 至 <!--endDateTime--> 的請假申請。
Please note that the leave application for your child <!--studentNameEnWithClass--> from <!--startDateTime--> to <!--endDateTime--> submitted at <!--datetime--> has been accepted.";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['ResubmitDocumentTitle'] = "要求重交文件通知
Request Document Re-submission Alert";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['ResubmitDocumentContent'] = "閣下於 <!--datetime-->  提交      貴子弟 <!--studentNameChWithClass--> 的請假申請文件並未符合要求 / 並不清晰，請重新提交。
Please note that the leave application document of your child <!--studentNameEnWithClass--> submitted at <!--datetime--> did not meet the requirement / was not clear. Please submit again.";
	$Lang['StudentAttendance']['ApplyLeaveAry']['SendPushMessageToClassTeacher'] = "於家長申請請假時自動傳送推送訊息給有關學生的班主任";
	$Lang['StudentAttendance']['ApplyLeaveAry']['ReasonMustInput'] = '原因必須輸入';
	$Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['StudentLeaveApplicationTitle'] = "請假申請通知
Leave Application Alert";
	$Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['StudentLeaveApplicationContent'] = "<!--studentNameChWithClass--> 將由 <!--startDateTime--> 至 <!--endDateTime--> 因 <!--reason--> 請假 <!--numOfDays--> 天。
Please note that <!--studentNameEnWithClass--> will be on leave for <!--numOfDays--> day(s) from <!--startDateTime--> to <!--endDateTime--> due to <!--reason-->.";
    $Lang['StudentAttendance']['ApplyLeaveAry']['enableApplyLeaveCutOffTiming'] = "使用截止時間設定";
	$Lang['StudentAttendance']['ApplyLeaveAry']['applyLeaveCutOffTiming'] = "設定截止時間";
	$Lang['StudentAttendance']['ApplyLeaveAry']['daysBeforeApplyLeave'] = "需要提前幾天申請請假紀錄";
	$Lang['StudentAttendance']['ApplyLeaveAry']['maxApplyLeaveDay'] = "請假紀錄的最多申請天數";
	$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodSet'] = "請假期間家課遞交方式設定";
	$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodChi'] = "遞交方式(中文)";
	$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodEng'] = "遞交方式(英文)";
    $Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethod'] = "家課遞交方式";
    $Lang['StudentAttendance']['ApplyLeaveAry']['AMWDClasses'] = "上午班/全日";
    $Lang['StudentAttendance']['ApplyLeaveAry']['PMClasses'] = "下午班";

    $Lang['StudentAttendance']['WaiveAbsence'] = "銷假";
	$Lang['StudentAttendance']['AbsenceWaived'] = "已銷假";
	$Lang['StudentAttendance']['AbsenceNotWaived'] = "未銷假";
	$Lang['StudentAttendance']['AbsentProveReport']['AbsentProveReport'] = '缺席早退證明文件呈交狀況報告';
	$Lang['StudentAttendance']['AbsentProveReport']['Hand-inStatus'] = '呈交狀況';
	$Lang['StudentAttendance']['AbsentProveReport']['AllHand-inStatus'] = '所有呈交狀況';
	$Lang['StudentAttendance']['AbsentProveReport']['StatusNotHandin'] = '未呈交';
	$Lang['StudentAttendance']['AbsentProveReport']['AbsentNum'] = '缺席次數';
	$Lang['StudentAttendance']['AbsentProveReport']['NumberOfTimes'] = '次數';
	$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedNum'] = '欠交數目';
	$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedDates'] = '欠交日期';
	$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedProveDocNum'] = '欠交證明文件數目';
	$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedProveDocDates'] = '欠交證明文件日期';
	$Lang['StudentAttendance']['AbsentProveReport']['PushMessageremarks'] = '推送信息只會傳送到有未呈交證明文件紀錄及已登入eClass App之學生家長';
	$Lang['StudentAttendance']['AbsentProveReport']['RemarksInconsistency'] = '<span class="tabletextrequire">*</span> 代表與請假紀錄不一致';
	$Lang['StudentAttendance']['AbsentProveReport']['RemarksAMPMInconsistency'] = '<span class="tabletextrequire">*</span> 代表上/下午缺席證明文件呈交狀況不一致';
	$Lang['StudentAttendance']['AbsentProveReport']['DateOfAbsentLateEarlyLeave'] = '缺席/ 遲到/ 早退  日期';
	$Lang['StudentAttendance']['AbsentProveReport']['RemarksInconsistencyPreset'] = '<span class="tabletextrequire">*</span> 代表與請假紀錄不一致';
	$Lang['StudentAttendance']['AbsentProveReport']['RemarksInconsistencyApply'] = '<span class="tabletextrequire">#</span> 代表未確認請假紀錄';
	$Lang['StudentAttendance']['StudentAttendanceInfo']['Title'] = '出席資料報告';

	if($sys_custom['StudentAttendance_WaiveAbsent'])
	{
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['OverdueNotice'] = "逾期未銷假通知書";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DemeritForm'] = "未銷假報告 - 記缺點表格";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['BelowStudentRecordOneDemerit'] = "下列學生逾期未銷假記缺點一次";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ClassName'] = "班別";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ClassNumber'] = "學號";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['Name'] = "姓名";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['OverdueRecords'] = "逾期未銷假報告記錄";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DayMonthYearSession'] = "[ 日日 / 月月 / 年年 (節數) ]";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['PleaseNoticeParent'] = "*** 請通知家長 ***";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['GrowthGroup'] = "學生成長組";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ToMoralGuidanceDepartment'] = "致 ﹕學生成長組";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['YourRef'] = "來函檔號 ﹕";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['AbsenteeismOnThatDay'] = "該生當日曠課。";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['NotWaiveRecordOneDemerit'] = "該生未銷假，記缺點一次。";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['WaivedNoDemerit'] = "該生已辦銷假，不記缺點。";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ClassTeacherSignature'] = "班主任簽名 ﹕";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['StudentSignature'] = "學生簽名 ﹕";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DateColon'] = "日期 ﹕";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['PrintReportDate'] = "列印報告日期 ：";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['PrintReportTime'] = "時間：";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['NotWaiveAbsenceMoreThanSevenDays'] = "七天或以上未銷假又未報告記錄";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['Truancy'] = "曠課 / 另行處理";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['NoDemerit'] = "不記缺點";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['Demerit'] = "記缺點";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DateLong'] = "日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;期";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['RecorderSignature'] = "記錄者簽名：";
		$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ReportCompleted'] = "報告完成";
	}

	$Lang['StudentAttendance']['Attendance']="考勤";
	$Lang['StudentAttendance']['TurnTo']="轉為";
	$Lang['StudentAttendance']['Confirm']="確認";
	$Lang['StudentAttendance']['TeacherApp']['AreYouSureToSubmit'] = "你確認要提交嗎？";
	$Lang['StudentAttendance']['TeacherApp']['RecordHasUpdated'] = "記錄已經更新。";
	$Lang['StudentAttendance']['CannotTakeFutureDateRecord'] = "智能卡紀錄 - 不能為將來的日子點名";
	$Lang['StudentAttendance']['WarningCannotTakeFutureDateRecord'] = "不能為將來的日子點名";
	$Lang['StudentAttendance']['Reason'] = "原因";
	$Lang['StudentAttendance']['Remark'] = "老師備註";
	$Lang['StudentAttendance']['NotTake'] = "未點名";
	$Lang['StudentAttendance']['HasTaken'] = "已點名";
	$Lang['StudentAttendance']['ApplicationReason'] = "申請原因";
	$Lang['StudentAttendance']['RejectReason'] = "拒絕原因";
	$Lang['StudentAttendance']['TransferToAbsentRecord'] = "添加學生請假記錄";
	$Lang['StudentAttendance']['ApplyLeave']['ExistingRecord']="已有記錄";
	$Lang['StudentAttendance']['ApplyLeave']['SendPushNotification']="發送推送通知";
	$Lang['StudentAttendance']['ApplyLeave']['ApproveLeaveAlert']="請選擇至少一個項目。";
	$Lang['StudentAttendance']['ApplyLeave']['UpdateAcknowledgmentStatus']="更新確認狀況";
    $Lang['StudentAttendance']['ApplyLeave']['AMPMChooseAlert']="請選擇至少一個時段。";
	$Lang['StudentAttendance']['ApplyLeave']['AMPMDuplicate']="不能選擇重複時段。";

	$Lang['StudentAttendance']['EntryLog'] = "完整拍卡紀錄";
	$Lang['StudentAttendance']['DateSelected'] = "已選日期";
	$Lang['StudentAttendance']['LastModify'] = "最後更新";
	$Lang['StudentAttendance']['OfficeRemark'] = "校務處備註";
	$Lang['StudentAttendance']['PresetAbsenceReason'] = '請假原因';
	$Lang['StudentAttendance']['PresetAbsenceRemark'] = '請假備註';
	$Lang['StudentAttendance']['MonitorTerminal'] = "監控終端機";
	$Lang['StudentAttendance']['MonitorPeriod'] = "監控時間間隔";
	//$Lang['StudentAttendance']['MonitorPeriodSeconds'] = "秒";
	$Lang['StudentAttendance']['MonitorTimeSlot'] = "監控時段";
	$Lang['StudentAttendance']['ConfirmRemoveMonitorTimeSlot'] = "你確定要移除這個監控時段?";
	$Lang['StudentAttendance']['OverlappingTimeSlot'] = "與其他時段重疊了";
	$Lang['StudentAttendance']['NotifyUser'] = "通知用戶";
	$Lang['StudentAttendance']['ReportType'] = "報告類型";
	$Lang['StudentAttendance']['DetailRecords'] = "詳細紀錄";
	$Lang['StudentAttendance']['IncludeTapCardLocation'] = "包括拍卡地點";
	$Lang['StudentAttendance']['NumberOfOuting'] = "外出活動數";
	$Lang['StudentAttendance']['SyncAttendanceData'] = "同步考勤資料";
	$Lang['StudentAttendance']['AttendanceRecords'] = "出席記錄";
	$Lang['StudentAttendance']['Arrived'] = "到達";
	$Lang['StudentAttendance']['LateReason'] = "遲到理由";
	$Lang['StudentAttendance']['AM2'] = "早上";
	$Lang['StudentAttendance']['PM2'] = "下午";
	$Lang['StudentAttendance']['DateOfHandInParentLetter'] = "繳交家長信日期";
	$Lang['StudentAttendance']['TeacherSignature'] = "老師簽署";
	$Lang['StudentAttendance']['EntryLogByLocation'] = "以拍卡地點檢視出入紀錄";
	$Lang['StudentAttendance']['TapCardLocation'] = "拍卡地點";
	$Lang['StudentAttendance']['RequestSelectLocation'] = "請選擇拍卡地點。";
	$Lang['StudentAttendance']['EntryLogByLocationRemark'] = "請注意: 系統已假設拍卡紀錄以成對的出入紀錄為計算逗留時間的基準，不規則的紀錄將不予處理。";
	$Lang['StudentAttendance']['Hours'] = "小時";
	$Lang['StudentAttendance']['Minutes'] = "分";
	$Lang['StudentAttendance']['Seconds'] = "秒";
	$Lang['StudentAttendance']['HKUSPHDataSubmissionSettings'] = "港大疾病監測 提交數據設定";
	$Lang['StudentAttendance']['SubmissionJob'] = "提交數據工作";
	$Lang['StudentAttendance']['SubmissionTime'] = "提交數據時間";
	$Lang['StudentAttendance']['DailyOn'] = "每天 ";
	$Lang['StudentAttendance']['DisallowNonTeachingStaffTakeAttendance'] = "不允許非教學職員點名";
	$Lang['StudentAttendance']['CustomizedTimeSlotSettings'] = "自訂時段設定";
	$Lang['StudentAttendance']['StartTime'] = "開始時間";
	$Lang['StudentAttendance']['EndTime'] = "結束時間";
	$Lang['StudentAttendance']['AttendanceStatus'] = "考勤狀況";
	$Lang['StudentAttendance']['CustomizedTimeSlotRemark'] = "備註: 此時段設定只適用於回校拍卡的檢測。";
	$Lang['StudentAttendance']['InvalidTimeWarning'] = "開始時間必須早過結束時間。";
	$Lang['StudentAttendance']['TimePeriodOverlapWarning'] = "時間與另外一個時段(START_TIME - END_TIME)重疊了。";
	$Lang['StudentAttendance']['WebSAMS_MergeAMPMAsWD'] = "如果上午及下午狀況一致，合併並計算為全日(WD)。";
	$Lang['StudentAttendance']['WebSAMS_HalfDayCountAsWD'] = "如果只有半日的考勤紀錄則計算為全日(WD)。";
	$Lang['StudentAttendance']['TeacherCanManageDataAtMySmartCard'] = "在「我的智能咭紀錄」中，老師能管理以下的數據";
	$Lang['StudentAttendance']['NoBringCardPhoto'] = "無帶咭拍照紀錄";
	$Lang['StudentAttendance']['PhotoTakenTime'] = "拍攝照片時間";
	$Lang['StudentAttendance']['Photo'] = "照片";
	$Lang['StudentAttendance']['TerminalLocation'] = "終端機位置";
	$Lang['StudentAttendance']['ForgotBringCard'] = "忘記帶咭";
	$Lang['StudentAttendance']['BodyTemperatureRecords'] = "體溫紀錄";
	$Lang['StudentAttendance']['StudentBodyTemperatureAbnormalValue'] = '異常體溫';
	$Lang['StudentAttendance']['Above'] = '高於';
	$Lang['StudentAttendance']['BodyTemperature'] = "體溫";
	$Lang['StudentAttendance']['BodyTemperatureNormal'] = "正常";
	$Lang['StudentAttendance']['BodyTemperatureAbnormal'] = "不正常";
	$Lang['StudentAttendance']['HKUSPHParentConsentRecords'] = "港大疾病監測";
	$Lang['StudentAttendance']['ParentLoginID'] = "家長內聯網帳號";
	$Lang['StudentAttendance']['ParentName'] = "家長姓名";
	$Lang['StudentAttendance']['StudentLoginID'] = "學生內聯網帳號";
	$Lang['StudentAttendance']['ConsentStatus'] = "同意狀況";
	$Lang['StudentAttendance']['SignDate'] = "簽署時間";
	$Lang['StudentAttendance']['AllConsentStatus'] = "所有同意狀況";
	$Lang['StudentAttendance']['Agreed'] = "同意";
	$Lang['StudentAttendance']['Denied'] = "拒絕";
	$Lang['StudentAttendance']['Unsigned'] = "未簽";
	$Lang['StudentAttendance']['SetAsAgreed'] = "設定為同意";
	$Lang['StudentAttendance']['SetAsDenied'] = "設定為拒絕";
	$Lang['StudentAttendance']['SetAsUnsigned'] = "設定為未簽";
	$Lang['StudentAttendance']['ImportType'] = "匯入類型";
	$Lang['StudentAttendance']['General'] = "通用格式";
	$Lang['StudentAttendance']['From_eNotice'] = "從".$Lang['Header']['Menu']['eNotice']."匯出的格式";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'] = array();
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][] = array('Parent Login ID','家長內聯網帳號。','1',''); // field name, description, is required field, has extra output
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][] = array('Parent Name','家長名稱，用作參考顯示用途，可以不填。','0','');
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][] = array('Student Login ID','學生內聯網帳號。','1','');
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][] = array('Class Name','學生班別，用作參考顯示用途，可以不填。','0','');
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][] = array('Class Number','，學生班號，用作參考顯示用途，可以不填。','0','');
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][] = array('Student Name','學生名稱，用作參考顯示用途，可以不填。','0','');
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][] = array('Consent','1 表示同意，0 表示拒絕。','1','');
	$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][] = array('Consent Time','家長簽署時間，格式必須是 YYYY-MM-DD hh:mm:ss 或者 DD/MM/YYYY hh:mm:ss。留空即表示使用匯入系統時的時間。','0','');
	$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'] = array();
	$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][] = array('Class','學生班別。','1','');
	$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][] = array('Class No.','學生班號。','1','');
	$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][] = array('Student','學生名稱，用作參考顯示用途，可以不填。','0','');
	$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][] = array('Consent','如果包含關鍵字 "同意" 就表示同意。如果包含關鍵字 "不同意" 就表示不同意。','1','');
	$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][] = array('Parent Name','家長名稱。作為顯示名稱提交給港大。倘若留空，會使用家長簽名名稱作為替代。','0','');
	$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][] = array('Signer','家長簽名。必須要與內聯網的家長用戶英文名或中文名匹配。','1','');
	$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][] = array('Consent Time','家長簽署時間，格式必須是 YYYY-MM-DD hh:mm:ss 或者 DD/MM/YYYY hh:mm:ss。','1','');
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors'] = array();
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankParentLoginID'] = "家長內聯網帳號不能為空。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankStudentLoginID'] = "學生內聯網帳號不能為空。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['ParentUserCannotBeFound'] = "找不到家長用戶 <!--USERLOGIN-->。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['CannotFoundStudentForParent'] = "找不到匹配家長用戶 <!--PARENT_USERLOGIN--> 的學生用戶 <!--STUDENT_USERLOGIN-->。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['StudentUserCannotBeFound'] = "找不到學生用戶 <!--USERLOGIN-->。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['CannotFoundParentForStudent'] = "找不到匹配學生用戶 <!--STUDENT_USERLOGIN--> 的家長用戶 <!--PARENT_USERLOGIN-->。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['InvalidConsentTime'] = "簽署時間格式不正確，格式必須是年月日時分秒 YYYY-MM-DD hh:mm:ss 或者日月年時分秒DD/MM/YYYY hh:mm:ss。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['InvalidConsentValue'] = "同意選項不正確。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankClass'] = "班別不能為空。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankClassNumber'] = "班號不能為空。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankParentName'] = "家長名稱不能為空。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankSignatureName'] = "家長簽名不能為空。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['CannotFoundStudentWithClassClassNumber'] = "找不到班別是 <!--CLASSNAME--> 班號是 <!--CLASSNUMBER--> 的學生用戶。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['CannotFoundParent'] = "找不到家長用戶 <!--PARENTNAME-->。";
	$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['ConsentValueNoKeyword'] = "同意選項必須包含 \"同意\" 或者 \"不同意\" 關鍵字。";
	$Lang['StudentAttendance']['HostelAttendance'] = "宿舍點名";
	$Lang['StudentAttendance']['HostelAttendanceSettings'] = "宿舍點名設定";
	$Lang['StudentAttendance']['HostelGroupCategory'] = "宿舍小組所屬類別";
	$Lang['StudentAttendance']['HostelAttendanceAdmin'] = "宿舍點名管理員";
	$Lang['StudentAttendance']['ViewHostelGroupStatus'] = "檢視宿舍點名情況";
	$Lang['StudentAttendance']['TakeAttendanceForAllHostelGroups'] = "為所有宿舍小組點名";
	$Lang['StudentAttendance']['HostelGroup'] = "宿舍點名小組";
	$Lang['StudentAttendance']['InStatus'] = "進入狀況";
	$Lang['StudentAttendance']['OutStatus'] = "離開狀況";
	$Lang['StudentAttendance']['InTime'] = "進入時間";
	$Lang['StudentAttendance']['OutTime'] = "離開時間";
	$Lang['StudentAttendance']['YesterdayStatus'] = "昨日狀況";
	$Lang['StudentAttendance']['OptionalDataRemark'] = "<!--SYMBOL--> 可選入的數據。";
	$Lang['StudentAttendance']['HostelStudentList'] = "留宿學生名單";
	$Lang['StudentAttendance']['HostelStatistic'] = "留宿人數統計";
	$Lang['StudentAttendance']['HostelStudentReport'] = "學生留宿紀錄";
	$Lang['StudentAttendance']['Monthly'] = "按月份";
	$Lang['StudentAttendance']['Weekly'] = "按星期";
	$Lang['StudentAttendance']['Daily'] = "按日";
	$Lang['StudentAttendance']['CountFor'] = "計算";
	$Lang['StudentAttendance']['StayOvernight'] = "過夜";
	$Lang['StudentAttendance']['DoNotStayOvernight'] = "不過夜";
	$Lang['StudentAttendance']['GroupResponsibleUsers'] = "小組負責點名用戶";
	$Lang['StudentAttendance']['ResponsibleUsersForTakingAttendance'] = "負責點名的用戶";
	$Lang['StudentAttendance']['AddRemoveUsers'] = "加入/移除 用戶";
	$Lang['StudentAttendance']['CustomSettings'] = "自訂設定";
	$Lang['StudentAttendance']['LateTime'] = "遲到時間";
	$Lang['StudentAttendance']['EmailNotificationJobStatus'] = "電郵通知工作狀態";
	$Lang['StudentAttendance']['EmailNotificationJobExecutionTime'] = "電郵通知執行時間";
	$Lang['StudentAttendance']['LateAbsentRecords'] = "遲到缺席紀錄";
	$Lang['StudentAttendance']['PresetTeacherRecords'] = "老師自訂備註";
	$Lang['StudentAttendance']['NumberOfLate'] = "遲到次數";
	$Lang['StudentAttendance']['NumberOfAbsence'] = "缺席次數";
	$Lang['StudentAttendance']['MoreThanNumberOfLate'] = "多過或等於<!--NUMBER-->次遲到。";
	$Lang['StudentAttendance']['MoreThanNumberOfAbsence'] = "多過或等於<!--NUMBER-->次缺席。";
	$Lang['StudentAttendance']['And'] = "與";
	$Lang['StudentAttendance']['Or'] = "或者";
	$Lang['StudentAttendance']['ByDateRange'] = "使用日期範圍";
	$Lang['StudentAttendance']['EmailToClassTeachers'] = "發送電郵通知給班主任";
	$Lang['StudentAttendance']['NumberOfStudentsSentingProgressStatement'] = "<!--CURRENT--> / <!--TOTAL--> 位學生已發送。";
	$Lang['StudentAttendance']['SendStatus'] = "發送狀況";
	$Lang['StudentAttendance']['Pending'] = "等待中";
	$Lang['StudentAttendance']['Success'] = "成功";
	$Lang['StudentAttendance']['Failed'] = "不成功";
	$Lang['StudentAttendance']['CancelSending'] = "取消發送";
	$Lang['StudentAttendance']['SendingWasCancelled'] = "發送已被取消。";
	$Lang['StudentAttendance']['HKUGACLateAbsentRecordsNote'] = "備註: 遲到紀錄是以遲到時間遲過 <!--LATE_TIME--> 為基準來計算，並且遲到紀錄與缺席紀錄都是只計算上午上學時段的紀錄。";
	$Lang['StudentAttendance']['HKUGACLateAbsentEmailTitle'] = "eAttendance late/absent email notification";
	$Lang['StudentAttendance']['ContinuousAbsenceReport'] = '連續缺席報告';
	$Lang['StudentAttendance']['ContinuousAbsentDays'] = '連續缺席日數';
	$Lang['StudentAttendance']['Days'] = '日';
	$Lang['StudentAttendance']['AbsentDates'] = '缺席日子';
	$Lang['StudentAttendance']['ContinuousAbsenceAlert'] ='連續缺席提示';
	$Lang['StudentAttendance']['ContinuousAbsenceAlertRemark'] = '相同的報告結果將不會重複發送。';
	$Lang['StudentAttendance']['ContinuousAbsenceAlertEmailTitle'] = '連續缺席學生提示';
	$Lang['StudentAttendance']['ContinuousAbsenceAlertEmailContent'] = '以下學生至少已連續缺席<!--DAY-->天';
	$Lang['StudentAttendance']['TotalNumberOfStudent'] = '學生總人數';
	$Lang['StudentAttendance']['TotalNumberOfPresentStudent'] = '出席學生人數';
	$Lang['StudentAttendance']['TotalNumberOfOnTimeStudent'] = '準時學生人數';
	$Lang['StudentAttendance']['TotalNumberOfAbsentStudent'] = '缺席學生人數';
	$Lang['StudentAttendance']['TotalNumberOfLateStudent'] = '遲到學生人數';
	$Lang['StudentAttendance']['TotalNumberOfOutingStudent'] = '外出學生人數';
	$Lang['StudentAttendance']['TotalNumberOfNonConfirmedStudent'] = '未確認學生人數';
	$Lang['StudentAttendance']['iSmartCardDoNotAllowTeacherModifyAttendanceStatusAfterAdminConfirmedRecord'] = '智能卡紀錄 - 管理員確認記錄後，不允許教師修改考勤狀況';
	$Lang['StudentAttendance']['AttendanceRecordConfirmedByAdminAlertMsg'] = '此紀錄已被管理員確認，考勤狀況不能再作修改。';
    $Lang['StudentAttendance']['ReprintCard']['ReprintCard(App)'] = "補領智能卡 (App)";
    $Lang['StudentAttendance']['ReprintCard']['StudentPhoto'] = "學生相片";
    $Lang['StudentAttendance']['ReprintCard']['ClassNumber'] = "班號";
    $Lang['StudentAttendance']['ReprintCard']['ReplacementReason'] = "補領原因";
    $Lang['StudentAttendance']['ReprintCard']['DeliveryMethod'] = "送交辦法";
    $Lang['StudentAttendance']['ReprintCard']['ApplyDate'] = "申請日期";
    $Lang['StudentAttendance']['ReprintCard']['Status'] = "狀況";
    $Lang['StudentAttendance']['ReprintCard']['PendingConfirm'] = "待確認";
    $Lang['StudentAttendance']['ReprintCard']['PendingPayment'] = "待付款";
    $Lang['StudentAttendance']['ReprintCard']['AwaitingCardReprint'] = "待印卡";
    $Lang['StudentAttendance']['ReprintCard']['Sent'] = "已寄出 / 待領取";
    $Lang['StudentAttendance']['ReprintCard']['ApplicationCompleted'] = "已完成";
    $Lang['StudentAttendance']['ReprintCard']['Rejected'] = "已拒絕";
    $Lang['StudentAttendance']['ReprintCard']['Cancelled'] = "已取消";
    $Lang['StudentAttendance']['ReprintCard']['PickUp'] = "自取";
    $Lang['StudentAttendance']['ReprintCard']['CourierToApplicant'] = "速遞至申請人";
    $Lang['StudentAttendance']['ReprintCard']['StudentID'] = "學生代號";
    $Lang['StudentAttendance']['ReprintCard']['PPS'] = "PPS";
    $Lang['StudentAttendance']['ReprintCard']['IssueDate'] = "發行時間";
    $Lang['StudentAttendance']['ReprintCard']['ExpiryDate'] = "到期時間";
    $Lang['StudentAttendance']['ReprintCard']['PaymentMethod'] = "付款方式";
    $Lang['StudentAttendance']['ReprintCard']['PaymentItem'] = "付款項目";
    $Lang['StudentAttendance']['ReprintCard']['PaymentAmount'] = "付款金額";
    $Lang['StudentAttendance']['ReprintCard']['PaymentDate'] = "付款日期";
    $Lang['StudentAttendance']['ReprintCard']['ReferenceNumber'] = "參考編號";
    $Lang['StudentAttendance']['ReprintCard']['ReprintCardFee'] = "補領智能卡費用";
    $Lang['StudentAttendance']['ReprintCard']['TNG'] = "TNG";
    $Lang['StudentAttendance']['ReprintCard']['ALIPAY'] = "Alipay HK";
    $Lang['StudentAttendance']['ReprintCard']['AllStatus'] = "所有狀況";
    $Lang['StudentAttendance']['ReprintCard']['PleaseInsertRejectReason'] = "請輸入拒絕原因";
    $Lang['StudentAttendance']['ReprintCard']['Year(1-12)'] = "年度(1-12)";
    $Lang['StudentAttendance']['ReprintCard']['Class(1-12)'] = "班別(1-12)";
    $Lang['StudentAttendance']['ReprintCard']['ApproveAndNext'] = "批准並到下一個";
    $Lang['StudentAttendance']['ReprintCard']['Society'] = "社團";
    $Lang['StudentAttendance']['ReprintCard']['PleaseUploadStudentPhoto'] = "請上載學生相片";
    $Lang['StudentAttendance']['ReprintCard']['TeacherRemark'] = "老師備註";
    $Lang['StudentAttendance']['ReprintCard']['InsertSmartCardID'] = "輸入智能咭ID";
    $Lang['StudentAttendance']['ReprintCard']['SmartCardID'] = "智能咭ID";
    $Lang['StudentAttendance']['ReprintCard']['TurnToCompleted'] = "轉為已完成";
    $Lang['StudentAttendance']['ReprintCard']['PleaseInsertSmartCardID'] = "請輸入智能咭ID";
    $Lang['StudentAttendance']['ApprovalTime'] = "審批時間";
    $Lang['StudentAttendance']['ApprovalUser'] = "審批用戶";
    $Lang['StudentAttendance']['SeriousLate'] = "嚴重遲到";
    $Lang['StudentAttendance']['NumberOfSeriousLate'] = "嚴重遲到數";

    $Lang['StudentAttendance']['EmailNotificationJobSendToAdmin'] = '電郵通知管理員';
    $Lang['StudentAttendance']['EmailNotificationJobSendToTeacher'] = '電郵通知班主任';
    $Lang['StudentAttendance']['AllAdminUsers'] = '所有管理員';
    $Lang['StudentAttendance']['OnlySelectedAdminUsers'] = '只選擇管理員';
    $Lang['StudentAttendance']['AllTeacherUsers'] = '所有班主任';
    $Lang['StudentAttendance']['OnlySelectedTeacherUsers'] = '只選擇班主任';
    $Lang['StudentAttendance']['EmailNotificationJobCurrentAcademicYear'] = '本學年';
    $Lang['StudentAttendance']['EmailNotificationJobSpecificDateRange'] = '選擇時間範圍';
    $Lang['StudentAttendance']['SelectSendToAdminClassTeacherErrorMessage'] = '請選擇管理員或班主任';

    $Lang['StudentAttendance']['SessionFrom'] = '節數(由)';
    $Lang['StudentAttendance']['SessionTo'] = '節數(至)';
    $Lang['StudentAttendance']['PlsSelectSession'] = "請選擇節數";

	$Lang['StudentAttendance']['HostelGroupAbsenceReason'] = array('回家渡假','住院','其他');

	$Lang['StudentAttendance']['HostelGroupStudentList'] = '學生在宿舍/不在宿舍名單';
	$Lang['StudentAttendance']['StaffAtSchoolList'] = '職員在校/不在校名單';
	$Lang['StudentAttendance']['HostelGroupStudentPresentNumber'] = '在宿舍學生人數';
	$Lang['StudentAttendance']['HostelGroupStudentAbsentNumber'] = '不在宿舍學生人數';
	$Lang['StudentAttendance']['StaffAtSchoolNumber'] = '在校職員人數';
	$Lang['StudentAttendance']['StaffNotAtSchoolNumber'] = '不在校職員人數';
	$Lang['StudentAttendance']['HostelAttendanceLocationIn'] = '宿舍點名位置進入';
	$Lang['StudentAttendance']['HostelAttendanceLocationOut'] = '宿舍點名位置離開';
	$Lang['StudentAttendance']['HostelAttendanceDisablePastDate'] = '不能為已過去日子點名';
	$Lang['StudentAttendance']['HostelAttendanceDisableFutureDate'] = '不能為將來的日子點名';
	$Lang['StudentAttendance']['Hostel'] = '宿舍';
	$Lang['StudentAttendance']['StudentTapCardStatusAry']['HostelArrive'] = "到宿舍";
	$Lang['StudentAttendance']['StudentAttendance']['HostelArrive'] = "學生宿舍
Student Reaches Hostel";
	$Lang['StudentAttendance']['StudentTapCardPushMessage']['HostelArrive'] = "<!--StudentNameCh-->已於<!--DateTime-->到宿舍。
<!--StudentNameEn--> has reached hostel at <!--DateTime-->.";
	$Lang['StudentAttendance']['StudentTapCardStatusAry']['HostelLeave'] = "離開宿舍";
	$Lang['StudentAttendance']['StudentAttendance']['HostelLeave'] = "學生離開宿舍
Student Leaves Hostel";
	$Lang['StudentAttendance']['StudentTapCardPushMessage']['HostelLeave'] = "<!--StudentNameCh-->已於<!--DateTime-->離開宿舍。
<!--StudentNameEn--> has left hostel at <!--DateTime-->.";
	$Lang['StudentAttendance']['AllowClassTeacherManagePresetStudentAbsence'] = '容許班主任管理學生請假紀錄';
	$Lang['StudentAttendance']['StudentNotInTeacherClassList'] = '該學生不屬於任教班別';
	$Lang['StudentAttendance']['StudentNotInTeacherClassListOnlyForClassStudent'] = '該學生不屬於任教班別。只可為任教班別學生新增請假紀錄。';

	$Lang['StudentAttendance']['LeaveType'] = '假別';
	$Lang['StudentAttendance']['LeaveTypeCode'] = '假別代號';
	$Lang['StudentAttendance']['LeaveTypeName'] = '假別名稱';
	$Lang['StudentAttendance']['SameTypeNameExists'] = '相同的原因已經存在';
	$Lang['StudentAttendance']['SameTypeCodeExists'] = '相同的代號已經存在';
	$Lang['StudentAttendance']['TotalSession'] = '共節數';
	$Lang['LessonAttendance']['DailyLessonLateOverview'] = '檢視課堂遲到名單';
	$Lang['LessonAttendance']['DailyLessonAbsenceOverview'] = '檢視課堂缺席名單';
	$Lang['StudentAttendance']['LessonLateListConfirmSuccess'] = '1|=|課堂遲到紀錄確定成功';
	$Lang['StudentAttendance']['LessonAbsenceListConfirmSuccess'] = '1|=|課堂缺席紀錄確定成功';
	
	$Lang['StudentAttendance']['ViewBodyTemperatureStatus'] = '檢視學生體溫情況';
	$Lang['StudentAttendance']['StudentBodyTemperatureStatus'] = '學生體溫概況';
	$Lang['StudentAttendance']['DailyAbnormalBodyTemperature'] = '每天異常名單';
	$Lang['StudentAttendance']['StudentBodyTemperatureRecords'] = '學生體溫記錄檔';
	
	$Lang['StudentAttendance']['AbnormalBodyTemperature'] = '異常體溫';
	$Lang['StudentAttendance']['BodyTemperatureTime'] = '量度體溫時間';
	$Lang['StudentAttendance']['BodyTemperatureYearReference'] = '參考 - 本學年異常體溫記錄';
	$Lang['StudentAttendance']['BodyTemperatureTodayStatus'] = '今天統計';
	$Lang['StudentAttendance']['BodyTemperatureRecord'] = '體溫記錄';
	$Lang['StudentAttendance']['BodyTemperatureLastSevenDaysRecord'] = '最近14天統計';
	$Lang['StudentAttendance']['AbnormalBodyTemperatureStudentCount'] = '異常體溫人數';
	$Lang['StudentAttendance']['OnlyAllowTimeslotTakeAttendance'] = '只限第{TIMESLOTS}節才能進行點名';
}
/* ------------------- End of Student Attendance -------------------- */

/* ------------------- Start of Lesson Attendance -------------------- */
if ($plugin['attendancelesson']) {
	$Lang['LessonAttendance']['ClassFloorPlanSetup'] = '班別座位表';
	$Lang['LessonAttendance']['ImportLessonAttendanceAttendDataFormatDesc'] = '
																第一欄 : 班別名稱<br />
																第二欄 : 班號<br />
																第三欄 : 日期 (YYYY-MM-DD)<br />
																第四欄 : 時間表時段 (1 = 時段 1, 2 = 時段 2 ...etc)<br />
																第五欄 : 考勤狀況 (2 = 遲到, 3 = 缺席)<br />
																';
}
/* ------------------- End of Lesson Attendance -------------------- */
/* ------------------- Start of Student Attendance 3.0 --------------------- */
if(($module_version['StudentAttendance']==3.0 || strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/StudentMgmt/eAttendance/')!==false) && ($plugin['attendancestudent'] || $plugin['attendancelesson'])) {
	include_once('student_attendance_lang.b5.php');
}
/* -------------------  End of Student Attendance 3.0 ---------------------- */
###  eNotice
$Lang['eNotice']['SchoolNotice'] = "家長簽署通告";
$Lang['eNotice']['SchoolStudentNotice'] = "學生簽署通告";
$Lang['eNotice']['DisciplineNotice'] = "操行紀錄系統通告";
$Lang['eNotice']['Settings'] = "設定";
$Lang['eNotice']['BasicSettings'] = "基本設定";
$Lang['eNotice']['NoticeSettings'] = "通知設定";
$Lang['eNotice']['DisableNotice'] = "不使用". $Lang['Header']['Menu']['eNotice'];
$Lang['eNotice']['FullControlGroup'] = "進階管理小組<br>(可刪除通告及代家長/學生更改答案)";
$Lang['eNotice']['NormalControlGroup'] = "一般管理小組<br>(可發通告及檢視回條)";
$Lang['eNotice']['DisciplineGroup'] = "整批列印訓導管理系統通告";
$Lang['eNotice']['DisableClassTeacher'] = "不讓班主任代家長及學生更改回條";
$Lang['eNotice']['AllHaveRight'] = "所有教職員可發通告";
$Lang['eNotice']['DefaultNumDays'] = "預設簽署期限日數";
$Lang['eNotice']['AdminGroupOnly'] = "必須為行政小組";
$Lang['eNotice']['AllStaffAllow'] = "所有教職員可檢視回條內容";
$Lang['eNotice']['ParentStudentCanViewAll'] = "讓所有家長及學生檢視所有通告";
$Lang['eNotice']['SettingsUpdateSuccess'] = "1|=|設定成功";
$Lang['eNotice']['SettingsUpdateFailed'] = "0|=|設定失敗";
$Lang['eNotice']['Options'] = "通告選項";
$Lang['eNotice']['Type'] = "類型";
$Lang['eNotice']['NotifyStudentsByEmail'] = "以電郵通知學生";
$Lang['eNotice']['NotifyParentsByEmail'] = "以電郵通知家長";
$Lang['eNotice']['NotifyPICByEmail'] = "以電郵通知負責人";
$Lang['eNotice']['NotifyClassTeachersByEmail'] = "以電郵通知班主任";
$Lang['eNotice']['NotifyParentsByPushMessage'] = "以eClass App推送訊息通知家長";
$Lang['eNotice']['NotifyStudentsByPushMessage'] = "以eClass Student App推送訊息通知學生";
$Lang['eNotice']['NotifyPICByPushMessage'] = "以eClass Teacher App推送訊息通知負責人";
$Lang['eNotice']['NotifyClassTeachersByPushMessage'] = "以eClass Teacher App推送訊息通知班主任";
$Lang['eNotice']['NotifyEmailRemark'] = "備註：如勾選此功能，按 \"呈送\" 後會即時發出電郵。";
$Lang['eNotice']['NotifyRemark'] = "備註：如勾選此功能，按 \"呈送\" 後會即時發出電郵。";
$Lang['eNotice']['ToBeDistributed'] = "將會派發";
$Lang['eNotice']['NotToBeDistributed'] = "暫不派發";
$Lang['eNotice']['ReplySlipForm'] = "回條";
$Lang['eNotice']['ReplySlip'] = "回條";
$Lang['eNotice']['Preview'] = "預覽";
$Lang['eNotice']['PreviewSlipSetting'] = "檢視設定";
$Lang['eNotice']['SetReplySlipContent'] = "設定回條內容";
$Lang['eNotice']['ReplySlipQuestions'] = "回條問題";
$Lang['eNotice']['ReplySlipQuestionSetting'] = "回條設定";
$Lang['eNotice']['NonPaymentItem'] = "非繳費項目";
$Lang['eNotice']['LateSubmission'] = "逾期簽署";
$Lang['eNotice']['AllowLateSign'] = "容許遲交";
$Lang['eNotice']['NotAllowReSign'] = "不容許家長及學生於限期前修改答案";
$Lang['eNotice']['PICAllowReply'] = "負責人可代家長及學生更改回條";
$Lang['eNotice']['AllowClassTeacherSendeNoticeMessage'] = "容許班主任發送手機提醒";
$Lang['eNotice']['AllowAdminToSignPaymentNotice'] = "容許管理員代家長簽署繳費通告";
$Lang['eNotice']['Audience'] = "適用對象";
$Lang['eNotice']['AddStudents'] = "加入學生";
$Lang['eNotice']['UpdateStatusWarning'] = "若修改已發佈的電子通告內容,所有已填寫的回條內容,將會消失。";
$Lang['eNotice']['DisplayQuestionNumber'] = "顯示問題編號";
$Lang['eNotice']['MaxNumberReplySlipOption'] = "回條選項數目上限";
$Lang['eNotice']['QuestionNeedToReply'] = "題目必須回答";
$Lang['eNotice']['AllowClassTeacerAccessDisciplineNotice'] = "容許班主任於資訊服務內檢視訓導通告";
$Lang['eNotice']['Signed_Unsigned'] = "已簽及未簽";
$Lang['eNotice']['UnsignedOnly'] = "未簽";
$Lang['eNotice']['NoticeSummary'] = "通告總表";
$Lang['eNotice']['PrintDetails'] = "列印詳細資料";
$Lang['eNotice']['UpdateAudienceRemark'] = "如刪除對象，該對象所填寫的回條內容會一併被刪除。";
$Lang['eNotice']['ChangePaymentReplyRemark'] = "如刪除回條內問題，該相應繳費項目會一併被刪除。";
$Lang['eNotice']['UnexpiredNotice'] = '未過期通告';
$Lang['eNotice']['ExpiredNotice'] = '已過期通告';
$Lang['eNotice']['AllNoticeType'] = '全部學校通告';
$Lang['eNotice']['Signed'] = '已簽署';
$Lang['eNotice']['Not Signed'] = '未簽署';
$Lang['eNotice']['NotSignedNoticeNum'] = '未簽通告編號';
$Lang['eNotice']['NumberOfNotSigned'] = '未簽通告數目';
$Lang['eNotice']['NoticeSummaryReport']['PushMessageremarks'] = '推送信息只會傳送到有未簽通告及已登入eClass App之學生家長';
$Lang['eNotice']['SignedByAuthCode'] = '簽署授權碼';
$Lang['eNotice']['AssignNoticeToStudent'] = "設定通告";
$Lang['eNotice']['NoticeTotalNumber'] = "通告數量";
$Lang['eNotice']['NoticeInCurrentYear'] = "本年度通告";
$Lang['eNotice']['PleaseSelectOneNotice'] = "請選擇最少一個通告";
$Lang['eNotice']['FailedToSetNotice'] = "未能設定通告";
$Lang['eNotice']['NoticeNotIssuedYet'] = '此通告並未開放，請於 <!--startDate--> 或之後回來簽署通告。';
$Lang['eNotice']['NoticeDeleted'] = '通告已被刪除，故未能檢視此通告。';
$Lang['eNotice']['NoticeSuspended'] = '通告已被暫停，故未能檢視此通告。';
$Lang['eNotice']['AppNotEnabledNotice'] = '學校尚未啟用電子通告功能，故未能檢視此通告。';
$Lang['eNotice']['NotCurrentNoticeTarget'] = '你並不是此通告對象，故未能檢視。';
$Lang['eNotice']['ContentSetting'] = "通告內容設定";
$Lang['eNotice']['StaticContent'] = "固定內容";
$Lang['eNotice']['MergeContent'] = "合併內容";
$Lang['eNotice']['MergeCSVFormat'] = "CSV 資料檔設定";
$Lang['eNotice']['MergeCSVFormat1'] = "根據班別及班號";
$Lang['eNotice']['MergeCSVFormat2'] = "根據內聯網帳號";
$Lang['eNotice']['ProblemNoCSV'] = "沒有設定 CSV 資料檔！";
$Lang['eNotice']['MergeCSVRemarks1'] = "可於班別及班號/內聯網帳號後新增欄目，為學生增加可用合併資料種類";
$Lang['eNotice']['MergeCSVRemarks2'] = "可於新增行列中設定新種類(TYPE)，為學生增加可用合併種類。<br />* 可於新增行列中為相同合併種類設定新內容(CONTENT)，以增加顯示內容於現有合併資料種類內。";
$Lang['eNotice']['PendingApproval'] = "待審批";
$Lang['eNotice']['NeedApproval'] = "需要批核";
$Lang['eNotice']['ApprovalUser'] = "批核用戶";
$Lang['eNotice']['AdminGroupHaveApprovalRight']="(只有批核用戶有批核權限)";
$Lang['eNotice']['Reject']='不批核';
$Lang['eNotice']['ConfirmReject']='你確認拒絕所有已選擇的通告?';
$Lang['eNotice']['MoveQuestion'] = "排列問題";
$Lang['eNotice']['ActionAfterSelect'] = "選擇問題選項後";
$Lang['eNotice']['ActionAfterAnswer'] = "回答問題後";
$Lang['eNotice']['SkipToQuestion'] = "前往問題 ";
$Lang['eNotice']['SkipToAllQuestion'] = "前往回條結尾";
$Lang['eNotice']['PleaseFillOptionNumber'] = "請輸入選項數目!";
$Lang['eNotice']['NoNeedToAnswerThisQuestion'] = "不需回答這條題目";
$Lang['eNotice']['PleaseCompleteThisQuestion'] = "請完成此問題。";
$Lang['eNotice']['PleaseCompleteAllQuestions'] = "請完成所有問題。";
$Lang['eNotice']['PleaseAnswerThisQuestion'] = "請回答此問題。";
$Lang['eNotice']['PleaseAnswerAllQuestions'] = "請回答所有問題。";
$Lang['eNotice']['PleaseInputValidQuantity'] = "請輸入正確的數量。";
$Lang['eNotice']['ApplySubsidySettings'] = "套用資助設定";
$Lang['eNotice']['SetSubsidyByIdentity'] = "以資助身份設定";
$Lang['eNotice']['SetSubsidyByIndividualStudent'] = "以個別學生設定";
$Lang['eNotice']['PleaseCompleteThisSubsidySettings'] = "請完成此資助設定。";
$Lang['eNotice']['PreviewWithSubsidySettings'] = "套用資助設定預覽";
$Lang['eNotice']['PreviewByIdentity'] = "以資助身份預覽";
$Lang['eNotice']['PreviewByStudent'] = "以個別學生預覽";
$Lang['eNotice']['AmountDescription'] = "描述";
$Lang['eNotice']['PaymentNoticePayAtSchool'] = '到校繳費';
$Lang['eNotice']['PaymentNoticePaymentMethod'] = "繳款方式";
$Lang['eNotice']['PaymentNoticeUnpaid'] = '未繳費';
$Lang['eNotice']['ModifyReplySlipAnswer'] = "修改回條答案";
$Lang['eNotice']['AnswerModifiedByAt'] = "<!--USER_NAME--> 於 <!--DATETIME--> 修改了回條答案。";
$Lang['eNotice']['LastModifyAnswerPerson'] = "最後修改答案用戶";
$Lang['eNotice']['LastModifyAnswerTime'] = "最後修改答案時間";
$Lang['eNotice']['SureToApproveAll'] = '確定要批核全部通告？';
$Lang['eNotice']['ChangeApprovalStatus'] = '更改審批狀態';
$Lang['eNotice']['AppNotiy']['Instant'] = '即時 (發出日期)';
$Lang['eNotice']['Warning']['EmptyApprovalStatus'] = '請選擇審批狀態';
$Lang['eNotice']['Warning']['ConfirmChangingStatus'] = '你確定更改審批狀態？';
$Lang['eNotice']['TeacherCanSeeAllNotice'] = '所有教職員可檢視所有通告內容';
$Lang['eNotice']['AccessGroupCanSeeAllNotice'] = '所有管理小組成員可檢視所有回條內容';
$Lang['eNotice']['WrongUserLogin'] = '沒有此用戶或已選取';
$Lang['eNotice']['PaymentNotice_ReminderAfterEditMustSubmit'] = '勾選或取消勾選選項後，需要於設定回條內容介面內再按儲存。以更新 "題目必須回答" 設定';
$Lang['eNotice']['SysProperties']['General'] = "一般";
$Lang['eNotice']["settings"]['notification']['yes'] = "是";
$Lang['eNotice']["settings"]['notification']['no'] = "否";
$Lang['eNotice']["settings"]['notification']['push_message_overdue'] = "到期日推播提示(eClass App)";
$Lang['eNotice']["settings"]['notification']['push_message_due_date'] = "到期日前推播提示(eClass App)";
$Lang['eNotice']["settings"]['notification']['push_message_due_date_reminder'] = "到期[ ]日前推播提示 (eClass App)";
$Lang['eNotice']["settings"]['notification']['push_message_time'] = "推播提示時間 (eClass App)";
$Lang['eNotice']["settings"]['system']['notification_input_non_negative_integer'] = "請輸入非負整數";
$Lang['eNotice']['daily']['push_message']['return_date']['title']="電子通告到期提示通知 eNotice(s) due date reminder";
$Lang['eNotice']['daily']['push_message']['return_date']['content']="貴子弟所需簽署電子通告快將到期，請按期簽署。如你已簽署有關通告，請無需理會此通知。
    
The following eNotice(s) which you have not signed will be due for return soon. Please signed it/them on time. Please ignore this notice if you have already signed.";
$Lang['eNotice']['daily']['push_message']['return_date']['Studentcontent']="您所需簽署電子通告快將到期，請按期簽署。如你已簽署有關通告，請無需理會此通知。
    
The following eNotice(s) which you have not signed will be due for return soon. Please signed it/them on time. Please ignore this notice if you have already signed.";
$Lang['eNotice']['daily']['push_message']['overdue']['title']="電子通告到期通知 eNotice(s) due date reminder";
$Lang['eNotice']['daily']['push_message']['overdue']['content']="貴子弟所需簽署電子通告已到期，請今天內簽署。如你已簽署有關通告，請無需理會此通知。
    
The following eNotice(s)  which you have not signed will be due today. Please sign it/them today. Please ignore this notice if you have already signed.";
$Lang['eNotice']['daily']['push_message']['overdue']['Studentcontent'] = "您所需簽署電子通告已到期，請今天內簽署。如你已簽署有關通告，請無需理會此通知。

The following eNotice(s)  which you have not signed will be due today. Please sign it/them today. Please ignore this notice if you have already signed.";

$Lang['eNotice']['NoticeTitle'] = "通告名稱";

$Lang['eNotice']['SignedNotices'] = '己簽通告';
$Lang['eNotice']['NotyetSignedNotices'] = '未簽通告';

### eSports
if($plugin['Sports']||$plugin['swimming_gala'])
{
	$Lang['Sports']['AdminType'] = "管理員類別";
	$Lang['Sports']['Admin'] = "管理員";
	$Lang['Sports']['Helper'] = "助手";
	$Lang['Sports']['StaffName'] = "職員/老師姓名";
	$Lang['Sports']['HelperName'] = "學生/職員/老師姓名";
	$Lang['Sports']['AdminSettings'] = "管理設定";
	$Lang['Sports']['AdminLevel_Helper_Detail'] = "";
	$Lang['Sports']['AdminLevel_Admin_Detail'] = "";
	$i_Sports_menu_Admin_Settings = "";
	$Lang['Sports']['Report']['AllArrangement'] = "運動員人數統計表";
	$Lang['Sports']['Report']['GameArrangement'] = "項目人（隊）數統計表";
	$Lang['Sports']['Report']['Classes'] = "班級";
	$Lang['Sports']['Report']['ClassTotal'] = "合計";
	$Lang['Sports']['Report']['AgeGroupTotal'] = "總數";
	$Lang['Sports']['Report']['Boy'] = "男";
	$Lang['Sports']['Report']['Girl'] = "女";
	$Lang['Sports']['Report']['Events'] = "項目";

	$Lang["eSports"]["Class"] = "班別";
	$Lang["eSports"]["Form"] = "年級";
	$Lang['eSports']['csv']['ClassTitle'] = "班別";
	$Lang['eSports']['csv']['ClassNumber'] = "學號";
	$Lang['eSports']['csv']['EventCode'] = "項目編號";

	$Lang['eSports']['ErrorLog'] = "錯誤紀錄";
	$Lang['eSports']['warnDeleteEnrolRecord'] = "所有目標班別現存的報名紀錄會被刪除，繼續？";
	$Lang['eSports']['warnNoRecordInserted'] = "資料未被儲存，請更正錯誤後再試";
	$Lang['eSports']['Error'] = "錯誤";
	$Lang['eSports']['DuplicateRecord'] = "項目重複";
	$Lang["eSports"]["WrongAgeGroup"] = "此學生不屬於此項目所屬的年齡組別";
	$Lang['eSports']['ExceedTotalQuota'] = "超出報名限額(總數)";
	$Lang['eSports']['ExceedQuota'][1] = "超出報名限額(徑賽)";
	$Lang['eSports']['ExceedQuota'][2] = "超出報名限額(田賽)";
	$Lang['eSports']['StudentNotExist'] = "沒有此學生";
	$Lang['eSports']['NotInTargetClasses'] = "此學生不屬於目標班別 / 級別";
	$Lang['eSports']['NoError'] = "沒有錯誤";
	$Lang['eSports']['ImportStatus'] = "匯入狀況";

	$Lang['eSports']['EventCode'] = "項目編號";
	$Lang['SwimmingGala']['ExceedQuota'] = "超出報名限額";
	$Lang['eSports']['warnSelectClass'] = "請選擇最少一個級別 / 班別";
	$Lang['eSports']['warnSelectcsvFile'] = "請選擇匯入的檔案";
	$Lang["eSports"]["NoSuchEvent"] = "沒有此項目";
	$Lang['eSports']['warnNoEventCode'] = "請輸入項目編號";

	$Lang['eSports']['FinalRoundSettingWarning'] = "參賽者數目如不超過每組人數數目，賽事將直入決賽。";
	$Lang['eSports']['AutoArrangeWarning'] = "你是否確定要繼續自動安排賽程? 現時的賽程安排將被複寫，而且不可恢復。";
	$i_Sports_Rank_Pattern = "名次排列";
	$i_Sports_New_Record_Delete = "<font color=green>新紀錄已刪除</font>";

	$Lang['eSports']['IndividualEvent'] = "個人項目";
	$Lang['eSports']['Individual'] = "個人";

	$Lang['eSports']['ExportParticipateCertificate'] = "匯出出席証書";

	$Lang['eSports']['DeductScore'] = "扣分";
	$Lang['eSports']['Waive'] = "豁免";
	$Lang['eSports']['InputWaiveReason'] = "請填上缺席原因。";
	$Lang['eSports']['SpecialArrangeForSecondRound'] = "設定複賽的線道";
	$Lang['eSports']['Trial'] = "次";
	$Lang['eSports']['BestTrial'] = "最佳成績";
	$Lang['eSports']['Round1BestTrial'] = "初賽最佳成績";
	$Lang['eSports']['AllGirlsGroup'] = "全部女子組";
	$Lang['eSports']['AllBoysGroup'] = "全部男子組";
	$Lang['eSports']['Athlete'] = "運動員";
	$Lang['eSports']['ManualLaneArrangement'] = "自行安排線道";
	$Lang['eSports']['Ranking'] = "排名";
	$Lang['eSports']['Event'] = "賽事";
	$Lang['eSports']['Result'] = "成績";
	$Lang['eSports']['ExportTotalScore'] = "匯出總分";
	$Lang['eSports']['ExportTotalScoreWithEventDetails'] = "匯出總分及賽事詳細資料";
	$Lang['eSports']['EnrolScore'] = "報名分";
	$Lang['eSports']['AttendanceScore'] = "出席分";
	$Lang['eSports']['RankScore'] = "名次分";
	$Lang['eSports']['RecordBrokenScore'] = "破紀錄分";
	$Lang['eSports']['QualifiedScore'] = "達到標準分";
	$Lang['eSports']['TotalScore'] = "總分";

	$Lang['eSports']['RoundResult'][1] = "初賽成績";
	$Lang['eSports']['RoundResult'][2] = "複賽成績";
	$Lang['eSports']['RoundTitle'][1] = "初賽";
	$Lang['eSports']['RoundTitle'][2] = "複賽";
	$Lang['eSports']['RoundTitle'][0] = "決賽";

	$Lang['eSports']['HighJumpEvent'] = "跳高項目";
	$Lang['eSports']['BestTrialIncludeRound1'] = "最佳成績 (以初賽及決賽成績計算)";

	$Lang['eSports']['WarningArr']['PleaseSelectRound'] = "請選擇賽事";
	$Lang['eSports']['WarningArr']['PleaseUpdateFinalResult'] = "請更新決賽成績";

	$Lang['eSports']['ExportPreviewFormat'] = "預覽格式";

	$Lang['eSports']['RelayTeamNumber'] = array();
	$Lang['eSports']['RelayTeamNumber'][1] = "一隊";
	$Lang['eSports']['RelayTeamNumber'][2] = "二隊";
	$Lang['eSports']['RelayTeamNumber'][3] = "三隊";
	$Lang['eSports']['RelayTeamNumber'][4] = "四隊";
	$Lang['eSports']['RelayTeamNumber'][5] = "五隊";
	$Lang['eSports']['RelayTeamNumber'][6] = "六隊";
	$Lang['eSports']['RelayTeamNumber'][7] = "七隊";
	$Lang['eSports']['RelayTeamNumber'][8] = "八隊";
}
$Lang['eSports']['per'] = "項";
$Lang['eSports']['events'] = "項目";
$Lang['eSports']['NoOfStudentsEnrolled'] = "報名人數";
$Lang['eSports']['FontSettings'] = "字型設定";
$Lang['eSports']['AthleteInformation'] = "運動員資料";
$Lang['eSports']['AthleteNumber'] = "運動員號碼";
$Lang['eSports']['EnroledEvent'] = "參與項目";
$Lang['eSports']['ExportStudentDetails'] = "匯出下表(連同學生名單)";
$Lang['eSports']['EventQuota'] = "設定報名人數上限";
$Lang['eSports']['Quota'] = "上限";
$Lang['eSports']['Quota_jsAlert'] = "上限數目必需為整數及不能少於 1。";
$Lang['eSports']['QuotaFull'] = "名額已滿";
$Lang['eSports']['IndividualEnrolmentQuota'] = "計算個人報名限額?";
$Lang['eSports']['Enrolled'] = "已報名";
$Lang['eSports']['Quota_jsAlert_LessThanEnrolled'] = "名額不能少過已報名人數。";
//$Lang['eSports']['ExportForElectronicBoard'] = "滙出至電子板格式";
$Lang['eSports']['ExportForElectronicBoard'] = "匯出至電子板格式";
$Lang['eSports']['EnrolmentRestriction'] = "限制學生報名名單";
$Lang['eSports']['NotAllowForEnrolment'] = "不允許報名, 原因: ";
$Lang['eSports']['ExportAthleteResultInDetail'] = "匯出所有賽事成績細項";
$Lang['eSports']['ClassRelayEnrol'] = "班際接力報名";
$Lang['eSports']['RelayEventType'] = "賽事種類";
$Lang['eSports']['RelayRoundType'] = "賽事階段";
$Lang['eSports']['AthleteNumberOnly'] = "只顯示運動員編號";
$Lang['eSports']['ClassRelayTeamNumber'] = "班隊數目";
$Lang['eSports']['ClassRelayArrangeRemarks'] = "(參賽班隊數目如不超過決賽班隊數目，賽事將直入決賽)";
$Lang['eSports']['SelectStudent'] = "選擇學生";
$Lang['eSports']['SelectedStudent'] = "已選擇學生";
$Lang['eSports']['DisplaySettings'] = "顯示設定";
$Lang['eSports']['Lane'] = "線道";
$Lang['eSports']['Order'] = "順序";
$Lang['eSports']['IndividualEventRanking'] = '個人賽項排名';

# MKSS

$Lang['eSports']['Report']['AverageScore'] = "平均分數";
$Lang['eSports']['Settings']['EventGroupSchedule'] = "賽事日程";
$Lang['eSports']['Settings']['EventRound'] = "賽事階段";
$Lang['eSports']['Settings']['EventScheduleCode'] = "場號";
$Lang['eSports']['Settings']['EventTime'] = "賽事時間";
$Lang['eSports']['Settings']['EventGroupScheduleReset'] = "重置賽事日程";
$Lang['eSports']['Participation']['AutoStatusReminder'] = "如未曾輸入比賽成績，系統將以今天學生考勤管理系統資料來預設出席狀態。";

### Calendar
$Lang['Calendar']['PrevMonth'] = "上一月";
$Lang['Calendar']['NextMonth'] = "下一月";


### School Setting -> Group
$Lang['Group']['SetAdmin'] = "設定為小組管理員";
$Lang['Group']['CancelAdmin'] = "設定為一般組員";
$Lang['Group']['SetAsMember'] = "設定為一般組員";
$Lang['Group']['Update'] = "更新";
$Lang['Group']['NewRole'] = "新增組別角色";
$Lang['Group']['GroupMgmt'] = "小組";
$Lang['Group']['GroupMgmtCtr'] = "小組";
$Lang['Group']['NoOfMember'] = "組員數量";
$Lang['Group']['Role']['PresetValue'] = "設定為預設";
$Lang['Group']['RoleSetting'] = "小組角色";

$Lang['Group']['GroupCatSetting'] = "小組類別";
//$Lang['Group']['WarnHasGroup'] = "方框為黃色的小組類別已設有小組，不能刪除。";
$Lang['Group']['WarnHasGroup'] = "此小組類別已設有小組，不能刪除。";
$Lang['Group']['DefaultGroup'] = "預設小組類別";

$Lang['Group']['GroupName'] = "名稱";
$Lang['Group']['GroupDescription'] = "描述";
$Lang['Group']['Category'] = "類別";
$Lang['Group']['GroupCategory'] = "小組類別";
$Lang['Group']['Import']['CheckCategory'] ='按此查詢小組類別名稱';
$Lang['Group']['Type'] = "類型";
$Lang['Group']['StorageQuota'] = "存儲配額";
$Lang['Group']['CanUsePublicAnnouncementEvents'] = "可發放公眾宣布、事項通知";
$Lang['Group']['AllowUsingAllGroupTools'] = "允許使用所有小組工具";
$Lang['Group']['AllowUsingGroupTools'] = "只允許使用勾選的小組工具";
$Lang['Group']['SendEmail'] = "發送電郵";
$Lang['Group']['GroupAdmin'] = "小組管理員";
$Lang['Group']['SubjectLeader'] = "科長";
$Lang['Group']['QuickAssignAs'] = "立即設定為";
$Lang['Group']['UserList'] = "用戶名單";
$Lang['Group']['SelectGroupCategory'] = "選擇小組類別";
$Lang['Group']['Recipient'] = "收件者";
$Lang['Group']['Subject'] = "主題";
$Lang['Group']['Comment'] = "內容";
$Lang['Group']['DefaultGroupRole'] = "預設小組角色";
$Lang['Group']['ViewThisCategoryOfGroupOnly'] = "只檢視此類別下之小組：";

$Lang['Group']['jsWarning']['SelectGroupCategory'] = "請選擇一小組類別。";
$Lang['Group']['warnEmptyGroupName'] = '沒有小組名稱';
$Lang['Group']['warnEngNameDuplicate'] = '已有相同名稱（英文）的小組存在';
$Lang['Group']['warnChiNameDuplicate'] = '已有相同名稱（中文）的小組存在';
$Lang['Group']['warnPositiveNum'] = '存儲配額必須為正整數';
$Lang['Group']['warnGroupCatNotExist'] = '沒有此小組分類';

$Lang['Group']['GroupMember'] = "組員";
$Lang['Group']['StudentNotExist'] = "沒有此學生";

$Lang['Group']['EmptyGroupName'] = "沒有輸入組別";
$Lang['Group']['GroupNotExist'] = "沒有此組別";
$Lang['Group']['GroupNotInCurrentYear'] = "組別不屬於本年度";

$Lang['Group']['UserNotExist'] = "沒有此用戶";
$Lang['Group']['EmptyUserInfo'] = "沒有用戶資料";
$Lang['Group']['IsIdentityGroup'] = "不能加入組員到此組別";
$Lang['Group']['IsECAGroup'] = "學會會員必須於課外活動管理中匯入";
$Lang['Group']['UsePublicAnnonuceOrEventIsEmpty'] ='請設定是否發放允許公眾宣布、事項通知';
$Lang['Group']['UsePublicAnnonuceOrEventIsInvalid'] ='允許公眾宣布、事項通知設定錯誤';
$Lang['Group']['DisplayineCommunIsNull'] = '請設定是否在社群管理工具顯示';
$Lang['Group']['DisplayineCommunIsInValid'] = '社群管理工具顯示設定錯誤';
$Lang['Group']['AllowAdminEditOrDeleteIsNull'] = '請設定是否允許小組管理員可互相刪除/編輯宣佈';
$Lang['Group']['AllowAdminEditOrDeleteIsInValid'] = '小組管理員權限設定錯誤';

$Lang['Group']['RolesAll'] = '所有小組成員';
$Lang['Group']['RolesAdmin'] = '小組管理員';
$Lang['Group']['RolesNormal'] = '一般組員';

$Lang['Group']['Options']= '選項';
$Lang['Group']['CopyResult'] = '複製結果';
$Lang['Group']['Confirmation'] = '確認';
$Lang['Group']['CopyFrom']= '由學年';
$Lang['Group']['CopyTo']= '至學年';
$Lang['Group']['Group']= '組別';
$Lang['Group']['CopyGroup']= '複製組別';
$Lang['Group']['CopyMember']= '複製組員';
$Lang['Group']['Member'] = '組員';
$Lang['Group']['CopyFromOtherYear'] = '從其他年度複製';
$Lang['Group']['Copy'] = '複製';
$Lang['Group']['warnSelectGroup'] = '請選擇最少一個組別';
$Lang['Group']['warnSelectDifferentYear'] = '請選擇不同的年度';
$Lang['Group']['GroupExistInTargetYear'] = '目標年度已有此組別';
$Lang['Group']['GroupCopiedSuccessfully'] = '個組別複製成功';
$Lang['Group']['AddUserInstrution'] = "在學生新增到小組之前，該學生必須已被分配到相同學年的班別中，否則，新增將會失敗。";
$Lang['Group']['AddUserInstrution2'] = "請在匯入檔案中輸入小組英文名稱，系統會以小組英文名稱作資料配對。";
$Lang['Group']['FailToAddSomeUser'] = "一些用戶未能加到小組內。";
$Lang['Group']['UserNotInSelectedSchoolYear'] = "此學生尚未加入到所選的學年";
$Lang['Group']['NotInAcademicYearRemarks'] = "此學生尚未加入到該學年的班別";

$Lang['Group']['Note'] = "註";
$Lang['Group']['AdminRightArr']['MemberManagement'] = "管理成員";
$Lang['Group']['AdminRightArr']['MemberManagement_eEnrolRemarks'] = "如已經安裝eEnrolment課外活動管理系統，請使用eEnrolment管理課外活動小組成員。";

$Lang['Group']['ReturnMessage']['CannotDeleteClassGroup'] = '0|=|一些小組與班別連結，不能刪除。';

$Lang['Group']['NameEn'] = "名稱 (英文)";
$Lang['Group']['NameCh'] = "名稱 (中文)";
$Lang['General']['ReturnMessage']['GroupDuplicated'] = '0|=|已有此組別';
$Lang['Group']['warnEmptyGroupNameEn'] = '沒有小組名稱(英文)';
$Lang['Group']['warnEmptyGroupNameCh'] = '沒有小組名稱(中文)';
$Lang['General']['ReturnMessage']['CannotDeleteClassGroup'] = '0|=|一些小組與班別連結，不能刪除。';
$Lang['Group']['jsWarningMsgArr']['deleteRole'] = '如你刪除小組角色，於小組成員資料中，預設小組角色將取代已刪除的小組角色。你是否確定要刪除小組角色？';
$Lang['Group']['jsWarningMsgArr']['editSingleGroup'] = '請選擇多於一個小組';
$Lang['Group']['jsWarningMsgArr']['diffGroupSelected'] = '請選擇同類別的小姐';

### eComm
$Lang['Group']['WarnAddGroup'] = "請選擇最少一個組別";
$Lang['Group']['WarnStratDayPassed'] = "開始日期必須是今日或以後";
$Lang['Group']['MissingFile'] = "在下列連結找不到檔案";

$Lang['Group']['NotEnoughSpace'] = "以下小組沒有足夠儲存空間: ";
$Lang['Group']['Role']['SetPresetRole'] = "是否為預設小組角色";

$Lang['eComm']['NoPublicAnnounceRightEdit'] = "你的組別沒有編輯公眾宣佈的權限";
$Lang['eComm']['NoPublicAnnounceRightDelete'] = "你的組別沒有刪除公眾宣佈的權限";
$Lang['eComm']['NotSendToFullGroup'] = "新主題將不會呈送到沒有足夠儲存空間的小組，繼續?";
$Lang['eComm']['DeleteInternalOnly'] = "只刪除了你的社群內的宣佈";
$Lang['eComm']['PleaseInsertTitle'] = "請輸入標題";
$i_con_msg_GroupLogo_PhotoWarning = "群組圖示只接受'.JPG'、'.GIF'或'.PNG'等格式。";

$eComm['Allow_Att'] = "允許加入附件?";
$eComm['Allow_Forum'] = "允許使用討論區";
$eComm['ForumDisabled'] = "討論區不允許使用";
$eComm['Allow_Reply'] = "允許回覆";

$Lang['eComm']['BatchUploadRemark'] = "可以以壓縮檔案(.zip)形式上載多個檔案";

$Lang['eComm']['WaitingForApproval'] = "等候批核";
$Lang['eComm']['ManualInput'] = "手動輸入";
$Lang['eComm']['AllowUsingWebsite'] = "接受使用網頁";

$Lang['eComm']['PhotoSize'] = "相片大小";
$Lang['eComm']['Origanal'] = "原本大小";
$Lang['eComm']['Resize'] = "縮小";
$Lang['eComm']['KeepAspectRatio'] = "保持比例";
$Lang['eComm']['Width'] = "闊度";
$Lang['eComm']['Height'] = "高度";
$Lang['eComm']['DemensionErr'] = "必須為正整數";
$Lang['eComm']['AnnouncementNum'] = "首頁顯示的小組宣佈數量";
$Lang['eComm']['Manage'] = "管理";
$Lang['eComm']['BackToHome'] = "返回首頁";
$Lang['eComm']['Waiting_Reject_Remark'] = "你已上載的文件。(被批核的不在此列)";

$eComm['PublicStatus'] = "公開狀態";

### eEnrolment
if ($plugin['eEnrollment'])
{
	$Lang['eEnrolment']['Club'] = '學會';
	$Lang['eEnrolment']['Activity'] = '活動';
	$Lang['eEnrolment']['Role'] = '職位';
	$Lang['eEnrolment']['ClubName'] = '學會名稱';
	$Lang['eEnrolment']['ClubNameEn'] = '學會名稱 (英文)';
	$Lang['eEnrolment']['ClubNameCh'] = '學會名稱 (中文)';
	$Lang['eEnrolment']['AllCategories'] = '所有類別';
	$Lang['eEnrolment']['SelectClub'] = '選擇學會';
	$Lang['eEnrolment']['Children'] = "子女";
	$Lang['eEnrolment']['LastGeneratedDate'] = "最後產生日期";
	$Lang['eEnrolment']['Location'] = "地點";
	$Lang['eEnrolment']['MemberList'] = "會員名單";
	$Lang['eEnrolment']['ActiveStatus'] = '活躍狀態';
	$Lang['eEnrolment']['InactiveMemberOnly'] = "只限非活躍會員";

	$Lang['eEnrolment']['MenuTitle']['Settings']['AdminSettings'] = "管理權限設定";

	$Lang['eEnrolment']['UserRole']['EnrolmentAdmin'] = "學會報名管理員";
	$Lang['eEnrolment']['UserRole']['EnrolmentMaster'] = "學會報名主任";
	$Lang['eEnrolment']['UserRole']['NormalUser'] = "一般使用者";

	$Lang['eEnrolment']['Warning']['AgeRangeInvalid'] = "對象年齡範圍不正確";
	$Lang['eEnrolment']['Warning']['ClubHasNoMember'] = "此學會沒有會員。";
	$Lang['eEnrolment']['Warning']['ActivityHasNoParticipant'] = "此活動沒有參加者。";
	$Lang['eEnrolment']['Warning']['DateRangeInvalid'] = "開始日期必須為結束日期之前。";
	$Lang['eEnrolment']['Warning']['EndTimeOutOfRange'] = "結束時間超出範圍。";
	$Lang['eEnrolment']['Warning']['StartTimeOutOfRange'] = "開始時間超出範圍。";
	$Lang['eEnrolment']['Warning']['TimeRangeInvalid'] = "開始時間必須為結束時間之前。";
	$Lang['eEnrolment']['Warning']['SelectWeekday'] = "請至少選擇星期中的一日。";
	$Lang['eEnrolment']['Warning']['TimeOverlap'] = "活動時間不可重疊。";
	$Lang['eEnrolment']['Warning']['StartDateInvalid'] = "開始日期必須遲於 <!--targetDate-->。";
	$Lang['eEnrolment']['Warning']['EndDateInvalid'] = "結束日期必須早於 <!--targetDate-->。";
	$Lang['eEnrolment']['Warning']['EnglishNameAndClubTypeDuplicated'] = "英文學會名稱及學期不能重複";
	$Lang['eEnrolment']['Warning']['ClubNameSameAsExistingRecord'] = "學會名稱與現有紀錄相同。";
	$Lang['eEnrolment']['Warning']['ActivityNameDuplicated'] = "活動名稱不能重複";
	$Lang['eEnrolment']['Warning']['SelectCategory'] = "請至少選擇一個類別。";

	$Lang['eEnrolment']['eBooking']['BookingTime'] = '預訂時段';
	$Lang['eEnrolment']['eBooking']['DeleBookingRecord'] = "已選取的記錄將被刪除, 繼續?";
	$Lang['eEnrolment']['eBooking']['ClickThickBox'] = "按此進行預訂(電子資源預訂系統)";
	$Lang['eEnrolment']['eBooking']['eBooking'] = "電子資源預訂系統";
	$Lang['eEnrolment']['eBooking']['eBookingRecordDetail'] = "電子資源預訂詳情";
	$Lang['eEnrolment']['eBooking']['Time'] = '活動時段';
	$Lang['eEnrolment']['eBooking']['TimeChangedError'] = "由於活動時段或日期更改了,請按左邊連結進行預訂更改";
	$Lang['eEnrolment']['eBooking']['Location'] = '地點';
	$Lang['eEnrolment']['eBooking']['NoAvailableRoom'] = '沒有可預訂地點';
	$Lang['eEnrolment']['eBooking']['Warning']['SelectLocation'] = "請選擇地點以套用到全部";

	$Lang['eEnrolment']['StaffName'] = "職員/老師姓名";
	$Lang['eEnrolment']['AdminLevel'] = "管理權限";
	$Lang['eEnrolment']['AdminSettings'] = "管理權限設定";
	$Lang['eEnrolment']['WholeYear'] = "全年";
	$Lang['eEnrolment']['WholeYearClub'] = "全年性學會";
	$Lang['eEnrolment']['EnterClubName'] = "輸入學會名稱";
	$Lang['eEnrolment']['EnterClubName_EN'] = "Enter Club Name";
	$Lang['eEnrolment']['EnterClubName_B5'] = "輸入學會名稱";
	$Lang['eEnrolment']['EnterActivityName'] = "輸入活動名稱";
	$Lang['eEnrolment']['ActivityManagement'] = "活動管理";

	$Lang['eEnrolment']['SendEmail'] = "傳送電郵";
	$Lang['eEnrolment']['SendEmailAtOnce'] = "在審批後即時寄電郵予學生及家長";
	$Lang['eEnrolment']['SendEmailForEnrollmentResult'] = "對已登記人士發出電郵";
	$Lang['eEnrolment']['ExportClubApplicantList'] = "報名情況";
	$Lang['eEnrolment']['ExportStudentMaxWanted'] = "學生希望參加的學會數目";

	$Lang['eEnrolment']['jsWarning']['DuplicatedPersonInPicAndHelper'] = "同一人不能同時為活動負責人及活動點名助手。";
	$Lang['eEnrolment']['jsWarning']['StudentCannotRemoveTeacher'] = "你沒有權限移除有關老師。";
	$Lang['eEnrolment']['jsWarning']['RemoveMyselfWarning'] = "如你從選項中移除自己，你將失去此學會或活動的權限。你確定要從選項中移除自己？";
	$Lang['eEnrolment']['jsWarning']['SelectClass'] = "請至少選擇一個班別。";
	$Lang['eEnrolment']['jsWarning']['SelectClub'] = "請至少選擇一個學會。";
	$Lang['eEnrolment']['jsWarning']['CannotApplySameClub'] = "不可申請相同的學會。";
	$Lang['eEnrolment']['jsWarning']['DeleteMeetingDate'] = "如你刪除活動時間，當你儲存此版面的資料時，已刪除的活動時間之出席記錄將會被刪除。你是否確定要刪除活動時間？";
	$Lang['eEnrolment']['jsWarning']['CancelActivityApplication'] = "如你取消申請此活動，「報名者資料」將會重置為該活動的預設資料。你是否確定要取消活動申請？";
	$Lang['eEnrolment']['jsWarning']['AnnounceResultTimeInvalid'] = "公佈報名結果時間不能早於報名時段結束時間。";
	$Lang['eEnrolment']['jsWarning']['NoClubTypeIsSelected'] = "沒有選擇學會類型. 請選擇";
	$Lang['eEnrolment']['jsWarning']['EnrollMinWrong'] = "可申請學會數目下限不能小於可參加學會數目下限";

	$Lang['eEnrolment']['EmailSample'] = "電郵範本";
	$Lang['eEnrolment']['ClubEmailSampleArr'][0] = '學生 __STUDENT_NAME__ 的報名結果如下:';
	$Lang['eEnrolment']['ClubEmailSampleArr'][1] = '1. Club 1 - 已批核';
	$Lang['eEnrolment']['ClubEmailSampleArr'][2] = '2. Club 2 - 已批核';
	$Lang['eEnrolment']['ClubEmailSampleArr'][3] = '3. Club 3 - 已拒絕';
	$Lang['eEnrolment']['ActivityEmailSampleArr'][0] = '學生 __STUDENT_NAME__ 的報名結果如下:';
	$Lang['eEnrolment']['ActivityEmailSampleArr'][1] = '1. Activity 1 - 已批核';
	$Lang['eEnrolment']['ActivityEmailSampleArr'][2] = '2. Activity 2 - 已批核';
	$Lang['eEnrolment']['ActivityEmailSampleArr'][3] = '3. Activity 3 - 已拒絕';

	$Lang['eEnrolment']['SendEmailWithParent'] = '同時寄給家長';
	$Lang['eEnrolment']['AllowClubPICCrateNAActviity'] = '允許學會負責人建立非學會活動';

	$Lang['eEnrolment']['DisableCheckingOfNumberOfClubStudentWantToJoin'] = '批核學生時不檢查學生參加學會數目的意願';
	$Lang['eEnrolment']['DisableCheckingOfNumberOfActivityStudentWantToJoin'] = '批核學生時不檢查學生參加活動數目的意願';
	$Lang['eEnrolment']['ImportFailRemarks'] = "紅色字代表匯入失敗";
	$Lang['eEnrolment']['TimeofArrival'] = "到逹時間";
	$Lang['eEnrolment']['TimeofDeparture'] = "離開時間";
	$Lang['eEnrolment']['heungto_PerformanceMark'] = "表現分數";

	$Lang['eEnrolment']['Refresh_Performance']['Name'] = '重新計算表現等級 ';
	$Lang['eEnrolment']['Refresh_Performance']['Export'] = '考勤紀錄(連表現)';
	$Lang['eEnrolment']['Refresh_Performance']['Role'] = '請選擇要計算的學期。';
	$Lang['eEnrolment']['Refresh_Performance']['extraInfo'] = '重新計算將會覆寫所有表現等級，包括已微調的等級。';
	$Lang['eEnrolment']['Refresh_Performance']['AdjustedReason'] = '表現微調原因';

	$Lang['eEnrolment']['EnrollmentStatus']['Unhandled'] = '候批';
	$Lang['eEnrolment']['EnrollmentStatus']['Waiting'] = '候批';
	$Lang['eEnrolment']['EnrollmentStatus']['Rejected'] = '已拒絕';
	$Lang['eEnrolment']['EnrollmentStatus']['Approved'] = '已批核';
	#Added by Omas
	$Lang['eEnrolment']['AppliedClub'] = '已報名學會';
	$Lang['eEnrolment']['JoinedClub'] = '已參加學會';
	$Lang['eEnrolment']['StudentClubEnrolInfo'] = '學會報名資料';
	$Lang['eEnrolment']['MaxStudentWant'] = '學生希望參加的學會數目';
	$Lang['eEnrolment']['ClubCategory'] ='學會類別';
	$Lang['eEnrolment']['MeetingSchedule'] ='活動時段';
	$Lang['eEnrolment']['MinStudentApply'] = '學生可申請學會數目下限';
	$Lang['eEnrolment']['MaxStudentApply'] = '學生可申請學會數目上限';
	$Lang['eEnrolment']['MinStudentEnrol'] = '學生可參加學會數目下限';

	# Copy Clubs
	$Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'] = '學會管理';
	$Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClub'] = '複製學會';
	$Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClubFromOtherYear'] = '複製學會自其他學年';
	$Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClubFromOtherTerm'] = '複製學會自其他學期';
	$Lang['eEnrolment']['CopyClub']['StepArr'][0] = '選項';
	$Lang['eEnrolment']['CopyClub']['StepArr'][1] = '確定';
	$Lang['eEnrolment']['CopyClub']['StepArr'][2] = '複製結果';
	$Lang['eEnrolment']['CopyClub']['CopyFromOtherYearTerm'] = '從其他年度/學期複製';
	$Lang['eEnrolment']['CopyClub']['CopyWarning'][0] = '如執行複製學會，系統將自動刪除<b>所有</b>學會的報名紀錄。複製學會前，請先確保學會報名紀錄可被刪除。';
	$Lang['eEnrolment']['CopyClub']['CopyWarning'][1] = '- 請檢查已選擇的學會是否已複製到目標學年 / 學期，否則該學會於目標學年 / 學期的紀錄將會重複。';
	$Lang['eEnrolment']['CopyClub']['FromAcademicYear'] = '由學年';
	$Lang['eEnrolment']['CopyClub']['ToAcademicYear'] = '至學年';
	$Lang['eEnrolment']['CopyClub']['FromTerm'] = '由學期';
	$Lang['eEnrolment']['CopyClub']['ToTerm'] = '至學期';
	$Lang['eEnrolment']['CopyClub']['TermMapping'] = '對應學期';
	$Lang['eEnrolment']['CopyClub']['ClubSelection'] = '選擇學會';
	$Lang['eEnrolment']['CopyClub']['Club'] = '學會';
	$Lang['eEnrolment']['CopyClub']['Term'] = '學期';
	$Lang['eEnrolment']['CopyClub']['Copy'] = '複製';
	$Lang['eEnrolment']['CopyClub']['ClubMember'] = '複製會員';
	$Lang['eEnrolment']['CopyClub']['ClubActivity'] = '複製相關活動';
	$Lang['eEnrolment']['CopyClub']['jsWarningArr']['SelectAtLeastOneClub'] = '請選擇複製學會。';
	$Lang['eEnrolment']['CopyClub']['BackToClubManagement'] = '回到學會管理';
	$Lang['eEnrolment']['CopyClub']['ClubCopiedSuccessfully'] = '學會複製成功。';
	$Lang['eEnrolment']['CopyClub']['ClubTitleDuplicationRemarks'] = '學會名稱於 <!--YearName--> 重複。';
	$Lang['eEnrolment']['CopyClub']['ClubCopyToTheSameTermRemarks'] = '不能將來自同一小組的學會複製到同一學期。';
	$Lang['eEnrolment']['CopyClub']['ClubExistedInTermAlready'] = '<!--TermName-->已設有此學會。';

	$Lang['eEnrolment']['Btn']['ExportClubInfo'] = '匯出學會資料';
	$Lang['eEnrolment']['Btn']['ExportClubInfo2'] = '學會資料';
	$Lang['eEnrolment']['Btn']['ExportActivityInfo'] = '匯出活動資料';
	$Lang['eEnrolment']['Btn']['ExportActivityInfo2'] = '活動資料';
	$Lang['eEnrolment']['Btn']['ExportMember'] = '匯出成員';
	$Lang['eEnrolment']['Btn']['ExportMember2'] = '成員';
	$Lang['eEnrolment']['Btn']['ExportMemberPerformance'] = '成員 (連學期表現)';
	$Lang['eEnrolment']['Btn']['Select50Items'] = '選取50個項目';

	$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderSuccess'] = "1|=|排列類別成功";
	$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderFailed'] = "0|=|排列類別失敗";
	$Lang['eEnrolment']['Settings']['WarningArr']['CategoryLinkedToDataAlready'] = "已輸入的學會或活動資料包含將被刪除的類別，如刪除類別，學會或活動資料將受影響，而該類別及有關資料將不能回復。你是否確定你要刪除類別？";
	$Lang['eEnrolment']['Settings']['BudgetAmount'] = '財政預算($)';
	$Lang['eEnrolment']['Settings']['BudgetAmountEmpty'] = "財政預算只能輸入正數數字";
	$Lang['eEnrolment']['Settings']['NotCheckTimeCrash'] = '不檢查時間衝突';
	$Lang['eEnrolment']['Settings']['one_club_when_time_crash'] = '如出現時間衝突，只允許參加其中一個學會';
	$Lang['eEnrolment']['Settings']['ClearDataIsMoved'] = ' 此功能已移到「管理 > 資料處理 > 清除資料」';
	$Lang['eEnrolment']['Settings']['Role']['WebSAMSCode'] = 'WebSAMS 代號';
	
	$Lang['eEnrolment']['Process']['TargetForm'] = '本輪報名級別 :  ';
	$Lang['eEnrolment']['Process']['StudentChoose'] = '學生可報名學會數目 :  ';
	$Lang['eEnrolment']['Process']['StudentJoin'] = '學生可參加學會數目 : ';
	
	$Lang['eEnrolment']['SignUpWarnMsg'] = '現不開放任何班級報名';
	$Lang['eEnrolment']['SignUpTargetForm'] = '只限 [=TargetForm=] 學生進行學會報名';
	$Lang['eEnrolment']['ShowForm'] = '顯示級別';
	$Lang['eEnrolment']['ShowRoundForm'] = '本輪報名級別 ';
	$Lang['eEnrolment']['ShowAllForm'] = '全部級別 ';

	# From IP20
	$eEnrollment['participant_selection_and_drawing'] = "申請批核及抽籤";
	$i_ClubsEnrollment_NumOfSatisfied = "符合個人要求的學生";
	$eEnrollment['front']['wish_enroll_end'] = "&nbsp;個學會。";
	$eEnrollment['front']['wish_enroll_end_event'] = "&nbsp;個活動。";
	$eEnrollment['js_select_semester'] = "請選擇學期。";
	$eEnrollment['ClubType'] = "學會類型";
	$eEnrollment['YearBased'] = "全年性";
	$eEnrollment['SemesterBased'] = "學期性";
	$eEnrollment['WholeYear'] = "全年";
	$eEnrollment['MinimumMemberQuota'] = "會員人數下限";
	$eEnrollment['MinimumQuota'] = "人數下限";
	$eEnrollment['ToBeConfirmed'] = "待定";
	$eEnrollment['ChangeClubTypeWarning'] = "如你更改學會類型，學會會員及報名資料將被刪除。你是否確定更改學會類型？";
	$eEnrollment['enrol_same_club_once_only'] = "學生只可參加此學會一次";
	$eEnrollment['only_allow_student_to_join_the_club_in_the_first_semester'] = "學生只可於第一學期報名參加此學會";
	$i_ClubsEnrollment_WholeYear = "全年";
	$eEnrollment['semester_cannot_be_match'] = "學期與學會不相配";
	$eEnrollment['semester_not_found'] = "沒有此學期";
	$eEnrollment['target_enrolment_semester'] = "是次報名學期";
	$eEnrollment['selectTypeTransferToStudentProflie'] = "請選擇最少一個頂目";
	$eEnrollment['add_activity']['act_target'] = "對象級別";
	$eEnrollment['no_such_time_slot'] = "沒有此時段。";
	$eEnrollment['attendance_locked'] = "出度紀錄已上鎖。";
	$eEnrollment['target_gendar_not_match'] = "性別不正確";
	$eEnrollment['MinQuotaShouldBeSmallerThanQuota'] = "會員人數下限必須少於會員名額";
	$eEnrollment['NoOfClubMustBeApply'] = "必須要選擇學會數目";
	$eEnrollment['MinNoOfClubNeedToApply'] = "最少須要選擇學會數目";
	$eEnrollment['MaxNoOfClubWantToParticipate'] = "你希望最多參加的學會數目";
	$eEnrollment['member_perform_comment_list'] = "組員表現及老師評語";
	$eEnrollment['student_perform_comment_list'] = "學生表現及老師評語";
	$eEnrollmentMenu['data_handling_to_OLE'] = "資料處理 (轉移至學生學習概覽)";
	$Lang['eEnrolment']['data_handling']['export_websams'] = "資料匯出 (至網上校管系統)";
	$eEnrollment['js_ole_category_alert'] = "請選擇記錄類別";
	$eEnrollment['TotalStudent'] = "總參加人數";

	$Lang['eEnrolment']['Nature'] = "活動性質";
	$Lang['eEnrolment']['SchoolActivity'] = "校內";
	$Lang['eEnrolment']['NonSchoolActivity'] = "校外";
	$Lang['eEnrolment']['MixedActivity'] = "混合";

	$Lang["eEnrolment"]["ClubParticipationReport"]["ClubParticipationReport"] = "學會參與報告";
	$Lang['General']['ClubParticipationReport']['SelectAtLeastOneClub'] = "請選擇最少一個學會";
	$Lang['General']['ActivityParticipationReport']['SelectAtLeastOneActivity'] = "請選擇最少一個活動";

	$Lang["eEnrolment"]["ActivityParticipationReport"]["ActivityParticipationReport"] = "活動參與報告";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"] = "學年";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"] = "學期";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"] = "時段";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Target"] = "對象";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"] = "可按Ctrl鍵選擇多項";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["ActivityCategory"] = "活動類型";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Nature"] = "性質";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"] = "次數";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"] = "時數";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["DisplayUnit"] = "顯示單位";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["SortBy"] = "排序";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["All"] = "全部";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolActivity"] = "校內活動";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Non-schoolActivity"] = "校外活動";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrAbove"] = "或以上";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrBelow"] = "或以下";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Student"] = "學生";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Class"] = "班級";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"] = "班級名稱";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassNumber"] = "班級編號";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["AscendingOrder"] = "遞增";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["DescendingOrder"] = "遞減";

	$Lang["eEnrolment"]["ActivityParticipationReport"]["Name"] = "學生姓名";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["TimesFor"] = "次數";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalTimesFor"] = "總次數";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"] = "總時數";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] = "小時";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Total"] = "合共";

	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_Class"] = "班級";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_ClassNumber"] = "班級編號";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_StudentName"] = "學生姓名";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_TimesFor"] = "次數";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_TotalHoursFor"] = "總時數";

	$Lang["eEnrolment"]["ActivityParticipationReport"]["Number"] = "數目";

	$eEnrollmentMenu["ActivityParticipationReport"] = "活動參與報告";

	$Lang['eEnrolment']['Attendance']['Present'] = "出席";
	$Lang['eEnrolment']['Attendance']['Absent'] = "缺席";
	$Lang['eEnrolment']['Attendance']['Exempt'] = "豁免";
	$Lang['eEnrolment']['Attendance']['Late'] = "遲到";
	$Lang['eEnrolment']['Attendance']['EarlyLeave'] = "早退";
	$Lang['eEnrolment']['AttendanceDataInputRemarks'] = "出席資料輸入代號";
	$Lang['eEnrolment']['Code'] = "代號";
	$Lang['eEnrolment']['AttendanceStatus'] = "出席狀態";
	$Lang['eEnrolment']['ImportAttendanceRemarks'] = "如漏空出席狀態，原有的出席態度將被保留。";
	$Lang['eEnrolment']['AttendanceHour'] = "出席時數";
	$Lang["eEnrolment"]["DirectToSetting"] = "現在進行設定?";
	$Lang['eEnrolment']['DirectToDateSetting'] = "現在進行活動日期設定?";
	$Lang['eEnrolment']['DirectToMemberSetting'] = "現在進行學生名單設定?";
	$Lang["eEnrolment"]["ShowEntireList"] = "顯示所有日期";

	$Lang["eEnrolment"]["NoOfEmptyActPerformance"] = "未評估表現活動";
	$Lang["eEnrolment"]["NoOfEmptyClubPerformance"] = "未評估表現學會";

	$Lang['eEnrolment']['record_type'] = "紀錄類型";
	$Lang['eEnrolment']['show_data'] = "顯示資料";
	$Lang['eEnrolment']['attendance_percent'] = "出席率";
	$Lang['eEnrolment']['Club_Activity_Hours'] = "活動時數";
	$Lang['eEnrolment']['Club_Activity_Times'] = "活動次數";
	$Lang['eEnrolment']['select_club_activity'] = "請最少選擇一種紀錄類型。";
	$Lang['eEnrolment']['select_role_attendance_performance'] = "請最少選擇一項資料。";
	$Lang['eEnrolment']['bulk_print'] = "Bulk print individual enrolment records";
	$Lang['eEnrolment']['show_email'] = "電郵";

	$Lang['eEnrolment']['default_format_print'] = "列印簡易格式";
	$Lang['eEnrolment']['custom_format_print'] = "列印詳細格式";
	$Lang['eEnrolment']['SimpleView'] = "簡易格式";
	$Lang['eEnrolment']['SimpleView1'] = "簡易格式1";
	$Lang['eEnrolment']['SimpleView2'] = "簡易格式2";
	$Lang['eEnrolment']['DetailedView'] = "詳細格式";
	$Lang['eEnrolment']['EnrolledClubList'] = "參與學會列表";
	$Lang['eEnrolment']['delete_selected_date'] = "移除所選日期";
	$Lang['eEnrolment']['ConfirmDelete'] = '你是否確定刪除所選日期？';
	$Lang['eEnrolment']['delete_selected_date_warning'] = "請最少選擇一個日期。";
	$Lang['eEnrolment']['ApplyPageBreakForEachClass'] = "使用新分頁列印不同班別資料";
	$Lang['eEnrolment']['ApplyPageBreakForEachClub'] = "使用新分頁列印不同學會資料";
	$Lang['eEnrolment']['ApplyPageBreakForEachStudent'] = "使用新分頁列印不同學生資料";

	$Lang['eEnrolment']['AddWeeklyAct'] = "加入每週聚會或活動";
	$Lang['eEnrolment']['StartDate'] = "開始日期";
	$Lang['eEnrolment']['EndDate'] = "結束日期";
	$Lang['eEnrolment']['OnEvery'] = "逄星期";
	$Lang['eEnrolment']['PeriodOfTime'] = "選取時期";
	$Lang['eEnrolment']['MustBeLater'] = "必須大於";
	$Lang['eEnrolment']['MustBeEarlier'] = "必須小於";
	$Lang['eEnrolment']['Error'] = "錯誤 ";
	$Lang['eEnrolment']['Err_IsInvalid'] = "不正確 ";
	$Lang['eEnrolment']['Err_NotSelect'] = "尚未選取";
	$Lang['eEnrolment']['NoLimit'] = "無限制";
	$Lang['eEnrolment']['ApplicationQuota'] = "報名配額";

	$Lang['eEnrolment']['disableUpdate'] = "不允許新增或更改資料";
	$Lang['eEnrolment']['disableUpdateAlertMsg'] = "此記錄已被鎖定, 不可更改";
	$Lang['eEnrolment']['selectRoleAlertMsg'] = "請選擇一個學生角色。";

	$Lang['eEnrolment']['disableHelperModificationRightLimitation'] = "容許點名助手修改過往出席紀錄";
	$Lang['eEnrolment']['disallowPICtoAddOrRemoveMembers'] = "不允許負責人新增或刪除會員";

	$Lang['eEnrolment']['lookupClashestoSendPushMessagetoclubPIC'] = "允許檢查時間衝突按批准發送推送訊息給學會負責人";
	$Lang['eEnrolment']['lookupClashestoSendPushMessagetoeventPIC'] = "允許檢查時間衝突按批准發送推送訊息給活動負責人";
	$Lang['eEnrolment']['PushMessage']['ClubCrash'] = "學生  __StudentChiName__ 已被   __CurrentPICChi__ 加到 「__CurrentClubChiName__」，時間和「__ClubChiName__」有相撞，如有查詢可向學會/小組負責老師聯絡。
		Student named __StudentEngName__  has been added to \"__CurrentClubEngName__\" by __CurrentPICEng__  which will have time-clash with \"__ClubEngName__\". For details enquiry, please contact the teacher-in-charge.";

	$Lang['eEnrolment']['PushMessage']['ActivityCrash'] = "學生 __StudentChiName__ 已被 __CurrentPICChi__ 加到「__CurrentActivityChiName__」，時間和「__ActivityName__」有相撞，如有查詢可向活動負責老師聯絡。
		Student named __StudentEngName__  has been added to \"__CurrentActivityEngName__\" by __CurrentPICEng__  which will have time-clash with  \"__ActivityName__\". For details enquiry, please contact the teacher-in-charge.";
	$Lang['eEnrolment']['ClubPIC'] = "學會負責人";
	$Lang['eEnrolment']['ActivityPIC'] = "活動負責人";
	$Lang['eEnrolment']['PushMessage']['ClubCrashTitle'] = "學會/活動 發生時間衝突通知
Time clashes with Club / Activities Reminder ";


	$Lang['eEnrolment']['ClearUnhandledData']['UnhandledData'] = "候批紀錄";
	$Lang['eEnrolment']['ClearUnhandledData']['jsWarningArr']['NoData'] = "因沒有學會紀錄，所以未能進行資料刪除。";
	$Lang['eEnrolment']['ClearUnhandledData']['jsWarningArr']['Delete'] = "你是否確定要清除所有未批核申請紀錄？";

	$Lang['eEnrolment']['NotificationLetter']['Content'] = "<!--Name-->的報名過程已經完成。貴子弟已被取錄。你可以登入內聯網，前往 資訊服務 > 課外活動管理 檢視貴子弟被批准參與的<!--Type-->。 如有任何查詢，請聯絡系統管理員或課外活動報名負責老師。";
	$eEnrollment['PaymentAmount'] = "暫定費用($)";

	### Transfer to SP
	$Lang['eEnrolment']['Transfer_to_SP']['StepArr'][0] = "設定";
	$Lang['eEnrolment']['Transfer_to_SP']['StepArr'][1] = "確定";
	$Lang['eEnrolment']['Transfer_to_SP']['StepArr'][2] = "結果";
	$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['SelectDataSource'] = "請選擇資料來源。";
	$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['SelectForm'] = "請選擇對象級別。";
	$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['TransferDataToSP'] = "以上紀錄將傳送至學生檔案。你是否確定要將紀錄傳送到學生檔案?";
	$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['UploadAllFiles'] = "請上載所有對應檔案。";
	$Lang['eEnrolment']['Transfer_to_SP']['Button']['PrintDetailsData'] = "列印詳細資料";
	$Lang['eEnrolment']['Transfer_to_SP']['Button']['TransferOtherRecords'] = "傳送其他紀錄";
	$Lang['eEnrolment']['Transfer_to_SP']['DataSource'] = "資料來源";
	$Lang['eEnrolment']['Transfer_to_SP']['DataNameLang'] = "資料名稱";
	$Lang['eEnrolment']['Transfer_to_SP']['PreparingData'] = "正在準備資料...";
	$Lang['eEnrolment']['Transfer_to_SP']['ClubTransferProgress'] = "已傳送學會會員資料: <!--NumOfRecord-->";
	$Lang['eEnrolment']['Transfer_to_SP']['ActivityTransferProgress'] = "已傳送活動參加者資料: <!--NumOfRecord-->";

	$Lang['eEnrolment']['Transfer_to_SP']['ProgressDisplayArr']['DataPreparationArr']['Club'] = "已預備學會會員資料: <!--NumOfRecord-->";
	$Lang['eEnrolment']['Transfer_to_SP']['ProgressDisplayArr']['DataPreparationArr']['Activity'] = "已預備活動參加者資料: <!--NumOfRecord-->";
	$Lang['eEnrolment']['Transfer_to_SP']['ProgressDisplayArr']['DataTransferringArr']['Club'] = "學會資料傳送中...";
	$Lang['eEnrolment']['Transfer_to_SP']['ProgressDisplayArr']['DataTransferringArr']['Activity'] = "活動資料傳送中...";
	$Lang['eEnrolment']['Transfer_to_SP']['ReturnMsgArr']['DataTransferArr']['success'] = "資料傳送完成。";
	$Lang['eEnrolment']['Transfer_to_SP']['ReturnMsgArr']['DataTransferArr']['failed'] = "資料傳送失敗。";
	$Lang['eEnrolment']['Transfer_to_SLP']['Filter']['NotYetTransfer'] = "未傳送";
	$Lang['eEnrolment']['total_overall_perform'] = "總分";
	$Lang['eEnrolment']['overall_class_perform'] = "班別整體表現";
	$Lang['eEnrolment']['overall_perform'] = "全校整體表現";
	$Lang['eEnrolment']['perform_score_comment'] = "表現(分數/評語)";
	$Lang['eEnrolment']['print_house_enrolment_result'] = "列印整個社報名結果";

	$Lang['eEnrolment']['PerformanceAdjustment'] = "出席分數";
	$Lang['eEnrolment']['GeneratePerformance'] = "計算出席分數";
	$Lang['eEnrolment']['GeneratePerformanceWarning'] = "出席分數將以此公式計算：出席次數 x 10<br><br>如果你曾手動更改出席分數，進行計算將複寫該等變更。";
	$Lang['eEnrolment']['GeneratePerformanceCompleted'] = "出席分數計算完成";
	$Lang['eEnrolment']['LastGenerated'] = "上一次計算";
	$Lang['eEnrolment']['GeneratePerformanceFailed'] = "無法計算出席分數";
	$Lang['eEnrolment']['ClubActTotalPerformance'] = "學會活動綜合出席分數";
	$Lang['eEnrolment']['Adjustment'] = "調整";
	$Lang['eEnrolment']['TotalPerformance'] = "總分";
	$Lang['eEnrolment']['InvalidData'] = "請輸入數字";
	$Lang['eEnrolment']['Priority'] = "優先次序";

	$Lang['eEnrolment']['EnablePopupNotification'] = "使用自動提示";
	$Lang['eEnrolment']['PopupNotification'] = "自動提示";
	$Lang['eEnrolment']['PopupDateRange'] = "顯示日期 (由...到...)";
	$Lang['eEnrolment']['PopupContent'] = "內容";
	$Lang['eEnrolment']['Reminder'] = "自動提示";

	$Lang['eEnrolment']['ConfirmChangeScheduleTime'] = "更改活動時間將導致現有學生出席紀錄被移除，還未開始的活動則不受影響。你是否確定要繼續？";

	$Lang['eEnrolment']['TransferToSPWarningArr'][0] = "資料傳送的學會名稱語言取決於用戶現行的介面語言，請確保你現正使用與資料傳送相同的介面語言。";
	$Lang['eEnrolment']['TransferToSPWarningArr'][1] = "如傳送的學會/活動的名稱和「學生檔案」中現存紀錄的學會/活動名稱相同，系統會自動更新該等紀錄。";
	$Lang['eEnrolment']['TransferToOleWarning'] = "資料傳送的學會名稱語言取決於用戶現行的介面語言，請確保你現正使用與資料傳送相同的介面語言。";

	$eEnrollment['student_enrolment_rule'] = "學生可選擇<font color='red'><!--MinNumApp--></font>至<font color='red'><!--MaxNumApp--></font>學會並最多可參加<font color='red'><!--MaxEnrol--></font>學會";
	$eEnrollment['student_enrolment_rule_with_min'] = "學生可選擇<font color='red'><!--MinNumApp--></font>至<font color='red'><!--MaxNumApp--></font>學會並可參加<font color='red'><!--MinEnrol--></font>至<font color='red'><!--MaxEnrol--></font>學會";
	$eEnrollment['student_enrolment_rule_activity'] = "學生可選擇<font color='red'><!--MinNumApp--></font>至<font color='red'><!--MaxNumApp--></font>活動並最多可參加<font color='red'><!--MaxEnrol--></font>活動";
	$eEnrollment['student_enrolment_rule_activity2'] = "個";
	$Lang['eEnrolment']['ReturnMsg_DataComeFromAdminConsole'] = "0|=|部份由行政管理中心加入的資料不能被更新。";

	$Lang['eEnrolment']['CategorySetting'] = "類別設定";
	$Lang['eEnrolment']['GeneralSetting'] = "一般設定";

	$Lang['eEnrolment']['Category'] = "類別";
	$Lang['eEnrolment']['UseCategorySetting'] = "使用類別設定";
	$Lang['eEnrolment']['Enable'] = "使用";
	$Lang['eEnrolment']['Disable'] = "停用 (使用一般設定)";

	$Lang['eEnrolment']['PleaseSelectCategory'] = "請選擇類別";
	$Lang['eEnrolment']['CategorySettingInstruction'] = "學生已開始報名，如果要啟用/停用此設定，請先清除所有報名紀錄。";
	$Lang['eEnrolment']['BasicSettings'] = "基本設定";
	$Lang['eEnrolment']['AllowToManageRecordOfPreviousYear'] = "允許管理往年紀錄";
	$Lang['eEnrolment']['GradingScheme'] = "評級準則";
	$Lang['eEnrolment']['List'] = "清單";
	$Lang['eEnrolment']['MinScore'] = "分數下限";
	$Lang['eEnrolment']['MaxScore'] = "分數上限";
	$Lang['eEnrolment']['ScoreRange'] = "分數範圍";
	$Lang['eEnrolment']['Grade'] = "評級";
	$Lang['eEnrolment']['AlertMsg_MinScoreNotGreaterThanMaxScore'] = "分數上限必須大於分數下限";
	$Lang['eEnrolment']['CategoryName'] = "類別名稱";
	$Lang['eEnrolment']['StudentEnrolmentReport'] = "學生活動記錄";
	$Lang['eEnrolment']['DefaultOleSetting'] = "預設 OLE 設定";
	$Lang['eEnrolment']['ForClubOnly'] = "(只適用於學會資料)";
	$Lang['eEnrolment']['EnroledClubList'] = "匯出參與學會列表";
	$Lang['eEnrolment']['ClearAllEnrol'] = "清除資料";
	$Lang['eEnrolment']['DataDeletionRemarks'][0] = '此功能主要用於新學年開始報名前';
	$Lang['eEnrolment']['ClearEnrol'] = "清除報名紀錄";
	$Lang['eEnrolment']['EnrolRecord'] = "報名紀錄";
	$Lang['eEnrolment']['LastClearEnrol'] = "最近清除紀錄時間";
	$Lang['eEnrolment']['DataHandling'] = "資料處理";

	# Transfer to WebSAMS
	$Lang['eEnrolment']['TransferToWebSAMS']['SelectCriteria'] = "選擇條件";
	$Lang['eEnrolment']['TransferToWebSAMS']['UploadMappingFiles'] = "上載對應檔案";
	$Lang['eEnrolment']['TransferToWebSAMS']['reportCardReadableIndicator'] = "成績表可讀取示標";
	$Lang['eEnrolment']['TransferToWebSAMS']['Yes'] = "是";
	$Lang['eEnrolment']['TransferToWebSAMS']['No'] = "否";
	$Lang['eEnrolment']['TransferToWebSAMS']['eClassCode']= "eClass 編號";
	$Lang['eEnrolment']['TransferToWebSAMS']['eClassTitleEng']= "eClass 名稱 (英文)";
	$Lang['eEnrolment']['TransferToWebSAMS']['eClassTermNameEng']= "eClass 學期名稱 (英文)";
	$Lang['eEnrolment']['TransferToWebSAMS']['eClassType']= "eClass 類型";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSchoolYear']= "WebSAMS 學年";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSDuration']= "WebSAMS 時段";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTACode']= "WebSAMS 課外活動代碼";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAType']= "WebSAMS 課外活動類型";
	$Lang['eEnrolment']['TransferToWebSAMS']['ReportCardReadableIndicator']= "成績表可讀取示標";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSMajorComponentsofOtherLearningExperiencesCode']= "其他學習經歷的主要種類代碼";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicator'] = "匯出模式";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicatorBySingle'] = "匯出儲存所有學會活動報告的單一檔案";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicatorByAll'] = "匯出一個獨立儲存所有學會活動報告的壓縮(zipped)資料夾";
	$Lang['eEnrolment']['TransferToWebSAMS']['Instruction'] = "請下載範例，核對及加入對應資料，再上載對應檔案。";

	$Lang['eEnrolment']['TransferToWebSAMS']['eClassPost']= "eClass 職位";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAPostCode']= "WebSAMS 活動職位代碼";

	$Lang['eEnrolment']['TransferToWebSAMS']['Club&ActivityMapping']= "學會及活動對應";
	$Lang['eEnrolment']['TransferToWebSAMS']['PostMapping']= "職位對應";
	$Lang['eEnrolment']['TransferToWebSAMS']['PerformanceMapping']= "表現對應";

	$Lang['eEnrolment']['TransferToWebSAMS']['eClassPerformance'] = "eClass 表現";
	$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAPerformanceCode'] = "WebSAMS 活動表現代碼";

	$Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] = "不存在";
	$Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isInvalid'] = "不正確";

	$Lang['eEnrolment']['TransferToWebSAMS']['NoOfRecord'] = "紀錄數量";
	$Lang['eEnrolment']['TransferToWebSAMS']['ValidRecord'] = "有效紀錄數量";
	$Lang['eEnrolment']['TransferToWebSAMS']['InvalidRecord'] = "錯誤紀錄數量";

	# Attendance Statistic Report
	$Lang["eEnrolment"]['ReportArr']["AttendanceStatistic"] = "出席統計";
	$Lang["eEnrolment"]['AttendanceStatistic']['TotalNum'] = "會員人數";
	$Lang["eEnrolment"]['AttendanceStatistic']['PresentNum'] = "出席人數";
	$Lang["eEnrolment"]['AttendanceStatistic']['AbsentNum'] = "缺席人數";
	$Lang["eEnrolment"]['AttendanceStatistic']['ExemptNum'] = "豁免人數";
	if($sys_custom['eEnrolment']['Attendance_Late']){
		$Lang["eEnrolment"]['AttendanceStatistic']['LateNum'] = "遲到人數";
	}
	if($sys_custom['eEnrolment']['Attendance_EarlyLeave']){
		$Lang["eEnrolment"]['AttendanceStatistic']['EarlyLeaveNum'] = "早退人數";
	}
	# Attendance Status Report
	$Lang['eEnrolment']['AttendanceStatusReport'] = "每日出席狀態";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['IndividualStudent'] = "單一學生";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['AttendanceStatusReport'] = "出席狀況";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['StudentSource'] = "選擇學生自";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['FormClass'] = "級別 / 班別";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['IndividualStudent'] = "單一學生";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['StudentName'] = "學生姓名";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['StudentLogin'] = "學生內聯網登入帳戶";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['AttendanceSource'] = "出席資料來源";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['AttendanceStatus'] = "出席資料狀態";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['ShowMainGuardianInfo'] = "顯示主監護人資料";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['Club/ActivityName'] = "學會 / 活動名稱";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['GuardianName'] = "監護人姓名";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['GuardianRelationship'] = "監護人關係";
	$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['EmergencyPhoneNo'] = "緊急聯絡電話";

	# Student without Enrolment Report
	$Lang['eEnrolment']['StudentWithoutEnrolmentReport'] = "未報名學生名單";
	$Lang['eEnrolment']['ReportArr']['StudentWithoutEnrolmentReport']['EnrolmentYear'] = "報名資料年份";
	$Lang['eEnrolment']['ReportArr']['StudentWithoutEnrolmentReport']['EnrolmentSource'] = "報名資料來源";
	$Lang['eEnrolment']['WarningArr']['StudentWithoutEnrolmentReport']['ChooseSource'] = "請選擇來源";
	$Lang['eEnrolment']['ReportArr']['StudentWithoutEnrolmentReportArr']['StudentName'] = "學生姓名";


	$Lang['eEnrolment']['WarningArr']['InputDate'] = "請輸入日期";
	$Lang['eEnrolment']['WarningArr']['SelectClub'] = "請選擇學會";
	$Lang['eEnrolment']['WarningArr']['SelectActivity'] = "請選擇活動";
	$Lang['eEnrolment']['WarningArr']['SelectClass'] = "請選擇班別";
	$Lang['eEnrolment']['WarningArr']['SelectStudent'] = "請選擇學生";
	$Lang['eEnrolment']['WarningArr']['ChooseSource'] = "請選擇來源";
	$Lang['eEnrolment']['WarningArr']['InputStudentName'] = "請輸入學生姓名";
	$Lang['eEnrolment']['WarningArr']['InputStudentLogin'] = "請輸入學生內聯網登入帳戶";
	$Lang['eEnrolment']['WarningArr']['ChooseStatus'] = "請選擇狀態";


	$eEnrollment['school_quota_exceeded'] = "超出學校準許參與學會數目上限";
	$Lang['eEnrolment']['ClubCode'] = "學會編號";
	$Lang['eEnrolment']['ActivityCode'] = "活動編號";
	$Lang['eEnrolment']['SemesterGrade'] = "學期等級";
	$Lang['eEnrolment']['SemesterMark'] = "學期分數";
	$Lang['eEnrolment']['OverallGrade'] = "全年等級";
	$Lang['eEnrolment']['OverallMark'] = "全年分數";
	$Lang['eEnrolment']['Club_Import_FileDescription'] = array("學會編號", "學會名稱 (英文)", "學會名稱 (中文)", "可發放公眾宣布、事項通知", "小組簡介", "學會類型", "學期名稱", "對象級別", "對象所屬社", "對象年齡範圍", " 對象性別", "暫定費用 HK($)", "會員名額", "會員人數下限", "活動負責人", "活動點名助手", "活動日期及時間");
	$Lang['eEnrolment']['Club_Import_ReferenceLink'][3] = array("announce","按此查詢");
	$Lang['eEnrolment']['Club_Import_ReferenceLink'][5] = array("category","按此查詢學會類型");
	$Lang['eEnrolment']['Club_Import_ReferenceLink'][6] = array("term","按此查詢學期名稱");
	$Lang['eEnrolment']['Club_Import_ReferenceLink'][7] = array("form","按此查詢對象級別");
	$Lang['eEnrolment']['Club_Import_ReferenceLink'][8] = array("house","按此查詢對象社");
	$Lang['eEnrolment']['Club_Import_ReferenceLink'][9] = array("age","按此查詢對象年齡範圍");
	$Lang['eEnrolment']['Club_Import_ReferenceLink'][10] = array("gender","按此查詢對象性別");
	$Lang['eEnrolment']['Club_Import_ReferenceLink'][11] = array("fee","按此查詢暫定費用");
	$Lang['eEnrolment']['Club_Import_Remark'][6] = '在代號之間加上「,」可選擇多個學期';
	$Lang['eEnrolment']['Club_Import_Remark'][7] = '在代號之間加上「,」可選擇多個級別';
	$Lang['eEnrolment']['Club_Import_Remark'][8] = '在代號之間加上「,」可選擇多個社';
	$Lang['eEnrolment']['Club_Import_Remark'][14] = "請輸入內聯網帳號";
	$Lang['eEnrolment']['Club_Import_Remark'][15] = "請輸入內聯網帳號";
	$Lang['eEnrolment']['Club_Import_Remark'][16] = "格式 : YYYY-MM-DD hh:mm-hh:mm , 「分鐘」必須為5的倍數";
	$Lang['eEnrolment']['AllClubMember_Import_FileDescription'] = array('學會名稱 (英文)','學期','班別','學號','內聯網帳號','角色','表現','成就','意見','活躍/非活躍','職員角色');
	$Lang['eEnrolment']['AllActivityMember_Import_FileDescription'] = array('活動名稱','班別','學號','內聯網帳號','角色','表現','成就','意見','活躍/非活躍','職員角色');
	$Lang['eEnrolment']['ClubActivityMember_Import_FileDescription'] = array('班別','學號','內聯網帳號','角色','表現','成就','意見','活躍/非活躍','職員角色');
	$Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit']=array('優點', '小功', '大功','SuperMerit','UltraMerit');
	$Lang['eEnrolment']['ImportRemarks_ClassnameClassNumber_Userlogin'] = "<span class='tabletextrequire'>^</span>請使用「班別及學號」或「內聯網帳號」";
	$Lang['eEnrolment']['Schedule'] = "日期及時間";
	$Lang['eEnrolment']['AllForm'] = "所有級別";
	$Lang['eEnrolment']['AgeGroup'] = "年齡範圍";
	$Lang['eEnrolment']['AgeGroupRange'] = "年齡範圍由 11 至 20 (如:12-17)";
	$Lang['eEnrolment']['TentativeFee'] = "暫定費用";
	$Lang['eEnrolment']['DirectlyInputFee'] = "直接輸入費用";
	$Lang['eEnrolment']['ScheduleFormat'] = "日期及時間格式";
	$Lang['eEnrolment']['Achievement'] = "成就";
	$Lang['eEnrolment']['AvailiableDate'] = "有效日期";
	$Lang['eEnrolment']['AvailiableDateStart'] = "有效日期 (開始)";
	$Lang['eEnrolment']['AvailiableDateEnd'] = "有效日期(完結)";
	$Lang['eEnrolment']['AvailiableDateFrom'] = "由";
	$Lang['eEnrolment']['AvailiableDateTo'] = "至";
	$Lang['eEnrolment']['UpdatePerformanceAndPerformance'] = "更新表現及成就";
	$Lang['eEnrolment']['FromOtherYear'] = "自其他學年";
	$Lang['eEnrolment']['FromOtherTerm'] = "自其他學期";
	$Lang['eEnrolment']['ApplySettingsToClubOfOtherTerms'] = "套用設定至此小組於其他學期的學會";
	$Lang['eEnrolment']['ApplySettingsToClubOfOtherTermsWarning'] = "系統將會重寫此小組於其他學期的學會的設定。你是否確定套用設定至此小組於其他學期的學會？";

	$Lang['eEnrolment']['Activity_Import_FileDescription'] = array("所屬學會", "活動名稱", "活動編號", "小組簡介", "活動類型");
	if($sys_custom['eEnrolment']['TWGHCYMA']){
		$Lang['eEnrolment']['Activity_Import_FileDescription'] = array_merge($Lang['eEnrolment']['Activity_Import_FileDescription'],array("義工服務","義工服務時數","活動地點","校內地點","校外地點"));
	}
	$Lang['eEnrolment']['Activity_Import_FileDescription'] = array_merge($Lang['eEnrolment']['Activity_Import_FileDescription'],array("活動性質", "對象級別", "對象所屬社" ,"對象年齡範圍", " 對象性別", "暫定費用 HK($)", "活動負責人", "活動點名助手", "報名方式", "參加名額", "報名時段開始", "報名時段完結", "報名權限", "活動日期及時間"));
	$Lang['eEnrolment']['Activity_Import_ReferenceLink'][0] = array("existingClub","按此查詢學會");
	$Lang['eEnrolment']['Activity_Import_ReferenceLink'][4] = array("category","按此查詢學會類型");
	if($sys_custom['eEnrolment']['TWGHCYMA']){
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][7] = array("activityLocation","按此查詢活動地點");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][10] = array("activityNature","按此查詢活動性質");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][11] = array("form","按此查詢對象級別");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][12] = array("house","按此查詢對象社");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][13] = array("age","按此查詢對象年齡範圍");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][14] = array("gender","按此查詢對象性別");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][15] = array("fee","按此查詢暫定費用");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][18] = array("enrolMethod","按此查詢報名方式");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][22] = array("enrolSetting","按此查詢報名權限");
		$Lang['eEnrolment']['Activity_Import_Remark'][4] = "如多於一個「活動類型」， 請使用逗號分隔。";
		$Lang['eEnrolment']['Activity_Import_Remark'][5] = "請輸入 Yes 或者 No";
		$Lang['eEnrolment']['Activity_Import_Remark'][10] = "如多於一個「活動性質」， 請使用逗號分隔。";
		$Lang['eEnrolment']['Activity_Import_Remark'][11] = '在代號之間加上「,」可選擇多個級別';
		$Lang['eEnrolment']['Activity_Import_Remark'][12] = '在代號之間加上「,」可選擇多個社';
		$Lang['eEnrolment']['Activity_Import_Remark'][16] = "請輸入內聯網帳號";
		$Lang['eEnrolment']['Activity_Import_Remark'][17] = "請輸入內聯網帳號";
		$Lang['eEnrolment']['Activity_Import_Remark'][20] = "格式 : YYYY-MM-DD hh:mm , 「分鐘」必須為5的倍數";
		$Lang['eEnrolment']['Activity_Import_Remark'][21] = "格式 : YYYY-MM-DD hh:mm , 「分鐘」必須為5的倍數";
		$Lang['eEnrolment']['Activity_Import_Remark'][23] = "格式 : YYYY-MM-DD hh:mm-hh:mm , 「分鐘」必須為5的倍數";
		$Lang['eEnrolment']['Activity_Import_Remark'][24] = "如多於一個「對象級別」或「對象所屬社」， 請使用逗號分隔。 (e.g. F.1, F.2)";
	}else{
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][5] = array("nature","按此查詢活動性質");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][6] = array("form","按此查詢對象級別");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][7] = array("house","按此查詢對象社");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][8] = array("age","按此查詢對象年齡範圍");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][9] = array("gender","按此查詢對象性別");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][10] = array("fee","按此查詢暫定費用");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][13] = array("enrolMethod","按此查詢報名方式");
		$Lang['eEnrolment']['Activity_Import_ReferenceLink'][17] = array("enrolSetting","按此查詢報名權限");
		$Lang['eEnrolment']['Activity_Import_Remark'][6] = '在代號之間加上「,」可選擇多個級別';
		$Lang['eEnrolment']['Activity_Import_Remark'][7] = '在代號之間加上「,」可選擇多個社';
		$Lang['eEnrolment']['Activity_Import_Remark'][11] = "請輸入內聯網帳號";
		$Lang['eEnrolment']['Activity_Import_Remark'][12] = "請輸入內聯網帳號";
		$Lang['eEnrolment']['Activity_Import_Remark'][15] = "格式 : YYYY-MM-DD hh:mm , 「分鐘」必須為5的倍數";
		$Lang['eEnrolment']['Activity_Import_Remark'][16] = "格式 : YYYY-MM-DD hh:mm , 「分鐘」必須為5的倍數";
		$Lang['eEnrolment']['Activity_Import_Remark'][18] = "格式 : YYYY-MM-DD hh:mm-hh:mm , 「分鐘」必須為5的倍數";
		$Lang['eEnrolment']['Activity_Import_Remark'][19] = "如多於一個「對象級別」或「對象所屬社」， 請使用逗號分隔。 (e.g. F.1, F.2)";
	}
	$Lang['eEnrolment']['EnrolmentPeriodStart'] = "報名時段開始";
	$Lang['eEnrolment']['EnrolmentPeriodEnd'] = "報名時段完結";
	#House Mapping
	$Lang['eEnrolment']['TargetHouse'] = '對象所屬社';
	$Lang['eEnrolment']['jsWarning']['SpecifyHouse'] = '請指定一個社';
	#Award Mgmt
	$Lang['eEnrolment']['AwardMgmt']['Award'] = '獎項';
	$Lang['eEnrolment']['AwardMgmt']['AwardMgmt'] = '獎項管理';
	$Lang['eEnrolment']['AwardMgmt']['AwardNameCh'] = '獎項名稱(中文)';
	$Lang['eEnrolment']['AwardMgmt']['AwardNameEn'] = '獎項名稱(英文)';
	$Lang['eEnrolment']['AwardMgmt']['Award'] = '獎項';
	$Lang['eEnrolment']['AwardMgmt']['AwardType'] = '獎項類別';
	$Lang['eEnrolment']['AwardMgmt']['IndividualAward'] = '個人獎項';
	$Lang['eEnrolment']['AwardMgmt']['GroupAward'] = '團體獎項';
	$Lang['eEnrolment']['AwardMgmt']['GroupName'] = '團體名稱';
	$Lang['eEnrolment']['AwardMgmt']['Division'] = '分組';
	$Lang['eEnrolment']['AwardMgmt']['DivisionCh'] = '分組名稱(中文)';
	$Lang['eEnrolment']['AwardMgmt']['DivisionEn'] = '分組名稱(英文)';
	$Lang['eEnrolment']['AwardMgmt']['EventName'] = '活動/比賽名稱';
	$Lang['eEnrolment']['AwardMgmt']['ReportType'] = '報告類型';
	$Lang['eEnrolment']['AwardMgmt']['twghwfns']['ActivityAwardReport'] = '活動獎項報告';
	$Lang['eEnrolment']['AwardMgmt']['twghwfns']['ECM'] = '學校活動報告 (ECM)';
	$Lang['eEnrolment']['AwardMgmt']['twghwfns']['IMC'] = '學校活動報告 (IMC)';
	$Lang['eEnrolment']['AwardMgmt']['twghwfns']['MagazineReport'] = '校刊報告';
	$Lang['eEnrolment']['AwardMgmt']['twghwfns']['UpdateAch'] = '更新獎項資料到學生成就';

	# Tseung Kwan O Govt Sec Sch
	$Lang['eEnrolment']['PerformanceMark'] = "Performance Mark";
	$Lang['eEnrolment']['ECA'] = "ECA";
	$Lang['eEnrolment']['SS'] = "SS";
	$Lang['eEnrolment']['CS'] = "CS";

	# HKUGA College
	$Lang['eEnrolment']['Location'] = "地點";

	$Lang['eEnrolment']['DisplaySelectedStudentOnly'] = "只列印已選擇的學生";

	$Lang['eEnrolment']['ImportMember']['WaitingRemarks'] = "只適用於新成員，已批准的成員將被忽略。";
	$eEnrollment['already_member'] = "這學生已被批核。";

	$Lang['eEnrolment']['TargetNameLang'] = "目標名稱語文";

	$Lang['eEnrolment']['MaxWantZeroWarningMsg'] = "因你不希望參加任何學會，如你按下「呈送」，除已批核的學會紀錄外，過往的學會報名紀錄將被刪除。";
	$Lang['eEnrolment']['ClubApplication'] = "學會報名";
	$Lang['eEnrolment']['ClubRejected'] = "已拒絕學會";
	$Lang['eEnrolment']['DateType'] = "日期種類";
	$Lang['eEnrolment']['DateTypeArr']['Single'] = "一次性";
	$Lang['eEnrolment']['DateTypeArr']['Periodic'] = "重複性";
	$Lang['eEnrolment']['EnrollmentInformation'] = "報名資料";
	$Lang['eEnrolment']['SubmitApplication'] = "呈送報名資料";
	$Lang['eEnrolment']['ApplicantInformation'] = "報名者資料";
	$Lang['eEnrolment']['MaxClub'] = "學生希望參加的學會數目";
	$Lang['eEnrolment']['ViewAll'] = "顯示所有";
	$Lang['eEnrolment']['HideAll'] = "隱藏所有";
	$Lang['eEnrolment']['TimeDescription'] = "顯示活動階段";
	$Lang['eEnrolment']['ViewDetailedTimePeriod'] = "顯示詳細活動時段";
	$Lang['eEnrolment']['HideDetailedTimePeriod'] = "隱藏詳細活動時段";
	$Lang['eEnrolment']['AnnounceEnrolmentResultTime'] = "公佈報名結果時間";
	$Lang['eEnrolment']['WithinEnrolmentProcessPeriod'] = "學校正處理報名資料，你可於 <!--date--> <!--time--> 檢視報名結果。";
	$Lang['eEnrolment']['ExportMember'] = "匯出成員";
	$Lang['eEnrolment']['ExportMemberExtraData'] = "匯出成員額外資料";
	$Lang['eEnrolment']['RecordName'] = "紀錄名稱";
	$Lang['eEnrolment']['ReferenceEnrolmentPeriodRemarks'] = "確實報名截止日期以每個活動的獨立截止日期為準";
	$Lang['eEnrolment']['ApplicationDeadline'] = "截止報名日期";
	$Lang['eEnrolment']['OleCategory'] = "對應其他學習經歷類別";
	$Lang['eEnrolment']['OleCategoryRemarks'] = "現在更新其他學習經歷類別配對不會影響已配對的學會和活動紀錄。";

	$Lang['eEnrolment']['MeetingDatePeriod']['ShortWeekday'][0] = "日";
	$Lang['eEnrolment']['MeetingDatePeriod']['ShortWeekday'][1] = "一";
	$Lang['eEnrolment']['MeetingDatePeriod']['ShortWeekday'][2] = "二";
	$Lang['eEnrolment']['MeetingDatePeriod']['ShortWeekday'][3] = "三";
	$Lang['eEnrolment']['MeetingDatePeriod']['ShortWeekday'][4] = "四";
	$Lang['eEnrolment']['MeetingDatePeriod']['ShortWeekday'][5] = "五";
	$Lang['eEnrolment']['MeetingDatePeriod']['ShortWeekday'][6] = "六";


	# Enrolment Approval Reason
	$Lang['eEnrolment']['EnrolReason']['FillInReason'] = "填寫原因";
	$Lang['eEnrolment']['Reason'] = "原因";
	$Lang['eEnrolment']['RejectError']['ApplicationHasBeenRejectedAlready'] = "申請狀況已是已拒絕";

	$Lang['eEnrolment']['RecordType'] = "資料類型";
	$Lang['eEnrolment']['AllMembers'] = "全部會員";
	$Lang['eEnrolment']['MembersApprovedByYou'] = "由你批核的會員";
	$Lang['eEnrolment']['AllParticipants'] = "全部參加者";
	$Lang['eEnrolment']['ParticipantsApprovedByYou'] = "由你批核的會員參加者";

	$Lang['eEnrolment']['jaWarning']['TermMinimumApplication'] = "最少須要選擇<!--NumOfClub-->個學期「<!--TermName-->」的學會";
	$Lang['eEnrolment']['jaWarning']['TermMaximumApplication'] = "最多只可於學期「<!--TermName-->」選擇<!--NumOfClub-->個學會";

	$Lang['eEnrolment']['CommentBank'] = "評語庫";
	$Lang['eEnrolment']['PerformanceCommentBank'] = "表現評語庫";
	$Lang['eEnrolment']['TeacherCommentBank'] = "老師評語庫";
	# Achievement Comment Bank
	//if($sys_custom['eEnrolment']['AchievementCommentBank']){
	$Lang['eEnrolment']['AchievementCommentBank'] = "成就評語庫";
	$Lang['eEnrolment']['jaWarning']['PleaseFillInCode'] = "請輸入編號";
	$Lang['eEnrolment']['jaWarning']['PleaseFillInComment'] = "請輸入評語";
	//$Lang['eEnrolment']['jaWarning']['PleaseFillInWebSAMS'] = "請輸入WebSAMS 代號";
	$Lang['eEnrolment']['jaWarning']['CodeAlreadyExists'] = "編號已存在";
	$Lang['eEnrolment']['jaWarning']['AlertSelectFile'] = '請選擇檔案';
	//}

	# CHING CHUNG HAU PO WOON SEC SCH Report
	if($sys_custom['eEnrolment']['CCHPWOutOfSchoolActivityApplicationForm']){
		$Lang['eEnrolment']["OutOfSchoolActivityApplicationForm"] = "校外活動申請表";
	}
	if($sys_custom['eEnrolment']['CCHPWOutOfSchoolActivityStudentAttendConfirmList']){
		$Lang['eEnrolment']["OutOfSchoolActivityStudentList"] = "校外活動學生名單";
	}
	if($sys_custom['eEnrolment']['CCHPWOutOfSchoolActivityStudentList']){
		$Lang['eEnrolment']["OutOfSchoolActivityStudentAttendConfirmList"] = "校外活動學生出席名單確認表";
	}
	if($sys_custom['eEnrolment']['CCHPWEnrolParentNotice']){
		$Lang['eEnrolment']["EnrolParentNotice"] = "課外活動家長通知書";
	}

	$Lang['eEnrolment']['Transportation'] = "交通";
	$Lang['eEnrolment']['Organizer'] = "舉辦單位";
	$Lang['eEnrolment']['RewardOrCertification'] = "獎項或證書";
	$Lang['eEnrolment']['GatheringLocation'] = "集合地點";
	$Lang['eEnrolment']['DismissLocation'] = "散隊地點";

	# Singapore International School (SIS)
	$Lang['eEnrolment']['CategoryType'] = "類別類型";

	$Lang['eEnrolment']['AbsentNotification'] = "缺席通知";
	$Lang['eEnrolment']['AbsentNotificationRemarks'] = '通知將會透過eClassApp傳送予家長。如家長不是eClassApp用戶，將會以電郵通知。';
	$Lang['eEnrolment']['ParentName'] = "家長姓名";
	$Lang['eEnrolment']['AbsentStudent'] = "缺席學生";
	$Lang['eEnrolment']['MailTitle'] = "標題";
	$Lang['eEnrolment']['MailContent'] = "內容";
	$Lang['eEnrolment']['MailTitleFixed'] = "<!--clubName-->缺席提示";
	$Lang['eEnrolment']['MailContentFixed'] = "<!--studentName--> 於 <!--datetime--> 缺席 <!--clubName--> 之活動。<br/>特此通知。";
	$Lang['eEnrolment']['ActivityDates'] = "活動日期";
	$Lang['eEnrolment']['ActivityFee'] = "活動費用";
	$Lang['eEnrolment']['Report']['CalendarView'] = '每月行事曆';

	if($sys_custom['eEnrolment']['TWGHCYMA']){
		$Lang['eEnrolment']['BelongToGroup'] = "所屬小組";
		$Lang['eEnrolment']['CategoryNature'] = "活動性質";
		$Lang['eEnrolment']['VolunteerService'] = "義工服務";
		$Lang['eEnrolment']['VolunteerServiceHour'] = "義工服務時數";
		$Lang['eEnrolment']['Hour'] = "小時";
		$Lang['eEnrolment']['ActivityLocation'] = "活動地點";
		$Lang['eEnrolment']['SchoolLocation'] = "校內地點";
		$Lang['eEnrolment']['ExternalLocation'] = "校外地點";
		$Lang['eEnrolment']['SchoolLocationSettingInstruction'] = "對於校內地點，請於 [ 設定活動日期及時間 ] 預訂地點。";
		$Lang['eEnrolment']['InputVolunteerServiceHourWarning'] = "請輸入義工服務時數。";
		$Lang['eEnrolment']['InputActivityExternalLocationWarning'] = "請入輸入校外活動地點。";
		$Lang['eEnrolment']['SelectActivityNatureWarning'] = "請選擇活動性質。";
		$Lang['eEnrolment']['ReserveActivityLocation'] = "預訂活動地點";
		$Lang['eEnrolment']['ReserveLocationNoDateWarning'] = "請先設定日期。";
		$Lang['eEnrolment']['RoomAvailableAtTheFollowingTimes'] = "所選地點可在以下時段預訂:";
		$Lang['eEnrolment']['RoomNotAvailableAtTheFollowingTimes'] = "所選地點不可在以下時段預訂:";
		$Lang['eEnrolment']['ReserveBookingTimeChangedWarning'] = "您已更改活動時間，請再次核實預約地點及時間。";
		$Lang['eEnrolment']['ConfirmClearBookingMsg'] = "你是否確定要清除地點預約？";
		$Lang['eEnrolment']['EmailReminder'] = "自動電郵提示";
		$Lang['eEnrolment']['UseEmailReminder'] = "使用自動電郵提示";
		$Lang['eEnrolment']['EmailSubject'] = "主旨";
		$Lang['eEnrolment']['Content'] = "內容";
		$Lang['eEnrolment']['RemindTime'] = "提示時間";
		$Lang['eEnrolment']['DaysBefore'] = "天之前";
		$Lang['eEnrolment']['At'] = "於";
		$Lang['eEnrolment']['EmailReminderRemark'] = "* 用以下這種格式連接變量與動態數據: <br />[=UserName=], [=ActivityName=], [=ActivityCode=], [=Category=], [=ActivityContent=], [=ActivityLocation=], [=ActivityTime=]";
		$Lang['eEnrolment']['ActivitySummaryGroup']['ActivitySummaryGroup'] = "活動總表 (組別)";
		$Lang['eEnrolment']['ActivitySummaryGroup']['PleaseSelectCategory'] = "請選擇活動類型。";
		$Lang['eEnrolment']['ActivitySummaryGroup']['GroupOrClub'] = "組別/科組";
		$Lang['eEnrolment']['ActivitySummaryGroup']['OLEType'] = "OLE類型";
		$Lang['eEnrolment']['ActivitySummaryGroup']['Target'] = "對象";
		$Lang['eEnrolment']['ActivitySummaryGroup']['ActivityTotalHour'] = "活動總時間(小時)";
		$Lang['eEnrolment']['ActivitySummaryGroup']['Total'] = "合計";
		$Lang['eEnrolment']['ActivitySummaryGroup']['GrandTotal'] = "總計";
		$Lang['eEnrolment']['ClubRolesAndComments']['ClubRolesAndComments'] = "學會職位及評語";
		$Lang['eEnrolment']['ClubRolesAndComments']['StudentsWithoutComment'] = "無評語的學生";
		$Lang['eEnrolment']['ClubRolesAndComments']['StudentsWithComment'] = "有評語的學生";
		$Lang['eEnrolment']['ActivitySummaryForm']['ActivitySummaryForm'] = "各級活動紀錄總表";
		$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityNatureOleTypeStatistics'] = "活動性質及OLE類型統計表";
		$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityTotal'] = "活動總數";
		$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityTotalHours'] = "活動總時數";
		$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['AwardedActvityTotal'] = "獲獎活動總數";
		$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['NumberOfActivity'] = "相關活動數目";
		$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['OLEType'] = "OLE類型";
		$Lang['eEnrolment']['ActivityAwardsReport']['ActivityAwardsReport'] = "活動獎項報告";
		$Lang['eEnrolment']['ActivityAwardsReport']['AwardOrAchievement'] = "獎項 / 證書文憑 / 成就";
		$Lang['eEnrolment']['ClubActivityStatistics']['ClubActivityStatistics'] = "學會活動數量統計表";
		$Lang['eEnrolment']['ClubActivityStatistics']['NumberOfClubActivity'] = "學會活動數目";
		$Lang['eEnrolment']['ActivitySummaryReport']['ActivitySummaryReport'] = "活動紀錄總表";
		$Lang['eEnrolment']['ActivitySummaryReport']['DisplayData'] = "顯示資料";
		$Lang['eEnrolment']['ActivitySummaryReport']['GroupOrClub'] = "組別/科組";
		$Lang['eEnrolment']['ActivitySummaryReport']['ActivityCode'] = "活動編號";
		$Lang['eEnrolment']['ActivitySummaryReport']['InternalOrExternal'] = "校內/校外舉行";
		$Lang['eEnrolment']['ActivitySummaryReport']['PersonInCharge'] = "活動負責人";
		$Lang['eEnrolment']['ActivitySummaryReport']['SupportTeachers'] = "支援老師";
		$Lang['eEnrolment']['ActivitySummaryReport']['ActivityGoal'] = "活動目標";
		$Lang['eEnrolment']['ActivitySummaryReport']['OLEType'] = "OLE類型";
		$Lang['eEnrolment']['ActivitySummaryReport']['ActivityEndDate'] = "完結日期";
		$Lang['eEnrolment']['ActivitySummaryReport']['NumberOfActivity'] = "活動次數";
		$Lang['eEnrolment']['ActivitySummaryReport']['NumberOfParticipants'] = "參與人數";
		$Lang['eEnrolment']['ActivitySummaryReport']['ActivityTotalHour'] = "活動總時間(小時)";
		$Lang['eEnrolment']['ActivitySummaryReport']['JointActivity'] = "合辦活動";
		$Lang['eEnrolment']['ActivitySummaryReport']['SchoolOrganizedActivity'] = "校內組別合辦活動";
		$Lang['eEnrolment']['ActivitySummaryReport']['ExternalOrganizationOrganizedActivity'] = "校外機構合辦活動";
		$Lang['eEnrolment']['ActivitySummaryReport']['Organiser'] = "主辦機構";
		$Lang['eEnrolment']['ActivitySummaryReport']['Partner'] = "合作機構";
		$Lang['eEnrolment']['ActivitySummaryReport']['OrganisedGroupOrganizationName'] = "合辦組別/機構名稱";
		$Lang['eEnrolment']['ActivitySummaryReport']['AwardedInActivity'] = "於活動獲取獎項";
		$Lang['eEnrolment']['ActivitySummaryReport']['AwardType'] = "獎項類型";
		$Lang['eEnrolment']['ActivitySummaryReport']['ChineseAuthorityName'] = "頒發機構名稱(中文)";
		$Lang['eEnrolment']['ActivitySummaryReport']['EnglishAuthorityName'] = "頒發機構名稱(英文)";
		$Lang['eEnrolment']['ActivitySummaryReport']['ChineseAwardName'] = "獎項名稱(中文)";
		$Lang['eEnrolment']['ActivitySummaryReport']['EnglishAwardName'] = "獎項名稱(英文)";
		$Lang['eEnrolment']['ActivitySummaryReport']['AwardWinners'] = "得獎學生名單";
		$Lang['eEnrolment']['ActivitySummaryReport']['School'] = "本校";
		$Lang['eEnrolment']['DailyActivityRecords']['DailyActivityRecords'] = "每日活動資料";
		$Lang['eEnrolment']['DailyActivityRecords']['ActivityCode'] = "活動編號";
		$Lang['eEnrolment']['DailyActivityRecords']['ActivityName'] = "活動名稱";
		$Lang['eEnrolment']['DailyActivityRecords']['ActivityCategory'] = "活動類型";
		$Lang['eEnrolment']['DailyActivityRecords']['ActivityGroup'] = "活動組別";
		$Lang['eEnrolment']['DailyActivityRecords']['StartDate'] = "開始日期";
		$Lang['eEnrolment']['DailyActivityRecords']['EndDate'] = "結束日期";
		$Lang['eEnrolment']['DailyActivityRecords']['Time'] = "時間";
		$Lang['eEnrolment']['DailyActivityRecords']['ActivityLocation'] = "活動地點";
		$Lang['eEnrolment']['DailyActivityRecords']['PersonInCharge'] = "活動負責人";
		$Lang['eEnrolment']['DailyActivityRecords']['Staff'] = "工作人員";
		$Lang['eEnrolment']['DailyActivityRecords']['StudentList'] = "參與學生名單";
		$Lang['eEnrolment']['DailyActivityRecords']['Remark'] = "備註";
		$Lang['eEnrolment']['GroupActivityStatistics']['GroupActivityStatistics'] = "各組別/屬組活動數量統計表";
		$Lang['eEnrolment']['GroupActivityStatistics']['Group'] = "組別";
		$Lang['eEnrolment']['GroupActivityStatistics']['Code'] = "對碼編號";
		$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupName'] = "組別/屬組名稱";
		$Lang['eEnrolment']['GroupActivityStatistics']['NumberOfActivity'] = "屬組活動數目";
		$Lang['eEnrolment']['GroupActivityStatistics']['TotalNumberOfActivity'] = "各會/組活動總數";
		$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupActivityHours'] = "屬組活動時數";
		$Lang['eEnrolment']['GroupActivityStatistics']['ClubOrGroupActivityHours'] = "各會/組活動總時數";
		$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupActivityParticipants'] = "屬組活動人次";
	}


	if($sys_custom['eEnrolment']['UnitRolePoint']){
		$Lang['eEnrolment']['MenuTitle']['Settings']['UnitRolePoint'] = "學會角色分數設定";
		$Lang['eEnrolment']['SysRport']['SendeNotice'] = "發送電子通告";
		$Lang['eEnrolment']['SysRport']['NoticeNumber'] = "通告編號";
		$Lang['eEnrolment']['SysRport']['NoticeContent'] = "內容";
		$Lang['eEnrolment']['SysRport']['NoticeTitle'] = "標題";
		$Lang['eEnrolment']['SysRport']['NoticeSendSuccess'] = "通告發送成功！";
		$Lang['eEnrolment']['SysRport']['button']['AttendanceReport'] = "列印學生課外活動出席記錄";
		$Lang['eEnrolment']['SysRport']['AttendanceReport']['Student Name'] = "Student Name";
		$Lang['eEnrolment']['SysRport']['AttendanceReport']['Class']="Class";
		$Lang['eEnrolment']['SysRport']['AttendanceReport']['Number']="Class No.";
		$Lang['eEnrolment']['SysRport']['AttendanceReport']['RoleCode'] ="Position Code";
		$Lang['eEnrolment']['SysRport']['AttendanceReport']['Qualified'] = "Qualified";
		$Lang['eEnrolment']['SysRport']['AttendanceReport']['Attendance'] = "Attendance < 70%";
		$Lang['eEnrolment']['SysRport']['AttendanceReport']['Unsatisfactoryperformance'] = "Unsatisfactory performance";
		$Lang['eEnrolment']['UnitRolePoint']['Point']= "Point";

		$Lang['eEnrolment']['UnitRolePoint']['CurrentYear']="將匯入的學年";
		$Lang['eEnrolment']['UnitRolePoint']['Semester'] = "Semester";
		$Lang['General']['JS_warning']['UnitRolePoint']['SemesterNotFound'] = "沒有該學期";
		$Lang['General']['JS_warning']['UnitRolePoint']['NotFound'] = "沒有該角色";
		$Lang['General']['JS_warning']['UnitRolePoint']['ClubNotFound']="沒有該學會";
		$Lang['General']['JS_warning']['UnitRolePoint']['ClubEmpty'] = "學會不能為空";
		$Lang['General']['JS_warning']['UnitRolePoint']['PointNotInt'] = "分數應為數字";
		$Lang['eEnrolment']['SysRport']['Report'] = "課外活動報告";
		$Lang['eEnrolment']['SysRport']['StudentReportCard'] = "學生報告單";


		$Lang['eEnrolment']['SysRport']['SchoolECAReport'] ="學校課外活動記錄";
		$Lang['eEnrolment']['SysRport']['SchoolECA']['Period'] = "Period";
		$Lang['eEnrolment']['SysRport']['SchoolECA']['Role'] = "Position";
		$Lang['eEnrolment']['SysRport']['SchoolECA']['Unit']="Unit";
		$Lang['eEnrolment']['SysRport']['SchoolECA']['StudentCode'] ="Student Code";
		$Lang['eEnrolment']['SysRport']['SchoolECA']['ChineseName'] = "Chinese";
		$Lang['eEnrolment']['SysRport']['SchoolECA']['EnglishName'] = "Name";
		$Lang['eEnrolment']['SysRport']['IndividualECAReport'] = "個人課外活動報告";
		$Lang['eEnrolment']['SysRport']['IndividualECA']['StudentType'] ="學生類型";
		$Lang['eEnrolment']['SysRport']['IndividualECA']['UpperLimit'] = "超過建議上限的學生";
		$Lang['eEnrolment']['SysRport']['IndividualECA']['AllStudent'] = "全部學生";
		$Lang['eEnrolment']['SysRport']['IndividualECA']['TotalPoint'] = "Points assigned";
		$Lang['eEnrolment']['SysRport']['IndividualECA']['SuggestPoint'] = "Suggested upper limit";
		$Lang['eEnrolment']['SysRport']['IndividualECA']['ClassNumber'] = "Class Number";
		$Lang['eEnrolment']['SysRport']['MasterList']="課外活動總括";

		$Lang['eEnrolment']['SysRport']['ECAPoint'] = "ECA Point";
		$Lang['eEnrolment']['SysRport']['UpperLimit'] = "Upper Limit";
		$Lang['eEnrolment']['SysRport']['ExceededBy'] = "Exceeded By";
		$Lang['eEnrolment']['SysRport']['Result'] = "Result";
		$Lang['eEnrolment']['SysRport']['FollowUpBy'] = "Follow up by";
		$Lang['eEnrolment']['SysRport']['Consequence'] = "Consequence<br/>(ECA pts)";
		$Lang['eEnrolment']['SysRport']['NewECAPoints'] = "New ECA Points";


		$Lang['eEnrolment']['SysRport']['ECAAttendanceReport'] = "學生課外活動出席記錄";

		$Lang['eEnrolment']['MenuTitle']['Settings']['ExceedPointSetting'] = "超額分數設定";
		$Lang['eEnrolment']['UnitRolePoint']['ExceedPoint']= "超額分數";

		$Lang['eEnrolment']['SysRport']['StudentECATotalPoint'] = "你的學會角色分數";
	}

	if($sys_custom['eEnrolment']['CanEditClubActivityDisplayName']){
	    $Lang['eEnrolment']['MenuTitle']['Settings']['NameSettings'] = "名稱設定";
	    $Lang['eEnrolment']['NameSettings']['ClubNameSetting']= "學會名稱設定";
	    $Lang['eEnrolment']['NameSettings']['ActivityNameSetting']= "活動名稱設定";
	    $Lang['eEnrolment']['NameSettings']['CurrentName']= "現有名稱";
	    $Lang['eEnrolment']['NameSettings']['DisplayName']= "顯示名稱";

	}


	if($sys_custom['eEnrolment']['CommitteeRecruitment']){
	    $eEnrollment['tab']['CommitteeRecruitment'] = "預報委員會";
	    $Lang['eEnrolment']['CommitteeRecruitmentSetting'] = "委員會設定";
	    $Lang['eEnrolment']['CommitteeRecruitment']['ChangeStatus'] = "更改狀態";
	    $Lang['eEnrolment']['CommitteeRecruitment']['CancelApply'] = "取消申請";
	    $Lang['eEnrolment']['CommitteeRecruitment']['Applied'] = "已申請";
	    $Lang['eEnrolment']['CommitteeRecruitment']['AddStudent'] = "新增";
	    $Lang['eEnrolment']['CommitteeRecruitment']['Setting']['Period'] = "預報申請時間";
	    $Lang['eEnrolment']['CommitteeRecruitment']['Setting']['SendMail'] = "允許預報申請狀態更改時發出電郵給學生";
	    $Lang['EmailNotification']['eEnrolment']['CommitteeRecruitmentResult']['Subject']= "Result of Committee Recruitment";
	    $Lang['EmailNotification']['eEnrolment']['CommitteeRecruitmentResult']['ApproveContent'] = "Dear student, <br>\r\n <br>\r\n  You have successfully enrolled to __CLUB_NAME__. The ECA advisor will contact you soon.
        <br>\r\n <br>\r\n ECA Team<br>\r\n";
	    $Lang['EmailNotification']['eEnrolment']['CommitteeRecruitmentResult']['RejectContent'] = "Dear student,<br>\r\n <br>\r\n Your enrolment to __CLUB_NAME__ is unsuccessful.
        <br>\r\n <br>\r\nECA Team<br>\r\n";
	    $Lang['eEnrolment']['CommitteeRecruitment']['ConfirmChangeStatus'] = "你是否確認要更改預報委員會報名狀態?";
	    $Lang['eEnrolment']['CommitteeRecruitment']['ConfirmCancelApply'] = "你是否確認要取消申請所選學會?";
	    $Lang['eEnrolment']['CommitteeRecruitment']['ClubYear'] = "學會年份";
	    $Lang['eEnrolment']['CommitteeRecruitment']['ConfirmApprove'] = "你是否確認要批准所選學生?";
	    $Lang['eEnrolment']['CommitteeRecruitment']['ConfirmReject'] = "你是否確認要拒絕所選學生?";
	    $Lang['eEnrolment']['CommitteeRecruitment']['CannotDuplicatePriority'] = "所選學會次序不能重複";
	    $Lang['eEnrolment']['CommitteeRecruitment']['CannotLeaveSpacePriority'] = "所選學會次序不能留空";
	    $Lang['eEnrolment']['CommitteeRecruitment']['StudentAlreadyEnrol'] = "該學生已報名此委員會";
	}

	if ($sys_custom['eEnrolment']['ClassTeacherView']) {
	    $eEnrollmentMenu['class_membership'] = "班別學生會員資料";
	}

	$Lang['eEnrolment']['WarningMsg']['NeedSelectEachCategory'] = "每個活動類別最少選擇一個學會";
	$Lang['eEnrolment']['WarningMsg']['NeedSelectTheCategory'] = "活動類別\"[__name__]\"必須選擇 [__qty__] 個學會";
	$Lang['eEnrolment']['WarningMsg']['NeedApplyClub'] = "你必須要選擇 <!--NoOfClub--> 個學會";
}

if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
	$Lang['eEnrolment']['tkogss_target'] = "本學年之對象";
	$Lang['eEnrolment']['tkogss_regNo'] = "註冊編號";
	$Lang['eEnrolment']['tkogss_hkid'] = "身分證號碼";
	$Lang['eEnrolment']['tkogss_dob'] = "出生日期";
	$Lang['eEnrolment']['tkogss_date'] = "簽發日期";
	$Lang['eEnrolment']['tkogss_Item'] = "項目";
	$Lang['eEnrolment']['tkogss_details'] = "活動詳情";
	$Lang['eEnrolment']['tkogss_grade'] = "等級";
	$Lang['eEnrolment']['tkogss_title'] = "標題";
	$Lang['eEnrolment']['tkogss_male'] = "男";
	$Lang['eEnrolment']['tkogss_female'] = "女";
	$Lang['eEnrolment']['tkogss_crossyear'] = " (跨年度)";
	$Lang['eEnrolment']['tkogss_eachyear'] = " (每年度)";
	$Lang['eEnrolment']['tkogss_supplementary'] = "丁. 補充資料";
	$Lang['eEnrolment']['tkogss_role_award'] = "職位 / 獎項";
	$Lang['eEnrolment']['tkogss_award'] = "積點";
	$Lang['eEnrolment']['tkogss_teacher'] = "教師";
	$Lang['eEnrolment']['tkogss_teacher_signature'] = "教師簽署";
	$Lang['eEnrolment']['tkogss_eca'] = "ECA";
	$Lang['eEnrolment']['tkogss_ser'] = "SS";
	$Lang['eEnrolment']['tkogss_cs'] = "CS";
	$Lang['eEnrolment']['tkogss_student_signature'] = "學生簽署";
}
if($sys_custom['eEnrolment']['ActivityIncludeInReports']){
	$Lang['eEnrolment']['IncludeInReports']='在報表中包含';
}

$Lang['eEnrolment']['EmailReminder'] = "自動電郵提示";
$Lang['eEnrolment']['AttendanceReminder'] = "自動電郵提示（點名）";
$Lang['eEnrolment']['UseAttendanceReminder'] = "使用自動電郵提示（點名）";
$Lang['eEnrolment']['AttendanceReminderTitle'] = "電郵主旨";
$Lang['eEnrolment']['AttendanceReminderContent'] = "電郵內容";
$Lang['eEnrolment']['AttendanceReminderTime'] = "提示時間";
$Lang['eEnrolment']['AttendanceReminderToday'] = "當天";
$Lang['eEnrolment']['AttendanceReminderDayAfter'] = "天後";
$Lang['eEnrolment']['AttendanceReminderAt'] = "於";
$Lang['eEnrolment']['excludeHoliday'] = "排除假期";
$Lang['eEnrolment']['AttenanceMailTitleFixed'] = "<!--clubName-->缺席提示";
$Lang['eEnrolment']['AttenanceMailContentFixed'] = "<!--studentName--> 於 <!--datetime--> 缺席 <!--clubName--> 之活動。<br/>截至<!--datetime--> <!--studentName-->出席率為<!--attendanceRate-->。<br/>特此通知。";
$Lang['eEnrolment']['AttenanceReminderRemark'] = "* 用以下這種格式連接變量與動態數據: <br />[=AttendanceDate=], [=ActivityName=]";
$Lang['eEnrolment']['Import']['DefaultOLE'] = '預設OLE<!--DataName-->';
$Lang['eEnrolment']['Import']['OLESettingRemark1'] = "在代號之間加上「,」可選擇多個種類";
$Lang['eEnrolment']['Import']['OLESettingBtn1'] = "按此查詢預設OLE資料類型";
$Lang['eEnrolment']['Import']['OLESettingBtn2'] = "按此查詢預設OLE語文代號";
$Lang['eEnrolment']['Import']['OLESettingBtn3'] = "按此查詢預設OLE紀錄類別";
$Lang['eEnrolment']['Import']['OLESettingBtn4'] = "按此查詢預設OLE其他學習經歷種類";
$Lang['eEnrolment']['Import']['OLESetting1_Warn'] = "預設OLE資料類型不正確";
$Lang['eEnrolment']['Import']['OLESetting2_Warn'] = "預設OLE語文代號不正確";
$Lang['eEnrolment']['Import']['OLESetting3_Warn'] = "沒有此OLE紀錄類別";
$Lang['eEnrolment']['Import']['OLESetting4_Warn'] = "沒有OLE其他學習經歷種類: ";

$Lang['eEnrolment']['Alert']['NotInASCOrder']="請依順序排列日期";

$Lang['eEnrolment']['StudentECASchedule']['ReportName']='學生課外活動日程';
$Lang['eEnrolment']['StudentECASchedule']['DisplayType'] = '顯示模式';
$Lang['eEnrolment']['StudentECASchedule']['Daily']='每日';
$Lang['eEnrolment']['StudentECASchedule']['Summary']='總結';
$Lang['eEnrolment']['StudentECASchedule']['DataSource']='包括資料';

$Lang['eEnrolment']['CreatedAndModifiedRecords'] = "建立及修改紀錄";
$Lang['eEnrolment']['CreateUserID'] = "建立紀錄用戶ID";
$Lang['eEnrolment']['CreateUserName'] = "建立紀錄用戶名稱";
$Lang['eEnrolment']['CreateUserDate'] = "建立紀錄時間";
$Lang['eEnrolment']['ArchivedStudent'] = "已整存學生";
$Lang['eEnrolment']['DateOfArchive'] = "整存日期";
$Lang['eEnrolment']['ArchivedBy'] = "整存者";
$Lang['eEnrolment']['ApproveClubActivity'] = "已批准學會/活動";

$Lang['eEnrolment']['button']['perform_drawing'] = "進行抽籤";
$Lang['eEnrolment']['ClearDrawingInstruction'] = "要重設所有申請至候批，請按此按鈕：";
$Lang['eEnrolment']['button']['clear_perform_drawing'] = "重設所有申請至候批";
$Lang['eEnrolment']['ClearDrawing']['jsWarningArr'] ="是否確定取消抽籤？";
$Lang['eEnrolment']['PerformDrawing']['jsWarningArr']['NoSelected']= "請選擇至少一個學會進行抽籤";
$Lang['eEnrolment']['InformedToClassTeacher'] = "通知班主任";
$Lang['eEnrolment']['InformedToPIC'] = "通知負責人";


$eEnrollment['add_club_activity']['WebSAMSSetting'] = "WebSAMS 設定";
$eEnrollment['WebSAMSSTACode'] = "WebSAMS 課外活動代碼";
$eEnrollment['WebSAMSSTAType'] = "WebSAMS 課外活動類型";
$eEnrollment['WebSAMSSTA']['ECA'] = "課外活動";
$eEnrollment['WebSAMSSTA']['ServiceDuty'] = "服務";
$eEnrollment['WebSAMSSTA']['InterSchoolActivities'] = "校內活動";
$eEnrollment['WebSAMSSTA']['MappingMode'] = "對應方法";
$eEnrollment['WebSAMSSTA']['UseWebSAMSCode'] = "使用系統內的WebSAMS code";
$eEnrollment['WebSAMSSTA']['UseMappingFile'] = "使用對應檔案";
$Lang['eEnrolment']['Import']['WebSAMS']['TypeError'] = "错误的 WebSAMS 課外活動類型";
$Lang['eEnrolment']['Import']['WebSAMS']['CheckType'] = "按此查詢WebSAMS課外活動類型";
$Lang['eEnrolment']['ReplySlip']= "回條";
$Lang['eEnrolment']['Customization'] = "定制";
$Lang['eEnrolment']['ExportActivityAwardMeritData'] = "匯出課外活動、獎項與記功資料";

$Lang['eEnrolment']['booking']['notAllowToDeleteApprovedBooking'] = '因為已經有確定的預定地點,所以不允許刪除';
$Lang['eEnrolment']['booking']['roomBooked'] = '所選時段及位置已有預定,請重選';
$Lang['eEnrolment']['booking']['unavailablePeriod'] = '所選時段無效,請重選';
$Lang['eEnrolment']['booking']['unknownError'] = '預定有衝突,請重選';
$Lang['eEnrolment']['booking']['violateBookingRule'] = '不符合預定規則,請重選';
$Lang['eEnrolment']['category']['applyToCourseCategory'] = '適合的課程類別';
$Lang['eEnrolment']['category']['qualifiedInstructor'] = '合資格導師';
$Lang['eEnrolment']['course']['addLesson'] = '新增課堂';
$Lang['eEnrolment']['course']['editLesson'] = '編輯課堂';
$Lang['eEnrolment']['course']['isCombinedLesson'] = '合併課堂';
$Lang['eEnrolment']['course']['locationList']['combinedCourseDescription'] = '(1) 容量充足的位置<br>(2) 容量不足的位置<br>N.B. 如該位置已被預定則不會顯示';
$Lang['eEnrolment']['course']['locationList']['description'] = '(1) 班別主位置<br>(2) 容量充足的位置<br>(3) 容量不足的位置<br>N.B. 如該位置已被預定則不會顯示';
$Lang['eEnrolment']['course']['locationList']['title'] = '地點排序';
$Lang['eEnrolment']['course']['warning']['afterEndDate'] = '上課日期必須於入學期最後一天或之前';
$Lang['eEnrolment']['course']['warning']['beforeStartDate'] = '上課日期必須於入學期首天或以後';
$Lang['eEnrolment']['course']['warning']['confirmDeleteCourse'] = '刪除課程時,如有相關的房間預定和課堂也會一併刪除,是否繼續?';
$Lang['eEnrolment']['course']['warning']['confirmDeleteLesson'] = '刪除課堂時,如有相關的房間預定也會一併刪除,是否繼續?';
$Lang['eEnrolment']['course']['warning']['mixupCourseInstructor'] = "最少有一個導師並非任教所選課程";
$Lang['eEnrolment']['course']['warning']['selectCourse'] = "請選擇課程";
$Lang['eEnrolment']['course']['warning']['selectLesson'] = "請選擇課堂";
$Lang['eEnrolment']['course']['warning']['selectStaff'] = "請選擇導師";
$Lang['eEnrolment']['course']['warning']['startTimeOutbound'] = "開始時間超出範圍";
$Lang['eEnrolment']['course']['warning']['timeConflict'] = "時間有衝突";
$Lang['eEnrolment']['course_tempMenu'] = '課程範本';
$Lang['eEnrolment']['course_temp']['template'] = '範本';
$Lang['eEnrolment']['course_temp']['sim'] = '以下為相似的項目';
$Lang['eEnrolment']['course_temp']['noSim'] = '找不到相似的項目';
$Lang['eEnrolment']['course_temp']['group']['Title'] = '課程名稱';
$Lang['eEnrolment']['course_temp']['group']['eventTotal'] = '活動總數';
$Lang['eEnrolment']['course_temp']['generate'] = '產生';
$Lang['eEnrolment']['course_temp']['totalDay'] = '總天數';
$Lang['eEnrolment']['course_temp']['event']['Title'] = '活動名稱';
$Lang['eEnrolment']['course_temp']['event']['Description'] = '活動簡介';
$Lang['eEnrolment']['course_temp']['period']['rangeError'] = '時間範圍錯誤';
$Lang['eEnrolment']['course_temp']['period']['label'] = '週期';
$Lang['eEnrolment']['course_temp']['period']['day'] = '天';
$Lang['eEnrolment']['course_temp']['period']['duration'] = '活動時段';
$Lang['eEnrolment']['course_temp']['result']['clubTotal'] = '已產生的學會總數';
$Lang['eEnrolment']['course_temp']['result']['eventTotal'] = '已產生的活動總數';
$Lang['eEnrolment']['curriculumTemplate']['allTemplate'] = ' - 所有範本 - ';
$Lang['eEnrolment']['curriculumTemplate']['categoryClassInstructor'] = '類別班別導師';
$Lang['eEnrolment']['curriculumTemplate']['course']['addClassCombinationAndInstructor'] = '班別組合與導師';
$Lang['eEnrolment']['curriculumTemplate']['course']['classCombinationAndInstructor'] = '班別組合與導師';
$Lang['eEnrolment']['curriculumTemplate']['course']['combinedLesson'] = '合併課程';
$Lang['eEnrolment']['curriculumTemplate']['course']['category'] = '類別';
$Lang['eEnrolment']['curriculumTemplate']['course']['class'] = '班別 (人數)';
$Lang['eEnrolment']['curriculumTemplate']['course']['code'] = '課程代號';
$Lang['eEnrolment']['curriculumTemplate']['course']['combinedCourse'] = '合併課程';
$Lang['eEnrolment']['curriculumTemplate']['course']['course'] = '子課程';
$Lang['eEnrolment']['curriculumTemplate']['course']['dateTime'] = '日期 / 時間';
$Lang['eEnrolment']['curriculumTemplate']['course']['description'] = '簡介';
$Lang['eEnrolment']['curriculumTemplate']['course']['lesson'] = '課堂';
$Lang['eEnrolment']['curriculumTemplate']['course']['lessonTitle'] = '課堂名稱';
$Lang['eEnrolment']['curriculumTemplate']['course']['nonCombinedCourse'] = '非合併課程';
$Lang['eEnrolment']['curriculumTemplate']['course']['numberOfLesson'] = '課堂數目';
$Lang['eEnrolment']['curriculumTemplate']['course']['removeClassAndInstructor'] = '移除班別及導師';
$Lang['eEnrolment']['curriculumTemplate']['course']['title'] = '子課程名稱';
$Lang['eEnrolment']['curriculumTemplate']['curriculum']['description'] = '簡介';
$Lang['eEnrolment']['curriculumTemplate']['curriculum']['details'] = '課程詳細資料';
$Lang['eEnrolment']['curriculumTemplate']['curriculum']['numberOfCourse'] = '子課程總數';
$Lang['eEnrolment']['curriculumTemplate']['curriculum']['template'] = '課程樣板';
$Lang['eEnrolment']['curriculumTemplate']['curriculum']['title'] = '課程名稱';
$Lang['eEnrolment']['curriculumTemplate']['dayNumber'] = '日序';
$Lang['eEnrolment']['curriculumTemplate']['generate'] = '產生';
$Lang['eEnrolment']['curriculumTemplate']['generateCurriculum'] = '產生課程';
$Lang['eEnrolment']['curriculumTemplate']['generateStep1'] = '選擇入學期';
$Lang['eEnrolment']['curriculumTemplate']['generateStep2'] = '確定';
$Lang['eEnrolment']['curriculumTemplate']['generateSuccess'] = '1|=|課程新增成功.';
$Lang['eEnrolment']['curriculumTemplate']['generateUnSuccess'] = '0|=|課程新增失敗.';
$Lang['eEnrolment']['curriculumTemplate']['intake']['addClass'] = '增加班別';
$Lang['eEnrolment']['curriculumTemplate']['intake']['class'] = '班別';
$Lang['eEnrolment']['curriculumTemplate']['intake']['className'] = '班別';
$Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'] = '導師';
$Lang['eEnrolment']['curriculumTemplate']['intake']['intake'] = '入學期';
$Lang['eEnrolment']['curriculumTemplate']['intake']['location'] = '位置';
$Lang['eEnrolment']['curriculumTemplate']['intake']['locationCapacity'] = '位置 (容量)';
$Lang['eEnrolment']['curriculumTemplate']['intake']['numberOfClass'] = '班別數目';
$Lang['eEnrolment']['curriculumTemplate']['intake']['numberOfStudent'] = '學生數目';
$Lang['eEnrolment']['curriculumTemplate']['intake']['removeClass'] = '移除班別';
$Lang['eEnrolment']['curriculumTemplate']['intake']['student'] = '學生';
$Lang['eEnrolment']['curriculumTemplate']['intake']['title'] = '入學期名稱';
$Lang['eEnrolment']['curriculumTemplate']['tabs']['generatedCurriculum'] = '產生的課程';
$Lang['eEnrolment']['curriculumTemplate']['tabs']['curriculumTemplate'] = '課程範本';
$Lang['eEnrolment']['curriculumTemplate']['title'] = '課程範本';
$Lang['eEnrolment']['curriculumTemplate']['to'] = '至';
$Lang['eEnrolment']['curriculumTemplate']['warning']['addClassAtLeastOneClass'] = '請輸入最少一個班別';
$Lang['eEnrolment']['curriculumTemplate']['warning']['changeToLessLesson'] = '是否確定轉為較少課堂?多餘的課堂會被刪除';
$Lang['eEnrolment']['curriculumTemplate']['warning']['confirmChangeIntake'] = '是否確定轉換入學期?以下合併課程的資料將會重設';
$Lang['eEnrolment']['curriculumTemplate']['warning']['confirmDeleteClass'] = '是否確定刪除該班別?';
$Lang['eEnrolment']['curriculumTemplate']['warning']['confirmGenerateCourse'] = '是否確定產生課程?';
$Lang['eEnrolment']['curriculumTemplate']['warning']['duplicateClassSelection'] = '班別選擇重複了';
$Lang['eEnrolment']['curriculumTemplate']['warning']['inputClassName'] = '請輸入班別名稱';
$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseDescription'] = '請輸入課程簡介';
$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseCode'] = '請輸入課程代號';
$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseTitle'] = '請輸入課程名稱';
$Lang['eEnrolment']['curriculumTemplate']['warning']['inputIntakeTitle'] = '請輸入入學期名稱';
$Lang['eEnrolment']['curriculumTemplate']['warning']['inputLessonTitle'] = '請輸入課堂名稱';
$Lang['eEnrolment']['curriculumTemplate']['warning']['inputTitleChinese'] = '請輸入課程範本的中文名稱';
$Lang['eEnrolment']['curriculumTemplate']['warning']['inputTitleEnglish'] = '請輸入課程範本的英文名稱';
$Lang['eEnrolment']['curriculumTemplate']['warning']['instructorIsUnavailable'] = '導師請了假';
$Lang['eEnrolment']['curriculumTemplate']['warning']['locationIsBookedByOthers'] = '所選地點已被人預約';
$Lang['eEnrolment']['curriculumTemplate']['warning']['locationOrTimeNotAvailable'] = '所選地點及時段不可預訂';
$Lang['eEnrolment']['curriculumTemplate']['warning']['noAvailableLocation'] = '沒有合適的地點';
$Lang['eEnrolment']['curriculumTemplate']['warning']['reselectLocationOrTime'] = '請重新選擇位置或日期/時間';
$Lang['eEnrolment']['curriculumTemplate']['warning']['selectAtLeastOneClass'] = '請選擇最少一個班別';
$Lang['eEnrolment']['curriculumTemplate']['warning']['selectCombinedCourse'] = '請選擇是否合併課程';
$Lang['eEnrolment']['curriculumTemplate']['warning']['selectCourseCategory'] = '請選擇課程類別';
$Lang['eEnrolment']['curriculumTemplate']['warning']['selectInstructor'] = '請選擇導師';
$Lang['eEnrolment']['curriculumTemplate']['warning']['selectIntake'] = '請選擇入學期';
$Lang['eEnrolment']['curriculumTemplate']['warning']['selectLocation'] = '請選擇位置';
$Lang['eEnrolment']['curriculumTemplate']['warning']['selectNumberOfLesson'] = '請選擇課堂數目';
$Lang['eEnrolment']['curriculumTemplate']['warning']['selectNumberOfStudent'] = '請選擇學生數目';
$Lang['eEnrolment']['curriculumTemplate']['warning']['startDateGtEndDate'] = '開始日期不能早於結束日期';
$Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflict'] = '導師的時間有衝突';
$Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictClass'] = '與現有紀錄的時間有衝突: ';
$Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictInstructor'] = '導師的時間有衝突';
$Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictLesson'] = '與現有紀錄的時間有衝突';
$Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictUnknown'] = '時間有衝突';
$Lang['eEnrolment']['curriculumTemplate']['weekNumber'] = '週序';
$Lang['eEnrolment']['instructor']['left'] = '* 已離職導師';
$Lang['eEnrolment']['MeetingDateCycle']['Choice'] = '類別選擇';
$Lang['eEnrolment']['MeetingDateCycle']['Period'] = '重複';
$Lang['eEnrolment']['MeetingDateCycle']['Cycle'] = '週期';
$Lang['eEnrolment']['report']['classWeeklySchedule']['title'] = '%s %s (由%s開始的星期)';
$Lang['eEnrolment']['report']['displayType'] = '顯示模式';
$Lang['eEnrolment']['report']['eventOrLesson'] = '事件或課堂';
$Lang['eEnrolment']['report']['location'] = '位置';
$Lang['eEnrolment']['report']['selectClass'] = '-- 選擇班別 --';
$Lang['eEnrolment']['report']['selectIntake'] = '-- 選擇入學期 --';
$Lang['eEnrolment']['report']['staff'] = '導師';
$Lang['eEnrolment']['report']['staffTeachingSummary']['nrCourse'] = '課程數目';
$Lang['eEnrolment']['report']['staffTeachingSummary']['nrLesson'] = '課堂數目';
$Lang['eEnrolment']['report']['subHeading'] = '列印副題';
$Lang['eEnrolment']['report']['type']['detail'] = '詳細資料';
$Lang['eEnrolment']['report']['type']['summary'] = '摘要';
$Lang['eEnrolment']['report']['warning']['inputPeriod'] = '請選擇時期';
$Lang['eEnrolment']['report']['warning']['selectClass'] = '請選擇班別';
$Lang['eEnrolment']['report']['warning']['selectDisplayType'] = '請選擇顯示類型';
$Lang['eEnrolment']['report']['warning']['selectInstructor'] = '請選擇導師';
$Lang['eEnrolment']['report']['warning']['selectCorrectTime'] = '請選擇正確的日期範圍';
$Lang['eEnrolment']['report']['warning']['selectTimeRange'] = '請選擇時間範圍';
$Lang['eEnrolment']['report']['warning']['startDateGtEndDate'] = '開始日期不能早於結束日期';
$Lang['eEnrolment']['report']['weekday'][0] = "星期日";
$Lang['eEnrolment']['report']['weekday'][1] = "星期一";
$Lang['eEnrolment']['report']['weekday'][2] = "星期二";
$Lang['eEnrolment']['report']['weekday'][3] = "星期三";
$Lang['eEnrolment']['report']['weekday'][4] = "星期四";
$Lang['eEnrolment']['report']['weekday'][5] = "星期五";
$Lang['eEnrolment']['report']['weekday'][6] = "星期六";
$Lang['eEnrolment']['settings']['timeLimitSetting']['instruction'] = '請於日曆內選擇導師不能教授的日期。當日期被選取後，系統便會自動顯示相應時槽以供設定。';
$Lang['eEnrolment']['settings']['timeLimitSetting']['selectStaffInstruction'] = '請選擇導師';
$Lang['eEnrolment']['settings']['timeLimitSetting']['instructorCannotTeachDate'] = '設定導師不能任教的日期';
$Lang['eEnrolment']['settings']['timeLimitSetting']['instructorCannotTeachTime'] = '設定導師不能任教的時段';
$Lang['eEnrolment']['settings']['timeLimitSetting']['title'] = '時間限制設定';
$Lang['eEnrolment']['ClassTrainingSummary']['ReportDetail'] = '班別訓練排程詳情';
$Lang['eEnrolment']['ClassTrainingSummary']['ReportName'] = '班別訓練排程摘要';
$Lang['eEnrolment']['ClassWeeklySchedule']['ReportName'] = '班別每週排程表';
$Lang['eEnrolment']['StaffTeachingSummary']['ReportDetail'] ='導師教學排程詳情';
$Lang['eEnrolment']['StaffTeachingSummary']['ReportName'] ='導師教學排程摘要';

#lslp
if($plugin['lslp'])
{
	$Lang['lslp']['lslp'] = "通識學習平台";
}

# iAccount > student official photo


# iAccount > student profile
$Lang['iAccount']['InvalidStudentData'] = "學生資料無效";
$Lang['iAccount']['AccountOverview'] = "戶口概況";
$Lang['iAccount']['EnrolmentAttendanceOverview'] = "活動出席概況";
$Lang['iAccount']['StudentPerformance'] = "課堂表現記錄";
$Lang['iAccount']['StudentOfficialPhoto'] = "學生相片";
$Lang['iAccount']['StudentPerformance_viewType'] = "檢視類型";
$Lang['iAccount']['StudentPerformance_rawData'] = "原始資料";
$Lang['iAccount']['StudentPerformance_statistics'] = "統計資料";
$Lang['iAccount']['StudentPerformance_STSummary'] = "老師評分資料";

# iAccount > Timetable
$Lang['iAccount']['Timetable']['IndividualTimetable'] = "個人時間表";
$Lang['iAccount']['Timetable']['ClassTimetable'] = "班別時間表";
$Lang['iAccount']['Timetable']['SubjectGroupTimetable'] = "科組時間表";
$Lang['iAccount']['Timetable']['TodayDayRemarks'] = "附有「<span class='tabletextrequire'>*</span>」的欄為今天的課堂。";
$Lang['iAccount']['Timetable']['TimeZoomRemarks'] = "- 系統只能顯示當前時區";
$Lang['iAccount']['ContactInfo']['AlertChangeEmail'] = "請修改電子郵件地址，以便於閣下忘記密碼時，收到由系統傳送的重設密碼通知。";

$Lang['eDiscipline']['CaterPriority'] = "優先次序先決";

### Discipline v12
if($plugin['Disciplinev12'])
{
	$Lang['eDiscipline']['DisplayMode'] = "顯示形式";
	$Lang['eDiscipline']['DisplayHorizontal'] = "水平";
	$Lang['eDiscipline']['DisplayVertical'] = "垂直";

	$Lang['eDiscipline']['ActivityScore'] = "活動分";
	$Lang['eDiscipline']['AutoFillIn'] = "自動填充項目";
	$Lang['eDiscipline']['Insert'] = "加入";
	$Lang['eDiscipline']['DeleteLog'] = "資料刪除紀錄";
	$Lang['eDiscipline']['DeletedDate'] = "刪除日期";
	$Lang['eDiscipline']['DeletedBy'] = "刪除者";
	$Lang['eDiscipline']['RecordInfo'] = "紀錄資料";
	$Lang['eDiscipline']['RecordType'] = "紀錄類別";
	$Lang['eDiscipline']['Others'] = "其他";
	$Lang['eDiscipline']['DeleteRecordsInDateRange'] = "刪除所選日期範圍內的所有紀錄";
	$Lang['eDiscipline']['ConfirmDeleteRecordsInDateRange'] = "確定要刪除所選日期範圍內的所有紀錄?";
	$Lang['eDiscipline']['ButtonUnselectAll'] = "取消全選";
	$Lang['eDiscipline']['Month'] = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
	$Lang['eDiscipline']['Month2'] = array('','一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月');
	# eDis - Chinese Number - Copy from EJ
	$Lang['eDiscipline']['IssueDateChi']['Year'] = "年";
	$Lang['eDiscipline']['IssueDateChi']['Month'] = "月";
	$Lang['eDiscipline']['IssueDateChi']['Day'] = "日";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][0] = "零";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][1] = "一";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][2] = "二";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][3] = "三";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][4] = "四";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][5] = "五";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][6] = "六";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][7] = "七";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][8] = "八";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][9] = "九";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][10] = "十";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][20] = "二十";
	$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][30] = "三十";

	$Lang['eDiscipline']['CustomizedReports'] = "自定報告";
	$Lang['eDiscipline']['SchoolTermNotYetSet'] = "尚未設定學期。";
	$Lang['eDiscipline']['PeriodCannotAcrossSchoolTerm'] = "時段不可跨越不同學期。";
	$Lang['eDiscipline']['PeriodCannotAcrossSchoolYear'] = "時段不可跨越不同學年。";

	# 天主教慈幼會伍少梅中學 cust report (Award & Punishment Monthly report)
	$Lang['eDiscipline']['MonthlyAwardPunishmentReport'] = "品行報告表";
	$Lang['eDiscipline']['YearlyAwardPunishmentReport'] = "品行年度報告表";
	$Lang['eDiscipline']['sdbnsm_SchoolName'] = "SDB Ng Siu Mui Secondary School 天主教慈幼會伍少梅中學";
	$Lang['eDiscipline']['sdbnsm_ap_report'] = "品行報告表";
	$Lang['eDiscipline']['sdbnsm_ap_item'] = "品行評註項目";
	$Lang['eDiscipline']['sdbnsm_punish_A'] = "(甲項)記過";
	$Lang['eDiscipline']['sdbnsm_award_B'] = "(乙項)記功";
	$Lang['eDiscipline']['sdbnsm_punish'] = "記過";
	$Lang['eDiscipline']['sdbnsm_award'] = "記功";
	$Lang['eDiscipline']['sdbnsm_times'] = "次數";
	$Lang['eDiscipline']['sdbnsm_times_2'] = "次";
	$Lang['eDiscipline']['sdbnsm_scoreChange'] = "減分";
	$Lang['eDiscipline']['sdbnsm_ap_list'] = "功過欄";
	$Lang['eDiscipline']['sdbnsm_record_date'] = "日期";
	$Lang['eDiscipline']['sdbnsm_item'] = "事項";
	$Lang['eDiscipline']['sdbnsm_teacher_signature'] = "教師簽署";
	$Lang['eDiscipline']['sdbnsm_parent_signature'] = "家長簽署";
	$Lang['eDiscipline']['sdbnsm_baseMark_1'] = "每月基本品行分為";
	$Lang['eDiscipline']['sdbnsm_baseMark_2'] = "分";
	$Lang['eDiscipline']['sdbnsm_absent'] = "缺席";
	$Lang['eDiscipline']['sdbnsm_half_day'] = "半天";
	$Lang['eDiscipline']['sdbnsm_whole_day'] = "全天";
	$Lang['eDiscipline']['sdbnsm_whole_year'] = "全年";
	$Lang['eDiscipline']['sdbnsm_late'] = "遲到";
	$Lang['eDiscipline']['sdbnsm_earlyLeave'] = "早退";
	$Lang['eDiscipline']['sdbnsm_conduct_mark'] = "品行分";
	$Lang['eDiscipline']['sdbnsm_conduct_grade'] = "品行等級";
	$Lang['eDiscipline']['sdbnsm_classTeacher_signature'] = "班主任簽署";
	$Lang['eDiscipline']['sdbnsm_guidance_signature'] = "監護人簽署";
	$Lang['eDiscipline']['sdbnsm_teacher_comment'] = "班主任評語";
	$Lang['eDiscipline']['sdbnsm_ap_yearly_report'] = "品行年度報告表";
	### end of cust

	$Lang['eDiscipline']['Gain'] = "增加";
	$Lang['eDiscipline']['Deduct'] = "扣減";
	$Lang['eDiscipline']['ChangeReasonNotice'] = "系統乃根據通告\\\"發放原因\\\"提供相關之自動填充項目。更改\\\"發放原因\\\"將引致部份先前插入的自動填充項目無法顯示。";

	$eDiscipline["Websams_Transition_Step2_Instruction1"] = "系統只會傳送 <i>已經批核</i> 及 <i>已經開放</i> 的獎懲紀錄。";

	# 靈糧怡文  cust
	$Lang['eDiscipline']['FollowUp'] = "跟進事項";
	$Lang['eDiscipline']['FollowUpBlank'] = "請輸入跟進事項";
	$Lang['eDiscipline']['FollowUpDuplicate'] = "跟進事項重複";
	$Lang['eDiscipline']['ConfirmDeleteFollowUp'] = '你是否決定刪除這跟進事項?';
	$Lang['eDiscipline']['Completed'] = "完成";
	$Lang['eDiscipline']['InProgress'] = "進行中";
	$Lang['eDiscipline']['Times2'] = "次數";
	$Lang['eDiscipline']['ChangeFollowUpActionWarning'] = "如更改跟進事項, 跟進次數將會被重設為 0。 確定更改?";
	$Lang['eDiscipline']['FollowUpTimes'] = "跟進次數";
	$Lang['eDiscipline']['LastFollowUpBy'] = "最後跟進";
	$Lang['eDiscipline']['AddFollowUp1'] = "增加跟進次數 1 次";
	$Lang['eDiscipline']['AllFollowUpTimes'] = "所有跟進次數";
	$Lang['eDiscipline']['AddPunishmentForSelectedStudent'] = "新增懲罰紀錄于已選擇的學生";
	$Lang['eDiscipline']['select_student'] = "請最少選擇一位學生。";
	$Lang['eDiscipline']['TodayFollowUpList'] = "今天跟進名單";
	$Lang['eDiscipline']['FollowUpRecord'] = "事由";
	$Lang['eDiscipline']['FollowUpLetter'] = "有催信";
	$Lang['eDiscipline']['DisciplineTeam'] = "已到違規隊";
	$Lang['eDiscipline']['FollowUpChecked'] = "覆檢OK";
	$Lang['eDiscipline']['NewPunishmentRecord'] = "新增之違規記錄";
	$Lang['eDiscipline']['FollowUpStatus'] = "進度";
	$Lang['eDiscipline']['DefaultFollowUp'] = "預設跟進事項";

	# Data Transfer (to WebSAMS)
	$Lang['eDiscipline']['SLPReadIndicate'] = "於網上校管系統學生學習概覽顯示所傳送之獎懲紀錄";
	$Lang['eDiscipline']['ApplicableToWebSAMS2.0Only'] = "只適用於網上校管系統 v2.0";
	$Lang['eDiscipline']['SupportWebSAMSVersionStatement'] = "請注意，系統目前只支援傳送紀錄至WebSAMS2.0.1版本。";
	$Lang['eDiscipline']['Disclaimer'] = "免責聲明";
	$Lang['eDiscipline']['DisclaimerArr'][] = "鑒於WebSAMS可能作出系統變更，而該等變更並不屬於本公司可掌握範圍之內。因此，我們無法保證所傳送的資料能夠於任何時間均正確顯示。";
	$Lang['eDiscipline']['DisclaimerArr'][] = "系統目前只支援傳送紀錄至WebSAMS2.0.1版本。";
	$Lang['eDiscipline']['AgreeToUseDataTransfer'] = "我明白使用資料傳送之風險及我同意現在傳送資料至WebSAMS";
	$Lang['eDiscipline']['FailedToAgreeDisclaimer'] = "0|=|系統未能處理同意免責聲明";
	$Lang['eDiscipline']['NotYetAgreeDisclaimer'] = "你尚未同意免責聲明，系統將轉至免責聲明版面。";

	# System Properties
	$Lang['eDiscipline']['SysProperties']['SysProperties'] = "系統特性";
	$Lang['eDiscipline']['SysProperties']['AccessRights'] = "存取權限";
	$Lang['eDiscipline']['SysProperties']['General'] = "一般";
	$Lang['eDiscipline']['SysProperties']['DisciplineRecord'] = "操行紀錄";
	$Lang['eDiscipline']['SysProperties']['Detention'] = "留堂";
	$Lang['eDiscipline']['SysProperties']['Report'] = "報告";

	$Lang['eDiscipline']['MasterReport'] = "綜合報告";
	$Lang['eDiscipline']['GoodConductMisconductReport'] = "良好及違規行為報告";
	$Lang['eDiscipline']['SystemReport'] = "系統報告";
	$Lang['eDiscipline']['GMReport'] = "良好及違規行為報告";
	$Lang['eDiscipline']['GMRankingReport'] = "良好及違規行為排名";
	$Lang['eDiscipline']['AwardPunishmentReport'] = "獎勵及懲罰報告";
	$Lang['eDiscipline']['APReport'] = "獎勵及懲罰報告";
	$Lang['eDiscipline']['APRankingReport'] = "獎勵及懲罰排名";
	$Lang['eDiscipline']['AP_Detail_Ranking_Report'] = "獎勵及懲罰排名詳細報告";
	$Lang['eDiscipline']['GMDailyReport'] = "每天班級累積違規報告";
	$Lang['eDiscipline']['AP_Class_Report'] = "獎勵及懲罰班別報告";
	$Lang['eDiscipline']['AP_Class_Report_short'] = "班別報告";
	$Lang['eDiscipline']['DateRange'] = "日期範圍";
	$Lang['eDiscipline']['TotalConductScore'] = "總操行分";
	$Lang['eDiscipline']['AccumulatedConductScore'] = "累積操行分";
	$Lang['eDiscipline']['TotalSubScore'] = "總勤學分";
	$Lang['eDiscipline']['SubScoreBaseMark'] = "勤學分底分";
	$Lang['eDiscipline']['TotalNoStudent'] = "學生總數";

	$Lang['eDiscipline']['StudyScoreIncrement_MAX'] = "勤學分調整上限";
	$Lang['eDiscipline']['UseDefaultSettings'] = "使用預設設定";

	# TWGHs Lee Ching Dea Memorial College [CRM Ref NO.: 2011-0831-1742-14126]
	$Lang['eDiscipline']['TermConductReport'] = "學期操行報告";
	$Lang['eDiscipline']['APConductReport_ExportHeader'][0] = array("WebSAMS 註冊號碼", "級別", "班別", "班號", "學生姓名", "上學期操行優點", "上學期服務優點", "上學期課外活動優點", "上學期勤學優點", "上學期大過", "上學期小過", "上學期缺點", "上學期操行", "下學期操行優點", "下學期服務優點", "下學期課外活動優點", "下學期勤學優點", "下學期大過", "下學期小過", "下學期缺點", "下學期操行");
	$Lang['eDiscipline']['APConductReport_ExportHeader'][1] = array("WebSAMS Code", "Form", "Class", "Class Number", "Student Name", "Conduct", "T2_Cond_Merit", "Service", "ECA", "Diligence", "Major_Demerit", "Demerit", "Minor_Demerit", "T1_Conduct", "T2_Service", "T2_ECA", "T2_Diligence", "T2_Major Demerit", "T2_Demerit", "T2_Minor_Demerit", "T2_Conduct");

	$Lang['eDiscipline']['NotToShowStudentWithoutRecord'] = "不顯示沒有紀錄的學生";
	$Lang['eDiscipline']['CurrentYearTotalRecord'] = "本學年紀綠總數";
	$Lang['eDiscipline']['CurrentSemesterTotalRecord'] = "本學期紀綠總數";
	$Lang['eDiscipline']['MaximumGainDeductMark']= "最高可增加 / 扣除分數";
	$Lang['eDiscipline']['MaximumDeductMark']= "最高可扣除分數";
	$Lang['eDiscipline']['MaximumGainMark']= "最高可增加分數";

	$Lang['eDiscipline']['FileNumber'] = "個案編號";
	$Lang['eDiscipline']['RecordNumber'] = "紀錄編號";

	$Lang['eDiscipline']['PushMessage']['DetentionAbsent']['Title']= "學生缺席留堂通知
Student Absent for Detention Reminder";
	$Lang['eDiscipline']['PushMessage']['DetentionAbsent']['Content'] = "學生 __StudentChiName__ ( __ClassName__ - __ClassNo__) 缺席 __Date__ 由 __StartTime__ 至 __EndTime__ 的留堂班。 如有疑問，歡迎向  __TeacherName__查詢。
Student __StudentEngName__ ( __ClassName__ - __ClassNo__) is absent from detention class on __Date__ form __StartTime__ to __EndTime__. 
For any enquiries, please feel free to contact __TeacherName__.";
    $Lang['eDiscipline']['PushMessage']['DetentionAbsent']['Content2'] = "學生 __StudentChiName__ ( __ClassName__ - __ClassNo__) 缺席 __Date__ 由 __StartTime__ 至 __EndTime__ 的留堂班。 如有疑問，歡迎向  __TeacherChiName__查詢。
Student __StudentEngName__ ( __ClassName__ - __ClassNo__) is absent from detention class on __Date__ form __StartTime__ to __EndTime__. 
For any enquiries, please feel free to contact __TeacherEngName__.";


};
$Lang['eDiscipline']['JSWarning']['PleaseDoNotSelectMoreThan10Records'] = "請勿選擇超過10頂記錄";
//$Lang['eDiscipline']['JSWarning']['NoMoreVacancy'] = "地點餘額不足";
$Lang['eDiscipline']['JSWarning']['NoMoreVacancy'] = "地點名額不足";
$Lang['eDiscipline']['JSWarning']['SessionDuplicate'] = "時段重複 / 重疊";
$Lang['eDiscipline']['FieldTitle']['DetentionTimes'] = "留堂次數";
$Lang['eDiscipline']['FieldTitle']['DetentionSession'] = "時段";
$Lang['eDiscipline']['FieldTitle']['DetentionReason'] = "原因";
$Lang['eDiscipline']['FieldTitle']['DetentionRemark'] = "備註";
$Lang['eDiscipline']['FieldTitle']['LastDetentionSession'] = "上一次時段";
$Lang['eDiscipline']['FieldTitle']['DetentionAbsentRemark'] = "缺席備註";
$Lang['eDiscipline']['FieldTitle']['LastAbsentDetails'] = "上一次缺席資料";
$Lang['eDiscipline']['FieldTitle']['NumOfStudentsAssignedInThisSession'] = "已分配到此時段的學生人數";

$Lang['eDiscipline']['FieldTitle']['CalculationMethod'] = "操行分累計週期";
$Lang['eDiscipline']['FieldTitle']['StudyScoreCalculationMethod'] = "勤學分累計週期";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByTerm'] = "每個學期重新累計";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByAcademicYear'] = "每個學年重新累計";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['SystemGenerated'] = "系統管理的操行分";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ManuallyAdjusted'] = "手動調整總計 (累計週期內)";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ConsolidatedMark'] = "最終得分";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['TotalGain'] = "累積加分";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['TotalDeduct'] = "累積扣分";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['NoRecordAtThisTerm'] = "本學期暫時仍未有任何記錄";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['OnlyAffectTheCurrentRecord'] = "更改底分將只會影響本學年的操行分紀錄。<br>你可於更改底分後到<b>管理>操行分</b>驗證分數變更。";
$Lang['eDiscipline']['FieldTitle']['SelectSchoolYear'] = " - 選擇學年 - ";
$Lang['eDiscipline']['FieldTitle']['SelectTerm'] = " - 選擇學期 - ";
$Lang['eDiscipline']['FieldTitle']['LastUpdated'] = "最後更新";
$Lang['eDiscipline']['FieldTitle']['RelatedItems'] = "相關物品";
$Lang['eDiscipline']['FieldTitle']['StudyScoreBaseMark'] = "底分";
$Lang['eDiscipline']['jsStudyScoreEditWarning'] = "不論結餘, 所有勤學分數將會統一更新成所輸入數目。你是否確定要繼續?";
$Lang['eDiscipline']['GenerateGradeOfStudyScoreInstruction'] = "更改評級準則後，請前往<b>管理 > 勤學分</b>，重新計算操行評級。";
$Lang['eDiscipline']['StudyScoreLowerBoundary'] = "勤學分下限";
$Lang['eDiscipline']['ComputeStudyScoreGrade'] = "計算勤學評級";
$Lang['eDiscipline']['StudyScoreGrade'] = "勤學等級";
$Lang['eDiscipline']['SomeCategoriesAlreadyInUse'] = "0|=|有已選的類別已在使用中";
$Lang['eDiscipline']['SomeItemsAlreadyInUse'] = "0|=|有已選的項目已在使用中";

$Lang['eDiscipline']['Warning'] = "警告";
$Lang['eDiscipline']['Duplicated'] = "重覆";

# eDiscipline error msg
$Lang['eDiscipline']['ReturnMessage']['DuplicateItemName'] = '0|=|該類別名稱已經被使用過，請重新輸入。';

$Lang['eDiscipline']['Import']['Mode'] = "匯入模式";
$Lang['eDiscipline']['Import']['ImportNew'] ="匯入新項目";
$Lang['eDiscipline']['Import']['UpdateExisting'] ="更新現有項目";
$Lang['eDiscipline']['Import']['ExistingItemCode'] ="現有項目編號";



### Event Type
$Lang['EventType']['School'] = "學校事項";
$Lang['EventType']['Academic'] = "教學事項";
$Lang['EventType']['Group'] = "小組事項";
$Lang['EventType']['PublicHoliday'] = "公眾假期";
$Lang['EventType']['SchoolHoliday'] = "學校假期";

### eClass Update in admin console
$i_adminmenu_sc_eclass_update = "eClass更新";
$i_manual_update = "手動更新";
$i_auto_update = "自動更新";
$i_update_report = "更新報告";
$i_update_islatest = "你已經使用最新版本。";
$i_update_notlatest = "你可以更新到新版本。";
$i_eu_welcome_msg = "歡迎使用eClass自動更新服務!!";
$i_Maintenace_Status = "你的系統保養狀況";
$i_eu_not_activated = "你的 eclass 伺服器並未啟動eClass自動更新服務.<br><br>請聯絡我們的客戶服務部, 電郵: support@broadlearning.com";
$i_System_Status = "系統更新狀況";
$i_eu_submit_wizard = "請按\"呈送\"進行下一步。";
$i_eu_contact_cs = "請聯絡我們的客戶服務部, 電郵: support@broadlearning.com";
$i_eu_check_connection_pass = "正在測試與更新伺服器的連線：已連線!";
$i_eu_check_connection_fail = "正在測試與更新伺服器的連線：未能連線!";
$i_eu_check_space_pass ="正在檢查儲存空間：通過!";
$i_eu_check_space_fail ="正在檢查儲存空間：儲存空間不足!";
$i_eu_now_will_up = "你的系統將作如下更新";
$i_eu_from_ver = "由版本: ";
$i_eu_to_ver = "到版本: ";
$i_eu_next_msg = "按\"下一步\"繼續更新程序。前往其他頁面可取消更新。";
$i_eu_next_button = "下一步";
$i_eu_download_msg="正在下載更新包，請稍候!";
$i_eu_download_completed="更新包下載完成!";
$i_eu_download_next="按\"下一步\"繼續更新程序。";
$i_eu_download_progress="進度";
$i_eu_verify_msg="正在測試你的系統，請稍候!";
$i_eu_verifying = "測試中";
$i_eu_verify_done= "測試完成!";
$i_eu_extract_msg ="正在更新你的系統，請稍候!";
$i_eu_extract_msg2 ="打開更新包...";
$i_eu_update = "正在更新你的內聯網資料庫";
$i_eu_update2 = "正在更新你的教室資料庫";
$i_eu_success = "成功";
$i_eu_final = "你可以前往以下網頁，了解版本更新資訊。";
$i_eu_final2 = "系統更新完成，你的系統已更新到最新版本。";
$i_eu_summary = "eClass 系統更新摘要";
$i_eu_latest_ver = "系統最新版本";
$i_eu_current_ver ="當前系統版本";
$i_eu_error = "錯誤：系統無法更新";
$i_eu_error2 = "請電郵到support@broadlearning.com，並引用以上錯誤訊息。";
$Lang['eu']['notice'] = "為免影響eClass系統的運作及學校日常的使用(如登入及上載檔案等)，請確保伺服器有足夠的硬碟空間！";
$Lang['eu']['db_space'] = "MySql 硬碟空間情況：";
$Lang['eu']['eclass_space'] = "eClass 硬碟空間情況：";
$Lang['eu']['space_left'] = "剩餘";

$Lang['SysMgr']['Homework']['Management'] = "管理";
$Lang['SysMgr']['Homework']['HomeworkList'] = "家課紀錄表";
$Lang['SysMgr']['Homework']['SubjectLeader'] = "科長";
$Lang['SysMgr']['Homework']['HomeworkImage'] = "家課表圖片";
$Lang['SysMgr']['Homework']['Reports'] = "報告";
$Lang['SysMgr']['Homework']['WeeklyHomeworkList'] = "一週家課紀錄表";
$Lang['SysMgr']['Homework']['NotSubmitList'] = "欠交家課清單";
$Lang['SysMgr']['Homework']['NotSubmitRecord'] = "欠交功課記錄報告";
$Lang['SysMgr']['Homework']['NotSubmitDate'] = "欠交功課記錄日期";
$Lang['SysMgr']['Homework']['InputNotSubmitRecord'] = "所輸入之欠交功課記錄";
$Lang['SysMgr']['Homework']['ViolationRecord'] = "欠交違規記錄報告";
$Lang['SysMgr']['Homework']['ViolationDate'] = "欠交違規記錄日期";
$Lang['SysMgr']['Homework']['InputViolationRecord'] = "所輸入之欠交違規記錄";
$Lang['SysMgr']['Homework']['CancelViolationRecord'] = "取消欠交違規報告";
$Lang['SysMgr']['Homework']['CancelViolationRecordDate'] = "取消欠交違規日期";
$Lang['SysMgr']['Homework']['InputCancelViolationRecord'] = "所輸入之取消欠交違規記錄";
$Lang['SysMgr']['Homework']['HomeworkReport'] = "家課報告";
$Lang['SysMgr']['Homework']['ParentNotice'] = "欠交通知書";
$Lang['SysMgr']['Homework']['Statistics']= "統計";
$Lang['SysMgr']['Homework']['HomeworkStatistics'] = "科目統計";
$Lang['SysMgr']['Homework']['StatsByClass'] = "班別統計";
$Lang['SysMgr']['Homework']['StatsByMonth'] = "每月統計";
$Lang['SysMgr']['Homework']['Settings'] = "設定";
$Lang['SysMgr']['Homework']['BasicSettings'] = "基本設定";
$Lang['SysMgr']['Homework']['HandInStatusValue'] = "繳交狀況值";
$Lang['SysMgr']['Homework']['HandInStatusSettings'] = "繳交狀況設定";
$Lang['SysMgr']['Homework']['HomeworkType'] = "家課類型";
$Lang['SysMgr']['Homework']['AdminSettings'] = "管理權限設定";
$Lang['SysMgr']['Homework']['Record'] = "紀錄";
$Lang['SysMgr']['Homework']['NewRecord'] = "新增紀錄";
$Lang['SysMgr']['Homework']['ImportRecord'] = "匯入紀錄";
$Lang['SysMgr']['Homework']['Edit'] = "編輯";
$Lang['SysMgr']['Homework']['EditRecord'] = "編輯紀錄";
$Lang['SysMgr']['Homework']['AssignSubjectLeader'] = "委任科長";
$Lang['SysMgr']['Homework']['ImportSourceFile'] = "資料檔案";
$Lang['SysMgr']['Homework']['ImportSourceFileNote'] = "你可於 \"資料檔案\" 欄輸入CSV資料檔的路徑，或按\"瀏覽\"，從你的電腦中選取資料檔。";
$Lang['SysMgr']['Homework']['WeeklyDiary'] = "週記";
$Lang['SysMgr']['Homework']['NewspaperCutting'] = "剪報";
$Lang['SysMgr']['Homework']['WeeklyDiaryType'] = "類型";
$Lang['SysMgr']['Homework']['Single'] = "單次";
$Lang['SysMgr']['Homework']['Byweek'] = "每週";
$Lang['SysMgr']['Homework']['Choosethedeadline'] = "請選擇限期";
$Lang['SysMgr']['Homework']['LatestUpdate'] = "最後確認日期";
$Lang['SysMgr']['Homework']['Mon'] = "一";
$Lang['SysMgr']['Homework']['Tue'] = "二";
$Lang['SysMgr']['Homework']['Wed'] = "三";
$Lang['SysMgr']['Homework']['Thur'] = "四";
$Lang['SysMgr']['Homework']['Fri'] = "五";
$Lang['SysMgr']['Homework']['Sat'] = "六";
$Lang['SysMgr']['Homework']['Sun'] = "日";

$Lang['SysMgr']['Homework']['SubjectGroupCode'] = "學科組別代號";
//$Lang['SysMgr']['Homework']['SubjectGroupTitle'] = "學科組別標題";
$Lang['SysMgr']['Homework']['SubjectCode'] = "學科代號";
$Lang['SysMgr']['Homework']['Student'] = "學生";
$Lang['SysMgr']['Homework']['StudentName'] = "學生姓名";
$Lang['SysMgr']['Homework']['Children'] = "子女";
$Lang['SysMgr']['Homework']['AllStudents'] = "所有學生";
$Lang['SysMgr']['Homework']['Form'] = "班級";
$Lang['SysMgr']['Homework']['Form2'] = "年級";
$Lang['SysMgr']['Homework']['AllForms'] = "所有班級";
$Lang['SysMgr']['Homework']['Class'] = "班別";
$Lang['SysMgr']['Homework']['ClassNumber'] = "班號";
$Lang['SysMgr']['Homework']['Subject'] = "學科";
$Lang['SysMgr']['Homework']['Topic'] = "標題";
$Lang['SysMgr']['Homework']['Poster'] = "發出人";
$Lang['SysMgr']['Homework']['Workload'] = "工作量";
$Lang['SysMgr']['Homework']['TotalWorkload'] = "總工作量";
$Lang['SysMgr']['Homework']['Hours'] = "小時";
$Lang['SysMgr']['Homework']['Teacher'] = "老師";
$Lang['SysMgr']['Homework']['AllTeachers'] = "所有老師";
$Lang['SysMgr']['Homework']['StartDate']="開始日期";
$Lang['SysMgr']['Homework']['DueDate'] = "限期";
$Lang['SysMgr']['Homework']['DueDateForViolation'] = "欠交功課日期";
$Lang['SysMgr']['Homework']['Description'] = "內容";
$Lang['SysMgr']['Homework']['Attachment'] = "附件";
$Lang['SysMgr']['Homework']['NoAttachment'] = "沒有附件";
$Lang['SysMgr']['Homework']['HandinRequired'] = "須繳交";
$Lang['SysMgr']['Homework']['HandinStatus'] = "狀況";
$Lang['SysMgr']['Homework']['LastModified'] = "最後更新";
$Lang['SysMgr']['Homework']['Status'] = "現在狀況";;
$Lang['SysMgr']['Homework']['Error'] = "錯誤";
$Lang['SysMgr']['Homework']['TotalViolation'] = "總欠交功課數目";
$Lang['SysMgr']['Homework']['CancelViolationDate'] = "取消記欠交記錄日期";
$Lang['SysMgr']['Homework']['LatestConfirmDate'] = "已交確認日期";

$Lang['SysMgr']['Homework']['SearchAlert'] = "請輸入內容";
$Lang['SysMgr']['Homework']['ToDoList'] = "家課清單";
$Lang['SysMgr']['Homework']['History'] = "紀錄";
$Lang['SysMgr']['Homework']['SubjectGroup'] = "學科組別";
$Lang['SysMgr']['Homework']['AllSubjects'] = "所有學科";
$Lang['SysMgr']['Homework']['AllSubjectGroups'] = "所有學科組別";
$Lang['SysMgr']['Homework']['HomeworkHandinlist'] = "家課繳交清單";
$Lang['SysMgr']['Homework']['Handin'] = "已繳交";
$Lang['SysMgr']['Homework']['Total'] = "總數";
$Lang['SysMgr']['Homework']['ClassTotal'] = "毎班總數";
$Lang['SysMgr']['Homework']['DayTotal'] = "毎日總數";
$Lang['SysMgr']['Homework']['MonthlyReportTitle'] = "[=MONTH=]份各科家課統計";

$Lang['SysMgr']['Homework']['NotSubmittedInput'] = "欠交輸入";
$Lang['SysMgr']['Homework']['SupplementaryRecord'] = "補交功課名單";
$Lang['SysMgr']['Homework']['DataTransferringForDis'] = "資料傳送至訓導管理";
$Lang['SysMgr']['Homework']['SubmittedInput'] = "交齊輸入";

$Lang['SysMgr']['Homework']['ImportOtherRecords'] = "匯入其他紀錄";
$Lang['SysMgr']['Homework']['PreviousWeek'] = "上一週";
$Lang['SysMgr']['Homework']['CurrentWeek'] = "本週";
$Lang['SysMgr']['Homework']['NextWeek'] = "下一週";
$Lang['SysMgr']['Homework']['NoRecord'] = "暫時仍未有任何紀錄";
$Lang['SysMgr']['Homework']['NotSubmitCount'] = "欠交次數";
$Lang['SysMgr']['Homework']['SupplementaryCount'] = "補交次數";
$Lang['SysMgr']['Homework']['From'] = "由";
$Lang['SysMgr']['Homework']['To'] = "至";
$Lang['SysMgr']['Homework']['HomeworkCount'] = "家課總數";
$Lang['SysMgr']['Homework']['RelatedSubject'] = "相關科目";
$Lang['SysMgr']['Homework']['Print'] = "列印";
$Lang['SysMgr']['Homework']['Export'] = "匯出";

$Lang['SysMgr']['Homework']['Disable'] = "不使用家課紀錄表";
$Lang['SysMgr']['Homework']['TeacherSearchDisabled'] = "不使用發出人名字搜尋";
$Lang['SysMgr']['Homework']['SubjectSearchDisabled'] = "不使用科目搜尋";
$Lang['SysMgr']['Homework']['SubjectsTaughtSearchDisabled'] = "不使用以教師任教職務搜尋";
$Lang['SysMgr']['Homework']['StartFixed'] = "只可設定今天家課";
$Lang['SysMgr']['Homework']['ParentAllowed'] = "允許家長檢視家課";
$Lang['SysMgr']['Homework']['NonTeachingAllowed'] = "非教學職務員工可以管理家課紀錄表 (所有學科組別)";
$Lang['SysMgr']['Homework']['TeachingRestricted'] = "教學職務員工只可新增任教科目及班別";
$Lang['SysMgr']['Homework']['AllowSubLeader'] = "容許科長輸入家課";
$Lang['SysMgr']['Homework']['AllowExport'] = "容許所有職員匯出家課";
$Lang['SysMgr']['Homework']['AllowPast'] = "容許輸入舊日紀錄";
$Lang['SysMgr']['Homework']['UseHomeworkCollect'] = "顯示是否需要收回有關功課";
$Lang['SysMgr']['Homework']['ClearAll'] = "清除所有家課紀錄";
$Lang['SysMgr']['Homework']['ToDateWrong'] = "限期必須是開始日期或以後";

$Lang['SysMgr']['Homework']['AcademicYear'] = "學年";
$Lang['SysMgr']['Homework']['YearTerm'] = "學期";
$Lang['SysMgr']['Homework']['AllYearTerms'] = "所有學期";

$Lang['SysMgr']['Homework']['LastModifiedBy'] = "最後修改老師";
$Lang['SysMgr']['Homework']['AllLastModifiedBy'] = "所有最後修改用戶";
$Lang['SysMgr']['Homework']['Submitted'] = "已交";
$Lang['SysMgr']['Homework']['NotSubmitted'] = "未交";
$Lang['SysMgr']['Homework']['ExpiredWithoutSubmission'] = "逾期未交";
$Lang['SysMgr']['Homework']['LateSubmitted'] = "遲交";
$Lang['SysMgr']['Homework']['UnderProcessing'] = "未處理";
$Lang['SysMgr']['Homework']['Redo'] = "重做";
$Lang['SysMgr']['Homework']['Supplementary'] = "補交";
$Lang['SysMgr']['Homework']['NoNeedSubmit'] = "不需繳交";
$Lang['SysMgr']['Homework']['SetAllToSubmitted'] = "全部轉為已交";
$Lang['SysMgr']['Homework']['SetAllToNotSubmitted'] = "全部轉為未交";
$Lang['SysMgr']['Homework']['SetAllToLateSubmitted'] = "全部轉為遲交";
$Lang['SysMgr']['Homework']['SetAllToRedo'] = "全部轉為重做";
$Lang['SysMgr']['Homework']['SetNotSubmittedToNoNeedSubmit'] = "把未繳交轉為不需繳交";
$Lang['SysMgr']['Homework']['SetExpiredWithoutSubmissionToNoNeedSubmit'] = "把逾期未交轉為不需繳交";
$Lang['SysMgr']['Homework']['AllSubmissionStatus'] = "全部繳交狀況";
$Lang['SysMgr']['Homework']['Title'] = "標題";
$Lang['SysMgr']['Homework']['Content'] = "內容";
$Lang['SysMgr']['Homework']['StudentHomework'] = "學生功課";
$Lang['SysMgr']['Homework']['StudentSubmiitedContent'] = "學生呈交內容";
$Lang['SysMgr']['Homework']['TeacherFeedback'] = "老師回饋";
$Lang['SysMgr']['Homework']['Mark/Grade'] = "分紙 / 等級";
$Lang['SysMgr']['Homework']['HasGraded'] = "已批改";
$Lang['SysMgr']['Homework']['NotYetGraded'] = "未批改";
$Lang['SysMgr']['Homework']['AllGradingStatus'] = "全部批改狀況";

$Lang['SysMgr']['Homework']['HomeworkListWarning'] = "家課紀錄表並不適用於本學期沒有任教任何科目的教師用戶。";
$Lang['SysMgr']['Homework']['SubjectLeaderWarning'] = "科長並不適用於本學期沒有任教任何科目的教師用戶。";
$Lang['SysMgr']['Homework']['DailyHomeworkListWarning'] = "每日家課紀錄表並不適用於本學期沒有任教任何科目的教師用戶。";
$Lang['SysMgr']['Homework']['WeeklyHomeworkListWarning'] = "一週家課紀錄表並不適用於本學期沒有任教任何科目的教師用戶。";
$Lang['SysMgr']['Homework']['NotSubmitListWarning'] = "欠交家課清單並不適用於本學期沒有任教任何科目的教師用戶。";

$Lang['SysMgr']['Homework']['HomeworkListWarningStudent'] = "家課紀錄表並不適用於本學期沒有就讀任何科目的學生用戶。";
$Lang['SysMgr']['Homework']['WeeklyHomeworkListWarningStudent'] = "一週家課紀錄表並不適用於本學期沒有就讀任何科目的學生用戶。";

$Lang['SysMgr']['Homework']['HomeworkListWarningSubjectLeader'] = "家課紀錄表並不適用於本學期不是科長的學生用戶。";
$Lang['SysMgr']['Homework']['DailyHomeworkListWarningSubjectLeader'] = "每日家課紀錄表並不適用於本學期不是科長的學生用戶。";
$Lang['SysMgr']['Homework']['WeeklyHomeworkListWarningSubjectLeader'] = "一週家課紀錄表並不適用於本學期不是科長的學生用戶。";

$Lang['SysMgr']['Homework']['HomeworkListWarningParent'] = "家課紀錄表並不適用於本學期子女沒有就讀任何科目的家長用戶。";
$Lang['SysMgr']['Homework']['WeeklyHomeworkListWarningParent'] = "一週家課紀錄表並不適用於本學期子女沒有就讀任何科目的家長用戶。";

$Lang['SysMgr']['Homework']['Remark']['EmptyFields']= "部份欄位沒有填上";
$Lang['SysMgr']['Homework']['Remark']['NoSubjectGroup'] = "沒有此學科組別";
$Lang['SysMgr']['Homework']['Remark']['NoSubject'] = "沒有此學科";
$Lang['SysMgr']['Homework']['Remark']['Handin'] = "請填上'yes'或'no'選擇是否需要繳交家課";
$Lang['SysMgr']['Homework']['Remark']['GroupTeacher'] = "此老師沒有任教此學科組別";
$Lang['SysMgr']['Homework']['Remark']['SubjectTeacher'] = "此老師沒有任教此學科";
$Lang['SysMgr']['Homework']['Remark']['noAccessRight'] = "此用戶沒有製作家課之權限";
$Lang['SysMgr']['Homework']['Remark']['StartDateError'] = "開始日期早於今天";
$Lang['SysMgr']['Homework']['Remark']['DueDateError'] = "限期早於開始日期";
$Lang['SysMgr']['Homework']['Remark']['NonTeachingStaff'] = "用戶不是老師";
$Lang['SysMgr']['Homework']['Remark']['InvalidUser'] = "用戶不存在";
$Lang['SysMgr']['Homework']['Remark']['SubjectGroupNotMatch'] = "學科組別不屬於此學科";
$Lang['SysMgr']['Homework']['Remark']['Collect'] = "請填上'yes'或'no'選擇是否需要收回有關功課";

$Lang['SysMgr']['Homework']['WeekDay'][]= "星期日";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期一";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期二";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期三";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期四";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期五";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期六";

$Lang['SysMgr']['Homework']['PleaseSelect']= "-- 請選擇 --";
$Lang['SysMgr']['Homework']['PleaseSelectClass']= "-- 請選擇班別 --";
$Lang['SysMgr']['Homework']['PleaseSelectSubject']= "-- 請選擇學科 --";
$Lang['SysMgr']['Homework']['PleaseSelectHomework']= "-- 請選擇家課 --";
$Lang['SysMgr']['Homework']['AllHandInStatus']= "-- 全部狀況 --";
$Lang['SysMgr']['Homework']['AllClass']= "-- 全部班別 --";
$Lang['SysMgr']['Homework']['AllSubject']= "-- 全部學科 --";
$Lang['SysMgr']['Homework']['Target'] ="對象";
$Lang['SysMgr']['Homework']['ViewSubjectLeaderByClass'] ="以班別檢視科長";
$Lang['SysMgr']['Homework']['SupplementaryList'] ="補交清單";
$Lang['SysMgr']['Homework']["Student"] = "學生";
$Lang['SysMgr']['Homework']["Class"] = "班別";

$Lang['SysMgr']['Homework']["PrintWithoutSudtentName"] = "列印 (隱藏學生名稱)";

$Lang['SysMgr']['Homework']["NoticeMsg1"] = "'當天缺席' 或 '派功課日缺席'狀況下而沒有選取補交狀況 ('已補交'、'欠補交'及'記欠交')，將不處理。";
$Lang['SysMgr']['Homework']["NoticeMsg2"] = "其他狀況下，將示作'欠補交'處理。";

$Lang['SysMgr']['Homework']['RequestTransferRecord'] = "更新後，請傳送至訓導管理。";
$Lang['SysMgr']['Homework']['RequestTransferMoreThanOneDayRecord'] = "重要提示：資料更新已超過一天，請先傳送至訓導管理。";
$Lang['SysMgr']['Homework']['CancelMisconductMsg'] = "確定要取消這個記錄'記欠交'狀況嗎?";
$Lang['SysMgr']['Homework']['AllHomework'] = "所有家課";
$Lang['SysMgr']['Homework']['CustomizedExport'] = "客制化匯出";
$Lang['SysMgr']['Homework']['CustomizedPrint'] = "客制化列印";
$Lang['SysMgr']['Homework']['ExportOptions'] = "匯出選項";
$Lang['SysMgr']['Homework']['PrintOptions'] = "列印選項";
$Lang['SysMgr']['Homework']['SelfHomework'] = "自己發出的家課";
$Lang['SysMgr']['Homework']['ReportByClass'] = "以班別做單位";
$Lang['SysMgr']['Homework']['ReportByForm'] = "以年級做單位";
$Lang['SysMgr']['Homework']['Warning']['NotSameAcademicYear'] = "開始日期及結束日期必須在同一學年";

$Lang['homework_import']['upload_image'] = '上載拍攝的家課表';
$Lang['homework_import']['upload_csv'] = '上載CSV檔案';
$Lang['homework_import']['alert_select_image'] = '請選擇圖像';
$Lang['homework_import']['uploaded_image'] = '圖像';
$Lang['homework_import']['attachment_format_alert'] = '檔案格式無效';
$Lang['homework_import']['hw_list_import_remark'] = "注意:<br>- 支援.jpg檔案 <br/>- 檔案名稱必須為班名.jpg (如1A.jpg, 1B.jpg)<br/>- 重複檔案將被取替";
$Lang['homework_import']['hw_list_submit_confirm'] = "你是否確定要呈送?";
$Lang['homework_import']['batch_delete_image'] = "批次刪除已上載的家課表圖像";
$Lang['homework_import']['Button']['Search'] = "查詢";
$Lang['homework_import']['jsWarningArr']['InvalidSearchPeriod'] = "查詢時段不正確，";
$Lang['homework_import']['total'] = "共 ";
$Lang['homework_import']['total_image'] = " 幅圖像";
$Lang['homework_import']['batch_delete_warning'] = "刪除圖像後不能恢復，如確定要刪除圖像，請選擇日期範圍之後按刪除鍵。";
$Lang['homework_import']['batch_delete_alert'] = "刪除圖像後不能恢復，是否確定刪除圖像？";
$Lang['homework_import']['batch_delete_finish'] = "圖像刪除成功。";
$Lang['homework_import']['APP']['todday'] = "今天";
$Lang['homework_import']['APP']['class'] = "班別";
$Lang['homework_import']['APP']['date'] = "日期";
$Lang['homework_import']['APP']['homework_list_image'] = "上載家課表圖像";
$Lang['homework_import']['APP']['need_attachment'] = "請上載圖像";
$Lang['homework_import']['APP']['upload_count'] = "已上載";
$homework_import_extra['succeed_num'] = "成功匯入之功課數目";

$Lang['homework_list_image'] = "家課表圖像";
$Lang['homework_list_confirm_upload_image'] = "確認上傳";
$Lang['homework_list_image_verified'] = "成功確認";
$Lang['homework_list_image_unverified'] = "不能確認";
$Lang['homework_list_view_uploaded_image'] = "檢視已上載的家課表圖像";
$Lang['homework_list_uploaded_image'] = "已上載的家課表圖像";
$Lang['homework_list_uploaded_image_verify_result'] = "上傳檔案驗證";
$Lang['homework_list_uploaded_image_upload_result'] = "上傳檔案結果";
$Lang['homework_list_uploaded_image_file_uploaded'] = "已上傳";

# School News
$Lang['SysMgr']['SchoolNews']['SchoolNews'] = "校園最新消息";
$Lang['SysMgr']['SchoolNews']['Settings'] = "設定";
$Lang['SysMgr']['SchoolNews']['NewRecord'] = "新增";
$Lang['SysMgr']['SchoolNews']['StartDate'] = "發出日期";
$Lang['SysMgr']['SchoolNews']['EndDate'] = "結束日期";
$Lang['SysMgr']['SchoolNews']['Title'] = "題目";
$Lang['SysMgr']['SchoolNews']['TitleEng'] = "題目 (英文)";
$Lang['SysMgr']['SchoolNews']['Description'] = "內容";
$Lang['SysMgr']['SchoolNews']['DescriptionEng'] = "內容 (英文)";
$Lang['SysMgr']['SchoolNews']['Attachment'] = "附件";
$Lang['SysMgr']['SchoolNews']['Status'] = "狀況";
$Lang['SysMgr']['SchoolNews']['DisplayOnTop'] = "置頂";
$Lang['SysMgr']['SchoolNews']['StatusPublish'] = "已發佈";
$Lang['SysMgr']['SchoolNews']['StatusPending'] = "未發佈";
$Lang['SysMgr']['SchoolNews']['EmailAlert'] = "發送電子郵件給用戶";
$Lang['SysMgr']['SchoolNews']['PushMessageAlert'] = "發送eClass App推送訊息給用戶";
$Lang['SysMgr']['SchoolNews']['StudentParentAlert'] = "同時發送至相關的學生家長";
$Lang['SysMgr']['SchoolNews']['PublicAnnouncement'] = "公眾宣佈";
$Lang['SysMgr']['SchoolNews']['EditRecord'] = "編輯";
$Lang['SysMgr']['SchoolNews']['CurrentAttachment'] = "現有附件";
$Lang['SysMgr']['SchoolNews']['AllStatus'] = "所有狀況";
$Lang['SysMgr']['SchoolNews']['ViewReadStatus'] = "檢視觀看情況";
$Lang['SysMgr']['SchoolNews']['Read'] = "已閱";
$Lang['SysMgr']['SchoolNews']['Unread'] = "未閱";
$Lang['SysMgr']['SchoolNews']['ReadList'] = "已閱名單";
$Lang['SysMgr']['SchoolNews']['UnreadList'] = "未閱名單";
$Lang['SysMgr']['SchoolNews']['Name'] = "姓名";
$Lang['SysMgr']['SchoolNews']['Role'] = "身份";
$Lang['SysMgr']['SchoolNews']['WarnSelectGroup'] = "請選擇組別";
$Lang['SysMgr']['SchoolNews']['Approval'] = "批核";
$Lang['SysMgr']['SchoolNews']['NoApproval'] = "不經批核";
$Lang['SysMgr']['SchoolNews']['Reject'] = "不批核";
$Lang['SysMgr']['SchoolNews']['Approve'] = "批核";
$Lang['SysMgr']['SchoolNews']['Copied'] = "1|=|紀錄已經複製";
$Lang['SchoolNews']['DefaultNumDays'] = "預設結束日期日數";
$Lang['SchoolNews']['SpecificGroup'] = "特定小組";
$Lang['SchoolNews']['target'] = "對象";
$Lang['SysMgr']['SchoolNews']['PleaseFillIn'] = "請填上 ";
$Lang['SysMgr']['SchoolNews']['Invaliddate'] = "日期不符";
$Lang['SysMgr']['SchoolNews']['StartEndWrongAlert'] = "日期錯誤. 結束日期不能早於開始日期。";
$Lang['SysMgr']['SchoolNews']['UpdateScuccess'] = "紀錄已經新增。";
$Lang['SysMgr']['SchoolNews']['UpdateUnsuccess'] = "更新紀錄失敗。";
$Lang['SysMgr']['SchoolNews']['AllowUserToViewPastNews'] = "允許用戶檢視過期消息";
$Lang['SysMgr']['SchoolNews']['SchoolNewsDeleted'] = '此消息已被刪除，故未能檢視。';
$Lang['SysMgr']['SchoolNews']['SchoolNewsHasExpired'] = '此消息已過期，故未能檢視。';
$Lang['SysMgr']['SchoolNews']['AppNotEnabledSchoolNews'] = '學校尚未啟用學校宣佈功能，故未能檢視。';
$Lang['SysMgr']['SchoolNews']['SchoolNewsNotStart'] = '此消息並未開放，請於 <!--startDate--> 或之後回來檢視。';
$Lang['SysMgr']['SchoolNews']['WebsiteCode'] = '網站識別編號';
$Lang['SysMgr']['SchoolNews']['onlyShowOwnClass'] = '班導師可發放校園消息 (只限自己班學生)';

# campus link
$i_CampusLink_edit = "修改校園連結";
$i_CampusLink_add = "新增校園連結";
$i_CampusLink_edit_fail = "紀錄修改失敗";
$i_CampusLink_delete_fail = "紀錄刪除失敗";
$i_CampusLink_connect_fail = "連接伺服器失敗";
$i_CampusLink_validate = "名稱與超連結不可是空的";
$i_CampusLink_delete_warning = "你是否決定刪除以下連結";
$i_CampusLink_wrong_addr = "超連結必定以 'http://', 'https://', 'ftp://' 開頭";
$i_CampusLink_moveUp = "移上";
$i_CampusLink_moveDown = "移下";
$i_CampusLink_moveUp_success = "成功上移";
$i_CampusLink_moveDown_success = "成功下移";

#icalendar
$iCalendar_iCalendar = '我的行事曆';
$iCalendar_iCalendarLite = '我的行事曆(基本版)';
$Lang['iCalendar']['FieldTitle']['ShowInSchoolCalendar'] = '在校曆表中顯示';
$Lang['iCalendar']['null_created_by'] = '<font style="color:red;">**</font>已刪除用戶';

#eclass
$Lang['eclass']['warning']['checkRemoveConfrim'] = "你是否想移除以下科目的學科組別:\\n";
$Lang['eclass']['directory']['notCategorized'] = "未分類";
$Lang['eclass']['setting']['TeacherEditEclass'] = "讓老師在eClass目錄頁自行新增、編輯及刪除eClass";
$Lang['eclass']['setting']['NAEditEclass'] = "讓非教學職務員工在eClass目錄頁自行新增、編輯及刪除eClass";
$Lang['eclass']['warning']['noSubjectGroup'] = "由於並未選擇科組(或並無合適的對應科組)，此網上教室將新增到網上教室目錄的\"未分類\"類別下。";
$Lang['eclass']['warning']['noSubjectAndSubjectGroup'] = "由於並未選擇科目及科組(或並無合適的對應科組)，此網上教室將新增到網上教室目錄的\"未分類\"類別下。";
$Lang['eclass']['warning']['noClassEnrolled'] = "你並未加入任何電子教室";
$Lang['eclass']['default_max_quota'] = "預設教室的容量限額";
$Lang['eclass']['default_max_size'] = "預設學生於評估和PowerLesson上傳文件的大小";
$Lang['eclass']['import_member_success'] = "成功滙入到以下網上課室";
$Lang['eclass']['import_member_skipped'] = "有些紀錄已存在";
$Lang['eclass']['import_member_success_amount'] = "成功匯入數目";
$Lang['eclass']['import_member_error_user'] = "用戶已經存在於該網上課室";
$Lang['eclass']['import_member_error_no_user'] = "系統內沒有相關用戶用此登入ID ";
$Lang['eclass']['import_member_error_no_course'] = "系統內沒有相關網上課室用此ID";
$Lang['eclass']['import_member_role_alert'] = "請選擇教室角色";
$Lang['eclass']['import_member_csv_alert'] = "請選擇檔案!";
$Lang['eclass']['import_member_csv_format'] = "第一欄 : <span class='tabletextrequire'>*</span>UserLogin<br />第二欄 : <span class='tabletextrequire'>*</span>CourseID";
$Lang['eclass']['import_member_error_failed'] = "部份資料出現問題，因此不能匯入！請先修正，然後再匯入。";
$Lang['eclass']['import_member2course'] = "匯入用戶至網上課室";
$Lang['eClass']['Student'] = "學生";
$Lang['eClass']['Teacher'] = "老師";
$Lang['eClass']['TeachingAssistant'] = "教學助理";
$Lang['eClass']['LfAssessmentReport']['Ranking'] = "排行榜";
$Lang['eClass']['LfAssessmentReport']['Progress'] = "學習進度";
$Lang['eClass']['LfAssessmentReport']['Weekly'] = "每週";
$Lang['eClass']['LfAssessmentReport']['Accumulated'] = "累計成績";
$Lang['eClass']['LfAssessmentReport']['NoClass'] = "對不起，你不屬於任何課程";
$Lang['eClass']['LfAssessmentReport']['EmptyRecord'] = "沒有記錄";
$Lang['eClass']['LfAssessmentReport']['Week'] = "按週計算";
$Lang['eClass']['LfAssessmentReport']['Year'] = "按月計算";
$Lang['eClass']['LfAssessmentReport']['header1'] = "排名";
$Lang['eClass']['LfAssessmentReport']['header2'] = "學生";
$Lang['eClass']['LfAssessmentReport']['header3'] = "總得分";
$Lang['eClass']['LfAssessmentReport']['header4'] = "練習數量";
$Lang['eClass']['LfAssessmentReport']['WeekHeader'] = "週";
$Lang['eClass']['Management']['select_subject'] = "選擇學科組別";
$Lang['eClass']['Management']['teacher_only'] = "只將老師加入課室";
$Lang['eClass']['Management']['student_only'] = "只將學生加入課室";
$Lang['eclass']['import_course_and_assign_teacher'] = "匯入網上課室及指派教師";
$Lang['eclass']['import_course_and_assign_teacher_csv_format'] = "第一欄 : <span class='tabletextrequire'>*</span>CourseCode<br />第二欄 : <span class='tabletextrequire'>*</span>Course<br />第三欄 : <span class='tabletextrequire'>*</span>UserLogin";
$Lang['eclass']['import_course_and_assign_teacher_error_course_exists'] = "網上課室 (%s - %s) 已經存在!";
$Lang['eclass']['import_course_and_assign_teacher_error_user_exists'] = "網上課室 (%s - %s) 用戶已經存在!";
$Lang['eclass']['import_course_and_assign_teacher_success'] = "成功滙入以新增網上課室及指派教師到相關網上課室";
$Lang['eclass']['import_course_and_assign_teacher_amount'] = "教師數目";

#subject Group
$Lang['SysMgr']['SubjectClassMapping']['eClass'] = '網上教室';
$Lang['SysMgr']['SubjectClassMapping']['Createelcass']='為每個科組建立網上教室';
$Lang['SysMgr']['SubjectClassMapping']['elcassCreated'] ='有相關網上教室';
$Lang['SysMgr']['SubjectClassMapping']['warningRemoveSubject'] ='你是否想一同移除該學科組別對應之網上教室?';
$Lang['SysMgr']['SubjectClassMapping']['createNew'] ='新增';
$Lang['SysMgr']['SubjectClassMapping']['newStd2eclass'] ='由科組加新的學生到網上教室';
$Lang['SysMgr']['SubjectClassMapping']['noneOfTheAbove'] ='不屬於這課室的任何科組';
$Lang['SysMgr']['SubjectClassMapping']['selectFromExisting'] ='由現存的網上教室選取';
$Lang['SysMgr']['SubjectClassMapping']['subjectGrpAssignedToCourse'] ='已為此課室設定的科組';
$Lang['SysMgr']['SubjectClassMapping']['currentSubGrpDetectedFromStudents'] ='來自現時學生分佈的科組';

### If you have to update the IP20 wordings in IP25, please update here with the same variable name as IP20
$i_ClassNameNumber = "班別/學號";
$i_GroupCategoryNewDefaultRole = "預設小組角色";
$i_RoleDescription = "描述";
$i_GroupCategoryName = "類別";
$i_RoleTitle = "名稱";
$i_RoleDescription = "簡述";
$i_frontpage_assessment = "評估";
$i_Homework_admin_use_teacher_collect_homework = "顯示是否需要收回有關功課";
$i_Homework_Collect_Required="是否需要收回有關功課";
$Lang['SystemHelpter']['SavePageSettingsAsText'] = "Save Page Settings as Text";
$i_Profile_Import_NoMatch_Entry[3]="WebSAMS 註冊號碼錯誤";
$button_export_pps = "匯出繳費靈之手續費";
$i_Payment_Menu_Settings_PPS_Setting = "繳費靈設定";
$i_Payment_Menu_Browse_MissedPPSBill = "不明的繳費靈紀錄";
$i_Payment_Import_Format_PPSLog_Charge_Deduction = "繳費靈手續費($".LIBPAYMENT_PPS_CHARGE.", Counter Bill 則為 $".LIBPAYMENT_PPS_CHARGE_COUNTER_BILL.")將自動從所有交易項目中扣除。";
$i_Payment_PPS_Charge = "繳費靈手續費";
$i_Payment_AccountNoMatch_Remark="系統會將此交易紀錄視為不明的繳費靈紀錄處理。<br>請前往 <b>$i_Payment_Menu_DataBrowsing</b> &gt; <b>$i_Payment_Menu_Browse_MissedPPSBill</b> 匹配此帳戶號碼予相關學生。";
$i_InventorySystem_Import_CorrectSingleItemCodeReminder = " * 以上之物品已報銷，請使用新的物品編號進行匯入。";
$i_InventorySystem_Import_CorrectBulkItemCodeReminder = " * 以上之物品已報銷，匯入已有的物品編號，將更改所對應物品之數量。";
$i_InventorySystem_NewItem_ItemBarcodeRemark = "請參考 \"設定> 系統屬性 > 條碼格式\" 中的條碼格式。";
$i_Sports_field_Gender = "性別";
$i_eClass_Admin_AssessmentReport = "評估報告";

### Session Timeout
$Lang['SessionTimeout']['ReadyToTimeout_Header'] = "自動登出提示";
$Lang['SessionTimeout']['ReadyToTimeout_str1'] = "由於你已經一段時間沒有對系統進行任何操作，<b>系統將於";
$Lang['SessionTimeout']['ReadyToTimeout_str2'] = "秒後自動登出</b>。";
$Lang['SessionTimeout']['ReadyToTimeout_str3'] = "如要取消自動登出，請按";
$Lang['SessionTimeout']['ReadyToTimeout_str4'] = "。";
$Lang['SessionTimeout']['Continue'] = "繼續使用";
$Lang['SessionTimeout']['CancelTimeout'] = "自動登出已經取消。";

$Lang['SessionTimeout']['Timeout_str1'] = "由於你已經一段時間沒有對系統進行任何操作，系統已經自動登出。<br>如要再次使用系統，請按";
$Lang['SessionTimeout']['Timeout_str2'] = "。";
$Lang['SessionTimeout']['ReLogin'] = "重新登入";

$Lang['PromotionGuide'] = "學年過渡(升班)指引";

### editform
$Lang['Button']['MoveUp'] = "上移";
$Lang['Button']['MoveDown'] = "下移";
$Lang['Button']['Delete'] = "刪除";
$Lang['Button']['Edit'] = "編輯";
$Lang['Button']['Download'] = "下載";
$Lang['Button']['Extract'] = "解壓";

$Lang['Button']['SaveSubmit'] = "儲存及呈送";
$Lang['Button']['SaveApply'] = "儲存及套用";

############################################################
#  Please add wodings at the below start at 2009-09-09  (Need to re-order later after checked by Claudio)
############################################################
$eEnrollment['front']['wish_enroll_front'] = "，希望參加&nbsp;";

$Lang['SysMgr']['FormClassMapping']['Term'] = "學期";
$Lang['SysMgr']['FormClassMapping']['EditClassNumber'] = "修改學號";
$Lang['SysMgr']['FormClassMapping']['ClassNumberWarning'] = "學號有誤";
$Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedWarning'] = "已有學生使用此學號";
$Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedinCSVWarning'] = "在已上載的檔案中，已分配此學號予另一學生";
$Lang['SysMgr']['FormClassMapping']['ClassNumberMustBeWithinDigits'] = "學號不能超過九位數字";
$Lang['SysMgr']['FormClassMapping']['ClassInfoDuplicated'] = "如匯入該資料，學號在此班別將會被重複使用。";
$Lang['SysMgr']['FormClassMapping']['TargetClassNumberWarning'] = "目標學號有誤";
$Lang['SysMgr']['FormClassMapping']['TargetClassNumberMustBeWithinDigits'] = "目標學號不能超過九位數字";
$Lang['SysMgr']['FormClassMapping']['TargetClassInfoDuplicated'] = "如匯入該資料，目標學號在此班別將會被重複使用。";

$Lang['SysMgr']['FormClassMapping']['ClassGroup'] = "班別組別";
$Lang['SysMgr']['FormClassMapping']['ClassGroupCodeInvalid'] = "組別代號有誤";
$Lang['SysMgr']['FormClassMapping']['ClassGroupDisabled'] = "組別已停用";
$Lang['SysMgr']['FormClassMapping']['ClassGroupCode'] = "組別代號";
$Lang['SysMgr']['FormClassMapping']['ClassGroupSettings'] = "班別組別設定";
$Lang['SysMgr']['FormClassMapping']['Reorder']['ClassGroup'] = "移動班別組別";
$Lang['SysMgr']['FormClassMapping']['DeleteTips']['ClassGroup'] = "刪除班別組別";
$Lang['SysMgr']['FormClassMapping']['Add']['ClassGroup'] = "新增班別組別";
$Lang['SysMgr']['FormClassMapping']['Edit']['ClassGroup'] = "編輯班別組別";
$Lang['SysMgr']['FormClassMapping']['AllClassGroup'] = "所有班別組別";

$Lang['SysMgr']['FormClassMapping']['FormStage'] = "班級階段";
$Lang['SysMgr']['FormClassMapping']['FormStageCodeInvalid'] = "階段代號有誤";
$Lang['SysMgr']['FormClassMapping']['FormStageDisabled'] = "階段已停用";
$Lang['SysMgr']['FormClassMapping']['FormStageCode'] = "階段代號";
$Lang['SysMgr']['FormClassMapping']['FormStageSettings'] = "班級階段設定";
$Lang['SysMgr']['FormClassMapping']['Reorder']['FormStage'] = "移動班級階段";
$Lang['SysMgr']['FormClassMapping']['DeleteTips']['FormStage'] = "刪除班級階段";
$Lang['SysMgr']['FormClassMapping']['Add']['FormStage'] = "新增班級階段";
$Lang['SysMgr']['FormClassMapping']['Edit']['FormStage'] = "編輯班級階段";


$Lang['SysMgr']['FormClassMapping']['InUse'] = "使用中";
$Lang['SysMgr']['FormClassMapping']['NotInUse'] = "已停用";

$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Add']['Success']['ClassGroup'] = '1|=|班別組別新增成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Add']['Failed']['ClassGroup'] = '0|=|班別組別新增失敗。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Edit']['Success']['ClassGroup'] = '1|=|班別組別編輯成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Edit']['Failed']['ClassGroup'] = '0|=|班別組別編輯失敗。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Delete']['Success']['ClassGroup'] = '1|=|班別組別刪除成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Delete']['Failed']['ClassGroup'] = '0|=|班別組別刪除失敗。';
$Lang['SysMgr']['FormClassMapping']['Warning']['DuplicatedCode']['ClassGroup'] = '* 已有其他班別組別使用此代號';
$Lang['SysMgr']['FormClassMapping']['jsWarning']['ConfirmDeleteClassGroup'] = '你是否確定刪除此班別組別？';
$Lang['SysMgr']['FormClassMapping']['Select']['ClassGroup'] = '選擇班別組別';

$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Add']['Success']['FormStage'] = '1|=|班級階段新增成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Add']['Failed']['FormStage'] = '0|=|班級階段新增失敗。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Edit']['Success']['FormStage'] = '1|=|班級階段編輯成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Edit']['Failed']['FormStage'] = '0|=|班級階段編輯失敗。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Delete']['Success']['FormStage'] = '1|=|班級階段刪除成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Delete']['Failed']['FormStage'] = '0|=|班級階段刪除失敗。';
$Lang['SysMgr']['FormClassMapping']['Warning']['DuplicatedCode']['FormStage'] = '* 已有其他班級階段使用此代號';
$Lang['SysMgr']['FormClassMapping']['jsWarning']['ConfirmDeleteFormStage'] = '你是否確定刪除此班級階段？';
$Lang['SysMgr']['FormClassMapping']['Select']['FormStage'] = '選擇班級階段';

$Lang['SysMgr']['SubjectClassMapping']['ImportTeacherStudent'] = '新增科組成員';
$Lang['SysMgr']['SubjectClassMapping']['ExportTeacherStudent'] = '匯出老師及學生';
$Lang['SysMgr']['SubjectClassMapping']['TeacherStudent'] = '科組成員';
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectGroup'] = '匯入新科組';
$Lang['SysMgr']['SubjectClassMapping']['ExportSubjectGroup'] = '匯出科組';
$Lang['SysMgr']['SubjectClassMapping']['ImportSubject'] = '匯入學科';
$Lang['SysMgr']['SubjectClassMapping']['ExportSubject'] = '匯出學科';
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponent'] = '匯入學科分卷';
$Lang['SysMgr']['SubjectClassMapping']['ExportSubjectComponent'] = '匯出學科分卷';
$Lang['SysMgr']['SubjectClassMapping']['Term'] = '學期';
$Lang['SysMgr']['SubjectClassMapping']['NoSubjectGroupWarning'] = '沒有此科組';
$Lang['SysMgr']['SubjectClassMapping']['TeacherInSubjectGroupAlready'] = '老師已任教此科組';
$Lang['SysMgr']['SubjectClassMapping']['StudentInSubjectGroupAlready'] = '學生已加入此科組';
$Lang['SysMgr']['SubjectClassMapping']['StudentHasJoinedOtherSubjectGroupOfTheSameSubject'] = '學生已參加此科目的其他科組';
$Lang['SysMgr']['SubjectClassMapping']['StudentDoNotMatchFormOfSubjectGroup'] = '科組不允許此班級學生參加';
$Lang['SysMgr']['SubjectClassMapping']['NoSubjectWarning'] = '沒有此學科';
$Lang['SysMgr']['SubjectClassMapping']['NoSubjectComponentWarning'] = '沒有此學科分卷';
$Lang['SysMgr']['SubjectClassMapping']['SubjectComponentCodeWarning'] = '代號屬於學科分卷';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupExistWarning'] = '科組已存在';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupNotExistWarning'] = '沒有此科組';
$Lang['SysMgr']['SubjectClassMapping']['NoFormWarning'] = '班級不存在';
$Lang['SysMgr']['SubjectClassMapping']['OutOfFormRelation'] = '級別-科目關係以外的級別';
$Lang['SysMgr']['SubjectClassMapping']['CreateEclassFieldNotVaild'] = '增加eClass一欄必需填寫 "yes" 或 "no"';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeDuplicatedWithinCSVWarning'] = '在已上載的檔案中有重複的科組代號';
$Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroupCodeDuplicatedWithinCSVWarning'] = '在已上載的檔案中有重複的新科組代號';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeUsedWarning'] = '已有其他科組使用此新科組代號';
$Lang['SysMgr']['SubjectClassMapping']['BlankCodeWarning'] = '* 代號空白';
$Lang['SysMgr']['SubjectClassMapping']['Duplicated_LC_CodeWarning'] = '* 已有學習領域使用此代號';
$Lang['SysMgr']['SubjectClassMapping']['NoLearningCategoryWarning'] = '沒有此學習領域';
$Lang['SysMgr']['SubjectClassMapping']['SubjectExistWarning'] = '學科已存在';
$Lang['SysMgr']['SubjectClassMapping']['SubjectCodeDuplicatedWithinCSVWarning'] = '在已上載的檔案中有重複的科目代號';
$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCodeInUseWarning'] = '已有其他學科或學科分卷使用此WebSAMS代號';
$Lang['SysMgr']['SubjectClassMapping']['ComponentCodeDuplicatedWithinCSVWarning'] = '在已上載的檔案中有重複的科目分卷代號';
$Lang['SysMgr']['SubjectClassMapping']['DuplicatedStudent&SubjectGroupWithinCSVWarning'] = '在已上載的檔案中，此用戶已分配到同一學科分組';
$Lang['SysMgr']['SubjectClassMapping']['TeacherIsNotTeachingStaffWarning'] = '此用戶不是教學職員';
$Lang['SysMgr']['SubjectClassMapping']['TimeClashWithOtherSubjectGroup'] = '此用戶已參加其他相同時間上課的科組';
$Lang['SysMgr']['SubjectClassMapping']['SubjectCodeLongerThanThreeChar'] = 'WebSAMS代號不可長於三個字';
$Lang['SysMgr']['SubjectClassMapping']['SubjectCodeLongerThan10Char'] = 'WebSAMS代號不可長於10個字';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeRemark'] = '備註: ';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeNoSpace'] = '學科分組代號不可包含空格';

$Lang['SysMgr']['SubjectClassMapping']['SubjectCode'] = '學科WebSAMS代號';
$Lang['SysMgr']['SubjectClassMapping']['SubjectComponentCode'] = '學科分卷WebSAMS代號';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCode'] = '學科分組代號';
$Lang['SysMgr']['SubjectClassMapping']['LearningCategoryCode'] = '學習領域代號';
$Lang['SysMgr']['SubjectClassMapping']['ApplicableForm'] = '適用班級';
$Lang['SysMgr']['SubjectClassMapping']['CreateEclass'] = '建立網上課室';
$Lang['SysMgr']['SubjectClassMapping']['TimeTableConflict'] = '*以上學生應要移除，因他們已存在於其他學科組別，而在時間表設定上，那些組別與此組別發生衝突。';

// Copy Subject Group
$Lang['SysMgr']['SubjectClassMapping']['Copy'] = '複製';
$Lang['SysMgr']['SubjectClassMapping']['CopySubjectGroup'] = '複製科組';
$Lang['SysMgr']['SubjectClassMapping']['CopyFromOtherYearTerm'] = '透過複製現有資料';
$Lang['SysMgr']['SubjectClassMapping']['Options'] = '選項';
$Lang['SysMgr']['SubjectClassMapping']['CopyResult'] = '複製結果';
$Lang['SysMgr']['SubjectClassMapping']['CopyFrom'] = '複製自';
$Lang['SysMgr']['SubjectClassMapping']['CopyFromAcademicYear'] = '複製自學年';
$Lang['SysMgr']['SubjectClassMapping']['CopyFromTerm'] = '複製自學期';
$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear'] = '複製到學年';
$Lang['SysMgr']['SubjectClassMapping']['CopyToTerm'] = '複製到學期';
$Lang['SysMgr']['SubjectClassMapping']['CopyTimetable'] = '複製時間表';
$Lang['SysMgr']['SubjectClassMapping']['CopyClassStudents'] = '複製班別學生';
$Lang['SysMgr']['SubjectClassMapping']['SuccessCountSubjectGroup'] = '能夠複製的科組數目';
$Lang['SysMgr']['SubjectClassMapping']['FailCountSubjectGroup'] = '不能複製的科組數目';
$Lang['SysMgr']['SubjectClassMapping']['FailureSubjectGroup'] = '不能複製的科組';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['SubjectGroupCopiedAlready'] = '已複製此科組到此學期';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['TimetableLocationInUse'] = '此科組於時間表中的位置已被使用';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['StudentTimeClash'] = '此科組有學生於時間表中發生時間衝突';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseSelectAtLeastOneSubjectGroup'] = '請至少選擇一科組。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['CopySubjectGroupSuccess'] = '1|=|複製科組成功';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['CopySubjectGroupFailed'] = '0|=|複製科組失敗';
$Lang['SysMgr']['SubjectClassMapping']['QuickEdit'] = '更新科組資料';
$Lang['SysMgr']['SubjectClassMapping']['BuildGroups'] = '建立科組';
$Lang['SysMgr']['SubjectClassMapping']['GroupDetails'] = '科組資料';
$Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroupCode'] = '新學科分組編號';
$Lang['SysMgr']['SubjectClassMapping']['NewNameEn'] = '新名稱(英文)';
$Lang['SysMgr']['SubjectClassMapping']['NewNameCh'] = '新名稱(中文)';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCopySuccessfully'] = '科組複製成功';
$Lang['SysMgr']['SubjectClassMapping']['GoQuickEditInstruction'] = '系統已產生不同的科組編號至各複製科組。你可使用科組版面的「快速編輯」功能來更改各科組的編號及名稱。';
$Lang['SysMgr']['SubjectClassMapping']['BackToSubjectGroupPage'] = '回到科組版面';
$Lang['SysMgr']['SubjectClassMapping']['WarningText'] = '警告';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['CannotCopyStudentForDifferentAcademicYear'] = '如複製自不同學年，你將不能選擇複製科組學生。';
$Lang['SysMgr']['SubjectClassMapping']['CreateeClassIfSubjectGroupDoesNotHaveOne'] = '如所選擇的科組未有連結網上教室，為複製到本學期的科組建立網上教室';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['TheFollowingSubjectGroupWillBeDeleted'] = '以下所有科組將被永久移除，而且無法恢復。如確定要刪除，請按<font style="font-size:large">刪除全部</font>。';
$Lang['SysMgr']['SubjectClassMapping']['SelectedSubjectGroup'] = '已選擇科組';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['DeleteSubjectGroup'] = '你是否確定你要刪除這些科組?';

$Lang['SysMgr']['RoleManagement']['LessonAttendance'] = '課堂考勤管理';

# Student Lesson Attendance
$Lang['LessonAttendance']['DailyLessonStatus'] = '每天課堂考勤情況';
$Lang['LessonAttendance']['LessonStatusAllConfirmed'] = '全部確認';
$Lang['LessonAttendance']['LessonStatusAllNotConfirmed'] = '全部未確認';
$Lang['LessonAttendance']['LessonStatusPartialConfirmed'] = '部份確認';
$Lang['LessonAttendance']['Confirmed'] = '已確認';
$Lang['LessonAttendance']['NotConfirmed'] = '未確認';
$Lang['LessonAttendance']['LessonClassDetailReport'] = '課堂每日報告';
$Lang['LessonAttendance']['Late'] = '遲到';
$Lang['LessonAttendance']['Absent'] = '缺席';
$Lang['LessonAttendance']['Present'] = '出席';
$Lang['LessonAttendance']['Outing'] = '外出';
$Lang['LessonAttendance']['LateForSplitClass'] = '遲到走堂';
$Lang['LessonAttendance']['MedicalLeave'] = '病假';
$Lang['LessonAttendance']['SchoolActivities'] = '學校活動';
$Lang['LessonAttendance']['OtherApprovedEvents'] = '其他經批准的活動';
$Lang['LessonAttendance']['InvalidDateFormat'] = '日期格式錯誤';
$Lang['LessonAttendance']['LessonSummaryReport'] = '課堂考勤總結';
$Lang['LessonAttendance']['LessonSummaryReportRemark'] = '* 此報告只會顯示有遲到或缺席紀錄的學生。';
$Lang['LessonAttendance']['FromDate'] = '開始日期';
$Lang['LessonAttendance']['ToDate'] = '結束日期';
$Lang['LessonAttendance']['LessonSessionCountAsDay'] = '課堂數目對應整日(缺席)';
$Lang['LessonAttendance']['LessonSessionLateCountAsDay'] = '課堂數目對應整日(遲到)';
$Lang['LessonAttendance']['LessionAttendanceSetting'] = '課堂考勤設定';
$Lang['LessonAttendance']['BasicSettingsLessionAttendance'] = '基本設定(課堂考勤)';
$Lang['LessonAttendance']['CountAsDay'] = '換算成日數';
$Lang['LessonAttendance']['ClassSeatingPlanSavedSuccess'] = "1|=|座位表儲存成功";
$Lang['LessonAttendance']['LessonAttendanceDataSavedSuccess'] = "1|=|課堂考勤紀錄儲存成功";
$Lang['LessonAttendance']['ImportAttendenceData'] = "匯入課堂考勤紀錄";
$Lang['LessonAttendance']['Status'] = "狀況";
$Lang['LessonAttendance']['ImportAttendanceDataWarningMsg'] = "以下紀錄發生問題而不會匯入到系統中";
$Lang['LessonAttendance']['Late&Absent'] = '遲到及缺席';
$Lang['LessonAttendance']['EnablePSLAD'] = '允許家長及學生檢視課堂考勤紀錄';
$Lang['LessonAttendance']['DaysBeforeToOpenData'] ='學生可查看多少天前的課堂考勤紀錄';
$Lang['LessonAttendance']['LessonNonConfirmedReport'] = "課堂未點名報告";
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Title'] = "匯入課堂考勤紀錄";
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'] = array();
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][] = array('Date(日期)','請使用格式 YYYY-MM-DD 或者 DD/MM/YYYY。');
//$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][] = array('Subject Group Code(科組代碼)','請到 [ '.$Lang['Header']['Menu']['SchoolSettings'].' > '.$Lang['Header']['Menu']['Subject'].' > '. $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'].' ] 查看。');
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][] = array('Time Slot Name(時段)','請到 [ '.$Lang['Header']['Menu']['SchoolSettings'].' > '.$Lang['Header']['Menu']['Timetable'].' ] 查看。');
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][] = array('Class Name(班別)','學生班別。請使用英文班別名稱。');
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][] = array('Class Number(學號)','學生學號。');
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][] = array('Status(狀況)','狀況代碼:<ul><li>0 : 出席</li><li>1 : 外出</li><li>2 : 遲到</li><li>3 : 缺席</li>'.($sys_custom['LessonAttendance_LaSalleCollege']?'<li>4 : 遲到走堂</li><li>5 : 病假</li><li>6 : 學校活動</li><li>7 : 其他經批准的活動</li>':'').'</ul>');
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][] = array('Reason(原因)','不適用於出席狀況。');
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][] = array('Remarks(附註)','附加的文字資料。');
$Lang['LessonAttendance']['LARShownBeforeDate'] = "課堂考勤紀錄只顯示在此日期前：";
$Lang['LessonAttendance']['LARDisable'] = "不允許檢視課堂考勤紀錄。";
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorSubjectGroupIsNotFound'] = '找不到科組。';
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorTimeSlotIsNotFound'] = '找不到時段。';
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorInvalidClassName'] = '班別名稱不正確。';
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorInvalidClassNumber'] = '學號不正確。';
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorInvalidAttendanceStatus'] = '狀況代碼不正確。';
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorLessonIsNotFound'] = '找不到課堂。';
$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Remarks'] = "* 請注意系統只會處理本學年的課堂考勤紀錄。";
$Lang['LessonAttendance']['NoLessonToTake'] = "你沒有課堂需要點名。";
$Lang['LessonAttendance']['iSmartCardTeacherCanTakeAllLessons'] = "智能卡紀錄 - 教職員可以為所有課堂點名";
$Lang['LessonAttendance']['LessonType'] = "課堂類別";
$Lang['LessonAttendance']['TeachersOwnLessons'] = "所教的課堂";
$Lang['LessonAttendance']['AllLessons'] = "所有課堂";
$Lang['LessonAttendance']['LessonDailyReportCSVReminder'] = "如選取多過一日，系統會以CSV形式產生報告。 ";
$Lang['LessonAttendance']['DefaultStatus'] = "預設狀況";
$Lang['LessonAttendance']['LessonStatusFollowSchoolStatus'] = "課堂考勤狀況跟隨上學考勤狀況";
$Lang['LessonAttendance']['LessonAttendanceReasons'] = "課堂考勤原因";
$Lang['LessonAttendance']['ReasonType'] = "原因類別";
$Lang['LessonAttendance']['Reason'] = "原因";
$Lang['LessonAttendance']['Symbol'] = "符號";
$Lang['LessonAttendance']['RequestInputThisData'] = "請輸入該項資料。";
$Lang['LessonAttendance']['SameReasonExists'] = "相同的原因已經存在。";
$Lang['LessonAttendance']['SameReasonSymbolExists'] = "相同的原因符號已經存在。";
$Lang['LessonAttendance']['LateSkipLessonRecords'] = "遲到 / 走堂紀錄";
$Lang['LessonAttendance']['SkipLesson'] = "走堂";
$Lang['LessonAttendance']['Subject'] = "學科";
$Lang['LessonAttendance']['LessonSession'] = "堂數";
$Lang['LessonAttendance']['LessonSummary'] = "課堂總結";
$Lang['LessonAttendance']['AbsentSummary'] = "缺席總結";
$Lang['LessonAttendance']['SkipLessonSummary'] = "走堂總結";
$Lang['LessonAttendance']['LateSummary'] = "遲到總結";
$Lang['LessonAttendance']['Type'] = "類別";
$Lang['LessonAttendance']['GroupBy'] = "分組";
$Lang['LessonAttendance']['Filter'] = "篩選";
$Lang['LessonAttendance']['WithReason'] = "帶有原因";
$Lang['LessonAttendance']['Daily'] = "每天";
$Lang['LessonAttendance']['Weekly'] = "每星期";
$Lang['LessonAttendance']['Monthly'] = "每月";
$Lang['LessonAttendance']['Period'] = "時段";
$Lang['LessonAttendance']['SingleStudentSummaryReport'] = "個別學生總結報告";
$Lang['LessonAttendance']['AttendanceRate'] = "出席率";
$Lang['LessonAttendance']['PunctualRate'] = "準時率";
$Lang['LessonAttendance']['SkipLessonRate'] = "走堂率";
$Lang['LessonAttendance']['AbsencesByReason'] = "缺席原因";
$Lang['LessonAttendance']['FullDay'] = "全日";
$Lang['LessonAttendance']['HalfDay'] = "半日";
$Lang['LessonAttendance']['DaysAbsentLate'] = "缺席日數 / 遲到日數";
$Lang['LessonAttendance']['TotalSchoolDays'] = "上學日數";
$Lang['LessonAttendance']['Count'] = "數目";
$Lang['LessonAttendance']['AttendanceBySubject'] = "按學科的考勤統計";
$Lang['LessonAttendance']['NumberOfPeriod'] = "時段數目";
$Lang['LessonAttendance']['OverviewByPeriod'] = "按時段的統計";
$Lang['LessonAttendance']['Overall'] = "總體";
$Lang['LessonAttendance']['DisplayWeekday'] = array();
$Lang['LessonAttendance']['DisplayWeekday'][0] = "星期日";
$Lang['LessonAttendance']['DisplayWeekday'][1] = "星期一";
$Lang['LessonAttendance']['DisplayWeekday'][2] = "星期二";
$Lang['LessonAttendance']['DisplayWeekday'][3] = "星期三";
$Lang['LessonAttendance']['DisplayWeekday'][4] = "星期四";
$Lang['LessonAttendance']['DisplayWeekday'][5] = "星期五";
$Lang['LessonAttendance']['DisplayWeekday'][6] = "星期六";
$Lang['LessonAttendance']['DisplayWeekday'][7] = "星期日";
$Lang['LessonAttendance']['HalfDayAbsentSummaryReport'] = "缺席統計報告 (半日)";
$Lang['LessonAttendance']['NumberOfStudentDays'] = "上學日數目";
$Lang['LessonAttendance']['PresentRate'] = "出席率";
$Lang['LessonAttendance']['ByClass'] = "以班別";
$Lang['LessonAttendance']['BySubject'] = "以學科";
$Lang['LessonAttendance']['NumberOfStudent'] = "學生數目";
$Lang['LessonAttendance']['StudentDailyRecords'] = "學生每日課堂紀錄";
$Lang['LessonAttendance']['WeekStart'] = "星期開始日";
$Lang['LessonAttendance']['Key'] = "圖例";
$Lang['LessonAttendance']['SessionsToCountAsAM'] = "上午的堂數";
$Lang['LessonAttendance']['SessionsToCountAsPM'] = "下午的堂數";
$Lang['LessonAttendance']['FromSessionToSession'] = "由第 <!--START--> 堂到第 <!--END--> 堂";
$Lang['LessonAttendance']['InvalidSessionRange'] = "堂數範圍不正確。";
$Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'] = "課堂考勤的學生請假及外出紀錄";
$Lang['LessonAttendance']['OverlapRecordsWarning'] = "新增的紀錄與現有的紀錄重複了。";
$Lang['LessonAttendance']['OverlappingRecords'] = "重複的紀錄";
$Lang['LessonAttendance']['RequestSelectAllStatus'] = "請選擇所有狀況。";
$Lang['LessonAttendance']['Lesson'] = "堂";
$Lang['LessonAttendance']['RequestSelectLessons'] = "請選擇課堂。";
$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'] = array();
$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'][] = array('Class Name','學生班別。請使用英文班別名稱。');
$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'][] = array('Class Number','學生學號。');
$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'][] = array('Date','請使用格式 YYYY-MM-DD。');
$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'][] = array('Leave Type','1: 外出, 3: 缺席。');
if($sys_custom['LessonAttendance_LaSalleCollege']){
	$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'][] = array('Leave Sessions','課堂節數，多於一堂可用逗號分隔。');
}
$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'][] = array('Reason','原因。');
$Lang['LessonAttendance']['StudentCannotBeFound'] = "找不到該學生。";
$Lang['LessonAttendance']['StudentAlreadyHasLeaveRecordsOnThatDay'] = "該學生當天已有請假紀錄。";
$Lang['LessonAttendance']['InvalidLeaveType'] = "紀錄類型不正確。";
$Lang['LessonAttendance']['InvalidLessonSessions'] = "堂數不正確。";

$Lang['StudentAttendance']['SMSTemplateNotSet'] = "There is no sms template yet.";

$Lang['StudentAttendance']['SMSTemplateNotSet'] = "未設定短訊範本。";

# Smart Card Student Attendance
$Lang['SmartCard']['StudentAttendence']['ReportSettings'] = "報告設定";
$Lang['SmartCard']['StudentAttendence']['CustomSymbol'] = "自定符號(最多三個字符)";
$Lang['SmartCard']['StudentAttendence']['DefaultSymbol'] = "預設符號";
$Lang['SmartCard']['StudentAttendence']['SchoolDaysStat'] = "上課日統計";

$Lang['SmartCard']['StudentAttendance']['CustomizeStatusSymbols'] = "自定狀態符號";
$Lang['SmartCard']['StudentAttendance']['CustomizeReasonSymbols'] = "自定原因符號";
$Lang['SmartCard']['StudentAttendance']['AddRow'] = "新增列";
$Lang['SmartCard']['StudentAttendance']['DeleteRow'] = "刪除此列";
$Lang['SmartCard']['StudentAttendance']['ReasonsAreDuplicated'] = "個原因重複了";
$Lang['SmartCard']['StudentAttendance']['SymbolsAreDuplicated'] = "個符號重複了";
$Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat'] = "自定原因統計";

$Lang['SmartCard']['StudentAttendance']['PresetAbsenceInfo'] = "請假資訊";

# Smart Card Staff Attendance
$Lang['SmartCard']['StaffAttendence']['ConfirmTime'] = "最後確認時間";
$Lang['SmartCard']['StaffAttendence']['DutyChanged'] = "上班時間有變動";
$Lang['SmartCard']['StaffAttendence']['UpdateDuty'] = "更新上班時間";
$Lang['SmartCard']['StaffAttendence']['ReportSettings'] = "報告設定";
$Lang['SmartCard']['StaffAttendence']['CustomSymbol'] = "自定符號(最多三個字符)";
$Lang['SmartCard']['StaffAttendence']['DefaultSymbol'] = "預設符號";
$Lang['SmartCard']['StaffAttendence']['StaffMonthlyAttendanceReport'] = "教職員每月出席資料表";
$Lang['SmartCard']['StaffAttendence']['MonthlyAttendanceReport'] = "每月出席資料表";
$Lang['SmartCard']['StaffAttendence']['StatNormal'] = "正常數";
$Lang['SmartCard']['StaffAttendence']['StatAbsent'] = "缺席數";
$Lang['SmartCard']['StaffAttendence']['StatLate'] = "遲到數";
$Lang['SmartCard']['StaffAttendence']['StatEarlyLeave'] = "早退數";
$Lang['SmartCard']['StaffAttendence']['StatHoliday'] = "放假數";
$Lang['SmartCard']['StaffAttendence']['StatOutgoing'] = "外出數";
$Lang['SmartCard']['StaffAttendence']['ShowAllColumns'] = "顯示所有欄目";
$Lang['SmartCard']['StaffAttendence']['SelectMethod'] = "選擇方法";

$Lang['StudentAttendance']['AbsentSessions'] = "缺席節數";
$Lang['StudentAttendance']['AbsentSessionsInfo'] = "缺席堂數資訊";
$Lang['StudentAttendance']['AbsentSessionsErrorMsg'] = "請於缺席堂數輸入正整數";

$Lang['StudentAttendance']['DailyOperation_MissParentLetter'] = "未有家長信的缺席紀錄";
$Lang['StudentAttendance']['NoOfDaySinceAbsentDay'] = "缺席日期後的日數";
$Lang['StudentAttendance']['AbsentDate'] = "缺席日期";
$Lang['StudentAttendance']['LastDisciplineRecord'] = "最後訓導紀錄日期";

$Lang['StudentAttendance']['StudentAttendanceRate'] = "學生出席率報告";
$Lang['StudentAttendance']['StudentAttendanceRateKPM'] = "學生出席率KPM報告";
$Lang['StudentAttendance']['SchoolDayKPM'] = "上課日數";
$Lang['StudentAttendance']['DaysAbsent'] = "缺席日數";
$Lang['StudentAttendance']['JS_warning_SchoolDay'] = "請輸入上課日。";
$Lang['StudentAttendance']['KPMFormula'] = "KPM公式";
$Lang['StudentAttendance']['NumberOfStudendsAttended'] = '出席人次';
$Lang['StudentAttendance']['AttendDays'] = "出席日";
$Lang['StudentAttendance']['NumberOfDaysAttended'] = "出席日";
$Lang['StudentAttendance']['DaysReminderText'] = "1.出席日：計算準時、遲到及外出活動日數<br/>
2.上課日：計算有學生考勤記錄的日子。非上課日的考勤記錄則不計算在内。<br/>
3.出席率： (出席日 / 上課日 ) * 100%";
$Lang['StudentAttendance']['DaysReminderTextFormClass'] = "1.出席人次：計算準時、遲到及外出活動學生人次<br/>
2.上課日：計算有學生考勤記錄的日子。非上課日的考勤記錄則不計算在内。<br/>
3.出席率： (出席日 / 上課日 ) * 100%";
$Lang['StudentAttendance']['SchoolDays'] = "上學日";

# Mun Sang Merit & Demerit Record (cust Report)
$Lang['eDiscipline']['MonthlyMeritAndDemeritRecord'] = "學生獎懲報告";
$Lang['eDiscipline']['YearlyMeritAndDemeritRecord'] = "學生獎懲年度報告";
$Lang['eDiscipline']['MunSangAPReport_AwardRecord'] = "獎勵紀錄";
$Lang['eDiscipline']['MunSangAPReport_AwardEventDate'] = "獎勵日期";
$Lang['eDiscipline']['MunSangAPReport_AwardItemName'] = "獎勵事項";
$Lang['eDiscipline']['MunSangAPReport_Award'] = "獎勵";
$Lang['eDiscipline']['MunSangAPReport_PunishRecord'] = "違規紀錄";
$Lang['eDiscipline']['MunSangAPReport_PunishEventDate'] = "違規日期";
$Lang['eDiscipline']['MunSangAPReport_PunishItemName'] = "違規事項";
$Lang['eDiscipline']['MunSangAPReport_Punish'] = "處分";
$Lang['eDiscipline']['MunSangAPReport_RecordDate'] = "紀錄日期";
$Lang['eDiscipline']['MunSangAPReport_Others'] = "其他";
$Lang['eDiscipline']['MunSangSchoolName'] = "民生書院";
$Lang['eDiscipline']['MunSangMeritDemeritRecordTitle'] = "學生獎懲報告";
$Lang['eDiscipline']['MunSangReportNo'] = "報告編號";
$Lang['eDiscipline']['MunSangInputReportNo'] = "報告編號";
$Lang['eDiscipline']['MunSangDate'] = "日期";
$Lang['eDiscipline']['MunSangClass'] = "班級";
$Lang['eDiscipline']['MunSangStudentName'] = "姓名";
$Lang['eDiscipline']['MunSangTimes'] = "次";
$Lang['eDiscipline']['MunSangThisTermRecords'] = "本學年之累積獎懲紀錄";
$Lang['eDiscipline']['MunSangTo'] = "至";
$Lang['eDiscipline']['MunSangSignOfTeacher'] = "班主任簽署";
$Lang['eDiscipline']['MunSangSignOfParent'] = "家長簽署";
$Lang['eDiscipline']['MunSangFinish'] = "完";
$Lang['eDiscipline']['MunSangAchievement'] = "Achievement";
$Lang['eDiscipline']['MunSangServiceAward'] = "Service Award";
$Lang['eDiscipline']['MunSangBlackMark'] = "Black Mark";
$Lang['eDiscipline']['MunSangSubTotal'] = "Subtotal";
$Lang['eDiscipline']['SomeRecordsCannotAdd'] = "<font color=red>部份累積紀錄新增失敗。</font>\n";

# 伍少梅 AP report (cust)
$Lang['eDiscipline']['sdbnsm_record'] = "記";
$Lang['eDiscipline']['sdbnsm_times2'] = "次";
$Lang['eDiscipline']['sdbnsm_day'] = "天";
$Lang['eDiscipline']['sdbnsm_conductMark'] = "品行分";
$Lang['eDiscipline']['sdbnsm_conductMark2'] = "";
$Lang['eDiscipline']['sdbnsm_mark'] = "分";
$Lang['eDiscipline']['sdbnsm_yy'] = "年";
$Lang['eDiscipline']['sdbnsm_mm'] = "月";
$Lang['eDiscipline']['sdbnsm_dd'] = "日";
$Lang['eDiscipline']['sdbnsm_comma'] = "，";
$Lang['eDiscipline']['sdbnsm_fullStop'] = "。";
$Lang['eDiscipline']['sdbnsm_monthlySummary'] = "本月總結";
$Lang['eDiscipline']['sdbnsm_scoreChange2'] = "加分";
$Lang['eDiscipline']['sdbnsm_add'] = "加";
$Lang['eDiscipline']['sdbnsm_minus'] = "扣";
$Lang['eDiscipline']['sdbnsm_otherAward'] = "其他記功事項";
$Lang['eDiscipline']['sdbnsm_otherPunishment'] = "其他記過事項";
$Lang['eDiscipline']['MunSangMonth'] = "月";
$Lang['eDiscipline']['MunSangTotal']  = "總計(次)";

# ?基書院(東九龍) customized report [CRM Ref No.: 2009-0923-0926]
$Lang['eDiscipline']['uccke_TermAwardScheme'] = "學期獎勵計劃";


##### eDis
$eDiscipline['AP_Interval_Value'] = "獎懲紀錄數目差額";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_4'] = "按此";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_3'] = "或";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_5'] = "檢視科目編號。";

$Lang['eDiscipline']['Tag'] = "標籤";
$Lang['eDiscipline']['TagName'] = "標籤名稱";
$Lang['eDiscipline']['TagList'] = "標籤列表";
$Lang['eDiscipline']['NewTag'] = "新增標籤";
$Lang['eDiscipline']['EditTag'] = "編輯標籤";

$Lang['eDiscipline']['DisplayConductMarkInAP'] = "獎勵及懲罰顯示操行分";

$Lang['eDiscipline']['compare_different_periods_convertedDate'] = "不同時期的比較 (轉換後資料)";

$Lang['eDiscipline']['ClassSummary_ConvertedData'] = "班別摘要 (轉換後資料)";
$Lang['eDiscipline']['DisplayIn'] = "顯示";
$Lang['eDiscipline']['Misconduct_Detail_Ranking_Report'] = "違規行為排名詳細報告";
$Lang['eDiscipline']['AwardPunishment_Detail_Ranking_Report'] = "獎勵及懲罰排名詳細報告";
$Lang['eDiscipline']['Individual'] = "學生";
$Lang['eDiscipline']['OnOrAbove'] = "或以上";
$Lang['eDiscipline']['ListOf'] = "";
$Lang['eDiscipline']['ListOf2'] = "名單";
$Lang['eDiscipline']['GoodConductItem'] = "良好行為項目";
$Lang['eDiscipline']['MisconductItem'] = "違規行為項目";
$Lang['eDiscipline']['MisconductItemReportMsg'] = "排名報告顯示違規項目最多的級別、班別或學生。請設定報告時段、對象、及排名方式。";
$Lang['eDiscipline']['AwardPunishmentItemReportMsg'] = "排名報告顯示獎勵或違規項目最多的級別、班別或學生。請設定報告時段、對象、及排名方式。";

$Lang['eDiscipline']['AddAPStopStep2'] = "完成設定";
$Lang['eDiscipline']['AddAPStopStep2_Remark'] = "如無需設定跟進行動及發送通知，請按\"完成設定\"，否則請按\"繼續\"。";

$Lang['eDiscipline']['DetentinoSession_MAX'] = "留堂次數上限";
$Lang['eDiscipline']['Times'] = "次";
$Lang['eDiscipline']['TimesCH'] = "次";
$Lang['eDiscipline']['ExportForDetention'] = "匯出留堂格式";
$Lang['eDiscipline']['TermAwardScheme'] = "學期獎勵計劃";
$Lang['eDiscipline']['NoMeritTypeInUse'] = "沒有獎勵項目使用中";
$Lang['eDiscipline']['NoDemeriTypeInUse'] = "沒有懲罰項目使用中";
$Lang['eDiscipline']['AverageScore'] = "平均分";
$Lang['eDiscipline']['totalStudents'] = "班人數";

$Lang['eDiscipline']['uccke_MeritScore'] = "獎懲分";
$Lang['eDiscipline']['uccke_ConductGrade_Instruction'] = "請選擇要計算的年度、學期及班別。";


$eDiscipline['MaxRecordDayBeforeAllow'] = "容許輸入過去的訓導紀錄";
$eDiscipline['MaxRecordDayBeforeAllow_Day'] = "日";
$eDiscipline['JSWarning']['RecordDateIsNotAllow'] = "事件日期無效";
$eDiscipline['Import']['Error']['RecordDateIsNotAllow'] = "紀錄日期無效";
$Lang['eDiscipline']['AutoReleaseForAPRecord'] = "於批核後自動開放獎勵及懲罰紀錄";
$Lang['eDiscipline']['AutoReleaseAGM2AP'] = "自動開放轉換自 累積良好行為/違規紀錄 的 獎懲紀錄";
$Lang['eDiscipline']['AutoReleasedToStudent'] = "紀錄已自動開放予學生";
$Lang['eDiscipline']['JSWarning']['EventDateIsNotAllow'] = "發生日期無效";
$Lang['eDiscipline']['DuplicateStudent'] = "學生重複";
$Lang['eDiscipline']['JSWarning']['DetenedRecordCannotCancel'] = "已留堂紀錄不能取消";
$Lang['eDiscipline']['JSWarning']['AbsentRecordCannotCancel'] = "缺席紀錄不能取消";
$Lang['eDiscipline']['RejectRecordEmailNotification'] = "於獎勵及懲罰紀錄不批核後自動發送電郵通知";

$Lang['eDiscipline']['PrintNotice'] = "列印通告";
$Lang['eDiscipline']['PrintNewPagePerClass'] = "列印時每班作新分頁";

$Lang['eDiscipline']['ConductMark'] = "操行分";
$Lang['eDiscipline']['displayConductMark'] = "顯示操行分";
$Lang['eDiscipline']['displayConductMarkAndSubScore'] = "顯示操行分及勤學分";
$Lang['eDiscipline']['displayRecordRemarks'] = "顯示備註";
$Lang['eDiscipline']['GM_WeeklyReport'] = "良好及違規行為每週報告";
$Lang['eDiscipline']['Interval'] = "差額";
$Lang['eDiscipline']['ThisGmTimes'] = "是次紀錄次數";
$Lang['eDiscipline']['TotalTimes'] = "總次數";
$Lang['eDiscipline']['TotalNotSubmitTimes'] = "總欠交次數";
$Lang['eDiscipline']['NotSubmit'] = "欠交";
$Lang['eDiscipline']['MeetInterval'] = "達標差額";
$Lang['eDiscipline']['AP_AmountReport'] = "獎勵及懲罰數量報告";
$Lang['eDiscipline']['WSCSS_MisconductReport'] = "違規行為報告";
$Lang['eDiscipline']['StudyScoreReport'] = "勤學分報告";
$Lang['eDiscipline']['MissStudyScoreRange'] = "請輸入勤學分範圍";
$Lang['eDiscipline']['StudyScoreStatistics'] = "勤學分統計";

$Lang['eDiscipline']['AccessRight_General_Right'] = "基本權限";
$Lang['eDiscipline']['AccessRight_GM_Right'] = "良好及違規行為權限";
$Lang['eDiscipline']['AccessRight_AP_Right'] = "獎勵及懲罰權限";
$Lang['eDiscipline']['AccessRight_UserList'] = "成員列表";
$Lang['eDiscipline']['UnselectAll'] = "取消全選";
$Lang['eDiscipline']['SelectAll'] = "全選";
$Lang['eDiscipline']['Items'] = "項目";
$Lang['eDiscipline']['DisplayOtherTeacherGrade'] = "於「操行評級」顯示其他科目老師的評級";
$Lang['eDiscipline']['SubmitAndNext'] = "呈送並繼續";
$Lang['eDiscipline']['Follow'] = "依循";
$Lang['eDiscipline']['ConductMarkReport'] = "操行分報告";
$Lang['eDiscipline']['MissConductMarkRange'] = "請輸入操行分範圍";
$Lang['eDiscipline']['StartMarkLargeThenEndMark'] = "開始分數不可大於結束分數";

$iDiscipline['Good_Conduct_No_Conduct_Item'] = "沒有此行為項目 / 此行為項目沒有權限";
$iDiscipline['AP_No_Access_Right'] = "此項目沒有權限";

$Lang['eDiscipline']['FieldTitle']['SeriousLate'] = "嚴重遲到";
$Lang['eDiscipline']['FieldTitle']['LateMinutes'] = "遲到分鐘";
$Lang['eDiscipline']['SendEmailtoClassTeacherWhenStudentTriggerWarningReminderPoint'] = "如有學生觸發操行分提示分數, 發出電郵通知班主任";
$Lang['eDiscipline']['SendEmailtoDisciplineAdminWhenStudentTriggerWarningReminderPoint'] = "如有學生觸發操行分提示分數, 發出電郵通知".$eDiscipline["DisciplineAdmin2"];
$Lang['eDiscipline']['EMAIL_CONTENT_WARNING_RULE_str'] = "學生 %%_student_name_%% 的操行分 %%_to_score_%% 少於操行分提示分數 %%_reminder_point%%, 相關的紀錄如下:<br>\n<br>\n";
$Lang['eDiscipline']['EMAIL_CONTENT_SUBSCORE_WARNING_RULE_str'] = "學生 %%_student_name_%% 的勤學分 %%_to_score_%% 少於操行分提示分數 %%_reminder_point%%, 相關的紀錄如下:<br>\n<br>\n";

$Lang['eDiscipline']['Report'] = "報告";
$Lang['eDiscipline']['RankingReport'] = "排名報告";
$Lang['eDiscipline']['RankingDetailReport'] = "排名詳細報告";

$Lang['eDiscipline']['NeedToReassessInConductMeeting'] = "若第一欄\"註\"顯示「<font color='red'>*</font>」即表示該生某些項目之評級須於操行作討論。";
$Lang['eDiscipline']['InvalidConductCategory'] = "行為類別無效";
$Lang['eDiscipline']['CallForActionAfterEffectiveDate'] = "於特定日數後「等待批核」的獎勵及懲罰紀錄會被要求跟進";
$Lang['eDiscipline']['AP_ActionDueDate'] = "跟進限期";
$Lang['eDiscipline']['ActionDateCompareWithRecordDate'] = $i_RecordDate."需等如或早於".$Lang['eDiscipline']['AP_ActionDueDate'];
$Lang['eDiscipline']['PassedActionDueDate'] = "已過跟進限期";
$Lang['eDiscipline']['1stPeriod'] = "時段一";
$Lang['eDiscipline']['2ndPeriod'] = "時段二";
$Lang['eDiscipline']['3rdPeriod'] = "時段三";
$Lang['eDiscipline']['MFBMCLCT_ConductGradeReport'] = "年度操行評級報告";
$Lang['eDiscipline']['totalNoOf'] = "總數";
$Lang['eDiscipline']['1stTermGrade'] = "上學期操行分";
$Lang['eDiscipline']['2ndTermGrade'] = "下學期操行分";
$Lang['eDiscipline']['Appearance'] = "儀容";
$Lang['eDiscipline']['DateCannotInPast'] = "留堂日期不能早於今天";
$Lang['eDiscipline']['PIC_Not_Available'] = "負責人時間不允許";
$Lang['eDiscipline']['AbsentInConductGradeMeeting'] = "「操行評級會議」缺席名單";
$Lang['eDiscipline']['AbsentList'] = "缺席名單";
$Lang['eDiscipline']['MarkingType'] = "評分類別";
$Lang['eDiscipline']['Marking_NA'] = "不適用";
$Lang['eDiscipline']['Marking_OriginalGrade'] = "原有評級";
$Lang['eDiscipline']['SpecialCaseRevised'] = "特別個案(已完成)";

# eDis 20100125
$Lang["eDiscipline"]["ByYear"] = "按年";
$Lang["eDiscipline"]["BySemester"] = "按學期";
$Lang["eDiscipline"]["ByMonth"] = "按月份(整個月)";
$Lang["eDiscipline"]["BySeparatedMonth"] = "按月份 (學期分開)";
$Lang["eDiscipline"]["ConversionPeriod"] = "轉換時段";

$Lang['eDiscipline']['DisplayMode'] = "顯示形式";
$Lang['eDiscipline']['DisplayInSummary'] = "摘要";
$Lang['eDiscipline']['DisplayInDetails'] = "詳細";

$Lang['eDiscipline']['NotAllowSameItemInSameDay'] = "不容許相同學生於同一天內有「".$eDiscipline['Award_and_Punishment']."」或「".$eDiscipline['Good_Conduct_and_Misconduct']."」相同的項目紀錄";
$Lang['eDiscipline']['PaymentNoticeChecking'] = "請輸入所有項目資料。";
$Lang['eDiscipline']['ItemAlreadyAdded'] = "已有相同項目在同一".$i_RecordDate."內";

$Lang['eDiscipline']['DisplaySchoolLogo'] = "顯示校徽";
$Lang['eDiscipline']['DisplayWaivedRecord'] = "顯示豁免紀錄";
$Lang['eDiscipline']['DisplayWarningRecord'] = "顯示警告紀錄";

$Lang['eDiscipline']['APSetting_BatchEmptyInput'] = "不容許加入學生獎懲紀錄時沒有選擇獎懲內容";
$Lang['eDiscipline']['EmptyInput'] = "";
$Lang['eDiscipline']['IsSelected'] = "沒有撰擇";
$Lang['eDiscipline']['IsEntered'] = "沒有輸入";

$Lang['eDiscipline']['AddNotice'] = "新增通告";
$Lang['eDiscipline']['DeleteNotice'] = "刪除通告";
$Lang['eDiscipline']['ChangeAPItemStatusWarning'] = "部份".$eDiscipline['Good_Conduct_and_Misconduct']."的".$eDiscipline['Setting_Period']."正使用此項目，不能更改其狀況。";
$Lang['eDiscipline']['AddToDiscipline'] = "新增至\"".$ip20TopMenu['eDisciplinev12']."\"的懲罰紀錄";
$Lang['eDiscipline']['NewLeafTypeA'] = "類別A";
$Lang['eDiscipline']['NewLeafTypeB'] = "類別B";

$Lang['eDiscipline']['OnlyAdminSelectPIC'] = "只允許管理人員選擇紀錄負責人";
$Lang['eDiscipline']['PIC_SelectionDefaultOwn'] = "預設於\"良好及違規行為\"、\"獎勵及懲罰\"、及\"個案紀錄\"頁面，顯示當前用戶的紀錄<br>(選\"否\"以顯示所有用戶的紀錄)";

$Lang['eDiscipline']['SelectionMode'] = "選擇模式";
$Lang['eDiscipline']['RecordisAutoReleased'] = "紀錄自動開放";
$Lang['eDiscipline']['RecordisAutoApproved'] = "紀錄自動批核";
$Lang['eDiscipline']['RecordCreatedBy'] = "紀錄新增於";
$Lang['eDiscipline']['LastCaseNumberRef'] = "曾使用過的格式";
$Lang['eDiscipline']['AddMisconductForAbsent'] = "新增違規行為記錄于缺席的學生";
$Lang['eDiscipline']['AddPunishmentForAbsent'] = "新增懲罰記錄于缺席的學生";
$Lang['eDiscipline']['TeacherType'] = "教師類別";
$Lang['eDiscipline']['AddPunishmentToSelectedStudent'] = "新增懲罰記錄于已選取的學生";

$Lang['eDiscipline']['DefaultSetDetention'] = "預設勾選留堂選項";
$Lang['eDiscipline']['DefaultSendNotice'] = "預設勾選發送電子通告選項";

$Lang['eDiscipline']['Import_GM_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_CategoryName'];
$Lang['eDiscipline']['Import_GM_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Period'];
$Lang['eDiscipline']['Import_GM_Col'][] = "<span class='tabletextrequire'>*</span>".$iDiscipline['MeritType'];
$Lang['eDiscipline']['Import_GM_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Status'];
$Lang['eDiscipline']['Import_GM_Col'][] = $Lang['eDiscipline']['DefaultSetDetention'];
$Lang['eDiscipline']['Import_GM_Col'][] = $Lang['eDiscipline']['DefaultSendNotice'];
$Lang['eDiscipline']['Import_GM_Col'][] = "電子通告模板";
$Lang['eDiscipline']['Import_GM_Col'][] = "嚴重遲到";
$Lang['eDiscipline']['Import_AlreadyExist'] = "已存在";
$Lang['eDiscipline']['Import_DoesNotExist'] = "不存在";
$Lang['eDiscipline']['Import_NoDefaultCat'] = "選擇預設類別不可匯入新項目";
$Lang['eDiscipline']['ImportCategory'] = "匯入類別";
$Lang['eDiscipline']['ImportItem'] = "匯入項目";
$Lang['eDiscipline']['Import_GM_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$i_Discipline_System_ItemCode;
$Lang['eDiscipline']['Import_GM_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_ItemName'];
$Lang['eDiscipline']['Import_GM_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Category'] ;
$Lang['eDiscipline']['Import_GM_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Status'];
$Lang['eDiscipline']['Import_AP_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_ItemName'];
$Lang['eDiscipline']['Import_AP_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Type'];
$Lang['eDiscipline']['Import_AP_Col'][] = "<span class='tabletextrequire'>*</span>狀況";
$Lang['eDiscipline']['Import_AP_Col'][] = $Lang['eDiscipline']['DefaultSetDetention'];
$Lang['eDiscipline']['Import_AP_Col'][] = $Lang['eDiscipline']['DefaultSendNotice'];
$Lang['eDiscipline']['Import_AP_Col'][] = "電子通告模板";
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$i_Discipline_System_ItemCode;
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_ItemName'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$iDiscipline['Accumulative_Category'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Conduct_Mark'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$i_Discipline_System_Subscore1;
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$Lang['eDiscipline']['ActivityScore'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$i_Discipline_System_Quantity;
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline["Type"];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Status'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "標籤";
$Lang['eDiscipline']['AP_Import_SubScore_Negative'] = $i_Discipline_System_Subscore1."應少於0";
$Lang['eDiscipline']['AP_Import_SubScore_Positive'] = $i_Discipline_System_Subscore1."應大於0";
$Lang['eDiscipline']['ImportRemarks_ClassnameClassNumber_Userlogin'] = "<span class='tabletextrequire'>^</span>請使用「班別及學號」或「內聯網帳號」";
$Lang['eDiscipline']['EventDate'] = "事件日期";
$Lang['eDiscipline']['EventDateChi'] = "事件日期(中文)";
$Lang['eDiscipline']['EventDateEng'] = "事件日期(英文)";
$Lang['eDiscipline']['EventRecordDate'] = "事件記錄日期";
$eDiscipline['JSWarning']['RecordInputDateIsNotAllow'] = "事件記錄日期無效";
$iDiscipline['Award_Punishment_Missing_EventRecordDate'] = "沒有事件記錄日期";
$Lang['eDiscipline']['NoticeCopies'] = "通告例印份數";
$Lang['eDiscipline']['NoticeRemark'] = "通告描述";
$Lang['eDiscipline']['ConductMarkSummary'] = "操行分摘要";
$Lang['eDiscipline']['TotalNo'] = "總數";
$Lang['eDiscipline']['TotalNoConverted'] = "總數 (轉換後資料)";
$Lang['eDiscipline']['TotalNoOverallConverted'] = "總數 (轉換後總結資料)";
$Lang['eDiscipline']['ConductMarkRemark'] = "加分／扣分描述";
$Lang['eDiscipline']['AddedMeritRecord'] = "記功 ";
$Lang['eDiscipline']['AddedDemeritRecord'] = "記過";
$Lang['eDiscipline']['UnwaiveRecord'] = "取消「豁免」";

# Notes for import
$Lang['eDiscipline']['ImportSourceFileNote'] = "你可於\"資料檔案\"欄輸入CSV或TXT資料檔的路徑，或按\"瀏覽\"，從你的電腦中選取資料檔。";
$Lang['eDiscipline']['Award_Punishment_Import_Instruct_Note_1_1'] = "你需於CSV或TXT資料檔中以項目編號形式，指定每個紀錄所屬之獎懲項目。你可以";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_2_1'] = "請使用YYYY-MM-DD或D/M/YYYY或DD/MM/YYYY格式，於\"".$Lang['eDiscipline']['EventDate']."\"欄中輸入紀錄日期。";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_c_1'] = "儲存CSV或TXT資料檔。輸入資料檔的路徑，或按\"瀏覽\"，從你的電腦中選取資料檔，然後按<strong>繼續</strong>。";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_1'] = "你需於CSV或TXT資料檔中以項目編號形式，指定每個紀錄所屬之良好及違規行為項目。你可以";
$Lang['eDiscipline']['Case_Record_Import_Instruct_Note_1_1'] = "請使用YYYY-MM-DD格式，於\"".$Lang['eDiscipline']['EventDate']."\"欄中輸入紀錄日期。";

# Notes for GM setting
$Lang['eDiscipline']['Accumulative_Period_In_Use_Warning_GoodConduct'] = "該時段已有良好紀錄，新設定將只對往後新增之紀錄有效。";
$Lang['eDiscipline']['Accumulative_Period_In_Use_Warning_Misconduct'] = "該時段已有違規紀錄，新設定將只對往後新增之紀錄有效。";

$Lang['eDiscipline']['RecordGenerated'] = "產生紀錄";
$Lang['eDiscipline']['ConvertedTo'] = "轉換成";
$Lang['eDiscipline']['MissingStudyScore'] = "沒有勤學分";
$Lang['eDiscipline']['StudyScoreShouldBeNegative'] = "勤學分應少於0";
$Lang['eDiscipline']['StudyScoreShouldBePositive'] = "勤學分應大於0";
$Lang['eDiscipline']['StudyScoreOutOfRange'] = "勤學分超出了範圍";
$Lang['eDiscipline']['ConductMarkStudyScore'] = "操行分<br>及勤學分";
$Lang['eDiscipline']['SubScoreWarningEmail'] = "如有學生觸發勤學分提示分數, 發出電郵通知班主任";
$Lang['eDiscipline']['SubScoreWarningEmailToDisciplineAdmin'] = "如有學生觸發勤學分提示分數, 發出電郵通知模組管理員";
$Lang['eDiscipline']['js_affect_eAttendance_Alert'] =  "操行管理系統能夠自動接收來自考勤管理系統的遲到紀錄，並作為訓導紀錄，加入到「遲到」類別下。如更改「遲到」類別成「不使用」，考勤管理系 統將無法傳送有關紀錄到操行管理系統。你是否要繼續？";
$Lang['eAttendance']['eDisciplineCategoryNotInUseWarning'] = "由於操行管理系統內的「遲到」良好及違規行為類別已經被更改成「不使用」，考勤管理系統將無法傳送遲到紀錄到操行管理系統。如要傳送紀錄，請前往操行管理系統，將「遲到」類別的狀況恢復到「使用中」。";
$Lang['eDiscipline']['ReCalculateAccumulatedRecord'] = "重新計算累計紀錄";
$Lang['eDiscipline']['ReCalculateAccumulatedRecordWarning'] = "重新計算累計紀錄將會移除已選學生獎懲紀錄相關的留堂紀錄及通告。如確定要重新計算，請按<font style='font-size:large'>".$Lang['Btn']['Submit']."</font>";
$Lang['eDiscipline']['ReCalculateJsAlert'] = "重新計算累計紀錄將會移除已選學生獎懲紀錄相關的留堂紀錄及通告。你是否確定要重新計算累計紀錄？";
$Lang['eDiscipline']['AwardPunishmentInterfaceFile'] = "獎勵及懲罰介面檔案";
$Lang['eDiscipline']['FieldsInInterfaceFile'] = array("WebSAMS學生註冊編號", "學年", "獎懲日期", "獎懲類別", "內容文字", "內容代碼", "獎懲級別一", "獎懲級別二", "獎懲級別三", "獎懲級別四", "獎懲級別五", "操行分", "負責人代碼", "成績表列印示標", "成績表列印次序", "獎勵出處代碼", "獎勵類別代碼", "前置補充", "後置補充", "備註", "其他跟進代碼", "其他跟進開始日期", "其他跟進結束日期", "留堂班示標", "學生學習槪覽可讀取示標", "學生學習槪覽的備註", "其他語言", "其他前置補充", "其他後置補充", "其他內容");
$Lang['eDiscipline']['FieldsInInterfaceFile_V2'] = array("學生註冊編號", "學年", "獎懲日期", "獎懲類別", "內容文字(英文)", "內容文字(中文)", "內容代碼", "前置補充(英文)", "前置補充(中文)", "後置補充(英文)", "後置補充(中文)", "獎懲級別一", "獎懲級別二", "獎懲級別三", "獎懲級別四", "獎懲級別五", "操行分", "負責人代碼", "成績表列印示標", "成績表列印次序", "獎勵出處代碼", "獎勵類別代碼", "備註", "其他跟進代碼", "其他跟進開始日期", "其他跟進結束日期", "留堂班示標", "學生學習概覽可讀取示標", "學生學習概覽的備註");
$Lang['eDiscipline']['NormalExport'] = "一般";
$Lang['eDiscipline']['WebSAMSFormatExport'] = "WebSAMS格式";
$Lang['eDiscipline']['WebSAMSSystemMode'] = "WebSAMS系統制度";
$Lang['eDiscipline']['WebSAMSSystemMode_All'] = "所有制度";
$Lang['eDiscipline']['WebSAMSSystemMode_Conduct'] = "操行分制度";
$Lang['eDiscipline']['WebSAMSSystemMode_Merit'] = "功過制度";
$Lang['eDiscipline']['ReceiveTimes'] = "次數";
$Lang['eDiscipline']['Import_GM_Column'] = array("班別","班號","事件日期","項目編號");
$Lang['eDiscipline']['Import_GM_Column_With_Times'] = array("班別","班號","事件日期","項目編號","次數","負責人","備註");
$Lang['eDiscipline']['Import_GM_Reference'][3] = array("按此查詢項目編號", "ItemCode", "ic_click");
if($sys_custom['eDiscipline_GM_Times']){
	$Lang['eDiscipline']['Import_GM_Column'] = array_merge($Lang['eDiscipline']['Import_GM_Column'],array("次數"));
}
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$Lang['eDiscipline']['Import_GM_Column'] = array_merge($Lang['eDiscipline']['Import_GM_Column'],array("分數"));
	$Lang['eDiscipline']['Import_GM_Remark'][$sys_custom['eDiscipline_GM_Times']?5:4] = "只適用於違規行為項目，請使用正數，0表示不使用。";
}
$Lang['eDiscipline']['Import_GM_Column'] = array_merge($Lang['eDiscipline']['Import_GM_Column'],array("負責人","備註"));
//if($sys_custom['eDiscipline_GM_Times'] )
	//	$Lang['eDiscipline']['Import_GM_Reference'][4] = array("按此查詢次數", "Receive", "rc_click");	# add to cust lang
$Lang['eDiscipline']['Import_GM_Remark'][2] = "格式 : YYYY-MM-DD 或 D/M/YYYY 或  DD/MM/YYYY";
$Lang['eDiscipline']['Import_GM_Error'][1] = "次數空白";
$Lang['eDiscipline']['Import_GM_Error'][2] = "次數不正確";
$Lang['eDiscipline']['Import_GM_Error'][3] = "分數不正確";
$Lang['eDiscipline']['UserList'] = "用戶列名單";
$Lang['eDiscipline']['Rights'] = "權限";
$Lang['eDiscipline']['CategoryCanBeApproved'] = "可批核的類別";
$Lang['eDiscipline']['CategorySelected'] = "已選類別";
$Lang['eDiscipline']['AllAwardPunishment'] = "所有獎勵及懲罰";
$Lang['eDiscipline']['AllGoodMisConduct'] = "所有良好及違規行為";
##### eDiscipline - Updated from IP20
$i_Discipline_Vacancy = "名額";
$i_Discipline_Invalid_Vacancy = "名額不正確";
$i_Discipline_Vacancy_Is_Not_Enough = "名額不足。";
$eDiscipline['Detention_Session_Default_String'] =  "$i_Discipline_Date | $i_Discipline_Time | $i_Discipline_Location | $i_Discipline_Vacancy | $i_Discipline_PIC";

##### eEnrolment (updated from ip20)
$eEnrollment['ToBeConfirmed'] = "待定";
$eEnrollment['invalid_admin_role'] = "職員角色不正確";
$eEnrollment['Member_Import_FileDescription'] = "\"ClassName\", \"ClassNumber\", \"Role\", \"Performance\", \"Achievement\", \"Comment\", \"Active/Inactive\", \"AdminRole\"";
$eEnrollment['MemberWithMerit_Import_FileDescription'] = "\"ClassName\", \"ClassNumber\", \"Role\", \"Performance\", \"Achievement\", \"Comment\", \"Active/Inactive\", \"AdminRole\", \"Merit\", \"Minor Merit\", \"Major Merit\", \"Super Merit\", \"Ultra Merit\"";
$eEnrollment['All_Member_Import_FileDescription'] = "\"ClubName(EN)\", \"Term\", \"ClassName\", \"ClassNumber\", \"Role\", \"Performance\", \"Achievement\", \"Comment\", \"Active/Inactive\", \"AdminRole\"";
$eEnrollment['All_MemberWithMerit_Import_FileDescription'] = "\"ClubName(EN)\", \"Term\", \"ClassName\", \"ClassNumber\", \"Role\", \"Performance\", \"Achievement\", \"Comment\", \"Active/Inactive\", \"AdminRole\", \"Merit\", \"Minor Merit\", \"Major Merit\", \"Super Merit\", \"Ultra Merit\"";
$eEnrollment['All_Member_Import_FileDescription_AdminRoleRemarks'] = "職員角色輸入備註";
$eEnrollment['AdminRole'] = "職員角色";
$eEnrollment['All_Member_Import_FileDescription_PICTitle'] = "PIC";
$eEnrollment['All_Member_Import_FileDescription_HelperTitle'] = "HELPER";
$eEnrollment['All_Member_Import_FileDescription_NATitle'] = "N.A.";
$eEnrollment['All_Member_Import_FileDescription_PICRemarks'] = "負責人";
$eEnrollment['All_Member_Import_FileDescription_HelperRemarks'] = "點名助手";
$eEnrollment['All_Member_Import_FileDescription_NARemarks'] = "刪除現有的職員角色";
$eEnrollment['All_Member_Import_FileDescription_UnchangeRemarks'] = "如沒有輸入職員角色，系統將不會更改學生現有的職員角色";
$eEnrollment['All_Member_Import_FileDescription_TermRemarksTitle'] = "學期輸入備註";
$eEnrollment['All_Member_Import_FileDescription_TermTile'] = "Term";
$eEnrollment['All_Member_Import_FileDescription_TermRemarks'] = "(如留空即代表全年)";
$eEnrollment['All_Participant_Import_FileDescription'] = "\"ActivityTitle\", \"ClassName\", \"ClassNumber\", \"Role\", \"Performance\", \"Achievement\", \"Comment\", \"Active/Inactive\", \"AdminRole\"";
$eEnrollment['All_Participant_Import_FileDescription_WithReason'] = "\"ActivityTitle\", \"ClassName\", \"ClassNumber\", \"Role\", \"Performance\", \"Achievement\", \"Comment\", \"Active/Inactive\", \"AdminRole\", \"Reason\"";


if($sys_custom['eDiscipline']['Conduct_Grade']['MWYY']) {
	$Lang['eDiscipline']['MWYY']['ConductGradeReport'] = "操行評級報告";
	$Lang['eDiscipline']['MWYY']['ConductGrade'] = "操行評級";
	$Lang['eDiscipline']['MWYY']['TotalMarksDeducted'] = "扣分總數";
	$Lang['eDiscipline']['MWYY']['MarksDeductedNonHW'] = "非功課扣分總數";
	$Lang['eDiscipline']['MWYY']['Reasons'] = "原因";
	$Lang['eDiscipline']['MWYY']['Demerits'] = "懲罰";
	$Lang['eDiscipline']['MWYY']['MP'] = "MP";
	$Lang['eDiscipline']['MWYY']['MU'] = "MU";
	$Lang['eDiscipline']['MWYY']['MH'] = "MH";
	$Lang['eDiscipline']['MWYY']['Remarks'] = "備註";

}

if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Title'] = "學生考勤及獎懲總紀錄";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['StudentName'] = "學生姓名";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ClassName'] = "班別";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['StatisticsDate'] = "資料統計日期";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['To'] = "至";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsenceRecords'] = "缺席記錄";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Times'] = "次數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsentDate'] = "缺席日期";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Sessions'] = "堂數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['WaiveMarker'] = "銷假示標";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['AbsentStatistics'] = "缺席統計";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalAbsentDaysAndSessions'] = "缺席統日數及節數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Day'] = "日";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Session'] = "節";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalUnwaived'] = "未銷假總次數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateRecords'] = "遲到記錄";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Number'] = "編號";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateDate'] = "遲到日期";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['LateStatistics'] = "遲到統計";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalLate'] = "遲到總次數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Time'] = "次";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ConvertedDemeritNumber'] = "已轉缺點個數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UnconvertedDemeritNumber'] = "尚未轉缺點的遲到次數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductRecords'] = "扣分記錄";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['EventDate'] = "違規日期";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductItem'] = "違規事項類別";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ItemCode'] = "分類編號";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Deducted'] = "扣分值";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MisconductStatistics'] = "扣分統計";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['TotalNumberOfMisconduct'] = "扣分總次數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UnconvertedMisconductScore'] = "尚未轉缺點的扣分分數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Marks'] = "分";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['SeriousPunishmentRecords'] = "較嚴重違規記錄";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['ConversionDate'] = "違規(轉換)日期";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['PunishmentRecords'] = "懲處記錄";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Remark'] = "備註";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['PunishmentStatistics'] = "違規統計";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['DemeritTotal'] = "缺點總數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MinorDemeritTotal'] = "小過總數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MajorDemeritTotal'] = "大過總數";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Unit'] = "個";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Warning'] = "警告";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['BlackMark'] = "缺點";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MinorDemerit'] = "小過";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MajorDemerit'] = "大過";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['SuperDemerit'] = "超過";
	$Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UltraDemerit'] = "極過";

	$Lang['eDiscipline']['EmailNotifictionCust']['RecordType'] = "記錄類型";
	$Lang['eDiscipline']['EmailNotifictionCust']['SendStatus'] = "發送狀況";
	$Lang['eDiscipline']['EmailNotifictionCust']['Sent'] = "已發出";
	$Lang['eDiscipline']['EmailNotifictionCust']['NotSent'] = "未發出";
	$Lang['eDiscipline']['EmailNotifictionCust']['RecordTypeArr'] = array("","已達五次遲到記錄","遲到六次轉記缺點","違規記錄已扣5分","扣分達六分或以上轉記缺點","違反校規被處分");
	$Lang['eDiscipline']['EmailNotifictionCust']['RecordTypeWarning'] = "請選擇記錄類型。";
	$Lang['eDiscipline']['EmailNotifictionCust']['EmailsSentSuccess'] = "1|=|電郵已成功發送。";
	$Lang['eDiscipline']['EmailNotifictionCust']['EmailsSentUnsuccess'] = "0|=|發送電郵失敗。";
	$Lang['eDiscipline']['EmailNotifictionCust']['SendEmails'] = "發送電郵";
	$Lang['eDiscipline']['EmailNotifictionCust']['PreviewEmails'] = "預覽電郵內容";
	$Lang['eDiscipline']['EmailNotifictionCust']['ClassName'] = "班別";
	$Lang['eDiscipline']['EmailNotifictionCust']['ClassNumber'] = "學號";
	$Lang['eDiscipline']['EmailNotifictionCust']['StudentName'] = "學生姓名";
	$Lang['eDiscipline']['EmailNotifictionCust']['LateNumberRecords'] = "遲到次數報告記錄";
	$Lang['eDiscipline']['EmailNotifictionCust']['MisconductDeductionRecords'] = "扣分報告記錄";
	$Lang['eDiscipline']['EmailNotifictionCust']['PunishmentRecords'] = "獎懲記錄";
	$Lang['eDiscipline']['EmailNotifictionCust']['SentOn'] = "發送於";
	$Lang['eDiscipline']['EmailNotifictionCust']['PleaseNoticeParent'] = "*** 請通知家長 ***";
	$Lang['eDiscipline']['EmailNotifictionCust']['GrowthGroup'] = "學生成長組";
	$Lang['eDiscipline']['EmailNotifictionCust']['Times'] = "次數";
	$Lang['eDiscipline']['EmailNotifictionCust']['Deduction'] = "扣分";
	$Lang['eDiscipline']['EmailNotifictionCust']['Punishment'] = "獎懲值";
	$Lang['eDiscipline']['EmailNotifictionCust']['HeaderArr'] = array("","下列同學已有五次遲到記錄，煩請提醒同學","下列學生遲到六次轉記缺點","下列同學已有多次扣分記錄，煩請提醒同學","下列學生扣分達六分或以上轉記缺點","下列同學因違反校規被處分");
	$Lang['eDiscipline']['EmailNotifictionCust']['TableTitleArr'] = array("","遲到次數報告記錄 [日日/月月/年年]","遲到次數報告記錄 [日日/月月/年年]","扣分報告記錄 (代號) [日日/月月/年年]","扣分報告記錄 (代號) [日日/月月/年年]","獎懲記錄 [日日/月月/年年] (類別) (代號)");
	$Lang['eDiscipline']['EmailNotifictionCust']['EmailTitleArr'] = array(""," 已有五次遲到記錄，煩請提醒同學"," 遲到六次轉記缺點"," 已有多次扣分記錄，煩請提醒同學"," 扣分達六分或以上轉記缺點"," 因違反校規被處分");
	$Lang['eDiscipline']['EmailNotifictionCust']['SemesterTotalDeduction'] = "本學期共扣分數";
	$Lang['eDiscipline']['EmailNotifictionCust']['ConfirmSendEmail'] = "你確定要發送電郵?";
}
if($sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']){
	$Lang['eDiscipline']['IndividualDisciplineRecord']['Title'] = "訓導組個人奬懲紀錄";
	$Lang['eDiscipline']['IndividualDisciplineRecord']['LangSettings'] = "設定使用英文報告學生";
	$Lang['eDiscipline']['IndividualDisciplineRecord']['Period'] = "時段";
	$Lang['eDiscipline']['IndividualDisciplineRecord']['SchoolYear'] = "學年";
	$Lang['eDiscipline']['IndividualDisciplineRecord']['Semester'] = "學期";
	$Lang['eDiscipline']['IndividualDisciplineRecord']['Target'] = "對象";
	$Lang['eDiscipline']['IndividualDisciplineRecord']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
	$Lang['eDiscipline']['APReportCust']['DateRangeType'] = "日期類別";
	$Lang['eDiscipline']['APReportCust']['ModifiedDate'] = "更新日期";
}
if($sys_custom['eDiscipline']['HoYuPrimarySchCust']){
	$Lang['eDiscipline']['HoYuPrimarySchCustReport']['Period'] = "時段";
	$Lang['eDiscipline']['HoYuPrimarySchCustReport']['SchoolYear'] = "學年";
	$Lang['eDiscipline']['HoYuPrimarySchCustReport']['Semester'] = "學期";
	$Lang['eDiscipline']['HoYuPrimarySchCustReport']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
	$Lang['eDiscipline']['DemeritRecordReport']['Title'] = "學生違規紀錄";
	$Lang['eDiscipline']['StudentAnnualDisciplineReport']['Title'] = "全年品行表現報告";
}
if($sys_custom['eDiscipline']['MoPuiChingDailyReport']){
	$Lang['eDiscipline']['MoPuiChingDailyReport']['Title'] = "學生獎勵及懲罰紀錄(黃紙)";
	$Lang['eDiscipline']['MoPuiChingDailyReport']['Date'] = "日期";
	$Lang['eDiscipline']['MoPuiChingDailyReport']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
	$Lang['eDiscipline']['MoPuiChingDailyReport']['PleaseSelectOption'] = "請選擇類別";
	$Lang['eDiscipline']['MoPuiChingDailyReport']['Class'] = "班別";
	$Lang['eDiscipline']['MoPuiChingDailyReport']['Student'] = "學生";
}
if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport']){
	$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['Semester'] = "按本學年學期設定日期範圍";
	$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['AccumulateCount'] = "累計次數";
}
if($sys_custom['eDiscipline']['TKPSchoolRecordSummary']){
	$Lang['eDiscipline']['TKPSchoolRecordSummary']['Title'] = "輔委會學生校內生活紀錄總表";
	$Lang['eDiscipline']['TKPSchoolRecordSummary']['SchoolYear'] = "學年";
	$Lang['eDiscipline']['TKPSchoolRecordSummary']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
}
if($sys_custom['eDiscipline']['TKPClassDemeritRecord']){
	$Lang['eDiscipline']['TKPClassDemeritRecord']['Title'] = "違規班報表";
	$Lang['eDiscipline']['TKPClassDemeritRecord']['Period'] = "時段";
	$Lang['eDiscipline']['TKPClassDemeritRecord']['SchoolYear'] = "學年";
	$Lang['eDiscipline']['TKPClassDemeritRecord']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
}

$Lang['eDiscipline']['BlackMarkNum'] = "缺點總數";
$Lang['eDiscipline']['MinorDemeritNum'] = "小過總數";
$Lang['eDiscipline']['MajorDemeritNum'] = "大過總數";
$Lang['eDiscipline']['MeritNum'] = "優點總數";
$Lang['eDiscipline']['MinorCreditNum'] = "小功總數";
$Lang['eDiscipline']['MajorCreditNum'] = "大功總數";

$Lang['eDiscipline']['DetentionDateChi'] = "留堂日期";
$Lang['eDiscipline']['DetentionDateEng'] = "留堂日期(英文)";

if($sys_custom['UseActScore']) {
	$Lang['eDiscipline']['ActivityScoreGrade'] = "活動分等級";
	$Lang['eDiscipline']['ActivityScoreBaseMark'] = "活動分底分";
	$Lang['eDiscipline']['ActivityScoreLowerBoundary'] = "活動分下限";
	$Lang['eDiscipline']['ActivityScoreIncrement_MAX'] = "活動分調整上限";
	$Lang['eDiscipline']['ActivityScoreIncrement'] = "活動分 (增加)";
	$Lang['eDiscipline']['ActivityScoreDecrement'] = "活動分 (扣減)";
	$Lang['eDiscipline']['MissingActivityScore'] = "沒有活動分";
	$Lang['eDiscipline']['ActivityScoreShouldBeNegative'] = "活動分應少於0";
	$Lang['eDiscipline']['ActivityScoreShouldBePositive'] = "活動分應大於0";
	$Lang['eDiscipline']['ActivityScoreOutOfRange'] = "活動分超出了範圍";
	$Lang['eDiscipline']['ComputeActivityScoreGrade'] = "計算活動分等級";
	$Lang['eDiscipline']['FieldTitle']['ActivityScoreCalculationMethod'] = "活動分累計週期";
	$Lang['eDiscipline']['FieldTitle']['ActivityScoreBaseMark'] = "底分";
	$Lang['eDiscipline']['ActivityScoreWarningEmail'] = "如有學生觸發活動分提示分數, 發出電郵通知班主任";
	$Lang['eDiscipline']['ActivityScoreWarningEmailToDisciplineAdmin'] = "如有學生觸發活動分提示分數, 發出電郵通知模組管理員";
	$Lang['eDiscipline']['EMAIL_CONTENT_ACTSCORE_WARNING_RULE_str'] = "學生 %%_student_name_%% 的活動分 %%_to_score_%% 少於活動分提示分數 %%_reminder_point%%, 相關的紀錄如下:<br>\n<br>\n";
	$Lang['eDiscipline']['GenerateGradeOfActivityScoreInstruction'] = "更改評級準則後，請前往<b>管理 > 活動分</b>，重新計算活動分等級。";
	$Lang['eDiscipline']['jsActivityScoreEditWarning'] = "不論結餘, 所有活動分數將會統一更新成所輸入數目。你是否確定要繼續?";
}
# Teacher App
$Lang['eDiscipline']['App']['RecordHasSentNotice'] = "記錄已發放通告";
$Lang['eDiscipline']['App']['PleaseDeleteLateRecordInAttendance'] = "請於考勤管理內刪除遲到記錄";
$Lang['eDiscipline']['App']['CannotDeleteWaivedRecords'] = "不能刪除豁免記錄";
$Lang['eDiscipline']['App']['RecordDetails'] = "詳細資料";
$Lang['eDiscipline']['App']['PleaseSearchStudent'] = "請搜尋學生";
$Lang['eDiscipline']['App']['SearchStudentUsingName'] = "以學生姓名搜尋";
$Lang['eDiscipline']['App']['SearchStudentUsingClass'] = "以班別學號搜尋 (E.G. 1A15)";
$Lang['eDiscipline']['App']['PleaseSearch'] = "請搜尋";
$Lang['eDiscipline']['App']['SearchPICsUsingName'] = "以姓名搜尋負責人";
$Lang['eDiscipline']['App']['SelectedPICs'] = "已選擇負責人";
$Lang['eDiscipline']['App']['CaseIsFinished'] = "個案已完成";
$Lang['eDiscipline']['App']['RecordIsWavied'] = "紀錄已被豁免";
$Lang['eDiscipline']['App']['RecordIsRejected'] = "紀錄已被拒絕";
$Lang['eDiscipline']['App']['RecordisGoodConduct'] = "紀錄是".$i_Discipline_GoodConduct.".";
$Lang['eDiscipline']['App']['RecordNotPassWaiveDate'] = "紀錄未過豁免期";
$Lang['eDiscipline']['App']['RecordHappenAgain'] = "豁免期內重犯";
$Lang['eDiscipline']['App']['RecordNotEnoughNewLeaf'] = "未有足夠改過遷善的限額.";
$Lang['eDiscipline']['App']['RecordNoNewLeafSetting'] = "未有改過遷善的設定";
$Lang['eDiscipline']['App']['RecordIsPending'] = "紀錄正等待批核";
$Lang['eDiscipline']['App']['RecordIsCase'] = "紀錄屬於個案紀錄。";
$Lang['eDiscipline']['App']['RecordIsRedeemed'] = "紀錄已功過相抵。";
$Lang['eDiscipline']['App']['RecordIsAcc'] = "紀錄由良好/違規行為紀錄組成。";
$Lang['eDiscipline']['App']['noDeleteRight'] = "沒有刪除權限。";
$Lang['eDiscipline']['App']['RecordIsApproved'] = "紀錄已被批核";

##### eSurvey
$Lang['eSurvey']['LearnAndTeach']['LearnAndTeach'] = "學與教";
$Lang['eSurvey']['LearnAndTeach']['BatchCreate'] = "批次新增";
$Lang['eSurvey']['LearnAndTeach']['ToSDAS'] = "轉移至SDAS";
$Lang['eSurvey']['LearnAndTeach']['ExportRaw'] = "匯出數據";
$Lang['eSurvey']['LearnAndTeach']['ExportNotUnansweredUser'] = "匯出作答狀態";
$Lang['eSurvey']['LearnAndTeach']['SubmissionStatus'] = "提交狀態";
$Lang['eSurvey']['LearnAndTeach']['SubmissionStatusY'] = "已提交";
$Lang['eSurvey']['LearnAndTeach']['SubmissionStatusN'] = "未提交";
$Lang['eSurvey']['LearnAndTeach']['SelectData'] = "選擇資料";
$Lang['eSurvey']['LearnAndTeach']['SurveyStartDate'] = "問卷開始日期";
$Lang['eSurvey']['LearnAndTeach']['SurveyEndDate'] = "問卷結束日期";
$Lang['eSurvey']['LearnAndTeach']['ResultAcademicYear'] = "結果所屬學年";
$Lang['eSurvey']['LearnAndTeach']['SurveyTitleHint'] = "問卷必需以 {班別}-{科目縮寫}-{老師} 作為標題，例如：1A-英文-陳大文";
$Lang['eSurvey']['LearnAndTeach']['CurrentData'] = "<!--Year-->系統紀錄";
$Lang['eSurvey']['LearnAndTeach']['Exists'] = "已存在";
$Lang['eSurvey']['LearnAndTeach']['NotExists'] = "未存在";
$Lang['eSurvey']['LearnAndTeach']['WrongRemarks'] = "錯誤原因";
$Lang['eSurvey']['LearnAndTeach']['WrongRemarksDetail']['ERROR_FORMAT'] = "格式錯誤";
$Lang['eSurvey']['LearnAndTeach']['WrongRemarksDetail']['ERROR_CLASS_NAME'] = "找不到班別";
$Lang['eSurvey']['LearnAndTeach']['WrongRemarksDetail']['ERROR_SUBJ_ABBR'] = "找不到科目縮寫";
$Lang['eSurvey']['LearnAndTeach']['WrongRemarksDetail']['ERROR_TEACHER_NAME'] = "找不到教師";
$Lang['eSurvey']['LearnAndTeach']['WrongRemarksDetail']['ERROR_TEACHER_SUBJECT'] = "找不到教師任教科目";
$Lang['eSurvey']['LearnAndTeach']['TransferRemarks'] = "以上錯誤資料將不會轉存至SDAS";
$Lang['eSurvey']['LearnAndTeach']['ConfirmOverwrite'] = "系統已存在資料，是否更新資料？";
$Lang['eSurvey']['LearnAndTeach']['Transfer'] = "轉存";
$Lang['eSurvey']['LearnAndTeach']['SelectedSubjectGroup'] = "已選擇的科組";
$Lang['eSurvey']['public'] = "所有用戶";
$Lang['eSurvey']['SurveyList'] = "問卷調查清單";
$Lang['eSurvey']['StartDate'] = "開始日期";
$Lang['eSurvey']['EndDate'] = "結束日期";
$Lang['eSurvey']['Title'] = "標題";
$Lang['eSurvey']['Creator'] = "製作人";
$Lang['eSurvey']['LastModified'] = "最後修改日期";
$Lang['eSurvey']['NewSurvey'] = "新增問卷";
$Lang['eSurvey']['EditSurvey'] = "編輯問卷";
$Lang['eSurvey']['SurveyPreview'] = "問卷預覽";
$Lang['eSurvey']['SurveyPreviewTitle'] = "問卷預覽";
$Lang['eSurvey']['StartDateWarning'] = "開始日期必須是今天或以後";
$Lang['eSurvey']['PleaseConstructSurvey'] = "請製作問卷";
$Lang['eSurvey']['InvalidSurvey'] = "問卷無效";
$i_con_msg_survey_add = "<font color=green</font>\n";
$i_con_msg_survey_update = "<font color=green>問卷已更新。</font>\n";
$i_con_msg_survey_delete = "<font color=green>問卷已刪除。</font>\n";
$Lang['eSurvey']['SurveyType'] = "問卷對象";
$Lang['eSurvey']['SpecificGroups'] = "指定小組";
$Lang['eSurvey']['WholeSchool'] = "全校";
$Lang['eSurvey']['CurrentSurvey'] = "現有問卷";
$Lang['eSurvey']['PastSurvey'] = "昔日問卷";
$Lang['eSurvey']['Answered'] = "已填";
$Lang['eSurvey']['NotAnswered'] = "未填";
$Lang['eSurvey']['AnsweredSurvey'] = "已完成問卷";
$Lang['eSurvey']['NonAnswerSurvey'] = "待完成問卷";
$Lang['eSurvey']['CompletedDate'] = "完成日期";
$Lang['eSurvey']['Survey'] = "問卷調查";
$Lang['eSurvey']['SurveyResult'] = "問卷調查結果";
$Lang['eSurvey']['ViewSurveyResult'] = "檢視問卷結果";
$Lang['eSurvey']['SurveyForm'] = "問卷";
$Lang['eSurvey']['ViewResults'] = "檢視結果";
$Lang['eSurvey']['Question'] = "問題";
$Lang['eSurvey']['ReplySlipQuestions'] = "回條問題";
$Lang['eSurvey']['Preview'] = "預覽";
$Lang['eSurvey']['SyrveyConstruction'] = "問卷製作";
$Lang['eSurvey']['ReplySlip'] = "回條";
$Lang['eSurvey']['ViewSurvey'] = "檢視相關問卷";
$Lang['eSurvey']['PrintPreiew'] = "列印預覽";
$Lang['eSurvey']['DisplayQuestionNumber'] = "顯示問題編號";
$Lang['eSurvey']['MaxNumberReplySlipOption'] = "回條選項數目上限";
$Lang['eSurvey']['BasicSettings'] = "基本設定";
$Lang['eSurvey']['Settings'] = "設定";
$Lang['eSurvey']['NotAllowReSign'] = "不容許用戶修改回條";
$Lang['eSurvey']['OrderingType'] = "排序";
$Lang['eSurvey']['OrderingTypeNumOfAns'] = "可作答數目";
$Lang['eSurvey']['StudentName'] = "受訪者";
$Lang['eSurvey']['ClassName'] = "班別";
$Lang['eSurvey']['ClassNumber'] = "學號";
$Lang['eSurvey']['ClassAndNumber'] = "班別學號";
$Lang['eSurvey']['SubmissionDate'] = "呈交日期";
$Lang['eSurvey']['ParentSignature'] = "家長簽署";
$Lang['eSurvey']['Year'] = "年";
$Lang['eSurvey']['Month'] = "月";
$Lang['eSurvey']['Day'] = "日";
$Lang['eSurvey']['AM'] = "上午";
$Lang['eSurvey']['PM'] = "下午";
$Lang['eSurvey']['Order'] = "次序";
$Lang['eSurvey']['NoApproStat'] = "統計分析並不適用於此項目";
$Lang['eSurvey']['Hint'] = "如問卷未能正確顯示，請於瀏覽器上清除快取檔案或按Ctrl + F5鍵重試。";
$Lang['eSurvey']['fillallorderingquestion'] = "必須填上此問卷所有已嘗試回答的排序問題";
$Lang['eSurvey']['EditRemarks'] = "如刪除對象組別，該組別已填寫的問卷內容會一併被刪除。";
$Lang['eSurvey']['TemplateRemarks'] = "(只有 <font color=green>標題</font>, <font color=green>內容</font>, 和 <font color=green>回條問題</font> 會成為範本的內容.)";
$Lang['eSurvey']['Target'] = "對象";
$Lang['eSurvey']['TemplateTitle'] = "載入範本標題 ";
$Lang['eSurvey']['MissingTitle'] = "請輸入標題";
$Lang['eSurvey']['MissingDate'] = "沒有輸入日期";
$Lang['eSurvey']['ErrorTemplateTitle'] = "範本標題錯誤";
$Lang['eSurvey']['ErrorStatus'] = "請輸入正確的狀況";
$Lang['eSurvey']['ErrorTarget'] = "請輸入正確的對象";
$Lang['eSurvey']['error']['AllFieldsReq'] = "請輸入Y/N";
$Lang['eSurvey']['error']['DisplayQNum'] = "請輸入Y/N";
$Lang['eSurvey']['error']['RecordType'] = "請輸入Y/N";
$Lang['eSurvey']['ErrorDate'] = '請輸入正確的日期';
$Lang['eSurvey']['ErrorTitle'] = '請輸入正確的標題';
$Lang['eSurvey']['Import']['Mode'] = "匯入模式";
$Lang['eSurvey']['Import']['ImportNew'] = "匯入新項目";
$Lang['eSurvey']['Import']['UpdateExisting'] = "更新現有項目";
$Lang['eSurvey']['Import']['Remarks1'] = "(空白此欄位則必須填寫「標題」)";
$Lang['eSurvey']['Import']['Remarks2'] = "(空白此欄位則必須填寫「結束日期」)";
$Lang['eSurvey']['Import']['Remarks3'] = "(空白此欄位則必須填寫「開始日期」)";
$Lang['eSurvey']['Import']['Remarks4'] = "(填寫「Whole School」/組別中文名稱)";

$Lang['eSurvey']['DefaultAllowSaveDraft'] = "預設允許保存為草稿";
$Lang['eSurvey']['AllowSaveDraft'] = "允許保存為草稿";
$Lang["eSurvey"]["SaveAsDraft"] = "保存為草稿";
$Lang['eSurvey']['Draft'] = "未呈送 (草稿)";

##### eNotice
$Lang['eNotice']['ConfirmForPrint'] = "「最後列印日期」將會被更新, 確定列印?";
$Lang['eNotice']['AllSignStatus'] = "所有簽署狀態";
$Lang['eNotice']['AllPrintStatus'] = "所有列印狀態";
$Lang['eNotice']['Printed'] = "已列印";
$Lang['eNotice']['NonPrint'] = "未列印";
$Lang['eNotice']['NonSendMsg'] = "未傳送";
$Lang['eNotice']['PrintDate'] = "列印日期";
$Lang['eNotice']['AllNoticeTitle'] = "所有通告標題";
$Lang['eNotice']['PaymentNotice'] = "繳費通告";
$Lang['eNotice']['EnablePaymentNotice'] = "啟動「繳費通告」";
$Lang['eNotice']['InChargeNotice'] = "我負責的通告";
$Lang['eNotice']['MyClassNotice'] = "自己班別的通告";

$Lang['eNotice']['AllAwardRecords'] = "所有獎勵紀錄";
$Lang['eNotice']['AllPunishRecords'] = "所有懲罰紀錄";
$Lang['eNotice']['AllGoodConductRecords'] = "所有良好行為紀錄";
$Lang['eNotice']['AllMisconductRecords'] = "所有違規行為紀錄";
$Lang['eNotice']['PaymentItemName'] = "繳費項目名稱";

$Lang['eNotice']['NotEnoughBalance'] = "你的帳戶餘額不足，請增值。";
$Lang['eNotice']['ViewNotice'] = "檢視通告";
##### From IP20
$i_UserPhotoGuide = "相片只能以'.JPG'的格式上載，相片大小也規定為100 X 130 pixel (闊 x 高)。";
$eEnrollment['student_quota_exceeded'] = "超過學生想參與的學會上限。";
$eEnrollment['student_quota_exceeded_activity'] = "超過學生想參與的活動上限。";

# iForm
$i_Form_pls_fill_in_title = "請輸入題目!";
$i_Form_pls_specify_option_num = "請為此填寫方式選擇數目!";

# SLS Library
$Lang['SLSLibrary'] = "SLS圖書館系統";
$Lang['SLSSearch'] = "檢索";
$Lang['SLSInfo'] = "一般資料";

# SMS
$i_SMS['SMSStatus'][0] = '等待中';
$i_SMS['SMSStatus'][1] = '傳送中';
$i_SMS['SMSStatus'][2] = '已傳送';
$i_SMS['SMSStatus'][4] = '傳送失敗';
$i_SMS['SMSStatus'][5] = '已預定傳送';
$i_SMS['SMSStatus']['-39'] = '沒有此短訊要求';

$i_SMS['SMSStatus']['-1'] = '學校代號錯誤';
$i_SMS['SMSStatus']['-2'] = '學校短訊登入帳戶或密碼錯誤';
$i_SMS['SMSStatus']['-4'] = '短訊帳戶被鎖上';
$i_SMS['SMSStatus']['-31'] = '短訊參考編號錯誤';
$i_SMS['SMSStatus']['-32'] = '短訊類別誤';
$i_SMS['SMSStatus']['-99'] = '未能接駁到短訊中心';
$i_SMS['SMSStatus']['-100'] = '互聯網接駁出現錯誤';
$i_SMS['SMSStatus']['FailToSendSMS'] = '傳送失敗';

$i_SMS['RefreshAllStatus'] = '重新整理所有狀況';
$i_SMS['PleaseContactBLForEnquiry'] = '請到 "短訊服務 > 檢視已發送訊息" 檢視短訊狀況及與博文教育技術支援部聯絡';
$i_SMS['MultipleStatus'] = '多項狀況';

$i_SMS_Notice_Show_Student_With_MobileNo_Only = "不顯示沒有流動電話號碼的收件人";

$Lang['SMS']['SMSFeeNotes'] = "<li>短訊服務為一內建功能。你可隨時在此發送短訊。貴校將會按月獲寄月結單，每個短訊收費港幣$<!--FeeValue-->。</li>";
$Lang['SMS']['SMSFeeNotes'] .= "<li>短訊不一定能即時傳送到所有手機（如對方的手機正在關機模式），但系統會定期嘗試，你可在７天內按重新「整理所有狀況」來更新狀況。</li>";
$Lang['SMS']['SMSSentChargeNotes'] = "<li>無論成功接收與否，所有發出的短訊均需收費。</li>";
$Lang['SMS']['SMSUEMONotes'] = "<font color=\"red\"><li>《非應邀電子訊息條例》(下稱「條例」)已全面生效。於條例下，除非得到有關的電話號碼登記使用者的同意，否則電子訊息發送人不可發送任何商業電子訊息到這些號碼。故此，客戶須遵從有關條例，如有任何違反，所構成之一切法律責任將全部由客戶一方承擔。</li></font>";

$Lang['SMS']['LimitationArr'][0]['Content'] = "全英文短訊，最多可輸入160字元";
$Lang['SMS']['LimitationArr'][1]['Content'] = "如短訊包含BIG5編碼之繁體中文字，則最多可輸入70字元(包括中英文字元及標點符號)";
$Lang['SMS']['LimitationArr'][2]['Content'] = "超出字數上限的短訊將被刪除過長部分";
$Lang['SMS']['LimitationArr'][3]['Content'] = "短訊不能顯示所有全形特殊字元包括 「」，、。";
$Lang['SMS']['LimitationArr'][4]['Content'] = "短訊不能顯示半形特殊字元包括 `~!#%^&*()-=_+[]{}\|;':\"/<>";
$Lang['SMS']['LimitationArr'][5]['Content'] = "Unicode編碼之字元可能不能顯示";
$Lang['SMS']['LimitationArr'][6]['Content'] = "接收人可能因以下原因未能收到短信";
$Lang['SMS']['LimitationArr'][6]['SubContentArr'][0] = "i. 收信手機不在網絡內，手機已經關掉";
$Lang['SMS']['LimitationArr'][6]['SubContentArr'][1] = "ii. 手機在我們不支援傳送短信的漫遊網絡內";
$Lang['SMS']['LimitationArr'][6]['SubContentArr'][2] = "iii. 手機在漫遊網絡信號覆蓋差的地方";
$Lang['SMS']['LimitationArr'][6]['SubContentArr'][3] = "iv. 流動網絡營辦商網絡擠塞";
$Lang['SMS']['LimitationArr'][6]['SubContentArr'][4] = "v. 收信人電話號碼錯誤";
$Lang['SMS']['ViewLog'] = "檢視紀錄";
$Lang['SMS']['Send'] = "寄出短訊";
$Lang['SMS']['ReSend'] = "重寄短訊";
$Lang['SMS']['MessageToBeSent'] = "將發出的訊息";
$Lang['SMS']['ConfirmCouting'] = "注意：過長的短訊內容依然可以被寄出，但會按長短而計算為兩條或以上。請留意最右欄位的短訊數目。";
$Lang['SMS']['MessageCutNotes'] = "注意：請留意上表的最右欄位，過長的短訊內容會被刪減，然後寄出。";
$Lang['SMS']['MessageCutted'] = "被刪減出來的部份（若有）";
$Lang['SMS']['RemarkChargeForSMS'] = "每條SMS訊息需繳付費用";
$Lang['SMS']['SmsCreationTime'] = "短訊建立時間";
$Lang['SMS']['ScheduleDateTime'] = "預定發出短訊時間";
$Lang['SMS']['Immediately'] = "即時";
$Lang['SMS']['ScheduledDateIsPast'] = "傳送日期及時間已過，請輸入將來的日期和時間。";
$Lang['SMS']['UsageReportRemarks'] = "只能重新整理最近六個月的短訊數目";

# eHomework
$Lang['SysMgr']['Homework']['Remark']['Remark'] = "備註";
$Lang['SysMgr']['Homework']['Remark']['NoStudent'] = "沒有此學生";
$Lang['SysMgr']['Homework']['Remark']['GroupStudent'] = "此學生不屬於此學科組別";
$Lang['SysMgr']['Homework']['Remark']['RecordsImportedSuccessfully'] = "個資料匯入成功";

$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn'] = "資料欄";
$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Reference'] = "請參考";
$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField'] = "附有「<span class='tabletextrequire'>*</span>」的項目必須填寫";
$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Workload'] = "<span class='tabletextrequire'>^</span> 需時必須為整數, 每半小時為一個單位. 例: 2.5 代表 1.25 小時. (0 為少於0.25 小時, 17 為多於8 小時)";
$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DateFormat'] = "<span class='tabletextrequire'>#</span> 日期的格式必須為 YYYY-MM-DD 或 YYYY/MM/DD. 如你使用 Excel 編輯, 請在日期的資料上更改儲存格格式";

//$Lang['SysMgr']['Homework']['ImportHomework'][] = "<font color=red>*</font> 學科代號";
//$Lang['SysMgr']['Homework']['ImportHomework'][] = "學科";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "<font color=red>*</font> 學科組別代號";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "學科組別";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "<font color=red>*</font> 標題";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "<font color=red>^</font> 工作量";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "老師 <span class='tabletextremark'>(內聯網帳號)</span>";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "<font color=red>*#</font> 開始日期";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "<font color=red>*#</font> 限期";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "<font color=red></font> 內容";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "須繳交";
$Lang['SysMgr']['Homework']['ImportHomework_Col10'] = "是否需要收回有關功課";
$Lang['SysMgr']['Homework']['ImportHomework_Col11'] = "功課類別";
$Lang['SysMgr']['Homework']['Column'] = "欄位";

$Lang['SysMgr']['Homework']['StudentList'] = "學生名單";
$Lang['SysMgr']['Homework']['ImportSubjectLeader'][] = "第一欄 : <font color=red>*</font> 學科組別代號";
$Lang['SysMgr']['Homework']['ImportSubjectLeader'][] = "第二欄 : <font color=red>*</font> 班別";
$Lang['SysMgr']['Homework']['ImportSubjectLeader'][] = "第三欄 : <font color=red>*</font> 班號";
$Lang['SysMgr']['Homework']['UseStartDateToGenerateList'] = "根據開始日期顯示家課項目(否 = 根據結束日期顯示家課項目)";
$Lang['SysMgr']['Homework']['DailyHomeworkList'] = "每日家課紀錄表";
$Lang['SysMgr']['Homework']['MonthlyHomeworkList'] = "每月家課紀錄表";
$Lang['SysMgr']['Homework']['currentMonth'] = "本月";
$Lang['SysMgr']['Homework']['printByClass'] = "列印(以班作單位)";
$Lang['SysMgr']['Homework']['warning']['OutOfCurrentAcademicYear'] = "日期不在本學年範圍!";

# added by Henry on 20100301
$Lang['SysMgr']['Homework']['MonthlyHomeworkListWarningSubjectLeader'] = "每月家課紀錄表並不適用於本學期不是科長的學生用戶。";
$Lang['SysMgr']['Homework']['MonthlyHomeworkListWarning'] = "每月家課紀錄表並不適用於本學期沒有任教任何科目的教師用戶。";

# Export format in eHomework
$Lang['Homework']['ExportColumn']['EN'] = array("Subject Group Code","Subject Group [Ref]","Topic","Workload","Teacher","Start Date","Due Date","Description [Optional]","Hand-in Required [Optional]","Collection Required [Optional]","Homework Type [Optional]");
$Lang['Homework']['ExportColumn']['CH'] = array("學科組別代號","學科組別 [參考用途]","標題","工作量","老師","開始日期","限期","內容 [非必須的]","須繳交 [非必須的]","是否需要收回有關功課 [非必須的]","功課類別 [非必須的]");
$Lang['Homework']['HandinHistoryExportColumn']['EN'] = array("Subject", "Subject Group", "Title", "Teacher", "Start Date", "Due Date", "Class Name", "Class No.", "Student Name", "Current Status");
$Lang['Homework']['HandinHistoryExportColumn']['CH'] = array("學科", "學科組別", "標題", "任教老師", "開始日期", "期限", "班別", "班號", "學生姓名", "現在狀況");

$i_CampusLink_title = "校園連結";

# Role Management
$Lang['SysMgr']['RoleManagement']['StaffAttendance'] = "老師考勤系統管理";

# Staff Attendance V3
$Lang['StaffAttendance']['Management'] = "管理";
$Lang['StaffAttendance']['Attendance'] = "每日紀錄";
$Lang['StaffAttendance']['Roster'] = "值勤表";
$Lang['StaffAttendance']['OTRecords'] = "超時工作紀錄";
$Lang['StaffAttendance']['LeaveAndAbsent'] = "請假及缺席";
$Lang['StaffAttendance']['WorkingDays'] = "工作日";
$Lang['StaffAttendance']['FunctionAccess'] = "存取權限";
$Lang['StaffAttendance']['Terminal'] = "終端機";
$Lang['StaffAttendance']['OT'] = "超時工作規則";
$Lang['StaffAttendance']['LeaveReason'] = "原因";
$Lang['StaffAttendance']['GroupMenuTitle'] = "職員小組及工作時段";
$Lang['StaffAttendance']['Statistics'] = "統計";
$Lang['StaffAttendance']['Settings'] = "設定";
$Lang['StaffAttendance']['Report'] = "報告";
$Lang['StaffAttendance']['StaffAttendance'] = "職員考勤系統";
$Lang['StaffAttendance']['Group'] = "組別";
$Lang['StaffAttendance']['Individual'] = "個人";
$Lang['StaffAttendance']['GroupType'] = "組別類型";
$Lang['StaffAttendance']['NumberOfMember'] = "組員數目";
$Lang['StaffAttendance']['TimeSlot'] = "當值時段";
$Lang['StaffAttendance']['StartTime'] = "開始時間";
$Lang['StaffAttendance']['EndTime'] = "結束時間";
$Lang['StaffAttendance']['DayCount'] = "等於工作";
$Lang['StaffAttendance']['GroupList'] = "組別名單";
$Lang['StaffAttendance']['GroupTitle'] = "組別名稱";
$Lang['StaffAttendance']['GroupTitleDuplicateWarning'] = "*組別名稱重複或空白";
$Lang['StaffAttendance']['GroupRenameSuccess'] = "1|=|組別重新命名成功 ";
$Lang['StaffAttendance']['GroupRenameFail'] = "0|=|組別重新命名失敗 ";
$Lang['StaffAttendance']['WorkingTimeSlot'] = "- 工作當值時段 -";
$Lang['StaffAttendance']['SlotName'] = "當值時段名稱";
$Lang['StaffAttendance']['SlotStart'] = "開始時間";
$Lang['StaffAttendance']['SlotEnd'] = "結束時間";
$Lang['StaffAttendance']['WavieIn'] = "進入時無需拍卡";
$Lang['StaffAttendance']['WavieOut'] = "離開時無需拍卡";
$Lang['StaffAttendance']['AddTimeSlot'] = "新增當值時段";
$Lang['StaffAttendance']['WavieSetting'] = "拍卡設定";
$Lang['StaffAttendance']['SlotNameWarning'] = "*編號重複或尚未輸入編號.";
$Lang['StaffAttendance']['SlotTimeFormatWarning'] = "*時間格式不符";
$Lang['StaffAttendance']['SlotTimeIntervalLimitWarning'] = "*開始及結束時間相距不可大於24小時.";
$Lang['StaffAttendance']['DutyCountFormatWarning'] = "*請輸入由0.1至1之間的數字.";
$Lang['StaffAttendance']['EG'] = "例:";
$Lang['StaffAttendance']['SaveSlotSuccess'] = "1|=|當值時段儲存成功 ";
$Lang['StaffAttendance']['SaveSlotFail'] = "0|=|當值時段儲存失敗 ";
$Lang['StaffAttendance']['DeleteSlotSuccess'] = "1|=|當值時段移除成功 ";
$Lang['StaffAttendance']['DeleteSlotFail'] = "0|=|當值時段移除失敗 ";
$Lang['StaffAttendance']['DeleteSlotConfirmMessage'] = "移除當值時段時，所有相關的值勤設定也會一拼移除。確定要繼續？";
$Lang['StaffAttendance']['Staff'] = '- 成員 -';
$Lang['StaffAttendance']['StaffName'] = '姓名';
$Lang['StaffAttendance']['AddUser'] = '新增成員';
$Lang['StaffAttendance']['AddMemberSuccess'] = "1|=|新增成員成功 ";
$Lang['StaffAttendance']['AddMemberFail'] = "0|=|新增成員失敗 ";
$Lang['StaffAttendance']['DeleteMemberConfirmMessage'] = "當從組別中移除成員時。所有相關的值勤設定也會一拼移除。確定要繼續？";
$Lang['StaffAttendance']['SelectAtLeastOneWarning'] = "請選取最少一名用戶";
$Lang['StaffAttendance']['DeleteMemberSuccess'] = "1|=|成員移除成功 ";
$Lang['StaffAttendance']['DeleteMemberFail'] = "0|=|成員移除失敗 ";
$Lang['StaffAttendance']['DeleteGroupWarning'] = "移除組別會將所有成員已設定係值勤設定一拼移除，確定要繼續？";
$Lang['StaffAttendance']['DeleteGroupSuccess'] = "1|=|組別移除成功 ";
$Lang['StaffAttendance']['DeleteGroupFail'] = "0|=|組別移除失敗 ";
$Lang['StaffAttendance']['AddGroup'] = "新增組別";
$Lang['StaffAttendance']['Normal'] = "正常";
$Lang['StaffAttendance']['Shifting'] = "輪班";
$Lang['StaffAttendance']['SaveGroupSuccess'] = "1|=|組別儲存成功 ";
$Lang['StaffAttendance']['SaveGroupFail'] = "0|=|組別儲存失敗 ";
$Lang['StaffAttendance']['AddMemberWarning'] = "將職員加入組別將移除其個人職務設定。你是否確定要繼續?";
$Lang['StaffAttendance']['StaffWorkingPeriod'] = "僱用期";
$Lang['StaffAttendance']['PeriodStart'] = "開始日期";
$Lang['StaffAttendance']['PeriodEnd'] = "結束日期";
$Lang['StaffAttendance']['AddTWorkingPeriod'] = "新增僱用期";
$Lang['StaffAttendance']['EditWorkingPeriod'] = "編輯僱用期";
$Lang['StaffAttendance']['WorkingPeriodOverlapWarning'] = "*僱用期重疊";
$Lang['StaffAttendance']['WorkingPeriodNullWarning'] = "*請最少輸入開始日期或結束日期";
$Lang['StaffAttendance']['SaveWorkingPeriodSuccess'] = "1|=|僱用期儲存成功 ";
$Lang['StaffAttendance']['SaveWorkingPeriodFail'] = "0|=|僱用期儲存失敗 ";
$Lang['StaffAttendance']['DeleteWorkingPeriodSuccess'] = "1|=|僱用期移除成功 ";
$Lang['StaffAttendance']['DeleteWorkingPeriodFail'] = "0|=|僱用期移除失敗 ";
$Lang['StaffAttendance']['WorkingPeriodEndDateLesserThenStartDateWarning'] = "*結束日期必須在開始日期之後";
$Lang['StaffAttendance']['From'] = "由";
$Lang['StaffAttendance']['To'] = "至";
$Lang['StaffAttendance']['SkipSchoolCalendar'] = "在此類日子(校曆表事件)常規職務不適用";
$Lang['StaffAttendance']['AddDuty'] = "新增職務時段";
$Lang['StaffAttendance']['EffectiveDate'] = "時期";
$Lang['StaffAttendance']['EffectiveDateNullWarning'] = "*請輸入開始日期及結束日期 ";
$Lang['StaffAttendance']['EffectiveEndDateLesserThenStartDateWarning'] = "*結束日期必須在開始日期之後 ";
$Lang['StaffAttendance']['EffectiveDateOverlapWarning'] = "*有效日期重疊";
$Lang['StaffAttendance']['SaveNormalGroupDutyPeriodSuccess'] = "1|=|組別職務時段設定成功 ";
$Lang['StaffAttendance']['SaveNormalGroupDutyPeriodFail'] = "0|=|組別職務時段設定失敗 ";
$Lang['StaffAttendance']['DutyPeriodList'] = "職務時段表";
$Lang['StaffAttendance']['Mon'] = "一";
$Lang['StaffAttendance']['Tue'] = "二";
$Lang['StaffAttendance']['Wed'] = "三";
$Lang['StaffAttendance']['Thur'] = "四";
$Lang['StaffAttendance']['Fri'] = "五";
$Lang['StaffAttendance']['Sat'] = "六";
$Lang['StaffAttendance']['Sun'] = "日";
$Lang['StaffAttendance']['NotAssigned'] = "無需當值";
$Lang['StaffAttendance']['WeekDay'] = "星期";
$Lang['StaffAttendance']['OnDuty'] = "當值";
$Lang['StaffAttendance']['Leave'] = "休假";
$Lang['StaffAttendance']['EditRoster'] = "更改值勤表";
$Lang['StaffAttendance']['NumberOfDutyPeriod'] = "職務時段數目";
$Lang['StaffAttendance']['ApplyTo'] = "應用到";
$Lang['StaffAttendance']['StaffList'] = "職員名單";
$Lang['StaffAttendance']['HideList'] = "開關";
$Lang['StaffAttendance']['DutyOverlapWarning'] = "以下職員被指派的當值時段當值時間重疊 ";
$Lang['StaffAttendance']['DutyOver24HourWarning'] = "以下職員被指派的當值時段超過二十四小時 ";
$Lang['StaffAttendance']['DutyWarning'] = "請修正以上問題 ";
$Lang['StaffAttendance']['DutyPeriodDutySaveSuccess'] = "1|=|職務時段配置儲存成功 ";
$Lang['StaffAttendance']['DutyPeriodDutySaveFail'] = "0|=|職務時段配置儲存失敗 ";
$Lang['StaffAttendance']['DayOfWeekWarning'] = "*請選擇應用到最少一天";
$Lang['StaffAttendance']['DutyApplyDuty'] = "請最少選擇一個當值時段 ";
$Lang['StaffAttendance']['SelectDeSelectAll'] = "取消/ 選取全部";
$Lang['StaffAttendance']['SpecialDateSetting'] = "特別日子設定";
$Lang['StaffAttendance']['Month'] = "月份";
$Lang['StaffAttendance']['DutyCalendar'] = "值勤日曆";
$Lang['StaffAttendance']['StaffSummary'] = "每月概覧";
$Lang['StaffAttendance']['FullDayOutgoing'] = "全日外出工作";
$Lang['StaffAttendance']['FullDayHoliday'] = "全日休假";
$Lang['StaffAttendance']['FullDaySetting'] = "請假紀錄";
$Lang['StaffAttendance']['DutySetting'] = "值勤設定";
$Lang['StaffAttendance']['EditFullDaySetting'] = "更改請假要求";
$Lang['StaffAttendance']['SettingType'] = "類別";
$Lang['StaffAttendance']['FullDaySettingNullWarning'] = "請輸入開始及結束日期";
$Lang['StaffAttendance']['FullDaySettingEndDayBeforeStartWarning'] = "結束日期必須在開始日期以後或相同";
$Lang['StaffAttendance']['StartDate'] = '開始日期';
$Lang['StaffAttendance']['EndDate'] = '結束日期';
$Lang['StaffAttendance']['NoSetting'] = '取消設定';
$Lang['StaffAttendance']['FullDaySettingSaveSuccess'] = '1|=|請假要求儲存成功 ';
$Lang['StaffAttendance']['FullDaySettingSaveFail'] = '0|=|請假要求儲存失敗 ';
$Lang['StaffAttendance']['FullDayReason'] = '原因';
$Lang['StaffAttendance']['Outgoing'] = '出外工作';
$Lang['StaffAttendance']['Leave'] = '請假';
$Lang['StaffAttendance']['Total'] = '總共';
$Lang['StaffAttendance']['TargetDateSelectWarning'] = '請最少選取一天';
$Lang['StaffAttendance']['ShiftDutySaveSuccess'] = '1|=|特別值勤設定成功 ';
$Lang['StaffAttendance']['ShiftDutySaveFail'] = '0|=|特別值勤設定失敗 ';
$Lang['StaffAttendance']['SelectReason'] = '- 選擇原因 -';
$Lang['StaffAttendance']['StaffList'] = '職員名單';
$Lang['StaffAttendance']['SaveDutyPeriodSuccess'] = "1|=|職務時段設定成功 ";
$Lang['StaffAttendance']['SaveDutyPeriodFail'] = "0|=|職務時段設定失敗 ";
$Lang['StaffAttendance']['DeleteDutyPeriodSuccess'] = "1|=|職務時段移除成功 ";
$Lang['StaffAttendance']['DeleteDutyPeriodFail'] = "0|=|職務時段移除失敗 ";
$Lang['StaffAttendance']['DutyPeriodDeleteConfim'] = "確定要移除職務時段？ ";
$Lang['StaffAttendance']['Config'] = "設定";
$Lang['StaffAttendance']['FullTimeSlotOption'] = "全日/ 時段要求";
$Lang['StaffAttendance']['FullDay'] = "全日";
$Lang['StaffAttendance']['TargetDate'] = "日期";
$Lang['StaffAttendance']['DeleteFullDaySettingConfirm'] = "確定要移除請假設定？ ";
$Lang['StaffAttendance']['EditConfirmedDailyRecord'] = "修改已確認值勤紀錄 ";
$Lang['StaffAttendance']['EditTimeSlot'] = "更改當值時段";
$Lang['StaffAttendance']['Description'] = "敘述";
$Lang['StaffAttendance']['AccessRight'] = "存取權限";
$Lang['StaffAttendance']['View'] = "檢視";
$Lang['StaffAttendance']['Manage'] = "管理";
$Lang['StaffAttendance']['Redeem'] = "扣除";
$Lang['StaffAttendance']['Access'] = "存取";
$Lang['StaffAttendance']['FunctionalRights'] = "功能權限";
$Lang['StaffAttendance']['MonitoringRights'] = "監察權限";
$Lang['StaffAttendance']['MonitoringSetGroupLeader'] = "設定為組長";
$Lang['StaffAttendance']['MonitoringSetGroupMember'] = "設定為組員";
$Lang['StaffAttendance']['MonitoringLeader'] = "組長";

# Doc Routinh menu
$Lang['DocRouting']['DocRouting'] = "文件傳遞";

# Settings - Terminal Settings
$Lang['StaffAttendance']['TerminalSettings'] = "終端機設定";
$Lang['StaffAttendance']['TerminalIPList'] = "終端機的 IP 位址";
$Lang['StaffAttendance']['TerminalIPInput'] = "請把終端機的 IP 地址每行一個輸入";
$Lang['StaffAttendance']['TerminalYourAddress'] = "你現時的 IP 地址";
$Lang['StaffAttendance']['TerminalIgnorePeriod'] = "不接受重複拍咭的時間";
$Lang['StaffAttendance']['TerminalIgnorePeriodUnit'] = "分鐘";
$Lang['StaffAttendance']['TerminalRemarks'] = "備註";
$Lang['StaffAttendance']['DescriptionTerminalIPSettings'] = "
在 IP 位址的格上, 可以使用:
<ol><li>固定 IP 位址 (e.g. 192.168.0.101)</li>
<li>IP 位址一個範圍 (e.g. 192.168.0.[10-100])</li>
<li>CISCO Style 範圍(e.g. 192.168.0.0/24)</li>
<li>容許所有位址 (輸入 0.0.0.0)</li></ol>
<span class='tabletextrequire'>連接讀咭器的電腦, 建議該電腦設定固定 IP 地址 (即不採用 DHCP), 以確保資料不會被其他電腦更改.</span><br>
本系統設有安全措施防範假冒的IP 地址 (Faked IP Address).
";
$Lang['StaffAttendance']['SettingApplySuccess'] = "1|=|設定更新成功";
$Lang['StaffAttendance']['SettingApplyFail'] = "0|=|設定更新失敗";

$Lang['StaffAttendance']['IgnoreForOT'] = "不計算超時工作的時間";
$Lang['StaffAttendance']['NoOTTime'] = "分鐘 (999 = 不設超時工作)";
$Lang['StaffAttendance']['CountAsOT'] = "非工作日超時工作";
$Lang['StaffAttendance']['ReasonTypeSettings'] = "原因設定";
$Lang['StaffAttendance']['Name'] = "姓名";
$Lang['StaffAttendance']['ReasonType'] = "原因類別";
$Lang['StaffAttendance']['Reason'] = "原因";
$Lang['StaffAttendance']['ReportSymbol'] = "報告用符號";
$Lang['StaffAttendance']['Waived'] = "豁免";
$Lang['StaffAttendance']['NotWaived'] = "不豁免";
$Lang['StaffAttendance']['Status'] = "狀況";
$Lang['StaffAttendance']['AddReason'] = "新增原因";
$Lang['StaffAttendance']['Active'] = "可使用";
$Lang['StaffAttendance']['InActive'] = "不可使用";
$Lang['StaffAttendance']['DeleteReasonSuccess'] = "1|=|成功刪除原因";
$Lang['StaffAttendance']['DeleteReasonFail'] = "0|=|刪除原因失敗";
$Lang['StaffAttendance']['SaveReasonSuccess'] = "1|=|成功保存原因";
$Lang['StaffAttendance']['SaveReasonFail'] = "0|=|保存原因失敗";
$Lang['StaffAttendance']['ConfirmDeleteReason'] = "確定要刪除此原因?";
$Lang['StaffAttendance']['ReasonReportSymbolExists'] = "原因/報告用符號已經存在";
$Lang['StaffAttendance']['EmptyReason'] = "原因不能留空";
$Lang['StaffAttendance']['GroupName'] = "組別名稱";
$Lang['StaffAttendance']['AttendanceRecord'] = "出席紀錄";
$Lang['StaffAttendance']['InOutRecord'] = "進出紀錄";
$Lang['StaffAttendance']['AlertTakeFutureAttendance'] = "不能預先確認未來的出席紀錄";
$Lang['StaffAttendance']['WorkingTimeSlotHeader'] = "當值時段";
$Lang['StaffAttendance']['Leave2'] = "不在校";
$Lang['StaffAttendance']['LastUpdated'] = "最近更新";
$Lang['StaffAttendance']['NoTimeSlot'] = "無當值時段";
$Lang['StaffAttendance']['UnAssigned'] = "未分配";
$Lang['StaffAttendance']['Summary'] = "撮要";
$Lang['StaffAttendance']['SessionAttendance'] = "出席時段";
$Lang['StaffAttendance']['AllStatus'] = "所有狀況";
$Lang['StaffAttendance']['Absent'] = "缺席";
$Lang['StaffAttendance']['Present'] = "在校";
$Lang['StaffAttendance']['Late'] = "遲到";
$Lang['StaffAttendance']['EarlyLeave'] = "早退";
$Lang['StaffAttendance']['Outing'] = "外出";
$Lang['StaffAttendance']['Holiday'] = "請假";
$Lang['StaffAttendance']['AbsentSuspected'] = "懷疑缺席";
$Lang['StaffAttendance']['EditAttendance'] = "編輯出席資料";
$Lang['StaffAttendance']['RosterDuty'] = "執勤職責";
$Lang['StaffAttendance']['In'] = "進入";
$Lang['StaffAttendance']['Out'] = "離開";
$Lang['StaffAttendance']['InStation'] = "進入拍卡地點";
$Lang['StaffAttendance']['OutStation'] = "離開拍卡地點";
$Lang['StaffAttendance']['Remark'] = "備註";
$Lang['StaffAttendance']['NoDuty'] = "不用執勤";
$Lang['StaffAttendance']['AllDuty'] = "所有執勤人員";
$Lang['StaffAttendance']['NonWorking'] = "非工作人員";
$Lang['StaffAttendance']['EditStatus'] = "編輯狀況";
$Lang['StaffAttendance']['Duty'] = "執勤";
$Lang['StaffAttendance']['TakeAttendanceSuccess'] = "1|=|出席紀錄確認成功";
$Lang['StaffAttendance']['TakeAttendanceFail'] = "0|=|出席紀錄確認失敗";
$Lang['StaffAttendance']['TakeViewFutureOTWarning'] = "不能檢視/編輯未來的超時工作紀錄";
$Lang['StaffAttendance']['InvalidRedeemWarning'] = "扣除分鐘不正確";
$Lang['StaffAttendance']['NoOTRecordWarning'] = "當天沒有超時工作紀錄/扣除分鐘數目超過當日的總超時工作時間";
$Lang['StaffAttendance']['InvalidDateRange'] = "日期範圍不正確";
$Lang['StaffAttendance']['AllStaff'] = "所有教職員";
$Lang['StaffAttendance']['AtSchool'] = "在校";
$Lang['StaffAttendance']['StaffWithOutstandingMins'] = "未扣除超時時間的教職員";
$Lang['StaffAttendance']['TotalOTMins'] = "超時工作 (分鐘)";
$Lang['StaffAttendance']['RedeemMins'] = "扣除 (分鐘)";
$Lang['StaffAttendance']['OutstandingMins'] = "剩餘 (分鐘)";
$Lang['StaffAttendance']['OTSummaryList'] = "超時工作撮要表";
$Lang['StaffAttendance']['RedeemRecord'] = "扣除紀錄";
$Lang['StaffAttendance']['PleaseSelect'] = "請選擇";
$Lang['StaffAttendance']['Today'] = "今天";
$Lang['StaffAttendance']['Yesterday'] = "昨天";
$Lang['StaffAttendance']['ThisMonth'] = "本月";
$Lang['StaffAttendance']['SelectPeriod'] = "選擇時段";
$Lang['StaffAttendance']['OTMins'] = "超時分鐘";
$Lang['StaffAttendance']['RedeemDate'] = "扣除超時分鐘的日期";
$Lang['StaffAttendance']['RedeemRecordSuccess'] = "1|=|成功扣除超時紀錄";
$Lang['StaffAttendance']['RedeemRecordFail'] = "0|=|扣除超時紀錄失敗";
$Lang['StaffAttendance']['DailyLog'] = "完整紀錄";
$Lang['StaffAttendance']['CustomizeAttendanceReport'] = "自訂報告";
$Lang['StaffAttendance']['MonthlySummary'] = "每月總結";
$Lang['StaffAttendance']['DailyDetails'] = "每日詳細資料";
$Lang['StaffAttendance']['DaysOfAttendanceStatus'] = "日數";
$Lang['StaffAttendance']['DutyTime'] = "當值時段";
$Lang['StaffAttendance']['ColDutyTime'] = '工作時區名稱(時間)';
$Lang['StaffAttendance']['RecordTime'] = "紀錄時間";
$Lang['StaffAttendance']['LateMins'] = "遲到分鐘";
$Lang['StaffAttendance']['EarlyLeaveMins'] = "早退分鐘";
$Lang['StaffAttendance']['Location'] = "拍卡地點";
$Lang['StaffAttendance']['HideReportOptions'] = "隱藏報告選項";
$Lang['StaffAttendance']['ShowReportOptions'] = "顯示報告選項";
$Lang['StaffAttendance']['PleaseChoose'] = "請作出配搭";
$Lang['StaffAttendance']['ReportType'] = "報告類別";
$Lang['StaffAttendance']['PersonalReport'] = "個人報告";
$Lang['StaffAttendance']['GroupReport'] = "組別報告";
$Lang['StaffAttendance']['Target'] = "對象";
$Lang['StaffAttendance']['PeriodType'] = "時間類別";
$Lang['StaffAttendance']['MonthlyReport'] = "每月報告";
$Lang['StaffAttendance']['WeeklyReport'] = "每週報告";
$Lang['StaffAttendance']['DailyReport'] = "每日報告";
$Lang['StaffAttendance']['Format'] = "格式";
$Lang['StaffAttendance']['RecordDetails'] = "詳細紀錄";
$Lang['StaffAttendance']['SummaryStatistic'] = "簡要紀錄";
$Lang['StaffAttendance']['WithColumns'] = "顯示";
$Lang['StaffAttendance']['InOutTime'] = "進出時間";
$Lang['StaffAttendance']['InOutStation'] = "進出拍卡地點";
$Lang['StaffAttendance']['SlotName2'] = "工作時區名稱";
$Lang['StaffAttendance']['InTime'] = "進入時間";
$Lang['StaffAttendance']['OutTime'] = "離開時間";
$Lang['StaffAttendance']['ChangeGroupDescriptionSuccess'] = "1|=|成功更改組別描述";
$Lang['StaffAttendance']['ChangeGroupDescriptionFail'] = "0|=|更改組別描述失敗";
$Lang['StaffAttendance']['FunctionAccessSettings'] = "功能權限設定";
$Lang['StaffAttendance']['AccessRightDeleteGroupWarning'] = "你確定要刪除此權限組別?";
$Lang['StaffAttendance']['AccessRightDeleteGroupMemberWarning'] = "你確定要刪除所選權限組別的組員?";
$Lang['StaffAttendance']['UserType'] = "成員類別";
$Lang['StaffAttendance']['RosterReport'] = "執勤人員報告";
$Lang['StaffAttendance']['Max3Chars'] = "最多三個字";
$Lang['StaffAttendance']['WeekdayDistribution'] = "出席分析";
$Lang['StaffAttendance']['TimesOf'] = "次數";
$Lang['StaffAttendance']['ShowDetail'] = "顯示細節";
$Lang['StaffAttendance']['HideDetail'] = "隱藏細節";
$Lang['StaffAttendance']['OTReport'] = "超時工作報告";
$Lang['StaffAttendance']['WorkPeriodImportDesc'] = "第一欄 : 職員登入名稱<br />第二欄 : 開始日期 (YYYY-MM-DD)<br />第三欄 : 結束日期 (YYYY-MM-DD)";
$Lang['StaffAttendance']['DataImportSuccess'] = "1|=|資料匯入成功";
$Lang['StaffAttendance']['DataImportFail'] = "0|=|資料匯入失敗";
$Lang['StaffAttendance']['Months'] = array('','一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月');
$Lang['StaffAttendance']['Mins'] = "分鐘";
$Lang['StaffAttendance']['Day'] = "日";
$Lang['StaffAttendance']['InEmploymentPeriod'] = "現職";
$Lang['StaffAttendance']['NotInEmploymentPeriod'] = "非現職";
$Lang['StaffAttendance']['OtherIndividuals'] = "個人";
$Lang['StaffAttendance']['DutySetup'] = "職務設定";
$Lang['StaffAttendance']['AttendanceReport'] = "出席報告";
$Lang['StaffAttendance']['DailyLogReport'] = "每日出席紀錄";
$Lang['StaffAttendance']['EntryLogReport'] = "完整拍卡紀錄";
$Lang['StaffAttendance']['EntryLogSelectStaffWarning'] = "請最少選擇一位職員";
$Lang['StaffAttendance']['RecordDate'] = "紀錄日期";
$Lang['StaffAttendance']['RenameGroup'] = "重新命名";
$Lang['StaffAttendance']['ManageMember'] = "管理成員";
$Lang['StaffAttendance']['LastUpdateOn'] = "最後修改時間:";
$Lang['StaffAttendance']['StaffInfo'] = "職員資訊";
$Lang['StaffAttendance']['CardID'] = "智能卡號碼";
$Lang['StaffAttendance']['SmartcardWarning'] = "智能卡號碼已被其他用戶使用";
$Lang['StaffAttendance']['SmartcardIDUpdateSuccess'] = "1|=|智能卡號碼更新成功";
$Lang['StaffAttendance']['SmartcardIDUpdateFail'] = "0|=|智能卡號碼更新失敗";
$Lang['StaffAttendance']['StaffInfoImportDesc'] = "第一欄: 職員登入名稱<br />第二欄: 智能卡號碼";
$Lang['StaffAttendance']['StaffNotExists'] = "此用戶不存在";
$Lang['StaffAttendance']['NoDataFound'] = "沒有資料";
$Lang['StaffAttendance']['ImportedAlready'] = "此職員被匯入多於一次";
$Lang['StaffAttendance']['SelectCSVFile'] = "選擇CSV檔案";
$Lang['StaffAttendance']['CSVConfirmation'] = "確定";
$Lang['StaffAttendance']['ImportResult'] = "匯入結果";
$Lang['StaffAttendance']['UserLogin'] = "職員登入名稱";
$Lang['StaffAttendance']['BasicDutySetting'] = "常規職務";
$Lang['StaffAttendance']['SpecialDutySetting'] = "特別職務";
$Lang['StaffAttendance']['SetupStatus'] = "設定狀況";
$Lang['StaffAttendance']['MemberStatus'] = "成員";
$Lang['StaffAttendance']['DutyPeriodStatus'] = "職務時段";
$Lang['StaffAttendance']['SettingCompletedStatus'] = "完成";
$Lang['StaffAttendance']['SettingNotCompletedStatus'] = "立即設定";
$Lang['StaffAttendance']['ClickToSetup'] = "按此設定";
$Lang['StaffAttendance']['ClickToView'] = "按此檢視";
$Lang['StaffAttendance']['ImportOfflineRecords'] = "匯入離線紀錄";
$Lang['StaffAttendance']['ImportOfflineRecordsDesc'] = "使用內置格式匯入時，請使用 YYYY-MM-DD hh:mm:ss 格式輸入時間。<br />你只可匯入同一天的紀錄。";
$Lang['StaffAttendance']['TimeRecorded'] = "紀錄時間";
$Lang['StaffAttendance']['Site'] = "紀錄地點";
$Lang['StaffAttendance']['SmartCardID'] = "智能咭 ID";
$Lang['StaffAttendance']['ImportInvalidEntries'] = "匯入資料錯誤";
$Lang['StaffAttendance']['ImportWarningOneDayOnly'] = "並非所有紀錄為同一天的紀錄，請分開處理。";
$Lang['StaffAttendance']['AbsenceReport'] = "缺席紀錄";
$Lang['StaffAttendance']['LeaveAbsenceReport'] = "請假缺席統計";
$Lang['StaffAttendance']['SaveAndManageMember'] = "儲存及管理成員";
$Lang['StaffAttendance']['SaveAndManageTimeSlot'] = $Lang['Btn']['Add']."及新增".$Lang['StaffAttendance']['TimeSlot'];
$Lang['StaffAttendance']['SaveAndManageDutyPeriod'] = $Lang['Btn']['Submit']."及管理職務時段";
$Lang['StaffAttendance']['SaveAndManageRoster'] = $Lang['Btn']['Submit']."及".$Lang['StaffAttendance']['Config'];
$Lang['StaffAttendance']['WholeSchoolSpecialDaySetting'] = '全校性特別職務';
$Lang['StaffAttendance']['Individuals'] = "個人";
$Lang['StaffAttendance']['SelectBy'] = "統計類型";
$Lang['StaffAttendance']['DateRange'] = "日期範圍";
$Lang['StaffAttendance']['DaysAbsent'] = "缺席日數";
$Lang['StaffAttendance']['StaffAbsenceReport'] = "教職員缺席統計表";
$Lang['StaffAttendance']['ReportCreationTime'] = "報表建立時間";
$Lang['StaffAttendance']['IncludeWaivedRecords'] = "包括豁免紀錄";
$Lang['StaffAttendance']['Template'] = "預設";
$Lang['StaffAttendance']['AddTemplate'] = "新增預設";
$Lang['StaffAttendance']['DateApplied'] = "已套用到(日)";
$Lang['StaffAttendance']['GroupInvolved'] = "受影響".$Lang['StaffAttendance']['Group'];
$Lang['StaffAttendance']['IndividualInvolved'] = "受影響".$Lang['StaffAttendance']['Individual'];
$Lang['StaffAttendance']['RenameTemplate'] = "重新命名";
$Lang['StaffAttendance']['ManageDateApplied'] = "管理已套用日期";
$Lang['StaffAttendance']['EditGroupInvolved'] = "更改涉及者";
$Lang['StaffAttendance']['Others'] = "其他";
$Lang['StaffAttendance']['TotalDays'] = "總日數";
$Lang['StaffAttendance']['StaffLeaveAbsenceReport'] = "教職員請假缺席統計";
$Lang['StaffAttendance']['ManageInvolvedParties'] = "管理涉及者";
$Lang['StaffAttendance']['ManageInvolvedGroup'] = "設定受影響".$Lang['StaffAttendance']['Group'];
$Lang['StaffAttendance']['ManageInvolvedIndividual'] = "設定受影響".$Lang['StaffAttendance']['Individual'];
$Lang['StaffAttendance']['SaveAndMgmtInvolvedGroup'] = $Lang['Btn']['Add']."並前往設定受影響".$Lang['StaffAttendance']['Group'];
$Lang['StaffAttendance']['SaveAndMgmtInvolvedIndividual'] = $Lang['Btn']['Add']."並前往設定受影響".$Lang['StaffAttendance']['Individual'];
$Lang['StaffAttendance']['SaveAndManageDateAssoicated'] = "儲存及".$Lang['StaffAttendance']['ManageDateApplied'];
$Lang['StaffAttendance']['DuplicateTemplateNameWarning'] = "*預設名稱重複或空白";
$Lang['StaffAttendance']['SaveTemplateSuccess'] = "1|=|預設儲存成功";
$Lang['StaffAttendance']['SaveTemplateFail'] = "0|=|預設儲存失敗";
$Lang['StaffAttendance']['DeleteTemplateWarning'] = "您是否確定要移除此預設?";
$Lang['StaffAttendance']['DeleteTemplateSuccess'] = "1|=|預設移除成功";
$Lang['StaffAttendance']['DeleteTemplateFail'] = "0|=|預設移除失敗";
$Lang['StaffAttendance']['CurrentSelection'] = "已套用的日期";
$Lang['StaffAttendance']['TargetDateMustBeAfterTodayWarning'] = "*套用日期必須為今天以後";
$Lang['StaffAttendance']['TargetDateUsedWarning'] = "*此日期已套用於此或其他預設";
$Lang['StaffAttendance']['DateAppliedSuccess'] = "1|=|預設已套用於此日期";
$Lang['StaffAttendance']['DateAppliedFail'] = "0|=|預設已套用失敗";
$Lang['StaffAttendance']['DateAppliedWarning'] = "如使用此預設，所有受影響單位的現存職務設定將被覆寫。\\n你是否確定要繼續?";
$Lang['StaffAttendance']['DateAppliedDeleteWarning'] = "所有涉及者於所選日期內的非全日請假紀錄將會移除. \\n你是否確定要繼續?";
$Lang['StaffAttendance']['DeleteTemplateTargetDateSuccess'] = "1|=|日期已從預設中移除";
$Lang['StaffAttendance']['DeleteTemplateTargetDateFail'] = "0|=|日期從預設中移除失敗";
$Lang['StaffAttendance']['TemplateTargetSlotConflictWarning'] = "系統發現以下涉及者在同一預設中已設定的當值時段發生衝突:";
$Lang['StaffAttendance']['MaxSlotPerSettingWarning1'] = "最多只可套用 ";
$Lang['StaffAttendance']['MaxSlotPerSettingWarning2'] = " 個".$Lang['StaffAttendance']['TimeSlot'];
$Lang['StaffAttendance']['EditDutyRemoveSlotWarning'] = "您是否確定要移除此".$Lang['StaffAttendance']['TimeSlot']."?";
$Lang['StaffAttendance']['Staff'] = "職員";
$Lang['StaffAttendance']['SlotSpecialCharacterCheckWarning'] = '當值時段代號不可含有以下符號: <br>% & ^ @ " \\\\ \\\' : ; < > , . / ? [ ] ~ ! # $ * ( ) { } | = + -';
$Lang['StaffAttendance']['OTRecordReport'] = '超時工作報告';
$Lang['StaffAttendance']['SpecialRoster'] = '小組/個人特別職務';
$Lang['StaffAttendance']['And'] = '及';
$Lang['StaffAttendance']['AddEditLeaveRecord'] = $Lang['Btn']['Add'].'/'.$Lang['Btn']['Edit'].'請假紀錄';
$Lang['StaffAttendance']['ShowReportOptions'] = '顯示報告選項';
$Lang['StaffAttendance']['ReportOptions'] = '報告選項';
$Lang['StaffAttendance']['OTRecordsFull'] = "超時工作紀錄";
$Lang['StaffAttendance']['AddAndFinish'] = "儲存並完成";
$Lang['StaffAttendance']['AddAndProceed'] = "儲存並前往下一步";
$Lang['StaffAttendance']['ManageDutyPeriod'] = "管理職務時段";
$Lang['StaffAttendance']['ManageTimeSlot'] = "管理當值時段";
$Lang['StaffAttendance']['ViewDutyPeriod'] = "檢視職務時段";
$Lang['StaffAttendance']['ViewTimeSlot'] = "檢視當值時段";
$Lang['StaffAttendance']['ViewMember'] = "檢視成員";
$Lang['StaffAttendance']['ApplyTimeSlot'] = "設定所選當值時段的職務安排";
$Lang['StaffAttendance']['DragAndDropDutyIntruction'] = "說明: <br>
																												 1. 點擊職員名稱.<br>
																												 2. 將已選取職員拖曳並放置於目標當值時段.";
$Lang['StaffAttendance']['Count'] = '計算';
$Lang['StaffAttendance']['NotCount'] = '不計算';
$Lang['StaffAttendance']['CurrentMember'] = "現時組員";
$Lang['StaffAttendance']['WarningInputNonNegativeNumber'] = "請輸入一個非負數";
$Lang['StaffAttendance']['AllIndividuals'] = "所有個人職員";
$Lang['StaffAttendance']['NativeFormat'] = "內置格式";
$Lang['StaffAttendance']['TerminalFormat'] = "終端機格式";
$Lang['StaffAttendance']['NoCardTappingIn'] = "進入時無需拍卡";
$Lang['StaffAttendance']['NoCardTappingOut'] = "離開時無需拍卡";
$Lang['StaffAttendance']['RealTimeStaffStatus'] = "實時職員狀況";
$Lang['StaffAttendance']['LastSlotName'] = "最後".$Lang['StaffAttendance']['TimeSlot'];
$Lang['StaffAttendance']['InSchool'] = "在校";
$Lang['StaffAttendance']['NotInSchool'] = "不在校";
$Lang['StaffAttendance']['PresetStatus'] = "預設狀況";
$Lang['StaffAttendance']['RFID'] = "無線射頻辨識ID";
$Lang['StaffAttendance']['SmartCard'] = "智能卡";
$Lang['StaffAttendance']['SmartCardImportType'] = "匯入類型";
$Lang['StaffAttendance']['SpecialDutySaveNoSettingWarning'] = "注意：你必須於最少一個當值時段中，指定職員當值情況(即「當值」、「出外工作」、及「請假」方框不可全部留空)，否則系統將自動套用該當值時段的常規職務設定。";
$Lang['StaffAttendance']['NoTimeRecord'] = "無時間紀錄";
$Lang['StaffAttendance']['RestoreRegularDuty'] = "套用常規職務到時段";
$Lang['StaffAttendance']['SetNonSchoolDay'] = "設定為無需上課時段";
$Lang['StaffAttendance']['DeleteSpecialNonSchoolDayWarning'] = "此日含有無需上課時段。除非要取消無需上課時段，否則請勿修改空置的當值時段。";
$Lang['StaffAttendance']['OffDuty'] = "無需當值";
$Lang['StaffAttendance']['SwitchTargetDateMethod'] = "切換選擇日期方法";
$Lang['StaffAttendance']['RepeatType']['DAILY'] = "每日";
$Lang['StaffAttendance']['RepeatType']['WEEKLY'] = "每星期";
$Lang['StaffAttendance']['RepeatType']['MONTHLY'] = "每月";
$Lang['StaffAttendance']['RepeatType']['YEARLY'] = "每年";
$Lang['StaffAttendance']['RepeatStart'] = "開始: ";
$Lang['StaffAttendance']['RepeatEnd'] = "完結: ";
$Lang['StaffAttendance']['RepeatEvery'] = "多久重複一次?";
$Lang['StaffAttendance']['Week'] = "星期";
$Lang['StaffAttendance']['RepeatOn'] = "重複在:";
$Lang['StaffAttendance']['RepeatMonth'] = "個月的";
$Lang['StaffAttendance']['MonthlyMonthDay'] = "每月的一日";
$Lang['StaffAttendance']['MonthlyWeekDay'] = "每星期的一日";
$Lang['StaffAttendance']['Year'] = "年";
$Lang['StaffAttendance']['Every'] = "每";
$Lang['StaffAttendance']['Until'] = "直到";
$Lang['StaffAttendance']['WeekCount'] = array("第一個", "第二個", "第三個", "第四個", "第五個", "第六個");
$Lang['StaffAttendance']['MonthName'] = array("一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月");
$Lang['StaffAttendance']['StartDateMustAfterTodayWarning'] = '*開始日期必須於今日之後';
$Lang['StaffAttendance']['StatusMustSetIfInTimeSetWarning'] = "當已輸入進入時間時，必須設定出席狀況。";
$Lang['StaffAttendance']['OnlyShowStaffHaveRecords'] = "只顯示有紀錄的職員";
$Lang['StaffAttendance']['EveryMonth'] = "每月的";
$Lang['StaffAttendance']['On'] = "第";
$Lang['StaffAttendance']['Last'] = "最後的";
$Lang['StaffAttendance']['HideAttendStatus'] = "不顯示考勤狀態";
$Lang['StaffAttendance']['RealTimeStaffStatusNote'] = "甄選條件將會於下次自動更新時套用";
$Lang['StaffAttendance']['SortBy'] = "排序";
$Lang['StaffAttendance']['WithWaived'] = "包括豁免紀錄";
$Lang['StaffAttendance']['WithoutWaived'] = "不包括豁免紀錄";
$Lang['StaffAttendance']['DataOutdatedWarning'] = "0|=|資料已過時，請重新呈送一次。";
$Lang['StaffAttendance']['AbsMinsForTimeSlot'] = "於當值時段開始前";
$Lang['StaffAttendance']['TargetScope'] = "對象範圍";
$Lang['StaffAttendance']['CustomSummary'] = "自定總結";
$Lang['StaffAttendance']['AbsMinsForTimeSlot1'] = "分鐘開始記錄拍卡";
$Lang['StaffAttendance']['AbsMinsForTimeSlotDescription'] = "
<b>這是什麽？</b><br>
此設定用於協助系統判斷職員出席狀況的。請細閱以下說明，確保你理解設定的用法。我們建議用戶於完成設定後，<span class='tabletextrequire'>先進行測試，然後才落實使用</span>。<br><br>
<b>爲什麽需要此設定？</b><br>
職員考勤系統乃根據時段的開始及結束時間，去判斷職員的出席情況。例如，在08:00-12:30時段中，於08:00前拍卡，將被視為準時，於12:30前拍卡，將被視為早退。<br><br>
不過，當同一日內有多個時段、並且時段之間存有空隙時，系統將無法判斷該段空隙是屬於前一個時段還是後一個時段，因此亦無法判斷兩個時段之間的拍卡，應作前一時段離校論，還是後一時段抵校論，以致顯示非預期的出席狀況。為此，你需要透過此設定，協助系統作出判斷。<br><br>
<b>設定的限制：</b><br>
<ol>
	<li>系統只提供一個統一設定，所有時段只能共用一個設定。如設定時間為十五分鐘，則時段分割線將置於所有時段前的十五分鐘。</li>
	<li>當系統中存有任何兩個彼此相連的時段時，後一個時段的開始記錄拍卡時間，將位於前一個時段範圍內，系統將一概根據後一個時段判斷出席狀況(此時仍有機會出現錯誤判斷問題)。</li>
</ol>
<br>
<b>因此，請根據以下指引作出設定：</b><br>
<ul>
	<li>若系統中所有時段之間均存有空隙，請設定一個大於\"0\"的時間，並確保此時間短於所有空隙中最短的一個。</li>
	<li>若部份時段之間均存有空隙，部份時段彼此相連，請設定為“0”，即不使用。此時，系統將根據具體拍卡時間和時段設定情況，判斷出席狀(此時仍有機會出現錯誤判斷問題)。</li>
</ul>
";
$Lang['StaffAttendance']['WhatIsThis'] = "這是什麼？";
$Lang['StaffAttendance']['Times'] = "次數";
$Lang['StaffAttendance']['GroupBy'] = "整合方式";
$Lang['StaffAttendance']['GroupByDate'] = "日期";
$Lang['StaffAttendance']['GroupByStaff'] = "職員";
$Lang['StaffAttendance']['ShowNoDutyDateOnly'] = "只顯示沒有設定值勤時段紀錄";
$Lang['StaffAttendance']['ChangeStatus'] = "更改狀況";
$Lang['StaffAttendance']['SetAbsentToPresent'] = "將缺席/懷疑缺席轉為在校";
$Lang['StaffAttendance']['SettingsSaveSuccess'] = "1|=|成功儲存設定。";
$Lang['StaffAttendance']['SettingsSaveFail'] = "0|=|儲存設定失敗。";
$Lang['StaffAttendance']['SettingsLoadSuccess'] = "1|=|成功讀取設定。";
$Lang['StaffAttendance']['SettingsLoadFail'] = "0|=|讀取設定失敗。";
$Lang['StaffAttendance']['SettingsDeleteSuccess'] = "1|=|成功刪除設定。";
$Lang['StaffAttendance']['SettingsDeleteFail'] = "0|=|刪除設定失敗。";
$Lang['StaffAttendance']['InvalidSettingTemplateName'] = "不合法的設定範本名稱";
$Lang['StaffAttendance']['SettingTemplate'] = "設定範本";
$Lang['StaffAttendance']['SettingTemplateName'] = "設定範本名稱";
$Lang['StaffAttendance']['ConfirmOverwriteSettingTemplate'] = "你確定要儲存並覆蓋此設定範本?";
$Lang['StaffAttendance']['ConfirmDeleteSettingTemplate'] = "你確定要刪除此設定範本?";
$Lang['StaffAttendance']['LeaveRecordImportDesc'] = "
	第一欄 : 職員登入名稱<br />
	第二欄 : 1 - ".$Lang['StaffAttendance']['Holiday'].", 2 - ".$Lang['StaffAttendance']['Outgoing']."<br/>
	第三欄 : 日期 (YYYY-MM-DD)<br />
	第四欄 : ".$Lang['StaffAttendance']['TimeSlot']." (留空將視為全日".$Lang['StaffAttendance']['Holiday']."/".$Lang['StaffAttendance']['Outgoing'].")<br />
	第五欄 : 原因 (請參照 設定 -> 原因 -> 原因名稱)";
$Lang['StaffAttendance']['InvalidLeaveType'] = "請假紀錄類型不符 (1 - ".$Lang['StaffAttendance']['Holiday'].", 2 - ".$Lang['StaffAttendance']['Outgoing'].")";
$Lang['StaffAttendance']['InvalidReason'] = "原因不符";
$Lang['StaffAttendance']['InvalidTimeSlotName'] = $Lang['StaffAttendance']['TimeSlot']."不正確";
$Lang['StaffAttendance']['CannotFoundSlotOnTargetDate'] = "在指定的日期內打不到相對的".$Lang['StaffAttendance']['TimeSlot'];
$Lang['StaffAttendance']['DuplicateImportLeave'] = "在同一檔案中找到重複的請假要求";
$Lang['StaffAttendance']['ShowHidePastDate'] = "顯示/ 隱藏已過去日子";
$Lang['StaffAttendance']['DayByDay'] = "單日";
$Lang['StaffAttendance']['ChooseFromSchCal'] = "從校曆表中選擇";
$Lang['StaffAttendance']['EventSelected'] = "已選擇";
$Lang['StaffAttendance']['SetAsDefault'] = "設為預設原因";
$Lang['StaffAttendance']['ViewLateAbsentEarlyList'] = "檢視遲到/ 缺席/ 早退名單";
$Lang['StaffAttendance']['SpecialDutyCalendarMethod'] = "應用到所選日子";
$Lang['StaffAttendance']['SpecialDutyRepeatMethod'] = "自動重複應用";
$Lang['StaffAttendance']['SpecialDutySelectTimeSlotReminder'] = '勾選特別職務的當值時段，然後按"'.$Lang['StaffAttendance']['ApplyTimeSlot'].'"。 如果所需的當值時段不存在，請按"'.$Lang['StaffAttendance']['AddTimeSlot'].'"。<br>
																																 請確保你所選擇的時段沒有重疊，否則你將無法儲存設定。';
$Lang['StaffAttendance']['SpecialDutyReminder'] = '<b>職務安排：</b><br>
																									<hr>
																									請確認你已經於畫面的“當值時段”部份，選擇了要設定的時段，並且時段之間沒有重疊。 然後，你可以開始為每一名職員設定其職務安排。為加快設定，你可以先將 全部職員或時段設定為同一狀況，然後修改不同者。<br><br>
																									如職員之間職務安排分別比較大，你可選用"拖放模式"。';
$Lang['StaffAttendance']['DutySetupMode'] = '設定模式';
$Lang['StaffAttendance']['BasicMode'] = '基本模式';
$Lang['StaffAttendance']['DragAndDropMode'] = '拖放模式';

$Lang['StaffAttendance']['MonthlyReportGroupByDate'] = '按日期分組';
$Lang['StaffAttendance']['MonthlyReportGroupByStaff'] = '按教職員分組';
$Lang['StaffAttendance']['YearMonth'] = '年 / 月';

$Lang['StaffAttendance']['WorkingHourReport'] = "工作時數報告";
$Lang['StaffAttendance']['CalculationMode'] = "計算模式";
$Lang['StaffAttendance']['CountOverTimeWork'] = "計算超時工作";
$Lang['StaffAttendance']['CardPriorityMode'] = "拍卡優先模式";
$Lang['StaffAttendance']['FixedMode'] = "固定模式";
$Lang['StaffAttendance']['EqualPriorityMode'] = "平衡模式";
$Lang['StaffAttendance']['WorkingHours'] = "工作時數";
$Lang['StaffAttendance']['TotalWorkingHours'] = "總工作時數";
$Lang['StaffAttendance']['RecordsToBeCount'] = "計算紀錄限於";
$Lang['StaffAttendance']['CalculationModeDescription'] = '系統支援以下幾種計算模式，請根據貴校的情況選用：<br>
<ul>
	<li>平衡模式</li>
	<li>拍卡優先模式</li>
	<li>固定模式</li>
</ul>
<br>
<b>平衡模式</b><br><br>
系統比較當值時段的開始和結束時間，以及出入拍卡時間，然後以距離最短者作為工作時間。
如果選用此模式，超時工作將不被計算，但用戶可以勾選“計算超時工作”選項 ，以改為計算
較遲的離校時間。<br><br>
<ul style="list-style-type: none">
	<li><b>例</b><br>
	當值時段：9:00-13:00<br>
	拍卡時間：8:50, 13:30<br>
	工作時間：9:00-13:00<br>
	工作時間(計算超時)：9:00-13:30</li></ul><br>
<b>拍卡優先模式</b><br><br>
系統根據出入拍卡時間計算工時。如果進入或離開時忘記拍卡，將以當值時段的開始或結束時
間取代。如果進入或離開均沒有拍卡，則不計算工時。如果選用此模式，超時工作會被計算。<br><br>
<ul style="list-style-type: none">
	<li><b>例</b><br>
	當值時段：9:00-13:00<br>
	拍卡時間：8:50, 離開時沒有拍卡<br>
	工作時間：8:50-13:00</li></ul><br>
<b>固定模式</b><br><br>
系統將當值時段的開始和結束時間作為工時計算。如果選用此模式，超時工作將不被計算。<br><br>
<ul style="list-style-type: none">
	<li><b>例</b><br>
	當值時段：9:00-13:00<br>
	拍卡時間：8:30, 13:30<br>
	工作時間：9:00-13:00</li></ul><br>';

$Lang['StaffAttendance']['DailyAttendanceRecord'] = "每日考勤紀錄";
$Lang['StaffAttendance']['SendEmail'] = "傳送電郵";
$Lang['StaffAttendance']['SendAttendanceRecordsToAdmin'] = "傳送請假缺席考勤紀錄給管理員";
$Lang['StaffAttendance']['UnableToViewFutureAttendanceRecords'] = "無法檢閱未來的考勤紀錄。";
$Lang['StaffAttendance']['DailyAttendanceRecordEmailSubject'] = "教職員考勤管理系統 每日請假缺席紀錄";
$Lang['StaffAttendance']['ToEmail'] = "收件者";
$Lang['StaffAttendance']['SendingEmailMessage'] = "電郵傳送中，請耐心等候...";
$Lang['StaffAttendance']['ReturnMsg']['AttendanceRecordSentSuccess'] = "1|=|考勤紀錄傳送成功。";
$Lang['StaffAttendance']['ReturnMsg']['AtttendanceRecordSentUnsuccess'] = "0|=|考勤紀錄傳送失敗。";
$Lang['StaffAttendance']['DoctorCertificate'] = "醫生證明";
$Lang['StaffAttendance']['DoctorCertificateFiles'] = "醫生證明文件";
$Lang['StaffAttendance']['UploadDoctorCertificates'] = "上載醫生證明文件";
$Lang['StaffAttendance']['DisplayRecordsWithDoctorCertificates'] = "顯示有醫生證明的紀錄";
$Lang['StaffAttendance']['DisplayRecordsWithoutDoctorCertificates'] = "顯示無醫生證明的紀錄";
$Lang['StaffAttendance']['WarningMsg']['ConfirmDeleteFiles'] = "你確定要刪除所選擇的文件?";
$Lang['StaffAttendance']['WarningMsg']['UploadAtLeastOneFile'] = "請至少上載一個文件。";
$Lang['StaffAttendance']['StaffNo'] = "教職員編號";
$Lang['StaffAttendance']['DailyLeaveAbsenceRecords'] = "每日請假缺席紀錄";
$Lang['StaffAttendance']['GeneralSettings'] = "一般設定";
$Lang['StaffAttendance']['ReportDisplaySuspendedStaff'] = "報表顯示暫停使用的教職員";
$Lang['StaffAttendance']['ReportDisplayLeftStaff'] = "報表顯示已離校的教職員";
$Lang['StaffAttendance']['InStatus'] = "進入狀況";
$Lang['StaffAttendance']['InWaived'] = "豁免進入";
$Lang['StaffAttendance']['OutStatus'] = "離開狀況";
$Lang['StaffAttendance']['OutWaived'] = "豁免離開";
$Lang['StaffAttendance']['ErrorDetail'] = "錯誤詳細內容";
$Lang['StaffAttendance']['ImportPastAttendanceRecords'] = "匯入過去的考勤紀錄";
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Date','可使用格式 YYYY-MM-DD 或者 DD/MM/YYYY');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('UserLogin','職員內聯網帳號');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Time Slot','當值時段。 禁止使用以下字符:<br>% & ^ @ " \\ \' : ; < > , . / ? [ ] ~ ! # $ * ( ) { } | = + - ');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Duty Start Time','24小時時間格式, 例如 14:30:00');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Duty End Time','24小時時間格式, 例如 16:00:00');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Duty Count','0.1 至 1.0');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('No Tap Card In','如無需拍卡進入請設定為 1，否則可留空');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('In Status','拍卡狀況代碼:<ul><li>0 : 準時</li><li>1 : 缺席</li><li>2 : 遲到</li><li>4 : 放假</li><li>5 : 外出</li></ul>');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('In Time','24小時時間格式, 例如 13:50:01');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('In Station','拍卡地點');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('In Waived','如要豁免遲到、 放假或者外出，請設定為1。');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Out Status','拍卡狀況代碼:<ul><li>0 : 準時</li><li>1 :缺席</li><li>3 :早退</li><li>4 : 放假</li><li>5 : 外出</li></ul>');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Out Time','24小時時間格式, 例如 13:50:01');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Out Station','拍卡地點');
$Lang['StaffAttendance']['ImportPastRecord']['Fields'][] = array('Out Waived','如要豁免早退、 放假或者外出，請設定為1。');

$Lang['StaffAttendance']['Warnings']['InvalidDate'] = "日期不正確。";
$Lang['StaffAttendance']['Warnings']['MustBePastDateRecord'] = "必須是過去的日子。";
$Lang['StaffAttendance']['Warnings']['StaffUserNotFound'] = "職員用戶不存在。";
$Lang['StaffAttendance']['Warnings']['InvalidTimeSlot'] = "當值時間不正確。";
$Lang['StaffAttendance']['Warnings']['TimeSlotHasRestrictedChars'] = "當值時段有禁止使用的字符。";
$Lang['StaffAttendance']['Warnings']['InvalidDutyStartTime'] = "當值時段開始時間不正確。";
$Lang['StaffAttendance']['Warnings']['InvalidDutyEndTime'] = "當值時段結束時間不正確。";
$Lang['StaffAttendance']['Warnings']['InvalidDutyCountValue'] = "當值時段計算天數不正確。";
$Lang['StaffAttendance']['Warnings']['InvalidInStatus'] = "進入狀況不正確。";
$Lang['StaffAttendance']['Warnings']['InvalidOutStatus'] = "離開狀況不正確。";
$Lang['StaffAttendance']['Warnings']['AlreadyHasDutyOnDate'] = "職員於<!--DATE-->已經有當值時段。";
$Lang['StaffAttendance']['Warnings']['DutyTimeOverlapWithDuty'] = "當值時間與<!--TIMESLOT-->衝突了。";
$Lang['StaffAttendance']['Warnings']['StaffIsFulldayDayoff'] = "職員當日全日休假。";
$Lang['StaffAttendance']['Warnings']['StaffIsFulldayOuting'] = "職員當日全日外出。";

$Lang['StaffAttendance']['TimeSlotTapCardSettingRemark'] = "只適用於兩個時段中間的離開或進入狀況";
$Lang['StaffAttendance']['DisplayAllCalendarDay'] = "顯示所有日子";

$Lang['StaffAttendance']['CustomizedSettings'] = "自訂設定";
$Lang['StaffAttendance']['EmailTitle'] = "電子郵件標題";
$Lang['StaffAttendance']['EmailHeader'] = "電子郵件內容頁頭";
$Lang['StaffAttendance']['EmailFooter'] = "電子郵件內容頁腳";
$Lang['StaffAttendance']['EmailRecipient'] = "電子郵件收件人";
$Lang['StaffAttendance']['EmailSendTime'] = "電子郵件發送時間";
$Lang['StaffAttendance']['DatePlaceholderRemark'] = "使用 ##DATE## 作為日期的佔位符號。";
$Lang['StaffAttendance']['DatePlaceholderWarning'] = "請在電子郵件標題輸入日期佔位符號 ##DATE##。";
$Lang['StaffAttendance']['OneEmailPerLineRemark'] = "請每行輸入一個電郵地址。";
$Lang['StaffAttendance']['MondayToFriday'] = "(星期一 至 星期五)";
$Lang['StaffAttendance']['RequestInputRecipientEmail'] = "請輸入至少一個收件人的電郵地址。";
$Lang['StaffAttendance']['DisableSendEmail'] = "關閉電子郵件發送功能";

$Lang['StaffAttendance']['InSchoolInOutStatus'] = "職員回校/進出狀況";
$Lang['StaffAttendance']['InSchoolStatus'] = "職員回校狀況";
$Lang['StaffAttendance']['InOutStatus'] = "職員進出狀況";
$Lang['StaffAttendance']['InSchoolStatusRemark'] = "查看職員當天已回校與否";
$Lang['StaffAttendance']['InOutStatusRemark'] = "在上班及下班時段內，可查看職員的進出紀錄";
$Lang['StaffAttendance']['DutyCalcType'] = "計算方法";
$Lang['StaffAttendance']['CycleDay'] = "循環日";
$Lang['StaffAttendance']['NoCycleDayCanCoverDutyPeriodDateRange'] = "無任何循環日的日期範圍可以涵蓋該職務時段的日期範圍。<br />請調整循環日的設定或職務時段的日期範圍。";
$Lang['StaffAttendance']['ImportSpecialDuty'] = "匯入特別職務";
$Lang['StaffAttendance']['ViewGroupList'] = "檢視職務小組列表";
$Lang['StaffAttendance']['ViewStaffList'] = "檢視教職員列表";
$Lang['StaffAttendance']['ViewReasonList'] = "檢視原因列表";
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'] = array();
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Group ID", "職務小組ID。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("User Logins", "教職員用戶登入帳號(以逗號分隔)。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Date", "日期 (格式為年月日 yyyy-mm-dd)。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Duty Type", "<b>D</b>表示需要當值，<b>L</b>表示放假，<b>FL</b>表示全日放假，<b>O</b>表示外出工作，<b>FO</b>表示全日外出工作，<b>N</b>表示沒有職務。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Time Slot", "職務時段名稱。全日設定應該留空。不能包含符號: % & ^ @ \" \\ ' : ; < > , . / ? [ ] ~ ! # $ * ( ) { } | = + - ");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Duty Start", "職務開始時間，格式為hh:mm:ss。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Duty End", "職務結束時間，格式為hh:mm:ss。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Duty Count", "職務比重，例如: 0.5 表示半日，1.0 表示全日。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Waive In", "<b>Y</b>表示進入的時候不需要拍卡，<b>N</b>表示進入的時候需要拍卡。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Waive Out", "<b>Y</b>表示離開的時候不需要拍卡，<b>N</b>表示離開的時候需要拍卡。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Reason ID", "預設的原因ID。");
if($sys_custom['StaffAttendance']['WongFutNamCollege']){
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Attend One Time Slot", "職員只需要出席同一日的其中一個職務時段。1表示是，0表示否。");
}
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'] = array();
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("User Login", "教職員用戶登入帳號");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Date", "日期 (格式為年月日 yyyy-mm-dd)。");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Duty Type", "<b>D</b>表示需要當值，<b>L</b>表示放假，<b>FL</b>表示全日放假，<b>O</b>表示外出工作，<b>FO</b>表示全日外出工作，<b>N</b>表示沒有職務。");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Time Slot", "職務時段名稱。全日設定請留空。不能包含符號: % & ^ @ \" \\ ' : ; < > , . / ? [ ] ~ ! # $ * ( ) { } | = + - ");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Duty Start", "職務開始時間，格式為hh:mm:ss。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Duty End", "職務結束時間，格式為hh:mm:ss。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Duty Count", "職務比重，例如: 0.5 表示半日，1.0 表示全日。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Waive In", "<b>Y</b>表示進入的時候不需要拍卡，<b>N</b>表示進入的時候需要拍卡。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Waive Out", "<b>Y</b>表示離開的時候不需要拍卡，<b>N</b>表示離開的時候需要拍卡。全日設定請留空。");
$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Reason ID", "預設的原因ID。");
if($sys_custom['StaffAttendance']['WongFutNamCollege']){
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Attend One Time Slot", "職員只需要出席同一日的其中一個職務時段。1表示是，0表示否。");
}
$Lang['StaffAttendance']['ImportSpecialDutyWarning'] = array();
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankGroupID'] = "小組ID不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['GroupIDIsNotFound'] = "找不到小組ID %1。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankUserLogins'] = "用戶登入帳號不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['UserloginIsNotFound'] = "找不到用戶登入帳號 %1。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['UserloginNotBelongToGroup'] = "教職員 %1 不是ID編號為 %2 的小組成員。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankUserLogin'] = "用戶登入帳號不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['IndividualUserIsNotFound'] = "找不到個人用戶 %1。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDate'] = "日期不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['DateIsInvalid'] = "日期不正確。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankTimeSlot'] = "職務時段不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['TimeSlotContainsInvalidSymbols'] = "職務時段 %1 含有不正確的符號。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDutyStart'] = "職務開始時間不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['DutyStartTimeIsInvalid'] = "職務開始時間不正確。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDutyEnd'] = "職務結束時間不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['DutyEndTimeIsInvalid'] = "職務結束時間不正確。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDutyCount'] = "職務比重不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['DutyCountMustUseNonNegativeNumber'] = "職務比重應該使用非負數。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankWaiveIn'] = "進入不需要拍卡 不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['WaiveInValueInvalid'] = "進入不需要拍卡 不正確。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankWaiveOut'] = "離開不需要拍卡 不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['WaiveOutValueInvalid'] = "離開不需要拍卡 不正確。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDutyType'] = "職務類型 不能留空。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['DutyTypeValueInvalid'] = "職務類型 不正確";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['ReasonMismatchesWithDutyType'] = "原因不對應職務類型。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasOverlappingTimeSlots'] = "%1 的職務時段時間與前面紀錄裡的職務時段時間重複了。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasSameTimeSlotName'] = "%1 的職務時段名稱與前面紀錄裡的職務時段名稱重複了。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffShouldNotHaveDutyOnThatDay'] = "%1 在該日已設定為無職務，請不要再指定職務。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasSetDutyThatDay'] = "%1 在該日已設定了職務，請不要再指定全日職務。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasSetFulldayLeaveThatDay'] = "%1 在該日已設定為全日放假，請不要再指定職務。";
$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasSetFulldayOutingThatDay'] = "%1 在該日已設定為全日外出工作，請不要再指定職務。";
$Lang['StaffAttendance']['ImportSpecialDutyHeader'] = array();
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Group'] = "小組";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Staff'] = "教職員";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Date'] = "日期";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['TimeSlot'] = "職務時段";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['DutyStart'] = "職務開始時間";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['DutyEnd'] = "職務結束時間";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['DutyCount'] = "職務比重";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['WaiveIn'] = "進入不需要拍卡";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['WaiveOut'] = "離開不需要拍卡";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['DutyType'] = "職務類型";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Reason'] = "原因";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['AttendOneTimeSlot'] = "只需出席一個職務時段";
$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Errors'] = "錯誤";
$Lang['StaffAttendance']['NoTapCardReport'] = "無拍卡報告";
$Lang['StaffAttendance']['Search'] = "尋找";
$Lang['StaffAttendance']['NoInTime'] = "無進入時間";
$Lang['StaffAttendance']['NoOutTime'] = "無離開時間";
$Lang['StaffAttendance']['IncludeStatus'] = "包括狀況";
$Lang['StaffAttendance']['RequestCheckOptionWarningMsg'] = "請至少選擇一個選項。";
$Lang['StaffAttendance']['Symbol'] = "符號";
$Lang['StaffAttendance']['Color'] = "顏色";
$Lang['StaffAttendance']['DisplayStatusAs'] = "顯示狀況為";
$Lang['StaffAttendance']['Icon'] = "圖標";
$Lang['StaffAttendance']['ShowAbsentSuspectedRecord'] = "顯示懷疑缺席紀錄";
$Lang['StaffAttendance']['ExportSpecialDuty'] = "匯出特別職務";
$Lang['StaffAttendance']['DoNotDisplayTimeSlot'] = "不顯示當值時段";
$Lang['StaffAttendance']['WorkingHoursDeduction'] = "扣減工作時數";
$Lang['StaffAttendance']['DeductHours'] = "扣減時數";
$Lang['StaffAttendance']['CheckingResult'] = "核對結果";
$Lang['StaffAttendance']['OTRecordIsValid'] = "該超時工作紀錄有效。";
$Lang['StaffAttendance']['NoAttendanceRecordAndOTRecordInvalid'] = "當天沒有考勤紀錄，無法添加超時工作紀錄。";
$Lang['StaffAttendance']['DuplicatedOTRecord'] = "當天已經有職員的超時工作紀錄。";
$Lang['StaffAttendance']['ReportFormat'] = "報告形式";
$Lang['StaffAttendance']['DailyWorkingDetailByStaff'] = "按人列出每天工作資料";
$Lang['StaffAttendance']['OverallReportOfDailyWorkingHours'] = "大表列出各人每天工作時數";
$Lang['StaffAttendance']['Staff2'] = "員工";
$Lang['StaffAttendance']['TotalHours'] = "總時數";
$Lang['StaffAttendance']['Check'] = "核對";
$Lang['StaffAttendance']['CheckRecord'] = "核對紀錄";
$Lang['StaffAttendance']['OTHoursMatched'] = "時數相符。員工當天於<!--LEAVE_TIME-->離校，超時<!--HOUR-->小時工作。";
$Lang['StaffAttendance']['OTHoursNotMatch'] = "時數不符。員工當天於<!--LEAVE_TIME-->離校，超時<!--HOUR-->小時工作。";
$Lang['StaffAttendance']['AttendanceRecordNoOutTime'] = "當天考勤紀錄沒有離開時間。";
$Lang['StaffAttendance']['OTNotEnoughTapCardTimes'] = "該員工當天沒有足夠的拍卡時間用以計算超時時數。";
$Lang['StaffAttendance']['TapCardTimesMatchOTHours'] = "時數相符。 員工當天於 <!--IN_TIME--> 返校並在 <!--OUT_TIME--> 離校，超時<!--HOUR-->小時工作。";
$Lang['StaffAttendance']['TapCardTimesDoesNotMatchOTHours'] = "時數不符。 員工當天於 <!--IN_TIME--> 返校並在 <!--OUT_TIME--> 離校，超時<!--HOUR-->小時工作。";
$Lang['StaffAttendance']['CheckOTTime'] = "核對超時工作";
$Lang['StaffAttendance']['OTDate'] = "超時工作日期";
$Lang['StaffAttendance']['OTHours'] = "超時時數";
$Lang['StaffAttendance']['AttendOnlyOneTimeSlot'] = "職員只需要出席以下設定的其中一個職務時段?";
$Lang['StaffAttendance']['CheckDuty'] = "檢查職務";
$Lang['StaffAttendance']['OnlyCheckTapCardTimes'] = "只檢查拍卡時間";
$Lang['StaffAttendance']['NoNeedToCheckDutyAndTapCardTimes'] = "不需要檢查職務及拍卡時間";
$Lang['StaffAttendance']['OverallAttendanceRecords'] = "整體考勤紀錄";
$Lang['StaffAttendance']['LeaveRecordByStaff'] = "請假紀錄 (按職員)";
$Lang['StaffAttendance']['LeaveRecordByDate'] = "請假紀錄 (按日期)";
$Lang['StaffAttendance']['LeaveRecordByDateInstruction'] = "請先選擇職員和日期，然後按[套用]編輯請期紀錄。";
$Lang['StaffAttendance']['LeaveRecordCannotEditPastRecordRemark'] = "備註: 今天和過去的紀錄不能被編輯。";
$Lang['StaffAttendance']['DutyChangeLog'] = "職務更改日誌";
# Staff Attendance V3 End

# ePOS start
$Lang['ePOS']['PurchaseOnBehalfOfStudent'] = '代購';
$Lang['ePOS']['Transaction'] = '交易';
$Lang['ePOS']['Inventory'] = '存貨';
$Lang['ePOS']['POSTransactionReport'] = 'POS 交易報告';
$Lang['ePOS']['POSCancelTransactionReport'] = 'POS 取消交易報告';
$Lang['ePOS']['POSItemReport'] = 'POS 項目統計';
$Lang['ePOS']['POSStudentReport'] = 'POS 用戶統計';
$Lang['ePOS']['ItemSetting'] = '項目設定';
$Lang['ePOS']['HealthIngredientSetting'] = '健康指標';
$Lang['ePOS']['TerminalSetting'] = '智能咭終端機設定';
$Lang['ePOS']['MerchantSetting'] = '商戶設定';
$Lang['ePOS']['CategorySetting'] = '項目類別設定';
$Lang['ePOS']['Category&ItemSetting'] = '項目類別及項目';
$Lang['ePOS']['Category'] = '項目類別';
$Lang['ePOS']['Item'] = '項目';
$Lang['ePOS']['Inventory'] = '存貨數量';
$Lang['ePOS']['PickupManagement'] = '取貨管理';
$Lang['ePOS']['CurrentInventory'] = '現有存貨數量';
$Lang['ePOS']['AllowClientProgramConnection'] = '允許POS客戶端連接';
$Lang['ePOS']['Name'] = '名稱';
$Lang['ePOS']['Code'] = '編號';
$Lang['ePOS']['Photo'] = '相片';
$Lang['ePOS']['Barcode'] = '條碼';
$Lang['ePOS']['UnitPrice'] = '單價';
$Lang['ePOS']['Description'] = '描述';
$Lang['ePOS']['RepresentDisabledCategory'] = '表示該項目類別已停用。';
$Lang['ePOS']['IncludeItem'] = '包括項目';
$Lang['ePOS']['AllCategories'] = '所有項目類別';
$Lang['ePOS']['PageLoadingTime'] = '載入此頁時間';
$Lang['ePOS']['InventoryNotRealTimeRemarks'] = '以下顯示的存貨數量是以「載入此頁時間」為準。如你要得到最新資料，請重新載入此頁面。';
$Lang['ePOS']['IncreaseInventory'] = '增加存貨數量';
$Lang['ePOS']['DecreaseInventory'] = '減少存貨數量';
$Lang['ePOS']['ViewPhoto'] = '檢視相片';
$Lang['ePOS']['ViewLog'] = '檢視紀錄';
$Lang['ePOS']['IncreaseBy'] = '增加存貨數量';
$Lang['ePOS']['DecreaseBy'] = '減少存貨數量';
$Lang['ePOS']['InventoryUpdateLog'] = '存貨數量更新紀錄';
$Lang['ePOS']['UpdateTime'] = '更新時間';
$Lang['ePOS']['UpdatedBy'] = '更新用戶';
$Lang['ePOS']['InventoryAfter'] = '更新後存貨數量';
$Lang['ePOS']['CostPrice'] = '成本價';
$Lang['ePOS']['CategoryPhotoDimensionRemarks'] = '項目類別相片不能大於 95 x 95。';
$Lang['ePOS']['ItemPhotoDimensionRemarks'] = '項目相片不能大於 180 x 120 (寬 x 高)。';
$Lang['ePOS']['NoOfItems'] = '項目數量';
$Lang['ePOS']['Completed'] = '已完成';
$Lang['ePOS']['NotCompleted'] = '未完成';
$Lang['ePOS']['ExportPickupList'] = '匯出提貨清單';
$Lang['ePOS']['SingleOrderDetails'] = '單筆訂單詳情';
$Lang['ePOS']['NumberOfItems'] = '件物品';
$Lang['ePOS']['VoidThisOrder'] = '註銷本訂單';
$Lang['ePOS']['ItemDoNotTake'] = '訂單中未領取物品';
$Lang['ePOS']['ItemTaken'] = '訂單中已領取物品';
$Lang['ePOS']['ChangeItemAndVoidRecord'] = '換貨及註銷記錄';
$Lang['ePOS']['Taken'] = '已領取';
$Lang['ePOS']['NotTaken'] = '未領取';
$Lang['ePOS']['ChangeItem'] = '更換貨品';
$Lang['ePOS']['WriteoffItem'] = '註銷貨品';
$Lang['ePOS']['Take'] = '領取';
$Lang['ePOS']['PickupAll'] = '全單領取';
$Lang['ePOS']['PrintReceipt'] = '列印收據';
$Lang['ePOS']['ChangeTo'] = '更換成';
$Lang['ePOS']['Writeoff'] = '已註銷';
$Lang['ePOS']['SamePriceItemThatAllowToChange'] = '可供更換的同價物品';
$Lang['ePOS']['Item'] = '件';
$Lang['ePOS']['ExportOrderList'] = '匯出提貨清單';
$Lang['ePOS']['ChooseTheOrderingToExportOrderList'] = '選擇匯出清單的排序方法';
$Lang['ePOS']['OrderByInvoiceNumber'] = '以發單編號排序';
$Lang['ePOS']['OrderByClassAndClassNumber'] = '以班別及學號排序';

$Lang['ePOS']['Btn']['BackToCategorySettings'] = '返回項目類別設定';

$Lang['ePOS']['Add']['Category'] = '新增項目類別';
$Lang['ePOS']['Add']['Item'] = '新增項目';
$Lang['ePOS']['Add']['HealthIngredient'] = "新增營養指標";

$Lang['ePOS']['Edit']['Category'] = '編輯項目類別';
$Lang['ePOS']['Edit']['Item'] = '編輯項目';
$Lang['ePOS']['Edit']['HealthIngredient'] = "編輯營養指標";

$Lang['ePOS']['Move']['Category'] = '移動項目類別';
$Lang['ePOS']['Move']['Item'] = '移動項目';
$Lang['ePOS']['Move']['HealthIngredient'] = "移動營養指標";

$Lang['ePOS']['Manage']['HealthIngredient'] = '管理項目健康指標';

$Lang['ePOS']['WarningArr']['Blank']['Code'] = '請輸入編號。';
$Lang['ePOS']['WarningArr']['Blank']['Name'] = '請輸入名稱。';
$Lang['ePOS']['WarningArr']['Blank']['UnitPrice'] = '請輸入單價。';
$Lang['ePOS']['WarningArr']['Blank']['Barcode'] = '請輸入條碼。';
$Lang['ePOS']['WarningArr']['Blank']['Adjustment'] = '請輸入調節數字';
$Lang['ePOS']['WarningArr']['Blank']['HealthIngredientCode'] = "請輸入代碼";
$Lang['ePOS']['WarningArr']['Blank']['HealthIngredientName'] = "請輸入名稱";
$Lang['ePOS']['WarningArr']['Blank']['UnitName'] = "請輸入單位";
$Lang['ePOS']['WarningArr']['Blank']['StandardIntakePerDay'] = "請輸入每日標準攝取量";

$Lang['ePOS']['WarningArr']['Duplicated']['Code'] = '代號重複。';
$Lang['ePOS']['WarningArr']['Duplicated']['Name'] = '名稱重複。';

$Lang['ePOS']['WarningArr']['NotFloat']['UnitPrice'] = '單價應為數字。';
$Lang['ePOS']['WarningArr']['NotFloat']['StandardIntakePerDay'] = "每日標準攝取量應為數字";
$Lang['ePOS']['WarningArr']['NotFloat']['StandardIntakePerDay'] = "每日標準攝取量應為數字";

$Lang['ePOS']['WarningArr']['NotInteger']['Adjustment'] = '調節存貨數量應為整數。';

$Lang['ePOS']['WarningArr']['GreaterThanZero']['UnitPrice'] = '單價必須大於零。';
$Lang['ePOS']['WarningArr']['GreaterThanZero']['InventoryAdjustment'] = '調節存貨數量必須大於零。';

$Lang['ePOS']['WarningArr']['InvalidFile'] = '檔案不正確。';
$Lang['ePOS']['WarningArr']['Barcode']['LengthTooLong'] = '條碼數不能多於一百字。';
$Lang['ePOS']['WarningArr']['Barcode']['FormatInvalid'] = '條碼格式錯誤。';
$Lang['ePOS']['WarningArr']['NoteEnoughtInventory'] = '沒有足夠存貨。';
$Lang['ePOS']['WarningArr']['Unique']['HealthIngredientCode'] = "代碼已被使用";
$Lang['ePOS']['WarningArr']['Unique']['HealthIngredientName'] = "名稱已被使用";
$Lang['ePOS']['WarningArr']['SelectedPhotoTooLarge'] = '已選擇的相片過大。';

$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings'] = '因系統現在允許POS客戶端連接，所以系統未能更新項目類別資料。你是否要更新設定為拒絕POS客戶端連接？';
$Lang['ePOS']['jsWarningArr']['DeletePhoto'] = '你是否確定要刪除相片？';
$Lang['ePOS']['jsWarningArr']['Max10EnableCategory'] = '因項目類別之啟用上限為十個類別，所以未能啟用此類別。';
$Lang['ePOS']['jsWarningArr']['StartDateCannotGreaterThanEndDate'] = "開始日期必需為結束日期之前。";
$Lang['ePOS']['jsWarningArr']['DeleteHealthIngredient'] = '你是否確定要刪除營養指標?';
$Lang['ePOS']['jsWarningArr']['CollectAllItems'] = '你是否確定要全部領取此訂單的貨品?';
$Lang['ePOS']['jsWarningArr']['CollectTheItems'] = '你是否確定要領取此訂單的貨品?';
$Lang['ePOS']['jsWarningArr']['ChangeTheItems'] = '你是否確定要更換此訂單的貨品?';
$Lang['ePOS']['jsWarningArr']['VoidTheItems'] = '你是否確定要註銷此訂單的貨品?';

$Lang['ePOS']['ReturnMessage']['TerminalSettingsUpdateSuccess'] = "1|=|智能咭終端機設定成功。";
$Lang['ePOS']['ReturnMessage']['TerminalSettingsUpdateFailed'] = "0|=|智能咭終端機設定失敗。";

$Lang['ePOS']['ReturnMessage']['Add']['Success']['Category'] = "1|=|項目類別新增成功。";
$Lang['ePOS']['ReturnMessage']['Add']['Success']['Item'] = "1|=|項目新增成功。";
$Lang['ePOS']['ReturnMessage']['Add']['Failed']['Category'] = "0|=|項目類別新增失敗。";
$Lang['ePOS']['ReturnMessage']['Add']['Failed']['Item'] = "0|=|項目新增失敗。";
$Lang['ePOS']['ReturnMessage']['Edit']['Success']['Category'] = "1|=|項目類別編輯成功。";
$Lang['ePOS']['ReturnMessage']['Edit']['Success']['Item'] = "1|=|項目編輯成功。";
$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['Category'] = "0|=|項目類別編輯失敗。";
$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['Item'] = "0|=|項目編輯失敗。";
$Lang['ePOS']['ReturnMessage']['Update']['Success']['Inventory'] = "1|=|存貨更新成功。";
$Lang['ePOS']['ReturnMessage']['Update']['Failed']['Inventory'] = "0|=|存貨更新失敗。";
$Lang['ePOS']['ReturnMessage']['Update']['Failed']['NotEnoughInventory'] = "0|=|沒有足夠存貨。";
$Lang['ePOS']['ReturnMessage']['Add']['Success']['HealthIngredient'] = "1|=|加入營養指標成功";
$Lang['ePOS']['ReturnMessage']['Add']['Failed']['HealthIngredient'] = "0|=|加入營養指標失敗";
$Lang['ePOS']['ReturnMessage']['Edit']['Success']['HealthIngredient'] = "1|=|修改營養指標成功";
$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['HealthIngredient'] = "0|=|修改營養指標失敗";
$Lang['ePOS']['ReturnMessage']['Delete']['Success']['HealthIngredient'] = "1|=|刪除營養指標成功";
$Lang['ePOS']['ReturnMessage']['Delete']['Failed']['HealthIngredient'] = "0|=|刪除營養指標失敗";

$Lang['ePOS']['SelectDateRange'] = "選擇時間範圍";
$Lang['ePOS']['From'] = "由";
$Lang['ePOS']['To'] = "至";
$Lang['ePOS']['ProcessBy'] = "處理者";
$Lang['ePOS']['VoidBy'] = "取消者";
$Lang['ePOS']['ProcessDate'] = "處理日期";
$Lang['ePOS']['VoidDate'] = "取消日期";
$Lang['ePOS']['UserName'] = "名稱";
$Lang['ePOS']['ClassName'] = "班別";
$Lang['ePOS']['ClassNumber'] = "學號";
$Lang['ePOS']['RefCode'] = "參考編號";
$Lang['ePOS']['TransactionTimes'] = "交易次數";
$Lang['ePOS']['LastTransactionDate'] = "最後交易日期";
$Lang['ePOS']['Remark'] = "備註";
$Lang['ePOS']['Void'] = "取消此交易";
$Lang['ePOS']['ReportVoidTitle'] = "取消POS 交易";
$Lang['ePOS']['VoidTransactionSuccess'] = "1|=|交易取消成功";
$Lang['ePOS']['VoidTransactionFail'] = "0|=|交易取消失敗";
$Lang['ePOS']['VoidTransactionConfirmMsg'] = "此動作不能回復。 \\n您是否確定要取消此交易?";


$Lang['ePOS']['UnitName'] = '單位';
$Lang['ePOS']['StandardIntakePerDay'] = "每日標準攝取量";
$Lang['ePOS']['HealthIngredient']['InUse'] = "使用";
$Lang['ePOS']['HealthIngredient']['NotInUse'] = "不使用";
$Lang['ePOS']['TransactionTime'] = "交易時間";
$Lang['ePOS']['ReportGenerationTime'] = "報表建立時間";
$Lang['ePOS']['StudentRemoved'] = "<font color=red>*</font> - 表示該用戶已被移除.";
$Lang['ePOS']['DisplayFormat'] = "顯示方式";
$Lang['ePOS']['PrinterFriendlyPage'] = "可列印格式";
$Lang['ePOS']['CSV'] = 'CSV';
$Lang['ePOS']['POSSalesReport'] = "POS 銷售統計";
$Lang['ePOS']['PrintTransactionDetail'] = "列印交易明細";
$Lang['ePOS']['ItemIntake'] = "項目攝取量";
$Lang['ePOS']['Unit'] = "單位";

$Lang['ePOS']['InventoryReturnSetting'] = "取消時回撥存貨選項";
$Lang['ePOS']['InventoryReturn'] = "回撥存貨數量";
$Lang['ePOS']['InventoryReturnRemark'] = "將會回撥為存貨的數量，如交易項目與回撥總數不符，差額將被視為報廢。";
$Lang['ePOS']['InventoryReturnedRemark'] = "已回撥為存貨的數量，如交易項目與回撥總數不符，差額被視為報廢。";
$Lang['ePOS']['GeneralTerminalSetting'] = "一般設定";
$Lang['ePOS']['TerminalManagementSetting'] = "終端機管理";
$Lang['ePOS']['TerminalIndependentCategorySetting'] = "終端機獨立設定".$Lang['ePOS']['Category'];
$Lang['ePOS']['SiteName'] = '位置';
$Lang['ePOS']['IP'] = '最後連接網絡位址';
$Lang['ePOS']['LastConnectTime'] = '最後連接時間';
$Lang['ePOS']['LastPhotoSyncTime'] = '最後同步相片時間';
$Lang['ePOS']['MacAddress'] = '實體網絡位址';
$Lang['ePOS']['AddSite'] = 'Add '.$Lang['ePOS']['SiteName'];
$Lang['ePOS']['SiteNameEmptyDuplicateWarning'] = '*站台名稱重複或是空白';
$Lang['ePOS']['TerminalSaveSuccess'] = '1|=|終端機儲存成功 ';
$Lang['ePOS']['TerminalSaveFail'] = '0|=|終端機儲存失敗 ';
$Lang['ePOS']['ClearClientTerminalLinkage'] = '解除終端機連接狀態';
$Lang['ePOS']['ClearClientTerminalLinkageConfirmWarning'] = '您是否確定要解除此終端機連接狀態? \\n(請確定終端機已停止連接, 否則它會自動重新連接)';
$Lang['ePOS']['SetTerminalCategorySetting'] = '設定項目類別關聯';
$Lang['ePOS']['Rename'] = '重新名命';
$Lang['ePOS']['RemoveTerminalSetting'] = '所有項目類別關聯將會移除。\\n 您是否確定要繼續?';
$Lang['ePOS']['TerminalDeleteSuccess'] = '1|=|終端機移除成功 ';
$Lang['ePOS']['TerminalDeleteFail'] = '0|=|終端機移除失敗 ';
$Lang['ePOS']['StationName'] = '站台名稱';
$Lang['ePOS']['CategoryAssociated'] = '關聯的項目類別';
$Lang['ePOS']['TerminalUnassigned'] = '未分類';
$Lang['ePOS']['AssignToAnotherSite'] = '指派到其他'.$Lang['ePOS']['SiteName'];
$Lang['ePOS']['TemplateSaveSuccess'] = '1|=|'.$Lang['ePOS']['SiteName'].'儲存成功 ';
$Lang['ePOS']['TemplateSaveFail'] = '0|=|'.$Lang['ePOS']['SiteName'].'儲存失敗 ';
$Lang['ePOS']['SyncPhotoForThisTerminal'] = '下次連接時同步化相片';
$Lang['ePOS']['LastPhotoRequestBy'] = '最後同步相片要求者';
$Lang['ePOS']['LastPhotoRequestTime'] = '最後同步相片要求日期';
$Lang['ePOS']['RemoveSite'] = '移除'.$Lang['ePOS']['SiteName'];
$Lang['ePOS']['RemoveSiteWarning'] = '所有相關聯的站台將會重新指派到"'.$Lang['ePOS']['TerminalUnassigned'].'". \\n您是否確定要繼續?';
$Lang['ePOS']['TemplateDeleteSuccess'] = '1|=|'.$Lang['ePOS']['SiteName'].'移除成功 ';
$Lang['ePOS']['TemplateDeleteFail'] = '0|=|'.$Lang['ePOS']['SiteName'].'移除失敗 ';
$Lang['ePOS']['DeleteTerminal'] = '移除站台';
$Lang['ePOS']['SyncPhotoForAllTerminal'] = '所有站台同步相片';
$Lang['ePOS']['TranscationTime'] = '交易時間';
$Lang['ePOS']['ConfirmSyncAllPhoto'] = '您是否確定需要同步所以站台的相片?';
$Lang['ePOS']['TerminalIPList'] = "終端機的 IP 位址";
$Lang['ePOS']['TerminalIPInput'] = "請把終端機的 IP 地址每行一個輸入";
$Lang['ePOS']['TerminalYourAddress'] = "你現時的 IP 地址";
$Lang['ePOS']['LogType'] = "紀錄類別";
$Lang['ePOS']['TransactionMaking'] = "交易賣出";
$Lang['ePOS']['TransactionVoid'] = "交易退回";
$Lang['ePOS']['TransactionItemChange'] = "交易項目更換";
$Lang['ePOS']['InventoryAdd'] = "增加存貨";
$Lang['ePOS']['InventoryDeduct'] = "減小存貨";
$Lang['ePOS']['IncludeTransactionDetail'] = "包括交易相關紀錄";
$Lang['ePOS']['HealthPersonalReport'] = "個人健康指標報告";
$Lang['ePOS']['HealthClassReport'] = "班別健康指標報告";
$Lang['ePOS']['AvgIntake'] = "每平日均攝取量";
$Lang['ePOS']['Various'] = "超標";
$Lang['ePOS']['TargetDate'] = "目標日期";
$Lang['ePOS']['SelectAtLeast1StudentWarning'] = "*請最少選擇一位用戶";
$Lang['ePOS']['SelectAtLeast1ClassWarning'] = "*請最少選擇一個班別";
$Lang['ePOS']['SelectItemToReplaceWarning'] = "請選擇要更換的物品";
$Lang['ePOS']['ExceedNumberOfPeople'] = "超標人數";
$Lang['ePOS']['SelectMerchantID'] = "請選擇商戶";

$Lang['ePOS']['PaymentMethod'] = "付款方法";
$Lang['ePOS']['TapCard'] = "拍卡";
$Lang['ePOS']['StudentSelection'] = "選取學生";
$Lang['ePOS']['ChangeLog'] = "更改日誌";
$Lang['ePOS']['ExportChangeLog'] = "匯出更改日誌";
$Lang['ePOS']['VoidItem'] = "取消項目";
$Lang['ePOS']['SoldItem'] = "銷售項目";
$Lang['ePOS']['IncreaseItemInventory'] = "增加項目庫存";
$Lang['ePOS']['DecreaseItemInventory'] = "減少項目庫存";
$Lang['ePOS']['ChangedAmount'] = "更改數量";
$Lang['ePOS']['SalesLocation'] = "銷售地點";
$Lang['ePOS']['ItemTotal'] = "項目總額";
$Lang['ePOS']['ConfirmBeforePaying'] = "繳費前需要確認?";
$Lang['ePOS']['eClassApp']['BackToMain'] = "返回主頁";
$Lang['ePOS']['eClassApp']['Balance'] = "可用餘額";
$Lang['ePOS']['eClassApp']['CancelThisOrder'] = "取消此訂單";
$Lang['ePOS']['eClassApp']['ChangeGoods'] = "已退換記錄";
$Lang['ePOS']['eClassApp']['ChangeTo'] = "已更換成";
$Lang['ePOS']['eClassApp']['Checkout'] = "結帳";
$Lang['ePOS']['eClassApp']['CollectedOrder'] = "已領取訂單";
$Lang['ePOS']['eClassApp']['CollectTime'] = "取貨時間";
$Lang['ePOS']['eClassApp']['DownloadReceipt'] = "下載收據";
$Lang['ePOS']['eClassApp']['error']['DatabaseError'] = "數據庫錯誤";
$Lang['ePOS']['eClassApp']['error']['Deflict'] = "餘額不足";
$Lang['ePOS']['eClassApp']['error']['IncorrectPath'] = "路徑錯誤";
$Lang['ePOS']['eClassApp']['error']['LostTransaction'] = "未能完成訂單";
$Lang['ePOS']['eClassApp']['error']['MissingAccount'] = "未能確認支付者";
$Lang['ePOS']['eClassApp']['error']['NoItemDetail'] = "貨品明細欠缺";
$Lang['ePOS']['eClassApp']['error']['NoOutTradeNo'] = "沒有支付寶參考編號";
$Lang['ePOS']['eClassApp']['error']['OutOfStock'] = "存貨不足";
$Lang['ePOS']['eClassApp']['error']['PleaseWaitForStudentLoad'] = "請等待或刷新本頁以確認學生";
$Lang['ePOS']['eClassApp']['error']['UnknownError'] = "原因不明錯誤";
$Lang['ePOS']['eClassApp']['error']['UserNotFound'] = "找不到學生";
$Lang['ePOS']['eClassApp']['InformParentToCheckout'] = "請通知家長結帳";
$Lang['ePOS']['eClassApp']['js']['CancelOrder'] = '你是否確定要取消此訂單?';
$Lang['ePOS']['eClassApp']['js']['RemoveItem'] = '你是否確定要從訂單移除此物品?';
$Lang['ePOS']['eClassApp']['LastPickupTime'] = "上次取貨時間";
$Lang['ePOS']['eClassApp']['MaxNumberOfItems'] = "限購 %s 件";
$Lang['ePOS']['eClassApp']['NoSelectedItem'] = "暫無已選物品";
$Lang['ePOS']['eClassApp']['NumberOfItems'] = "%s 件物品";
$Lang['ePOS']['eClassApp']['OrderTime'] = "訂購時間";
$Lang['ePOS']['eClassApp']['OutOfStock'] = "暫時缺貨";
$Lang['ePOS']['eClassApp']['OutOfStockItem'] = "以下物品暫時缺貨:";
$Lang['ePOS']['eClassApp']['Pay'] = "付款";
$Lang['ePOS']['eClassApp']['PayByAlipay']['Instruction_1'] = "現在使用「支付寶HK」進行付款。";
$Lang['ePOS']['eClassApp']['PayByAlipay']['Instruction_2'] = "完成後，將會自動跳轉回本系統。";
$Lang['ePOS']['eClassApp']['PayByFollowingAcct'] = "使用以下學生帳戶餘額支付";
$Lang['ePOS']['eClassApp']['PaymentComplete'] = "付款完成";
$Lang['ePOS']['eClassApp']['PendingOrder'] = "未領取訂單";
$Lang['ePOS']['eClassApp']['Receipt'] = "收據";
$Lang['ePOS']['eClassApp']['ReturnGoods'] = "已退貨";
$Lang['ePOS']['eClassApp']['SearchResult'] = "搜尋結果";
$Lang['ePOS']['eClassApp']['SelectedItems'] = "已選物品";
$Lang['ePOS']['eClassApp']['Title'] = 'eClass 銷售系統';
$Lang['ePOS']['eClassApp']['TotalQty'] = "總計";
$Lang['ePOS']['eClassApp']['ViewOrderDetail'] = "查看訂單詳情";
$Lang['ePOS']['PaidByAlipay'] = "AlipayHK 支付 / AlipayHK Paid";
$Lang['ePOS']['TransactionStatus']['Cancelled'] = "付款取消";
$Lang['ePOS']['TransactionStatus']['Complete'] = "付款完成";
$Lang['ePOS']['TransactionStatus']['DoNotCloseWindow'] = "付款進行中,請勿關閉本視窗";
$Lang['ePOS']['TransactionStatus']['Failed'] = "付款失敗";
$i_ePOS_InventoryImportFileDescription1 = "條碼, 成本價, 增加存貨數量, 備註";
$i_ePOS_InventoryImport_NoMatch_Entry = "條碼錯誤";
$i_ePOS_InventoryImport_InvalidIncreaseBy = "增加存貨數量無效";
$i_ePOS_InventoryImport_Import_Action_New = "新增存貨";
$i_ePOS_InventoryImport_InvalidCostPrice = "成本價無效";
$Lang['ePOS']['TransactionDetails']['Refund'] = '交易已退款';
# ePOS end

# subject Group Mapping
$i_subjectGroupMapping_showStat = "展示統計數字";
$i_subjectGroupMapping_hideStat = "隱藏統計數字";

# text to speech
$Lang['Header']['Menu']['PowerSpeech'] 		= "語音發聲工具";
$Lang['Header']['Menu']['PowerTool']		= "網上教學增值工具";
$Lang['TextToSpeech']['Error'] = '0|=|開啟MP3失敗.';
$Lang['TextToSpeech']['SpeechSetting'] = '語音設定';
$Lang['TextToSpeech']['FullText'] = '全文發聲';
$Lang['TextToSpeech']['FullPage'] = '全文發聲';
$Lang['TextToSpeech']['Highlighted'] = 'Highlight發聲';
$Lang['TextToSpeech']['HighlightedText'] = 'Highlight發聲';
$Lang['TextToSpeech']['Voice'] = '聲音';
$Lang['TextToSpeech']['Male'] = '男';
$Lang['TextToSpeech']['Female'] = '女';
$Lang['TextToSpeech']['Speed'] = '速度';
$Lang['TextToSpeech']['Slowest'] = '最慢';
$Lang['TextToSpeech']['Slow'] = '慢';
$Lang['TextToSpeech']['Normal'] = '正常';
$Lang['TextToSpeech']['NoHighlightError'] = '請選擇文字';
$Lang['TextToSpeech']['NoChineseMaleVoice'] = '中文字不支援男聲發音，將會轉用女聲。';
$Lang['TextToSpeech']['Expired'] = "你的PowerSpeech使用期已過。請與你的系統管理員聯繫，以延續使用期。";
$Lang['TextToSpeech']['Instruction'] = "輸入文字後按下方按鈕進行聆聽。請確保你已經開啟電腦的揚聲器。";

$i_transfer_file_ownership = '移交文檔擁有權';

### eSports
$Lang['eSports']['Arrangement_Schedule_Setting'] = "賽程自動安排設定";
$Lang['eSports']['HiddenAutoArrangeFunction'] = "隱藏「". $i_Sports_menu_Arrangement_Schedule ."」功能";
$Lang['eSports']['RefereeRemark'] = "裁判評語";
$i_con_msg_New_Record_Delete = "<font color=green>新紀錄已刪除</font>";
$Lang['eSports']['Sports']['ClassRelay']['EnrolLimitAlert'] = "必須有四至六名學生參與";
$Lang['eSports']['Sports']['ClassRelay']['StudentAlreadyInAnotherLine'] = "部份學生已列入其他線道";
$Lang['eSports']['Sports']['ClassRelay']['ClassIsAlreadySelected'] = "這個班別已被選取";
$Lang['eSports']['Sports']['ClassRelay']['ClassGroupAlreadyInAnotherLine'] = "這個班別隊伍已列入其他線道";
$Lang['eSports']['PleaseSelectAtLeastOneRank'] = "請最少為一個同分者選擇 ";
$Lang['eSports']['ExportForCustExportFormat'] = "匯出客制化格式";
$Lang['eSports']['Sports']['EnrolMinEvent'] = "最少參與項目";
$Lang['eSports']['Sports']['Warn_EnrolMinEvent'] = "你最少須參與";
$Lang['eSports']['Sports']['Print_FinalResultReport_MKSS'] = "列印所有組別總成績排名";
$Lang['eSports']['Sports']['Print_Top3EventResult_MKSS'] = "列印最佳三名決賽者成績";
$Lang['eSports']['Sports']['Export_TrackFieldResult_MKSS'] = "匯出所有項目成績";
$Lang['Sports']['Management']['SyncResultToPortfolio'] = "資料傳送 (至iPortfolio)";
$Lang['Sports']['LastTransferDateTime'] = "最近傳送日期";

### eHomework
$Lang['SysMgr']['Homework']['DefaltHandinRequired'] = "預設家課是必須繳交";
$Lang['SysMgr']['Homework']['ClearHomework'] = "清除家課紀錄";
$i_con_msg_homework_clear = "<font color=green>家課紀錄已被刪除</font>";
$Lang['SysMgr']['Homework']['SelectRecordsCleared'] = "選擇要刪除的紀錄";
$Lang['SysMgr']['Homework']['AllTeachersSubjectLeaders'] = "所有老師及科長";
$Lang['SysMgr']['Homework']['AllSubjectLeaders'] = "所有科長";
$Lang['SysMgr']['Homework']['TeacherSubjectLeader'] = "老師/科長";
$Lang['SysMgr']['Homework']['SubjectGroupTeacher'] = "任教老師";
$Lang['SysMgr']['Homework']['CreatedBy'] = "紀錄建立者";

### Admin Console
// $Lang['SysMgr']['NetWorkContact'] = "此頁面可連結其他系統(如防火牆, 電腦病毒掃瞄)之管理版面. <br> 詳情可與博文教育有限公司聯絡.";
$Lang['SysMgr']['NetWorkContact'] = "你的系統保養狀況：Basic Maintenance<br>如欲使用此功能，請升級至 Premium 或  Vantage 的保養計劃。";

### Group
$Lang['Group']['DisplayInCommunity'] = "在社群管理工具顯示";
$Lang['Group']['AllowDeleteOthersAnnouncement'] = "小組管理員可互相刪除/編輯宣佈";

### General
$Lang['Btn']['Export2'] = '匯出(二)';

# 天主教慈幼會伍少梅中學 cust report (Award & Punishment Monthly report)
$Lang['Btn']['Export_without_ap'] = '匯出 (不包括功過欄)';


# imail gamma
$Lang['Gamma']['Sender'] = '寄件人';
$Lang['Gamma']['To']= '收件人';
$Lang['Gamma']['From']= '由';
$Lang['Gamma']['cc']= '副本';
$Lang['Gamma']['bcc']= '密件副本';
$Lang['Gamma']['Attachment']= '附件';
$Lang['Gamma']['AddAttachment']= '加入附件';
$Lang['Gamma']['RemoveAttachment']= '移除附件';
$Lang['Gamma']['Date'] = '日期';

$Lang['Gamma']['Subject']= '主旨';
$Lang['Gamma']['AddCc'] = '加入副本';
$Lang['Gamma']['AddBcc'] = '加入密件副本';
$Lang['Gamma']['SeparateAddrTips'] = '(請以 ; 或 , 分隔每個電郵地址)';
$Lang['Gamma']['Message']= '內容';
$Lang['Gamma']['iMailGamma']= 'iMail plus';

$Lang['Gamma']['DefaulFolder'] = '郵件匣';
$Lang['Gamma']['PersonalFolder'] = '個人資料夾';
$Lang['Gamma']['SelectAll'] = '全選';
$Lang['Gamma']['App']['ComposeMail']  ='撰寫郵件';
$Lang['Gamma']['App']['FolderCCNBCC'] = '副本/密件';
$Lang['Gamma']['App']['Finish'] = '完成';
$Lang['Gamma']['App']['DeleteEmailHeader'] = '刪除郵件';
$Lang['Gamma']['App']['DeleteEmailTitle'] = '您確定刪除郵件嗎?';
$Lang['Gamma']['App']['DeleteEmailCancel'] = '取消';
$Lang['Gamma']['App']['DeleteEmailComfirm'] = '確認';
$Lang['Gamma']['App']['UploadFileExceedLimit'] = '上載附件大小請不要超過';
$Lang['Gamma']['App']['UploadFileExceedQuota'] = '當前郵件超出限額。';
$Lang['Gamma']['App']['DeviceNoSupportAttachment'] = '當前Android版本不支持添加附件。';
$Lang['Gamma']['App']['NeedEmailAddressInput'] = '請輸入郵件收件人。';
$Lang['Gamma']['App']['NoSubjectComfirm'] = '該郵件沒有主題。您確定要發送嗎?';
$Lang['Gamma']['App']['UploadFileFailed'] = '添加附件失敗。';
$Lang['Gamma']['App']['MoveTo'] = '移至';
$Lang['Gamma']['App']['ConfirmMoveNotification'] = "您確定要移此動郵件嗎?";
$Lang['Gamma']['App']['ConfirmDeleteAddressNotification'] = '您確定要移除此郵箱地址嗎?';
$Lang['Gamma']['App']['UploadFileStyleFailed'] = '郵件不支持此文件格式。';
$Lang['Gamma']['App']['ExternalEmailAccessFail'] = '互聯網郵件不可用。';
$Lang['Gamma']['App']['AllEmailAccessFail'] = '該系統已配置為限制所有電子郵件發送。';
$Lang['Gamma']['App']['Updating'] = '更新中';
$Lang['Gamma']['App']['InvalidAddress'] = '此郵箱地址無效。';
$Lang['Gamma']['App']['NoAccessRight'] = '當前賬戶無權限瀏覽此頁。';
$Lang['Gamma']['App']['MarkMailImportant'] = '列為重要信件';
$Lang['Gamma']['App']['NotifyReceiver'] = '要求回覆';
$Lang['Gamma']['App']['NotificationOnlyForInternal'] = '( 只適用於 內聯網收件人 )';
$Lang['Gamma']['App']['OpenReceiver'] = '展開';
$Lang['Gamma']['App']['HideReceiver'] = '隱藏';

$Lang['MassMailing']['New']= '新增';
$Lang['MassMailing']['Edit']= '編輯';
$Lang['MassMailing']['ShowInfo']= '顯示資料';
$Lang['MassMailing']['ShowContent']= '併合內容為';
$Lang['MassMailing']['InMailBody']= '郵件內容';
$Lang['MassMailing']['AsAttachment']= '附件';
$Lang['MassMailing']['CSVFormat1']= '格式一：一人一行數據';
$Lang['MassMailing']['CSVFormat2']= '格式二: 一人有多行數據';
$Lang['MassMailing']['CSVFile']= 'CSV 資料檔';
$Lang['MassMailing']['AttachmentsZIP']= '電郵附件 ZIP';
$Lang['MassMailing']['Attachments']= '電郵附件';
$Lang['MassMailing']['AttachmentsUploadMore']= '上載更多附件';
$Lang['MassMailing']['AttachmentFiles']= '個檔案';
$Lang['MassMailing']['Attachments_NotFound']= "找不到電郵附件\"<--FileName-->\"";
$Lang['MassMailing']['Attachments_remark'] = "若有附件，必須在 CSV 資料檔內建一欄\"EmailAttachment\"，並指定附給各收件者的檔案名稱（建議用英文檔案名字），而只可發一個附件檔給一位收件者。";

$Lang['MassMailing']['MailBody']= '電郵內容';
$Lang['MassMailing']['HTMLContents']= '網上編輯';
$Lang['MassMailing']['HTMLFile']= '上載 HTML 樣版檔';
$Lang['MassMailing']['HTMLContents_Code_remark']= '為連繫 CSV 檔內的資料，需使用這個格式： <b>[=ColumnTitle=]</b>，例如： [=StudentName=], [=ConductMark=]';
$Lang['MassMailing']['SysAdmin']= '系統總管的';
$Lang['MassMailing']['Yourself']= '你自己的';
$Lang['MassMailing']['NoOfDone']= '已發紀錄';
$Lang['MassMailing']['StatusDraft']= '草稿';
$Lang['MassMailing']['StatusSending']= '已寄出部份';
$Lang['MassMailing']['StatusSent']= '已寄完';
$Lang['MassMailing']['Instruction']= "請先仔細檢查以下資料、有效之收件者及預覽電郵內容，確定所有都正確，才進行發送。";
$Lang['MassMailing']['NoValidRecipient']= "沒有任何有效電郵地址！";
$Lang['MassMailing']['NoStudentImported']= "由於並沒有匯入學生(班別及班號)，故不能發送電郵給家長！";
$Lang['MassMailing']['NoParentRelation']= "於系統內部份學生沒有家長的連結！";
$Lang['MassMailing']['NoUser']= "系統內沒有以下帳戶！";
$Lang['MassMailing']['NoUserLogin']= "部份 UserLogin 可能於系統內不存在或沒有電郵地址";
$Lang['MassMailing']['ParentNoEmail']= "部份家長沒有電郵地址！";
$Lang['MassMailing']['StudentNoEmail']= "部份學生沒有電郵地址！";
$Lang['MassMailing']['NoOfStudents']= "位學生";
$Lang['MassMailing']['NoOfParents']= "位家長";
$Lang['MassMailing']['NoOfAddress']= " 個電郵地址";
$Lang['MassMailing']['ToStudent']= "學生 <span class='tabletextremark'>(根據 CSV 內的 Class and ClassNumber)</span>";
$Lang['MassMailing']['ToParent']= "家長 <span class='tabletextremark'>(根據 CSV 內的 Class and ClassNumber)</span>";
$Lang['MassMailing']['GivenEmails']= "指定人士 <span class='tabletextremark'>(根據 CSV 內的 Email 電郵地址)</span>";
$Lang['MassMailing']['GivenUserLoginID']= "指定人士 <span class='tabletextremark'>(根據 CSV 內的 UserLogin 內聯網帳號)</span>";
$Lang['MassMailing']['And']= "和";
$Lang['MassMailing']['ProblemNoCSV']= "沒有收到 CSV 資料檔！";
$Lang['MassMailing']['ProblemNoHTML']= "沒有收到 HTML 樣版檔！";
$Lang['MassMailing']['ProblemNoSubject']= "請輸入電郵主旨！";
$Lang['MassMailing']['ProblemNoRecipient']= "請選擇收件者！";
$Lang['MassMailing']['BtnReSend']= "重發";
$Lang['MassMailing']['BtnChange']= "更改";
$Lang['MassMailing']['ConfirmToSend']= "是否確定要發送電郵？";
$Lang['MassMailing']['ConfirmToReSend']= "是否確定要重發電郵？";
$Lang['MassMailing']['ConfirmToDelete']= "是否確定要移除此紀錄？";
$Lang['MassMailing']['ConfirmToTerminate']= "是否確定要終止發送電郵？";
$Lang['MassMailing']['LastSendTime']= "最後發送時間";
$Lang['MassMailing']['SendRecord']= "發送紀錄";
$Lang['MassMailing']['SendTime']= "發送時間";
$Lang['MassMailing']['Start2Send']= "現在開始發送 ...";
$Lang['MassMailing']['SendingTo1']= "正在發送到 ";
$Lang['MassMailing']['SendingTo2']= " 個電郵地址 ... ";
$Lang['MassMailing']['RecipientEmail']= "電郵地址";
$Lang['MassMailing']['NewStep1']= "步驟一";
$Lang['MassMailing']['NewStep2']= "步驟二";
$Lang['MassMailing']['NextRecord']= "下一個紀錄";
$Lang['MassMailing']['PreviousRecord']= "上一個紀錄";
$Lang['MassMailing']['PreviewMergedData']= "代表併合的資料";
$Lang['MassMailing']['PreviewMissingData']= "代表遺漏的資料（請檢查 CSV 資料有沒有該欄）";
$Lang['MassMailing']['CSV_CLASS_NO']= "（根據班別及班號）";
$Lang['MassMailing']['CSV_Email']= "（根據電郵地址）";
$Lang['MassMailing']['UserLogin']= "（根據內聯網帳號）";


## Fields
$Lang['AppNotifyMessage']['SchoolNewsForApp'] = "校園最新消息(用於手機應用程式)";
$Lang['AppNotifyMessage']['Date'] = "日期";
$Lang['AppNotifyMessage']['SendTime'] = "傳送時間";
$Lang['AppNotifyMessage']['Title'] = "標題";
$Lang['AppNotifyMessage']['Description'] = "內容";
$Lang['AppNotifyMessage']['AttachImage'] = "附加圖像檔案";
$Lang['AppNotifyMessage']['Recipients'] = "接收人";
$Lang['AppNotifyMessage']['NotifyFor'] = "目的";
$Lang['AppNotifyMessage']['Public'] = "全部家長";
$Lang['AppNotifyMessage']['Public(Teacher)'] = "全部教職員";
$Lang['AppNotifyMessage']['Public(Student)'] = "全部學生";
$Lang['AppNotifyMessage']['ToPublic'] = "傳送給全部家長";
$Lang['AppNotifyMessage']['ToPublic(Teacher)'] = "傳送給全部教職員";
$Lang['AppNotifyMessage']['ToPublic(Student)'] = "傳送給全部學生";
$Lang['AppNotifyMessage']['ToNonPublic'] = "傳送給指定家長";
$Lang['AppNotifyMessage']['ToNonPublic(Teacher)'] = "指定教職員";
$Lang['AppNotifyMessage']['ToNonPublic(Student)'] = "指定學生";
$Lang['AppNotifyMessage']['NonPublic'] = "指定學生家長";
$Lang['AppNotifyMessage']['NonPublic(Teacher)'] = "指定教職員";
$Lang['AppNotifyMessage']['NonPublic(Student)'] = "指定學生";
$Lang['AppNotifyMessage']['UsingParentAppParentOnly'] = "只限現正使用 eClass App 的家長";
$Lang['AppNotifyMessage']['UsingTeacherAppTeacherOnly'] = "只限現正使用 Teacher App 的教職員";
$Lang['AppNotifyMessage']['UsingStudentAppStudentOnly'] = "只限現正使用 Student App 的學生";
$Lang['AppNotifyMessage']['NotifyUser'] = "通知用戶";
$Lang['AppNotifyMessage']['SendStatus'] = "發送狀況";
$Lang['AppNotifyMessage']['NumOfUsers'] = "接收人數目";
$Lang['AppNotifyMessage']['SendMessage'] = "發送消息";
$Lang['AppNotifyMessage']['SystemAdmin'] = "系統管理員";
$Lang['AppNotifyMessage']['ViewDetailedRecord'] = "檢視詳細紀錄";
$Lang['AppNotifyMessage']['AllRecords'] = "全部紀錄";
$Lang['AppNotifyMessage']['AllSenders'] = "所有傳送者";
$Lang['AppNotifyMessage']['FromSystem'] = "由系統傳送";
$Lang['AppNotifyMessage']['FromTeacher'] = "由教職員傳送";
$Lang['AppNotifyMessage']['FromMyself'] = '由自己傳送';
$Lang['AppNotifyMessage']['eNoticeSigned'] = "簽署電子通告確認通知 [[sign_notice_number]]
eNotice Signed Alert [[sign_notice_number]]";
$Lang['AppNotifyMessage']['eNoticeSignedContent'] = "已收到閣下於 [sign_datetime] 簽署的電子通告「[sign_notice_title]」。";
$Lang['AppNotifyMessage']['eNoticeSignedContent_eClassApp'] = "已收到閣下於 [sign_datetime] 簽署的電子通告[sign_notice_number]「[sign_notice_title]」。
Please note that the eNotice [sign_notice_number] titled \"[sign_notice_title]\" signed at [sign_datetime] has been received.";
$Lang['AppNotifyMessage']['eCircularSigned'] = "簽署職員通告確認通知  [[sign_notice_number]]
eCircular Signed Alert [[sign_notice_number]]";
$Lang['AppNotifyMessage']['eCircularSignedContent_eClassApp'] = "已收到閣下於 [sign_datetime] 簽署的職員通告[sign_notice_number]「[sign_notice_title]」。
Please note that the eCircular [sign_notice_number] titled \"[sign_notice_title]\" signed at [sign_datetime] has been received.";
$Lang['AppNotifyMessage']['MethodMessage'] = "訊息";
$Lang['AppNotifyMessage']['MethodSameMessage'] = "相同";
$Lang['AppNotifyMessage']['MethodDifferentMessage'] = "各人的都不同 (需匯入 CSV 檔)";
$Lang['AppNotifyMessage']['CSVFile'] = "接收人及內容 CSV 檔";
$Lang['AppNotifyMessage']['Error']['invalidate_class'] = "錯誤的班別或班號";
$Lang['AppNotifyMessage']['Error']['no_student'] = "找不到該學生";
$Lang['AppNotifyMessage']['Error']['no_teacher'] = "找不到該教職員或該教職員未有使用 Teacher App";
$Lang['AppNotifyMessage']['Error']['no_related_parent'] = "找不到相關家長帳戶";
$Lang['AppNotifyMessage']['Error']['parent_did_not_login_eClassApp'] = "家長未有登入 eClass App";
$Lang['AppNotifyMessage']['Error']['parent_did_not_reg_push_message'] = "家長沒有註冊推送訊息";
$Lang['AppNotifyMessage']['Error']['student_did_not_login_eClassApp'] = '學生未有登入 eClass App';
$Lang['AppNotifyMessage']['Error']['student_did_not_reg_push_message'] = '學生沒有註冊推送訊息';
$Lang['AppNotifyMessage']['Error']['no_title'] = "沒有標題";
$Lang['AppNotifyMessage']['Error']['no_content'] = "沒有內容";
$Lang['AppNotifyMessage']['Error']['duplicated_msg'] = "此學生已重複的訊息";
$Lang['AppNotifyMessage']['CSV_ROW'] = "CSV 行";
$Lang['AppNotifyMessage']['valid_table_title'] = "將會傳送訊息至以下學生的家長:";
$Lang['AppNotifyMessage']['valid_table_title_teacher'] = "將會傳送訊息至以下教職員:";
$Lang['AppNotifyMessage']['valid_table_title_student'] = "將會傳送訊息至以下學生:";
$Lang['AppNotifyMessage']['sending'] = "傳送中 ...";
$Lang['AppNotifyMessage']['SendTime'] = "傳送時間";
$Lang['AppNotifyMessage']['Now'] = "現在";
$Lang['AppNotifyMessage']['SpecificTime'] = "指定時間";
$Lang['AppNotifyMessage']['Resend'] = "重發";
$Lang['AppNotifyMessage']['Warning']['FillInTitle'] = "請輸入標題。";
$Lang['AppNotifyMessage']['Warning']['FillInContent'] = "請輸入內容。";
$Lang['AppNotifyMessage']['SyncWithIssueDate'] ='與發出時間同步';
$Lang['AppNotifyMessage']['SyncWithIssueDateOnly'] ='與發出日期同步';
$Lang['AppNotifyMessage']['searchAndInsertStudent'] ='搜尋及新增同學';
$Lang['AppNotifyMessage']['searchAndInsertStudentAndStaff'] ='搜尋及新增同學和職員';
$Lang['AppNotifyMessage']['searchAndInsertPIC'] ='搜尋及新增負責人';
$Lang['AppNotifyMessage']['msgSearchAndInsertInfo'] ='輸入一行一個搜尋記錄  <br>"[班名] [學號] 或  [內聯網帳號]"<br><br>e.g. 搜尋 1A班15號<br>及1B班16號二位同學，請輸入<br>1A15<br>1B16';
$Lang['AppNotifyMessage']['msgSearchAndInsertPICInfo'] ='輸入一行一個搜尋記錄 "[內聯網帳號]"<br>e.g. 搜尋 tchr01 和 tchr02 二位負責人<br>請輸入<br>tchr01<br>tchr02<br><br>[只可選取教職員]';
$Lang['AppNotifyMessage']['searchAndInsertStaff'] ='搜尋及新增職員';
$Lang['AppNotifyMessage']['WarningRequestInputSearchLoginID'] = "請輸入用戶內聯網帳號以作搜尋。";
$Lang['AppNotifyMessage']['WarningFollowingUsersCannotBeFound'] = "找不到以下的用戶： ";
$Lang['AppNotifyMessage']['WarningFollowingUsersCannotBeFoundOrAdded'] = "找不到或已新增以下的用戶： ";

$Lang['AppNotifyMessage']['MessageToStaff'] = "發出職員通知 (手機)";
$Lang['AppNotifyMessage']['button'] = "發出家長通知 (手機)";
$Lang['AppNotifyMessage']['MessageToNotSignedParents'] = "追收未簽家長 (手機)";
$Lang['AppNotifyMessage']['send_failed'] = "家長通知發出失敗！";
$Lang['AppNotifyMessage']['send_result'] = "完成!<br />已發出了 [SentTotal] 則推送訊息，並於通訊中心內記錄相關紀錄。";
$Lang['AppNotifyMessage']['MessagingMethod'] = "選擇發出家長通知方法";
$Lang['AppNotifyMessage']['MessagingMethodToStaff'] = "選擇發出職員通知方法";
$Lang['AppNotifyMessage']['PushMessage'] = "eClass App 推送訊息";
$Lang['AppNotifyMessage']['SMS'] = "手機短訊";
$Lang['AppNotifyMessage']['PushMessageThenSMS'] = "eClass App 推送訊息(優先) 或 手機短訊";
$Lang['AppNotifyMessage']['SMS_send_failed'] = "家長短訊通知發出失敗！";
$Lang['AppNotifyMessage']['SMS_send_result'] = "完成!<br />已發出了 [smsTotal] 則家長通知短訊，並於通訊中心內記錄相關紀錄。";
$Lang['AppNotifyMessage']['PushMessageThenSMS_send_failed'] = "家長通知發出失敗！";
$Lang['AppNotifyMessage']['PushMessageThenSMS_send_result'] = "完成!<br />已發出了 [SentTotal] 則推送訊息和 [smsTotal] 則家長通知短訊，並於通訊中心內記錄相關紀錄。";
$Lang['AppNotifyMessage']['PushMessageThenSMS_msg_count'] = "完成!<br />將會發出 [SentTotal] 則推送訊息和 [smsTotal] 則家長通知短訊，並於通訊中心內記錄相關紀錄。";


$Lang['AppNotifyMessage']['eNotice']['Alert']['SMS'] = "請儘快簽署電子通告[NoticeNumber]「[NoticeTitle]」(簽署限期: [NoticeDeadline])。";
$Lang['AppNotifyMessage']['eNotice']['Title']['SMS'] = "電子通告重要提示 [[NoticeNumber]]";
$Lang['AppNotifyMessage']['eNotice']['Alert']['App'] = "請儘快簽署電子通告[NoticeNumber]「[NoticeTitle]」(簽署限期: [NoticeDeadline])。
Please sign the eNotice [NoticeNumber] titled \"[NoticeTitle]\" as soon as possible (Original Deadline: [NoticeDeadline]).";
$Lang['AppNotifyMessage']['eNotice']['Title']['App'] = "電子通告重要提示 [[NoticeNumber]]
eNotice Important Reminder [[NoticeNumber]]";
$Lang['AppNotifyMessage']['eNotice']['button'] = "發出提醒 (手機)";
$Lang['AppNotifyMessage']['eNotice']['send_failed'] = "家長提醒發出失敗！";
$Lang['AppNotifyMessage']['eNotice']['send_result'] = "完成!<br />已發出了 [SentTotal] 則提醒訊息。";
$Lang['AppNotifyMessage']['eNotice']['send_msg_count'] = "完成!<br />將會發出 [SentTotal] 則提醒訊息。";
$Lang['AppNotifyMessage']['eNotice']['send_result_no_parent'] = "請先建立相關家長帳戶。";
$Lang['AppNotifyMessage']['eNotice']['send_failed_staff'] = "教職員提醒發出失敗！";
$Lang['AppNotifyMessage']['eNotice']['send_result_staff'] = "完成!<br />已發出了 [SentTotal] 則提醒訊息。";
$Lang['AppNotifyMessage']['eNotice']['send_result_no_staff'] = "請先建立相關教職員帳戶。";
$Lang['AppNotifyMessage']['eNotice']['Subject'] = "最新電子通告提示 [__NOTICENUMBER__]
Latest eNotice alert [__NOTICENUMBER__]";
//$Lang['AppNotifyMessage']['eNotice']['Content'] = "一則新的電子通告 (__TITLE__) __TERM__於__STARTDATE__發出。請於__ENDDATE__或之前簽妥。";
$Lang['AppNotifyMessage']['eNotice']['Content'] = "請於__ENDDATE__或之前簽署電子通告__NOTICENUMBER__「__TITLE__」。
Please sign the eNotice __NOTICENUMBER__ titled \"__TITLE__\" on or before __ENDDATE__.";

$Lang['AppNotifyMessage']['eCircular']['Subject'] = "最新職員通告提示 [__NOTICENUMBER__]
Latest eCircular alert [__NOTICENUMBER__]";
$Lang['AppNotifyMessage']['eCircular']['Alert']['App'] = "請儘快簽署職員通告[NoticeNumber]「[NoticeTitle]」(簽署限期: [NoticeDeadline])。
Please sign the eCircular [NoticeNumber] titled \"[NoticeTitle]\" as soon as possible (Original Deadline: [NoticeDeadline]).";
$Lang['AppNotifyMessage']['eCircular']['Title']['App'] = "職員通告重要提示 [[NoticeNumber]]
eCircular Important Reminder [[NoticeNumber]]";
//$Lang['AppNotifyMessage']['eCircular']['Content'] =  "一則新的職員通告 (__TITLE__) __TERM__於__STARTDATE__發出。請於__ENDDATE__或之前簽妥。";
$Lang['AppNotifyMessage']['eCircular']['Content'] = "請於__ENDDATE__或之前簽署職員通告__NOTICENUMBER__「__TITLE__」。
Please sign the eCircular __NOTICENUMBER__ titled \"__TITLE__\" on or before __ENDDATE__.";
$Lang['AppNotifyMessage']['eAttendance']['Absent']['Title'] = "缺席提示
Absent Alert";
$Lang['AppNotifyMessage']['eAttendance']['Absent']['Content'] = "貴子弟[StudentName]於[DateInvolved]缺席。
Please be informed that your child [StudentName] was absent from school on [DateInvolved].";
$Lang['AppNotifyMessage']['eAttendance']['Late']['Title'] = "遲到提示
Late Alert";
$Lang['AppNotifyMessage']['eAttendance']['Late']['Content'] = "貴子弟[StudentName]於[DateInvolved]遲到。
Please be informed that your child [StudentName] was late to school on [DateInvolved].";
$Lang['AppNotifyMessage']['eAttendance']['EarlyLeave']['Title'] = "早退提示
Early Leave Alert";
$Lang['AppNotifyMessage']['eAttendance']['EarlyLeave']['Content'] = "貴子弟[StudentName]於[DateInvolved]早退。
Please be informed that your child [StudentName] left school early on [DateInvolved].";
$Lang['AppNotifyMessage']['RepairSystem']['Subject']['Process'] = "報修系統的請求在處理中 [__CASENUMBER__]
Request of Repair System is being process [__CASENUMBER__]";
$Lang['AppNotifyMessage']['RepairSystem']['Subject']['Completed'] = "報修系統的請求已完成 [__CASENUMBER__]
Request of Repair System has been completed [__CASENUMBER__]";
$Lang['AppNotifyMessage']['RepairSystem']['Subject']['Rejected'] = "報修系統的請求已遭拒絕 [__CASENUMBER__]
Request of Repair System has been rejected [__CASENUMBER__]";
$Lang['AppNotifyMessage']['RepairSystem']['Subject']['Pending'] = "報修系統的請求正等待處理 [__CASENUMBER__]
Request of Repair System is pending [__CASENUMBER__]";
$Lang['AppNotifyMessage']['RepairSystem']['Content'] = "請求日期 : __DATEINPUT__
類別 : __CATEGORY__
地點 : __LOCATION_B5__
請求摘要 : __SUMMARY__
請求詳情 : __DETAILS__
管理組別 : __MGMTGROUP__
備註 : __REMARK__
狀況 : __STATUS_B5__

Request Date : __DATEINPUT__
Category : __CATEGORY__
Location : __LOCATION_EN__
Request Summary : __SUMMARY__
Request Details : __DETAILS__
Management Group : __MGMTGROUP__
Remarks : __REMARK__
Status : __STATUS_EN__";


## Warnings
$Lang['AppNotifyMessage']['WarningMsg']['RequestInputTitle'] = "請輸入標題。";
$Lang['AppNotifyMessage']['WarningMsg']['RequestInputDescription'] = "請輸入內容。";
$Lang['AppNotifyMessage']['WarningMsg']['RequestSelectRecipients'] = "請輸入至少一個接收人。";
$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid'] = "傳送時間已過";
$Lang['AppNotifyMessage']['WarningMsg']['SendTimeMustAfterStartDate'] = "傳送時間必須等於或遲於發出日期";
$Lang['AppNotifyMessage']['WarningMsg']['ResendPushMessage'] = "你是否確定要重發訊息給已選擇的用戶？";
$Lang['AppNotifyMessage']['WarningMsg']['ProcessingMessageAlready'] = "訊息正在傳送";
$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange'] = "請選擇 30 天內的日期。";
$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange90'] = "請選擇 90 天內的日期。";

## Return messages
$Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess'] = "1|=|已成功通知發送";
$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'] = "0|=|通知傳送失敗";
$Lang['AppNotifyMessage']['ReturnMsg']['RemoveSuccess'] = "1|=|已刪除紀錄";
$Lang['AppNotifyMessage']['ReturnMsg']['RemoveFail'] = "0|=|紀錄刪除失敗";

//$Lang['AppNotifyMessage']['Homework']['Alert'] = "貴子弟[StudentName]於[DateInvolved]欠交[Subjects]科功課，敬請留意。";
$Lang['AppNotifyMessage']['Homework']['Alert']['sms'] = "貴子弟 [StudentName]於[DateInvolved]未能準時呈交[Subjects]功課。";
$Lang['AppNotifyMessage']['Homework']['Alert']['app']['period'] = "貴子弟 [StudentName]於[StartDate]至[EndDate]期間未能準時呈交[Subjects]功課。
Please be informed that your child  [StudentName] has not submitted [Subjects] homework on period from [StartDate] to [EndDate].";
$Lang['AppNotifyMessage']['Homework']['Alert']['app']['single_day'] = "貴子弟 [StudentName]於[StartDate]未能準時呈交[Subjects]功課。
Please be informed that your child  [StudentName] has not submitted [Subjects] homework on [StartDate].";
$Lang['AppNotifyMessage']['Homework']['Alert']['student_app']['period'] = "閣下於[StartDate]至[EndDate]期間未能準時呈交[Subjects]功課。
Please be informed that you have not submitted [Subjects] homework on period from [StartDate] to [EndDate].";
$Lang['AppNotifyMessage']['Homework']['Alert']['student_app']['single_day'] = "閣下於[StartDate]未能準時呈交[Subjects]功課。
Please be informed that you have not submitted [Subjects] homework on [StartDate].";
$Lang['AppNotifyMessage']['Homework']['Period'] = "期間";
$Lang['AppNotifyMessage']['Homework']['In'] = "";
$Lang['AppNotifyMessage']['Homework']['On'] = "";
$Lang['AppNotifyMessage']['Homework']['From'] = "";
$Lang['AppNotifyMessage']['Homework']['To'] = "至";
$Lang['AppNotifyMessage']['Homework']['Title'] = "逾期未交功課提示
Homework not Submitted Reminder";
$Lang['AppNotifyMessage']['Homework']['button'] = "發出家長通知 (手機)";
$Lang['AppNotifyMessage']['Homework']['studentButton'] = "發出學生通知 (手機)";
$Lang['AppNotifyMessage']['Homework']['send_failed'] = "家長通知發出失敗！";
$Lang['AppNotifyMessage']['Homework']['send_result'] = "完成!<br />已發出了 [SentTotal] 則推送訊息，並於通訊中心內記錄相關紀錄。";
$Lang['AppNotifyMessage']['Homework']['send_result_student'] = "完成!<br />已發出了 [SentTotal] 則推送訊息，並於通訊中心內記錄相關紀錄。";
$Lang['AppNotifyMessage']['Homework']['MessagingMethod'] = "選擇發出家長通知方法";
$Lang['AppNotifyMessage']['Homework']['PushMessage'] = "eClass App 推送訊息";
$Lang['AppNotifyMessage']['Homework']['SMS'] = "手機短訊";
$Lang['AppNotifyMessage']['Homework']['PushMessageThenSMS'] = "eClass App 推送訊息(優先) 或 手機短訊";
$Lang['AppNotifyMessage']['Homework']['SMS_send_failed'] = "家長短訊通知發出失敗！";
$Lang['AppNotifyMessage']['Homework']['SMS_send_result'] = "完成!<br />已發出了 [smsTotal] 則家長通知短訊，並於通訊中心內記錄相關紀錄。";
$Lang['AppNotifyMessage']['Homework']['PushMessageThenSMS_send_failed'] = "家長通知發出失敗！";
$Lang['AppNotifyMessage']['Homework']['PushMessageThenSMS_send_result'] = "完成!<br />已發出了 [SentTotal] 則推送訊息和 [smsTotal] 則家長通知短訊，並於通訊中心內記錄相關紀錄。";

$Lang['AppNotifyMessage']['ePayment']['IncludeAppParent'] = "包括使用eClass App的家長";
$Lang['AppNotifyMessage']['ePayment']['ParentNotUsingEClassApp'] = "家長未使用eClass App";

$Lang['AppNotifyMessage']['SchoolNews']['Subject'] = "新__TYPE__宣佈 (__DATE__)";
$Lang['AppNotifyMessage']['SchoolNews']['Title']['SCHOOL'] = "校園最新消息通知
Latest School News Reminder";
$Lang['AppNotifyMessage']['SchoolNews']['Title']['GROUP'] = "小組最新消息通知
Latest Group News Reminder";
$Lang['AppNotifyMessage']['SchoolNews']['Content']['SCHOOL'] = "請留意於__DATE__發放的「__TITLE__」最新消息。
Please note that the latest news of \"__TITLE__\" was released on __DATE__.";
$Lang['AppNotifyMessage']['SchoolNews']['Content']['GROUP'] = "請留意於__DATE__發放的「__TITLE__」小組最新消息。
Please note that the latest group news of \"__TITLE__\" was released on __DATE__.";
$Lang['AppNotifyMessage']['ChangePassword']['Title'] = "eClass 密碼更改";
$Lang['AppNotifyMessage']['ChangePassword']['Content'] = "閣下已於 <!--DateTime--> 更改 eClass 密碼。";
$Lang['AppNotifyMessage']['SendInBatchesRemarks'] = "如發放推送訊息的對象超過200位，系統將自動分批送出該訊息。";
$Lang['AppNotifyMessage']['AppLastLoginTime'] = "App 最後登入時間";
$Lang['AppNotifyMessage']['eEnrolment']['Title'] = "課外活動出席狀態通知
eEnrolment Attendance Status Alert";
$Lang['AppNotifyMessage']['eEnrolment']['Absent']['Content'] = "貴子弟[StudentName]於[DateInvolved]缺席[eEnrolmentTitle]。
Please be informed that your child [StudentName] was absent from [eEnrolmentTitle] on [DateInvolved].";
$Lang['AppNotifyMessage']['eEnrolment']['Late']['Content'] = "貴子弟[StudentName]於[DateInvolved]遲到于[eEnrolmentTitle]。
Please be informed that your child [StudentName] was late to [eEnrolmentTitle] on [DateInvolved].";
$Lang['AppNotifyMessage']['eEnrolment']['present']['Content'] = "貴子弟[StudentName]於[DateInvolved]出席[eEnrolmentTitle]。
Please be informed that your child [StudentName] was present at [eEnrolmentTitle] on [DateInvolved].";
$Lang['AppNotifyMessage']['eEnrolment']['exempt']['Content'] = "貴子弟[StudentName]於[DateInvolved]豁免于[eEnrolmentTitle]。
Please be informed that your child [StudentName] was exempt from [eEnrolmentTitle] on [DateInvolved].";

$Lang['MessageCenter']['ToTeacher'] = "傳送至教職員";
$Lang['MessageCenter']['ToParent'] = "傳送至家長";
$Lang['MessageCenter']['ToStudent'] = "傳送至學生";
$Lang['MessageCenter']['ExportMobile'] ='匯出電話號碼#';
$Lang['MessageCenter']['ExportMobileRemarks'] = '#匯出沒有使用 App 的家長電話號碼：系統會先擷取主監護人的緊急聯絡號碼，如找不到就會擷取家長戶口的手提電話號碼。';
$Lang['MessageCenter']['ExportMobileRemarks_T'] = '#匯出沒有使用 App 的教職員電話號碼';
$Lang['MessageCenter']['UserSelected'] = ' 用戶已被選擇';
$Lang['MessageCenter']['SendSingleEmail']='傳送單一電郵';
$Lang['MessageCenter']['ResendUnsentEmail'][0]='由第';
$Lang['MessageCenter']['ResendUnsentEmail'][1]='欄起重發';
$Lang['MessageCenter']['ImageAttachementReminder'] = "圖像只能以'.JPG'、'.GIF' 或'.PNG'的格式上載";
$Lang['SMS']['SendTo'] = "發短訊";
$Lang['SMS']['SendToSelect'] = "-- 請選擇 --";
$Lang['SMS']['SendToIntranetUser'] = "至 eClass 用戶";
$Lang['SMS']['SendToGuardian'] = "至學生監護人";
$Lang['SMS']['SendToMobileNumber'] = "至輸入的手機號碼";
$Lang['SMS']['SendByCSV'] = "使用 CSV 匯入 (支援不同短訊內容)";
$Lang['SMS']['MessageMgmt'] = "短訊";
$Lang['SMS']['UsageReport'] = "使用報告";
$Lang['SMS']['MessageTemplates'] = "訊息範本";
$Lang['SMS']['MessageNo'] = "短訊數目（條）";
$Lang['SMS']['MessageTotal'] = "短訊總數（計算收費）";
$Lang['SMS']['SendingSms'] = "正在傳送短訊...";
$Lang['AppNotifyMessage']['SMS_notificaction']['Remark'] ="* 此特殊情況範本可於".$Lang['Header']['Menu']['MessageCenter'].$Lang['Header']['Menu']['SMS']."之".$Lang['SMS']['MessageTemplates']."內修改。";
$Lang['MessageCenter']['ContentMaximumRemarks'] = "最多500個字元";
$Lang['MessageCenter']['ContentMaximumRemarks150'] = "最多150個字元";


$Lang['Warning']['UnicodeText'] = "上載的檔案格式不正確！請先儲存為 Unicode 文字檔，然後再上載。";
$Lang['Warning']['EmptyToFieldWarning'] = "請輸入收件人電郵地址";
$Lang['Warning']['EmailAttachmentWarning'] = "你現在未加入任何附件 \\n繼續發送電郵?";
$Lang['Warning']['ToMailFormatWarning'] = '請檢查 "收件人" 的電郵格式';
$Lang['Warning']['CcMailFormatWarning'] = '請檢查 "副本" 的電郵格式';
$Lang['Warning']['BccMailFormatWarning'] = '請檢查 "密件副本" 的電郵格式';
$Lang['email']['WarningLimitAttachmentSize'] = "附件大小超過 <!--MAX_SIZE-->MB, 請將附件移除。";
$Lang['Warning']['MailFormatWarning'] = "收件人/副本/密件副本 的電郵格式錯誤\\n";
$Lang['Gamma']['Warning']['MaxAttachmentSizeExceededAndRemoved'] = "附件大小超過 <!--MAX_SIZE-->MB，已將附件移除。";
$Lang['Gamma']['Warning']['IllegalFileTypeOrFileTypeForbidden'] = "附件類型不合法或系統已禁止上傳該類文件。";
$Lang['Gamma']['Warning']['AttachmentUploadInProgress'] = "上載附件中，請耐心等候上載完成。";

$Lang['Gamma']['AdvancedSearch'] = "進階搜尋";
$Lang['Gamma']['Sent'] = "寄件箱";
$Lang['Gamma']['Junk'] = "雜件箱";

$Lang['Gamma']['UnseenStatus']['ALL'] = "所有郵件";
$Lang['Gamma']['UnseenStatus']['UNSEEN'] = "未閱郵件";
$Lang['Gamma']['UnseenStatus']['SEEN'] = "已閱郵件";
$Lang['Gamma']['UnseenStatus']['FLAGGED'] = "已標記星號";

$Lang['Gamma']['reportSpam'] = "報告垃圾郵件";
$Lang['Gamma']['reportNonSpam'] = "報告非垃圾郵件";

$Lang['Gamma']['ConfirmMsg']['reportSpam'] = "將已選取的郵件報告為垃圾郵件並移至雜件箱?";
$Lang['Gamma']['ConfirmMsg']['reportNonSpam'] = "將已選取的郵件報告為非垃圾郵件並移至收件箱?";
$Lang['Gamma']['ConfirmMsg']['ReportSpamToFolder'] = "將已選取的郵件報告為垃圾郵件並移至郵件夾<!--FOLDER-->?";
$Lang['Gamma']['ConfirmMsg']['MarkAsSeen'] = "將已選取的郵件設定為已閱郵件?";
$Lang['Gamma']['ConfirmMsg']['MarkAsUnseen'] = "將已選取的郵件設定為未閱郵件?";
$Lang['Gamma']['ConfirmMsg']['MarkAsStarred'] = "將已選取的郵件標記為星號?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['reportSpam'] = "將郵件報告為垃圾郵件並移至雜件箱?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['reportNonSpam'] = "將郵件報告為非垃圾郵件並移至收件箱?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['reportSpamToFolder'] = "將郵件報告為垃圾郵件並移至郵件夾<!--FOLDER-->?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['removeMail'] = "將郵件移至垃圾箱?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['moveMail'] = "你確定要移動郵件?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['emptyTrash'] = "你確定要清空垃圾箱?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['emptyJunk'] = "你確定要清空雜件箱?";

$Lang['Gamma']['FillForwardingEmail'] = " 請輸入要轉寄到的電郵地址";

$Lang['Gamma']['Remove'] = "刪除";
$Lang['Gamma']['Forward']= "轉寄";
$Lang['Gamma']['Reply']= "回覆";
$Lang['Gamma']['ReplyAll']= "回覆全部";

$Lang['Gamma']['MarkAs'] = "標記為";

$Lang['ReturnMsg']['MailSavedSuccess'] = "1|=|郵件儲存成功。";
$Lang['ReturnMsg']['MailSavedUnsuccess'] = "0|=|郵件儲存失敗。";

$Lang['Gamma']['SystemFolderName']["INBOX"] = "收件箱";
$Lang['Gamma']['SystemFolderName']["Sent"] = "寄件箱";
$Lang['Gamma']['SystemFolderName']["Drafts"] = "草稿箱";
$Lang['Gamma']['SystemFolderName']["Junk"] = "雜件箱";
$Lang['Gamma']['SystemFolderName']["Trash"] = "垃圾箱";

$Lang['Gamma']['AutoSaveInterval'] = "自動儲存草稿時間";
$Lang['Gamma']['Min'] = "分鐘";
$Lang['Gamma']['ZeroToDisable'] = "(輸入 0 關閉自動儲存)";
$Lang['Gamma']['ReturnMsg']['UserPref']['Save']['Success'] = "1|=|用戶設定成功";
$Lang['Gamma']['ReturnMsg']['UserPref']['Save']['Failed'] = "0|=|用戶設定失敗";
$Lang['Gamma']['SelectFromInternalRecipient'] = "從內聯網收件人選取";
$Lang['Gamma']['SelectFromExternalRecipient'] = "從外在收件人選取";
$Lang['Gamma']['SelectFromExternalRecipientGroup'] = "從外在收件組別選取";
$Lang['Gamma']['SelectFromInternalRecipientGroup'] = "從內聯網收件組別選取";

$Lang['Gamma']['FolderNameContainDots'] = "郵件夾名稱不能含有\".\" 或 \"/\" 或 \"+\"";
$Lang['Gamma']['ViewAllImage'] = "檢視所有圖片";
$Lang['Gamma']['DownloadAllAttachment'] = "下載所有附件";

$Lang['Gamma']['Quota']= "配額";
$Lang['Gamma']['EmailStatus']= "電郵狀況";

$Lang['Gamma']['DisplayRelatedMail']= "顯示相關郵件";

$Lang['Gamma']['InternalGroupLabel'] = "(內聯網組別)";
$Lang['Gamma']['ExternalGroupLabel'] = "(外在組別)";
$Lang['Gamma']['DisplayName'] = "顯示名稱";
$Lang['Gamma']['ReplyEmail'] = "回覆電郵地址";

$Lang['Gamma']['iMailArchive'] = "iMail 封存";
$Lang['Gamma']['Recipient'] = "收件者";
$Lang['Gamma']['EmptyTrash'] = "清空垃圾箱";
$Lang['Gamma']['EmptyJunk'] = "清空雜件箱";
$Lang['Gamma']['ReturnMsg']['EmptyTrashSuccess'] = "成功清空垃圾箱。";
$Lang['Gamma']['ReturnMsg']['EmptyTrashUnsuccess'] = "清空垃圾箱失敗。";
$Lang['Gamma']['ReturnMsg']['EmptyJunkSuccess'] = "成功清空雜件箱。";
$Lang['Gamma']['ReturnMsg']['EmptyJunkUnsuccess'] = "清空雜件箱失敗。";
$Lang['Gamma']['ExpandAllMails'] = "顯示所有郵件";
$Lang['Gamma']['CollapseAllMails'] = "隱藏所有郵件";
$Lang['Gamma']['InternalRecipient'] = "內聯網收件人";
$Lang['Gamma']['RecipientGroup'] = "收件組別";
$Lang['Gamma']['AddImage'] = "加入圖片";
$Lang['Gamma']['InsertImageToContent'] = "於內容加入圖片";
$Lang['Gamma']['Uploading'] = "上載中";
$Lang['Gamma']['UploadFail'] = "上載失敗";
$Lang['Gamma']['AccountInformation'] = "帳戶資料";
$Lang['Gamma']['IMAPMailServer'] = "IMAP 電郵伺服器 ";
$Lang['Gamma']['ConnectionMethod'] = "連線方法";
$Lang['Gamma']['Username'] = "用戶名稱";
$Lang['Gamma']['Password'] = "密碼";
$Lang['Gamma']['YourPasswordINeClass'] = "你的eClass密碼";
$Lang['Gamma']['ReturnMsg']['FailToSaveSentCopy'] = "郵件經已發送，但因郵箱空間不足未能儲存於寄件箱。";

// admin console
$Lang['Gamma']['DisallowSendReceive'] = "這些用戶不能發送或接收互聯網郵件.";
$Lang['Gamma']['DayInTrash'] = "垃圾箱郵件保留";
$Lang['Gamma']['DayInSpam'] = "雜件箱郵件保留";
$Lang['Gamma']['Days'] = "天";
$Lang['Gamma']['MailBoxQuota'] = "郵箱儲存量設定";
$Lang['Gamma']['BlockUserToSend'] = "禁發郵件用戶";
$Lang['Gamma']['GeneralSetting'] = "預設使用權限及郵箱儲存量";
$Lang['Gamma']['Warning']['PleaseSelectUser'] = "請選擇使用者";
$Lang['Gamma']['Warning']['PleaseInputQuota'] = "請輸入儲存量調整數";
$Lang['Gamma']['Warning']['WrongQuota'] = "儲存量調整數不正確";
$Lang['Gamma']['InternalMailOnly'] = "只限內聯網電郵";
$Lang['Gamma']['InternalAndInternet'] = "互聯網及內聯網電郵";
$Lang['Gamma']['WebmailStorage'] = "電郵設定";
$Lang['Gamma']['AddUsers'] = "加入用戶";
$Lang['Gamma']['BanInstruction'] = "請輸入限制用戶的登入名稱. <br>每一行輸入一個登入名稱.<br>這些用戶不能發送郵件, 但能接收郵件.<br>";
$Lang['Gamma']['UserUsageRightsAndQuota'] = "個別使用權限及郵箱儲存量";

$Lang['Gamma']['BatchRemoval'] = "批次移除";
$Lang['Gamma']['RemovalSettings'] = "移除設定";
$Lang['Gamma']['FilteredRemovedMails'] = "已隱藏/已刪除電郵";
$Lang['Gamma']['Criteria'] = "條件";
$Lang['Gamma']['Settings'] = "設定";
$Lang['Gamma']['DateRange'] = "日期範圍";
$Lang['Gamma']['SubjectKeywordKeyPhrase'] = "主旨關鍵字 / 關鍵句";
$Lang['Gamma']['SendersMailAddress'] = "寄件者電郵地址";
$Lang['Gamma']['FilteredMessages'] = "已隱藏郵件";
$Lang['Gamma']['DeletedMessages'] = "已刪除郵件";

$Lang['Gamma']['Warning']['LeaveAllFieldsBlankToDisable'] = "留空所有項目，此功能將不會生效。";
$Lang['Gamma']['Warning']['PleaseInputValidStartDate'] = "請輸入正確的開始日期。";
$Lang['Gamma']['Warning']['PleaseInputValidEndDate'] = "請輸入正確的結束日期。";
$Lang['Gamma']['Warning']['PleaseInputValidDateRange'] = "請輸入正確的日期範圍。";
$Lang['Gamma']['Warning']['PleaseInputSubjectKeyword'] = "請輸入主旨關鍵字。";
$Lang['Gamma']['Warning']['PleaseInputValidEmailAddress'] = "請輸入正確的電郵地址。";
$Lang['Gamma']['ConfirmMsg']['UndoMessages'] = "你確定要還原所選的郵件?";

$Lang['Gamma']['NotAllowListedUsersSendReceiveInternetMails'] = "不允許接收/傳送外來互聯網郵件";
$Lang['Gamma']['ActivateListedUsersMailAccount'] = "啟用所列出用戶的郵件戶口";
$Lang['Gamma']['SuspendListedUsersMailAccount'] = "暫停所列出用戶的郵件戶口";

$Lang['Gamma']['Action'] = "動作";
$Lang['Gamma']['CleanMailbox'] = "清空所有信箱";
$Lang['Gamma']['ForceRemovalRemarks'] = "備註: 使用日期範圍清理信件可能需時很長。";
$Lang['Gamma']['Done'] = "完成";
$Lang['Gamma']['NumberOfMails'] = "電郵數目";

$Lang['Gamma']['RuleSettings'] = "規則設定";
$Lang['Gamma']['Rule'] = "規則";
$Lang['Gamma']['Score'] = "分數";
$Lang['Gamma']['Warning']['PleaseInputRule'] = "請輸入規則。";
$Lang['Gamma']['Warning']['PleaseInvalidScore'] = "請輸入正確的分數。";
$Lang['Gamma']['AVASScoreDescription'] = "* 數值越少代表垃圾郵件控制程度越高，反之亦然。範圍： -20.0 - +20.0";
$Lang['Gamma']['RuleRemark'] = "^ 規則不允許輸入任何特殊字符，所有輸入的特殊字符都會被自動移除。";

$Lang['Gamma']['QuotaAlertSettings'] = "郵箱儲存量使用警告設定";
$Lang['Gamma']['AlertWhenQuotaOver'] = "當使用量超過此百分比時發出警告";
$Lang['Gamma']['Warning']['PleaseInputNumber0to100'] = "請輸入一個0至100的數字。";
$Lang['Gamma']['Warning']['AlertQuotaUsageStatus'] = '你已經使用了<!--USED_PERCENT-->% 的郵箱儲存量 (<!-USED_QUOTA-> / <!--TOTAL_QUOTA--> MB)‧ \n請清理郵箱。';

$Lang['Gamma']['RealTimeSearch'] = "實時搜索";
$Lang['Gamma']['ConfirmMg']['ConfirmDoRealTimeSearch'] = "請注意實時搜索會需要很長的時間來完成，是否要繼續?";
// end admin console

$Lang['Gamma']['UserEmail'] = "備用電郵";
$Lang['Gamma']['Warning']['SendMessageWithoutSubject'] = "要傳送沒有主旨的郵件嗎?";
$Lang['Gamma']['GroupName'] = "組別名稱";
$Lang['Gamma']['GroupRemark'] = "組別備註";

$Lang['Gamma']['UserGmail'] = "啟用 G Suite 帳戶";
$Lang['Gamma']['UserGmailAccountExists'] = " G Suite 帳戶已經存在, 沒有另外新增 G Suite 帳戶";

$Lang['Gamma']['WarningHouseKeepingJunk'] = "在 [雜件箱] 中的郵件 <!--DaysNum--> 天後會自動刪除。";
$Lang['Gamma']['WarningHouseKeepingTrash'] = "在 [垃圾箱] 中的郵件 <!--DaysNum--> 天後會自動刪除。";

$Lang['Gamma']['ConfirmMsg']['ConfirmSplitMailListAndSendInBatch'] = '由於收件者數量已超出上限<!--UpperLimit-->，系統會以密件副本的形式傳送出去，是否繼續？';
$Lang['Gamma']['ConfirmMsg']['ConfirmRemoveFolder'] = "此資料夾內的所有郵件會被移至".$Lang['Gamma']['SystemFolderName']["Trash"]."作為備份。你確定要刪除此資料夾?";
$Lang['Gamma']['ConfirmMsg']['ConfirmSaveDraftBeforeClosingWindow'] = "你要在關閉視窗前保存為草稿嗎?";

$Lang['Gamma']['RemindMsg']['MaxAttachmentSize'] = "最大附件大小為<!--Size-->";
$Lang['Gamma']['Warning']['SendWithoutAttachment'] = "這封電郵未有加入附件。你可以按「取消」再加入附件，或按「繼續」馬上發送。";

$Lang['Gamma']['CommonMsg']['CurrentPageSelectedMails'] = '已選取此頁面上的所有 <!--NUMBER--> 封郵件。';
$Lang['Gamma']['CommonMsg']['SelectAllMailsInFolder'] = '選取 <!--FOLDER--> 中的所有 <!--NUMBER--> 封郵件。';
$Lang['Gamma']['CommonMsg']['SelectedAllMailsInFolder'] = '已選取 <!--FOLDER--> 中的所有 <!--NUMBER--> 封郵件。';
$Lang['Gamma']['CommonMsg']['ClearSelection'] = '清除選取';
$Lang['Gamma']['CommonMsg']['SelectMatchedMails'] = '選取所有符合此搜尋的郵件。';
$Lang['Gamma']['CommonMsg']['SelectedAllMatchedMails'] = '已選取所有符合此搜尋的所有 <!--NUMBER--> 封郵件。';

$Lang['Gamma']['Warning']['CouldNotConnectMailServer'] = '由於你的eClass帳戶密碼與電郵帳戶密碼不一致，未能連接上郵件伺服器。\n請於 '.$Lang['Header']['Menu']['iAccount'].' 更改你的密碼。';
$Lang['Gamma']['AutoReplyInstruction'] = "請輸入自動回覆之訊息";
$Lang['Gamma']['Unlimited'] = "無限制";
$Lang['Gamma']['PreviousMail'] = "較舊郵件";
$Lang['Gamma']['NextMail'] = "較新郵件";
$Lang['Gamma']['ViewMessageSource'] = "檢視郵件源碼";

$Lang['Gamma']['CreateRoutingDocument'] = "建立傳遞文件";
$Lang['Gamma']['ShowUploadAttachmentWidget'] = "顯示上載附件工具";
$Lang['Gamma']['HideUploadAttachmentWidget'] = "隱藏上載附件工具";
$Lang['Gamma']['Email'] = "電子郵件";
$Lang['Gamma']['EmailAccount'] = "電子郵件帳號";

$i_CampusMail_New_RemoveBySearch = "以電郵標題刪除郵件";
$i_campusmail_forceRemoval = "以用戶類型及日期範圍刪除郵件";
$Lang['Gamma']['BatchRemoval'] = "以電郵標題及日期範圍刪除郵件";

$Lang['Gamma']['OneWeek'] = "一星期";
$Lang['Gamma']['OneMonth'] = "一個月";
$Lang['Gamma']['ThreeMonths'] = "三個月";
$Lang['Gamma']['SixMonths'] = "六個月";
$Lang['Gamma']['OneYear'] = "一年";
$Lang['Gamma']['CustomDateRange'] = "自訂日期範圍";
$Lang['Gamma']['WarningRequestSelectFolder'] = "請選擇至少一個郵件夾。";
$Lang['Gamma']['ViewRelatedEmails'] = "查看相關的郵件";
$Lang['Gamma']['ExternalContacts'] = "外部聯絡人";
$Lang['Gamma']['InternalContacts'] = "內部聯絡人";
$Lang['Gamma']['ExternalRecipientGroup'] = "外在收件組別";
$Lang['Gamma']['ExternalRecipient'] = "外在收件人";
$Lang['Gamma']['InternalRecipientGroup'] = "內在收件組別";
$Lang['Gamma']['InternalRecipient'] = "內在收件人";
$Lang['Gamma']['RecipientType'] = "收件人類別";

## Account Management > Shared MailBox
$Lang['SharedMailBox']['SharedMailBox'] = "共享郵箱";
$Lang['SharedMailBox']['EmailAddress'] = "電郵地址";
$Lang['SharedMailBox']['NoOfSharers'] = "分享者數目";
$Lang['SharedMailBox']['SharedBy'] = "分享者";
$Lang['SharedMailBox']['NewSharedMailBox'] = "新增共享郵箱";
$Lang['SharedMailBox']['EditSharedMailBox'] = "編輯共享郵箱";
$Lang['SharedMailBox']['MailBox'] = "郵箱";
$Lang['SharedMailBox']['Password'] = "密碼";
$Lang['SharedMailBox']['NameLimitDesc'] = "限制30字";
$Lang['SharedMailBox']['AddSharers'] = "加入分享者";
$Lang['SharedMailBox']['QuotaDescription'] = "0表示無配額限制";
$Lang['SharedMailBox']['Warning']['SelectOnlyOneMailBoxToEdit'] = "請只選擇一個共享郵箱進行編輯。";
$Lang['SharedMailBox']['Warning']['SelectAtLeastOneMailBox'] = "請選擇至少一個共享郵箱。";
$Lang['SharedMailBox']['Warning']['ConfirmDeleteMailBox'] = "你確定要刪除所選擇的共享郵箱? 請注意所有相關的郵件將會被同時刪除。";
$Lang['SharedMailBox']['Warning']['MailBoxNameCannotBlank'] = "郵箱名稱不能留空。";
$Lang['SharedMailBox']['Warning']['MailBoxExist'] = "郵箱已經存在。";
$Lang['SharedMailBox']['Warning']['InvalidMailBoxName'] = "郵箱名稱不正確。";
$Lang['SharedMailBox']['Warning']['PasswordCannotBlank'] = "密碼不能留空。";
$Lang['SharedMailBox']['Warning']['PasswordsDonotMatch'] = "密碼不匹配。";
$Lang['SharedMailBox']['Warning']['QuotaRequestPositiveInt'] = "郵箱配額請輸入正整數。";
$Lang['SharedMailBox']['ReturnMsg']['AddMailBoxSuccess'] = "1|=|成功新增共享郵箱。";
$Lang['SharedMailBox']['ReturnMsg']['AddMailBoxFail'] = "0|=|新增共享郵箱失敗。";
$Lang['SharedMailBox']['ReturnMsg']['EditMailBoxSuccess'] = "1|=|成功更新共享郵箱。";
$Lang['SharedMailBox']['ReturnMsg']['EditMailBoxFail'] = "0|=|更新共享郵箱失敗。";
$Lang['SharedMailBox']['ReturnMsg']['DeleteMailBoxSuccess'] = "1|=|成功刪除共享郵箱。";
$Lang['SharedMailBox']['ReturnMsg']['DeleteMailBoxFail'] = "0|=|刪除共享郵箱失敗。";

## eNotice / eCircular
$Lang['Notice']['AnswerAllQuestions'] = "必須回答此通告的所有問題";
$Lang['Notice']['AnswerSpecificQuestions'] = "必須回答通告內附有 * 的問題";
$Lang['Notice']['NoticeHasExpired'] = "此通告簽署限期已過，故未能繼續。";
$Lang['Notice']['CopiedSatusRemark'] = "(複製通告預設類別, 如有需要請自行更改)";
$Lang['Notice']['ExportUnsignedNoticeList'] = "匯出 (未簽通告名單)";

# Time slot setting
$Lang['eAttendance']['warn_PleaseChooseAtLeastOneItem'] = "請最少選擇一個項目";

$Lang['eAttendance']['ClassAttendanceMode'] = "班別點名模式";
$Lang['eAttendance']['ShowAllClasses'] = "所有班級";

$Lang['eAttendance']['PunishmentNullAlertMsg'] = "部份紀錄未有任何懲罰數量, 繼續?";
$Lang['eAttendance']['DisplayPastPunishmentRecord'] = "顯示過去懲罰紀錄";

# admin console
$i_general_Principal = "校長";
$i_general_VPrincipal = "副校";
$i_general_Director = "主任";

# Repair System
$Lang['Header']['Menu']['RepairSystem'] = "報修系統";
$Lang['Menu']['RepairSystem']['Management'] = "管理";
$Lang['Menu']['RepairSystem']['Settings'] = "設定";
$Lang['RepairSystem']['List'] = "清單";
$Lang['RepairSystem']['Category'] = "類別";
$Lang['RepairSystem']['Location'] = "地點";
$Lang['RepairSystem']['LocationProperties'] = "地點特性";
$Lang['RepairSystem']['MgmtGroup'] = "管理組別";
$Lang['RepairSystem']['FollowUpPerson'] = '跟進人員';
$Lang['RepairSystem']['LocationList'] = "地點列表";
$Lang['RepairSystem']['LocationNew'] = "新增地點";
$Lang['RepairSystem']['LocationEdit'] = "更新地點";
$Lang['RepairSystem']['LocationName'] = "地點名稱";
$Lang['RepairSystem']['CategoryList'] = "類別列表";
$Lang['RepairSystem']['CategoryNew'] = "新增類別";
$Lang['RepairSystem']['CategoryEdit'] = "更新類別";
$Lang['RepairSystem']['CategoryName'] = "類別名稱";
$Lang['RepairSystem']['DeleteGroupError'] = "組別已分配到某些類別中，不能刪除。";
$Lang['RepairSystem']['NewRequest'] = "新增請求";
$Lang['RepairSystem']['ViewRequest'] = "檢視要求";
$Lang['RepairSystem']['CancelRequest'] = "取消要求";
$Lang['RepairSystem']['CancelRequestMsg'] = "你是否確定要取消要求?";
$Lang['RepairSystem']['LocationDetail'] = "地點詳情";
$Lang['RepairSystem']['RequestInformation'] = "請求明細";
$Lang['RepairSystem']['FollowupInformation'] = "跟進詳情";
$Lang['RepairSystem']['RemarkByAdmin'] = "管理員備註";
$Lang['RepairSystem']['on'] = "於";
$Lang['RepairSystem']['by'] = "由";
$Lang['RepairSystem']['LastModify'] = "最後更新";
$Lang['RepairSystem']['EditViewRequest'] = "檢視/更新請求";
$Lang['RepairSystem']['AddRemark'] = "加入新備註";
$Lang['RepairSystem']['MgmtGroupMember'] = "管理組別成員";
$Lang['RepairSystem']['SelectMembers'] = "選擇成員";
$Lang['RepairSystem']['RemarkHistory'] = "備註紀錄";
$Lang['RepairSystem']['ImportDataCol'][0] = "登入名稱";
$Lang['RepairSystem']['ImportDataCol'][1] = "<font color=red>*#</font> 紀錄日期";
$Lang['RepairSystem']['ImportDataCol'][2] = "<font color=red>*</font> 類別";
$Lang['RepairSystem']['ImportDataCol'][3] = "<font color=red>*</font> 地點";
$Lang['RepairSystem']['ImportDataCol'][4] = "地點詳情";
$Lang['RepairSystem']['ImportDataCol'][5] = "請求摘要";
$Lang['RepairSystem']['ImportDataCol'][6] = "請求詳情";
$Lang['RepairSystem']['ImportDataCol'][7] = "請求詳情";
$Lang['RepairSystem']['ImportDataCol'][8] = "狀況代號";
$Lang['RepairSystem']['ImportDataCol_New'] = array("登入名稱", "姓名", "<font color=red>#</font> 紀錄日期", "類別", "大樓", "地點", "<font color=red>&</font> 子位置", "<font color=red>&</font> 地點詳情", "請求摘要", "請求詳情", "備註", "狀況代號");
$Lang['RepairSystem']['LocationID'] = "地點代號";
$Lang['RepairSystem']['CategoryID'] = "類別代號";
$Lang['RepairSystem']['UserLoginIncorrect'] = "老師代號不正確";
$Lang['RepairSystem']['RecordDateMissing'] = "沒有輸入紀錄日期";
$Lang['RepairSystem']['RecordDateNotADate'] = "紀錄日期格式不正確";
$Lang['RepairSystem']['CategoryMissing'] = "沒有輸入類別";
$Lang['RepairSystem']['IncorrectCategory'] = "類別不正確";
$Lang['RepairSystem']['LocationMissing'] = "沒有輸入地點";
$Lang['RepairSystem']['LocationIncorrect'] = "地點不正確";
$Lang['RepairSystem']['TitleMissing'] = "沒有輸入請求摘要";
$Lang['RepairSystem']['ContentMissing'] = "沒有輸入請求詳情";
$Lang['RepairSystem']['StatusCodeMissing'] = "沒有輸入狀況代號";
$Lang['RepairSystem']['StatusCodeIncorrect'] = "狀況代號不正確";
$Lang['RepairSystem']['DateInTheFuture'] = "紀錄日期不能輸入將來的日期";
$Lang['RepairSystem']['AllRequests'] = "所有請求";
$Lang['RepairSystem']['ArchivedRequests'] = "已存檔請求";
$Lang['RepairSystem']['AllYears'] = "所有年度";
$Lang['RepairSystem']['AllMonths'] = "所有月份";
$Lang['RepairSystem']['Request'] = "請求";
$Lang['RepairSystem']['RequestDetails'] = "請求詳情";
$Lang['RepairSystem']['RequestSummary'] = "請求摘要";
$Lang['RepairSystem']['Reporter'] = "填報人";
$Lang['RepairSystem']['Completed'] = "已完成";
$Lang['RepairSystem']['Name'] = "名稱";
$Lang['RepairSystem']['ResponsibleGroup'] = "負責小組";
$Lang['RepairSystem']['Member'] = "組員";
$Lang['RepairSystem']['CompleteDate'] = "完成日期";
$Lang['RepairSystem']['RequestDate'] = "請求日期";
$Lang['RepairSystem']['AllCategory'] = "全部類別";
$Lang['RepairSystem']['AllRequestSummary'] = "全部摘要";
$Lang['RepairSystem']['AllLocation'] = "全部地點";
$Lang['RepairSystem']['Processing'] = $i_Discipline_System_Discipline_Case_Record_Processing;
$Lang['RepairSystem']['DefaultDetailsContent'] = "預設維修請求語句";
$Lang['RepairSystem']['Pending'] = '等待處理';
$Lang['RepairSystem']['ClicktoCheck'] = '按此查詢';
$Lang['RepairSystem']['CaseNumber'] ='參考編號';
$Lang['RepairSystem']['BuildingIncorrect'] = '大樓不正確';
$Lang['RepairSystem']['LocationIncorrect'] = '地點不正確';
$Lang['RepairSystem']['StubLocIncorrect'] = '子位置不正確';
$Lang['RepairSystem']['CategoryPrefix']['Title'] = '類別字首';
$Lang['RepairSystem']['CategoryPrefix']['Remarks'] ='1-4個英文字母作參考編號的字首';
$Lang['RepairSystem']['CategoryPrefix']['Alert'] ='不支援數字和特殊字元';
$Lang['RepairSystem']['CategoryPrefix']['Alert2'] = '超出長度限制';
$Lang['RepairSystem']['Notify'] = "通知";
$Lang['RepairSystem']['NotifyPerson'] = "通知相關人員";
$Lang['RepairSystem']['SystemFollowUpPerson'] = '跟進人員(系統用戶)';
$Lang['RepairSystem']['MessageToReporter'] = "通知相關人員方法";
$Lang['RepairSystem']['MessageNotSend'] = "不通知";
$Lang['RepairSystem']['MessageApp'] = "eClass App";
$Lang['RepairSystem']['MessageEmail'] = "電郵";

$Lang['RepairSystem']['AllRequests'] = "所有請求";
$Lang['RepairSystem']['MyRequests'] = "我的請求";
$Lang['RepairSystem']['NewRequestSummary'] = "新增摘要";
$Lang['RepairSystem']['AllInUseStatus'] = "所有狀況";
$Lang['RepairSystem']['EditRequestSummary'] = "修改摘要";
$Lang['RepairSystem']['Statistics'] = "統計";
$Lang['RepairSystem']['RequestStatistics'] = "請求統計";
$Lang['RepairSystem']['RequestPeriod'] = "請求時段";
$Lang['RepairSystem']['NoOfRecords'] = "紀錄數量";
$Lang['RepairSystem']['UpdateRequestSummaryWarning'] = "如修改類別或請求摘要，變更將被套用到現存的請求紀錄。<br><br>受影響的紀錄數量： ";
$Lang['RepairSystem']['ReOrder'] = "重新排序";
$Lang['RepairSystem']['DeleteRequestSummaryConfirm'] = "是否確定要移除已選的紀錄？";
$Lang['RepairSystem']['Category_RequestSummary'] = "類別及請求摘要";
$Lang['RepairSystem']['MismatchRequestSummary'] = "摘要不正確";
$Lang['RepairSystem']['Building'] = "大樓";
$Lang['RepairSystem']['MainBuilding'] = "主大樓";
$Lang['RepairSystem']['IncludeArchivedRecord'] = "包含已存檔紀錄";
$Lang['RepairSystem']['Archived'] = "已存檔";
$Lang['RepairSystem']['NoOfArchivedRecordIncluded'] = "包含已存檔紀錄數量";
$Lang['RepairSystem']['DisplayArchivedRecord'] = "顯示已存檔之紀錄";
$Lang['RepairSystem']['SendEmailToReporter'] = "電郵給填報人";
$Lang['RepairSystem']['FollowupAction'] = "跟進行動";
$Lang['RepairSystem']['RequestCompleted'] = "報修系統的請求已完成";

$Lang['RepairSystem']['ImportField_IP25']['EN'] = array("User Login", "Name [Ref]", "Record Date", "Category", "Building", "Location", "Sub-location", "Location Detail", "Request Summary", "Request Details", "Remark", "Status Code");
$Lang['RepairSystem']['ImportField_IP25']['B5'] = array("(內聯網帳號)", "(姓名 [參考用途])", "(紀錄日期)", "(類別)", "(大樓)", "(位置)", "(子位置)", "(地點詳情)", "(請求摘要)", "(請求詳情)", "(備註)", "(狀況代號)");
$Lang['RepairSystem']['JSWarning']['AreYouSureArchiveSelectedRecord'] = "系統只會存檔「已完成」或「已被拒絕」的紀錄，你是否確定要存檔已選之紀錄？";
$Lang['RepairSystem']['JSWarning']['AreYouSureArchiveRecord'] = "你是否確定要存檔紀錄？";
$Lang['RepairSystem']['JSWarning']['AreYouSureDeleteSelectedRecord'] = "系統只會刪除「已取消」的紀錄，你是否確定要刪除紀錄？";

$Lang['RepairSystem']['MappingSettings'] = "配對設定	";
$Lang['RepairSystem']['MapToInventory'] = "配對物品 (只適用於單一物品)";
$Lang['RepairSystem']['NoItemIsMapped'] = "尚未配對";
$Lang['RepairSystem']['SelectMapMethod'] = "請點選以下其中一項:";
$Lang['RepairSystem']['SuggestedItemsCheck'] = "系統按物品名稱配對";
$Lang['RepairSystem']['AdvanceSearch'] = "手動配對";
$Lang['RepairSystem']['SystemUser'] = "系統用戶: ";
$Lang['RepairSystem']['NonSystemUser'] = "非系統用戶: ";
$Lang['RepairSystem']['FollowUpRequest'] = "我需跟進的請求";
$Lang['RepairSystem']['Mapped'] = "已配對:";
$Lang['RepairSystem']['NoResults'] = "找不到配對結果";
$Lang['RepairSystem']['UploadPhotos'] = "上載照片";
$Lang['RepairSystem']['SelectPhotos'] = "選取照片";
$Lang['RepairSystem']['OrDragAndDropPhotosHere'] = "或者在此拖放照片。";
$Lang['RepairSystem']['ViewOriginalPhoto'] = "瀏覽原圖";
$Lang['RepairSystem']['jsWaitAllPhotosUploaded'] = "請等待直至所有照片完成上傳。";
$Lang['RepairSystem']['PhotoRemark'] = "只支援以下圖片格式: JPEG檔(.jpg)，GIF檔(.gif) 或 PNG檔(.png)";
$Lang['RepairSystem']['SelectSublocation'] = "- 選擇子位置 -";
$Lang['RepairSystem']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
$Lang['RepairSystem']['RequestProcess'] = "報修系統的請求在處理中";
$Lang['RepairSystem']['RequestRejected'] = "報修系統的請求已遭拒絕";
$Lang['RepairSystem']['RequestPending'] = "報修系統的請求正等待處理";
$Lang['RepairSystem']['SubmitAndNotifyReporter'] = "呈送並通知填報人";
$Lang['RepairSystem']['SubmitWiNotifyRemark'] = "通知填報人: 如果有購買eClass App,將會以推送訊息通知,否則以電郵通知";
$Lang['RepairSystem']['B5']['MainBuilding'] = "主大樓";
$Lang['RepairSystem']['EN']['MainBuilding'] = "Main Building";
$Lang['RepairSystem']['B5']['StatusProcess'] = "處理中";
$Lang['RepairSystem']['EN']['StatusProcess'] = "Processing";
$Lang['RepairSystem']['B5']['StatusCompleted'] = "已完成";
$Lang['RepairSystem']['EN']['StatusCompleted'] = "Completed";
$Lang['RepairSystem']['B5']['StatusRejected'] = "已拒絕";
$Lang['RepairSystem']['EN']['StatusRejected'] = "Rejected";
$Lang['RepairSystem']['B5']['StatusPending'] = "等待處理";
$Lang['RepairSystem']['EN']['StatusPending'] = "Pending";
$Lang['RepairSystem']['CopyFromInventory'] = "從資產管理行政系統複製";
$Lang['RepairSystem']['GroupList'] = "小組列表";
$Lang['RepairSystem']['ItemPhoto'] = "物品相片";
$Lang['RepairSystem']['Export']['RefNo'] = "(參考編號)";
$Lang['RepairSystem']['Export']['FollowupPerson'] = "(跟進人員)";
$Lang['RepairSystem']['ReturnMessage']['UpdateAndNotifySuccess'] = "1|=|紀錄已經更新並且通知填報人";
$Lang['RepairSystem']['ReturnMessage']['UpdateSuccessNotifyFail'] = "0|=|紀錄已經更新但未能通知填報人";
$Lang['RepairSystem']['Settings']['Location'] = "位置";
$Lang['RepairSystem']['Settings']['Room'] = "房間";
$Lang['RepairSystem']['Settings']['Instruction'] = "指示";
$Lang['RepairSystem']['Settings']['PleaseSelectLocation'] = "請選擇位置。";
$Lang['RepairSystem']['Settings']['PleaseSelectOneOption'] = "請選擇一個選項。";
$Lang['RepairSystem']['Settings']['ApplyToSelected'] = "套用到所選位置";

# Country array
$Lang['Country'] =
array(
		"Afghanistan",
		"Albania",
		"Algeria",
		"American Samoa",
		"Andorra",
		"Angola",
		"Anguilla",
		"Antarctica",
		"Antigua and Barbuda",
		"Argentina",
		"Armenia",
		"Aruba",
		"Australia",
		"Austria",
		"Azerbaijan",
		"Bahamas",
		"Bahrain",
		"Bangladesh",
		"Barbados",
		"Belarus",
		"Belgium",
		"Belize",
		"Benin",
		"Bermuda",
		"Bhutan",
		"Bolivia",
		"Bosnia and Herzegowina",
		"Botswana",
		"Bouvet Island",
		"Brazil",
		"British Indian Ocean Territory",
		"Brunei Darussalam",
		"Bulgaria",
		"Burkina Faso",
		"Burundi",
		"Cambodia",
		"Cameroon",
		"Canada",
		"Cape Verde",
		"Cayman Islands",
		"Central African Republic",
		"Chad",
		"Chile",
		"China",
		"Christmas Island",
		"Cocos (Keeling) Islands",
		"Colombia",
		"Comoros",
		"Congo",
		"Congo, the Democratic Republic of the",
		"Cook Islands",
		"Costa Rica",
		"Cote d'Ivoire",
		"Croatia (Hrvatska)",
		"Cuba",
		"Cyprus",
		"Czech Republic",
		"Denmark",
		"Djibouti",
		"Dominica",
		"Dominican Republic",
		"East Timor",
		"Ecuador",
		"Egypt",
		"El Salvador",
		"Equatorial Guinea",
		"Eritrea",
		"Estonia",
		"Ethiopia",
		"Falkland Islands (Malvinas)",
		"Faroe Islands",
		"Fiji",
		"Finland",
		"France",
		"France, Metropolitan",
		"French Guiana",
		"French Polynesia",
		"French Southern Territories",
		"Gabon",
		"Gambia",
		"Georgia",
		"Germany",
		"Ghana",
		"Gibraltar",
		"Greece",
		"Greenland",
		"Grenada",
		"Guadeloupe",
		"Guam",
		"Guatemala",
		"Guinea",
		"Guinea-Bissau",
		"Guyana",
		"Haiti",
		"Heard and Mc Donald Islands",
		"Holy See (Vatican City State)",
		"Honduras",
		"Hong Kong",
		"Hungary",
		"Iceland",
		"India",
		"Indonesia",
		"Iran",
		"Iraq",
		"Ireland",
		"Israel",
		"Italy",
		"Jamaica",
		"Japan",
		"Jordan",
		"Kazakhstan",
		"Kenya",
		"Kiribati",
		"Korea, Democratic People's Republic of",
		"Korea, Republic of",
		"Kuwait",
		"Kyrgyzstan",
		"Lao People's Democratic Republic",
		"Latvia",
		"Lebanon",
		"Lesotho",
		"Liberia",
		"Libyan Arab Jamahiriya",
		"Liechtenstein",
		"Lithuania",
		"Luxembourg",
		"Macau",
		"Macedonia, The Former Yugoslav Republic of",
		"Madagascar",
		"Malawi",
		"Malaysia",
		"Maldives",
		"Mali",
		"Malta",
		"Marshall Islands",
		"Martinique",
		"Mauritania",
		"Mauritius",
		"Mayotte",
		"Mexico",
		"Micronesia, Federated States of",
		"Moldova, Republic of",
		"Monaco",
		"Mongolia",
		"Montserrat",
		"Morocco",
		"Mozambique",
		"Myanmar",
		"Namibia",
		"Nauru",
		"Nepal",
		"Netherlands",
		"Netherlands Antilles",
		"New Caledonia",
		"New Zealand",
		"Nicaragua",
		"Niger",
		"Nigeria",
		"Niue",
		"Norfolk Island",
		"Northern Mariana Islands",
		"Norway",
		"Oman",
		"Pakistan",
		"Palau",
		"Panama",
		"Papua New Guinea",
		"Paraguay",
		"Peru",
		"Philippines",
		"Pitcairn",
		"Poland",
		"Portugal",
		"Puerto Rico",
		"Qatar",
		"Reunion",
		"Romania",
		"Russian Federation",
		"Rwanda",
		"Saint Kitts and Nevis",
		"Saint LUCIA",
		"Saint Vincent and the Grenadines",
		"Samoa",
		"San Marin",
		"Sao Tome and Principe",
		"Saudi Arabia",
		"Senegal",
		"Seychelles",
		"Sierra Leone",
		"Singapore",
		"Slovakia (Slovak Republic)",
		"Slovenia",
		"Solomon Islands",
		"Somalia",
		"South Africa",
		"South Georgia and the South Sandwich Islands",
		"Spain",
		"Sri Lanka",
		"St. Helena",
		"St. Pierre and Miquelon",
		"Sudan",
		"Suriname",
		"Svalbard and Jan Mayen Islands",
		"Swaziland",
		"Sweden",
		"Switzerland",
		"Syrian Arab Republic",
		"Taiwan, Province of China",
		"Tajikistan",
		"Tanzania",
		"Thailand",
		"Togo",
		"Tokelau",
		"Tonga",
		"Trinidad and Tobago",
		"Tunisia",
		"Turkey",
		"Turkmenistan",
		"Turks and Caicos Islands",
		"Tuvalu",
		"Uganda",
		"Ukraine",
		"United Arab Emirates",
		"United Kingdom",
		"United States",
		"United States Minor Outlying Islands",
		"Uruguay",
		"Uzbekistan",
		"Vanuatu",
		"Venezuela",
		"Viet Nam",
		"Virgin Islands (British)",
		"Virgin Islands (U.S.)",
		"Wallis and Futuna Islands",
		"Western Sahara",
		"Yemen",
		"Yugoslavia",
		"ZMZambia",
		"Zimbabwe"
);

# eAdmin > Account Management
$Lang['Header']['Menu']['AccountMgmt'] = "用戶管理";
$Lang['Header']['Menu']['StudentAccount'] = "學生用戶";
$Lang['Header']['Menu']['StaffAccount'] = "教職員用戶";
$Lang['Header']['Menu']['ParentAccount'] = "家長用戶";
$Lang['Header']['Menu']['AlumniAccount'] = "校友用戶";
$Lang['Header']['Menu']['StudentRegistry'] = "學籍系統";
$Lang['Header']['Menu']['SharedMailBox'] = "共享郵箱";
$Lang['Menu']['AccountMgmt']['Account'] = "用戶";
$Lang['Menu']['AccountMgmt']['Photo'] = "相片";
$Lang['Menu']['AccountMgmt']['Management'] = "管理";
$Lang['Menu']['AccountMgmt']['Reports'] = "報告";
$Lang['Menu']['AccountMgmt']['Settings'] = "設定";
$Lang['Menu']['AccountMgmt']['StudentRegistryInfo'] = "學籍資料";
$Lang['Menu']['AccountMgmt']['AccessRight'] = "權限";
$Lang['Menu']['AccountMgmt']['Statistics'] = "統計";
$Lang['Menu']['AccountMgmt']['Parent'] = "家長統計";
$Lang['Menu']['AccountMgmt']['Student'] = "學生統計";
$Lang['AccountMgmt']['Menu_ThreeAccMgmt'] = " (家長用戶, 教職員用戶, 學生用戶)";
$Lang['AccountMgmt']['Menu_ParentMgmt'] = " (家長用戶)";
$Lang['AccountMgmt']['Menu_StaffMgmt'] = " (教職員用戶)";
$Lang['AccountMgmt']['Menu_StudentMgmt'] = " (學生用戶)";
$Lang['AccountMgmt']['Menu_StudentRegistry'] = " (學籍系統)";
$Lang['AccountMgmt']['Menu_SharedMailBox'] = " (共享郵箱)";
$Lang['AccountMgmt']['UserList'] = "用戶清單";
$Lang['AccountMgmt']['StudentNotInClass'] = "無所屬班別學生";
$Lang['AccountMgmt']['ParentWithoutChildInClass'] = "其他";
$Lang['AccountMgmt']['ParentAll'] = "所有家長";
$Lang['AccountMgmt']['ParentWithoutChild'] = "無連結學生之家長";
$Lang['AccountMgmt']['ParentWithoutChildInCurrentYearClass'] = "無所屬班別學生家長";;
$Lang['AccountMgmt']['NewUser'] = "新增用戶";
$Lang['AccountMgmt']['EditUser'] = "更新用戶";
$Lang['AccountMgmt']['ExportUser'] = "匯出用戶";
$Lang['AccountMgmt']['DeleteUser'] = "刪除用戶";
$Lang['AccountMgmt']['ArchiveStudent'] = "封存用戶";
$Lang['AccountMgmt']['SystemInfo'] = "系統資料";
$Lang['AccountMgmt']['BasicInfo'] = "基本資料";
$Lang['AccountMgmt']['InternetUsage'] = "互聯網用途";
$Lang['AccountMgmt']['Chi'] = "中文";
$Lang['AccountMgmt']['UserDisplayTitle'] = "顯示稱謂";
$Lang['AccountMgmt']['Retype'] = "再次輸入";
$Lang['AccountMgmt']['SelectCountry'] = "選擇國家";
$Lang['AccountMgmt']['Tel'] = "電話";
$Lang['AccountMgmt']['Home'] = "住宅";
$Lang['AccountMgmt']['Office'] = "公司";
$Lang['AccountMgmt']['Mobile'] = "手提";
$Lang['AccountMgmt']['Nationality'] = "國籍";
$Lang['AccountMgmt']['PlaceOfBirth'] = "出生地點";
$Lang['AccountMgmt']['AdmissionDate'] = "入學日期";
$Lang['AccountMgmt']['GraduationDate'] = "畢業日期";
$Lang['AccountMgmt']['QuitTime'] = "離校時間";
$Lang['AccountMgmt']['Fax'] = "傳真";
$Lang['AccountMgmt']['SaveAndContinue'] = "儲存並繼續";
$Lang['AccountMgmt']['ClassGroupInfo'] = "班別及組別資料";
$Lang['AccountMgmt']['UsedByOtherUser'] = "已被其他人使用。";
$Lang['AccountMgmt']['SendPassword'] = "傳送密碼";
$Lang['AccountMgmt']['SendResetPasswordEmail'] = "傳送重設密碼電郵";
$Lang['AccountMgmt']['HKJApplNo'] = 'HK JUPAS Application Number';
$Lang['AccountMgmt']['Group'] = "組別";
$Lang['AccountMgmt']['Role'] = "身份角色";
$Lang['AccountMgmt']['RoleID'] = "身份角色代號";
$Lang['AccountMgmt']['Barcode'] = "條碼";
$Lang['AccountMgmt']['Grade'] = "職級";
$Lang['AccountMgmt']['Duty'] = "職責";
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserLogin;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserPassword;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_identity." (\"T\" = ".$i_campusmail_teaching_staff.", \"NT\" = ".$i_campusmail_non_teaching_staff.")";
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserEmail;
$Lang['AccountMgmt']['StaffImport'][] = $i_SmartCard_CardID;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserEnglishName;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserChineseName;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserNickName;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserGender." (\"M\" = $i_gender_male, \"F\" = $i_gender_female)";
//$Lang['AccountMgmt']['StaffImport'][] = $i_UserTitle;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserDisplayTitle_English;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserDisplayTitle_Chinese;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserAddress;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserCountry;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserHomeTelNo;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserOfficeTelNo;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserMobileTelNo;
//$Lang['AccountMgmt']['StaffImport'][] = $i_StaffAttendance_GroupID;
//$Lang['AccountMgmt']['StaffImport'][] = $Lang['AccountMgmt']['RoleID'];
$Lang['AccountMgmt']['StaffImport'][] = $i_Mail_AllowSendReceiveExternalMail." (\"Y\" = $i_general_yes, \"N\" = $i_general_no)";
$Lang['AccountMgmt']['StaffImport'][] = $i_Files_OpenAccount." (\"Y\" = $i_general_yes, \"N\" = $i_general_no)";
$Lang['AccountMgmt']['SomeGroupsDoNotExist'] = "部份組別不存在";
$Lang['AccountMgmt']['SomeRolesDoNotExist'] = "部份身份角式不存在";
$Lang['AccountMgmt']['InputStaffDetails'] = "輸入教職員資料";
$Lang['AccountMgmt']['SelectGroupAndRole'] = "選擇相關組別及身份角色";
$Lang['AccountMgmt']['InputStudentDetails'] = "輸入學生資料";
$Lang['AccountMgmt']['InputGuardianDetail'] = "輸入監護人資料";
$Lang['AccountMgmt']['YYYYMMDD'] = "yyyy-mm-dd";
$Lang['AccountMgmt']['OtherInfo'] = "其他資料";
$Lang['AccountMgmt']['ClassGroupInfo'] = "班別及組別資料";
$Lang['AccountMgmt']['GuardianInfo'] = "監護人資料";
$Lang['AccountMgmt']['StudentInfo'] = "學生資料";
$Lang['AccountMgmt']['MainGuardian'] = "主監護人";
$Lang['AccountMgmt']['ReceiveSMS'] = "SMS收取人";
$Lang['AccountMgmt']['EmergencyContact'] = "緊急聯絡人";
$Lang['AccountMgmt']['AlertDeleteEmergencyContact'] = "你選擇刪除緊急聯絡人，請選擇其他緊急聯絡人。";
$Lang['AccountMgmt']['InputParentDetails'] = "輸入家長資料";
$Lang['AccountMgmt']['SelectCorrespondingStudents'] = "選擇相關學生";
$Lang['AccountMgmt']['CheckAvailability'] = "檢查可用性";
$Lang['AccountMgmt']['IsAvailable'] = "可以使用。";
$Lang['AccountMgmt']['IsNotAvailable'] = "已有人使用。";
$Lang['AccountMgmt']['RelatedToStudent'] = "學生相關資料";
$Lang['AccountMgmt']['AddAsStudentGuardian'] = "成為學生監護人";
$Lang['AccountMgmt']['SelectMore'] = "選擇更多";
$Lang['AccountMgmt']['CopyToAll'] = "套用至所有";
$Lang['AccountMgmt']['AllClassesParent'] = "所有班別家長";
$Lang['AccountMgmt']['ClassNo'] = "學號";
$Lang['AccountMgmt']['StudentName'] = "學生姓名";
$Lang['AccountMgmt']['LoginID'] = "登入編號";
$Lang['AccountMgmt']['Photo'] = "相片";
$Lang['AccountMgmt']['Form'] = "級別";
$Lang['AccountMgmt']['Class'] = "班別";
$Lang['AccountMgmt']['NoOfStudents'] = "學生人數";
$Lang['AccountMgmt']['NoOfStudentsWithoutPhoto'] = "沒有相片的學生人數";
$Lang['AccountMgmt']['ConfirmMsg']['RemoveSelectedPhoto'] = "確定要刪除所選所選的相片?";
$Lang['AccountMgmt']['UploadPhoto']['StepArr'][] = "上載相片";
$Lang['AccountMgmt']['UploadPhoto']['StepArr'][] = "上載完成";
$Lang['AccountMgmt']['Remarks'] = "備註";
$Lang['AccountMgmt']['File'] = "檔案";
$Lang['AccountMgmt']['PhotoOperationOption'] = "相片操作選項";
$Lang['AccountMgmt']['DoNotScalePhoto'] = "不縮放相片";
$Lang['AccountMgmt']['ScalePhotoToFitRecommendedDimension'] = "縮放相片至建議的尺寸";
$Lang['AccountMgmt']['RemarksList'][] = "建議使用 412px x 459px (闊 x 高)的相片";
$Lang['AccountMgmt']['RemarksList'][] = "只接受JPEG檔的相片";
$Lang['AccountMgmt']['RemarksList'][] = "可以以壓縮檔案(.zip)形式上載多個檔案";
$Lang['AccountMgmt']['RemarksList'][] = "壓縮檔案內不能存在文件夾";
$Lang['AccountMgmt']['RemarksList'][] = "相片的檔案名稱要與學生的登入編號一樣";
$Lang['AccountMgmt']['PhotoAttempt'] = "上載相片數";
$Lang['AccountMgmt']['UploadSuccess'] = "上載成功";
$Lang['AccountMgmt']['UploadFail'] = "上載失敗";
$Lang['AccountMgmt']['UploadMorePhoto'] = "上載更多相片";
$Lang['AccountMgmt']['BackToPhotoMgmt'] = "返回相片管理";
$Lang['AccountMgmt']['ManagePhoto'] = "管理相片";
$Lang['AccountMgmt']['WarnMsg']['PleaseSelectJPGorZIP'] = "請選擇JPEG檔(.jpg)或壓縮檔(.zip)";
$Lang['AccountMgmt']['ImportStudentAccount'] = "匯入學生戶口";
$Lang['AccountMgmt']['ImportParentAccount'] = "匯入家長戶口";
$Lang['AccountMgmt']['ImportTeacherAccount'] = "匯入老師戶口";
$Lang['AccountMgmt']['ImportAlumniAccount'] = "匯入校友戶口";
$Lang['AccountMgmt']['Import'] = "匯入";
$Lang['AccountMgmt']['Export'] = "匯出";
$Lang['AccountMgmt']['FileMailRemarks'] = "系統只會為新 eClass 用戶, 在檔案/郵件伺服器開啟戶口. 現有用戶需要在 <b>行政管理中心 > 功能設定 > iFolder 儲存量 </b> 設定";
$Lang['AccountMgmt']['Options'] = "選項";
//2013-1106-0919-12066
//$Lang['AccountMgmt']['ImportRemarks'][] = "檔案中的 'UserLogin' 須由 'a-z'(小楷), '0-9' 及 '_' 組成, 而且必須使用 'a-z' 為開始字元";
//$Lang['AccountMgmt']['ImportRemarks'][] = "如果檔案中的 'UserLogin' 已經存在, 該帳戶所有資料會被更新(包括電郵及密碼)";
//$Lang['AccountMgmt']['ImportRemarks'][] = "如欲不更新電郵及密碼, 可在檔案中留空該欄";
//$Lang['AccountMgmt']['ImportRemarks'][] = "密碼請不要使用 \" ' 空格 $ & &lt; &gt; + \ 符號。";
$Lang['AccountMgmt']['ImportRemarks'][] = "檔案中的 'UserLogin' 可由 'a-z'(小楷)、 '0-9' 或 '_' 組成，並必須使用 'a-z' 為開始字元。";
$Lang['AccountMgmt']['ImportRemarks'][] = "如檔案中包含現有的 'UserLogin'，其帳戶的資料只會被更新，系統並不會創建另一個相同的帳戶。";
$Lang['AccountMgmt']['ImportRemarks'][] = "如不更新密碼，可在檔案中留空該欄；但倘若留空其他欄位，原有內容會被更新至空白。";
$Lang['AccountMgmt']['ImportRemarks'][] = "密碼請不要使用 \" ' 空格 $ & &lt; &gt; + \ 符號。";
$Lang['AccountMgmt']['ImportRemarks2'] = "為免遺失號碼前端的數字 \"0\"，請在每個 WebSAMS 學生註冊編號前加上 \"#\" 號 (如: #0012345)。";
$Lang['AccountMgmt']['ShowPrimarySchoolCode'] = "如無合適選項請留空" ;
$Lang['AccountMgmt']['Remarks'] = "備註";
$Lang['AccountMgmt']['PrimarySchoolEnglishName'] = "小學英文名稱";

$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT'] = "請選擇CSV檔(.csv) 或純文字檔(.txt)";

$Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'] = "電郵格式錯誤";
$Lang['AccountMgmt']['ErrorMsg']['InvalidLoginEmail'] = "登入名稱或電郵地址格式錯誤";
$Lang['AccountMgmt']['ErrorMsg']['MissedLogin'] = "未有輸入登入名稱";
$Lang['AccountMgmt']['ErrorMsg']['MissedEnglishName'] = "未有輸入英文姓名";
$Lang['AccountMgmt']['ErrorMsg']['EmailUsedInImport'] = "電郵已被其他匯入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['EmailUsed'] = "電郵已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['DifferentUserType'] = "使用者類別與匯入中的使用者類別不同";
$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsedInImport'] = "智能卡編號已被其他匯入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'] = "智能卡編號已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['HKIDUsedInImport'] = "HKID 已被其他匯入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['HKIDUsed'] = "HKID 已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['HKJApplNoUserd'] = $Lang['AccountMgmt']['HKJApplNo']."已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['STRNUsedInImport'] = "STRN 已被其他匯入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['STRNUsed'] = "STRN 已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoUsedInImport'] = "WebSAMS 註冊號碼已被其他匯入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoUsed']  = "WebSAMS 註冊號碼已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoCannotChange'] = "WebSAMS 註冊號碼不能更改";
$Lang['AccountMgmt']['ErrorMsg']['PleaseAddSymbol'] = "請在WebSAMS 註冊號碼前加上#號";
$Lang['AccountMgmt']['ErrorMsg']['CannotFindStudent'] = "找不到學生";
$Lang['AccountMgmt']['ErrorMsg']['GenderCannotBeEmpty'] = "性別不能留空";
$Lang['AccountMgmt']['ErrorMsg']['InvalidGenderFormat'] = "性別格式錯誤";
$Lang['AccountMgmt']['ErrorMsg']['NewAccountPasswordCannotBeEmpty'] = "新用戶密碼不能留空";
$Lang['AccountMgmt']['ErrorMsg']['MissedDOB'] = "未有輸入出生日期";
$Lang['AccountMgmt']['ErrorMsg']['InvalidDateFormat'] = "日期格式錯誤";
$Lang['AccountMgmt']['ErrorMsg']['UserAccountExistForAddAction'] = "該用戶已經存在，新增動作不會進行處理。";
$Lang['AccountMgmt']['ErrorMsg']['UserAccountNotExistForUpdateAction'] = "該用戶不存在，不能進行更新動作。";
$Lang['AccountMgmt']['ErrorMsg']['HouseNotFound'] = "找不到對應的社級。";
$Lang['AccountMgmt']['ErrorMsg']['PrimarySchoolCodeNotFound'] = "未能配對小學編號";
$Lang['AccountMgmt']['ExportStudentAccount'] = "匯出學生戶口";
$Lang['AccountMgmt']['ExportParentAccount'] = "匯出家長戶口";
$Lang['AccountMgmt']['ExportTeacherAccount'] = "匯出老師戶口";
$Lang['AccountMgmt']['ExportAlumniAccount'] = "匯出校友戶口";
$Lang['AccountMgmt']['CondfirmDeleteAllParent'] = "你是否確定要刪除所有家長?";
$Lang['AccountMgmt']['DeleteSelected'] = "刪除已選";
$Lang['AccountMgmt']['DeleteAll'] = "刪除所有";
$Lang['AccountMgmt']['ConfirmDeleteAllParent'] = "你是否確定刪除所有家長戶口?";
$Lang['AccountMgmt']['ConfirmArchiveAllStudent'] = "用戶帳號將被永久封存，而且無法恢復。你是否確定封存所有學生戶口?";
$Lang['AccountMgmt']['ConfirmArchiveSelectedStudent'] = "用戶帳號將被永久封存，而且無法恢復。你是否確定封存已選的學生戶口?";
$Lang['AccountMgmt']['ArchiveAll'] = "封存所有";
$Lang['AccountMgmt']['ArchiveSelected'] = "封存已選";
$Lang['AccountMgmt']['ExportFormat'] = "匯出格式";
$Lang['AccountMgmt']['SpecificColumns'] = "指定資料";
$Lang['AccountMgmt']['Importing'] = "匯入";
$Lang['AccountMgmt']['Settings']['AccessRights'] = "權限設定";
$Lang['AccountMgmt']['Settings']['GroupList'] = "小組列表";
$Lang['AccountMgmt']['Settings']['GroupName'] = "小組名稱";
$Lang['AccountMgmt']['Settings']['Description'] = "描述";
$Lang['AccountMgmt']['Settings']['NoOfMembers'] = "成員人數";
$Lang['AccountMgmt']['Settings']['AccessRight'] = "存取權限";
$Lang['AccountMgmt']['Settings']['UserList'] = "用戶名單";
$Lang['AccountMgmt']['Settings']['View'] = "檢視";
$Lang['AccountMgmt']['Settings']['Manage'] = "管理";
$Lang['AccountMgmt']['Settings']['Access'] = "存取";
$Lang['AccountMgmt']['Settings']['RemindChangeEmail'] = "於首次登入提示更改電子郵件";
$Lang['AccountMgmt']['Settings']['Remarks']['OnlyApplyToEclass'] = "只適用於網上教室內的名冊。";
$Lang['AccountMgmt']['AlertMsg_SuspendConfirm'] = "你確定要暫停已選的學生戶口?";
$Lang['AccountMgmt']['AlertMsg_LeftConfirm'] = "你是否確定所選學生已經離校?";

$Lang['AccountMgmt']['PersonalInfo'] = "個人資料";
$Lang['AccountMgmt']['ContactInfo'] = "聯絡資料";
$Lang['AccountMgmt']['display'] = "顯示";
$Lang['AccountMgmt']['UserCanUpdate'] = "用戶可修改";
$Lang['AccountMgmt']['AdditionInfo'] = "附加資料";
$Lang['AccountMgmt']['UpdateMobileWarning'] = "若允許用戶更改手提電話號碼，即使所輸入的號碼並不存在，傳送SMS短訊到該等號碼時，仍然會佔用限額。你是夠確定要允許用戶更改號碼?";
$Lang['AccountMgmt']['MobileUsage'] = "(此號碼將同時用於接收SMS短訊。)";
$Lang['AccountMgmt']['EnablePasswordPolicy'] = "強制用戶使用至少由六個英文字母及數字混合組成的密碼，以加強戶口的安全性！";

# Account Mgmt > Parent Mgmt / Student Mgmt / Staff Mgmt
$Lang['AccountMgmt']['ParentImportFields'] = array("內聯網帳號", "密碼", "電子郵件", "英文姓名", "中文姓名", "性別", "手提電話", "傳真號碼", $Lang['AccountMgmt']['Barcode'], "備註");
$Lang['AccountMgmt']['StaffImportFields'] = array("內聯網帳號", "密碼", "電子郵件", "英文姓名", "中文姓名", "別名", "性別", "手提電話", "傳真號碼", $Lang['AccountMgmt']['Barcode'], "備註", "顯示稱謂(中文)", "顯示稱謂(英文)", "WebSAMS教職員代碼");
$Lang['AccountMgmt']['StudentImportFields'] = array("內聯網帳號", "密碼", "電子郵件", "英文姓名", "中文姓名", "別名", "性別", "住宅電話", "手提電話", "傳真號碼", $Lang['AccountMgmt']['Barcode'], "備註", "出生日期", "WebSAMS學生註冊編號",$Lang['AccountMgmt']['HKJApplNo'], "地址");
$Lang['AccountMgmt']['AlumniImportFields'] = array("內聯網帳號", "密碼", "電子郵件", "英文姓名", "中文姓名", "別名", "性別", "手提電話", "傳真號碼", $Lang['AccountMgmt']['Barcode'], "備註", "班別", "班號","離校年份");
$Lang['AccountMgmt']['ParentLink'] = "連結家長";
if($ssoservice["Google"]["Valid"]){
	$Lang['AccountMgmt']['GoogleImportFields'] = array('啟用 G Suite 帳戶');
}

# Account Mgmt > Parent Mgmt / Student Mgmt / Staff Mgmt > Personal Settings
$Lang['AccountMgmt']['AddnPersonalSettings'] = array('Nationality','PlaceOfBirth','AdmissionDate','GraduationDate');
$Lang['AccountMgmt']['AddnStaffFields'] = array('Grade','Duty');
if($plugin['medical']){
	$Lang['AccountMgmt']['AddnPersonalSettings'] = array_merge(array('StayOverNight'),$Lang['AccountMgmt']['AddnPersonalSettings']);
	$Lang['AccountMgmt']['StayOverNight'] = "留宿";
}

if($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox']  || $plugin['eEnrollment']) {
	$Lang['AccountMgmt']['StaffImportFields'][] = "智能咭 ID";
	$Lang['AccountMgmt']['StudentImportFields'][] = "智能咭 ID";
	if($sys_custom['SupplementarySmartCard']){
		$Lang['AccountMgmt']['StudentImportFields'][] = "智能咭 ID 2";
		$Lang['AccountMgmt']['StudentImportFields'][] = "智能咭 ID 3";
		$Lang['AccountMgmt']['StudentImportFields'][] = "智能咭 ID 4";
	}
}
if($special_feature['ava_hkid']) {
	$Lang['AccountMgmt']['ParentImportFields'][] = "身份証號碼";
	$Lang['AccountMgmt']['StaffImportFields'][] = "身份証號碼";
	$Lang['AccountMgmt']['StudentImportFields'][] = "身份証號碼";
}
$Lang['AccountMgmt']['ParentImportFields'] = array_merge($Lang['AccountMgmt']['ParentImportFields'], array("內聯網帳號(學生一)", "英文姓名(學生一)", "內聯網帳號(學生二)", "英文姓名(學生二)", "內聯網帳號(學生三)", "英文姓名(學生三)"));
if($special_feature['ava_strn']) {
	$Lang['AccountMgmt']['StudentImportFields'][] = "STRN";
}
if($sys_custom['BIBA_AccountMgmtCust']){
	$Lang['AccountMgmt']['StaffImportFields'][] = "社級";
}
if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']){
	$Lang['AccountMgmt']['StaffImportFields'][] = "電子郵件帳號";
}
if($sys_custom['BIBA_AccountMgmtCust']){
	$Lang['AccountMgmt']['StudentImportFields'][] = "戶口";
	$Lang['AccountMgmt']['StudentImportFields'][] = "社級";
}
if($sys_custom['StudentAccountAdditionalFields']){
	$Lang['AccountMgmt']['StudentImportFields'][] = "非中文語系";
	$Lang['AccountMgmt']['StudentImportFields'][] = "特殊教育需要";
	$Lang['AccountMgmt']['StudentImportFields'][] = "小學";
	$Lang['AccountMgmt']['StudentImportFields'][] = "大學/專上院校";
	$Lang['AccountMgmt']['StudentImportFields'][] = "專業課程";
}


$Lang['AccountMgmt']['EmailFieldReminder'] = "(當忘記密碼，用於取回個人密碼。)";
$Lang['AccountMgmt']['GenderFieldReminder'] = "(性別的格式為M 或 F。)";
$Lang['AccountMgmt']['DOBFieldReminder'] = "(日期的格式為YYYY-MM-DD 或 DD/MM/YYYY。)";
$Lang['AccountMgmt']['WebSAMSFieldReminder'] = "(請在WebSAMS 學生註冊編號前加上\"#\"號。)";
$Lang['AccountMgmt']['DuplicateStudentToParent'] = "學生已在此家長的名單內。";
$Lang['AccountMgmt']['StudentNotYetLinkToParent'] = "一位已選的學生未被加至此家長, 繼續？";
$Lang['AccountMgmt']['UserExtraInfo'] = "個人附加資料";
$Lang['AccountMgmt']['StaffCode'] = "WebSAMS 教職員代碼";
$Lang['AccountMgmt']['EmailUserLoginFieldReminder'] = "(如留空，會使用內聯網帳號作為電郵帳號名稱。)";

if($special_feature['signature_upload'])
{
	$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "建議使用 120px x 55px (闊 x 高)的相片";
	$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "只接受JPEG檔的相片";
	$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "可以以壓縮檔案(.zip)形式上載多個檔案";
	$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "壓縮檔案內不能存在文件夾";
	$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "相片的檔案名稱要與教職員的登入編號一樣";
	$Lang['AccountMgmt']['UploadSignature'] = "上載簽名";
	$Lang['AccountMgmt']['SignatureUpload']['WrongStaffLogin'] = "不是教職員登入編號";
}
if($special_feature['personal_photo_upload'])
{
	$Lang['AccountMgmt']['PersonalPhotoUpload']['RemarksList'][] = "建議使用 100px x 130px (闊 x 高)的相片";
	$Lang['AccountMgmt']['PersonalPhotoUpload']['RemarksList'][] = "只接受JPEG檔的相片";
	$Lang['AccountMgmt']['PersonalPhotoUpload']['RemarksList'][] = "可以以壓縮檔案(.zip)形式上載多個檔案";
	$Lang['AccountMgmt']['PersonalPhotoUpload']['RemarksList'][] = "壓縮檔案內不能存在文件夾";
	$Lang['AccountMgmt']['PersonalPhotoUpload']['RemarksList'][] = "相片的檔案名稱要與教職員的登入編號一樣";
	$Lang['AccountMgmt']['UploadPersonalPhoto'] = "個人相片上載";
	$Lang['AccountMgmt']['PersonalPhotoUpload']['WrongStaffLogin'] = "不是教職員登入編號";
}
$Lang['AccountMgmt']['StaffMgmt']['PersonalPhotoUpload'] = "個人相片上載";
$Lang['AccountMgmt']['StaffMgmt']['PersonalPhotoList'] = "個人相片列表";
$Lang['AccountMgmt']['StaffMgmt']['NotATeacher'] = "並非老師職位";
$Lang['AccountMgmt']['StaffMgmt']['TeacherName'] = '名稱';
$Lang['AccountMgmt']['StaffMgmt']['LoginID'] = '登入編號';
$Lang['AccountMgmt']['StaffMgmt']['Photo'] = '相片';
$Lang['AccountMgmt']['UserName'] = "名稱";
$Lang['AccountMgmt']['Signature'] = "簽名";
$Lang['AccountMgmt']['Uploaded'] = "已上載";
$Lang['AccountMgmt']['NotYetUploaded'] = "未上載";
$Lang['AccountMgmt']['ExportStudentInfo'] = "學生資料(英文姓名,班別,班號)";
$Lang['AccountMgmt']['LastYear'] = "原有年份";
$Lang['AccountMgmt']['LastClass'] = "原有班別";
$Lang['AccountMgmt']['UserExtraInfoMgmt'] = "個人附加資料設定";
$Lang['AccountMgmt']['Category'] = "類別";
$Lang['AccountMgmt']['Item'] = "項目";
$Lang['AccountMgmt']['AddItem'] = "新增項目";
$Lang['AccountMgmt']['MoveItem'] = "移動項目";
$Lang['AccountMgmt']['DeleteItem'] = "刪除項目";
$Lang['AccountMgmt']['WarnMsg']['ItemNameEn'] = "請輸入英文名稱。";
$Lang['AccountMgmt']['WarnMsg']['ItemNameCh'] = "請輸入中文名稱。";
$Lang['AccountMgmt']['EditCategory'] = "編輯類別";
$Lang['AccountMgmt']['UserExtraInfoCategoryCode'] = "代碼";
$Lang['AccountMgmt']['UserExtraInfoItemCode'] = "代碼";
$Lang['AccountMgmt']['MultiSelect'] = "多重選擇";
$Lang['AccountMgmt']['AddCategory'] = "新增類別";
$Lang['AccountMgmt']['MoveCategory'] = "移動類別";
$Lang['AccountMgmt']['DeleteCategory'] = "刪除類別";
$Lang['AccountMgmt']['EditMultiSelect'] = "編輯多重選擇";
$Lang['AccountMgmt']['WarnMsg']['InvalidCode'] = "代碼格式錯誤";
$Lang['AccountMgmt']['ReturnMessage']['CodeNotAvailable'] = " 0 |=|代碼已被使用。";
$Lang['AccountMgmt']['jsConfirm']['DeleteUserExtraInfoItem'] = "你確定要刪除這個項目嗎？";
$Lang['AccountMgmt']['CreateImailAccounts'] = "開設iMail戶口";
$Lang['AccountMgmt']['CreateIfolderAccounts'] = "開設iFolder戶口";
$Lang['AccountMgmt']['ImportStudentRemark'] = "你可使用匯入功能增設新用戶，或更新舊用戶資料。<p>
<b>增設新用戶：</b><br>如勾選以上選項，系統將自動為新用戶建立iMail/iFolder戶口。<p>
更新舊用戶資料：<br>請勿更改選項。要為現存用戶建立或變更iMail/iFolder口，請前往：<p>
行政管理中心 > 內聯網設定 > 功能設定 > iMail設定 > 個別使用權限及郵箱儲存量 > 檢視用戶儲存量<br>
行政管理中心 > 內聯網設定 > 功能設定 > iFolder設定 > iFolder儲存量 > 檢視用戶儲存量";

$Lang['AccountMgmt']['NoOfSelectedStudents'] = "已選擇學生數目";
$Lang['AccountMgmt']['YearOfLeft'] = "離校年份";
$Lang['AccountMgmt']['YearOfLeft_Error1'] = "年份錯誤 (例: 2011)";
$Lang['AccountMgmt']['YearOfLeft_Error2a'] = "年份必定為 1900年 至 ";
$Lang['AccountMgmt']['YearOfLeft_Error2b'] = "年之間";
$Lang['AccountMgmt']['ClickContinueToProceed'] = "請按繼續以啟動此程序. 程序進行需時, 請耐心等候.";
$Lang['AccountMgmt']['ToBeAlumni'] = "此程序啟動後, 將為已離校學生自動開啟校友戶口 (使用原來的登入帳號及密碼)";
$Lang['AccountMgmt']['JSWarning']['PleaseSelectUserAccount'] = "請選擇最少一個戶口";
$Lang['AccountMgmt']['Occupation'] = "職業";
$Lang['AccountMgmt']['Company'] = "工作機構";
$Lang['AccountMgmt']['PassportNo'] = "護照號碼";
$Lang['AccountMgmt']['PassportValidDate'] = "護照有效日期";
$Lang['AccountMgmt']['KnowAboutBlissWisdom'] = "得知福智途徑";
$Lang['AccountMgmt']['KnowAboutBlissWisdomOptions'] = array(1=>"朋友介紹", 2=>"印刷宣傳品", 3=>"網路宣傳", 4=>"悅意軒", 5=>"報章雜誌", 6=>"其他");
$Lang['AccountMgmt']['BlissWisdomFieldAry'] = array('Occupation', 'Company', 'PassportNo', 'Passport_ValidDate', 'HowToKnowBlissWisdom');
$Lang['AccountMgmt']['PasswordRemark'] = "( 請不要使用 \" ' 空格 $ & &lt; &gt; + \ )";
$Lang['AccountMgmt']['DeleteStudentWarning'] = "學生檔案及iPortfolio的資料將會在刪除學生戶口時被整存。用戶帳號將被永久移除，而且無法恢復。";
$Lang['AccountMgmt']['DeleteUserWarning'] = "用戶帳號將被永久移除，而且無法恢復。你是否確定要刪除項目?";
$Lang['AccountMgmt']['SmartCardID'] = "智能咭 ID";
$Lang['AccountMgmt']['SmartCardID2'] = "智能咭 ID 2";
$Lang['AccountMgmt']['SmartCardID3'] = "智能咭 ID 3";
$Lang['AccountMgmt']['SmartCardID4'] = "智能咭 ID 4";
$Lang['AccountMgmt']['IsDuplicatedInput'] = "重複輸入了。";

$Lang['AccountMgmt']['ExportForwardingEmails'] = "匯出轉寄電郵地址";
$Lang['AccountMgmt']['ImportForwardingEmails'] = "匯入轉寄電郵地址";
$Lang['AccountMgmt']['ImportStaffForwardingEmails'] = "匯入教職員用戶的轉寄電郵地址";
$Lang['AccountMgmt']['ImportStudentForwardingEmails'] = "匯入學生用戶的轉寄電郵地址";
$Lang['AccountMgmt']['ImportParentForwardingEmails'] = "匯入家長用戶的轉寄電郵地址";
$Lang['AccountMgmt']['ImportAlumniForwardingEmails'] = "匯入校友用戶的轉寄電郵地址";
$Lang['AccountMgmt']['UserEmailForwarding'] = "用戶郵件轉寄";
$Lang['AccountMgmt']['ForwardingEmails'] = "轉寄電郵地址";
$Lang['AccountMgmt']['KeepCopy'] = "保留郵件副本";
$Lang['AccountMgmt']['EmailForwardingCSVFields'] = array();
$Lang['AccountMgmt']['EmailForwardingCSVFields'][] = '<font color=red>*</font>Email <span class="tabletextremark">(iMail plus 電郵帳戶)</span><br />';
$Lang['AccountMgmt']['EmailForwardingCSVFields'][] = '<font color=red>^</font>English Name<br />';
$Lang['AccountMgmt']['EmailForwardingCSVFields'][] = '<font color=red>^</font>Chinese Name<br />';
$Lang['AccountMgmt']['EmailForwardingCSVFields'][] = '<font color=red></font>Forwarding Emails <span class="tabletextremark">(請使用豆號分隔電郵地址。留空設定為不轉寄。)</span><br />';
$Lang['AccountMgmt']['EmailForwardingCSVFields'][] = '<font color=red>*</font>Keep Copy <span class="tabletextremark">(1代表保留郵件副本，0代表不保留郵件副本。)</span><br />';
$Lang['AccountMgmt']['EmailForwardingErrors'] = array();
$Lang['AccountMgmt']['EmailForwardingErrors']['InvalidEmail'] = "電郵地址不正確";
$Lang['AccountMgmt']['EmailForwardingErrors']['UserEmailNotFound'] = "系統找不到該電郵用戶";
$Lang['AccountMgmt']['EmailForwardingErrors']['InvalidForwardingEmail'] = "轉寄電郵地址不正確";
$Lang['AccountMgmt']['EmailForwardingErrors']['DoNotForwardToSelfEmail'] = "請不要轉寄到自己的用戶電郵";
$Lang['AccountMgmt']['UnsettledLibraryOverduePaymentsWarning'] = "以下用戶戶口仍有圖書未還或圖書館罰款未處理。操作終止。";
$Lang['AccountMgmt']['BookUnreturned'] = "借出未還";
$Lang['AccountMgmt']['WifiUsage'] = "Wi-Fi 使用狀況";
$Lang['AccountMgmt']['EnableWifiAccess'] = "允許使用 Wi-Fi";
$Lang['AccountMgmt']['DisableWifiAccess'] = "不允許使用 Wi-Fi";
$Lang['AccountMgmt']['ExportWiFiAccessStatus'] = "匯出 Wi-Fi 使用狀況列表";
$Lang['AccountMgmt']['NonChineseSpeaking'] = "非中文語系";
$Lang['AccountMgmt']['SpecialEducationNeeds'] = "特殊教育需要";
$Lang['AccountMgmt']['PrimarySchool'] = "小學";
$Lang['AccountMgmt']['PrimarySchoolCode'] = "小學編號";
$Lang['AccountMgmt']['UniversityInstitution'] = "大學/專上院校";
$Lang['AccountMgmt']['Programme'] = "專業課程";
$Lang['AccountMgmt']['UniversityProgrammeRemark'] = "請嚴格採用 JUPAS 所定標準用語。";
$Lang['AccountMgmt']['StudentAccount']['HouseFilter']['SelectHouse']='選擇社級';
$Lang['AccountMgmt']['StudentAccount']['HouseFilter']['AllHouse']='所有社級';
$Lang['AccountMgmt']['StudentAccount']['HouseFilter']['WithoutHouse']='無所屬社級';
$Lang['AccountMgmt']['ActionType'] = "行動類型";
$Lang['AccountMgmt']['ActionType.Add'] = "新增";
$Lang['AccountMgmt']['ActionType.Update'] = "更新";
$Lang['AccountMgmt']['CreateUserInfo'] = "建立紀錄";
$Lang['AccountMgmt']['CreatedAndModifiedRecords'] = "建立及修改紀錄";
$Lang['AccountMgmt']['CreateUserID'] = "建立紀錄用戶ID";
$Lang['AccountMgmt']['CreateUserName'] = "建立紀錄用戶名稱";
$Lang['AccountMgmt']['CreateUserDate'] = "建立紀錄時間";
$Lang['AccountMgmt']['LastEditUserName'] = "最後修改用戶";
$Lang['AccountMgmt']['LastEditUserDate'] = "最後修改日期";
$Lang['AccountMgmt']['ArchivedRecords'] = "整存紀錄";
$Lang['AccountMgmt']['ArchivedStudent'] = "已整存學生";
$Lang['AccountMgmt']['DateOfArchive'] = "整存日期";
$Lang['AccountMgmt']['ArchivedBy'] = "整存者";
$Lang['AccountMgmt']['NameOfCompany/Organization'] = "公司 / 機構名稱";
$Lang['AccountMgmt']['HouseholdRegister'] = "戶口";
$Lang['AccountMgmt']['House'] = "社級";
$Lang['AccountMgmt']['DisplayPriority'] = "顯示優先級";
$Lang['AccountMgmt']['DisplayPriorityRemark'] = "較小的值表示更高的優先級";
$Lang['AccountMgmt']['PrintQRCode'] = "列印QR Code";
$Lang['AccountMgmt']['ArchivedStudentImportFields'] = array();
$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('UserLogin','內聯網帳號','<span style="color:red">*</span>','');
$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('ClassName','班別','','<span class="tabletextremark"> (此資料只供參考, 並不會存入紀錄)</span>');
$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('ClassNumber','學號','','<span class="tabletextremark"> (此資料只供參考, 並不會存入紀錄)</span>');
if($special_feature['ava_strn']){
	$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('STRN','STRN 學生編號','','');
}
if($_SESSION["platform"]!="KIS"){
	$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('WebSAMSRegNo','WebSAMS 學生註冊編號','','<span class="tabletextremark"> (請在WebSAMS 學生註冊編號前加上"#"號。)</span>');
	$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('HKJApplNo','HK JUPAS Application Number','','');
}
if($sys_custom['AccountMgmt']['StudentDOBRequired']){
    $DOBRequired = '<span style="color:red">*</span>';
}
$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('DOB','出生日期',"$DOBRequired",'<span class="tabletextremark"> (日期的格式為YYYY-MM-DD 或 DD/MM/YYYY。)</span>');
if($special_feature['ava_hkid']){
	$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('HKID','身份証號碼','','');
}
$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('YearOfLeft','離校年份','','<span class="tabletextremark"> (此資料只供參考, 並不會存入紀錄)</span>');
# export with photo [K130888 - 保良局香港道教聯合會圓玄小學]
$Lang['AccountMgmt']['StudentWithPhotoList'] = "下載相片";
$Lang['AccountMgmt']['StudentWithoutPhotoList']= "匯出沒有相片";
$Lang['AccountMgmt']['PasswordRequirements'] = array();
$Lang['AccountMgmt']['PasswordRequirements'][] = '密碼不能使用這些特殊字符： " \' 空格 $ & < > + \\。';
$Lang['AccountMgmt']['PasswordRequirements'][] = '密碼必須至少有<!--PASSWORD_LENGTH-->個字符。';
$Lang['AccountMgmt']['PasswordRequirements'][] = '密碼不能太簡單。 （例如，與登錄ID相同或使用容易猜到的常用詞）。';
$Lang['AccountMgmt']['PasswordRequirements'][] = '密碼必須包含字母和數字。';
$Lang['AccountMgmt']['PasswordCheckingWarnings'] = array();
$Lang['AccountMgmt']['PasswordCheckingWarnings']['-1'] = '密碼不符合要求的最小長度。';
$Lang['AccountMgmt']['PasswordCheckingWarnings']['-2'] = '密碼包含一個或多個限製字符： " \' 空格 $ & < > + \\。';
$Lang['AccountMgmt']['PasswordCheckingWarnings']['-3'] = '密碼過於簡單。';
$Lang['AccountMgmt']['PasswordCheckingWarnings']['-4'] = '密碼必須包含字母和數字。';
$Lang['AccountMgmt']['PrincipalMgmt'] = "特別身份配對";
$Lang['AccountMgmt']['SpecialRole'] = "特別身份";
$Lang['AccountMgmt']['SpecialRolePrincipal'] = "校長";
$Lang['AccountMgmt']['SpecialRoleVicePrincipal'] = "副校長";
$Lang['AccountMgmt']['SpecialRoleChancellor'] = "校監";
$Lang['AccountMgmt']['DeleteStaffAcctWarning']['ChangeSENCasePIC'] = '此用戶為輔導管理中SEN機密個案的負責老師或授權人士。必須於相關個案移除此負責老師或授權人士，方可刪除此用戶。';

$Lang['StudentMgmt']['ExportForAlumni'] = '匯出至 eAlumni';
$Lang['StudentMgmt']['MayNotImportToAlumniDirectly'] = '因缺少自訂資料項目，及缺少部份必須填寫之資料，匯出之檔案可能無法直接匯入至 eAlumni';
$Lang['StudentMgmt']['ExportSpecificYear'] = '匯出指定年份';
$Lang['StudentMgmt']['ExportAll'] = '匯出全部';
$Lang['StudentMgmt']['EmptyYearRecords'] = '欠缺年份記錄';

# Student Registry
$Lang['StudentRegistry']['ClassList'] = "班別列表";
$Lang['StudentRegistry']['StudentList'] = "學生列表";
$Lang['StudentRegistry']['Class'] = "班別";
$Lang['StudentRegistry']['NoOfStudents'] = "學生人數";
$Lang['StudentRegistry']['AcademicYear'] = "學年";
$Lang['StudentRegistry']['AllRecordStatus'] = "所有狀況";
$Lang['StudentRegistry']['Normal'] = "正常記錄";
$Lang['StudentRegistry']['WaitForApproval'] = "等待批核更新人數";
$Lang['StudentRegistry']['RecordStatusArray'] = array(0=>$Lang['StudentRegistry']['WaitForApproval'], 1=>$Lang['StudentRegistry']['Normal']);
$Lang['StudentRegistry']['AllRegistryStatus'] = "所有學籍狀況";
$Lang['StudentRegistry']['RegistryStatus'] = "學籍狀況";
$Lang['StudentRegistry']['StatusApproved'] = "在籍";
$Lang['StudentRegistry']['StatusSuspended'] = "休學";
$Lang['StudentRegistry']['StatusResume'] = "復學";
$Lang['StudentRegistry']['StatusLeft'] = "退學";
$Lang['StudentRegistry']['RegistryStatusArray'] = array(0=>$Lang['StudentRegistry']['StatusSuspended'], 1=>$Lang['StudentRegistry']['StatusApproved'], 3=>$Lang['StudentRegistry']['StatusLeft']);
$Lang['StudentRegistry']['Total'] = "總數";
$Lang['StudentRegistry']['ClassNumber'] = "座號";
$Lang['StudentRegistry']['ChineseName'] = "中文姓名";
$Lang['StudentRegistry']['EnglishName'] = "英文姓名";
$Lang['StudentRegistry']['ForeignName'] = "外文姓名";
$Lang['StudentRegistry']['WebSAMSRegNo'] = "WebSAMS 學生註冊編號";
$i_WebSAMS_Registration_No = "WebSAMS 學生註冊編號";
$i_WebSAMSRegNo_Format_Notice="(\"<b>#</b>\" 號是WebSAMS 學生註冊編號格式的一部份)";
$Lang['StudentRegistry']['DSEJ_Number'] = "教青局學生編號";
$Lang['StudentRegistry']['LastUpdated'] = "最後更新日期 (更新者)";
$Lang['StudentRegistry']['ApproveStatus'] = "批核更新狀態 (批核者)";
$Lang['StudentRegistry']['Approved'] = "已批核";
$Lang['StudentRegistry']['ApprovedBySystem'] = "系統";
$Lang['StudentRegistry']['CSV'] = "CSV";
$Lang['StudentRegistry']['XML'] = "XML - 學年網上註冊系統(教青)[方案三和五]";
$Lang['StudentRegistry']['XML2'] = "XML - 學年網上註冊系統(教青)[方案二]";
$Lang['StudentRegistry']['PageName1'] = "第";
$Lang['StudentRegistry']['TotalRecord'] = "合共";
$Lang['StudentRegistry']['UserList'] = "成員列名單";
$Lang['StudentRegistry']['LastSubmitted'] = "最後呈交日期 (呈交者)";
$Lang['StudentRegistry']['Submitted'] = "已呈交";
$Lang['StudentRegistry']['NonSubmitted'] = "未呈交";

# Student Registry (for student details - Common)
$Lang['StudentRegistry']['BasicInfo'] = "基本資料";
$Lang['StudentRegistry']['AdvInfo'] = "進階資料";
$Lang['StudentRegistry']['StudInfo'] = "學生資料";
$Lang['StudentRegistry']['FatherInfo'] = "父親資料";
$Lang['StudentRegistry']['MotherInfo'] = "母親資料";
$Lang['StudentRegistry']['GuardianInfo'] = "監護人資料";
$Lang['StudentRegistry']['ContactPersonInfo'] = "聯絡人資料";
$Lang['StudentRegistry']['EmergencyContactInfo'] = "緊急聯絡人資料";

$Lang['StudentRegistry']['Save'] = "儲存";
$Lang['StudentRegistry']['Cancel'] = "取消";
$Lang['StudentRegistry']['Edit'] = "編輯";
$Lang['StudentRegistry']['Hide'] = "隱藏";
$Lang['StudentRegistry']['Show'] = "展開";
$Lang['StudentRegistry']['Other'] = "其他";
$Lang['StudentRegistry']['EditInfo'] = "編輯資料";
$Lang['StudentRegistry']['Mandatory'] = "必須填寫項目";
$Lang['StudentRegistry']['PleaseEnter'] = "請輸入";
$Lang['StudentRegistry']['NoInformation'] = "沒有資料";

//$Lang['StudentRegistry']['ChineseName'] = "中文姓名";
//$Lang['StudentRegistry']['EnglishName'] = "英文姓名";
$Lang['StudentRegistry']['EntryDate'] = "入學日期";
$Lang['StudentRegistry']['StudyAt'] = "就讀";
$Lang['StudentRegistry']['Grade'] = "年級";
//$Lang['StudentRegistry']['Class'] = "班別";
$Lang['StudentRegistry']['InClassNumber'] = "班內號";
$Lang['StudentRegistry']['Gender'] = "性別";
$Lang['StudentRegistry']['BirthDate'] = "出生日期";
$Lang['StudentRegistry']['BirthPlace'] = "出生地點";
$Lang['StudentRegistry']['Nationality'] = "國籍";
$Lang['StudentRegistry']['HomePhone'] = "住家電話";
$Lang['StudentRegistry']['CellPhone'] = "流動電話";
$Lang['StudentRegistry']['PastEnroll'] = "過去班級";
$Lang['StudentRegistry']['Relationship'] = "與學生關係";
$Lang['StudentRegistry']['RelationOther'] = "與學生關係[其他]";
$Lang['StudentRegistry']['OtherGuardian'] = "其他監護人";
$Lang['StudentRegistry']['ByWho'] = "資料來源";

# Student Registry (for student details - Macau)
$Lang['StudentRegistry']['StudentID'] = "學校內學生代號";
$Lang['StudentRegistry']['PortugueseName'] = "葡文姓名";
$Lang['StudentRegistry']['Province'] = "籍貫";
$Lang['StudentRegistry']['SchoolCode'] = "校部編號";

$Lang['StudentRegistry']['IdCard'] = "身份證明文件";
$Lang['StudentRegistry']['StayPermission'] = "逗留許可";
$Lang['StudentRegistry']['CardType'] = "種類";
$Lang['StudentRegistry']['CardNumber'] = "編號";
$Lang['StudentRegistry']['CardIssuePlace'] = "發出地點";
$Lang['StudentRegistry']['CardIssueDate'] = "本次發出日期";
$Lang['StudentRegistry']['CardValidDate'] = "有效日期";

$Lang['StudentRegistry']['NightRes'] = "夜間住宿";
$Lang['StudentRegistry']['Area'] = "地區";
$Lang['StudentRegistry']['DistrictName'] = "地區之名稱";
$Lang['StudentRegistry']['HomeDistrict'] = "居住地區";
$Lang['StudentRegistry']['Address'] = "住址";
$Lang['StudentRegistry']['Street'] = "街名";
$Lang['StudentRegistry']['AddDetail'] = "門牌、大廈、層數、座";

$Lang['StudentRegistry']['Job'] = "職業";
$Lang['StudentRegistry']['StayTogether'] = "與學生同住";
$Lang['StudentRegistry']['Phone'] = "電話號碼";

# Student Registry (for student details - Malaysia)
$Lang['StudentRegistry']['Mal_StudentNo'] = "學號";
$Lang['StudentRegistry']['BirthCertNo'] = "報生紙號碼";
$Lang['StudentRegistry']['Race'] = "種族";
$Lang['StudentRegistry']['AncestalHome'] = "祖籍";
$Lang['StudentRegistry']['Religion'] = "宗教";
$Lang['StudentRegistry']['PayType'] = "繳費類別";
$Lang['StudentRegistry']['Email'] = "電子郵件";
$Lang['StudentRegistry']['FamilyLang'] = "家庭用語";
$Lang['StudentRegistry']['HousingState'] = "住宿情況";
$Lang['StudentRegistry']['HomeAddress'] = "居住地址";
$Lang['StudentRegistry']['ContactAddress'] = "通訊地址";
$Lang['StudentRegistry']['Door'] = "門牌號碼";
$Lang['StudentRegistry']['Road'] = "路名";
$Lang['StudentRegistry']['Garden'] = "花園";
$Lang['StudentRegistry']['Town'] = "市填區";
$Lang['StudentRegistry']['State'] = "州屬";
$Lang['StudentRegistry']['Postal'] = "郵區號碼";
$Lang['StudentRegistry']['PastPrimarySch'] = "小學就讀學校";
$Lang['StudentRegistry']['Siblings'] = "目前就讀之兄姐";
$Lang['StudentRegistry']['ContactPerson'] = "聯絡人";
$Lang['StudentRegistry']['EducationLevel'] = "教育程度";
$Lang['StudentRegistry']['JobNature'] = "職業性質";
$Lang['StudentRegistry']['JobTitle'] = "職稱";
$Lang['StudentRegistry']['OfficeNumber'] = "辦公電話";
$Lang['StudentRegistry']['MaritalStatus'] = "婚姻狀況";
$Lang['StudentRegistry']['SearchSiblings'] = "請輸入學號";
$Lang['StudentRegistry']['Search'] = "搜尋";
$Lang['StudentRegistry']['Add'] = "增加";
$Lang['StudentRegistry']['Remove'] = "刪除";
$Lang['StudentRegistry']['NoRecord'] = "沒有紀錄";

# export column name (Macau)
$Lang['StudentRegistry']['CSV_field'][0] = array("STUD_ID", "CODE", "NAME_C", "NAME_P", "SEX", "B_DATE", "B_PLACE", "ID_TYPE", "ID_NO", "I_PLACE", "I_DATE", "V_DATE", "S6_TYPE", "S6_IDATE", "S6_VDATE", "NATION", "ORIGIN", "TEL", "R_AREA", "RA_DESC", "AREA", "ROAD", "ADDRESS", "FATHER", "MOTHER", "F_PROF", "M_PROF", "GUARD", "LIVE_SAME", "EC_NAME", "EC_REL", "EC_TEL", "EC_AREA", "EC_ROAD", "EC_ADDRESS", "S_CODE", "GRADE", "CLASS", "C_NO", "G_NAME", "G_RELATION", "G_PROFESSION", "G_AREA", "G_ROAD", "G_ADDRESS", "G_TEL", "GUARDMOBILE");
$Lang['StudentRegistry']['CSV_field'][1] = array("學校內學生代號", "教青局學生編號", "中文姓名", "葡文姓名", "性別", "出生日期", "出生地點", "身份證明文件種類", "身份證號碼", "身份證明文件發出地點", "身份證明文件本次發出日期", "身份證明文件有效日期", "逗留許可", "逗留許可本次發出日期", "逗留許可有效日期", "國籍", "籍貫", "電話", "夜間住宿地區", "夜間住宿地區之名稱", "居住地區", "住址街名", "住址", "父親姓名", "母親姓名", "父親職業", "母親職業", "與監護人關係", "與監護人同住", "緊急聯絡人姓名", "緊急聯絡人關係", "緊急聯絡人電話", "緊急聯絡人居住地區", "緊急聯絡人住址街名", "緊急聯絡人住址", "校部編號", "年級", "班別", "班號", "監護人姓名", "監護人與學生關係", "監護人職業", "監護人居住地區", "監護人住址街名", "監護人住址", "監護人電話", "監護人流動電話");
/*
 $Lang['StudentRegistry']['Macau_CSV'][0] = array("Class Name", "Class Number", "Login Name", "Student ID", "DSEJ Code", "Chinese Name", "Foreign Name", "Gender", "Date of Birth", "Birth Place", "Nationality", "Origin", "Religion", "School Code", "Date of Entry", "ID Card Type", "ID Card No.", "ID Issue Place", "ID Issue Date", "ID Valid Date", "Staying Permit Type", "Staying Permit Issue Date", "Staying Permit Valid Date", "Phone No. (Home)", "Email", "Residential Area", "Name of Residential District", "Address – (Home District)", "Address – (Street)", "Address – (Door, Bldg, Floor, Block)", "Father Chinese Name", "Father Foreign Name", "Father Job Occupation", "Father Marital Status", "Father Phone No.(Home)", "Father Phone No.(Mobile)", "Father Phone No.(Office)", "Father Email", "Mother Chinese Name", "Mother Foreign Name", "Mother Job Occupation", "Mother Marital Status", "Mother Phone No.(Home)", "Mother Phone No.(Mobile)", "Mother Phone No.(Office)", "Mother Email", "Guardian Chinese Name", "Guardian Foreign Name", "Guardian Gender", "Guardian Relationship with Student", "Guardian Relationship with Student (Other)", "Guardian Live with Student", "Guardian Job Occupation", "Guardian Phone No.(Home)", "Guardian Phone No.(Mobile)", "Guardian Phone No.(Office)", "Guardian Address – (Home District)", "Guardian Address – (Street)", "Guardian Address – (Door, Bldg, Floor, Block)", "Guardian Email", "Emergency Contact Person Chinese Name", "Emergency Contact Person Foreign Name", "Emergency Contact Person Relationship with Student", "Emergency Contact Person Phone No.(Home)", "Emergency Contact Person Phone No.(Mobile)", "Emergency Contact Person Phone No.(Office)", "Emergency Address (Home District)", "Emergency Address (Street)", "Emergency Address (Door, Bldg, Floor, Block)");
 $Lang['StudentRegistry']['Macau_CSV'][1] = array("班別", "班內號", "學生內聯網帳號", "學校內學生代號 ", "教青局學生編號", "中文姓名", "外文姓名", "性別", "出生日期", "出生地點", "國籍", "籍貫", "宗教", "校部編號", "入學日期", "身份證明文件種類", "身份證明文件編號", "身份證明文件發出地點", "身份證明文件本次發出日期", "身份證明文件有效日期", "逗留許可", "逗留許可本次發出日期", "逗留許可有效日期", "電話號碼", "電子郵件", "夜間住宿地區", "夜間住宿地區(地區之名稱)", "居住地區", "住址(街名)", "住址(門牌、大廈、層數、座)", "父親中文姓名", "父親外文姓名", "父親職業", "父親婚姻狀況", "父親住宅電話", "父親手提電話", "父親辦公電話", "父親電子郵件", "母親中文姓名", "母親外文姓名", "母親職業", "母親婚姻狀況", "母親住宅電話", "母親手提電話", "母親辦公電話", "母親電子郵件", "監護人中文姓名", "監護人外文姓名", "監護人性別", "監護人與學生關係", "監護人與學生關係(其他)", "監護人與學生同住", "監護人職業", "監護人住宅電話", "監護人手提電話", "監護人辦公電話", "監護人居住地區", "監護人住址(街名)", "監護人住址(門牌、大廈、層數、座)", "監護人電子郵件", "緊急聯絡人中文姓名", "緊急聯絡人外文姓名", "與緊急聯絡人關係", "緊急聯絡人電話", "緊急聯絡人流動電話", "緊急聯絡人居住地區", "緊急聯絡人住址(地區之名稱)", "緊急聯絡人住址(街名)", "緊急聯絡人住址(門牌、大廈、層數、座)");
 */
$Lang['StudentRegistry']['Macau_CSV'][0] = array("Class Name", "Class Number", "Login Name", "Student ID", "DSEJ Code", "Chinese Name", "Foreign Name", "Gender", "Date of Birth", "Birth Place", "Nationality", "Origin", "School Code", "Date of Entry", "ID Card Type", "ID Card No.", "ID Issue Place", "ID Issue Date", "ID Valid Date", "Staying Permit Issue Date", "Staying Permit Valid Date", "Phone No. (Home)", "Email", "Address – (Street)", "Address – (Door, Bldg, Floor, Block)", "Father Chinese Name", "Father Foreign Name", "Father Job Occupation", "Father Marital Status", "Father Phone No.(Home)", "Father Phone No.(Mobile)", "Father Phone No.(Office)", "Father Email", "Mother Chinese Name", "Mother Foreign Name", "Mother Job Occupation", "Mother Marital Status", "Mother Phone No.(Home)", "Mother Phone No.(Mobile)", "Mother Phone No.(Office)", "Mother Email", "Guardian Chinese Name", "Guardian Foreign Name", "Guardian Gender", "Guardian Relationship with Student", "Guardian Relationship with Student (Other)", "Guardian Live with Student", "Guardian Job Occupation", "Guardian Phone No.(Home)", "Guardian Phone No.(Mobile)", "Guardian Phone No.(Office)", "Guardian Address – (Street)", "Guardian Address – (Door, Bldg, Floor, Block)", "Guardian Email", "Emergency Contact Person Chinese Name", "Emergency Contact Person Foreign Name", "Emergency Contact Person Relationship with Student", "Emergency Contact Person Phone No.(Home)", "Emergency Contact Person Phone No.(Mobile)", "Emergency Contact Person Phone No.(Office)", "Emergency Address (Street)", "Emergency Address (Door, Bldg, Floor, Block)");
$Lang['StudentRegistry']['Macau_CSV'][1] = array("班別", "班內號", "學生內聯網帳號", "學校內學生代號 ", "教青局學生編號", "中文姓名", "外文姓名", "性別", "出生日期", "出生地點", "國籍", "籍貫", "校部編號", "入學日期", "身份證明文件種類", "身份證明文件編號", "身份證明文件發出地點", "身份證明文件本次發出日期", "身份證明文件有效日期", "逗留許可本次發出日期", "逗留許可有效日期", "電話號碼", "電子郵件", "住址(街名)", "住址(門牌、大廈、層數、座)", "父親中文姓名", "父親外文姓名", "父親職業", "父親婚姻狀況", "父親住宅電話", "父親手提電話", "父親辦公電話", "父親電子郵件", "母親中文姓名", "母親外文姓名", "母親職業", "母親婚姻狀況", "母親住宅電話", "母親手提電話", "母親辦公電話", "母親電子郵件", "監護人中文姓名", "監護人外文姓名", "監護人性別", "監護人與學生關係", "監護人與學生關係(其他)", "監護人與學生同住", "監護人職業", "監護人住宅電話", "監護人手提電話", "監護人辦公電話", "監護人住址(街名)", "監護人住址(門牌、大廈、層數、座)", "監護人電子郵件", "緊急聯絡人中文姓名", "緊急聯絡人外文姓名", "與緊急聯絡人關係", "緊急聯絡人電話", "緊急聯絡人流動電話", "緊急聯絡人辦公電話", "緊急聯絡人住址(街名)", "緊急聯絡人住址(門牌、大廈、層數、座)");

$Lang['StudentRegistry']['Relation'] = array("F"=>"Father", "M"=>"Mother", "G"=>"Guardian", "E"=>"Emergency Contact", "O"=>"Others");

# import column name (Macau)
//$Lang['StudentRegistry']['Import_Macau'] = array("班別", "班內號", "學生內聯網帳號", "學校內學生代號", "教青局學生編號", "中文姓名", "外文姓名", "入學日期", "校部編號", "性別", "出生日期", "出生地點", "身份證明文件種類", "身份證明文件編號", "身份證明文件發出地點", "身份證明文件本次發出日期", "身份證明文件有效日期", "逗留許可", "逗留許可本次發出日期", "逗留許可有效日期", "國籍", "籍貫", "宗教", "電子郵件", "電話號碼", "夜間住宿地區", "夜間住宿地區(補充)", "居住地區", "住址(街名)", "住址(門牌、大廈、層數、座)", "父親中文姓名", "父親外文姓名", "父親職業", "父親婚姻狀況", "父親住宅電話", "父親手提電話", "父親辦公電話", "父親電子郵件", "母親中文姓名", "母親外文姓名", "母親職業", "母親婚姻狀況", "母親住宅電話", "母親手提電話", "母親辦公電話", "母親電子郵件", "監護人中文姓名", "監護人外文姓名", "監護人性別", "監護人職業", "與學生關係", "與學生關係(其他)", "與學生同住", "監護人居住地區", "監護人住址(街名)", "監護人住址(門牌、大廈、層數、座)", "監護人住宅電話", "監護人手提電話", "監護人辦公電話", "監護人電子郵件", "緊急聯絡人中文姓名", "緊急聯絡人外文姓名", "與緊急聯絡人關係", "緊急聯絡人電話", "緊急聯絡人流動電話", "緊急聯絡人居住地區", "緊急聯絡人住址(街名)", "緊急聯絡人住址(門牌, 大廈, 層數, 座)");
//$Lang['StudentRegistry']['Import_Macau'] = array("班別", "班內號", "學生內聯網帳號", "學校內學生代號 ", "教青局學生編號", "中文姓名", "外文姓名", "性別", "出生日期", "出生地點", "國籍", "籍貫", "宗教", "校部編號", "入學日期", "身份證明文件種類", "身份證明文件編號", "身份證明文件發出地點", "身份證明文件本次發出日期", "身份證明文件有效日期", "逗留許可", "逗留許可本次發出日期", "逗留許可有效日期", "電話號碼", "電子郵件", "夜間住宿地區", "夜間住宿地區(地區之名稱)", "居住地區", "住址(街名)", "住址(門牌、大廈、層數、座)", "父親中文姓名", "父親外文姓名", "父親職業", "父親婚姻狀況", "父親住宅電話", "父親手提電話", "父親辦公電話", "父親電子郵件", "母親中文姓名", "母親外文姓名", "母親職業", "母親婚姻狀況", "母親住宅電話", "母親手提電話", "母親辦公電話", "母親電子郵件", "監護人中文姓名", "監護人外文姓名", "監護人性別", "監護人與學生關係", "監護人與學生關係(其他)", "監護人與學生同住", "監護人職業", "監護人住宅電話", "監護人手提電話", "監護人辦公電話", "監護人居住地區", "監護人住址(街名)", "監護人住址(門牌、大廈、層數、座)", "監護人電子郵件", "緊急聯絡人中文姓名", "緊急聯絡人外文姓名", "與緊急聯絡人關係", "緊急聯絡人電話", "緊急聯絡人流動電話", "緊急聯絡人居住地區", "緊急聯絡人住址(地區之名稱)", "緊急聯絡人住址(街名)", "緊急聯絡人住址(門牌、大廈、層數、座)");
$Lang['StudentRegistry']['Import_Macau'] = array("班別", "班內號", "學生內聯網帳號", "學校內學生代號 ", "教青局學生編號", "中文姓名", "外文姓名", "性別", "出生日期", "出生地點", "國籍", "籍貫", "校部編號", "入學日期", "身份證明文件種類", "身份證明文件編號", "身份證明文件發出地點", "身份證明文件本次發出日期", "身份證明文件有效日期", "逗留許可本次發出日期", "逗留許可有效日期", "電話號碼", "電子郵件", "住址(街名)", "住址(門牌、大廈、層數、座)", "父親中文姓名", "父親外文姓名", "父親職業", "父親婚姻狀況", "父親住宅電話", "父親手提電話", "父親辦公電話", "父親電子郵件", "母親中文姓名", "母親外文姓名", "母親職業", "母親婚姻狀況", "母親住宅電話", "母親手提電話", "母親辦公電話", "母親電子郵件", "監護人中文姓名", "監護人外文姓名", "監護人性別", "監護人與學生關係", "監護人與學生關係(其他)", "監護人與學生同住", "監護人職業", "監護人住宅電話", "監護人手提電話", "監護人辦公電話", "監護人住址(街名)", "監護人住址(門牌、大廈、層數、座)", "監護人電子郵件", "緊急聯絡人中文姓名", "緊急聯絡人外文姓名", "與緊急聯絡人關係", "緊急聯絡人電話", "緊急聯絡人流動電話", "緊急聯絡人辦公電話", "緊急聯絡人住址(街名)", "緊急聯絡人住址(門牌、大廈、層數、座)");

# export column name (Malaysia)
$Lang['StudentRegistry']['Malaysia_CSV'][0] = array("Class Name", "Class No.", "Login Name", "Student No.", "Chinese Name", "English Name", "Gender", "Date of Birth", "Birth Place", "Nationality", "Ancestral Home", "Race", "ID Card No.", "Birth Cert. No.", "Religion", "Family Language", "Payment Type", "Phone No.(Home)", "Email", "Housing Status", "Home Address – (Door)", "Home Address – (Road)", "Home Address – (Garden)", "Home Address – (Town)", "Home Address – (State)", "Home Address – (Postal Code)", "Date of Entry", "Elder Brother/Sister in School", "Primary School", "Father Chinese Name", "Father English Name", "Father Job Occupation", "Father Job Nature", "Father Job Title", "Father Marital Status", "Father Level of Education", "Father Phone No.(Home)", "Father Phone No.(Mobile)", "Father Phone No.(Office)", "Father Contact Address – (Door)", "Father Contact Address – (Road)", "Father Contact Address – (Garden)", "Father Contact Address – (Town)", "Father Contact Address – (State)", "Father Contact Address – (Postal Code)", "Father Postal Code", "Father Email", "Mother Chinese Name", "Mother English Name", "Mother Job Occupation", "Mother Job Nature", "Mother Job Title", "Mother Marital Status", "Mother Level of Education", "Mother Phone No.(Home)", "Mother Phone No.(Mobile)", "Mother Phone No.(Office)", "Mother Contact Address – (Door)", "Mother Contact Address – (Road)", "Mother Contact Address – (Garden)", "Mother Contact Address – (Town)", "Mother Contact Address – (State)", "Mother Contact Address – (Postal Code)", "Mother Postal Code", "Mother Email", "Guardian Chinese Name", "Guardian English Name", "Guardian Gender", "Guardian Relationship", "Guardian Relationship (Other)", "Guardian Live with Student", "Guardian Job Occupation", "Guardian Job Nature", "Guardian Job Title", "Guardian Level of Education", "Guardian Phone No.(Home)", "Guardian Phone No.(Mobile)", "Guardian Phone No.(Office)", "Guardian Contact Address – (Door)", "Guardian Contact Address – (Road)", "Guardian Contact Address – (Garden)", "Guardian Contact Address – (Town)", "Guardian Contact Address – (State)", "Guardian Contact Address – (Postal Code)", "Guardian Postal Code", "Guardian Email");
$Lang['StudentRegistry']['Malaysia_CSV'][1] = array("班別", "班內號", "學生內聯網帳號", "學號", "中文姓名", "英文姓名", "性別", "出生日期", "出生地點", "國籍", "祖籍", "種族", "身份證明文件編號", "報生紙號碼", "宗教", "家庭用語", "繳費類別", "住家電話", "電子郵件", "住宿情況", "居住地址 – 門牌號碼", "居住地址 – 路名", "居住地址 – 花園", "居住地址 – 市填區", "居住地址 – 州屬", "居住地址 – 郵區號碼", "入學日期", "目前就讀之兄姐 ", "小學就讀學校", "父親中文姓名", "父親英文姓名", "父親職業", "父親職業性質", "父親職稱", "父親婚姻狀況", "父親教育程度", "父親住家電話", "父親流動電話", "父親辦公電話", "父親通訊地址 – 門牌號碼", "父親通訊地址 – 路名", "父親通訊地址 – 花園", "父親通訊地址 – 市填區", "父親通訊地址 – 州屬", "父親通訊地址 – 郵區號碼", "父親郵區號碼", "父親電子郵件", "母親中文姓名", "母親英文姓名", "母親職業", "母親職業性質", "母親職稱", "母親婚姻狀況", "母親教育程度", "母親住家電話", "母親流動電話", "母親辦公電話", "母親通訊地址 – 門牌號碼", "母親通訊地址 – 路名", "母親通訊地址 – 花園", "母親通訊地址 – 市填區", "母親通訊地址 – 州屬", "母親通訊地址 – 郵區號碼", "母親郵區號碼", "母親電子郵件", "監護人中文姓名", "監護人英文姓名", "監護人性別", "監護人與學生關係", "監護人與學生關係(其他)", "監護人與學生同住", "監護人職業", "監護人職業性質", "監護人職稱", "監護人教育程度", "監護人住家電話", "監護人流動電話", "監護人辦公電話", "監護人通訊地址 – 門牌號碼", "監護人通訊地址 – 路名", "監護人通訊地址 – 花園", "監護人通訊地址 – 市填區", "監護人通訊地址 – 州屬", "監護人通訊地址 – 郵區號碼", "監護人郵區號碼", "監護人電子郵件");

# import column name (Malaysia)
//$Lang['StudentRegistry']['Import_Malaysia'] = array("班別", "班內號", "學生內聯網帳號", "中文姓名", "英文姓名", "入學日期", "報生紙號碼", "性別", "出生日期", "出生地點", "身份證號碼", "種族", "祖籍", "國籍", "宗教", "繳費類別", "電子郵件", "家庭用語", "住宿情況", "居住地址 (門牌號碼)", "居住地址 (路名)", "居住地址 (花園)", "居住地址 (市填區)", "居住地址 (州屬)", "居住地址 (郵區號碼)", "小學就讀學校", "目前在校就讀之兄姐 (最高年級者)", "電話號碼", "父親中文姓名", "父親英文姓名", "父親職業", "父親職業性質", "父親職稱", "父親婚姻狀汜", "父親住宅電話", "父親手提電話", "父親辦公電話", "父親通訊地址 (門牌號碼)", "父親通訊地址 (路名)", "父親通訊地址 (花園)", "父親通訊地址 (市填區)", "父親通訊地址 (州屬)", "父親通訊地址 (郵區號碼)", "父親郵區號碼", "父親電子郵件", "父親教育程度", "母親中文姓名", "母親英文姓名", "母親職業", "母親職業性質", "母親職稱", "母親婚姻狀況", "母親住宅電話", "母親手提電話", "母親辦公電話", "母親通訊地址 (門牌號碼)", "母親通訊地址 (路名)", "母親通訊地址 (花園)", "母親通訊地址 (市填區)", "母親通訊地址 (州屬)", "母親通訊地址 (郵區號碼)", "母親郵區號碼", "母親電子郵件", "母親教育程度", "監護人中文姓名", "監護人英文姓名", "監護人性別", "監護人職業", "監護人職業性質", "監護人職稱", "與學生關係(其他)", "與學生同住", "監護人住宅電話", "監護人手提電話", "監護人辦公電話", "監護人通訊地址 (門牌號碼)", "監護人通訊地址 (路名)", "監護人通訊地址 (花園)", "監護人通訊地址 (市填區)", "監護人通訊地址 (州屬)", "監護人通訊地址 (郵區號碼)", "監護人郵區號碼", "監護人電子郵件", "監護人教育程度");
$Lang['StudentRegistry']['Import_Malaysia'] = array("班別", "班內號", "學生內聯網帳號", "學號", "中文姓名", "英文姓名", "性別", "出生日期", "出生地點", "國籍", "祖籍", "種族", "身份證明文件編號", "報生紙號碼", "宗教", "家庭用語", "繳費類別", "住家電話", "電子郵件", "住宿情況", "居住地址 – 門牌號碼", "居住地址 – 路名", "居住地址 – 花園", "居住地址 – 市填區", "居住地址 – 州屬", "居住地址 – 郵區號碼", "入學日期", "目前就讀之兄姐 ", "小學就讀學校", "父親中文姓名", "父親英文姓名", "父親職業", "父親職業性質", "父親職稱", "父親婚姻狀況", "父親教育程度", "父親住家電話", "父親流動電話", "父親辦公電話", "父親通訊地址 – 門牌號碼", "父親通訊地址 – 路名", "父親通訊地址 – 花園", "父親通訊地址 – 市填區", "父親通訊地址 – 州屬", "父親通訊地址 – 郵區號碼", "父親郵區號碼", "父親電子郵件", "母親中文姓名", "母親英文姓名", "母親職業", "母親職業性質", "母親職稱", "母親婚姻狀況", "母親教育程度", "母親住家電話", "母親流動電話", "母親辦公電話", "母親通訊地址 – 門牌號碼", "母親通訊地址 – 路名", "母親通訊地址 – 花園", "母親通訊地址 – 市填區", "母親通訊地址 – 州屬", "母親通訊地址 – 郵區號碼", "母親郵區號碼", "母親電子郵件", "監護人中文姓名", "監護人英文姓名", "監護人性別", "監護人與學生關係", "監護人與學生關係(其他)", "監護人與學生同住", "監護人職業", "監護人職業性質", "監護人職稱", "監護人教育程度", "監護人住家電話", "監護人流動電話", "監護人辦公電話", "監護人通訊地址 – 門牌號碼", "監護人通訊地址 – 路名", "監護人通訊地址 – 花園", "監護人通訊地址 – 市填區", "監護人通訊地址 – 州屬", "監護人通訊地址 – 郵區號碼", "監護人郵區號碼", "監護人電子郵件");

$Lang['StudentRegistry']['ResidentStatus'] = array(1=>"永久", 2=>"有限期", 3=>"其他逗留許可");
$Lang['StudentRegistry']['R_Area_Ref'] = array("M"=>"澳門(包括離島)", "C"=>"中國", "O"=>"其他");
$Lang['StudentRegistry']['Area_Ref'] = array("M"=>"澳門", "T"=>"氹仔", "C"=>"路環", "O"=>"其他");

# export Class column list (Malaysia)
$Lang['StudentRegistry']['Malaysia_Class_CSV_field'][0] = array("Form", "Class", "Total no. of approved student(s)", "Total no. of left student(s)", "Total no. of suspend student(s)", "Total no. of resume student(s)", "Total no. of student(s)");
$Lang['StudentRegistry']['Malaysia_Class_CSV_field'][1] = array("級別", "班級", "在學人數", "退學人數", "休學人數", "復學人數", "總人數");

# export Student column list (Malaysia)
$Lang['StudentRegistry']['Malaysia_Student_CSV_field'][0] = array("Student No.", "Class", "Class Number", "English Name", "Chinese Name", "Registry Status", "Reason", "Apply Date", "Last Updated (Updated By)");
$Lang['StudentRegistry']['Malaysia_Student_CSV_field'][1] = array("學號", "班級", "座號", "中文姓名", "英文姓名", "學籍狀況", "原因", "申請日期", "最後更新日期 (更新者)");


# Import Checking (Macau)
$Lang['StudentRegistry']['Error'] = "錯誤";
$Lang['StudentRegistry']['IsMissing'] = "不存在";
$Lang['StudentRegistry']['IsInvalid'] = "不正確";
$Lang['StudentRegistry']['alreadyExist'] = "已存在";
$Lang['StudentRegistry']['StudentInfo'] = "班別/班內號或學生內聯網帳號";
$Lang['StudentRegistry']['StudentInfo2'] = "學生資料";
$Lang['StudentRegistry']['FatherName'] = "父親姓名";
$Lang['StudentRegistry']['MotherName'] = "母親姓名";
$Lang['StudentRegistry']['GuardianName'] = "監護人姓名";
$Lang['StudentRegistry']['EmergencyContactPerson'] = "緊急聯絡人姓名";
$Lang['StudentRegistry']['MaritalStatus'] = "婚姻狀況";
$Lang['StudentRegistry']['DateFormatRemark'] = "<font color='red'>#</font>";
$Lang['StudentRegistry']['MandatoryRemark'] = "<font color='red'>*</font>";

#Import Checking (Malaysia)
$Lang['StudentRegistry']['SiblingsRemark'] = "<font color='red'>+</font>";
$Lang['StudentRegistry']['SiblingsFormat'] = "請輸入兄姐之學號, 如多於一位兄姐於同校就讀, 請以「,」將學號分隔";

# Student Registry Status
$Lang['StudentRegistry']['StudentRegistryStatus'] = "學籍狀況";
$Lang['StudentRegistry']['RegistryStatus_Approved'] = "在籍";
$Lang['StudentRegistry']['RegistryStatus_Left'] = "退學";
$Lang['StudentRegistry']['RegistryStatus_Suspend'] = "休學";
$Lang['StudentRegistry']['RegistryStatus_Resume'] = "復學";
$Lang['StudentRegistry']['AmountOfApprove'] = "在學人數";
$Lang['StudentRegistry']['AmountOfLeft'] = "退學人數";
$Lang['StudentRegistry']['AmountOfSuspend'] = "休學人數";
$Lang['StudentRegistry']['AmountOfResume'] = "復學人數";
$Lang['StudentRegistry']['TotalAmountOfStudents'] = "總人數";
$Lang['StudentRegistry']['StudentNo'] = "學號";
$Lang['StudentRegistry']['ApplyDate'] = "申請日期";
$Lang['StudentRegistry']['Notice'] = "通告";
$Lang['StudentRegistry']['Reason'] = "原因";
$Lang['StudentRegistry']['View'] = "檢視";
$Lang['StudentRegistry']['ReasonAry'] = array("1"=>"轉入國中","2"=>"外國升造","3"=>"搬遷","4"=>"經濟困難","5"=>"連續曠課七天","6"=>"健康不佳","7"=>"工作","8"=>"適應不良","9"=>"無心向學","10"=>"升學","0"=>"其他");
$Lang['StudentRegistry']['SelectStudent'] = "選擇學生";
$Lang['StudentRegistry']['InputLeftInfo'] = "輸入退學資料";
$Lang['StudentRegistry']['InputSuspendInfo'] = "輸入休學資料";
$Lang['StudentRegistry']['InputResumeInfo'] = "輸入復學資料";
$Lang['StudentRegistry']['LeftNotice'] = "退學通告";
$Lang['StudentRegistry']['SuspendNotice'] = "休學通告";
$Lang['StudentRegistry']['ResumeNotice'] = "復學通告";
$Lang['StudentRegistry']['PrintNotice'] = "列印通告";
$Lang['StudentRegistry']['NoStudentRecord'] = "未有學生資料";
$Lang['StudentRegistry']['ApplyYear'] = "申請時年度";
$Lang['StudentRegistry']['ApplyForm'] = "申請時級別";
$Lang['StudentRegistry']['Class'] = "班級";
$Lang['StudentRegistry']['Mobile'] = "手提電話";
$Lang['StudentRegistry']['RelationshipWithStudent'] = "與學生關係";
$Lang['StudentRegistry']['ChangeOfRegistryStatus'] = "學藉狀況變更";
$Lang['StudentRegistry']['ApplyFor'] = "申請";
$Lang['StudentRegistry']['PrintNoticeToGuardian'] = "列印通告至監戶人";
$Lang['StudentRegistry']['eNoticeTemplate'] = "電子通告模板";
$Lang['StudentRegistry']['eNotice_ContactPersonName'] = "聯絡人姓名";
$Lang['StudentRegistry']['eNotice_ContactPersonAddress'] = "聯絡人地址";
$Lang['StudentRegistry']['eNotice_ReasonOfLeft'] = "退學原因";
$Lang['StudentRegistry']['eNotice_DateOfLeft'] = "退學日期";
$Lang['StudentRegistry']['eNotice_ReasonOfSuspend'] = "休學原因";
$Lang['StudentRegistry']['eNotice_DateOfSuspend'] = "休學日期";
$Lang['StudentRegistry']['eNotice_DateOfResume'] = "復學日期";
$Lang['StudentRegistry']['NoNoticeForThisStudent'] = "此學生並無通告";
$Lang['StudentRegistry']['ListOfLeft'] = "退學名單";
$Lang['StudentRegistry']['StatisticsAndReasonOfLeft'] = "退學率及原因統計";
$Lang['StudentRegistry']['ListOfSuspend'] = "休學名單";
$Lang['StudentRegistry']['StatisticsAndReasonOfSuspend'] = "休學率及原因統計";
$Lang['StudentRegistry']['ListOfResume'] = "復學名單";
$Lang['StudentRegistry']['StatisticsOfResume'] = "復學率";
$Lang['StudentRegistry']['GenerateReport'] = "產生報告";
$Lang['StudentRegistry']['PercentageOfLeft'] = "退學率";
$Lang['StudentRegistry']['StatisticsOfLeftReason'] = "退學原因統計";
$Lang['StudentRegistry']['PercentageOfSuspend'] = "休學率";
$Lang['StudentRegistry']['StatisticsOfSuspendReason'] = "休學原因統計";
$Lang['StudentRegistry']['PercentageOfResume'] = "復學率";
$Lang['StudentRegistry']['NotYetAttended'] = "未報到";
$Lang['StudentRegistry']['Amount'] = "人數";
$Lang['StudentRegistry']['SubTotal'] = "小計";
$Lang['StudentRegistry']['Total'] = "總計";
$Lang['StudentRegistry']['TotalStudents'] = "全校人數";
$Lang['StudentRegistry']['Percentage'] = "巴仙率";
$Lang['StudentRegistry']['HinHuaHighSchool'] = "興華中學";
$Lang['StudentRegistry']['Of'] = "";
$Lang['StudentRegistry']['StudentStatistics_Type_Malaysia'] = array("LODGING"=>$Lang['StudentRegistry']['HousingState'], "RACE"=>$Lang['StudentRegistry']['Race'], "FAMILY_LANG"=>$Lang['StudentRegistry']['FamilyLang'], "GENDER"=>$Lang['StudentRegistry']['Gender'], "NATION"=>$Lang['StudentRegistry']['Nationality'], "PAYMENT_TYPE"=>$Lang['StudentRegistry']['PayType'], "B_PLACE"=>$Lang['StudentRegistry']['BirthPlace'], "RELIGION"=>$Lang['StudentRegistry']['Religion']);
$Lang['StudentRegistry']['StudentGrouping1'] = "學生分類比較 1";
$Lang['StudentRegistry']['StudentGrouping2'] = "學生分類比較 2";
$Lang['StudentRegistry']['StudentGrouping'] = "學生分類比較";
$Lang['StudentRegistry']['NA'] = "無";
$Lang['StudentRegistry']['NewStatusApplyDateError'] = "<font color=red>新申請日期必須遲於或等於之前的申請日期</font>";
$Lang['StudentRegistry']['TransferredStudents'] = "插班生";
$Lang['StudentRegistry']['NewStudents'] = "新生";
$Lang['StudentRegistry']['CurrentStudents'] = "在籍生";
$Lang['StudentRegistry']['LeftStudents'] = "退學生";

$Lang['StudentRegistry']['ChineseCommercialCode'] = "中文商用電碼";
$Lang['StudentRegistry']['ID_DocumentType'] = "身份証明文件類型";
$Lang['StudentRegistry']['ID_DocumentNo'] = "身份証明文件號碼";
$Lang['StudentRegistry']['Passport'] = "護照";
$Lang['StudentRegistry']['Ethnicity_Chinese'] = "華人";
$Lang['StudentRegistry']['HomeLang'] = "家中使用語言";
$Lang['StudentRegistry']['HomeLangOptions'] = array("粵語", "英語", "普通話");
$Lang['StudentRegistry']['ReligionOptions'] = array("基督教", "天主教", "佛教");
$Lang['StudentRegistry']['Church'] = "教會";
$Lang['StudentRegistry']['HomePhoneNo'] = "住宅電話號碼";
$Lang['StudentRegistry']['MobilePhoneNo'] = "手提電話號碼";
$Lang['StudentRegistry']['ParentEMailAddress'] = "電郵地址";
$Lang['StudentRegistry']['EMailAddress'] = "學生電郵地址";
$Lang['StudentRegistry']['EnglishAddress'] = "英文地址";
$Lang['StudentRegistry']['ChineseAddress'] = "中文地址";
$Lang['StudentRegistry']['AddressField']['Flat'] = "室";
$Lang['StudentRegistry']['AddressField']['Floor'] = "樓";
$Lang['StudentRegistry']['AddressField']['Block'] = "座";
$Lang['StudentRegistry']['AddressField']['Building'] = "大廈";
$Lang['StudentRegistry']['AddressField']['Estate'] = "村/屋村";
$Lang['StudentRegistry']['AddressField']['Street'] = "街道名稱及號碼";
$Lang['StudentRegistry']['AddressField']['District'] = "地區";
$Lang['StudentRegistry']['AddressField']['Area'] = "地域";
$Lang['StudentRegistry']['AreaOptions'] = array("香港","九龍","新界");
$Lang['StudentRegistry']['LastSchool'] = "去年就讀學校 (只適用於新生)";
$Lang['StudentRegistry']['LastClassLevelClassNo'] = "過往就讀班別及班號 (只適用於舊生)";
$Lang['StudentRegistry']['BrotherSisterStudyingInSchool'] = "現時就讀本校之兄弟姊妹";
$Lang['StudentRegistry']['Settings']['OnlineReg'] = "網上註冊";
$Lang['StudentRegistry']['Settings']['NewStudent'] = "新生";
$Lang['StudentRegistry']['Settings']['CurrentStudent'] = "舊生";
$Lang['StudentRegistry']['Settings']['NewStudentParentGroup'] = "新學生家長組別";
$Lang['StudentRegistry']['Settings']['ReleasePeriod'] = "開放時段";
$Lang['StudentRegistry']['Settings']['CustomCol'] = "自訂欄位";
$Lang['StudentRegistry']['Settings']['Custom']['DisplayText_ch'] = '顯示名稱 (中文)';
$Lang['StudentRegistry']['Settings']['Custom']['DisplayText_en'] = '顯示名稱 (英文)';
$Lang['StudentRegistry']['Settings']['Custom']['Code'] = '欄代碼';
$Lang['StudentRegistry']['Settings']['Custom']['Section'] = '所屬部份';
$Lang['StudentRegistry']['Settings']['CustomColNotSelected'] = '未設定';
$Lang['StudentRegistry']['Settings']['CustomColSectionReminder'] = '「未設定」則會顯示於自訂欄位部份';
$Lang['StudentRegistry']['1st'] = "第一";
$Lang['StudentRegistry']['2nd'] = "第二";
$Lang['StudentRegistry']['3rd'] = "第三";
$Lang['StudentRegistry']['GuardianStudentRelationship'] = "與學生關係";
$Lang['StudentRegistry']['GuardianDayPhone'] = "日間聯絡電話";
$Lang['StudentRegistry']['GuardianMobile'] = "監護人手提電話號碼";
$Lang['StudentRegistry']['NoNeedCompleteSameAsStudent'] = "如與學生同住則不用填寫";
$Lang['StudentRegistry']['AddressSameAsStudent'] = "與學生同住";
$Lang['StudentRegistry']['OtherContactPerson'] = "其他聯絡人";
$Lang['StudentRegistry']['Number'] = "號碼";
$Lang['General']['PlsSpecify'] = "請註明";
$Lang['StudentRegistry']['PreviousYearChurch'] = "過去一年經常參加的教會";
$Lang['StudentRegistry']['Settings']['Text2Display'] = "給新生家長之訊息";
$Lang['StudentRegistry']['Settings']['Text2Display4Current'] = "給舊生家長之訊息";
$Lang['StudentRegistry']['Menu']['Syn2Guardian'] = "同步監護人資料";
$Lang['StudentRegistry']['Menu']['Syn2Guardian_difference'] = "位監護人的聯絡資料和家長帳戶的不一樣。";
$Lang['StudentRegistry']['Menu']['Syn2Guardian'] = "同步聯絡資料";
$Lang['StudentRegistry']['Menu']['Syn2Guardian_remark'] = " 依照「學籍資料」內的第一位監護人的手提電話號碼及電郵地址";
$Lang['StudentRegistry']['Menu']['Syn2GuardianConfirm'] = "確定要同步聯絡資料至家長帳戶";
$Lang['StudentRegistry']['Menu']['Syn2GuardianIntro'] = "以下為監護人與家長帳戶不同的的聯絡資料";

$Lang['StudentRegistry']['IncludeEverUpdatedOnly'] = "只包括曾經更新的資料";
$Lang['StudentRegistry']['LastUpdatedDate'] = "最後更新日期";
$Lang['StudentRegistry']['InputValidHKID'] = "請輸入正確的香港身份證號碼";
$Lang['StudentRegistry']['IncludeLeftStudent'] = "包括離校生";
$Lang['StudentRegistry']['IncludeLeftStudentRemark'] = "<span class='tabletextrequire'>*</span> 表示該用戶為校友或已離校。";
$Lang['StudentRegistry']['NoSpecialCharacter'] = "不能使用特殊字元(包括空白)";
$Lang['AccountMgmt']['StartedWithA2Z'] = "必須使用 'a-z' 為開始字元";
$Lang['AccountMgmt']['SubjectGroups'] = "科組";
$Lang['AccountMgmt']['OnlyCanAssignTo1SubjectGroup'] = "<font color='red'>學生只可被編排到同一科目中的一個學科。</font>";
$Lang['AccountMgmt']['OfficalPhotoMgmt'] = "學生相片管理小組";



$i_EventTypeString = array (
		"學校事項",
		"教學事項",
		"假期",
		"小組事項",
		"學校假期"
);

# Student Registry (Access Rights - Group Settings)
$Lang['StudentRegistry']['GroupNameDuplicateWarning'] = "*小組名稱重複或空白";

# Student Registry Statistic
$Lang['StudentRegistry']['Options'] = "選項";
$Lang['StudentRegistry']['ReportType'] = "報告類型";
$Lang['StudentRegistry']['ParentType'] = "家長類型";
$Lang['StudentRegistry']['Father'] = "父親";
$Lang['StudentRegistry']['Mother'] = "母親";
$Lang['StudentRegistry']['Guardian'] = "監護人";
$Lang['StudentRegistry']['ProfessionStat'] = "職業性質統計";
$Lang['StudentRegistry']['TitleStat'] = "職稱統計";
$Lang['StudentRegistry']['GenerateReport'] = "產生報告";
$Lang['StudentRegistry']['ParentJobNatureStat'] = "家長職業性質統計";
$Lang['StudentRegistry']['ParentJobTitleStat'] = "家長職稱統計";
$Lang['StudentRegistry']['JobNatureGroup'] = "職業性質分類";
$Lang['StudentRegistry']['JobTitleGroup'] = "職稱分類";
$Lang['StudentRegistry']['NoOfParent'] = "人數";
$Lang['StudentRegistry']['ShowStatOption'] = "顯示統計選項";
$Lang['StudentRegistry']['HideStatOption'] = "隱藏統計選項";

$Lang['StudentRegistry']['PrintReport']['ParentProfession'] = "家長職業性質";
$Lang['StudentRegistry']['PrintReport']['ParentJobTitle'] = "家長職稱";
$Lang['StudentRegistry']['PrintReport']['And'] = "及";
$Lang['StudentRegistry']['PrintReport']['Statistics'] = "統計";

# Student Registry Report
$Lang['StudentRegistry']['ShowReportOption'] = "顯示報告選項";
$Lang['StudentRegistry']['HideReportOption'] = "隱藏報告選項";
$Lang['StudentRegistry']['EntireClassList'] = "全班學生列表";
$Lang['StudentRegistry']['PersonalReport'] = "個人報告";
$Lang['StudentRegistry']['BlankForm'] = "空白表格";
$Lang['StudentRegistry']['Student'] = "學生";
$Lang['StudentRegistry']['InfoShown'] = "顯示資料";
$Lang['StudentRegistry']['StudentBasicInfo_s'] = "學生基本資料";
$Lang['StudentRegistry']['StudentAdvInfo_s'] = "學生進階資料";
$Lang['StudentRegistry']['FatherInfo_s'] = "父親資料";
$Lang['StudentRegistry']['MotherInfo_s'] = "母親資料";
$Lang['StudentRegistry']['GuardianInfo_s'] = "監護人資料";
$Lang['StudentRegistry']['CtrlSelectAll'] = "可按Ctrl鍵選擇多項";
$Lang['StudentRegistry']['StudentRegistryForm'] = "學籍資料表格";
$Lang['StudentRegistry']['SelectDateWithinSameAcademicYear'] = "日期選項須於同一學年內";
$Lang['StudentRegistry']['WithTransferredStudents'] = "顯示插班生資料";

$Lang['StudentRegistry']['Tag'] = "標籤";
$Lang['StudentRegistry']['AllTags'] = "所有標籤";
$Lang['StudentRegistry']['CommaAsSeparator'] = "請用逗號 (,) 作分隔";

# 2013-07-08 eLib license
$Lang['StudentRegistry']['License'] = "購買和啟用記錄";
$Lang['StudentRegistry']['Quotation#'] = "Quotation #";
$Lang['StudentRegistry']['Numberofbooks'] = "Number of books";
$Lang['StudentRegistry']['Status'] = "Status";
$Lang['StudentRegistry']['InstallationDate'] = "Installation Date";
$Lang["ebook_license"]["select"] = "選擇";
$Lang["ebook_license"]["deselect"] = "取消選擇";
$Lang["ebook_license"]["finish"] = "完成";
$Lang["ebook_license"]["available_books"] = "可供選擇的圖書";
$Lang["ebook_license"]["selected_books"] = "已選擇的圖書";
$Lang["ebook_license"]["quotation_number"] = "訂單編號";
$Lang["ebook_license"]["language"] = "語言";
$Lang["ebook_license"]["number_of_books"] = "選擇圖書數量";
$Lang["ebook_license"]["status"] = "狀態";
$Lang["ebook_license"]["installation_date"] = "安裝日期";
$Lang["ebook_license"]["activate_from"] = "從";
$Lang["ebook_license"]["activate_from_end"] = "起生效";
$Lang["ebook_license"]["activate_until"] = "有效期至";
$Lang["ebook_license"]["activate_until_end"] = "";
$Lang["ebook_license"]["expired"] = "已過期";
$Lang["ebook_license"]["activated"] = "生效";
$Lang["ebook_license"]["pending_for_book_selection"] = "等待選擇圖書";
$Lang["ebook_license"]["select_exactly_books"] = "請選擇";
$Lang["ebook_license"]["select_exactly_books_end"] = "本圖書";
$Lang["ebook_license"]["compulsory"] = "必選";
$Lang["ebook_license"]["permanent_use"] = "永久使用";
$Lang['libms']['bookmanagement']['ebook_license'] = "電子圖書使用證";
$Lang["libms"]["book_export_marc21"]="匯出MARC21";

# Student Registry Report - Student Registry Information
$Lang['StudentRegistry']['InfoReportTitle']['Stud_Adv']   = array("學號", "中文姓名", "英文姓名", "性別", "出生日期", "出生地點", "國籍", "祖籍", "種族", "身份證明文件編號", "報生紙號碼", "宗教", "家庭用語", "繳費類別", "住家電話", "電子郵件", "住宿情況", "居住地址 – 門牌號碼", "居住地址 – 路名", "居住地址 – 花園", "居住地址 – 市填區", "居住地址 – 州屬", "居住地址 – 郵區號碼", "入學日期", "目前就讀之兄姐 ", "小學就讀學校");
$Lang['StudentRegistry']['InfoReportTitle']['Stud_Basic'] = array("學號", "中文姓名", "英文姓名", "性別", "國籍", "住家電話");
$Lang['StudentRegistry']['InfoReportTitle']['FatherInfo'] = array("父親中文姓名", "父親英文姓名", "父親職業", "父親職業性質", "父親職稱", "父親婚姻狀況", "父親教育程度", "父親住家電話", "父親流動電話", "父親辦公電話", "父親通訊地址 – 門牌號碼", "父親通訊地址 – 路名", "父親通訊地址 – 花園", "父親通訊地址 – 市填區", "父親通訊地址 – 州屬", "父親通訊地址 – 郵區號碼", "父親郵區號碼", "父親電子郵件");
$Lang['StudentRegistry']['InfoReportTitle']['MotherInfo'] = array("母親中文姓名", "母親英文姓名", "母親職業", "母親職業性質", "母親職稱", "母親婚姻狀況", "母親教育程度", "母親住家電話", "母親流動電話", "母親辦公電話", "母親通訊地址 – 門牌號碼", "母親通訊地址 – 路名", "母親通訊地址 – 花園", "母親通訊地址 – 市填區", "母親通訊地址 – 州屬", "母親通訊地址 – 郵區號碼", "母親郵區號碼", "母親電子郵件");
$Lang['StudentRegistry']['InfoReportTitle']['GuardInfo']  = array("監護人中文姓名", "監護人英文姓名", "監護人性別", "監護人與學生關係", "監護人與學生關係(其他)", "監護人與學生同住", "監護人職業", "監護人職業性質", "監護人職稱", "監護人教育程度", "監護人住家電話", "監護人流動電話", "監護人辦公電話", "監護人通訊地址 – 門牌號碼", "監護人通訊地址 – 路名", "監護人通訊地址 – 花園", "監護人通訊地址 – 市填區", "監護人通訊地址 – 州屬", "監護人通訊地址 – 郵區號碼", "監護人郵區號碼", "監護人電子郵件");

# Student Registry Report - Student Registry Information - export
$Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Adv'][0]   = array("Student No.", "Chinese Name", "English Name", "Gender", "Date of Birth", "Birth Place", "Nationality", "Ancestral Home", "Race", "ID Card No.", "Birth Cert. No.", "Religion", "Family Language", "Payment Type", "Phone No.(Home)", "Email", "Housing Status", "Home Address – (Door)", "Home Address – (Road)", "Home Address – (Garden)", "Home Address – (Town)", "Home Address – (State)", "Home Address – (Postal Code)", "Date of Entry", "Elder Brother/Sister in School", "Primary School");
$Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Basic'][0] = array("Student No.", "Chinese Name", "English Name", "Gender", "Nationality", "Phone No.(Home)");
$Lang['StudentRegistry']['InfoReportTitle']['export']['FatherInfo'][0] = array("Father Chinese Name", "Father English Name", "Father Job Occupation", "Father Job Nature", "Father Job Title", "Father Marital Status", "Father Level of Education", "Father Phone No.(Home)", "Father Phone No.(Mobile)", "Father Phone No.(Office)", "Father Contact Address – (Door)", "Father Contact Address – (Road)", "Father Contact Address – (Garden)", "Father Contact Address – (Town)", "Father Contact Address – (State)", "Father Contact Address – (Postal Code)", "Father Postal Code", "Father Email");
$Lang['StudentRegistry']['InfoReportTitle']['export']['MotherInfo'][0] = array("Mother Chinese Name", "Mother English Name", "Mother Job Occupation", "Mother Job Nature", "Mother Job Title", "Mother Marital Status", "Mother Level of Education", "Mother Phone No.(Home)", "Mother Phone No.(Mobile)", "Mother Phone No.(Office)", "Mother Contact Address – (Door)", "Mother Contact Address – (Road)", "Mother Contact Address – (Garden)", "Mother Contact Address – (Town)", "Mother Contact Address – (State)", "Mother Contact Address – (Postal Code)", "Mother Postal Code", "Mother Email");
$Lang['StudentRegistry']['InfoReportTitle']['export']['GuardInfo'][0]  = array("Guardian Chinese Name", "Guardian English Name", "Guardian Gender", "Guardian Relationship", "Guardian Relationship (Other)", "Guardian Live with Student", "Guardian Job Occupation", "Guardian Job Nature", "Guardian Job Title", "Guardian Level of Education", "Guardian Phone No.(Home)", "Guardian Phone No.(Mobile)", "Guardian Phone No.(Office)", "Guardian Contact Address – (Door)", "Guardian Contact Address – (Road)", "Guardian Contact Address – (Garden)", "Guardian Contact Address – (Town)", "Guardian Contact Address – (State)", "Guardian Contact Address – (Postal Code)", "Guardian Postal Code", "Guardian Email");
$Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Adv'][1]   = array("學號", "中文姓名", "英文姓名", "性別", "出生日期", "出生地點", "國籍", "祖籍", "種族", "身份證明文件編號", "報生紙號碼", "宗教", "家庭用語", "繳費類別", "住家電話", "電子郵件", "住宿情況", "居住地址 – 門牌號碼", "居住地址 – 路名", "居住地址 – 花園", "居住地址 – 市填區", "居住地址 – 州屬", "居住地址 – 郵區號碼", "入學日期", "目前就讀之兄姐 ", "小學就讀學校");
$Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Basic'][1] = array("學號", "中文姓名", "英文姓名", "性別", "國籍", "住家電話");
$Lang['StudentRegistry']['InfoReportTitle']['export']['FatherInfo'][1] = array("父親中文姓名", "父親英文姓名", "父親職業", "父親職業性質", "父親職稱", "父親婚姻狀況", "父親教育程度", "父親住家電話", "父親流動電話", "父親辦公電話", "父親通訊地址 – 門牌號碼", "父親通訊地址 – 路名", "父親通訊地址 – 花園", "父親通訊地址 – 市填區", "父親通訊地址 – 州屬", "父親通訊地址 – 郵區號碼", "父親郵區號碼", "父親電子郵件");
$Lang['StudentRegistry']['InfoReportTitle']['export']['MotherInfo'][1] = array("母親中文姓名", "母親英文姓名", "母親職業", "母親職業性質", "母親職稱", "母親婚姻狀況", "母親教育程度", "母親住家電話", "母親流動電話", "母親辦公電話", "母親通訊地址 – 門牌號碼", "母親通訊地址 – 路名", "母親通訊地址 – 花園", "母親通訊地址 – 市填區", "母親通訊地址 – 州屬", "母親通訊地址 – 郵區號碼", "母親郵區號碼", "母親電子郵件");
$Lang['StudentRegistry']['InfoReportTitle']['export']['GuardInfo'][1]  = array("監護人中文姓名", "監護人英文姓名", "監護人性別", "監護人與學生關係", "監護人與學生關係(其他)", "監護人與學生同住", "監護人職業", "監護人職業性質", "監護人職稱", "監護人教育程度", "監護人住家電話", "監護人流動電話", "監護人辦公電話", "監護人通訊地址 – 門牌號碼", "監護人通訊地址 – 路名", "監護人通訊地址 – 花園", "監護人通訊地址 – 市填區", "監護人通訊地址 – 州屬", "監護人通訊地址 – 郵區號碼", "監護人郵區號碼", "監護人電子郵件");

$Lang['StudentRegistry']['ExportForImport'] = "匯出 (用於匯入資料)";
$Lang['StudentRegistry']['CodeFormat'] = "資料代號";
$Lang['StudentRegistry']['ExportForWebSAMS'] = "匯出 (". $Lang['eDiscipline']['WebSAMSFormatExport'] .")";
# Start Student Registry Kentville
$Lang['StudentRegistry']['StudentName'] = '學生名稱';
$Lang['StudentRegistry']['StudentIDNo'] = '學生編號';
$Lang['StudentRegistry']['Date'] = '日期';
$Lang['StudentRegistry']['StudentDataRecord'] = '學生資料';
$Lang['StudentRegistry']['LastName'] = "姓";
$Lang['StudentRegistry']['GivenNames'] = "名";
$Lang['StudentRegistry']['OfficialPhoto'] = '學生相';
$Lang['StudentRegistry']['AsAppearsOn'] = "根據出世紙上顯示紀錄";
$Lang['StudentRegistry']['inEnglish'] = "英文";
$Lang['StudentRegistry']['inChinese'] = "中文";
$Lang['StudentRegistry']['ChiAddress'] = "中文住址";
$Lang['StudentRegistry']['SchoolBus'] = "校車路線";
$Lang['StudentRegistry']['NannyBus'] = "保姆車 / 聯絡號碼";
$Lang['StudentRegistry']['DisplayName'] = "顯示名稱";
$Lang['StudentRegistry']['BusinessName'] = "公司名稱";
$Lang['StudentRegistry']['BusinessAddress'] = "公司聯絡地址";
$Lang['StudentRegistry']['Alumni'] = "校友/畢業年份";
$Lang['StudentRegistry']['LastSchoolName'] = "舊校資料";
$Lang['StudentRegistry']['EnglishAreaOptions'] = array("Hong Kong","Kowloon","New Territories", "");
$Lang['StudentRegistry']['BrotherSisterInfo'] = "兄弟姊妹資料";
$Lang['StudentRegistry']['Sibling']['Relationship'] = "兄弟姊妹";
$Lang['StudentRegistry']['Sibling']['Age'] = "年齡";
$Lang['StudentRegistry']['Sibling']['RelationshipAge'] = "兄弟姊妹(年齡)";
$Lang['StudentRegistry']['Sibling']['Name'] = "姓名";
$Lang['StudentRegistry']['Sibling']['IsSchoolStudentOrAlumni'] = "是否本校生/校友";
$Lang['StudentRegistry']['Sibling']['ClassLevel'] = "級別/班別";
$Lang['StudentRegistry']['Sibling']['KentvilleClass'] = "本校生(級別)";
$Lang['StudentRegistry']['Sibling']['ExKentvilleClass'] = "校友(班別)";
$Lang['StudentRegistry']['Sibling']['ExKentvilleYear'] = "校友(畢業年份)";
$Lang['StudentRegistry']['Sibling']['ExKentvilleClassYear'] = "校友(畢業年份)";
$Lang['StudentRegistry']['Sibling']['School'] = "現就讀";
$Lang['StudentRegistry']['Sibling']['Level'] = "班級";
$Lang['StudentRegistry']['Sibling']['SchoolLevel'] = "現就讀/班級";
$Lang['StudentRegistry']['Sibling']['AddSibling'] = "增加兄弟姊妹紀錄";
$Lang['StudentRegistry']['Sibling']['RemoveSibling'] = "刪除此兄弟姊妹紀錄";
$Lang['StudentRegistry']['EmergencyContactPersonInfo'] = "緊急聯絡人資料";
$Lang['StudentRegistry']['EmergencyContact'] = "緊急聯絡人";
$Lang['StudentRegistry']['NonParent'] = "家長以外人士";
$Lang['StudentRegistry']['ContactPhone'] = "聯絡電話";
$Lang['StudentRegistry']['FatherFirstName'] = "父親名字(英文)";
$Lang['StudentRegistry']['FatherLastName'] = "父親姓氏(英文)";
$Lang['StudentRegistry']['FatherChineseName'] = "父親姓名(中文)";
$Lang['StudentRegistry']['FatherEmail'] = "父親電郵地址";
$Lang['StudentRegistry']['FatherMobile'] = "父親手機號碼";
$Lang['StudentRegistry']['MotherFirstName'] = "母親名字(英文)";
$Lang['StudentRegistry']['MotherLastName'] = "母親姓氏(英文)";
$Lang['StudentRegistry']['MotherChineseName'] = "母親姓名(中文)";
$Lang['StudentRegistry']['MotherEmail'] = "母親電郵地址";
$Lang['StudentRegistry']['MotherMobile'] = "母親手機號碼";
$Lang['StudentRegistry']['EmergencyContactFirstName'] = "緊急聯絡人名字(英文)";
$Lang['StudentRegistry']['EmergencyContactLastName'] = "緊急聯絡人姓氏(英文)";
$Lang['StudentRegistry']['EmergencyContactChineseName'] = "緊急聯絡人名稱(中文)";
$Lang['StudentRegistry']['EmergencyContactRelationship'] = "與緊急聯絡人關係";
$Lang['StudentRegistry']['EmergencyContactPhone'] = "緊急聯絡人電話";
$Lang['StudentRegistry']['JS_warning']['InvalidHomeTel'] = "住宅電話號碼無效";
$Lang['StudentRegistry']['JS_warning']['InvalidMobile'] = "手機號碼無效";
$Lang['StudentRegistry']['JS_warning']['InvalidTel'] = "電話號碼無效";
$Lang['StudentRegistry']['JS_warning']['MaxSiblings'] = "最多只可輸入三個兄弟姊妹";
$Lang['StudentRegistry']['JS_warning']['InputNumericOnly'] = "請輸入數字";
$Lang['StudentRegistry']['JS_warning']['AgeOutOfRange'] = "年齡超出範圍";
$Lang['StudentRegistry']['JS_warning']['AcceptDeclarations'] = "請閱讀及接受所附聲明";
$Lang['StudentRegistry']['BtnExportCsv'] = "匯出 csv";
$Lang['StudentRegistry']['BtnExportPdf'] = "匯出 pdf";
$Lang['StudentRegistry']['ExportPdf']['PageNumSuffix'] = "第 ";
$Lang['StudentRegistry']['ExportPdf']['TotPageNumPrefix'] = " 頁共 ";
$Lang['StudentRegistry']['ExportPdf']['TotPageNumSuffix'] = " 頁";
$Lang['StudentRegistry']['ExportPdf']['SchoolName'] = "根德園幼稚園";
$Lang['StudentRegistry']['ExportPdf']['EnglishFlat'] = "Flat/Room";
$Lang['StudentRegistry']['ExportPdf']['EnglishFloor'] = "Floor";
$Lang['StudentRegistry']['ExportPdf']['EnglishBlock'] = "Block";
$Lang['StudentRegistry']['ImportAry'][] = array('ClassName','班別','');
$Lang['StudentRegistry']['ImportAry'][] = array('ClassNumber','學號','');
$Lang['StudentRegistry']['ImportAry'][] = array('StudentID','學生編號<font color="red">*</font>','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_EngLastName','英文姓','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_EngFirstName','英文名','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_ChiName','中文姓名','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_DateOfBirth','出生日期(日期格式為年月日 yyyy-mm-dd)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_PlaceOfBirthCode','出生地點,如找不到代號,請輸入名稱','getPlaceOfBirth');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_BirthCertificateNo','出生登記號碼','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_Gender','性別(F/M)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_NationalityCode','國籍,如找不到代號,請輸入名稱','getNationality');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_EthnicityCode','種族,如找不到代號,請輸入名稱','getEthnicity');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_HomeLangCode','家中常用語言,如找不到代號,請輸入名稱','getHomeLang');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressFlat','室','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressFloor','樓','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressBlock','座','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressBuilding','大廈','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressEstate','村/屋村','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressStreet','街道名稱及號碼','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressDistrictCode','地區,如找不到代號,請輸入名稱','getDistrict');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressAreaCode','地域','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_AddressChi','住址(中文)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_HomePhone','住宅電話號碼','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_NannyBus','保姆車 / 聯絡號碼','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_EngLastName','父親英文姓','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_EngFirstName','父親英文名','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_ChiName','父親中文姓名','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_BusinessName','公司名稱','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_Email','電郵地址','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_AddressFlat','室','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_AddressFloor','樓','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_AddressBlock','座','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_AddressBuilding','大廈','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_AddressEstate','村/屋村','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_AddressStreet','街道名稱及號碼','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_AddressDistrictCode','地區,如找不到代號,請輸入名稱','getDistrict');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_AddressAreaCode','地域','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_Title','職位','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_Occupation','職業','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_IsAlumni','校友(Y/N)','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_GraduateYear','畢業年份','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_OfficePhone','公司電話','');
$Lang['StudentRegistry']['ImportAry'][] = array('FA_Mobile','手提電話號碼','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_EngLastName','母親英文姓','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_EngFirstName','母親英文名','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_ChiName','母親中文姓名','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_BusinessName','公司名稱','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_Email','電郵地址','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_AddressFlat','室','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_AddressFloor','樓','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_AddressBlock','座','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_AddressBuilding','大廈','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_AddressEstate','村/屋村','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_AddressStreet','街道名稱及號碼','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_AddressDistrictCode','地區,如找不到代號,請輸入名稱','getDistrict');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_AddressAreaCode','地域','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_Title','職位','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_Occupation','職業','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_IsAlumni','校友','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_GraduateYear','畢業年份','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_OfficePhone','公司電話','');
$Lang['StudentRegistry']['ImportAry'][] = array('MO_Mobile','手提電話號碼','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_LastSchoolEngName','舊校資料','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsRelationshipCode_1','兄弟姊妹,如找不到代號,請輸入關係','getSiblingRelationship');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsDateOfBirth_1','出生日期(日期格式為年月日 yyyy-mm-dd)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsName_1','姓名','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsIsSchoolPupilOrAlumni_1','本校生/校友(Y/N)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsKVClass_1','級別','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsGraduateYear_1','校友(畢業年份)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsCurSchool_1','現就讀學校','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsCurLevelCode_1','班級,如找不到代號,請輸入程度','getSchoolLevel');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsRelationshipCode_2','兄弟姊妹,如找不到代號,請輸入關係','getSiblingRelationship');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsDateOfBirth_2','出生日期(日期格式為年月日 yyyy-mm-dd)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsName_2','姓名','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsIsSchoolPupilOrAlumni_2','本校生/校友(Y/N)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsKVClass_2','級別','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsGraduateYear_2','校友(畢業年份)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsCurSchool_2','現就讀學校','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsCurLevelCode_2','班級,如找不到代號,請輸入程度','getSchoolLevel');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsRelationshipCode_3','兄弟姊妹,如找不到代號,請輸入關係','getSiblingRelationship');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsDateOfBirth_3','出生日期(日期格式為年月日 yyyy-mm-dd)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsName_3','姓名','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsIsSchoolPupilOrAlumni_3','本校生/校友(Y/N)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsKVClass_3','級別','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsGraduateYear_3','校友(畢業年份)','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsCurSchool_3','現就讀學校','');
$Lang['StudentRegistry']['ImportAry'][] = array('STU_SiblingsCurLevelCode_3','班級,如找不到代號,請輸入程度','getSchoolLevel');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_EngLastName_1','緊急聯絡人英文姓','');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_EngFirstName_1','緊急聯絡人英文名','');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_ChiName_1','緊急聯絡人中文姓名','');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_RelationshipCode_1','關係,如找不到代號,請輸入關係','getRelationship');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_Telephone_1','聯絡電話','');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_EngLastName_2','急聯絡人英文姓','');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_EngFirstName_2','緊急聯絡人英文名','');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_ChiName_2','緊急聯絡人中文姓名','');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_RelationshipCode_2','關係,如找不到代號,請輸入關係','getRelationship');
$Lang['StudentRegistry']['ImportAry'][] = array('EmgContact_Telephone_2','聯絡電話','');
$Lang['StudentRegistry']['Here'] = "此處";
$Lang['StudentRegistry']['PersonalInformationCollectionStatement'] = "請點擊%s下載根德園幼稚園收集個人資料聲明;使用學生資料聲明和家長聲明。";
$Lang['StudentRegistry']['ReadAndUnderstandDeclarations'] = "我已閱讀根德園幼稚園收集個人資料聲明;使用學生資料聲明和家長聲明。我授權學校按所述內容使用收集到的資料。";
$Lang['StudentRegistry']['Synchronization'] = "同步學生資料";
$Lang['StudentRegistry']['SynchronizationConfirm'] = "是否同步該學生資料到學生用戶資料?";
$Lang['StudentRegistry']['SynchronizationResult']['SyncFail'] = "0|=|同步學生資料失敗";
$Lang['StudentRegistry']['SynchronizationResult']['SyncSuccess'] = "1|=|同步學生資料成功";
$Lang['StudentRegistry']['SynchronizeScope'] = "同步學生資料的範圍";
$Lang['StudentRegistry']['SpecificStudent'] = "個別學生";
# End Student Registry Kentville

## eBooking Words
$Lang['eBooking']['General']['FieldTitle']['Item'] = "物品";
$Lang['eBooking']['General']['FieldTitle']['Room'] = "房間";
$Lang['eBooking']['General']['FieldTitle']['Location'] = "場地";
$Lang['eBooking']['General']['FieldTitle']['PleaseSelectItem'] = "請選擇物品";
$Lang['eBooking']['General']['FieldTitle']['PleaseSelectRoom'] = "請選擇房間";
$Lang['eBooking']['General']['FieldTitle']['TimeZone'] = "時區";
$Lang['eBooking']['General']['FieldTitle']['Category'] = '類別';
$Lang['eBooking']['General']['FieldTitle']['Category2'] = '子類別';
$Lang['eBooking']['General']['Export']['FieldTitle']['RequestDate'] = "申請日期";
$Lang['eBooking']['General']['Export']['FieldTitle']['ApprovalDate'] = "審核日期";
$Lang['eBooking']['Button']['FieldTitle']['Copy'] = "複製";
$Lang['eBooking']['Button']['FieldTitle']['NewRule'] = "新增規則";
$Lang['eBooking']['Button']['FieldTitle']['EditRule'] = "編輯規則";
$Lang['eBooking']['Button']['FieldTitle']['CheckIn'] = "取用";
$Lang['eBooking']['Button']['FieldTitle']['CheckOut'] = "退還";
$Lang['eBooking']['Management']['FieldTitle']['Management'] = "管理";
$Lang['eBooking']['Management']['FieldTitle']['BookingRequest'] = "預訂申請";
$Lang['eBooking']['Management']['FieldTitle']['Reserve'] = "優先預訂";
$Lang['eBooking']['Management']['FieldTitle']['RoomBooking'] = "房間預訂紀錄";
$Lang['eBooking']['Management']['FieldTitle']['ItemBooking'] = "物品預訂紀錄";
$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'] = array('','一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月');
$Lang['eBooking']['Management']['FieldTitle']['Dates'] = "日期";
$Lang['eBooking']['Management']['FieldTitle']['Time'] = "時間";
$Lang['eBooking']['Management']['FieldTitle']['RelatedBooking'] = "相關預訂";
$Lang['eBooking']['Management']['FieldTitle']['Responsible'] = "使用者";
$Lang['eBooking']['Management']['FieldTitle']['BookedBy'] = "預訂者";
$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy'] = "批核人";
$Lang['eBooking']['Management']['FieldTitle']['FollowUpWork'] = "跟進工作日程";
$Lang['eBooking']['Management']['FieldTitle']['SelectFollowUpGroup'] = "選擇跟進小組";
$Lang['eBooking']['Management']['FieldTitle']['SelectItem'] = "選擇物品";
$Lang['eBooking']['Management']['FieldTitle']['ShowRemarks'] = "顯示備註";
$Lang['eBooking']['Management']['FieldTitle']['HideRemarks'] = "不顯示備註";
$Lang['eBooking']['Management']['FieldTitle']['FollowUpWorkSchedule'] = "跟進工作時間表";
$Lang['eBooking']['Management']['FieldTitle']['BookingMethod'] = "預訂方法";
$Lang['eBooking']['Management']['General']['JSWarning']['PleaseSelectStatus'] = "請選擇狀況";
$Lang['eBooking']['Management']['FollowUpWork']['WarningArr']['SelectFollowUpGroup'] = "請選擇跟進小組";
$Lang['eBooking']['Management']['BookingRequest']['JSWarning']['ConfirmDelete'] = "所有已選取的記錄將被刪除, 繼續?";
$Lang['eBooking']['Management']['BookingRequest']['JSWarning']['ConfirmReject'] = "所有已選取的記錄將被拒絕, 繼續?";
$Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['RequestDate'] = "預訂日子";
$Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['ReportTitle']['RoomBookingList'] = "房間預訂紀錄列表";
$Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['ReportTitle']['RoomBookingWeek'] = "房間預訂紀錄時間表";
$Lang['eBooking']['Management']['ItemBooking']['FieldTitle']['ReportTitle']['ItemBookingList'] = "物品預訂紀錄列表";
$Lang['eBooking']['Management']['ItemBooking']['FieldTitle']['ReportTitle']['ItemBookingWeek'] = "物品預訂紀錄時間表";
$Lang['eBooking']['Management']['DayView']['Settings']['TimeInterval'] = "時間間隔 (分鐘)";
$Lang['eBooking']['Management']['Filter']['AllBookingCategory'] = '全部預訂用途類別';
$Lang['eBooking']['Management']['Warning']['StartTimeEndTime'] = '<span class="tabletextrequire">開始時間不能在結束時間之後</span>';

$Lang['eBooking']['Reports']['FieldTitle']['Reports'] ="報告";
$Lang['eBooking']['Reports']['FieldTitle']['UsageReport'] = "用量報告";
$Lang['eBooking']['Reports']['UsageReport']['Date'] = "日期";
$Lang['eBooking']['Reports']['UsageReport']['Type'] = "類型";
$Lang['eBooking']['Reports']['UsageReport']['DataUnits'] = "單位";
$Lang['eBooking']['Reports']['UsageReport']['Times'] = "次數";
$Lang['eBooking']['Reports']['UsageReport']['Hours'] = "小時";

$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckInAndCheckOut'] = '取用及退還';
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['TodayRecords'] = '今天的預訂紀錄';
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckInRecords'] = '已登記紀錄';
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutRecords'] = '已退還紀錄';
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['StartDate'] = '開始日期';
$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['EndDate'] = '結束日期';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedInSuccessfully'] = '1|=|所有選取的記錄已成功登記';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedInFailed'] = '0|=|所有選取的記錄未能成功登記';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedOutSuccessfully'] = '1|=|所有選取的記錄已成功退還';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedOutFailed'] = '0|=|所有選取的記錄未能成功退還';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['CancelCheckInSuccessfully'] = '1|=|所有選取的記錄已成功取消登記';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['CancelCheckInFailed'] = '0|=|所有選取的記錄未能成功取消登記';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['CancelCheckOutSuccessfully'] = '1|=|所有選取的記錄已成功取消退還';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['CancelCheckOutFailed'] = '0|=|所有選取的記錄未能成功取消退還';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['UpdateRemarkSuccessfully'] = '1|=|所有相關記錄的備註已成功更新';
$Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['UpdateRemarkFailed'] = '0|=|所有相關記錄的備註未能成功更新';
$Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['Waiting'] = '等待';
$Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['CheckedInOrBorrowed'] = '已取用/已借出';
$Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['CheckedOutOrReturned'] = '已退還/已歸還';

$Lang['eBooking']['Settings']['FieldTitle']['NoFollowUpGroup'] = "沒有跟進小組";
$Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup'] = "沒有批核小組";
$Lang['eBooking']['Settings']['FieldTitle']['ListView'] = "列表";
$Lang['eBooking']['Settings']['FieldTitle']['WeekView'] = "星期";
$Lang['eBooking']['Settings']['FieldTitle']['Settings'] = "設定";
$Lang['eBooking']['Settings']['FieldTitle']['BookingPeriodsSettings'] = "預訂時期設定";
$Lang['eBooking']['Settings']['FieldTitle']['GeneralPermission'] = "預訂特性";
$Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'] = "批核小組";
$Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'] = "跟進小組";
$Lang['eBooking']['Settings']['FieldTitle']['BasicBookingPeriod'] = "預設時段";
$Lang['eBooking']['Settings']['FieldTitle']['ItemOrRoomBookingPeriod'] = "房間 / 物品時段";
$Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod'] = "預訂時段";
$Lang['eBooking']['Settings']['FieldTitle']['AvailableAt'] = "可預約的時間";
$Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule'] = "用戶權限";
$Lang['eBooking']['Settings']['FieldTitle']['ItemBookingRule'] = "物品預訂權限";
$Lang['eBooking']['Settings']['FieldTitle']['BasicCondition'] = "特別設定";
$Lang['eBooking']['Settings']['FieldTitle']['SpecificCondition'] = "特別設定";
$Lang['eBooking']['Settings']['FieldTitle']['TimeZoneSettings'] = "時區設定";
$Lang['eBooking']['Settings']['FieldTitle']['Category'] = "資源類別";
$Lang['eBooking']['Settings']['FieldTitle']['SelectItem'] = "- 選擇物品 -";
$Lang['eBooking']['Settings']['FieldTitle']['AllItems'] = "所有物品";
$Lang['eBooking']['Settings']['FieldTitle']['SelectCategory'] = "- 選擇類別 -";
$Lang['eBooking']['Settings']['FieldTitle']['SelectSubCategory'] = "- 選擇子類別 -";

$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['New'] = "新增預約時段";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Edit'] = "修改預約時段";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Delete'] = "刪除預約時段";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['To'] = "至";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['DateRange'] = "時段";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Repeat'] = "重複頻率";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Time'] = "時間";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Daily'] = "每天";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['CycleDay'] = "循環日";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Weekday'] = "週日";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['DoesNotRepeat'] = "不重複";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['WholeDay'] = "全日";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Duration'] = "持續時間";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['StartTime'] = "開始時間";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['EndTime'] = "結束時間";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['hours'] = "小時";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['mins'] = "分鐘";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period'] = "課節";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['SelectRepeatOption'] = "請選擇";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['AvaliablePeriod'] = "可預訂";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['NonAvaliablePeriod'] = "不可預訂";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['FacilityType'] = "類型";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Locations'] = "場地";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Items'] = "物品";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['SpecificTime'] = "特定時間";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['PeriodStart'] = "開始日期";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['PeriodEnd'] = "結束日期";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['TimeSlotTitle'] = "第num節";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['JSWarning']['PleaseSelectCopyTarget'] = "請選擇房間 / 物品";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['JSWarning']['AreYouSureWantToCopy'] = "你是否決定進行複製？";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['ReturnMsg']['BookingPeriodCopiedSuccessfully'] = "1|=|複製可預訂時期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['ReturnMsg']['BookingPeriodCopiedFailed'] = "0|=|複製可預訂時期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['RemoveAll']['TotalNumOfBookingPeriodRule'] = "規則總數";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['RemoveAll']['WarningMsg']['AllBookingPeriodWillBeDeleted'] = "以下所有規則將被永久移除，而且無法恢復。如確定要刪除，請按<font style='font-size:large'>刪除全部</font>。";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['RemoveAll']['JSWarning']['DeleteAllBookingPeriod'] = "是否確定刪除所有規則？";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InputDateRange'] = "請輸入日期範圍";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidDurationTime'] = "持續時間無效";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidSettingBookingPeriodTime'] = "預訂時間無效";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidDateRange'] = "日期範圍無效";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DateRangebNotInSamePeriod'] = "所選日期範圍並不是在同一時區內";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DateRangebNotInSelectedSchoolYear'] = "所選日期範圍並不是在已選的學年內";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['CycleDayIsNotSet'] = "* 並未設定循環日";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['BookingPeriodIsExist'] = "系統已有相似的可預訂時期。";
$Lang['eBooking']['Settings']['DefaultSettings']['JSMsg']['SureToDeleteBookingPerion'] = "是否確定要刪除該可預訂時期？";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['AssignNewBookingPeriod'] = "系統只允許你指派一個預訂時間到任何一日。所選時段部份或全部日子，已獲指派預定時段。你是否確定要指派新的預定時段？";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidTimeSelection'] = "時間無效";

$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['WeekdayNotInDateRange'] = "所選之星期並不在日期範圍之內";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodCreatedSuccessfully'] = "1|=|新增可預訂時期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodCreatedFailed'] = "0|=|新增可預訂時期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodEditSuccessfully'] = "1|=|修改可預訂時期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodEditFailed'] = "0|=|修改可預訂時期失敗";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodDeletedSuccessfully'] = "1|=|刪除可預訂時期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodDeletedFailed'] = "0|=|刪除可預訂時期失敗";
$Lang['eBooking']['Settings']['ManagementGroup']['NewGroup'] = "新增小組";
$Lang['eBooking']['Settings']['ManagementGroup']['EditGroup'] = "編輯小組";
$Lang['eBooking']['Settings']['ManagementGroup']['MgmtGroupList'] = "管理小組列表";
$Lang['eBooking']['Settings']['ManagementGroup']['GroupName'] = "小組名稱";
$Lang['eBooking']['Settings']['ManagementGroup']['Description'] = "描述";
$Lang['eBooking']['Settings']['ManagementGroup']['NumOfMembers'] = "成員數量";
$Lang['eBooking']['Settings']['ManagementGroup']['NewGroupStepArr']['Step1'] = "小組資料";
$Lang['eBooking']['Settings']['ManagementGroup']['NewGroupStepArr']['Step2'] = "選擇小組成員";
$Lang['eBooking']['Settings']['ManagementGroup']['ChooseMember'] = "選擇成員";
$Lang['eBooking']['Settings']['ManagementGroup']['FromClassOrGroup'] = "由班別或小組";
$Lang['eBooking']['Settings']['ManagementGroup']['SearchByLoginID'] = "搜尋登入編號";
$Lang['eBooking']['Settings']['ManagementGroup']['SelectedMembers'] = "已選擇成員";
$Lang['eBooking']['Settings']['ManagementGroup']['EditGroup'] = "編輯小組";
$Lang['eBooking']['Settings']['ManagementGroup']['SelectMember'] = "選擇成員";
$Lang['eBooking']['Settings']['ManagementGroup']['ViewMemberList'] = "檢視成員列表";
$Lang['eBooking']['Settings']['ManagementGroup']['MemberList'] = "成員列表";
$Lang['eBooking']['Settings']['ManagementGroup']['AddMember'] = "增加會員";
$Lang['eBooking']['Settings']['ManagementGroup']['ChooseUser'] = "選擇用戶";
$Lang['eBooking']['Settings']['ManagementGroup']['SelectedUser'] = "已選擇用戶";
$Lang['eBooking']['Settings']['ManagementGroup']['Color'] = "日程顏色";
$Lang['eBooking']['Settings']['ManagementGroup']['JSWarningArr']['DeleteManagementGroup'] = "系統將會同時刪除所有小組成員。你是否確定要刪除已選擇的小組？";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember'] = "已選擇的用戶中包含些小組的成員";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['GroupNameInUse'] = "已有其他小組使用此名稱";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['GroupNameBlank'] = "小組名稱空白";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['DataChangeAffect_eInventory'] = "以下類別由電子資源預定系統及資產管理系統共用。任何變更將同時套用到兩個系統。";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['CannotDeleteCategoryWithItem'] = "已經加入資源項目的類別將不能刪除。";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['AddMgmtGroupSuccess'] = "1|=|新增批核小組成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['AddMgmtGroupFailed'] = "0|=|新增批核小組失敗";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['EditMgmtGroupSuccess'] = "1|=|編輯批核小組成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['EditMgmtGroupFailed'] = "0|=|編輯批核小組失敗";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['DeleteMgmtGroupSuccess'] = "1|=|刪除批核小組成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['DeleteMgmtGroupFailed'] = "0|=|刪除批核小組失敗";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['AddMgmtGroupMemberSuccess'] = "1|=|新增批核小組成員成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['AddMgmtGroupMemberFailed'] = "0|=|新增批核小組成員失敗";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['DeleteMgmtGroupMemberSuccess'] = "1|=|刪除批核小組成員成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['DeleteMgmtGroupMemberFailed'] = "0|=|刪除批核小組成員失敗";

$Lang['eBooking']['Settings']['FollowUpGroup']['NewGroup'] = "新增小組";
$Lang['eBooking']['Settings']['FollowUpGroup']['EditGroup'] = "編輯小組";
$Lang['eBooking']['Settings']['FollowUpGroup']['FollowUpGroupList'] = "跟進小組列表";
$Lang['eBooking']['Settings']['FollowUpGroup']['GroupName'] = "小組名稱";
$Lang['eBooking']['Settings']['FollowUpGroup']['Description'] = "描述";
$Lang['eBooking']['Settings']['FollowUpGroup']['NumOfMembers'] = "成員人數";
$Lang['eBooking']['Settings']['FollowUpGroup']['NewGroupStepArr']['Step1'] = "小組資料";
$Lang['eBooking']['Settings']['FollowUpGroup']['NewGroupStepArr']['Step2'] = "選擇小組成員";
$Lang['eBooking']['Settings']['FollowUpGroup']['ChooseMember'] = "選擇成員";
$Lang['eBooking']['Settings']['FollowUpGroup']['FromClassOrGroup'] = "由班別或小組";
$Lang['eBooking']['Settings']['FollowUpGroup']['SearchByLoginID'] = "搜尋登入編號";
$Lang['eBooking']['Settings']['FollowUpGroup']['SelectedMembers'] = "已選擇成員";
$Lang['eBooking']['Settings']['FollowUpGroup']['EditGroup'] = "編輯小組";
$Lang['eBooking']['Settings']['FollowUpGroup']['SelectMember'] = "選擇成員";
$Lang['eBooking']['Settings']['FollowUpGroup']['ViewMemberList'] = "檢視成員列表";
$Lang['eBooking']['Settings']['FollowUpGroup']['MemberList'] = "成員列表";
$Lang['eBooking']['Settings']['FollowUpGroup']['AddMember'] = "增加會員";
$Lang['eBooking']['Settings']['FollowUpGroup']['ChooseUser'] = "選擇用戶";
$Lang['eBooking']['Settings']['FollowUpGroup']['SelectedUser'] = "已選擇用戶";
$Lang['eBooking']['Settings']['FollowUpGroup']['JSWarningArr']['DeleteFollowUpGroup'] = "系統將會同時刪除所有小組成員。你是否確定要刪除已選擇的小組？";
$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['SelectionContainsMember'] = "已選擇的用戶中包含些小組的成員";
$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['GroupNameInUse'] = "已有其他小組使用此名稱";
$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['GroupNameBlank'] = "小組名稱空白";
$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['DataChangeAffect_eInventory'] = "資產管理行政系統及電子資源預訂系統使用相同的類別設定，如你更新類別設定，更新的設定將會同時影響資產管理行政系統及電子資源預訂系統。";
$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['CannotDeleteCategoryWithItem'] = "如類別已設有物品，你將不能刪除有關類別。";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['AddFollowUpGroupSuccess'] = "1|=|新增跟進小組成功";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['AddFollowUpGroupFailed'] = "0|=|新增跟進小組失敗";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['EditFollowUpGroupSuccess'] = "1|=|編輯跟進小組成功";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['EditFollowUpGroupFailed'] = "0|=|編輯跟進小組失敗";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['DeleteFollowUpGroupSuccess'] = "1|=|刪除跟進小組成功";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['DeleteFollowUpGroupFailed'] = "0|=|刪除跟進小組失敗";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['AddFollowUpGroupMemberSuccess'] = "1|=|新增跟進小組成員成功";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['AddFollowUpGroupMemberFailed'] = "0|=|新增跟進小組成員失敗";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['DeleteFollowUpGroupMemberSuccess'] = "1|=|刪除跟進小組成員成功";
$Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg']['DeleteFollowUpGroupMemberFailed'] = "0|=|刪除跟進小組成員失敗";
$Lang['eBooking']['Settings']['FollowUpGroup']['ArchiveUserRemarks'] = '<span class="red">^</span>已刪除用戶';

$Lang['eBooking']['Settings']['Category']['CurrentPhoto'] = "使用中的相片";
$Lang['eBooking']['Settings']['Category']['AddedFrom_eBooking'] = "於電子資源預訂系統新增";
$Lang['eBooking']['Settings']['Category']['AddedFrom_eInventory'] = "於資產管理行政系統新增";
$Lang['eBooking']['Settings']['BookingCategory']['BookingCategory'] = '預訂用途類別';
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['RuleTitle'] = "規則名稱";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['UserType'] = "用戶類別";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['TeacherAndStaff'] = "教師 / 非教學職務員工";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Students'] = "學生";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Parents'] = "家長";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Target'] = "適用於";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['All'] = "所有";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['UserGroups'] = "-用戶組別-";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookingRules'] = "-預訂權限-";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookingRulesOptions'] = "選項";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NeedApproval'] = "需要批核";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookingWithin'] = "預約限制";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Within'] = "內";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['CanBookForOthers'] = "可代他人預約";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NoLimit'] = "沒有限制";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][1] = "1 星期 (7日)";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][2] = "1 個月 (30日)";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][3] = "1 年 (365日)";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Yes'] = "是";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['No'] = "否";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['DisableBooking'] = '暫停預訂';
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['InsertBookingRuleSuccessfully'] = "1|=|新增規則成功";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['InsertBookingRuleFailed'] = "0|=|新增規則失敗";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditBookingRuleSuccessfully'] = "1|=|編輯規則成功";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditBookingRuleFailed'] = "0|=|編輯規則失敗";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['DeleteBookingRuleSuccessfully'] = "1|=|刪除規則成功";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['DeleteBookingRuleFailed'] = "0|=|刪除規則失敗";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditGeneralPermissionSuccessfully'] = "1|=|編輯一般權限成功";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditGeneralPermissionFailed'] = "0|=|編輯一般權限失敗";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['BookingRuleNameEmpty'] = "請輸入戶預預訂權限名稱";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['AllowBookingDayEmpty'] = "請輸入預約限制日數";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['AllowBookingDayNegative'] = "預約限制日數不能為負數";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['BookingRuleNameIsUsed'] = "戶預預訂權限名稱已被使用";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['PleaseSelectUserTarget'] = "請選擇最少一頂目標";
$Lang['eBooking']['Settings']['GeneralPermission']['ApplyToSelected'] = "套用到所選位置";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'] = "房間";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'] = "物品";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['ItemList'] = "物品列表";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['NewItem'] = "新增物品";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Code'] = "代號";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['NameEn'] = "名稱 (英文)";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['NameCh'] = "名稱 (中文)";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Description'] = "描述";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['DescriptionEn'] = "描述 (英文)";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['DescriptionCh'] = "描述 (中文)";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Location'] = "位置";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingPermission'] = "預訂權限";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['AllowBooking'] = "允許預訂";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['AllowBookingIndependence'] = "允許獨立預訂";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDayBeforehand'] = "需於多少日前預訂";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['AvailableForBooking'] = "提早預約";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Instruction'] = "指示";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Attachment'] = "附件";
$Lang['eBooking']['Settings']['GeneralPermission']['SelectCategory'] = "選擇類別";
$Lang['eBooking']['Settings']['GeneralPermission']['SelectSubCategory'] = "選擇子類別";
$Lang['eBooking']['Settings']['GeneralPermission']['AllSubCategory'] = "所有子類別";
$Lang['eBooking']['Settings']['GeneralPermission']['RemarksArr']['ZeroBookingDayBeforehandMeansNoLimit'] = "\"0\" 意指沒有限制。";
$Lang['eBooking']['Settings']['GeneralPermission']['Set'] = "設定";
$Lang['eBooking']['Settings']['GeneralPermission']['Management'] = "批核";
$Lang['eBooking']['Settings']['GeneralPermission']['FollowUp'] = "跟進";
$Lang['eBooking']['Settings']['GeneralPermission']['LogisticGroup'] = "跟進小組";
$Lang['eBooking']['Settings']['GeneralPermission']['Permission'] = "權限";
$Lang['eBooking']['Settings']['GeneralPermission']['DayBefore'] = "日前";
$Lang['eBooking']['Settings']['GeneralPermission']['Before'] = "前";
$Lang['eBooking']['Settings']['GeneralPermission']['Group(s)'] = "組";
$Lang['eBooking']['Settings']['GeneralPermission']['NoGroupSelected'] = "沒有選擇小組";
$Lang['eBooking']['Settings']['GeneralPermission']['Rule(s)'] = "條規則";
$Lang['eBooking']['Settings']['GeneralPermission']['NoRuleSelected'] = "沒有選擇權限";
$Lang['eBooking']['Settings']['GeneralPermission']['Available'] = "可預訂";
$Lang['eBooking']['Settings']['GeneralPermission']['NotAvailable'] = "不可預訂";
$Lang['eBooking']['Settings']['GeneralPermission']['BatchSelect'] = "指定";
$Lang['eBooking']['Settings']['GeneralPermission']['DeleteItem'] = "刪除物品";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['AddItemSuccess'] = "1|=|新增物品成功";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['AddItemFailed'] = "0|=|新增物品失敗";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['EditItemSuccess'] = "1|=|編輯物品成功";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['EditItemFailed'] = "0|=|編輯物品失敗";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemSuccess'] = "1|=|刪除物品成功";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemFailed'] = "0|=|刪除物品失敗";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentSuccess'] = "1|=|刪除附件成功";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentFailed'] = "0|=|刪除附件失敗";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['UploadItemAttachmentSuccess'] = "1|=|上載附件成功";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['UploadItemAttachmentFailed'] = "0|=|上載附件失敗";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectCategory'] = "請選擇類別。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectSubCategory'] = "請選擇子類別。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectLocation'] = "請選擇位置。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneLocation'] = "請最少選擇一個位置。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup'] = "請最少選擇一個管理小組。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneFollowUpGroup'] = "請最少選擇一個跟進小組。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['BookingDayBeforehandIsNotValid'] = "預訂日前無效";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneUserBookingRule'] = "請最少選擇一個用戶預訂權限。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputItemCode'] = "請輸入編碼。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputNameEng'] = "請輸入英文名稱。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputNameChi'] = "請輸入中文名稱。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['CodeIsInUse'] = "已有其他物品使用此編碼。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['CannotGoToEditMode'] = "因沒有物品在此類別，所以未能進入編輯模式。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['DeleteItem'] = "你是否確定你要刪除此物品？";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAllowBooking'] = "請選擇一個選項。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['ItemDeleteFailed_AlreadyExistInPandingBooking'] = "刪除物品失敗 - 物品已被用戶預定。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['DeleteAttachment'] = "你是否確定你要刪除此附件？";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAttachment'] = "請選擇文件。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputPositiveInterger'] = "請輸入正整樓";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Facilities'] = "設備";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Facility'] = "附屬物品";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['UserPermission'] = "用戶權限";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['UserBookingRule'] = "用戶預訂權限";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['RoomBookingRule'] = "場地預訂規則";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Quantity'] = "數量";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['IncludedWhenBooking'] = "已包含在內";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['ShowForReference'] = "可選物品";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['PleaseSelectCategory'] = "請選擇類別。";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['PleaseSelectLocation'] = "請選擇位置。";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Days'] = "日";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Day'] = "日";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'] = array(
		array(0,"0日"),
		array(1,"1日"),
		array(2,"2日"),
		array(3,"3日"),
		array(4,"4日"),
		array(5,"5日"),
		array(6,"6日"),
		array(7,"7日"),
);
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark'] = "備註";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RoomOrItem'] = "房間 / 物品";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Date(s)'] = "日期";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Time'] = "時間";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ResponsiblePerson'] = "使用者";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookedBy'] = "預訂者";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'] = "狀況";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'] = "等待批核";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'] = "批核";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Pending'] = "待批";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'] = "不批核";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_iCalTemporyRecord'] = "iCalendar暫存紀錄";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory'] = "iCalendar暫存紀錄";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'] = "日前";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['SingleBooking'] = "單次預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RepeatedBooking'] = "重複預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['CurrentDayRequest'] = "今天的預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllComingRequest'] = "將來的預訂申請";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['PastRequest'] = "過往的預訂申請";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['SpecificDateRange'] = "指定日期範圍";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ThisWeek'] = "本星期的預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['NextWeek'] = "下星期的預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ThisMonth'] = "本月的預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['NextMonth'] = "下一個月的預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllStatus'] = "所有狀況";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup'] = "所有批核小組";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System'] = "系統";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ReservedByAdmin'] = "<font color='red'>*</font> - 由eBooking Admin預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['CancelledByBookedUser'] = "<font color='red'>^</font> - 由預訂者取消";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordApprovedSuccessfully'] = "1|=|已成功批核有關預訂。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordApprovedFailed'] = "0|=|未能成功批核有關預訂。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordRejectedSuccessfully'] = "1|=|已成功拒絕有關預訂。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordRejectedFailed'] = "0|=|未能成功拒絕有關預訂。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordDeleted'] = "1|=|已成功刪除有關預訂。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordDeletedFailed'] = "0|=|未能成功刪除有關預訂。";
$Lang['eBooking']['Settings']['BookingRequest']['JSWarning']['RequestRejected_TimeOverLapped'] = "預訂未能批核 - 該時段已被預訂。";
$Lang['eBooking']['Settings']['BookingRequest']['JSWarning']['RequestRejected_AlreadyBooked'] = "預訂未能批核 - 物品/房間已被預訂";
$Lang['eBooking']['Settings']['BookingRequest']['JSWarning']['DeleteRejected_AlreadyApproved'] = "預訂未能刪除 - 該預訂已被批核";
$Lang['eBooking']['Import']['WarningMessage']['LocationNotFound'] = '場地不存在';
$Lang['eBooking']['Import']['WarningMessage']['ReserveTimeInvalid'] = '預訂時間不正確';
$Lang['eBooking']['Import']['WarningMessage']['ReserveTimeFormatInvalid'] = '預訂時間格式不正確';
$Lang['eBooking']['Import']['WarningMessage']['UserNotFound'] = '用戶不存在';
$Lang['eBooking']['Import']['WarningMessage']['NotInBookableTimePeriod'] = '不在可供預訂時段內';
$Lang['eBooking']['Import']['WarningMessage']['TimeClash'] = '與其他已批核預訂時間衝突';
$Lang['eBooking']['Import']['WarningMessage']['SelfTimeClash'] = '與其他預訂時間衝突  (行: <!!-RowNumber-!!>)';
$Lang['eBooking']['Import']['WarningMessage']['RoomNotBookable'] = '場地不可預訂';
$Lang['eBooking']['Import']['WarningMessage']['TimeInterval'] = '請以 <!!-TimeInterval-!!> 分鐘時間間隔進行預訂';
$Lang['eBooking']['Import']['Remarks']['Date'] = '請使用格式 YYYY-MM-DD 或者 DD/MM/YYYY';
$Lang['eBooking']['Import']['Remarks']['User'] = '如閣下為場地使用者請留空';
$Lang['eBooking']['Import']['Remarks']['Time'] = '24小時時間格式 - hh:mm';
$Lang['eBooking']['Admin']['ReserveBookingAdminOverwriteWarning'] = '以下已批核的預訂將會被覆寫';
$Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord'] = "我的預訂紀錄";
$Lang['eBooking']['eService']['FieldTitle']['NewBooking'] = "新增預訂";
$Lang['eBooking']['eService']['FieldTitle']['AllBooking'] = "所有預訂";
$Lang['eBooking']['eService']['FieldTitle']['Item'] = "物品";
$Lang['eBooking']['eService']['FieldTitle']['Room'] = "房間";
$Lang['eBooking']['eService']['FieldTitle']['SelectStatus'] = "選擇狀況";
$Lang['eBooking']['eService']['FieldTitle']['PastRecord'] = "過去預訂紀錄";
$Lang['eBooking']['eService']['FieldTitle']['ComingBooking'] = "將來預訂紀錄";
$Lang['eBooking']['eService']['FieldTitle']['SelectedDate'] = "所選日期";
$Lang['eBooking']['eService']['FieldTitle']['Date(s)'] = "日期";
$Lang['eBooking']['eService']['FieldTitle']['Time'] = "時間";
$Lang['eBooking']['eService']['FieldTitle']['SelectedTime'] = "所選時間";
$Lang['eBooking']['eService']['FieldTitle']['AvailablePeriod'] = "可用時段";
$Lang['eBooking']['eService']['FieldTitle']['SpecifyPeriod'] = "指定課節";
$Lang['eBooking']['eService']['FieldTitle']['TimeSlotIsNotAvailablePeriod'] = "不是可用時段";
$Lang['eBooking']['eService']['FieldTitle']['SpecificTimeRange'] = "指定時間範圍";
$Lang['eBooking']['eService']['FieldTitle']['SpecifyTime'] = "指定時間";
$Lang['eBooking']['eService']['FieldTitle']['ItemsIncluded'] = "已包括物品";
$Lang['eBooking']['eService']['FieldTitle']['YouMayAlsoLikeToBook'] = "你也可預訂以下物品";
$Lang['eBooking']['eService']['FieldTitle']['SelectMore'] = "更多選擇";
$Lang['eBooking']['eService']['FieldTitle']['CancelAllBookingsIfRejectedByAdmin'] = "如管理員拒絕任何一件物品的預訂，所有預訂紀錄將自動取消。";
$Lang['eBooking']['eService']['FieldTitle']['CancelDayBookingIfOneItemIsNA'] = "如任何一件物品未能預訂，當天的預訂將自動取消。";
$Lang['eBooking']['eService']['FieldTitle']['ResponsiblePerson'] = "使用者";
$Lang['eBooking']['eService']['FieldTitle']['BookedBy'] = "已預訂";
$Lang['eBooking']['eService']['FieldTitle']['DoNotHaveItemBookingRight'] = "你沒有此物品的預訂權限。";
$Lang['eBooking']['eService']['FieldTitle']['ShowDetails'] = "顯示詳細資料";
$Lang['eBooking']['eService']['FieldTitle']['Period'] = "時段";
$Lang['eBooking']['eService']['FieldTitle']['Status_WaitingForApproval'] = "等待批核";
$Lang['eBooking']['eService']['FieldTitle']['Status_Approved'] = "已批核";
$Lang['eBooking']['eService']['FieldTitle']['Status_Rejected'] = "已拒絕";
$Lang['eBooking']['eService']['FieldTitle']['ExistingBookingRecord'] = "現有預訂紀錄";
$Lang['eBooking']['eService']['FieldTitle']['RequestsList'] = "預訂列表";
$Lang['eBooking']['eService']['FieldTitle']['Day'] = "日";
$Lang['eBooking']['eService']['FieldTitle']['Week'] = "週";
$Lang['eBooking']['eService']['FieldTitle']['FacilityType'] = "預訂類別";
$Lang['eBooking']['eService']['FieldTitle']['StartTime'] = "開始時間";
$Lang['eBooking']['eService']['FieldTitle']['EndTime'] = "結束時間";
$Lang['eBooking']['eService']['FieldTitle']['SelectDateOfBooking'] = "選擇要預訂的日期";
$Lang['eBooking']['eService']['FieldTitle']['ConfirmDatesOfBooking'] = "確認預定日期";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectTimeSlot'] = "請選擇時段。";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectDate'] = "請選擇一個或多個日子。";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectItem'] = "請選擇物品";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectRoom'] = "請選擇房間";
$Lang['eBooking']['eService']['jsWarningArr']['GoBackViewModeAlert'] = "如現在轉回檢視模式，所有剛更改的資料將會失去。你是否確定要轉回檢視模式？";
$Lang['eBooking']['eService']['JSWarningArr']['InvalidDurationTime'] = "請輸入相隔最少五分鐘的時間。";
$Lang['eBooking']['eService']['JSWarningArr']['InvalidDurationTimeAndDoNotOvelap'] = "請輸入正確的間隔時間，並不要重疊同一日的間隔時間。";
$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotEdit'] = "預訂不能修改 - 預訂已被批核。";
$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotCancel'] = "預訂不能取消 - 預訂等待批核中。";
$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotDelete'] = "預訂不能刪除 - 預訂已被批核。";
$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectOneRequestOnly'] = "只可選擇一個預訂紀錄。";
$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectAtLeaseOneRequest'] = "請選擇其中一個預訂紀錄。";
$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputCalendarEventTitle'] = "請輸入事件名稱";
$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputCalendarEventTitle'] = "請輸入事件";
$Lang['eBooking']['eService']['MyBookingRecord']['JSWarning']['ConfirmDelete'] = "所以已選取的紀錄將被刪除, 繼續?";
$Lang['eBooking']['eService']['MyBookingRecord']['JSWarning']['ConfirmCancel'] = "所以已選取的紀錄將被取消, 繼續?";

$Lang['eBooking']['eService']['jsMsg']['ConfirmRemoveDates'] = "已選擇的日期會被移除,繼續?";
$Lang['eBooking']['eService']['WarningArr']['NoTimetableSettings'] = "所選擇的日期未有設定時間表。";
$Lang['eBooking']['eService']['WarningArr']['NotimeSlotInTheTimetable'] = "所選擇日期的時間表未有設定時段。";
$Lang['eBooking']['eService']['WarningArr']['SelectedDatesInDifferentTimezone'] = "所選擇日期在不同的時區。";
$Lang['eBooking']['eService']['WarningArr']['SelectedDatesInDifferentTimetable'] = "所選擇日期在不同的時間表。";
$Lang['eBooking']['eService']['ReturnMsg']['NewBookingSuccess'] = "1|=|新增預訂成功";
$Lang['eBooking']['eService']['ReturnMsg']['NewBookingFailed'] = "0|=|新增預訂失敗";
$Lang['eBooking']['eService']['ReturnMsg']['CancelBookingSuccess'] = "1|=|取消預訂成功";
$Lang['eBooking']['eService']['ReturnMsg']['CancelBookingFailed'] = "0|=|取消預訂失敗";
$Lang['eBooking']['eService']['StepArr']['NewBooking'][0] = "選擇日期";
$Lang['eBooking']['eService']['StepArr']['NewBooking'][1] = "選擇時段";
$Lang['eBooking']['eService']['StepArr']['NewBooking'][2] = "選擇物品";

$Lang['eBooking']['eService']['StepArr']['NewBooking']['SelectDate'] = "選擇日期";
$Lang['eBooking']['eService']['StepArr']['NewBooking']['SelectPeriod'] = "選擇時段";
$Lang['eBooking']['eService']['StepArr']['NewBooking']['SelectTime'] = "選擇時間";
$Lang['eBooking']['eService']['StepArr']['NewBooking']['SelectItem'] = "選擇物品";
$Lang['eBooking']['eService']['StepArr']['NewBooking']['InputOtherInformation'] = "輸入其他資料";

$Lang['eBooking']['eService']['Cycle'] = "循環日";
$Lang['eBooking']['eService']['CycleDay'] = "循環日";
$Lang['eBooking']['eService']['Select'] = "選擇";
$Lang['eBooking']['eService']['Cancel'] = "取消";
$Lang['eBooking']['eService']['Edit'] = "修改";
$Lang['eBooking']['eService']['Ref'] = "參考";
$Lang['eBooking']['eService']['Available'] = "可用";
$Lang['eBooking']['eService']['NotAvailable'] = "不可用";
$Lang['eBooking']['eService']['ViewAvaliablePeriod'] = "檢視可用時段";
$Lang['eBooking']['eService']['Search'] = "搜尋";
$Lang['eBooking']['eService']['SelectCategorie(s)'] = "選擇子類別";
$Lang['eBooking']['eService']['hrs'] = "小時";
$Lang['eBooking']['eService']['mins'] = "分鐘";
$Lang['eBooking']['eService']['CheckAvailable'] = "檢查";
$Lang['eBooking']['eService']['FieldTitle']['CreateCalendarEvent'] = "在\"我的行事曆\"中新增事件";
$Lang['eBooking']['eService']['FieldTitle']['CalendarEventTitle'] = "事件名稱";
$Lang['eBooking']['eService']['FieldTitle']['ReservedByAdmin'] = "<font color='red'>*</font> - 由eBooking Admin預訂";

$Lang['eBooking']['Mail']['FieldTitle']['DearSirOrMadam'] = "閣下的預訂批核結果如下:";
$Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] = "預訂日期";
$Lang['eBooking']['Mail']['FieldTitle']['StartTime'] = "開始時間";
$Lang['eBooking']['Mail']['FieldTitle']['EndTime'] = "結束時間";
$Lang['eBooking']['Mail']['FieldTitle']['Remark'] = "備註";
$Lang['eBooking']['Mail']['FieldTitle']['RoomsResult'] = "房間結果";
$Lang['eBooking']['Mail']['FieldTitle']['RoomsResult'] = "結果";
$Lang['eBooking']['Mail']['FieldTitle']['ResultOfYourBookigRequest'] = "資源預訂批核結果";
$Lang['eBooking']['Mail']['FieldTitle']['WaitForApprovalRequest'] = "資源預訂批核通知";

$Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'] = "由eBooking選擇";
$Lang['eBooking']['iCal']['FieldTitle']['PleaseSelectLocation'] = " -- 選擇地點 -- ";
$Lang['eBooking']['iCal']['FieldTitle']['Approved'] = "已批核";
$Lang['eBooking']['iCal']['FieldTitle']['Pending'] = "等待批核";
$Lang['eBooking']['iCal']['FieldTitle']['Rejected'] = "已拒絕";
$Lang['eBooking']['iCal']['FieldTitle']['SomeApproved'] = "部份已批核";
$Lang['eBooking']['iCal']['FieldTitle']['SomePending'] = "部份等待批核";
$Lang['eBooking']['iCal']['FieldTitle']['SomeRejected'] = "部份已拒絕";
$Lang['eBooking']['iCal']['FieldTitle']['AllApproved'] = "全部已批核";
$Lang['eBooking']['iCal']['FieldTitle']['AllPending'] = "全部等待批核";
$Lang['eBooking']['iCal']['FieldTitle']['AllRejected'] = "全部已拒絕";

$Lang['eBooking']['WithinDays'] = "日內";
$Lang['eBooking']['eService']['JSWarning']['ConfirmDeleteSelectedSlot'] = "所以已選取的時段將被刪除, 繼續?";
$Lang['eBooking']['eService']['FieldTitle']['Others'] = "其他";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Booked'] = "已預訂";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookingDetail'] = "預約詳情";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Attachment'] = "附件";

# Payment Notice Wordings
$Lang['eNotice']['FieldTitle']['NoOfStudentPaidSuccessfully'] = "已成功繳費人數";
$Lang['eNotice']['FieldTitle']['NoOfStudentNeedToPaid'] = "需要繳費人數";
$Lang['eNotice']['FieldTitle']['NoOfItemPaidSuccessfully'] = "已成功繳費項目";
$Lang['eNotice']['FieldTitle']['NoOfItemNeedToPaid'] = "需要繳費項目";
$Lang['eNotice']['ExportTitle']['NoOfStudentPaidSuccessfully'] = "No. of students paid successfully";
$Lang['eNotice']['ExportTitle']['NoOfStudentNeedToPaid'] = "No. of students need to paid";
$Lang['eNotice']['ExportTitle']['NoOfItemPaidSuccessfully'] = "No. of items paid successfully";
$Lang['eNotice']['ExportTitle']['NoOfItemNeedToPaid'] = "No. of items need to paid";
$Lang['eNotice']['FieldTitle']['DebitMethod']['Title'] = "繳費方式";
$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly'] = "呈交時自動扣除所選數額";
$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLLater'] = "校方會於學生智能卡戶口扣除 / 學生拍咭繳付";
$Lang['eNotice']['PaymentTitle'] = "繳費項目名稱";
$Lang['eNotice']['PaymentCategory'] = "繳費類別";
$Lang['eNotice']['Amount'] = "金額";
$Lang['eNotice']['Description'] = "選項";
$Lang['eNotice']['PurchaseQuantity'] = "購買數量";
$Lang['eNotice']['Pay'] = "繳付";
$Lang['eNotice']['NotPaying'] = "不繳付";
$Lang['eNotice']['AccountBalance'] = "帳戶結餘";
$Lang['eNotice']['AnswerSavedSuccessfully'] = "答案儲存成功";
$Lang['eNotice']['AnswerSavedFail'] = "答案儲存失敗";
$Lang['eNotice']['ResignNotice'] = '發還重簽';
$Lang['eNotice']['ResignNoticeSuccess'] = '發還重簽成功';
$Lang['eNotice']['ResignNoticeFail'] = '發還重簽失敗';
$Lang['eNotice']['ResignNoticeConfirm'] = '確認發還重簽?';
$Lang['eNotice']['RejectedBy']='拒絕人';
$Lang['eNotice']['ApprovedBy']='批核者';
$Lang['eNotice']['RejectedTime']='拒絕日期';
$Lang['eNotice']['RejectedComment']='評語';
$Lang['eNotice']['Approve']='批核';
$Lang['eNotice']['ApproveAndIssue']='批核並發出';
$Lang['eNotice']['ApprovalMsg']['eClassApp']['Title']='通告批核通知 eNotice Approval Notification';
$Lang['eNotice']['ApprovalMsg']['eClassApp']['Content']='閣下的通告【<--NoticeNumber--> - <--Title-->】已被批核，通告發出時間:<--DateStart-->
The notice [<--NoticeNumber--> - <--Title-->] have been approved. Issue Date: <--DateStart-->';
$Lang['eNotice']['ApprovalMsg']['email']['Title']='通告批核通知 eNotice Approval Notification';
$Lang['eNotice']['ApprovalMsg']['email']['Content']='<p>閣下的通告【<--NoticeNumber--> - <--Title-->】已被批核，<br>通告發出時間:<--DateStart--> <p> The notice [<--NoticeNumber--> - <--Title-->] have been approved. <br> Issue Date: <--DateStart-->';

$Lang['eNotice']['RejectMsg']['eClassApp']['Title']='通告拒絕通知 eNotice Rejected Notification';
$Lang['eNotice']['RejectMsg']['eClassApp']['Content']='閣下的通告【<--NoticeNumber--> - <--Title-->】已被拒絕，原因如下:<--ApprovalComment-->
The notice [<--NoticeNumber--> - <--Title-->] have been rejected. Rejected reasons: <--ApprovalComment-->';
$Lang['eNotice']['RejectMsg']['email']['Title']='通告拒絕通知 eNotice Rejected Notification';
$Lang['eNotice']['RejectMsg']['email']['Content']='<p>閣下的通告【<--NoticeNumber--> - <--Title-->】已被拒絕，<br>原因如下:<--ApprovalComment--> <p> The notice [<--NoticeNumber--> - <--Title-->] have been rejected. <br> Rejected reasons: <--ApprovalComment-->';
$Lang['eNotice']['Logo']['eNoticeApproval']='通告批核';
$Lang['eNotice']['PaymentResignRemark'] = "如未能成功透過第三方支付服務繳費，請重新簽署此通告。";
$Lang['eNotice']['PaymentNoticeAdminSignForParentSettingRemark'] = "當應用於「繳費通告」 時, 此設定只會在收費管理系統的「到校繳費」設定開啟時有效。";
$Lang['eNotice']['DefaultUserNotifyUsingEmail'] = '預設使用電郵通知用戶';
$Lang['eNotice']['SpecialNotice'] = '特別通告（只限負責人及管理員可檢視詳情）';

$Lang['CampusLink']['VisibleTo'] = "開放予";
$Lang['CampusLink']['VisibleToRemark'] = "＊如果沒有選定任何人士，只有你和本功能的管理員才會看到此連結。";
$Lang['CampusLink']['VisibleNULL'] = "未開放 ";


## Module License
$Lang['ModuleLicense']['RemarkDeletedUser'] = '	已用配額包括已離校及已刪除學生所佔用的{NUMBER}個配額。';
$Lang['ModuleLicense']['ModuleCode'] = '模組碼';
$Lang['ModuleLicense']['Quota'] = '配額';
$Lang['ModuleLicense']['Product'] = '產品';
$Lang['ModuleLicense']['ModuleCreateDate'] = '模組創建日';
$Lang['ModuleLicense']['UsedQuota'] = '已用配額';
$Lang['ModuleLicense']['LicensedStudents'] = '已發牌學生';
$Lang['ModuleLicense']['Title'] = '標題';
$Lang['ModuleLicense']['LicenseManagement'] = '授權管理';
$Lang['ModuleLicense']['Confirm'] = '確定';
$Lang['ModuleLicense']['Close'] = '關閉';
$Lang['ModuleLicense']['StudentSelected'] = '已選學生';
$Lang['ModuleLicense']['RemainingQuota'] = '剩餘配額';
$Lang['ModuleLicense']['Success'] = '成功';
$Lang['ModuleLicense']['Fail'] = '失敗';
$Lang['ModuleLicense']['Total'] = '總共';
$Lang['ModuleLicense']['LastAdded'] = '最後新增';
$Lang['ModuleLicense']['ClassNumber'] = '班號';
$Lang['ModuleLicense']['ClassName'] = '班別';
$Lang['ModuleLicense']['StudentName'] = '學生姓名';
$Lang['ModuleLicense']['ConfirmRemove'] = "你是否確定要取消已選擇學生的授權？";
$Lang['ModuleLicense']['DelInstruction'] = "註：只有過往48小時內獲授權的用戶可以被取消授權。";
$Lang['ModuleLicense']['CancelLicense'] = '取消牌照';

$Lang['ModuleLicense']['NoMoreAvailableQuota'] = "尚餘配額滿";///"No more available quota.";
$Lang['ModuleLicense']['ConfirmAssignStudent'] = "你是否確定要授權已選擇的學生？";
$Lang['ModuleLicense']['ConfirmRemoveAssignStudent'] = "你是否確定要取消已選擇學生的授權？";
$Lang['ModuleLicense']['Err_NoStudentsSelected'] = "請選擇最少一名學生。";
$Lang['ModuleLicense']['Err_ExistInvalidQuota'] = "<font color='red'>配額由於未知的修改無效</font>";

# School Settings > Class
$Lang['SysMgr']['FormClassMapping']['Import'] = "匯入";
$Lang['SysMgr']['FormClassMapping']['Export'] = "匯出";
$Lang['SysMgr']['FormClassMapping']['ExportClassStudent'] = "匯出班學生";
$Lang['SysMgr']['FormClassMapping']['ExportClassTeacher'] = "匯出班主任";
$Lang['SysMgr']['FormClassMapping']['CopyFromOtherAcademicYear'] = "從其他年度複製";
$Lang['SysMgr']['FormClassMapping']['CopyClass'] = "複製班別";
$Lang['SysMgr']['FormClassMapping']['WebSAMS_IsBlank'] = "WebSAMS代號必須輸入";
$Lang['SysMgr']['FormClassMapping']['WebSAMS_Duplicate'] = "WebSAMS代號不能重複";
$Lang['formClassMapping']['ImportExport_Optional']['EN'] = "[Optional]";
$Lang['formClassMapping']['ImportExport_Reference']['EN'] = "[Ref]";
$Lang['formClassMapping']['ImportExport_Optional']['B5'] = "[非必須的]";
$Lang['formClassMapping']['ImportExport_Reference']['B5'] = "[參考用途]";
$Lang['formClassMapping']['ImportTeacher_Column'][] = "<font color='red'>*</font>老師內聯網帳號";
$Lang['formClassMapping']['ImportTeacher_Column'][] = "<font color='red'>^</font>老師姓名";
$Lang['formClassMapping']['ImportTeacher_Column'][] = "<font color='red'>*</font>班別(英文)";
$Lang['formClassMapping']['ImportStudent_Column'][] = "<font color='red'>*</font>學生內聯網帳號";
$Lang['formClassMapping']['ImportStudent_Column'][] = "<font color='red'>^</font>學生姓名";
$Lang['formClassMapping']['ImportStudent_Column'][] = "現時班別(英文)";
$Lang['formClassMapping']['ImportStudent_Column'][] = "現時班號";
$Lang['formClassMapping']['ImportStudent_Column'][] = "<font color='red'>*</font>目標班別(英文)";
$Lang['formClassMapping']['ImportStudent_Column'][] = "<font color='red'>*</font>目標班號";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>級別";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>級別 WebSAMS 代號";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>班別(英文)";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>班別(中文)";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>班別 WebSAMS 代號";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>組別代號";
$Lang['formClassMapping']['ExportClassTeacher_Column'][0][] = "Teacher Login ID";
$Lang['formClassMapping']['ExportClassTeacher_Column'][0][] = "Teacher Name ".$Lang['formClassMapping']['ImportExport_Reference']['EN'];
$Lang['formClassMapping']['ExportClassTeacher_Column'][0][] = "Class Name (EN)";
$Lang['formClassMapping']['ExportClassTeacher_Column'][1][] = "(老師內聯網帳號)";
$Lang['formClassMapping']['ExportClassTeacher_Column'][1][] = "(老師姓名) ".$Lang['formClassMapping']['ImportExport_Reference']['B5'];
$Lang['formClassMapping']['ExportClassTeacher_Column'][1][] = "(班別(英文))";
$Lang['formClassMapping']['ExportClassStudent_Column'][0][] = "Student Login ID";
$Lang['formClassMapping']['ExportClassStudent_Column'][0][] = "Student Name ".$Lang['formClassMapping']['ImportExport_Reference']['EN'];
$Lang['formClassMapping']['ExportClassStudent_Column'][0][] = "Class Name (EN)";
$Lang['formClassMapping']['ExportClassStudent_Column'][0][] = "Class Number ".$Lang['formClassMapping']['ImportExport_Optional']['EN'];
$Lang['formClassMapping']['ExportClassStudent_Column'][1][] = "(學生內聯網帳號)";
$Lang['formClassMapping']['ExportClassStudent_Column'][1][] = "(學生姓名) ".$Lang['formClassMapping']['ImportExport_Reference']['B5'];
$Lang['formClassMapping']['ExportClassStudent_Column'][1][] = "(班別(英文))";
$Lang['formClassMapping']['ExportClassStudent_Column'][1][] = "(班號) ".$Lang['formClassMapping']['ImportExport_Optional']['B5'];
$Lang['formClassMapping']['Reference'] = "附有「<span class='tabletextrequire'>^</span>」的項目參考用途";
$Lang['SysMgr']['FormClassMapping']['ClassCopySuccessfully'] = "1|=|班別複製成功.";
$Lang['formClassMapping']['FormSubject'] = "級別及科目";
$Lang['SysMgr']['FormClassMapping']['FormSubjectSettingNotComplete'] = "<span class='tabletext'> 設定尚未完成，請到 \"<a href='/home/system_settings/subject_class_mapping/form_subject.php'>學校基本設定 > 學科 > 級別及科目</a>\" 完成有關設定。</span></span>";

$eLib['SystemMsg']['AccessDenied'] = "訪問被拒絕";

$Lang['IP_FILE']['ModuleCode'] = "模塊代碼";
$Lang['IP_FILE']['Description'] = "描述";
$Lang['IP_FILE']['InputDate'] = "輸入日期";
$Lang['IP_FILE']['ModifiedDate'] = "更新日期";
$Lang['IP_FILE']['MaximumStorageSize'] = "存儲配額";
$Lang['IP_FILE']['PleaseEnterANumber'] = "請輸入一個數字";
$Lang['IP_FILE']['IPortalFileSettings'] = " 上載設定";

$Lang['IntranetModule']['Using'] = "已使用";

$Lang['eHomework']['Times'] = "次數";
$Lang['eHomework']['IncludeDueDate'] = "包括限期";
$Lang['eHomework']['OnOrAbove'] = "或以上";
$Lang['eHomework']['OnOrBelow'] = "或以下";
$Lang['eHomework']['StartDateWarningMsg'] = "開始日期必須是 %%s%% 或以後。";
$Lang['eHomework']['MissingTitle'] = "請輸入標題";
$Lang['eHomework']['ExportFormat1'] = "格式 1";
$Lang['eHomework']['ExportFormat2'] = "格式 2";
$Lang['eHomework']['ExportFormat2_EN'] = array("Class", "Class Number", "Student Name", "Subject", "Subject Group", "Topic", "Description", "Start Date", "Due Date");
$Lang['eHomework']['ExportFormat2_B5'] = array("班別", "班號", "學生姓名", "學科", "學科組別", "標題", "內容", "開始日期", "限期");
$Lang['eHomework']['ClearHomeworkWarningMsg'] = "下列範圍之功課將被永久移除，而且無法恢復。如確定要刪除，請按<font style='font-size:large'>呈送</font>。";
$Lang['eHomework']['PleaseSelectClass'] = "請選擇班別";
$Lang['eHomework']['PleaseSelectSubjectGroup'] = "請選擇學科組別";
$Lang['eHomework']['ImportWorkload_Desription'] = "<span class='tabletextrequire'>^</span> 以「小時」計算，時段差距為0.25。";
$Lang['eHomework']['OnlyCanEditDeleteOwn'] = "教師/科長只可以編輯/刪除自己建立的家課";
$Lang['eHomework']['SubjectLeaderAddedSuccessfully'] = "1|=|已成功加入科長";
$Lang['eHomework']['GeneralExport'] = "一般匯出";
$Lang['eHomework']['ExportCollectionList'] = "繳交清單";
$Lang['eHomework']['ExportDate'] = "匯出日期";
$Lang['eHomework']['NoOfHomeworkCollect'] = "要收取的功課總數";
$Lang['eHomework']['ViewerGroup'] = "檢視人員組別";
$Lang['eHomework']['ClassTeacherCanViewHomeworkOnly'] = "班主任不可管理自己班別的功課";
$Lang['eHomework']['HandinStatusReport'] = "繳交狀況報告";
$Lang['eHomework']['HandinStatus'] = "繳交狀況";
$Lang['eHomework']['Enabled'] = "啟用";
$Lang['eHomework']['Disabled'] = "停用";
$Lang['eHomework']['Count'] = "次數";
$Lang['eHomework']['SupplementaryCutOffTime'] = "自動產生違規記錄及留堂名單時間";
$Lang['eHomework']['HandInHomework'] = "提交功課";
$Lang['eHomework']['HandedInHomework'] = "已提交功課";
$Lang['eHomework']['NotSubmitted'] = "未提交";
$Lang['eHomework']['Uploading'] = "上載中......";
$Lang['eHomework']['FileSizeLimitRemark'] = "文件大小限制為<!--SIZE-->MB以下";
$Lang['eHomework']['SubmittedTimeRemark'] = "於<!--DATETIME-->提交";
$Lang['eHomework']['TeacherDocumentSubmittedRemark'] = "<!--NAME-->於<!--DATETIME-->提交";
$Lang['eHomework']['Mark'] = "批改";
$Lang['eHomework']['MarkedRemark'] = "<!--NAME-->批改於<!--DATETIME-->";
$Lang['eHomework']['NotMarked'] = "未批改";
$Lang['eHomework']['Rating'] = "評分";
$Lang['eHomework']['NotRated'] = "未評分";
$Lang['eHomework']['RatedRemark'] = "<!--NAME-->評分於<!--DATETIME-->";
$Lang['eHomework']['ConfirmUpdateRatingMsg'] = "確定要更新評分?";
$Lang['eHomework']['HomeworkRecords'] = "家課紀錄";
$Lang['eHomework']['ExportPeriodWithin90Days'] = '匯出時段長度必須在90天以內';
$Lang['eHomework']['ExportHandinHistory'] = '家課繳交紀錄';
$Lang['eHomework']['NormalExport'] = "一般";

$Lang['General']['Normal'] = "普通";
//$Lang['General']['Approved'] = "已准許"; //declared above already
$Lang['General']['s'] = "的家長";
$Lang['General']['ExportOptions'] = "匯出選項";
$Lang['General']['RemindMeLater'] = "稍後提醒我";
$Lang['General']['Close'] = "關閉";
$Lang['General']['Announce'] = "發佈消息";
$Lang['General']['NewFeatures'] = "新功能!";
$Lang['General']['PleaseSelectMembers'] = "請選擇成員";

$Lang['eDiscipline']['FromClass'] = "自 <b>班別</b>";
$Lang['eDiscipline']['DisplayIconInPortal'] = "於主頁顯示「訓導管理」圖示";
$Lang['eDiscipline']['ApprovalGroup'] = "批核小組";
$Lang['eDiscipline']['GroupName'] = "小組名稱";
$Lang['eDiscipline']['GroupDescription'] = "描述";
$Lang['eDiscipline']['NoOfMembers'] = "組員數目";
$Lang['eDiscipline']['hasNoApprovalRight'] = "<font color=red>沒有對此紀錄的批核權限.</font>";
$Lang['eDiscipline']['EmailNotifyParent'] = "以電郵通知家長";
$Lang['eDiscipline']['SignNotice'] = "簽署通告";
$Lang['eDiscipline']['SendNoticeActionMsg'] = "當到處以上的條件 時會自動發送電子通告";
$Lang['eDiscipline']['ManageNoticeSignature'] = "管理通告簽署";
$Lang['eDiscipline']['NoticeFooterArray'] =
array(
		1=>array("校長簽署", "學校蓋章 / <br>發出日期 "),
		3=>array("級聯絡老師", "簽署"),
		4=>array("班主任", "簽署"),
		5=>array("家長 / 監護人姓名", "簽署"),
		2=>array("簽收日期","學生簽署")
);
$Lang['eDiscipline']['UpdateSignatureOption'] = "更新簽署選項";
$Lang['eDiscipline']['RemainSignatureOption'] = "保留簽署選項";
$Lang['eDiscipline']['PleaseSelectAtLeastOneNotice'] = "請最少選擇一個通告";
$Lang['eDiscipline']['SelectFooterWarningMsg'] = "你最少選擇一項簽名選項";
$Lang['eDiscipline']['AlertMsgNumericInput'] = "請輸入數字";
$Lang['eDiscipline']['CopyGMPeriodSetting'] = "複製自現有時段計算方案";
$Lang['eDiscipline']['ConfirmToCopyPeriodSetting'] = "你是否想複製選擇的計算方案至這個時段?";
$Lang['eDiscipline']['PleaseSelectAtLeaseOneAward'] = "請最少選擇一項獎勵";


$Lang['Cust_Npl']['Non-AcademicAchievementReport'] = '學生課外活動表';
$i_eClass_Admin_MgmtCenter = "特別室管理中心";
$i_admintitle_eclass = "特別室";

$Lang['AdminConsole']['KeepStudentQuizResult'] = "保留學生測驗成績";
$Lang['AdminConsole']['i_StudentPromotion_iPortfolioRemind'] = "<b><u>注意:</u></b><br><ul><li>將會新增本年度的班別組別。</li><li>在 iPortfolio 中, 學生會按年度及所屬班別被分組。本程序會將學生加入到升班後所屬的年度及班別組別，而舊有的組別將一同保留。例如某同學在新學年前的所屬組別是 \"2005-2006 3A\"，在進行升班程序後，她將會被加入到 \"2006-2007 4A\"（但你仍然可從 \"2005-2006 3A\" 組別中找到該名學生）。</ul></li>";

# admin console
$Lang['ns_logout_alert'] = '請關閉所有瀏覽器視窗以登出';
$Lang['logout_alert'] ='由於 Microsoft Internet Explorer 不再支援一種特別的 URL 格式 (Security Update: 832894)，登出系統時，請關閉所有瀏覽器視窗。詳情請參閱 http://support.microsoft.com/default.aspx?scid=kb%3ben-us%3b834489';

# Cheung Sha Wan Catholic Secondary School
$Lang['SchoolName']['CSWCCS']['CH'] = "長沙灣天主教英文中學";
$Lang['SchoolName']['CSWCCS']['EN'] = "CHEUNG SHA WAN CATHOLIC SECONDARY SCHOOL";
$Lang['eDiscipline']['DetentionClass'] = "留堂班";
$Lang['eDiscipline']['Offence'] = "違規";
$Lang['eDiscipline']['SentBy'] = "負責人";
$Lang['eDiscipline']['Attend'] = "出席<br>P/A";
$Lang['eDiscipline']['ReplySlip'] = "回條<br>P/A";
$Lang['eDiscipline']['NewDetentionDate'] = "新留堂日期";
$Lang['eDiscipline']['Total'] = "共";
$Lang['eDiscipline']['Records'] = "個紀錄";
$Lang['eDiscipline']['PrefectInCharge'] = "負責領袖生";
$Lang['eDiscipline']['CheckBy'] = "覆核";
$Lang['eDiscipline']['CSWCSS_DetentionMsg_1'] = "查學生 ";
$Lang['eDiscipline']['CSWCSS_DetentionMsg_2'] = " 因 ";
$Lang['eDiscipline']['CSWCSS_DetentionMsg_3'] = "需於 ";
$Lang['eDiscipline']['CSWCSS_DetentionMsg_4'] = " 留堂，以示懲戒。";
$Lang['eDiscipline']['CSWCSS_DetentionMsg_5'] = "訓導處謹啓";
$Lang['eDiscipline']['ReferenceNumber'] = "參考編號";
$Lang['eDiscipline']['RecordOfMisbehavedStudent']['CH'] = "Record of Misbehaved Student";
$Lang['eDiscipline']['RecordOfMisbehavedStudent']['EN'] = "學生違規行為紀錄";
$Lang['eDiscipline']['FormA'] = "Form A";
$Lang['eDiscipline']['NameOfStudent'] = "Name of Student<br>(學生姓名)";
$Lang['eDiscipline']['ClassOfStudent'] = "Class<br>(班別)";
$Lang['eDiscipline']['ContactInformation'] = "Contact Information (聯絡資料)";
$Lang['eDiscipline']['RefNo'] = "Ref No.";
$Lang['eDiscipline']['HomePhone'] = "住宅電話";
$Lang['eDiscipline']['Father'] = "父親";
$Lang['eDiscipline']['Mother'] = "母親";
$Lang['eDiscipline']['DescriptionOfIncident'] = "Description of Incident (事件)";
$Lang['eDiscipline']['Event'] = "事件性質";
$Lang['eDiscipline']['DateTime'] = "日期、時間";
$Lang['eDiscipline']['Place'] = "地點";
$Lang['eDiscipline']['EventDescription'] = "事件描述";
$Lang['eDiscipline']['ReportedBy'] = "Reported by<br>(提供資料老師)";
$Lang['eDiscipline']['Date_Bilingual'] = "Date<br>(日期)";
$Lang['eDiscipline']['StudentBackground'] = "Student Background Information 學生背景資料 (由班主任填寫)";
$Lang['eDiscipline']['Discuss'] = "本人希望參與討論懲罰過程";
$Lang['eDiscipline']['FormTeacher'] = "Form Teacher<br>(班主任姓名)";
$Lang['eDiscipline']['InvestigationByDC'] = "Investigation conducted by D.C. 紀律小組調查結果";
$Lang['eDiscipline']['Results'] = "Resutls (調查結果)";
$Lang['eDiscipline']['Punishment'] = "Punishment (處罰)";
$Lang['eDiscipline']['InformedParent'] = "已通知家長";
$Lang['eDiscipline']['PIC'] = "經手人";
$Lang['eDiscipline']['Date'] = "日期";
$Lang['eDiscipline']['Time'] = "時間";
$Lang['eDiscipline']['Remark'] = "備註";
$Lang['eDiscipline']['Referral'] = "須轉介輔導小組/社工/心理學家/教署/警方 (請選擇)";
$Lang['eDiscipline']['OtherReferral'] = "其他部門";
$Lang['eDiscipline']['TeacherInCharge'] = "Teacher-in-charge<br>(小組簽署)";
$Lang['eDiscipline']['PrincipalDecision'] = "Principal's Decision 校長裁決 (如適用)";
$Lang['eDiscipline']['PrincipalSignature'] = "Principal<br>(校長簽署)";
$Lang['eDiscipline']['RemindialAction'] = "Remindial Action Taken by Guidance Team 輔導小組跟進 (如適用)";
$Lang['eDiscipline']['NameOfTeacherInCharge'] = "Name of Teacher-in-charge<br>(小組老師姓名)";
$Lang['eDiscipline']['Signature'] = "Signature (簽署)";
$Lang['eDiscipline']['IndividualStudentDisciplinaryRecord'] = "學生個人紀律報告";
$Lang['eDiscipline']['Tardiness'] = "遲到";
$Lang['eDiscipline']['AM'] = "(上)";
$Lang['eDiscipline']['PM'] = "(下)";
$Lang['eDiscipline']['SKSS']['StudentAwardReport'] = "學生優良表現嘉許通知書";
$Lang['eDiscipline']['SKSS']['CompulsoryInReport'] = "必然顯示於報告中";
$Lang['eDiscipline']['SKSS']['IssueDate'] = "派發日期(顯示於簽署欄)";
$Lang['eDiscipline']['SKSS']['PeriodDescription'] = "學段";
$Lang['eDiscipline']['SKSS']['Signature'] = "簽署";
$Lang['eDiscipline']['SKSS']['Code'] = "編號";
$Lang['eDiscipline']['SKSS']['GoodPerformance'] = "優良表現";
$Lang['eDiscipline']['SKSS']['Receive'] = "獎勵";
$Lang['eDiscipline']['SKSS']['Post'] = "服務崗位 / 備註";
$Lang['eDiscipline']['SKSS']['GoodConduct'] = "良好行為";
$Lang['eDiscipline']['SKSS']['Mark_1'] = "記";
$Lang['eDiscipline']['SKSS']['Mark_2'] = "個";
$Lang['eDiscipline']['SKSS']['RainbowScheme'] = "彩虹計劃(缺點)合格者";
$Lang['eDiscipline']['SKSS']['WaivedDemerit'] = "抵銷缺點";
$Lang['eDiscipline']['SKSS']['MeritType'] = array("","優點","小功","大功","超功","極功");
$Lang['eDiscipline']['NoticeFooterArrayForStudentAwardReport'] =
array(
		1=>array("班主任老師", "學校蓋章 / <br>發出日期 "),
		3=>array("級聯絡老師", "簽署"),
		5=>array("家長 / 監護人姓名", "簽署"),
		2=>array("簽收日期","學生簽署")
);
$Lang['eDiscipline']['NoticeFooterArrayForStudentAwardReport_Bilingual'] =
array(
		1=>array("班主任老師", "學校蓋章 / <br>發出日期 "),
		3=>array("級聯絡老師", "簽署"),
		5=>array("家長 / 監護人姓名", "簽署"),
		2=>array("簽收日期","學生簽署")
);
$Lang['eDiscipline']['SKSS']['SchoolName']['CH'] = "基 督 教 宣 道 會 宣 基 中 學";
$Lang['eDiscipline']['SKSS']['SchoolName']['EN'] = "Christian & Missionary Alliance Sun Kei Secondary School";
$Lang['eDiscipline']['SKSS']['ConductPotentialsAssessmentReport']['Name'] = "品德及潛能評估報告表";
$Lang['eDiscipline']['SKSS']['ConductPotentialsAssessmentReport']['Heading'] = "CONDUCT & POTENTIALS ASSESSMENT REPORT 品德及潛能評估報告表";
$Lang['eDiscipline']['SKSS']['Name'] = "NAME 姓名";
$Lang['eDiscipline']['SKSS']['Class'] = "CLASS 班別";
$Lang['eDiscipline']['SKSS']['RegNo'] = "Reg. No 註冊號碼";
$Lang['eDiscipline']['SKSS']['AssessmentItems'] = "ASSESSMENT ITEMS 評估項目";
$Lang['eDiscipline']['SKSS']['GradeDescription'] = "(Grade 等級 : A 優 B 良  C 常 D 差 E 劣)";
$Lang['eDiscipline']['SKSS']['Items'] =
array(
		array("",""),
		array("Self-confidence","自信心"),
		array("Sense of Responsibility","責任感"),
		array("Serving Attitude","服務精神"),
		array("Positive Attitude","正向思想"),
		array("Self-learning Ability","自學能力"),
		array("Self-discipline Ability","自律能力"),
		array("Inter-personal Communication","人際溝通"),
		array("Problem-solving Ability","解難能力"),
		array("Creative Thinking","創意思維"),
		array("Leadership","領導才能")
);
$Lang['eDiscipline']['SKSS']['Items_Junior'] =
array(
		array("",""),
		array("Honesty","誠信"),
		array("Politeness","禮貌"),
		array("Sense of Responsibility","責任感"),
		array("Tidiness & Cleanliness","儀容整潔"),
		array("Serving Attitude","服務精神"),
		array("Self-discipline Ability","自律能力"),
		array("Inter-personal Communication","人際溝通"),
		array("Problem-solving Ability","解難能力"),
		array("Creative Thinking","創意思維"),
		array("Leadership","領導才能")
);
$Lang['eDiscipline']['SKSS']['MeritsDemerits'] = "MERITS & DEMERITS 功過紀錄";
$Lang['eDiscipline']['SKSS']['Particulars'] = "PARTICULARS 詳情";
$Lang['eDiscipline']['SKSS']['NoOfMerits'] = array("No. of Merits", "優點數目");
$Lang['eDiscipline']['SKSS']['NoOfMinorHonour'] = array("No. of Minor Honour", "小功數目");
$Lang['eDiscipline']['SKSS']['NoOfMajorHonour'] = array("No. of Major Honour", "大功數目");
$Lang['eDiscipline']['SKSS']['NoOfDemerits'] = array("No. of Demerits", "缺點數目");
$Lang['eDiscipline']['SKSS']['NoOfMinorOffence'] = array("No. of Minor Offence", "小過數目");
$Lang['eDiscipline']['SKSS']['NoOfMajorOffence'] = array("No. of Major Offence", "大過數目");
$Lang['eDiscipline']['SKSS']['ConductPotentialSignature'][] = array("PRINCIPAL","校長");
$Lang['eDiscipline']['SKSS']['ConductPotentialSignature'][] = array("CLASS TEACHER","班主任");
$Lang['eDiscipline']['SKSS']['ConductPotentialSignature'][] = array("PARENT / GUARDIAN","家長 / 監護人");
$Lang['eDiscipline']['SKSS']['ConductPotentialSignature'][] = array("DATE OF ISSUE","派發日期");
$Lang['eDiscipline']['SKSS']['GradeOption'] = array("","A+","A","A-","B+","B","B-","C+","C","C-","D+","D","D-","E+","E","E-");
$Lang['eDiscipline']['SKSS']['Principal'] = "潘淑嫻 女士";
$Lang['SysMgr']['eDiscipline']['SKSS']['SaveSuccess'] = '1|=|評估已被儲存';
$Lang['SysMgr']['eDiscipline']['SKSS']['SaveFail'] = '0|=|評估儲存失敗';
$Lang['eDiscipline']['SKSS']['SaveAndPrint'] = "儲存並列印";
$Lang['eDiscipline']['SKSS']['TemplateManagement'] = "模板管理";
$Lang['eDiscipline']['SKSS']['ClearAssessmentGrade'] = "清除評估評級";
$Lang['eDiscipline']['SKSS']['ClearAssessmentGradeMessage'] = "所有學生之評級將被永久移除，而且無法恢復。如確定要刪除，請按<font style='font-size:large'>呈送</font>。";
$Lang['eDiscipline']['CategoryIsDeletedDisabled'] = "此類別已被刪除或停用";
$Lang['eDiscipline']['ItemIsDeletedDisabled'] = "此項目已被停用或從類別中移除";
$Lang['eDiscipline']['AllDemeritStatus'] = "所有懲罰狀況";
$Lang['eDiscipline']['WithDemerit'] = "有懲罰紀錄";
$Lang['eDiscipline']['WithoutDemerit'] = "未有懲罰紀錄";
$Lang['eDiscipline']['DemeritRecordAddedAlready'] = "已有懲罰紀錄";
$Lang['eDiscipline']['DuplicatedStudent'] = "學生重複";
$Lang['eDiscipline']['DetentionRecordAddPunishment'] = " 個遲到紀錄可新增懲罰紀錄";
$Lang['eDiscipline']['AutoApproveAPRecord'] = "自動批核獎勵及懲罰紀錄";
$Lang['eDiscipline']['IncludeWaivedRecords'] = "包括已豁免紀錄";
$Lang['eDiscipline']['IncludeUnreleasedRecords'] = "包括未開放紀錄";
$Lang['eDiscipline']['IncludeMeritDemeritCount'] = "包括獎勵/懲罰紀錄數目";
$Lang['eDiscipline']['WaviedRecords'] = "豁免記錄";
$Lang['eDiscipline']['UnreleasedRecords'] = "未開放紀錄";
$Lang['eDiscipline']['AllowToAssignStudentToAccessRightGroup'] = "允許學生加入「訓導成員」小組";
$Lang['eDiscipline']['SystemPropertiesNote']['AllowToAssignStudentToAccessRightGroup'] = "如希望允許個別學生(如領袖生)管理訓導紀錄，可啟動此選項。同時，你需要為有關學生於<b>設定>權限</b>畫面新增一個訓導成員小組，並開放相關的紀錄管理權限。";
$Lang['eDiscipline']['ApprovalGroupSettingInstruction'] = "建立批核小組，可以讓學校指派不同職位的用戶負責批核不同程度的獎懲。你可以前往<b>設定 > 獎勵及懲罰 > 獎勵 或 懲罰 > 批核</b>頁面，將已經建立的批核小組，分配到適合的獎懲程度。";
$Lang['eDiscipline']['ApprovalGroupApplyInstruction']['Award'] = "你可以分配批核小組到 適合的獎勵程度。獲分配批核小組 的獎勵程度，其批核權限將不再受<b>設定>權限</b>中的設定所影響。<p style='clear:both'>
由於批核小組只限教師加入，如果 <b>設定 > 系統特性</b>中的「允許學生加入『訓導成員』小組」選項已經啟動，請確保所有的獎勵程度，均沒有被分配任何批核小組。否則即使擁有權限的學生，都不能審批訓導紀錄。";
$Lang['eDiscipline']['ApprovalGroupApplyInstruction']['Punishment'] = "你可以分配批核小組到 適合的懲罰程度。獲分配批核小組 的懲罰程度，其批核權限將不再受<b>設定>權限</b>中的設定所影響。<p style='clear:both'>
由於批核小組只限教師加入，如果<b>設定 > 系統特性</b>中的「允許學生加入『訓導成員』小組」選項已經啟動，請確保所有的懲罰程度，均沒有被分配任何批核小組。否則即使擁有權限的學生，都不能審批訓導紀錄。";
$Lang['eDiscipline']['RecordsNotTransferred'] = "未傳送的紀錄";
$Lang['eDiscipline']['WholeSchoolTotal'] = "全級合計";
$Lang['eDiscipline']['StudentTotal'] = "學生合計";
$Lang['eDiscipline']['CurrentYear'] = "本年度";
$Lang['eDiscipline']['PreviousYear'] = "過去年度";
$Lang['eDiscipline']['Archived'] = "已封存";
$Lang['eDiscipline']['AP_Approval_Number_Note_1'] = "如發出的獎勵/懲罰相等或大於此數目，紀錄將需要批核。如無需批核，請選 擇「不設定」。";
$Lang['eDiscipline']['AP_Approval_Number_Note_2'] = "如選擇「不設定」，系統將根據<b>設定 > 權限 > 訓導成員</b>的設定處理批核權限。";
$Lang['eDiscipline']['AllowToAssignStudentToPreviousSession'] = "容許編排學生到[ ]日前的留堂時段";
$Lang['eDiscipline']['AllowToAssignStudentToPreviousSession_Day'] = "日";
$Lang['eDiscipline']['ConductGradeSetting'] = "操行評級設定";
$Lang['eDiscipline']['ConductAdjustmentBaseMark'] = "操行調整基準分";
$Lang['eDiscipline']['ConductNoticeTarget'] = "提示目標";
//$Lang['eDiscipline']['ConductAdjustmentGrade'] = "操行調整等級";
//$Lang['eDiscipline']['NumOfMisconductToAdjustGrade'] = "調整操行等級違規行為數量";
$Lang['eDiscipline']['ConductMarkExceedLimitRemark'] = "操行分超出上下限備註";
$Lang['eDiscipline']['ConductMarkWarningLimit'] = "操行分警告下限";
$Lang['eDiscipline']['MaxGainMark'] = "加分上限";
$Lang['eDiscipline']['MaxDeductMark'] = "扣分下限";
$Lang['eDiscipline']['MaxConductMark'] = "操行分加分上限";
$Lang['eDiscipline']['MinConductMark'] = "操行分扣分下限";
$Lang['eDiscipline']['ConductDownGradeCondition'] = "操行降級條件";
$Lang['eDiscipline']['AwardMustEarlierThanPunishment'] = "懲罰紀錄日期必須早於獎勵紀錄日期";
$Lang['eDiscipline']['NoLimit'] = "無日期限制";
$Lang['eDiscipline']['Quantity'] = "數量";
$Lang['eDiscipline']['Remark_DownGraded'] = "*被降等級";
$Lang['eDiscipline']['WarningMsgArr']['StudentExceedMeritItemMaxGainConductScore'] = "以下學生已超出項目最高可獎勵的操行分";
$Lang['eDiscipline']['WarningMsgArr']['StudentExceedDemeritItemMaxDeductConductScore'] = "以下學生已超出項目最高可扣減的操行分";
$Lang['eDiscipline']['WarningMsgArr']['StudentExceedSemesterMaxConductMark'] = "以下學生已超出學期最多可獎勵的操行分";
$Lang['eDiscipline']['WarningMsgArr']['StudentExceedSemesterMinConductMark'] = "以下學生已超出學期最多可扣減的操行分";
$Lang['eDiscipline']['WarningMsgArr']['StudentExceedTagMaxGainConductScore'] = "以下學生已超出標籤 [<!--TAG_NAME-->] 最高可獎勵的操行分";
$Lang['eDiscipline']['WarningMsgArr']['StudentExceedTagMaxDeductConductScore'] = "以下學生已超出標籤 [<!--TAG_NAME-->] 最高可扣減的操行分";
$Lang['eDiscipline']['WarningMsgArr']['StudentLowerThanConductMarkWarningLimit'] = "以下學生操行分已低於操行分警告下限 <!--WARN_MARK--> 分";
$Lang['eDiscipline']['WarningMsgArr']['StudentExceedConductMarkLimitConfirmSubmit'] = '請注意已超出操行分上下限的學生將不會再被加減操行分。\n你確定要繼續?';
$Lang['eDiscipline']['WarningMsgArr']['MeritScoreExceeded'] = "獎懲分數超出最高限額";
$Lang['eDiscipline']['WarningMsgArr']['SemesterConductScoreExceeded'] = "學年操行分數超出最高限額";
$Lang['eDiscipline']['WarningMsgArr']['TagScoreExceeded'] = "標籤 [<!--TAG_NAME-->] 分數超出最高限額";
$Lang['eDiscipline']['WarningMsgArr']['InvalidItem'] = "項目無效";
$Lang['eDiscipline']['WarningMsgArr']['InvalidQuantity'] = "數量不正確";
$Lang['eDiscipline']['ExceedMaxConductMark_EmailNotificationTitle'] = "學生超出<!--TYPE-->警告";
$Lang['eDiscipline']['ExceedMaxConductMark_EmailNotificationContent'] = "請注意以下學生已超出<!--TYPE-->: ";
$Lang['eDiscipline']['ConductMarkAttemptToGain'] = "企圖獎賞操行分";
$Lang['eDiscipline']['ConductMarkAttemptToDeduct'] = "企圖扣減操行分";
if($sys_custom['eDiscipline']['WSCSS_Conduct_Grade']) { // [CRM : 2012-0221-1557-57098]
	$Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'][1] = "最終操行分數要多於 [ ##INPUT_VALUE## ] 分或以上";
	$Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'][2] = "不可於系統扣 [ ##INPUT_VALUE## ] 分或以上";
	$Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'][3] = "數項獎勵項目共得的操行分要多於 [ ##INPUT_VALUE## ] 分或以上";
	$Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'][4] = "數項獎勵項目共得的操行分要多於 [ ##INPUT_VALUE## ] 分或以上";
	$Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'][5] = "數項獎勵項目共得的操行分要多於 [ ##INPUT_VALUE## ] 分或以上";
	$Lang['eDiscipline']['WSCSS_Conduct_Grade_Total_Setting'] = "項設定";
	$Lang['eDiscipline']['WSCSS_PleaseSelect_Setting'] = "請選擇設定";
	$Lang['eDiscipline']['WSCSS_ConductMarkMustBeNumber'] = "操行分必須為數字";
	$Lang['eDiscipline']['WSCSS_PleaseSelectAwardItem'] = "請選擇獎勵項目";
}
# [TWGHs Chen Zao Men College] Start
$Lang['eDiscipline']['DetentionNotice'] = "留堂通告";
$Lang['eDiscipline']['TeachersSignature'] = "老師簽署";
$Lang['eDiscipline']['DisciplineGroup'] = "訓導組";
$Lang['eDiscipline']['DetentionNoticeMessage'] = "請吩咐下列學生於第 8 節鐘聲響後(3:45)立即到禮堂正門前出席留堂班:";
$Lang['eDiscipline']['AfterClassTime'] = "下課時間";
$Lang['eDiscipline']['DetentionNoticeReminder'] = "宣讀後請把此通告交回留堂班學生帶返 101 室轉交教師助理存檔。";
$Lang['eDiscipline']['DetentionPunishmentList'] = "留堂懲處名單";
$Lang['eDiscipline']['DetentionStatus'] = "留堂狀況";
$Lang['eDiscipline']['DetentionAbsent'] = "缺席";
$Lang['eDiscipline']['DetentionNotTakeAttandance'] = "未點名";
$Lang['eDiscipline']['DetentionPresent'] = "出席";
$Lang['eDiscipline']['DetentionTimes'] = "次數";
$Lang['eDiscipline']['DetentionTimesAbove'] = "次或以上";
$Lang['eDiscipline']['DetentionDateAndEvent'] = "留堂日期及事件";
$Lang['eDiscipline']['WarningMsgArr']['SelectTarget'] = "請選擇對象。";
$Lang['eDiscipline']['WarningMsgArr']['SelectAttendanceStatus'] = "請選擇出席狀況。";
# [TWGHs Chen Zao Men College] End

# [Creative Secondary School] Start
$Lang['eDiscipline']['DetentionTypes']['Title'] = "留堂種類";
$Lang['eDiscipline']['DetentionTypes']['LunchTime'] = "午飯時間";
$Lang['eDiscipline']['DetentionTypes']['AfterSchool'] = "放學時間";
$Lang['eDiscipline']['DetentionTypes']['Suspension'] = "停學";

$Lang['eDiscipline']['WarningMsgArr']['DateRangeInvalid'] = "開始日期必須為結束日期之前。";
$Lang['eDiscipline']['WarningMsgArr']['TimeRangeInvalid'] = "開始時間必須為結束時間之前。";
$Lang['eDiscipline']['WarningMsgArr']['SelectWeekday'] = "請至少選擇星期中的一日。";
$Lang['eDiscipline']['WarningMsgArr']['StartDateInvalid'] = "開始日期必須是<!--targetDate-->或以後 。";
$Lang['eDiscipline']['WarningMsgArr']['EndDateInvalid'] = "結束日期必須是 <!--targetDate-->或以前。";
$Lang['eDiscipline']['WarningMsgArr']['SelectDetentionTypes'] = "請選擇留堂種類。";
$Lang['eDiscipline']['WarningMsgArr']['NoMatchDays'] = "沒有符合日期.";

$Lang['eDiscipline']['OnEvery'] = "逄星期";
$Lang['eDiscipline']['RepeatDatePeriod']['Title'] = "每星期重複";
$Lang['eDiscipline']['RepeatDatePeriod']['ShortWeekday'][0] = "日";
$Lang['eDiscipline']['RepeatDatePeriod']['ShortWeekday'][1] = "一";
$Lang['eDiscipline']['RepeatDatePeriod']['ShortWeekday'][2] = "二";
$Lang['eDiscipline']['RepeatDatePeriod']['ShortWeekday'][3] = "三";
$Lang['eDiscipline']['RepeatDatePeriod']['ShortWeekday'][4] = "四";
$Lang['eDiscipline']['RepeatDatePeriod']['ShortWeekday'][5] = "五";
$Lang['eDiscipline']['RepeatDatePeriod']['ShortWeekday'][6] = "六";

$Lang['eDiscipline']['Detention']['SendNotification'] = "發送推送訊息通知家長 (eClass App)";
$Lang['eDiscipline']['Detention']['AttendanceStatus'] = "出席狀況";
$Lang['eDiscipline']['Detention']['AttendanceRemark'] = "出席備註";

if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	$eDiscipline['Detention_Session_Default_String'] = $Lang['eDiscipline']['DetentionTypes']['Title']." | ".$eDiscipline['Detention_Session_Default_String'];
}

if($sys_custom['eDiscipline']['SendDetentionPushMessage']){
	$Lang['eDiscipline']['DetentionMgmt']['SendNotification'] = "發放通知";
	$Lang['eDiscipline']['DetentionMgmt']['SendNoticeWhenRelease'] = " (當記錄開放予學生)";
	$Lang['eDiscipline']['DetentionMgmt']['SendPushMessage'] = "發送推送訊息";
}
# [Creative Secondary School] End

# [Po Leung Kuk C W Chu College] Start
if($sys_custom['eDiscipline']['CSCProbation']){
	$Lang['eDiscipline']['CWC']['ClassAPStatTable'] = "學生獎懲統計表";
	$Lang['eDiscipline']['CWC']['StudentAPRecord'] = "學生獎罰紀錄表";
	$Lang['eDiscipline']['CWC']['Period'] = "時段";
	$Lang['eDiscipline']['CWC']['SchoolYear'] = "學年";
	$Lang['eDiscipline']['CWC']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
	$Lang['eDiscipline']['CWCProbation']['Title'] = "緩刑中";
	$Lang['eDiscipline']['CWCProbation']['Probation'] = "緩刑";
	$Lang['eDiscipline']['CWCProbation']['ConfirmProbation'] = "確立緩刑";
	$Lang['eDiscipline']['CWCProbation']['CancelProbation'] = "取消緩刑";
	$Lang['eDiscipline']['CWCProbation']['SetAsProbation'] = "設為緩刑紀錄";
	$Lang['eDiscipline']['CWCProbation']['ChangeInReceive'] = "獎懲內容變更";
	$Lang['eDiscipline']['CWCProbation']['StudentPhoto'] = "學生相片";
	$Lang['eDiscipline']['CWCProbation']['SelectStudentPhoto'] = "選擇 (學生相片)";
	$Lang['eDiscipline']['CWCProbation']['PleaseSelectPhoto'] = "請選擇學生相片";
	$Lang['eDiscipline']['CWCProbation']['SetRecordToProbationAlert'] = "如將此紀錄設為緩刑，相關的留堂紀錄將會被移除，及相關通告將被暫停。\\n你確定要繼續？";
	$Lang['eDiscipline']['CWCProbation']['RecordIsProbation'] = "<font color=red>紀錄已被設為緩刑</font>";
	$Lang['eDiscipline']['CWCProbation']['RecordIsAward'] = "<font color=red>紀錄為獎勵紀錄</font>";
	$Lang['eDiscipline']['CWCProbation']['RecordIsCaseRecord'] = "<font color=red>紀錄轉換自累積違規紀錄</font>";
	$Lang['eDiscipline']['CWCProbation']['RecordIsIncorrectType'] = "<font color=red>紀錄懲罰內容不屬於".$i_Merit_MinorDemerit."或".$i_Merit_MajorDemerit."</font>";
	$Lang['eDiscipline']['CWCRehabil']['Title'] = "自新計劃";
	$Lang['eDiscipline']['CWCRehabil']['Apply'] = "申請自新計劃";
	$Lang['eDiscipline']['CWCRehabil']['ApplicationContent'] = "自新計劃申請內容";
	$Lang['eDiscipline']['CWCRehabil']['FollowUpTeacher'] = "選擇跟進老師";
	$Lang['eDiscipline']['CWCRehabil']['RehabilReason'] = "自省內容";
	$Lang['eDiscipline']['CWCRehabil']['Status']['Title'] = "申請狀態";
	$Lang['eDiscipline']['CWCRehabil']['Status']['WaitingApproval'] = "等候批核的紀錄";
	$Lang['eDiscipline']['CWCRehabil']['Status']['Rejected'] = "不獲批准的紀錄";
	$Lang['eDiscipline']['CWCRehabil']['Status']['Processing'] = "進行中的紀錄";
	$Lang['eDiscipline']['CWCRehabil']['Status']['Complete'] = "已順利完成的紀錄";
	$Lang['eDiscipline']['CWCRehabil']['Status']['Incomplete'] = "未能完成的紀錄";
	$Lang['eDiscipline']['CWCRehabil']['Approval'] = "自新計劃申請批核";
	$Lang['eDiscipline']['CWCRehabil']['ResponseToRequest'] = "更改申請狀態及文字回應申請";
	$Lang['eDiscipline']['CWCRehabil']['RehabilResponse'] = "文字回應";
	$Lang['eDiscipline']['CWCRehabil']['PleaseSelectPIC'] = "請選擇跟進老師";
	$Lang['eDiscipline']['CWCRehabil']['PleaseFillinReason'] = "請填寫自省內容";
	$Lang['eDiscipline']['CWCRehabil']['CannotApply'] = "由於部份懲罰紀錄已被設為緩刑，故不能申請自新計劃";
}
# [Po Leung Kuk C W Chu College] End

# [HKUGA College] Start
if($sys_custom['eDiscipline']['PresetWaiveReason']){
	$Lang['eDiscipline']['PresetWaiveReason']['Title'] = "預設豁免原因";
	$Lang['eDiscipline']['PresetWaiveReason']['Reason'] = "預設原因";
	$Lang['eDiscipline']['JS_warning']['InputReason'] = "請輸入預設原因。";
}
if($sys_custom['eDiscipline']['AutoSendLateNotice']){
	$Lang['eDiscipline']['AutoSendNotice']['DefaultValue'] = "不適用";
	$Lang['eDiscipline']['AutoSendNotice']['MisConductLate'] = "遲到次數達<!--Number-->次";
}
if($sys_custom['eDiscipline']['AutoSendAppearanceNotice']){
	$Lang['eDiscipline']['AutoSendAppearance']['DefaultValue'] = "不適用";
	$Lang['eDiscipline']['AutoSendNotice']['MisConductAppearance'] = "外觀衣著問題次數達<!--Number-->次";
}
# [HKUGA College] End
# Common Title for Cust Report
$Lang['eDiscipline']['SchoolYear'] = "學年";
$Lang['eDiscipline']['Period'] = "時段";
$Lang['eDiscipline']['Target'] = "對象";
$Lang['eDiscipline']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
# [Jockey Club Government Secondary School] Start
if($sys_custom['eDiscipline']['MonthlySummaryReport']){
	$Lang['eDiscipline']['MonthlySummaryReport']['Title'] = "違規紀錄總覽";
}
# [Jockey Club Government Secondary School] End
# [NT Heung Yee Kuk Tai Po District Secondary School] Start
if($sys_custom['eDiscipline']['HYKClassSummaryReport']){
	$Lang['eDiscipline']['HYKClassSummaryReport']['Title'] = "每月班別統計";
	$Lang['eDiscipline']['HYKClassSummaryReport']['SemesterStart'] = "學期開始日期";
	$Lang['eDiscipline']['HYKClassSummaryReport']['RecordInputDate'] = "紀錄輸入日期";
	$Lang['eDiscipline']['HYKClassSummaryReport']['ShowRecordStudentOnly'] = "顯示被記違規學生列表 (缺點/小過/大過)";
	$Lang['eDiscipline']['HYKClassSummaryReport']['ExportToWord'] = "匯出為Word 檔";
}
if($sys_custom['eDiscipline']['HYKMeritStatisticsReport']){
	$Lang['eDiscipline']['HYKMeritStatisticsReport']['Title'] = "獎勵及懲罰統計";
	$Lang['eDiscipline']['HYKMeritStatisticsReport']['Period'] = "時段";
	$Lang['eDiscipline']['HYKMeritStatisticsReport']['SchoolYear'] = "學年";
	$Lang['eDiscipline']['HYKMeritStatisticsReport']['Semester'] = "學期";
	$Lang['eDiscipline']['HYKMeritStatisticsReport']['Target'] = "對象";
	$Lang['eDiscipline']['HYKMeritStatisticsReport']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
}
if($sys_custom['eDiscipline']['HYKPersonalReport']){
	$Lang['eDiscipline']['HYKPersonalReport']['Title'] = "違規學生個人報告";
	$Lang['eDiscipline']['HYKPersonalReport']['HideNoRecordStudent'] = "不顯示沒有紀錄的學生";
	$Lang['eDiscipline']['HYKPersonalReport']['IncludeDisciplineWarningNotice'] = "顯示每月學生違規紀錄通知書";
}
if($sys_custom['eDiscipline']['HYKPrintDisciplineNotice']) {
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Title'] = "列印學生違規紀錄通知書";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['StartDate'] = "輸入開始日期";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['EndDate'] = "輸入完結日期";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['IssueDate'] = "發出日期";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['GenerateDate'] = "製作時間";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['GeneratedBy'] = "製作者";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['GeneratedCount'] = "發出通知書數量";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['LastTenRecords'] = "列印每月學生違規紀錄通知書";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['GenerateInstruction'] = "如需查看或列印通知書，請前往<br/>
1. 「管理 > 列印通告」<br/>
2. 「自定報告 > 違規學生個人報告」";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['GenerateMonthlyLetter'] = "產生每月通知書";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['LetterType'] = "通知書類別";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Single'] = "單次";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Monthly'] = "每月";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Records'] = "記錄";

    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['Success'] = "1|=|已成功製作通知書。";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['Deleted'] = "1|=|已成功刪除製作記錄。";
    //$Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['ReplaceOverlapLog'] = "選擇時段內已製作過<!--COUNT-->次通知書記錄，如果繼續將會刪除這些記錄。\\n你確定要繼續？";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['ReplaceOverlapLog'] = "選擇時段已製作過通知書記錄，如果繼續將會刪除原有記錄。\\n你確定要繼續？";
    $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['OutOfTermPeriod'] = "選擇時段超出學期範圍";
}
# [NT Heung Yee Kuk Tai Po District Secondary School] End
# [Buddhist Wong Wan Tin College & Tung Wah Group Of Hospitals Wong Fut Nam College] Start
if($sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'])
{
	$Lang['eDiscipline']['BWWTC_ConductRemarks'] = "欠交功課";
	$Lang['eDiscipline']['BWWTC_DetentionType']['L'] = "3次遲到";
	$Lang['eDiscipline']['BWWTC_DetentionType']['H'] = "3次起欠交";
	$Lang['eDiscipline']['BWWTC_DetentionType']['S'] = "欠功課補交";
	$Lang['eDiscipline']['BWWTC_DetentionType']['H_Name'] = "欠交功課";
	$Lang['eDiscipline']['BWWTC_DetentionType']['S_Name'] = "欠功課補交";
	$Lang['eDiscipline']['BWWTC_DetentionType']['S_Title'] = "欠補交功課";
	$Lang['eDiscipline']['BWWTC_DetentionType']['H_Title'] = "欠交功課";
}
if($sys_custom['eDiscipline']['BWWTC_DetentionPrinAttendance'])
{
	$Lang['eDiscipline']['RemainDetentionRecord'] = "剩下留堂安排";
}
if($sys_custom['eDiscipline']['BWWTC_AutoFullIn_HWRemarks'])
{
	$Lang['eDiscipline']['AllRelatedHWRemark'] = "詳細功課資料";
}
if($sys_custom['eDiscipline']['DisplayHomeworkRelatedMsg'])
{
	$Lang['eDiscipline']['CancelRequestMessage'] = "取消要求";
	$Lang['eDiscipline']['CancelAPNotice'] = "(聯繫缺點及家長信)";
}
if($sys_custom['eDiscipline']['WFN_Reports'])
{
	$Lang['eDiscipline']['TWGHWFNS_Accumulated_Late_Report']['Title'] = "累積遲到紀錄表";
	$Lang['eDiscipline']['TWGHWFNS_Cancel_Misconduct_Report']['Title'] = "取消懲罰系統紀錄表";
	$Lang['eDiscipline']['TWGHWFNS_Cancel_Misconduct_Report']['Type'] = "紀錄表種類";
	$Lang['eDiscipline']['TWGHWFNS_Cancel_Misconduct_Report']['Homework'] = "取消電子家課系統紀錄表";
	$Lang['eDiscipline']['TWGHWFNS_Cancel_Misconduct_Report']['Late'] = "取消遲到系統紀錄表";
	$Lang['eDiscipline']['TWGHWFNS_Upgraded_Misconduct_Report']['Title'] = "累積增加懲罰系統紀錄表";
	$Lang['eDiscipline']['TWGHWFNS_Upgraded_Misconduct_Report']['Type'] = "紀錄表種類";
	$Lang['eDiscipline']['TWGHWFNS_Upgraded_Misconduct_Report']['Homework'] = "電子家課";
	$Lang['eDiscipline']['TWGHWFNS_Upgraded_Misconduct_Report']['Late'] = "遲到";
	$Lang['eDiscipline']['TWGHWFNS_Unassigned_Late_Report']['Title'] = "留堂餘數紀錄表";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['Title'] = "留堂學生紀錄";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['SchoolName'] = "	TWGHs Wong Fut Nam College";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['FormTitle'] = "下列學生因欠交功課，需要於 <!--detentionDate--> 放學後立刻到 <!--detentionLocation-->";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['ClassTitle'] = "下列 <!--studentClass--> 班學生因欠交功課，需要於 <!--detentionDate--> 放學後立刻到 <!--detentionLocation-->";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['ClassTeacher'] = "班主任";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['ClassName'] = "班別";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['ClassNumber'] = "學號";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['StudentName'] = "學生姓名";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['PrintTakeDetentionAttendanceSheet'] = "列印留堂點名紙";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Title'] = "欠交功課、遲到及違規紀錄";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['ReportTitle'] = "欠交功課、遲到及違規紀錄";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['SchoolName'] = "TWGHs Wong Fut Nam College";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['SchoolYear'] = "年度";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['WholeYear'] = "全年";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['EndDate'] = "截止";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['ClassTeacher'] = "班主任";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['ClassName'] = "班別";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['ClassNumber'] = "學號";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['StudentName'] = "學生姓名";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['HWLateSubmission'] = "欠交功課";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Late'] = "遲到";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Punishment'] = "違規處分";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Time'] = "次";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['Times'] = "次數";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][0] = "警告";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][-1] = "缺點";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][-2] = "小過";
	$Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][-3] = "大過";
}
# [Buddhist Wong Wan Tin College & Tung Wah Group Of Hospitals Wong Fut Nam College] End
# [HKSYC & IA Chan Nam Chong Memorial College] Start
if($sys_custom['eDiscipline']['CustLateDemeritReport'])
{
	$Lang['eDiscipline']['CustLateDemeritReport']['Title'] = "遲到記錄紙";
	$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
}
# [HKSYC & IA Chan Nam Chong Memorial College] End
# [St. Joan of Arc Secondary School] Start
$Lang['eDiscipline']['ExceedStudyScoreMaxGain'] = "勤學分超出加分上限";
$Lang['eDiscipline']['ExceedStudyScoreMaxDeduct'] = "勤學分超出扣分上限";
$Lang['eDiscipline']['MaxDeductStudyScore'] = "扣分上限";
$Lang['eDiscipline']['StudentExceedMeritItemMaxStudyScore'] = "獎懲項目可加減勤學分上限為 <!--MAX_SCORE--> 分";
$Lang['eDiscipline']['MeritItemMaxStudyScore'] = "勤學分加減上限";
$Lang['eDiscipline']['MeritItemMaxStudyScoreRemarks'] = "<span class='tabletextremark'>(輸入 0 代表不適用)</span>";
# [St. Joan of Arc Secondary School] End
# [HKUGA College] Start
$Lang['eDiscipline']['GM_HOYRemarks'] = "備註<br/>(如有敏感資料, 請剔選 \"詳細內容可向 HOY 查詢\" 選項)";
$Lang['eDiscipline']['DetailsReferToHOY'] = "詳細內容可向 HOY 查詢";
$Lang['eDiscipline']['DataHandling_ToPortfolio'] = "資料傳送(至 iPortfolio)";
$Lang['eDiscipline']['DataHandling_ToPortfolioStepArr'][0] = "設定";
$Lang['eDiscipline']['DataHandling_ToPortfolioStepArr'][1] = "結果";
$Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['Success'] = "成功傳送";
$Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['Student'] = "<!--count-->個學生記錄";
$Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['Pastoral'] = "<!--count-->個學生行為紀錄";
$Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['TransferOtherRecords'] = "傳送其他紀錄";
# [HKUGA College] End
# [Tin Ka Ping Secondary School] Start
$Lang['eDiscipline']['TKP']['JoinRainbowScheme'] = "加入彩虹計劃";
$Lang['eDiscipline']['TKP']['JoinRainbowSchemeCH'] = "參加彩虹計劃";
$Lang['eDiscipline']['TKP']['JoinRainbowBy'] = "由";
$Lang['eDiscipline']['TKP']['JoinRainbowBy2'] = "於";
$Lang['eDiscipline']['TKP']['JoinRainbowBy3'] = "加入彩虹計劃";
$Lang['eDiscipline']['TKP']['LeaveRainbowScheme'] = "離開彩虹計劃";
$Lang['eDiscipline']['TKPRainbowScheme']['RecordIsNotPunishment'] = "<font color=red>紀錄並非懲罰紀錄</font>";
$Lang['eDiscipline']['TKPRainbowScheme']['RecordIsIncorrectType'] = "<font color=red>紀錄懲罰內容不屬於".$i_Merit_MajorDemerit."或".$i_Merit_SuperDemerit."</font>";
$Lang['eDiscipline']['TKPRainbowScheme']['RecordIsInRainbowScheme'] = "<font color=red>紀錄已加入彩虹計劃</font>";
$Lang['eDiscipline']['TKPRainbowScheme']['SetRecordToRBSchemeAlert'] = "如加入彩虹計劃，此懲罰紀錄將會降級。\\n你確定要繼續？";
$Lang['eDiscipline']['TKPRainbowScheme']['SetRecordToLeaveRBSchemeAlert'] = "如離開彩虹計劃，此懲罰紀錄將會還原。\\n你確定要繼續？";
# [Tin Ka Ping Secondary School] End
# [Pope Paul VI College] Start
if($sys_custom['eDiscipline']['EntryRecordLog']) {
    $Lang['eDiscipline']['InputLog']['Title'] = "資料新增記錄";
    $Lang['eDiscipline']['InputLog']['InputDate'] = "新增日期";
    $Lang['eDiscipline']['InputLog']['InputBy'] = "新增者";
}
# [Pope Paul VI College] End
# [Tsuen Wan Public Ho Chuen Yiu Memorial College] Start
if($sys_custom['eDiscipline']['HCY_ClassRecordCountReport']) {
    $Lang['eDiscipline']['HCY_ClassRecordCountReport']['Title'] = "各班操行數字總表";
    $Lang['eDiscipline']['HCY_ClassRecordCountReport']['SchoolYear'] = "學年";
    $Lang['eDiscipline']['HCY_ClassRecordCountReport']['Period'] = "時段";
    $Lang['eDiscipline']['HCY_ClassRecordCountReport']['Target'] = "對象";
    $Lang['eDiscipline']['HCY_ClassRecordCountReport']['PressCtrlKey'] = "可按Ctrl鍵選擇多項";
}
# [Tsuen Wan Public Ho Chuen Yiu Memorial College] End
$Lang['eDiscipline']['FollowUpRemark'] = "訓輔跟進狀態";
$Lang['eDiscipline']['FollowUpRemarkShortForm'] = "訓輔跟進狀態";

$Lang['eDiscipline']['ReleaseWarningNoticeNotificationTitle'] = "發出學生<!--TYPE-->通知";
$Lang['eDiscipline']['WarningNotice'] = "違規警告紙";
$Lang['eDiscipline']['DemeritNotice'] = "記缺點通知信";
$Lang['eDiscipline']['NoticeReleaseDate'] = "電子通告發出日期";

$Lang['eClass']['DeleteUserWarningMsg'] = '如移除包含你在內的所有用戶，你將無法再管理此課室。屆時，課室將只能夠由建立者管理。';
$Lang['eClass']['ClassroomToBeDelete'] = "將被刪除的網上教室：";
$Lang['eClass']['DeleteClassroomConfirmMsg'] = "你是否確定要刪除這些網上教室?";
$Lang['eClass']['Classroom'] = "網上教室";

$Lang['iFolder']['FieldTitle']['Unzip'] = '解壓';
$Lang['ipFiles']['ViewStudentFileDesc'] = "以下為學生 [我的檔案] 內容 ，該等內容並非用於呈交或評分。";
## System Admin
$Lang['SystemAdmin'] = "臨時管理用戶";
$Lang['SetSystemAdmin'] = "設定臨時管理用戶";
$Lang['SystemAdminLoginExists'] = "<font color=red>內聯網帳號已存在。</font>";
$Lang['WhatIsAdminUser']['Title'] = "甚麼是管理用戶/臨時管理用戶?";
$Lang['WhatIsAdminUser']['Details'] = "「管理用戶」為內聯網前端的管理者。首次使用系統時，由於還沒有建立任何帳戶，所以你不能 夠指派「管理用戶」。<br><br>作為系統管理員，你可以「臨時管理用戶」身份，登入內聯網前端，以建立首批「管理用戶」的帳戶。其後，「管理用 戶」將可以接管系統前端的設定工作。<br><br>請輸入自定的登入帳號及密碼，然後按「儲存」。<br><br>然後，你便可以登入內聯網前端，前往 <b>學校行政管理工具 > 用戶管理 > 教職員用戶</b>，建立「管理用戶」的帳戶。完成後，請返回本頁，並使用上表，指派「管理用戶」。";

### Status
$Lang['Status']['Active'] = "使用中";
$Lang['Status']['Activate'] = "啟用";
$Lang['Status']['Suspend'] = "暫停";
$Lang['Status']['Suspended'] = "暫停";
$Lang['Status']['Left'] = "已離校";
$Lang['Status']['Archived'] = "已整存";

# Admin Console Message
$Lang['SysMgr']['Eclass']['UpgradeSuccessfully'] = "eClass 已成功升級";
$Lang['SysMgr']['Eclass']['UpgradeFailed'] = "eClass 升級失敗";
$Lang['SysMgr']['Eclass']['UpgradeMessage_V41'] = "eClass  網上教室的最新版本eClass 4.1，已經於最近一次eClass Update期間下載到貴校的伺服器。由於此為重大更新，eClass 4.1 並不會自動執行。我們建議你諮詢老師意見並獲得同意後，再按<input type=button name=upgrade id=upgrade value='啟用eClass 4.1' onClick=\"self.location.href='version_update.php?module=eclass&ver=4.10'\">。
<br><br>
有關eClass 4.1的各項改善，請參閱 <a href='http://www.broadlearning.com/enewsletter/201101/dk4e/' target='_blank'><u>eClass電子報</u></a>。
<br><br>
<font color=red>警告：啟用後，eClass 4.0 將被 eClass 4.1 取代，並且不能還原。</font>";

###### Email Notification
$Lang['EmailNotification']['eNotice']['Subject'] = "[eClass內聯網] 新電子通告 - ";
$Lang['EmailNotification']['eNotice']['Content'] = "一則新的電子通告 (__TITLE__) __TERM__於__STARTDATE__發出。<br>\r\n 請於__ENDDATE__或之前簽妥。<br>\r\n 你可登入__WEBSITE__，然後前往「資訊服務 > 電子通告」閱讀通告。<br>\r\n";
$Lang['EmailNotification']['eNotice']['Term1'] = "將會";
$Lang['EmailNotification']['eNotice']['Term2'] = "已經";
$Lang['EmailNotification']['SchoolNews']['School'] = "學校";
$Lang['EmailNotification']['SchoolNews']['Group'] = "小組";
//$Lang['EmailNotification']['SchoolNews']['Subject'] = "新__TYPE__宣佈 (__DATE__)";
$Lang['EmailNotification']['SchoolNews']['Subject'] = "新__TYPE__宣佈 - __TITLE__ (__DATE__)";
$Lang['EmailNotification']['SchoolNews']['Content']['SCHOOL'] = "一個新學校宣佈(\"__TITLE__\")已經於__DATE__發出。請登入__WEBSITE__閱讀。<br>\r\n";
$Lang['EmailNotification']['SchoolNews']['Content']['GROUP'] = "一個關於__LIST__的新小組宣佈(\"__TITLE__\")已經於__DATE__發出。請登入__WEBSITE__閱讀。<br>\r\n";
$Lang['EmailNotification']['eSurvey']['Subject'] = "新__TYPE__問卷調查 (__DATE__)";
$Lang['EmailNotification']['eSurvey']['Content']['SCHOOL'] = "一個新學校問卷調查(\"__TITLE__\")已經於__DATE__發出。請登入__WEBSITE__填寫。<br>\r\n";
$Lang['EmailNotification']['eSurvey']['Content']['GROUP'] = "一個關於__LIST__的新問卷調查(\"__TITLE__\")已經於__DATE__發出。請登入__WEBSITE__填寫。<br>\r\n";
$Lang['EmailNotification']['eCircular']['Subject'] = "[eClass內聯網] 新職員通告 - __TITLE__";
$Lang['EmailNotification']['eCircular']['Content'] =  "一則新的職員通告 (__TITLE__) __TERM__於__STARTDATE__發出。<br>\r\n 請於__ENDDATE__或之前簽妥。<br>\r\n 你可登入__WEBSITE__，然後前往「資訊服務 > 職員通告」閱讀通告。<br>\r\n";
$Lang['EmailNotification']['ForgotPassword']['Subject'] = "eClass Intranet: 申請密碼";
$Lang['EmailNotification']['PasswordReset']['Subject'] = "eClass Intranet: 因保安理由, 你的密碼已經重設";
$Lang['EmailNotification']['ForgotPassword']['Content'] = "__NAME__,<br>\r\n<br>\r\n你的登入戶口密碼已經重設，如下:<br>\r\n<br>\r\n登入名稱: __LOGIN__<br>\r\n密碼: __PASSWORD__<br>\r\n<br>\r\n祝 使用愉快!<br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;此致<br>\r\neClass<br>\r\n";
$Lang['EmailNotification']['ForgotPassword_ACK']['Subject'] = "eClass內聯網: 忘記密碼";
$Lang['EmailNotification']['ForgotPassword_ACK']['Content'] = "一名用戶忘記了密碼，並已經重新提取。<br>\r\n登入名稱: __LOGIN__<br>\r\n電郵: __EMAIL__<br>\r\n";
$Lang['EmailNotification']['Registration']['Subject'] = "eClass內聯網: 用戶登記";
$Lang['EmailNotification']['Registration']['Content'] = "__ENGNAME__ __CHINAME__,<br><br>\r\n\r\n歡迎使用eClass內聯網。你可以前往此網頁登入:__WEBSITE__<br><br>\r\n\r\n你的登入戶口如下:<br><br>\r\n\r\n電郵: __EMAIL__<br>\r\n登入名稱: __LOGIN__<br>\r\n密碼: __PASSWORD__<br><br>\r\n";
$Lang['EmailNotification']['UpdatePassword']['Subject'] = "eClass內聯網: 你的登入密碼已經重設";
$Lang['EmailNotification']['UpdatePassword']['Content'] = "__ENGNAME__ __CHINAME__,<br>\r\n<br>\r\n你的eClass內聯網登入密碼已經重設。如果你仍未能成功登入，請聯絡系統管理員。<br>\r\n<br>\r\n";
$Lang['EmailNotification']['SendPassword']['Subject'] = "eClass內聯網登入戶口";
$Lang['EmailNotification']['SendPassword']['Content'] = "__ENGNAME__ __CHINAME__,<br>\r\n<br>\r\n歡迎使用eClass內聯網。你可以前往此網頁登入:__WEBSITE__<br><br>\r\n\r\n你的登入戶口如下:<br>\r\n<br>\r\n電郵: __EMAIL__<br>\r\n登入名稱: __LOGIN__<br>\r\n密碼: __PASSWORD__<br>\r\n<br>\r\n";
$Lang['EmailNotification']['eEnrolment']['Club']['EnrolmentResult']['Subject'] = "學會報名取錄結果";
$Lang['EmailNotification']['eEnrolment']['Activity']['EnrolmentResult']['Subject'] = "活動報名取錄結果";
$Lang['EmailNotification']['eEnrolment']['EnrolmentResult']['Content'] = "學生 __STUDENT_NAME__ 的報名結果如下:";

$Lang['EmailNotification']['ForgotHashedPassword']['Subject'] = "eClass Intranet: 申請重設密碼";
$Lang['EmailNotification']['ForgotHashedPassword']['Content'] = "__NAME__,<br>\r\n<br>\r\n歡迎使用eClass內聯網。<br><br>\r\n\r\n你的登入戶口如下:<br>\r\n<br>\r\n登入名稱: __LOGIN__<br>\r\n電郵: __EMAIL__<br>\r\n你可以前往此連結重設密碼:<a href='__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__'>__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__</a><br>\r\n<br>\r\n";
$Lang['EmailNotification']['ForgotHashedPassword_ACK']['Subject'] = "eClass內聯網: 重設密碼";
$Lang['EmailNotification']['ForgotHashedPassword_ACK']['Content'] = "一名用戶忘記了密碼，並已經重新設定。<br>\r\n登入名稱: __LOGIN__<br>\r\n電郵: __EMAIL__<br>\r\n";

$Lang['EmailNotification']['Footer'] = "<br><br>\r\n\r\n------------------------------------------------------<br>\r\n此電郵由eClass內聯網自動發送。<br>\r\n如有任何查詢，請電郵到 __WEBMASTER__。<br>\r\n要獲得最新資訊，請前往 __WEBSITE__。<br>\r\n";
$Lang['EmailNotification']['DoNotReply'] = "此郵件由系統發出，請勿回覆。";

$Lang['AdminConsole']['Activity']['ActivityDate'] = "日期";
$Lang['AdminConsole']['Activity']['ActivityDateCanBeIgnored'] = "(可留空此格)";

############### From IP20 [Start]
$i_Survey_UnfillList = "未作答用戶";
$i_WebSAMS_AttendanceCode_No_RegNo_Students="未有WebSAMS 學生註冊編號的學生人數";

$i_Service_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期；輸入\"0\"代表全年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : 學生本學年班別 (注意大小寫)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第五欄 : 學生本學年班號<br>
第六欄 : $i_ServiceName<br>
第七欄 : $i_ServiceRole (可選擇留空).<br>
第八欄 : $i_ServicePerformance (可選擇留空).<br>
第九欄 : $i_ServiceRemark (可選擇留空).<br>
第十欄 : $i_ServiceOrganization (可選擇留空).
";

$i_Service_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期；輸入\"0\"代表全年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : WebSAMS 學生註冊編號 (帳號由 # 開始 如  #123456).<br>
第五欄 : $i_ServiceName<br>
第六欄 : $i_ServiceRole (可選擇留空).<br>
第七欄 : $i_ServicePerformance (可選擇留空).<br>
第八欄 : $i_ServiceRemark (可選擇留空).<br>
第九欄 : $i_ServiceOrganization (可選擇留空).
";

$i_Activity_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期；輸入\"0\"代表全年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : 學生本學年班別 (注意大小寫)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第五欄 : 學生本學年班號<br>
第六欄 : $i_ActivityName.<br>
第七欄 : $i_ActivityRole (可選擇留空).<br>
第八欄 : $i_ActivityPerformance (可選擇留空).<br>
第九欄 : $i_ActivityRemark (可選擇留空).<br>
第十欄 : $i_ActivityOrganization (可選擇留空).
";

$i_Activity_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期；輸入\"0\"代表全年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : WebSAMS 學生註冊編號 (帳號由 # 開始 如  #123456).<br>
第五欄 : $i_ActivityName.<br>
第六欄 : $i_ActivityRole (可選擇留空).<br>
第七欄 : $i_ActivityPerformance (可選擇留空).<br>
第八欄 : $i_ActivityRemark (可選擇留空).<br>
第九欄 : $i_ActivityOrganization (可選擇留空).
";

$i_Award_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期；輸入\"0\"代表全年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : 學生本學年班別 (注意大小寫)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第五欄 : 學生本學年班號<br>
第六欄 : $i_AwardName<br>
第七欄 : $i_AwardRemark (可選擇留空).<br>
第八欄 : $i_AwardOrganization (可選擇留空).<br>
第九欄 : $i_AwardSubjectArea (可選擇留空).
";

$i_Award_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期；輸入\"0\"代表全年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (可選擇留空).<br>
第四欄 : WebSAMS 學生註冊編號 (帳號由 # 開始 如  #123456).<br>
第五欄 : $i_AwardName<br>
第六欄 : $i_AwardRemark (可選擇留空).<br>
第七欄 : $i_AwardOrganization (可選擇留空).<br>
第八欄 : $i_AwardSubjectArea (可選擇留空).
";

$i_Attendance_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四欄 : 學生本學年班別 (注意大小寫)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第五欄 : 學生本學年班號<br>
第六欄 : 類別 (A - 缺席, L - 遲到, E - 早退)<br>
第七欄 : 時段 (WD - 全日, AM - 上午, PM - 下午).<br>
第八欄 : 原因 (可選擇留空).
";

$i_Attendance_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四欄 : WebSAMS 學生註冊編號 (帳號由 # 開始 如  #123456).<br>
第五欄 : 類別 (A - 缺席, L - 遲到, E - 早退)<br>
第六欄 : 時段 (WD - 全日, AM - 上午, PM - 下午).<br>
第七欄 : 原因 (可選擇留空).
";

$i_Merit_ImportInstruction1 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四欄 : 學生本學年班別 (注意大小寫)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第五欄 : 學生本學年班號<br>
第六欄 : 類別 (C1 - $i_Merit_Merit, C2 - $i_Merit_MinorCredit, C3 - $i_Merit_MajorCredit,
C4 - $i_Merit_SuperCredit, C5 - $i_Merit_UltraCredit,
D1 - $i_Merit_BlackMark, D2 - $i_Merit_MinorDemerit, D3 - $i_Merit_MajorDemerit, D4 - $i_Merit_SuperDemerit,
D5 - $i_Merit_UltraDemerit)<br>
第七欄 : $i_Merit_Qty.<br>
第八欄 : $i_Merit_Reason (可選擇留空).<br>
第九欄 : $i_Merit_Remark (可選擇留空).<br>
第十欄 : 負責人的登入帳號 (可選擇留空).
";

$i_Merit_ImportInstruction2 = "<b><u>檔案說明:</u></b><br>
第一欄 : 紀錄年度 (如留空即為本學年)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第二欄 : 紀錄學期 (如留空即為本學期)<span style=\"color:red;\">*請用英文填寫</span>.<br>
第三欄 : 紀錄日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四欄 : WebSAMS 學生註冊編號 (帳號由 # 開始 如  #123456).<br>
第五欄 : 類別 (C1 - $i_Merit_Merit, C2 - $i_Merit_MinorCredit, C3 - $i_Merit_MajorCredit,
C4 - $i_Merit_SuperCredit, C5 - $i_Merit_UltraCredit,
D1 - $i_Merit_BlackMark, D2 - $i_Merit_MinorDemerit, D3 - $i_Merit_MajorDemerit, D4 - $i_Merit_SuperDemerit,
D5 - $i_Merit_UltraDemerit)<br>
第六欄 : $i_Merit_Qty.<br>
第七欄 : $i_Merit_Reason (可選擇留空).<br>
第八欄 : $i_Merit_Remark (可選擇留空).<br>
第九欄 : 負責人的登入帳號 (可選擇留空).
";

$i_InventorySystem_GroupImportError[1] = $i_InventorySystem_Item_GroupCode."已存在";
$i_InventorySystem_GroupImportError[2] = "沒有填上".$i_InventorySystem_Item_GroupCode;
$i_InventorySystem_GroupImportError[3] = "沒有填上".$i_InventorySystem_Setting_ManagementGroup_ChineseName;
$i_InventorySystem_GroupImportError[4] = "沒有填上".$i_InventorySystem_Setting_ManagementGroup_EnglishName;
$i_InventorySystem_GroupImportError[5] = "沒有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_GroupImportError[6] = $i_InventorySystem_Item_GroupCode." 不能超過十位數字";
$i_InventorySystem_GroupImportError[19] = $i_InventorySystem_Item_GroupCode."已存在";

$i_InventorySystem_ImportError[1] = $i_InventorySystem_Item_Code."已存在";
$i_InventorySystem_ImportError[2] = "沒有填上".$i_InventorySystem_Item_Code;
$i_InventorySystem_ImportError[3] = $i_InventorySystem_Item_Barcode."已存在";
$i_InventorySystem_ImportError[4] = "沒有填上".$i_InventorySystem_Item_Barcode;
$i_InventorySystem_ImportError[5] = "沒有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_ImportError[6] = "沒有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_ImportError[7] = $i_InventorySystem_Item_Category2Code."無效";
$i_InventorySystem_ImportError[8] = "沒有填上".$i_InventorySystem_Item_Category2Code;
$i_InventorySystem_ImportError[9] = $i_InventorySystem_Item_GroupCode."無效";
$i_InventorySystem_ImportError[10] = "沒有填上".$i_InventorySystem_Item_GroupCode;
$i_InventorySystem_ImportError[11] = "大樓編號 / ". $i_InventorySystem_Item_LocationCode." / ".$i_InventorySystem_Item_Location2Code."無效";
$i_InventorySystem_ImportError[12] = "沒有填上".$i_InventorySystem_Item_LocationCode;
$i_InventorySystem_ImportError[13] = $i_InventorySystem_Item_FundingSourceCode."無效";
$i_InventorySystem_ImportError[14] = "沒有填上".$i_InventorySystem_Item_FundingSourceCode;
$i_InventorySystem_ImportError[15] = "沒有填上".$i_InventorySystem_Item_Ownership;
$i_InventorySystem_ImportError[16] = "沒有填上".$i_InventorySystem_Item_Qty;
$i_InventorySystem_ImportError[17] = $i_InventorySystem_Item_Ownership."無效";
$i_InventorySystem_ImportError[18] = $i_InventorySystem_Item_Qty."無效";
$i_InventorySystem_ImportError[19] = $i_InventorySystem_Item_Code."已被使用";
$i_InventorySystem_ImportError[20] = $i_InventorySystem_Item_Barcode."已被使用";
$i_InventorySystem_ImportError[21] = "沒有填上".$i_InventorySystem_Item_Price;
$i_InventorySystem_ImportError[22] = $i_InventorySystem_Item_Price."無效";
$i_InventorySystem_ImportError[23] = "沒有填上".$i_InventorySystem_Unit_Price;
$i_InventorySystem_ImportError[24] = $i_InventorySystem_Unit_Price."無效";
$i_InventorySystem_ImportError[25] = $i_InventorySystem_Item_BulkItemAdmin."無效";
$i_InventorySystem_ImportError[26] = $i_InventorySystem_Item_CategoryCode."無效";
$i_InventorySystem_ImportError[27] = "沒有填上".$i_InventorySystem_Item_CategoryCode;
$i_InventorySystem_ImportError[28] = $i_InventorySystem_Item_Barcode."無效";
$i_InventorySystem_ImportError[29] = "沒有填上".$i_InventorySystem_Item_Location2Code;
$i_InventorySystem_ImportError[30] = $i_InventorySystem_Item_CategoryCode." / ".$i_InventorySystem_Item_Category2Code."無效";
$i_InventorySystem_ImportError[31] = "沒有填上大樓編號";
$i_InventorySystem_ImportError[32] = "用戶不屬於管理小組的組長";
$i_InventorySystem_ImportError[33] = "沒有此物品在指定位置內";
$i_InventorySystem_ImportError[34] = "已有此物品在指定位置內";
$i_InventorySystem_ImportError[35] = "大樓不能被更改";
$i_InventorySystem_ImportError[36] = "位置不能被更改";
$i_InventorySystem_ImportError[37] = "子位置不能被更改";
$i_InventorySystem_ImportError[38] = $i_InventorySystem_Item_CategoryCode."不能被更改";
$i_InventorySystem_ImportError[39] = $i_InventorySystem_Item_Barcode."不能被更改";
$i_InventorySystem_ImportError[40] = $i_InventorySystem_Item_CategoryCode."與現存物品不符";
$i_InventorySystem_ImportError[41] = $i_InventorySystem_Item_Qty."必須為1";
$i_InventorySystem_ImportError[42] = $i_InventorySystem_Item_Qty."必須為整數";
$i_InventorySystem_ImportError[43] = "物品不存在";
$i_InventorySystem_ImportError[44] = "物品已經存在";
$i_InventorySystem_ImportError[45] = "不需輸入許可證";
$i_InventorySystem_ImportError[46] = "不需輸入保用日期";
$i_InventorySystem_ImportError[47] = "不需輸入序號";
$i_InventorySystem_ImportError[48] = "沒有填上許可證";
$i_InventorySystem_ImportError[49] = "沒有填上保用日期";
$i_InventorySystem_ImportError[50] = "沒有填上序號";
$i_InventorySystem_ImportError[51] = "兩個".$i_InventorySystem_Item_FundingSourceCode."不能相同";
$i_InventorySystem_ImportError[52] = "沒有填上".$i_InventorySystem_Item_Code;
$i_InventorySystem_ImportError[53] = "沒有填上購買日期或格式不符";
$i_InventorySystem_ImportError[54] = "保用日期格式不符";
$i_InventorySystem_ImportError[55] = "物品種類錯誤:這是單一物品";
$i_InventorySystem_ImportError[56] = "物品種類錯誤:這是大量物品";
$i_InventorySystem_ImportError[999] = "此行沒有填上物品資料";

############### From IP20 [End]


######################## From IP20 [New]
$Lang['General']['Procesesing'] = "處理進行中...";
$Lang['General']['PleaseWait']= "請稍候!";
$Lang['General']['Class'] = "班別";
$Lang['General']['ClassNo_ShortForm'] = "班號";
$Lang['General']['Form'] = "級別";

$Lang['eComm']['Reason'] = "原因為:";
$Lang['eComm']['SelectApproval'] = "需要批核, 批核者:";

$Lang['eEnrolment']['StudentEnrolmentStatus'] = "學生報名情況";

$Lang['AdminJob_Description_SetAdmin'] = "請選擇管理人員及選取管理權限.";



#####################################

# eInventory
$Lang['eInventory']['ActionType'] = "行動類別";
$Lang['eInventory']['Insert'] = "新增";
$Lang['eInventory']['Update'] = $button_update;
$Lang['eInventory']['PleaseSelectFile'] = "請選擇檔案";
$Lang['eInventory']['ValidRecord'] = "有效紀錄數量";
$Lang['eInventory']['InvalidRecord'] = "錯誤紀錄數量";
$Lang['eInventory']['RowLine1'] = "第";
$Lang['eInventory']['RowLine2'] = "行";
$Lang['eInventory']['InvalidRecordRow'] = "為錯誤紀錄";
$Lang['eInventory']['UpdateItemAmount'] = "更新物品數量";
$Lang['eInventory']['MandatoryWhenInsert'] = "附有「<span class='tabletextrequire'>^</span>」的項目在新增時必須填寫";
$Lang['eInventory']['EditableFields'] = "附有「<span class='tabletextrequire'>#</span>」的項目可於更新時被更改";
$Lang['eInventory']['BarCodeFormat'] = "<span class='tabletextrequire'>@</span> 請參考 \"設定> 系統屬性 > 條碼格式\" 中的條碼格式。";
$Lang['eInventory']['AutoCalcuateUnitPriceRemark'] = "<span class='tabletextrequire'>*</span>單位價格以購買金額/數量自動計算";
$Lang['eInventory']['RedColorInfoWillNotBeUpdated'] = "<font color='red'>紅色</font>代表資料不會被更新，其他資料則可被更新；倘若留空欄位，原有內容會被更新至空白。";

$Lang["eInventory"]["Max10Words"] = "最多 10 個標籤";
$Lang["eInventory"]["SelectionAllTags"] = "- 全部標籤 -";

$Lang['eInventory']['ExportFieldTitle']['RoomCapacity']['EN']='Sub-Location Capacity';
$Lang['eInventory']['ExportFieldTitle']['RoomCapacity']['B5']='子位置容量';
$Lang['eInventory']['ExportFieldTitle']['RoomCategory']['EN']='Suited Course Category';
$Lang['eInventory']['ExportFieldTitle']['RoomCategory']['B5']='適合的課程類別';
# eInventory eClassApp
$Lang['eInventory']['eClassApp']['AddToStockTake'] = "加入盤點清單";
$Lang['eInventory']['eClassApp']['AddToStockTakeBtn'] = "加入盤點";
$Lang['eInventory']['eClassApp']['AdjustBarcodeToMatchScreen'] = "將條碼對準畫面即可讀取。";
$Lang['eInventory']['eClassApp']['ConfirmAndScan'] = "確定並繼續掃描";
$Lang['eInventory']['eClassApp']['ConfirmStockTake'] = "確定盤點";
$Lang['eInventory']['eClassApp']['DifferentLocationRemark'] = "這物品不屬於這位置，請將其帶回所屬位置";
$Lang['eInventory']['eClassApp']['EditStockTakeItemInfo'] = "編輯盤點資料";
$Lang['eInventory']['eClassApp']['EmptySymbol'] = "-";
$Lang['eInventory']['eClassApp']['Error']['MultipleItems'] = "錯誤:物品詳細資料多於一項";
$Lang['eInventory']['eClassApp']['Error']['NotOriginalLocation'] = "物品位置錯誤";
$Lang['eInventory']['eClassApp']['Expected'] = "預期";
$Lang['eInventory']['eClassApp']['Filter'] = "進行篩選";
$Lang['eInventory']['eClassApp']['FinishStocktake'] = "完成盤點";
$Lang['eInventory']['eClassApp']['HasDoneStocktake'] = "已盤點";
$Lang['eInventory']['eClassApp']['HaveSelected'] = "已選擇";
$Lang['eInventory']['eClassApp']['Info']['BulkItemQty'] = "大量物品預期數量:";
$Lang['eInventory']['eClassApp']['Info']['EditQty'] = "如跟預期數量不同，請按此輸入盤點數量";
$Lang['eInventory']['eClassApp']['InputBarcode'] = "輸入條碼";
$Lang['eInventory']['eClassApp']['Item'] = "項";
$Lang['eInventory']['eClassApp']['ItemInfo'] = "物品資訊";
$Lang['eInventory']['eClassApp']['ItemList'] = "物品清單";
$Lang['eInventory']['eClassApp']['LastStockTake'] = "最後盤點";
$Lang['eInventory']['eClassApp']['NotDoneStockTakeItem'] = "未盤點物品";
$Lang['eInventory']['eClassApp']['NoStockTakeItem'] = "暫無盤點物品";
$Lang['eInventory']['eClassApp']['OnLoan'] = "借出";
$Lang['eInventory']['eClassApp']['OpenCameraToScan'] = "開啟相機掃描";
$Lang['eInventory']['eClassApp']['ScanBarcode'] = "掃瞄條碼";
$Lang['eInventory']['eClassApp']['ShowMore'] = "顯示更多";
$Lang['eInventory']['eClassApp']['StockTake'] = "盤點";
$Lang['eInventory']['eClassApp']['StockTakeDetail'] = "盤點明細";
$Lang['eInventory']['eClassApp']['StockTakeInstruction'] = "請輸入條碼或按掃描圖示進行條碼掃描。 如遺失條碼，可點擊左下角圖示，從未盤點物品中選擇將要盤點的物品。";
$Lang['eInventory']['eClassApp']['StockTakeInstructionForiOS'] = "請輸入條碼或按掃描圖示進行條碼掃描(iOS版本必須為13或以上)。 如遺失條碼，可點擊左下角圖示，從未盤點物品中選擇將要盤點的物品。";
$Lang['eInventory']['eClassApp']['StockTakeItemInfo'] = "盤點物品資料";
$Lang['eInventory']['eClassApp']['StockTakeNextLocation'] = "%s完成盤點，你是否到下個地方繼續盤點？";
$Lang['eInventory']['eClassApp']['StockTakeNotFound'] = "確定未尋獲";
$Lang['eInventory']['eClassApp']['StockTakeNotFoundBtn'] = "未尋獲";
$Lang['eInventory']['eClassApp']['StockTakeStatus']['Complete'] = "完成";
$Lang['eInventory']['eClassApp']['StockTakeStatus']['Incomplete'] = "未完成";
$Lang['eInventory']['eClassApp']['Stop'] = "停止";
$Lang['eInventory']['eClassApp']['Warning']['BarcodeNotFound'] = "找不到此條碼";
$Lang['eInventory']['eClassApp']['Warning']['GroupInChargeIsDifferent'] = "此物品屬於另一物品管理小組: %s,是否繼續?";
$Lang['eInventory']['eClassApp']['Warning']['HasDoneStocktake'] = "此物品已盤點";
$Lang['eInventory']['eClassApp']['Warning']['NotNeedStocktake'] = "此物品不需要盤點";
$Lang['eInventory']['eClassApp']['Warning']['PleaseDoStockInThisLocation'] = "此大量物品不屬於這位置，請將其帶回所屬位置%s進行盤點";
$Lang['eInventory']['eClassApp']['Warning']['PleaseInputBarcode'] = "請輸入物品條碼";

# Admin Console
$Lang['StudentAttendance']['AdminConsole']['ImportNewLinkageNote1'] = "eAttendance考勤管理系統用戶：<br>學生檔案為出席紀錄最終存儲的地方。因此，如果在此直接加入遲到、缺席、或早退紀錄，紀錄將不能與eAttendance考勤管理系統同步。";
$Lang['StudentAttendance']['AdminConsole']['ImportNewLinkageNote2'] = "(遲到紀錄亦無法由eAttendance考勤管理系統同步到eDiscipline操行管理系統)";
$Lang['StudentAttendance']['AdminConsole']['ImportNewLinkageNote3'] = "請進入eAttendance考勤管理加入出席紀錄。";
$Lang['AdminConsole']['ImportMessage']['AllRecordsAreImported'] = "所有紀錄已被匯入";
$Lang['AdminConsole']['ImportMessage']['SomeRecordsAreImported']  = "只有部份紀錄已被匯入";
$Lang['AdminConsole']['ImportMessage']['AllRecordsCannotBeImported'] = "所有紀錄未能被匯入";
$Lang['AdminConsole']['ImportMessage']['NoFileUploaded'] = "沒有上傳檔案";
$Lang['AdminConsole']['GuardianInfo']['import_remind_studentregistry_use_userlogin'] = "注意: <Br> - 第一欄為內聯網帳號 <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否) <Br> - 第九欄為監護人職業 <Br> - 第十欄為監護人工作公司或機構 <Br> - 第十一欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> 第十二欄為地址區碼<Br> - 第十三欄為住址街道 <Br> - 第十四欄為住址其他資料，如大廈，層數及單位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父親, 02 - 親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> (O - 其他, M - 澳門, T - &#27705;仔, C - 路環) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";
$Lang['AdminConsole']['GuardianInfo']['import_remind_studentregistry_use_userlogin_sms'] = "注意: <Br> - 第一欄為內聯網帳號 <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定為SMS的接收者 (1 - 是, 0 - 否) <br> - 第九欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否)  <Br> - 第十欄為監護人職業<Br> - 第十一欄為監護人工作公司或機構  <Br> - 第十二欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> 第十三欄為地址區碼<Br> - 第十四欄為住址街道 <Br> - 第十五欄為住址其他資料，如大廈，層數及單位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父親, 02 - 母親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> (O - 其他, M - 澳門, T - &#27705;仔, C - 路環) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";
# iTextbook
$Lang['itextbook']['itextbook'] = "電子課本";
$Lang['itextbook']['PurchaseRecord'] = "Purchase Record";
$Lang['itextbook']['PurchaseDate'] = "Purchase Date";
$Lang['itextbook']['NoOfUnits'] = "Number of Units";
$Lang['itextbook']['NoOfStudents'] = "Number of Students";
$Lang['itextbook']['AssignStudents'] = "Assign Students";
$Lang['itextbook']['UserQuotaLeft'] = "User Quota Left";
$Lang['itextbook']['QuotaAllocation'] = "Quota Allocation";
$Lang['itextbook']['Instruction'] = "Instruction";
$Lang['itextbook']['Remarks'] = "Remarks";
$Lang['itextbook']['Cooldown'] = "Cooldown";
$Lang['itextbook']['Quota_Released_and_Unassigned'] = "Quota Released and unassigned";
$Lang['itextbook']['ClassNo'] = "Class No.";
$Lang['itextbook']['Name'] = "Name";
$Lang['itextbook']['Unit_Assigned_and_Cant_Modified'] = "Unit is assigned to student and can't be modified.";
$Lang['itextbook']['NoQuotaLeft'] = "No Qouta Left!";
$Lang['itextbook']['UnitSelectedInvald'] = "No. of Units selected is invalid!";
$Lang['itextbook']['YouHave'] = "You have";
$Lang['itextbook']['QuotaReleasedNotReassign'] = "qouta(s) released and not re-assigned to any students!";
$Lang['itextbook']['ChangesWontSave'] = "Changes made won't be saved! Continue?";

# iTextbook - added on 2011-08-11
$Lang['itextbook']['PurchaseRecord'] = "購書記錄";
$Lang['itextbook']['BookList'] = "總書目";
$Lang['itextbook']['PurchaseDate'] = "購書日期";
$Lang['itextbook']['NoOfUnits'] = "單元數目";
$Lang['itextbook']['NoOfStudents'] = "學生數目";
$Lang['itextbook']['LicenseLeft'] = "剩餘許可證";
$Lang['itextbook']['DistributionStatus'] = "課本分配狀況";
$Lang['itextbook']['Distribution'] = "課本分配記錄";
$Lang['itextbook']['is_distributed'] = "已被分配";
$Lang['itextbook']['Edit'] = "編輯";
$Lang['itextbook']['Instruction'] = "說明";
$Lang['itextbook']['DistributionInstruction'][0] = "每次只限分配一套課本";
$Lang['itextbook']['DistributionInstruction'][1] = array("課本分配記錄新增後之", "日內為冷靜期");
$Lang['itextbook']['DistributionInstruction'][2] = "冷靜期後課本分配記錄不得更改";
$Lang['itextbook']['Book'] = "課本";
$Lang['itextbook']['AddStudentToDistribution'] = "新增學生記錄";
$Lang['itextbook']['Class'] = "班別";
$Lang['itextbook']['Name'] = "學生姓名";
$Lang['itextbook']['CoolDownPeriodEndAt'] = "冷靜期結束時間";
$Lang['itextbook']['DeleteDistribution'] = "刪除課本分配記錄";
$Lang['itextbook']['AddDistribution'] = "新增課本分配記錄";
$Lang['itextbook']['NoMoreLicenseLeft'] = "你已經用盡所有許可證配額";
$Lang['itextbook']['CreateDistributionConfirm'] = "是否確定新增課本分配記錄?\\n\\r(冷靜期於新增記錄後立即開始)";
$Lang['itextbook']['DeleteDistributionConfirm'] = "是否確定刪除課本分配記錄?\\n\\r(所有學生記錄將同時刪除)";
$Lang['itextbook']['DeleteDistributionUserConfirm'] = "是否確定要移除已選的紀錄？";
$Lang['itextbook']['AddNewBook'] = "新增課本";
$Lang['itextbook']['BookName'] = "課本名稱";
$Lang['itextbook']['ChapterList'] = "單元目錄";
$Lang['itextbook']['View'] = "查閱";
$Lang['itextbook']['DeleteBookWarning'] = "不能刪除已分配的課本";
$Lang['itextbook']['EditBook'] = "編輯課本";
$Lang['itextbook']['BookInstruction'][0] = "請將選取單元從左列拖曳至右列";
$Lang['itextbook']['BookInstruction'][1] = "選取單元數目必須與可用數目一致";
$Lang['itextbook']['BookInstruction'][2] = "若課本分配與學生, 選取之單元內容不得作任何改動, 惟單元次序則可調配";
$Lang['itextbook']['StudentsAssigned'] = "學生數目";
$Lang['itextbook']['AllowableNoOfChapters'] = "可用單元數目";
$Lang['itextbook']['SelectedChapters'] = "選取單元";
$Lang['itextbook']['LastUpdate'] = "最後更新日期";
$Lang['itextbook']['PleaseEnterBookName'] = "請輸入課本名稱";
$Lang['itextbook']['NoOfChapterExceedWarning'] = array("選取單元數目超出可用單元數目\\n\\r請移除", "篇多出的單元");
$Lang['itextbook']['NoOfChapterNotEnoughWarning'] = array("選取單元數目未及可用單元數目\\n\\r請再選取", "篇可加的單元");
$Lang['itextbook']['ImportRemark'] = array("每新増一位學生, 系統將自動扣除", "個許可證");
$Lang['itextbook']['AddStudentLicenseWarning'] = array("剩餘的許可證不足以新増", "位學生");
$Lang['itextbook']['RequireLicense'] = "所需的許可證";
$Lang['itextbook']['IncludeGiftChapter'] = "選取贈送單元";
$Lang['itextbook']['NoBookCreated'] = "在新增課本分配記錄前請先新增一套課本";
$Lang['itextbook']['BookLang']   = "課本語言";
$Lang['itextbook']['Lang']['en'] = "英文";
$Lang['itextbook']['Lang']['ch'] = "中文";
$Lang['itextbook']['Chapter']['en'] = "Chapter ";
$Lang['itextbook']['Chapter']['ch'] = "單元";

$Lang['itextbook']['AssignTeacherInCharge'] = "委任負責老師";
$Lang['itextbook']['ExpiryDate'] = "到期日";
$Lang['itextbook']['AddTeacher'] = "加入老師";
$Lang['itextbook']['TeachersName'] = "老師名稱";
$Lang['itextbook']['RemoveAssignTeacherConfirm'] = "刪除已選擇的負責老師?";

$Lang['itextbook']['Statistic'] = "統計";
$Lang['itextbook']['AllClasses'] = "所有班級";
$Lang['itextbook']['AllChapters'] = "所有單元";
$Lang['itextbook']['Title'] = "標題";
$Lang['itextbook']['Views'] = "檢視次數";
$Lang['itextbook']['Total'] = "總數";
$Lang['itextbook']['Unlimited'] = "無限制";

$Lang['StudentProfile']['Reamrk1'] = "<b>要匯入舊學年/學期紀錄？</b><br>請確認要匯入的學年及學期已經加入到 <b>學校基本設定 > 校曆表 > 學年/學期</b>，並且匯入檔中的學年及學期格式與 <b>學校基本設定 > 校曆表 > 學年/學期</b> 中所使用的相同。";
$Lang['StudentProfile']['Reamrk2'] = "<b>關於資料重複</b><br>如重複匯入相同的紀錄，系統將視重複的紀錄為新紀錄。因此，如要更新已經匯入的紀錄，請使用網頁介面進行，而非更新匯入檔匯，然後再行匯入。";

$Lang['kskgsmath']['kskgsmath'] = "無界學園";
$Lang['kskgsmath']['ManageSubject'] = "管理科目";
$Lang['kskgsmath']['used_up_all_license_msg'] = "你已使用所有許可證, 你不可再新增任何教室.";
$Lang['kskgsmath']['license_period_expired'] = "許可證已過期";
$Lang['kskgsmath']['license_exceed_msg1'] = "你只有";
$Lang['kskgsmath']['license_exceed_msg2'] = "個許可證可供使用。";
$Lang['kskgsmath']['license_exceed_msg3'] = "請返回";
$Lang['kskgsmath']['school_license'] = "學校許可證";
$Lang['kskgsmath']['student_license'] = "學生許可證";
$Lang['kskgsmath']['license_type'] = "許可證類型";
$Lang['kskgsmath']['license_period'] = "許可證有效期";

$Lang['kskgsmath']['kskgsmath_math'] = "數學科";
$Lang['kskgsmath']['kskgsmath_gs'] = "通識科";

$Lang['Britannica']['School'] = "Britannica School";
$Lang['Britannica']['LaunchPacks']['Science'] = "Britannica LaunchPacks Science";
$Lang['Britannica']['LaunchPacks']['Humanities'] = "Britannica LaunchPacks Humanities and Social Sciences";
$Lang['Britannica']['ImageQuest'] = "Britannica ImageQuest";
$Lang['PowerLesson']['PowerLesson'] = "PowerLesson";
$Lang['PowerLesson2']['PowerLesson2'] = "PowerLesson";$Lang['PowerLesson2']['PowerLesson2IP30'] = "PowerLesson 2";
$Lang['PowerLesson2']['PowerLesson2Stem'] = "PowerLesson 2 STEM";

$Lang['PasswordWarning'] = "你現在使用的密碼未符合要求！為加強你的帳戶保安，請立即更改密碼。";
$Lang['PasswordNotSafe'][1] = "";	// safe password
$Lang['PasswordNotSafe'][-1] = "請勿使用和登入名稱相等密碼，如戶口 是'peterlee'，密碼又用'peterlee'！";
$Lang['PasswordNotSafe'][-2] = "應使用至少由 [6] 個英文字母及數字混合組成的密碼！";
$Lang['PasswordNotSafe'][-3] = "應使用至少一個英文字母組成的密碼！";
$Lang['PasswordNotSafe'][-4] = "應使用至少一個數字組成的密碼！";
$Lang['PasswordNotSafe'][-5] = "請隔 [PERIOD] 天更改一次密碼！";


$Lang['UpdatePasswordOutSide'] = "按此更改密碼";


$Lang['General']['sys_copyright_purchased'] = "已授權給予[=schoolname=]使用此系統";
$Lang['General']['sys_copyright_trial'] = "此系統暫時授權給予[=schoolname=]試用";

$Lang['ForgotHashedPassword']['ResetPassword'] = "重置登入密碼";
$Lang['ForgotHashedPassword']['WarnMsgArr']['InvalidKey'] = "未能確定需要重置登入密碼的戶口，請重新提交重置登入密碼請求。";
$Lang['ForgotHashedPassword']['WarnMsgArr']['Expired'] = "你的重置登入密碼請求已過期，請重新提交重置登入密碼請求。";
$Lang['ForgotHashedPassword']['WarnMsgArr']['HasBeenReset'] = "你已經重置你的登入密碼。";
$Lang['ForgotHashedPassword']['WarnMsgArr']['CannotUpdatePassword'] = "你沒有權限更改你的登入密碼，請聯絡所屬學校或機構查詢。";
$Lang['ForgotHashedPassword']['WarnMsgArr']['UpdateSuccess'] = "你的登入密碼重置成功，頁面將於５秒後跳轉到登入頁面。";
$Lang['ForgotHashedPassword']['WarnMsgArr']['UpdateUnsuccess'] = "你的登入密碼重置失敗，請稍後再試。";


$Lang['plupload']['open_temp folder_failed'] = "不能開啟暫存資料夾";
$Lang['plupload']['open_input_stream_failed'] = "不能開啟 input stream";
$Lang['plupload']['open_output_stream_failed'] = "不能開啟 output stream";
$Lang['plupload']['move_uploaded_file_failed'] = "不能移動檔案";
$Lang['plupload']['no_runtime_found'] = "沒有支援plugin";
$Lang['plupload']['dnd_files_here'] = "拖曳檔案到此";
$Lang['plupload']['add_files'] = "加入檔案";
$Lang['plupload']['start_upload'] = "開始上載";
$Lang['plupload']['file_name'] = "檔案名稱";
$Lang['plupload']['file'] = "檔案";
$Lang['plupload']['status'] = "狀況";
$Lang['plupload']['size'] = "大小";
$Lang['plupload']['uploaded'] = "己上傳";
$Lang['plupload']['using_runtime'] = "現時使用";
$Lang['plupload']['invalid_file_ext'] = "檔案格式錯誤";
$Lang['plupload']['file_too_large'] = "檔案過大";


$Lang['wws']['eLearningProject'] = "自取其學";	//電子學習先導計劃教室
$Lang['wws']['MyeLearningProjectClassroom'] = "我的自取其學教室";

$Lang['DigitalArchiveModuleUpload']['Archive'] = "存檔";
$Lang['DigitalArchiveModuleUpload']['SelectFiles'] = "選擇文件";
$Lang['DigitalArchiveModuleUpload']['FileName'] = "文件名稱";
$Lang['DigitalArchiveModuleUpload']['FileOrFolderName'] = "文件或文件夾名稱";
$Lang['DigitalArchiveModuleUpload']['FileSize'] = "文件大小";
$Lang['DigitalArchiveModuleUpload']['ToFolder'] = "到文件夾";
$Lang['DigitalArchiveModuleUpload']['CreateNewFolder'] = "新增子文件夾於<!--FOLDER-->";
$Lang['DigitalArchiveModuleUpload']['RenameFile'] = "重新命名文件";
$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseInputFolderName'] = "請輸入文件夾名稱。";
$Lang['DigitalArchiveModuleUpload']['WarningMsg']['FolderExists'] = "文件夾已存在。";
$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseSelectAtLeastOneFile'] = "請至少選擇一個文件。";
$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseSelectFolder'] = "請選擇文件夾。";
$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseInputNewFileName'] = "請輸入新文件名稱。";
$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseDoNotUseForbiddenChars'] = "請不要使用字符 \\\\ / &lt; &gt; ? : * \" | 。";
$Lang['DigitalArchiveModuleUpload']['WarningMsg']['FileNameMustHaveExtension'] = "文件名稱必須要有擴展名。";
$Lang['DigitalArchiveModuleUpload']['ConfirmMsg']['ConfirmOverwriteDuplicatedFiles'] = "以下文件名稱重疊了，你確定要取代現有檔案?";
$Lang['DigitalArchiveModuleUpload']['ReturnMsg']['UploadFilesToDASuccess'] = "1|=|成功上載文件到電子文件。";
$Lang['DigitalArchiveModuleUpload']['ReturnMsg']['UploadFilestoDAUnsuccess'] = "0|=|上載文件到電子文件失敗了。";
$Lang['DigitalArchiveModuleUpload']['ReturnMsg']['CreateFolderSuccess'] = "1|=|成功新建文件夾。";
$Lang['DigitalArchiveModuleUpload']['ReturnMsg']['CreateFolderUnsuccess'] = "0|=|新建文件夾失敗。";
$Lang['DigitalArchiveModuleUpload']['ReturnMsg']['RenameFileSuccess'] = "1|=|成功重新命名文件。";
$Lang['DigitalArchiveModuleUpload']['ReturnMsg']['RenameFileUnsuccess'] = "0|=|重新命名文件失敗。";

# Digital Archive Menu
$Lang['Menu']['DigitalArchive']['Reports']['Title'] = "報告";
$Lang['Menu']['DigitalArchive']['Reports']['DeletionLog'] = "刪除紀錄";
$Lang['Menu']['DigitalArchive']['Settings']['AccessRight'] = "小組及權限";
$Lang['Menu']['DigitalArchive']['Settings']['AdministrativeDocument'] = "行政文件";
$Lang['Menu']['DigitalArchive']['Settings']['Settings_AllowAccessIP'] = "指定可存取電腦 IP";
$Lang['Tag']['DigitalArchive']['DeleteLog'] = "資料刪除紀錄";
$Lang['DigitalArchive']['Reports']['SystemReport']['DeletedDate'] = "刪除日期";
$Lang['DigitalArchive']['Reports']['SystemReport']['DeletedBy'] = "刪除者";
$Lang['DigitalArchive']['Reports']['SystemReport']['RecordType'] = "紀錄類別";
$Lang['DigitalArchive']['Reports']['SystemReport']['RecordInfo'] = "紀錄資料";


$Lang['login']['control'] = "帳戶登入保安措施";
$Lang['login']['attempt_limit'] = "允許相同 IP 相同帳戶 的嘗試次數";
$Lang['login']['lock_duration'] = "超出可嘗試次數後鎖定的時限";
$Lang['login']['unit_time'] = "次無效的登錄嘗試";
$Lang['login']['unit_minute'] = "分鐘";
$Lang['login']['input_alert'] = "請輸入一個數值！";
$Lang['login']['remark'] = "備註：當有帳戶超出可嘗試次數時，系統會向系統管理員發出電郵通知。";
$Lang['login']['password_alert'] = "安全警示";
$Lang['login']['password_notes'] = "<p>系統資料顯示以下 [=total_number=] 個帳戶的登入帳號和密碼相同，並且已經三個月以上沒有登入eClass。在這種情況下，很容易被他人盜用戶口來進行不當行為，例如：濫發垃圾電郵。</p>
<p>有見及此，eClass建議用戶重設密碼，方法如下：</p>";
$Lang['login']['password_follow_option_a'] = "按此設定隨機密碼<br />&nbsp; &nbsp; &nbsp; 〈備註：用戶可以使用「忘記密碼」功能，並透過備用電郵戶口取得新密碼。〉";
$Lang['login']['password_follow_option_b'] = "在「用戶管理」為所需帳戶逐一設定新密碼";
$Lang['login']['password_follow_option_or'] = "<p>&nbsp; &nbsp; &nbsp; &nbsp; 或</p>";
$Lang['login']['last_used'] = "最後登入時間";
$Lang['login']['password_follow_option_a_result_text'] = "已成功為 [=total_number=] 個帳戶設定隨機密碼。<br />多謝垂注！";
$Lang["Login"]["login"] = "登入";
$Lang["Login"]["login_id"] = "登入名稱";
$Lang["Login"]["login_id_input_alert"] = "請輸入登入名稱";
$Lang["Login"]["login_id_err"] = "登入名稱無效";
$Lang["Login"]["password"] = "密碼";
$Lang["Login"]["password_input_alert"] = "請輸入密碼";
$Lang["Login"]["password_err"]= "密碼錯誤";
$Lang['Login']['forgot_password'] = "忘記密碼?";

$Lang['replySlip']['Answer'] = "回應";
$Lang['replySlip']['User'] = "用戶";
$Lang['replySlip']['QuestionType']['En'] = "Question Type";
$Lang['replySlip']['QuestionTitle']['En'] = "Question Title";
$Lang['replySlip']['Topic']['En'] = "Topic";
$Lang['replySlip']['Options']['En'] = "Options";
$Lang['replySlip']['StartingFromThisColumn']['En'] = "starting from this column";
$Lang['replySlip']['QuestionType']['Ch'] = "問題類型";
$Lang['replySlip']['QuestionTitle']['Ch'] = "題目";
$Lang['replySlip']['Topic']['Ch'] = "標題";
$Lang['replySlip']['Options']['Ch'] = "選項";
$Lang['replySlip']['StartingFromThisColumn']['Ch'] = "由此欄開始";
$Lang['replySlip']['WarningArr']['invalidHeader'] = "標題格式不正確";
$Lang['replySlip']['WarningArr']['missingQuestionType'] = "問題類型不能留空";
$Lang['replySlip']['WarningArr']['invalidQuestionType'] = "問題類型不正確";
$Lang['replySlip']['WarningArr']['missingTitle'] = "題目 / 標題不能留空";
$Lang['replySlip']['WarningArr']['missingOptions'] = "此問題類型必須有選項";
$Lang['replySlip']['WarningArr']['cannotHaveOption'] = "此問題類型不能有選項";
$Lang['replySlip']['WarningArr']['mustAnswerQuestion'] = "請回答此問題";

# ePost - start - added on 2012-10-18
$Lang['ePost']['ePost'] = 'ePost';

$Lang['ePost']['Issues'] = '發行物';
$Lang['ePost']['Manage'] = '管理';
$Lang['ePost']['ArticleShelf'] = '文章庫';
$Lang['ePost']['ViewShelf'] = '檢視文章庫';
$Lang['ePost']['RequestList'] = '徵文列表';
$Lang['ePost']['ViewList'] = '檢視列表';
$Lang['ePost']['SubmitYourWriting'] = '呈交作品';
$Lang['ePost']['Postit'] = '發表作品';
$Lang['ePost']['Setting'] = '設定';
$Lang['ePost']['AssignStudentEditor'] = '委任學生編輯';
$Lang['ePost']['AssignPublishingManager'] = '委任出版經理';
$Lang['ePost']['YouDoNotHaveAnyRequestNow'] = '你現時沒有徵求任何文章';
$Lang['ePost']['NoIssuesFound'] = '沒有任何發行物';
$Lang['ePost']['PublishingManager'] = '出版經理';

$Lang['ePost']['FolderList'] = '資料夾列表';
$Lang['ePost']['CreateNewFolder'] = '建立新資料夾';
$Lang['ePost']['CreateNewIssue'] = '建立新發行物';
$Lang['ePost']['FolderName'] = '資料夾名稱';
$Lang['ePost']['NoOfIssue'] = '發行物的數量';
$Lang['ePost']['View'] = '檢視';
$Lang['ePost']['LastModified'] = '最後修訂日期〈修訂者〉';
$Lang['ePost']['PublicFolder'] = '公用資料夾';

$Lang['ePost']['NewFolder'] = '新資料夾';
$Lang['ePost']['EditFolder'] = '編輯資料夾';
$Lang['ePost']['Title'] = '標題';
$Lang['ePost']['ContainNewspapers'] = '包括報章';

$Lang['ePost']['ePostTitle'] = 'ePost標題';
$Lang['ePost']['Issue'] = '發行物';
$Lang['ePost']['NoOfPage'] = '頁數';
$Lang['ePost']['IssueDate'] = '發行日期';
$Lang['ePost']['Viewed'] = '已檢視';
$Lang['ePost']['PublishStatus'] = '出版狀況';

$Lang['ePost']['NewIssue'] = '新發行物';
$Lang['ePost']['EditIssue'] = '編輯發行物';
$Lang['ePost']['ViewIssueInformation'] = '檢視發行物資料';
$Lang['ePost']['Folder'] = '資料夾';
$Lang['ePost']['IssueName'] = '發行物名稱';
$Lang['ePost']['OpenToPublic'] = '開放';
$Lang['ePost']['Skin'] = '設計';
$Lang['ePost']['StudentEditor'] = '學生編輯';
$Lang['ePost']['StudentSelected'] = '所選學生';
$Lang['ePost']['AddAll'] = '加入全部學生';
$Lang['ePost']['AddSelected'] = '加入所選學生';
$Lang['ePost']['RemoveSelected'] = '移除所選學生';
$Lang['ePost']['RemoveAll'] = '移除全部學生';

$Lang['ePost']['EditIssueDesign'] = '編輯發行物版面設計';
$Lang['ePost']['SchoolLogo'] = '校徽';
$Lang['ePost']['SchoolLogoInstruction'] = '〈圖像大小：60 x 60 px， PNG〈首選〉/ JPG / GIF格式〉';
$Lang['ePost']['Uploading'] = '上載中...';
$Lang['ePost']['CustomizeBannerCover'] = '自訂封面橫幅';
$Lang['ePost']['CustomizeBannerCoverInstrucation'] = '〈圖像大小：718 x 120 px，PNG〈首選〉/ JPG / GIF格式〉';
$Lang['ePost']['HideEPostTitle'] = '隱藏ePost標題';
$Lang['ePost']['HideIssueName'] = '隱藏發行物名稱';
$Lang['ePost']['HidePageCategory'] = '隱藏頁面名稱';
$Lang['ePost']['CustomizeBannerInside'] = '自訂內頁橫幅';
$Lang['ePost']['CustomizeBannerInsideInstrucation'] = '〈圖像大小：718 x 80 px，PNG〈首選〉/ JPG / GIF格式〉';
$Lang['ePost']['Nil'] = '無';
$Lang['ePost']['TitleWillBeShownHere'] = '【ePost標題】';
$Lang['ePost']['NameWillBeShownHere'] = '【發行物名稱】';
$Lang['ePost']['PageNameWillBeShownHere'] = '【頁面名稱】';
$Lang['ePost']['CustomizeThemeIconInstruction'] = '〈圖像大小：90 x 65 px，PNG〈首選〉/ JPG / GIF格式〉';

$Lang['ePost']['CreateNewPage'] = '建立新頁';
$Lang['ePost']['EditIssueInformation'] = '編輯發行物資料';
$Lang['ePost']['PageTitle'] = '頁面名稱';
$Lang['ePost']['Theme'] = '主題';
$Lang['ePost']['Layout'] = '版面設計';
$Lang['ePost']['NoOfArticles'] = '文章數量';

$Lang['ePost']['NewPage'] = '新頁面';
$Lang['ePost']['EditPage'] = '編輯頁面';
$Lang['ePost']['Step'] = '步驟';
$Lang['ePost']['SelectThemeLayout'] = '選擇主題及版面設計';
$Lang['ePost']['SelectArticles'] = '選擇文章';
$Lang['ePost']['Type'] = '種類';
$Lang['ePost']['CoverPage1Article'] = '封面頁〈一篇文章〉';
$Lang['ePost']['CoverPage2Articles'] = '封面頁〈兩篇文章〉';
$Lang['ePost']['CoverPage3Articles'] = '封面頁〈三篇文章〉';
$Lang['ePost']['InsidePage1Article'] = '內頁〈一篇文章〉';
$Lang['ePost']['InsidePage2Articles'] = '內頁〈兩篇文章〉';
$Lang['ePost']['InsidePage3Articles'] = '內頁〈三篇文章〉';
$Lang['ePost']['InsidePage4Articles'] = '內頁〈四篇文章〉';
$Lang['ePost']['InsidePage5Articles'] = '內頁〈五篇文章〉';
$Lang['ePost']['InsidePage6Articles'] = '內頁〈六篇文章〉';
$Lang['ePost']['ThemeIcon'] = '主題圖示 ';
$Lang['ePost']['Customize'] = '自訂';
$Lang['ePost']['Default'] = '預設';
$Lang['ePost']['LocationInsertAfter'] = '位置〈在此頁之後插入〉';

$Lang['ePost']['NoArticleSelectedYet'] = '未有選擇文章';
$Lang['ePost']['Enjoy'] = '喜愛';
$Lang['ePost']['UnEnjoy'] = '取消喜愛';
$Lang['ePost']['Comment'] = '評語';
$Lang['ePost']['By'] = '建立者:';
$Lang['ePost']['ClickToEnjoy'] = '點擊表示喜愛';
$Lang['ePost']['ClickToUnEnjoy'] = '點擊取消喜愛';
$Lang['ePost']['PersonEnjoy'] = '人表示喜愛';
$Lang['ePost']['PeopleEnjoy'] = '人表示喜愛';
$Lang['ePost']['NoOneEnjoyYet'] = '暫未有人表示喜愛';

$Lang['ePost']['Article'] = '文章';
$Lang['ePost']['SourceType'] = '來源及種類';
$Lang['ePost']['Author'] = '作者';
$Lang['ePost']['DisplayAuthor'] = '於文章中顯示作者名稱';
$Lang['ePost']['Content'] = '內容';
$Lang['ePost']['From'] = '來自';
$Lang['ePost']['SelectAnother'] = '選擇另一個';
$Lang['ePost']['Image'] = '圖像';
$Lang['ePost']['Video'] = '影片';
$Lang['ePost']['Image1'] = '圖像一';
$Lang['ePost']['Image2'] = '圖像二';
$Lang['ePost']['ImageCaption'] = '圖像說明';
$Lang['ePost']['ImageCaption1'] = '圖像說明一';
$Lang['ePost']['ImageCaption2'] = '圖像說明二';
$Lang['ePost']['VideoCaption'] = '影片說明';
$Lang['ePost']['ImageAlignment'] = '圖像對齊';
$Lang['ePost']['VideoAlignment'] = '影片對齊';
$Lang['ePost']['FontStyle'] = '字體';
$Lang['ePost']['FontSize'] = '大小';
$Lang['ePost']['LineHeight'] = '行距';
$Lang['ePost']['Left'] = '靠左';
$Lang['ePost']['Right'] = '靠右';
$Lang['ePost']['Hide'] = '隱藏';
$Lang['ePost']['ImageSize'] = '圖像大小';
$Lang['ePost']['VideoSize'] = '影片大小';
$Lang['ePost']['Large'] = '大';
$Lang['ePost']['Medium'] = '中';
$Lang['ePost']['Small'] = '小';

$Lang['ePost']['ArticleList'] = '文章列表';
$Lang['ePost']['ImageVideo'] = '圖像 / 影片';
$Lang['ePost']['Class'] = '班級';
$Lang['ePost']['Student'] = '學生';
$Lang['ePost']['Source'] = '來源';
$Lang['ePost']['RequestTopic'] = '徵文的標題';
$Lang['ePost']['SubmissionDate'] = '呈交日期';
$Lang['ePost']['RecommendedBy'] = '推薦者';
$Lang['ePost']['Yes'] = '有';
$Lang['ePost']['Yes_1'] = '是';
$Lang['ePost']['No'] = '沒有';
$Lang['ePost']['No_1'] = '否';
$Lang['ePost']['TextImage'] = '文字 + 圖像';
$Lang['ePost']['TextVideo'] = '文字 + 影片';

$Lang['ePost']['CreateNewRequest'] = '建立新的徵文活動';
$Lang['ePost']['Topic'] = '標題';
$Lang['ePost']['CreateDate'] = '建立的日期';
$Lang['ePost']['Deadline'] = '限期';
$Lang['ePost']['NoOfEntriesReceived'] = '收到的作品數量';
$Lang['ePost']['AddedToShelf'] = '加入文章庫';

$Lang['ePost']['NewRequest'] = '新的徵文活動';
$Lang['ePost']['EditRequest'] = '編輯徵文活動';
$Lang['ePost']['Description'] = '描述';
$Lang['ePost']['PostedOnIssue'] = '在發行物發表';

$Lang['ePost']['CommentArea'] = '評語區';
$Lang['ePost']['Rubrics'] = '評量指標';
$Lang['ePost']['PleaseChoose'] = '請選擇......';
$Lang['ePost']['Redo'] = '要求重做';
$Lang['ePost']['DisplayIn'] = '顯示地方';
$Lang['ePost']['RedoAgain'] = '再次重做';
$Lang['ePost']['Accept'] = '接受';
$Lang['ePost']['DecideLater'] = '稍後決定';

$Lang['ePost']['CommentRubrics'][] = '文章筆觸流暢。';
$Lang['ePost']['CommentRubrics'][] = '詞彙生動豐富。';
$Lang['ePost']['CommentRubrics'][] = '文章結構完整緊密。';
$Lang['ePost']['CommentRubrics'][] = '內容描述生動有趣。';
$Lang['ePost']['CommentRubrics'][] = '文章首尾呼應。';
$Lang['ePost']['CommentRubrics'][] = '遣詞造句通順達意。';
$Lang['ePost']['CommentRubrics'][] = '內容主旨表達清晰。';
$Lang['ePost']['CommentRubrics'][] = '文章構思新穎。';
$Lang['ePost']['CommentRubrics'][] = '注意錯別字。';
$Lang['ePost']['CommentRubrics'][] = '詞彙不足。';
$Lang['ePost']['CommentRubrics'][] = '文章單調呆板。';
$Lang['ePost']['CommentRubrics'][] = '文章段落層次欠分明。';
$Lang['ePost']['CommentRubrics'][] = '內容描述單調枯燥。';
$Lang['ePost']['CommentRubrics'][] = '文章結尾欠完整。';
$Lang['ePost']['CommentRubrics'][] = '收辭技巧不足。';
$Lang['ePost']['CommentRubrics'][] = '句子結構有待改善。';

$Lang['ePost']['StudentEditorList'] = '學生編輯列表';
$Lang['ePost']['AddStudentEditor'] = '加入學生編輯';
$Lang['ePost']['ManagingRequest'] = '管理徵文活動';
$Lang['ePost']['ManagingIssues'] = '管理發行物';
$Lang['ePost']['Students'] = '學生';

$Lang['ePost']['PublishingManagerList'] = '出版經理列表';
$Lang['ePost']['AddPublishingManager'] = '加入出版經理';
$Lang['ePost']['Teacher'] = '老師';
$Lang['ePost']['Teachers'] = '老師';
$Lang['ePost']['NA'] = '不適用';

$Lang['ePost']['PostNow'] = '現在發表!';
$Lang['ePost']['AskingForArticles'] = '我們正在找尋與下列主題相關的文章。現在開始撰寫吧!';
$Lang['ePost']['SubmitOnOrBefore'] = array('在', '或之前呈交');

$Lang['ePost']['ClickHereToTypeInTitle'] = '在此點擊輸入標題...';
$Lang['ePost']['ClickHereToStartWriting'] = '在此點擊開始撰寫...';
$Lang['ePost']['OptionalAttachImageFile'] = '附加圖像檔案〈非必須的〉';
$Lang['ePost']['OptionalAttachImageFile1'] = '附加圖像檔案一〈非必須的〉';
$Lang['ePost']['OptionalAttachImageFile2'] = '附加圖像檔案二〈非必須的〉';
$Lang['ePost']['OptionalImageCaption'] = '圖像說明〈非必須的〉';
$Lang['ePost']['OptionalImageCaption1'] = '圖像說明一〈非必須的〉';
$Lang['ePost']['OptionalImageCaption2'] = '圖像說明二〈非必須的〉';
$Lang['ePost']['OptionalAttachVideoFiles'] = '附加 ".FLV" ".MOV" 或 ".MP4" 檔案〈非必須的〉';
$Lang['ePost']['OptionalAttachVideoFile'] = '附加 ".FLV" 檔案〈非必須的〉';
$Lang['ePost']['OptionalVideoCaption'] = '影片說明〈非必須的〉';

$Lang['ePost']['MyEntries'] = '我的作品';
$Lang['ePost']['EntryTitle'] = '作品標題';
$Lang['ePost']['Status'] = '狀況';
$Lang['ePost']['Saved'] = '已儲存';
$Lang['ePost']['Submitted'] = '已呈交';
$Lang['ePost']['Redone'] = '已重做';
$Lang['ePost']['PostedOn'] = '發表日期';

$Lang['ePost']['Attachment'] = '附件';
$Lang['ePost']['NoAttachment'] = '沒有附件';
$Lang['ePost']['DeleteAttachment'] = '刪除附件';

$Lang['ePost']['Records'] = '紀錄';
$Lang['ePost']['Total'] = '總數';
$Lang['ePost']['Page'] = array('第', '頁');
$Lang['ePost']['DisplayPage'] = array('每頁顯示', '項');

$Lang['ePost']['Select'] = '選擇';
$Lang['ePost']['Create'] = '建立';
$Lang['ePost']['Edit'] = '編輯';
$Lang['ePost']['Delete'] = '刪除';
$Lang['ePost']['Close'] = '關閉';
$Lang['ePost']['Next'] = '下一頁';
$Lang['ePost']['Previous'] = '前一頁';
$Lang['ePost']['Print'] = '列印';

//Siuwan (2013-02-05)
$Lang['ePost']['SortBy'] = "排序";
$Lang['ePost']['PublicationDate'] = "出版日期";
$Lang['ePost']['IssueTitle'] = "發行物標題";
$Lang['ePost']['WriteNow'] = "開始撰寫";
$Lang['ePost']['MySubmittedRecord'] = '已呈交作品';
$Lang['ePost']['Editors'] = "編輯";
$Lang['ePost']['ListView'] = "查看清單";
$Lang['ePost']['ViewThumb'] = "查看縮圖";
$Lang['ePost']['ShowMore'] = "顯示更多";
$Lang['ePost']['StartToWrite'] = "開始撰寫";
$Lang['ePost']['Guide'] = "指引";
$Lang['ePost']['AttachFile'] = "加入附件";
$Lang['ePost']['CreateNew'] = "新增";
$Lang['ePost']['Request'] = "徵文";
$Lang['ePost']['Requests'] = "徵文";
$Lang['ePost']['AssigningEditors'] = "指派編輯";
$Lang['ePost']['TeacherEditors'] = "老師編輯";
$Lang['ePost']['TeacherEditorsDescription'] = "<span>新增及管理徵文與發行物</span><span>發行</span>";
$Lang['ePost']['StudentEditors'] = "學生編輯";
$Lang['ePost']['StudentEditorsDescription'] = "<span>管理已指派的徵文與發行物</span>";
$Lang['ePost']['TargetLevels'] = "Target Levels";
$Lang['ePost']['IssueList'] = '發行物列表';
$Lang['ePost']['TeacherEditorList'] = '老師編輯列表';
$Lang['ePost']['AddTeacherEditor'] = '新增老師編輯';
$Lang['ePost']['InputIssueInformation'] = '輸入發行物資料';
$Lang['ePost']['SelectSkin'] = '選擇設計';
$Lang['ePost']['EditPages'] = '編輯頁面';
$Lang['ePost']['SubmitAndNext'] = '呈送並繼續';
$Lang['ePost']['CustomizeItem'] = '自訂';
$Lang['ePost']['CoverBanner'] = '封面橫幅';
$Lang['ePost']['InsidePageBanner'] = '內頁橫幅';
$Lang['ePost']['Preview'] = "預覽";
$Lang['ePost']['EditInformation'] = "編輯資料";
$Lang['ePost']['EditSkin'] = "編輯設計";
$Lang['ePost']['CoverPageInformationAndLayout'] = "封面資料及版面設計";
$Lang['ePost']['CoverPage'] = "封面";
$Lang['ePost']['EditPageInformationAndLayout'] = "編輯頁面";
$Lang['ePost']['LastModifiedBy'][0] = "由";
$Lang['ePost']['LastModifiedBy'][1] = "修改";
$Lang['ePost']['SelectArticle'] = "選擇文章";
$Lang['ePost']['EditorPage'] = "編輯首頁";
$Lang['ePost']['DeletePage'] = "刪除頁面";
$Lang['ePost']['IssueSkin'] = "發行物設計";
$Lang['ePost']['ActiveRequest'] = "進行中的徵文活動";
$Lang['ePost']['EntryTransferredToArticleShelf'] = "已加入文章庫的作品";
$Lang['ePost']['PublishedIssue'] = "已發行";
$Lang['ePost']['Show'] = '顯示';

$Lang['ePost']['Button']['Save'] = '儲存';
$Lang['ePost']['Button']['Submit'] = '呈送';
$Lang['ePost']['Button']['Submit_1'] = '呈交';
$Lang['ePost']['Button']['Update'] = '更新';
$Lang['ePost']['Button']['Cancel'] = '取消';
$Lang['ePost']['Button']['CreateAnotherPage'] = '建立另一頁';
$Lang['ePost']['Button']['BackToIssue'] = '返回發行物';
$Lang['ePost']['Button']['Edit'] = '編輯';
$Lang['ePost']['Button']['Back'] = '返回';
//Siuwan (2013-02-14)
$Lang['ePost']['Button']['SubmitAndNext'] = '呈送並繼續';
$Lang['ePost']['Button']['SubmitAndCreatePage'] = '呈送並建立頁面';
$Lang['ePost']['Button']['Finish'] = '完成';
$Lang['ePost']['Button']['Screen'] = '篩選';

$Lang['ePost']['Filter']['All'] = '全部';
$Lang['ePost']['Filter']['FoldersWithNewspaper'] = '附有報章的資料夾';
$Lang['ePost']['Filter']['EmptyFolders'] = '空的資料夾';
$Lang['ePost']['Filter']['Draft'] = '初稿';
$Lang['ePost']['Filter']['WaitingApproval'] = '等待審批';
$Lang['ePost']['Filter']['Published'] = '已發行';
$Lang['ePost']['Filter']['Courseware'] = '課件';
$Lang['ePost']['Filter']['NonCourseware'] = '非課件';
$Lang['ePost']['Filter']['DeadlinePassed'] = '限期已過';
$Lang['ePost']['Filter']['DeadlineNotPassed'] = '限期未過';
$Lang['ePost']['Filter']['SelectedArticle'] = '所選文章';
$Lang['ePost']['Filter']['ArticlePostedOnIssue'] = '在發行物發表的文章';
$Lang['ePost']['Filter']['StudentWithRequestsManaging'] = '可以管理需求的學生';
$Lang['ePost']['Filter']['StudentWithIssuesManaging'] = '可以管理發行物的學生';
$Lang['ePost']['Filter']['Go'] = '前往';
$Lang['ePost']['Filter']['Search'] = '搜尋';

$Lang['ePost']['Warning']['InputTitle'] = '請輸入標題!';
$Lang['ePost']['Warning']['InputContent'] = '請輸入內容!';
$Lang['ePost']['Warning']['InputEPostTitle'] = '請輸入ePost標題!';
$Lang['ePost']['Warning']['InputIssueName'] = '請輸入發行物名稱!';
$Lang['ePost']['Warning']['InputIssueDate'] = '請輸入發行日期!';
$Lang['ePost']['Warning']['InputPageTitle'] = '請輸入頁面名稱!';
$Lang['ePost']['Warning']['InputCaption'] = '請輸入圖像說明!';
$Lang['ePost']['Warning']['InputTopic'] = '請輸入標題!';
$Lang['ePost']['Warning']['InputDescription'] = '請輸入描述!';
$Lang['ePost']['Warning']['SelectFolder'] = '請選擇資料夾!';
$Lang['ePost']['Warning']['SelectSkin'] = '請選擇設計!';
$Lang['ePost']['Warning']['SelectType'] = '請選擇種類!';
$Lang['ePost']['Warning']['SelectLayout'] = '請選擇版面設計!';
$Lang['ePost']['Warning']['SelectThemeIcon'] = '請選擇主題圖示!';
$Lang['ePost']['Warning']['UploadThemeIcon'] = '請上載主題圖示!';
$Lang['ePost']['Warning']['SelectArticle'] = '請選擇文章!';
$Lang['ePost']['Warning']['SelectImageAlignment'] = '請選擇圖像對齊!';
$Lang['ePost']['Warning']['SelectAlignment'] = '請選擇對齊方式!';
$Lang['ePost']['Warning']['SelectWidth'] = '請選擇闊度!';
$Lang['ePost']['Warning']['SelectVideoAlignment'] = '請選擇影片對齊!';
$Lang['ePost']['Warning']['SelectDeadline'] = '請選擇限期!';
$Lang['ePost']['Warning']['SelectTopic'] = '請選擇標題!';
$Lang['ePost']['Warning']['GiveYourWorkTitle'] = '為你的作品提供標題!';
$Lang['ePost']['Warning']['UploadImageFile'] = '請上載圖像檔案!';
$Lang['ePost']['Warning']['UploadFLVFiles'] = '請上載 ".FLV" ".MOV" 或 ".MP4" 檔案!';
$Lang['ePost']['Warning']['UploadFLVFile'] = '請上載 ".FLV" 檔案!';
$Lang['ePost']['Warning']['FailedToUploadImage'] = '未能上載圖片!';
$Lang['ePost']['Warning']['FailedToLoadComment'] = '未能載入評語!';
$Lang['ePost']['Warning']['FailedToLoadComment'] = '未能載入評語!';
$Lang['ePost']['Warning']['FailtoLoadEnjoyList'] = '未能載入喜愛名單!';
$Lang['ePost']['Warning']['FailtoLoadEnjoyArticle'] = '未能載入喜愛文章!';
$Lang['ePost']['Warning']['LogintoLoadEnjoyArticle'] = '請先登入ePost，方可按"喜愛"。';
$Lang['ePost']['Warning']['UploadFileName'] = '檔案名稱不能重覆!';

$Lang['ePost']['Confirm']['RemoveFolder'] = array('你是否確定要刪除【', '】?\\n\\r〈注意：這個資料夾的報章會自動轉移到「', '」〉');
$Lang['ePost']['Confirm']['RemoveNewspaper'] = array('你是否確定要刪除【', '】?',);
$Lang['ePost']['Confirm']['RemovePage'] = array('你是否確定要刪除【', '】?',);
$Lang['ePost']['Confirm']['RemoveArticle'] = array('你是否確定要刪除【', '】?',);
$Lang['ePost']['Confirm']['RemoveRequest'] = array('你是否確定要刪除【', '】?',);;
$Lang['ePost']['Confirm']['RemoveWriting'] = array('你是否確定要刪除【', '】?',);
$Lang['ePost']['Confirm']['RemoveStudentEditor'] = array('你是否確定要刪除【', '】-【', '】?',);
$Lang['ePost']['Confirm']['RemovePublishingManager'] = array('你是否確定要刪除【', '】-【', '】?',);
$Lang['ePost']['Confirm']['RemoveAttachedImageFile'] = '你是否確定要刪除附加圖像檔案?';
$Lang['ePost']['Confirm']['RemoveAttachedVideoFile'] = '你是否確定要刪除附加影像檔案?';
$Lang['ePost']['Confirm']['RemoveCustomizeThemeIcon'] = '你是否確定要移除自訂的主題圖示？';
$Lang['ePost']['Confirm']['RemoveSchoolLogo'] = '你是否確定要移除校徽？';
$Lang['ePost']['Confirm']['RemoveCoverBanner'] = '你是否確定要移除自訂的封面橫幅？';
$Lang['ePost']['Confirm']['RemoveInsideBanner'] = '你是否確定要移除自訂的內頁橫幅？';
$Lang['ePost']['Confirm']['ChangePublishStatus'] = array('你是否確定要把【', '】-【', '】的出版狀況更改為【', '】?');
$Lang['ePost']['Confirm']['ChangePageLayout'] = array('倘若更改此頁面的版面設計，當中所有文章將會被刪除。\\n\\r你是否確定要更改版面設計?');
$Lang['ePost']['Confirm']['Submit'] = '你是否確定要呈交?';
$Lang['ePost']['Confirm']['ChangesWontBeMade'] = '不會儲存所作的更改。是否繼續?';
$Lang['ePost']['Confirm']['WorkSuccessfullySaved'] = '已經成功儲存你的作品。需要繼續嗎?';
$Lang['ePost']['Confirm']['RemoveStudentComment'] = '你是否確定刪除此留言?';

$Lang['ePost']['ExpiredMessage'] = 'ePost的使用期已過。<br/>請與你的系統管理員聯繫，以延續使用期。';
$Lang['ePost']['NoSubscriptionMessage'] = '尚未訂購或安裝ePost<br/>請與你的系統管理員聯繫。';

$Lang['ePost']['OrderA2Z'] = "A-Z";
$Lang['ePost']['OrderZ2A'] = "Z-A";
$Lang['ePost']['Latest'] = "最新";
$Lang['ePost']['Oldest'] = "最舊";

$Lang['ePost']['StudentComment'] = "學生評語";
$Lang['ePost']['NotAllow'] = "不容許";
$Lang['ePost']['Allow'] = "容許";
$Lang['ePost']['Editor'] = "編輯";
$Lang['ePost']['CommentPlaceHolder'] = "填寫評語";
$Lang['ePost']['Width'] = "闊度";
$Lang['ePost']['Alignment'] = "對齊方式";
$Lang['ePost']['Caption'] = "說明";
$Lang['ePost']['Add'] = "新增";
$Lang['ePost']['ReturnMessage']['DeleteSuccess'] = "紀錄已經刪除。";
$Lang['ePost']['ReturnMessage']['DeleteUnsuccess'] = "刪除紀錄失敗。";
$Lang['ePost']['ReturnMessage']['AddSuccess'] = "紀錄已經新增。";
$Lang['ePost']['ReturnMessage']['AddUnsuccess'] = "新增紀錄失敗。";
$Lang['ePost']['ReturnMessage']['UpdateSuccess'] = "紀錄已經更新。";
$Lang['ePost']['ReturnMessage']['UpdateUnsuccess'] = "更新紀錄失敗。";
# ePost - Finish

$Lang['eDiscipline']['ccm_term_report'] = "學期報告 (CCM)";
$Lang['eDiscipline']['ccm_term_report_desc'] = "這是學校經 eClass 客製的報告。";
$Lang['eDiscipline']['ccm_term_report_print_term'] = "列印學期";
$Lang['eDiscipline']['ccm_term_report_student_no'] = "學生編號";
$Lang['eDiscipline']['ccm_term_report_cur_student'] = "現時學生";
$Lang['eDiscipline']['ccm_term_report_punishment'] = "處分記錄";
$Lang['eDiscipline']['ccm_term_report_award_reason'] = "獎勵原因";
$Lang['eDiscipline']['ccm_term_report_punish_reason'] = "處分原因";
$Lang['eDiscipline']['ccm_term_report_award'] = "獎勵記錄";
$Lang['eDiscipline']['ccm_term_report_no_record'] = "未有任何紀錄";

$Lang['eDiscipline']['ttca_export_csv'] = '每月資料';
$Lang['eDiscipline']['ttca_export_csv_remark1'] = '只會匯出已批核了的良好及違規行為，和已批核兼開放矛學生的獎勵及懲罰紀錄。';
$Lang['eDiscipline']['ttca_export_csv_remark2'] = '學習態度分及操行分都是按學期重新計算的。';
$Lang['eDiscipline']['ttca_export_csv_remark3'] = '請小心選擇相應的學期及時段。否則會影響學習態度分及操行分的計算！';
$Lang['eDiscipline']['ttca_export_csv_remark4'] = '匯出之 CSV 檔可應用於通訊中心裡的合併電郵。';
$Lang['eDiscipline']['ApprovedDate'] = "批核日期";
$Lang['eDiscipline']['ReleasedDate'] = "開放日期";
$Lang['eDiscipline']['RejectedDate'] = "拒絕日期";
$Lang['eDiscipline']['WaivedDate'] = "豁免日期";
$Lang['eDiscipline']['SelectedMisconduct'] = "所選違規行為";
$Lang['eDiscipline']['SelectedAPItem'] = "所選獎懲項目";

$Lang['eDiscipline']['MisconductToPunishmentItemWhenReachSixMarks'] = "當違規行為扣分值達至六分轉換到懲罰項目";
$Lang['eDiscipline']['LateToPunishmentItemWhenReachSixMarks'] = "當遲到扣分達至六分轉換到懲罰項目";
$Lang['eDiscipline']['MisconductMaxScoreDeduction'] = "違規行為最高扣分值";
$Lang['eDiscipline']['Score'] = "分數";
$Lang['eDiscipline']['GenerateFromGM'] = "產生自良好及違規行為";
$Lang['eDiscipline']['EmailNotification'] = "電郵通告";
$Lang['eDiscipline']['AppNotifyMessage']['Subject'] = "最新操行紀錄系統通告提示
Latest Discipline Notice alert";
$Lang['eDiscipline']['AppNotifyMessage']['Content'] = "請於__ENDDATE__或之前簽署操行紀錄系統通告「__TITLE__」。
Please sign the Discipline Notice titled \"__TITLE__\" on or before __ENDDATE__.";
$Lang['eDiscipline']['Ng_Wah_AppNotifyMessage']['Single']['Subject'] = "學生違規行為提示
Student's accumlated Misbehaviour Alert";
$Lang['eDiscipline']['Ng_Wah_AppNotifyMessage']['Single']['Content'] = "貴子弟 __CNAME__ 於 __DATE__ 違犯校規。
違規事項：__CATEGORY__ __ITEM__

Please be notified that __ENAME__ has accumulated the following misbehaviour on __DATE__
Details： __CATEGORY__";
$Lang['eDiscipline']['Ng_Wah_AppNotifyMessage']['Group']['Subject'] = "學生違規行為提示
Student's accumlated Misbehaviour Alert";
$Lang['eDiscipline']['Ng_Wah_AppNotifyMessage']['Group']['Content'] = "貴子弟 __CNAME__ 於 __DATE__ 違犯校規。
違規事項：__CATEGORY__ __ITEM__

備註：
如學生於學期內，累積違犯上類校規3次，將以「警告」處分；累積違犯上類校規6次，將以「缺點」處分；隨後每累積3次違反校規（即9次、12次，如此類推），均以「缺點」處分。

Please be notified that __ENAME__ has accumulated the following misbehaviour on __DATE__
Details：__CATEGORY__ __ITEM__

Remark:
 
A student who violates the above-mentioned rules or regulations for 3 times, shall be disciplined a written warning. An accumulation of misbehaviour for 6 times shall be given demerit.
Further accumulation of misbehaviour for 3 times and onwards(i.e. 9 times, 12 times and so on), shall be given a misconduct.";

$Lang['eDiscipline']['AwardPunishmentNotifyMessage']['Award']['Subject'] = "New Award Record";
$Lang['eDiscipline']['AwardPunishmentNotifyMessage']['Punishment']['Subject'] = "New Punishment Record";

$Lang['eDiscipline']['GoodConductMisconductNotifyMessage']['Subject']['GoodConduct'] = "New Good Conduct Record";
$Lang['eDiscipline']['GoodConductMisconductNotifyMessage']['Subject']['Misconduct'] = "New Misconduct Record";
$Lang['eDiscipline']['GoodConductMisconductNotifyMessage']['Category_GoodConduct'] = "良好行為類型";
$Lang['eDiscipline']['GoodConductMisconductNotifyMessage']['Category_Misconduct'] = "違規類型";
$Lang['eDiscipline']['GoodConductMisconductNotifyMessage']['Item_GoodConduct'] = "良好行為項目";
$Lang['eDiscipline']['GoodConductMisconductNotifyMessage']['Item_Misconduct'] = "違規項目";

$Lang['eDiscipline']['UserLogin'] = "內聯網帳號";

$Lang['customization']['award_scheme'] = "全完獎勵計劃";

# PowerLesson App Web View - Start
$Lang['PowerLessonAppWeb']['ErrorDesc'][401] = '登入名稱或密碼不正確。';
$Lang['PowerLessonAppWeb']['ErrorDesc'][402] = 'Invalid license.';
$Lang['PowerLessonAppWeb']['ErrorDesc'][801] = '尚未安裝PowerLesson。';
$Lang['PowerLessonAppWeb']['ErrorDesc'][802] = '尚未訂購PowerLesson連線服務。';
$Lang['PowerLessonAppWeb']['ErrorDesc'][803] = 'PowerLesson連線服務已逾期。';

$Lang['PowerLessonAppWeb']['Username'] = '登入名稱';
$Lang['PowerLessonAppWeb']['Password'] = '密碼';
$Lang['PowerLessonAppWeb']['Login'] = '登入';
$Lang['PowerLessonAppWeb']['ContactEClassForInfo'] = '請聯絡eClass服務人員以獲取更多資訊。';

$Lang['PowerLessonAppWeb']['Loading'] = '載入中...';
$Lang['PowerLessonAppWeb']['NoClassroom'] = '沒有課室';
$Lang['PowerLessonAppWeb']['NoLesson'] = '沒有教案';

$Lang['PowerLessonAppWeb']['GoToIntranet'] = '前往內聯網';
$Lang['PowerLessonAppWeb']['BackToPowerLesson'] = '返回PowerLesson';
# PowerLesson App Web View - Finish

# eClass API [Begin]
$Lang['eClassAPI']['eClassApiSettings'] = "eClass API 設定";
$Lang['eClassAPI']['OpenForUse'] = "開放使用";
$Lang['eClassAPI']['ApiPassword'] = "API 密碼";
$Lang['eClassAPI']['RetypeApiPassword'] = "重新輸入 API 密碼";
$Lang['eClassAPI']['WarningMsg']['InputPassword'] = "請輸入密碼";
$Lang['eClassAPI']['WarningMsg']['InvalidPassword'] = "密碼無效";
# eClass API [End]

# Message Centre [Begin]
$Lang['MessageCentre']['pushMessageAry']['AllStatus'] = "所有狀態";
$Lang['MessageCentre']['pushMessageAry']['statusAry']['sendFailed'] = "傳送失敗";
$Lang['MessageCentre']['pushMessageAry']['statusAry']['sendSuccess'] = "傳送成功";
$Lang['MessageCentre']['pushMessageAry']['statusAry']['userHasRead'] = "用戶已讀";
$Lang['MessageCentre']['pushMessageAry']['statusAry']['waitingToSend'] = "等待傳送";
$Lang['MessageCentre']['pushMessageAry']['statusAry']['noRegisteredDevice'] = "沒有註冊推送訊息";
$Lang['MessageCentre']['pushMessageAry']['errorStatusAry']['cannotConnectToServiceProvider'] = "無法連接到服務供應商，請稍後再試。";
# Message Centre [End]
# eClass App [Begin]
$Lang['eClassApp']['AutoFillItem'] = '自動填充項目';
$Lang['eClassApp']['MessageContent'] = '訊息內容';
$Lang['eClassApp']['PushMessageTemplate'] = '推送訊息範本 (App)';
$Lang['eClassApp']['PushNotificationTemplate'] = '推送訊息範本';
$Lang['eClassApp']['DefaultUserNotifyUsingApp'] = '預設使用推送訊息通知用戶';
# eClass App [End]

$Lang["medical"]["stayOverNight"]		= "留宿";

$Lang['bookflix_expired'] = "BookFlix 已於 <expiry_date> 到期，請聯絡學校的 eClass 負責人以續期。";

$Lang['eClassStatistics']['MiscellaneousFileSize'] = "雜項檔案大小";

####################### set default wordings (used in convert gb wordings with script)
# Default setting wordings
$Lang['Identity']['Teacher'] = "教師";
$Lang['Identity']['AdminStaff'] = "職員";

# For default setting purpose
$Lang['Group']['DefaultCategory'][0]="Identity 基本組別";
$Lang['Group']['DefaultCategory'][1]="Admin 行政";
$Lang['Group']['DefaultCategory'][2]="Academic 學術";
$Lang['Group']['DefaultCategory'][3]="Class 班別";
$Lang['Group']['DefaultCategory'][4]="House 社級";
$Lang['Group']['DefaultCategory'][5]="ECA 課外活動";
$Lang['Group']['DefaultCategory'][6]="Miscellaneous 其他";

####################### Login Page
$Lang['LoginPage']['Login'] = "登入";
$Lang['LoginPage']['IncorrectLogin'] = "登入編號/密碼不正確。";
$Lang['LoginPage']['RequestSent'] = "重啟密碼程序將寄送至閣下備用電郵。";


####################### related to cust
$i_ChuenYuen_Award_Scheme = "全完獎勵計劃";
####################### related to cust end
$Lang['General']['PowerFlip'] = "PowerFlip";
$Lang['PowerLessonAppWeb']['AddLessonPlan'] = "新增教案";
$Lang['PowerLessonAppWeb']['PleaseSelectClassroom'] = "請選擇課室...";
$Lang['PowerLessonAppWeb']['ManageLessonPlan'] = "管理教案";

#### HKEdCity Login [Start]
$Lang['HKEdCity']['HKEdCityLogin'] = '香港教育城登入';
$Lang['HKEdCity']['HKEdCityUserLogin'] = '香港教育城帳戶名稱';
$Lang['HKEdCity']['HKEdCityPassword'] = '香港教育城帳戶密碼';
$Lang['HKEdCity']['PasswordRemarks'] = '香港教育城帳戶密碼只用作核實身份用途，eClass 系統不會儲存該密碼。';
$Lang['HKEdCity']['LinkToHKEdCityAcct'] = '連結香港教育城帳戶';
$Lang['HKEdCity']['NotLinkToHKEdCityAcct'] = '此香港教育城帳戶，未有連結任何 eClass 帳戶';
#### HKEdCity Login [End]

#### Markbook [Start]
$Lang['Markbook']['Markbook'] = '登分冊';
$Lang['Markbook']['NoMarkbook'] = "沒有登分冊";
$Lang['Markbook']['displayColHeader'] = '顯示欄標題';
#### Markbook [End]

#### HKUSPH [Start]
if($sys_custom['eClassApp']['HKUSPH']){
	$Lang['HKUSPH']['Title'] = '港大疾病監測';
	$Lang['HKUSPH']['NotASickLeave'] = '非因病請假';
	$Lang['HKUSPH']['Sick'] = '疾病';
	$Lang['HKUSPH']['Symptom'] = '病徵';
	$Lang['HKUSPH']['SickDate'] = '發病日期';
	$Lang['HKUSPH']['LeaveDate'] = '請假日期';
	$Lang['HKUSPH']['array_sick'] = array(
			0 => '上呼吸道感染（感冒、流感）',
			1 => '下呼吸道感染（氣管炎、肺炎）',
			2 => '胃腸炎',
			3 => '手足口病',
			4 => '水痘',
			5 => '急性結膜炎（紅眼症）',
			6 => '猩紅熱',
			7 => '流行性腮腺炎（生痄腮）',
			8 => '麻疹',
			9 => '風疹（德國麻疹）',
			10 => '未能判斷 (因未曾求醫)',
			11 => '其他病因:'
	);
	$Lang['HKUSPH']['array_symptom'] = array(
			0 => '發燒≥37.8°C',
			1 => '疲倦',
			2 => '頭痛',
			3 => '眼分泌物增多',
			4 => '鼻塞/流鼻水/打噴嚏',
			5 => '口腔出現水泡',
			6 => '咳嗽',
			7 => '嘔吐',
			8 => '發冷',
			9 => '喉嚨痛',
			10 => '皮疹',
			11 => '腹瀉',
			12 => '四肢出現水泡',
			13 => '其他病徵:'
	);
	$Lang['HKUSPH']['Please choose at least one sickless'] = '請選擇疾病';
	$Lang['HKUSPH']['Please choose at least one symptom'] = '請選擇病徵';
	$Lang['HKUSPH']['Please fill in the sick date'] = '請輸入病發日期';
	$Lang['HKUSPH']['Please fill in the other sickless'] = '請輸入其他病因';
	$Lang['HKUSPH']['Please fill in the other symptom'] = '請輸入其他病徵';
	$Lang['HKUSPH']['HKUSPH Report'] = '港大疾病監測報告';
	$Lang['HKUSPH']['Sick date must be earlier than leave date'] = '病發日期必須早於請假日期';
}
#### HKUSPH [End]

## Security
$Lang['Security']['Authentication'] = "登入認證";
$Lang['Security']['AuthenticationInstruction'] = "請輸入用戶密碼進行登入認證。";
$Lang['Security']['AuthenticationSetting'] = "首次進入時需要進行登入認證";
$Lang['Security']['AllowAccessIP'] = "指定可存取電腦 IP";
$Lang['Security']['TerminalIPList'] = "電腦的 IP 位址";
$Lang['Security']['TerminalIPInput'] = "請把電腦的 IP 地址每行一個輸入";
$Lang['Security']['TerminalYourAddress'] = "你現時的 IP 地址";
$Lang['Security']['TerminalIPSettingsDescription'] = "
在 IP 位址的格上, 可以使用:
<ol><li>固定 IP 位址 (e.g. 192.168.0.101)</li>
<li>IP 位址一個範圍 (e.g. 192.168.0.[10-100])</li>
<li>CISCO Style 範圍(e.g. 192.168.0.0/24)</li>
<li>容許所有位址 (輸入 0.0.0.0)</li></ol>
<span class='tabletextrequire'>建議該電腦設定固定 IP 地址 (即不採用 DHCP)， 以確保資料不會被其他電腦更改。</span><br>
本系統設有安全措施防範假冒的IP 地址 (Faked IP Address)。
";
$Lang['Security']['WarnMsgArr']['InvalidIPAddress'] = "IP位址不正確。";
$Lang['Security']['WarnMsgArr']['ComputerIpNotAllowed'] = "你的電腦不被允許進入<!--ModuleName-->系統，請與<!--ModuleName-->系統管理員聯絡。";
$Lang['Security']['WarnMsgArr']['AuthenticationRequired'] = "需要進行登入認證";

####################### This should be placed at the bottom

$i_Files_BatchOption_RemoveAccount = "刪除檔案伺服器上的戶口 <font color=red></font>";
$i_Files_alert_RemoveAccount = "你確定要移除這個檔案伺服器的戶口? ";

if (!$NoLangWordings && is_file("$intranet_root/templates/lang.$intranet_session_language.customized.php"))
{
	include("$intranet_root/templates/lang.$intranet_session_language.customized.php");
}

# if setting of merit/demerit title are set
# load the setting
$merit_demerit_customize_file = "$intranet_root/file/merit.b5.customized.txt";
if(file_exists($merit_demerit_customize_file) && is_file($merit_demerit_customize_file) && filesize($merit_demerit_customize_file)!=0){
	//if ($file_content_merit_demeirt_wordings=="")
	$file_content_merit_demeirt_wordings = get_file_content($merit_demerit_customize_file);
	$lines = explode("\n",$file_content_merit_demeirt_wordings);

	$i_Merit_Merit = $lines[0];
	$i_Merit_MinorCredit = $lines[1];
	$i_Merit_MajorCredit = $lines[2];
	$i_Merit_SuperCredit = $lines[3];
	$i_Merit_UltraCredit = $lines[4];

	$i_Merit_BlackMark = $lines[5];
	$i_Merit_MinorDemerit = $lines[6];
	$i_Merit_MajorDemerit = $lines[7];
	$i_Merit_SuperDemerit = $lines[8];
	$i_Merit_UltraDemerit = $lines[9];

	if($lines[10])
	{
		$i_Merit_Warning = $lines[10];
		$Lang['eDiscipline']['Warning'] = $i_Merit_Warning;
	}
}

$i_Merit_TypeArray = array(
		$i_Merit_Merit,
		$i_Merit_MinorCredit,
		$i_Merit_MajorCredit,
		$i_Merit_SuperCredit,
		$i_Merit_UltraCredit,
		$i_Merit_BlackMark,
		$i_Merit_MinorDemerit,
		$i_Merit_MajorDemerit,
		$i_Merit_SuperDemerit,
		$i_Merit_UltraDemerit
);

$i_Merit_TypeArrayWithWarning = array(
		$i_Merit_Merit,
		$i_Merit_MinorCredit,
		$i_Merit_MajorCredit,
		$i_Merit_SuperCredit,
		$i_Merit_UltraCredit,
		$i_Merit_Warning,
		$i_Merit_BlackMark,
		$i_Merit_MinorDemerit,
		$i_Merit_MajorDemerit,
		$i_Merit_SuperDemerit,
		$i_Merit_UltraDemerit
);

$i_frontpage_eclass_eclass_children = "子女eClass";
$i_frontpage_eclass_eclass_children_setting = "開放給家長檢視 (與子女有關的學習內容及評估作業)";
$i_frontpage_eclass_eclass_enable_children_setting = "開放所有課室給家長檢視";
$i_eClass_Setting = "eClass 顯示設定";
$i_frontpage_eclass_eclass_embeded_powerLesson = "開放所有課室使用PowerLesson";

	### Admin Console [edit from lang.ip20]
$ec_iPortfolio['guardian_import_remind'] = "注意: <br>  - #第一欄為班別 <br> - #第二欄為學號 <br> - #第三欄為登入名稱 <br> - #第四欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <br>  - 第五欄為監護人的英文姓名 <br>  - 第六欄為監護人的中文姓名 <br>  - 第七欄為監護人的聯絡號碼 <br> - 第八欄為監護人的緊急聯絡號碼 <br>  - *第九欄為監護人與學生關係 <br>  - 第十欄為設定為學生的主監護人 (1 - 是, 0 - 否) <br>  - 第十一欄為設定為學生的緊急聯絡人 (1 - 是, 0 - 否) <br> - 第十二欄為監護人職業 <br>  - 第十三欄為監護人工作公司或機構 <br> - 第十四欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<br>  - 第十五欄為住址<br>  * (01 - 父親, 02 - 母親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br>  - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>  <br>".$Lang['General']['OtherField'];
$ec_iPortfolio['guardian_import_remind_studentregistry'] = "注意: <br>  - #第一欄為班別 <br> - #第二欄為學號 <br> - #第三欄為登入名稱 <br> - #第四欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <br>  - 第五欄為監護人的英文姓名 <br>  - 第六欄為監護人的中文姓名 <br>  - 第七欄為監護人的聯絡號碼 <br>  - 第八欄為監護人的緊急聯絡號碼 <br>  - <span style='color:#0000ff'>*<span style='font-size:8px'>9</span></span> 第九欄為監護人與學生關係 <br>  - 第十欄為設定為學生的主監護人 (1 - 是, 0 - 否) <br>  - 第十一欄為設定為學生的緊急聯絡人 (1 - 是, 0 - 否) <br> - 第十二欄為監護人職業 <br> - 第十三欄為監護人工作公司或機構 <br>  - 第十四欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<br>  - <span style='color:#0000ff'>*<span style='font-size:8px'>14</span></span> 第十五欄為地址區碼<br>  - 第十六欄為住址街道 <br>  - 第十七欄為住址其他資料，如大廈，層數及單位 <br>  <span style='color:#0000ff'>*<span style='font-size:8px'>9</span></span> (01 - 父親, 02 - 親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br>  <span style='color:#0000ff'>*<span style='font-size:8px'>14</span></span> (O - 其他, M - 澳門, T - &#27705;仔, C - 路環) <br>  - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br> <br>".$Lang['General']['OtherField'];
$ec_iPortfolio['guardian_import_remind_sms_studentregistry'] = "注意: <Br> - 第一欄為RegNo 編號, 應加上#（如 \"#0025136\")，以確保編號數字的完整！ <Br> - 第二欄為監護人的英文姓名 <Br> - 第三欄為監護人的中文姓名 <Br> - 第四欄為監護人的聯絡號碼 <Br> - 第五欄為監護人的緊急聯絡號碼 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六欄為監護人與學生關係 <Br> - 第七欄為設定為學生的主監護人 (1 - 是, 0 - 否) <Br> - 第八欄為設定為SMS的接收者 (1 - 是, 0 - 否) <br> - 第九欄為設定監護人做緊急聯絡 (1 - 是, 0 - 否)  <Br> - 第十欄為監護人職業<Br> - 第十一欄為監護人工作公司或機構 <Br> - 第十二欄為監護人是否與該學生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> 第十三欄為地址區碼<Br> - 第十四欄為住址街道 <Br> - 第十五欄為住址其他資料，如大廈，層數及單位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父親, 02 - 母親, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 親戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> (O - 其他, M - 澳門, T - &#27705;仔, C - 路環) <Br> - <font color=red>新匯入的監護人資料將會覆蓋現有的監護人資料。</font><br>";
$i_SAMS_import_error_wrong_subject_format = "錯誤的科目標題匯入格式";

### HKSSF Connection
$Lang['HKSSF']['module'] = "香港學界體育聯會學生運動員註冊系統連接設定";
$Lang['HKSSF']['access_token'] = '認證碼';
$Lang['HKSSF']['refresh'] = '刷新';
$Lang['HKSSF']['remark'] = '刷新後原有的認證碼將會失效<br/>以其作驗證連接的系統須更新驗證碼才能繼續連接eClass';

### PaGamO License Setting ###
$Lang['PaGamO']['admin_module'] = "PaGamO 許可證設定";
$Lang['PaGamO']['std_quota'] = "學生配額";
$Lang['PaGamO']['tch_quota'] = "教師配額";
$Lang['PaGamO']['license_display_1_t'] = "你的PaGamO教師許可證有";
$Lang['PaGamO']['license_display_1_s'] = "你的PaGamO學生許可證有";
$Lang['PaGamO']['license_display_2'] = " 個，尚有 ";
$Lang['PaGamO']['license_display_3'] = " 個可供使用。";
$Lang['PaGamO']['license_display_4'] = "你已使用的許可證為 ";
$Lang['PaGamO']['license_display_5'] = " 個。";
$Lang['PaGamO']['choose_school'] = "請選擇一所學校";
$Lang['PaGamO']['confirm_class_level'] = "確定要連結此PaGamO 許可證至級別";

$MedalListMenu['medallist_sys_setting'] = "設置";
$MedalListMenu['medallist_sys_management']= "管理";
$MedalListMenu['game'] = "項目";
$MedalListMenu['event'] = "賽事";
$MedalListMenu['sport'] = '運動';
$Lang['Header']['Menu']['MedalList'] = "獎牌運動員";
$Lang['SFOC']['Settings']['GameChi'] = "項目名稱（中文）";
$Lang['SFOC']['Settings']['GameEng'] = "項目名稱（英文）";
$Lang['SFOC']['Settings']['EventChi'] = "賽事名稱（中文）";
$Lang['SFOC']['Settings']['EventEng'] = "賽事名稱（英文）";
$Lang['SFOC']['Settings']['SportChi'] = '運動名稱（中文）';
$Lang['SFOC']['Settings']['SportEng'] = '運動名稱（英文）';
$Lang['SFOC']['Settings']['SportIntroChi'] = "運動簡介（中文）";
$Lang['SFOC']['Settings']['SportIntroEng'] = "運動簡介（英文）";
$Lang['SFOC']['Settings']['Icon'] = '圖示';
$MedalListMenu['medallist'] = "獎牌運動員";
$Lang['SFOC']['MedalList']['Year'] = "年份";
$Lang['SFOC']['MedalList']['Location'] = "地點";
$Lang['SFOC']['MedalList']['LocationChi'] = "地點（中文）";
$Lang['SFOC']['MedalList']['LocationEng'] = "地點（英文）";
$Lang['SFOC']['MedalList']['Sport'] = "運動";
$Lang['SFOC']['MedalList']['SportChi'] = "運動（中文）";
$Lang['SFOC']['MedalList']['SportEng'] = "運動（英文）";
$Lang['SFOC']['MedalList']['Game'] = "項目";
$Lang['SFOC']['MedalList']['Event'] = "賽事";
$Lang['SFOC']['MedalList']['NameOfAthlete'] = "運動員名稱";
$Lang['SFOC']['MedalList']['NameOfAthleteChi'] = "運動員名稱（中文）";
$Lang['SFOC']['MedalList']['NameOfAthleteEng'] = "運動員名稱（英文）";
$Lang['SFOC']['MedalList']['Medal'] = "獎牌";
$Lang['SFOC']['MedalList']['WebsiteCode'] = "網站代號";
$Lang['SFOC']['MedalList']['Remark'] = "备注";
$Lang['SFOC']['MedalList']['emptyYear'] = "年份不能為空";
$Lang['SFOC']['MedalList']['emptyLocation'] = "地點不能為空";
$Lang['SFOC']['MedalList']['emptyGame'] = "項目不能為空";
$Lang['SFOC']['MedalList']['emptyEvent'] = "賽事不能為空";
$Lang['SFOC']['MedalList']['emptySport'] = "運動不能為空";
$Lang['SFOC']['MedalList']['emptyName'] = "運動員名稱不能為空";
$Lang['SFOC']['MedalList']['emptyMedal'] = "獎牌不能為空";
$Lang['SFOC']['MedalList']['noMappingGame'] = "沒有對應的項目";
$Lang['SFOC']['MedalList']['noMappingEvent'] = "沒有對應的賽事";
$Lang['SFOC']['MedalList']['noMappingSport'] = "沒有對應的運動";
$Lang['SFOC']['MedalList']['MappingEvent'] = "已有對應的賽事";
$Lang['SFOC']['MedalList']['MappingGame'] = "已有對應的頂目";
$Lang['SFOC']['MedalList']['MappingSport'] = "已有對應的運動";

$Lang['SystemSetting']['LoginRecords']['Title'] = '登入紀錄';
$Lang['SystemSetting']['AccountLog']['Title'] = '用戶更改紀錄';
$Lang['SystemSetting']['AccountLog']['Keyword'] = '關鍵字';
$Lang['SystemSetting']['AccountLog']['showID'] = '顯示用戶編號';
$Lang['SystemSetting']['AccountLog']['logType'] = '紀錄類別';
$Lang['SystemSetting']['AccountLog']['DateRange'] = '日期範圍';
$Lang['SystemSetting']['AccountLog']['From'] = '自';
$Lang['SystemSetting']['AccountLog']['To'] = '至';
$Lang['SystemSetting']['AccountLog']['LogBy'] = '用戶';
$Lang['SystemSetting']['AccountLog']['User'] = '影響用戶';
$Lang['SystemSetting']['AccountLog']['LogBylogin'] = '內聯網帳戶 (用戶)';
$Lang['SystemSetting']['AccountLog']['Userlogin'] = '內聯網帳戶 (影響用戶)';
$Lang['SystemSetting']['AccountLog']['Action'] = '更改行為';
$Lang['SystemSetting']['AccountLog']['Date'] = '日期';
$Lang['SystemSetting']['AccountLog']['Time'] = '時間';
$Lang['SystemSetting']['AccountLog']['IPAddress'] = 'IP位址';
$Lang['SystemSetting']['AccountLog']['Create'] = '創建';
$Lang['SystemSetting']['AccountLog']['Update'] = '修改';
$Lang['SystemSetting']['AccountLog']['Read'] = '檢閱';
$Lang['SystemSetting']['AccountLog']['Delete'] = '刪除';
$Lang['SystemSetting']['AccountLog']['FilterOption'] = '篩選選項';
$Lang['SystemSetting']['AccountLog']['All'] = '所有';
$Lang['SystemSetting']['AccountLog']['ShowPage'] = '每頁顯示';
$Lang['SystemSetting']['AccountLog']['NextPage'] = '下一頁';
$Lang['SystemSetting']['AccountLog']['PrevPage'] = '上一頁';
$Lang['SystemSetting']['AccountLog']['PageItem'] = '總數';
$Lang['SystemSetting']['AccountLog']['PageMax'] = '總頁數';
$Lang['SystemSetting']['AccountLog']['CurrPage'] = '頁';
$Lang['SystemSetting']['AccountLog']['PageRecord'] = '紀錄';

$Lang['SystemSetting']['ReportLog']['Title'] = '報告產生紀錄';
$Lang['SystemSetting']['ReportLog']['Column']['Rtype'] = '報告類別';
$Lang['SystemSetting']['ReportLog']['Column']['Action'] = '產生行為';
$Lang['SystemSetting']['ReportLog']['Rtype']['login'] = '登入紀錄';
$Lang['SystemSetting']['ReportLog']['Rtype']['account'] = '用戶更改紀錄';
$Lang['SystemSetting']['ReportLog']['Rtype']['report'] = '報告產生紀錄';
$Lang['SystemSetting']['ReportLog']['Action']['export'] = '匯出  (CSV)';
$Lang['SystemSetting']['ReportLog']['Action']['html'] = '列印 (HTML)';
$Lang['SystemSetting']['ReportLog']['Action']['pdf'] = '列印 (PDF)';


### eLearning Timetable ###
$Lang['eLearningTimetable']['weekendAlert'] = "如項目的開始時間為星期六或日，則不會顯示在網上學習表。";
$Lang['eLearningTimetable']['eLearningTimetable'] = "每週網上學習表";
$Lang['eLearningTimetable']['startTime'] = "開始時間";
$Lang['eLearningTimetable']['dueDate'] = "到期日";
$Lang['eLearningTimetable']['preLesson'] = "課前";
$Lang['eLearningTimetable']['postLesson'] = "課後";
$Lang['eLearningTimetable']['untitledLessonPlan'] = "未命名課堂";
$Lang['eLearningTimetable']['msg']['dateNotStartedItem'] = "此項目尚未到達開始日期 [{{time}}]，請稍後重試。";
$Lang['eLearningTimetable']['msg']['timeNotStartedItem'] = "此項目尚未到達開始時間 [{{time}}]，請稍後重試。";
$Lang['eLearningTimetable']['msg']['notStartedLessonHours'] = "課堂將於[{{time}}]開始，請稍後重試。";
$Lang['eLearningTimetable']['msg']['notStartedLessonMinutes'] = "尚餘{{time}}分鐘才開始這課堂。";
$Lang['eLearningTimetable']['msg']['lessonWillStartSoon'] = "課堂即將開始，請稍後。";


$Lang['SubsidyIdentity']['ClassName'] = "班別";
$Lang['SubsidyIdentity']['ClassNumber'] = "班號";
$Lang['SubsidyIdentity']['StudentName'] = "學生姓名";
$Lang['SubsidyIdentity']['EffectivePeriod'] = "有效時段";

$Lang['eLearningSection']['eLearningTimetable'] = "每週網上學習表";
$Lang['eLearningSection']['eClass'] = "網上教室";
$Lang['eLearningSection']['powerlesson2'] = "PowerLesson 2";
$Lang['eLearningSection']['NotEnrolledInAnyClassroom'] = "尚未加入任何網上教室";
$Lang['eLearningSection']['CreateClassroom'] = "新增網上教室";


if($sys_custom['DHL'] && file_exists($intranet_root.'/lang/DHL/dhl_lang.b5.php'))
{
	include_once($intranet_root.'/lang/DHL/dhl_lang.b5.php');
}

if($sys_custom['KiangWuNursing_Macau'] && file_exists($intranet_root.'/lang/KiangWuNursing_Macau/kwnm_lang.b5.php'))
{
    include_once($intranet_root.'/lang/KiangWuNursing_Macau/kwnm_lang.b5.php');
}

if (isset($sys_custom['Project_Label']) && $sys_custom['Project_Alias']) {
	if (file_exists($intranet_root.'/lang/' . $sys_custom['Project_Label']. '/' . $sys_custom['Project_Alias'] . basename(__FILE__))
		&& isset($sys_custom[$sys_custom['Project_Label']]) && $sys_custom[$sys_custom['Project_Label']]
	) {
		include($intranet_root.'/lang/' . $sys_custom['Project_Label']. '/' . $sys_custom['Project_Alias'] . basename(__FILE__));
	}
}
if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")||($sys_custom['project']['NCS'])){
	include_once($intranet_root."/ncs/lang.ncs.b5.php");
}
####################### This should be placed at the bottom
#### Attention: you should not add any wordings below
#### Attention: you should not add any wordings below
#### Attention: you should not add any wordings below
?>
