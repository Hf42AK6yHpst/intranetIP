<?php
# using: 

// Please do NOT add/ update/ delete any of the entry in this file (By Kenneth Chung 20090813)
// if you wish to add a language, please add it in lang.en.php

/********************** Change Log ***********************/
# Date	: 2015-06-16 [Jason]
#				replace split() by explode() in order to support php 5.4+
#
/******************* End Of Change Log *******************/

$i_title = "eClass IP";
$i_home_title  = "eClass IP";
$i_admin_title = "eClass IP Administrative Management Console";
$i_no_record_exists_msg = "There is no record at the moment.";
$i_no_record_exists_msg2 = "There is no record at this moment.";
$i_no_record_searched_msg = "No record found";
$i_no_search_result_msg = "Sorry, your search yielded no results. Please revise your search query and try again.";
$i_alert_or = " or ";
$i_alert_pleasefillin = "Please fill in the ";
$i_alert_pleaseselect = "Please select ";
$i_select_file = "Select File";
$i_general_startdate = "Start Date";
$i_general_enddate = "End Date";
$i_general_title = "Title";
$i_general_description = "Description";
$i_general_status = "Status";
$i_general_clickheredownloadsample = "Click here to download sample";
$i_general_sysadmin = "<B>System Admin</B>";
$i_general_WholeSchool = "Whole School";
$i_general_TargetGroup = "Target Group(s)";
$i_general_Poster = "Posted By";
$i_general_BasicSettings = "Basic Settings";
$i_general_NotSet = "Not Applicable";
$i_general_EachDisplay = "Display";
$i_general_PerPage = "/Page";
$i_general_DisplayOrder = "Display Order";
$i_general_Teacher = "Teacher";
$i_general_Day = "Day";
$i_general_Days = "Day(s)";
$i_general_Month = "Month";
$i_general_Months = "Months";
$i_general_Year = "Year";
$i_general_Years = "Years";
$i_general_Format = "Format";
$i_general_Format_web = "Web";
$i_general_Format_word = "MSWord";
$i_general_Type = "Type";
$i_general_SelectTarget = "Select Target";
$i_general_SelectOption = "Select Option";
$i_general_ViewDetailRecords = "View Detailed Records";
$i_general_MonthShortForm = array(
"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
);
$i_general_more = "More";
$i_general_subject_teacher = "Subject Teachers";
$i_general_ImportFailed = "The following records cannot be imported.";
$i_general_targetParent = "'s Parent";
$i_general_select_parent = "Select Parent";
$i_general_targetGuardian = "'s Guardian(s)";
$i_general_select_guardian_from_student = "Select Guardians from students";
$i_general_record_date = "Record Date";
$i_general_record_time = "Record Time";
$i_general_show_child = "Display Children List";
$i_general_BackToTop = "Top";
$i_general_students_selected = "Student(s) Selected";
$i_general_selected_students = "Selected Student(s)";
$i_general_selected_teachers = "Selected Teacher(s)";
$i_general_search_again = "Search Again";
$i_general_view_past = "View Past";
$i_general_english = "English";
$i_general_chinese = "Chinese";
$i_general_yes = "Yes";
$i_general_yes2 = "Yes";
$i_general_no = "No";
$i_general_active = "Active";
$i_general_inactive = "Inactive";
$i_general_main_menu = "Main Menu";
$i_general_logout = "Logout";
$i_general_steps = "Steps";
$i_general_required_field = "<span class='tabletextrequire'>*</span> Mandatory field(s)";
$i_general_display_format = "Display Format";

$i_general_required_field2 = "<span class='tabletextrequire'>*</span> Mandatory field(s)";
$i_general_alternative = "Alternative";
$i_general_expand_all = "Expand All";
$i_general_collapse_all = "Collapse All";
$i_general_report_creation_time="Report Creation Time";
$i_general_last_modified_by="Last Modified By";
$i_general_no_privileges = "You don't have privileges to view.";
$i_general_icon_list = "Icon List";
## Added on 18-7-2007 by Andy
$i_general_all_classes = "All Classes";
$i_general_all_houses = "All Houses";
$i_general_refresh = "Refresh";
$i_general_refreshed = "Refreshed";

$i_general_choose_student = "Choose Student";
$i_general_from_class_group = "from <b>Class</b> / <b>Group</b>";
$i_general_from_class = "from <b>Class</b>";
$i_general_or = "or";
$i_general_search_by_inputformat = "Search by input format<br>\"[Class Name] [Class Number] [Name]\"<br>(e.g. 1a15 Chan) ";
$i_general_search_by_loginid = "Search by Login ID";

$i_general_select_csv_file = "Select CSV file";
$i_general_imported_result = "Imported result";
$i_general_confirm_import_data = "Confirm import data";
$i_general_complete = "Complete";

$i_general_enabled = "Enabled";
$i_general_disabled = "Disabled";
$i_general_orderby = "Order by";
$i_general_target = "Target";
$i_general_class = "Class";
$i_general_orderby_class = "Class and then Class Number";
$i_general_count = "Count";
$i_general_display_details = "Display details";
$i_general_all = "All";
$i_general_no_access_right = "You do not have the access right to access this page.";

# modified by henry on 17 Nov 2008
$i_general_no_of = "No. of ";
$i_general_name = "Name";
$i_general_class = "Class";
$i_general_are_you_sure = "Are you sure?";
$i_general_please_select = "Please select";
$i_general_highest = "Highest";
$i_general_most = "most";
$i_general_every = "Every ";
$i_general_first = "First ";
$i_general_another = "Another ";
$i_general_instance_of = "instance(s) of ";
$i_general_will_be_count_as = "will be count as ";
$i_general_receive = "Receive";
$i_general_show = "Show";
$i_general_loading = "Loading...";
$i_general_timeout_relogin = "Timeout.  Please re-login again.";

# 20090324 yatwoon
$i_general_level = "Level";
$i_general_lowest = "Lowest";
########################
$i_general_na = "N.A.";
$i_general_print_date = "Print Date";

$i_email_to = "To";
$i_email_subject = "Subject";
$i_email_message = "Message";
$i_email_sendemail = "Send email";
$i_From = "From";
$i_To = "To";
$i_LastModified = "Last Modified";
$i_status_approve = "Approve";
$i_status_approved = "Approved";
$i_status_suspend = "Suspend";
$i_status_suspended = "Suspended";
$i_status_pending = "Pending";
$i_status_pendinguser = "Pending";
$i_status_graduate = "Left";
$i_status_publish = "Publish";
$i_status_published = "Published";
$i_status_checkin = "Checked In";
$i_status_checkout = "Checked Out";
$i_status_cancel = "Cancelled";
$i_status_reserved = "Reserved";
$i_status_rejected = "Rejected";
$i_status_all = "All";
$i_status_activated = "Activated";
$i_status_template = "Template";
$i_status_waiting = "Waiting";
$i_invalid_email = "Invalid email address";
$i_invalid_date = "Invalid date";
$i_gender_male = "Male";
$i_gender_female = "Female";
$i_title_mr = "Mr.";
$i_title_miss = "Miss";
$i_title_mrs = "Mrs.";
$i_title_ms = "Ms.";
$i_title_dr = "Dr.";
$i_title_prof = "Prof.";
$i_export_msg1 = "Fields separator";
$i_export_msg2 = "Export specific columns";
$i_import_msg1 = "1. Choose a format to import users from";
$i_import_msg2 = "2. Specify the file to import";
$i_import_msg3 = "Press Import to upload and import the file";
$i_import_msg4 = "What is an eClass .CSV file?";
$i_import_msg5 = "An eClass .CSV file is formatted .CSV file which:";
$i_import_msg6 = "contains all the fields available in the namelist";
$i_import_msg7 = "e.g. user login, password, user email, English name, Chinese name";
$i_import_msg8 = "can be edited in Microsoft Excel";
$i_import_msg9 = "download eclass.csv file";
$i_import_msg10 = "3. Select at least one identity group";
$i_import_msg_note = "Note:<br>
<ul>
<li>'UserLogin' in the import file should contain 'a-z'(lowercases), '0-9' and '_' <b>ONLY</b>, also 'UserLogin' should be started with 'a-z'.
<li>If 'UserLogin' in the import file already exists, all the information of that account will be updated (including email and password)
<li>If you don't want to update Password and Email, please leave the two fields empty in the import file.</ul>";
$i_import_msg_note2 = "Note 2:<br>
<ul><li>To prevent the lost of the leading \"0\", please add \"#\" before every WebSAMS Registration No. ( e.g <b>#</b>0012345 )</ul>";

$i_import_invalid_format = "The uploaded file is not in correct format. Your file should have fields in this order :";
$i_groupimport_msg = "Specify the file to import";
$i_groupimport_type = "Type of new groups";
$i_groupimport_fileformat = "File format";
$i_groupimport_fileformat2 = "The first column of the file should be UserLogin.<br>
The second column is the group name. If that group does not exist, the system will create a new group with that name.";
$i_groupimport_sample = "Click here to download sample file (CSV format)";

$i_import_big5 = "Import by Big-5";
$i_import_gb = "Import by GB";
$i_import_utf = "Import by UTF";

$i_identity = "Identity";
$i_identity_teachstaff = "Teacher/Staff";
$i_identity_student = "Student";
$i_identity_parent = "Parent";
$i_identity_alumni = "Alumni";
$i_identity_array = array("",$i_identity_teachstaff,$i_identity_student,$i_identity_parent,$i_identity_alumni);

$i_import_teacher_data = "Import $i_identity_teachstaff Data";
$i_import_student_data = "Import $i_identity_student Data";
$i_import_parent_data = "Import $i_identity_parent Data";
$i_import_alumni_data = "Import $i_identity_alumni Data";
$i_import_identity_array = array("",$i_import_teacher_data,$i_import_student_data,$i_import_parent_data,$i_import_alumni_data);


$i_teachingStaff = "Teaching Staff";
$i_nonteachingStaff = "Non-teaching Staff";
$i_teachingDifference = "(Note that: Teaching Staff will be added to group Teacher automatically whereas non-teaching staff to Admin Staff group.)";

$i_ifapplicable = "If applicable";
$i_notapplicable = "N/A";

$i_ClassLevel = "Class Level";
$i_ClassLevelName = "Class Level Name";
$i_ClassName = "Class Name";
$i_GroupName = "Group Name";
$i_ClassNumber = "Class Number";
$i_Class_ImportInstruction = "<b><u>File Description:</u></b><br>
1st Column : Class Name (e.g. 1A ).<br>
2nd Column : Level Name (e.g. F.1).<br>";
$i_Class_DownloadSample = "Click here to download sample";

$i_SettingsSchoolName = "School Name";
$i_SettingsOrganization = "Organization";
$i_SettingsSchoolPhone = "School Phone";
$i_SettingsSchoolAddress = "School Address";
$i_SettingsCurrentAcademicYear = "Current School Year";
$i_SettingsSemester = "Semester";
$i_SettingsSchoolDayType = "Operation Type";
$i_SettingsCurrentSemester = "Current";
$i_SettingsSemesterList = "Semester List";
$i_SettingsSchool_Class = "Class Structure Settings";
$i_SettingsSchool_ClassTeacher = "Teaching Appointment Settings";
$i_SettingsSchool_Forms = "School-defined Forms";

### added by ronald 20081118 ###
$i_SettingSemesterMode = "Semester Mode";
$i_SettingSemesterManualUpdate = "Manual Update";
$i_SettingSemesterAutoUpdate = "Auto Update";
$i_SettingSemesterSetting_Alert1 = "Start Date cannot empty.";
$i_SettingSemesterSetting_Alert2 = "End Date cannot empty.";
$i_SettingSemesterSetting_Alert3 = "Date is not valid.";
$i_SettingSemesterSetting_Alert4 = "Semesters' Date is not in correct order. Please check again.";
######

$i_DayTypeWholeDay = "WD";
$i_DayTypeAM = "AM";
$i_DayTypePM = "PM";
$i_DayTypeSession = "Session";
$i_DayTypeArray = array(
"",
$i_DayTypeWholeDay,
$i_DayTypeAM,
$i_DayTypePM);

$i_wordtemplates_select = "Select the type of wordings: ";
$i_wordtemplates_wordings = "Wordings: (separated line by line)";
$i_wordtemplates_attendance = "Reason for lateness/absence/early leave";
$i_wordtemplates_merit = "Reason for merit";
$i_wordtemplates_demerit = "Reason for demerit";
$i_wordtemplates_activity = "Activity Name";
$i_wordtemplates_performance = "Performance";
$i_wordtemplates_service = "Service Name";
$i_wordtemplates_service_role = "Service Position";
$i_wordtemplates_activity_role = "Activity Position";
$i_wordtemplates_award_name = "Award Name";
$i_wordtemplates_award_role = "Award Role";
$i_wordtemplates_type_array = array(
$i_wordtemplates_attendance,
$i_wordtemplates_merit,
$i_wordtemplates_demerit
);
$i_wordtemplates_instruction = "You can edit the preset wordings. Each line is one item. <br>Please click update to save the changes.";

$i_menu_setting = "Settings";
$i_menu_intranet = "Intranet";
$i_menu_eclass = "eClass";
$i_menu_network = "Network";

# menu name
$i_adminmenu_group = "Group";
$i_adminmenu_user = "User";
$i_adminmenu_announcement = "Announcement";
$i_adminmenu_event = "Event";
$i_adminmenu_polling = "Poll";
$i_adminmenu_timetable = "Group Timetable";
$i_adminmenu_resource = "Resources";
$i_adminmenu_booking = "Booking";
$i_adminmenu_staffdirectory = "Staff Directory";
$i_adminmenu_filemgmt = "File Management";
$i_adminmenu_eclass = "eClass";
$i_adminmenu_role = "Role";
$i_adminmenu_usermgmt = "User Mgmt";
$i_adminmenu_infomgmt = "Info Mgmt";
$i_adminmenu_resourcemgmt = "Resources Mgmt";
$i_adminmenu_eclassmgmt = "eClass Mgmt";
$i_adminmenu_referencefiles = "Reference Files";
$i_adminmenu_motd = "Message of the Day";
$i_adminmenu_sysmgmt = "System Mgmt";
$i_adminmenu_email_setting = "Email Settings";
//$i_adminmenu_admin_account = "Admin Account";
$i_adminmenu_rbps = "Resources Booking Period Settings";

$i_admintitle_group = "Group";
$i_admintitle_user = "User";
$i_admintitle_announcement = "Announcement";
$i_admintitle_event = "Event";
$i_admintitle_polling = "Poll";
$i_admintitle_timetable = "Timetable";
$i_admintitle_resource = "Resources";
$i_admintitle_tmpfiles = "Temporary files";
$i_admintitle_booking = "Booking";
$i_admintitle_staffdirectory = "Staff Directory";
$i_admintitle_filemgmt = "File Management";
$i_admintitle_eclass = "eClass";
$i_admintitle_role = "Role";
$i_admintitle_usermgmt = "User Mgmt";
$i_admintitle_infomgmt = "Info Mgmt";
$i_admintitle_resourcemgmt = "Resources Mgmt";
$i_admintitle_eclassmgmt = "eClass Mgmt";
$i_admintitle_referencefiles = "Reference Files";
$i_admintitle_motd = "Message of the Day";
$i_admintitle_sysmgmt = "System Mgmt";
$i_admintitle_email_setting = "Email Settings";
$i_admintitle_admin_account = "Admin Account";
$i_admintitle_rbps = "Resources Booking Period Settings";
$i_admintitle_subjects = "Homework Subjects";

$i_adminmenu_sa = "System Admin";
$i_adminmenu_sa_email = "Email Settings";
$i_adminmenu_sa_password = "Change Password";
$i_adminmenu_sa_helper = "System Helpers";
$i_adminmenu_sa_pw_leave_blank = "leave it blank for no password change";
$i_adminmenu_sc = "System Settings";
$i_adminmenu_sc_globalsetting = "Global Settings";
$i_adminmenu_sc_resourcebooking = "Resources Booking";
$i_adminmenu_sc_campuslink = "Campus Links";
$i_adminmenu_sc_basic_settings = "Basic Settings";
$i_adminmenu_sc_school_settings = "School Settings";
$i_adminmenu_sc_group_settings = "Group Settings";
$i_adminmenu_sc_user_info_settings = "User Info Settings";
$i_adminmenu_sc_clubs_enrollment_settings = "eEnrolment Settings";
$i_adminmenu_sc_campustv = "Campus TV";
$i_adminmenu_sc_url = "Intranet URL";
$i_adminmenu_sc_cycle = "Cycle Day";
$i_adminmenu_sc_period = "School Timetable";
$i_adminmenu_sc_language = "Default Language";
$i_adminmenu_sc_webmail = "Webmail";
$i_adminmenu_sc_campusmail = "Campus Mail";
$i_adminmenu_sc_homework = "Homework List";
$i_adminmenu_sc_campusquota = "Campus mail quota";
$i_adminmenu_sc_wordtemplates = "Preset Wordings";
$i_adminmenu_fs = "Function Settings";
$i_adminmenu_fs_homework = "Homework List Settings";
$i_adminmenu_fs_campusmail = "Campus Mail Settings";
$i_adminmenu_fs_resource_set = "Resources Booking Settings";
$i_adminmenu_sf = "System Files";
$i_adminmenu_plugin = "System Integration";
$i_adminmenu_plugin_sls_library = "SLS Library";
$i_adminmenu_plugin_qb = "Central Question Bank";
$i_adminmenu_am = "Account Mgmt";
$i_adminmenu_am_user = "User Mgmt Centre";
$i_adminmenu_am_usage = "Login Records";
//$i_adminmenu_am_group = "Groups";
$i_adminmenu_am_role = "Roles";
$i_adminmenu_adm = "Admin Mgmt";
$i_adminmenu_adm_academic_record = "Student Profile";
$i_adminmenu_adm_teaching = "Teaching Appointment Settings";
$i_adminmenu_adm_adminjob = "Admin Job Appointment Settings";
$i_adminmenu_adm_enrollment_approval = "Club Enrolment Mgmt";
$i_adminmenu_adm_enrollment_group = "Group Enrolment Status";
//$i_adminmenu_adm_enrollment_student = "Student Enrolment Status";
$i_adminmenu_im = "Information Mgmt";
$i_adminmenu_im_motd = "Message of the Day";
$i_adminmenu_im_announcement = "School News";
$i_adminmenu_im_event = "School Calendar";
$i_adminmenu_im_timetable = "Group Timetables";
$i_adminmenu_im_campusmail = "School Circulars";
$i_adminmenu_im_polling = "Poll";
$i_adminmenu_im_group_bulletin = "Group Bulletins";
$i_adminmenu_im_group_files = "Group Files";
$i_adminmenu_im_group_links = "Group Links";
$i_adminmenu_gm = "Group Mgmt";
$i_adminmenu_gm_group = "Group Mgmt Centre";
$i_adminmenu_gm_groupfunction = "Group Function Mgmt";
$i_adminmenu_rm = "Resources Mgmt";
$i_adminmenu_rm_item = "Item List";
$i_adminmenu_rm_record = "Booking Records";
$i_adminmenu_rm_record_periodic = "Periodic Booking Records";
$i_adminmenu_us = "Usage Statistics";
$i_adminmenu_us_user = "User";
$i_adminmenu_us_group = "Group";
$i_adminmenu_basic_settings_misc = "Other settings";
$i_adminmenu_basic_settings_email = "Users CANNOT change their email addresses.";
$i_adminmenu_basic_settings_title = "DO NOT display Title for $i_identity_teachstaff and $i_identity_parent.";
$i_adminmenu_basic_settings_home_tel = "Users CANNOT change their home telephone.";
$i_adminmenu_basic_settings_badge = "School Badge";
$i_adminmenu_basic_settings_badge_instruction = "The image must be in 'JPG', 'GIF' or 'PNG' format and the size should be fixed to 120 x 60 pixel (W x H).";
$i_adminmenu_basic_settings_badge_use = "Display School Badge";

$i_adminmenu_user_info_settings_disable_1 = "Teachers CANNOT change their:";
$i_adminmenu_user_info_settings_disable_2 = "Students CANNOT change their:";
$i_adminmenu_user_info_settings_disable_3 = "Parents CANNOT change their:";

$i_adminmenu_basic_settings_title_short = "DO NOT display Title for";

$i_adminmenu_user_info_settings_display = "Display";
$i_adminmenu_user_info_settings_display_disable_1 = "Teachers DO NOT display:";
$i_adminmenu_user_info_settings_display_disable_2 = "Students DO NOT display:";
$i_adminmenu_user_info_settings_display_disable_3 = "Parents DO NOT display:";


$i_admintitle_sa = "System Admin";
$i_admintitle_sa_email = "Email Settings";
$i_admintitle_sa_password = "Change Password";
$i_admintitle_sa_helper = "System Helpers";
$i_admintitle_sc = "System Settings";
$i_admintitle_sc_globalsetting = "Global Settings";
$i_admintitle_sc_resourcebooking = "Resources Booking";
$i_admintitle_sc_campuslink = "Campus Links";
$i_admintitle_sc_url = "Intranet URL";
$i_admintitle_sc_homework = "Homework List";
$i_admintitle_sc_subject = "Subjects List";
$i_admintitle_sc_cycle = "Cycle Day";
$i_admintitle_sc_period = "School Timetable";
$i_admintitle_sc_language = "Default Language";
$i_admintitle_sc_webmail = "Webmail";
$i_admintitle_sc_campusmail = "Campus Mail";
$i_admintitle_sc_campusquota = "Campus Mail Quota";
$i_admintitle_fs = "Function Settings";
$i_admintitle_fs_homework = "Homework List Settings";
$i_admintitle_fs_homework_subject = "Subject Settings";
$i_admintitle_fs_official_subject = "Add-On Subject Settings";
$i_admintitle_fs_campusmail = "Campus Mail Settings";
$i_admintitle_fs_resource_set = "Resources Booking Settings";
$i_admintitle_sf = "System Files";
$i_admintitle_am = "Account Mgmt";
$i_admintitle_am_user = "User Mgmt Centre";
$i_admintitle_am_group = "Groups";
$i_admintitle_am_role = "Roles";
$i_admintitle_im = "Information Mgmt";
$i_admintitle_im_motd = "Message of the Day";
$i_admintitle_im_announcement = "School News";
$i_admintitle_im_event = "School Calendar";
$i_admintitle_im_timetable = "Group Timetables";
$i_admintitle_im_campusmail = "School Circulars";
$i_admintitle_im_campusmail_outbox = "History";
$i_admintitle_im_campusmail_compose = "Issue Notice";
$i_admintitle_im_campusmail_template = "Draft";
$i_admintitle_im_campusmail_trash = "Trash";
$i_admintitle_im_polling = "Poll";
$i_admintitle_im_group_bulletin = "Group Bulletins";
$i_admintitle_im_group_files = "Group Files";
$i_admintitle_im_group_links = "Group Links";
$i_admintitle_rm = "Resources Mgmt";
$i_admintitle_rm_item = "Item List";
$i_admintitle_rm_record = "Booking Records";
$i_admintitle_rm_per_record = "Periodic Booking Records";
$i_admintitle_us = "Usage Statistics";
$i_admintitle_us_user = "User";
$i_admintitle_us_group = "Group";
$i_admintitle_im_photoalbum = "Photo Album";

//$i_admin_status_1 = "PROCESSING...";
//$i_admin_status_2 = "Please Wait!";

# List Value
$list_total = "Total";
$list_prev = "Prev";
$list_next = "Next";
$list_page = "Page";
$list_sortby = "Sort By";
$list_desc = "Descending";
$list_asc = "Ascending";

# Button Name
$button_add = "Add";
$button_attach = "Attach";
$button_approve = "Approve";
$button_archive = "Archive";
$button_archive_rb = "Archive Past Records";
$button_archive1 = "Archive";
$button_activate = "Activate";
$button_back = "Back";
$button_cancel = "Cancel";
$button_change = "Change";
$button_check_all = "Check All";
$button_checkin = "Check In";
$button_checkout = "Check Out";
$button_clear = "Clear";
$button_clear_all = "Clear All";
$button_close = "Close Window";
$button_continue = "Continue";
$button_copy = "Copy";
$button_deactivate = "Deactivate";
$button_edit = "Edit";
$button_email = "Email";
$button_erase = "Erase";
$button_export = "Export";
$button_export_all = "Export All";
$button_export_pps = "Export PPS Transaction Cost";
$button_export_xml = "Export XML";
$button_find = "Search";
$button_finish = "Finish";
$button_image = "Image";
$button_import = "Import";
$button_import_xml = "Import XML";
$button_move = "Move";
$button_moveto = "Move To";
$button_new = "New";
$button_new_group = "New Group";
$button_new_album = "New Album";
$button_newfolder = "New Folder";
$button_next = "Next";
$button_next_page = "Next Page";
$button_pay = "Pay";
$button_pending = "Pending";
$button_preview = "Preview";
$button_save_preview = "Save / Save and Preview";
$button_previous_page = "Previous Page";
$button_quickadd = "Quick Add";
$button_quicksearch = "Search";
$button_delete = "Delete";
$button_remove_selected = "Remove Selected";
$button_remove_selected_student = "Remove Selected Student(s)";
$button_skip = "Skip";
$button_add_attachment = "Add Attachment";
$button_remove_all = "Delete ALL";
$button_rename = "Rename";
$button_reject = "Reject";
$button_unlock = "Unlock";
$button_lock = "Lock";
$button_release = "Release";
$button_unrelease = "Unrelease";
$button_waive = "Waive";
$button_unwaive = "Unwaive";
$button_reset = "Reset";
$button_reserve = "Reserve";
$button_restore = "Restore";
$button_save = "Save";
$button_save_as = "Save AS";
$button_save_as_new = "Save As New";
$button_save_draft = "Save Draft";
$button_save_as_draft = "Save As Draft";
$button_save_continue = "Save and Continue";
$button_submit_continue = "Submit and Continue";
$button_submit_preview = "Submit and Preview";
$button_search = "Search";
$button_select = "Select";
$button_select_situation = "Select Situation";
$button_select_template = "Select Template";
$button_send = "Send";
$button_submit = "Submit";
$button_suspend = "Suspend";
$button_swap = "Swap";
$button_use_default = "Use Default";
$button_unzip = "Unzip";
$button_update = "Update";
$button_upload = "Upload";
$button_view = "View";
$button_emailpassword = "Send password to users";
$button_select_all = "Select ALL";
$button_undo = "Undo";
$button_updateperformance = "Update Performance";
$button_print = "Print";
# 20090227 yat woon
$button_print_all = "Print All";
$button_print_selected = "Print Selected";

$button_quit = "Quit";
$button_assignsubjectleader = "Assign Subject Leader";
$button_confirm = "Confirm";
$button_emptytrash = "Empty Trash";
$button_set = "Set";
$button_more_file = "Add more files";
$button_discard = "Discard";
$button_hide = "Hide";
$button_Add_Members_Now = "Add Members Now";
$button_Back_To_Group_List = "Back to Group List";
# added on 31 Jan 2008
$button_remove = "Remove";
$button_notify = "Notify";
########################
$button_view_template = "View Template";
$button_view_statistics = "View Statistics";
$button_hide_statistics = "Hide Statistics";
$button_print_statistics = "Print Statistics";

$button_previous_week = "Previous Week";
$button_next_week = "Next Week";

# General Confirmation Message
$i_con_gen_msg_add = "Record added.";
$i_con_gen_msg_update = "Record updated.";
$i_con_gen_msg_delete = "Record deleted.";
$i_con_gen_msg_email = "Email sent.";
$i_con_gen_msg_email_remove = "Email deleted.";
$i_con_gen_msg_email_restore = "Email restored.";
$i_con_gen_msg_email_save = "Email saved.";
$i_con_gen_msg_password1 = "Password changed.";
$i_con_gen_msg_password2 = "Invalid password.";
$i_con_gen_msg_password3 = "New Password Cannot be the same as Old Password.";
$i_con_gen_msg_email1 = "Email address changed.";
$i_con_gen_msg_email2 = "Email address cannot be changed. Possible reason: email address already exists.";
$i_con_gen_msg_photo_delete = "The Photo has been deleted.";


# Confirmation Message
$i_con_msg_add = "<font color=green>Record Added.</font>\n";
$i_con_msg_update = "<font color=green>Record Updated.</font>\n";
$i_con_msg_update_failed = "<font color=red>Record Update Failed.</font>\n";
$i_con_msg_delete = "<font color=green>Record Deleted.</font>\n";
$i_con_msg_delete_failed = "<font color=red>Record Delete Failed.</font>\n";
$i_con_msg_email = "<font color=green>Email Sent.</font>\n";
$i_con_msg_password1 = "<font color=green>Password changed.</font>";
$i_con_msg_password2 = "<font color=red>Invalid password.</font>";
$i_con_msg_password3 =" <font color=red>New Password Cannot be the same as Old Password.</font>";
$i_con_msg_email1 = "<font color=green>Email address changed.</font>";
$i_con_msg_email2 = "<font color=red>Email address cannot be changed. Possible reason: email address already exists.</font>";
$i_con_msg_archive = "<font color=green>Groups archived.</font>";
$i_con_msg_import_success = "<font color=green>Import success.</font>";
$i_con_msg_date_wrong = "<font color=red>Invalid date.</font>";
$i_con_msg_admin_modified = "<font color=green>Group Admin Updated.</font>";
$i_con_msg_suspend = "<font color=red>Record Suspended.</font>\n";
$i_con_msg_cannot_edit_poll = "<font color=red>You cannot edit a started poll.</font>\n";
$i_con_msg_photo_delete = "<font color=green>The Photo has been deleted.</font>\n";
$i_con_msg_import_failed = "<font color=red>Invalid file format.</font>";
$i_con_msg_import_header_failed = "<font color=red>Incorrect header format.</font>";
$i_con_msg_grade_generated = "<font color=green>Grade generated</font>";
$i_con_msg_import_failed2 = "<font color=red>Import file failed.</font>";
$i_con_msg_import_invalid_login="<font color=red>Invalid UserLogin.</font>";
$i_con_msg_user_add_failed = "<font color=red>Add user failed. (UserLogin and UserEmail in wrong format or already existing)</font>";
$i_con_msg_date_startend_wrong_alert = "Invalid date. Start date must be earlier than End date.";
$i_con_msg_date_startend_wrong = "<font color=red>$i_con_msg_date_startend_wrong_alert</font>";
$i_con_msg_date_start_wrong_alert = "Invalid date. Start date must not be earlier than Today.";
$i_con_msg_date_start_wrong = "<font color=red>$i_con_msg_date_start_wrong_alert</font>";
$i_con_msg_add_form_failed = "<font color=red>Failed. Form name must be unique.</font>";
$i_con_msg_delete_part = "<font color=red>Some records deleted. (Only non-default categories without any groups can be deleted)</font>";
$i_con_msg_enrollment_next = "<font color=green>Students can enter into next round selection now. (You may need to change the application period for next round).</font>";
$i_con_msg_enrollment_confirm = "<font color=green>Students approved are added to corresponding group.</font>";
$i_con_msg_enrollment_lottery_completed = "<font color=green>Lottery is performed.</font>";
$i_con_msg_subject_leader_updated = "<font color=green>Subject Leader Updated.</font>";
$i_con_msg_add_failed = "<font color=red>Failed to add record.</font>";
$i_con_msg_sports_house_group_existed = "<font color=red>This House Group has been Selected already.</font>";
$i_con_msg_voted = "<font color=green>Voted success.</font>";
$i_con_msg_delete_following_failed = "<font color=red>Failed To Delete Following Records.</font>\n";
$i_con_msg_copy = "<font color=green>Copy completed.</font>";
$i_con_msg_copy_failed_dup_name = "<font color=red>Copy failed, Reason: template name is in use.</font>";
$i_con_msg_copy_failed = "<font color=red>Copy failed.</font>";
$i_con_msg_activate = "<font color=green>Record Activated.</font>";
$i_con_msg_quota_exceeded = "<font color=red>Quota Exceeded.</font>";
$i_con_msg_notification_sent = "<font color=green>Notifications are sent to students successfully.</font>";
$i_con_msg_wrong_header = "<font color=red>Wrong Header.</font>";
$i_con_msg_import_success_with_update = "<font color='green'>Import successful on new records and existing records will be updated accordingly</font>";
$i_con_msg_sports_ranking_generated = "<font color=green>Record and ranking are updated.</font>";
$i_con_msg_import_no_record = "<font color=red>The file has no record.</font>";
$i_con_msg_duplicate_item = "<font color=red>This item name has been used before. Please enter a new one.</font>";
$i_con_msg_duplicate_category = "<font color=red>This category name has been used before. Please enter a new one.</font>";

# for club enrolment (old version) 20080924 yat woon
$i_con_msg_enrollment_club_quota_exceeded = "<font color=red>Club Quota Exceeded.</font>";
$i_con_msg_enrollment_no_student = "<font color=red>No student need approve.</font>";
$i_con_msg_enrollment_some_already_exists_club = "<font color=red>Some students already joined higher priority club.</font>";
$i_con_msg_enrollment_club_reject_success = "<font color=green>Student(s) rejected.</font>";


# eDiscipline v1.2 20081105
$i_con_msg_add_overlapped = "<font color=red>Session Overlapped. Record Add Failed.</font>\n";
$i_con_msg_update_overlapped = "<font color=red>Session Overlapped. Record Update Failed.</font>\n";
$i_con_msg_add_past = "<font color=red>Session is past. Record Add Failed.</font>\n";
$i_con_msg_update_past = "<font color=red>Session is past. Record Update Failed.</font>\n";
$i_con_msg_add_past_overlapped = "<font color=red>Session past or overlapped. Some records cannot be added.</font>\n";
$i_con_msg_session_not_available = "<font color=red>Session not available</font>\n";
$i_con_msg_session_not_available2 = "<font color=red>Session not available</font>\n";
$i_con_msg_session_past = "<font color=red>Session past</font>\n";
$i_con_msg_session_full = "<font color=red>Session full</font>\n";
$i_con_msg_session_assigned_before = "<font color=red>Student Assigned in Session Before</font>\n";
$i_con_msg_update_inapplicable_form = "<font color=red>Inapplicable Form. Record Update Failed.</font>\n";
$i_con_msg_update_already_in_session = "<font color=red>Student Assigned in Session Before. Record Update Failed.</font>\n";
$i_con_msg_update_not_enough_session = "<font color=red>Session Not Enough. Record Update Failed.</font>\n";
$i_con_msg_update_incompatible_form = "<font color=red>Incompatible Form. Record Update Failed.</font>\n";
$i_con_msg_update_incompatible_vacancy = "<font color=red>Incompatible Vacancy. Record Update Failed.</font>\n";
$i_con_msg_cancel = "<font color=green>Cancel Arrangement Success.</font>\n";
$i_con_msg_cancel_failed = "<font color=red>Cancel Arrangement Fail.</font>\n";
$i_con_msg_cancel_past = "<font color=red>Session is past. Arrangement cannot be cancelled.</font>\n";
$i_con_msg_approve = "<font color=green>Record Approved.</font>\n";
$i_con_msg_reject = "<font color=green>Record Rejected.</font>\n";
$i_con_msg_release = "<font color=green>Record Released.</font>\n";
$i_con_msg_unrelease = "<font color=green>Record Unreleased.</font>\n";
$i_con_msg_waive = "<font color=green>Record Waived.</font>\n";
$i_con_msg_unwaive = "<font color=green>Record Unwaived.</font>\n";
$i_con_msg_no_delete_acc_record = "<font color=green>Some records cannot be deleted.</font>\n";
$i_con_msg_no_waive_acc_record = "<font color=green>Some records cannot be waived.</font>\n";
$i_con_msg_no_unwaive_acc_record = "<font color=green>Record cannot be unwaived.</font>\n";
$i_con_msg_no_student_select = "<font color=red>No student is selected.</font>\n";
$i_con_msg_duplicate_students = "<font color=red>Student(s) is/are duplicated.</font>\n";
$i_con_msg_not_all_approve = "<font color=red>Only Some records are approved.</font>\n";
$i_con_msg_no_record_approve = "<font color=red>No record is approved.</font>\n";
$i_con_msg_not_all_reject = "<font color=red>Only Some records are rejected.</font>\n";
$i_con_msg_no_record_reject = "<font color=red>No record is rejected.</font>\n";
$i_con_msg_not_all_release = "<font color=red>Only Some records are released.</font>\n";
$i_con_msg_no_record_release = "<font color=red>No record is released.</font>\n";
$i_con_msg_not_all_unrelease = "<font color=red>Only Some records are unreleased.</font>\n";
$i_con_msg_no_record_unrelease = "<font color=red>No record is unreleased.</font>\n";

## added on 15 Jan 2009 by Ivan
$i_con_msg_redeem_success = "<font color=green>Redemption Success</font>\n";
$i_con_msg_redeem_failed = "<font color=red>Redemption Failed.</font>\n";
$i_con_msg_cancel_redeem_success = "<font color=green>Cancel Redemption Success</font>\n";
$i_con_msg_cancel_redeem_failed = "<font color=red>Cancel Redemption Failed</font>\n";

# For class history promotion February 2009 By Sandy
$i_con_msg_record_not_found[0] = "<font color=red>Record not found</font>";
$i_con_msg_record_not_found[1] = "<font color=red>Duplicate/Problematic record not found. <BR />Record may have been corrected in another session</font>";
$i_con_msg_promo_not_deleted = "<font color=red>No records were deleted.</font>";

# Functions
$i_AlbumName = "Album Name";
$i_AlbumDescription = "Description";
$i_AlbumNumberOfItems = "Number of Items";
$i_AlbumDateModified = "Last Modified";
$i_AlbumDisplayOrder = "Display Order";
$i_AlbumAccessType = "Access Type";
$i_AlbumIntranet = "All intranet users";
$i_AlbumInternet = "All internet users";
$i_AlbumSelectedGroupsUsers = "Selected groups and users";
$i_AlbumInternalGroup = "Internal Group";
$i_AlbumUserGroup = "User";
$i_AlbumQuota = "Quota";
$i_AlbumPhotoFile = "File";
$i_AlbumPhotoDescription = "Description";
$i_AnnouncementTitle = "Title";
$i_AnnouncementDescription = "Description";
$i_AnnouncementDate = "Start Date";
$i_AnnouncementEndDate = "End Date";
$i_AnnouncementRecordType = "Type";
$i_AnnouncementRecordStatus = "Status";
$i_AnnouncementDateInput = "Input Date";
$i_AnnouncementDateModified = "Last Modified";
$i_AnnouncementAlert = "Send email to users";
$i_AnnouncementAttachment = "Attachment";
$i_AnnouncementCurrAttachment = "Current Attachment";
$i_AnnouncementNewAttachment = "More Attachments";
$i_AnnouncementNoAttachment = "No Attachment";
$i_AnnouncementDelete = "Delete?";
$i_AnnouncementSchool = "School Announcement";
$i_AnnouncementAlert_SMS = "Send SMS to users";
$i_AnnouncementAdminRight = "You cannot edit or delete items created by system administrator(s).";
$i_AnnouncementPublic = "Display on Login page";
$i_AnnouncementOwner = "Posted By";
$i_AnnouncementByGroup = "From Group";
$i_AnnouncementSystemAdmin = $i_general_sysadmin;
$i_AnnouncementNoAnnouncer = "Unknown (Old Post)";
$i_AnnouncementPublicInstruction = "If you want this announcement to be shown to whole school, please leave the Target Group(s) empty.";
$i_AnnouncementPublic = "Public Announcement";
$i_AnnouncementGroup = "My Group Announcement";
$i_AnnouncementDisplayDate = "Display Date";
$i_AnnouncementWholeSchool = "Whole School";
$i_AnnouncementTargetGroup = "Target Group(s)";
$i_AnnouncementReadCount = "Read";
$i_AnnouncementUnreadCount = "Unread";
$i_AnnouncementReadList = "Read List";
$i_AnnouncementUnreadList = "Unread List";
$i_AnnouncementViewReadStatus = "View Read Status";
$i_AnnouncementViewReadList = "View Read List";
$i_AnnouncementViewUnreadList = "View Unread List";
$i_All_MyGroup = "All Groups";


$i_BookingDateStart = "Date";
$i_BookingDateEnd = "Time";
$i_BookingRemark = "Remarks";
$i_BookingRecordType = "Type";
$i_BookingRecordStatus = "Status";
$i_BookingDateInput = "Input Date";
$i_BookingDateModified = "Last Modified";
$i_BookingArterisk = "<font color=red>*</font>";
$i_BookingLegend = " - represents the record is pending for approval.";
$i_BookingListAll = "View all reserved records";
$i_BookingMyRecords = "My Record";
$i_BookingNew = "New Booking";
$i_BookingSingle = "Single";
$i_BookingPeriodic = "Periodic";
$i_BookingAddStep = array("");
$i_BookingAddStep[1] = "Click on the category of the desired item.";
$i_BookingAddStep[2] = "Choose the booking frequency: single or periodic.";
$i_BookingAddStep[3] = "Fill in the necessary details.";
$i_BookingAddRule = "To make booking, follow the steps:";
$i_BookingAddRule .= "<br>\n1. ".$i_BookingAddStep[1];
$i_BookingAddRule .= "<br>\n2. ".$i_BookingAddStep[2];
$i_BookingAddRule .= "<br>\n3. ".$i_BookingAddStep[3];
$i_BookingNewSingle = "New Single Booking";
$i_BookingEditSingle = "Edit Single Booking";
$i_BookingNewPeriodic = "New Periodic Booking";
$i_BookingEditPeriodic = "Edit Periodic Booking";
$i_BookingItem = "Item";
$i_BookingDate = "Date";
$i_BookingTimeSlots = "Time Slots";
$i_BookingApplied = "Applied On";
$i_BookingRetrieveTimeSlots = "View Time slots";
$i_BookingStatus = "Status";
$i_BookingStartDate = "Start Date";
$i_BookingEndDate = "End Date";
$i_BookingPeriodicType = "Type";
$i_BookingNotes = "Reminder";
$i_BookingReserved = "Unavailable - Reserved by";
$i_BookingAvailable = "Available";
$i_BookingUnavailable = "Unavailable";
$i_BookingAppliedByU = "Your original choice.";
$i_BookingPending = "Pending";
$i_BookingSingleBooking = "Single Booking";
$i_BookingPeriodicBooking = "Periodic Booking";
$i_BookingPeriodicDates = "Reserved Dates";
$i_BookingStatusArray = array(
"Pending",
"Cancelled",
"Checked in",
"Checked out",
"Approved",
"Rejected"
);
$i_BookingStatusImageArray = array(
"Pending",
"Cancelled",
"Checked in",
"Checked out",
"Approved",
"Rejected"
);
$i_BookingType_EveryDay = "Every Day";
$i_BookingType_EverySeparate = "Every";
$i_BookingType_Days = "Days";
$i_BookingType_Every = "Every";
$i_BookingType_Weekdays = "Weekday";
$i_BookingType_Cycleday = "Cycle Day";
$i_BookingType_PleaseSelect = "Please select";
$i_BookingPeriodicConfirm = "Periodic Booking Confirmation";
$i_BookingPeriodic_confirmMsg1 = "Booking Summary :";
$i_BookingPeriodic_confirmMsg2 = "Availability of the item based on your given details:";
$i_BookingPeriodic_confirmMsg3 = "Your bookings may not be all approved. Do you want to proceed?";
$i_BookingPeriodic_NoMatch = "There is no available dates matching your selection.";
$i_Booking_TooLate1 = "You need to apply for this item ";
$i_Booking_TooLate2 = "Days before.";
$i_Booking_TodayOrLater = "You can apply bookings only on or after the current date.";
$i_Booking_EndDateWrong = "Please do not set End Date before Start Date.";
$i_Booking_sepdayswrong = "No. of days must be a positive integer";
$i_Booking_PlsWeekDays = "Please select a weekday.";
$i_Booking_PlsCycleDays = "Please select a cycle day.";
$i_BookingWrongPeriodicDateEdit = "Start Date must be ";
$i_BookingWrongPeriodicDateEdit2 = "or later.";
$i_BookingWrongPeriodicEndDate = "End date must be ";
$i_BookingWrongPeriodicEndDate2 = "or earlier.";
$i_Booking_indexTitle = "<img border=0 src=/images/index/resources_glass.gif>";
$i_Booking_Period = "Period";
$i_Booking_filter = "Filter By";
$i_Booking_ViewSchool = "Normal Timeslots";
$i_Booking_ViewOther = "Special Timeslots";
$i_Booking_Weekday = array (
"<img src=/images/resourcesbooking/week_sun.gif>",
"<img src=/images/resourcesbooking/week_mon.gif>",
"<img src=/images/resourcesbooking/week_tue.gif>",
"<img src=/images/resourcesbooking/week_wed.gif>",
"<img src=/images/resourcesbooking/week_thu.gif>",
"<img src=/images/resourcesbooking/week_fri.gif>",
"<img src=/images/resourcesbooking/week_sat.gif>"
);
$i_Booking_Year = "Year";
$i_Booking_BookingRecords = "Booking Records";
$i_Booking_Time = "Time";
$i_Booking_ReservedBy = "Reserved By";
$i_Booking_ClearReject = "Clear Rejected Items";
$i_Booking_Signal = array("",
"<font color=green>Record added</font>",
"<font color=red>The Time Slot(s) is/are reserved</font>",
"<font color=green>Record updated</font>",
"<font color=red>You cannot edit others' record</font>",
"<font color=green>Record removed</font>",
"<font color=red>No matched days for your criteria</font>",
"<font color=red>Not valid date</font>",
"<font color=green>Record cleared</font>"
);
$i_Booking_ArchiveConfirm = "Are you sure to archive?";
$i_Booking_ViewArchive = "View archived records";
$i_Booking_ArchiveUser = "User";
$i_Booking_ArchiveLastAction = "Last Modified";
$i_Booking_ArchiveBooking = "Archived Resources Booking Record";
$i_Booking_DateSelectOr = "or pick from ";
$i_BookingLegendFull = "Fully occupied";
$i_BookingLegendHalf = "Some slots available";
$i_BookingLegendFree = "All available";
$i_BookingItemList = "/images/resourcesbooking/itemlist.gif";
$i_BookingBanList = "Restricted User List";
$i_BookingBanList_Instruction = "Input the user login of restricted users. <br>ONE login name per line. <br>.These Users cannot make new booking records, but existing records will not be affected.<br>";

$i_EmailWebsite = "Website";
$i_EmailWebmaster = "Webmaster";

$i_AdminLogin = "User Name";
$i_AdminPassword = "Password";
$i_AdminRights = "Function Access Rights";

$i_RbpsTitle = "Period Title";
$i_RbpsTimeStart = "Start Time";
$i_RbpsTimeEnd = "End Time";

$i_EventTitle = "Title";
$i_EventDescription = "Description";
$i_EventDate = "Event Date";
$i_EventVenue = "Venue";
$i_EventNature = "Nature";
$i_EventRecordType = "Type";
$i_EventRecordStatus = "Status";
$i_EventDateInput = "Input Date";
$i_EventDateModified = "Last Modified";
$i_EventTypeSchool = "School";
$i_EventTypeAcademic = "Academic";
$i_EventTypeHoliday = "Holiday";
$i_EventTypeGroup = "Group";
$i_EventSkipCycle = "DO NOT count cycle day";
$i_EventImport_msg = "Specify the file to import";
$i_EventImport_type = "Select event type";
$i_EventImport_fileformat = "File format";
$i_EventImport_fileformat2 = "";
$i_EventImport_sample = "Click here to download a sample file (CSV format).";
$i_EventImport_notes = "Event Date must be in YYYY-MM-DD or YYYY/MM/DD format. Set this date format in Excel before saving as CSV file.";
$i_Events = "Events";
$i_EventTypeString = array (
"School Events",
"Academic Events",
"Holidays",
"Group Events"
);
$i_EventAdminRight = "You cannot edit or delete items created by system administrator(s).";
$i_EventList = "<img src=$image_path/index/btn_eventlist_en.gif border=0>";
$i_EventShowAll = "<img border=0 src=$image_path/index/el_btn_allevents_en.gif width=76 height=22>";
$i_EventCloseImage = "<img border=0 src=$image_path/index/el_btn_close.gif width=14 height=14>";
$i_EventAll = "All $i_Events";
$i_EventUpcoming = "Upcoming $i_Events";
$i_EventPast = "Past $i_Events";
$i_EventPoster = "Posted By";
$i_EventPublicInstruction = "If this is an event for the whole school, please leave the Target Group(s) empty.";
$i_EventAcademicYear = "Academic Year";

$i_RecordDate = "Record Date";

$i_GroupTitle = "Title";
$i_GroupDescription = "Description";
$i_GroupRecordType = "Type";
$i_GroupRecordStatus = "Status";
$i_GroupDateInput = "Input Date";
$i_GroupDateModified = "Last Modified";
$i_GroupURL = "URL";
$i_GroupSetAdmin = "Assign admin";
$i_GroupUnsetAdmin = "Cancel admin";
$i_GroupAssignAdminRight = "Assign Group Admin Rights";
$i_GroupAssignAdminRight_SetAdminRightFor = "Set Admin Rights for";
$i_GroupAssignAdminRight_AvailableAdminRights = "Available Admin Rights";
$i_GroupAssignAdminRight_AllRights = "All Rights";
$i_GroupKey = "Represents group administrator.";
$i_GroupQuota = "Storage quota (MBytes)";
$i_GroupQuotaIsInt = "Storage quota must be positive integer.";
$i_GroupStorageUsage1 = "Using";
$i_GroupStorageUsage2 = "(KB)";
$i_GroupSettingsBasicInfo = "Basic Information";
$i_GroupSettingsMemberList = "Member List";
$i_GroupSettingsAnnouncement = "Announcement";
$i_GroupSettingsEvent = "Event";
$i_GroupSettingsSurvey = "Group Survey";
$i_GroupSettingsAlbum = "PhotoAlbum";

$i_GroupSettingTabs = array(
$i_GroupSettingsBasicInfo,
$i_GroupSettingsMemberList,
$i_GroupSettingsAnnouncement,
$i_GroupSettingsEvent,
$i_GroupSettingsAlbum);

$i_GroupAdminRightInternalAnnouncement = "Internal Announcements";
$i_GroupAdminRightAllAnnouncement = "All Announcements (including public and internal)";
$i_GroupAdminRightInternalEvent = "Internal Events";
$i_GroupAdminRightAllEvent = "All Events (including public and internal)";
$i_GroupAdminRightTimetable = "Timetable";
$i_GroupAdminRightBulletin = "Bulletin";
$i_GroupAdminRightLinks = "Shared Links";
$i_GroupAdminRightFiles = "Shared Files";
$i_GroupAdminRightQB = "Question Bank Management";
$i_GroupAdminRightInternalSurvey = "Internal Survey";
$i_GroupAdminRightAllSurvey = "All Survey";

$i_GroupPageDescription = "This page shows all groups relevant to you. To exchange messages and files within a group, click the corresponding icon(s).";
$i_GroupMemberImport = "Click $button_import for adding a member.";
$i_GroupMemberNewRole = "Click $button_new $i_admintitle_role for adding a new $i_admintitle_role.";
$i_GroupMemberFunction = "To change a member's $i_admintitle_role, select the member and the desired $i_admintitle_role, then click Update Role.";
$i_GroupSettingDescription = "Information below will be displayed on School Organization page.";
$i_GroupAnnounceRight = "Rights to post public Announcements/Events/Surveys";
$i_GroupPublicToSchool = "Make as school announcement";
$i_GroupUseAllTools = "Allowing using all group tools";
$i_GroupToolsAllowed = "Rights to use group tools";
$i_ClubToolsAllowed = "Rights to use club tools";
$i_GroupOnlyAcademicAllowed = "For Academic Groups Only";
$i_GroupAdminRights = array(
"Basic Info",
"Member list",
"Group Internal Announcement Only",
"Group All Announcement (Public Included)",
"Group Internal Events Only",
"Group All Events (Public Included)",
"Question bank");
$i_GroupCategorySettings = "Group Category";
$i_GroupCategoryName = "Category Name";
$i_GroupCategoryLastModified = "Last Modified";
$i_GroupCategoryNewDefaultRole = "Default Role for this category";

$i_Polling_Settings = "Polling Settings";
$i_PollingQuestion = "Question";
$i_PollingAnswerA = "A";
$i_PollingAnswerB = "B";
$i_PollingAnswerC = "C";
$i_PollingAnswerD = "D";
$i_PollingAnswerE = "E";
$i_PollingAnswerF = "F";
$i_PollingAnswerG = "G";
$i_PollingAnswerH = "H";
$i_PollingAnswerI = "I";
$i_PollingAnswerJ = "J";
$i_PollingReference = "Reference";
$i_PollingDateStart = "Start";
$i_PollingDateEnd = "End";
$i_PollingDateRelease = "Result release date";
$i_PollingDateRelease_remark = "Result will be released after poll submitted AND on or after release date";
$i_PollingRecordType = "Type";
$i_PollingRecordStatus = "Status";
$i_PollingDateInput = "Input Date";
$i_PollingDateModified = "Last Modified";
$i_PollingWrontStart = "Do not set Start Date before the current date.";
$i_PollingWrongEnd = "Do not set End Date before Start Date.";
$i_PollingTopic = "Recent Topics";
$i_PollingPeriod = "Period";
$i_PollingResult = "Result";
$i_Polling_PastDescription = "Click on any poll topic which interests you to view the results.";
$i_Polling_Vote = "Vote";

$i_eNews = "eNews";
$i_eNews_Settings = "eNews Settings";
$i_eNews_AddTo = "Add to";
$i_eNews_DeleteTo = "Delete to";
$i_eNews_Approval = "Approval";
$i_eNews_Approval2 = "eNews Approval";

$i_ResourceCode = "Resource Code";
$i_ResourceCategory = "Category";
$i_ResourceTitle = "Title";
$i_ResourceDescription = "Description";
$i_ResourceRemark = "Remark";
$i_ResourceAttachment = "Attachment";
$i_ResourceRecordType = "Type";
$i_ResourceRecordStatus = "Status";
$i_ResourceDateInput = "Input Date";
$i_ResourceDateModified = "Last Modified";
$i_ResourceDateStart = "Start Date";
$i_ResourceDateEnd = "End Date";
$i_ResourcePeriodType = "For every";
$i_ResourceDateStartWrong = "Do not set Start Date before the current date.";
$i_ResourceDateEndWrong = "Do not set End Date before Start Date.";
$i_ResourceDaysBefore = "Minimum days in advance for booking";
$i_ResourceTimeSlot = "Timetable";
$i_ResourceDaysBeforeWarning = "No. of days must be a positive integer or zero.";
$i_ResourceImportFile = "Please specify the file to import";
$i_ResourceImportNote = "Note";
$i_ResourceImportNote1 = "<ul><li>Timetable name needs to be exactly matched, otherwise that record will be ignored.
<li><a href=javascript:newWindow('tscheme.php',0)>View timetables</a>
<li><a href=itemsample.csv> Download sample (CSV format)</a></ul>";
$i_ResourceViewScheme = "<a href=javascript:newWindow('tscheme.php',0)>View timetables</a>";
$i_ResourcePopScheme = "current school timetable";
$i_ResourceAutoApprove = "Auto approved?";
$i_ResourceSelectCategory = "Category";
$i_ResourceSelectItem = "Item";
$i_ResourceSlotChangeAlert = "Changing timetable may cause inconsistencies in booking records. Do you want to proceed?";
$i_ResourceSlotChangeAlert2 = "(Users will NOT receive email notices)";
$i_ResourceResetBooking = "Reset booking records?";
$i_ResourceResetBooking2 = "(Remove all booking records of this item (future records will be deleted and past records will be archived.)<br> $i_ResourceSlotChangeAlert2";
$i_ResourceSelectAllCat = "All Categories";
$i_ResourceSelectAllSituations = "All Situations";
$i_ResourceSelectAllItem = "All Items";
$i_ResourceSelectAllResourceCode = "All Resource Codes";
$i_ResourceSelectAllUser = "All Users";
$i_ResourceSelectAllStatus = "All Status";

$i_RoleTitle = "Title";
$i_RoleDescription = "Description";
$i_RoleRecordType = "Type";
$i_RoleRecordStatus = "Status";
$i_RoleDateInput = "Input Date";
$i_RoleDateModified = "Last Modified";
$i_RoleDefault = "Default";
$i_RoleNewPrompt = "Please enter the name of new role";

$i_TimetableTitle = "Title";
$i_TimetableDescription = "Description";
$i_TimetableURL = "Attachment";
$i_TimetableRecordType = "Type";
$i_TimetableRecordStatus = "Status";
$i_TimetableDateInput = "Input Date";
$i_TimetableDateModified = "Last Modified";
$i_Timetable_type0 = "Use School Timetable <a href=\"/home/school/timetable/school_timetable.csv\">[sample (CSV format)]</a>";
$i_Timetable_type1 = "Use Self-defined Timetable <a href=\"/home/school/timetable/self_timetable.csv\">[sample (CSV format)]</a>";
$i_Timetable_IndexInstruction = "Click Edit to fill in the fields. You may use a self-defined timetable. Click Import to upload the file.";


$i_UserLogin = "Login ID";
$i_UserPassword = "Password";
$i_UserEmail = "Email";
$i_UserFirstName = "First Name";
$i_UserLastName = "Last Name";
$i_UserChineseName = "Chinese Name";
$i_UserEnglishName = "English Name";
$i_UserNickName = "Nick Name";
$i_UserDisplayName = "Display Name";
$i_UserTitle = "Title";
$i_UserGender = "Gender";
$i_UserDateOfBirth = "DOB";
$i_UserHomeTelNo = "Home Tel";
$i_UserOfficeTelNo = "Office Tel";
$i_UserMobileTelNo = "Mobile";
$i_UserMobileSMSNotes = "<br>(This mobile is used for receiving SMS message. For users who will not receive SMS message, please leave it blank.)";
$i_UserFaxNo = "Fax";
$i_UserICQNo = "ICQ";
$i_UserURL = "Homepage URL";
$i_UserAddress = "Address";
$i_HKID = "HKID";
$i_STRN = "STRN";
$i_UserCountry = "Country";
$i_UserInfo = "Info";
$i_UserRemark = "Remark";
$i_UserClassNumber = "Class No";
$i_UserClassName = "Class Name";
$i_UserClassLevel = "Class Level";
$i_UserLastUsed = "Last Used";
$i_UserLastModifiedPwd = "Password Last Modified";
$i_UserRecordType = "Type";
$i_UserRecordStatus = "Status";
$i_UserDateInput = "Input Date";
$i_UserDateModified = "Last Modified";
$i_UserPhoto = "Photo";
$i_UserPhotoDelete = "Delete Current Photo";
$i_UserPhotoGuide = "Photo must be in '.JPG', '.GIF', or '.PNG' format and the size should be fixed to 100 x 130 pixel (W x H).";
$i_ClassNameNumber = "Class (Class Number)";
$i_UserResetPassword = "Reset Password";
$i_UserNotMustChangePasswd = "(If you do not want to change your password, just leave the above 3 fields empty.)";
$i_UserName = "Name";
$i_UserChineseName = "Chinese Name";
$i_UserEnglishName = "English Name";
$i_UserOpenWebmail = "Open webmail account for new account";
$i_UserProfileDescription = "The Personal Info, Contact and Message below will be shown in your eClass(es). The fields are optional.";
$i_UserProfilePersonal = "Personal Info";
$i_UserProfileContact = "Contact Info";
$i_UserProfileMessage = "Message";
$i_UserProfileLogin = "Login Password";
$i_UserProfileEmailUse = "This is used for receiving system mail. Therefore, please enter ONE valid email only. Any invalid email may lead to problems when using the system.";
$i_UserProfilePwdChangeRule = "No need to fill in the following fields if you do not want to change your password.";
$i_UserProfilePwdUse = "for verification purpose";
$i_UserStudentName = "Student Name";
$i_UserProfile_Change_Notice="If you want to change it, please contact the administrator.";

$i_UserImportGraduate = "$button_import Leaver(s) List";
$i_UserGraduate_ImportInstruction = "<b><u>Description of import file:</u></b><br>
Column 1: $i_UserLogin of the user.<br>
";
$i_UserParentLink = "Linked Students";
$i_UserParentLink_SelectClass = "Select Class";
$i_UserParentLink_SelectStudent = "Select Student";
$i_UserParentWithLink = "Linked";
$i_UserParentWithNoLink = "No Linked";
$i_UserParent_LinkedChildren = "'s Children List";
$i_UserAccountOpenForNewOnly = "New Users: Linux Account (for FTP/Email) will be created automatically. <br>Existing Users: Access Right will be updated, but account will not be automatically created for security issues.";
$i_UserRemoveConfirm = "Please note that the following corresponding account will be removed:";
$i_UserLinuxAccount = "Linux Account (for email/personal files)";
$i_UserLDAPAccount = "LDAP Account (for authentication)";
$i_UserRemoveConfirmAsk = "Are you sure you want to remove selected user(s)";
$i_UserDefaultExport = "Export Default Format (which can be imported back to the system)";
$i_UserDefaultExportWarning = "To Export Default Format (which can be imported back to the system), please select an identity";
$i_UserDisplayTitle_English = "Display Title (Eng.)";
$i_UserDisplayTitle_Chinese = "Display Title (Chi.)";
$i_UserWarningiPortfolio = "<font color=red>Note that: iPortfolio account is unique and cannot be transferred. However, data of Student Profile and iPortfolio will be archived if the student account is deleted. But, all archived account records CANNOT be restored or modified.</font>";
//$i_UserRemoveStop_PaymentBalanceNonZero = "Action has been stopped as some users have non-zero balance in their account. Please refund for the following users before remove them.";
$i_UserRemoveStop_PaymentBalanceNonZero ="User(s) with non-zero account balance or unpaid item(s) found. This operation has been terminated.";

$i_FileSystemFiles = "File(s)";
$i_FileSystemName = "Name";
$i_FileSystemSize = "Size";
$i_FileSystemModified = "Last Modified";
$i_FileSystemUp = "Up to higher level directory";

$i_eClassCourseID = "Course ID";
$i_eClassCourseCode = "Course Code";
$i_eClassCourseCodePrefix = "Course Code Prefix";
$i_eClassCourseCodePrefixGuide = "i.e. \"C\" of course code \"C003\"";
$i_eClassCourseSettings = "Course settings";
$i_eClassCourseProposal = "Based on existing classes and subjects, possible course(s) is proposed below";
$i_eClassCourseProposalNotes = "Note:<br> Teacher(s) will be added to corresponding course according to \"Teaching Appointment Settings\". Please make sure all teaching appointment settings have been finished.";
$i_eClassCourseConfirm = "Are you sure you want to add the selected course(s)?";
$i_eClassCourseProcessing = "Creating course(s): ";
$i_eClassCourseWait = "Please wait ... ";
$i_eClassCourseDone = "Completed!";
$i_eClassCourseImportStudent = "Import Students";
$i_eClassLicenseExceed = "You have not enough classroom licenses for the courses selected.";
$i_eClassCourseNoneWarn = "Please select at least course.";
$i_eClassCourseProposalNone = "No possible course is available due to lack of class or subject record. You should input class and subject in \"School Settings\".";
$i_eClassCourseName = "Course Name";
$i_eClassCourseDesc = "Course Description";
$i_eClassIsGuest = "Guest Allowed";
$i_eClassNumUsers = "# Users";
$i_eClassMaxUser = "Max # Users";
$i_eClassMaxStorage = "Max Storage";
$i_eClassStorage = "Storage (MB)";
$i_eClassFileNo = "# File";
$i_eClassFileSize = "File Size (MB)";
$i_eClassFileSpace = "Storage Left (MB)";
$i_eClassHitsTotal = "Total Hits";
$i_eClassHitsTeacher = "Teacher Hits";
$i_eClassHitsStudent = "Student Hits";
$i_eClassHitsAssistant = "Assistant Hits";
$i_eClassInputdate = "Input Date";
$i_eClassCreatorBy = "Creator By";
$i_eClassLicenseDisplayN1 = "Your classroom license is ";
$i_eClassLicenseDisplayR1 = "Your readingroom license is ";
$i_eClassLicenseDisplay2 = " , with ";
$i_eClassLicenseDisplay3 = " license(s) left. ";
$i_eClassLicenseDisplay4 = "You have used up ";
$i_eClassLicenseDisplay5 = " .";
$i_eClassLicenseMsg1 = "Classroom Licenses";
$i_eClassLicenseMsg2 = "Total";
$i_eClassLicenseMsg3 = "Used";
$i_eClassLicenseMsg4 = "Left";
$i_eClassLicenseFull = "You have used all classroom licenses. You cannot create any new classroom.";
$i_eClassUserQuotaMsg1 = "User quota is";
$i_eClassUserQuotaMsg2 = ", with";
$i_eClassUserQuotaMsg3 = "vacanc(ies) left.";
$i_eClassUserQuotaMsg4 = "You have used up";
$i_eClassUserImport1 = "Step 1: Select Group";
$i_eClassUserImport2 = "Step 2: Check Users";
$i_eClassStorageDisplay1 = "Storage quota is";
$i_eClassStorageDisplay2 = " MB , with";
$i_eClassStorageDisplay3 = " MB storage space left. ";
$i_eClassStorageDisplay4 = "You have used up";
$i_eClassStorageDisplay5 = " MB.";
$i_eClassStorageDisplay6 = "folder(s) and";
$i_eClassStorageDisplay7 = " file(s) in total.";
$i_eClassAdminFileRight = "Release All File Access Rights";
$i_eClass_Admin_MgmtCenter = "eClass Mgmt Centre";
$i_eClass_Admin_Settings = "eClass Settings";
$i_eClass_Admin_Stats = "eClass Statistics";
$i_eClass_Admin_AssessmentReport = "Assessment Report";
$i_eClass_Admin_AllAssessments = "All Assessments";
$i_eClass_Admin_NoStudentsinAnyeClass = "No Students in Any eClasses";
$i_eClass_Admin_OnlyAssessmentsPassedDeadline = "Only Assessments Passed Deadline";
$i_eClass_Admin_Shared_Files = "Teacher Sharing Area";
$i_eClass_Admin_stats_login = "Login Records";
$i_eClass_Admin_stats_participation = "Participation Records";
$i_eClass_Admin_stats_files = "File Usage";
$i_eClass_Identity = "Identity";
$i_eClass_ClassLink = "Class";
$i_eClass_SubjectLink = "Subject";
# eclass homework list $i_eClass_ClassSubjectNote = "(Class and Subject is required for Intranet Homework List)";
$i_eClass_normal_course = "Normal Course";
$i_eClass_reading_room = "Reading Room";
$i_eClass_elp = "eClass Learning Park";
$i_eClass_elp_courseware = "Courseware";
$i_eClass_ssr = "Special Survey Room";
$i_eClass_ssr_type = "Survey Type";
$i_eClass_course_type = "Course Type";
$i_eClass_group_teacher = "Teacher 老師";
$i_eClass_group_assistant = "Assistant 助教";
$i_eClass_group_student = "Student 學生";
$i_eClass_group_default = "Default Group 預設小組";
$i_eClass_set_teacher_rights = "Set Teachers' Rights";
$i_eClass_eclass_group = "Set eClass Group";
$i_eClass_teacher_rights_1 = "Manage Course";
$i_eClass_teacher_rights_2 = "Approve/grade Reports";
$i_eClass_teacher_rights_3 = "Student Profile Team";
$i_eClass_Tools = "Tools Embedded";
$i_eClass_Tool_LicenseLeft = "License Left";
$i_eClass_Tool_EquationEditor = "Equation Editor";
$i_eClass_Tool_EquationEditorUsing = "Using Equation Editor";
$i_eClass_management_enable = "Enable eClass Management for teachers";
$i_eClass_teacher_open_course = "Allow teachers to add/edit/delete eClass";
$i_eClass_email_function = "Email Function";
$i_eClass_email_function_enable = "Enable email sending via eClass by";
$i_eClass_identity_helper = "Helper";
$i_eClass_quota_exceeded = "Import failed. User quota exceeded. Please add user quota of this eClass.";
$i_eClass_no_user_selected = "Please select at least one user!";
$i_eClass_batch_new = "Batch New";
$i_eClass_batch_import = "Batch Import Student List";
$i_eClass_ImportInstruction_batch_new = "
Column 1 : $i_eClassCourseCode<br>
Column 2 : $i_eClassCourseName<br>
Column 3 : $i_eClassMaxUser<br>
Column 4 : $i_eClassMaxStorage<br>
Column 5 : Code of Key Learning Areas
";
$i_eClass_CategoryList = "Key Learning Areas Code Mapping";
$i_eClass_ImportInstruction_batch_member = "
第一欄 : $i_UserLogin<br>
第二欄 : $i_eClassCourseID<br>
第三欄 : 小組名稱
";

$i_eClass_planner_function = "Planner";
$i_eClass_planner_type_color = "Define event type and color (leave it empty if don't use)";
$i_eClass_planner_type = "Type";
$i_eClass_planner_color = "Color";
$i_eClass_survey_deadline = "Questionnaire Deadline";

$i_eClass_function_access = "Function Access";
$i_eClass_parent_access = "Parent Access";
$i_eClass_portfolio_lang_c = "(Chi)";
$i_eClass_portfolio_lang_e = "(Eng)";

$i_eClass_Bulletin_BadWords_Instruction_top = "You can input restricted bad words in the textbox. These bad words will be replaced by *** in bulletin message.";
$i_eClass_MyeClass_Content = "Content";
$i_eClass_MyeClass_Bulletin = "Bulletin";
$i_eClass_MyeClass_Announcement = "Announcement";
$i_eClass_MyeClass_SharedLinks  = "Shared Links";

$i_ssr_anonymous_warning = "The \\\"Anonymous\\\" option cannot be reset once the survey has been attempted, unless all the received survey have been cleared.";
$i_ssr_anonymous_guide = "In order to reset this option, please clear all the received survey first.";

$i_GroupRole = array("Identity","Admin","Academic","Class","House","ECA","Miscellaneous");
$i_GroupRoleOrganization = array("identity","admin","academic","class","house","eca","misc");

# $i_frontpage_separator = "<img src=/images/frontpage/triangleicon.gif border=0 align=absmiddle hspace=5>";
$i_frontpage_separator = "<img src=/images/arrow.gif border=0 align=absmiddle hspace=5>";
$i_frontpage_menu_schoolinfo = "<img src=/images/header/icon_schinfo.gif border=0>";
$i_frontpage_menu_schoolinfo_organization = "Organization";
$i_frontpage_menu_schoolinfo_groupinfo = "Group Info";
$i_frontpage_menu_schoolinfo_campus_gallery = "Campus Gallery"; #add by kelvin ho 2008-12-23
$i_frontpage_menu_schoolinfo_homework = "Homework List";
$i_frontpage_menu_campusmail = "<img src=/images/header/icon_campusmail.gif border=0>";
$i_frontpage_menu_campusmail_inbox = "Inbox";
$i_frontpage_menu_campusmail_compose = "Compose";
$i_frontpage_menu_campusmail_title = "Campus Mail";
$i_frontpage_menu_myinfo = "<img src=/images/header/icon_myinfo.gif border=0>";
$i_frontpage_menu_myinfo_profile = "Update Profile";
$i_frontpage_menu_myinfo_email = "Email Settings";
$i_frontpage_menu_myinfo_password = "Password Settings";
$i_frontpage_menu_myinfo_bookmark = "My Bookmark";
$i_frontpage_menu_myinfo_cabinet = "My File Cabinet";
$i_frontpage_menu_classes = "<img src=/images/header/icon_classes.gif border=0>";
$i_frontpage_menu_resourcebooking = "<img src=/images/header/icon_resourcesbking.gif border=0>";
$i_frontpage_menu_resourcebooking_myrecords = "My Records";
$i_frontpage_menu_resourcebooking_reserved = "Reserved Records";
$i_frontpage_menu_eclass = "<img src=/images/header/icon_eclass.gif border=0>";
$i_frontpage_menu_campustv_title = "Campus TV";
$i_frontpage_menu_library = "<img src=/images/header/icon_library.gif border=0>";
$i_frontpage_menu_library_title = "Library";
$i_frontpage_menu_octopus = "SchoolPlus";
$i_frontpage_menu_logo = "<a href=/home/><img src=/images/frontpage/logo_eng.gif border=0 width=112 height=37></a>";
$i_frontpage_menu_home = "Home";
$i_frontpage_menu_exit = "<img src=/images/header/icon_logout.gif border=0>";
$i_frontpage_menu_eclass_mgt = "eClass Management";

$i_frontpage_button_result = "<img src=/images/frontpage/button_result_eng.jpg border=0 width=51 height=22>";
$i_frontpage_button_vote = "<img src=/images/frontpage/button_vote_eng.jpg border=0 width=50 height=22>";
$i_frontpage_button_cancel = "<img src=/images/frontpage/button_cancel_eng.gif border=0 width=75 height=28>";
$i_frontpage_button_reset = "<img src=/images/frontpage/button_reset_eng.gif border=0 width=75 height=28>";
$i_frontpage_button_save = "<img src=/images/frontpage/button_save_eng.gif border=0 width=75 height=28>";
$i_frontpage_button_submit = "<img src=/images/frontpage/button_submit_eng.gif border=0 width=75 height=28>";
$i_frontpage_button_update = "<img src=/images/frontpage/button_update_eng.gif border=0 width=75 height=28>";
$i_frontpage_image_result = "/images/frontpage/button_result_eng.jpg";
$i_frontpage_image_vote = "/images/frontpage/button_vote_eng.jpg";
$i_frontpage_image_cancel = "/images/frontpage/button_cancel_eng.gif";
$i_frontpage_image_reset = "/images/frontpage/button_reset_eng.gif";
$i_frontpage_image_save = "/images/frontpage/button_save_eng.gif";
$i_frontpage_image_submit = "/images/frontpage/button_submit_eng.gif";
$i_frontpage_image_update = "/images/frontpage/button_update_eng.gif";
$i_frontpage_day = array("S","M","T","W","T","F","S");
$i_frontpage_classes = "Classes";
$i_frontpage_eclass = "eClass";
$i_frontpage_eclass_courses = "eClass";
$i_frontpage_eclass_lastlogin = "Last Login";
$i_frontpage_eclass_whatsnews = "What's New";
$i_frontpage_myinfo = "My Info";
$i_frontpage_myinfo_profile = "Update Profile";
$i_frontpage_myinfo_email = "Email Settings";
$i_frontpage_myinfo_password = "Password Settings";
$i_frontpage_myinfo_password_old = "Old password";
$i_frontpage_myinfo_password_new = "New password";
$i_frontpage_myinfo_password_retype = "Retype password";
$i_frontpage_myinfo_password_mismatch = "Password mis-matched";
$i_frontpage_myinfo_bookmark = "My Bookmark";
$i_frontpage_myinfo_cabinet = "My File Cabinet";
$i_frontpage_myinfo_title_profile = "<img src=/images/myaccount_personalinfo/title_personalinfo.gif>";
$i_frontpage_myinfo_title_email = "<img src=/images/frontpage/title_myinfo_email_eng.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_password = "<img src=/images/frontpage/title_myinfo_password_eng.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_bookmark = "<img src=/images/frontpage/title_myinfo_bookmark_eng.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_cabinet = "<img src=/images/frontpage/title_myinfo_cabinet_eng.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_rb = "Resources Booking";
$i_frontpage_rb_msg = "Booking Record";
$i_frontpage_rb_title_add = "<img src=/images/frontpage/title_resourcebooking_add_eng.gif border=0 width=313 height=42 hspace=5 vspace=5>";
$i_frontpage_rb_title_edit = "<img src=/images/frontpage/title_resourcebooking_edit_eng.gif border=0 width=313 height=42 hspace=5 vspace=5>";
$i_frontpage_rb_title_form = "<img src=/images/frontpage/resourcebooking_form_title_eng.gif border=0 width=275 height=35>";
$i_frontpage_rb_title = "<img src=/images/resourcesbooking/title.gif>";
$i_frontpage_rb_title_reserved = "<img src=/images/resourcesbooking/title_reservedrecords.gif>";
$i_frontpage_rb_title_mybooking = "<img src=/images/frontpage/mybooking_title_eng.gif border=0 width=170 height=30>";
$i_frontpage_rb_title_mybookeditems = "<img src=/images/frontpage/mybookeditems_title_eng.gif border=0 width=414 height=50>";
$i_frontpage_rb_title_allbookeditems = "<img src=/images/frontpage/allbookeditems_title_eng.gif border=0 width=552 height=50>";
$i_frontpage_rb_title_allavailableitems = "<img src=/images/frontpage/allavailableitems_title_eng.gif border=0 width=552 height=50>";
$i_frontpage_rb_date = "Date";
$i_frontpage_rb_period = "Period";
$i_frontpage_rb_note = "Note";
$i_frontpage_rb_lastmodified = "Last Modified";
$i_frontpage_home_bookings_title = "<img src=/images/frontpage/home_bookings_eng.gif border=0 width=163 height=37>";
$i_frontpage_home_eclass_title = "<img src=/images/frontpage/home_eclass_eng.gif border=0 width=163 height=37>";
$i_frontpage_home_mygroup_title = "<img src=/images/frontpage/home_mygroup_eng.gif border=0 width=163 height=37>";
$i_frontpage_home_schoolannouncement_title = "<img src=/images/index/announcement_glass.gif>";
$i_frontpage_home_cal_legend = "<img src=/images/frontpage/cal_legend_eng.gif border=0 width=158 height=97 vspace=5>";
$i_frontpage_home_cal_legend_academicevents = "<img src=/images/frontpage/cal_legend_academicevents_eng.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_schoolevents = "<img src=/images/frontpage/cal_legend_schoolevents_eng.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_holidays = "<img src=/images/frontpage/cal_legend_holidays_eng.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_groupevents = "<img src=/images/frontpage/cal_legend_groupevents_eng.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_academicevents = "<img src=/images/frontpage/cal_white_legend_academicevents_eng.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_schoolevents = "<img src=/images/frontpage/cal_white_legend_schoolevents_eng.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_holidays = "<img src=/images/frontpage/cal_white_legend_holidays_eng.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_groupevents = "<img src=/images/frontpage/cal_white_legend_groupevents_eng.gif border=0 width=158 height=22>";
$i_frontpage_home_title_vote = "<img src=/images/index/pollingtab.gif width=69 height=21>";
$i_frontpage_home_title_vote2 = "<img src=/images/index/pollingtab2.gif width=203 height=6>";
$i_frontpage_home_title_pastvote = "<img border=0 src=/images/index/pollingicon.gif width=13 height=19>";
$i_frontpage_schoolinfo = "School Info";
$i_frontpage_schoolinfo_organization = "Organization";
$i_frontpage_schoolinfo_groupinfo = "Group Info";
$i_frontpage_schoolinfo_groupinfo_event = "Event";
$i_frontpage_schoolinfo_groupinfo_announcement = "Announcement";
$i_frontpage_schoolinfo_groupinfo_period = "Date Range";
$i_frontpage_schoolinfo_groupinfo_infotype = "Info Type";
$i_frontpage_schoolinfo_groupinfo_group = "Group";
$i_frontpage_schoolinfo_groupinfo_egroup = "eGroup";
$i_frontpage_schoolinfo_groupinfo_group_timetable = "Timetable";
$i_frontpage_schoolinfo_groupinfo_group_chat = "Chat room";
$i_frontpage_schoolinfo_groupinfo_group_bulletin = "Bulletin";
$i_frontpage_schoolinfo_groupinfo_group_links = "Shared Links";
$i_frontpage_schoolinfo_groupinfo_group_files = "Shared Files";
$i_frontpage_schoolinfo_groupinfo_group_settings = "Group Settings";
$i_frontpage_schoolinfo_groupinfo_group_members = "Member List";
$i_frontpage_schoolinfo_groupinfo_group_photoalbum = "Photo Album";
$i_frontpage_schoolinfo_groupinfo_thisweek = "This Week";
$i_frontpage_schoolinfo_groupinfo_thismonth = "This Month";
$i_frontpage_schoolinfo_groupinfo_today = "Today";
$i_frontpage_schoolinfo_groupinfo_today_onward = "From today onwards";
$i_frontpage_schoolinfo_popup_organization = "<img src=/images/organization/title.gif border=0>";
$i_frontpage_schoolinfo_title_organization = "<img src=/images/organization/title.gif border=0>";
$i_frontpage_schoolinfo_title_group = "<img src=/images/frontpage/title_schoolInfo_group_eng.gif border=0 width=255 height=42 hspace=5 vspace=5>";
$i_frontpage_schoolinfo_title_homework = "<img src=/images/frontpage/title_schoolinfo_homework_eng.gif width=289 height=45 border=0 hspace=5 vspace=5>";
$i_frontpage_schoolinfo_organization_misc_t = "<img src=/images/frontpage/schoolInfo_category_misc_title_eng.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_misc_m = "<img src=/images/frontpage/schoolInfo_category_misc_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_misc_b = "<img src=/images/frontpage/schoolInfo_category_misc_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_eca_t = "<img src=/images/frontpage/schoolInfo_category_eca_title_eng.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_eca_m = "<img src=/images/frontpage/schoolInfo_category_eca_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_eca_b = "<img src=/images/frontpage/schoolInfo_category_eca_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_house_t = "<img src=/images/frontpage/schoolInfo_category_house_title_eng.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_house_m = "<img src=/images/frontpage/schoolInfo_category_house_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_house_b = "<img src=/images/frontpage/schoolInfo_category_house_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_class_t = "<img src=/images/frontpage/schoolInfo_category_class_title_eng.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_class_m = "<img src=/images/frontpage/schoolInfo_category_class_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_class_b = "<img src=/images/frontpage/schoolInfo_category_class_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_admin_t = "<img src=/images/frontpage/schoolInfo_category_admin_title_eng.gif border=0 width=383 height=50>";
$i_frontpage_schoolinfo_organization_admin_m = "<img src=/images/frontpage/schoolInfo_category_admin_bg_m.gif border=0 width=383 height=5>";
$i_frontpage_schoolinfo_organization_admin_b = "<img src=/images/frontpage/schoolInfo_category_admin_bg_b.gif border=0 width=383 height=18>";
$i_frontpage_schoolinfo_organization_academic_t = "<img src=/images/frontpage/schoolInfo_category_academic_title_eng.gif border=0 width=383 height=50>";
$i_frontpage_schoolinfo_organization_academic_m = "<img src=/images/frontpage/schoolInfo_category_academic_bg_m.gif border=0 width=383 height=5>";
$i_frontpage_schoolinfo_organization_academic_b = "<img src=/images/frontpage/schoolInfo_category_academic_bg_b.gif border=0 width=383 height=18>";
$i_frontpage_schoolinfo_homework = "Homework List";
$i_frontpage_schoolinfo_popup_misc_t = "<img src=/images/frontpage/schoolinfo_popup_misc_tl_eng.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_eca_t = "<img src=/images/frontpage/schoolinfo_popup_eca_tl_eng.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_house_t = "<img src=/images/frontpage/schoolinfo_popup_house_tl_eng.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_class_t = "<img src=/images/frontpage/schoolinfo_popup_class_tl_eng.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_admin_t = "<img src=/images/frontpage/schoolinfo_popup_admin_tl_eng.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_academic_t = "<img src=/images/frontpage/schoolinfo_popup_academic_tl_eng.gif border=0 width=95 height=44>";
$i_frontpage_welcome = "<img src=/images/index/welcometxt.gif>";
$i_frontpage_currentpoll = "Current Polls";
$i_frontpage_pastpoll = "Past Polls";
$i_frontpage_my_eclass_t = "<img src=/images/frontpage/eclass_t_eng.gif border=0 width=770 height=45>";
$i_frontpage_dir_eclass_t = "<img src=/images/frontpage/eclass_directory_top_eng.gif border=0 width=172 height=27>";
$i_frontpage_eclass_eclass_dir = "eClass Directory";
$i_frontpage_eclass_eclass_my = "My eClass";
$i_frontpage_campusmail_campusmail = "Campus Mail";
$i_frontpage_campusmail_title_compose = "<img src=/images/campusmail/title_compose.gif border=0>";
$i_frontpage_campusmail_title_inbox = "<img src=/images/campusmail/title_inbox.gif border=0>";
$i_frontpage_campusmail_title_outbox = "<img src=/images/campusmail/title_outbox.gif border=0>";
$i_frontpage_campusmail_title_draft = "<img src=/images/campusmail/title_draft.gif border=0>";
$i_frontpage_campusmail_title_trash = "<img src=/images/campusmail/title_trash.gif border=0>";
$i_frontpage_campusmail_icon_compose = "<img src=/images/frontpage/campusmail/campusmail_compose_eng.gif border=0>";
$i_frontpage_campusmail_icon_inbox = "<img src=/images/frontpage/campusmail/campusmail_inbox_eng.gif border=0>";
$i_frontpage_campusmail_icon_outbox = "<img src=/images/frontpage/campusmail/campusmail_outbox_eng.gif border=0>";
$i_frontpage_campusmail_icon_draft = "<img src=/images/frontpage/campusmail/campusmail_draft_eng.gif border=0>";
$i_frontpage_campusmail_icon_trash = "<img src=/images/frontpage/campusmail/campusmail_trash_eng.gif border=0>";
$i_frontpage_campusmail_compose = "Compose";
$i_frontpage_campusmail_inbox = "Inbox";
$i_frontpage_campusmail_outbox = "Outbox";
$i_frontpage_campusmail_draft = "Draft";
$i_frontpage_campusmail_trash = "Trash";
$i_frontpage_campusmail_recipients = "Recipients";
$i_frontpage_campusmail_subject = "Subject";
$i_frontpage_campusmail_message = "Message";
$i_frontpage_campusmail_attachment = "Attachment";
$i_frontpage_campusmail_important = "Important";
$i_frontpage_campusmail_notification = "Notification";
$i_frontpage_campusmail_size = "Size (Kb)";
$i_frontpage_campusmail_size2 = "Size";
$i_frontpage_campusmail_read = "Read";
$i_frontpage_campusmail_unread = "Unread";
$i_frontpage_campusmail_total = "Total";
$i_frontpage_campusmail_reply = "Your Reply";
$i_frontpage_campusmail_i = "I";
$i_frontpage_campusmail_a = "A";
$i_frontpage_campusmail_date = "Date";
$i_frontpage_campusmail_subject = "Subject";
$i_frontpage_campusmail_sender = "Sender";
$i_frontpage_campusmail_status = "Status";
$i_frontpage_campusmail_folder = "Folder";
$i_frontpage_campusmail_choose = "Choose Recipient(s)";
$i_frontpage_campusmail_remove = "Remove Selected";
$i_frontpage_campusmail_attach = "Attach File(s)";
$i_frontpage_campusmail_send = "Send";
$i_frontpage_campusmail_save = "Save";
$i_frontpage_campusmail_reset = "Reset";
$i_frontpage_campusmail_delete = "Delete";
$i_frontpage_campusmail_close = "Close";
$i_frontpage_campusmail_add = "Add";
$i_frontpage_campusmail_expand = "Expand";
$i_frontpage_campusmail_retore = "Restore";
$i_frontpage_campusmail_icon_important = " <img src=/images/campusmail/btn_important.gif border=0>";
$i_frontpage_campusmail_icon_notification = "<img src=/images/frontpage/campusmail/icon_notification.gif border=0>";
$i_frontpage_campusmail_icon_attachment = "<img src=/images/frontpage/campusmail/icon_attachment.gif border=0>";
$i_frontpage_campusmail_icon_read = "<img src=/images/icon_envelope_read.gif border=0>";
$i_frontpage_campusmail_icon_unread = "<img src=/images/icon_envelope_unread.gif border=0>";
$i_frontpage_campusmail_icon_status_new = "<img src=/images/frontpage/campusmail/status_new_eng.gif border=0>";
$i_frontpage_campusmail_icon_status_read = "<img src=/images/frontpage/campusmail/status_read_eng.gif border=0>";
$i_frontpage_campusmail_icon_status_reply = "<img src=/images/frontpage/campusmail/status_replied_eng.gif border=0>";
$i_frontpage_campusmail_attachment_t = "<img src=/images/frontpage/campusmail/attachment_top_eng.gif width=450 height=40 border=0>";
$i_frontpage_campusmail_attachment_m = "<img src=/images/frontpage/campusmail/attachment_middle.gif width=450 height=16 border=0>";
$i_frontpage_campusmail_attachment_b = "<img src=/images/frontpage/campusmail/attachment_bottom.gif width=450 height=32 border=0>";
$i_frontpage_campusmail_namelist_t = "<img src=/images/frontpage/campusmail/namelist_top_eng.gif width=450 height=43 border=0>";
$i_frontpage_campusmail_namelist_m = "<img src=/images/frontpage/campusmail/namelist_middle.gif width=450 height=14 border=0>";
$i_frontpage_campusmail_namelist_b = "<img src=/images/frontpage/campusmail/namelist_bottom.gif width=450 height=21 border=0>";
$i_frontpage_campusmail_select_category = "Select group category";
$i_frontpage_campusmail_select_group = "Select group(s)";
$i_frontpage_campusmail_select_user = "Select user(s)";
$i_frontpage_campusmail_notification_instruction = "The sender of this message wants you to reply him upon receiving this message. Please reply below or simply tick the checkbox and click Send to state you have read this message.";
$i_frontpage_campusmail_notification_instruction_message_only = "The sender of this message wants you to reply this message. Please reply below and click Send.";
$i_frontpage_campusmail_alert_not_reply = "Are you sure you do not want to reply this message?";
$i_frontpage_campusmail_new_mail = " new message(s)";
$i_frontpage_survey = "Survey";
$i_frontpage_schedule = "Schedule";
$i_frontpage_announcement = "Announcement";
$i_frontpage_notes = "Contents";
$i_frontpage_links = "Shared Links";
$i_frontpage_bulletin = "Bulletin";

// Group Bulletin
$i_frontpage_bulletin_group = "Group";
$i_frontpage_bulletin_date = "Date";
$i_frontpage_bulletin_subject = "Subject";
$i_frontpage_bulletin_author = "Author";
$i_frontpage_bulletin_message = "Message";
$i_frontpage_bulletin_no_record = "There is no thread in this forum at the moment.";
$i_frontpage_bulletin_no_result = "Sorry, your search yielded no results. Please revise your search query and try again.";
$i_frontpage_bulletin_email_alert = "Reply Sender(s) via email";
$i_frontpage_bulletin_result = "Result";
$i_frontpage_bulletin_postnew = "New Topic";
$i_frontpage_bulletin_thread = "Topic";
$i_frontpage_bulletin_replies = "Response";
$i_frontpage_bulletin_reference = "Responses";
$i_frontpage_bulletin_postreply = "Post Reply";
$i_frontpage_bulletin_latest = "Last Message Posted";
$i_frontpage_bulletin_markallread = "Mark All Read";
$i_frontpage_bulletin_alert_msg1 = "Please enter the subject.";
$i_frontpage_bulletin_alert_msg2 = "Please enter a message.";
$i_frontpage_bulletin_alert_msg3 = "Please enter a keyword.";
$i_frontpage_bulletin_quote = "Quote";
$i_frontpage_bulletin_instruction = "To post a topic for discussion, click New Topic. To view and reply to messages, click on the topic(s).";

// Group Link
$i_LinkGroup = "Group";
$i_LinkUserName = "Author";
$i_LinkUserEmail = "Email";
$i_LinkCategory = "Category";
$i_LinkTitle = "Title";
$i_LinkURL = "URL";
$i_LinkKeyword = "Keyword";
$i_LinkDescription = "Description";
$i_LinkVoteNum = "# Votes";
$i_LinkRating = "Average Rate";
$i_LinkDateInput = "Input Date";
$i_LinkDateModified = "Last Modified";
$i_LinkSortBy = "Sort By";
$i_LinkNew = "New Link";
$i_LinkInstruction = "You may share the link with your other group(s) by pressing the Ctrl key while clicking the Group Name.";

// Group File
$i_FileGroup = "Group";
$i_FileUserName = "Author";
$i_FileUserEmail = "Email";
$i_FileCategory = "Category";
$i_FileTitle = "Filename";
$i_FileLocation = "Location";
$i_FileKeyword = "Keyword";
$i_FileDescription = "Description";
$i_FileDateInput = "Inputdate";
$i_FileDateModified = "Last Modified";
$i_FileSize = "File Size (KB)";
$i_FileNew = "New File";

// Campus Link
$i_CampusLinkTitle = "Title";
$i_CampusLinkURL = "URL";
$i_CampusLinkDescription = "Campus Link will be displayed in School Info Section.";
$i_CampusLink_title = "Campus Links";
$i_CampusLink_title_image = "<img src=/images/campus_links/title.gif>";
$i_CampusLink_LinkName = "Name";
$i_CampusLink_LinkAddress = "Address";
$i_CampusLink_Description = "Description";
$i_CampusLink_DisplayOrder = "Display Order";
$i_CampusLink_DisplayOrder_Desc = "1=First, 10=Last";
$i_CampusLink_Identity = "Identity";
$i_CampusLink_Identity_All = "All";
$i_CampusLink_RecordType = "Record Type";
$i_CampusLink_RecordType_Normal = "Normal";
$i_CampusLink_RecordType_Secure = "Secure";

// Cycle
$i_CycleDay = "Day";
$i_CycleCycle = "Cycle";
$i_CycleOptions = "Options";
$i_CycleType = "Day Type";
$i_CycleStart = "Start Date";
$i_CycleMsg = "The day info will be displayed on the school calendar.";
$i_CycleSelect = "Please Select";
$i_CycleWeek = "Week";
$i_Cycle5Day = "5-day";
$i_Cycle6Day = "6-day";
$i_Cycle7Day = "7-day";
$i_Cycle8Day = "8-day";
$i_CycleSpecialArrangment = "Special Arrangement";
$i_CycleSpecialForSatCounting = "Saturday will be counted as a cycle day in the following dates";
$i_CycleSpecialStart = "Start Date";
$i_CycleSpecialEnd = "End Date";
$i_CycleSpecialNotes = "All applied resources booking records using the old cycle timetable will be modified! Please inform users to update their applications after the Start Date of new cycle.";

$i_DayType0 = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
$i_DayType1 = array("Day 1","Day 2","Day 3","Day 4","Day 5","Day 6","Day 7","Day 8");
$i_DayType2 = array("Day A","Day B","Day C","Day D","Day E","Day F","Day G","Day H");
$i_DayType3 = array("Day I","Day II","Day III","Day IV","Day V","Day VI","Day VII","Day VIII");
$i_DayType4 = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");

$i_SimpleDayType0 = array ();
$i_SimpleDayType1 = array ("1","2","3","4","5","6","7","8");
$i_SimpleDayType2 = array ("A","B","C","D","E","F","G","H");
$i_SimpleDayType3 = array ("I","II","III","IV","V","VI","VII","VIII");

// Homework
$i_Homework_WholeSchool = "<font color=#FF6600>Normal Class</font>";
$i_Homework_New = "New";
$i_Homework_Import = "Import";
$i_Homework_SearchTeacher = "Search by poster";
$i_Homework_SearchSubject = "Search by subject";
$i_Homework_SearchTeaching = "Search by subjects taught by teacher";
$i_Homework_class = "Class";
$i_Homework_subject = "Subject";
$i_Homework_title = "Topic";
$i_Homework_description = "Description";
$i_Homework_loading = "Workload";
$i_Homework_startdate = "Start Date";
$i_Homework_duedate = "Due Date";
$i_Homework_hours = "hours";
$i_Homework_morethan = "More than";
$i_Homework_lastModified = "Last Modified";
$i_Homework_view = "View homework";
$i_Homework_unauthorized = "You are not allowed to view other classes' homework";
$i_Homework_404 = "This homework does not exist";
$i_Homework_TeacherName = "Teacher's Name";
$i_Homework_new_failed ="Insert failed, please check if the information you entered is valid.";
$i_Homework_new_succuess = "New homework has been assigned.";
$i_Homework_edit_failed = "Please check if the information you edited is valid and try again.";
$i_Homework_new_startdate_wrong ="Do not set Start Date before current date.";
$i_Homework_new_duedate_wrong ="Do not set Due Date before the Start Date.";
$i_Homework_new_title_missing = "Missing topic.";
$i_Homework_no_subject_alert = "Are you sure to submit this homework list without a subject?";
$i_Homework_new_subject_missing = "Please Select a Subject.";
$i_Homework_new_homework_type_missing="Please Select Homework Type.";
$i_Homework_import_errors = "<font color=red>Your file uploaded has some errors.</font>";
$i_Homework_import_success = "<font color=green> Records imported. </font>";
$i_Homework_new_failed = "Please check the information.";
$i_Homework_new_success = "Record created.";
$i_Homework_admin_startdate = "Enable Homework assigned on CURRENT day ONLY";
$i_Homework_admin_parent = "Enable Homework viewable by all parents";
$i_Homework_admin_disable = "Disable Homework List";
$i_Homework_admin_disable_search_subject = "Disable Search by Subject";
$i_Homework_admin_disable_search_teacher = "Disable Search by Poster";
$i_Homework_admin_disable_search_taught = "Disable Search by Subjects Taught";
$i_Homework_admin_nonteaching_access = "Enable Homework list (all classes and subjects) accessible to non-teaching staff";
$i_Homework_admin_restrict_access = "Restrict subject teachers to create homework of their own subjects ONLY";
$i_Homework_admin_enable_subjectleader = "Allow Subject Leader(s) to create homework";
$i_Homework_admin_enable_export = "Allow All teachers/staff using export";
$i_Homework_admin_clear_all_records = "Clear All Homework Records";
$i_Homework_admin_clear_all_records_alert = "Are you sure you want to clear these Homework Records?";
$i_Homework_admin_past_day_allowed = "Allow input past records";
$i_Homework_admin_use_homework_type = "Use Homework Type";
$i_Homework_admin_use_homework_handin = "Use Homework Handin Record Function";
$i_Homework_admin_use_teacher_collect_homework = "Indicate homework collection by class teacher";
$i_Homework_list = "Homework List";
$i_Homework_overdue_list="Homework Overdue List";
$i_Homework_handin_list = "Homework Hand-in List";
$i_Homework_handin_current_status ="Current Status";
$i_Homework_handin_records_will_be_deleted="Hand-in records of this homework will be deleted.";
$i_Homework_handin_continue="Continue";
$i_Homework_handin_not_handin="Not Handin";
$i_Homework_handin_handin="Handin";
$i_Homework_handin_no_need_to_handin="No Need To Handin";
$i_Homework_handin_change_status="Change Status";
$i_Homework_handin_set_NotHandin_to_NoNeedHandin="Set Not Handin to No Need To Handin";
$i_Homework_handin_set_All_to_Handin="Set All to Handin";
$i_Homework_handin_set_All_to_NotHandin="Set All to Not Handin";
$i_Homework_detail = "Detail";
$i_Homework_Handin_Required="Hand-in Required";
$i_Homework_Collection_Required = "Homework will be collected by class teacher";
$i_Homework_Collected_By_Class_Teacher = "Collected by class teacher";
$i_Homework_Collect_Required="Collection Required";
$i_Homework_import_sample = "Click here to download a sample file (CSV format).";
$i_Homework_ToDoList = "To-do list";
$i_Homework_AllRecords = "All Records";
$i_Homework_teacher_name = "Teacher Name";
$i_Homework_edit = "Edit";
$i_Homework_myrecords = "<font color=#FF6600>My record</font>";
$i_Homework_myrecords_icon = "<img src=/images/homeworklist/icon_myrecord.gif>";
$i_Homework_today = "<img src=/images/homework/en/hw/icon_todayhw.gif border=0 width=29 height=15>";
$i_Homework_con_add = "<font color=green>Record added.</font>";
$i_Homework_con_add_failed = "<font color=red>Errors in adding record. Please check and try again.</font>";
$i_Homework_import_instruction = "Please specify the file to upload";
$i_Homework_import_points = "Points to note:";
$i_Homework_import_instruction1 = "Class name and subject name need to be exactly matched.";
//$i_Homework_import_instruction2 = "Workload is in 1/2 hour unit. 5 represents 2.5 hours. (0 represents less than 0.5 hours and 17 represents more than 8 hours)";
$i_Homework_import_instruction2 = "Workload is in 1/4 hour unit. 2.5 represents 1.25 hours. (0 represents less than 0.25 hours and 17 represents more than 8 hours)";
$i_Homework_import_instruction3 = "Date format must be YYYY-MM-DD or YYYY/MM/DD. If you use Excel to edit, please change the date format in Format Cells.";
$i_Homework_import_instruction4 = "Fields other than pic and description must be filled in.";
$i_Homework_import_instruction5 = "Special Group Type homework cannot be imported.";
$i_Homework_import_instruction6 = "Homework Type need to be exactly matched.";
$i_Homework_import_reference = "For your reference";
$i_Homework_import_sample = "Sample CSV File";
$i_Homework_import_class = "Class List";
$i_Homework_import_subject = "Subject List";
$i_Homework_import_homeworktype = "Homework Type List";
$i_Homework_minimum = "--";
$i_Homework_WholeImage = "<img border=0 src=/images/btn_wholeschhomework.gif>";
$i_Homework_WholeIcon = "<img src=/images/homeworklist/icon_wholeschhomework.gif>";
$i_Homework_MyRecordsImage = "<img border=0 src=/images/btn_myrecord.gif>";
$i_Homework_PageTitle = "<img src=/images/homeworklist/title.gif>";
$i_Homework_PleaseSelectType = "Please select homework type";
$i_Homework_ClassType = "Class Type";
$i_Homework_GroupType = "Special Class Type";
$i_Homework_Type = "Homework Type";
$i_Homework_Group = "Special Class";
$i_Homework_IncludingClass = "Including Classes";
$i_Homework_MultipleClassInstruction = "(Holding CTRL to select more than 1 class)";
$i_Homework_Error_NoAccessRight = "The User has no right to create homework";
$i_Homework_Error_HandIn = "Please enter 'yes' or 'no' for 'Hand in Required' field";
$i_Homework_Error_Collect = "Please enter 'yes' or 'no' for 'Collect Required' field";
$i_Homework_Error_Empty = "Some fields missing";
$i_Homework_Error_NoSubject = "No such subject";
$i_Homework_Error_NoClass = "No such class";
$i_Homework_Error_Date = "Date invalid";
$i_Homework_Error_StartDate = "$i_Homework_startdate"." eariler than today.";
$i_Homework_Error_EndDate = "$i_Homework_duedate"." eariler than "."$i_Homework_startdate";
$i_Homework_Error_NoHomeworkType = "No such homework type";
$i_Homework_SpecialGroup = "Special Class";
$i_Homework_NormalClass = "Normal Class";
$i_Homework_WholeSchoolGroup = "<font color=#FF6600>Special Class</font>";
$i_Homework_ListView = "List View";
$i_Homework_MonthView = "Month View";
$i_Homework_WeekView = "Week View";
$i_Homework_ChildrenList = "Children Name List";
$i_Homework_Only = "Only";
$i_Homework_eClassAssignment = "This is the assignment of course: ";
$i_Homework_SubjectLeaderKey = "Represents Subject Leader";
$i_Homework_CreatedHomework = "Created Homework Record(s)";
$i_Homework_ByTeaching = "Taught Subjects Homework Record(s)";
$i_Homework_Poster = "Poster";
$i_Homework_Export_Instruction = "Please enter the date range to export homework records";
$i_Homework_HomeworkType = "Homework Type";

$i_Subject_name = "Subject Name";
$i_SAMS_code = "Code";
$i_SAMS_cmp_code = "Component Code";
$i_SAMS_description = "Description (Eng/Chi)";
$i_SAMS_abbr_name = "Abbreviation (Eng/Chi)";
$i_SAMS_short_name = "Short Form (Eng/Chi)";
$i_SAMS_CSV_file = "WEBSAMS CSV file";
$i_SAMS_download_subject_csv  = "download subject csv sample";
$i_SAMS_download_cmp_subject_csv  = "download component subject csv sample";
$i_SAMS_import_error_wrong_format = "Wrong import format";
$i_SAMS_import_update_no = "No. of items successfully updated";
$i_SAMS_import_add_no = "No. of items successfully added";
$i_SAMS_import_fail_no = "No. of items failed to be added";
$i_SAMS_import_error_row = "Row Number";
$i_SAMS_import_error_data = "Reason for the Error";
$i_SAMS_import_error_reason = "Error Record";
$i_SAMS_import_error_detail= "File info";
$i_SAMS_short_name_duplicate = "Same short form is not allowed.";
$i_SAMS_import_error_unknown = "Unknown";
$i_SAMS_notes_after = "After";


$i_import_error = "Some records in the uploaded file contains error. The records below cannot be updated.";
$i_image_home = "<img src=/images/home.gif border=0>";

$i_Campusquota_basic_identity = array("Teacher","Student","Admin Staff","Parent");
$i_Campusquota_identity = "Identity";
$i_Campusquota_quota = "Quota (in MBytes)";
$i_Campusquota_noLimit = "No Limit";
$i_Campusquota_max = "Total";
$i_Campusquota_used = "Used";
$i_Campusquota_left = "Left";
$i_Campusquota_uploadSuccess = "Attachment uploaded successfully";
$i_Campusquota_noQuota = "Not enough quota";
$i_Campusquota_using1 = "You are using ";
$i_Campusquota_using2 = " of your storage.";
$i_Set_enabled = "Permission";

$i_Tempfiles_display1 = "files occupying ";
$i_Tempfiles_display2 = "MBytes.";
$i_Tempfiles_confirm = "To delete the files, click Delete.";

$i_Batch_Default = "<font color=red> *</font>";
$i_Batch_SetDefault = "Set as School Timetable";
$i_Batch_Description = "Description for this Timetable";
$i_Batch_DefaultDescription = "* School Timetable";
$i_Batch_TitleNotNull = "You must enter the name of the timetable.";
$i_Batch_TitleDuplicated = "You cannot use the same name for a different timetable.";
$i_Slot_TimeRange = "Time Range";
$i_Slot_Title = "Period Title";
$i_Slot_ChangeArchiveRecords = "Remove all booking records using this time scheme (future records will be deleted and past records will be archived.)<br> $i_ResourceSlotChangeAlert2";
$i_Slot_NotArchiveAlert = "You have chosen not to reset the booking records. This may cause inconsistencies of booking records. Do you want to proceed?";


$i_schoolinfo_organization_iconleft_admin = "<img src=/images/organization/icon_admin.gif>";
$i_schoolinfo_organization_iconright_admin = "<img src=/images/organization/adminheading.gif>";
$i_schoolinfo_organization_iconbottom_admin = "<img src=/images/organization/adminfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_admin_popup = "<img src=/images/organization/adminfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_academic = "<img src=/images/organization/icon_academic.gif>";
$i_schoolinfo_organization_iconright_academic = "<img src=/images/organization/academicheading.gif>";
$i_schoolinfo_organization_iconbottom_academic = "<img src=/images/organization/academicfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_academic_popup = "<img src=/images/organization/academicfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_class = "<img src=/images/organization/icon_class.gif>";
$i_schoolinfo_organization_iconright_class = "<img src=/images/organization/classheading.gif>";
$i_schoolinfo_organization_iconbottom_class = "<img src=/images/organization/classfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_class_popup = "<img src=/images/organization/classfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_house = "<img src=/images/organization/icon_house.gif>";
$i_schoolinfo_organization_iconright_house = "<img src=/images/organization/househeading.gif>";
$i_schoolinfo_organization_iconbottom_house = "<img src=/images/organization/housefrbottom.gif>";
$i_schoolinfo_organization_iconbottom_house_popup = "<img src=/images/organization/housefrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_eca = "<img src=/images/organization/icon_eca.gif>";
$i_schoolinfo_organization_iconright_eca = "<img src=/images/organization/ecaheading.gif>";
$i_schoolinfo_organization_iconbottom_eca = "<img src=/images/organization/ecafrbottom.gif>";
$i_schoolinfo_organization_iconbottom_eca_popup = "<img src=/images/organization/ecafrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_misc = "<img src=/images/organization/icon_misc.gif>";
$i_schoolinfo_organization_iconright_misc = "<img src=/images/organization/mischeading.gif>";
$i_schoolinfo_organization_iconbottom_misc = "<img src=/images/organization/miscfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_misc_popup = "<img src=/images/organization/miscfrbottom_popup.gif>";

$image_back = "/images/btn_back.gif";
$image_cancel = "/images/btn_cancel.gif";
$image_chooserecipient = "/images/btn_chooserecipients.gif";
$image_delete = "/images/btn_delete.gif";
$image_edit = "/images/btn_edit.gif";
$image_next = "/images/btn_next.gif";
$image_ok = "/images/btn_ok.gif";
$image_pre = "/images/btn_pre.gif";
$image_removeselected = "/images/btn_removeselected.gif";
$image_reset = "/images/btn_reset.gif";
$image_save = "/images/btn_save.gif";
$image_search = "/images/btn_search.gif";
$image_send = "/images/btn_send.gif";
$image_submit = "/images/btn_submit.gif";
$image_update = "/images/btn_update.gif";
$image_periodic_a = "btn_periodic_a.gif";
$image_periodic_b = "btn_periodic_b.gif";
$image_single_a = "btn_single_a.gif";
$image_single_b = "btn_single_b.gif";
$image_moreAttach = "/images/btn_moreattachment.gif";
$image_add = "/images/btn_add.gif";
$image_closewindow = "/images/btn_closewindow.gif";
$image_expand = "/images/btn_expand.gif";
$image_selectall = "/images/btn_selectall.gif";
$image_iconlist = "/images/groupinfo/iconlist.gif";
$image_nextweek = "<img border=0 src=/images/btn_nextweek.gif width=131 height=24>";
$image_prevweek = "<img border=0 src=/images/btn_preweek.gif width=124 height=24>";
$image_prevday = "<img border=0 src=/images/btn_preday.gif width=124 height=24>";
$image_nextday = "<img border=0 src=/images/btn_nextday.gif width=131 height=24>";
$image_import = "/images/btn_import.gif";
$image_search = "/images/btn_search.gif";
$image_quote = "/images/btn_quote.gif";
$image_include_quote = "/images/btn_includequote.gif";
$image_edit = "/images/btn_edit.gif";
$image_clear = "/images/btn_clear.gif";
$image_restore = "/images/btn_restore.gif";
$image_viewSlots = "/images/btn_viewslots.gif";
$image_updaterole = "/images/btn_updaterole.gif";
$image_delete_member = "/images/btn_deletemember.gif";
$image_attachfiles = "/images/btn_attachfiles.gif";
$image_authorize = "/images/btn_authorize.gif";
$image_forbid = "/images/btn_forbid.gif";
$image_reject = "/images/btn_reject.gif";
$image_go = "/images/btn_go.gif";
$image_cs = "btn_customersupport.gif";
$image_cs_blink = "btn_customersupport_blink.gif";
$image_cs_admin = "btn_customersupport_admin.gif";
$image_cs_admin_blink = "btn_customersupport_admin_blink.gif";


$i_grouphead_timetable = "<img src=/images/groupinfo/heading_timetable.gif>";
$i_grouphead_chatroom = "<img src=/images/groupinfo/heading_chatroom.gif>";
$i_grouphead_bulletin = "<img src=/images/groupinfo/heading_bulletin.gif>";
$i_grouphead_sharedlinks = "<img src=/images/groupinfo/heading_sharedlinks.gif>";
$i_grouphead_sharedfiles = "<img src=/images/groupinfo/heading_sharedfiles.gif>";
$i_grouphead_groupsetting = "<img src=/images/groupinfo/heading_groupsetting.gif>";
$i_grouphead_questionbank = "<img src=/images/groupinfo/heading_schoolbasedqb.gif>";

$i_campusmail_stamp = "<img border=0 src=/images/campusmail/stamp.gif>";
$i_campusmail_compose = "<img border=0 src=/images/campusmail/icon_compose.gif>";
$i_campusmail_inbox = "<img border=0 src=/images/campusmail/icon_inbox.gif>";
$i_campusmail_outbox = "<img border=0 src=/images/campusmail/icon_outbox.gif>";
$i_campusmail_draft = "<img border=0 src=/images/campusmail/icon_draft.gif>";
$i_campusmail_trash = "<img border=0 src=/images/campusmail/icon_trash.gif>";
$i_campusmail_lettertext = "<img border=0 src=/images/campusmail/lettertext.gif>";
$i_campusmail_novaliduser = "No valid users in your selected recipients group";
$i_campusmail_policy = "Notification Reply Policy";
$i_campusmail_policy_original = "Sender will not know whether a recipient has read the mail until the recipient sends back a reply message.";
$i_campusmail_policy_auto = "Sender will know the mail is read once a recipient opened it.";
$i_campusmail_nondelivery = "Note that the following recipients cannot received this message as their mailbox does not have enough quota. They need to clear some space for this message, or you can send this message to them without any attachment.";
$i_campusmail_sent_successful = "Message has been delivered successfully";
$i_campusmail_save_successful = "Message has been saved succesfully";
$i_campusmail_processing = "Processing ...";
$i_campusmail_processing_note = "If the number of recipients is large, it may take some time to complete. Please wait.";
$i_campusmail_accessright = "Access Right";
$i_campusmail_ban = "Restrict Users to send messages";
$i_campusmail_ban_instruction = "Input the user login of restricted users. <br>ONE login name per line. <br>These users cannot send message in CampusMail but they can receive messages and reply to notification.<br>";
$i_campusmail_alert_trashall = "Are you sure to move all messages to trash?";
$i_campusmail_alert_emptytrash = "Are you sure to delete all messages in trash permanently?";
$i_campusmail_alert_export = "This will export only text in your messages NOT including attachments. Do you want to proceed?";
$i_campusmail_forceRemoval = "Force Removal";
$i_campusmail_forceRemoval_instruction1 = "Please input the start date and end date for the campusmail you want to remove";
$i_campusmail_forceRemoval_Start = "Start Date";
$i_campusmail_forceRemoval_End = "End Date";
$i_campusmail_next = "Next";
$i_campusmail_prev = "Prev";

$i_campusmail_recipient_selection_control="Recipients Selection Control";

$i_campusmail_select_interface="Select Interface";
$i_campusmail_use_old_interface= "Use old interface";
$i_campusmail_use_new_interface= "Use new interface";
$i_campusmail_staff_recipients_selection ="Select Staff Recipients";
$i_campusmail_student_recipients_selection="Select Student Recipients";
$i_campusmail_parent_recipients_selection ="Select Parent Recipients";
$i_campusmail_blocked_groups="Blocked Groups Categories";
$i_campusmail_group_categories="Group Categories";
$i_campusmail_usertypes ="User types";
$i_campusmail_blocked_usertypes="Blocked User types";
$i_campusmail_select_parents_from_students="Select parents from students";
$i_campusmail_teaching_staff="Teaching Staff";
$i_campusmail_non_teaching_staff="Non-teaching Staff";

$i_campusmail_all_teacher_staff="All teachers/staff";
$i_campusmail_no_teacher_staff="No teacher/staff";
$i_campusmail_teaching_sender_children_teacher="Teaching sender's children's teachers";
$i_campusmail_teaching_sender_teacher="Teaching sender's teachers";

$i_campusmail_all_students="All Students";
$i_campusmail_only_students_in_same_class_level="Only students in same class level";
$i_campusmail_only_students_in_same_class="Only students in same class";
$i_campusmail_no_students="No students can be sent";
$i_campusmail_only_students_in_taught_class="Only students in taught class";
$i_campusmail_only_sender_own_children="Only sender's own children";

$i_campusmail_all_parents="All parents";
$i_campusmail_only_parents_with_children_in_taught_class="Only parents with childrens in taught class";
$i_campusmail_only_parents_with_children_in_same_class_level="Only parents with children in same class level";
$i_campusmail_only_parents_with_children_in_same_class="Only parents with children in same class";
$i_campusmail_no_parents="No parents";
$i_campusmail_only_own_parents="Only students own parents";

$i_campusmail_no_users_available="There is no recipients available for selection";

$i_campusmail_usage_setting = "Usage Setting";


$i_header_myaccount = "<img border=0 src=/images/header/icon_myaccount.gif>";
$i_header_myaccount_teacher = "<img border=0 src=/images/header/icon_myaccount_t.gif>";
$i_header_newmail = "<img border=0 src=/images/header/icon_newmail.gif>";
$i_header_newmail_icon_path = "$image_path/header/icon_newmail.gif";
$i_header_studentprofile_icon = "$image_path/header/icon_studentprofile.gif";

$i_myac_title = "My Account";
$i_myac_myinfo = "Personal Info";

$i_Webmail_title = "Webmail";

$i_CampusTV_top = "<img src=/images/campus_tv/tv_top.gif width=640 height=110>";
$i_CampusTV_Title = "Title";
$i_CampusTV_56k = "Narrowband (56K)";
$i_CampusTV_200k = "Broadband (200K)";
$i_CampusTV_Select = "Please select a video clip and bandwidth";
#$i_CampusTV_Failed = "If the video clip cannot be displayed, <br>";
#$i_CampusTV_alt = "please click here to start a stand-alone player.";
$i_CampusTV_Failed = "";
$i_CampusTV_alt = "Click here to start a stand-alone player.";

$i_SMS_System_Message = "System Message";
$i_SMS_Balance_Lass_Than = "balance less than ";
$i_SMS_Notification = "SMS Notification";
$i_SMS_NoChangeMobile = "<br>(This mobile number is used for receiving SMS message only. If you want to change it, please contact the administrator.)";
$i_SMS_Cautions = "Note:";
$i_SMS_Limitation1 = "For English messages, a maximum of 160 characters can be sent.";
$i_SMS_Limitation2 = "For Traditional Chinese (BIG5) messages, a maximum of 70 characters (including Chinese/English characters and punctuation) can be sent.";
$i_SMS_Limitation3 = "Over-lengthened messages will be truncated.";
$i_SMS_Limitation4 = "Chinese punctuation may not be correctly displayed on some mobile devices. English punctuation is hence recommended.";
$i_SMS_Limitation5 = "These special characters are not supported: ~^`{}\|[]";
$i_SMS_Limitation6 = "Unicode characters may not be displayed.";
$i_SMS_SMS = "SMS Message";
$i_SMS_Send = "Send SMS";
$i_SMS_Send_Confirmation = "Send SMS Confirmation";
$i_SMS_Send_Broadcast = "Broadcast SMS Message";
$i_SMS_Send_SelectUser = "Send SMS Message to Intranet Users";
$i_SMS_Send_SelectParent = "Send SMS Message to Parents (Select By Students)";
$i_SMS_Send_Raw = "Send SMS Message (Input Mobile Numbers)";
$i_SMS_UsageReport = "Usage Report";
$i_SMS_MessageTemplate = "Message Templates";
$i_SMS_MobileNumber = "Mobile Number";
$i_SMS_Recipient = "Recipient(s)";
$i_SMS_Recipient2 = "Recipient";
$i_SMS_RelatedUser = "Related User";
$i_SMS_SeparateRecipient = "Please separate each mobile number by semi-colon (;)";
$i_SMS_MessageContent = "Message";
$i_SMS_SendDate = "Send Date";
$i_SMS_SendTime = "Send Time";
$i_SMS_Status = "Status";
$i_SMS_TargetSendTime = "Actual Transmission Time";
$i_SMS_SubmissionTime = "Request Sent Time";
$i_SMS_NoteSendTime = "Leave Send Date and Send Time blank to send message immediately.";
$i_SMS_MessageSent = "<font color=green>Message Sent</font>";
$i_SMS_AlertSelectUserType = "Please select at least 1 type of identity";
$i_SMS_AlertMessageContent = "Please input Message Content";
$i_SMS_AlertRecipient = "Please input Recipients";
$i_SMS_TargetStudent = "Target Students";
$i_SMS_ClearRecords = "Clear Records";
$i_SMS_RetrieveRecords = "Refresh Info from Service Center";
$i_SMS_MessageSentSuccessfully = "Messages Sent Successfully (Including status not retrieved from Service Center)";
$i_SMS_MessageQuotaLeft = "Message Quota Left";
$i_SMS_con_NoUser = "<font color=red>No mobile numbers have been input for the selected users</font>";
$i_SMS_con_DataRetrieved = "<font color=green>Data retrieved from Service Center</font>";
$i_SMS_NotExamined = "Not Examined";
$i_SMS_InTransit = "In Transit";
$i_SMS_Delivered = "Transmitted";
$i_SMS_Expired = "Expired";
$i_SMS_Deleted = "Deleted";
$i_SMS_Undeliverable = "Undeliverable";
$i_SMS_Accepted = "Accepted";
$i_SMS_Invalid = "Invalid";
$i_SMS_Pending = "Pending";
$i_SMS_InvalidPhone = "Invalid number";
$i_SMS_Status0 = $i_NotExamined;
$i_SMS_Status1 = $i_SMS_InTransit;
$i_SMS_Status2 = $i_SMS_Delivered;
$i_SMS_Status3 = $i_SMS_Expired;
$i_SMS_Status4 = $i_SMS_Deleted;
$i_SMS_Status5 = $i_SMS_Undeliverable;
$i_SMS_Status6 = $i_SMS_Accepted;
$i_SMS_Status7 = $i_SMS_Invalid;
$i_SMS_Status8 = $i_SMS_Pending;
$i_SMS_Status9 = $i_SMS_InvalidPhone;
$i_SMS_24Hr = "24-hr format in 2 digits";
$i_SMS_InvalidTime = "Invalid Time Format";
$i_SMS_no_student_select = "Please select at least one student!";
$i_SMS_RecipientCount = "Recipients#";
$i_SMS_PIC = "PIC";
$i_SMS_SentRecords = "View Sent Records";
$i_SMS_MessageCount = "Message#";

$i_SMS_TemplateName = "Template Name";
$i_SMS_TemplateCode = "Template Code";
$i_SMS_TemplateContent = "Content";
$i_SMS_File_Send = "File Input";
$i_SMS_ColData = "1st Column Data";
$i_SMS_Type_Mobile = "Mobile Number";
$i_SMS_Type_Login = "$i_UserLogin";
$i_SMS_UploadFile = "File Input";
$i_SMS_FileDescription = "<u><b>File Description:</b></u><br>
Column 1: Mobile Number<br>
Column 2: Message to be sent.";
$i_SMS_FileConfirm = "Please confirm uploaded messages to be sent is correct and click button Send.";
$i_SMS_SendTo_Student_Guardian ="Send To Student Guardian";
$i_SMS_Notice_Send_To_Users ="The message will be sent to the following recipient(s):";
$i_SMS_Warning_Cannot_Send_To_Users="Unable to send message to the following recipient(s):";
$i_SMS_Error_No_Guardian ="No Guardian";
$i_SMS_Error_NovalidPhone = "No Valid Phone Number";
$i_SMS_Notice_Show_Student_With_MobileNo_Only="Stduent who has no Mobile Number will not be shown";
$i_SMS_Notice_Show_Student_With_Guardian_Only="Student who has no Guardian will not be shown";

$i_SMS_SystemTemplate = "Preset Event Template";
$i_SMS_NormalTemplate = "Normal Event Template";
$i_SMS_Personalized_Msg_Tags = "Auto-Insertion Items";

/*
$i_SMS_Personalized_Tag['user_name_eng'] 		= "Target user's English name";
$i_SMS_Personalized_Tag['user_name_chi'] 		= "Target user's Chinese name";
$i_SMS_Personalized_Tag['user_class_name'] 		= "Target student's class name";
$i_SMS_Personalized_Tag['user_class_number'] 	= "Target student's class number";
$i_SMS_Personalized_Tag['user_login'] 			= "Target user's login name";
$i_SMS_Personalized_Tag['attend_card_time'] 	= "Arrival time or departure time when tapping card";
$i_SMS_Personalized_Tag['payment_balance'] 		= "Student's balance left";
$i_SMS_Personalized_Tag['payment_outstanding_amount'] = "Student's total outstanding amount";
*/



$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_ABSENCE'] = "Absence";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_LATE'] = "Late";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_EARLYLEAVE'] = "Early Leave";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_ARRIVAL'] = "Arrival";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_LEAVE'] = "Leave";
$i_SMS_Personalized_Msg_Template['STUDENT_MAKE_PAYMENT'] = "Make payment";
$i_SMS_Personalized_Msg_Template['BALANCE_REMINDER'] = "Balance reminder";

$i_SMS_Reply_Message="Reply Message";
$i_SMS_Reply_Message_View="View Messages";
$i_SMS_Reply_Message_View_Sent_Message="View Sent Records";
$i_SMS_Reply_Message_View_Replied_Message="View Replied Messages";
$i_SMS_Reply_Message_Message="Message";
$i_SMS_Reply_Message_Replied_MessageCount="Replied Count";
$i_SMS_Reply_Message_Replied_Message_DeliveryTime="Delivery Time";
$i_SMS_Reply_Message_Replied_Message="Replied Message";
$i_SMS_Reply_Message_Replied_PhoneNumber="Reply Phone Number";
$i_SMS_Reply_Message_Replied_Time="Reply Time";
$i_SMS_Reply_Message_Status_Not_Yet_Replied="Not Yet Replied";
$i_SMS_Reply_Message_Status_Replied="Replied";
$i_SMS_Reply_Message_Replied_Message_Stat="Replied Message Statistics";
$i_SMS_Reply_Message_Retrieve="Retrieve Reply Message";
$i_SMS_Reply_Message_DateRange_Msg="Display all records within the following date range";

$i_SMS_Personalized_Template_Instruction="You can add system generated information like student name and departure time to your sms message above.<BR>
<BR>To add those information, choose applicable items from the list below and click \"Add\". Placeholders in the form of (\$user_class_name) will be inserted.<BR><BR>";
$i_SMS_Personalized_Template_Type="Template Type";
$i_SMS_Personalized_Template_SendCondition="Preset Event";

$i_SMS_Select_DateRange="Download $i_SMS_UsageReport for the following data range";
$i_SMS_MultipleMessage="Multiple Messages";

$i_SMS['NoPluginWarning'] = "You can send instant messages using SMS (Short Message Service) directly from eClass IP. Please contact our sale representatives to activate SMS service!";
$i_SMS['jsWarning']['NoPluginWarning'] = "This SMS (Short Message Service) service is not yet enabled. Please contact our sale representatives to activate SMS service!";


$i_iconAttachment = "<img src=/images/icon_attachment.gif border=0>";

$i_Alert_Reject = "Are you sure you want to reject?";
$i_Alert_Checkin = "Are you sure you want to check in?";
$i_Alert_Checkout = "Are you sure you want to check out?";

$i_label_SelectAll = "ALL";

$i_Step_Name = "<img src=/images/step/step.gif border=0>";

$i_organization_description = "This page shows the organizational structure of the School. Each Section may consist of different groups.
To access the individual group's homepage, click on $i_image_home if available. To view the group members, click the group name.";

$i_SelectMultipleInstruction = "Press Ctrl key to select multiple";
$i_SelectMemberInstruction = "Please select the category first. You may select specific group(s) in Step 2 and individual user(s) in Step 3.";
$i_SelectMemberSteps = array (
"",
"Select Category",
"Select Or Expand specific group(s)",
"Select User(s)"
);
$i_SelectMemberNoGroupInstruction = "Please select the role of new user(s) first, then select category. Select specific group(s) in Step 3 to view a list of users. Individual user(s) can be added in Step 4.";
$i_SelectMemberNoGroupSteps = array (
"",
"Select Role of new user(s)",
"Select Category",
"Select and Expand specific group(s)",
"Select User(s)"
);

$i_toggleBy = "Filter By";

$i_sls_library_mapping_file = "User mapping file";
$i_sls_library_mapping_file_select = "Select the file to upload";
$i_sls_library_mapping_file_instruction = "Please note that the original file will be replaced by any new uploaded files. To add new mappings, download the original file first and append the records to it.";
$i_sls_library_mapping_file_use = "The mapping file is used in mapping user accounts of eClass Intranet and those of SLS Library System. The file is generated by a separate program. Please contact the Customer Support of BroadLearning for enquiry on generating the mapping file.";
$i_sls_library_mapping_file_download = "To download the current mapping file, click here.";
$i_sls_library_mapping_file_upload_success = "<font color=green>File Uploaded Successfully.</font>";
$i_sls_library_search = "Search";
$i_sls_library_info = "Information";
$i_sls_library_newadditions = "New Additions";
$i_sls_library_patron_top = "Top 10 Readers";
$i_sls_library_item_top = "Top 10 Items";
$i_sls_library_patron_info = "Reader Information";
$i_sls_library_checkout_list = "Checkout List";
$i_sls_library_hold_list = "Hold List";
$i_sls_library_outstanding_fine = "Outstanding Fine";
$i_sls_library_norecords = "There is no record at the moment.";
$i_sls_library_image_newadditions = "<img src='images/sls_info_btn_new_add.gif' border='0'>";
$i_sls_library_image_patron_top = "<img src='images/sls_info_btn_top_patron.gif' border='0'>";
$i_sls_library_image_item_top = "<img src='images/sls_info_btn_top_item.gif' border='0'>";
$i_sls_library_image_checkout_list = "<img src='images/sls_info_btn_checkout.gif' border='0'>";
$i_sls_library_image_hold_list = "<img src='images/sls_info_btn_hold.gif' border='0'>";
$i_sls_library_title_image_newadditions = "<img src='images/sls_info_table_new_add.gif' border='0'>";
$i_sls_library_title_image_patron_top = "<img src='images/sls_info_table_top_patron.gif' border='0'>";
$i_sls_library_title_image_item_top = "<img src='images/sls_info_table_top_item.gif' border='0'>";
$i_sls_library_title_image_checkout_list = "<img src='images/sls_info_table_checkout.gif' border='0'>";
$i_sls_library_title_image_hold_list = "<img src='images/sls_info_table_hold.gif' border='0'>";
$i_sls_library_book_title = "Book Title";
$i_sls_library_call_number = "Call Number";
$i_sls_library_from = "From:";
$i_sls_library_to = "To:";
$i_sls_library_reader_name = "Reader Name";
$i_sls_library_reader_class = "Class (Class Number)";
$i_sls_library_read_count = "Number of Readings";
$i_sls_library_borrow_count = "Number of Borrowings";
$i_sls_library_return_date = "Return Date";
$i_sls_library_current_status = "Current Status";

$i_motd_description = "This message will be running on the first page after the user logins.";

$i_QB_AccessGroup = "Groups using Central Question Bank";
$i_QB_ModifySetting = "Click on the group which you want to change the settings (Level, Category and Difficulty).";
$i_QB_NoGroupUsing = "No Groups using Central Question Bank now.";
$i_QB_Category = "Category";
$i_QB_Level = "Level";
$i_QB_Difficulty = "Difficulty";
$i_QB_EngName = "English Name";
$i_QB_ChiName = "Chinese Name";
$i_QB_DisplayOrder = "Display Order";
$i_QB_admin_titlegif = "/images/admin/system_admin/sls_en.gif";
$i_QB_NameCantEmpty = "Either English Name or Chinese Name must be input";
$i_QB_SelectFile = "Select File:";
$i_QB_DownloadSettingSample = "Click here to download sample import file.";
$i_QB_Pending = "Pending";
$i_QB_Approved = "Authorized";
$i_QB_Rejected = "Rejected";
$i_QB_Question = "Question";
$i_QB_Status = "Status";
$i_QB_LangVer = "Language";
$i_QB_EngOnly = "English Only";
$i_QB_ChiOnly = "Chinese Only";
$i_QB_BothLang = "Both Languages available";
$i_QB_LastModified = "Last Modified";
$i_QB_QuestionCode = "Question Code";
$i_QB_DateSubmission = "Date of Submission";
$i_QB_Owner = "Owner";
$i_QB_EngVer = "English";
$i_QB_ChiVer = "Chinese";
$i_QB_Answer = "Answer";
$i_QB_MC = "Multiple Choice";
$i_QB_ShortQ = "Short Question";
$i_QB_FillIn = "Fill In The Blanks";
$i_QB_Matching = "Matching";
$i_QB_TandF = "True & False";
$i_QB_ImportNotes = "Import Description";
$i_QB_QuestionType = "Question Type";
$i_QB_SelectFile = "Select File";
$i_QB_MyQuestion = "My Questions";
$i_QB_SharingArea = "Sharing Area";
$i_QB_FileList = "File List";
$i_QB_DownloadEnglish = "Download English Version";
$i_QB_DownloadChinese = "Download Chinese Version";
$i_QB_All = "All";
$i_QB_ToBulletin = "[Bulletin]";
$i_QB_EditNotes = "Edit information and save";
$i_QB_SubjectAdminLink = "Question Bank";
$i_QB_LangSelect = "Language";
$i_QB_LangSelectEnglish = "English";
$i_QB_LangSelectChinese = "Chinese";
$i_QB_LangSelectBoth = "Bi-Lang";
$i_QB_QuestionAdmin = "Admin";
$i_QB_Alert_Authorize = "Are you sure you want to authorize the checked questions?\\n(The questions will be shown in Sharing Area)";
$i_QB_Alert_Forbid = "Are you sure you want to forbid the checked questions?\\n(The questions will not be shown in Sharing Area)";
$i_QB_Alert_Reject = "Are you sure you want to reject the checked questions?";
$i_QB_Prompt_Reject = "Enter the reason of rejecting or cancelling this action.";
$i_QB_ViewOriginalEnglish = "View Original [English]";
$i_QB_ViewOriginalChinese = "View Original [Chinese]";
$i_QB_MarkAllRead = ExportIcon()."Mark All Read";
$i_QB_MarkAllReadDownload = ExportIcon()."Mark All Read & Downloaded";
$i_QB_ConfirmAllRead = "Are you sure you want to make all questions as read?";
$i_QB_ConfirmAllDownload = "Are you sure you want to make all questions as read and downloaded?";
$i_QB_ConfirmAddToBucket = "Are you sure you want to add selected questions to the pack?";
$i_QB_ConfirmAddAllToBucket = "Are you sure you want to add all questions to the pack?";
$i_QB_ConfirmClearBucket = "Are you sure you want to clear the pack?";
$i_QB_AddToBucket = "Add to pack";
$i_QB_AddAllToBucket = "Add all to pack";
$i_QB_ClearBucket = "Clear pack";
$i_QB_QuestionBucket1 = "Questions in current pack:";
$i_QB_QuestionBucket2 = " question(s) in current pack, you can:";
$i_QB_DownloadAsWord = "Download questions in Microsoft Word format as a zip file.";
$i_QB_DownloadToEclass = "Download to one of the following eClass classroom(s):";
$i_QB_SameServer = "This Server";
$i_QB_AnswerOption = "Answer Options";
$i_QB_ChangeGroupReset = "Changing subject will clear the questions you have added to pack. Do you want to proceed?";
$i_QB_Settings = "Settings";
$i_QB_NoGroupUsing = "No groups are assigned to use Question Bank at the moment.";
$i_QB_AwardPts = "Question Value";
$i_QB_Pts = "point(s)";
$i_QB_DownloadedCount = "Downloads";
$i_QB_Pts_Available = "Points Available";
$i_QB_Pts_InBucket = "Points in pack";
$i_QB_Pts_LeftAfterDownload = "Points available after this download";
$i_QB_Pts_NotEnough = "You do not have enough points for this download";
$i_QB_SelectRemovalTransfer1 = "Please select ";
$i_QB_SelectRemovalTransfer2 = " which questions will be transferred to.";
$i_QB_TypeRemovalDelete = "Delete the Questions";
$i_QB_AllQuestions = "All Questions ";
$i_QB_ViewAll = "View all";

$i_CustomerSupport = "User Support";

$i_Profile_Student_Profile = "Student Profile";
$i_Profile_settings = "Student Profile Settings";
$i_Profile_settings_display = "Display Settings";
$i_Profile_settings_acl = "Teachers Access Settings";
$i_Profile_settings_merit_type_select = "Select Merit/Demerit Type to be used";
$i_Profile_settings_attend_stat_method = "Absence Statistics Setting";
$i_Profile_settings_select_fields_to_disable = "Please select Field(s) to be disabled";
$i_Profile_settings_AttednanceReason = "Attendance Record - Teacher Editable";
$i_Profile_settings_AttednanceReasonNotEditable = "Cannot Edit";
$i_Profile_settings_AttednanceReasonEditable = "Can Be Edit";
# added on 9 Sept
$i_Profile_settings_merit_type_edit = "Edit Merit/Demerit Title";

$i_Profile_Attendance = "Attendance Record";
$i_Profile_Merit = "Merit/Demerit Record";
$i_Profile_Service = "Service Record";
$i_Profile_Activity = "Activity Record";
$i_Profile_Award = "Award Record";
$i_Profile_Assessment = "Student Assessment";
$i_Profile_Files = "Reference Files";
$i_Profile_Summary = "Summary";
$i_Profile_Summary_Has = "has";
$i_Profile_Summary_Conversion = "Conversion";
$i_Profile_Summary_Conversion_To = "to";
$i_Profile_AdminUserMgmt = "Student Profile";
$i_Profile_AdminLevel_Description_Normal = "Allow to add records and modify his/her own records";
$i_Profile_AdminLevel_Description_Full = "Allow to modify all records";
$i_Profile_AdminACL = "Access Control List";
$i_Profile_Admin_NotAllowed = "Not Allowed";
$i_Profile_Admin_NotHaveRight1 = "You are not allowed to do this action.";
$i_Profile_Admin_NotHaveRight2 = "Click here to back student profile section.";

$i_Profile_Attendance_Analysis = "Attendance Analysis";
$i_Profile_Merit_Analysis = "Merit Analysis";

$i_Profile_SelectUser = "Please select a student";
$i_Profile_Absent = "Absent";
$i_Profile_Late = "Late";
$i_Profile_EarlyLeave = "Early Leave";
$i_Profile_Days = "Day(s)";

$i_Profile_From = "From";
$i_Profile_To = "To";
$i_Profile_Today = "Today";
$i_Profile_ThisWeek = "This week";
$i_Profile_ThisMonth = "This month";
$i_Profile_ThisAcademicYear = "This school year";
$i_Discipline_System_Report_thisYear = "This School Year";
$i_Discipline_System_Report_thisSem = "Current Semester";

$i_Profile_SelectStudent = "Select Student To View";
$i_Profile_NotClassTeacher = "You are not Class Teacher. You cannot view students' profiles.";
$i_Profile_NoFamilyRelation = "No family relation has been established.";
$i_Profile_OverallStudentView = "View Current Student Profiles";
$i_Profile_DataLeftStudent = "View Archived Student Profiles";
$i_Profile_OverallStudentViewLeft = "View Temporary Left Student Profiles";
$i_Profile_DataRemovedStudent = "Data of Removed Students";
$i_Profile_SelectClass = "Select Class:";

$i_Profile_Year = "School Year";
$i_Profile_Semester = "Semester";
$i_Profile_SelectSemester = "Select Period";
$i_Profile_SelectReason = "Select Reason";
$i_Profile_ByReason = "View By Reason";
$i_Profile_StudentRecord = "Student Record";
$i_Profile_DataLeftYear = "Year of left";
$i_Profile_ClassOfFinalYear = "Class of final year";

$i_Profile_StudentNotFound = "Student not found";

$i_Profile_StudentName = "Student Name";

$i_Profile_ConfirmRemoveAllArchiveRecord = "Are you sure to delete the record of all archive students?";
$i_Profile_ConfirmRemoveLink = "Press here to remove all records";
$i_Profile_SucceedRemoveMsg = "Removed the record of all archive students";

$i_Profile_SearchMethod = "Search method";
$i_Profile_ClassHistory = "Class History";

$i_Profile_Hide_Frontend = "Not Display in eClass Intranet";
$i_Profile_Hide_PrintPage = "Not Display in Print Page";
//$i_Profile_PersonInCharge = "Person-in-charge"; // dim by kenneth in 20080408
$i_Profile_PersonInCharge = "PIC";

$i_Profile_RecordDate = "Record Date";
$i_Profile_SearchByDate = "Search Option";
$i_Profile_SearchByPeriod = "Search By Period";
$i_Profile_DateType = "Date Type";
$i_Profile_NotSearchByDate = "Don't Use Date";
$i_Profile_ByRecordDate = "By Record Date";
$i_Profile_ByLastModified = "By Last Modified Date";
$i_Profile_SearchText = "Search Text";

$i_Profile_AttendanceStatistic_Method1 = "All day types ($i_DayTypeWholeDay/$i_DayTypeAM/$i_DayTypePM) counted as 1";
$i_Profile_AttendanceStatistic_Method2 = "$i_DayTypeWholeDay counted as 1 while $i_DayTypeAM/$i_DayTypePM counted as 0.5";
$i_Profile_AttendanceStatistic_Description = "";

$i_Profile_Import_NoMatch_Entry[2]="Incorrect Class Name or Class Number(Please use English)";
$i_Profile_Import_NoMatch_Entry[3]="Incorrect WebSAMS Registration Number";
$i_Profile_Import_NoMatch_Entry[1]="Record column doesn't match";
$i_Profile_Import_NoMatch_Entry[4]="Missing Field(s)";
$i_Profile_Import_NoMatch_Entry[5]="School Year or Semester does not exist(Please use English)";

$i_AttendanceTypeArray = array("",$i_Profile_Absent, $i_Profile_Late,$i_Profile_EarlyLeave);
$i_Attendance="Attendance";
$i_Attendance_Others="Others";
$i_Attendance_Standard="Standard";
$i_Attendance_Remark ="Remark";
$i_Attendance_Date = "Date";
$i_Attendance_Type = "Type";
$i_Attendance_attendance_type="Attendance Type";
$i_Attendance_Year = "School Year";
$i_Attendance_AllYear = "All School Years";
$i_Attendance_Semester = "Semester";
$i_Attendance_DayType = "Time Slot";
$i_Attendance_Modified = "Last Modified";
$i_Attendance_Reason = "Reason";
$i_Attendance_ImportSelect = "Select File";
$i_Attendance_DownloadSample = "Click here to download sample";

$i_Attendance_ImportInstruction1 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current year, system will interpret empty as current academic year, which is set in Basic Settings).<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings).<br>
Column 3: Date of the record (YYYY-MM-DD) (You can leave it empty if the record is today, system will interpret empty as Today.<br>
Column 4: Class of the student (You need to type exactly matched case-sensitive class name).<br>
Column 5: Class Number of the student<br>
Column 6: Type of record (A - Absent, L - Late, E - Early leave)<br>
Column 7: Time Slot (WD - Whole Day, AM - Morning, PM - Afternoon).<br>
Column 8: Reason (optional).
";

$i_Attendance_ImportInstruction2 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current year, system will interpret empty as current academic year, which is set in Basic Settings).<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings).<br>
Column 3: Date of the record (YYYY-MM-DD) (You can leave it empty if the record is today, system will interpret empty as Today.<br>
Column 4: WebSamsRegNo (The number starts with # e.g: #123456)<br>
Column 5: Type of record (A - Absent, L - Late, E - Early leave)<br>
Column 6: Time Slot (WD - Whole Day, AM - Morning, PM - Afternoon).<br>
Column 7: Reason (optional).
";

$i_Attendance_DetailedAttendanceRecord = "Detailed Attendance Records";

$i_Activity_ViewStudent = "View Student Records";
$i_Activity_ArchiveRecord = "Archive Current Activity Records (should be executed only at the end of a semester or a school year)";
$i_Activity_Count = "Qty";
$i_ActivityYear = "School Year";
$i_ActivitySemester = "Semester";
$i_ActivityName = "Activity Name";
$i_ActivityRole = "Role";
$i_ActivityPerformance = "Performance";
$i_ActivityLastModified = "Last Modified";
$i_ActivityRemark = "Remark";
$i_ActivityOrganization = "Organisation";
$i_ActivityNoRecord = "No activity records at this moment.";
$i_ActivityArchive = "Archive Records";
$i_ActivityArchiveDescription = "The information (Activity Name, Role, Performance) in ECA groups will be imported using the following School Year and Semester:";
$i_ActivityArchiveWrongData = "If the above information is incorrect, please change the current school year and semester in Settings > Basic Settings, or contact the system administrator.";
$i_ActivityArchiveProceed = "Click here to proceed to import";

$i_Activity_ImportInstruction1 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current school year, system will interpret empty as current school year which is set in Basic Settings).<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings).<br>
Column 3: Class of the student (You need to type exactly matched case-sensitive class name).<br>
Column 4: Class Number of the student.<br>
Column 5: $i_ActivityName.<br>
Column 6: $i_ActivityRole (optional).<br>
Column 7: $i_ActivityPerformance (optional).<br>
Column 8: Remark of the record (optional).<br>
Column 9: Organization holding the activity (optional).
";

$i_Activity_ImportInstruction2 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current school year, system will interpret empty as current school year which is set in Basic Settings).<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings).<br>
Column 3: WebSamsRegNo (The number starts with # e.g: #123456)<br>
Column 4: $i_ActivityName.<br>
Column 5: $i_ActivityRole (optional).<br>
Column 6: $i_ActivityPerformance (optional).<br>
Column 7: Remark of the record (optional).<br>
Column 8: Organization holding the activity (optional).
";

$i_Merit_Date = "Merit Date";
$i_Merit_Type = "Type";
$i_Merit_Qty = "Qty";
$i_Merit_Reason = "Reason";
$i_Merit_Remark = "Remark";
$i_Merit_DateModified = "Last Modified";
$i_Merit_Award = "Award";
$i_Demerit_Punish = "Punish";
$i_Merit_Punishment = "Punishment";
$i_Merit_NoAwardPunishment = "N.A.";
$i_Merit_Merit = "Merit";
$i_Merit_MinorCredit = "Minor Credit";
$i_Merit_MajorCredit = "Major Credit";
$i_Merit_SuperCredit = "Super Credit";
$i_Merit_UltraCredit = "Ultra Credit";
$i_Merit_Warning = "Warning";
$i_Merit_BlackMark = "Black Mark";
$i_Merit_MinorDemerit = "Minor Demerit";
$i_Merit_MajorDemerit = "Major Demerit";
$i_Merit_SuperDemerit = "Super Demerit";
$i_Merit_UltraDemerit = "Ultra Demerit";
$i_Merit_MinorCredit_unicode = "Minor Merit";
$i_Merit_MajorCredit_unicode = "Major Merit";
$i_Merit_SuperCredit_unicode = "Super Merit";
$i_Merit_UltraCredit_unicode = "Ultra Merit";

$i_Merit_TypeArray = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_TypeArrayWithWarning = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_Warning,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

/*
$i_Merit_TypeArray = array(
$i_Merit_UltraCredit,
$i_Merit_SuperCredit,
$i_Merit_MajorCredit,
$i_Merit_MinorCredit,
$i_Merit_Merit,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);
*/
$i_Merit_Short_Merit = "Merit";
$i_Merit_Short_MinorCredit = "Minor";
$i_Merit_Short_MajorCredit = "Major";
$i_Merit_Short_SuperCredit = "Super";
$i_Merit_Short_UltraCredit = "Ultra";
$i_Merit_Short_BlackMark = "Black";
$i_Merit_Short_MinorDemerit = "Minor";
$i_Merit_Short_MajorDemerit = "Major";
$i_Merit_Short_SuperDemerit = "Super";
$i_Merit_Short_UltraDemerit = "Ultra";
$i_Merit_Short_TypeArray = array(
$i_Merit_Short_Merit,
$i_Merit_Short_MinorCredit,
$i_Merit_Short_MajorCredit,
$i_Merit_Short_SuperCredit,
$i_Merit_Short_UltraCredit,
$i_Merit_Short_BlackMark,
$i_Merit_Short_MinorDemerit,
$i_Merit_Short_MajorDemerit,
$i_Merit_Short_SuperDemerit,
$i_Merit_Short_UltraDemerit
);

$i_Merit_QtyMustBeInteger = "$i_Merit_Qty must be a positive integer";
$i_Merit_Unit = "Unit(s)";
$i_Merit_CountByReason = "Stats By Reason";
$i_Merit_Count = "Count(s)";
$i_Merit_DetailedMeritRecord = "Detailed Merit/Demerit Records";

$i_Merit_ImportInstruction1 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current year, system will interpret empty as current academic year, which is set in Basic Settings)<span style=\"color:red;\">*Please use English</span>.<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings)<span style=\"color:red;\">*Please use English</span>.<br>
Column 3: Date of the record (YYYY-MM-DD) (YYYY-MM-DD) (You can leave it empty if the record is today, system will interpret empty as Today.<br>
Column 4: Class of the student (You need to type exactly matched case-sensitive class name)<span style=\"color:red;\">*Please use English</span>.<br>
Column 5: Class Number of the student<br>
Column 6: Type of record (C1 - $i_Merit_Merit, C2 - $i_Merit_MinorCredit, C3 - $i_Merit_MajorCredit,
C4 - $i_Merit_SuperCredit, C5 - $i_Merit_UltraCredit,
D1 - $i_Merit_BlackMark, D2 - $i_Merit_MinorDemerit, D3 - $i_Merit_MajorDemerit, D4 - $i_Merit_SuperDemerit,
D5 - $i_Merit_UltraDemerit)<br>
Column 7: Number of Units (if empty, system will determine as 1)<br>
Column 8: Reason of the record (optional).<br>
Column 9: Remark of the record (optional).<br>
Column 10: Login ID of Person In Charge (Optional).
";

$i_Merit_ImportInstruction2 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current year, system will interpret empty as current academic year, which is set in Basic Settings)<span style=\"color:red;\">*Please use English</span>.<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings)<span style=\"color:red;\">*Please use English</span>.<br>
Column 3: Date of the record (YYYY-MM-DD) (YYYY-MM-DD) (You can leave it empty if the record is today, system will interpret empty as Today.<br>
Column 4: WebSamsRegNo (The number starts with # e.g: #123456)<br>
Column 5: Type of record (C1 - $i_Merit_Merit, C2 - $i_Merit_MinorCredit, C3 - $i_Merit_MajorCredit,
C4 - $i_Merit_SuperCredit, C5 - $i_Merit_UltraCredit,
D1 - $i_Merit_BlackMark, D2 - $i_Merit_MinorDemerit, D3 - $i_Merit_MajorDemerit, D4 - $i_Merit_SuperDemerit,
D5 - $i_Merit_UltraDemerit)<br>
Column 6: Number of Units (if empty, system will determine as 1)<br>
Column 7: Reason of the record (optional).<br>
Column 8: Remark of the record (optional).<br>
Column 9: Login ID of Person In Charge (Optional).
";

$i_Service_Count = "Qty";
$i_ServiceYear = "School Year";
$i_ServiceSemester = "Semester";
$i_ServiceDate = "Date";
$i_ServiceName = "Service Name";
$i_ServiceRole = "Role";
$i_ServiceOrganization = "Organisation";
$i_ServicePerformance = "Performance";
$i_ServiceLastModified = "Last Modified";
$i_ServiceRemark = "Remarks";
$i_ServiceNoRecord = "No service records at this moment.";
$i_ServiceDateCanBeIgnored = "(Can be empty if this is a service for a whole semester)";


$i_Service_ImportInstruction1 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current year, system will interpret empty as current academic year, which is set in Basic Settings).<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings).<br>
Column 3: Date of the record (YYYY-MM-DD) (optional).<br>
Column 4: Class of the student (You need to type exactly matched case-sensitive class name).<br>
Column 5: Class Number of the student.<br>
Column 6: $i_ServiceName.<br>
Column 7: $i_ServiceRole (optional).<br>
Column 8: $i_ServicePerformance (optional).<br>
Column 9: Remarks of the record (optional).<br>
Column 10: Organization holding the service (optional).
";

$i_Service_ImportInstruction2 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current year, system will interpret empty as current academic year, which is set in Basic Settings).<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings).<br>
Column 3: Date of the record (YYYY-MM-DD) (optional).<br>
Column 4: WebSamsRegNo (The number starts with # e.g: #123456)<br>
Column 5: $i_ServiceName.<br>
Column 6: $i_ServiceRole (optional).<br>
Column 7: $i_ServicePerformance (optional).<br>
Column 8: Remarks of the record (optional).<br>
Column 9: Organization holding the service (optional).
";

$i_Award_Count = "Qty";
$i_AwardYear = "School Year";
$i_AwardSemester = "Semester";
$i_AwardDate = "Date";
$i_AwardName = "Award";
$i_AwardRole = "Role";
$i_AwardOrganization = "Organization";
$i_AwardSubjectArea = "Subject Area";
$i_AwardPerformance = "Performance";
$i_AwardLastModified = "Last Modified";
$i_AwardRemark = "Remarks";
$i_AwardNoRecord = "No Award records at this moment.";
$i_AwardDateCanBeIgnored = "(Can be empty if this is a Award for a semester)";

$i_Award_ImportInstruction1 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current year, system will interpret empty as current academic year, which is set in Basic Settings).<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings).<br>
Column 3: Date of the record (optional).<br>
Column 4: Class of the student (You need to type exactly matched case-sensitive class name).<br>
Column 5: Class Number of the student.<br>
Column 6: $i_AwardName.<br>
Column 7: Remarks (optional).<br>
Column 8: $i_AwardOrganization (optional).<br>
Column 9: $i_AwardSubjectArea (optional).
";

$i_Award_ImportInstruction2 = "<b><u>Description of import file:</u></b><br>
Column 1: The School Year of the record (You can leave it empty if the record is current year, system will interpret empty as current academic year, which is set in Basic Settings).<br>
Column 2: The Semester of the record (You can leave it empty if the record is current semester, system will interpret empty as current semester, which is set in Basic Settings).<br>
Column 3: Date of the record (optional).<br>
Column 4: WebSamsRegNo (The number starts with # e.g: #123456)<br>
Column 5: $i_AwardName.<br>
Column 6: Remarks (optional).<br>
Column 7: $i_AwardOrganization (optional).<br>
Column 8: $i_AwardSubjectArea (optional).
";
$i_MyAccount_StudentID = "Student Card";
$i_MyAccount_SchoolRecord = "School Record";
$i_MyAccount_Library = "Library Record";
$i_MyAccount_Usage = "Login Record";
$i_MyAccount_CampusMail = "CampusMail";
$i_MyAccount_WebMail = "WebMail";
$i_MyAccount_FileCabinet = "File Cabinet";

$i_Teaching_TeacherName = "Teacher Name";
$i_Teaching_ClassTeacher = "Class Teacher";
$i_Teaching_SubjectTeacher = "Subject Teacher";
$i_Teaching_SubjectTaught = "No. of Teaching Subjects";
$i_Teaching_NotClassTeacher = "Not Class Teacher";
$i_Teaching_TeachingArrangement = "Teaching Appointment";
$i_Teaching_ClassSubjects = "Teaching Subjects/Class";
$i_Teaching_NotApplicable = "Not Applicable";
$i_Teaching_Class = "Class";
$i_Teaching_Subject = "Subject";
$i_Teaching_ImportInstruction = "<b><u>File Description:</u></b><br>
1st Column : Teacher's Login Name (e.g. tmchan ).<br>
2nd Column : Class teacher's class (e.g. 1A. If not class teacher, please leave this field blank.).<br>
3rd Column : Teaching class (e.g. 1A).<br>
4th Column : Teaching subject (e.g. English).<br>
5th and other columns : Teaching class and corresponding subject (must be in pairs)<br>
Class names and subject names are required to be exactly matched (case-sensitive)<br>
<br>
<b>Note that current settings will be replaced.</b><br>";
$i_Teaching_ClassList = "Class List";
$i_Teaching_SubjectList = "Subject List";
$i_Teaching_ImportFailed = "The following records cannot be imported.";

$i_Form_Name = "Form Name";
$i_Form_Description = "Description";
$i_Form_Type = "Type";
$i_Form_LastModified = "Last Modified";
$i_Form_Templates = "Templates";
$i_Form_Form = "Forms";
$i_Form_Suspended = "Suspended";
$i_Form_Approved = "Approved";
$i_Form_Assessment = "Student Assessment";
$i_Form_Activity = "Activity Enrolment";
$i_Form_ConstructForm = "Construct Form";
$i_Form_AssessmentDone = "Assessment Done";
$i_Form_FormEditNotAllow = "The form contents cannot be edited after the form has been used. Editing this form will make the information of the filled form incompatible. You may change the property of the form or you may suspend the form and create a new form (with a different name).";
$i_Form_FormNameMustBeUnique = "(Must be unique)";
$i_Form_RemovalAlert = "Removing these forms will remove the information related to these forms also. Are you sure you want to remove? <BR> (If you want to retain the information, you can suspend the forms instead.)";
$i_Form_answer_sheet="Online Form Construction";
$i_Form_answersheet_template="Templates";
$i_Form_answersheet_header="Topic / Title";
$i_Form_answersheet_no="Number of Questions";
$i_Form_answersheet_type="Method";
$i_Form_answersheet_maxno="Questions exceeded limit: English letter 'a - z';  Roman 'i - x'!";
$i_Form_no_options_for = "No need to fill number of options!";
$i_Form_pls_specify_type = "Please select a method!";
$i_Form_pls_fill_in = "Please fill in content!";
$i_Form_chg_title = "change topic/title:";
$i_Form_chg_template = "Are you sure you want to change template?";
$i_Form_answersheet_tf="True & False";
$i_Form_answersheet_mc="MC (Single)";
$i_Form_answersheet_mo="MC (Multiple)";
$i_Form_answersheet_sq1="Text (short)";
$i_Form_answersheet_sq2="Text (long)";
$i_Form_answersheet_option="Options";
$i_Form_answersheet_not_applicable="Not applicable";
$i_Form_ShowHide = "Show/Hide";
$i_Form_ShowDetails = "Show details";
$i_Form_HideDetails = "Hide details";
$i_Form_TotalNo = "Total Number of Received";


$i_Assessment_Year = "Year";
$i_Assessment_Semester = "Semester";
$i_Assessment_Date = "Assessment Date";
$i_Assessment_By = "Assessed By";
$i_Assessment_LastModified = "Last Modified";
$i_Assessment_SelectForm = "Select Form";
$i_Assessment_PleaseSelectAForm = "Please select a form for assessment";
$i_Assessment_FillForm = "Fill Form";
$i_Assessment_PleaseFillAForm = "Please fill in the details and click Open Form to open the assessment form";
$i_Assessment_Open = "Open Form";
$i_Assessment_UserSelectAdmin = "It will be marked as $i_general_sysadmin, if you do not choose any teacher/staff.";
$i_Assesyment_OriginalAssessBy = "Original assess by";

$i_ReferenceFiles_Qty = "Number of files";
$i_ReferenceFiles_Title = "Title";
$i_ReferenceFiles_FileType = "File Type";
$i_ReferenceFiles_NewFile = "New File";
$i_ReferenceFiles_SelectFile = "Select File";
$i_ReferenceFiles_Description = "Description";
$i_ReferenceFiles_FileName = "File Name";

$i_PrinterFriendlyPage = "Printer-friendly Page";
$i_PrinterFriendly_StudentName = "Student Name";

$i_UsageStartTime = "Login Time";
$i_UsageEndTime = "Logout Time";
$i_UsageDuration = "Duration";
$i_UsageHost = "User Address";
$i_UsageToday = "Today";
$i_UsageLastWeek = "Last Week";
$i_UsageLast2Week = "Last 2 Weeks";
$i_UsageLastMonth = "Last Month";
$i_UsageAll = "All";
$i_Usage_Hour = "hr.";
$i_Usage_Min = "min.";
$i_Usage_Sec = "sec.";
$i_Usage_RemoveRecords = "Remove Records";
$i_Usage_Remove1 = "Remove records on ";
$i_Usage_Remove2 = " or before.";
$i_Usage_RemoveConfirm = "Are you sure you want to remove the record(s)?";

$i_ClubsEnrollment = "Club Enrolment";
$i_ClubsEnrollment_BasicSettings = "Basic Settings";
$i_ClubsEnrollment_StudentRequirement = "Student Requirement Settings";
$i_ClubsEnrollment_QuotaSettings = "Group and Quota Settings";
$i_ClubsEnrollment_Mode = "Mode";
$i_ClubsEnrollment_AppStart = "Start Date of Student Application";
$i_ClubsEnrollment_AppEnd = "End Date of Student Application";
$i_ClubsEnrollment_GpStart = "Start Date of Group Approval";
$i_ClubsEnrollment_GpEnd = "End Date of Group Approval";
$i_ClubsEnrollment_ApplicationDescription = "Instruction to Applicants";
$i_ClubsEnrollment_DefaultMin = "Default min. no. of groups to be selected by each student";
$i_ClubsEnrollment_DefaultMax = "Default max. no. of groups to be selected by each student";
$i_ClubsEnrollment_TieBreakRule = "Priority in case of Supply less than Demand";
$i_ClubsEnrollment_ClearRecord = "Clear all enrolment records (For starting a new year)";
$i_ClubsEnrollment_Disable = "Disable";
$i_ClubsEnrollment_Simple = "Simple";
$i_ClubsEnrollment_Advanced = "Advanced";
$i_ClubsEnrollment_NoLimit = "No Limit";
$i_ClubsEnrollment_Random = "Random";
$i_ClubsEnrollment_AppTime = "First-come-first-served";
$i_ClubsEnrollment_DefaultMaxMinWrong = "Default min. no. should not be less than default max. no.";
$i_ClubsEnrollment_EnrollMaxWrong = "Max. no. of groups to be enrolled should not be less than default max. no. and greater than default min. no.";
$i_ClubsEnrollment_AppEndGpStart = "Enrolment Period and Approve Period cannot be overlapped.";
$i_ClubsEnrollment_AppStartAppEnd = "Invalid enrolment Period.";
$i_ClubsEnrollment_GpStartGpEnd = "Invalid Approve Period.";
$i_ClubsEnrollment_UseDefault = "Use Default?";
$i_ClubsEnrollment_Min = "Min";
$i_ClubsEnrollment_Max = "Max";
$i_ClubsEnrollment_GroupName = "Club Name";
$i_ClubsEnrollment_AllowEnrol = "Use Enrolment System?";
$i_ClubsEnrollment_Priority = "Priority";
# added on 10 Sept 2008
$i_ClubsEnrollment_OnlineRegistration = "Online Registration";
$i_ClubsEnrollment_ClubName = "Club Name";

$i_ClubsEnrollment_Quota = "Member Quota";
$i_ClubsEnrollment_ApplicationPeriod = "Application Period";
$i_ClubsEnrollment_From = "From";
$i_ClubsEnrollment_To = "To";
$i_ClubsEnrollment_MinForStudent = "Minimum number of club(s) you need to apply";
$i_ClubsEnrollment_MaxForOwnWill = "Maximum number of club(s) you want to participate";
$i_ClubsEnrollment_Priority = "Priority";
$i_ClubsEnrollment_ClubsForSelection = "Clubs Available";
$i_ClubsEnrollment_Status = "Status";
$i_ClubsEnrollment_StatusWaiting = "Waiting";
$i_ClubsEnrollment_StatusRejected = "Rejected";
$i_ClubsEnrollment_StatusApproved = "Approved";
$i_ClubsEnrollment_Alert_NotEnough = "You have not selected enough choice.";
$i_ClubsEnrollment_Alert_Reject = "Are you sure you want to reject?";
$i_ClubsEnrollment_LastSubmissionTime = "Last Submission Time";
$i_ClubsEnrollment_EnrollmentList = "Enrolment List";
$i_ClubsEnrollment_NotSubmitted = "Not yet submitted";
$i_ClubsEnrollment_GroupQuota1 = "Max. no. of Members";
$i_ClubsEnrollment_ActivityQuota1 = "Max. no. of Participants";
$i_ClubsEnrollment_GroupQuotaNo = "This club has no limit on number of members";
$i_ClubsEnrollment_GroupApprovedCount = "Number of Students Approved";
$i_ClubsEnrollment_SpaceLeft = "Quota Left";
$i_ClubsEnrollment_NoQuota = "No Limit";
$i_ClubsEnrollment_NumOfStudent = "No. of Students";
$i_ClubsEnrollment_NumOfReceived = "No. of Applications Received";
$i_ClubsEnrollment_NumOfNotFulfilSchool = "No. of Students NOT satisfying school requirements";
$i_ClubsEnrollment_NumOfNotFulfilOwn = "No. of Students satisfying school requirements but not own requirements";
$i_ClubsEnrollment_NumOfSatisfied = "No. of Students satisfying own requirements";
$i_ClubsEnrollment_NotHandinList = "Not Submitted Student List";
$i_ClubsEnrollment_HeadingNotHandinList = "The following students have not submitted their enrolment applications";
$i_ClubsEnrollment_HeadingNotFulfilSchool = "The following students have not satisfied the school requirements";
$i_ClubsEnrollment_HeadingNotFulfilOwn = "The following students have satisfied the school requirements, but have not satisfied their own.";
$i_ClubsEnrollment_HeadingSatisfied = "The following students have satisfied their own requirements.";
$i_ClubsEnrollment_NameList = "[Student List]";
$i_ClubsEnrollment_AllMemberList = "All Group Member List";
$i_ClubsEnrollment_NoMember = "No Approved Member Now";
$i_ClubsEnrollment_CurrentSize = "Number of Members";
$i_ClubsEnrollment_GoLottery = "Perform Lottery";
$i_ClubsEnrollment_Proceed2NextRound = "Proceed to Next Round";
$i_ClubsEnrollment_FinalConfirm = "Confirm Whole Enrolment Finished";
$i_ClubsEnrollment_Confirm_Clear2Next = "Are you sure you want to clear the applications to proceed to next round? (Please confirm that you have updated the enrolment period in settings.)";
$i_ClubsEnrollment_Confirm_GoLottery = "Are you sure you want to process random drawing?";
$i_ClubsEnrollment_Confirm_FinalConfirm = "Are you sure you want to confirm that the whole enrolment process has been finished?";
$i_ClubsEnrollment_Form_Updated = "Your enrolment form is updated and submitted.";
$i_ClubsEnrollment_LotteryInProcess = "Drawing is in progress. Please wait for a few seconds and do not close this window.";
$i_ClubsEnrollment_LotteryFinished = "Drawing is finished. Please close this window.";
$i_ClubsEnrollment_Mail1 = "School Clubs enrolment has proceeded to next round. The current result of your applicaion is:";
$i_ClubsEnrollment_MailChoice = "Choice";
$i_ClubsEnrollment_Mail2 = "For the records that has not yet been approved, you can change your application within the period ";
$i_ClubsEnrollment_Mail3 = "to ";
$i_ClubsEnrollment_Mail4 = "Please go to first page and click ECA Enrolment to check the result and enter your new choices (if any).";
$i_ClubsEnrollment_MailSubject = "Clubs Enrolment Proceeded to Next Round";
$i_ClubsEnrollment_MailConfirm1 = "Clubs Enrolment Process has been completed . The result of your application:";
$i_ClubsEnrollment_MailConfirm2 = "You can go Group Info to find the information of the clubs you have been approved.";
$i_ClubsEnrollment_MailConfirm3 = "Should you have any problems, please contact System Admin or the teachers responsible for ECA Enrolment.";
$i_ClubsEnrollment_MailConfirmSubject = "Clubs Enrolment Result";
//$i_ClubsEnrollment_Warning_NotUse = "If you choose any Class Level not use this enrolment (disable), the applications submitted by those class levels' students will be removed.";
$i_ClubsEnrollment_Warning_NotUse = "If any Class Level is set as 'Do not use Enrolment', the applicant records of that level will be deleted.";


$i_Survey = "Survey";
$i_Survey_Poster = "Creator";
$i_Survey_Progress = "In progress";
$i_Survey_Finished = "Finished";
$i_Survey_PublicInstruction = "If you would like to show the survey to whole school, please leave the Target group(s) empty.";
$i_Survey_PleaseFill = "Please fill in the following form and click submit. You are not allowed to change your answer after submission.";
$i_Survey_Result = "Your answer has been submitted. This is a copy of your answer. You can print this page or close this window.";
$i_Survey_Overall = "Overall Results";
$i_Survey_Detail = "View Detailed Results";
$i_Survey_perGroup = "View Group Results";
$i_Survey_perIdentity = "View Identity Results (Staff,Student or Parent)";
$i_Survey_perClass = "View Class Results";
$i_Survey_perUser = "View Survey Results";
$i_Survey_NoSurveyForCriteria = "No Survey Form has been submitted for this criterion";
$i_Survey_RemoveConfirm = "Removing these surveys will remove the information submitted. Are you sure you want to remove?";
$i_Survey_NewSurvey = " piece(s) of unfilled survey";
$i_Survey_NoSurveyAvailable = "There is no survey for you at this moment.";
$i_Survey_SurveyConstruction = "Construct Survey";
$i_Survey_FillSurvey = "Fill Survey";
$i_Survey_FilledSurvey = "Survey Filled";
$i_Survey_ClassList = "Choose Class";
$i_Survey_GroupList = "Choose Group";
$i_Survey_IdentityList = "Choose Identity";
$i_Survey_UserList = "Choose User";
$i_Survey_PleaseSelectType = "Please Select One Item";
$i_Survey_UnfillList = "Unfill User List";
$i_Survey_AllFilled = "There is no user not yet filled this survey.";
$i_Survey_Anonymous = "Anonymous";
$i_Survey_Anonymous_Description = "Not allowed to view single survey answer and filled users list.";
$i_Survey_AllRequire2Fill = "All questions are required to be answered";
$i_Survey_alert_PleaseFillAllAnswer = "please answer all the questions";

$i_Notice_ElectronicNotice2 = "E-Notice";
$i_Notice_ElectronicNotice = "E-Notice";
$i_Notice_ElectronicNoticeSettings = "E-Notice Settings";
$i_Notice_ElectronicNotice_History = "Past Notice";
$i_Notice_ElectronicNotice_Current = "Current Notice";
$i_Notice_ElectronicNotice_All = "All School Notices";
$i_Notice_Disable = "Disable E-Notice";
$i_Notice_Setting_FullControlGroup = "Advanced Control Group (allow removing notices and editing replies for parents).";
$i_Notice_Setting_NormalControlGroup = "Normal Control Group (allow issuing notices and viewing results).";
# 20090306 yat
$i_Notice_Setting_DisciplineGroup = "Discipline Notice Batch Print Group";

$i_Notice_Setting_DisableClassTeacher = "Disable Class teacher right to edit replies for parents.";
$i_Notice_Setting_AllHaveRight = "All staff can issue notices.";
$i_Notice_Setting_DefaultNumDays = "Default no. of days for returning notice.";
$i_Notice_Setting_AdminGroupOnly = "Admin Groups Only";
$i_Notice_Setting_ParentStudentCanViewAll = "Allow all parents/students to view all notices.";
$i_Notice_New = "New Notice";
$i_Notice_Edit = "Edit Notice";
$i_Notice_NoticeNumber = "Notice Number";
$i_Notice_Title = "Notice Title";
$i_Notice_Description = "Notice Content";
$i_Notice_DateStart = "Issue Date";
$i_Notice_DateEnd = "Deadline";
$i_Notice_Issuer = "Issued By";
$i_Notice_RecipientType = "Audience";
$i_Notice_RecipientTypeAllStudents = "Whole School";
$i_Notice_RecipientTypeLevel = "Some Levels Only";
$i_Notice_RecipientTypeClass = "Some Classes Only";
$i_Notice_RecipientTypeIndividual = "Applicable students";
$i_Notice_RecipientLevel = "Levels";
$i_Notice_RecipientClass = "Classes";
$i_Notice_RecipientIndividual = "Students/Groups";
$i_Notice_ReplyContent = "Edit Reply Slip";
$i_Notice_Attachment = "Attachment";
$i_Notice_FromTemplate = "Load from Template";
$i_Notice_NotUseTemplate = "Not Use Template";
$i_Notice_Type = "Type";
$i_Notice_StatusPublished = "Distributed";
$i_Notice_StatusSuspended = "Suspended";
$i_Notice_StatusTemplate = "Template Only";
$i_Notice_ViewOwnClass = "View Own Class";
$i_Notice_Signed = "Signed";
$i_Notice_Total = "Total";
$i_Notice_ViewResult = "View Result";
$i_Notice_ReplySlip = "Reply Slip";
$i_Notice_ReplySlip_Signed = "The following is the reply slip signed. You can print it or close the window.";
$i_Notice_StudentName = "Student Name";
$i_Notice_OpenSign = "Open";
$i_Notice_Sign = "Sign";
$i_Notice_SignStatus = "Status";
$i_Notice_Unsigned = "Unsigned";
$i_Notice_Signer = "Signed by";
$i_Notice_Editor = "Edited by";
$i_Notice_Signer = "Signed By";
$i_Notice_SignerNoColon = "Signed By";
$i_Notice_SignedAt = "Signed At";
$i_Notice_At = "at";
$i_Notice_SignInstruction = "Please fill in the above reply slip and click Sign button to sign this notice.";
$i_Notice_SignInstruction2 = "You can change your reply by clicking Sign button again.";
$i_Notice_NoRecord = "No notice at this moment.";
$i_Notice_MyNotice = "Issued Notice";
$i_Notice_AllNotice = "All Notices";
$i_Notice_RemovalWarning = "Warning: Removing this notice will remove the replies of parents together. Do you want to proceed?";
$i_Notice_TemplateNotes = "(Only <font color=green>$i_Notice_Title</font>, <font color=green>$i_Notice_Description</font>, and <font color=green>$i_Notice_ReplySlip</font> will be used in template.)";
$i_Notice_Alert_DateInvalid = "Do not set the Deadline before the Issue Date";
$i_Notice_ResultForEachClass = "Results for classes";
$i_Notice_TableViewReply = "View Replies";
$i_Notice_ViewStat = "View Statistics";
$i_Notice_NoReplyAtThisMoment = "There is no signed reply at this moment.";
$i_Notice_NumberOfSigned = "No. of signed Replies";
$i_Notice_Option = "Option";
$i_Notice_NotForThisClass = "This notice is not for this class.";
$i_Notice_NoStudent = "This notice has not been issued to any students.";
$i_Notice_AllStudents = "All Students";
$i_Notice_Alert_Sign = "Your reply slip will be submitted. Are you sure to submit the reply slip?";
$i_Notice_CurrentList = "Current Notice List";
$i_Notice_ListIncludingSuspend = "Including Distributed and Suspended";
$i_Notice_SendEmailToParent = "Send Email to notify parents (For $i_Notice_StatusPublished Type ONLY)";

# 20090227 - eNotice with Discipline Notice enhancement
$eNotice['SchoolNotice'] = "School Notice";
$eNotice['DisciplineNotice'] = "Discipline Notice";
$eNotice['Period_Start'] = "Date From";
$eNotice['Period_End'] = "To";

#20090611 eDisciplinev12 template setting
$eNotice['ReplySlipType'] = "Reply Slip Type";
$eNotice['QuestionBase'] = "Question Base";
$eNotice['ContentBase'] = "Content Base";

$i_Notice_Record_From_Other_Module = 4;
$i_Notice_Discipline_ModuleID = 1;
$i_Notice_Inventory_ModuleID = 2;
$i_Notice_ModuleID = array(1=>"eDiscipline",2=>"eInventory");
$i_Notice_Module = "Module";
$i_LinuxAccountQuotaSetting = "Online Storage Quota Settings";
$i_LinuxAccount_SetDefaultQuota = "Set Default Storage Quota";
$i_LinuxAccount_SetUserQuota = "Set Individual User Storage Quota";
$i_LinuxAccount_Quota_Description = "This storage quota includes: ";
$i_LinuxAccount_Webmail = "Webmail";
$i_LinuxAccount_Campusmail = "External Email Temporary Storage (When user views inbox, emails will be downloaded and do not occupied this storage).";
$i_LinuxAccount_PersonalFile = "Personal File Cabinet";
$i_LinuxAccount_Quota = "Quota";
$i_LinuxAccount_NotExist = "Account name input does not exist. Please check again. Account name is case-sensitive.";
$i_LinuxAccount_AccountName = "Account Name";
$i_LinuxAccount_Alert_NameMissing = "Please enter account name";
$i_LinuxAccount_Alert_QuotaMissing = "Please enter quota";
$i_LinuxAccount_DisplayQuota = "View User Storage Quota";
$i_LinuxAccount_SelectUserType = "Select User Type";
$i_LinuxAccount_SelectGroup = "Select Group";
$i_LinuxAccount_UsedQuota = "Used Quota";
$i_LinuxAccount_CurrentQuota = "Current Quota";
$i_LinuxAccount_SetGroupQuota = "Set Storage Quota By Identity/Group";
$i_LinuxAccount_IncreaseOnly = "Increase ONLY";
$i_LinuxAccount_Reset = "Reset ALL this type/group users to this quota";
$i_LinuxAccount_UserSetNote = "Please note that setting quota to 0 means no limit.";
$i_LinuxAccount_GroupSetNote = "Please note that setting quota to 0 means no limit. <br>Please be patient for this action if the number of users is large.";
$i_LinuxAccount_NoAccount = "No Account";
$i_LinuxAccount_NoLimit = "No Limit";
$i_LinuxAccount_Webmail_QuotaSetting = "Webmail Temp Storage";
$i_LinuxAccount_Folder_QuotaSetting = "iFolder Storage";
$i_LinuxAccount_PersonalFile_Description = "This settings will affect user quota in file server. Please use this carefully.";
$i_LinuxAccount_iMail_Description = "This storage space is only used for storing external Internet e-mails temporary.";
$i_Linux_Description_ImportUser = "File/Mail Server accounts will be opened for new eClass users ONLY. For existing users, please go to <b>Function Settings &gt; iFolder Storage </b>to configure.";
$i_LinuxAccount_BatchProcess = "Batch Process";
$i_LinuxAccount_NewCreation = "New OS Account will be created";
$i_LinuxAccount_prompt_TypeRestricted = "This account type is not allowed to have this service.";

$i_Files_ConnectionFailed_Remote = "Connection to the file server failed. Please check if your password is consistent.";
$i_Files_ConnectionFailed_Local = "Connection to the file server failed. Please change your password to Synchronize the password.";
$i_Files_ConnectionFailed = "Connection to the file server failed. Please change your password to Synchronize the password. If the error still occurs, please contact the System Administrator";
$i_Files_CurrentDirectory = "Current Directory";
$i_Files_Type = "Type";
$i_Files_Size = "Size";
$i_Files_Date = "Last Modified";
$i_Files_Name = "File Name";
$i_Files_NewFolder = "New Folder";
$i_Files_Upload = "Upload";
$i_Files_Files = "file(s)";
$i_Files_Move = "Move";
$i_Files_Delete = "Remove";
$i_Files_Rename = "Rename";
$i_Files_ClickToDownload = "Click to download file";
$i_Files_ThisIsPublic = "(This is a public folder for web page)";
$i_Files_UploadTarget = "Upload file(s) to : ";
$i_Files_To = "To";
$i_Files_OpenAccount = "Allow user to use iFolder (Create user account)";
$i_Files_LinkFTPAccount = "Allow user to link with file server via iFolder (File Server accounts should be already exist)";
$i_Files_LinkToAero = "Allow user using Personal File Folder, and connecting to AeroDrive. (AeroDrive accounts should be created in AeroDrive System)";
$i_Files_UseIFolder = "Allow to use iFolder";
$i_Files_Description_Unlink = "If this is unchecked, user cannot see iFolder icon. However, the OS account still exists";
$i_Files_Description_Link = "If this is checked, OS account will be created.";
#$i_Files_AllowUserUseFTPAccount ="Allow user using Personal File Folder. (Valid only if this user has Email/FTP/Linux account already)";
$i_Files_DefaultQuotaSameServer = "If mail server and file server is the same machine, please change in iMail Settings. (This quota setting will be ignored)";
$i_Files_Linked = "iFolder Linked";
$i_Files_LinkIfolder = "Link to iFolder";
$i_Files_BatchOption_OpenAccount = "Open accounts for listed users and allow them use iFolder with Quota ";
$i_Files_BatchOption_UnlinkAccount = "Not allow listed users to access iFolder via eClass <font color=red>(Accounts on File Server will not be affected)</font>";
$i_Files_BatchOption_RemoveAccount = "Remove all listed users' account on File Server <font color=red>(This may affect mail accounts if mail server is the same server as file server)</font>";
$i_Files_alert_RemoveAccount = "Are you sure to remove this file server account? (This may affect mail account if they are the same server)";
$i_Files_CheckQuota = "Check Quota";
$i_Files_msg_FailedToWrite = "Files uploaded failed. Probable reasons:<br>
1. Disk quota exceeded. Please remove some unused files.<br>
2. The names of your file are not accepted by this system. Please try to rename them first (to normal English or UTF-8 Chinese).<br>
If you still have problems, please contact System Administrator.<br>
";
$i_Files_ClickHereToBrowse = "Click here to return File list page.";

$i_CampusTV_BasicSettings = "Basic Settings";
$i_CampusTV_LiveBroadcastManagement = "Live Broadcast Management";
$i_CampusTV_ChannelManagement = "Channel Management";
$i_CampusTV_VideoManagement = "Video Management";
$i_CampusTV_BulletinManagement = "Bulletin Management";
$i_CampusTV_PollingManagement = "Poll Management";
$i_CampusTV_AccessByPublic = "Enable Public Access (No login is required)";
$i_CampusTV_DisableBulletin = "Disable Bulletin";
$i_CampusTV_DisablePolling = "Disable Poll";
$i_CampusTV_LiveBroadcast = "Live Broadcast";
$i_CampusTV_LiveURL = "Live Broadcast URL";
$i_CampusTV_ChannelName = "Channel Name";
$i_CampusTV_ClipName = "Video Name";
$i_CampusTV_Recommended = "Highlights";
$i_CampusTV_RecommendedStatus = "Yes";
$i_CampusTV_Type_Link = "Link Type";
$i_CampusTV_Type_File = "File Type";
$i_CampusTV_MovieURL = "Video Link";
$i_CampusTV_InputType = "Input type";
$i_CampusTV_SelectMovieFile = "Select Video File";
$i_CampusTV_MovieProvider = "Provider";
$i_CampusTV_Play = "Play";
$i_CampusTV_Alert_Recommend = "Are you sure you want to recommend selected video(s)?";
$i_CampusTV_Alert_CancelRecommend = "Are you sure you want to remove the selected movie clip(s) from the recommendation list?";
$i_CampusTV_LiveProgrammeList = "Schedule"; #"Programme List";
$i_CampusTV_Time = "Time";
$i_CampusTV_Programme = "Program";
$i_CampusTV_SelectClip = "Select a Video";
$i_CampusTV_SelectChannel = "Select a Channel";
$i_CampusTV_PollingName = "Poll Name";
$i_CampusTV_Reference = "Reference";
$i_CampusTV_NumClips = "Number of Video";
$i_CampusTV_PollingSelectClips = "Select Video for Poll";
$i_CampusTV_PollingCandidate = "Candidate";
$i_CampusTV_ViewProgrammer = "View Program";
$i_CampusTV_RecommendedList = "Program Highlights";
$i_CampusTV_MostChannel = "Top 10 Popular Channels";
$i_CampusTV_MostClip = "Top 10 Popular Programs";
$i_CampusTV_Bulletin = "Video Bulletin";
$i_CampusTV_Upload = "Video Upload";
$i_CampusTV_Polling = "Video Poll";
$i_CampusTV_UploadSuccessful =  "<font color=#FFFFFF>Video File Uploaded Successfully.</font>";
$i_CampusTV_NoPolling = "There is no poll at the moment.";
$i_CampusTV_NoVote = "No one has voted";
$i_CampusTV_ChannelList = "Channels"; #"Channel List";
$i_CampusTV_PastPoll = "Past Polls";
$i_CampusTV_CurrentPoll = "Current Polls";
$i_CampusTV_SecureNeeded = "Login to eClass IP required";
$i_CampusTV_URL = "URL";
$i_CampusTV_Guide_Please_Select_Channel="Please select a channel";
$i_CampusTV_Guide_Please_Select_Movie="Please select a movie";
$i_CampusTV_Alert_No_Movies_Available="No movies available";
$i_CampusTV_Alert_No_Channels_Available="No Channels available";
$i_CampusTV_Live_Portal_Config = "Settings in Portal Page";
$i_CampusTV_Live_Portal_display = "Show";
$i_CampusTV_Live_Portal_size = "Dimension";
$i_CampusTV_Live_Portal_pixel = "px";
$i_CampusTV_Live_Portal_auto = "Auto Play";
$i_CampusTV_Live_Portal_size_width_warn = "The video width must be between 1 to 320!";
$i_CampusTV_Live_Portal_size_height_warn = "The video height must be greater than 1!";

$i_Mail_OpenWebmailAccount = "Open webmail account.";
$i_Mail_LinkWebmail = "Link to webmail system";
$i_Mail_AllowSendReceiveExternalMail = "Allow user sending and receiving external emails.";
$i_Mail_AllowUserUseWebmail = "Allow user using Webmail System. (Valid only if this user has Email/FTP/Linux account already)";
$i_Mail_AllowSendReceiveExternalMailEdit = "Allow user sending and receiving external emails. (Valid only if this user has Email/FTP/Linux account already)";

$i_Help_Show = "eClass Quick Start Guide";
$i_Help_Description = "You can enable the eClass Quick Start Guide which acts as a reminder to all users after system login. Please enter the Date Range for the guide to be displayed. If one of the dates is not entered, the guide will not be shown.";
$i_Help_DateStart = "Start Date ";
$i_Help_DateEnd = "End Date";

$i_ec_file_assign = "Transfer file ownership";
$i_ec_file_remove_teacher = "Teacher(s) who owns file(s)";
$i_ec_file_exist_teacher = "inside this course";
$i_ec_file_not_exist_teacher = "outside this course";
$i_ec_file_target_teacher = "Target teacher";

$i_ec_file_assign_to = "pass file ownership to";
$i_ec_file_confirm = "This process is IRREVERSIBLE!\\nAre you sure you want to pass file ownership?";
$i_ec_file_confirm2 = "Target teacher is not teaching in this course. Only teacher in this course can receive file ownership!\\nAre you sure you want to add this teacher to this course?";
$i_ec_file_warning = "Please select at least one teacher who owns file(s)";
$i_ec_file_warning2 = "Please select a teacher to receive the ownership";
$i_ec_file_user_delete = "User(s) to be removed from this eClass:";
$i_ec_file_msg_transfer = "File ownwership can be transfered to another teacher so that these files can be managed when the teacher is removed. Transfer to:";
$i_ec_file_no_transfer = "Do not transfer";
$i_ec_file_user_delete_confirm = "Are you sure you want to delete this eClass user(s)?";

$i_OrganizationPage_Settings = "Organization Page Settings";
$i_OrganizationPage_NotDisplayMemberList = "Not display member list.";
$i_OrganizationPage_NotDisplayEmailAddress = "Not display members' email address.";
$i_OrganizationPage_HideInOrganization = "No display on Organization Page";
$i_OrganizationPage_DisplayOption = "In Organization Page";
$i_OrganizationPage_NotDisplay = "Not Display";
$i_OrganizationPage_Unchange = "Unchange";
$i_OrganizationPage_DisplayAll = "Display All";
$i_OrganizationPage_DefaultCat = "Default Category";

$i_SpecialRoom = "Special Room";
$i_SpecialRoom_ReadingRoom = "Reading Room";
$i_SpecialRoom_ReadingRoom_Name = "Room Title";
$i_SpecialRoom_ReadingRoom_Description = "Description";
$i_SpecialRoom_ELP = "ELP";
$i_SpecialRoom_iPortfolio = "iPortfolio";

//$i_AdminJob_AdminCenter = "Admin Center";

$i_AdminJob_StaffName = "Staff Name";
$i_AdminJob_AdminLevel = "Admin Level";
$i_AdminJob_AdminLevel_Normal = "Normal Level";
$i_AdminJob_AdminLevel_Full = "Full Control";
$i_AdminJob_AdminLevel_Detail_Normal = "$i_AdminJob_AdminLevel_Normal";
$i_AdminJob_AdminLevel_Detail_Full = "$i_AdminJob_AdminLevel_Full";
//$i_AdminJob_Description_SetAdmin = "Please Select which user to be new admin and choose the admin level.";
//$i_AdminJob_AccessClass = "Classes";

$i_AdminJob_Announcement = "Announcement Approval";

//$i_AdminJob_Announcement_SelectApproval = "Seek for Approval, select user:";
$i_AdminJob_Announcement_NoEdit = "<font color=red>(This announcement record has been approved. You cannot edit this record.)</font>";
$i_AdminJob_Announcement_NewEdit = "<font color=red>(This announcement record has been rejected. You can edit it and system will post it as new record.)</font>";
$i_AdminJob_Announcement_ApprovalUser = "Approval Admin";
$i_AdminJob_Announcement_Remark = "Remark";
$i_AdminJob_Announcement_HandleAnnouncement = "Handle Announcement";
$i_AdminJob_Announcement_Handle_Reason = "Reason: <br> (Poster will receive)";
$i_AdminJob_Announcement_Handle_Remark = "Remark: <br> (Only you can see)";
$i_AdminJob_Announcement_ApproveSubject = "Announcement Record Approved";
$i_AdminJob_Announcement_RejectSubject = "Announcement Record Rejected";
$i_AdminJob_Announcement_ApproveMessage1 = "Your application of announcement (";
$i_AdminJob_Announcement_ApproveMessage2 = ") has been approved and published. It has been displayed to related users in main page announcement.";
$i_AdminJob_Announcement_RejectMessage1 = "Your application of announcement (";
$i_AdminJob_Announcement_RejectMessage2 = ") has been rejected. ";
//$i_AdminJob_Announcement_Reason = "Reason:";
$i_AdminJob_Announcement_AlertWaiting = " Announcement(s) waiting";
$i_AdminJob_Announcement_At = " at ";
//$i_AdminJob_Announcement_ApprovedBy = "Approved by ";
//$i_AdminJob_Announcement_RejectedBy = "Rejected by ";
$i_AdminJob_Announcement_Handle = "Handle";
$i_AdminJob_Announcement_Action_Approve = "Approve";
$i_AdminJob_Announcement_Action_Waiting = "Waiting";
$i_AdminJob_Announcement_Action_Reject = "Reject";
$i_AdminJob_Announcement_LastAction = "Last Handle Time";
$i_AdminJob_Announcement_NeedApproval = "Approved";
$i_AdminJob_Announcement_NoApproval = "No approval";
$i_AdminJob_Announcement_RemoveAdmin = "Are you sure to cancel this admin?";

$i_CampusMail_Internal_Recipient_Group ="Internal Recipient Group";
$i_CampusMail_External_Recipient= "External Recipient";
$i_CampusMail_External_Recipient_Group="External Recipient Group";
$i_CampusMail_New_iMail = "iMail";
$i_CampusMail_New_FolderManagement = "Manage Folder";
$i_CampusMail_New_Alert_RemoveFolder = "Remove folder will remove all messages inside the folder. Do you want to proceed?";
$i_CampusMail_New_FolderName = "Folder Name";
$i_CampusMail_New_Internal_Recipient_Group ="New Internal Recipient Group";
$i_CampusMail_New_Prompt_FolderExist = "This name already exists.";
$i_CampusMail_New_ToFolder = "Go to folder";
$i_CampusMail_New_AddressBook = "Address Book";
$i_CampusMail_New_AddressBook_Name = "Name";
$i_CampusMail_New_AddressBook_Type = "User/Group";
$i_CampusMail_New_AddressBook_EmailAddress = "Email Address";
$i_CampusMail_New_AddressBook_Category = "Category";
$i_CampusMail_New_AddressBook_Internal = "Int";
$i_CampusMail_New_AddressBook_External = "Ext";
$i_CampusMail_New_AddressBook_Prompt_Num = "How many addresses you want to input?";
$i_CampusMail_New_AddressBook_Alert_NeedInteger = "Please input a positive integer.";
$i_CampusMail_New_AddressBook_ByUser = "User";
$i_CampusMail_New_AddressBook_ByGroup = "Group";
$i_CampusMail_New_AddressBook_TypeSelect = "Select";
$i_CampusMail_New_InternalRecipients = "Internal Recipients";
$i_CampusMail_New_ExternalRecipients = "External Recipients";
$i_CampusMail_New_Settings_PersonalSetting ="General Preferences";
$i_CampusMail_New_Settings_Signature="Signature";
$i_CampusMail_New_Settings_EmailForwarding="Email Forwarding";
$i_CampusMail_New_Settings_ReplyEmail ="Reply-to address for external mail";
$i_CampusMail_New_Settings_DisplayName ="Display name for external mail";
$i_CampusMail_New_Settings_DaysInSpam ="Spam mail reserved day(s)";
$i_CampusMail_New_Settings_DaysInTrash ="Trash mail reserved day(s)";
$i_CampusMail_New_Settings_PleaseFillInYourSignature="Please fill in your signature";
$i_CampusMail_New_Settings_PleaseFillInForwardEmail ="Please fill in the forwarding email addresses:<br> <font color=red>(Each line for one email address)</font>";
$i_CampusMail_New_Settings_PleaseFillInForwardKeepCopy ="Keep local copy";
$i_CampusMail_New_Settings_Forever="Forever";
$i_CampusMail_New_Settings_NoticeDefaultDisplayName ="<font color=red>(Leave it blank to use the default user name in the system.)</font>";
$i_CampusMail_New_Settings_NoticeDefaultReplyEmail="<font color=red>(Leave it blank to use the default reply email in the system.)</font>";
$i_CampusMail_New_Settings_DisableEmailForwarding="Disable Email Forwarding";
$i_CampusMail_New_Settings_POP3 = "POP3";
$i_CampusMail_New_Settings_DisableCheckEmail = "Stop iMail to download Internet emails";
$i_CampusMail_New_Settings_AutoReply = "Auto Reply";
$i_CampusMail_New_Settings_EnableAutoReply = "Use Auto Reply";
$i_CampusMail_New_Settings_PleaseFillInAutoReply = "Please fill in your reply message (Applicable for Internet email only)";
$i_CampusMail_New_Settings_POP3_guideline = "Click here for the guideline of email client setup";

$i_CampusMail_New_To = "To";
$i_CampusMail_New_CC = "CC";
$i_CampusMail_New_BCC = "BCC";
$i_CampusMail_New_MailFolder = "Mail Folder";
$i_CampusMail_New_Settings = "Preferences";
$i_CampusMail_New_Quota1 = "Used up";
$i_CampusMail_New_Quota2 = "Storage Space";
$i_CampusMail_New_MailSource = "Mail Source";
$i_CampusMail_New_MailSource_Internal = "Internal Mail";
$i_CampusMail_New_MailSource_External = "Internet E-mail";
$i_CampusMail_New_Icon_MailSource = "<img src=\"$image_path/frontpage/imail/icon_mailfrom.gif\" border=0>";
$i_CampusMail_New_Icon_IntMail = "<img src=\"$image_path/frontpage/imail/icon_intmail.gif\" alt=\"$i_CampusMail_New_MailSource_Internal\" border=0>";
$i_CampusMail_New_Icon_ExtMail = "<img src=\"$image_path/frontpage/imail/icon_extmail.gif\" alt=\"$i_CampusMail_New_MailSource_External\" border=0>";
$i_CampusMail_New_Icon_NewMail = "<img src=\"$image_path/frontpage/imail/icon_newmail.gif\" border=0>";
$i_CampusMail_New_Icon_ReadMail = "<img src=\"$image_path/frontpage/imail/icon_mailread_$intranet_session_language.gif\" border=0>";
$i_CampusMail_New_Icon_RepliedMail = "<img src=\"$image_path/frontpage/imail/icon_mailreplied_$intranet_session_language.gif\" border=0>";
$i_CampusMail_New_EmailAddressSeparationNote = "Please use ; or , to separate each email address";
$i_CampusMail_New_NotificationInternalOnly = "Only applicable for $i_CampusMail_New_InternalRecipients";
$i_CampusMail_New_ExternalSentSuccess = "Internet E-mail sent successfully.";
$i_CampusMail_New_ExternalSentFailed = "Internet E-mail cannot be sent.";
$i_CampusMail_New_MoveTo = "Move To";
$i_CampusMail_New_alert_moveto = "Are you sure to move selected mail(s)?";
$i_CampusMail_New_alert_norecipients = "No Recipients";
$i_CampusMail_New_NumOfMail = "Number of mail(s)";
$i_CampusMail_New_NoFolder = "Please click New to add new folder.";
$i_CampusMail_New_MailAliasGroup = "Mail Alias Group";
$i_CampusMail_New_AliasGroup = "Alias Group";
$i_CampusMail_New_AliasGroupRemark = "Alias Group Remark";
$i_CampusMail_New_NumberOfEntry = "Number of entries";
$i_CampusMail_New_AddressBook_TargetGroup = "Target Group";
$i_CampusMail_New_SelectFromAlias = "Select From Alias Group";
$i_CampusMail_New_Reply = "Reply";
$i_CampusMail_New_ReplyAll = "Reply All";
$i_CampusMail_New_Forward = "Forward";
$i_CampusMail_New_Wrote = " Wrote...";
$i_CampusMail_New_iMail_Settings = "iMail Settings";
$i_CampusMail_New_QuotaSettings = "Mailbox Quota Settings";
$i_CampusMail_New_ViewQuota_Identity = "View By Identity";
$i_CampusMail_New_ViewQuota_Group = "View By Group";
$i_CampusMail_New_Quota = "Quota";
$i_CampusMail_New_BatchUpdate = "Update all users";
$i_CampusMail_New_SingleEdit = "Edit Single User Quota";
$i_CampusMail_New_TargetUser = "Target User";
$i_CampusMail_New_BatchUpdateTarget = "Batch User Update Target";
$i_CampusMail_New_DefaultQuota = "Default Quota (in MBytes)";
$i_CampusMail_New_ExternalAllow = "Allow to send/receive Internet e-mails";
$i_CampusMail_New_alert_RemoveAccount = "Are you sure to remove this mail server account? (This may affect file server account if they are the same server)";
$i_CampusMail_New_BatchOption_OpenAccount = "Allow listed users to use iMail to send/receive Internet e-mails. Quota: ";
$i_CampusMail_New_BatchOption_UnlinkAccount = "Not allow listed users to use iMail to send/receive Internet e-mails <font color=red>(Accounts on Mail Server will not be affected)</font>";
$i_CampusMail_New_BatchOption_RemoveAccount = "Remove all listed users' mail account on Mail Server <font color=red>(This may affect file server accounts if iFolder server is the same server as mail server)</font>";
$i_CampusMail_New_QuotaNotEnough = "Quota is not enough to send all attachments";
$i_CampusMail_New_ViewFormat_PlainText = "View Plaintext Format";
$i_CampusMail_New_ViewFormat_HTML = "View HTML Format";
$i_CampusMail_New_ViewFormat_MessageSource = "View Message Source";
$i_CampusMail_New_PlsWait = "Please Wait ...";
$i_CampusMail_New_ConnectingMailServer = "Connecting to mail server ... ";
$i_CampusMail_New_Receiving1 = "Downloaded";
$i_CampusMail_New_Receiving2 = "message(s) of";
$i_CampusMail_New_Receiving3 = "";
$i_CampusMail_New_WarningQuotaNotEnough = "Please clear your mailbox to receive emails in mail server.";
$i_CampusMail_New_BackMailbox = "Back to Inbox";
$i_CampusMail_New_CheckMail = "Check Mail";
$i_CampusMail_New_MailServerNotReachable = "Warning: Mail Server not reachable. Please try to change your password to synchronize with the Mail Server. If this message still appears, please contact System Administrator.";
$i_CampusMail_New_MailSearch_Description = "Please input searching criteria";
$i_CampusMail_New_MailSearch_hasAttachment = "with attachments";
$i_CampusMail_New_MailSearch_SearchFolder = "Search in Folder";
$i_CampusMail_New_MailSearch_SearchResult = "Search Result";
$i_CampusMail_New_MailSearch_BackToResult = "Back to Search Result";
$i_CampusMail_New_Confirm_remove_mail = "Are you sure to remove this mail message?";
$i_CampusMail_New_BadWords_Settings = "Prohibited Words";
$i_CampusMail_New_BadWords_Instruction_top = "You can input restricted bad words in the textbox. These bad words will be replaced by *** in mail message.";
$i_CampusMail_New_BadWords_Instruction_bottom = "Please enter one phrase for each line.";
$i_CampusMail_New_ViewMail = "View Mail";
$i_CampusMail_New_Inbox = "Inbox";
$i_CampusMail_New_Outbox = "Outbox";
$i_CampusMail_New_Draft = "Draft";
$i_CampusMail_New_Trash = "Trash";
$i_CampusMail_New_Action_Clean = "Clean";
$i_CampusMail_New_FolderRename_NewName = "New Folder Name";
$i_CampusMail_New_InternalAlias = "Int. Alias";
$i_CampusMail_New_ExternalContact = "Ext. Contacts";
$i_CampusMail_New_ListAlias = "list";
$i_CampusMail_New_RemoveBySearch = "Removal by Search";
$i_CampusMail_New_RemoveBySearch_SearchPhrase = "Search Phrase";
$i_CampusMail_New_AddToAliasGroup = "Add to Alias Group";
$i_CampusMail_New_Mail_ExternalContact = "External Contact";
$i_CampusMail_New_Add_CC = "Add CC";
$i_CampusMail_New_Add_BCC = "Add BCC";
$i_CampusMail_New_IntranetNameList = "Intranet Name List";
$i_CampusMail_New_SearchRecipient = "Search Recipient(s)";
$i_CampusMail_Encoding_Warning="The attachment of this mail contains some characters which cannot represented by your language, the recipients may not be able to see the correct filename(s). Please save the file(s) and reattch to the mail.";
$i_CampusMail_New_FolderManager = "Folder Manager";
$i_CampusMail_New_ComposeMail = "Compose Mail";
$i_CampusMail_New_Mail_Search = "Search";
$i_CampusMail_New_Recipient_Status = "Recipient Status";
$i_CampusMail_Warning_External_Recipient_No_Semicolon ="The Recipient Name should not contain semicolon( ; )";
$i_CampusMail_New_No_Subject = "(No Subject)";
$i_CampusMail_New_No_Sendmail = "You have no such right.";
$i_CampusMail_New_Show_All_Recipient = "Show Details";
$i_CampusMail_New_Hide_All_Recipient = "Hide Details";
$i_CampusMail_OutOfQuota_Warning="Since your inbox does not have enough space, you need to clear your inbox to receive the new messages.";
### added by Ronald on 20090211 ##
$i_CampusMail_ClickHereToDownloadAttachment = "or click here to download";
### added by Ronald on 20090401 ##
$i_CampusMail_Admin_DayInTrashSettings = "Deleted/Spam Mail Retention Period";
$i_CampusMail_Admin_DayInTrashSettings_Notice = "* If user has setup their own trash mail reserved day settings, System will use the user's setting instead of this.";
### added by Ronald on 20090402 ##
$i_CampusMail_New_Settings_EmailRules = "Email Rules";
### added by Ronald on 20090417 ###
$i_CampusMail_Condition = "Conditions";
$i_CampusMail_Condition_From = "From";
$i_CampusMail_Condition_To = "To";
$i_CampusMail_Condition_Subject = "Subject";
$i_CampusMail_Condition_Message = "Message";
$i_CampusMail_Condition_HasAttachment = "Has Attachment";
$i_CampusMail_Action = "Action";
$i_CampusMail_Action_MarkAsRead = "Mark As Read";
$i_CampusMail_Action_DeleteIt = "Delete";
$i_CampusMail_Action_MoveToFolder = "Move To Folder";
$i_CampusMail_Action_ForwardTo = "Forward To";
$i_CampusMail_MailRule_DeleteActionJSWaring = "\"Delete\" is selected, so cannot select \"Mark As Read\" or \"Move To Folder\".";
$i_CampusMail_MailRule_ActionEmptyJSWaring = "Please select at lease ONE action for the email rule.";

$i_Calendar_Admin_Setting = "iCalendar Settings";

$i_SmartCard = "Smart Card";
$i_SmartCard_CardID = "Smart Card ID";
$i_SmartCard_DateRecorded = "Record Date";
$i_SmartCard_TimeRecorded = "Time Recorded";
$i_SmartCard_Site = "Site";
$i_SmartCard_StudentOutingDate = "Activity / Outgoing Date";
$i_SmartCard_StudentOutingOutTime = "Activity / Outgoing Start Time";
$i_SmartCard_StudentOutingBackTime = "Activity / Outgoing Back Time";
$i_SmartCard_StudentOutingPIC = "Person-in-charge";
$i_SmartCard_StudentOutingFromWhere = "Leaves At";
$i_SmartCard_StudentOutingLocation = "Acitvity / Outgoing Location";
$i_SmartCard_StudentOutingReason = "Activity / Outgoing Reason";
$i_SmartCard_StudentOutingNoOut = "Not Start&nbsp;from School";
$i_SmartCard_StudentOutingNoBack = "Not Yet Back school";
$i_SmartCard_ClassName = "Class";
$i_SmartCard_GroupName = "Group";
$i_SmartCard_Remark = "Remark";
$i_SmartCard_DetentionDate = "Detention Date";
$i_SmartCard_DetentionArrivalTime = "Arrival Time";
$i_SmartCard_DetentionDepartureTime = "Departure Time";
$i_SmartCard_DetentionReason = "Reason of Detention";
$i_SmartCard_DetentionLocation = "Detention Location";
$i_SmartCard_DetentionNotArrived = "Not Arrived";
$i_SmartCard_DetentionNotLeft = "Not Left";
$i_SmartCard_ReportMonth = "Month";
$i_SmartCard_Description_Terminal_IP_Settings = "
In IP Address box, you can input:
<ol><li>Exact IP Address (e.g. 192.168.0.101)</li>
<li>IP Address Range (e.g. 192.168.0.[10-100])</li>
<li>CISCO Style Subnet Range (e.g. 192.168.0.0/24)</li>
<li>Allow All incoming IP address (Input 0.0.0.0)</li></ol>
<font color=red>For Card reader-connecting PC, system suggests you should set that machine in FIXED IP address (i.e. Not use DHCP), in order to ensure data transmission source is legitimate.</font><br>
This System has security policy to prevent Faked IP Address attack.
";
$i_SmartCard_Instruction_RemoveLog = "Please input the dates";
$i_SmartCard_DownloadOnly = "Download ONLY";
$i_SmartCard_RemoveOnly = "Remove ONLY";
$i_SmartCard_DownloadRemove = "Remove After Download";
$i_SmartCard_FromToIncluded = "Start and end date will be included";
$i_SmartCard_DownloadLogFormat = "Downloaded File format is Class name and Class Number (can be uploaded back to the server)";
$i_SmartCard_SystemSettings = "System Settings";

$i_StudentAttendance_System = "Student Smart Card Attendance";
$i_StudentAttendance_SystemAdminUserSetting = "Set Admin User";
$i_StudentAttendance_AllStudentsWithRecords = "Show all students have records";
$i_StudentAttendance_Menu_ResponsibleAdmin = "Responsible Admin Settings";
$i_StudentAttendance_Menu_DailyOperation = "Daily Operation";
$i_StudentAttendance_Menu_DataManagement = "Attendance Data Management";
$i_StudentAttendance_Menu_OtherFeatures = "Other Features";
$i_StudentAttendance_Menu_DataExport = "Data Export";
$i_StudentAttendance_Menu_DataImport = "Data Import";
$i_StudentAttendance_Menu_Report = "Report";
$i_StudentAttendance_Menu_OtherSettings = "Other Settings";
$i_StudentAttendance_Menu_CustomFeatures = "Custom Features";

$i_StudentAttendance_AttendanceMode = "Attendance Mode";
$i_StudentAttendance_AttendanceMode_AM_Only = "AM Only";
$i_StudentAttendance_AttendanceMode_PM_Only = "PM Only";
$i_StudentAttendance_AttendanceMode_WD_Lunch = "Whole Day with Lunchtime";
$i_StudentAttendance_AttendanceMode_WD_NoLunch = "Whole Day without Lunchtime";

$i_StudentAttendance_Menu_Slot_School = "Whole School Slot Settings";
$i_StudentAttendance_Menu_Slot_Class = "Class Specific Slot Settings";
$i_StudentAttendance_Menu_Slot_Group = "Group Specific Slot Settings";

$i_StudentAttendance_NormalDays = "Normal Days";
$i_StudentAttendance_Weekday_Specific = "Weekday-specific Settings";
$i_StudentAttendance_Cycleday_Specific = "Cycleday-specific Settings";
$i_StudentAttendance_NoSpecialSettings = "No Specific Settings";
$i_StudentAttendance_SpecialDay = "Special Day Settings";

$i_StudentAttendance_SetTime_AMStart = "AM Lesson Time";
$i_StudentAttendance_SetTime_LunchStart = "Lunch Time";
$i_StudentAttendance_SetTime_PMStart = "PM Lesson Time";
$i_StudentAttendance_SetTime_SchoolEnd = "School End Time";

### Added On 3 Sep 2007 ###
$i_StudentAttendance_NonSchoolDaySetting_Warning = "Non School Day Cannot Used As Normal Days";
###########################

###############################
$i_StudentAttendance_NonSchoolDay="Non School Day";
$i_StudentAttendance_TodayOnward="Today Onward";
$i_StudentAttendance_Past = "Past";
$i_StudentAttendance_ViewPastRecords="View Past Records";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart="$i_StudentAttendance_SetTime_AMStart Must be smaller than $i_StudentAttendance_SetTime_LunchStart";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart="$i_StudentAttendance_SetTime_AMStart Must be smaller than $i_StudentAttendance_SetTime_PMStart";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_AMStart Must be smaller than $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart="$i_StudentAttendance_SetTime_LunchStart Must be smaller than $i_StudentAttendance_SetTime_PMStart";
$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_LunchStart Must be smaller than $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_PMStart Must be smaller than $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_Cannot_Modify_Past_Records="Cannot modify the past record(s).";
$i_StudentAttendance_Previous_Week="Prev. week";
$i_StudentAttendance_Next_Week="Next week";
###############################

$i_StudentAttendance_WeekDay = "Weekday";
$i_StudentAttendance_CycleDay = "Cycle Day";
$i_StudentAttendance_TimeSlot_SpecialDay = "Special Day";
$i_StudentAttendance_Warn_Please_Select_WeekDay="Please select a Weekday";
$i_StudentAttendance_Warn_Please_Select_CycleDay="Please select a Cycle Day";
$i_StudentAttendance_Warn_Please_Select_A_Day="Please select a Day";
$i_StudentAttendance_ShowALlGroup_Options = "All Groups";
$i_StudentAttendance_HiddenGroup_Options = "With Time Settings";
$i_StudentAttendance_Diplay_Mode = "Group Attendance Mode";

$i_StudentAttendance_Warn_Invalid_Time_Format="Invalid Time Format";
$i_StudentAttendance_Warn_Invalid_Date_Format="Invalid Date Format";

$i_EditGroupHints = "To add a group, go to 'Intranet', 'Group Mgmt', 'Group Mgmt Cetre'.";
$i_StudentAttendance_ClassMode = "Class Attendance Mode";
$i_StudentAttendance_GroupMode = "Group Attendance Mode";
$i_StudentAttendance_ClassMode_UseSchoolTimetable = "Use School Timetable";
$i_StudentAttendance_ClassMode_UseClassTimetable = "Use Class-specific Timetable";
$i_StudentAttendance_GroupMode_UseSchoolTimetable = "Attendance Taking without Time Settings";
$i_StudentAttendance_GroupMode_UseGroupTimetable = "Attendance Taking with Time Settings";

$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance = "No need to take attendance";
$i_StudentAttendance_GroupMode_NoNeedToTakeAttendance = "No Attendance Taking";
$i_StudentAttendance_ClassMode_Edit = "Edit Timetable";

$i_StudentAttendance_CardLog = "Card Log";
$i_StudentAttendance_ViewTodayRecord = "View Today Record";
$i_StudentAttendance_ArchiveTodayRecord = "Archive Today Record";
$i_StudentAttendance_Outing = "Outing Record";
$i_StudentAttendance_Detention = "Detention Record";
$i_StudentAttendance_TimeSlotSettings = "Time Slot Settings";
$i_StudentAttendance_Report = "Report";
$i_StudentAttendance_WordTemplates = "Preset wordings";
$i_StudentAttendance_DataExport = "Data Export (For Offline PC Terminal)";
$i_StudentAttendance_Reminder = "Teacher Call Reminder";
$i_StudentAttendance_DataImport = "Data Import";
$i_StudentAttendance_ParentLetters = "Parent Letters";
$i_StudentAttendance_By = "By";
$i_StudentAttendance_Slot = "Time Slot";
$i_StudentAttendance_Type = "Type";
$i_StudentAttendance_Slot_Start = "Start";
$i_StudentAttendance_Slot_End = "End";
$i_StudentAttendance_Slot_Boundary = "Go To School Time/Leave School Time";
$i_StudentAttendance_Slot_InUse  = "In Use";
$i_StudentAttendance_Slot_Special = "Special Days";
$i_StudentAttendance_Slot_Special_Today1 = "Today is ";
$i_StudentAttendance_Slot_Special_Today1 = " , so the time is different from normal days.";
$i_StudentAttendance_Type_GoToSchool = "Go To School";
$i_StudentAttendance_Type_LeaveSchool = "Leave School";
$i_StudentAttendance_Slot_AM = "AM";
$i_StudentAttendance_Slot_PM = "PM";
$i_StudentAttendance_Slot_AfterSchool = "After School";
$i_StudentAttendance_Slot_SettingsDescription = "&nbsp; * Please use 24-hr format (HH:mm) with leading zero (e.g. 07:30, 16:30).";
$i_StudentAttendance_View_Date = "View Date";
$i_StudentAttendance_ToSchoolTime = "Go To School Time";
$i_StudentAttendance_LeaveSchoolTime = "Leave School Time";
$i_StudentAttendance_Time_Arrival = "Arrival Time";
$i_StudentAttendance_Time_Departure = "Departure Time";
$i_StudentAttendance_Status = "Status";
$i_StudentAttendance_MinLate = "Late for (min)";
$i_StudentAttendance_MinEarly = "Left early for (min)";
$i_StudentAttendance_Status_OnTime = "On Time";
$i_StudentAttendance_Status_Present = "Present";
$i_StudentAttendance_Status_Late = "Late";
$i_StudentAttendance_Status_Absent = "Absent";
$i_StudentAttendance_Status_PreAbsent = "Absent"; #"No Attend";
$i_StudentAttendance_Status_EarlyLeave = "Early Leave";
$i_StudentAttendance_Status_Outing = "Outing";
$i_StudentAttendance_Status_SL = "病假";
$i_StudentAttendance_Status_AR = "事假";
$i_StudentAttendance_Status_LE = "遲到及早退";
$i_StudentAttendance_Status_Truancy = "逃學";
$i_StudentAttendance_Status_Waived = "Waived";
$i_StudentAttendance_Archive_Date = "Archive Data for Date";
$i_StudentAttendance_con_RecordArchived = "Data Archived";
$i_StudentAttendance_Report_Daily = "Daily Record";
$i_StudentAttendance_Report_Student = "Student Monthly Record";
$i_StudentAttendance_Report_Class = "Whole Class Report";
$i_StudentAttendance_Report_SelectSlot = "Select Slot(s)";
$i_StudentAttendance_Report_PlsSelectSlot = "Please select at least 1 time slot";
$i_StudentAttendance_Report_PlsSelectLeaveType = 'Please select leave type';
$i_StudentAttendance_Menu_DailyLessonOperation = 'Daily Operation (Lesson)';
$i_StudentAttendance_Menu_ReportLesson = 'Report (Lesson)';
$i_StudentAttendance_Report_NoRecord = "No Record";
$i_StudentAttendance_Report_Search="Search";
$i_StudentAttendance_Report_NoCardTab = "No Tapping Card Report";
$i_StudentAttendance_InSchool = "In School";
$i_StudentAttendance_LeaveSchool = "Leave School";
$i_StudentAttendance_Today_lastRecord = "Today's last record";
$i_StudentAttendance_PresetWord_CardSite = "Card Station";
$i_StudentAttendance_PresetWord_OutingFromWhere = "[Activity / Outgoing] Leaves At";
$i_StudentAttendance_PresetWord_OutingLocation = "[Activity / Outgoing] Location";
$i_StudentAttendance_PresetWord_OutingObjective = "[Activity / Outgoing] Objective";
$i_StudentAttendance_PresetWord_DetentionLocation = "[Detention] Location";
$i_StudentAttendance_PresetWord_DetentionReason = "[Detention] Reason";
$i_StudentAttendance_NotDisplayOntime = "NOT display on time record";
$i_StudentAttendance_ImportFormat_CardID = "Time, $i_SmartCard_Site, Card ID";
$i_StudentAttendance_ImportFormat_ClassNumber = "Time, $i_SmartCard_Site, Class Name, Class Number";
$i_StudentAttendance_ImportFormat_From_OfflineReader = "Records From Offline Reader";
$i_StudentAttendance_ImportTimeFormat = "Please use time format of YYYY-MM-DD HH:mm:ss";
$i_StudentAttendance_Import_Instruction_OneDayOnly = "Records in the file MUST be the same day.";
$i_StudentAttendance_Import_Warning_OneDayOnly = "Not ALL records in the file are in the same day. Please use one file for each date.";
$i_StudentAttendance_ImportConfirm = "If the above records are correct, please click Import to finish import.";
$i_StudentAttendance_ImportCancel = "If the above records are wrong, please click cancel to remove these records.";
$i_StudentAttendance_ArchiveWait = "This action needs some time, please wait...";
$i_StudentAttendance_StudentInformation = "Student Information";
$i_StudentAttendance_Reminder = "Reminder";
$i_StudentAttendance_ParentLetters_Late = "Late Parent Letters";
$i_StudentAttendance_Reminder_Date = "Remind Date";
$i_StudentAttendance_Reminder_Teacher = "Corresponding Teacher";
$i_StudentAttendance_Reminder_Reason = "Reason";
$i_StudentAttendance_Reminder_Status_Past = "Past";
$i_StudentAttendance_Reminder_Status_Coming = "Coming";
$i_StudentAttendance_Status_PastRecord = "Past record";
$i_StudentAttendance_Status_TodayAndComing = "Today & coming days";
$i_StudentAttendance_Reminder_ImportFileDescription = "
Column 1 : Class Name <br>
Column 2 : Class Number <br>
Column 3 : Date of Reminder (Empty will be interpreted as <b>Tomorrow</b>) <br>
Column 4 : Teacher's Login name <br>
Column 5 : Content
";
$i_StudentAttendance_Reminder_StartDate = "Reminder Starts Date";
$i_StudentAttendance_Reminder_FinishDate = "Reminder Ends Date";
$i_StudentAttendance_Reminder_RepeatSelection[0] = "Does Not Repeat";
$i_StudentAttendance_Reminder_RepeatSelection[1] = "Daily";
$i_StudentAttendance_Reminder_RepeatSelection[2] = "Weekday";
$i_StudentAttendance_Reminder_RepeatSelection[3] = "Cycle Day";
$i_StudentAttendance_Reminder_WeekdaySelection = "Please Select The Weekday(s)";
$i_StudentAttendance_Reminder_CycledaySelection = "Please Select The Cycle Day(s)";
$i_StudentAttendance_Reminder_RepeatFrequency = "Repeat";
$i_StudentAttendance_Reminder_StartDateEmptyWarning = "Please Input The Reminder Starts Date";
$i_StudentAttendance_Reminder_FinishDateEmptyWarning = "Please Input The Reminder Ends Date";
$i_StudentAttendance_Reminder_WrongDateWarning = "Please Input A Correct Date(s)";
$i_StudentAttendance_Reminder_WeekdaySelectionWarning = "Please Select The Weekday(s)";
$i_StudentAttendance_Reminder_CycleDaySelectionWarning = "Please Select The Cycle Day(s)";
$i_StudentAttendance_Reminder_TeacherSelectionWarning = "Please Select The Corresponding Teacher";
$i_StudentAttendance_Reminder_InputReasonWarning = "Please Input The Reason";
$i_StudentAttendance_Outing_ImportFileDescription = "
Column 1 : Class Name <br>
Column 2 : Class Number <br>
Column 3 : Teacher's Login name <br>
Column 4 : Activity / Outgoing Date (Empty will be interpreted as <b>Today</b>)(YYYY-MM-DD) <br>
Column 5 : Activity / Outgoing Start Time (HH:mm:ss)<br>
Column 6 : Activity / Outgoing End Time (HH:mm:ss)<br>
Column 7 : Reason
";
$i_StudentAttendance_Detention_ImportFileDescription = "
Column 1* : Class Name <br>
Column 2* : Class Number <br>
Column 3: $i_SmartCard_DetentionDate (Empty will be interpreted as <b>Today</b>)(YYYY-MM-DD) <br>
Column 4: $i_SmartCard_DetentionArrivalTime (HH:mm:ss) <br>
Column 5: $i_SmartCard_DetentionDepartureTime (HH:mm:ss) <br>
Column 6: $i_SmartCard_DetentionLocation<br>
Column 7: $i_SmartCard_DetentionReason<br>
Column 8: $i_SmartCard_Remark
<br>
* - Must be filled
";
$i_StudentAttendance_SimCard = "Simulate Card Record";
$i_StudentAttendance_Action_SpecialArchive = "Special Archival Time";
$i_StudentAttendance_NeedTakeAttendance = "Need to take Attendance";
$i_StudentAttendance_NoCardRecord = "No Record";

$i_StudentAttendance_RemoveByDateRange = "Remove By Date Range";
$i_StudentAttendance_ImportCardID = "Import Smart Card ID";
$i_StudentAttendnace_ImportRawRecord = "Import Raw Card Log";
$i_StudentAttendance_OR_ExportStudentInfo = "Export Student Info for Offline Reader Use";

# New Attendance 2
$i_StudentAttendance_LunchSettings = "Lunch Settings";
$i_StudentAttendance_NonTargetSettings = "Skip Student List";
$i_StudentAttendance_BackSchoolTime = "Back School Time";
$i_StudentAttendance_LunchStartTime = "Lunch Start Time";
$i_StudentAttendance_LunchEndTime = "Lunch End Time";
$i_StudentAttendance_SchoolEndTime = "After School Time";

$i_StudentAttendance_Frontend_menu_TakeAttendance = "Take Attendance";
$i_StudentAttendance_Frontend_menu_CheckStatus = "Check Status Record";
$i_StudentAttendance_Frontend_menu_Report = "Report";
$i_StudentAttendance_Frontend_menu_CheckMyRecord = "My Record";

$i_StudentAttendance_SelectAnotherClass = "Select Another Class";
$i_StudentAttendance_Field_Date = "Date";
$i_StudentAttendance_Field_Date_From = "From Date";
$i_StudentAttendance_Field_Date_To = "To Date";
$i_StudentAttendance_Field_ConfirmedBy = "Confirmed By";
$i_StudentAttendance_Field_LastConfirmedTime = "Confirmation Time";
$i_StudentAttendance_LeftStatus_Type_InSchool = "In School Status";
$i_StudentAttendance_LeftStatus_Type_Lunch = "Lunch Status";
$i_StudentAttendance_LeftStatus_Type_AfterSchool = "After School Status";
$i_StudentAttendance_NoLateStudents = "No Late Students";
$i_StudentAttendance_NoAbsentStudents = "No Absent Students";
$i_StudentAttendance_NoEarlyStudents = "No Early Leave Students";
$i_StudentAttendance_NoNeedTakeAttendance = "No need to use Smart Card Attendance";
$i_StudentAttendance_InSchool_BackAlready = "In School";
$i_StudentAttendance_InSchool_HaveBeenToSchool= "Have been to School";
$i_StudentAttendance_InSchool_NotBackYet = "Not in School";
$i_StudentAttendance_Lunch_NotOutYet = "Not out for Lunch";
$i_StudentAttendance_Lunch_GoneOut = "Out for lunch";
$i_StudentAttendance_Lunch_Back = "Back from Lunch";
$i_StudentAttendance_AfterSchool_Left = "Left School";
$i_StudentAttendance_AfterSchool_Stay = "Staying in school";
$i_StudentAttendance_ViewStudentList = "View Student List";
$i_StudentAttendance_Menu_DataMgmt_ResetTime = "Reset In School Time for Past Days";
$i_StudentAttendance_Menu_DataMgmt_UndoPastProfileRecord = "Undo confirmed Absence/Late Student Profile Records";
$i_StudentAttendance_Menu_DataMgmt_DataClear = "Clear past information";
$i_StudentAttendance_Menu_DataMgmt_BadLogs = "Bad Record Log";
$i_StudentAttendance_Menu_OtherFeatures_Outing = "Outing Records";
$i_StudentAttendance_Menu_OtherFeatures_Detention = "Detention Records";
$i_StudentAttendance_Menu_OtherFeatures_Reminder = "Reminder Records";
$i_StudentAttendance_Offline_Import_DataType = "Data Type";
$i_StudentAttendance_Offline_Import_DataType_InSchool = "Go to School";
$i_StudentAttendance_Offline_Import_DataType_LunchOut = "Out for Lunch";
$i_StudentAttendance_Offline_Import_DataType_LunchIn = "Back from Lunch";
$i_StudentAttendance_Offline_Import_DataType_AfterSchool = "Leave School";
$i_StudentAttendance_BadLogs_Type_NotInLunchList = "Try to go out for lunch w/o right";
$i_StudentAttendance_BadLogs_Type_GoLunchAgain = "Try to go out for lunch again";
$i_StudentAttendance_BadLogs_Type_FakedCardAM = "Not Attend w/ Card Record (AM)";
$i_StudentAttendance_BadLogs_Type_FakedCardPM = "Not Attend w/ Card Record (PM)";
$i_StudentAttendance_BadLogs_Type_NoCardRecord = "Forgot To Bring Card / Place Card To Reader";
$i_StudentAttendance_BadLogs_Type = $i_general_Type;
$i_StudentAttendance_UndoProfile_Warning = "The above attendance records (no matter the records are generated by Smart Card System or input manually) will be removed. Please Click continue to restore.";
$i_StudentAttendance_ResetTime_Warning = "Student Profile Attendance Records will be updated after confirm";
$i_StudentAttendance_DataRemoval_Warning = "All card records (including time, month report) will be removed while <u><b>Student Profile Information WILL BE Kept</b></u>.";
$i_StudentAttendance_Field_CardStation = "Terminal";
$i_StudentAttendance_Report_ClassMonth = "Class Monthly Attendance Report";
$i_StudentAttendance_Report_ClassDaily = "Class Daily Attendance Report";
$i_StudentAttendance_Report_ClassBadRecords = "Class Bad Records Statistics";
$i_StudentAttendance_Report_StudentBadRecords = "Student Bad Records Statistics";
$i_StudentAttendance_Report_TwoMonths = "Monthly Attendance Report";
$i_StudentAttendance_Report_WholeYear = "Yearly Attendance Report";
$i_StudentAttendance_Field_InSchoolTime = "In-School Time";
$i_StudentAttendance_Field_LunchOutTime = "Lunch Out";
$i_StudentAttendance_Field_LunchBackTime = "Lunch Back";
$i_StudentAttendance_Field_LeaveTime = "Left Time";
$i_StudentAttendance_ReportHeader_NumStudents = "Number of Students";
$i_StudentAttendance_ReportHeader_NumRecords = "Number of Records";
$i_StudentAttendance_DisplayTop = "Display Top";
$i_StudentAttendance_Top = "Top";
$i_StudentAttendance_Top_student_suffix = "Student(s)";
$i_StudentAttendance_Message_ThisClassNoNeedToTake = "Students in this class does not need to take attendance";
$i_StudentAttendance_Message_NoStudentInClass = "No students in the class";
$i_StudentAttendance_New_InSchoolTime = "New In-School Time";
$i_StudentAttendance_Field_NumLateOriginal = "# of Late Students (Original)";
$i_StudentAttendance_Field_NumLateNew = "# of Late Students (After Reset)";
$i_StudentAttendance_CalendarLegend_NotConfirmed = "Not Confirmed";
$i_StudentAttendance_CalendarLegend_Confirmed = "Confirmed";
$i_StudentAttendance_Action_SetAbsentToPresent = "Set $i_StudentAttendance_Status_PreAbsent to $i_StudentAttendance_Status_Present";
$i_StudentAttendance_Action_SetAbsentToOntime = "Set $i_StudentAttendance_Status_PreAbsent to $i_StudentAttendance_Status_OnTime";

$i_StudentAttendance_Symbol_Present = "<span >/</span>";
$i_StudentAttendance_Symbol_Absent = "<span >O</span>";
$i_StudentAttendance_Symbol_Late = "<span lang=EN-US style='font-family:Symbol;mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:Symbol'><span style='mso-char-type:symbol;mso-symbol-font-family:Symbol'>&AElig;</span></span>";
$i_StudentAttendance_Symbol_Outing = "z";
$i_StudentAttendance_Symbol_EarlyLeave = "<span class=GramE><span lang=EN-US style='font-family:新細明體'>#</span></span>";
$i_StudentAttendance_Symbol_SL = "<span style='font-family:新細明體'>⊕</span>";
$i_StudentAttendance_Symbol_AR = "<span lang=EN-US style='font-family:Webdings;mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:Webdings'><span style='mso-char-type:symbol;mso-symbol-font-family:Webdings'>y</span></span>";
$i_StudentAttendance_Symbol_LE = "<span lang=EN-US style='font-family:\"Wingdings 2\";mso-ascii-font-family:新細明體;mso-hansi-font-family:新細明體;mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'><span style='mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'>U</span></span>";
$i_StudentAttendance_Symbol_Truancy = "<span lang=EN-US style='font-family:新細明體'>●</span>";
$i_StudentAttendance_Symbol_Waived = "<span class=GramE><span lang=EN-US style='font-family:新細明體'>*</span></span>";

$i_StudentAttendance_Stat_Present = "Number of Present";
$i_StudentAttendance_Stat_Absent = "Number of Absent";
$i_StudentAttendance_Stat_Late = "Number of Late";
$i_StudentAttendance_Stat_EarlyLeave = "Number of Early Leave";
$i_StudentAttendance_Stat_Truancy = "Number of Truancy";
$i_StudentAttendance_Daily_Stat = "Daily Statistic";
$i_StudentAttendance_Monthly_Level_Stat = "Monthly Stats";
$i_StudentAttendance_Attendance_Number = "Number of Attendants";
$i_StudentAttendance_Average_Present = "Average Present Number";
$i_StudentAttendance_Average_Absent = "Average Absent Number";
$i_StudentAttendance_Symbol_Reference = "Notation Symbols";
$i_StudentAttendance_FullReport_Days1 = "Number of School Days:";
$i_StudentAttendance_FullReport_Days2 = "";
$i_StudentAttendance_exactly_matched="Exactly Matched";

$i_StudentAttendance_Export_Notice="Note: Please choose \"Student Information\" for Offline Mode ( not Offline Reader)";

### Added By Ronald On 12 Apr 2007
$i_StudentAttendance_LeftStatus_Type_LeavingTime = "Leaving Time";
### Added By Ronald On 13 Apr 2007
$i_StudentAttendance_Leave_Status_Early_Leave_AM = "Early Leave (AM)";
$i_StudentAttendance_Leave_Status_Early_Leave_PM = "Early Leave (PM)";
$i_StudentAttendance_Leave_Status_Normal = "Normal";
### Added By Ronald On 16 Apr 2007
$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime = "Order By Leaving Time";
### Added On 19 Apr 2007
$i_StudentAttendance_Early_Leave_Insert_Record = "Insert A New Early Leave Record";
### Added On 20 Apr 2007
$i_StudentAttendance_Class_Select_Instruction = "Select The Class";
$i_StudentAttendance_Student_Select_Instruction = "Select The Student(s)";
$i_StudentAttendance_Input_Correct_Time = "Please Input A Correct Time";
$i_StudentAttendance_Input_Time = "Please Input The Time";
$i_StudentAttendance_Early_Leave_Warning = "<font color=red>Note - The Following Student's Record Will Be Changed:</font>";

$i_StudentAttendance_Menu_Slot_Session_Setting = "Session Settings";
$i_StudentAttendance_Menu_Slot_Session_School = "Whole School Session Settings";
$i_StudentAttendance_Menu_Slot_Session_Class = "Class Specific Session Settings";
$i_StudentAttendance_Menu_Slot_Session_Group = "Group Specific Session Settings";

$i_StudentAttendance_Menu_Slot_Session_Name = "Name";
$i_StudentAttendance_Menu_Time_Mode = "Time Table Mode";
$i_StudentAttendance_Menu_Time_Input_Mode = "Time Slot";
$i_StudentAttendance_Menu_Time_Session_Mode = "Time Session";
$i_StudentAttendance_TimeSessionSettings = "Time Session Settings";
$i_StudentAttendance_Menu_Time_Session_MorningTime = "AM Lesson Time";
$i_StudentAttendance_Menu_Time_Session_LunchStartTime = "Lunch Time";
$i_StudentAttendance_Menu_Time_Session_LunchEndTime = "PM Lesson Time";
$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime = "School End Time";
$i_StudentAttendance_Menu_Time_Session_NonSchoolDay = "Non School Day";
$i_StudentAttendance_Menu_Time_Session_Using = "Using";
$i_StudentAttendance_Menu_Time_Session_Activated = "Activated";
$i_StudentAttendance_Menu_Time_Session_School_Use = "Use";
$i_StudentAttendance_ViewToSchoolTime = "View Arrival Time";
$i_StudentAttendance_CalendarLegend_RecordWithData = "Records Exist";
$i_StudentAttendance_TimeSessionSettings_SessionName_Warning = "Please Input The Name";
$i_StudentAttendance_TimeSessionSettings_WeekdaySelection_Warning = "Please Select The Weekday";
$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning = "Please Select The Session";
$i_StudentAttendance_TimeSessionSettings_CycledaySelection_Warning = "Please Select The Cycleday";

$i_StudentAttendance_TimeSessionSettings_NowUsing1 = "Class(es) Using ";
$i_StudentAttendance_TimeSessionSettings_NowUsing2 = " Now";
$i_StudentAttendance_TimeSessionSettings_AlreadyHaveSpecialDaySession = "Date(s) With Special Day Settings";
$i_StudentAttendance_TimeSession_GetSpecialdayInfo_Warning = "Note: The Above Date Will Not Be Changed";
$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning = "* - Session Is Inactive";
$i_StudentAttendance_TimeSession_UsingAsNormalSetting = "Class(es) Now Using This Session As <span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_NormalDays</span>]</span>";
$i_StudentAttendance_TimeSession_UsingAsWeeklySetting = "Class(es) Now Using This Session As <span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_Weekday_Specific</span>]</span>";
$i_StudentAttendance_TimeSession_UsingAsCycleSetting = "Class(es) Now Using This Session As <span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_Cycleday_Specific</span>]</span>";
$i_StudentAttendance_TimeSession_UsingAsSpecialSetting = "Class(es) Now Using This Session As <span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_SpecialDay</span>]</span>";
$i_StudentAttendance_TimeSession_SessionNowUsing = "* - Now Using";
$i_StudentAttendance_TimeSession_OtherClassUseSameSession =  "Other Class(es) Use The Same Session";
$i_StudentAttendance_TimeSession_TypeSelection = "Type";
$i_StudentAttendance_TimeSession_ClassSelection = "Class Name";
$i_StudentAttendance_TimeSession_TypeSelectionWarning = "Please Select The Type";
$i_StudentAttendance_TimeSession_ClassSelectionWarning = "Please Select The Class";
### Added By Ronald On 2 Aug 2007 ###
$i_StudentAttendance_StaffAndStudentInformation = "Staff And Student Information";
$i_StudentAttendance_Setting_DisallowStudentProfileInput="Disallow New/Delete Attendance Record in Student Profile";

### Added On 24 Aug 2007 ###
$i_StudentAttendance_Reminder_Content = "Content";
############################

### Added on 22 Oct 2008 ###
$i_StudentAttendance_ShortCut = "Short Cut";
############################

$i_StudentAttendance_Warning_Data_Outdated="Data Outdated. Please submit again.";

### Added on 23 Jan 2009 ###
$i_StudentAttendance_default_attend_status="Default attendance status (when no data exists)";

// added on 20090608 by kenneth chung
$Lang['StudentAttendance']['TakeAttendanceFor'] = "Take Attendance For";
$Lang['StudentAttendance']['School'] = "School";
$Lang['StudentAttendance']['Lesson'] = "Lesson";
$Lang['StudentAttendance']['Session'] = "Session";

// added on 20090710 by kenneth chung
$Lang['StudentAttendance']['RecordTime'] = "Record Time";

$i_SmartCard_TerminalSettings = "Smart Card Terminal Settings";
$i_SmartCard_Terminal_IPList = "Terminal IP Address";
$i_SmartCard_Terminal_IPInput = "Please input the IP address of a terminal in each line";
$i_SmartCard_Terminal_ExpiryTime = "Terminal Expiry Time after Registration";
$i_SmartCard_Terminal_YourAddress = "Your Current IP Address";
$i_SmartCard_Terminal_IgnorePeriod = "Ignore Period of consecutive Card Read";
$i_SmartCard_Terminal_IgnorePeriod_unit = "min.";

$i_SmartCard_Confirm_Update_Attend = "Confirm to update the record?";

$i_SmartCard_Responsible_Admin_Class = "Class Representative";

$i_SmartCard_Responsible_Admin_Settings_List = "View Responsible Admin";
$i_SmartCard_Responsible_Admin_Settings_Manage = "Manage Responsible Admin";

$i_SmartCard_Settings_Lunch_List_Title = "View Student Allowed to go out for Lunch";
$i_SmartCard_Settings_Lunch_Misc_Title = "Other Settings";

$i_SmartCard_Settings_Lunch_New_Type = "Mode";
$i_SmartCard_Settings_Lunch_New_Type_Class = "Set By Whole Class";
$i_SmartCard_Settings_Lunch_New_Type_Student = "Set By Individual Students";
$i_SmartCard_Settings_NoStudentsAvailableForSelection = "No Available Students For Selection. All already in Allowed List";
$i_SmartCard_Settings_Lunch_Instruction_Class = "Please Select class(es). All students of selected class(es) will be added to Allow List of lunch outing.";
$i_SmartCard_Settings_Lunch_Instruction_Student = "Please Select student(s) for adding into Allow List of Lunch outing.";
$i_SmartCard_Settings_Admin_Number_Students = "Number of Student(s) allowed to edit daily attendance records";

$i_SmartCard_Settings_Lunch_Misc_No_Record = "Do not need to record Lunch Out";
$i_SmartCard_Settings_Lunch_Misc_Out_Once = "Restrict students to go out once ONLY";
$i_SmartCard_Settings_Lunch_Misc_All_Allow = "All students allow to go out";

$i_SmartCard_DailyOperation_viewClassStatus_PrintOption = "$i_PrinterFriendlyPage Option";
$i_SmartCard_DailyOperation_ViewClassStatus = "View Individual Class Status";
$i_SmartCard_DailyOperation_ViewGroupStatus = "View Individual Group Status";
$i_SmartCard_DailyOperation_ViewLateStatus = "View Late Student List";
$i_SmartCard_DailyOperation_ViewAbsenceStatus = "View Absence Student List";
$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus = "View Early Leave Student List";
$i_SmartCard_DailyOperation_ViewLeftStudents = "View Students in School";
$i_SmartCard_DailyOperation_ImportOfflineRecords = "Import Offline Records";
$i_SmartCard_DailyOperation_ViewEntryLog = "View Entry Log";
$i_SmartCard_DailyOperation_BlindConfirmation="Confirm for all classes";
$i_SmartCard_DailyOperation_Number_Of_Absent_Student="Number of Absent Students not confirmed";
$i_SmartCard_DailyOperation_Number_Of_Outing_Student="Number of Outing Students not confirmed";
$i_SmartCard_DailyOperation_OrderBy_InSchoolTime="Order By In School Time";
$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber="Order By Class Name and Class Number";
$i_SmartCard_DailyOperation_Preset_Absence="Preset Student Absence";
$i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent="Browse By Student";
$i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate="Browse By Date";
$i_SmartCard_DailyOperation_Preset_Absence_Has_Leave_Record="Has Leave Record";

$i_SmartCard_DailyOperation_Check_Time = "View Student Arrival Setting";
$i_SmartCard_DailyOperation_Check_Time_School_Time_Table="Whole School / Class Settings";
$i_SmartCard_DailyOperation_Check_Time_Group_Time_Table="Group Specific Settings";
$i_SmartCard_DailyOperation_Check_Time_Final_Time_Table="Setting summary";



$i_SmartCard_DailyOperation_BlindConfirmation_Notes="<b><u>Notes:</u></b>
<br><ul><li>Please check the daily attendance record before submit</li></ul>
";
$i_SmartCard_DailyOperation_BlindConfirmation_Notes1="Notes";
$i_SmartCard_DailyOperation_BlindConfirmation_Notes2="Please check the daily attendance record before submit";

$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll = "Show All Student";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs = "Show Only absence";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate = "Show Only Late";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate = "Show Absence And Late Only";

$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record = "Record is not yet confirmed";
$i_SmartCard_Frontend_Take_Attendance_In_School_Time = "In School Time";
$i_SmartCard_Frontend_Take_Attendance_In_School_Station = "In School Station";
$i_SmartCard_Frontend_Take_Attendance_Status = "Attend?";
$i_SmartCard_Frontend_Take_Attendance_PreStatus = "Status";
$i_SmartCard_Frontend_Take_Attendance_Waived = "Waived";
$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record_Update = "Record is not yet confirmed ( Data has been modified before submit ). Please confirm record and submit again.";
### Added On 25 Apr 2007
$i_SmartCard_Payment_alert_RecordAddSuccessful = "<font color=green>Record added successfully</font>";
$i_SmartCard_Payment_Student_Select_Instruction = "Select The Student(s)";
$i_SmartCard_Payment_Input_Amount_Instruction = "Please Input Amount";
$i_SmartCard_Payment_Photocopier_Quota_Purchase_Quantity = "Quantity";
### Added On 2 May 2007
$i_SmartCard_Payment_Input_PaymentItem_Instruction = "Please Input The Payment Item";
$i_SmartCard_Payment_Input_Quota_Instruction = "Please Input The Quota";
### Added On 3 May 2007
$i_SmartCard_Payment_PhotoCopier_TotalQuota = "Total Quota";
$i_SmartCard_Payment_PhotoCopier_UsedQuota = "Used Quota";
$i_SmartCard_Payment_PhotoCopier_Class_Select_Instruction = "Select The Class";
$i_SmartCard_Payment_PhotoCopier_LastestTransactionTime = "Lateset Transaction Time";
### Added On 4 May 2007
$i_SmartCard_Payment_PhotoCopier_SelectUser = "Select User";
### Added On 7 May 2007
$i_SmartCard_Payment_PhotoCopier_ResetQuota = "Are you sure to reset all students' quota?";
### Added On 8 May 2007
$i_SmartCard_Payment_PhotoCopier_ClassLevel_Select = "Select The Class Level";
$i_SmartCard_Payment_PhotoCopier_Class_Select = "Select The Class";
### Added On 9 May 2007
$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction = "Please Input The Total Quota";
$i_SmartCard_Payment_PhotoCopier_ClassLevel_Select_Instruction = "Please Select The Class Level";
$i_SmartCard_Payment_PhotoCopier_Class_Select_Instruction = "Please Select The Class";
$i_SmartCard_Payment_PhotoCopier_SelectUser_Instruction = "Please Select The User";
$i_SmartCard_Payment_photoCopier_UserType = "User Types";
$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid = "Invalid Total Quota";

$i_SmartCard_Payment_PhotoCopier_ChangeQuota = "Changed Quota";
$i_SmartCard_Payment_PhotoCopier_FinalQuota = "Final Quota";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType = "Record Type";

$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Reset_By_Admin = "Reset By Admin";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Add_By_Admin = "Add By Admin";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Purchasing = "Purchasing";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Copied_Deduction = "Copier Deduction";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Quota_Purchase_Quantity = "Quantity";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type = "Type";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_bw = "A4 Black and White";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_bw = "A4 Not Black and White";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_color = "A4 Color";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_color = "A4 Not Color";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_No_Record= "There is no record.";

### Added On 30 Apr 2007
$i_Payment_Menu_PhotoCopierQuotaSetting = "Photocopy Quota Settings";
$i_Payment_Menu_PhotoCopier_Package_Setting = "Photocopy Package Settings";
$i_Payment_Menu_PhotoCopier_Quota_Management = "Photocopy Quota Management";
$i_Payment_Menu_PhotoCopier_Quota_Record = "Photocopy Quota Records";
$i_Payment_Menu_PhotoCopier_Quota_Details = "Photocopy Quota Used Details";
$i_Payment_Menu_PhotoCopier_Quota_Purchase = "Purchase Photocopy Quota";
$i_Payment_Menu_PhotoCopier_View_Quota_Record = "View Photocopy Quota Records";
$i_Payment_Menu_PhotoCopier_TitleName = "Package Name";

$i_StudentPromotion = "Student Promotion System";
$i_StudentPromotion_Menu_Reset = "Reset All Student Promotion Data (Preparation Step)";
$i_StudentPromotion_Menu_ViewEditList = "Assign New Classes (Promotion Step 1)";
$i_StudentPromotion_Menu_EditClassNumber = "Assign New Class Numbers (Promotion Step 2)";
$i_StudentPromotion_Menu_Finalize = "Finalize the promotion (Not Used)";
$i_StudentPromotion_Menu_NewClassEffective = "Update Student Information to New Classes (Finalization Step 1)";
$i_StudentPromotion_Menu_ArchiveLeftStudents = "Archive School Leavers' Information (Finalization Step 2)";
$i_StudentPromotion_StudentAssigned = "Students Assigned to New Class";
$i_StudentPromotion_Unset = "Unassigned Students (Interpreted as School Leavers)";
$i_StudentPromotion_OldClass = "Current Class";
$i_StudentPromotion_OldClassNumber = "Current Class Number";
$i_StudentPromotion_NewClass = "New Class";
$i_StudentPromotion_NewClassNumber = "New Class Number";
$i_StudentPromotion_Type_ClassName = "$i_StudentPromotion_OldClass, $i_StudentPromotion_OldClassNumber, $i_StudentPromotion_NewClass, $i_StudentPromotion_NewClassNumber";
$i_StudentPromotion_Type_Login = "$i_UserLogin, $i_StudentPromotion_NewClass, $i_StudentPromotion_NewClassNumber";
$i_StudentPromotion_Prompt_PreparationDone = "<font color=green>Preparation completed</font>";
$i_StudentPromotion_Prompt_NewClassEffectiveDone = "<font color=green>New Class Information updated.</font>";
$i_StudentPromotion_Prompt_DataArchived = "<font color=green>Data Archived.</font>";
$i_StudentPromotion_Summary = "Summary";
$i_StudentPromotion_ConfirmFinalize = "Please click Submit to complete the whole process";
$i_StudentPromotion_ClickContinueToProceed = "Please click Continue to proceed. It may take sometime to process, please be patient.";
$i_StudentPromotion_Sorting_Name = "English Name";
$i_StudentPromotion_Sorting_BoysName = "Boys, English Name";
$i_StudentPromotion_Sorting_GirlsName = "Girls, English Name";
$i_StudentPromotion_GenerateClassNumber = "Generate Class Number";
$i_StudentPromotion_ClassSize = "Total";
$i_StudentPromotion_Unset_List = "Student List without new Class";
$i_StudentPromotion_RestoreLogin = "New $i_UserLogin";
$i_StudentPromotion_LoginDuplicated = "As original $i_UserLogin already used by other users";
$i_StudentPromotion_Description_ClassAssignment = "Click on the Current Class to assign the students to new classes.<br>Or click Import to assign new classes and new class numbers by importing a CSV file.";
$i_StudentPromotion_Notes_NewClassEffective = "<b><u>Notes:</u></b><br>
<ul><li>This step is <font color=red><b><u>IRREVERSIBLE</font></u></b>, please perform system backup before proceed.</li>
<li>After this process: <br>
    <ul>
       <li>All student information will be updated with the new classes and the new class numbers</li>
       <li><font color=red><u><b>Students without a new class and class number for the new school year, will be classified as school leavers.</b></u></font></li>
       <li>Students will be automatically assigned to the new class groups. All previous class groups will be archived.</li>
       <li>School Leavers (Unassigned Students) will NOT be able to login the system.</li>
       <li>If necessary, you may still assign School Leavers to new classes in User Mgmt Center (Promote)</li>
    </ul></li>
<li>Please ensure <u>The Completed Academic Year</u> is correct. The Academic Year should ONLY be updated to new academic year AFTER this step.</li>
</ul>
";
$i_StudentPromotion_NumLeftStudents = "Number of Students in Left Status";
$i_StudentPromotion_ArchivalYear = "Mark Year of Left as";
$i_StudentPromotion_Notes_ArchiveLeftStudents = "<b><u>Notes:</u></b><br>
<ul><li>This step is <font color=red><b><u>IRREVERSIBLE</font></u></b>, please perform system backup before proceed.</li>
<li>After this process:<br>
    <ul>
      <li>The Student Profiles of the School Leavers will be archived.</li>
      <li>You can view the archived student profiles in <u>Student Profile &gt; View Archived Student Profiles</u>.</li>
      <li>School Leavers cannot be re-assigned to new classes and student accounts will be deleted from User Mgmt Center</li>
    </ul></li>
</ul>
";
$i_StudentPromotion_Notes_SpecialPromote = "<b><u>Notes:</u></b><br>
<ul><li>This function is for SPECIAL use.</li>
<li>This step is <font color=red><b><u>IRREVERSIBLE</font></u></b>, please perform backup before proceed.</li>
<li>Please make sure this student has already enrolled, as this action will affect the class history of this student</li>
</ul>";
$i_StudentPromotion_Notes_MarkLeft = "<b><u>Notes:</u></b><br>
<ul><li>This function is for SPECIAL use (Leave in the middle of Academic Year).</li>
<li>This step is <font color=red><b><u>IRREVERSIBLE</font></u></b>, please perform backup before proceed.</li>
<li>Please make sure that this student has already withdrawn, as this action will affect the class history of this student</li>
</ul>";

$i_StudentPromotion_NoAutoAlumniAdd = "<font color=green>After this process, the students to be archived will <b>NOT</b> have alumni account automatically. You need to open accounts for them separately.</font>";
$i_StudentPromotion_AutoAlumni = "<font color=green>After this process, the students to be archived will automatically generate alumni accounts for them (Use current login names and passwords).</font>";
$i_StudentPromotion_AutoAlumni_NoGroupSet = "<font color=red>System cannot open alumni accounts (Reason: Alumni Group is not yet configured. Please contact Broadlearning Customer Support.)</font>";
$i_StudentPromotion_AutoAlumni_NoGroupExist = "<font color=red>System cannot open alumni accounts (Reason: Alumni Group does not exist/is already removed. Please contact Broadlearning Customer Support.)</font>";
$i_StudentPromotion_SpecialPromote = "Special Promote";
$i_StudentPromotion_SetToLeft = "Set to Left";
$i_StudentPromotion_PromoteTo = "Promote to";
$i_StudentPromotion_NewClassNumber = "New Class Number";

$i_StudentPromotion_Alert_Reset = "Are you sure to reset all student promotion data?";
$i_StudentPromotion_Alert_PromoteClass = "Please select new Class";
$i_StudentPromotion_Alert_NoClassNumber = "Are you sure to promote this student without Class Number?";
$i_StudentPromotion_Alert_RemoveRecord = "Are you sure to delete the student from this new class?";
$i_StudentPromotion_AcademicYear = "The Completed Academic Year";
$i_StudentPromotion_PlsChangeAfterPromotion = "Please proceed this step before changing to new Academic Year.";
$i_StudentPromotion_ImportFailed = "The following records cannot be imported.";

// for iPortfolio Groups Promote
$i_StudentPromotion_Menu_iPortfolioGroup = "Promote iPortfolio Students to New Classes";
$i_StudentPromotion_iPortfolioGroup_Summary = "iPortfolio Promotion Report";
$i_StudentPromotion_Alert_iPortfolioGroup = "Are you sure you want to promote iPortfolio students to new classes?";
$i_StudentPromotion_iPortfolioGroup_UserNum = "Number of students to be promoted";
$i_StudentPromotion_Current_AcademicYear = "New Academic Year";
$i_StudentPromotion_Confirm_iPortfolioGroup = "The process of iPortfolio Students Promotion has been finished.";
$i_StudentPromotion_iPortfolioRemind = "<b><u>Notes:</u></b><br><ul><li>In iPortfolio, students are put in Academic Year/Class groups (e.g. 2005-2006 1A). The promotion procedure will add students into a new Year/Class group without removing the original group information. Therefore, a student belonging to group \"2005-2006 1A\", will be added to group \"2006-2007 2A\" after the promotion. However, you can still find her record from the original group \"2005-2006 1A\".</ul></li>";

//Class History Management 2009-01-15
$i_StudentPromotion_mgt['ACADEMICYEAR'] = "School Year";
$i_StudentPromotion_mgt['NUMSTUDENT']	= "Number of Students";
$i_StudentPromotion_mgt['CLASS']		= "Class";
$i_StudentPromotion_mgt['CLASSNUM']		= "Class Number";
$i_StudentPromotion_mgt['STUDENT']		= "Student";
$i_StudentPromotion_mgt['CLASSHISTORY']	= "Class History";
$i_StudentPromotion_mgt['CONFIRMDELETE'][0]= "Are you sure you want to delete this students history?";
$i_StudentPromotion_mgt['CONFIRMDELETE'][1]= "Are you sure you want to delete all students from this class?";
$i_StudentPromotion_mgt['CONFIRMDELETE'][2]= "Are you sure you want to delete all students from this year?";
$i_StudentPromotion_mgt['CONFIRMDELETE'][3]= "Are you sure you want to delete ALL duplicated students history?";
$i_StudentPromotion_mgt['BACKTOTOP']	= "Back to the top";
$i_StudentPromotion_mgt['IMPORTDESC'][0]	= "User Login, Student, Academic Year, Class Name, Class Number";
$i_StudentPromotion_mgt['IMPORTDESC'][1]	= "WebSAMSRegNo, Student, Academic Year, Class Name, Class Number";
$i_StudentPromotion_mgt['YEARDUPLICATE']	= "Duplicate year. Please enter another school year.";
$i_StudentPromotion_mgt['EDITERROR']		= "Please fill in all fields";
$i_StudentPromotion_mgt['TOTAL'] = "Total";
$i_StudentPromotion_mgt['PREVIOUS']	= "Previous";
$i_StudentPromotion_mgt['NEXT']	= "Next";
$i_StudentPromotion_mgt['PAGE']	= "PAGE";
$i_StudentPromotion_mgt['NEXT']	= "Next";
$i_StudentPromotion_mgt['DISPLAY']	= "Display";
$i_StudentPromotion_mgt['PERPAGE']	= "/Page";
$i_StudentPromotion_mgt['USER']	= "User";
$i_StudentPromotion_mgt['NAV']['YEAR']		= "Class History Management";
$i_StudentPromotion_mgt['NAV']['CLASS']		= "Class Name History Management";
$i_StudentPromotion_mgt['NAV']['STUDENT']	= "Student History Management";
$i_StudentPromotion_mgt['NAV']['DUPLICATE']	= "Duplicate Records";
$i_StudentPromotion_mgt['NAV']['EDIT']		= "Edit History";
$i_StudentPromotion_mgt['NAV']['PROBLEM']	= "Problematic Records";
$i_StudentPromotion_mgt['BTNDUPLICATE']		= "Duplicate Records";
$i_StudentPromotion_mgt['BTNPROBLEMDATA']	= "Problematic Records";
$i_StudentPromotion_mgt['BTNSEARCH']		= "Search";
$i_StudentPromotion_mgt['NORECORD'] 		= "No Records Found";
$i_StudentPromotion_mgt['UNCHECKTOREMOVE']	= "Uncheck to remove";
$i_StudentPromotion_mgt['DUPSEARCH']		= "Search";
$i_StudentPromotion_mgt['EDIT_NOTE']		= "* Duplicate/ Problematic records";
$i_StudentPromotion_mgt['RECORD']			= "Record";
$i_StudentPromotion_mgt['OCCURRENCE']		= "Occurrence";
$i_StudentPromotion_mgt['DUPLICATE']		= "Duplicated";

$i_WebSAMS_Notice_Export="To export attendance data in WebSAMS format, please complete the following settings:<Br><Br>";
$i_WebSAMS_Notice_Export1="1. Enter the WebSAMS Level Name for each Class Level.<br><br>";
$i_WebSAMS_Notice_Export2="2. Enter the Basic Information of the School (e.g WebSAMS SchoolID, current WebSAMS School Year etc).<br><br>";
$i_WebSAMS_Notice_Export3="3. Enter the WebSAMS Reason Code(s).<br><Br>";
$i_WebSAMS_Notice_AttendanceCode="For Attendance Data Export in WebSAMS format";
$i_WebSAMSRegNo_Format_Notice="(The leading \"<B>#</b>\" is part of the format of WebSAMSReg. No)";
$i_WebSAMS_AttendanceCode_Export="Export Attendance Records";
$i_WebSAMS_AttendanceCode_No_RegNo_Students="Number of student(s) has no WebSAMS Registration No";
$i_WebSAMS_Registration_No ="WebSAMS Reg. No";
$i_WebSAMS_ClassLevelName ="WebSAMS Class Level Name";
$i_WebSAMS_ClassLevel ="WebSAMS Class Level";
$i_WebSAMS_Integration = "WebSAMS Integration";
$i_WebSAMS_Menu_Attendance = "Attendance Data Synchronization";
$i_WebSAMS_Menu_AttendanceCode = "Attendance Data Export in WebSAMS format";
$i_WebSAMS_Attendance_StartDate = "Start Date";
$i_WebSAMS_Attendance_EndDate = "End Date";
$i_WebSAMS_con_RecordSynchronized = "Date Synchronized Successfully";
$i_WebSAMS_AttendanceCode_Basic = "Basic Information";
$i_WebSAMS_AttendanceCode_ReasonCode = "Reason Code Management";
$i_WebSAMS_AttendanceCode_SchoolID = "WebSAMS School ID";
$i_WebSAMS_AttendanceCode_SchoolYear = "Current WebSAMS School Year";
$i_WebSAMS_AttendanceCode_SchoolLevel = "WebSAMS School Level";
$i_WebSAMS_AttendanceCode_SchoolSession = "WebSAMS School Session";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Kindergarten = "Kindergarten";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Primary = "Primary School";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Secondary = "Secondary School";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Others = "Others";

# Added On 13 Aug 2007
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_DependOnClassLevel = "Depend On Student Class Level";
####################

$i_WebSAMS_AttendanceCode_Option_SchoolSession_AM = "AM";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_PM = "PM";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_WD = "Whole Day";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_Evening = "Evening";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K1 = "K1 - Nursery Class";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K2 = "K2 - Lower Kindergarten";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K3 = "K3 - Upper Kindergarten";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_LP = "LP - Lower Preparatory";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P1 = "P1 - Primary 1";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P2 = "P2 - Primary 2";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P3 = "P3 - Primary 3";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P4 = "P4 - Primary 4";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P5 = "P5 - Primary 5";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P6 = "P6 - Primary 6";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_PP = "PP - Pre-Primary - Spec";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_PR = "PR - Primary - Spec";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S1 = "S1 - Secondary 1";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S2 = "S2 - Secondary 2";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S3 = "S3 - Secondary 3";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S4 = "S4 - Secondary 4";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S5 = "S5 - Secondary 5";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S6 = "S6 - Secondary 6";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S7 = "S7 - Secondary 7";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_SJ = "SJ - Junior Secondary";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_SS = "SS - Senior Secondary";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_UP = "UP - Upper Preparatory";
$i_WebSAMS_Attendance_Reason_ReasonCode = "Reason Code";
$i_WebSAMS_Attendance_Reason_ReasonText = "Reason Text";
$i_WebSAMS_Attendance_Reason_ReasonType="Reason Type";
$i_WebSAMS_Attendance_Reason_OtherReason="Other Reason";
$i_WebSAMS_Attendance_Reason_OtherReason_Notice="used when there is no Code ID can be mapped";
$i_WebSAMS_Attendance_Reason_Notice1="Use WEBSAMS Attendance reasons.";
$i_WebSAMS_ClassCode = "WebSAMS Class Code";
$i_WebSAMS_Class_ImportInstruction="<b><u>File Description:</u></b><br>
1st Column : Class Name (e.g. 1A ).<br>
2nd Column : Level Name (e.g. F.1 ).<br>
3rd Column : WebSAMS Class Code (e.g. 1B ).<br>
4th Column : WebSAMS Class Level (e.g. S1 ).<br>";

$i_WebSAMS_ReasonCode_ImportInstruction="<table border=0 cellspacing=1 cellpadding=1><tr><td align=right><b><u>File Description:</u></b><br></td></tr>
<tr><td align=right>1st Column : </td><td>Reason Code (e.g. 01 )</td></tr>
<tr><td align=right>2nd Column : </td><td>Reason Text (e.g. Sick Leave )</td></tr>
<tr><td align=right>3rd Column : </td><td>Reason Type (e.g. 1) :</td></tr>
<tr><td></td><td>1 - Absent</td></tr>
<tr><td></td><td>2 - Late</td></tr>
<tr><td></td><td>3 - Early Leave</td></tr>
</table>";
$i_WebSAMS_Attendance_Export_Warning1="Please fill in the following Information :<br><li>School ID</li><li>School Year</li><li>School Level</li></li>School Session</li>";
$i_WebSAMS_Attendance_Export_Warning2="The following Class Level has not mapped to the WebSAMS Class Level:";
$i_WebSAMS_Attendance_Export_Warning3="There are more than 10000 results, please narrow down the date range.";
$i_WebSAMS_Attendance_Reason_CurrenlyMapTo="Currently Using";

$i_Payment_UserType="User Type";
$i_Payment_All ="All";
## Added on 18-7-2007
$i_Payment_All_Users ="All Users";
$i_Payment_System = "Smart Card Payment System";
$i_Payment_System_Admin_User_Setting = "Set Admin User";
$i_Payment_Menu_Settings = "Basic Settings";

$i_Payment_Menu_DataImport = "Account Data Import";
$i_Payment_Menu_DataBrowsing = "Data Log Browsing";
$i_Payment_Menu_StatisticsReport = "Statistics and Report";
$i_Payment_Menu_CustomReport = "Customize Report";
$i_Payment_Menu_PrintPage = "Print Page";
$i_Payment_Menu_Settings_TerminalIP = "Terminal Settings";
$i_Payment_Menu_Settings_TerminalAccount = "Login Account for Terminal";
$i_Payment_Menu_Settings_PaymentCategory = "Payment Item Category Settings";
$i_Payment_Menu_Settings_PaymentItem = "Payment Item Settings";
$i_Payment_Menu_Settings_PaymentItem_SelectDateRange="Display all payment items within the following date range";
$i_Payment_Menu_Settings_PurchaseCategory = "Purchasing Item Category Settings";
$i_Payment_Menu_Settings_PurchaseItem = "Purchasing Item Settings";
$i_Payment_Menu_Settings_ClearTransaction = "Clear ALL Transaction Records";
$i_Payment_Menu_Settings_ResetAccount = "Reset Balance of ALL Accounts";
$i_Payment_Menu_Settings_Letter = "Payment Letter Settings";
$i_Payment_Menu_Settings_Letter_Header = "Letter Header";
$i_Payment_Menu_Settings_Letter_Footer = "Letter Footer";
$i_Payment_Menu_Settings_PPS_Setting = "PPS Setting";
$i_Payment_Menu_Settings_AlipayHK_Handling_Fee = "AlipayHK Handling Fee";
$i_Payment_Menu_Settings_Letter_Disable_Signature = "Hide Parent Signature Field";

$i_Payment_Menu_Import_PPSLink = "Student and PPS account Linkage";
$i_Payment_Menu_Import_PPSLog = "PPS Daily Log File";
$i_Payment_Menu_Import_CashDeposit = "Cash Deposit CSV File";
$i_Payment_Menu_Browse_TerminalLog = "Terminal Log";
$i_Payment_Menu_Browse_CreditTransaction = "Credit Transaction";
$i_Payment_Menu_Browse_CreditTransaction_DateRange = "Select Date Range";

$i_Payment_Menu_Browse_StudentBalance = "View Users' Balance and Transaction Record";
$i_Payment_Menu_Browse_StudentBalance_DirectPay = "View Users' Transaction Record";
$i_Payment_TransactionType_Income = "Income";
$i_Payment_Menu_Browse_TransactionRecord = "Transaction Log Details";
$i_Payment_Menu_Browse_PurchasingRecords = "Single Purchasing Records";
$i_Payment_Menu_Browse_MissedPPSBill = "Unknown PPS Credit Transaction Records";
$i_Payment_Menu_PrintPage_Outstanding = "Outstanding Payment List";
$i_Payment_Menu_PrintPage_AddValueReport = "Credit Transaction Report";
$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal="Payment (Total)";
$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount="Number of Transactions";
$i_Payment_Menu_PrintPage_AddValueReport_Date= "Date";
$i_Payment_Menu_Report_SchoolAccount = "School Account Report";
$i_Payment_Menu_Report_PresetItem = "Payment Item Report";
$i_Payment_Menu_Report_TodayTransaction = "Today's Transaction Report";

$i_Payment_Menu_PrintPage_Receipt = "Receipt";
$i_Payment_Menu_PrintPage_Receipt_PaymentItemList = "Payment Item List By Student";

$i_Payment_Import_Format_PPSLink2 = "<b><u>Description of import file:</u></b><br>
Column 1: $i_UserLogin<br>
Column 2: PPS Bill account number
";
$i_Payment_Import_Format_PPSLink = "<b><u>Description of import file:</u></b><br>
Column 1: Class Name<br>
Column 2: Class Number<br>
Column 3: PPS Bill account number
";

$i_Payment_Import_Format_PPSLog = "Please upload the daily log file from PPS";
$i_Payment_Import_Format_PPSLog_Charge_Deduction = "PPS handling charge ($".LIBPAYMENT_PPS_CHARGE.", while Counter Bill is $".LIBPAYMENT_PPS_CHARGE_COUNTER_BILL.") will be deducted for each transaction item.";
$i_Payment_Import_PPSLink_Result = "Import Result";
$i_Payment_Import_PPSLink_Result_Summary_TotalRows = "Total Rows";
$i_Payment_Import_PPSLink_Result_Summary_Successful = "Successful";
$i_Payment_Import_PPSLink_Result_Summary_NoMatch = "No Matched Student(s)";
$i_Payment_Import_PPSLink_Result_Summary_Occupied = "PPS Number Occupied";
$i_Payment_Import_PPSLink_Result_Summary_Missing = "Missing PPS Number";
$i_Payment_Import_PPSLink_Result_Summary_ExistAlready = "Student has PPS Number already";
$i_Payment_Import_PPSLink_Result_ErrorMsg_NotSuccessful = "The following records cannot be updated";
$i_Payment_Import_PPSLink_Result_ErrorMsg_NoMatch = "No matched student.";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Occupied = "PPS account has been used.";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Missing = "PPS account Number is missing.";
$i_Payment_Import_PPSLink_Result_ErrorMsg_ExistAlready = "Student has PPS account already. Please remove linkage first.";
$i_Payment_Import_PPSLink_Result_GoToIndex = "Go back to index";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason = "Reason";
$i_Payment_Import_PPS_FileType = "File Type";
$i_Payment_Import_PPS_FileType_Normal = "IEPS File (By Phone or Internet)";
$i_Payment_Import_PPS_FileType_CounterBill = "Counter Bill File (e.g. by Circle K)";
$i_Payment_PPS_Charge = "PPS Charge";

$i_Payment_Field_PPSAccountNo = "PPS Account No.";
$i_Payment_Field_CreditAmount = "Credit Amount";
$i_Payment_Field_CreditAmount_AfterDeduction = "Credit Amount After Charge Deduction";
$i_Payment_Field_TransactionTime = "Transaction Time";
$i_Payment_Field_PostTime = "Post Time";
$i_Payment_Field_Username = "Username";
$i_Payment_Field_PaymentAllowed = "For Payment";
$i_Payment_Field_PurchaseAllowed = "For Purchasing/Add-value";
$i_Payment_Field_LastLogin = "Last Login";
$i_Payment_Field_AccountCreation = "Account Created";
$i_Payment_Field_RefCode = "Ref Code";
$i_Payment_Field_Balance = "Balance";
$i_Payment_Field_LastUpdated = "Last Updated";
$i_Payment_Field_LastUpdatedBy = "Last Updated Admin";
$i_Payment_Field_AdminHelper = "in Admin Console";
$i_Payment_Field_TerminalUser = "in Terminal";
$i_Payment_Field_TransactionType = "Transaction Type";
$i_Payment_Field_TransactionDetail = "Details";
$i_Payment_Field_PaymentCategory = "Payment Category";
$i_Payment_Field_DisplayOrder = "Display Order";
$i_Payment_Field_PaymentItem = "Payment Item";
$i_Payment_Field_PaymentCategory_Code = "Payment Category Code";
$i_Payment_Warning_Enter_PaymentCategoryCode = "Please enter Payment Category Code";
$i_Payment_Warning_Duplicate_PaymentCategoryCode = "Duplicate Payment Category Code";
$i_Payment_Field_PaymentItemCode = "Payment Item Code";
$i_Payment_Warning_Enter_PaymentItemCode = "Please enter Payment Item Code";
$i_Payment_Warning_Duplicate_PaymentItemCode = "Duplicate Payment Item Code";
$i_Payment_Warning_Select_PaymentMethod = "Please select Payment Method";
$i_Payment_Field_PaymentDefaultAmount = "Default Amount";
$i_Payment_Field_Amount = "Amount";
$i_Payment_Field_Function = "Function";
$i_Payment_Field_ChangePassword = "Need to change password";
$i_Payment_Field_LoginTime = "Login Time";
$i_Payment_Field_LogoutTime = "Logout Time";
$i_Payment_Field_IP = "Terminal IP Address";
$i_Payment_Field_CurrentBalance = "Current Balance";
$i_Payment_Field_BalanceAfterPay = "Balance After Pay";
$i_Payment_Field_BalanceAfterUndo = "Balance After Undo";
$i_Payment_Field_PaidTime = "Time of paid";
$i_Payment_Field_PaymentDeadline = "Deadline";
$i_Payment_Field_BalanceAfterTransaction = "Balance After Transaction";
$i_Payment_Field_PayPriority = "Priority";
$i_Payment_Field_NewDefaultAmount = "New $i_Payment_Field_PaymentDefaultAmount";
$i_Payment_Field_SinglePurchase_Unit = "Payment Receive Unit";
$i_Payment_Field_SinglePurchase_TotalAmount = "Amount Received";
$i_Payment_Field_SinglePurchase_Count = "Number of Transactions";
$i_Payment_Field_AdminInCharge = "Admin In Charge";
$i_Payment_Field_RecordedAccountNo = "Recorded $i_Payment_Field_PPSAccountNo";
$i_Payment_Field_MappedStudent = "Mapped student";
$i_Payment_Field_PaidCount = "Paid";
$i_Payment_Field_TotalPaidCount = "Total";

$i_Payment_Empty_Will_Be_Genreated_By_System = "empty will be generated by System";

$i_Payment_Field_AllPaid = "Settled";
$i_Payment_Field_NotAllPaid = "Outstanding";

$i_Payment_Field_PaidCount = "# of Student Paid";
$i_Payment_Field_TotalPaidCount = "Total";
$i_Payment_Field_TransactionFileTime = "Add value Record Time";

### Added On 3 Aug 2007 ###
$i_Payment_Menu_ManualCashDeposit = "Manual Cash Deposit";
$i_Payment_CashDepositDate = "Date of Cash Deposit";
$i_Payment_CashDepositTime = "Time of Cash Deposit";
###########################

### Added On 7 Aug 2007 ###
$i_Payment_Field_SetAllUse = "Set All To Use";
$i_Payment_AmountWarning = "Please Input Amount";
$i_Payment_RefCodeWarning = "Please Input Ref Code";
$i_Payment_DepositDateWarning = "Please Input Date of Cash Deposit";
###########################

### Added On 8 Aug 2007 ###
$i_Payment_DetailTransactionLog = "Detail Transaction Log";
$i_Payment_DepositWrongDateWarning = "Please Input Correct Date of Cash Deposit";
$i_Payment_DepositAmountWarning = "Please Input Correct Amount";
###########################

### Added  On 30 Apr 2007
$i_Payment_Field_Quota = "Quota";

### Sentence is too long for using linterface->GET_SYS_MSG()
$i_Payment_Import_ConfirmRemoval = "<table class='systemmsg' width='50%' border='0' cellpadding='3' cellspacing='0'><tr><td>System detected that there are some existing intermediate records not yet confirmed. This is due to confirmation not yet finished or another administrator is importing data.<br />";
$i_Payment_Import_ConfirmRemoval2 = "You need to wait (refresh again to check) or you can remove the intermediate records by clicking button Delete before you can import new log file.</td><tr></table>";

$i_Payment_Import_Confirm = "Please click button Confirm to proceed the process or click Cancel to clear the intermediate results.";
$i_Payment_AccountNoMatch = "This PPS account number has not been registered.<font color=red>*</font>";
$i_Payment_AccountNoMatch_Remark="This transaction record will be treated as Unknown PPS Credit Transaction Record. Please visit <b>$i_Payment_Menu_DataBrowsing</b> &gt; <b>$i_Payment_Menu_Browse_MissedPPSBill</b> to re-assign a student to this PPS account number.";
$i_Payment_Import_Error="Record import failure. Please check if the CSV file is correct and import the file once again.";
//$i_Payment_Import_NoMatch_Entry=" - The record cannot be recognized due to incorrect class name and class number.";
//$i_Payment_Import_NoMatch_Entry2=" - The record cannot be recognized due to incorrect user login.";
$i_Payment_Import_NoMatch_Entry="Incorrect class name and class number";
$i_Payment_Import_NoMatch_Entry2="Incorrect user login";
$i_Payment_Import_InvalidAmount="Invalid amount.";
$i_Payment_Import_DuplicatedStudent="Duplicated Records";
$i_Payment_Import_PayAlready="The student had paid already";

$i_Payment_Import_CashDeposit_Instruction = "
<span class=extraInfo>For Student Only</span><Br><br>
Column 1 : Class Name<br>
Column 2 : Class Number<br>
Column 3 : Amount of Deposit<br>
Column 4 : Date Time of Deposit (YYYY-MM-DD HH:mm:ss, empty will be interpreted as now)<br>
Column 5 : $i_Payment_Field_RefCode (empty will be generated by System)
";
$i_Payment_Import_CashDeposit_Instruction2 = "
<span class=extraInfo>For Student and Teacher</span><Br><br>
Column 1 : $i_UserLogin<br>
Column 2 : Amount of Deposit<br>
Column 3 : Date Time of Deposit (YYYY-MM-DD HH:mm:ss, empty will be interpreted as now)<br>
Column 4 : $i_Payment_Field_RefCode (empty will be generated by System)
";


$i_Payment_Auth_PaymentRequired = "Payment Terminal does NOT Require Login Authentication";
$i_Payment_Auth_PurchaseRequired = "Purchase/Add-value Terminal does NOT Require Login Authentication";

$i_Payment_Credit_Method = "Credit Method";
$i_Payment_Credit_TypePPS = "PPS";
$i_Payment_Credit_TypeCashDeposit = "Cash Deposit";
$i_Payment_Credit_TypeAddvalueMachine = "Add Value Machine";
$i_Payment_Credit_TypeUnknown = "No Record";

$i_Payment_TransactionType_Credit = "Credit";
$i_Payment_TransactionType_Payment = "Payment";
$i_Payment_TransactionType_Purchase = "Purchase";
$i_Payment_TransactionType_TransferTo = "Transfer To";
$i_Payment_TransactionType_TransferFrom = "Transfer From";
$i_Payment_TransactionType_CancelPayment = "Cancel Payment";
$i_Payment_TransactionType_Refund = "Refund";
$i_Payment_TransactionType_Debit = "Debit";
$i_Payment_TransactionType_Other = "Other";

$i_Payment_TransactionDetailPPS = "PPS 增值 / PPS Credit";
$i_Payment_TransactionDetailCashDeposit = "現金增值 / Cash Deposit";
$i_Payment_ClassInvolved = "Classes Involved";
$i_Payment_NoClass = "If not whole class payment, please use import after created this item.";

$i_Payment_PaymentStatus_NotStarted="Not Started";
$i_Payment_PaymentStatus_Paid = "Paid";
$i_Payment_PaymentStatus_Unpaid = "Unpaid";
$i_Payment_Payment_PaidCount = "Number of Students Paid";
$i_Payment_Payment_UnpaidCount = "Number of Students Unpaid";
/*
$i_Payment_Payment_StudentImportFileDescription1 = "
Column 1: Class Name <br>
Column 2: Class Number <br>
Column 3: Amount to be paid<br>";
*/
$i_Payment_Payment_StudentImportFileDescription1 = "Class Name, Class Number, Amount to be paid";
$i_Payment_Payment_StudentImportFileDescription2 = "User Login, Amount to be paid";

$i_Payment_Payment_StudentImportNotice = "
Please note that:<br>
- Paid students will not be affected<br>
- Blank in amount means the student no need to complete this payment item.<br>
";

$i_Payment_con_PaidNoEdit = "<font color=red>Cannot edit paid records.</font>";
$i_Payment_con_PaymentProcessed = "<font color=green>Payment Processed Successfully.</font>";
$i_Payment_con_PaymentUndo = "<font color=green>Payment Undo Successfully.</font>";
$i_Payment_con_PaymentProcessFailed = "<font color=red>Payment Processed Failed.</font>";
$i_Payment_con_RefundFailed = "<font color=red>Refund failure (maybe balance is $0)</font>";
$i_Payment_con_RecordMapped = "<font color=red>Record has been already handled.</font>";
$i_Payment_con_FileImportedAlready = "<font color=red>This file has been imported previously.</font>";

$i_Payment_alert_selectOneFunction = "You should choose at least 1 function";
$i_Payment_alert_forcepay = "Are you sure you want to force selected student(s) to pay?";
$i_Payment_alert_undopay = "Are you sure to undo the payment of selected student(s)?";
$i_Payment_alert_refund = "Are you sure to refund selected student(s)? (This is an IRREVERSIBLE action)";

// add by kenneth chung on 20090112
$i_Payment_alert_forcepayall = "Are you sure you want to force student(s) to pay?";

$i_Payment_action_batchpay = "Pay";
$i_Payment_action_undo = "Undo to unpaid";
$i_Payment_action_proceed_payment = "Proceed To Pay";
$i_Payment_action_proceed_undo = "Proceed To Undo";
$i_Payment_action_cancel_payment = "Cancel";
$i_Payment_action_refund = "Refund";
$i_Payment_action_handle = "Handle";

$i_Payment = "Payment";
$i_Payment_AccountBalance = "Account Balance and Transaction Records";
$i_Payment_PaymentRecord = "Payment Records";
$i_Payment_ValueAddedRecord = "Add Value Records";
$i_Payment_Transfer = "Transfer to another child";
$i_Payment_SelectChildToTransfer = "Transfer to";
$i_Payment_TransferAmount = "Transfer Amount";
$i_Payment_ItemSummary = "Summary";
$i_Payment_ItemPaid = "Amount Paid";
$i_Payment_ItemUnpaid = "Amount Unpaid";
$i_Payment_ItemTotal = "Total Amount";
$i_Payment_RefundAmount = 'Refund Amount';
$i_Payment_Students = "student(s)";
$i_Payment_Refunded = " refunded";
$i_Payment_ChangeDefaultAmount = "Change $i_Payment_Field_PaymentDefaultAmount";
# Added on 17-7-2007
$i_Payment_Item_Select_Class_Level = "Whole Level";

### Added on 6 Sep 2007
$i_Payment_UpdateRecordConfirm = "Are You Sure To Update The Record?";

$i_Payment_Note_Priority = "Smallest value has highest priority";

$i_Payment_Title_Receipt_Payment = "Payment Receipt";
$i_Payment_Title_Overall_SinglePurchasing = "Overall Single Purchasing Stats";
$i_Payment_Title_SinglePurchasing_Detail = "Single Purchasing Detail";

$i_Payment_SearchResult = "Search Result";
$i_Payment_IncreaseSensitivity = "Increase Sensitivity (less search results will appear)";
$i_Payment_DecreaseSensitivity = "Decrease Sensitivity (more search results will appear)";
$i_Payment_ManualSelectStudent = "Manual Select";
$i_Payment_ShowAll = "Show all";
$i_Payment_ShowUnlink = "Show not handled";
$i_Payment_ShowHandled = "Show Handled";
$i_Payment_Exclude = "Exclude Classes with No Records<Br> <font color=red>(not applicable to Payment Letter)</font>";
$i_Payment_IncludeNonOverDue = "Include All Non-OverDue Items";

$i_Payment_TotalAmount = "Total Amount";
$i_Payment_TotalCountTransaction = "Number of transaction";

// Add on 20-2-2009
$i_Payment_Receipt_Person_In_Charge = "Person in charge";

$i_Payment_Receipt_Principal = "Principal";
$i_Payment_Receipt_SchoolChop = "Chop";
$i_Payment_Receipt_Date = "Date";
$i_Payment_Receipt_ThisIsComputerReceipt = "This is a computer-generated receipt";
$i_Payment_Receipt_Remark = "Remark";
$i_Payment_Receipt_UpTo = "Up to";
$i_Payment_Receipt_AccountBalance = "payment account balance";
$i_Payment_Receipt_Official_Receipt = "Official Receipt";
$i_Payment_Receipt_Computer_GeneratedReceipt = "Computer-generated Receipt";
$i_Payment_Receipt_ReceivedFrom = "Received from";
$i_Payment_Receipt_PaidAmount = "paid amount HKD";
$i_Payment_Receipt_ForItems = ", for paying the following fees:";
$i_Payment_Receipt_Payment_Category = "Category";
$i_Payment_Receipt_Payment_Item = "Item";
$i_Payment_Receipt_Payment_Description = "Description";
$i_Payment_Receipt_Payment_Amount = "Amount";
$i_Payment_Receipt_Payment_Total = "Total";
$i_Payment_Receipt_Payment_ReceiptCode = "Receipt Code";
$i_Payment_Receipt_Payment_StaffInCharge_Name = "Staff In Charge";
$i_Payment_Receipt_Payment_StaffInCharge_Signature = "Staff Signature";
$i_Payment_Receipt_Payment_Date = "Date of Issued";
$i_Payment_PrintPage_Outstanding_Total = "Total Balance Due";
$i_Payment_PrintPage_Outstanding_AccountBalance = "Account Balance";
$i_Payment_PrintPage_Outstanding_DueDate = "Due Date";
$i_Payment_PrintPage_PaymentLetter = "Payment Letter";
$i_Payment_PrintPage_PaymentLetter_ParentSign = "Parent's Signature";
$i_Payment_PrintPage_PaymentLetter_Date = "Date";
$i_Payment_PrintPage_PaymentLetter_UnpaidItem = "Unpaid Item(s)";
$i_Payment_PrintPage_Browse_UserIdentityWarning = "Please Select The Identity";
$i_Payment_PrintPage_Browse_UserStatusWarning = "Please Select The Status";
$i_Payment_PrintPage_StudentOnly = "Student Only";

$i_Payment_PrintPage_Outstanding_LeftAmount = "Inadequate Account Balance";
$i_Payment_Note_StudentRemoved = "<font color=red>*</font> - Represents the user account is removed.";
$i_Payment_Note_PPSCostExport = "The date range counts the transaction time stated in PPS log file";

### added on 11 Oct 2007
$i_Payment_PhotocopierQuotaNotValid_Warning = "Please Input A Valid Quantity";
$i_Payment_PhotocopierPackageQtyNotValid_Warning = "Please Input A Valid Quantity";
$i_Payment_PhotocopierQuotaSelectPackage_Warning = "Please Select A Package";
$i_Payment_PhotocopierQuotaPackageName_Warning = "Please Input A Package Name";
$i_Payment_PhotocopierQuotaPriceNotValid_Warning = "Please Input A Valid Amount";

$i_Payment_SchoolAccount_Detailed_Transaction="Income/Expenses";
$i_Payment_SchoolAccount_Deposit="Income";
$i_Payment_SchoolAccount_Expenses="Expenses";
$i_Payment_SchoolAccount_PresetItem="Preset Payment Item";
$i_Payment_SchoolAccount_Total="Total";
$i_Payment_SchoolAccount_SinglePurchase="Single Purchase";
$i_Payment_SchoolAccount_TotalIncome="Total Income";
$i_Payment_SchoolAccount_TotalExpense="Total Expense";
$i_Payment_SchoolAccount_NetIncomeExpense="Net Income (Expense)";
$i_Payment_SchoolAccount_Other="Other(s)";
$i_Payment_PresetPaymentItem_Item ="Item(s)";
$i_Payment_PresetPaymentItem_PaidCount ="Paid";
$i_Payment_PresetPaymentItem_UnpaidCount ="Unpaid";
$i_Payment_PresetPaymentItem_PaidAmount ="Amount Paid";
$i_Payment_PresetPaymentItem_UnpaidAmount ="Amount Unpaid";
$i_Payment_PresetPaymentItem_ClassStat ="Class Stats";
$i_Payment_PresetPaymentItem_Personal ="Student Records";
$i_Payment_PresetPaymentItem_Progress ="In-Progress";
$i_Payment_PresetPaymentItem_PaymentPeriod="Payment Period";
$i_Payment_PaymentStatus_Ended="Ended";
$i_Payment_PresetPaymentItem_PaidStudentCount="Paid Count";
$i_Payment_PresetPaymentItem_UnpaidStudentCount="Unpaid Count";
$i_Payment_Menu_StatisticsReport_CreateTime="Report Generation Time";
$i_Payment_Warning_Enter_PaymentItemName="Please enter Item Name";
$i_Payment_Warning_Invalid_DisplayOrder ="Invalid display order";
$i_Payment_Warning_Invalid_PayPriority ="Invalid pay priority";
$i_Payment_Warning_Enter_NewDefaultAmount="Please enter new default amount";
$i_Payment_Warning_Enter_DefaultAmount="Please enter default amount";
$i_Payment_Warning_Item_Has_Paid_Student = "The item(s) selected has/have been paid by student(s). Please undo the payment(s) concerned before delete.";
$i_Payment_Warning_Item_No_Has_Paid_Student="The item(s) selected has/have not been paid by any student. You may safely delete the item(s).";
$i_Payment_Class_Payment_Report="Class Payment Report";
$i_Payment_Class_Payment_Report_Warning="There are over 256 payment items in the date range specified. Please try a smaller date range.";
$i_Payment_PresetPaymentItem_NoNeedToPay ="--";
$i_Payment_Menu_Settings_Letter_Header2 = "Letter Header 2";
$i_Payment_Warning_InvalidAmount ="Invalid Amount";
$i_Payment_Total_Balance ="Total Balance";
$i_Payment_Total_Users ="Total Users";
$i_Payment_Menu_Settings_PaymentItem_Import_Action_New="New Student";
$i_Payment_Menu_Settings_PaymentItem_Import_Action_Update="Update Amount";
$i_Payment_Menu_Settings_PaymentItem_Import_Action_NoChange="No Change (paid)";
$i_Payment_Menu_Settings_PaymentItem_Import_Action="Type";
$i_Payment_Menu_Settings_PaymentItem_ArchivedRecord="Archived";
$i_Payment_Menu_Settings_PaymentItem_ActiveRecord="Current";
$i_Payment_Menu_Settings_PaymentItem_Archive_Warning="Are you sure to archive the items?";
$i_Payment_Menu_Settings_PaymentItem_UndoArchive_Warning="Are you sure to undo archive?";
$i_Payment_Menu_Settings_PaymentItem_Archive_Warning2="The following items has been archived, no further payment allowed :";

### Addded On 23 Aug 2007 ###
$i_Payment_Teacher_PaymentRecord = "View Teacher Personal Payment Record";
#############################


$i_Payment_Subsidy_Option_No="N/A";
$i_Payment_Subsidy_Current_Amount="Current Amount";
$i_Payment_Subsidy_New_Amount="New Amount";
$i_Payment_Subsidy_Current_Subsidy_Amount="Current Subsidy Amount";
$i_Payment_Subsidy_New_Subsidy="New Subsidy Amount";
$i_Payment_Subsidy_Amount="Subsidy Amount";
$i_Payment_Subsidy_Unit="Source of Subsidy";
$i_Payment_Subsidy_UnitName=$i_Payment_Subsidy_Unit;
$i_Payment_Subsidy_UnitCode = 'Source Of Subsidy Code';
$i_Payment_Subsidy_UnitCode_Duplicate = 'Duplicate Source Of Subsidy Code';
$i_Payment_Import_SubsidyNotFound = "Invalid Source of subsidy";
$i_Payment_Import_InvalidSubSidyAmount = "Invalid Source Of Subsidy Amount";
$i_Payment_Import_SourceOfSubsidyNotEnoughMoney = "Source of subsidy does not have enough money";
$i_Payment_Subsidy_Edit_Confirm="Are you sure to submit?";
$i_Payment_Subsidy_Total_Subsidy_Amount ="Total Subsidy Amount";
$i_Payment_Subsidy_Total_Subsidy_Amount_Detail = "Total Subsidy Amount Detail";
$i_Payment_Subsidy_Total_Amount ="Total";
$i_Payment_Subsidy_Setting="Source of Subsidy Settings";
$i_Payment_Subsidy_Warning_Enter_UnitName="Please enter ".$i_Payment_Subsidy_UnitName;
$i_Payment_Subsidy_Used_Amount="Consumed";
$i_Payment_Subsidy_Left_Amount="Balance";
$i_Payment_Subsidy_Search_Amount="Amount consumed by the following student(s)";
$i_Payment_Subsidy_Unit_Admin ="Last Updated";
$i_Payment_Subsidy_Unit_UpdateTime="Update Time";
$i_Payment_StudentStatus_Removed="Deleted student(s)";
$i_Payment_StudentStatus_NonRemoved="Current student(s)";
$i_Payment_PPS_Export_Choices_1="$i_status_suspended, $i_status_approved, $i_status_pendinguser student(s).";
$i_Payment_PPS_Export_Choices_2="$i_status_graduate student(s).";
$i_Payment_PPS_Export_Choices_3=$i_Payment_StudentStatus_Removed.". <span class='extraInfo'>(marked with </span><b>*</b><span class='extraInfo'>)</span>";
$i_Payment_PPS_Export_Select_Student="Student Type";
$i_Payment_PPS_Export_Warning="Please select ".$i_Payment_PPS_Export_Select_Student;
$i_Payment_Note_StudentRemoved2 = "* - Represents the user account is removed.";
$i_Payment_Note_StudentLeft = "# - Representing that the person has left.";
$i_Payment_OutstandingAmount="Outstanding Amount";
$i_Payment_OutstandingAmountLeft="Amount Left";
$i_Payment_Subsidy_Warning_Select_SubsidyUnit="Please select ".$i_Payment_Subsidy_Unit;
$i_Payment_Subsidy_Warning_Enter_New_SubsidyAmount=$i_Payment_Subsidy_New_Subsidy." must be greater than 0.";
$i_Payment_Subsidy_Warning_NotEnough_Amount="Not enough ".$i_Payment_Subsidy_Unit." ".$i_Payment_Subsidy_Left_Amount;
$i_Payment_PaymentItem_Pay_Warning="Student(s) with a negative balance after the payment will NOT be processed.";
$i_Payment_Refund_Warning="The account(s) without balance will NOT be refunded.";
$i_Payment_Subsidy_Warning_TotalAmount_MustGreaterThan_ConsumedAmount=$i_Payment_Subsidy_Total_Amount." must NOT less than ".$i_Payment_Subsidy_Used_Amount." ".$i_Payment_Field_Amount;
$i_Payment_Class_Payment_Report_Display_Mode="Display Mode";
$i_Payment_Class_Payment_Report_Display_Mode_1="List students by row, payment items by column";
$i_Payment_Class_Payment_Report_Display_Mode_2="List payment items by row, students by column";
$i_Payment_PresetPaymentItem_ClassStat_Subsidy_Plus_Paid="Total Amount(Paid+Subsidy)";
$i_Payment_Class_Payment_Report_NoNeedToPay="No need to pay";
$i_Payment_Class_Item_Type="Record Type";
$i_Payment_Report_Print_Date="Print Date";
$i_Payment_Class_Payment_Report_Notice="The maximum number of students per page is 20.";
$i_Payment_Class_Payment_Report_No_of_Items="Items per page";
$i_Payment_Search_By_TransactionTime="By ".$i_Payment_Field_TransactionTime;
$i_Payment_Search_By_PostTime="By ".$i_Payment_Field_PostTime;
$i_Payment_Field_Total_Chargeable_Amount="Total Amount Payable";
$i_Payment_Field_Chargeable_Amount="Amount Payable";
$i_Payment_Field_Current_Chargeable_Amount="Current Amount Payable";
$i_Payment_Field_New_Chargeable_Amount="New Amount Payable";
$i_Payment_NoTransactions_Processed="No credit transactions has been processed.";

### added on 5 jun 2008 ###
$i_Payment_ViewStudentAccount_InvalidStartDate = "Invaild Start Date.";
$i_Payment_ViewStudentAccount_InvalidEndDate = "Invaild End Date.";
$i_Payment_ViewStudentAccount_InvalidDateRange = "Invaild Date Range.";
###########################

### Added on 20080813 ###
$i_Payment_Menu_Report_Export_StudentBalance = "Export Users' Balance and Transaction Record";
$i_Payment_Menu_Report_StudentBalance = "Users' Balance and Transaction Record";
$i_Payment_Menu_Report_Export_StudentBalance_Alert1 = "Please select the class level";
$i_Payment_Menu_Report_Export_StudentBalance_Alert2 = "Please select the class(es)";
$i_Payment_Menu_Report_Export_StudentBalance_Alert3 = "Please select the student(s)";
$i_Payment_Menu_Report_Export_StudentBalance_MultiSelect = "To make multiple selections, hold the CTRL key while you click on the items.";
$i_Payment_Menu_Browse_StudentBalance_Status_Left_Temp = "Left (Temporary)";
$i_Payment_Menu_Browse_StudentBalance_Status_Left_Archived = "Left (Archived)";
#########################

// added on 20090407
$Lang['Payment']['CancelDeposit'] = "Cancel Deposit Record";
$Lang['Payment']['CSVImportDeposit'] = "CSV Import";
$Lang['Payment']['ManualDeposit'] = "Manual Input";

// added on 20090415
$Lang['Payment']['CancelDepositDescription'] = "Cancel Cash Deposit Record.";
$Lang['Payment']['CashDeposit'] = "Cash Deposit";
$Lang['Payment']['CancelDepositWarning'] = "Are you sure you want to cancel these record?  (This is an IRREVERSIBLE action)";
$Lang['Payment']['DonateBalanceWarning'] = "Are you sure you want to donate these balance to school?  (This is an IRREVERSIBLE action)";
$Lang['Payment']['DonateBalance'] = "Donate to school";
$Lang['Payment']['DonateBalanceDescription'] = "Balance donation to school.";
$Lang['Payment']['Donated'] = "donated";
$Lang['Payment']['DonateFailed'] = "<font color=red>Donation failure (maybe balance is $0)</font>";
$Lang['Payment']['DepositCancelSuccess'] = "<font color=green>Deposit(s) cancelled.</font>\n";
$Lang['Payment']['CancelDepositInvalidWarning'] = "Some of the cash deposit item cannot be checked/ cancelled, because their balance is not enough.";

// added on 20090708
$Lang['Payment']['CancelDepositReport'] = "Cancel Deposit Report";
$Lang['Payment']['DonationReport'] = "Donation Report";
$Lang['Payment']['Keyword'] = "Keyword";

// added on 20090803
$Lang['Payment']['ArchiveUserLegend'] = "<font color=red>*</font> - Archived user";

$i_StaffAttendance_InputTime_Warning1="In-Time should not greater than Duty End Time";
$i_StaffAttendance_InputTime_Warning2="This staff is not required to take attendance on the selected day";
$i_StaffAttendance_InputTime_Warning3="The system has time record already";
$i_StaffAttendance_InputTime_Warning4="Out-Time should not smaller than Duty Start time";
$i_StaffAttendance_InputTime_Confirmation="Please make sure the input time is correct. Are you sure to add the time?";

$i_StaffAttendance_ManualTimeAdjustment ="Manual Time Adjustment";
$i_StaffAttendance_Outing_warning="The following staff has Holiday Preset on the selected day already";
$i_StaffAttendance_Holiday_warning="The following staff has Outgoing Preset on the selected day already";
$i_StaffAttendance_msg_password1 = "Password changed.";
$i_StaffAttendance_import_invalid_entries="Invalid Import Data";
$i_StaffAttendance_msg_invalid_number="Invalid input number";
$i_StaffAttendance_separator = "<img src=/images/staffattend/arrow.gif border=0 align=absmiddle hspace=5>";
$i_StaffAttendance_System = "eClass Staff Smart Card Attendance System";
$i_StaffAttendance_SystemSettings = "System Settings";
$i_StaffAttendance_SystemSettings_Terminal = "Terminal Settings";
$i_StaffAttendance_SystemSettings_ChangePassword = "Change Password";
$i_StaffAttendance_StaffSettings = "Staff Information";
$i_StaffAttendance_StaffSettings_Group = "Group Settings";
$i_StaffAttendance_StaffSettings_Staff = "Staff Information";
$i_StaffAttendance_DataManagement = "Data Management";
$i_StaffAttendance_DataManagement_RawLog = "Raw Log";
$i_StaffAttendance_DataManagement_ViewArchive = "View/Archive Raw Records";
$i_StaffAttendance_DataManagement_Outing = "Outing Records";
$i_StaffAttendance_DataManagement_Leave = "Leave Records";
$i_StaffAttendance_Report = "Report";
$i_StaffAttendance_Report_Day = "Daily";
$i_StaffAttendance_Report_Week = "Weekly";
$i_StaffAttendance_Report_Staff = "Individual Staff";
$i_StaffAttendance_Report_Summary = "Summary";
$i_StaffAttendance_Report_Day_Range_Abs = "Staff Absence Report";
$i_StaffAttendance_Report_Day_Range_Abs_By_Name = "Staff Leave/ Absence Report";
$i_StaffAttendance_PageTitle_Day = "Staff Attendance";
$i_StaffAttendance_Report_Staff_Waived = "Waived";
$i_StaffAttendance_Report_Date = "Date";
$i_StaffAttendance_Report_Date_Title = "Date";
$i_StaffAttendance_Report_Number_Of_Date = "Date Taken";
$i_StaffAttendance_Report_Monthly = "Monthly";

$i_StaffAttendance_LogOut="Logout";
$i_StaffAttendance_Find="Find";

$i_StaffAttendance_Greeting = "Please Select from menu on left";

$i_StaffAttendance_Group = "Working Time Group";
$i_StaffAttendance_Group_ID = "Group ID";
$i_StaffAttendance_Group_Title = "Group Name";
$i_StaffAttendance_Group_Start = "On Duty Time";
$i_StaffAttendance_Group_End = "Off Time";
$i_StaffAttendance_Group_Special = "Special Days Arrangement";
$i_StaffAttendance_Group_SpecialDayType = "Day Type";
$i_StaffAttendance_Group_Special_DutyTime = "Special Days Duty Time";
$i_StaffAttendance_Group_WorkingHrs = "Working Hours";
$i_StaffAttendance_NoNeedMark = "No need to take Attendance";
$i_StaffAttendance_NeedMark = "Need to take Attendance";
$i_StaffAttendance_Group_OnDuty = "On Duty";
$i_StaffAttendance_Group_InTime = "In-time";
$i_StaffAttendance_Group_InStatus = "In-Status";
$i_StaffAttendance_Group_InWaived = "In-Waived";
$i_StaffAttendance_Group_OutTime = "Out-time";
$i_StaffAttendance_Group_OutStatus = "Out-status";
$i_StaffAttendance_Group_OutWaived = "Out-waived";

$i_StaffAttendance_Status = "Status";
$i_StaffAttendance_Status_in = "In";
$i_StaffAttendance_Status_out = "Out";

$i_StaffAttendance_Change_Status = "Change Status";
$i_StaffAttendance_Status_Normal = "Normal";
$i_StaffAttendance_Status_Absent = "Absent";
$i_StaffAttendance_Status_Late = "Late";
$i_StaffAttendance_Status_Holiday = "On Holiday";
$i_StaffAttendance_Status_Outgoing = "Outgoing";
$i_StaffAttendance_Status_EarlyLeave = "Early Leave";

$i_StaffAttendance_Mode = "Mode";
$i_StaffAttendance_Mode_List = "List Mode";
$i_StaffAttendance_Mode_Group = "Group Mode";

$i_StaffAttendance_StaffName = "Staff Name";
$i_StaffAttendance_Field_RecordDate = "Record Date";
$i_StaffAttendance_Field_ArrivalTime = "Arrival Time";
$i_StaffAttendance_Field_DepartureTime = "Departure Time";
$i_StaffAttendance_Field_Signature = "Signature";
$i_StaffAttendance_Field_InSite = "In Site";
$i_StaffAttendance_Field_OutSite = "Out Site";
$i_StaffAttendance_Status_NoRecord = "No Record";
$i_StaffAttendance_Field_ArrivalAndDepartureTime = "Arrival And Departure Time";

$i_StaffAttendance_Sim_BackSchool = "Simulate Card Record (Back School)";
$i_StaffAttendance_Sim_LeaveSchool = "Simulate Card Record (Leave School)";

$i_StaffAttendance_Outgoing_Type = "Outgoing Type";
$i_StaffAttendance_Warning_Select_OutgoingType="Please select an Outgoing Type";


$i_StaffAttendance_Outing_Date = "Outing Date";
$i_StaffAttendance_Outing_OutTime = "Start Time";
$i_StaffAttendance_Outing_BackTime = "Back Time";
$i_StaffAttendance_Outing_FromWhere = "From Where";
$i_StaffAttendance_Outing_Location = "Location";
$i_StaffAttendance_Outing_Objective = "Objective";
$i_StaffAttendance_Outing_Remark = "Remark";
$i_StaffAttendance_Outing_ImportFormat = "
<b><u>File Description : </u></b><br>
Column 1 : $i_UserLogin<br>
Column 2 : $i_StaffAttendance_Outing_Date <br>
Column 3 : $i_StaffAttendance_Outing_OutTime<br>
Column 4 : $i_StaffAttendance_Outing_BackTime<br>
Column 5 : $i_StaffAttendance_Outing_FromWhere<br>
Column 6 : $i_StaffAttendance_Outing_Location<br>
Column 7 : $i_StaffAttendance_Outing_Objective<br>
Column 8 : $i_StaffAttendance_Sim_BackSchool<br>
Column 9 : $i_StaffAttendance_Sim_LeaveSchool<br>
";

$i_StaffAttendance_Slot_AM = "AM";
$i_StaffAttendance_Slot_PM = "PM";
$i_StaffAttendance_Slot_WD = "WD";

$i_StaffAttendance_Leave_Date = "Leave Date";
$i_StaffAttendance_Leave_Deduct_Salary = "Deduct Salary";
$i_StaffAttendnace_Leave_TimeSlot = "Time";
$i_StaffAttendance_Leave_Type = "Leave Type";
$i_StaffAttendance_Leave_Reason = "Reason";
$i_StaffAttendance_Leave_Remark = "Remarks";
$i_StaffAttendance_LeaveType_AL = "Annual Leave";
$i_StaffAttendance_LeaveType_SL = "Sick Leave";
$i_StaffAttendance_LeaveType_CL = "Compensation Leave";
$i_StaffAttendance_LeaveType_OL = "Other Leave";
$i_StaffAttendance_LeaveType_AL_Short = "AL";
$i_StaffAttendance_LeaveType_SL_Short = "SL";
$i_StaffAttendance_LeaveType_CL_Short = "CL";
$i_StaffAttendance_LeaveType_OL_Short = "OL";
$i_StaffAttendance_Leave_ImportFormat = "
<b><u>File Description : </u></b><br>
Column 1 : $i_UserLogin<br>
Column 2 : $i_StaffAttendance_Leave_Date <br>
Column 3 : $i_StaffAttendnace_Leave_TimeSlot (AM, PM or WD)<br>
Column 4 : $i_StaffAttendance_Leave_Type (
$i_StaffAttendance_LeaveType_AL_Short - $i_StaffAttendance_LeaveType_AL
,$i_StaffAttendance_LeaveType_SL_Short - $i_StaffAttendance_LeaveType_SL
,$i_StaffAttendance_LeaveType_CL_Short - $i_StaffAttendance_LeaveType_CL
,$i_StaffAttendance_LeaveType_OL_Short - $i_StaffAttendance_LeaveType_OL)
<br>
Column 5 : $i_StaffAttendance_Leave_Reason<br>
Column 6 : $i_StaffAttendance_Sim_BackSchool<br>
Column 7 : $i_StaffAttendance_Sim_LeaveSchool<br>
";

$i_StaffAttendance_msg_time1 = "Off Time should be greater than On Duty Time.";
$i_StaffAttendance_ReasonTypeName = "Reason Type Name";
$i_StaffAttendance_ReasonType = "Reason Type";
$i_StaffAttendance_ReasonText = "Reason";

$i_StaffAttendance_Result_ArchivedSummary = "Archived Summary";
$i_StaffAttendance_Result_Total = "Total";
$i_StaffAttendance_Result_Ontime = "On time";
$i_StaffAttendance_Result_Absent = "Absent";
$i_StaffAttendance_Result_Late = "Late";
$i_StaffAttendance_Result_EarlyLeave = "Early Leave";
$i_StaffAttendance_Result_Normal = "Normal";
$i_StaffAttendance_Result_Waived = "Waived";
$i_StaffAttendance_Result_NotWaived = "Not Waived";

$i_StaffAttendance_Month = "Month";
$i_StaffAttendance_StaffType ="Type";
$i_StaffAttendance_ReportTitle_Weekly = "Weekly Attendance Sheet";
$i_StaffAttendance_ReportTitle_Daily = "Daily Attendance Report";
$i_StaffAttendance_ReportTitle_StaffMonthly = "Monthly Staff Report";
$i_StaffAttendance_ReportTitle_Summary = "Summary";
$i_StaffAttendance_Report_Week = "Week View";
$i_StaffAttendance_Report_Month = "Month View";
$i_StaffAttendance_Report_Year = "Year View";
$i_StaffAttendance_Report_SelfInput = "Date Input";
$i_StaffAttendance_Report_Roster = "Roster Report";

#########################
$i_StaffAttendance_Report_Staff_Duty = "Duty";
$i_StaffAttendance_Report_Staff_Time="Time";
$i_StaffAttendance_Report_Staff_WaivedOnly="Waived Only";
$i_StaffAttendance_Report_Staff_Waived="Waived";
$i_StaffAttendance_Report_Staff_ValidOnly ="Valid Only";
$i_StaffAttendance_Report_Staff_All ="All";
$i_StaffAttendance_Report_Staff_Off = "OFF";
$i_StaffAttendance_Off_Reason_SpecialDuty="Special Duty";
$i_StaffAttendance_Off_Reason_GroupSpecialDuty="Group Speical Duty";
$i_StaffAttendance_Off_Reason_GroupDuty="Group Duty";


#########################

$i_StaffAttendance_ImportFormat_CardID = "Time, $i_SmartCard_Site, Card ID";
$i_StaffAttendance_ImportFormat_UserLogin = "Time, $i_SmartCard_Site, $i_UserLogin";
$i_StaffAttendance_ImportFormat_OfflineReader = "From Offline Reader";
$i_StaffAttendance_ImportTimeFormat = "Please use time format of YYYY-MM-DD HH:mm:ss";
$i_StaffAttendance_ImportConfirm = "If the above records are correct, please click Import to finish import.";
$i_StaffAttendance_ImportCancel = "If the above records are wrong, please click cancel to remove these records.";

$i_StaffAttendance_ImportCardFormat = "
<b><u>File Description : <br>
Column 1 : $i_UserLogin<br>
Column 2 : $i_SmartCard_CardID <br>
Column 3 : $i_StaffAttendance_Group_ID <br>
";

$i_StaffAttendance_SelectAnother = "Select Another Staff Member";

#### New version
$i_StaffAttendance_SystemSettings_OtherSettings = "Option Settings";

$i_StaffAttendance_DataManagement_DailyStatus = "Daily Status";
$i_StaffAttendance_Group_InStation = "In-Station";
$i_StaffAttendance_Group_OutStation = "Out-Station";

$i_StaffAttendance_DataManagement_LateList = "Daily Late List";
$i_StaffAttendance_DataManagement_AbsentList = "Daily Absence List";
$i_StaffAttendance_DataManagement_EarlyLeaveList = "Daily Early Leave List";
$i_StaffAttendance_DataManagement_SpecialWorkingPreset = "Special Working Preset";
$i_StaffAttendance_DataManagement_HolidayPreset = "Holiday Preset";
$i_StaffAttendance_DataManagement_OutgoingPreset = "Outgoing Preset";
$i_StaffAttendance_DataManagement_ReasonTypeSettings = "Reason Type Settings";
$i_StaffAttendance_DataManagmenet_IntermediateRecord = "Intermediate Records";
$i_StaffAttendance_DataManagmenet_OTRecord="OT Records";


$i_StaffAttendance_OutgoingType1 = "No need to check both in time and leave time";
$i_StaffAttendance_OutgoingType2 = "No need to check in time";
$i_StaffAttendance_OutgoingType3 = "No need to check leave time";

$i_StaffAttendance_OutgoingType[1] = "No need to check both in time and leave time";
$i_StaffAttendance_OutgoingType[2] = "No need to check in time";
$i_StaffAttendance_OutgoingType[3] = "No need to check leave time";

$i_StaffAttendance_WordTemplate_Reason = "Preset Reason";
$i_StaffAttendance_GroupID = "Group ID";

##########################
$i_StaffAttendance_Big5="繁";
$i_StaffAttendance_GB="簡";
$i_StaffAttendance_IntermediateRecord_SelectView = "Select View";
$i_StaffAttendance_IntermediateRecord_Date ="Date";
$i_StaffAttendance_IntermediateRecord_Staff ="Staff";
$i_StaffAttendance_IntermediateRecord_Warn_Please_Select_Staff="Please select a staff";
$i_StaffAttendance_IntermediateRecord_Warn_Please_Select_Day="Please select a Day";
$i_StaffAttendance_IntermediateRecord_StaffName="Staff Name";
$i_StaffAttendance_IntermediateRecord_RecordTime="Record Time";
$i_StaffAttendance_OTRecord_WorkingRecord="Working Record";

$i_StaffAttendance_OTRecord_ConfirmRedeem="Are you sure to deem?";
$i_StaffAttendance_OTRecord_Redeem ="Redeem";
$i_StaffAttendance_OTRecord_RedeemRecord="Redeem Record";
$i_StaffAttendance_OTRecord_Outstanding="Outstanding";
$i_StaffAttendance_OTRecord_RecordDate="Record Date";
$i_StaffAttendance_OTRecord_StartTime="Start Time";
$i_StaffAttendance_OTRecord_EndTime="End Time";
$i_StaffAttendance_OTRecord_OTmins="OT Minutes";
$i_StaffAttendance_OTRecord_RedeemDate="Redeem Date";
$i_StaffAttendance_OTRecord_MinsRedeemed="Minutes Redeemed";
$i_StaffAttendance_OTRecord_Remark="Remark";
$i_StaffAttendance_OTRecord_OutstandingOTmins="Outstanding OT Minutes";
$i_StaffAttendance_OTRecord_DatePeriod="Period";
$i_StaffAttendance_OTRecord_DateFrom="From";
$i_StaffAttendance_OTRecord_DateTo="To";
$i_StaffAttendance_OTRecord_Warn_Please_Select_Range="Please select the date period";
$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format="Invalid Date Format";
$i_StaffAttendance_OTRecord_Warn_RedeemedMins_Out_Of_Range="Redeemed Minutes out of Range";
$i_StaffAttendance_OTRecord_TotalOTmins="Total OT Minutes";
$i_StaffAttendance_OTRecord_TotalMinsRedeemed="Total Minutes Redeemed";

$i_StaffAttendance_Special_Working_ViewALL="View ALL";
$i_StaffAttendance_Special_Working_ViewByDate="View By Date";
$i_StaffAttendance_OT_Mins_Ignored ="Minutes ignored in Overtime Working for normal days";

$i_staffAttendance_Date = "Date";
$i_staffAttendance_ShowBy = "Type";
$i_staffAttendance_ExportFileType = "Report Format";
$i_staffAttendance_DetailEntryLogRecord = "Detail Entry Log Record";
$i_staffAttendance_DailyEntryLogRecord = "Daily Entry Log Record";
$i_staffAttendance_GroupSelection = "Select Group";
$i_staffAttendance_SelectAllGroup = "All Group";
$i_staffAttendance_SelectNotInGroup = "Not In Group";
$i_staffAttendance_PleaseSpecify = "Please Specify";
$i_staffAttendance_OtherReason = "Other Reason";
$i_staffAttendance_GroupSelectionWarning = "Please Select The Group(s)";
##########################

### Added On 16 Aug 2007 ###
$i_staffAttendance_SpecialWorking_ImportFormat = "$i_UserLogin, $i_StaffAttendance_Field_RecordDate, $i_StaffAttendance_Group_OnDuty, $i_StaffAttendance_Group_Start, $i_StaffAttendance_Group_End";
$i_staffAttendance_Holiday_ImportFormat = "$i_UserLogin, $i_StaffAttendance_Field_RecordDate, $i_StaffAttendance_ReasonTypeName, $i_StaffAttendance_ReasonText";
$i_staffAttendance_Outgoing_ImportFormat = "$i_UserLogin, $i_StaffAttendance_ReasonTypeName, $i_StaffAttendance_ReasonText, $i_StaffAttendance_Field_RecordDate, *$i_StaffAttendance_Outgoing_Type";
############################

### Added On 22 Aug 2007 ###
$i_staffAttendance_DateFormat_Warning = "Please Input A Correct Date";
$i_staffAttendance_SelectType_Warning = "Please Select The Type";
$i_staffAttendance_TimeFormat_Warning = "Please Input A Correct Time";
$i_staffAttendance_ImportDutyRecordDateFormat = "Please Use Record Date Format of YYYY-MM-DD";
$i_staffAttendance_ImportDutyFormat = "Please Use Duty Format of Y/N";
$i_staffAttendance_IMportDutyStartFormat = "Please Use On Duty Time Format of HH:mm:ss";
$i_staffAttendance_ImportDutyEndFormat = "Please Use Off Duty Time Format of HH:mm:ss";
$i_staffAttendance_OutgoingTypeFormat = "Outgoing Type: <br><ol><li>$i_StaffAttendance_OutgoingType1</ul><li>$i_StaffAttendance_OutgoingType2</ul><li>$i_StaffAttendance_OutgoingType3</ul></ol>";
############################

### Added On 23 Aug 2007 ###
$i_staffAttendance_InvalidImportFormatWarning = "Insert failed, please check if the information you entered is valid.";
$i_staffAttendance_Import_Row = "Row";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[1] = "User Login Failed";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[2] = $i_StaffAttendance_Group_OnDuty." Failed";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[3] = $i_general_record_date."/".$i_StaffAttendance_Group_Start."/".$i_StaffAttendance_Group_End." Failed";
$i_staffAttendance_Holiday_Import_ErrorMsg[1] = "User Login Failed";
$i_staffAttendance_Holiday_Import_ErrorMsg[2] = $i_general_record_date." Failed";
$i_staffAttendance_Holiday_Import_ErrorMsg[3] = $i_StaffAttendance_ReasonTypeName." Failed";
$i_staffAttendance_Holiday_Import_ErrorMsg[4] = $i_StaffAttendance_Holiday_warning;
$i_staffAttendance_Outgoing_Import_ErrorMsg[1] = "User Login Failed";
$i_staffAttendance_Outgoing_Import_ErrorMsg[2] = $i_StaffAttendance_ReasonTypeName." Failed";
$i_staffAttendance_Outgoing_Import_ErrorMsg[3] = $i_general_record_date." Failed";
$i_staffAttendance_Outgoing_Import_ErrorMsg[4] = $i_StaffAttendance_Outgoing_Type." Failed";
$i_staffAttendance_Outgoing_Import_ErrorMsg[5] = $i_StaffAttendance_Outing_warning;
############################

/*----- Added On 18 DEC 2007 */
$i_StaffAttendance_Sort = "Sort By";
$i_StaffAttendance_Sort_Type = "Sort Order";
$i_StaffAttendance_Sort_Name = "Staff Name";
$i_StaffAttendance_Sort_Status = "Status";
$i_StaffAttendance_Sort_Time = "Time";
$i_StaffAttendance_Sort_Type_Asc = "Ascending";
$i_StaffAttendance_Sort_Type_Desc = "Descending";
/*---------------------------*/

/*----- Added On 21 DEC 2007 */
$i_StaffAttendance_Field_Date_From = "From Date";
$i_StaffAttendance_Field_Date_To = "To Date";
/*---------------------------*/

/*----- Added On 24 DEC 2007 */
$i_StaffAttendance_Total_Day_Of_Abs = "Total Days";
/*---------------------------*/

/*----- Added On 27 DEC 2007 */
$i_StaffAttendance_Report_Staff_Waived_Flag = "Include Waived Records";
/*---------------------------*/

/*----- Added On 29 Feb 2008 */
$i_StaffAttendance_Report_Staff_Outgoing_Monthy = "Monthly Outgoing Report";
/*---------------------------*/

/*----- Added On 03 Mar 2008 */
$i_StaffAttendance_Report_Date_Range = "Date Range";
$i_StaffAttendance_Report_Days_Abs = "Days Absent";
/*---------------------------*/

/*----- Added On 14 Mar 2008 */
$i_StaffAttendance_ReasonType_Message = "The reason types below will be used as column names for Staff<br>
 																					Leave/Absent Report. These reason types will not be shown when<br><br>

																				 1. the reason types concerned have been deactivated, and<br>
																				 2. the reason types concerned have not been used in the selected reporting period of the Staff Leave/ Absent Report.
																				 ";
/*---------------------------*/

/*----- Added On 27 Mar 2008 */
$i_StaffAttendance_SelfAttendance = "Personal Attendance";
$i_StaffAttendance_Month_Selector_SelectMonth = "Select Month";
/*---------------------------*/

### added on 17 Jun 2008 ###
$i_StaffAttendnace_ShowReportMethod = "View By";
$i_StaffAttendnace_ShowReportByHoldPage = "Full Page";
$i_StaffAttendnace_ShowReportByseparatePage = "Separate Page";
###

### added on 29 Dec 2008 ###
$i_staffAttendance_all_group = "All groups";
$i_staffAttendance_no_group = "No groups";
###
$i_Circular_System = "e-Circular System";
$i_Circular_Circular = "e-Circular";
$i_Circular_Settings = "e-Circular Settings";
$i_Circular_Admin = "Circular Administrators Mgmt";
$i_Circular_Settings_Disable = "Disable $i_Circular_System.";
$i_Circular_Settings_AllowHelpSigning = "Allow Circular admin sign others' reply.";
$i_Circular_Settings_AllowLateSign = "Allow Late Signing.";
$i_Circular_Settings_AllowResign = "Allow recipients sign again and modify the reply.";
$i_Circular_Settings_DefaultDays = "Default no. of days for returning circular.";
$i_Circular_Setting_TeacherCanViewAll = "Allow all teachers to view all circulars.";
$i_Circular_AdminLevel = "Admin Level";
$i_Circular_AdminLevel_Normal = "Normal Level";
$i_Circular_AdminLevel_Full = "Full Control";
$i_Circular_AdminLevel_Detail_Normal = "$i_Circular_AdminLevel_Normal (Allowed to issue circulars and view the results of circulars issued by him/her)";
$i_Circular_AdminLevel_Detail_Full = "$i_Circular_AdminLevel_Full (Allowed to issue circulars and view the results of all circulars)";
$i_Circular_Description_SetAdmin = "Please Select which user to be new admin and choose the admin level.";
$i_Circular_Management = "e-Circular Management";
$i_Circular_MyIssueCircular = "Circular Issued By Me";
$i_Circular_AllCircular = "All Circulars";
$i_Circular_MyCircular = "My Circulars";
$i_Circular_DateStart = "Start Date";
$i_Circular_DateEnd = "End Date";
$i_Circular_CircularNumber = "Circular Number";
$i_Circular_Title = "Title";
$i_Circular_Description = "Content";
$i_Circular_Issuer = "Issuer";
$i_Circular_RecipientType = "Recipient Type";
$i_Circular_RecipientTypeAllStaff = "All Staff";
$i_Circular_RecipientTypeAllTeaching = "All Teaching Staff";
$i_Circular_RecipientTypeAllNonTeaching = "All Non-teaching Staff";
$i_Circular_RecipientTypeIndividual = "Individual Recipients / Groups";
$i_Circular_Signed = "Signed";
$i_Circular_Total = "Total";
$i_Circular_ViewResult = "View Result";
$i_Circular_NoRecord = "This is no circular at the moment.";
$i_Circular_FromTemplate = "Load from Template";
$i_Circular_NotUseTemplate = "Not Use Template";
$i_Circular_RecipientIndividual = "Individual Staff";
$i_Circular_CurrentList = "Current Circulars List";
$i_Circular_Attachment = "Attachment(s)";
$i_Circular_SetReplySlipContent = "Edit Reply Slip Content";
$i_Circular_ReplySlip = "Reply Slip";
$i_Circular_Type = "Type";
$i_Circular_TemplateNotes = "(Only <font color=green>$i_Circular_Title</font>, <font color=green>$i_Circular_Description</font>, and <font color=green>$i_Circular_ReplySlip</font> will be used in template.)";
$i_Circular_ListIncludingSuspend = "Including Distributed and Suspended";
$i_Circular_CurrentCircular = "Current Circulars";
$i_Circular_PastCircular = "Past Circulars";
$i_Circular_Alert_Sign = "Your reply slip will be submitted. Are you sure to submit the reply slip?";
# 20090326 yatwoon eService>eCircular
$i_Circular_Alert_Sign_AddStar = "Your reply slip will be submitted and this circular will be starred. Are you sure to submit the reply slip?";
$i_Circular_Alert_UnStar = "This circular will be un-starred.  Are you sure to continue?";
$i_Circular_Alert_AddStar = "This circular will be starred.  Are you sure to continue?";

$i_Circular_Sign = "Sign";
$i_Circular_SignStatus = "Status";
$i_Circular_Unsigned = "Unsigned";
$i_Circular_Signer = "Signed by";
$i_Circular_Editor = "Edited by";
$i_Circular_Signer = "Signed By";
$i_Circular_SignedAt = "Signed At";
$i_Circular_Recipient = "Recipient";
$i_Circular_SignInstruction = "Please fill in the above reply slip and click Sign button to sign this notice.";
$i_Circular_SignInstruction2 = "You can change your reply by clicking Sign button again.";
$i_Circular_Sign = "Sign";
$i_Circular_Option = "Option";
$i_Circular_NoTarget = "This circular has not been issued to any staff.";
$i_Circular_ViewResult = "View Result";
$i_Circular_TableViewReply = "View Replies";
$i_Circular_NoReplyAtThisMoment = "There is no signed reply at this moment.";
$i_Circular_ViewStat = "View Statistics";
$i_Circular_NumberOfSigned = "No. of signed Replies";
$i_Circular_RemovalWarning = "Warning: Removing this circular will remove the replies of staff together. Do you want to proceed?";
$i_Circular_Alert_DateInvalid = "Do not set the Deadline before the Issue Date";
$i_Circular_NotExpired = "Not Expired";
$i_Circular_Expired = "Expired";
$i_Circular_AllSigned = "All Signed";
$i_Circular_NotAllSigned = "Not All Signed";
############ New wordings for eClass IP ###############
$i_Circular_Select_Template = "Select a template to load";
$i_Circular_Select_Recipient_Type = "Select a recipient type";
$i_Circular_Add_Circular = "Add the new circular";
$i_Circular_New = "New Circular";
$i_Circular_Edit = "Edit Circular";

# 20090326 yatwoon eCircular Stared function
$eCircular["StarredCirculars"] = "Starred Circulars";
$eCircular["SignClose"] = "Sign & Close";
$eCircular["SignAddStar"] = "Sign & Add Star";
$eCircular["RemoveStar"] = "Remove Star";
$eCircular["AddStar"] = "Add Star";

#######################################################

$i_CycleNew_Menu_DefinePeriods = "Define Periods";
$i_CycleNew_Menu_GeneratePreview = "Generate Preview";
$i_CycleNew_Menu_CheckPreview = "Check Preview";
$i_CycleNew_Menu_MakePreviewToProduction = "Make Current Preview to Production";
$i_CycleNew_Alert_GeneratePreview = "Are you sure to clear current and generate the preview?";
$i_CycleNew_Alert_RemoveConfirm = "Are you sure to remove this period?";
$i_CycleNew_Alert_RemoveAllConfirm = "Are you sure to remove ALL periods?";
$i_CycleNew_Alert_MakeToProduction = "Are you sure to make current preview to production?";
$i_CycleNew_Prompt_DateRangeOverlapped = "The period cannot be overlapped.";
$i_CycleNew_Prompt_PreviewGenerated = "Preview Generated. Please check the preview is correct before make to production.";
$i_CycleNew_Prompt_ProductionMade = "Production updated.";
$i_CycleNew_Field_PeriodStart = "Period Start";
$i_CycleNew_Field_PeriodEnd = "Period End";
$i_CycleNew_Field_PeriodType = "Period Type";
$i_CycleNew_Field_CycleType = "Cycle Type";
$i_CycleNew_Field_PeriodDays = "Days of a cycle";
$i_CycleNew_Field_SaturdayCounted = "Saturday Counted";
$i_CycleNew_Field_FirstDay = "First Day";
$i_CycleNew_Field_Date = "Date";
$i_CycleNew_Field_TxtChi = "Chinese Display";
$i_CycleNew_Field_TxtEng = "English Display";
$i_CycleNew_field_TxtShort = "Short Display";
$i_CycleNew_PeriodType_NoCycle = "No Cycle";
$i_CycleNew_PeriodType_Generation = "Normal Calculation";
$i_CycleNew_PeriodType_FileImport = "File Import";
$i_CycleNew_CycleType_Numeric = "Numeric (1, 2, 3 ...)";
$i_CycleNew_CycleType_Alphabetic = "Alphabets (A, B, C ...)";
$i_CycleNew_CycleType_Roman = "Roman (I, II, III ...)";
$i_CycleNew_Description_FileImport = "<u><b>File Description:</b></u><br>
Column 1: Date of the day (YYYY-MM-DD)<br>
Column 2: Cycle display in Chinese interface (e.g. 循環日 A)<br>
Column 3: Cycle display in English interface (e.g. Day A)<br>
Column 4: Cycle display in short area and for Periodic Resource Booking etc. use(e.g. A)
";
$i_CycleNew_Warning_FileImport = "Previous records import for this date range will be removed.";
$i_CycleNew_View_ImportRecords = "View Current Records";
$i_CycleNew_Export_FileImport = "Download as import format";
$i_CycleNew_Prefix_Chi = "循環日 ";
$i_CycleNew_Prefix_Eng = "Day ";
$i_CycleNew_NumOfMonth = "# of months";

# Discipline System
$i_Discipline_System = "Discipline System";
$i_Discipline_System_Approval = "Approval";
$i_Discipline_System_PunishCounselling = "Detention & Conduct Score Records";
$i_Discipline_System_Report = "Report";
$i_Discipline_System_Stat = "Stat";
$i_Discipline_System_SchoolRules = "School Rules";
$i_Discipline_System_Settings = "Basic Settings";
$i_Discipline_System_Accumulative = "Accumulative Misconduct";
$i_Discipline_System_Settings_SchoolRule = "School Rules Settings";
$i_Discipline_System_Settings_Merit = "Reward Definition";
$i_Discipline_System_Settings_Punishment = "Misconduct Definition";
$i_Discipline_System_Settings_MildPunishment = "Minor Punishment Definition";
$i_Discipline_System_Settings_MeritType = "Merit Type and Approval Settings";
$i_Discipline_System_Settings_DemeritType = "Demerit Type and Approval Settings";
$i_Discipline_System_Settings_UpgradeRule = "Accumulated Upgrade Rule";
$i_Discipline_System_Settings_SpecialPunishment = "Special Punishment Definition";
$i_Discipline_System_Settings_Conduct = "Conduct Score Usage Settings";
$i_Discipline_System_Settings_Subscore1 =  "Study Score Usage Settings";
$i_Discipline_System_Settings_Calculation = "Conduct Score Calculation";
$i_Discipline_System_Settings_Calculation_Conduct_Semester = "Semester Conduct Grade";
$i_Discipline_System_Settings_Calculation_Conduct_Annual = "Annual Conduct Score & Grade";
$i_Discipline_System_Approval = "eDiscipline Approval";


$i_Discipline_System_RuleCode = "Rule Code";
$i_Discipline_System_RuleName = "Rule Name";
$i_Discipline_System_UpgradeRule = "Upgrade Rule";
$i_Discipline_System_NeedApproval = "Needs Approval";
$i_Discipline_System_ApprovalLevel = "Approval By";
$i_Discipline_System_WaivedBy_Prefix = "Waived By ";
$i_Discipline_System_WaivedBy_Subfix = " ";
$i_Discipline_System_UnwaivedBy_Prefix = "Unwaived By ";
$i_Discipline_System_UnwaivedBy_Subfix = " ";
$i_Discipline_System_Admin_Level1 = "Discipline Teacher";
$i_Discipline_System_Admin_Level2 = "System Administrator";
$i_Discipline_System_Category_Merit = "Merit Category";
$i_Discipline_System_Category_Punish = "Punishment Category";
$i_Discipline_System_CategoryName = "Category Name";
$i_Discipline_System_Item_Merit = "Merit Item";
$i_Discipline_System_Item_Punish = "Punishment Item";
$i_Discipline_System_ItemName = "Item Name";
$i_Discipline_System_ItemCode = "Item Code";
$i_Discipline_System_Item_Code_Name = "Item Code/Name";
$i_Discipline_System_Item_Code_Item_Name = "Item Code/Item Name";
$i_Discipline_System_ConductScore = "Conduct Score";
$i_Discipline_System_NumOfMerit = "Merit Num/Type";
$i_Discipline_System_NumOfPunish = "Demerit Num/Type";
$i_Discipline_System_NotAllowElimination = "Elimination Not Allowed";
$i_Discipline_System_SpecialPunishment = "Special Punishment";
$i_Discipline_System_DetentionMinutes = "Detention Period (Minutes)";
$i_Discipline_System_InputAward = "Input Merit Record";
$i_Discipline_System_InputPunishment = "Input Demerit Record";
$i_Discipline_System_Add_Merit = "Add Merit Record";
$i_Discipline_System_No_Of_Award_Punishment = "No. of Award/Punishment";
$i_Discipline_System_Quantity = "Quantity";
$i_Discipline_System_Add_Demerit = "Add Demerit Record";
$i_Discipline_System_Add_Merit_Demerit = "Award/Punishment";
$i_Discipline_System_Event_Merit = "Merit Record(S)";
$i_Discipline_System_Event_Demerit = "Demerit Record(s)";
$i_Discipline_System_ContactReq_Parent = "Parent Meeting Required";
$i_Discipline_System_ContactReq_Social = "Social Worker Meeting Required";
$i_Discipline_System_Contact_Date = "Meeting Date";
$i_Discipline_System_Contact_Time = "Meeting Time";
$i_Discipline_System_Contact_Venue = "Venue";
$i_Discipline_System_Contact_PIC = "Person-in-charge";
$i_Discipline_System_Illegal_Character = " contains illegal character: backslashes";
#### modified by henry on 25 Nov 2008
$i_Discipline_System_CategoryList = "Category List";
$i_Discipline_System_NewCategory = "New Category";
$i_Discipline_System_Not_Yet_Assigned = "Not yet assigned";


####


$i_Discipline_System_Description_Approval = "Number of Merit/Demerit larger than this requires approval";
$i_Discipline_System_Message_SaveRequired = "As other settings have been modified, please save the settings once.";
$i_Discipline_System_general_increment = "Increment";
$i_Discipline_System_general_decrement = "Decrement";
$i_Discipline_System_general_date_merit = "Event Date"; #"Merit Date";
$i_Discipline_System_general_date_demerit = "Event Date"; #"Demerit Date";
$i_Discipline_System_general_record = "Record Item";
$i_Discipline_System_general_remark = "Remark";
$i_Discipline_System_default = "Default Set";
$i_Discipline_System_customize = "Category-Specific Set";
$i_Discipline_System_period_setting = "Use Period Set";
$i_Discipline_System_new_period_setting = "New Period";
$i_Discipline_System_Field_Times = "Time(s) Over";
$i_Discipline_Ssytem_Category_Item_Not_Selected = "Record category or item not selected.";


$i_Discipline_System_Control_LevelSetting = "Level Name Settings";
$i_Discipline_System_Control_UserACL = "Teacher/Staff Level Settings";
$i_Discipline_System_Control_ItemLevel = "Access Control List Settings";
$i_Discipline_System_Control_AttendanceSettings = "Integration with Smart Card Student Attendance";
$i_Discipline_System_Control_Attend_YearSemester = "Set Accumulated Year/Semester";
$i_Discipline_System_Control_Attend_CalculationRule = "Counting Rule";

$i_Discipline_System_Control_Warning_LevelSetting = "Step 1 : Please provide Level Name settings";
$i_Discipline_System_Control_Warning_ItemLevel = "Step 2 : Please provide Access Control List settings";
$i_Discipline_System_Control_Warning_UserACL = "Step 3 : Please provide Teacher/Staff Level settings";

$i_Discipline_System_Field_Type = "Type";
$i_Discipline_System_Field_AdminLevel = "Admin Level";
$i_Discipline_System_Field_Personal = "Personal";

$i_Discipline_System_NumOfLevel = "Number of Levels";
$i_Discipline_System_alert_NumLevelLess = "The number selected is less than the existing number of levels. Higher levels will be removed. Are you sure?";
$i_Discipline_System_alert_NeedToSave = "Caution: Settings have been changed. Click 'Save' to save the settings.";
$i_Discipline_System_alert_ChangeAdminLevel = "Are you sure to change the admin level of the selected user(s)?";
$i_Discipline_System_alert_RecordAddSuccessful = "Record added successfully";
$i_Discipline_System_alert_RecordAddFail = "Failed to add record";
$i_Discipline_System_alert_RecordModifySuccessful = "Record modified successfully";
$i_Discipline_System_alert_RecordModifyFail = "Failed to modify record";
$i_Discipline_System_alert_RecordWaitForApproval = "Record added and is now waiting for approval";
$i_Discipline_System_alert_PleaseSelectStudent = "Please select student(s)";
$i_Discipline_System_alert_PleaseSelectMember = "Please select member(s)";
$i_Discipline_System_alert_PleaseSelectClass = "Please select class";
$i_Discipline_System_alert_PleaseSelectClassLevel = "Please select class level";
$i_Discipline_System_alert_PleaseSelectDemeritRecord = "Please select demerit record(s)";
$i_Discipline_System_alert_PleaseSelectDetentionRecord = "Please select detention record(s)";
$i_Discipline_System_alert_PleaseSelectEvent = "Please select event(s)";
$i_Discipline_System_alert_PleaseSelectMeritRecord = "Please select merit record(s)";
$i_Discipline_System_alert_PleaseInsertNewConductScore = "Please fill in the new conduct score";
$i_Discipline_System_alert_PleaseInsertNewSubscore1 = "Please fill in the new study score";
$i_Discipline_System_alert_remove_warning_level = "Are you sure to remove this record?";
$i_Discipline_System_alert_warning_level_already_exists = "<font color=red>Warning Level already exists</font>";
$i_Discipline_System_alert_grading_scheme_already_exists = "<font color=red>Grading Scheme already exists</font>";
$i_Discipline_System_alert_warning_level_not_exists = "<font color=red>Warning Level does not exist</font>";
$i_Discipline_System_alert_reset_conduct = "Are you sure to reset all students' records?";
$i_Discipline_System_alert_Approved = "<font color=green>Record(s) Approved</font>";
$i_Discipline_System_alert_ApprovedSome = "<font color=red>Not all records are approved. You do not have enough access right to approve some records.</font>";
$i_Discipline_System_alert_calculate = "Are you sure to proceed calculation?";
$i_Discipline_System_alert_SchoolYear = "Please fill in a school year";
$i_Discipline_System_alert_remove_category = "Related items will also be deleted. Are you sure?";
$i_Discipline_System_alert_remove_record = "All related records will be deleted as well. Are you sure you want to delete?";
$i_Discipline_System_alert_require_group_title = "Please fill in Group Title";
$i_Discipline_System_alert_exist_group_title = "Group Title already existed";
$i_Discipline_System_alert_Award_Punishment_Missing = "You have selected not to reward/punish the student(s). Are you sure you want to proceed?";

# added by Henry on 5 Nov 2008
$i_Discipline_System_Discipline_Group_Access_Rights = "Group Access Rights";
$i_Discipline_System_Discipline_Members_Rights = "Discipline Members";
$i_Discipline_System_Teacher_Rights = "Teacher Rights";
$i_Discipline_System_Student_Rights = "Parent / Student Rights";

# added by Henry on 6 Nov 2008
//$i_Discipline_System_Student_Right_Navigation_Student_Right = "Student Rights";
$i_Discipline_System_Student_Right_Navigation_Edit_Right = "Edit";
$i_Discipline_System_Student_Right_Navigation2 = "Student Access Right";
$i_Discipline_System_Student_Right_Self_Records = "- Self Records -";
$i_Discipline_System_Access_Right_Award_Punish = "Award <br>& Punishment";
$i_Discipline_System_Access_Right_Case_Record = "Case Record";
$i_Discipline_System_Access_Right_Case = "Case";
$i_Discipline_System_Access_Right_Good_Conduct_Misconduct = "Good Conduct <br>& Misconduct";
$i_Discipline_System_Access_Right_Conduct_Mark = "Conduct Mark";
$i_Discipline_System_Access_Right_Detention = "Detention";
$i_Discipline_System_Access_Right_Detention_Management = "Detention -<br>Management";
$i_Discipline_System_Access_Right_Detention_Session_Arrangement = "Detention -<br>Session Arrangement";
$i_Discipline_System_Access_Right_Top10 = "Ranking Report";
$i_Discipline_System_Access_Right_Email = "Email";
$i_Discipline_System_Access_Right_Letter = "Letter";
$i_Discipline_System_Access_Right_eNotice_Template = "eNotice Template";
$i_Discipline_System_Access_Right_View = "View";
$i_Discipline_System_Access_Right_New = "Create";
$i_Discipline_System_Access_Right_Edit = "Edit";
$i_Discipline_System_Access_Right_Own = "Own";
$i_Discipline_System_Access_Right_All = "All";
$i_Discipline_System_Access_Right_Delete = "Delete";
$i_Discipline_System_Access_Right_Waive = "Waive";
$i_Discipline_System_Access_Right_NewLeaf = "New Leaf";
$i_Discipline_System_Access_Right_Release = "Release";
$i_Discipline_System_Access_Right_Approval = "Approve";
$i_Discipline_System_Access_Right_Finish = "Finish";
$i_Discipline_System_Access_Right_Lock = "Lock";
$i_Discipline_System_Access_Right_Adjust = "Adjust";
$i_Discipline_System_Access_Right_Take_Attendance = "Take Attendance";
$i_Discipline_System_Access_Right_Rearrange_Student = "Rearrange Sessions";
$i_Discipline_System_Access_Right_New_Notes = "New Notes";
$i_Discipline_System_Access_Right_Edit_Notes = "Edit Notes";
$i_Discipline_System_Access_Right_Delete_Notes = "Delete Notes";
$i_Discipline_System_Access_Right_Access = "Access";
$i_Discipline_System_Access_Right_Member_Name = "Member Name";
$i_Discipline_System_Access_Right_Added_Date = "Date Added";

//$i_Discipline_System_Teacher_Right_Navigation_Teacher_Right = "Teacher Right";
$i_Discipline_System_Teacher_Right_Navigation_Edit_Right = "Edit";
$i_Discipline_System_Teacher_Right_Navigation2 = "Teacher Access Right";
$i_Discipline_System_Teacher_Right_Management = "- Management -";
$i_Discipline_System_Teacher_Right_Statistics = "- Statistics -";
$i_Discipline_System_Teacher_Right_Reports = "- Reports -";
$i_Discipline_System_Teacher_Right_Settings = "- Settings -";

$i_Discipline_System_Group_Right_Navigation_Group_List = "Group List";
$i_Discipline_System_Group_Right_Navigation_New_Group = "New Group";
$i_Discipline_System_Group_Right_Navigation_Edit_Group = "Edit";
$i_Discipline_System_Group_Right_Navigation_New_Member = "New Member";
$i_Discipline_System_Group_Right_Navigation_Choose_Member = "Choose Member";
$i_Discipline_System_Group_Right_Navigation_Selected_Member = "Selected Member(s)";
$i_Discipline_System_Group_Right_Navigation_Selected_Member2 = "Selected Member(s)";
$i_Discipline_System_Group_Right_Navigation_Group_Info = "Group Info";
$i_Discipline_System_Group_Right_Navigation_Group_Title = "Group Name";
$i_Discipline_System_Group_Right_Navigation_Group_Description = "Description";
$i_Discipline_System_Group_Right_Navigation_Group_Access_Right = "Group Access Rights";
$i_Discipline_System_Group_Right_Record_Added = "<span class=\"tabletextrequire\">(Record Added)</span>";
$i_Discipline_System_Group_Right_Group_Name = "Group Name";
$i_Discipline_System_Group_Right_Description = "Description";
$i_Discipline_System_Group_Right_No_Of_Members = "No. of Members";

$i_Discipline_System_Conduct_Mark_Student_List = "Student List";
$i_Discipline_System_Conduct_Mark_Adjust_Conduct_Mark = "Adjust Conduct Mark";
$i_Discipline_System_Conduct_Mark_Current_Conduct_Mark = "Current Conduct Mark";
$i_Discipline_System_Conduct_Mark_Pre_Adjustment = "Pre-adjustment";
$i_Discipline_System_Conduct_Mark_Acc_Adjustment = "Accumulated Adjustment";
$i_Discipline_System_Conduct_Mark_Current = "Current";
$i_Discipline_System_Conduct_Mark_Previous_Adjustment = "Previous Adjustment";
$i_Discipline_System_Conduct_Mark_Manually_Adjustment = "Manually Adjustment";
$i_Discipline_System_Conduct_Mark_Reversed_Conduct_Mark = "Reversed Conduct Mark";
$i_Discipline_System_Conduct_Mark_Reversed_Conduct_Grade = "Reversed Conduct Grade";
$i_Discipline_System_Conduct_Mark_Reason = "Reason";
$i_Discipline_System_Conduct_Instruction_Msg_Content = "The update of Base Mark will not change the existing records.";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction1 = "Instruction";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction2 = "Please select the School Year and Semester that you want to base the calculation on.";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction3 = "Once calculated, all previous calculated grades for the period chosen will be overidden.";
$i_Discipline_System_Discipline_Conduct_Mark_Weighting = "Conduct Mark Weighting";
$i_Discipline_System_Discipline_Conduct_Mark_Weight = "Weight";
$i_Discipline_System_Discipline_Conduct_Mark_Weight_Total = "Total";
$i_Discipline_System_Discipline_Conduct_Mark_Weight_Semester = "Semester";

$i_Discipline_System_Award_Punishment_Edit = "Edit";
$i_Discipline_System_Award_Punishment_Merit_List = "Merit List";
$i_Discipline_System_Award_Punishment_Submenu_Category_Item = "Category & Items";
$i_Discipline_System_Award_Punishment_Submenu_Approval = "Approval";
$i_Discipline_System_Award_Punishment_Submenu_Promotion = "Conversion";
$i_Discipline_System_Award_Punishment_Move_Up = "Move Up";
$i_Discipline_System_Award_Punishment_Move_Down = "Move Down";
$i_Discipline_System_Award_Punishment_Move_Top = "Move Top";
$i_Discipline_System_Award_Punishment_Move_Bottom = "Move Bottom";
$i_Discipline_System_Award_Punishment_No_of_Item = "No. of Items";
$i_Discipline_System_Award_Punishment_Award_Category = "Award Category";
$i_Discipline_System_Award_Punishment_Punish_Category = "Punishment Category";
$i_Discipline_System_Award_Punishment_Setting_Category = "Independent settings for each category";
$i_Discipline_System_Award_Punishment_Setting_Global = "Common settings for all categories";
$i_Discipline_System_Award_Punishment_Common_Setting = "Common Setting";
$i_Discipline_System_Award_Punishment_Method = "Method";
$i_Discipline_System_Award_Punishment_Conversion_Method_List = "Conversion Method List";
$i_Discipline_System_Award_Punishment_Conversion_Method = "Conversion Method";
$i_Discipline_System_Award_Punishment_Equivalent_Point_Msg = "*Equivalent point(s) state the no. of good point required for attaining the award";
$i_Discipline_System_Award_Punishment_Equivalent_Point = "Equivalent Point(s) *";
$i_Discipline_System_Award_Punishment_Search_Alert = "Please fill in the content";
$i_Discipline_System_Award_Punishment_All_School_Year = "All School Years";
$i_Discipline_System_Award_Punishment_Whole_Year = "Whole Year ";
$i_Discipline_System_Award_Punishment_All_Classes = "All Classes";
$i_Discipline_System_Award_Punishment_Warning = "You can use the \"Select Status\" drop-down menu below to select to view records of particular status(es).";
$i_Discipline_System_Award_Punishment_All_Records = "All Records";
$i_Discipline_System_Award_Punishment_Awards = "Award";
$i_Discipline_System_Award_Punishment_Punishments = "Punishment";
$i_Discipline_System_Award_Punishment_Change_Status = "Change Status";
$i_Discipline_System_Award_Punishment_Select_Status = "Select Status";
$i_Discipline_System_Award_Punishment_Reference = "Reference";
$i_Discipline_System_Award_Punishment_Pending = "Waiting for Approval";
$i_Discipline_System_Award_Punishment_Wait_For_Waive = "Waiting for Waive";
$i_Discipline_System_Award_Punishment_Wait_For_Release = "Waiting to Release";
$i_Discipline_System_Award_Punishment_Approved = "Approved";
$i_Discipline_System_Award_Punishment_Rejected = "Rejected";
$i_Discipline_System_Award_Punishment_Released = "Released";
$i_Discipline_System_Award_Punishment_UnReleased = "UnReleased";
$i_Discipline_System_Award_Punishment_ProcessedBy = "Processed By";
$i_Discipline_System_Award_Punishment_Processed = "";
$i_Discipline_System_Award_Punishment_Change_To_Processed = "Change Status to 'Processed'";
$i_Discipline_System_Award_Punishment_Change_To_Processing = "Change Status to 'Processing'";
$i_Discipline_System_Award_Punishment_Change_To_Release = "Change Status to 'Release'";
$i_Discipline_System_Award_Punishment_Change_To_Unrelease = "Change Status to 'UnRelease'";
$i_Discipline_System_Award_Punishment_Waived = "Waived";
$i_Discipline_System_Award_Punishment_Locked = "Locked";
$i_Discipline_System_Award_Punishment_Redeemed = "Redeemed";
$i_Discipline_System_Award_Punishment_Approve = "Approve";
$i_Discipline_System_Award_Punishment_Reject = "Reject";
$i_Discipline_System_Award_Punishment_Release = "Release";
$i_Discipline_System_Award_Punishment_UnRelease = "UnRelease";
$i_Discipline_System_Award_Punishment_Waive = "Waive";
$i_Discipline_System_Award_Punishment_Redeem = "Redeem";
$i_Discipline_System_Award_Punishment_Detention = "Detention";
$i_Discipline_System_Award_Punishment_Send_Notice = "Send Notice";
$i_Discipline_System_Award_Punishment_Lower_Level = "Lower level Record";
$i_Discipline_System_Award_Punishment_Higher_Level = "Higher level Record";
$i_Discipline_System_Award_Punishment_Convert_To = "Convert to";
$i_Discipline_System_Award_Punishment_Event_Date = "Event Date";

$i_Discipline_System_GoodConduct_Misconduct_Waive_Date_Count = "Waive Period Passed (Day(s))";
$i_Discipline_System_GoodConduct_Misconduct_Every = "Every ";
$i_Discipline_System_GoodConduct_Misconduct_Instance_Of = " instance of ";
$i_Discipline_System_GoodConduct_Misconduct_Will_Be_Count_As = " will be count as ";
$i_Discipline_System_GoodConduct_Misconduct_Category_To_Subcategory = " - ";
$i_Discipline_System_GoodConduct_Misconduct_Wait_For_NewLeaf = "Waiting to waive via New Leaf Scheme";

$i_Discipline_System_Insert_From_PPC = "Inserted From PPC";

$i_Discipline_System_Case_Record_Case_Info = "Case Info";
$i_Discipline_System_Case_Record_Notes = "Notes";
$i_Discipline_System_Case_Record_Student_Involved = "Student(s) Involved";
$i_Discipline_System_Case_Record_New_Student = "New Student(s)";

$i_Discipline_System_Ranking_Report_Msg = "Ranking Report shows the most awarded or punished classes or forms. You can set report period and ranking method below.";
$i_Discipline_System_Ranking_Goodconduct_Misconduct_Report_Msg = "Ranking Report shows the most good conduct or misconduct classes, forms or students. You can set report period and ranking method below.";
$i_Discipline_System_Case_Record_Delete_Msg = "Deleting case records will cause the award/punishment records of all student(s) involved to be removed as well. Are you sure you want to delete the selected case(s)?";
##############################

$i_Discipline_System_msg_GoBackPersonalRecord = "Back to Personal Record";
$i_Discipline_System_normal_teacher = "Ordinary Teacher/Staff";
$i_Discipline_System_msg_AddAnotherRecord = "Add new record(s)";
$i_Discipline_System_msg_ModifyAnotherRecord = "Modify more records";
$i_Discipline_System_msg_ModifyOtherRecord = "Modify other record";
$i_Discipline_System_msg_GoBackApproval = "Back to Approval Page";
$i_Discipline_System_field_discipline_record = "Discipline Record";
$i_Discipline_System_Report_Type_Personal = "Personal Report";
$i_Discipline_System_Report_Type_AwardPunish = "Reward/Misconduct Records";
$i_Discipline_System_Report_Type_Class = "Class Summary";
$i_Discipline_System_Report_Type_PunishCounselling = "Punishment and Counselling Report";
$i_Discipline_System_Report_Type_Assessment = "Assessment Items";
$i_Discipline_System_Report_Type_Detail = "Detailed Report";
$i_Discipline_System_Report_Personal_PersonalRecord = "Personal Record";
$i_Discipline_System_Report_Personal_ConductAdjustment = "Discipline Records Adjustment";
$i_Discipline_System_Report_AwardPunish_ClassLevel = "Class Level Record";
$i_Discipline_System_Report_AwardPunish_ClassRecord = "Class Record";
$i_Discipline_System_Report_AwardPunish_TodayRecord = "Daily Reward/Misconduct Records";
$i_Discipline_System_Report_PunishCounselling_ParentMeeting = "Parent Meeting(s)";
$i_Discipline_System_Report_PunishCounselling_SocialMeeting = "Counselling Appointment(s)";
$i_Discipline_System_Report_PunishCounselling_PunishReport = "Punishment Report";
$i_Discipline_System_Report_PunishCounselling_DetentionReport = "Detention Report";
$i_Discipline_System_Report_Assessment_Assessment = "Assessment";
$i_Discipline_System_Report_Detail_Student = "Detailed Student Report";
$i_Discipline_System_Report_Detail_CountReport = "Violation Report";
$i_Discipline_System_Report_Class_Total = "Class Total";
$i_Discipline_System_Field_Total = "Total";
$i_Discipline_System_Stat_Type_General = "Class Disciplinary Statistics";
$i_Discipline_System_Stat_Type_Award = "Reward Statistics";
$i_Discipline_System_Stat_Type_Detention = "Detention Statistics";
$i_Discipline_System_Stat_Type_Discipline = "Misconduct Statistics";
$i_Discipline_System_Stat_Type_Punishment = "Punishment Stats";
$i_Discipline_System_Stat_Type_Others = "Other Statistics";
$i_Discipline_System_Stat_General_ClassLevel = "Class Level Stats";
$i_Discipline_System_Stat_General_Class = "Class Stats";
$i_Discipline_System_Stat_General_Detail = "Individual Discipline Stats";
$i_Discipline_System_Stat_Award_ClassLevel = "Class Level Stats";
$i_Discipline_System_Stat_Award_Class = "Class Stats";
$i_Discipline_System_Stat_Detention_Reason_ClassLevel = "Class Level-Reason Stats";
$i_Discipline_System_Stat_Detention_Reason_Class = "Class-Reason Stats";
$i_Discipline_System_Stat_Detention_Subject_ClassLevel = "Form/Class-Subject Stats";
$i_Discipline_System_Stat_Detention_Subject_Class = "Class Subject Stats";
$i_Discipline_System_Stat_Discipline_ClassLevel = "Class Level Stats";
$i_Discipline_System_Stat_Discipline_Class = "Class Stats";
$i_Discipline_System_Stat_Punishment_ClassLevel = "Class Level Stats";
$i_Discipline_System_Stat_Punishment_Class = "Class Stats";
$i_Discipline_System_Stat_Others_Top_Punish_Student = "Most Punished Students";
$i_Discipline_System_Stat_Others_Top_Punish_Class = "Most Punished Class";
$i_Discipline_System_Stat_Others_Top_Award_Student = "Most Awarded Students";
$i_Discipline_System_Stat_Others_Top_Award_Class = "Most Awarded Class";
$i_Discipline_System_PunishCouncelling_Type_PunishmentFollowup = "Today's Punishment Follow-up";
$i_Discipline_System_PunishCouncelling_Type_DetentionFollowup = "Today's Detention Follow-up";
$i_Discipline_System_PunishCouncelling_Type_MeetingFollowup = "Today's Meeting Follow-up";
$i_Discipline_System_PunishCouncelling_Type_ExportNotice = "Export Notice";
$i_Discipline_System_PunishCouncelling_Punishment_NotExecuted = "Not Executed Punishment";
$i_Discipline_System_PunishCouncelling_Detention_TodayDetentionRecord = "Today's Detention Record";
$i_Discipline_System_PunishCouncelling_Detention_Attendance = "Detention Attendance";
$i_Discipline_System_PunishCouncelling_Detention_Attendance_Manual = "Detention Attendance (Manual)";
$i_Discipline_System_PunishCouncelling_Detention_New = "Add New Detention Record";
$i_Discipline_System_PunishCouncelling_Detention_Outstanding_Minutes = "Outstanding Minutes";
$i_Discipline_System_PunishCouncelling_Detention_This_Minutes = "Length of current detention";
$i_Discipline_System_PunishCouncelling_Detention_Past_Minutes = "Cumulative detention time";

$i_Discipline_System_Detention_Calentar_Instruction_Msg = "The Calendar view provides an overview of all detention sessions of the current month. You can assign students to available sessions from the Unassigned Students List.";
$i_Discipline_System_Student_Detention_Calentar_Instruction_Msg = "The Calendar view provides an overview of all detention sessions assigned for you in the current month. ";
$i_Discipline_System_Detention_List_Instruction_Msg = "The Session view shows detail information of detention sessions. You can assign students to available sessions from the Unassigned Students list.";
$i_Discipline_System_Detention_Student_Instruction_Msg = "The Student view shows detention arrangement of students. You can assign students to available sessions.";

$i_Discipline_System_PunishCouncelling_Meeting_Parent = "Today's Parent Meeting(s)";
$i_Discipline_System_PunishCouncelling_Meeting_Social = "Today's Counselling Appointment(s)";
$i_Discipline_System_PunishCouncelling_ExportNotice_Punishment = "Not Printed Punishment Notice(s)";
$i_Discipline_System_PunishCouncelling_ExportNotice_Award = "Not Printed Award Notice(s)";
$i_Discipline_System_PunishCouncelling_ExportNotice_Meeting = "Not Printed Meeting Notice(s)";
$i_Discipline_System_PunishCouncelling_ExportNotice_Detention = "Not Printed Detention Notice(s)";
$i_Discipline_System_PunishCouncelling_Conduct = "Conduct Score Adjustment";
$i_Discipline_System_PunishCouncelling_Conduct_ManualAdjustment = "Conduct Score Manual Adjustment";
$i_Discipline_System_PunishCouncelling_PointElimination = "Point Elimination";

$i_Discipline_System_PunishCouncelling_Detention_SetMinutes = "Set to attend all minutes";
$i_Discipline_System_PunishCouncelling_Detention_SetAllMinutes = "Set all students to attend all minutes";

$i_Discipline_System_Count_Option = "Late Counting Options";
$i_Discipline_System_Count_Semester = "Count the same School Year and the same Semester ONLY.";
$i_Discipline_System_Count_Year = "Count the same School Year ONLY.";
$i_Discipline_System_Count_All = "Count all records";
$i_Discipline_System_LateUpgrade_Rule_AddNew = "Add next rule";
$i_Discipline_System_LateUpgrade_Accumulated = "Cumulative Late Records";
$i_Discipline_System_LateUpgrade_Next = "Further Late Records";
$i_Discipline_System_LateUpgrade_Detention_Minutes = "Detention (Minutes)";

$i_Discipline_System_Report_DateSelected = "Date Selected";
$i_Discipline_System_Report_YearSemesterSelected = "Year/Semester Selected";
$i_Discipline_System_Report_DetailType_Demerit = "Misconduct Record";
$i_Discipline_System_Report_DetailType_Merit = "Reward Record";
$i_Discipline_System_Report_DetailType_Merit_Demerit = "Reward/Misconduct Record";
$i_Discipline_System_Report_DetailType_SpecialPunishment = "Special Punishment Record";
$i_Discipline_System_Report_DetailType_Detention = "Detention Attendance Record";
$i_Discipline_System_Report_DetailType_Punishment_Detention = "Detention Punishment Record";
$i_Discipline_System_Report_DetailType_Meeting = "Meeting Record";
$i_Discipline_System_Report_ClassReport_Grade = "Conduct Grade";
$i_Discipline_System_general_SortField = "Sorting Order";

$i_Discipline_System_Approval_Demerit_Waiting = "Misconduct Record(s) Waiting For Approval";
$i_Discipline_System_Approval_Merit_Waiting = "Reward Record(s) Waiting For Approval";
$i_Discipline_System_Approval_Amount = "Amount";
$i_Discipline_System_Approval_View = "View Detail";
$i_Discipline_System_Waiting_Approval_Event = "Event(s) Waiting For Approval";


$i_Discipline_System_Conduct_Initial_Score = "Base Score";
$i_Discipline_System_Conduct_Warning_Point = "Reminder Point";
$i_Discipline_System_Conduct_List_ConductScore_Dropped = "The conduct score of the following student(s) has/have dropped to the warning level";
$i_Discipline_System_Conduct_DroppedPassWarningPoint = "Dropped to the warning level";
$i_Discipline_System_Conduct_UpdatedScore = "Updated Conduct Score";
$i_Discipline_System_Conduct_ShowNotice = "Show Notice";
$i_Discipline_System_Conduct_Print_Remark = "Print Remark";
$i_Discipline_System_Conduct_ResetAllStudents = "Reset all students' conduct scores (Current semester)";
$i_Discipline_System_Conduct_ManualAdjustment = "Manual Adjustment";
$i_Discipline_System_Conduct_CurrentScore = "Current Conduct Score";
$i_Discipline_System_Subscore1_CurrentScore = "Current Study Score";
$i_Discipline_System_Conduct_ScoreDifference = "Added / Deducted Conduct Score";
$i_Discipline_System_Conduct_UpdatedScore = "Updated Conduct Score";
$i_Discipline_System_Subscore1_ScoreDifference = "Added / Deducted Study Score";
$i_Discipline_System_Conduct_AdjustTo = "Adjust To";
$i_Discipline_System_Conduct_ConductGradeRule = "Conduct Grade Rule";
$i_Discipline_System_Conduct_ConductGradeRule_MinScore = "Min. Conduct Mark";
$i_Discipline_System_Conduct_ConductGradeRule_CalculatedGrade = "Equivalent Grade";
$i_Discipline_System_Calculation_ScoreRatio = "Score Ratio";

# modified by henry on 18 Nov 2008
$i_Discipline_System_Conduct_Edit_Conduct_Grade_Rule = "Edit Conduct Grade Rule";
$i_Discipline_System_Conduct_Warning_Rules = "Warning Rules";
$i_Discipline_System_Conduct_Base_Mark = "Base Mark";
$i_Discipline_System_Conduct_Grading_Scheme = "Grading Scheme";
$i_Discipline_System_Conduct_Semester_Ratio = "Scheme Ratio";
$i_Discipline_System_Conduct_alert_min_conduct_mark = "Please fill in Min. Conduct Mark";
$i_Discipline_System_Conduct_Instruction = "Instructions";
$i_Discipline_System_Conduct_Instruction_Msg = "The update of Base Mark will not change the existing records.";
$i_Discipline_System_Conduct_Conduct_Balance_Auto_Update = "All students' conduct marks will be reset to the value entered regarless of existing balances. Are you sure you want to continue?";
$i_Discipline_System_Conduct_Instruction_Recalculate_Msg = "Please re-compute conduct grade at <b>Management > Conduct mark</b> after any changes on the grading scheme.";
$i_Discipline_System_Conduct_Warning_Rules_List = "Warning Rules List";
$i_Discipline_System_Conduct_Warning_Reminder_Point = "Warning reminder point";
$i_Discipline_System_Conduct_School_Year = "School Year";
$i_Discipline_System_Conduct_Semester_Ratio = "Semester Ratio";
$i_Discipline_System_Conduct_Ratio2 = "Ratio";
$i_Discipline_System_Conduct_Semester = "Semester";
$i_Discipline_System_Conduct_Ratio = "Ratio";
$i_Discipline_System_Conduct_Total = "Total";

# Modified by henry on 30 Dec 2008
$i_Discipline_System_Reports_Instruction_Msg = "Student Report shows award/punishment records and good conduct/misconduct records possessed by the  selected student(s). You can set report period and the type(s) of disciplinary records to show below.";
$i_Discipline_System_Reports_Report_Option = "- Options -";
$i_Discipline_System_Reports_First_Section = "- Award/Punishment Record -";
$i_Discipline_System_Reports_Second_Section = "- Good Conduct/Misconduct Records -";
$i_Discipline_System_Reports_Award_Punishment = "Award(s)/ Punishment(s)";
$i_Discipline_System_Reports_All_Awards = "All Awards";
$i_Discipline_System_Reports_All_Punishment = "All Punishments";
$i_Discipline_System_Reports_Include_Waived_Record = "Include Waived Record ";
$i_Discipline_System_Reports_Report_Period = "Report Period";
$i_Discipline_System_Reports_Conduct_Behavior = "Conduct / Behavior";
$i_Discipline_System_Reports_All_Good_Conducts = "All Good Conducts";
$i_Discipline_System_Reports_All_Misconduct = "All Misconduct";
$i_Discipline_System_Reports_Discipline_report = "Discipline Report";
$i_Discipline_System_Reports_Show_Reports_Option = "Show Options";
$i_Discipline_System_Reports_Misconduct_Behavior = "Misconduct Behavior";
$i_Discipline_System_Reports_Invalid_Date_Compare = "Start Date should early than End Date";
$iDiscipline['RankingTarget'] = "Target";
$iDiscipline['RecordType'] = "Record Type";
$iDiscipline['RecordType2'] = "Record Type";
$iDiscipline['Awarded'] = "awarded";
$iDiscipline['Punished'] = "punished";
$iDiscipline['Rank'] = "Rank";
$iDiscipline['RankingRange'] = "Ranking Range";
$iDiscipline['ReceiveType'] = "Receive Type";
$iDiscipline['byCategory'] = "By Category";
$iDiscipline['byItem'] = "By Item";
$iDiscipline['HorizontalDisplay'] = "Horizontal";
$iDiscipline['StackDisplay'] = "Stack";

# Conduct Grade
$iDiscipline['ConductGradeAssessment'] = "Conduct Grade Assessment";
$iDiscipline['ConductGradeMeeting'] = "Conduct Grade Meeting";
$iDiscipline['CompleteRatio'] = "Complete Ratio";
$iDiscipline['Assessment'] = "Assessment";
$iDiscipline['Percentage'] = "Percentage";
$iDiscipline['TotalAward'] = "Total no. of award";
$iDiscipline['TotalPunishment'] = "Total no. of punishment";
$iDiscipline['ViewConductGradeOf1stSemester'] = "View Conduct Grade of 1st Semester";
$iDiscipline['ConductGradeOverview'] = "Conduct Grade Overview";
$iDiscipline['ConductGradeFinal'] = "Final Conduct Grade";
$iDiscipline['Grade'] = "Grade";
$iDiscipline['Grade Range'] = "Grade Range";
$iDiscipline['TeachersCompleted'] = "Teachers Completed";
$iDiscipline['ReAssessmentOfConductGrade'] = "Re-Assessment of Conduct Grade";
$iDiscipline['OriginalResult'] = "Original Result";
$iDiscipline['IntegratedConductGrade'] = "Integrated Conduct Grade";
$iDiscipline['Overall'] = "Overall";
$iDiscipline['Revised'] = "Revised";
$iDiscipline['WaitingForReview'] = "Waiting for review";
$iDiscipline['Class_Report'] = "Class Report";
$iDiscipline['Teacher_Report'] = "Teacher Report";
$iDiscipline['NoAssessmentIsNeeded'] = "No assessment is needed.";
$iDiscipline['PrintedBy'] = "Printed by";
$iDiscipline['NeedToReassessInConductMeeting'] = "若第一欄\"註\"顯示「<font color='red'>*</font>」即表示該生某些項目之評級須於操行會議作討論。";
$iDiscipline['ClassNo'] = "Class Number";
$iDiscipline['ChiName'] = "Chinese Name";
$iDiscipline['Grade2'] = "Grade";
$iDiscipline['Remark'] = "Remark";
$iDiscipline['IntegratedConduct'] = "Integrated Conduct";
$iDiscipline['TableTopText1'] = "各項目之等級(";
$iDiscipline['TableTopText2'] = "給予學生之等級) / 等級範圍 / 參與評級老師之百分比";

# conduct report
$eDiscipline['Student_Conduct_Report'] = "Student Conduct Report";
$eDiscipline['Item'] = "Item";
$eDiscipline['PICTeacher'] = "PIC";
$eDiscipline['ConductReportPeriod'] = "Period";
$eDiscipline['AccumulateHomeworkNotSubmitted'] = "Accumulated homework not submitted in ";
$BaptistWingLungSecondarySchool = "浸信會永隆中學<br>BAPTIST WING LUNG SECONDARY SCHOOL";
$eDiscipline['Remark1ForWaiveRecord'] = "註一：\"R\"已抵消之記錄";
$eDiscipline['Remark2ForPrefectReport'] = "註二：由領袖生報告之記錄已交訓導老師查證並核對。";
$eDiscipline['PrincipalSignature'] = "Principal's Signature";
$eDiscipline['DisciplineGroupSignature'] = "Discipline Group's Signature";
$eDiscipline['GuardianSignature'] = "Parent's / Guardian's Signature";
$eDiscipline['Date'] = "Date";

# Modified by henry on 30 Dec 2008
$eDiscipline['NewLeaf_Notes'] = "Attention: After records are waived, they will not be unwaived automatically even when additional misconduct records within the rehabilitation period are added afterward.";

######################

$i_Discipline_System_Subscore1 = "Study Score";
$i_Discipline_System_Subscore1_ResetAllStudents = "Reset all students' Study Score (Current semester)";
$i_Discipline_System_alert_reset_subscore1 = "Are you sure to reset all student's records?";
$i_Discipline_System_Settings_Calculation_SubScore1_Annual = "Annual Study Score";
$i_Discipline_System_Subscore1_UpdatedScore = "Updated Study Score";
$i_Discipline_System_Subscore1_List_SubScore_Dropped = "The Study Score of the following student(s) has/have dropped to the warning level";
$i_Discipline_System_PunishCouncelling_Subscore1_ManualAdjustment = "Study Score Manual Adjustment";

# added on 31 Jan 2008
$i_Discipline_System_Notice_RecordType = "Record Type";
$i_Discipline_System_Notice_RecordType1 = "Notice";
$i_Discipline_System_Notice_RecordType2 = "Warning";
#################

# added on 4 Feb 2008
$i_Discipline_System_Notice_Date = "Date";
$i_Discipline_System_Notice_Pending = "Pending Notice(s)";
$i_Discipline_System_Notice_Show = "Show";
$i_Discipline_System_Notice_Remove = "Remove";
$i_Discipline_System_Notice_Warning_Please_Select = "Please select at least one record.";
$i_Discipline_System_Notice_Alert_Remove_Notice = "The selected notice record(s) will be deleted. Please print it/them";
####################

$i_Discipline_System_Notice_AttachItem = "Attach Demerit List";
$i_Discipline_System_Notice_Following = "The following is";
$i_Discipline_System_Notice_Demerit_Receord = "demerit record(s)";
$i_Discipline_System_Option_SameEvent = "Identical Event";
$i_Discipline_System_Option_DifferentEvent = "Different Events";

$i_Discipline_System_Select_Student = "Select student(s)";
$i_Discipline_System_Add_Record = "Add record to student(s)";
$i_Discipline_System_Over = "over";
$i_Discipline_System_Times = "time(s)";
$i_Discipline_System_Conduct_Change_Log ="Conduct Score Change Log";
$i_Discipline_System_SubScore1_Change_Log ="Study Score Change Log";
$i_Discipline_System_FromScore="From Score";
$i_Discipline_System_ToScore ="To Score";
$i_Discipline_System_New_MeritDemerit="Input Record";
$i_Discipline_System_Edit_MeritDemerit="Update Record";
$i_Discipline_System_Remove_MeritDemerit="Remove Record";
$i_Discipline_System_Record_Already_Removed="Record has been Removed";
$i_Discipline_System_Manual_Adjustment="Manual Adjustment";
$i_Discipline_System_System_Revised="System Revised";
$i_Discipline_System_Previous_Student="Previous Student";
$i_Discipline_System_Next_Student="Next Student";
$i_Discipline_System_Print_WarningLetter="Print Warning Letter";
$i_Discipline_System_WaiveStatus ="Waive Status";
$i_Discipline_System_WaiveStatus_Waived ="Waived";
$i_Discipline_System_WaiveStatus_Waived_Prefix = "Including ";
$i_Discipline_System_WaiveStatus_Waived_Subfix = " waived record(s)";
$i_Discipline_System_WaiveStatus_NoWaived="No Waive";
$i_Discipline_System_WaiveStatus_InProgress="In progress";
$i_Discipline_System_WaiveStatus_Success="Success";
$i_Discipline_System_WaiveStatus_Fail="Failed";
$i_Discipline_System_WaiveStatus_ChangeStatus_Warning="Re-Challenging the Waive program, the previous fail record will be overwritten. Continue?";
$i_Discipline_System_RecordNotYet_Arppoved ="Cannot Show Notice. Record(s) are waiting for approval";
$i_Discipline_System_Waive_NoUpdate_Warning="This record has been set to waived, any changed data (except Remark) will not be saved";
$i_Discipline_System_Set_Status_Warning="Please select $i_Discipline_System_WaiveStatus ";
$i_Discipline_System_Show_WaiveStatus="Show Waive Records";
$i_Discipline_System_No_WaiveStatus="Hide Waive Records";
$i_Discipline_System_WaiveStartDate="Waive Start Date";
$i_Discipline_System_WaiveEndDate="Waive End Date";
$i_Discipline_System_Waive_LostDateInfo_Warning="Setting status to $i_Discipline_System_WaiveStatus_NoWaived will lost all previous waive date information.";
$i_Discipline_System_Waive_LostEndDateInfo_Warning="Setting status to $i_Discipline_System_WaiveStatus_InProgress will lost the previous waive end date information.";
$i_Discipline_System_Please_Select_Record = "Please select the type(s) of records to show.";

# added on 16 Jan 2008 #
$i_Discipline_System_Frontend_menu_eDiscipline = "eDiscipline";

# added on 29 Jan 2008 #
$i_Discipline_System_Conduct_KeepNotice = "Keep Notice";
########################

# eDiscipline v1.2 20081105
$i_Discipline_Date = "Date";
$i_Discipline_Time = "Time";
$i_Discipline_Applicable_Form = "Applicable Form";
$i_Discipline_Form = "Form";
$i_Discipline_Location = "Location";
$i_Discipline_Duty_Teacher = "Duty Teacher";
$i_Discipline_Vacancy = "Vacancy";
$i_Discipline_Assigned = "Assigned";
$i_Discipline_Attended = "Attended";
$i_Discipline_StartEnd_Time_Alert = "Invalid Time. Start Time must be earlier than End Time.";
$i_Discipline_Invalid_Vacancy = "Invalid Vacancy";
$i_Discipline_Date_Selected = "Date Selected";
$i_Discipline_Assigned_Session_Cannot_Be_Deleted = "Assigned session cannot be deleted.";
$i_Discipline_Delete_Session_Alert = "Are you sure to delete the session?";
$i_Discipline_Delete = "Delete";
$i_Discipline_All_Forms = "All Forms";
$i_Discipline_Select = "Select";
$i_Discipline_Remove = "Remove Selected";
$i_Discipline_Select_Duty_Teachers = "Select Duty Teacher(s)";
$i_Discipline_All_Date = "All Dates";
$i_Discipline_All_Week = "All Week";
$i_Discipline_This_Month = "This Month";
$i_Discipline_This_Month2 = "This Month";
$i_Discipline_This_Week = "This Week";
$i_Discipline_This_Week2 = "This Week";
$i_Discipline_Next_Week = "Next Week";
$i_Discipline_All_Status = "All Statuses";
$i_Discipline_All_Classroom = "All Classrooms";
$i_Discipline_Available = "Available";
$i_Discipline_Full = "Full";
$i_Discipline_Finished = "Finished";
$i_Discipline_All_Time = "All Times";
$i_Discipline_All_Duty = "All Duty";
$i_Discipline_Weekday = "Weekday";
$i_Discipline_In_The_Past_Alert = " is in the past";
$i_Discipline_Records = "Records";
$i_Discipline_Session_List = "Session List";
$i_Discipline_New_Sessions = "New Session(s)";
$i_Discipline_Edit_Session = "Edit Session";
$i_Discipline_Detention_List = "Detention List";
$i_Discipline_Detention_All_Status = "All Status";
$i_Discipline_Detention_All_Time_Slot = "All Time Slots";
$i_Discipline_Detention_All_Teachers = "All Teachers";
$i_Discipline_New_Student_Records = "New Student Record(s)";
$i_Discipline_Select_Student = "Select student(s)";
$i_Discipline_Add_Record = "Add record to student(s)";
$i_Discipline_Enotice_Setting = "Set Up eNotice";
$i_Discipline_Apply_To_All = "Apply to All";
$i_Discipline_Previous = "Previous";
$i_Discipline_Next = "Next";
$i_Discipline_Detention_Times = "Detention Times";
$i_Discipline_Reason = "Reason";
$i_Discipline_Reason2 = "Receive";
$i_Discipline_Session = "Session";
$i_Discipline_Session2 = "Session";
$i_Discipline_Remark = "Remark";
$i_Discipline_Calendar = "Calendar";
$i_Discipline_List = "List";
$i_Discipline_Auto_Assign_Session = "Auto Assign Session";
$i_Discipline_New_Session = "New Session";
$i_Discipline_Student_Record = "Student Records";
$i_Discipline_Take_Attendance = "Take Attendance";
$i_Discipline_Assign_To_Here = "Assign to here";
$i_Discipline_Assign_To_Here_Alert = "Are you sure to assign to here?";
$i_Discipline_Unassign_Student_List = "Unassigned Student List";
$i_Discipline_Unassign_Student = "Unassigned Student";
$i_Discipline_All_Classes = "All Classes";
$i_Discipline_Student = "Student";
$i_Discipline_Times_Remain = "Times<br />Remaining";
$i_Discipline_Times = "Times";
$i_Discipline_View_Student_Detention_Arrangement = "View Detention Arrangement";
$i_Discipline_PIC = "PIC";
$i_Discipline_Vacancy_Is_Not_Enough = "Vacancy is not enough.";
$i_Discipline_Auto_Arrange = "Auto-arrange";
$i_Discipline_Arrange = "Arrange";
$i_Discipline_Arrange_Alert = "Are you sure you want to assign a detention session to the student(s) selected?";
$i_Discipline_Auto_Arrange_Alert = "Are you sure you want eDiscipline to assign a detention session to the student(s) selected?";
$i_Discipline_Detention_Details = "Detention Details";
$i_Discipline_Detention_Info = "Detention Info";
$i_Discipline_Students_Attending = "Student(s) Attending";
$i_Discipline_Attending_Student_List = "Attending Student List";
$i_Discipline_Student_List = "Student List";
$i_Discipline_Cancel_Arrangement = "Cancel Arrangement";
$i_Discipline_Class = "Class";
$i_Discipline_Detention_Reason = "Detention Reason";
$i_Discipline_Request_Date = "Request Date";
$i_Discipline_Arranged_Session = "Arranged Session";
$i_Discipline_Status = "Status";
$i_Discipline_Cancel_Arrange_Alert = "Are you sure to cancel selected arrangement(s)?";
$i_Discipline_Display_Photos = "Display Photos";
$i_Discipline_Hide_Photos = "Hide Photos";
$i_Discipline_Set_Absent_To_Present = "Set All to \"Present\"";
$i_Discipline_Last_Updated = "Last Updated";
$i_Discipline_Last_Updated_Str = "Last Updated: %DATE% by %NAME%";
$i_Discipline_Take_Attendance_Alert = "Are you sure to update records?";
$i_Discipline_Present = "Present";
$i_Discipline_Absent = "Absent";
$i_Discipline_Attendance = "Attendance";
$i_Discipline_Details = "Details";
$i_Discipline_Take = "Take";
$i_Discipline_Waiting_For_Arrangement = "Waiting for Arrangement";
$i_Discipline_Arranged = "Arranged";
$i_Discipline_Not_Yet = "Not Taken Yet";
$i_Discipline_All_Students = "All Students";
$i_Discipline_Not_Assigned = "Not Assigned";
$i_Discipline_Detented = "Detented";
$i_Discipline_Send_Notice_Via_ENotice = "Send Notice via eNotice";
$i_Discipline_Template = "Template";
$i_Discipline_Usage_Template = "Reason/Template";
$i_Discipline_Additional_Info = "Additional Info";
$i_Discipline_Select_Category = "Select Category";
$i_Discipline_Session_Arranged_Before = "Session has been arranged before.";
$i_Discipline_Session_Not_Arranged_Before = "Session has not been arranged before.";
$i_Discipline_Event_Type = "Event Type";
$i_Discipline_No_of_Records = "No. of Records";
$i_Discipline_No_of_Records2 = "No. of Records";
$i_Discipline_Pending_Event = "Pending Event(s)";
$i_Discipline_Released_To_Student = "Released to Student";
$i_Discipline_No_Action = "None";
$i_Discipline_Attendance_Sheet = "Attendance Sheet";
$i_Discipline_Session_Info = "Session Info";
$i_Discipline_Statistics_Options = "Options";
$i_Discipline_Show_Statistics_Option = "Show Options";
$i_Discipline_Hide_Statistics_Option = "Hide Options";
$i_Discipline_School_Year = "School Year";
$i_Discipline_Semester = "Semester";
$i_Discipline_By = "By";
$i_Discipline_Target = "Target";
$i_Discipline_Press_Ctrl_Key = "Press Ctrl key to select multiple items";
$i_Discipline_No_Of_Detentions = "Total No. of Detentions";
$i_Discipline_Reasons = "No. of Particular Detention Reason(s)";
$i_Discipline_Detention_Reasons = "No. of Records of Specified Detention Reason";
$i_Discipline_Subjects = "Subjects";
$i_Discipline_Subject = "Subject";
$i_Discipline_Generate = "Generate";
$i_Discipline_Generate_Update_Statistic = "Generate/Update";
$i_Discipline_Generate_Update_Report = "Generate/Update";
$i_Discipline_Put_Into_Unassign_List = "Put into unassign list";
$i_Discipline_Quantity = "Quantity";
$i_Discipline_No_Of_Merits_Demerits = "Total No. of Records";
$i_Discipline_Award_Punishment_Titles = "No. of Records of Specified Record Type";
$i_Discipline_Show_Only = "Range";
$i_Discipline_Top = "Top";
$i_Discipline_No_Of_GoodConductsMisconducts = "Total No. of Records";
$i_Discipline_GoodConductMisconduct_Titles = "No. of Records of Specified Record Type";
$i_Discipline_GoodConduct = "Good Conduct";
$i_Discipline_Misconduct = "Misconduct";
$i_Discipline_Conduct_Grade = "Conduct Grade";
$iDiscipline['Confirmation'] = "Confirmation";
$iDiscipline['ConductCategoryItem'] = "Conduct Category/Item";
$iDiscipline['SelectBehaviourItem'] = "-- Select Behaviour Item --";
$eDiscipline["CompareShow"] = "Compare/Show";
$i_Discipline_Award_Punishment_Miss_Titles = "You have not selected any award/punishment category and item(s).";


$iDiscipline['By'] = "by";
$iDiscipline['Period_Start'] = "From";
$iDiscipline['Period_End'] = "To";
$iDiscipline['Period_SchoolYear'] = "Corresponding ".$i_Profile_Year;
$iDiscipline['Period_Semester'] = "Corresponding ".$i_Profile_Semester;
$iDiscipline['Accumulative_Category'] = "Category";
$iDiscipline['Accumulative_Category_EditItem'] = "Edit Item";
$iDiscipline['Accumulative_Category_EditPeriod'] = "Edit Period Condition";
$iDiscipline['Accumulative_Category_Name'] = "Category Name";
$iDiscipline['Accumulative_DefaultCat_Late'] = "Late";
$iDiscipline['Accumulative_DefaultCat_Homework'] = "Homework Not Submitted";
$iDiscipline['Accumulative_DefaultCat_Uniform'] = "Improper Uniform";
$iDiscipline['Accumulative_Category_Item_Name'] = "Item Name";
$iDiscipline['Accumulative_Category_Homework_Intranet_Homework_List']="Intranet Subject List";
$iDiscipline['Accumulative_Period_Overlapped_Warning']="The period cannot be overlapped.";
$iDiscipline['Accumulative_Period_InUse_Warning']="Cannot Remove Record(s). Some of the selected periods are already in use.";
$iDiscipline['Accumulative_Homework_ChangeList_Warning']="create new ".$i_Homework_subject." will stop using ".$iDiscipline['Accumulative_Category_Homework_Intranet_Homework_List'];
$iDiscipline['Accumulative_Demerit_Times']="Accumulated Demerit Count";
# added on 01 Sept 2008
$iDiscipline['Accumulative_Merit_Times']="Accumulated Merit Count";

$iDiscipline['Accumulative_Punishment']="Punishment";
$iDiscipline['Period']="Period";
$iDiscipline['Accumulative_Category_Period']="Corresponding ".$iDiscipline['Period'];
$iDiscipline['Accumulative_Category_Select_Period']="Select Period(s)";
$iDiscipline['Accumulative_Category_Warning_Select_Period']="Please select period(s)";
$iDiscipline['Accumulative_Category_Warning_Invalid_Accumulated_Demerit']="Invalid ".$iDiscipline['Accumulative_Demerit_Times'];
$iDiscipline['Accumulative_Select_Category']="Select Category";
$iDiscipline['Accumulative_Please_Select_Category']="Please select ".$iDiscipline['Accumulative_Category'];
$iDiscipline['SetAll']="Set to All";
$iDiscipline['GeneralSettings']="General Settings";
$iDiscipline['Accumulative_Category_Item'] = "Item";
$iDiscipline['Accumulative_Punishment_Warning_Select_Category']="Please select ".$iDiscipline['Accumulative_Category'];
$iDiscipline['Accumulative_Punishment_Warning_Select_Item']="Please select ".$iDiscipline['Accumulative_Category_Item'];
$iDiscipline['Accumulative_Punishment_Warning_Select_Subject']="Please select ".$i_Homework_subject;
$iDiscipline['Warning_Enter_Integer'] ="The score must be an integer";
$iDiscipline['Accumulative_Punishment_Report']="Accumulative Misconduct Record";
# added on 3 Sept 2008
$iDiscipline['Accumulative_Reward_Report']="Accumulative Reward Record";

$iDiscipline['Accumulative_Punishment_No_Category_Period_Setting']="no linked period";
$iDiscipline['Accumulative_Period_In_Use_Warning']= "Misconduct records already exist for this period. New settings will be effective for new records only.";
$iDiscipline['Accumulative_Category_EditPeriod_UseRule'] ="Edit Period Condition (Use Upgrade Rules)";
$iDiscipline['Accumulative_Category_NextNumDemerit']="Further Demerit Records";
$iDiscipline['Outdated_Page_Waring']="The page is outdated, visit eAdmin > eDiscipline > Setting";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme']="Calculation Scheme";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme']="Calculation Scheme";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_Details']="Calculation Scheme Details";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1']="Fixed Range";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_2']="Accumulative";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_Undefined']="Undefined";
$iDiscipline['Accumulative_Category_Period_Select_Calculation_Scheme']="Select ".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme'];
$iDiscipline['Accumulative_Category_Selected_Period']="Selected Period";
$iDiscipline['Accumulative_Category_Period_Set_Details']="Setting Calculation Details";
$iDiscipline['Accumulative_Category_Period_Set_Details_Warning']="Please set at least one punishment method.";
$iDiscipline['Accumulative_Category_Period_Set_Details_Warning_2']="Please set at least one punishment method.";
$iDiscipline['Management_Child_AccumulativePunishmentAlert']="Accumulative Misconduct Alert";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_NoNeed_Alert']="No Need To Alert";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_NA']="Not Applicable";

$iDiscipline['Management_Child_AccumulativePunishmentAlert_AlertCount']="Accumulated Punishment Count to Alert";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_DateRange_Part1']="View Records Starting From :";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Notice']="Notice";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendEmailCampusmail']="Send Email / Campusmail";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendEmail']="Send Email to";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendCampusmail']="Send Campusmail to";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Show_TeacherNotice']="Class Teacher Notice Letter";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Show_ParentNotice']="Show Parent Notice Letter";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Select_Recipient']="Please select recipient";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Mail_Sent']="The mails has been sent.";
$iDiscipline['Reports_Child_AccumulativePunishmentReport']="Accumulative Misconduct Report";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_PrintAll']="Print All Classes";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Total']="Total";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Item']="time(s)";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_BlackMarkCount']="Black Mark Count";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Sum']="Total";
$i_Discipline_System_Merit = "Merit";
$i_Discipline_System_Demerit = "Demerit";
$iDiscipline['Reports_Child_AccumulativePunishmentClassReport']="Class Accumulative Misconduct (".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1'].") Report";
$iDiscipline['Reports_Child_AccumulativePunishmentCategoryReport']="Daily Accumulative Misconduct Report";
$iDiscipline['AccumulativePunishment_Preset_Category']="Asterisked items are preset categories. Deletion disabled.";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning1']="Note: Violation records under this preset category will always be treated as \"Late\" items regarless of its Category Name.";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning2']="Note: Violation records under this preset category will always be treated as \"Assignment Not Submitted\" items regarless of its Category Name.";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning3']="Note: Violation records under this preset category will always be treated as \"Improper Uniform\" items regarless of its Category Name.";
$iDiscipline['AccumulativePunishment_Preset_Category_Delete_Warning']="Selected items contains some non-deletable categories.";
$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning']="To create good conduct or misconduct records for this date, the date has to be within a accumulation period AND the period\'s conversion scheme has to be already defined. Please consult product documentations for more information.";
$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning2']="To create good conduct or misconduct records for this date, the date has to be within a accumulation period AND the period\'s conversion scheme has to be already defined. Please consult product documentations for more information.";
$iDiscipline['AccumulativePunishment_Period_FromDate_MustSmallerThan_ToDate']="Period Start Date Must be smaller than End Date";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_DateRange_Part2']="";
$iDiscipline['AccumulativePunishment_Setting_EditCategoryProperty']="Edit Category Property";
$iDiscipline['AccumulativePunishment_Setting_DeleteCategory']="Delete Category";
$iDiscipline['AccumulativePunishment_Import']="Accumulative Misconduct Import";
$iDiscipline['AccumulativePunishment_Parent']="Parent Punishment Category/ Item";
$iDiscipline['AccumulativePunishment_Report_Remark']="X/Y: Number of violations at present / number of violations to result in a punishment.";
$iDiscipline['AccumulativePunishment_Import_FileDescription1']="$i_UserClassName, $i_ClassNumber, $i_RecordDate, ".$iDiscipline['Accumulative_Category'].", ".$iDiscipline['Accumulative_Category_Item_Name'].", ".$i_Discipline_System_general_remark;
$iDiscipline['AccumulativePunishment_Import_FileDescription2']="$i_UserLogin, $i_RecordDate, ".$iDiscipline['Accumulative_Category'].", ".$iDiscipline['Accumulative_Category_Item_Name'].", ".$i_Discipline_System_general_remark;
$iDiscipline['AccumulativePunishment_Import_Failed_Reason']="Error";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason1']="No matched student";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason2']="No valid date";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason3']="No matched ".$iDiscipline['Accumulative_Category']." / ".$iDiscipline['Accumulative_Category_Item'];
$iDiscipline['AccumulativePunishment_Import_Failed_Reason4']="No period setting for target category";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason5']="No import of late records";
$iDiscipline['AccumulativePunishment_Input_Confirm']="Are you sure to submit?";
$iDiscipline['DuplicatdItemCode'] = "Duplicated Item Code";
$iDiscipline['item_code_not_found'] = "Item Code not found.";
$iDiscipline['invalid_date'] = "Invalid Event Date.";
$iDiscipline['student_not_found'] = "Student not found.";
$iDiscipline['MeritType'] = "Merit Type";
$iDiscipline['Award_Punishment_Type'] = "Award/Punishment Type";
$iDiscipline['DemeritType'] = "Demerit Type";
$iDiscipline['Award_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore, $i_Discipline_System_Subscore1, $i_Discipline_System_Add_Merit, ". $iDiscipline['MeritType'].", $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withSubject'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Subscore1 ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_Subject_name, $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withoutSubject'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Subscore1 ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_UserRemark";

# added on 01 Sept 2008
$iDiscipline['AccumulativeReward_Import']="Accumulative Reward Import";
# added on 04 Sept 2008
$iDiscipline['Reports_Child_AccumulativeRewardReport']="Accumulative Reward Report";
$iDiscipline['Reports_Child_AccumulativeRewardClassReport']="Class Accumulative Reward (".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1'].") Report";
$iDiscipline['Reports_Child_AccumulativeRewardCategoryReport']="Daily Accumulative Reward Report";
$iDiscipline['Reports_Child_AccumulativeRewardReport_RewardCount']="Merit Count";
$iDiscipline['AccumulativeReward_Report_Remark']="X/Y: Number of rewards at present / number of rewards to result in a merit.";


# added by Kelvin Ho on 25 Nov 2008
$i_Discipline_System_Discipline_Template_Name = "Template Name";
$i_Discipline_System_Discipline_Category = "Category";
$i_Discipline_System_Discipline_Situation = "Situation";
$i_Discipline_System_Discipline_Status = "Status";
$i_Discipline_System_Discipline_Reason_For_Issue = "Reason for Issue";
$i_Discipline_System_Discipline_Template_Published = "In Use";
$i_Discipline_System_Discipline_Template_All_Status = "All Status";
$i_Discipline_System_Discipline_Template_Draft = "Not In Use";
$i_Discipline_System_Discipline_Template_Title = "Template Name";
$i_Discipline_System_Discipline_Reply_Slip = "Reply Slip";
$i_Discipline_System_Discipline_Template_Title_JS_warning= "Please input a Template Title.";
$i_Discipline_System_Discipline_Template_Content_JS_warning= "Please input a Template Content.";
$i_Discipline_System_Discipline_Template_Subject_JS_warning= "Please input a Template Subject.";
$i_Discipline_System_Discipline_Template_Auto_Fillin = "Auto Fill-in";
$i_Discipline_System_Discipline_Template_Topic_Title = "Topic/Title";
$i_Discipline_System_Discipline_Conduct_Mark_Adjustment = "Adjustment";
$i_Discipline_System_Discipline_Conduct_Mark_Adjusted_Conduct_Mark = "Adjusted Conduct Mark";
$i_Discipline_System_Discipline_Conduct_Mark_Generate_Conduct_Grade = "Compute Conduct Grade";
$i_Discipline_System_Discipline_Conduct_Last_Updated = "Last Updated";
$i_Discipline_System_Discipline_Conduct_Last_Adjustment = "Last Adjustment";
$i_Discipline_System_Discipline_Conduct_This_Adjustment = "This Adjustment";

# added by Kelvin Ho on 1 Dec 2008
$i_Discipline_System_Discipline_Conduct_JS_alert = "Please input a mark";
# added by Kelvin Ho on 2 Dec 2008
$i_Discipline_System_Discipline_Case_Record_Case_Details = "Case Info";
$i_Discipline_System_Discipline_Case_Record_Case_Number = "Case Number";
$i_Discipline_System_Discipline_Case_Record_Case_Title = "Case Name";
$i_Discipline_System_Discipline_Case_Record_Category = "Category";
$i_Discipline_System_Discipline_Case_Record_Case_School_Year = "School Year";
$i_Discipline_System_Discipline_Case_Record_Case_Semester = "Semester";
$i_Discipline_System_Discipline_Case_Record_Case_Event_Date = "Event Date";
$i_Discipline_System_Discipline_Case_Record_Case_Location = "Location";
$i_Discipline_System_Discipline_Case_Record_Case_PIC = "Person-in-charge";
$i_Discipline_System_Discipline_Case_Record_Attachment = "Attachment";
$i_Discipline_System_Discipline_Conduct_Last_Generated = "Last Generated";
# added by Kelvin Ho on 3 Dec 2008
$i_Discipline_System_Discipline_Case_Record_Processing = "Processing";
$i_Discipline_System_Discipline_Case_Record_Finished = "Finished";
$i_Discipline_System_Discipline_Case_Record_Processed = "Processed";
$i_Discipline_System_Discipline_Case_Record_Case_Number_JS_alert = "Please input Case Number";
$i_Discipline_System_Discipline_Case_Record_Case_Number_Exists_JS_alert = "Case Number already exists";
$i_Discipline_System_Discipline_Case_Record_Case_Title_JS_alert = "Please input Case Title";
$i_Discipline_System_Discipline_Case_Record_Category_JS_alert = "Please input Category";
$i_Discipline_System_Discipline_Case_Record_Year_JS_alert = "Please input Year";
$i_Discipline_System_Discipline_Case_Record_Event_Date_JS_alert = "Please input Event Date";
$i_Discipline_System_Discipline_Case_Record_Location_JS_alert = "Please input Location";
$i_Discipline_System_Discipline_Case_Record_PIC_JS_alert = "Please input Person-in-charge";
$i_Discipline_System_Discipline_Case_Record_PIC = "PIC";
$i_Discipline_System_Discipline_Case_Record_Student_Involved = "Student(s) involved";
$i_Discipline_System_Discipline_Case_Record_Message = "You cannot view case information for locked records unless you are granted permission by the case PIC.";
$i_Discipline_System_Discipline_Case_Record_Case_List = "Case List";
$i_Discipline_System_Discipline_Case_Record_New_Case = "New Case";
$eDiscipline["CaseIsFinished"] = "<font color=red>Case is finished.</font>";
$eDiscipline["RecordIsWavied"] = "<font color=red>Record is already waived.</font>";
$eDiscipline["RecordIsRejected"] = "<font color=red>Record is already rejected.</font>";
$eDiscipline["RecordisGoodConduct"] = "<font color=red>Record is ".$i_Discipline_GoodConduct.".</font>";
$eDiscipline["RecordNotPassWaiveDate"] = "<font color=red>Record does not pass the Waive Day.</font>";
$eDiscipline["RecordHappenAgain"] = "<font color=red>Happen again within the Waive Period.</font>";
$eDiscipline["RecordNotEnoughNewLeaf"] = "<font color=red>Not enough quota to proceed.</font>";
$eDiscipline["RecordNoNewLeafSetting"] = "<font color=red>No new leaf setting.</font>";
$eDiscipline["RecordIsPending"] = "<font color=red>Record is waiting for approval.</font>";
$eDiscipline["RecordIsCase"] = "<font color=red>Record is created in Case Record.</font>";
$eDiscipline["RecordIsRedeemed"] = "<font color=red>Record is redeemed.</font>";
$eDiscipline["RecordIsAcc"] = "<font color=red>Record is converted by Goodconduct/Miscount records.</font>";
$eDiscipline["noDeleteRight"] = "<font color=red>You have no delete access right for this record.</font>";
$eDiscipline["RejectRecord"] = "Reject Record";
$eDiscipline["ApproveRecord"] = "Approve Record";
$eDiscipline["RecordIsApproved"] = "<font color=red>Record is already approved.</font>";

$i_Discipline_System_Discipline_Case_Record_Case = "Case";
$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete = "Are you sure you want to delete?";
# added by Kelvin Ho on 3 Dec 2008
$i_Discipline_System_Discipline_Case_Record_days_ago = "day(s) ago";
$i_Discipline_System_Conduct_Mark_Tips = "When one's conduct mark reaches a reminder point, warning will be sent.";

$i_Discipline_System_Discipline_Conduct_Mark_Grade_JS_alert = "Some characters are not allowed in equivalent grade";
$i_Discipline_System_Discipline_Conduct_Mark_Score_Integer_JS_alert = "Please input integer";
###########################################
## cases (added by YatWoon 20080211)
###########################################
$iDiscipline['CaseReport'] = "Case Report";
$iDiscipline['CreateReport'] = "Create Report";
$iDiscipline['SelectCaseReport'] = "Select Case Report";
$iDiscipline['SelectedCaseReport'] = "Selected Case Report";
$iDiscipline['ReportInfo'] = "Report Setup";
$iDiscipline['CaseChoice'] = array(
									array(1, "Violation of Mobile Phone Regulations"),
									array(2, "Violation of School Regulations"),
									array(3, "Case Record (form1)"),
									array(4, "Case Record (form4)"),
									array(5, "Warning Letter"),
									array(6, "Improper School Uniform"),
);

$iDiscipline['Venue'] = "Venue";
$iDiscipline['VenueChoice'] = array(
									array(1, "Classroom ([#Room#])"),
									array(2, "Corridor"),
									array(3, "Hall"),
									array(4, "New Annex"),
									array(5, "Tuck Shop"),
									array(6, "Playground"),
);

$iDiscipline['Others'] = "Others";
$iDiscipline['StudentInvolved'] = "Student(s) involved";
$iDiscipline['TearchersInvolved'] = "Tearcher(s) involved";
$iDiscipline['Description'] = "Description";
$iDiscipline['CourseOfAction'] = "Course of action";
$iDiscipline['CourseOfActionChoice'] = array(
									array(1, "Detention"),
									array(2, "Compensation"),
									array(3, "Suspension"),
									array(4, "Verbal Warning"),
									array(5, "Record in 'Black Book'"),
									array(6, "School Services"),
									array(7, "Social Services"),
									array(8, "Inform Parents"),
									array(9, "Refer to Counseling Committee / Social Worker"),
);
$iDiscipline['UniformAction'] = array(
									array(1, "See [#ActionTeacherName#] on [#ActionDate#] at [#ActionTime#] [#ActionAPM#]"),
									array(2, "Send student home to change"),
									array(3, "Detention on [#ActionDate#]"),
);
$iDiscipline['Remarks'] = "Remarks";
$iDiscipline['ReferenceNo'] = "Reference No";
$iDiscipline['Reason_s'] = "Options";
$iDiscipline['MobileReasons'] = array(
									array(1, "The phone was switched on during school hours;"),
									array(2, "He brought the mobile phone without prior application;"),
									array(3, "The phone rang during the lesson or during school hours;"),
									array(4, "The mobile phone had no \"Approved Label\" issued by the Discipline Committee;"),
);
$iDiscipline['NoReasonSelected'] = "Please select at least one option!";
$iDiscipline['NoProblemSelected'] = "Please select at least one option!";
$iDiscipline['DatesTimes'] = "Date(s) & Time(s)";
$iDiscipline['Details'] = "Details";
$iDiscipline['Punishment'] = "Punishment";
$iDiscipline['RecordInBlackBook'] = "Record in black book";
//$iDiscipline['DetailsOffence'] = "Details of offence";
$iDiscipline['DetailsDescription'] = "Details / Description";
$iDiscipline['RecordDate'] = "Record Date";
$iDiscipline['RecordTime'] = "Record Time";
$iDiscipline['UniformProblem'] = array(
									array(1, "Belt"),
									array(2, "School Badge / Tie"),
									array(3, "Shoes / Socks"),
									array(4, "Coloured Singlet / Vest"),
									array(5, "Ring / Accessories"),
									array(6, "Sweater"),
									array(7, "Hair Style"),
									array(8, "School Blazer"),
									array(9, "Trousers"),
									array(10, "Jacket"),
									array(11, "Shirt"),
);
$iDiscipline['UniformProblems'] = "Options";
$iDiscipline['fromStaffList'] = "from <b>Staff List</b>";
$iDiscipline['CaseHandledby'] = "Case Handled by";
$iDiscipline['FollowUp'] = "Follow Up";
$iDiscipline['PleaseSelectOption'] = "Please select at least one option.";
$iDiscipline['PreviewReport'] = "Preview Report";
$iDiscipline['RemoveRecord'] = "Are you sure you want to delete this?";

### added on 20080828 ###
$i_Discipline_System_Settings_Email_Notification = "Email Notification Settings";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Teachers = "Email Notification (Teachers)";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Parents = "Email Notification (Parents)";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Students = "Email Notification (Students)";
$i_Discipline_System_Settings_Email_Notification_Email_ClassTeacher = "Email class teachers for their students' new misconduct / reward records";
$i_Discipline_System_Settings_Email_Notification_Email_PIC = "Email teachers when the records is approved / rejected / edited";
$i_Discipline_System_Settings_Email_Notification_Email_Parents = "Email parent letter when issuing parent letter";
$i_Discipline_System_Settings_Email_Notification_Email_Student_Record = "Email students for their new misconduct / reward records";
$i_Discipline_System_Settings_Email_Notification_Email_Student_Detention = "Email students about the outstanding detention";
$i_Discipline_System_Record_Status_Updated = "Updated";

### added on 20080918 ###
$i_Discipline_System_ParentLetter_Title = "違規紀錄通知";
$i_Discipline_System_ParentLetter_Content1 = "敬啟者︰";
$i_Discipline_System_ParentLetter_Content2 = "貴子弟";
$i_Discipline_System_ParentLetter_Content3 = "於";
$i_Discipline_System_ParentLetter_Content4 = "違反校規";
$i_Discipline_System_ParentLetter_Content5 = "現給予懲罰";
$i_Discipline_System_ParentLetter_Content6 = "希　台端督促　貴子弟遵守校規，用心向學。";
$i_Discipline_System_ParentLetter_Content7 = "此致";
$i_Discipline_System_ParentLetter_Content8 = "貴家長";

$eDiscipline['RankingReport'] = "Ranking Report";
$eDiscipline['AwardPunishmentRanking'] = "Award & Punishment Ranking";
$eDiscipline['GoodconductMisconductRanking'] = "Good Conduct & Misconduct Ranking";
$eDiscipline['StudentReport'] = "Student Report";
$eDiscipline['ClassSummary'] = "Class Summary";
$eDiscipline['MonthlyReport'] = "Monthly Report";
$eDiscipline['UniformRecords'] = "Uniform Records";
$eDiscipline['Top10'] = $eDiscipline['RankingReport'];
$eDiscipline['MisconductReport'] = "Misconduct Report";
$eDiscipline['GoodConductMisconductReport'] = "Good Conduct & Misconduct Report";
$eDiscipline['AwardPunishmentReport'] = "Award & Punishment Report";
$eDiscipline['Conduct_Grade_Report'] = "Conduct Grade Report";

# added by marcus 3/7/09
$eDiscipline['Viewing'] = "Discipline Records";
$eDiscipline['ClassRank'] = " Ranking in Class";
$eDiscipline['ClassLevelRank'] = " Ranking in Class Level";
$eDiscipline['AwardRanking'] = "Award Ranking";
$eDiscipline['GoodconductRanking'] = "Good Conduct Ranking";
$eDiscipline['Detention_Reason'] = "Reason of Detention";
$i_Discipline_List_View = "List";
$i_Discipline_System_Student_Detention_List_Instruction_Msg = "The list view shows all of your detention arrangement.";
$i_Discipline_All_Arrangement = "All Arrangement";

$eDiscipline['no_ConductScore_MeritNum_selected'] = "No Merit/Demeit and Conduct Mark is selected. Continue?";

$eDiscipline['ReportGeneratedDate'] = "Report generated : ";
###########################################

$i_eSports = "eSports";
$i_Sports_Select_Menu = "Please select from left side menu.";
$i_Sports_System = "Sports Meeting System";
$i_Sports_menu_Settings = "Settings";
$i_Sports_menu_General_Settings = "Basic Settings";
$i_Sports_menu_Annual_Settings = "Annual Settings";
$i_Sports_menu_Arrangement = "Event Arrangement";
$i_Sports_menu_Item_Settings = "Item and Event Settings";
$i_Sports_menu_Report = "Result";
$i_Sports_menu_Participation = "Input Result";
$i_Sports_menu_Settings_House = "House Setting";
$i_Sports_menu_Settings_Lane = "Number of lanes";
$i_Sports_menu_Settings_Score = "Score Setting";
$i_Sports_menu_Settings_LaneSet = "Lane Position";
$i_Sports_menu_Settings_AgeGroup = "Age Group Setting";
$i_Sports_menu_Settings_GroupNumber = "Group Number Setting";
$i_Sports_menu_Settings_Participant = "Athlete Number";
$i_Sports_menu_Settings_ParticipantFormat = "Athlete Number Format Setting";
$i_Sports_menu_Settings_ParticipantGenerate = "Generate Athlete Number";
$i_Sports_menu_Settings_ParticipantRecord = "Participation Summary";
$i_Sports_menu_Settings_Enrolment = "Enrolment Setting";
$i_Sports_menu_Settings_TrackFieldName = "Item";
$i_Sports_menu_Settings_CommonTrackFieldEvent = "Common Track and Field Event";
$i_Sports_menu_Settings_TrackFieldEvent = "Event";
$i_Sports_menu_Settings_EnrolmentLimit = "Enrolment Limit";
$i_Sports_menu_Settings_Detail = "Sports Day Details";
$AllGroups = "All Groups";

$i_Sports_export_reuslt = "Export Athlete's Result";

#########################################################
$i_Sports_menu_Settings_YearEndClearing = "Year End Record Clearing";
#########################################################

$i_Sports_menu_Arrangement_EnrolmentUpdate = "Enrolment Update";
$i_Sports_menu_Arrangement_Schedule = "Lane Arrangement / Grouping";
$i_Sports_menu_Arrangement_Relay = "Relay User List";
$i_Sports_menu_Report_WholeSchool = "School Statistics";
$i_Sports_menu_Report_Class = "Class Statistics";
$i_Sports_menu_Report_House = "House Statistics";
$i_Sports_menu_Report_Event = "Event Statistics";
$i_Sports_menu_Report_EventRanking = "Event Rankings";
$i_Sports_menu_Report_HouseGroupScore = "House/Group Score";
$i_Sports_menu_Report_GroupChampion = "Group Champion";
$i_Sports_menu_Report_ClassEnrolment = "Class Enrolment Table";
$i_Sports_menu_Report_HouseEnrolment = "House Enrolment Table";
$i_Sports_menu_Report_ExportRecord = "Export Event Record";
$i_Sports_menu_Report_ExportRaceArrangement = "Export Event Arrangement";
$i_Sports_menu_Report_ExportRaceResult = "Event Result Paper Printout";
$i_Sports_menu_Report_ExportAthleticNumber = "Athlete Number Paper Printout";
$i_Sports_menu_Report_RaceRecord = "Event Record";
$i_Sports_menu_Report_RaceTopRecord = "Top Event Record";
$i_Sports_menu_Report_RaceResult = "Event Result";
$i_Sports_menu_Participation_TrackField = "Track and Field Record";
$i_Sports_menu_Participation_Relay = "Relay Record";

#added by marcus 20090713
$i_Sports_menu_Report_Best_Athlete_By_Group = "Best Athlete By Group";
$i_Sports_menu_Report_Best_Athlete_By_Class = "Best Athlete By Class";


$i_Sports_field_Total_Score = "Total Score";
$i_Sports_field_Score = "Score";
$i_Sports_field_NumberOfLane = "Number of Lanes";
$i_Sports_field_ScoreStandard = "Score Standard";
$i_Sports_field_Rank_1 = "1<sup>st</sup>";
$i_Sports_field_Rank_2 = "2<sup>nd</sup>";
$i_Sports_field_Rank_3 = "3<sup>rd</sup>";
$i_Sports_field_Rank_4 = "4<sup>th</sup>";
$i_Sports_field_Rank_5 = "5<sup>th</sup>";
$i_Sports_field_Rank_6 = "6<sup>th</sup>";
$i_Sports_field_Rank_7 = "7<sup>th</sup>";
$i_Sports_field_Rank_8 = "8<sup>th</sup>";
$i_Sports_field_Lane = "Lane";
$i_Sports_field_Rank = "Rank";
$i_Sports_field_Grade = "Grade";
$i_Sports_field_GradeName = "Grade Name";
$i_Sports_field_DOBUpLimit = "Earliest DOB";
$i_Sports_field_DOBLowLimit = "Latest DOB";
$i_Sports_field_GradeCode = "Group Code";
$i_Sports_field_Enrolment_Date_start = "Enrolment Date Start";
$i_Sports_field_Enrolment_Date_end = "Enrolment Date End";
$i_Sports_field_Event_Name = "Item";
$i_Sports_field_Event_Type = "Type";
$i_Sports_field_Group = "Group";
$i_Sports_Class = "Class";
$i_Sports_Time = "Time";
$i_Sports_field_Heat = "Heat";
$i_Sports_field_Remark = "Remark";
$i_Sports_field_Gender = "Gender";
$i_Sports_field_Result = "Result";
$i_Sports_Detail = "Detail";
$i_Sports_The = "The";
$i_Sports_TheLine1 = "Line ";
$i_Sports_TheLine2 = "";
$i_Sports_TheGroup1 = "Group ";
$i_Sports_TheGroup2 = "";
$i_Sports_Pos = "Position";
$i_Sports_Order = "Order";
$i_Sports_Line = "Line";
$i_Sports_Group = "Group";
$i_Sports_From = "From";
$i_Sports_To = "To";
$i_Sports_All = "All Items";
$i_Sports_All_Grade = "All Grades";
$i_Sports_Color_Palette = "Color Palatte";
$i_Sports_Color_Preview = "Color Preview";
$i_Sports_Metres = "Metre(s)";

$i_Sports_Enrolment_Warn_MaxEvent = "You can enrol at most";
$i_Sports_Enrolment_MaxEvent = "Maximun Number of Events that can be Enroled";
$i_Sports_Enrolment_Detail = "Enrolment Detail";
$i_Sports_Warn_Please_Select = "Please select or fill in at least one field";
$i_Sports_Warn_OpenGroup_Error = "A event cannot be set as open group event(s) and age group event(s) at the same time, please select again.";
$i_Sports_Warn_Select_Group = "Please select group";
$i_Sports_Warn_Please_Fill_EventName = "Please fill in the Event Name";
$i_Sports_Record_Holder = "Record Holder";
$i_Sports_New_Record = "New Record";
$i_Sports_New_Record_Delete = "New Record is removed.";
$i_Sports_Record = "Record";
$i_Sports_Record_Year = "Record Year";
$i_Sports_House = "House";
$i_Sports_Standard_Record = "Standard Record";
$i_Sports_First_Round = "First Round";
$i_Sports_Second_Round = "Second Round";
$i_Sports_Final_Round = "Final Round";
$i_Sports_Person_Per_Group = "Person Per Group";
$i_Sports_Num_Of_Group = "Number of Group";
$i_Sports_Random_Order = "Random Order";
$i_Sports_Race_Day = "Event Day";
$i_Sports_Item = "Item";
$i_Sports_Event_Jump = "High Jump";
$i_Sports_Enroled_Student_Count = "Number of students enroled";
$i_Sports_Enrolled = "Enrolled";
$i_Sports_Arranged_Student_Count = "Number of students arranged";
$i_Sports_Participation_Count = "Number of participation";
$i_Sports_Explain = "Explanation";
$i_Sports_Item_Without_Setting_Meaning = "This event have not been set";
$i_Sports_Not_Complete_Events_Meaning = "This event have not completed";
$i_Sports_Auto_Lane_Arrange_Reminder = "<font color=blue>*Please click submit button to start auto lane arrangement</font>";

$i_Sports_Re_Auto_Arrange_Lane = "Auto Lane Arrangement/Grouping";
$i_Sports_Arrange_Lane = "Lane Arrangement/Grouping";
$i_Sprots_Proceed_Arrange = "Click OK to proceed";
$i_Sports_Stu_Num_Exceed_Lane_Num ="Number of participants exceeds number of lanes. Please modify the event setting.";


############################################################################
$i_Sports_Warn_Please_Select_Rank=" Please select the Rank for Tie Breaker";
$i_Sports_Warn_Rank_Assigned_To_Others=" has been assigned to other Athlete";
$i_Sports_YearEndClearing_Msg1 = "This will clear the records of the past year.";
$i_Sports_YearEndClearing_Msg2 = "<font color=red>The records has been cleared.</font>";
$i_Sports_YearEndClearing_Msg3 = "This will:<li>Clear the Enrolment Records<li>Update the Age Group Years<li>Update the Event Records";
$i_Sports_Rank = "Rank";
$i_Sports_YearEndClearing_Clear ="Clear";
$i_Sports_YearEndClearing_Continue="Continue";
############################################################################

$i_Sports_RecordBroken = "Record Broken";
$i_Sports_Qualified = "Qualified";

#################################
$i_Sports_Foul ="Foul";
$i_Sports_NotAttend="Not Attend";
##################################

$i_Sports_Day_Enrolment = "Sports Day Enrolment";
$i_Sports_Enrolment = "Enrolment ";
$i_Sports_Present = "Present";
$i_Sports_Absent = "Absent";
$i_Sports_Other = "Other";

#############################################
$i_Sports_Tie_Breaker= "Rank in tie breaker";
$i_Sports_Tie_Breaker_Rank ="Rank";
####################
$i_Sports_Count = "time(s)";
#####################

$i_Sports_Decrement = "Decrement";
$i_Sports_total = "Total";
$i_Sports_already_set = "Setup";
$i_Sports_Unsuitable= "Unsuitable";
$i_Sports_Event_Set_OnlineEnrol = "Online Enrol?";
$i_Sports_Event_Set_RestrictQuota = "Quota?";
$i_Sports_Event_Set_CountHouseScore = "Count House Score?";
$i_Sports_Event_Set_CountClassScore = "Count Class Score?";
$i_Sports_Event_Set_CountIndividualScore = "Count Individual Score?";
$i_Sports_Event_Set_ScoreStandard = "Score Standard";
$i_Sports_Event_Open_Group_Event = "Open Group Event(s)";
$i_Sports_Event_UnrestrictQuota_Event = "Unrestrict Quota Event(s)";
$i_Sports_Event_Open = "Open";
$i_Sports_Event_Boys_Open = "Boys Open";
$i_Sports_Event_Girls_Open = "Girls Open";
$i_Sports_Event_Mixed_Open = "Mixed Open";
$i_Sports_Event_Boys = "Boys";
$i_Sports_Event_Girls = "Girls";
$i_Sports_Event_Mixed = "Mixed";
$i_Sports_Event_All = "All";

$i_Sports_Participant = "Athlete";
$i_Sports_Participant_Number = "Athlete Number";
$i_Sports_Participant_Number_Type = "Type";
$i_Sports_Participant_Number_1stRow = "Row 1";
$i_Sports_Participant_Number_2ndRow = "Row 2";
$i_Sports_Participant_Number_3rdRow = "Row 3";
$i_Sports_Participant_Number_AutoNumLength = "Auto Num Length";
$i_Sports_Participant_Number_Generation_cond_HouseOrder = "House Order";
$i_Sports_Participant_Number_Generate = "Generate Athlete Number";
$i_Sports_Participant_Number_alert_generate = "Are you sure to generate Athlete Number?";
$i_Sports_Participant_Number_Summary_NumberOfStudents = "Number of Students";
$i_Sports_Participant_Number_Summary_NoClass  = "No Class";
$i_Sports_Participant_Number_Summary_NoClassNum = "No Class Num";
$i_Sports_Participant_Number_Summary_NoHouse = "No House";
$i_Sports_Participant_Number_Summary_MultipleHouse = "Multiple House";
$i_Sports_Participant_Number_Summary_NoGender = "No Gender";
$i_Sports_Participant_Number_Summary_NoDOB = "No DOB";
$i_Sports_Participant_Number_Summary_NoAthleticNumber = "No Athlete Number";
$i_Sports_AdminConsole_AccountAllowedToUse = "Accounts can config/use Sports System";
$i_Sports_AdminConsole_UserTypes = "User Type";
$i_Sports_AdminConsole_UserType['ADMIN'] = "Admin";
$i_Sports_AdminConsole_UserType['HELPER'] = "Helper";
$i_Sports_AdminConsole_Alert_ChangeAdminLevel = "Are you sure to change the admin level of the selected user(s)?";

$i_Sports_Report_Total_Participant_Count = "Total Number of Participant";
$i_Sports_Report_Total_Student_Count = "Total Number of Student";
$i_Sports_Report_Enrolment_Count = "Total Number of Enrolment";
$i_Sports_Report_Average_Enrolment_Count = "Average Number of Enrolment";
$i_Sports_Report_Item_Name = "Item Name";
$i_Sports_Arrange_Change_Postion = "Change Postion";
$i_Sports_Record_Generate_Second_Round = "Generate Second Round Participation List";
$i_Sports_Record_Generate_Final_Round = "Generate Final Round Participation List";
$i_Sports_Record_Alert_Duplicate_RankNumber = "Duplicated rank existed. Please select again";
$i_Sports_Record_Msg_Generate_Success = "<font color=red>Participation list generated successfully</font>";
$i_Sports_Record_Msg_Generate_Unsuccess = "<font color=red>Insufficient event records, participation list of next round can not ie generated</font>";
$i_Sports_Setting_Alert_FinalRound_Req = "If you have set up the second round event for this event, please also set up the final round event.";
$i_Sports_No_DOB = "Date of birth has not been defined";

$i_Sports_Participation_Score = "Participation Score";
$i_Sports_Event_Detail = "Event Details";
$i_Sports_Invalid_Data = "Invalid data";
$i_Sports_Edit_House_Alert = "Modifying house setting may affect other modules, proceed?";


####### Add for lunch box
$i_SmartCard_Lunchbox_System = "Lunch Box Management System";
$i_SmartCard_Lunchbox_Menu_Settings  = "System Settings";
$i_SmartCard_Lunchbox_Menu_DataInput = "Data Input";
$i_SmartCard_Lunchbox_Menu_StatusChecking = "View Daily Status";
$i_SmartCard_Lunchbox_Menu_Report = "Report";
$i_SmartCard_Lunchbox_Settings_Terminal = "Terminal Settings";
$i_SmartCard_Lunchbox_Settings_Calendar = "Monthly Calendar Settings";
$i_SmartCard_Lunchbox_Settings_DataClearance = "Data Clearance (To enhance performance)";
$i_SmartCard_Lunchbox_DataInput_Day = "Daily List";
$i_SmartCard_Lunchbox_DataInput_Month = "Monthly List";
$i_SmartCard_Lunchbox_Sync_From_Attendance = "Import List From Attendance of Today";
$i_SmartCard_Lunchbox_Total_LunchBox = "Number of Students in Daily List";
$i_SmartCard_Lunchbox_Total_Present = "Number of Present Students (including late)";
$i_SmartCard_Lunchbox_Sync_From_Attendance_Confirm = "Are you sure you want to import the students according to student attendance records (Present and Late)?";
$i_SmartCard_Lunchbox_Report_Generation = "Report Generation";
$i_SmartCard_Lunchbox_Report_View = "View Report";
$i_SmartCard_Lunchbox_Warning_CannotEditPrevious = "You cannot modify previous records";
$i_SmartCard_Lunchbox_Warning_AlreadyHasLunchTicket = "Some students have lunch box tickets in this month. Modify month calendar will <b>NOT</b> modify student lunch box records automatically.";
$i_SmartCard_Lunchbox_Warning_Save = "Are you sure to save?";
$i_SmartCard_Lunchbox_Warning_Change = "Are you sure to change?";
$i_SmartCard_Lunchbox_Warning_MonthlyCalendarNotExist = "This month calendar is not yet set. No student records can be added.";
$i_SmartCard_Lunchbox_DataField_NumTickets = "Number of days";
$i_SmartCard_Lunchbox_DateField_Date = "Date";
$i_SmartCard_Lunchbox_Action_AddStudent = "Edit List";
$i_SmartCard_Lunchbox_Action_SetTo = "Set to ";
$i_SmartCard_Lunchbox_Status_NotTaken = "Not taken";
$i_SmartCard_Lunchbox_Status_Taken = "Taken";
$i_SmartCard_Lunchbox_Status_NoTicket = "Not bought";
$i_SmartCard_Lunchbox_Report_Daily = "Daily Report";
$i_SmartCard_Lunchbox_Report_Month = "Monthly Report";
$i_SmartCard_Lunchbox_Report_BadAction = "Bad Action Report";
$i_SmartCard_Lunchbox_Report_Type_Summary = "Summary";
$i_SmartCard_Lunchbox_Report_Type_UntakenList = "Un-taken List";
$i_SmartCard_Lunchbox_Report_Type_TakenList = "Taken List";
$i_SmartCard_Lunchbox_Report_Type_StudentList = "All Students List";
$i_SmartCard_Lunchbox_Report_Type_DayBreakdown = "Daily Breakdown";
$i_SmartCard_Lunchbox_Report_Type_TopUntaken = "Top Students untaken";
$i_SmartCard_Lunchbox_Report_Type_BadAction_NoTicket = "Try to take lunch box but not bought before.";
$i_SmartCard_Lunchbox_Report_Type_BadAction_TakeAgain = "Try to take lunch box again";
$i_SmartCard_Lunchbox_Report_Param_Type = "Report Type";
$i_SmartCard_Lunchbox_Report_Param_SplitClass = "Split Class";
$i_SmartCard_Lunchbox_Report_Param_SelectClass = "Select Class";
$i_SmartCard_Lunchbox_Report_Lang_AllClasses = "All Classes";
$i_SmartCard_Lunchbox_Report_Field_Untaken_Times = "Untaken Times";
$i_SmartCard_Lunchbox_Report_Field_Untaken_Days = "Untaken Days";
$i_SmartCard_Lunchbox_Report_Words_NoUntaken = "No student has not taken.";
$i_SmartCard_Lunchbox_Report_Words_NoTaken = "No student has taken";
$i_SmartCard_Lunchbox_Report_Words_NoTicket = "No student has lunch box";
$i_SmartCard_Lunchbox_Import_Format_Login = "Intranet Login Format";
$i_SmartCard_Lunchbox_Import_Format_ClassNumber = "Class Name & Class Number Format";
$i_SmartCard_Lunchbox_Import_From_LastMonth = "Import From Last Month";
$i_SmartCard_Lunchbox_MonthTarget = "Target Month to copy";
$i_SmartCard_Lunchbox_TargetMonthNumStudents = "Students have lunchboxes in above Month";
$i_SmartCard_Lunchbox_NewMonth_Days = "Number of days have lunchboxes in new month";
$i_SmartCard_Lunchbox_Import_From_LastMonth_Description = "";


# yat 20090625
$i_eSwimmingGala = "eSwimmingGala";
$i_swimming_gala = "Swimming Gala";
$i_Swimming_Gala_Enrolment = "Swimming Gala Enrolment";


$i_House = "House";
$i_House_name = "House Name";
$i_House_GroupLink = "Intranet Group";
$i_House_ColorCode = "House Color";
$i_House_HouseCode = "House Code";

$i_Community_Communities = "Groups";
$i_Community_MyCommunities = "My Groups";
$i_Community_CommunityDirectory = "Group Directory";
$i_Community_CommunityFunction_Settings = "Settings";
$i_Community_Community_GoTo = "Go To";
$i_Community_EventAll = "All";
$i_Community_EventUpcoming = "Upcoming";
$i_Community_EventPast = "Past";

$i_ServiceMgmt_System = "Student Service Time Management System";
$i_ServiceMgmt_System_Settings = "$i_ServiceMgmt_System Settings";
$i_ServiceMgmt_System_Admin = "System User Access Right";
$i_ServiceMgmt_System_Description_SetAdmin = "Please Select which user can use and choose the admin level.";
$i_ServiceMgmt_System_AccessLevel = "Access Level";
$i_ServiceMgmt_System_AccessLevel_Normal = "Normal User";
$i_ServiceMgmt_System_AccessLevel_High = "Administrator";
$i_ServiceMgmt_System_AccessLevel_Detail_Normal = "$i_ServiceMgmt_System_AccessLevel_Normal (Allowed to input data and view reports)";
$i_ServiceMgmt_System_AccessLevel_Detail_High = "$i_ServiceMgmt_System_AccessLevel_High (Allowed to modify settings and student requirements)";

$i_ServiceMgmt_System_Menu_Report = "Report";
$i_ServiceMgmt_System_Menu_ClassReport = "Class View";
$i_ServiceMgmt_System_Menu_InputNew = "New Service Hours";
$i_ServiceMgmt_System_Menu_PresetActivity_Attend = "Update Preset-Activity Attendance";
$i_ServiceMgmt_System_Menu_PresetActivity_Report = "Preset-Activity Report";
$i_ServiceMgmt_System_Menu_PresetActivity_Input = "Preset-Activity Settings";
$i_ServiceMgmt_System_Menu_Settings_Type = "Activity Type Settings";
$i_ServiceMgmt_System_Menu_Settings_Student = "Student Requirement Settings";

$i_ServiceMgmt_System_NotSet = "Not set";
$i_ServiceMgmt_System_Field_HoursRequired = "Working Hour";
$i_ServiceMgmt_System_Field_ActivityCategory = "Activity Category";
$i_ServiceMgmt_System_TargetClass = "Classes to be changed";
$i_ServiceMgmt_System_Warning_Numeric = "Please input numeric";

$i_ServiceMgmt_System_Hours_Type1_Name = "Internal";
$i_ServiceMgmt_System_Hours_Type2_Name = "External";
$i_ServiceMgmt_System_Hours_Type3_Name = "Others";

$i_ServiceMgmt_System_ActivityCategory = "Activity Category";

$i_PresetActivity_ActivityName = "Activity Name";
$i_PresetActivity_ActivityCode = "Activity Code";
$i_PresetActivity_ActivityCategory = "Activity Category";
$i_PresetActivity_ActivityNature = "Nature";
$i_PresetActivity_ActivityDate = "Activity Date";
$i_PresetActivity_ActivityTime = "Activity Time";
$i_PresetActivity_PIC = "Person-in-charge";
$i_PresetActivity_StudentSelect = "Students Selected";
$i_PresetActivity_HoursReq = "Hours Required";
$i_PresetActivity_HoursComplete = "Hours Completed";
$i_PresetActivity_HoursLeft = "Hours Left";
$i_PresetActivity_DateStart = "Start Date";
$i_PresetActivity_DateEnd = "End Date";

$i_ServiceMgmt_System_Class_Settings = "Class Settings";

$i_StarPicking_StarGrading_Name = "Star Grading Name";

$i_Extra_SocialNumber = "Social Dept Number";
$i_Extra_Manage_SocialNumber = "Manage $i_Extra_SocialNumber";

$i_Qualied_Late = '??';


################################################################################################
$iDiscipline['Management'] = "Management";
$iDiscipline['Management_Child_AwardInput'] = "Input Reward Records";
$iDiscipline['Management_Child_AwardImport'] = "Import Reward Records";
$iDiscipline['Management_Child_AwardApproval'] = "Approve Reward Records";
$iDiscipline['Management_Child_PunishmentInput'] = "Input Misconduct Records";
$iDiscipline['Management_Child_PunishmentImport'] = "Import Misconduct Records";
$iDiscipline['Management_Child_PunishmentApproval'] = "Approve Misconduct Records";
$iDiscipline['Management_Child_PunishmentCounselling'] = $i_Discipline_System_PunishCounselling;
$iDiscipline['Management_Child_AccumulativePunishment'] = "Input Accumulative Misconduct Records";
# added on 01 Sept 2008
$iDiscipline['Management_Child_AccumulativeReward'] = "Input Accumulative Reward Records";
# added on 31 Jan 2008
$iDiscipline['Management_Child_PendingNoticeRecord'] = 'Pending Notice Records';
# added on 8 Sept 2008
$iDiscipline['Management_Child_Personal'] = "Personal Records";
# added on 9 Sept 2008
$iDiscipline['Detention_Attendance_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber,$i_Discipline_System_PunishCouncelling_Detention_This_Minutes";
#####################

$iDiscipline['Reports'] = "Reports";
$iDiscipline['Reports_Child_PersonalReport'] = $i_Discipline_System_Report_Type_Personal;
$iDiscipline['Reports_Child_AwardPunishmentRecord'] = "Reward/Misconduct Report";
$iDiscipline['Reports_Child_DetailReport'] = $i_Discipline_System_Report_Type_Detail;
$iDiscipline['Reports_Child_ClassBasedReport'] = $i_Discipline_System_Report_Type_Class;

$iDiscipline['Statistics'] = "Statistics";
$iDiscipline['Statistics_Child_GeneralStats'] = $i_Discipline_System_Stat_Type_General;
$iDiscipline['Statistics_Child_DisciplineStats'] = $i_Discipline_System_Stat_Type_Discipline;
$iDiscipline['Statistics_Child_AwardStats'] = $i_Discipline_System_Stat_Type_Award;
$iDiscipline['Statistics_Child_DetentionStats'] = $i_Discipline_System_Stat_Type_Detention;
$iDiscipline['Statistics_Child_OtherStats'] = $i_Discipline_System_Stat_Type_Others;

$iDiscipline['Settings'] = "Settings";
$iDiscipline['Settings_Child_Settings'] = $i_Discipline_System_Settings;
$iDiscipline['Settings_Child_Calculation'] = $i_Discipline_System_Settings_Calculation;
$iDiscipline['Settings_Child_Accumulative'] = "Accumulative Misconduct";
# added on 01 Sept 2008
$iDiscipline['Settings_Child_Accumulative_Reward'] = "Accumulative Reward";

$button_addtoview = "Add to view";
$iDiscipline['select_students'] = "Select student(s)";
$iDiscipline['select_members'] = "Select member(s)";
$iDiscipline['class'] = "Class";
$iDiscipline['students'] = "Student";
$iDiscipline['teacher'] = "Teacher";
$iDiscipline['parent'] = "Parent";

$iDiscipline['compare_different_periods'] = "Comparison in Different Periods";
$iDiscipline['Accumulative_Setting_Period'] = "Period Setting";
$iDiscipline['Accumulative_NO_Setting_Period'] = "No period setting can be applied today";
$iDiscipline['Accumulative_Setting_Category'] = "Category & Item";

# eDiscipline v1.2
$eDiscipline['Overview'] = "Overview";
$eDiscipline['Management'] = "Management";
$eDiscipline['Award_and_Punishment'] = "Award & Punishment";
$eDiscipline['Good_Conduct_and_Misconduct'] = "Good Conduct & Misconduct";
$eDiscipline['Case_Record'] = "Case Record";
$eDiscipline['Conduct_Mark'] = "Conduct Mark";
$eDiscipline['Conduct_Grade'] = "Conduct Grade";
$eDiscipline['Detention'] = "Detention";
$eDiscipline['eNoticeTemplate'] = "eNotice Template";
$eDiscipline['Statistics'] = "Statistics";
$eDiscipline['Reports'] = "Reports";
$eDiscipline['Settings'] = "Settings";
$eDiscipline['Access_Right'] = "Access Rights";
$eDiscipline['General_Settings'] = "System Properties";
$eDiscipline['Detention_Settings'] = "Detention Settings";
$eDiscipline['Detention_Statistics'] = "Detention Statistics";
$eDiscipline['Award_and_Punishment_Statistics'] = "Award & Punishment Statistics";
$eDiscipline['Good_Conduct_and_Misconduct_Statistics'] = "Good Conduct & Misconduct Statistics";
$eDiscipline['Conduct_Mark_Statistics'] = "Conduct Mark Statistics";

$eDiscipline['Personal_Report_Goodconduct_Record']="Good Conduct Record";
$eDiscipline['Personal_Report_Misconduct_Record']="Misconduct Record";
$eDiscipline['Personal_Report_Good_Misconduct_Record']="Good Conduct/Misconduct Record";
$eDiscipline["Personal_Report_AwardPunishmentRecord"] = "Award/Punishment Record";

$eDiscipline['WarningLetter'] = "Warning";
$eDiscipline['DetentionNoticeLetter'] = "Detention";
$eDiscipline['AwardNoticeLetter'] = "Award";
$eDiscipline['PunishmentNoticeLetter'] = "Punishment";
$eDiscipline['ConductMarkNoticeLetter'] = "Conduct Mark";
$eDiscipline['GoodConductNoticeLetter'] = "Good Conduct";
$eDiscipline['MisConductNoticeLetter'] = "Misconduct";

$eDiscipline['AdjustmentDate'] = "Adjustment Date";

$eDiscipline_System_Admin_User_Setting = "Set Admin User";
$eDiscipline["AdminUser"] = "Admin User";
$eDiscipline["RecordList"] = "Record List";
$eDiscipline["NewRecord"] = "New Record";
$eDiscipline["EditRecord"] = "Edit Record";
$eDiscipline["WaiveRecord"] = "Waive Record";
$eDiscipline["NewLeafRecord"] = "New Leaf Record";
$eDiscipline["NewLeaf_Rehabilitation_Record"] = "Proposed records for \"New Leaf Scheme\"";
$eDiscipline["SelectStudents"] = "Select student(s)";
$eDiscipline["AddRecordToStudents"] = "Add record to student(s)";
$eDiscipline["SelectActions"] = "Select Action(s)";
$eDiscipline["FinishNotification"] = "Finish and send notification";
$eDiscipline["AwardRecord"] = "Award Record";
$eDiscipline["PunishmentRecord"] = "Punishment Record";
$eDiscipline["SelectSemester"] = "Select Semester";
$eDiscipline["AwardPunishmentRecord"] = "Award/Punishment Record";
$eDiscipline["AwardPunishmentRecord2"] = "Award/Punishment Record";
$eDiscipline["BasicInfo"] = "Basic Info";
$eDiscipline["DisciplineDetails"] = "Record Details";
$eDiscipline["DisciplineDetails2"] = "Record Details";
$eDiscipline["RecordItem"] = "Record Item";
$eDiscipline["Award_Punishment_RecordItem"] = "Award/Punishment Item";
$eDiscipline["SelectRecordCategory"] = "Select Good Conduct/Misconduct Category";
$eDiscipline["SelectRecordCategory2"] = "Select Award & Punishment Category";
$eDiscipline["SelectRecordCategory3"] = "Select Award/Punishment Category";
$eDiscipline["SelectRecordItem"] = "Select Award/Punishment Item";
$eDiscipline["SelectRecordItem2"] = "Select Award & Punishment Item";
$eDiscipline["MeritDemeritContent"] = "Merit/Demerit Record(s)";
$eDiscipline["TeacherStaffList"] = "Teacher/Staff List";
$eDiscipline["SelectTeacherStaff"] = "Select Teacher/Staff";
$eDiscipline["Type"] = "Type";
$eDiscipline["Award_Punishment_Type"] = "Award/Punishment Category";
$eDiscipline["Award_Punishment_Type2"] = "Award/Punishment Category";
$eDiscipline["Award_Punishment"] = "Award/Punishment";
$eDiscipline["Action"] = "Action";
$eDiscipline["Actions"] = "Action(s)";
$eDiscipline["SendNoticeWhenReleased"] = "Send notice via eNotice when the record is released";
$eDiscipline["Record"] = "Record";
$eDiscipline["AwardPunishmentQty"] = "Record";
$eDiscipline["Notification"] = "Notification";
$eDiscipline["EmailTo"] = "Email to";
$eDiscipline["DisciplineAdmin"] = "Discipline Admin";
$eDiscipline["DisciplineAdmin2"] = "eDiscipline Administrator";
$eDisicpline["GoodConduct_Misconduct_View"] = "View";

$eDiscipline["Status"] = "Status";
$eDiscipline["ApproveRelease"] = "Approve and Release";
$eDiscipline["RecordDetails"] = "Record Details";
$eDiscipline["PreviewNotice"] = "Preview Notice";
$eDiscipline["ApprovedBy"] = "Approved By ";
$eDiscipline["ApprovedBy2"] = " ";
$eDiscipline["ApprovedBy3"] = " ";
$eDiscipline["ReleasedBy"] = "Released By ";
$eDiscipline["ReleasedBy2"] = " ";
$eDiscipline["ReleasedBy3"] = " ";
$eDiscipline["UnReleasedBy"] = "Unreleased By";
$eDiscipline["RejectedBy"] = "Rejected By ";
$eDiscipline["RejectedBy2"] = " ";
$eDiscipline["RejectedBy3"] = " ";
$eDiscipline["WaivedBy"] = "Waived By ";
$eDiscipline["WaivedBy2"] = " ";
$eDiscipline["WaivedBy3"] = " ";
$eDiscipline["LockedBy"] = "Locked By ";
$eDiscipline["LockedBy2"] = " ";
$eDiscipline["LockedBy3"] = " ";
$eDiscipline["RecordRejectAlert"] = "Reject this record will remove the detention record(s) and suspend the eNotice. \\nAre you sure you want to reject this record?";
$eDiscipline["RecordRemoveAlert"] = "Remove this record will remove the detention record(s) and remove the eNotice. \\nAre you sure you want to remove this record?";
$eDiscipline["SelectedRecord"] = "Selected Record";
$eDiscipline["WaiveReason"] = "Waive Reason";
$eDiscipline["EditRecordInfo"] = "Edit This Record";
$eDiscipline["EditActions"] = "Edit Action(s)";
$eDiscipline["GlobalSettingsToAllStudent"] = "Global Settings Common to All Student(s)";
$eDiscipline["ApplyToAll"] = "Apply To All";
$eDiscipline["AddDetention"] = "Add Detention";
$eDiscipline["EditDetention"] = "Edit Detention";
$eDiscipline["NoSemesterSettings"] = "No semester settings.\\nPlease contact System Admin to set the semester information.";
$eDiscipline["HISTORY"] = "History";
$eDiscipline["CorrespondingConductRecords"] = "Corresponding conduct records";
$eDiscipline["DisciplineName"] = "Discipline";	# for eNotice usage
$eDiscipline["SendNotice"] = "Send Notice";
$eDiscipline["WaivedReason"] = "Waived Reason";
$eDiscipline["SemesterSettingError"] = "Semester Setting Error.";
$eDiscipline["AddStudents"] = "Add Student(s)";
$eDiscipline["FinishCaseConfirm"] = "Are you sure this case has been processed?";
$eDiscipline["ReleaseCaseConfirm"] = "All the related Award&Punishment records will release automatically.\\n\\nAre you sure want to release this case?";
$eDiscipline["UnReleaseCaseConfirm"] = "All the related Award&Punishment records will unrelease automatically.\\n\\nAre you sure want to unrelease this case?";
$eDiscipline["EventDate"] = $i_RecordDate;
$eDiscipline["SelectTemplate"] = "Select Template";
$i_Discipline_System_Discipline_Case_Record_CaseNumber = "Case Number";
$eDiscipline["SelectConductCategory"] = "Select Conduct Category";
$eDiscipline["SelectConductItem"] = "Select Conduct Item";
$eDiscipline["RecordCategoryItem"] = "Record Category/Item";
$eDiscipline["Category_Item"] = "Category/Item";
$eDiscipline["Category_Item2"] = "Category/Item";

# 20090401 yatwoon
$eDiscipline["MissingSemesterPromptMsg_WithRight"] = "System administrator have previously changed Semester Mode from \"Auto Update\" to \"Manual Update\", corresponding semesters of Accumulation Periods cannot be determined. Users will need to re-assign a semester to each of the accumulation periods. The Management>Good Conduct & Misconduct page will not be accessible until ALL accumulation periods are assigned their corresponding semesters.";
$eDiscipline["MissingSemesterPromptMsg_WithoutRight"] = "Due to change of Semester Mode from \"Auto Update\" to \"Manual Update\", corresponding semesters of Accumulation Periods cannot be determined. You will need to re-assign a semester to each of the accumulation periods. You will not be able to access the Management>Good Conduct & Misconduct page until ALL Accumulation Periods are assigned their corresponding semesters. Please contact your system administrator for more information on Semester Mode change.";

# 20090513 Henry
$eDiscipline["PPC_Input_Award_Record"] = "Input Award Record";
$eDiscipline["PPC_Input_Punishment_Record"] = "Input Punishment Record";
$eDiscipline["PPC_Input_Goodconduct_Record"] = "Input Good Conduct Record";
$eDiscipline["PPC_Input_Misconduct_Record"] = "Input Misconduct Record";


### added on 21 Nov 2008 by Ronald ###
/*
$eDiscipline_Settings_GoodConduct = "Good Conduct";
$eDiscipline_Settings_Misconduct = "Misconduct";
$eDiscipline_Settings_DefaultPeriodSetting = "Default Period Setting";
$eDiscipline_Settings_CategorySetting = "Category Setting";
$eDiscipline_Settings_DefaultPeriodOverlapWarning = "Period overlapped. Please input again.";
$eDiscipline_Settings_JSWarning_PeriodStartDateEmpty = "Please input a Start Date.";
$eDiscipline_Settings_JSWarning_PeriodEndDateEmpty = "Please input a End Date.";
$eDiscipline_Settings_JSWarning_InvalidStartDate = "Start Date is not vaild.";
$eDiscipline_Settings_JSWarning_InvalidEndDate = "End Date is not vaild.";
$eDiscipline_Settings_JSWarning_StartDateLargerThanEndDate = "Start date cannot be larger than End Date.";
$eDiscipline_Settings_DefaultPeriod_From = "From";
$eDiscipline_Settings_DefaultPeriod_To = "To";
$eDiscipline_Settings_GoodConductMisconuct_PeriodRange = "Period Range";
$eDiscipline_Settings_GoodConductMisconuct_NAV_CategoryList = "Category List";
$eDiscipline_Settings_GoodConductMisconuct_NAV_EditCategory = "Edit Category";
$eDiscipline_Settings_GoodConductMisconuct_NAV_NewCategory = "New Category";
$eDiscipline_Settings_GoodConductMisconuct_NAV_NewCategoryItem = "New Category Item";
$eDiscipline_Settings_GoodConductMisconuct_NAV_EditCategoryItem = "Edit Category Item";
$eDiscipline_Settings_GoodConductMisconuct_NAV_NewPeriod = "New Period";
$eDiscipline_Settings_GoodConductMisconuct_NAV_EditPeriod = "Edit Period";
$eDiscipline_Settings_GoodConductMisconuct_Title_CategoryName1 = "Name";
$eDiscipline_Settings_GoodConductMisconuct_Title_CategoryName2 = "Category Name";
$eDiscipline_Settings_GoodConductMisconuct_Title_NumOfItem = "No. Of Item";
$eDiscipline_Settings_GoodConductMisconuct_Title_PeriodCondition = "Period Condition";
$eDiscipline_Settings_GoodConductMisconuct_Title_Status = "Status";
$eDiscipline_Settings_GoodConductMisconuct_Title_Period = "Period";
$eDiscipline_Settings_GoodConductMisconuct_Title_CategoryItemName1 = "Name";
$eDiscipline_Settings_GoodConductMisconuct_Title_CategoryItemName2 = "Item Name";
$eDiscipline_Settings_GoodConductMisconuct_Title_CategoryItemStatus = "Status";
$eDiscipline_Settings_GoodConductMisconuct_Title_CategoryItemReorder = "Re-order";
$eDiscipline_Settings_GoodConductMisconuct_Title_CategoryItemStatus_InUse = "In Use";
$eDiscipline_Settings_GoodConductMisconuct_Title_CategoryItemStatus_NotInUse = "Not in Use";
$eDiscipline_Settings_GoodConductMisconuct_Category_Status_All = "All Status";
$eDiscipline_Settings_GoodConductMisconuct_Category_Status_Published = "Published";
$eDiscipline_Settings_GoodConductMisconuct_Category_Status_Drafted = "Drafted";
$eDiscipline_Settings_GoodConductMisconuct_Category_Period_Default = "Use Default";
$eDiscipline_Settings_GoodConductMisconuct_Category_Period_Specify = "Catgory's Specify";
$eDiscipline_Settings_GoodConductMisconuct_JSWarning_CategoryItemNameEmpty = "Please input an Item Name";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_Step1 = "Input period range";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_Step2 = "Select calculation schema";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_Step3 = "Set calculation details";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_AccumulatedCount_Title = "Accumulated Count";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_AddMeritRecord_Title = "Add merit Record(s)";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_AddDeeritRecord_Title = "Add demerit Record(s)";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_ItemName_Title = "Item Name";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_DetentionSession_Title = "Detention (sessions)";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_Reminder_Title = "Reminder";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_CalScheme = "Scheme";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_CalScheme_Static = "Static Increment";
$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_CalScheme_Floating = "Floating Increment";
$eDiscipline_Settings_GoodConductMisconuct_CalculationDetails = "- Calculation Details -";
$eDiscipline_Settings_GoodConductMisconuct_FutherCalculationInfo = "- Futher Calculation Info -";
$eDiscipline_Settings_GoodConductMisconuct_1stCalculationInfo = "- 1st Calculation Info -";
$eDiscipline_Settings_GoodConductMisconuct_CalculationInfo = "- Calculation Info -";
$eDiscipline_Settings_GoodConductMisconuct_ConductMarkIncrement = "Conduct Mark (Increment)";
$eDiscipline_Settings_GoodConductMisconuct_StudyMarkIncrement = "Study Mark (Increment)";
$eDiscipline_Settings_GoodConductMisconuct_AddMeritRecord = "Add Merit Record";
*/
### Settings - Good Conduct & Misconduct
$eDiscipline['GoodConduct_and_Misconduct'] = "Good Conduct and Misconduct";
$eDiscipline['Setting_GoodConduct'] = "Good Conduct";
$eDiscipline['Setting_Misconduct'] = "Misconduct";
$eDiscipline['Setting_Cancel_PPC'] = "Cancel PPC Status";
$eDiscipline['Setting_NewLeaf'] = "New Leaf Scheme";
$eDiscipline['Waive_By_New_Leaf_Scheme'] = "Waive (New Leaf Scheme)";
$eDiscipline['Setting_NewLeaf_Remark'] = "Waived by New Leaf.";
$eDiscipline['Setting_DefaultPeriodSettings'] = "Accumulation Period";
$eDiscipline['Setting_CategorySettings'] = "Category & Item";
$eDiscipline['Setting_HeldEach'] = "Held each";
$eDiscipline['Setting_Rehabilitation_Period'] = "Period";
$eDiscipline['Setting_Days'] = "Day(s)";
$eDiscipline['Setting_WaiveFirst'] = "Max. Waive";
$eDiscipline['Setting_Category'] = "Category";
$eDiscipline['Setting_Period'] = "Conversion Period";
$eDiscipline['Setting_Status'] = "Status";
$eDiscipline['Setting_Status_All'] = "All Status";
$eDiscipline['Setting_Status_Using'] = "In Use";
$eDiscipline['Setting_Status_NonUsing'] = "Not In Use";
$eDiscipline['Setting_CategoryList'] = "Category List";
$eDiscipline['Setting_Status_Published'] = "In Use";
$eDiscipline['Setting_Status_Drafted'] = "Not In Use";
$eDiscipline['Setting_ItemName'] = "Item Name";
$eDiscipline['Setting_Reorder'] = "Re-order";
$eDiscipline['Setting_NAV_CategoryList'] = "Category List";
$eDiscipline['Setting_NAV_PeriodList'] = "Period List";
$eDiscipline['Setting_NAV_NewCategoryItem'] = "New Category Item";
$eDiscipline['Setting_NAV_EditCategoryItem'] = "Edit Category Item";
$eDiscipline['Setting_NAV_NewCategory'] = "New Category";
$eDiscipline['Setting_NAV_EditCategory'] = "Edit Category";
$eDiscipline['Setting_NAV_NewPeriod'] = "New Period";
$eDiscipline['Setting_NAV_EditPeriod'] = "Edit Period";
$eDiscipline['Setting_CategoryName'] = "Category Name";
$eDiscipline['Setting_CalculationSchema'] = "Scheme";
$eDiscipline['Setting_CalculationSchema_Static'] = "Static Increment";
$eDiscipline['Setting_CalculationSchema_Static_Abb'] = "Static";
$eDiscipline['Setting_CalculationSchema_Floating'] = "Floating Increment";
$eDiscipline['Setting_CalculationSchema_Floating_Abb'] = "Floating";
$eDiscipline['Setting_CalculationSchema_Undefined'] = "Undefined";
$eDiscipline['Setting_AccumulatedTimes'] = "Accumulated Count";
$eDiscipline['Setting_DetentionTimes'] = "Detention Times";
$eDiscipline['Setting_SendNotice'] = "Send Reminder";
$eDiscipline['Setting_NewPeriod_Step1'] = "Input period range";
$eDiscipline['Setting_NewPeriod_Step2'] = "Choose conversion scheme";
$eDiscipline['Setting_NewPeriod_Step3'] = "Set up conversion steps";
$eDiscipline['Setting_NewPeriod_Step4'] = "Calculation Scheme Summary";
$eDiscipline['Setting_CalculationDetails'] = "- Initial Step -";
$eDiscipline['Setting_FirstCalculationSchema']  = "- 1st Calculation Info -";
$eDiscipline['Setting_FutherCalculationInfo'] = "- Additional Steps -";
$eDiscipline['Setting_CalculationInfo'] = "- Calculation Info -";
$eDiscipline['Setting_FollowUpAction'] = "- Follow Up Action -";
$eDiscipline['Setting_Reminder'] = "- Reminder -";
$eDiscipline['Setting_Condition'] = "Condition";
$eDiscipline['Setting_Duplicate'] = "Duplicate";
$eDiscipline['Setting_DefaultFollowUpAction'] = "Default Follow Up Action";
$eDiscipline['Setting_Detention'] = "Detention";
$eDiscipline['Setting_DetentionSession'] = "Session";
$eDiscipline['Setting_Semester'] = "Semester";
$eDiscipline['Setting_BasicInformation'] = "Basic Information";
$eDiscipline['Setting_NumOfItem'] = "No. of Items";
$eDiscipline['Setting_ConductMark_Increase']  = "Conduct Mark (Increment)";
$eDiscipline['Setting_ConductMark_Decrease']  = "Conduct Mark (Decrement)";
$eDiscipline['Setting_StudyMark_Increase']  = "Study Mark (Increment)";
$eDiscipline['Setting_StudyMark_Decrease']  = "Study Mark (Decrement)";
$eDiscipline['Setting_Period_Default'] = "Default";
$eDiscipline['Setting_Period_Default_Setting'] = "Use Default Accumulation Period";
$eDiscipline['Setting_Period_Use_Default'] = "Use Default Periods";
$eDiscipline['Setting_Period_Use_Default_Periods'] = "Using Default Period(s)";
$eDiscipline['Setting_Period_Specify'] = "Specify";
$eDiscipline['Setting_Period_Specify_Setting'] = "User Category's Accumulation Period";
$eDiscipline['Setting_Period_Use_Specify'] = "Use Category\'s Periods";
$eDiscipline['Setting_Period_Use_Specify_Periods'] = "Using Category\'s Period(s)";
$eDiscipline['Accumulative_Period_InUse_Warning'] = "Cannot Remove Record(s). Some of the selected periods are already in use.";
$eDiscipline['Accumulative_Period_InUse_Warning_Update'] = "Cannot Update Record(s). Some of the selected periods are already in use.";
$eDiscipline['Setting_Warning_InputCategoryName'] = "Please input a Category Name";
$eDiscipline['Setting_Warning_PleaseSelectAtLeaseOnePunlishment'] = "Please set at least one punishment method.";
$eDiscipline['Setting_CalculationScheme'] = "Calculation Scheme";
$eDiscipline['Setting_ConversionScheme'] = "Conversion Scheme";
$eDiscipline['Setting_AddMeritRecord'] = "Add Merit Record";
$eDiscipline['Setting_AddDemeritRecord'] = "Add Demerit Record";
$eDiscipline['Setting_DefaultPeriodOverlapWarning'] = "Overlapping periods not allowed.";
$eDiscipline['Setting_JSWarning_PeriodStartDateEmpty'] = "Please input a Start Date.";
$eDiscipline['Setting_JSWarning_PeriodEndDateEmpty'] = "Please input a End Date.";
$eDiscipline['Setting_JSWarning_InvalidStartDate'] = "Start Date is not vaild.";
$eDiscipline['Setting_JSWarning_InvalidEndDate'] = "End Date is not vaild.";
$eDiscipline['Setting_JSWarning_StartDateLargerThanEndDate'] = "Start date cannot be later than end date.";
$eDiscipline['Setting_JSWarning_RemoveCategory'] = "Cannot remove this category.";
$eDiscipline['Setting_JSWarning_CategoryItemNameEmpty'] = "Please input an Item Name";
$eDiscipline['Setting_JSWarning_CategoryItemCodeEmpty'] = "Please input an Item Code";
$eDiscipline['Content'] = "Content";
$eDiscipline['Subject'] = "Subject";
$eDiscipline['MeritDemerit'] = "Merit/Demerit";
$eDiscipline['AwardPunishmentRecord'] = "Record";

$i_Discipline['Warning_Integer_Input'] = " must be a positive integer or zero.";
$i_Discipline['Warning_Input_WaiveFirst'] = "Please input value in Waive Day when the corresponding Waive First has value.";
$i_Discipline['Warning_Need_Setting'] = "Please set ".$eDiscipline['Setting_Period']." first.";
$i_Discipline['Waive_Day_Header_Detail'] = "幾多日之內冇再犯可Waive* (days)";
$i_Discipline['Waive_First_Header_Detail']  = "Waive first* (times)";


### added on 13 Jan 2009 by Ivan ###
$eDiscipline["Redeem"]["Step1"] = "Select Punishment(s)";
$eDiscipline["Redeem"]["Step2"] = "Confirm Record(s)";
$eDiscipline["RedeemRecord"] = "Record Redemption";
$eDiscipline["Redemption"] = "Redemption";
$eDiscipline["jsWarning"]["MeritOnly"] = "Please select an award record for redemption.";
$eDiscipline["jsWarning"]["OneStudentOnly"] = "Please select records of one student only";
$eDiscipline["jsWarning"]["ApprovedRecordOnly"] = "Only approved award(s) can be redeemed.";
$eDiscipline["SelectPunishmentToRedeem"] = "Please select punishment(s) to redeem.";
$eDiscipline["AwardRedemption"] = "Award(s) for redemption";
$eDiscipline["PunishmentRedemption"] = "Punishment(s) to redeem";
$eDiscipline["CorrespondingWaivedRecords"] = "- Corresponding waived records -";
$eDiscipline["CancelRedemption"] = "Cancel Redemption";
$eDiscipline["Processing"] = "Processing...";
$eDiscipline["CancelRedemptionSuccess"] = "Cancel Redemption Success";
$eDiscipline["CancelRedemptionFailed"] = "Cancel Redemption Failed";
### added on 2 Mar 2009 by Ivan ###
$eDiscipline["Websams_Transition"] = "Data Transfer (to WebSAMS)";
$eDiscipline["Websams_Transition_Step1"] = "Upload WebSAMS CSV";
$eDiscipline["Websams_Transition_Step2"] = "Choose record type to transfer";
$eDiscipline["Websams_Transition_Step3"] = "Transfer Record(s)";
$eDiscipline["Websams_Transition_Step1_url_example"] = "Example: http://websams.xxx.edu.hk/";
$eDiscipline["File"] = "File";
$eDiscipline["WebSAMS_URL"] = "WebSAMS URL";
### added on 3 Mar 2009 by Ivan ###
$eDiscipline["Websams_Transition_CSV_File_Remarks"] = "CSV file containing STUID and REGNO from WebSAMS";
$eDiscipline["jsWarning"]["SelectFileFirst"] = "CSV file not yet selected.";
$eDiscipline["Websams_Transition_Step1_Instruction1"] = "You can only transfer records to WebSAMS for ONCE. Please make sure all award/punishment records are correct and up to date.";
$eDiscipline["Websams_Transition_Step1_Instruction2"] = "To export the required CSV file, please use this query:";
$eDiscipline["Websams_Transition_Step1_Instruction3"] = "<i>SELECT STUID, REGNO FROM TB_STU_STUDENT</i>";
$eDiscipline["Websams_Transition_Step1_Instruction4"] = "To transfer award/punishment records to WebSAMS, please log in WebSAMS and export a CSV file with STUID and REGNO before visiting this page. The transfer process can only be started within FIVE minutes after the login and export actions.";
$eDiscipline["WebSAMS_Warning_CsvNoData"] = "The CSV file has no data";
$eDiscipline["WebSAMS_Warning_wrongExtension"] = "<font color=\"red\">Wrong file extension</font>";
$eDiscipline["WebSAMS_Warning_uploadFailed"] = "<font color=\"red\">Upload Failed</font>";
$eDiscipline["WebSAMS_Warning_wrongHeader"] = "<font color=\"red\">Wrong Header</font>";
$eDiscipline["Instruction"] = "Instruction";
$eDiscipline["NumOfWebSAMSRegNoInCSV"] = "Number of REGNOs uploaded";
$eDiscipline["ReportCardPrintIndicate"] = "Show transferred records in WebSAMS report card";
$eDiscipline["Caution"] = "Caution";
$eDiscipline["Websams_Transition_Step2_Instruction1"] = "Only award/punishment records which are APPROVED will be transferred.";
$eDiscipline["TargetSchoolYear"] = "Target School Year";
$eDiscipline["CurrentSchoolYear"] = "Current School Year";
$eDiscipline["RecordsInDiscipline"] = "Record Summary";
$eDiscipline["RecordType"] = "Record Summary";
$eDiscipline["TransferredRecords"] = "Records Transferred";
$eDiscipline["NotYetTransferredRecords"] = "Records Not Transferred";
$eDiscipline["LastTransferringDate"] = "Last Transferred";
$eDiscipline["Websams_Transition_Step3_Instruction1"] = "Do NOT close any browser or pop-up window or visit other pages during the data transfer.";
$eDiscipline["Websams_Transition_Step3_Instruction2"] = "Press the \"Start Transfer\" in the pop-up window to start transferring records to WebSAMS.";
$eDiscipline["DataTransferComplete"] = "Data Transfer Completed.";
$eDiscipline["RecordsTransferred1"] = "";
$eDiscipline["RecordsTransferred2"] = "Record(s) Transferred";
$eDiscipline["jsWarning"]["InvalidURL"] = "Invalid URL.";
$eDiscipline["jsWarning"]["NothingSelected"] = "You have not selected any record type.";
$eDiscipline["jsWarning"]["AllAwardsTransferrred"] = "All award records has been transferred already.";
$eDiscipline["jsWarning"]["AllPunishmentsTransferrred"] = "All punishment records has been transferred already.";
$eDiscipline["Transferring"] = "Transferring...";
$eDiscipline["CheckConnection"] = "Check connection";
$eDiscipline["Connecting"] = "Connecting";
$eDiscipline["Successful"] = "Successful";
$eDiscipline["Failed"] = "Failed";
$eDiscipline["StartDataTransfer"] = "Start Transfer";
$eDiscipline["Websams_Transition_Step3_PopUpWarning"] = "<b>Caution:</b><br />
Please <b><font color=\"red\">DO NOT</font></b> close this window during data transfer.<br />
This window will be closed automatically upon the completion of data transfer.";
$eDiscipline["Websams_Transition_Step1_Instruction5"] = "Only WebSAMS 1.5.1 is supported by data transfer at this moment.";
$eDiscipline["Re-generate_CM_Warning"] = "Since grading scheme is modified, please re-compute conduct grade at <b>Management > Conduct mark</b>.";
# added on 30-07-2009 by Ivan - to WebSAMS
$eDiscipline["StudentMappingCSVFile"] = "Student Mapping CSV File";
$eDiscipline["TeacherMappingCSVFile"] = "Teacher Mapping CSV File";
$eDiscipline["Websams_Transition_Teacher_CSV_File_Remarks"] = "CSV file containing ID from WebSAMS & eClass";
$eDiscipline["Download_Teacher_CSV"] = "Click here to download the Teacher ID from eClass";


# without SubScore
//$iDiscipline['Award_Import_FileDescription_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_SettingsSemester, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore, $i_Discipline_System_Discipline_Case_Record_PIC, $i_email_subject, $i_Discipline_System_Add_Merit, $i_Discipline_System_Discipline_Category, ".$iDiscipline['MeritType'].", $i_UserRemark";
/*$iDiscipline['Award_Import_FileDescription_withoutSubScore'] = "Example:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>$i_UserClassName</td><td>$i_ClassNumber</td><td>$i_RecordDate</td><td>$i_SettingsSemester</td><td>$i_Discipline_System_ItemCode</td><td>$i_Discipline_System_ConductScore</td><td>$i_Discipline_System_Discipline_Case_Record_PIC</td><td>$i_email_subject</td><td>$i_Discipline_System_Add_Merit</td><td>$i_Discipline_System_Discipline_Category</td><td>".$iDiscipline['MeritType']."</td><td>$i_UserRemark</td></tr>
<tr><td>1A</td><td>12</td><td>2009-03-20</td><td>Fall semester</td><td>LATE002F</td><td>4</td><td>Teacher01</td><td>English</td><td>1</td><td>Punishment</td><td>-3</td><td>Remark</td></tr>
</table>";*/

$iDiscipline['Demerit_Import_FileDescription_withSubject_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_Subject_name, $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withoutSubject_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_UserRemark";

$iDiscipline['Award_Import_Remarks'] = "If you are using Excel to edit CSV files, please remember to change all cells to text format. Select the cells you want to format, then right-click and choose <b>Format Cells</b>, then click on the <b>Number</b> tab in the <b>Format Cells</b> dialog box. Choose <b>Text</b>.";
$iDiscipline['ImportOtherRecords'] = "Import other records";
$iDiscipline['DemeritCount'] = "Demerit Count";

$iDiscipline['TotalTime'] = "Total Time(s)";
$iDiscipline['PastRecord'] = "Past Record(s)";
$iDiscipline['TotalRecord'] = "Total Record(s)";

### added on 9 Feb 2009 by Kelvin ###
$eDiscipline["jsWarning"]["EnableEnotice"] = "This function has been disabled by your system administrator.";

### added on 11 Feb 2009 by Kelvin ###
$eDiscipline["EnoticeDisabledMsg"] = "eNotice is either disabled OR you are not granted the right to use ";

$eDiscipline["Every"] = "Every";
$eDiscipline["InstanceOf"] = "instance(s) of";
$eDiscipline["WillBeCounted"] = " will be counted as";

$eDiscipline["SelectCategory"] = "-- Select Category --";
$eDiscipline["SelectItem"] = "-- Select Item --";

$eDiscipline['RecordWillChangeToWaiting_alert'] = "This record will auto change to 'Waiting for approval' status.  Continue?";

$eDiscipline['SettingName'] = "System Properties";
$eDiscipline['Value'] = "Value";
$eDiscipline['SemesterRatio_MAX'] = "Max. ratio for semester";
$eDiscipline['ConductMarkIncrement_MAX'] = "Max. Conduct Mark Increment/Decrement";
$eDiscipline['AwardPunish_MAX'] = "Max. number of awards/punishments to receive";
$eDiscipline['AccumulativeTimes_MAX'] = "Max. number of accumulated good conduct/misconduct records";
$eDiscipline['ApprovalLevel_MAX'] = "Max. need-to-approve award/punishment quantity";
$eDiscipline['UseSubject'] = "Need to specify related subjects for award and punishment records";
$eDiscipline['AP_Conversion_MAX'] = "Max. number of awards/punishments to be converted";
$eDiscipline['Activate_NewLeaf'] = "Enable \"New Leaf Scheme\" for Misconduct Records";
$eDiscipline['Detention_Sat'] = "Detention session include Saturday.";
$eDiscipline['Hidden_ConductMark'] = "Hidden Conduct Mark function";

$iPowerVoice['bit_rate'] = "Bit Rate (Affects sound quality)";
$iPowerVoice['bit_rate_unit'] = "kbps";
$iPowerVoice['sampling_rate'] = "Sampling Rate (Affects sound clarity)";
$iPowerVoice['sampling_rate_unit'] = "Hz";
$iPowerVoice['sound_length'] = "Maximum Length";
$iPowerVoice['sound_length_unit'] = "mins";
$iPowerVoice['sound_unlimited'] = "Unlimited";
$iPowerVoice['power_voice_setting'] = "PowerVoice Settings";
$iPowerVoice['sound_recording'] = "Sound Recording";
$iPowerVoice['powervoice'] = "PowerVoice";

$i_frontpage_campusmail_icon_important_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_important_title.gif'  align='absmiddle'  border='0'  >";
$i_frontpage_campusmail_icon_important2_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_important.gif' align='absmiddle' border='0' alt='{$i_frontpage_campusmail_important}' >";
$i_frontpage_campusmail_icon_notification_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_notification_title.gif' align='absmiddle'  border='0' >";
$i_frontpage_campusmail_icon_notification2_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_notification.gif' align='absmiddle' border='0' alt='{$i_frontpage_campusmail_notification}' >";
$i_frontpage_campusmail_icon_status_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_unread_mail_title.gif' align='absmiddle' border='0' >";
$i_CampusMail_New_Icon_ReadMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_open_mail.gif' align='absmiddle' border='0' alt='{$i_frontpage_campusmail_read}' >";
$i_CampusMail_New_Icon_RepliedMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_reply_mail.gif' align='absmiddle' border='0' alt='{$i_CampusMail_New_Reply}' >";
$i_CampusMail_New_Icon_NewMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_unread_mail.gif' align='absmiddle' border='0' alt='{$i_frontpage_campusmail_unread}' >";
$i_frontpage_campusmail_icon_attachment_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_attachment.gif' border='0' align='absmiddle' alt='{$i_frontpage_campusmail_attachment}' >";
$i_frontpage_campusmail_icon_restore_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash_restore.gif' border='0' align='absmiddle' alt='{$button_restore}' >";
$i_frontpage_campusmail_icon_trash_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif' border='0' align='absmiddle' alt='{$button_remove}' >";
$i_frontpage_campusmail_icon_empty_trash_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash_clear.gif' border='0' align='absmiddle' alt='{$button_emptytrash}' >";

$i_frontpage_campusmail_ago['days'] = " days ago";
$i_frontpage_campusmail_ago['hours'] = " hours ago";
$i_frontpage_campusmail_ago['minutes'] = " minutes ago";
$i_frontpage_campusmail_ago['seconds'] = " seconds ago";

$i_frontpage_campusmail_after['days'] = " days after";
$i_frontpage_campusmail_after['hours'] = " hours after";
$i_frontpage_campusmail_after['minutes'] = " minutes after";
$i_frontpage_campusmail_after['seconds'] = " seconds after";


$ip20_lang_eng = "ENG";
$ip20_lang_b5 = "繁";
$ip20_lang_gb = "&#x7B80;";
$ip20_powerby = "<span class=\"footertext\">Powered by</span> <a href=\"http://www.eclass.com.hk\" target=\"_blank\"><img src=\"$image_path/2007a/logo_eclass_footer.gif\" border=\"0\" align=\"absmiddle\"></a>";
$ip20_welcome = "Welcome";
$ip20_event_today = "Today's Event";
$ip20_event_month = "Month's Event";
$ip20_esurvey = "eSurvey";
$ip20_enotice = "eNotice";
$ip20_reminder = "Reminder";
$ip20_what_is_new = "What's New";
$ip20_loading = "Loading";
$ip20_public = "Public";
$ip20_my_group = "My Group";


################################################################################################
$i_ReportCard = "Report Card";
$i_ReportCard_System = "eReportCard";
$i_ReportCard_System_Setting = "Report Card System Settings";
$i_ReportCard_System_Admin_User_Setting = "Set Admin User";
$i_ReportCard_All_Year = "All Year";
$eReportCard["AdminUser"] = "Admin User";
$eReportCard["TeachingAppointmentSettings"] = "Teaching Appointment Settings";
$eReportCard['AssignSubjectClassWarning'] = "Please assign class(es) for selected subject!";
$eReportCard['Code'] = "Subject Code";
$eReportCard['SubjectName'] = "Subject Name";
$i_ReportCard_Teaching_ImportInstruction = "<b><u>File Description:</u></b><br>
1st Column : Teacher's Login Name (e.g. tmchan ).<br>
2nd Column : Teaching Subject Code (e.g. ENG).<br>
3rd Column : Teaching class(es) (e.g. 1A;1B. Class names are sepeprated by ';').<br>
Class names and subject code are required to be exactly matched (case-sensitive)<br>
<br>
<b>Note that current settings will be replaced.</b><br>";

#####
$i_StudentGuardian['MenuInfo'] = "Guardian Information";
$i_StudentGuardian_all_students="All Students";
$i_StudentGuardian_MainContent="Main Guardian";
$i_StudentGuardian_SMSPhone="SMS Phone";
$i_StudentGuardian_Phone="Phone No.";
$i_StudentGuardian_EMPhone="Emergency Phone No.";
$i_StudentGuardian_GuardianName="Guardian Name";
$i_StudentGuardian_SMS="SMS";
$i_StudentGuardian_warning_enter_chinese_english_name="Please enter the English name or Chinese name of the Guardian";
$i_StudentGuardian_warning_enter_phone_emphone ="Please enter $i_StudentGuardian_Phone or $i_StudentGuardian_EMPhone";
$i_StudentGuardian_warning_Delete_Main_Guardian="You have choosed to delete the main guardian, please set the main guardian.";
$i_StudentGuardian_warning_Delete_EmergencyContact="You have choosed to delete the emergency contact guardian, please set the emergency contact guardian.";
$i_StudentGuardian_warning_Delete_SMS="You have choosed to delete the SMS contact no, please set the choose other SMS contact no.";
$i_StudentGuardian_Additional_Searching_Criteria="Additional Searching Criteria";
$i_StudentGuardian_warning_PleaseSelectClass="Please select at least one class";
$i_StudentGuardian_Back_To_Search_Result="Back to Search Result";
$i_StudentGuardian_Searching_Area = "Searching Area";



$ec_guardian['01'] = "Father";
$ec_guardian['02'] = "Mother";
$ec_guardian['03'] = "Grandfather";
$ec_guardian['04'] = "Grandmother";
$ec_guardian['05'] = "Brother";
$ec_guardian['06'] = "Sister";
$ec_guardian['07'] = "Relative";
$ec_guardian['08'] = "Other";
$ec_iPortfolio['relation'] = "Relationship";

$ec_iPortfolio['guardian_import_remind'] = " Attention: <br> - The first column is RegNo. To ensure the number is complete, # has to be added to RegNo (e.g. \"#0025136\"). <Br> - The second column is the guardian english name <Br> - The third column is the guardian chinese name <Br> - The fourth column is the guardian contact number <Br> - The fifth column is the guardian emergency contact number <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> The sixth column is the relationship between the guardian and the student <Br> - The seventh column is to set the guardian to be the main guardian <Br> - The eighth column is to set the guardian to be the emergency contact guardian <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - Father, 02 - Mother, 03 - Grandfather, 04 - Grandmother, 05 - Brother, 06 - Sister, 07 - Relative, 08 - Other) <br> - <font color=red>The new imported guardian records will overwrite the existing records.</font><Br>";

$ec_yyc['yyc_export_usage'] = "Export for offline system";


### new smartcard
$StatusRecord = "Status Record";
$MonthlyRecord = "Monthly Record";
$DisplayAllPhoto = "Display all photo";
$HiddenAllPhoto = "Hidden all photo";
$OverallRecord = "Overall Record";


## PPC
$i_PPC_SmartCard_Take_Attendance_In_School_TimeLocation="Time / Location";
$i_PPC_SmartCard_Take_Attendance_Show_LateAbsentOuting="Show Late / $i_StudentAttendance_Status_PreAbsent Only";
$i_PPC_SmartCard_Take_Attendnace_Show_AllStatus ="Show All Status";
$i_ppc_home_title ="Mobile eClass";
$i_ppc_menu_home ="Home";
### eOffice
$eOffice = "eOffice";

### eFilling
$eFilling = "eFilling";

### iAccount
$iAccount = "iAccount";
$iAccount_Account = "Account";
$AllRecords = "All Records";

###############################################################################################################
##### eEnrolment Begin #######################################################################################
###############################################################################################################

$eEnrollmentMenu['act_attendance_mgt'] = "Activity Attendance";
$eEnrollmentMenu['act_date_time'] = "Activity Date";
$eEnrollmentMenu['act_event_further_mgt'] = "Activity Advanced Management";
$eEnrollmentMenu['act_event_status_stat'] = "Activity Enrolment";
$eEnrollmentMenu['act_event_status_stat_overall'] = "Club Enrolment Statistics";
$eEnrollmentMenu['act_event_status_average_attendance'] = "Average Attendance Summary";
$eEnrollmentMenu['act_status_stat'] = "Club Enrolment";
$eEnrollmentMenu['add_act_event'] = "New Activity";
$eEnrollmentMenu['attendance_mgt'] = "Attendance";
$eEnrollmentMenu['attendance_mgt_stu_attend'] = "Student Attendance Record";
$eEnrollmentMenu['attendance_mgt_stu_stat'] = "Student Attendance Statistics";
$eEnrollmentMenu['cat_setting'] = "Category";
$eEnrollmentMenu['cat_title'] = "Category Title";
$eEnrollmentMenu['class_lvl_setting'] = "Class Settings";
$eEnrollmentMenu['club_attendance_mgt'] = "Club Attendance";
$eEnrollmentMenu['club_enroll'] = "Club Enrolment";
$eEnrollmentMenu['club_enroll_status'] = "Club Enrolment Status";
$eEnrollmentMenu['data_batch_process'] = "Synchronize Data";
$eEnrollmentMenu['enroll_title'] = "My Enrolment";
$eEnrollmentMenu['enrollment_sys_setting'] = "Settings";
$eEnrollmentMenu['enrollment_basic_setting'] = "Basic Settings";
$eEnrollmentMenu['event_enroll'] = "Activity Enrolment";
$eEnrollmentMenu['gro_setting'] = "Club Management";
$eEnrollmentMenu['head_count'] = "No of Students";
$eEnrollmentMenu['mgt_act_event'] = "Management";
$eEnrollmentMenu['mgt_cat'] = "Category Management";
$eEnrollmentMenu['mgt_event'] = "Activity Management";
$eEnrollmentMenu['percentage'] = "Percentage";
$eEnrollmentMenu['title'] = "eEnrolment";
$eEnrollmentMenu['user_permission_mgt'] = "User Access Management";
$eEnrollmentMenu['user_set'] = "User Role Setting";
## added on 13 Aug 2008
$eEnrollmentMenu['reports'] = "Reports";
$eEnrollmentMenu['overall_perform_report'] = "Overall Performance Report";
## added on 01/09/2008 Yat Woon
$eEnrollmentMenu['transition'] = "Transition";
$eEnrollmentMenu['transfer_to_ole'] = "Transfer to OLE";
## added on 5 Sept 2008
$eEnrollmentMenu['club'] = "Club";
$eEnrollmentMenu['club_mc'] = ' Extra Curricular Activities';
$eEnrollmentMenu['activity'] = "Activity";
$eEnrollment['tab']['management'] = "Management";
$eEnrollment['tab']['process'] = "Process";
$eEnrollment['tab']['attendance'] = "Attendance";
## added on 10 Sept 2008
$eEnrollment['tab']['record'] = "Record";
## added on 11 Sept 2008
$eEnrollmentMenu['overall_perform'] = "Overall Performance";
$eEnrollmentMenu['class_level_access'] = "Class Level Access";
$eEnrollmentMenu['attendance_taking'] = "Attendance Taking";
## added on 02 Dec 2008
$eEnrollmentMenu['data_handling_to_SP'] = "Data Handling (to SP)";
$eEnrollmentMenu['data_handling_to_OLE'] = "Data Handling (to OLE)";
$eEnrollmentMenu['data_handling_to_WebSAMS'] = "Data Handling (to WebSAMS)";
## added on 23 Mar 2009
$eEnrollmentMenu['enrolment_summary'] = "Enrolment Summary";
$eEnrollment['enrolment_summary_report'] = "Enrolment Summary Report";
$eEnrollmentMenu['enrolment_status'] = "Enrolment Status";
$eEnrollment['enrolment_status_report'] = "Enrolment Status Report";
$eEnrollment['GuardianInformationRemarks'] = "Main Guardian";

$eEnrollment['absent'] = "Absent";
$eEnrollment['act_attendence'] = "Activity Attendance Summary";
$eEnrollment['act_most_update_avg_attendance'] = "Most updated average attendance";
$eEnrollment['act_name'] = "Activity Title";
$eEnrollment['accept_apply'] = "Accept Apply";
$eEnrollment['activity'] = "Activity";
$eEnrollment['add_student_conflict_warning'] = "<font color=\"#ff0000\">The following student(s) cannot be added into the activity due to time clash.</font>";
$eEnrollment['add_payment']['step_1'] = "Payment Details";
$eEnrollment['add_payment']['step_2'] = "Subsidies";
$eEnrollment['all'] = "All";
$eEnrollment['app_period'] = "Enrolment Period";
$eEnrollment['app_stage'] = "Enrolment Stage";
$eEnrollment['app_stu_list'] = "Applied Student List";
$eEnrollment['approve_period'] = "Manual Approval Period";
$eEnrollment['approve_stage'] = "Manual Approval Stage";
$eEnrollment['approved'] = "Approved";
$eEnrollment['approved_stu_list'] = "Approved Students";
$eEnrollment['attendence'] = "Attendance";
$eEnrollment['amount_paid_by_student'] = "Amount paid by student";
$eEnrollment['amount_paid_by_school'] = "Remaining sum paid by school";
$eEnrollment['back_to_event_index'] = "Back to Event Management";
$eEnrollment['back_to_group_index'] = "Back to Club Management";
$eEnrollment['backup_activity_btn'] = "Add ECA information to the students' profile";
$eEnrollment['backup_activity_confirm'] = "Are you sure to add the records to the student's profile?";
$eEnrollment['belong_to_group'] = "Belong to Club";
$eEnrollment['club_name'] = "Club Name";
$eEnrollment['club_quota'] = "Member Quota";
$eEnrollment['club_setting'] = "Club Settings";
$eEnrollment['conflict_warning'] = "<font color=\"#ff0000\">Time Clash</font>";
$eEnrollment['debit_directly'] = "Debit directly";
$eEnrollment['debit_later'] = "Debit later";
$eEnrollment['debit_method'] = "Debit Method";
$eEnrollment['decline_apply'] = "decline application";
$eEnrollment['default_enroll_max_club'] = "Max. no. of clubs for a student to join";
$eEnrollment['default_max_club'] = "Max. no. of clubs for a student to select";
$eEnrollment['default_min_club'] = "Min. no. of clubs for a student to select";
$eEnrollment['denied'] = "Denied";
$eEnrollment['del_warning'] = "<font color=\"#ff0000\">All applied records will be deleted if you save this page.</font>";
$eEnrollment['DoNoUseEnrolment'] = "Do not use eEnrolment";
$eEnrollment['js_del_warning'] = "If you have input data in this step before, and edit and save the data now, all applicant records will be deleted. Confirm saving?";
$eEnrollment['draw'] = "Lottery";
$eEnrollment['draw_and_end'] = "Perform Lottery and Confirm Enrolment Finished";
$eEnrollment['event_member'] = "Member";
# added on 10 Sept 2008
$eEnrollment['member'] = "Member";

$eEnrollment['enroll_persontype'] = "Target Applicants";
$eEnrollment['female'] = "F";
$eEnrollment['groupmates_list'] = "Club Member List";
$eEnrollment['lucky_draw'] = "Perform Lottery";
$eEnrollment['mail_enroll_event_mail1'] = "Activity Enrolment Process has been completed.";
$eEnrollment['mail_enroll_event_mail2'] = "You can go to Activity Info to find the information of the activities you have been approved.";
$eEnrollment['mail_enroll_event_mail3'] = "Should you have any problems, please contact System Admin or the teachers responsible for ECA Enrolment.";
$eEnrollment['mail_enroll_event_mail_fail'] = "Result: <STRONG>Unsuccessful</STRONG>";
$eEnrollment['mail_enroll_event_mail_success'] = "Result: <STRONG>Successful</STRONG>";
$eEnrollment['mail_enroll_event_subject'] = "Activity Enrolment Result";
$eEnrollment['mail_enroll_club_mail1'] = "Club Enrolment Process has been completed.";
$eEnrollment['mail_enroll_club_mail2'] = "You can go to Club Info to find the information of the clubs you have been approved.";
$eEnrollment['mail_enroll_club_mail3'] = "Should you have any problems, please contact System Admin or the teachers responsible for ECA Enrolment.";
$eEnrollment['mail_enroll_club_mail_fail'] = "Result: <STRONG>Unsuccessful</STRONG>";
$eEnrollment['mail_enroll_club_mail_success'] = "Result: <STRONG>Successful</STRONG>";
$eEnrollment['mail_enroll_club_subject'] = "Clubs Enrolment Result";
$eEnrollment['male'] = "M";
$eEnrollment['manage_app_stu_list'] = "Applicants";
$eEnrollment['manage_apply_namelist'] = "Applicants";
$eEnrollment['manage_groupmates_list'] = "Members";
$eEnrollment['manage_namelist'] = "Manage Namelist";
$eEnrollment['meeting'] = "Meeting";
$eEnrollment['next_app_stage'] = "Proceed to Next Round";
$eEnrollment['no_limit'] = "No Limit";
$eEnrollment['no_name'] = "No Name";
$eEnrollment['no_record'] = "No record";
$eEnrollment['parent'] = "Parent";
$eEnrollment['payment'] = "Payment";
$eEnrollment['PaymentAmount'] = "Tentative Fee HK($)";
$eEnrollment['payment_item'] = "Payment Item";
$eEnrollment['position'] = "Position";
$eEnrollment['present'] = "Present";
$eEnrollment['process_end'] = "Process finished";
$eEnrollment['quota_left'] = "Quota Left";
$eEnrollment['set_admin'] = "Set as Admin";
$eEnrollment['status'] = "Status";
$eEnrollment['stu_club_rule'] = "Student can only choose <font color=\"#ff0000\"><!--MinClub--></font> to <font color=\"#ff0000\"><!--MaxChoice--></font> club(s) by default, and they can join <font color=\"#ff0000\"><!--MaxClub--></font> club(s) at most.";
$eEnrollment['studnet_attendence'] = "Student Attendance";
$eEnrollment['student_name'] = "Student Name";
$eEnrollment['student'] = "Student";
$eEnrollment['system_user_list'] = "eEnrolment Teacher Access List";
$eEnrollment['system_user_name'] = "Teacher's Name";
$eEnrollment['teachers_comment'] = "Teacher's Comment";
$eEnrollment['to'] = "to";
$eEnrollment['too_many_apply_process'] = "Rule for tie-breaker";
$eEnrollment['unset_admin'] = "Unset Admin Role";
$eEnrollment['up_to'] = "Up to";
$eEnrollment['use_system'] = "Can use this system";
$eEnrollment['waiting'] = "Pending";
$eEnrollment['whole_stage_end'] = "Send Notification to Successful Applicants";
$eEnrollment['year'] = "Academic Year";

$eEnrollment['replySlip']['replySlip'] = "Reply Slip";
$eEnrollment['replySlip']['Preview'] = "Preview";
$eEnrollment['replySlip']['Edit'] = "Edit";
$eEnrollment['replySlip']['SaveAsTemplate'] = "Save As Template";
$eEnrollment['replySlip']['SignedPerson'] = "SignedPerson";
$eEnrollment['replySlip']['Instruction'] = "Instruction";
$eEnrollment['replySlip']['SignedDate'] = "Signed Date";
$eEnrollment['replySlip']['Title'] = "Reply Slip Title";
$eEnrollment['replySlip']['Signed_Total'] = "Signed/ Total";
$eEnrollment['replySlip']['BuildDate'] = "Build Date";
$eEnrollment['replySlip']['Instruction'] = 'Instruction';
$eEnrollment['replySlip']['SignedInstruction'] = 'This reply slip is signed by ##PERSON## at ##DATE##';
$eEnrollment['replySlip']['NotSign'] = "Not Signed";
$eEnrollment['replySlip']['ClassName'] = "ClassName";
$eEnrollment['replySlip']['ClassNumber'] = "Class Number";
$eEnrollment['replySlip']['StudentName'] = "Student Name";
$eEnrollment['replySlip']['Question'] = "Qusetion";
$eEnrollment['replySlip']['SignedBy'] = "Signed By";
$eEnrollment['replySlip']['DateSigned'] = "Date Signed"; 
$eEnrollment['replySlip']['ReplySlipDetail'] = "Reply Slip Detail";
$eEnrollment['replySlip']['ReplySlipNotSet'] = "This Item Have Not Set A Reply Slip";
$eEnrollment['replySlip']['MustFillWarning'] = "All The Question Starred Must Be Filled In";


$eEnrollment['add_club_activity']['step_1'] = "Set Info";
$eEnrollment['add_club_activity']['step_1b'] = "Assign Staff & Helper";
$eEnrollment['add_club_activity']['step_2'] = "Schedule";

$eEnrollment['add_activity']['act_age'] = "Target Age Group";
$eEnrollment['add_activity']['act_assistant'] = "Attendance Helper";
$eEnrollment['add_activity']['act_category'] = "Category";
$eEnrollment['add_activity']['act_content'] = "Content";
$eEnrollment['add_activity']['act_date'] = "Date";
$eEnrollment['add_activity']['act_gender'] = "Target Gender";
$eEnrollment['add_activity']['act_name'] = "Activity Name";
$eEnrollment['add_activity']['act_pic'] = "Person-in-charge";
$eEnrollment['add_activity']['act_quota'] = "Quota";
$eEnrollment['add_activity']['act_target'] = "Target Level";
$eEnrollment['add_activity']['act_time'] = "Time";
$eEnrollment['add_activity']['app_method'] = "Enrolment Method";
$eEnrollment['add_activity']['app_period'] = "Enrolment Period";
$eEnrollment['add_activity']['attachment'] = "Attachment";
$eEnrollment['add_activity']['age'] = "years old";
$eEnrollment['add_activity']['apply_ppl_type'] = "Enrolment Setting";
$eEnrollment['add_activity']['apply_role_type'] = "Assign Role";
$eEnrollment['add_activity']['more_attachment'] = "More Attachment";
$eEnrollment['add_activity']['parent_enroll'] = "Enrolled By Parent";
$eEnrollment['add_activity']['pickup_ppl'] = "Select participants";
$eEnrollment['add_activity']['select'] = "Select";
$eEnrollment['add_activity']['set_select_period'] = "Set Selected Time";
$eEnrollment['add_activity']['set_all'] = "Set All";
$eEnrollment['add_activity']['step_1'] = "Define Activity";
$eEnrollment['add_activity']['step_1b'] = "Assign Staff & Helper";
$eEnrollment['add_activity']['step_2'] = "Schedule";
$eEnrollment['add_activity']['step_3'] = "Set Rights";
$eEnrollment['add_activity']['student_enroll'] = "Enrol By Student";

$eEnrollment['front']['app_period'] = "Apply Period : <!--AppStart--> to <!--AppEnd-->";
$eEnrollment['front']['applied'] = "Applied";
$eEnrollment['front']['approve_period'] = "Manual Approval Period : <!--AppStart--> to <!--AppEnd-->";
$eEnrollment['front']['enroll'] = "Enrol";
$eEnrollment['front']['enrolled'] = "Enrolled";
$eEnrollment['front']['event_time_conflict'] = "<font color=\"#ff0000\">If the activities enrolled have time clashes, you can only participate in one of them.</font>";
$eEnrollment['front']['information'] = "Information";
$eEnrollment['front']['quota_warning'] = "<font color=\"#ff0000\">Full</font>";
$eEnrollment['front']['time_conflict_warning'] = "<font color=\"#ff0000\">If the meetings of the clubs enrolled have time clashes, you can only enrol one of them.</font>";
$eEnrollment['front']['wish_enroll_min'] = "You should apply at least <!--NoOfClub--> club(s)";
$eEnrollment['front']['wish_enroll_min_event'] = "You should apply at least <!--NoOfActivity--> activities";
$eEnrollment['front']['wish_enroll_front'] = " and at most &nbsp;";
$eEnrollment['front']['wish_enroll_end'] = "&nbsp; club(s).";
$eEnrollment['front']['wish_enroll_end_event'] = "&nbsp; activities.";
$eEnrollment['approve_quota_exceeded'] = "Quota exceeded, some students cannot be added to the activity.";

$eEnrollment['js_confirm_del'] = "Are you sure you want to delete the item(s)?";
$eEnrollment['js_confirm_del_single'] = "Are you sure you want to delete the item(s)?";
$eEnrollment['js_payment_alert'] = "Payment record can be processd once only and cannot be modified after process, are you sure to continue?";
$eEnrollment['js_prepayment_alert'] = "You can process this Payment record once only. Make sure the information is correct.  Are you sure you want to process the item?";
$eEnrollment['js_sel_pic'] = "Please select person in charge";
$eEnrollment['js_sel_student'] = "Please select student";
$eEnrollment['js_quota_exists'] = "Quota exceeded, you have selected <!-- NoOfSelectedStudent--> students, please select <!-- NoOfStudent--> student(s) only.";
$eEnrollment['js_sel_date'] = "Please select activity date.";
$eEnrollment['js_time_invalid'] = "Invalid time range.";
$eEnrollment['js_confirm_cancel'] = "Are you sure you want to cancel this application?";
$eEnrollment['js_fee_positive'] = "Please enter a positive integer for Tentative Fee";
## added on 26 Aug 2008
//add_member.php
$eEnrollment['js_enter_role'] = "Please enter a role";
## added on 03 Sept 2008
//add_member.php
$eEnrollment['js_zero_means_no_limit'] = "Note that setting quota to 0 means no limit";
# added on 24 Sept 2008
//active_member.php
$eEnrollment['js_update_active_member_per'] = "Active member status of students will be updated according to the new percentage. Continue?";
## added on 29 Sept 2008
//payment.php
$eEnrollment['js_amount_cannot_be_zero'] = "Amount must be greater than zero";
## added on 02 Oct 2008
$eEnrollment['js_enrol_one_only'] = "Time clash occurs. You can apply for one of the time-clashed clubs only.";
## added on 10 Oct 2008
$eEnrollment['js_percentage_alert'] = "The input percentage should in between 0.00-100.00%";
$eEnrollment['js_hours_alert'] = "The input hours should be a positive number.";
## added on 11 Oct 2008
$eEnrollment['js_target_group_alert'] = "Please choose at least one target group.";
$eEnrollment['js_no_active_member_club'] = "No active member for all above clubs";
$eEnrollment['js_no_active_member_activity'] = "No active member for all above activities";
$eEnrollment['js_close_compose_email_window'] = "All the modified contents will be lost if you close this window. Are you sure you want to close this window?";


$eEnrollment['month'][0] = "December";
$eEnrollment['month'][1] = "January";
$eEnrollment['month'][2] = "February";
$eEnrollment['month'][3] = "March";
$eEnrollment['month'][4] = "April";
$eEnrollment['month'][5] = "May";
$eEnrollment['month'][6] = "June";
$eEnrollment['month'][7] = "July";
$eEnrollment['month'][8] = "August";
$eEnrollment['month'][9] = "September";
$eEnrollment['month'][10] = "October";
$eEnrollment['month'][11] = "November";
$eEnrollment['month'][12] = "December";
$eEnrollment['month'][13] = "January";

$eEnrollment['pay']['amount'] = "Amount";
$eEnrollment['pay']['item_name'] = "Item Name";

$eEnrollment['select']['helper'] = "Attendance Helper";
$eEnrollment['select']['pic'] = "Person-in-charge";

$eEnrollment['setting']['club_lower_limit'] = "Max. no of club can be applied";
$eEnrollment['setting']['club_upper_limit'] = "Min. no of club shoule be applied";

$eEnrollment['user_role']['normal_user'] = "Normal User 一般使用者";
$eEnrollment['user_role']['enrollment_master'] = "Enrolment Master 學會報名主任";
$eEnrollment['user_role']['enrollment_admin'] = "Enrolment Admin 學會報名管理員";

$eEnrollment['weekday']['sun'] = "Sun";
$eEnrollment['weekday']['mon'] = "Mon";
$eEnrollment['weekday']['tue'] = "Tue";
$eEnrollment['weekday']['wed'] = "Wed";
$eEnrollment['weekday']['thu'] = "Thu";
$eEnrollment['weekday']['fri'] = "Fri";
$eEnrollment['weekday']['sat'] = "Sat";

$eEnrollment['skip'] = "Skip This Step";

### added on 19 Jun 2008
$eEnrollment['payment_success_list'] = "The following payments have been processed:";
$eEnrollment['payment_fail_list'] = "The following payments cannot be processed:";
$eEnrollment['payment_status_success'] = "Success.";
$eEnrollment['payment_status_fail'] = "Failed. Inadequate Account Balance";
$eEnrollment['payment_summary'] = "Payment Summary";

$eEnrollment['attendance']['present'] = "Present";
$eEnrollment['attendance']['absent'] = "Absent";

$eEnrollment['role'] = "Role";
$eEnrollment['class_name'] = "Class";
$eEnrollment['class_number'] = "Class Number";
$eEnrollment['student_name'] = "Name";
$eEnrollment['performance'] = "Performance";
$eEnrollment['comment'] = "Comment";
$eEnrollment['member_perform_comment_list'] = "Member Performance and Comment";
$eEnrollment['student_perform_comment_list'] = "Student Performance and Comment";

### added on 04 Aug 2008
// choice/index.php
$eEnrollment['club'] = "Club";
$eEnrollment['selection_mode'] = "Selection Mode";
// export.php
$eEnrollment['assigning_member'] = "Assigning Member";
$eEnrollment['assigning_student'] = "Assigning Student";
$eEnrollment['assigning_enrolment'] = "Assigning Enrolment";

### added on 05 Aug 2008
// import.php
$eEnrolment['DownloadCSVFile'] = "Download CSV file template";
$eEnrolment['AlertSelectFile'] = "Please select a file first";
$eEnrolment['attendance_title'] = "Attendance";

### added on 07 Aug 2008
// import_result.php
$eEnrollment['invalid_date'] = "Invalid event date time.";
$eEnrollment['student_not_found'] = "Student not found.";
$eEnrollment['student_not_member'] = "Student is not a member.";
$eEnrollment['record_existed'] = "Record existed already.";
$eEnrollment['empty_row'] = "No data entry.";
$eEnrollment['student_not_event_student'] = "Student is not in the list of this activity.";
$eEnrollment['club_not_found'] = "Club not found.";
$eEnrollment['activity_not_found'] = "Activity not found.";
$eEnrollment['clubs_with_same_name'] = "More than one clubs having the same club name.";
$eEnrollment['activity_with_same_name'] = "More than one activities having the same title.";
$eEnrollment['invalid_active_member_status'] = "Invalid active member status.";
$eEnrollment['member_added'] = "Member info added.";
$eEnrollment['member_updated'] = "Member info updated.";
$eEnrollment['participant_added'] = "Participant info added.";
$eEnrollment['participant_updated'] = "Participant info updated.";

$eEnrollment['import_status'] = "Import Status";

$eEnrollment['back_to_enrolment'] = "Back To Enrolment";
$eEnrollment['back_to_attendance'] = "Back to Attendance";

//member_index and event_member_index
$eEnrollment['import_performance_comment'] = "Import Performance and Comment";

// import.php
$eEnrollment['Attendance_Import_FileDescription'] = "\"ClassName\",\"ClassNumber\",\"ActivityDate(YYYY-MM-DD)\",\"ActivityStartTime(HH:MM)\"";
$eEnrollment['Performance_Import_FileDescription'] = "\"ClassName\",\"ClassNumber\",\"Performance\",\"Comment\"";
$eEnrollment['Member_Import_FileDescription'] = "\"ClassName\",\"ClassNumber\",\"Role\",\"Performance\",\"Comment\",\"Active/Inactive\"";
$eEnrollment['All_Member_Import_FileDescription'] = "\"ClubName\", \"ClassName\",\"ClassNumber\",\"Role\",\"Performance\",\"Comment\",\"Active/Inactive\"";
$eEnrollment['All_Participant_Import_FileDescription'] = "\"ActivityTitle\", \"ClassName\",\"ClassNumber\",\"Role\",\"Performance\",\"Comment\",\"Active/Inactive\"";
$eEnrollment['activity_start_time'] = "Activity Start Time";
$eEnrollment['activity_title'] = "Activity Title";
$eEnrollment['performance_title'] = "Performance and Comment";
$eEnrollment['csv_header']['club_name'] = "ClubName";
$eEnrollment['csv_header']['activity_title'] = "ActivityTitle";

### added on 11 Aug 2008
//add_member.php
$eEnrollment['add_student_result'] = "Add Student(s) Results";
$eEnrollment['select_student'] = "Select Student(s)";

//add_member_update.php
$eEnrollment['student_is_member'] = "Student is a current member";
$eEnrollment['student_is_joined_event'] = "Student has joined the activity already";
$eEnrollment['crash_group'] = "Time clashes with other club(s)";
$eEnrollment['crash_activity'] = "Time clashes with other activit(ies)";
$eEnrollment['crash_group_activity'] = "Time clashes with other club(s) and activit(ies)";

//add_member_result.php
$eEnrollment['back_to_member_list'] = "Back to Member List";
$eEnrollment['back_to_student_list'] = "Back to Student List";
$eEnrollment['back_to_applicant_list'] = "Back to Applicant List";
$eEnrollment['add_others_student'] = "Add Other Student(s)";
$eEnrollment['back_to_club_management'] = "Back to Club Management";
$eEnrollment['back_to_activity_management'] = "Back to Activity Management";

### added on 12 Aug 2008
//get_sample_csv.php
$eEnrollment['sample_csv']['ClassName'] = "ClassName";
$eEnrollment['sample_csv']['ClassNumber'] = "ClassNumber";

//basic.php
$eEnrollment['disable_status'] = "Disable Function of changing Applicant Status after Payment";
$eEnrollment['active_member_per'] = "Minimum attendance required of active member";
$eEnrollment['active_member_per_short'] = "Min. attendance Required of Active Member";

$eEnrollment['active_member'] = "Active Member";
$eEnrollment['in_active_member'] = "In-active Member";

## added on 01/09/2008 Yat Woon
$eEnrollment['Record_Type'] = "Record Type";
$eEnrollment['All_Records'] = "All Records";
$eEnrollment['Club_Records'] = "Club Records";
$eEnrollment['Activity_Records'] = "Activity Records";
$eEnrollment['Transfer'] = "Transfer";
$eEnrollment['Not_Transfer'] = "Not Transfer";
$eEnrollment['Date'] = "Date";
$eEnrollment['Period'] = "Period";
$eEnrollment['No_of_Student'] = "No. of Students";
$eEnrollment['Component_of_OLE '] = "Component of OLE";
$eEnrollment['transfer_finish']['step1'] = "Select Record Type";
$eEnrollment['transfer_finish']['step2']  = "Set Details";
$eEnrollment['transfer_finish']['step3']  = "Replace Records";
$eEnrollment['transfer_finish']['step4']  = "Complete";
$eEnrollment['duplicate_ole_records']  = "Overwite the settings of duplicated clubs/activities? ";
$eEnrollment['duplicate_ole_participant_record']  = "Overwrite duplicated student OLE records? ";
$eEnrollment['overwrite']  = "Overwrite";
$eEnrollment['skip2']  = "Skip";
$eEnrollment['Already_Transfered']  = "Already Transfered";
$eEnrollment['Transfer_Another_OLE']  = "Transfer another OLE";
$eEnrollment['transfer_student_setting']  = "Transfer Records of";
$eEnrollment['all_member']  = "All Members";
$eEnrollment['active_member_only']  = "Active Member(s) Only";
$eEnrollment['OLE_Category'] = "Category";


## added on 13 Aug 2008
//basic.php
$eEnrollment['active_member_alert'] = "Attendance Percentage of a Active Member must be between 0.00-100.00%";

//overall_performance_report.php
$eEnrollment['total_performance'] = "Overall Performance";

## added on 14 Aug 2008
//overall_performance_report.php
$eEnrollment['total_club_performance'] = "Total Club Performance";
$eEnrollment['total_act_performance'] = "Total Activity Performance";

## added on 21 Aug 2008
$eEnrollment['notification_letter'] = "Notification Letter";

## added on 25 Aug 2008
//overall_performance_report.php
$eEnrollment['print_class_enrolment_result'] = "Print Class Enrolment Result";
$eEnrollment['print_club_enrolment_result'] = "Print Club Enrolment Result";

//add_member_result.php
$eEnrollment['club_quota_exceeded'] = "Club Quota Exceeded";
$eEnrollment['student_quota_exceeded'] = "Student's Club Quota Exceeded";
$eEnrollment['act_quota_exceeded'] = "Activity Quota Exceeded";
$eEnrollment['rejected'] = "Rejected";

//basic.php
$eEnrollment['one_club_when_time_crash'] = "Enrol student to ONE club only if there is a time clash";
$eEnrollment['hide_club_if_quota_full'] = "Hide club selection if the number of applicants has reached the club's quota";

## added on 26 Aug 2008
//add_member_result.php
$eEnrollment['time_crash'] = "Time Clash";
$eEnrollment['club_or_act_title'] = "Club/Activity and Time";

## added on 28 Aug 2008
//payment_summary.php
$eEnrollment['back_to_club_enrolment'] = "Back to Club Enrolment";
$eEnrollment['back_to_act_enrolment'] = "Back to Activity Enrolment";

//search box initial text
$eEnrollment['enter_name'] = "Enter Name";
$eEnrollment['enter_student_name'] = "Enter Student Name";

## added on 10 Sept 2008
$eEnrollment['enable_online_registration'] = "Enable Online Registration";
$eEnrollment['disable_online_registration'] = "Disable Online Registration";
$eEnrollment['current_title'] = "Current Title";
$eEnrollment['new_title'] = "New Title";

## added on 11 Sept 2008
$eEnrollment['waiting_list'] = "Waiting List";
$eEnrollment['number_of_applicants'] = "Number of Applicants";
$eEnrollment['process'] = "Process";
$eEnrollment['participant'] = "Participant";
$eEnrollment['participant2'] = "Participant";

## added on 12 Sept 2008
$eEnrollment['processed'] = "Processed";

## added on 17 Sept 2008
$eEnrollment['quota_exceeded'] = "Quota exceeded";
$eEnrollment['default_school_quota_exceeded'] = "Default School Quota Exceeded";
//basic_event.php
$eEnrollment['default_max_act'] = "Max. no. of activities for a student to select";
$eEnrollment['default_min_act'] = "Min. no. of activities for a student to select";
$eEnrollment['default_enroll_max_act'] = "Max. no. of activities for a student to join";
$eEnrollment['one_act_when_time_crash'] = "Enrol student to ONE activity only if there is a time clash";

## added on 26 Sept 2008
$eEnrollment['Edit_Club_Info'] = "Edit Group Info";
$eEnrollment['crash_with'] = "Clash with";

## added on 29 Sept 2008
$eEnrollment['app_stage_event'] = "The following settings will be applicable between";
$eEnrollment['event_and'] = "and";

## added on 02 Oct 2008
$eEnrollment['warn_no_more_enrolment'] = "<font color=\"red\">The number of clubs you have enrolled has reached the school limit already. No more club enrolment is allowed.</font>";

## added on 09 Oct 2008
$eEnrollment['total_hours_of_student'] = "Participated hours by individual student";
$eEnrollment['total_hours_if_above_percentage'] = "Total hours if attendance rate is equal to or greater than ";
$eEnrollment['manual_input_hours'] = "Custom - ";
$eEnrollment['unit_hour'] = "hour(s)";
$eEnrollment['hours_setting'] = "Hours";
$eEnrollment['transfer_if_zero_hour'] = "Transfer participated records with zero hour";
$eEnrollment['num_transferred_record'] = "No. of Transferred Records";

## added on 10 Oct 2008
$eEnrollment['overall_performance_instruction'] = "The Overall Performance Report can be applied to performance <font color=\"red\"><strong>SCORE</strong></font> only. Text can still be entered as the performance of students but all texts will be ignored in the Overall Performance Report.";

## added on 20 Oct 2008
$eEnrollment['OLE_Components'] = "OLE Components";

## added on 21 Oct 2008
$eEnrollment['club_act_in_OLE'] = "Club/Activity in OLE";
$eEnrollment['student_OLE_records'] = "Student OLE Records";
$eEnrollment['replace_all'] = "Replace All";
$eEnrollment['replace'] = "Replace";

## added on 22 Oct 2008
$eEnrollment['global_settings'] = "Global Settings";
// for OLE program record
$eEnrollment['Record_exists'] = "Record exists.";
// for OLE student record
$eEnrollment['record_exists'] = "record(s) exists.";
$eEnrollment['record_in_OLE'] = "No. of records in OLE";

## added on 3 Nov 2008
$eEnrollment['alert_all_enrolment_records_deleted_club'] = "Alert: All enrolment records, except the clubs' information, will be deleted.";
$eEnrollment['alert_all_enrolment_records_deleted_activity'] = "Alert: All enrolment records, except activities' information, will be deleted.";

## added on 14 Nov 2008
$eEnrollment['all_categories'] = "All Categories";

## added on 2 Dec 2008
$eEnrollment['enrolment_settings'] = "Enrolment Settings";
$eEnrollment['schedule_settings']['instruction'] = "Check the date(s) of the meetings/events for the Club in the calendar and the checked dates will be automatically displayed with the timeslots for setting the periods. To clear the box(es), unselect it/them.";
$eEnrollment['schedule_settings']['scheduling_dates'] = "Scheduling Dates";
$eEnrollment['schedule_settings']['scheduling_times'] = "Scheduling Times";
$eEnrollment['date_chosen'] = "Date";

$eEnrollment['button']['add&assign'] = "Add & Assign";
$eEnrollment['button']['active_member'] = "View & Edit ".$eEnrollment['active_member'];
$eEnrollment['button']['notification_letter'] = "Print ".$eEnrollment['notification_letter'];
$eEnrollment['button']['update_role'] = $button_update." ".$eEnrollment['role'];
$eEnrollment['button']['next_round'] = "Clear the unhandled data";
$eEnrollment['button']['perform_drawing'] = "Perform drawing";


$eEnrollment['select_role'] = "- Select Role -";
$eEnrollment['add_member_and_assign_role'] = "Add Member & Assign Role";
$eEnrollment['add_participant_and_assign_role'] = "Add Participant & Assign Role";
$eEnrollment['add_member'] = "Add Member";
$eEnrollment['add_participant'] = "Add Participant";
$eEnrollment['class_no'] = "Class No.";
$eEnrollment['time_conflict'] = "Time Conflict";
$eEnrollment['lookup_for_clashes'] = "Lookup for Clashes";
$eEnrollment['succeeded'] = "Succeeded";
$eEnrollment['failed'] = "Failed";
$eEnrollment['active'] = "Active";
$eEnrollment['inactive'] = "Inactive";
$eEnrollment['category'] = "Category";
$eEnrollment['view_statistics'] = "View Statistics";
$eEnrollment['take_attendance'] = "Take Attendance";
$eEnrollment['unhandled'] = "Unhandled";
$eEnrollment['drawing'] = "Drawing";
$eEnrollment['new_activity'] = "New Activity";
$eEnrollment['edit_activity'] = "Edit Activity";
$eEnrollment['participant_quota'] = "Participant Quota";
$eEnrollment['participant_list'] = "Participant List";
$eEnrollment['enter_role'] = "Enter Role";
$eEnrollment['attendance_rate'] = "Attendance Rate";
$eEnrollment['member_selection_and_drawing'] = "Member Selection & Drawing";
$eEnrollment['participant_selection_and_drawing'] = "Participant Selection & Drawing";
$eEnrollment['random_drawing'] = "Random Drawing";
$eEnrollment['last_modified'] = "Last modified";
$eEnrollment['confirm'] = "CONFIRM";
$eEnrollment['proceed_to_step2'] = "Proceed to Step 2";

$eEnrollment['next_round_instruction'] = "Before moving into the next round of selection, click this button";
$eEnrollment['drawing_instruction'] = "To enable system <font color='red'><!--DrawingMethod--></font> distribution, click this button";
$eEnrollment['student_enrolment_rule'] = "Students can choose <font color='red'><!--MinNumApp--></font> to <font color='red'><!--MaxNumApp--></font> club(s) and can join <font color='red'><!--MaxEnrol--></font> club(s) at most.";
$eEnrollment['enrolment_period'] = "Enrolment Period of this Round:";
$eEnrollment['student_enrolment_rule_activity'] = "Students can choose <font color='red'><!--MinNumApp--></font> to <font color='red'><!--MaxNumApp--></font> Activit(ies) and can join <font color='red'><!--MaxEnrol--></font> Activt(ies) at most.";
$eEnrollment['enrolment_period_activity'] = "Enrolment Period:";

$eEnrollment['archive_instruction'] = "The data of ECA groups can be imported to the Students' Profile. The data include <b>Activity Name, Role and Performance</b>, and are based on the following year and school term.";
$eEnrollment['archive_footer1'] = "To change the year and school term above, please access Admin Console > Settings > Basic Settings, or contact your System Administrator.";
$eEnrollment['archive_footer2'] = "Are you ready to add ECA Data to the Students Profile?";

$eEnrollment['attendence_record'] = "Attendance";
$eEnrollment['import_member'] = "Import Member";
$eEnrollment['import_participant'] = "Import Participant";

# added on 24 Mar 2008 by Ivan for Enrolment Summary Report
$eEnrollment['Specify'] = "Specify";
$eEnrollment['generate_or_update'] = "Generate / Update";

# added on 26 Mar 2008 by Ivan for Club Enrolment notification
$eEnrollment['send_club_enrolment_notification'] = "Send Club Enrolment Notification";
$eEnrollment['notify_by_email'] = "Notify by Email";
$eEnrollment['default_student_notification_title'] = "Reminder: Club Enrolment (deadline: <--deadline-->)";
$eEnrollment['default_student_notification_content'] = "Please be reminded to register clubs in eEnrolment by <--deadline-->!";
$eEnrollment['default_class_teacher_notification_title'] = "Reminder: Student Club Enrolment (deadline: <--deadline-->)";
$eEnrollment['default_class_teacher_notification_content'] = "Some of your students have not registerred any clubs. They are listed below. Please kindly remind them to register clubs in eEnrolment by <--deadline-->! \n";
$eEnrollment['to_student'] = "To student(s)";
$eEnrollment['to_class_teacher'] = "To class teacher(s)";
$eEnrollment['no_class_teacher_warning'] = "There are no class teachers in class(es) <--ClassList-->.";
$eEnrollment['class'] = "Class";
$eEnrollment['and'] = "and";
$eEnrollment['system_append_student_list_atuomatically'] = "A student list of the corresponding class will be appended to the email content automatically.";

$eEnrollment['success'] = "Success";
$eEnrollment['ImportOtherRecords'] = "ImportOtherRecords";

$eEnrollment['Content'] = "Content";

$eEnrollment['Target'] = "Target";
$eEnrollment['AverageAttendance'] = "Average Attendance";

###############################################################################################################
##### eEnrolment End #########################################################################################
###############################################################################################################


## IP20 Menu language
$ip20TopMenu['iTools'] = "iTools";
$ip20TopMenu['iMail'] = "iMail";
$ip20TopMenu['iFolder'] = "iFolder";
$ip20TopMenu['iCalendar'] = "iCalendar";
$ip20TopMenu['iSmartCard'] = "iSmartCard";
$ip20TopMenu['iPortfolio'] = "iPortfolio";
$ip20TopMenu['iAccount'] = "iAccount";

$ip20TopMenu['eAdmin'] = "eAdmin";
$ip20TopMenu['eAttendance'] = "eAttendance";
$ip20TopMenu['eAttendanceLesson'] = 'eAttendance(Lesson)';
$ip20TopMenu['eDiscipline'] = "eDiscipline";
$ip20TopMenu['eDisciplinev12'] = "eDiscipline";
$ip20TopMenu['eReportCard'] = "eReportCard";
$ip20TopMenu['eMarking'] = "eMarking";
$ip20TopMenu['eHomework'] = "eHomework";
$ip20TopMenu['eEnrollment'] = "eEnrolment";
$ip20TopMenu['eSports'] = "eSports";
$ip20TopMenu['SwimmingGala'] = "Swimming Gala";
$ip20TopMenu['eOffice'] = "eOffice";
$ip20TopMenu['ePayment'] = "ePayment";
$ip20TopMenu['eBooking'] = "eBooking";
$ip20TopMenu['eInventory'] = "eInventory (Asset Management)";
$ip20TopMenu['eSurvey'] = "eSurvey";
$ip20TopMenu['eTimeTabling'] = "eTimeTabling";

$ip20TopMenu['eLearning'] = "eLearning";
$ip20TopMenu['eClass'] = "eClass";
$ip20TopMenu['eLC'] = "eLC";
$ip20TopMenu['eMeeting'] = "eMeeting";
$ip20TopMenu['LSLP'] = "LSLP";
$ip20TopMenu['eLibrary'] = "eBook";
if (isset($plugin['eLib_trial']) && $plugin['eLib_trial'])
{
	$ip20TopMenu['eLibrary'] .= " (trial version)";
}
$ip20TopMenu['eTV'] = "eTV";

$ip20TopMenu['eCommunity'] = "eCommunity";
$ip20TopMenu['eGroups'] = "eGroups";
$ip20TopMenu['eParents'] = "eParents";
$ip20TopMenu['eAlumni'] = "eAlumni";

$ip20TopMenu['eService'] = "eService";
$ip20TopMenu['SLSLibrary'] = "SLS Library";

$i_SMS_Personalized_Tag['user_name_eng'] = $i_UserEnglishName;
$i_SMS_Personalized_Tag['user_name_chi'] = $i_UserChineseName;
$i_SMS_Personalized_Tag['user_class_name'] = $i_ClassName;
$i_SMS_Personalized_Tag['user_class_number'] = $i_ClassNumber;
$i_SMS_Personalized_Tag['user_login'] = $i_UserLogin;
$i_SMS_Personalized_Tag['attend_card_time'] = "Arrival/Departure card read time";
$i_SMS_Personalized_Tag['payment_balance'] = $i_Payment_PrintPage_Outstanding_AccountBalance;
$i_SMS_Personalized_Tag['payment_outstanding_amount'] = $i_Payment_PrintPage_Outstanding_LeftAmount;

### Added On 6 Sep 2007
$i_send_sms = "Send SMS";

$i_license_sys_msg = "Your student license is <!--license_total-->, with <!--license_left--> license(s) left. You have used up <!--license_used-->.";
$i_license_sys_msg_none = "No student license left!";

$i_spam['FolderSpam'] =  "SPAM";
$i_spam['Setting'] = "Anti-virus Anti-spam Settings";
$i_spam['WhiteList'] = "White List";
$i_spam['BlackList'] = "Black List";
$i_spam['BypassSetting'] = "Bypass Setting";
$i_spam['SpamControl'] = "Spam Control";
$i_spam['VirusScan'] = "Virus Scan";
$i_spam['Option_Activated'] = "Activated";
$i_spam['Option_Bypass'] = "Bypass";
$i_spam['EmailAddress'] = "Email Address";
$i_spam['AlertRemove'] = "Are you sure you want to remove this record?";
# added by Ronald on 20080807 #
$i_spam['SpamControlLevel'] = "Spam Control Level";
$i_spam['SpamControlLevel_DefaultLevel'] = "Use Default Level";
$i_spam['SpamControlLevel_CustomLevel'] = "Use Custom Level";
$i_spam['js_Alert_SpamControlLevelEmpty'] = "Please input the control level";
$i_spam['js_Alert_SpamControlLevelNotVaild'] = "Please input the vaild control level";
# added by Ronald on 20080812 #
$i_spam['CustoSpamControlLevel_Description'] = "* - Smaller value, higher spam control level and vice versa. Range: 0 - 6.0";
$i_Campusmail_MailQuotaSettting_SelectGroupWarning = "Please select the group";

###################################
### Student Leave School Option ###
### Added On 29 Aug 2007 ###
$i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption = "Students Leave Option";
$i_StudentAttendance_LeaveOption_Weekday = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
$i_StudentAttendance_LeaveOption_Selection = array("None","Parent","School Bus","Self");
$i_StudentAttendance_LeaveOption_ImportFileDescription = "
Column 1 : Class Name <br>
Column 2 : Class Number <br>
Column 3 : * <b>Monday</b> Leave Option <br>
Column 4 : * <b>Tuesday</b> Leave Option <br>
Column 5 : * <b>Wednesday</b> Leave Option <br>
Column 6 : * <b>Thursday</b> Leave Option <br>
Column 7 : * <b>Friday</b> Leave Option <br>
Column 8 : * <b>Saturday</b> Leave Option <br>
<br>
*&nbsp;0 - None <br>
&nbsp;&nbsp;&nbsp;1 - Parent <br>
&nbsp;&nbsp;&nbsp;2 - School Bus <br>
&nbsp;&nbsp;&nbsp;3 - Student
";
############################

### Added On 31 Aug 2007 ###
$i_studentAttendance_LeaveOption_ClassSelection_Warning = "Please Select The Class";

$i_studentAttendance_LeaveOption_Import_ErrorMsg[1] = "<b>Monday</b> Leave Option";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[2] = "<b>Tuesday</b> Leave Option";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[3] = "<b>Wednesday</b> Leave Option";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[4] = "<b>Thursday</b> Leave Option";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[5] = "<b>Friday</b> Leave Option";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[6] = "<b>Saturday</b> Leave Option";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[7] = "Class Name/Class Number Failed";
############################

### Added On 1 Sep 2007 ###
$i_studentAttendance_ImportFailed_Reason = "Error Reason";
###########################

### Added On 3 Sep 2007 ###
$attendst_leave_option = array(
array(0,"None"),
array(1,"Parent"),
array(2,"School Bus"),
array(3,"Self"),
);

$i_studentAttendance_ConfirmMsg = "<font color=red>Please Click Submit If The Following Records Are Correct</font>";
### End Of Student Leave School Option ###
##########################################

### Added On 4 Sep 2007 ###
$i_Staff_Patrol = "Teacher Patrol Recording System";

# eInventory words
$i_InventorySystem['eInventory'] = "eInventory (Asset Management)";
$i_InventorySystem['InventorySystemSetting'] = "eInventory (Asset Management) Settings";
$i_InventorySystem['AdminUserSetting'] = "Set Admin User";
$i_InventorySystem['AdminUser'] = "Admin User";
$i_InventorySystem['Inventory'] = "Inventory";
$i_InventorySystem['Category'] = "Category";
$i_InventorySystem['SubCategory'] = "Sub-category";
$i_InventorySystem['Location'] = "Location";
$i_InventorySystem['Caretaker'] = "Resource Management Group";
$i_InventorySystem['FundingSource'] = "Funding Source";
$i_InventorySystem['WriteOffReason'] = "Write-off Reason";
$i_InventorySystem['StockTake'] = "Stocktake";
$i_InventorySystem['Report'] = "Reports";
$i_InventorySystem['Settings'] = "Settings";
$i_InventorySystem['HidePictures'] = "Hide Pictures";
$i_InventorySystem['Others'] = "Others";
$i_InventorySystem['PageManagement'] = "Management";

### Added On 28 Sep 2007 ###
$i_InventorySystem_Select_Menu = "Please select from left side menu.";
$i_InventorySystem_Category_Code = "Category Code";
$i_InventorySystem_Category_Name = "Category Name";
$i_InventorySystem_Category_ChineseName = "Chinese Name";
$i_InventorySystem_Category_EnglishName = "English Name";
$i_InventorySystem_Category_DisplayOrder = "Display Order";
$i_InventorySystem_Category_Photo = "Photo";
$i_InventorySystem_Category2_License = "License";
$i_InventorySystem_Category2_Warranty = "Warranty";
$i_InventorySystem_Category2_Barcode = "Barcode";

### Added on 13 November 2007 ###
$i_InventorySystem_ReportType_Array = array(
array(1,"Item Detail"),
array(2,"Quantity Changed"),
array(3,"Warranty Expiry Date"),
array(4,"Current Quantity Level"));
$i_InventorySystem_ReportType = "Type";
$i_InventorySystem_Report_Warranty = "Warranty";
$i_InventorySystem_Report_QuantityLevel = "Current Quantity";
$i_InventorySystem_Report_ItemName = "Name";
$i_InventorySystem_Report_CurrentQty = "Current Quantity";
$i_InventorySystem_Report_InitialQty = "Initial Quantity";
$i_InventorySystem_Report_QtyDifference = "Quantity Difference";
$i_InventorySystem_Report_WarrantyExpiryDate = "Warranty Expiry Date";
$i_InventorySystem_Report_WarrantyExpied = "Expired";
$i_InventorySystem_Report_WarrantyNotExpied = "Not Expired";
$i_InventorySystem_Report_QtyMoreThan = "more than $eInventory_StockAlertLevel";
$i_InventorySystem_Report_QtyLessEqualTo = "less than or equal to $eInventory_StockAlertLevel";
$i_InventorySystem_Report_ItemDetails = "Item Details";

$i_InventorySystem_Action_Purchase = "Purchase";
$i_InventorySystem_Action_StockCheck = "Stocktake";

$i_InventorySystem_Action_UpdateStatus = "Update Status";
$i_InventorySystem_Action_ChangeLocation = "Change Location";
$i_InventorySystem_Action_ChangeLocationQty = "Change Quantity";
$i_InventorySystem_Action_MoveBackToOriginalLocation = "Move Back To Original Location";
$i_InventorySystem_Action_Stocktake_Original_Location = "Stocktake - Found in original location";
$i_InventorySystem_Action_Stocktake_Other_Location = "Stocktake - Found in other location";
$i_InventorySystem_Action_Stocktake_Not_Found = "Stocktake - Not found";
$i_InventorySystem_Action_Surplus_Ignore = "Ignore Surplus Quantity";
$i_InventorySystem_Action_Edit_Item = "Edit Item Details";
$i_InventorySystem_Action_WriteOff = "Write-off";

$i_InventorySystem_ItemStatus_Normal = "Normal";
$i_InventorySystem_ItemStatus_Repair = "Repairing";
$i_InventorySystem_ItemStatus_WriteOff = "Write-off";
$i_InventorySystem_ItemStatus_Damaged = "Damaged";

/*
$i_InventorySystem_Action_Array = array(
array(1,$i_InventorySystem_Action_Purchase),
array(2,$i_InventorySystem_Action_StockCheck),
array(3,$i_InventorySystem_Action_WriteOff),
array(4,$i_InventorySystem_Action_UpdateStatus),
array(5,$i_InventorySystem_Action_ChangeLocation),
array(6,$i_InventorySystem_Action_ChangeLocationQty),
array(7,$i_InventorySystem_Action_MoveBackToOriginalLocation)
);
*/
$i_InventorySystem_Action_Array = array(
array(1,$i_InventorySystem_Action_Purchase),
array(2,$i_InventorySystem_Action_Stocktake_Original_Location),
array(3,$i_InventorySystem_Action_Stocktake_Other_Location),
array(4,$i_InventorySystem_Action_Stocktake_Not_Found),
array(5,$i_InventorySystem_Action_WriteOff),
array(6,$i_InventorySystem_Action_UpdateStatus),
array(7,$i_InventorySystem_Action_ChangeLocation),
array(8,$i_InventorySystem_Action_ChangeLocationQty),
array(9,$i_InventorySystem_Action_MoveBackToOriginalLocation),
array(10,$i_InventorySystem_Action_Surplus_Ignore)
);


$i_InventorySystem_ItemType_Single = "Single Items";
$i_InventorySystem_ItemType_Bulk = "Bulk Items";

$i_InventorySystem_ItemType_Array = array(
array(1,$i_InventorySystem_ItemType_Single),
array(2,$i_InventorySystem_ItemType_Bulk));

$i_InventorySystem_Assigned = "Assigned";
$i_InventorySystem_Avanced_Search = "Advanced Search";
$i_InventorySystem_Simple_Search = "Simple Search";
$i_InventorySystem_Found = "Found";
$i_InventorySystem_Group_Member = "Member";
$i_InventorySystem_Import_Member_From = "Import Members From";
$i_InventorySystem_Loading = "Loading";
$i_InventorySystem_Location_Level = "Location";
$i_InventorySystem_Location = "Sub-location";

$i_InventorySystem_Keyword = "Keyword";
$i_InventorySystem_Optional = "Optional";
$i_InventorySystem_Item_NotFound = "Dislocated Item";
$i_InventorySystem_Item_NotFound_Display = "View Dislocated Item(s)";
$i_InventorySystem_Item_Name = "Item Name";
$i_InventorySystem_Item_Qty = "Quantity";
$i_InventorySystem_Item_Code = "Item Code";
$i_InventorySystem_Item_Barcode = "Barcode";
$i_InventorySystem_Item_Description = "Description";
$i_InventorySystem_Item_Ownership = "Ownership";
$i_InventorySystem_Item_Price = "Purchase Price (Invoice Total)";
$i_InventorySystem_Item_Detail = "Item Detail";
$i_InventorySystem_Item_History = "History";
$i_InventorySystem_Item_Invoice = "Invoice";
$i_InventorySystem_Item_Supplier_Name = "Supplier";
$i_InventorySystem_Item_Supplier_Contact = "Supplier's Contact";
$i_InventorySystem_Item_Supplier_Description = "Supplier Description";
$i_InventorySystem_Item_Purchase_Date = "Purchase Date";
$i_InventorySystem_Item_Purchase_Date2 = "Purchase Date";
$i_InventorySystem_Item_Quot_Num = "Quotation No.";
$i_InventorySystem_Item_Tender_Num = "Tender No.";
$i_InventorySystem_Item_Invoice_Num = "Invoice No.";
$i_InventorySystem_Item_Voucher_Num = "Voucher No.";
$i_InventorySystem_Item_Attachment = "Attachment";
$i_InventorySystem_Item_Brand_Name = "Brand";
$i_InventorySystem_Item_Warrany_Expiry = "Warranty Expiry Date";
$i_InventorySystem_Item_License_Type = "License";
$i_InventorySystem_Item_Record_Date = "Record Date";
$i_InventorySystem_Item_Action = "Action";
$i_InventorySystem_Item_Past_Status = "Last Status";
$i_InventorySystem_Item_New_Status = "New Status";
$i_InventorySystem_Item_Remark = "Remarks";
$i_InventorySystem_Item_PIC = "Person-in-charge";
$i_InventorySystem_Item_AssignTo = "Assign To";
$i_InventorySystem_Item_Stock_Qty = $i_InventorySystem['StockCheck']. " ".$i_InventorySystem_Item_Qty;
$i_InventorySystem_Item_ChineseName= "Item Name (Chinese)";
$i_InventorySystem_Item_EnglishName= "Item Name (English)";
$i_InventorySystem_Item_ChineseDescription = "Description (Chinese)";
$i_InventorySystem_Item_EnglishDescription = "Description (English)";
$i_InventorySystem_Item_Photo = "Photo";
$i_InventorySystem_Item_Location = "Location";
$i_InventorySystem_Item_Funding = "Funding Source";
$i_InventorySystem_Item_Barcode = "Barcode";
$i_InventorySystem_Item_License = "License";
$i_InventorySystem_Item_WarrantyExpiryDate = "Warranty Expiry Date";
$i_InventorySystem_Item_Serial_Num = "Serial Number";
$i_InventorySystem_Item_BulkItemAdmin = "Variance Manager";

$i_InventorySystem_Ownership_School = "School";
$i_InventorySystem_Ownership_Government = "Government";
$i_InventorySystem_Ownership_Donor = "Sponsoring body";

$i_InventorySystem_Location_Level_Name = "Location Name";
$i_InventorySystem_Location_Name = "Sub-location Name";
$i_InventorySystem_Group_Name = "Resource Management Group Name";
$i_InventorySystem_Group_MemberName = "Member Name";

$i_InventorySystem_ItemStatus_Array = array(
array(1,$i_InventorySystem_ItemStatus_Normal),
array(2,$i_InventorySystem_ItemStatus_Damaged),
array(3,$i_InventorySystem_ItemStatus_Repair)
);

$i_InventorySystem_ItemDetails = "Item Details";
$i_InventorySystem_PurchaseDetails = "Purchase Details";

# added on 30 Nov 2007

$i_InventorySystem_Item_CategoryCode = "Category Code";
$i_InventorySystem_Item_CategoryName = "Category Name";
$i_InventorySystem_Item_Category2Code = "Sub-category Code";
$i_InventorySystem_Item_LocationCode = "Location Code";
$i_InventorySystem_Item_Location2Code = "Sub-Location Code";
$i_InventorySystem_Item_GroupCode = "Resource Management Group Code";
$i_InventorySystem_Item_FundingSourceCode = "Funding Source Code";
$i_InventorySystem_Index = "Index";

$i_InventorySystem_Item_New = "New Item";
$i_InventorySystem_Item_Existing = "Existing Item";

# added on 5 Dec 2007 #
$i_InventorySystem_ImportInstruction_Title = "To use the CSV file";
$i_InventorySystem_ImportInstruction_Download = "Download";
$i_InventorySystem_ImportInstruction_InstallFontType = " and install the barcode font - IDAutomationHC39M (true type)";
$i_InventorySystem_ImportInstruction_ReportTemplate = " template(MS Word format)";
$i_InventorySystem_ImportInstruction_ImportCSVFile = "Customise template to extract required data from CSV file";
$i_InventorySystem_ImportInstruction_PrintReport = "Print Barcode";
$i_InventorySystem_Category_CustomPhoto = "Customize";

# addded on 6 Dec 2007 #
$i_InventorySystem_Setting_StockCheckPeriod = "Stocktake Period";
$i_InventorySystem_Setting_StockCheckStart = "From";
$i_InventorySystem_Setting_StockCheckEnd = "To";
$i_InventorySystem_Setting_WarrantyExpiryWarning = "Warranty Expiry Reminder";
$i_InventorySystem_Setting_WarrantyExpiryWarningDayPeriod = "Day(s) Before Expire";
$i_InventorySystem_Report_WarrantyExpiry = "Warranty Expiry";
$i_InventorySystem_Report_ItemNotStockCheck = "Stocktake Pending";

# added on 10 Dec 2007 #
$i_InventorySystem_StockAssigned = "Quantity (Assigned)";
$i_InventorySystem_StockFound = "Quantity (Found)";
$i_InventorySystem_Input_Category_Code_Warning = "Please Input The Code";
$i_InventorySystem_Input_Category_Item_ChineseName_Warning = "Please Input The Chinese Name";
$i_InventorySystem_Input_Category_Item_EnglishName_Warning = "Please Input The English Name";

# added on 11 Dec 2007 #
$i_InventorySystem_Import_CheckItemCode = "Click here to check ".$i_InventorySystem_Item_Code;
$i_InventorySystem_Import_CheckCategory = "Click here to check ".$i_InventorySystem_Item_CategoryCode;
$i_InventorySystem_Import_CheckCategory2 = "Click here to check ".$i_InventorySystem_Item_Category2Code;
$i_InventorySystem_Import_CheckGroup = "Click here to check ".$i_InventorySystem_Item_GroupCode;
$i_InventorySystem_Import_CheckLocation = "Click here to check ".$i_InventorySystem_Item_LocationCode;
$i_InventorySystem_Import_CheckLocation2 = "Click here to check ".$i_InventorySystem_Item_Location2Code;
$i_InventorySystem_Import_CheckFunding = "Click here to check ".$i_InventorySystem_Item_FundingSourceCode;
$i_InventorySystem_Import_CorrectSingleItemCodeReminder = " * The above Item Codes are in use. Please use new Item Codes to import.";
$i_InventorySystem_Import_CorrectBulkItemCodeReminder = " * The above Item Codes already exist. The qantity of their cooresponding items will be increased if existing Item Codes are imported.";
$i_InventorySystem_Import_CorrectFundingIndexReminder = " * Please make sure the above Funding Source Codes are inputted correctly in your csv import file.";
$i_InventorySystem_Import_CorrectGroupIndexReminder = " * Please make sure the above Resource Management Group Codes are inputted correctly in your csv import file.";
$i_InventorySystem_Import_CorrecrCategory2IndexReminder = " * Please make sure the above Sub-Category Codes are inputted correctly in your csv import file.";
$i_InventorySystem_Import_CorrectLocationIndexReminder = " * Please make sure the above Sub-Location Codes are inputted correctly in your csv import file.";
$i_InventorySystem_Import_CorrectLocation2IndexReminder = " * Please use the correct ".$i_InventorySystem_Item_Location2Code." to import file";
$i_InventorySystem_StockCheck_QtyDifferent = "Quantity Different";
$i_InventorySystem_StockCheck_ValidQuantityWarning = "Please input a valid quantity";
$i_InventorySystem_StockCheck_EmptyQuantityWarning = "Please input quantity";
$i_InventorySystem_StockCheck_Found = "Found";
$i_InventorySystem_StockCheck_LastStatus = "Last Status";
$i_InventorySystem_StockCheck_NewStatus = "New Status";
$i_InventorySystem_Import_CorrectCategoryCodeWarning = " * The above Category Codes are in use. Please use new Category Codes to import.";
$i_InventorySystem_Import_CorrecrCategory2CodeWarning = " * The above Sub-Category Codes are in use. Please use new Sub-Category Codes to import.";
$i_InventorySystem_Import_CorrectLocationCodeWarning = " * The above Location Codes are in use. Please use new Location Codes to import.";
$i_InventorySystem_Import_CorrectLocation2CodeWarning = " * The above Sub-Location Codes are in use. Please use new Sub-Location Codes to import.";
$i_InventorySystem_Import_CorrectFundingCodeWarnging = " * The above Funding Source Codes are in use. Please use new Funding Source Codes to import.";

# added on 12 Dec 2007 #
$i_InventorySystem_Item_Registration = "Add New Item";
$i_InventorySystem_Input_Item_Code_Warning = "Please input item code";
$i_InventorySystem_Input_Item_Code_Length_Warning = "Item Code cannot longer than 10 characters";
$i_InventorySystem_Input_Item_GroupSelection_Warning = "Please select a resource management group";
$i_InventorySystem_Input_Item_LocationSelection_Warning = "Please select the location";
$i_InventorySystem_Input_Item_FundingSelection_Warning = "Please select the funding source";
$i_InventorySystem_Input_Item_PurchaseDate_Warning = "Please input a correct purchase date";
$i_InventorySystem_Input_Item_WarrantyExpiryDate_Warning = "Please input a correct warranty expiry date";
$i_InventorySystem_Input_Item_Barcode_Warning = "Please input/generate barcode";

# added on 14 Dec 2007 #
$i_InventorySystem_StockCheck_Step1 = "Select Stocktake Location";
$i_InventorySystem_StockCheck_Step2 = "Begin Stocktake";
$i_InventorySystem_StockCheck_Step3 = "Confirm Stocktake Result";
$i_InventorySystem_Setting_WarrantyGiveWarning1 = "Give reminder ";
$i_InventorySystem_Setting_WarrantyGiveWarning2 = " day(s) before expiry";
$i_InventorySystem['BasicSettings'] = "Basic Settings";
$i_InventorySystem['FullList'] = "Full List";
$i_InventorySystem['View'] = "View";
$i_InventorySystem_Quantity_StockChecked = "Quantity (Stock Checked)";
$i_InventorySystem_PhotoGuide = "Photo must be in '.JPG', '.GIF', or '.PNG' format and the size should be fixed to 100 x 100 pixel (W x H).";
$i_InventorySystem_PurchasePriceGuide = "Purchase Price is the Total Cost of this Item.";
$i_InventorySystem_ImportCaretaker_Warning = "Only teachers & staffs in the selected group will be imported";

# added on 27 Dec 2007 #
$i_InventorySystem_Setting_BarcodeMaxLength1 = "Barcode maximun length";
$i_InventorySystem_Setting_BarcodeMaxLength2 = "digit(s)";
$i_InventorySystem_Setting_BarcodeFormat = "Barcode format";
$i_InventorySystem_Setting_BarcodeFormat_NumOnly = "Use numbers, special characters ('-','$','|','.','/') and space";
$i_InventorySystem_Setting_BarcodeFormat_BothNumAndChar = "Use numbers (0-9), upper case alphabets (A-Z), special characters ('-','$','|','.','/') and space";
$i_InventorySystem_ItemWarrantyExpiryWarning = "The following items' warranty is going to expire";

# added on 28 Dec 2007 #
$i_InventorySystem_WriteOffItem = "Write-off Item";
$i_InventorySystem_WriteOffQty = "Quantity (Write-off)";

# added on 2 Jan 2008 #
$i_InventorySystem_Input_Item_PurchasePrice_Warning = "Please input a valid purchasing price";

# added on 3 Jan 2008 #
$i_InventorySystem_Input_Item_Generate_Item_Code = "Generate Item Code";
$i_InventorySystem_Input_Item_Generate_Item_Barcode = "Generate Barcode";

# added on 4 Jan 2008 #
$i_InventorySystem_Report_ItemFinishStockCheck = "Stocktake";

# added on 8 Jan 2008 #
$i_InventorySystem_Search_Result = "Search Result";
$i_InventorySystem_ViewItem_All_Location_Level = "All Locations";
$i_InventorySystem_ViewItem_All_Resource_Management_Group = "All Groups";
$i_InventorySystem_ViewItem_All_Finding_Source = "All Fundings";
$i_InventorySystem_Item_Update_Status = "Update Status";

# added on 9 Jan 2008 #
$i_InventorySystem_Location_Code = "Sub-location Code";
$i_InventorySystem_Location_Level_Code = "Location Code";
$i_InventorySystem_Caretaker_Code = "Group Code";
$i_InventorySystem_Funding_Code = "Funding Code";

# added on 10 Jan 2008 #
$i_InventorySystem_WriteOffReason_Name = "Write-off Reason";

# added on 10 Jan 2008 #
$i_InventorySystem_RequestWriteOff = "Request Write-off";

# added on 15 Jan 2008 #
$i_InventorySystem_StockTake_SelectItemStatus_Warning = "Please select the single item(s) status";

# added on 16 Jan 2008 #
$i_InventorySystem_ItemStatus_Lost = "Lost";
$i_InventorySystem_ItemStatus_Surplus = "Surplus";
$i_InventorySystem_Btn_Generate = "Generate";
$i_InventorySystem_Unit_Price = "Unit Price";
$i_InventorySystem_Group_MemberPosition = "Role";
$i_InventorySystem_Group_MemberPosition_Leader = "Leader";
$i_InventorySystem_Group_MemberPosition_Member = "Member";
$i_InventorySystem_Group_Member_LeaderRemark = "Group Leader";
$i_InventorySystemItemStatus = "Item Status";
$i_InventorySystemItemStatusWriteOff = "Written-off";
$i_InventorySystemItemStatusNonWriteOff = "Not Written-off";
$i_InventorySystemItemStatusAll = "All";
$i_InventorySystemItemMaintainInfo = "Maintenance Details";
$i_InventorySystem_Approval = "eInventory Approval";
$i_InventorySystem_WriteOffItemApproval = "Write-off Approval";

$i_InventorySystem_Error = "Error";


# added on 17 Jan 2008 #
$i_InventorySystem_Input_Item_ExistingItemCode = "Existing Item Code";

# added on 18 Jan 2008 #
$i_InventorySystem_Input_Item_Barcode_Exist_Warning = "The barcode input is already used. Please input another one.";
$i_InventorySystem_Input_Category_Item_Name_Exist_Warning = "Category name is already used. Please input another name.";
$i_InventorySystem_Input_Category_Code_Exist_Warning = "Category code already in use. Please input another code.";
$i_InventorySystem_Last_Stock_Take_Time = "Last Stocktake On";
$i_InventorySystem_Write_Off_RequestTime = "Request Date";
$i_InventorySystem_Write_Off_RequestQty = "Request Quantity";
$i_InventorySystem_Write_Off_Reason = "Write-off Reason";

# added on 21 Jan 2008 #
$i_InventorySystem_Export_Select_Column = "Select item detail(s) to be shown:";

# added on 22 Jan 2008 #
$i_InventorySystem_Report_ItemWrittenOff = "Written-off Items";

# added on 31 Jan 2008 #
$i_InventorySystem_StocktakeVarianceHandling = "Stocktake Variance Handling";

# added on 4 Feb 2008 #
$i_InventorySystem_Resource_Management_GroupLeader = "Group Leader";
$i_InventorySystem_SetAsGroupLeader = "Set as Leader";
$i_InventorySystem_SetAsGroupMember = "Set as Member";
$i_InventorySystem_Report_FixedAssetsRegister = "Fixed Assets Register";
$i_InventorySystem_Report_ItemStatus = "Item Status";
$i_InventorySystem_Input_Category_Code_RegExp_Warning = "Category code is not valid.";
$i_InventorySystem_Input_SubCategory_Code_RegExp_Warning = "Sub-category code is not valid.";
$i_InventorySystem_Input_SubCategory_Item_Name_Exist_Warning = "Sub-category name is already used. Please input another name.";
$i_InventorySystem_Input_SubCategory_Code_Exist_Warning = "Sub-category code already in use. Please input another code.";
$i_InventorySystem_Input_LocationLevel_Code_RegExp_Warning = "Location code is not valid.";
$i_InventorySystem_Input_LocationLevel_Name_Exist_Warning = "Location name is already used. Please input another name.";
$i_InventorySystem_Input_LocationLevel_Code_Exist_Warning = "Location code already in use. Please input another code.";
$i_InventorySystem_Input_Location_Code_RegExp_Warning = "Sub-location code is not valid.";
$i_InventorySystem_Input_Location_Name_Exist_Warning = "Sub-location name is already used. Please input another name.";
$i_InventorySystem_Input_Location_Code_Exist_Warning = "Sub-location code already in use. Please input another code.";
$i_InventorySystem_Input_Group_Code_RegExp_Warning = "Resource management group code is not valid.";
$i_InventorySystem_Input_Group_Name_Exist_Warning = "Resource management group name is already used. Please input another name.";
$i_InventorySystem_Input_Group_Code_Exist_Warning = "Resource management group code already in use. Please input another code.";
$i_InventorySystem_Input_Funding_Code_RegExp_Warning = "Funding source code is not valid.";
$i_InventorySystem_Input_Funding_Name_Exist_Warning = "Funding source name is already used. Please input another name.";
$i_InventorySystem_Input_Funding_Code_Exist_Warning = "Funding source code already in use. Please input another code.";

# added on 6 Feb 2008 #
$i_InventorySystem_EditItem_NoEditRightWarning = "You cannot edit this item";
$i_InventorySystem_Report_Col_Item = "Item";
$i_InventorySystem_Report_Col_Cost = "Cost";
$i_InventorySystem_Report_Col_Description = "Description";
$i_InventorySystem_Report_Col_Purchase_Date = "Date of Purchase";
$i_InventorySystem_Report_Col_Govt_Fund = "Govt. Fund";
$i_InventorySystem_Report_Col_Sch_Fund = "School Fund";
$i_InventorySystem_Report_Col_Sch_Unit_Price = "Unit Price";
$i_InventorySystem_Report_Col_Sch_Purchase_Price = "Purchase Price";
$i_InventorySystem_Report_Col_Quantity = "Quantity";
$i_InventorySystem_Report_Col_Location = "Location";
$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off = "Date & Reason of Write-off";
$i_InventorySystem_Report_Col_Signature = "Supervisor's / Principal's Signature";
$i_InventorySystem_Report_Col_Remarks = "Remarks";

# added on 11 Feb 2008 #
$i_InventorySystem_Input_Item_ItemPrice_Warning = "Please input a valid unit price";

# added on 12 Feb 2008 #
$i_InventorySystem_StockTakeResult = "Stocktake Result";

# added on 13 Feb 2008 #
$i_InventorySystem_FileFormat = "File Format";
$i_InventorySystem_ViewItem_All_Category = "All Categories";
$i_InventorySystem_Setting_BarcodeSetting = "Barcode Format";
$i_InventorySystem_Input_ItemCode_Exist_Warning = "Item code already in use. Please input another code.";

# added on 14 Feb 2008 #
$i_InventorySystem_MissingQty = "Missing Quantity";
$i_InventorySystem_SurplusQty = "Surplus Quantity";
$i_InventorySystem_TotalMissingQty = "Total Missing Quantity";
$i_InventorySystem_TotalSurplusQty = "Total Surplus Quantity";
$i_InventorySystem_RecordDate = "Record Date";
$i_InventorySystem_RequestPerson = "Requestor";
$i_InventorySystem_ApprovePerson = "Approver";

# added on 15 Feb 2008 #
$i_InventorySystem_ApproveDate = "Approve Date";
$i_InventorySystem_Write_Off_Attachment = "Write-off Attachment";
$i_InventorySystem_Item_UpdateStatus = "Update Item Status";
$i_InventorySystem_Item_UpdateLocation = "Update Item Location";
$i_InventorySystem_Item_RequestWriteOff = "Request Write-off";

# added on 19 Feb 2008 #
$i_InventorySystem_Search_Purchase_Price_Range = "Total Purchase Amount Between";
$i_InventorySystem_Search_Unit_Price_Range = "Unit Price Between";
$i_InventorySystem_From = "From";
$i_InventorySystem_To = "To";
$i_InventorySystem_BarcodeLabels = "Barcode Labels";
$i_InventorySystem_Report_Stocktake = "Stocktake";
$i_InventorySystem_NumOfItemAdd = "Quantity";
$i_InventorySystem_AddNewItem_Step1 = "Enter Basic Item Information";
$i_InventorySystem_AddNewItem_Step2 = "Enter Details for Individual Items";
$i_InventorySystem_Report_Stocktake_Progress = "Stocktake Progress";
$i_InventorySystem_Stocktake_ProgressFinished = "Done";
$i_InventorySystem_Stocktake_ProgressNotFinished = "Not Done";
$i_InventorySystem_VarianceHandling_Qty = "Quantity";

# addded on 20 Feb 2008 #
$i_InventorySystem_ViewItem_All_Category = "All Categories";
$i_InventorySystem_DisplayAllImage = "Display Image";
$i_InventorySystem_Setting_StockCheckDateRange = "Select Date Range";
$i_InventorySystem_Funding_Type = "Funding Type";
$i_InventorySystem_Funding_Type_School = "School";
$i_InventorySystem_Funding_Type_Government = "Government";
$i_InventorySystem_Funding_Type_Array = array(
array(1,$i_InventorySystem_Funding_Type_School),
array(2,$i_InventorySystem_Funding_Type_Government));
$i_InventorySystem_ItemType_All = "All Types";
$i_InventorySystem_HideAllImage = "Hide Image";
$i_InventorySystem_Stocktake_List = "Stocktake List";
$i_InventorySystem_Stocktake_ReadBarcode = "Enter or scan item barcode";
$i_InventorySystem_Stocktake_LastStocktakeBy = "Last Stocktake By";
$i_InventorySystem_Status = "Status";
$i_InventorySystem_Stocktake_TotalQty = "Total";
$i_InventorySystem_Stocktake_FoundQty = "Found";
$i_InventorySystem_Stocktake_UnlistedQty = "Unlisted";
$i_InventorySystem_Stocktake_ExpectedQty = "Expected Quantity";
$i_InventorySystem_Stocktake_StocktakeQty = "Stocktake Quantity";
$i_InventorySystem_Stocktake_NormalQty = "Normal";
$i_InventorySystem_Stocktake_RequestWriteOffQty = "Request Write-off";
$i_InventorySystem_Stocktake_VarianceQty = "Variance";
$i_InventorySystem_Stocktake_MissingRemark = "Item Missing";
$i_InventorySystem_Stocktake_UnlistedItem = "Unlisted Item";
$i_InventorySystem_Stocktake_ExpectedLocation = "Expected Location";
$i_InventorySystem_Stocktake_Dislocated_Item = "Dislocated item";
$i_InventorySystem_Stocktake_ItemNotExist = "Item does not exist";
$i_InventorySystem_Stocktake_ReasonAndAttachment = "Reason And Attachment";
$i_InventorySystem_VarianceHandling_ChangeLocation = "Change Location";
$i_InventorySystem_Report_StocktakeProgress = "Stocktake Progress";

$i_InventorySystem_CategoryImport_Format1_Row1 = "Column 1 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_CategoryImport_Format1_Row2 = "Column 2 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_CategoryImport_Format1_Row3 = "Column 3 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_CategoryImport_Format1_Row4 = "Column 4 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_Category2Import_Format1_Row1 = "Column 1 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_Category2Import_Format1_Row2 = "Column 2 : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_Category2Import_Format1_Row3 = "Column 3 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_Category2Import_Format1_Row4 = "Column 4 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_Category2Import_Format1_Row8 = "Column 8 : $i_InventorySystem_Category_DisplayOrder";
$i_InventorySystem_Category2Import_Format1_Row5 = "Column 5 : $i_InventorySystem_Category2_License (1 - Applicable, 0 - Not applicable)";
$i_InventorySystem_Category2Import_Format1_Row6 = "Column 6 : $i_InventorySystem_Category2_Warranty (1 - Applicable, 0 - Not applicable)";

$i_InventorySystem_LocationImport_Format1_Row1 = "Column 1 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_LocationImport_Format1_Row2 = "Column 2 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_LocationImport_Format1_Row3 = "Column 3 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_LocationImport_Format1_Row4 = "Column 4 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_Location2Import_Format1_Row1 = "Column 1 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_Location2Import_Format1_Row2 = "Column 2 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_Location2Import_Format1_Row3 = "Column 3 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_Location2Import_Format1_Row4 = "Column 4 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_Location2Import_Format1_Row5 = "Column 5 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_FundingImport_Format1_Row1 = "Column 1 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_FundingImport_Format1_Row2 = "Column 2 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_FundingImport_Format1_Row3 = "Column 3 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_FundingImport_Format1_Row4 = "Column 4 : $i_InventorySystem_Funding_Type (e.g School, Government)";
$i_InventorySystem_FundingImport_Format1_Row5 = "Column 5 : $i_InventorySystem_Category_DisplayOrder";


$i_InventorySystem_CategoryImportError[1] = "Category Code already exists";
$i_InventorySystem_CategoryImportError[2] = "Category Code is empty";
$i_InventorySystem_CategoryImportError[3] = "Item Chinese Name is empty";
$i_InventorySystem_CategoryImportError[4] = "Item English Name is empty";
$i_InventorySystem_CategoryImportError[5] = "Display Order is empty";
$i_InventorySystem_CategoryImportError[19] = "Category Code already used";
$i_InventorySystem_CategoryImportError[20] = "Category Code length should within 5 characters";

$i_InventorySystem_Category2ImportError[1] = "Category Code does not exist";
$i_InventorySystem_Category2ImportError[2] = "Category Code is empty";
$i_InventorySystem_Category2ImportError[3] = "Sub-Category Code is empty";
$i_InventorySystem_Category2ImportError[4] = "Chinese Name is empty";
$i_InventorySystem_Category2ImportError[5] = "English Name is empty";
$i_InventorySystem_Category2ImportError[6] = "License is empty";
$i_InventorySystem_Category2ImportError[7] = "Warranty is empty";
$i_InventorySystem_Category2ImportError[8] = "Display Order is empty";
$i_InventorySystem_Category2ImportError[9] = "Sub-Category already exists";
$i_InventorySystem_Category2ImportError[19] = "Sub-Category Code already used";
$i_InventorySystem_Category2ImportError[22] = "Sub-Category Code length should within 5 characters";

$i_InventorySystem_LocationLevelImportError[1] = "Location Code already exists";
$i_InventorySystem_LocationLevelImportError[2] = "Location Code is empty";
$i_InventorySystem_LocationLevelImportError[3] = "Location Chinese Name is empty";
$i_InventorySystem_LocationLevelImportError[4] = "Location English Name is empty";
$i_InventorySystem_LocationLevelImportError[5] = "Display Order is empty";
$i_InventorySystem_LocationLevelImportError[19] = "Location Code already used";

$i_InventorySystem_LocationImportError[1] = "Location Code does not exist";
$i_InventorySystem_LocationImportError[2] = "Sub-Location Code already exists";
$i_InventorySystem_LocationImportError[3] = "Location Code is empty";
$i_InventorySystem_LocationImportError[4] = "Location Chinese Name is empty";
$i_InventorySystem_LocationImportError[5] = "Location English Name is empty";
$i_InventorySystem_LocationImportError[6] = "Display Order is empty";
$i_InventorySystem_LocationImportError[19] = "Location Code already used";
$i_InventorySystem_LocationImportError[9] = "Sub-Location Code already exists";

$i_InventorySystem_FundingImportError[1] = "Funding Code already exists";
$i_InventorySystem_FundingImportError[2] = "Funding Code is empty";
$i_InventorySystem_FundingImportError[3] = "Chinese Name is empty";
$i_InventorySystem_FundingImportError[4] = "English Name is empty";
$i_InventorySystem_FundingImportError[5] = "Funding Type is empty";
$i_InventorySystem_FundingImportError[6] = "Display Order is empty";
$i_InventorySystem_FundingImportError[7] = "Funding Type does not exist";
$i_InventorySystem_FundingImportError[8] = "Display Order is not valid";
$i_InventorySystem_FundingImportError[19] = "Funding Code already used";

# Added on 4 Mar 2008 #
$i_InventorySystem_VarianceHandling_Action[1] = "Move to original location";
$i_InventorySystem_VarianceHandling_Action[2] = "Reassign to new location";
$i_InventorySystem_VarianceHandling_Action[3] = "Request write-off";
$i_InventorySystem_VarianceHandling_Action[4] = "Add as new item";
$i_InventorySystem_VarianceHandling_Action[5] = "Ignore";

# Added on 6 Mar 2008 #
$i_InventorySystem_Report_Stocktake_groupby = "Group By";

# Added on 7 Mar 2008 #
$i_InventorySystem['item_type'] = "Item Type";

# added on 10 Mar 2008 #
$i_InventorySystem_WriteOff_Reason_LostItem = "Not found"; #"Lost while stocktaking";
$i_InventorySystem_VarianceHandling_AddAsNewItemQty = "Add as new item quantity";
$i_InventorySystem_VarianceHandling_IgnoreQty = "Ignore quantity";
# added on 12 Mar 2008 #
$i_InventorySystem['jsLocationCheckBox'] = "Please select a location";
$i_InventorySystem['jsGroupCheckBox'] = "Please select a group";
$i_InventorySystem['jsLocationGroupCheckBox'] = "Please select a location/group";

# added on 13 Mar 2008 #
$i_InventorySystem['VarianceHandlingNotice'] = "Variance Handling Notice";
$i_InventorySystem_VarianceHandlingNotice_CurrentLocation = "Currnet Location";
$i_InventorySystem_VarianceHandlingNotice_NewLocation = "New Location";
$i_InventorySystem_VarianceHandlingNotice_Sender = "Sender";
$i_InventorySystem_VarianceHandlingNotice_Handler = "Handler";
$i_InventorySystem_VarianceHandlingNotice_SendDate = "Sent Date";

# added on 14 Mar 2008 #
$i_InventorySystem_BackToFullList = "Back to full list";

# added on 17 Mar 2008 #
$i_InventorySystem_SubCategory_Code = "Sub-category Code";
$i_InventorySystem_SubCategory_Name = "Sub-category Name";
$i_InventorySystem_Setting_Location_ChineseName = "Location Name (Chinese)";
$i_InventorySystem_Setting_Location_EnglishName = "Location Name (English)";
$i_InventorySystem_Setting_SubLocation_ChineseName = "Sub-location Name (Chinese)";
$i_InventorySystem_Setting_SubLocation_EnglishName = "Sub-location Name (English)";
$i_InventorySystem_Setting_NewLocation = "New Location";
$i_InventorySystem_Setting_NewSubLocation  = "New Sub-location";
$i_InventorySystem_Setting_EditLocation = "Edit Location";
$i_InventorySystem_Setting_EditSubLocation  = "Edit Sub-location";
$i_InventorySystem_Setting_NewManagementGroup = "New Resource Management Group";
$i_InventorySystem_Setting_EditManagementGroup = "Edit Resource Management Group";
$i_InventorySystem_Setting_ManagementGroup_ChineseName = "Group Name (Chinese)";
$i_InventorySystem_Setting_ManagementGroup_EnglishName = "Group Name (English)";
$i_InventorySystem_Setting_ManagementGroup_NewMember = "New Member";
$i_InventorySystem_Setting_ManagementGroup_SelectUser = "Select User(s)";
$i_InventorySystem_Setting_Funding_Name = "Funding Name";
$i_InventorySystem_Setting_NewFunding = "New Funding Source";
$i_InventorySystem_Setting_EditFunding = "Edit Funding Source";
$i_InventorySystem_Setting_Funding_ChineseName = "Funding Name (Chinese)";
$i_InventorySystem_Setting_Funding_EnglishName = "Funding Name (English)";
$i_InventorySystem_Setting_NewWriteOffReason = "New Write-off Reason";
$i_InventorySystem_Setting_EditWriteOffReason = "Edit Write-off Reason";
$i_InventorySystem_Setting_WriteOffReason_ChineseName = "Reason (Chinese)";
$i_InventorySystem_Setting_WriteOffReason_EnglishName = "Reason (English)";
$i_InventorySystem_Search_Warrany_Expiry_Between = "Warranty Expires Between";
$i_InventorySystem_Search_And = "and";
$i_InventorySystem_Search_And2 = "and";
$i_InventorySystem_Search_Purchase_Date_Between = "Purchased Between";

# added on 20 Mar 2008 #
$i_InventorySystem_NewItem_PurchasePrice_Help = " <p>\"Total Purchase Amount\" may not be equal to \"Unit Price\" x \"Quantity\" if there is a discount involved.</p><p>It is usually the total amount listed on the invoice and is the amount you pay for the entire purchase.</p><p>This amount will be included in Fixed Assets Register's \"Purchase Price\" column.</p>";
$i_InventorySystem_NewItem_UnitPrice_Help = "\"Unit Price\" is the list price of the item and is multiplied by \"Quantity\" to generate the amount shown in Fixed Assets Register's \"Cost\" column.";
$i_InventorySystem_MultiSelect_Help = "To make multiple selections, hold the CTRL key while you click on the items.";

# added on 28 Mar 2008 #
$i_InventorySystem_VarianceHandling_New_Quantity = "New Quantity";
$i_InventorySystem_VarianceHandling_Original_Quantity = "Original Quantity";

# added on 8 Apr 2008 #
$i_InventorySystem_Send_Reminder = "Send Reminder";

# added on 15 Apr 2008 #
$i_InventorySystem_ImportItem_Format1_Title = "<span class=extraInfo>Import Single Items</span><Br><Br>";
$i_InventorySystem_ImportItem_Format1_Row1 = "Column 1 : $i_InventorySystem_Item_Code";
$i_InventorySystem_ImportItem_Format1_Row2 = "Column 2 : $i_InventorySystem_Item_Barcode";
$i_InventorySystem_ImportItem_Format1_Row3 = "Column 3 : $i_InventorySystem_Item_ChineseName";
$i_InventorySystem_ImportItem_Format1_Row4 = "Column 4 : $i_InventorySystem_Item_EnglishName";
$i_InventorySystem_ImportItem_Format1_Row5 = "Column 5 : $i_InventorySystem_Item_ChineseDescription";
$i_InventorySystem_ImportItem_Format1_Row6 = "Column 6 : $i_InventorySystem_Item_EnglishDescription";
$i_InventorySystem_ImportItem_Format1_Row7 = "Column 7 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_ImportItem_Format1_Row8 = "Column 8 : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_ImportItem_Format1_Row9 = "Column 9 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_ImportItem_Format1_Row10 = "Column 10 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_ImportItem_Format1_Row11 = "Column 11 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_ImportItem_Format1_Row12 = "Column 12 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_ImportItem_Format1_Row13 = "Column 13 : $i_InventorySystem_Item_Ownership (1 - School, 2 - Government, 3 - Donor)";
$i_InventorySystem_ImportItem_Format1_Row14 = "Column 14 : $i_InventorySystem_Item_Warrany_Expiry (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format1_Row15 = "Column 15 : $i_InventorySystem_Item_License";
$i_InventorySystem_ImportItem_Format1_Row16 = "Column 16 : $i_InventorySystem_Item_Serial_Num";
$i_InventorySystem_ImportItem_Format1_Row17 = "Column 17 : $i_InventorySystem_Item_Brand_Name";
$i_InventorySystem_ImportItem_Format1_Row18 = "Column 18 : $i_InventorySystem_Item_Supplier_Name";
$i_InventorySystem_ImportItem_Format1_Row19 = "Column 19 : $i_InventorySystem_Item_Supplier_Contact";
$i_InventorySystem_ImportItem_Format1_Row20 = "Column 20 : $i_InventorySystem_Item_Supplier_Description";
$i_InventorySystem_ImportItem_Format1_Row21 = "Column 21 : $i_InventorySystem_Item_Quot_Num";
$i_InventorySystem_ImportItem_Format1_Row22 = "Column 22 : $i_InventorySystem_Item_Tender_Num";
$i_InventorySystem_ImportItem_Format1_Row23 = "Column 23 : $i_InventorySystem_Item_Invoice_Num";
$i_InventorySystem_ImportItem_Format1_Row24 = "Column 24 : $i_InventorySystem_Item_Purchase_Date (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format1_Row25 = "Column 25 : $i_InventorySystem_Item_Price";
$i_InventorySystem_ImportItem_Format1_Row26 = "Column 26 : $i_InventorySystem_Unit_Price";
$i_InventorySystem_ImportItem_Format1_Row27 = "Column 27 : $i_InventorySystemItemMaintainInfo";
$i_InventorySystem_ImportItem_Format1_Row28 = "Column 28 : $i_InventorySystem_Item_Remark";

$i_InventorySystem_ImportItem_Format2_Title = "<span class=extraInfo>Import Bulk Items</span><Br><Br>";
$i_InventorySystem_ImportItem_Format2_Row1 = "Column 1 : $i_InventorySystem_Item_Code";
$i_InventorySystem_ImportItem_Format2_Row2 = "Column 2 : $i_InventorySystem_Item_ChineseName";
$i_InventorySystem_ImportItem_Format2_Row3 = "Column 3 : $i_InventorySystem_Item_EnglishName";
$i_InventorySystem_ImportItem_Format2_Row4 = "Column 4 : $i_InventorySystem_Item_ChineseDescription";
$i_InventorySystem_ImportItem_Format2_Row5 = "Column 5 : $i_InventorySystem_Item_EnglishDescription";
$i_InventorySystem_ImportItem_Format2_Row6 = "Column 6 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_ImportItem_Format2_Row7 = "Column 7 : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_ImportItem_Format2_Row8 = "Column 8 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_ImportItem_Format2_Row9 = "Column 9 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_ImportItem_Format2_Row10 = "Column 10 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_ImportItem_Format2_Row11 = "Column 11 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_ImportItem_Format2_Row12 = "Column 12 : $i_InventorySystem_Item_Ownership (1 - School, 2 - Government, 3 - Donor)";
$i_InventorySystem_ImportItem_Format2_Row13 = "Column 13 : $i_InventorySystem_Item_BulkItemAdmin";
$i_InventorySystem_ImportItem_Format2_Row14 = "Column 14 : $i_InventorySystem_Item_Qty";
$i_InventorySystem_ImportItem_Format2_Row15 = "Column 15 : $i_InventorySystem_Item_Supplier_Name";
$i_InventorySystem_ImportItem_Format2_Row16 = "Column 16 : $i_InventorySystem_Item_Supplier_Contact";
$i_InventorySystem_ImportItem_Format2_Row17 = "Column 17 : $i_InventorySystem_Item_Supplier_Description";
$i_InventorySystem_ImportItem_Format2_Row18 = "Column 18 : $i_InventorySystem_Item_Quot_Num";
$i_InventorySystem_ImportItem_Format2_Row19 = "Column 19 : $i_InventorySystem_Item_Tender_Num";
$i_InventorySystem_ImportItem_Format2_Row20 = "Column 20 : $i_InventorySystem_Item_Invoice_Num";
$i_InventorySystem_ImportItem_Format2_Row21 = "Column 21 : $i_InventorySystem_Item_Purchase_Date (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format2_Row22 = "Column 22 : $i_InventorySystem_Item_Price";
$i_InventorySystem_ImportItem_Format2_Row23 = "Column 23 : $i_InventorySystem_Unit_Price";
$i_InventorySystem_ImportItem_Format2_Row24 = "Column 24 : $i_InventorySystemItemMaintainInfo";
$i_InventorySystem_ImportItem_Format2_Row25 = "Column 25 : $i_InventorySystem_Item_Remark";

# added on 16 Apr 2008 #
$i_InventorySystem_ImportItem_RowWithRecord = "Row(s) with record";
$i_InventorySystem_ImportItem_TotalRow = "Total row(s)";
$i_InventorySystem_ImportItem_EmptyRowWarning = "CSV with empty row(s) is not accepted";
$i_InventorySystem_ImportItem_EmptyRowRecord = "Empty row(s)";

# added on 17 Apr 2008 #
$i_InventorySystem_UpdateSingleItemStatusWarning = "Please select the status";
$i_InventorySystem_UpdateBulkItemStatusWarning_Normal = "Please input valid normal quantity";
$i_InventorySystem_UpdateBulkItemStatusWarning_Damaged = "Please input valid damaged quantity";
$i_InventorySystem_UpdateBulkItemStatusWarning_Reparing = "Please input valid reparing quantity";
$i_InventorySystem_UpdateItemRequestWriteOffQty_Warning = "Please input valid request write-off quantity";
$i_InventorySystem_UpdateItemRequestWriteOffReason_Warning = "Please select the write-off reason";

# added on 18 Apr 2008 #
$i_InventorySystem_GroupImport_Format1_Row1 = "Column 1 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_GroupImport_Format1_Row2 = "Column 2 : $i_InventorySystem_Setting_ManagementGroup_ChineseName";
$i_InventorySystem_GroupImport_Format1_Row3 = "Column 3 : $i_InventorySystem_Setting_ManagementGroup_EnglishName";
$i_InventorySystem_GroupImport_Format1_Row4 = "Column 4 : $i_InventorySystem_Category_DisplayOrder";


# added on 21 Apr 2008 #
$i_InventorySystem_Import_CorrectLocationCodeWarning2 = " * Please make sure the above Location Codes are inputted correctly in your csv import file.";
$i_InventorySystem_Import_CorrectCategoryCodeWarning2 = " * Please make sure the above Category Codes are inputted correctly in your csv import file.";
$i_InventorySystem_Import_CorrectGroupCodeWarning = " * The above Resource Management Group Codes are in use. Please use new Resource Management Group Codes to import.";

# added on 24 Apr 2008 #
$i_InventorySystem_Category2_Serial = "Serial Number";
$i_InventorySystem_Category2Import_Format1_Row7 = "Column 7 : $i_InventorySystem_Category2_Serial (1 - Applicable, 0 - Not applicable)";
$i_InventorySystem_Category2ImportError[10] = $i_InventorySystem_Category2_Serial." is empty.";

# added on 2 May 2008 #
$i_InventorySystem_Settings_BulkItemAdmin = "Variance Manager";
$i_InventorySystem_GroupMemberImport_Format1_Row1 = "Row 1 : Resource Management Group Code";
$i_InventorySystem_GroupMemberImport_Format1_Row2 = "Row 2 : Login ID";
$i_InventorySystem_GroupMemberImport_Format1_Row3 = "Row 3 : Identity (1 - Group Leader, 2 - Group Member)";
$i_InventorySystem_Import_CorrectGroupCodeWarning2 = " * Please make sure the above Resource Management Group Codes are inputted correctly in your csv import file.";
$i_InventorySystem_GroupMemberImportError[1] = $i_InventorySystem_Item_GroupCode." is not valid";
$i_InventorySystem_GroupMemberImportError[2] = $i_InventorySystem_Item_GroupCode." is empty";
$i_InventorySystem_GroupMemberImportError[3] = $i_UserLogin." is empty";
$i_InventorySystem_GroupMemberImportError[4] = $i_UserLogin." is not valid";
$i_InventorySystem_GroupMemberImportError[5] = $i_identity." is empty";
$i_InventorySystem_GroupMemberImportError[6] = $i_identity." is not valid";
$i_InventorySystem_GroupMemberImportError[7] = $i_UserLogin." already exist";

# added on 5 May 2008 #
$i_InvertorySystem_SelectOriginalLocation_Warning = "Please select the original location";
$i_InvertorySystem_SelectOriginalGroup_Warning = "Please select the original group";
$i_InvertorySystem_SelectNewLocation_Warning = "Please select the new location";
$i_InvertorySystem_SelectNewGroup_Warning = "Please select the new group";
$i_InventorySystem_Notice = "eInventory Notice";
$i_InventorySystem_Input_ItemName_Warning = "Please input item name";

# added on 7 May 2008 #
$i_InventorySystem_VarianceHandle_ReAssignLocationQuantity = "Re-assign Location Quantity";
$i_InventorySystem_VarianceHandle_TotalAvaliableQuantity = "Total Avaliable Quantity";
$i_InventorySystem_VarianceHandle_TotalAssignedQuantity = "Total Assigned Quantity";
$i_InventorySystem_VarianceHandle_TotalRemainingQuantity = "Total Remaining Quantity";
$i_InventorySystem_VarianceHandle_TotalIgnoreQuantity = "Total Ignore Quantity";
$i_InventorySystem_VarianceHandle_TotalWriteOffQuantity = "Total Write-off Quantity";
$i_InventorySystem_VarianceHandle_InputWriteOffQuantity = "Input write-off quantity for each location";
$i_InventorySystem_VarianceHandle_InputWriteOffQty_Warning = "The input write-off quantity is not valid";
$i_InventorySystem_VarianceHandle_InputReAssignQty_Warning = "Re-assign Location Quantity is not valid";
$i_InventorySystem_VarianceHandle_InputReAssignQty_Warning = "Re-assign Location Quantity and Total Avaliable Quantity need to be exactly matched.";
$i_InventorySystem_VarianceHandle_QuantityRemaining_Warning = "Remaining Quantity";
$i_InventorySystem_VarianceHandle_SelectHandlerWarning = "Please select a handler";
$i_InventorySystem_VarianceHandle_SelectActionWarning = "Please select an action";
$i_InventorySystem_VarianceHandle_InputReAssignQtyWarning = "Please input re-assign quantity";
$i_InventorySystem_VarianceHandle_ButtonCalculate = "Calculate";
$i_InventorySystem_VarianceHandle_ClickCalculateWarning = "Click $i_InventorySystem_VarianceHandle_ButtonCalculate "."to check the quantity first";
$i_InventorySystem_NewItem_BulkItemAdmin_Warning = "Please select the variance manager";
$i_InventorySystem_UpdateItemLocation_OriginalLocation = "Original Location";
$i_InventorySystem_UpdateItemLocation_OriginalGroup = "Original Group";
$i_InventorySystem_UpdateItemLocation_OriginalQuantity = "Original Quantity";
$i_InventorySystem_UpdateItemLocation_NewLocation = "New Location";
$i_InventorySystem_UpdateItemLocation_NewGroup = "New Group";
$i_InventorySystem_UpdateItemLocation_NewQuantity = "New Quantity";
$i_InventorySystem_SettingOthers_StocktakePeriod_Warning = "Stocktake period is not valid";
$i_InventorySystem_SettingOthers_ReminderDay_Warning = "Please input a reminder day";
$i_InventorySystem_SettingOthers_BarcodeLength_Warning = "Please input the barcode maximun length";
$i_InventorySystem_Setting_Catgeory_DeleteFail = "Record Delete Failed, the category is being used.";
$i_InventorySystem_Setting_SubCatgeory_DeleteFail = "Record Delete Failed, the sub-category is being used.";
$i_InventorySystem_Setting_Location_DeleteFail = "Record Delete Failed, the location is being used.";
$i_InventorySystem_Setting_SubLocation_DeleteFail = "Record Delete Failed, the sub-location is being used.";
$i_InventorySystem_Setting_ResourceManagementGroup_DeleteFail = "Record Delete Failed, the group is being used.";
$i_InventorySystem_Setting_FundingSource_DeleteFail = "Record Delete Failed, the funding source is being used.";
$i_InventorySystem_ItemFullList_DeleteItemFail = "Record Delete Failed, the item is still using.";
$i_InventorySystem_ItemFullList_Cannot_New_Item = "Cannot add new item. Please go to <b>Management</b> to complete setup of <b>Category</b>, <b>Location/Sub-location</b>, <b>Resource Management Group</b> and <b>Funding Source</b> and try again.";

### added on 14 May 2008 ###
$i_InventorySystem_UpdateBulkItemStatus_Warning = "The total quantity entered is larger/smaller than the listed quantity. Are you sure you want to proceed?";
$i_InventorySystem_Import_CheckVarianceManager = "Click here to check ".$i_InventorySystem_Item_BulkItemAdmin;

### added on 15 May 2008 ###
$i_InventorySystem_VarianceHandling_CurrentLocation = "Current Location";
$i_InventorySystem_Report_Stocktake_NotDone = "Stocktake not yet performed";
$i_InventorySystem_FullList_DeleteItemAlert = "This record will be removed permanently. You will no longer be able to access this item's historical information in the future. Please write-off this item if you want to remove this item from the inventory without removing its historical information.";
$i_InventorySystem_NewItem_PurchaseType_Help = "If \"Existing Item\" is chosen, the item you are going to add MUST share the same resource management group and funding source as the existing item to be designated. If you are adding an item with identical name as an existing item, but different resource management group or funding source, you should still choose \"New Item\".";
$i_InventorySystem_NewItem_SelectLocationFisrt_MSG = "Select the location first";
$i_InventorySystem_LabelPerPage = "Label Per Page";
$i_InventorySystem_Report_PCS = "pcs";
$i_InventorySystem_Stocktake_NotIncludeWriteOffQty = "Not include items to be written-off";

# added on 29 May 2008 #
$i_InventorySystem_SetAsResourceBookingItem = "Set as Resoruce item";
$i_InventorySystem_Warning_SetBulkItemAsResourceItem = "Bulk Item cannot use in resource booking";
$i_InventorySystem_ResourceItemsSetting = "Resource items setting";

# added on 2 Jun 2008 #
$i_InventorySystem_RequestWriteOff_ResourceItemStatusSelection = "Resource item setting";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus1 = "Delete corresponding resource item";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus2 = "Change the resource item to Pending status";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus3 = "Keep the resource item";
$i_InventorySystem_RequestWriteOff_ResourceItemConditionWarning = "Please select the Resource items setting";
$i_InventorySystem_UpdateStatus_ResourceItemStatus = "Resource status";

# added on 12 Jan 2009
$i_InventorySystem_Setting_ItemCodeFormat_Title = "Item Code Format";
$i_InventorySystem_Setting_ItemCodeFormat_Year = "Purchase Year";

# added on 22 Jan 2009
$i_InventorySystem_Setting_ItemCodeFormatSetting_Warning = "Please complete the Item Code Format setting first";
$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup = "Please select the resource management group";
$i_InventorySystem_JSWarning_NewItem_SelectLocation = "Please select the location";
$i_InventorySystem_JSWarning_NewItem_SelectSubLocation = "Please select the sub-location";
$i_InventorySystem_JSWarning_NewItem_SelectFunding = "Please select the funding source";
$i_InventorySystem_JSWarning_NewItem_InvalidItemBarcode = "Item Barcode is not valid";
$i_InventorySystem_JSWarning_NewItem_ItemCodeEmpty = "Please input the item code";

# added on 4 Feb 2009
$i_InventorySystem_NewItem_ItemBarcodeRemark = "Please refer the barcode format in \"settings > others > barcode format\"";

# added on 20090213
$i_InventorySystem_ExportItemBarcode = "Export Barcode";

# added on 20090324
$i_InventorySystem_ImportFileFormatWarning = "Please note that the format of the CSV is the most updated one";
$i_InventorySystem_Setting_GroupLeaderRight = "Group Leader Right";
$i_InventorySystem_Setting_GroupLeader_NotAllowAddItem = "Not Allow Group Leader Add Items";
$i_InventorySystem_Setting_GroupLeader_AllowAddItem = "Allow Group Leader Add Items";
# added on 20090325
$i_InventorySystem_Setting_Photo_Use = "Use";
$i_InventorySystem_Setting_Photo_DoNotUse = "Do Not Use";
# added on 20090331
$i_InventorySystem_Setting_Barcode_ChangedWarning = "Note: Barcode Format has been changed.";

# added on 20090409
$i_InventorySystem_ItemType_Single_ExportTitle_ExportTitle = "Single Items";
$i_InventorySystem_ItemType_Bulk_ExportTitle = "Bulk Items";
$i_InventorySystem_Item_Code_ExportTitle = "Item Code";
$i_InventorySystem_Item_Barcode_ExportTitle = "Barcode";
$i_InventorySystem_Item_ChineseName_ExportTitle = "Item Chinese Name";
$i_InventorySystem_Item_EnglishName_ExportTitle = "Item English Name";
$i_InventorySystem_Item_ChineseDescription_ExportTitle = "Chinese Description";
$i_InventorySystem_Item_EnglishDescription_ExportTitle = "English Description";
$i_InventorySystem_Item_CategoryCode_ExportTitle = "Category Code";
$i_InventorySystem_Item_Category2Code_ExportTitle = "Sub-category Code";
$i_InventorySystem_Item_GroupCode_ExportTitle = "Group Code";
$i_InventorySystem_Item_LocationCode_ExportTitle = "Location Code";
$i_InventorySystem_Item_Location2Code_ExportTitle = "Sub-location Code";
$i_InventorySystem_Item_FundingSourceCode_ExportTitle = "Funding Source Code";
$i_InventorySystem_Item_Ownership_ExportTitle = "Ownership";
$i_InventorySystem_Item_Warrany_Expiry_ExportTitle = "Warranty Expiry Date";
$i_InventorySystem_Item_License_ExportTitle = "License";
$i_InventorySystem_Item_Serial_Num_ExportTitle = "Serial No";
$i_InventorySystem_Item_Brand_Name_ExportTitle = "Brand";
$i_InventorySystem_Item_Supplier_Name_ExportTitle = "Supplier";
$i_InventorySystem_Item_Supplier_Contact_ExportTitle = "Supplier Contact";
$i_InventorySystem_Item_Supplier_Description_ExportTitle = "Supplier Description";
$i_InventorySystem_Item_Quot_Num_ExportTitle = "Quotation No";
$i_InventorySystem_Item_Tender_Num_ExportTitle = "Tender No";
$i_InventorySystem_Item_Invoice_Num_ExportTitle = "Invoice No";
$i_InventorySystem_Item_Purchase_Date_ExportTitle = "Purchase Date";
$i_InventorySystem_Item_Price_ExportTitle = "Total Purchase Amount";
$i_InventorySystem_Unit_Price_ExportTitle = "Unit Price";
$i_InventorySystemItemMaintainInfo_ExportTitle = "Maintanence Details";
$i_InventorySystem_Item_Remark_ExportTitle = "Remarks";
$i_InventorySystem_Item_BulkItemAdmin_ExportTitle = "Variance Manager";
$i_InventorySystem_Item_Qty_ExportTitle = "Quantity";
$i_InventorySystem_Item_CategoryName_ExportTitle = "Category";
$i_InventorySystem_Item_Category2Name_ExportTitle = "Sub-category";
$i_InventorySystem_Item_GroupName_ExportTitle = "Resource Management Group";
$i_InventorySystem_Item_LocationName_ExportTitle = "Location";
$i_InventorySystem_Item_Location2Name_ExportTitle = "Sub-location";
$i_InventorySystem_Item_FundingSourceName_ExportTitle = "Funding Source";

## added on 20090416
$i_InventorySystem_FixedAssetsRegister_Warning = "When \"Location\" is used as display order, Unit Price, Purchase Price and Cost of bulk items will be calculated from their quantity at a particular location. These calculated values MAY differ from the actual Unit Price, Purchase Price and Cost. For more accurate information, please use \"Item Type\" or \"Resources Management Group\" as display order.";
$i_InventorySystem_ExportItemDetails1 = "Export (Code format)";
$i_InventorySystem_ExportItemDetails2 = "Export (Name format)";
$i_InventorySystem_ExportWarning = "If no Items selected, all Item Details will be exported. Are you sure to continuous?";
$i_InventorySystem_CategorySetting_PriceCeiling = "Price Ceiling";
$i_InventorySystem_CategorySetting_PriceCeiling_ApplyToBulk = "Apply to Bulk Item under this category";
$i_InventorySystem_CategorySetting_PriceCeiling_JSWarning1 = "Please input a valid price ceiling";

## added on 20090527
$i_InventorySystem_DeleteItemPhoto = "Delete The Current Item Photo";

## added on 20090812
$i_InventorySystem_StocktakeType_AllItem = "Show all items";
$i_InventorySystem_StocktakeType_StocktakeNotDone = "Show stocktake not done only";

### End Of eInventory Words ###

$i_StudentRegistry['System'] = "Student Information System";
$i_StudentRegistry_ModifiedSince = "Modified Since";
$i_StudentRegistry_AuthCodeMain = "DSEJ Student Code";
$i_StudentRegistry_AuthCodeCheck = "Check Digit";
$i_StudentRegistry_PlaceOfBirth = "Place of Birth";
$i_StudentRegistry_ID_Type = "ID Type";
$i_StudentRegistry_ID_No = "ID No.";
$i_StudentRegistry_ID_IssuePlace = "Place of Issue";
$i_StudentRegistry_ID_IssueDate = "Date of Issue";
$i_StudentRegistry_ID_ValidDate = "Valid Thru";
$i_StudentRegistry_SP_Type = "Stay Permit Type";
$i_StudentRegistry_SP_No = "Stay Permit No.";
$i_StudentRegistry_SP_IssueDate = "Date of Issue";
$i_StudentRegistry_SP_ValidDate = "Valid Thru";
$i_StudentRegistry_Province = "Province";
$i_StudentRegistry_ResidentialArea_Night = "Residential Area(Night)";
$i_StudentRegistry_ResidentialArea = "Residential Area";
$i_StudentRegistry_ResidentialRoad = "Residential Road";
$i_StudentRegistry_ResidentialAddress = "Residential Address";

$i_StudentRegistry_ExportPurpose = "Export Purpose";
$i_StudentRegistry_ExportFormat_XML_1 = "For batch query on Student Code use";
$i_StudentRegistry_ExportFormat_XML_2 = "For Student Information Update Purpose (Scheme 2)";
$i_StudentRegistry_ExportFormat_XML_3 = "For Student Information Update Purpose (Scheme 3 / Scheme 5)";

$i_StudentRegistry_EnglishName_Alert = "The English Name only accepts Upper Case. It is now converted automatically. Please Save this change.";

$i_UserAddress_Area = "Area";
$i_UserAddress_Road = "Road";
$i_UserAddress_Address = "Flat/Floor/Block";

$i_StudentGuardian_EmergencyContact = "Emergency Contact";
$i_StudentGuardian_LiveTogether = "Live Together";
$i_StudentGuardian_Occupation = "Occupation";

$i_StudentRegistry_AreaCode = array();//see cust. template
$i_StudentRegistry_PlaceOfBirth_Code = array();//see cust. template
$i_StudentRegistry_ID_Type_Code = array();//see cust. template
$i_StudentRegistry_ID_IssuePlace_Code = array();//see cust. template
$i_StudentRegistry_Country_Code = array();//see cust. template
$i_StudentRegistry_ResidentialAreaNight_Code = array();//see cust. template


$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE1 = "EE1 - Special Education";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE2 = "EE2 - Special Education";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE3 = "EE3 - Special Education";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EEP = "EEP - Special Education";

$i_WebSAMS_AttendanceCode_Option_ClassLevel_1 = "1 - Portuguese Primary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_2 = "2 - Portuguese Primary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_3 = "3 - Portuguese Primary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_4 = "4 - Portuguese Primary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_5 = "5 - Portuguese Primary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_6 = "6 - Portuguese Primary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EP6 = "EP6 - English Secnondary School Prep";

$i_WebSAMS_AttendanceCode_Option_ClassLevel_F1 = "F1 - English Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F2 = "F2 - English Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F3 = "F3 - English Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F4 = "F4 - English Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F5 = "F5 - English Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F6 = "F6 - English Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_7 = "7 - Portuguese Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_8 = "8 - Portuguese Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_9 = "9 - Portuguese Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_10 = "10 - Portuguese Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_11 = "11 - Portuguese Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_12 = "12 - Portuguese Secondary School";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERP = "ERP - Primary Recurrent Education";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERG = "ERG - Junior Secondary Recurrent Education";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERC = "ERC - Senior Secondary Recurrent Education";

$ec_iPortfolio['guardian_import_remind_sms'] = " Attention: <br> - The first column is RegNo. To ensure the number is complete, # has to be added to RegNo (e.g. \"#0025136\"). <Br> - The second column is the guardian english name <Br> - The third column is the guardian chinese name <Br> - The fourth column is the guardian contact number <Br> - The fifth column is the guardian emergency contact number <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> The sixth column is the relationship between the guardian and the student <Br> - The seventh column is to set the guardian to be the main guardian <Br> - The eighth column is to set the guardian to be SMS receiver <Br> - The ninth column is to set the guardian to be the emergency contact guardian <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - Father, 02 - Mother, 03 - Grandfather, 04 - Grandmother, 05 - Brother, 06 - Sister, 07 - Relative, 08 - Other) <br> - <font color=red>The new imported guardian records will overwrite the existing records.</font><Br>";

$ec_iPortfolio['guardian_import_remind_studentregistry'] = " Attention: <br> - The first column is RegNo. To ensure the number is complete, # has to be added to RegNo (e.g. \"#0025136\"). <Br> - The second column is the guardian english name <Br> - The third column is the guardian chinese name <Br> - The fourth column is the guardian contact number <Br> - The fifth column is the guardian emergency contact number <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> The sixth column is the relationship between the guardian and the student <Br> - The seventh column is to set the guardian to be the main guardian <Br> - The eighth column is to set the guardian to be the emergency contact guardian <br> - The ninth column is the occupation of the guardian <br> - The tenth column is whether the student lives with the guardian <br> - <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> The eleventh column is the Area Code of the address <br> - The twelfth column is the Road of the Address<br> - The thirteenth column is the remaining Part of the Address, ie. Flat, Floor, Building, etc <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - Father, 02 - Mother, 03 - Grandfather, 04 - Grandmother, 05 - Brother, 06 - Sister, 07 - Relative, 08 - Other) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> (O-Other, M-Macau, T-Taipa, C-Coloane)<br> - <font color=red>The new imported guardian records will overwrite the existing records.</font><Br>";

$ec_iPortfolio['guardian_import_remind_sms_studentregistry'] = " Attention: <br> - The first column is RegNo. To ensure the number is complete, # has to be added to RegNo (e.g. \"#0025136\"). <Br> - The second column is the guardian english name <Br> - The third column is the guardian chinese name <Br> - The fourth column is the guardian contact number <Br> - The fifth column is the guardian emergency contact number <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> The sixth column is the relationship between the guardian and the student <Br> - The seventh column is to set the guardian to be the main guardian <Br> - The eighth column is to set the guardian to be SMS receiver <Br> - The ninth column is to set the guardian to be the emergency contact guardian <br> - The tenth column is the occupation of the guardian <br> - The eleventh column is whether the student lives with the guardian <br> - <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> The twelfth column is the Area Code of the address <br> - The thirteenth column is the Road of the Address<br> - The fourteenth column is the remaining Part of the Address, ie. Flat, Floor, Building, etc <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - Father, 02 - Mother, 03 - Grandfather, 04 - Grandmother, 05 - Brother, 06 - Sister, 07 - Relative, 08 - Other) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> (O-Other, M-Macau, T-Taipa, C-Coloane)<br> - <font color=red>The new imported guardian records will overwrite the existing records.</font><Br>";

$i_StudentRegistry_Import_XML_Format = "XML Import Format:<br />
&lt;?xml version=&quot;1.0&quot; encoding=&quot;ISO-8859-1&quot;?&gt;<br />
&lt;!DOCTYPE SRA SYSTEM &quot;http://app.dsej.gov.mo/prog/edu/sra/sra3.dtd&quot;&gt;<br />

&lt;SRA&gt;<br />
&lt;STUDENT&gt;<br />
&lt;STUD_ID&gt;12345&lt;/STUD_ID&gt;<br />
&lt;CODE&gt;9400999-6&lt;/CODE&gt;<br />
&lt;NAME_C&gt;&#37758;&#22283;&#20161;&lt;/NAME_C&gt;<br />
&hellip;<br />
&lt;/STUDENT&gt;<br />
&lt;STUDENT&gt;<br />
&lt;STUD_ID&gt;12346&lt;/STUD_ID&gt;<br />
&lt;CODE&gt;9400998-X&lt;/CODE&gt;<br />
&lt;NAME_C&gt;&#37758;&#22283;&#26862;&lt;/NAME_C&gt;<br />
&hellip;<br />
&lt;/STUDENT&gt;<br />
&hellip;<br />
&lt;/SRA&gt;";

$i_StudentRegistry_Import_Invalid_Record = "Record importing including invalid records.";
$i_StudentRegistry_Import_No_Record = "Record importing no record.";
$i_StudentRegistry_Import_ValidInvalid_Record = "Record importing including both valid and invalid records. If continue on import, only the valid records will be imported.";
$i_StudentRegistry_Import_Valid_Record = "Record importing including valid records.";

$i_StudentRegistry_Export_Invalid_Record = "Record exporting including incomplete records.";
$i_StudentRegistry_Export_Valid_Record = "Record exporting including <--NoOfValid--> complete records.";

$i_general_clickheredownloaddtd = 'Click here to download DTD file';
$i_general_invalid = 'Invalid Data';
$i_general_incomplete = 'Incomplete Data';

$i_StudentRegistry_Import_Invalid_Reason = "The problem(s) may be :<br>1. <span style='color:red;'>-</span>Value expected.<br>2. <span style='color:red;'>[Value Entered]</span> incorrect mapping found.";
$i_StudentRegistry_Export_Invalid_Reason = "The problem(s) may be :<br><span style='color:red;'>-</span>Value expected.";




##########################################
# eCommunity
##########################################
$eComm['eCommunity'] = "eCommunity";
$eComm['MyGroups'] = "My Groups";
$eComm['OtherGroups'] = "Other Groups";
$eComm['MyPage'] = "My Page";
$eComm['Settings'] = "Settings";
$eComm['LatestShare'] = "Latest Share";
$eComm['Photo'] = "Photo";
$eComm['Video'] = "Video";
$eComm['Website'] = "Website";
$eComm['File'] = "File";
$eComm['P'] = $eComm['Photo'];
$eComm['V'] = $eComm['Video'];
$eComm['W'] = $eComm['Website'];
$eComm['F'] = $eComm['File'];
$eComm['Add_'] = "Add ";
$eComm['Edit_'] = "Edit ";
$eComm['New_'] = "New ";

$eComm['View'] = "View";
$eComm['Add'] = "Add";

$eComm_Field_Topic = "Topic";
$eComm_Field_Createdby = "Created by";
$eComm_Field_ReplyNo = "No. of Replies";
$eComm_Field_LastUpdated = "Last Updated";
$eComm['Forum'] = "Forum";
$eComm['Title'] = "Title";
$eComm['Content'] = "Content";
$eComm['Group'] = "Group";
$eComm['YourReply'] = "Your Reply";
$eComm['Search'] = "Search";
$eComm['Close'] = "Close";
$eComm['Attachment'] = "Attachment";

$eComm['Share'] = "Share";
$eComm['By'] = "By";
$eComm['Comment'] = "Comment";
$eComm['Album'] = "Album";
$eComm['Folder'] = "Folder";
$eComm['Category'] = "Category";
$eComm['NewAlbum'] = "New " . $eComm['Album'];
$eComm['NewFolder'] = "New " . $eComm['Folder'];
$eComm['NewCateogry'] = "New " . $eComm['Cateogry'];
$eComm['EditAlbum'] = "Edit " . $eComm['Album'];
$eComm['EditFolder'] = "Edit " . $eComm['Folder'];
$eComm['EditCateogry'] = "Edit " . $eComm['Cateogry'];
$eComm['LastUpload'] = "Last Uploaded";
$eComm['Status'] = "Status";
$eComm['Allow_Att'] = "Allow Attachment";
$eComm['Public'] = "Public";
$eComm['Private'] = "Private";
$eComm['Top'] = "Top";
$eComm['jsDeleteRecord'] = "Are you sure you want to delete these record(s)?";
$eComm['jsDeleteAttachment'] = "Are you sure you want to delete these attachment(s)?";
$eComm['jsDeleteFolder'] = "Are you sure you want to delete these record(s)?  Related file(s) and comment(s) will also be deleted.";
$eComm['CreatedBy'] = "Created by";
$eComm['CreatedDate'] = "Created date";
$eComm['WriteYourcomment'] = "Write Your comment";
$eComm['Yourcomment'] = "Your comment";
$eComm['NoRecord'] = "There is no record at the moment.";
$eComm['jsSelectStudent'] = "Please select a student";

$eComm['Steps'] = "Steps";
$eComm['select_file'] = "Select/Input your file(s) ";
$eComm['select_photo'] = "Select/Input your photo(s) ";
$eComm['input_website'] = "Input website(s) ";
$eComm['select_video'] = "Select your video ";
$eComm['input_detail_info'] = "Input detailed information ";
$eComm['add_file_finished'] = "File(s) added.";
$eComm['add_photo_finished'] = "Photo(s) added.";
$eComm['add_website_finished'] = "Website(s) added.";
$eComm['add_video_finished'] = "Video added.";
$eComm['add_to_folder'] = "Add to Folder";
$eComm['add_to_album'] = "Add to Album";
$eComm['add_to_category'] = "Add to Category";
$eComm['move_to_folder'] = "Move to Folder";
$eComm['move_to_album'] = "Move to Album";
$eComm['move_to_category'] = "Move to Category";
$eComm['Upload'] = "Upload";
$eComm['URL'] = "URL";
$eComm['js_please_select_file'] = "Please select at least one file.";
$eComm['js_please_select_photo'] = "Please select at least one photo.";
$eComm['js_please_input_url'] = "Please input at least one URL.";
$eComm['info_str']['P'] = "Photo(s) have been uploaded. You can click on the thumbnail to input the related information.";
$eComm['info_str']['F'] = "File(s) have been uploaded. You can click on the thumbnail to input the related information.";
$eComm['info_str']['W'] = "Website(s) have been added. You can click on the thumbnail to input the related information.";
$eComm['info_str']['V'] = "Video have been added. You can click on the thumbnail to input the related information.";
$eComm['Description'] = "Description";
$eComm['remove_this_file'] = "Remove this file";
$eComm['remove_this_video'] = "Remove this video";
$eComm['set_private'] = "Set to Private";
$eComm['set_all_to_folder'] = "Add all file(s) to Folder";
$eComm['set_all_to_album'] = "Add all photo(s) to Album";
$eComm['set_all_to_category'] = "Add all website(s) to Category";
$eComm['set_all_to_video_album'] = "Add all video to Folder";
$eComm['select_all'] = "Select All";

$eComm['Member'] = "Member";
$eComm['NewTopic'] = "New Topic";
$eComm['NewMember'] = "New Member";
$eComm['Storage'] = "Storage";
$eComm['NoFolder']['P'] = "No album found. Please contact your system administrator to create album and upload again.";
$eComm['NoFolder']['F'] = "No folder found. Please contact your system administrator to create folder and upload again.";
$eComm['NoFolder']['W'] = "No category found. Please contact your system administrator to create category and upload again.";
$eComm['NoFolder']['V'] = "No album found. Please contact your system administrator to create album and upload again.";
$eComm['Performance'] = "Performance";
$eComm['MemberList'] = "Member List";
$eComm['Warning'] = "Warning";
$eComm['no_space'] = "Not enough storage space.";
$eComm['no_file'] = "No file is uploaded.";
$eComm['no_website'] = "No website is added.";
$eComm['chatroom'] = "Chat room";
$eComm['GroupSettings'] = "Group Settings";
$eComm['MemberSettings'] = "Member Settings";
$eComm['AnnouncementSettings'] = "Announcement Settings";
$eComm['SharingSettings'] = "Sharing Settings";
$eComm['ForumSettings'] = "Forum Settings";
$eComm['GroupLogo'] = "Group Logo";
$eComm['GroupLogo_PhotoGuide'] = "Group Logo must be in '.JPG', '.GIF', or '.PNG' format and the size should be fixed to 225 x 170 pixel (W x H).";
$eComm['GroupLogoDelete'] = "Delete Current Group Logo";
$eComm['PublicStatus'] = "Public Status";
$eComm['Allow Reply'] = "Allow Reply";
$eComm['Management'] = "Management";
$eComm['IndexAnnounce'] = "Announcement# at index page";
$eComm['FolderType'] = "Folder Type";
$eComm['FileType'] = "File Type";
$eComm['FileName'] = "File Name";
$eComm['NoFolderSelected'] = "No folder is selected.";
$eComm['WaitingRejectList'] = "Waiting Approval / Rejected List";
$eComm['WaitingApproval'] = "Waiting Approval";
$eComm['Rejected'] = "Rejected";
$eComm['ApprovalStatus'] = "Approval Status";
$eComm['DisplayOnTop'] = "Display on Top";
$eComm['GrantAdmin'] = "Grant admin permission";
$eComm['LatestNumber'] = "Latest Record#";
$eComm['NeedApproval'] = "Need Approval";
$eComm['SetAsCoverImage'] = "Set as cover image";
$eComm['DefaultViewModeIndexPage'] = "Default View Mode at index page";
$eComm['ViewMode_List'] = "List";
$eComm['ViewMode_4t'] = "4 Thumbnails";
$eComm['ViewMode_2t'] = "2 Thumbnails";
$eComm['ViewMode_1t'] = "1 Thumbnail";
$eComm['AllowedImageTypes'] = "Allowed image types";
$eComm['AllowedFileTypes'] = "Allowed file types";
$eComm['AllowedVideoTypes'] = "Allowed video types";
$eComm['AllowedTypesRemarks'] = "\"ALL\" will result in all allowed file types.  If you want to restrict the allowed file types to certain types only, enter a slash-separated list of extensions, e.g. jpg/bmp/tif";
$eComm['file_type_not_allowed'] = "File(s) upload failure.  File type is not allowed.";
$eComm['basic_setting'] = "Basic Setting";
$eComm['CalendarSettings'] = "Calendar Settings";
$eComm['NoCalendar'] = "No calendar at this moment, create it now?";
$eComm['CreateCalendar'] = "Create Calendar";
$eComm['DisplayAtGroupIndex'] = "Display at Group index page?";
$eComm['AddTo'] = "<< Add to";
$eComm['DeleteTo'] = "Delete to >>";
$eComm['Message'] = "Message(s)";
$eComm['TopicSettings'] = "Topic Settings";

# HKU Medicine research
$i_MedicalReasonTitle = "Medical Reason";
$i_MedicalReasonName[] = array(0,"Undefined");
$i_MedicalReasonName[] = array(1,"ILI/Resp illness");
$i_MedicalReasonName[] = array(2,"GI illness");
$i_MedicalReasonName[] = array(3,"'Hand Foot and Mouth Disease' (HFMD)");
$i_MedicalReasonName[] = array(99,"Other illness");
$i_MedicalReasonName[] = array(999,"Not illness");
$i_MedicalReason_Export = "Export Medical Report";
$i_MedicalDataTransferSuccess = "Data Transferred Successfully.";
$i_MedicalDataTransferFailed = "Data Transferred Failed.";
$i_MedicalReason_Export2="Export Medical Report";
$i_MedicalExportError ="Cannot export as WebSAMS Class Level Name has not been set properly.";

# eLibrary
$i_eLibrary_System = "eBook";
$i_eLibrary_System_Admin_User_Setting = "Set Admin User";
$i_eLibrary_System_Current_Batch = "Current Batch";
$i_eLibrary_System_Update_Batch = "Update Batch";
$i_eLibrary_System_Updated_Batch = "Updated Batch";
$i_eLibrary_System_Reset_Fail = "Reset Book Status Failed.";
$i_eLibrary_System_Updated_Records = "Updated Records";
$eLib["AdminUser"] = "Admin User";

$eLib['ManageBook']["Title"] = "Book Management";
$eLib['ManageBook']["ConvertBook"] = "Convert Book";
$eLib['ManageBook']["ImportView"] = "Import Content";
$eLib['ManageBook']["ContentManage"] = "Manage Content";
$eLib['ManageBook']["ContentView"] = "View Content";
$eLib['ManageBook']["ImportBook"] = "Import Book List";
$eLib['ManageBook']["ImportCSV"] = "Import CSV File";
$eLib['ManageBook']["ImportXML"] = "Import XML File";
$eLib['ManageBook']["ImportZIP"] = "Import ZIP File";
$eLib['ManageBook']["ImportZIPDescribe"] = "
				The ZIP file should contain the following files: <br />
				content.text - the text file to be converted info XML. <br />
				Folder \"image\" - the folder contains the images that will be used the the book. <br />
				control.text - the text file to control the conversion process. <br />
													";

$eLib['Book']["Author"] = "Author";
$eLib['Book']["SeriesEditor"] = "Series Editor";
$eLib['Book']["Category"] = "Category";
$eLib['Book']["Subcategory"] = "Subcategory";
$eLib['Book']["DateModified"] = "DateModified";
$eLib['Book']["Description"] = "Description";
$eLib['Book']["Language"] = "Language";
$eLib['Book']["Level"] = "Level";
$eLib['Book']["AdultContent"] = "Sensitive Content";
$eLib['Book']["Title"] = "Title";
$eLib['Book']["Publisher"] = "Publisher";
$eLib['Book']["TableOfContent"] = "Table of Content";
$eLib['Book']["Copyright"] = "Copyright";

$eLib["SourceFrom"] = "Source From";
$eLib['Source']["green"] = "Green Apple";
$eLib['Source']["cup"] = "Cambridge University Press";

$eLib['EditBook']["IsChapterStart"] = "Chapter Start?";
$eLib['EditBook']["ChapterID"] = "Chapter ID";

# HTML editor
$ec_html_editor['bold'] = "Bold";
$ec_html_editor['italic'] = "Italic";
$ec_html_editor['underline'] = "Underline";
$ec_html_editor['strikethr'] = "Strikethrough";
$ec_html_editor['subscript'] = "Subscript";
$ec_html_editor['superscript'] = "Superscript";
$ec_html_editor['justifyleft'] = "Justify Left";
$ec_html_editor['justifyright'] = "Justify Right";
$ec_html_editor['justifycent'] = "Justify Center";
$ec_html_editor['justifyfull'] = "Justify Full";
$ec_html_editor['orderlist'] = "Ordered List";
$ec_html_editor['bulletlist'] = "Bulleted List";
$ec_html_editor['decindent'] = "Decrease Indent";
$ec_html_editor['incindent'] = "Increase Indent";
$ec_html_editor['fontcolor'] = "Font Color";
$ec_html_editor['bgcolor'] = "Background Color";
$ec_html_editor['horrule'] = "Insert Horizontal Rule";
$ec_html_editor['weblink'] = "Insert or Modify Web Link";
$ec_html_editor['insertimage'] = "Insert or Modify Image";
$ec_html_editor['table'] = "Table";
$ec_html_editor['htmlsource'] = "View HTML Source";
$ec_html_editor['topicsent'] = "Topic Sentence";
$ec_html_editor['wordcount'] = "Word Counting";
$ec_html_editor['highlight'] = "Highlight";
$ec_html_editor['missing'] = "Missing";
$ec_html_editor['comment'] = "Comment";
$ec_html_editor['correct'] = "Correct";
$ec_html_editor['pcorrect'] = "Partly Correct";
$ec_html_editor['incorrect'] = "Incorrect";
$ec_html_editor['appreciate'] = "Appreciate";
$ec_html_editor['markcode'] = $ec_wordtemplates['marking_code'];
$ec_html_editor['errsummy'] = "Error Summary";
$ec_html_editor['markscheme'] = $ec_wordtemplates['marking_scheme'];
$ec_html_editor['sticker'] = "Sticker";
$ec_html_editor['eqeditor'] = "Equation Editor";
$ec_html_editor['undo'] = "Undo";
$ec_html_editor['redo'] = "Redo";
$ec_html_editor['hvmode'] = "Horizontal/Vertical Mode";
$ec_html_editor['mgcomment'] = "Marginal Comment";
$ec_html_editor['linespace'] = "Set Line Space";
$ec_html_editor['copy'] = "Copy";
$ec_html_editor['cut'] = "Cut";
$ec_html_editor['paste'] = "Paste";
$ec_html_editor['font'] = "Font";
$ec_html_editor['font_size'] = "Font Size";
$ec_html_editor['clear_text_style'] = "Clear Text Style";
$ec_html_editor['createanchor'] = "Create or Modify Anchor";
$ec_html_editor['anchorname'] = "Anchor Name";
$ec_html_editor['anchor'] = "Anchor";
$ec_html_editor['selectcolor'] = "Select Color";
$ec_html_editor['targetwindow'] = "Target Window";
$ec_html_editor['power_voice'] = "Power Voice";

$ec_html_editor['headingNor'] = "Normal";
$ec_html_editor['heading1'] = "Heading 1";
$ec_html_editor['heading2'] = "Heading 2";
$ec_html_editor['heading3'] = "Heading 3";
$ec_html_editor['heading4'] = "Heading 4";
$ec_html_editor['heading5'] = "Heading 5";
$ec_html_editor['heading6'] = "Heading 6";
$ec_html_editor['headingAdd'] = "Address";

$ec_html_editor['inserttable'] = "Insert Table...";
$ec_html_editor['edittable'] = "Edit Table Properties...";
$ec_html_editor['editcell'] = "Edit Cell Properties...";
$ec_html_editor['insertcoll'] = "Insert Column to the Left";
$ec_html_editor['insertcolr'] = "Insert Column to the Right";
$ec_html_editor['insertrowa'] = "Insert Row Above";
$ec_html_editor['insertrowb'] = "Insert Row Below";
$ec_html_editor['increasecols'] = "Increase Column Span";
$ec_html_editor['decreasecols'] = "Decrease Column Span";
$ec_html_editor['increaserows'] = "Increase Row Span";
$ec_html_editor['decreaserows'] = "Decrease Row Span";
$ec_html_editor['deleterow'] = "Delete Row";
$ec_html_editor['deletecol'] = "Delete Column";

$ec_html_editor['image_url'] = "Image URL";
$ec_html_editor['alternate_text'] = "Alternate Text";
$ec_html_editor['layout'] = "Layout";
$ec_html_editor['alignment'] = "Alignment";
$ec_html_editor['border_thickness'] = "Border Thickness";
$ec_html_editor['spacing'] = "Spacing";
$ec_html_editor['horizontal'] = "Horizontal";
$ec_html_editor['vertical'] = "Vertical";

$ec_html_editor['al_none'] = "None";
$ec_html_editor['al_left'] = "Left";
$ec_html_editor['al_center'] = "Center";
$ec_html_editor['al_right'] = "Right";
$ec_html_editor['al_texttop'] = "Texttop";
$ec_html_editor['al_absmiddle'] = "Absmiddle";
$ec_html_editor['al_baseline'] = "Baseline";
$ec_html_editor['al_absbottom'] = "Absbottom";
$ec_html_editor['al_bottom'] = "Bottom";
$ec_html_editor['al_middle'] = "Middle";
$ec_html_editor['al_top'] = "Top";

$ec_html_editor['tb_size'] = "Size";
$ec_html_editor['tb_row'] = "Rows";
$ec_html_editor['tb_col'] = "Cols";
$ec_html_editor['tb_height'] = "Height";
$ec_html_editor['tb_width'] = "Width";
$ec_html_editor['tb_pixel'] = "Pixels";
$ec_html_editor['tb_percent'] = "Percent";
$ec_html_editor['tb_cellspacing'] = "Cell Spacing";
$ec_html_editor['tb_cellpadding'] = "Cell Padding";
$ec_html_editor['tb_color'] = "Color";
$ec_html_editor['tb_bgcolor'] = "Background Color";
$ec_html_editor['tb_bordercolor'] = "Border Color";
$ec_html_editor['tb_wrap'] = "Wrap";
$ec_html_editor['tb_nowrap'] = "NoWrap";
$ec_html_editor['tb_nowrap_yes'] = "Yes";
$ec_html_editor['tb_nowrap_no'] = "No";
$ec_html_editor['tb_hor_alignment'] = "Hori. Alignment";
$ec_html_editor['tb_ver_alignment'] = "Vert. Alignment";

$ec_html_editor['warn_image_url'] = "Image URL must be specified.";
$ec_html_editor['warn_hor_space'] = "Horizontal spacing must be a number between 0 and 999.";
$ec_html_editor['warn_border_thick'] = "Border thickness must be a number between 0 and 999.";
$ec_html_editor['warn_ver_space'] = "Vertical spacing must be a number between 0 and 999.";
$ec_html_editor['warn_specify_a'] = "You must specify a value for the ";
$ec_html_editor['warn_specify_b'] = " field!";
$ec_html_editor['warn_specify_num_a'] = "You must specify a number between 0 and 999 for ";
$ec_html_editor['warn_specify_num_b'] = " !";
$ec_html_editor['warn_overwrite'] = "Overwrite selected content?";

$ec_html_editor['total_word'] = "Total number of words";
$ec_html_editor['total_symbol'] = "Total number of symbols";

$ec_html_editor['save_content'] = "Save Content";
$button_ok = "OK";

$file_kb = "KB";
$file_mb = "MB";

###############################################################################
# iPortfolio 2.5
###############################################################################
$ec_iPortfolio['address'] = "Address";
$ec_iPortfolio['admission_date'] = "Admission Date";
$ec_iPortfolio['award'] = "Award";
$ec_iPortfolio['basic_info'] = "Basic Information";
$ec_iPortfolio['class'] = "Class";
$ec_iPortfolio['class_list'] = "Class List";
$ec_iPortfolio['display_by_grade'] = "By grade";
$ec_iPortfolio['display_by_rank'] = "By position in form";
$ec_iPortfolio['display_by_score'] = "By score";
$ec_iPortfolio['display_by_score_grade'] = "By score & grade";
$ec_iPortfolio['display_by_stand_score'] = "By standard score";
$ec_iPortfolio['em_phone'] = "Emergency Phone No.";
$ec_iPortfolio['enter_student_name'] = "Enter Student Name";
$ec_iPortfolio['enter_student_name_or_userlogin'] = "Enter Student Name or Login ID";
$ec_iPortfolio['guardian_info'] = "Guardian Information";
$ec_iPortfolio['house'] = "House";
$ec_iPortfolio['import_activation'] = "Activate iPortfolio account";
$ec_iPortfolio['list'] = "List";
$ec_iPortfolio['main_guardian'] = "Main Guardian";
$ec_iPortfolio['merit'] = "Merit/Demerit Record";
$ec_iPortfolio['number'] = "Class No.";
$ec_iPortfolio['phone'] = "Phone No.";
$ec_iPortfolio['record'] = "Records";
$ec_iPortfolio['semester'] = "Semester";
$ec_iPortfolio['self_account'] = "Self Account";
$ec_iPortfolio['student_photo_no'] = "No photo";
$ec_iPortfolio['student_account'] = "Student Account";
$ec_iPortfolio['thumbnail'] = "Thumbnail";
$ec_iPortfolio['total'] = "Total";
$ec_iPortfolio['total_record'] = "Total <!--NoRecord-->";
$ec_iPortfolio['upload_photo'] = "Upload Photo";
$ec_iPortfolio['whole_year'] = "Whole Year";
$ec_iPortfolio['year'] = "School Year";
$ec_iPortfolio['ole'] = "OLE";
$ec_iPortfolio['overall_result'] = "Overall Result";
$ec_iPortfolio['overall_score'] = "Overall Average Score";
$ec_iPortfolio['overall_rank'] = "Overall Position in Form";
$ec_iPortfolio['overall_score_grade'] = "Overall Average Score and Grade";
$ec_iPortfolio['overall_grade'] = "Overall Grade";
$ec_iPortfolio['overall_stand_score'] = "Overall Standard Score";
$ec_iPortfolio['overall_comment'] = "Overall Comment";
$ec_iPortfolio['overall_summary'] = "Overall Summary";
$ec_iPortfolio['subject_chart'] = "Subject's Chart";
$ec_iPortfolio['by_semester'] = "Term";
$ec_iPortfolio['by_year'] = "SchoolYear";
$ec_iPortfolio['show_by'] = "Show by every";
$ec_iPortfolio["score"] = "Score";
 $ec_iPortfolio["stand_score"] = "Standard Score";
 $ec_iPortfolio["rank"] = "Position in Form";
 $ec_iPortfolio['term'] = "Term";
 $ec_iPortfolio['activity_name'] = "Activity Name";
 $ec_iPortfolio['role'] = "Position";
 $ec_iPortfolio['performance'] = "Performance";
 $ec_iPortfolio['SAMS_last_update'] = "Last update";
 $ec_iPortfolio['activity'] = "Activity Record";
 $ec_iPortfolio['portfolio_status'] = "<!--PortfolioNo--> of <!--TotalStudentNo--> iPortfolio Student  are activated<!--GreenFrame-->.";
 $ec_iPortfolio['green_frame'] = " (with <span style=\"color:#3d7001; font-weight:bold\">green photo frame</span>)";
 $ec_iPortfolio['record'] = "Records";
 $ec_iPortfolio['report_type'] = "Report Type";
 $ec_iPortfolio['year'] = "School Year";
 $ec_iPortfolio['year_period'] = "Period";
 $ec_iPortfolio['full_report'] = "Full Report";
 $ec_iPortfolio['assessment_report'] = "Academic Report";
 $ec_iPortfolio['composite_performance_report'] = "Composite Performance Report";
$ec_iPortfolio['view_my_record'] = "View my Records";
$ec_iPortfolio['programme'] = "Programme";
$ec_iPortfolio['role_participate'] = "Role of Participation";


$ec_iPortfolio['heading']['learning_portfolio'] = "Learning Portfolio";
$ec_iPortfolio['heading']['no'] = "No";
$ec_iPortfolio['heading']['photo'] = "Photo";
$ec_iPortfolio['heading']['student_info'] = "Personal Information";
$ec_iPortfolio['heading']['student_name'] = "Student Name";
$ec_iPortfolio['heading']['student_record'] = "School Record";
$ec_iPortfolio['heading']['student_regno'] = "Student Reg. No.";
$ec_iPortfolio['heading']['school_record_updated'] = "School Record Updated";
$ec_iPortfolio['heading']['sb_scheme_updated'] = "School-based scheme Updated";
$ec_iPortfolio['heading']['lp_updated'] = "Learning Portfolio Updated";
$ec_iPortfolio['heading']['no_lp_active_stu'] = "No. of Students Activated iPortfolio";
$ec_iPortfolio['heading']['no_stu'] = "No.of Students";
$ec_iPortfolio['student_photo'] = "Student Photo";
$ec_iPortfolio['last_update'] = "Update the date";
$ec_iPortfolio['service_name'] = "Service Name";
$ec_iPortfolio['title'] = "Title";
$ec_iPortfolio['category'] = "Category";
$ec_iPortfolio['ele'] = "Components of OLE";
$ec_iPortfolio['ole_role'] = "Role of Participation";
$ec_iPortfolio['total_hours'] = "Total Hours";
$ec_iPortfolio['achievement'] = "Awards / Certifications / Achievements (if any)";
$ec_iPortfolio['details'] = "Details";
$ec_iPortfolio['school_remark'] = "School Remarks";
$ec_iPortfolio['class_and_number'] = "Class No.";
$ec_iPortfolio['comments'] = "Comment(s)";
$ec_iPortfolio['new_comments'] = " New Comments";
$ec_iPortfolio['last_update_time'] = "Last Updated";
$ec_iPortfolio['action_type'] = "Action Type";
$ec_iPortfolio['total_records'] = "Total Records";

$range_all = "All";
$list_item_no = "/Page";
$list_display = "Display";
$ec_iPortfolio['date'] = "Date";
$ec_iPortfolio['publish_date'] = "Publish Date";
$ec_iPortfolio['award_name'] = "Award";
$ec_iPortfolio['award_date'] = "Award Date";
$ec_iPortfolio['upload_cert'] = "Award/Proof";
$ec_iPortfolio['remark'] = "Remarks";
$ec_iPortfolio['SAMS_last_update'] = "Last update";
$ec_iPortfolio['award'] = "Award Record";
$ec_iPortfolio['teacher_comment'] = "Teacher's Comment";
$ec_iPortfolio['class_number'] = "Class No.";
$ec_iPortfolio['conduct_grade'] = "Conduct";
$ec_iPortfolio['comment_chi'] = "Chinese Comment";
$ec_iPortfolio['comment_eng'] = "English Comment";
$ec_iPortfolio['title_merit'] = "Merit/Demerit Comment";
$ec_iPortfolio['title_academic_report'] = "Academic Report";
$ec_iPortfolio['title_activity'] = "Activity";
$ec_iPortfolio['title_award'] = "Award";
$ec_iPortfolio['title_teacher_comments'] = "Teacher's Comments";
$ec_iPortfolio['title_attendance'] = "Attendance";
$ec_iPortfolio['total_late_count'] = "Total no. of Lateness";
$ec_iPortfolio['total_absent_count'] = "Total no. of Absence";
$ec_iPortfolio['total_earlyleave_count'] = "Total no. of Early Leave";
$ec_iPortfolio['stype'] = "Type";
$ec_iPortfolio['period'] = "Period";
$ec_iPortfolio['reason'] = "Reason";
$ec_iPortfolio['detail'] = "Details";
$ec_iPortfolio['student_report_print'] = "Student Report Printing";
$ec_iPortfolio['class'] = "Class";
$ec_iPortfolio_Report['subject'] = "Subject";
$ec_iPortfolio_Report['year'] = "School Year";
$ec_iPortfolio_Report['term'] = "Semester";
$ec_iPortfolio['report_title_year'] = "Year (學年)";
$ec_iPortfolio['report_title_class'] = "Class (班別)";
$ec_iPortfolio['report_title_classno'] = "Class No. (學號)";
$ec_iPortfolio['report_title_regno'] = "Reg. No. (編號)";
$ec_iPortfolio['report_title_name'] = "Name (姓名)";
$ec_iPortfolio['title_form_teacher_eng'] = "FORM TEACHER";
$ec_iPortfolio['title_form_teacher_chi'] = "班主任";
$ec_iPortfolio['title_principal_chi'] = "校長";
$ec_iPortfolio['title_principal_eng'] = "PRINCIPAL";
$ec_iPortfolio['title_guardian_chi'] = "家長/監護人";
$ec_iPortfolio['title_guardian_eng'] = "PARENT/GUARDIAN";
//$ec_iPortfolio['cutomized_report_remark_chi'] = "有關報告內之詳情，可參閱學生綜合表現紀錄小冊子";
//$ec_iPortfolio['cutomized_report_remark_eng'] = "For details, please refer to the handbook of Student Portfolio";
$ec_iPortfolio['title_eca'] = "Extra-Curricular Activities 課外活動";
$ec_iPortfolio['title_service'] = "Services in School 校內服務";
$ec_iPortfolio['title_award_punishment'] = "Awards & Punishment 獎懲資料";
$ec_iPortfolio['title_ole'] = "Other Learning Experiences 其他學習經歷";
$ec_iPortfolio['pic'] = "Person-in-charge";
$ec_iPortfolio['mtype'] = "Type";
$ec_iPortfolio['attendance'] = "Attendance Record";
$ec_iPortfolio['disable_review'] = "Disable";
$ec_iPortfolio['enable_review'] = "Enable";
$ec_iPortfolio['review_type'] = "Peer Review";
$ec_iPortfolio['peer_review_type'] = "Peer Review Type";
$ec_iPortfolio['school_review'] = "Peer Review within the Whole School";
$ec_iPortfolio['level_review'] = "Peer Review within Class Level";
$ec_iPortfolio['class_review'] = "Peer Review within Class";
$ec_iPortfolio['friends_review'] = "Also allow students to choose peer reviewer(s) from the whole school";
$ec_iPortfolio['yes'] = "Yes";
$ec_iPortfolio['no'] = "No";
$ec_iPortfolio['update_msg'] = "Record Updated.";
$ec_iPortfolio['peer_review_setting'] = "Peer Review Setting";
$ec_iPortfolio['attendance_detail'] = "Attendance Detail";
$ec_iPortfolio['merit_detail'] = "Merit/ Demerit Detail";
$ec_iPortfolio['generate_score_list'] = "Generate Score List";
$ec_iPortfolio['display'] = "Display";
$ec_iPortfolio['highlight_result'] = "Result Highlight";
$ec_iPortfolio['grade'] = "Grade";
$ec_iPortfolio['bold'] = "Bold";
$ec_iPortfolio['italic'] = "Italic";
$ec_iPortfolio['underline'] = "Underline";
$ec_iPortfolio['color'] = "Color";
$profiles_to = "to";
$ec_iPortfolio['red'] = "Red";
$ec_iPortfolio['green'] = "Green";
$ec_iPortfolio['blue'] = "Blue";
$ec_iPortfolio['growth_title'] = "Title";
$ec_iPortfolio['growth_phase_period'] = "Period";
$ec_iPortfolio['growth_description'] = "Description";
$ec_iPortfolio['growth_status'] = "Status";
$profile_modified = "Modified";
$ec_iPortfolio['web_view'] = "Learning Portfolio";
$ec_iPortfolio['portfolios'] = "Portfolios";
$ec_iPortfolio['templates'] = "Template Management";
$ec_iPortfolio['activate'] = "Activate";
$ec_iPortfolio['deactive'] = "Deactivate";
$ec_iPortfolio['delete'] = "Delete";
$ec_iPortfolio['edit'] = "Edit";
$ec_iPortfolio['edit_content'] = "Edit Content";
$ec_iPortfolio['your_comment'] = "Your Comment";
$StartTime = "Start Time";
$EndTime = "End Time";
$ec_iPortfolio['iportfolio_folder_size'] = "Storage Quota";
$ec_iPortfolio['size_unit'] = '(MB)';
$ec_iPortfolio['growth_group'] = "Participating Group";
$ec_iPortfolio_guide['group_growth'] = "If no group is selected, the scheme will be released to ALL students.";
$ec_iPortfolio['edit_this_page'] = "Edit this page";
$ec_iPortfolio['view_comment'] = "View Comments";
$ec_warning['growth_title'] = "Please enter the scheme title.";
$assignments_alert_msg13 = "Storage Quota must be positive value";
$assignments_alert_msg9 = "Invalid Date.";
$w_alert['start_end_time2'] = "Start time must not be greater than end time.";
$ec_iPortfolio['new_portfolios'] = "New Portfolios";
$ec_iPortfolio['mandatory_field_description']= "<span class='tabletextrequire'>*</span> Mandatory field(s)";
$con_msg_add = "Record Added.";
$con_msg_update = "Record Updated.";
$con_msg_del = "Record Deleted.";
$con_msg_save = "Record saved.";
$qbank_status = "Status";
$button_public = "Public";
$button_private = "Private";
$ec_iPortfolio['edit_portfolios'] = "Edit Portfolios";
$ec_iPortfolio['choose_group'] = "Choose Group";
$ec_iPortfolio['select_group'] = "Selected Group(s)";
$qbank_public = "Public";
$ec_iPortfolio['class'] = "Class";
$ec_iPortfolio_Report['form'] = "Form";
$ec_iPortfolio['SAMS_subject'] = "Subject";
$ec_iPortfolio['based_on'] = "Based on";
$ec_iPortfolio['view_stat'] = "View Statistics";
$ec_iPortfolio['master_score_list'] = "Class Subject Performance Summary";
$ec_iPortfolio['overall_perform_stat'] = "Subject Performance Statistic";
$ec_iPortfolio['class_perform_stat'] = "Term/Year Performance Statistic";
$ec_iPortfolio['student_perform_stat'] = "Individual Performance Statistic";
$ec_iPortfolio['mean'] = "Mean";
$ec_warning['please_select_class'] = "Please select class(es)!";
$ec_warning['please_select_subject'] = "Please select subject!";
$ec_iPortfolio['standard_deviation'] = "S.D.";
$ec_iPortfolio['highest_score'] = "Max.";
$ec_iPortfolio['lowest_score'] = "Min.";
$ec_iPortfolio['number_using_template'] = "Number of Learning Portfolio which is using this template";
$ec_iPortfolio['portfolios_mgt'] = "Portfolios";
$ec_iPortfolio['templates_mgt'] = "Template Management";
$ec_iPortfolio['form'] = "Form ";
$ec_iPortfolio['whole_school'] = "Whole School";
$ec_iPortfolio['school_yr'] = "All School Year";
$ec_iPortfolio['display_class_stat'] = "Display Class Statistic";
$ec_iPortfolio['display_class_distribution'] = "Display Class Statistic";
$ec_iPortfolio['all_statistic'] = "All Form Statistic";
$ec_iPortfolio['term'] = "Term";
$ec_iPortfolio['class'] = "Class";
$ec_iPortfolio['group_by'] = "Group by";
$ec_iPortfolio['school_year'] = "School Year";
$ec_iPortfolio['form'] = "Form";
$ec_iPortfolio['above_mean'] = "Above mean of the class level";
$ec_iPortfolio['below_mean'] = "Below mean of the class level";
$ec_iPortfolio['all_classlevel'] = "All Class Level";
$ec_iPortfolio['all_class'] = "All Class";
$ec_iPortfolio['all_ele'] = "All OLE Components";
$ec_iPortfolio['learning_portfolio_content'] = "Learning Portfolio Management";
$button_organize = "Organize";
$wording['contents_notice1'] = "Contents in <font color=#AAAAAA>gray</font> are private.";
$ec_iPortfolio['statistic'] = "Individual OLE Hours";
$ec_iPortfolio['adv_analyze'] = "Searching Report";
$notes_title = "Page title";
$ec_iPortfolio['notes_guide'] = "Instruction";
$ec_iPortfolio['notes_template'] = "Default template";
$button_new_template = "New Template";
$button_edit_template = "Edit Template";
$ec_guide['learning_portfolio_time_start'] = "If start-time is set, student can modify the content of this learning portfolio by this time setting.";
$ec_guide['learning_portfolio_time_end'] = "If end-time is set, student cannot modify the content of this learning portfolio by this time setting.";
$ec_iPortfolio['edit_template_alert'] = "Warning: This page is using template \'XXX\'. Any modification made on this page will be mirrored in other pages sharing the same template. To modify just this page, assign it a new template.";
$ec_iPortfolio['type'] = "Create";
$notes_position = "Position of the page";
$ec_iPortfolio['type_category'] = "File folder";
$ec_iPortfolio['type_document'] = "File";
$ec_warning['notes_title'] = "Please enter the title.";
$ec_iPortfolio['notes_method'] = "Input Format";
$ec_iPortfolio['notes_method_html'] = "HTML format";
$ec_iPortfolio['notes_method_form'] = "Form format";
$ec_iPortfolio['notes_method_weblog'] = "WebLog";
$ec_iPortfolio['notes_method_eClass'] = "eClass import";
$quizbank_desc = "Description";
$assignments_alert_msg6 = "HTML Preview";
$ec_iPortfolio['notes_relocate_Up'] = "Move up and before &quot;XXX&quot;";
$ec_iPortfolio['notes_relocate_Down'] = "Move down and after &quot;XXX&quot;";
$ec_iPortfolio['notes_relocate_DL'] = "Move up a level and after &quot;XXX&quot;";
$ec_iPortfolio['notes_relocate_DR'] = "Move down a level and in &quot;XXX&quot;";
$ec_iPortfolio['notes_relocate_UL'] = "Move up a level and before &quot;XXX&quot;";
$ec_iPortfolio['notes_relocate_UR'] = "Move down a level and in &quot;XXX&quot;";
$usertype_s = "Student";
$ec_iPortfolio['view_analysis'] = "View";
$ec_iPortfolio['hours_range'] = "Hours range";
$ec_iPortfolio['hours_above'] = "hours above";
$ec_iPortfolio['hours_below'] = "hours below";
$ec_iPortfolio['hours_to'] = "to";
$ec_iPortfolio['analysis_report'] = "Analysis Report";
$ec_iPortfolio['overall'] = "Overall";
$valueTitle = "Score";
$ec_iPortfolio['all_school_year'] = "All School Years";
$ec_iPortfolio['no_student'] = "No Students";
$ec_iPortfolio['ole_report_stat'] = "OLE Report";
$ec_iPortfolio['template_title'] = "Template title";
$ec_iPortfolio['copied_template_title'] = "Copied template title";
$ec_iPortfolio['full_report_config'] = "Full Report";
$ec_iPortfolio['slp_config'] = "Student Learning Profile";
$ec_iPortfolio['transcript_config'] = "Transcript";
$ec_iPortfolio['csv_report_config'] = "Import Learning Record";
$ec_iPortfolio['not_display_student_photo'] = "Student Photos";
$ec_iPortfolio['no_student_details'] = "Student Details";
$ec_iPortfolio['no_school_name'] = "School name (in the first page)";
$ec_iPortfolio['other_learning_record'] = "Other Learning Record";
$ec_iPortfolio['show_component_sub'] = "Show Component Subject";
$ec_iPortfolio['copy_web_portfolio_files'] = "Prepare Learning Portfolio files";
$ec_iPortfolio['remove_copied_files'] = "Remove prepared Learning Portfolio files";
$ec_iPortfolio['with_comment'] = "With comment";
$ec_iPortfolio['review_author'] = "Post:";
$ec_iPortfolio['finished_cd_burning_preparation'] = "The Learning Profolio files for students of class XXX are prepared to this folder:";
$ec_iPortfolio['get_prepared_portfolio'] = "Learning Profile files of students are put into folders which named by student number, teacher can burn CD-ROM according to the student number of students";
$ec_iPortfolio['prepare_other_class_cd_burning'] = "Prepare CD-ROM burning of other classes";
$ec_iPortfolio['copied_portfolio_reomved'] = "The prepared Learning Portfolio files for students of class XXX have been removed";
$ec_iPortfolio['copied_portfolio_reomved_fail'] = "The prepared Learning Portfolio files for students of class XXX are failed to be removed";
$ec_iPortfolio['preview'] = "Preview";
$ec_words['school_badge'] = "School Badge";
$ec_words['school_address'] = "School Address";
$ec_iPortfolio['transcript_description'] = "Student Transcript Description";
$ec_iPortfolio['transcript_CSV_file'] = "CSV Supplementary Info";
$ec_iPortfolio['not_display'] = "Do not display ";
$ec_iPortfolio['reading_record'] = "Reading Records";
$ec_iPortfolio['ole_record'] = "Other Learning Experience Records";
$ec_iPortfolio['file'] = "Files";
$ec_iPortfolio['iPortfolio_published'][1] = "<font color=green>Your iPortfolio has been successfully <u> published </u>!</font>";
$ec_iPortfolio['iPortfolio_published'][0] = "<font color=green>Your iPortfolio has been successfully <u> removed </u>!</font>";
$ec_iPortfolio['available_portfolio_acc'] = "Available iPortfolio account remaining (for whole school)";
$ec_iPortfolio['import_regno'] = "Import Reg No.";
$ec_iPortfolio['account_quota_free'] = "Available user account remaining";
$ec_iPortfolio['activation_student_activate'] = "No. of students for activation";
$ec_iPortfolio['activation_student_reactivate'] = "No. of students for RE-activation";
$ec_iPortfolio['activation_quota_afterward'] = "User account remaining after activation";
$ec_iPortfolio['activation_total_success'] = "No. of student accounts successfully activated";
$ec_iPortfolio['activation_total_fail'] = "No. of student accounts failed to be activated";
$namelist_class_number = "Class No.";
$ec_guide['fail_reason'] = "Reason for the Failure";
$ec_iPortfolio['activation_result_no_regno'] = "RegNo. missing";
$ec_iPortfolio['activation_result_activated'] = "This user account has been activated before this.";
$ec_guide['import_error_unknown'] = "Unknown";
$ec_iPortfolio['draft'] = "Draft";
$ec_iPortfolio_guide['manage_student_portfolio_msg'] = "You will manage Learning iPortfolio by changing to Student Role!";
$ec_iPortfolio['suspend_total_success'] = "No. of students suspended successfully";
$ec_iPortfolio['suspend_total_fail'] = "No. of students failed to be suspended";
$ec_iPortfolio['suspend_result_suspended'] = "This account has been suspended";
$ec_iPortfolio['suspend_result_inactive'] = "This account has not been activated";
$ec_iPortfolio['class_photo_number'] = "No. of photos/<br />Total no. of <span class='guide'>*</span>students";
$ec_iPortfolio['unknown_photo'] = "Unknown Photo";
$ec_iPortfolio['photo_num_guide'] = "* Refers to students who have activated their iPortfolio accounts.";
$ec_iPortfolio['photo_remove_confirm'] = "Are you sure you want to remove the uploaded photo?";
$ec_iPortfolio['student_photo_upload_warning'] = "The photo(s) must be zipped before uploading.";
$ec_iPortfolio['SAMS_CSV_file'] = "WEBSAMS CSV file";
$ec_guide['import_error_wrong_format'] = "Wrong import format";
$ec_guide['import_update_no'] = "No. of items successfully updated";
$ec_guide['import_error_row'] = "Row number";
$ec_guide['import_error_reason'] = "Reason for the Error";
$ec_guide['import_error_detail'] = "File info";
$ec_guide['import_error_no_user'] = "No such user";
$ec_guide['import_error_duplicate_regno'] = "WebSAMS RegNo already existing";
$ec_guide['import_error_activated_regno'] = "This student account has already been activated, the WebSAMS RegNo cannot be modified.";
$ec_guide['import_error_incorrect_regno'] = "# has to be added to RegNo";
$ec_guide['import_error_incorrect_regno_2'] = "RegNo must be a number";
$ec_guide['import_error_duplicate_classnum'] = "The class name and class number of this student are duplicated.";
$ec_guide['import_back'] = "Re-import";
$ec_iPortfolio['photo_removed_msg'] = "Photo are removed successfully";
$ec_iPortfolio['success_uploaded_photo_count'] = "No. of photos uploaded successfully";
$ec_iPortfolio['failed_uploaded_photo_count'] = "No. of photos failed to be uploaded";
$ec_iPortfolio['date_issue'] = "Date of Issue";
$ec_iPortfolio['student_learning_profile'] = "Student Learning Profile";
$ec_iPortfolio['student_particulars'] = "Student Particulars";
$ec_iPortfolio['school_code'] = "School Code";
$ec_iPortfolio['school_phone'] = "School Phone";
$ec_iPortfolio['academic_performance'] = "Academic Performance in School at Senior Secondary Level";
$ec_iPortfolio['full_mark'] = "Full Mark";
$ec_iPortfolio['mark_performance'] = "Mark/Performance in School";
$ec_iPortfolio['proj_ext_act_prog'] = "Name of Projects/Extension Activities/Programmes";
$ec_iPortfolio['award_achievement'] = "Awards and Achievements";
$ec_iPortfolio['list_award'] = "List of Awards and Major Achievements Issued by the School";
$ec_iPortfolio['student_self_account'] = "Student's 'Self-Account'";
$ec_iPortfolio['optional'] = "(Optional)";
$ec_iPortfolio['within_word_limit'] = "(For this part, no more than <!--EngLimit--> words in English or <!--ChiLimit--> words in Chinese)";
$ec_iPortfolio['end_report'] = "End of Report";
$ec_iPortfolio['student_self_account_desc'] = "In this part, student could provide additional information to highlight any aspects of his/her learning life and personal development during
the senior secondary education for readers’ (e,g. tertiary institutions, future employers) references. In order to give a fuller picture to the
readers, student could consider to provide information about their key achievements before the period of senior secondary ";
$ec_iPortfolio['place_of_birth'] = "Place of Birth";


#for Kei Wai Report
$ec_iPortfolio['keiwai_personal_info'] = "Personal Particulars";
$ec_iPortfolio['keiwai_education'] = "Education";
$ec_iPortfolio['keiwai_academic_results'] = "Academic Results";
$ec_iPortfolio['keiwai_professional_profile'] = "Professional Profile";
$ec_iPortfolio['keiwai_extra_data_manage'] ="Kei Wai Supplementory Data Management";
$ec_iPortfolio['keiwai_report'] ="Student's Achievement Form";
$ec_iPortfolio['keiwai_report_2'] ="Letter of Accomplishment";
$ec_iPortfolio['keiwai_HKID'] = "HKID";
$ec_iPortfolio['keiwai_religion'] = "Religion";
$ec_iPortfolio['pob'] = "Place of Birth";
$ec_iPortfolio['nationality'] = "Nationality";
$ec_iPortfolio['telno'] = "Tel. No.";
$ec_iPortfolio['keiwai_subject_area'] = "Subject Area";
$ec_iPortfolio['keiwai_organization_a'] = "Awarded by Organisation";
$ec_iPortfolio['keiwai_organization_b'] = "Organisation";
$ec_iPortfolio['keiwai_organization_c'] = "Organisation";
$ec_iPortfolio['service'] = "Service Record";
$ec_iPortfolio['female'] = "Female";
$ec_iPortfolio['male'] = "Male";
$ec_iPortfolio['institute'] = "Name of Educational Institution";
$ec_iPortfolio['doc'] = "Date of Completion";
$ec_iPortfolio['keiwai_period'] = "Period";
$ec_iPortfolio['keiwai_classlevel'] = "Class Level";
$ec_iPortfolio['keiwai_position_c'] = "Position in class";
$ec_iPortfolio['keiwai_totalnumber_c'] = "Total number of student in class";
$ec_iPortfolio['keiwai_position_l'] = "Position in level";
$ec_iPortfolio['keiwai_totalnumber_l'] = "Total number of student in the level";
$ec_iPortfolio['keiwai_certificate'] = "Certificate(s) Awarded";
$ec_iPortfolio['keiwai_qualification'] = "Qualification (with honors, classification, if applicable)";
$ec_iPortfolio['keiwai_organization'] = "Awarded by Organisation";

# Warning message
$ec_warning['comment_content'] = "Please enter your comments.";
$ec_warning['comment_submit_confirm'] = "Are you sure that you want to submit your comments?";
$ec_iPortfolio['suspend_account_confirm'] = "Are you sure that you want to suspend the intended user?";
$ec_iPortfolio['suspend_template_confirm'] = "Are you sure that you want to suspend the intended template(s)?";
$ec_iPortfolio['active_template_confirm'] = "Are you sure that you want to resume the intended template(s)?";
$ec_iPortfolio['delete_template_confirm'] = "Are you sure that you want to delete the intended template(s)?";
$ec_iPortfolio['used_template_cant_delete'] = "*Attention: Template which is being used cannot be deleted.";
$ec_warning['start_end_year'] = "End year cannot be earlier than start year";
$ec_warning['start_end_hours'] = "Maximum hours cannot be less than minimum hours";
$ec_warning['please_enter_pos_integer'] = "Please enter a positive integer";
$ec_iPortfolio['delete_attachment_confirm'] = "Are you sure that you want to delete the intended attachment?";
$ec_iPortfolio['student_photo_upload_remind'] = "The photos should be named with case-sensitive Student RegNo and zipped before uploading!<br />Photo must be in '.JPG' format and the Size should be fixed to 100 x 130 pixel (W x H).";
$ec_warning['start_end_scheme_start_time'] = "Start time cannot be earlier than scheme start time";
$ec_warning['start_end_scheme_end_time'] = "End time cannot be later than scheme end time";

# for semester sort name
$ec_iPortfolio['semester_name_array_1'] = array(1, '一', '上', 'first', '1st Semester');
$ec_iPortfolio['semester_name_array_2'] = array(2, '二', '中', 'second', '2nd Semester');
$ec_iPortfolio['semester_name_array_3'] = array(3, '三', '下', 'third', '3rd Semester');

$ec_student_word['registration_no'] = "Student RegNo";
$ec_student_word['name_english'] = "English Name";
$ec_student_word['name_chinese'] = "Chinese Name";

$MyInfo = "My Information";
$no_record_msg = "There is no record at the moment.";

$button_activate_iPortfolio = "Activate";
$button_publish_iPortfolio = "Publish";
$button_apply = "Apply";
$button_save_and_publish_iPortfolio = "Save & Publish";
$i_general_others = "Others";
$i_status_shared = "Shared";
$i_status_usable = "Usable";
$i_list_all = "List All";
$i_general_settings = "Settings";
$notes_after = "After";
$notes_under = "Subordinate to";

$msg_check_at_least_one = "Please choose at least one option.";
$msg_check_at_least_one_item = "Please choose at least one item.";
$msg_check_at_most_one = "Please choose one option only.";

$profile_gender = "Gender";
$profile_dob = "DOB";
$profile_nationality = "Nationality";
$profile_pob = "Place of Birth";


	$ec_iPortfolio['eClass_update_assessment'] = "Update Academic Record from eClass";
	$ec_iPortfolio['eClass_update'] = "Upate from eClass";
	$ec_iPortfolio['eClass_update_merit'] = "Update Merit/Demerit Record from eClass";
	$ec_iPortfolio['eClass_update_activity'] = "Update Activity Record from eClass";
	$ec_iPortfolio['eClass_update_attendance'] = "Update Attendance Record from eClass";
	$ec_iPortfolio['eClass_update_award'] = "Update Award record from eClass";
	$ec_iPortfolio['eClass_update_comment'] = "Update Teacher Comment from eClass";
	$ec_iPortfolio['update_more_assessment'] = "Update academic record again";
	$ec_iPortfolio['update_more_award'] = "Update award record again";
	$ec_iPortfolio['update_more_activity'] = "Update activity record again";
	$ec_iPortfolio['update_more_attendance'] = "Update attendance record again";
	$ec_iPortfolio['update_more_merit'] = "Update merit/demerit record again";
	$ec_iPortfolio['update_more_comment'] = "Update teacher comment again";
	$ec_iPortfolio['last_update'] = "Update the date";
	$ec_iPortfolio['SAMS_last_update'] = "Last update";
	$ec_iPortfolio['activation_no_quota'] = "Sorry, the account quota is insufficient!";
	$ec_iPortfolio['SAMS_code'] = "Code";
	$ec_iPortfolio['SAMS_cmp_code'] = "Component Code";
	$ec_iPortfolio['SAMS_description'] = "Description (Eng/Chi)";
	$ec_iPortfolio['SAMS_abbr_name'] = "Abbreviation (Eng/Chi)";
	$ec_iPortfolio['SAMS_short_name'] = "Short Form (Eng/Chi)";
	$ec_iPortfolio['SAMS_display_order'] = "Display order";
	$ec_iPortfolio['SAMS_subject'] = "Subject";
	$ec_iPortfolio['SAMS_short_name_duplicate'] = "Same short form is not allowed.";
	$ec_iPortfolio['WebSAMSRegNo'] = "WebSAMS RegNo";

	$ec_iPortfolio['record_submitted'] = "Submitted award record";
	$ec_iPortfolio['record_approved'] = "Approved award record";
	$ec_iPortfolio['house'] = "House";
	$ec_iPortfolio['pending'] = "Pending for approval";
	$ec_iPortfolio['approved'] = "Approved";
	$ec_iPortfolio['rejected'] = "Rejected";
	$ec_iPortfolio['accept'] = "Accept";
	$ec_iPortfolio['approve'] = "Approve";
	$ec_iPortfolio['reject'] = "Reject";
	$ec_iPortfolio['status'] = "Status";
	$ec_iPortfolio['source'] = "Source";
	$ec_iPortfolio['student_apply'] = "Student Application";
	$ec_iPortfolio['teacher_submit_record'] = "Teacher Submitted Record";
	$ec_iPortfolio['school_record'] = "School Record";
	$ec_iPortfolio['process_date'] = "Process Date";
	$ec_iPortfolio['submit_external_award_record'] = "Submit external award record";
	$ec_iPortfolio['total'] = "Total";
	$ec_iPortfolio['grade'] = "Grade";
	$ec_iPortfolio['score'] = "Score";
	$ec_iPortfolio['stand_score'] = "Standard Score";
	$ec_iPortfolio['rank'] = "Position in Form";
	$ec_iPortfolio['conduct_grade'] = "Conduct";
	$ec_iPortfolio['comment_chi'] = "Chinese Comment";
	$ec_iPortfolio['comment_eng'] = "English Comment";
	$ec_iPortfolio['detail'] = "Details";

	$ec_iPortfolio['all_status'] = "All Status";
	$ec_iPortfolio['all_category'] = "All Categorie";
	$ec_iPortfolio['title'] = "Title";
	$ec_iPortfolio['category'] = "Category";
	$ec_iPortfolio['category_code'] = "Category Code";
	$ec_iPortfolio['hours'] = "Hours";
	$ec_iPortfolio['achievement'] = "Awards / Certifications / Achievements";
	$ec_iPortfolio['organization'] = "Partner Organizations";
	$ec_iPortfolio['details'] = "Details";
	$ec_iPortfolio['approved_by'] = "Approved By";
	$ec_iPortfolio['attachment'] = "Attachment / Evidence";
	$ec_iPortfolio['attachment_only'] = "Attachment";
	$ec_iPortfolio['startdate'] = "Start Date";
	$ec_iPortfolio['enddate']= "End Date";
	$ec_iPortfolio['or'] = "or";
	$ec_iPortfolio['add_enddate'] = "Add End Date";
	$ec_iPortfolio['unit_hour'] = "hour(s)";
	$ec_iPortfolio['school_remarks'] = "School Remarks";
	$import_csv['download'] = "download sample ";
	$ec_iPortfolio['customized_file_upload_remind'] = "Attention: The uploaded file(s) should be named by the corresponding year. (e.g. 2006.csv ) ";


	$ec_warning['date'] = "Please enter the date.";
	$w_alert['Invalid_Date'] = "Invalid Date";
	$w_alert['Invalid_Time'] = "Invalid Time";
	$ec_warning['title'] = "Please enter the title.";
	$ec_iPortfolio['upload_error'] = "Please Enter Valid File Path";

	$ec_iPortfolio_ele_array = array("[ID]"=>"Intellectual Development",
									"[MCE]"=>"Moral and Civic Education",
									"[CS]"=>"Community Service",
									"[PD]"=>"Physical Development",
									"[AD]"=>"Aesthetic Development",
									"[CE]"=>"Career-related Experiences",
									"[OTHERS]"=>"Others");
	$ec_iPortfolio['type_competition'] = "Competition";
	$ec_iPortfolio['type_activity'] = "Activity";
	$ec_iPortfolio['type_course'] = "Course";
	$ec_iPortfolio['type_service'] = "Service";
	$ec_iPortfolio['type_award'] = "Award";
	$ec_iPortfolio['type_others'] = "Others";
	$ec_iPortfolio_category_array = array("1"=>$ec_iPortfolio['type_competition'],
										"2"=>$ec_iPortfolio['type_activity'],
										"3"=>$ec_iPortfolio['type_course'],
										"4"=>$ec_iPortfolio['type_service'],
										"5"=>$ec_iPortfolio['type_award'],
										"6"=>$ec_iPortfolio['type_others']);
	$ec_iPortfolio['waiting_approval']= "Waiting for Approval";
	$ec_iPortfolio['program']= "Programme";
	$ec_iPortfolio['program_list']= "Programme List";
	$ec_warning['olr_from_teacher'] = "This record is submmited by teacher. You do not need to approve it!";
	$ec_iPortfolio['student_list']= "Student List";
	$ec_iPortfolio['assign_more_student']= "Assign Student";
	$ec_iPortfolio['no_record_found'] = "No such record.";
	$ec_iPortfolio['no_privilege_to_read_file'] = "You don't have the access right to view this file.";
	$ec_iPortfolio['new_ole_activity'] = "New OLE Activity";
	$ec_iPortfolio['assign_student_now'] = "Assign Student Now";
	$ec_iPortfolio['comment'] = "Comment";
	$ec_iPortfolio['no_of_records'] = "No. of records";


$ip_lang['class_history_management'] = "Class History Management";

$iDiscipline['Conduct_Category'] = "Conduct Category";
$iDiscipline['Conduct_Item'] = "Conduct Item";
$iDiscipline['Good_Conduct_Import_FileDescription'] = "Example:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Record Date</td><td>Semester</td><td>Item Code</td><td>PIC</td><td>Remark</tr>
<tr><td>1A</td><td>11</td><td>2009-03-20</td><td>Fall Semester</td><td>Item01</td><td>Teacher01</td><td>Remark</td></tr></table>";
//$iDiscipline['Conduct_Mark_Import_FileDescription'] = "$i_general_Year, $i_SettingsSemester, $i_UserClassName, $i_ClassNumber, $i_Discipline_System_Discipline_Case_Record_PIC, $i_Discipline_System_Discipline_Conduct_Mark_Adjustment,$i_Attendance_Reason";
$iDiscipline['Conduct_Mark_Import_FileDescription'] = "Example:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Year</td><td>Semester</td><td>Adjustment</td><td>PIC</td><td>Reason</td></tr>
<tr><td>1A</td><td>12</td><td>2008-2009</td><td>Fall semester</td><td>20</td><td>Teacher01</td><td>Helping Teacher</td></tr>
</table>";
//$iDiscipline['Detention_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_Attendance_Reason, $i_UserRemark";
$iDiscipline['Detention_Import_FileDescription'] = "Example:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>$i_UserClassName</td><td>$i_ClassNumber</td><td>$i_Attendance_Reason</td><td>$i_UserRemark</td></tr>
<tr><td>1A</td><td>12</td><td>Late for school</td><td>Remark</td></tr>
</table>";
$iDiscipline['Good_Conduct_Missing_Category'] = "Missing Category";
$iDiscipline['Good_Conduct_Wrong_Category'] = "Wrong Category";
$iDiscipline['Good_Conduct_Missing_Conduct_Category'] = "Missing Conduct Category";
$iDiscipline['Good_Conduct_Missing_Conduct_Item'] = "Missing Conduct Item";
$iDiscipline['Good_Conduct_Missing_Conduct_Score'] = "Missing Conduct Mark";
$iDiscipline['Good_Conduct_No_Conduct_Item'] = "No such Conduct Item";
$iDiscipline['Good_Conduct_No_Conduct_Category'] = "No such Conduct Category";
$iDiscipline['Good_Conduct_No_Staff'] = "No such staff";
$iDiscipline['Good_Conduct_No_Student'] = "No such student";
$iDiscipline['Award_Punishment_Wrong_Merit_Type'] = "Wrong Merit Type";
$iDiscipline['Award_Punishment_ConductScore_Numeric'] = "Conduct Score has to be numeric";
$iDiscipline['Award_Punishment_No_Subject'] = "No such Subject";
$iDiscipline['Award_Punishment_No_Merit_Num'] = "Missing Merit Number";
$iDiscipline['Award_Punishment_Missing_Merit_Num'] = "Missing Merit Type";
$iDiscipline['Award_Punishment_Merit_Numeric'] = "Merit Number has to be numeric";
$iDiscipline['Award_Punishment_Merit_Negative'] = "Merit Number has to be positive";
$iDiscipline['Award_Punishment_Wrong_MeritType'] = "No such Category";
$iDiscipline['Award_Punishment_No_ItemCode'] = "No such Item Code";
$iDiscipline['Case_ItemCode_MeritType_Mismatch'] = "Item Code and Merit Type do not match";
$iDiscipline['Award_Punishment_Missing_ItemCode'] = "Missing Item Code";
$iDiscipline['Award_Punishment_Missing_Year'] = "Missing Year";
$iDiscipline['Award_Punishment_No_Year'] = "No such Year";
$iDiscipline['Award_Punishment_Missing_Semester'] = "Missing Semester";
$iDiscipline['Award_Punishment_RecordDate_In_Future'] = "Do not set Event Date after Current Date";
$iDiscipline['Award_Punishment_RecordDate_Error'] = "Event Date is not a date";
$iDiscipline['Award_Punishment_Missing_RecordDate'] = "Missing RecordDate";
$iDiscipline['Good_Conduct_Cat_Item_Mismatch'] = "Conduct Category and Conduct Item do not match";
$iDiscipline['Good_Conduct_No_Category'] = "No such Category";
$iDiscipline['Good_Conduct_No_Period_Setting'] = "Period was not set";
$iDiscipline['Good_Conduct_Category_Mismatch'] = "Conduct Category and Conduct Item do not match with Category";
$iDiscipline['Detention_Missing_Reason'] = "Missing Reason";
$iDiscipline['Assign_Duty_Teacher'] = "Please assign at least one person-in-charge.";

$iDiscipline['Form_Templates'] = 'Templates';
$iDiscipline['Form_answersheet_option'] = 'No. of Options';
$iDiscipline['Form_answersheet_header'] = 'Topic/Title';
$iDiscipline['Form_answersheet_content'] = 'Content';
$iDiscipline['Form_answersheet_type_selection'] = '-- Select Format --';
$iDiscipline['Form_answersheet_selection'] = '-- Select a Number --';
$i_Discipline_System_Discipline_Conduct_Mark_No_Ratio_Setting = 'Ratio is not set in this year';
//$iDiscipline['Case_Import_FileDescription'] = "$i_Discipline_System_Discipline_Case_Record_Case_Number,$i_Discipline_System_Discipline_Case_Record_Case_Title,$i_Discipline_System_Discipline_Case_Record_Case_Event_Date,$i_Discipline_System_Discipline_Case_Record_Case_Semester,$i_Discipline_System_Discipline_Case_Record_Case_Location,$i_Discipline_System_Discipline_Case_Record_Case_PIC";
$iDiscipline['Example'] = "Example";
$iDiscipline['Case_Import_FileDescription'] = "Example:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>$i_Discipline_System_Discipline_Case_Record_Case_Number</td><td>$i_Discipline_System_Discipline_Case_Record_Case_Title</td><td>$i_Discipline_System_Discipline_Case_Record_Case_Event_Date</td><td>$i_Discipline_System_Discipline_Case_Record_Case_Semester</td><td>$i_Discipline_System_Discipline_Case_Record_Case_Location</td><td>$i_Discipline_System_Discipline_Case_Record_PIC</td><td>Remark</td></tr>
<tr><td>001</td><td>Case 1</td><td>2009-03-20</td><td>Fall Semester</td><td>Classroom</td><td>Teacher01</td><td>Remark</td></tr>
</table>";
$iDiscipline['Case_Missing_CaseNumber'] = "Missing Case Number";
$iDiscipline['Case_Missing_CaseName'] = "Missing Case Name";
$iDiscipline['Case_Missing_Category'] = "Missing Category";
$iDiscipline['Case_Missing_EventDate'] = "Missing Event Date";
$iDiscipline['Case_Missing_Location'] = "Missing Location";
$iDiscipline['Case_Missing_PIC'] = "Missing PIC";
//$iDiscipline['Case_Student_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_general_record_date, $i_SettingsSemester,$i_Discipline_System_ItemCode,$i_Discipline_System_ConductScore,$i_Discipline_System_Discipline_Case_Record_PIC, ".$eDiscipline['Subject'].",$i_Discipline_System_Add_Merit,$i_ResourceCategory,".$iDiscipline['MeritType'].",$i_UserRemark";
/*$iDiscipline['Case_Student_Import_FileDescription'] = "Example:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>$i_UserClassName</td><td>$i_ClassNumber</td><td>$i_general_record_date</td><td>$i_SettingsSemester</td><td>$i_Discipline_System_ItemCode</td><td>$i_Discipline_System_ConductScore</td><td>$i_Discipline_System_Discipline_Case_Record_PIC</td><td>".$eDiscipline['Subject']."</td><td>$i_Discipline_System_Add_Merit</td><td>$i_ResourceCategory</td><td>".$iDiscipline['MeritType']."</td><td>$i_UserRemark</td></tr>
<tr><td>1A</td><td>12</td><td>2009-03-20</td><td>Fall Semester</td><td>LATE002F</td><td>3</td><td>Teacher01</td><td>English</td><td>1</td><td>Punishment</td><td>-2</td><td>Remark</td></tr>
</table>";*/
$iDiscipline['Case_Student_Import_No_Semester'] = "No such semester";
$iDiscipline['Case_Student_Import_Conduct_Score_OutRange'] = "Conduct Mark is out of range";
$iDiscipline['Case_Student_Import_Quantity_OutRange'] = "Quantity is out of range";
$iDiscipline['Case_Student_Import_Conduct_Score_Negative'] = "Conduct Mark has to be smaller than zero";
$iDiscipline['Case_Student_Import_Conduct_Score_Positive'] = "Conduct Mark has to be greater than zero";
$iDiscipline['Conduct_Mark_Import_Adjustment_OutRange'] = "Adjustment is out of range";

$eDiscipline['Detention_Arrangement'] = "Arrangement";
$eDiscipline['Session_Management'] = "Session Management";
$eDiscipline['Detention_Session_Default_String'] =  "$i_Discipline_Date | $i_Discipline_Time | $i_Discipline_Location | $i_Discipline_Vacancy | $i_Discipline_PIC";
$iDiscipline['Category_Item_Code_Warning'] = "Item code already exists or has been used/deleted before";
$iDiscipline['Reord_Import_Successfully'] = "Record(s) Import Successfully";
$iDiscipline['Invalid_Date_Format'] = "Invalid Date Format";
$iDiscipline['Import_Instruct_Main'] = "To import, please do the following:";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1'] = "Press ";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2'] = "here";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3'] = "to download the CSV template.";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1'] = "Fill in the template following this example:";
$iDiscipline['Import_Instruct_Note'] = "Note:";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_1'] = "In the CSV source file, you need to indicate each record's corresponding good conduct/misconduct item using \"item code\". You can view a list of good conduct/misconduct items and their item codes";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2'] = "here";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_2_1'] = "For \"Record Date\" column, please enter dates in YYYY-MM-DD format. ";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_c_1'] = "Save the CSV source file. Enter the path manually or browse your hard drive to locate the file for upload. Then, press <strong>Continue</strong>.";

$iDiscipline['Award_Punishment_Import_Instruct_Note_1_1'] = "In the CSV source file, you need to indicate each record's corresponding <i>award/punshment</i> item using \"item code\". You can view a list of <i>award/punshment</i> item and their item codes ";
$iDiscipline['Award_Punishment_Import_Instruct_Note_1_2'] = "here";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_1'] = "<b>If you are upgrading from eDiscipline 1.0</b>: You need to indicate each record's corresponding subject using \"subject code\". You can view a list of subjects and their subject codes ";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_2'] = "here";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_3'] = ".You can leave this column empty if it is not applicable.";

$iDiscipline['Award_Punishment_Import_Instruct_Note_3_1'] = "Please enter the kind and number of merits/demerits corresponding to each record into the \"Type\" and \"Quantity\" columns, with reference to the following codes.";
$iDiscipline['Award_Punishment_Import_Instruct_Note_3_2'] = "View code list";
$iDiscipline['Award_Punishment_Import_Instruct_Note_3_3'] = "For example, to enter \"3 minor demerits\", please enter \"3\" and \"{code corresponding to minor demerit}\" into the \"Quantity\" and \"Type\" columns respectively. ";
$iDiscipline['Award_Punishment_Import_Instruct_Note_4_1'] = "For \"Record Date\" column, please enter dates in YYYY-MM-DD format.";
$iDiscipline['Award_Punishment_Import_Instruct_c_1'] = "Save the CSV source file. Enter the path manually or browse your hard drive to locate the file for upload. Then, press <strong>Continue</strong>.";

$iDiscipline['Case_Record_Import_Instruct_Note_1_1'] = "For \"Record Date\" column, please enter dates in YYYY-MM-DD format. ";
$iDiscipline['Case_Record_Import_Instruct_c_1'] = "Save the CSV source file. Enter the path manually or browse your hard drive to locate the file for upload. Then, press <strong>Continue</strong>.";
$iDiscipline['Import_Source_File'] = "CSV Scoure File";
$iDiscipline['Import_Source_File_Note'] = "The \"Source File\" field is used to enter the CSV source file's path. You may either enter the path manually or browse your hard drive to locate the file.";


###############################################################################
# scrabble fun corner
###############################################################################
$word_sfc['title'] = "Fun Corner";
$word_sfc['description'] = "The Scrabble Fun Corner is an easy accessible site to help educate teachers and students about the rules and tricks of the Scrabble game.";


# 20090403 yatwoon (semester related)
$semester_change_warning = "Warning: Please DO NOT modify Semester Mode before the current school year ends. This can damage the integrity of data and cause unpredicatable result.";



###############################################################################

###############################################################################
# LSLP
###############################################################################
$i_LSLP['LSLP'] = "LSLP";
$i_LSLP['admin_user_setting'] = "Admin User Setting";
$i_LSLP['user_license_setting'] = "User License Setting";
$i_LSLP['license_display_1'] = "Your LSLP license is ";
$i_LSLP['license_display_2'] = " , with ";
$i_LSLP['license_display_3'] = " license(s) left. ";
$i_LSLP['license_display_4'] = "You have used up ";
$i_LSLP['license_display_5'] = " .";
$i_LSLP['used_up_all_license_msg'] = "All the license is used all";
$i_LSLP['license_period_expired'] = "License is expired";
$i_LSLP['license_exceed_msg1'] = "You only have ";
$i_LSLP['license_exceed_msg2'] = " license left	";
$i_LSLP['license_exceed_msg3'] = "Please proceed back";
$i_LSLP['school_license'] = "School License";
$i_LSLP['student_license'] = "Student License";
$i_LSLP['license_type'] = "License Type";
$i_LSLP['license_period'] = "License Period";
###############################################################################

##########################################
# eHomework   add by Sunny 20090616 18:42
##########################################
$eHomework['Management'] = "Management";
$eHomework['Homework_List'] = "Homework List";
$eHomework['Subject_Leader'] = "Subject Leader";
$eHomework['Reports'] = "Reports";
$eHomework['Weekly_Homework_List'] = "Weekly Homework List";
$eHomework['Not_Submit_List'] = "Not Submit List";
$eHomework['Statistics']="Statistics";
$eHomework['New Record'] = "New Record";
$eHomework['Import Record'] = "Import Record";
$eHomework['Edit Record'] = "Edit Record";
$eHomework['Assign Subject Leader'] = "Assign Subject Leader";

$eHomework_Homework_List_All_School_Year = "All School Years";
$eHomework_Homework_List_Whole_Year = "Whole Year";
$eHomework_Homework_List_All_Classes = "All Classes";
$eHomework["Record"] = "Record";
$eHomework['Import_Source_File'] = "CSV Scoure File";
$eHomework['Import_Source_File_Note'] = "The \"CSV Source File\" field is used to enter the CSV source file's path. You may either enter the path manually or browse your hard drive to locate the file.";
$eHomework['ImportOtherRecords'] = "Import other records";
$eHomework_class = "Class";
$eHomework_subject = "Subject";
$eHomework_topic = "Topic";
$eHomework_loading = "Loading";
$eHomework_workload = "Workload";
$eHomework_pic = "PIC";
$eHomework_startDate = "Start Date";
$eHomework_dueDate = "Due Date";
$eHomework_description = "Description";
$eHomework_handin = "Hand-in Required";
$eHomework_remark = "Remark";
$eHomework_Search_Alert = "Please fill in the content";
$eHomework_to_do_list = "To-do-list";
$eHomework_history = "History";
$eHomework_teacher = "Teacher";


# 20090627 yat woon - eRC reports
$i_alert_PleaseSelectClassForm = "Please select Class or Form";

# always stay at the end of this file!!
if (is_file("$intranet_root/templates/lang.$intranet_session_language.customized.php"))
{
  include_once("$intranet_root/templates/lang.$intranet_session_language.customized.php");
}

# if setting of merit/demerit title are set
# load the setting
$merit_demerit_customize_file = "$intranet_root/file/merit.en.customized.txt";
if(file_exists($merit_demerit_customize_file) && is_file($merit_demerit_customize_file) && filesize($merit_demerit_customize_file)!=0){
	if ($file_content_merit_demeirt_wordings=="")
		$file_content_merit_demeirt_wordings = get_file_content($merit_demerit_customize_file);
	$lines = explode("\n",$file_content_merit_demeirt_wordings);

	$i_Merit_Merit = $lines[0];
	$i_Merit_MinorCredit = $lines[1];
	$i_Merit_MajorCredit = $lines[2];
	$i_Merit_SuperCredit = $lines[3];
	$i_Merit_UltraCredit = $lines[4];

	$i_Merit_BlackMark = $lines[5];
	$i_Merit_MinorDemerit = $lines[6];
	$i_Merit_MajorDemerit = $lines[7];
	$i_Merit_SuperDemerit = $lines[8];
	$i_Merit_UltraDemerit = $lines[9];
}

$i_Merit_TypeArray = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_TypeArrayWithWarning = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_Warning,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_studentAttendance_HostelAttendance_InputReason = 'Please Input Reason';

?>