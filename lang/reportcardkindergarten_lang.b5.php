<?php
$Lang['eReportCardKG']['Title'] = "(成績表)";

$Lang['eReportCardKG']['General']['NoRecord'] = '沒有資料';
$Lang['eReportCardKG']['General']['PleaseConfirmChanges'] = '修改內容後或會影響系統其他功能，繼續前請先再作確認。';
$Lang['eReportCardKG']['General']['PleaseConfirmScoreUpdate'] = '呈送後原有輸入資料會被覆蓋。';

$Lang['eReportCardKG']['Management']['LearningZone']['Title'] = "學習區";

$Lang['eReportCardKG']['Management']['TopicTimeTable']['Title'] = "主題時間表";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['StartDate'] = "開始時間";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['EndDate'] = "結束時間";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseEnterTopic'] = "請選擇主題庫";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseEnterYear'] = "請選擇應用級別";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['NoLearningTool'] = "沒有教具";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['NumOfZones'] = "學習區數目";
$Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseConfirmChanges'] = '學習區設定已經更改，如要儲存已更改的設定，請按"呈送"。';

$Lang['eReportCardKG']['Management']['ToolScore']['Title'] = "教具分數修改";
$Lang['eReportCardKG']['Management']['ToolScore']['ToolList'] = "教具列表";
$Lang['eReportCardKG']['Management']['ToolScore']['InputScore'] = "輸入分數";
$Lang['eReportCardKG']['Management']['ToolScore']['AllTimeTable'] = "所有時間表";
$Lang['eReportCardKG']['Management']['ToolScore']['TimeTable'] = "時間表";
$Lang['eReportCardKG']['Management']['ToolScore']['TitleName'] = "主題";
$Lang['eReportCardKG']['Management']['ToolScore']['ClassName'] = "班別";
$Lang['eReportCardKG']['Management']['ToolScore']['TeachingTools'] = "教具";
$Lang['eReportCardKG']['Management']['ToolScore']['ClassNumber'] = "學號";
$Lang['eReportCardKG']['Management']['ToolScore']['StudentName'] = "學生";
$Lang['eReportCardKG']['Management']['ToolScore']['AddNewInputScoreRow'] = "增加輸入行數";
$Lang['eReportCardKG']['Management']['ToolScore']['MaxAddNewInputScoreRow'] = "每次行數增加不可以多於20";
$Lang['eReportCardKG']['Management']['ToolScore']['InputMarkInvalid'] = "輸入分數無效";
$Lang['eReportCardKG']['Management']['ToolScore']['InputMarkCannotBeNegative'] = "所需入分數不可為負數。";
$Lang['eReportCardKG']['Management']['ToolScore']['InputMarkCannotBeLargerThanFullMark'] = "所輸入的分數不可高於滿分。";

$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Title'] = "科目能力指標分數修改";
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputScore'] = "輸入分數";
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Form'] = '級別';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Subject'] = '科目';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['TermTimeTable'] = '學期  / 主題時間表';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['Term'] = '學期';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['TimeTable'] = '主題時間表';
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputMarkInvalid'] = "輸入分數無效";
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputMarkCannotBeNegative'] = "所需入分數不可為負數。";
$Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputMarkCannotBeLargerThanFullMark'] = "所輸入的分數不可高於滿分。";

$Lang['eReportCardKG']['Management']['LanguageBehavior']['Title'] = "語文能力及行為指標分數修改";
$Lang['eReportCardKG']['Management']['LanguageBehavior']['InputScore'] = "輸入分數";

$Lang['eReportCardKG']['Management']['OtherInfo']['Title'] = "其他資料";
$Lang['eReportCardKG']['Management']['OtherInfoArr']['summary'] = "學生資料";
$Lang['eReportCardKG']['Management']['OtherInfoArr']['award'] = "獎項";

$Lang['eReportCardKG']['Management']['OtherInfoWarningArr']['InputGrade'] = "輸入等級";
$Lang['eReportCardKG']['Management']['OtherInfoWarningArr']['InputNumber'] = "輸入數字";

$Lang['eReportCardKG']['Management']['GenerateReports']['Title'] = "列印學生成績表";

$Lang['eReportCardKG']['Management']['ExportPerformance']['Title'] = "匯出學生表現";
$Lang['eReportCardKG']['Management']['ExportYearlyPerformance']['Title'] = "匯出學生全年表現";

$Lang['eReportCardKG']['Management']['DataTransition']['Title'] = "資料處理";
$Lang['eReportCardKG']['Management']['DataTransition']['Transition'] = "轉變";
$Lang['eReportCardKG']['Management']['DataTransition']['ActiveAcademicYear'] = "系統使用中的學年";
$Lang['eReportCardKG']['Management']['DataTransition']['CurrentAcademicYear'] = "本學年";
$Lang['eReportCardKG']['Management']['DataTransition']['NewAcademicYear'] = "新學年";
$Lang['eReportCardKG']['Management']['DataTransition']['OtherAcademicYears'] = "其他學年";
$Lang['eReportCardKG']['Management']['DataTransition']['DataTransitionWarning1'] = "請在以下兩種情況發生前，執行此功能:<br />- 於學年始時，重新啟用eReportCard成績表系統<br />- 還原以往某學年eReportCard成績表系統的資料";
$Lang['eReportCardKG']['Management']['DataTransition']['DataTransitionWarning2'] = "- 系統內現有資料將會被整存。";
$Lang['eReportCardKG']['Management']['DataTransition']['ConfirmCreateNewYearDatabase'] = "新學年資料會複製自學年<!--academicYearName-->，你是否確定轉變系統至新學年？";
$Lang['eReportCardKG']['Management']['DataTransition']['ConfirmTransition'] = "確定進行所選擇的資料轉變?";

$Lang['eReportCardKG']['Management']['Device']['Title'] = "上課平板介面";
$Lang['eReportCardKG']['Management']['Device']['LearningZone'] = "學習區";
$Lang['eReportCardKG']['Management']['Device']['InputScore'] = "課堂入分";
$Lang['eReportCardKG']['Management']['Device']['Class'] = "班別";
$Lang['eReportCardKG']['Management']['Device']['Topic'] = "主題";
$Lang['eReportCardKG']['Management']['Device']['NoClasses'] = "無所屬班別";
$Lang['eReportCardKG']['Management']['Device']['NoTopics'] = "無可以選擇主題";
$Lang['eReportCardKG']['Management']['Device']['NotSelectAnyClasses'] = "無選擇任何班別";
$Lang['eReportCardKG']['Management']['Device']['NotSelectAnyTopics'] = "無選擇任何主題";
$Lang['eReportCardKG']['Management']['Device']['CompleteInputScore'] = "已評分";
$Lang['eReportCardKG']['Management']['Device']['IncompleteInputScore'] = "未評分";
$Lang['eReportCardKG']['Management']['Device']['AllEntryStatus'] = "全部";
$Lang['eReportCardKG']['Management']['Device']['InZoneStatus'] = "已進入";
$Lang['eReportCardKG']['Management']['Device']['CompleteStatus'] = "已完成";
$Lang['eReportCardKG']['Management']['Device']['IncompleteStatus'] = "未完成";
$Lang['eReportCardKG']['Management']['Device']['NoQuota'] = "學習區已滿";
$Lang['eReportCardKG']['Management']['Device']['PleaseLeaveFirst1'] = "你仍未離開學習區[";
$Lang['eReportCardKG']['Management']['Device']['PleaseLeaveFirst2'] = "]";
$Lang['eReportCardKG']['Management']['Device']['PleaseSelectTool'] = "請選擇教具";

$Lang['eReportCardKG']['Setting']['MacauAbilityIndex']['Title'] = "澳門基本學力要求";
$Lang['eReportCardKG']['Setting']['MacauAbilityIndex']['Code'] = "編碼";
$Lang['eReportCardKG']['Setting']['MacauAbilityIndex']['Name'] = "基本學力要求";

//$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Title'] = "台灣能力指標";
$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Title'] = "能力指標";
$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Code'] = "編碼";
$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Name'] = "能力指標";

$Lang['eReportCardKG']['Management']['AbilityRemarks']['Title'] = '能力指標評語 (級別及學期設定)';
$Lang['eReportCardKG']['Management']['AbilityRemarks']['Reset'] = '重設評語';
$Lang['eReportCardKG']['Management']['AbilityRemarks']['ConfirmResetRemark'] = '你確定要重設評語？';
$Lang['eReportCardKG']['Management']['AbilityRemarks']['ChangeAbilityResetAll'] = '如更改能力指標，所有相關評語設定將被同時重設。你是否確定更改至已選的能力指標？';

$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Title'] = '能力指標評語';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Grading'] = '評級';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['UpperLimit'] = '評級範圍';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Comment'] = '能力指標評語 (原始)';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['OriComment'] = '能力指標評語 (自定)';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['NewComment'] = '能力指標評語 (級別及學期設定)';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Term'] = '學期';
$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Year'] = '學年';

$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['Title'] = "能力指標配對";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['MacauCode'] = "澳門編碼";
//$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['TaiwanCode'] = "台灣編碼";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['TaiwanCode'] = "能力指標編碼";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllMacauCat'] = "所有澳門編碼類別";
//$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanCat'] = "所有台灣編碼類別";
//$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanLevel'] = "所有台灣級別";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanCat'] = "所有能力指標編碼類別";
$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['AllTaiwanLevel'] = "所有能力指標級別";

$Lang['eReportCardKG']['Setting']['FormTopic']['Title'] = "主題庫";
$Lang['eReportCardKG']['Setting']['LearningZone']['Title'] = "學習區庫";
$Lang['eReportCardKG']['Setting']['TeachingTool']['Title'] = "教具庫";
$Lang['eReportCardKG']['Setting']['TeachingToolCategory']['Title'] = "教具類別";
$Lang['eReportCardKG']['Setting']['LanguageBehavior']['Title'] = "語文能力及行為指標";
$Lang['eReportCardKG']['Setting']['LanguageBehaviorItem']['Title'] = "語文能力及行為指標評估項目";
$Lang['eReportCardKG']['Setting']['LanguageBehavior']['Category1'] = "語文能力";
$Lang['eReportCardKG']['Setting']['LanguageBehavior']['Category2'] = "行為指標";

$Lang['eReportCardKG']['Setting']['Code'] = "編號";
$Lang['eReportCardKG']['Setting']['Type'] = "類型";
$Lang['eReportCardKG']['Setting']['Name'] = "名稱";
$Lang['eReportCardKG']['Setting']['NameB5'] = "中文名稱";
$Lang['eReportCardKG']['Setting']['NameEN'] = "英文名稱";
$Lang['eReportCardKG']['Setting']['ApplyForm'] = "應用級別";
$Lang['eReportCardKG']['Setting']['ZoneTopic'] = "所屬主題";
$Lang['eReportCardKG']['Setting']['ZoneQuota'] = "容納人數";
$Lang['eReportCardKG']['Setting']['PhotoRemarks'] = "建議解像度: 300px * 300px (闊 * 高)";
$Lang['eReportCardKG']['Setting']['PleaseInputContent'] = "請輸入內容";
$Lang['eReportCardKG']['Setting']['PleaseInputNum'] = "請輸入數字";
$Lang['eReportCardKG']['Setting']['PleaseSelectForm'] = "請選擇一個級別";
$Lang['eReportCardKG']['Setting']['PleaseOneOption'] = "請選擇一個選項";
$Lang['eReportCardKG']['Setting']['Photo'] = "相片";
$Lang['eReportCardKG']['Setting']['Picture'] = "學習區圖象";
$Lang['eReportCardKG']['Setting']['AbilityIndex'] = "能力指標";
$Lang['eReportCardKG']['Setting']['Year'] = "應用級別";
$Lang['eReportCardKG']['Setting']['Remarks'] = "備註";
$Lang['eReportCardKG']['Setting']['ToolCategory'] = "教具類別";
$Lang['eReportCardKG']['Setting']['Chapter'] = "單元";

$Lang['eReportCardKG']['Setting']['InputWarning'] = "請輸入";
$Lang['eReportCardKG']['Setting']['DuplicateWarning'] = "重複";
$Lang['eReportCardKG']['Setting']['EditWarning']['NoMoreThan1'] = "編緝數目不能大於一";
$Lang['eReportCardKG']['Setting']['EditWarning']['PleaseSelectEdit'] = "請選擇編緝項目";
$Lang['eReportCardKG']['Setting']['DeleteWarning']['PleaseSelectDelete'] = "請選擇刪除項目";

$Lang['eReportCardKG']['Setting']['SubjectMapping']['Title'] = '科目能力指標配對';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['Form'] = '級別';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['Subject'] = '科目';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['TermTimeTable'] = '學期  / 主題時間表';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['Term'] = '學期';
$Lang['eReportCardKG']['Setting']['SubjectMapping']['TimeTable'] = '主題時間表';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TermInUse'] = '此級別科目已設定使用學期';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TermUsed'] = '此學期已被使用';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TermEmpty'] = '請輸入學期';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TopicInUse'] = '此級別科目已設定使用主題時間表';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TopicUsed'] = '此主題時間表已被使用';
$Lang['eReportCardKG']['Setting']['SubjectMappingJSArr']['TopicEmpty'] = '請輸入主題時間表';

$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['ModuleTitle'] = '能力指標分類配對 (級別及學期設定)';
$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['Title'] = '能力指標分類';
$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['Selections'] = '分類選項';
$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['None'] = '無分類';

$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Title'] = '管理時間表';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Term'] = '學期';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['StartDate'] = '開始日期';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['StartTime'] = '開始時間';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['EndDate'] = '結束日期';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['EndTime'] = '結束時間';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ModifiedDate'] = '最後更新日期';
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['InputScorePeriod'] = "老師呈交分數";
$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ViewReportPeriod'] = "家長列印成績表";

$Lang['eReportCardKG']['Management']['AwardGeneration']['Title'] = "選擇/製作學生獎項";
$Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'] = "學生獎項";
$Lang['eReportCardKG']['Management']['AwardGeneration']['SelectAward'] = "選擇獎項";
$Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAward'] = "製作獎項";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReceivedAwardCount'] = "得獎學生數目";
$Lang['eReportCardKG']['Management']['AwardGeneration']['LastGenerate'] = "上一次製作";
$Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAll'] = "製作所有班別獎項";

$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoAward'] = "請選擇獎項。";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoClass'] = "請選擇班別。";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoStudentInClass'] = "此班別沒有學生";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['NoAwardedStudentSettings'] = "沒有學生獲得此獎項";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['AtLeastOneStudent'] = "請選擇至少一個學生。";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['DeleteAward'] = "你是否確定要刪除此學生的此獎項？";
$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['ReGenerateAward'] = "如製作獎項，所有已製作的獎項將被刪除。你是否確定要製作獎項？";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardGenerateSuccess'] = "1|=|製作獎項成功。";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardGenerateFailed'] = "0|=|製作獎項失敗。";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteSuccess'] = "1|=|刪除獎項成功。";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteFailed'] = "0|=|刪除獎項失敗。";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddSuccess'] = "1|=|新增獎項成功。";
$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddFailed'] = "0|=|新增項失敗。";


$Lang['eReportCardKG']['Setting']['AwardsList']['Title'] = "學生獎項庫";
$Lang['eReportCardKG']['Setting']['AwardsList']['AwardType'] = "獎項種類";
$Lang['eReportCardKG']['Setting']['AwardsList']['AwardTypeArr']['G'] = "系統製作獎項";
$Lang['eReportCardKG']['Setting']['AwardsList']['AwardTypeArr']['I'] = "老師選擇獎項";
# Settings > Admin Group
$Lang['eReportCardKG']['Setting']['AdminGroup']['MenuTitle'] = '行政小組';
$Lang['eReportCardKG']['Setting']['AdminGroup']['Title'] = '行政小組';
$Lang['eReportCardKG']['Setting']['AdminGroup']['NumOfMember'] = "組員數目";
$Lang['eReportCardKG']['Setting']['AdminGroup']['NewAdminGroupStep'][1] = "資料及權限";
$Lang['eReportCardKG']['Setting']['AdminGroup']['NewAdminGroupStep'][2] = "選擇成員";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupCode'] = "行政小組代號";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupName'] = "行政小組名稱";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupList'] = "行政小組列表";
$Lang['eReportCardKG']['Setting']['AdminGroup']['MemberList'] = "組員列表";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AddMember'] = "加入組員";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupInfo'] = "行政小組資料";
$Lang['eReportCardKG']['Setting']['AdminGroup']['AccessRight'] = "權限";

$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['AddSuccess'] = "1|=|新增行政小組成功。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['AddFailed'] = "0|=|新增行政小組失敗。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['EditSuccess'] = "1|=|編輯行政小組成功。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['EditFailed'] = "0|=|編輯行政小組失敗。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['DeleteSuccess'] = "1|=|刪除行政小組成功。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['DeleteFailed'] = "0|=|刪除行政小組失敗。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['AddMemberSuccess'] = "1|=|已加入行政小組組員。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['AddMemberFailed'] = "0|=|行政小組組員加入失敗。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['DeleteMemberSuccess'] = "1|=|已刪除行政小組組員。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ReturnMessage']['DeleteMemberFailed'] = "0|=|行政小組組員刪除失敗。";
$Lang['eReportCardKG']['Setting']['AdminGroup']['ModifiedDate'] = '最後更新日期';

$Lang['eReportCardKG']['Setting']['AdminGroup']['Warning']['SelectionContainsMember'] = "已選擇的用戶中包含此行政小組的成員";
$Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['Blank']['Code'] = "不能沒有代號";
$Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['Blank']['Name'] = "不能沒有名稱";
$Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['InUse']['Code'] = "此代號已被使用";

$Lang['eReportCardKG']['Management']['ArchiveReport']['Title'] = "成績表存檔";
$Lang['eReportCardKG']['Management']['ArchiveReport']['LastArchived'] = "上一次存檔";
$Lang['eReportCardKG']['Management']['ArchiveReport']['ConfirmArchiveReport'] = '確定要覆蓋原有成績表存檔？';

$Lang['eReportCardKG']['Management']['PrintArchiveReport']['Title'] = "列印成績表存檔";
$Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchTerm'] = "沒有對應的學期";
$Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchClass'] = "沒有對應的班別";
$Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchStudent'] = "沒有對應的學生";
?>