<?php
if ($sys_custom['DHL']) {
	// Editing by anna
	$Lang['Header']['HomeTitle'] = "DHL Hong Kong - Employee Mobile App Platform";
	
	$Lang["PageTitle"] = "DHL Hong Kong - Employee Mobile App Platform";
	$Lang['Header']['Menu']['eAdmin'] = "行政管理工具";
	$Lang['Header']['Menu']['MessageCenter'] = "推送訊息";
	$Lang['Header']['Menu']['schoolNews'] = "企業新聞";
	$Lang['SysMgr']['SchoolNews']['SchoolNews'] = "企業新聞";
	$Lang['Header']['Menu']['Settings'] = "設定";
	$Lang['Header']['Menu']['eClassApp'] = "App";
	$Lang['Header']['Menu']['Organization'] = "組織設定";
	$Lang['Header']['Menu']['StaffAccount'] = "用戶管理";
	$Lang['Header']['Menu']['TeacherAppsNotify'] = "用戶通知  (Apps)";
	
	$Lang['AccountMgmt']['Menu_ThreeAccMgmt'] = "";
	$Lang['SysMgr']['RoleManagement']['eCircularAdmin'] = "職員通告管理";
	$Lang['SysMgr']['RoleManagement']['eClassTeacherAppAdmin'] = "App 管理";
	$Lang['SysMgr']['RoleManagement']['StaffAttendance'] = "考勤系統管理";
	$Lang['SysMgr']['RoleManagement']['TeacherAppNotify'] = "推送訊息管理";
	$Lang['SysMgr']['RoleManagement']['OrganizationAdmin'] = "組織管理";
	$Lang['SysMgr']['RoleManagement']['TeachingStaff'] = "職員";
	$Lang['SysMgr']['RoleManagement']['eAdmin'] = "行政管理工具";
	$Lang['SysMgr']['RoleManagement']['SchoolSettings'] = "設定";
	$Lang['SysMgr']['RoleManagement']['schoolNews'] = "企業新聞管理";
	
	$Lang['eClassApp']['ModuleNameAry']['SchoolInfo'] = '公司資訊';
	$Lang['eClassApp']['ModuleNameAry']['GroupMessage'] = '小組訊息';
	$Lang['eClassApp']['TeacherApp'] = 'App';
    $Lang['eClassApp']['AllStaffs'] = '所有職員';
	
	$Lang['DHL'] = array();
	$Lang['DHL']['Organization'] = "組織";
	$Lang['DHL']['Company'] = "公司";
	$Lang['DHL']['Division'] = "分區";
	$Lang['DHL']['Department'] = "部門";
	$Lang['DHL']['Chinese'] = "中文";
	$Lang['DHL']['English'] = "English";
	$Lang['DHL']['Code'] = "代碼";
	$Lang['DHL']['Description'] = "描述";
	$Lang['DHL']['CompanyName'] = "公司名稱";
	$Lang['DHL']['CompanyPIC'] = "公司負責人";
	$Lang['DHL']['DivisionName'] = "分區名稱";
	$Lang['DHL']['DivisionPIC'] = "分區負責人";
	$Lang['DHL']['DepartmentName'] = "部門名稱";
	$Lang['DHL']['RelatedCompany'] = "所屬公司";
	$Lang['DHL']['RelatedDivision'] = "所屬分區";
	$Lang['DHL']['DepartmentStaff'] = "部門職員";
	$Lang['DHL']['DepartmentPIC'] = "部門負責人";
	$Lang['DHL']['NoOfStaff'] = "職員數目";
	$Lang['DHL']['StaffName'] = "職員名稱";
	$Lang['DHL']['ViewStaffList'] = "查看員工列表";
	
	$Lang['DHL']['JoinDate'] = "入職日期";
	$Lang['DHL']['TerminatedDate'] = "離職日期";
	$Lang['DHL']['EffectiveDate'] = "生效日期";
	
	$Lang['DHL']['Staff'] = "職員";
	$Lang['DHL']['ChooseCompanies'] = "選擇公司";
	$Lang['DHL']['ChooseDivisions'] = "選擇分區";
	$Lang['DHL']['ChooseDepartments'] = "選擇部門";
	
	$Lang['DHL']['ImportStaffAccounts'] = "匯入職員戶口";
	$Lang['DHL']['ImportCreateNewUser'] = "新建用戶。";
	$Lang['DHL']['ImportUpdateExistingUser'] = "更新現有用戶。";
	$Lang['DHL']['ImportType'] = "類別";
	$Lang['DHL']['ImportTypeNewJoiner'] = "新職員";
	$Lang['DHL']['ImportTypeMovement'] = "調動";
	$Lang['DHL']['ImportTypeLeaver'] = "離職";
	$Lang['DHL']['NoNewRecordForImporting'] = "沒有新的匯入紀錄。";
	
	$Lang['DHL']['JS_warning']['PICandUserCannotSame'] = "部門負責人不能與部門職員重複。";
	
	$Lang['DHL']['ImportAccountColumnDefinitions'] = array();
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('UserLogin','用戶登錄ID。',1,''); // field name, description, is required field, has extra output
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Password','用戶密碼。現有用戶如不想改變密碼則可以留空。',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('UserEmail','個人電郵地址。用來接收系統通知。',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('EnglishName','用戶英文名稱。',1,'');
	//$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('ChineseName','用戶中文名稱。',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Company','公司代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetCompanyList\');">[檢視公司列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Division','分區代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDivisionList\');">[檢視分區列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Department','部門代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDepartmentList\');">[檢視部門列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('JoinDate','加入公司日期。(日期格式: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('EffectiveDate','帳戶生效日期。(日期格式: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('TerminatedDate','離職日期。可以留空。(日期格式: YYYY-MM-DD or DD/MM/YYYY)',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Status','戶口狀態。 1表示使用中，0表示暫停。',1,'');
	
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'] = array();
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('UserLogin','用戶登錄ID。',1,''); // field name, description, is required field, has extra output
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Password','用戶密碼。現有用戶如不想改變密碼則可以留空。',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('UserEmail','個人電郵地址。用來接收系統通知。',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('EnglishName','用戶英文名稱。',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Company','公司代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetCompanyList\');">[檢視公司列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Division','分區代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDivisionList\');">[檢視分區列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Department','部門代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDepartmentList\');">[檢視部門列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('JoinDate','加入公司日期。(日期格式: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('TerminatedDate','離職日期。可以留空。(日期格式: YYYY-MM-DD or DD/MM/YYYY)',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Status','戶口狀態。 1表示使用中，0表示暫停。',1,'');
	
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'] = array();
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('UserLogin','用戶登錄ID。',1,''); // field name, description, is required field, has extra output
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Password','用戶密碼。現有用戶如不想改變密碼則可以留空。',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('UserEmail','個人電郵地址。用來接收系統通知。',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('EnglishName','用戶英文名稱。',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Company','公司代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetCompanyList\');">[檢視公司列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Division','分區代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDivisionList\');">[檢視分區列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Department','部門代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDepartmentList\');">[檢視部門列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('EffectiveDate','帳戶生效日期。(日期格式: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Status','戶口狀態。 1表示使用中，0表示暫停。',1,'');
	
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'] = array();
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('UserLogin','用戶登錄ID。',1,''); // field name, description, is required field, has extra output
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Password','用戶密碼。現有用戶如不想改變密碼則可以留空。',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('UserEmail','個人電郵地址。用來接收系統通知。',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('EnglishName','用戶英文名稱。',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Company','公司代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetCompanyList\');">[檢視公司列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Division','分區代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDivisionList\');">[檢視分區列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Department','部門代碼。',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDepartmentList\');">[檢視部門列表]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('JoinDate','加入公司日期。(日期格式: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('TerminatedDate','離職日期。可以留空。(日期格式: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Status','戶口狀態。 1表示使用中，0表示暫停。',1,'');
	
	$Lang['DHL']['ImportErrors']['UserNotFound'] = '<!--FIELDVALUE-->: 找不到該用戶。';
	$Lang['DHL']['ImportErrors']['FieldUsed'] = '<!--FIELDVALUE-->: 其他用戶已在使用。';
	$Lang['DHL']['ImportErrors']['BlankValue'] = '<!--FIELDNAME-->: 必須輸入。';
	$Lang['DHL']['ImportErrors']['DuplicatedImportRecord'] = '<!--FIELDNAME-->: 重複匯入的紀錄。';
	$Lang['DHL']['ImportErrors']['NewUserMustInputPassword'] = '<!--FIELDNAME-->: 新用戶必須輸入密碼。';
	$Lang['DHL']['ImportErrors']['InvalidEmail'] = '<!--FIELDNAME-->: 電郵格式不正確。';
	$Lang['DHL']['ImportErrors']['EmailUsedByOthers'] = '<!--FIELDNAME-->: 電郵已經被使用了。';
	$Lang['DHL']['ImportErrors']['CodeNotFound'] = '<!--FIELDNAME-->: 找不到代碼。';
	$Lang['DHL']['ImportErrors']['InvalidDepartment'] = '<!--FIELDNAME-->: 部門不正確。';
	$Lang['DHL']['ImportErrors']['InvalidDateFormat'] = '<!--FIELDNAME-->: 日期格式不正確。';
	$Lang['DHL']['ImportErrors']['InvalidStatus'] = '<!--FIELDNAME-->: 狀態不正確。';
	
	$Lang["DHL"]["Login"]["Login"] = "登入";
	$Lang["DHL"]["Login"]["LoginTitle"] = "Employee Mobile App Platform";
	$Lang["DHL"]["Login"]["LoginID"] = "登入名稱";
	$Lang["DHL"]["Login"]["LoginID_alert"] = "請輸入登人名稱";
	$Lang["DHL"]["Login"]["LoginID_err"] = "登入名稱/密碼無效";
	$Lang["DHL"]["Login"]["Password"] = "密碼";
	$Lang["DHL"]["Login"]["Password_alert"] = "請輸入密碼";
	$Lang["DHL"]["Login"]["Password_err"]= "密碼錯誤";
	$Lang["DHL"]['Login']['ForgotPassword'] = "忘記密碼?";
	$Lang["DHL"]['Login']['RequestSent'] = "重設密碼程序已寄送至閣下的<!--DOMAIN-->電郵。";
	$Lang["DHL"]['Login']['FailToSendResetPasswordEmail'] = "未能發送重設密碼程序至閣下的電郵地址。";
	$Lang["DHL"]['Login']['YourUserAccountDoesNotHaveEmailAddress'] = "閣下的帳戶尚未設定電郵地址。";
	
	$Lang["DHL"]["Login"]["ForgotPasswordMsg"] = '1. 請輸入登入名稱，重啟密碼程序將寄送至閣下備用電郵。\n2. 如無法取得密碼，請聯絡所屬機構查詢。';
	$Lang['DHL']['Warnings']['DuplicatedName'] = "重複名稱。";
	$Lang['DHL']['Warnings']['DuplicatedCode'] = "重複代碼。";
	
	###### Email Notification
	$Lang['EmailNotification']['eNotice']['Subject'] = "[DHL] 新電子通告 - ";
	$Lang['EmailNotification']['eNotice']['Content'] = "一則新的電子通告 (__TITLE__) __TERM__於__STARTDATE__發出。<br>\r\n 請於__ENDDATE__或之前簽妥。<br>\r\n 你可登入__WEBSITE__，然後前往「資訊服務 > 電子通告」閱讀通告。<br>\r\n";
	$Lang['EmailNotification']['eNotice']['Term1'] = "將會";
	$Lang['EmailNotification']['eNotice']['Term2'] = "已經";
	$Lang['EmailNotification']['SchoolNews']['School'] = "學校";
	$Lang['EmailNotification']['SchoolNews']['Group'] = "小組";
	$Lang['EmailNotification']['SchoolNews']['Subject'] = "新__TYPE__宣佈 (__DATE__)";
	$Lang['EmailNotification']['SchoolNews']['Content']['SCHOOL'] = "一個新宣佈(\"__TITLE__\")已經於__DATE__發出。請登入__WEBSITE__閱讀。<br>\r\n";
	$Lang['EmailNotification']['SchoolNews']['Content']['GROUP'] = "一個關於__LIST__的新小組宣佈(\"__TITLE__\")已經於__DATE__發出。請登入__WEBSITE__閱讀。<br>\r\n";
	$Lang['EmailNotification']['eCircular']['Subject'] = "[".$Lang['Header']['HomeTitle']."] 新職員通告 - __TITLE__";
	$Lang['EmailNotification']['eCircular']['Content'] =  "一則新的職員通告 (__TITLE__) __TERM__於__STARTDATE__發出。<br>\r\n 請於__ENDDATE__或之前簽妥。<br>\r\n 你可登入__WEBSITE__，然後前往「行政管理工具 > 職員管理 > 職員通告」閱讀通告。<br>\r\n";
	$Lang['EmailNotification']['ForgotPassword']['Subject'] = $Lang['Header']['HomeTitle'].": 申請密碼";
	$Lang['EmailNotification']['PasswordReset']['Subject'] = $Lang['Header']['HomeTitle'].": 因保安理由, 你的密碼已經重設";
	$Lang['EmailNotification']['ForgotPassword']['Content'] = "__NAME__,<br>\r\n<br>\r\n你的登入戶口密碼已經重設，如下:<br>\r\n<br>\r\n登入名稱: __LOGIN__<br>\r\n密碼: __PASSWORD__<br>\r\n<br>\r\n祝 使用愉快!<br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;此致<br>\r\nDHL<br>\r\n";
	$Lang['EmailNotification']['ForgotPassword_ACK']['Subject'] = $Lang['Header']['HomeTitle'].": 忘記密碼";
	$Lang['EmailNotification']['ForgotPassword_ACK']['Content'] = "一名用戶忘記了密碼，並已經重新提取。<br>\r\n登入名稱: __LOGIN__<br>\r\n電郵: __EMAIL__<br>\r\n";
	$Lang['EmailNotification']['Registration']['Subject'] = $Lang['Header']['HomeTitle'].": 用戶登記";
	$Lang['EmailNotification']['Registration']['Content'] = "__ENGNAME__ __CHINAME__,<br><br>\r\n\r\n歡迎使用".$Lang['Header']['HomeTitle']."。你可以前往此網頁登入:__WEBSITE__<br><br>\r\n\r\n你的登入戶口如下:<br><br>\r\n\r\n電郵: __EMAIL__<br>\r\n登入名稱: __LOGIN__<br>\r\n密碼: __PASSWORD__<br><br>\r\n";
	$Lang['EmailNotification']['UpdatePassword']['Subject'] = $Lang['Header']['HomeTitle'].": 你的登入密碼已經重設";
	$Lang['EmailNotification']['UpdatePassword']['Content'] = "__ENGNAME__ __CHINAME__,<br>\r\n<br>\r\n你的".$Lang['Header']['HomeTitle']."登入密碼已經重設。如果你仍未能成功登入，請聯絡系統管理員。<br>\r\n<br>\r\n";
	$Lang['EmailNotification']['SendPassword']['Subject'] = $Lang['Header']['HomeTitle']."登入戶口";
	$Lang['EmailNotification']['SendPassword']['Content'] = "__ENGNAME__ __CHINAME__,<br>\r\n<br>\r\n歡迎使用".$Lang['Header']['HomeTitle']."。你可以前往此網頁登入:__WEBSITE__<br><br>\r\n\r\n你的登入戶口如下:<br>\r\n<br>\r\n電郵: __EMAIL__<br>\r\n登入名稱: __LOGIN__<br>\r\n密碼: __PASSWORD__<br>\r\n<br>\r\n";
	$Lang['EmailNotification']['ForgotHashedPassword']['Subject'] = $Lang['Header']['HomeTitle'].": 申請重設密碼";
	//$Lang['EmailNotification']['ForgotHashedPassword']['Content'] = "__NAME__,<br>\r\n<br>\r\n歡迎使用".$Lang['Header']['HomeTitle']."。<br><br>\r\n\r\n你的登入戶口如下:<br>\r\n<br>\r\n登入名稱: __LOGIN__<br>\r\n電郵: __EMAIL__<br>\r\n你可以前往此連結重設密碼:<a href='__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__'>__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__</a><br>\r\n<br>\r\n";
	$Lang['EmailNotification']['ForgotHashedPassword']['Content'] = "__NAME__，<br>\r\n<br>\r\n按閣下更改帳戶密碼之要求，請核對以下的帳號資料，然後按連結更改新密碼。<br>\r\n<br>\r\n帳號資料:<br>\r\n電郵: __EMAIL__<br>\r\n登入帳戶: __LOGIN__<br>\r\n<br>\r\n更改新密碼連結<br>\r\n<a href='__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__'>__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__</a><br>\r\n";
	$Lang['EmailNotification']['ForgotHashedPassword_ACK']['Subject'] = $Lang['Header']['HomeTitle'].": 重設密碼";
	$Lang['EmailNotification']['ForgotHashedPassword_ACK']['Content'] = "一名用戶忘記了密碼，並已經重新設定。<br>\r\n登入名稱: __LOGIN__<br>\r\n電郵: __EMAIL__<br>\r\n";
	//$Lang['EmailNotification']['Footer'] = "<br><br>\r\n\r\n------------------------------------------------------<br>\r\n此電郵由".$Lang['Header']['HomeTitle']."自動發送。<br>\r\n如有任何查詢，請電郵到 __WEBMASTER__。<br>\r\n要獲得最新資訊，請前往 __WEBSITE__。<br>\r\n";
	$Lang['EmailNotification']['Footer'] = "<br>\r\n<br>\r\n------------------------------------------------------<br>\r\n此電郵由DHL Hong Kong DHL360@HK自動發送。<br>\r\n如有任何查詢，請電郵到 __WEBMASTER__。<br>\r\n要獲得最新資訊，請前往 __WEBSITE__。<br>\r\n";
	$Lang['EmailNotification']['DoNotReply'] = "此郵件由系統發出，請勿回覆。";
	
	/***********************************************/
	$Lang['StudentAttendance']['ClassTeacherTakeOwnClassOnlySetting'] = "在我的智能卡紀錄中職員只可為有關的班別點名";
	$Lang['StudentAttendance']['ExportForOfflineReaderUse'] = "匯出離線拍咭機使用的學生及職員資料";
	$Lang['StudentAttendance']['ExportForOfflineReaderUse_950e'] = "匯出離線拍咭機使用的學生及職員資料 (950e 格式)";
	$Lang['Header']['Menu']['TeacherPortfolio'] = "職員檔案系統";
	$Lang['Header']['Menu']['TeacherAppsNotify'] = "職員通知 (Staff App)";
	$Lang['Identity']['Staff'] = "職員";
	$Lang['SysMgr']['RoleManagement']['eAttendanceAdmin'] = "職員考勤系統管理";
	$Lang['SysMgr']['RoleManagement']['schoolNews'] = "最新消息管理";
	$Lang['SysMgr']['RoleManagement']['TeacherAppNotify'] = "職員通知 (Staff App) 管理";
	$Lang['eCircular']['NotifyStaffByEmail'] = "以電郵通知職員";
	$Lang['eCircular']['NotifyStaffByPushMessage'] = "以Staff App推送訊息通知職員";
	
	$Lang['ePayment']['Staff'] = "職員";
	$Lang['LessonAttendance']['iSmartCardTeacherCanTakeAllLessons'] = "智能卡紀錄 - 職員可以為所有課堂點名";
	$Lang['eNotice']['AllHaveRight'] = "所有職員可發通告";
	$Lang['eNotice']['AllStaffAllow'] = "所有職員可檢視回條內容";
	$Lang['eNotice']['TeacherCanSeeAllNotice'] = '所有職員可檢視所有通告內容';
	$Lang['SmartCard']['StaffAttendence']['StaffMonthlyAttendanceReport'] = "職員每月出席資料表";
	$Lang['StaffAttendance']['AllStaff'] = "所有職員";
	$Lang['StaffAttendance']['StaffWithOutstandingMins'] = "未扣除超時時間的職員";
	$Lang['StaffAttendance']['StaffAbsenceReport'] = "職員缺席統計表";
	$Lang['StaffAttendance']['StaffLeaveAbsenceReport'] = "職員請假缺席統計";
	$Lang['StaffAttendance']['MonthlyReportGroupByStaff'] = "按職員分組";
	$Lang['StaffAttendance']['DailyAttendanceRecordEmailSubject'] = "職員考勤管理系統 每日請假缺席紀錄";
	$Lang['StaffAttendance']['StaffNo'] = "職員編號";
	$Lang['StaffAttendance']['ReportDisplaySuspendedStaff'] = "報表顯示暫停使用的職員";
	$Lang['StaffAttendance']['ReportDisplayLeftStaff'] = "報表顯示已離校的職員";
	$Lang['StaffAttendance']['ViewStaffList'] = "檢視職員列表";
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'] = array();
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Group ID", "職務小組ID。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("User Logins", "職員用戶登入帳號(以逗號分隔)。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Date", "日期 (格式為年月日 yyyy-mm-dd)。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Duty Type", "<b>D</b>表示需要當值，<b>L</b>表示放假，<b>FL</b>表示全日放假，<b>O</b>表示外出工作，<b>FO</b>表示全日外出工作，<b>N</b>表示沒有職務。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Time Slot", "職務時段名稱。全日設定應該留空。不能包含符號: % & ^ @ \" \\ ' : ; < > , . / ? [ ] ~ ! # $ * ( ) { } | = + - ");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Duty Start", "職務開始時間，格式為hh:mm:ss。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Duty End", "職務結束時間，格式為hh:mm:ss。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Duty Count", "職務比重，例如: 0.5 表示半日，1.0 表示全日。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Waive In", "<b>Y</b>表示進入的時候不需要拍卡，<b>N</b>表示進入的時候需要拍卡。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Waive Out", "<b>Y</b>表示離開的時候不需要拍卡，<b>N</b>表示離開的時候需要拍卡。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Reason ID", "預設的原因ID。");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$Lang['StaffAttendance']['ImportSpecialDutyGroupFormat'][] = array("Attend One Time Slot", "職員只需要出席同一日的其中一個職務時段。1表示是，0表示否。");
	}
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'] = array();
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("User Login", "職員用戶登入帳號");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Date", "日期 (格式為年月日 yyyy-mm-dd)。");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Duty Type", "<b>D</b>表示需要當值，<b>L</b>表示放假，<b>FL</b>表示全日放假，<b>O</b>表示外出工作，<b>FO</b>表示全日外出工作，<b>N</b>表示沒有職務。");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Time Slot", "職務時段名稱。全日設定請留空。不能包含符號: % & ^ @ \" \\ ' : ; < > , . / ? [ ] ~ ! # $ * ( ) { } | = + - ");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Duty Start", "職務開始時間，格式為hh:mm:ss。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Duty End", "職務結束時間，格式為hh:mm:ss。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Duty Count", "職務比重，例如: 0.5 表示半日，1.0 表示全日。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Waive In", "<b>Y</b>表示進入的時候不需要拍卡，<b>N</b>表示進入的時候需要拍卡。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Waive Out", "<b>Y</b>表示離開的時候不需要拍卡，<b>N</b>表示離開的時候需要拍卡。全日設定請留空。");
	$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Reason ID", "預設的原因ID。");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$Lang['StaffAttendance']['ImportSpecialDutyIndividualFormat'][] = array("Attend One Time Slot", "職員只需要出席同一日的其中一個職務時段。1表示是，0表示否。");
	}
	$Lang['StaffAttendance']['ImportSpecialDutyWarning']['UserloginNotBelongToGroup'] = "職員 %1 不是ID編號為 %2 的小組成員。";
	$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Staff'] = "職員";
	$Lang['AppNotifyMessage']['SchoolNewsForApp'] = "最新消息(用於手機應用程式)";
	$Lang['AppNotifyMessage']['Public(Teacher)'] = "全部職員";
	$Lang['AppNotifyMessage']['ToPublic(Teacher)'] = "傳送給全部職員";
	$Lang['AppNotifyMessage']['ToNonPublic(Teacher)'] = "指定職員";
	$Lang['AppNotifyMessage']['NonPublic(Teacher)'] = "指定職員";
	$Lang['AppNotifyMessage']['UsingTeacherAppTeacherOnly'] = "只限現正使用  Staff App 的職員";
	$Lang['AppNotifyMessage']['Error']['no_teacher'] = "找不到該職員或該職員未有使用  Staff App";
	$Lang['AppNotifyMessage']['valid_table_title_teacher'] = "將會傳送訊息至以下職員:";
	$Lang['AppNotifyMessage']['msgSearchAndInsertPICInfo'] ='輸入一行一個搜尋記錄 "[內聯網帳號]"<br>e.g. 搜尋 tchr01 和 tchr02 二位負責人<br>請輸入<br>tchr01<br>tchr02<br><br>[只可選取職員]';
	$Lang['AppNotifyMessage']['eNotice']['send_failed_staff'] = "職員提醒發出失敗！";
	$Lang['AppNotifyMessage']['eNotice']['send_result_no_staff'] = "請先建立相關職員帳戶。";
	$Lang['AppNotifyMessage']['SchoolNews']['Title']['SCHOOL'] = "最新消息通知
Latest School News Reminder";
	$Lang['MessageCenter']['ToTeacher'] = "傳送至職員";
	$Lang['MessageCenter']['ExportMobileRemarks_T'] = '#匯出沒有使用  App 的職員電話號碼';
	$Lang['AccountMgmt']['Menu_ThreeAccMgmt'] = " (家長用戶, 職員用戶, 學生用戶)";
	$Lang['AccountMgmt']['Menu_StaffMgmt'] = " (職員用戶)";
	$Lang['AccountMgmt']['InputStaffDetails'] = "輸入職員資料";
	
	$Lang['AccountMgmt']['StaffImportFields'] = array("內聯網帳號", "密碼", "電子郵件", "英文姓名", "中文姓名", "別名", "性別", "手提電話", "傳真號碼", $Lang['AccountMgmt']['Barcode'], "備註", "顯示稱謂(中文)", "顯示稱謂(英文)", "WebSAMS職員代碼");
	$Lang['AccountMgmt']['StaffCode'] = "WebSAMS 職員代碼";
	
	if($special_feature['signature_upload'])
	{
		$Lang['AccountMgmt']['SignatureUpload'] = array();
		$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "建議使用 120px x 55px (闊 x 高)的相片";
		$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "只接受JPEG檔的相片";
		$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "可以以壓縮檔案(.zip)形式上載多個檔案";
		$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "壓縮檔案內不能存在文件夾";
		$Lang['AccountMgmt']['SignatureUpload']['RemarksList'][] = "相片的檔案名稱要與職員的登入編號一樣";
		$Lang['AccountMgmt']['UploadSignature'] = "上載簽名";
		$Lang['AccountMgmt']['SignatureUpload']['WrongStaffLogin'] = "不是職員登入編號";
	}
	$Lang['AccountMgmt']['ImportStaffForwardingEmails'] = "匯入職員用戶的轉寄電郵地址";
	$Lang['WhatIsAdminUser']['Details'] = "「管理用戶」為內聯網前端的管理者。首次使用系統時，由於還沒有建立任何帳戶，所以你不能 夠指派「管理用戶」。<br><br>作為系統管理員，你可以「臨時管理用戶」身份，登入內聯網前端，以建立首批「管理用戶」的帳戶。其後，「管理用 戶」將可以接管系統前端的設定工作。<br><br>請輸入自定的登入帳號及密碼，然後按「儲存」。<br><br>然後，你便可以登入內聯網前端，前往 <b>學校行政管理工具 > 用戶管理 > 職員用戶</b>，建立「管理用戶」的帳戶。完成後，請返回本頁，並使用上表，指派「管理用戶」。";
	
	$ip20_what_is_new = "最新消息";
	$i_identity_teachstaff = "職員";
	$i_identity_array = array("",$i_identity_teachstaff,$i_identity_student,$i_identity_parent,$i_identity_alumni);
	$i_import_teacher_data = "匯入$i_identity_teachstaff"."資料";
	$i_import_identity_array = array("",$i_import_teacher_data,$i_import_student_data,$i_import_parent_data,$i_import_alumni_data);
	$i_adminmenu_im_announcement = "最新消息";
	$i_adminmenu_basic_settings_title = "不顯示'老師'字眼或稱謂 (職員 及 家長)";
	$i_admintitle_im_announcement = "最新消息";
	$i_eNews = "最新消息";
	$i_eNews_Settings = "最新消息設定";
	$i_eNews_Approval2 = "最新消息批核";
	$i_StaffAttendance_ReasonType_Message = "以下原因類型將作為「職員請假缺席統計」報告之欄位名稱，若<br><br>
																				 1.有關原因類型已停用，及<br>
																				 2.有關原因類型於缺席統計之統計日期範圍期間未曾選用<br><br>
			
																				 則該原因類型將不予顯示於該統計報告中。
																				 ";
	$i_Assessment_UserSelectAdmin = "如不選擇職員, 評估者將會顯示為 <B>系統管理員</B>.";
	$i_Survey_perIdentity = "檢視單一身份結果(職員,學生,家長)";
	$i_Notice_Setting_AllHaveRight = "所有職員可發通告.";
	$i_StudentAttendance_StaffAndStudentInformation = "職員及學生資料";
	$i_StaffAttendance_Report_Day_Range_Abs = "職員缺席統計表";
	$i_StaffAttendance_Report_Day_Range_Abs_By_Name = "職員請假缺席統計";
	
	$iDiscipline['fromStaffList'] = "自 <b>職員名單</b>";
	$eEnrollment['system_user_list'] = "能使用系統之職員名單";
	$eEnrollment['system_user_name'] = "職員姓名";
	$i_InventorySystem_ImportCaretaker_Warning = "系統只會匯入所選組別內的職員";
	$i_import_teacher_data = "匯入職員資料";

$Lang['AppNotifyMessage']['eCircularSigned'] = "簽署職員通告確認通知  [[sign_notice_number]]
eNotice Signed Alert [[sign_notice_number]]";
$Lang['AppNotifyMessage']['eCircularSignedContent_eClassApp'] = "已收到閣下於 [sign_datetime] 簽署的職員通告[sign_notice_number]「[sign_notice_title]」。
Please note that the eNotice [sign_notice_number] titled \"[sign_notice_title]\" signed at [sign_datetime] has been received.";
/*$Lang['AppNotifyMessage']['eCircular']['Subject'] = "最新職員通告提示 [__NOTICENUMBER__]
Latest eNotice alert [__NOTICENUMBER__]"; */
$Lang['AppNotifyMessage']['eCircular']['Subject'] = "__TITLE__";
$Lang['AppNotifyMessage']['eCircular']['Title']['App'] = "職員通告重要提示 [[NoticeNumber]]
eNotice Important Reminder [[NoticeNumber]]";
$Lang['AppNotifyMessage']['eCircular']['Alert']['App'] = "請儘快簽署職員通告[NoticeNumber]「[NoticeTitle]」(簽署限期: [NoticeDeadline])。
Please sign the eNotice [NoticeNumber] titled \"[NoticeTitle]\" as soon as possible (Original Deadline: [NoticeDeadline]).";
$Lang['AppNotifyMessage']['eCircular']['Content'] = "請於__ENDDATE__或之前簽署職員通告__NOTICENUMBER__「__TITLE__」。
Please sign the eNotice __NOTICENUMBER__ titled \"__TITLE__\" on or before __ENDDATE__.";

$Lang['AppNotifyMessage']['SchoolNews']['Subject'] = "新__TYPE__宣佈 (__DATE__)";
$Lang['AppNotifyMessage']['SchoolNews']['Title']['SCHOOL'] = "__TITLE__";
$Lang['AppNotifyMessage']['SchoolNews']['Title']['GROUP'] = "__TITLE__";
$Lang['AppNotifyMessage']['SchoolNews']['Content']['SCHOOL'] = "請留意於__DATE__發放的「__TITLE__」最新消息。
Please note that the latest news of \"__TITLE__\" was released on __DATE__.";
$Lang['AppNotifyMessage']['SchoolNews']['Content']['GROUP'] = "請留意於__DATE__發放的「__TITLE__」小組最新消息。
Please note that the latest group news of \"__TITLE__\" was released on __DATE__.";

	/*
	if ($_SERVER["REMOTE_ADDR"] == "10.0.3.133") {
		if (!function_exists("DHLTextReplace")) {
			function DHLTextReplace(&$item, $key)
			{
				$replaceArr = array(
						"teacher" => "<span style='color:#f00'>staff</span>",
						"教職員" => "<span style='color:#f00'>職員</span>",
						"校園最新消息" => "<span style='color:#f00'>最新消息</span>",
						"學校資訊 " => "<span style='color:#f00'>公司資訊</span>"
				);
				foreach ($replaceArr as $kk => $vv) {
					if (strpos($item, $kk) !== false) {
						echo $kk . " - " . $key . "<hr>";
						echo $item . "<br>";
						echo str_replace(array_keys($replaceArr), array_values($replaceArr), $item) . "<hr>";
					}
				}
			}
		}
		$allVariable = get_defined_vars();
		
		// $gpc = array(&$allVariable);
		// $gpc = array(&$Lang);
		array_walk_recursive($gpc, 'DHLTextReplace');
		exit;
	}
	*/
}
?>