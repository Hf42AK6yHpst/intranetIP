<?php
if ($sys_custom['DHL']) {
	// Editing by 
	$Lang['Header']['HomeTitle'] = "DHL Hong Kong - Employee Mobile App Platform";
	
	$Lang["PageTitle"] = "DHL Hong Kong - Employee Mobile App Platform";
	$Lang['Header']['Menu']['eAdmin'] = "eAdmin";
	$Lang['Header']['Menu']['MessageCenter'] = "Push Notification";
	$Lang['Header']['Menu']['schoolNews'] = "Corporate News";
	$Lang['SysMgr']['SchoolNews']['SchoolNews'] = "Corporate News";
	$Lang['Header']['Menu']['Settings'] = "Settings";
	$Lang['Header']['Menu']['eClassApp'] = "App";
	$Lang['Header']['Menu']['Organization'] = "Organization Settings";
	$Lang['Header']['Menu']['StaffAccount'] = "Account Management";
	$Lang['Header']['Menu']['eCircular'] = "eNotice";
	$Lang['Header']['Menu']['TeacherAppsNotify'] = "Push Notification (Apps)";
	
	$Lang['AccountMgmt']['Menu_ThreeAccMgmt'] = "";
	$Lang['SysMgr']['RoleManagement']['eCircularAdmin'] = "eNotice Admin";
	$Lang['SysMgr']['RoleManagement']['eClassTeacherAppAdmin'] = "App Admin";
	$Lang['SysMgr']['RoleManagement']['StaffAttendance'] = "eAttendance Admin";
	$Lang['SysMgr']['RoleManagement']['TeacherAppNotify'] = "Push Notification Admin";
	$Lang['SysMgr']['RoleManagement']['OrganizationAdmin'] = "Organization Admin";
	$Lang['SysMgr']['RoleManagement']['TeachingStaff'] = "Staff";
	$Lang['SysMgr']['RoleManagement']['eAdmin'] = "eAdmin";
	$Lang['SysMgr']['RoleManagement']['SchoolSettings'] = "Settings";
	$Lang['SysMgr']['RoleManagement']['schoolNews'] = "Corporate News Admin";
	
	$Lang['eClassApp']['ModuleNameAry']['eCircular'] = 'eNotice';
	$Lang['eClassApp']['ModuleNameAry']['eNotice'] = 'Student Notice';
	$Lang['eClassApp']['ModuleNameAry']['SchoolNews'] = 'Corporate News';
	$Lang['eClassApp']['ModuleNameAry']['SchoolInfo'] = 'Corporate Info';
	$Lang['eClassApp']['ModuleNameAry']['GroupMessage'] = 'Group Message';
	$Lang['eClassApp']['TeacherApp'] = 'App';
	$Lang['eClassApp']['AllStaffs'] = 'All Staff';
	
	$Lang['DHL'] = array();
	$Lang['DHL']['Organization'] = "Organization";
	$Lang['DHL']['Company'] = "Company";
	$Lang['DHL']['Division'] = "Division";
	$Lang['DHL']['Department'] = "Department";
	$Lang['DHL']['Chinese'] = "中文";
	$Lang['DHL']['English'] = "English";
	$Lang['DHL']['Code'] = "Code";
	$Lang['DHL']['Description'] = "Description";
	$Lang['DHL']['CompanyName'] = "Company Name";
	$Lang['DHL']['CompanyPIC'] = "Company PIC";
	$Lang['DHL']['DivisionName'] = "Division Name";
	$Lang['DHL']['DivisionPIC'] = "Division PIC";
	$Lang['DHL']['DepartmentName'] = "Department Name";
	$Lang['DHL']['RelatedCompany'] = "Related Company";
	$Lang['DHL']['RelatedDivision'] = "Related Division";
	$Lang['DHL']['DepartmentStaff'] = "Department Staff";
	$Lang['DHL']['DepartmentPIC'] = "Department PIC";
	$Lang['DHL']['NoOfStaff'] = "No. of Staff";
	$Lang['DHL']['StaffName'] = "Staff Name";
	$Lang['DHL']['ViewStaffList'] = "View Staff List";
	
	$Lang['DHL']['JoinDate'] = "Join Date";
	$Lang['DHL']['TerminatedDate'] = "Terminated Date";
	$Lang['DHL']['EffectiveDate'] = "Effective Date";
	
	$Lang['DHL']['Staff'] = "Staff";
	$Lang['DHL']['ChooseCompanies'] = "Select companies";
	$Lang['DHL']['ChooseDivisions'] = "Select divisions";
	$Lang['DHL']['ChooseDepartments'] = "Select departments";
	
	$Lang['DHL']['ImportStaffAccounts'] = "Import Staff Accounts";
	$Lang['DHL']['ImportCreateNewUser'] = "Create new user.";
	$Lang['DHL']['ImportUpdateExistingUser'] = "Update existing user.";
	$Lang['DHL']['ImportType'] = "Type";
	$Lang['DHL']['ImportTypeNewJoiner'] = "New joiner";
	$Lang['DHL']['ImportTypeMovement'] = "Movement";
	$Lang['DHL']['ImportTypeLeaver'] = "Leaver";
	$Lang['DHL']['NoNewRecordForImporting'] = "No new record for importing.";
	
	$Lang['DHL']['JS_warning']['PICandUserCannotSame'] = "Department PIC cannot duplicate with department staff.";
	
	
	$Lang['DHL']['ImportAccountColumnDefinitions'] = array();
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('UserLogin','Login ID.',1,''); // field name, description, is required field, has extra output
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Password','User password. For existing user can leave blank if do not want to change.',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('UserEmail','Personal email address. For receiving notification emails.',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('EnglishName','User English name.',1,'');
	//$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('ChineseName','User Chinese name.',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Company','Company code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetCompanyList\');">[View company list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Division','Division code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDivisionList\');">[View division list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Department','Department code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDepartmentList\');">[View department list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('JoinDate','Date joined the company. (Format: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('EffectiveDate','Effective date for user account. (Format: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('TerminatedDate','Terminated date. Can leave blank. (Format: YYYY-MM-DD or DD/MM/YYYY)',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions'][] = array('Status','Account status. 1 for activated, 0 for suspended.',1,'');
	
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'] = array();
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('UserLogin','Login ID.',1,''); // field name, description, is required field, has extra output
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Password','User password. For existing user can leave blank if do not want to change.',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('UserEmail','Personal email address. For receiving notification emails.',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('EnglishName','User English name.',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Company','Company code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetCompanyList\');">[View company list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Division','Division code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDivisionList\');">[View division list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Department','Department code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDepartmentList\');">[View department list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('JoinDate','Date joined the company. (Format: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('TerminatedDate','Terminated date. Can leave blank. (Format: YYYY-MM-DD or DD/MM/YYYY)',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_NewJoiner'][] = array('Status','Account status. 1 for activated, 0 for suspended.',1,'');
	
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'] = array();
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('UserLogin','Login ID.',1,''); // field name, description, is required field, has extra output
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Password','User password. For existing user can leave blank if do not want to change.',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('UserEmail','Personal email address. For receiving notification emails.',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('EnglishName','User English name.',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Company','Company code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetCompanyList\');">[View company list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Division','Division code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDivisionList\');">[View division list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Department','Department code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDepartmentList\');">[View department list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('EffectiveDate','Effective date for user account. (Format: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Movement'][] = array('Status','Account status. 1 for activated, 0 for suspended.',1,'');
	
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'] = array();
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('UserLogin','Login ID.',1,''); // field name, description, is required field, has extra output
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Password','User password. For existing user can leave blank if do not want to change.',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('UserEmail','Personal email address. For receiving notification emails.',0,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('EnglishName','User English name.',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Company','Company code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetCompanyList\');">[View company list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Division','Division code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDivisionList\');">[View division list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Department','Department code.',1,'<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\'GetDepartmentList\');">[View department list]</a>');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('JoinDate','Date joined the company. (Format: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('TerminatedDate','Terminated date. Can leave blank. (Format: YYYY-MM-DD or DD/MM/YYYY)',1,'');
	$Lang['DHL']['ImportAccountColumnDefinitions_Leaver'][] = array('Status','Account status. 1 for activated, 0 for suspended.',1,'');
	
	$Lang['DHL']['ImportErrors']['UserNotFound'] = '<!--FIELDVALUE-->: user cannot be found.';
	$Lang['DHL']['ImportErrors']['FieldUsed'] = '<!--FIELDVALUE-->: is being used by another user.';
	$Lang['DHL']['ImportErrors']['BlankValue'] = '<!--FIELDNAME-->: must not be blank.';
	$Lang['DHL']['ImportErrors']['DuplicatedImportRecord'] = '<!--FIELDNAME-->: duplicated import record.';
	$Lang['DHL']['ImportErrors']['NewUserMustInputPassword'] = '<!--FIELDNAME-->: must input password for new user.';
	$Lang['DHL']['ImportErrors']['InvalidEmail'] = '<!--FIELDNAME-->: invalid email.';
	$Lang['DHL']['ImportErrors']['EmailUsedByOthers'] = '<!--FIELDNAME-->: email is being used by another user.';
	$Lang['DHL']['ImportErrors']['CodeNotFound'] = '<!--FIELDNAME-->: code is not found.';
	$Lang['DHL']['ImportErrors']['InvalidDepartment'] = '<!--FIELDNAME-->: invalid department.';
	$Lang['DHL']['ImportErrors']['InvalidDateFormat'] = '<!--FIELDNAME-->: invalid date format.';
	$Lang['DHL']['ImportErrors']['InvalidStatus'] = '<!--FIELDNAME-->: invalid status.';
	
	$Lang["DHL"]["Login"]["Login"] = "Login";
	$Lang["DHL"]["Login"]["LoginTitle"] = "Employee Mobile App Platform";
	$Lang["DHL"]["Login"]["LoginID"] = "Login ID";
	$Lang["DHL"]["Login"]["LoginID_alert"] = "Please enter the user login.";
	$Lang["DHL"]["Login"]["LoginID_err"] = "Invalid LoginID/Password.";
	$Lang["DHL"]["Login"]["Password"] = "Password";
	$Lang["DHL"]["Login"]["Password_alert"] = "Please enter the password.";
	$Lang["DHL"]["Login"]["Password_err"]= "Invaid Password";
	$Lang["DHL"]['Login']['ForgotPassword'] = "Forgot password?";
	$Lang["DHL"]['Login']['RequestSent'] = "Reset password email has been sent to your <!--DOMAIN--> account.";
	$Lang["DHL"]['Login']['FailToSendResetPasswordEmail'] = "Failed to send reset password email to your email address.";
	$Lang["DHL"]['Login']['YourUserAccountDoesNotHaveEmailAddress'] = "Your user account does not have email address setup yet.";
	
	$Lang["DHL"]["Login"]["ForgotPasswordMsg"] = '1. Input UserLogin, then reset pwd mail will be sent to your preset address.\n2. If no mail received, kindly contact your organization.';
	$Lang['DHL']['Warnings']['DuplicatedName'] = "Duplicated name.";
	$Lang['DHL']['Warnings']['DuplicatedCode'] = "Duplicated code.";
	
	###### Email Notification
	$Lang['EmailNotification']['eNotice']['Subject'] = "[DHL] New electronic Notice - ";
	$Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
	$Lang['EmailNotification']['eNotice']['Term1'] = "will be";
	$Lang['EmailNotification']['eNotice']['Term2'] = "has been";
	$Lang['EmailNotification']['SchoolNews']['School'] = "School";
	$Lang['EmailNotification']['SchoolNews']['Group'] = "Group";
	$Lang['EmailNotification']['SchoolNews']['Subject'] = "New __TYPE__ Announcement (__DATE__)";
	$Lang['EmailNotification']['SchoolNews']['Content']['SCHOOL'] = "A new School Announcement (\"__TITLE__\") has been issued on __DATE__. Please login here __WEBSITE__ to view the announcement.<br>\r\n";
	$Lang['EmailNotification']['SchoolNews']['Content']['GROUP'] = "A new Group Announcement (\"__TITLE__\") has been issued for __LIST__ on __DATE__. Please login here __WEBSITE__ to view the announcement.<br>\r\n";
	$Lang['EmailNotification']['eCircular']['Subject'] = "[DHL] New electronic notice - __TITLE__";
	$Lang['EmailNotification']['eCircular']['Content'] =  "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eAdmin > Staff Management > eNotice to view the notice.<br>\r\n";
	$Lang['EmailNotification']['ForgotPassword']['Subject'] = "eClass Intranet: Request for password";
	$Lang['EmailNotification']['PasswordReset']['Subject'] = "eClass Intranet: password reset (for security reason)";
	$Lang['EmailNotification']['ForgotPassword']['Content'] = "Dear __NAME__,<br>\r\n<br>\r\nPlease be informed that your ".$Lang['Header']['HomeTitle']." password has been reset as follows:<br>\r\n\r\n<br>\r\nLogin ID: __LOGIN__<br>\r\nPassword: __PASSWORD__<br>\r\n<br>\r\nHave a nice day!<br>\r\nSincerely,<br>\r\nDHL<br>\r\n";
	$Lang['EmailNotification']['ForgotPassword_ACK']['Subject'] = $Lang['Header']['HomeTitle'].": Forget Password Acknowledgement";
	$Lang['EmailNotification']['ForgotPassword_ACK']['Content'] = "A user has used forget password to retrieve his/her password<br>\r\nLogin: __LOGIN__<br>\r\nEmail: __EMAIL__<br>\r\n";
	$Lang['EmailNotification']['Registration']['Subject'] = $Lang['Header']['HomeTitle'].": New registration";
	$Lang['EmailNotification']['Registration']['Content'] = "Dear __ENGNAME__ __CHINAME__,<br>\r\n<br>\r\nWelcome to join ".$Lang['Header']['HomeTitle'].", you may login at __WEBSITE__<br><br>\r\n\r\nYour account information is:<br>\r\n<br>\r\nEmail: __EMAIL__<br>\r\nLogin ID: __LOGIN__<br>\r\nPassword: __PASSWORD__<br>\r\n<br>\r\nHave a nice day!<br>\r\n";
	$Lang['EmailNotification']['UpdatePassword']['Subject'] = $Lang['Header']['HomeTitle'].": Your Password Has Been Reset";
	$Lang['EmailNotification']['UpdatePassword']['Content'] = "Dear __ENGNAME__ __CHINAME__,<br>\r\n<br>\r\nYour password in ".$Lang['Header']['HomeTitle']." has been reset. Please contact the administrator if you do not know the new password.<br>\r\n<br>\r\nHave a nice day!<br>\r\n";
	$Lang['EmailNotification']['SendPassword']['Subject'] = $Lang['Header']['HomeTitle']." account information";
	$Lang['EmailNotification']['SendPassword']['Content'] = "Dear __ENGNAME__ __CHINAME__,<br>\r\n<br>\r\nWelcome to join ".$Lang['Header']['HomeTitle'].", you may login at __WEBSITE__<br>\r\n<br>\r\nYour account information is:<br>\r\n<br>\r\nEmail: __EMAIL__<br>\r\nLogin ID: __LOGIN__<br>\r\nPassword: __PASSWORD__<br>\r\n<br>\r\nHave a nice day!<br>\r\n";
	$Lang['EmailNotification']['ForgotHashedPassword']['Subject'] = $Lang['Header']['HomeTitle'].": Request for resetting password";
	//$Lang['EmailNotification']['ForgotHashedPassword']['Content'] = "Dear __NAME__,<br>\r\n<br>\r\n<br>\r\n\r\n<br>\r\n\r\nYour account information is:<br>\r\n<br>\r\nEmail: __EMAIL__<br>\r\nLogin ID: __LOGIN__<br>\r\nyou may reset your password at <a href='__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__'>__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__</a><br>\r\n\r\n<br>\r\n";
	$Lang['EmailNotification']['ForgotHashedPassword']['Content'] = "Dear __NAME__,<br>\r\n<br>\r\nAs per your request for password reset, please check your account information below and click the hyperlink to reset your password. <br>\r\n<br>\r\nAccount information is:<br>\r\nEmail: __EMAIL__<br>\r\nLogin ID: __LOGIN__<br>\r\n<br>\r\nPassword Reset Hyperlink<br>\r\n<a href='__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__'>__WEBSITE____RESET_PW_PATH__?Key=__RESETKEY__</a><br>\r\n";
	$Lang['EmailNotification']['ForgotHashedPassword_ACK']['Subject'] = $Lang['Header']['HomeTitle'].": Reset Password Acknowledgement";
	$Lang['EmailNotification']['ForgotHashedPassword_ACK']['Content'] = "A user has used forget password to reset his/her password<br>\r\nLogin: __LOGIN__<br>\r\nEmail: __EMAIL__<br>\r\n";
	//$Lang['EmailNotification']['Footer'] = "<br><br>\r\n\r\n------------------------------------------------------<br>\r\nThis email was sent to you by ".$Lang['Header']['HomeTitle'].".<br>\r\nFor any enquiry, please send mail to __WEBMASTER__.<br>\r\nFor the latest infomation, please go to __WEBSITE__.<br>\r\n";
	$Lang['EmailNotification']['Footer'] = "<br>\r\n<br>\r\n------------------------------------------------------<br>\r\nThis email was sent to you by DHL Hong Kong DHL360@HK.<br>\r\nFor any enquiry, please send mail to __WEBMASTER__.<br>\r\nFor the latest information, please go to __WEBSITE__.<br>\r\n";
	$Lang['EmailNotification']['DoNotReply'] = "This is an automated email. Please DO NOT respond.";

	/***********************************************/
	$Lang['General']['JS_warning']['SelectAtLeastOneTeacher'] = "Please choose at least one staff.";
	$Lang['General']['WarningArr']['PleaseSelectAtLeastOneTeacher'] = "Please select at least one staff";
	$Lang['SysMgr']['Homework']['AllowExport'] = "Allow all staff using export";
	$Lang['Circular']['TeacherCanViewAll'] = "Allow all staffs to view all circulars.";
	$Lang['Circular']['SearchStaffName'] = "Search by staff name";
	$Lang['eCircular']['NotifyStaffByPushMessage'] = "Notify staff using push message";
	
	$Lang['CommonChoose']['SelectTargetField']['TeacherStaff'] = "Select Staff";
	
	$Lang['eEnrolment']['jsWarning']['StudentCannotRemoveTeacher'] = "You do not have the right to remove staffs from the selection.";
	$Lang['eEnrolment']['NotificationLetter']['Content'] = "The <!--Name--> enrolment process has been completed. Your child has successfully enrolled in the <!--Type-->. You may go to the eService > eEnrolment page to view activities your child has been approved to take part in. Should you have any inquiries, please contact System Admin or staffs responsible for ECA enrolment.";
	
	$Lang['eclass']['setting']['TeacherEditEclass'] = "Allow staffs to add/edit/delete eClass";
	
	$Lang['AppNotifyMessage']['Public(Teacher)'] = "All staffs";
	$Lang['AppNotifyMessage']['NonPublic(Teacher)'] = "Specific staff(s)";
	$Lang['AppNotifyMessage']['ToPublic(Teacher)'] = "To all staffs";
	$Lang['AppNotifyMessage']['ToNonPublic(Teacher)'] = "To specific staff(s)";
	$Lang['AppNotifyMessage']['UsingTeacherAppTeacherOnly'] = "for teachers who are using Staff App only";
	$Lang['AppNotifyMessage']['Error']['no_teacher'] = "No such staff / staff not using Staff App";
	$Lang['AppNotifyMessage']['valid_table_title_teacher'] = "Message(s) will be sent to the following staff(s):";
	
	$Lang['AppNotifyMessage']['SchoolNews']['Title']['SCHOOL'] = "最新消息通知
Latest School News Reminder";

	$Lang['AppNotifyMessage']['UsingTeacherAppTeacherOnly'] = "for staffs who are using Staff App only";

	$i_Homework_admin_enable_export = "Allow All staff using export";
	$i_eClass_management_enable = "Enable eClass Management for staffs";
	$i_eClass_teacher_open_course = "Allow staffs to add/edit/delete eClass";
	$i_campusmail_all_teacher_staff="All staff";
	$i_campusmail_no_teacher_staff="No staff";
	$i_Assessment_UserSelectAdmin = "It will be marked as $i_general_sysadmin, if you do not choose any staff.";
	$i_ClubsEnrollment_MailConfirm3 = "Should you have any problems, please contact System Admin or the staffs responsible for ECA Enrolment.";
	$i_ec_file_target_teacher = "Target staff";
	$i_ec_file_warning = "Please select at least one staff who owns file(s)";
	$i_ec_file_warning2 = "Please select a staff to receive the ownership";
	$i_ec_file_msg_transfer = "File ownwership can be transfered to another staff so that these files can be managed when the staff is removed. Transfer to:";
	$i_Circular_Setting_TeacherCanViewAll = "Allow all staffs to view all circulars.";
	$ec_iPortfolio['update_more_comment'] = "Update staff comment again";
	$i_InventorySystem_ImportCaretaker_Warning = "Only staffs in the selected group will be imported";
	$ec_warning['olr_from_teacher'] = "This record is submmited by staff. You do not need to approve it!";
	$i_transfer_file_ownership = "Transfer file ownership for removed staff";
	
	$i_identity_teachstaff = "Staff";

$Lang['AppNotifyMessage']['eCircularSigned'] = "簽署職員通告確認通知  [[sign_notice_number]]
eNotice Signed Alert [[sign_notice_number]]";
$Lang['AppNotifyMessage']['eCircularSignedContent_eClassApp'] = "已收到閣下於 [sign_datetime] 簽署的職員通告[sign_notice_number]「[sign_notice_title]」。
Please note that the eNotice [sign_notice_number] titled \"[sign_notice_title]\" signed at [sign_datetime] has been received.";
/*$Lang['AppNotifyMessage']['eCircular']['Subject'] = "最新職員通告提示 [__NOTICENUMBER__]
Latest eNotice alert [__NOTICENUMBER__]"; */
$Lang['AppNotifyMessage']['eCircular']['Subject'] = "__TITLE__";
$Lang['AppNotifyMessage']['eCircular']['Title']['App'] = "職員通告重要提示 [[NoticeNumber]]
eNotice Important Reminder [[NoticeNumber]]";
$Lang['AppNotifyMessage']['eCircular']['Alert']['App'] = "請儘快簽署職員通告[NoticeNumber]「[NoticeTitle]」(簽署限期: [NoticeDeadline])。
Please sign the eNotice [NoticeNumber] titled \"[NoticeTitle]\" as soon as possible (Original Deadline: [NoticeDeadline]).";
$Lang['AppNotifyMessage']['eCircular']['Content'] = "請於__ENDDATE__或之前簽署職員通告__NOTICENUMBER__「__TITLE__」。
Please sign the eNotice __NOTICENUMBER__ titled \"__TITLE__\" on or before __ENDDATE__.";

$Lang['AppNotifyMessage']['SchoolNews']['Subject'] = "New __TYPE__ Announcement (__DATE__)";
$Lang['AppNotifyMessage']['SchoolNews']['Title']['SCHOOL'] = "__TITLE__";
$Lang['AppNotifyMessage']['SchoolNews']['Title']['GROUP'] = "__TITLE__";
$Lang['AppNotifyMessage']['SchoolNews']['Content']['SCHOOL'] = "請留意於__DATE__發放的「__TITLE__」最新消息。
Please note that the latest news of \"__TITLE__\" was released on __DATE__.";
$Lang['AppNotifyMessage']['SchoolNews']['Content']['GROUP'] = "請留意於__DATE__發放的「__TITLE__」小組最新消息。
Please note that the latest group news of \"__TITLE__\" was released on __DATE__.";
	
/*
	if ($_SERVER["REMOTE_ADDR"] == "10.0.3.133") {
		if (!function_exists("DHLTextReplace")) {
			function DHLTextReplace(&$item, $key)
			{
				$replaceArr = array(
						"teacher" => "<span style='color:#f00'>staff</span>",
						"教職員" => "<span style='color:#f00'>職員</span>",
						"校園最新消息" => "<span style='color:#f00'>最新消息</span>",
						"學校資訊 " => "<span style='color:#f00'>公司資訊</span>"
				);
				foreach ($replaceArr as $kk => $vv) {
					if (strpos($item, $kk) !== false) {
						echo $kk . " - " . $key . "<hr>";
						echo $item . "<br>";
						echo str_replace(array_keys($replaceArr), array_values($replaceArr), $item) . "<hr>";
					}
				}
			}
		}
		$allVariable = get_defined_vars();
		$gpc = array(&$allVariable);
		// $gpc = array(&$Lang);
		array_walk_recursive($gpc, 'DHLTextReplace');
		exit;
	}
*/
}
?>