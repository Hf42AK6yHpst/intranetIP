<?php
# Editing by 

### General language file ###
$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkAbsentZeorMark'] = "+";
$eReportCard['RemarkNotAssessed'] = "N.A.";

$eReportCard['Template']['ComponentSubjectPrefix'] = "　";

### Student Info Table
$eReportCard['Template']['NameEn'] = "Name";
$eReportCard['Template']['NameCh'] = "學生姓名";
$eReportCard['Template']['ClassNameEn'] = "Class Name";
$eReportCard['Template']['ClassNameCh'] = "班別";
$eReportCard['Template']['SexEn'] = "Sex";
$eReportCard['Template']['SexCh'] = "性別";
$eReportCard['Template']['MaleEn'] = "Male";
$eReportCard['Template']['MaleCh'] = "男";
$eReportCard['Template']['FemaleEn'] = "Female";
$eReportCard['Template']['FemaleCh'] = "女";
$eReportCard['Template']['HighestAverageInClassEn'] = "Highest Average In Class";
$eReportCard['Template']['HighestAverageInClassCh'] = "全班最高平均分";
$eReportCard['Template']['DOBEn'] = "DOB";
$eReportCard['Template']['DOBCh'] = "出生日期";
$eReportCard['Template']['RegNoEn'] = "Reg. No.";
$eReportCard['Template']['RegNoCh'] = "註冊編號";
$eReportCard['Template']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['DateOfIssueCh'] = "派發日期";

### Mark Table
$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['FullMarkEn'] = "Full Mark";
$eReportCard['Template']['FullMarkCh'] = "滿分";
$eReportCard['Template']['ResultsEn'] = "Results";
$eReportCard['Template']['ResultsCh'] = "成績";
$eReportCard['Template']['FirstTermEn'] = "Half-Yearly";
$eReportCard['Template']['FirstTermCh'] = "上學期";
$eReportCard['Template']['FirstTermShortEn'] = "HY";
$eReportCard['Template']['SecondTermEn'] = "Yearly";
$eReportCard['Template']['SecondTermCh'] = "下學期";
$eReportCard['Template']['SecondTermShortEn'] = "YR";
$eReportCard['Template']['AnnualEn'] = "Annual";
$eReportCard['Template']['AnnualCh'] = "全年";
$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['RankEn'] = "Rank";
$eReportCard['Template']['RankCh'] = "名次";
$eReportCard['Template']['GrandTotalEn'] = "Grand Total";
$eReportCard['Template']['GrandTotalCh'] = "總分";
$eReportCard['Template']['GrandAverageEn'] = "Average";
$eReportCard['Template']['GrandAverageCh'] = "平均分";
$eReportCard['Template']['PositionInClassEn'] = "Position in Class";
$eReportCard['Template']['PositionInClassCh'] = "全班名次";
$eReportCard['Template']['PositionInLevelEn'] = "Position in Level";
$eReportCard['Template']['PositionInLevelCh'] = "全級名次";
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";

### Misc Table 
$eReportCard['Template']['CommentEn'] = "Comment";
$eReportCard['Template']['CommentCh'] = "評語";
$eReportCard['Template']['MinorDemeritEn'] = "Minor Demerit";
$eReportCard['Template']['MinorDemeritCh'] = "小過";
$eReportCard['Template']['MajorDemeritEn'] = "Major Demerit";
$eReportCard['Template']['MajorDemeritCh'] = "大過";
$eReportCard['Template']['HasCompletedFormEn'] = "Has completed ";
$eReportCard['Template']['HasCompletedFormCh'] = "已完成";
$eReportCard['Template']['PromotedToFormEn'] = "Promoted to ";
$eReportCard['Template']['PromotedToFormCh'] = "升上";
$eReportCard['Template']['ToRepeatInFormEn'] = "To repeat in ";
$eReportCard['Template']['ToRepeatInFormCh'] = "重讀";
$eReportCard['Template']['PassingPercentageRemarkEn'] = "Passing % : 50 for S1-S3, 40 for S4-S6";

### Signature Table
$eReportCard['Template']['ClassTeacherEn'] = "Class Teacher";
$eReportCard['Template']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校長";
$eReportCard['Template']['ParentEn'] = "Parent";
$eReportCard['Template']['ParentCh'] = "家長";
$eReportCard['Template']['GuardianEn'] = "Guardian";
$eReportCard['Template']['GuardianCh'] = "監護人";

### Footer Table
$eReportCard['Template']['ExcellentEn'] = "Excellent";
$eReportCard['Template']['ExcellentCh'] = "優異";
$eReportCard['Template']['GoodEn'] = "Good";
$eReportCard['Template']['GoodCh'] = "良好";
$eReportCard['Template']['AverageEn'] = "Average";
$eReportCard['Template']['AverageCh'] = "滿意";
$eReportCard['Template']['UnsatisfactoryEn'] = "Unsatisfactory";
$eReportCard['Template']['UnsatisfactoryCh'] = "尚可";
$eReportCard['Template']['SeeExplanatoryNotesEn'] = "See overleaf for explanatory notes";
$eReportCard['Template']['SeeExplanatoryNotesCh'] = "請參閱背頁註釋";
?>