<?php
# Editing by ivan

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['SchoolNameCh'] = "蘇 浙 公 學";
$eReportCard['Template']['SchoolNameEn'] = "KIANGSU-CHEKIANG COLLEGE";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name( 姓名 )";
$eReportCard['Template']['StudentInfo']['Class'] = "Class( 班別 )";
$eReportCard['Template']['StudentInfo']['Gender'] = "Sex ( 性別 )";
$eReportCard['Template']['StudentInfo']['HKID'] = "I.D. Card No. ( 身份証號碼 )";
$eReportCard['Template']['StudentInfo']['DateOfIssue'] = "Date Of Issue ( 簽發日期 )";
$eReportCard['Template']['StudentInfo']['StudentNo'] = "Student No. ( 學號 )";
$eReportCard['Template']['StudentInfo']['STRN'] = "Student Reference Number (STRN) ( 學生編號 )";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";
$eReportCard['Template']['TestMarkEn'] = "Test Mark";
$eReportCard['Template']['TestMarkCh'] = "測驗成績";

// MS Table Header
$eReportCard['Template']['FullMarkEn'] = "Full Mark";
$eReportCard['Template']['FullMarkCh'] = "滿分";
$eReportCard['Template']['WeightEn'] = "Weight";
$eReportCard['Template']['WeightCh'] = "比重";
$eReportCard['Template']['1stTermEn'] = "1st Term";
$eReportCard['Template']['1stTermCh'] = "上學期";
$eReportCard['Template']['2ndTermEn'] = "2nd Term";
$eReportCard['Template']['2ndTermCh'] = "下學期";
$eReportCard['Template']['DailyEn'] = "Daily";
$eReportCard['Template']['DailyCh'] = "平時分";
$eReportCard['Template']['ExamEn'] = "Exam";
$eReportCard['Template']['ExamCh'] = "考試分";
$eReportCard['Template']['TotalEn'] = "Total";
$eReportCard['Template']['TotalCh'] = "總分";
$eReportCard['Template']['YearlyTotalEn'] = "Yearly Total";
$eReportCard['Template']['YearlyTotalCh'] = "全年總分";

// MS Table Footer
$eReportCard['Template']['WeightedGrandTotalEn'] = "Weighted Grand Total";
$eReportCard['Template']['WeightedGrandTotalCh'] = "比重總分";
$eReportCard['Template']['WeightedAverageEn'] = "Weighted Average";
$eReportCard['Template']['WeightedAverageCh'] = "比重平均分";
$eReportCard['Template']['PositionInStreamEn'] = "Position in Stream";
$eReportCard['Template']['PositionInStreamCh'] = "分組名次";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "全級排名";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";
$eReportCard['Template']['StreamNumOfStudentEn'] = "No. of Students in Stream";
$eReportCard['Template']['StreamNumOfStudentCh'] = "分組人數";


$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "Drop";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkAbsentZeorMark'] = "0";
$eReportCard['RemarkNotAssessed'] = "--";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['PromotionEn'] = "Promotion";
$eReportCard['Template']['PromotionCh'] = "升級";
$eReportCard['Template']['Promotion'] = "升級";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

$eReportCard['Template']['MajorMeritEn'] = "Major Merit";
$eReportCard['Template']['MajorMeritCh'] = "大功";
$eReportCard['Template']['MinorMeritEn'] = "Minor Merit";
$eReportCard['Template']['MinorMeritCh'] = "小功";
$eReportCard['Template']['MajorDemeritEn'] = "Major Demerit";
$eReportCard['Template']['MajorDemeritCh'] = "大過";
$eReportCard['Template']['MinorDemeritEn'] = "Minor Demerit";
$eReportCard['Template']['MinorDemeritCh'] = "小過";
$eReportCard['Template']['SelfDisciplineEn'] = "Self-discipline";
$eReportCard['Template']['SelfDisciplineCh'] = "自律";
$eReportCard['Template']['MannersEn'] = "Manners";
$eReportCard['Template']['MannersCh'] = "態度";
$eReportCard['Template']['SenseOfResponsibilityEn'] = "Sense of Responsibility";
$eReportCard['Template']['SenseOfResponsibilityCh'] = "責任感";
$eReportCard['Template']['LearningAttitudesEn'] = "Learning Attitudes";
$eReportCard['Template']['LearningAttitudesCh'] = "學習態度";

$eReportCard['Template']['CommentsEn'] = "Comments";
$eReportCard['Template']['CommentsCh'] = "評語";
$eReportCard['Template']['RemarksEn'] = "Remarks";
$eReportCard['Template']['RemarksCh'] = "備註";
$eReportCard['Template']['AssessmentReport']['RemarksEn'] = "Remarks";
$eReportCard['Template']['AssessmentReport']['RemarksCh'] = "備註";

#MiscTable
$eReportCard['Template']['SeeAppendixCh'] = "見附頁";
$eReportCard['Template']['SeeAppendixEn'] = "See Appendix";	

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "Class Teacher<br>班導師";
$eReportCard['Template']['Principal'] = "Principal<br>校長";
$eReportCard['Template']['ParentGuardian'] = "Parent / Guardian<br>家長 / 監護人";
$eReportCard['Template']['IssueDate'] = "Date of Issue<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "School Chop<br>校印";

$eReportCard['Template']['Report_ClassTeacher'] = "Class Teacher 班導師";
$eReportCard['Template']['Report_Principal'] = "Principal 校長";
$eReportCard['Template']['Report_ParentGuardian'] = "Parent / Guardian 家長/監護人";
$eReportCard['Template']['Report_IssueDate'] = "Date of Issue 派發日期";
$eReportCard['Template']['Report_SchoolChop'] = "School Chop 校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");


?>