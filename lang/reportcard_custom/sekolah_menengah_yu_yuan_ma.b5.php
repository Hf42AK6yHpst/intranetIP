<?php
# Editing by Connie


$eReportCard['Template']['StudentNameCh'] = "姓名";
$eReportCard['Template']['StudentNameEn'] = "Name";

$eReportCard['Template']['AdmisNoCh'] = "學號";
$eReportCard['Template']['AdmisNoEn'] = "Admission No";

$eReportCard['Template']['ClassCh'] = "班級";
$eReportCard['Template']['ClassEn'] = "Class";

$eReportCard['Template']['FormTeacherCh'] = "班導師";
$eReportCard['Template']['FormTeacherEn'] = "Form Teacher";

$eReportCard['Template']['SubjectsCh'] = "科目";
$eReportCard['Template']['SubjectsEn'] = "Subjects";

$eReportCard['Template']['P'] = "P";
$eReportCard['Template']['MarksEn'] = "Marks";

$eReportCard['Template']['AverageEn']="Average";
$eReportCard['Template']['AverageCh']="總平均";

$eReportCard['Template']['PositionEn']="Position";
$eReportCard['Template']['PositionCh']="名次";

$eReportCard['Template']['NoOfStudentEn']="No of Students";
$eReportCard['Template']['NoOfStudentCh']="全班人數";

$eReportCard['Template']['ConductEn']="Conduct";
$eReportCard['Template']['ConductCh']="操行";

$eReportCard['Template']['SchoolTermsEn']="School Terms";
$eReportCard['Template']['SchoolTermsCh']="上課日期";

$eReportCard['Template']['PerOfApprovedLeaveEn']="Periods of Approved Leave";
$eReportCard['Template']['PerOfApprovedLeaveCh']="請假節數";

$eReportCard['Template']['PerOfAbsenceEn']="Periods of Absence";
$eReportCard['Template']['PerOfAbsenceCh']="曠課節數";

$eReportCard['Template']['TeacherCh']="老師";

$eReportCard['Template']['FormTeacherSignEn']="Form Teacher's Signature";
$eReportCard['Template']['FormTeacherSignCh']="班導師簽名";

$eReportCard['Template']['PrincipalSignEn']="Principal's Signature";
$eReportCard['Template']['PrincipalSignCh']="校長簽名";

$eReportCard['Template']['ParentSignEn']="Parent's Signature";
$eReportCard['Template']['ParentSignCh']="家長簽名";


$eReportCard['Template']['FromEn'] = "From";
$eReportCard['Template']['ToEn'] = "To";

$eReportCard['Template']['FooterLine1'] = "7km, Jalan Utara, 90000 Sandakan. Peti Surat 968, 90710 Sandakan, Sabah, Malaysia.";
$eReportCard['Template']['FooterLine2'] = "Tel : 089-215762 Fax : 089-218843 E-mail : yuyuan@streamyx.com";


?>