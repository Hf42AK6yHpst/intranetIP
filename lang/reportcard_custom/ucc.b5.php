<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['SchoolName'] = "United Christian College 滙基書院";
$eReportCard['Template']['MidTermReportNameEn'] = "Mid-Term Report";
$eReportCard['Template']['MidTermReportNameCh'] = "中期報告";
$eReportCard['Template']['Term1En'] = "1st Term";
$eReportCard['Template']['Term1Ch'] = "上學期";
$eReportCard['Template']['Term2En'] = "2nd Term";
$eReportCard['Template']['Term2Ch'] = "下學期";

$eReportCard['Template']['StudentInfo']['Name'] = "Student Name 姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "Class 班別";
$eReportCard['Template']['StudentInfo']['RegNo'] = "Registration Number 註冊編號";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN 學生編號";
$eReportCard['Template']['StudentInfo']['DateIssue'] = "Date of Issue 派發日期";

$eReportCard['Template']['MSTable']['Subject'] = "Subject <br/> 科目";
$eReportCard['Template']['MSTable']['FullMark'] = "Full Mark <br/> 滿分";
$eReportCard['Template']['MSTable']['Result'] = "Results <br/> 成績";
$eReportCard['Template']['MSTable']['PositionInForm'] = "Position in Form <br/> 級名次";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
// $eReportCard['Template']['Remark'] = "Remark 備註";
// $eReportCard['Template']['RemarkStr'] = "1. 中國語文分數比例: 120, 120, 30, 30','2. 英國語文分數比例: 70, 70, 70, 70, 20', '3. \"abs\"=0分, \"--\"=合理缺席', '4. 英文網上學習: P.S.=合格, NI=有待改善'.'5.該生於普通話科總測驗缺席, 只考得了口試, 得分為 13/30'.'6.非華語學生以特別卷作答'";

$eReportCard['Template']['SubjectOverall'] = "Overall";
$eReportCard['Template']['Annual'] = "Annual";
$eReportCard['Template']['GrandTotal'] = "Grand Total";
$eReportCard['Template']['ClassTeacher'] = "Class Teacher <br> 班主任";
$eReportCard['Template']['Principal'] = "Principal <br/> 校長";
$eReportCard['Template']['ParentGuardian'] = "Parent / Guardian <br/> 家長／監護⼈";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['FormTeacher'] = "Form Teacher";
$eReportCard['Template']['SchoolChop'] = "School Chop <br/> 校印";
$eReportCard['Template']['Signature'] = "Signature";
$eReportCard['Template']['Position'] = "Position";
$eReportCard['Template']['Effort'] = "Effort";
$eReportCard['Template']['ProgressPerformance'] = "Progress<br/>Performance";
$eReportCard['Template']['ProgressChart'] = "Progress Chart";

$eReportCard['Template']['DisciplineRecord'] = "Discipline Record 獎懲紀錄";
$eReportCard['Template']['GoodSchWorkEn'] = "Good School Work";
$eReportCard['Template']['GoodSchWorkCh'] = "學業良好紀錄";
$eReportCard['Template']['GoodConductEn'] = "Good Conduct";
$eReportCard['Template']['GoodConductCh'] = "操行良好紀錄";
$eReportCard['Template']['MeritEn'] = "Merit";
$eReportCard['Template']['MeritCh'] = "優點";
$eReportCard['Template']['BadSchWorkEn'] = "Bad School Work";
$eReportCard['Template']['BadSchWorkCh'] = "學業不良紀錄";
$eReportCard['Template']['BadConductEn'] = "Bad Conduct";
$eReportCard['Template']['BadConductCh'] = "操行不良紀錄";
$eReportCard['Template']['MiniDemeritEn'] = "Mini Demerit";
$eReportCard['Template']['MiniDemeritCh'] = "缺點";
$eReportCard['Template']['MinDemeritEn'] = "Minor Demerit";
$eReportCard['Template']['MinDemeritCh'] = "小過";
$eReportCard['Template']['MajDemeritEn'] = "Major Demerit";
$eReportCard['Template']['MajDemeritCh'] = "大過";

//$eReportCard['Template']['Remark'] = "Remark 備註";

$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher’s Comment 班主任評語";
$eReportCard['Template']['eca'] = "ECA 課外活動";

//email "From Lau Sir (UCCKE)" 2015-12-15 13:47:56 (Tuesday)
//$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkExempted'] = "----";
//
//$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkDropped'] = "----";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "N.A.";
// 2014-0703-0932-14140
$eReportCard['RemarkAbsentZeorMark'] = "U";

$eReportCard['DisplayOverallResult'] = "顯示總成績";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowFormPosition'] = "顯示級名次";

$eReportCard['Template']['Promotion'] = "Promotion";
$eReportCard['Template']['Retention'] = "Retention";
$eReportCard['Template']['ToBePromotedTo'] = "To be promoted to ";
$eReportCard['Template']['ByConcession'] = " by concession";
$eReportCard['Template']['ToBeRetainedIn'] = "To be retained in ";

$eReportCard['ExtraInfoLabel'] = "努力";
$eReportCard['AdjustPositionLabal'] = "名次";

$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

# otherInfo
$eReportCard['Template']['Reading'] = "閱讀";
$eReportCard['StudentResultSummary']['Reading'] = "閱讀";

$eReportCard['Template']['StudentCertficateEn1'] = "Honour Student";
$eReportCard['Template']['StudentCertficateEn2'] = "Certificate";
$eReportCard['Template']['StudentCertficateCh'] = "獎狀";
$eReportCard['Template']['AcademicCertficateEn'] = "Academic Certificate";
$eReportCard['Template']['AcademicCertficateCh'] = "學業獎狀";

$eReportCard['FormTeacherCommentReport']['YearlyTheme'] = "年度主題";
$eReportCard['FormTeacherCommentReport']['ThemeVerse'] = "主題經文";
?>