<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

$eReportCard['Template']['SchoolNameEn'] = "Chinese YMCA Secondary School";
$eReportCard['Template']['SchoolNameCh'] = "中  華  基  督  教  青  年  會  中  學";

$eReportCard['Template']['AcademicReportEn'] = "Academic Report";
$eReportCard['Template']['AcademicReportCh'] = "學 業 成 績 表";

$eReportCard['Template']['FirstTermEn'] = "First Term";
$eReportCard['Template']['FirstTermCh'] = "上學期";

$eReportCard['Template']['AnnualTitleEn'] = "Annual";
$eReportCard['Template']['AnnualTitleCh'] = "全年";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['Gender'] = "Sex";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";
$eReportCard['Template']['StudentInfo']['RegNo'] = "Reg No";

$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "學生姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班別";
$eReportCard['Template']['StudentInfo']['GenderEn'] = "Sex";
$eReportCard['Template']['StudentInfo']['GenderCh'] = "性別";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "STRN";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "學生編號";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "Class No";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "班號";
$eReportCard['Template']['StudentInfo']['DateOfBirthEn'] = "Date of Birth";
$eReportCard['Template']['StudentInfo']['DateOfBirthCh'] = "出生日期";
$eReportCard['Template']['StudentInfo']['RegNoEn'] = "Reg No";
$eReportCard['Template']['StudentInfo']['RegNoCh'] = "註冊編號";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "派發日期";

$eReportCard['Template']['Signature']['NameOfClassTeacherEn'] = "Name of Class Teacher(s)";
$eReportCard['Template']['Signature']['NameOfClassTeacherCh'] = "班主任姓名";
$eReportCard['Template']['Signature']['PrincipalNameEn'] = "Principal's Name";
$eReportCard['Template']['Signature']['PrincipalNameCh'] = "校長姓名";
$eReportCard['Template']['Signature']['PrincipalSignEn'] = "Principal's Signature";
$eReportCard['Template']['Signature']['PrincipalSignCh'] = "校長簽署";


# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkNotAssessed'] = "---";
//2014-0123-1043-02066
//$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkExempted'] = "-";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";

$eReportCard['Template']['GradeEn'] = "Grade";
$eReportCard['Template']['GradeCh'] = "等級";
$eReportCard['Template']['ContinousAssessmentEn'] = "Continuous Assessment";
$eReportCard['Template']['ContinousAssessmentCh'] = "持續評估";
$eReportCard['Template']['ExaminationEn'] = "Examination";
$eReportCard['Template']['ExaminationCh'] = "考試";
$eReportCard['Template']['TestEn'] = "Test";
$eReportCard['Template']['TestCh'] = "測驗";
$eReportCard['Template']['FirstTermEn'] = "First Term";
$eReportCard['Template']['FirstTermCh'] = "上學期";
$eReportCard['Template']['SecondTermEn'] = "Second Term";
$eReportCard['Template']['SecondTermCh'] = "下學期";
$eReportCard['Template']['AnnualEn'] = "Year Grade";
$eReportCard['Template']['AnnualCh'] = "全年";

# CSV Info
$eReportCard['Template']['CommentsEn'] = "Comments";
$eReportCard['Template']['CommentsCh'] = "評語";

$eReportCard['Template']['PersonalDevelopmentEn'] = "Personal Development";
$eReportCard['Template']['PersonalDevelopmentCh'] = "個人成長";
$eReportCard['Template']['AttendanceEn'] = "Attendance(%)";
$eReportCard['Template']['AttendanceCh'] = "出席百分率";
$eReportCard['Template']['PunctualityEn'] = "Punctuality(%)";
$eReportCard['Template']['PunctualityCh'] = "準時百分率";
$eReportCard['Template']['IntegrityEn'] = "Integrity";
$eReportCard['Template']['IntegrityCh'] = "誠信";
$eReportCard['Template']['ConfidenceEn'] = "Confidence";
$eReportCard['Template']['ConfidenceCh'] = "自信";
$eReportCard['Template']['HumilityEn'] = "Humility";
$eReportCard['Template']['HumilityCh'] = "謙虛";
$eReportCard['Template']['GratitudeEn'] = "Gratitude";
$eReportCard['Template']['GratitudeCh'] = "感恩";

$eReportCard['Template']['AwardsEn'] = "Awards / Duties / Activities"; 
$eReportCard['Template']['AwardsCh'] = "獎項 / 職務 / 活動";



?>