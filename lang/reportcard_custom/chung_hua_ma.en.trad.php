<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

## School Info
$eReportCard['Template']['SchoolInfo']['NameEn'] = "CHUNG HUA HIGH SCHOOL";
$eReportCard['Template']['SchoolInfo']['NameCh'] = "芙 蓉 中 華 中 學";
$eReportCard['Template']['SchoolInfo']['Address'] = "Jalan Tun Dr. Ismail, 70200 Seremban, Negeri Sembilan, Malaysia";
$eReportCard['Template']['SchoolInfo']['Tel'] = "Tel: 06-7612782";
$eReportCard['Template']['SchoolInfo']['Fax'] = "Fax: 06-7621890";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['StudentAdmNoEn'] = "Adm. No";
$eReportCard['Template']['StudentInfo']['StudentAdmNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['AcademicYearEn'] = "Year";
$eReportCard['Template']['StudentInfo']['AcademicYearCh'] = "年份";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['OverallResultEn'] = "Total";
$eReportCard['Template']['OverallResultCh'] = "共計";
$eReportCard['Template']['AvgMarkEn'] = "Average %";
$eReportCard['Template']['AvgMarkCh'] = "總平均";
$eReportCard['Template']['ClassPositionEn'] = "Position In Class";
$eReportCard['Template']['ClassPositionCh'] = "名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "Class Enrolment";
$eReportCard['Template']['ClassNumOfStudentCh'] = "全班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "NA";

$eReportCard['Template']['SubjectEng'] = "Subjects";
$eReportCard['Template']['SubjectChn'] = "科目";
$eReportCard['Template']['SemesterEn'] = 'Semester';
$eReportCard['Template']['SemesterCh'] = '學期';
$eReportCard['Template']['AverageEn'] = 'Average';
$eReportCard['Template']['AverageCh'] = '平均';
$eReportCard['Template']['PeriodEn'] = 'Period(s)';
$eReportCard['Template']['PeriodCh'] = '學點';
$eReportCard['Template']['TotalEn'] = 'Total';
$eReportCard['Template']['TotalCh'] = '積分';

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct Marks";
$eReportCard['Template']['ConductCh'] = "操行分數"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";
$eReportCard['Template']['ClassTeacherCommentEn'] = "Comment";
$eReportCard['Template']['ClassTeacherCommentCh'] = "評語";

$eReportCard['Template']['ActualAttendanceEn'] = "Actual Attendance";
$eReportCard['Template']['ActualAttendanceCh'] = "上課日數";
$eReportCard['Template']['TotalAttendanceEn'] = "Total Attendance";
$eReportCard['Template']['TotalAttendanceCh'] = "應上課日數";
$eReportCard['Template']['MedicalLeaveEn'] = "Medical Leave";
$eReportCard['Template']['MedicalLeaveCh'] = "病假";
$eReportCard['Template']['OfficialLeaveEn'] = "Official Leave";
$eReportCard['Template']['OfficialLeaveCh'] = "公假";
$eReportCard['Template']['PersonalLeaveEn'] = "Personal Leave";
$eReportCard['Template']['PersonalLeaveCh'] = "事假";
$eReportCard['Template']['LeaveWithoutPermissionEn'] = "Leave Without Permission";
$eReportCard['Template']['LeaveWithoutPermissionCh'] = "曠課";
$eReportCard['Template']['AverageConductEn'] = "Average Conduct";
$eReportCard['Template']['AverageConductCh'] = "全年操行";
$eReportCard['Template']['ECAEn'] = "ECA";
$eReportCard['Template']['ECACh'] = "課外活動";
$eReportCard['Template']['Services&DutiesEn'] = "Services and Duties";
$eReportCard['Template']['Services&DutiesCh'] = "服務";
$eReportCard['Template']['AwardEn'] = "Award";
$eReportCard['Template']['AwardCh'] = "獎勵";
$eReportCard['Template']['PunishmentEn'] = "Punishment";
$eReportCard['Template']['PunishmentCh'] = "懲罰";
$eReportCard['Template']['ClassEnrolledNextYearEn'] = "Class to be enrolled next year";
$eReportCard['Template']['ClassEnrolledNextYearCh'] = "明年編入年級";


# Signature Table
$eReportCard['Template']['ClassTeacher'] = "&nbsp;&nbsp;班導師簽蓋<br />&nbsp;&nbsp;Signature<br />&nbsp;&nbsp;of<br />&nbsp;&nbsp;Class Teacher";
$eReportCard['Template']['Principal'] = "&nbsp;&nbsp;校長簽蓋<br />&nbsp;&nbsp;Signature<br />&nbsp;&nbsp;of<br />&nbsp;&nbsp;Principal";
$eReportCard['Template']['ParentGuardian'] = "&nbsp;&nbsp;家長簽蓋<br />&nbsp;&nbsp;Signature<br />&nbsp;&nbsp;of<br />&nbsp;&nbsp;Parent / Guardian";$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");


?>