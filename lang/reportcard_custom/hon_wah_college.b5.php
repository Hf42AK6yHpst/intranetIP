<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['StudentInfo']['Name'] = "學生姓名 Name";
$eReportCard['Template']['StudentInfo']['Class'] = "班別 Class Name";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別 Sex";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號 STRN";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "班號 Class No.";
$eReportCard['Template']['StudentInfo']['DateOfBirth'] = "出生日期 Date of Birth";
$eReportCard['Template']['StudentInfo']['RegNo'] = "註冊編號 Reg. no.";
$eReportCard['Template']['StudentInfo']['DateOfIssue'] = "派發日期 Date of Issue";

$eReportCard['Template']['StudentInfo']['Male'] = "男 Male";
$eReportCard['Template']['StudentInfo']['Female'] = "女 Female";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Rank'] = "名次<br>Rank";
// $eReportCard['Template']['Remark'] = "Remark 備註";
// $eReportCard['Template']['RemarkStr'] = "1. 中國語文分數比例: 120, 120, 30, 30','2. 英國語文分數比例: 70, 70, 70, 70, 20', '3. \"abs\"=0分, \"--\"=合理缺席', '4. 英文網上學習: P.S.=合格, NI=有待改善'.'5.該生於普通話科總測驗缺席, 只考得了口試, 得分為 13/30'.'6.非華語學生以特別卷作答'";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['ClassTeacher'] = "班主任 (Class Teacher)";
$eReportCard['Template']['ActingPrincipal'] = "校長 (Principal)";
$eReportCard['Template']['ParentGuardian'] = "家長 / 監護人 (Parent / Guardian)";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['ClassTeacherComment'] = "評語 Comment(s)";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['Award'] = "獎項 Award(s)";
$eReportCard['Template']['Remark'] = "備註 Remarks";

//$eReportCard['Template']['FormRank'] = "全級排名 (名次)/(全級總人數) Rank: (Rank)/(Total number of students):";
//$eReportCard['Template']['FormRankRemarks'] = "*只顯示全級前90名同學的名次。The rank is shown only if the student is ranked between 1st and 90th.";
$eReportCard['Template']['FormRank'] = "全級排名 Form Rank:";
$eReportCard['Template']['FormRankRemarks'] = "*只顯示全級前90名學生的名次。The rank is shown only if the student is ranked between the 1st and 90th.";
$eReportCard['Template']['Absence'] = "缺席日數 Absence";
$eReportCard['Template']['Lateness'] = "遲到次數 Lateness";
$eReportCard['Template']['DayUnit'] = "(days)";
$eReportCard['Template']['TimeUnit'] = "(times)";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['DisplayOverallResult'] = "顯示總成績";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowFormPosition'] = "顯示級名次";

$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

# Summary Info
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";

$eReportCard['Template']['FirstTerm'] = "上學期<br />First Term";
$eReportCard['Template']['SecondTerm'] = "下學期<br />Second Term";
$eReportCard['Template']['SecondTermCh'] = "下學期";
$eReportCard['Template']['WholeYear'] = "全學年<br />Academic Year";
$eReportCard['Template']['GraduationReport'] = "大學預科結業試<br />Matriculation Class<br />Final Examination";
$eReportCard['Template']['F7_GraduationReport'] = "大學預科結業試<br />Matriculation Class<br />Final Examination";
$eReportCard['Template']['F5_GraduationReport'] = "畢業試<br />Graduate Examination";

$eReportCard['Template']['SchemesFullMarkCh'] = "滿分";
$eReportCard['Template']['SchemesFullMarkEn'] = "Full Mark";
$eReportCard['Template']['WholeYearCh'] = "全學年";
$eReportCard['Template']['WholeYearEn'] = "Whole Year";

# Conduct Table
$eReportCard['Template']['ConductTableTitle'] = "操行評級 Conduct";
$eReportCard['Template']['StudentPerformanceEn'] = "Student Performance";
$eReportCard['Template']['StudentPerformanceCh'] = "學生表現";
$eReportCard['Template']['ConductGradeEn'] = "Grade";
$eReportCard['Template']['ConductGradeCh'] = "操行等第";
$eReportCard['Template']['ConductOverall'] = "品德言行";
$eReportCard['Template']['ConductOverallEn'] = "Morality";
$eReportCard['Template']['ConductOverallCh'] = "品德言行";
$eReportCard['Template']['Courtesy RequirementsEn'] = "Courtesy Requirements";
$eReportCard['Template']['Courtesy RequirementsCh'] = "禮貌要求";
$eReportCard['Template']['Appearance RequirementsEn'] = "Appearance Requirements";
$eReportCard['Template']['Appearance RequirementsCh'] = "儀容要求";
$eReportCard['Template']['Manners and Social SkillsEn'] = "Manners and Social Skills";
$eReportCard['Template']['Manners and Social SkillsCh'] = "待人處事";
$eReportCard['Template']['Learning AttitudeEn'] = "Learning Attitude";
$eReportCard['Template']['Learning AttitudeCh'] = "學習態度";
$eReportCard['Template']['Discipline RequirementsEn'] = "Discipline Requirements";
$eReportCard['Template']['Discipline RequirementsCh'] = "守紀要求";
$eReportCard['Template']['AppearanceEn'] = "Appearance";
$eReportCard['Template']['AppearanceCh'] = "儀表";
$eReportCard['Template']['AttitudeEn'] = "Attitude";
$eReportCard['Template']['AttitudeCh'] = "態度";
$eReportCard['Template']['AspirationEn'] = "Aspiration";
$eReportCard['Template']['AspirationCh'] = "志向";
$eReportCard['Template']['CompetenceEn'] = "Competence";
$eReportCard['Template']['CompetenceCh'] = "能力";
$eReportCard['Template']['DisciplineEn'] = "Discipline";
$eReportCard['Template']['DisciplineCh'] = "守紀";
$eReportCard['Template']['ServiceHourEn'] = "Service hour(s)";
$eReportCard['Template']['ServiceHourCh'] = "參與服務時數紀錄";
//$eReportCard['Template']['HourUnit'] = "小時(hrs)";
//$eReportCard['Template']['ConductBonus'] = "熱心服務，操行獲加<!--ConductBonus-->分。";
$eReportCard['Template']['HourUnit'] = "hour(s)";
$eReportCard['Template']['ConductBonus'] = "Additional <!--ConductBonus--> mark(s) in couduct for outstanding service.";

# Post Table
$eReportCard['Template']['PostTableTitle'] = "參與職務紀錄 Service";
$eReportCard['Template']['StudentOrganization'] = "學生組織 School Club";
$eReportCard['Template']['PostTableTitleV2'] = "參與職務紀錄 Service Record";
$eReportCard['Template']['StudentOrganizationV2'] = "學生組織 School Club and Teams";
$eReportCard['Template']['Position'] = "職務 Post";

# Activity Table
$eReportCard['Template']['InternalActivityTitle'] = "校內活動紀錄 School Activities Record";
$eReportCard['Template']['ExternalActivityTitle'] = "校外活動紀錄 External Activities Record";
$eReportCard['Template']['ActivityDate'] = "活動日期 Date";
//$eReportCard['Template']['ActivitySemester'] = "學期 Semester";
$eReportCard['Template']['ActivitySemester'] = "學期 Term";
$eReportCard['Template']['ActivityItem'] = "活動項目 Activities";
$eReportCard['Template']['ActivityOrganizer'] = "主辦單位 Organizer";
$eReportCard['Template']['ActivityMerit'] = "獎項 Award";

//2015-0703-1343-26073
$eReportCard['RemarkNotAssessed'] = "N.A.";
?>