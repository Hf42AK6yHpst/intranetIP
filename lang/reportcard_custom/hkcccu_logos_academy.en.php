<?php
# Editing by 

### General language file ###
//$eReportCard['RemarkExempted'] = "/";
//$eReportCard['RemarkDropped'] = "*";
//$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
//$eReportCard['RemarkAbsentZeorMark'] = "+";
$eReportCard['RemarkNotAssessed'] = "N/A";

$eReportCard['Template']['ReportView']['DSE'] = "DSE";
$eReportCard['Template']['ReportView']['IB'] = "IB";

$eReportCard['Template']['StudentParticularsEn'] = "Student Particulars";
$eReportCard['Template']['StudentParticularsCh'] = "學生資料";
$eReportCard['Template']['NameEn'] = "Name";
$eReportCard['Template']['NameCh'] = "姓名";
$eReportCard['Template']['GenderEn'] = "Gender";
$eReportCard['Template']['GenderCh'] = "性別";
$eReportCard['Template']['ClassEn'] = "Class";
$eReportCard['Template']['ClassCh'] = "班別";
$eReportCard['Template']['ClassNoEn'] = "Class No.";
$eReportCard['Template']['ClassNoCh'] = "班號";
$eReportCard['Template']['AttendanceRecordEn'] = "Attendance Record";
$eReportCard['Template']['AttendanceRecordCh'] = "考勤紀錄";
$eReportCard['Template']['NumOfDaysAbsentEn'] = "Number of Days Absent";
$eReportCard['Template']['NumOfDaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['EarlyLeaveEn'] = "Early Leave";
$eReportCard['Template']['EarlyLeaveCh'] = "早退次數";
$eReportCard['Template']['LatenessEn'] = "Lateness";
$eReportCard['Template']['LatenessCh'] = "遲到次數";

$eReportCard['Template']['StudentPerformanceEn'] = "Student Performance";
$eReportCard['Template']['StudentPerformanceCh'] = "學生表現";
$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['ContinuousAssessmentEn'] = "Continuous Assessment";
$eReportCard['Template']['ContinuousAssessmentCh'] = "持續性評估";
$eReportCard['Template']['SummativeAssessmentEn'] = "Summative Assessment";
$eReportCard['Template']['SummativeAssessmentCh'] = "總結性評估";
$eReportCard['Template']['EffortGradeEn'] = "Effort Grade";
$eReportCard['Template']['EffortGradeCh'] = "努力表現";
$eReportCard['Template']['GradePointSymbol'] = "‧";
$eReportCard['Template']['GradeEn'] = "Grade";
$eReportCard['Template']['GradeCh'] = "等第";

$eReportCard['Template']['HomeroomTeachersRemarksEn'] = "Homeroom Teacher's Remarks";
$eReportCard['Template']['HomeroomTeachersRemarksCh'] = "班主任評語";
$eReportCard['Template']['AwardsServicesAndActivitiesEn'] = "Awards, Services & Activities";
$eReportCard['Template']['AwardsServicesAndActivitiesCh'] = "獎項、服務及活動";
$eReportCard['Template']['SchoolChopEn'] = "School Chop";
$eReportCard['Template']['SchoolChopCh'] = "校印";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['ParentGuardianSignatureEn'] = "Parent / Guardian Signature";
$eReportCard['Template']['ParentGuardianSignatureCh'] = "家長或監護人簽署";
$eReportCard['Template']['HomeroomTeacherEn'] = "Homeroom Teacher";
$eReportCard['Template']['HomeroomTeacherCh'] = "班主任";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校長";
$eReportCard['Template']['PrincipalNameEn'] = "Dr. Paul CHO";
$eReportCard['Template']['PrincipalNameCh'] = "曹希銓博士";
$eReportCard['Template']['PBLEn'] = 'PROJECT-BASED LEARNING (PBL)';
$eReportCard['Template']['PBLChi'] = '專題研習';
$eReportCard['Template']['PBLTitleEn'] = 'Title';
$eReportCard['Template']['PBLTitleCh'] = '題目';
$eReportCard['Template']['AttainmentEn'] = 'Attainment';
$eReportCard['Template']['AttainmentCh'] = '成績';

$eReportCard['Template']['DateOfIssue'] = "Date of Issue";
$eReportCard['Template']['Ref'] = "Ref";
$eReportCard['Template']['PageOf'] = "Page <!--curPageNum--> of <!--totalPageNum-->";

$eReportCard['Template']['OverallEn'] = "Overall";
$eReportCard['Template']['OverallCh'] = "總成績";
?>