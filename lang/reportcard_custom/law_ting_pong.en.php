<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";
$eReportCard['ExtraInfoLabel'] = "Attitude";
$eReportCard['Template']['ReportView']['Normal'] = "Normal";
$eReportCard['Template']['ReportView']['Personalized'] = "Personalized";

$eReportCard['RemarkAbsentZeorMark'] = "ABS";
$eReportCard['RemarkAbsentNotConsidered'] = "--";
$eReportCard['RemarkDropped'] = "--";
$eReportCard['RemarkExempted'] = "--";
$eReportCard['RemarkNotAssessed'] = "--";

# Report Title
$eReportCard['Template']['ReportTitleEn'] = "Report Card";
$eReportCard['Template']['ReportTitleCh'] = "成績表";
$eReportCard['Template']['MockReportTitle'] = "Mock Examination Report";
$eReportCard['Template']['AcademicPerformanceEn'] = "Academic Performance";
$eReportCard['Template']['AcademicPerformanceCh'] = "學業表現";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['StudentNo'] = "STRN";
$eReportCard['Template']['StudentInfo']['ClassClassNo'] = "Class and Class Number";
$eReportCard['Template']['StudentInfo']['DateIssue'] = "Date of Issue";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "學生姓名";
$eReportCard['Template']['StudentInfo']['StudentNoEn'] = "Student No.";
$eReportCard['Template']['StudentInfo']['StudentNoCh'] = "學生編號";
$eReportCard['Template']['StudentInfo']['ClassClassNoEn'] = "Class and Class Number";
$eReportCard['Template']['StudentInfo']['ClassClassNoCh'] = "班別及學號";
$eReportCard['Template']['StudentInfo']['DateIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateIssueCh'] = "派發日期";

# Mark Table
$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['AssessScoreEn'] = "Assessment Score";
$eReportCard['Template']['AssessScoreCh'] = "評估分數";
$eReportCard['Template']['ContiAccessEn'] = "Continuous Assessment";
$eReportCard['Template']['ContiAccessCh'] = "持續評估";
$eReportCard['Template']['SummatiAccessEn'] = "Summative Assessment";
$eReportCard['Template']['SummatiAccessCh'] = "總結性評估";
$eReportCard['Template']['MockExamEn'] = "Mock Examination";
$eReportCard['Template']['MockExamCh'] = "模擬考試";
$eReportCard['Template']['TGradeEn'] = "Subject Grade/Level";
$eReportCard['Template']['TGradeCh'] = "等級";
$eReportCard['Template']['WGradeEn'] = "Subject Grade/Level";
$eReportCard['Template']['WGradeCh'] = "全年等級";
$eReportCard['Template']['AttitudeEn'] = "Attitude to Learning";
$eReportCard['Template']['AttitudeCh'] = "學習態度";
$eReportCard['Template']['SubjectWeightEn'] = "Subject Weighting";
$eReportCard['Template']['SubjectWeightCh'] = "學科比重";
$eReportCard['Template']['SubjectWeightRemark'] = "[%]";
$eReportCard['Template']['ScoreEn'] = "Score";
$eReportCard['Template']['ScoreCh'] = "分數";
$eReportCard['Template']['ScoreRemark'] = "(Full Mark<br/>滿分: 100)";
$eReportCard['Template']['GradeEn'] = "Grade";
$eReportCard['Template']['GradeCh'] = "等級";
$eReportCard['Template']['LevelEn'] = "Level";
$eReportCard['Template']['LevelCh'] = "等級";
$eReportCard['Template']['CommentsEn'] = "Comments";
$eReportCard['Template']['CommentsCh'] = "評語";

# Misc Table
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['DaysAbsentwReasonEn'] = "Days Absent with Valid Reason";
$eReportCard['Template']['DaysAbsentwReasonCh'] = "經核准缺席日數";
$eReportCard['Template']['DaysAbsentwoReasonEn'] = "Days Absent without Valid Reason";
$eReportCard['Template']['DaysAbsentwoReasonCh'] = "非核准缺席日數";
$eReportCard['Template']['EarlyLeaveEn'] = "Early Leave";
$eReportCard['Template']['EarlyLeaveCh'] = "早退日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
//$eReportCard['Template']['TimesLateCh'] = "遲到日數";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['DaysUnit'] = "day(s) 日";
$eReportCard['Template']['TimesUnit'] = "time(s) 次";
$eReportCard['Template']['PointsUnit'] = "point(s) 分";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['TermAverageEn'] = "Average Score";
$eReportCard['Template']['TermAverageCh'] = "平均分";
$eReportCard['Template']['YearAverageEn'] = "Average Score";
$eReportCard['Template']['YearAverageCh'] = "平均分";
$eReportCard['Template']['HighestAverageEn'] = "Highest Average in Form";
$eReportCard['Template']['HighestAverageCh'] = "全級最高平均分";
$eReportCard['Template']['LevelPositionEn'] = "Position in Form";
$eReportCard['Template']['LevelPositionCh'] = "級名次";
$eReportCard['Template']['CommentEn'] = "Comments";
$eReportCard['Template']['CommentCh'] = "評語";
// $eReportCard['Template']['AwardsEn'] = "Awards and Punishments";
// $eReportCard['Template']['AwardsCh'] = "奬懲";
$eReportCard['Template']['AwardsEn'] = "Awards and Pastoral Records";
$eReportCard['Template']['AwardsCh'] = "獎項及學生行為記錄";
$eReportCard['Template']['COAEn'] = "Activities and Positions of Responsibility";
$eReportCard['Template']['COACh'] = "活動及服務崗位";
$eReportCard['Template']['PointsEn'] = "LTPSS Points";
$eReportCard['Template']['PointsCh'] = "羅中積分";

# CSV Info
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

# Signature Table with lang
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校長";
$eReportCard['Template']['ClassTeacherEn'] = "Class Teacher";
$eReportCard['Template']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['ParentGuardianEn'] = "Parent/Guardian";
$eReportCard['Template']['ParentGuardianCh'] = "父母/監護人";
$eReportCard['Template']['SchoolChopEn'] = "School Chop";
$eReportCard['Template']['SchoolChopCh'] = "學校蓋印";

# Remarks
$eReportCard['Template']['Remarks'] = "Remarks";
$eReportCard['Template']['RemarksContent'][0] = "Weightings of various assessment components are correct to 1 decimal place.";
$eReportCard['Template']['RemarksContent'][1] = "The full mark for Assessment Scores is 100.";
$eReportCard['Template']['RemarksContent'][2] = "For S4 – S6 the performance grades follow the DSE Grade Scale of 5** to 1. Please refer to the HKEAA for the most updated information on the level descriptors. For S1 – S3 the performance grades range from A* to E where Grade D denotes a performance not meeting the required standard of the school. Please refer to the school website (Academic section) for the mark ranges of each grade / level of each subject.";
//2018-0601-1431-32164
//$eReportCard['Template']['RemarksContent'][3] = "The following subjects in the Junior Curriculum use 100% Continuous Assessment and do not hold a Final Exam – ICT, PE, Design and Technology, Food Science, Performing Arts, Visual Arts and Music.";
$eReportCard['Template']['RemarksContent'][3] = "The following subjects in the Junior Curriculum use 100% Continuous Assessment and do not hold a Final Exam – ICT, PE, Design and Technology, Food Science, Performing Arts and Visual Arts.";
$eReportCard['Template']['RemarksContent'][4] = "Please refer to the table below for the descriptors of Attitude to Learning.";
$eReportCard['Template']['MockRemarksContent'][0] = "Weightings of various assessment components are correct to 1 decimal place.";
$eReportCard['Template']['MockRemarksContent'][1] = "The full mark for Assessment Scores is 100.";
$eReportCard['Template']['MockRemarksContent'][2] = "The S6 performance grades follow the DSE Grade Scale of 5** to 1. Please refer to the HKEAA for the most updated information on the level descriptors.";
$eReportCard['Template']['MockRemarksContent'][3] = "Please refer to the table below for the descriptors of Attitude to Learning.";

$eReportCard['Template']['AttitudeLevel']['Attitude'] = "Attitude";
$eReportCard['Template']['AttitudeLevel']['Level'] = "Level";
$eReportCard['Template']['AttitudeLevel']['Descriptor'] = "Descriptor";
$eReportCard['Template']['AttitudeLevel']['LevelTable'][6] = "The student meets both the pastoral and academic demands with the highest quality in terms of effort and enthusiasm all the time.";
$eReportCard['Template']['AttitudeLevel']['LevelTable'][5] = "The student meets the pastoral but not necessarily academic demands with very high quality most of the time. Although not quite the standard to merit excellence, the student has shown great eagerness to learn.";
$eReportCard['Template']['AttitudeLevel']['LevelTable'][4] = "The student has enthusiasm and a desire to meet the pastoral and academic demands of the school.";
$eReportCard['Template']['AttitudeLevel']['LevelTable'][3] = "The student meets the basic level of expectation, has some enthusiasm, contributes reasonably to group work and gets on well with others.";
$eReportCard['Template']['AttitudeLevel']['LevelTable'][2] = "The student shows inconsistent achievement of basic level of expectation. More effort is needed to become satisfactory.";
$eReportCard['Template']['AttitudeLevel']['LevelTable'][1] = "The student needs to be closely supervised to meet the very basic level of expectation.";
$eReportCard['Template']['AttitudeLevel']['MockLevelTable'][6] = "The student meets both the pastoral and academic demands with the highest quality in terms of effort and enthusiasm all the time.";
$eReportCard['Template']['AttitudeLevel']['MockLevelTable'][5] = "The student meets the pastoral but not necessarily academic demands with very high quality most of the time. Although not quite the standard to merit excellence, the student has shown great eagerness to learn.";
$eReportCard['Template']['AttitudeLevel']['MockLevelTable'][4] = "The student has enthusiasm and a desire to meet the pastoral and academic demands of the school.";
$eReportCard['Template']['AttitudeLevel']['MockLevelTable'][3] = "The student meets the basic level of expectation, has some enthusiasm, contributes reasonably to group work and gets on well with others.";
$eReportCard['Template']['AttitudeLevel']['MockLevelTable'][2] = "The student shows inconsistent achievement of basic level of expectation. More effort is needed to become satisfactory.";
//$eReportCard['Template']['AttitudeLevel']['MockLevelTable'][1] = "The student needs to be closely supervised to meet the very basic level of expectation, may have anti-social behaviour and his/her conduct in class and around the school often causes trouble.";
$eReportCard['Template']['AttitudeLevel']['MockLevelTable'][1] = "The student needs to be closely supervised to meet the very basic level of expectation.";

$eReportCard['DisplayCommentSection'] = "Show Comments Section";
//$eReportCard['DisplayAwardSection'] = "Show Awards and Punishments Section";
$eReportCard['DisplayAwardSection'] = "Show Awards and Pastoral Records Section";
$eReportCard['DisplayActivitySection'] = "Show Activities and Positions of Responsibility Section";
$eReportCard['MaxActivityCount'] = "Maximum items to be shown in 1st page";
//$eReportCard['MaxActivityCountRemark'] = "* whole Awards and Punishments Section will be displayed in 2nd page if number of items exceeds the limit.";
$eReportCard['MaxActivityCountRemark'] = "* whole Awards and Pastoral Records Section will be displayed in 2nd page if number of items exceeds the limit.";
$eReportCard['MaxActivityCountRemark2'] = "* all items will be disiplayed in 1st page if the limit is set to 0.";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

?>