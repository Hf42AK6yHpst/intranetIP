<?php
# Editing by 

### General language file ###

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "--";

$eReportCard['SchemesFullMark'] = "滿分";
$eReportCard['ExtraInfoLabel'] = "補考";


# Report Header
$eReportCard['Template']['SchoolNameEn'] = "Colégio Diocesano de São José (5ª)";
$eReportCard['Template']['SchoolNameCh'] = "聖 若 瑟 教 區 中 學 第 五 校";
$eReportCard['Template']['AcademicYearEn'] = "Academic Year";
$eReportCard['Template']['AcademicYearCh'] = "學 年";


# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";

$eReportCard['Template']['StudentInfo']['StudentNameEn'] = "Student Name";
$eReportCard['Template']['StudentInfo']['StudentNameCh'] = "學生姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班　　別";
$eReportCard['Template']['StudentInfo']['DateOfBirthEn'] = "Date of Birth";
$eReportCard['Template']['StudentInfo']['DateOfBirthCh'] = "出生日期";
$eReportCard['Template']['StudentInfo']['IDNumberEn'] = "ID Number";
$eReportCard['Template']['StudentInfo']['IDNumberCh'] = "證 件 編 號";
$eReportCard['Template']['StudentInfo']['DSEJNumberEn'] = "DSEJ Number";
$eReportCard['Template']['StudentInfo']['DSEJNumberCh'] = "學生證編號";
$eReportCard['Template']['StudentInfo']['TotalEnrolledEn'] = "Total Enrolled";
$eReportCard['Template']['StudentInfo']['TotalEnrolledCh'] = "全班人數";
//$eReportCard['Template']['StudentInfo']['ArtsTotalEnrolledEn'] = "Enrolled (Art&Com)";
//$eReportCard['Template']['StudentInfo']['ArtsTotalEnrolledCh'] = "文組人數";
$eReportCard['Template']['StudentInfo']['ArtsTotalEnrolledEn'] = "Art & Com. Enrolled";
$eReportCard['Template']['StudentInfo']['ArtsTotalEnrolledCh'] = "文商組人數";
//$eReportCard['Template']['StudentInfo']['SciTotalEnrolledEn'] = "Enrolled (Science)";
$eReportCard['Template']['StudentInfo']['SciTotalEnrolledEn'] = "Science Enrolled";
$eReportCard['Template']['StudentInfo']['SciTotalEnrolledCh'] = "理組人數";
//$eReportCard['Template']['StudentInfo']['RankingEn'] = "Ranking";
$eReportCard['Template']['StudentInfo']['RankingEn'] = "Class Ranking";
$eReportCard['Template']['StudentInfo']['RankingCh'] = "考列名次";
//$eReportCard['Template']['StudentInfo']['ArtsRankingEn'] = "Ranking (Art&Com)";
//$eReportCard['Template']['StudentInfo']['ArtsRankingCh'] = "文組名次";
$eReportCard['Template']['StudentInfo']['ArtsRankingEn'] = "Art & Com. Ranking";
$eReportCard['Template']['StudentInfo']['ArtsRankingCh'] = "文商組名次";
//$eReportCard['Template']['StudentInfo']['SciRankingEn'] = "Ranking (Science)";
$eReportCard['Template']['StudentInfo']['SciRankingEn'] = "Science Ranking";
$eReportCard['Template']['StudentInfo']['SciRankingCh'] = "理組名次";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "發出日期";


# Marks Table
$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科　目";
$eReportCard['Template']['UnitsEn'] = "Unit(s)";
$eReportCard['Template']['UnitsCh'] = "單位";
// $eReportCard['Template']['Term1En'] = "1st Term";
$eReportCard['Template']['Term1Ch'] = "第一學段";
// $eReportCard['Template']['Term2En'] = "2nd Term";
$eReportCard['Template']['Term2Ch'] = "第二學段";
$eReportCard['Template']['Term1En'] = "1st Term";
//$eReportCard['Template']['Term1Ch'] = "上學期";
$eReportCard['Template']['Term2En'] = "2nd Term";
//$eReportCard['Template']['Term2Ch'] = "下學期";
$eReportCard['Template']['Term3En'] = "3rd Term";
$eReportCard['Template']['Term3Ch'] = "第三學段";
$eReportCard['Template']['Term4En'] = "4th Term";
$eReportCard['Template']['Term4Ch'] = "第四學段";
$eReportCard['Template']['Term1n2En'] = "1st, 2nd Term";
$eReportCard['Template']['Term1n2Ch'] = "第一、二學段";
$eReportCard['Template']['Term3n4En'] = "3rd , 4th Term";
$eReportCard['Template']['Term3n4Ch'] = "第三、四學段";
$eReportCard['Template']['CAEn'] = "C.A.";
$eReportCard['Template']['CACh'] = "平";
$eReportCard['Template']['ExamEn'] = "Exam";
$eReportCard['Template']['ExamCh'] = "考";
$eReportCard['Template']['AverageEn'] = "Average";
$eReportCard['Template']['AverageCh'] = "學年平均分";
$eReportCard['Template']['Average2En'] = "Average";
$eReportCard['Template']['Average2Ch'] = "學年平均";
$eReportCard['Template']['RankingEn'] = "Ranking";
//$eReportCard['Template']['RankingCh'] = "科目排名";
$eReportCard['Template']['RankingCh'] = "科目級排名";

$eReportCard['Template']['TotalAverageEn'] = "Total Average";
$eReportCard['Template']['TotalAverageCh'] = "總平均分";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操　　行";


# Misc Info
$eReportCard['Template']['CommentEn'] = "Comments";
$eReportCard['Template']['CommentCh'] = "評　語";
$eReportCard['Template']['ExtraCurricularActivitiesEn'] = "Extra-Curricular<br/>Activities";
$eReportCard['Template']['ExtraCurricularActivitiesCh'] = "餘 暇 活 動";
$eReportCard['Template']['SchoolTeamClubEn'] = "School Team / Club";
$eReportCard['Template']['SchoolTeamClubCh'] = "校隊 / 學會";
$eReportCard['Template']['AwardsEn'] = "Award(s)";
$eReportCard['Template']['AwardsCh'] = "獎項";
$eReportCard['Template']['AcademicAwardNumDisplay'] = "學術獎勵共<!--award_count-->項。";
//$eReportCard['Template']['AcademicAwardNumENDisplay'] = "Academic reward(s): <!--award_count--> in total.";
$eReportCard['Template']['AcademicAwardNumENDisplay'] = "Academic Award(s): <!--award_count--> in Total.";
$eReportCard['Template']['ServiceAwardNumDisplay'] = "服務及非學術獎勵共<!--award_count-->項。";
//$eReportCard['Template']['ServiceAwardNumENDisplay'] = "Service and Non-Academic reward(s): <!--award_count--> in total.";
$eReportCard['Template']['ServiceAwardNumENDisplay'] = "Service and Non-Academic Award(s): <!--award_count--> in Total.";

$eReportCard['Template']['AttendanceEn'] = "Attendance";
$eReportCard['Template']['AttendanceCh'] = "出 勤";
$eReportCard['Template']['LatenessEn'] = "Lateness (Times)";
$eReportCard['Template']['LatenessCh'] = "遲  到  (次)";
//$eReportCard['Template']['UnexcusedAbsentEn'] = "Unexcused Absent (Periods)";
$eReportCard['Template']['UnexcusedAbsentEn'] = "Unexcused Absences (Periods)";
$eReportCard['Template']['UnexcusedAbsentCh'] = "曠  課  (節)";
$eReportCard['Template']['PersonalLeaveEn'] = "Personal Leave (Periods)";
$eReportCard['Template']['PersonalLeaveCh'] = "事  假  (節)";
$eReportCard['Template']['SickLeaveEn'] = "Sick Leave (Periods)";
$eReportCard['Template']['SickLeaveCh'] = "病  假  (節)";
//$eReportCard['Template']['RewardsEn'] = "Rewards";
$eReportCard['Template']['RewardsEn'] = "Awards";
$eReportCard['Template']['RewardsCh'] = "奬 勵";
// $eReportCard['Template']['AcademicRewardsEn'] = "Academic Rewards";
// $eReportCard['Template']['AcademicRewardsCh'] = "學 術 奬 勵";
// $eReportCard['Template']['ServiceRewardsEn'] = "Service Rewards";
// $eReportCard['Template']['ServiceRewardsCh'] = "服 務 奬 勵";
// $eReportCard['Template']['ConductRewardsEn'] = "Conduct Rewards";
// $eReportCard['Template']['ConductRewardsCh'] = "品 行 奬 勵";
$eReportCard['Template']['AcademicRewardsEn'] = "Academic";
$eReportCard['Template']['AcademicRewardsCh'] = "學 術";
$eReportCard['Template']['ServiceRewardsEn'] = "Service and Non-Academic";
$eReportCard['Template']['ServiceRewardsCh'] = "非學術及服務";
$eReportCard['Template']['ConductRewardsEn'] = "Conduct";
$eReportCard['Template']['ConductRewardsCh'] = "品 行";
$eReportCard['Template']['PenaltiesEn'] = "Penalties";
$eReportCard['Template']['PenaltiesCh'] = "懲 罰";
// $eReportCard['Template']['ConductPenaltiesEn'] = "Conduct Penalties";
// $eReportCard['Template']['ConductPenaltiesCh'] = "品 行 懲 罰";
$eReportCard['Template']['ConductPenaltiesEn'] = "Conduct";
$eReportCard['Template']['ConductPenaltiesCh'] = "品 行";
$eReportCard['Template']['MeritEn'] = "Merit";
$eReportCard['Template']['MeritCh'] = "優點";
$eReportCard['Template']['MinAchEn'] = "Min. Ach.";
$eReportCard['Template']['MinAchCh'] = "小功";
$eReportCard['Template']['MajAchEn'] = "Maj. Ach.";
$eReportCard['Template']['MajAchCh'] = "大功";
$eReportCard['Template']['DemeritEn'] = "Demerit";
$eReportCard['Template']['DemeritCh'] = "缺點";
$eReportCard['Template']['MinFitEn'] = "Min. Flt.";
$eReportCard['Template']['MinFitCh'] = "小過";
$eReportCard['Template']['MajFitEn'] = "Maj. Flt.";
$eReportCard['Template']['MajFitCh'] = "大過";

$eReportCard['Template']['RemarksEn'] = "Remark(s)";
$eReportCard['Template']['RemarksCh'] = "備 註";
$eReportCard['Template']['SupplementarySubjectsEn'] = "Supplementary Exam Subject(s)";
$eReportCard['Template']['SupplementarySubjectsCh'] = "補考科目";

// $eReportCard['Template']['Conduct'] = "Conduct";
// $eReportCard['Template']['IncompleteHomeworkEn'] = "Incomplete homework";
// $eReportCard['Template']['IncompleteHomeworkCh'] = "欠功課次數";
// $eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
// $eReportCard['Template']['Merits'] = "Merits 優點";
// $eReportCard['Template']['Demerits'] = "Demerits 缺點";
// $eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
// $eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
// $eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
// $eReportCard['Template']['MajorFault'] = "Major Fault 大過";
// $eReportCard['Template']['Remark'] = "Remark 備註";
// $eReportCard['Template']['eca'] = "ECA 課外活動";
// $eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

// $eReportCard['Template']['ExtraActivityEn'] = "Extracurricular Activities";
// $eReportCard['Template']['ExtraActivityCh'] = "課外活動";
// $eReportCard['Template']['ServiceEn'] = "Services";
// $eReportCard['Template']['ServiceCh'] = "服務";
// $eReportCard['Template']['AwardPunishEn'] = "Awards and Punishment";
// $eReportCard['Template']['AwardPunishCh'] = "獎懲";
// $eReportCard['Template']['FinalCommentEn'] = "Final Comments";
// $eReportCard['Template']['FinalCommentCh'] = "學年結語";

// $eReportCard['Template']['NextYearClassCh'] = "下學年就讀";

// $eReportCard['Template']['RemarkCh'] = "備註";
// $eReportCard['Template']['RemarkArr']['A'] = "A—表現優異；";
// $eReportCard['Template']['RemarkArr']['B'] = "B—表現良好；";
// $eReportCard['Template']['RemarkArr']['C'] = "C—仍需努力。";

// $eReportCard['Template']['DailyLifeReportCh'] = "生活報告";

# Signature Table
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校  長";
$eReportCard['Template']['SchoolStampEn'] = "School Stamp";
$eReportCard['Template']['SchoolStampCh'] = "校  印";
$eReportCard['Template']['SectionDeanEn'] = "Section Dean";
$eReportCard['Template']['SectionDeanCh'] = "學 部 主 任";
$eReportCard['Template']['HomeroomTeacherEn'] = "Homeroom Teacher";
$eReportCard['Template']['HomeroomTeacherCh'] = "班 主 任";
$eReportCard['Template']['ParentSignatureEn'] = "Parent's Signature";
$eReportCard['Template']['ParentSignatureCh'] = "家 長 簽 名";


# Master Report
// $eReportCard['MasterReport']['DataShortName']['ClassNumber'] = "班號";
// $eReportCard['MasterReport']['DataShortName']['ChineseName'] = "中文名";
// $eReportCard['MasterReport']['DataShortName']['EnglishName'] = "英文名";
// $eReportCard['MasterReport']['DataShortName']['Gender'] = "性別";
// $eReportCard['MasterReport']['DataShortName']['Mark'] = "分數";
// $eReportCard['MasterReport']['DataShortName']['GrandTotal'] = "總分";
// $eReportCard['MasterReport']['DataShortName']['GrandAverage'] = "平均分";
// $eReportCard['MasterReport']['DataShortName']['OrderMeritForm'] = "全級名次";
// $eReportCard['MasterReport']['DataShortName']['OrderMeritClass'] = "全班名次";
// $eReportCard['MasterReport']['DataShortName']['FailSubjectUnit'] = "單位";
// $eReportCard['MasterReport']['OtherInfo']['Conduct'] = "Conduct";
// $eReportCard['MasterReport']['OtherInfo']['TimesLate'] = "遲到次數";
// $eReportCard['MasterReport']['OtherInfo']['ExcusedAbsencePeriods'] = "缺席節數";
// $eReportCard['MasterReport']['OtherInfo']['UnexcusedAbsencePeriods'] = "曠課節數";
// $eReportCard['MasterReport']['OtherInfo']['FinalComments'] = "升留級";


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

# Signature
// $eReportCard['Template']['ClassTeacherSignStr'] ="班主任簽署";
// $eReportCard['Template']['RemarkStr'] ="備註 :<br/>1. 只供校內查閱，不作文件影印及證明。<br/>2. 須與該生之入學註冊表一併歸入檔案。<br/>3. 須經負責部門審查及蓋章。<br/>4. 背面可填寫該生於該年的備忘事項，並註明日期及簽署。";

# Final Comment
// $eReportCard['Template']['FinalCommentArr']['GrandAverageFailed'] = "學年平均成績不及格, 次學年仍留<!--FormName-->年級.";
// $eReportCard['Template']['FinalCommentArr']['ExceededSubjectFailUnit'] = "不及格科目共<!--FailedSubjectWeight-->單位, 次學年仍留<!--FormName-->年級.";
// $eReportCard['Template']['FinalCommentArr']['ReExamSubject'] = "應補考<!--SubjectNameList-->";
// $eReportCard['Template']['FinalCommentArr']['AttendSummerClass'] = "應入夏令班補修";
// $eReportCard['Template']['FinalCommentArr']['ReExamFailedAndRetain'] = "補考不及格, 次學年仍留<!--FormName-->年級";
// $eReportCard['Template']['FinalCommentArr']['ReExamPassedAndGraduate'] = "各科補考及格, 准予畢業";
// $eReportCard['Template']['FinalCommentArr']['PromoteToForm'] = "次學年准升<!--FormName-->年級";
// $eReportCard['Template']['FinalCommentArr']['CanBeGraduate'] = "准予畢業";

# Student Promotion Status
//$eReportCard["PromotionStatus"][1] = "升級";
//$eReportCard["PromotionStatus"][2] = "補考合格";
//$eReportCard["PromotionStatus"][3] = "補考不合格";
$eReportCard["PromotionStatus"][1] = "不需補考";
$eReportCard["PromotionStatus"][2] = "全部科目通過";
$eReportCard["PromotionStatus"][3] = "全部科目不通過";
$eReportCard["ManualInputReExamStatus"][1] = "不需補考";
$eReportCard["ManualInputReExamStatus"][2] = "全部科目通過";
$eReportCard["ManualInputReExamStatus"][3] = "全部科目不通過";

?>