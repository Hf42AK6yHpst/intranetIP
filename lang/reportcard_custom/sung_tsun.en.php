<?php
# Editing by 

### General language file ###
$eReportCard['SchemesFullMark'] = "Full Mark";
$eReportCard['NIL'] = "NIL";
$eReportCard['DeleteLine'] = "-----------<br>-----------<br>-----------";
$eReportCard['GenderArr']['M'] = "Male";
$eReportCard['GenderArr']['F'] = "Female";

$eReportCard['Template']['ExtraReportTitleEn'] = "Continuing Assessment Student Report";
$eReportCard['Template']['ExtraReportTitleCh'] = "學生持續評估報告";

## School Info 
$eReportCard['Template']['SchoolInfo']['SchoolNameCh'] = "西貢崇真天主教學校(中學部)";
$eReportCard['Template']['SchoolInfo']['SchoolNameEn'] = "SAI KUNG SUNG TSUN CATHOLIC SCHOOL (SECONDARY SECTION)";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "NAME";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "CLASS";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班別";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['HKIDEn'] = "I.D.NO";
$eReportCard['Template']['StudentInfo']['HKIDCh'] = "身分證號碼";
$eReportCard['Template']['StudentInfo']['GenderEn'] = "SEX";
$eReportCard['Template']['StudentInfo']['GenderCh'] = "性別";
$eReportCard['Template']['StudentInfo']['DateOfBirthEn'] = "DATE OF BIRTH";
$eReportCard['Template']['StudentInfo']['DateOfBirthCh'] = "出生日期";
$eReportCard['Template']['StudentInfo']['ClassTeacherEn'] = "CLASS TEACHER";
$eReportCard['Template']['StudentInfo']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "DATE OF ISSUE";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "發出日期";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['SubjectEn'] = "SUBJECT";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";

$eReportCard['Template']['AcademicPerformance'] = "ACADEMIC PERFORMANCE 學習表現";
$eReportCard['Template']['LearningAttitude'] = "學習態度<br>LEARNING<br>ATTITUDE";
$eReportCard['Template']['CourseWork'] = "功課表現<br>COURSE<br>WORK";
$eReportCard['Template']['BasicCompetence'] = "學科基礎能力<br>BASIC<br>COMPETENCE";
$eReportCard['Template']['UniformTestResult'] = "統測成績 (%)<br>UNIFORM TEST RESULT";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
//2011-0704-1044-43067 - 西貢崇真天主教中學 - about the NA displayed in the no mark subjects
//$eReportCard['RemarkNotAssessed'] = "N.A.";
$eReportCard['RemarkNotAssessed'] = "--";
// [2020-0609-1211-53054]
$eReportCard['MarkRemindSet1'] = "Remark: \"0\" stands for absent (zero mark); \"N.A.\" for dropped; \"N.A.\" for not assessed.";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

$eReportCard['Template']['GuardianSignatureCh'] = "監護人簽署";
$eReportCard['Template']['GuardianSignatureEn'] = "Guardian Signature";

$eReportCard['ExtraInfoLabel'] = "Adjusted Grade";
$eReportCard['AdjustPositionLabal'] = "Uniform Test Marksheet";
$eReportCard['ManualAdjustMarkInstruction'] = "";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

# Remarks
$eReportCard['Template']['1st'] = "1.";
$eReportCard['Template']['2nd'] = "2.";
$eReportCard['Template']['Remarks'] = "Remarks 備註 :";
$eReportCard['Template']['Grading'] = "ACADEMIC PERFORMANCE GRADING 評估學習表現的等級";
$eReportCard['Template']['Excellent'] = "A = Excellent 優;"; 
$eReportCard['Template']['Good'] = "B,C = Good 良;";
$eReportCard['Template']['Fair'] = "D = Fair 常;";
$eReportCard['Template']['Pass'] = "E = Pass 及格;";
$eReportCard['Template']['Fail'] = "F = Fail 不及格;";
$eReportCard['Template']['NA'] = "N.A. = Not Applicable 不適用";
$eReportCard['Template']['NoSignRequire'] = "This is a computer-generated document. NO School signature is required.<br>此乃電腦編印文件，學校不作簽署安排。";

?>