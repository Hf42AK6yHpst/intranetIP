<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

## School Info
$eReportCard['Template']['SchoolInfo']['NameEn'] = "Society of Boys' Centres Chak Yan Centre School (Primary Section)";
$eReportCard['Template']['SchoolInfo']['NameCh'] = "香港扶幼會則仁中心學校(小學部)";
$eReportCard['Template']['SchoolInfo']['Address'] = "九龍深水埗歌和老街47號";
$eReportCard['Template']['SchoolInfo']['Tel'] = "電話：27783981";
$eReportCard['Template']['SchoolInfo']['Fax'] = "傳真：27761587";
$eReportCard['Template']['SchoolInfo']['ReportCard'] = "成績報告表";

# Student Info Table
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name: ";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名: ";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班別: ";
$eReportCard['Template']['StudentInfo']['ClassNumCh'] = "學號: ";
$eReportCard['Template']['StudentInfo']['DateOfEnter'] = "入學日期: ";
$eReportCard['Template']['StudentInfo']['DateOfPublish'] = "派發日期: ";


# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>軞傖憎";
$eReportCard['Template']['GrandTotal'] = "軞煦";

$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['OverallAvgMarkCh'] = "全學年總平均分";
$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['SchemesFullMark'] = "總分";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "NA";

$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['Result'] = "成績";

# CSV Info

#Attendance
$eReportCard['Template']['Attendance'] = "考勤記錄";
$eReportCard['Template']['ShouldAttend'] = "應上課日數";
$eReportCard['Template']['AttendancePercent'] = "出席率";
$eReportCard['Template']['DaysAbsenteeCh'] = "曠課日數";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";

#Behaviour
$eReportCard['Template']['Behaviour'] = "行為表現";
$eReportCard['Template']['Appearance'] = "儀容";
$eReportCard['Template']['Mood'] = "情緒";     
$eReportCard['Template']['Courtesy'] = "禮貌";
$eReportCard['Template']['Gregarious'] = "合群";
$eReportCard['Template']['Discipline'] = "紀律";
$eReportCard['Template']['Honesty'] = "誠實";
$eReportCard['Template']['Service'] = "服務";
$eReportCard['Template']['AppearanceOverall'] = "整體表現";
$eReportCard['Template']['AppearanceOverallYear'] = "全學年整體表現";

#OtherInformation
$eReportCard['Template']['OtherInformation'] = "其他資料";
$eReportCard['Template']['PELesson'] = "體藝課: ";
$eReportCard['Template']['ServiceItem'] = "服務項目: ";
$eReportCard['Template']['Award'] = "獎項/成就: ";
$eReportCard['Template']['Remarks'] = "備註: ";

#Commnet
$eReportCard['Template']['Comment'] = "評語";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "班主任";
$eReportCard['Template']['Principal'] = "校長";
$eReportCard['Template']['ParentGuardian'] = "家長/監護人";


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

?>