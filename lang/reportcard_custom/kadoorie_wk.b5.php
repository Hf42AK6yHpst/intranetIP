<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

# School Info Table
$eReportCard['Template']['SchoolInfo']['SchoolNameEn'] = "Sir Ellis Kadoorie Secondary School (West Kowloon)";
$eReportCard['Template']['SchoolInfo']['SchoolNameCh'] = "官立嘉道理爵士中學﹙西九龍﹚";
$eReportCard['Template']['SchoolInfo']['AddressCh'] = "西九龍大角咀海帆道22號";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";
$eReportCard['Template']['StudentInfo']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['StudentInfo']['ClassPositionCh'] = "全班名次";
$eReportCard['Template']['StudentInfo']['FormPositionEn'] = "Position in Level";
$eReportCard['Template']['StudentInfo']['FormPositionCh'] = "全級名次";
$eReportCard['Template']['StudentInfo']['DateOfBirthEn'] = "Date of Birth";
$eReportCard['Template']['StudentInfo']['DateOfBirthCh'] = "出生日期";
$eReportCard['Template']['StudentInfo']['GenderEn'] = "Sex";
$eReportCard['Template']['StudentInfo']['GenderCh'] = "性別";

# Marks Table
$eReportCard['Template']['SubjectEn'] = "SUBJECTS";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarkEn'] = "MAXIMUM MARK";
$eReportCard['Template']['MaximumMarkCh'] = "最高分數";
$eReportCard['Template']['ExaminationMarkEn'] = "EXAMINATION MARK";
$eReportCard['Template']['ExaminationMarkCh'] = "考試得分";
$eReportCard['Template']['CourseworkMarkEn'] = "COURSEWORK MARK";
$eReportCard['Template']['CourseworkMarkCh'] = "課堂學習表現";
$eReportCard['Template']['TermMarkEn'] = "TERM MARK";
$eReportCard['Template']['TermMarkCh'] = "本學期得分";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['Template']['TermTotalEn'] = "TERM TOTAL";
$eReportCard['Template']['TermTotalCh'] = "本學期總分";
$eReportCard['Template']['GrandAverageEn'] = "Grand Average";
$eReportCard['Template']['GrandAverageCh'] = "總平均分";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "Abs";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

$eReportCard['Template']['ECAPerformanceEn'] = "ECA Performance";
$eReportCard['Template']['ECAPerformanceCh'] = "課外活動表現"; 

$eReportCard['Template']['MajorMeritEn'] = "Major Merit";
$eReportCard['Template']['MajorMeritCh'] = "大功"; 
$eReportCard['Template']['MeritEn'] = "Merit";
$eReportCard['Template']['MeritCh'] = "小功"; 
$eReportCard['Template']['CreditEn'] = "Credit";
$eReportCard['Template']['CreditCh'] = "優點"; 
$eReportCard['Template']['MajorDemeritEn'] = "Major Demerit";
$eReportCard['Template']['MajorDemeritCh'] = "大過"; 
$eReportCard['Template']['DemeritEn'] = "Demerit";
$eReportCard['Template']['DemeritCh'] = "小過"; 
$eReportCard['Template']['BlackMarkEn'] = "Black Mark";
$eReportCard['Template']['BlackMarkCh'] = "缺點"; 

$eReportCard['Template']['1stTermTotalEn'] = "1st Term Total";
$eReportCard['Template']['1stTermTotalCh'] = "上學期總分";
$eReportCard['Template']['2ndTermTotalEn'] = "2nd Term Total";
$eReportCard['Template']['2ndTermTotalCh'] = "下學期總分";
$eReportCard['Template']['AnnualTotalEn'] = "Annual Total";
$eReportCard['Template']['AnnualTotalCh'] = "全年總分";
$eReportCard['Template']['AnnualAverageEn'] = "Annual Average(%)";
$eReportCard['Template']['AnnualAverageCh'] = "全年總平均分(%)";
$eReportCard['Template']['HighestAverageInClassEn'] = "Highest Average in Class";
$eReportCard['Template']['HighestAverageInClassCh'] = "全班最高平均分";

$eReportCard['Template']['ClassTeacherCommentEn'] = "Class Teacher's Comment";
$eReportCard['Template']['ClassTeacherCommentCh'] = "班主任評語";

$eReportCard['Template']['RemarkEn'] = "Remark";
$eReportCard['Template']['RemarkCh'] = ""; 


# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER 班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL 校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT / GUARDIAN<br>家長 / 監護人";
$eReportCard['Template']['IssueDate'] = "DATE<br>日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

$eReportCard['Template']['PrincipalName'] = "Mr. FUNG KAM CHEUNG";

$eReportCard['Template']['Mr'] = "Mr.";
$eReportCard['Template']['Miss'] = "Miss";
$eReportCard['Template']['Mrs'] = "Mrs.";
$eReportCard['Template']['Ms'] = "Ms.";
$eReportCard['Template']['Dr'] = "Dr.";
$eReportCard['Template']['Prof'] = "Prof.";


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");


$eReportCard['Template']['AddressDisplay'][0] = "西九龍大角咀海帆道22號";
$eReportCard['Template']['AddressDisplay'][1] = "22 Hoi Fan Road,";
$eReportCard['Template']['AddressDisplay'][2] = "Tai Kok Tsui, West Kowloon,";
$eReportCard['Template']['AddressDisplay'][3] = "Hong Kong";
$eReportCard['Template']['AddressDisplay'][4] = "Tel No.: 25761871";
$eReportCard['Template']['AddressDisplay'][5] = "Fax No.: 28824548";
$eReportCard['Template']['AddressDisplay'][6] = "E-mail : sekss100@edb.gov.hk";
$eReportCard['Template']['AddressDisplay'][7] = "Website: http://seksswk.edu.hk";

?>