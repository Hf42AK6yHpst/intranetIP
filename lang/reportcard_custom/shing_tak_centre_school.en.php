<?php
# Editing by 

### General language file ###
$eReportCard['SchemesFullMark'] = "Full Mark";


### Header Table
$eReportCard['Template']['SchoolNameEn'] = "SBC Shing Tak Centre School";
$eReportCard['Template']['SchoolNameCh'] = "香港扶幼會盛德中心學校";
$eReportCard['Template']['AcademicReportEn'] = "Academic Report";
$eReportCard['Template']['AcademicReportCh'] = "學業成績表";
$eReportCard['Template']['MaleEn'] = "M";
$eReportCard['Template']['MaleCh'] = "男";
$eReportCard['Template']['FemaleEn'] = "F";
$eReportCard['Template']['FemaleCh'] = "女";

### Student Info Table
$eReportCard['Template']['StudentNameEn'] = "Name";
$eReportCard['Template']['StudentNameCh'] = "學生姓名";
$eReportCard['Template']['STRNEn'] = "STRN";
$eReportCard['Template']['STRNCh'] = "學生編號";
$eReportCard['Template']['SexEn'] = "Sex";
$eReportCard['Template']['SexCh'] = "性　　別";
$eReportCard['Template']['ClassNameEn'] = "Class Name";
$eReportCard['Template']['ClassNameCh'] = "班　　別";
$eReportCard['Template']['DateOfBirthEn'] = "Date of Birth";
$eReportCard['Template']['DateOfBirthCh'] = "出生日期";
$eReportCard['Template']['ClassNoEn'] = "Class No.";
$eReportCard['Template']['ClassNoCh'] = "班　　號";
$eReportCard['Template']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['DateOfIssueCh'] = "派發日期";

### Mark Table
$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['FullMarkEn'] = "Full Mark";
$eReportCard['Template']['FullMarkCh'] = "滿分";
$eReportCard['Template']['ResultsEn'] = "Results";
$eReportCard['Template']['ResultsCh'] = "成績";
$eReportCard['Template']['AverageEn'] = "Average";
$eReportCard['Template']['AverageCh'] = "平均分";
$eReportCard['Template']['PositionInClassEn'] = "Position in Class";
$eReportCard['Template']['PositionInClassCh'] = "班名次";
$eReportCard['Template']['PositionInFormEn'] = "Position in Form";
$eReportCard['Template']['PositionInFormCh'] = "級名次";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到日數";
$eReportCard['Template']['DaysLeaveEn'] = "Days Leave";
$eReportCard['Template']['DaysLeaveCh'] = "請假日數";
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "曠課日數";
$eReportCard['Template']['ElectiveEn'] = "Elective";
$eReportCard['Template']['ElectiveCh'] = "選修科";

### Misc Table
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['TeachersCommentEn'] = "Teachers' Comments";
$eReportCard['Template']['TeachersCommentCh'] = "老師評語";
$eReportCard['Template']['AcademicPerformanceEn'] = "Academic Performance";
$eReportCard['Template']['AcademicPerformanceCh'] = "學術表現";
$eReportCard['Template']['PersonalDevelopmentEn'] = "Personal Development";
$eReportCard['Template']['PersonalDevelopmentCh'] = "社交表現";
$eReportCard['Template']['OtherInformationEn'] = "Other Information";
$eReportCard['Template']['OtherInformationCh'] = "其他資料";
$eReportCard['Template']['PromoteToEn'] = "Promote to";
$eReportCard['Template']['PromoteToCh'] = "升班";
$eReportCard['Template']['RepeatEn'] = "Repeat";
$eReportCard['Template']['RepeatCh'] = "留班";

### Signature Table
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校長";
$eReportCard['Template']['ClassTeacherEn'] = "Class Teacher";
$eReportCard['Template']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['ParentEn'] = "Parent";
$eReportCard['Template']['ParentCh'] = "家長";
$eReportCard['Template']['GuardianEn'] = "Guardian";
$eReportCard['Template']['GuardianCh'] = "監護人";

$eReportCard['Template']['FailMarkRemark'] = "註：括號內之成績為不合格分數";
?>