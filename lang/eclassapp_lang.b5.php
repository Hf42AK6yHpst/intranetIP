<?php
//using:  
//A
$Lang['eClassApp']['AccountPageBanner'] = '帳戶界面背景圖片';
$Lang['eClassApp']['AcceptPushMessage'] = '已註冊推送訊息';
$Lang['eClassApp']['AddMember'] = '加入組員';
$Lang['eClassApp']['AddNoOfCode'] = '增加授權碼數目';
$Lang['eClassApp']['AllEnableStatus'] = '所有啟用狀況';
$Lang['eClassApp']['AllParent'] = '所有家長';
$Lang['eClassApp']['AllRegistrationStatus'] = '所有註冊狀況';
$Lang['eClassApp']['AllStatus'] = '所有狀態';
$Lang['eClassApp']['AuthorizationCode'] = '授權碼';
$Lang['eClassApp']['AdvancedSetting'] = '進階設定';
$Lang['eClassApp']['AdvancedSettingList']['PushMessageRight'] = '推送訊息權限';
$Lang['eClassApp']['AllowClassTeacherSendMessage'] = '允許班主任發送推送訊息';
$Lang['eClassApp']['AllowClubAndActivityPICSendMessage'] = '允許學會管理員發送推送訊息';
$Lang['eClassApp']['AllowDeleteOwnPushMessage'] = '允許非管理員刪除自己發送的訊息紀錄';
$Lang['eClassApp']['AllowSendPushMessageToClassStudent'] = '允許發送推送訊息給自己班的學生家長';
$Lang['eClassApp']['AllowSendPushMessageToAllStudent'] = '允許發送推送訊息給所有學生家長';
$Lang['eClassApp']['AllowSendPushMessageToClassStudentApp'] = '允許發送推送訊息給自己班的學生';
$Lang['eClassApp']['AllowSendPushMessageToAllStudentApp'] = '允許發送推送訊息給所有學生';
$Lang['eClassApp']['AllowViewAllStudentList'] = '允許檢視所有學生列表';
$Lang['eClassApp']['AllowViewStudentContact'] = '允許檢視所有學生聯絡方法';
$Lang['eClassApp']['AllowViewStudentContactClass'] = '允許檢視自己班的學生聯絡方法';
$Lang['eClassApp']['AllowTeacherReceivePushMessageWhileLoggedOut'] = '允許老師在登出後繼續接收推送訊息';
$Lang['eClassApp']['AppHomePageDisplay'] = 'App 首頁顯示';
$Lang['eClassApp']['AppEAttendanceDisplay'] = 'App 「考勤紀錄」顯示';

//B
$Lang['eClassApp']['BackgroundImage'] = '主頁背景圖片';
$Lang['eClassApp']['Blacklist'] = '黑名單';

//C
$Lang['eClassApp']['CannotUpdateSchoolBannerduringTrialPeroid'] = '試用期內不能更改背景圖片';
$Lang['eClassApp']['CommonFunction'] = '共同功能';
$Lang['eClassApp']['ClassTeacher'] = '班主任';
$Lang['eClassApp']['Content'] = '內容';
$Lang['eClassApp']['ComposeMessage'] = '撰寫訊息';

//D
$Lang['eClassApp']['DigitalChannels']['UntitledAlbum'] = '未命名相簿';
$Lang['eClassApp']['DigitalChannels']['PhotoTitle'] = '標題';
$Lang['eClassApp']['DigitalChannels']['AlbumTitle'] = '相簿';
$Lang['eClassApp']['DigitalChannels']['Category'] = '分類';
$Lang['eClassApp']['DigitalChannels']['UploadDate'] = '上載日期';
$Lang['eClassApp']['DigitalChannels']['PhotoViews'] = '觀看次數';
$Lang['eClassApp']['DigitalChannels']['PhotoFavorite'] = '喜愛';
$Lang['eClassApp']['DigitalChannels']['PhotoComment'] = '留言';
$Lang['eClassApp']['DigitalChannels']['Comment_no_empty'] = '請輸入留言。';
$Lang['eClassApp']['DigitalChannels']['EventTitle'] = '賽事名稱';
$Lang['eClassApp']['DigitalChannels']['EventDate'] = '賽事日期';

//E
$Lang['eClassApp']['EnableStatus'] = '啟用狀況';
$Lang['eClassApp']['EnableStudentApp'] = '啟用學生 app';
$Lang['eClassApp']['eNoticeS']['SignResults'] = '簽署情況';
$Lang['eClassApp']['eNoticeS']['All'] = '全部學生';
$Lang['eClassApp']['eNoticeS']['Signed'] = '已簽學生';
$Lang['eClassApp']['eNoticeS']['Unsigned'] = '未簽學生';
$Lang['eClassApp']['eNoticeS']['Parent-signedNotice'] = '家長簽署通告';
$Lang['eClassApp']['eNoticeS']['Student-signedNotice'] = '學生簽署通告';
$Lang['eClassApp']['eNoticeS']['BackToList'] = '返回學生列表';
$Lang['eClassApp']['eHomework']['DeleteSuccess'] = '紀錄已經刪除。';
$Lang['eClassApp']['eHomework']['NoDeleteAccRecord'] = '刪除紀錄失敗，沒有刪除數據權限。';
$Lang['eClassApp']['eHomework']['NeedToHand-in'] = '需繳交';
$Lang['eClassApp']['eHomework']['NoNeedToHand-in'] = '不需繳交';
$Lang['eClassApp']['eHomework']['NeedToCollectHomework'] = '需要收回有關功課';
$Lang['eClassApp']['eHomework']['NoNeedToCollectHomework'] = '不需要收回有關功課';
$Lang['eClassApp']['eHomework']['UpcomingExpiringHomeworks'] = '未到期的功課';
$Lang['eClassApp']['eHomework']['PastExpiringHomeworks'] = '已過期功課';
$Lang['eClassApp']['eHomework']['AllHomeworks'] = '全部功課';
$Lang['eClassApp']['eHomework']['All'] = '全部';
$Lang['eClassApp']['eHomework']['NotAllCollected'] = '未收齊';
$Lang['eClassApp']['eEnrollment']['NotTake'] = '未點名';
$Lang['eClassApp']['eEnrollment']['HasTaken'] = '已點名';
$Lang['eClassApp']['eEnrollment']['RecordDate'] = '日期';
$Lang['eClassApp']['eEnrollment']['RecordHasUpdated'] = '記錄已經更新。';
$Lang['eClassApp']['eEnrollment']['Name'] = '名稱';
$Lang['eClassApp']['eEnrollment']['Amount'] = '人數';
$Lang['eClassApp']['eEnrollment']['AreYouSureToSubmit'] = '你確認要提交嗎？';
$Lang['eClassApp']['eEnrollment']['MultiSelect'] = '多重選擇';
$Lang['eClassApp']['eEnrollment']['TurnTo'] = '轉為';
$Lang['eClassApp']['eEnrollment']['Confirm'] = '確認';

//F
$Lang['eClassApp']['FunctionAccessRight'] = '功能權限';

//G
$Lang['eClassApp']['GroupQuota'] = '小組數目限額';
$Lang['eClassApp']['Group'] = '小組';
$Lang['eClassApp']['GroupMessage']['CommunicationMode'] = '通訊模式';
$Lang['eClassApp']['GroupMessage']['CommunicationMode_Normal'] = '正常模式';
$Lang['eClassApp']['GroupMessage']['CommunicationMode_DisableParentToParent'] = '家長受限模式';
$Lang['eClassApp']['GroupMessage']['CannotChangeLater'] = '設定後不可再次修改';
$Lang['eClassApp']['GroupMessage']['ChangeCommunicateModeWarning1'] = '家長可閱讀同組內老師及其他家長的回應 / 訊息';
$Lang['eClassApp']['GroupMessage']['ChangeCommunicateModeWarning2'] = '家長只能閱讀老師的訊息，只有組內的老師能閱讀家長的回應 / 訊息';
$Lang['eClassApp']['GroupMessageBatchCreate'] = '每班建立一個小組';

//H
$Lang['eClassApp']['HKUFlu']['PushMessageTitle'] = '香港大學疾病監測計劃';
$Lang['eClassApp']['HKUFlu']['PushMessageContent'] = '多謝參與香港大學疾病監測計劃。
	因學校考勤系統顯示　貴子女%NAME%今天缺席，請按以下連結為　貴子女提供缺席的相關資料。
	
	缺席日期：%DATE%';


//I
$Lang['eClassApp']['ImageResolutionRemarks'] = "建議解像度: <!--width-->px * <!--height-->px (闊 * 高)";
$Lang['eClassApp']['ImageResolutionWarning'] = "過大的圖像會增加學校 eClass 伺服器的負荷。建議用戶上傳與建議解像度相若的圖像。";
$Lang['eClassApp']['Info'] = '資料';
$Lang['eClassApp']['InUse'] = '已使用';
$Lang['eClassApp']['iPortfolio']['AllForms'] = "所有級別";
$Lang['eClassApp']['iPortfolio']['ReadBySystem'] = "系統將自行讀取";
$Lang['eClassApp']['iPortfolio']['SelectOEATitlePairingMethod'] = "請選擇 OEA 標題的配對方式";
$Lang['eClassApp']['iPortfolio']['NewOLE'] = "新增預設活動";
$Lang['eClassApp']['iPortfolio']['AssignStudentToProgramme'] = "新增學生到項目";
$Lang['eClassApp']['iPortfolio']['ViewThisProgramme'] = "檢視此項目";
$Lang['eClassApp']['iPortfolio']['AddCommonRecordForStudent'] = "為學生編輯共同紀錄";
$Lang['eClassApp']['iPortfolio']['Selected'] = "已選擇";
$Lang['eClassApp']['iPortfolio']['SaveRecord'] = "儲存紀錄";
$Lang['eClassApp']['iPortfolio']['AreYouSureToSaveTheRecord'] = "你是否確定要儲存？";
$Lang['eClassApp']['iPortfolio']['Programme'] = "項　目";
$Lang['eClassApp']['iPortfolio']['Student'] = "學　生";
$Lang['eClassApp']['iPortfolio']['ActivityTotalHours'] = "活動總時數";
$Lang['eClassApp']['iPortfolio']['Items'] = "項";
$Lang['eClassApp']['iPortfolio']['TotalRecordsApproved'] = "紀綠總數 (已批核)";
$Lang['eClassApp']['iPortfolio']['TotalRecords'] = "紀綠總數";

//J
$Lang['eClassApp']['jsWarning']['deleteAccountPageBanner'] = '你是否確定要刪除帳戶界面背景圖片？';
$Lang['eClassApp']['jsWarning']['deleteBackgroundImage'] = "你是否確定要刪除主頁背景圖片？";
$Lang['eClassApp']['jsWarning']['deleteSchoolBadge'] = "你是否確定要刪除校徽？";
$Lang['eClassApp']['jsWarning']['reachedNumOfGroupLimit'] = "已達小組數目限額";
$Lang['eClassApp']['jsWarning']['reachedNumOfMemberLimit'] = "已達組員數目限額";
$Lang['eClassApp']['jsWarning']['selectAtLeastOneParent'] = "請選擇最少一位家長。";
$Lang['eClassApp']['jsWarning']['selectedMemberExceedMemberLimit'] = "已選的組員超出限額數目";
$Lang['eClassApp']['jsWarning']['schoolWebsiteURLFormatIncorrect'] = '錯誤的學校網址。';

//K


//L
$Lang['eClassApp']['LastLoginTime'] = '最後登入時間';
$Lang['eClassApp']['LoggedInUser'] = '已登入用戶';
$Lang['eClassApp']['LoginStatus'] = '登入狀況';

//M
$Lang['eClassApp']['MemberList'] = '組員名單';
$Lang['eClassApp']['MemberQuota'] = '組員數目限額';
$Lang['eClassApp']['Module'] = '模組';
$Lang['eClassApp']['ModuleNameAry']['ApplyLeave'] = '請假';
$Lang['eClassApp']['ModuleNameAry']['CrossBoundarySchoolCoaches'] = '跨境校巴';
$Lang['eClassApp']['ModuleNameAry']['eAttendance'] = '考勤紀錄';
$Lang['eClassApp']['ModuleNameAry']['eCircular'] = '職員通告';
$Lang['eClassApp']['ModuleNameAry']['eHomework'] = '家課表';
$Lang['eClassApp']['ModuleNameAry']['eNotice'] = '通告';
$Lang['eClassApp']['ModuleNameAry']['ePayment'] = '繳費紀錄';
$Lang['eClassApp']['ModuleNameAry']['GroupMessage'] = '小組訊息';
$Lang['eClassApp']['ModuleNameAry']['iMail'] = 'iMail';
$Lang['eClassApp']['ModuleNameAry']['SchoolCalendar'] = '校曆表';
$Lang['eClassApp']['ModuleNameAry']['SchoolNews'] = '學校宣佈';
$Lang['eClassApp']['ModuleNameAry']['TeacherStatus'] = '教職員目錄'; 
$Lang['eClassApp']['ModuleNameAry']['eEnrolment'] = '活動點名';
$Lang['eClassApp']['ModuleNameAry']['StudentStatus'] = '學生目錄'; 
if ($sys_custom['DHL']) {
	$Lang['eClassApp']['ModuleNameAry']['SchoolInfo'] = '公司資訊';
} else {
	$Lang['eClassApp']['ModuleNameAry']['SchoolInfo'] = '學校資訊';
}
$Lang['eClassApp']['ModuleNameAry']['StaffAttendance'] = '教職員考勤紀錄';
$Lang['eClassApp']['ModuleNameAry']['StudentPerformance'] = '課堂表現記錄';
$Lang['eClassApp']['ModuleNameAry']['flippedchannel'] = '翻轉頻道';
$Lang['eClassApp']['ModuleNameAry']['digitalChannels'] = '數碼頻道';
$Lang['eClassApp']['ModuleNameAry']['photoAlbum'] = '電子相簿';
$Lang['eClassApp']['ModuleNameAry']['takeAttendance'] = '點名';
$Lang['eClassApp']['ModuleNameAry']['medicalCaring'] = '護理系統';
$Lang['eClassApp']['ModuleNameAry']['weeklyDiary'] = '週記和剪報';
$Lang['eClassApp']['ModuleNameAry']['eLibPlus'] = '電子圖書館';
$Lang['eClassApp']['ModuleNameAry']['timetable'] = '時間表';
$Lang['eClassApp']['ModuleNameAry']['hkuFlu'] = '港大疾病監測';
$Lang['eClassApp']['ModuleNameAry']['classroom'] = '網上教室';
$Lang['eClassApp']['ModuleNameAry']['eBooking'] = '電子資源預訂';
$Lang['eClassApp']['ModuleNameAry']['eDiscipline'] = '訓導管理';
$Lang['eClassApp']['ModuleNameAry']['hostelTakeAttendance'] = '宿舍點名';
$Lang['eClassApp']['ModuleNameAry']['iPortfolio'] = 'iPortfolio';
$Lang['eClassApp']['ModuleNameAry']['eSchoolBus'] = '校車紀錄';
$Lang['eClassApp']['ModuleNameAry']['eEnrolmentUserView'] = '課外活動';
$Lang['eClassApp']['ModuleNameAry']['reprintCard'] = '補領智能卡';
$Lang['eClassApp']['ModuleNameAry']['ePOS'] = 'ePOS';
$Lang['eClassApp']['ModuleNameAry']['eInventory'] = '資產管理行政系統';
$Lang['eClassApp']['ModuleNameAry']['eLearningTimetable'] = '網上學習表';
$Lang['eClassApp']['ModuleNameAry']['bodyTemperature'] = '體溫紀錄';

//N
$Lang['eClassApp']['NoOfCode'] = '授權碼數目';
$Lang['eClassApp']['NoOfUsedCode'] = '已使用授權碼數目';
$Lang['eClassApp']['NotAcceptPushMessage'] = '沒有註冊推送訊息';
$Lang['eClassApp']['NotInUse'] = '未使用';
$Lang['eClassApp']['NotLoggedInUser'] = '未登入用戶';
$Lang['eClassApp']['NotYetRegistered'] = '未註冊';
$Lang['eClassApp']['NonClassTeacher'] = '非班主任';
$Lang['eClassApp']['NonTeacher'] = '非教職員';

//O


//P
$Lang['eClassApp']['ParentApp'] = '家長 App';
$Lang['eClassApp']['PushMessage'] = '推送訊息';
$Lang['eClassApp']['StudentPerformance']['Term'] = '學期';
$Lang['eClassApp']['StudentPerformance']['Student'] = '學生';
$Lang['eClassApp']['StudentPerformance']['Number'] = '班號';
$Lang['eClassApp']['StudentPerformance']['Name'] = '名稱';
$Lang['eClassApp']['StudentPerformance']['LatestScore'] = '最新表現分';
$Lang['eClassApp']['StudentPerformance']['TotalScore'] = '累積';
$Lang['eClassApp']['StudentPerformance']['TotalComment'] = '備註數量';
$Lang['eClassApp']['StudentPerformance']['TotalActScore'] = '累積表現分';
$Lang['eClassApp']['StudentPerformance']['Comment'] = '備註';
$Lang['eClassApp']['StudentPerformance']['ActScore'] = '表現分';
$Lang['eClassApp']['StudentPerformance']['Submit'] = '呈送';
$Lang['eClassApp']['StudentPerformance']['NumOfComment'] = '備註數量';
//Q


//R
$Lang['eClassApp']['Registered'] = '已註冊';
$Lang['eClassApp']['RegistrationDate'] = '註冊日期';
$Lang['eClassApp']['RegistrationStatus'] = '註冊狀況';
$Lang['eClassApp']['Recipient'] = '接收人';

//S
$Lang['eClassApp']['SchoolBadge'] = '校徽';
$Lang['eClassApp']['SchoolImage'] = '學校圖像';
$Lang['eClassApp']['SelectUser'] = '選擇成員';
$Lang['eClassApp']['SendTapCardPushMessage'] = '發送拍卡推送訊息';
$Lang['eClassApp']['SendTapCardPushMessageOnNonSchoolDay'] = '於非上課日發送拍卡推送訊息';
$Lang['eClassApp']['SendTapCardPushMessageType'] = '發送拍卡推送訊息類型';
$Lang['eClassApp']['SetAsAdmin'] = '設定為管理員';
$Lang['eClassApp']['SetAsMember'] = '設定為一般成員';
$Lang['eClassApp']['SchoolInfo']['Sub-menusOrItems'] = '子項目數量';
$Lang['eClassApp']['SchoolInfo']['AlertDelete1'] = '請選擇要刪除的項目。';
$Lang['eClassApp']['SchoolInfo']['AlertDelete2'] = '刪除項目會同時刪除其子項目，你是否確定要刪除?';
$Lang['eClassApp']['SchoolInfo']['AlertEnglishTitle'] = '請輸入英文標題。';
$Lang['eClassApp']['SchoolInfo']['AlertChineseTitle'] = '請輸入中文標題。';
$Lang['eClassApp']['SchoolInfo']['AlertChangeStatus'] = '請選擇要改變狀態的項目。';
$Lang['eClassApp']['SchoolInfo']['AlertInputUrl'] = '請輸入超鏈接。';
$Lang['eClassApp']['SchoolInfo']['AlertFileMissing'] = '請上載PDF檔。';
$Lang['eClassApp']['SchoolInfo']['AlertWrongFileType'] = '請上載PDF檔。';
$Lang['eClassApp']['SchoolInfo']['AlertFileOvercast'] = '舊PDF檔會被新PDF檔覆蓋，是否繼續？';
$Lang['eClassApp']['SchoolInfo']['Icon'] = '圖標';
$Lang['eClassApp']['SchoolInfo']['Type'] = '類型';
$Lang['eClassApp']['SchoolInfo']['NoIcon'] = '沒有圖標';
$Lang['eClassApp']['SchoolInfo']['DefaultIcon'] = '預設圖標';
$Lang['eClassApp']['SchoolInfo']['CustomIcon'] = '自定義圖標';
$Lang['eClassApp']['SchoolInfo']['Menu'] = '菜單';
$Lang['eClassApp']['SchoolInfo']['Item'] = '項目';
$Lang['eClassApp']['SchoolInfo']['Content'] = '內容';
$Lang['eClassApp']['SchoolInfo']['Hyperlink'] = '超鏈接';
$Lang['eClassApp']['SchoolInfo']['Eng'] = '英文';
$Lang['eClassApp']['SchoolInfo']['Chi'] = '中文';
$Lang['eClassApp']['SchoolInfo']['ItemTitle'] = '項目標題';
$Lang['eClassApp']['SchoolInfo']['SelectedMenu'] = '已選菜單';
$Lang['eClassApp']['SchoolInfo']['Status'] = '狀態';
$Lang['eClassApp']['SchoolInfo']['Public'] = '公開';
$Lang['eClassApp']['SchoolInfo']['Private'] = '保密';
$Lang['eClassApp']['SchoolInfo']['PDF'] = 'PDF檔';
$Lang['eClassApp']['SchoolInfo']['ReturnMsgArr']['ReorderSuccess'] = '1|=|排列成功';
$Lang['eClassApp']['SchoolInfo']['ReturnMsgArr']['ReorderFailed'] = '0|=|排列失敗';
$Lang['eClassApp']['StudentStatus']['MALE'] = '男';
$Lang['eClassApp']['StudentStatus']['FEMALE'] = '女';
$Lang['eClassApp']['StudentStatus']['ONTIME'] = '準時';
$Lang['eClassApp']['StudentStatus']['LATE'] = '遲到';
$Lang['eClassApp']['StudentStatus']['OUT'] = '外出活動';
$Lang['eClassApp']['StudentStatus']['EARLYLEAVE'] = '早退';
$Lang['eClassApp']['StudentStatus']['UNAVAILABLE'] = '缺席';
$Lang['eClassApp']['StudentStatus']['BIRTHDATE'] = '出生日期';
$Lang['eClassApp']['StudentStatus']['STATUS'] = '狀態';
$Lang['eClassApp']['StudentStatus']['CONTACT'] = '電話';
$Lang['eClassApp']['StudentStatus']['HOME'] = '住宅';
$Lang['eClassApp']['StudentStatus']['MOBILEPHONE'] = '手提';
$Lang['eClassApp']['StudentStatus']['EMAIL'] = '電子郵件';
$Lang['eClassApp']['StudentStatus']['ADDRESS'] = '地址';
$Lang['eClassApp']['StudentStatus']['NOACCESSRIGHT'] = '此功能僅對班主任開放。';
$Lang['eClassApp']['StudentStatus']['REMARK'] = '班主任預設可以查看自己班的學生名單。';
$Lang['eClassApp']['StudentStatus']['EMERGENCYCONTACT'] = '緊急聯絡人';
$Lang['eClassApp']['ShowStudentAttendanceStatus']= '於 app 首頁顯示考勤狀況';
$Lang['eClassApp']['ShowStudentAttendanceTime']= '於 app 首頁顯示考勤時間';
$Lang['eClassApp']['ShowStudentAttendanceLeaveSection']= '於 app 首頁顯示離校考勤資訊';
$Lang['eClassApp']['ShowStudentAttendanceDetails']= '於 app 顯示「考勤紀錄」';
$Lang['eClassApp']['ShowAttendanceTimeInEAttendance']= '於 app 「考勤紀錄」顯示考勤時間';
$Lang['eClassApp']['ShowStatusStatisticsInEAttendance']= '於 app 「考勤紀錄」顯示考勤狀態統計';
$Lang['eClassApp']['Send'] = '發送';
$Lang['eClassApp']['SendPushMessageToParentOfTheSelectedStudents'] = '發放推送訊息到以下已選擇學生的家長';
$Lang['eClassApp']['SendSuccess'] = '發送成功！';
$Lang['eClassApp']['SendFail'] = '發送失敗，請稍後再試。';
$Lang['eClassApp']['StudentApp'] = '學生 App';
$Lang['eClassApp']['StudentAttendance']['ShowStudentTag'] = "學生標籤";
$Lang['eClassApp']['StudentAttendance']['ShowNames'] = '顯示名字';
$Lang['eClassApp']['StudentAttendance']['ShowClassAndNum'] = '顯示班別和學號';
$Lang['eClassApp']['StudentAttendance']['ShowFullTag'] = '顯示完整標籤';
$Lang['eClassApp']['StudentAttendance']['ShowGroup'] = '顯示宿舍點名小組';
$Lang['eClassApp']['StudentAttendance']['NotShowTag'] = '不顯示標籤';
$Lang['eClassApp']['StudentAttendance']['TakeAttendance'] = '點名';
$Lang['eClassApp']['StudentAttendance']['EarlyLeave'] = '早退';
$Lang['eClassApp']['SchoolWebsiteURL'] = "學校網址";

//T
$Lang['eClassApp']['TeacherApp'] = '教職員 App';
$Lang['eClassApp']['Title'] = '標題';
$Lang['eClassApp']['Teacher'] = '教職員';
$Lang['eClassApp']['TapCardStatusAry']['Arrival'] = '到校';
$Lang['eClassApp']['TapCardStatusAry']['Leave'] = '離校';

//U
$Lang['eClassApp']['TeacherStatus'] = '教職員狀況';
$Lang['eClassApp']['TeacherStatusAccessRole'] = '開放對象';
$Lang['eClassApp']['TeacherStatusAccessTeacher'] = '指定教職員';
$Lang['eClassApp']['TeacherStatusAllTeacher'] = '全部教職員';
$Lang['eClassApp']['TeacherStatusSelectAtLeastOne'] = '請選擇至少一位教職員。';
$Lang['eClassApp']['TeacherStatus_Nonarrival'] = '未到';
$Lang['eClassApp']['TeacherStatus_Leave'] = '已離開';
$Lang['eClassApp']['TeacherStatus_Arrival'] = '在校';
$Lang['eClassApp']['TeacherStatus_Search'] = '搜尋';
$Lang['eClassApp']['TeacherStatus_UserPhoneNo'] = '此用戶未提供手提電話';
$Lang['eClassApp']['TeacherStatus_UserNoAccessRight'] = '此功能未經授權';
$Lang['eClassApp']['TeacherStatusAccessGroup'] = '可檢視小組';
$Lang['eClassApp']['TeacherStatusEnableAllGroup'] = '開放全部小組';
$Lang['eClassApp']['TeacherStatusEnableSpecificGroup'] = '開放指定小組';
$Lang['eClassApp']['TeacherStatusDisableGroup'] = '不啟用';
$Lang['eClassApp']['TeacherStatusGetGroup'] = '選擇小組';
$Lang['eClassApp']['TeacherStatusAuthorizedGroup'] = '指定組別';
$Lang['eClassApp']['TeacherStatusSelectAtLeastOneGroup'] = '請選擇至少一個組別。';
$Lang['eClassApp']['TakeAttendance']['ArrivalTime'] = '回校時間';
$Lang['eClassApp']['TakeAttendance']['LeaveTime'] = '離校時間';

//V

//W
$Lang['eClassApp']['Warning']['CannotSignBecauseNoAuthCode'] = '此裝置未經授權，閣下未能簽署此通告。';
$Lang['eClassApp']['Warning']['EmptyTitle'] = '請輸入訊息標題';
$Lang['eClassApp']['Warning']['EmptyContent'] = '請輸入訊息內容';
$Lang['eClassApp']['Warning']['NoRecipient'] = '請選擇最少一名收件者';
$Lang['eClassApp']['Warning']['PleaseSelectAtLeastOneOption'] = '請選擇最少一個項目';

$Lang['eClassApp']['WebModules']['DeleteConfirm']= '確定刪除';
$Lang['eClassApp']['WebModules']['DeleteConfirm2'] = '你是否確定刪除此紀錄?';
$Lang['eClassApp']['WebModules']['Delete'] = '刪除';
$Lang['eClassApp']['WebModules']['Close'] = '取消';

//X


//Y


//Z
/*
if($sys_custom['DHL'] && file_exists($intranet_root.'/lang/DHL/dhl_lang.b5.php'))
{
	include($intranet_root.'/lang/DHL/dhl_lang.b5.php');
}
*/
if (isset($sys_custom['Project_Label']) && $sys_custom['Project_Alias']) {
	if (file_exists($intranet_root.'/lang/' . $sys_custom['Project_Label']. '/' . $sys_custom['Project_Alias'] . 'lang.b5.php')) {
		include($intranet_root.'/lang/' . $sys_custom['Project_Label']. '/' . $sys_custom['Project_Alias'] . 'lang.b5.php');
	}
}
if (!$NoLangWordings && is_file("$intranet_root/templates/lang.$intranet_session_language.customized.php"))
{
  include("$intranet_root/templates/lang.$intranet_session_language.customized.php");
}
?>