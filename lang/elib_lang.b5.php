<?php
// Modifing by
$eLib_plus["html"]["title"] = "標題";
$eLib_plus["html"]["add"] = "新增";
$eLib_plus["html"]["all"] = "全部";
$eLib_plus["html"]["order"] = "排序";
$eLib_plus["html"]["book"] = "實體書";
$eLib_plus["html"]["ebook"] = "電子書";
$eLib_plus["html"]["advanced"] = "進階搜尋";
$eLib_plus["html"]["circulations"] = "借還/圖書流通管理";
$eLib_plus["html"]["adminsetting"] = "設定及圖書管理";
$eLib_plus["html"]["themesetting"] = "設定個人化主題";
$eLib_plus["html"]["background"] = "背景";
$eLib_plus["html"]["bookshelf"] = "書架";
$eLib_plus["html"]["myrecord"] = "我的紀錄";
$eLib_plus["html"]["bookcategory"] = "圖書類別";
$eLib_plus["html"]["reports"] = "報告";
$eLib_plus["html"]["pconly"] = "電腦版本";
$eLib_plus["html"]["pcipad"] = "電腦+iPad版本";
$eLib_plus["html"]["listallbooks"] = "全部顯示";
$eLib_plus["html"]["ranking"] = "排行榜";
$eLib_plus["html"]["calender"] = "日曆";
$eLib_plus["html"]["announcement"] = "告示";
$eLib_plus["html"]["rules"] = "規則";
$eLib_plus["html"]["staff"] = "管理員";
$eLib_plus["html"]["news"] = "最新消息";
$eLib_plus["html"]["more"] = "更多";
$eLib_plus["html"]["manage"] = "管理";
$eLib_plus["html"]["recommend"] = "推介";
$eLib_plus["html"]["new"] = "最新";
$eLib_plus["html"]["readnow"] = "開始閱讀";
$eLib_plus["html"]["bookdetail"] = "詳細資料";
$eLib_plus["html"]["youve"] = "已";
$eLib_plus["html"]["thisweek"] = "今週";
$eLib_plus["html"]["thismonth"] = "今月";
$eLib_plus["html"]["thisyear"] = "今年";
$eLib_plus["html"]["accumulated"] = "累積";
$eLib_plus["html"]["times"] = "次";
$eLib_plus["html"]["reviews"] = "書評";
$eLib_plus["html"]["location"] = "館藏";
$eLib_plus["html"]["status"] = "狀態";
$eLib_plus["html"]["hits"] = "點擊";
$eLib_plus["html"]["hitsofreading"] = "點擊";
$eLib_plus["html"]["addtofavourite"] = "加入我的最愛";
$eLib_plus["html"]["removefavourite"] = "從我的最愛中移除";
$eLib_plus["html"]["requesthold"] = "預約";
$eLib_plus["html"]["cancelhold"] = "取消預約";
$eLib_plus["html"]["cannothold"] = "無法預約更多此類書了!";
$eLib_plus["html"]["cannothold_borrowed"] = "無法預約你借閱中的圖書。";
$eLib_plus["html"]["cannothold_over_quota"] = "您沒有限額去預約圖書。";
$eLib_plus["html"]["cannothold_book_disable"] = "此圖書不能預約。";
$eLib_plus["html"]["cannothold_book_available"] = "此圖書可供借出，現在無需預約。";
$eLib_plus["html"]["writereview"] = "寫書評";
$eLib_plus["html"]["yourreview"] = "你的書評";
$eLib_plus["html"]["unlike"] = "收回讚";
$eLib_plus["html"]["like"] = "讚！";
$eLib_plus["html"]["likes"] = "讚好數目";
$eLib_plus["html"]["lessthan1minago"] = '少於 1 分鐘前';
$eLib_plus["html"]["minsago"] = '分鐘前';
$eLib_plus["html"]["hoursago"] = '小時前';
$eLib_plus["html"]["today"] = '今天';
$eLib_plus["html"]["yesterday"] = '昨天';
$eLib_plus["html"]["daysago"] = "天前";
$eLib_plus["html"]["available"] = "可供借閱";
$eLib_plus["html"]["notavailable"] = "不可借閱";
$eLib_plus["html"]["allbookcategories"] = "所有圖書類別";
$eLib_plus["html"]["viewby"] = "檢視";
$eLib_plus["html"]["booklist"] = "圖書列表";
$eLib_plus["html"]["bookcover"] = "封面";
$eLib_plus["html"]["showmore"] = "顯示更多";
$eLib_plus["html"]["nomorebooks"] = "沒有其他圖書";
$eLib_plus["html"]["eng"] = "英文電子書";
$eLib_plus["html"]["chi"] = "中文電子書";
$eLib_plus["html"]["others"] = "實體書";
$eLib_plus["html"]["tags_all_books"] = "標籤 (實體及電子書)";
$eLib_plus["html"]["mostloanbook"] = "借出最多的圖書";
$eLib_plus["html"]["mosthitbook"] = "點擊最多的圖書";
$eLib_plus["html"]["bestreview"] = "最好書評";
$eLib_plus["html"]["mostactivereviewers"] = "最活躍的書評作者";
$eLib_plus["html"]["mostactiveborrowers"] = "借書最多的讀者";
$eLib_plus["html"]["mosthelpfulreview"] = "最有用書評";
$eLib_plus["html"]["edition"] = "版本";
$eLib_plus["html"]["noofcopyavailable"]  = "可借書量";
$eLib_plus["html"]["noofcopyavailable_short"]  = "可借書量";
$eLib_plus["html"]["noofcopyborrowed_short"]  = "借出書量";
$eLib_plus["html"]["noofcopyreserved_short"]  = "預約數目";
$eLib_plus["html"]["tags"]  = "標籤";
$eLib_plus["html"]["tag"]  = "標籤";
$eLib_plus["html"]["noreviews"] = "沒有書評";
$eLib_plus["html"]["hitrate"] = "點閱率";
$eLib_plus["html"]["currentstatus"] = "現時紀錄";
$eLib_plus["html"]["loanrecord"] = "借還紀錄";
$eLib_plus["html"]["penaltyrecord"] = "罰款紀錄";
$eLib_plus["html"]["lostrecord"] = "報失紀錄";		//20140509-lost-book-list
$eLib_plus["html"]["quota"] = "配額";
$eLib_plus["html"]["checkedout"] = "借出";
$eLib_plus["html"]["checkedoutdate"] = "借書日期";
$eLib_plus["html"]["duedate"] = "到期日期";
$eLib_plus["html"]["overdue"] = "逾期";
$eLib_plus["html"]["renew"] = "續借";
$eLib_plus["html"]["days"] = "天";
$eLib_plus["html"]["requesthold"] = "預約";
$eLib_plus["html"]["requesteddate"] = "預約日期";
$eLib_plus["html"]["readyforpickup"] = "現可借";
$eLib_plus["html"]["notreturnedyet"] = "待還";
$eLib_plus["html"]["myloanbookrecord"] = "借閱紀錄";
$eLib_plus["html"]["ebookrecord"] = "電子書閱讀紀錄";
$eLib_plus["html"]["myfavourite"] = "我的最愛";
$eLib_plus["html"]["myreview"] = "我的書評";
$eLib_plus["html"]["reviewer"] = "書評作者";
$eLib_plus["html"]["norecord"] = "未有記錄";
$eLib_plus["html"]["borrowed"] = "曾被借閱";
$eLib_plus["html"]["borrowed2"] = "借閱";
$eLib_plus["html"]["reserved"] = "預約";
$eLib_plus["html"]["returndate"] = "還書日期";
$eLib_plus["html"]["reason"] = "原因";
$eLib_plus["html"]["totalpenalty"] = "罰款總數";
$eLib_plus["html"]["outstandingpenalty"] = "未繳罰款";
$eLib_plus["html"]["bestrate"] = "最佳評分";
$eLib_plus["html"]["mosthit"] = "最高點擊";
$eLib_plus["html"]["mostloan"] = "最多借閱";
$eLib_plus["html"]["borrow"] = "借閱";
$eLib_plus["html"]["books_unit"] = "本";
$eLib_plus["html"]["borrows_unit"] = "項借閱";
$eLib_plus["html"]["lastreview"] = "最近書評";
$eLib_plus["html"]["noofbooksborrowed"] = "借書總數";
$eLib_plus["html"]["areusuretocancelthereservation"] = "你確定要取消預約嗎?";
$eLib_plus["html"]["areusuretorenewthebook"] = "你確定要續借書本嗎?";
$eLib_plus["html"]["areusuretoremovethereview"] = "你確定要移除書評嗎?";
$eLib_plus["html"]["failedtorenew"] = "續借失敗";
$eLib_plus["html"]["renewcount"] = "續借次數";
$eLib_plus["html"]["booklost"] = "遺失書本";
$eLib_plus["html"]["openinghours"] = "開放時間";
$eLib_plus["html"]["closed"] = "休館";
$eLib_plus["html"]["closed"] = "休館";
$eLib_plus["html"]["closing"] = "休館中";
$eLib_plus["html"]["opening"] = "開放中";
$eLib_plus["html"]["specialtime"] = "特別開放";
$eLib_plus["html"]["weekday"]['Monday'] = "星期一";
$eLib_plus["html"]["weekday"]['Tuesday'] = "星期二";
$eLib_plus["html"]["weekday"]['Wednesday'] = "星期三";
$eLib_plus["html"]["weekday"]['Thursday'] = "星期四";
$eLib_plus["html"]["weekday"]['Friday'] = "星期五";
$eLib_plus["html"]["weekday"]['Saturday'] = "星期六";
$eLib_plus["html"]["weekday"]['Sunday'] = "星期日";
$eLib_plus["html"]["anonymous"] = "無名氏";
$eLib_plus["html"]["recommendbookto"] = "推介好書給";
$eLib_plus["html"]["content"] = "內容";
$eLib_plus["html"]["attachment"] = "附件";
$eLib_plus["html"]["attachfile"] = "加入附件";
$eLib_plus["html"]["attachedfile"] = "已加入的附件";
$eLib_plus["html"]["replaceattachedfile"] = "取代已加入的附件?";
$eLib_plus["html"]["areusuretoremovetherecommendation"] = "你確定要移除推介嗎?";
$eLib_plus["html"]["pleaseentercontent"] = '請輸入內容';
$eLib_plus["html"]["pleasechooseclasslevels"] = '請選擇班級';
$eLib_plus["html"]["librarywillopenedon"] = '圖書館將於 %s 開始開放使用';
$eLib_plus["html"]["libraryclosedsince"] = '圖書館已於 %s 暫停開放';
$eLib_plus["html"]["export"]["review_statistics"] = "匯出書評數據";
$eLib_plus["html"]["export"]["reviews"] = "匯出書評內容";
$eLib_plus["html"]["export"]["Title"] = "書名";
$eLib_plus["html"]["export"]["Identity"] = "身份";
$eLib_plus["html"]["export"]["UserLogin"] = "內聯網帳號";
$eLib_plus["html"]["export"]["EnglishName"] = "英文名";
$eLib_plus["html"]["export"]["ChineseName"] = "中文名";
$eLib_plus["html"]["export"]["ClassName"] = "班別";
$eLib_plus["html"]["export"]["ClassNumber"] = "學號";
$eLib_plus["html"]["export"]["Rating"] = "評分";
$eLib_plus["html"]["export"]["Content"] = "書評";
$eLib_plus["html"]["export"]["DateModified"] = "最近修改日期";
$eLib_plus["html"]["expiredBook"]["onCoverRemark"] = "此書已下架";


$eLib["eLibrary"] = "<a href=\"/home/eLearning/elibrary/\" style=\"text-decoration:none\" class=\"title\">網上圖書館</a>";
$eLib["html"]["home"] = "主頁";
$eLib["html"]["elibrary_settings"] = "網上圖書館設定";
$eLib["html"]["portal_display_settings"] = "主頁顯示設定";

$eLib["html"]["book_name"] = "書本名稱";
$eLib["html"]["book_cover"] = "封面";

$eLib["html"]["records"] = "記錄";
$eLib["html"]["display_top"] = "顯示 : 置頂";
$eLib["html"]["catalogue"] = "目錄";
$eLib["html"]["books"] = "書本";
$eLib["html"]["display"] = "顯示 : ";
$eLib["html"]["display2"] = "顯示";
$eLib["html"]["page"] = "頁";
$eLib["html"]["view"] = "檢視";

$eLib["html"]["most_active_reviewers"] = "最活躍的書評作者";
$eLib["html"]["most_useful_reviews"] = "最有用的書評";
$eLib["html"]["list_all"] = "檢視全部";
$eLib["html"]["last_week"] = "上星期最多閱讀次數";
$eLib["html"]["accumulated"] = "累積閱讀次數";

$eLib["html"]["recommended_books"] = "校方推介好書";
$eLib["html"]["bookS_with_highest_hit_rate"] = "最高點閱率的書";
$eLib["html"]["recommended_reason"] = "推介理由";

$eLib["html"]["save"] = "儲存";
$eLib["html"]["submit"] = "提交";
$eLib["html"]["reset"] = "重設";
$eLib["html"]["cancel"] = "取消";
$eLib["html"]["confirm"] = "確定";
$eLib["html"]["Close"] = "關閉";
$eLib["html"]["Add"] = "添加>>";
$eLib["html"]["Remove"] = "<<刪除";
$eLib["html"]["Delete"] = "刪除";
$eLib["html"]["Success"] = '成功\"';
$eLib["html"]["Fail"] = "失敗";
$eLib["html"]["Cancel_License"] = "取消授權";


$eLib["html"]["settings"] = "設定";

$eLib["html"]["personal"] = "個人";
$eLib["html"]["my_reading_history"] = "我的閱讀紀錄";
$eLib["html"]["my_books"] = "我的圖書";
$eLib["html"]["my_favourites"] = "我的最愛";
$eLib["html"]["my_reviews"] = "我的書評";
$eLib["html"]["my_notes"] = "我的筆記";
$eLib["html"]["public"] = "公開";
$eLib["html"]["all_reviews"] = "全部書評";
$eLib["html"]["all_reviews_2"] = "所有書評";
$eLib["html"]["student_summary"] = "學生閱讀及書評數目";
$eLib["html"]["chinese"] = "中文書";
$eLib["html"]["english"] = "英文書";
$eLib["html"]["show_all"] = "全部顯示";

$eLib["html"]["advance_search"] = "進階搜尋";
$eLib["html"]["search_result"] = "搜尋結果";

$eLib["html"]["title"] = "書名";
$eLib["html"]["subtitle"] = "副題";
$eLib["html"]["author"] = "作者";
$eLib["html"]["source"] = "來源";
$eLib["html"]["category"] = "類別";
$eLib["html"]["level"] = "該系列中的程度";
$eLib["html"]["date"] = "日子";
$eLib["html"]["book_input_date"] = "書本輸入日期";
$eLib["html"]["last_added"] = "最近更新";
$eLib["html"]["with_worksheets"] = "工作紙";
$eLib["html"]["sort_by"] = "排序";
$eLib['Book']["Code"] = "編號";

$eLib["html"]["book_title"] = "書名";
$eLib["html"]["publisher"] = "出版社";
$eLib["html"]["publish_year"] = "出版年份";
$eLib["html"]["publish_place"] = "出版地";

$eLib["html"]["all_category"] = "全部類別";
$eLib["html"]["all_level"] = "所有系列中的程度";

$eLib["html"]["report"] = "報告";
$eLib["html"]["record"] = "記錄";
$eLib["html"]["total"] = "全部";

$eLib["html"]["keywords"] = "關鍵字";
$eLib["html"]["no_record"] = "沒有記錄 !";
$eLib["html"]["description"] = "描述";
$eLib["html"]["rating"] = "評分";

$eLib["html"]["click_to_read"] = "點擊閱讀";
$eLib["html"]["reviews"] = "書評";
$eLib["html"]["reviewsUnit"] = "則";
$eLib["html"]["add_to_my_favourite"] = "加入我的最愛";
$eLib["html"]["remove_my_favourite"] = "從我的最愛中移除";

$eLib["html"]["recommend_this_book"] = "推介此書";
$eLib["html"]["add_new_review"] = "加入新的書評";

$eLib["html"]["yes"] = "是";
$eLib["html"]["no"] = "否";
$eLib["html"]["was_this_review_helpful"] = "你覺得這書評有幫助嗎?";
$eLib["html"]["of"] = "/";
$eLib["html"]["people_found_this_review_helpful"] = "用戶覺得此書評有用";

$eLib["html"]["review_content"] = "書評內容";

$eLib["html"]["personal_records"] = "個人記錄";

$lastRead = "最近點閱.不要";
$lastReadArray = explode(".", $lastRead);
$eLib["html"]["last_read"] = $lastReadArray[0];
$eLib["html"]["last_modified"] = "最近更新";
$eLib["html"]["remove"] = "移除";

$eLib["html"]["confirm_remove_msg"] = "你確定要移除這紀錄嗎?";
$eLib["html"]["notes_content"] = "筆記內容";

$eLib["html"]["all_books"] = "全部書本";
$eLib["html"]["all_categories"] = "全部類別";

$eLib["html"]["num_of"] = "數字";

$eLib["html"]["num_of_review"] = "書評總數";
$eLib["html"]["last_review_date"] = "最近評書日期";
$eLib["html"]["last_submitted"] = "最近提交";

$eLib["html"]["student_name"] = "學生姓名";
$eLib["html"]["num_of_book_read"] = "書本總數";
$eLib["html"]["num_of_book_hit"] = "點擊總數";

$eLib["html"]["name"] = "名字";

$eLib["html"]["class"] = "班級";
$eLib["html"]["class_number"] = "學號";
$eLib["html"]["all_class"] = "全部班級";
$eLib["html"]["student"] = "學生";
$eLib["html"]["class_name"] = "班級名字";

$eLib["html"]["book_read"] = "書本閱讀";
$eLib["html"]["review"] = "書評";

$eLib["html"]["book_with_hit_rate_accumulated"] = "累積最高";
$eLib["html"]["book_with_hit_rate_last_week"] = "最近一星期最高";

$eLib["html"]["whole_school"] = "全校";

$eLib["html"]["recommend_to"] = "校方推介好書給";
$eLib["html"]["reason_to_recommend"] = "推介原因";

$eLib["html"]["language"] = "語言";
$eLib["html"]["subcategory"] = "子類別";
$eLib["html"]["english_books"] = "英文書";
$eLib["html"]["chinese_books"] = "中文書";

$eLib["html"]["all_english_books"] = "全部英文書";
$eLib["html"]["all_chinese_books"] = "全部中文書";

$eLib["html"]["all_english_categories"] = "全部英文類別";
$eLib["html"]["all_chinese_categories"] = "全部中文類別";
$eLib["html"]["search_input"] = "搜尋..";
$eLib["html"]["search"] = "搜尋";
$eLib["html"]["please_enter_keywords"] = "請輸入搜尋關鍵字";
$eLib["html"]["publish"] = "公開";
$eLib["html"]["unpublish"] = "不公開";

$eLib["html"]["please_enter_title"] = "請輸入書名";
$eLib["html"]["please_enter_author"] = "請輸入作者";
$eLib["html"]["please_upload_csvfile"] = "請選擇CSV資料檔";

$eLib["html"]["with_books_cover"] = "顯示封面";
$eLib["html"]["table_list"] = "顯示列表";

$eLib["html"]["no_recommend_book"] = "沒有推介書。";


$eLib['ManageBook']["ImportZIPDescribeHeading"] = "ZIP檔案需備下檔案";

$eLib['ManageBook']["ImportZIPDescribeChinese"] = "
				中文書:<br />
				<b>content.text</b> - text檔,  將轉換成XML檔案 <br />
				<b>\"image\" 文件夾</b> - 包含相關圖片案. <br />
				<b>control.text</b> - 控制轉換過程. <br />
													";
$eLib['ManageBook']["ImportZIPDescribeEnglish"] = "
				英文書:<br />
				<b>content.xml</b> - XML檔,  將轉換成XML檔案 <br />
				<b>\"image\" 文件夾</b> - 包含相關圖片案. <br />
				";

//ebook by Josephine
$eLib["html"]["recommended_books_2"] = "推介好書";
$eLib["html"]["num_of_students"] = "學生總數";
$eLib["html"]["num_of_completed_books"] = "完成閱讀總數";
$eLib["html"]['Total'] = "總數";
$eLib["html"]['Progress'] = "進度";
$eLib["html"]['ReadTimes'] = "閱讀次數";
$eLib["html"]['AddToFavourite']	= "Add this book to My Favourite";
$eLib["html"]['AddBook'] = "Add Book";
$eLib["html"]['MyFavourite'] = "My Favourite";
$eLib["html"]['MyRecommendation'] = "My Recommendation";
$eLib["html"]["class_summary"] = "班級閱讀及書評數目";
$eLib["html"]["Books_Read"]	= "過往閱讀";
$eLib["html"]["Incorrect_Date"]	= "輸入的時段不正確。請確認結束日期大於開始日期。";


//admin book license
$eLib['admin']['Enable'] = "Enable";
$eLib['admin']['Enabled_Quota'] = "Enabled Quota";
$eLib['license']['ManageBookLicense'] = "管理授權";

$eLib['admin']['Assigned_Quota'] = "已使用配額";
$eLib['admin']['Available_Quota'] = "尚餘配額/配額總數";
$eLib['admin']['Quota_Left'] = "尚餘配額";
$eLib['admin']['Assign_Student'] = "進行授權";
$eLib['admin']['View_Assign_Student'] = "檢視/編輯獲授權學生";

$eLib['admin']['Confirm_Assign_Student'] = "你是否確定要授權已選擇的學生？";
$eLib['admin']['Confirm_Remove_Assign_Student'] = "你是否確定要取消已選擇學生的授權？";
$eLib['admin']['Delete_Assign_Student_Instruction'] = "註：只有過往48小時內獲授權的用戶可以被取消授權。";
$eLib['admin']['SelectedStudent'] = "已選學生 ";
$eLib['admin']['Students_Licensed'] = "已授權學生";
$eLib['admin']['Licensed_Students'] = "獲授權學生";
$eLib['SystemMsg']['NoMoreAvailableQuota'] = "尚餘配額滿";///"No more available quota.";
$eLib['SystemMsg']['AccessDenied'] = "很抱歉，你沒有進入此頁的權限。";
$eLib['SystemMsg']['Err_NoReadRight'] = "你沒有權限閱讀這本書";
$eLib['SystemMsg']['Err_NoStudentsSelected'] = "請選擇最少一名學生。";

$eLib['SystemMsg']['NoMyBook'] = "你沒有圖書";
$elib['SystemMsg']['Err_ExistInvalidQuota'] = "<font color='red'>Quota is invalidated due to unknown modifications</font>";

$eLib["html"]["Average"] = "平均";
$eLib["html"]["Period"] = "時段";
$eLib["html"]["Apply"] = "套用";



# eLibrary
$i_eLibrary_System = "網上圖書館";
$i_eLibrary_System_Admin_User_Setting = "設定管理用戶";
$i_eLibrary_System_Current_Batch = "現有批次";
$i_eLibrary_System_Update_Batch = "批次更新";
$i_eLibrary_System_Updated_Batch = "已更新批次";
$i_eLibrary_System_Reset_Fail = "更新批次失敗";
$i_eLibrary_System_Updated_Records = "已更新記錄";
$eLib["AdminUser"] = "管理用戶";

$eLib['ManageBook']["Title"] = "管理書籍";
$eLib['ManageBook']["ConvertBook"] = "轉換圖書";
$eLib['ManageBook']["ContentManage"] = "管理內容";
$eLib['ManageBook']["ContentView"] = "檢視內容";
$eLib['ManageBook']["ImportBook"] = "匯入圖書";
$eLib['ManageBook']["ImportCSV"] = "匯入CSV檔案";
$eLib['ManageBook']["ImportXML"] = "匯入XML檔案";
$eLib['ManageBook']["ImportZIP"] = "匯入ZIP檔案";
$eLib['ManageBook']["ImportZIPDescribe"] = "
				ZIP檔案需備下檔案<br />
				content.text - text檔,  將轉換成XML檔案 <br />
				 \"image\" 文件夾 - 包含相關圖片案. <br />
				control.text - 控制轉換過程. <br />
													";

$eLib['Book']["Author"] = "作者";
$eLib['Book']["SeriesEditor"] = "系列編輯";
$eLib['Book']["Category"] = "類型";
$eLib['Book']["Subcategory"] = "子類型";
$eLib['Book']["DateModified"] = "最近修改日期";
$eLib['Book']["Description"] = "簡介";
$eLib['Book']["Language"] = "語言";
$eLib['Book']["Level"] = "等級";
$eLib['Book']["AdultContent"] = "敏感內容";
$eLib['Book']["Title"] = "書名";
$eLib['Book']["Publisher"] = "出版社";
$eLib['Book']["TableOfContent"] = "目錄";
$eLib['Book']["Copyright"] = "版權";

$eLib["SourceFrom"] = "資料來源";
$eLib['Source']["green"] = "Green Apple";
$eLib['Source']["cup"] = "Cambridge University Press";

$eLib['EditBook']["IsChapterStart"] = "章節開始?";
$eLib['EditBook']["ChapterID"] = "章節索引";
$i_junior_leftmenu_hide="隱<br/>藏";
$button_submit = "呈送";
$button_cancel = "取消";
$i_QB_LangSelectEnglish = "英文";
$i_QB_LangSelectChinese = "中文";
$button_upload = "上載";
$i_QB_LangSelect = "語言";
$i_status_all = "全部";
$button_edit = "編輯";
$button_remove = "刪除";
$button_new = "新增";
$list_total = "總數";
$list_page = "頁";
$i_general_EachDisplay = "每頁顯示";
$i_general_PerPage = "項";
$button_previous_page = "上一頁";
$button_next_page = "下一頁";
$i_adminmenu_fs = "功能設定";
$i_general_all_classes = "全部班別";
$button_select = "選擇";



# below are the wordings may duplicate with the one above

$eLib["html"]["home"] = "主頁";
$eLib["html"]["elibrary_settings"] = "電子圖書設定";
$eLib["html"]["portal_display_settings"] = "主頁顯示設定";

$eLib["html"]["book_name"] = "書本名稱";
$eLib["html"]["book_cover"] = "封面";

$eLib["html"]["records"] = "記錄";
$eLib["html"]["display_top"] = "顯示 : 置頂";
$eLib["html"]["catalogue"] = "目錄";
$eLib["html"]["books"] = "書本";
$eLib["html"]["display"] = "顯示 : ";
$eLib["html"]["display2"] = "顯示";
$eLib["html"]["page"] = "頁";
$eLib["html"]["view"] = "檢視";

$eLib["html"]["most_active_reviewers"] = "最活躍的書評作者";
$eLib["html"]["most_useful_reviews"] = "最有用的書評";
$eLib["html"]["list_all"] = "檢視全部";
$eLib["html"]["last_week"] = "上星期最多閱讀次數";
$eLib["html"]["accumulated"] = "累積閱讀次數";

$eLib["html"]["recommended_books"] = "校方推介好書";
$eLib["html"]["bookS_with_highest_hit_rate"] = "最高點閱率的書";
$eLib["html"]["recommended_reason"] = "推介理由";

$eLib["html"]["save"] = "儲存";
$eLib["html"]["submit"] = "提交";
$eLib["html"]["reset"] = "重設";
$eLib["html"]["cancel"] = "取消";
$eLib["html"]["confirm"] = "確定";
$eLib["html"]["Close"] = "關閉";
$eLib["html"]["Add"] = "添加>>";
$eLib["html"]["Remove"] = "<<刪除";
$eLib["html"]["Delete"] = "刪除";
$eLib["html"]["Success"] = "成功";
$eLib["html"]["Fail"] = "失敗";
$eLib["html"]["Cancel_License"] = "取消授權";


$eLib["html"]["settings"] = "設定";

$eLib["html"]["personal"] = "個人";
$eLib["html"]["my_reading_history"] = "我的閱讀紀錄";
$eLib["html"]["my_books"] = "我的圖書";
$eLib["html"]["my_favourites"] = "我的最愛";
$eLib["html"]["my_reviews"] = "我的書評";
$eLib["html"]["my_notes"] = "我的筆記";
$eLib["html"]["public"] = "公開";
$eLib["html"]["all_reviews"] = "全部書評";
$eLib["html"]["all_reviews_2"] = "所有書評";
$eLib["html"]["student_summary"] = "學生閱讀及書評數目";
$eLib["html"]["chinese"] = "中文";
$eLib["html"]["english"] = "英文";
$eLib["html"]["show_all"] = "全部顯示";

$eLib["html"]["advance_search"] = "進階搜尋";
$eLib["html"]["search_result"] = "搜尋結果";

$eLib["html"]["title"] = "書名";
$eLib["html"]["author"] = "作者";
$eLib["html"]["source"] = "來源";
$eLib["html"]["category"] = "類別";
$eLib["html"]["level"] = "該系列中的程度";
$eLib["html"]["date"] = "日子";
$eLib["html"]["book_input_date"] = "書本輸入日期";
$eLib["html"]["last_added"] = "最近更新";
$eLib["html"]["with_worksheets"] = "工作紙";
$eLib["html"]["sort_by"] = "排序";
$eLib['Book']["Code"] = "編號";
$eLib['Book']["ePUB"] = "ePUB / zip 檔";
$eLib['Book']["ePUBIsImageBook"] = "ePUB 是否圖書";
$eLib['Book']["ePUBImageWidth"] = "ePUB 圖片寬度";
$eLib['Book']["ePUBImageWidthUnit"] = "象素";
$eLib['Book']["Format"] = "格式";
$eLib["html"]["book_title"] = "書名";
$eLib["html"]["publisher"] = "出版社";

$eLib["html"]["all_category"] = "全部類別";
$eLib["html"]["all_level"] = "所有系列中的程度";

$eLib["html"]["report"] = "報告";
$eLib["html"]["record"] = "記錄";
$eLib["html"]["total"] = "全部";

$eLib["html"]["keywords"] = "關鍵字";
$eLib["html"]["no_record"] = "沒有記錄 !";
$eLib["html"]["description"] = "描述";
$eLib["html"]["rating"] = "評分";

$eLib["html"]["click_to_read"] = "點擊閱讀";
$eLib["html"]["reviews"] = "書評";
$eLib["html"]["reviewsUnit"] = "則";
$eLib["html"]["add_to_my_favourite"] = "加入我的最愛";
$eLib["html"]["remove_my_favourite"] = "從我的最愛中移除";

$eLib["html"]["recommend_this_book"] = "推介此書";
$eLib["html"]["add_new_review"] = "加入新的書評";

$eLib["html"]["yes"] = "是";
$eLib["html"]["no"] = "否";
$eLib["html"]["was_this_review_helpful"] = "你覺得這書評有幫助嗎?";
$eLib["html"]["of"] = "/";
$eLib["html"]["people_found_this_review_helpful"] = "用戶覺得此書評有用";

$eLib["html"]["review_content"] = "書評內容";

$eLib["html"]["personal_records"] = "個人記錄";

$lastRead = "最近點閱.不要";
$lastReadArray = explode(".", $lastRead);
$eLib["html"]["last_read"] = $lastReadArray[0];
$eLib["html"]["last_modified"] = "最近更新";
$eLib["html"]["remove"] = "移除";

$eLib["html"]["confirm_remove_msg"] = "你確定要移除這紀錄嗎?";
$eLib["html"]["notes_content"] = "筆記內容";

$eLib["html"]["all_books"] = "全部書本";
$eLib["html"]["all_categories"] = "全部類別";

$eLib["html"]["num_of"] = "數字";

$eLib["html"]["num_of_review"] = "書評總數";
$eLib["html"]["last_review_date"] = "最近評書日期";
$eLib["html"]["last_submitted"] = "最近提交";

$eLib["html"]["student_name"] = "學生姓名";
//$eLib["html"]["num_of_book_read"] = "點閱書本總數";

$eLib["html"]["name"] = "名字";

$eLib["html"]["class"] = "班級";
$eLib["html"]["class_number"] = "學號";
$eLib["html"]["all_class"] = "全部班級";
$eLib["html"]["student"] = "學生";
$eLib["html"]["class_name"] = "班級名字";

$eLib["html"]["book_read"] = "書本閱讀";
$eLib["html"]["review"] = "書評";

$eLib["html"]["book_with_hit_rate_accumulated"] = "累積最高";
$eLib["html"]["book_with_hit_rate_last_week"] = "最近一星期最高";

$eLib["html"]["whole_school"] = "全校";

$eLib["html"]["recommend_to"] = "校方推介好書給";
$eLib["html"]["reason_to_recommend"] = "推介原因";

$eLib["html"]["language"] = "語言";
$eLib["html"]["subcategory"] = "子類別";
$eLib["html"]["english_books"] = "英文書";
$eLib["html"]["chinese_books"] = "中文書";

$eLib["html"]["all_english_books"] = "全部英文書";
$eLib["html"]["all_chinese_books"] = "全部中文書";

$eLib["html"]["all_english_categories"] = "全部英文類別";
$eLib["html"]["all_chinese_categories"] = "全部中文類別";
$eLib["html"]["search_input"] = "搜尋 ...";
$eLib["html"]["search"] = "搜尋";
$eLib["html"]["please_enter_keywords"] = "請輸入搜尋關鍵字";
$eLib["html"]["publish"] = "公開";
$eLib["html"]["unpublish"] = "不公開";

$eLib["html"]["please_enter_title"] = "請輸入書名";
$eLib["html"]["please_enter_author"] = "請輸入作者";

$eLib["html"]["with_books_cover"] = "顯示封面";
$eLib["html"]["table_list"] = "顯示列表";

$eLib["html"]["no_recommend_book"] = "沒有推介書。";


$eLib['ManageBook']["ImportZIPDescribeHeading"] = "ZIP檔案需備下檔案";

$eLib['ManageBook']["ImportZIPDescribeChinese"] = "
				中文書:<br />
				<b>content.text</b> - text檔,  將轉換成XML檔案 <br />
				<b>\"image\" 文件夾</b> - 包含相關圖片案. <br />
				<b>control.text</b> - 控制轉換過程. <br />
													";
$eLib['ManageBook']["ImportZIPDescribeEnglish"] = "
				英文書:<br />
				<b>content.xml</b> - XML檔,  將轉換成XML檔案 <br />
				<b>\"image\" 文件夾</b> - 包含相關圖片案. <br />
				";

//ebook by Josephine
$eLib["html"]["recommended_books_2"] = "推介好書";
$eLib["html"]["num_of_students"] = "學生總數";
$eLib["html"]["num_of_completed_books"] = "完成閱讀總數";
$eLib["html"]['Total'] = "總數";
$eLib["html"]['Progress'] = "進度";
$eLib["html"]['ReadTimes'] = "閱讀次數";
$eLib["html"]['AddToFavourite']	= "加到我的最愛";
$eLib["html"]['AddBook'] = "Add Book";
$eLib["html"]['MyFavourite'] = "我的最愛";
$eLib["html"]['MyFavouriteTime'] = "加入時間";
$eLib["html"]['MyRecommendation'] = "我的推介";
$eLib["html"]["class_summary"] = "班級閱讀及書評數目";
$eLib["html"]["Books_Read"]	= "過往閱讀";
$eLib["html"]["Incorrect_Date"]	= "輸入的時段不正確。請確認結束日期大於開始日期。";


//admin book license
$eLib['admin']['Enable'] = "Enable";
$eLib['admin']['Enabled_Quota'] = "Enabled Quota";
$eLib['license']['ManageBookLicense'] = "管理授權";

$eLib['admin']['Assigned_Quota'] = "已使用配額";
$eLib['admin']['Available_Quota'] = "尚餘配額/配額總數";
$eLib['admin']['Quota_Left'] = "尚餘配額";
$eLib['admin']['Assign_Student'] = "進行授權";
$eLib['admin']['View_Assign_Student'] = "檢視/編輯獲授權學生";

$eLib['admin']['Confirm_Assign_Student'] = "你是否確定要授權已選擇的學生？";
$eLib['admin']['Confirm_Remove_Assign_Student'] = "你是否確定要取消已選擇學生的授權？";
$eLib['admin']['Delete_Assign_Student_Instruction'] = "註：只有過往48小時內獲授權的用戶可以被取消授權。";
$eLib['admin']['SelectedStudent'] = "已選學生 ";
$eLib['admin']['Students_Licensed'] = "已授權學生";
$eLib['admin']['Licensed_Students'] = "獲授權學生";
$eLib['SystemMsg']['NoMoreAvailableQuota'] = "尚餘配額滿";///"No more available quota.";
$eLib['SystemMsg']['AccessDenied'] = "很抱歉，你沒有進入此頁的權限。";
$eLib['SystemMsg']['Err_NoReadRight'] = "你沒有權限閱讀這本書";
$eLib['SystemMsg']['Err_NoStudentsSelected'] = "請選擇最少一名學生。";

$eLib['SystemMsg']['NoMyBook'] = "你沒有圖書";
$elib['SystemMsg']['Err_ExistInvalidQuota'] = "<font color='red'>Quota is invalidated due to unknown modifications</font>";

$eLib["html"]["Average"] = "平均";


$eLib['Source']["-"] = "-";
$eLib['Source']["breakthrough"] = "突破";
$eLib['Source']["hkcrown"] = "皇冠";
$eLib['Source']["enrichculture"] = "天窗";
$eLib['Source']["rightman"] = "正文社";
$eLib['Source']['witman'] = "Witman";

$Lang['SysMgr']['RemoveReview'] = "你是否確定要刪除此書評？";
$Lang['SysMgr']['WarnEmptyReview'] = "請輸入你的書評！";

$eLib["html"]['hits_accumulated'] = "有 <HITS> 人次閱讀這書";
$eLib["html"]['hits'] = "閱讀點擊";

$eLib["action"]['open_book'] = "立即閱讀";
$eLib["action"]['view_details'] = "查看書本資料及書評";
$eLib["action"]['next_book'] = "下一本";
$eLib["action"]['previous_book'] = "上一本";

$eLib["html"]['ISBN'] = "ISBN";
$eLib["html"]['NoOfPage'] = "頁數";
$eLib["html"]['RelevantSubject'] = "適用科目";
$eLib["html"]['Subject'] = "科目";

$Lang['Btn']['Preview'] = "預覽";

$Lang["Btn"]['Save'] = "儲存";
$i_From = "由";
$i_To = "至";
$ip20TopMenu['eLibrary'] = $eLib["eLibrary"];
$Lang['JS_warning']['InvalidDateRange'] = "日期範圍無效。";

//----------------  2012-09-12 (CharlesMa) ------------------------
$eLib['Book']["forceToOnePageMode"] = "強制到單頁模式?";
$eLib['Book']["onePageModeWidth"] = "單頁模式寬度";
$eLib['Book']["onePageModeHeight"] = "單頁模式高度";
$eLib['Book']["onePageModeCover"] = "圖書封面";

//----------------  2013-05-30 (CharlesMa) ------------------------
$eLib['Book']["FirstPublished"] = "初版日期";
$eLib['Book']["ePublisher"] = "電子出版社";
$eLib['Book']["CopyrightYear"] = "版權年份";
$eLib['Book']["CopyrightStatement"] = "版權細則";
$eLib['Book']["IndexPageImage"] = "目錄圖像";
$eLib['Book']["Publisher"] = "出版社";

//----------------  2013-06-14 (CharlesMa) ------------------------
$eLib['Book']["WorkSheet"] = "工作紙";
$eLib['Book']["WorkSheetAns"] = "工作紙(附答案)";

//----------------  2013-04-29 (CharlesMa) ------------------------
$eLib_plus["html"]["video_btn"] = " video_btn_chn ";

//----------------  2013-09-03 (Siuwan) ------------------------
$eLib_plus["html"]['call_number'] = "索書號";

//----------------  2013-09-27 (CharlesMa) ------------------------
$eLib_plus["html"]['stats'] = "統計";

//----------------  2013-10-16 (CharlesMa) ------------------------
$eLib['Book']["FirstPageBlank"] = "空白首頁";
$eLib['Book']["BlankFirstPage"] = "首頁設白";
$eLib['Book']["UnblankFirstPage"] = "首頁還原";

$eLib["html"]["num_of_books"] = "書目";

//----------------  2013-11-07 (Siuwan) ------------------------
$eLib["html"]["book_status"] = "圖書狀態";

//----------------  2013-11-12 (CharlesMa) ------------------------
$eLib['Book']["DLPDF"] = "PDF 版本";

//----------------  2013-11-14 (CharlesMa) 		2013-11-14-deleteallbookcomment ------------------------
$eLib["html"]["remove_all_reviews"] = "移除全部書評";
$eLib_plus["html"]["areusuretoremoveallthereview"] = "你確定要移除全部書評嗎?";


$eLib['Book']["Renewal"][0] = "不可續借";
$eLib['Book']["Renewal"][-3] = "已到達續借上限";
$eLib['Book']["RenewalHoldBook"] = "圖書已被其他讀者預約";
//----------------  2013-12-27 (CharlesMa) 		20131213-eBookTW ------------------------
$eLib['Book']["BookID"] = "編號";
$eLib['Book']["CLC"] = "中圖分類";
$eLib['Book']["DDC"] = "杜威分類";
$eLib['Book']["Edition"] = "版本";
$eLib['Book']["ISBN"] = "ISBN";
$eLib['Book']["New"] = "新增";
$eLib['Book']["Update"] = "更新";
$eLib['Book']["BeforeImportMsg"] = "請注意，匯入前請先上載相關檔案到伺服器上的指定位置";
$eLib['Book']["IsFirstPageBlank"] = "空白首頁";
$eLib['Book']["IsRightToLeft"] = "設為直排";
$eLib['Book']["IsLeftToRight"] = "取消直排";

####################### This should be placed at the bottom

if (!$NoLangWordings && is_file("$intranet_root/templates/elib_lang.$intranet_session_language.customized.php"))
{
  include("$intranet_root/templates/elib_lang.$intranet_session_language.customized.php");
}

?>