<?php
// modifying by: Henry

/**
 * ******************
 * Log :
 * Date 2019-07-11 [Henry]
 * Ignore time checking for getDecryptedText
 *
 * Date 2018-09-19 [Henry]
 * File Created
 *
 * ******************
 */
$PATH_WRT_ROOT = "../../";

include_once ("{$PATH_WRT_ROOT}includes/global.php");
include_once ("{$PATH_WRT_ROOT}includes/libdb.php");
include_once ("{$PATH_WRT_ROOT}includes/libinterface.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_ui.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_utility.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_apps.php");
include_once ("{$PATH_WRT_ROOT}includes/json.php");

include_once ("../config.php");

include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

// for the customization
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_cust.php");
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_ui_cust.php");

// Get the intruction content
parse_str(getDecryptedText(urldecode($_REQUEST['id']), $admission_cfg['FilePathKey'],1000000), $output);

intranet_opendb();

// ### Lang START ####
if (method_exists($lauc, 'getAdmissionLang')) {
    $intranet_session_language = $lauc->getAdmissionLang($output['ApplicationID'], true);
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
} elseif ($intranet_session_language == 'en') {
	if($admission_cfg['IsBilingual']){
	    $intranet_session_language = 'b5';
	    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
	    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
	    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
	    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
	    $LangB5 = $Lang;
	    $kis_lang_b5 = $kis_lang;
	    unset($Lang);
	    unset($kis_lang);
	}
    $intranet_session_language = 'en';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
} else {
	if($admission_cfg['IsBilingual']){
	    $intranet_session_language = 'en';
	    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
	    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
	    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
	    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
	    $LangEn = $Lang;
	    $kis_lang_en = $kis_lang;
	    unset($Lang);
	    unset($kis_lang);
	}

    $intranet_session_language = 'b5';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
// ### Lang END ####

$IsUpdate = true;
$libkis = new kis('');
$lac = new admission_cust();
$lauc = new admission_ui_cust();
$li = new interface_html();
$json = new JSON_obj();
//$lfs = new libfilesystem();

if (! $plugin['eAdmission']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT("", "../");
    exit();
}

$allowToUpdate =
    (
        $sys_custom['KIS_Admission']['ApplicantUpdateForm'] &&
        (
            $lac->IsUpdatePeriod()
        ||
            $lac->IsUpdateExtraAttachmentPeriod()
        ||
            (
                $sys_custom['KIS_Admission']['UCCKE']['Settings']
            &&
                $lac->IsAfterUpdatePeriod()
            )
        )
    );


if (! $allowToUpdate) {
    header("Location: index_edit.php");
    exit();
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();
$basic_settings = $lac->getBasicSettings('99999');

#### Get next school year name START ####
$academicYearB5 = getAcademicYearByAcademicYearID($lac->schoolYearID, 'b5');
$academicYearEn = getAcademicYearByAcademicYearID($lac->schoolYearID, 'en');
#### Get next school year name END ####


$applicationSetting = $lac->getApplicationSetting($output['SchoolYearID'] ? $output['SchoolYearID'] : $lac->schoolYearID);


// #### Get last content START #### //
$lastContent = $applicationSetting[$output['sus_status']]['LastPageContent'];
if ($admission_cfg['MultipleLang'] && !$sys_custom['KIS_Admission']['CREATIVE']['Settings'] && !$sys_custom['KIS_Admission']['STANDARD']['Settings']) {
    foreach ($admission_cfg['Lang'] as $index => $lang) {
        if ($lang == $intranet_session_language) {
            $lastContent = $applicationSetting[$output['sus_status']]["LastPageContent{$index}"];
            break;
        }
    }
}
// #### Get last content END #### //

if($_REQUEST['StudentDateOfBirthYear'] || $_REQUEST['StudentDateOfBirthMonth'] || $_REQUEST['StudentDateOfBirthDay']){
	$_REQUEST['InputStudentDateOfBirth'] = $_REQUEST['StudentDateOfBirthYear'].'-'.$_REQUEST['StudentDateOfBirthMonth'].'-'.$_REQUEST['StudentDateOfBirthDay'];
}

$allowToViewOnly =
    (
        $sys_custom['KIS_Admission']['ApplicantUpdateForm'] &&
        (
            !$lac->IsUpdatePeriod()
        &&
            (
                $sys_custom['KIS_Admission']['UCCKE']['Settings']
            &&
                $lac->IsAfterUpdatePeriod()
            )
        )
    );
if($sys_custom['KIS_Admission']['EditUploadDocumentMode']){
	$main_content = $lauc->getUpdateAttachmentFinishPageContent($output['ApplicationID'], $lastContent, $output['SchoolYearID'], $output['sus_status']);
}
else if($allowToViewOnly){
	//if(!$_SESSION["UserID"]){
		$result = $lac->getApplicationResult($_REQUEST['InputStudentDateOfBirth'], $_REQUEST['InputStudentBirthCertNo'], $lac->schoolYearID, $_REQUEST['InputApplicationID']);
		if($result['ApplicationID'] != ''){
			$_SESSION['KIS_ApplicationID'] = $result['ApplicationID'];
			$_SESSION['KIS_StudentDateOfBirth'] = $_REQUEST['InputStudentDateOfBirth'];
			$_SESSION['KIS_StudentBirthCertNo'] = $_REQUEST['InputStudentBirthCertNo'];
		}

		$time = $_SERVER['REQUEST_TIME'];
		/**
		 * for a 30 minute timeout, specified in seconds
		 */
		$timeout_duration = 3600;

		/**
		 * Here we look for the user・s LAST_ACTIVITY timestamp. If
		 * it・s set and indicates our $timeout_duration has passed,
		 * blow away any previous $_SESSION data and start a new one.
		 */
		if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
			unset($_SESSION['KIS_ApplicationID']);
			unset($_SESSION['KIS_StudentDateOfBirth']);
			unset($_SESSION['KIS_StudentBirthCertNo']);
		}
		/**
		 * Finally, update LAST_ACTIVITY so that our timeout
		 * is based on it and not the user・s login time.
		 */
		$_SESSION['LAST_ACTIVITY'] = $time;
	//}

 	//$result = $lac->getApplicationResult($_REQUEST['InputStudentDateOfBirth'], $_REQUEST['InputStudentBirthCertNo'], $lac->schoolYearID, $_REQUEST['InputApplicationID']);
	if($result['ApplicationID'] != '' && $_REQUEST['InputStudentDateOfBirth'] != "" && $_REQUEST['InputStudentBirthCertNo'] != "" && $_REQUEST['InputApplicationID'] != ""){
		 $main_content = $lauc->getAfterUpdateFinishPageContent($result['ApplicationID'], $lastContent);
	}
	else{
		header("Location: index_edit.php?err=1");
    	exit();
	}
}
else if ($sys_custom['KIS_Admission']['MGF']['Settings'])
    $main_content = $lauc->getUpdateFinishPageContent($output['ApplicationID'], $lastContent, $output['SchoolYearID'], $output['sus_status']);
else
    $main_content = $lauc->getUpdateFinishPageContent($output['ApplicationID'], $lastContent, $output['SchoolYearID']);

//unset($_SESSION['KIS_ApplicationID']);
//unset($_SESSION['KIS_StudentDateOfBirth']);
//unset($_SESSION['KIS_StudentBirthCertNo']);

#### Step START ####
if($sys_custom['KIS_Admission']['EditUploadDocumentMode']){
	$stepArr = array(
	    array(
	        'id' => 'pageDocsUpload',
	        'title' => $lauc->getLangStr(array(
                'b5' => $LangB5['Admission']['docsUpload'],
                'en' => $LangEn['Admission']['docsUpload'],
            ))
	    ),
	    array(
	        'id' => 'pageConfirmation',
	        'title' => $lauc->getLangStr(array(
		        'b5' => $LangB5['Admission']['confirmation'],
		        'en' => $LangEn['Admission']['confirmation'],
	        ))
	    ),
	    array(
	        'id' => 'pageFinish',
	        'title' => $lauc->getLangStr(array(
		        'b5' => $LangB5['Admission']['finish'],
		        'en' => $LangEn['Admission']['finish'],
	        ))
	    )
	);
	$stepArr = $lauc->applyFilter($lauc::FILTER_ADMISSION_FORM_WIZARD_STEPS, $stepArr);
}
else if(!$allowToViewOnly){
	$stepArr = array(
	    array(
	        'id' => 'pageInstruction',
	        'title' => $lauc->getLangStr(array(
		        'b5' => $LangB5['Admission']['instruction'],
		        'en' => $LangEn['Admission']['instruction'],
	        ))
	    ),
	    array(
	        'id' => 'pagePersonalInfo',
	        'title' => $lauc->getLangStr(array(
		        'b5' => $LangB5['Admission']['personalInfo'],
		        'en' => $LangEn['Admission']['personalInfo'],
	        ))
	    ),
	    array(
	        'id' => 'pageDocsUpload',
	        'title' => $lauc->getLangStr(array(
		        'b5' => $LangB5['Admission']['docsUpload'],
		        'en' => $LangEn['Admission']['docsUpload'],
	        ))
	    ),
	    array(
	        'id' => 'pageConfirmation',
	        'title' => $lauc->getLangStr(array(
		        'b5' => $LangB5['Admission']['confirmation'],
		        'en' => $LangEn['Admission']['confirmation'],
	        ))
	    ),
	    array(
	        'id' => 'pageFinish',
	        'title' => $lauc->getLangStr(array(
		        'b5' => $LangB5['Admission']['finish'],
		        'en' => $LangEn['Admission']['finish'],
	        ))
	    ),
	);
	$stepArr = $lauc->applyFilter($lauc::FILTER_ADMISSION_FORM_WIZARD_STEPS, $stepArr);
}
#### Step END ####

###################################### UI START ######################################
include(__DIR__.'/header.php');
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/commonJs.php");
?>
    <style>
        ol{
            padding-left: 40px;
            list-style-type: decimal;
        }
        ul{
            padding-left: 40px;
            list-style-type: disc;
        }
    </style>
    <div id="blkSteps">
    	<span id="btnPrevStep" class="icon icon-button fa-chevron-left"></span>
    	<span id="btnNextStep" class="icon icon-button fa-chevron-right"></span>
		<div id="stepContainer"></div>
    </div>

    <article>
    	<?= $main_content ?>
    </article>

    <div id="blkButtons" class="layout-m">
		<div class="button floatR" id="btnFinishEdit"><?=$lauc->getLangStr(array(
            'b5' => $LangB5['Admission']['finish'],
            'en' => $LangEn['Admission']['finish'],
        ))?></div>
	</div>

<div id="blkFooter">
	<span id="lbleClass"><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="/images/kis/eadmission/eClassLogo.png"></a></span>
</div>

<div id="blkTopBar" class="transition">
	<div id="blkTitle">
		<img id="imgSchLogo" src="<?=$school['logo']?>">
    	<?php if($admission_cfg['DefaultLang'] == 'en'): ?>
    		<span id="lblSchName-eng"><?=$basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en']?></span> <?=$admission_cfg['AdmissionSystemTitle']['b5']?$admission_cfg['AdmissionSystemTitle']['b5']:$LangEn['Admission']['eAdmissionSystem'] ?>
    	<?php else: ?>
    		<span id="lblSchName-chi"><?=$basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5']?></span> <?=$admission_cfg['AdmissionSystemTitle']['en']?$admission_cfg['AdmissionSystemTitle']['en']:$LangB5['Admission']['eAdmissionSystem'] ?>
    	<?php endif; ?>
    </div>
</div>
<script id="stepHtml" type="text/html">
    <span class="step <%= cssClass %>">
        <div class="step-order <%= stepOrderCssClass %>"><%= stepOrder %></div>
        <div class="step-description"><div><%= title %></div></div>
    </span>
</script>


<?php
include(__DIR__.'/footer.php');
